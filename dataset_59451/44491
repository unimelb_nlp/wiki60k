{{About|the novella}}
{{Infobox book| 
| name = Flatland: A Romance of Many Dimensions
| image = Houghton EC85 Ab264 884f - Flatland, cover.jpg
| image_size = 200px <!-- prefer first edition-->
| caption = The cover to ''Flatland'', first edition
| author = [[Edwin A. Abbott]]
| illustrator = Edwin A. Abbott
| cover_artist =
| country = United Kingdom
| language = English
| genre = [[Novella]]
| publisher = [[Seeley & Co.]]
| release_date = 1884
| oclc = 2306280
| congress = QA699
| wikisource = Flatland
}}
'''''Flatland: A Romance of Many Dimensions''''' is a [[satire|satirical]] [[novella]] by the English schoolmaster [[Edwin Abbott Abbott]], first published in 1884 by [[Seeley & Co.]] of London.

Written pseudonymously by "A Square",<!-- note no period after the "A", which is not an initial but the indefinite article | it is, however, capitalized in the original work, and also set in the same typeface, suggesting a play on an initial, sans period --><ref>{{cite book|author=Abbott, Edwin A. |date=1884|title=Flatland: A Romance in Many Dimensions|publisher= Dover Thrift Edition (1992 unabridged)|location= New York|page= ii}}</ref> the book used the fictional [[two-dimensional space|two-dimensional world]] of Flatland to comment on the hierarchy of [[Victorian era|Victorian]] culture, but the novella's more enduring contribution is its examination of [[dimension]]s.<ref name="isbn0-465-01123-3 1"/>

Several films have been made from the story, including the feature film  ''[[Flatland (2007 film)|Flatland]]'' (2007). Other efforts have been short or experimental films, including one narrated by [[Dudley Moore]] and the short films ''[[Flatland: The Movie]]'' (2007) and ''[[Flatland 2: Sphereland]]'' (2012).<ref>{{cite news| url=https://www.sciencenews.org/article/flatland-and-its-sequel-bring-math-higher-dimensions-silver-screen |work=Science News |title=Review of ''Flatland: The Movie'' and ''Flatland 2: Sphereland''}}</ref>

== Plot ==
[[File:Houghton EC85 Ab264 884f - Flatland, men and women doors.jpg|thumb|Illustration of a simple house in Flatland.]]
The story describes a two-dimensional world occupied by geometric figures, whereof women are simple line-segments, while men are polygons with various numbers of sides. The narrator is a [[square (geometry)|square]] named A Square, a member of the caste of gentlemen and professionals, who guides the readers through some of the implications of life in two dimensions. The first half of the story goes through the practicalities of existing in a two-dimensional universe as well as a history leading up to the year 1999 on the eve of the 3rd Millennium.

On New Year's Eve, the Square dreams about a visit to a one-dimensional world (Lineland) inhabited by "lustrous points", in which he attempts to convince the realm's monarch of a second dimension; but is unable to do so. In the end, the monarch of Lineland tries to kill A Square rather than tolerate his nonsense any further.

Following this vision, he is himself visited by a three-dimensional [[sphere]] named A Sphere, which he cannot comprehend until he sees Spaceland (a tridimensional world) for himself. This Sphere visits Flatland at the turn of each millennium to introduce a new apostle to the idea of a third dimension in the hopes of eventually educating the population of Flatland. From the safety of Spaceland, they are able to observe the leaders of Flatland secretly acknowledging the existence of the sphere and prescribing the silencing of anyone found preaching the truth of Spaceland and the third dimension. After this proclamation is made, many witnesses are massacred or imprisoned (according to caste), including A Square's brother, B.

After the Square's mind is opened to new dimensions, he tries to convince the Sphere of the theoretical possibility of the existence of a fourth (and fifth, and sixth ...) spatial dimension; but the Sphere returns his student to Flatland in disgrace.

The Square then has a dream in which the Sphere visits him again, this time to introduce him to Pointland, whereof the point (sole inhabitant, monarch, and universe in one) perceives any communication as a thought originating in his own mind (cf. [[Solipsism]]):

{{quote|"You see," said my Teacher, "how little your words have done. So far as the Monarch understands them at all, he accepts them as his own{{spaced ndash}}for he cannot conceive of any other except himself{{spaced ndash}}and plumes himself upon the variety of ''Its Thought'' as an instance of creative Power. Let us leave this God of Pointland to the ignorant fruition of his omnipresence and omniscience: nothing that you or I can do can rescue him from his self-satisfaction."<ref>Abbott, Edwin A. (1884) [[wikisource:Flatland (first edition)/Other Worlds|Flatland]], ''Part II, § 20.—How the Sphere encouraged me in a Vision'', p 92</ref>
| the Sphere}}

The Square recognizes the identity of the ignorance of the monarchs of Pointland and Lineland with his own (and the Sphere's) previous ignorance of the existence of higher dimensions. Once returned to Flatland, the Square cannot convince anyone of Spaceland's existence, especially after official decrees are announced that anyone preaching the existence of three dimensions will be imprisoned (or executed, depending on caste). Eventually the Square himself is imprisoned for just this reason, with only occasional contact with his brother who is imprisoned in the same facility. He does not manage to convince his brother, even after all they have both seen. Seven years after being imprisoned, A Square writes out the book ''Flatland'' in the form of a memoir, hoping to keep it as posterity for a future generation that can see beyond their two-dimensional existence.

==Social elements==

Men are portrayed as [[polygons]] whose social status is determined by their regularity and the number of their sides, with a Circle considered the "perfect" shape. On the other hand, females consist only of lines and are required by law to sound a "peace-cry" as they walk, lest they be mistaken face-to-face for a [[Point (geometry)|point]]. The Square evinces accounts of cases where women have accidentally or deliberately stabbed men to death, as evidence of the need for separate doors for women and men in buildings.

In the world of Flatland, classes are distinguished by the "Art of Hearing", the "Art of Feeling", and the "Art of Sight Recognition". Classes can be distinguished by the sound of one's voice, but the lower classes have more developed vocal organs, enabling them to feign the voice of a Polygon or even a Circle. Feeling, practised by the lower classes and women, determines the configuration of a person by feeling one of its angles. The "Art of Sight Recognition", practised by the upper classes, is aided by "Fog", which allows an observer to determine the depth of an object. With this, polygons with sharp angles relative to the observer will fade more rapidly than polygons with more gradual angles. Colour of any kind is banned in Flatland after Isosceles workers painted themselves to impersonate noble Polygons. The Square describes these events, and the ensuing [[class conflict|class war]] at length.

The population of Flatland can "evolve" through the "Law of Nature", which states: "a male child shall have one more side than his father, so that each generation shall rise (as a rule) one step in the scale of development and nobility. Thus the son of a Square is a Pentagon, the son of a Pentagon, a Hexagon; and so on".

This rule is not the case when dealing with [[isosceles triangle|Isosceles Triangles]] (Soldiers and Workmen) with only two [[Congruence (geometry)|congruent]] sides. The smallest angle of an Isosceles Triangle gains thirty arc minutes (half a degree) each generation. Additionally, the rule does not seem to apply to many-sided Polygons. For example, the sons of several hundred-sided Polygons will often develop fifty or more sides more than their parents. Furthermore, the angle of an Isosceles Triangle or the number of sides of a (regular) Polygon may be altered during life by deeds or surgical adjustments.

An [[equilateral triangle|Equilateral Triangle]] is a member of the craftsman class. Squares and Pentagons are the "gentlemen" class, as doctors, lawyers, and other professions. Hexagons are the lowest rank of nobility, all the way up to (near) Circles, who make up the priest class. The higher-order Polygons have much less of a chance of producing sons, preventing Flatland from being overcrowded with noblemen.

Only regular Polygons are considered until chapter seven of the book when the issue of irregularity, or physical deformity, became considered. In a two dimensional world a regular polygon can be identified by a single [[angle]] and/or [[vertex (geometry)|vertex]]. In order to maintain social cohesion, irregularity is to be abhorred, with moral irregularity and criminality cited, "by some" (in the book), as inevitable additional deformities, a sentiment with which the Square concurs. If the error of deviation is above a stated amount, the irregular Polygon faces [[euthanasia]]; if below, he becomes the lowest rank of civil servant. An irregular Polygon is not destroyed at birth, but allowed to develop to see if the irregularity can be “cured” or reduced. If the deformity remains, the irregular is “painlessly and mercifully consumed.”<ref>{{citation|last=Abbott|first=Edwin A.|title=Flatland: A Romance of Many Dimensions|year=1952|place=New York|publisher=Dover|origyear=1884|page=31|edition=6th|isbn=0-486-20001-9}}</ref>

==As a social satire==
In ''Flatland'' Abbott describes a society rigidly divided into classes. Social ascent is the main aspiration of its inhabitants, apparently granted to everyone but strictly controlled by the top of the hierarchy. Freedom is despised and the laws are cruel. Innovators are imprisoned or suppressed. Members of lower classes who are intellectually valuable, and potential leaders of riots, are either killed, or promoted to the higher classes. Every attempt for change is considered dangerous and harmful. This world is not prepared to receive "Revelations from another world".

The satirical part is mainly concentrated in the first part of the book, "This World", which describes Flatland. The main points of interest are the Victorian concept of women's roles in the society and in the class-based hierarchy of men.<ref name="isbn0-465-01123-3 3">{{cite book |author=Stewart, Ian |title=The Annotated Flatland: A Romance of Many Dimensions |publisher=Basic Books |location=New York |year=2008 |pages= xvii |isbn=0-465-01123-3 |oclc= |doi= |accessdate=}}</ref> Abbott has been accused of misogyny due to his portrait of women in ''Flatland''. In his Preface to the Second and Revised Edition, 1884, he answers such critics by stating that the Square: {{quote|text=was writing as a Historian, he has identified himself (perhaps too closely) with the views generally adopted by Flatland and (as he has been informed) even by Spaceland, Historians; in whose pages (until very recent times) the destinies of Women and of the masses of mankind have seldom been deemed worthy of mention and never of careful consideration.|sign=the Editor}}

==Critical reception==
Although ''Flatland'' was not ignored when it was published,<ref name="Flatland Reviews">{{cite web |url=http://www.math.brown.edu/~banchoff/abbott/Flatland/Reviews/index.shtml |title=Flatland Reviews |work= |accessdate=2011-04-02}}</ref> it did not obtain a great success. In the entry on Edwin Abbott in the ''Dictionary of National Biography'', ''Flatland'' is not even mentioned.<ref name="isbn0-465-01123-3 1">{{cite book |author=Stewart, Ian |title=The Annotated Flatland: A Romance of Many Dimensions |publisher=Basic Books |location=New York |year=2008 |pages= xiii |isbn=0-465-01123-3 |oclc= |doi= |accessdate=}}</ref>

The book was discovered again after [[Albert Einstein]]'s [[General relativity|general theory of relativity]] was published, which introduced the concept of a fourth dimension. ''Flatland'' was mentioned in a letter entitled "Euclid, Newton and Einstein" published in ''[[Nature (journal)|Nature]]'' on February 12, 1920. In this letter Abbott is depicted, in a sense, as a prophet due to his intuition of the importance of ''time'' to explain certain phenomena:<ref name="isbn0-465-01123-3 4">{{cite book |author=Stewart, Ian |title=The Annotated Flatland: A Romance of Many Dimensions |publisher=Basic Books |location=New York |year=2008 |pages= 11|isbn=0-465-01123-3 |oclc= |doi= |accessdate=}}</ref><ref name="urlFlatland Reviews">{{cite web |url=http://www.math.brown.edu/~banchoff/abbott/Flatland/Reviews/1920nature.shtml |title=Flatland Reviews – Nature, February 1920 |accessdate=2011-04-02}}</ref>

{{quote|Some thirty or more years ago a little ''[[wiktionary:jeu d'esprit|jeu d'esprit]]'' was written by Dr. Edwin Abbott entitled ''Flatland''. At the time of its publication it did not attract as much attention as it deserved... If there is motion of our three-dimensional space relative to the fourth dimension, all the changes we experience and assign to the flow of time will be due simply to this movement, the whole of the future as well as the past always existing in the fourth dimension.|from a "Letter to the Editor" by William Garnett. in ''Nature'' on February 12, 1920.}}

The ''Oxford Dictionary of National Biography'' now contains a reference to ''Flatland''.

==Adaptations and parodies==
Numerous imitations or sequels to ''Flatland'' have been written, and multiple other works have alluded to it. Examples include:

===In film===
[[Flatland (1965 film)|''Flatland'']] (1965), an animated short film based on the novella, was directed by Eric Martin and based on an idea by [[John Hubley]].<ref>{{IMDb title|0823212}}</ref><ref>{{cite web |title=DER Documentary: Flatland |url=http://www.der.org/films/flatland.html |accessdate=11 October 2012}}</ref><ref>{{cite web |title=Flatland Animation: The project |url=http://flatlandanimation.blogspot.com/2011/05/project.html |accessdate=11 October 2012}}</ref>

[[Flatland (2007 film)|''Flatland'']] (2007), a 98-minute animated independent feature film version directed by [[Ladd Ehlinger Jr.|Ladd Ehlinger Jr]],<ref name="flatlandthefilm">{{cite web | title=''Flatland the Film'' | url=http://www.flatlandthefilm.com | accessdate=2007-01-14}}</ref> updates the satire from Victorian England to the modern-day United States.<ref name="flatlandthefilm"/>

''[[Flatland: The Movie]]'' (2007), by Dano Johnson and Jeffrey Travis,<ref>{{cite web | title=''Flatland: The Movie'' | url=http://www.flatlandthemovie.com | accessdate=2007-01-14}}</ref> is a 34-minute animated educational film.<ref>{{cite web|url=http://www.imdb.com/title/tt0814106/|title= IMDB Flatland: The Movie}}</ref> Its sequel was ''[[Flatland 2: Sphereland]]'' (2012), inspired by the novel ''[[Sphereland]]'' by [[Dionys Burger]].<ref>{{cite web | title=''Flatland 2: Sphereland'' | url=http://www.spherelandthemovie.com }}</ref><ref>{{IMDb title|tt2062960|Flatland 2: Sphereland}}</ref><ref>[http://geekdad.com/2013/06/flatland-and-sphereland/ GeekDad.com Review of Flatland: The Movie and Flatland 2: Sphereland]</ref>

===In literature===
Books and short stories inspired by ''Flatland'' include:
*''An Episode on Flatland: Or How a Plain Folk Discovered the Third Dimension'' by [[Charles Howard Hinton]] (1907)
*''[[The Dot and the Line|The Dot and the Line: A Romance in Lower Mathematics]]'' by [[Norton Juster]] (1963)
*''[[Sphereland]]'' by [[Dionys Burger]] (1965)
*''The Incredible Umbrella'' by [[Marvin Kaye]] (1980)
*"Message Found in a Copy of ''Flatland''" by [[Rudy Rucker]] (1983)
*''[[The Planiverse]]'' by [[A. K. Dewdney]] (1984)
*''[[Flatterland]]'' by [[Ian Stewart (mathematician)|Ian Stewart]] (2001)
*''[[Spaceland (novel)|Spaceland]]'' by [[Rudy Rucker]] (2002)
*''VAS: An Opera in Flatland'' (2002) by [[Steve Tomasula]], which uses Square and Circle and their two-dimensional world to critique contemporary society<ref>{{cite journal|last1=Vanderborg|first1=Susan|title=Of 'Men and Mutations': The Art of Reproduction in Fatland|journal=Journal of Artistic Books|date=Fall 2008|issue=24|pages=4–11}}</ref>
*''Unflattening'' (2015) by Nick Sousanis

Physicists and science popularizers [[Carl Sagan]] and [[Stephen Hawking]] have both commented on and postulated about the effects of ''Flatland''. Sagan recreates the [[thought experiment]] as a set-up to discussing the possibilities of higher dimensions of the physical universe in both the [[Cosmos (book)|book]] and [[Cosmos: A Personal Voyage|television series ''Cosmos'']],<ref name=Sagan>{{cite book|last=Tremlin|first=Todd|title=Minds and Gods: The Cognitive Foundations of Religion|date=2006|publisher=Oxford University Press|location=USA|isbn=978-0199739011|page=91|url=https://books.google.com/books?id=BLKvDNpSvBAC&pg=PA91&dq=cosmos+sagan+flatland&hl=en&sa=X&ei=ukciU9HaJMHCywH10YHYCg&ved=0CDYQ6AEwAg#v=onepage&q=cosmos%20sagan%20flatland&f=false}}</ref> whereas Dr. Hawking notes the impossibility of life in two-dimensional space, as any inhabitants would necessarily be unable to digest their own food.<ref name=Hawking>{{cite book|last=Gott|first=J. Richard|title=Time Travel in Einstein's Universe: The Physical Possibilities of Travel through Time|date=2001-05-21|publisher=Houghton Mifflin Company|location=USA|isbn=978-0395955635|page=61|url=https://books.google.com/books?id=MME33bSTCDsC&pg=PA61&lpg=PA61&dq=a+brief+history+of+time+flatland&source=bl&ots=MslIxWYQ6B&sig=2L_JyJOWl7I6EWTY_P6TiJhyfsY&hl=en&sa=X&ei=O0UiU5O3G6yMyAHOloHYDQ&ved=0CC0Q6AEwAQ#v=onepage&q=a%20brief%20history%20of%20time%20flatland&f=false}}</ref>

===In television===
''Flatland'' features in ''[[The Big Bang Theory]]'' episode "The Psychic Vortex",<ref name="Psychic Vortex">{{cite web|last=VanDerWerff|first=Todd|title=The Big Bang Theory: "The Psychic Vortex"|url=http://www.avclub.com/tvclub/the-big-bang-theory-the-psychic-vortex-36976|publisher=A.V. Club|accessdate=2014-03-14}}</ref> when Sheldon Cooper declares it one of his favourite imaginary places to visit.<ref name=Sheldon>{{cite web|title=Flatland Featured on The Big Bang Theory on CBS Television|url=http://www.giantscreencinema.com/News/tabid/70/ctl/ViewItem/mid/528/ItemId/730/Default.aspx?SkinSrc=/Portals/_default/Skins/NewSkinner/News&ContainerSrc=/Portals/_default/Containers/NewSkinner/Basic|publisher=Giant Screen Cinema Association|accessdate=2014-03-14}}</ref>

It also features in the ''[[Futurama]]'' episode "2-D Blacktop", when Professor Farnsworth's adventures in [[drag racing]] lead to a foray of drifting in and out of inter-dimensional spaces.<ref name=Futurama>{{cite web|last=DeNitto|first=Nick|title=Futurama Invades Flatland|url=http://stagebuddy.com/comedy/futurama-invades-flatland/|publisher=Stage Buddy|accessdate=2014-03-14}}</ref>

The main antagonist of ''[[Gravity Falls]]'', Bill Cipher, is hinted to hail from a world like ''Flatland'', describing the inhabitants of his dimension as "flat minds in a flat world with flat dreams"<ref>{{Cite episode |title=Weirdmageddon 3: Take Back The Falls |series=Gravity Falls |series-link=Gravity Falls |station=Disney XD |date=May 3, 2016 |season=2 |number=20 |quote='''Bill Cipher:''' You think those chains are tight? Imagine living in the second dimension, flat minds in a flat world with flat dreams. I liberated my dimension, Stanford, and I'm here to liberate yours.}}</ref> and responding to inquiries about his origins on Reddit that "EDWIN ABBOTT ABBOTT HAS A DECENT IDEA."<ref>{{cite web|url=https://www.reddit.com/r/gravityfalls/comments/315yoy/im_bill_cipher_i_know_lots_of_things_ask_me/cpyoywu|website=Reddit|accessdate=3 May 2016}}</ref>

==See also==
*''[[Animal Farm]]'' (1945), novella by George Orwell
*[[Fourth dimension in literature]]
*''[[Godel, Escher, Bach]]''
*[[Sphere-world]]
*''[[Triangle and Robert]]'' (1999–2007 webcomic)
*[["—And He Built a Crooked House—"]] (1941 short story)
*Dimension-bending video games:
**''[[Super Paper Mario]]'' (2007)
**''[[Crush (video game)|Crush]]'' (2007)
**''[[Echochrome]]'' (2008)
**''[[Lost in Shadow]]'' (2010)
**''[[Fez (video game)|Fez]]'' (2012)
**''[[The Legend of Zelda: A Link Between Worlds]]'' (2013)
**''[[The Bridge (video game)]]'' (2013)
**''[[Miegakure]]'' (in development)

==References==
{{Reflist|30em}}

==External links==
{{Wikisource}}
{{Commons category}}
* [http://www.npr.org/2012/09/21/161551778/the-scifri-book-club-visits-flatland  "Sci-Fri Bookclub"]—recording of National Public Radio discussion of ''Flatland'', featuring mathematician Ian Stewart (Sept. 21, 2012)

=== Online and downloadable versions of the text ===
;eBooks
* [[wikisource:Flatland (first edition)|''Flatland, a Romance of Many Dimensions (first edition)'']] on Wikisource
* [[wikisource:Flatland (second edition)|''Flatland, a Romance of Many Dimensions (second edition)'']] on Wikisource
* {{gutenberg|no=97|name=Flatland}}, text, no illustrations
* {{gutenberg|no=201|name=Flatland}}, with [[ASCII art|ASCII illustrations]]
* [https://archive.org/details/flatlandromanceo00abbouoft ''Flatland''], digitized copy of the first edition from the [[Internet Archive]]
* [http://www.geom.uiuc.edu/~banchoff/Flatland/ ''Flatland'' (Second Edition), Revised with original illustrations] (HTML format, one page)
* [http://xahlee.org/flatland/index.html ''Flatland'' (Fifth Edition), Revised, with original illustrations] ([[HTML]] format, one chapter per page)
* [https://github.com/Ivesvdf/flatland/blob/master/oneside_a4.pdf?raw=true ''Flatland'' (Fifth Edition), Revised, with original illustrations]  ([[PDF]] format, all pages, with LaTeX source on [https://github.com/Ivesvdf/flatland github])
* [http://manybooks.net/titles/abbottedetext95flat10a.html ''Flatland''] (illustrated version) on Manybooks
* {{OL work|id=OL118420W}}

;Recording
* {{librivox book | title=Flatland: A Romance of Many Dimensions | author=Edwin Abbott Abbott}}

{{Flatland}}

[[Category:1880s science fiction novels]]
[[Category:1884 novels]]
[[Category:British science fiction novels]]
[[Category:Dimension]]
[[Category:Fictional dimensions]]
[[Category:Mathematics fiction books]]
[[Category:Novels about totalitarianism]]
[[Category:Satirical novels]]
[[Category:Works published under a pseudonym]]
[[Category:19th-century British novels]]
[[Category:British novels adapted into films]]