{{Infobox person
| name        = Bradish Johnson
| image       = Bradish_Johnson_1811-1892.jpg
| alt         = 
| caption     = Bradish Johnson
| birth_date  = {{Birth date|1811|04|22}} 
| birth_place = [[Plaquemines Parish, Louisiana]]
| death_date  = {{Death date and age|1892|11|03|1811|04|22}}  
| death_place = Bayside, New York
| nationality = American
| residence   = New York City, New Orleans
| spouse      = Louisa Anna Lawrence
| other_names = 
| known_for   = 1858 "Swill Milk" scandal
| occupation  = Sugar and distillery magnate
}}
'''Bradish Johnson''' (April 22, 1811 – November 3, 1892) was an American industrialist. He owned plantations and [[sugar refineries]] in [[Louisiana]] and a large [[distillery]] in New York City. In 1858 his distillery was at the heart of a scandal when an exposé in a weekly magazine accused it (and other distilleries) of producing altered and unsafe milk, called "[[swill]] milk", for sale to the public. The swill milk scandal helped to create the demand for [[consumer protection law]]s in the United States.

==Early life and education==
Bradish Johnson's father, William M. Johnson, was a sea captain from [[Nova Scotia]]. In 1795 he purchased land in [[Plaquemines Parish]], Louisiana, along with a partner from Salem, Massachusetts named George Bradish.  The partners built a sugar plantation there called "Magnolia", where they settled and began to produce sugar. In the 1830s, William Johnson moved his family to a new plantation four miles further up the Mississippi River, in [[Pointe à la Hache, Louisiana]]. He named his new plantation "Woodland".<ref name=Obit>{{cite news|title=Obituary-Bradish Johnson|url=https://query.nytimes.com/mem/archive-free/pdf?res=F30911FC345515738DDDAC0894D9415B8285F0D3|accessdate=25 February 2013|newspaper=New York Times|date=November 5, 1892}}</ref>

Bradish Johnson, born in 1811, was the third of four sons. He was named after his father's business partner, George Bradish. By 1820, Captain William Johnson had also begun purchasing property on the West side of [[Manhattan]] and had gone into the distillery and sugar refining business in New York. Bradish Johnson, who was born in Louisiana, attended [[Columbia College, Columbia University|Columbia College]] in New York City, graduating in the class of 1831.<ref name="Louisiana Planter 1892">{{cite journal|journal=The Louisiana Planter and Sugar Manufacturer|date=12 Nov 1892|volume=9|page=350|url=https://books.google.com/?id=JfMoAAAAYAAJ&pg=PA350&dq=%22bradish+johnson%22#v=onepage&q=%22bradish%20johnson%22&f=false|accessdate=23 February 2013|title=The Louisiana Planter and Sugar Manufacturer}}</ref>

==Business==
Johnson started out as a partner in the distilling company William Johnson and Sons.  After his father's death, he went into business with a man named Moses Lazarus as Johnson and Lazarus. Upon the retirement of Lazarus, the firm was renamed Bradish Johnson and Sons. The Johnsons owned several properties, including a distillery at 244 Washington Street. The largest facility occupied two city blocks near the [[Hudson River]], from Ninth Avenue to Eleventh Avenue between 15th and 16th Streets. The distillery was east of Tenth Avenue, while the cow barns and dairy were located west of Tenth.

Through his distilleries and his investments in real estate, Johnson became very wealthy. He became one of the directors of the [[Chemical Bank]] of New York when it was rechartered in 1844. He served as a director for the next twenty years.<ref name="Chemical Bank">{{cite book|last=Chemical Corn Exchange Bank|title=History of the Chemical Bank|year=1913|publisher=Private printing|pages=104–106|url=https://books.google.com/books?id=dvFCAAAAIAAJ&dq=%22bradish%20johnson%22%20louisa%20lawrence%20marriage&pg=PA106#v=onepage&q=%22bradish%20johnson%22%20louisa%20lawrence%20marriage&f=false}}</ref> Johnson was an innovator in the sugar industry, and his refinery was the first to "successfully make use of centrifugal machines in the manufacture of sugar".<ref name=Obit />

Johnson inherited [[Woodland Plantation (West Pointe a la Hache, Louisiana)|Woodland Plantation]] from one of his brothers before the Civil War. He eventually purchased a number of other plantations in the area: Pointe Celeste, Bellevue, and the Orange Farm. He also acquired two plantations above New Orleans which he renamed after his married daughters: [[Whitney Plantation Historic District|Whitney Plantation]] and Carroll Plantation.<ref name="Louisiana Planter 1892" />

When Johnson's estate was settled in 1900, it included 31 pieces of New York real estate, which together added up to 78 acres. All the lots were purchased by a corporation formed by his heirs for a total of $4,769,100.<ref name="BJ Estate">{{cite news|title=Bradish Johnson Holdings Bring $4,769,100 at Auction|url=https://query.nytimes.com/mem/archive-free/pdf?res=FB0D1FF93C5B11738DDDAB0994D8415B808CF1D3|accessdate=26 February 2013|newspaper=New York Times|date=October 12, 1900}}</ref>

=="Swill milk" scandal==
[[File:Swillmilk1.jpg|thumb|left|A 19th century illustration of "swill milk" being produced: a sickly cow being milked while held up by ropes.]]
{{main article|Swill milk scandal}}
The  Johnson & Lazarus distillery at 16th Street was the subject of a famous [[muckraking]] exposé by ''[[Frank Leslie’s Illustrated Newspaper]]'' in 1858.<ref name=NYT/> Distilleries in 19th century New York had to dispose of the tons of organic waste they generated, and their solution was to feed the still hot mash to hundreds of sick old cows and then sell the milk.  The cows were crowded into filthy stables, and were so sickly that some of them were reportedly held up by slings.  The milk, referred to as "[[swill milk]]", was often cut with water and then thickened with chalk or flour. Swill milk was accused of being a major cause of infant mortality — it was sold from pushcarts all over the city, advertised, e.g., as farm-fresh milk from [[Orange County, New York|Orange County]].<ref name=NYT>{{cite news|title=How We Poison Our Children|url=https://query.nytimes.com/mem/archive-free/pdf?res=F10913FB3E581B7493C1A8178ED85F4C8584F9|accessdate=22 February 2013|newspaper=New York Times|date=May 13, 1858}}</ref>

Johnson was a supporter of the [[Tammany Hall]] politician Alderman [[Michael Tuomey (politician)|Michael Tuomey]], known as "Butcher Mike".  Tuomey defended the distillers vigorously throughout the scandal — in fact, he was put in charge of the Board of Health investigation.  ''Frank Leslie’s Illustrated Newspaper'' staked out Johnson's mansion at 21st and Broadway, and reported that in the midst of the investigation, Tuomey was observed making late night visits. The Board of Health exonerated the distillers, but public outcry led to the passage of the first food safety laws in the form of milk regulations in 1862.<ref name=Swindled>{{cite book|last=Wilson|first=Bee|title=Swindled|year=2008|publisher=Princeton University Press|page=162}}</ref>

==Civil War years==

===Petitioning Lincoln===
In 1863 Johnson took a leading part in the "Conservative Unionists", a group of businessmen with interests in the South who wanted occupied Louisiana let back into the Union with her 1852 constitution intact. They claimed that the state constitution had not been dissolved and the secession was illegal, so the President should allow the state back into the Union with slavery intact. Johnson and two other plantation owners made their argument in a letter to President Lincoln, reinforced by a personal visit. Lincoln was not impressed. In his dismissive response he wrote "I do not perceive how such committal could facilitate our military operations in Louisiana, I really apprehend it might be so used as to embarrass them."<ref name=Scalawags>{{cite book|last=Wetta|first=Frank Joseph|title=The Louisiana Scalawags|year=2013|publisher=LSU Press|pages=74–75|url=https://books.google.com/?id=8WPwUx-qFYAC&pg=PA73&dq=%22bradish+johnson%22#v=onepage&q=%22bradish%20johnson%22&f=false|isbn=9780807147467}}</ref>

===Johnson v. Dow===
In 1863 Johnson brought a suit against a Union general.  The suit claimed that in 1862 the occupying Union Army, under the command of General [[Neal S. Dow]] of the 13th Maine Regiment, 'took from Johnson's plantation twenty-five hogsheads of sugar, plundered the dwelling-house hereon and took one silver pitcher, one-half dozen silver knives, one-half dozen silver spoons, one fish knife, one-half dozen silver teaspoons and other articles.'

Johnson presented himself as a loyal citizen of the Union, residing in New York, who had simply been robbed by the Union Army. He was awarded $1750 in damages by the court. When Dow failed to pay him, he sued Dow in Dow's home state of Maine after the war. The case went all the way to the Supreme Court, which handed down a decision in 1876 against Johnson, pointing out that his holdings were in conquered territory during a time of war, and that it would be very hard to engage in warfare if the enemy could sue for damages. "Johnson v. Dow" became a hot topic of debate during the heated [[United States presidential election, 1876|Tilden-Hayes Presidential election of 1876]], as the country tried to figure out the confusing nature of the status of the defeated Confederate states.<ref name="Johnson v. Dow">{{cite  book|last=Clifford|first=Phillip Greely|title=Nathan Clifford, Democrat|year=1922|publisher=G. P. Putnam|pages=295–307|url=https://books.google.com/?id=aZIsAAAAIAAJ&dq=nathan+clifford+democrat}}</ref>

===Woodland described===
In 1863 the Union Army's "Office of Negro Labor" was sent to Woodland to investigate conditions there.  They found that on the plantation "great ill feeling and discontent" existed.  The slaves begged to be given permission to enlist in the Union Army.  They complained that their rations were "unfairly curtailed" by the overseer and that he was "lecherous toward their women".  After the inspectors had left, the overseer is said to have "harangued the Negroes, boasted of his unlimited power over them," and "used seditious and insulting language" towards the Union.<ref name="Emma Lazarus">{{cite book|last=Young|first=Bette Roth|title=Emma Lazarus and Her World|year=1997|publisher=Jewish Publication Society|page=49|url=https://books.google.com/books?id=da8rJnTp7mAC&pg=PR7&lpg=PP1&dq=emma+lazarus+and+her+world|isbn=9780827606180}}</ref>  This report presents a very different picture from the one that appeared in Johnson's ''New York Times'' obituary and in the official history of the Chemical Corn Exchange Bank, which claimed that he had freed his slaves prior to the [[Emancipation Proclamation]].<ref name=Obit /><ref name="Chemical Bank" />

==Personal life==
[[File:New Orleans 2343 Prytania Street.jpg|thumb|right|The Bradish Johnson House, New Orleans]]
Johnson married a New Yorker named Louisa Anna Lawrence around 1834. Together they had ten children. Their New York residence was located near fashionable [[Madison Square]], at 21st Street on the short block between Broadway and Fifth Avenue.<ref>In opposition to a plan to run a railroad up Fifth Avenue in 1885,  group, including Cornelius Vanderbilt, Chauncey M. Depew, John Sloane, Bradish Johnson and William Waldorf Astor, organized the [[Fifth Avenue Association|Association for the Protection of the Fifth Avenue Thoroughfare]] (Robert T. Swaine, ''The Cravath Firm And Its Predecessors: 1819-1947'', 2006:413ff).</ref>  In 1874 Johnson retired from business in New York and moved to New Orleans, where he had a new Italianate mansion built in the [[Garden District, New Orleans|Garden District]] at 2343 Prytania Street. The house is now home to the Louise S. [[McGehee School]]. The family also had an estate in East Islip, on the South Shore of Long Island, NY, which is where Johnson died on November 3, 1892.<ref name=Obit /> Johnson is interred at [[Green-Wood Cemetery]] in [[Brooklyn]], [[New York (state)|New York]].

==Legacy==
After Johnson retired to New Orleans, his house at 21st and Broadway was home to the [[Lotos Club]]. In 1918 the Johnson heirs had an office building erected on the site.  The building, at 921–925 Broadway, is called the Bradish Johnson Building.<ref name="BJ Building">{{cite web|title=Emporis|url=http://www.emporis.com/building/bradishjohnsonbuilding-newyorkcity-ny-usa|accessdate=25 February 2013}}</ref>

An image of "Woodland" was used on the logo of the liqueur [[Southern Comfort]] from 1934 to 2001.<ref name="Southern Comfort">{{cite web|title=SouthernComfort.com|url=http://www.southerncomfort.com/history.aspx|accessdate=26 February 2013}}</ref>

Since 1997 the site of the Johnson & Lazarus distillery at 16th Street and Ninth Avenue, later the factory of [[Nabisco|National Biscuit Company]],  has been home to [[Chelsea Market]].

The house at [[Woodland Plantation (West Pointe a la Hache, Louisiana)|Woodland Plantation]]  has been listed on the [[National Register of Historic Places]] since 1998, and is operated as a bed and breakfast.

==References==
{{reflist}}

== External links ==
*[http://mcnycatablog.org/2013/01/16/bradish-johnson-papers-1818-1894-bulk-1830-1870/ Finding aid for the Bradish Johnson Papers at the Museum of the City of New York]
*[http://www.green-wood.com/burial_results/index.php Green-Wood Cemetery Burial Search]

{{DEFAULTSORT:Johnson, Bradish}}
[[Category:1811 births]]
[[Category:1892 deaths]]
[[Category:People from Plaquemines Parish, Louisiana]]
[[Category:People from New York City]]
[[Category:American industrialists]]
[[Category:Columbia University alumni]]
[[Category:People from Bay Shore, New York]]