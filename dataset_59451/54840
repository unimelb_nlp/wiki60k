[[File:TheTragedyOfTheKorosko.jpg|thumb|First edition (publ. [[Smith, Elder & Co.]])]]
{{DISPLAYTITLE:''The Tragedy of the ''Korosko}}
'''''The Tragedy of the''''' '''Korosko''' ([[1898 in literature|1898]]) is a [[novel]] by [[Arthur Conan Doyle|Sir Arthur Conan Doyle]]. It was serialized a year earlier in [[The Strand Magazine|''The Strand'' magazine]] between May and December 1897. It was later adapted into a play ''[[Fires of Fate (play)|Fires of Fate]]'' by Doyle. The play was in turn twice adapted into film, a [[Fires of Fate (1923 film)|1923 silent film]] and a [[Fires of Fate (1932 film)|1932 talkie]].

==Plot summary==
A group of European tourists are enjoying their trip to [[Egypt]] in the year 1895. They are sailing up the River [[Nile]] in "a turtle-bottomed, round-bowed stern-wheeler", the ''Korosko''. They intend to travel to [[Abousir]] at the southern frontier of Egypt, after which the [[Dervish#Historical and political use|Dervish]] country starts. They are attacked and abducted by a marauding band of Dervish warriors. The novel contains a strong defence of British [[Imperialism]] and in particular the Imperial project in North Africa. It also reveals the very great suspicion of [[Islam]] felt by many Europeans at the time.

==External links==
{{Gutenberg|no=21768|name=The Tragedy of the Korosko}}
* {{librivox book | title=The Tragedy of the Korosko | author=Sir Arthur Conan Doyle}}

{{Conan Doyle}}

{{DEFAULTSORT:Tragedy Of The Korosko, The}}
[[Category:1898 novels]]
[[Category:Novels by Arthur Conan Doyle]]
[[Category:Novels first published in serial form]]
[[Category:Works originally published in The Strand Magazine]]
[[Category:Novels set in Egypt]]
[[Category:1895 in fiction]]


{{1890s-novel-stub}}