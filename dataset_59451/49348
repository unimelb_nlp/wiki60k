{{Infobox newspaper
| name                 = Lögberg-Heimskringla
| logo                 = 
| image                = 
| caption              = 
| type                 = Icelandic-Canadian community newspaper
| format               = semi-monthly (formerly weekly)
| owners               = 
| founder              = Frimann B. Anderson (Heimskringla, 1886)
| publisher            = 
| editor               = Stefan M. Jonasson
| chiefeditor          = 
| assoceditor          = 
| maneditor            = 
| newseditor           = 
| managingeditordesign = 
| campuseditor         = 
| campuschief          = 
| opeditor             = 
| sportseditor         = 
| photoeditor          = 
| seniorstaff          =
| staff                = see http://www.lh-inc.ca/whatisLH.asp
| foundation           = October 7, 1886 (Heimskringla) and January 14, 1888 (Lögberg)
| political            = 
| language             = [[English language|English]] and formerly [[Icelandic language|Icelandic]]
| ceased publication   = 
| relaunched           =
| headquarters         = [[Winnipeg, Manitoba]]
| circulation          = 
| sister newspapers    = 
| ISSN                 = 0047-4967
| oclc                 = 
| website              = {{URL|www.lh-inc.ca}}
}}

'''Lögberg-Heimskringla''' is a community newspaper serving the Icelandic community in North America. A former weekly, it is currently published twice per month in [[Winnipeg, Manitoba|Winnipeg]], [[Canada]]. The newspaper was created in 1959 by the amalgamation of two newspapers, the Heimskringla and the Lögberg, which had been in publication in North America since the 1880s

== History ==

=== Background ===
Icelandic immigration to North America actually began in the 1850s.<ref>Walter J. Lindal, The Icelanders in Canada (Ottawa and Winnipeg: National Publishers and Viking Printers, 1967), 77.</ref>  The main immigration began in 1870 with a small group of people immigrating to North America, and continued on a much larger scale through 1914 and later.<ref>Júníus H. Kristinsson, Vesturfaraskrá 1870-1914: A Record of Immigrants from Iceland to America 1870-1914 (Reykjavík: Institute of History, University of Iceland, 1983), XVI.</ref> [[Iceland]] has always been a very literate country, arguably fostering the largest number of poets per capita of any country in the world.<ref>Deborah A. Styles, Societal Influences on Children’s Psychology: The Schools in Iceland and Singapore
Promote Prosocial Values, Positive Self-Concepts, and Achievement in Young Adolescents (Paper Presented at the Oxford Round Table, London: Webster University, 2005) 5.</ref>

The very first newspaper to be published in North America by the Icelandic immigrant population was handwritten by Jon Gudmundsson in 1876, and was called “Nýi Þjóðólfur”.<ref>Lindal, The Icelanders in Canada, 125.</ref> In 1877, the first edition of a newspaper printed on a printing press, “Framfari” was published on September 1 at Lundi.  The need for [[Icelandic language]] publications was felt keenly by the immigrants, who felt it important from the beginning to preserve both their language and their culture.<ref>Wilhelm Kristjanson, The Icelandic People in Manitoba: a Manitoba Saga ISBN 0-9694651 1-4 (Winnipeg: Wallingford Press,1965), 60.</ref> This was followed by “Leifur” and several other publications.<ref>David Gislason, “Why Lögberg and Why Heimskringla?,” Lögberg-Heimskringla, {{ISSN|0047-4967}}, 15 October 2011, Special Anniversary Feature 1.</ref>

=== Heimskringla newspaper ===
The first of the larger Icelandic weeklies was Heimskringla.  Heimskringla derived its name from the sagas of the Norse Kings, written in the early 13th century by Icelandic poet and historian [[Snorri Sturluson|Snorri Sturlusson]]. It is an amalgamation of the first two words of one of the historic manuscripts, “Ynglinga Saga” which are “kringla heimsins”, meaning “the round world”.<ref>Snorri Sturlusson, Heimskringla or The Chronicle of the Kings of Norway (c.1230), trans. Douglas B. Killings, Online Medieval and Classical Library, http://omacl.org/Heimskringla/ (accessed 2 Aug, 2012) 1</ref>

The founder of the newspaper was Frimann B. Anderson, who worked with Eggert Johannsson and [[Einar Hjörleifsson Kvaran|Einar Hjörleifsson]] on the publication, which first appeared on September 9, 1886.<ref>Heimskringla, 9 Sept. 1886. Tímarit, http://timarit.is/view_page_init.jsp?pubId=129&lang=en , (accessed 2 Aug., 2012).</ref>  In the first issue, the paper commits to cover the interests of Icelanders in the west, including public affairs, employment, and education. It vows to keep Western Icelanders in touch with the politics and culture of their homeland. It states that it will be politically independent, and would include works of fiction and poetry.  It said, however, that it would not be an agent of immigration.

The paper‘s early days were fraught with problems, both of personality clashes and financial difficulties.<ref>Kristjanson, The Icelandic People in Manitoba, 249.</ref>  There was a period from December 16, 1886 when the paper did not publish again until April 7, 1887.  Heimskringla had its ups and downs financially and with a series of editors through March, 1898, when [[Baldwin Baldwinson|Baldvin L. Baldwinson]] became publisher and editor.  His management skills were good and the paper prospered under him.  After its first year, Heimskringla became Conservative in Canadian politics and Democratic in United States politics, and espoused liberal religious views.<ref>Kristjanson.  The Icelandic People in Manitoba, 251.</ref>   Baldvin L. Baldwinson remained with Heimskringla until April 17, 1913.

=== Lögberg newspaper ===
Partly as a result of Icelandic immigrants having aligned themselves with different political parties and religions, there became a demand for another newspaper as a competitor for Heimskringla.<ref>Lindal. The Icelanders in Canada, 311.</ref> On January 14, 1888 the new Lögberg was launched by six men – Sigtryggur Jónasson, Bergvin Jónsson, Arni Frederickson, Einar Hjörleifsson (formerly of Heimskringla), Ólafur Thorgeirsson, and Sigurdur J. Johannesson.<ref>Kristjanson, The Icelandic People in Manitoba, 252.</ref>  The name of the paper means, literally, Law Rock and refers to the rock on which the speaker of the ancient Althing in Iceland read the codified laws out loud. Lögberg emphasized its ties with the now-defunct Framfari and with Leifur, also defunct.  It stated from the beginning that if only one Icelandic weekly could survive, then the better of the two would remain.<ref>David Gislason, “Why Lögberg and Why Heimskringla?“</ref>

The first editor of Lögberg was [[Einar Hjörleifsson Kvaran|Einar Hjörleifsson]], who remained in his position until February 1895 and then returned to Iceland.  During his editorship, he had strengthened Icelandic cultural groups in Winnipeg, taking part  in many events.  He was also a brilliant translator of books, poems, and short stories from English to Icelandic, publishing them in the paper.<ref>Kristjanson, The Icelandic People in Manitoba, 255.</ref>

The paper began with exactly the same format as its rival, Heimskringla.  It was 4 pages long, and the subscription rate was ½ that of Heimskringla.  On January 15, 1890 Lögberg increased its size to 8 pages, doubling that of Heimskringla.<ref>Lögberg, 15 Jan. 1890. Tímarit, http://timarit.is/view_page_init.jsp?pubId=132&lang=en (accessed 3 Aug., 2012).</ref> A regular women´s interest column, edited by Ingibjorg Jonsson, was introduced in Lögberg in the 1950s.<ref>Kristjanson, The Icelandic People in Manitoba, 499.</ref>

== Amalgamation ==
Both newspapers, the Heimskringla and Lögberg were important influences in keeping the Icelandic community in North America informed about communities in other places, politics and culture in their homeland, literature, religion, and politics in North America.  In both newspapers, different editors had different ideas and focus and varied the newspapers in coverage during their tenure.  Both newspapers stressed the importance of loyalty to their readers' North American communities and countries.  It was rare to find an Icelandic home in North America that did not subscribe to one or the other newspaper, and some subscribed to both.  However, due to differences in the two papers regarding politics and religious stances, some people were either fiercely in the camp of Heimskringla or vice versa.

===  August 1959 amalgamation of Lögberg and Heimskringla  ===
For several years prior to the actual amalgamation of the two weekly Icelandic newspapers, there was talk of such an amalgamation.  However, this concept was not accepted at the time.  [[Paul Thorlakson|Dr. P.H.T. Thorlakson]] on behalf of Lögberg, and Senator G.S. Thorvaldson on behalf of Heimskringla had been conducting negotiations for amalgamation during this time.<ref>Lindal, The Icelanders in Canada, 312.</ref>   The amalgamation finally took place as a result of finances.  Both newspapers needed to apply for funds from the Canada Iceland Foundation, which was considered to be too much.  The Icelandic Canadian journal was also supported by that Foundation.<ref>Jonas  Thor.  “The Amalgamation of Heimskringla and Lögberg.” Lögberg-Heimskringla, {{ISSN|0047-4967}} 12 September 1986, 5 and 7.</ref>

The amalgamation was finally achieved by a voluntary committee of five – Stefan Hansen, Chairman, Consul Grettir I. Johannson, Judge W. J. Lindal, Rev. Philip M. Petursson, and Dr. P.H. T. Thorlakson.  The North American Publishing Company was formed, which took over the assets of both newspapers. The president of the company was Arni G. Eggertson. An editorial committee was formed.  Dr. P.H.T. Thorlakson was the chairman, and Ingibjorg Jónsson was named editor, a post she held from August 20, 1959 until April 29, 1971.<ref>Kristjanson, remaining with Lögberg-Heimskringla as editor emeritus until October 23, 1975. The Icelandic People in Manitoba, 499-500.</ref>   Both Dr. Thorlakson and Judge Lindal wrote articles about the amalgamation.  Dr. Thorlakson‘s article appeared in the Lögberg-Heimskringla’s inaugural issue on August 20, 1959.<ref>Dr. Ken Thorlakson, “Why Lögberg-Heimskringla?” Lögberg-Heimskringla, {{ISSN|0047-4967}} October, 2011, Special Anniversary Feature, 1.</ref>   Judge Lindal’s article appeared in The Icelandic Canadian’s Fall 1959 issue.<ref>Thorlakson, “Why Lögberg-Heimskringla?”. 1</ref>

As for the combination of the two names, it was decided to keep both names for their emotional impact, and to use Lögberg first, because it sounded better that way.<ref>Jonas Thor. “The Amalgamation of Heimskringla and Lögberg.” 5.</ref>   It was decided at the time, that the newspaper would remain in the Icelandic language and be an instrument of the preservation of the language among immigrants from Iceland and their families The Icelandic Canadian magazine would fulfill the role of an English-language publication of Icelandic heritage interest.<ref>Jonas Thor. “The Amalgamation of Heimskringla and Lögberg.” 7.</ref>  However, English-language articles began to appear immediately.  In the inaugural issue of Lögberg-Heimskringla on August 20, 1959, the article written by Dr. P H. T. Thorlakson about the amalgamation was written in English, and appeared on the front page of the new newspaper.<ref>Dr. P. H. T. Thorlakson. “”An Adventure in Co-operation”  Lögberg-Heimskringla, 20 August 1959. 1  Tímarit, http://timarit.is/view_page_init.jsp?pubId=160&lang=en  (accessed 18 Aug., 2012).</ref>

By August 1, 1980, the newspaper was almost all in the English language, with Icelandic articles interspersed.<ref>Lögberg-Heimskringla {{ISSN|0047-4967}} 1 August 1980. Tímarit, http://timarit.is/view_page_init.jsp?pubId=160&lang=en  (accessed 18 August. 2012).</ref>  By August 31, 1990, the only Icelandic language articles were on the last page of the publication.<ref>Lögberg-Heimskringla {{ISSN|0047-4967}} 31 August 1990, 8. Tímarit, http://timarit.is/view_page_init.jsp?pubId=160&lang=en  (accessed 18 August 2012).</ref>  Ten years after that, in August 2000, there were no longer any Icelandic language articles in Lögberg-Heimskringla.<ref>Lögberg-Heimskringla {{ISSN|0047-4967}} 18 August 2000.  Tímarit http://timarit.is/view_page_init.jsp?pubId=160&lang=en  (accessed 18 August 2012).</ref>   This remains the case up to the present day, and is largely a reflection of the families of the original immigrants from Iceland having become predominantly English-speaking.

== Modern times ==

The newspaper depends on subscriptions, advertisements, paid notices, donations, and government grants from both Iceland and Canada.

From time-to-time, Icelandic lessons have been included in the paper by various editors.

Subscribers live in many countries, but are mostly located in Canada, the United States of America, and Iceland. For approximately one year in 2000 the newspaper had an office in [[Gimli, Manitoba]] with the production still being done in [[Winnipeg, Manitoba|Winnipeg]].  In 2001 the office was moved back to Winnipeg, where it remains today.

In January, 2004 the newspaper began publishing every two weeks rather than on a weekly basis, and with a 16-page format.<ref>Lögberg-Heimskringla {{ISSN|0047-4967}} 16 January 2004.  Tímarit http://timarit.is/view_page_init.jsp?pubId=160&lang=en  (accessed 18 August 2012).</ref> In October, 2011 the paper celebrated its 125th birthday – the anniversary of the first edition of Heimskringla. This publication is the oldest continually publishing newspaper in Canada.<ref>{{cite journal|last=Ervin|first=Linda|author2=Ruth Bogusis |title=Collections of Ethnic Canadiana at the National Library of Canada: the Ethnic Serials Project|journal=Serials Librarian|date=Summer 1977|volume=1|issue=4|pages=334|issn=0361-526X}}</ref>

The newspaper is now available in an online subscription as well as in the regular newspaper format. In print, the paper is currently published on the 1st and 15th day of each month.

== See also ==
*  [[Canada-Iceland relations]]
*  [[Iceland-United States relations]]
*  [[Icelandic American]]
*  [[Icelandic Canadian]]
*  [[List of newspapers in Canada]]

== References ==
{{reflist}}

=== Bibliography  ===
*Ervin, Linda and Ruth Bogusis.  "Collections of Ethnic Canadiana at the National Library of Canada: the Ethnic Serials Project" Serials Librarian, {{ISSN|0361-526X}} Summer, 1977, 331-336.  
*Gislason, David.  “Why Lögberg and Why Heimskringla?” Lögberg-Heimskringla, {{ISSN|0047-4967}} October 15, 2011, Special Anniversary Feature: 1.
*Kristinsson, Június H.  Vesturfaraskrá 1870-1914: A Record of Immigrants from Iceland to America 1870-1914.  Reykjavík, Iceland: Institute of History, University of Iceland, 1983.
*Kristjanson, Wilhelm.  The Icelandic People in Manitoba: a Manitoba Saga. ISBN 978-0-9694651-1-9 Winnipeg: Wallingford Press, 1965.
*Lindal, Walter J.  The Icelanders in Canada.  Ottawa and Winnipeg: National Publishers and Viking Printers, 1967.
*Sturlusson, Snorri.  Heimskringla or the Chronicle of the Kings of Norway. (C.1230), trans. Douglas B. Killings. Online Medieval and Classical Library.  accessed August 2, 2012.  http://omacl.org/Heimskringla/
*Styles, Deborah A. “Societal Influences on Children’s Psychology: The Schools in Iceland and Singapore Promote Prosocial Values, Positive Self-Concepts, and Achievement in Young Adolescents.”  Paper presented at the Oxford Round Table, London, England, 2005. 
*Thor, Jonas.  “The Amalgamation of Heimskringla and Lögberg.” Lögberg-Heimskringla, {{ISSN|0047-4967}} September 12, 1986: 5 and 7.  
*Thorlakson, Dr. Ken.  “Why Lögberg-Heimskringla?” Lögberg-Heimskringla, {{ISSN|0047-4967}} October 15, 2011.  Special Anniversary Feature: 1.
*Thorlakson, Dr. P. H. T.  “An Adventure in Co-operation.”  Lögberg-Heimskringla August 20, 1959: 1. accessed August 18, 2012, http://timarit.is/view_page_init.jsp?pubId=160&lang=en  
*Tímarit.  Heimskringla, September 9, 1886.  Accessed August 2, 2012. http://timarit.is/view_page_init.jsp?pubId=129&lang=en
*Tímarit.  Lögberg, January 15, 1890.  Accessed August 3, 2012.  http://timarit.is/view_page_init.jsp?pubId=132&lang=en
*Tímarit.  Lögberg-Heimskringla, {{ISSN|0047-4967}} August 1, 1980.  Accessed August 18, 2012. http://timarit.is/view_page_init.jsp?pubId=160&lang=en	
*Tímarit.  Lögberg-Heimskringla, {{ISSN|0047-4967}} August 31, 1990.  Accessed August 18, 2012. http://timarit.is/view_page_init.jsp?pubId=160&lang=en	
*Tímarit.  Lögberg-Heimskringla, {{ISSN|0047-4967}} August 18, 2000.  Accessed August 18, 2012. http://timarit.is/view_page_init.jsp?pubId=160&lang=en
*Tímarit.  Lögberg-Heimskringla, {{ISSN|0047-4967}} January 16, 2004.  Accessed August 18, 2012. http://timarit.is/view_page_init.jsp?pubId=160&lang=en

==External links==
* {{official website|http://www.lh-inc.ca}}

{{DEFAULTSORT:Logberg-Heimskringla}}
[[Category:Newspapers published in Winnipeg]]
[[Category:Publications established in 1959]]
[[Category:1959 establishments in Manitoba]]
[[Category:Icelandic-language newspapers]]