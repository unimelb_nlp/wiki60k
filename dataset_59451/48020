{{technical|date=January 2014}}

The '''Function-Behaviour-Structure ontology''' – or short, the FBS ontology – is an [[ontology (information science)|ontology]] of design objects, i.e. things that have been or can be [[design]]ed. The Function-Behaviour-Structure ontology conceptualizes design objects in three ontological categories: function (F), behaviour (B), and structure (S). The FBS ontology has been used in [[design science]] as a basis for modelling the process of designing as a set of distinct activities. This article relates to the concepts and models proposed by John S. Gero and his collaborators. Similar ideas have been developed independently by other researchers.<ref>Umeda et al. (1990)</ref><ref>Chandrasekaran and Josephson (2000)</ref><ref>Bhatta and Goel (1994)</ref>

== Overview ==
The ontological categories composing the Function-Behaviour-Structure ontology are defined as follows:<ref name="GeroKannengiesser">Gero and Kannengiesser (2004)</ref><ref name="GeroKannengiesser_b">Gero and Kannengiesser (2014)</ref>
* ''Function'' (F): the [[teleology]] (purpose) of the design object. For example, the functions of a [[turbocharger]] include increasing the power output of an engine, providing reliability, and providing affordability.
* ''Behaviour'' (B): the attributes that can be derived from the design object’s structure. For example, the behaviour of a turbocharger includes attributes such as [[Airflow|air mass flow]], efficiency ratio, thermal strength, and weight.
* ''Structure'' (S): the components of the design object and their relationships. In the turbocharger example, structure includes the turbocharger components ([[Centrifugal compressor|compressor]], [[Radial turbine|turbine]], shaft, etc.) and their spatial dimensions, interconnections and materials.
The three ontological categories are interconnected: Function is connected with behaviour, and behaviour is connected with structure. There is no connection between function and structure.

== Ontological Models of Designing ==
The Function-Behaviour-Structure ontology is the basis for two frameworks of designing: the FBS framework, and its extension, the situated FBS framework. They represent the process of designing as transformations between function, behaviour and structure, and subclasses thereof.

=== The Function-Behaviour-Structure Framework ===
The original version of the FBS framework was published by John S. Gero in 1990.<ref>Gero (1990)</ref> It applies the FBS ontology to the process of designing, by further articulating the three ontological categories. In this articulation, behaviour (B) is specialised into ''expected behaviour'' (Be) (the "desired" behaviour) and ''behaviour derived from structure'' (Bs) (the "actual" behaviour). In addition, two further notions are introduced on top of the existing ontological categories: ''requirements'' (R) that represent intentions from the client that come from outside the designer, and ''description'' (D) that represents a depiction of the design created by the designer. Based on these articulations, the FBS framework proposes eight processes claimed as fundamental in designing,<ref name="GeroKannengiesser" /><ref name="GeroKannengiesser_a">Gero and Kannengiesser (2002)</ref> specifically:

[[File:The Function-Behaviour-Structure Framework.png|thumb|alt=Image showing the Function-Behaviour-Structure Framework.|The Function-Behaviour-Structure Framework]]

# '''Formulation''': formulates the problem space, by transforming requirements into a function [[state space]] (R → F), and transforming functions into a behaviour state space (F → Be).
# '''Synthesis''': generates structure based on expectations of the behaviour state space (Be → S).
# '''Analysis''': derives behaviour from the generated structure (S → Bs).
# '''Evaluation''': compares expected behaviour with the behaviour derived from structure (Be ↔ Bs).
# '''Documentation''': produces descriptions of the design based on structure (S → D).
# '''Reformulation type 1''': modifies the structure state space, based on a re-interpretation of structure (S → S’).
# '''Reformulation type 2''': modifies the behaviour state space, based on a re-interpretation of structure (S → Be’).
# '''Reformulation type 3''': modifies the function state space, based on a re-interpretation of structure and subsequent reformulation of expected behaviour (S → F’ via Be).

==== Example ====
The eight fundamental processes in the FBS framework are illustrated using a turbocharger design process.

# Formulation: External requirements (R) for a turbocharger are interpreted by the designer as functions (F) including to increase the power output of an engine. A set of behaviours (Be) is then produced that are expected to achieve this function. They include the air mass flow and efficiency ratios for a range of engine speeds.
# Synthesis: Based on the expected behaviours (Be), a structure (S) is produced that includes components such as a compressor, a turbine, a core assembly, a shaft, and their interconnections. It also includes their geometry and materials.
# Analysis: After the structure (S) is produced, the "actual" behaviours (Bs) can be derived based on that structure. This may include the physical testing of prototypes (e.g. for measuring air mass flow), and computational simulations (e.g. for calculating thermal behaviours).
# Evaluation: The "actual" behaviours (Bs) of the turbocharger are compared against the expected behaviours (Be), to assess whether the current turbocharger design performs as required.
# Documentation: The turbocharger design is documented by generating a description (D), commonly a [[Computer-aided design|CAD model]], based on the structure (S).
# Reformulation type 1: The designer modifies the space of possible design structures (S) by including a new component such as a variable sliding ring inside the turbine.
# Reformulation type 2: The designer modifies the space of expected behaviours (Be) by introducing a new control behaviour that allows varying the air mass flow. This is a consequence of introducing the variable sliding ring into the design structure (S).
# Reformulation type 3: The designer modifies the function space (F) by adapting it to serve the needs of an engine with increased exhaust temperature. This is based on the discovery of a high thermal strength (Be) of existing design materials (S).

=== The Situated Function-Behaviour-Structure Framework ===
The situated FBS framework was developed by John S. Gero and Udo Kannengiesser in 2000<ref name="GeroKannengiesser_a" /> as an extension of the FBS framework to explicitly capture the role of [[situated cognition]] or situatedness in designing.<ref name="Schon">Schön (1983)</ref><ref>Clancey (1997)</ref>

==== Situatedness ====
The basic assumption underpinning the situated FBS framework is that designing involves interactions between three worlds: the external world, the interpreted world and the expected world. They are defined as follows:<ref name="GeroKannengiesser" /><ref name="GeroKannengiesser_b" /><ref name="GeroKannengiesser_a" />
* ''External world'': contains things in the “outside” world (for example, in the physical environment of the designer)
* ''Interpreted world'': contains experiences, percepts and concepts, formed by the designer’s interactions with the external world
* ''Expected world'': contains expectations of the results of the designer’s actions, driven by goals and hypotheses about the current state of the world

The three worlds are interconnected by four classes of interaction:
* ''Interpretation'': transforms variables sensed in the external world into variables within the interpreted world
* ''Focussing'': selects subsets of variables in the interpreted world and uses them as goals in the expected world
* ''Action'': changes the external world according to the goals and hypotheses composing the expected world
* ''Constructive memory'': produces memories as a result of re-interpreting past experiences. It is based on a constructivist model of human memory.<ref>Dewey (1896)</ref> in which new memories are generated by reflection<ref name="Schon" />

==== Situatedness and FBS Combined ====
The situated FBS framework is a result of merging the three-world model of situatedness with the original FBS framework, by specialising the ontological categories as follows:<ref name="GeroKannengiesser" /><ref name="GeroKannengiesser_b" /><ref name="GeroKannengiesser_a" />

{|
|-
| valign="top" |
* Fe<sup>i</sup>: expected function
* F<sup>i</sup>: interpreted function
* F<sup>e</sup>: external function
* FR<sup>e</sup>: external requirements on function
| valign="top" |
* Be<sup>i</sup>: expected behaviour
* B<sup>i</sup>: interpreted behaviour
* B<sup>e</sup>: external behaviour
* BR<sup>e</sup>: external requirements on behaviour
| valign="top" |
* Se<sup>i</sup>: expected structure
* S<sup>i</sup>: interpreted structure
* S<sup>e</sup>: external structure
* SR<sup>e</sup>: external requirements on structure
|}

[[File:The Situated Function-Behaviour-Structure Framework.png|thumb|alt=Image showing the situated Function-Behaviour-Structure Framework.|The Situated Function-Behaviour-Structure Framework]]

20 processes connect these specialised ontological categories. They elaborate and extend the eight fundamental processes in the FBS framework, providing more descriptive power with regards to the situatedness of designing.

# '''Formulation''': generates a design state space in terms of a function state space (process 7 in the image showing the situated Function-Behaviour-Structure Framework), a behaviour state space (processes 8 and 10), and a structure state space (process 9). It is based on the interpretation of external requirements on function (process 1), behaviour (process 2), and structure (process 3), and on the construction of memories of function (process 4), behaviour (process 5), and structure (process 6).
# '''Synthesis''': produces a design solution that is a point in the structure state space (process 11) and an external representation of that solution (process 12).
# '''Analysis''': interprets the synthesised structure (process 13) and derives behaviour from that structure (process 14).
# '''Evaluation''': compares the expected behaviour with the interpreted behaviour (process 15).
# '''Documentation''': produces an external representation of the design, which may be in terms of structure (process 12), behaviour (process 17) and function (process 18).
# '''Reformulation type 1''': generates a new or modified structure state space (process 9). Potential drivers of this reformulation include processes 3, 6 and 13.
# '''Reformulation type 2''': generates a new or modified behaviour state space (process 8). Potential drivers of this reformulation include processes 2, 5, 14 and 19.
# '''Reformulation type 3''': generates a new or modified function state space (process 7). Potential drivers of this reformulation include processes 1, 4, 16 and 20.

== Applications ==
The FBS ontology has been used as a basis for modelling designs (the results of designing) and design processes (the activities of designing) in a number of design disciplines, including engineering design, architecture, construction and software design.<ref>Deng (2002)</ref><ref>Christophe et al. (2010)</ref><ref>Clayton et al. (1999)</ref><ref>Kruchten (2005)</ref><ref>Howard et al. (2008)</ref><ref>Yan (1993)</ref><ref>Colombo et al. (2007)</ref> While the FBS ontology has been discussed in terms of its completeness,<ref>Galle (2009)</ref><ref>Dorst and Vermaas (2005)</ref><ref>Vermaas and Dorst (2007)</ref><ref>Ralph (2010)</ref> several research groups have extended it to fit the needs of their specific domains.<ref>Cascini et al. (2013)</ref><ref>Uflacker and Zeier (2008)</ref><ref>Cebrian-Tarrason et al. (2008)</ref><ref>Gu et al. (2012)</ref><ref>Eichhoff and Maass (2011)</ref><ref>Russo et al. (2012)</ref>
It has also been used as a schema for coding and [[Protocol analysis| analysing behavioural studies]] of designers.<ref>Jiang (2012)</ref><ref>Kan (2008)</ref><ref>Kan and Gero (2009)</ref><ref>McNeill (1998)</ref><ref>Lammi (2011)</ref>

== Notes ==
{{reflist|3}}

== References ==
* Bhatta S.R. and Goel A.K. (1994) "Model-based discovery of physical principles from design experiences", ''Artificial Intelligence for Engineering Design, Analysis and Manufacturing'', '''8'''(2), pp.&nbsp;113–123.
* Cascini G., Fantoni G. and Montagna F. (2013) "Situating needs and requirements in the FBS framework", ''Design Studies'', '''34'''(5), pp.&nbsp;636–662.
* Cebrian-Tarrason D., Lopez-Montero J.A. and Vidal R. (2008) "OntoFaBeS: Ontology design based in FBS framework", ''CIRP Design Conference 2008'', University of Twente.
* Chandrasekaran B. and Josephson J.R. (2000) "Function in device representation", ''Engineering with Computers'', '''16'''(3-4), pp.&nbsp;162–177.
* Christophe F., Bernard A. and Coatanéa É. (2010) "RFBS: A model for knowledge representation of conceptual design", ''CIRP Annals - Manufacturing Technology'', '''59'''(1), pp.&nbsp;155–158.
* Clancey, W.J. (1997) ''Situated Cognition: On Human Knowledge and Computer Representations'', Cambridge University Press, Cambridge. ISBN 0-521-44871-9.
* Clayton M.J., Teicholz P., Fischer M. and Kunz J. (1999) "Virtual components consisting of form, function and behavior", ''Automation in Construction'', '''8'''(3), pp.&nbsp;351–367.
* Colombo G., Mosca A. and Sartori F. (2007) "Towards the design of intelligent CAD systems: An ontological approach", ''Advanced Engineering Informatics'', '''21'''(2), pp.&nbsp;153–168.
* Eichhoff J.R. and Maass W. (2011) "Representation and Reuse of Design Knowledge: An Application for Sales Call Support", ''Knowledge-Based and Intelligent Information and Engineering Systems'', LNCS 6881, Springer, pp.&nbsp;387–396.
* Deng Y.M. (2002) "Function and behavior representation in conceptual mechanical design", ''Artificial Intelligence for Engineering Design, Analysis and Manufacturing'', '''16'''(5), pp.&nbsp;343–362.
* Dewey J. (1896 reprinted in 1981) "The reflex arc concept in psychology", ''Psychological Review'', '''3''', pp.&nbsp;357–370.
* Dorst K. and Vermaas P.E. (2005) "John Gero’s Function-Behaviour-Structure model of designing: a critical analysis", ''Research in Engineering Design'', '''16'''(1-2), pp.&nbsp;17–26.
* Galle P. (2009) "The ontology of Gero's FBS model of designing", ''Design Studies'', '''30'''(4), pp.&nbsp;321–339.
* Gero J.S. (1990) "Design prototypes: a knowledge representation schema for design", ''AI Magazine'', '''11'''(4), pp.&nbsp;26–36.
* Gero J.S. and Kannengiesser U. (2002) "The situated function-behaviour-structure framework", ''Artificial Intelligence in Design '02'', Kluwer Academic Publishers, Dordrecht, pp.&nbsp;89–104.
* Gero J.S. and Kannengiesser U. (2004) "The situated function-behaviour-structure framework", ''Design Studies'', '''25'''(4), pp.&nbsp;373–391.
* Gero J.S. and Kannengiesser U. (2014) "The function-behaviour-structure ontology of design", in A. Chakrabarti and L.T.M. Blessing (eds) ''An Anthology of Theories and Models of Design'', Springer, pp.&nbsp;263–283.
* Gu C.-C., Hu J., Peng Y.-H. and Li S. (2012) "FCBS model for functional knowledge representation in conceptual design", ''Journal of Engineering Design'', '''23'''(8), pp.&nbsp;577–596.
* Howard T.J., Culley S.J. and Dekoninck E. (2008) "Describing the creative design process by the integration of engineering design and cognitive psychology literature", ''Design Studies'', '''29'''(2), pp.&nbsp;160–180.
* Jiang H. (2012) "Understanding Senior Design Students' Product Conceptual Design Activities", PhD Thesis, National University of Singapore, Singapore.
* Kan J.W.T. (2008) "Quantitative Methods for Studying Design Protocols", PhD Thesis, The University of Sydney, Sydney.
* Kan J.W.T. and Gero J.S. (2009) "Using the FBS ontology to capture semantic design information in design protocol studies", in J. McDonnell and P. Lloyd (eds) ''About: Designing.  Analysing Design Meetings'', CRC Press, pp.&nbsp;213–229.
* Kruchten P. (2005) "Casting software design in the function-behavior-structure framework", ''IEEE Software'', '''22'''(2), pp.&nbsp;52–58.
* Lammi M.D. (2011) "Characterizing high school students' systems thinking in engineering design through the function-behavior-structure (FBS) framework", Doctoral Dissertation, Utah State University, Logan.
* McNeill T. (1998) "The Anatomy of Conceptual Electronic Design", Doctoral Dissertation, University of South Australia, Adelaide.
* Ralph P. (2010) "Comparing Two Software Design Process Theories", ''Global Perspectives on Design Science Research'', LNCS 6105, Springer, pp.&nbsp;139–153.
* Russo D., Montecchi T. and Ying L. (2012) "Functional-based search for patent technology transfer", ''Proceedings of the ASME 2012 International Design Technical Conferences & Computers and Information in Engineering Conference IDETC/CIE 2012'', August 12–15, 2012, Chicago, IL, DETC2012-70833.
* Schön D.A. (1983) ''The Reflective Practitioner: How Professionals Think in Action'', Harper Collins, New York. ISBN 0-465-06874-X.
* Uflacker M. and Zeier A. (2008) "Extending the Situated Function-Behaviour-Structure Framework for User-Centered Software Design", ''Design Computing and Cognition '08'', Springer, pp.&nbsp;241–259.
* Umeda Y., Takeda H., Tomiyama T. and Yoshikawa H. (1990) "Function, behaviour, and structure", ''Applications of Artificial Intelligence in Engineering V'', Vol. 1, pp.&nbsp;177–194.
* Vermaas P.E. and Dorst K. (2007) "On the conceptual framework of John Gero's FBS-model and the prescriptive aims of design methodology", ''Design Studies'', '''28'''(2), pp.&nbsp;133–157.
* Yan M. (1993) "Representing design knowledge as a network of function, behaviour and structure", ''Design Studies'', '''14'''(3), pp.&nbsp;314–329.

[[Category:Design]]
[[Category:Ontology (information science)]]