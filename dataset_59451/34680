{{Use dmy dates|date=May 2011}}
{{good article}}
{{Infobox military unit
|unit_name=95th Infantry Division
|image=[[File:95TrainingDivSSI SVG.svg|150px]]
|caption=95th Infantry Division shoulder sleeve insignia
|dates=1918–45<br />1947–present
|country={{flag|United States}}
|allegiance=
|branch={{army|USA}}
|type=[[Infantry]]
|role=
|size=[[Division (military)|Division]]
|command_structure=
|garrison=[[Fort Sill]], Oklahoma 
|garrison_label=
|equipment=
|equipment_label=
|nickname= "Iron Men of Metz" ([[special designation]])<ref name="CMH Designations"/> or "Victory Division"<ref>''Order of Battle'', p. 351.</ref>
|patron=
|motto=
|colors=
|colors_label=
|march=
|mascot=
|battles=[[World War II]]
* [[Operation Overlord|Northern France]]
* [[Allied advance from Paris to the Rhine|Rhineland]]
* [[Western Allied invasion of Germany|Central Europe]]
|anniversaries=
|decorations=
|battle_honours=
<!-- Commanders -->
|current_commander=[[Brigadier General]] Daniel Christian<ref name="95th(IET)">{{cite web | title=95th Training Division (Initial Entry Training) |  url=http://www.usar.army.mil/arweb/organization/commandstructure/usarc/tng/108tng/commands/95th/leadership/Pages/default.aspx | year=2011 | accessdate=14 June 2011}}</ref> 
|current_commander_label= 
|ceremonial_chief=
|ceremonial_chief_label=
|colonel_of_the_regiment= 
|colonel_of_the_regiment_label=
|command_sergeant_major=[[Command Sergeant Major]] CSM Hill <ref name="95th(IET)"/>  
|command_sergeant_major_label=
|notable_commanders=
<!-- Insignia -->
|identification_symbol=[[File:95TrainingDivDUI.jpg|100px]]
|identification_symbol_label=Distinctive unit insignia
|identification_symbol_2=
|identification_symbol_2_label=
}}

The '''95th Infantry Division''' was an [[infantry]] [[division (military)|division]] of the [[United States Army]]. Today it exists as the '''95th Training Division''', a component of the [[United States Army Reserve]] headquartered  at [[Fort Sill, Oklahoma|Fort Sill]], Oklahoma.

Activated too late to deploy for [[World War I]], the division remained in the Army's reserve until [[World War II]], when it was sent to Europe. Renowned for fighting back fierce German counterattacks, the division earned the nickname "Iron Men of [[Metz]]" for fighting to liberate and defend the town. After World War II, the division spent another brief period in reserve before being activated as one of the Army's training divisions.

Over the next fifty years the division would see numerous changes to its structure as its training roles changed and subordinate units shifted in and out of its command. It activated a large number of regimental and brigade commands to fulfill various training roles. The division then began conducting [[one station unit training]], a responsibility it continues to this day.

== History ==

=== World War I ===
The 95th Division was first constituted on 4 September 1918 in the national army. It was organized that month at [[Camp Sherman, Ohio|Camp Sherman]], Ohio.<ref name="Lineage">{{cite web|url=http://www.history.army.mil/html/forcestruc/lineages/branches/div/095d.htm |title=Lineage and Honors Information: 95th Infantry Division |publisher=The [[United States Army Center of Military History]] |accessdate=1 July 2009}}</ref> The division was organized with the [[189th Infantry Brigade]] and the 190th Infantry Brigade of the [[Oklahoma]] organized reserve.<ref>McGrath, p. 174.</ref> The division was slated to be deployed overseas to fight in [[World War I]]. Training of all of the division's units began immediately.<ref name="GSO">{{cite web|url=http://www.globalsecurity.org/military/agency/army/95d.htm |title=GlobalSecurity.org: 95th Infantry Division |work=[[GlobalSecurity]] |accessdate=1 July 2009| archiveurl= https://web.archive.org/web/20090716154804/http://www.globalsecurity.org/military/agency/army/95d.htm| archivedate= 16 July 2009 <!--DASHBot-->| deadurl= no}}</ref> On 11 November, the [[Armistice with Germany (Compiègne)|Armistice with Germany]] was signed, ending the hostilities. The division's deployment was cancelled and it was demobilized in December 1918.<ref name="Lineage"/> All of the division's officers and enlisted men were discharged from the military or transferred to other units.<ref name="GSO"/> On 24 June 1921, the division was reactivated in the [[organized reserves]].<ref name="Lineage"/> From that point until 1942, the division remained as a reserve unit based in [[Oklahoma City]].<ref name="GSO"/>

===World War II===
On 15 July 1942, the division was ordered into active military service and reorganized at [[Camp Swift, Texas|Camp Swift]], Texas.<ref name="Lineage"/> The 189th and 190th Infantry Brigades were disbanded as part of an army-wide elimination of infantry brigades. Instead, the division was based around three infantry regiments, the 377th Infantry Regiment, the 378th Infantry Regiment, and the 379th Infantry Regiment.<ref name="Almanac592">''Almanac'', p. 592.</ref> Also assigned to the division were the 358th, 359th, 360th, and [[920th Field Artillery Battalion (United States)|920th Field Artillery Battalion]]s, the 95th Signal Company, the 795th Ordnance Company, the 95th Quartermaster Company, the 95th Reconnaissance Troop, the 320th Engineer Battalion, the 320th Medical Battalion, and the 95th Counter-Intelligence Detachment.<ref name="Almanac592"/> [[Major General]] Harry L. Twaddle took command of the division, a command he held for its entire duration in World War II,<ref>''Order of Battle'', p. 352.</ref> making him one of only eleven generals to do so.<ref name="Order374">''Order of Battle'', p. 374.</ref> The division also received a [[Shoulder Sleeve Insignia]] this year.<ref name="TIOH">{{cite web|url=http://www.tioh.hqda.pentagon.mil/Heraldry/ArmyDUISSICOA/ArmyHeraldryUnit.aspx?u=3319 |title=The Institute of Heraldry: 95th Infantry Division |publisher=[[The Institute of Heraldry]] |accessdate=28 February 2011}}</ref> Over the next two years, the division trained extensively in locations throughout the United States.<ref name="GSO"/>

==== Europe ====
[[File:EnteringMetz.jpg|thumb|American soldiers of the [[378th Infantry Regiment (United States)|378th Infantry Regiment]] enter Metz, 1944.]]

The 95th Infantry Division was assigned to [[XIII Corps (United States)|XIII Corps]] of the [[Ninth United States Army]], [[Twelfth United States Army Group]].<ref name="Order356">''Order of Battle'', p. 356.</ref> The division sailed for Europe on 10 August 1944.<ref name="Almanac565">''Almanac'', p. 565.</ref> The 95th Infantry Division arrived in England on 17 August. After receiving additional training, it moved to France one month later on 15 September. During this time it was reassigned to [[III Corps (United States)|III Corps]].<ref name="Order356"/> The division [[military camp|bivouac]]ked near [[Norroy-le-Sec]], from 1 to 14 October.<ref name="Almanac565"/> It was then assigned to [[XX Corps (United States)|XX Corps]] of the [[United States Army Central|Third United States Army]].<ref name="Order356"/> The division was sent into combat on 19 October in the [[Moselle (river)|Moselle]] [[bridgehead]] sector east of [[Moselle]] and South of [[Metz]] and patrolled the [[Seille (Moselle)|Seille]] near [[Cheminot]], capturing the forts surrounding [[Metz]] and repulsing enemy attempts to cross the river.<ref name="Almanac565"/> It was during the [[Battle of Metz|defense of this town from repeated German attacks]] that the division received its nickname, "The Iron Men of Metz."<ref name="CMH Designations">{{cite web | title=Regular Army / Army Reserve Special Designation Listing | url=http://www.history.army.mil/html/forcestruc/spdes-123-ra_ar.html | publisher=United States Army Center of Military History | year=2009 | accessdate=27 April 2009| archiveurl= https://web.archive.org/web/20090512061407/http://www.history.army.mil/html/forcestruc/spdes-123-ra_ar.html| archivedate= 12 May 2009 <!--DASHBot-->| deadurl= no}}</ref> On 1 November, elements went over to the offensive, reducing an enemy pocket east of [[Maizières-lès-Metz]]. On the 8 November, these units crossed the Moselle River and advanced to Bertrange. Against heavy resistance, the 95th captured the forts surrounding Metz and captured the city by 22 November.<ref name="Almanac565"/>

The division pushed toward the [[Saar (river)|Saar]] on 25 November and entered Germany on the 28th. The 95th seized a [[Saar River]] bridge on 3 December and engaged in bitter [[house-to-house fighting]] for [[Saarlautern]].<ref name="Almanac565"/> Suburbs of the city fell and, although the enemy resisted fiercely, the Saar bridgehead was firmly established by 19 December. While some units went to an assembly area, others held the area against strong German attacks.<ref name="Almanac565"/> On 2 February 1945, the division began moving to the [[Maastricht]] area in the [[Netherlands]], and by 14 February, elements were in the line near Meerselo in relief of [[British Army|British]] units.<ref name="Almanac565"/> During this time the division returned to the Ninth Army under [[XIX Corps (United States)|XIX Corps]], though saw temporary assignments to several other corps through the spring.<ref name="Order356"/>

On 23 February, the division was relieved, and the 95th assembled near [[Jülich]], Germany, on 1 March. It forced the enemy into a pocket near the Hitler Bridge at [[Uerdingen]] and cleared the pocket on 5 March, while elements advanced to the [[Rhine]].<ref name="Almanac565"/> From 12 March, the 95th established defenses in the vicinity of [[Neuss]]. Assembling east of the Rhine at [[Beckum, Germany|Beckum]] on 3 April, it launched an attack across the [[Lippe River]] the next day and captured [[Hamm]] and [[Kamen]] on the 6th.<ref name="Almanac565"/> After clearing the enemy pocket north of the [[Ruhr (river)|Ruhr]] and the [[Möhne]] Rivers,  the division took [[Werl]] and [[Unna]] on 9/10 April, [[Dortmund]] on 13 April and maintained positions on the north bank of the Ruhr.<ref name="Almanac565"/> It held this position until the end of the war.

====Casualties====

*'''Total battle casualties:''' 6,591<ref name="Nonbattle Deaths 1953">Army Battle Casualties and Nonbattle Deaths, Final Report (Statistical and Accounting Branch, Office of the Adjutant General, 1 June 1953)</ref>
*'''Killed in action:''' 1,205<ref name="Nonbattle Deaths 1953"/>
*'''Wounded in action:''' 4,945<ref name="Nonbattle Deaths 1953"/>
*'''Missing in action:''' 61<ref name="Nonbattle Deaths 1953"/>
*'''Prisoner of war:''' 380<ref name="Nonbattle Deaths 1953"/>

==== Demobilization ====
The division returned to the United States on 29 June 1945 where it began the process of preparing to join the invasion forces of the Japanese Island of Honshu as part of the First United States Army.  With the ending of the war in Japan, the division, remaining on orders for the Pacific, staged a minor mutiny before the orders were changed.  This resulted in the division being demobilized and releasing its soldiers from Army service. It was inactivated on 15 October 1945 at [[Camp Shelby]], [[Mississippi]].<ref name="Almanac565"/> The division took 31,988 German prisoners.<ref name="Order353">''Order of Battle'', p. 353.</ref> Soldiers of the division were awarded one [[Medal of Honor]], 18 [[Distinguished Service Cross (United States)|Distinguished Service Cross]]es, 1 British Military Cross, 14 [[Legion of Merit]] Medals, 665 [[Silver Star Medal]]s, 15 [[Soldier's Medal]]s, 2,992 [[Bronze Star Medal]]s, and 162 [[Air Medal]]s. The division was awarded one [[Presidential Unit Citation (United States)|Presidential Unit Citation]] and four [[campaign streamers]] during its time in combat.<ref name="Order353"/>

===Cold War===
The division was reactivated on 13 May 1947 at Oklahoma City as a reserve unit. However, it was not mobilized for any combat duties following World War II.<ref name="Lineage"/> In 1952, the division underwent reorganization, with the first change being the addition of the 291st Infantry Regiment of [[Tulsa, Oklahoma|Tulsa]], Oklahoma, from the [[75th Infantry Division (United States)|75th Infantry Division]].<ref name="GSO"/> The second change that year for the division was the withdrawal of assignment of the 377th Infantry Regiment from the 95th, which was transferred to the 75th Infantry Division. The 377th had headquartered in [[New Orleans, Louisiana|New Orleans]], Louisiana since its activation after World War II.<ref name="GSO"/>

The year 1955 saw further changes to the division and more changes of assignment for subordinate elements. On 1 January 1955, the 291st Regiment was again assigned to the 75th Infantry Division from the 95th and was subsequently inactivated on 31 January 1955.<ref name="GSO"/> On 30 January, the 377th Regiment was reassigned to the 95th from the 75th and its headquarters moved from New Orleans to Tulsa, Oklahoma. The same date saw the relocation of the 379th Regimental headquarters from [[Hot Springs, Arkansas|Hot Springs]], Arkansas where it had been since 1947, to [[Little Rock, Arkansas|Little Rock]], Arkansas.<ref name="GSO"/>

[[File:Army Bayonet Training.jpg|thumb|US Army recruits in [[Basic Combat Training]].]]
On 1 April 1958 the 95th Infantry Division was redesignated as the 95th Division (Training) and a major reorganization of mission assignments was underway. Personnel trained for [[infantry]] combat, [[field artillery]], [[military police]] and [[combat support]] roles were now to undergo re-training to enable them to train others.<ref name="GSO"/> The division had a new role as one of the 13 Training Divisions in the Army Reserve. The same year the division's size increased as the 291st Regiment was reassigned again from the 75th and was redesignated as 291st Regiment (Advanced Individual Training). With the reorganization of the division all of the regiments were redesignated. The 95th Divisional Artillery became the 95th Regiment (Common Specialist Training) with headquarters at [[Shreveport, Louisiana|Shreveport]], Louisiana. The 377th became the 377th Regiment (Basic Combat Training), as did the 378th and 379th, and all were reassigned new training sites.<ref name="GSO"/> In 1966, the division received a [[distinctive unit insignia]].<ref name="TIOH"/>

In 1967, the division was reorganized according to the [[Reorganization Objective Army Divisions]] plan, part of an army-wide transformation. The division's former World War II components were reorganized into brigades.<ref>McGrath, p. 159.</ref> The division's former headquarters was reactivated as 1st Brigade, 95th Division at Tulsa, Oklahoma. The 920th Field Artillery Battalion became the 2nd Brigade, 95th Division, also in Tulsa.<ref name="McGrath221">McGrath, p. 221.</ref> The 320th Engineer Battalion became the 3rd Brigade, 95th Division at Oklahoma City, and the 795th Ordnance Battalion became the 4th Brigade, 95th Division in Shreveport, Louisiana.<ref name="McGrath221"/> In 1975, the division's center was changed to [[Midwest City, Oklahoma|Midwest City]], Oklahoma.<ref name="Lineage"/>

The division was located in three states, Oklahoma, Arkansas, and Louisiana. The 1st Brigade was headquartered in Tulsa, Oklahoma and had elements of the 377th and 379th in regiments in its battalions. The 2nd Brigade was headquartered in [[Lawton, Oklahoma|Lawton]], Oklahoma with elements of the 378th and 379th Regiments.<ref name="GSO"/> The 3rd Brigade was headquartered in [[Stillwater, Oklahoma|Stillwater]], Oklahoma, a move made in September 1975, and consisted of only 291st Regiment elements. The 4th Brigade was headquartered in [[Bossier City, Louisiana|Bossier City]], Louisiana, a suburb of [[Shreveport]], and includes the 95th Regiment and one element of the 379th.<ref name="GSO"/> The Committee Group was headquartered in Little Rock, Arkansas with no Regimental elements. The 95th Support Battalion was headquartered in [[Midwest City, Oklahoma|Midwest City]], Oklahoma with the Division Headquarters, Headquarters and Headquarters Company, 95th Division Leadership Academy, and the 95th Division Maneuver Training Command.<ref name="GSO"/> On 1 January 1979 the division's four brigades was reorganized specifically for [[one station unit training]].<ref name="GSO"/>

The division experienced tremendous expansion in October 1984 with the addition of the 4073d US Army Reception Station, in Lafayette, Louisiana, with a strength of 809 personnel.<ref name="GSO"/> The 402nd Brigade was also activated under the division's administrative control. It was designated to expand the training base for the Army's Field Artillery Training Center located at [[Fort Sill]], Oklahoma.<ref name="GSO"/> In 1989 the division's location was returned to Oklahoma City.<ref name="Lineage"/>

===Present day===
The division continued its mission of training and operating one station unit training. In 1996, the division received three additional brigades as part of an Army consolidation of training commands. The 5th Brigade, 95th Division was activated in [[San Antonio, Texas|San Antonio]], Texas, the 6th Brigade, 95th Division was activated in [[Topeka, Kansas|Topeka]], Kansas, and the 7th Brigade, 95th Division was activated from the 95th Training Command in Little Rock, Arkansas.<ref name="McGrath222">McGrath, p. 222.</ref>

In 2000, the brigade took on the additional responsibility of training [[Reserve Officer's Training Corps]] cadets. The 8th Brigade, 95th Division was activated as a provisional unit in charge of ROTC units throughout the southwestern United States.<ref name="McGrath222"/> In 2005, the division headquarters were relocated to [[Fort Sill]], Oklahoma. This put the division at the area's major training center, allowing it to more effectively provide training oversight.<ref name="Lineage"/>

==Honors==

===Unit decorations===
The division has never received a unit award from the United States Army.<ref name="Lineage"/>
However, in a recent ceremony in Columbus, Indiana a bridge was named "Iron Men of Metz Memorial Bridge"<ref>{{cite web|author=mhansel@therepublic.com |url=http://www.therepublic.com/view/local_story/Celebration-of-Iron-Men-stretc_1403315686 |title=Celebration of ‘Iron Men’ stretches far beyond city |publisher=Therepublic.com |date=2014-06-20 |accessdate=2015-12-24}}</ref>

===Campaign streamers===
{| class="wikitable" style="float:left;"
|- style="background:#efefef;"
! Conflict
! Streamer
! Year(s)
|-
|| World War II
|| [[Northern France]]
|| 1944
|-
|| World War II
|| [[Rhineland]]
|| 1944–1945
|-
|| World War II
|| [[Ardennes-Alsace]]
|| 1944–1945
|-
|| World War II
|| Central Europe
|| 1945
|}
{{Clear}}

== Legacy ==
In the 1962 film, ''[[Hell Is for Heroes (film)|Hell Is for Heroes]]'', the actors wear the 95th Division's shoulder patch on their uniforms. The division is also an element of the [[Legacy of the Aldenata]] book series.

A number of soldiers who served with the 95th Division later went on to achieve notability for various reasons. They include journalist [[Harry Ashmore]],<ref>{{cite journal|first=Martin |last=Ochs |url=http://www.vqronline.org/articles/1995/spring/ochs-search-racial-justice/ |title=Search for Racial Justice |journal=[[The Virginia Quarterly Review]] |volume=71 |year=1995 |issue=2 |issn=0042-675X |accessdate=11 July 2006 |deadurl=yes |archiveurl=https://web.archive.org/web/20061004213518/http://www.vqronline.org/articles/1995/spring/ochs-search-racial-justice/ |archivedate=4 October 2006 |df=dmy }}</ref> writer [[Jerry Rosholt]], oil [[tycoon]] [[Ernest L. Massad]], and [[Lieutenant General]] [[Emmett H. Walker, Jr.]]. Additionally, one soldier of the division received the [[Medal of Honor]] during his service, [[Andrew Miller (soldier)|Andrew Miller]], who received the medal in World War II during the division's fight for Metz. Miller captured a number of German machine gun nests and soldiers while leading a squad of men in assaulting the city.<ref>{{cite web| url=http://www.history.army.mil/html/moh/vietnam-a-l.html | publisher=United States Army |title=Medal of Honor Recipients&nbsp;— Vietnam (A-L)|accessdate=24 April 2008| archiveurl= https://web.archive.org/web/20080414030126/http://www.history.army.mil/html/moh/vietnam-a-l.html| archivedate= 14 April 2008 <!--DASHBot-->| deadurl= no}}</ref><ref>{{cite web| url=http://www.history.army.mil/html/moh/vietnam-m-z.html | publisher=United States Army |title=Medal of Honor Recipients&nbsp;— Vietnam (M-Z)|accessdate=24 April 2008| archiveurl= https://web.archive.org/web/20080424094845/http://www.history.army.mil/html/moh/vietnam-m-z.html| archivedate= 24 April 2008 <!--DASHBot-->| deadurl= no}}</ref>
German born (MG) Gerd Grombacher served as an NCO interrogating POWs and was commissioned 2LT Grombacher in January 1945. He directly assisted in the negotiations for the capture of Metz in 1944.

== References ==
{{reflist}}

== Sources ==
* {{cite book|first=John J. |last=McGrath |title= The Brigade: A History: Its Organization and Employment in the US Army |year=2004 |publisher=Combat Studies Institute Press |isbn=978-1-4404-4915-4}}
* {{cite book |title= Army Almanac: A Book of Facts Concerning the Army of the United States |year=1959 |publisher=United States Government Printing Office |asin=B0006D8NKK}}
* {{cite book |title= Order of Battle of the United States Army: World War II European Theater of Operations |year=1945 |publisher=Department of the Army |isbn=978-0-16-001967-8}}

== External links ==
* [http://www.lonesentry.com/gi_stories_booklets/95thinfantry/index.html Bravest of the Brave: The Story of the 95th Infantry Division], Information and Education Division, Special and Information Services ETOUSA, 1945.
* [http://www.ironmenofmetz.new.fr Virtual Museum of the 95th Infantry Division], original pictures, full information about the division, stories of veterans, pictures "Then & Now".
* [http://www.95divassociation.com/ Official site of the 95th Infantry Division Association]

{{Army Divisions (United States)}}

{{DEFAULTSORT:045}}
[[Category:Infantry divisions of the United States Army|095th Infantry Division, U.S.]]
[[Category:United States Army divisions during World War II|Infantry Division, U.S. 095th]]
[[Category:Military units and formations established in 1942]]
[[Category:1918 establishments in the United States]]