{{Use dmy dates|date=May 2013}}
{{good article}}
{{Infobox museum
| name             = Beit Al Qur'an
| native_name      = بيت القرآن
| native_name_lang = ara
| image            = G310s bqan 2009 t.jpg
| imagesize        = 200
| caption          = 
| alt              = 
| map_type         = 
| map_caption      = 
| map_alt          = 
| map_size         = 
| latitude         = 
| longitude        = 
| coordinates_type = 
| coordinates_region = 
| coordinates_display = 
| coordinates_format = 
| coordinates      = 
| established      = 1990
| dissolved        = 
| location         = [[Hoora]], [[Manama]], [[Bahrain]]
| type             = [[Islam]]ic museum
| collection       = 10,000 books and manuscripts
| visitors         = 
| director         = Samar al Gailani<ref>{{cite web|title=People in the field of Islamic manuscripts.|url=http://www.islamicmanuscript.org/PeopleByName.aspx|publisher=The Islamic Manuscript Association|accessdate=5 October 2012}}</ref> 
| president        = 
| curator          = 
| owner            =
| publictransit    = 
| car_park         = 
| network          = 
| website          = [http://www.moc.gov.bh/en/top10/Name,7534,en.html Official website]
}}

'''Beit Al Qur'an''' ({{lang-ar|بيت القرآن}}, meaning: the '''House of Qur'an''') is a multi-purpose complex dedicated to the [[Islamic art]]s and is located in [[Hoora]], [[Bahrain]].<ref>{{cite book|title=The Middle East, Volume 1|year=2004|publisher=Greenwood Publishing Group|isbn=9780313329234|pages=6|url=https://books.google.com/?id=4-xaVGUWVYQC&pg=PA6}}</ref> Established in 1990, the complex is most famous for its Islamic [[museum]], which has been acknowledged as being one of the most renowned Islamic museums in the world.<ref name="Aramco">{{cite journal|last=Nawwab|first=Ni'Mah Isma'il|title=Beit Al Qur’an  Religion, Art, Scholarship|journal=Saudi Aramco World|date=May–June 2000|volume= 51|issue=3|url=http://www.saudiaramcoworld.com/issue/200003/beit.al.qur.an-religion.art.scholarship.htm|accessdate=5 October 2012}}</ref>

==Establishment==
[[File:Qur'anic Manuscript - Kufic script.jpg|thumb|280px|An early [[Kufic]] manuscript of the [[Qur'an]] developed in the late 7th century, present at the museum]]
[[File:Early Kufic script - Qur'anic Manuscript.jpg|thumb|280px|A Qur'anic manuscript on parchment containing verses 94, 95 and 96, part of verse 97 from [[Al-Ma'ida|Surah al-Ma'idah]], present at the museum.]]
Construction of the complex began in 1984 and the museum was officially opened in March 1990 by [[Abdul Latif Jassim Kanoo]]. It was built to "accommodate a comprehensive and valuable collection of the [[Qur'an]] and other rare manuscripts", a concept which, according to a regional magazine, is unique in the [[Persian Gulf]] region.<ref>{{cite web|title=Beit Al Qur'an|url=http://www.timeoutbahrain.com/community/features/10362-beit-al-quran|publisher=TimeOut Bahrain|accessdate=5 October 2012|date=August 2009}}</ref> 
The core of the museum's holdings is Kanoo's own collection of Qur'anic manuscripts and Islamic art, since he was reportedly said to have been an avid collector. As his collection grew, he reportedly came to feel a strong sense of responsibility toward the rare manuscripts he had acquired. In 1990, he donated his collection to the museum he established to operate a first-of-its-kind institution dedicated to the service of the Qur'an and the preservation of historic manuscripts.<ref name=Aramco/>

The establishment of the institute was funded completely by public donations, with added help from a variety of people from all walks of life in Bahrain, ranging from heads of state to school children. The facilities at Beit Al Qur'an are free to the general public.<ref>{{cite web|title=Beit al-Quran in Manama, Bahrain|url=http://www.lonelyplanet.com/bahrain/manama/sights/museum/beit-al-quran|publisher=Lonely Planet|accessdate=5 October 2012}}</ref>

The institution and its museum house an internationally celebrated collection of historic Quranic manuscripts from various parts of the Islamic world, from China in the East and to Spain in the West, representing a progression of [[Calligraphy|calligraphic]] traditions from the first [[Hijri]] century (622–722 AD) and of the [[Islamic Golden Age]], to the present day.<ref>{{cite web|last=Muqueem Khan|first=Shaista|title=Islamic Manuscripts|url=http://ddms.usim.edu.my/bitstream/handle/123456789/1923/ISLAMIC%20MANUSCRIPTS.pdf?sequence=1|publisher=World Congress of Muslim Librarian & Information Scientists|accessdate=17 October 2012|pages=10, 11|date=November 2008}}</ref><ref name=BNA>{{cite web|title=بيت القرآن يستأنف فعالياته غداً السبت الاول من سبتمبر|url=http://www.bna.bh/portal/news/522645|publisher=Bahrain News Agency|accessdate=4 November 2012}}</ref>

==Facilities==
{{Quran}}
The Beit al Qur'an complex is open to the public on Saturdays to Wednesdays from 9am to 12pm and 4pm to 6pm respectively.<ref>{{cite web|title=Bait al Quran |url=http://www.moc.gov.bh/en/top10/Name,7534,en.html |archive-url=https://archive.is/20121130040805/http://www.moc.gov.bh/en/top10/Name,7534,en.html |dead-url=yes |archive-date=30 November 2012 |publisher=Ministry of Culture (Bahrain) |accessdate=25 June 2013 }}</ref>  The complex's exterior designs are based on an old fashioned 12th-century mosque.<ref name=BNA/> The entire complex itself comprises a [[mosque]], a [[library]], an [[auditorium]], a [[madrasa]], and a [[museum]] that consists of ten exhibition halls. A large stained glass dome covers the grand hall and mosque. The [[Mihrab]], the sign indicating the direction to Mecca, is covered in blue ceramic tiles with engraved Al Qursi Qur'anic verse.<ref name="TO Bahrain"/>

The library consists of over 50,000 books and [[manuscripts]] in three languages – [[Arabic]], English and French – that are mostly on Islam. The institute does specialise in [[Islamic art]], and many of the [[reference books]] have international importance. The library and its reading rooms are open to the public during working hours with internet access available, as well as providing individual rooms for researchers and specialists.<ref name=BNA/>

There is also an auditorium – named the Mohammed Bin Khalifa Bin Salman Al Khalifa Lecture Hall – which can accommodate up to 150 people, and is mainly used for [[lectures]] and [[Academic conference|conferences]]. Guest speakers are brought to Bahrain from many countries, including the US, UK, and France. The conference hall is often made available for general use for public lectures in cooperation with different societies and institutions in Bahrain.<ref name="TO Bahrain"/>

The Yousuf Bin Ahmad Kanoo School for [[Islamic Studies|Qur'anic Studies]] is located within the site. The school offers seven study areas fully equipped with computers and modern aids, with separate classes for women and children learning the Qur'an.<ref name="TO Bahrain">{{cite web|title=Beit Al Qur'an in Bahrain|url=http://www.timeoutbahrain.com/art/features/17229-beit-al-quran-in-bahrain|publisher=TimeOut Bahrain|accessdate=5 October 2012}}</ref>

==Museum==
The '''Al Hayat Museum''' is the complex's most recognized establishments; it consists of ten halls spread over two floors, exhibiting rare Qur'anic manuscripts from different periods, starting from the first century [[Hijri year|Hijra]] (700&nbsp;AD). Manuscripts on parchments that originate from [[Saudi Arabia]] ([[Mecca]] and [[Medina]]), [[Damascus]] and [[Baghdad]], are present in the museum.<ref name=BNA/> The manuscripts undergo special procedures for the preservation of these artifacts, in order to protect them from damages. Some of the artifacts present in the museum include a rare manuscript of the Qur'an, dating to 1694&nbsp;AD and was printed in Germany. The museum also houses the world's oldest translated copy of the Qur'an, which was translated to [[Latin]] in [[Switzerland]] and dates to 955&nbsp;AD.<ref name="BNA" /> The first copy of the Qur'an, written during the reign of [[Caliph]] [[Uthman ibn Affan]], is on display in the museum alongside a number of small copies of the Qur'an, which could only be read using optical instruments.<ref name="BNA" />

Grains, peas and rice, dating from the 14th century in present-day [[Pakistan]], which contain surahs engraved into them, are displayed in the museum.<ref name=BNA/> The exhibits include a rare number of gold and copper [[pottery]] and glass from different eras of Iraq, Turkey, Iran and Egypt, respectively.<ref name=BNA/>

The works of Islamic scholars, such as [[Ibn Taymiyyah]] are preserved in the museum. It has been claimed to have been "''the only institute in the world dedicated to the Qur'an and Qur'anic studies''".<ref>{{cite web| title=Arabic Textbook | url=http://yalepress.yale.edu/yupbooks/languages/pdf/Frangieh_prefaceintro.pdf | publisher=[[Yale University Press]] | accessdate=17 October 2012}}</ref>

== See also ==
* [[Khamis Mosque]]
* [[List of tourist attractions in Bahrain]]
* [[Islam in Bahrain]]

== References ==
{{reflist}}

{{coord|26|14|23|N|50|35|30|E|region:FR_type:city_source:kolossus-frwiki|display=title}}

[[Category:Library buildings completed in 1990]]
[[Category:Museums established in 1990]]
[[Category:Islamic culture]]
[[Category:Museums in Bahrain]]
[[Category:Islamic museums]]
[[Category:Libraries in Bahrain]]
[[Category:Mosques in Manama]]
[[Category:Buildings and structures in Manama]]
[[Category:Tourist attractions in Manama]]
[[Category:Quran]]