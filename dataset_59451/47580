{{Use mdy dates|date=December 2011}}
{{Infobox song <!-- See Wikipedia:WikiProject_songs -->
| Name= Double Helix
| Cover = DH portfolio cover.jpg
| Alt = 
| Caption =Cover for score and parts
| Type = Composition
| Artist = 1991 [[SCSBOA]] Honor Jazz Ensemble
| Released = 1995 
| Format = [[Music download|Digital download]]
| Recorded = 1993
| Studio = [[Omega Recording Studios|Omega Recording]], Rockville, Maryland
| Composer = [[Jack Cooper (musician)|Jack Cooper]]
| Genre = [[Jazz]], [[blues]]
| Misc =
<!--sound file not in Category:Wikipedia non-free audio samples
{{Extra music sample 
| Type = Song
| filename = Doub Hel 20 second OGG file.ogg
| title = "Double Helix" 20-second sample}}-->
}}
"'''Double Helix'''" is an original music composition written for 17-piece [[jazz]] orchestra in 1991.<ref>[http://www.reallygoodmusic.com/rgm.jsp?page=itemDetail&iid=123206 ReallyGoodMusic.com print music listing]</ref>  The recording of "Double Helix" also goes by the title "Twice is Nice" (as a sound file)<ref name="FIRST">[http://www.firstcom.com/#/en/search-results.aspx?searchtype=quick&predictsearch=true&keyword=twice+is+nice&tracks_title=Track+Title Sound file for ''Twice Is Nice'']</ref> when distributed worldwide as production music. The work has been used and heard around the world for music, media, and entertainment broadcasts.

==Background==
''Double Helix'' was first written in 1991 as a commission for the [[Southern California School Band and Orchestra Association|SCSBOA]] Honor Jazz Ensemble and premiered at [[Disneyland]] that year.<ref>[http://linahonmusic.com/ James Linahon] called Cooper in 1991 to commission a new work for the [http://www.scsboa.org/ SCSBOA] jazz group, Linahon would conduct them later that year.</ref> Late in 1991 Jack Bullock (jazz editor for [[Alfred Music Publishing|Columbia Pictures Publications/Belwin]], (aka [[Alfred Music Publishing|CPP Belwin]] and currently [[Alfred Music Publishing|Belwin Jazz, a Division of Alred Music Publishing]]) hired [[Jack Cooper (musician)|Jack Cooper]] as a staff writer for new jazz ensemble works.<ref>[http://www.alfred.com/ Alfred Music Publishing] biograghical page for [http://www.alfred.com/Company/Authors/JackCooper.aspx Jack Cooper]</ref> Jack Bullock heard his writing done for the [[The Jazz Knights|U.S. Army ''Jazz Knights'']].<ref>CPP/Belwin composer [http://www.alfred.com/Company/Authors/CarlStrommen.aspx Carl Strommen] forwarded Cooper's demo tape to [http://www.alfred.com/Company/Authors/JackBullock.aspx Jack Bullock] after hearing the [[The Jazz Knights|U.S.M.A. ''Jazz Knights'']] perform while Cooper was with the group during that time.</ref> The first piece Bullock decided to publish (and record) was ''Double Helix''.<ref>Review: BD Guide, November/December 1993, Village Press, Inc.  J. Richard Dunscomb (reviewer), rated: ''the very best'', page 36</ref>

The work was recorded at [[Omega Recording Studios]] in [[Rockville, Maryland]] for the 1993 [[Alfred Music Publishing|CPP/Belwin]] Jazz catalogue of new published works.<ref>1993 CPP/Belwin New Music Jazz Ensemble Catalogue, music for credits and opening track for demo, ''Double Helix''</ref> Later In 1994, Canadian beverage giant [[Seagram]] bought a part of [[Time Warner]] and the [[Warner/Chappell Music|Warner Music Publishing division]] of that company acquired [[Alfred Music Publishing|CPP/Belwin]]. ''Double Helix'' was now distributed by [[Warner Brothers Music]] publications and the company made the move to allow [[FirstCom]]<ref name=FIRST /> and [[Universal Music Publishing Group|Universal Publishing Production Music]] to have access to a limited number of [[Alfred Music Publishing|CPP/Belwin]] sound files. The recordings from [[Omega Recording Studios|Omega Studios]] for [[Alfred Music Publishing|CPP/Belwin]] were of very high quality as well as the music itself that was being written and acquired for the catalogue.

While the original [[Alfred Music Publishing|CPP/Belwin]] sound file of ''Double Helix'' continues to be widely used in the media, the rights to the print music were acquired back by the composer in 2006. The print music division of [[Time Warner]] was [[Spin out|sold]] to [[Alfred Music Publishing]] in June 2005 which made a great deal of the older [[Alfred Music Publishing|CPP/Belwin]] print music catalogue obsolete. At that time it was best to move ''Double Helix'' (the print music) to a company who would keep the work in distribution for a much longer period; the print music is now distributed and sold by Really Good Music Publishing.

The music and sound file of ''Double Helix'' has been and is still frequently utilized for media production in [[television]] and [[radio]].<ref name=DANISH /><ref>''Double Helix'' is in repeated syndicated T.V. broadcasts around the world</ref> The [[ASCAP]] distributions have far exceeded the original royalty payments made through sale of the print music by [[Alfred Music Publishing|CPP/Belwin]].

==Composition==
The work itself is 170 measures and based on a [[Blues#Form|Blues form]] in the key of ''f minor''. The smaller 12 measure sub-form repeats numerous times; the overall work mimicks a hybrid [[Sonata form]] with an [[Sonata form#Exposition|exposition/statement]], [[Musical development|development]] (with improvisation section), and [[Recapitulation (music)|recap]] (or [[Repetition (music)|restatement]]).

===The form and layout of ''Double Helix''===
{| class="wikitable"
| style="width:300px; text-align:center;"|'''Double Helix'''
|-
|align=center|
{| cellspacing="1" class="wikitable"
|- style="text-align:center;"
| style="width:50px;"| Measures<br />1-12 
| style="width:50px;"| 13-48 
| style="width:50px;"| 49-64
| style="width:50px;"| 77-88
| style="width:50px;"| 89-112
| style="width:50px;"| 113-124
| style="width:50px;"| 125-144
| style="width:50px;"| 145-156
| style="width:50px;"| 157-170
|- style="text-align:center;"
| style="width:50px;"| Introduction
| style="width:50px;"| statement<br />melody/song
| style="width:50px;"| interlude<br />''send off''
| style="width:50px;"| open improvisation/<br />background figures
| style="width:50px;"| saxophone section<br />soli
| style="width:50px;"| ''stop time''<br />[[A cappella|Acappella]]<br />brass
| style="width:50px;"| development
| style="width:50px;"| recap of<br />melody/song
| style="width:50px;"| coda/Finé
|}
|}

==Reviews==
{{Album ratings
| rev1 = Band Directors Guide<br/>(BD Guide)
| rev1Score = (*)The Very Best<ref name="BDGUIDE">Dunscomb, J. Richard, Review, ''Double Helix'', BD Guide. Nov./Dec. 1993. Vol. 8, No. 2, pp.36</ref> 
}}
{{quote
|"Aptly titled, this original work spirals its way into the world of contemporary big band swing music.  The voicing selections are superb.  It comes off sounding much more difficult than it sctually is...The driving into the head (section) set(s) up the soloist, who emerges with a roar from the full ensemble scoring...The shout chorus is memorable and, like the rest of the chart, it contains terrific dynamic contrasts."
|Dick Dunscomb ''BD Guide'' (review, 1993)<ref name=BDGUIDE/>
}}

==Use in media==
"Double Helix" has been used as feature, interlude, and background music in the United Kingdom,<ref name="DEALNODEAL">''Deal or No Deal'' (TV Series), Episode 49067/011 [[United Kingdom|U.K.]], [[PRS for Music|PRS]]/[http://www.ascap.com/ ASCAP] April–August 2013 distribution</ref> the Czech Republic,<ref>[[Performance rights organisation|OSA]]. credits as per [http://www.ascap.com/ ASCAP] distribution, Aug. 2011</ref> the Netherlands,<ref name="DANISH">[http://www.dr.dk/ DR (broadcaster)|Danish Radio P2, TROS], used often as library music, [http://www.bumastemra.nl/ BUMA] through current [http://www.ascap.com/ ASCAP] distribution, confirmed May 25, 2010</ref> Ireland,<ref>[http://www.imro.ie/ IMRO] credits as per [http://www.ascap.com/ ASCAP] distribution, Feb. 2005</ref> Canada,<ref>[http://www.socan.ca/ SOCAN] credits as per [http://www.ascap.com/ ASCAP] distribution, Feb. 2001</ref> Romania,<ref>[http://www.ucmr-ada.ro/ UCMR-ADA] credits as per [http://www.ascap.com/ ASCAP] distribution, Feb. 2009</ref> France,<ref>[http://www.sacem.fr/cms/site/en/home?pop=1 SACEM], credits as per [http://www.ascap.com/ ASCAP] distribution</ref> Norway,<ref name="VEGAS">''50 Hottest Vegas Moments'' (2005), January 17, 2005 (USA) first aired, confirmed through [http://www.ascap.com/ ASCAP] July Distribution, 2006 - episode aired 14 times during that time period</ref><ref>[http://www.teosto.fi/teosto/websivut.nsf Teosto] [http://www.tono.no/ TONO] credits as per [http://www.ascap.com/ ASCAP] distribution, May 2007</ref> Finland,<ref name=VEGAS /> Hong Kong,<ref>[http://www.cash.org.hk/en/home.do CASH] credits as per [http://www.ascap.com/ ASCAP] distribution</ref> and Japan.<ref>Under the title ''Twice is Nice'', ''Double Helix'' [http://www.jasrac.or.jp/ejhp/index.htm JASRAC] ''UNCUT (ETV) - Meg Ryan,'' first aired May 7, 1996</ref> 

Television shows using the work include ''[[The Jenny Jones Show]]'',<ref>''[http://www.tv.com/shows/jenny-jones/ The Jenny Jones Show]'' (1995). Episodes ''Double Helix'' is used on include "Celebrity Look-A-Likes" and "Second Annual Jenny Jones Awards", [http://www.ascap.com/ ASCAP] Jan. 1999 distribution</ref> ''F.Y.E.'', ''[[E! Entertainment]]'' shows (2),<ref name=VEGAS /><ref>''[http://tv.nytimes.com/show/160981/Vegas-Showgirls-Nearly-Famous/overview ''Vegas Showgirls: Nearly Famous]'' (TV Series 2001-2003), [http://www.ascap.com/ ASCAP] distribution records April and July 2002</ref> ''[[Access Hollywood]]'',<ref>''[http://www.tv.com/shows/access-hollywood/ Access Hollywood]'' (TV series) Episode dated May 7, 2001, #0435, confirmed through [http://www.ascap.com/ ASCAP] Jan. distribution 2002</ref> ''[[Johnny Bravo|JBVO: Your All Request Cartoon Show]]'',<ref>''Johnny Bravo'' (TV Series), Episodes #14 and #24 from ''JBVO: Your All Request Cartoon Show'', [http://www.ascap.com/ ASCAP] April–September 2001 distribution</ref> ''[[American Restoration]]'',<ref>''American Restoration'' (TV/Cable series), Episode ''Surfing The Strip'', [http://www.ascap.com/ ASCAP] April–August 2013 distribution</ref> ''[[Deal or No Deal (UK game show)|Deal or No Deal (UK)]]'',<ref name=DEALNODEAL/> and ''[[Extra (TV series)|Extra]]''.

==References==
{{reflist}}

==External links==
* {{OCLC|668441746}}
* [http://iswcnet.cisac.org/iswcnet/MWI/result/detail.do?workDetails=0 ISWC T-070.045.419-8]
* [http://www.reallygoodmusic.com/index.jsp?page=itemDetail&iid=123206 '''Double Helix''', Really Good Music Publishing]
* [http://www.unippmglobal.com/# '''Universal Publishing Production Music''']
* [http://www.firstcom.com/# '''FirstCom Production Music''']
{{portal bar|music|jazz}}
{{Jack Cooper (musician)}}
{{DEFAULTSORT:Double Helix (music composition)}}
[[Category:Television soundtracks]]
[[Category:Jazz compositions]]
[[Category:Television music]]
[[Category:Swing music]]
[[Category:Blues]]