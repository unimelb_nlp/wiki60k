{{redirect|Aachen Airport|the minor airport serving Aachen|Merzbrück Airport}}
{{Infobox airport
| name         = Maastricht Aachen Airport
| nativename   = 
| image        = Maastricht Aachen Airport logo.jpg
| image-width  = 250
| image2       = MaastrichtAachenAirportTerminal.JPG
| image2-width = 250
| IATA         = MST
| ICAO         = EHBK
| pushpin_map            = Netherlands
| pushpin_map_caption    = Location of airport in Netherlands
| pushpin_label          = MST
| pushpin_label_position = top
| type         = Public
| owner        =
| operator     = Maastricht Aachen Airport BV
| city-served  = [[Maastricht]], Netherlands<br />[[Aachen]], Germany
| location     = [[Beek]], Netherlands
| elevation-f  = 375
| elevation-m  = 114
| coordinates  = {{Coord|50|54|57|N|005|46|37|E|region:NL_type:airport}}
| website      = [http://www.maa.nl/en/home/ www.maa.nl]
| metric-rwy   = y
| r1-number    = 03/21
| r1-length-m  = {{formatnum:2750}}
| r1-length-f  = {{formatnum:9022}}
| r1-surface   = [[Asphalt]]
| stat-year    = 2011
| stat1-header = Passengers
| stat1-data   = 363,000 {{increase}}
| stat2-header = Cargo ([[tonne]]s)
| stat2-data   = 92,500 {{increase}}
| footnotes    = Source: [[Aeronautical Information Publication|AIP]] from ''[[Aeronautical Information Service|AIS]], Netherlands'',<ref name="AIP">{{AIP_NL|EHBK|name=MAASTRICHT/Maastricht Aachen}}</ref> Airport Facts & Figures<ref>{{cite web|url=http://www.maa.nl/en-us/about/figures|title=Maastricht Aachen Airport - Pagina niet gevonden|work=maa.nl|accessdate=20 August 2016}}</ref>
}}

'''Maastricht Aachen Airport''' {{airport codes|MST|EHBK}} is a regional airport in [[Beek]], [[Netherlands]], located {{convert|5|NM|abbr=on|lk=in}} northeast of [[Maastricht]]<ref name="AIP"/> and {{convert|15|NM|abbr=on}} northwest of [[Aachen]], [[Germany]].<ref>[http://gc.kls2.com/cgi-bin/gc?PATH=50%B046%27N+006%B006%27E-50%B054%2757%22N+005%B046%2737%22E%0D%0A%0D%0A&RANGE=&PATH-COLOR=red&PATH-UNITS=nm&PATH-MINIMUM=&SPEED-GROUND=&SPEED-UNITS=kts&RANGE-STYLE=best&RANGE-COLOR=navy&MAP-STYLE= Distance and heading from Aachen (50°46'N 006°06'E) to EHBK (50°54'57"N 005°46'37"E)].</ref> It is the second-largest [[airline hub|hub]] for cargo flights in the Netherlands. As of 2011, the airport had a passenger throughput of 360,000 and handled 92,500 [[ton]]s of cargo.<ref>{{cite web |url=http://www.maa.nl/nl/news.asp?ii=124 |title= MAA website, Press release |accessdate=2011-05-27}}</ref>

The Maastricht Upper Area Control Centre (MUAC) of the [[European Organisation for the Safety of Air Navigation]] (EUROCONTROL) is also located at the airport.

==History==

===Pre-World War II===
Plans for an airport in southern [[Limburg (Netherlands)|Limburg]] date back as far as 1919, with various locations being considered. Years of debate between various municipalities over the location and funding of the airport delayed its construction. In July 1939 the Limburg provincial government agreed to financially back the airport, however, the start of World War II meant the plans were put on hold once more.<ref>Vleugels 1987, p. 12-13</ref>

===Advanced Landing Ground Y-44===
[[File:IXEngineering-psprunway.jpg|right|thumb|IX Engineering Command constructing an advanced landing ground]]
After the allied [[invasion of Normandy]], the [[United States Army Air Forces|USAAF]] [[Ninth Air Force]], specifically the IX Engineer Command, was tasked with constructing temporary airfields close to the advancing [[Front (military)|front]]. The area around Maastricht was liberated in 1944. In October 1944, the advance headquarters of the XIX Tactical Air Command and the 84th and 303rd Fighter Wings were moved to [[Maastricht]] to keep up with the Ninth Army.<ref>{{cite web| title= USAAF Chronology October 1944 | author= McKillop, Jack | url= http://www.usaaf.net/chron/44/oct44.htm | accessdate=2010-02-03}}</ref>

Because of the proximity to the new headquarters, the decision was made to create a temporary airfield between the towns of [[Beek]], [[Geulle]] and [[Ulestraten]]. Several [[orchard]]s which had suffered damage from a tank battle were commandeered and cleared. Rubble from the nearby town of [[Geleen]], which had been unintentionally bombed in 1942, was used to level the area.<ref name="Vleugels 1987, p. 14">Vleugels 1987, p. 14</ref>

The runway was {{convert|5565|ft|m}} <ref>{{cite web| title= IX Engineer Command | url=http://www.ixengineercommand.com/airfields/physical.php | author= Little, David | accessdate=2010-02-03}}</ref> and reinforced with [[Advanced Landing Ground#Runway types|pierced steel planks]].

The field was built in less than 2 months and was operational on 22 March 1945,<ref>Vleugels 1987, p. 15</ref> and was designated Y-44.

The first unit to be based at the field was the 31st Tactical Reconnaissance Squadron, flying the F-6, a reconnaissance version of the [[P-51 Mustang]]. The unit arrived on 22 March 1945.<ref name="Vleugels 1987, p. 14"/><ref name="USAAF3-45">{{cite web| title= USAAAF Chronology March 1945 | author= McKillop, Jack
| url= http://www.usaaf.net/chron/45/mar45.htm | accessdate=2010-02-03}}</ref>

As [[Nazi Germany]] was rapidly collapsing, the front was already well into Germany by the time the field was ready, and no direct combat sorties were operated from Y-44. 31st TRS was moved to [[Wiesbaden Army Airfield|Y-80 near Wiesbaden]] on 19 April 1945.<ref name="Vleugels 1987, p. 14"/><ref name="USAAF4-45">{{cite web| title= USAAF Chronology April 1945 | author= McKillop, Jack | url= http://www.usaaf.net/chron/45/apr45.htm | accessdate=2010-02-03}}</ref>

====Units operating at the field====
*31st Tactical Reconnaissance Squadron, [[P-51 Mustang|F-6 Mustang]] (22 March 1945<ref name="USAAF3-45"/> – 19 April 1945<ref name="USAAF4-45"/> )
*39th Photographic Reconnaissance Squadron, [[F-5 Lightning]] (2 April 1945<ref name="USAAF4-45"/> – 20 April 1945<ref name="USAAF4-45"/>)
*[[67th Network Warfare Group|67th Tactical Reconnaissance Group]]
**155th Photographic Reconnaissance Squadron (Night), [[A-20 Havoc|F-3]] (4 April 1945<ref name="USAAF4-45"/> – 10 July 1945<ref>{{cite web| title= USAAF Chronology July 1945 | author= McKillop, Jack | url= http://www.usaaf.net/chron/45/jul45.htm | accessdate=2010-02-03}}</ref><ref>{{cite web | title= 45 Reconnaissance Factsheet | author= Air Force Historical Research Agency |url=http://www.afhra.af.mil/factsheets/factsheet.asp?id=10248 | accessdate=2010-02-03}}</ref>)
*[[387th Air Expeditionary Operations Group|387th Bombardment Group (Medium)]]
**556, 557, 558 and 559th Squadrons, [[B-26 Marauder]] (4 May 1945)<ref name="USAAF5-45"/> – 30 May 1945<ref name="USAAF5-45">{{cite web| title= USAAF Chronology May 1945 | author= McKillop, Jack | url= http://www.usaaf.net/chron/45/may45.htm | accessdate=2010-02-03}}</ref>

===After World War II===
Authority over what was to become known as Beek airfield (''vliegveld Beek''), was officially transferred to the Dutch government on 1 August 1945. It was decided to keep it open rather than re-open the pre-war debate over the location of an airport in the Maastricht area. The first civilian aircraft landed on 26 September 1945 and were operated by the ''Regeeringsvliegdienst'', a government service with the purpose of carrying government officials and other people with urgent business, because the war had left many roads and railroads heavily damaged. The service used six [[de Havilland Dragon Rapide]]s made available by the British government.<ref>Vleugels 1987, p. 18</ref>

In 1946, the service was taken over by [[KLM]], using [[DC-3 Dakota]]s.<ref>Vleugels 1987, p. 22</ref> However, as repairs to the Dutch infrastructure progressed, demand for the service dropped and it was stopped in 1949.<ref name="Vleugels 1987, p. 28">Vleugels 1987, p. 28</ref> The first semi-permanent [[airport terminal]] was completed in 1947.<ref name="Vleugels 1987, p. 28"/> The runway was paved in 1949, and a second paved runway was completed in 1950.<ref name="Vleugels 1987, p. 28"/> In 1951, an agreement between the airport and the [[Dutch Air Force]] allowed for rapid expansion of the facilities.<ref>Vleugels 1987, p. 32</ref> Runway 04/22 was lengthened to 1,850 m, and permanent runway lighting was installed in 1960.

===1950s and 1960s===
The late 1950s and early 1960s brought significant expansion in commercial operations at the airport. Operators included [[KLM]], [[Airnautical]], [[Skytours]], [[Euravia]], [[Tradair]] and [[Transair (UK)|Transair]].<ref>Vleugels 1987, p. 36</ref> The airport was also used as an intermediate stop for services from London and Manchester to Switzerland, Austria, Italy and Yugoslavia.<ref>Vleugels 1987, p. 43</ref> A local airline based at the airport, ''Limburg Airways'', had a contract with the [[International Herald Tribune]] for distributing the newspaper's European edition, which was printed in Paris. Limburg Airways was taken over by Martin's Air Charter (now [[Martinair]]) in 1962.<ref>Vleugels 1987, p. 45</ref>

A promotion campaign by the Dutch tourist board for the nearby town of [[Valkenburg aan de Geul]], aimed at British tourists, was highly successful and brought services by [[Invicta International Airlines|Invicta Airlines]], [[Britannia Airways|Britannia]] and [[Channel Airways]].

Domestic travel picked up as well, and newly created [[NLM CityHopper]] started to operate a service between Maastricht and [[Amsterdam Airport Schiphol|Amsterdam Schiphol]] in 1966. The service would continue after KLM acquired NLM in 1992, and would last until 2008. When it was cancelled, it was the last remaining domestic service in the Netherlands.<ref>{{cite web| author= [[Het Nieuwsblad]] | url= http://www.nieuwsblad.be/article/detail.aspx?articleid=2S1VMKM5 | title= KLM vliegt niet meer naar Amsterdam | accessdate= 2010-02-06}}</ref>

An [[Instrument Landing System|ILS system]], which allows landings in poor weather, was built in 1967, for runway 22 only.

===1970s===
In 1973 the airport was expanded again to handle bigger aircraft. The main runway was lengthened to 2500m, taxiways were widened and aprons enlarged.<ref>Vleugels 1987, p. 60</ref> This mostly offset the negative effects of the [[1973 oil crisis]], passenger volume remained the same and cargo operations expanded.<ref>Vleugels 1987, p. 61</ref>

The international air traffic control [[Area Control Center|area control centre]] for [[EUROCONTROL]] was built at the airport. It started operations on March 1, 1972.<ref>{{cite web| title= Eurocontrol MUAC / About Us |url= http://www.eurocontrol.int/muac/public/standard_page/AboutUs.html | author= Eurocontrol | accessdate=2010-02-06}}</ref>

===1980s===
Around 1980 the airport changed its name to "Maastricht Airport". In 1983, the aging passenger terminal and air traffic control tower were replaced by new buildings <ref>Vleugels 1987, p. 85</ref> The new terminal was later expanded and is still in use as of 2010.

On 14 May 1985, [[Pope John Paul II]] held an open-air mass for 50,000 people at the airport, as part of his visit to the Netherlands.<ref>{{cite web |title= Toen en nu 14 Mei 1985 | author= De Limburger | url= http://www.limburger.nl/article/20090514/TOENENNU/369854960/1018 | accessdate= 2010-02-08}}</ref>

====Plan for an east–west runway====
In 1981, a development plan for the airport recommended constructing a 3,500m east–west runway to facilitate growth in cargo operations, particularly during the night hours.<ref>Vleugels 1987, p. 77</ref> The new runway would greatly reduce noise impact over the towns of [[Beek]], [[Meerssen]] and the city of [[Maastricht]]. Although some night operations are allowed (including distribution of the European edition of ''[[The Wall Street Journal]]''), runway length limits intercontinental operations.<ref>Vleugels 1987, p. 78</ref> The Dutch government initially approved plans for the runway in 1985,<ref>Vleugels 1987, p. 95</ref> however, the new runway would mean increased noise over other towns and parts of Belgium as well, and the final decision was delayed. As the new east-west runway would require substantial investment, it would only be profitable if night operations were permitted and increasingly the debate became focused on whether or not night flights should be allowed.<ref>{{cite web | url=http://www.volkskrant.nl/archief_gratis/article695147.ece/Jorritsma_en_De_Boer_onverzoenlijk_over_nachtvluchten_op_Beek_Nachtvluchten | title= 1996-10-03: Jorritsma en De Boer onverzoenlijk over nachtvluchten op Beek | author= [[De Volkskrant]] |accessdate= 2010-02-11}}</ref> Successive cabinets could not reach a final decision, and in 1998, after some 25 years of debate and postponement, the plan was aborted altogether.<ref>{{cite web | url= http://www.volkskrant.nl/archief_gratis/article781779.ece/Oost-westbaan_vliegveld_Beek_niet_langer_hoogstnoodzakelijk_werkgelegenheidsproject_Limburg_zet_kaarten_op_werken_rond_Nedcar | title= 1998-11-19: Oost-westbaan vliegveld Beek niet langer hoogstnoodzakelijk | author= [[De Volkskrant]] |accessdate= 2010-02-11}}</ref>

===1990-2009===
In 1992 the Belgian town of Tongeren became shareholder of the airport. Two years later, the board of trade or chamber of commerce of the nearby German city of [[Aachen]] became shareholder. This interest eventually became prominent and in October 1994 the airport's name was changed to "Maastricht-Aachen Airport".

In July 2004, a 100% share in the airport was acquired by OmDV, a consortium of airport investment company ''Omniport'' and the construction company ''Dura Vermeer'', making it the first fully privatised airport in the Netherlands.<ref>{{cite web |url= http://www.maa.nl/en/content.asp?i=248 |title= MAA website (Shareholders page) |accessdate=2008-04-11}}</ref>

Substantial investments in the airport infrastructure have been made since the privatization. Between August and October 2005, the runway was resurfaced and renamed to 03/21 (from 04/22) to compensate for changes in the [[earth's magnetic field]]. The airport originally had two runways; the second (shorter, {{convert|1080|m|abbr=on}}) runway (07/25) was closed and removed to make room for a new cargo [[airport terminal|terminal]] and additional aircraft maintenance facilities. Construction of the new facilities started in April 2008.

On 7 May 2005, [[Air Force One]] carrying US president [[George W. Bush]] landed at the airport.<ref>{{cite web | url=http://www.elsevier.nl/web/1030313/Nieuws/Nederland/Amerikaanse-president-aangekomen-in-Nederland.htm | title= Amerikaanse president aangekomen in Nederland | author= Elsevier.nl | accessdate=2010-05-31}}</ref> Bush visited the [[Netherlands American Cemetery]] in nearby [[Margraten]] the next day.<ref>{{cite web | url=http://www.volkskrant.nl/binnenland/article197755.ece/Bezoek_Bush_beeindigd_na_herdenking_in_Margraten | title= Bezoek Bush beëindigd na herdenking in Margraten | author= De Volkskrant | accessdate=2010-05-31}}</ref>

The [[instrument landing system]] (ILS) for runway 21 was upgraded to category III in 2008, which allows landings in very low visibility conditions. [[Amsterdam Airport Schiphol]] is the only other airport in the Netherlands that has category III ILS.

===2010s===
In March 2011, the airport was certified to handle the upcoming [[Boeing 747-8]], as two of the airports major airlines [[Cargolux]] and [[AirBridge Cargo]] have placed orders for this aircraft.<ref>{{Cite web | url= http://www.maa.nl/nl/news.asp?ii=126 | title= MAA Website News (dutch) |accessdate=2011-05-27}}</ref>

Ryanair announced on July 3, 2012 that Maastricht will become a new Ryanair base from December 2012, the first on Dutch soil, with one [[Boeing 737-300]] being based at the airport and three new routes being launched: [[Dublin]], [[London-Stansted]] and Treviso.

In late October 2012, start-up Dutch airline, [[Maastricht Airlines]], announced plans to base six Fokker 50 aircraft at the airport, initially operating to Berlin, Munich, and Amsterdam, before adding Copenhagen, Paris Charles de Gaulle and [[Southend]] in 2014. This did not happen and the company declared bankruptcy.<ref>{{cite web|url=http://www.nu.nl/economie/3491799/maastricht-airlines-failliet-verklaard.html|title=Maastricht Airlines failliet verklaard|work=nu.nl|accessdate=20 August 2016}}</ref>

Also in 2013, the airport was helped by the province with a 4.5 million Euro contribution. The airport was very close to bankruptcy during this period. Later on, in March 2014, the same province of Limburg, believed that closure was never an option. They decided that they would like to take over the airport.<ref>http://www.limburg.nl/Actueel/Nieuws_en_persberichten/2014/Maart_2014/GS_Maastricht_Aachen_Airport_overnemen</ref>

In December 2013 a spokesperson of the airport confirmed the closure of the Ryanair base from March 2014, entailing the ending of the [[Bergamo]], [[Brive]], Dublin, London-Stansted and [[Málaga]] flights.<ref>{{cite web|url=http://www.dutchnews.nl/news/archives/2013/12/budget_airline_ryanair_to_clos.php|title=Budget airline Ryanair to close Maastricht base - DutchNews.nl|date=17 December 2013|work=dutchnews.nl|accessdate=20 August 2016}}</ref><ref name="ryanairschedule">{{cite web|url=https://www.bookryanair.com/SkySales/booking.aspx?culture=de-de&lc=de-de#Select|title=Ryanair.com|work=bookryanair.com|accessdate=20 August 2016}}</ref>

==Aircraft movements==
The number of aircraft movements decreased significantly between 2005 and 2007 compared to previous years due to relocation of a major Dutch flight school, the ''Nationale Luchtvaartschool'', nowadays better known as [[CAE Global Academy|CAE Oxford Aviation Academy]]. The flight school, which was originally based at this airport, moved all flight operations to [[Évora Airport]] in Portugal. In the summer of 2007, flight training at the airport resumed as the ''Stella Aviation Academy'' moved into the facilities previously used by the NLS.

In 2009, there were a total of 40,621 aircraft movements, up 13.9% from 2008.<ref>{{cite web |title= Jaarverslag 2009 (Yearly Report 2009) |author= LVNL (Air Traffic Control the Netherlands)|url=http://lvnl.nl/nl/actueel/publicaties/publicaties/Jaarverslag%202009.pdf |accessdate=2010-06-01}}</ref> In 2008, there were a total of 35,668 aircraft movements, up 83.4% from 2007.<ref>{{cite web |title= Jaarverslag 2008 (Yearly Report 2008) |author= LVNL (Air Traffic Control the Netherlands)|url=http://www.lvnl.nl/nl/actueel/publicaties/publicaties/LVNL-jaarverslag%202008%20TOTAAL.pdf |accessdate=2009-10-29 }}</ref> In 2007, there were a total of 19,454 aircraft movements, up 35% from 2006.<ref>{{cite book |title=Jaarverslag 2007 (Yearly Report 2007) |author=Air Traffic Control the Netherlands |date=2008-04-11}}</ref>

==Airlines and destinations==
===Passenger===
{{Airport-dest-list
<!-- -->
| [[AlbaStar]] | '''Seasonal charter:''' [[Tarbes–Lourdes–Pyrénées Airport|Lourdes]]
<!-- -->
| [[Corendon Airlines]] | '''Seasonal:''' [[Antalya Airport|Antalya]] (resumes 20 April 2017)
<!-- -->
| [[Corendon Dutch Airlines]] | '''Seasonal:''' [[Faro Airport|Faro]] (begins 19 April 2017), [[Heraklion International Airport|Heraklion]] (begins 21 April 2017)
<!-- -->
| [[Germania (airline)|Germania]] | '''Seasonal:''' [[Palma de Mallorca Airport|Palma de Mallorca]] (begins 20 May 2017)
<!-- -->
| [[Ryanair]] | [[Alicante–Elche Airport|Alicante]]<br>'''Seasonal:''' [[Bari Karol Wojtyła Airport|Bari]], [[Girona–Costa Brava Airport|Girona]]
<!-- -->
| [[Wizz Air]] | [[Katowice International Airport|Katowice]] 
}}

===Cargo===
{{Airport-dest-list 
<!-- -->
| [[Royal Jordanian Cargo]] | [[Houari Boumediene Airport|Algiers]], [[Queen Alia International Airport|Amman–Queen Alia]], [[Cairo Airport|Caïro]], [[John F. Kennedy International Airport|New York–JFK]]
<!-- -->
| [[Silk Way Airlines]] <br>{{nowrap|operated by Sky Gates Airlines}} | [[Heydar Aliyev International Airport|Baku]],<ref name="luchtvaartnieuws.nl">http://www.luchtvaartnieuws.nl/nieuws/categorie/2/airlines/sky-gates-airlines-start-vluchten-naar-maastricht</ref> [[Sheremetyevo International Airport|Moscow–Sheremetyevo]]<ref name="luchtvaartnieuws.nl">http://www.luchtvaartnieuws.nl/nieuws/categorie/2/airlines/sky-gates-airlines-start-vluchten-naar-maastricht</ref>
<!-- -->
| [[Turkish Airlines Cargo]] | [[Kotoka International Airport|Accra]], [[Istanbul Atatürk Airport|Istanbul–Atatürk]]
<!-- -->
| [[Zimex Aviation]] | [[Birmingham Airport|Birmingham]], [[Dublin Airport|Dublin]] 
<!-- -->
}}

==Ground transportation==

===Car===
The airport is located along [[A2 motorway (Netherlands)|motorway A2]], exit 50. Taxis are available at the airport.

===Bus===
There is bus service (line 30), operated by [[Arriva]], running between Sittard and Maastricht. This line also covers transport between the [[Maastricht railway station]] and the airport.

==See also==
*[[Advanced Landing Ground#Y-44 to Y-64|Advanced Landing Ground]]

==References==

===Citations===
{{reflist|30em}}

===Bibliography===
*{{Air Force Historical Research Agency}}
*Vleugels, M (1987). ''Maastricht Airport 1919-1947-1987'', De Limburger.

==External links==
{{Commonscat-inline}}
* [http://www.maa.nl/ Official website]
* {{NWS-current|EHBK}}
* {{ASN|MST}}

{{Portalbar|Netherlands|Aviation|World War II}}
{{Maastricht}}
{{Airports in the Netherlands}}

{{Authority control}}

[[Category:Airfields of the United States Army Air Forces in the Netherlands]]
[[Category:Airports established in 1945]]
[[Category:Airports in Limburg (Netherlands)]]
[[Category:South Limburg (Netherlands)]]
[[Category:Transport in Maastricht]]
[[Category:Aachen]]
[[Category:Beek]]