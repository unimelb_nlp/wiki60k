{{infobox musical artist
| name                 = Marc Benjamin
| image                = 
| alt                  =
| image_size           =
| caption              = Marc Benjamin 2013
| landscape            = <!-- yes, if wide image, otherwise leave blank -->
| logo                 = 
| background           = non_vocal_instrumentalist
| birth_name           = Marc Benjamin Latupapua
| birth_date           = {{birth date and age|1990|01|16|}}
| birth_place          = [[Heemstede]], [[Netherlands]]
| nationality          = [[Haarlemmermeer]], [[Netherlands]]
| origin               = [[Netherlands]]
| occupation           = [[DJ]], [[record producer]]
| genre                = [[Electro house]], [[progressive house]], [[electronic dance music]]
| years_active         = 2007-present
| label                = [[Revealed Recordings]], [[Ultra Records]], [[Mixmash Records]], [[Wall Recordings]], [[Bertelsmann Music Group|BMG]]
| instrument           = [[Ableton Live]]
| time                 = 2007&ndash;present
| record               = [[Revealed Recordings]], [[Ultra Records]]
| associated_acts      = [[Benny Benassi]], [[Laidback Luke]]
| website             = {{url|marc-benjamin.com}}
| url2                 = 
| imdb                 = 
}}

'''Marc Benjamin Latupapua''' (born 16 January 1990 in [[Heemstede]]), better known under his stage name '''Marc Benjamin''', is a [[Dutch people|Dutch]]-[[Indonesian people|Indonesian]] [[DJ]] and [[music producer]]. He is known for his [[remix]] work and collaborations with [[Benny Benassi]], [[Laidback Luke]], and [[Zedd (musician)|Zedd]], and his releases on [[Ultra Records]], [[Revealed Recordings]], and [[Wall Recordings]].<ref name=Biography>[http://thedjlist.com/djs/MARC_BENJAMIN/bio/ ''The DJ List'', Marc Benjamin Biography]</ref>

In 2014, he performed at [[Ministry of Sound]] [[London]]<ref name=Ministry>[http://www.ministryofsound.com/events/calendar/club/2014/03/12/15/51/140426-laidback-luke#Rb5DVVTJv7APXvdq.97 ''Ministry of Sound'', Marc Benjamin at Saturday Sessions The Box]</ref> and made his [[Tomorrowland (festival)|Tomorrowland]] debut on the second [[Mainstage (festival)|mainstage]].<ref name=Tomorrowland>[http://www.youredm.com/2014/05/08/tomorrowland-2014-reveals-full-2-weekend-line/ ''Your EDM'', Marc Benjamin at Tomorrowland 2014]</ref><ref name=Biography/> [[Tomorrowland (festival)|Tomorrowland Belgium]] is marked as one of the biggest [[electronic music festivals]] in the world.

== Musical Career ==
Coming from a musical family, with his sister also being a DJ, Marc Benjamin has been working with music since a child.<ref>[http://www.festivalling.com/interviews/interview-marc-benjamin/ ''Festivalling'', Marc Benjamin Interview]</ref> The [[Dutch people|Dutch]] [[DJ]] achieved his first residency at the age of 16 years old, where he gained experience in the European club scene. At the age of 17 years old, Marc Benjamin was performing internationally, across Europe and soon later around the world.<ref name=Biography/><ref>[http://partyflock.nl/artist/14754/archive#archive ''Partyflock'', Marc Benjamin History]</ref>

Marc Benjamin’s first big release occurred on the 30th of December 2013 when his single “Riser”<ref>[http://dancingastronaut.com/2013/12/marc-benjamin-riser-original-mix/ ''Dancingastronaut'', Marc Benjamin - Riser]</ref><ref name=Biography/> on [[Afrojack]]’s [[Wall Recordings]] achieved a top ten slot in [[Beatport]]’s top 100 chart.<ref name=Biography/><ref>[http://www.bptoptracker.com/track/riser-original-mix/4996794''Beatport'', Top Tracker]</ref> This track gained him international exposure. Amongst other releases, Marc Benjamin signed a record deal with [[Mixmash Records]] and collaborated with [[Laidback Luke]], which led to the release of We’re Forever (June 02, 2014).<ref>[http://thedjlist.com/news/laidback-luke-marc-benjamin-release-forever/''The DJ List'', Laidback Luke & Marc Benjamin - We're Forever]</ref><ref name=Biography/> Marc then followed up with three singles on [[Hardwell]]’s record label [[Revealed Recordings]], “The Center” (October 10, 2014), “Lights Camera Action” (January 19, 2015) and “Galactic” (March 20, 2015).<ref name=Numano/><ref>[https://pro.beatport.com/artist/marc-benjamin/103697''Beatport Music'', Profile]</ref> As a result, Marc performed at [[Ministry of Sound|Ministry of Sound London]], followed by a showcase at [[Tomorrowland (festival)|Tomorrowland]], [[Belgium]].<ref name=Biography2>[https://member.djguide.nl/djinfo.p?djid=4306''DJ Guide'', Marc Benjamin Biography]</ref>

Marc Benjamin is a resident DJ in [[Amsterdam]]’s nightclub Escape<ref>[http://www.dancefair.nl/artist/marc-benjamin/ ''Dancefair'', Escape Amsterdam Residency]</ref> and is still producing tracks and remixes.<ref name=Biography2/> Marc was requested twice to remix work of the [[Grammy Award]]-winning producer, [[Zedd (musician)|Zedd]], which led to Marc’s remix of [[Zedd (musician)|Zedd]]'s 2014 single, “Find You” (April 7, 2014) and to Marc’s rendition for [[Zedd (musician)|Zedd]] & [[Selena Gomez]]’s “I Want You To Know,” (May 19, 2015).<ref name=Biography2/><ref name=Numano>[http://numano.media/MarcBenjaminDiscography2016.pdf''Numano Media'', Marc Benjamin Discography]</ref> Later, Marc joined [[Zedd (musician)|Zedd]]’s ‘True Colors tour’.<ref>[http://www.melkweg.nl/nl/agenda/zedd-21-11-2015/''Melkweg'', Zedd True Colors Tour]</ref><ref name=Biography2/>

In July 2015, Marc Benjamin collaborated with [[Benny Benassi]] on “Who I Am” featuring [[Christian Burns]], which was released on [[Ultra Music]] (July 17, 2015).<ref>[http://www.youredm.com/2015/07/16/premiere-benny-benassi-marc-benjamin-ft-christian-burns-who-i-am-ultra/''Your EDM'', Benny Benassi & Marc Benjamin ft Christian Burns - Who i am]</ref>

Furthermore, Marc Benjamin teamed up with [[Laidback Luke]] to work on their follow up single 'Tell Me That You Love Me’ which was released on [[Laidback Luke|Luke]]'s album ‘Focus' (November 06, 2015).<ref>[http://news.iheart.com/articles/trending-465498/interview-laidback-luke-reveals-song-meanings-14120060/''iHeart News'', Laidback Luke & Marc Benjamin - Tell me that you love me]</ref>

Marc Benjamin has gained support from [[Tiesto]], [[Steve Angello]], [[Martin Garrix]], [[Hardwell]], [[David Guetta]], [[Showtek]], [[Zedd (musician)|Zedd]], [[Dyro]], [[Dannic]], [[R3hab]] and [[Dimitri Vegas & Like Mike]].<ref name=Biography2/>

== Discography<ref name=Numano/> ==
=== Releases ===
* 2016: Marc Benjamin & DNMKG ft. Jaicko Lawrence - Reflection (This Time) (Powerhouse Music)
* 2015: [[Laidback Luke]] & Marc Benjamin - Tell Me That You Love Me (Mixmash Records)
* 2015: [[Benny Benassi]] & Marc Benjamin ft. [[Christian Burns]] - Who I Am (Back to the Future Mix) ([[Ultra Records]])
* 2015: [[Benny Benassi]] & Marc Benjamin ft. [[Christian Burns]] - Who I Am ([[Ultra Records]])
* 2015: Marc Benjamin - Galactic ([[Revealed Recordings]])
* 2015: Marc Benjamin & Revero - Lights Camera Action ([[Revealed Recordings]])
* 2014: Marc Benjamin - The Center ([[Revealed Recordings]])
* 2014: Marc Benjamin - City Lights (Mixmash Records)
* 2014: Marc Benjamin - Phantom (Mixmash Records)
* 2014: [[Laidback Luke]] & Marc Benjamin - We're Forever (Mixmash Records)
* 2014: Marc Benjamin - Rocket Science ([[Ultra Records]])
* 2013: Marc Benjamin - Wall ([[Wall Recordings]])
* 2013: Marc Benjamin - Break it down (OTW Mixmash Records)
* 2013: Marc Benjamin - The Crash (OTW Mixmash Records)
* 2013: Marc Benjamin - Soldier (OTW Mixmash Records)
* 2013: Marc Benjamin - Last Night ([[Dim Mak Records]])
* 2013: Marc Benjamin - Surrender (Mixmash Records)
* 2013: Marc Benjamin - Superstuff (Mixmash Records)
* 2012: Marc Benjamin - Games ([[Dim Mak Records]])
* 2011: Marc Benjamin - Just Relax (Bedroom Muzik)
* 2011: Marc Benjamin - Loose Cannon (Caballero Recordings)
* 2011: Marc Benjamin - Tuesday (Caballero Recordings)
* 2011: Marc Benjamin & Remaniax - Dancing in the dark ([[Vendetta Records]])
* 2011: Marc Benjamin - So Damn Baby (Soulman Music)
* 2010: Marc Benjamin - Members Only (Bedroom Muzik)
* 2010: Marc Benjamin - Roadtrip (Mixmash Records)
* 2010: Marc Benjamin, Remaniax & Skitzofrenix - Activate My Love ([[Spinnin Records]])
* 2010: Marc Benjamin, Remaniax & Skitzofrenix - Is That You ([[Spinnin Records]])
* 2010: Marc Benjamin - Trick (Sneakerz Muzik)
* 2010: Marc Benjamin - Nimsay (Sneakerz Muzik)
* 2009: Marc Benjamin - Trick (Sneakerz Muzik)
* 2009: Marc Benjamin - Nimsay (Sneakerz Muzik)
* 2009: Marc Benjamin - On the Boxes (Sneakerz Muzik)
* 2009: Marc Benjamin & Remaniax - Blazin (Audiodamage Records)

=== Remixed ===
* 2015: [[Zedd (musician)|Zedd]] & [[Selena Gomez]] - I Want You to Know (Marc Benjamin Remix) ([[Interscope]] / [[Universal Music Group|Universal Music]])
* 2014: [[Laidback Luke]] & Marc Benjamin - We're Forever (Marc Benjamin Remix) (Mixmash Records)
* 2014: John Christian - Next Level (Marc Benjamin Remix) ([[Protocol Recordings]])
* 2014: [[Zedd (musician)|Zedd]] ft [[Matthew Koma]] & [[Miriam Bryant]] - Find You (Marc Benjamin Remix) ([[Interscope]] / [[Universal Music Group|Universal Music]])
* 2014: [[Laidback Luke]] - Collide (Marc Benjamin Remix) (Mixmash Records)
* 2013: Clinton VanSciver - Indie Anna Jones (Marc Benjamin Remix) ([[Dim Mak Records]])
* 2013: Tom Staar - Kingdom (Marc Benjamin Remix) (Mixmash Records)

=== Extended plays and remixes ===
* 2014: We're Forever (The Remixes) (Mixmash Records)

=== Compilations ===
* 2011: Nope Is Dope Vol.10 (Mixed by Marc Benjamin) (Rodeo Media)

== References ==
{{reflist|30em}}

== External links ==
* [https://www.facebook.com/themarcbenjamin Marc Benjamin] on [[Facebook]]
* [https://twitter.com/marcbenjamins Marc Benjamin] on [[Twitter]]
* [https://soundcloud.com/marcbenjamin Marc Benjamin] on [[SoundCloud]]
* [https://www.youtube.com/djmarcbenjamin Marc Benjamin] on [[YouTube]]
* [http://www.instagram.com/marcbenjamin Marc Benjamin] on [[Instagram]]
* [http://open.spotify.com/user/marcbenjaminofficial Marc Benjamin] on [[Spotify]]



{{DEFAULTSORT:Benjamin, Marc}}
[[Category:1990 births]]
[[Category:Living people]]
[[Category:Remixers]]
[[Category:Club DJs]]
[[Category:Dutch DJs]]
[[Category:Dutch record producers]]
[[Category:Dutch dance musicians]]
[[Category:Dutch house musicians]]
[[Category:Dutch people of Moluccan descent]]
[[Category:Dutch people of Indonesian descent]]
[[Category:Musicians from Amsterdam]]
[[Category:Ultra Records artists]]
[[Category:Indo people]]