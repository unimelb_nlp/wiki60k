{{Accounting}}

'''International Accounting Standard 7: Statement of Cash Flows''' or '''IAS 7''' is an [[accounting]] standard that establishes standards for [[cash flow]] reporting used in [[International Financial Reporting Standards]].

A statement of cash flows for the periods, is an integral "Component of financial statements" as per [[IAS 1|IAS 1 — Presentation of Financial Statements]].

==History of IAS 7==

IAS 7 was reissued in December 1992, retitled in September 2007, and is operative for financial statements covering periods beginning on or after 1 January 1994.

{| class="wikitable"
|-
| June 1976 || Exposure Draft E7 ''Statement of Source and Application of Funds''
|-
| October 1977 || IAS 7 ''Statement of Changes in Financial Position''
|-
| July 1991 || Exposure Draft E36 ''Cash Flow Statements''
|-
| December 1992 || IAS 7 (1992) ''Cash Flow Statements''
|-
| 1 January 1994 || Effective date of IAS 7 (1992)
|-
|6 September 2007 || Retitled from ''Cash Flow Statements'' to ''Statement of Cash Flows'' as a consequential amendment resulting from revisions to [[IAS 1]]
|-
| 16 April 2009 || IAS 7 amended by Annual Improvements to [[International Financial Reporting Standards|IFRSs]] 2009 with respect to expenditures that do not result in a recognised asset.
|-
| 1 July 2009 || Effective date for amendments from IAS 27(2008) relating to changes in ownership of a subsidiary
|-
| 1 January 2010 || Effective date of the April 2009 revisions to IAS 7
|}

== References ==
{{reflist}}

== External links ==
* [http://www.iasb.org/Home.htm IASB] (International Accounting Standards Board)

{{International Financial Reporting Standards}}

{{DEFAULTSORT:Ias 7}}
[[Category:International Financial Reporting Standards|IAS 07]]
[[Category:Cash flow]]


{{business-stub}}