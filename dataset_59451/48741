{{Orphan|date=November 2016}}

'''Integrated quantum photonics,''' uses [[photonic integrated circuit]]s to control photonic quantum states for applications in quantum technologies.<ref name="politi2009">{{cite journal|last1=Politi|first1=A|last2=Matthews|first2=J.C.F|last3=Thompson|first3=M|last4=O'Brien|first4=J.L.|title=Integrated Quantum Photonics|journal=elected Topics in Quantum Electronics, IEEE Journal of|date=2009|volume=15|issue=6|pages=1673–1684|doi=10.1109/JSTQE.2009.2026060|url=http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=5340091&isnumber=5340088}}</ref> As such, integrated quantum photonics provides a promising approach to the miniaturization and optical scaling of optical quantum circuits.<ref>{{cite journal|last1=O'Brien|first1=Jeremy L.|last2=Furusawa|first2=Akira|last3=Vuckovic|first3=Jelena|title=Photonic quantum technologies|journal=Natue Photonics|date=2009|volume=3|issue=12|pages=687–695|doi=10.1038/nphoton.2009.229|url=http://dx.doi.org/10.1038/nphoton.2009.229}}</ref> Major areas of application of integrated quantum photonics include: [[quantum computation|quantum computing]],<ref>{{Cite journal|title = Quantum computers|url = http://www.nature.com/doifinder/10.1038/nature08812|journal = Nature|pages = 45–53|volume = 464|issue = 7285|doi = 10.1038/nature08812|first = T. D.|last = Ladd|first2 = F.|last2 = Jelezko|first3 = R.|last3 = Laflamme|first4 = Y.|last4 = Nakamura|first5 = C.|last5 = Monroe|first6 = J. L.|last6 = O’Brien}}</ref> [[Quantum key distribution|quantum communication]], [[Quantum simulator|quantum simulation]],<ref>{{Cite journal|title = Photonic quantum simulators|url = http://www.nature.com/doifinder/10.1038/nphys2253|journal = Nature Physics|pages = 285–291|volume = 8|issue = 4|doi = 10.1038/nphys2253|first = Alán|last = Aspuru-Guzik|first2 = Philip|last2 = Walther}}</ref><ref>{{cite journal|last1=Georgescu|first1=I. M.|last2=Ashhab|first2=S.|last3=Nori|first3=F.|title=Quantum Simulation|journal=Rev. Mod. Phys.|date=2014|volume=86|issue=1|pages=153–185|doi=10.1103/RevModPhys.86.153|url=http://link.aps.org/doi/10.1103/RevModPhys.86.153|bibcode=2014RvMP...86..153G}}</ref><ref>{{cite journal|last1=Perzzo|first1=Alberto|last2=Et al.|title=A variational eigenvalue solver on a photonic quantum processor|journal=Nat Commun|date=2014|volume=5|doi=10.1038/ncomms5213|url=http://dx.doi.org/10.1038/ncomms5213}}</ref> [[quantum walk]]s<ref>{{Cite journal
| last = Peruzzo
| first = Alberto
| last2 = et al.
| date = 
| year = 2010
| title = Quantum Walks of Correlated Photons
| url = 
| journal = Science
| volume = 329
| pages = 1500–1503
| doi = 10.1126/science.1193515
| pmid = 
| access-date = 
}}</ref><ref>{{Cite journal
| last = Crespi
| first = Andrea
| last2 = et al.
| date = 
| year = 2013
| title = Anderson localization of entangled photons in an integrated quantum walk
| url = 
| journal = Nature Photonics
| volume = 7
| pages = 322–328
| doi = 10.1038/nphoton.2013.26
| pmid = 
| access-date = 
}}</ref> and [[quantum metrology]].

== Introduction ==
Quantum photonics is the science of generating, manipulating and detecting light in regimes where it is possible to coherently control individual quanta of the light field (photons). Quantum photonics is generally recognised as being fundamental field in exploring quantum phenomena. Quantum photonics is also expected to play a central role in advancing future technologies - such as Quantum Information Processing (QIP). Photons are particularly attractive carriers of quantum information due to their low decoherence properties, light-speed transmission and ease of manipulation. Single photons were used for the very first violations of Bells inequality,<ref name="aspect1981">{{cite journal|last1=Aspect|first1=A.|last2=Grainger|first2=P.|last3=Gerard|first3=R|title=Experimental Tests of Realistic Local Theories via Bell's Theorem|journal=Phys. Rev. Lett|date=1981|volume=47|issue=7|pages=460–463|doi=10.1103/PhysRevLett.47.460|url=http://link.aps.org/doi/10.1103/PhysRevLett.47.460|bibcode=1981PhRvL..47..460A}}</ref><ref name="clauser1972">{{cite journal|last1=Freedman|first1=S. J.|last2=Clauser|first2=J. F.|title=Experimental Test of Local Hidden-Variable Theories|journal=Phys. Rev. Lett.|date=1972|volume=28|pages=938–941|doi=10.1103/PhysRevLett.28.938|url=http://link.aps.org/doi/10.1103/PhysRevLett.28.938|bibcode=1972PhRvL..28..938F}}</ref> and are employed within many highly successful proof-of principle demonstrations of emerging quantum technologies.<ref name="dx.doi.org">{{cite journal|last1=Shadbolt|first1=Peter|last2=Matthews|first2=Jonathan C. F.|last3=Laing|first3=Anthony|last4=O'Brien|first4=Jeremy L.|title=Testing foundations of quantum mechanics with photons|journal=Nat Phys|date=2014|volume=10|issue=4|pages=278–286|doi=10.1038/nphys2931|url=http://dx.doi.org/10.1038/nphys2931}}</ref>

The most common implementation of quantum photonics employs linear optical components such as beamsplitters, mirrors and wave-plates. Such realizations of quantum photonics have been used to implement Linear Optical quantum Computation (LOQC).

However, such experiments suffer from several optical scaling problems as the size of the experiments are increased.
# '''Stability''' - Large numbers of linear optical components can introduce non-coherent phase changes.
# '''Experiment size''' - Linear optical components are typical large, requiring
# '''Manufacturability''' - Devices built using linear optical components can present problems in mass-manufacturing 
These problems are especially pertinent for LOQC applications of quantum photonics.

Integrated Quantum Photonics is an approach that addresses these problems by employing implementations of linear optical components that can be built on-chip using variations on existing fabrication technologies. Such an integrated approach to quantum photonics inherently ensures phase stability (with all components being within the same chip substrate), allowing high quality non-classical interference to be achieved.<ref name="politi2009" /> Being based on well-developed fabrication techniques, the elements employed in Integrated Quantum Photonics are more readily miniaturisable, and products based on this approach can be manufactured using existing production methodologies.

== History ==

Linear optics, as a platform for quantum computation specifically, began to show a marked rise in activity after the publication of the seminal theory paper of Knill-Laflamme-Milburn,<ref>{{Cite journal|title = A scheme for efficient quantum computation with linear optics|url = http://www.nature.com/doifinder/10.1038/35051009|journal = Nature|pages = 46–52|volume = 409|issue = 6816|doi = 10.1038/35051009|first = E.|last = Knill|first2 = R.|last2 = Laflamme|first3 = G. J.|last3 = Milburn|pmid=11343107|date=January 2001}}</ref> which demonstrated the feasibility of linear optical quantum computers using post-selection, feed-forward and number resolving detection. Following this there were several experimental proof-of-principle demonstrations of two-qubit gates performed in bulk optics.<ref>{{Cite journal|title = Demonstration of an all-optical quantum controlled-NOT gate|url = http://www.nature.com/doifinder/10.1038/nature02054|journal = Nature|pages = 264–267|volume = 426|issue = 6964|doi = 10.1038/nature02054|first = J. L.|last = O'Brien|first2 = G. J.|last2 = Pryde|first3 = A. G.|last3 = White|first4 = T. C.|last4 = Ralph|first5 = D.|last5 = Branning|pmid=14628045|date=November 2003}}</ref><ref>{{Cite journal|title = Experimental controlled-NOT logic gate for single photons in the coincidence basis|url = http://link.aps.org/doi/10.1103/PhysRevA.68.032316|journal = Physical Review A|date = 2003-09-26|pages = 032316|volume = 68|issue = 3|doi = 10.1103/PhysRevA.68.032316|first = T. B.|last = Pittman|first2 = M. J.|last2 = Fitch|first3 = B. C|last3 = Jacobs|first4 = J. D.|last4 = Franson}}</ref><ref>{{Cite journal|title = Realization of a Knill-Laflamme-Milburn controlled-NOT photonic quantum circuit combining effective optical nonlinearities|url = http://www.pnas.org/content/108/25/10067|journal = Proceedings of the National Academy of Sciences|date = 2011-06-21|issn = 0027-8424|pmc = 3121828|pmid = 21646543|pages = 10067–10071|volume = 108|issue = 25|doi = 10.1073/pnas.1018839108|language = en|first = Ryo|last = Okamoto|first2 = Jeremy L.|last2 = O’Brien|first3 = Holger F.|last3 = Hofmann|first4 = Shigeki|last4 = Takeuchi}}</ref>  It soon became clear that integrated optics could provide a powerful enabling technology for this emerging field.<ref>{{Cite journal|title = On the genesis and evolution of Integrated Quantum Optics|url = http://onlinelibrary.wiley.com/doi/10.1002/lpor.201100010/abstract|journal = Laser & Photonics Reviews|date = 2012-01-02|issn = 1863-8899|pages = 115–143|volume = 6|issue = 1|doi = 10.1002/lpor.201100010|language = en|first = S.|last = Tanzilli|first2 = A.|last2 = Martin|first3 = F.|last3 = Kaiser|first4 = M.p.|last4 = De Micheli|first5 = O.|last5 = Alibart|first6 = D.b.|last6 = Ostrowsky}}</ref>  Early experiments in integrated optics demonstrated the feasibility of the field via demonstrations of high-visibility non-classical and classical interference in simple components based on linear optics such as directional couplers (DCs) and Mach–Zehnder interferometers.<ref name="Smith2009" /><ref>{{cite journal|last1=Politi|first1=A|last2=Et al.|title=Silica-on-Silicon Waveguide Quantum Circuits|journal=Science|date=2008|volume=320|issue=5878|pages=646–649|doi=10.1126/science.1155441|url=http://science.sciencemag.org/content/320/5876/646.abstract|pmid=18369104}}</ref><ref>{{cite journal|last1=Laing|first1=Anthony|last2=Et al.|title=High-fidelity operation of quantum photonic circuits|journal=Applied Physics. Letters|date=2010|volume=97|issue=21|doi=10.1063/1.3497087|url=http://scitation.aip.org/content/aip/journal/apl/97/21/10.1063/1.3497087|pages=211109}}</ref> These components were then be used to fabricate more complicated circuits such as multi-photon [[Controlled NOT gate|entangling gates]] as well as core components used for fully reconfigurable quantum circuits,<ref name="carolan2015">{{cite journal|last1=Carolan|first1=J|last2=Et al.|title=Universal Linear Optics|journal=Science|date=2015|volume=349|issue=6249|pages=711–716|doi=10.1126/science.aab3642|url=http://science.sciencemag.org/content/349/6249/711|pmid=26160375}}</ref> where reconfigurability can achieved using both thermal and electro-optic phase shifters.<ref>{{cite journal|last1=Miya|first1=T|title=Silica-based planar lightwave circuits: passive and thermally active devices|journal=Selected Topics in Quantum Electronics, IEEE Journal of|date=2000|volume=6|issue=1|pages=38–45|doi=10.1109/2944.826871|url=http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=826871&isnumber=17936}}</ref><ref>{{cite journal|last1=Wang|first1=J|last2=Et al.|title=Gallium Arsenide (GaAs) Quantum Photonic Waveguide Circuits|journal=Optics Communications|date=2014|doi=10.1016/j.optcom.2014.02.040|arxiv=1403.2635|volume=327|pages=49–55}}</ref><ref>{{Cite journal
| last = Chaboyer
| first = Zachary
| last2 = et al.
| date = 
| year = 2015
| title = Tunable quantum interference in a 3D integrated circuit
| url = 
| journal = Scientific Reports
| volume = 5
| page = 9601
| doi = 10.1038/srep09601
| pmid = 25915830
| access-date = 
}}</ref><ref>{{Cite journal
| last = Flamini
| first = Fulvio
| last2 = et al.
| date = 
| year = 2015
| title = Thermally reconfigurable quantum photonic circuits at telecom wavelength by femtosecond laser micromachining
| url = 
| journal = Light: Science & Applications
| volume = 4
| page = e354
| doi = 10.1038/lsa.2015.127
| pmid = 
| access-date = 
}}</ref>

Another area of research in which integrated optics will prove pivotal in its development is Quantum communication and has been marked by extensive experimental development demonstrating, for example, quantum key distribution (QKD),<ref>{{Cite journal|title = Reference-Frame-Independent Quantum-Key-Distribution Server with a Telecom Tether for an On-Chip Client|url = http://link.aps.org/doi/10.1103/PhysRevLett.112.130501|journal = Physical Review Letters|date = 2014-01-01|volume = 112|issue = 13|doi = 10.1103/PhysRevLett.112.130501|first = P.|last = Zhang|bibcode=2014PhRvL.112m0501Z|pmid=24745397|page=130501}}</ref><ref>{{Cite journal|title = Chip-based Quantum Key Distribution|arxiv = 1509.00768|journal = Quantum Physics|date = 2015-09-02|first = Philip|last = Sibson|first2 = Chris|last2 = Erven|first3 = Mark|last3 = Godfrey|first4 = Shigehito|last4 = Miki|first5 = Taro|last5 = Yamashita|first6 = Mikio|last6 = Fujiwara|first7 = Masahide|last7 = Sasaki|first8 = Hirotaka|last8 = Terai|first9 = Michael G.|last9 = Tanner}}</ref> quantum teleportation,<ref>{{Cite journal|title = Quantum teleportation on a photonic chip|url = http://www.nature.com/doifinder/10.1038/nphoton.2014.217|journal = Nature Photonics|pages = 770–774|volume = 8|issue = 10|doi = 10.1038/nphoton.2014.217|first = Benjamin J.|last = Metcalf|first2 = Justin B.|last2 = Spring|first3 = Peter C.|last3 = Humphreys|first4 = Nicholas|last4 = Thomas-Peter|first5 = Marco|last5 = Barbieri|first6 = W. Steven|last6 = Kolthammer|first7 = Xian-Min|last7 = Jin|first8 = Nathan K.|last8 = Langford|first9 = Dmytro|last9 = Kundys}}</ref> quantum relays based on entanglement swapping, and quantum repeaters.

Since the birth of integrated quantum optics experiments have ranged from technological demonstrations, for example integrated sources<ref>{{Cite journal|title = On-chip quantum interference between silicon photon-pair sources|url = http://www.nature.com/doifinder/10.1038/nphoton.2013.339|journal = Nature Photonics|pages = 104–108|volume = 8|issue = 2|doi = 10.1038/nphoton.2013.339|first = J. W.|last = Silverstone|first2 = D.|last2 = Bonneau|first3 = K.|last3 = Ohira|first4 = N.|last4 = Suzuki|first5 = H.|last5 = Yoshida|first6 = N.|last6 = Iizuka|first7 = M.|last7 = Ezaki|first8 = C. M.|last8 = Natarajan|first9 = M. G.|last9 = Tanner}}</ref><ref>{{Cite journal|title = On-chip low loss heralded source of pure single photons|url = https://www.osapublishing.org/oe/abstract.cfm?uri=oe-21-11-13522|journal = Optics Express|date = 2013-06-03|volume = 21|issue = 11|doi = 10.1364/oe.21.013522|language = EN|first = Justin B.|last = Spring|first2 = Patrick S.|last2 = Salter|first3 = Benjamin J.|last3 = Metcalf|first4 = Peter C.|last4 = Humphreys|first5 = Merritt|last5 = Moore|first6 = Nicholas|last6 = Thomas-Peter|first7 = Marco|last7 = Barbieri|first8 = Xian-Min|last8 = Jin|first9 = Nathan K.|last9 = Langford|pages=13522}}</ref><ref>{{cite journal|last1=Dousse|first1=Adrien|last2=Et al.|title=Ultrabright source of entangled photon pairs|journal=Nature|date=2010|volume=466|issue=7303|pages=217–220|url=http://dx.doi.org/10.1038/nature09148|doi=10.1038/nature09148}}</ref> and integrated detectors,<ref>{{cite journal|last1=Sahin|first1=D.|last2=Et al.|title=Waveguide Nanowire Superconducting Single-Photon Detectors Fabricated on GaAs and the Study of Their Optical Properties|journal=IEEE JOURNAL OF SELECTED TOPICS IN QUANTUM ELECTRONICS|date=2015|volume=21|issue=2|pages=1–10|doi=10.1109/JSTQE.2014.2359539|url=http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=6905769&isnumber=6895192}}</ref> to fundamental tests of nature,<ref name="dx.doi.org"/><ref>{{cite journal|last1=Peruzzo|first1=Alberto|last2=Shadbolt|first2=Peter|last3=Brunner|first3=Nicolas|last4=Popescu|first4=Sandu|last5=O'Brien|first5=Jeremy L.|title=A Quantum Delayed-Choice Experiment|journal=Science|date=2012|volume=338|issue=6107|pages=634–637|doi=10.1126/science.1226719|url=http://science.sciencemag.org/content/338/6107/634|pmid=23118183}}</ref> new methods for quantum key distribution,<ref>{{cite arXiv|last1=Sibson|first1=P.|last2=Et al.|title=Chip-based Quantum Key Distribution|arxiv=1509.00768 |class=quant-ph}}</ref> and the generation of new quantum states of light.<ref>{{cite journal|last1=Orieux|first1=Adeline|last2=Et al.|title=Experimental Generation of Robust Entanglement from Classical Correlations via Local Dissipation|journal=Phys. Rev. Lett|date=2015|volume=115|issue=16|page=5|doi=10.1103/PhysRevLett.115.160503|url=http://link.aps.org/doi/10.1103/PhysRevLett.115.160503|bibcode=2015PhRvL.115p0503O}}</ref>  It has also been demonstrated that a single integrated device is sufficient to implement the full field of linear optics.<ref name="carolan2015" /><ref>{{cite arXiv|last1=Harris|first1=N. C.|last2=Et al.|title=Bosonic transport simulations in a large-scale programmable nanophotonic processor|date=2015 |arxiv=1507.03406}}</ref><ref>{{Cite journal|title = Experimental realization of any discrete unitary operator|url = http://link.aps.org/doi/10.1103/PhysRevLett.73.58|journal = Physical Review Letters|date = 1994-07-04|pages = 58–61|volume = 73|issue = 1|doi = 10.1103/PhysRevLett.73.58|first = Michael|last = Reck|first2 = Anton|last2 = Zeilinger|first3 = Herbert J.|last3 = Bernstein|first4 = Philip|last4 = Bertani|bibcode=1994PhRvL..73...58R|pmid=10056719}}</ref>

As the field has progressed new quantum algorithms have been developed which provide short and long term routes towards the demonstration of the superiority of quantum computers over their classical counterparts.  Cluster state computation is now generally accepted as the approach that will be used to develop a fully fledged quantum computer.<ref>{{cite journal | author=H. J. Briegel and R. Raussendorf| authorlink= | title=Persistent Entanglement in arrays of Interacting Particles |journal=[[Physical Review Letters]]| year=2001 |volume=86 | doi=10.1103/PhysRevLett.86.910 | pmid=11177971 | issue=5 | bibcode=2001PhRvL..86..910B | pages=910–3|arxiv = quant-ph/0004051 }}</ref>  Whilst development of quantum computer will require the synthesis of many different aspects of integrated optics, boson sampling<ref name="AaronsonBS">{{cite web|last1=Aaronson|first1=Scott|last2=Arkhipov|first2=Alex|title=The Computational Complexity of Linear Optics|url=http://www.scottaaronson.com/papers/optics.pdf|website=scottaaronson}}</ref> seeks to demonstrate the power of quantum computation via technologies readily available and is therefore a very promising near term algorithm to doing so.  In fact shortly after its proposal there were several small scale experimental demonstrations of the [[Boson Sampling|boson sampling]] algorithm<ref>{{Cite journal|title = Photonic Boson Sampling in a Tunable Circuit|url = http://science.sciencemag.org/content/339/6121/794|journal = Science|date = 2013-02-15|issn = 0036-8075|pmid = 23258411|pages = 794–798|volume = 339|issue = 6121|doi = 10.1126/science.1231440|language = en|first = Matthew A.|last = Broome|first2 = Alessandro|last2 = Fedrizzi|first3 = Saleh|last3 = Rahimi-Keshari|first4 = Justin|last4 = Dove|first5 = Scott|last5 = Aaronson|first6 = Timothy C.|last6 = Ralph|first7 = Andrew G.|last7 = White}}</ref><ref>{{Cite journal|title = Boson Sampling on a Photonic Chip|url = http://science.sciencemag.org/content/339/6121/798|journal = Science|date = 2013-02-15|issn = 0036-8075|pmid = 23258407|pages = 798–801|volume = 339|issue = 6121|doi = 10.1126/science.1231692|language = en|first = Justin B.|last = Spring|first2 = Benjamin J.|last2 = Metcalf|first3 = Peter C.|last3 = Humphreys|first4 = W. Steven|last4 = Kolthammer|first5 = Xian-Min|last5 = Jin|first6 = Marco|last6 = Barbieri|first7 = Animesh|last7 = Datta|first8 = Nicholas|last8 = Thomas-Peter|first9 = Nathan K.|last9 = Langford}}</ref><ref>{{cite journal|last1=Tillman|first1=M|last2=Et al.|title=Experimental boson sampling|journal=Nat Photon|date=2013|volume=7|issue=7|pages=540–544|doi=10.1038/nphoton.2013.102|url=http://dx.doi.org/10.1038/nphoton.2013.102}}</ref><ref>{{Cite journal|title = Integrated multimode interferometers with arbitrary designs for photonic boson sampling|url = http://www.nature.com/doifinder/10.1038/nphoton.2013.112|journal = Nature Photonics|pages = 545–549|volume = 7|issue = 7|doi = 10.1038/nphoton.2013.112|first = Andrea|last = Crespi|first2 = Roberto|last2 = Osellame|first3 = Roberta|last3 = Ramponi|first4 = Daniel J.|last4 = Brod|first5 = Ernesto F.|last5 = Galvão|first6 = Nicolò|last6 = Spagnolo|first7 = Chiara|last7 = Vitelli|first8 = Enrico|last8 = Maiorino|first9 = Paolo|last9 = Mataloni|first10 = Fabio|last10 = Sciarrino}}</ref>

== Materials ==
Control over photons can be achieved with integrated devices that can be realised in different material platforms such as Silica, [[Silicon photonics|Silicon]], [[Gallium arsenide]], [[Lithium niobate]] and [[Indium phosphide]].

=== Silica ===
Two methods for using silica:

# Flame hydrolosis and another method - more like traditional lithography etching and depositing different materials.
2. Direct write - only uses single material and laser (use computer controlled laser to damage the glass and user lateral motion and focus to write paths with required refractive indices to produce waveguides). This method has the benefit of not needing a clean room. Most common method now. Some challenges, but promising. Also starting to write heaters using lasers (Roberto osellame).

=== Silicon ===
Using traditional lithography - same recipes used in electronics. Structures are different from modern electronic ones. But techniques scale. Silicon has a really high refractive index. Makes nice interface with glass which has lower refractive index. This allows waveguides made form silicon and glass to be small and have tight bends, which allows you to make denser systems.

=== Lithium Niobate ===

== Fabrication ==
Conventional fabrication technologies are based on [[Photolithography|photolitographic processes]], which enable strong miniaturization and mass production. In quantum optics applications a relevant role has been played also by the direct inscription of the circuits by femtosecond lasers<ref>{{cite journal|last1=Marshall|first1=Graham D.|last2=Politi|first2=Alberto|last3=Matthews|first3=Jonathan C. F.|last4=Dekker|first4=Peter|last5=Ams|first5=Martin|last6=Withford|first6=Michael J.|last7=O'Brien|first7=Jeremy L.|title=Laser written waveguide photonic quantum circuits|journal=Optics Express|date=9 July 2009|volume=17|issue=15|pages=12546|doi=10.1364/OE.17.012546}}</ref> or UV lasers;<ref name="Smith2009">{{cite journal|last1=Smith|first1=Brian J.|last2=Kundys|first2=Dmytro|last3=Thomas-Peter|first3=Nicholas|last4=Smith|first4=P. G. R.|last5=Walmsley|first5=I. A.|title=Phase-controlled integrated photonic quantum circuits|journal=Optics Express|date=22 July 2009|volume=17|issue=16|pages=13516|doi=10.1364/OE.17.013516}}</ref> these are serial fabrication technologies, which are particularly convenient for research purposes, where novel designs have to be tested with rapid fabrication turnaround. Femtosecond laser written quantum circuits have proven particularly suited for the manipulation of the polarization degree of freedom<ref>{{cite journal|last1=Sansoni|first1=Linda|last2=Sciarrino|first2=Fabio|last3=Vallone|first3=Giuseppe|last4=Mataloni|first4=Paolo|last5=Crespi|first5=Andrea|last6=Ramponi|first6=Roberta|last7=Osellame|first7=Roberto|title=Polarization Entangled State Measurement on a Chip|journal=Physical Review Letters|date=10 November 2010|volume=105|issue=20|doi=10.1103/PhysRevLett.105.200503|bibcode=2010PhRvL.105t0503S}}</ref><ref>{{Cite journal
| last = Crespi
| first = Andrea
| last2 = et al.
| date = 
| year = 2011
| title = Integrated photonic quantum gates for polarization qubits
| url = 
| journal = Nature Communications
| volume = 2
| page = 566
| doi = 10.1038/ncomms1570
| pmid = 
| access-date = 
}}</ref><ref>{{Cite journal
| last = Corrielli
| first = Giacomo
| last2 = et al.
| date = 
| year = 2014
| title = Rotated waveplates in integrated waveguide optics
| journal = Nature Communications
| volume = 5
| page = 4249
| doi = 10.1038/ncomms5249
| pmid = 24963757
| pmc=4083439
}}</ref><ref>{{Cite journal
| last = Heilmann
| first = Renè
| last2 = et al.
| date = 
| year = 2014
| title = Arbitrary photonic wave plate operations on chip: Realizing Hadamard, Pauli-X, and rotation gates for polarisation qubits
| journal = Scientific Reports
| volume = 4
| page = 4118
| doi = 10.1038/srep04118
| pmid = 24534893
| pmc=3927208
}}</ref>  and for building circuits with innovative three-dimensional design.<ref>{{cite journal|last1=Crespi|first1=Andrea|last2=Sansoni|first2=Linda|last3=Della Valle|first3=Giuseppe|last4=Ciamei|first4=Alessio|last5=Ramponi|first5=Roberta|last6=Sciarrino|first6=Fabio|last7=Mataloni|first7=Paolo|last8=Longhi|first8=Stefano|last9=Osellame|first9=Roberto|title=Particle Statistics Affects Quantum Decay and Fano Interference|journal=Physical Review Letters|date=2 March 2015|volume=114|issue=9|doi=10.1103/PhysRevLett.114.090201|bibcode=2015PhRvL.114i0201C|pmid=25793783|page=090201}}</ref><ref>{{cite journal|last1=Gräfe|first1=Markus|last2=Heilmann|first2=René|last3=Perez-Leija|first3=Armando|last4=Keil|first4=Robert|last5=Dreisow|first5=Felix|last6=Heinrich|first6=Matthias|last7=Moya-Cessa|first7=Hector|last8=Nolte|first8=Stefan|last9=Christodoulides|first9=Demetrios N.|last10=Szameit|first10=Alexander|title=On-chip generation of high-order single-photon W-states|journal=Nature Photonics|date=31 August 2014|volume=8|issue=10|pages=791–795|doi=10.1038/nphoton.2014.204}}</ref><ref>{{Cite journal
| last = Spagnolo
| first = Nicolò
| last2 = et al.
| date = 
| title = Three-photon bosonic coalescence in an integrated tritter
| url = 
| journal = Nature Communications
| volume = 4
| page = 1606
| doi = 10.1038/ncomms2616
| pmid = 23511471
| access-date = 
}}</ref><ref>{{Cite journal
| last = Crespi
| first = Andrea
| last2 = et al.
| date = 
| year = 2016
| title = Suppression law of quantum states in a 3D photonic fast Fourier transform chip
| journal = Nature Communications
| volume = 7
| page = 10469
| doi = 10.1038/ncomms10469
| pmid = 26843135
| pmc=4742850
}}</ref> [[Quantum information]] is encoded on-chip in either the path, polarisation, time bin or frequency state of the photon, and manipulated using active integrated components in a compact and stable manner.

== Components ==

=== Waveguides ===
Channels

=== Directional coupler ===
Produces a fixed phase

=== Active components ===
Thermo-optic effect – slowly varying changes ,

Electro-optic effect - Injecting carriers in PN. Can’t be done in QM domain yet

=== Light sources ===

=== Detectors ===

==See also==

* [[Linear optical quantum computing]]
* [[Quantum information]]
* [[Quantum key distribution]]

== References ==
{{Reflist}}

== External links ==
* [http://www.quchip.eu QUCHIP Project]
* [http://www.3dquest.eu 3D-QUEST Project]
* [http://www.bristol.ac.uk/physics/research/quantum/ Center for Quantum Photonics, University of Bristol]
* [http://www.mi.ifn.cnr.it/research/fs-micromachining Fast Group, Istituto di Fotonica e Nanotecnologie, Consiglio Nazionale delle Ricerche]
* [http://physik.uni-paderborn.de/en/silberhorn/ Integrated Quantum Optics, Paderborn University]

[[Category:Photonics]]