{{pp-move-indef}}
{{Infobox television episode
| title = A Rugrats Chanukah
| series = Rugrats 
| image = [[File:Rugrats Chanukah.jpg|225px|alt=A cartoon of an elderly man lighting a Menorah. He is bald and wearing a Kippah. At his feet are three toddlers; two are on their hands and knees, the other is standing. To their right are two infants sitting on a large dog. One infant is bald and wearing a nappy; the other is wearing a t-shirt and shorts.]]
| caption = Promotional artwork featuring Grandpa Boris and the Rugrats lighting the [[Menorah (Hanukkah)|Menorah]]
| season = 4
| episode = 1
| airdate = {{Start date|1996|12|04}}
| production = 999<ref name="main"/><ref name="DVD"/>
| writer = {{Plainlist|
* J. David Stem
* [[David N. Weiss]]
}}
| director = Raymie Muzquiz
| guests =*[[Fyvush Finkel]] as Shlomo
*[[Ron Leibman]] as Rabbi / Old Man
*[[Alan Rachins]] as Lowell / Greek Bully / Donut Man
*[[Alan Rosenberg]] as Mr. Dreidel / TV Announcer
*Bruce Young Berman as Parade Crooner
*Mt. Zion's Women Choir
**Edie Lehmann - Choir Leader
**Joan Beal
**Susan Boyd
**Linda Harmon
**Luana Jackman
**Susan McBride
**Bobbi Page
**Sally Stevens
**[[Carmen Twillie (actress)|Carmen Twillie]]
| episode_list = [[List of Rugrats episodes|List of ''Rugrats'' episodes]]
| prev = [[A Rugrats Passover]]
| next = "[[Mother's Day (Rugrats)|Mother's Day]]"
}}
"'''A Rugrats Chanukah'''", titled onscreen as "'''Chanukah'''" and sometimes called the "'''Rugrats Chanukah Special'''", is a [[television special|special]] episode of [[Nickelodeon]]'s animated television series ''[[Rugrats]]''. The first episode of the show's fourth season and the sixty-sixth overall, it tells the story of the [[Jewish]] holiday [[Hanukkah|Chanukah]] through the eyes of [[List of Rugrats characters#Babies and kids|the Rugrats]], who imagine themselves as the main characters. Meanwhile, [[List of Rugrats characters|Grandpa Boris]] and his long-time rival, Shlomo, feud over who will play the lead in the local [[synagogue]]'s Chanukah play.

Raymie Muzquiz directed "A Rugrats Chanukah" from a script by J. David Stem and [[David N. Weiss]]. In 1992, Nickelodeon executives had [[Pitch (filmmaking)|pitched]] the idea of a Chanukah special to the production team, but the concept was revised and became the 1995 special, "[[A Rugrats Passover]]". After production of the Passover episode wrapped, the crew returned to the Chanukah idea. Nickelodeon broadcast "A Rugrats Chanukah" on December 4, 1996; the episode received a [[Nielsen ratings|Nielsen rating]] of 7.9 and positive reviews from television critics. Along with other ''Rugrats'' episodes featuring Boris and his wife, the special attracted controversy when the [[Anti-Defamation League]] compared the character designs to [[Antisemitism|anti-Semitic]] drawings from a 1930s [[Nazism|Nazi]] newspaper.

==Plot==
On Chanukah, [[List of Rugrats characters#Adults|Grandma Minka]] reads a book about the meaning of the holiday to the babies [[Tommy Pickles|Tommy]], Chuckie, [[Phil and Lil DeVille|Phil, and Lil]]. The babies imagine that they are the story's characters; [[Judas Maccabeus|Judah]] (Tommy) is outraged by [[Antiochus IV|King "Antonica"]], who has taken over [[Judea|the Jewish kingdom]] and forced [[Hellenization|Greek culture]] on its inhabitants. Judah leads an army of Jewish [[Maccabees]] to war against Antonica's [[Seleucid Empire]], emerging victorious. The story is left unfinished as Minka stops to help make [[potato pancakes|latkes]] in the kitchen with her daughter Didi.

Meanwhile, Grandpa Boris is furious that Shlomo, a rival from his youth in Russia, is pictured in the local newspaper for playing the Greek king in the local synagogue's Chanukah play, where Boris is portraying Judah. The babies find out about Shlomo and form the impression that he truly is the Greek king, whom they dub the "Meanie of Chanukah". At the play that night, they attempt to storm on stage to defeat the "Meanie of Chanukah", but are stopped and taken into the synagogue's nursery. Angelica is in the nursery already and, vehement in her desire to watch a Christmas special that is airing that night, convinces the babies to help her break out and steal a television set from the custodian's office.

Boris and Shlomo begin fighting on stage during the play, interrupting the production and inciting an intermission. Backstage, Shlomo and Boris argue once more, with Boris mentioning Shlomo's dedication to his business pursuits over familial values. Shlomo informs Boris that he and his late wife were unable to bear children, making Boris feel sympathy for his rival. Angelica sprints backstage, bumping into Shlomo and inadvertently destroying the television set. Shlomo unsuccessfully tries to console her, but eventually lets Boris take over. Tommy hands Shlomo the Chanukah story book Minka read to the babies earlier; Boris convinces Shlomo to read it to the children. In the conclusion of the story, the Maccabees rededicate the [[Holy Temple]], and discover that there is only enough oil to light the Temple's eternal flame for one day; miraculously, it remains lit for eight. Shlomo's reciting dissolves both the babies' assertion of him as the "Meanie of Chanukah" and his and Boris' rivalry.

==Production==
Nickelodeon executives [[Pitch (filmmaking)|pitched]] the idea of making a Chanukah special to the ''Rugrats'' production team in 1992. [[Paul Germain]], the show's co-creator, responded with a [[Passover]] special instead, as he considered it to be a "funny idea"<ref name="raise">{{cite news|title=How raising the Rugrats children became as difficult as the real thing|author =Swartz, Mimi|date=1998-10-30|work=[[The New Yorker]]|page=62}}</ref> and of "historical interest".<ref name="four">{{cite journal|title=Four questions for creator of 'Rugrats': Cartoon series offers a Passover plot for the younger set|author =Elkin, Michael|date=1995-04-14|work=[[Jewish Exponent]]}}</ref> "[[A Rugrats Passover]]" was completed in 1995;<ref name="main">{{cite web|url=http://www.cooltoons2.com/rugrats/|title=Rugrats → Episode Guide → Specials → More → Rugrats chanukah|accessdate=2009-10-25|publisher=[[Klasky-Csupo]]|format=[[Adobe Flash]] page}}</ref><ref name="NYT">{{cite news|title='Rugrats' Observes Passover|newspaper=[[The New York Times]]|date=1995-04-13|url=https://www.nytimes.com/1995/04/13/movies/television-review-rugrats-observes-passover.html|author =O'Connor, John J.|page=16|accessdate=2009-12-22}}</ref><ref>{{cite news|title='A Rugrats Passover'|work=[[The Washington Post]]|author =Moore, Scott|date=1995-04-09}}</ref> the show was one of the first animated television series to produce a special for a Jewish holiday.<ref name="NYT"/> After production wrapped on "A Rugrats Passover", the crew considered creating the Chanukah special that Nickelodeon had originally pitched.<ref name="raise"/> The episode was written by {{nowrap|David Stem and [[David N. Weiss|David Weiss]]}}, and directed by Raymie Muzquiz.<ref name="main"/> By the time Weiss came to write the teleplay, he had abandoned [[Christianity]] and converted to [[Judaism]].<ref>{{cite journal|title=Shrek's Orthodox author|work=[[Jerusalem Post]]|author =Brown, Hannah|page=24|date=2005-05-18}}</ref>

[[Paramount Home Video]] finished production of the home media version in July 1997; originally scheduling a release date of October that year,<ref>{{cite journal|title=Coming Attractions: After a somewhat sleepy spring, video retailers can prepare to deck their shelves with sackfuls of third-and fourth-quarter releases that aim to satisfy entries on just about everybody's wish lists|work=[[Billboard (magazine)|Billboard]]|author =Olson, Catherine Applefield|page=60|date=1997-07-12}}</ref> Paramount instead pushed the [[VHS]] release into 1998 .<ref>{{cite journal|title=Rave Review|work=[[Sesame Workshop#Later|Sesame Street Magazine]]|page=31|year=2001}}</ref><ref>{{cite news|title=Videos, DVDs to stuff into stockings|author =Bassave, Roy|date=1998-12-15|publisher=[[Tribune News Service]]}}</ref> In time for Christmas 1997, Paramount released the video ''Nickelodeon Holiday'', which featured "A Rugrats Chanukah" and other holiday specials, such as "[[Hey Arnold!]]'s Christmas" for {{US$|12.95|1997}}.<ref>{{cite journal|title=Holi-disks for '97 marry Christmas to every conceivable musical genre|page=82|author =Block, Debbie Galante|work=[[Billboard (magazine)|Billboard]]|date=1997-08-23}}</ref><ref name="Inc.1997">{{cite journal|journal=[[Billboard (magazine)|Billboard]]|last1=McCormick|first1=Moria|last2=Garza|first2=Morella|title='Best Ever' Holiday Vids Due|url=https://books.google.com/books?id=CQoEAAAAMBAJ&pg=PA64|accessdate=21 December 2011|date=20 September 1997|publisher=Nielsen Business Media, Inc.|page=64|issn=0006-2510}}</ref> On August 31, 2004, Paramount also released a DVD compilation titled ''Rugrats Holiday Celebration'', which featured several holiday-themed episodes, including "A Rugrats Chanukah".<ref name="DVD">{{cite web|url=http://www.dvdtalk.com/reviews/12132/rugrats-holiday-celebration/|title=Rugrats Holiday Celebration|publisher=[[DVD Talk]]|author =Rizzo, Francis|accessdate=2009-10-25|date=2004-11-01}}</ref><ref>{{cite news|title=Small fry will enjoy new DVDs|author =Hicks, Chris|work=Deseret News|date=2004-11-06}}</ref> Sarah Willson adapted the episode into the book, ''The Rugrats' Book of Chanukah'', illustrated by Barry Goldberg and published by [[Simon & Schuster]] in 1997.<ref>{{cite news|title=The Rugrats' Book of Chanukah|work=[[Chicago Jewish Star]]|date=1997-12-18}}</ref>

==Reception==

===Critical response===
{|class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 95%; background:#ADD8E6; color:black; width:22em; max-width: 40%;" cellspacing="5"
|style="text-align: left;"|"The babies acting out their own version of the story is enough to entertain a child of any religious denomination, so learning the historical meaning behind latkes and dreidels is just an added bonus."
|-
|style="text-align: left;"|&nbsp;—''[[TV Guide]]''<ref name="guide">{{cite web|url=http://www.tvguide.com/PhotoGallery/Holiday-TV-Classics-67160/7.aspx|title=Holiday and Christmas TV Classics|work=[[TV Guide]]|page=7|accessdate=2009-10-25}}</ref>
|}
"A Rugrats Chanukah" was originally broadcast on December 4, 1996 on Nickelodeon. Repeated twice that night,<ref>{{cite news|title=Rites of Chanukah reach many|author =Ribadeneira, Diego|work=[[The Boston Globe]]|date=1996-12-05}}</ref> the episode received a [[Nielsen ratings|Nielsen rating]] of 7.9 in the show's target demographic of children aged 2–11.<ref>{{cite news|title=Nickelodeon drives kids TV marketplace in new season Grows While Competition Declines; Outperforms Broadcasters In Key Dayparts|date=1996-12-18|publisher=Press release, [[Viacom]]}}</ref> On December 1, 2001, [[CBS]] broadcast the episode for the first time on its network, at 8:30&nbsp;p.m. [[Eastern Time]]. Carrying a [[TV-Y]] parental rating, it followed the ''Rugrats'' Christmas special, "The Santa Experience".<ref>{{cite news|title=Remake of 'Brian's Song' sings without excess: ; Story of friendship, love and loss still a tear-jerker; even 29 years later|work=[[Charleston Daily Mail]]|date=2001-12-01|author =McDonough, Kevin}}</ref> Nickelodeon has aired the episode throughout subsequent holiday seasons.<ref>{{cite news|title=A special Christmas from the Grinch to Spongebob, the networks are offering all sorts of holiday-themed shows|work=[[Albany Times Union]]|date=2002-11-29|author1=McGuire, Mark |author2=Wiley, Casey }}</ref><ref>{{cite news|title=Nickelodeon's 'Ha-Ha Holidays' to Spread Chuckles and Cheer, December 5–29|work=[[PR Newswire]]|date=2005-11-08}}</ref><ref>{{cite news|title=Check out holiday TV offerings|author =Elber, Lynn|work=[[Post-Tribune]]|date=2005-12-16}}</ref>

"A Rugrats Chanukah" received positive reviews from television critics, and is one of the most popular episodes of ''Rugrats.''<ref>{{cite book|title=The Half-Jewish Book: A Celebration|author1=Klein, Daniel |author2=Vuijst, Freke |publisher=Villard|year=2000|isbn=0-375-50385-4|page=36}}</ref> Delia O'Hera of the ''[[Chicago Sun-Times]]'' called it a "multigenerational tale".<ref>{{cite news|title=Holidays are a good time for family video viewing|date=2000-12-08|work=[[Chicago Sun-Times]]}}</ref> Judith Pearl, in her book ''The Chosen Image: Television's Portrayal of Jewish Themes and Characters'', described the episode as a "fun [treatment] of Chanukah".<ref>{{cite book|title=The Chosen Image: Television's Portrayal of Jewish Themes and Characters|author =Pearl, Judith|year=1999|publisher=[[McFarland & Company]]|isbn=0-7864-0522-8|page=39}}</ref> Chuck Barney of [[Knight Ridder]] and the [[Tribune News Service]] considered the episode a "hilariously imaginative take on the Chanukah legend".<ref>{{cite news|title=Other holiday programming between now and Christmas|author =Barney, Chuck|date=2000-11-29|publisher=[[Knight Ridder]]/[[Tribune News Service]]}}</ref>

In a 1999 issue of ''[[TV Guide]]'', "A Rugrats Chanukah" was listed at number 5 in their "10 Best Classic Family Holiday Specials".<ref>{{cite news|title=10 Best Classic Family Holiday Specials|work=[[TV Guide]]|date=1999-11-27}}</ref> ''TV Guide'' later wrote that "Nickelodeon's ''Rugrats'' secured its place in television history" with the episode, opining that it could "entertain a child of any religious denomination".<ref name="guide"/> Ted Cox of the ''[[Daily Herald (Arlington Heights)|Daily Herald]]'' said that although the episode was not as good as the show's Passover special—which he considered "among the best holiday TV specials ever produced"—it was "still noteworthy".<ref>{{cite journal|title=Seasonal all-stars The 12 top TV specials of Christmas – and other winter holidays.|author =Cox, Ted|date=2005-12-01|work=[[Daily Herald (Arlington Heights)|Daily Herald]]}}</ref> [[DVD Talk]] reviewer Francis Rizzo&nbsp;III wrote that the special "has a great historical opening".<ref name="DVD"/> In ''Flickipedia: Perfect Films for Every Occasion, Holiday, Mood, Ordeal, and Whim'', Michael Atkinson and Laurel Shifrin said that the special was "...&nbsp;a richer meal, even, for parents than for tykes".<ref>{{cite book|title=Flickipedia: Perfect Films for Every Occasion, Holiday, Mood, Ordeal, and Whim|author1=Atkinson, Michael |author2=Shifrin, Laurel |page=12|publisher=Chicago Review Press|isbn=1-55652-714-4|year=2007}}</ref>

===Anti-Defamation League controversy===
"A Rugrats Chanukah", along with other ''Rugrats'' episodes featuring Boris and his wife, Minka, attracted controversy when the [[Anti-Defamation League]] (ADL) charged that the two characters resembled [[Antisemitism|anti-Semitic]] drawings that were featured in a 1930s [[Nazism|Nazi]] newspaper. Nickelodeon's then-president, [[Albie Hecht]] (himself Jewish), professed bewilderment and called the accusation absurd.<ref name="controversy">{{cite book|title=How The Left Lost Teen Spirit|author =Goldberg, Danny|year=2005|publisher=Akashic Books|page=216|isbn=0-9719206-8-0}}</ref> The controversy resurfaced in 1998 after the ADL made the same claims about Boris' appearance in a ''Rugrats'' comic strip that ran in newspapers during the [[Jewish New Year]]. The organization was also offended by the character's recitation of the [[Kaddish#Mourners.27 Kaddish|Mourner's Kaddish]] in the strip. Nickelodeon's new president, [[Herb Scannell]], agreed with the criticism and promised never to run the character or the strip again.<ref>{{cite news|url=http://www.awn.com/mag/issue3.9/3.9pages/3.9business.html|title=Rugrats Offends Media Watchdogs|work=[[Animation World Magazine]]|author1=Jackson, Wendy |author2=Amidi, Amid |date=December 1998|accessdate=2009-12-19}}</ref>

==See also==
{{Portal|Nickelodeon|Judaism|United States|Animation|Television}}
*[[1996 in American television]]
*[[Judaism in Rugrats]]
*[[List of Rugrats episodes]]
{{Clear}}

==References==
{{Reflist|30em}}

==External links==
*[http://www.imdb.com/title/tt1159155/ "A Rugrats Chanukah"] at the [[Internet Movie Database]]
*[http://www.tv.com/rugrats/chanukah/episode/76511/summary.html?tag=episode_header;summary "A Rugrats Chanukah"] at [[TV.com]]

===Video===
* [https://web.archive.org/web/20120108001633/http://www.nick.com:80/videos/clip/NTV_rug_chanukah_full.html Rugrats: "Rugrats Chanukah" Episode], [[Nick.com]]
* [https://web.archive.org/web/20111215140604/http://www.nick.com:80/videos/clip/chanukah-clip.html Rugrats Chanukah Clip], [[Nick.com]]

{{Rugrats}}

{{featured article}}

{{DEFAULTSORT:Rugrats Chanukah, A}}
[[Category:1996 in American television]]
[[Category:1996 television episodes]]
[[Category:1996 television specials]]
[[Category:1990s American television specials]]
[[Category:Hanukkah fiction]]
[[Category:Jewish-related television episodes]]
[[Category:Rugrats and All Grown Up! episodes]]