{{good article}}
{{Use mdy dates|date=January 2013}}
{{Infobox television episode
| title = 11001001
| series = [[Star Trek: The Next Generation]]
| season = 1
| episode =  15
| production = 116
| airdate = {{Start date|1988|02|01}}
| writer = {{Plainlist|
*[[Maurice Hurley (screenwriter)|Maurice Hurley]]
*[[Robert Lewin (screenwriter)|Robert Lewin]]  }}
| director = [[Paul Lynch (director)|Paul Lynch]]
| photographer = [[Edward R. Brown]]
| music = [[Ron Jones (composer)|Ron Jones]]
| guests =
* [[Carolyn McCormick]] as Minuet
* [[Eugene Dynarski|Gene Dynarski]] as Orfil Quinteros
* [[Katy Boyer]] as Zero One
* Alexandra Johnson as One Zero
* [[Iva Lane]] as Zero Zero
* [[Kelli Ann McNally]] as One One
* [[Jack Sheldon]] as Piano Player
* [[Abdul Salaam El Razzac]] as Bass Player
* Ron Brown as Drummer<ref name=gross163/>
| prev = [[Angel One]]
| next = [[Too Short a Season]]
| episode_list = [[List of Star Trek: The Next Generation episodes|List of ''Star Trek: The Next Generation'' episodes]]
}}

"'''11001001'''" is the 15th episode of the [[Star Trek: The Next Generation (season 1)|first season]] of the American [[science fiction]] television series ''[[Star Trek: The Next Generation]]''. It was first broadcast on February 1, 1988, in the United States in [[broadcast syndication]]. It was written by [[Maurice Hurley (screenwriter)|Maurice Hurley]] and [[Robert Lewin (screenwriter)|Robert Lewin]], and directed by [[Paul Lynch (director)|Paul Lynch]].

Set in the 24th century, the series follows the adventures of the crew of the [[Starfleet]] starship [[USS Enterprise (NCC-1701-D)|''Enterprise''-D]]. In this episode, members of an alien race called the [[Bynars]] hijack a nearly evacuated ''Enterprise'' while retrofitting the computer in space dock.

Make-up supervisor [[Michael Westmore]] created the look of the Bynars, who were four women in extensive make-up. The musical score was scored by [[Ron Jones (composer)|Ron Jones]]. Reviewers praised the Bynars themselves, and the response to the episode was generally positive, with one critic calling it the best of the season. It was awarded an [[Emmy Award]] for Outstanding Sound Editing for a Series.

==Plot==
The [[United Federation of Planets|Federation]] starship ''Enterprise'' arrives at [[Starbase (Star Trek)|Starbase]]&nbsp;74 for a routine maintenance check. Captain [[Jean-Luc Picard]] ([[Patrick Stewart]]) and Commander [[William Riker]] ([[Jonathan Frakes]]) greet Starbase Commander Quinteros ([[Eugene Dynarski|Gene Dynarski]]) and a pair of small humanoid aliens known as Bynars; the Bynars heavily rely on their computer technology and work in pairs for best efficiency. Much of the crew take shore leave while Picard, Riker and a skeleton crew remain aboard. Riker is intrigued by the Bynars' claimed upgrades to the [[holodeck]] and starts a program in a jazz bar. The program includes a woman named Minuet ([[Carolyn McCormick]]), by whom Riker is fascinated, both as a beautiful and charming woman, but also by the level of sophistication in her responses. Riker shortly returns, and Picard walks in on him kissing Minuet, and he too is amazed by the simulation.

Meanwhile, the Bynars discreetly create a catastrophic failure in the ship's warp core. Lt. Cmdr. [[Data (Star Trek)|Data]] ([[Brent Spiner]]) and [[Geordi La Forge]] ([[LeVar Burton]]) are unable to locate Picard or Riker and, assuming them to already be on the Starbase, order an emergency evacuation. They set the ship to leave the Starbase and warp to a safe location before it would explode. However, once they are clear of the dock, the failure disappears and the ship sets course for the Bynar system, the planet Bynaus orbiting Beta Magellan. Data, La Forge, and Quinteros realize that the Bynars are still aboard the ship, but there are currently no other working vessels to follow them. Back on the ''Enterprise'', Riker and Picard leave the simulation to find the ship empty and at warp to the Bynar system, with the ship's controls locked to the bridge. Fearing that the Bynars have taken over the ship for nefarious purposes, they set the ship to self-destruct in 5 minutes and then take the bridge by inter-ship [[transporter (Star Trek)|transporter beam]] and find the Bynars there unconscious.

After cancelling the self-destruct, they find the Bynars have uploaded massive amounts of information to the ''Enterprise'' computers, but they are unable to decode it. Realizing that Minuet was purposely created by the Bynars as a distraction, Picard and Riker ask the simulation about what is going on as the ship nears the orbit of Bynar. Minuet explains that a star near the Bynar homeworld had gone supernova, and the [[Electromagnetic pulse|EMP]] it emitted would knock out their computer systems, effectively killing the Bynars. They had used the ''Enterprise'' to upload their computer information for safekeeping and then planned to download it back to the Bynar computers after the threat of the EMP had passed. With Data's help, Picard and Riker successfully download the data, and the Bynars recover. They apologize for their actions, having feared that [[Starfleet]] would refuse to help, though Picard notes they only had to ask. As the ''Enterprise'' returns to Starbase, Riker returns to the holodeck to thank Minuet but finds that without the Bynar data, the simulation has regressed to the expected norm for the holodeck, and while Minuet still exists, she is not the same as before. Riker reports to Picard that Minuet is gone.

==Production==
[[File:ST-TNG 11001001.jpg|thumb|left|The Bynars were portrayed by young female actresses in extensive make-up.]]
The episode at one point was called "10101001".<ref>{{cite web|last=Hurley|first=Maurice|author2=Lewin, Robert|title=Star Trek: The Next Generation "11001001"|url=http://www.st-minutiae.com/academy/literature329/116.txt|publisher=Paramount Pictures|accessdate=March 4, 2013}}</ref> It was originally intended that this episode would take place prior to "[[The Big Goodbye]]", with the Bynars' modifications causing the problems with the [[holodeck]] seen in that episode. Instead it was changed to the Bynars aiming to fix the holodeck to prevent those problems re-occurring.<ref name=decandido/> The Bynars themselves were played by young women.<ref name=gross163>[[#grossaltman1993|Gross; Altman (1993)]]: p. 163</ref> Children were considered for the parts, but the production team thought that it would be too problematic because of the limited time they could work each day and the need to hire teachers. Each actress was required to wear extensive make-up, which was created by make-up supervisor [[Michael Westmore]]. A large single-piece bald cap was made from the same mold for each actress, which required some customised trimming to get it to fit properly. To cover up problems with the seams of the cap, some fake hair was added on the Bynars' necks.<ref name=westmore>[[#westmorenazzaro1993|Westmore; Nazzaro (1993)]]: p. 59</ref> Each actress also controlled the flashing light inside the apparatus on the side of the headpiece through a battery pack attached to the waistband of their costumes.<ref name=westmore/> To disguise their voices, the pitch was lowered in post production. It was originally planned to add subtitles over the Bynars' conversations between themselves.<ref name=nemecek48>[[#Nemecek2003|Nemecek (2003)]]: p. 48</ref>

The image of the [[Starbase]] orbiting a planet was re-used from ''[[Star Trek III: The Search for Spock]]''.<ref name=nemecek48/> The score was created by [[Ron Jones (composer)|Ron Jones]], who incorporated jazz themes composed by [[John Beasley (musician)|John Beasley]].<ref name=score>{{cite AV media notes|title=Star Trek: The Next Generation: The Ron Jones Project |titlelink= |others=Ron Jones |year=2010 |chapter= |url=http://www.filmscoremonthly.com/notes/box05_disc02.html |accessdate=February 15, 2013 |first= |last= |authorlink= |first2= |last2= |authorlink2= |page= |pages= |type= |publisher=Film Score Monthly |id= |location= |deadurl=no |archiveurl=https://web.archive.org/web/20160304050852/http://www.filmscoremonthly.com/notes/box05_disc02.html |archivedate=March 4, 2016 |df=mdy }}</ref> The episode was written by [[Maurice Hurley (screenwriter)|Maurice Hurley]] and [[Robert Lewin (screenwriter)|Robert Lewin]]. Hurley was pleased with the outcome of the episode, praising the work of Westmore on the Bynars' makeup and the performance of Jonathan Frakes. Frakes enjoyed the episode too, saying, "A fabulous show. Those were the kind of chances we took first season that when they worked, they worked great. It was a very chancy show and I loved it."<ref name=gross163/> Director [[Paul Lynch (director)|Paul Lynch]] also thought that the Bynars were "great".<ref name=gross163/> [[Carolyn McCormick]] appeared as Minuet and subsequently became a regular cast member in [[Law & Order|Law and Order]].<ref name=gross163/> She returned to the role of Minuet in the [[Star Trek: The Next Generation (season 4)|season four]] episode "[[Future Imperfect]]". [[Eugene Dynarski|Gene Dynarski]] had previously appeared as Ben Childress in ''[[Star Trek: The Original Series]]'' episode "[[Mudd's Women]]" and Krodak in "[[The Mark of Gideon]]".<ref name=nemecek48/>

==Reception==
"11001001" aired in [[broadcast syndication]] during the week commencing February 7, 1988. It received [[Nielsen rating]]s of 10.7, reflecting the percentage of all households watching the episode during its timeslot. These ratings were lower than both the episodes broadcast both before and afterwards.<ref>{{cite web|title=Star Trek: The Next Generation Nielsen Ratings – Seasons 1–2|url=http://treknation.com/nielsens/tng/season12.shtml|website=''TrekNation''|publisher=UGO Networks|accessdate=June 12, 2016|archiveurl=https://web.archive.org/web/20001005185203/http://treknation.com/nielsens/tng/season12.shtml|archivedate=October 5, 2000}}</ref> For their work in this episode, Bill Wistrom, Wilson Dyer, Mace Matiosian, James Wolvington, Gerry Sackman and Keith Bilderbeck were awarded an [[Emmy Award]] for Outstanding Sound Editing for a Series.<ref name=emmydatabase>{{cite web|title=Primetime Emmy Award Database|url=http://www.emmys.com/award_history_search|publisher=Emmys.com|accessdate=February 9, 2013}}</ref>

Several reviewers re-watched the episode after the end of the series. James Hunt reviewed the episode for the website "[[Den of Geek]]" in January 2013. He was surprised by the episode as he "went in expecting something that was typically season one awful, and got something that was actually a lot of fun".<ref name=denofgeekreview>{{cite news|last=Hunt|first=James|title=Revisiting Star Trek TNG: 11001001|url=http://www.denofgeek.com/tv/star-trek-the-next-generation/24054/revisiting-star-trek-tng-11001001|accessdate=March 4, 2013|newspaper=''Den of Geek''|date=January 11, 2013}}</ref>  He thought that the theme of symbiosis between man and machine was "interesting", stating it was the best episode up until that point in season one and one of the best of the season overall.<ref name=denofgeekreview/> Zack Handlen watched the episode for ''[[The A.V. Club]]'' in May 2010. He too was surprised by what he found. Handlen said that "last time I saw it, I thought Riker and Minuet's interactions were cheesy as hell. They didn't bother me so much now, because they don't go on very long, and there's something hilarious about a man trying to seduce a computer simulation designed to respond to his seductions".<ref name=avclub>{{cite news|last=Handlen|first=Zack|title="11001001"/"Too Short A Season"/"When The Bough Breaks"|url=http://www.avclub.com/articles/11001001too-short-a-seasonwhen-the-bough-breaks,40903/|accessdate=March 4, 2013|newspaper=The A.V. Club|date=May 7, 2010}}</ref> But he said, "I had fun with this, which I wasn't expecting",<ref name=avclub/> and "thought this was solid".<ref name=avclub/> He gave the episode a B grade.<ref name=avclub/>

[[Keith DeCandido]] reviewed the episode for [[Tor Books|Tor.com]] in June 2011. He described it as "one of the strongest first-season episodes",<ref name=decandido>{{cite web|last=DeCandido|first=Keith|title=Star Trek: The Next Generation Rewatch: "11001001"|url=http://www.tor.com/blogs/2011/06/star-trek-the-next-generation-rewatch-q11001001q|publisher=Tor.com|date=June 23, 2011|accessdate=March 4, 2013}}</ref> and the Bynars as "one of the finest alien species ''Trek'' has provided".<ref name=decandido/> He also thought that turning off the auto-destruct with two minutes to go instead of mere seconds neatly avoided a cliché, and gave it a score of seven out of ten.<ref name=decandido/> Michelle Erica Green for [[TrekNation]] watched the episode in June 2007. She thought that it came "very close to being a really good episode".<ref name=greenreview>{{cite web|last=Green|first=Michelle Erica|title=11001001|url=http://www.trektoday.com/reviews/tng/11001001.shtml|publisher=TrekNation|date=June 8, 2007|accessdate=March 4, 2013}}</ref> She also thought that Picard and Riker's actions were the "most boneheaded joint behavior by the top two officers", in that they got distracted by a female character on the holodeck and didn't notice the ship being evacuated.<ref name=greenreview/> Jamahl Epsicokhan at his website "Jammer's Reviews" described "11001001" as "easily season one's best and most memorable episode".<ref name=jammer>{{cite web|last=Epsicokhan|first=Jamahl|title=Star Trek: The Next Generation "11001001"|url=http://www.jammersreviews.com/st-tng/s1/11001001.php|publisher=Jammer's Reviews|accessdate=March 4, 2013}}</ref> He thought that it was the "season's most solid sci-fi concept" and that the series was "firing on all cylinders, with everything coming together, from plot to character, to sensible use of technology and action".<ref name=jammer/> He gave it a score of four out of four.<ref name=jammer/>

==Home media release==
The first home media release of "11001001" was on [[VHS]] cassette, appearing on August 26, 1992, in the United States and Canada.<ref>{{cite web|title=Star Trek - The Next Generation, Episode 16: 11001001 [VHS]|url=http://www.tower.com/star-trek-next-generation-episode-16-11001001-patrick-stewart-vhs/wapi/109139375|publisher=Tower Video|accessdate=March 5, 2013}}</ref> The episode was later included on the ''Star Trek: The Next Generation'' season one [[DVD]] box set, released in March 2002,<ref>{{cite news|last=Periguard|first=Mark A|title='Life as a House' rests on shaky foundation|url=http://www.highbeam.com/doc/1G1-84129654.html|accessdate=October 13, 2012|newspaper=The Boston Herald|date=March 24, 2002}} {{Subscription required}}</ref> and then released as part of the season one [[Blu-ray Disc|Blu-ray]] set on July&nbsp;24, 2012.<ref>{{cite news|last=Shaffer|first=RL|title=Star Trek: The Next Generation Beams to Blu-ray|url=http://uk.ign.com/articles/2012/04/30/star-trek-the-next-generation-beams-to-blu-ray|accessdate=17 October 2012|newspaper=IGN|date=April 30, 2012}}</ref>

==Notes==
{{reflist|30em}}

==References==
{{refbegin}}
*{{cite book|last=Gross|first=Edward|author2=Altman, Mark A.|title=Captain's Logs: The Complete Trek Voyages|year=1993|publisher=Boxtree|location=London|isbn=978-1-85283-899-7|ref=grossaltman1993}}
*{{cite book|last=Westmore|first=Michael G|author2=Nazzaro, Joe|title=Star Trek: The Next Generation Make-Up FX Journal|year=1993|publisher=Titan|location=London|isbn=978-1-85286-491-0|ref=westmorenazzaro1993}}
*{{cite book|last=Nemecek|first=Larry|title=Star Trek: The Next Generation Companion|year=2003|edition=3rd|publisher=Pocket Books|location=New York|isbn= 0-7434-5798-6|ref=Nemecek2003}}
{{refend}}

==External links==
{{portal|Star Trek}}
{{wikiquote|Star Trek: The Next Generation#11001001 .5B1.15.5D|"11001001"}}
* {{IMDb episode|0708668}}
* {{tv.com episode|star-trek-the-next-generation/11001001-19002}}
{{Memoryalpha|11001001|"11001001"}}
{{StarTrek.com link|TNG|68338|"11001001"}}

{{Star Trek holodeck stories}}
{{Star Trek TNG S1}}
{{Star Trek TNG}}
{{Star Trek}}

[[Category:1988 American television episodes]]
[[Category:Emmy Award-winning episodes]]
[[Category:Holography in television]]
[[Category:Star Trek: The Next Generation episodes]]