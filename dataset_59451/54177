{{Use dmy dates|date=June 2014}}
{{Use Australian English|date=June 2014}}
'''Alexander George Downer''' (28 January 1839 &ndash; 17 August 1916), usually known as George, or A. G. Downer, was a prominent South Australian businessman and a partner with his brother Sir [[John Downer]] in the legal firm G & J Downer.<ref name=splendid>{{cite news |url=http://nla.gov.au/nla.news-article87312225 |title=A Splendid Citizen |newspaper=[[The Chronicle (Adelaide)|The Chronicle]] |location=Adelaide |date=19 August 1916 |accessdate=13 November 2012 |page=37 |publisher=National Library of Australia}}</ref>

==History==
George Downer was born in Adelaide, the son of [[Downer family|Henry Downer]]. He was educated at Francis Haire's Academy and while quite young was articled to the firm of Bartley, Bakewell & Stow, whose principals included [[Randolph Isham Stow]]. He combined study of law, for which he had a ready talent, with journalism, and was for a time editor of the (Adelaide) ''Telegraph''. He was admitted to the bar in 1868. His brother John had only recently qualified and the two entered into partnership. The firm G & J Downer prospered and was eventually taken over by Frank H. and J. Fred Downer, sons of his brothers Henry Edward and John respectively.

His only attempt at a position in Parliament was in 1870 when he contested the seat of Gumeracha against [[Ebenezer Ward]] and [[Arthur Blyth]]. He was unsuccessful.<ref>{{cite news |url=http://nla.gov.au/nla.news-article60325086 |title=A Versatile Personality |newspaper=[[The Register (Adelaide)|The Register]] |location=Adelaide |date=9 October 1917 |accessdate=14 November 2012 |page=6 |publisher=National Library of Australia}}</ref>

Around 1880 he was appointed by the government to a Pastoral Commission and was in a large part responsible for lengthening the tenure of leases of Crown lands.

He never married. After his death at his (ca.170) South Terrace residence, his Adelaide Hills property "Monalta" (previously known as "Hope Lodge")<ref>{{cite news |url=http://nla.gov.au/nla.news-article57510354 |title=Missionary Students Migrating |newspaper=[[The Register (Adelaide)|The Register]] |location=Adelaide |date=7 May 1907 |accessdate=14 November 2012 |page=4 |publisher=National Library of Australia}}</ref> near the [[Belair, South Australia|Belair]] railway station, was subdivided. The mansion eventually became Blackwood District Community Hospital.<ref>{{cite news |url=http://nla.gov.au/nla.news-article48932986 |title=Out Among the People. |newspaper=[[The Advertiser (Adelaide)|The Advertiser]] |location=Adelaide |date=18 November 1953 |accessdate=14 November 2012 |page=4 |publisher=National Library of Australia}}</ref> Sir John, his brother, had predeceased him on 2 August 1915 and his sister Amelia Rivaz had died on 21 July 1916.

==Business activities==
He was much in demand for his business acumen as well as his knowledge of mercantile law. For around twenty years he was a
*director of China Traders Company
*trustee of [[Harrold Brothers]]
*director, Norwich Union Assurance Company
*director, [[Elder, Smith, & Co.]] from 1892<ref>{{cite news |url=http://nla.gov.au/nla.news-article48174231 |title=Elder,Smith & Co., Limited |newspaper=[[The Advertiser (Adelaide)|The Advertiser]] |location=Adelaide |date=1 September 1936 |accessdate=14 November 2012 |page=32 |publisher=National Library of Australia}}</ref>
*director [[Bank of Adelaide]] from around 1894 and Chairman for most of that time
He relinquished these positions in May 1914 as his health deteriorated.
*He invested in a northern pastoral property which was resumed by the Government, then in 1888 a share of Minburra station and an adjoining property, making Melton Station, about 40&nbsp;km from [[Yunta, South Australia|Yunta]].

== References ==
{{Reflist}}

{{DEFAULTSORT:Downer, George}}
[[Category:Australian people of English descent]]
[[Category:Australian businesspeople]]
[[Category:Australian lawyers]]
[[Category:Downer family]]
[[Category:1839 births]]
[[Category:1916 deaths]]