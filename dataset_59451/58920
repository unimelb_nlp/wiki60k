{{Infobox journal
| title = Microwave and Optical Technology Letters
| cover =
| editor = Kai Chang
| discipline = [[Electromagnetic wave|High frequency]] technologies 
| formernames =
| abbreviation = Microw. Opt. Technol. Lett.
| publisher = [[Wiley-Blackwell]]
| country = 
| frequency = Monthly
| history = 1988-present
| openaccess =
| license =
| impact = 0.585
| impact-year = 2013
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1098-2760
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1098-2760/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1098-2760/issues
| link2-name = Online archive
| JSTOR =
| OCLC = 38552382
| LCCN = 88640906
| CODEN =
| ISSN = 0895-2477
| eISSN = 1098-2760
}}
'''''Microwave and Optical Technology Letters''''' is a monthly [[peer-reviewed]] [[scientific journal]] published by [[Wiley-Blackwell]]. The [[editor-in-chief]] is [[Kai Chang]] ([[Texas A&M University]]). The journal covers technology that operates in [[wavelengths]] ranging from [[radio frequency]] to the [[EM spectrum|optical domain]].

== Abstracting and indexing ==
This journal is abstracted and indexed in:<ref name=overview-page>{{cite web |url=http://www3.interscience.wiley.com/journal/37176/home/ProductInformation.html |title=Microwave and Optical Technology Letters - Wiley InterScience - Home page|work= |accessdate=2013-07-22}}</ref>
{{Columns-list|colwidth=30em|
* [[Science Citation Index Expanded]]
* [[Scopus]]
* [[Academic Search]] 
* [[Ceramic Abstracts]]
* [[Compendex]]
* [[Advanced Polymer Abstracts]]
* [[Civil Engineering Abstracts]]
* [[Mechanical & Transportation Engineering Abstracts]]
* [[EBSCO|Current Abstracts]]
* [[Current Contents]]/Engineering, Computing & Technology
* [[Engineered Materials Abstracts]] 
* [[INSPEC]]
* [[Computer & Information Systems Abstracts]]
* [[CSA (database company)|Materials Business File]]
* [[METADEX]]
* [[Earthquake Engineering Abstracts]]
* [[International Aerospace Abstracts & Database]]
* [[Technology Research Database]]
* [[Computer information & Technology Abstracts]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 0.585.<ref name=WoS>{{cite book |year=2014 |chapter=Microwave and Optical Technology Letters |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
*{{Official website| http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1098-2760}}

[[Category:Publications established in 1988]]
[[Category:English-language journals]]
[[Category:Optics journals]]
[[Category:Monthly journals]]
[[Category:John Wiley & Sons academic journals]]