{{Infobox company
| name             = Check Into Cash
| logo             = [[Image:Check Into Cash logo.png|160px|Check Into Cash]]
| type             = Privately Held Company
| industry         = [[Financial Services]]
| foundation       = 1993
| headquarters     = [[Cleveland, Tennessee]]
| area_served      = USA, UK
| key_people       = [[Allan Jones (businessman)|W. Allan Jones]]<br /><small>(Chairman/CEO)</small>
Steve Scoggins<br /><small>(President/COO)</small>
| website          = {{URL|www.checkintocash.com}}
}}
'''Check Into Cash''' is a financial services retailer with more than 1,100 stores in 30 states.<ref name=chatt>{{cite web|url=http://www.chattanoogan.com/2013/6/28/254153/Check-Into-Cash-Reaches-20th-Milestone.aspx|title=Check Into Cash Reaches 20th Milestone - 06/28/2013 - Chattanoogan.com|publisher=chattanoogan.com|accessdate=2014-03-16}}</ref><ref name=times>{{cite web|url=http://www.timesfreepress.com/news/2010/jun/21/payday-lenders-fear-federal-regulations/|title=Payday lenders fear federal regulations &#124; Times Free Press|publisher=timesfreepress.com|accessdate=2014-03-16}}</ref> The company was founded in 1993 by [[Allan Jones (businessman)|W. Allan Jones]] in [[Cleveland, Tennessee]], where the headquarters are located today.<ref name=chatt />

The firm offers [[payday loans]], online payday advances, [[title loans]], bill payment services, check cashing, reloadable [[debit card|prepaid debit cards]], and [[Western Union]] money transfers and [[money order]] services.<ref name="bloomberg">{{cite web|url=https://www.bloomberg.com/quote/CHEK:US/profile|title=CHEK Profile & Executives - Check Into Cash Inc - Bloomberg|publisher=bloomberg.com|accessdate=2014-03-16}}</ref>

== History ==
[[File:Check Into Cash store.jpg|thumb|Check Into Cash store]]

Jones founded Check Into Cash in 1993. He has been referred to as "the father of the payday lending industry" for creating the first national payday lending chain.<ref name=times /><ref name=harpers>{{cite web|url=http://harpers.org/archive/2009/04/usury-country/|title=Usury Country &#124; Harper&#039;s Magazine|publisher=harpers.org|accessdate=2014-03-16}}</ref>
In 1973, at age 20, he left college, where he had been pursuing a business degree, to help stabilize the family’s business, the [[Credit Bureau]] of Cleveland (TN). He purchased the reporting and debt collection business in 1977 and built it into one of the largest credit bureau databases in Tennessee.<ref name=chatt />  He sold the credit reporting side of the business to Equifax in 1988, retaining the name and collection agency division. He then built the company to be the largest in the state, and sold it in 1998.<ref name=chatt />

Check Into Cash has grown to become one of the largest payday loan companies in the United States, with over 1,200 locations.<ref name=harpers />

In 2012, The firm acquired Cash and Cheque Express, a consumer financial services chain in the United Kingdom.<ref name="clevelandbanner">{{cite web|url=http://www.clevelandbanner.com/view/full_story/19207497/article-CIC-buys-overseas-business|title=Cleveland Daily Banner - CIC buys overseas business|publisher=clevelandbanner.com|accessdate=2014-03-16}}</ref>
In 2013, the firm acquired Great American Pawn and Title and Quic!oans, both based in Georgia, plus Great American Cash Advance and Nations Quick Cash Title Pawn, which operate in Mississippi, Alabama, and Tennessee.<ref name="myajc">{{cite web|url=http://www.myajc.com/news/business/check-into-cash-expanding-pawnshop-reach-in-metro-/nbJgK/|title=Check Into Cash expanding pawnshop reach in metro Atlanta &#124; www.myajc.com|publisher=myajc.com|accessdate=2014-03-16}}</ref> Check Into Cash has also acquired Title First Title Pawn, which is based in Georgia.<ref name="clevelandbanner2">{{cite web|url=http://www.clevelandbanner.com/pages/full_story/push?article-Check+Into+Cash+Title+Pawn++acquires+Georgia+locations%20&id=23496667|title=Cleveland Daily Banner - Check Into Cash Title Pawn  acquires Georgia locations|publisher=clevelandbanner.com|accessdate=2014-03-16}}</ref>

== Other organizations ==

Check Into Cash is a founding member of the [[Community Financial Services Association of America]], which sets best practices standards for its members in the payday advance industry.<ref name="cfsaa">{{cite web|url=http://cfsaa.com/about-cfsa/2013-cfsa-corporate-members.aspx|title=CFSA > About CFSA|publisher=cfsaa.com|accessdate=2014-03-16}}</ref>
The firm has affiliate companies that operate under these brand names:<ref name="jonesmanagement">{{cite web|url=http://www.jonesmanagement.com/our_companies.htm|title=Our Companies|publisher=jonesmanagement.com|accessdate=2014-03-16}}</ref>
*U.S. Money Shops
*U.S. Money Title Loan
*U.S. Money Shops Pawn
*Check Into Cash Title Pawn
*LoanByPhone.com
*Title First Title Pawn
*Great American Pawn and Title
*Great American Cash Advance
*Quic!oans
*Nations Quick Cash Title Pawn

== See also ==
*[[Alternative financial services in the United States]]

== References ==

{{Reflist}}

== External links ==
*[http://checkintocash.com/ Official Website]
*[http://local.checkintocash.com/ Check Into Cash Locations]
*[http://loanbyphone.com/ LoanByPhone]  A wholly owned subsidiary of Check Into Cash, Inc
*[https://www.google.com/maps/d/viewer?mid=z0aZTOTZlHTY.kOCMVj_QNL1k Check Into Cash on the Map]

[[Category:Financial services companies of the United States]]
[[Category:Companies based in Tennessee]]
[[Category:Privately held companies based in Tennessee]]
[[Category:Companies established in 1993]]