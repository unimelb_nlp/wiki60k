{{other hurricanes|Tropical Storm Barry|the Atlantic tropical storm of 2007}}
{{Infobox Hurricane
| Name=Tropical Storm Barry (2007)
| Type=Tropical storm
| Year=2007
| Basin=Atl
| Image location=Barry 01 jun 2007 1905Z.jpg
| Image name=Tropical Storm Barry shortly after being classified
| Formed=June 1, 2007
| Dissipated=June 5, 2007
| Extratropical=June 2, 2007
| 1-min winds=50
| Pressure=997
| Damages=0.118
| Fatalities=1&nbsp;direct, 2&nbsp;indirect
| Areas=[[El Salvador]], western [[Cuba]], [[Florida]], [[East Coast of the United States]]
| Hurricane season=[[2007 Atlantic hurricane season]]
}}
'''Tropical Storm Barry''' was a rapidly forming [[tropical cyclone]] that made [[landfall (meteorology)|landfall]] on [[Florida]], [[United States]], in early June 2007. The second [[tropical cyclone|named storm]] of the [[2007 Atlantic hurricane season]], Barry developed from a [[trough (meteorology)|trough]] of low pressure in the southeastern [[Gulf of Mexico]] on June 1. It tracked rapidly northeastward, reaching peak winds of 60&nbsp;mph (95&nbsp;km/h) before weakening and making landfall near [[Tampa Bay]] as a tropical depression. Barry quickly lost tropical characteristics after [[wind shear]] removed much of the convection, and early on June 3 it completed the transition into an [[extratropical cyclone]]. The extratropical remnants tracked up the [[East Coast of the United States]], and were absorbed by a larger extratropical cyclone on June 5.

The precursor trough produced heavy rainfall across the western Caribbean Sea, which on [[Cuba]] unofficially reached over 7.8&nbsp;inches (200&nbsp;mm). Outer [[tropical cyclone#Physical structure|rainbands]] in [[Pinar del Río Province]] injured three people and damaged 55&nbsp;houses. In [[Florida]], Barry dropped a moderate amount of precipitation across the drought-ridden state; rainfall peaked at 6.99&nbsp;inches (178&nbsp;mm). The rain caused some flooding and wet roads, which led to two indirect traffic fatalities. Rough seas killed one Florida surfer in [[Pinellas County, Florida|Pinellas County]]. In Florida and [[Georgia (U.S. state)|Georgia]], the precipitation assisted firefighters in combating severe [[2007 Georgia/Florida wildfires|wildfires]]. Overall damage from the storm was minor.

==Meteorological history==
{{storm path|Barry 2007 track.png}}
By late on May 29, a weak [[trough (meteorology)|trough]] over the [[Yucatán Peninsula]] produced a small area of [[convection]] over the [[Yucatán Channel]].<ref name="529twd">{{cite web|author=Waddington |date=2007-05-29 |title=May 29 Tropical Weather Discussion |publisher=National Hurricane Center |accessdate=2007-06-01 |url=ftp://ftp.met.fsu.edu/pub/weather/tropical/Atl-Dis/2007052917.AXNT20 }}{{dead link|date=January 2017 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> Convection increased in association with the trough, and the next day a broad envelop of cyclonic turning developed within the system. By May 30, the moisture from the trough extended from [[Nicaragua]] through the southeastern [[Gulf of Mexico]], with the greatest area of convection near [[Cuba]].<ref name="530twd">{{cite web|author=John Cangialosi |year=2007 |title=May 30 Tropical Weather Discussion |publisher=National Hurricane Center |accessdate=2007-06-01 |url=ftp://ftp.met.fsu.edu/pub/weather/tropical/Atl-Dis/2007053017.AXNT20 }}{{dead link|date=January 2017 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> A westward moving [[tropical wave]] spawned a broad area of low pressure on May 30, and by May 31 a circulation developed within the system to the southeast of [[Cozumel]], Mexico. The low moved north-northeastward,<ref name="tcr">{{cite web|author=Lixion Avila|year=2007|title=Tropical Storm Barry Tropical Cyclone Report|publisher=National Hurricane Center|accessdate=2007-06-22|url={{NHC TCR url|id=AL022007_Barry}}|format=PDF}}</ref> and gradually became better organized despite high amounts of vertical [[wind shear]].<ref name="531twd2">{{cite web|author=John Cangialosi |date=2007-05-31 |title=May 31 Tropical Weather Discussion 17z |publisher=National Hurricane Center |accessdate=2007-06-01 |url=ftp://ftp.met.fsu.edu/pub/weather/tropical/Atl-Dis/2007053117.AXNT20 }}{{dead link|date=January 2017 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> The deep convection became more concentrated near the center, and it is estimated the system developed into a tropical depression at 1200&nbsp;[[UTC]] on June 1 just to the northwest of the western tip of [[Cuba]]. Operationally, it was not classified until eleven hours later.<ref name="tcr"/>

[[Image:TS Barry 2007 Tampa radar.gif|thumb|right|Animated radar image from [[Tampa, Florida|Tampa]] of Tropical Storm Barry passing through [[Florida]] on June 2]]
The depression developed a large area of squalls, and organized enough to warrant a [[Hurricane Hunters]] flight into the area.<ref name="61two">{{cite web|author=Lixion Avila |date=2007-06-01 |title=June 1 Tropical Weather Outlook 15z |publisher=National Hurricane Center |accessdate=2007-06-01 |url=ftp://ftp.met.fsu.edu/pub/weather/tropical/Outlook-A/2007060115.ABNT20 }}{{dead link|date=January 2017 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> The plane reported flight level winds of {{convert|60|mi/h|km/h|abbr=on}} and a pressure of 1000&nbsp;[[Bar (unit)|mbar]] near the increasingly well-defined low-level circulation. Initially it maintained characteristics of both a tropical and [[subtropical cyclone]], although deep convection continued to organize near the center; based on the observations, the [[National Hurricane Center]] initiated advisories on Tropical Storm Barry at 2100&nbsp;[[UTC]] on June 1 while the storm was centered about 235&nbsp;miles (375&nbsp;km) west of [[Key West, Florida]].<ref name="disc1">{{cite web|author=Lixion Avila|date=2007-06-01|title=Tropical Storm Barry Discussion One|publisher=National Hurricane Center|accessdate=2007-06-01|url=http://www.nhc.noaa.gov/archive/2007/al02/al022007.discus.001.shtml}}</ref> Embedded within the southwesterly flow ahead of an approaching mid-level trough, it tracked quickly northward,<ref name="disc1"/> and early on June 2 attained peak winds of 60&nbsp;mph (95&nbsp;km/h).<ref name="tcr"/>

Shortly after reaching peak intensity, strong wind shear removed most of the deep convection; the cloud pattern consisted of an exposed yet well-defined center surrounded by a curved convective band extending from Cuba along the eastern Gulf of Mexico.<ref name="disc2">{{cite web|author=Lixion Avila|date=2007-06-02|title=Tropical Storm Barry Discussion Two|publisher=National Hurricane Center|accessdate=2007-06-02|url=http://www.nhc.noaa.gov/archive/2007/al02/al022007.discus.002.shtml}}</ref> The center became elongated and weakened as it accelerated northeastward, and at 1400&nbsp;UTC on June 2 Barry made [[landfall (meteorology)|landfall]] near [[Tampa, Florida]], as a weakening tropical depression.<ref name="disc4">{{cite web|author=Lixion Avila and Xavier William Proenza|date=2007-06-02|title=Tropical Storm Barry Discussion Four|publisher=National Hurricane Center|accessdate=2007-06-02|url=http://www.nhc.noaa.gov/archive/2007/al02/al022007.discus.004.shtml}}</ref> As it continued inland, it rapidly lost tropical characteristics, and later that day the National Hurricane Center discontinued advisories on Barry while it was located over northeastern Florida.<ref name="disc5">{{cite web|author=Lixion Avila|year=2007|title=Tropical Storm Barry Discussion Five|publisher=National Hurricane Center|accessdate=2007-06-01|url=http://www.nhc.noaa.gov/archive/2007/al02/al022007.discus.005.shtml}}</ref> The [[extratropical cyclone|extratropical]] remnants strengthened as the system continued northeastward, and on June 3 it moved ashore along [[South Carolina]].<ref name="hpcpa7">{{cite web|author=Rubin-Oster|year=2007|title=Public Advisory Number 7 for Remnants of Barry|publisher=Hydrometeorological Prediction Center|accessdate=2007-06-03|url=http://www.wpc.ncep.noaa.gov/tropical/2007/BARRY/BARRY_7.html}}</ref> Spiral bands developed to the north of the system as it moved up the coast, and a large plume of moisture extended well ahead of the low-level circulation.<ref name="hpcpa9">{{cite web|author=Kong|date=2007-06-03|title=Public Advisory Number 9 for Remnants of Barry|publisher=Hydrometeorological Prediction Center|accessdate=2007-06-03|url=http://www.wpc.ncep.noaa.gov/tropical/2007/BARRY/BARRY_9.html}}</ref> Late on June 4, the extratropical remnant entered [[New England]],<ref name="hpcpa13">{{cite web|author=Fracasso|date=2007-06-03|title=Public Advisory Number 13 for Remnants of Barry|publisher=Hydrometeorological Prediction Center|accessdate=2007-06-03|url=http://www.wpc.ncep.noaa.gov/tropical/2007/BARRY/BARRY_13.html}}</ref> and late on June 5 the remnants of Barry were absorbed by a larger extratropical cyclone near the border between the [[U.S. state]] of [[Maine]] and the [[Provinces and territories of Canada|Canadian province]] of [[Quebec]].<ref name="tcr"/>

==Preparations==
[[Image:TD Barry.jpg|thumb|right|Barry as a tropical depression shortly after landfall in Florida]]
Coinciding with its classification as a tropical storm, the [[National Hurricane Center]] issued a [[tropical cyclone warnings and watches|tropical storm warning]] for the west coast of [[Florida]] from [[Bonita Beach, Florida|Bonita Beach]] through [[Keaton Beach, Florida|Keaton Beach]], with a tropical storm watch declared from Keaton Beach northward to [[Saint Marks, Florida|Saint Marks]].<ref name="pa1a">{{cite web|author=Avila|year=2007|title=Tropical Storm Barry Public Advisory One-A|publisher=National Hurricane Center|accessdate=2007-09-25|url=http://www.nhc.noaa.gov/archive/2007/al02/al022007.public_a.001.shtml?}}</ref> An inland tropical storm warning was also issued for non-coastal and non-tidal areas of [[Charlotte County, Florida|Charlotte]], [[Citrus County, Florida|Citrus]], [[DeSoto County, Florida|DeSoto]], [[Hardee County, Florida|Hardee]], [[Hernando County, Florida|Hernando]], [[Highlands County, Florida|Highlands]], [[Lee County, Florida|Lee]], [[Levy County, Florida|Levy]], [[Manatee County, Florida|Manatee]], [[Pasco County, Florida|Pasco]], [[Pinellas County, Florida|Pinellas]], [[Polk County, Florida|Polk]], [[Sarasota County, Florida|Sarasota]], and [[Sumter County, Florida|Sumter]] counties. The passage of the storm resulted in an increased threat for rip currents, with officials recommending that swimmers stay out of the water until the storm leaves the area.<ref name="Ruskin">{{cite web|author=Ruskin, FL National Weather Service|year=2007|title=Tropical Storm Local Statement|accessdate=2007-06-01|url=http://www.weather.unisys.com/hurricane/archive/07060200}}</ref> A tornado watch was also posted for the southern portion of the state, though was dropped after the storm weakened.<ref name="mh62">{{cite web|author=Martin Merzer|year=2007|title=Barry weakens but drenches South Florida|publisher=Miami Herald|accessdate=2007-06-02|url=http://www.neorunner.com/archive/2007/06/03/Barry_Weakens_but_Drenches_South_Florida.php}}{{Dead link|date=May 2010}}</ref>

After becoming an extratropical cyclone, local [[National Weather Service]] offices issued flood watches for portions of [[South Carolina]], much of eastern [[North Carolina]], southeastern [[Virginia]], and southeastern [[Maryland]].<ref name="hpcpa7"/> Later, flood and [[flash flood]] watches were issued for southeast [[Pennsylvania]], northern [[Delaware]], northern [[New Jersey]], east-central [[New York (state)|New York]], and southern [[New England]].<ref name="hpcpa9"/> Wind and lake wind advisories were posted in parts of Georgia.<ref>{{cite web|author=|publisher=National Climatic Data Center|title=Tropical Storm Barry Event Report for Georgia|year=2007|accessdate=2008-11-19|url=https://www.ncdc.noaa.gov/stormevents/eventdetails.jsp?id=42575}}</ref>

==Impact==
[[Image:Barry 2007 rainfall.gif|left|thumb|Rainfall Summary for Tropical Storm Barry]]

===Caribbean===
In [[El Salvador]], the precursor trough produced about 2.76&nbsp;inches (70&nbsp;mm) of rainfall in about ten hours.<ref name="531twd">{{cite web|author=Mike Tichacek |year=2007 |title=May 31 Tropical Weather Discussion |publisher=National Hurricane Center |accessdate=2007-06-01 |url=ftp://ftp.met.fsu.edu/pub/weather/tropical/Atl-Dis/2007053111.AXNT20 }}{{dead link|date=January 2017 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>

The precursor system dropped heavy rainfall across western Cuba, peaking at 12.0&nbsp;inches (305&nbsp;mm) in [[Sancti Spíritus Province]].<ref name="tcr"/> Several other locations recorded over 4&nbsp;inches (100&nbsp;mm) of precipitation, which caused flooding along rivers and low-lying areas. The city of [[Guane]] was isolated after flooding cut off communications. In total, more than 2,000&nbsp;people were evacuated due to the threat for flooding. Additionally, the precursor disturbance spawned four tornadoes in Pinar del Río Province; the tornadoes injured three people and damaged fifty-five&nbsp;houses, of which four collapsed.<ref name="efe62">{{cite web|author=[[EFE]] |year=2007 |title=La tormenta tropical "Barry" baña con sus lluvias a Cuba |publisher=Cuba Debate |language=Spanish |accessdate=2007-06-02 |url=http://archivo.cubadebate.cu/index.php?tpl=design/noticias.tpl.html&newsid_obj_id=9291 |archive-url=https://archive.is/20130210070644/http://archivo.cubadebate.cu/index.php?tpl=design/noticias.tpl.html&newsid_obj_id=9291 |dead-url=yes |archive-date=2013-02-10 }}</ref>

===United States===

====Florida====
Barry dropped moderate to heavy rainfall across [[Florida]], peaking at 6.99&nbsp;inches (178&nbsp;mm) at [[Palm Beach International Airport]]; several other locations reported over 3&nbsp;inches (75&nbsp;mm).<ref name="hpcpa7"/> The rainfall alleviated persistent drought conditions<ref name="mh62"/> and assisted in combating severe [[Bugaboo scrub fire|wildfires]] across the state.<ref name="fcn62">{{cite web|author=First Coast News|year=2007|title=Tropical Storm Barry Helps Out Wildfires|accessdate=2007-06-03|url=http://www.firstcoastnews.com/news/georgia/news-article.aspx?storyid=83316}}</ref> In [[Brevard County, Florida|Brevard County]], the rainfall closed a portion of [[Florida State Road 518|Eau Gallie Boulevard]] after a large sinkhole developed. Several other roads across the area were flooded,<ref name="ft63">{{cite web|author=Susanne Cervenka|year=2007|title=Barry brings precious gift of rain|publisher=Florida Today|accessdate=2007-06-03|url=http://www.floridatoday.com/apps/pbcs.dll/article?AID=/20070603/NEWS01/706030336/1006|archiveurl=https://web.archive.org/web/20070606070121/http://www.floridatoday.com/apps/pbcs.dll/article?AID=/20070603/NEWS01/706030336/1006|archivedate=2007-06-06}}</ref> and on [[Interstate 95 in Florida|Interstate 95]] near [[Lake Worth, Florida|Lake Worth]], a sinkhole closed two lanes of traffic.<ref>{{cite web|author=Kimberly Miller|publisher=Palm Beach Post|year=2007|title=Welcome rains carve sinkholes in I-95|accessdate=2007-06-03|url=http://www.palmbeachpost.com/localnews/content/local_news/epaper/2007/06/03/m1a_RAIN_0603.html}} {{Dead link|date=September 2010|bot=H3llBot}}</ref> Wet roads caused several traffic accidents across the state; in both Brevard and [[Volusia County, Florida|Volusia]] counties, a motorist was killed from an accident. On [[Interstate 4]], a tractor trailer led to disruptions near [[Orlando, Florida|Orlando]] after it crashed into a guardrail.<ref>{{cite web|author=''WFTV-9'' |year=2007 |title=Barry Downgraded After Soaking Central Florida |accessdate=2007-06-03 |url=http://www.wftv.com/news/13428614/detail.html |deadurl=yes |archiveurl=https://web.archive.org/web/20070606113708/http://www.wftv.com/news/13428614/detail.html |archivedate=2007-06-06 |df= }}</ref>
{|class="wikitable" style="float:right;margin:0 0 0.5em 1em;"
|+Rainfall maxima by [[U.S. state|state]]<ref name="hpcpa14">{{cite web|author=Fracasso|year=2007|title=Public Advisory Number 14 for Remnants of Barry|publisher=Hydrometeorological Prediction Center|accessdate=2007-06-03|url=http://www.wpc.ncep.noaa.gov/tropical/2007/BARRY/BARRY_14.html}}</ref>
|-
!rowspan=2|Location
!colspan=2|Peak
|-
! inch || mm
|-
| [[West Palm Beach, Florida]] || 6.99 || 178
|-
| [[Mount Vernon, Georgia]] || 8.00 || 203
|-
| Near [[Hardeeville, South Carolina]] || 6.12 || 156
|-
| [[Fuquay-Varina, North Carolina]] || 3.73 || 94.7
|-
| [[Pennington Gap, Virginia]] || 3.75 || 95.3
|-
| [[Frostburg, Maryland]] || 1.70 || 43.2
|-
| [[Dover, Delaware]] || 1.54 || 39.1
|-
| [[Philadelphia, Pennsylvania]] || 1.66 || 42.2
|-
| [[Absecon, New Jersey]] || 4.50 || 114
|-
| [[Central Park|Central Park, New York]] || 3.91 || 99.3
|-
| [[Berlin, Connecticut]] || 2.90 || 73.7
|-
| [[Taunton, Massachusetts]] || 3.19 || 81.0
|-
| [[Burrillville, Rhode Island]] || 3.10 || 78.7
|-
| [[Newmarket, New Hampshire]] || 2.75 || 69.9
|-
| [[Saco, Maine]] || 2.64 || 67.1
|}
The storm produced heavy surf along the western coastline, as well as a [[storm tide]] of 4.78&nbsp;feet (1.46&nbsp;m) at [[Clearwater, Florida|Clearwater Beach]]. The wave action caused minor beach erosion, with 50–60&nbsp;feet (15–18&nbsp;m) of sand washed away at [[Bradenton, Florida|Bradenton Beach]]. The increased ocean action caused minor flooding along several roads in the [[Tampa Bay]] area,<ref name="tbwnws">{{cite web|author=McMichael|year=2007|title=Tropical Storm Barry Post Tropical Cyclone Report|publisher=Ruskin, Florida National Weather Service|accessdate=2007-06-11|url=http://www.srh.noaa.gov/data/warn_archive/TBW/PSH/0609_100716.txt}} {{Dead link|date=March 2012|bot=H3llBot}}</ref> which trapped some automobile travelers.<ref>{{cite web|author=Fox13|year=2007|title=Barry leaves his calling card|accessdate=2007-06-03|url=http://www.myfoxtampabay.com/dpp/news/Barry_leaves_his_calling_card}} {{Dead link|date=March 2012|bot=H3llBot}}</ref> At [[Indian Shores, Florida|Indian Shores]], a woman died after sustaining injuries from the rough surf.<ref name="tbwnws"/>

High winds across the state included a report of 47&nbsp;mph (76&nbsp;km/h) near the state's southeastern coastline.<ref name="ap622">{{cite web|author=Associated Press |year=2007 |title=Tropical Storm Barry brings rain to Florida |accessdate=2007-06-02 |url=http://www.dallasnews.com/sharedcontent/dws/news/nationworld/stories/060307dnnattsbarry.c156f.html |deadurl=yes |archiveurl=https://web.archive.org/web/20070930023506/http://www.dallasnews.com/sharedcontent/dws/news/nationworld/stories/060307dnnattsbarry.c156f.html |archivedate=September 30, 2007 }}</ref> The winds downed some trees and resulted in power outages,<ref>{{cite web|author=Tiffany Pakkala|year=2007|title=Barry dumps 5 inches of rain on area|publisher=The St. Augustine Record|accessdate=2007-06-03|url=http://staugustine.com/stories/060307/news_4633795.shtml}}</ref> and one person in [[Carrollwood, Florida|Carrolwood]] was injured after a tree fell onto a house.<ref name="tbwnws"/> The storm spawned several tornadoes in the southern portion of the state, some of which damaged fences and power lines. One possible tornado in [[Goulds, Florida|Goulds]] left about 2,000&nbsp;people without power after it knocked down a power line.<ref name="mh62"/> Another tornado near [[Miami, Florida|Miami]] damaged a few homes and trees.<ref name="abc30">{{cite web|author=''KFSN-TV''|year=2007|title=Tropical Storm Barry Brings Much Needed Rain To Florida|accessdate=2007-06-03|url=http://abclocal.go.com/kfsn/story?section=news/local&id=5363314}}</ref>

====Elsewhere====
[[File:Tropical Storm Barry.jpg|left|thumb|Minor street flooding from Barry in Florida]]
Rainfall in [[Georgia (U.S. state)|Georgia]] peaked at 8&nbsp;inches (203&nbsp;mm) in [[Mount Vernon, Georgia|Mount Vernon]].<ref name="hpcpa9"/> The precipitation assisted firefighters in combating [[2007 Georgia/Florida wildfires|wildfires]] in the southern portion of the state, which gave thousands of workers a brief respite after they had fought the fires daily for over a month.<ref name="ap63">{{cite web|author=Associated Press|year=2007|title=First significant rain in weeks gives fire crews much-needed break|accessdate=2007-06-03|url=http://news.mywebpal.com/partners/680/public/news811591.html| archiveurl = https://web.archive.org/web/20070926213010/http://news.mywebpal.com/partners/680/public/news811591.html| archivedate = September 26, 2007}}</ref> The rainfall caused some minor flooding, and in [[Savannah, Georgia|Savannah]] a few minor traffic accidents occurred.<ref>{{cite news|author=Savannah Morning News|year=2007|title=Barry brings relief with 5 inches of rain|url=http://savannahnow.com/node/297818|accessdate=2007-06-03}}{{dead link|date=October 2014}}</ref> Gusty winds blew down trees and power lines, and along the coast, rough surf was reported.<ref name="gancdc">{{cite web|author=|title=Tropical Storm Barry Event Report for Georgia (2)|year=2007|publisher=National Climatic Data Center|accessdate=2008-11-25|url=|dead-url=}}</ref> Heavy rainfall from the storm spread across much of the [[East Coast of the United States]]. State totals peaked at 6.12&nbsp;inches (155&nbsp;mm) near [[Hardeeville, South Carolina]],<ref name="hpcpa9"/> 3.73&nbsp;inches (95&nbsp;mm) in [[Fuquay-Varina, North Carolina]], and 3.75&nbsp;inches (95&nbsp;mm) near [[Pennington Gap, Virginia]].<ref name="hpcpa14"/> High winds also occurred in South Carolina.<ref name="gancdc"/>

The extratropical remnants of Barry produced gusty winds along the Atlantic coastline which peaked at 60&nbsp;mph (97&nbsp;km/h) near [[Charleston, South Carolina]].<ref name="hpcpa7"/> Around 200&nbsp;houses in [[Craven County, North Carolina]], were without power after winds downed a power line.<ref>{{cite web|author=Charlie Hall |publisher=New Bern Sun Journal |year=2007 |accessdate=2007-06-04 |title=Barry causes few problems in region |url=http://www.newbernsj.com/news/sunday-34694-afternoon-rain.html |deadurl=yes |archiveurl=https://web.archive.org/web/20110727155409/http://www.newbernsj.com/news/sunday-34694-afternoon-rain.html |archivedate=2011-07-27 |df= }}</ref> In North Carolina, adverse conditions from the storm delayed an elimination baseball game between the [[East Carolina University]] and [[Western Carolina University]] teams.<ref>{{cite web|author=Bonesville.net|year=2007|title=Chapel Hill Regional slips in the rain|accessdate=2007-06-03|url=http://bonesville.net/Articles/OtherArticles/Bonesville/Bonesville_Baseball/2007/Regionals/060307_Rain.htm}}</ref> In southeastern [[Virginia]], the remnants of Barry caused over 60&nbsp;traffic accidents, which resulted in 10&nbsp;injuries.<ref>{{cite web|author=Steve Stone and Amy Coutee|year=2007|title=Tropical storm's leftovers force cancellations across region|publisher=Virginia Pilot|accessdate=2007-06-04|url=http://hamptonroads.com/node/276471}}</ref> Rough seas off of [[Cape Fear (headland)|Cape Fear]] left a sailboat containing three people requiring rescue from the [[Coast Guard]].<ref>{{cite web|author=Daily Press|year=2007|title=Boaters off Cape Fear rescued|url=http://www.wistv.com/Global/story.asp?S=6606323|accessdate=2007-06-03}}{{dead link|date=October 2014}}</ref> Rainfall extended into the [[Mid-Atlantic states]] through [[New England]], with 4.50&nbsp;inches (113&nbsp;mm) reported at [[Absecon, New Jersey]], 3.91&nbsp;inches (99&nbsp;mm) recorded near [[Central Park|Central Park, New York]], and 3.19&nbsp;inches (81&nbsp;mm) at [[Taunton, Massachusetts]].<ref name="hpcpa14"/> The remnants of Tropical Storm Barry contributed to heavy rainfall and flooding in the [[Finger Lakes]] region of New York State. Roads and several driveways were washed out.<ref>{{cite web|author=|title=Tropical Storm Barry Event Report for New York|year=2007|publisher=National Climatic Data Center|accessdate=2008-11-19|url=|dead-url=}}</ref> [[Flash flood]]ing was also reported in southeast New York, and high wind gusts caused sporadic tree damage.<ref>{{cite web|author=|title=Tropical Storm Barry Event Report for New York (2)|year=2007|publisher=National Climatic Data Center|accessdate=2008-11-19|url=|dead-url=}}</ref> In New Jersey, northeast onshore flow associated with the remnants of Barry produced high tides and minor coastal flooding.<ref>{{cite web|author=|title=Tropical Storm Barry Event Report for New Jersey|year=2007|publisher=National Climatic Data Center|accessdate=2008-11-25|url=|dead-url=}}</ref>

==See also==
{{Portal|Tropical cyclones}}
* [[List of Florida hurricanes (2000–present)]]
* [[List of New England hurricanes]]
* [[List of New Jersey hurricanes]]
* [[List of New York hurricanes]]
* [[List of North Carolina hurricanes (1980–present)]]
* [[Timeline of the 2007 Atlantic hurricane season]]
* [[Tropical Storm Barry|Other storms of the same name]]
* [[Tropical Storm Andrea (2013)]]

==References==
{{Reflist|33em}}

==External links==
{{Commons category|Tropical Storm Barry (2007)}}
* {{NHC TCR url|id=AL022007_Barry|title=NHC Tropical Cyclone Report for Barry}}
* [http://www.wpc.ncep.noaa.gov/tropical/rain/barry2007.html HPC rainfall page for Barry]

{{2007 Atlantic hurricane season buttons}}
{{Featured article}}

{{DEFAULTSORT:Barry (2007)}}
[[Category:2007 Atlantic hurricane season]]
[[Category:Atlantic tropical storms]]
[[Category:Hurricanes in Cuba]]
[[Category:Hurricanes in Florida]]
[[Category:Hurricanes in Georgia (U.S. state)]]
[[Category:Hurricanes in South Carolina]]
[[Category:Hurricanes in North Carolina]]
[[Category:Hurricanes in Virginia]]
[[Category:Hurricanes in Maryland]]
[[Category:Hurricanes in Delaware]]
[[Category:Hurricanes in New Jersey]]
[[Category:Hurricanes in Pennsylvania]]
[[Category:Hurricanes in New York]]
[[Category:Hurricanes in New England]]
[[Category:2007 natural disasters in the United States|Tropical Storm Barry]]