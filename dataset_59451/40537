{{Infobox writer
| name          = S. M. Ikram
| native_name   = {{Nastaliq|شیخ محمد اِکرام}}
| native_name_lang = ur 
| image         = SM Ikram at his desk.jpeg
| caption       = S. M. Ikram at his desk, c. 1935.
| birth_name    = Sheikh Muhammad Ikram
| birth_date    = {{Birth date|df=yes|1908|09|10}}
| birth_place   = [[Chak Jhumra]], [[Lyallpur]], [[British India]], now [[Pakistan]]
| death_date    = {{Death date and age|df=yes|1973|01|17|1908|09|10}}
| death_place   = [[Lahore]]
| occupation    = Civil servant, historian, biographer and author
| religion      = [[Islam]]
| citizenship   = [[Pakistan]]
| genre         = [[History]], [[Biography]], [[Urdu literature]]
| spouse        = Zebunnisa (1910–1991)
| children      = Hamid, Khalid, and Zahid Ikram, and Shahida Azfar
| influences    = Sir Syed, Shibli, Iqbal
| influenced    = Unclear
| alma_mater    = Government College, Lahore
}}

'''Sheikh Muhammad Ikram''' ([[Urdu]]: '''شیخ محمد اکرام'''; b. 10 September{{efn|This is the date on his passport. According to Moazzamuddin (1994, hereafter ''Life''), who interviewed close family members for his biography of Ikram, there is a minor dispute over his exact day of birth: according to Ikram's sister Surayya, it is September 2; while another report has it as August 21 or 22.}} 1908 – 17 January 1973) better known as '''S. M. Ikram''', was a  Pakistani historian, biographer, and littérateur.  He was member of the [[Indian Civil Service]] (which he joined in 1931). In 1947, when [[Pakistan]] emerged from [[British India]], Ikram opted for Pakistan and served in the [[Civil Service of Pakistan]]. On July 1, 1966, he was appointed as Director, [http://www.punjab.gov.pk/information_and_culture_institutions#faq9 Institute of Islamic Culture], Lahore, a position he occupied until his death in 1973, at the age of sixty-four.

== Personal life ==
S. M. Ikram's parents were from [[Rasulnagar]], a small town in the [[Wazirabad Tehsil|Wazirabad Sub-Division]] of [[Gujranwala District]] in the [[Punjab]] in present-day [[Pakistan]]. His father, Sheikh Fazal Kareem, was a [[Qanungoh Shaikh|Qanungo]], a pre-Mughal hereditary office of revenue and judicial administration; his mother  was Sardar Begum. Ikram was the eldest of five brothers and two sisters. Ikram's father wanted to name his son [[Abdul Qadir (Muslim leader)|Abdul Qadir]], after the name of the editor of the first [[Urdu]] language magazine, ''Makhzan'', but his own father (Dasaundi Khan) prevailed to name him after his friend, the Assistant Editor of ''Makhzan'', "Sheikh Muhammad Ikram".{{efn|Muazzamuddin, ''Life'', 14, who relates this from Mrs. S. M. Ikram. In the opinion of one of Ikram's lifelong friend, it indicative of the father's expectations of the son: Professor Hameed Ahmad Khan (1973, cf. Muazzamuddin, ''Life'', 16); Khan relates this from Ikram's brother, S. M. Iqbal.}} Ikram was married on December 30, 1936 in [[Gujrat City|Gujrat]] to the elder of two daughters (Zebunnisa and Zeenat) of Mian Mukhtar Nabi ("Mianji"), at the time Deputy Director, the Punjab Agriculture Department. Ikram's wife passed her matriculation examinations from Delhi, and obtained her B.A. in Persian, English, and History from [[Kinnaird College for Women University|Kinnaird College for Women]], Lahore. At his death in [[Lahore]] on January 17, 1973 he was survived by his wife, Zebunnisa (1910-1999), and four children.{{efn|For more on S. M. Ikram's family members see Khaled Ahmed, S. M. Ikram: Saga of a family of extraordinary distinction, ''Friday Times'', April 20–26, 2007.}}

== Education ==
Ikram completed his primary education in [[Gojra|Kacha Gojra]] (located between [[Faisalabad]] and [[Toba Tek Singh]]); his secondary education, from Mission High School, [[Wazirabad]]; his matriculation, in 1924, from Government High School (that later became 
[http://gcuf.edu.pk/about/history-and-introduction/ Government Intermediate College]), [[Faisalabad|Lyallpur]]; from where also he passed the Faculty of Arts (F.A.) examinations in 1926. During these four years in Lyallpur (1922-1926) Ikram developed his taste and proficiency in the Persian language and poetry. From Lyallpur he moved to Lahore and graduated from [[Government College University (Lahore)|Government College]] with a B.A. in Persian (''cum laude''), English, and Economics, in 1928; and an M.A. in English Literature in 1930.

== Professional life ==
Although a full-time civil servant, S. M. Ikram is more famous for his prolific output as a published writer. 
[[File:SM Ikram & Allama Iqbal.jpeg|thumb|S. M. Ikram and [[Muhammad Iqbal|Allama Iqbal]], London, 29 December 1932.{{efn|This photograph appears as the frontispiece in Ikram's first book, ''Ghalibnāma'', which is dedicated to (tr.) "The interpreter of reality, Allama Sir Muhammad Iqbal, May we live long under his shadow (''mudda zilluhū''--lit. may his shadow be extended, traditional expression of respect for revered elders)" and also in his anthology of Persian verse by poets of Indo-Pakistan origin, ''Armaghān-e-Pāk, from the 5th century Hijri (12th century CE) to Iqbal'', first published c. 1950.}}]] After obtaining his M.A. (1930) Ikram appeared for the [[Indian Civil Service (British India)|ICS]] examinations in January 1931 in Delhi. On selection, he was sent in September to [[Jesus College, Oxford]] for two years (1931-1933). On return from England in October Ikram was posted to various positions in the Bombay Presidency (November 1933 to September 1947). At partition, he opted for Pakistan and after attending an official farewell in Puna on September 18, 1947, he emigrated to Pakistan and took up his official position on September 29, 1947. He taught  at Columbia University in New York (as a Visiting Professor in 1953-1954, and  visited again in 1958-59 and 1961-62. It was here that he made the transition from literature to history and started writing in English rather than Urdu.

== Major works ==
A major difficulty in reviewing the works of Ikram arises from the fact that he published interim works which he revised often, in the light of his new findings: correcting mistakes, adding, deleting, and rearranging sections, expanding one volume into two (changing the title of the work in the new edition and reverting to the old title in the next edition). In many cases the revisions were sufficiently major for the original and the revised to be treated as two separate works. A study of these differences is still awaited.{{efn|A notable exception is: Muazzamuddin (1990).}}

=== Works in the Urdu language ===
In their final versions, S. M. Ikram's major works in Urdu consist of biographies of two major literary figures in Urdu, and his ''magnum opus'', the three-volume intellectual history of Muslim India and Pakistan, comparable in scope and method to Vernon Parrington's ''[[Main Currents in American Thought]]'' (1927):
* A critical biography of the classical Urdu and Persian poet, [[Ghalib|Ghālib]], in three volumes (that might be called his Ghalib trilogy):
** ''Hayāt-e Ghālib'' (The Life of [[Ghalib|Ghālib]], 5th ed. 1957): biography (''tazkira'')
** ''Hakīm-e Farzāna'' (The Wise Philosopher, 5th ed. 1957): criticism (of prose, ''tabsira''; consisting of two sections: [[Ghalib|Ghālib]]'s literary development, and general discussion)
** ''Armaghān-e Ghālib'' (A Ghalib Offering, 3rd ed. c. 1944): critical appreciation (of poetry, ''intikhāb'')
* A counter-biography—a subgenre that has been called "pathography" in our times<ref>https://www.nytimes.com/1988/08/28/books/adventures-in-abandonment.html?pagewanted=all</ref>—of [[Shibli Nomani|Maulānā Shiblī Nu`mānī]], written in response to [[Sulaiman Nadvi|Maulānā Sayyid Sulaimān Nadvī]]'s ''Life of Shiblī'':{{efn|Maulānā Sayyid Sulaimān Nadvī, ''Hayāt-e Shiblī'' (Azamgarh, Dārul Musannifīn, n.d. c. 1943). In 1909, Shiblī had demurred when [[Nazir Ahmad Dehlvi|Maulvī Nazīr Ahmad]]'s biographer, Maulvī Iftikhār `Ālam [[Marhara (Assembly constituency)|Mārharvī]], had asked his permission to write his biography. In 1914, Shiblī wrote to Sulaimān Nadvī: "Some time, when you are done with all the other works of the world, ''you'' write it." (''Makātīb-e Shiblī'', vol. 2, 1966, 264-265, and vol. 1, 1971, 107).}}
** Biography (''Shiblīnāma'', 1st ed. 1945/46); and
** A Revised Amended Ed. (''Yādgār-e Shiblī'', 1971)
* A religious history of Muslim India and Pakistan: 
** ''Āb-e Kausar'' (The Water of ''Kausar''), covering the Pre-Mughal (711-1526) period;
** ''Rūd-e Kausar'' (The River of ''Kausar''), covering the [[Mughal Empire|Mughal]] period (1526-1800);
** ''Mauj-e Kausar'' (The Wave of ''Kausar''), covering the post-Mughal (1800-1947) era.

=== Works in the English language ===
With the birth of Pakistan, Ikram took up his official duties in the Ministry of Information and Broadcasting, and his attentions turned toward nation-building both in his official duties and his personal commitments. 
*The result were two books in English that were adapted from ''Mauj-e Kausar'':
** ''Makers of Pakistan and Modern Muslim India'', 1950
** ''Modern Muslim India and the Birth of Pakistan (1858-1951)'', 1965

In August 1953 Ikram took leave for one yearto take up the position of Visiting Professor at Columbia University, New York, which he visited again in 1958-59, and 1961-62. At Columbia he encountered an entirely different (non-Muslim, English-speaking) audience and was introduced to professional historians and their methods which, with his sympathy with Islam, facility in the Persian language, familiarity with original sources, and learning acquired over years of reading, writing, and reflecting, he found deficient:
:In 1953-54, when I undertook a year's teaching assignment at Columbia University, the need for a book in English, dealing with all aspects of Indo-Muslim history, was forcefully brought home to me. I felt this need particularly with regard to American students who, in the absence of anything better, had to fall back upon [[Vincent Arthur Smith|Vincent Smith]]'s [https://archive.org/details/oxfordhistoryofi00smituoft ''Oxford History of India''] or similar compilations.{{efn|Preface to S. M. Ikram, ''Muslim Rule'' (2nd ed. 1966).}}

Ikram's lectures at Columbia were the basis for three books:
* ''History of Muslim Civilization in India and Pakistan (711–1858 A.D.)'', 1962;
* A shorter American summary: ''Muslim Civilization in India'', 1964, edited by [[Ainslie Embree|Professor Embree]];{{efn|The book is not ,as a distinguished American historian has been misled to believe: "a one-volume version of Shaikh Muhammad Ikram’s three-volume Urdu [''Kausar''] trilogy."[[Barbara D. Metcalf]] (2005). This may have been an echo of Abbott (1968, 233): "A. T. Embree has prepared an English edition of much of S.M. Ikram's three-volume Urdu study, ''Āb-i-Kauthar'' (Lahore, 1952), ''Mawj-i-Kauthar'' (Karachi, 1958), and ''Rūd-i-Kauthar'' (Karachi, n.d.), titled ''Muslim Civilization in India'' (New York, 1964)." [http://www.columbia.edu/itc/mealac/pritchett/00fwp/#fwp Frances W. Pritchett] (n.d.) states the correct position: "''Muslim Civilization in India'' was edited by Ainslie T. Embree, ... He created it out of the author's 712-page ''History of Muslim Civilization in India and Pakistan'' (Lahore: Institute of Islamic Culture, 1993 [1961]), by removing most of the notes and many specialized passages, leaving a kind of bare-bones account."}} and
* An expanded national version: ''Muslim Rule in India and Pakistan (711-1858 A.D.)'', 1966

===Unfinished works===
At the time of his death, Ikram had been working on the draft of two books: one, a candid history written after he had retired and could write freely, entitled ''A History of Pakistan, 1947–1971'', was finished and was to have been published by June 1973; the other, ''A Biography of Quaid-e-Azam'', in which he wished to remedy the gap between the scholarship on Gandhi in India and that on Jinnah in Pakistan, was at an advanced stage of preparation.{{efn|The outlines of this can be seen in the chapter on Jinnah in Ikram's ''Modern Muslim India'' (2nd ed. 1977, pp. 354-471, written in June 1965).}} Unfortunately in the disarray surrounding his death both manuscripts were lost.{{efn|For details, see Muazzamuddin, ''Life'', 35.}}

== Honors and awards ==
[[File:SM Ikram receiving DLitt Degree.jpeg|thumb|SM Ikram receiving DLitt Degree|S. M. Ikram receiving an honorary D. Litt. from the [[Nawab of Kalabagh]]. [[Ayub Khan (President of Pakistan)]] is seated at right.]]
* MRCAS (London): On the title page of ''Ghalibnama'' (1936) the author is listed as "Sheikh Muhammad Ikram, MA, MRCAS (London), ICS, Sub-Divisional Officer, Surat, Bombay Area.{{efn|MRCAS probably refers to membership of the [[Royal Society for Asian Affairs|Royal Central Asian Society]].}}
* In January 1964, the Punjab University awarded him an honorary Doctor of Letters (D. Litt. or  ''Litterarum doctor'') degree.
* In 1965, he was also awarded the [[Sitara-i-Imtiaz|Sitara-e Imtiaz]], a civil award for especially meritorious contribution to the security or national interests of Pakistan, world peace, cultural or other significant public endeavors.
* Shortly thereafter, the government of Iran awarded him ''Nishan-i Sipas'' (Order of Merit) for his service to literature.
* In 1971, the government of Pakistan awarded him a [[Pride of Performance]] medal, a civil award given to Pakistani citizens in recognition of distinguished merit, for his writings in the field of literature.

== List of publications ==
The following list is based largely on Moazzamuddin (1994, and 1990).

===Works in Urdu===
* {{cite book |ref=harv |last1=Ikram |first1=S. M. |title=Ghālibnāma ''(A History of Ghalib)'' |date=1936 |publisher=Taj Company |location=Lahore |edition=1st}} This consisted of three sections: tazkira (Remembrance), tabsira (Criticism), intikhāb (Selections).
* {{cite book |ref=harv |last1=Ikram |first1=S. M. |title=Ghālibnāma ''(The Ghalib Epistle)'' |date=1939 |publisher=Taj Company |location=Lahore |edition=rev. 2nd}}
* {{cite book |ref=harv |last1=Ikram |first1=S. M. |title=Ghālibnāma: Āsār-e Ghālib ''(The Ghalib Epistle: The Annals of Ghalib--Prose: Remembrance and Criticism); final edition)'' |date=1944 |publisher=Taj Company |location=Lahore |edition=rev. 3rd}}
* {{cite book |ref=harv |last1=Ikram |first1=S. M. |title=Ghālibnāma: Armaghān-e Ghālib ''(The Ghalib Epistle: An Offering of Ghalib--Poetry: Selections; final edition)'' |date=1944 |publisher=Taj Company |location=Lahore |edition=rev. 3rd}}
* {{cite book |ref=harv |last1=Ikram |first1=S. M. |title=Ghālibnāma: Hayāt-e Ghālib ''(The Life of Ghalib; first half of Āsār-e Ghālib--Remembrance)'' |date=1946–1947 |publisher=Taj Company |location=Lahore |edition=rev. 4th}}
* {{cite book |ref=harv |last1=Ikram |first1=S. M. |title=Ghālibnumā ''(An Offering of Ghalib; second half of Āsār-e Ghālib--Criticism)'' |date=1946–1947 |publisher=Taj Company |location=Lahore |edition=rev. 4th}}
* {{cite book |ref=harv |last1=Ikram |first1=S. M. |title=Ghālibnāma: Hayāt-e Ghālib ''(The Life of Ghalib; first half of Āsār-e Ghālib--Remembrance)'' |date=1957 |publisher=Taj Company |location=Lahore |edition=rev. 5th}}
* {{cite book |ref=harv |last1=Ikram |first1=S. M. |title=Hakīm-e Farzāna ''(The Wise Philosopher; revised title of Ghālibnumā--Criticism)'' |date=1957 |publisher=Taj Company |location=Lahore |edition=rev. 5th}} Revised ed. 1977, published posthumously with preface by Ahmad Nadeem Qasmi, Lahore: Idāra-e Saqāfat-e Islāmiya (Institute of Islamic Culture).
* {{cite book |ref=harv |last1=Ikram |first1=S. M. |title=''Vol. 1:'' Āb-e Kausar ''(The Water of Kausar)''; ''Vol. 2:'' Mauj-e Kausar ''(The Wave of Kausar)'' |date=1940 |publisher=Mercantile Press |location=Lahore |edition=1st}}
* {{cite book |ref=harv |last1=Ikram |first1=S. M. |title=''Vol. 1:'' Chashma'-e Kausar ''(The Spring of Kausar; first half of ''Āb-e Kausar'', 1st ed.)''; ''Vol. 2:'' Rūd-e Kausar ''(The River of Kausar; second half of ''Āb-e Kausar'', 1st ed.)''; ''Vol. 3:'' Mauj-e Kausar ''(The Wave of Kausar)'' |date=n.d. c. 1944 |publisher=Istaqlāl Press |location=Lahore |edition=2nd}}
* {{cite book |ref=harv |last1=Ikram |first1=S. M. |title=''Vol. 1:'' Āb-e Kausar: Islāmī Hind-o-Pākistān kī Mazhabī aur ‘Ilmī Tārīkh, ‘Ahd-e Mughalia sē Pahlē ''(The Water of Kausar: Islamic Indo-Pakistan's Religious and Intellectual History, Before the Mughal Era)''; ''Vol. 2:'' Rūd-e Kausar: Islāmī Hind-o-Pākistān kī Mazhabī aur ‘Ilmī Tārīkh, ‘Ahd-e Mughalia ''(The River of Kausar: Islamic Indo-Pakistan's Religious and Intellectual History, The Mughal Era)''; ''Vol. 3:'' Mauj-e Kausar: Islāmī Hind-o-Pākistān kī Mazhabī aur ‘Ilmī Tārīkh kā Daur-e-Jadīd, Unīswīn Sadī kē Āghāz sē Zamāna-e-Hāl Tak ''(Wave of the Heavenly Spring: A Religious and Intellectual History of Islamic India and Pakistan, The Modern Period, from the Early Nineteenth Century to the Present Times)'' |date=1952 |publisher=Ferozsons |location=Lahore |edition=3rd}} 4th ed. 1958.
* {{cite book |ref=harv |last1=Ikram |first1=S. M. |title=''Vol. 1:'' Āb-e Kausar: Islāmī Hind-o-Pākistān kī Mazhabī aur ‘Ilmī Tārīkh, ‘Ahd-e Mughalia sē Pahlē ''(The Water of Kausar: Islamic Indo-Pakistan's Religious and Intellectual History, Before the Mughal Era)''; ''Vol. 2:'' Rūd-e Kausar: Islāmī Hind-o-Pākistān kī Mazhabī aur ‘Ilmī Tārīkh, ‘Ahd-e Mughalia ''(The River of Kausar: Islamic Indo-Pakistan's Religious and Intellectual History, The Mughal Era)''; ''Vol. 3:'' Mauj-e Kausar: Islāmī Hind-o-Pākistān kī Mazhabī aur ‘Ilmī Tārīkh kā Daur-e-Jadīd, Unīswīn Sadī kē Āghāz sē Zamāna-e-Hāl Tak ''(Wave of the Heavenly Spring: A Religious and Intellectual History of Islamic India and Pakistan, The Modern Period, from the Early Nineteenth Century to the Present Times)'' |date=1964 |publisher=Ferozsons |location=Lahore |edition=5th & final}}
* {{cite book |ref=harv |last1=Ikram |first1=S. M. |title=Shiblināma: Aik funkār ki dāstān-e hayāt ''(A History of Shibli: The Tale of an Artist's Life)'' |date=n.d. c. 1946 |publisher=Taj Office |location=Bombay |edition=1st}}
* {{cite book |ref=harv |last1=Ikram |first1=S. M. |title=Yādgār-e Shiblī: Aik funkār kī dāstān-e hayāt ''(A History of Shibli: The Tale of an Artist's Life)'' |date=1971 |publisher=Idāra-e Saqāfat-e Islāmiya (Institute of Islamic Culture) |location=Lahore |edition=2nd & final}}
* {{cite book |ref=harv |last1=Ikram |first1=S. M. |title=Nawā'-ē Pāk ''(A Voice of Pakistan)'' |date=1951 |publisher=Idara-e matbu`at Pakistan |location=Karachi |edition=1st}}
* {{cite book |ref=harv |last1=["Iqbal |first1=S. M."] |title=Jamā`at-e Islāmī par aik Nazar ''(A Look at Jama`at-e Islami)'' |date=1952 |publisher= |location= |edition=1st}} Pseudonymous.
* {{cite book |ref=harv |last1=Ikram |first1=S. M. |title=Saqāfat-e Pākistān ''(The Culture of Pakistan)'' |date=c. 1957 |publisher=Idara-e Matbu`at Pakistan |location=Karachi |edition=1st}} 2nd ed. 1967.

===Works in Persian===
* {{cite book |ref=harv |last1=Ikram |first1=S. M. |title=Armaghān-e Pāk: Islāmī Hind-o-Pākistān ki Fārsī Shā‘erī kā Intikhāb ''(A Pure/Pakistani Offering: A Selection of Persian Poetry of India and Pakistan)'' |date=c. 1950 |publisher=Idara-e Matbu`at Pakistan |location=Karachi |edition=1st}} 2nd ed. 1953; Tehran: Nashir Kanun Ma`arafat, 3rd ed. 1954, and Lahore: Combine Printers, Bilal Ganj, 3rd ed. c. 1971; Karachi: Idāra-e Matbū`āt Pakistan, 4th ed. 1959.
* {{cite book |ref=harv |last1=[Qureshi |first1=with Dr. Waheed] |title=Darbār-e Millī ''(The National Court)'' |date=1961 |publisher=Majlis-e Taraqqī-e Adab |location=Lahore |edition=1st}} Urdu tr. Khwaja Abdul Hameed Yazdani, Lahore: Majlis-e Taraqqī-e Adab, 1966.

===Works in English===
* {{cite book |ref=harv |last1=["Albiruni |first1=A. H."] |title=Makers of Pakistan and Modern Muslim India ''(Based on ''Mauj-e Kausar'')'' |date=1950 |publisher= |location= |edition=1st}} Pseudonymous.
* {{cite book |ref=harv |last1=Ikram |first1=S. M. |title=Modern Muslim India and the Birth of Pakistan (1858-1951) ''(Rev. ed. of ''Makers of Pakistan'')'' |date=1965 |publisher= |location= |edition=1st}} An unauthorized reprint of this work enjoys wide circulation with the title ''Indian Muslims and Partition of India'' (New Delhi: Atlantic Publishers & Distributors, 1992); available on [https://books.google.com.pk/books?id=7q9EubOYZmwC&pg=PP1&lpg=PR3&focus=viewport&dq=Indian+Muslims+And+Partition+Of+India++by+S.M.+Ikram#v=onepage&q=Indian%20Muslims%20And%20Partition%20Of%20India%20%20by%20S.M.%20Ikram&f=false Google Books].
* {{cite book |ref=harv |last1=Ikram |first1=S. M. |title=History of Muslim Civilization in India and Pakistan (711-1858 A.D.) |date=1962 |publisher= |location= |edition=1st}}
* {{cite book |ref=harv |last1=[Embree |first1=with A. T.] |title=Muslim Civilization in India ''(Abridged American Summary of ''History of Muslim Civilization'')'' |date=1964 |publisher= |location= |edition=1st}}
* {{cite book |ref=harv |last1=Ikram |first1=S. M. |title=Muslim Rule in India and Pakistan (711-1858 A.D.) ''(Revised and expanded version of ''Muslim Civilization'')'' |date=1966 |publisher= |location= |edition=1st}}
* {{cite book |ref=harv |last1=[Spear |first1=with Percival, eds.] |title=The Cultural Heritage of Pakistan |date=1955 |publisher=Oxford University Press |location=Karachi |edition=1st}}

==Notes==
{{notelist |2}}

== References ==
{{Reflist}}
* {{cite book |ref=harv |last1=Abbott |first1=Freeland |title=Islam and Pakistan |date=1968 |publisher=Cornell University |location=Ithaca |url= |accessdate=}}
* {{cite book |last1=Baloch |first1=N. A. |title=''Historical Writings on Pakistan: Tradition and Progress'', ''in A. H. Dani, ed.'', Historical Writings on Pakistan (Methodology and Interpretation), Vol. II: Special Seminar Papers |date=1974 |publisher=University of Islamabad Press |location=Islamabad |pages=124–153}}
* {{cite journal |last1=Farīd |first1=Ibn-e |title=Shiblī chūñ ba khalvat mī ravad (When Shibli Goes into Solitude) |journal=Sahifa (Shibli Number) |date=Jul–Dec 2014 |volume=218-219 |issue= |pages=101–140}}
* {{cite journal |last1=Fārūqī |first1=Shams-ur-Rahmān |title=Shiblī kī Fārsī Ghazal (Shibli's Persian Poetry) |journal=Sahifa (Shibli Number) |date=Jul–Dec 2014 |volume=218-219 |issue= |pages=340–351}}
* {{cite journal |last1=Khan |first1=Professor Hameed Ahmad |title=Sheikh Muhammad Ikram Marhūm ''(The Late Sheikh Muhammad Ikram)'' |journal=Al-Mu`ārif |date=1973 |issue=Feb-Mar}} Cited in Moazzamuddin, ''Life'', 16.
* {{cite book |last1=Knighton |first1=W. |title=The Private Life of an Eastern King |date=1921 |publisher=Humphrey Milford, Oxford University Press |location=London}}
* {{cite book |last1=Lambert-Hurley |first1=Siobhan |last2=Sharma |first2=Sunil |title=Atiya's Journeys: A Muslim Woman from Colonial Bombay to Edwardian Britain |date=2010 |publisher=Oxford University Press |location=New Delhi |isbn=9780198068334 |edition=1st}}
* {{cite book |ref=harv |last1=Latif |first1=Sayyid Abdul |title=Ghalib: A Critical Appreciation of his Life and Urdu Poetry |date=1928 |publisher=Chandrakanth Press |location=Hyderabad, Deccan |url= |accessdate=}} 2nd ed. 1929. An [https://rekhta.org/ebooks/ghalib-dr-syed-abdul-lateef-ebooks Urdu translation] by Syed Mueenuddin Qureshi appeared in 1932.
* {{cite book |ref=harv |last1=Lațīf |first1=Sayyid `Abdu'l |title=The Influence Of English Literature On Urdu Literature |date=1924 |publisher=Forster Groom And Company Limited |url=https://archive.org/details/influenceofengli031348mbp |accessdate=24 February 2016}}
* {{cite web |last1=[[Barbara D. Metcalf|Metcalf]] |first1=Barbara D. |title=The Study of Muslims in South Asia, A talk at the University of California at Santa Barbara, December 2, 2005 |url=http://www.columbia.edu/itc/mealac/pritchett/00islamlinks/ikram/part0_metcalfintro.html |website=Columbia University, New York |accessdate=24 February 2016}}
* {{cite book |ref=harv |last1=Muazzamuddin |first1=Muhammad |title=: Ikrām Nāma: Ya`nī Sheikh Muhammad Ikrām kē Halāt-i Zindagī aur Unkī `Ilmī va Adabī Khidmat kā Tafsīlā Jā'iza ''(The Ikram Epistle: i.e. Sheikh Muhammad Ikram's Biography and a Detailed Review of his Educational and Literary Services)'' |date=2010 |publisher=Ejukeshnal Pablishing Haus |location=Nai Dihli |isbn=8182236851 |edition=1st |url=http://isbnplus.com/8182236851 |accessdate=24 February 2016}} Originally presented as the author's Ph.D. Thesis, Department of Urdu, Aligarh Muslim University, 1994; page numbers cited refer to the thesis.
* {{cite book |ref=harv |last1=Muazzamuddin |first1=Muhammad |title=Mauj-e Kausar kā Tanqīdī Mutāla`a ''(A Critical Study of ''Mauj-e Kausar''), M.Phil. Dissertation, Department of Urdu'' |date=1990 |publisher=Aligarh Muslim University |url=http://amu.informaticspublishing.com/5638/ |accessdate=24 February 2016}}
* {{cite book |ref=harv |last1=Nasr |first1=S. V. R. |title=Mawdudi & the Making of Islamic Revivalism |date=1966 |publisher=Oxford University Press |location=New York |isbn=9780195357110 |url= |accessdate=}}
* {{cite news |last1=[[Rauf Parekh|Parekh]] |first1=Rauf |title=Literary Notes: Atiya Fyzee, Shibli and Saheefa's Special Issue |url=http://www.dawn.com/news/1189574 |accessdate=24 February 2016 |work=Dawn |agency=Karachi |date=June 22, 2015}}
* {{cite web |last1=Pritchett |first1=Frances W. |title=Introduction |url=http://www.columbia.edu/itc/mealac/pritchett/00islamlinks/ikram/part0_fwpintro.html |publisher=Columbia University |location=New York |accessdate=24 February 2016}}
* {{cite journal |last1=Salmān Nadvī |first1=Sayyid |title=''`Allāma Sayyid Sulaimān Nadvī kē Maulānā Shiblī Naumānī aur Maulānā Ashraf `Alī Thānwī kē mā bayn Ta`alluqāt kī Sarguzasht'' (An Account of the Relations between `Allāma Sayyid Sulaimān Nadvī and Maulānā Ashraf `Alī Thānwī), in 2 Parts |journal=Ma`ārif |date=January–February 2016 |volume=197 |issue=1: pp. 9-22 & 2: pp. 85-97}}
* {{cite book |last1=Sleeman |first1=Lt. Col. W. H. |title=Rambles and Recollections of an Indian Official'', 2 vols'' |date=1844 |publisher=J. Hatchard & Son |location=London}}
* {{cite book |ref=harv |last1=[[Sulaiman Nadvi|Sulaimān Nadvī]] |first1=Sayyid |title=Hayat-e Shibli ''(Life of Shibli)'' |date=1943 |publisher=Dārul Musannifīn |location=Ā`zamgarh |url= |accessdate=}}
* {{cite book |ref=harv |last1=[[Richard Symonds (academic)|Symonds]] |first1=Richard |title=The Making of Pakistan |date=1950 |publisher=Faber & Faber |location=London |isbn=9781299035324 |url= |accessdate=}}
* {{cite journal |last1=Wasti |first1=S. Razi |title=Book Reviews: S.M. Ikram, Modern Muslim India and the Birth of Pakistan, Institute of Islamic Culture, Lahore, 1977 |journal=Iqbal Review |date=January 1978 |volume=22 |issue=4}}
* {{cite book |ref=harv |last1=[[Mortimer Wheeler|Wheeler]] |first1=R. E. M. |title=Five Thousand Years of Pakistan |date=1950 |publisher=Christopher Johnson |location=London |isbn= |url= |accessdate=}}

== External links ==
* {{worldcat id|lccn-n90725263}}
* [http://www.columbia.edu/itc/mealac/pritchett/00islamlinks/ikram/index.html ''Muslim Civilization in India'', by S. M. Ikram, ed. A. T. Embree (full text at Columbia University website)]

{{DEFAULTSORT:Ikram, S. M.}}
<!-- Categories -->
[[Category:Articles created via the Article Wizard]]
[[Category:Pakistani people]]
[[Category:1908 births]]
[[Category:1973 deaths]]
[[Category:Pakistani scholars]]