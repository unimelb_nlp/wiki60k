{{Use dmy dates|date=October 2013}}
{{Infobox military conflict
| conflict = Battle of Kaiapit
| partof = [[Markham and Ramu Valley – Finisterre Range campaign]], [[World War II]]
| image = [[File:Kaiapit fly in 057499.jpg|300px|Australian soldiers from the 2/16th Infantry Battalion arriving at Kaiapit after the area was captured by the 2/6th Independent Company |alt=A line of Dakotas on a grass airstrip. Men wearing slouch hats file off a plane. A jeep drives along the strip. Other men in uniform and civilians look on.]]
| caption = Australian soldiers from the 2/16th Battalion arriving at Kaiapit on 20 September after the area was captured by the 2/6th Independent Company
| date = 19–20 September 1943
| place = [[Kaiapit]], [[Territory of New Guinea]]
| result = [[Allies of World War II|Allied]] victory
| status =
| combatant1 =
  {{plainlist |
* {{flag|Australia}}
* {{flag|United States|1912}}
  }}
| combatant2 = {{flag|Empire of Japan|name=Imperial Japan}}
| combatant3 =
| commander1 = {{Flag icon|Australia}} [[Gordon Grimsley King|Gordon King]]
| commander2 = {{Flag icon|Empire of Japan}} Tsuneo Yonekura {{KIA}}
| commander3 =
| strength1 = 226
| strength2 = 500
| strength3 =
| casualties1 =
  {{plainlist |
* 14 killed
* 23 wounded
  }}
| casualties2 = >214 killed
| casualties3 =
| notes =
| campaignbox =
{{Campaignbox Finisterres}}
{{Campaignbox New Guinea}}
}}
[[File:Eastern New Guinea and New Britain 1944.jpg|thumb|right|Papua, New Guinea and the Solomon Islands]]

The '''Battle of Kaiapit''' was an action fought in 1943 between Australian and [[Empire of Japan|Japanese]] forces in [[New Guinea]] during the [[Markham and Ramu Valley – Finisterre Range campaign]] of [[World War II]]. Following the landings [[landing at Nadzab|at Nadzab]] and [[landing at Lae|at Lae]], the [[Allies of World War II|Allies]] attempted to exploit their success with an advance into the upper [[Markham Valley]], starting with [[Kaiapit]]. The Japanese intended to use Kaiapit to threaten the Allied position at [[Nadzab]], and to create a diversion to allow the Japanese garrison at [[Lae]] time to escape.

The [[2/6th Independent Company (Australia)|Australian 2/6th Independent Company]] flew in to the [[Markham Valley]] from [[Port Moresby]] in 13 [[USAAF]] [[C-47 Dakota]]s, making a difficult landing on a rough airstrip. Unaware that a much larger Japanese force was also headed for Kaiapit, the company attacked the village on 19 September to secure the area so that it could be developed into an airfield. The company then held it against a strong counterattack. During two days of fighting the Australians defeated a larger Japanese force while suffering relatively few losses.

The Australian victory at Kaiapit enabled the [[Australian 7th Division]] to be flown in to the upper Markham Valley. It accomplished the 7th Division's primary mission, for the Japanese could no longer threaten Lae or Nadzab, where a major airbase was being developed. The victory also led to the capture of the entire [[Ramu|Ramu Valley]], which provided new forward fighter airstrips for the air war against the Japanese.

== Background ==

=== Geography ===
The [[Markham Valley]] is part of a flat, elongated depression varying from {{convert|8|to|32|km|mi}} wide that cuts through the otherwise mountainous terrain of the interior of New Guinea, running from the mouth of the [[Markham River]] near the port of Lae, to that of the [[Ramu River]] {{convert|600|km|mi}} away. The two rivers flow in opposite directions, separated by an invisible divide about {{convert|130|km|mi}} from [[Lae]]. The area is flat and suitable for airstrips, although it is intercut by many tributaries of the two main rivers. Between the [[Ramu|Ramu Valley]] and [[Madang]] lies the rugged and aptly named [[Finisterre Range]]s.{{sfn|Dexter|1961|pp=415–416}}

=== Military situation ===
Following the [[landing at Nadzab]], [[General (Australia)|General]] Sir [[Thomas Blamey]], the [[Allies of World War II|Allied]] Land Forces commander, intended to exploit his success with an advance into the upper Markham Valley, which would protect Nadzab from Japanese ground attack, and serve as a jumping off point for an overland advance into the Ramu Valley to capture airfield sites there. On 16 September 1943—the same day that Lae fell—[[Lieutenant General (Australia)|Lieutenant General]] Sir [[Edmund Herring]], commander of [[I Corps (Australia)|I Corps]], [[Major General (Australia)|Major General]] [[George Alan Vasey]], commander of the [[7th Division (Australia)|7th Division]], and [[Major General (United States)|Major General]] [[Ennis Whitehead]], commander of the Advanced Echelon, [[Fifth Air Force]], met at Whitehead's headquarters. Whitehead wanted fighter airstrips established in the [[Kaiapit]] area by 1 November 1943 in order to bring short-range fighters within range of the major Japanese base at [[Wewak]]. The 7th Division's mission was to prevent the Japanese at Madang from using the Markham and Ramu valleys to threaten Lae or Nadzab. Vasey and Herring considered both an overland operation to capture Dumpu, and an airborne operation using paratroops of the [[US Army]]'s [[503rd Infantry Regiment (United States)|503rd Parachute Infantry Regiment]]. Blamey did not agree with their idea of capturing Dumpu first, insisting that Kaiapit be taken beforehand.{{sfn|Horner|1992|pp=269–270}}{{sfn|Dexter|1961|p=414}}

Until a road could be opened from Lae, the Kaiapit area could only be supplied by air and there were a limited number of transport aircraft. Even flying in an airborne engineer aviation battalion to improve the airstrip would have involved taking aircraft away from operations supporting the 7th Division at Nadzab. Moreover, Whitehead warned that he could not guarantee adequate air support for both Kaiapit and the upcoming [[Battle of Finschhafen|Finschhafen operation]] at the same time. However, Herring calculated that the 7th Division had sufficient reserves at Nadzab to allow maintenance flights to be suspended for a week or so after the capture of Kaiapit. He planned to seize Kaiapit with an overland advance from Nadzab by [[Australian commandos|independent companies]], the [[Papuan Infantry Battalion]], and the 7th Division's [[21st Brigade (Australia)|21st Infantry Brigade]].{{sfn|Dexter|1961|p=414}}

Fifth Air Force commander [[Lieutenant General (United States)|Lieutenant General]] [[George Kenney]] later recalled that Colonel David W. "Photo" Hutchison, who had been the air task force commander at [[Marilinan Airfield|Marilinan]] and had moved over to Nadzab to take charge of air activities there, was told to work out the problem with Vasey: "I didn't care how it was done but I wanted a good forward airdrome about a hundred miles further up the Markham Valley. Photo Hutchison and Vasey were a natural team. They both knew what I wanted and Vasey not only believed that the air force could perform miracles but that the 7th Division and the Fifth Air Force working together could do anything."{{sfn|Kenney|1949|p=300}}

The airstrip at Kaiapit was reconnoitred on 11 September 1943 by [[No. 4 Squadron RAAF]], which reported that it was apparently in good condition, with the [[Kunai grass]] recently cut.<ref>RAAF Reconnaissance Report, 11 September 1943, War Diary, 45 Air Liaison Section, AWM52 1/14/40.</ref> Lieutenant Everette E. Frazier, [[USAAF]], selected a level, burned-off area near the Leron River, not far from Kaiapit, and landed in an L-4 [[Piper J-3 Cub|Piper Cub]]. He determined that it would be possible to land C-47 Dakota aircraft there. On 16 September, Hutchison approved the site for Dakotas to land.{{sfn|Watson|1950|p=190}}

== Prelude ==
The [[2/6th Independent Company (Australia)|2/6th Independent Company]] arrived in Port Moresby from Australia on 2 August 1943. The unit had fought in Papua in 1942 in the [[Battle of Buna–Gona]] and had since conducted intensive training in Queensland. The company was under the command of [[Captain (OF-2)|Captain]] [[Gordon Grimsley King|Gordon King]], who had been its second in command at Buna. King received a warning order on 12 September alerting him to prepare for the capture of Kaiapit, and had access to detailed aerial photographs of the area.{{sfn|Bradley|2004|pp=14–16}}

[[File:Army No. 208 Wireless Set.jpg|thumb|left|An Australian soldier with an Army No. 208 Wireless Set. The large box is the radio itself; the small one is the battery. The weapon in the foreground is an [[Owen Gun]].|alt=A man in slouch hat and shirt wearing earphones. In front of him are two boxes, the smaller on top of the larger.]]
An independent company at this time had a nominal strength of 20 officers and 275 other ranks. Larger than a conventional infantry company, it was organised into three platoons, each of three sections, each of which contained two subsections. It had considerable firepower. Each subsection had a [[Bren light machine gun]]. The gunner's two assistants carried rifles and extra 30-round Bren magazines. A sniper also carried a rifle, as did one man equipped with [[rifle grenade]]s. The remaining four or five men carried [[Owen submachine gun]]s. Each platoon also had a section of [[2-inch mortar]]s.{{sfn|Bradley|2004|p=13}}

The company was self-supporting, with its own engineer, signals, transport, and quartermaster sections. The signals section had a powerful but cumbersome [[Wireless Set No. 11]] for communicating with the 7th Division. Powered by [[lead-acid batteries]] which were recharged with petrol generators, it required multiple signallers to carry and the noise was liable to attract the attention of the enemy. The platoons were equipped with the new [[Army No. 208 Wireless Set]]s.{{sfn|Bradley|2004|p=13}} These were small, portable sets developed for the communication needs of units on the move in jungle warfare.{{sfn|Mellor|1958|p=497}} However, the 2/6th Independent Company had not had time to work with them operationally.{{sfn|Bradley|2004|p=13}}

For three days in a row, the 2/6th Independent Company prepared to fly out from Port Moresby, only to be told that its flight had been cancelled due to bad weather. On 17 September 1943, 13 Dakotas of the [[374th Operations Group|US 374th Troop Carrier Group]] finally took off for Leron. King flew in the lead plane, which was piloted by Captain Frank C. Church,{{sfn|Bradley|2004|pp=16–18}} whom Kenney described as "one of Hutchison's 'hottest' troop carrier pilots".{{sfn|Kenney|1949|p=300}} As it came in to land, King spotted patrols from the Papuan Infantry Battalion in the area.{{sfn|Bradley|2004|pp=16–18}}

[[File:Markham Valley.jpg|thumb|Markham and Ramu Valley Operations, September–November 1943. Allied movements are shown in red, Japanese in black. Kaiapit is near the centre of the map. |alt=A map of the Markham and Ramu Valleys in English and Japanese, indicating the Australian advance and Japanese counter movements. The Allied advance from went from Nadzab (lower right) to Kaiapit (centre) and later on to Dumpu (upper left). The Markham and Ramu Rivers run roughly parallel to the Allied advance. To the north of them are the Saruwaged and Finisterre Ranges. Inscriptions are in both Japanese and English.]]
One of the Dakotas blew a tyre touching down on the rough airstrip; another tried to land on one wheel. Its undercarriage collapsed and it made a [[belly landing]]. The former was subsequently salvaged, but the latter was a total loss.{{sfn|Bradley|2004|p=19}} King sent out patrols that soon located Captain [[John Chalk|J. A. Chalk]]'s B Company, Papuan Infantry Battalion, which was operating in the area. That evening Chalk and King received airdropped messages from Vasey instructing them to occupy Kaiapit as soon as possible, and prepare a landing strip for troop-carrying aircraft. Vasey informed them that only small Japanese parties that had escaped from Lae were in the area, and their morale was very low.{{sfn|Dexter|1961|pp=415–416}} Vasey flew in to Leron on 18 September to meet with King. Vasey's orders were simple: "Go to Kaiapit quickly, clean up the Japs and inform division."{{sfn|Dexter|1961|p=417}}

As it happened, the Japanese commander, Major General [[Masutaro Nakai]] of the [[20th Division (Imperial Japanese Army)|20th Division]], had ordered a sizeable force to move to Kaiapit under the command of Major Yonekura Tsuneo. Yonekura's force included the 9th and 10th Companies of the [[78th Infantry Regiment (Imperial Japanese Army)|78th Infantry Regiment]], the 5th Company of the [[80th Infantry Regiment (Imperial Japanese Army)|80th Infantry Regiment]], a heavy machine-gun section, a signals section and an engineer company—a total of about 500 troops.{{sfn|Bradley|2004|p=34}} From Kaiapit it was to threaten the Allied position at Nadzab, creating a diversion to allow the Japanese garrison at Lae time to escape. The main body left Yokopi in the [[Finisterre Range]] on 6 September but was delayed by heavy rains that forced the troops to move, soaking wet, through muddy water for much of the way. Only the advance party of this force had reached Kaiapit by 18 September, by which time Lae had already fallen.{{sfn|Kuzuhara|2004|p=123}} Yonekura's main body, moving by night to avoid being sighted by Allied aircraft, was by this time no further from Kaiapit than King, but had two rivers to cross. Since both were heading for the same objective, a clash was inevitable.{{sfn|Bradley|2004|p=35}}

== Battle ==
[[File:Kaiapit Japanese body 057500.jpg|thumb|Japanese dead at Kaiapit. After the battle 214 Japanese bodies were counted by the Australians around their positions.|alt=Two grass huts and some palm trees. One dead Japanese soldier lies at the base of a palm tree.]]
King assembled his troops at Sangan, about {{convert|16|km|mi|round_to=0}} south of Kaiapit, except for one section under Lieutenant E. F. Maxwell that had been sent ahead to scout the village. On the morning of 19 September, King set out for Kaiapit, leaving behind his quartermaster, transport and engineering sections, which would move the stores left behind at the Leron River first to Sangan and then to Kaiapit on the 20th. He took one section of Papuans with him, leaving Chalk and the rest of his men to escort the native carriers bringing up the stores.{{sfn|Bradley|2004|p=21}}

King's men walked for fifty minutes at a time and then rested for ten. The going was relatively easy insofar as the ground was fairly flat, but the {{convert|2|m|ft|adj=on}} high Kunai grass trapped the heat and humidity and the men were heavily loaded with ammunition.{{sfn|Bradley|2004|p=21}} The company reached Ragitumkiap, a village within striking distance of Kaiapit, at 14:45. While his men had a brief rest, King attempted to contact the large Army No.&nbsp;11 Wireless Set he had left behind at Sangan—and from there Vasey back at Nadzab—with the new Army No.&nbsp;208 Wireless Sets he had brought with him. Unfortunately, King found that their range was insufficient. He also heard shots being fired in the distance and guessed that Maxwell's section had been discovered.{{sfn|Dexter|1961|pp=417–419}}

The 2/6th Independent Company formed up at 15:15 in Kunai grass about {{convert|1200|m|yd}} from Kaiapit. As the company advanced it came under fire from [[Defensive fighting position|foxholes]] on the edge of the village.{{sfn|Dexter|1961|pp=417–419}} A 2-inch mortar knocked out a light machine gun.{{sfn|Bradley|2004|p=21}} The foxholes were outflanked and taken out with [[Mills bomb|hand grenades]] and [[bayonets]]. The Japanese withdrew, leaving 30 dead behind. The Australians suffered two killed and seven wounded, including King, who was lightly wounded.{{sfn|Dexter|1961|pp=417–419}}

[[File:Yarawa of the Royal Papuan Constabulary.jpg|thumb|left|Brigadier I. N. Dougherty (centre) and [[WO2]] H. P. Seale of [[ANGAU]] (right) congratulate "Yarawa" (left) of the [[Royal Papua New Guinea Constabulary|Royal Papuan Constabulary]] for his feat of single-handedly capturing a Japanese sergeant, Hideo Kadota, on 25 September 1943.{{sfn|Bradley|2004|pp=34–35}}|alt=A shirtless Papuan man stands at attention with rifle at slope. Two Australian soldiers wearing shirts and slouch hats stand facing him. ]]
The company established a defensive position for the night. While they were doing so, Lieutenant D. B. Stuart, the commander of one of the Papuan platoons, arrived. They had become concerned when radio contact had been lost and he had been sent to find out what was going on. King ordered him to bring the Papuans up from Sangan with extra ammunition and the No.&nbsp;11 set. At around 17:30, a native appeared with a message for the Japanese commander. The paper was taken from him and he was shot when he tried to escape.{{sfn|Dexter|1961|pp=417–419}} Later, a Japanese patrol returned to Kaiapit, unaware that it was now in Australian hands. They were killed when they stumbled across a Bren gun position. Four more Japanese soldiers returned after midnight. One of them escaped.{{sfn|Bradley|2004|p=29}}

Yonekura and his men had reached Kaiapit after an exhausting night march. Yonekura was aware that the Australians had reached Kaiapit but his main concern was not to be caught in the open by Allied aircraft. Spotting Australian positions in the pre-dawn light, the Japanese column opened fire. A torrent of fire descended on the Australians, who replied sporadically, attempting to conserve their ammunition.{{sfn|Bradley|2004|pp=30–31}} Although he was running low on ammunition, King launched an immediate counter-attack on the Japanese,{{sfn|Dexter|1961|p=420}} which took them by surprise.{{sfn|Bradley|2004|p=44}}

Lieutenant Derrick Watson's C Platoon set out at around 06:15 and advanced to the edge of Village 3, a distance of about {{convert|200|yd}}, before becoming pinned down by heavy Japanese fire. King then sent Captain Gordon Blainey's A Platoon around the right flank, towards the high ground on Mission Hill which overlooked the battlefield. It was secured by 07:30. In the meantime, some of the 2/6th Independent Company's signallers and headquarters personnel gathered together what ammunition they could, and delivered it to C Platoon at around 07:00. C Platoon then fixed bayonets and continued its advance.{{sfn|Dexter|1961|pp=421–422}}

The commander of No. 9 Section of C Platoon, Lieutenant Bob Balderstone, was nicked by a bullet, apparently fired by one of his own men.{{sfn|Bradley|2004|p=37}} He led his section in an advance across {{convert|70|yd}} of open ground, and attacked three Japanese machine gun posts with hand grenades.{{sfn|Dexter|1961|pp=421–422}}{{sfn|Bradley|2004|p=38}} He was later awarded the [[Military Cross]] for his "high courage and leadership".{{sfn|Australian War Memorial, Robert Taylor Balderstone}}{{sfn|London Gazette, 20 January 1944, p. 392}} Lieutenant Reg Hallion led his No. 3 Section of A Platoon against the Japanese positions at the base of Mission Hill.{{sfn|Bradley|2004|p=39}} He was killed in an attack on a machine gun post, but his section captured the position and killed twelve Japanese. By 10:00, the action was over.{{sfn|Dexter|1961|pp=421–422}}

After the action, King's men counted 214 Japanese bodies, and estimated that another 50 or more lay dead in the tall grass.{{sfn|Dexter|1961|p=422}} Yonekura was among the dead.{{sfn|Bradley|2004|p=40}} The Australians suffered 14 killed and 23 wounded. Abandoned equipment included 19 machine guns, 150 rifles, 6 [[Type 89 Grenade Discharger|grenade throwers]] and 12 [[Japanese sword]]s.{{sfn|Dexter|1961|p=422}}

== Aftermath ==

=== Consolidation ===
The 2/6th Independent Company had won a significant victory, but now had 23 wounded and was very low on ammunition. Frazier landed on the newly captured airstrip in his Piper Cub at 12:30.{{sfn|Dexter|1961|p=423}} He rejected the airstrip as unsuitable for Dakotas, and oversaw the preparation of a new airstrip on better ground near Mission Hill.{{sfn|Bradley|2004|p=41}} This was still a difficult approach, as aircraft had to land upwind while avoiding Mission Hill. Although it was not known if the airstrip would be ready, Hutchison flew in for a test landing there the next day, 21 September, at 15:30. He collected the wounded and flew them to Nadzab, and returned an hour later with a load of rations and ammunition. He also brought with him [[Brigadier]] [[Ivan Dougherty]], the commander of the 21st Infantry Brigade, and his headquarters, who took charge of the area. Around 18:00, six more transports arrived.{{sfn|Dexter|1961|p=423}}

Vasey was concerned about the security of the Kaiapit area, as he believed that the Japanese were inclined to continue with a plan once it was in motion.{{sfn|Dexter|1961|p=430}} Taking advantage of good flying weather on 22 September,{{sfn|Dexter|1961|p=423}} 99 round trips were made between Nadzab and Kaiapit.{{sfn|Kenney|1949|p=301}} Most of the [[2/16th Battalion (Australia)|2/16th Infantry Battalion]] and some American engineers were flown in.{{sfn|Dexter|1961|p=423}} The [[2/14th Battalion (Australia)|2/14th Infantry Battalion]] and a battery of the [[2/4th Field Regiment (Australia)|2/4th Field Regiment]] arrived on 25 September, and Brigadier [[Kenneth Eather]]'s [[25th Brigade (Australia)|25th Infantry Brigade]] began to arrive two days later, freeing Dougherty to advance on Dumpu.{{sfn|Dexter|1961|pp=431–434}}

=== Base development ===
[[File:Bulldozer arrives on plane at Kaiapit strip 1943.jpg|thumb|24 September 1943. A bulldozer arrives for use on the Kaiapit strip on a Dakota of the 65th Troop Carrier Squadron. The small, light bulldozers of the US airborne aviation engineer battalions were designed to be air portable in order to perform work on strips in forward areas.|alt=A tiny bulldozer emerges from the rear door of an aircraft onto a grass airstrip. Three men stand around watching.]]
Kaiapit did not become an important airbase. By the time engineering surveys of the area had been completed, as a direct consequence of the victory at Kaiapit, Dougherty's men had captured [[Gusap Airport|Gusap]]. There, the engineers found a well-drained area with soil conditions suitable for the construction of all-weather airstrips, an unobstructed air approach and a pleasant climate. It was therefore decided to limit construction at the swampy and malarial Kaiapit and concentrate on Gusap, where the US 871st, 872nd and 875th Airborne Aviation Engineer Battalions constructed ten airstrips and numerous facilities. Although some equipment was carried on the trek overland, most had to be flown in and nearly all of it was worn out by the time the work was completed. The first [[Curtiss P-40 Warhawk|P-40 Kittyhawk]] fighter squadron began operating from Gusap in November and an all-weather fighter runway was completed in January 1944. The airstrip at Gusap "paid for itself many times over in the quantity of Japanese aircraft, equipment and personnel destroyed by Allied attack missions projected from it."{{sfn|Casey|1951|pp=171–172}}

=== War crimes ===
Three natives were found at Kaiapit who had been tied with rope to the uprights of a native hut and had then been bayoneted.
As a result of the [[Moscow Declaration]], the [[Minister for Foreign Affairs (Australia)|Minister for External Affairs]], Dr. [[H. V. Evatt]], commissioned a report by [[William Webb (judge)|William Webb]] on war crimes committed by the Japanese. Webb took depositions from three members of the 2/6th Independent Company about the Kaiapit incident which formed part of his report, which was submitted to the [[United Nations War Crimes Commission]] in 1944.{{sfn|Webb|1944|pp=261–266}}

=== Results ===
The 2/6th Independent Company had defeated the vanguard of Nakai's force and stopped his advance down the Markham Valley dead in its tracks.{{sfn|Bradley|2004|p=42}} The Battle of Kaiapit accomplished Vasey's primary mission, for the Japanese could no longer threaten Nadzab. It opened the gate to the Ramu Valley for the 21st Infantry Brigade, provided new forward fighter airstrips for the air war against the Japanese,{{sfn|Dexter|1961|p=422}} and validated the Australian Army's new training methods and the organisational emphasis on firepower.{{sfn|Bradley|2004|p=44}}

[[File:Kaiapit flags 057510.jpg|thumb|left|Members of the 2/6th Independent Company display Japanese flags they captured at Kaiapit|alt=A crowd of men, some shirtless, some wearing slouch hats, hold up two rising sun flags. One has Japanese writing on it.]]
Vasey later told King that "We were lucky, we were very lucky." King countered that "if you're inferring that what we did was luck, I don't agree with you sir because I think we weren't lucky, we were just bloody good." Vasey replied that what he meant was that he, Vasey, was lucky.{{sfn|Bradley|2004|p=43}} He confided to Herring that he felt that he had made a potentially disastrous mistake: "it is quite wrong to send out a small unit like the 2/6th Independent Company so far that they cannot be supported."{{sfn|Dexter|1961|p=426}}

The Japanese believed that they had been attacked by "an Australian force in unexpected strength".{{sfn|Willoughby|1966|p=225}} One Japanese historian, Tanaka Kengoro, went so far as to argue that the mission of the Nakai Detachment—to threaten Nadzab so as to draw Allied attention away from the troops escaping from Lae—was achieved; this argument passed over the fact that Nakai fully intended to hold Kaiapit, just as the Allies planned to secure it as a base for future operations.{{sfn|Horner|1992|p=272}} The Australian historian, David Dexter, concluded that the "leisurely Nakai was outwitted by the quick-thinking and aggressive Vasey."{{sfn|Dexter|1961|p=425}} In the end, Vasey had moved faster, catching the Japanese off balance. The credit for getting to Kaiapit went first to the USAAF aircrews that managed to make a difficult landing on the rough airstrip at Leron. The 2/6th Independent Company proved to be the ideal unit for the mission, as it combined determined leadership with thorough training and effective firepower.{{sfn|Bradley|2004|p=43}}

For his part in the battle, King was awarded the [[Distinguished Service Order]] on 20 January 1944.{{sfn|London Gazette, 20 January 1944, p. 391}}{{sfn|Australian War Memorial, Gordon Grimley King}} He considered it a form of unit award, and later regretted not asking Whitehead for an American [[Presidential Unit Citation (United States)|Distinguished Unit Citation]], such as was awarded to D Company of the [[6th Battalion, Royal Australian Regiment]], for a similar action in the [[Battle of Long Tan]] in 1966.{{sfn|Bradley|2004|p=42}}

== Notes ==
{{reflist|20em}}

== References ==
{{refbegin}}
* {{cite book
  | last = Bradley
  | first = Phillip
  | year = 2004
  | title = On Shaggy Ridge. The Australian 7th Division in the Ramu Valley: From Kaiapit to the Finisterre Ranges
  | publisher = Oxford University Press
  | location = Melbourne
  | isbn = 0-19-555100-1
  | oclc = 56601964
  | ref = harv
  }}
* {{cite book
  | editor-link = Hugh John Casey
  | editor-last = Casey
  | editor-first = Hugh J.
  | year = 1951
  | title = Airfield and Base Development
  | series = Engineers of the Southwest Pacific
  | publisher = United States Government Printing Office
  | location = Washington, D.C.
  | oclc = 16114629
  | ref = harv
  }}
* {{cite book
  | last = Dexter
  | first = David
  | year = 1961
  | title = The New Guinea Offensives
  | series = [[Australia in the War of 1939–1945]]. Series 1 – Army
  | location = Canberra
  | publisher = Australian War Memorial
  | url = https://www.awm.gov.au/collection/RCDIG1070205/
  | oclc = 2028994
  | ref = harv
  }}
* {{cite book
  | last = Horner
  | first = David
  | authorlink = David Horner
  | year = 1992
  | title = General Vasey's War
  | publisher = Melbourne University Press
  | location = Melbourne
  | isbn = 0-522-84462-6
  | oclc = 26195398
  | ref = harv
  }}
* {{cite book
  | last = Kenney
  | first = George C.
  | authorlink = George Kenney
  | year = 1949
  | title = General Kenney Reports: A Personal History of the Pacific War
  | publisher = Duell, Sloan and Pearce
  | location = New York City
  | url = http://handle.dtic.mil/100.2/ADA442853
  | accessdate = 3 October 2011
  | isbn = 0-912799-44-7
  | oclc = 477957447
  | ref = harv
  }}
* {{cite book
  | first= Kazumi
  | last= Kuzuhara
  | year = 2004
  | contribution = The Nakai Contingency Unit and the Battles of Kankirei Ridge
  | pages = 118–135
  | editor-last = Dennis
  | editor-first = Peter
  | editor2-last = Grey
  | editor2-first = Jeffrey
  | title = The Foundations of Victory: The Pacific War 1943–1944
  | publisher = Army History Unit
  | location = Canberra
  | url = https://web.archive.org/web/20110604021834/http://www.army.gov.au/AHU/docs/The_Foundations_of_Victory_Kuzuhara.pdf
  | accessdate = 3 October 2011
  | isbn = 0-646-43590-6
  | oclc = 59714455
  | ref = harv
  }}
* {{cite book
  | last = Mellor
  | first = D. P.
  | year = 1958
  | title = The Role of Science and Industry
  | series = [[Australia in the War of 1939–1945]]. Series 4 – Civil
  | location = Canberra
  | publisher = Australian War Memorial
  | url = https://www.awm.gov.au/collection/RCDIG1070217/
  | oclc = 4092792
  | ref = harv
  }}
* {{cite book
  | last = Watson
  | first = Richard L.
  | year = 1950
  | editor-last = Craven
  | editor-first = Wesley Frank
  | editor2-last = Cate
  | editor2-first = James Lea
  | contribution = Huon Gulf and Peninsula
  | pages = 163–202
  | title = Vol. IV, The Pacific: Guadalcanal to Saipan, August 1942 to July 1944
  | series = The Army Air Forces in World War II
  | publisher = University of Chicago Press
  | location = Chicago
  | url = http://www.ibiblio.org/hyperwar/AAF/IV/AAF-IV-6.html
  | accessdate = 20 October 2006
  | oclc = 5732980
  | ref = harv
  | archiveurl= https://web.archive.org/web/20061116053114/http://www.ibiblio.org/hyperwar/AAF/IV/AAF-IV-6.html| archivedate= 16 November 2006 <!--DASHBot-->| deadurl= no}}
* {{cite book
  | last = Webb
  | first = William
  | authorlink = William Webb (judge)
  | date = October 1944
  | title = A Report on War Crimes Against Australians Committed by Individual Members of the Armed Forces of the Enemy
  | id = NAA (ACT): A10950 1
  | ref = harv
  }}
* {{cite book
  | editor-last = Willoughby
  | editor-first = Charles A.
  | editor-link = Charles A. Willoughby
  | year = 1966
  | title = Japanese Operations in the Southwest Pacific Area, Volume II – Part I
  | work = Reports of General MacArthur
  | publisher = United States Army Center of Military History
  | location = Washington, D.C.
  | url = http://www.history.army.mil/books/wwii/MacArthur%20Reports/MacArthur%20V2%20P1/macarthurv2.htm
  | accessdate = 12 February 2008
  | oclc = 62685965
  | ref = harv
  | archiveurl= https://web.archive.org/web/20080125072601/http://www.history.army.mil/books/wwii/MacArthur%20Reports/MacArthur%20V2%20P1/macarthurv2.htm| archivedate= 25 January 2008 <!--DASHBot-->| deadurl= no}}

* {{cite web
  | title = Recommendation for Robert Taylor Balderstone to be awarded a Military Cross
  | work = Recommendations: Second World War
  | publisher = Australian War Memorial
  | format = PDF
  | url = http://www.awm.gov.au/collection/records/awm192/awm192-309-0179.pdf
  | archiveurl = https://web.archive.org/web/20120119083058/http://www.awm.gov.au/collection/records/awm192/awm192-309-0179.pdf
  | archivedate = 2012-01-19
  | accessdate = 22 December 2011
  | ref = {{sfnRef|Australian War Memorial, Gordon Grimley King}}
  }}
* {{cite web
  | title = Recommendation for Gordon Grimley King to be awarded a Distinguished Service Order
  | work = Recommendations: Second World War
  | publisher = Australian War Memorial
  | format = PDF
  | url = http://www.awm.gov.au/cms_images/awm192/00303/003030767.pdf
  | accessdate = 22 December 2011
  | ref = {{sfnRef|Australian War Memorial, Robert Taylor Balderstone}}
  }}

* {{wikicite
  | reference =
    {{London Gazette
    | issue = 36337
    | supp = yes
    | startpage = 391
    | date = 20 January 1944
    | accessdate = 22 December 2011
    }}
  | ref = {{sfnRef|London Gazette, 20 January 1944, p. 391}}
  }}
* {{wikicite
  | reference =
    {{London Gazette
    | issue = 36337
    | supp = yes
    | startpage = 392
    | date = 20 January 1944
    | accessdate = 22 December 2011
    }}
  | ref = {{sfnRef|London Gazette, 20 January 1944, p. 392}}
  }}
{{refend}}

{{Coord|6|16|S|146|15|E|type:city|name=Kaiapit|display=title}}

{{Featured article}}

{{DEFAULTSORT:Kaiapit, Battle Of}}
[[Category:1943 in Papua New Guinea]]
[[Category:Battles and operations of World War II involving Papua New Guinea]]
[[Category:Battles of World War II involving Australia]]
[[Category:Battles of World War II involving Japan]]
[[Category:Battles of World War II involving the United States]]
[[Category:Conflicts in 1943]]
[[Category:South West Pacific theatre of World War II]]