{{Use dmy dates|date=February 2013}}
{{Infobox journal
| title = Herpetological Monographs
| cover = 
| editor = Todd Reeder
| discipline = [[Herpetology]]
| abbreviation = Herpetol. Monogr.
| publisher = [[Herpetologists' League]]
| country = United States
| frequency = Annually
| history = 1982-present
| openaccess = 
| license =
| impact = 2.818
| impact-year = 2011
| website = http://www.hljournals.org/
| link1 = http://www.hljournals.org/loi/hmon
| link1-name = Online archive
| link2 = 
| link2-name =
| JSTOR = 07331347
| OCLC = 52427622
| LCCN = 89657063
| CODEN = 
| ISSN = 0733-1347
| eISSN = 1938-5137
}}
'''''Herpetological Monographs''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] covering the [[zoology]] of [[amphibians]] and [[reptiles]]. It is published annually by the [[Herpetologists' League]] and was established in 1982. The League also publishes a quarterly peer-reviewed journal, ''[[Herpetologica]]''.<ref>{{cite book|last1=Schmidt |first1=Diane |last2=Bell |first2=George H. |title=Guide to Reference and Information Sources in the Zoological Sciences |year=2003 |publisher=Greenwood Publishing Group |location=Westport, CT |isbn=1563089777 |pages=221-222 |url=https://books.google.com/books?id=hnVlcs3wHbIC&pg=PA221&dq=herpetological+monographs&hl=en&sa=X&ei=oDX5T_eNKdK48gPF5aT8Bg&ved=0CE8Q6AEwBQ#v=onepage&q=herpetological%20monographs&f=false}}</ref> Since 2008, the [[editor-in-chief]] has been Tod Reeder ([[University of California, San Diego]]). The previous editor was [[Lee Fitzgerald]] ([[Texas A&M University]]).<ref>{{cite web |title=Minutes of the Board of Trustees Meeting of the Herpetologists’ League, 23 July 2008 |url=http://www.herpetologistsleague.org/dox/BoTmin2008.pdf |publisher=Herpetologists' League |accessdate=8 July 2012}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2011 [[impact factor]] of 2.818, ranking it 13th out of 146 journals in the category "Zoology".<ref name=WoS>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Zoology |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2012-07-08 |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official|http://www.hljournals.org/}}
* [http://www.herpetologistsleague.org/en/index.php Herpetologists' League]

[[Category:Herpetology journals]]
[[Category:Publications established in 1982]]
[[Category:Annual journals]]
[[Category:English-language journals]]
[[Category:Academic journals published by non-profit organizations]]
[[Category:1982 establishments in the United States]]