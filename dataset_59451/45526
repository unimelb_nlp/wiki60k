{{redirect|Aviator}}
{{redirects|Aviatrix|a discussion of the role of women in aviation and aeronautics|Women in aviation}}
{{multiple issues|
{{undue|date=November 2014}}
{{refimprove|date=March 2015}}
}}
{{use mdy dates|date=May 2012}}
[[File:Lt Mike Hunter 1.jpg|thumb|right|[[United States Army|U.S. Army]] test pilot [[Lieutenant|Lt.]] F.W. "Mike" Hunter wearing a [[flight suit]] in October 1942]]

A '''pilot''' or '''aviator''' is a person who controls the flight of an [[aircraft]] by operating its [[Aircraft flight control system|directional flight controls]]. While other [[aircrew|aircrew members]] such as [[navigator|navigators]] or [[flight engineer|flight engineers]] are also considered aviators, because they are involved in operating the aircraft´s flight systems, they are not pilots and do not command a flight or aircraft.  Other aircrew members such as [[Flight attendant|flight attendants]], [[Aircraft mechanic|mechanics]] and ground crew, are not classified as aviators.

In recognition of the pilots' qualifications and responsibilities, most militaries and many airlines worldwide award [[aviator badge]]s to their pilots, and this includes [[naval aviator]]s.

==History==
[[Image:Basket and crew.jpg|thumb|left|[[Hot air balloon]] pilot and passenger in basket]]
{{expand section|date=March 2015}}
The first recorded use of the term ''aviator'' (''aviateur'' in French) was in 1887, as a variation of "aviation", from the Latin ''avis'' (meaning ''bird''), coined in 1863 by G. de la Landelle in ''Aviation Ou Navigation Aérienne'' ("Aviation or Air Navigation"). The term '''aviatrix''' (''aviatrice'' in French), now archaic, was formerly used for a female aviator. These terms were used more in the [[aviation history|early days of aviation]], when airplanes were extremely rare, and connoted bravery and adventure. For example, a 1905 reference work described the [[Wright brothers]]' first airplane: "The weight, including the body of the aviator, is a little more than 700 pounds".<ref>{{cite encyclopedia |contribution=Aeronautics in 1904 |work=Collier's Self-Indexing Annual |url=https://books.google.com/books?id=Eb86AQAAMAAJ&pg=PA6 |page=6 |location=New York |publisher=P. F. Collier & Son |year=1905 }}</ref>

To ensure the safety of people in the air and on the ground, early [[aviation]] soon required that aircraft be under the operational control of a properly trained, certified pilot at all times, who is responsible for the safe and legal completion of the flight. The  [[Aéro-Club de France]] delivered the first certificate to [[Louis Blériot]] in 1908—followed by [[Glenn Curtiss]], [[Léon Delagrange]], and [[Robert Esnault-Pelterie]]. The absolute authority given to the "[[pilot in command]]" derives from that of a ship's captain.{{Citation needed|date=February 2007}}

==Civilian==
[[File:Transaero 777 landing at Sharm-el-Sheikh Pereslavtsev.jpg|thumb|Pilots landing a [[Boeing 777]]]]
Civilian pilots fly aircraft of all types privately for pleasure, charity, or in pursuance of a business, and/or commercially for non-scheduled (charter) and scheduled passenger and cargo air carriers (airlines), corporate aviation, agriculture (crop dusting, etc.), forest fire control, law enforcement, etc. When flying for an airline, pilots are usually referred to as airline pilots, with the [[pilot in command]] often referred to as the ''captain''.

===Africa and Asia===
In some countries, such as [[Pakistan]], [[Thailand]] and several [[Africa]]n nations, there is a strong relationship between the military and the principal national airlines, and many airline pilots come from the military; however, that is no longer the case in the [[United States]] and [[Western Europe]].{{citation needed|date=June 2012}} While the flight decks of U.S. and European airliners do have ex-military pilots, many pilots are civilians. Military training and flying, while rigorous, is fundamentally different in many ways from civilian piloting.

===Canada===

Pilots [[Pilot licensing in Canada|licensing]] in Canada is similar to the United States.

The [[Aeronautics Act]] of 1985 and [[Canadian Aviation Regulations]] provide rules for pilots in Canada.

Retirement age is provided by each airline with some set to age 60, but changes to the [[Canadian Human Rights Act]] have restricted retirement age set by the airlines.<ref>{{cite web|author=Vanessa Lu |url=http://www.thestar.com/business/2013/01/24/air_canada_pilots_can_continue_flying_past_age_60_under_new_rules.html |title=Air Canada pilots can continue flying past age 60 under new rules |newspaper=Toronto Star |date=January 24, 2013 |accessdate=November 5, 2013}}</ref>

===United States===
In 1930, the [[Air Commerce Act]] established pilot licensing requirements for American civil aviation.

Commercial airline pilots in the United States have a mandatory retirement age of 65, having increased from age 60 in 2007.<ref>{{cite news | url=http://www.dallasnews.com/sharedcontent/dws/bus/stories/DN-pilots_15bus.ART0.State.Edition1.2a48e73.html | title=Retirement age raised to 65 in nick of time for pilots turning 60 | work=The Dallas Morning News | date=December 15, 2007 | accessdate=October 15, 2009 | last=Maxon | first=Terry}}</ref>

==Military==
[[Image:USAF pilot.jpg|thumb|left|[[F-16]] pilot in flight]]
Military pilots fly with the armed forces of a government or [[nation-state]]. Their tasks involve [[combat]] and non-combat operations, including direct hostile engagements and support operations. Military pilots undergo specialized training, often with [[weapon]]s. Examples of military pilots include [[fighter pilot]]s, bomber pilots, transport pilots, [[test pilot]]s and [[astronaut]]s. Military pilots also serve as flight crews on aircraft for government personnel, such as [[Air Force One]] and [[Air Force Two]] in the United States.

Military pilots are trained with a different syllabus than civilian pilots, which is delivered by military instructors. This is due to the different aircraft, flight goals, flight situations and chains of responsibility. Many military pilots do transfer to civilian-pilot qualification after they leave the military, and typically their military experience provides the basis for a civilian pilot's license.

==Unmanned aerial vehicles==
{{further|Unmanned aerial vehicle#Degree of autonomy}}
[[File:CBP unmanned aerial vehicle control.jpg|thumb|Operators in a control room pilot and monitor video feeds from a remotely piloted UAV.]]
[[Unmanned aerial vehicles]] (UAVs, also known as "drones") operate without a pilot on-board and are classed into two categories: autonomous aircraft that operate without active<!-- "active" should be included, because I'm sure most autonomous UAVs will have some form of link with a human operator that results in specific actions, eg. giving a UAV a location to travel to while it is in flight --> human control during flight and remotely piloted UAVs which are operated remotely by one or more persons. The person controlling a remotely piloted UAV may be referred to as its pilot or operator. Depending on the sophistication and use of the UAV, pilots/operators of UAVs may require certification or training, but are generally not subject to the licensing/certification requirements of pilots of manned aircraft.

Most jurisdictions have restrictions on the use of UAVs which have greatly limited their use in controlled airspace; UAVs have mostly been limited to military and hobbyist use. In the United States, use of UAVs is very limited in controlled airspace (generally, above 400&nbsp;ft/122m<!-- 400ft is exact, so 121.92m rounds to 122m --> and away from airports) and the FAA prohibits nearly all commercial use. Once regulations are made to allow expanded use of UAVs in controlled airspace, there is expected to be a large surge of UAVs in use and, consequently, high demand for pilots/operators of these aircraft.<ref>{{cite news|last1=Rooney|first1=Ben|title=Drone pilot wanted: Starting salary $100,000|url=http://money.cnn.com/2014/11/25/news/drone-pilot-degree/|accessdate=March 24, 2015|work=CNN|date=November 25, 2014}}</ref>

==Space==
The general concept of an airplane pilot can be applied to [[human spaceflight]], as well. The spacecraft pilot is the [[astronaut]] who directly controls the operation of a [[spacecraft]], while located within that same craft (not remotely). This term derives directly from the usage of the word "pilot" in aviation, where it is synonymous with "aviator". Note that on the N.A.S.A [[Space Shuttle]], the term "pilot" is analogous to the term "[[co-pilot]]" in aviation, as the "[[Commander#NASA rank|commander]]" has ultimate responsibility for the shuttle.

==Pilot certifications==
[[File:RAF Pilot Training in Cockpit of Nimrod Aircraft MOD 45152088.jpg|thumb|Military aviation training in a [[Royal Air Force]] Nimrod aircraft]]
{{Further|Pilot licensing and certification}}
Pilots are required to go through many hours of [[flight training]] and theoretical study, that differ depending on the country. The first step is acquiring the [[Private Pilot License]] (PPL), or Private Pilot Certificate.  This takes at least 40 hours of flight time with a Certified Flight Instructor (CFI).

In the United States, an LSA ([[Light-sport aircraft|Light Sport Aircraft]]) license can be obtained in at least 20 hours of flight time.

The next step in a pilot's progression is either [[Instrument Rating]] (IR), or Multi-Engine Rating (MEP) endorsements.

If a professional career or professional-level skills are desired, a [[Commercial Pilot License]] (CPL) endorsement would also be required. To captain an airliner, one must obtain an [[Airline Transport Pilot License]] (ATPL). After 1 August 2013, even when being a First Officer (FO), an ATPL is required.<ref>{{cite news|last1=Pope|first1=Stephen|title=FAA Finalizes ATP Rule for First Officers|url=http://www.flyingmag.com/news/faa-finalizes-atp-rule-first-officers|accessdate=October 15, 2014|publisher=Flying Magazine|date=July 11, 2013}}</ref>

Some countries/carriers require/use a [[Multi Crew Coordination]] (MCC).

== See also ==
{{Wikipedia books|Aviation}}

* [[Aircrew]] (flight crew)
* [[Airline pilot uniforms]]
* [[Air safety]]
* [[Pilot Fatigue]]
* [[IMSAFE]] (mnemonic for pilot's fitness to fly)
* [[List of aerospace engineers]]
* [[List of aviators]]
* [[List of Russian aviators]]
* [[Women of Aviation Worldwide Week]], an international celebration of all women of aviation

== References ==

{{Reflist|30em}}

==External links==
{{Commons category inline|Aviators}}
* [http://www.womenofaviationweek.org/five-decades-of-women-pilots-in-the-united-states-how-did-we-do/ U.S. Women Pilots Statistics 1960–2010]

{{Commercial aviation}}

{{Authority control}}

[[Category:Aviators| ]]
[[Category:Military aviation occupations]]
[[Category:Occupations in aviation]]