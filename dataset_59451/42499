{|{{Infobox Aircraft Begin
 | name= CAMit 3300 
 | image=File:CAMit-3300.jpg
 | caption=CAMit 3300
}}
{{Infobox Aircraft Engine
 | type = [[Piston]] [[aircraft engine]]
 | national origin = Australia
 | manufacturer = [[CAMit Pty Ltd]]
 | major applications = [[Light Sport Aircraft|Light sport aircraft]]<br />[[ultralight aviation|Ultralight aircraft]]
 |variants with their own articles= 
}}
|}

The '''CAMit 3300''' is an [[Australia]]n lightweight [[Four-stroke engine|four-stroke]],  6-cylinder, horizontally-opposed air-cooled aircraft engine. Direct drive and using a [[Overhead valve engine|solid-lifter]] valvetrain, the 3300 develops {{convert|127|hp|kW|0|abbr=on}} at 3300rpm.

The engine was manufactured by [[CAMit Pty Ltd]], of [[Bundaberg]], [[Queensland]], [[Australia]].<ref>{{cite web|title=CAMit|url=https://camitaeroengines.net/pages/about-us|accessdate=2016-08-01}}</ref> CAMit has manufactured engines<ref name=":0" /> for Jabiru since the Italian-sourced engines used by Jabiru became unavailable in 1991.<ref>{{Cite book|title=Sport Pilot, February 2016|last=|first=|publisher=Recreational Aviation Australia|year=2016|isbn=|location=https://www.raa.asn.au/storage/54february-2016.pdf|pages=11|via=}}</ref><ref>{{Cite web|url=http://web.aeromech.usyd.edu.au/AERO1400/Jabiru_Construction/jabiru.html|title=Background to the Development of the Jabiru|last=|first=|date=|website=web.aeromech.usyd.edu.au|publisher=|access-date=2016-08-12}}</ref>  After receiving requests for and producing [[Aftermarket (automotive)|aftermarket]] Jabiru engine parts, CAMit went on to produce whole engines incorporating these modifications in late 2013.<ref name=":0">{{Cite web|url=https://camitaeroengines.net/pages/new-camit-aero-engines|title=New CAMit Aero Engines|website=CAMit Aero Engines|access-date=2016-08-04 |archiveurl = https://web.archive.org/web/20160128174309/http://camitaeroengines.net/pages/new-camit-aero-engines |archivedate = 28 January 2016}}</ref><ref>{{Cite web|url=http://www.kitplanes.com/issues/32_2/buyers_guide/2015-Engine-Buyers-Guide-Traditional-and-Conversion-Engines_21176-1.html|title=KITPLANES The Independent Voice for Homebuilt Aviation - 2015 Engine Buyer's Guide: Traditional and Conversion Engines - KITPLANES Article|website=www.kitplanes.com|access-date=2016-08-07}}</ref>

Many parts are interchangeable with the original components from [[Jabiru 3300]] engine. However, in terms of component design, lubrication, valve train operation and metallurgy, the CAE engine is a completely different motor to the Jabiru design.<ref>{{Cite book|title=AirSport, January 2015|last=Garland|first=Ken|publisher=SAAA|year=2015|isbn=|location=http://saaa.com/Portals/0/Airsport/AirSport_Jan15.pdf|pages=28|via=}}</ref><ref name=":4" />

The CAMit series of engines are classed as Experimental. Each serial number includes 'SLRE', designating 'Solid-Lifter Revised Experimental'.

CAMit Pty Ltd ceased operations and was placed in receivership in October 2016.<ref>{{Cite web|url=http://jabiruna.com/demise-of-camit-aero-engines/|title=Demise of CAMit Aero Engines|website=jabiruna.com|access-date=2016-11-23}}</ref><ref>{{Cite news|url=http://www.abc.net.au/news/2016-10-10/aviation-manufacturer-camit-closes/7918952|title=Aviation manufacturer closure a sign of the times, industry insider says|date=2016-10-10|newspaper=ABC News|language=en-AU|access-date=2016-11-23}}</ref>

== Results of R&D program ==
* Improved crankcase sealing, and the addition of piston cooling jets<ref name=":0" /><ref name=":1">{{Cite web|url=http://kitplanes2.com/blog/2015/07/new-camit-distributor/|title=New CAMit Distributor|language=en-US|access-date=2016-08-09}}</ref>
* The use of CAE-design 7/16" thru bolts<ref name=":2">{{Cite web|url=http://www.lightaircraftassociation.co.uk/engineering/TechnicalLeaflets/Mods%20and%20Repairs/TL%203.17%20List%20of%20approved%20Prototype%20mods.pdf|title=LAA Technical Leaflet TL 3.17: List of approved Prototype mods|last=|first=|date=2016-07-05|website=Light Aircraft Association|publisher=|access-date=2016-08-10}}</ref>
* 25+ modifications to the [[Cylinder head|heads]],<ref name=":0" /> including use of an improved alloy<ref name=":1" />
* Modified [[Cylinder (engine)|barrels]]: 10+ modifications, including thicker base flanges, improved honing process, and the reversal of piston orientation<ref name=":0" /><ref name=":1" />
* Improved flywheel coupling - lessens the chance of failure through various modifications<ref name=":1" />
* Belt-driven 40A and 15A alternators to help damp the flywheel and provide charging at idle<ref>{{Cite web|url=ftp://jabiruownersgroup.org/laa_mods/Alternator%20Mod/LAA-MOD%203%20-%20Mod%20Application%20-%20Alternator%20(V4)%20compressed.doc|title=Replace stator, flywheel rotor magnet and regulator with belt driven alternator|last=Licheri|first=D|date=2015-01-08|website=|publisher=|access-date=2016-08-08}}</ref>
* Redesigned [[rocker arm]]s<ref name=":2" />
* Modified oil cooler adaptor (TOCA also available)<ref name=":3">{{Cite web|url=http://kitplanes2.com/blog/2016/07/new-camit-127-hp-engine-at-airventure/|title=New CAMit 127-hp engine at AirVenture|language=en-US|access-date=2016-08-08}}</ref>
* Breather/oil separator<ref>{{Cite web|url=https://camitaeroengines.net/products/breather-oil-separator|title=Breather/Oil Separator|website=CAMit Aero Engines|access-date=2016-08-09 |archiveurl = https://web.archive.org/web/20160828055201/https://camitaeroengines.net/products/breather-oil-separator |archivedate = 28 August 2016}}</ref> (see photo; device connected to rear end of black hose)
* Redesigned, easy to read oil dipstick<ref name=":3" />
* [[Honda]] [[ignition coil]]s, lifters, and starter clutches are used<ref name=":2" />
* Ignition lead sets are assembled in-house<ref name=":0" />
* CAE-design collectors and twin-exhaust system<ref name=":3" />
A number of accessories are being sold to complement both CAE and Jabiru engines.
* CHT Sensors<ref>{{Cite web|url=https://camitaeroengines.net/products/engine-cht-sensor|title=Engine CHT Sensor|website=CAMit Aero Engines|access-date=2016-08-09 |archiveurl = https://web.archive.org/web/20160828021213/https://camitaeroengines.net/products/engine-cht-sensor |archivedate = 28 August 2016}}</ref>
* TOCA (Thermostatic Oil Cooler Adaptor)<ref name=":3" /> - blocks off oil passage to cooler until engine is at temperature
* Digital tachometer sensor<ref name=":3" />
* Engine inhibitor system - allows longer storage while preventing bore rust from occurring. Can be applied from the pilot's seat, just on shutdown.<ref>{{Cite web|url=https://camitaeroengines.net/products/engine-inhibitor-unit|title=Engine Inhibitor System|website=CAMit Aero Engines|access-date=2016-08-05 |archiveurl = https://web.archive.org/web/20160825230640/https://camitaeroengines.net/products/engine-inhibitor-unit |archivedate = 25 August 2007}}</ref><ref>{{Cite web|url=http://www.sonexfoundation.com/uploads/August_2015.pdf|title=Shop Talk, May 2015|last=|first=|date=|website=|publisher=Sonex Foundation|access-date=2016-08-08}}</ref>

== CAE not subject to CASA restrictions ==
In December 2014, the Australian [[Civil Aviation Safety Authority]] (CASA) imposed restrictions on all aircraft powered by Jabiru engines "in response to power-related problems involving engines manufactured by Jabiru Aircraft Pty Ltd (Jabiru)".<ref>{{Cite web|url=https://www.casa.gov.au/standard-page/operational-limits-jabiru-powered-aircraft|title=Operational limits on Jabiru-powered aircraft|last=|first=|date=|website=|publisher=|access-date=5 August 2016}}</ref> It was stated in ''Sport Pilot'' magazine shortly after this that "the CAMIT [sic] engine is not subject to the Jabiru operational limitations described in 292/14, because the engine is not manufactured by a person under licence from, or under a contract with, Jabiru."<ref name=":4">{{Cite book|title=Sport Pilot, March 2015|last=Marcel|first=Arthur|publisher=Recreational Aviation Australia|year=2015|isbn=|location=https://issuu.com/raaus/docs/sport_pilot_43_mar_2015/38|pages=39|via=}}</ref>

As of July 1, 2016, these restrictions were lifted for "most Jabiru-powered aircraft in Australia. Stock Jabiru engines that are maintained in strict accordance with Jabiru service bulletins and maintenance instructions are no longer affected by the limitations, which were issued in late 2014."<ref>{{Cite web|url=http://jabiruna.com/australia-lifts-restrictions-on-jabiru-engines/|title=Australia Lifts Restrictions on Jabiru Engines|website=jabiruna.com|access-date=2016-08-05}}</ref>

== Specifications ==
Data from the manufacturer<ref name=":0" />
{| class="wikitable" style="text-align:center;font-size:90%;"
|-
|- style="background-color:#efefef"
|
|'''3300SLRE'''
|-
|'''Configuration'''
|6-cylinder opposed
|-
|'''First run'''
|October 2013
|-
|'''Displacement'''
|3300cc (201cu. in)
|-
|'''Bore'''
|97.5mm
|-
|'''Stroke'''
|74mm
|-
|'''[[Compression ratio|Compression Ratio]]'''
|8.45:1
|-
|'''Dry Weight'''
|82.4&nbsp;kg (182&nbsp;lbs)
|-
|'''Max. Power'''
|127&nbsp;hp/95&nbsp;kW @3300rpm
|-
|'''Max. Torque'''
|285N.m @2900rpm
|-
|'''[[Power-to-weight ratio|Power to Weight]]'''
|1.15&nbsp;kW/kg
|-
|'''Firing Order'''
|1-4-5-2-3-6
|}
==See also==
*[[CAMit 2200]]

==References==
{{Reflist|30em}}

[[Category:Boxer engines]]
[[Category:Aircraft piston engines 2000–2009]]