{{Infobox building
| name                 = ADGB Trade Union School
| former_names         = 
| alternate_names      = Bundesschule des Allgemeinen Deutschen Gewerkschaftsbundes
| image                = Gewerkschaftsschule-Bernau 2007-08-19 AMA fec5.JPG
| alt                  =
| caption              = The Trade Union School after restoration 
| map_type             = 
| map_alt              = 
| map_caption          = 
| altitude             = 
| status               = Renovated
| building_type        = Education and Training Complex
| architectural_style  = [[Bauhaus]]
| structural_system    = Steel, Glass
| cost                 = ~EU 28 million (2007 renovation)
| client               = [[Allgemeiner Deutscher Gewerkschaftsbund|Allgemeiner Deutsche Gewerkschaftsbund]]
| location             = 
| address              = Fritz-Heckert-Straße
| location_town        = Bernau, Berlin
| location_country     = [[Germany]] 
| coordinates          = {{coord|52.7065|13.5440|type:edu_region:DE|display=inline,title}}
| start_date           = 1928
| completion_date      = 1930
| height               = 
| other_dimensions     = 
| floor_count          = 
| floor_area           = 
| main_contractor      = 
| architect            = [[Hannes Meyer]] 
}}

The '''ADGB Trade Union School''' (Bundesschule des Allgemeinen Deutschen Gewerkschaftsbundes (ADGB)), was a complex of teaching and administrative buildings in [[Bernau bei Berlin|Berlin bei Bernau]], [[Germany]], constructed for the former [[Allgemeiner Deutscher Gewerkschaftsbund|Federation of German Trade Unions]].  It was designed by the [[Bauhaus]] architect [[Hannes Meyer]] with his partner Hans Wittwer<ref>[https://www.bauhaus100.de/en/past/people/masters/hans-wittwer/ Hans Wittwer: Bauhaus100.] (Accessed: 10 October 2016)</ref> between 1928–1930, at which time Meyer was the director of the Bauhaus school in [[Dessau]].<ref name="ADGB Foundation">[http://www.bauhaus-denkmal-bernau.de/en/landmark/significance.html ''Significance. Bauhaus trade union school''] (Accessed: 23 October 2016).</ref>

The former ADGB School is a preeminent example of Bauhaus-designed architecture.<ref>[https://web.archive.org/web/20071017201907/http://denkmalschutz.de/1571.html Deutsche Stiftung Denkmal Schutz] (2007) (Accessed: 20 June 2013)</ref>

Next to the Bauhaus school buildings in Dessau,<ref>[http://www.bauhaus-dessau.de/the-bauhaus-building-by-walter-gropius.html ''The Bauhaus building by Walter Gropius'' (1925-26)] (Accessed: 21 October 2016).</ref> it was the second largest project ever undertaken by the Bauhaus.<ref name=HVK>[http://dlw.baunetz.de/sixcms/detail.php?id=456893 ''Internat der Handwerkskammer Berlin in Bernau''] (Photos with German text) (Accessed: 21 October 2016).</ref><ref name=architectuul>[http://architectuul.com/architecture/adgb-trade-union-school Architectuul: ''ADGB trade union school''] (2013) (Accessed: 27 October 2016).</ref>

== Purpose ==

The ADGB required a facility to educate and train members of the [[Trade union|union]] in a variety of areas including [[labour law]], [[industry]], [[management]] and [[economics]]. The scope of the project therefore meant different facilities were required such as [[classroom]]s, accommodation, [[catering]] and a sporting complex.<ref name="WMF">[https://www.wmf.org/sites/default/files/article/pdfs/Knoll%202008%20Modernism%20Brochure.pdf World Monuments Fund / Knoll Modernism Prize brochure (pdf)] (2008) (Accessed: 15 November 2016).</ref> 

== Architecture ==

=== Concept ===

The architecture reflects the teachings intrinsic to the Bauhaus ideologies and is a pragmatic example of [[Functionalism (architecture)|functional architecture]].<ref>[https://www.bauhaus100.de/en/past/works/architecture/bundesschule-des-adgb/index.html Bauhaus 100. Trade Union School of ADGB, Bernau.] (Accessed: 19 October 2016).</ref>  The functionality taking precedence over anything else, the school was stripped back of any unnecessary decoration. Meyer's design is composed of separate, individual structures that come together cohesively in the surrounding landscape. The design came directly from the functional diagrams that Meyer had developed where all the lounges are oriented towards the landscape and the nearby lake.

=== Design ===

The overall complex is difficult to comprehend and can only be properly understood from the air, which is a similar execution to [[Walter Gropius]]' Bauhaus in Dessau. Each separately functioning building is positioned to form a Z-shape.<ref name=architectuul />

The reception building bears a resemblance to the entrance of a factory site, which keeps in line with the purely functionalist design. There are three [[chimney]]s which constitute the heating system and are accompanied by a block-like cube of the auditorium, creating a dominating entrance scene to the complex. Immediately behind the entrance are the public buildings, positioned to create a square plan, which is exacerbated by the square auditorium in the middle. This form is intended to create an expression of unity, the unity of a community. The auditorium is a windowless room, the strong introversion allowing maximum concentration on the action. Sophisticated technology supported the lecturers: A push-button would reduce the light band and dim the lights, while all three wall elements at the front hung with maps and graphs were moveable.

Around the auditorium lined the administration building to the west, while south and east was the kitchen, dining room, sun room and recreation rooms together. The restrooms were opposite to the auditorium. All of these facilities were designed so that they allowed a digression of thought, and a relaxation of the mind.

The remaining facilities can be accessed by a covered glass transition corridor, accentuated with red-coloured [[steel]]. Five residential wings are lined to the south giving a clear view to the landscape in the north.<ref name="Archrecord">Sokol, David. [http://archrecord.construction.com/news/daily/archives/080813germany.asp "An Architectural Gem in Germany is Reborn], ''[[Architectural Record]]'', 13 August 2008, Retrieved on 21 June 2013.</ref> The course follows the terrain profile and thus has a slope of five meters. The recessed edges of the accommodation wings form niches that serve as communication nooks and lounges.  Meyer created not only a development, but also a public space that can be used in rainy weather as a movement zone. Each housing unit had a colour (e.g. red) assigned to each floor and was then differentiated further once on the floor (e.g., carmine, vermilion, pink).

== Interior furnishings ==

In keeping with the Bauhaus philosophy of teaching via practical experience and working with industry,<ref>Snider, C., [http://academic.chrissnider.com/bauhaus/pages/philosophy.html ''Bauhaus: Philosophy''] (Accessed: 21 October 2016).</ref> students were involved with this project, notably two women students.

{{Interlanguage link multi|Wera Meyer-Waldeck|de}} (1906 - 1964), who studied under [[Marcel Breuer]] in the carpentry workshop and later became an architect, designed most of the furniture for the project.<ref>[https://www.bauhaus100.de/de/damals/koepfe/studierende/wera-meyer-waldeck/index.html ''Wera Meyer-Waldeck'': Bauhaus100] (in German) (Accessed: 21 October 2016).</ref> This included simple but functional study desks.<ref>[https://www.bauhaus100.de/en/past/works/design-classics/modell-schreibtisch-bundesschule-des-adgb/index.html ''Model desk, trade union school of ADGB, Bernau'': Bauhaus100.] (Accessed: 21 October 2016).</ref>

[[Margaretha Reichardt]] (1907–1984), who studied in the weaving workshop under [[Gunta Stölzl]] and later set up her own weaving business, designed textiles which were used in the furnishings of the school.<ref>[https://www.bauhaus100.de/en/past/people/students/margaretha-reichardt/ ''Margaretha Reichardt'': Bauhaus100.] (Accessed: 21 October 2016).</ref>

== History ==

The school opened on 4 May 1930. It could accommodate 120 trainees.<ref name=history>[http://www.bauhaus-denkmal-bernau.de/en/landmark/history.html ''History. Bauhaus trade union school''] (Accessed: 23 October 2016).</ref>

On 2 May 1933 the building was confiscated by the [[Nazism|Nazis]]. Until the end of [[World War II]] the site was used by the Reich Leadership School, for training leaders of the [[Schutzstaffel|SS (Schutzstaffel)]], [[Sicherheitsdienst|SD (Sicherheitsdienst)]] and [[Gestapo]].<ref name=architectuul /><ref name="history" />

At the end of World War II, in spring 1945, the site was in the [[Russian sector in Berlin|Russian controlled sector of Berlin]]. The Russian Army used it as a temporary hospital and for military housing.<ref name="history" />

In spring 1946 the building was given to the [[Free German Trade Union Federation|Free German Trade Union Federation (FDGB)]], an East German organisation. During the Nazi period and the Russian occupation the school had been extensively damaged, so long term repair work began. In 1947 the school opened under the name ''FDGB-Bundesschule "Theodor Leipart"'' ([[Theodor Leipart]] FDGB Trade Union School). In January 1952 it was renamed again as the ''Gewerkschaftshochschule "Fritz Heckert"'' ([[Fritz Heckert]] Trade Union College).<ref name="history" />

Prior to [[German reunification]] in October 1990, the FDGB was disestablished (May 1990). The school was closed in September 1990. The property of the former FDGB was initially managed by an asset management company which temporarily leased the complex to various organisations. From August 1991 it was leased long term to the [[State of Brandenburg]] for use as a school of public administration, which opened in January 1992 following renovation work.<ref name="history" />

In 1996 the state government took over the complex and it remained vacant for sometime. In 2001 the Handwerkskammer Berlin (Berlin Chamber of Skilled Crafts) sub-leased the main historic building of the complex, the ''Meyer-Wittwer-Bau'' (Meyer Wittwer Building) to use as a training centre. The centre, known in German as the ''Internat des Bildungszentrums der Handwerkskammer Berlin'', has been in operation since 2007 when the major renovation project was completed.<ref name="HVK" /><ref name="history" />

== Restoration ==

The building was listed as protected monument by the East German government in 1977.<ref name="ADGB Foundation"/>

In January 2001 the new lease holders, the Berlin Chamber of Skilled Crafts (''Handwerkskammer Berlin''), made a  European-wide call for tenders for a redevelopment project, to restore Hannes Meyers original architecture and to enable to building to be useable as a modern teaching facility. There were 102 responses with the contract awarded to Brenne Gesellschaft von Architekten in July 2001.<ref name="history" />

The first phase of construction was started in February 2003. In early 2005, the work in the foyer and the dormitories was completed. The second phase was completed 2007. The school building has been almost completely restored to its original state, although the 1950s entrance has been retained.<ref name="HVK" /><ref name="history" />

In 2008 the architects, Brenne Gesellschaft von Architekten, won the [[World Monuments Fund]] / Knoll Modernism prize for the restoration.<ref name=WMF /> The prize is now given every two years, but this was the first time that it had been awarded.

In 2012 the ADGB Trade Union School was nominated to be included on the [[World Heritage List]], as part of the already listed Bauhaus sites in Weimar and Dessau.<ref name="ADGB Foundation" /> A decision is still pending.

The ''Stiftung Baudenkmal Bundeschule Bernau'' (Foundation for the Preservation of the Trade Union School Landmark Bernau), was established in September 2011 and granted legal status by the Brandenberg State Ministry of the Interior. Its main objective is "to continue researching the building complex ... as a cultural and historical landmark, and to raise public awareness".<ref>[http://www.bauhaus-denkmal-bernau.de/en/foundation/objectives.html ''Stiftung. Baudenkmal Bundesschule Bernau''] (in English)    (Accessed: 23 October 2016)</ref>

<gallery>
File:Bernau bei Berlin ADGB Schule Hörsäle.jpg|Back view of the lecture halls
File:Bernau bei Berlin ADGB Schule Wohntrakte vorne.jpg|Student Halls from the front – showing the way the buildings are positioned into the natural slope
File:Gewerkschaftsschule Bernau 2007-08-19 AMA fec 12 Ausschnitt.JPG|Original sketch showing how the separate buildings come together

</gallery>

== References ==

{{reflist}}

[[Category:Bauhaus]]
[[Category:Modernist architecture]]
[[Category:Buildings and structures in Berlin]]
[[Category:German architecture]]
[[Category:1920s architecture]]
[[Category:1930s architecture]]
[[Category:School buildings completed in 1930]]
[[Category:Modernist architecture in Germany]]