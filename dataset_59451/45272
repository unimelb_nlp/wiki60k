{{multiple issues|
{{BLP sources|date=January 2011}}
{{inadequate lead|date=July 2012}}
}}

'''Bill Bussey''' is a [[dentist]] in [[Longview, Texas]], an avid [[hot air balloon]] pilot and official world record holder in balloon flights.  He is the founder of [[Great Texas Balloon Race|The Great Texas Balloon Race]] and the first person to organize a "Balloon Glow" at a public event (1980 [[Great Texas Balloon Race]]) - balloon  glows are now a part of [[hot air balloon festival]]s and events around the world.   

Bussey states how his interest and entrance in hot air ballooning began, "I was skiing in [[Aspen, Colorado]] and saw two hot air balloons tethered on a rugby field and I just sat down in awe!" Bussey says that it was just a few months after that experience that he had his first opportunity to buy and interest in a fellow [[Texas|Texan's]] hot air balloon.    That investment quickly led to a second investment with three other partners when they bought anew balloon.   

[[Image:Bill Bussey in his hot-air balloon Skydancer.jpg|thumb|Skydancer - The hot air balloon frequently flown by Bussey]]Since that beginning, Bussey has logged over 3,000 hours flying hot air balloons. According to Bussey's website, as a competitive pilot he has won 35 championships; has finished in the top 3 in more than 100 events; is a perennial top 10 competitor at the US National Championships, and has represented the [[United States|US]] in World competition three times.  

In 1986, Bussey set his first world record. It became a drive and over the years since, he has set 29 [[United States|US]] and 14 World hot air balloon records.  A summation of some of the most important [http://www.balloonadventuresusa.com/recs.html record setting flights] can be found on Bussey's website.

Bussey's website lists some of his [http://www.balloonadventuresusa.com/meet.html accomplishments];

* 1978 - 1st balloon flight
* 1978 - 1st balloon championship, Florida Midwinter Festival
* 1986 - 1st World Record flight - AX5 world distance record of 179 miles
* 1988 - #1 ranking with the National Balloon Racing Association
* 1989 - #1 ranking with the Balloon Federation of American
* 1985, 1988, 1995 - member of the US World Balloon Team
* 1995 - Flight of [[SkyQuest 5]] setting world record for AX6-9
* 1999 - Founding member/director of the North American Balloon Association

AWARDS

* 1988 - Spirit of Fiesta Award - [[Albuquerque International Balloon Fiesta]]
* 1993 - National Aeronautic Association - 1 of Top 10 Most Memorable Flights
* 1994 - Shields-Trauger Award, Balloon Federation of America for most significant balloon flight
* 1994 - Al Desmond Award for Contribution to Competitive Ballooning
* Guest of King Hussein of Jordan for Champion of Champions Race
* Guest of Sultan of Malaysia for 1st balloon flights ever in Malaysia
* Federation Aeronautic Internationale Awards:
** Silver Badge
** Gold Badge with 2 Diamonds
* [[Montgolfier]] Diploma - Highest International Award in Ballooning
* 2007 - Inducted into the [http://www.lsfm.org/index.php?pgid=19 Texas Aviation Hall of Fame]

On August 6, 2016, Bussey and four others, including coaches [[Ray Germany]], [[Mickey Slaughter]], and [[Billy Montgomery]], will be inducted into the Ark-La-Tex Museum of Champions at the [[Shreveport, Louisiana|Shreveport]] Convention Center.<ref>{{cite web|url=http://sportsnola.com/tech-pair-germany-slaughter-inducted-ark-la-tex-sports-museum-champions/|title=Tech pair Germany, Slaughter to be inducted into Ark-La-Tex Sports Museum of Champions|publisher=sportsnola.com|date=June 29, 2016|author=Malcolm Butler|accessdate=July 9, 2016}}</ref>


Notable achievements

* Organized and flew in 1st Underground Balloon Glow and Competition, [[Marvel Cave]], [[Branson, MO]].
* Host of 6 one-hour TV Specials on Ballooning for the [[Travel Channel]] and [[Outdoor Life Network]]
* Driving Instructor for [http://www.racingexperience.com The Racing Experience] at 14 NASCAR Tracks around the nation.

== Biography ==

Dr. Bill Bussey worked several jobs such as an apartment manager, post office janitor, waiter, assistant private investigator, and others – before he graduated from the [[University of Texas]] with a B.S. degree and headed towards The [[University of Tennessee College of Dentistry]] in [[Memphis, Tennessee|Memphis]].  At that time, this dental school and [[Kentucky]] were the only two accelerated programs running year – round. His years there were rewarding and full of memories.  While he helped support himself by doing lab work, he had time to be president of the Student American Dental Association and join the [[Xi Psi Phi]] Dental Fraternity.  Upon graduation he received the [[International College of Dentists]] award for most outstanding student, as well as other honors.  Bussey was strongly impressed by the graduation speech given by [[Dr. Arthur Guyton]], famous author of ''Medical Physiology''.  He remembers that powerful speech and the incredible accomplishments of Dr. Guyton.  Incidentally, the dean told Bussey that he would never graduate because he did not have sufficient funds for his education, and Bussey says that statement gave him even more determination.  The same dean ultimately presented him with the most outstanding student award, and 12 years later, his school loans were paid in full.

Graduation was December 15, 1968 and 2 weeks later Bussey was a captain in the [[United States Army]] Dental Corps. He was stationed in [[Ft. Lewis]], [[Washington (U.S. state)|Washington]], with [[Mt. Rainier]] in full view of his dental chair.  Over the years he made several ascents of not only this mountain, but also many others over the next decade.  The dental clinic allowed Bussey to rotate through most of the specialties of dentistry, and he attributes that beginning as a spring-board towards later success and happiness in his private practice, especially the surgical and prosthetic aspects.

[[Image:BUSSY-ALDRICH-PHOTO.jpg|thumb|left|Bussey and his balloon in Colorado Rockies]]Bussey began fulfilling a lifelong dream of aviation.  He began his flying lessons on the very first day of his arrival at [[Ft. Lewis]].  He found that this was a most gorgeous place to fly, with all the mountains, the ocean, the islands with the yachts, as well as killer whales.  He learned the ability when to say “no, it is not safe to fly” and utilizes that to this day when flying.

[[Image:Dr Bill BUSSEY.jpg|thumb|right|Dr. Bill Bussey]]After leaving the [[United States Army Dental Corps|Dental Corps]] in 1971, Bussey opened his practice in [[Longview, Texas]] and has enjoyed what he describes as “a most charmed life”.  He practices general dentistry and has been placing implants since 1972.  Through the 1970s, he fulfilled his mountain climbing dreams, bought and flew airplanes, purchased a place on Lake Cherokee near Longview, and enjoyed the steady growth of his dental practice.  He attributes his later success to the Quest Study Club in Dallas, which he attended for many years.  Their motto of “being the best possible dentist achievable” rings loudly even today.

More than thirty years ago while skiing in [[Aspen, Colorado]], and taking hang-gliding lessons, he saw two tethered hot air balloons. The fascination with the fire, the beautiful fabrics, the wicker, the connection with aviation, and the colorful history of ballooning immediately hooked him.   This led to a very distinguished career in hot air balloon events all over the United States.  Bussey received the Spirit of Albuquerque Award, and has flown in [[Japan]], [[Mexico]], [[Canada]], and the [[Middle East]] as a guest of the Sultan of Malaysia.  For 18 of those 30 years, he maintained a national contract with [[Stroh Brewery Company]].  Bussey has broken 15 world records for distance and duration and 29 United States national records.  For these accomplishments he received the [[Montgolfier]] Diploma, the highest international award in ballooning, and the Shields-Trauger Award, the highest award in the United States.

In late 2007, he became the only not air balloonist enshrined in the [[Texas Aviation Hall of Fame]] in [[Galveston]].  He was among three new inductees to join the likes of [[Howard Hughes]], [[Wiley Post]], [[Alan Bean]], [[Gene Cernan]], [[George H. W. Bush]], [[Tom Landry]], and many [[World War II]] aviators.  His long list of accomplishments – competing on the world teams, being a consistent top 10 finisher in the national championships, organizing of the [[Great Texas Balloon Race]], creating the Balloon Glow now copied around the world, flying underground in [[Marvel Cave]], and being a Level VIII Master Instructor – dictated his consideration for enshrinement.  He now has a permanent display in the [[Lone Star Flight Museum]].  Although today Bussey does not compete as much, he still operates Balloon Adventures USA and offers champagne flights over [[East Texas]] to the general public. 

For the last 15 years, Bussey has been keenly involved with [[NASCAR]] and he and his oldest son, Kelly are partners in [http://www.racingexperience.com/ “The Racing Experience,”] which allows the novice driver to pilot a real [[NASCAR]] race car at speeds exceeding 165&nbsp;mph on tracks such as [[Talladega Superspeedway|Talladega]]; [[Auto Club Speedway|California]]; [[Kansas Speedway]]; [[Texas Motor Speedway]]; [[Atlanta Motor Speedway]]; and many others.  Bussey explains that “the excitement of driving a high-performance vehicle at those speeds on a famous track is beyond describable.  Plus, being able to participate at this level with your son is very special.” He has two other children, Casey Tobin, Ph.D., now living in [[Wisconsin]], and Zachary Bussey, who just graduated from [[Texas A&M University]]. 

Although Bussey has considered retirement, he continues with enthusiasm his dental practice, which began in 1969. He has missed only two and a half days from illness. He has treated extended families over the period of four decades. Most of his staff has been with him for more than twenty years. He says, "It just doesn’t get any better than this!”<ref>Source: Wayne Woods, D.D.S., Texas Dental Journal published by the Texas Dental Association - reprinted with permission.</ref>

==References==
{{Reflist}}<!--added above categories/infobox footers by script-assisted edit-->

{{DEFAULTSORT:Bussey, Bill}}
[[Category:American balloonists]]
[[Category:Living people]]
[[Category:Year of birth missing (living people)]]