{{Infobox journal
| title = Advances in Difference Equations
| cover = 
| abbreviation = Adv. Difference Equ.
| discipline = [[Recurrence relation|Difference equations]]
| editor = [[Ravi P. Agarwal]]
| publisher = [[Hindawi Publishing Corporation]]
| frequency = 
| history = 2004–present
| impact = 0.892
| impact-year = 2009
| url = http://www.hindawi.com/journals/ade/
| ISSN = 1687-1839
| eISSN = 1687-1847
| CODEN = 
| LCCN = 2006242013
| OCLC = 59228023
| link1 = http://www.hindawi.com/journals/ade/contents.html 
| link1-name = Online access
}}
''''' Advances in Difference Equations''''' is a [[peer review|peer-reviewed]] [[mathematics journal]] covering research on [[recurrence relation|difference equation]]s, published by [[Hindawi Publishing Corporation]].

The journal was established in 2004 and publishes articles on theory, methodology, and application of difference and differential equations. The [[editor-in-chief]] is [[Ravi P. Agarwal]] ([[Florida Institute of Technology]]).

== Abstracting and indexing ==
The journal is abstracted and indexed by the [[Science Citation Index|Science Citation Index Expanded]], [[Current Contents]]/Physical, Chemical & Earth Sciences, and ''[[Zentralblatt MATH]]''. According to the ''[[Journal Citation Reports]]'', the journal has a 2009 [[impact factor]] of 0.892.<ref name=WoS>{{cite web |url=http://isiwebofknowledge.com |title=Web of Science |year=2011 |accessdate=2011-06-02| archiveurl= https://web.archive.org/web/20110423125535/http://isiwebofknowledge.com/| archivedate= 23 April 2011 <!--DASHBot-->| deadurl= no}}</ref>

== References ==
{{Reflist}}

== External links ==
*{{Official website|https://web.archive.org/web/20110101174412/http://www.hindawi.com:80/journals/ade/}}

[[Category:Mathematics journals]]
[[Category:Publications established in 2004]]
[[Category:English-language journals]]
[[Category:Hindawi Publishing academic journals]]