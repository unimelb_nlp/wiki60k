{{Tone|date=November 2013}}
{{Use mdy dates|date=March 2012}}
{{Infobox person
| honorific_prefix = 
| name = 
| honorific_suffix = 
| image = 
| caption = 
| birth_name = 
| birth_date = August 22
| birth_place = 
| death_date = 
| death_place = 
| death_cause = 
| resting_place = 
| resting_place_coordinates = 
| monuments = 
| residence = {{Plainlist|
* Westerlo, New York, United States
* San Diego, California, US
}}
| nationality = 
| other_names = 
| education = 
| alma_mater = {{Plainlist|
* Queens University 
* University of California: Los Angeles
}}
| occupation = {{Plainlist|
* Filmmaker
* Professor
* Artist
}}
| organization = 
| agent = 
| known_for = 
| notable_works = 
| style = 
| home_town = 
| television = 
| religion = 
| spouse = 
| partner = 
| children = 
| parents = 
| relatives = 
| awards = 
| website = 
}}
'''Pierre Hermann L. Desir Jr.''' is an American filmmaker, artist, and professor, currently working as a Lecturer at the [[University of California, San Diego]]. After he attended Davies College in London and traveled through Europe, Africa, and the Middle East, Desir realized his passion for art and teaching. In art, he started with painting, moved to sculpting, and after a brief period of dabbling in journalism, he transitioned into film, receiving his MFA in Motion Picture Production at [[University of California, Los Angeles]]. He then moved back to sculpting while filming his process. In teaching, he started with arts and crafts as well as sculpting and moved into higher education after attending UCLA. One of his biggest challenges as a Haitian American filmmaker is the reception of his work due to the fact that it is highly experimental.

==Early life==
Pierre Desir’s parents were married in the 1940s. Desir was born to Pierre Hermann Desir Sr. who is [[Haiti]]an and Nicolina Melchionna, who is [[Italy|Italian]]. Desir was born in [[Jersey City]] hospital where his mother worked, though his family was living in Harlem at the time. Very soon after his family moved to Jamaica, Queens, Long Island where Pierre spent his childhood. He was considered illegal in some states in the Union because of his racial heritage. His father, worked at the Haitian Delegation of the UN there in Parkway Village. It was because he lived there that he was able to make friends from all over the world, which was very different than the rest of the country, which was segregated. When he was 10 years old, his parents wanted to move to the country, so they bought a farm in upstate NY.

==Education==
Desir attended [[Jamaica High School]] in Jamaica, NY.<ref>{{cite web|url=http://www.classmates.com/directory/public/memberprofile/list.htm?regId=442944391 |title=Pierre Desir &#124; Jamaica High School &#124; Jamaica, NY &#124; Classmates.com is now part of - Memory Lane |publisher=Classmates.com |date= |accessdate=March 19, 2012}}</ref> When 18, he left to go to school at Davies College in London, receiving a  C.Ed. European Literature and History, in 1987.. After travelling  through Europe, Africa, and the Middle East, he came back to the US, and worked various entry level positions and taught arts & crafts as well as ceramics to the mentally disturbed in Astoria, Queens,and taking  classes in painting, ceramics, and sculpting at the School of Visual Arts, New York. He then went to Queens College, where he received a BA in African Studies in 19979.  He then received his Masters in African History at [[UCLA]]Instead of studying for a PhD l he switched over to the MFA program and received his MFA in Film at UCLA in 1992.

==Artistic career==
Desir always has been artistic, and his mom and sister have always been supportive of him. His brother is supportive but critical, and his dad was disappointed in his choice to pursue art instead of Journalism, but still proud of him. In art, he started in painting, moved to sculpting, realized that he wanted his sculptures to move, moved onto film, and now he works mostly filming his process of making sculptures. Desir has struggled as a filmmaker financially, His film work is very experimental, even more so in his later work. Because of the strong experimental aspect his work is not well received, and it is often very hard for him to get it into festivals. It usually takes him years to complete his works.

===Influences===
He is a very pedantic person, but tries not to make his work pedantic. When he tried putting a political message into his work, he didn’t enjoy his work. He is inspired by Felonious Monk, who had a hard time being accepted as a jazz musician. He resents that as an African American, society expects him to deal with struggle in his work and it is liberating to him to refrain from engaging in the debate. He would like the public to appreciate his work, but it is not his goal to please the public. He was raised Catholic, but now he most closely relates to Buddhism, and closely puts weight in the idea of reincarnation. He also connects strongly with  [[West African Vodun|Vudun]] the West African origin of Voodoo and Native American mythology and tradition. His biggest influences in his work, in terms of style are [[Thelonious Monk]], [[Akira Kurosawa]], [[Orson Welles]], [[Pablo Picasso]], and [[Vincent van Gogh]]. He also appreciates and adapts the techniques of [[Keith Jarrett]].

===L.A. Rebellion===
He was on the outskirts of the [[L.A. Rebellion]], with most of the big players having left before he got to UCLA.<ref>{{cite web|url=http://www.cinema.ucla.edu/blogs/archival-spaces/2011/10/28/what%E2%80%99s-name-la-rebellion |title=What’s in a Name? L.A. Rebellion &#124; UCLA Film & Television Archive |publisher=Cinema.ucla.edu |date=October 28, 2011 |accessdate=March 19, 2012}}</ref> The LA Rebellion emerged at the time of a political awakening, when African Americans, Latinos, and others realized that the American Dream was not true.<ref>{{cite web|url=http://www.movingimagesource.us/articles/rebel-forces-20111026 |title=Rebel Forces by Vera Brunner-Sung - Moving Image Source |publisher=Movingimagesource.us |date= |accessdate=March 19, 2012}}</ref> There were exaggerated demands on the system, and the system pushing back. He was drawn into the movement during his time at UCLA because of his interest in film. He helped others with their work too, helping shoot and work on their films. In that way he was one of the unsung heroes of the rebellion, with his work challenging to digest, he has been able to gain recognition through the work of his colleagues.

==Current life==
He splits his time between San Diego where he currently a professor in the Communication department,<ref>{{cite web|url=http://communication.ucsd.edu/Courses.php |title=UCSD Communication - Courses |publisher=Communication.ucsd.edu |date= |accessdate=March 19, 2012}}</ref> and his family farm in New York that he inherited. It is on that farm that he does most of his work and where he has his workshop.

==Education career==
He was inspired to become a professor by teaching mentally disturbed individuals in Astoria, Queens.   In 2008, he and another black professor were denied tenure at Emerson College while all of the white candidates were granted tenure, this sparked an investigation by the NAACP because they believed that the denial was a product of racial discrimination.<ref>{{cite web|url=http://www.mindingthecampus.com/originals/2009/10/tenure_and_diversity.html |title=Tenure And Diversity |publisher=Mindingthecampus.com |date=October 19, 2009 |accessdate=March 19, 2012}}</ref><ref>{{cite web|url=http://diverseeducation.com/article/13542/ |title=Black Professor Denied Tenure at Emerson Vindicated by Report |publisher=Diverseeducation.com |date=February 16, 2010 |accessdate=March 19, 2012}}</ref>
* Visiting Lecturer. Department of Communication, University of California, San Diego. 2012 to present.<ref>{{cite web|url=http://communication.ucsd.edu/People.php |title=UCSD Communication - People |publisher=Communication.ucsd.edu |date= |accessdate=March 19, 2012}}</ref>
* Visiting Artist & Lecturer. Department of Communications, Dillard University. 2010.
* Assistant Professor. Department of Visual and Media Arts, Emerson College. 2002-09.<ref>{{cite web|url=http://www.sagindie.org/indieblog/my-experience-with-a-man-eating-croc-at-the-lake-placid-film-festival-just-kidding-about-the-croc-not-about-the-festival |title=My Experience with a Man-Eating Croc at the Lake Placid Film Festival (Just Kidding- about the croc, not about the festival) |publisher=Sagindie.org |accessdate=2013-11-18}}</ref>
* Assistant Professor. Department of Cinema and Photography, Park School of Communications, Ithaca College. 1992-2002.<ref>{{cite web|url=http://www.ratemyprofessors.com/ShowRatings.jsp?tid=177678 |title=Pierre Desir - Emerson College |publisher=RateMyProfessors.com |date= |accessdate=March 19, 2012}}</ref>
* Teaching Assistant. Department of Theater, Film and Television, UCLA. 1984-87.
* Teacher. Los Angeles County Juvenile Courts. 1989-90.
* Lecturer. City University, Queens College. 1981.
* Teaching Assistant. Martin de Porres School. 1971-76.

==Filmography==
{| class="wikitable"
|-
! Year !! Film Title !! Role !! Notes
|-
| In Progress || '''''Thelonious: Unknown Experiments in Flight Part 2''''' || Director || Poetry in Motion, on 16mm film.
|-
| In Progress || '''''Erzulie's Gift''''' || Director || Experimental Film. Poetry in Motion, on 16mm film.
|-
| 2007 || '''''Thelonious: Unknown Experiments in Flight Part 1''''' || Director || Poetry in Motion, on 16mm film.<ref>{{cite web|url=http://www.filmfestamiens.org/?THELONIOUS-UNKNOWN-EXPERIMENTS-IN,1450&lang=en |title=THELONIOUS: UNKNOWN EXPERIMENTS IN FLIGHT: Amiens International Film Festival |publisher=Filmfestamiens.org |date= |accessdate=March 19, 2012}}</ref>
|-
| 2006 || '''''Satchmo : The Arrival of Gabriel on a Wing and a Half-Note''''' || Director, co-writer, actor, co-producer || Poetry in Motion, on 16mm film. 20Mins.<ref>{{cite web|url=http://www.filmfestamiens.org/?SATCHMO-THE-ARRIVAL-OF-GABRIEL-ON%2C1448&lang=en |title=SATCHMO: THE ARRIVAL OF GABRIEL ON A WING AND A HALF-NOTE: Amiens International Film Festival |publisher=Filmfestamiens.org |date= |accessdate=2013-11-18}}</ref><ref>{{cite web|url=http://www.balaganfilms.com/big-balagan-local-filmmakers-series-new-films-emerson-college |accessdate=March 19, 2012 }}{{dead link|date=June 2016|bot=medic}}{{cbignore|bot=medic}}</ref>
|-
| 2000 || '''''ZoNA''''' || Director, co-writer, actor, co-producer, Editor, Sound design. || Narrative Feature, on 16mm film.<ref>{{cite web|url=http://articles.chicagotribune.com/2000-08-18/entertainment/0008180116_1_largest-festival-chicago-underground-film-festival-year-s-festival/2 |title=Underground Film Festival Continues To Prosper - Page 2 - Chicago Tribune |publisher=Articles.chicagotribune.com |date=August 18, 2000 |accessdate=March 19, 2012}}</ref>
|-
| 1992 || '''''The Gods & The Thief''''' || Writer, director, editor, cinematographer || Experimental/ Narrative, on 16mm film.<ref name="worldcat2001">{{cite web|url=http://www.worldcat.org/identities/lccn-no2002-31250 |title=The gods and the thief a film |publisher=Worldcat.org |date=October 15, 2001 |accessdate=March 19, 2012}}</ref>
|-
| 1990 || '''''Work''''' || Writer, director, cinematographer, editor. || Experimental, on 16mm film.
|-
| 1988 || '''''Kakilambe''''' || Director || A dance video of the Ballet Koumankale of Los Angeles. 
|-
| 1986-1987 || '''''Blockade Runners''''' || Director, camera operator, editor. || A documentary about film production in Cuba, including interviews with Cuban film directors. Taped at the 7th Festival of New Latin American Cinema, Havana, Cuba, December 1985.
|-
| 1985 || '''''Just a Job''''' || Director, co-writer, editor. || A drama about the assassination of a union official by a "has been" secret agent. 
|-
| 1984 || '''''Edge''''' || Cinematographer, editor. || An experimental film exploring the boundaries of light, shadow and film contrast. 
|-
| 1984 || '''''Impression of Cuba''''' || Director, camera editor. || A documentary about the 14th Contingent of the Venceremos Brigade in Cuba.
|-
| In Progress || '''''Detroit: UnRuined Voices''''' || Cinematographer, camera operator, editor. || Documentary Video by Kathryne Lindberg and Todd L. Duncan.
|-
| In Progress || '''''Bob Kaufman: When I Die, I Wont Stay Dead''''' || Cinematographer, camera operator. || Documentary Video by Billy Woodberry
|-
| 2005 || '''''The Brother''''' || Cinematographer, camera-operator. || Narrative Feature, on Digital video by Eva Benedikt (San Antonio International film Festival 2006 (Audience Award, Best Feature); International Film Festival Mannheim-Heidelberg, 2006) 
|-
| 2005 || '''''Trumpetistically''''' || Cinematographer, camera operator. || Documentary, on multiple formats (video, film, DVCam) by Clora Bryant. ( National Black Programming Consortium Project Grant, $60k, November 2002; "Sneak Preview", Prized Pieces Film Festival, 20th Anniversary celebration, National Black Programming Consortium, Schomburg Center, New York; Area Premiere, 13th Annual Pan African Film Festival, Magic Johnson Theaters, Los Angeles)<ref name="autogenerated1">{{cite web|url=http://www.cinema.ucla.edu/la-rebellion/pierre-d%C3%A9sir |title=Pierre Désir &#124; UCLA Film & Television Archive |publisher=Cinema.ucla.edu |date= |accessdate=March 19, 2012}}</ref>
|-
| In Progress || '''''Planet Without a Visa: the Embattled Life of Leon Trotsky''''' || Cinematographer, camera operator. || Documentary Feature Film by Lindy Laub<ref>{{cite web|url=http://rustbeltradical.wordpress.com/2008/07/ |title=2008 July « The Rustbelt Radical |publisher=Rustbeltradical.wordpress.com |date= |accessdate=March 19, 2012}}</ref>
|-
| 1999 || '''''Compensation''''' || Cinematographer, camera operator, storyboard artist, script consultant. || Narrative Feature by Zeinabu Davis (2002 Paul Robeson Prize, Best Film, Narrative Section; Newark Museum 28th Annual Black Film Festival; Gordon Parks Award, 1999 Independent Feature Market, NYC; “Outstanding Film” - Reel Black Award, Black Film & Video Network, Toronto; Independent Spirit Award Nomination, Best First Feature under $500k, IFP-West; Sundance Channel 2000-2003; BET Stars Network 2000-2003)<ref name="worldcat2001"/><ref name="autogenerated1"/>
|-
| 1988 || '''''I Look Through a Window''''' || Cinematographer, camera operator. || Documentary Feature Film by Stephen Edwards.
|-
| 1988 || '''''Cycles''''' || Cinematographer, Camera Operator, Script Consultant || Experimental Drama and Film Short by Zeinabu Davis. Many Awards.<ref name="worldcat2001"/><ref name="autogenerated1"/>
|-
| 1987 || '''''[[Sweet Bird of Youth]]''''' || Cinematographer, camera operator. || Film by Zeinabu Davis.
|-
| 1986 || '''''Minstrels''''' || Cinematographer, camera operator. || Documentary Film by Marie Kellier.
|-
| 1986 || '''''Out of Arabie''''' || Cinematographer, camera operator. || Narrative Film Short by Susan Evans.
|-
| 1985 || '''''Throwing Strikes''''' || Cinematographer, camera operator. || Narrative Film Short by Dino Castro.
|-
| 1985 || '''''Audio Visual''''' || Cinematographer, camera operator. || Narrative Film Short by David Strom.
|-
| 1992 || '''''Golden Chickpeas''''' || Production sound recordist. || Narrative Feature Film by Nigol Bezjian.
|-
| 1988 || '''''No Not One''''' || Production sound recordist. || Narrative Feature Film by George Gary.
|}

===Awards and exhibitions===

====Awards====
* Faculty Development Grant, 2006, Emerson College.
* Faculty Development Grant, 2002, Emerson College.
* Rockefeller Fellowship Nominee, 2000
* Finalist for the American Film Institute Filmmakers Fund, 1995
* James Pendleton Production Grant, 1997
* Kodak Product Grant, 1994
* Dana Internship, 1994
* Jerry Loveless Grant, 1994
* The Hortense Fishbaugh Fellowship, 1986
* The Foreign Press of Hollywood Award, 1986
* The Dorothy Arzner Award, 1985

====Exhibitions====
* Thelonious: Unknown Experiments in Flight, Part 1. Festival International du Film d’Amiens, Amiens France, 2007.
* Satchmo : The Arrival of Gabriel on a Wing and a Half-Note. Festival International du Film d’Amiens, Amiens France. 2007. Film Today, California Institute for the Arts, 2007. Balagan Films Boston, 2007. University Film and Video Association Conference, 2006. FPS, Emerson College, March 2004.
* ZONA. University Film and Video Association Conference, August 2001. Cornell Cinema, Cornell University, February 2000. California Institute of the Arts, “Film Today”, April 2000. Ithaca College, April 2000. Athens, Ohio International Film Festival, April 2000. Chicago Underground Film Festival, August 2000. Brooklyn Film Festival, September 2000.
* The Gods & The Thief. ONI Art Collective, Boston, MA, January 2003. Black Harvest International Film & Video Festival, Film Center of the School of the Arts Institute, Chicago, Illinois, July 1995. Rada Artist Collective, Port-au-Prince, Haiti, March 1994. 26th Humboldt International Film Festival, "Romano Robertisini Banana Slug Award for Surrealist Film," Arcata, California, March 1993. Whitney Museum of American Art; "Re-mapping Cultures," New York, New York, May 1992. London Film Festival, November 1992. Festival International du Film D'Amiens, France, November 1992.

==References==
{{Reflist}}

==External links==
*[http://www.kansas.com/2010/08/25/1461499/haitian-born-player-making-a-difference.html Kansas.com]
*[http://www.tuftsdaily.com/emerson-professors-allege-racial-discrimination-in-tenure-program-1.1574905#.UnzVWOJTVUp Tuftsdaily.com]
*[http://baystatebanner.com/news/2011/sep/19/emerson-college-busy-diversifying-its-staff/ Baystatebanner.com]
*[http://mije.org/richardprince/randall-pinkston-transferred-cbs-news-cutbacks Mije.org]

{{DEFAULTSORT:Desir, Pierre}}
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:American filmmakers]]
[[Category:American artists]]
[[Category:American academics]]
[[Category:American people of Haitian descent]]
[[Category:American people of Italian descent]]
[[Category:People from Westerlo, New York]]
[[Category:University of California, Los Angeles faculty]]
[[Category:University of California, San Diego alumni]]
[[Category:Queens College, City University of New York alumni]]
[[Category:20th-century American sculptors]]
[[Category:21st-century American sculptors]]
[[Category:American male sculptors]]
[[Category:20th-century American painters]]
[[Category:American male painters]]
[[Category:21st-century American painters]]