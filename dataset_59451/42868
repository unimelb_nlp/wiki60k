<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{{Use dmy dates|date=April 2017}}
{{Use British English|date=April 2017}}
{|{{Infobox Aircraft Begin
 | name=Goldfinch
 | image=Gloster Goldfinch c.png
 | caption=
}}{{Infobox Aircraft Type
 | type=Single-seat fighter
 | national origin=United Kingdom
 | manufacturer=[[Gloster Aircraft Company]]
 | designer=[[Henry Folland|H.P. Folland]]
 | first flight=Summer 1927
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=1
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Gloster Goldfinch''' was a single-engined single-seat high-altitude biplane fighter of all-metal construction from the later 1920s.  It did not reach production and only one was built.

==Development==
In January 1926 the [[Air Ministry]] funded [[Gloster Aircraft Company|Gloster Aircraft]] to produce an all-metal version of their [[Gloster Gamecock|Gamecock]] for a high altitude fighter role, hence requiring a supercharged engine.  The result was the Goldfinch, a single-bay biplane with unequal span wings of marked stagger. Unsurprisingly, this aircraft had a strong similarity to the Gamecock and in particular to the Gamecock II with its narrow-chord ailerons.  Because of the supercharged engine the fuselage was longer forward of the cockpit.  The tail also was slightly different, the tailplane having rounded trailing tips and the fin, initially, was  very broad in chord and short in height.<ref name="DNJ">{{harvnb|James|1971|pp=145–8}}</ref><ref name="Flight">''Flight'' 4 October 1928</ref>

Only one Goldfinch was built but there were two rather different versions.  The first build had all-metal wings but a fuselage of mixed metal and wood construction.  Both the wings and fuselage were fabric covered. The fuselage was then rebuilt entirely of metal, apart from its covering and lengthened.  The rebuild also brought a revised fin and rudder, with a narrower, higher fin of simpler overall shape.<ref>''Flight'' 4 October 1928: the g/a drawings show the early fin and the photographs the later arrangement</ref>  Throughout, the Goldfinch was powered by a supercharged 450&nbsp;hp (335&nbsp;kW) [[Bristol Jupiter|Bristol Jupiter VIIF]] driving a 9&nbsp;ft (2.74 m) two-bladed fixed-pitch propeller.<ref name="DNJ"/><ref name="Flight"/>

The Goldfinch first flew in May 1927,<ref name="Jarrett p48-9">Jarrett 1997, pp. 48–49.</ref> and in December it went to [[RAF Martlesham Heath]] for trials where it proved fast and high with a rapid climb rate,<ref name="DNJ"/> and generally good manoeverability, although spinning behaviour was substandard.<ref name="Jarrett p50">Jarrett 1997, p. 50.</ref> Gloster's submitted it as a contender for [[List of Air Ministry Specifications#1920-1929|Air Ministry specification]] F.9/26 for an all-metal day-and-night fighter, but it failed to meet the required load and fuel requirements and was eliminated quite early.  The [[Bristol Bulldog]] was the eventual winner,<ref name="DNJ"/><ref name="Flight"/> with the Goldfinch being used for trials at Martlesham Heath until October 1928.<ref name="Jarrett p51">Jarrett 1997, p. 51.</ref>

<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications==
[[File:Gloster Goldfinch 3v.png|thumb]]
{{aerospecs
|ref={{harvnb|James|1971|p=148}}
|met or eng?=eng<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->
|crew=one
|capacity=
|length m=6.7
|length ft=22
|length in=3
|span m=9.14
|span ft=30
|span in=0
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=3.19
|height ft=10
|height in=6
|wing area sqm=25.56
|wing area sqft=274.3
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=933
|empty weight lb=2,058
|gross weight kg=1,467
|gross weight lb=3,236
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|eng1 number=1
|eng1 type=[[Bristol Jupiter|Bristol Jupiter VIIF]] 9-cylinder supercharged radial
|eng1 kw=335<!-- prop engines -->
|eng1 hp=450<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|max speed kmh=276
|max speed mph=at 10,000 ft (3,048 m) 172
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=8,217
|ceiling ft=26,960
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=6.35
|climb rate ftmin=to 20,000 ft (6,096 m) 1,250
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|armament1=2×synchronised 0.303 in (7.7 mm) [[Vickers machine guns]] firing forward
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{commons category|Gloster Goldfinch}}
{{refbegin}}
*{{cite book |last=James |first=Derek N. |year=1971 |title=Gloster Aircraft since 1917 |publisher=Putnam Publishing |location=London |isbn=0-370-00084-6 |ref=harv}}
*{{cite magazine |last=Jarrett |first=Philip |title=Limited Editions:Gloster Goldfinch |magazine=Aeroplane Monthly |publisher=IPC |location=London |issue=January 1997 |pages=46–51}}
*{{cite magazine |title=THE GLOSTER "GOLDFINCH". |magazine=[[Flight International|Flight]] |issue=4 October 1928 |pages=845–849 |url=http://www.flightglobal.com/pdfarchive/view/1928/1928%20-%200915.html |accessdate= }}
{{refend}}

{{Gloster aircraft}}

[[Category:British fighter aircraft 1920–1929]]
[[Category:Gloster aircraft|Goldfinch]]