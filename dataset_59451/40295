{{Use dmy dates|date=January 2013}}
{{Use British English|date=January 2013}}
The '''Creative Circle''' (also known as '''The Advertising Creative Circle''' and '''The Advertising Creative Circle of Great Britain''') is an educational awards body dedicated to creativity in [[United Kingdom|British]] [[advertising]], and the oldest advertising and [[marketing]] awards body in [[Europe]].<ref name="Advertiser's Weekly 18/10/45"> Advertiser's Weekly, "Advertising Creative Circle Will Raise Status of Profession", 18 October 1945</ref> Their mission is to help promote creative excellence in advertising, while sharing knowledge and experience through educational programs and supporting young creative people.<ref name="Creative Circle">[http://creativecircle.co.uk/#about] About Us</ref> The annual Creative Circle awards are highly regarded, and judged by the British advertising creative community themselves. Their main offices are in [[Covent Garden]], London.

==Origins==

After many months of preliminary discussions,<ref name="World's Press News 18/10/45"> World's Press News, "Advertising Creative Circle Formed to Foster Pride in the Craft", 18 October 1945</ref> the Advertising Creative Circle of Great Britain was founded on 8 October 1945,<ref name="Advertiser's Weekly 18/10/45" /> at a luncheon at The Trocadero Hotel, London.<ref name="Advertiser's Weekly 18/10/45" /> The concept can be traced back to two forward-thinking ad men, [[H.F. Crowther]] (Director of [[Advertising Agency]] [[Rumble, Crowther & Nicholas]]) and [[G.R. Pope]] (Advertising Manager of [[The Times]]).<ref name="Advertiser's Weekly 18/10/45" />

At the time, Crowther and Pope felt there was no other advertising organisation concerned exclusively with the craft of visualising, writing and designing advertising,<ref name="The Newspaper World 20/10/45"> The Newspaper World, "ADVERTISING REVIEW: Formation of an Advertising Creative Circle", 20 October 1945</ref> and so decided to create an Advertising Creative Circle that could "not only provide a forum for such creative men, but [also] contribute to raising the status of advertising as a profession".<ref name="The Newspaper World 20/10/45" />

=== The First Council ===
{| class="wikitable"
! Title !! Name !! Position; Company
|-
| President || Mr. G. O. Nickalls<ref name="Advertiser's Weekly 18/10/45" /> || Director; [[Alfred Pemberton]], Ltd
|-
| Vice-President || Mr H. F. Crowther<ref name="Advertiser's Weekly 18/10/45" /> || Director; [[Rumble, Crowther & Nicholas]] Ltd
|-
| Hon. Secretary || Mr Ernest Briggs<ref name="Advertiser's Weekly 18/10/45" /> || Director; [[London Press Exchange]] Ltd
|-
| Hon. Treasurer || Mr S. J. G. Chipperfield<ref name="Advertiser's Weekly 18/10/45" /> || [[Masius and Fergusson]] Ltd
|-
| Council Member || Mr G. Butler<ref name="Advertiser's Weekly 18/10/45" /> || Art Director; [[J. Walter Thompson]] Co. Ltd
|-
| Council Member || Mr G. H. Saxon Mills<ref name="Advertiser's Weekly 18/10/45" /> || Director; [[W. S. Crawford]] Ltd
|-
| Council Member || Mr G. J. Redgrove<ref name="Advertiser's Weekly 18/10/45" /> || Director; [[C. Vernon and Sons]] Ltd
|-
| Council Member || Mr G. Worledge<ref name="Advertiser's Weekly 18/10/45" /> || [[S. H. Benson]] Ltd
|-
|}

This first council was responsible for determining subscriptions, membership control, copy vigilance, press relations and a programme of art exhibitions, publications and functions.<ref name="Advertiser's Weekly 18/10/45" /> In addition, they were tasked with selecting 10 more honorary members and a further 50 ordinary members, to be made up of noteworthy people wholly engaged in creative work - [[copywriters]], [[copy editor|copy chiefs]], [[Storyboard artist|artists]], [[art directors]], [[graphic designer|layout men]], [[Visualizer (advertising)|visualisers]] and more.<ref name="Advertiser's Weekly 18/10/45" />

Within its press relations functions, the Creative Circle hoped not only to keep the press informed about advertising matters generally, but also "''take up the cudgels''" whenever it was publicly attacked.<ref name="Advertiser's Weekly 18/10/45" />

==Honours (awards)==

The Creative Circle has awarded the very best of British advertising creativity since 1986.<ref name="1st Annual">The Annual of the Advertising Creative Circle, The Advertising Creative Circle, 1986</ref> There are several levels of awards presented on the '''Honours Evening'''. From commendations, through to Bronze, Silver, and Gold, and ultimately, the Gold of Gold award for the single best piece of work that year. The President also presents a personal award to the person or organisation that has had the greatest impact on advertising that year.

=== List of President's Award winners ===

{| class="wikitable"
! Year !! Winner
|-
| 1986 || ---
|-
| 1987 || [[West Herts College|Watford College]] (College)<ref name="2nd Annual">The Second Annual of the Advertising Creative Circle, The Advertising Creative Circle, 1987</ref>
|-
| 1988 || [[Central Office of Information]] (Client)<ref name="3rd Annual">The Third Annual of the Advertising Creative Circle, The Advertising Creative Circle, 1988</ref>
|-
| 1989 || Tony Cox (Creative)<ref name="4th Annual">The Fourth Annual of the Advertising Creative Circle, The Advertising Creative Circle, 1989</ref>
|-
| 1990 || [[Tony Kaye (director)|Tony Kaye]] (Director)<ref name="5th Annual">The Fifth Annual of the Advertising Creative Circle, Trigon Press, 1990. ISBN 0-904929-26-4</ref>
|-
| 1991 || Tim Delaney (Creative)<ref name="6th Annual">The Sixth Awards Annual of the Advertising Creative Circle, Trigon Press, 1991. ISBN 0-904929-29-9</ref>
|-
| 1992 || Roger Woodburn (Director)<ref name="7th Annual">The 7th Creative Circle Annual, Trigon Press, 1992. ISBN 0-904929-36-1</ref>
|-
| 1993 || Barbara Nokes (creative)<ref name="8th Annual">Volume 8 of the Creative Circle Honours, Trigon Press, 1993. ISBN 0-904929-38-8</ref>
|-
| 1994 || Chris Palmer and Mark Denton (Creatives)<ref name="9th Annual">Volume 9 of the Creative Circle Honours, Trigon Press, 1994. ISBN 0-904929-44-2</ref>
|-
| 1995 || ---
|-
| 1996 || Tom Carty and Walter Campbell (Creatives)<ref name="Campaign 08-03-96">Campaign Magazine, "Twister dominates awards", 8 March 1996</ref>
|-
| 1997 || Paul Weinburger (Creative)<ref name="13th Annual">Volume 13 of the Creative Circle Honours, Trigon Press, 1992. ISBN 0-904929-53-1</ref>
|-
| 1998 || Richard Flintham and Andy Mcleod (Creatives)<ref name="14th Annual">The 1998 Creative Circle Honours, Trigon Press, 1992. ISBN 0-904929-54-X</ref>
|-
| 1999 || The men and women of the [[Creative services|Creative Services Departments]]<ref name="15th Annual">The Creative Circle Honours 1999, The Advertising Creative Circle, 2000</ref>
|-
| 2000 || Paddy Easton and [[Framestore|The Computer Film Company]] (Production Company)<ref name="16th Annual">The Creative Circle Honours 2000, The Advertising Creative Circle, 2001</ref>
|-
| 2001 || Dave Waters (Creative)<ref name="17th Annual">The Creative Circle Honours 2001, The Advertising Creative Circle, 2002</ref>
|-
| 2002 || Roger Kennedy (Typographer)<ref name="18th Annual">The Creative Circle Honours Winners 2002, The Advertising Creative Circle, 2003</ref>
|-
| 2003 || Paul Silburn (Creative)<ref name="19th Annual">The Creative Circle Honours 2003, The Advertising Creative Circle, 2004</ref>
|-
| 2004 || Steve Henry (Creative)[http://creativecircle.co.uk/archive/2004/president.htm]
|-
| 2005 || [[Daniel Kleinman]] (Director)<ref name="21st Annual">The Creative Circle Honours 2005, The Advertising Creative Circle, 2006</ref>
|-
| 2006 || ---
|-
| 2007 || Ed Morris (Creative)<ref name="23rd Annual">Creative Circle Annual 2007, The Creative Circle Ltd, 2007. ISBN 978-0-9557983-0-6</ref>
|-
| 2008 || Juan Cabral (Creative)<ref name="24th Annual">The Bumper Book Of British Advertising - Creative Circle Annual 2008, The Creative Circle Ltd, 2008. ISBN 978-0-9557983-1-3</ref>
|-
| 2009 || No Award Given<ref name="25th Annual">Adland - Creative Circle Annual 2009, The Creative Circle Ltd, 2010. ISBN 978-0-9557983-2-0</ref>
|-
| 2010 || Malcolm Gaskin (Creative)<ref name="26th Annual">Nautical But Nice - Creative Circle Annual 2010, The Creative Circle Ltd, 2011</ref>
|-
| 2011 || [[Graham Fink]] (Creative)[http://www.canneslions.com/festival/speakers.cfm?speaker_id=508]
|-
| 2012 || Nick Gill (Creative)[http://bbh-labs.com/nick-gill-10-things-ive-learned-that-might-help]<ref name="Circle Mag 2012"> Circle Magazine, 2 July 2012</ref>
|-
| 2013 || Matt Gooden & Ben Walker (Creatives)[http://www.thedrum.com/news/2013/03/15/creative-circle-award-winners-2013-sunday-times-rich-list-and-channel-4s-superhumans]<ref name="Circle Mag 2013"> Circle Magazine, 23 July 2013</ref>
|}

=== List of Gold of Gold Award winners ===

Established in 1989 as 'The Big One',<ref name="4th Annual" /> and known from 1996-2011 as 'the Platinum Award', the currently named '''Gold of Golds''' is given to the single best advertising creative idea of the year (the only exception being 2008, when it was felt advertising agency Fallon deserved the award, having produced both the [[Cadbury gorilla|Cadbury Gorilla commercial]] and the [[Cake (advertisement)|Skoda Fabia Cake commercial]] in the same year [http://www.brandrepublic.com/news/788217/Fallon-sweeps-board-Creative-Circle-Awards/]).

{| class="wikitable"
! Year !! Winning Work !! Client !! Agency
|-
| 1989 (The Big One) || Relax<ref name="4th Annual" /> || [[British Rail]] || [[Saatchi & Saatchi]]
|-
| 1990 (The Big One) || Into the Valley/Israelites<ref name="5th Annual" /> || [[Maxell]] || Hutchins Film Company / [[HHCL|Howell Henry Chaldecott Lury]]
|-
| 1991 (The Big One) || Club/Bar<ref name="6th Annual" /> || [[Red Rock Cider]] || GGT
|-
| 1992 (The Big One) || Shoes In Action<ref name="7th Annual" /> || [[Reebok]] || Lowe Howard Spink
|-
| 1993 (The Big One) || Reg on...<ref name="8th Annual" /> || Regal, Imperial Tobacco || Lowe Howard Spink
|-
| 1994 (The Big One) || Unexpected<ref name="9th Annual" /> || Dunlop || [[AMV BBDO]] / [[Tony Kaye (director)|Tony Kaye Films]]
|-
| 1995 || --- || --- || ---
|-
| 1996 (Platinum Award) || Twister<ref name="Campaign 08-03-96" /> || [[Volvo]] || [[AMV BBDO]]
|-
| 1997 (Platinum Award) || [[Blackcurrant tango St George|St George]][http://www.campaignlive.co.uk/news/20055/Tango-ad-tops-Creative-Circle/] || [[Tango (drink)|Blackcurrant Tango]] || [[HHCL|HHCL & Partners]]
|-
| 1998 (Platinum Award) || Hiccups/Dentist/Chair/Guard/Tennis/Lamppost<ref name="13th Annual" /> || [[Volkswagen|Volkswagen UK]] || BMP DDB
|-
| 1999 || --- || --- || ---
|-
| 2000 || --- || --- || ---
|-
| 2001 (Platinum Award) || Bear<ref name="16th Annual" /> || [[John West Salmon]] || [[Leo Burnett]]
|-
| 2002 (Platinum Award) || Sofa<ref name="17th Annual" /> || [[Reebok]] || [[Lowe Worldwide|Lowe]]
|-
| 2003 (Platinum Award) || Ball Skills/Mum/Diving/Babies/Monsters [http://www.brandrepublic.com/news/173120/John-Smiths-Peter-Kay-TV-spots-win-top-awards-Creative-Circle/] || [[John Smiths]] || [[TBWA|TBWA\London]]
|-
| 2004 (Platinum Award) || [[Cog (advertisement)|Cog]][http://www.brandrepublic.com/news/203933/Mesmerising-cog-spot-wins-creative-platinum-W-K/] || [[Honda]] || Partizan / [[Wieden + Kennedy]]
|-
| 2005 (Platinum Award) || [[Grrr (advertisement)|Grrr]][http://creativecircle.co.uk/archive/2005/platinum.htm] || [[Honda]] || [[Wieden + Kennedy]]
|-
| 2006 (Platinum Award) || [[BRAVIA#Launch.2C_.27Balls.27|Balls]][http://www.brandrepublic.com/news/545946/Sony-balls-nets-top-prize-Creative-Circle-Awards] || [[Sony]] || [[Fallon Worldwide|Fallon]]
|-
| 2007 (Platinum Award) || No Award Given [http://www.brandrepublic.com/news/642396/Creative-Circle-2007-winners/]|| N/A || N/A
|-
| 2008 (Platinum Award) || [[Fallon Worldwide|Fallon]]<ref name="23rd Annual" /> || [[Fallon Worldwide|Fallon]] || [[Fallon Worldwide|Fallon]]
|-
| 2009 (Platinum Award) || Wallace & Gromit<ref name="24th Annual" />  || [[Harvey Nichols]] || [[DDB Worldwide|DDB]]
|-
| 2010 (Platinum Award) || Knife Crime<ref name="25th Annual" /> || [[Metropolitan Police]] || [[AMV BBDO]]
|-
| 2011 (Platinum Award) || Straight/Catch [http://creativecircle.co.uk/archive/2011/platinum.htm] || [[Magners]] || [[Red Brick Road]]
|-
| 2012 (Gold of Golds) || The Long Wait [http://www.creativecircle.co.uk/2012.html#1300]<ref name="Circle Mag 2012" /> || [[John Lewis Partnership]] || Adam & Eve
|-
| 2013 (Gold of Golds) || Don't Cover It Up [http://www.creativecircle.co.uk/2012.html#1300]<ref name="Circle Mag 2013" /> || [[Refuge_(United_Kingdom_charity)|Refuge]] || [[Bartle_Bogle_Hegarty|BBH London]]
|}

==Role Reversal Seminar==

The Creative Circle Role Reversal Seminar was created in 1968<ref name="Marketing 15-10-98">Marketing Magazine, "Trading places: clients and creatives don’t always see eye to eye", 15 October 1998</ref> by Sam Rothenstein - a copywriter who believed that creative standards don’t just depend on advertising agencies but on clients too.<ref name="Marketing 15-10-98" />

The concept of the course is simple: one of the best ways to understand someone is to put yourself in their shoes.<ref name="CCRR Brochure">A blob of ink or a fish juggling cantaloupes?, Creative Circle, 11 September 2008</ref> So, a group of middleweight [[marketers]] take on the role of an [[advertising agency]] [[Advertising_agency#Creative_department|creative department]]. While agency [[Creative Director]]s take on the role of the clients.<ref name="Financial Times 17-09-81">The Financial Times, "Brain Storming in Oxford", 17 September 1981</ref>

The marketers are grouped into "Agency" teams and made to [[Sales pitch|pitch]] against each other for a piece of business (as happens in the real world).<ref name="Marketing 15-10-98" /> The "Agencies" are given a [[creative brief]] from a fictional client simultaneously. They then have just 72 hours to conceive and produce a pitch-winning idea. This includes deciding upon a strategy and slogan, then creating and designing ad executions, across all media - they're even expected to film, edit, and present a television commercial in that time.<ref name="Brand Strategy 01-10-00">Brand Strategy (Newspaper), "Trading places teaches clients a lesson", 1 October 2000</ref>

Each "Agency" is assisted by a professional Creative Director, to keep them on track, and an Art Director (known as a "Tutor" or a "[[Visualizer (advertising)|Wrist]]") to help them turn their ideas into visuals and [[storyboards]]. A film crew and production team are also on hand to help with the filming and editing of the commercial.<ref name="Marketing 15-10-98" />

After the 72 hours are up, the "Agency" teams pitch their ideas to the fictional clients - a consortium of real [[Creative Director]]s - who then select a winner.<ref name="Brand Strategy 01-10-00" />

The course is designed to give marketers the opportunity to see things from the point of view of their agency - learning through doing - which often gives them a completely new perspective on that relationship.<ref name="Marketing 15-10-98" /> It's also renowned throughout the industry for incredibly late nights and much drunken bonding, giving clients and agencies an insight into each other's worlds.<ref name="Marketing Week 12-09-80">Marketing Week, "How a Circle got its roles in a twist", 12 September 1980</ref> It's even been known to influence real-world client/agency relationships, with one story speculating that many years ago, [[Guinness]] switched its multi-million pound advertising account to [[J. Walter Thompson]] as a result of one such seminar.<ref name="Financial Times 17-09-81" />

The Role Reversal Seminar ran unbroken for precisely 40 years, always taking place in one of the UK's top universities - including [[Cambridge University|Cambridge]] and [[St Andrews University|St Andrew's]]<ref name="Creative Review 1980">Creative Review, "The Client is Always Right", Autumn 1980</ref> - before settling for the last 20 years at [[Trinity College, Oxford]]. Over those 40 years, the course attracted thousands of marketers, but was closed in 2008 due to financial constraints.

== References ==
{{Reflist|2}}

== External links ==
* [http://www.creativecircle.co.uk Official Site]
* [http://creativecircle.co.uk/archive/ Winners Archive 2003-2011]

<!--- Categories --->



[[Category:Advertising awards]]