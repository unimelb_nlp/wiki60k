{{Infobox technology standard
| title             = Journal Article Tag Suite
| long_name         = 
| native_name       = <!-- Name in local language. If more than one, separate using {{plain list}} -->
| native_name_lang  = <!-- ISO 639-2 code e.g. "fr" for French. If more than one, use {{lang}} inside native_name items instead -->
| image             = File:Jats-logo.jpg
| caption           = 
| status            = Published
| year_started      = <!-- {{Start date|YYYY|MM|DD|df=y}} -->
| first_published   = {{Start date|2003|03|31|df=y}}
| version           = NISO JATS 1.1
| version_date      = {{Start date|2016|01|06|df=y}}
| preview           = 
| preview_date      = 
| organization      = {{plainlist |
* [[National Information Standards Organization]]
* [[American National Standards Institute]]
}}
| committee         = 
| editors           = 
| authors           = {{plainlist |
* [[National Center for Biotechnology Information]]
* [[National Information Standards Organization]]
}}
| base_standards    = [[XML]]
| related_standards = {{plainlist|
* ISO Standards Tag Set (ISOSTS)
* NISO Book Interchange Tag Suite (BITS)
* SciELO Publishing Schema (SPS)
}}
| abbreviation      = JATS
| domain            = {{plainlist |
* [[Academic publishing]]
* [[Semantic publishing]]
}}
| license           = 
| website           = {{URL|jats.niso.org}}
}}

The '''Journal Article Tag Suite''' ('''JATS''') is an [[XML]] format used to [[Metadata|describe]]  [[scientific literature]] published online. It is a [[technical standard]] developed by the [[National Information Standards Organization]] (NISO) and approved by the [[American National Standards Institute]] with the code '''Z39.96-2012'''.

The NISO project was a continuation of the work done by [[National Center for Biotechnology Information|NLM/NCBI]], and popularized by the NLM's [[PubMed Central]] as an [[de facto standard|''de facto'' standard]] for archiving and interchange of [[open-access journal|scientific open-access journals]] and its contents with [[XML]].

With the NISO standardization the NLM initiative has gained a wider reach, and several other repositories, such as [[SciELO]], adopted the XML formatting for [[Scientific literature|scientific articles]].

The JATS provides a set of XML elements and attributes for describing the textual and graphical content of journal articles
as well as some non-article material such as letters, editorials, and book and product reviews.<ref>ANSI/NISO Z39.96-2012 ISSN 1041-5653. See [http://www.niso.org/standards/z39-96-2012 z39.96-2012.pdf] at www.niso.org/standards/z39-96-2012</ref>
JATS allows for descriptions of the full article content or just the article header metadata; 
and allows other kinds of contents, including research and non-research articles, letters, editorials, and book and product reviews.

== History ==
Since its introduction, NCBI's NLM Archiving and Interchange [[Document type definition|DTD]] suite has become the [[De facto standard|''de facto'' standard]] for journal article markup in [[Academic publishing|scholarly publishing]].<ref>{{cite journal | pmc = 3227009 | pmid=22140303 | doi=10.3998/3336451.0014.106 | volume=14 | title=NISO Z39.96The Journal Article Tag Suite (JATS): What Happened to the NLM DTDs? | year=2011 | journal=J Electron Publ | last1 = Beck | first1 = J}}</ref> With the introduction of NISO JATS, it has been elevated to a [[De jure|true standard]].<ref>{{cite web |last=Zimmerman |first=Sara |date=2012 |title= The new NISO journal Article Tag Suite standard |website=Zeeba.tv |url=http://river-valley.zeeba.tv/the-new-niso-journal-article-tag-suite-standard/ }}</ref>
Even without public data interchange, the advantages of NISO JATS adoption affords publishers in terms of streamlining production workflows and optimizing system interoperability.<ref>{{cite conference |first1=Paul |last1=Donohoe |first2=Jenny |last2=Sherman |first3=Ashwin |last3=Mistry |title=The Long Road to JATS |date=2015 |conference=JATS-Con 2015 |book-title=Journal Article Tag Suite Conference (JATS-Con) Proceedings 2015 |publisher=National Center for Biotechnology Information |location=Bethesda, MD |url=http://www.ncbi.nlm.nih.gov/books/NBK279831/ |conference-url=http://jats.nlm.nih.gov/jats-con/2015/schedule2015.html }}</ref><ref>{{cite conference |first1=Tommie |last1=Usdin |first2=Deborah Aleyne |last2=Lapeyre |first3=Carter M. |last3=Glass |title=Superimposing Business Rules on JATS |date=2015 |conference=JATS-Con 2015 |book-title=Journal Article Tag Suite Conference (JATS-Con) Proceedings 2015 |publisher=National Center for Biotechnology Information |location=Bethesda, MD |url=http://www.ncbi.nlm.nih.gov/books/NBK279902/ |conference-url=http://jats.nlm.nih.gov/jats-con/2015/schedule2015.html }}</ref>

=== Timeline ===
; NLM JATS
: NLM JATS, version 1
:* {{Timeline-event |date={{Start date|2003|03|31}} |event=NLM DTD v1.0 introduced. }}<ref name=TagSuiteHome>{{cite web |date=13 September 2012 |title=NLM Journal Archiving and Interchange Tag Suite |publisher=National Center for Biotechnology Information |url=http://dtd.nlm.nih.gov/index.html |dead-url=no |archive-url=http://web.archive.org/web/20160827204618/http://dtd.nlm.nih.gov/index.html |archive-date=27 August 2016 }}</ref>
:* {{Timeline-event |date={{Start date|2003|11|05}} |event=Version 1.1  update released. }}<ref name=TagSuiteHome/>
: NLM JATS, version 2
:* {{Timeline-event |date={{Start date|2004|12|30}} |event=Version 2.0 major update released. It is designed to support customization best-practices. }}<ref name=TagSuiteHome/>
:* {{Timeline-event |date={{Start date|2005|11|14}} |event=Version 2.1 update released with the addition the ''Article Authoring DTD''. }}<ref name=TagSuiteHome/><ref name=AboutJATS>{{cite web |title=JATS and the NLM DTDs |date=8 January 2016 |website=Journal Article Tag Suite |publisher=National Center for Biotechnology Information |url=http://jats.nlm.nih.gov/about.html |dead-url=no |archive-url=http://web.archive.org/web/20160307111920/http://jats.nlm.nih.gov/about.html |archive-date=7 March 2016 }}</ref>
:* {{Timeline-event |date={{Start date|2006|06|08}} |event=Version 2.2 update released. }}<ref name=TagSuiteHome/>
:* {{Timeline-event |date={{Start date|2007|03|28}} |event=Version 2.3 update released. }}<ref name=TagSuiteHome/>
: NLM JATS, version 3

:* {{Timeline-event |date={{Start date|2008|11|21}} |event=Version 3.0 major update released. }}<ref name=TagSuiteHome/><ref name=AboutJATS/>
; NISO JATS
: NISO JATS, version 1.0
:* {{Timeline-event |date={{Start date|2011|03|30}} |end_date={{End date|2011|09|30}} |event=First draft, NISO Z39.96.201x version 0.4 released; six-month comment period. }}<ref name=NISOv0_4>{{cite web |title=NISO JATS v0.4: Draft Standard for Trial Use |website=Journal Article Tag Suite |publisher=National Center for Biotechnology Information |url=http://jats.nlm.nih.gov/0.4/ }}</ref>
:* {{Timeline-event |date={{Start date|2012|07|15}} |event=NISO JATS, v1.0 received NISO approval. }}<ref name=NISOv1_0>{{cite web |title=ANSI/NISO Z39.96-2012 JATS: Journal Article Tag Suite |date=26 July 2013 |website=National Information Standards Organization |url=http://www.niso.org/apps/group_public/project/details.php?project_id=93 }}</ref>
:* {{Timeline-event |date={{Start date|2012|08|09}} |event=NISO JATS, v1.0 received ANSI approval. }}<ref name=NISOv1_0/>
:* {{Timeline-event |date={{Start date|2012|08|22}} |event=ANSI/NISO Z39.96-2012, JATS: Journal Article Tag Suite (version 1.0) published. It supports full backward-compatibility with NLM JATS v3.0. }}<ref name=TagSuiteHome/><ref name=NISOv1_0/>
: NISO JATS, version 1.1
:* {{Timeline-event |date={{Start date|2013|12|09}} |event=First draft, NISO JATS, v1.1d1 released. }}<ref>{{cite web |date=14 April 2015 |title=JATS v1.1d1 (DRAFT) |website=Journal Article Tag Suite |publisher=National Center for Biotechnology Information |url=http://jats.nlm.nih.gov/1.1d1/ }}</ref>
:* {{Timeline-event |date={{Start date|2014|12|29}} |event=Second draft, NISO JATS, v1.1d2 released. }}<ref>{{cite web |date=14 April 2015 |title=JATS v1.1d2 (DRAFT) |website=Journal Article Tag Suite |publisher=National Center for Biotechnology Information |url=http://jats.nlm.nih.gov/1.1d2/ }}</ref>
:* {{Timeline-event |date={{Start date|2015|04|14}} |event=Third draft, NISO JATS, v1.1d released. }}<ref>{{cite web |date=14 April 2015 |title=JATS v1.1d3 (DRAFT) |website=Journal Article Tag Suite |publisher=National Center for Biotechnology Information |url=http://jats.nlm.nih.gov/1.1d3/ }}</ref>
:* {{Timeline-event |date={{Start date|2015|10|22}} |event=NISO JATS, v1.1 received NISO approval. }}<ref name=NISOv1_1>{{cite web |title=ANSI/NISO Z39.96-2015 JATS: Journal Article Tag Suite |date=8 January 2016 |website=National Information Standards Organization |url=http://www.niso.org/apps/group_public/project/details.php?project_id=133 }}</ref>
:* {{Timeline-event |date={{Start date|2015|11|19}} |event=NISO JATS, v1.1 received ANSI approval }}<ref name=NISOv1_1/>
:* {{Timeline-event |date={{Start date|2016|01|06}} |event=ANSI/NISO Z39.96-2015, JATS: Journal Article Tag Suite, version 1.1 published. }}<ref name=NISOv1_1/>

==Technical scope==
By design, this is a model for journal articles, such as the typical research article found in an [[International Association of Scientific, Technical, and Medical Publishers|STM journal]], and not a model for complete journals.<ref name=JATSIntro>{{cite web |title=General Introduction |date=August 2012 |website=Journal Publishing Tag Library NISO JATS Version 1.0 |publisher=National Center for Biotechnology Information |url=http://jats.nlm.nih.gov/publishing/tag-library/1.0/n-7w00.html  }}</ref>

=== Tag sets ===
There are three tag sets, which due to their color-coded documentation, are colloquially referred to by color:
; Journal ''Archiving'' and Interchange ({{color|green|Green}})
: "The most permissive of the Tag Sets,"<ref name=JATSHome>{{cite web |title=JATS: Journal Article Tag Suite |publisher=National Center for Biotechnology Information |url=http://jats.nlm.nih.gov/ }}</ref> primarily intended for the capture and archiving of extant journal data.
; Journal ''Publishing'' ({{color|blue|Blue}})
: "A moderately prescriptive Tag Set,"<ref name=JATSHome/> intended for general use in journal production and publication.
: Formally this model is a subset of the ''Archiving'' model.
; Article ''Authoring'' ({{color|orange|Orange}})
: "The most prescriptive [tightest and smallest] of the Tag Sets,"<ref name=JATSHome/> intended for the relatively lightweight creation of journal articles valid to JATS.
: Formally this model a subset of the ''Publishing'' model.

[[Document type definition]]s (also released in the form of [[RELAX NG]] and [[XML schema]]) define each set and incorporate other standards such as [[MathML]] and XHTML Tables (although not in the [[XHTML]] [[XML namespace|namespace]]).

=== Document structure ===
JATS ''Publishing'' set defines a document that is a top-level component of a journal such as an article, a book or product review, or a letter to the editor. Each such document is composed of front matter (required) and up to three optional parts.<ref name=JATSIntro/> These must appear in the following order:
; Front matter
: The article front matter contains the metadata for the article (also called article header information), for example, the article title, the journal in which it appears, the date and issue of publication for that issue of that journal, a copyright statement, etc. Both article-level and issue-level metadata (in the element <code><article-meta></code>) and journal-level metadata (in the element <code><journal-meta></code>) may be captured.
; Body (of the article)
: The body of the article is the main textual and graphic content of the article. This usually consists of paragraphs and sections, which may themselves contain figures, tables, sidebars (boxed text), etc. The body of the article is optional to accommodate those repositories that just keep article header information and do not tag the textual content.
; Back matter
: If present, the article back matter contains information that is ancillary to the main text, such as a glossary, appendix, or list of cited references.
; Floating material
: A publisher may choose to place all the floating objects in an article and its back matter (such as tables, figures, boxed text sidebars, etc.) into a separate container element outside the narrative flow for convenience of processing.<ref name=JATSIntro/>

Following the front, body, back, and floating material, there may be either one or more responses to the article or one or more subordinate articles.<ref name=JATSIntro/>

== Example ==
This is the minimal article's structure,

<source lang="xml">
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE article
  PUBLIC "-//NLM//DTD JATS (Z39.96) Journal Publishing DTD v1.0 20120330//EN"
         "JATS-journalpublishing1.dtd"
>
<article dtd-version="1.0" article-type="article" specific-use="migrated"
 xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:xlink="http://www.w3.org/1999/xlink" 
>
  <front>...</front>
  <body>...</body>
  <back>...</back>
</article>
</source>

The <code>DOCTYPE</code> header is optional,  a legacy from [[Standard Generalized Markup Language|SGML]] and [[Document type definition|DTD]]-oriented [[XML schema#Validation|validators]]. The <code>dtd-version</code> attribut can be used even without a DTD header.

The root element <code>article</code> is common for any version of JATS or "JATS family", as NLM DTDs.  The rules for <code>front</code>, <code>body</code> and  <code>back</code> tags validation, depends on the JATS version, but all versions have similar structure, with good compatibility in a range of years. The evolution of the schema preserves an overall stability.

Less common, "only <code>front</code>", "only <code>front</code> and <code>back</code>" variations are also used for other finalities than full-content representation. The general article composition (as an [[Backus–Naur form|DTD-content expression]]) is
<pre>
   (front, body?, back?, floats-group?, (sub-article* | response*))
</pre>

== Tools ==
There are a variety of tools for create, edit, convert and transform JATS.
They range from simple forms<ref>A 2012's semanticpublishing.wordpress.com [http://semanticpublishing.wordpress.com/2012/12/20/jats-metadata-entry-form/ JATS Metadata Input Form].</ref> to complete conversion automation:

=== Conversion ===
;To JATS: Take as input a scientific document, and, with some human support, produce a JATS output.
* [[OpenOffice.org|OpenOffice]] ([[LibreOffice]]) and [[MS Word]] documents to JATS:
** ''OxGarage'':<ref name="oxga">http://www.oucs.ox.ac.uk/oxgarage/ ([http://wiki.tei-c.org/index.php/OxGarage documentation])</ref> can convert documents from various formats into "National Library of Medicine (NLM) DTD 3.0".
** ''meTypeset'': meTypeset<ref>{{cite web|url=https://github.com/MartinPaulEve/meTypeset|title=MartinPaulEve/meTypeset|work=GitHub}}</ref> "is a fork of the OxGarage stack" "to convert from Microsoft Word .docx format to NLM/JATS-XML".
** ''eXtyles'':<ref>{{cite web|url=http://www.inera.com/extyles-products/extyles|title=eXtyles|publisher=}}</ref> automates time-consuming aspects of document editing in Microsoft Word and exports to JATS XML (as well as many other DTDs).
* [[Markdown]] to JATS: [[pandoc]]'s "pandoc-jats" plugin.<ref>https://github.com/mfenner/pandoc-jats  ([http://blog.martinfenner.org/2013/12/12/from-markdown-to-jats-xml-in-one-step/ how-to and explain])</ref> Pandoc 2.0 can convert a number of input formats to JATS.<ref>https://groups.google.com/d/msg/pandoc-discuss/09XJwmrTXjI/hNKYso_5AgAJ</ref>
* PDF to JATS: this is a very difficult problem to solve.  Success depends on how well structured your PDFs are and, for batch conversion, how consistently structured your PDFs are.
**  Shabash Merops<ref>http://shabash.net/merops/automatic_text_editing.php</ref>
** The ''Public Knowledge Project''<ref name="pkp">{{cite web|url=http://pkp.sfu.ca/|title=Public Knowledge Project|publisher=}}</ref> is developing a pipeline for converting PDF to JATS.  It will include use of ''pdfx''.<ref>{{cite journal | last1 = Constantin | first1 = S.Pettifer | year = 2013 | title = PDFX: fully-automated PDF-to-XML conversion of scientific literature | url = | journal = Proceedings of the 2013 ACM symposium on Document engineering - DocEng ''13| volume = | issue = | page = | doi = 10.1145/2494266.2494271 }}</ref>

;From JATS: Take JATS as input, produce another kind of document as output.
* from JATS to HTML
** JATS Preview Stylesheets (canonical [[XSLT]] conversion) 
** ''eLife Lens''<ref>{{cite web|url=http://lens.elifesciences.org|title=eLife Lens|publisher=}}</ref> converts NLM XML to JSON for displaying using HTML and Javascript.
* from JATS to PDF: some JATS Preview Stylesheets, XSLT + XSL-FO conversion.
* from JATS to EPUB.<ref>[http://www.biglist.com/lists/lists.mulberrytech.com/jats-list/archives/201204/msg00002.html biglist.com/mulberrytech msg] and [http://www.ncbi.nlm.nih.gov/books/NBK159966/ ncbi.nlm.nih.gov/books article] description</ref>
* Generic (from JATS DTD): ''DtdAnalyzer''<ref>{{cite web|url=https://github.com/ncbi/DtdAnalyzer|title=ncbi/DtdAnalyzer|work=GitHub}}</ref> &mdash; compare JATS with other DTDs and helps into create a XML representation, XSLT and Schematron generation, and other tools.

=== Editors ===
* JATS Framework for oXygen XML Editor:  users of oXygen XML Editor and oXygen XML Author can now install support for current versions of NISO JATS (and as a bonus, NLM BITS). Based on an identifier given in a DOCTYPE declaration, oXygen will detect that you are editing a JATS document and provide stylesheets and utilities.<ref>{{cite web|url=https://github.com/wendellpiez/oXygenJATSframework|title=wendellpiez/oXygenJATSframework|work=GitHub}}</ref>
* FontoXML for JATS: WYSIWYS editor for editing and reviewing JATS content: <ref>https://fontoxml.com/jats/</ref>
* PubRef "Pipeline": Browser-based realtime-preview JATS editor: <ref>https://pubref.org</ref>
* ''Annotum'':<ref>{{cite web|url=http://annotum.org/|title=Annotum|work=Annotum}}</ref> a WordPress theme that contains WYSIWYG authoring in JATS (Kipling subset), peer-review and editorial management, and publishing.<ref>{{cite web|url=http://www.ncbi.nlm.nih.gov/books/NBK63828/|title=Annotum: An open-source authoring and publishing platform based on WordPress|author=Carl Leubsdorf, Jr|publisher=Journal Article Tag Suite Conference (JATS-Con) Proceedings 2011 - NCBI Bookshelf}}</ref>
* JATS edition for web-based XML editor [[Xeditor]].
* ''Texture Editor''<ref>https://github.com/substance/texture</ref> of the Substance Consortium.<ref>http://substance.io/consortium/</ref>

=== Preview ===
Tools that render JATS as HTML, usually on fly.
* JATS Preview Stylesheets:<ref>{{cite web|url=https://github.com/NCBITools/JATSPreviewStylesheets|title=ncbi/JATSPreviewStylesheets|work=GitHub}}</ref> the JATS Preview Stylesheets are a series of .xsl, .xpl, .css, and .sch files that will create .html or .pdf versions of valid NISO Z39.96-2012 JATS 1.0 files. It is primarily intended for internal use by publishers and a basis for customization.<ref>{{cite web|url=http://www.ncbi.nlm.nih.gov/books/NBK47104/|title=Fitting the Journal Publishing 3.0 Preview Stylesheets to Your Needs: Capabilities and Customizations|author=Wendell Piez|publisher=Journal Article Tag Suite Conference (JATS-Con) Proceedings 2010 - NCBI Bookshelf}}</ref>
* ''PubReader'' – "The PubReader view is an alternative web presentation ... Designed particularly for enhancing readability on tablet and other small screen devices, PubReader can also be used on desktops and laptops and from multiple web browsers".<ref>[http://www.ncbi.nlm.nih.gov/pmc/about/pubreader/ NCBI/PubReader] with source-code at [https://github.com/ncbi/PubReader github.com/ncbi/PubReader]</ref>

=== Customization ===
; Jatsdoc
: Produces documentation for any particular JATS customization. Jatsdoc is integrated with NCBI's ''DtdAnalyzer''.<ref>{{cite web|url=https://github.com/Klortho/jatsdoc|first=Chris |last=Maloney |title=Jatsdoc Documentation Browser |website=GitHub }}</ref><ref>{{cite web|url=http://dtd.nlm.nih.gov/ncbi/dtdanalyzer/|title=DtdAnalyzer: A tool for analyzing and manipulating DTDs |website=Journal Archiving and Interchange Tag Suite |publisher=National Center for Biotechnology Information }}</ref>

== JATS central repositories  ==
As NISO JATS began the ''de facto'' and ''de juri'' standard for [[open access journal]]s, the [[scientific community]] has  adopted the JATS repositories as a kind of [[legal deposit]], more valuable than the [[Digital library|traditional digital libraries]] where only a PDF version is stored. [[Open knowledge]] need richer and structured formats as JATS: PDF and JATS must be certified as "same content", and the set "PDF+JATS" forming the unit of legal deposit. 
List of ''JATS repositories'' and its contained:

* [[PubMed Central]]: (please check these numbers)
** US PubMed Central: in 2016 ~3.8 million articles<ref>PMC home, http://www.ncbi.nlm.nih.gov/pmc/</ref>
** [[Europe PubMed Central]]: in 2016 ~3,7 million articles <ref>PMC Europe, "about" page, http://europepmc.org/About</ref>
** [[PubMed Central Canada]]: in 2013 ~2.6 million articles.<ref>PMC-Canadá FAQ, http://pubmedcentralcanada.ca/pmcc/static/aboutUs/#q6</ref>
* [[SciELO]]: in 2016 ~0.6 million articles<ref>SciELO home, http://www.scielo.org/php/index.php?lang=en</ref>

NOTE: there are some overlapping in the repositories, the same article can be accessed in more than one repositories.

== See also==
{{Columns-start|num=3}}
Related to
* [[NISO]]
* [[Open science data]]
* [[Scientific literature]]
* [[Semantic publishing]]
* [[Separation of presentation and content]]&nbsp;
* [[XML]]
{{Column}}
Used by
* [[PubMed Central]]
* [[SciELO]]
{{Column}}
Similar to
* [[DocBook]]
* [[Text Encoding Initiative]]
* [[XHTML]]
{{Columns-end}}

==References==
{{reflist|30em}}

== Further reading ==
* {{cite web |first1=Abel L. |last1=Packer |first2=Eliana |last2=Salgado |first3=Javani |last3=Araujo |first4=Letícia |last4=Aquino |first5=Renata |last5=Almeida |first6=Jesner |last6=Santos |first7=Suely |last7=Lucena |first8=Caroline M. |last8=Soares |date=4 April 2014  |title= Why XML? |website=SciELO in Perspective |url=http://blog.scielo.org/en/2014/04/04/why-xml }}
* {{cite web |first=Molly |last=Sharp |date=4 June 2013 |title=Structured Documents for Science: JATS XML as Canonical Content Format |website=PLOS Tech |url=http://blogs.plos.org/tech/structured-documents-for-science-jats-xml-as-canonical-content-format/ }}

== External links ==
* [http://jats.nlm.nih.gov NLM Journal Journal Article Tag Suite] – NCBI's information and documentation site.
* [https://webservices.itcs.umich.edu/mediawiki/jats/ JATS Wiki]
* NISO JATS Version 1.1 (current standard):
** [http://jats.nlm.nih.gov/archiving/ Archiving and Interchange] tag library
** [http://jats.nlm.nih.gov/publishing/ Publishing] tag library
** [http://jats.nlm.nih.gov/articleauthoring/ Article Authoring] tag library
* Styles and customization:
** [http://docs.scielo.org/projects/scielo-publishing-schema/ SciELO Publishing Schema] (SPS) – SciELO's customization.
** [https://www.ncbi.nlm.nih.gov/pmc/pmcdoc/tagging-guidelines/article/style.html Tagging Guidelines of PubMed Central's preferred XML tagging style]
** [http://www.iso.org/schema/isosts/v1.0/doc/  ISO Standards Tag Set (ISOSTS) as a customization of NISO JATS]
**  NISO ''[http://jats.nlm.nih.gov/extensions/bits/tag-library/1.0/ Book Interchange Tag Suite (BITS)]'', based on JATS.

[[Category:Markup languages]]
[[Category:XML-based standards]]
[[Category:Academic publishing]]
[[Category:Open science]]
[[Category:Open data]]