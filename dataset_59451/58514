{{italic title}}
{{Infobox Journal
| title        = Journal of Speculative Philosophy
| cover        = [[Image:JSP_newcov.jpg|150px]]
| editor       = Vincent Colapietro, John J. Stuhr
| discipline   = [[Philosophy]]
| language     = English
| abbreviation = 
| publisher    = [[Penn State University Press]]
| country      = United States
| frequency    = Quarterly
| history      = 1987–present
| openaccess   = 
| impact       = 
| impact-year  = 
| website      = http://philosophy.emory.edu/stuhr/journalofspeculativephilosophy/index.html
| link1        = http://muse.jhu.edu/journals/journal_of_modern_periodical_studies/toc/current.html
| link1-name   = Online access
| link2        = 
| link2-name   = 
| RSS          = 
| atom         = 
| JSTOR        = 0891625X
| OCLC         = 497826301
| LCCN         = 
| CODEN        = 
| ISSN         = 0891-625X
| eISSN        = 1527-9383
}}
The '''''Journal of Speculative Philosophy''''' is an [[academic journal]] that examines basic philosophical questions, the interaction between [[Continental philosophy|Continental]] and [[American philosophy|American]] philosophy, and the relevance of historical philosophers to contemporary thinkers. The journal is published quarterly by the [[Penn State University Press]].

==History==
An unrelated journal by the same name was established in 1867 by [[William Torrey Harris]] of [[St. Louis]], [[Missouri]], thus becoming the first journal on philosophy in the English-speaking world.<ref>Mott, Frank Luther. ''A History of American Magazines, Vol. 3: 1865-1885''. Oxford University Press, 1970. p. 385.</ref><ref name=murphy/> The journal ceased publication in 1893, but the name was revived in 1987 at the Pennsylvania State University with the founding of the ''Journal of Speculative Philosophy''.<ref name=murphy>Murphy, Arthur E. ''Reason, Reality and Speculative Philosophy''. University of Wisconsin Press, 1996. p. xlvii.</ref>

==References==
{{reflist}}

== External links ==
* {{Official website|http://philosophy.emory.edu/stuhr/journalofspeculativephilosophy/index.html}}
*[http://www.psupress.org/journals/jnls_jsp.html ''Journal of Speculative Philosophy'' on the Penn State Press website]
*[http://muse.jhu.edu/journals/jsp/ ''Journal of Speculative Philosophy''] at [[Project MUSE]]

[[Category:English-language journals]]
[[Category:Penn State University Press academic journals]]
[[Category:Publications established in 1987]]
[[Category:Philosophy journals]]
[[Category:Quarterly journals]]


{{philo-journal-stub}}