{{Infobox journal
| title = Molecular Biology Reports
| cover = 
| caption = 
| former_name = 
| abbreviation = Mol. Biol. Rep.
| discipline = [[Biology]], [[biochemistry]], [[biophysics]]
| editor = Nilanjana Maulik
| publisher = [[Springer Science+Business Media]]
| country = 
| history = 1974-present
| frequency = Monthly
| openaccess = 
| license = 
| impact = 2.024
| impact-year = 2014
| ISSN = 0301-4851
| eISSN = 1573-4978
| CODEN = 
| JSTOR = 
| LCCN = 
| OCLC = 300185386
| website = http://www.springer.com/journal/11033/about
| link1 = http://link.springer.com/journal/volumesAndIssues/11033
| link1-name = Online access
| link2 = <!-- up to |link5= -->
| link2-name = <!-- up to |link5-name= -->
}}

'''''Molecular Biology Reports''''' is a monthly [[peer-reviewed]] [[scientific journal]] covering research on normal and pathological molecular processes.

==Abstracting and indexing==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Science Citation Index]]
* [[PubMed]]/[[MEDLINE]] 
* [[Scopus]]
* [[Embase]]
* [[Chemical Abstracts Service]]
* [[EBSCO Information Services|EBSCO databases]]
* [[CAB International]] 
* [[Academic OneFile]] 
* [[AGRICOLA]]
* [[Aquatic Sciences and Fisheries Abstracts]] 
* [[Biological Abstracts]]
* [[BIOSIS Previews]]
* [[Current Contents]]/Life Sciences
* [[Elsevier Biobase]]
* [[EMBiology]]
* [[Global Health]] 
* [[INIS Atomindex]]
}}
According to the ''[[Journal Citation Reports]]''. The journal has a 2014 [[impact factor]] of 2.024.<ref name=WoS>{{cite book |year=2015 |chapter=Molecular Biology Reports |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{Reflist}}

==External links==
*{{Official website|http://www.springer.com/journal/11033/about}}

[[Category:English-language journals]]
[[Category:Molecular and cellular biology journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1974]]


{{Biology-journal-stub}}