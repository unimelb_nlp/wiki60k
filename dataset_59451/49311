'''Linear Optical Quantum Computing''' or '''Linear Optics Quantum Computation''' ('''LOQC''') is a paradigm of [[quantum computer|quantum computation]], allowing (under certain conditions, described below) [[universal quantum computer|universal quantum computation]]. LOQC uses [[photon]]s as information carriers, mainly uses [[linear optics|linear optical]] elements (including [[beam splitter]]s, [[phase shift module|phase shifters]], and [[mirror]]s) to process [[quantum information]], and uses photon detectors and [[quantum memory|quantum memories]] to detect and store quantum information.<ref name="Adami1999">{{cite journal | title=Quantum computation with linear optics | author1=Adami, C. | author2=Cerf, N. J. | journal=Quantum Computing and Quantum Communications | year=1999 | pages=391–401 | publisher=Springer | doi=10.1007/3-540-49208-9_36 | series=Lecture Notes in Computer Science | isbn=978-3-540-65514-5 | volume=1509}}</ref><ref name="Knill2001">{{cite journal | title=A scheme for efficient quantum computation with linear optics | author1=Knill, E. | journal=Nature | year=2001 | volume=409 | pages=46–52 | author2=Laflamme, R. | author3=Milburn, G. J. | publisher=Nature Publishing Group | doi=10.1038/35051009 | pmid=11343107 | issue=6816|bibcode = 2001Natur.409...46K }}</ref><ref name="Kok2007">{{cite journal | title=Linear optical quantum computing with photonic qubits | author=Kok, P. | journal=Rev. Mod. Phys. | year=2007 | volume=79 | pages=135–174 | author2=Munro, W. J. | author3=Nemoto, K. | author4=Ralph, T. C. | author5=Dowling, J. P. | author6=Milburn, G. J. | publisher=American Physical Society | doi=10.1103/RevModPhys.79.135|arxiv = quant-ph/0512071 |bibcode = 2007RvMP...79..135K }}</ref>

== Overview ==

Although there are many other implementations for [[quantum information processing]] (QIP) and quantum computation, [[quantum optics|optical quantum systems]] are prominent candidates, since they link quantum computation and [[quantum communication]] in the same framework. In optical systems for quantum information processing, the unit of light in a given mode—or [[photon]]—is used to represent a [[qubit]]. [[Quantum superposition|Superpositions]] of quantum states can be easily represented, [[quantum encryption|encrypted]], transmitted and detected using photons. Besides, linear optical elements of optical systems may be the simplest building blocks to realize quantum operations and [[quantum gate]]s. Each linear optical element equivalently applies a [[unitary transformation]] on a finite number of qubits. The system of finite linear optical elements constructs a network of linear optics, which can realize any [[quantum circuit]] diagram or [[quantum network]] based on the [[quantum circuit]] model. Quantum computing with continuous variables is also possible under the linear optics scheme.<ref name="Lloyd2003">{{cite journal | title=Quantum computation over continuous variables | author=Lloyd, S. | journal=Quantum Information with Continuous Variables | year=2003 | pages=9–17 | author2=Braunstein, S. L. | publisher=Springer | bibcode=1999PhRvL..82.1784L | volume=82 | doi=10.1103/PhysRevLett.82.1784 | issue=8|arxiv = quant-ph/9810082 }}</ref>

The universality of 1- and 2-bit [[quantum gate|gates]] to implement arbitrary quantum computation has been proven.<ref>{{Cite journal
| doi = 10.1103/PhysRevA.51.1015
| volume = 51
| issue = 2
| pages = 1015–1022
| last = DiVincenzo
| first = David P.
| title = Two-bit gates are universal for quantum computation
| journal = Physical Review A
| accessdate = 2014-01-25
| date = 1995-02-01
| url = http://link.aps.org/doi/10.1103/PhysRevA.51.1015
|arxiv = cond-mat/9407022 |bibcode = 1995PhRvA..51.1015D }}</ref><ref>{{Cite journal
| doi = 10.1098/rspa.1995.0065
| issn = 1471-2946
| volume = 449
| issue = 1937
| pages = 669–677
| last = Deutsch
| first = David
| first2 = Adriano
| last2 = Barenco
| first3 = Artur
| last3 = Ekert
| title = Universality in Quantum Computation
| journal = Proceedings of the Royal Society of London. Series A: Mathematical and Physical Sciences
| accessdate = 2014-01-25
| date = 1995-06-08
| url = http://rspa.royalsocietypublishing.org/content/449/1937/669
|arxiv = quant-ph/9505018 |bibcode = 1995RSPSA.449..669D }}</ref><ref>{{Cite journal
| doi = 10.1098/rspa.1995.0066
| issn = 1471-2946
| volume = 449
| issue = 1937
| pages = 679–683
| last = Barenco
| first = Adriano
| title = A Universal Two-Bit Gate for Quantum Computation
| journal = Proceedings of the Royal Society of London. Series A: Mathematical and Physical Sciences
| accessdate = 2014-01-25
| date = 1995-06-08
| url = http://rspa.royalsocietypublishing.org/content/449/1937/679
|arxiv = quant-ph/9505016 |bibcode = 1995RSPSA.449..679B }}</ref><ref>{{Cite journal
| doi = 10.1103/PhysRevLett.75.346
| volume = 75
| issue = 2
| pages = 346–349
| last = Lloyd
| first = Seth
| title = Almost Any Quantum Logic Gate is Universal
| journal = Physical Review Letters
| accessdate = 2014-01-25
| date = 1995-07-10
| url = http://link.aps.org/doi/10.1103/PhysRevLett.75.346
|bibcode = 1995PhRvL..75..346L }}</ref> Up to <math>N\times N</math> unitary matrix operations (<math>U(N)</math>) can be realized by only using mirrors, beam splitters and phase shifters<ref>{{Cite journal
| doi = 10.1103/PhysRevLett.73.58
| volume = 73
| issue = 1
| pages = 58–61
| last = Reck
| first = Michael
| first2 = Anton
| last2 = Zeilinger
| first3 = Herbert J.
| last3 = Bernstein
| first4 = Philip
| last4 = Bertani
| title = Experimental realization of any discrete unitary operator
| journal = Physical Review Letters
| accessdate = 2014-01-25
| date = 1994-07-04
| url = http://link.aps.org/doi/10.1103/PhysRevLett.73.58
|bibcode = 1994PhRvL..73...58R
| pmid=10056719}}</ref> (this is also a starting point of [[boson sampling]] and of [[Computational complexity theory|computational complexity]] analysis for LOQC). It points out that each <math>U(N)</math> operator with <math>N</math> inputs and <math>N</math> outputs can be constructed  via <math>\mathcal{O}(N^2)</math> linear optical elements. Based on the reason of universality and complexity, LOQC usually only uses mirrors, beam splitters, phase shifters and their combinations such as [[Mach-Zehnder interferometer]]s with phase shifts to implement arbitrary [[operator (physics)|quantum operators]]. If using a non-deterministic scheme, this fact also implies that LOQC could be resource-inefficient in terms of the number of optical elements and time steps needed to implement a certain quantum gate or circuit, which is a major drawback of LOQC.

Operations via linear optical elements (beam splitters, mirrors and phase shifters, in this case) preserve the photon statistics of input light. For example, a [[coherent light|coherent]] (classical) light input produces a coherent light output; a superposition of quantum states input yields a [[nonclassical light|quantum light state]] output.<ref name="Kok2007" /> Due to this reason, people usually use single photon source case to analyze the effect of linear optical elements and operators. Multi-photon cases can be implied through some statistical transformations.

An intrinsic problem in using photons as information carriers is that photons hardly interact with each other. This potentially causes a scalability problem for LOQC, since nonlinear operations are hard to implement, which can increase the complexity of operators and hence can increase the resources required to realize a given computational function. One way to solve this problem is to bring nonlinear devices into the quantum network. For instance, the [[Kerr effect]] can be applied into LOQC to make a single-photon [[controlled NOT gate|controlled-NOT]] and other operations.<ref>{{Cite journal
| doi = 10.1103/PhysRevLett.62.2124
| volume = 62
| issue = 18
| pages = 2124–2127
| last = Milburn
| first = G. J.
| title = Quantum optical Fredkin gate
| journal = Physical Review Letters
| accessdate = 2014-01-25
| date = 1989-05-01
| url = http://link.aps.org/doi/10.1103/PhysRevLett.62.2124
|bibcode = 1989PhRvL..62.2124M }}</ref><ref>{{Cite journal
| doi = 10.1080/09500340408230417
| issn = 0950-0340
| volume = 51
| issue = 8
| pages = 1211–1222
| last = Hutchinson
| first = G. D.
| first2 = G. J.
| last2 = Milburn
| title = Nonlinear quantum optical computing via measurement
| journal = Journal of Modern Optics
| accessdate = 2014-01-25
| year = 2004
| url = http://www.tandfonline.com/doi/abs/10.1080/09500340408230417
|arxiv = quant-ph/0409198 |bibcode = 2004JMOp...51.1211H }}</ref>

=== KLM protocol ===
{{main article|KLM protocol}}
It was believed that adding nonlinearity to the linear optical network was sufficient to realize efficient quantum computation.<ref>{{Cite journal
| doi = 10.1016/0375-9601(92)90201-V
| issn = 0375-9601
| volume = 167
| issue = 3
| pages = 255–260
| last = Lloyd
| first = Seth
| title = Any nonlinear gate, with linear gates, suffices for computation
| journal = Physics Letters A
| accessdate = 2014-01-25
| date = 1992-07-20
| url = http://www.sciencedirect.com/science/article/pii/037596019290201V
|bibcode = 1992PhLA..167..255L }}</ref> However, to implement nonlinear optical effects is a difficult task. In 2000, Knill, Laflamme and Milburn proved that it is possible to create universal quantum computers solely with linear optical tools.<ref name="Knill2001" /> Their work has become known as the "KLM scheme" or "[[KLM protocol]]", which uses linear optical elements, single photon sources and photon detectors as resources to construct a quantum computation scheme involving only [[ancilla (quantum computing)|ancilla]] resources, [[quantum teleportation]]s and [[quantum error correction|error corrections]]. It uses another way of efficient quantum computation with linear optical systems, and promotes nonlinear operations solely with linear optical elements.<ref name="Kok2007" />

At its root, the KLM scheme induces an effective interaction between photons by making projective measurements with [[photodetector]]s, which falls into the category of non-deterministic quantum computation. It is based on a non-linear sign shift between two qubits that uses two ancilla photons and post-selection.<ref>{{Cite journal
| doi = 10.1137/S0097539795293639
| issn = 0097-5397
| volume = 26
| issue = 5
| pages = 1524–1540
| last = Adleman
| first = Leonard M.
| first2 = Jonathan
| last2 = DeMarrais
| first3 = Ming-Deh A.
| last3 = Huang
| title = Quantum Computability
| journal = SIAM Journal on Computing
| accessdate = 2014-01-26
| date = 1997
| url = http://epubs.siam.org/doi/abs/10.1137/S0097539795293639
}}</ref> It is also based on the demonstrations that the probability of success of the quantum gates can be made close to one by using entangled states prepared non-deterministically and [[quantum teleportation]] with single-qubit operations<ref>{{Cite journal
| doi = 10.1103/PhysRevLett.70.1895
| volume = 70
| issue = 13
| pages = 1895–1899
| last = Bennett
| first = Charles H.
| first2 = Gilles
| last2 = Brassard
| first3 = Claude
| last3 = Crépeau
| first4 = Richard
| last4 = Jozsa
| first5 = Asher
| last5 = Peres
| first6 = William K.
| last6 = Wootters
| title = Teleporting an unknown quantum state via dual classical and Einstein-Podolsky-Rosen channels
| journal = Physical Review Letters
| accessdate = 2014-01-26
| date = 1993-03-29
| url = http://link.aps.org/doi/10.1103/PhysRevLett.70.1895
|bibcode = 1993PhRvL..70.1895B
| pmid=10053414}}</ref><ref name="Gottesman1999">{{Cite journal
| doi = 10.1038/46503
| issn = 0028-0836
| volume = 402
| issue = 6760
| pages = 390–393
| last = Gottesman
| first = Daniel
| first2 = Isaac L.
| last2 = Chuang
| title = Demonstrating the viability of universal quantum computation using teleportation and single-qubit operations
| journal = Nature
| accessdate = 2014-01-26
| date = 1999-11-25
| url = http://www.nature.com/nature/journal/v402/n6760/abs/402390a0.html
|arxiv = quant-ph/9908010 |bibcode = 1999Natur.402..390G }}</ref> Otherwise, without a high enough success rate of a single quantum gate unit, it may require an exponential amount of computing resources. Meanwhile, the KLM scheme is based on the fact that proper quantum coding can reduce the resources for obtaining accurately encoded qubits efficiently with respect to the accuracy achieved, and can make LOQC fault-tolerant for photon loss, detector inefficiency and phase [[decoherence]]. As a result, LOQC can be robustly implemented through the KLM scheme with a low enough resource requirement to suggest practical scalability, making it as promising a technology for QIP as other known implementations.

=== Boson sampling ===
{{main article|boson sampling}}
The more limited [[boson sampling]] model was suggested and analyzed by Aaronson and Arkhipov in 2013.<ref name="Aaronson13">{{cite journal|last1=Aaronson|first1=Scott|last2=Arkhipov|first2=Alex|title=The computational complexity of linear optics|journal=Theory of Computing|date=2013|volume=9|pages=143–252|doi=10.4086/toc.2013.v009a004|url=http://www.theoryofcomputing.org/articles/v009a004/}}</ref> It is not believed to be universal,<ref name="Aaronson13"/> but can still solve problems that are believed to be beyond the ability of classical computers, such as the [[boson sampling#The task of boson sampling|boson sampling problem]].

== Elements of LOQC ==

[[DiVincenzo's criteria]] for quantum computation and QIP<ref name="DiVincenzo1998">{{cite journal | title=Quantum information is physical | author=DiVincenzo, D. | journal=Superlattices and Microstructures | year=1998 | volume=23 | pages=419–432 | author2=Loss, D. | doi=10.1006/spmi.1997.0520 | issue=3–4|arxiv = cond-mat/9710259 |bibcode = 1998SuMi...23..419D }}</ref><ref name="DiVincenzo2000">{{cite journal | title=The Physical Implementation of Quantum Computation | author=Divincenzo, D. P. | journal=Fortschritte der Physik | year=2000 | volume=48 | pages=771–783 | bibcode=2000ForPh..48..771D | doi=10.1002/1521-3978(200009)48:9/11<771::AID-PROP771>3.0.CO;2-E | issue=9–11|arxiv = quant-ph/0002077 }}</ref> give that a universal system for QIP should satisfy at least the following requirements:
# a scalable physical system with well characterized qubits,
# the ability to initialize the state of the qubits to a simple fiducial state, such as <math>|000\cdots\rangle</math>,
# long relevant decoherence times, much longer than the gate operation time,
# a "universal" set of quantum gates (this requirement cannot be satisfied by a non-universal system),
# a qubit-specific measurement capability;<br />if the system is also aiming for quantum communication, it should also satisfy at least the following two requirements:
# the ability to interconvert stationary and [[flying qubit]]s, and
# the ability to faithfully transmit flying qubits between specified location.

As a result of using photons and linear optical circuits, in general LOQC systems can easily satisfy conditions 3, 6 and 7.<ref name="Kok2007" /> The following sections mainly focus on the implementations of quantum information preparation, readout, manipulation, scalability and error corrections, in order to discuss the advantages and disadvantages of LOQC as a candidate for QIP

=== Qubits and modes ===

A [[qubit]] is one of the fundamental QIP units. A [[qubit#Qubit states|qubit state]] which can be represented by
<math>\alpha |0\rangle + \beta|1\rangle</math> is a [[quantum superposition|superposition state]] which, if [[Quantum measurement|measured]] in the [[orthonormal basis]] <math>\{|0\rangle, |1\rangle\}</math>, has probability <math>|\alpha|^2</math> of being in the <math>|0\rangle</math> state and probability <math>|\beta|^2</math> of being in the <math>|1\rangle</math> state, where <math>|\alpha|^2+|\beta|^2=1</math> is the normalization condition. An optical mode is a distinguishable optical communication channel, which is usually labeled by subscripts of a quantum state. There are many ways to define distinguishable optical communication channels. For example, a set of modes could be different [[Photon polarization|polarization]] of light which can be picked out with linear optical elements, various [[frequency|frequencies]], or a combination of the two cases above.

In the KLM protocol, each of the photons is usually in one of two modes, and the modes are different between the photons (the possibility that a mode is occupied by more than one photon is zero). This is not the case only during implementations of [[Quantum gate#Controlled gates|controlled quantum gates]] such as CNOT. When the state of the system is as described, the photons can be distinguished, since they are in different modes, and therefore a qubit state can be represented using a single photon in two modes, vertical (V) and horizontal (H): for example, <math>|0\rangle \equiv |0,1\rangle _{VH}</math> and <math>|1\rangle \equiv |1,0\rangle _{VH}</math>. It is common to refer to the states defined via occupation of modes as [[Fock state]]s.

In boson sampling, photons are not distinguished, and therefore cannot directly represent the qubit state. Instead, we represent the [[qudit]] state of the entire quantum system by using the Fock states of <math>M</math> modes which are occupied by <math>N</math> indistinguishable single photons (this is a <math>\tbinom {M+N-1} {M} </math>-level quantum system).

=== State preparation ===

To prepare a desired multi-photon quantum state for LOQC, a single-photon state is first required. Therefore, [[Non-linear optics|non-linear optical elements]], such as [[single photon sources|single-photon generators]] and some optical modules, will be employed. For example, [[Spontaneous parametric down-conversion|optical parametric down-conversion]] can be used to conditionally generate the <math>|1\rangle \equiv |1,0\rangle _{VH}</math> state in the vertical polarization channel at time <math>t</math> (subscripts are ignored for this single qubit case). By using a conditional single-photon source, the output state is guaranteed, although this may require several attempts (depending on the success rate). A joint multi-qubit state can be prepared in a similar way. In general, an arbitrary quantum state can be generated for QIP with a proper set of photon sources.

=== Implementations of elementary quantum gates ===

To achieve universal quantum computing, LOQC should be capable of realizing a complete set of [[quantum gate#Universal quantum gates|universal gates]]. This can be achieved in the KLM protocol but not in the boson sampling model.

Ignoring error correction and other issues, the basic principle in implementations of elementary quantum gates using only mirrors, beam splitters and phase shifters is that by using these [[linear optics|linear optical]] elements, one can construct any arbitrary 1-qubit unitary operation; in other words, those linear optical elements support a complete set of operators on any single qubit.

The unitary matrix associated with a beam splitter <math>\mathbf{B}_{\theta,\phi}</math> is:

:<math> U(\mathbf{B}_{\theta,\phi})
=\begin{bmatrix}
\cos \theta & -e^{i\phi}\sin \theta \\
e^{-i\phi} \sin \theta & \cos \theta \end{bmatrix}</math>,

where <math>\theta</math> and <math>\phi</math> are determined by the [[Reflection coefficient|reflection amplitude]] <math>r</math> and the [[transmission coefficient|transmission amplitude]] <math>t</math> (relationship will be given later for a simpler case). For a symmetric beam splitter, which has a phase shift <math>\phi=\frac{\pi}{2}</math> under the unitary transformation condition <math>|t|^2+|r|^2=1</math> and <math>t^*r+tr^*=0</math>, one can show that

:<math> U(\mathbf{B}_{\theta,\phi=\frac{\pi}{2}})
=\begin{bmatrix} t & r\\
r & t\end{bmatrix}
=\begin{bmatrix}
\cos \theta & -i\sin \theta \\
-i \sin \theta & \cos \theta \end{bmatrix}=\cos \theta \hat{I}-i \sin \theta \hat{\sigma}_x=e^{-i\theta\hat{\sigma}_x}</math>,

which is a rotation of the single qubit state about the <math>x</math>-axis by <math>2\theta=2\cos^{-1}(|t|)</math> in the [[Bloch sphere]].

A mirror is a special case where the reflecting rate is 1, so that the corresponding unitary operator is a [[rotation matrix]] given by

:<math>R(\theta) = 
\begin{bmatrix}
\cos \theta & -\sin \theta \\
\sin \theta & \cos \theta \\
\end{bmatrix}
</math>.

For most cases of mirrors used in QIP, the [[angle of incidence (optics)|incident angle]] <math>\theta=45^\circ</math>.

Similarly, a phase shifter operator <math>\mathbf{P}_\phi</math> associates with a unitary operator described by <math>U(\mathbf{P}_\phi)=e^{i\phi}</math>, or, if written in a 2-mode format

:<math> U(\mathbf{P}_{\phi})= \begin{bmatrix}
 e^{i\phi} & 0 \\
0 & 1  \end{bmatrix}=\begin{bmatrix} e^{i\phi/2} & 0\\
0 & e^{-i\phi/2}\end{bmatrix} \text{(global phase ignored)}=e^{i\frac{\phi}{2} \hat{\sigma}_z}</math>,

which is equivalent to a rotation of <math>-\phi</math> about the <math>z</math>-axis.

Since any two [[Special unitary group|<math>SU(2)</math> rotations]] along orthogonal rotating axes can generate arbitrary rotations in the Bloch sphere, one can use a set of symmetric beam splitters and mirrors to realize an arbitrary <math>SU(2)</math> operators for QIP. The figures below are examples of implementing a [[Quantum gate#Hadamard gate|Hadamard gate]] and a [[Quantum gate#Pauli-X gate (= NOT gate)|Pauli-X-gate]] (NOT gate) by using beam splitters (illustrated as rectangles connecting two sets of crossing lines with parameters <math>\theta</math> and <math>\phi</math>) and mirrors (illustrated as rectangles connecting two sets of crossing lines with parameter <math>R(\theta)</math>).

{| class="wikitable"
| [[File:Hadamar Linar optics Circuit.jpg|thumb|none|Implementation of a Hadamard gate with a beam splitter and a mirror. [[Quantum circuit]] is on the top part.]]
| [[File:X circuit in LOQC.gif|thumb|none|Implementation of a Pauli-X gate (NOT gate) with a beam splitter. [[Quantum circuit]] is on the top part.]]
|}

In the above figures, a qubit is encoded using two mode channels (horizontal lines): <math>\left\vert0\right\rangle</math> represents a [[photon]] in the top mode, and <math>\left\vert1\right\rangle</math> represents a photon in the bottom mode.

== Integrated photonic circuits for LOQC ==

In reality, assembling a whole bunch (possibly on the order of <math>10^4</math><ref name="Hayes2004">{{Cite journal
| doi = 10.1088/1464-4266/6/12/008
| issn = 1464-4266
| volume = 6
| issue = 12
| pages = 533–541
| last = Hayes
| first = A. J. F.
| first2 = A.
| last2 = Gilchrist
| first3 = C. R.
| last3 = Myers
| first4 = T. C.
| last4 = Ralph
| title = Utilizing encoding in scalable linear optics quantum computing
| journal = Journal of Optics B: Quantum and Semiclassical Optics
| accessdate = 2014-01-26
| date = 2004-12-01
| url = http://iopscience.iop.org/1464-4266/6/12/008
| publisher= IOP Publishing
|arxiv = quant-ph/0408098 |bibcode = 2004JOptB...6..533H }}</ref>) of beam splitters and phase shifters in an optical experimental table is challenging and unrealistic. To make LOQC functional, useful and compact, one solution is to miniaturize all linear optical elements, photon sources and photon detectors, and to integrate them onto a chip. If using a [[semiconductor]] platform, single photon sources and photon detectors can be easily integrated. To separate modes, there have been integrated [[arrayed waveguide grating]] (AWG) which are commonly used as optical (de)multiplexers in [[wavelength division multiplexing|wavelength division multiplexed]] (WDM). In principle, beam splitters and other linear optical elements can also be miniaturized or replaced by equivalent [[nanophotonics]] elements. Some progress in these endeavors can be found in the literature, for example, Refs.<ref>{{cite journal | last1 = Gevaux | first1 = D | year = 2008 | title = Optical quantum circuits: To the quantum level | url = | journal = Nature Photonics | volume = 2 | issue = | pages = 337–337 | doi=10.1038/nphoton.2008.92|bibcode = 2008NaPho...2..337G }}</ref><ref>{{cite journal | last1 = Politi | first1 = A. | last2 = Cryan | first2 = M. J. | last3 = Rarity | first3 = J. G. | last4 = Yu | first4 = S. | last5 = O'Brien | first5 = J. L. | year = 2008 | title = Silica-on-silicon waveguide quantum circuits | url = | journal = Science | volume = 320 | issue = | pages = 646–649 | doi=10.1126/science.1155441 | pmid=18369104|arxiv = 0802.0136 |bibcode = 2008Sci...320..646P }}</ref><ref>{{cite journal | last1 = Thompson | first1 = M. G. | last2 = Politi | first2 = A. | last3 = Matthews | first3 = J. C. | last4 = O'Brien | first4 = J. L. | year = 2011 | title = Integrated waveguide circuits for optical quantum computing | url = | journal = IET circuits, devices & systems | volume = 5 | issue = | pages = 94–102 | doi=10.1049/iet-cds.2010.0108}}</ref> In 2013, the first integrated photonic circuit for quantum information processing has been demonstrated using photonic crystal waveguide to realize the interaction between guided field and atoms.<ref>{{cite arXiv|eprint=1312.3446|last1=Goban|first1=A.|last2=Hung|first2=C. -L.|last3=Yu|first3=S. -P.|last4=Hood|first4=J. D.|last5=Muniz|first5=J. A.|last6=Lee|first6=J. H.|last7=Martin|first7=M. J.|last8=McClung|first8=A. C.|last9=Choi|first9=K. S.|title=Atom-Light Interactions in Photonic Crystals|class=physics.optics|year=2013|last10=Chang|first10=D. E.|last11=Painter|first11=O.|last12=Kimble|first12=H. J.}}</ref>
<!--complexity analysis to be added-->
<!--More content is welcome to be added on entanglement generation/purification/distillation and realizing practical functions using present technologies.-->

== Implementations comparison ==

=== Comparison of the KLM protocol and the boson sampling model ===

The advantage of the KLM protocol over the boson sampling model is that while the KLM protocol is a universal model, boson sampling is not believed to be universal. On the other hand, it seems that the scalability issues in boson sampling are more manageable than those in the KLM protocol.

In boson sampling only a single measurement is allowed, a measurement of all the modes at the end of the computation. The only scalability problem in this model arises from the requirement that all the photons arrive at the photon detectors within a short-enough time interval and with close-enough frequencies.<ref name="Aaronson13" />

In the KLM protocol, there are non-deterministic quantum gates, which are essential for the model to be universal. These rely on gate teleportation, where multiple probabilistic gates are prepared offline and additional measurements are performed mid-circuit. Those two factors are the cause for additional scalability problems in the KLM protocol.

In the KLM protocol the desired initial state is one in which each of the photons is in one of two modes, and the possibility that a mode is occupied by more than one photon is zero. In boson sampling, however, the desired initial state is specific, requiring that the first <math>N</math> modes are each occupied by a single photon<ref name="Aaronson13" /> (<math>N</math> is the number of photons and <math>M \ge N</math> is the number of modes) and all the other states are empty.
<!--If there is a brief summary about advantages and disadvantages compared to other implementation methods, that would be great! -->

=== Earlier models ===

Another, earlier model which relies on the representation of several qubits by a single photon is based on the work of C. Adami and N. J. Cerf.<ref name="Adami1999" /> By using both the location and the polarization of photons, a single photon in this model can represent several qubits; however, as a result, [[Quantum gate#Controlled gates|CNOT-gate]] can only be implemented between the two qubits represented by the same photon.

The figures below are examples of making an equivalent [[quantum gate#Hadamard gate|Hadamard-gate]] and [[quantum gate#Controlled gates|CNOT-gate]] using beam splitters (illustrated as rectangles connecting two sets of crossing lines with parameters <math>\theta</math> and <math>\phi</math>) and phase shifters (illustrated as rectangles on a line with parameter <math>\phi</math>).

{| class="wikitable"
| [[File:Linear optics H gate.svg|thumb|none|Implementation of Hadamard-gate on a "location" qubit with a beam splitter and phase shifters. [[Quantum circuit]] is on the top part.]]
| [[File:Linear optics CNOT gate.svg|thumb|none|Implementation of Controlled-NOT-gate with a beam splitter. [[Quantum circuit]] is on the top part.]]
|}

In the optical realization of the CNOT gate, the polarization and location are the control and target qubit, respectively.

== References ==
{{Reflist|40em}}

== External links==
*{{cite web|url=http://www.kurzweilai.net/optical-chip-allows-for-reprogramming-quantum-computer-in-seconds|title=Optical chip allows for reprogramming quantum computer in seconds|date=August 14, 2015|publisher=kurzweilai.net}}

{{Quantum computing}}

[[Category:Quantum information science]]
[[Category:Quantum optics]]
[[Category:Quantum gates]]