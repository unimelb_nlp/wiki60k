{{Infobox software
| name                   = News360
| title                  = News360
| logo                   = [[Image:News360 logo.png|frameless|100px]]
| logo caption           = 
| screenshot             = News 360 screenshot.png
| screenshot_size        = 250px
| caption                = 
| collapsible            = 
| author                 = 
| developer              = News360 Ltd.
| released               = 2010<!-- {{Start date|YYYY|MM|DD|df=yes/no}} -->
| discontinued           = 
| latest release version = 
| latest release date    = <!-- {{Start date and age|YYYY|MM|DD|df=yes/no}} -->
| latest preview version = 
| latest preview date    = <!-- {{Start date and age|YYYY|MM|DD|df=yes/no}} -->
| frequently updated     = <!-- DO NOT include this parameter unless you know what it does -->
| programming language   = 
| operating system       = [[iOS]], [[Android (operating system)|Android]], [[Windows 8]], [[Windows Phone]]
| platform               = [[iPad]], [[iPhone]], [[iPod Touch]]<br>[[List of Android devices|Android devices]], [[Windows 8]], [[Windows Phone]]
| size                   = 
| language               = 
| status                 = 
| genre                  = [[News Aggregator]], [[Personalization]]
| license                = 
| alexa                  = 
| website                = [http://news360.com news360.com]
}}

'''News360''' is a [[Personalization|personalized]] [[News Aggregator|news aggregation]] app for [[smartphone]]s, [[tablet computer|tablets]] and the [[web application|web]]. It attempts to learn a user's interests by analyzing their behavior and activity on social media and using [[Semantic analysis (linguistics)|semantic analysis]] and [[natural language processing]] to create an [[Interest Graph]] and construct a unique feed of relevant content for each user.<ref name="NLP">{{cite web|title=News360 is Changing the Content Delivery Game
|url=http://scobleizer.com/2011/09/01/news360-is-changing-the-content-delivery-game/|work=Scobleizer|date=1 Sep 2011|accessdate=19 Apr 2013}}</ref> The app has an audience of more than 4M<ref name="Audience">{{cite web|title=News360 Mobilizes Native Advertising With Personalized Content|url=http://www.mediapost.com/publications/article/205951/news360-mobilizes-native-advertising-with-personal.html|work=Mediapost|date=5 Aug 2013|accessdate=12 Feb 2014}}</ref> users and is located in [[San Francisco, California|San Francisco, CA]], with offices in [[Vancouver|Vancouver, BC]] and [[Moscow|Moscow, Russia]].<ref name="CB">{{cite web|title=News360 Crunchbase Profile
|url=http://www.crunchbase.com/company/news360|work=Crunchbase|accessdate=19 Apr 2013}}</ref>

Initially released for iPhone in late 2010, News360 is now available for all major mobile platforms, with versions for the [[iPad]],<ref name="iPad">{{cite web|title=News360 Revamps Its iPad App: New Look, New Home Screen, New Reading Scores|url=http://techcrunch.com/2012/07/10/news360-ipad/|work=TechCrunch|date=10 Jul 2012|accessdate=19 Apr 2013}}</ref> [[iPhone]],<ref name="iPhone">{{cite web|title=Thanks to an update, the News360 iPhone app is now as sleek as it is smart|url=http://www.digitaltrends.com/mobile/thanks-to-an-update-the-news360-iphone-app-is-now-as-sleek-as-it-is-smart/|work=Digital Trends|date=29 Nov 2012|accessdate=19 Apr 2013}}</ref> [[Android (operating system)|Android]],<ref name="Android">{{cite web|title=News360′s Roman Karachinsky Shows Off Revamped Android App, Hints At Plans Beyond News|url=http://techcrunch.com/2013/03/03/news360-android-ipad-demo/|work=TechCrunch|date=3 Mar 2013|accessdate=19 Apr 2013}}</ref> [[Windows 8]],<ref name="Win8">{{cite web|title=Interview: We chat with the CEO of News360 for Windows 8|url=http://www.neowin.net/news/interview-we-chat-with-the-ceo-of-news360-for-windows-8|work=Neowin|date=4 Nov 2012|accessdate=19 Apr 2013}}</ref> [[Windows Phone]]<ref name="WinPhone">{{cite web|title=News360 Is the Wikipedia of News Apps|url=http://www.windowsphonedaily.com/2011/05/news360-is-wikipedia-of-news-apps.html|work=Windows Phone Daily|date=27 May 2011|accessdate=19 Apr 2013}}</ref> and the web.<ref name="Web">{{cite web|title=NEWS360 2.0 BRINGS WEB APP, UPDATES FOR IPAD AND HONEYCOMB|url=http://bgr.com/2011/08/10/news360-2-0-brings-web-app-updates-for-ipad-and-honeycomb/|work=BGR|date=10 Aug 2011|accessdate=19 Apr 2013}}</ref>

==History==

News360 was founded in July, 2010. In October 2010, the first News360 Windows Phone 7 app was released. In November 2010, the first News360 iPhone app was released,<ref name="1dot0">{{cite web|title=News360 delivers smarter news to your iPhone|url=http://venturebeat.com/2010/12/20/news360-funding-version-2-0/|work=VentureBeat|date=20 Dec 2010|accessdate=21 Mar 2013}}</ref> followed by an iPad version in March 2011.<ref name="VB Demo">{{cite web|title=DEMO: News360 takes personalized news feeds mobile|url=http://venturebeat.com/2011/03/01/demo-news360-takes-personalized-news-feeds-mobile/|work=VentureBeat|date=1 Mar 2011|accessdate=21 Mar 2013}}</ref> In May, 2011 the company released News360 for Android tablets,<ref name="Android Tablets">{{cite web|title=News360 now available for Honeycomb tablets|url=http://www.androidcentral.com/news360-now-available-honeycomb-tablets|work=Android Central|date=24 May 2011|accessdate=21 Mar 2013}}</ref> and in July - for Android phones.<ref name="Android Phones">{{cite web|title=News360 for phones now available|url=http://www.androidcentral.com/news360-phones-now-available|work=Android Central|date=7 Jul 2011|accessdate=21 Mar 2013}}</ref> News360 2.0 was released in November, 2011,<ref name=News36020>{{cite web|title=Personalized News Reader News360 2.0 Arrives On iPhone|url=http://techcrunch.com/2011/11/01/personalized-news-reader-news360-arrives-on-iphone/|work=TechCrunch|date=1 Nov 2011|accessdate=21 Mar 2013}}</ref> adding an ability to create custom feeds. The next version, News360 3.0, was launched in July, 2012,<ref name=News36030>{{cite web|title=Improved News360 iPad App Learns Your News Habits, Wisely Consolidates|url=http://abcnews.go.com/blogs/technology/2012/07/improved-news360-ipad-app-learns-your-news-habits-wisely-consolidates/|work=ABC News|date=10 July 2012|accessdate=21 Mar 2013}}</ref> introducing the News360 personalization engine and the "Home" feed, which merges all of the user's interests, custom feeds and sources into a single feed that uses behavioral analysis to get better at selecting the most relevant content the more the user uses the app. The release was generally well-received, with a [[New York Times]] reviewer saying "For a great news experience, the free app News360 has to be one of the better news-aggregating [apps] I’ve seen on any platform".<ref name="NYTimes">{{cite news|title=Showing Off a Tablet's Graphical Powers, Android Style|url=https://www.nytimes.com/2012/11/15/technology/personaltech/apps-that-dart-and-delight-on-tablets-piloted-by-android.html?_r=0|work=New York Times|date=14 Nov 2012|accessdate=21 Mar 2013}}</ref> In September, 2012 News360 announced the Publisher Partnership Program together with 30 brands, including the [[Chicago Tribune]], [[CNBC]], [[Fox Sports]], [[Business Insider]], [[Gigaom]] and others.<ref name="Publisher Program">{{cite web|title=News360 Offers Publishers a Branded Presence|url=http://blogs.wsj.com/tech-europe/2012/09/21/news360-offers-publishers-a-branded-presence/|work=Wall Street Journal|date=21 Sep 2012|accessdate=21 Mar 2013}}</ref>

==Technology==

News360 uses a semantic analysis engine, developed in-house by the company, to analyze content aggregated from over 100,000 sources around the web. After performing [[named-entity recognition]], [[Classification in machine learning|classification]], and [[cluster analysis]], News360 uses a complex formula to rank stories, taking in the influence of the source, author, text quality and complexity, size and velocity of the story cluster and many other metrics.<ref name="N360tech">{{cite web|title=The Long Quest for Personalization|url=http://blog.news360.com/2011/04/the-long-quest-for-personalization-part-i/|work=News360 blog|date=5 Apr 2011|accessdate=21 Mar 2013}}</ref> The system then takes the clustered and ranked set of stories and identifies a personalized selection for each user, based on their [[interest graph]].<ref name="Interest graph">{{cite web|title=NEWS360 3.0 NOW AVAILABLE FOR IPHONE AND ANDROID SMARTPHONES [UPDATED]|url=http://bgr.com/2011/11/01/news360-2-0-now-available-for-iphone-and-android-smartphones-video/|work=BGR|date=1 Nov 2011|accessdate=21 Mar 2013}}</ref> The Interest Graph is initially constructed by collecting, with permission, user data from [[Facebook]], [[Twitter]], [[Google+]], [[Google Reader]] or [[Evernote]].<ref name="WSJ Interest graph">{{cite web|title=news360 Aims to Make News Social|url=http://blogs.wsj.com/tech-europe/2011/08/11/news360-aims-to-make-news-social/|work=Wall Street Journal TechEurope|date=11 Aug 2011|accessdate=19 Apr 2013}}</ref> As the user uses News360, additional data is collected from their behavior - as they read stories and give feedback, the interest graph becomes more and more accurate and attuned to their real interests, making the personalization more effective.<ref name="Android" />

==See also==
*[[Flipboard]]
*[[Pulse (software)|Pulse]]

==References==
{{Reflist|30em}}

==External links==
* [http://news360.com news360.com], Official website
* {{Twitter}}

{{Aggregators|state=collapsed}}

[[Category:English-language media]]
[[Category:News aggregators]]
[[Category:IOS software]]
[[Category:IPad]]
[[Category:IPod software]]
[[Category:Mobile social software]]
[[Category:Products introduced in 2010]]
[[Category:Android (operating system) software]]