{{multiple issues|
{{notability|Products|date=December 2010}}
{{refimprove|date=November 2013}}
}}

{{Infobox software
|name = SlimDX
|logo = Slimdx-logo.svg
|screenshot = 
|caption = 
|developer = SlimDX Group
|latest_release_version = January 2012
|programming_language = 
|operating_system = [[Microsoft Windows]]
|genre = 
|license = [[MIT license]]
|website = {{URL|slimdx.org}}
}}

'''SlimDX''' is an [[open-source]] [[API]] to [[DirectX]] programming under [[.NET Framework]].  SlimDX can be used from any [[Computer language|language]] under the .NET runtime (due to the [[Common Language Runtime|CLR]]).

SlimDX can be used to develop [[multimedia]] and interactive [[Application software|applications]] (e.g. [[Computer games|games]]). Enabling high performance graphical representation and enabling the programmer to make use of modern graphical hardware while working inside the .NET framework.

SlimDX was first publicly revealed in [[Software release life cycle#Beta|beta]] form on June 30, 2007. Since then it has grown at a rapid pace and now covers most multimedia APIs provided by Microsoft. As of the June 2008 release, it is no longer in beta and is considered [[Software release life cycle#Stable or unstable|stable]]. Several commercial projects and companies are using SlimDX, including [[Star Wars: The Force Unleashed]] and [[Operation Flashpoint: Dragon Rising]].<ref>{{cite web|title=SlimDX Homepage|url=http://slimdx.org/|accessdate=27 November 2012}}</ref><ref>{{cite web|last=Promit|first=Roy|title=Software and Projects Using SlimDX|url=http://ventspace.wordpress.com/2010/06/24/software-and-projects-using-slimdx/|work=Promit's Ventspace (lead developer on SlimDX)|accessdate=27 November 2012}}</ref>

For the March 2011 release, SlimDX fully supports the following APIs:
* [[Direct3D]] 9.0c, 9.0 Ex, 10.0, 10.1, 11
* [[DXGI]] 1.0, 1.1
* [[DirectInput]], [[XInput]], Raw Input
* [[DirectSound]], [[XAudio 2]], X3DAudio, XAPO, XACT3
* [[Direct2D]], [[DirectWrite]]

==See also==
{{Portal|Free software}}
* [[Microsoft XNA]]
* [[Managed DirectX]]
* [[SharpDX]]

==External links==
* [http://slimdx.org SlimDX]
* Developer blogs: [http://ventspace.wordpress.com/ Promit Roy], [http://www.gamedev.net/community/forums/mod/journal/journal.asp?userid=62708 Mike Popoloski], [http://scientificninja.com/ Josh Petrie]

{{Reflist}}

{{DEFAULTSORT:Slimdx}}
[[Category:Video game development software]]
[[Category:DirectX]]
[[Category:Software using the MIT license]]
[[Category:C Sharp libraries]]

{{computer-stub}}