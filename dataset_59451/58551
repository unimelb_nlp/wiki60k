{{Infobox journal
| title = Journal of the American Pharmacists Association
| cover =
| former_name = Journal of the American Pharmaceutical Association
| abbreviation = J. Am. Pharm. Assoc.
| discipline = [[Pharmacy]]
| editor = Andy Stergachis
| publisher = [[American Pharmacists Association]]
| country = United States
| history = 1961-present
| frequency = Bimonthly
| openaccess = 
| license = 
| impact = 1.238
| impact-year =2014
| ISSN = 1544-3191
| eISSN = 1544-3450
| CODEN =
| JSTOR = 
| LCCN = 2003212218
| OCLC = 715062221
| website = http://japha.org/index.aspx
| link1 = http://japha.org/issue.aspx?journalid=63
| link1-name = Online access
| link2 = http://japha.org/issues.aspx?journalid=63
| link2-name = Online archive
}}
The '''''Journal of the American Pharmacists Association''''' is a bimonthly [[peer-reviewed]] [[medical journal]] covering [[pharmacy]]-related topics. It was established in 1961 as the ''Journal of the American Pharmaceutical Association'', obtaining its current title in 2003, and is the official journal of the [[American Pharmacists Association]]. The [[editor-in-chief]] is Andy Stergachis ([[University of Washington]]).

==Abstracting and indexing==
The journal is abstracted and indexed in:
* [[Biological Abstracts]]
* [[Chemical Abstracts]] 
* [[Excerpta Medica]]
* [[FDA Clinical Experience Abstracts]]
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]
* [[Science Citation Index Expanded]]
* [[Current Contents]]/Clinical Medicine
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.238.<ref name=WoS>{{cite book |year=2015 |chapter=Journal of the American Pharmacists Association |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

==References==
{{Reflist}}

== External links ==
* {{Official website|http://japha.org/index.aspx}}

[[Category:Publications established in 1961]]
[[Category:Pharmacology journals]]
[[Category:Bimonthly journals]]
[[Category:Academic journals published by learned and professional societies of the United States]]
[[Category:English-language journals]]