{{Infobox journal
| cover         = 
| editors       =Priscilla Wald, Matthew A. Taylor 
| discipline    =American literature, literary history, [[literary criticism]] and bibliography 
| peer-reviewed = 
| former_names  = 
| abbreviation  =Am. lit and Am. lit.  
| publisher     =Duke University Press and the Modern Literature Association 
| country       =United States 
| frequency     =Quarterly 
| history       =1929–present 
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       =http://americanliterature.dukejournals.org/ 
| link2         =http://muse.jhu.edu/journals/al/ 
| link2-name    =Project Muse (archives) 
| link1         =http://americanliterature.dukejournals.org/current.dtl 
| link1-name    =Current issue 
| JSTOR         =00029831
| OCLC          =1480320 
| LCCN          =30020216 
| CODEN         = 
| ISSN          =0002-9831 
| eISSN         =1527-2117 
| boxwidth      = 
}}

'''''American Literature''''' is a [[Literary magazine|literary journal]] published by [[Duke University Press]]. It is sponsored by the American Literature Section of the [[Modern Language Association]] (of America), known as the MLA. The current editors are Priscilla Wald and Matthew A. Taylor. The first volume of this journal was published in March 1929.<ref name=AmLitSec>
{{Cite web
  | title =American Literature Section General Information 
  | quote =The American Literature Section of the MLA is a community of scholars, teachers, and students of American literature and culture. The Section sponsors the journal American Literature and the annual volume American Literary Scholarship, both of which are published by Duke University Press 
  | publisher =Modern Language Association of America (MLA) 
  | date = 
  | url =http://als-mla.org/AboutALS-MLA.htm 
  | format =Online 
  | accessdate =2011-02-16}}</ref><ref name=details/>

Coverage includes the contributions and works of American authors. Temporal coverage is from the colonial period until present day. Publishing formats also include book review, and relevant announcements (conferences, grants, and publishing opportunities). A citations index is also part of this journal. The index is for future issues and reprints, collections, anthologies, and professional books.<ref name=details>
{{Cite web
  | title =Details 
  | publisher =[[Duke University Press]] 
  | date = 
  | url =http://www.dukeupress.edu/Catalog/ViewProduct.php?viewby=journal&productid=45633
  | format =Online 
  | accessdate =2011-02-16}}</ref>

==Abstracting and indexing==
This journal is Indexed/abstracted in the following databases:<ref name=thomReu>
{{Cite web
  | title =Master Journal List
  | publisher =Thomson Reuters  
  | date = 
  | url =http://science.thomsonreuters.com/cgi-bin/jrnlst/jlresults.cgi?PC=MASTER&ISSN=%200002-9831
  | format =Online 
  | accessdate =2011-02-17}}
*Quarterly - Arts & Humanities Citation Index and Current Contents / Arts & Humanities</ref><ref name=wellesley>
{{Cite web
  | title =catalog record 
  | publisher =Wellesley Library and Technology  
  | date = 
  | url =http://www.worldcat.org/wcpa/oclc/1480320?page=frame&url=http%3A%2F%2Fluna.wellesley.edu%2Fsearch%2Fo1480320%26checksum%3Defefa420f1174ef2fe14bb25f2c7e311&title=Wellesley+College&linktype=opac&detail=WEL%3AWellesley+College%3AAcademic+Library 
  | format =Online 
  | accessdate =2011-02-17}} [[Wellesley College]]. Branch Location - Clapp Library. 
*Full title of this journal listed as "''American literature; a journal of literary history, criticism and bibliography''"
*Alternate authors (Noted editors): [[Jay Hubbell|Jay Broadus Hubbell]] (Jay B. or J.B.), 1885-1979.   [[Clarence Gohdes]] 1901-1997</ref><ref name=NABx>
{{Cite web
  | title =catalogue record 
  | publisher =National Library of Australia  
  | date = 
  | url =http://catalogue.nla.gov.au/Record/60297?lookfor=isn:0002-9831%20%23
  | format =Online 
  | accessdate =2011-02-17}} 
*Full title listed as "''American literature : a journal of literary history, criticism and bibliography''".
*Noted editors (also listed in this catalogue): March 1929 - May 1954, [[J. B. Hubbell]] and others; November 1954 - [[C. Gohdes]] and others.</ref>
:[[Thomson Reuters]]:
::[[Arts & Humanities Citation Index]]
::[[Current Contents]] / Arts & Humanities
:Academic Abstracts Fulltext Elite & Ultra
:Academic Research Library,
:[[Academic Search]] Elite & Premier
:America: History and Life
:Book Review Digest
:Discovery
:[[Expanded Academic ASAP]]
::Student Resource Center College
:[[General Reference Center Gold]] & International
:Historical Abstracts
:Humanities and Social Sciences Index Retrospective, 1907–1984,
::Humanities Abstracts,
::Humanities Full Text,
::Humanities Index,
::Humanities Index Retrospective, 1907–1984,
::Humanities International Complete,
::Humanities International Index,
::Social Sciences Index Retrospective, 1907–1984
:[[International Bibliography of Periodical Literature]] (IBZ),
:Literary Reference Center,
:Literature Online
:Magazines for Libraries
:MasterFILE Elite & Premier & Select
:[[MLA Bibliography]],
:OmniFile Full Text - Mega Edition
:Professional Development Collection (EBSCO)
:Research Library

==References==
<references/>

==External links==
*[http://americanliterature.dukejournals.org/ Official website]
*[http://www.mla.org/homepage Modern Language Association]
*[http://americanliterature.dukejournals.org/reports/mfc1.dtl The most frequenctly cited articles] of ''American Literature''.

[[Category:English-language journals]]
[[Category:Publications established in 1929]]
[[Category:American literary magazines]]
[[Category:American literature]]
[[Category:American literature by medium]]
[[Category:Duke University Press academic journals]]
[[Category:Quarterly journals]]