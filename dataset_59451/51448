{{Infobox journal
| title = Health Informatics Journal
| cover = [[File:Health Informatics Journal.jpg]]
| editor = Rob Procter
| discipline = [[Health informatics]]
| former_names =
| abbreviation = Health Inform. J.
| publisher = [[Sage Publications]]
| country =
| frequency = Quarterly
| history = 1992-present
| openaccess =
| license =
| impact = 0.787 (2013)<br>
 1.578 (2016)
| impact-year =
| website = http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal201654
| link1 = http://jhi.sagepub.com/content/current
| link1-name = Online access
| link2 = http://jhi.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR =
| OCLC = 49996259
| LCCN = sn99039672
| CODEN =
| ISSN = 1460-4582
| eISSN = 1741-2811
}}
The '''''Health Informatics Journal''''' is a quarterly [[Peer review|peer-reviewed]] [[medical journal]] that covers the field of [[health informatics]]. its [[editors-in-chief]] are Rob Procter ([[University of Edinburgh]]) and P. A. Bath ([[University of Sheffield]]). It was established in 1992 and is published by [[SAGE Publications]]. Chris Dowd [[University of Sheffield]] was editor from launch to 1997 and [[Michael F Smith|M F Smith]] from 1997 to 2001.

== Abstracting and indexing ==
The journal is abstracted and indexed in:
* [[Science Citation Index Expanded]]
* [[Scopus]]
* [[Academic Search|Academic Premier]]
* [[Educational Research Abstracts Online]]
* [[Health & Safety Science Abstracts]]
* [[MEDLINE]]
According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 0.787,<ref name=WoS>{{cite book |year=2014 |chapter=Health Informatics Journal |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}} {{subscription required}}</ref> ranking it 19 out of 25 journals in the category "Medical Informatics"<ref name="WoS1">{{cite book |year=2014 |chapter=Journals Ranked by Impact: Medical Informatics |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |work=[[Web of Science]] |postscript=.}}</ref> and 75 out of 85 journals in the category "Health Care Sciences & Services".<ref name="WoS2">{{cite book |year=2014 |chapter=Journals Ranked by Impact: Health Care Sciences & Services |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]]|work=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://jhi.sagepub.com/}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Biomedical informatics journals]]
[[Category:Articles created via the Article Wizard]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1992]]