{{EngvarB|date=October 2013}}
{{Use dmy dates|date=October 2013}}
<!--PLEASE DO NOT ADD AN INFO BOX TO THIS PAGE.-->
[[Image:Harriet Arbuthnot.jpg|thumb|right|300px|Harriet Arbuthnot by [[John Hoppner]]<ref>This portrait by [[John Hoppner]] is now in the Fundación Lázaro Galdiano, [[Madrid]].</ref>]]

'''Harriet Arbuthnot''' (10 September 1793 – 2 August 1834) was an early 19th-century English diarist, social observer and political hostess on behalf of the [[Tory]] party.  During the 1820s she was the "''closest woman friend''" of the hero of [[Battle of Waterloo|Waterloo]] and [[United Kingdom of Great Britain and Ireland|British]] [[Prime Minister of the United Kingdom|Prime Minister]], the [[Arthur Wellesley, 1st Duke of Wellington|1st Duke of Wellington]].<ref name="Longford195">Longford, p. 195.</ref> She maintained a long correspondence and association with the Duke, all of which she recorded in her diaries, which are consequently extensively used in all authoritative biographies of the Duke of Wellington.

Born into the periphery of the [[British Honours System#Hereditary peerage|British aristocracy]] and married to a politician and member of the establishment, she was perfectly placed to meet all the key figures of the [[English Regency|Regency]] and late [[Napoleonic era|Napoleonic]] eras. Recording meetings and conversations often verbatim, she has today become the "''Mrs. Arbuthnot''" quoted in many biographies and histories of the era. Her observations and memories of life within the British establishment are not confined to individuals but document politics, great events and daily life with an equal attention to detail, providing historians with a clear picture of the events described. Her diaries were themselves finally published in 1950 as ''The Journal of Mrs Arbuthnot''.<ref>{{cite book|author=Arbuthnot, Harriet|title=The Journal of Mrs. Arbuthnot, 1820–1832|year=1950}}
</ref>

==Early life==
[[Image:Charles Blair.jpg|thumb|upright|left|[[Henry Fane, MP|Hon. Henry Fane]] MP (1739–1802), Harriet Arbuthnot's father]]

Harriet Arbuthnot was born Harriet Fane, the daughter of the [[Henry Fane, MP|Hon. Henry Fane]], second son of [[Thomas Fane, 8th Earl of Westmorland|Thomas Fane]], 8th [[Earl of Westmorland]]. As a young man, Henry Fane had been described as "very idle and careless and spending much time in the country".<ref name="Lincolnshire19">Lincolnshire archives, p. 19.</ref> However, he found time to be the Member of Parliament for [[Lyme Regis (UK Parliament constituency)|Lyme]] and in 1772 was appointed Keeper of the King's Private Roads.<ref name="Lincolnshire19"/> In 1778, he married Arbuthnot's mother, Anne Batson, an heiress, the daughter of Edward Buckley Batson. The couple had 14 children: nine sons and five daughters.<ref name="Lincolnshire19"/>

The young Harriet spent much of her childhood at the family home at [[Fulbeck|Fulbeck Hall]] in Lincolnshire, sited high on the [[limestone]] hills above [[Grantham]]. The house, which had been given to Henry Fane by his father, was a not over-large modern mansion at the time of Arbuthnot's childhood. It was rebuilt following a fire in 1733, and further extended and modernised in 1784 by Henry Fane.<ref>Fulbeck Hall.</ref> At Fulbeck Harriet and her 13 siblings enjoyed a comfortable and reasonably affluent rural childhood.

Harriet Fane's father died when she was nine years old, but the family fortunes improved considerably in 1810 when her mother inherited the [[Avon Tyrrell]] estate in [[Hampshire]] and the Upwood Estate in [[Dorset]].<ref name="Lincolnshire19"/> This yielded the widowed Mrs Fane an income of [[pound sterling|£]]6,000 per annum<ref name="Lincolnshire20">Lincolnshire archives, p. 20.</ref> (£{{formatnum:{{inflation|UK|6000|1810|r=-4}}}} per year as of {{CURRENTYEAR}}), a large income by the standards of the day.  With 14 children and a position in society to maintain, however, the money was fully utilised.

==Marriage==
Harriet Fane married Rt Hon [[Charles Arbuthnot]], member of [[Parliament of the United Kingdom|Parliament]], at Fulbeck on 31 January 1814.  Born in 1767, her husband was 26 years older than she was, an age difference which had initially caused her family to object to the marriage.<ref name="Mullen">Mullen</ref> Another of the principal obstacles to finalising the arrangements for the marriage was financial. Her widowed mother delegated the arrangements for the marriage of her 20-year-old daughter to her elder son Vere, a 46-year-old widower who was considered qualified in these matters as he worked at [[Child's Bank]].  It seems that Vere Fane and his mother were not initially prepared to [[Settlement (trust)|settle]] enough money on his sister to satisfy her future husband, causing the prospective bridegroom to write to his fiancée: "How can you and I live upon £1000 or £1200 and Fane [her mother] finds it so impossible to live upon her £6000 that she can offer you no assistance whatsoever?"<ref name="Lincolnshire20"/>

[[Image:Lord Castlereagh.jpg|thumb|upright|[[Robert Stewart, Viscount Castlereagh|Lord Castlereagh]], Harriet Arbuthnot's "dearest and best friend."]]

Charles Arbuthnot was a widower with four children; his son [[Charles George James Arbuthnot|Charles]] was a mere nine years junior to his new wife. His first wife Marcia, a [[lady in waiting]] to the notorious [[Caroline of Brunswick|Princess of Wales]], had died in 1806. Like the other two men his second wife so admired, [[Robert Stewart, Viscount Castlereagh|Viscount Castlereagh]] and Wellington, Charles Arbuthnot was a member of the [[Anglo-Irish]] [[aristocracy]].  He had been a member of parliament since 1795, when he became the member for [[East Looe (UK Parliament constituency)|East Looe]]. At the time of his marriage to Fane, he was the member for [[St Germans (UK Parliament constituency)|St Germans]].  He had briefly interrupted his political career to become [[Ambassador Extraordinary]] to the [[Ottoman Empire]] between 1804 and 1807.<ref>Hobhouse, note 177</ref> Marriage to such a pillar of the establishment as Charles Arbuthnot opened all doors to his young new wife, who, as one of the 14 children of a younger son of an aristocratic family possessed of no great fortune, would otherwise have been on the periphery of the highest society. However, as the debate and wrangling over her [[dowry]] proved, money was tight.

Throughout her marriage, Mrs Arbuthnot, the former Harriet Fane, formed close friendships with powerful older men.  She described Castlereagh as her "dearest and best friend"<ref name="Arbuthnot">Arbuthnot.</ref> until his death in 1822, when she transferred her affections to the other great 19th-century Anglo-Irish peer, the Duke of Wellington.<ref name="Aspinall">Aspinall.</ref> All social commentators of the time, however, agree that her marriage was happy; indeed, her husband was as close a friend of Wellington's as was his wife. Married to a politician, she was fascinated by politics and enjoyed success as a political hostess while exerting her energies to promote [[Tory]] causes.  However, while she was the dominant partner,<ref name="Longford441">Longford, p. 441.</ref> her conservative outlook<ref name="Longford441"/> ensured her continued favour among her elderly Tory admirers.  During the early part of her marriage, her husband served as an [[Secretary to the Treasury|Under-Secretary at the Treasury]]. Later, in 1823, he was given the [[First Commissioner of Woods and Forests|Department of Woods and Forests]],<ref name="Longford441"/> a position which gave him charge of the [[Royal Parks|Royal parks and gardens]]. The subsequent access to the [[British Royal family|Royal family]] this allowed increased not only his status but also that of his wife.

When remarking in her diaries on other women who shared their affections with great men of the day, Arbuthnot displayed a sharp, ironic wit. Of Wellington's one-time [[mistress (lover)|mistress]] Princess [[Dorothea Lieven]], wife to the [[Russian Empire|Imperial Russian]] ambassador to London from 1812 to 1834, she wrote "It is curious that the loves and intrigues of a femme galante should have such influence over the affairs of Europe."<ref>Charmley.</ref> Arbuthnot obviously failed to realise she was regarded by some in London society as a ''[[femme galante]]'' in a similar situation herself.

Her political observations are clearly written from her own Tory viewpoint.<ref name="Aspinall"/> However, her detailed description of the rivalry for power between the Tories and [[Liberal Party (UK)|Liberals]] which took place between 1822 and 1830 is one of the most authoritative accounts of this struggle.<ref name="Aspinall"/>

==Relationship with Wellington==
[[Image:Arthur Wellesley, 1st Duke of Wellington by Robert Home cropped.jpg|thumb|left|upright|[[Arthur Wellesley, 1st Duke of Wellington|The Duke of Wellington]]. Harriet Arbuthnot was his "closest woman friend."<ref name="Longford195"/>]]

It is likely that Arbuthnot first came to the attention of Wellington during 1814 in the re-opened [[Salon (gathering)|salon]]s of Paris following the exile of [[Napoleon I of France|Napoleon]] to [[Elba]].  Wellington had been appointed the [[List of Ambassadors from the United Kingdom to France|British Ambassador]] to the [[Tuileries|Court of the Tuileries]], and the city was crowded with English visitors anxious to travel on the continent and socialise after the [[Napoleonic Wars]].<ref name="Longford441"/>

Amongst those sampling the rounds of entertainment in this lively environment were the newly married Arbuthnots. Charles Arbuthnot was known to Wellington, as he had been a strong supporter of Wellington's younger brother [[Henry Wellesley, 1st Baron Cowley|Henry]] during his divorce,<ref>The Wellesley divorce had occurred in 1810 after Henry Wellesley had discovered his wife, Charlotte, was having an affair with [[Henry Paget, 1st Marquess of Anglesey|Lord Paget]].  Wellesley chose not to turn the customary blind eye and a huge scandal resulted.  Wellington and Paget were later reconciled and it was Paget who at [[Battle of Waterloo|Waterloo]] exclaimed to Wellington  "By God, sir, I've lost my leg!"—to which Wellington replied, "By God, sir, so you have!"</ref> and it is possible Wellington had met, or at least heard of, Mrs Arbuthnot—she was a first cousin to his favourites the Burghersh family.<ref name="Longford441"/><ref>The Burghershes were the family of John Fane, [[Baron Burghersh|Lord Burghersh]], who was married to Wellington's niece Lady [[Priscilla Wellesley-Pole]]. Burghersh later succeeded to the family [[earl]]dom to become [[John Fane, 11th Earl of Westmorland|Earl of Westmorland]].</ref> However, it was only after the death of Castlereagh in 1822 that the Wellington–Arbuthnot friendship blossomed. It is unlikely any close friendship developed before this time. Wellington, ensconced in the [[Hotel de Charost]] (recently vacated by Napoleon's sister Princess [[Pauline Bonaparte|Pauline Borghese]]) and [[fête]]d by the whole of [[Bourbon Restoration|Restoration]] Paris,<ref>Longford, pp. 435–441.</ref> had already found himself a close female companion, [[Giuseppina Grassini]].<ref>Longford, p. 440.</ref> This woman, known, due to her close friendship with Napoleon as "''La Chanteuse de l'Empereur''", scandalised Parisian society both English and French by appearing on Wellington's arm, especially after the arrival in Paris of the [[Catherine Wellesley, Duchess of Wellington|Duchess of Wellington]].<ref name="Longford441"/>

The story of a "[[ménage à trois]]" between Mrs Arbuthnot, her husband Charles, and Wellington, widely speculated upon, has been rejected by some biographers.<ref>Smith.</ref> However, it has been said that the unhappily married Duke enjoyed his relationship with Mrs Arbuthnot because he found in her company "the comfort and happiness his wife could not give him."<ref name="Arbuthnot"/> Arbuthnot was certainly the Duke's confidante in all matters, especially that of his marriage. He confided to her that he only married his wife because "they asked me to do it" and that he was "not the least in love with her."<ref>Both quotes are from Longford, p. 141.</ref> In fact, Wellington had not seen his wife for ten years before their wedding day.<ref>Longford, pp. 130–140.</ref> Following the marriage, the bride and groom found they had little if anything in common. Despite producing two sons, they led mostly separate lives until the death of the Duchess of Wellington in 1831. Harriet had a rather poor opinion of the Duchess ("she is such a fool"), although she disagreed with Wellington when he said that his wife cared nothing for his comfort: in Harriet's view the Duchess longed to make her husband happy, but had no idea how to go about the task.

As a consequence of his unsatisfactory marriage, Wellington formed relationships with other women, but it was for Arbuthnot that "he reserved his deepest affection."<ref name="Moncrieff">Moncrieff.</ref> Her husband at this time was working at The Treasury and Arbuthnot in effect became what would today be termed Wellington's [[social secretary]] during his first term of [[Prime Minister of the United Kingdom|premiership]] between January 1828 and November 1830.<ref name="Moncrieff"/> It has been suggested that the Duke of Wellington allowed her "almost unrestricted access to the secrets of the cabinet".<ref name="Aspinall"/> Whatever her knowledge and access, however, it appears she was unable to influence the Duke, but even his refusal to bring her husband into the Cabinet in January 1828 failed to shake the intimacy of the trio.<ref name="Aspinall"/>

Wellington made no attempts to conceal his friendship with Arbuthnot. An indication that their relationship was [[Platonic love|platonic]] and accepted as such in the highest echelons of society can be drawn from the [[Princess Victoria of Saxe-Coburg-Saalfeld|Duchess of Kent]] permitting Wellington to present Arbuthnot to her infant daughter, the future [[Victoria of the United Kingdom|Queen Victoria]], in 1828.  Arbuthnot noted that the young princess was "the most charming child I ever saw" and that "the Duchess of Kent is a very sensible person, who educates her (Victoria) remarkably well."<ref>Woodham-Smith, p 89.</ref> Arbuthnot's impressions of the Duchess were less than candid, and not shared by Wellington and other establishment figures.<ref>Woodham-Smith pp 92–114</ref> However, had Arbuthnot's own character not been judged respectable an audience with the infant princess would not have been permitted.

Many references in Arbuthnot's diary, however, are less respectful than those she accorded to the Duchess of Kent. Wellington and Arbuthnot often travelled together, and a visit to [[Blenheim Palace]] they shared in 1824 provoked a scathing entry in her journal concerning Wellington's fellow duke the [[George Spencer-Churchill, 5th Duke of Marlborough|5th Duke of Marlborough]], of whom she wrote: "The family of the great General is, however, gone sadly to decay, and are but a disgrace to the illustrious name of Churchill, which they have chosen this moment to resume. The present Duke is overloaded with debt, is very little better than a common swindler".<ref>Blenheim: The Grandest and Most Famous House in England</ref>

When Wellington and the Tories fell from power in November 1830, Arbuthnot lost interest in her diary, writing: "I shall write very seldom now, I dare say, in my book, for, except the Duke, none of the public men interest me."<ref name="Aspinall"/> Her account of the break-up of the Tory party is a thoroughly partisan narration, accurate as to happenings outside the Tory inner circle, but on a broader scale and not so completely political as that of [[Henry Hobhouse (MP)|Henry Hobhouse]].<ref name="Aspinall"/>

==Legacy==
Arbuthnot died suddenly of [[cholera]] at a farmhouse near the Arbuthnots' seat, [[Woodford, Northamptonshire|Woodford House]], near [[Kettering]] in Northamptonshire, in the summer of 1834.<ref>NCC Record office, has information regarding the Arbuthnot family at Woodford</ref> Immediately after her death an express message was sent to [[Apsley House]].  The messenger, however, had to divert to [[Hatfield House]] where Wellington was dining with the [[James Gascoyne-Cecil, 2nd Marquess of Salisbury|Marquess and Marchioness of Salisbury]]. After her death, it was revealed she had been on a [[civil list]] pension of £936 per annum (£{{formatnum:{{inflation|UK|936|1823|r=-3}}}} per year as of {{CURRENTYEAR}}) since January 1823.<ref>"News", ''[[The Times]]'', 6 August 1834</ref>

The exact nature of Arbuthnot's relationship with Wellington has always been a subject for conjecture. Fuel was added to the speculations when Wellington was immediately pursued by female admirers following her death. One was a Miss Jenkins who, from the moment of Arbuthnot's death, pursued him "body and soul."<ref>Longford, p 192.</ref> Another, who resurfaced from his past, was Arbuthnot's own cousin, the eccentric Lady [[Brympton d'Evercy#The Fane family|Georgiana Fane]], who constantly pestered Wellington with threats to publish intimate letters he had once sent her, and to sue him for, allegedly, reneging on a promise to marry her.<ref>It has also been claimed that Lady Georgiana in fact refused the young future [[Arthur Wellesley, 1st Duke of Wellington|Duke of Wellington]]'s marriage proposal, on the grounds she could not marry so lowly a soldier.  Another version of the same story is that Lady Georgiana's father, the [[John Fane, 10th Earl of Westmorland|10th Earl of Westmorland]], forbade the marriage of his daughter to an untitled soldier with apparently limited prospects.  Both of these stories however must be apocryphal, as Lady Georgiana never knew him before he was a "great man."  She was born in 1801.</ref> It seems most likely that in addition to assisting Wellington with his social life, Harriet's presence at his side protected him from the advances of other women. The Duke certainly kept [[Mistress (lover)|mistresses]] during the period he knew Arbuthnot, but it has never been proven that Harriet was one of them.  The tour at [[Apsley House]], the Duke's London residence, asserts that she merely served as his hostess at political dinners.<ref>This is asserted in the official tour of [[Apsley House]]; if true, it is unusual for the standards of etiquette of the period.</ref>

After her death, Charles left Woodford House and lived with his close friend Wellington.  Charles died at Apsley House in 1850, aged 83.<ref>New, pp. 384–385.</ref> During their time together the two elderly men mourned the loss of Arbuthnot and bemoaned the splits developing within the Tory party.<ref name="Mullen"/> Wellington lived on for another two years and was buried with due pomp and circumstance in [[St Paul's Cathedral]]. Harriet Arbuthnot had been buried with the Fane family at [[St Nicholas' Church, Fulbeck|St Nicholas']] parish church, [[Fulbeck]].

==See also==
*[[List of diarists]]

==Notes==
{{Reflist|colwidth=30em}}

==References==
*{{cite book
  |author=Arbuthnot, Charles
  |title=The Correspondence of Charles Arbuthnot
  |editor=A. Aspinall
  |location=London
  |publisher=Royal Historical Society
  |year=1941
  |oclc=15746373
 }}
*{{cite book
 |author=Arbuthnot, Harriet
 |editor=Francis Bamford
 |editor2=The 7th Duke of Wellington
 |editor2-link=Gerald Wellesley, 7th Duke of Wellington
 |title=The Journal of Mrs. Arbuthnot, 1820–1832
 |location=London
 |publisher=Macmillan
 |year=1950
 |oclc=2731598
}}
*Aspinall, A: Review of ''The Journal of Mrs. Arbuthnot, 1820–1832''. ''The English Historical Review, Vol. 67, No. 262 (Jan. 1952), pp.&nbsp;92–94.
*[https://web.archive.org/web/20110928225126/http://www.dcs.kcl.ac.uk/staff/mark/icsm99/blenheim.html Blenheim: The Grandest and Most Famous House in England] retrieved 15 May 2007
*Charmley, John. ''The Princess and the Politicians'' Penguin Books Ltd. ISBN 0-14-028971-2.
*[http://www.touruk.co.uk/houses/houselincs_fulb.htm Fulbeck Hall], TourUK.  Retrieved 9 May 2007.
*[http://www.hobby-o.com/constantinople.php Hobhouse, John. Diary from period in Constantinople]{{dead link|date=September 2016|bot=medic}}{{cbignore|bot=medic}} retrieved 17 May 2007
*[[Lincolnshire Archives Committee]], {{cite web|url= http://www.lincolnshire.gov.uk/upload/public/attachments/551/REPORT17.pdf |title=Archivists' Report 17, 1965–1966 }}&nbsp;{{small|(922&nbsp;KB)}}. Retrieved 9 May 2007. (53 pages)
*{{cite book
 | last = Longford
 | first = Elizabeth, Countess of
 | year = 1969
 | title = Wellington, the Years of the Sword
 | publisher = Weidenfeld and Nicolson
 | location = London
}}
*Moncrieff, Chris. [http://politics.guardian.co.uk/election2001/comment/0,,502650,00.html The pleasures and perils of life at No 10], ''[[The Guardian]]'', 7 June 2001.  Retrieved 9 May 2007.
*{{cite journal
 | last=Mullen
 | first=Richard
 | title=Review of "Wellington and the Arbuthnots: A Triangular Friendship"
 | journal = Contemporary Review
 |date=July 1995
 | url = http://www.thefreelibrary.com/Wellington+and+the+Arbuthnots%3A+A+Triangular+Friendship.-a017281275
}}
*{{cite journal
  | last=New
  | first=Chester W.
  | title=Review of "''The Correspondence of Charles Arbuthnot''" by A. Aspinall.
  | journal=The Journal of Modern History
  | volume=14
  | issue=3
  | year=1942
  | pages=384–385
  | jstor=1874548
  | doi=10.1086/236648
}}
*[https://web.archive.org/web/20090507231354/http://www3.northamptonshire.gov.uk/Community/record/familyP.htm Record Office], [[Northamptonshire County Council]].  Retrieved 9 May 2007.
*{{cite book|author=Smith, E.A.|title=Wellington and the Arbuthnots: a triangular friendship|location=UK|publisher=Alan Sutton Publishing|year=1994|isbn=0-7509-0629-4}}
*{{cite book
 | last = Woodham-Smith
 | first = Cecil
 | year = 1972
 | title = Queen Victoria, her Life and Times. Vol. I (1819–1861)
 | publisher = Hamish Hamilton Ltd.
 | location = London
}}

{{featured article}}
{{Arbuthnot family}}
{{Authority control}}

{{DEFAULTSORT:Arbuthnot, Harriet}}
[[Category:1793 births]]
[[Category:1834 deaths]]
[[Category:English diarists]]
[[Category:Women diarists]]
[[Category:Women of the Regency era]]
[[Category:Arbuthnot family|Harriet Arbuthnot]]
[[Category:Fane family|Harriet]]
[[Category:Women memoirists]]