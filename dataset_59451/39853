{{For|the airport|Sabiha Gökçen International Airport}}
{{Infobox person
|name   = Sabiha Gökçen
|image     = 180px-Sabiha gökçen.jpg
|image_size     = 180px
|caption  = 
|birth_date  ={{birth date|1913|3|22|df=y}}
|birth_place =[[Bursa, Turkey|Bursa]], [[Hüdavendigâr Vilayet]], [[Ottoman Empire]]
|known_for = First Turkish woman to fly during conflict.
|alma_mater = Uskudar American Academy.
|death_date  = {{death date and age|2001|3|22|1913|3|22|df=y}}
|death_place = [[Ankara]], [[Turkey]]
|occupation  = Aviator, author and spokesperson
|spouse      =
|parents     = Mustafa İzzet Bey and Hayriye Hanım
}}

'''Sabiha Gökçen''' ({{IPA-tr|sabiha ɡøkt͡ʃen|lang}}; 22 March 1913 – 22 March 2001)<ref>http://www.ntv.com.tr/turkiye/sabiha-gokcen-google-ana-sayfasinda,84NXBXk620mwTL0rKrwfSA</ref> was a [[Turkish people|Turkish]] aviator. She was the first Turkish female combat pilot, aged 23.<ref>{{cite web |url= http://www.ctie.monash.edu.au/hargrave/gokcen.html |title=Sabiha Gokcen (1913-2001), Pioneer Aviatrix |first=Russell  |last=Naughton |work=Hargrave Pioneers of Aviation |year=2014 |accessdate=17 November 2014}}</ref> According to some sources, including Guinness World Records,<ref name="First Female Combat Pilot">{{cite web|title=First Female Combat Pilot|url=http://www.guinnessworldrecords.com/world-records/first-female-combat-pilot|website=Guinness World Records Official Web Site|accessdate=15 May 2016}}</ref> she was also the world's first female [[fighter pilot]], being enrolled in the Military Aviation Academy in Eskisehir in 1936. However, others such as [[Marie Marvingt]]<ref>{{cite news|title=1915 - First woman pilot in combat missions as a bomber pilot - Marie Marvingt (France)|url=http://www.centennialofwomenpilots.com/content/1915-first-woman-pilot-combat-missions-bomber-pilot-marie-marvingt-france|publisher=Centennial of Women Pilots|accessdate=10 January 2015|quote=In 1915, Marvingt became the first woman in the world to fly combat missions when she became a volunteer pilot flying bombing missions over German-held territory and she received the Croix de Guerre (Military Cross) for her aerial bombing of a German military base in Metz.}}</ref><ref>Historic Wings – Online Magazine; Article on Hélène Dutrieu Coupe Femina and Marie Marvingt:, Published on December 21, 2012:
http://fly.historicwings.com/2012/12/helene-dutrieux-and-the-coupe-femina
Retrieved 10 January 2015.</ref> and [[Eugenie Mikhailovna Shakhovskaya]]<ref>{{cite book|last=Lawson|first=Eric and Jane|title=The First Air Campaign: August 1914- November 1918|url=https://books.google.com/books?id=9PGHckhHiX0C&pg=PA56&lpg=PA56&dq=aviatrix+Shakhovskaya&source=bl&ots=dP4jdGetaf&sig=7hN0XUfHRRuS7Hal_Qna9a-Ngbs&hl=ru&ei=0fdDTuDoE42YOqnR2aEJ&sa=X&oi=book_result&ct=result&resnum=5&ved=0CEIQ6AEwBA#v=onepage&q=aviatrix%20Shakhovskaya&f=false|date=1996|publisher=[[Da Capo Press]]|isbn=0-306-81213-4|page=56|quote=Eugenie Shakhovskaya, a 25 year old Russian princess, was the first female fighter pilot in history.}}</ref><ref>{{cite web|url=http://www.ctie.monash.edu.au/hargrave/women_combat_pilots_ww1.html|title=Women Combat Pilots of WW1|accessdate=10 January 2015|quote=Princess Eugenie M. Shakhovskaya was Russia's first woman military pilot. Served with the 1st Field Air Squadron. Unknown if she actually flew any combat missions, and she was ultimately charged with treason and attempting to flee to enemy lines. Sentenced to death by firing squad, sentence commuted to life imprisonment by the Tsar, freed during the Revolution, became chief executioner for Gen. Tchecka and drug addict, shot one of her assistants in a narcotic delirium and was herself shot.|publisher=[[Monash University]]}}</ref><ref>{{cite web|url=http://www.britannica.com/women/timeline?tocId=9404138&section=249216|title=300 Women who changed the world |accessdate=10 January 2015|quote=In Russia, Princess Eugenie Shakhovskaya is the first female military pilot. She flies reconnaissance missions.|publisher=[[Encyclopædia Britannica]]}}</ref> preceded her as military pilots in other roles, probably without a military academy enrollment. She was one of the eight [[Adoption|adopted]] children of [[Mustafa Kemal Atatürk]].

Gökçen made headlines and sparked controversy, in 2004,  when [[Hrant Dink]], a journalist of Turkish-Armenian descent, published an interview with Sabiha's niece that revealed that she was of [[Armenians|Armenian]] origin. Her adopted sister [[Ülkü Adatepe]] disputed this during an interview, stating that Sabiha was actually of [[Bosniaks|Bosniak]] ancestry.<ref name="Koser">{{cite news
|url=http://arama.hurriyet.com.tr/arsivnews.aspx?id=204528
|accessdate=13 July 2008
|title=İşte soyağacı
|date=23 February 2004
|work=[[Hürriyet]]
|first=Mutlu
|last=Koser
|archive-url=https://web.archive.org/web/20150402171256/http://www.hurriyet.com.tr/index/ArsivNews.aspx?id=204528
|archive-date= 2 Apr 2015
}}</ref>

== Early life ==
[[File:ZehraRukiyeSabiha.jpg|thumb|left|200px|Daughters of Mustafa Kemal; left to right: Zehra Aylin, Rukiye (Erkin) and Sabiha (Gökçen).]]
[[File:RukiyeSabihaAfetZehra.jpg|thumb|left|200px|Left to right: Rukiye (Erkin), Sabiha (Gökçen), [[Afet İnan|Afet]] (İnan), and Zehra Aylin.]] 
Sabiha Gökçen's origins are a matter of dispute. According to official Turkish sources and interviews with Sabiha Gökçen, she was the daughter of Mustafa Izzet Bey and Hayriye Hanım. During Atatürk's visit to [[Bursa]] in 1925, Sabiha, who was only twelve years old, asked for permission to talk with Atatürk and expressed her wish to study in a [[boarding school]]. After learning her story and about her miserable living conditions, Atatürk decided to adopt her and asked Sabiha's brother for permission to take her to the Çankaya Presidential Residence in Ankara, where Sabiha would live among Atatürk's other adoptive daughters, Zehra, [[Afet İnan|Afet]] and Rukiye. Sabiha attended the Çankaya Primary School in [[Ankara]] and the [[Üsküdar American Academy]] in [[Istanbul]].

In February 2004 an article in the newspaper ''[[Agos]]'', headlined "The Secret of Sabiha Hatun", contained an interview with Hripsime Sebilciyan, a former Gaziantep resident, who claimed to be Gökçen's niece and that Gökçen herself was of Armenian ancestry.<ref name=dink>{{cite news |work=[[Agos]] |title=Sabiha Hatun'un Sırrı |first=Hrant |last=Dink |date=6 February 2004 }}</ref> Mustafa Kemal took a liking to Sebilciyan, who was in an orphanage shortly after the [[Armenian Genocide]], and had her adopted.<ref>{{cite book |last=Morris |first=Chris |title=The New Turkey: The Quiet Revolution On The Edge Of Europe |url=https://books.google.com/books?id=AfeZAwAAQBAJ |date=1 May 2014 |publisher=Granta Publications |isbn=978-1-78378-031-0}}</ref> According to historian Pars Tuğlacı, Gökçen herself found out about her Armenian identity while in Ankara, when members of her family contacted her from Beirut.<ref name=hurriyet>{{cite news |title=Gökçen Ermeni’ydi |url=http://arama.hurriyet.com.tr/arsivnews.aspx?id=204371 |newspaper=[[Hürriyet]] |date=22 February 2004 |language=tr}}</ref> Gökçen reportedly visited her Armenian relatives in Beirut. She had four brothers, Sarkis, Boğos, Haçik and Hovhannes.<ref>{{cite news|last1=Bal |first1=Ali |title=Sabiha Gökçen et la controverse sur ses origines |url=http://www.turquie-news.com/rubriques/histoire/sabiha-gokcen-et-la-controverse,3990.html |agency=Turquie News |date=16 April 2010 |language=French}}</ref>

Just after the introduction of the [[Surname Law]], Atatürk gave her the family name Gökçen on 19 December 1934. 'Gök' means sky in [[Turkish language|Turkish]] and Gökçen means 'belonging or relating to the sky'. However, she was not an aviator at that time,<ref name="TurkishAirForce">{{cite web |url= http://www.hvkk.tsk.tr/tr/IcerikDetay.aspx?ID=34&IcerikID=86 |title=Dünyanın İlk Kadın Savaş Pilotu: Sabiha Gökçen |trans_title=The world's first female combat pilot: Sabiha Gökçen |work=Türk Hava Kuvvetleri |year=2014 |accessdate=17 November 2014 |language=tr }}</ref><ref>{{cite book |last=Atatürk |first=Kemal |title=Atatürk'ün bütün eserleri |trans_title=Ataturk's Complete Works |url=https://books.google.com/books?id=p49AAQAAIAAJ|volume=27 |date=1 January 1998 |publisher=Kaynak Yayınları |isbn=978-975-343-235-1 |page=109 |language=tr }}</ref> and it was only six months later that Sabiha developed a passion for flying.

==Career==
[[File:Sabiha Gokcen with cap.jpg|thumb|Sabiha Gökçen with officers.]]
[[File:Sabiha Gokcen in Athens.jpg|thumb|Sabiha Gökçen in Athens, during her 1938 Balkan tour.]]
Atatürk attached great importance to aviation and for that purpose oversaw the foundation of the [[Turkish Aeronautical Association]] in 1925. He took Sabiha along with him to the opening ceremony of ''Türkkuşu'' (''Turkishbird'') ''Flight School'' on 5 May 1935. During the [[airshow]] of [[Glider aircraft|gliders]] and [[Parachute|parachutists]] invited from foreign countries, she got very excited. As Atatürk asked her whether she would also want to become a skydiver, she nodded "yes indeed, I am ready right now". Atatürk instructed Fuat Bulca, the head of the school, to enroll her as the first female trainee. She was meant to become a [[skydiver]], but she was much more interested in flying, so she received her [[pilot's licence]]. Gökçen was sent to [[Russia]], together with seven male students, for an advanced course in glider and powered aircraft piloting. However, when she was in Moscow, she learned the news that Zehra had died, and with collapsed morale, she immediately returned to Turkey, isolating herself from social activities for some time.

As girls were not being accepted by the [[Turkish War Colleges|War College]] in Turkey in those years,  Sabiha Gökçen was provided, on Atatürk's orders, with a personalized uniform, and attended a special education programme of eleven months at the ''Tayyare Mektebi'' (Aviation School) in [[Eskişehir]] in the academic year 1936-1937. After receiving her flight patents (diploma) she trained to become a war pilot at the 1st Airplane Regiment in Eskişehir for six months.

She improved her skills by flying bomber and fighter planes at the 1st Aircraft Regiment in [[Eskişehir Province|Eskişehir]] Airbase and got experience after participating in the [[Aegean Sea|Aegean]] and [[Thrace]] exercises in 1937. In that same year, she took part in the [[Dersim rebellion]] and became the first Turkish female air force combat pilot. A report of the General Staff mentioned the "serious damage" that had been caused by her 50&nbsp;kg bomb to a group of fifty fleeing "bandits."<ref>{{cite book |first=Reşat |last=Hallı |title=Türkiye Cumhuriyetinde Ayaklanmalar (1924–1938) |trans_title=The Rebellions in the Republic of Turkey, 1924-1938 |publisher=T. C. Genelkurmay Baskanlığı Harp Tarihi Dairesi |year=1972 |page=382 |language=tr }}</ref> and she was awarded with a ''takdirname'' (letter of appreciation). She was also awarded the [[Turkish Aeronautical Association]]'s first "''Murassa'' (Jeweled) Medal" for her superior performance in this operation.<ref name="TurkishAirForce"/>

In 1938, she carried out a five-day flight around the [[Balkan]] countries to great acclaim. In the same year, she was appointed "chief trainer" of the Türkkuşu Flight School of the [[Turkish Aeronautical Association]], where she served until 1954 as a flight instructor<ref name="TurkishAirForce"/> and became a member of the association's executive board. She trained four female aviators, Edibe Subaşı, Yıldız Uçman, Sahavet Karapas and [[Nezihe Viranyalı]]. Sabiha Gökçen flew around the world for a period of 28 years until 1964. Her book entitled "''A Life Along the Path of Atatürk''" was published in 1981 by the Turkish Aeronautical Association to commemorate Atatürk's 100th birthday.

Throughout her career in the [[Turkish Air Force]], Gökçen flew 22 different types of aircraft for more than 8,000 hours, 32 hours of which were active combat and bombardment missions.<ref name=trt>{{YouTube|id=m-hv6MfohTs#t=9m30s|title=TRT documentary on Sabiha Gökçen}}. See 9m30s in for 1996 USAF poster claim.</ref>

==Controversies==
[[File:Atatürk, Ali Çetinkaya and Sabiha Gökçen in Diyarbakır.jpg|thumb|200px|right|Atatürk, [[Ali Çetinkaya]] and Sabiha Gökçen in [[Diyarbakır]].]]
After the 2004 article "The Secret of Sabiha Hatun" said that Sabiha had an Armenian origin, many contested the matter, including the last living daughter of Atatürk, [[Ülkü Adatepe]], who disputed the claim during an interview.<ref>{{cite news
|url=http://webarsiv.hurriyet.com.tr/2004/02/28/419832.asp
|title=Sabiha Gökçen tartışmasında kim ne yazdı
|work=[[Hürriyet]]
|date=28 February 2004
|accessdate=13 July 2008
|first=Hüseyin
|last=Tekin
|language=Turkish
}}</ref><ref>{{cite news |last=Morgan |first=Tabitha |url=http://news.bbc.co.uk/2/hi/europe/3519561.stm |title=Turkish heroine's roots spark row |work=[[BBC News]] |date=29 February 2004}}</ref><ref>{{cite web
|url=http://www.state.gov/g/drl/rls/hrrpt/2004/41713.htm
|title=2004 Country Report on Human Rights Practices in Turkey
|date=28 February 2005
|accessdate=25 July 2008
|publisher=[[US State Department]]
|work=[[Country Reports on Human Rights Practices]]
|author=Bureau of Democracy, Human Rights, and Labor
|quote=In February, the Hurriyet newspaper's publication of a report that Sabiha Gokcen--an adopted daughter of Mustafa Kemal Ataturk, who was the country's first female pilot--was of Armenian descent drew a number of racist public statements. The Turkish General Staff issued a statement criticizing the reports on Gokcen's Armenian ancestry as 'a claim that abuses national values and feelings' while the Turkish Air Association called the report 'an insult' to Gokcen and to Ataturk.
}}</ref> According to Adatepe, Sabiha's mother Hayriye was an ethnic [[Bosniaks|Bosniak]].<ref name="Koser"/> The mere notion that Gökçen could have been Armenian caused an uproar throughout Turkey and [[Hrant Dink]], the journalist who wrote the article, came under fire, most notably from newspaper columnists and Turkish ultra-nationalist groups, which labeled him a traitor.<ref name="Cable">{{cite web |url=http://www.cablegatesearch.net/cable.php?id=04ISTANBUL374 |archiveurl=https://web.archive.org/web/20131209123810/http://www.cablegatesearch.net/cable.php?id=04ISTANBUL374 |archivedate=9 December 2013 |title=Cable reference id: #04ISTANBUL374 |work=web.archive.org |date=10 March 2004 |accessdate=17 November 2014}}</ref> A [[United States diplomatic cables leak|US consul dispatch]] leaked by [[WikiLeaks]] and penned by an official from the consulate in Istanbul observed that the entire affair "exposed an ugly streak of racism in Turkish society."<ref name="Cable"/> It is also believed that the affair was one of the reasons that led to [[Assassination of Hrant Dink|Hrant Dink's assassination]] in Istanbul in January 2007, by Ogün Samast, a 17-year-old Turkish nationalist.<ref>{{cite news|last1=Insel |first1=Ahmet |title="Bu Hareket Beşeriyet Namına Bir Cinayetti": Ermenilerden Özür Dileme Girişiminin Değerlendirilmesi |url=http://www.birikimdergisi.com/birikim/dergiyazi.aspx?did=1&dsid=367&dyid=5466&dergiyazi=%22Bu%20Hareket%20Be%FEeriyet%20Nam%FDna%20Bir%20Cinayetti%22:%20Ermenilerden%20%D6z%FCr%20Dileme%20Giri%FEiminin%20De%F0erlendirilmesi |agency=Birikim |date=28 February 2009 |language=Turkish}}</ref><ref>{{cite news |last1=Kemal Cengiz |first1=Orhan |title=The ‘smoke’ which killed Hrant Dink |url=http://www.todayszaman.com/news-198552-109-the-smoke-which-killed-hrant-dink-byorhan-kemal-cengiz.html |agency=Zaman |date=15 January 2010}}</ref><ref>{{cite book |last1=Göktaş |first1=Kemal |title=Hrant Dink cinayeti: medya, yargı, devlet |date=2009 |publisher=Güncel Yayıncılık |location=İstanbul |isbn=9944840491 |edition=1. baskı. |url=https://books.google.com/books?ei=vkMyVIjNNs2togTV24GYAg}}</ref>

==Legacy and recognition==
[[File:İstanbul 5366.jpg|thumb|175px|right|Bust of Sabiha Gökçen in the [[Istanbul Aviation Museum]].]]
[[Sabiha Gökçen International Airport]] in [[Istanbul]] is named after her.

She is recognized as the first female combat pilot by [[The Guinness Book of World Records]].<ref name="First Female Combat Pilot"/>

She was selected as the only female pilot for the poster of "''The 20 Greatest Aviators in History''" published by the [[United States Air Force]] in 1996.<ref name=trt/>

She was the subject of a [[Google Doodle]] honoring her birthday which was displayed in Turkey on 22 March 2009.<ref>{{cite web |url= https://www.google.com/doodles/sabiha-gokcens-birthday |title=Sabiha Gökçen's Birthday |work=Google Doodles Archive |year=2014 |accessdate=17 November 2014}}</ref>

==See also==
{{Portal|Turkey|Biography|Aviation}}
*[[List of firsts in aviation]]

== References ==
{{reflist|30em}}

== External links ==
* {{commons category-inline|Sabiha Gökçen}}
* Atatürk Arşivi - Sabiha Gökçen: [https://www.youtube.com/watch?v=k0C_DraLdXU Part 1][https://www.youtube.com/watch?v=uFNZpeaikxs Part 2][https://www.youtube.com/watch?v=XI7WMjPnTok Part 3][https://www.youtube.com/watch?v=AyiJrVj2vnA Part 4][https://www.youtube.com/watch?v=m-hv6MfohTs Part 5][https://www.youtube.com/watch?v=NS5yB9FdIzs Part 6]

{{Authority control}}

{{DEFAULTSORT:Gokcen, Sabiha}}
[[Category:1913 births]]
[[Category:2001 deaths]]
[[Category:People from Bursa]]
[[Category:People from Hüdavendigâr Vilayet]]
[[Category:Dersim massacre]]
[[Category:Bosniak diaspora]]
[[Category:Turkish people of Bosniak descent]]
[[Category:Turkish women in warfare]]
[[Category:Turkish Air Force personnel]]
[[Category:Turkish aviators]]
[[Category:Mustafa Kemal Atatürk]]
[[Category:Female aviators]]
[[Category:Aviation pioneers]]
[[Category:Üsküdar American Academy alumni]]
[[Category:People of the Dersim massacre]]
[[Category:Women in war 1900–1945]]
[[Category:Armenian aviators]]