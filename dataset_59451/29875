{{Infobox Magazine
|         title = Authentic Science Fiction
|    image_file = Authentic cover issue 29.jpg
|    image_size = 180px
| image_caption = December 1952 issue; cover by Vann
|        editor = [[Gordon Landsborough]], [[H. J. Campbell|H.J. Campbell]], [[E.C. Tubb]]
|  editor_title = Editor
|     frequency = Fortnightly for 8 issues, then monthly
|      category = [[Science fiction magazine]]
|       company = Hamilton & Co.
|     firstdate = {{Start date|1951|01}}
|     finaldate = {{End date|1957|10}}
|   finalnumber = 85
|       country = [[United Kingdom]]
|      language = [[English language|English]]
}}
'''''Authentic Science Fiction''''' was a British [[science fiction magazine]] published in the 1950s that ran for 85 issues under three editors: [[Gordon Landsborough]], [[H. J. Campbell|H.J. Campbell]], and [[Edwin Charles Tubb|E.C. Tubb]]. The magazine was published by Hamilton and Co., and began in 1951 as a series of novels appearing every two weeks; by the summer it became a monthly [[magazine]], with readers' letters and an [[editorial]] page, though fiction content was still restricted to a single novel. In 1952 short fiction began to appear alongside the novels, and within two more years it completed the transformation into a science fiction magazine.

''Authentic'' published little in the way of important or ground-breaking fiction, though it did print [[Charles L. Harness|Charles L. Harness's]] "The Rose", which later became well-regarded. The poor rates of pay—£1 per 1,000 words—prevented the magazine from attracting the best writers. During much of its life it competed against three other moderately successful British science fiction magazines, as well as the American science fiction magazine market. Hamilton folded the magazine in October 1957, because they needed cash to finance an investment in the UK rights to an American best-selling [[novel]].

== History ==
In 1950, science fiction (sf) magazines had been published successfully in North America for over twenty years, but little progress had been made in establishing British equivalents. The bulk of British sf was published as [[paperback]] books, rather than magazines; a situation opposite of that in the US.<ref name="Ashley_T_82_A">Ashley (''Transformations'', p.&nbsp;82.) quotes the figures for 1952 as 95 sf books to 33 issues of sf magazines in the UK, whereas in the US that year there were 16 sf books and 152 sf magazine issues.</ref> Several short-lived magazines had come and gone, both before and after the war. John Spencer launched four very poor quality juvenile magazines in 1950, which continued into the mid-1950s,<ref name="AshleyV3_65">Ashley, ''History of SF Magazine Vol. 3'', pp.&nbsp;65–66.</ref> while one magazine, ''[[New Worlds (magazine)|New Worlds]]'', had survived since 1946.<ref name="ESF_867">Brian Stableford & Peter Nicholls, "New Worlds", in Nicholls & Clute, ''Encyclopedia of Science Fiction'', p.&nbsp;867</ref> Since 1939, Atlas, a British publisher, had been producing a reprint edition of ''[[Analog Science Fiction and Fact|Astounding Science Fiction]]'', one of the most well-regarded American sf magazines. During the war the contents had often been cut severely, and the schedule had not been regular, but it was reputed to sell 40,000 copies a month. This was enough to attract the attention of Hamilton & Co., a British publisher looking for new markets.<ref name="AshleyV3_68">Ashley, ''History of SF Magazine Vol. 3'', pp.&nbsp;68–71</ref>

In 1949, Hamilton hired Gordon Landsborough as an editor. Landsborough did his best to improve the quality of the science fiction he was publishing, and was allowed to offer £1 per 1,000 words for selected material. He also was joined at Hamilton by [[H. J. Campbell|H.J. Campbell]], who was hired as a technical editor. Campbell was a London [[science fiction fandom|science fiction fan]]; he had been brought on by Hulton Press (publisher of the very successful comic the ''[[Eagle (comic)|Eagle]]'') to create a science fiction magazine, but the project had been abandoned before seeing print.<ref name="AshleyV3_68" />

By the start of 1951, Hamilton's science fiction titles were being published every two weeks. On 1 January 1951, Hamilton published ''Mushroom Men from Mars'', by Lee Stanton, which was a [[pseudonym]] for Richard Conroy. A banner was added to the base of the cover reading "Authentic Science Fiction Series"; the same banner appeared on the 15 January novel, ''Reconnoitre Krellig II'', by Jon J. Deegan, also a pseudonym, this time for Robert G. Sharp. With the next book, Roy Sheldon's ''Gold Men of Aureus'', Landsborough changed the banner to read "Science Fiction Fortnightly No. 3", thinking that the caption might help sales.<ref name="Ashley_T_82">Ashley, ''Transformations'', pp.&nbsp;82–86.</ref> In addition to the banner, a contents page (including a date and issue number), a letter column, an editorial, and an [[advertisement]] for subscriptions were inserted.<ref name="issues" /> According to Landsborough, the banner was only intended to indicate the publishing schedule to readers, but combined with the other changes the appearance became much more magazine-like.<ref name="Ashley_T_82" /> These changes established the sequence in the minds of readers and collectors, and retroactively determined that ''Mushroom Men from Mars'' had been the first in the series: the first two issues had carried no issue number.<ref name="Ashley_T_82" /> Issue 3 was also the first issue to carry the editors' names:<ref name="issues">See the individual issues.</ref> Landsborough used the pseudonym L.G. Holmes ("Holmes" was his middle name) for his editing role on the magazine.<ref name="Ashley_T_82" />

The caption did apparently help sales: Landsborough subsequently commented that while Hamilton's other titles were selling perhaps 15,000 copies, ''Authentic'' managed to sell 30,000.<ref name="Ashley_T_82" /> After the banners were in place, Hamilton proposed launching a monthly sf magazine. Landsborough was concerned about the workload, and also felt it would be difficult to find enough good material; Hamilton refused to increase the pay rate, which was not high enough to attract the best stories. A compromise was reached, and ''Authentic'' was born as a monthly magazine in paperback format, with a single novel and a short editorial feature in each issue, plus an occasional short story. The eighth issue was the last on the fortnightly schedule. Issues 9–12 were titled "Science Fiction Monthly" in the [[page footer|footer]] of the cover. In mid-1951, Landsborough left Hamilton, and Campbell replaced him as editor of ''Authentic'' with the thirteenth issue, which was also the first one on which the title changed to "Authentic Science Fiction".<ref name="AshleyV3_68" /><ref name="issues" />

Under Campbell ''Authentic'' improved somewhat, and continued its metamorphosis into a magazine, with additional non-fiction writing, and short fiction in addition to the main novel in each issue. Hamilton also ran a science fiction paperback imprint, [[Panther Books]], which would go on to become one of the leading British sf houses. By 1953 the British sf market was going through a metamorphosis similar to the one going in the US at the same time: poor quality sf markets were failing, and the result was a reduced but active market, with four magazines: ''Authentic'', ''[[New Worlds (magazine)|New Worlds]]'', ''[[Science Fantasy (magazine)|Science Fantasy]]'', and ''[[Nebula Science Fiction]]''.<ref name="Ashley_T_93">Ashley, ''Transformations'', p.&nbsp;93.</ref>

At the end of 1955 Campbell decided to give up editing in favour of his scientific career as a research chemist. He was replaced from the February 1956 issue by [[E.C. Tubb]], who remained editor to the end of the magazine's life.<ref name="AshleyV3_68" /> Tubb had contributed a great deal of material to the magazine under various pseudonyms, often amounting to more than half of an issue's fiction, and he later recalled that Campbell's way of hiring him as editor was to say to him, "As you're practically writing it, you may as well edit it."<ref name="AshleyV4_40_quote">Ashley, ''History of SF Magazine Part 4'', pp.&nbsp;40–42. The quote is given as "You write most of it, you might as well edit it" in ''Transformations'', p.&nbsp;99.</ref>

The quality of material submitted to Tubb was "dreadful", in the words of sf historian Michael Ashley,<ref name="AshleyV4_40_dreadful">Ashley, ''History of SF Magazine Part 4'', p.&nbsp;40</ref> and included many stories that had previously been rejected by Campbell: he was able to recognize these because Campbell had kept a log of all submissions. One story was rejected that had been plagiarized from one that had appeared twelve years earlier in ''Astounding Science Fiction''. Tubb's overall acceptance rate was about one in twenty-five submissions. As a result, he found it difficult to keep standards up, often finding himself forced to write material under pseudonyms to fill an issue.<ref name="AshleyV4_40">Ashley, ''History of SF Magazine Part 4'', pp.&nbsp;40–42</ref>

In early 1957, Tubb persuaded Hamilton to switch the magazine from pocket-book to [[digest size]] format, in the hope that this would improve the magazine's visibility on bookstalls. The circulation did indeed rise, to about 14,000 copies per month—a surprisingly low figure given Landsborough's assertion that ''Authentic'' had been selling 30,000 copies in the early days. However, later that year, Hamilton made the decision to invest a substantial sum in the UK paperback rights of an American best-seller: it is not known for certain which book this was, but it is thought to have been [[Evan Hunter|Evan Hunter's]] ''The Blackboard Jungle''. Hamilton could no longer afford to have cash tied up in ''Authentic'', and in the summer of 1957 Tubb was given two months to close down the magazine, printing stories that had already been paid for. The last issue was dated October 1957.<ref name="AshleyV4_40" />

== Contents and reception ==
[[File:Authentic insert issue 27 front.jpg|thumb|An advertising insert that ran in issue 27; the inset image is the cover for issue 28. The other side of the insert advertised books published by Hamilton.]]For the first twenty-five issues, ''Authentic'' ran a full novel in every issue, but no other fiction, though there were various non-fiction departments such as "Projectiles" (readers' letters), an editorial, [[book review]]s, fanzine reviews, and science related articles, [[quiz]]zes, and news columns. In issue 26, dated October 1952, the first installment of ''Frontier Legion'', a serial by Sydney J. Bounds, appeared. With issue 29, the full-length novel, ''Immortal's Playthings'' by [[William F. Temple]], was accompanied by a short story, [[Ray Bradbury|Ray Bradbury's]] "Welcome, Brothers!" as well as part four of ''Frontier Legion''. The serial was stretched out over six issues by printing scarcely more than a dozen pages in each installment; it finally completed in issue 31.<ref name="Ashley_T_82" /><ref name="issues" />

With issue 36 (August 1953), the cover text changed from advertising a "Full-length Novel" to "Full-length Story"; the "featured story", as it was called in the contents page, was still the longest piece of fiction in the issue, but was no longer necessarily even close to novel length. Issue 41, for example, ran Richard deMille's "The Phoenix Nest" as the lead story, with fewer than forty pages of text. Finally, in issue 60 (August 1955), the word "feature" was removed from the contents page, and with it the last vestige of the origin of the magazine as a series of novels.<ref name="issues" />

The early novels published by Hamilton were of generally poor quality. Michael Ashley, a historian of sf, described the first issue, Lee Stanton's ''Mushroom Men of Mars'' as "of abysmal quality", and the third, [[Roy Sheldon|Roy Sheldon's]] ''Gold Men of Aureus'' as "atrocious". However, Campbell contributed some better work, beginning with ''Phantom Moon'', under the [[Pen name#Collective names|house name]] Roy Sheldon, which appeared in issue 6, dated 15 March 1951; his first novel under his own name was ''World in a Test Tube'', which appeared in issue 8, dated 15 April 1951. He continued to write for the magazine after he became editor—his work has been described as "enjoyable", though "not especially sophisticated". Tubb was also a regular contributor, often under house names, which according to Landsborough were used by Hamilton to prevent authors gaining name recognition under a pseudonym and then taking that name to another publisher.<ref name="AshleyV3_68" /><ref name="Ashley_T_82" />

Regulars in the magazine included Sydney J. Bounds, [[William F. Temple]], Bryan Berry, and [[Ken Bulmer]].<ref name="Nicholls_74">Frank H. Parnell & Peter Nicholls, "Authentic Science Fiction", in Nicholls & Clute, ''Encyclopedia of Science Fiction'', p.&nbsp;74</ref> At the start of 1953, ''Authentic'' began to include material that had been previously published in the US; this practice ceased later that year, but began again in 1956, and led to the reprinting of material by well-known names such as [[Isaac Asimov]], whose 1951 story "Ideals Die Hard" was reprinted in issue 78, dated March 1957. Other well-known names that appeared in ''Authentic'' included [[Brian Aldiss]] and [[John Brunner (novelist)|John Brunner]].<ref name="Ashley_T_82" /><ref name="issues" /> Campbell had encouraged science articles during his tenure, but under Tubb's editorship these were gradually eliminated.<ref name="Nicholls_74" />

Perhaps the most notable story ''Authentic'' published was [[Charles L. Harness|Charles L. Harness's]] "The Rose", which appeared in the March 1953 issue.<ref name="Notable_Tuck">Mentioned in the "Notable Fiction" section of Tuck, ''Encyclopedia of SF'', p.&nbsp;548.</ref> Other than this, ''Authentic'' published little of note: the [[Peter Nicholls (writer)|Nicholls]]/[[John Clute|Clute]] ''Encyclopedia of SF'' commented that it "seldom published stories of the first rank", specifically excepting Harness's "The Rose".<ref name="Nicholls_74" /> [[David Kyle]], in his ''Pictorial History of Science Fiction'', states that Campbell improved the magazine, making it "remarkably good",<ref name="Kyle_118">Kyle, ''Pictorial History'', p.&nbsp;118.</ref> and sf expert [[Donald H. Tuck|Donald Tuck's]] opinion was that it eventually achieved "a good standard",<ref name="Tuck p548">Donald H. Tuck, ''Encyclopedia of SF'', p.&nbsp;548.</ref> but in [[Mike Ashley (writer)|Michael Ashley's]] opinion, the magazine "sadly lacked originality", and ran fiction that was "stereotyped and forced, frequently because Campbell had to rely on the same small band of regulars to supply the bulk of the fiction".<ref name="Holdstock_65">Michael Ashley, "Magazines", in Holdstock, ''Encyclopedia of SF'', p.&nbsp;65.</ref>

The cover artwork was initially poor: the very first issue has been described as "British [[Pulp magazine|pulp]] at its most infantile",<ref name="Nicholls1_53">Frank H. Parnell & Peter Nicholls, "Authentic Science Fiction", in Nicholls, ''Encyclopedia of Science Fiction'', p.&nbsp;53</ref> but the covers began to improve from mid-1953.<ref name="Ashley_T_82" /> [[Josh Kirby]], now well known for his [[Discworld]] art, contributed seven covers, beginning with issue 61 in September 1955. There were also many covers on astronomical themes: these were clearly influenced by the US artist [[Chesley Bonestell]], and were fairly successful.<ref name="Holdstock_129">[[David A. Hardy|David Hardy]], "Art and Artists", in Holdstock, ''Encyclopedia of SF'', p.&nbsp;129.</ref>

== Bibliographic details ==
''Authentic'' was pocket book size (7.25 × 4.75&nbsp;inches) for most of its life, changing to digest size (7.5 × 5.5&nbsp;inches) for the last eight issues. The issue numbering was consecutive from 1 to 85, with no volume numbers. The first issue had a publication date of 1 January 1951, and the first eight issues had publication dates of the 1st and 15th of each month. From the ninth issue to the end ''Authentic'' maintained a completely regular monthly schedule; the publication date was given in the magazine as the 15th of each month from issue 9 through issue 73; thereafter the date was just given as the month and year.<ref name="issues" />

The price began as 1/6 (one [[shilling]] and six pence); the price was raised to two shillings with issue 60, February 1955, and stayed at that price until the end of the run. Interior artwork was not used for the first issues, which contained no fiction other than a single novel; illustrations began to appear with issue 29. Tubb announced in issue 85, which turned out to be the last issue, that he had dropped all interior artwork.<ref name="issues" />

The title of the magazine changed several times:<ref name="issues" /><ref name="Tuck p548" />

{|class="wikitable"
! Issues !! Dates !! Title
|-
| 1–2 || 1 January 1951&nbsp;– 15 January 1951 || ''Authentic Science Fiction Series''
|-
| 3–8 || 1 February 1951&nbsp;– 15 April 1951 || ''Science Fiction Fortnightly''
|-
| 9–12 || May 1951&nbsp;– August 1951 || ''Science Fiction Monthly''
|-
| 13–28 || September 1951&nbsp;– December 1952 || ''Authentic Science Fiction''
|-
| 29–67 || January 1953&nbsp;– April 1956 || ''Authentic Science Fiction Monthly''
|-
| 69–85 || May 1956&nbsp;– October 1957 || ''Authentic Science Fiction'' on the masthead<ref>The contents page and the cover would sometimes use the title ''Authentic Science Fiction Monthly''</ref>
|}

The first six issues were 132 pages, with the page count dropped to 116 for issues 7 through 25. Issue 26 saw the page count return to 132. The cover layout for all these issues remained essentially the same, despite title changes. With issue 29 a layout using a yellow inverted "L" to frame the cover picture was introduced, and the page count was increased to 148. Another cover redesign with issue 39 saw the yellow "L" removed, and the page count went up again to 164 with issue 41, then back to 148 with issue 47. The cover design varied further, with different title fonts; the page count went back to 132 with issue 57, then returned to 164 from issue 60 through issue 77, the last in pocket-book format. The eight issues in digest format all had 132 pages.<ref name="issues" />

The editors were:<ref name="Tuck p548" />
* L.G. Holmes (pseudonym for Gordon Landsborough), issues 1–27 (27 issues)
* H.J. Campbell, issues 28–65 (38 issues)
* E.C. Tubb, issues 66–85 (20 issues)

== Notes ==
{{reflist|30em}}

== References ==
{{refbegin}}
* {{cite book | first = Michael | last = Ashley | title = The History of the Science Fiction Magazine Vol. 3 1946–1955 | publisher = Contemporary Books | location = Chicago | year = 1976 | isbn = 978-0-8092-7842-8 }}
* {{cite book | first = Michael | last = Ashley | title = The History of the Science Fiction Magazine Part 4 1956–1965 | publisher = New English Library | location = London | year = 1978 | isbn = 978-0-450-03438-1 }}
* {{cite book | first = Mike | last = Ashley | title = Transformations: The Story of the Science Fiction Magazines from 1950 to 1970 | publisher = Liverpool University Press | location = Liverpool | year = 2005 | isbn = 978-0-85323-779-2 }}
* {{cite book | first = Robert | last = Holdstock | authorlink = Robert Holdstock | title = Encyclopedia of Science Fiction | publisher = Octopus Books | location = London | year = 1978 | isbn = 978-0-7064-0756-3 }}
* {{cite book | first = David | last = Kyle | title = A Pictorial History of Science Fiction | publisher = the Hamlyn Publishing Group | location = London | year = 1976 | isbn = 978-0-600-38193-8 }}
* {{cite book | last = Nicholls | first = Peter | title = The Encyclopedia of Science Fiction | year = 1979 | publisher = Granada Publishing | location = St Albans | isbn = 978-0-586-05380-5 }}
* {{cite book | last = Clute | first = John |author2=Nicholls, Peter | title = The Encyclopedia of Science Fiction | year = 1993 | publisher = St. Martin's Press | location = New York | isbn = 978-0-312-09618-2 }}
* {{cite book | first = Donald H. | last = Tuck | title = The Encyclopedia of Science Fiction and Fantasy: Volume 3 | publisher = Advent: Publishers | location = Chicago | year = 1982 | isbn = 978-0-911682-26-7 }}
{{refend}}

== External links ==
{{Portal|Speculative fiction}}
* [http://isfdb.org/wiki/index.php/Magazine:Authentic_Science_Fiction Comprehensive index of issues and contents]
* [http://www.worldcat.org/search?q=%22Authentic+Science+Fiction%22&=Search&qt=results_page "Authentic Science Fiction"] in [[WorldCat]]

{{BritishMagazines}}

{{Featured article}}

[[Category:1951 establishments in the United Kingdom]]
[[Category:1957 disestablishments in the United Kingdom]]
[[Category:Defunct British science fiction magazines]]
[[Category:Magazines disestablished in 1957]]
[[Category:Magazines established in 1951]]
[[Category:British monthly magazines]]
[[Category:Science fiction magazines established in the 1950s]]
[[Category:Fortnightly magazines]]