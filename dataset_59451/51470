{{Infobox journal
| title         = Historical Biology
| cover         = [[File:Historical_Biology_cover.jpg|150px]]
| caption       = 
| former_name   = <!-- or |former_names= -->
| abbreviation  = Hist. Biol.
| discipline    = <!-- or |subject= -->
| peer-reviewed = 
| language      = 
| editor        = [[Gareth J. Dyke]] <!-- or |editors= -->
| publisher     = [[Taylor & Francis]]
| country       = 
| history       = 1988&ndash;present
| frequency     = 8/year
| openaccess    = 
| license       = 
| impact        = 1.489
| impact-year   = 2014
| ISSNlabel     =
| ISSN          = 0891-2963
| eISSN         = 1029-2381
| CODEN         = HIBIEW
| JSTOR         = 
| LCCN          = 88659240
| OCLC          = 915061764
| website       = http://www.tandfonline.com/action/journalInformation?show=aimsScope&journalCode=ghbi20
| link1         = http://www.tandfonline.com/loi/ghbi20#.VfgGfpc-dcw
| link1-name    = Online access
| link2         = <!-- up to |link5= -->
| link2-name    = <!-- up to |link5-name= -->
| boxwidth      = 
}}

'''''Historical Biology''''' is a [[peer-review]]ed [[scientific journal]] of [[paleobiology]]. It was established in 1988, and is published by [[Taylor & Francis]]. The journal is edited by [[Gareth J. Dyke]] ([[National Oceanography Centre]]).

== Abstracting & Indexing  ==
The journal is abstracted and indexed in the following databases.<ref>http://miar.ub.edu/issn/0891-2963</ref><ref>http://www.tandfonline.com/action/journalInformation?show=abstractingIndexing&journalCode=ghbi20#.VfgIEJc-dcw</ref>
{{columns-list|colwidth=30em|
*[[Academic Search Premier]]
*[[AGORA]]
*[[Biosis]]
*[[Clasificación integrada de revistas científicas|CIRC]]
*[[EMBASE]]
*[[Geobase]]
*[[OARE]]
*[[Science Citation Index]]
*[[Scopus]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.489.<ref name=WoS>{{cite book |year=2015 |chapter=Historical Biology |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{reflist}}

==External links==
*{{Official|1=http://www.tandfonline.com/action/journalInformation?show=aimsScope&journalCode=ghbi20}}

[[Category:Biology journals]]
[[Category:Taylor & Francis academic journals]]
[[Category:Publications established in 1988]]
[[Category:Paleontology journals]]