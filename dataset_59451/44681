{{Infobox organization
|name  = Australia ICOMOS
|logo  = 
|motto =
|type  = Professional Body
|founded            = {{start date|1976}}
|location           = [[Melbourne]], [[Australia]]
|key_people         = 
|fields             = 
|services           = Conservation and protection of [[cultural heritage]] places in Australia
|num_members        = 
|homepage           = {{URL|http://australia.icomos.org}}
}}

'''Australia ICOMOS''' is the peak cultural heritage conservation body in Australia. It is a branch of the [[United Nations]]-sponsored [[International Council on Monuments and Sites]] (ICOMOS), a non-government professional organization promoting expertise in the conservation of cultural heritage.<ref name=PlanAust>Susan Thompson, [https://books.google.com/books?id=cE37xlhiF1cC ''Planning Australia: An Overview of Urban and Regional Planning''] (Cambridge University Press, 2007)</ref>  Its secretariat is based at the [[Cultural Heritage Centre for Asia and the Pacific]] at [[Deakin University]].

==Formation and role==

Australia ICOMOS was formed in 1976 and is one of over 100 current ICOMOS national committees. ICOMOS was formed in 1965 to advise [[UNESCO]] in the assessment of sites proposed for the [[World Heritage List]]. Membership of Australia ICOMOS comprises over 650 members, managed by an Executive Committee of 15 people who are elected from the membership. Several Australia ICOMOS members are also represented on various ICOMOS [[International Council on Monuments and Sites#International committees|International Scientific Committees]], and expert committees and boards in Australia.<ref name=AI>[http://australia.icomos.org/about-us/australia-icomos/mission-statement/ Australia ICOMOS Website]</ref> It plays an important role in co-ordinating advocacy activities to raise the profile of Australia's cultural heritage.<ref>[http://www.nationaltrust.org.au/Assets/1176/1/AustralianHeritagePartnership-3August20111.pdf  National Trust Australia, Media Release 3 August 2011] {{webarchive |url=https://web.archive.org/web/20130618020304/http://www.nationaltrust.org.au/Assets/1176/1/AustralianHeritagePartnership-3August20111.pdf |date=June 18, 2013 }}</ref>

The first meeting which led to the formation of Australia ICOMOS was in Melbourne on 20 October 1976, and the first ICOMOS conference was in Beechworth in 1978, where they devised a committee to work up a local version of the [[Venice Charter]]. The actual ICOMOS meeting where the committee's draft was provisionally endorsed was in the town of [[Burra, South Australia|Burra]] in 1979.<ref name=PlanAust/><ref>[http://australia.icomos.org/publications/charters/ Australia ICOMOS > The Burra Charter] Full text of the 1999 revised version of the Burra Charter. Retrieved 16 August 2011</ref><ref name=AI/> Australia ICOMOS played the pivotal role in developing and [[Burra Charter]], regarded as the best-practice standard for cultural heritage management in Australia, which has influenced subsequent heritage legislation and conservation guidelines and practices in Australia.<ref name=PlanAust/><ref>Marta De la Torre, Getty Conservation Institute, [https://books.google.com/books?id=pj24e6iE7DoC ''Heritage Values in Site Management: Four Case Studies''] (Getty Publications, 2005)</ref>

Australia ICOMOS has also been responsible for producing the Australia [[State of the Environment]] Report (SoE), to advise the Minister for Environment on ''...the current condition of the Australian environment, the pressures on it and the drivers of those pressures.''<ref>[http://www.environment.gov.au/soe/2011/report/heritage/pubs/soe2011-supplementary-heritage-icomos-workshop-notes.pdf Department of Sustainability, Environment, Water, Population and Communities, Australia ICOMOS State of the environment 2011 workshop, summary notes DECEMBER 2011] {{webarchive |url=https://web.archive.org/web/20131022175529/http://www.environment.gov.au/soe/2011/report/heritage/pubs/soe2011-supplementary-heritage-icomos-workshop-notes.pdf |date=October 22, 2013 }}</ref>

Australia ICOMOS also organises an annual national conference on themes relevant to conservation and heritage in Australia and South East Asia, often on a specific heritage and conservation theme for example on the Australian Capital city's 100th anniversary in 2012.<ref name="canberra100.com.au">[http://www.canberra100.com.au/calendar/view/1942/icomos-national-conference-imagined-pasts-imagined-future/ ICOMOS National Conference, 'Imagined Pasts, Imagined Futures']</ref>

Recent collaboration between the Chinese government, the Getty Conservation Institute and Australia ICOMOS has seen the export of Australian conservation expertise in developing '''China Principles''', ''...the Middle Kingdom's statement of conservation philosophy and method that is based on Australia's highly regarded Burra Charter''.<ref>[http://www.theaustralian.com.au/arts/selling-heritage-to-china/story-e6frg8n6-1226067465487#sthash.HZJLffKT.dpuf Robert Bevan, 'Selling heritage to China' ''The Australian'' June 02, 2011]</ref> Australia ICOMOS and the Burra Charter have also been held up as a world standard in Malta.<ref>[http://www.timesofmalta.com/articles/view/20051218/opinion/conserving-local-heritage-through-heritage-management.68715#.UmdsVdS4bVs Samantha Fabry' Conserving local heritage through heritage management ''Times of Malta'' December 18, 2005]</ref>

{{Infobox journal
| title = Historic Environment
| cover =
| editor = Tim Winter
| discipline = [[History]]
| former_names =
| abbreviation = Hist. Env.
| publisher = Australia ICOMOS
| country = Australia
| frequency = Triannually
| history = 1980-present
| openaccess =
| license =
| impact =
| impact-year =
| website = http://australia.icomos.org/publications/historic-environment
| link1 =http://catalogue.nla.gov.au/Record/166952?lookfor=historic%20environment&offset=1&max=3221
| link1-name =National Library Australia
| link2 =http://www.informit.com.au/AHB-journals.html
| link2-name =Australian Heritage Bibliography
| ISSN = 0726-6715
| eISSN =
| LCCN =
| OCLC =
}}

==''Historic Environment''==

Australia ICOMOS publishes ''[[Historic Environment]]'' (ISSN 0726-6715), a triannual [[Peer review|peer-reviewed]] [[academic journal]] covering [[cultural heritage]] and [[heritage conservation]],<ref name=icomos>[http://australia.icomos.org/publications/historic-environment/ Australia ICOMOS publications]</ref><ref name=AHB>[http://www.informit.com.au/AHB-journals.html Australian Heritage Bibliography, RMIT,  INFORMIT]</ref>  first published by the [[Council for the Historic Environment]], from 1980-1991, then by Australia ICOMOS and the Council for the Historic Environment in 1992 and by Australia ICOMOS alone from 1993.<ref>[http://trove.nla.gov.au/version/38988958  ''Historic Environment'' (Online) Australia ICOMOS. Published Carlton Vic. : Council for the Historic Environment, (Vic.), 1980-], [http://www.informit.com.au On line access] or [http://www.rmitpublishing.com.au on CD-ROM]</ref> Editions are often on a specific heritage and conservation theme, for example 'Canberra's 100th anniversary' in 2012.<ref name="canberra100.com.au"/> and 'Extreme Heritage'  dealing with ''... managing heritage in the face of climatic extremes, natural disasters and military conflicts in tropical, desert, polar and off-world landscapes.''<ref>[http://eprints.jcu.edu.au/19271/1/Editorial_mcintyre-tamwoy.pdf 'Extreme Heritage' ''Historic Environment'' Volume 23 number 2 2007]</ref>

The journal is the pre-eminent publication on heritage conservation in Australia, is allied to the international organisation ICOMOS, it is cited extensively in conservation literature, records the major heritage conferences in Australia, and has been in publication for over 30 years.<ref name="icomos"/><ref name="AHB"/> The journal aims to bring together ''… dynamic, critical interdisciplinary research in the field of cultural heritage and heritage conservation.''  The journal has an editorial committee of five with lead editor Dr Tim Winter in 2013. It was ranked ‘A’ by the [[Excellence in Research for Australia]] classification scheme.<ref name=icomos/><ref name="Researchers, ANU Research">[https://researchers.anu.edu.au/researchers/blair-sj Researchers, ANU Research]</ref>

Accessed on-line via [[State Library of New South Wales]],<ref>[http://library.sl.nsw.gov.au/search~S1?/tHistoria+y+sociedad+(Medell%7B226%7Din%2C+Colombia+%3A+On/thistoria+y+sociedad+medellin+colombia+online/-3%2C-1%2C0%2CB/frameset&FF=thistoric+environment+online&1%2C1%2C State Library of New South Wales on-line access]</ref> and indexed through various index services including [[RMIT]]'s Australian Heritage Bibliography,<ref name=AHB/> and the Australian Public Affairs Information Service (APAIS).<ref>[http://www.nla.gov.au.rp.nla.gov.au/apais/journals.html Australian Public Affairs Information Service (APAIS)]</ref> It is held in over 80 libraries worldwide including all of the Australian state libraries and major Australian and New Zealand university libraries.<ref>[http://www.worldcat.org/title/historic-environment/oclc/60616363&referer=brief_results OCLC World Cat]</ref> and was ranked ‘A’ by the Australian Research Council [[Excellence in Research for Australia]] classification scheme.<ref name=icomos/><ref name="Researchers, ANU Research"/> Australia ICOMOS also publishes the ''Australia ICOMOS newsletter'' (ISSN 0155-3534) on a regular basis.

==Mentoring==

Australia ICOMOS provides a mentoring program for cultural heritage students as well as architectural students who have completed subjects in architectural conservation<ref>[http://edsc.unimelb.edu.au/sites/default/files/docs/ICOMOS%20Mentoring%20application%20%26%20agreement%202013.pdf Australia ICOMOS mentoring scheme, University of Melbourne]</ref> and the Australia ICOMOS Victorian Scholarships.<ref>[http://www.historyatwork.com.au/documents/ICOMOS%20Scholarship.pdf Winners of the Inaugural Australia ICOMOS Victorian Scholarship 2008] {{webarchive |url=https://web.archive.org/web/20131022032436/http://www.historyatwork.com.au/documents/ICOMOS%20Scholarship.pdf |date=October 22, 2013 }}</ref> Australia ICOMOS projects extend to providing experise and fund-raising for restoration projects in the wider Asia and Pacific region, including the ''Streetwise Asia'' School Restoration Project in the Philippines.<ref>[http://australia.icomos.org/e-news/australia-icomos-e-mail-news-no-439/#2. ''Australia ICOMOS E-Mail News'' No. 439 Jun 04 2010 'Successful completion of Streetwise Asia School Restoration Project in the Philippines' & 'Fund Raising for Streetwise Asia Fund Philippines School Restoration project No 2 – 2010 – 2011']</ref> Australia ICOMOS, and in particular Dr Richard Mackay, have been involved in conservation management advice for the [[World Heritage List]]ing of [[Ankor Wat]] in Cambodia for many years.<ref>[http://openarchive.icomos.org/74/1/77-JUT6-142.pdf LIVING WITH HERITAGE AT ANGKOR, Prof Richard Mackay, AM & Sharon Sullivan]</ref>

==References==

{{Reflist}}

== External links ==
* {{Official website|http://australia.icomos.org/publications/historic-environment}}
* [http://eprints.jcu.edu.au/8081/ A volume revisited: a retrospective review of Historic Environment Volume 11 Nos. 2 & 3: managing a shared heritage (1995)] ''Australia ICOMOS E-Newsletter'', 243 . pp.&nbsp;3–5.

{{DEFAULTSORT:Australia ICOMOS}}
[[Category:Organizations established in 1976]]
[[Category:Heritage organizations]]
[[Category:Conservation and restoration organizations]]
[[Category:1976 establishments in Australia]]
[[Category:History journals]]
[[Category:Triannual journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1982]]
[[Category:Historiography of Australia]]
[[Category:Academic journals published by learned and professional societies]]