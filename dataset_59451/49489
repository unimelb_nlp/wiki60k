{{Distinguish|JavaScript}}
{{Infobox programming language
| name                   = JScript
| logo                   = [[File:Jscript icon.gif|32px]]<!--Native resolution-->
| file_ext               = .js, .jse, .wsf, .wsc (.htm, .html, .asp)<ref>{{cite web|url=https://msdn.microsoft.com/en-us/library/67w03h17(v=VS.85).aspx |title=Types of Script Files |publisher=Msdn.microsoft.com |date= |accessdate=2012-08-17}}</ref>
| paradigm               = 
| year                   = 1996
| designer               = 
| developer              = [[Microsoft]]
| latest_release_version = 9.0
| latest_release_date    = March 2011
| typing                 = [[Dynamic typing|Dynamic]], [[weak typing|weak]], [[duck typing|duck]]
| implementations        = [[Active Scripting]], [[JScript .NET]]
| dialects               =
| influenced by          =
| influenced             =
| operating system       = [[Microsoft Windows]]
| license                =
| website                = {{url|msdn.microsoft.com/library/hbxc2t98.aspx}}
}}

'''JScript''' is Microsoft's dialect of the [[ECMAScript]] standard<ref>{{cite web|url=https://msdn.microsoft.com/en-us/library/hbxc2t98(v=vs.85).aspx |title=JScript (ECMAScript3) |publisher=Msdn.microsoft.com |date= |accessdate=2012-08-17}}</ref> that is used in [[Microsoft]]'s [[Internet Explorer]].

JScript is implemented as an [[Active Scripting]] engine. This means that it can be "plugged in" to [[OLE Automation]] applications that support Active Scripting, such as [[Internet Explorer]], [[Active Server Pages]], and [[Windows Script Host]].<ref>{{cite web|url=https://msdn.microsoft.com/en-us/library/shzd7dy4(v=VS.85).aspx |title=What Is WSH? |publisher=Msdn.microsoft.com |date= |accessdate=2012-08-17}}</ref> It also means such applications can use multiple Active Scripting languages, e.g., JScript, [[VBScript]] or [[PerlScript]].

JScript was first supported in the [[Internet Explorer#Internet Explorer 3|Internet Explorer 3.0]] browser released in August 1996. Its most recent version is JScript 9.0, included in Internet Explorer 9.

JScript 10.0<ref>[https://msdn.microsoft.com/en-us/library/xkx7dfw1.aspx What is JScript 10.0?]</ref> is a separate dialect, also known as [[JScript .NET]], which adds several new features from the abandoned fourth edition of the ECMAScript standard. It must be compiled for [[.NET Framework]] version 2 or version 4, but static type annotations are optional.

==Comparison to JavaScript==
As explained by [[JavaScript]] guru [[Douglas Crockford]] in his talk titled ''The JavaScript Programming Language'' on [[YUI Theater]], {{quote|[Microsoft] did not want to deal with [[Sun Microsystems]] about the trademark issue, and so they called their implementation JScript. A lot of people think that JScript and JavaScript are different but similar languages. That's not the case. They are just different names for the same language, and the reason the names are different was to get around trademark issues.<ref>Douglas Crockford, ''The JavaScript Programming Language''</ref>}}

However, JScript supports [[Conditional comment#Conditional comments in JScript|conditional compilation]], which allows a programmer to selectively execute code within [[block comments]].  This is an extension to the [[ECMAScript]] standard that is not supported in other JavaScript implementations, thus making the above statement not completely true. However, conditional compilation is no longer supported in Internet Explorer 11 Standards mode.

==Versions==

===JScript===
The original JScript is an [[Active Scripting]] engine. Like other Active Scripting languages, it is built on the [[OLE Automation|COM/OLE Automation]] platform and provides scripting capabilities to host applications.

This is the version used when hosting JScript inside a Web page displayed by [[Internet Explorer]], in an [[HTA (programming language)|HTML application]], in [[Active Server Pages|classic ASP]], in [[Windows Script Host]] scripts and several other [[OLE Automation|Automation]] environments.

JScript is sometimes referred to as "classic JScript" or "Active Scripting JScript" to differentiate it from newer .NET-based versions.

Some versions of JScript are available for multiple versions of Internet Explorer and Windows.  For example, JScript 5.7 was introduced with [[Internet Explorer 7|Internet Explorer 7.0]] and is also installed for [[Internet Explorer 6|Internet Explorer 6.0]] with [[Windows XP]] [[XP service pack 3|Service Pack 3]], while JScript 5.8 was introduced with [[Internet Explorer 8|Internet Explorer 8.0]] and is also installed with Internet Explorer 6.0 on [[Windows Mobile#Windows Mobile 6.5|Windows Mobile 6.5]].

Microsoft's implementation of ECMAScript 5th Edition in [[Windows 8#Consumer Preview|Windows 8 Consumer Preview]] is called ''JavaScript'' and the corresponding [[Visual Studio]] 11 Express Beta includes a “completely new”, full-featured JavaScript editor with [[IntelliSense]] enhancements for [[HTML5]] and [[ECMAScript 5]] syntax, “VSDOC” annotations for multiple overloads, simplified [[Document Object Model|DOM]] configuration, brace matching, collapsible outlining and “go to definition”.<ref name="vs11beta">{{cite web |url=http://www.asp.net/vnext/overview/whitepapers/whats-new#_Toc318097407 |title=What's New in ASP.NET 4.5 and Visual Web Developer 11 Beta: The Official Microsoft ASP.NET Site }}<br>{{cite web |url=https://msdn.microsoft.com/en-us/library/hh420390(v=vs.110).aspx#javascript_editor |title=What’s New for ASP.NET 4.5 and Web Development in Visual Studio 11 Beta }}</ref>

{| class="wikitable" style="text-align: center;"
|-
! Version
! Date
! Introduced with<ref>{{citation |url=https://msdn.microsoft.com/en-us/library/s4esdbwz.aspx |title=Version Information (Windows Scripting - JScript) |publisher=Microsoft |accessdate=2010-05-31}}</ref>
! Based on{{refn|JScript supports various features not specified in the ECMA standard,<ref>{{citation |url=https://msdn.microsoft.com/en-us/library/4tc5a343.aspx |title=Microsoft JScript Features - Non-ECMA (Windows Scripting - JScript) |publisher=Microsoft |accessdate=2010-05-31}}</ref> as does JavaScript.|group=note}}
! Similar [[JavaScript]] version
|-
| 1.0
| Aug 1996
| [[Internet Explorer 3|Internet Explorer 3.0]]
| Netscape JavaScript
| 1.0
|-
| 2.0
| Jan 1997
| [[Internet Information Services|Windows IIS]] 3.0
| Netscape JavaScript
| 1.1
|-
| 3.0
| Oct 1997
| [[Internet Explorer 4.0]]
| ECMA-262 1st edition{{refn|Microsoft said JScript 3.0 was "the first scripting language to fully conform to the ECMA-262 standard".<ref>{{citation |url=http://www.microsoft.com/presspass/press/1997/Jun97/jecmapr.mspx |title=Microsoft Embraces ECMA Internet Scripting Standard; Delivers Industry's First ECMA-Compliant Scripting Language, JScript 3.0, In Key Microsoft Products |publisher=Microsoft |date=1997-06-30}}</ref>|group=note}}
| 1.3
|-
| 4.0
| 
| [[Microsoft Visual Studio|Visual Studio]] 6.0 (as part of [[Visual InterDev]])
| ECMA-262 1st edition
| 1.3
|-
| 5.0
| Mar 1999
| [[Internet Explorer 5.0]]
| ECMA-262 2nd edition
| 1.4
|-
| 5.1
|
| Internet Explorer 5.01
| ECMA-262 2nd edition
| 1.4
|-
| 5.5
| Jul 2000
| Internet Explorer 5.5 & [[Windows CE]] 4.2
| ECMA-262 3rd edition
| 1.5
|-
| 5.6
| Oct 2001
| [[Internet Explorer 6|Internet Explorer 6.0]] & [[Windows CE]] 5.0
| ECMA-262 3rd edition
| 1.5
|-
| 5.7
| Nov 2006
| [[Internet Explorer 7|Internet Explorer 7.0]]
| ECMA-262 3rd edition + ECMA-327 (ES-CP){{refn|JScript 5.7 includes an implementation of the ECMAScript Compact Profile (ECMA-327) which turns off features not required by the ES-CP when using the "JScript.Compact" ProgID.{{Citation needed|date=May 2010}}|group=note}}
| 1.5
|-
| 5.8
| Mar 2009
| [[Internet Explorer 8|Internet Explorer 8.0]] & [[Internet Explorer Mobile|Internet Explorer Mobile 6.0]]
| ECMA-262 3rd edition + ECMA-327 (ES-CP) + [[JSON]] (RFC 4627)<sup id="fn_3_back">[[#fn 3|3]]</sup>
| 1.5
|-
| 9.0
| Mar 2011
| [[Internet Explorer 9|Internet Explorer 9.0]] (64-bit)
| ECMA-262 5th edition
| 1.8.1
|}

JScript is also available on Windows CE (included in Windows Mobile, optional in Windows Embedded CE). The Windows CE version lacks Active Debugging.

===JScript .NET===
{{Main|JScript .NET}}
JScript .NET is a [[Microsoft .NET]] implementation of JScript. It is a [[Common Language Specification|CLS]] language and thus inherits very powerful features, but lacks many features of the original JScript language, making it inappropriate for many scripting scenarios.
JScript .NET can be used for [[ASP.NET]] pages and for complete .NET applications, but the lack of support for this language in Microsoft Visual Studio places it more as an upgrade path for classic [[Active Server Pages|ASP]] using classic JScript than as a new first-class language.

{| class="wikitable" style="text-align: center;"
|-
! Version
! Platform
! Date
! Introduced with
! Based on
|-
| 7.0 || Desktop [[Common Language Runtime|CLR]] 1.0 || 2002-01-05 || [[Microsoft .NET Framework]] 1.0 || ECMA-262 3rd edition{{refn|JScript .NET is "being developed in conjunction with ECMAScript Edition 4".<ref>{{citation |url=https://msdn.microsoft.com/en-us/library/xkx7dfw1(VS.71).aspx |title=What Is JScript .NET? |publisher=Microsoft}}</ref>|group=note|name=dotnet}}
|-
| 7.1 || Desktop [[Common Language Runtime|CLR]] 1.1 || 2003-04-01 || [[Microsoft .NET Framework]] 1.1 || ECMA-262 3rd edition<ref group="note" name="dotnet" />
|-
| 8.0 || Desktop [[Common Language Runtime|CLR]] 2.0 || 2005-11-07 || [[Microsoft .NET Framework]] 2.0 || ECMA-262 3rd edition<ref group="note" name="dotnet" />
|-
|10.0 || Desktop [[Common Language Runtime|CLR]] 4.0 || 2010-08-03 || [[Microsoft .NET Framework]] 4.0 || ECMA-262 3rd edition<ref group="note" name="dotnet" />
|}

JScript .NET is not supported in the [[.NET Compact Framework]].{{citation needed|date=March 2012}}

Note: JScript .NET versions are not related to classic JScript versions. JScript .NET is a separate product. Even though JScript .NET is not supported within the Visual Studio IDE, its versions are in sync with other .NET languages versions ([[C Sharp (programming language)|C#]], [[VB.NET]], [[VC++]]) that follow their corresponding Visual Studio versions.

.NET Framework 3.0 and 3.5 are built on top of 2.0 and do not include the newer JScript.NET release (version 10.0 for .NET Framework 4.0).

(Source: file version of jsc.exe JScript.NET compiler and Microsoft.JScript.dll installed with .NET Framework)

==See also==
*[[JScript.Encode]]
*[[Windows Script File]]
*[[Windows Script Host]]

==Notes==
{{Reflist | group=note}}

==References==
{{Reflist}}

==External links==
{{wikibooks|Computer Programming/Hello world}}
* [http://msdn2.microsoft.com/en-us/library/hbxc2t98.aspx JScript documentation in the MSDN Library]
* [http://download.microsoft.com/download/f/f/e/ffea3abf-b55f-4924-b5a5-bde0805ad67c/Windows%20Script%20Release%20Notes.rtf JScript 5.7 Release Notes]
* [http://msdn2.microsoft.com/en-us/library/72bd815a(vs.71).aspx JScript .NET documentation in the MSDN Library]
* [http://blogs.msdn.com/jscript JScript blog]
* [http://www.webmasterworld.com/forum91/68.htm JavaScript - JScript - ECMAScript version history]
* [http://www.jPaq.org/ jPaq - A Fully Customizable JScript Library]

{{ECMAScript}}
{{Microsoft APIs}}
{{Internet Explorer}}
{{Windows Components}}

{{DEFAULTSORT:Jscript}}
[[Category:Internet Explorer]]
[[Category:JavaScript dialect engines]]
[[Category:JavaScript programming language family]]
[[Category:Object-based programming languages]]
[[Category:Prototype-based programming languages]]
[[Category:Scripting languages]]