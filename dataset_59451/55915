{{Infobox Journal
| title =       Biometrics
| cover = [[File:Biometricscover.png]]
| editor = Marie Davidian
| abbreviation = Biometrics
|frequency = Quarterly
| impact = 1.827
| impact-year = 2011
| discipline = [[Statistics]]
| website = http://www.biometrics.tibs.org/
| link1 = http://www.wiley.com/bw/journal.asp?ref=0006-341X
| link1-name = Journal page at publisher's website
| link2 = http://www3.interscience.wiley.com/journal/118538342/home
| link2-name = Online access
| link3 = http://www3.interscience.wiley.com/journal/118538342/toc
| link3-name = Online archive 
| publisher = [[International Biometric Society]], [[John Wiley & Sons|Wiley-Blackwell]]
| country = [[United Kingdom]]
| history = 1945–present
| CODEN = BIOMB6
| LCCN = 49001784
| ISSN = 0006-341X
| eISSN = 1541-0420
| JSTOR = 0006341X
| OCLC = 5898885
| RSS = http://www3.interscience.wiley.com/rss/journal/118538342
}}
'''''Biometrics''''' is a journal that publishes articles on the application of statistics and mathematics to the biological sciences.<ref name="JSTOR">[http://www.jstor.org/journal/biometrics JSTOR entry on biometrics]</ref> It is published by the International Biometric Society.<ref name="IBS">[http://www.biometrics.tibs.org/ Biometrics homepage] {{webarchive |url=https://web.archive.org/web/20070625090905/http://www.biometrics.tibs.org/ |date=June 25, 2007 }}</ref>
Originally published in 1945<ref>[http://www.jstor.org/journals/00994987.html Biometrics Bulletin JSTOR Coverage]</ref> under the title ''Biometrics Bulletin'', the journal adopted the shorter title in 1947.<ref name="BiometricsV3I1">[http://www.jstor.org/stable/3001538  Biometrics, Vol. 3, No. 1, Mar., 1947 Page 53]</ref> A notable contributor to the journal was [[Ronald Fisher|R.A. Fisher]], for whom a memorial edition was published in 1964.<ref name="BiometricsV20I2">[http://www.jstor.org/stable/i343394 Biometrics, Vol. 20, No. 2, In Memoriam: Ronald Aylmer Fisher, 1890-1962, Jun., 1964]</ref> In a recent survey of statistics researchers' opinions, it was ranked fifth overall among 40 statistics journals, and it was second only to the ''[[Journal of the American Statistical Association]]'' in the ranking provided by [[biometrics]] specialists.<ref>{{citation|first1=Vasilis|last1=Theoharakis|first2=Mary|last2=Skordia|journal=The American Statistician|volume=57|year=2003|title=How do statisticians perceive statistics journals?|url=http://pubs.amstat.org/doi/abs/10.1198/0003130031414|doi=10.1198/0003130031414|pages=115|format=}}.</ref>

== References ==
<references/>

== External links ==
* {{Official website|https://web.archive.org/web/20070625090905/http://www.biometrics.tibs.org:80/}}
* [https://web.archive.org/web/20070625090905/http://www.biometrics.tibs.org:80/ International Biometrics Society]
* [https://web.archive.org/web/20080221101324/http://www.tibs.org:80/WorkArea/showcontent.aspx?id=632 Biometry by R.A. Fisher]

{{Statistics journals}}

[[Category:Biostatistics journals]]
[[Category:Publications established in 1945]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]