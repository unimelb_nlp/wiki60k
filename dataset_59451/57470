{{Infobox journal
| title = Integrative Cancer Therapies
| cover = [[File:Integrative Cancer Therapies.tif]]
| editor = [[Keith I. Block]]
| discipline = [[Oncology]]
| formernames = 
| abbreviation = Integr. Cancer. Ther.
| publisher = [[Sage Publications]]
| country = 
| frequency = Quarterly
| history = 2002-present
| openaccess = 
| license = 
| impact = 2.361
| impact-year = 2014
| website = http://www.sagepub.com/journals/Journal201510/title
| link1 = http://ict.sagepub.com/current.dtl
| link1-name = Online access
| link2 = http://ict.sagepub.com/
| link2-name = Online archive
| JSTOR = 
| OCLC = 46736454
| LCCN = 2003209158
| CODEN = 
| ISSN = 1534-7354
| eISSN = 1552-695X
}}
'''''Integrative Cancer Therapies''''' is a [[peer-reviewed]] [[medical journal]] focusing on [[alternative medicine|complementary and alternative and integrative medicine]] in the care for and treatment of patients with [[cancer]]. Therapies like [[diet (nutrition)|diet]]s and lifestyle modifications, as well as experimental [[vaccine]]s and [[chemotherapy]] are the subject of this journal. It was established in 2002 and is published by [[Sage Publications]]. The [[editor-in-chief]] is Keith I. Block ([[University of Illinois at Chicago]]).

A study published in the March 2006 issue, ''Diagnostic Accuracy of Canine Scent Detection in Early-and Late-Stage Lung and Breast Cancers'' by McCulloch ''et al.'', garnered widespread media attention.<ref>{{Cite web |last= |first= |authorlink= |coauthors= |title=Can Dogs Smell Cancer? |work=ScienceDaily |publisher= |date=2006-01-06 |url=http://www.sciencedaily.com/releases/2006/01/060106002944.htm |doi= |accessdate=2010-01-16}}</ref><ref>{{Cite web |last= |first= |authorlink= |coauthors= |title=Can Dogs Sniff Out Cancer? Studies Show They Probably Can |work=Medical News Today |publisher= |date=2006-01-07 |url=http://www.medicalnewstoday.com/articles/35857.php |format= |doi= |accessdate=2010-01-16}}</ref> The study presented evidence that a dog's scenting ability can distinguish people with both early and late stage lung and breast cancers from healthy controls.<ref>{{Citation |first=Michael |last=McCullochb|first2=Tadeusz |last2=Jezierski  |first3=Michael |last3=Broffman |first4=Alan |last4=Hubbard |first5=Kirk |last5=Turner |first6=Teresa |last6=Janecki |title=Diagnostic Accuracy of Canine Scent Detection in Early- and Late-Stage Lung and Breast Cancers |year=2006 |url=http://ict.sagepub.com/cgi/reprint/5/1/30 | doi=10.1177/1534735405285096 |journal=Integr. Cancer Ther.  |volume=5 |issue=1  |pages=30–39  |date= |PMID=16484712 |id=}}
</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2014 [[impact factor]] is 2.361, ranking it 5th out of 24 journals in the category "Integrative & Complementary Medicine"<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Integrative & Complementary Medicine |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Social Sciences |work=[[Web of Science]] |postscript=.}}</ref> and 131st out of 211 journals in the category "Oncology".<ref name=WoS1>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Oncology |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition= Sciences |work=[[Web of Science]] |postscript=.}}</ref>

==References==
<references/>

== External links ==
* {{Official website|http://www.sagepub.com/journals/Journal201510/title}}

[[Category:Oncology journals]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 2002]]
[[Category:Alternative and traditional medicine journals]]
[[Category:SAGE Publications academic journals]]

{{med-journal-stub}}