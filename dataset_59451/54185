{{Use dmy dates|date=September 2014}}
{{Use Australian English|date=September 2014}}
{{Infobox Australian place | type = suburb
| name      = Eagle On The Hill
| state     = sa
| image     = Devil's Elbow.JPG
| caption   = Eagle On The Hill is directly above the tunnel entrance, with the old road winding round from Devils Elbow
| lga       = Adelaide Hills Council
| postcode  = 
| est       = 1850
| pop       = 
| elevation = 
| maxtemp   = 
| mintemp   = 
| rainfall  = 
| stategov  = [[electoral district of Heysen|Heysen]]
| fedgov    = [[Division of Mayo|Mayo]]
| dist1     = 
| location1 = 
}}
'''Eagle On The Hill''' is an unbounded locality of [[Adelaide]] in the [[Adelaide Hills|Adelaide Foothills]]. It borders [[Mount Osmond, South Australia|Mount Osmond]] and [[Waterfall Gully, South Australia|Waterfall Gully]].

The village is located on [[Mount Barker Road]], which was formerly the connection from Adelaide to the [[South Eastern Freeway]].  Once the freeway was extended through the [[Heysen Tunnels]] in 2000, through the ridge underneath Eagle On The Hill, the locality went into a precipitous decline.  It is now a relic of its former self; its restaurant, hotel, bottle shop and service stations have all closed, leaving behind a quiet suburb with a small resident population. Eagle On The Hill is now a very popular spot for downhill speedboarders who travel down the road at speeds close to 65 kilometres per hour.

==Hotel==
[[File:Eagle on the hill.jpg|thumb|left|The now-closed Eagle on the Hill Hotel]]
The Eagle on the Hill Hotel was first opened in 1853 by William Anderson, who named it ''Anderson Hotel''. The hotel went through further name changes in 1855 to ''The Eagle on the Hill'', then to ''Eagle's Nest'' in 1859 and back to ''Eagle on the Hill'' in 1860.

The hotel has twice been ravaged by bushfires. First in 1899; a fire destroyed all of the old hotel with the exception of two rooms. Again on [[Ash Wednesday fires|Ash Wednesday]] (16 February 1983) the hotel suffered, with it being completely destroyed. It was rebuilt and operated with much success until its closure after the construction of the new freeway route.

==External links==
*[http://www.slsa.sa.gov.au/manning/pn/e/e1.htm History of Eagle on the Hill]

{{Adelaide Hills Council suburbs}}
{{Adelaide Hills}}

{{coord|34.9766|S|138.6737|E|format=dms|display=title|region:AU-SA_type:city}}
{{refimprove|date=January 2008}}

[[Category:Adelaide]]


{{Adelaide-geo-stub}}