{{one source|date=January 2015}}
{{Infobox journal
| title = Journal of World-Systems Research
| cover = 
| editor = Jackie Smith (sociologist)
| discipline = [[Sociology]]
| former_names = 
| abbreviation = J. World Syst. Res.
| publisher = University Library System, University of Pittsburgh
| country = 
| frequency = Biannual
| history = 1995-present
| openaccess = Yes
| license = 
| impact = 
| impact-year = 
| website = http://www.jwsr.org/
| link1 = 
| link1-name = 
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 782887960
| LCCN = sn94005097
| CODEN = 
| ISSN = 1076-156X
| eISSN = 
}}
The '''''Journal of World-Systems Research''''' ('''''JWSR''''') is a biannual, [[open access]], [[Peer review|peer-reviewed]] [[academic journal]] in the field of [[World-systems theory|world-systems analysis]], established in 1995 by founding editor [[Christopher Chase-Dunn]] at the Institute for World-System Research at the University of California at Riverside.<ref>{{Cite web|url=http://irows.ucr.edu/|title=Welcome to IROWS|website=irows.ucr.edu|access-date=2016-10-31}}</ref> As of 2015, it is published by the Political Economy of the World-System (PEWS) Section of the [[American Sociological Association]] and  by the [[D-Scribe Digital Publishing|University Library System, University of Pittsburgh]].<ref>{{Cite journal|last=Smith|first=Jackie|last2=Bair|first2=Jennifer|last3=Byrd|first3=Scott|date=2015-08-31|title=Editors' Introduction|url=http://jwsr.pitt.edu/ojs/index.php/jwsr/article/view/28|journal=Journal of World-Systems Research|language=en|volume=21|issue=2|pages=238–240|doi=10.5195/jwsr.2015.28|issn=1076-156X}}</ref> The journal's current [[editor-in-chief]] is [[Jackie Smith (sociologist)|Jackie Smith]] ([[University of Pittsburgh]]).

The journal was one of the first online, [[peer-review]]ed academic journals, published originally as an online archive of scholarly papers accessed using the [[Gopher (protocol)]].{{citation needed|date=January 2015}}

The journal describes its purpose as being: {{cquote|to produce a high quality publication of world-systems research articles; to publish quantitative and comparative research on world-systems; to publish works of theory construction and codification of causal propositions; to publish data sets in connection with articles; to publish reviews of books relevant to world-systems studies; and to encourage authors to use the hypermedia advantages of electronic publication to present their research results.<ref>[http://www.jwsr.org/editorial-policy "Editorial Policy,"] JWSR website. Accessed: April 4, 2013.</ref>}}

== References ==

{{reflist}}

==External links==
*{{Official website|http://jwsr.pitt.edu/ojs/index.php/jwsr/}}

[[Category:Sociology journals]]
[[Category:Publications established in 1995]]
[[Category:English-language journals]]
[[Category:Biannual journals]]
[[Category:Globalization-related journals]]
[[Category:World systems theory]]

{{social-science-journal-stub}}
{{globalization-stub}}
[[Category:Open access journals]]