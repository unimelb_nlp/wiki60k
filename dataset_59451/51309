{{Infobox journal
| title = Extrapolation 
| cover =
| editor = Javier A. Martinez, Andrew M. Butler, Michael Levy, Wendy Pearson, John Rieder
| discipline = [[Speculative fiction]]
| former_names = 
| abbreviation = 
| publisher = [[Liverpool University Press]]
| country = 
| frequency = Triannual
| history = 1959-present
| openaccess = 
| license = 
| impact = 
| impact-year = 
| website = http://online.liverpooluniversitypress.co.uk/loi/extr
| link1 = http://online.liverpooluniversitypress.co.uk/toc/extr/current
| link1-name = Online access
| link2 = http://online.liverpooluniversitypress.co.uk/action/doSearch
| link2-name = Online archive
| JSTOR = 
| OCLC = 1568678
| LCCN = 72206280
| CODEN = 
| ISSN = 0014-5483
| eISSN = 2047-7708
}}
'''''Extrapolation''''' is an [[academic journal]] covering [[speculative fiction]]. It was established in 1959 by Thomas D. Clareson and was published at the [[College of Wooster]]. In 1979 it moved to the [[Kent State University]] Press. A decade later, Clareson stepped down as [[editor-in-chief]] and was succeeded by Donald M. Hassler of the KSU English Department. In 2002 the journal was transferred to the [[University of Texas at Brownsville and Texas Southmost College|University of Texas at Brownsville]].<ref>[http://www.worldcat.org/title/extrapolation-university-of-texas-at-brownsville/oclc/746937022 "Extrapolation (University of Texas at Brownsville)"]</ref> At that time Donald M. Hassler became executive editor, and the position of editor was filled by Javier A. Martinez of UTB/TSC's Department of English. In 2007, Hassler retired and the current editors are Martinez, Andrew M. Butler ([[Canterbury Christ Church University]]), Michael Levy ([[University of Wisconsin-Stout]]), Gerry Canavan ([[Marquette University]]), Rachel Haywood-Ferreira ([[Iowa State University]]) and John Rieder ([[University of Hawaii]]). The reviews editor is [[D. Harlan Wilson]] ([[Wright State University]]).

==History==
''Extrapolation'' was the first journal to publish academic work on [[science fiction]] and fantasy.{{citation needed|date=May 2012}} It covers all areas of speculative culture, including print, film, television, comic books, and video games, and particularly encourages papers which consider popular texts within their larger cultural context. The journal publishes papers from a wide variety of critical approaches including literary criticism, Utopian studies, genre criticism, feminist theory, critical race studies, queer theory, and postcolonial theory. It is interested in promoting dialogue among scholars working within a number of traditions and in encouraging the serious study of popular culture.

''Extrapolation'' appears three times a year.

== See also ==
*''[[Feminist science fiction#Femspec|Femspec]]''
*''[[Foundation – The International Review of Science Fiction]]''
*''[[Science Fiction Studies]]''

==References==
{{Reflist}}

== Further reading ==
* Peña, Adrian. [http://blue.utb.edu/collegian/2005/02/Feb%2028/on_camp7.htm Sci-fi and Spanish literature publications call UTB/TSC home] in ''The Collegian Online'', Vol. 57, Issue 22, Feb. 28, 2005.

== External links ==
* {{official website|http://online.liverpooluniversitypress.co.uk/loi/extr}}

[[Category:Triannual journals]]
[[Category:Science fiction and fantasy journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1959]]
[[Category:University of Liverpool]]