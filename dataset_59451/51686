{{Infobox journal
| title = Investigative Radiology
| editor = [[Val Murray Runge]]
| discipline = [[Radiology]]
| formernames =
| abbreviation = Invest. Radiol.
| publisher =[[Lippincott Williams & Wilkins]]
| country =
| frequency = Monthly
| history = 1966-present
| openaccess =
| license =
| impact = 4.887
| impact-year = 2015
| website = http://journals.lww.com/investigativeradiology/pages/default.aspx
| link1 = http://journals.lww.com/investigativeradiology/pages/currenttoc.aspx
| link1-name = Online access
| link2 = http://journals.lww.com/investigativeradiology/pages/issuelist.aspx
| link2-name = Online archive
| ISSN = 0020-9996
| eISSN = 1536-0210
| OCLC = 730022217
| CODEN = INVRAV
}}
'''''Investigative Radiology''''' is a monthly [[peer-reviewed]] [[medical journal]] published by [[Lippincott Williams & Wilkins]]. It covers research on [[diagnostic imaging]], focusing on [[magnetic resonance imaging|magnetic resonance]], [[computed tomography]], [[ultrasound]], [[digital subtraction angiography]], and new technologies. An additional focus is that of contrast media research, primarily for diagnostic imaging but also in the field of [[theranostics]]. The [[editor-in-chief]] is [[Val Murray Runge|Val M. Runge]] ([[Inselspital, Universitätsspital Bern]]).<ref>{{cite web |url=http://journals.lww.com/investigativeradiology/pages/editorialboard.aspx |title=Editorial Board |work=Investigative Radiology |publisher=Lippincott Williams & Wilkins |accessdate=2016-06-18}}</ref>

== History ==
The journal was established in 1966 with [[S. David Rockoff]] as the founding editor until 1976. Other editors have been [[Richard H. Greenspan]] (1976-1984), [[Charles E. Putman]] (1984-1989), and [[Bruce J. Hillman]] (1990-1994). Val M. Runge has served as editor since 1994.<ref>{{cite journal |last1=Rockoff |first1=SD |title=The founding and the early years of Investigative Radiology |journal=Invest. Radiol. |date=1990 |volume=25 |issue=4 |page=313-317 |accessdate=9 February 2015}}</ref>

The journal was originally published by J.B. Lippincott (now [[Lippincott Williams & Wilkins]]). Between 1966-1984 the journal appeared bimonthly but with the growth of [[radiology]] as a field, the frequency was increased to 9 issues in 1985, and to monthly in 1986.<ref>{{cite journal |last1=Hillman |first1=BJ |title=Twenty-five years of the best of Investigative Radiology |journal=Invest. Radiol. |date=1990 |volume=25 |issue=4 |page=347-350 |accessdate=9 February 2015}}</ref> Each year, one or more special issues are published focusing on a single topic.

== Abstracting and indexing ==
The journal is abstracted and indexed in the [[Science Citation Index]], [[Current Contents]]/Clinical Medicine, Current Contents/Life Sciences, [[BIOSIS Previews]],<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2014-03-04}}</ref> and [[Index Medicus]]/[[MEDLINE]]/[[PubMed]].<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/45377 |title=Investigative Radiology |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |format= |accessdate=2014-03-04}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 4.887, ranking it 9th out of 124 journals in the category "Radiology, Nuclear Medicine, and Medical Imaging".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Radiology, Nuclear Medicine, and Medical Imaging |title=2015 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://journals.lww.com/investigativeradiology/pages/default.aspx}}

[[Category:Radiology and medical imaging journals]]
[[Category:Monthly journals]]
[[Category:Lippincott Williams & Wilkins academic journals]]
[[Category:Publications established in 1966]]
[[Category:English-language journals]]