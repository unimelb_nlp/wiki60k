{{Infobox journal
| title = Journal of Proteome Research
| cover = [[File:Jpr cover.jpg|150 px]]
| editor = [[John R. Yates]]
| discipline = [[Proteomics]]
| abbreviation = J. Proteome Res.
| publisher = [[American Chemical Society]]
| country = United States
| frequency = Monthly
| history = 2002–present
| openaccess =
| license =
| impact = 4.173<ref>http://pubs.acs.org/page/jprobs/about.html</ref>
| impact-year = 2015
| website = http://pubs.acs.org/journal/jprobs
| link1 =
| link1-name =
| link2 =
| link2-name =
| JSTOR =
| OCLC = 47082747
| LCCN = 2001211122
| CODEN = JPROBS
| ISSN = 1535-3893
| eISSN = 1535-3907
}}
The '''''Journal of Proteome Research''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] published since 2002 by the [[American Chemical Society]].<ref>[http://pubs.acs.org American Chemical Society]</ref> Its publication frequency switched from bimonthly to monthly in 2006. The current [[editor-in-chief]] is [[John R. Yates]].<ref>[http://pubs.acs.org/page/jprobs/profile.html Editor profile]</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in [[EBSCO Publishing|EBSCOhost]], [[PubMed]], [[Scopus]], and the [[Web of Science]]. In 2013, ''J. Proteome Res.'' had an [[impact factor]] of 5.001 as reported in the 2013 Journal Citation Reports by Thomson Reuters, ranking it 10th out of 75 journals in the category “Biochemical Research Methods”.<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Biochemical Research Methods |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition= Sciences |accessdate= |work=Web of Science}}</ref> The impact factor in 2015 was 4.173.<ref>http://pubs.acs.org/page/jprobs/about.html</ref>


Google scholar lists the journal as 2nd in the category "Proteomics, Peptides & Aminoacids" <ref>https://scholar.google.ch/citations?view_op=top_venues&hl=en&vq=bio_proteomicspeptides</ref> with an h5-index of 67. The most cited papers in the last 5 years are "Andromeda: A Peptide Search Engine Integrated into the MaxQuant Environment" (Cox et al, 2011), "More than 100,000 Detectable Peptide Species Elute in Single Shotgun Proteomics Runs but the Majority is Inaccessible to Data-Dependent LC− MS/MS" (Michalski et al, 2011) and "Universal and Confident Phosphorylation Site Localization Using phosphoRS" (Taus et al, 2011).

== References ==
{{Reflist}}

== External links ==
* {{Official|http://pubs.acs.org/journal/jprobs}}

{{DEFAULTSORT:Journal Of Proteome Research}}
[[Category:American Chemical Society academic journals]]
[[Category:Bimonthly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 2002]]
[[Category:Proteomics journals]]