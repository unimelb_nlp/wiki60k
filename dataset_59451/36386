{{Other uses2|Andy Griffith}}
{{good article}}
{{Use mdy dates|date=June 2013}}
{{Infobox television
 | image       = Andy Griffith Show Opening.ogg
 | caption     = Opening sequence including<br>"The Fishin' Hole"
 | genre       = [[Sitcom]]
 | runtime     = 25–26 minutes
 | executive_producer = [[Sheldon Leonard]]<br/>[[Danny Thomas]]
 | creator     = [[Sheldon Leonard]]
 | starring    = [[Andy Griffith]]<br>[[Ron Howard|Ronny Howard]]<br>[[Don Knotts]]<br>[[Frances Bavier]]
 | opentheme   = "The Fishin' Hole"
 | theme_music_composer = [[Earle Hagen]] and [[Herbert W. Spencer]]
 | location    = [[RKO Forty Acres|Desilu Culver]] <small>(1960–67)</small><br>[[Paramount Pictures|Paramount Studios]] <small>(1967–68)</small>
 | country     = United States
 | language    = English
 | network     = [[CBS]]
 | company     = [[Danny Thomas|Danny Thomas Enterprises]]<br>Mayberry Enterprises
 | distributor = CBS Films (before 1971)<br>[[Viacom Enterprises]] (1971–95)<br>[[Paramount Domestic Television]] (1995–2006)<br>[[CBS Paramount Domestic Television]] (2006–07)<br>[[CBS Television Distribution]] (2007–present)
 | camera      = [[Single-camera setup|Single-camera]]
 | picture_format = [[Black-and-white]] <small>(1960–65)</small> <br>[[Color]] <small>(1965–68)</small>
 | audio_format = [[Monaural]]
 | first_aired = {{start date|1960|10|03}}
 | last_aired  = {{end date|1968|04|01}}
| num_seasons = 8
 | num_episodes = 249
 | list_episodes = List of The Andy Griffith Show episodes
 | related     = ''[[The Danny Thomas Show]]''<br>''[[Gomer Pyle, U.S.M.C.]]''
 | followed_by = ''[[Mayberry R.F.D.]]'' }}

'''''The Andy Griffith Show''''' is<!-- Do NOT change to "was" per WP:TVLEAD--> an American [[situation comedy]] which aired on [[CBS]] from October 3, 1960 to April 1, 1968, with a total of 249 half-hour episodes spanning over eight seasons, first in [[black and white]] and then in [[color]], which partially originated from an episode of ''[[The Danny Thomas Show]]''. It stars [[Andy Griffith]], who portrays the widowed sheriff of the fictional small community of [[Mayberry]], [[North Carolina]]. His life is complicated by an inept but well-meaning deputy, Barney Fife ([[Don Knotts]]), Aunt Bee ([[Frances Bavier]]), a spinster aunt and housekeeper, and Opie ([[Ron Howard]]), a precocious young son. Eccentric townspeople and temperamental girlfriends complete the cast. Regarding the tone of the show, Griffith said that despite a contemporary setting, the show evoked nostalgia, stating in a ''[[Today (U.S. TV program)|Today Show]]'' interview: "Well, though we never said it, and though it was shot in the 1960's, it had a feeling of the 1930's. It was, when we were doing it, of a time gone by."<ref>{{cite web|url=https://www.youtube.com/watch?v=3ahbyf9cbEU|title=Andy Griffith & Don Knotts on The Today Show|accessdate=September 10, 2012|date=March 4, 1996|publisher=NBC Today Show}}</ref>

The series never placed lower than seventh in the [[Nielsen ratings]] and ended its final season at number one. On separate occasions, it has been ranked by ''[[TV Guide]]'' as the 9th-best and 13th-best show in American [[television history]].<ref name=Top50Shows>{{cite news|url=http://www.cbsnews.com/2100-207_162-507388.html|title=TV Guide Names Top 50 Shows|agency=Associated Press|date=February 11, 2009|accessdate=July 5, 2012}}</ref><ref name="Fretts">{{cite journal |last=Fretts |first=Bruce |last2=Roush |first2=Matt |date= |title=The Greatest Shows on Earth |url= |journal=TV Guide Magazine |publisher= |volume=61 |issue=3194–3195 |pages=16–19 |doi= <!--|accessdate=22 December 2013 -->}}</ref> Though neither Griffith nor the show won awards during its eight-season run, co-stars Knotts and Bavier accumulated a combined total of six [[Emmy Awards]].   The series spawned its own spinoff, ''[[Gomer Pyle, U.S.M.C.]]'' (1964), a sequel series, ''[[Mayberry R.F.D.]]'' (1968), and a reunion [[telemovie]], ''[[Return to Mayberry]]'' (1986). Reruns of the show are often aired on [[TV Land]], [[MeTV]], and [[SundanceTV]], while the complete series is available on DVD. The show has also been made available on streaming video services such as [[Netflix]]. An annual festival celebrating the show, Mayberry Days, is held each year in Griffith's hometown of [[Mount Airy, North Carolina]].<ref>{{cite web|url=http://www.surryarts.org/mayberrydays/index.htmltitle| title= Mayberry Days| website=The Surry Arts Council Presents Mayberry Days}}</ref>

==Origin==
[[Sheldon Leonard]], producer of ''The Danny Thomas Show,'' and [[Danny Thomas]] hired veteran comedy writer Arthur Stander (who had written many of the "Danny Thomas" episodes) to create a pilot show for Andy Griffith, featuring him as justice of the peace and newspaper editor in a small town.<ref name="Kelly">Kelly, Richard. ''The Andy Griffith Show''. Blair, 1981.</ref>  At the time, Broadway, film, and radio star Griffith was interested in attempting a television role, and the [[William Morris Agency]] told Leonard that Griffith's rural background and previous rustic characterizations were suited to the part.<ref name="Kelly" /> After conferences between Leonard and Griffith in New York, Griffith flew to Los Angeles and filmed the episode.<ref name="Kelly" />  On February 15, 1960, ''The Danny Thomas Show'' episode "Danny Meets Andy Griffith" aired.<ref name="Kelly" />  In the episode Griffith played fictional Sheriff Andy Taylor of Mayberry, North Carolina, who arrests Danny Williams (Thomas's character) for running a stop sign.  Future players in ''The Andy Griffith Show'', Frances Bavier and Ron Howard, appeared in the episode as townspeople Henrietta Perkins and Opie Taylor (the sheriff's son).<ref name="Kelly" /> [[General Foods]], sponsor of ''The Danny Thomas Show'', had [[Right of first refusal|first access]] to the [[Spin-off (media)|spin-off]] and committed to it immediately.<ref name="Kelly" /> On October 3, 1960 at 9:30&nbsp;pm, ''The Andy Griffith Show'' made its debut.<ref name="Beck">Beck, Ken, and Jim Clark. ''The Andy Griffith Show Book''. St. Martin's Griffin, 1995.</ref>

==Production==
[[File:Andy Griffith Don Knotts 1970.JPG|right|thumb|Knotts and Griffith as their characters in a still taken from the October 7, 1965 one-hour variety special ''The Andy Griffith, Don Knotts, and Jim Nabors Show'']]
The show's production team included producers [[Aaron Ruben]] (1960–65) and Bob Ross (1965–68).<ref name="Kelly" /> First-season writers (many of whom worked in pairs) included Jack Elinson, Charles Stewart, Arthur Stander and Frank Tarloff (as "David Adler"), Benedict Freedman and John Fenton Murray, Leo Solomon and Ben Gershman, and Jim Fritzell and Everett Greenbaum.<ref name="Kelly" />  During season six, Greenbaum and Fritzell left the show and Ruben departed for ''[[Gomer Pyle, U.S.M.C.]]'', a show which he owned in part.<ref name="Kelly" />  Writer [[Harvey Bullock (writer)|Harvey Bullock]] left after season six. [[Bob Sweeney (director)|Bob Sweeney]] directed the first three seasons save the premiere.

The show was filmed at [[Desilu Studios]],<ref name="Kelly" /> with exteriors filmed at [[RKO Forty Acres|Forty Acres]] in Culver City, California.<ref name="Kelly" />  Woodsy locales were filmed north of [[Beverly Hills]] at [[Franklin Canyon Park|Franklin Canyon]].<ref name="Kelly" />

[[Don Knotts]], who knew Griffith professionally and had seen ''The Danny Thomas Show'' episode, called Griffith during the developmental stages of the show and suggested the Sheriff character needed a deputy. Griffith agreed. Knotts auditioned for the show's creator and executive producer, [[Sheldon Leonard]], and was offered a five-year contract playing Barney Fife.<ref name="Kelly" />

The show's theme music, "The Fishin' Hole", was composed by [[Earle Hagen]] and [[Herbert W. Spencer|Herbert Spencer]], with lyrics written by [[Everett Sloane]], who also guest starred as Jubal Foster in the episode "The Keeper of the Flame" (1962).  Whistling in the opening sequence, as well as the closing credits sequence, was performed by Earle Hagen.<ref name="Kelly" />  One of the show's tunes, "The Mayberry March", was reworked a number of times in different tempo, styles and orchestrations as background music.

The show's sole sponsor was [[General Foods]],<ref name="Kelly" /> with promotional consideration paid for (in the form of cars) by [[Ford Motor Company]] (mentioned in the credits).

=== Griffith's development of Andy Taylor ===

Initially, Griffith played Taylor as a heavy-handed country bumpkin, grinning from ear to ear and speaking in a hesitant, frantic manner.  The style recalled that used in the delivery of his popular  [[monologue]]s such as "[[What It Was, Was Football]]".  He gradually abandoned the 'rustic Taylor' and developed a serious and thoughtful characterization.  Producer Aaron Ruben recalled: <blockquote>He was being that marvelously funny character from ''[[No Time for Sergeants]]'', Will Stockdale [a role Griffith played on stage and in film]&nbsp;... One day he said, 'My God, I just realized that I'm the [[Double act|straight man]]. I'm playing straight to all these kooks around me.' He didn't like himself [in first year reruns]&nbsp;... and in the next season he changed, becoming this [[Abraham Lincoln|Lincolnesque]] character.<ref name="Kelly" /></blockquote>

As Griffith stopped portraying some of the sheriff's more unsophisticated character traits and mannerisms, it was impossible for him to create his own problems and troubles in the manner of other central sitcom characters such as Lucy in ''[[I Love Lucy]]'' or Archie Bunker in ''[[All in the Family]]'', whose problems were the result of their temperaments, philosophies and attitudes. Consequently, the characters around Taylor were employed to create the problems and troubles, with rock-solid Taylor stepping in as problem solver, mediator, advisor, disciplinarian and counselor.<ref name="Kelly"/>

==Plot and characters==
{{Main article|List of The Andy Griffith Show characters|List of The Andy Griffith Show guest stars}}

[[File:Cast 01.JPG|thumb|right|175px|From left: Deputy [[Barney Fife]] ([[Don Knotts]]), [[Opie Taylor]] ([[Ron Howard]]), [[Sheriff Andy Taylor]] ([[Andy Griffith]]), and [[Aunt Bee]] ([[Frances Bavier]])]]

The series plot revolves around [[Andy Taylor (The Andy Griffith Show)|Sheriff Andy Taylor]] ([[Andy Griffith]]) and his life in sleepy, slow-paced fictional [[Mayberry]], [[North Carolina]]. Sheriff Taylor's level-headed approach to law enforcement makes him the scourge of local [[moonshine]]rs and out-of-town criminals, while his abilities to settle community problems with common-sense advice, mediation, and conciliation make him popular with his fellow citizens. His professional life, however, is complicated by the repeated gaffes of his inept deputy, [[Barney Fife]] ([[Don Knotts]]). Barney is portrayed as Andy's cousin in the first, second, and sixth episodes, but is never again referred to as such. Andy socializes with male friends in the [[Main Street]] barber shop and dates various ladies until a schoolteacher becomes his steady interest in season three. At home, Andy enjoys fishing trips with his son, [[Opie Taylor|Opie]] ([[Ronny Howard]]), and quiet evenings on the front porch with his maiden aunt and housekeeper, [[Aunt Bee]] ([[Frances Bavier]]). Opie tests his father's parenting skills season after season, and Aunt Bee's ill-considered romances and adventures cause her nephew concern.

Andy's friends and neighbors include barber [[Floyd Lawson]] ([[Howard McNear]] – but played by Walter Baldwin in the 1960 episode "Stranger in Town"), service station attendants and cousins [[Gomer Pyle]] ([[Jim Nabors]]) and [[Goober Pyle]] ([[George Lindsey]]), and local drunkard [[Otis Campbell]] ([[Hal Smith (actor)|Hal Smith]]). There were two mayors: Mayor Pike, who was more relaxed, and Mayor Stoner, who had a more assertive personality.  On the [[Distaff#Other meanings|distaff]] side, townswoman [[Clara Edwards]] ([[Hope Summers]]), Barney's sweetheart [[Thelma Lou]] ([[Betty Lynn]]) and Andy's  schoolteacher sweetheart [[Helen Crump]] ([[Aneta Corsaut]]) become semi-regulars. Ellie Walker ([[Elinor Donahue]]) is Andy's girlfriend in the first season, while Peggy McMillan ([[Joanna Moore]]) is a nurse who becomes his girlfriend in season 3. [[Ernest T. Bass]] ([[Howard Morris]]) made his first appearance in Episode #94 ("Mountain Wedding"). In the color seasons, County Clerk [[Howard Sprague]] ([[Jack Dodson]]) and handyman Emmett Clark ([[Paul Hartman]]) appear regularly, while Barney's replacement deputy [[Warren Ferguson]] ([[Jack Burns]]) appears in season six. [[Unseen character]]s such as telephone operator Sarah, and Barney's love interest, local diner waitress Juanita Beasley, as mentioned in the first season, are often referenced. In the series' last few episodes, farmer Sam Jones ([[Ken Berry]]) debuts, and later becomes the lead of the show's sequel, ''[[Mayberry R.F.D.]]''.<ref name="Kelly" />

[[Don Knotts]], [[Aneta Corsaut]], [[Jack Dodson]], and [[Betty Lynn]] also appeared on Andy Griffith's later show ''[[Matlock (TV series)|Matlock]]''.

==Episodes==
{{Main article|List of The Andy Griffith Show episodes}}
The show comprises 8 full seasons and 249 episodes<ref name="Kelly" />—159 episodes in black and white (seasons 1–5) and 90 in color (seasons 6–8).  Griffith appears in all 249 episodes with Howard coming in second at 209.  Only Griffith, Howard, Bavier, Knotts, and Hope Summers appeared in all eight seasons.

Knotts left the show at the end of season five to pursue a career in films (on the show it is told that he takes a job as a detective with the State Police in [[Raleigh, North Carolina|Raleigh]]) but returned to make five guest appearances as Barney in seasons six through eight.  His last appearance in the final season in a story about a summit meeting with Russian dignitaries "ranked eleventh among single comedy programs most watched in television between 1960 and [1984], with an audience of thirty-three and a half million."<ref name="Kelly" />

==Reruns, spinoffs, and reunions==
[[File:Andy Griffith Ken Berry Mayberry RFD 1968.JPG|thumb|right|Sam Jones ([[Ken Berry]]) and his son Mike ([[Buddy Foster]]) were recurring characters in the final season of ''The Andy Griffith Show'', setting up the sequel series ''[[Mayberry R.F.D.]]''. ]]
In 1964, daytime reruns began airing.<ref name="Kelly" /> The show was retitled ''Andy of Mayberry'' to distinguish the repeat episodes from the new episodes airing in prime time.<ref>{{cite book|last=Terrace|first=Vincent |title=Encyclopedia of Television Shows, 1925 Through 2007|year=2009|publisher=McFarland|isbn=0-7864-3305-1|page=66}}</ref> As of 2017, the show has been seen in syndication for 53 years.

At the end of season four (May 1964), the [[backdoor pilot]] "Gomer Pyle, [[United States Marine Corps|U.S.M.C.]]" aired, and the following September, the spinoff series ''[[Gomer Pyle, U.S.M.C.]]'' debuted with Jim Nabors in the role of Gomer and [[Frank Sutton]] as [[drill instructor]] Sergeant Vince Carter.

In the last episodes of the series, the character Sam Jones, played by [[Ken Berry]], was introduced, and a sequel series, ''[[Mayberry R.F.D.]]'', was fashioned around him for the fall of 1968 (in essence replacing ''The Andy Griffith Show''). Several performers reprised their original roles in the sequel, with Bavier becoming Sam's housekeeper. To create a smooth transition from the original series to ''RFD'', Andy and Helen were married in the first episode, remained for a few additional episodes, and then left the show, with a move to [[Raleigh, North Carolina|Raleigh]], effectively ending their appearances. After ''RFD'''s cancellation in 1971, George Lindsey played Goober for many years on the popular country-variety show ''[[Hee Haw]]''.

In 1986, the reunion [[telemovie]] ''[[Return to Mayberry]]'' was broadcast with several cast members reprising their original roles. Absent, however, was [[Frances Bavier]]. She was living in [[Siler City, North Carolina]], in ill health, and declined to participate. In the TV movie, Aunt Bee is portrayed as deceased (and in fact Bavier did die three years later), with Andy visiting her grave.

Griffith and Howard reprised their roles a final time for a [[Funny or Die]] skit supporting the [[Barack Obama presidential campaign, 2008|2008 presidential campaign of Barack Obama]].<ref>{{cite web|url=http://www.huffingtonpost.com/2008/11/20/ron-howard-brian-grazer-t_n_145164.html|title=Inside Ron Howard's Obama Video Endorsement – EXCLUSIVE|work=[[The Huffington Post]]|last=Thomson|first=Katherine|date=December 21, 2008|accessdate=October 2, 2015}}</ref>

==Reception==

===Ratings===
<!-- Deleted image removed: [[File:Andy Taylor Statue.jpg|thumb|right|250px|Statue of Andy and Opie at the Andy Griffith Playhouse in Mount Airy {{Pufc|1=Andy Taylor Statue.jpg|log=2011 September 12}}]] -->
''The Andy Griffith Show'' consistently placed in the top ten during its run.<ref>{{cite web |url=http://www.classictvhits.com/tvratings/index.htm |title=Classic TV Hits: TV Ratings}}</ref>

{| class="wikitable" darkgrey;"
|-
! Season !! Time !! Rank !! Rating
|-
! 1) [[1960–61 United States network television schedule|1960&ndash;61]]
| rowspan="4"|Monday at 9:30-10:00 PM || style="text-align:center;"|#4 || style="text-align:center;"|27.8
|-
! 2) [[1961–62 United States network television schedule|1961&ndash;62]]
| style="text-align:center;"|#7 || style="text-align:center;"|27.0
|-
! 3) [[1962–63 United States network television schedule|1962&ndash;63]]
| style="text-align:center;"|#6 || style="text-align:center;"|29.7
|-
! 4) [[1963–64 United States network television schedule|1963&ndash;64]]
| style="text-align:center;"|#5 || style="text-align:center;"|29.4
|-
! 5) [[1964–65 United States network television schedule|1964&ndash;65]]
| Monday at 8:30-9:00 PM || style="text-align:center;"|#4 || style="text-align:center;"|28.3
|-
! 6) [[1965–66 United States network television schedule|1965&ndash;66]]
| rowspan="3"|Monday at 9:00-9:30 PM || style="text-align:center;"|#6 || style="text-align:center;"|26.9
|-
! 7) [[1966–67 United States network television schedule|1966&ndash;67]]
| style="text-align:center;"|#3 || style="text-align:center;"|27.4
|-
! 8) [[1967–68 United States network television schedule|1967&ndash;68]]
| style="text-align:center;"|#1 || style="text-align:center;"|27.6
|}

A Nielsen study conducted during the show's final season (1967&ndash;68) indicated the show ranked number one among blue collar workers followed by ''[[The Lucy Show]]'' and ''[[Gunsmoke]]''. Among white collar workers, the show ranked number three following ''Saturday Movies'' and ''[[The Dean Martin Show]]''.<ref name="Kelly" /> ''The Andy Griffith Show'' is one of only three shows to have its final season be the number one ranked show on television, the other two being ''I Love Lucy'' and ''[[Seinfeld]]''. In 1998, more than five million people a day watched the show's reruns on 120 stations.<ref>{{cite web|url=http://www.drpolitics.com/article-view.php?id=4 |title=What Andy, Opie, and Barney Fife Mean to Americans |accessdate=January 27, 2009 |author=Ted Rueter |publisher=The Christian Science Monitor |date=January 22, 1998 |deadurl=yes |archiveurl=https://web.archive.org/web/20080524183316/http://www.drpolitics.com/article-view.php?id=4 |archivedate=May 24, 2008 }}</ref>

===Awards and nominations===

====Emmys====
;1961
*Outstanding Performance in a Supporting Role by an Actor or Actress in a Series: Don Knotts – Won
*Outstanding Program Achievement in the Field of Humor – Nominated (Winner: ''[[The Jack Benny Program]]'')
;1962
*Outstanding Performance in a Supporting Role by an Actor: Don Knotts – Won
*Outstanding Program Achievement in the Field of Humor – Nominated (Winner: ''[[The Bob Newhart Show (1961 TV series)|The Bob Newhart Show]]'')
;1963
*Outstanding Performance in a Supporting Role by an Actor: Don Knotts – Won
;1966
*Outstanding Performance by an Actor in a Supporting Role in a Comedy: Don Knotts for "The Return of Barney Fife" – Won
;1967
*Outstanding Comedy Series – Nominated (Winner: ''[[The Monkees (TV series)|The Monkees]]'')
*Outstanding Performance by an Actor in a Supporting Role in a Comedy: Don Knotts for "Barney Comes to Mayberry" – Won
*Outstanding Performance by an Actress in a Supporting Role in a Comedy: Frances Bavier – Won

====TV Land Awards====
*Favorite Second Banana: Don Knotts – Won (2003)
*Single Dad of the Year: Andy Griffith – Won (2003)
*Legend Award – Won (2004)

==Merchandise and pop culture==
[[Dell Comics]] published two ''The Andy Griffith Show'' [[comic book]]s during the show's first run, with art work by [[Henry Scarpelli]].<ref>https://www.lambiek.net/artists/s/scarpelli_henry.htm</ref> In 2004, copies in near-mint condition were priced in excess of $500 each.<ref>Overstreet, Robert M.. ''Official Overstreet Comic Book Price Guide''. 34th edition. House of Collectibles, Random House Information Group, May 2004.</ref> The show's enduring popularity has spawned considerable merchandise since its first run, including board games, bobblehead dolls, kitchenware, books, and other items. In 2007, a line of canned foods inspired by the series was made available in grocery stores across America. Griffith's hometown of [[Mount Airy, North Carolina]] annually hosts a week-long "Mayberry Days" celebration featuring concerts, parades, and appearances by the show's players.

In 1997, the episode "Opie the Birdman" was ranked No. 24 on [[TV Guide's 100 Greatest Episodes of All Time]].<ref>{{cite journal |last1= |first1= |last2= |first2= |year=1997 |title=Special Collector's Issue: 100 Greatest Episodes of All Time |journal=[[TV Guide]] |volume= |issue=June 28-July 4 |pages= |publisher= |doi= |url= <!--|accessdate=October 4, 2011 -->}}</ref> In 2002, ''[[TV Guide]]'' ranked ''The Andy Griffith Show'' ninth on its list of the 50 Best Shows of All Time.<ref name=Top50Shows/> [[Bravo (US TV channel)|Bravo]] ranked Andy Taylor 63rd on their list of the 100 greatest TV characters.<ref>{{cite web|url=http://www.bravotv.com/The_100_Greatest_TV_Characters//index.shtml|archiveurl=https://web.archive.org/web/20071015070449/http://www.bravotv.com/The_100_Greatest_TV_Characters/index.shtml| archivedate=October 15, 2007|title=The 100 Greatest TV Characters|publisher=Bravo|accessdate=October 19, 2010}}</ref>

In 2003, the country band Rascal Flatts released the song Mayberry and many of the lyrics pay tribute to the show.
 
The cable television network [[TV Land]] erected bronze statues of Andy and Opie in [[Mount Airy, North Carolina|Mount Airy]] and [[Raleigh, North Carolina]] (''see:'' [[Pullen Park]]).<ref>{{cite news | url=http://www.highbeam.com/doc/1G1-219640620.html| title=Vandals toss paint on statue of Andy and Opie in N.C.(Front) | agency=Associated Press | date=February 25, 2010 | accessdate=August 6, 2013 }}{{Subscription required|via=[[HighBeam Research]]}}</ref>

The Taylor Home Inn in Clear Lake, Wisconsin, is a bed-and-breakfast modeled after the Taylor Home.<ref>{{cite news | url=http://www.msnbc.msn.com/id/14063707/ns/travel-road_trips/t/little-touch-mayberry/ | title=A little touch of Mayberry: B&B recreates Andy Griffith's TV show home | agency=Associated Press | date=July 27, 2006 | accessdate=September 7, 2012}}</ref>

The Mayberry Cafe in Danville, Indiana features Aunt Bee's Fried Chicken and a replica of Andy's Ford Galaxie police car.

In 2013, TV Guide ranked ''The Andy Griffith Show'' #15 on their list of the 60 Greatest Shows of All Time.<ref name="Fretts"/>

===Video releases===
In the late 1980s, Premier Promotions released various episodes on VHS.  Most tapes had either two or four episodes. In the early to mid-1990s, [[United American Video]] released VHS tapes of various episodes. They either had two or three episodes. These compilations were culled from episodes early in the show's run that had lapsed into the public domain; these episodes continue to be circulated on unofficial video releases.

Between 2004 and 2006, [[Paramount Home Entertainment]] and later in 2006, CBS DVD released all eight seasons as single-season packages on Region 1 DVD. ''The Andy Griffith Show: The Complete Series'' was first released as a 40-disc boxed set in 2007. In addition to all 249 episodes of the series, its bonus features included the episode "Danny Meets Andy Griffith" from ''[[The Danny Thomas Show]]'' which served as the pilot, the episode "Opie Joins the Marines" from ''[[Gomer Pyle, U.S.M.C.]]'' which featured Ron Howard and the 95-minute, [[Television film|made-for-television]] [[comedy film]] ''[[Return to Mayberry]]''. In 2016, ''The Andy Griffith Show: The Complete Series'' was repackaged and released again as a 39-disc set that featured all 249 episodes of the series but did not include the bonus feature disc.

Sixteen episodes from the season three (believed to be in [[public domain]]) are available on discount DVDs. The public domain status of these 16 episodes has been challenged however by the 2007 lawsuit ''CBS Operations Inc v. Reel Funds International Inc.'', which ruled that there was a valid copyright on those episodes and prevented the defendant, a public domain distributor, from distributing the episodes within the jurisdiction of the [[United States District Court for the Northern District of Texas]].<ref>{{cite web|url=http://www.winston.com/index.cfm?contentid=34&itemid=2488 |title=Winston.com |work=winston.com |deadurl=yes |archiveurl=https://web.archive.org/web/20130831181432/http://winston.com/index.cfm?contentid=34&itemid=2488 |archivedate=August 31, 2013 }}</ref><ref>{{cite web|url=http://www.gpo.gov/fdsys/pkg/USCOURTS-txnd-3_06-cv-00588|title=CBS Operations Inc v. Reel Funds International Inc|work=gpo.gov}}</ref><ref>{{cite news| url=http://www.hollywoodreporter.com/lists/jonathan-zavin-209761 | work=The Hollywood Reporter | first=Matthew | last=Belloni | title=Jonathan Zavin | date=July 16, 2009}}</ref>

{| class="wikitable"
|-
!DVD Name
!Ep#
!Release Date
|-
| ''The First Season''
| 32
| November 16, 2004
|-
| ''The Second Season''
| 31
| May 24, 2005
|-
| ''The Third Season''
| 32
| August 16, 2005
|-
| ''The Fourth Season''
| 32
| November 22, 2005
|-
| ''The Fifth Season''
| 32
| February 14, 2006
|-
| ''The Sixth Season''
| 30
| May 9, 2006
|-
| ''The Seventh Season''
| 30
| August 29, 2006
|-
| ''The Final Season''
| 30
| December 12, 2006
|-
| ''The Complete Series''
| 249
| May 29, 2007
|-
| ''The Complete Series''
| 249
| February 16, 2016
|}

'''Note:''' The Region 1 release of ''The Third Season'' contains two episodes edited for syndication: "The Darlings Are Coming", which had several scenes cut, and "Barney Mends a Broken Heart", which had its epilogue cut.

==References==
{{Reflist|30em}}

==Further reading==
* {{Cite book |last1=Beck |first1=Ken |last2=Clark |first2=Jim |title=The Andy Griffith Show Book |type=trade paperback|year=1985 |publisher=St. Martin's Press |location=New York |isbn=0-312-03654-X }}
* Beck, Ken, and Clark, Jim. ''Mayberry Memories.'' Rutledge Hill Press, 2000.
* Fann, Joey. ''The Way Back to Mayberry''. Broadman and Holman, 2001. ISBN 0-8054-2420-2.
* Kelly, Richard. ''The Andy Griffith Show'' (John F. Blair, 1981). ISBN 0-89587-043-6.
* McElroy, Kathleen. "Remembering Mayberry in White and Black: The Andy Griffith Show's Construction of the South," ''Memory Studies,'' 8 (Oct. 2015), 440–53.
* [[Gustavo Pérez Firmat | Pérez Firmat, Gustavo]]. ''A Cuban in Mayberry: Looking Back at America’s Hometown''. Austin: The University of Texas Press, 2014.
* {{Cite news |url=http://www.accessmylibrary.com/coms2/summary_0286-14331567_ITM |title=Why "The Andy Griffith Show" is Important to Popular Culture |first=Don Rodney |last=Vaughn |publisher=Journal of Popular Culture | date=November 1, 2004}}

==External links==
{{Prone to spam|date=December 2015}}
{{Z148}}<!--     {{No more links}}

       Please be cautious adding more external links.

Wikipedia is not a collection of links and should not be used for advertising.

     Excessive or inappropriate links will be removed.

 See [[Wikipedia:External links]] and [[Wikipedia:Spam]] for details.

If there are already suitable links, propose additions or replacements on
the article's talk page, or submit your link to the relevant category at
DMOZ (dmoz.org) and link there using {{Dmoz}}.

-->
{{Wikiquote}}
{{Commons category|The Andy Griffith Show}}
*{{tv.com show|the-andy-griffith-show|The Andy Griffith Show}}
*{{tvguide title|100443}}
*[http://www.tvland.com/fullepisodes/andygriffith/?xrs=wiki_andy Watch full episodes of ''The Andy Griffith Show'' on TVLand.com]{{dead link|date=December 2015}}
*[http://www.scn.org/~bj078/andygriffith.html Public domain episodes of The Andy Griffith Show]
* {{IMDb title|0053479|The Andy Griffith Show}}
*{{epguides|id=AndyGriffithShow}}
*[http://www.telovation.com/articles/andy-griffith-show.html Behind The Scenes of ''The Andy Griffith Show'']
* {{EmmyTVLegends title|andy-griffith-show-the|The Andy Griffith Show}}
{{The Andy Griffith Show}}

{{mayberry}}
{{TopUSTVShows}}

{{DEFAULTSORT:Andy Griffith Show, The}}
[[Category:American television sitcoms]]
[[Category:The Andy Griffith Show characters| ]]
[[Category:1960s American television series]]
[[Category:1960 American television series debuts]]
[[Category:1968 American television series endings]]
[[Category:Black-and-white television programs]]
[[Category:CBS network shows]]
[[Category:English-language television programming]]
[[Category:Nielsen ratings winners]]
[[Category:North Carolina culture]]
[[Category:Police comedies]]
[[Category:Television series by CBS Television Studios]]
[[Category:Television shows set in North Carolina]]
[[Category:Television spin-offs]]
[[Category:Television programs adapted into films]]
[[Category:Television programs adapted into comics]]
[[Category:Television shows filmed in Los Angeles]]
[[Category:Articles containing video clips]]