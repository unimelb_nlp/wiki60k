{{Italic title}}
{{Infobox journal
| title = The Journal of Economic History
| cover = [[File:Journal of Economic History.jpg]]
| editor = Ann Carlos, William Collins
| discipline = [[Economic history]]
| abbreviation = J. Econ. Hist.
| publisher = [[Cambridge University Press]]
| country = 
| frequency = Quarterly
| history = 1941–present
| impact = 0.742
| impact-year = 2015
| website = http://journals.cambridge.org/action/displayJournal?jid=JEH
| link1 = http://journals.cambridge.org/action/displayIssue?jid=JEH&tab=currentissue
| link1-name = Online access
| link2 = http://journals.cambridge.org/action/displayBackIssues?jid=JEH
| link2-name = Online archive
| JSTOR = 00220507
| OCLC = 1782353
| LCCN = 43006024
| ISSN = 0022-0507
| eISSN = 1471-6372
}}

'''''The Journal of Economic History''''' is an [[academic journal]] of [[economic history]] which has been published since 1941.<ref>[http://www.worldcat.org/title/journal-of-economic-history/oclc/1782353&referer=brief_results OCLC Worldcat page]</ref> Many of its articles are  quantitative, often following the formal approaches that have been called [[new economic history|cliometrics]] or the [[new economic history]] to make statistical estimates.

The journal is published on behalf of the [[Economic History Association]] by [[Cambridge University Press]].<ref>[http://eh.net/eha/ Economic History Association website]</ref> Its editors are [[Ann Carlos]] at the University of Colorado and William Collins at Vanderbilt University. Its 2015 [[impact factor]] is 0.742.<ref>[http://journals.cambridge.org/action/displayMoreInfo?jid=JEH&type=if ''The Journal of Economic History'': Impact factor]</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|1=http://journals.cambridge.org/action/displayJournal?jid=JEH}}

{{DEFAULTSORT:Journal Of Economic History, The}}
[[Category:Economic history journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1941]]
[[Category:Quarterly journals]]
[[Category:Cambridge University Press academic journals]]
[[Category:Academic journals associated with learned and professional societies]]