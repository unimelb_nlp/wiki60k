{{Infobox journal
| title = Journal of Neuroimaging
| editor = [[Rohit Bakshi (neurologist)|Rohit Bakshi]]
| discipline = [[Neuroimaging]]
| abbreviation = J. Neuroimaging
| publisher = [[John Wiley & Sons]] on behalf of the [[American Society of Neuroimaging]]
| country = United States
| frequency = Bimonthly
| history = 1991–present
| impact = 1.734
| impact-year = 2014
| website = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1552-6569
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1552-6569/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1552-6569/issues
| link2-name = Online archive
| ISSN = 1051-2284
| eISSN = 1552-6569
| OCLC = 710019389
}}
The '''''Journal of Neuroimaging''''' is a bimonthly [[peer-reviewed]] [[scientific journal]] covering al aspects of [[neuroimaging]]. It was established in 1991 and is published by [[John Wiley & Sons]] on behalf of the [[American Society of Neuroimaging]]. Since 2015, the [[editor-in-chief]] is [[Rohit Bakshi (neurologist)|Rohit Bakshi]] ([[Harvard Medical School]]). The founding editor-in-chief was Leon Prockop. He was succeeded in 1999 by Lawrence Wechsler and in 2008 by Joseph Masdeu.

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Abstracts in Anthropology]]
* [[Academic Search]]
* [[Elsevier Biobase|BIOBASE]]/[[Current Awareness in Biological Sciences]]
* [[Current Contents]]/Clinical Medicine
* [[Embase]]
* [[Expanded Academic ASAP]]
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]
* [[InfoTrac]]
* [[ProQuest|ProQuest databases]]
* [[Science Citation Index Expanded]]
* [[Scopus]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.734.<ref name=WoS>{{cite book |year=2015 |chapter=Journal of Neuroimaging |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== Prockop award ==
The John and Sophie Prockop Memorial Lectureship was established in 2005 by the founding editor, Leon Prockop, in memory of his parents. The recipient is the first author of a manuscript published in the prior year that has been judged to have outstanding value to the development and success of the journal or is the highest quality manuscript.

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1552-6569}}
* [http://www.asnweb.org/ American Society of Neuroimaging]

[[Category:Neuroimaging journals]]
[[Category:John Wiley & Sons academic journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1991]]
[[Category:English-language journals]]