{{Other uses|Agricola (disambiguation){{!}}Agricola}}
{{Infobox Bibliographic Database
|title = AGRICOLA
|image =
|caption =
|producer = [[United States Department of Agriculture]]
|country = [[United States|USA]]
|history =
|languages = [[English language|English]]
|providers = [[United States Department of Agriculture]]
|cost = Free and Subscription 
|disciplines = [[Agriculture]]
|depth = Index, some abstracts
|formats = Journals, Books, Audiovisual, other
|temporal = 17th century - present
|geospatial =
|number =
|updates = Daily
|p_title =
|p_dates =
|ISSN =
|web = http://agricola.nal.usda.gov/
|titles =  http://riley.nal.usda.gov/nal_display/index.php?info_center=8&tax_level=2&tax_subject=157&topic_id=2010
}}

'''AGRICOLA''' ('''AGRIC'''ultural '''O'''n'''L'''ine '''A'''ccess) is a database created and maintained by the [[United States Department of Agriculture]].  The database serves as the catalog and index for the collections of the [[United States National Agricultural Library]], but it also provides public access to information on [[agriculture]] and allied fields.<ref name=about>{{cite web|url=http://agricola.nal.usda.gov/help/aboutagricola.html|title=About the NAL Catalog: AGRICOLA|publisher=National Agricultural Library|accessdate=2009-10-16| archiveurl= http://webarchive.loc.gov/all/20091016215553/http%3A//agricola%2Enal%2Eusda%2Egov/help/aboutagricola%2Ehtml| archivedate= October 16, 2009 <!--DASHBot-->| deadurl= no}}</ref>  

A related database, [http://pubag.nal.usda.gov/pubag/home.xhtml PubAg], was released in 2015 and is focused on the full-text publications from USDA scientists, as well as some of the journal literature. PubAg was designed for a broad range of users, including farmers, scientists, scholars, students, and the general public.<ref name=NAL-unveils>{{cite press release |last=Kaplan |first=Kim |date=2015-01-13 |title=NAL unveils new search engine for published USDA research |url=http://www.ars.usda.gov/is/pr/2015/150113.htm |publisher=USDA Agricultural Research Service |access-date=2016-03-23}}</ref> The distinctions AGRICOLA and PubAg them are described below:

"AGRICOLA serves as the public catalog of  the National Agricultural Library. It contains records for all of the holdings of the library. It also contains citations to articles, much like PubAg. AGRICOLA also contains citations to many items that, while valuable and relevant to the agricultural sciences, are not peer-reviewed journal articles. Also, AGRICOLA has a different interface. So, while there is some overlap between the two resources, they are different in significant ways. There are no plans to eliminate AGRICOLA." <ref>{{Cite web|url = http://pubag.nal.usda.gov/pubag/static/FAQ.html|title = FAQ|website = pubag.nal.usda.gov|access-date = 2016-03-04}}</ref>

==Coverage==
AGRICOLA indexes a wide variety of publications covering agriculture and its allied fields, including, "[[animal science|animal]] and [[veterinary science]]s, [[entomology]], [[plant science]]s, [[forestry]], [[aquaculture]] and [[fisheries]], [[farming]] and farming systems, [[agricultural economics]], [[agricultural extension|extension]] and [[education]], [[food science|food]] and [[human nutrition]], and [[earth science|earth]] and [[environmental science]]s."<ref name=about/>

==References==
<references/>

==External links==
{{Commons|SLC Agricola Logos}}
*[http://agricola.nal.usda.gov/ Agricola]

[[Category:Agricultural databases]]
[[Category:Bibliographic databases and indexes]]
[[Category:Databases in the United States]]
[[Category:Public domain databases]]
[[Category:United States Department of Agriculture]]


{{database-stub}}