{| {{Infobox ship begin
| Ship name = SMS ''Bayern'' (1878)
   }}
|+ SMS ''Bayern'' (1878)
{{Infobox ship image
| Ship image   = [[File:SMS Bayern (1878) Gartenlaube 1889.jpg|300px]]
| Ship caption = SMS ''Bayern''
}}
{{Infobox ship career
| Hide header = 
| Ship country = [[German Empire]]
| Ship flag = {{shipboxflag|German Empire|naval}}
| Ship name = SMS ''Bayern''
| Ship builder = [[Kaiserliche Werft Kiel|Imperial Dockyard]], [[Kiel]]
| Ship laid down = 1874
| Ship launched = 13 May 1878
| Ship commissioned = 4 August 1881
| Ship struck = 19 Feb 1910
| Ship fate = Sold for scrap, 1919
}}
{{Infobox ship characteristics
| Hide header = 
| Header caption = 
| Ship class = {{Sclass-|Sachsen|ironclad|0}} armoured frigate
| Ship displacement = {{convert|7742|t|LT|abbr=on}}
| Ship tons burthen = 
| Ship length = {{convert|98.20|m|ftin|abbr=on}}
| Ship beam   = {{convert|18.40|m|ftin|abbr=on}}
| Ship draft  = {{convert|6.32|m|ftin|abbr=on}}
| Ship propulsion =* Two 3-cylinder steam engines
* Two four-bladed screws
* {{convert|5600|ihp|abbr=on}}
| Ship speed = {{convert|13|kn|abbr=on}}
| Ship range = {{convert|1940|nmi|abbr=on}} at {{convert|10|kn|abbr=on}}
| Ship complement =
*  32 officers
* 285 enlisted men
| Ship armament =
*6 × {{convert|26|cm|in|abbr=on}} L/22 guns
*6 × {{convert|8.7|cm|in|abbr=on}} guns
*8 × {{convert|3.7|cm|in|abbr=on}} guns
| Ship armor =
* Belt: {{convert|203|-|254|mm|in|abbr=on}}
* Deck: {{convert| 50|-| 75|mm|in|abbr=on}}
| Ship notes = 
}}
|}

'''SMS ''Bayern'''''{{efn|name=SMS}} was one of four {{sclass-|Sachsen|ironclad|0}} armored frigates of the [[German Empire|German]] [[Kaiserliche Marine|Imperial Navy]]. Her sister ships were {{SMS|Sachsen|1877|2}}, {{SMS|Baden|1880|2}}, and {{SMS|Württemberg|1878|2}}. Named for [[Bavaria]], ''Bayern'' was built by the [[Kaiserliche Werft Kiel|Imperial Dockyard]] in [[Kiel]] from 1874 to 1881. The ship was commissioned into the Imperial Navy in August 1881. She was armed with a main battery of six {{convert|26|cm|abbr=on}} guns in two open [[barbette]]s.

After her commissioning, ''Bayern'' served with the fleet on numerous training exercises and cruises. She participated in several cruises escorting Kaiser [[Wilhelm II]] on state visits to Great Britain and to various cities in the [[Baltic Sea]] in the late 1880s and early 1890s. During 1895–1898, the ship was modernized at the [[Schichau-Werke]] dockyard in [[Danzig]]; she served for another decade with the fleet before being withdrawn from active service in 1910. She was used as a target ship after 1911, until she was sold in 1919 and broken up for scrap.

== Construction ==
{{main|Sachsen-class ironclad}}
[[File:SMSSachsen.PNG|thumb|left|Illustration of the ''Sachsen''-class ships]]

''Bayern'' was ordered by the Imperial Navy under the contract name "A," which denoted that the vessel was a new addition to the fleet. She was built at the [[Kaiserliche Werft Wilhelmshaven|Imperial Dockyard]] in [[Wilhelmshaven]]; her keel was laid in 1874 under construction number 3.{{sfn|Gröner|pp=7–8}} The ship was launched on 13 May 1878 and commissioned into the German fleet on 4 August 1881.{{sfn|Gröner|p=8}} Along with her three sisters, ''Bayern'' was the first large, armored warship built for the German navy that relied entirely on engines for propulsion.{{sfn|Hovgaard|p=111}}

The ship was {{convert|98.20|m|ftin|sp=us}} [[length overall|long overall]] and had a beam of {{convert|18.40|m|ftin|abbr=on}} and a draft of {{convert|6.32|m|ftin|abbr=on}} forward.{{sfn|Gröner|p=8}} ''Bayern'' was powered by two 3-cylinder [[triple expansion engine]]s, which were supplied with steam by eight coal-fired Dürr boilers. The ship's top speed was {{convert|13|kn}}, at {{convert|5600|ihp}}{{sfn|Gröner|pp=7–8}} Her standard complement consisted of 32 officers and 285 enlisted men, though while serving as a squadron flagship this was augmented by another 7 officers and 34 men.{{sfn|Gröner|p=8}}

She was armed with six {{convert|26|cm|in|abbr=on}} guns, two of which were single-mounted in an open [[barbette]] forward of the [[conning tower]] and the remaining four mounted amidships, also on single mounts in an open barbette. As built, the ship was also equipped with six {{convert|8.7|cm|in|abbr=on}} L/24 guns and eight {{convert|3.7|cm|in|abbr=on}} [[Hotchkiss gun|Hotchkiss revolver cannons]].{{sfn|Gröner|p=8}}{{sfn|Gardiner|p=245}} ''Bayern''{{'}}s armor was made of wrought iron, and was concentrated in an armored citadel amidships.{{sfn|Hovgaard|p=111}} The armor ranged from {{convert|203|to|254|mm|in|abbr=on}} on the armored citadel, and between {{convert|50|-|75|mm|in|abbr=on}} on the deck. The barbette armor was 254&nbsp;mm of wrought iron backed by 250&nbsp;mm of [[teak]].{{sfn|Gröner|p=7}}

== Service history ==
After her commissioning in August 1881,{{sfn|Gröner|p=8}} ''Bayern'' was placed in reserve. She was not activated for service with the fleet until 1884; this in part had to do with the poor performance of her sister {{SMS|Sachsen|1877|2}} in the fleet maneuvers of 1880.{{sfn|Sondhaus|p=161}} Among the problems associated with the ''Sachsen''-class ships was a tendency to roll dangerously due to their flat bottoms, which greatly reduced the accuracy of their guns. The ships were also poorly armored, compared to their contemporaries. In addition, they were slow and suffered from poor maneuverability.{{sfn|Sondhaus|pp=135–136}} Nevertheless, ''Bayern'' and her three sisters served as the I Division in the 1884 fleet maneuvers, under the command of Rear Admiral [[Alexander von Monts]].{{sfn|Sondhaus|p=161}}

''Bayern'' remained with the fleet for the 1885 maneuvers, though she was joined only by the older ironclads {{SMS|Friedrich Carl|1867|2}} and {{SMS|Hansa|1872|2}}. The maneuvers were begun with a visit to [[Ålesund]], Norway, after which the fleet went to the [[Baltic Sea]] for training exercises.{{sfn|Sondhaus|p=162}} ''Bayern'' was demobilized at the close of maneuvers. In October 1885, [[August von Thomsen]], who had been appointed chief gunner, set up the first long range gunnery experiments on ''Bayern''. He went on to gain fame as "the father of German naval artillery."{{sfn|Friedman et al.|p=158}} ''Bayern''{{'}}s three sisters and the new ironclad {{SMS|Oldenburg|1884|2}} comprised the training squadron for 1886.{{sfn|Sondhaus|p=163}} ''Bayern'' returned to active duty in 1888, when she participated in a tour of the Baltic by the newly crowned Kaiser [[Wilhelm II]]. The fleet stopped in [[St. Petersburg]], [[Stockholm]], and [[Copenhagen]] on the seventeen-day cruise.{{sfn|Sondhaus|p=177}}

''Bayern'' participated in the ceremonial transfer of the island of [[Helgoland]] from British to German control in the summer of 1890. She was present during the fleet maneuvers in September, where the entire eight-ship armored squadron simulated a Russian fleet blockading [[Kiel]].{{sfn|Sondhaus|p=192}} She remained with the I Division in 1891; the year's maneuvers simulated a two-front war against Russia and either France of Denmark. ''Bayern'' participated in the 1892 fleet maneuvers as well. Three separate simulations were conducted, which included French blockades of the German [[North Sea]] coast and a Russian attack on [[Kiel]].{{sfn|Sondhaus|pp=194–195}} Vice Admiral [[Wilhelm Schröder]] commanded the fleet maneuvers of 1893, which simulated a protracted campaign against a superior French fleet.{{sfn|Sondhaus|p=195}} ''Bayern'' and her three sisters served as the Russian [[Baltic Fleet]] during the 1894 maneuvers.{{sfn|Sondhaus|p=196}}

[[File:SMS Bayern NH 88651.jpg|thumb|SMS Bayern circa 1893]]
The four ''Sachsen''-class ships were transferred to the II Division before the winter cruise of 1894–1895, following the completion of the four {{sclass-|Brandenburg|battleship|2}}s. The German fleet now possessed two homogenous squadrons of four ships each. The two divisions steamed to [[Orkney]] and the [[Shetland Islands]] in the spring of 1895.{{sfn|Sondhaus|p=198}} ''Bayern'' joined a massive fleet review on 21 July 1895 for the opening of the [[Kaiser Wilhelm Canal]], which connected Kiel to the North Sea.{{sfn|Sondhaus|p=199}} The Autumn 1895 maneuvers simulated a high-seas battle between the I and II Divisions in the North Sea, followed by combined maneuvers with the rest of the fleet in the Baltic.{{sfn|Sondhaus|p=201}}

After the conclusion of the 1895 maneuvers, ''Bayern'' was taken into drydock at the [[Schichau-Werke]] in [[Danzig]] for reconstruction.{{sfn|Gröner|p=8}} The ship's old wrought iron and teak armor was replaced with new [[Krupp]] nickel-steel armor.{{sfn|Sondhaus|p=219}} The four funnels were trunked into a single large funnel and new engines were also installed,{{sfn|Gardiner|p=245}} which increased the ship's speed to {{convert|15.4|kn|abbr=on}}. The ship's 8.7&nbsp;cm guns were replaced with quick-firing {{convert|8.8|cm|abbr=on}} SK L/30 guns and four {{convert|3.7|cm|abbr=on}} autocannons. Work was completed in 1898. ''Bayern''{{'}}s three sisters were similarly modified between 1896 and 1899.{{sfn|Gröner|p=8}} ''Bayern'' remained with the fleet until 19 February 1910, when the ship was stricken from the [[naval register]]. She was converted into a target ship for the fleet and served in this capacity off [[Stollergrund]] after 1911. On 5 May 1919, ''Bayern'' was sold for scrapping and broken up in Kiel.{{sfn|Gröner|p=8}}

== Notes ==

{{notes
| notes =

{{efn
| name = SMS
| "SMS" stands for "''[[Seiner Majestät Schiff]]''", or "His Majesty's Ship".
}}

}}

{{reflist|20em}}

== References ==

* {{cite book
  | last = Friedman
  | first = Norman
  |author2=W. J. Jurens |author3=A. D. Baker
   | year = 2008
  | title = Naval Firepower: Battleship Guns and Gunnery in the Dreadnought Era
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-1-59114-555-4
  | ref = {{sfnRef|Friedman et al.}}
  }}
* {{cite book
  | editor-last = Gardiner
  | editor-first = Robert
  | year = 1979
  | title = Conway's All the World's Fighting Ships 1860–1905
  | publisher = Conway Maritime Press
  | location = Greenwich
  | isbn = 978-0-8317-0302-8
  | ref = {{sfnRef|Gardiner}}
  }}
* {{cite book
  | last = Gröner
  | first = Erich
  | year = 1990
  | title = German Warships: 1815–1945
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-0-87021-790-6
  | oclc = 22101769
  | ref = {{sfnRef|Gröner}}
  }}
* {{cite book
  | last = Hovgaard
  | first = William
  | year = 1971
  | title = Modern History of Warships
  | publisher = Conway Maritime Press
  | location = London
  | isbn = 978-0-85177-040-6
  | ref = {{sfnRef|Hovgaard}}
  }}
* {{cite book
  | last = Sondhaus
  | first = Lawrence
  | year = 1997
  | title = Preparing for Weltpolitik: German Sea Power Before the Tirpitz Era
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-1-55750-745-7
  | ref = {{sfnRef|Sondhaus}}
  }}

{{Sachsen class armored frigate}}
{{Good article}}

{{DEFAULTSORT:Bayern (1878)}}
[[Category:Sachsen-class ironclads]]
[[Category:Victorian-era naval ships of Germany]]
[[Category:1878 ships]]
[[Category:Ships built in Wilhelmshaven]]