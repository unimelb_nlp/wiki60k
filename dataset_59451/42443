<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=Breda Ba.88 ''Lince'' 
 |image=Breda Ba88.jpg
 |caption=A Breda Ba.88 entering a shallow dive.
}}{{Infobox Aircraft Type
 |type=[[Ground-attack aircraft]]
 |manufacturer=[[Ernesto Breda]]
 |designer=Giuseppe Panzeri and Antonio Parano
 |first flight=October [[1936 in aviation|1936]]
 |introduced=[[1939 in aviation|1939]]
 |retired=[[1941 in aviation|1941]]
 |status=
 |primary user=''[[Regia Aeronautica]]''
 |more users=<!--up to three more. please separate with <br/>.-->
 |produced=1936–1940
 |number built=149
 |unit cost=
 |variants with their own articles=
}}
|}
[[File:Breda Ba.88 on ground.jpg|300px|right|thumb]]
The '''Breda Ba.88 ''Lince''''' ([[Italian language|Italian]]: [[Lynx]]) was a [[ground-attack aircraft]] used by the [[Italy|Italian]] ''[[Regia Aeronautica]]'' during [[World War II]]. Its streamlined design and retractable [[Landing gear|undercarriage]] were advanced for the time, and after its debut in 1937 the aircraft established several world speed records.<ref name="Angelucci and Matricardi p. 201">Angelucci and Matricardi 1978 p. 201.</ref> However, when military equipment was installed on production examples, problems of instability developed and the aeroplane's general performance deteriorated. Eventually its operational career was cut short, and the remaining Ba.88 airframes were used as fixed installations on airfields to mislead enemy reconnaissance.<ref name="Angelucci and Matricardi p. 201"/> It represented, perhaps, the most remarkable failure of any operational aircraft to see service in World War II.<ref name="Mondey p. 28">Mondey 2006 p. 28.</ref>

==Design and development==
The Breda Ba.88 was designed to fulfill a 1936 requirement by the ''[[Regia Aeronautica]]'' for a heavy fighter bomber capable of a maximum speed of 530&nbsp;km/h (329&nbsp;mph)<ref name= "Lembo">Lembo 2005</ref> (more than that of any other aircraft existing or being planned at the time<ref name= "Lembo"/>), armament of 20&nbsp;mm cannons and range of 2,000&nbsp;km (1,240&nbsp;mi). It first flew in October 1936. The project was derived from the aborted [[Breda Ba.75|Ba.75]] also designed by [[Giuseppe Panzeri]] and [[Antonio Parano]].

===Technical description===
The Ba.88 was an all-metal, twin-engine, two-crew [[monoplane]], with a high-mounted wing. It employed a "concentric" [[fuselage]] design, with a framework of steel tubes and a metallic skin covering which was both streamlined (having a very small fuselage cross-section) and strong. However, this internal load-bearing structure was very complex and of outdated design, as [[Monocoque#Aircraft|monocoque]] designs were starting to be developed elsewhere. The internal struts resulted in excessive weight compounded by manufacturing delays. The narrow confines of the fuselage would require the Ba.88 to carry bombs in a semi-external structure, much to the detriment of the aircraft's aerodynamics. The all-metal wings had two [[longerons]], and housed the engine nacelles, undercarriage main elements, and the majority of the 12 [[self-sealing fuel tank]]s (the only protective [[Vehicle armour|armour]] in the aircraft), providing 1,586&nbsp;L (419&nbsp;US gal) total capacity. All three undercarriage units were retractable, which was unusual for the time.<ref name= "Lembo"/>

The aircraft was powered by two [[Piaggio P.XI]] air-cooled [[radial engine]]s. They were of the same type as used in other projects such as the Re.2000, and drove two three-blade, [[Constant speed propeller|continuous-speed]] 3.2&nbsp;m (10.4&nbsp;ft) diameter Breda [[propeller (aircraft)|propeller]]s. The engine nacelles also carried the two main undercarriage units. The aircraft had a twin tail to provide the dorsal 7.7&nbsp;mm (0.303&nbsp;in) [[Breda-SAFAT machine gun]] with a better field of fire.

The aircraft had three nose-mounted 12.7&nbsp;mm (0.5&nbsp;in) Breda machine guns with 400, 450 and 400 rounds respectively. Another Breda (7.7&nbsp;mm/0.303&nbsp;in caliber, with 250-500 rounds) with a high arc of fire, was fitted in the rear cockpit and controlled by a complex motorised electrical system. A modern "San Giorgio" [[Reflector sight|reflector gunsight]] was fitted, and there was also provision to mount a 20&nbsp;mm cannon instead of the central [[Breda-SAFAT]] machine gun in the nose.<ref name= "Lembo"/> The payload was composed of three bombs of 50&nbsp;kg (110&nbsp;lb), 100&nbsp;kg (220&nbsp;lb) and 250&nbsp;kg (550&nbsp;lb), or a Nardi dispenser for 119 2&nbsp;kg (4&nbsp;lb) bomblets. All these weapons gave the Ba.88 impressive armament for its time.<ref name= "Lembo"/>

The forward pilot's cockpit was fully instrumented, with an [[airspeed indicator]] capable of reading to 560&nbsp;km/h (350&nbsp;mph), [[gyroscope]] and an [[altimeter]] (useful to 8,000&nbsp;m/26,250&nbsp;ft).

===Testing and evaluation===
Despite its structural weight liabilities, the single-tailed prototype set a speed record over a 100&nbsp;km (60&nbsp;mi) circuit on 1 April 1937 by reaching 517.84&nbsp;km/h (322&nbsp;mph),<ref>[http://www.fai.org/fai-record-file/?recordId=9678 "FAI Record ID #9678"]. ''Fédération Aéronautique Internationale''. Retrieved 4 February 2017.</ref> taking the record away from France. Another record was obtained on 10 April 1937 when it achieved 475&nbsp;km/h (295&nbsp;mph) over 1,000&nbsp;km (620&nbsp;mi).<ref>[http://www.fai.org/fai-record-file/?recordId=8757 "FAI Record ID #8757"]. ''Fédération Aéronautique Internationale''. Retrieved 4 February 2017.</ref> Piloted by [[Furio Niclot Doglio]], the Ba.88 prototype had two 671&nbsp;kW (900&nbsp;hp) [[Isotta-Fraschini]] K 14 engines. This record speed was increased to 554&nbsp;km/h (344&nbsp;mph) when the modified prototype, using a double tail, was re-equipped with the definitive engines; the 746&nbsp;kW (1,000&nbsp;hp) Piaggio P.XI-RC40s. This time it broke German records in a 100&nbsp;km (60&nbsp;mi) stage at an average speed of 554.4&nbsp;km/h/344.5&nbsp;mph (with 1,000&nbsp;kg/2,200&nbsp;lb load) on 5 December 1937. Finally on 9 December 1937, another world record was set when averaging 524&nbsp;km/h (326&nbsp;mph) over 1,000&nbsp;km (621&nbsp;mi) with a 1,000&nbsp;kg (2,200&nbsp;lb) load.<ref name= "Lembo"/>

The Ba.88 had all the design specifications to be a very effective heavy fighter-bomber. It had a slim, streamlined shape (noted by all aviation observers), a rugged structure, heavy firepower, long range and high speed, with the same horsepower of [[medium bomber]]s such as the Br.20 (but at 9&nbsp;tonnes/10&nbsp;tons vs. 5&nbsp;tonnes/6&nbsp;tons). Despite its promising beginning, the addition of military equipment in the production series aircraft resulted in high [[wing loading]] and detrimental aerodynamic effects with a corresponding loss of performance, below any reasonable level. The contract was subsequently canceled, but production was later resumed, mostly for political reasons to avoid closing production lines of Breda and its satellite company IMAM.<ref name= "Lembo"/>

===Production===
Production numbers of the first series (production started in 1939) were 81 machines (MM 3962-4042) made by Breda, and 24 by IMAM (MM 4594-4617). The first series included eight trainers, with an elevated second pilot's seat. This was one of the few combat aircraft to have a dedicated trainer version, but it was not enough to prevent the overall failure of the programme.

The second series totalled 19 Breda (4246-4264) and 24 IMAM (MM:5486-5509) machines fitted with small engine cowling rings. There was a limited evolution in this series, with the second series mainly being sent straight to the scrapyard.<ref name="Mondey p. 27">Mondey 2006 p. 27.</ref>

==Operational history==
[[File:Breda Ba.88 line-up.jpg|300px|thumb|left|A line-up of Breda Ba.88 aircraft.]]Two ''Gruppi'' (Groups) were equipped with the Breda Ba.88 on June 1940, operating initially from [[Sardinia]] against the main airfield of [[Corsica]], with 12 aircraft on 16 June 1940 and three on 19 June 1940. The crews soon found that the Bredas were extremely underpowered and lacked agility, but the lack of fighter opposition resulted in them being able to perform their missions without losses.

Later, 64 aircraft became operational serving 7º ''Gruppo'' in the North African Theatre with 19º ''Gruppo'' stationed in Sardinia, but their performance remained extremely poor resulting in 7º ''Gruppo'' being grounded from the end of June until September, when the Italian offensive against British forces started. Of three aircraft used, one was not even capable of taking off, and another could not turn and was forced to fly straight from their base at [[Castelvetrano]] to Sidi Rezegh.

With anti-sand filters fitted, a maximum horizontal speed of 250&nbsp;km/h (155&nbsp;mph) was reported in some cases and several units were even unable to take off at all. These machines were fitted with "Spezzoniera" Nardi dispensers (with 119&nbsp;kg/262&nbsp;lb bomblets), 1,000 rounds for the three 12.7&nbsp;mm (0.5&nbsp;in) machine guns and 500 rounds for the 7.7&nbsp;mm (0.303&nbsp;in) Bredas. Although the weapons were not loaded to full capacity and the aircraft was lightened by eliminating the rear machine gun, observer, bombs and some fuel, lessening the weight did not substantially affect the aircraft's performance.

By mid-November, just five months after the start of the war on 10 June 1940, most surviving Ba.88s had been phased out as bombers and stripped of useful equipment, and were scattered around operational airfields as decoys for attacking aircraft.<ref name="Mondey p. 27"/> This was a degrading end for the new and theoretically powerful aircraft.<ref name= "Lembo"/> It forced the ''Regia Aeronautica'' to use totally outdated aircraft in North Africa, such as the [[Breda Ba.65]] and [[Fiat C.R.32]]. As an additional problem, the Regia Aereonautica remained without any suitable heavy fighter, a notable weakness for a major air arm.

Similar "heavy fighter-destroyer" projects were developed in several countries. In France, the [[Breguet Br.690]] even with only 1,044&nbsp;kW (1,400&nbsp;hp) was more capable than the Ba.88. Despite some problems of reliability, the Br.690 showed a practical usefulness that the Breda never achieved. It is notable that the Ba.88 was also a contemporary of the [[Messerschmitt Bf 110]], with no great differences in horsepower, weight, power to weight ratio or wingload. But the difference in success was immensely in the Bf 110's favour.

Niclot was the only pilot capable of flying this machine at its best (and only in the racer version which was much lighter), while the average pilot was not capable of using it effectively. Despite its impressive world records and aggressive, slim shape, the Ba.88 was a failure and unable to undertake combat missions. Its structure was too heavy, wing loading too high, and the engines were quite unreliable with insufficient power. The Piaggio P.XI was quite powerful, but never reliable, leading also to the overall failure of the [[Reggiane Re.2000]]. (Hungary substituted the engines with similar ones for the first license-produced examples).

Three Ba.88s were modified by the [[Agusta]] plant in late 1942 to serve as ground-attack aircraft. The '''Ba.88M''' had the wingspan increased by 2 meters (6&nbsp;ft 6½ in) to alleviate wing-loading problems, and featured dive brakes, [[Fiat A.74|Fiat A.74 RC.38]] engines, and a nose armament increased to four 12,7&nbsp;mm (0,5 in) Breda-SAFAT machine guns. Evaluated at [[Guidonia Montecelio|Guidonia]], they were delivered to the 103° ''Gruppo Autonomo Tuffatori'' at [[Lonate Pozzolo]] on 7 September 1943, the day before [[Italian Armistice]]. Later they were evaluated by [[Luftwaffe]] pilots and that was the last heard of the aircraft.<ref>Mondey 2006, pp. 27–28</ref>

==Variants==
;Ba 88 Lince
:The production ground attack and reconnaissance aircraft
;Ba 88M
:Three aircraft modified as dedicated ground-attack aircraft with Fiat A.74 engines increased wing-span and dive brakes.

==Operators==
;{{flag|Kingdom of Italy}}:[[Regia Aeronautica]]

==Specifications (Ba.88)==
{{Aircraft specs
|ref=<ref name="Greenaottr">{{cite book|last=Green|first=William|title=Aircraft of the Third Reich|publisher=Aerospace Publishing Limited|location=London|year=2010|edition=1st|pages=136–144|isbn=978-1-900732-06-2}}</ref>
|prime units?=met
<!--
        General characteristics
-->
|crew=2
|length m=10.79
|span m=15.6
|span note=<br/>
::'''Ba.88M:''' {{convert|17.6|m|ft|abbr=on|0}}
|height m=3.1
|wing area sqm=33.34
|empty weight kg=4,650
|max takeoff weight kg=6,750
<!--
        Powerplant
-->
|eng1 number=2
|eng1 name=[[Piaggio P.XI RC.40 Stella]]
|eng1 type=14-cyl. air-cooled radial piston engines
|eng1 kw=746
|eng1 note=for take-off<br/>
::'''Ba.88M:''' 2 x {{convert|742|kW|hp|abbr=on|0}} [[Fiat A.74 RC.38]] 14-cyl. air-cooled radial piston engines

|prop blade number=3
|prop name=variable-pitch propellers
<!--
        Performance
-->
|max speed kmh=490
|max speed note=at {{convert|4,000|m|ft|abbr=on|0}}
|range km=1,640
|ceiling m=8,000
|time to altitude={{convert|3,000|m|ft|abbr=on|0}} in 7 minutes 30 seconds
<!--
        Armament
-->
|guns= 3 x fixed {{convert|12.7|mm|in|abbr=on|3}} [[Breda-SAFAT]] machine-guns in the nose + 1x manually aimed {{convert|7.7|mm|in|abbr=on|3}} [[Breda-SAFAT]] machine-gun in the rear cockpit<br/>
::'''Ba.88M:'''  4 x fixed {{convert|12.7|mm|in|abbr=on|3}} [[Breda-SAFAT]] machine-guns in the nose + 1x manually aimed {{convert|7.7|mm|in|abbr=on|3}} [[Breda-SAFAT]] machine-gun in the rear cockpit
|bombs= up to {{convert|1,000|kg|lb|abbr=on|0}} of bombs in the internal weapons bay<br/>
:::'''or'''
::3 x {{convert|200|kg|lb|abbr=on|0}} bombs semi-recessed in the fuselage belly
}}

==See also==
{{aircontent|
|related=
|similar aircraft=
*[[Breguet 693]]
*[[Curtiss A-18 Shrike]]
*[[Fiat CR.25]]
*[[Focke-Wulf Fw 187]]
*[[Tupolev SB]]
|sequence= 
* ← [[Breda Ba.75|Ba.75]] - [[Breda Ba.79|Ba.79]] - [[Breda Ba.82|Ba.82]] - '''Ba.88'''
|see also=
|lists=
* [[List of Interwar military aircraft]]
* [[List of aircraft of World War II]]
* [[List of Regia Aeronautica aircraft used in World War II]]
}}

==References==
;Notes
{{reflist|colwidth=30em}}

;Bibliography
{{refbegin}}
* Angelucci, Enzo and Paolo Matricardi. ''World Aircraft: World War II, Volume I'' (Sampson Low Guides). Maidenhead, UK: Sampson Low, 1978.
* Donald, David, ed. "Breda Ba.88." ''The Encyclopedia of World Aircraft''. Etobicoke, Ontario, Canada: Prospero Books, 1997. ISBN 1-85605-375-X.
* Lembo, Daniele. "Breda 88 Lince. (in Italian)" ''Aerei Nella Storia n.44'' October 2005. Parma, Italy: West-ward Edizioni.
* Mondey, David. ''The Hamlyn Concise Guide to Axis Aircraft of World War II''. London: Bounty Books, 2006. ISBN 0-7537-1460-4.
{{refend}}

==External links==
{{commons category|Breda Ba.88}}
* [http://www.comandosupremo.com/Bredaba88.html Breda Ba.88]
* [http://1000aircraftphotos.com/APS/2473.htm Ron Dupas Collection No. 2473. Breda Ba 88 Lince (Lynx) Italian Air Force]
* FAI listings of the records set by the Ba.88 are in two different listings, due to differences in the way the model number was recorded: [http://records.fai.org/general_aviation/aircraft.asp?id=2796 Breda Ba-88 records]{{dead link|date=December 2016 |bot=InternetArchiveBot |fix-attempted=yes }}{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }} and [http://records.fai.org/general_aviation/aircraft.asp?id=3117 Breda 88 records]{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}
{{Breda aircraft}}

[[Category:Breda aircraft|Ba.088]]
[[Category:Italian attack aircraft 1930–1939]]
[[Category:World War II Italian ground attack aircraft]]