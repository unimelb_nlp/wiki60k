{{Multiple issues|
{{Orphan|date=August 2016}}
{{tone|date=August 2014}}
}}

{{italic title}}
'''''Hollywood and Swine''''' is a website that posts [[Satire|satirical]] articles about [[Hollywood]]'s A-list stars and show business news. ''Hollywood and Swine'' was created in January 2012 in Hollywood, CA by co-founders Andy Marx and William McArdle, who for a time remained anonymous. The idea for the website was inspired by the satirical news source ''[[The Onion]]'', and the name originates from the road intersection of [[Hollywood and Vine]] in Hollywood, CA. ''Hollywood and Swine'' uses photoshopped images and over-the-top headlines to play off of real news stories by adding a fake twist. Their efforts are supported by advertising sales. ''Hollywood and Swine'''s slogan is "So many pigs...so little time". Marx and McArdle stated that "the key [of ''Hollywood and Swine''] is to do something so funny that you can get away with being tasteless" (''The Guardian'', 2012). ''Hollywood and Swine'' has fast become a regular news source for those looking to the lives of celebrities as a source of entertainment.

== Growth ==

In March of 2012, ''Hollywood and Swine'' got their big break from publication of their satirical article "[[Starbucks]] Bans Screenwriters From All 19,435 Locations Worldwide; [[Writers Guild of America]] Vows to Fight the Decision". The article stated that writers bring a morose air into the coffee shop's environment and therefore were banned from coming inside. The article offended and outraged readers and consequently brought a considerable amount of traffic to the ''Hollywood and Swine'' website. Their second big break came from their article "[[Sharon Stone]] Named Suspect in [[Cannes]] Jewel Heist After Police Realize She Hasn't Been A Movie Star In Years". The article facetiously suggested that actress Sharon Stone was a suspect in a jewel heist because of her lack of movie presence at the time. The article created a great deal of controversy and resulted in an appreciable amount of traffic to the website. These two articles were catalysts in making ''Hollywood and Swine'' a Hollywood staple.

== Andy Marx and William McArdle ==

Andy Marx and William McArdle are the co-founders and writers of ''Hollywood and Swine''. Both Marx and McArdle attempted to stay anonymous, but in the end, their names were outed. Andy Marx is the grandson of both comedian and famed television and film actor [[Groucho Marx]], and famed songwriter [[Gus Kahn]]. He is the son of [[Arthur Marx]] and Irene Kahn, and the brother of Steve Marx. Marx, in addition to being a journalist, is an author, photographer, and a musician. In addition to being a writer, McArdle is also a screenwriter and has written the television/movie scripts ''Unhitched'', ''A Year of Living Without Her'' (written with Marx), ''Take Your Kid to Work Day'' (written with Marx), and ''Paroled'' (written with Marx).

== Reception ==

Although some find the ''Hollywood and Swine'' articles to be less than profound, many enjoy their take on the lives of the rich and famous. They are generally well received in Hollywood even by those who they have written articles about. Actor [[Edward Burns]], in reply to an article written about him stated, "It is mean spirited, but I got a good chuckle out of it. It was very funny" (''The Guardian'', 2012). On an average news day ''Hollywood and Swine'' has about 2,500-4,000 visitors. On a big news day, such as a day when an article of theirs goes viral, they can expect around 30,000 visitors to the site.

== Controversy ==

As one of their typical satirical posts, ''Hollywood and Swine'' released an article titled "Sharon Stone Named Suspect in Cannes Jewel Heist After Police Realize She Hasn't Been A Movie Star In Years". Without scanning the articles for validity first, the news outlets ''[[The Chicago Tribune]]'' and ''[[Yahoo! News]]'' both released the story as if it were real. Both received backlash for releasing satirical articles and mixing them with real news.

== References ==

{{reflist}}

*"Andy Marx." IMDb. http://www.imdb.com/name/nm0555583/IMDb.com, n.d. Web.
*Carroll, Rory. "Site's Satirical Take on Hollywood Pushes Boundaries – and Earns Raves." https://www.theguardian.com/film/2012/oct/02/hollywood-and-swine-pushes-boundaries. Guardian News and Media, 02 Oct. 2012. Web.
*Brennan, Jude. "Hollywood & Swine Skewers Hollywood." http://www.forbes.com/sites/judebrennan/2014/02/23/hollywood-swine-skewers-hollywood. Forbes. Forbes Magazine, 23 Feb. 2014. Web.
*Finke, Nikkie. "Hollywood & Swine’s Andy Marx & Will McArdle Peddling Projects This Summer: Will Industry Exact Revenge?" http://www.deadline.com/2013/06/hollywood-can/. Deadline.com. Deadline|Hollywood, 12 June 2013. Web.
*Marx, Andy, and William McArdle. "Humor: Sharon Stone Named Suspect in Cannes Jewel Heist." http://variety.com/2013/film/news/sharon-stone-named-suspect-in-cannes-jewel-heist-1200489578/ .Variety. N.p., n.d. Web. 18 June 2014.
* "About - Andy Marx." http://andymarx.com/about/ .ANDY MARX. N.p., n.d. Web. 1 July 2014.
*Horn, John. "Hollywood & Swine Pokes Fun at Its Writers' Industry." http://articles.latimes.com/2012/sep/09/entertainment/la-et-mn-hollywood-swine-20120910 .Los Angeles Times. Los Angeles Times, 09 Sept. 2012. Web. 1 July 2014.

== External links ==

*{{official website|hollywoodandswine.com}}

[[Category:2012 establishments in the United States]]
[[Category:Internet properties established in 2012]]