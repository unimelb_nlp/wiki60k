{{Infobox journal
| title = Discourse Studies 
| cover = [[File:Discourse Studies journal front cover image.jpg]]
| editor = Teun A van Dijk 
| discipline = [[Communication]]
| former_names = 
| abbreviation = Discourse Stud.
| publisher = [[SAGE Publications]]
| country = 
| frequency = Bimonthly
| history = 1999-present
| openaccess = 
| license = 
| impact = 1.603
| impact-year = 2014
| website = http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal200865
| link1 = http://dis.sagepub.com/content/current
| link1-name = Online access
| link2 = http://dis.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 475056445
| LCCN = 99023261 
| CODEN = 
| ISSN = 1461-4456 
| eISSN = 1461-7080
}}
'''''Discourse Studies''''' is a bimonthly [[Peer review|peer-reviewed]] [[academic journal]] that covers the field of [[discourse analysis]], especially articles that offer a detailed, systematic and explicit analysis of the structures and strategies of text and talk, their cognitive basis and their social, political and cultural functions. It specifically also publishes studies in [[conversation analysis]].  The journal was established in 1999 by [[Teun A. van Dijk]].

==Scope==
''Discourse Studies'' is a general journal for the study of text and talk. It features work on the structures and strategies of written and spoken discourse, with a particular focus on cross-disciplinary studies of text and talk in linguistics, communication studies, ethnomethodology, anthropology, cognitive and social psychology, and law. Articles on the socio-political aspects of discourse are published in its sister journal, ''[[Discourse & Society]]''. Articles that study the relations between discourse and communication are specifically published in ''[[Discourse & Communication]]''.

== Abstracting and indexing ==
''Discourse Studies'' is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2012 [[impact factor]] is 0.938, ranking it 31st out of 72 journals in the category "Communication".

==External links==
* {{Official website|1=http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal200865}}


[[Category:Communication theory]]
[[Category:Sociology journals]]
[[Category:Bimonthly journals]]
[[Category:SAGE Publications academic journals]]
[[Category:Publications established in 1999]]
[[Category:English-language journals]]