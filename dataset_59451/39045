{{good article}}
{{Use British English|date=May 2011}}
{{Infobox Holiday Camp
| camp_name          = Butlins Skegness
| logo               = Butlin's.svg
| logo_width         = 200
| image              = Butlins Skegness - geograph.org.uk - 1762469.jpg
| image_width        = 250 
| caption            = Butlins Skegness looking toward the Skyline pavilion.
| pushpin_map        = United Kingdom Lincolnshire
| location           = [[Ingoldmells]], [[Skegness]], [[Lincolnshire]], [[England]]
| subsequent_names   = Butlin's Skegness (1936–1987)<br>Funcoast World (1987–1999)<br>Butlins Resort Skegness (1999 – present)
| chain              = [[Butlins]]
| coordinates        = {{coord|53.18|0|0|N|0.35|0|0|E|display=inline,title}}
| opening_date       = 11 April 1936
| number_of_residences  = over 1,000<ref name="StudentPack" group="web"/>
| other_facilities   = [[Amusement Park]], [[Swimming pool]]
| site_area          = {{convert|200|acre|ha}}<ref name="StudentPack"  group="web"/>
| website           = http://www.butlins.com https://www.butlinsskegnesscaravans.com
}}
'''Butlins Skegness''' (officially '''Butlins Resort Skegness'''), formerly '''Butlin's Skegness''' or '''Funcoast World'''; is a [[holiday camp]] located in [[Ingoldmells]] near [[Skegness]] in [[Lincolnshire]], England. [[Billy Butlin|Sir William Butlin]] conceived of its creation based on his experiences at a Canadian summer camp in his youth and by observation of the actions of other holiday accommodation providers, both in seaside resort lodging houses and in earlier smaller holiday camps.

Construction of the camp began in 1935 and it was opened in 1936, when it quickly proved to be a success with a need for expansion.
The camp included dining and recreation facilities, such as dance halls and sports fields. During World War II, the camp was subject to a short military occupation when it served as a Naval training base, reverting to being a holiday camp in 1946. Over the past 75 years the camp has seen continuous use and development, in the mid-1980s and again in the late 1990s being subject to substantial investment and redevelopment.

In the late 1990s the site was re-branded as a holiday [[resort]], and remains open today as one of three remaining [[Butlins]] resorts.

==Holiday camp conception==
In 1914, Billy Butlin was living in [[Toronto]] with his mother and stepfather, when he left school and went to work for [[Eatons]] [[department store]]. According to Butlin, one of the best aspects of working for the company was that he was able to visit their [[Summer camps|summer camp]] which gave him his first taste of a real holiday&mdash;indeed a taste of what was to become a very big part of his life.<ref name="CBC1"  group="web">{{citation | title=Billy Butlin, holiday camp man.|publisher=The CBC Digital Archives Website.|author=Canadian Broadcasting Corporation.|date = 31 March 1960 (Last Updated:4 July 2008.)|url=http://archives.cbc.ca/lifestyle/leisure/clips/14873/|accessdate=26 April 2011}}</ref>

The onset of [[World War I]] led to his leaving Eatons and enlisting in the [[Canadian Expeditionary Force]] serving in Europe, but seeing little if any action.<ref group="notes">[[#refDacre1982|Dacre 1982]], pp.43-45</ref> After the war, Butlin made his way back to England where he used some of his last £5 (2011:£{{Formatprice|{{Inflation|UK|5|1919|2011}}}}) to purchase a stall in his uncle [[Marshall Hill]]'s travelling fair.<ref name="CBC1" group="web"/><ref name="Scott 2001, p.6" group="notes">[[#refScott2001|Scott 2001]], p.6</ref><ref name="SundayHerald" group="news">{{citation |title=Hoop-La Boy Now Juggles Millions|url=http://trove.nla.gov.au/ndp/del/article/18509519?searchTerm=Billy%20Butlin|publisher=The Sunday Herald (Sydney, NSW)|date=17 August 1952|accessdate=26 April 2011}}</ref>

As a showman, Butlin quickly became successful, one stall becoming several, and several becoming his own travelling fair.<ref name="SundayHerald" group="news"/><ref group="notes">[[#refDacre1982|Dacre 1982]], p.72</ref> Butlin soon had fixed fairground sites as well as his travelling fair – the first was at [[Olympia, London|Olympia]] in [[London]] outside [[Bertram Mills]]' Circus. In 1925 he opened a set of fairground stalls in [[Barry Island]], [[Wales]] where he observed the way landladies in [[Seaside resorts#British seaside resorts|seaside resorts]] would (sometimes literally) push families out of the lodgings between meals, and began to nurture the idea of a holiday camp similar to the one he had attended whilst an employee at Eatons.<ref name="CBC1" group="web"/>

In 1927 Butlin leased a piece of land from the [[Earl of Scarbrough]] by the seaside town of [[Skegness]], where he set up an amusement park with [[Hoopla (game)|hoopla stalls]], a [[Helter skelter (ride)|tower slide]], a [[Haunted attraction (simulated)|haunted house ride]] and, in 1928, a [[miniature railway]] and [[Dodgems|Dodgem cars]]&mdash;these were the first bumper cars in Britain, as Butlin had an exclusive license to import them.<ref name="Scott 2001, p.6" group="notes"/><ref name="butlins 1" group="web">{{cite web|title= Bygone Butlins website|url=http://www.bygonebutlins.com/skegness/|accessdate=11 October 2010|publisher=BygoneButlins.com|year=2007|work=Skegness}}</ref>

==Butlin's camp==
During the early 1930s Butlin joined the board of Harry Warner's holiday camp company (now [[Warner Leisure Hotels]]) and in 1935 he observed the construction of Warner's holiday camp in [[Seaton, Devon]].<ref name="Russell 2010 pg.38" group="notes">[[#refButlerRussell2010|Butler, Russell 2010]], p.38</ref> Butlin learned from the experience of Warner, and employed the workers who had constructed the Seaton camp to come to [[Lincolnshire]] to build his new camp at [[Skegness]].<ref name="Russell 2010 pg.38" group="notes"/>

[[File:One of Butlins original chalets - 1761986 cropped.jpg|thumb|One of Butlin's original chalets preserved and [[Listed building|listed]] in Skegness|left]]
Construction began in September 1935 with the local paper reporting the first sod being turned on the 4th of that month.<ref name="Scott 2001, p.11" group="notes">[[#refScott2001|Scott 2001]], p.11</ref> Butlin designed the camp himself and said of the camp, "my plans were for 1,000 people in 600 chalets with electricity, running water, 250 bathrooms, dining and recreational halls. A theatre, a gymnasium, a [[rhododendron]] bordered swimming pool with cascades at both ends and a boating lake."<ref name="butlins 2" group="web">{{cite web|title= Butlins Memories|url=http://www.butlinsmemories.com/skegness/index.htm|accessdate=3 May 2011|publisher=butlinsmemories.com|work=Butlins Memories}}</ref> However Butlin hired the architect [[Harold Ridley Hooper]],  to draw up the formal plans for the camp buildings.<ref name="Hooper" group="web">{{citation|title=Architectural Drawings of Colonel Harold Ridley Hooper, A.R.I.B.A. (1886–1953) and Others, 1882–1939|author=Harold Ridley Hooper|url=http://www.nationalarchives.gov.uk/a2a/records.aspx?cat=173-hg404&cid=-1#-1|publisher=The National Archives|year=1937|id=HG404/43|accessdate=26 April 2011}}</ref> In the camp's landscaped grounds, there were to be tennis courts, bowling and putting greens and cricket pitches.<ref name="butlins 2" group="web"/> The total cost of the project was £100,000 (2011:£{{Formatprice|{{Inflation|UK|100000|1935|2011}}}}) and despite having suffered a financial shortfall during construction, the camp opened on schedule in 1936.<ref name="Scott 2001, p.11" group="notes"/><ref name="butlins 2" group="web"/> One of the original 1936 chalet accommodation units is still present and is now a [[grade II listed]] building, recognising its historical significance.<ref group="web">{{IoE|196037|Gardeners Office, Butlins Holiday Camp}}</ref>

He opened his camp on 11 April 1936 ([[Easter Even]]). It was officially opened by [[Amy Johnson]] from [[Kingston upon Hull|Hull]], who had been the first woman to fly solo from England to Australia. An advertisement costing £500 (2011:£{{Formatprice|{{Inflation|UK|500|1936|2011|r=-3}}}}) was placed in the ''[[Daily Express]]'', announcing the opening of the camp and inviting the public to book for a week's holiday. The advertisement offered holidays with three meals a day and free entertainment with a week's full board, at a cost of between 35&nbsp;shillings (£1.75) and £3 (2011:£{{Formatprice|{{Inflation|UK|3|1936|2011}}}}), according to the time of year.<ref name="Guardian1" group="news">{{citation|title=Hello, campers: Butlins is 70 years old|author=Duncan Campbell|url=https://www.theguardian.com/uk/2006/apr/13/travelnews.travel|publisher=The Guardian|date=13 April 2006|accessdate=31 October 2010}}</ref> The advert proved successful, and over the first summer season the capacity of the camp had to be increased from 500 to 2,000, to cope with the demand.<ref name="Scott 2001, p.11" group="notes"/>

When the camp opened, Butlin realised that his guests were not engaging with activities in the way he had envisioned, as most kept to themselves, and others looked bored. He asked [[Norman Bradford (Entertainer)|Norman Bradford]] (who was engaged as an engineer constructing the camp) to take on the duty of entertaining the guests which he did with a series of ice breakers and jokes. By the end of the night the camp was buzzing and the Butlin's atmosphere was born. From that point on, entertainment was at the very heart of Butlin's and Bradford became the first of the [[Butlins Redcoats|Butlin's Redcoats]]. That night Butlin decided that for his camp to work he would require an army of people to carry out the same job as Bradford, and the role of Redcoat was formed.<ref name="Redcoats1" group="web">{{citation |title=Butlins Memories-Redcoats|url=http://www.butlinsmemories.com/other/redcoats.htm |publisher=Butlinsmemories.com |accessdate=27 October 2010}}</ref>

In 1938 Butlin won the contract to supply amusements to the [[Empire Exhibition, Scotland 1938|Empire Exhibition]] in Glasgow.<ref group="notes">[[#refScott2001|Scott 2001]], p.21</ref> After the exhibition was complete, Butlin returned with some of the infrastructure. His [[Butlin's Clacton|Clacton]] camp and [[Sheerness]] amusement park each received miniature railways,<ref group="notes">[[#refScott2001|Scott 2001]], p.24</ref> while Skegness received a building in the shape of the "Butlin theatre" which was later renamed the "Gaiety".<ref group="news">{{citation |title=Lincolnshire past & present, Issues 31–38|publisher=Society for Lincolnshire History and Archaeology|year=1998|page=9|accessdate=28 April 2011}}</ref>

Butlin continued to increase the capacity of the camp until 3 September 1939 when the [[Second World War]] was declared. The next morning, the campers were sent home and the site was taken over by the [[Royal Navy]] for use as a training establishment.<ref name="Scott 2001, p.11" group="notes"/><ref name="butlins 1" group="web"/>

==Wartime use==
[[File:Recruits at HMS Royal Arthur.jpg|thumb|right|The Petty Officer in charge meets trainee Belgian sailors as they arrive at HMS ''Royal Arthur'']]
{{main|HMS Royal Arthur (shore establishment)}}
Once the Navy took over, the camp became known as HMS ''Royal Arthur'' and was used to train sailors for the war effort. In order to operate as a military base, many of the bright external colours were painted over, the dance hall became an [[armoury (military)|armoury]], and the rose beds were dug up, to become sites for [[air raid shelter]]s.<ref name="ReferenceA" group="notes">[[#refHailey2009|Hailey 2001]], p.201</ref>

While the outside was repainted, much of the interior décor went unchanged. Speaking of his time there [[George Melly]] reported that Royal Arthur had "a certain architectural frivolity inappropriate to a Royal Navy Shore Establishment." Melly went on to mention how the main reception still had a sky scene with clouds painted on the ceiling and a large artificial (though realistic) tree in the centre of it. He also noted that their meals were served from an approximation of an Elizabethan inn named "Ye Olde Pigge and Whistle".<ref name="Lavery" group="notes">[[#refLavery2009|Lavery 2009]], p.104</ref>

During the war, the German airforce bombed Royal Arthur 52 times,<ref name="ReferenceA" group="notes"/> including one incident on 21 August 1940 when an attack led to damage or demolition of 900 small buildings.<ref group="notes">[[#refJamesCox2000|James, Cox 2000]], p.139</ref>  By the end of the conflict however, the camp's condition was good enough that it took only 6 weeks for Butlin to repair the wartime damage and reopen the camp to the public on 11 May 1946.<ref name="Scott 2001, p.11" group="notes"/><ref name="ReferenceA" group="notes"/> After reopening, some signs of military occupation remained with one observer noting that the blankets supplied to campers retained the insignia of HMS Royal Arthur.<ref group="notes">[[#refDonald2006|Donald 2006]], p.36</ref>

==Later history==
In 1947, Butlin had experimented with opening an airfield attached to his camp at [[Butlin's Pwllheli|Pwllheli]]. Patrons could fly in, to be collected by a Redcoat and transported to the camp. [[Flight Magazine]] reported that "flying visitors were unanimous in their praise of what they saw and experienced", noting that the experience contrasted to the poor reputations the camps were given in the media of the period. Welcoming the experiment as a success, Butlin announced his intention to open airfields at his other camps.<ref group="news">{{cite journal|journal= Flight Magazine|title=Back on Monday . . . . .|date=2 October 1947|page=382|accessdate=28 April 2011}})</ref> The following year he opened his next airfield at Skegness and announced that visitors could fly to the camp on a service run by [[British European Airways|BEA]] from 26 June.<ref group="news">{{cite journal|journal= Flight Magazine|title=News in Brief|date=18 March 1948|page=311|accessdate=28 April 2011}})</ref> The airfield also allowed Butlin's to offer services such as pleasure trips and sightseeing trips, as well as allowing private charters.<ref name="Scott 2001, p.11" group="notes"/>

In 1948 Butlin's also opened the Ingoldmells Hotel, which was situated outside the camp on the main road.<ref name="butlins 2" group="web"/> In 1949, the hotel became the first in Skegness to have a television for the use of guests. Situated in the hotel's [[palm court]] function room, the TV was manufactured by [[Radio Gramophone Development|R.G.D.]] but could be subject to interference from the hotel's refrigerator.<ref group="news">{{cite news |title=TV in the Palm Court New Attraction at Ingoldmells Hotel|url=http://skegness.wordpress.com/2010/02/24/tv-first-at-ingoldmells-hotel/ |newspaper=Skegness News|date=28 December 1949|accessdate=3 May 2011}}</ref>

Butlin's had a long history of combining amusements with transport, starting with their first [[miniature railway]] at the Empire Exhibition in 1938. Skegness was to receive its own miniature railway in 1962.<ref group="notes">[[#refScott2001|Scott 2001]], p.53</ref> A chairlift system was installed at the same time.<ref group="notes">[[#refScott2001|Scott 2001]], p.90</ref> In 1965, the camp became home to the UK's first commercial monorail system. According to Peter Scott, who has researched the history of Butlin's transport systems, Butlin apparently got the idea for the system from [[Disneyland]]. Construction began in 1964 with the cars being manufactured locally; the total cost of the system was £50,000 (2011:£{{Formatprice|{{Inflation|UK|50000|1965|2011}}}}).<ref name="Scott 2001, p.91" group="notes">[[#refScott2001|Scott 2001]], p.91</ref> 
In 1974 a fire broke out in the kitchens of the Beachcomber Chinese restaurant leading to the complete destruction of the Princes building. In the [[Ballroom]] upstairs, a "Miss Personality Competition" was taking place when smoke was first spotted. The Redcoat in Charge of this event and the [[Master of Ceremonies|compère]] acted quickly, and were able to evacuate the building with no loss of life. To compensate for the loss of the entertainment venue, the fenceline was moved to encompass the Ingoldmells Hotel, which then became another venue.<ref group="web">{{cite web |title=The 1974 Butlins Skegness Fire: I Was There!|url=http://www.redcoatsreunited.com/74fire.html|last=Marriot|first=Alan J.|publisher=redcoatsreunited.com |accessdate=27 April 2011}}</ref><ref group="news">{{cite web|title=Skegness Butlins Fire 1974|url=http://skegness.wordpress.com/2011/03/10/skegness-butlins-fire-1974/  |publisher=Skegness Secrets |accessdate=1 May 2011}}</ref>

In 1987, the resort benefited from a £14&nbsp;million (2011:£{{Formatprice|{{Inflation|UK|14000000|1987|2011}}}}) investment and improvement scheme, following which the resort was known as Funcoast World.<ref name="butlins 2" group="web"/> Around this time, many of the structures were subject to change with the demolition of many chalets and some central buildings including the Windsor dining hall and Empire theatre. The 1980s saw the removal of the miniature railway and chairlift system, but also saw the construction of a new indoor swimming pool named the Funsplash and an outdoor fun pool.<ref name="SkegnessOverYears" group="web">{{cite web|title=Skegness over the years|url=http://www.butlinsmemories.com/skegness/overyears.htm|accessdate=3 May 2011|publisher=butlinsmemories.com|work=Butlins Memories}}</ref>

As with its [[Butlins Bognor Regis|Bognor Regis]] and [[Butlins Minehead|Minehead]] counterparts, the Skegness resort underwent further improvement work in 1998 with the construction of the Skyline Pavilion.<ref name="StudentPack" group="web"/> This tented structure is described by tourism writer Bruce Prideaux as a "Baby [[Millennium Dome]]" even though it pre-dated the dome. The Pavilion contains entertainment facilities such as stages, bars, restaurants, shops and amusements.<ref group="notes">[[#refPrideaux2009|Prideaux 2009]], p.81</ref>  The improvement programme also included the construction of 1,045 brand new accommodation units, making it the largest timber-frame construction project in Europe that year.<ref name="StudentPack" group="web">{{cite web |title= Butlins Student Pack |url=http://www.butlins.com/pdfs/student-pack.pdf|publisher=Butlins|accessdate= 27 April 2011}}</ref> At the same time, the company dropped its use of the possessive apostrophe, changing from Butlin's to Butlins;<ref group="news">{{cite news|url=http://www.independent.co.uk/life-style/pedantry-1238437.html|title=Pedantry|first=William|last=Hartston|date=10 September 1997|publisher=The Independent|accessdate=7 June 2011}}</ref> after the refurbishment the resort was renamed as Butlins Resort Skegness, as it remains to date.<ref name="butlins 2" group="web"/><ref name="SkegnessOverYears" group="web"/>

==Butlins Resort Skegness==
Today the resort caters for over 400,000 visitors per year with 350,000 being resident and 70,000 visiting for the day.<ref name="StudentPack" group="web"/> Along with the nearby [[Fantasy Island (UK amusement park)|Fantasy Island]] amusement park, the resort is the largest employer in the Skegness area,<ref group="web">{{citation|url=http://www.dwp.gov.uk/docs/jcpsc-district-info.pdf |page=66 |date=3 July 2009 |publisher=Department of Work and Pensions |title=Background Labour Market Information |work=Jobcentre Plus Support Contract Package Areas |deadurl=yes |archiveurl=https://web.archive.org/web/20140312225039/http://www.dwp.gov.uk/docs/jcpsc-district-info.pdf |archivedate=12 March 2014 |df=dmy }}</ref> and currently employs 1,200 staff each year, 40 of which make up the Redcoat team.<ref name="StudentPack" group="web"/> According to the national police profile of the local residents, a large proportion of the Butlins workforce is now Eastern European in origin.<ref group="web">{{citation| url=http://ukcrimestats.com/Neighbourhood/Lincolnshire_Police/Skegness|title=Neighbourhood Crime League Table: Skegness|publisher=ukcrimestats.com|date=1 April 2011}}</ref>

Over the years many of the attractions have been removed, including the monorail at the end of 2002.<ref name="SkegnessOverYears" group="web"/> However the resort still retains several swimming pools and a funfair. Today it provides a range of activities such as [[rock climbing]], [[fencing]], and [[archery]]. It also provides a wide range of entertainment, aided by the formation of strategic partnerships with popular brands, including [[The X Factor (TV series)|The X Factor]], [[Britain's Got Talent]], [[Thomas & Friends]], [[Brainiac: Science Abuse]], [[Guinness World Records]], [[Bob the Builder]], [[Pingu]] and [[Angelina Ballerina]].<ref name="StudentPack" group="web"/>

==Pop Culture & Other Influences==
In 1962 the Camp played its part in the formation of the rock band [[the Beatles]], when [[Paul McCartney]] and [[John Lennon]] visited to meet [[Ringo Starr]] who was playing drums with [[Rory Storm and the Hurricanes]] at the time, to offer Starr the drummers position with The Beatles, who had just secured a recording contract.<ref group="notes">[[#refHailey2009|Hailey 2001]], p.200</ref> Storm's group had a summer booking at the camp, following on from the previous year.<ref group="notes">[[#refHarry2005|Harry 2005]], p.3</ref> It was in 1960 whilst playing Butlin's [[Butlin's Pwllheli|Pwllheli]] camp that Richard Starkey developed his stage name Ringo Starr.<ref group="notes">[[#refHarry2005|Harry 2005]], p.5</ref> The first song McCartney himself ever sang in public was "[[Long Tall Sally]]", at a Butlin's talent competition.<ref group="notes">[[#refWhite2003|White 2003]], p.114-115</ref>

For several years between 1956 and 1959, the comedian and TV presenter [[Dave Allen (comedian)|Dave Allen]] worked as a Redcoat at the camp. For Allen it was his first experience of fame (within the community of campers) and he found that he could not escape it, saying, "You can't get away once they know you&mdash;unless you lock yourself in your chalet. If you put on a moustache and dark glasses they'd think you were doing a stunt."<ref group="notes">[[#refFletcher1974|Fletcher 1974]], p.150</ref>

Other acts who have performed at the camp during the careers include [[Laurel and Hardy]], [[T'Pau (band)|T'Pau]], and [[Suzi Quatro]].<ref name="StudentPack" group="web"/>

In his 2010 book ''How to Land an A330 Airbus (And Other Vital Skills for the Modern Man)'', [[James May]] included a section entitled "How to Escape from Butlins" - specifically, Butlins Skegness in the event of hostile forces using it as an internment camp. He reasoned that it had already proved easily convertible to a naval base and that Britain was unprepared for sudden invasions as a result of overseas deployments. He suggests a tunnel in the style of ''[[The Great Escape (film)|The Great Escape]]'', only with help from a microboring machine to aid the process, from a chalet to the static caravan park.

In September 2016, after [[Burning Man]] organizers concocted a [[PR stunt]] claiming [[David Bowie]]'s ashes had been publicly dispersed on [[Black Rock Desert|the playa]] that year, Bowie's son [[Duncan Jones]] denied the claim, jocularly adding, "We all know if dad DID want his ashes scattered in front of strangers, it would be at the Skegness Butlins."<ref>Petit, S., [http://www.people.com/article/david-bowie-son-denies-ashes-spread-burning-man "David Bowie's Son Denies Claims That Musician's Ashes Were Scattered at Burning Man"], [[People (magazine)|''People'']], Sep 12, 2016.</ref>

==Bibliography==
*{{cite book
  |ref=refButlerRussell2010
  |last1=Butler
  |first1=Richard
  |last2=Russell
  |first2=Roslyn A.
  |title=Giants of Tourism
  |publisher=CABI
  |year=2010
  |isbn=1-84593-652-3
  |url=https://books.google.com/books?id=ssSijGtEWyAC
  |accessdate=26 April 2011}}
*{{cite book
  |ref=refDacre1982
  |title=The Billy Butlin Story
  |first= Peter
  |last=Dacre
  |publisher=Robson Books
  |year=1982
  |isbn=0-86051-864-7
  |url=https://books.google.com/books?id=i-tKAAAAYAAJ
  |accessdate=26 April 2011}}
*{{cite book
  |ref=refDonald2006
  |last=Donald
  |first=Thomas
  |title=Villains' paradise:a history of Britain's underworld
  |publisher=Pegasus Books
  |year=2006
  |isbn=1-933648-17-1
  |url=https://books.google.com/books?id=JaUDhjsMdcEC
  |accessdate=27 April 2011}}
*{{cite book
  |title=Beneath the surface: an account of three styles of sociological research
  |publisher=Taylor & Francis
  |year=1974
  |isbn=0-7100-7979-6
  |first=Colin
  |last=Fletcher (Ph.D.)
  |ref=refFletcher1974}}<!--|accessdate=3 May 2011-->
*{{cite book
  |ref=refHailey2009
  |last=Hailey
  |first=Charlie
  |title=Camps:a guide to 21st-century space
  |publisher=MIT Press
  |year=2009
  |isbn=0-262-08370-1
  |url=https://books.google.com/books?id=jv-91EAm7fQC
  |accessdate=26 April 2011}}
*{{cite web
  |ref=refHarry2005
  |first=Bill
  |last=Harry
  |url=http://triumphpc.com/mersey-beat/a-z/rorystorm-hurricanes.shtml
  |title=While My Guitar Gently Weeps: The Tragic Story of Rory Storm & the Hurricanes
  |publisher=[[Bill Harry]]
  |accessdate=28 April 2011}}
*{{cite book
  |ref=refJamesCox2000
  |last1=James
  |first1=T. C. G.
  |last2=Cox
  |first2=Sebastian
  |title=The battle of Britain
  |publisher=Routledge
  |page=139
  |year=2000
  |isbn=0 714651 23 0
  |url=https://books.google.com/books?id=Sh7vUCDA3ZkC
  |accessdate=26 April 2011}}
*{{cite book
  |ref=refLavery2009
  |last=Lavery
  |first=Brian
  |title=In Which They Served
  |publisher=Anova Books
  |year=2009
  |isbn=1-84486-097-3
  |url=https://books.google.com/books?id=dmeTMpPj_7gC
  |accessdate=26 April 2011}}
*{{cite book
  |ref=refPrideaux2009
  |last=Prideaux
  |first=Bruce 
  |title=Resort Destinations: Evolution, Management and Development
  |publisher=Butterworth-Heinemann
  |isbn=0-7506-5753-7
  |year=2009
  |url=https://books.google.com/books?id=6aDss75PQ9AC
  |accessdate=3 May 2011}}
*{{cite book
  |ref=refScott2001
  |last=Scott
  |first=Peter
  |title=A History of the Butlin's Railways
  |publisher=Peter Scott
  |date=February 2001
  |isbn=1-902368-09-6
  |url=https://books.google.com/books?id=97dmsRv3mI8C
  |accessdate=26 April 2011}}
*{{cite book
  |ref=refWhite2003
  |last=White
  |first=Charles
  |title=The Life and Times of Little Richard: The Authorized Biography
  |year=2003
  |publisher=Omnibus Press
  |isbn=978-0-7119-9761-5
  |url=https://books.google.com/books?id=ktmTr0sh5W4C}}

===Bibliographic Notes===
{{reflist|colwidth=30em|group="notes"}}

==References==
{{reflist|colwidth=30em}}

===Websites===
{{reflist|colwidth=30em|group="web"}}

===News and Journals===
{{reflist|colwidth=30em|group="news"}}

==External links==
{{Commons category|Butlins Skegness}}
* [http://www.butlins.com Butlins website]
* [http://www.butlins.com/resorts/skegness Skegness Resort] on main Butlins
* [http://www.butlinsmemories.com/skegness/index.htm Skegness] at Butlins Memories

{{Butlins}}
{{Use dmy dates|date=May 2011}}
{{good article}}

[[Category:Holiday camps]]
[[Category:Butlins camps]]
[[Category:Skegness]]