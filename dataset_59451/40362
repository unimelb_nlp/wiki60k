{{Infobox scientist
|name              = Weinan E
|image             = WeinanE.jpg
|caption           = Weinan E in 2003
|birth_place       = [[Jingjiang]], [[China]]
|nationality       = [[United States]]
|fields            = [[Applied mathematics]]
|workplaces        = [[Princeton University]]<br> [[Peking University]]
|alma_mater        = [[University of California, Los Angeles]]<br> [[Chinese Academy of Science]]<br> [[University of Science and Technology of China]]
|doctoral_advisor  = [[Björn Engquist]]
}}
{{Chinese name|[[E (surname)|E]]}}

'''Weinan E''' ({{zh|c=鄂维南|p=È Wéinán}}; born September 1963 in [[Jingjiang]]),  is a [[Chinese people|Chinese]] [[mathematician]]. He is known for his work in [[applied mathematics]], with applications to [[fluid mechanics]] and [[material science]].<ref>Applied Mathematics Entering the 21st Century: Invited Talks- 2004 Page 411 
James M. Hill, Ross Moore "The scientific work of Weinan E covers many areas of applied mathematics ranging from fluid dynamics to condensed matter physics, including incompressible flows, turbulence, statistical physics, superconductivity, liquid crystals and .."</ref> In addition, he has worked on [[multiscale modeling]] and the study of rare events.<ref>[http://www.math.princeton.edu/string/ Rare events, transition pathways and reaction rates]</ref>

He has also made contributions to [[Homogenization (mathematics)|homogenization theory]], theoretical models of [[turbulence]], stochastic partial differential equations, [[electronic structure]] analysis, multiscale methods, [[computational fluid dynamics]], and weak [[Kolmogorov–Arnold–Moser theorem|KAM theory]].  He is currently a professor in the Department of Mathematics and Program in Applied and Computational Mathematics at [[Princeton University]], and the Beijing International Center for Mathematical Research at [[Peking University]].

== Biography ==
E Weinan was born in [[Jingjiang]], [[China]]. He completed his undergraduate studies in the Department of Mathematics at [[University of Science and Technology of China]] in 1982, and his master's degree in Academy of Mathematics and Systems Science at [[Chinese Academy of Sciences]] in 1985.  He obtained his Ph.D. degree under the advice of [[Björn Engquist]] in the Department of Mathematics at [[University of California, Los Angeles]] in 1989. He then became a visiting member in [[Courant Institute]], [[New York University]] from 1989 to 1991, and a member in [[Institute for Advanced Study]] from 1991 to 1992.  After spending two more years as a long term member in [[Institute for Advanced Study]], he joined [[Courant Institute]], [[New York University]] as an associate professor in 1994, and became a full professor in 1997.  Since 1999, he has been holding a professorship in the Department of Mathematics and Program in Applied and Computational Mathematics at [[Princeton University]].  He has also been holding a professorship in the Beijing International Center for Mathematical Research (BICMR) since 2005.

He has made contributions to [[Homogenization (mathematics)|homogenization theory]], theoretical models of [[turbulence]], stochastic partial differential equations, [[electronic structure]] analysis, multiscale methods, [[computational fluid dynamics]], and weak [[Kolmogorov–Arnold–Moser theorem|KAM theory]].
In the study of rare events, he and collaborators have developed the string method and transition path theory.  In multiscale modeling, he and collaborators have developed the heterogeneous multiscale methods (HMM).  He has also made significant contributions to the mathematical understanding of the microscopic foundation to the macroscopic theories for solids.

== Awards ==
He received Presidential Early Career Award in Science and Engineering in 1996,<ref>[http://www.nsf.gov/news/news_summ.jsp?cntn_id=101827 Presidential Early Career Award in Science and Engineering] (1996)</ref> and Feng Kang Prize in Scientific Computing in 1999.<ref>[http://lsec.cc.ac.cn/fengkangprize/ewn.html Feng Kang Prize in Scientific Computing ] (1999)</ref>  He was the recipient of ICIAM Collatz Prize<ref>[http://www.iciam.org/council/node7_ct.html ICIAM Collatz Prize] (2003)</ref> at the 5th International Congress of Industrial & Applied Math for his work on industrial and applied mathematics, the Ralph E. Kleinman Prize in 2009,<ref>[https://www.princeton.edu/main/news/archive/S24/64/78G40/index.xml?section=science Ralph E. Kleinman Prize] (2009)</ref> and the Theodore von Kármán Prize in 2014.<ref>[http://www.siam.org/prizes/sponsored/vonkarman.php Theodore von Kármán Prize]</ref>  He was elected as a fellow of Institute of Physics in 2005, a fellow of SIAM in 2009,<ref>[http://www.siam.org/about/news-siam.php?id=1545 SIAM fellow](2009)</ref> and a member of the Chinese Academy of Sciences in 2011.<ref>[http://www.cas.cn/xw/zyxw/yw/201112/t20111209_3410911.shtml Member of the Chinese Academy of Sciences](2011)</ref> He was invited to speak at the International Congress of Mathematicians (2002), and at Annual Meeting of the American Mathematical Society (2003). In 2012 he became a fellow of the [[American Mathematical Society]].<ref>[http://www.ams.org/profession/fellows-list List of Fellows of the American Mathematical Society], retrieved 2012-11-10.</ref>

== Selected publications ==
[http://www.cambridge.org/gb/knowledge/isbn/item6453557/?site_locale=en_GB Book: Principles of Multiscale Modeling].

== References ==
{{Reflist}}

== External links ==
* [http://www.math.princeton.edu/~weinan/ Weinan E's professional webpage].
* [http://www.cambridge.org/gb/knowledge/isbn/item6453557/?site_locale=en_GB Book: Principles of Multiscale Modeling].
* [http://genealogy.math.ndsu.nodak.edu/id.php?id=33928 Weinan E] at the [http://genealogy.math.ndsu.nodak.edu/index.php Mathematics Genealogy Project].

{{Authority control}}

{{DEFAULTSORT:E, Weinan}}
[[Category:1963 births]]
[[Category:Living people]]
[[Category:Chinese mathematicians]]
[[Category:Writers from Taizhou, Jiangsu]]
[[Category:Educators from Jiangsu]]
[[Category:New York University faculty]]
[[Category:Princeton University faculty]]
[[Category:People's Republic of China science writers]]
[[Category:Fellows of the American Mathematical Society]]
[[Category:Scientists from Jiangsu]]
[[Category:American people of Wu descent]]