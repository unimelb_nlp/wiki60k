{{Use dmy dates|date=October 2012}}
{{Infobox Australian place| type = suburb
| name     = Hove
| city     = Adelaide
| state    = sa
| image    =
| caption  =
| lga      = City of Holdfast Bay
| postcode = 5048
| est      =
| pop      = 2,940
| pop_year = {{CensusAU|2011}}
| pop_footnotes = <ref name="ABS">{{Census 2011 AUS|id=SSC40305|name=Hove (State Suburb)|accessdate=3 February 2016|quick=on}}</ref>
| area     =
| stategov = [[Electoral district of Bright|Bright]]
| fedgov   = [[Division of Boothby|Boothby]]
| near-nw  =''[[Gulf St Vincent]]''
| near-n   = [[North Brighton, South Australia|North Brighton]]
| near-ne  =
| near-w   = ''[[Gulf St Vincent]]''
| near-e   = [[Warradale, South Australia|Warradale]]
| near-sw  =''[[Gulf St Vincent]]''
| near-s   = [[Brighton, South Australia|Brighton]]
| near-se  =
| dist1    =
| location1=
|latd=35.009
|longd=138.522
| alternative_location_map = Australia Greater Adelaide
}}

'''Hove''' is a coastal suburb of [[Adelaide]], [[South Australia]]. It is situated north of [[Brighton, South Australia|Brighton]], west of [[Warradale, South Australia|Warradale]], and south of [[North Brighton, South Australia|North Brighton]]. Running along the west of the suburb is the Esplanade, a street with numerous townhouses with views of the [[Gulf St Vincent]]. The suburb is bisected by its major thoroughfare, Brighton Road. Property prices are generally higher on the coastal side of Brighton Road.

==Public transport==
The [[Seaford railway line]] serves most of the suburb's public transport needs with the [[Hove railway station, Adelaide|Hove railway station]] situated just off Brighton Road. The 262, 263 and 265 bus routes which run from the Adelaide [[Central business district|CBD]] to [[Westfield Marion]] via [[Glenelg, South Australia|Glenelg]] also serve the suburb.

==Landmarks==
Major landmarks within the suburb include Brighton Road Oval, Marymount College and Townsend House, an iconic building which provides services for the hearing and sight impaired. The oldest landmark in Hove is the Brighton Town Hall that was constructed in 1869.  It was the civic centre for 90 years and the birthplace of local government. It still stands at the Hove rail crossing at 388 Brighton Road.

==Politics==
The suburb is split between the Brighton and Seacliff wards of the [[City of Holdfast Bay]] local government council. The suburb is represented by [[Australian Labor Party]] member [[Chloë Fox]] in the [[Parliament of South Australia|South Australian Parliament]] for the electorate of [[Electoral district of Bright|Bright]]. Federally, the suburb is located within the electorate of [[Division of Boothby|Boothby]] and is represented by [[Liberal Party of Australia|Liberal]] member Dr [[Andrew Southcott]].

==References==
{{Reflist}}

{{City of Holdfast Bay suburbs}}

[[Category:Suburbs of Adelaide]]


{{adelaide-geo-stub}}