{{good article}}
{{Infobox song
| Name = Cabaret
| Artist = [[Justin Timberlake]] featuring [[Drake (rapper)|Drake]]
| Album = [[The 20/20 Experience – 2 of 2]]
| Recorded = {{unbulleted list|May&nbsp;– July 2012|[[Jungle City Studios]] ([[New York City]])}}
| Genre = {{Flat list|
*[[Pop music|Pop]]
*[[Soul music|soul]]}} <!--- SOURCED IN THE 'COMPOSITION AND LYRICAL INTERPRETATION' SECTION --->
| Length = {{Duration|m=4|s=33}}
| Label = [[RCA Records|RCA]]
| Writer = {{flat list|
*Justin Timberlake
*[[Timbaland|Timothy Mosley]]
*[[Drake (rapper)|Aubrey Graham]]
*[[Jerome "J-Roc" Harmon]]
*[[James Fauntleroy]]
*Daniel Jones
}}
| Producer = {{flat list|
*[[Timbaland]]
*Justin Timberlake
*Jerome "J-Roc" Harmon
*Daniel Jones (add.)
}}
| prev = "True Blood"
| prev_no = 2
| track_no = 3
| next = "[[TKO (Justin Timberlake song)|TKO]]"
| next_no = 4
}}

"'''Cabaret'''" is a song recorded by American singer and songwriter [[Justin Timberlake]] for his fourth studio album, ''[[The 20/20 Experience – 2 of 2]]'' (2013). Featuring a [[Rapping|rap verse]] by Canadian rapper [[Drake (rapper)|Drake]], the song was written and produced by Timberlake, [[Timbaland|Timothy "Timbaland" Mosley]], [[Jerome "J-Roc" Harmon]] and Daniel Jones, with additional writing from [[James Fauntleroy]] and Drake. "Cabaret" is a [[Pop music|pop]] and [[Soul music|soul]] song which features beatboxing, handclapping, guitars and keyboards in its composition. Music critics noted its similarity to Timbaland's earlier works with singer [[Aaliyah]] and Timberlake's 2006 single "[[My Love (Justin Timberlake song)|My Love]]". Lyrically, it finds the singer comparing his love to a [[burlesque]] and features sexually oriented lyrics.

"Cabaret" received generally positive reviews from contemporary critics who praised Timbaland's production as well as Drake's rap verse on the song. As a result of the strong digital downloads following the release of the album, the track debuted on the charts in South Korea and the United States. It peaked at number 18 on the US [[R&B Songs]] chart and number 50 on the South Korean [[Gaon Chart]], selling over 3,800 digital copies for the week in the latter country. Timberlake included "Cabaret" on the set list of his fourth worldwide concert tour entitled [[The 20/20 Experience World Tour]] (2013-2014).

== Writing and production ==

"Cabaret" was written by Timberlake, [[Timbaland|Timothy "Timbaland" Mosley]], [[Drake (rapper)|Aubrey Graham]], [[Jerome "J-Roc" Harmon]], [[James Fauntleroy]] and Daniel Jones. The song was produced by Timbaland, Timberlake and Harmon while Jones provided additional production. Elliot Ives recorded the song at the [[Jungle City Studios]] in [[New York City]]. Timberlake arranged and produced his vocals. Harmon provided keyboards for the song, while Ives played the guitar. The track was engineered by Chris Godbey and mixed by Jimmy Douglass, Godbey and Timberlake; for the process they were assisted by Matt Webber. The keyboards were provided by Harmon and Jones.<ref name="Album Credits"/>

== Composition and lyrical interpretation ==
{{Listen
| pos = right
| filename = Justin Timberlake - Cabaret.ogg
| title = "Cabaret"
| description = A twenty-seven second sample of "Cabaret", a [[pop music|pop]] and [[soul music|soul]] song that features "stuttering beatboxing", mouth noises and classic sounding beat provided by the producer [[Timbaland]].
}}

"Cabaret" is a [[Pop music|pop]]<ref name="entertainmentwise">{{cite web|url=http://www.entertainmentwise.com/reviews/127960/ALBUM-REVIEW-Justin-Timberlake-The-2020-Experience-2-Of-2 |title=Album Review: Justin Timberlake&nbsp;– The 20/20 Experience 2 Of 2 |publisher=[[Entertainmentwise]] |last=Gravelle |first=Amy |date=September 30, 2013 |accessdate=October 30, 2014 |deadurl=yes |archiveurl=https://web.archive.org/web/20141117120613/http://www.entertainmentwise.com/reviews/127960/ALBUM-REVIEW-Justin-Timberlake-The-2020-Experience-2-Of-2 |archivedate=November 17, 2014 |df= }}</ref> and [[Soul music|soul]] song<ref>{{cite web|url=http://montrealgazette.com/entertainment/music/new-music-review-justin-timberlake-the-2020-experience-2-of-2-sony|title=New music review: Justin Timberlake, The 20/20 Experience&nbsp;– 2 of 2 (Sony)|work=[[Montreal Gazette]]|last=Dunlevy|first=T'cha|date=September 30, 2013|accessdate=October 30, 2014}}</ref> with a length of four minutes and thirty-three seconds (the shortest song on ''The 20/20 Experience - 2 of 2'')<ref name="hitfix"/> that features "stuttering beatboxing".<ref>{{cite web|url=http://www.avclub.com/review/justin-timberlake-emthe-2020-experience2-of-2em-103532|title=Justin Timberlake: The 20/20 Experience—2 of 2|work=[[The A.V. Club]]|last=Zaleski|first=Annie|date=October 1, 2013|accessdate=October 28, 2014}}</ref> Kyle Anderson of ''[[Entertainment Weekly]]'' noted that the song builds a symphony of mouth noises and compared it to [[Aaliyah]]'s 1998 single "[[Are You That Somebody?]]"<ref name="entertainmentweekly">{{cite web|url=http://www.ew.com/ew/article/0,,20738466,00.html|title=The 20/20 Experience 2 of 2 (2013)|work=[[Entertainment Weekly]]|last=Anderson|first=Kyle|date=October 9, 2013|accessdate=October 28, 2014}}</ref> Brad Stern of [[MTV Buzzworthy]] agreed with Anderson, however, labeled "Cabaret"  "as a much more 'dirty-minded'" version of the former.<ref name="buzzworthy">{{cite web|url=http://buzzworthy.mtv.com/2013/10/01/justin-timberlake-the-20-20-experience-2-of-2-review-gifs/|title=Justin Timberlake’s ‘The 20/20 Experience&nbsp;— 2 of 2′: A Track-By-Track Review... In GIFs!|publisher=[[MTV Buzzworthy]]|last=Stern|first=Brad|date=October 1, 2013|accessdate=October 29, 2014}}</ref> [[ABC News]]' Allan Raible noticed the song's beat has a classic sounding Timbaland beat, which—according to him— "the kind of beat Aaliyah used to turn into gold".<ref>{{cite web|url=http://abcnews.go.com/blogs/entertainment/2013/03/review-justin-timberlakes-the-2020-experience/|title=Review: Justin Timberlake’s ‘The 20/20 Experience’|publisher=[[ABC News]]|last=Raible|first=Allan|accessdate=October 29, 2014}}</ref> Arasia Graham of [[HipHopDX]] stated that "Cabaret" sounds like a 2013 answer to Timberlake's 2006 single "[[My Love (Justin Timberlake song)|My Love]]".<ref>{{cite web|url=http://www.hiphopdx.com/index/album-reviews/id.2153/title.justin-timberlake-the-20-20-experience-2-of-2|title=Justin Timberlake&nbsp;— The 20/20 Experience: 2 of 2|publisher=[[HipHopDX]]|last=Graham|first=Arasia|date=September 30, 2013|accessdate=October 28, 2014}}</ref>

Mikael Wood of ''[[Los Angeles Times]]'' stated that "Cabaret" is a "percolating bedroom jam that depicts sex as a form of at-home theater".<ref name="la times">{{cite news|url=http://www.latimes.com/entertainment/music/posts/la-et-ms-justin-timberlake-20130930-story.html|title=Review: Justin Timberlake edgier on '20/20 Experience&nbsp;— 2 of 2'|work=[[Los Angeles Times]]|date=September 30, 2013|accessdate=October 28, 2014}}</ref> ''[[Boston Globe]]'''s Sarah Rodman called the song a "sexed-up jam".<ref>{{cite news|url=http://www.bostonglobe.com/arts/music/2013/09/30/album-review-justin-timberlake-the-experience/kxFXGztzUgBDHZ9pBF6lJN/story.html|title=Album Review: Justin Timberlake, ‘The 20/20 Experience 2 of 2’|work=[[Boston Globe]]|last=Rodman|first=Sarah|date=October 1, 2013|accessdate=October 28, 2014}}</ref> [[HitFix]]'s Melinda Newman described the song as a "loop-driven to getting down with your lady, who’s taking off her clothes as quickly as she can".<ref name="hitfix"/> According to Eric Henderson of [[Slant Magazine]], in "Cabaret", Timberlake compares the love to a [[burlesque]] and noted that the song resembles producer Timbaland's earlier work.<ref>{{cite web|url=http://www.slantmagazine.com/music/review/justin-timberlake-the-20-20-experience-2-of-2|title=Justin Timberlake: The 20/20 Experience - 2 of 2|publisher=[[Slant Magazine]]|last=Henderson|first=Eric|date=September 28, 2013|accessdate=October 28, 2014}}</ref> Andy Kellman of [[Allmusic]] wrote that the song features Timbaland's signature sound and finds Timberlake proclaiming more "clever/nauseating" lyrics: "If sex is a contest, then you're coming first"; "'Cause I got you saying Jesus so much, it's like we're laying in a manger."<ref name="allmusic">{{cite web|url=http://www.allmusic.com/album/the-20-20-experience-2-of-2-mw0002569550|title=The 20/20 Experience - 2 of 2: Justin Timberlake|publisher=[[Allmusic]]|last=Kellman|first=Andy|accessdate=October 29, 2014}}</ref> The [[Refrain|chorus]] is consisted of Timberlake singing "It's a cabaret" while being accompanied by Timbaland, who repeats "Put on a show, get on the floor".<ref name="hitfix">{{cite web|url=http://www.hitfix.com/news/justin-timberlakes-the-2020-experience-2-of-2-track-by-track-review|title=Justin Timberlake's 'The 20/20 Experience 2 of 2': Track-by-track review|publisher=[[HitFix]]|last=Newman|first=Melinda|date=September 30, 2013|accessdate=October 29, 2014}}</ref> According to Stern, in his part with a duration about two minutes, Drake raps raunchy and fast-talking lines, "I’mma fuck you like we’re having an affair".<ref name="buzzworthy"/> Nick Krewen of ''[[Toronto Star]]'' wrote that Drake raps lines "about a sexual tête-à-tête amidst a melee of scattershot rhythms".<ref>{{cite news|url=http://www.thestar.com/entertainment/music/2013/10/01/justin_timberlakes_the_2020_experience_2_of_2_review.html|title=Justin Timberlake’s The 20/20 Experience&nbsp;— 2 of 2: review|work=[[Toronto Star]]|last=Krewen|first=Nick|date=October 1, 2013|accessdate=October 30, 2014}}</ref>

== Critical reception ==
[[File:Drake Bluesfest.jpg|thumbnail|left|[[Drake (rapper)|Drake]]'s rap verse on the song received praise from music critics]]

"Cabaret" received generally positive reviews from [[Music journalism|music critics]]. Even though giving mixed review to the album, Kellman of Allmusic called the song a "standout".<ref name="allmusic"/> Anderson of ''Entertainment Weekly'' chose "Cabaret" as the best song on the album, labeling it a "slick drum orgy with a ferocious Drake cameo".<ref name="entertainmentweekly"/> Similarly, Julia Leconte of ''[[Now (newspaper)|Now]]'' selected the song as a top track on the album and stated that it has catchy hook and cheesy lyrics which only Timberlake can pull off.<ref>{{cite web|url=http://www.nowtoronto.com/music/story.cfm?content=194635|title=Justin Timberlake&nbsp;— The 20/20 Experience 2 Of 2 (RCA)|work=[[Now (newspaper)|Now]]|last=Leconte|first=Julia|accessdate=October 28, 2014}}</ref> Mesfin Fekadu of ''[[The Huffington Post]]'' wrote that "the song is smooth and has an addictive hook".<ref>{{cite web|url=http://www.huffingtonpost.com/2013/09/30/justin-timberlake-the-2020-experience-2-of-2_n_4017355.html|title=Justin Timberlake's 'The 20/20 Experience 2 of 2' Album Is Full Of Leftovers (Review)|work=[[The Huffington Post]]|last=Fekadu|first=Mesfin|date=September 30, 2013|accessdate=October 29, 2014}}</ref> John Meagher of ''[[Irish Independent]]'' labelled the song "highly potent" and noted that it features Timberlake's "typical heavy-hitting approach".<ref>{{cite news|url=http://www.independent.ie/entertainment/music/music-review-john-meagher-justin-timberlake-2020-vision-29633309.html|title=Music Review: Justin Timberlake - 20/20 Vision|work=[[Irish Independent]]|last=Meagher|first=John|date=October 4, 2013|accessdate=October 29, 2014}}</ref>

''Los Angeles Times'''s Wood praised Drake's verse and wrote that he "takes the sex talk to a level that Timberlake the boy-band veteran still can't".<ref name="la times"/> HitFix's Newman graded the song with a "B-" mark, and wrote that "Drake comes in for a rap that works perfectly with the song in their first collaboration".<ref name="hitfix"/> Dave Hanratty of [[Drowned in Sound]] stated that "Cabaret" feels "navel-gazing" as a result of Drake's braggadocios appearance and the mechanical input given by Timbaland.<ref>{{cite web|url=http://drownedinsound.com/releases/17875/reviews/4146930?ticker|title=Album Review: Justin Timberlake&nbsp;— The 20/20 Experience – 2 of 2|publisher=[[Drowned in Sound]]|last=Hanratty|first=Dave|date=September 30, 2013|accessdate=October 29, 2014}}</ref> [[Entertainmentwise]]'s Amy Gravelle stated that Drake is featured on the track to add substance and style and proves that he was the "hottest" rapper at the moment.<ref name="entertainmentwise"/> A reviewer of [[Capital (radio network)|Capital FM]] stated that the song proves why Drake was the rapper on everyone's lips and further described "Cabaret" as a smart team-up between the artists backed by Timbaland's classic beats.<ref>{{cite web|url=http://www.capitalfm.com/artists/justin-timberlake/news/new-album-the-2020-experience-album-review/|title=Justin Timberlake's 'The 20/20 Experience 2 Of 2' Track By Track Album Review|publisher=[[Capital (radio network)|Capital FM]]|date=September 29, 2013|accessdate=October 29, 2014}}</ref> On the negative side, Lanre Bakare of ''[[The Guardian]]'' criticized the rapper's verse calling it "predictably self-indulgent".<ref>{{cite news|url=https://www.theguardian.com/music/2013/sep/26/justin-timberlake-the-20-20-experience-part-2|title=Justin Timberlake: The 20/20 Experience Part 2&nbsp;– review|work=[[The Guardian]]|last=Bakare|first=Lanre|date=September 26, 2013|accessdate=October 28, 2014}}</ref> [[Vibe (magazine)|Vibe]]'s Stacy-Ann Ellis noted that although Drake gives some "lover-boy swag" to the song, [[Jay-Z]] outperformed him with his verse on "Murder".<ref>{{cite web|url=http://www.vibe.com/article/review-leftovers-struggle-hit-spot-justin-timberlakes-2020-experience-2-2|title=Review: Leftovers Struggle To Hit The Spot On Justin Timberlake's 'The 20/20 Experience&nbsp;— 2 of 2'|work=[[Vibe (magazine)|Vibe]]|last=Ellise|first=Stacy-Ann|date=September 30, 2013|accessdate=October 30, 2014}}</ref>

In a review of ''The 20/20 Experience - 2 of 2'', Craig Manning of website [[AbsolutePunk]] wrote that even thought the song features "a rapidfire" rap verse by Drake, both "Cabaret" and the lead single, "[[Take Back the Night (song)|Take Back the Night]]", "feel somehow less impressive than a lot of the songs Timberlake was slinging last time around, if only because they don’t add up to an 'experience' greater than the sum of their parts."<ref>{{cite web|url=http://www.absolutepunk.net/showthread.php?t=3454031|title=Justin Timberlake&nbsp;— The 20/20 Experience 2 of 2|publisher=[[AbsolutePunk]]|last=Manning|first=Craig|date=September 30, 2013|accessdate=October 28, 2014}}</ref> According to Brice Ezell of [[PopMatters]], "for whatever reason [the song] never gets off of the ground."<ref>{{cite web|url=http://www.popmatters.com/review/175560-justin-timberlake-the-20-20-experience-2-of-2/|title=Justin Timberlake: The 20/20 Experience (2 of 2)|publisher=[[PopMatters]]|last=Ezell|first=Brice|date=September 30, 2013|accessdate=October 28, 2014}}</ref> Lydia Jenkin of ''[[New Zealand Herald]]'' opined that the song would be a lot of sexier without the overpowering bass pulses in its production and "some odd lyrics".<ref>{{cite news|url=http://www.nzherald.co.nz/entertainment/news/article.cfm?c_id=1501119&objectid=11133607|title=Album review: Justin Timberlake, The 20/20 Experience: 2 of 2|work=[[New Zealand Herald]]|last=Jenkins|first=Lydia|date=October 3, 2013|accessdate=October 30, 2014}}</ref>

== Live performances ==
[[File:Justin Timberlake - The 2020 Experience World Tour - Charlotte, North Carolina 03.jpg|thumbnail|Timberlake performing during [[The 20/20 Experience World Tour]] in 2014]]
Timberlake included "Cabaret" on the set list of his 2013-14 worldwide tour entitled [[The 20/20 Experience World Tour]].<ref>{{cite web|url=http://www.hollywoodreporter.com/review/concert-review-justin-timberlake-at-672976|title=Justin Timberlake Touts Suits, Ties and Tequila at Newly Renovated Forum: Concert Review|publisher=[[The Hollywood Reporter]]|last=Zemler|first=Emily|date=January 21, 2014|accessdate=October 29, 2014}}</ref> In a review of the concert that took place at the [[United Center]] in Chicago on February 16, Claudia Perry of the ''[[Chicago Tribune]]'' wrote "the whole apparatus returned to the A position (hey, Timberlake likes golf) while he performed 'Cabaret' and 'Take Back the Night'."<ref>{{cite news|url=http://articles.chicagotribune.com/2014-02-17/entertainment/ct-ent-0218-justin-timberlake-review-20140218_1_suit-tie-tennessee-kids-timberlake-8-p-m|title=Review: Timberlake does it all with flash at United Center|work=[[Chicago Tribune]]|last=Perry|first=Claudia|date=February 17, 2014|accessdate=October 29, 2014}}</ref>

== Credits and personnel ==

Credits adapted from the liner notes of ''The 20/20 Experience – 2 of 2''.<ref name="Album Credits">{{cite AV media notes |title=[[The 20/20 Experience – 2 of 2]] |others=[[Justin Timberlake]] |year=2013 |type=booklet |publisher=[[RCA Records]], a division of [[Sony Music Entertainment]] |location=[[New York City]], NY}}</ref>
;Locations
*Vocals recorded and mixed at [[Jungle City Studios]], New York City
;Personnel
{{Div col|cols=2}}
*[[Timbaland|Timothy "Timbaland" Mosley]]&nbsp;— production
*[[Justin Timberlake]]&nbsp;— vocals, mixing, production, vocal production, vocal arrangement
*[[J-Roc (record producer)|Jerome "J-Roc" Harmon]]&nbsp;— keyboards, production
*[[Drake (rapper)|Drake]] &nbsp;— vocals
*Daniel Jones&nbsp;— keyboards
*Chris Godbey&nbsp;— engineering, mixing
*Jimmy Douglass&nbsp;— mixing
*Elliot Ives&nbsp;— recording, guitar
{{Div col end}}

== Charts ==

Following the release of ''The 20/20 Experience – 2 of 2'', "Cabaret" debuted on charts in South Korea and the US. The song debuted at number 50 in South Korea,<ref name="South Korea Debut"/> with sales of 3,800 copies for that week ending October 5, 2013.<ref>{{cite web|url=http://gaonchart.co.kr/chart/download.php?f_chart_kind_cd=E&f_week=41&f_year=2013&f_type=week|title=Gaon Download Chart|publisher=Gaon Chart|accessdate=October 30, 2014}}</ref> Additionally, it debuted and peaked at number 18 on the US [[R&B Songs]] chart.<ref name="US-R&B-songs"/>

{| class="wikitable sortable plainrowheaders"
|-
! Chart (2013)
! Peak<br />position
|-
! scope="row"| South Korea ([[Gaon Chart]])<ref name="South Korea Debut">{{cite web|url=http://gaonchart.co.kr/chart/digital.php?f_chart_kind_cd=E&f_week=41&f_year=2013&f_type=week|title=Gaon Digital Chart|publisher=[[Gaon Chart]]|accessdate=October 30, 2013}}</ref>
| style="text-align:center;"| 50
|-
! scope="row"| US [[R&B Songs]] (''[[Billboard (magazine)|Billboard]]'')<ref name="US-R&B-songs">{{cite web | url=http://www.billboard.com/charts/2013-10-19/r-and-b-songs?order=gainer | title=R&B Songs: Oct 19, 2013 - (Biggest Jump) | work=[[Billboard (magazine)|Billboard]] | accessdate=May 15, 2014}}</ref>
| style="text-align:center;"| 18
|-
|}

== References ==
{{reflist|30em}}

== External links ==
*{{MetroLyrics song|justin-timberlake|cabaret}}<!-- Licensed lyrics provider -->

{{Justin Timberlake songs}}
{{Drake songs}}

[[Category:2013 songs]]
[[Category:Justin Timberlake songs]]
[[Category:Drake (rapper) songs]]
[[Category:Song recordings produced by Jerome "J-Roc" Harmon]]
[[Category:Song recordings produced by Justin Timberlake]]
[[Category:Song recordings produced by Timbaland]]
[[Category:Songs written by James Fauntleroy]]
[[Category:Songs written by Jerome "J-Roc" Harmon]]
[[Category:Songs written by Justin Timberlake]]
[[Category:Songs written by Timbaland]]
[[Category:Songs written by Drake (rapper)]]
[[Category:Songs about sexuality]]