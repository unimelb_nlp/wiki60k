{{good article}}
{{Infobox Grand Prix race report
|Type          = CUST
|Description   = Race 8 of 11 of the [[2014–15 Formula E season]]
|Country       = Germany
|Grand Prix    = Berlin
|GP_Suffix     = ePrix
|Details ref   = <ref name=timetable>{{cite web|title=Race Timetable|url=http://berlin.fiaformulae.com/en/guides/race-timetable.aspx|website=berlin.fiaformulae.com|publisher=FIA|accessdate=19 May 2015}}</ref>
|Image         = Schaltung flughafen Tempelhof.svg
|Caption       = Layout of the Berlin-Tempelhof Formula E street circuit
|Date          = 23 May
|Year          = 2015
|Official name = 2015 FIA Formula E DHL Berlin ePrix<ref>{{cite web|title=2015 Formula E&nbsp;– Berlin ePrix|url=http://berlin.fiaformulae.com/de.aspx|publisher=FIA|accessdate=17 May 2015}}</ref>
|Race_No       = 8
|Season_No     = 11
|Location      = [[Berlin Tempelhof Airport|Tempelhof airport]], [[Berlin]], [[Germany]]
|Course        = Temporary circuit
|Course_km     = 2.469
|Course_mi     = 1.534
|Distance_laps = 33
|Distance_km   = 81.477
|Distance_mi   = 50.622
|Weather       = 
|Pole_Driver   = [[Jarno Trulli]]
|Pole_Team     = [[Trulli GP|Trulli]]
|Pole_Time     = 1:21.547
|Pole_Country  = ITA
|Fast_Driver   = [[Nelson Piquet, Jr.]]
|Fast_Team     = [[China Racing|China]]
|Fast_Time     = 1:24.435
|Fast_Lap      = 20
|Fast_Country  = BRA
|First_Driver  = [[Jérôme d'Ambrosio]]
|First_Team    = [[Dragon Racing|Dragon]]
|First_Country = BEL
|Second_Driver = [[Sébastien Buemi]]
|Second_Team   = [[DAMS|e.dams]]
|Second_Country= SUI
|Third_Driver  = [[Loïc Duval]]
|Third_Team    = [[Dragon Racing|Dragon]]
|Third_Country = FRA
|Lapchart = <!--- {{FELaps2015|BER}} --->
}}

The '''2015 Berlin ePrix''', formally known as the '''2015 DHL Berlin ePrix''', was a [[Formula E]] [[Auto racing|motor race]] that took place on 23 May 2015 on the purpose-built [[Tempelhof Airport Street Circuit]] in [[Berlin]]. It was the eighth round of the [[2014–15 Formula E season]]. A special anti-clockwise track was built for the race next to the terminal building of the closed airport, including 17 turns over a distance of {{convert|2.469|km|3|abbr=on}}.

Prior to the ePrix, [[Lucas di Grassi]] was leading the Drivers' Championship by four points over [[Nelson Piquet, Jr.]], while [[DAMS|e.dams Renault]] led the Teams' Championship in front of second placed [[Abt_Sportsline#Formula_E|Audi Sport ABT]].

[[Jarno Trulli]] started the race from [[pole position]], but eventually retired. The race was initially won by championship leader Lucas di Grassi. After di Grassi's car was found to have used a modified front wing, the victory was handed to second-placed [[Jérôme d'Ambrosio]]. As a result of the race, Nelson Piquet, Jr. took the lead in the drivers' championship.

==Background==
On 11 July 2013, it was announced that the newly founded [[Formula E]], a class of [[auto racing]] for [[One-Design#Car racing|one-make]], [[Open-wheel car|single-seater]], [[Electric car|electrically powered]] racing cars, was set to race in [[Berlin]] on the apron of [[Berlin Tempelhof Airport|Tempelhof airport]],<ref>{{cite web|last1=Harberg|first1=Kalle|title=Der große Preis von Tempelhof|url=http://www.tagesspiegel.de/berlin/die-formel-e-kommt-nach-berlin-der-grosse-preis-von-tempelhof/8484848.html|publisher=Tagesspiegel|accessdate=17 May 2015|date=11 July 2013|language=German|trans_title=The Grand Prix of Tempelhof}}</ref> which was closed in October 2008.<ref>{{cite web|last1=Hebel|first1=Christina|title=Schließung des Flughafens Tempelhof: Endstation Wehmut|url=http://www.spiegel.de/politik/deutschland/schliessung-des-flughafens-tempelhof-endstation-wehmut-a-587235.html|website=spiegel.de|publisher=Spiegel online|accessdate=17 May 2015|date=30 October 2008|language=German|trans_title=Closure of Tempelhof Airport: terminal nostalgia}}</ref> In April 2014, the race was included in the [[Fédération Internationale de l'Automobile|FIA]]'s final calendar.<ref>{{cite web|title=FIA confirms Formula E updates following World Motor Sport Council|url=http://www.fiaformulae.com/en/news/2014/april/fia-confirms-formula-e-updates-following-world-motor-sport-council.aspx|publisher=FIA|accessdate=17 May 2015|date=11 April 2014}}</ref> Prior to the ePrix, [[Formula One]] had visited Berlin in {{F1|1959}} for the {{F1 GP|1959|German}} held at the [[AVUS]] highway track, a race marred by the death of Frenchman [[Jean Behra]], who died in an accident during a [[Formula Two]] support race.<ref>{{cite web|title=Brooks and Ferrari shine amid the tedium|url=http://en.espn.co.uk/f1/motorsport/story/16177.html|website=espn.co.uk|publisher=ESPN|accessdate=17 May 2015|date=2 August 1959}}</ref><ref>{{cite web|title=Vergilbter Glanz einer Legende|url=http://www.stern.de/auto/service/avus-rennstrecke-in-berlin-vergilbter-glanz-einer-legende-618838.html|website=stern.de|accessdate=17 May 2015|date=8 May 2008}}</ref> Local politicians in Berlin supported the race, with the Senator for Economy, Technology and Research, {{illm|Cornelia Yzer|de}} saying that Berlin "as the capital of [[Electric vehicle|electric mobility]]" was predestined for such an event. However, the [[Senate of Berlin]] did not agree to the organiser's plea to be allowed to hold the race at the central [[Straße des 17. Juni]] street overlooking the [[Brandenburg Gate]].<ref>{{cite web|last1=Busse|first1=Axel F.|title=Mit 225 Sachen durch Berlin|url=http://www.sueddeutsche.de/auto/formel-e-mit-sachen-durch-berlin-1.2476981-2|publisher=Süddeutsche Zeitung|accessdate=17 May 2015|language=German|date=17 May 2015}}</ref>

Coming into the race from [[2015 Monaco ePrix|Monaco]] two weeks earlier, [[Lucas di Grassi]] ([[Abt Sportsline#Formula E|Audi Sport ABT]]) was leading the championship with 93 points, four ahead of his compatriot [[Nelson Piquet, Jr.]] ([[China Racing|NEXTEV TCR]]). Another six points adrift, third placed [[Sébastien Buemi]] ([[DAMS#Formula E|e.dams Renault]]) was the only driver who scored two victories over the course of the season, but less consistent outings prevented him from placing higher up in the standings.<ref>{{cite web|title=Driver's Championship: Monaco&nbsp;– After Race|url=http://fiaformulae.alkamelsystems.com/Results/00_2014/08_Monaco/62_FIA%20Formula%20E%20Championship/201505092106_Drivers%20Championship.pdf|format=[[Portable Document Format|PDF]]|website=fiaformulae.com|publisher=FIA|date=9 May 2015|accessdate=10 August 2015}}</ref> In the teams' championship, e.dams Renault on 160 points had a 45-point lead over Audi Sport ABT.<ref>{{cite web|title=Team's Championship: Monaco&nbsp;– After Race|url=http://fiaformulae.alkamelsystems.com/Results/00_2014/08_Monaco/62_FIA%20Formula%20E%20Championship/201505092107_Teams%20Championship.pdf|format=[[Portable Document Format|PDF]]|website=fiaformulae.com|publisher=FIA|date=9 May 2015|accessdate=10 August 2015}}</ref>

===The circuit===
The {{convert|2.469|km|3|abbr=on}} anti-clockwise circuit was designed by Rodrigo Nunes, featuring 17 corners.<ref>{{cite web|title=Circuit Guide|url=http://www.fiaformulae.com/en/calendar/2016-berlin/berlin-circuit.aspx|publisher=FIA|accessdate=17 May 2015}}</ref><ref name=circuit>{{cite web|title=Berlin, Germany&nbsp;– Round 8|url=http://www.fiaformulae.com/en/calendar/2015-berlin/berlin-circuit.aspx|website=fiaformulae.com|publisher=FIA|accessdate=17 May 2015|archiveurl=https://web.archive.org/web/20150224193742/http://www.fiaformulae.com/en/calendar/2015-berlin/berlin-circuit.aspx|archivedate=23 August 2015}}</ref> It was described as a "twisty and challenging circuit" by Formula E driver [[Nick Heidfeld]] ([[Venturi Grand Prix|Venturi]]), who also stated that "overtaking will not be easy" due to the nature of the course.<ref name=circuit /> Construction of the track started 14 May, nine days prior to the race.<ref>{{cite web|title=Twitter post|url=https://twitter.com/gleventsuk/status/598754876064141313|website=twitter.com|publisher=GL Events UK|accessdate=17 May 2015|date=14 May 2015}}</ref>

===World record===
Between qualifying and the race, a new world record was set for the largest parade of electric vehicles, when 577 cars and scooters took to the track. The parade surpassed the previous record set in September 2014 in [[Silicon Valley]], [[United States]].<ref>{{cite web|title=World record set at DHL Berlin ePrix|url=http://www.fiaformulae.com/en/news/2015/may/world-record-set-at-dhl-berlin-eprix.aspx|website=fiaformulae.com|publisher=FIA|accessdate=1 June 2015|date=25 May 2015}}</ref><ref>{{cite web|last1=Jacobs|first1=Stefan|title=Rasende Staubsauger und ein Weltrekord|url=http://www.tagesspiegel.de/berlin/formel-e-in-berlin-tempelhof-rasende-staubsauger-und-ein-weltrekord/11818810.html|publisher=Der Tagesspiegel|accessdate=1 June 2015|language=German|date=23 May 2015}}</ref>

==Report==
All sessions took place on Saturday, 23 May 2015,<ref name=timetable /> in contrast to Formula One, where all sessions are divided up over a three- to four-day period.<ref>{{cite web|title=Practice and qualifying|url=http://www.formula1.com/content/fom-website/en/championship/inside-f1/rules-regs/Practice_qualifying_and_race_start_procedure.html|publisher=FIA|accessdate=23 August 2015}}</ref>

===Free practice===
[[File:Formel E in Berlin 2.JPG|thumb|Cars pass through turn ten.]]
Two free practice sessions were held before qualifying, running for 45 and 30 minutes respectively.<ref name=timetable /> The first session started at 08:15 local time, with Sébastien Buemi topping the timesheets for [[DAMS|e.dams-Renault]] in a time of 1:23.158.<ref name=result /> The track got faster as the temperatures rose and more cars drove around the circuit; lap times improved by more than a second in the second session, which started at 10:30. This time, it was championship leader Lucas di Grassi who topped the timesheets with a lap time of 1:22.032.<ref name=result />

===Qualifying===
In contrast to other racing series such as Formula One, Formula E has a specific qualifying mode, in which the twenty drivers are divided up into four groups, leaving enough space on track for everyone to produce lap times without interference by other drivers. Each group had ten minutes on track and the fastest time of each driver determined the grid position. A lottery determined which drivers started in which group. With five-minute breaks between the groups, qualifying had an overall length of 55 minutes.<ref name=rules>{{cite web|title=Rules & Regulations|url=http://www.fiaformulae.com/en/guide/rules-and-regs.aspx|website=fiaformulae.com|publisher=FIA|accessdate=1 June 2015}}</ref>

Racing veteran [[Jarno Trulli]] ([[Trulli GP]]) started in the first group, as qualifying began at 12:00, and posted a strong time of 1:21.547 early in the session, being the first to lap the course in under 1:22. Also in the group was Nelson Piquet, Jr.&nbsp;– second in the championship&nbsp;– who ultimately managed only 13th place on the grid. As qualifying progressed, championship leader Lucas di Grassi and Monaco winner Sébastien Buemi came close to beating Trulli's time, but proved unable to do so, handing Trulli a surprising<ref name=formulaspy>{{cite web|last1=Murphy|first1=Luke|title=Berlin ePrix: di Grassi exclusion gifts D'Ambrosio victory|url=https://formulaspy.com/features/berlin-eprix-di-grassi-exclusion-gifts-dambrosio-victory-14437|website=formulaspy.com|accessdate=1 June 2015|date=27 May 2015}}</ref> [[pole position]], his first in Formula E.<ref>{{cite web|title=Trulli storms to pole in Berlin|url=http://berlin.fiaformulae.com/en/news/trulli-storms-to-pole-in-berlin.aspx|website=berlin.fiaformulae.com|publisher=FIA|accessdate=1 June 2015}}</ref>

===Race===
A special feature of Formula E was the "Fan Boost" feature, an additional {{convert|30|kW}} of power to use for five seconds during the race. The three drivers, who were allowed to use the boost, were determined by a fan vote.<ref name=rules /> For the Berlin race, Nelson Piquet, Jr., Sébastien Buemi and [[Charles Pic]] (NEXTEV TCR) were handed the extra power. For Piquet, it was the third boost in a row, while both Buemi and Pic were able to use it for the first time.<ref name=formulaspy />

[[File:Formula E - Berlin 2015 - Daniel Abt.jpg|thumb|Local driver [[Daniel Abt]] spun early in the race, leaving him out of the points.]]
At the start of the race, Lucas di Grassi challenged Jarno Trulli for the lead, but Trulli closed the racing line into turn one. As Trulli lost the rear end of the car going into turn three, di Grassi took the lead. The other Audi Sport Abt of [[Daniel Abt]] was less fortunate, being sent into a spin at the same corner, dropping to the back of the field. Nelson Piquet, Jr. immediately made up for ground lost in qualifying, gaining three positions during the first lap, running in tenth place. At turn six on the following lap, he went past [[Stéphane Sarrazin]] (Venturi) for ninth. While di Grassi built an early lead, several manoeuvres were made behind him, with [[Nicolas Prost]] (e.dams-Renault) losing two positions to [[Jérôme d'Ambrosio]] ([[Dragon Racing]]) and [[Vitantonio Liuzzi]] (Trulli GP), dropping back to seventh. Pole sitter Jarno Trulli looked to be in trouble with his car and lost positions continuously, running in thirteenth after lap twelve. Third placed Nick Heidfeld was also unable to hold onto his podium position, being overtaken by d'Ambrosio on lap thirteen.

[[File:Formula E - Berlin 2015 - Loic Duval.jpg|thumb|left|[[Loïc Duval]] finished third, his first podium finish in [[Formula E]].]]
The mandatory pit stops, during which all drivers need to change into a second car,<ref name=rules /> started on lap 17. Everyone came in at this point except the two NEXTEV TCR drivers Piquet and Charles Pic. D'Ambrosio managed to get ahead of Buemi into second place thanks to swift work from his crew. Piquet benefited from his late stop by moving ahead into eighth after everyone had pitted and having more power left to use towards the end of the race. Heidfeld ran into additional trouble during the second half of the race, losing two positions during the final laps to both [[Loïc Duval]] (Dragon Racing) and the fast running Piquet, who managed to finish fifth from thirteenth on the grid, even closing on Duval on the final lap. Meanwhile, di Grassi won comfortably from d'Ambrosio, the gap being more than twelve seconds.<ref name=formulaspy />

====Post-race====
After the chequered flag fell, Lucas di Grassi's car stopped on the way back to pit lane.<ref name=formulaspy /> Once his car returned to [[parc fermé]], it failed post-race scrutineering, and was found to have a modified front wing, leading to his disqualification.<ref>{{cite web|title=Di Grassi excluded from DHL Berlin ePrix|url=http://www.fiaformulae.com/en/news/2015/may/di-grassi-excluded-from-dhl-berlin-eprix.aspx|website=fiaformulae.com|publisher=FIA|accessdate=1 June 2015|date=23 May 2015}}</ref> This meant that Jérôme d'Ambrosio was handed his first Formula E victory, moving him into fifth place in the drivers' championship. Nelson Piquet, Jr. recovered from his "dismal" qualifying performance to finish an eventual fourth, taking the championship lead.<ref name=formulaspy /> While e.dams Renault retained their lead in the teams' championship, the strong result for the Dragon Racing squad meant they moved up into second.<ref name=ambrosio>{{cite web|title=Jerome is second to one|url=http://www.fiaformulae.com/en/news/2015/may/jerome-is-second-to-one.aspx|website=fiaformulae.com|publisher=FIA|accessdate=1 June 2015|date=25 May 2015}}</ref> The Abt team decided not to appeal the decision against Di Grassi, while emphasizing that the front wing did not lead to a performance advantage.<ref>{{cite web|title=Di Grassi wird disqualifiziert|url=http://www.sport1.de/motorsport/formel-e/2015/05/formel-e-lucas-di-grassi-disqualifiziert-heidfeld-in-berlin-sechster|publisher=Sport 1|accessdate=23 August 2015|date=23 May 2015|language=German|trans_title=Di Grassi is disqualified}}</ref> Di Grassi himself responded on [[Twitter]], writing: "They are trying to make me win this championship in the hard mode. Don't worry, we will be back to kick some Piquet and Buemi ass on track."<ref>{{cite web|last1=Straw|first1=Edd|title=Berlin Formula E: D'Ambrosio gets win as di Grassi disqualified|url=http://www.autosport.com/news/report.php/id/119141|website=autosport.com|accessdate=23 August 2015|date=23 May 2015}}</ref>

D'Ambrosio was delighted with the result, commenting: "It was a great weekend for us overall. I think in the last four races we've had the pace to be on the podium but we didn't always manage to put it all together in the race. [...] This time we did it and it paid off!" He however added regret about the fact that he and the team were unable to celebrate the win and a double podium at the respective ceremony.<ref name=ambrosio />

==Classification==

===Qualifying===
{| class=wikitable style="font-size:95%"
! {{Tooltip|Pos.|Qualifying position}}
! {{Tooltip|No.|Number}}
! Driver
! Constructor
! Time
! Gap
|-
! 1
| 10
| {{flagicon|ITA}} [[Jarno Trulli]]
| [[Trulli GP|Trulli]]
| '''1:21.547'''
|
|-
! 2
| 11
| {{flagicon|BRA}} [[Lucas di Grassi]]
| [[Abt Sportsline|Audi Sport ABT]]
| 1:21.623
| +0.076
|-
! 3
| 9
| {{flagicon|SUI}} [[Sébastien Buemi]]
| [[DAMS|e.dams-Renault]]
| 1:21.685
|  +0.138
|-
! 4
| 23
| {{flagicon|GER}} [[Nick Heidfeld]]
| [[Venturi Grand Prix|Venturi]]
| 1:21.710
| +0.163
|-
! 5
| 66
| {{flagicon|GER}} [[Daniel Abt]]
| [[Abt Sportsline|Audi Sport ABT]]
| 1:21.754
| +0.207
|-
! 6
| 7
| {{flagicon|BEL}} [[Jérôme d'Ambrosio]]
| [[Dragon Racing]]
| 1:21.861
| +0.314
|-
! 7
| 8
| {{flagicon|FRA}} [[Nicolas Prost]]
| [[DAMS|e.dams-Renault]]
| 1:21.911
| +0.364
|-
! 8
| 6
| {{flagicon|FRA}} [[Loïc Duval]]
| [[Dragon Racing]]
| 1:21.917
| +0.370
|-
! 9
| 30
| {{flagicon|FRA}} [[Stéphane Sarrazin]]
| [[Venturi Grand Prix|Venturi]]
| 1:21.978
| +0.431
|-
! 10
| 27
| {{flagicon|FRA}} [[Jean-Éric Vergne]]
| [[Andretti Autosport|Andretti]]
| 1:22.015
| +0.468
|-
! 11
| 18
| {{flagicon|ITA}} [[Vitantonio Liuzzi]]
| [[Trulli GP|Trulli]]
| 1:22.032
| +0.485
|-
! 12
| 28
| {{flagicon|USA}} [[Scott Speed]]
| [[Andretti Autosport|Andretti]]
| 1:22.096
| +0.549
|-
! 13
| 99
| {{flagicon|BRA}} [[Nelson Piquet, Jr.]]
| [[China Racing|NEXTEV TCR]]
| 1:22.310
| +0.763
|-
! 14
| 3
| {{flagicon|ESP}} [[Jaime Alguersuari]]
| [[Virgin Racing (Formula E team)|Virgin Racing]]
| 1:22.395
| +0.848
|-
! 15
| 2
| {{flagicon|GBR}} [[Sam Bird]]
| [[Virgin Racing (Formula E team)|Virgin Racing]]
| 1:22.437
| +0.890
|-
! 16
| 77
| {{flagicon|MEX}} [[Salvador Durán]]
| [[Team Aguri|Amlin Aguri]]
| 1:22.444
| +0.897
|-
! 17
| 88
| {{flagicon|FRA}} [[Charles Pic]]
| [[China Racing|NEXTEV TCR]]
| 1:22.464
| +0.917
|-
! 18
| 21
| {{flagicon|BRA}} [[Bruno Senna]]
| [[Mahindra Racing]]
| 1:22.575
| +1.028
|-
! 19
| 55
| {{flagicon|POR}} [[António Félix da Costa]]
| [[Team Aguri|Amlin Aguri]]
| 1:22.586
| +1.039
|-
! 20
| 5
| {{flagicon|IND}} [[Karun Chandhok]]
| [[Mahindra Racing]]
| 1:22.803
| +1.256
|-
! colspan=8|Source:<ref name=result>{{cite web|title=DHL Berlin ePrix|url=http://www.fiaformulae.com/en/results/berlin-eprix.aspx|website=fiaformulae.com|publisher=FIA|accessdate=1 June 2015}}</ref>
|}

===Race===
[[File:Lucas di Grassi, Abt, Berlin ePrix.JPG|thumb|[[Lucas di Grassi]] was disqualified for using non-standardised components.]]
{| class="wikitable" style="font-size: 95%"
! {{Tooltip|Pos.|Position}}
! {{Tooltip|No.|Car number}}
! Driver
! Team
! Laps
! Time/Retired
! Grid
! Points
|-
! 1
| 7
| {{flagicon|BEL}} '''[[Jérôme d'Ambrosio]]'''
| '''[[Dragon Racing]]'''
| 33
| 48:26.566
| 6
| '''25'''
|-
! 2
| 9
| {{flagicon|SUI}} '''[[Sébastien Buemi]]'''
| '''[[DAMS|e.dams-Renault]]'''
| 33
| +2.433
| 3
| '''18'''
|-
! 3
| 6
| {{flagicon|FRA}} '''[[Loïc Duval]]'''
| '''[[Dragon Racing]]'''
| 33
| +3.508
| 8
| '''15'''
|-
! 4
| 99
| {{flagicon|BRA}} '''[[Nelson Piquet, Jr.]]'''
| '''[[China Racing|NEXTEV TCR]]'''
| 33
| +3.975
| 13
| '''14'''{{ref|r1|1}}
|-
! 5
| 23
| {{flagicon|GER}} '''[[Nick Heidfeld]]'''
| '''[[Venturi Grand Prix|Venturi]]'''
| 33
| +13.046
| 4
|'''10'''
|-
! 6
| 30
| {{flagicon|FRA}} '''[[Stéphane Sarrazin]]'''
| '''[[Venturi Grand Prix|Venturi]]'''
| 33
| +13.335
| 9
| '''8'''
|-
! 7
| 27
| {{flagicon|FRA}} '''[[Jean-Éric Vergne]]'''
| '''[[Andretti Autosport|Andretti]]'''
| 33
| +13.678
| 10
| '''6'''
|-
! 8
| 2
| {{flagicon|GBR}} '''[[Sam Bird]]'''
| '''[[Virgin Racing (Formula E team)|Virgin Racing]]'''
| 33
| +14.055
| 15
| '''4'''
|-
! 9
| 18
| {{flagicon|ITA}} '''[[Vitantonio Liuzzi]]'''
| '''[[Trulli GP|Trulli]]'''
| 33
| +15.636
| 11
| '''2'''
|-
! 10
| 8
| {{flagicon|FRA}} '''[[Nicolas Prost]]'''
| '''[[DAMS|e.dams-Renault]]'''
| 33
| +16.602
| 7
| '''1'''
|-
! 11
| 55
| {{flagicon|POR}} [[António Félix da Costa]]
| [[Team Aguri|Amlin Aguri]]
| 33
| +16.797
| 19
|
|-
! 12
| 3
| {{flagicon|ESP}} [[Jaime Alguersuari]]
| [[Virgin Racing (Formula E team)|Virgin Racing]]
| 33
| +20.594
| 14
| 
|-
! 13
| 28
| {{flagicon|USA}} [[Scott Speed]]
| [[Andretti Autosport|Andretti]]
| 33
| +21.149
| 12
|
|-
! 14
| 66
| {{flagicon|GER}} [[Daniel Abt]]
| [[Abt Sportsline|Audi Sport ABT]]
| 33
| +23.688
| 5
| 
|-
! 15
| 88
| {{flagicon|FRA}} [[Charles Pic]]
| [[China Racing|NEXTEV TCR]]
| 33
| +25.491
| 17
|
|-
! 16
| 77
| {{flagicon|MEX}} [[Salvador Durán]]
| [[Team Aguri|Amlin Aguri]]
| 33
| +44.157
| 16
| 
|-
! 17
| 21
| {{flagicon|BRA}} [[Bruno Senna]]
| [[Mahindra Racing]]
| 33
| +46.257
| 18
|
|-
! 18
| 5
| {{flagicon|IND}} [[Karun Chandhok]]
| [[Mahindra Racing]]
| 33
| +52.703{{ref|r2|2}}
| 20
| 
|-
! 19
| 10
| {{flagicon|ITA}} [[Jarno Trulli]]
| [[Trulli GP|Trulli]]
| 31
| +2 laps
| 1
| '''3'''{{ref|r3|3}}
|-
! DSQ
| 11
| {{flagicon|BRA}} [[Lucas di Grassi]]
| [[Abt Sportsline|Audi Sport ABT]]
| 33
| Disqualified{{ref|r4|4}}
| 2
|
|-
! colspan=8|Source:<ref name=result />
|}
;Notes:
*{{note|r1|1}}&nbsp;– Two points for fastest lap.
*{{note|r2|2}}&nbsp;– 28 second time penalty for exceeding maximum power usage.
*{{note|r3|3}}&nbsp;– Three points for pole position.
*{{note|r4|4}}&nbsp;– Excluded from race for non-standardised components.

==Standings after the race==
Drivers or teams listed in '''bold''' were still able to take their respective titles.

{{col-start}}
{{col-2}}
;Drivers' Championship standings<ref>{{cite web|title=Driver's Championship: Berlin&nbsp;– After Race|url=http://fiaformulae.alkamelsystems.com/Results/00_2014/09_Berlin/62_FIA%20Formula%20E%20Championship/201505231607_Drivers%20Championship.pdf|format=[[Portable Document Format|PDF]]|website=fiaformulae.com|publisher=FIA|date=23 May 2015|accessdate=10 August 2015}}</ref>
{| class="wikitable" style="font-size: 95%;"
|-
!
!Pos
!Driver
!Points
|-
|align="left"| [[File:1uparrow green.svg|10px]]
| 1
| {{flagicon|BRA}} '''[[Nelson Piquet, Jr.]]'''
| align="right"| 103
|-
|align="left"| [[File:1uparrow green.svg|10px]]
| 2
| {{flagicon|SUI}} '''[[Sébastien Buemi]]'''
| align="right"| 101
|-
|align="left"| [[File:1downarrow red.svg|10px]]
| 3
| {{flagicon|BRA}} '''[[Lucas di Grassi]]'''
| align="right"| 93
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
| 4
| {{flagicon|FRA}} '''[[Nicolas Prost]]'''
| align="right"| 78
|-
|align="left"| [[File:1uparrow green.svg|10px]]
| 5
| {{flagicon|BEL}} '''[[Jérôme d'Ambrosio]]'''
| align="right"| 77
|}
{{col-2}}
;Teams' Championship standings<ref>{{cite web|title=Team's Championship: Berlin&nbsp;– After Race|url=http://fiaformulae.alkamelsystems.com/Results/00_2014/09_Berlin/62_FIA%20Formula%20E%20Championship/201505231608_Teams%20Championship.pdf|format=[[Portable Document Format|PDF]]|website=fiaformulae.com|publisher=FIA|date=23 May 2015|accessdate=10 August 2015}}</ref>
{|class="wikitable" style="font-size: 95%;"
|-
!
! Pos
! Constructor
! Points
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
| 1
| {{flagicon|FRA}} '''[[DAMS#Formula E|e.dams Renault]]'''
| 179
|-
|align="left"| [[File:1uparrow green.svg|10px]]
| 2
| {{flagicon|USA}} '''[[Dragon Racing]]'''
| 116
|-
|align="left"| [[File:1downarrow red.svg|10px]]
| 3
| {{flagicon|DEU}} '''[[Abt Sportsline#Formula E|Audi Sport ABT]]'''
| 115
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
| 4
| {{flagicon|CHN}} '''[[China Racing]]'''
| 107
|-
|align="left"| [[File:1uparrow green.svg|10px]]
| 5
| {{flagicon|GBR}} '''[[Virgin Racing (Formula E team)|Virgin Racing]]'''
| 98
|}
{{col-end}}
* <small>'''Note''': Only the top five positions are included for both sets of standings.</small>

==See also ==
* [[Sport in Berlin]]

==References==
{{reflist|30em}}

==External links==
{{Commons category|2015 Berlin ePrix}}
* {{official website|http://berlin.fiaformulae.com/en.aspx}}
* [http://www.fiaformulae.com/en/results/berlin-eprix.aspx Official results]

{{s-start}}
|- style="text-align:center"
|width="35%"|Previous&nbsp;race:<br/>'''[[2015 Monaco ePrix]]'''
|width="30%"|'''[[FIA Formula E Championship|FIA Formula E]] Championship<br/>[[2014–15 Formula E season|2014–15 season]]'''
|width="35%"|Next&nbsp;race:<br/>'''[[2015 Moscow ePrix]]'''
|- style="text-align:center"
|width="35%"|Previous&nbsp;race:<br/>'''N/A'''
|width="30%"|'''[[Berlin ePrix]]'''
|width="35%"|Next&nbsp;race:<br/>'''[[2016 Berlin ePrix]]'''
|- style="text-align:center"
{{s-end}}

{{FIA Formula E Championship}}

[[Category:2014–15 Formula E season|Berlin ePrix]]
[[Category:Berlin ePrix]]
[[Category:2015 in German motorsport|ePrix]]
[[Category:2015 in Berlin]]