{{refimprove|date=August 2016}}
{{Use dmy dates|date=March 2015}}
'''Genetic sexual attraction''' ('''GSA''') is incest between close relatives, such as siblings or half-siblings, a parent and offspring, or first and second cousins, who first meet as adults.<ref>[http://www.bbcamerica.com/content/334/index.jsp BBC America: Brothers and Sisters in Love] {{webarchive |url=https://web.archive.org/web/20081025090221/http://www.bbcamerica.com/content/334/index.jsp |date=25 October 2008 }}</ref>

The term was coined in the US in the late 1980s by Barbara Gonyo, the founder of ''Truth Seekers In Adoption'', a Chicago-based support group for [[Adoption|adoptees]] and their new-found relatives.<ref>{{cite news |title=Genetic sexual attraction |first=Alix |last= Kirsta |newspaper=[[The Guardian]] |date=17 May 2003 |url=https://www.theguardian.com/theguardian/2003/may/17/weekend7.weekend2}}</ref>

== Contributing factors ==
People tend to select mates who are like themselves, which is known as [[assortative mating]]. This holds both for physical appearances and mental traits. People commonly rank faces similar to their own as more attractive, trustworthy, etc. than average.<ref>{{cite journal|last=Penton-Voak|first=I.S.|title=Computer graphic studies of the role of facial similarity in judgements of attractiveness|journal=Current Psychology: Developmental, Learning, Personality, Social|date=Spring 1999|volume=18|issue=1|pages=104–117 |url=http://www.psyc.nott.ac.uk/research/vision/jwp/papers/pentonvoak1999.pdf |doi=10.1007/s12144-999-1020-4|display-authors=etal}}</ref> However, Bereczkei (2004) attributes this in part to childhood [[imprinting (psychology)|imprinting]] on the opposite-sex parent. As for mental traits, one study found a [[correlation]] of 0.403 between husbands and wives, with husbands averaging about 2 IQ points higher. The study also reported a correlation of 0.233 for [[extraversion]] and 0.235 for inconsistency (using [[Eysenck Personality Questionnaire|Eysenck's Personality Inventory]]). A review of many previous studies found these numbers to be quite common.<ref>{{cite journal|last=Watson|first=David|last2=Klohnen |first2=Eva C. |last3=Casillas |first3=Alex |last4=Nus Simms |first4=Ericka |last5=Haig |first5=Jeffrey |last6=Berry |first6=Diane S.|title=Match Makers and Deal Breakers: Analyses of Assortative Mating in Newlywed Couples|journal=[[Journal of Personality]]|date=1 October 2004|volume=72|issue=5|pages=1029–1068|doi=10.1111/j.0022-3506.2004.00289.x|pmid=15335336}}</ref>

[[Heredity]] produces substantial physical and mental similarity between close relatives. Shared interests and personality traits are commonly considered desirable in a mate. The [[heritability]] of these qualities is a matter of [[Nature versus nurture|debate]] but estimates are that [[IQ]] is about 80% heritable, and the [[Big Five personality traits|big five]] personality factors are about 50% heritable. These data are for adults in Western countries.<ref>{{cite journal|last=Bouchard|first=Thomas J.|title=Genetic Influence on Human Psychological Traits. A Survey|journal=[[Current Directions in Psychological Science]]|date=1 August 2004|volume=13|issue=4|pages=148–151|doi=10.1111/j.0963-7214.2004.00295.x}}</ref>

For the above reasons, genetic sexual attraction is presumed to occur as a consequence of genetic relatives meeting as adults, typically as a consequence of adoption. Although this is a rare consequence of adoptive reunions, the large number of adoptive reunions in recent years means that a larger number of people are affected.<ref>{{cite news|work=[[The Current (radio program)|The Current]] |title=Part 2: Genetic Sexual Attraction – Part One |url=http://www.cbc.ca/thecurrent/2009/200905/20090507.html |author=[[Bob McKeown]] |author2=Aziza Sindhu |publisher=[[CBC Radio]] |date=May 7, 2009}}</ref> If a sexual relationship is entered, it is known as [[incest]].

GSA is rare between people raised together in early childhood due to a ''reverse'' [[sexual imprinting]] known as the [[Westermarck effect]], which desensitizes them to later close sexual attraction. It is hypothesized that this effect [[Evolution|evolved]] to prevent [[inbreeding]].<ref>{{cite journal|last=Lieberman|first=Debra|author2=Tooby, John |author3=Cosmides, Leda |title=The architecture of human kin detection|journal=[[Nature (journal)|Nature]]|volume=445|issue=7129|pages=727–731|doi=10.1038/nature05510|pmid=17301784|pmc=3581061}}</ref><ref>{{cite journal|last=Fessler|first=Daniel M.T.|author2=Navarrete, C.David|title=Third-party attitudes toward sibling incest|journal=[[Evolution and Human Behavior]]|volume=25|issue=5|pages=277–294|doi=10.1016/j.evolhumbehav.2004.05.004}}</ref>

== Instances ==

=== Patrick Stübing and Susan Karolewski ===
A brother and sister couple in Germany, [[Patrick Stübing]] and Susan Karolewski, fought their country's anti-incest laws. They grew up separately, met in 2000 when he was 23 and she was 15. He moved in with his mother and sister and the couple had four children which began in January 2001, the month after their mother died. Their appeal was rejected in 2008, upholding Germany's anti-incest laws.<ref>Kate Connolly, [https://www.theguardian.com/world/2007/feb/27/germany.kateconnolly "Brother and sister fight Germany's incest laws"], ''[[The Guardian]]'', 27 February 2007. Accessed 20 May 2008.</ref><ref>Dietmar Hipp: [http://www.spiegel.de/international/germany/0,1518,540831,00.html "Dangerous Love: German High Court Takes a Look at Incest"]. ''[[Der Spiegel]]'', 11 March 2008.</ref>

=== Kathryn Harrison memoir ===
[[Kathryn Harrison]] published a memoir in the 1990s regarding her four-year incestuous relationship with her biological father, whom she had not seen for almost 20 years prior to beginning the relationship, titled ''The Kiss''.<ref>{{cite book|last=Harrison|first=Kathryn|title=The Kiss|year=1997|publisher=Avon Books, Inc.|isbn=0-380-73147-9|url=https://books.google.com/?id=bGZIfEqPX4cC&printsec=frontcover&dq=The+kiss+harrison#v=onepage&q&f=false}}</ref>

=== South African couple ===
A couple in South Africa who had been together for five years, had a child and discovered that they are brother and sister just before their wedding. They were raised separately and met as adults in college.<ref>STEWART MACLEAN, [http://www.dailymail.co.uk/news/article-2057081/Engaged-couple-discover-brother-sister-parents-meet-days-wedding.html "Engaged couple discover they are brother and sister when their parents meet just before wedding"], ''[[Daily Mail]]'', 3 November 2011. Accessed 9 November 2011.</ref>

=== Garry Ryan and Penny Lawrence ===
At age 18, Garry Ryan left his pregnant girlfriend and moved to the United States. The daughter, Penny Lawrence, grew up and later set out to find her missing father. When they  met, they "both felt an immediate sexual attraction". They then lived together as a couple and as of April 2012 were expecting their first child together.<ref>{{cite news |url=http://www.thejournal.ie/woman-carries-fathers-baby-and-claims-were-in-love-108502-Mar2011/ |title=Woman carries father’s baby and claims: We’re in love |work=The Journal |location=Dublin, Ireland |date=23 March 2011}}</ref>

=== British twin couple ===
In 2008 a UK court annulled a marriage between a couple on the grounds that they were (fraternal) twins. They had been separated at birth. <ref>http://www.dailymail.co.uk/news/article-507588/Shock-married-couple-discovered-twins-separated-birth.html</ref>

=== 2012 father-daughter couple ===
In August 2012, a 32-year-old father and his 18-year-old daughter in New Zealand were convicted of incest after they admitted to having an incestuous relationship, which began in August 2010 when the girl was 16. The incest continued until May 2012 and resulted in the couple having a daughter, who was born in 2011. The teen told the court she was in love with her father and that they had been living as 'husband and wife' after meeting each other in 2010.<ref name="BNONews2012">{{cite web |title=New Zealand father-daughter couple told to end incest |publisher=[[BNO News]] |date=29 August 2012 |url=http://www.bnonews.com/inbox/?id=1069 |accessdate=29 August 2012}}</ref>

== See also ==
* [[Accidental incest]]
* [[Kin recognition]]
* [[Laws regarding incest]]
* [[Oedipus Rex]]
* [[Gregorius]]
* [[Green-beard effect]]
*[[Westermarck effect]]

== Notes ==
{{reflist}}

== References ==
* {{cite journal | author=Bereczkei, Tamas| title=Sexual imprinting in human mate choice | journal= [[Proceedings of the Royal Society#Proceedings of the Royal Society B|Proceedings of the Royal Society B: Biological Sciences]] | year=2004 | volume=271 | issue=1544 | pages=1129–1134 | doi=10.1098/rspb.2003.2672 | pmid=15306362 | pmc=1691703|display-authors=etal}}

== Further reading ==
* {{cite journal |last1=Greenberg |first1=M. |last2=Littlewood |first2=R. |date=1995 |url=http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=PubMed&list_uids=7779767&dopt=Abstract |title=Post Adoption Incest and Phenotypic Matching: Experience, Personal Meanings and Biosocial Implications |journal=British Journal of Medical Psychology |volume=68 |issue=Pt. 1 |pp=29–44 |pmid=7779767 |doi=10.1111/j.2044-8341.1995.tb01811.x}}
* {{cite news |title=Genetic sexual attraction |first=Alix |last= Kirsta |newspaper=[[The Guardian]] |date=17 May 2003 |url=https://www.theguardian.com/theguardian/2003/may/17/weekend7.weekend2}}
* {{cite web |url= http://thegsaforum.com/ <!-- redirect http://www.geneticsexualattraction.com  --> |title= The GSA (Genetic Sexual Attraction) Forum }}

== External links ==
* {{cite news |url= https://www.theguardian.com/theguardian/2003/may/17/weekend7.weekend2 |title= Genetic sexual attraction |first= Alix |last= Kirsta |publisher= [[The Guardian]] |date= May 17, 2003 }}

{{Incest}}

[[Category:Adoption reunion]]
[[Category:Incest]]
[[Category:Sexual attraction]]