{{good article}}
{{Infobox ice hockey player
| position = [[Centre (ice hockey)|Centre]]
| image = Max Bentley.jpg
| image_size = 230px
| shoots = Left
| height_ft = 5
| height_in = 9
| weight_lb = 158
| played_for = [[Chicago Blackhawks]]<br />[[Toronto Maple Leafs]]<br />[[New York Rangers]]
| birth_date = {{birth date|1920|3|1|mf=y}}
| birth_place = [[Delisle, Saskatchewan]], Canada
| death_date = {{death date and age|1984|1|19|1920|3|1}}
| death_place = [[Saskatoon, Saskatchewan]], Canada
| career_start = 1940
| career_end = 1954
| halloffame = 1966
}}
'''Maxwell Herbert Lloyd''' "'''Max'''" '''Bentley''' (March 1, 1920 – January 19, 1984) was a [[Canada|Canadian]]  [[ice hockey]] [[forward (ice hockey)|forward]] who played for the [[Chicago Blackhawks]], [[Toronto Maple Leafs]], and [[New York Rangers]] in the [[National Hockey League]] (NHL) as part of a professional and [[senior hockey|senior]] career that spanned 20 years. He was a two-time [[Art Ross Trophy]] winner as the NHL's leading scorer, and in 1946 won the [[Hart Memorial Trophy|Hart Trophy]] as most valuable player.  He played in four [[National Hockey League All-Star Game|All-Star Games]] and was twice named to a post-season [[NHL All-Star Team|All-Star team]]. 

Bentley was one of six hockey-playing brothers, and at one point played with four of his brothers with the [[Drumheller Miners]] of the [[Alberta Senior Hockey League (1965–78)|Alberta Senior Hockey League]]. In [[1942–43 NHL season|1942–43]], he made NHL history when he played on the league's first all-brother line with [[Doug Bentley|Doug]] and [[Reg Bentley|Reg]].  He played five seasons in Chicago with Doug before a 1947 trade sent him to the Maple Leafs in one of the most significant transactions in NHL history to that point.  Bentley won three [[Stanley Cup]] championships with the Maple Leafs before spending a final NHL season with the Rangers in [[1953–54 NHL season|1953–54]].  He then returned to his home in Saskatoon to finish his playing career.  Considered one of the best players of his era, Bentley was inducted into the [[Hockey Hall of Fame]] in 1966. Bentley was named one of the [[100 Greatest NHL Players|NHL's 100 greatest players of all-time]] prior the [[NHL Centennial Classic|league's Centennial Classic]] in 2017.<ref>{{cite web|title=100 Greatest NHL Players|url=https://www.nhl.com/fans/nhl-centennial/100-greatest-nhl-players|website=NHL.com|accessdate=January 1, 2017|date=January 1, 2017}}</ref>

==Early life==
Bentley was born March 1, 1920, in [[Delisle, Saskatchewan|Delisle]], Saskatchewan.  He was the youngest of six boys, and one of thirteen children.  His father Bill was a native of [[Yorkshire]], England who emigrated to the United States as a child and became a [[speed skating]] champion in [[North Dakota]] before settling in Delisle.<ref name="Rivalries">{{cite book |last=Batten |first=Jack |last2=Johnson |first2=George |last3=Duff |first3=Bob |last4=Milton |first4=Steve |last5=Hornby |first5=Lance |title=Hockey Dynasties: Blue lines and bloodlines |year=2002 |publisher=Firefly Books |location=Buffalo |isbn=1-55297-676-9 |pages=14–19}}</ref> He became mayor and helped build the town's covered skating rink.  All of the Bentley children were athletes, and all six brothers played hockey.<ref name="LOH1on1">{{cite web |last=Shea |first=Kevin |url=http://www.hhof.com/htmlSpotlight/spot_oneononep196601.shtml |title=One on one with Max Bentley |publisher=Hockey Hall of Fame |date=2010-01-15 |accessdate=2011-08-05}}</ref>  Bill Bentley believed that all six boys could have played in the [[National Hockey League]] (NHL), though responsibilities on the family farm resulted in the eldest four boys spending the majority of their careers playing senior hockey on the [[Canadian Prairies]].<ref name="Rivalries" />

His father taught Bentley to play hockey on their farm, where the family patriarch believed the daily chores would give his children the strength to have strong shots. Bentley's father also taught him to use his speed to elude bigger and stronger opponents as he weighed only 155 pounds fully grown.<ref name="LOH1on1" />  He played two years in [[Rosetown, Saskatchewan]] between 1935 and 1937 where he led the Saskatchewan Intermediate league in scoring as a 16-year-old.<ref name="OGHHOF">{{cite book |last=Duplacey |first=James |last2=Zweig |first2=Eric |title=Official Guide to the Players of the Hockey Hall of Fame |publisher=Firefly Books |year=2010 |isbn=1-55407-662-5 |page=47}}</ref> He moved onto the [[Drumheller Miners]] of the [[Alberta Senior Hockey League (1936–41)|Alberta Senior Hockey League]] (ASHL) in 1937, leading that league in scoring while playing on a line with brothers Roy and Wyatt.  The trio were joined in Drumheller by [[Doug Bentley|Doug]] and [[Reg Bentley|Reg]] for the 1938–39 season. The family operated a gas station in town when not playing hockey.<ref name="LOH1on1" />

==Professional career==
===Chicago Blackhawks===
While playing for Rosetown, Bentley attended a tryout camp for the [[Boston Bruins]].  Believing him too small to play in the NHL, the Bruins sent him home.  He then traveled to Montreal for a tryout with the [[Montreal Canadiens|Canadiens]]. The team advised him to see a doctor who stated he had a heart condition, and that if he did not quit hockey, he would be dead within a year.<ref name="LOHBio">{{cite web |url=http://www.legendsofhockey.net/LegendsOfHockey/jsp/LegendsMember.jsp?mem=p196601&type=Player&page=bio&list= |title=Max Bentley biography |publisher=Hockey Hall of Fame |accessdate=2011-08-05}}</ref>  Bentley chose to continue playing, but developed into a [[hypochondriac]] following the diagnosis.  He constantly complained of aches, pains and ailments, and carried so many drugs and medications he was known as a "walking drug store".<ref name="PodnieksPlayers">{{cite book |last=Podnieks |first=Andrew |title=Players: The ultimate A–Z guide of everyone who has ever played in the NHL |publisher=Doubleday Canada |year=2003 |location=Toronto |isbn=0-385-25999-9 |page=59}}</ref>

He played two years of senior hockey in Drumheller, and one more with the [[Saskatoon Quakers]] in the [[Saskatchewan Senior Hockey League]] (SSHL) before playing his first professional games with the [[Providence Reds]] of the [[American Hockey League]] (AHL) in [[1940–41 AHL season|1940–41]]. He caught the attention of the [[Chicago Blackhawks]], and while the team was impressed with his play, they wanted him to start with their [[American Hockey Association (1926-42)|American Hockey Association]] (AHA) affiliate in Kansas City.  Bentley initially refused, and considered retiring.  He was convinced to report by Kansas City's coach, [[Johnny Gottselig]], and played only five games before injuries in Chicago led the Blackhawks to request a call-up.  Gottselig sent Bentley up, reuniting him with brother Doug who had joined Chicago in 1939.<ref name="LOHBio" /> Max played his first NHL game on November 21, 1940, against the Bruins.  He scored his first goal on December 1 against the [[New York Rangers]].<ref name="LOH1on1" />

In his third NHL season, [[1942–43 NHL season|1942–43]], Bentley scored 70 points to finish third in the league in scoring.<ref name="43LadyByng">{{cite news |url=https://news.google.com/newspapers?id=lBc_AAAAIBAJ&sjid=Ok8MAAAAIBAJ&pg=3810,1426759 |title=Max Bentley is honored |work=Windsor Star |date=1943-04-03 |accessdate=2011-08-05 |page=S3}}</ref> He finished three points behind brother Doug, who won the scoring title.<ref>{{cite web |url=http://www.legendsofhockey.net/LegendsOfHockey/jsp/LegendsMember.jsp?mem=p196401&type=Player&page=statsawards&list= |title=Doug Bentley statistics |publisher=Hockey Hall of Fame |accessdate=2011-08-05}}</ref> Max tied an NHL record by scoring four goals in one period of a 10–1 victory over the Rangers on January 28, 1943.<ref name="OGHHOF" />  He added three assists in the game, tying the league record at the time for points in one game with seven.<ref>{{cite news |url=https://news.google.com/newspapers?id=RRg_AAAAIBAJ&sjid=LU8MAAAAIBAJ&pg=3835,5549708 |title=Max Bentley gets seven points in a single game |work=Windsor Daily Star |date=1943-01-29 |accessdate=2011-08-07 |page=3}}</ref> He was called for only one [[penalty (ice hockey)|penalty]] during the season, and as a result was voted the winner of the [[Lady Byng Memorial Trophy|Lady Byng Trophy]] as the league's most sportsmanlike player.<ref name="43LadyByng" /> 

World War II had decimated the rosters of all NHL teams, and with the Blackhawks searching for players, Max and Doug convinced the team to sign their brother Reg.<ref name="LOH1on1" />  The trio made history on January 1, 1943, when they became the first all-brother line the NHL had seen.  Two nights later, Max and Doug assisted on Reg's first, and only, NHL goal, the only time in league history that a trio of family members recorded the goal and assists on a scoring play.  While Max and Doug were established NHL stars, Reg played only 11 games in his NHL career.<ref>{{cite web |url=http://www.legendsofhockey.net/LegendsOfHockey/jsp/SearchPlayer.jsp?player=11973 |title=Reg Bentley profile |publisher=Hockey Hall of Fame |accessdate=2011-08-05}}</ref>

Bentley's career was interrupted in 1943 when he joined the [[Royal Canadian Infantry Corps|Canadian Infantry Corps]].  He was briefly stationed in [[Victoria, British Columbia|Victoria]], British Columbia, where he completed the 1942–43 season playing with the Victoria Navy team then spent the following two years stationed in [[Calgary]] where he played with the Calgary Currie Army team in the Canadian military leagues.  He led the Alberta league in goals and points with 18 and 31 respectively in 1943–44.<ref name="OGHHOF" />

Following the war, Bentley returned to the Blackhawks where he was reunited with his brother Doug and joined on a line by [[Bill Mosienko]].  The trio, who were all small and exceptionally fast, were dubbed the "Pony Line" and emerged as one of the top scoring lines in the league.<ref name="PodnieksPlayers" /> Max [[List of past NHL scoring leaders|led the league in scoring]] with 61 points,<ref>{{cite news |url=https://news.google.com/newspapers?id=epJjAAAAIBAJ&sjid=QXoNAAAAIBAJ&pg=3423,1677411 |title=Max Bentley wins N.H.L. scoring title |work=Saskatoon Star-Phoenix |date=1946-03-19 |accessdate=2011-08-05 |page=11}}</ref> and was awarded the [[Hart Memorial Trophy|Hart Trophy]] as the league's most valuable player. He was the first Blackhawk to ever win the award.<ref>{{cite news |url=https://news.google.com/newspapers?id=ZhM_AAAAIBAJ&sjid=CE8MAAAAIBAJ&pg=2262,554734 |title=Hart Trophy awarded to fleet Max Bentley |work=Windsor Daily Star |date=1946-03-30 |accessdate=2011-08-05 |page=S3}}</ref>

Bentley again led the league in scoring in [[1946–47 NHL season|1946–47]], recording 72 points in 60 games.<ref name="LOHStats">{{cite web |url=http://www.legendsofhockey.net/LegendsOfHockey/jsp/LegendsMember.jsp?mem=p196601&type=Player&page=statsawards&list= |title=Max Bentley statistics |publisher=Hockey Hall of Fame |accessdate=2011-08-05}}</ref> He won the title on the final night of the season, finishing one point ahead of Montreal's [[Maurice Richard]].<ref name="PodnieksPlayers" /> In doing so, he became only the third player in NHL history to win consecutive scoring titles after [[Charlie Conacher]] and [[Sweeney Schriner]], both of whom accomplished the feat in the 1930s.<ref name="LOH1on1" /> He then played in the [[1st National Hockey League All-Star Game]] on October 13, 1947, for the NHL All-Stars, a 4–3 victory over the [[Toronto Maple Leafs]].<ref>{{cite news |url=https://news.google.com/newspapers?id=S89TAAAAIBAJ&sjid=hzgNAAAAIBAJ&pg=1266,1415143&dq=all-star+game&hl=en |title=Mosienko costly All-Star Game injury |work=Regina Leader-Post |date=1947-10-14 |accessdate=2011-08-05 |page=19}}</ref>

===Toronto Maple Leafs===
{{Quote box| quote="It is the biggest deal in NHL hockey in a long, long time and only goes to emphasize the worth of such a player as Bentley and puts him on a very high plane."|align=right |width=30%|source=NHL President Clarence Campbell's assessment of the trade that sent Bentley to Toronto.<ref name="TorontoTrade" />}}
Three weeks later and six games into the season, the Maple Leafs completed a deal to acquire Bentley.   He was sent to Toronto with [[Cy Thomas]] in exchange for [[Gus Bodnar]], [[Bud Poile]], [[Gaye Stewart]], [[Ernie Dickens]] and [[Bob Goldham]], on November 2, 1947.<ref name="LOHStats" /> The trade sent shockwaves throughout the league.  The five players sent to Chicago essentially formed an entire starting unit; NHL President [[Clarence Campbell]] stated he was "astounded" by the deal, and stated it ranked with the Maple Leafs' purchase of [[King Clancy]] in 1930 as one of the most significant transactions in league history.<ref name="TorontoTrade">{{cite news |url=https://news.google.com/newspapers?id=o_4uAAAAIBAJ&sjid=R9wFAAAAIBAJ&pg=2258,679246 |title=Hawks trade Max Bentley for five Leafs |work=Ottawa Citizen |date=1947-11-04 |accessdate=2011-08-05 |page=17}}</ref>  The trade was still being discussed weeks later as observers throughout the league attempted to assess which team received the better deal.<ref>{{cite news |url=https://news.google.com/newspapers?id=XBs_AAAAIBAJ&sjid=kU8MAAAAIBAJ&pg=5225,226771 |title=Who got the best deal, Hawks or the Leafs? |work=Windsor Daily Star |date=1947-12-06 |accessdate=2011-08-05 |page=S4}}</ref>  Bentley was initially disappointed to leave his brother in Chicago, but quickly adapted to Toronto where he was immediately popular.<ref name="LOHBio" />

With the Leafs, Bentley challenged for his third consecutive scoring title.<ref>{{cite news |url=https://news.google.com/newspapers?id=RB5lAAAAIBAJ&sjid=94cNAAAAIBAJ&pg=4135,2553332 |title=3rd straight scoring title is target for Max Bentley |work=Edmonton Journal |date=1948-01-28 |accessdate=2011-08-05 |page=6}}</ref> He eventually finished fifth with 54 points, seven behind [[Elmer Lach]]'s league-leading 61.<ref>{{cite news |url=https://select.nytimes.com/gst/abstract.html?res=FA0A11FB3E5F167B93C0AB1788D85F4C8485F9 |title=Second title for Lach |work=New York Times |date=1948-03-22 |accessdate=2011-08-05}}</ref>  The Leafs finished in first place in the regular season standings, then went on to win the [[Stanley Cup]] in a four-game sweep over the [[Detroit Red Wings]] in the [[1948 Stanley Cup Finals]].<ref>{{cite news |url=https://news.google.com/newspapers?id=bABQAAAAIBAJ&sjid=jVUDAAAAIBAJ&pg=3279,6061806 |title=Toronto sweeps Detroit to retain Stanley Cup |work=St. Petersburg Independent |date=1948-04-15 |accessdate=2011-08-05 |page=18}}</ref>  Bentley was overjoyed, stating: "I waited a long time for this.  A Stanley Cup championship at last!"<ref name="LOH1on1" />

Bentley and the Leafs struggled in the [[1948–49 NHL season|1948–49]] regular season. He fell to 41 points on the year&nbsp;– 31 less than his total of two seasons previous&nbsp;– while the Leafs finished fourth out of six teams.<ref name="LOH1on1" /><ref name="LOHStats" /> The team recovered in the playoffs, eliminating the Red Wings in four consecutive games for the second year in a row to win the team's third consecutive Stanley Cup.  Bentley scored the third goal in a 3–1 win in the deciding contest.<ref>{{cite news |url=https://news.google.com/newspapers?id=01wbAAAAIBAJ&sjid=QE0EAAAAIBAJ&pg=5082,559442 |title=Toronto wins Stanley Cup; Third in a row |work=The Pittsburgh Press |date=1949-04-17 |accessdate=2011-08-05 |page=40}}</ref>  

The Leafs' championship streak came to an end in [[1949–50 NHL season|1949–50]] but Bentley showed a modest improvement offensively, leading the team with 23 goals.  He contemplated retiring and returning to Saskatchewan, but chose to return to Toronto for the [[1950–51 NHL season|1950–51 season]].<ref name="LOH1on1" /> He finished the season with significantly improved scoring totals, finishing third in the league with 62 points, behind Maurice Richard's 66, and [[Gordie Howe]]'s league-record 86.<ref>{{cite news |url=https://news.google.com/newspapers?id=w4svAAAAIBAJ&sjid=etwFAAAAIBAJ&pg=6649,4729881 |title=Howe heads goals, points in NHL race |work=Ottawa Citizen |date=1951-03-24 |accessdate=2011-08-05 |page=18}}</ref> The Leafs faced the Canadiens in the [[1951 Stanley Cup Finals]], which was won by Toronto in five games.  Bentley finished with 13 points in the playoffs, tying him with Richard for the league lead.<ref>{{cite news |url=https://news.google.com/newspapers?id=UChkAAAAIBAJ&sjid=mHsNAAAAIBAJ&pg=7175,3129294 |title=Toronto wins Stanley Cup; Fourth time in five years |work=Calgary Herald |date=1951-04-23 |accessdate=2011-08-05 |page=20}}</ref>

After finishing the [[1951–52 NHL season|1951–52 season]] with 41 points,<ref name="LOHStats" /> Bentley contemplated his future in hockey.  He mused about an opportunity to coach the [[Calgary Stampeders (ice hockey)|Calgary Stampeders]] of the [[Western Hockey League (minor pro)|Western Hockey League]] (WHL) and stated a desire to play again with his brother Doug, but ultimately returned to Toronto.<ref>{{cite web |url=https://news.google.com/newspapers?id=UollAAAAIBAJ&sjid=B4oNAAAAIBAJ&pg=2648,2826116 |title=Max likes Saskatoon, not Calgary |work=Vancouver Sun |date=1952-06-17 |accessdate=2011-08-05 |page=10}}</ref>  He only played in 36 games in [[1952–53 NHL season|1952–53]] after suffering a back injury,<ref name="LOHBio" /> but reached 500 career points on November 5, 1952, with two goals against the Rangers.<ref>{{cite news |url=https://news.google.com/newspapers?id=QPpBAAAAIBAJ&sjid=X6oMAAAAIBAJ&pg=2193,1031543 |title=Bentley joins club |work=Portsmouth Times |date=1952-11-06 |accessdate=2011-08-28}}</ref>

===New York and Saskatoon===
Following the season, the New York Rangers offered Max and Doug Bentley the opportunity to play together again.  The Rangers acquired both players in cash transactions over the summer.  Max finished the season with 32 points in 54 games, while Doug played only 20 games.<ref name="LOH1on1" /> Bentley's rights reverted to the Maple Leafs in the fall of 1954 when he refused to report to the Rangers for the [[1954–55 NHL season]].<ref>{{cite news |url=https://news.google.com/newspapers?id=XChgAAAAIBAJ&sjid=Bm8NAAAAIBAJ&pg=6331,3494364 |title=Max Stays with Leafs |work=Saskatoon Star-Phoenix |date=1954-09-24 |accessdate=2011-08-05 |page=19}}</ref>  From the Leafs, he demanded a C$20,000 contract, more than the team was willing to pay.<ref name="LeafsRelease">{{cite news |url=https://news.google.com/newspapers?id=Ui5kAAAAIBAJ&sjid=5HsNAAAAIBAJ&pg=4918,4159796 |title=Smythe to help Bentley join Saskatoon Quakers |work=Calgary Herald |date=1954-10-23 |accessdate=2011-08-05 |page=34}}</ref>  He was initially placed on the suspended list by Toronto after he refused to report to training camp and attempted to purchase his release from the team.<ref>{{cite news |url=https://news.google.com/newspapers?id=sFRjAAAAIBAJ&sjid=DG8NAAAAIBAJ&pg=6024,1299792 |title=Leafs asking suspension for Bentley |work=Saskatoon Star-Phoenix |date=1954-10-09 |accessdate=2011-08-05 |page=21}}</ref> Bentley expressed a desire to leave the NHL and play for the WHL's [[Saskatoon Quakers]], where Doug had become coach. The Maple Leafs ultimately supported Bentley's request.<ref name="LeafsRelease" /> At the time he was granted his release, he was second amongst all active players with 245 goals, behind only Richard.<ref name="PodnieksPlayers" />

He joined the Quakers in November 1954 to great excitement in Saskatoon.<ref>{{cite news |url=https://news.google.com/newspapers?id=FExjAAAAIBAJ&sjid=C28NAAAAIBAJ&pg=7020,355864 |title=Season's largest crowd to see Thursday's game |work=Saskatoon Star-Phoenix |date=1954-11-03 |accessdate=2011-08-05 |page=16}}</ref>  Bentley finished the season with 41 points in 40 games.<ref name="LOHStats" /> He began the 1955–56 season with Saskatoon, but retired on November 15, 1955, due to recurring back problems.  He played his final game on that night, scoring his final goal in an 8–3 victory over the [[Winnipeg Warriors (minor pro)|Winnipeg Warriors]].<ref>{{cite news |url=https://news.google.com/newspapers?id=FD5lAAAAIBAJ&sjid=zIkNAAAAIBAJ&pg=2328,2950035 |title=Max Bentley calls it quits |work=Vancouver Sun |date=1955-11-16 |accessdate=2011-08-05 |page=16}}</ref>

In 1956, Bentley joined his brother Doug in hockey management when the brothers launched a new [[Saskatchewan Junior Hockey League]] (SJHL) team in Saskatoon.<ref>{{cite news |url=https://news.google.com/newspapers?id=dSxUAAAAIBAJ&sjid=KToNAAAAIBAJ&pg=6202,754430 |title=Regina, Saskatoon join puck circuit |work=Regina Leader-Post |date=1956-08-06 |accessdate=2011-08-06 |page=15}}</ref>  He attempted to get into coaching, first offering his services to the WHL's [[Vancouver Canucks (WHL)|Vancouver Canucks]] in the winter of 1961,<ref>{{cite news |url=https://news.google.com/newspapers?id=0IZlAAAAIBAJ&sjid=FYoNAAAAIBAJ&pg=2084,1416838 |title=Max Bentley eyes Canucks |work=Vancouver Sun |date=1961-12-06 |accessdate=2011-08-07 |page=26}}</ref> before going south to coach the [[Burbank Stars]] of the [[California Hockey League (1957–63)|California Hockey league]] in 1962.<ref>{{cite news |url=https://news.google.com/newspapers?id=hDBgAAAAIBAJ&sjid=hm8NAAAAIBAJ&pg=5650,2869156 |title=Max Bentley goes south |work=Saskatoon Star-Phoenix |date=1962-11-20 |accessdate=2011-08-07 |page=13}}</ref>  His nephew [[Bev Bentley|Bev]] and son Lynn played with him in Burbank, while Doug was the player-coach of the rival [[Long Beach Gulls]].<ref name="California">{{cite news |url=https://news.google.com/newspapers?id=-2FkAAAAIBAJ&sjid=WnwNAAAAIBAJ&pg=2848,5819994 |title=Bentleys carry on with puck activities |work=Calgary Herald |date=1962-11-26 |accessdate=2011-08-07 |page=11}}</ref>

==Playing style==
Max Bentley was known for his speed, passing and puck handling skills.  He learned his trade with his brothers as they constantly played street hockey in the summers and on the ice in the winters.  Bentley's father flooded a sheet of ice that was the length of a regulation NHL [[Ice hockey rink|hockey rink]] but much narrower, forcing the boys to develop the ability to maintain control of the puck while making fast, hard turns to reach the net.<ref name="Rivalries" /> He was nicknamed the "Dipsy Doodle Dandy from Delisle" in reference to his ability to skate around opponents who often found that the only way to stop him was via rough play.<ref name="WhosWho">{{cite book |last=Fischler |first=Stan |last2=Fischler |first2=Shirley |title=Who's Who in Hockey |year=2003 |publisher=Andrews McMeel Publishing |location=Kansas City |isbn=0-7407-1904-1 |pages=27–28}}</ref>  Bentley was able to score from nearly any angle, an ability that confounded even his brother Doug.<ref>{{cite news |url=https://news.google.com/newspapers?id=FoAtAAAAIBAJ&sjid=MpkFAAAAIBAJ&pg=6471,1142473 |title=Max Bentley's goal scoring ability is deep mystery even to brother Doug |work=Montreal Gazette |date=1948-01-08 |accessdate=2011-08-07 |page=14}}</ref>  Long-time prairie hockey promoter [[Bill Hunter (ice hockey)|Bill Hunter]] said Bentley was "a phenomenal hockey player, an absolute artist with the puck".<ref name="MGEulogy">{{cite news |url=https://news.google.com/newspapers?id=B4oxAAAAIBAJ&sjid=iaUFAAAAIBAJ&pg=4375,443421 |title=Max Bentley is dead, was artist with puck |work=Montreal Gazette |date=1984-01-21 |accessdate=2011-08-07 |page=H3}}</ref>  Opponents occasionally attempted to use Bentley's hypochondria against him, making remarks on how he looked ill in a bid to distract him during the game.<ref name="WhosWho" />

Bentley was inducted into the [[Hockey Hall of Fame]] in 1966, two years after his brother Doug.<ref name="MGEulogy" />  One year later, Max and Doug were inducted together into the Saskatchewan Sports Hall of Fame.<ref>{{cite news |last=Riddell |first=Walt |url=https://news.google.com/newspapers?id=CS1gAAAAIBAJ&sjid=jG8NAAAAIBAJ&pg=3761,3878993 |title=Hall of Fame list increases |work=Saskatoon Star-Phoenix |date=1967-05-23 |accessdate=2011-08-28 |page=19}}</ref> ''[[The Hockey News]]'' ranked him 48th on its 1998 list of the top 100 players of all time.<ref name="LOH1on1" />

==Personal life==
In addition to hockey, Bentley and his brothers played [[baseball]] in the summers. Representing their hometown of Delisle, they participated in regional tournaments and were repeat winners.<ref>{{cite news |url=https://news.google.com/newspapers?id=wDJgAAAAIBAJ&sjid=224NAAAAIBAJ&pg=7176,1095748 |title=Delisle repeats at Lloyd |work=Saskatoon Star-Phoenix |date=1950-06-09 |accessdate=2011-08-28 |page=14}}</ref> Bentley played summer baseball throughout the 1950s, and was a member of the Saskatoon Gems of the Western Canada Senior League.<ref>{{cite news |url=https://news.google.com/newspapers?id=oDNgAAAAIBAJ&sjid=QW8NAAAAIBAJ&pg=7124,939147 |title=Gems plan lively opener |work=Saskatoon Star-Phoenix |date=1955-06-07 |accessdate=2011-08-28 |page=14}}</ref>  He was also a long-time [[curling|curler]], often playing with his brothers, son and nephews.<ref>{{cite news |url=https://news.google.com/newspapers?id=dApUAAAAIBAJ&sjid=ZTkNAAAAIBAJ&pg=2769,3765285 |title=Max Bentley enters spiel |work=Regina Leader-Post |date=1953-03-25 |accessdate=2011-08-28 |page=20}}</ref><ref>{{cite news |last=McKenzie |first=Cam |url=https://news.google.com/newspapers?id=GjZgAAAAIBAJ&sjid=z28NAAAAIBAJ&pg=5908,184999 |title=Hub rinks dominate |work=Saskatoon Starr-Phoenix |date=1967-02-01 |accessdate=2011-08-28 |page=35}}</ref> 

The majority of Bentley's time away from the hockey rink was spent on the family farm outside Delisle.  The Bentleys operated a large farm, raising cattle and growing wheat,<ref name="California" /> and Max tended to return to the farm to recuperate during hockey seasons when he felt he needed to rest up.<ref>{{cite news |url=https://news.google.com/newspapers?id=xS1kAAAAIBAJ&sjid=1nsNAAAAIBAJ&pg=1960,359969 |title=Bentley returns home for 'much needed rest' |work=Calgary Herald |date=1953-03-03 |accessdate=2011-08-28 |page=22}}</ref>  He and his wife Betty<ref>{{cite news |url=https://news.google.com/newspapers?id=FypgAAAAIBAJ&sjid=Um8NAAAAIBAJ&pg=6728%2C2273966 |title=Max Bentley may be forced to call it quits in NHL |work=Saskatoon Star-Phoenix |date=1953-02-18 |accessdate=2011-09-25 |page=16}}</ref> had a son, Lynn, who was also a hockey player and a younger son, Gary.<ref>{{cite news |url=https://news.google.com/newspapers?id=QmFkAAAAIBAJ&sjid=TnwNAAAAIBAJ&pg=2251,3008587 |title=Spurs to test Rossland crew |work=Calgary Herald |date=1965-02-13 |accessdate=2011-08-28 |page=17}}</ref>  Bentley died at his home in Saskatoon on January 19, 1984, at the age of 63.<ref>{{cite news |title=Max Bentley, 63, a member of the Hockey Hall of Fame |work=New York Times |date=1984-01-21 |page=19}}</ref>

==Career statistics==
{| border="0" cellpadding="1" cellspacing="0" style="text-align:center; width:50em;"
|- style="background:#e0e0e0;"
! colspan="3"  bgcolor="#ffffff" | &nbsp;
! rowspan="99" bgcolor="#ffffff" | &nbsp;
! colspan="5"                    | [[regular season|Regular&nbsp;season]]
! rowspan="99" bgcolor="#ffffff" | &nbsp;
! colspan="5"                    | [[Playoffs]]
|- style="background:#e0e0e0;"
! [[Season (sports)|Season]]
! Team
! League
! GP
! [[Goal (ice hockey)|G]]
! [[Assist (ice hockey)|A]]
! [[Point (ice hockey)|Pts]]
! [[Penalty (ice hockey)|PIM]]
! GP
! G
! A
! Pts
! PIM
|-
| 1937–38||[[Drumheller Miners]]||[[Alberta Senior Hockey League (1936–41)|ASHL]]||26||28||15||43||10||5||7||1||8||2
|- style="background:#f0f0f0;"
| 1938–39||Drumheller Miners||ASHL||32||29||24||53||16||6||5||3||8||6
|-
| 1939–40||[[Saskatoon Quakers]]||SSHL||31||37||14||51||4||4||1||1||2||2
|- style="background:#f0f0f0;"
| [[1940–41 AHL season|1940–41]]||[[Providence Reds]]||[[American Hockey League|AHL]]||9||4||2||6||0||—||—||—||—||—
|-
| 1940–41||[[Kansas City Americans]]||[[American Hockey Association (1926-42)|AHA]]||5||5||5||10||0||—||—||—||—||—
|- style="background:#f0f0f0;"
| [[1940–41 NHL season|1940–41]]||[[Chicago Blackhawks]]||[[National Hockey League|NHL]]||36||7||10||17||6||4||1||3||4||2
|-
| [[1941–42 NHL season|1941–42]]||Chicago Blackhawks||NHL||38||13||17||30||2||3||2||0||2||0
|- style="background:#f0f0f0;"
| [[1942–43 NHL season|1942–43]]||Chicago Blackhawks||NHL||47||26||44||70||2||—||—||—||—||—
|-
| [[1945–46 NHL season|1945–46]]||Chicago Blackhawks||NHL||47||31||30||61||6||4||1||0||1||4
|- style="background:#f0f0f0;"
| [[1946–47 NHL season|1946–47]]||Chicago Blackhawks||NHL||60||29||43||72||12||—||—||—||—||—
|-
| [[1947–48 NHL season|1947–48]]||Chicago Blackhawks||NHL||6||3||3||6||0||—||—||—||—||—
|- style="background:#f0f0f0;"
| 1947–48||[[Toronto Maple Leafs]]||NHL||53||23||25||48||14||9||4||7||11||0
|-
| [[1948–49 NHL season|1948–49]]||Toronto Maple Leafs||NHL||60||19||22||41||18||9||4||3||7||2
|- style="background:#f0f0f0;"
| [[1949–50 NHL season|1949–50]]||Toronto Maple Leafs||NHL||69||23||18||41||14||7||3||3||6||0
|-
| [[1950–51 NHL season|1950–51]]||Toronto Maple Leafs||NHL||67||21||41||62||34||11||2||11||13||4
|- style="background:#f0f0f0;"
| [[1951–52 NHL season|1951–52]]||Toronto Maple Leafs||NHL||69||24||17||41||40||4||1||0||1||2
|-
| [[1952–53 NHL season|1952–53]]||Toronto Maple Leafs||NHL||36||12||11||23||16||—||—||—||—||—
|- style="background:#f0f0f0;"
| [[1953–54 NHL season|1953–54]]||[[New York Rangers]]||NHL||57||14||18||32||15||—||—||—||—||—
|-
| 1954–55||[[Saskatoon Quakers]]||[[Western Hockey League (minor pro)|WHL]]||40||24||17||41||23||—||—||—||—||—
|- style="background:#f0f0f0;"
| 1955–56||Saskatoon Quakers||WHL||10||2||2||4||20||—||—||—||—||—
|- 
| 1958–59||Saskatoon Quakers||WHL||26||6||12||18||2||—||—||—||—||—
|- style="background:#f0f0f0;"
| 1962–63||[[Burbank Stars]]||[[California Hockey League (1957–63)|CalHL]]||—||—||—||—||—||—||—||—||—||—
|- style="background:#e0e0e0;"
! colspan="3" | NHL totals
! 645
! 245
! 299
! 544
! 179
! 51
! 18
! 27
! 45
! 14
|}

==See also==
*[[List of family relations in the NHL]]

==References==
{{reflist|2}}

==External links==
*{{Hockeydb|329}}
*{{Legendsmember|Player|P196601|Max Bentley}}

{{s-start}}
{{succession box | before = [[Elmer Lach]] | title = [[Art Ross Trophy|NHL Scoring Champion]] | years = [[1945–46 NHL season|1946]], [[1946–47 NHL season|1947]] | after = [[Elmer Lach]]}}
{{succession box | before = [[Elmer Lach]] | title = Winner of the [[Hart Trophy]] | years = [[1945–46 NHL season|1946]] | after = [[Maurice Richard]]}}
{{succession box | before = [[Syl Apps]] | title = Winner of the [[Lady Byng Trophy]] | years = [[1942–43 NHL season|1943]]| after = [[Clint Smith]]}}
{{s-end}}

{{DEFAULTSORT:Bentley, Max}}
[[Category:1920 births]]
[[Category:1984 deaths]]
[[Category:Canadian ice hockey centres]]
[[Category:Canadian military personnel of World War II]]
[[Category:Chicago Blackhawks players]]
[[Category:Hart Memorial Trophy winners]]
[[Category:Hockey Hall of Fame inductees]]
[[Category:Ice hockey people from Saskatchewan]]
[[Category:Kansas City Americans players]]
[[Category:Lady Byng Memorial Trophy winners]]
[[Category:National Hockey League scoring leaders (prior to 1947–48)]]
[[Category:New York Rangers players]]
[[Category:Providence Reds players]]
[[Category:Saskatoon Quakers players]]
[[Category:Stanley Cup champions]]
[[Category:Toronto Maple Leafs players]]
[[Category:Hypochondriacs]]