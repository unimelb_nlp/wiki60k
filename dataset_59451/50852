{{Infobox journal
| title         = Canteen
| cover         = [[File:Canteen_1.jpg]]
| editor        = 
| discipline    = [[Literary journal]]
| peer-reviewed = 
| language      = English
| former_names  = 
| abbreviation  = 
| publisher     = 
| country       = 
| frequency     = Biannual
| history       = 2007-present
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       = http://www.canteenmag.com
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 
| LCCN          = 
| CODEN         = 
| ISSN          = 1936-3168
| eISSN         = 
| boxwidth      = 
}}
'''''Canteen''''' is an English-language literary and arts magazine published twice a year. Founded in 2007<ref>{{cite news |title=Canteen Magazine Launch Party |url=http://artbusiness.com/1open/051707.html |date=2007-05-17 |accessdate=2010-11-10}}</ref> by publisher Stephen Pierson, editor-in-chief Sean Finney, executive editor Mia Lipman, and former art director Sai Sriskandarajah, the magazine asks its contributors to reveal their creative process to the reader. As described by Finney, "''Canteen'' is the literary magazine that comes with instructions."<ref>{{cite news |title=Meet Maxim and McSweeney's Newest Competition: The Editors of Canteen Lit Mag |url=http://litminds.org/blog/2007/09/litminds_literary_innovators_i.html |date=2007-09-03 |accessdate=2010-11-10}}</ref> "''Canteen'' was born at the restaurant of the same name in San Francisco, where [[Dennis Leary (chef)|chef Dennis Leary]] hosted literary salons."<ref>{{cite news |first=Katherine |last=McGarr |title=Review: Canteen |url=http://issuu.com/nyrm/docs/reviews/6 |publisher=New York Review of Magazines |date=2009-04-27 |accessdate=2010-11-10 }}</ref>  The magazine has offices in Brooklyn, NY, and San Francisco, CA.

==Content==
''Canteen'' consists of photos, essays, fiction, nonfiction, poetry, and paintings by both well-known and amateur writers and artists. In addition to input from published authors and celebrated artists, the magazine accepts submissions of written and visual work from readers through their [http://canteenmag.com/submit.shtml website].  Issue five of ''Canteen'' was named "Book of the Week" by the arts blog [http://blog.largeheartedboy.com/ Large Hearted Boy] and [http://wordbrooklyn.com/ WORD Bookstore] in Brooklyn for skillfully combining word and image.<ref>{{cite news |title=Largehearted Word Books of the Week - January 27th 2010 |url=http://www.largeheartedboy.com/blog/archive/2010/01/largehearted_wo.html |date=2010-01-27 |accessdate=2010-11-10 }}</ref>

==''Anatomy of a Photo Contest''==
In 2010, ''Canteen'' held a photo contest that was designed to reveal the decision-making processes behind such competitions, which are sometimes criticized for their opaque nature. The magazine published the comments and critiques of the contest's judges in full, as well as the response of one of the critiqued artists.<ref>{{cite news |first=Rob |last=Haggart |title=Photography Contests: The Fix Is In |url=http://www.aphotoeditor.com/2010/08/11/photography-contests-the-fix-is-in/ |date=2010-08-11 |accessdate=2010-11-10 }}</ref> The competition was judged by Brooklyn Museum director Arnold Lehman and photographer Matthew Porter.<ref>{{cite news |title=Canteen Photo Contest Winners and Non-Winner |url=http://newpagesblog.blogspot.com/2010/10/canteen-photo-contest-winners-and-non.html |date=2010-10-07 |accessdate=2010-11-10 }}</ref> Winning entries, along with other images selected from the contest's submissions, were presented in a show at the [[PowerHouse Books|powerHouse]] Arena in [[DUMBO, Brooklyn]] in August 2010.

==canTeens (tutoring program)==
In September 2008, ''Canteen'' launched a literacy tutoring program for middle school students in Harlem, NY.  The workshops are designed to increase the students' confidence in their work, as well as encouraging them to continue writing and to share it with others.  Workshops have been taught by contributing ''Canteen'' authors such as [[Porochista Khakpour]], [[Gina Gionfriddo]] and Garth Risk Hallberg, and other authors such as [[Lorraine Adams]] and [[Amy Braunschweiger]]. At the end of each semester, the students' work is compiled into a professionally designed and printed magazine that is nationally distributed.

==Outwrite==
Outwrite is a [[Flash fiction|flash-fiction]] writing contest created by ''Canteen'' that pits an established writer against literary novices who have never been published, deciding the winner based on audience response.  In the inaugural contest in Los Angeles on April 12, 2010, Dana Goodyear (poet and ''New Yorker'' staff writer) competed against unknowns Bradley Spinelli and A. Wolfe.  Wolfe's piece garnered the best audience reaction and took home the grand prize. ''Canteen'' is planning more Outwrite events to be held in New York, Boston, Philadelphia, Madison, Washington D.C., and San Francisco.

==Notable Contributors==
Matthew Porter (photographer)<br />
[[Tao lin|Tao Lin]] (author)<br />
Dana Goodyear (poet)<br />
[[Benjamin kunkel|Benjamin Kunkel]] (author)<br />
[[Joyce Maynard]] (author)<br />
[[Nathaniel Rich (novelist)|Nathaniel Rich]] (author)<br />
[[Stephen Elliott (author)|Stephen Elliott]] (author)<br />
[[Ben Fountain]] (author)<br />
[[Porochista Khakpour]] (author)<br />
[[Po Bronson]] (author)<br />
[[Stephen shore|Stephen Shore]] (photographer)

==Distribution==
''Canteen'' achieved national distribution through Disticor<ref>{{cite news|title=Disticor Direct: Canteen |url=http://www.disticor.com/Client/Disticor/Disticor_LP4W_LND_WebStation.nsf/e3d52eadbccc650d8525732b004fc4a4/9f37dbc4cfa4c2b8852573e6005aef58!OpenDocument |accessdate=2010-11-21 |deadurl=yes |archiveurl=https://web.archive.org/web/20101224070716/http://www.disticor.com/Client/Disticor/Disticor_LP4W_LND_WebStation.nsf/e3d52eadbccc650d8525732b004fc4a4/9f37dbc4cfa4c2b8852573e6005aef58!OpenDocument |archivedate=2010-12-24 |df= }}</ref> with its first issue in 2007. It is sold at most [[Barnes and noble|Barnes and Noble]] locations in the U.S. and Canada, as well as at [[independent bookstore]]s around the country.

==References==
{{reflist}}

==External links==
*[https://web.archive.org/web/20100914215811/http://canteenmag.com:80/index.shtml ''Canteen'' magazine official site]
*[http://www.sfgate.com/cgi-bin/article.cgi?file=/c/a/2007/05/18/DDG5MPSRLM1.DTL ''Canteen'' magazine in the San Francisco Chronicle]
*[http://www.blackbookmag.com/article/canteen-confidential/2364 ''Canteen Confidential'' in BlackBook magazine]
*[http://artbusiness.com/1open/051707.html ''Modernism West at Foreign Cinema:'' Canteen ''magazine launch party'' on ArtBusiness.com]
*[http://www.newpages.com/magazinestand/litmags/reviews_archive_2009/2009_08/litmagreviews_2009_08.htm#Canteen Review of ''Canteen'' magazine, issue 4, from Newpages.com]
*[http://brooklynbased.net/email/black-friday-shopping-thats-good-for-the-soul/ ''Black Friday Shopping That's Good for the Soul'' on BrooklynBased.net]


[[Category:American literary magazines|Canteen magazine]]
[[Category:Biannual magazines]]
[[Category:Magazines established in 2007]]
[[Category:Magazines published in California]]
[[Category:Magazines published in New York City]]
[[Category:American arts magazines]]
[[Category:2007 establishments in California]]