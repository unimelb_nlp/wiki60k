{{good article}}
{{Infobox bridge
|bridge_name      = Alconétar Bridge<ref group="A.">All measurements refer to the time prior to the bridge relocation and are necessarily fragmentary due to the ruined state of the structure.</ref>
|native_name      = Puente de Alconétar 
|native_name_lang = es
|image            = Puente de Alconétar, Cáceres Province, Spain. Pic 01.jpg
|image_size       = 300px
|alt              = 
|caption          = Remains of the Alconétar Bridge with its flattened arches
|official_name    = 
|other_name       =
|carries          = 
|crosses          = [[Tagus]]
|locale           = [[Garrovillas de Alconétar]], [[Cáceres Province]], [[Extremadura]], [[Spain]]
|maint            = 
|id               = 
|designer         = Possibly [[Apollodorus of Damascus]]
|design           = Segmental [[arch bridge]]
|material         = [[Masonry|Stone]], [[Roman concrete]]
|spans            = Ca. 18 [incl. flood outlets]
|pierswater       = 
|mainspan         = {{convert|15|m|abbr=on}}
|length           = {{convert|290|m|abbr=on}} [incl. approaches]
|width            = {{convert|6.55|-|6.80|m|abbr=on}}
|height           = Minimum {{convert|12.50|m|abbr=on}}
|load             = 
|clearance        = 
|below            = 
|traffic          = 
|builder          = 
|begin            = 
|complete         = Probably reign of [[Trajan]] or [[Hadrian]] (98–138 AD)
|open             = 
|preceded         = 
|followed         = 
|heritage         = Listed as [[cultural heritage]] of Spain
|collapsed        = 
|closed           = 
|toll             = 
|map_cue          = 
|map_image        = 
|map_alt          = 
|map_text         = 
|map_width        = 
|coordinates      = 
|lat              = 
|long             = 
|extra            = 
{{Location map many | Spain
 | label=Alconétar&nbsp;Bridge | pos=right | label_size=90
      | mark= 
      | lat=39.753838   | long=-6.437345
 | label2=Mérida       | pos2=right | label2_size=80 
      | mark2= Black pog.svg 
      | lat2=38.90      | long2=-6.33
 | label3=Astorga | pos3=right  | label3_size=80 
      | mark3= Black pog.svg 
      | lat3=42.45   | long3=-6.05
 | label4=Salamanca | pos4=right  | label4_size=80
      | mark4= Black pog.svg 
      | lat4=40.96   | long4=-5.67
 | width=250 | float=center | border=none
 | caption = Alconétar Bridge with itinerary of the Roman ''[[Via de la Plata]]''
}}
}}

The '''Alconétar Bridge''' ([[Spanish language|Spanish]]: ''Puente de Alconétar''), also known as '''Puente de Mantible''', was a [[Roman segmental arch bridge]] in the [[Extremadura]] region, [[Spain]]. The [[Classical antiquity|ancient]] structure, which featured flattened arches with a span-to-rise ratio of 4–5:1, is one of the earliest of its kind.<ref>{{harvnb|Durán Fuentes|2004|p=184}}; {{harvnb|Fernández Casado|1970}}; {{harvnb|Prieto Vives|1925|p=150}}</ref> Due to its design, it is assumed that the bridge was erected in the early 2nd century AD by the emperors [[Trajan]] or [[Hadrian]], possibly under the guidance of [[Apollodorus of Damascus]], the most famous architect of the time.<ref>{{harvnb|Prieto Vives|1925|p=155}}; {{harvnb|O'Connor|1993|pp=108f.}}; {{harvnb|Galliazzo|1994|p=359}}</ref> 

The almost 300 m long Alconétar Bridge served as a crossing point for the Roman ''[[Via de la Plata]]'', the most important north-south connection in western [[Hispania]],<ref name="O'Connor 20">{{harvnb|O'Connor|1993|p=20}}</ref> over the [[Tagus]], the longest river of the [[Iberian peninsula]]. It presumably remained in service until the [[Reconquista]], after which numerous [[Early modern period|early modern]] reconstruction attempts by Spanish engineers failed. The ruins, which were mainly to be found on the right river bank, were relocated from their original position in 1970 when the [[Alcantara Dam|Alcántara reservoir]] was created.

== Location and road access ==
[[File:Location of Alconétar Bridge Roman Roads.svg|thumb|300px|left|Location of Alconétar Bridge within Roman road network in Hispania]] 

The historic Alconétar Bridge, which should not be confused with the monumental [[Alcántara Bridge]] further downstream, spanned the Tagus not far from the mouth of the [[Almonte (river)|Almonte]], in the heart of the Spanish [[Cáceres Province]] in the Extremadura region.<ref name="Prieto Vives 147">{{harvnb|Prieto Vives|1925|p=147}}</ref> A modern motorway and a railway, which cross the Tagus in the immediate vicinity, underline the historical importance of this crossing point between northern and southern Spain.<ref name="Prieto Vives 147"/> During the building of the Alcántara Dam in 1970, the remains of the bridge were moved from their original site to a meadow six kilometers to the north, close to the municipality of [[Garrovillas de Alconétar]].<ref name="O'Connor 108f.">{{harvnb|O'Connor|1993|pp=108f.}}</ref> By contrast, few traces are left of the neighbouring ancient bridge over the river Almonte.<ref name="Prieto Vives 149">{{harvnb|Prieto Vives|1925|p=149}}</ref>

In the [[Classical antiquity|classical period]], the Alconétar Bridge was part of the [[Roman road]] ''[[Via de la Plata|Iter ab Emerita Caesaraugustam]]'', that was later called ''Via de la Plata''. This important inner Iberian connection led from the provincial capital [[Mérida, Spain|Mérida]] in the south, through the river valleys of [[Alagón (river)|Alagón]], Tiétar and Tagus to the north, then on to the western part of [[Meseta Central#The Inner Plateau and associated mountains|Meseta Central]], passing the major town of [[Salamanca]].<ref name="Durán Fuentes 182">{{harvnb|Durán Fuentes|2004|p=182}}</ref> Its terminal point was [[Astorga, Spain|Astorga]] in north-western Spain.<ref name="O'Connor 20"/> The Via de la Plata was one of the four main routes which were established by [[Augustus]] (30 BC–14 AD) and his successors for military control of the peninsula and for facilitating the exploitation of the rich Spanish silver and gold mines.<ref name="O'Connor 20"/>

Apart from the junction over the Tagus, the remains of four other ancient bridges can be found along the road: one over the Albarregas, another over the Aljucén, a third close to Caparra and a fourth over the [[Tormes]].<ref name="Durán Fuentes 92f.">{{harvnb|Durán Fuentes|2004|pp=91f.}}</ref> In the hills overlooking the Alconétar Bridge, a Roman [[mansio]] with the name of ''Turmulus'' (Spanish: "Ad Túrmulos") was established, according to the then customary distance intervals.<ref name="Prieto Vives 149"/> It was the fourth of a total number of sixteen between Mérida and Astorga.<ref>{{harvnb|Gil Montes|2004|pp=9f.}}</ref>

== History ==
[[File:Puente de Alconétar, Cáceres Province, Spain. Pic 07.jpg|thumb|The relocated remains during a flood]]

The exact construction date of the Puente de Alconétar is unknown because of missing literary and [[epigraphic]] sources.<ref name="Prieto Vives 155">{{harvnb|Prieto Vives|1925|p=155}}</ref> Its segmental arches suggest that it was built in the early 2nd century AD, more specifically during the reign of the emperors Trajan (98–117 AD) or Hadrian (117–138 AD), as the use of this arch form was typical of that era. Both rulers were born in the southern Spanish province of [[Hispania Baetica|Baetica]] and Trajan is known to have ordered the restoration of the ''Iter ab Emerita Caesaraugustam'' when he came to power.<ref>{{harvnb|Prieto Vives|1925|p=155}}; {{harvnb|O'Connor|1993|pp=108f.}}</ref> Segmental arches were often employed by Trajan's court architect Apollodorus of Damascus, such as in [[Trajan's Forum]] and most notably in the greatest civil engineering achievement of its time, [[Trajan's Bridge]], which rested on twenty huge [[Opus caementicium|concrete]] piers and was used during the [[Trajan's Dacian Wars|Dacian Wars]] for moving troops across the more than {{convert|1000|m|abbr=on}} wide [[Danube]].<ref name="O'Connor 142f.">{{harvnb|O'Connor|1993|pp=142f.}}</ref>

[[File:Puente de Alconétar, Cáceres Province, Spain. Pic 03.JPG|thumb|left|2nd floodway. The arch thickness is 1.20 m.]]

[[Al-Andalus|Moorish]] geographers make no mention of the Alconétar Bridge, even though they praise the [[Alcántara Bridge|Roman bridge of Alcántara]] which also leads across the Tagus.<ref name="Prieto Vives 10f.">{{harvnb|Prieto Vives|1925|pp=10f.}}</ref> There is some evidence that a community called Alconétar, Alconétara or Alcontra ([[Arabic language|Arabic]]: "small bridge") existed at least temporarily – probably an indirect reference to the high-rising bridge of Alcántara.<ref name="Prieto Vives 149"/> It also remains unknown why the bridge of Alconétar is called ''Puente de Mantible'' in the local vernacular, an expression which alludes to the legend of [[Charlemagne]] and his [[Paladins|Twelve Paladins]].<ref name="Prieto Vives 158">{{harvnb|Prieto Vives|1925|p=158}}</ref>

The bridge was probably in use until the time of the Reconquista, when the Tagus constituted the border between the [[Christianity|Christian]] and the Moorish realm from the 11th to the 13th century, and the frequent clashes might have easily made the ancient bridge unusable.<ref name="Prieto Vives 155f.; Durán Fuentes 182">{{harvnb|Prieto Vives|1925|pp=155f.}}; {{harvnb|Durán Fuentes|2004|p=182}}</ref> According to another theory, the water could have begun to wash away the ancient foundations at the time.<ref name="Prieto Vives 155f.; Durán Fuentes 182"/>

[[File:Puente de Alconétar, Cáceres Province, Spain. Pic 04.jpg|thumb|The two arches 1 and 3, reconstructed in later times]]

The Alconétar Bridge first appears in records in 1231 and, again, in 1257, when it is explicitly referred to as being in use.<ref name="Prieto Vives 157f.">{{harvnb|Prieto Vives|1925|pp=157f.}}</ref> It was probably repaired by the [[Knights Templar]] who had taken control of the bridge as well as the village in the meantime.<ref name="Prieto Vives 157f."/> The extant arches 1 and 3, both of which are not of Roman fabric, are assumed to date back to this period.<ref name="Prieto Vives 157f."/> Around 1340, however, the bridge was apparently unusable again, so that a ferry service was established to cross the river, which is also recorded at later times.<ref name="Prieto Vives 157f."/> On the site of the ancient way station, a fortress was erected in the [[Middle Ages]];<ref name="Fernández Casado">{{harvnb|Fernández Casado|1970}}</ref> its tower built of Roman [[spolia]] emerges from the Alcántara reservoir at low water.

Several attempts to reopen the bridge in the early modern era proved unsuccessful. In 1553, the architect [[Rodrigo Gil de Hontañón]] calculated a cost of 80,000 [[Ducats]] for the reconstruction of the bridge without ever realising his plans. The construction project of Alonso de Covarrubias and Hernán Ruiz of 1560 never went beyond the planning stage, and neither did another project between 1569 and 1580.<ref name="Durán Fuentes 182"/>

[[File:Puente de Alconétar, Cáceres Province, Spain. Pic 05.JPG|thumb|left|In the foreground the two Roman flood openings 1 and 2]]

In the 18th century two further attempts to repair the bridge failed, in 1730 and 1760–70. The latter plan of the military engineer José García Galiano included a complete reconstruction with three large-span segmental arches. The planning sketch shows that already at that time the remaining arches were limited to the right bank, a finding confirmed by the drawing of Fernando Rodríguez from 1797 and engravings in [[Alexandre de Laborde]]s' ''Voyage pittoresque de l'Espagne'' a few years later.<ref name="Durán Fuentes 182"/> The reconstruction sketch produced by Rodriguez (see diagram below) shows the profile of the bridge, rising evenly and dominated by three central arches in the centre of the river. These arches are flanked by a further nine segmental arches on both sides. The symmetry of the arches suggests that, in lieu of the fortification on the right bank viewed upstream, there might have been another segmental arch in Roman times.<ref name="Durán Fuentes 183">{{harvnb|Durán Fuentes|2004|p=183}}</ref>

The basis for the modern scientific analysis of the bridge was laid out by the civil engineer Antonio Prieto in his 1925 survey, which details the condition of the bridge before its relocation in 1970. Although this was a serious attempt to reconstruct the bridge as close to the original as possible, the Spanish scholar Durán points out that slight changes to the main body can never be avoided in the course of such a difficult undertaking.<ref name="Durán Fuentes 181, 184">{{harvnb|Durán Fuentes|2004|pp=181, 184}}</ref> 

The Alconétar Bridge has been classified as "historical heritage" since 1931 by the Spanish authorities.<ref>Patrimonio histórico: [http://www.mcu.es/bienes/cargarFiltroBienesInmuebles.do?layout=bienesInmuebles&cache=init&language=es Bienes culturales protegidos. Consulta de bienes inmuebles. Bien: "Puente de Alconétar"] {{es icon}}</ref>

== Construction ==
[[File:Puente de Alconétar, Cáceres Province, Spain. Drawing 01.jpg|thumb|Ground plan of the extant bridge section]]
[[File:Puente de Alconétar, Cáceres Province, Spain. Pic 08.jpg|thumb|Arch no. 3. The right pier, no. 3, preserves the sloping support for the Roman segmental arch.]]
[[File:Puente de Alconétar, Cáceres Province, Spain. Drawing 02.jpg|thumb|Elevation of pier no. 3: by using the degree of curvature of the originally preserved supports for the arch, the span-to-rise ratio of the ancient segmental arches can be calculated.]]

Main feature of the Alconétar Bridge was the segmental shape of its arches,<ref>{{harvnb|Prieto Vives|1925|pp=149f.}}; {{harvnb|Fernández Casado|1970}}; {{harvnb|O'Connor|1993|pp=108f.}}</ref> which were rather uncommon in ancient bridge building: in a survey of Roman bridges in Hispania, only one in ten showed the same characteristics, the vast majority being of semi-circular design.<ref>{{harvnb|Durán Fuentes|2004|p=239}}</ref> According to Prieto, the bridge had sixteen [[arch]]es, not including the [[Culvert|flood openings]] on the right approach, with the following spans (estimates are in square brackets):

*Meters: 7.30 – 8.20 – 9 – 10.15 – [11 – 12 – 13 – 14 – 15 – 14] – 13 – 12 – 11 – [10] – 9.30 – 9.10
:(Feet: 24.0 – 26.9 – 30 – 33.3 – [36 – 39 – 43 – 46 – 49 – 46] – 43 – 39 – 36 – [33] – 30.5 – 29.9.)<ref name="Prieto Vives 155"/>

Other sources however vary from eleven to fifteen arches.<ref name="Durán Fuentes 184">{{harvnb|Durán Fuentes|2004|p=184}}</ref> The total length of the rectilinear structure was {{convert|290|m|abbr=on}}, of which {{convert|190|m|abbr=on}} spanned the riverbed at low water.<ref name="Prieto Vives 149"/> If one adds the clear span of the arches and assumes, on the basis of the preserved [[Pier (architecture)|pier]]s, an average pier thickness of {{convert|4.4|m|abbr=on}}, then the distance between both bridge ramps was {{convert|244|m|abbr=on}} (= 178 + 15 x 4.4), which corresponded to a river cross section of 73%.<ref name="O'Connor 108f."/> By comparison, the corresponding discharge profiles for the Roman bridges [[Roman bridge of Córdoba|of Córdoba]], [[Puente Romano (Mérida)|of Mérida]] and [[Salamanca]] were 62%, 64% and 80% respectively.<ref name="O'Connor 165">{{harvnb|O'Connor|1993|p=165}}</ref>

Most of the surviving fabric was concentrated on the right bank of the Tagus where the current was less strong; a number of pier stumps rose just above the water surface in the middle of the river, whilst on the left bank a further two piers remained standing, next to which the left [[abutment]] followed.<ref name="Prieto Vives 150f.">{{harvnb|Prieto Vives|1925|pp=150f.}}</ref> The parts of the bridge moved to a safe place were essentially (see images): the right bridge ramp with its two arch-shaped flood openings, the piers 1, 2, and 3 with the remains of 4 and 6, as well as the [[Vault (architecture)|vault]]s 1 and 3.<ref name="O'Connor 108f."/> The other scarce remains were submerged by the flooding of the Alcantara reservoir in 1970.<ref>{{harvnb|Galliazzo|1994|p=358}}</ref> 

The approach to the ramp is {{convert|42|m|abbr=on}} long and {{convert|6.55|-|6.80|m|abbr=on}} wide.<ref name="Durán Fuentes 184"/> The clear spans of its two segmental arches are {{convert|6.95|m|abbr=on}} and {{convert|7.40|m|abbr=on}},<ref name="Durán Fuentes 184"/> which corresponds to a span-to-rise ratio of 4.0 and 3.3 to 1 respectively.<ref name="González Limón 250, Tab. 2">{{harvnb|González Limón|2001|p=250, tab. 2}}</ref> The accurately fitting inclined contact surface of the springings clearly proves the Roman origin of these arches.<ref>{{harvnb|Prieto Vives|1925|pp=149f.}}; {{harvnb|Fernández Casado|1970}}; {{harvnb|O'Connor|1993|pp=108f.}}; {{harvnb|Durán Fuentes|2004|p=184}}</ref> With an extraordinary width of {{convert|1.20|m|abbr=on}}, the [[voussoir]]s appear oversized in relation to the span (arch slenderness of 1:5.8 and 6.2 respectively).<ref name="Durán Fuentes 184"/> In contrast, the two surviving bridge arches no. 1 and 3 are instable substitutions, made of carelessly laid rubble.<ref>{{harvnb|Prieto Vives|1925|p=150}}; {{harvnb|Durán Fuentes|2004|p=183}}</ref> The date of these arches is unknown, as with all other repairs; possibly they were built in the period of the Knights Templar.<ref name="Prieto Vives 157f."/>

Equally evidently of post-classical origin is the masonry of the first two piers above the lower [[cornice]], which projects from all piers at the same height.<ref name="Prieto Vives 150">{{harvnb|Prieto Vives|1925|p=150}}</ref> The superstructure of these piers could have been reconstructed for a [[drawbridge]]<ref name="Prieto Vives 150"/> or a tower, which, according to an illustration in the ''Voyage Pittoresque'', rested upon pier 2.<ref name="Fernández Casado"/> In contrast, the third pier has still preserved its original Roman character up to the top at {{convert|12.50|m|abbr=on}}. Its carefully worked [[ashlar]] and the second cornice, which ran along all ancient piers, provide us with the most distinct impression of the original shape of the Roman bridge.<ref>{{harvnb|Prieto Vives|1925|p=152}}; {{harvnb|Durán Fuentes|2004|pp=185f.}}</ref>

In particular, it is possible to reconstruct relatively precisely the rise of the Roman segmental arches on the basis of the angles of the inclined stone supports at the springing level.<ref name="Fernández Casado"/> Thus, the third arch originally described a [[circular sector]] of 95°,<ref name="Durán Fuentes 184"/> which points to a span-to-rise ratio of about 4–5 to 1 for the other segmental arches.<ref name="Fernández Casado"/> This value is supported by de Labordes' engravings which depict an intact Roman segmental arch spanning the adjoining fourth bay as late as the early 19th century.<ref name="Prieto Vives 150"/> Along with other early examples, such as [[Limyra Bridge]] and the [[Ponte San Lorenzo]], the Alconétar Bridge therefore ranks among the oldest segmental arched bridges in the world. Its existence demonstrates that, in contrast to what had previously been widely believed and taught, Roman bridge builders possessed intimate knowledge of flattened arches.<ref name="O'Connor 171">{{harvnb|O'Connor|1993|p=171}}</ref>

The pier thicknesses, measuring {{convert|4.25|m|abbr=on}}, {{convert|4.45|m|abbr=on}} and {{convert|4.55|m|abbr=on}}, increase slightly towards the middle of the river, while the distances between the piers increase from {{convert|7.30|m|abbr=on}} to {{convert|10.20|m|abbr=on}}.<ref name="Durán Fuentes 183"/> The fifth and ultimate pier, which carries the distinctive nickname "Bishop's table", takes an advanced position in the riverbed and possesses by far the largest cross-section ({{convert|8.10|m|abbr=on}}). It may be of medieval origin and could have served as a base for a watchtower, replacing two ancient arches.<ref name="Prieto Vives 151">{{harvnb|Prieto Vives|1925|p=151}}</ref> All five piers are strengthened on the upstream face by pointed [[cutwater]]s.<ref name="Prieto Vives 150, fig. 3">{{harvnb|Prieto Vives|1925|p=150, fig. 3}}</ref>

The facing of the piers consists of local [[granite]] ashlar, set in parallel courses without the use of [[mortar (masonry)|mortar]] or iron ties (''[[opus quadratum]]''); their interior, as those of the ramps, was filled with [[Roman concrete]], a common method applied for Roman bridges.<ref>{{harvnb|Prieto Vives|1925|p=152}}; {{harvnb|Fernández Casado|1970}}; {{harvnb|Galliazzo|1994|p=359}}</ref> Outwardly, the flattened arches must have lent the bridge a rather elongated appearance, with its roadway following a horizontal or slightly convex line.<ref name="Durán Fuentes 186f.">{{harvnb|Durán Fuentes|2004|pp=186f.}}</ref>

== Reconstruction and measurements ==
[[File:Puente de Alconétar, Cáceres Province, Spain. Drawing 04.jpg|thumb|620px|center|18th century elevation and ground plan by Fernando Rodríguez (view upstream). Above the bridge as of 1797, below his reconstruction attempt of the original Roman structure.]]

<center>Recorded measurements from left to right bank (viewed upstream):
{| class="wikitable" style="margin:1px; border:1px solid #cccccc; "
|----- align="left" valign="top" bgcolor="cccccc"
! Bridge part!! Galiano (ca. 1770) !! Rodríguez (1797) !! Prieto (1925)<ref group="A.">Estimated values in italics.</ref>!! Durán (2004)<ref group="A.">All values refer to the relocated bridge.</ref>
|-
|bgcolor="#F1F1F1"|Span 1st floodway
|bgcolor="#F1F1F1"|{{0}}{{convert|6.86|m|abbr=on}}
|bgcolor="#F1F1F1"|{{0}}{{convert|7.04|m|abbr=on}}
|bgcolor="#F1F1F1"|{{0}}{{convert|7.00|m|abbr=on}}
|bgcolor="#F1F1F1"|{{0}}{{convert|6.95|m|abbr=on}}
|-
|Ramp thickness 
|{{convert|10.60|m|abbr=on}}
|{{convert|11.63|m|abbr=on}}
|{{convert|12.00|m|abbr=on}}
|{{convert|14.00|m|abbr=on}}
|-
|bgcolor="#F1F1F1"|Span 2nd floodway
|bgcolor="#F1F1F1"|{{0}}{{convert|7.40|m|abbr=on}}
|bgcolor="#F1F1F1"|{{0}}{{convert|7.10|m|abbr=on}}
|bgcolor="#F1F1F1"|{{0}}{{convert|7.50|m|abbr=on}}
|bgcolor="#F1F1F1"|{{0}}{{convert|7.40|m|abbr=on}}
|-
|Ramp thickness 
|{{convert|12.90|m|abbr=on}}
|{{convert|11.65|m|abbr=on}}
|{{convert|13.00|m|abbr=on}}
|{{convert|13.50|m|abbr=on}}
|-
|bgcolor="#F1F1F1"|Span 1st arch
|bgcolor="#F1F1F1"|{{0}}{{convert|7.50|m|abbr=on}}
|bgcolor="#F1F1F1"|{{0}}{{convert|7.62|m|abbr=on}}
|bgcolor="#F1F1F1"|{{0}}{{convert|7.30|m|abbr=on}}
|bgcolor="#F1F1F1"|{{0}}{{convert|7.30|m|abbr=on}}
|-
|Thickness 1st pier
|{{0}}{{convert|5.60|m|abbr=on}}
|{{0}}{{convert|4.17|m|abbr=on}}
|{{0}}{{convert|4.25|m|abbr=on}}
|{{0}}{{convert|4.25|m|abbr=on}}
|-
|bgcolor="#F1F1F1"|Span 2nd arch
|bgcolor="#F1F1F1"|{{0}}{{convert|9.00|m|abbr=on}}
|bgcolor="#F1F1F1"|{{0}}{{convert|8.44|m|abbr=on}}
|bgcolor="#F1F1F1"|{{0}}{{convert|8.20|m|abbr=on}}
|bgcolor="#F1F1F1"|{{0}}{{convert|8.10|m|abbr=on}}
|-
|Thickness 2nd pier
|{{0}}{{convert|5.60|m|abbr=on}}
|{{0}}{{convert|4.19|m|abbr=on}}
|{{0}}{{convert|4.25|m|abbr=on}}
|{{0}}{{convert|4.45|m|abbr=on}}
|-
|bgcolor="#F1F1F1"|Span 3rd arch
|bgcolor="#F1F1F1"|{{0}}{{convert|9.86|m|abbr=on}}
|bgcolor="#F1F1F1"|{{0}}{{convert|8.92|m|abbr=on}}
|bgcolor="#F1F1F1"|{{0}}{{convert|8.95|m|abbr=on}}
|bgcolor="#F1F1F1"|{{0}}{{convert|8.50|m|abbr=on}}
|-
|Thickness 3rd pier
|{{0}}{{convert|5.50|m|abbr=on}}
|{{0}}{{convert|4.21|m|abbr=on}}
|{{0}}{{convert|4.25|m|abbr=on}}
|{{0}}{{convert|4.55|m|abbr=on}}
|-
|bgcolor="#F1F1F1"|Span 4th arch
|bgcolor="#F1F1F1"|{{convert|10.10|m|abbr=on}}
|bgcolor="#F1F1F1"|{{convert|10.32|m|abbr=on}}
|bgcolor="#F1F1F1"|{{convert|10.15|m|abbr=on}}
|bgcolor="#F1F1F1"|{{convert|10.20|m|abbr=on}}
|-
|Thickness 4th pier
|
|{{0}}{{convert|4.81|m|abbr=on}}
|{{0}}{{convert|4.80|m|abbr=on}}
|
|-
|bgcolor="#F1F1F1"|Span 5th arch
|bgcolor="#F1F1F1"|
|bgcolor="#F1F1F1"|{{convert|12.03|m|abbr=on}}
|bgcolor="#F1F1F1"|''{{convert|11.00|m|abbr=on}}''
|bgcolor="#F1F1F1"|
|-
|Thickness 5th pier
|
|{{0}}{{convert|6.21|m|abbr=on}}
|
|
|-
|bgcolor="#F1F1F1"|Span 6th arch
|bgcolor="#F1F1F1"|
|bgcolor="#F1F1F1"|{{convert|16.72|m|abbr=on}}
|bgcolor="#F1F1F1"|''{{convert|12.00|m|abbr=on}}''
|bgcolor="#F1F1F1"|
|-
|Thickness 6th pier
|
|{{0}}{{convert|6.21|m|abbr=on}}
|
|
|-
|bgcolor="#F1F1F1"|Span 7th arch
|bgcolor="#F1F1F1"|
|bgcolor="#F1F1F1"|{{0}}{{convert|8.92|m|abbr=on}}
|bgcolor="#F1F1F1"|''{{convert|13.00|m|abbr=on}}''
|bgcolor="#F1F1F1"|
|-
|Thickness 7th pier
|
|{{0}}{{convert|6.21|m|abbr=on}}
|
|
|-
|bgcolor="#F1F1F1"|Span 8th arch
|bgcolor="#F1F1F1"|
|bgcolor="#F1F1F1"|{{convert|16.74|m|abbr=on}}
|bgcolor="#F1F1F1"|''{{convert|14.00|m|abbr=on}}''
|bgcolor="#F1F1F1"|
|-
|Thickness 8th pier
|
|{{0}}{{convert|6.21|m|abbr=on}}
|
|
|-
|bgcolor="#F1F1F1"|Span 9th arch
|bgcolor="#F1F1F1"|
|bgcolor="#F1F1F1"|{{convert|11.93|m|abbr=on}}
|bgcolor="#F1F1F1"|''{{convert|15.00|m|abbr=on}}''
|bgcolor="#F1F1F1"|
|-
|Thickness 9th pier
|
|{{0}}{{convert|4.79|m|abbr=on}}
|
|
|-
|bgcolor="#F1F1F1"|Span 10th arch
|bgcolor="#F1F1F1"|
|bgcolor="#F1F1F1"|{{convert|10.22|m|abbr=on}}
|bgcolor="#F1F1F1"|''{{convert|14.00|m|abbr=on}}''
|bgcolor="#F1F1F1"|
|-
|Thickness 10th pier
|
|{{0}}{{convert|4.19|m|abbr=on}}
|
|
|-
|bgcolor="#F1F1F1"|Span 11th arch
|bgcolor="#F1F1F1"|
|bgcolor="#F1F1F1"|{{0}}{{convert|8.82|m|abbr=on}}
|bgcolor="#F1F1F1"|{{convert|13.00|m|abbr=on}}
|bgcolor="#F1F1F1"|
|-
|Thickness 11th pier
|
|{{0}}{{convert|4.19|m|abbr=on}}
|
|
|-
|bgcolor="#F1F1F1"|Span 12th arch
|bgcolor="#F1F1F1"|
|bgcolor="#F1F1F1"|{{0}}{{convert|8.38|m|abbr=on}}
|bgcolor="#F1F1F1"|{{convert|12.00|m|abbr=on}}
|bgcolor="#F1F1F1"|
|-
|Thickness 12th pier
|
|{{0}}{{convert|4.19|m|abbr=on}}
|
|
|-
|bgcolor="#F1F1F1"|Span 13th arch
|bgcolor="#F1F1F1"|
|bgcolor="#F1F1F1"|
|bgcolor="#F1F1F1"|{{convert|11.00|m|abbr=on}}
|bgcolor="#F1F1F1"|
|-
|Thickness 13th pier
|
|
|
|
|-
|bgcolor="#F1F1F1"|Span 14th arch
|bgcolor="#F1F1F1"|
|bgcolor="#F1F1F1"|
|bgcolor="#F1F1F1"|''{{convert|10.00|m|abbr=on}}''
|bgcolor="#F1F1F1"|
|-
|Thickness 14th pier
|
|
|
|
|-
|bgcolor="#F1F1F1"|Span 15th arch
|bgcolor="#F1F1F1"|
|bgcolor="#F1F1F1"|
|bgcolor="#F1F1F1"|{{0}}{{convert|9.30|m|abbr=on}}
|bgcolor="#F1F1F1"|
|-
|Thickness 15th pier
|
|
|
|
|-
|bgcolor="#F1F1F1"|Span 16th arch
|bgcolor="#F1F1F1"|
|bgcolor="#F1F1F1"|
|bgcolor="#F1F1F1"|{{0}}{{convert|9.10|m|abbr=on}}
|bgcolor="#F1F1F1"|
|-
|}</center>

== See also ==
*[[List of Roman bridges]]
*[[Limyra Bridge]]

== Annotations ==
{{Reflist|group=A.}}

== References ==
{{reflist|3}}

== Sources ==
*{{Citation
 | last = Durán Fuentes
 | first = Manuel 
 | title = La Construcción de Puentes Romanos en Hispania
 | publisher = Xunta de Galicia
 | location = Santiago de Compostela 
 | year = 2004
 | pages = 181–87
 | isbn = 978-84-453-3937-4
}}
*{{Citation
 | last = Fernández Casado
 | first = Carlos 
 | title = Historia del Puente en Espania. Puentes Romanos: Puente de Alconétar
 | publisher = Instituto Eduardo Torroja de la Construcción y del Cemento
 | location = Madrid 
 | year = 1970 
}} <small>without page numbers</small>
*{{Citation
 | last = Galliazzo
 | first = Vittorio
 | title = I ponti romani. Catalogo generale
 | volume = Vol. 2
 | year = 1994
 | publisher = Edizioni Canova
 | location = Treviso
 | isbn = 88-85066-66-6
 | pages = 358–361 (No. 755)
}}
*{{Citation
 | last = Gil Montes
 | first = Juan 
 | contribution = Elementos de la Ingeniería Romana "Via Delapidata"
 | title = Las Obras Públicas Romanas
 | publisher = Congreso Europeo
 | location = Tarragona 
 | year = 2004
 | url = http://www.traianvs.net/textos/delapidata.php 
}}
*{{Citation
 | last1 = González Limón 
 | first1 = Teresa
 | contribution = A Brief Analysis of the Roman Bridges of the Way "La via de la Plata"
 | title = Historical Constructions
 | editor-last = Lourenço
 | editor-first = P. B.
 | editor2-last = Roca 
 | editor2-first = P.
 | pages = 247–256
 | location = Guimarães 
 | year = 2001
 | url = http://www.csarmento.uminho.pt/docs/ncr/historical_constructions/page%20247-256%20_45_.pdf
|display-authors=etal}}
*{{Citation
 | last = O'Connor
 | first = Colin 
 | title = Roman Bridges
 | publisher = Cambridge University Press
 | year = 1993
 | pages = 108f. (SP20), 171
 | isbn = 0-521-39326-4
}}
*{{Citation
 | last = Prieto Vives
 | first = Antonio 
 | title = El Puente Romano de Alconétar
 | journal = Archivo Español de Arte y Arqueología
 | volume = 2
 | pages = 147–158
 |date=May–Aug 1925
}}

== External links ==
{{Commonscat-inline}}
{{Roman bridges}}

{{coord|39.753838|N|6.437345|W|source:dewiki_region:ES-CC_type:landmark|format=dms|display=title}}

{{DEFAULTSORT:Alconetar Bridge}}
[[Category:Roman bridges in Spain]]
[[Category:Bridges over the Tagus River]]
[[Category:Roman segmental arch bridges]]
[[Category:Deck arch bridges]]
[[Category:Stone bridges in Spain]]
[[Category:Buildings and structures in Extremadura]]
[[Category:Relocated buildings and structures]]