{{Infobox person
|name          = Lewis Morris Pease
|image         = 
|image_size    = 
|caption       = 
|birth_name    = 
|birth_date    = {{birth date|1818|8|25}}
|birth_place   = [[Lisle, New York]], [[United States]]
|death_date    = {{death date and age|1897|5|30|1818|8|25}}
|death_place   = [[Asheville, North Carolina]]
|death_cause   = 
|resting_place = 
|resting_place_coordinates = 
|residence     = 
|nationality   = [[United States|American]]
|other_names   = Louis Morris Pease
|known_for     = Methodist clergyman and reformer who founded the Five Points Mission and later the Five Points House of Industry; later established schools for underprivileged children. 
|education     =
|alma_mater    = 
|employer      = 
|occupation    = Clergyman, reformer and educator 
|home_town     = 
|title         = 
|salary        = 
|networth      = 
|height        = 
|weight        = 
|term          = 
|predecessor   = 
|successor     = 
|party         = 
|boards        = 
|religion      = [[Methodist Episcopal Church|Methodist]]
|spouse        = Ann E. Pinney
|partner       = 
|children      = 
|parents       = Philo Pease and Polly Orton
|relations     = 4 brothers, 4 sisters
|signature     = 
|website       = 
|footnotes     = 
}}

Reverend '''Louis''' or '''Lewis Morris Pease''' (August 25, 1818 – May 30, 1897) was an American [[Methodist]] clergyman, educator and prominent reformer during the mid-to late 19th century. He founded the Five Points Mission and later the Five Points House of Industry, established in [[New York City]]'s infamous [[Five Points, Manhattan|Five Points district]], which provided religious teaching and work for the area's predominantly working-class [[Irish Catholics]].

==Biography==
Born to Philo Pease and Polly Orton in [[Lisle, New York]] on August 25, 1818, Lewis Morris Pease joined the New York Conference of the [[Methodist Episcopal Church]] and was ordained an [[Elder (Methodist)|elder]] in 1845.<ref name="Pease">Pease, David and Austin S. Pease. ''A Genealogical and Historical Record of the Descendants of John Pease''. Springfield, Massachusetts: Samuel Bowles & Company, 1869. (pg. 216)</ref> He had originally been commissioned by the Methodist Conference of 1850 to establish a mission in order to reform the area, efforts towards this having been attempted as early as the 1830s, and was able to do so under the supervision of the Ladies' Home Missionary Society that spring.<ref name="Article">"The Rev. Lewis M. Pease". <u>New York Times.</u> 1 Jun 1897</ref>

Upon arriving in the Five Points, he and his wife made their home in a room on Cross Street, near the Old Brewery, and opened their mission. The Ladies' Home Missionary Society believed that the mission's primary purpose was to preach the gospel, provide religious services and bring in converts for the Methodists, which Pease did for several months, however he eventually came to the conclusion that rehabilitation could more likely be achieved by providing education and employment to those who, without other alternatives, would likely turn to crime in order to survive.<ref name="Asbury">[[Herbert Asbury|Asbury, Herbert]]. ''The Gangs of New York: An Informal History of the New York Underworld''. New York: Alfred A. Knopf, 1928. (pg. 15-16) ISBN 1-56025-275-8</ref>

In spite of his humanitarian efforts, Pease was dismissed by Ladies' Home Missionary Society within a year. This was prompted when a group a ladies from the society came to visit Pease and learned that he had not preached a sermon in two days having been ''"too busy carting great loads of cloth from the manufacturing houses in Broadway to his Five Points workrooms"''. He was replaced by evangelist Rev. John Luckey and the society publicly criticized his work in the area. In 1854, the society published a book on the history of the Methodist mission in the Five Points, ''The Old Brewery'', in which Pease received no mention with exception to a brief reference to ''"our first missionary"''. Pease and his wife refused to leave the Five Points and instead opened a nondenominational mission.<ref name="Asbury"/>
[[File:(King1893NYC) pg428 FIVE-POINTS HOUSE OF INDUSTRY, 155 WORTH STREET, OPPOSITE PARADISE PARK.jpg|thumb|left|FIVE-POINTS HOUSE OF INDUSTRY, 155 WORTH STREET, OPPOSITE PARADISE PARK]]

After the falling out between him and the society, Pease and his wife leased several buildings and established the Five Points House. Schools for children and adults were held and, supervised by Pease and his wife, workrooms were opened in which material was received from local clothing manufacturers to be made into cheap garments. His reputation and success grew and other reformers joined his cause. He also received generous donations from wealthy and prominent New Yorkers. The Five Points House of Industry was founded in 1854 and organized under a Board of Trustees with Pease elected as a member. He held the position of superintendent for eight years.<ref name="Pease"/> Its first building, a commodious home, was erected in Anthony Street two years later <ref name="Article"/> and, in 1864, the old tenements in [[Cow Bay]] were purchased and torn down to build a larger mission house.<ref name="Asbury"/> He and his wife later retired to a farm in [[Westchester, New York]] which was owned by the Five Points House of Industry and which taught farming and agricultural methods.<ref name="Pease"/>

In 1870, Pease left New York and settled in [[Asheville, North Carolina]] where he spent the next thirty years dedicated to providing education to poor and disadvantaged children. He founded the Pease Industrial School and the Normal & Collegiate School for white girls, the Boys Industrial and Farm School for white boys, and the Colored Industrial School for negro boys and girls. Pease died in Asheville on the night of May 30, 1897.<ref name="Article"/>

==References==
{{Reflist}}

==Further reading==
*Anbinder, Tyler. ''Five Points: The 19th-century New York City Neighborhood that Invented Tap Dance, Stole Elections, and Became the World's Most Notorious Slum''. New York: Simon and Schuster, 2001. ISBN 0-684-85995-5
*[[Jacob Riis|Riis, Jacob A]]. ''How the Other Half Lives: Studies Among the Tenements of New York''. New York: Charles Scribner's Sons, 1890.

{{DEFAULTSORT:Pease, Lewis Morris}}
[[Category:1818 births]]
[[Category:1897 deaths]]
[[Category:American Methodists]]
[[Category:People from Asheville, North Carolina]]
[[Category:People from Lisle, New York]]
[[Category:People from Westchester County, New York]]