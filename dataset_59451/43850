<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Villiers II
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Fighter aircraft]]
 | national origin=[[France]]
 | manufacturer=[[Ateliers d'Aviation François Villiers]]
 | designer=
 | first flight=1925
 | introduced=1927
 | retired=1928
 | status=
 | primary user=[[Aéronautique Maritime]]
 | number built=32
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Villiers II''' was a [[France|French]] two-seat fighter aircraft of the 1920s intended for operation from the [[Aircraft carrier]] [[French aircraft carrier Béarn|Béarn]] of the [[French Navy]].  It was a single-engined [[tractor configuration|tractor]] [[biplane]] with a waterproof hull in the form of a flying boat to allow the aircraft to be safely landed on water in an emergency.  Two prototypes and 30 production aircraft were built, the type serving briefly with the French Navy, although never operated from an aircraft carrier.

==Development and design==

In 1924, the French aircraft manufacturer [[Ateliers d'Aviation François Villiers]] was formed at [[Meudon]] near Paris.<ref name="Gunston Man p484">Gunston 2005, p.484.</ref> One of its first designs was to meet a requirement of the French Navy for a two-seat shipboard fighter.  Villier's design, the Villiers II was a single-engined [[sesquiplane]] (i.e. a [[biplane]] with the lower wing much smaller than the upper wing). Although of conventional [[tractor configuration]] with the engine in the nose driving a two-bladed propeller, and a [[Conventional landing gear|tailwheel]] undercarriage, in order to allow safe [[Water landing|ditching]] in the event of an emergency, it had a number of unusual features, with the fuselage being watertight and in the form of a [[flying boat]] hull, and the undercarriage being jettisonable.<ref name="complete p582"/><ref name="Flight 26 p789"/>

Two prototypes were ordered, under the designation Vil 2<small>AM</small>C2 (''Avion Marin Chasse Biplace''), one powered by a 450&nbsp;hp (338&nbsp;kW) [[Hispano-Suiza 12H]] [[V12 engine]] and the other with a similarly powered [[Lorraine-Dietrich 12Eb]] [[W12 engine]]. After evaluation by the [[Aéronautique Maritime]] in May 1925, an order for 30 Vil 2<small>AM</small>C2 to be powered by the Lorraine-Dietrich engine, (together with an order for 20 of the competing [[Levasseur PL.5]]s) was placed on 19 December 1925.<ref name="complete p582"/>

==Operational history==
The Villiers II entered service with [[Escadrille 5C1|''Escadrille'' 5C1]] based at [[Hyères]] near [[Toulon]] in Southern France in May 1927, with the aircraft never being operated from the [[French aircraft carrier Béarn|Béarn]].  It was replaced by the single-seat [[Gourdou-Leseurre GL.30|Gourdou-Leseurre GL.32]], which did not have the same elaborate features for landing on water in September 1928.<ref name="complete p582"/>

==Operators ==
;{{FRA}}
*[[Aéronautique Maritime]]
**[[Escadrille 5C1|''Escadrille'' 5C1]]

==Specifications (Vil 2<small>AM</small>C2 ) ==

{{aircraft specifications
|plane or copter?=plane<!-- options: plane/copter -->
|jet or prop?=prop<!-- options: jet/prop/both/neither -->
|ref=The Complete Book of Fighters <ref name="complete p582">Green and Swanborough 1994, p.582.</ref> 
|crew=2
|capacity= <!-- the number of passengers carried in the case of a commercial aircraft-->
|payload main=
|payload alt=
|payload more=
|length main= 9.50 m
|length alt=31 ft 2 in
|span main=13.00 m
|span alt=42 ft 8 in
|height main=3.96 m
|height alt=12 ft 11⅞ in
|area main= 40.0 m²
|area alt= 431 sq ft
|airfoil=
|empty weight main= 1,260 kg
|empty weight alt= 2,780 lb
|loaded weight main= 1,900 kg
|loaded weight alt= 4,190 lb
|useful load main= 
|useful load alt= 
|max takeoff weight main= 
|max takeoff weight alt= 
|max takeoff weight more=
|more general=
|engine (prop)=[[Lorraine-Dietrich 12E]]b
|type of prop= 12-cylinder [[W engine]]<!-- meaning the type of propeller driving engines -->
|number of props=1<!--  ditto number of engines-->
|power main= 450 hp
|power alt=338 KW
|power original=
|power more=
|max speed main= 217 km/h
|max speed alt=117 knots, 135 mph
|max speed more= at sea level
|cruise speed main= 
|cruise speed alt=
|cruise speed more 
|stall speed main= 
|stall speed alt= 
|stall speed more=
|never exceed speed main= 
|never exceed speed alt= 
|range main= 
|range alt=
|ferry range main=
|ferry range alt=
|ferry range more=
|ceiling main= 6,250 m <ref name="Flight 26 p789">[http://www.flightglobal.com/pdfarchive/view/1926/1926%20-%200917.html ''Flight'' 2 December 1926, p.789.]</ref>
|ceiling alt= 20,500 ft
|climb rate main= 
|climb rate alt= 
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=
|power/mass alt=
|more performance=*'''Endurance:''' 3 hours
*'''Climb to 6,000 m (19,700 ft):''' 28 min
|guns= <br />
**2× fixed, forward firing synchronised 7.7 mm ([[.303 British|.303 in]] [[Vickers gun]]s
**2× flexibly mounted [[Lewis gun]]s in rear cockpit
|bombs= 
|rockets= 
|missiles= 
|hardpoints= 
|hardpoint capacity=  
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=*[[Levasseur PL.5]]<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{reflist}}
{{refbegin}}
*Green, William and Swanborough, Gordon. ''The Complete Book of Fighters''. New York:Smithmark, 1994. ISBN 0-8317-3939-8.
*[[Bill Gunston|Gunston, Bill]]. ''World Encyclopedia of Aircraft Manufacturers''. Stroud, UK:Sutton Publishing, 2nd Edition, 2005. ISBN 0-7509-3981-8.
*"[http://www.flightglobal.com/pdfarchive/view/1926/1926%20-%200875.html The Paris Airshow 1926]". ''[[Flight International|Flight]]'', 2 December 1926, pp.&nbsp;775–791.
{{refend}}

==External links==
*[http://www.aviafrance.com/5410.htm aviafrance]
{{Villiers aircraft}}
[[Category:French fighter aircraft 1920–1929]]
[[Category:Carrier-based aircraft]]
[[Category:Sesquiplanes]]
[[Category:Villiers aircraft]]