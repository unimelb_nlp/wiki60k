{{Italic title}}
'''''Armance''''' is a [[romance novel]] set during the [[Bourbon Restoration]] by [[Stendhal]], published anonymously in 1827.<ref name="Garzanti">{{cite book |title=Enciclopedia Garzanti della letteratura |last=Garzanti |first=Aldo |authorlink=Aldo Garzanti |year=1974 |origyear=1972 |publisher=[[Garzanti]] |location=Milan |page=811 |language=Italian}}</ref> It was Stendhal's first novel, though he had published essays and critical works on literature, art, and travel since 1815.

==Plot==
It concerns Octave de Malivert, a taciturn but brilliant young man barely out of the [[École Polytechnique]], who is attracted to Armance Zohiloff, who shares his feelings. The novel describes how a series of misunderstandings have kept the lovers Armance and Octave divided. Moreover, a series of clues suggest that Octave is impotent as a result of a severe accident. Octave is experiencing a deep inner turmoil; he himself illustrates the pain of the century's romantics. When the pair do eventually marry, the slanders of a rival convince Octave that Armance had married only out of selfishness. Octave leaves to fight in Greece, and dies there of sorrow.<ref name="Garzanti"/>

''Armance'' is based on the theme of ''Olivier'', a novel by the Duchess [[Claire de Duras]], whose scabrous nature forbade publication. But Stendhal has very quietly inserted the secret, without talking about it openly.

==Critical reception==
[[André Gide]] regarded this novel as the best of Stendhal's novels, whom he was grateful to for having created a helpless lover, even if he reproached him for having eluded the fate of this love: "I can hardly convince myself that Armance, as painted for us by Stendhal, would have been suited by it."

In [[Umberto Eco]]'s novel ''[[The Prague Cemetery]]'', the protagonist Simone Simonini pleads with another character, [[Yuliana Glinka]], that he suffers the same fate as Stendhal's Octave de Malivert -whose readers had long speculated about – and thus can't pursue the offer she had made him.

==Notes and references==
{{Reflist}}

==External links==
{{Wikisourcelang|fr|Armance|Armance}}
*[http://ebooks.adelaide.edu.au/s/stendhal/armance/ Full text of ''Armance''] in English.

{{Stendhal}}

{{Authority control}}
{{DEFAULTSORT:Armance (Novel)}}
[[Category:1827 novels]]
[[Category:Novels by Stendhal]]
[[Category:French romance novels]]
[[Category:19th-century French novels]]
[[Category:Novels set in France]]
[[Category:Bourbon Restoration]]


{{1820s-novel-stub}}
{{Romance-novel-stub}}