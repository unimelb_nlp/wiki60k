{{Infobox book  | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = Oakfield; or, Fellowship in the East
| title_orig    =
| translator    =
| image         =
| caption =
| author        = [[William Delafield Arnold|William Arnold]]
| illustrator   =
| cover_artist  =
| country       = United Kingdom
| language      = English
| series        =
| subject       = <!-- Subject is not relevant for fiction -->
| genre         = [[Novel]]<!-- [[Wikipedia:WikiProject Novels/Novel categorization]] -->
| publisher     = [[Longman, Brown, Green, and Longmans]]
| release_date  = [[1853 in literature|1853]]
| english_release_date =
| media_type    = Print ([[Hardcover]])
| pages         =
| preceded_by   = <!-- Preceding novel in series -->
| followed_by   = <!-- Following novel in series -->
}}

'''''Oakfield; or, Fellowship in the East''''' is a [[novel]] by [[William Delafield Arnold]], first published in 1853. The book is one of the earliest novelistic accounts of life in [[British India]], and its plot strongly mirrors the biography of its author. Set in India in the years surrounding the [[First Afghan War]], the novel describes the unhappy experiences of the eponymous Edward Oakfield, an [[University of Oxford|Oxford]] graduate who, we are told, enlisted in the [[East India Company]]'s military service because he had grown tired of the metaphysical debates dominating that university. In India, Oakfield is repelled by what he sees as an absence of Christian gentlemanliness among the Company's military officers, and he soon retreats to introspection and the comradeship of a few, thinly spread, kindred spirits.

==Major themes==
The novel is a stinging indictment of the moral standards of the British regiments in India. Indeed Arnold, fearing a backlash, originally published the novel under the pseudonym Punjabee. The second edition, of 1854, reveals the author's identity and adds a preface which functions as an [[apologia]].

==References==
D Goonetilleke, "Forgotten Nineteenth-century Fiction: William Arnold's Oakfield and William Knighton's Forest Life in Ceylon" ''The Journal of Commonwealth Literature'' 7 (1972): 14-21.

==External links==
* [http://books.google.ca/books?id=ujooAAAAYAAJ&printsec=frontcover&dq=william+arnold+oakfield&source=bl&ots=N1N2LqnUCT&sig=0EKRCK4-ep2xRR-uMPuMP3t1Slw&hl=en&ei=avIHTIOWK4zAMvnKlLYE&sa=X&oi=book_result&ct=result&resnum=1&ved=0CBgQ6AEwAA#v=onepage&q&f=false ''Oakfield; or Fellowship in the East''] (2nd Edition) at [[Google Books]]

{{DEFAULTSORT:Oakfield Or, Fellowship In The East}}
[[Category:1853 novels]]
[[Category:Novels set in India]]
[[Category:British India in fiction]]
[[Category:19th-century British novels]]


{{1850s-novel-stub}}