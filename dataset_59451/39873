{{Infobox person
| name          = Mary Goodrich Jenson
| image         = Mary_Goodrich_Jenson_died_2004.jpg
| alt           = 
| caption       = Mary Goodrich Jenson - aviator
| birth_name    = 
| birth_date    = {{birth year|1907}}
| birth_place   = Hartford, Connecticut
| death_date    = {{death date and age|2004|01|04|1907}}
| death_place   = Hartford
| nationality   = American
| spouse = Carl D. Jenson
| other_names   = 
| occupation    = journalist
| years_active  = 
| known_for     = Ninety-Nines
| notable_works = 
}}

'''Mary Goodrich Jenson''' (1907 &ndash; January 4, 2004) was an early woman aviator and journalist who became the first woman in Connecticut to earn a pilot’s license and the first woman to fly solo to [[Cuba]]. She was inducted into the [[Connecticut Women's Hall of Fame]] in 2000.

==Education and personal life==
Mary Goodrich was born in Hartford, Connecticut in 1907 to Ella E. (Reed) Goodrich and James Raymond Goodrich.<ref name=cwhf/><ref name=obit/> Her grandfather Elizur Stillman Goodrich ran the  Hartford–New York Steamboat Company and the Hartford–Wethersfield Horse Railway.<ref name=wethersfield/> She attended the Collegio Gazzola in Verona, Italy, and [[Gibbs College]] before moving on to [[Columbia University]].<ref name=cwhf/>

In 1940, she married Carl D. Jenson, with whom she had two children.<ref name=cwhf/><ref name=wethersfield/>

==Career as pilot and journalist==
Jenson earned her pilot's license in 1927, at the age of 20, thereby becoming the first woman in Connecticut to achieve that milestone.<ref name=cwhf/> While she was still training for her pilot’s license, she also pursued a career in journalism. The ''[[Hartford Courant]]'' hired her as its first aviation editor on condition that she obtain her pilot's license before a rival reporter for the ''Hartford Times'', which she did.<ref name=wethersfield/> She would later become the first woman to have a bylined column in the paper.<ref name=cwhf/>

A year after obtaining her license, Jenson bought her first plane, a [[Fairchild 21|Fairchild KR-21]] single-engine biplane.<ref name=wethersfield/> She competed in air shows in events such as racing and “bomb throwing,” which involved dropping bags of flour at ground targets.<ref name=wethersfield/> 

In the late 1920s, a group of women pilots formed a national organization called the [[Ninety-Nines]] to provide support for women in aviation. Jenson was one of the 99 charter members of the group.<ref name=cwhf/><ref name=nyt/>

Jenson was a director of the short-lived [[Betsy Ross Air Corps]] (1929–1933), which was founded during the Depression to support the Army Air Corps, though it was never formally recognized by the U.S. military.<ref name=cwhf/><ref name=nyt/>

Jenson flew her biplane all around the state and around 1933 became the first woman to fly solo to Cuba.<ref name=cwhf/> Due to failing vision, she lost her flying license after the Cuba trip.<ref name=wethersfield/> 

==Post-flying career==
In 1936, Jenson was a passenger on the [[LZ 129 Hindenburg|Hindenburg]] airship for a flyover of Hartford during its first and only full year of service.<ref name=cwhf/>

In the later 1930s, Jenson briefly worked for [[Walt Disney Productions]] in California, especially on the film ''[[Dumbo]]''.<ref name=cwhf/><ref name=wethersfield/> It was there that she met her future husband.<ref name=cwhf/> 

In 1941, they moved to Wethersfield, Connecticut, where Jenson became heavily involved in civic volunteerism. She founded and served as president for the town's Women’s Association and also served on the Board of Education and the Council of Social Agencies of Greater Hartford.<ref name=cwhf/>

Mary Jenson died in Hartford in early 2004.<ref name=obit/>

==Legacy==
Jenson's family established a memorial fund in her honor at the Wethersfield Historical Society, which holds numerous historical objects and artefacts from Jenson and her family.

==References==
{{reflist| refs=

<ref name=nyt>Ahles, Dick. [https://www.nytimes.com/2004/12/26/nyregion/the-extraordinary-who-lived-among-us.html?_r=0 "The Extraordinary Who Lived Among Us"]. ''New York Times'', Dec. 26, 2004.</ref>

<ref name=obit>[http://articles.courant.com/2004-01-05/news/0401040977_1_salvation-army-west-hartford-carl-d-jenson "Jenson, Mary (Goodrich)"]. ''Hartford Courant'', January 5, 2004. (Obituary)</ref>

<ref name=wethersfield>[http://wethersfieldhistory.org/collections/girl-pilot-mary-goodrich-jenson/ "Girl Pilot: Mary Goodrich Jenson"]. Wethersfield Historical Society, Feb. 24, 2015.</ref>

<ref name=cwhf>[http://www.cwhf.org/inductees/writers-journalists/mary-goodrich-jenson/#.V6ynpY5peCx "Mary Goodrich Jenson"]. Connecticut Women's Hall of Fame.</ref>

}}


{{Authority Control}}
{{DEFAULTSORT:Jenson, Mary Goodrich}}

[[Category:1907 births]]
[[Category:2004 deaths]]
[[Category:American female aviators]]
[[Category:American women journalists]]
[[Category:American journalists]]
[[Category:Aviation pioneers]]
[[Category:People from Hartford, Connecticut]]
[[Category:Columbia University alumni]]