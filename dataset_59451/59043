{{Infobox journal
| title = Natural Language & Linguistic Theory
| cover = [[File:Natural Language and Linguistic Theory.jpg]]
| editor = [[Julie Anne Legate]]
| discipline = [[Theoretical linguistics]]
| abbreviation = Nat. Lang. Linguist. Theory
| publisher = [[Springer Science+Business Media]]
| country =
| frequency = Quarterly
| history = 1983-present
| openaccess =
| impact = 0.845
| impact-year = 2015
| website = http://www.springer.com/journal/11049/
| link2 = http://link.springer.com/journal/volumesAndIssues/11049
| link2-name = Online archive
| JSTOR = 0167806X
| OCLC = 863227917
| LCCN =
| ISSN = 0167-806X
| eISSN = 1573-0859
}}
'''''Natural Language & Linguistic Theory''''' is a quarterly [[peer-reviewed]] [[academic journal]] covering [[theoretical linguistics|theoretical]] and [[generative linguistics]]. It was established in 1983 and originally published by [[Kluwer Academic Publishers]]. Since 2004 the journal is published by [[Springer Science+Business Media]]. The [[editor-in-chief]] is [[Julie Anne Legate]] ([[University of Pennsylvania]]).

The journal carries a "Topic-Comment" column (initiated by [[Geoffrey K. Pullum]]), in which a contributor presents a personal, sometimes controversial, opinion on some aspect of the field.

==Abstracting and indexing==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
*[[Academic OneFile]]
*[[Arts & Humanities Citation Index]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2016-07-30}}</ref>
*[[Bibliographie linguistique]]/Linguistic bibliography
*[[Current Contents]]/Social & Behavioral Sciences<ref name=ISI/>
*Current Contents/Arts and Humanities<ref name=ISI/>
*[[EBSCO Information Services|EBSCO databases]]
*[[FRANCIS]]
*[[International Bibliography of Periodical Literature]]
*[[MLA International Bibliography]]
*[[PASCAL (database)|PASCAL]]
*[[ProQuest|ProQuest databases]]
*[[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2016-07-30}}</ref>
*[[Social Science Citation Index]]<ref name=ISI/>
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 0.845.<ref name=WoS>{{cite book |year=2016 |chapter=Natural Language & Linguistic Theory |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

==References==
{{Reflist}}

==External links==
*{{Official website|http://www.springerlink.com/content/102968/}}

[[Category:Linguistics journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1983]]