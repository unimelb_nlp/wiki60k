{{Infobox Journal
| cover =
| discipline = [[United States labor law]]
| abbreviation = Berkeley J. Emp. & Lab. L.
| publisher = [[University of California, Berkeley School of Law]]
| country = [[United States]]
| frequency = Semiannual
| history = 1975–present
| openaccess =
| website = http://www.bjell.org
| ISSN = 1067-7666
}}
The '''''Berkeley Journal of Employment & Labor Law''''' (BJELL) is a law  journal that publishes articles focusing on current developments in labor and employment law. It was founded in 1975 as the '''Industrial Relations Law Journal'''. It changed its name to the current title in 1993. Articles in the journal cover legal issues dealing with employment discrimination, "traditional" labor law, public sector employment, international and comparative labor law, employee benefits, and the evolution of the doctrine of wrongful termination. In addition to scholarly articles, the journal includes student-authored comments, book reviews and essays. It is published twice a year by Berkeley Law.

BJELL is the most cited employment law journal in the world.<ref>{{cite web|url=http://lawlib.wlu.edu/lj/index.aspx |title=Law Journals: Submissions and Ranking |publisher=Lawlib.wlu.edu |date=2013-04-13 |accessdate=2013-04-13}}</ref>

In order "to bring attention to the study and practice of American labor law and to spur the academic exchange of ideas about its contemporary significance," BJELL holds the annual David E. Feller Memorial Labor Law Lecture.<ref>http://www.bjell.org/FellerLecture/tabid/67/Default.aspx</ref>

==References==
{{Reflist}}

==External links==
* [http://www.law.berkeley.edu/ Berkeley Law Web site]

[[Category:Labour law journals|Berkeley Journal of Employment and Labor Law]]
[[Category:University of California, Berkeley|Journal of Employment and Labor Law, Berkeley]]
[[Category:Publications established in 1979]]
[[Category:United States labor law]]
[[Category:Law in the San Francisco Bay Area]]


{{law-journal-stub}}