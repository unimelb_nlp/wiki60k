{{Infobox journal
| title = Manufacturing & Service Operations Management (M&SOM)
| cover = MSOM_Cover.jpg
| editor = [[Christopher S. Tang]]
| discipline = [[Management]]
| abbreviation = Manuf. Serv. Oper. Manag.
| publisher = [[Institute for Operations Research and the Management Sciences]]
| country =
| frequency = Quarterly
| history = 1999-present
| openaccess =
| impact = 1.966
| impact-year = 2015
| website = http://pubsonline.informs.org/journal/msom
| link1 = http://pubsonline.informs.org/toc/msom/current
| link1-name = Online access
| link2 = http://pubsonline.informs.org/loi/msom
| link2-name = Online archive
| ISSN = 1523-4614
| eISSN = 1526-5498
| OCLC = 60637557
| CODEN = MSOMFV
| LCCN = sn99008226 
}}
'''''Manufacturing & Service Operations Management''''' is a quarterly [[peer-reviewed]] [[academic journal]] established in 1999 that is published by the [[Institute for Operations Research and the Management Sciences]]. It covers analytical research about [[operations management]] in the [[manufacturing]]/service industry. According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 1.966.<ref name=WoS>{{cite book |year=2016 |chapter=Manufacturing & Service Operations Management |title=2015 [[Journal Citation Reports]] |publisher=[[Clarivate Analytics]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

==Editors-in-chief==
The following persons are, or have been, [[editors-in-chief]]:
* 2015–present: [[Christopher S. Tang]], [[UCLA]]
* 2009–2014: Stephen C. Graves,  [[MIT]]
* 2005–2009: Gérard Cachon, [[UPenn]]
* 2003–2005: Garrett J. van Ryzin, [[Columbia University]]
* 1999–2003: Leroy Schwarz, [[Purdue University]]

==References==
{{reflist}}

==External links==
*{{Official website|http://pubsonline.informs.org/journal/msom}}

{{DEFAULTSORT:Manufacturing And Service Operations Management}}
[[Category:Business and management journals]]
[[Category:Quarterly journals]]
[[Category:INFORMS academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1999]]