{{Infobox journal
| title = Australian Journal of Management
| cover = [[File:Australian Journal of Management - cover.gif]]
| editor = Baljit Sidhu
| discipline = [[Management]]
| former_names = 
| abbreviation = Aust. J. Manag.
| publisher = [[Sage Publications]]
| country = 
| frequency = Triannual
| history = 1976-present
| openaccess = 
| license = 
| impact = 0.795
| impact-year = 2013
| website = http://www.sagepub.com/journals/Journal201956/title
| link1 = http://aum.sagepub.com/content/current
| link1-name = Online access
| link2 = http://aum.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 181818942
| LCCN = sn84010478
| CODEN = 
| ISSN = 0312-8962
| eISSN = 1327-2020
}}
The '''Australian Journal of Management''' is a triannual [[peer-reviewed]] [[academic journal]] that covers research in [[accounting]], [[applied economics]], [[finance]], [[industrial relations]], [[political science]], [[psychology]], [[statistics]], and other disciplines in relation to their application to [[management]]. The journal was established in 1976 and is published by [[Sage Publications]] in association with the [[Australian School of Business]]. The [[editor-in-chief]] is Baljit Sidhu ([[University of New South Wales]]). The founding editor was [[Ray J. Ball]].

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]], the [[International Bibliography of the Social Sciences]], and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 0.795.<ref name=WoS>{{cite book |year=2014 |chapter=Australian Journal of Management |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== Editors ==
The following persons have been editors-in-chief of the journal:
* Ray J. Ball
* Chris Adam
* [[John Conybeare]]
* Vic Taylor
* Phillip Yetton
* [[John Roberts (businessman)|John Roberts]]
* [[Robert Marks (professor of management)|Robert Marks]]

== References ==
{{reflist}}

==External links==
* {{Official website|http://www.sagepub.com/journals/Journal201956/title}}

{{DEFAULTSORT:Australian Journal Of Management}}
[[Category:Business and management journals]]
[[Category:Publications established in 1976]]
[[Category:English-language journals]]
[[Category:SAGE Publications academic journals]]
[[Category:Triannual journals]]