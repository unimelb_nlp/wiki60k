{{good article}}
{{Infobox military unit
|unit_name= 331st Rifle Division (August 27, 1941&nbsp;– July 15, 1945)
|image = File:Major_General_Pyotr_Filippovich_Berestov.jpg
|caption = Division commander Major General P.F. Berestov, postwar
|dates= 1941–1945
|country= {{flag|Soviet Union|1923}}
|allegiance= 
|branch= [[File:Red Army flag.svg|23px]] [[Red Army]]
|type= Division
|role= Infantry
|size= 
|command_structure= 
|garrison= 
|motto= 
|march= 
|mascot= 
|battles= [[Battle of Moscow]]<br />[[Battles of Rzhev]]<br />[[Battle of Smolensk (1943)]]<br />[[Operation Bagration]]<br />[[Vistula-Oder Offensive]]<br />[[Battle of Königsberg]]<br />[[Prague Offensive]]
|anniversaries= 
| decorations = [[File:Order of the red Banner OBVERSE.jpg|20px]][[Order of the Red Banner]] (2) <br>
[[File:Order of suvorov medal 2nd class.jpg|20px]][[Order of Suvorov]] 2nd class
| battle_honours = [[Proletarian]]<br>
[[Bryansk]]<br>
[[Smolensk]]
|notable_commanders= Major General [[Fyodor Korol]]<br />[[Pyotr Berestov|Major General P.F. Berestov]] [[File:Hero of the Soviet Union medal.png|10px]]
| image_size    = 200px
|current_commander= 
}}
The '''331st Rifle Division''' was formed as a standard [[Red Army]] rifle division in the summer of 1941, based on a cadre of volunteer workers and reservists from the [[Bryansk]] oblast, and so was known from the beginning as the ''331st Bryansk Proletarian Rifle Division''. It fought to defend Moscow during the last stages of the German invasion, and then went over to the offensive in early December. It spent much of the next twelve months in the same general area, west of the capital, taking part in the mostly futile battles against the German-held salient at Rzhev. On September 25, 1943, the division shared credit with several other units for the liberation of the city of [[Smolensk]] and was given its name as an honorific. The 331st had a highly distinguished career as a combat unit, ending its combat path in Czechoslovakia, advancing on Prague.

== Formation ==
The formation of the 331st Rifle Division began on August 27, 1941, in the [[Tambov Oblast]] of the [[Oryol Military District]], under the command of Major General [[Fyodor Korol]]. Korol led the division until mid-February, 1942. Its order of battle was as follows:
* 1104th Rifle Regiment
* 1106th Rifle Regiment
* 1108th Rifle Regiment
* 896th Artillery Regiment<ref>Charles C. Sharp, ''"Red Tide", Soviet Rifle Divisions Formed from June to December 1941, Soviet Order of Battle World War II'', Nafziger, 1996, p. 79</ref>
* 253rd Antitank Battalion
* 508th Mortar Battalion
* 509th Sapper Battalion
* 783rd Signal Battalion
* 394th Reconnaissance Company

The division was moved to the [[Moscow Military District]] in October where it was assigned to the newly-forming 26th (Reserve) Army, Under the [[Reserve of the Supreme High Command]]. Some elements of the division entered active service in a highly dramatic manner, by first marching through [[Red Square]] in the famous [[October Revolution]] anniversary parade on November 7, then straight on to the front lines just 10–15&nbsp;km away, being assigned to the [[20th Army (Soviet Union)|20th Army]] of the [[Western Front (Soviet Union)|Western Front]].<ref>Sharp, ''"Red Tide"'', p. 79. Dunn states this assignment was not made until December; Walter S. Dunn, ''Stalin's Keys to Victory'', Stackpole Books, Mechanicsburg, PA, 2007, p. 82</ref>

== Combat service ==
The division played a vigorous role in the defense of Moscow. Major General [[Leonid Sandalov]], former chief of staff of the 20th Army, inspected the 1106th Rifle Regiment on its arrival in Moscow and noted, "The warmly clothed and adequately equipped subunits of the regiment made a good impression [on me]." As November moved into December, the 331st, fighting in the area of the [[Moscow Canal|Moscow-Volga canal]], stopped the enemy advance at [[Lobnya|Lobnia Station]], 25&nbsp;km from Moscow. On December 2, as one of the harbingers of the wider counter-offensive that started a few days later, the division took part in a strong counter-attack from the area of [[Khlebnikovo, Dolgoprudny Microdistrict|Khlebnikovo]] towards [[Lobnya|Krasnaia Poliana]]. Backed by tanks and artillery, the attack made limited gains, but on the 6th it merged with the general offensive, broke into the village with the help of 28th Rifle Brigade, secured it, and took a [[21 cm Mörser 18|German 210mm gun]], which had been used to shell Moscow, as a trophy. In the next two days the division and the brigade advanced 4.5&nbsp;km further and completely penetrated the German defenses, but the lack of skis and armor support limited the planned advance of 30&nbsp;km to only about 10 – 12&nbsp;km. Nevertheless, by December 20 the division had liberated the town of [[Volokolamsk]].<ref>Aleksander A. Maslov, ''Fallen Soviet Generals'', ed. and trans. David M. Glantz, Frank Cass Publishing, London, UK, 1998, pp. 63, 65</ref>

The 331st would remain in the Western Front until April, 1944. In early 1942 it was briefly transferred to the [[5th Army (Soviet Union)|5th Army]], before returning to the 20th Army, where it remained until March, 1943. In April, 1942, Colonel (later Major General) [[Pyotr Berestov]] took command and held it for the duration of the war.<ref>{{cite web|url=http://www.generals.dk/general/Berestov/Petr_Filippovich/Soviet_Union.html|title=Biography of Major-General Petr Filippovich Berestov - (Петр Филиппович Берестов) (1896 – 1961), Soviet Union|website=Generals.dk|accessdate=2016-08-30}}</ref>

In the planning for [[Operation Mars]] in November, the division was given a leading role in the 20th Army's assault. Alongside the [[247th Rifle Division (Soviet Union)|247th Rifle Division]], and supported by the 80th and 240th Tank Brigades, the 331st was tasked to cross the mostly-frozen [[Vazuza River]] between Trostino and Pechora, to take German strong points at Zevalovka and Prudy. On the second day, the second German defensive position would be taken. Following this the division would cross the railroad between [[Rzhev]] and [[Sychyovka, Sychyovsky District, Smolensk Oblast|Sychyovka]], and reduce the German strong point at Khlepen. In the event, on November 25, the division successfully forced the river line and took Prudy, but was halted by heavy fire from Khlepen. The 247th had made greater progress and the rest of the 331st reinforced that bridgehead. Overnight, the front commander, [[Ivan Konev|General I.S. Konev]], decided to try to pass a mobile reserve force through this scant lodgement to complete the breakthrough. The following day the two rifle divisions continued to struggle to expand the bridgehead, but with little success. The tanks and cavalry of the reserve managed to tear a hole in the defenses and get into the enemy rear, but the infantry found it impossible to follow. The gains made by the 331st over the following days were negligible. For most of the remainder of the operation the division held the left flank of its army, recovering from its losses.<ref>David M. Glantz, ''Zhukov's Greatest Defeat'', University Press of Kansas, Lawrence, KS, 1999, pp. 52, 84–88, 94</ref> In the period from November 25 to December 18 the division lost 597 men killed, 1,445 wounded, and 106 missing-in-action, for a total of 2,148 casualties.<ref>Glantz, ''After Stalingrad'', Helion & Co., Ltd., Solihull, UK, 2009, p. 90</ref>

In April, 1943, the 331st was transferred to the [[45th Rifle Corps]] in the [[31st Army (Soviet Union)|31st Army]]; it would remain in this army for the duration of the war. For most of this time it was designated as an assault division, and on Sept. 25 it was granted the honorific "Smolensk" for its role in the liberation of that city.<ref>Sharp, ''"Red Tide"'', p. 79</ref>{{quotation|"SMOLENSK" - 331st Rifle Division (Major General Berestov, Pyotr Filippovich)... The troops who participated in the battles of Smolensk and [[Roslavl]], by the order of the Supreme High Command of September 25, 1943, and a commendation in Moscow, are given a salute of 20 artillery salvoes from 224 guns.<ref>{{cite web|url=http://www.soldat.ru/spravka/freedom/1-ssr-5.html |title=Освобождение городов |website=Soldat.ru |date= |accessdate=2016-11-04}}</ref>}} Eventually it earned the moniker of being "the best in the 31st Army".<ref>Boris Gorbachevsky, ''Through the Maelstrom'', ed. and trans. by Stuart Britton, University Press of Kansas, Lawrence, KS, 2008, p. 333</ref>

== Operation Bagration ==
In March, 1944, the division was transferred to the [[71st Rifle Corps]], where it would remain for the duration of the conflict. At about the same time, the 31st Army became part of the [[3rd Belorussian Front]]. In the initial stages of [[Operation Bagration]], the front's immediate objective was the city of [[Orsha]]. The 331st had the direct support of the 959th SU Regiment ([[SU-76]]s) for its initial assault; this regiment had been assigned to support of the corps since the beginning of June.<ref>Sharp, ''"Red Hammers", Soviet Self-Propelled Artillery and Lend Lease Armor 1941 - 1945, Soviet Order of Battle World War II, vol. XII'', Nafziger, 1998, p. 51</ref> In recognition of the 331st's role in the liberation of Orsha, on June 27, it was awarded the [[Order of Suvorov]], 2nd Class. The division went on to assist in the liberation of [[Barysaw|Borisov]]. While two divisions of the [[8th Guards Rifle Corps]] of the [[11th Guards Army]] attacked from the west and north, the 331st drove into the south of the city and it was cleared of German forces by 3:00 am on July 1. In the following days it took part in the liberation of [[Minsk]], before joining the general advance to the 1941 Soviet-German frontier.<ref>Dunn, ''Soviet Blitzkrieg'', Stackpole Books, Mechanicsburg, PA, 2008, pp. 140–41, 156</ref> In recognition of his successful command of the division in this operation, Major General Berestov was awarded the Gold Star of the [[Hero of the Soviet Union]] on June 27, 1945.<ref>Gorbachevsky, p. 335</ref><ref>{{Ruheroes|name=Pyotr Berestov|id=8010}}</ref>

== Into Germany ==
In August, the division received a team of 23 women snipers from the Higher Sniper's School in Moscow. The survivors of this team continued to serve in the division until it was disbanded, although some members were detached to other divisions as conditions warranted.<ref>Gorbachevsky, pp. 336–44, 368</ref>

In the [[Vistula-Oder Offensive]] in January, 1945, the 3rd Belorussian Front was tasked with driving into [[East Prussia]] from the east. The 31st Army was not on an assault sector, and in the first stages it was ordered to hold its positions stubbornly. By January 23–24, the 331st had joined in pursuit of the retreating German forces, and along with its corps helped capture the important road junction of Blenkheim, and the strongpoint of [[Treuburg]] on the approaches to [[Giżycko|Lötzen]]. However, in the first week of February, the German command had regrouped their [[129th Infantry Division (Wehrmacht)|129th]] and [[558th Infantry Division (Wehrmacht)|558th Infantry Divisions]] as well as units of the [[24th Panzer Division]], which counterattacked in an attempt to encircle units of the 71st Rifle Corps; after several days fighting the attacks were beaten back and the advance resumed towards Kanditten.<ref>Soviet General Staff, ''Prelude to Berlin'', ed. and trans. Richard W. Harrison, Helion & Co., Ltd., Solihull, UK, 2016, pp. 124, 227, 233</ref>

Following this, the 31st Army drove towards the [[Baltic Sea|Baltic]] coast. The 331st captured the rail junction of [[Górowo Iławeckie|Landsberg]] from the march, and Berestov ordered the division northward, unaware that it was coming into the path of a powerful German counterstrike to hold open a land link to the rest of Germany. Under pressure, the 331st fell back to the outskirts of Landsberg, and savage fighting ensued. Meanwhile, the German columns led by [[Waffen SS]] troops bypassed the town to the south and north, leaving the fighting elements of the division encircled for two days until they were relieved from outside. Meanwhile, the southern German column had rampaged through the mostly-defenseless rear elements of the division, destroying the medical-sanitation battalion, the motor pool, and other units; in total more than 300 men and women were killed and more wounded.<ref>Gorbachevsky, pp. 365–69. Note that Gorbachevsky states this Landsberg is the present-day [[Gorzów Wielkopolski]], but that city is a good deal farther west, on the [[Warta River]].</ref>

Beginning at noon on April 6, following a powerful artillery preparation, the 3rd Belorussian Front, including the 331st, began its assault on the fortifications of [[Battle of Königsberg#Assault|Königsberg]]. By the end of the first day, the outer line of defenses had been penetrated, and 102 city blocks had been cleared, and on April 12, the garrison surrendered. Meanwhile, during all the fighting in East Prussia, the division had not been receiving any replacements, so by this time was operating with a far fewer troops than officially authorized, with "regiments" of about 400 infantry, backed by mortars, the artillery regiment, and air support. Upon finally reaching the Baltic several days later, thousands of German soldiers surrendered to just dozens of men of the 331st.<ref>Gorbachevsky, pp. 377–78, 381–85</ref>

In late April, the division loaded onto troop trains for a trip to the [[Sudetenland]] of [[Czechoslovakia]] along with the rest of the 31st Army, which was transferring to the [[1st Ukrainian Front]] for the final push to Prague. It disembarked in [[Saxony]] on the night of May 6. The army's role was as a flank guard on the left of the 1st Ukrainian Front, protecting the northern prong of the attack on the Czech capital. Advancing against little organized resistance, the 331st was on the outskirts of the town of Schlewitz when it got word of the German surrender.<ref>Gorbachevsky, pp. 386–89</ref>

When the shooting stopped, the men and women of the division had earned the title of the 331st Rifle, Proletarian, Bryansk-Smolensk, twice [[Order of the Red Banner]], Order of Suvorov Division. (Russian: 331-я стрелковая Пролетарская Брянско-Смоленская дважды Краснознамённая ордена Суворова дивизия).<ref>Sharp, ''"Red Tide"'', p. 79</ref>

==Postwar==
According to ''STAVKA'' Order No. 11096 of May 29, 1945, part 8, the 331st is listed as one of the rifle divisions to be "disbanded in place".<ref>{{cite web|url=http://militera.lib.ru/docs/da/berlin_45/16.html |title=чпеообс мйфетбфхтб -[ рЕТЧПЙУФПЮОЙЛЙ &#93;- тХУУЛЙК БТИЙЧ: чЕМЙЛБС пФЕЮЕУФЧЕООБС. ф. 15 (4-5). вЙФЧБ ЪБ вЕТМЙО (лТБУОБС бТНЙС Ч РПЧЕТЦЕООПК зЕТНБОЙЙ) |website=Militera.lib.ru |date= |accessdate=2016-11-04}}</ref><ref>Feskov et al. 2013, pp. 413{{En dash}}414</ref> It was disbanded at Friedeberg, [[Silesia]], in accordance with the directive between July 10–15, 1945.<ref>Gorbachevsky, pp. 423–24</ref>

==Notes==
{{Reflist|30em}}

==References==
* {{Cite book|title=Вооруженные силы СССР после Второй Мировой войны: от Красной Армии к Советской|last=Feskov|first=V.I.|last2=Golikov|first2=V.I.|last3=Kalashnikov|first3=K.A.|last4=Slugin|first4=S.A.|publisher=Scientific and Technical Literature Publishing|year=2013|isbn=9785895035306|location=Tomsk|language=Russian|trans-title=The Armed Forces of the USSR after World War II: From the Red Army to the Soviet: Part 1 Land Forces|ref={{sfnRef|Feskov et al.|2013}}}}

==External links==
*[http://www.generals.dk/general/Korol/Fedor_Petrovich/Soviet_Union.html Fedor Petrovich Korol]
*[http://www.generals.dk/general/Berestov/Petr_Filippovich/Soviet_Union.html Pyotr Filippovich Berestov]

{{Soviet Union divisions before 1945|state=collapsed}}

[[Category:Infantry divisions of the Soviet Union in World War II|331]]
[[Category:Military units and formations established in 1941]]
[[Category:Military units and formations disestablished in 1945]]
[[Category:Military units and formations awarded the Order of the Red Banner]]