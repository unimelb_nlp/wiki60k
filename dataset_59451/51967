{{Infobox journal
| title = Journal of Multilingual and Multicultural Development
| cover =
| editor = [[John Robert Edwards]]
| discipline = [[Applied Linguistics]], [[Sociolinguistics]]
| former_names =
| abbreviation = J. Multiling. Multicult. Dev.
| publisher = [[Routledge]]
| country =
| frequency = 7/year
| history = 1980-present
| openaccess = [[Hybrid open access]]
| license =
| impact = 0.403
| impact-year = 2012
| website = http://www.tandfonline.com/action/journalInformation?journalCode=rmmm20
| link1 = http://www.tandfonline.com/toc/rmmm20/current
| link1-name = Online access
| link2 = http://www.tandfonline.com/loi/rmmm20
| link2-name = Online archive
| JSTOR =
| OCLC = 848205886
| LCCN = 81643161
| CODEN =
| ISSN = 0143-4632
| eISSN = 1747-7557
}}
The '''''Journal of Multilingual and Multicultural Development''''' is a [[peer-reviewed]] [[academic journal]] covering the study of topics in the [[sociology]] and [[social psychology (sociology)|social psychology]] of [[language]], in language and cultural politics, policy, planning, and practice. The [[editor-in-chief]] is [[John Robert Edwards]]. It was established in 1980 and is published in 7 issues per year by [[Routledge]].

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Linguistics and Language Behavior Abstracts]], [[MLA International Bibliography]], and [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 0.403.<ref name=WoS>{{cite book |year=2013 |chapter=Journal of Multilingual and Multicultural Development |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official|1=http://www.tandfonline.com/action/journalInformation?journalCode=rmmm20}}


[[Category:Taylor & Francis academic journals]]
[[Category:Linguistics journals]]
[[Category:Publications established in 1980]]
[[Category:English-language journals]]
[[Category:Bimonthly journals]]
[[Category:Multiculturalism]]