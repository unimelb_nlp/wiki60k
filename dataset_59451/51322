{{Infobox journal
| title = Feminist Africa
| cover = [[File:Cover of Feminist Africa, issue 11 (2008).jpg]]
| editor = [[Amina Mama]]
| discipline = [[gender studies]], [[African studies]]
| former_names = 
| abbreviation = Fem. Afr.
| publisher = [[African Gender Institute]], [[University of Cape Town]]
| country = South Africa
| frequency = Once or twice a year
| history = 2002–present
| openaccess = Yes
| license = [[Creative Commons license|Creative Commons Attribution-Share Alike 2.5]]
| impact = 
| impact-year = 
| website = http://agi.ac.za/journals
| link1 = 
| link1-name = 
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 53869360
| LCCN = 
| CODEN = 
| ISSN = 1726-4596
| eISSN = 
}}
'''''Feminist Africa''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that addresses [[feminism|feminist]] topics from an "African continental perspective".<ref>{{cite journal |last=Groves |first=Sharon |year=2003 |title=News and Views| journal=[[Feminist Studies]] |volume=29 |issue=3 |pages=673–675 |jstor=3178734}}</ref> It is published by the [[African Gender Institute]] ([[University of Cape Town]]).<ref>{{cite journal |last=Guy-Sheftall |first=Beverly |year=2003 |title=African Feminist Discourse: A Review Essay |journal=Agenda |volume=58 |pages=31–36 |jstor=4548092}}</ref> Its founding [[editor-in-chief]] is [[Amina Mama]] ([[Mills College]] and [[University of California, Davis]]).<ref>"[https://archive.is/20130415233111/http://asci.researchhub.ssrc.org/amina-mama/person_view Amina Mama]" on SSRC (Social Science Research Council), accessed 24 October 2012.</ref> It was accredited in 2005 by the South African [[Department of Education (South Africa)|Department of Education]].<ref name="Gray2009">{{cite web|last1=Gray |first1=Eve |last2=Willmers |first2=Michelle |title=Case Study 2: Feminist Africa |url=http://www.cet.uct.ac.za/files/file/OS%20CaseStudy2%20_%20Feminist%20Africa.pdf |publisher=OpeningScholarship Project |accessdate=31 October 2012 |date=February 2009 }}{{dead link|date=December 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> This allows authors publishing in the journal to collect publication subsidy.<ref name="Gray2009"/> The journal is primarily online but also distributes a small number of print copies.<ref name="Gray2009"/>

==Founding==
According to Mama, the journal was created partly in response to a bias in existing scholarship towards the "[[Women In Development]]" (WID) perspective. Particular topics covered by the journal include: women's activism, sexism in higher education, [[militarism]] and [[peace]], and [[Violence against women|gender-related violence]].<ref name="Mama 2011" /><ref>Sharon Groves, "News and Views", ''Feminist Studies'' 29(3), Fall 2003; accessed [http://search.proquest.com/docview/233178888 via ProQuest].</ref> Patricia van der Spuy and Lindsay Clowes write that the publication of the journal marked an important step in the development of [[South African feminism]].<ref>{{cite journal |last=Van der Spuy |first=Patricia |author2=Lindsay Clowes|year=2007 |title=Accidental Feminists? Recent Histories of South African Women |journal=[[Kronos: A Journal of Interdisciplinary Synthesis]] |volume=33 |pages=211–235 |jstor=41056589}}</ref> [[Iris Berger]] has critiqued the journal (as an indicator of contemporary African feminism in general) for leaving out colonial and precolonial African women's history.<ref>Iris Berger, "[http://www-bcf.usc.edu/~judithb/documents/JMBHistMattersForumBergerComment.pdf Feminism, Patriarchy, and African Women’s History]", ''Journal of Women's History'' 20(2), Summer 2008, doi: [http://dx.doi.org/10.1353%2Fjowh.0.0021 10.1353/jowh.0.0004].</ref>

''Feminist Africa'' is the first "continental" African [[gender studies]] journal.<ref name="Mama 2011">[[Amina Mama]], "[http://www.palgrave-journals.com/fr/conf-proceedings/n1s/full/fr201122a.html what does it mean to do feminist research in African contexts]?", Feminist Theory & Activism in Global Perspective: ''feminist review'' conference proceedings, 2011, DOI: 0141-7789/11.</ref><ref>"[http://newsblaze.com/story/20080208153737tsop.np/topstory.html International Feminist Scholar Teams With U.S. Congresswoman Lee]", ''NewsBlaze'', 8 February 2008.</ref> The journal publishes works by African scholars in America and discusses the situation of intellectuals across the [[African diaspora]].<ref>Karen MacGregor, "[http://www.timeshighereducation.co.uk/story.asp?storyCode=197041&sectioncode=26 Out of Africa]", ''Times Higher Education'', 1 July 2005.</ref><ref>See ''Feminist Africa'' 7, December 2007, particularly the editorial introduction by Rhoda Reddock: "The journal ''Feminist Africa'' has come an important voice for feminists and scholars within the continent, making a space for continental voices in a world dominated by voices from the North including those of diasporic women. The publication of this issue from within the continent and edited by a woman from the economic South is an important development which opens up new possibilities for South-South collaboration and debate within the African diaspora. But the diasporic experience is not limited to those of African descent and must include all those who share and inhabit these diasporic spaces. Many parts of the world are today becoming spaces of inter-locking diasporic communities for example from Asia, Africa, China, Europe and even the Middle East" (pp. 4-5).</ref> These international contributors have raised the journal's profile but barred it from receiving Department of Education subsidies.<ref name="Gray2009"/> ''Feminist Africa'' does not receive funds from the University of Cape Town (although it is edited by salaried UCT workers) and relies on sponsorship by international donors—particularly the [[Ford Foundation]] and [[Hivos]].<ref name="Gray2009"/><ref>"Acknowledgement of Funders", [http://agi.ac.za/journals/ ''Feminist Africa'' website], accessed 26 October 2012.</ref>

== See also ==
* [[African studies]]
* ''[[African Identities]]''
* [[Agenda (feminist journal)|''Agenda'']]
* ''[[Journal of African Cultural Studies]]''
* ''[[Journal of Southern African Studies]]''
* [[List of African studies journals]]
*[[Feminism in South Africa]]
{{Portal|Africa|Feminism|South Africa}}

== References ==
{{reflist|30em}}

== External links ==
* {{Official website|http://agi.ac.za/journals}}

[[Category:Open access journals]]
[[Category:Gender studies journals]]
[[Category:African studies journals]]
[[Category:Feminism in South Africa]]
[[Category:Publications established in 2002]]
[[Category:English-language journals]]
[[Category:Creative Commons-licensed journals]]
[[Category:Feminist journals]]
[[Category:2002 establishments in South Africa]]
[[Category:Academic journals published by universities and colleges]]