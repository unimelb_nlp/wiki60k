{{Other ships|USS Alaska}}
{{Use dmy dates|date=October 2012}}
{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=[[File:USS Alaska (CB-1) off the Philadelphia Navy Yard on 30 July 1944.jpg|300px|USS Alaska]]
|Ship caption=
}}
{{Infobox ship career
|Hide header=
|Ship country=United States
|Ship flag={{USN flag|1944}}
|Ship name=USS ''Alaska''
|Ship namesake=Territory of [[Alaska]]
|Ship ordered=9 September 1940
|Ship awarded=
|Ship builder=[[New York Shipbuilding Corporation]]
|Ship original cost=
|Ship laid down=17 December 1941
|Ship launched=15 August 1943
|Ship commissioned=17 June 1944
|Ship recommissioned=
|Ship decommissioned=17 February 1947
|Ship struck=1 June 1960
|Ship identification=
|Ship motto=
|Ship nickname=
|Ship honors=Three [[battle star]]s for World War II service
|Ship fate=Scrapped in 1960
|Ship notes=
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship class={{sclass-|Alaska|cruiser|0}} [[Alaska-class cruiser#.22Large cruisers.22 or .22battlecruisers.22|large cruiser]]
|Ship displacement=*Standard: {{convert|29779|LT|MT}}
*Full load: {{convert|34253|LT|MT}}
|Ship length={{convert|808|ft|6|in|m|abbr=on|1}}
|Ship beam={{convert|91|ft|1|in|m|abbr=on|1}}
|Ship draft={{convert|31|ft|10|in|m|abbr=on|1}}
|Ship propulsion=*General Electric [[steam turbine]]s
*8 Babcock & Wilcox boilers
*4 shafts
|Ship speed= {{convert|33|kn|lk=in}}
|Ship range={{convert|12000|nmi|abbr=on|lk=in}} at {{convert|15|kn}}
|Ship power={{convert|153000|shp|abbr=on|lk=in}}
|Ship crew=1,517
|Ship time to activate=
|Ship sensors=
|Ship EW=
|Ship armament=*9 × [[12"/50 caliber Mark 8 gun|12-inch/50 caliber]] (305&nbsp;mm),
*12 × {{convert|5|in|mm|abbr=on|0}} guns
*56 × {{convert|40|mm|in|abbr=on}} guns
*34 × {{convert|20|mm|in|abbr=on}} guns
|Ship armor=*Belt: {{convert|9|in|abbr=on|0}}
*Turrets: {{convert|12.8|in|abbr=on|0}}
*Deck: {{convert|4|in|abbr=on|0}}
|Ship aircraft=4
|Ship aircraft facilities=
|Ship notes=
}}
|}

'''USS ''Alaska'' (CB-1)''' was the [[lead ship]] of the {{sclass-|Alaska|cruiser|4}} of [[Alaska-class cruiser#.22Large cruisers.22 or .22battlecruisers.22|large cruisers]] which served with the [[United States Navy]] during the end of [[World War II]]. She was the first of two ships of her class to be completed, followed only by {{USS|Guam|CB-2|2}}; four other ships were ordered but were not completed before the end of the war. ''Alaska'' was the third vessel of the US Navy to be named after what was then the territory of [[Alaska]]. She was laid down on 17 December 1941, ten days after the outbreak of war, was launched in August 1943 by the [[New York Shipbuilding Corporation]], in [[Camden, New Jersey]], and was commissioned in June 1944. She was armed with a main battery of nine {{convert|12|in|abbr=on}} guns in three triple turrets and had a top speed of {{convert|33|kn|abbr=on}}.

Due to being commissioned late in the war, ''Alaska'' saw relatively limited service. She participated in operations off [[Battle of Iwo Jima|Iwo Jima]] and [[Battle of Okinawa|Okinawa]] in February&ndash;July 1945, including providing anti-aircraft defense for various carrier task forces and conducting limited shore bombardment operations. She shot down several Japanese aircraft off Okinawa, including a possible [[Yokosuka MXY7 Ohka|Ohka]] piloted missile. In July&ndash;August 1945 she participated in sweeps for Japanese shipping in the East China and Yellow Seas. After the end of the war, she assisted in the occupation of Korea and transported a contingent of US Army troops back to the United States. She was decommissioned in February 1947 and placed in reserve, where she remained until she was stricken in 1960 and sold for [[Ship breaking|scrapping]] the following year.

==Design==
{{main|Alaska-class cruiser}}

''Alaska'' was {{convert|808|ft|6|in}} [[length overall|long overall]] and had a [[beam (nautical)|beam]] of {{convert|91|ft|1|in|abbr=on}} and a [[draft (nautical)|draft]] of {{convert|31|ft|10|in|abbr=on}}. She displaced {{convert|29779|LT}} as designed and up to {{convert|34253|LT}} at full combat load. The ship was powered by four-shaft [[General Electric]] geared [[steam turbines]] and eight oil-fired [[Babcock & Wilcox]] [[boiler]]s rated at {{convert|150000|shp|lk=on}}, generating a top speed of {{convert|33|kn|lk=in}}. The ship had a cruising range of {{convert|12000|nmi|lk=in}} at a speed of {{convert|15|kn|abbr=on}}.<ref name=G122>Gardiner & Chesneau, p. 122</ref><ref>Egan, p. 36</ref> She carried four [[OS2U Kingfisher]] or [[SC Seahawk]] seaplanes,<ref>Swanborough and Bowers, 148.</ref> with a pair of steam catapults mounted amidships.<ref>Friedman, p. 303</ref>

The ship was armed with a main battery of nine [[12"/50 caliber Mark 8 gun|12&nbsp;inch L/50 Mark 8]] guns in three triple [[gun turret]]s, two in a [[superfire|superfiring pair]] forward and one aft of the [[superstructure]].<ref group=Note>L/50 refers to the length of the gun in terms of [[Caliber (artillery)|calibers]]. An L/50 gun is 50 times long as it is in bore diameter.</ref> The secondary battery consisted of twelve [[5"/38 caliber gun|5-inch L/38]] [[dual-purpose gun]]s in six twin turrets. Two were placed on the [[centerline (ship marking)|centerline]] superfiring over the main battery turrets, fore and aft, and the remaining four turrets were placed on the corners of the superstructure. The light anti-aircraft battery consisted of 56 quad-mounted [[Bofors 40 mm|{{convert|40|mm|abbr=on}} Bofors]] guns and 34 single-mounted [[Oerlikon 20 mm cannon|{{convert|20|mm|abbr=on}} Oerlikon]] guns.<ref name=G122/> A pair of Mk 34 gun directors aided gun laying for the main battery, while two Mk 37 directors controlled the 5-inch guns and a Mk 57 director aided the 40&nbsp;mm guns.<ref>Friedman, p. 483</ref> The main [[armored belt]] was {{convert|9|in|abbr=on|0}} thick, while the gun turrets had {{convert|12.8|in|abbr=on|0}} thick faces. The main armored deck was {{convert|4|in|abbr=on|0}} thick.<ref name=G122/>

==Service history==
[[File:USS Alaska (CB-1)-3.jpg|thumb|''Alaska'' on her shakedown cruise in August 1944]]
''Alaska'' was authorized under the Fleet Expansion Act on 19 July 1940, and ordered on 9 September.<ref>Friedman, p. 301</ref> On 17 December 1941 she was laid down at [[New York Shipbuilding]] in Camden, New Jersey. She was launched on 15 August 1943, sponsored by the wife of the governor of Alaska, after which [[fitting-out]] work was effected. The ship was completed by June 1944, and was commissioned into the US Navy on 17 June, under the command of Captain Peter K. Fischler.<ref name=DANFS>{{cite web|title=Alaska|url=http://www.history.navy.mil/danfs/a5/alaska-iii.htm|publisher=[[Naval History & Heritage Command]]|accessdate=21 January 2012}}</ref>

After her commissioning, ''Alaska'' steamed down to [[Hampton Roads]], escorted by the destroyers {{USS|Simpson|DD-221|2}} and {{USS|Broome|DD-210|2}}. The ship was then deployed for a [[shakedown cruise]], first in the [[Chesapeake Bay]] and then into the Caribbean, off [[Trinidad]]. On the cruise she was escorted by the destroyers {{USS|Bainbridge|DD-246|2}} and {{USS|Decatur|DD-341|2}}. After completing the cruise ''Alaska'' returned to the Philadelphia Navy Yard for some minor alterations, including the installation of four Mk 57 fire control directors for her 5-inch guns. On 12 November she left Philadelphia in the company of the destroyer-minelayer {{USS|Thomas E. Fraser|DM-24|2}}, bound for two weeks of [[sea trials]] off [[Guantanamo Bay]], Cuba. On 2 December she left Cuba for the Pacific, transiting the [[Panama Canal]] two days later, and reaching [[San Diego]] on 12 December. There her gun crews trained for shore bombardment and anti-aircraft fire.<ref name=DANFS/>

===Pacific deployment===
On 8 January 1945, ''Alaska'' left California for Hawaii, arriving in [[Pearl Harbor]] on 13 January. There she participated in further training and was assigned to Task Group 12.2, which departed for [[Ulithi]] on 29 January. The Task Group reached Ulithi on 6 February and was merged into Task Group 58.5, part of Task Force 58, the [[Fast Carrier Task Force]]. Task Group 58.5 was assigned to provide anti-aircraft defense for the [[aircraft carrier]]s; ''Alaska'' was assigned to the carriers {{USS|Enterprise|CV-6|2}} and {{USS|Saratoga|CV-3|2}}. The fleet sailed for Japan on 10 February to conduct air strikes against Tokyo and the surrounding airfields. The Japanese did not attack the fleet during the operation. ''Alaska'' was then transferred to Task Group 58.4 and assigned to support the assault on [[Iwo Jima]]. She served in the screen for the carriers off Iwo Jima for nineteen days, after which time she had to return to Ulithi to replenish fuel and supplies.<ref name=DANFS/>

''Alaska'' remained with TG 58.4 for the [[Battle of Okinawa]]. She was assigned to screen the carriers {{USS|Yorktown|CV-10|2}} and {{USS|Intrepid|CV-11|2}}; the fleet left Ulithi on 14 March and reached its operational area southeast of [[Kyushu]] four days later. The first air strikes on Okinawa began that day, and claimed 17 Japanese aircraft destroyed on the ground. Here, ''Alaska'' finally saw combat, as the Japanese launched a major air strike on the American fleet. Her anti-aircraft gunners destroyed a [[Yokosuka P1Y]] bomber [[kamikaze|attempting to crash]] into ''Intrepid''. Shortly thereafter, ''Alaska'' was warned that American aircraft were in the vicinity. About ten minutes later, her gunners spotted an unidentified aircraft, approaching in what they thought was a threatening manner; they shot down what turned out to be an [[F6F Hellcat]] fighter, though the pilot was uninjured. Later that afternoon, ''Alaska'' shot down a second Japanese bomber, a [[Yokosuka D4Y]].<ref name=DANFS/>

[[File:Alaska (CB-1).jpg|thumb|left|''Alaska'' underway]]
The following day, the carrier {{USS|Franklin|CV-13|2}} was badly damaged by several bomb hits and a kamikaze. ''Alaska'' and her sister {{USS|Guam|CB-2|2}}, two other cruisers, and several destroyers were detached to create Task Group 58.2.9 to escort the crippled ''Franklin'' back to Ulithi. On the voyage back to port, another D4Y bomber attacked ''Franklin'', though the ships were unable to shoot it down. Gunfire from one of the 5-inch guns accidentally caused flash burns on several men standing nearby; these were the only casualties suffered by her crew during the war. ''Alaska'' then took on the role of fighter director; using her anti-air search radar she vectored fighters to intercept and destroy a [[Kawasaki Ki-45]] heavy fighter. On 22 March, the ships reached Ulithi and ''Alaska'' was detached to rejoin TG 58.4.<ref name=DANFS/>

After returning to her unit, ''Alaska'' continued to screen for the aircraft carriers off Okinawa. On 27 March she was detached to conduct a bombardment of [[Minamidaitō]]. She was joined by ''Guam'', two light cruisers, and Destroyer Squadron 47. On the night of 27&ndash;28 March, she fired forty-five 12-inch shells and three hundred and fifty-two 5-inch rounds at the island. The ships rejoined TG 58.4 at a refueling point, after which they returned to Okinawa to support the landings when they began on 1 April. On the evening of 11 April, ''Alaska'' shot down one Japanese plane, assisted in the destruction of another, and claimed what might have been an [[Yokosuka MXY7 Ohka|Ohka]] piloted rocket-bomb. On 16 April, the ship shot down another three aircraft and assisted with three others. Throughout the rest of the month, her heavy anti-aircraft fire succeeded in driving off Japanese bombers.<ref name=DANFS/>

''Alaska'' then returned to Ulithi to resupply, arriving on 14 May. She was then assigned to TG 38.4, the reorganized carrier task force. The fleet then returned to Okinawa, where ''Alaska'' continued in her anti-aircraft defense role. On 9 June, she and ''Guam'' bombarded [[Oki Daitō]]. TG 38.4 then steamed to [[San Pedro Bay, Philippines|San Pedro Bay]] in the [[Leyte Gulf]] for rest and maintenance; the ship remained there from 13 June until 13 July, when she was assigned to Cruiser Task Force 95 along with her sister ''Guam'', under the command of Rear Admiral [[Francis S. Low]].<ref>Cressman, p. 339</ref> On 16 July, ''Alaska'' and ''Guam'' conducted a sweep into the [[East China Sea|East China]] and [[Yellow Sea]]s to sink Japanese shipping vessels. They had only limited success, however, and returned to the fleet on 23 July. They then joined a major raid, which included three battleships and three [[escort carrier]]s, into the estuary of the [[Yangtze River]] off Shanghai. Again, the operation met with limited success.<ref>Rohwer, p. 423</ref>

===Post-war operations===
[[File:Reserve fleet, Bayonne.jpg|thumb|Reserve fleet in Bayonne; the two large ships at right are ''Alaska'' and ''Guam''.]]
In the course of her service during World War II, ''Alaska'' was awarded three [[battle star]]s. On 30 August ''Alaska'' left Okinawa for Japan to participate in the 7th Fleet occupation force. She arrived in [[Incheon]], Korea on 8 September and supported Army operations there until 26 September, when she left for [[Tsingtao]], China, arriving the following day. There, she supported the [[6th Marine Division (United States)|6th Marine Division]] until 13 November, when she returned to Incheon to take on Army soldiers as part of [[Operation Magic Carpet]], the mass repatriation of millions of American servicemen from Asia and Europe. ''Alaska'' left Incheon with a contingent of soldiers bound for San Francisco. After reaching San Francisco, she left for the Atlantic, via the Panama Canal, which she transited on 13 December. The ship arrived in the [[Boston Navy Yard]] on 18 December, where preparations were made to place the ship in reserve. She left Boston on 1 February 1946 for [[Bayonne, NJ|Bayonne]], New Jersey, where she would be berthed in reserve. She arrived there the following day, and on 13 August, she was removed from active service, though she would not be decommissioned until 17 February 1947.<ref name=DANFS/>

===Conversion proposals===
In 1958, the [[Bureau of Ships]] prepared two feasibility studies to see if ''Alaska'' and ''Guam'' were suitable to be converted to guided missile cruisers. The first study involved removing all of the guns in favor of four different missile systems. At $160 million this was seen as too costly, so a second study was conducted. This study left the forward batteries—the two 12″ triple turrets and three of the 5" dual turrets—in place and added a reduced version of the first plan for the aft. This would have cost $82 million, and was still seen as too cost-prohibitive.<ref name= Dulin187>Garzke & Dulin, p. 187</ref> As a result, the conversion proposal was abandoned and the ship was instead stricken from the naval registry on 1 June 1960. On 30 June she was sold to the Lipsett Division of Luria Brothers to be broken up for scrap.<ref name=DANFS/>

==Awards==
*[[Asiatic-Pacific Campaign Medal]] with three [[battle star]]s
*[[World War II Victory Medal]]
*[[Navy Occupation Medal]]
*[[China Service Medal]]

==Footnotes==
{{reflist|group=Note}}

==Citations==
{{Reflist|colwidth=30em}}

==References==
* {{cite book|last=Cressman|first=Robert|title=The Official Chronology of the U.S. Navy in World War II|publisher=US Naval Institute Press|location=Annapolis, MD|year=2000|isbn=1-55750-149-1}}
* {{cite book|author1=Gardiner, Robert |author2=Chesneau, Roger |title=Conway's All the World's Fighting Ships, 1922–1946|location=Annapolis, MD|publisher=Naval Institute Press|year=1980|isbn=0-87021-913-8}}
* {{cite book|author1=Dulin, Robert O. Jr. |author2=Garzke, William H. Jr. |title=Battleships: United States Battleships in World War II |location=Annapolis, MD|publisher=Naval Institute Press|year=1976|isbn=1-55750-174-2}}
* {{Cite journal|last=Egan|first=Robert S.|date=March 1971|title=The US Navy's Battlecruisers|journal=Warship International |volume=VIII| issue =  1|pages=28–51|publisher=[[International Naval Research Organization]]}}
* {{cite book|last=Friedman|first=Norman|title=U.S. Cruisers: An Illustrated Design History|publisher=US Naval Institute Press|location=Annapolis, MD|year=1984|isbn=0-87021-718-6}}
* {{cite book|last=Rohwer|first=Jürgen|authorlink=Jürgen Rohwer|title=Chronology of the War at Sea, 1939–1945: The Naval History of World War Two|publisher=US Naval Institute Press|location=Annapolis|year=2005|isbn=1-59114-119-2}}
* {{cite book|author1=Swanborough, Gordon |author2=Bowers, Peter M. | title=United States Navy Aircraft Since 1911 |publisher=Funk & Wagnalls |year=1968 |id=<!--no ISBN-->}}

== External links ==
{{Commons category|USS Alaska (CB-1)}}
* [http://www.ussalaskacb-1.com/ USS Alaska (CB-1) Website] Information for and about veterans that served on the USS Alaska CB-1 during World War II.

{{Alaska class large cruiser}}

{{good article}}

{{DEFAULTSORT:Alaska (Cb-1)}}
[[Category:Alaska-class cruisers]]
[[Category:World War II cruisers of the United States]]
[[Category:Ships built in Camden, New Jersey]]
[[Category:1943 ships]]
[[Category:United States Navy Alaska-related ships]]