{{Use dmy dates|date=March 2017}}
{| {{Infobox ship begin}}
{{Infobox ship image
|Ship image   = [[File:Bundesarchiv DVM 10 Bild-23-63-06, Panzerschiff "Admiral Graf Spee".jpg|300px]]
|Ship caption = ''Admiral Graf Spee'' in 1936
}}
{{Infobox ship career
|Hide header       = 
|Ship country = [[Nazi Germany]]
|Ship flag = {{shipboxflag|Nazi Germany|naval}}
|Ship name         = ''Admiral Graf Spee''
|Ship namesake     = [[Maximilian von Spee]]
|Ship ordered      = 
|Ship homeport     = 
|Ship builder      = [[Kriegsmarinewerft Wilhelmshaven|''Reichsmarinewerft'']], [[Wilhelmshaven]]
|Ship laid down    = 1 October 1932
|Ship launched     = 30 June 1934
|Ship commissioned = 6 January 1936
|Ship fate         = Scuttled, 17 December 1939
}}
{{Infobox ship characteristics
|Hide header = 
|Header caption = 
|Ship class = {{sclass-|Deutschland|cruiser|3}}
|Ship displacement =
* {{convert|14890|MT|abbr=on}} (design)
* {{convert|16020|LT|abbr=on}} (full load)
|Ship length = {{convert|186|m|ftin|abbr=on}}
|Ship beam   = {{convert|21.65|m|ftin|abbr=on}}
|Ship draft  = {{convert|7.34|m|ftin|abbr=on}}
|Ship power = {{convert|52050|bhp|lk=in|abbr=on}}
|Ship propulsion =2 propellers; 8 × [[diesel engine]]s
|Ship speed = {{convert|28.5|kn|lk=in}}
|Ship range = {{convert|16300|nmi|lk=in}} at {{convert|18.69|kn}}
|Ship complement =
*As built:
**  33 officers
** 586 enlisted
*After 1935:
**  30 officers
** 921–1,040 enlisted
| Ship sensors =
*1939:
** FMG 39 G(gO)
| Ship armament =
*As built:
** 6 × [[28 cm SK C/28 naval gun|{{convert|28|cm|in|abbr=on}}]] in triple turrets
** 8 × [[15 cm SK C/28|{{convert|15|cm|abbr=on}}]] in single turrets
** 8 × {{convert|53.3|cm|abbr=on}} [[torpedo tube]]s
| Ship armor =
* Main [[turret]]s: {{convert|140|mm|in|abbr=on}}
* Belt: {{convert|80|mm|in|abbr=on}}
* Main deck: {{convert|17|-|45|mm|in|abbr=on}}
|Ship aircraft = 2 × [[Arado Ar 196]] [[floatplane]]s
|Ship aircraft facilities = 1 × [[aircraft catapult|catapult]]
|Ship notes = 
}}
|}

'''''Admiral Graf Spee''''' was a {{sclass-|Deutschland|cruiser|0}} "''Panzerschiff''" (armored ship), nicknamed a "pocket battleship" by the British, which served with the [[Kriegsmarine]] of [[Nazi Germany]] during [[World War II]]. The two sister-ships of her class, {{ship|German cruiser|Deutschland||2}} and {{ship|German cruiser|Admiral Scheer||2}}, were reclassified as [[heavy cruisers]] in 1940. The vessel was named after Admiral [[Maximilian von Spee]], commander of the [[German East Asia Squadron|East Asia Squadron]] that fought the battles of [[Battle of Coronel|Coronel]] and the [[Battle of the Falkland Islands|Falkland Islands]], where he was killed in action, in [[World War I]]. She was laid down at the [[Kriegsmarinewerft Wilhelmshaven|''Reichsmarinewerft'']] shipyard in [[Wilhelmshaven]] in October 1932 and completed by January 1936. The ship was nominally under the {{convert|10000|LT}} limitation on warship size imposed by the [[Treaty of Versailles]], though with a [[Full-load displacement|full load displacement]] of {{convert|16020|LT}}, she significantly exceeded it. Armed with six {{convert|28|cm|abbr=on}} guns in two triple [[gun turret]]s, ''Admiral Graf Spee'' and her sisters were designed to outgun any cruiser fast enough to catch them. Their top speed of {{convert|28|kn}} left only the few [[battlecruisers]] in the Anglo-French navies fast enough and powerful enough to sink them.{{sfn|Pope|p=7}}

The ship conducted five [[Non-intervention in the Spanish Civil War|non-intervention patrols]] during the [[Spanish Civil War]] in 1936–1938, and participated in the [[Coronation Review]] of [[King George VI]] in May 1937. ''Admiral Graf Spee'' was deployed to the South Atlantic in the weeks before the outbreak of World War II, to be positioned in merchant sea lanes once war was declared. Between September and December 1939, the ship sank nine ships totaling {{GRT|50089|disp=long}}, before being confronted by three British cruisers at the [[Battle of the River Plate]] on 13 December. ''Admiral Graf Spee'' inflicted heavy damage on the British ships, but she too was damaged, and was forced to put into port at [[Montevideo]]. Convinced by false reports of superior British naval forces approaching his ship, [[Hans Langsdorff]], the commander of the ship, ordered the vessel to be [[scuttling|scuttled]]. The ship was partially broken up ''[[in situ]]'', though part of the ship remains visible above the surface of the water.

==Design==
{{main|Deutschland-class cruiser}}
[[File:Admiral Scheer ONI.jpg|thumb|left|Recognition drawing of a ''Deutschland''-class cruiser]]

''Admiral Graf Spee'' was {{convert|186|m|ft|sp=us}} [[length overall|long overall]] and had a beam of {{convert|21.65|m|ft|abbr=on}} and a maximum draft of {{convert|7.34|m|ft|abbr=on}}. The ship had a design displacement of {{convert|14890|MT|abbr=on}} and a full load displacement of {{convert|16020|LT|abbr=on}},{{sfn|Gröner|p=60}} though the ship was officially stated to be within the {{convert|10000|LT|abbr=on}} limit of the [[Treaty of Versailles]].{{sfn|Pope|p=3}} ''Admiral Graf Spee'' was powered by four sets of [[MAN SE|MAN]] 9-cylinder double-acting two-stroke diesel engines.{{sfn|Gröner|p=60}} The ship's top speed was {{convert|28.5|kn|lk=in}}, at {{convert|54000|shp|lk=in}}. At a cruising speed of {{convert|18.69|kn}}, the ship had a range of {{convert|16300|nmi|lk=in}}.{{sfn|Koop & Schmolke|p=33&ndash;34}} As designed, her standard complement consisted of 33 officers and 586 enlisted men, though after 1935 this was significantly increased to 30 officers and 921–1,040 sailors.{{sfn|Gröner|p=60}}

''Admiral Graf Spee''{{'}}s primary armament was six [[28 cm SK C/28 naval gun|{{convert|28|cm|in|abbr=on|1}} SK C/28]] guns mounted in two triple [[gun turret]]s, one forward and one aft of the [[superstructure]]. The ship carried a secondary battery of eight [[15 cm SK C/28|{{convert|15|cm|abbr=on}} SK C/28]] guns in single turrets grouped [[amidships]]. Her anti-aircraft battery originally consisted of three {{convert|8.8|cm|abbr=on}} L/45 guns, though in 1935 these were replaced with six [[8.8 cm SK C/31 naval gun|8.8&nbsp;cm L/78]] guns. In 1938, the 8.8&nbsp;cm guns were removed, and six {{convert|10.5|cm|abbr=on}} L/65 guns, four {{convert|3.7|cm|abbr=on}} guns, and ten {{convert|2|cm|abbr=on}} guns were installed in their place.{{sfn|Gröner|p=60}} The ship also carried a pair of quadruple {{convert|53.3|cm|abbr=on}} deck-mounted torpedo launchers placed on her stern. The ship was equipped with two [[Arado Ar 196]] seaplanes and one catapult. ''Admiral Graf Spee''{{'}}s [[belt armor|armored belt]] was {{convert|60|to|80|mm|abbr=on}} thick; her upper deck was {{convert|17|mm|abbr=on}} thick while the main armored deck was {{convert|17|to|45|mm|abbr=on}} thick. The main battery turrets had {{convert|140|mm|abbr=on}} thick faces and 80&nbsp;mm thick sides.{{sfn|Gröner|p=60}} Radar consisted of a [[Seetakt radar|FMG G(gO) "Seetakt" set]];{{sfn|Gröner|p=61}}{{efn|name=radar}} ''Admiral Graf Spee'' was the first German warship to be equipped with radar equipment.{{sfn|Bidlingmaier|p=74}}

==Service history==
[[File:Graf Spee at Spithead.jpg|thumb|left|''Admiral Graf Spee'' at Spithead in 1937; {{HMS|Hood|51|6}} and {{HMS|Resolution|09|2}} lie in the background]]

''Admiral Graf Spee'' was ordered by the Reichsmarine from the [[Kriegsmarinewerft Wilhelmshaven|''Reichsmarinewerft'']] shipyard in [[Wilhelmshaven]].{{sfn|Gröner|p=60}} Ordered as ''Ersatz Braunschweig'', ''Admiral Graf Spee'' replaced the reserve battleship {{SMS|Braunschweig||2}}. Her keel was laid on 1 October 1932,{{sfn|Gardiner & Chesneau|p=227}} under construction number 125.{{sfn|Gröner|p=60}} The ship was launched on 30 June 1934; at her launching, she was christened by the daughter of Admiral [[Maximilian von Spee]], the ship's namesake.{{sfn|Williamson|p=39}} She was completed slightly over a year and a half later on 6 January 1936, the day she was commissioned into the German fleet.{{sfn|Gröner|p=62}}

''Admiral Graf Spee'' spent the first three months of her career conducting extensive [[sea trial]]s to ready the ship for service. The ship's first commander was ''[[Kapitän zur See]]'' (''KzS'') Conrad Patzig; he was replaced in 1937 by ''KzS'' [[Walter Warzecha]].{{sfn|Williamson|p=39}} After joining the fleet, ''Admiral Graf Spee'' became the flagship of the German Navy.{{sfn|Bidlingmaier|p=73}} In the summer of 1936, following the outbreak of the [[Spanish Civil War]], she deployed to the Atlantic to participate in [[Non-intervention in the Spanish Civil War|non-intervention patrols]] off the [[Republican faction (Spanish Civil War)|Republican]]-held coast of Spain. Between August 1936 and May 1937, the ship conducted three patrols off Spain.{{sfn|Williamson|p=40}} On the return voyage from Spain, ''Admiral Graf Spee'' stopped in Great Britain to represent Germany in the [[Coronation Review]] at [[Spithead]] for [[King George VI]] on 20 May.{{sfn|Bidlingmaier|p=73}}

After the conclusion of the Review, ''Admiral Graf Spee'' returned to Spain for a fourth non-intervention patrol. Following fleet manoeuvres and a brief visit to Sweden, the ship conducted a fifth and final patrol in February 1938.{{sfn|Williamson|p=40}} In 1938, ''KzS'' [[Hans Langsdorff]] took command of the vessel;{{sfn|Williamson|p=39}} she conducted a series of goodwill visits to various foreign ports throughout the year.{{sfn|Williamson|p=40}} These included cruises into the Atlantic, where she stopped in [[Tangier]] and [[Vigo]].{{sfn|Whitley|p=72}} She also participated in extensive fleet manoeuvres in German waters. She was part of the celebrations for the reintegration of the port of [[Klaipėda|Memel]] into Germany,{{sfn|Williamson|p=40}} and a fleet review in honour of Admiral [[Miklós Horthy]], the Regent of Hungary. Between 18 April and 17 May 1939, she conducted another cruise into the Atlantic, stopping in the ports of [[Ceuta]] and [[Lisbon]].{{sfn|Whitley|p=72}} On 21 August 1939, ''Admiral Graf Spee'' departed [[Wilhelmshaven]], bound for the South Atlantic.{{sfn|Bidlingmaier|p=73}}

=== World War II ===
[[File:Graf Spee Fahrten.jpg|thumb|upright|Map of the cruises of ''Admiral Graf Spee'' and ''Deutschland'']]
Following the outbreak of war between Germany and the [[Allies of World War II|Allies]] in September 1939, [[Adolf Hitler]] ordered the German Navy to begin [[commerce raiding]] against Allied merchant traffic. Hitler nevertheless delayed issuing the order until it became clear that Britain would not countenance a peace treaty following the conquest of Poland. The ''Admiral Graf Spee'' was instructed to strictly adhere to [[prize rules]], which required raiders to stop and search ships for contraband before sinking them, and to ensure that their crews are safely evacuated. Langsdorff was ordered to avoid combat, even with inferior opponents, and to frequently change position.{{sfn|Bidlingmaier|pp=76–77}} On 1 September, the cruiser rendezvoused with her supply ship {{ship|German tanker|Altmark||2}} southwest of the [[Canary Islands]]. While replenishing his fuel supplies, Langsdorff ordered superfluous equipment transferred to the ''Altmark''; this included several of the ship's boats, flammable paint, and two of her ten 2&nbsp;cm anti-aircraft guns, which were installed on the tanker.{{sfn|Bidlingmaier|p=77}}

On 11 September, while still transferring supplies from ''Altmark'', ''Admiral Graf Spee''{{'}}s [[Arado Ar 198|Arado]] [[floatplane]] spotted the British heavy cruiser {{HMS|Cumberland|57|6}} approaching the two German ships. Langsdorff ordered both vessels to depart at high speed, successfully evading the British cruiser.{{sfn|Bidlingmaier|p=77}} On 26 September, the ship finally received orders authorizing attacks on Allied merchant shipping. Four days later ''Admiral Graf Spee''{{'}}s Arado located [[Alfred Booth and Company|Booth Steam Ship Co's]] cargo ship ''Clement'' off the coast of Brazil. The cargo ship transmitted an "RRR" signal ("I am under attack by a raider") before the cruiser ordered her to stop. ''Admiral Graf Spee'' took ''Clement''{{'}}s [[Sea captain|captain]] and [[chief engineer]] prisoner but let the rest of her crew to abandon ship in the lifeboats.{{sfn|Slader|p=25}} The cruiser then fired 30 rounds from her 28&nbsp;cm and 15&nbsp;cm guns and two torpedoes at the cargo ship, which broke up and sank.{{sfn|Slader|p=26}} Langsdorff ordered a distress signal sent to the naval station in [[Pernambuco]] to ensure the rescue of the ship's crew. The British [[Admiralty]] immediately issued a warning to merchant shipping that a German surface raider was in the area.{{sfn|Bidlingmaier|p=78}} The British crew later reached the Brazilian coast in their lifeboats.{{sfn|Slader|p=25}}

On 5 October, the British and French navies formed eight groups to hunt down ''Admiral Graf Spee'' in the South Atlantic. The British [[aircraft carrier]]s {{HMS|Hermes|95|6}}, {{HMS|Eagle|1918|2}}, and {{HMS|Ark Royal|91|2}}, the {{ship|French aircraft carrier|Béarn}}, the British [[battlecruiser]] {{HMS|Renown|1916|2}}, and French [[battleship]]s {{ship|French battleship|Dunkerque||2}} and {{ship|French battleship|Strasbourg||2}}, and 16 [[cruiser]]s were committed to the hunt.{{sfn|Rohwer|p=6}} Force G, commanded by [[Commodore (Royal Navy)|Commodore]] [[Henry Harwood]] and assigned to the east coast of South America, comprised the cruisers ''Cumberland'' and {{HMS|Exeter|68|2}}. Force G was reinforced by the light cruisers {{HMS|Ajax|22|2}} and {{HMNZS|Achilles|70|2}}; Harwood detached ''Cumberland'' to patrol the area off the [[Falkland Islands]] while his other three cruisers patrolled off the [[Río de la Plata|River Plate]].{{sfn|Jackson|pp=61–63}}

On the same day as the formation of the Anglo-French hunter groups, ''Admiral Graf Spee'' captured the steamer ''Newton Beech''. Two days later, she encountered and sank the merchant ship ''Ashlea''. On 8 October, the following day, she sank ''Newton Beech'',{{sfn|Williamson|pp=40–41}} which Langsdorff had been using to house prisoners.{{sfn|Bidlingmaier|p=79}} ''Newton Beech'' was too slow to keep up with ''Admiral Graf Spee'', and so the prisoners were transferred to the cruiser. On 10 October, she captured the steamer ''Huntsman'', the captain of which had not sent a distress signal until the last minute, as he had mistakenly identified ''Admiral Graf Spee'' as a French warship. Unable to accommodate the crew from ''Huntsman'', ''Admiral Graf Spee'' sent the ship to a rendezvous location with a prize crew. On 15 October, ''Admiral Graf Spee'' rendezvoused with ''Altmark'' to refuel and transfer prisoners; the following morning, the prize ''Huntsman'' joined the two ships. The prisoners aboard ''Huntsman'' were transferred to ''Altmark'' and Langsdorff then sank ''Huntsman'' on the night of 17 October.{{sfn|Bidlingmaier|p=80}}

[[File:Panzerschiff Admiral Graf Spee in 1936.jpg|thumb|left|''Admiral Graf Spee'' before the war]]
On 22 October, ''Admiral Graf Spee'' encountered and sank the steamer ''Trevanion''.{{sfn|Bidlingmaier|p=41}} At the end of October, Langsdorff sailed his ship into the Indian Ocean south of Madagascar. The purpose of that foray was to divert Allied warships away from the South Atlantic, and to confuse the Allies about his intentions. By this time, ''Admiral Graf Spee'' had cruised for almost {{convert|30000|nmi|lk=in}} and needed an engine overhaul.{{sfn|Bidlingmaier|p=81}} On 15 November, the ship sank the [[Tanker (ship)|tanker]] ''Africa Shell'', and the following day, she stopped an unidentified Dutch steamer, though did not sink her. ''Admiral Graf Spee'' returned to the Atlantic between 17 and 26 November to refuel from ''Altmark''.{{sfn|Rohwer|p=8}} While replenishing supplies, the crew of ''Admiral Graf Spee'' built a dummy gun turret on her [[Bridge (nautical)|bridge]] and erected a dummy second funnel behind the aircraft catapult to alter her silhouette significantly in a bid to confuse allied shipping as to her true identity.{{sfn|Bidlingmaier|p=82}}

''Admiral Graf Spee''{{'}}s Arado floatplane located the merchant ship ''Doric Star'': Langsdorff fired a shot across her bow to stop the ship.{{sfn|Bidlingmaier|p=83}} ''Doric Star'' was able to send out a distress signal before she was sunk, which prompted Harwood to take his three cruisers to the mouth of the River Plate, which he estimated would be Langsdorff's next target. On the night of 5 December, ''Admiral Graf Spee'' sank the steamer ''Tairoa''. The next day, she met with ''Altmark'' and transferred 140 prisoners from ''Doric Star'' and ''Tairoa''. ''Admiral Graf Spee'' encountered her last victim on the evening of 7 December: the freighter ''Streonshalh''. The prize crew recovered secret documents containing shipping route information.{{sfn|Bidlingmaier|p=86}} Based on that information, Langsdorff decided to head for the seas off [[Montevideo]]. On 12 December, the ship's [[Arado 196]] broke down and could not be repaired, depriving ''Graf Spee'' of her aerial reconnaissance.{{sfn|Pope|p=101}} The ship's disguise was removed, so it would not hinder the ship in battle.{{sfn|Bidlingmaier|p=88}}

==== Battle of the River Plate ====
{{main|Battle of the River Plate}}
[[File:Graf Spee in Montevideo.png|thumb|''Admiral Graf Spee'' in Montevideo following the battle]]

At 05:30 on the morning of 13 December 1939, lookouts spotted a pair of masts off the ship's starboard bow. Langsdorff assumed this to be the escort for a convoy mentioned in the documents recovered from ''Tairoa''. At 05:52, however, the ship was identified as {{HMS|Exeter|68|6}}; she was accompanied by a pair of smaller warships, initially thought to be destroyers but quickly identified as {{sclass-|Leander|cruiser|1||1931}}s. Langsdorff decided not to flee from the British ships, and so ordered his ship to battle stations and to close at maximum speed.{{sfn|Bidlingmaier|p=88}} At 06:08, the British spotted ''Admiral Graf Spee''; [[Henry Harwood|Commodore Harwood]] divided his forces up to split the fire of ''Admiral Graf Spee''{{'}}s 28&nbsp;cm guns.{{sfn|Jackson|p=64}} The German ship opened fire with her main battery at ''Exeter'' and her secondary guns at the flagship {{HMS|Ajax|22|2}} at 06:17. At 06:20, ''Exeter'' returned fire, followed by ''Ajax'' at 06:21 and {{HMNZS|Achilles|70|2}} at 06:24. In the span of thirty minutes, ''Admiral Graf Spee'' had hit ''Exeter'' three times, disabling her two forward turrets, destroying her bridge and her aircraft catapult, and starting major fires. ''Ajax'' and ''Achilles'' moved closer to ''Admiral Graf Spee'' to relieve the pressure on ''Exeter''.{{sfn|Bidlingmaier|p=91}}

Langsdorff thought the two light cruisers were making a torpedo attack, and turned away under a smokescreen.{{sfn|Bidlingmaier|p=91}} The respite allowed ''Exeter'' to withdraw from the action; by now, only one of her gun turrets was still in action, and she had suffered 61 dead and 23 wounded crew members.{{sfn|Jackson|p=64}} At around 07:00, ''Exeter'' returned to the engagement, firing from her stern turret. ''Admiral Graf Spee'' fired on her again, scored more hits, and forced ''Exeter'' to withdraw again, this time with a list to port. At 07:25, ''Admiral Graf Spee'' scored a hit on ''Ajax'' that disabled her aft turrets.{{sfn|Bidlingmaier|p=91}} Both sides broke off the action, ''Admiral Graf Spee'' retreating into the River Plate estuary, while Harwood's battered cruisers remained outside to observe any possible breakout attempts. In the course of the engagement, ''Admiral Graf Spee'' had been hit approximately 70 times; 36 men were killed and 60 more were wounded,{{sfn|Jackson|p=67}} including Langsdorff, who had been wounded twice by splinters while standing on the open bridge.{{sfn|Bidlingmaier|p=91}}

==== Scuttling ====
[[File:Graf Spee scuttled.png|thumb|''Admiral Graf Spee'' shortly after her scuttling]]
As a result of battle damage and casualties, Langsdorff decided to put into Montevideo, where repairs could be effected and the wounded men could be evacuated from the ship.{{sfn|Jackson|p=67}} Most of the hits scored by the British cruisers caused only minor structural and superficial damage but the oil purification plant, which was required to prepare the diesel fuel for the engines, was destroyed. Her [[desalination]] plant and galley were also destroyed, which would have increased the difficulty of a return to Germany. A hit in the bow would also have negatively affected her seaworthiness in the heavy seas of the North Atlantic. ''Admiral Graf Spee'' had fired much of her ammunition in the engagement with Harwood's cruisers.{{sfn|Williamson|p=42}}

After arriving in port, the wounded crewmen were taken to local hospitals and the dead were buried with full military honours. Captive Allied seamen still aboard the ship were released. Repairs necessary to make the ship seaworthy were expected to take up to two weeks.{{sfn|Bidlingmaier|p=92}} British naval intelligence worked to convince Langsdorff that vastly superior forces were concentrating to destroy his ship, if he attempted to break out of the harbour. The Admiralty broadcast a series of signals, on frequencies known to be intercepted by German intelligence. The closest heavy units—the carrier ''Ark Royal'' and battlecruiser ''Renown''—were some {{convert|2500|nmi|abbr=on}} away, much too far to intervene in the situation. Believing the British reports, Langsdorff discussed his options with commanders in Berlin. These were either to break out and seek refuge in [[Buenos Aires]], where the Argentine government would intern the ship, or to scuttle the ship in the Plate estuary.{{sfn|Jackson|p=67}}

Langsdorff was unwilling to risk the lives of his crew, so he decided to scuttle the ship. He knew that although Uruguay was neutral, the government was on friendly terms with Britain and if he allowed his ship to be interned, the Uruguayan Navy would allow British intelligence officers access to the ship.{{sfn|Williamson|p=42}} Under Article 17 of the Hague Convention, neutrality restrictions limited ''Admiral Graf Spee'' to a period of 72 hours for repairs in Montevideo, before she would be interned for the duration of the war.<ref>{{cite web|publisher=International Committee of the Red Cross|title=Convention (XIII) concerning the Rights and Duties of Neutral Powers in Naval War|date=18 October 1907|url=http://www.icrc.org/ihl.nsf/FULL/240}}</ref>{{sfn|Bonner|p=56}} On 17 December 1939, Langsdorff ordered the destruction of all important equipment aboard the ship. The ship's remaining ammunition supply was dispersed throughout the ship, in preparation for scuttling. On 18 December, the ship, with only Langsdorff and 40 other men aboard, moved into the outer [[roadstead]] to be scuttled.{{sfn|Bidlingmaier|p=93}} A crowd of 20,000 watched as the scuttling charges were set; the crew was taken off by an Argentine [[tugboat|tug]] and the ship was scuttled at 20:55.{{sfn|Bonner|p=56}}{{sfn|Williamson|p=43}} The multiple explosions from the munitions sent jets of flame high into the air and created a large cloud of smoke that obscured the ship which burned in the shallow water for the next two days.{{sfn|Bidlingmaier|p=93}}

On 20 December, in his room in a Buenos Aires hotel, Langsdorff shot himself in full dress uniform and lying on the ship's battle ensign.{{sfn|Bidlingmaier|p=93}} In late January 1940, the neutral American cruiser {{USS|Helena|CL-50|6}} arrived in Montevideo and the crew was permitted to visit the wreck of ''Admiral Graf Spee''. The Americans met the German crewmen, who were still in Montevideo.{{sfn|Bonner|p=56}} In the aftermath of the scuttling, the ship's crew were taken to Argentina, where they were interned for the remainder of the war.{{sfn|Bidlingmaier|p=93}}

=== Wreck ===
[[File:Graf Spee telémetro 01.jpg|thumb|''Graf Spee''{{'}}s salvaged [[rangefinder]]]]

The wreck was partially broken up [[in situ]] in 1942–1943, though parts of the ship are still visible; the wreck lies at a depth of only {{convert|11|m|abbr=on}}.{{sfn|Gröner|p=62}} The salvage rights were purchased from the German Government by the British, for [[Pound sign|£]]14,000, using a Montevideo engineering company as a front. The British had been surprised by the accuracy of the shooting and expected to find a radar range finder and were not disappointed. They used the knowledge thus acquired to try to develop countermeasures, under the leadership of [[Fred Hoyle]] at the British radar project. The Admiralty complained about the large sum paid for the salvage rights.<ref>Simon Mitton, ''Fred Hoyle, a Life in Science'', Chapter 4, ''Hoyle's Secret War'', p. 83, Cambridge University Press (2011)</ref>

In February 2004, a salvage team began work raising the wreck of ''Admiral Graf Spee''. The operation was in part being funded by the government of [[Uruguay]], in part by the private sector as the wreck was a hazard to navigation. The first major section—a {{convert|27|MT|sp=us}} gunnery range-finding [[telemeter]]—was raised on 25 February.{{sfn|BBC 2004-02-26}} On 10 February 2006, the {{convert|2|m|ftin|abbr=on}}, 400&nbsp;kg eagle and [[Swastika#As the symbol of Nazism|swastika]] crest of ''Admiral Graf Spee'' was recovered from the [[stern]] of the ship;{{sfn|BBC 2006-02-10}} it was stored in a Uruguayan naval warehouse following German complaints about exhibiting "Nazi paraphernalia".{{sfn|BBC 2014-12-15}}

==Footnotes==
{{Commons|Admiral Graf Spee| <br> Admiral Graf Spee}}

===Notes===
{{notes
| notes =
{{efn
| name = radar
| FMG stands for Funkmess Gerät (radar equipment). "G" denoted that the equipment was manufactured by GEMA, "g" indicated that it operated between 335 and 440 [[megahertz|MHz]], while "O" indicated the positioning of the set atop of the forward [[rangefinder]]. See {{harvnb|Williamson|p=7}}.
}}

}}

===Citations===
{{reflist|20em}}

==References==
*{{cite book
 |last = Bidlingmaier
 |first = Gerhard
 |year = 1971
 |chapter = KM Admiral Graf Spee
 |pages = 73–96
 |title = Warship Profile 4
 |publisher = Profile Publications
 |location = Windsor, England
 |oclc = 20229321
 |ref = {{sfnRef|Bidlingmaier}}
}}
*{{cite book
 |last = Bonner
 |first = Kermit
 |year = 1996
 |title = Final Voyages
 |publisher = Turner Publishing Company
 |location = Paducah, KY
 |isbn = 978-1-56311-289-8
 |url = https://books.google.com/books?id=53l3ebkpCjcC
 |ref = {{sfnRef|Bonner}}
}}
*{{cite news
 |publisher = BBC News
 |date = 26 February 2004
 |title = Divers recover piece of Graf Spee
 |url = http://news.bbc.co.uk/2/hi/americas/3489532.stm
 |accessdate = 10 February 2008
 |ref = {{sfnRef|BBC 2004-02-26}}
}}
*{{cite book
 |editor1-last = Gardiner
 |editor1-first = Robert
 |editor2-last = Chesneau
 |editor2-first = Roger
 |year = 1980
 |title = Conway's All the World's Fighting Ships, 1922–1946
 |publisher = Naval Institute Press
 |location = Annapolis
 |isbn = 978-0-87021-913-9
 |url = https://books.google.com/books?id=bJBMBvyQ83EC&printsec=frontcover
 |ref = {{sfnRef|Gardiner & Chesneau}}
}}
*{{cite news
 |publisher = BBC News
 |date = 10 February 2006
 |title = Graf Spee's eagle rises from deep
 |url = http://news.bbc.co.uk/1/hi/world/americas/4702832.stm
 |accessdate = 10 February 2008
 |ref = {{sfnRef|BBC 2006-02-10}}
}}
*{{cite book
 |last = Gröner
 |first = Erich
 |year = 1990
 |title = German Warships: 1815–1945
 |publisher = Naval Institute Press
 |location = Annapolis
 |isbn = 978-0-87021-790-6
 |ref = {{sfnRef|Gröner}}
}}
*{{cite book
 |editor-last = Jackson
 |editor-first = Robert
 |year = 2001
 |title = Kriegsmarine: The Illustrated History of the German Navy in WWII
 |publisher = MBI Publishing Company
 |location = Osceola, WI
 |isbn = 978-0-7603-1026-7
 |ref = {{sfnRef|Jackson}}
}}
*{{cite book
 |last1=Koop
 |first1=Gerhard
 |last2=Schmolke
 |first2=Klaus-Peter
 |title=Pocket battleships of the Deutschland class : Deutschland/Lützow, Admiral Scheer, Admiral Graf Spee
 |year=2014
 |location=Barnsley
 |publisher=Seaforth Publishing
 |isbn=9781848321960
 |ref={{sfnref|Koop & Schmolke}}
}}
*{{cite book
 |last = Pope
 |first = Dudley
 |year = 2005
 |title = The Battle of the River Plate: The Hunt for the German Pocket Battleship Graf Spee
 |publisher = McBooks Press
 |location = Ithaca, NY
 |isbn = 978-1-59013-096-4
 |ref = {{sfnRef|Pope}}
}}
*{{cite book
 |last = Rohwer
 |first = Jürgen
 |authorlink = Jürgen Rohwer
 |year = 2005
 |title = Chronology of the War at Sea, 1939–1945: The Naval History of World War Two
 |publisher = US Naval Institute Press
 |location = Annapolis
 |isbn = 978-1-59114-119-8
 |ref = {{sfnRef|Rohwer}}
}}
*{{cite book
 |last = Slader
 |first = John
 |year = 1988
 |title = The Red Duster at War
 |publisher = William Kimber & Co Ltd
 |place = London
 |isbn = 0-7183-0679-1
 |pages = 25–26
 |ref = {{sfnRef|Slader}}
}}
*{{cite news
 |publisher = BBC News
 |date = 15 December 2014
 |title = What should Uruguay do with its Nazi eagle?
 |url = http://www.bbc.com/news/world-latin-america-30471063
 |accessdate = 15 December 2014
 |ref = {{sfnRef|BBC 2014-12-15}}
}}
*{{cite book
 |last = Whitley
 |first = M. J.
 |year = 1998
 |title = Battleships of World War II
 |publisher = Naval Institute Press
 |location = Annapolis
 |isbn = 978-1-55750-184-4
 |ref = {{sfnRef|Whitley}}
}}
*{{cite book
 |last = Williamson
 |first = Gordon
 |year = 2003
 |title = German Pocket Battleships 1939–1945
 |publisher = Osprey Publishing
 |location = Oxford
 |isbn = 978-1-84176-501-3
 |ref = {{sfnRef|Williamson}}
}}

==See also==

* [[The Battle of the River Plate (film)]] (titled in the United States as "Pursuit of the Graf Spee") is a 1956 British war film about the Battle of the River Plate
* [https://archive.org/details/70700SinkingOfTheGrafSpee contemporary newsreel of the sinking]

==Further reading==
*{{cite book
 |last = Whitley
 |first = M. J.
 |year = 2000
 |title = German Capital Ships of World War Two
 |publisher = Cassell & Co.
 |location = London
 |isbn = 0-304-35707-3
 }}

{{coord|34|58|S|56|17|W|display=title}}

{{Deutschland class cruiser}}
{{December 1939 shipwrecks}}
{{Good article}}

{{DEFAULTSORT:Admiral Graf Spee}}
[[Category:1934 ships]]
[[Category:1939 in Uruguay]]
[[Category:Battle of the River Plate]]
[[Category:Deutschland-class cruisers]]
[[Category:Maritime incidents in December 1939]]
[[Category:Military units and formations of Nazi Germany in the Spanish Civil War]]
[[Category:Scuttled vessels of Germany]]
[[Category:Ships built in Wilhelmshaven]]
[[Category:Shipwrecks in rivers]]
[[Category:World War II cruisers of Germany]]
[[Category:World War II shipwrecks in the Atlantic Ocean]]
[[Category:World War II shipwrecks in the South Atlantic]]