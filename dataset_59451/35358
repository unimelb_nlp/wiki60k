{{Good article}}
{{Use American English|date=January 2016}}
{{Use mdy dates|date=January 2016}}
{{NASCAR race season infobox
| Type           = NAS
| country        = United States
| Race Name      = AAA 400
| Details ref    =<ref name="fx">{{cite web|url=http://espn.go.com/racing/schedule/_/year/2009|title=2009 NASCAR Sprint Cup Schedule|work=[[ESPN]]|publisher=ESPN Internet Ventures|accessdate=September 19, 2010}}</ref><ref name=event>{{cite web|title=The Race: AAA 400|url=http://www.jayski.com/next/2009/28dov2009.htm|work=Jayski's Silly Season Site|publisher=ESPN Internet Ventures|accessdate=January 3, 2015}}</ref><ref name=pole>{{cite web|title=Defending champion Johnson wins pole at Dover|url=http://usatoday30.usatoday.com/sports/motor/nascar/2009-09-25-dover-qualifying_N.htm|work=[[USA Today]]|publisher=[[Gannett Company]]|date=September 25, 2009|accessdate=January 3, 2015}}</ref><ref name="weather-information">{{cite web|title=Weather Information for Dover, Delaware|url=http://m.almanac.com/weather/history/DE/Dover/2009-09-27|work=[[Old Farmer's Almanac]]|publisher=Yankee Publishing|accessdate=January 14, 2015}}</ref>
| Fulldate       = {{Start date|2009|9|27}}
| Year           = 2009
| Race_No        = 28
| Season_No      = 36
| Image          = Dover International Speedway.PNG
| Caption     = Layout of Dover International Speedway
| Location       = [[Dover International Speedway]], [[Dover, Delaware]]
| Course_mi      = 1.0
| Course_km      = 1.609
| Distance_laps  = 400
| Distance_mi    = 400
| Distance_km    = 643.737
| Weather        = Temperatures reaching up to {{convert|75.2|F|C}}; wind speeds up to {{convert|11.1|mph|km/h}}
| Avg            = {{convert|118.704|mi/h}}
| Pole_Driver    = [[Jimmie Johnson]]
| Pole_Team      = [[Hendrick Motorsports]]
| Pole_Time      = 22.878
| Most_Driver    = Jimmie Johnson
| Most_Team      = Hendrick Motorsports
| Most_laps      = 271
| Car            = 48
| First_Driver   = Jimmie Johnson
| First_Team     = Hendrick Motorsports
| Network        = [[ESPN]]
| Announcers     = [[Jerry Punch]], [[Dale Jarrett]] and [[Andy Petree]]
| Ratings          = {{Plainlist|
* 3.1/6 (Final)
* 2.7/5 (Overnight)
* (5.08 million)<ref name=ratings>{{cite web|title=2009 NASCAR Sprint Cup TV Ratings|url=http://www.jayski.com/pages/tvratings2010.htm|work=Jayski's Silly Season Site|publisher=ESPN Internet Ventures|accessdate=January 14, 2015}}</ref>}}
}}
The '''2009 AAA 400''' was the twenty-eighth [[Stock car racing|stock car race]] of the [[2009 NASCAR Sprint Cup Series]] and the second in the ten-race season-ending [[Chase for the Sprint Cup]]. It was held on September 27, 2009 at [[Dover International Speedway]], in [[Dover, Delaware]] before a crowd of 110,000 people. The 400-lap race was won by [[Jimmie Johnson]] of the [[Hendrick Motorsports]] team after he started from [[pole position]]. His teammate [[Mark Martin]] finished second and [[Matt Kenseth]] came in third.

Johnson won the [[pole position]] and maintained his lead on the first lap to begin the race. After a [[Glossary of motorsport terms#C|competition caution]] on lap 25, [[Ryan Newman]] became the leader of the race. Chase for the Sprint Cup participants [[Kurt Busch]] and [[Jeff Gordon]] were in the top ten for most of the race. Johnson reclaimed the lead, after passing Kurt Busch. Johnson maintained the first position to lead the most laps of 271, and to win his fourth race of the season. There were nine cautions and six lead changes among four different drivers during the course of the race.

The race was Johnson's fourth win of the 2009 season, as well as the forty-fourth of his career. The result kept Martin in the lead in the [[List of NASCAR Sprint Cup Series champions|Drivers' Championship]], ten points ahead of Johnson, and sixty-five in front of [[Juan Pablo Montoya]]. [[Chevrolet]] maintained its lead in the [[NASCAR Manufacturers' Championship|Manufacturers' Championship]], forty-five ahead of [[Toyota]] and seventy-five in front of [[Ford Motor Company|Ford]], who bumped [[Dodge]], with one-hundred and twenty-two points, to fourth place. The race attracted 5.08 million television viewers.

==Report==
===Background===
[[File:Dover International Raceway.jpg|thumb|left|[[Dover International Speedway]], where the race was held.]]
[[Dover International Speedway]] is one of five [[Short track motor racing|short track]]s to hold NASCAR races; the others are [[Bristol Motor Speedway]], [[Richmond International Raceway]], [[Martinsville Speedway]], and [[Phoenix International Raceway]].<ref name="tacksnascar">{{Cite web|title=NASCAR Race Tracks|url=http://www.nascar.com/races/tracks/|accessdate=23 September 2010|work=[[NASCAR]]|publisher=Turner Sports Interactive, Inc.|archiveurl= https://web.archive.org/web/20100912133620/http://www.nascar.com/races/tracks/| archivedate=September 12, 2010 <!--DASHBot-->}}</ref> The NASCAR race makes use of the track's standard configuration, a four-turn short track [[Oval track racing|oval]] that is {{convert|1|mi|km}} long.<ref name="doverint">{{Cite web|title=NASCAR Tracks—The Dover International Speedway|url=http://www.doverspeedway.com/track/records.php|publisher=Dover International Speedway|accessdate=23 September 2010| archiveurl= https://web.archive.org/web/20100921052901/http://doverspeedway.com/track/records.php| archivedate=September 21, 2010 <!--DASHBot-->}}</ref> The track's turns are [[Banked turn|banked]] at twenty-four degrees, and both the [[Straight (racing)|front stretch]] (the location of the finish line) and the backstretch are banked at nine degrees.<ref name="doverint" />

Before the race, [[Mark Martin]] led the [[List of NASCAR Sprint Cup Series champions|Drivers' Championship]] with 5,230 points; [[Jimmie Johnson]] and [[Denny Hamlin]] were tied for second with 5,195 points each, 35 points behind Martin. [[Juan Pablo Montoya]] was fourth with 5,175 points, ten ahead of [[Kurt Busch]] and nineteen ahead of [[Tony Stewart]] in fifth and sixth respectively. [[Ryan Newman]] with 5,151 was eleven points ahead of [[Brian Vickers]], as [[Greg Biffle]] with 5,138 points, was ten ahead of [[Jeff Gordon]]. [[Carl Edwards]] and [[Kasey Kahne]] rounded out the top twelve positions in the [[Chase for the Sprint Cup]] with 5,117 and 5,069 points respectively.<ref>{{cite web|title=Drivers' Championship Classification|url=http://www.nascar.com/races/cup/2009/27/data/standings_official.html|work=NASCAR|publisher=Turner Sports Interactive, Inc.|accessdate=January 3, 2015|archiveurl=https://web.archive.org/web/20090926115226/http://www.nascar.com/races/cup/2009/27/data/standings_official.html|archivedate=September 26, 2009}}</ref>  In the [[NASCAR Manufacturers' Championship|Manufacturers' Championship]], [[Chevrolet]] was leading with 199 points, thirty-nine points ahead of their rivals [[Toyota]] in second place. [[Dodge]], with 118 points, were one point ahead of [[Ford]] in the battle for third place.<ref name=manufacturers>{{cite web|title=Manufacturer's Championship Classification|url=http://www.jayski.com/stats/2009/manu2009.htm|work=Jayski's Silly Season Site|publisher=ESPN Internet Ventures|accessdate=December 27, 2014}}</ref> Johnson was the race's defending champion.<ref>{{cite web|title=2008 Camping World RV 400|url=http://racing-reference.info/race/2008_Camping_World_RV_400_Presented_by_Coleman/W|work=Racing-Reference|publisher=Fox Sports Digital|accessdate=January 3, 2015}}</ref>

===Practice and qualifying===
Three practice sessions were held before the Sunday race—one on Friday, and two on Saturday. The first session lasted 90 minutes, and the second 45 minutes. The final session lasted 60 minutes.<ref name=event/> In the first practice session, Johnson was fastest, placing ahead of Newman in second and Montoya in third. Biffle took fourth position and Kurt Busch placed fifth. [[A.J. Allmendinger]], [[David Reutimann]], [[Kevin Harvick]], Martin and [[David Gilliland]] rounded out the top ten fastest drivers in the session.<ref>{{cite web|title=Practice One Speeds|url=http://www.nascar.com/races/cup/2009/28/data/practice1_speeds.html|work=NASCAR|publisher=Turner Sports Interactive, Inc.|accessdate=January 3, 2015|archiveurl=https://web.archive.org/web/20090928083513/http://www.nascar.com/races/cup/2009/28/data/practice1_speeds.html|archivedate=September 28, 2009}}</ref> During the session, Bowyer broke a rocker arm, and his team changed engines as a consequence.<ref name=event/>

Although forty-four drivers were entered in the qualifier;<ref>{{cite web|title=Qualifying Order|url=http://www.nascar.com/races/cup/2009/28/data/qual_order.html|work=NASCAR|publisher=Turner Sports Interactive, Inc.|accessdate=January 14, 2015|archiveurl=https://web.archive.org/web/20091120025033/http://www.nascar.com/races/cup/2009/28/data/qual_order.html|archivedate=November 20, 2009}}</ref> according to NASCAR's [[Go or Go Home|qualifying procedure]], only forty-three could race. Each driver ran two laps, with the starting order determined by the competitor's fastest times.<ref name="event" /> Johnson clinced his third [[pole position]] of the season, with a time of 22.878. He was joined on the [[Glossary of motorsport terms#grid|grid]]'s front row by Montoya. Newman qualified third, Biffle took fourth and Reutimann started fifth. Kahne, Gordon, Bowyer, [[Sam Hornish, Jr.]] and [[Paul Menard]] completed the top ten positions. The driver that failed to qualify was [[Scott Wimmer]].<ref name=lineup>{{cite web|title=Race Lineup|url=http://www.nascar.com/races/cup/2009/28/data/lineup.html|work=NASCAR|publisher=Turner Sports Interactive, Inc.|accessdate=January 3, 2015|archiveurl=https://web.archive.org/web/20090928082030/http://www.nascar.com/races/cup/2009/28/data/lineup.html|archivedate=September 28, 2009}}</ref> During qualifying, [[Elliott Sadler]]'s changed his car's engine, after one failed during the session.<ref>{{cite web|author=Sporting Wire News Service|title=Johnson on pole at Dover as top two rows all Chasers|url=http://www.nascar.com/2009/news/headlines/cup/09/25/jjohnson.pole.dover/index.html|work=NASCAR|publisher=Turner Sports Interactive, Inc.|date=September 25, 2009|accessdate=February 3, 2015|archiveurl=https://web.archive.org/web/20090928082001/http://www.nascar.com/2009/news/headlines/cup/09/25/jjohnson.pole.dover/index.html|archivedate=September 28, 2009}}</ref> After the qualifier Johnson said, "A pole today will make the start of the weekend much better and give us a lot of direction and momentum moving into tomorrow, It does carry you, and there is an aspect of momentum. But at the same time, you've got to go out and perform."<ref name=pole/>

On Saturday morning, Kurt Busch was fastest in the second practice session, ahead of Montoya in second, and Newman in third. Johnson was fourth quickest, and Bowyer took fifth. [[Kyle Busch]] managed sixth. Stewart, Hamlin, [[Matt Kenseth]] and Gilliland followed in the top ten. Of the other drivers in the Chase, Biffle finished with the eleventh fastest time, while Kahne set the fourteenth fastest time.<ref>{{cite web|title=Practice Two Speeds|url=http://www.nascar.com/races/cup/2009/28/data/practice2_speeds.html|work=NASCAR|publisher=Turner Sports Interactive, Inc.|accessdate=January 3, 2015|archiveurl=https://web.archive.org/web/20090929084605/http://www.nascar.com/races/cup/2009/28/data/practice2_speeds.html|archivedate=September 29, 2009 }}</ref> Kahne set the fastest time in the final practice session, while Montoya and [[Joey Logano]] followed in second and third respectively. Martin was fourth quickest, ahead of Biffle and Johnson. Kurt Busch was seventh fastest, [[Jamie McMurray]] eighth, Gilliland ninth and [[Martin Truex, Jr.]] tenth. Other chase drivers included Newman in eleventh and Hamlin in eighteenth.<ref>{{cite web|title=Practice Three Speeds|url=http://www.nascar.com/races/cup/2009/28/data/practice3_speeds.html|work=NASCAR|publisher=Turner Sports Interactive, Inc.|accessdate=January 3, 2015|archiveurl=https://web.archive.org/web/20090929084615/http://www.nascar.com/races/cup/2009/28/data/practice3_speeds.html|archivedate=September 29, 2009}}</ref>

===Race===
The race, the twenty-eighth of a total of thirty-six in the [[2009 NASCAR Sprint Cup Series|2009 season]], began at 2:00 p.m. and was televised live in the United States on [[ESPN]].<ref name=fx/> Around the start of the race, weather conditions were cloudy with the air temperature {{convert|71|F|C}}; a moderate chance of rain was forecast. Pastor Dan Schafer began pre-race ceremonies by giving the [[invocation]]. Country music group and [[Show Dog-Universal Music]] recording artists [[Trailer Choir]] performed the [[The Star-Spangled Banner|national anthem]], and Sergeant Major John Jones of the [[Pennsylvania National Guard]] gave the command for drivers to start their engines. During the [[Parade lap|pace laps]], Bowyer, Sadler and [[Tony Raines]] all had to move to the rear of the grid because of them changing their engines. NASCAR announced that a [[Glossary of motorsport terms#C|competition caution]] would take place on lap 25, meaning drivers would make mandatory [[Pit stop|pit stops]].<ref name=summary>{{cite web|title=Lap-by-Lap: Dover|url=http://www.nascar.com/2009/races/lapbylap/09/26/lap.by.lap.dover2/index.html|work=NASCAR|publisher=Turner Sports Interactive, Inc.|date=September 28, 2009|accessdate=January 13, 2015|archiveurl=https://web.archive.org/web/20090930083637/http://www.nascar.com/2009/races/lapbylap/09/26/lap.by.lap.dover2/index.html|archivedate=September 30, 2009}}</ref>
[[File:Jimmie Johnson.jpg|thumb|right|[[Jimmie Johnson]] won [[pole position]] and the race.]]
Johnson maintained his pole position lead into the first corner. One lap later, Newman passed Montoya for the second position. After starting the race in twelfth, Martin had lost four positions to run eighteenth by lap 4. By the 10th lap, Johnson had built up a 1.2 second lead over Newman. Bowyer, who began the race at the rear of the grid, had moved up thirteen positions to twenty-ninth by lap 15. Two laps later, Raines drove to his garage. On lap 18, Kurt Busch passed Vickers for ninth, as Kahne claimed fourth position from Biffle, two laps later. On lap 25, the competition caution came out. During the caution, all of the leaders made pit stops; Gilliland became the new leader on lap 28. After pit stops, Newman claimed the first position and held it at the restart.<ref name=summary/>

On lap 31, a [[The Big One (NASCAR)|multi-car collision]] occurred in turn 3 as Logano was bumped by Stewart, causing him to flip sideways, which collected Truex, [[Robby Gordon]] and [[Reed Sorenson]], prompting the second caution. On the same lap, the [[Racing flags#Red flag|red flag]] was shown to allow race officials to clear the track of debris. The race was restarted 24 minutes later under caution. Newman maintained his lead at the restart, followed by Kurt Busch and Menard. Three laps later, Montoya and Gordon passed Menard for fifth and sixth respectively. On lap 43, [[Jeff Burton]] fell to ninth after being passed by Hamlin and Martin. Four laps later, Martin passed Hamlin for the thirteenth position.<ref name=summary/>

By lap 50, Johnson passed Reutimann to move back into the top ten. On lap 58, Kurt Busch passed Newman to claim the lead. Three laps later, Jeff Gordon claimed fifth position off Kahne, while Menard was passed by Johnson for eighth. On lap 63, Montoya moved into the third position after passing Biffle, while Gilliland went to his garage to retire from the race. Two laps later, Kyle Busch claimed seventh position off Kahne, as Montoya passed Newman for second on lap 66. On lap 67, Gordon and Biffle passed Newman for third and fourth positions. Four laps later, Johnson claimed seventh from Kahne. On lap 80, Reutimann ran out of fuel, forcing him to make a pit stop.<ref name=summary/>

On lap 83, Biffle was passed by Johnson for the fourth position. One lap later, [[Michael Waltrip]] lost his car's right-front tire and collided with the [[SAFER barrier|wall]], causing the third caution. All of the leaders elected to make pit stops. Kurt Busch remained the leader at the restart, ahead of Biffle and Kyle Busch. On lap 93, Kyle Busch passed Biffle for second position, while McMurray was passed by Kahne for ninth on lap 96. Three laps later, Biffle dropped to sixth after being passed by Jeff Gordon, as Montoya passed Kyle Busch for the third position on lap 113. Sixteen laps later, Kahne passed Jeff Gordon for sixth, while Stewart moved into the eighteenth position on lap 137. On the 147th lap, Kurt Busch was blocked by [[Bobby Labonte]], allowing Johnson to claim the lead. By the 150th lap, Kyle Busch dropped three positions to sixth after being passed by Martin, Kahne and Jeff Gordon.<ref name=summary/>

Green flag pit stops began on lap 152, when Hornish made a pit stop. Eleven laps later, the fourth caution came out. During the caution, which was caused by liquid on the track, all of the leaders made pit stops. Kurt Busch reclaimed the lead for the lap 168 restart. Stewart moved into the eleventh position on lap 170. Six laps later, Johnson reclaimed the lead off Kurt Busch. By the 183rd lap, Johnson opened out a 1.6 second lead over Kurt Busch. Twenty-four laps later, Kyle Busch collided with the wall in turn 3 and turn 4, prompting the fifth caution. All of the leaders chose to make pit stops during the caution. The race resumed on lap 211, with Johnson leading from Jeff Gordon and Kurt Busch.<ref name=summary/>

By the 223rd lap, Johnson had a lead of 1.6 seconds. Twenty-seven laps later, Sorenson rejoined the race track. On lap 252, Stewart passed Allmendinger for the ninth position. Seven laps later, Stewart moved into eighth after passing Newman, and passed Bowyer for seventh on the 262nd lap. On lap 272, Johnson's lead of 3.5 seconds was reduced to nothing when the [[Safety car|pace car]] moved on track. During the caution, which was caused by [[David Stremme]] making contact with the wall at turn 4, all of the leaders made pit stops. Johnson remained the leader at the restart. On lap 286, Kahne was passed  by Newman for ninth. By lap 290, Johnson built up a lead of two seconds. Seventeen laps later, Martin passed Kenseth for the fourth position. Johnson's lead had increased to 2.2 seconds by lap 319. Five laps later, debris was spotted on the track and the seventh caution was prompted. All of the leaders elected to make pit stops during the caution.<ref name=summary/>

Johnson maintained his lead at the restart, followed by Martin and Kenseth. One lap later, Montoya moved into the second position, as Martin fell to fifth. On lap 336, Martin moved into fourth after passing Kurt Busch, and passed Kenseth for third two laps later. On lap 341, the eighth caution came out when [[Regan Smith]] spun off, collecting Stremme, Truex and Sadler. Johnson kept the lead at the lap 347 restart. One lap later, Kenseth moved into the second position, as Stewart and Gordon moved into fourth and tenth respectively. On lap 349, Gordon passed Newman for ninth, before falling to eleventh position after contact with Newman one lap later. On the 354th lap, Stewart was passed by Martin for the fourth position, as Johnson had built a 2.2 second lead by lap 357. One lap later, Jeff Gordon moved into the ninth position after passing Newman.<ref name=summary/>

On the 368th lap, Hornish, spun on the backstraightway, prompting the ninth and final caution of the race. During the caution, some of the leaders made pit stops. Johnson maintained the lead on the lap 373. Four laps later, Newman passed Mears for eighth, as Edwards was passed by Stewart for twelfth. On the 383rd lap, Kahne, Allemndinger and Stewart passed Newman for seventh, eighth and ninth respectively. On the next lap, Kenseth was passed by Martin for second position. On the 391st lap, Kahne was passed by Allmendinger for the second position. Two laps later, Johnson's lead had increased to 2.2 seconds and held it to win the race. Martin finished second, ahead of Kenseth in third, Montoya fourth, and Kurt Busch fifth. Gordon, Allmendinger, Kahne, Stewart and Newman rounded out the top ten finishers.<ref name=summary/><ref name=results>{{cite web|title=2009 Official Race Results: AAA 400|url=http://www.nascar.com/races/cup/2009/28/data/results_official.html|work=NASCAR|publisher=Turner Sports Interactive, Inc.|accessdate=January 14, 2015|archiveurl=https://web.archive.org/web/20091001152551/http://www.nascar.com/races/cup/2009/28/data/results_official.html|archivedate=October 1, 2009}}</ref>

===Post-race===
{{Quote box|quote  = As far as sending a message, I hope it does. I hope people are worried." |source = Johnson, speaking after the race.<ref>{{cite web|author=Associated Press|title=Johnson caps Dover season sweep|url=http://sports.espn.go.com/rpm/nascar/cup/news/story?id=4508718|work=ESPN|publisher=ESPN Internet Ventures|date=September 27, 2009|accessdate=January 14, 2015}}</ref>|width  = 37%|align  = left}}
Johnson appeared in [[Glossary of motorsport terms#V|victory lane]] to celebrate his fourth victory of the season in front of 110,000 people who attended the race.<ref>{{cite web|title=2009 AAA 400|url=http://racing-reference.info/race/2009_AAA_400/W|work=Racing-Reference|publisher=Fox Sports Digital|accessdate=January 14, 2015}}</ref> Johnson also earned $276,076 in race winnings.<ref name=results/> Afterward, he said, "I woke up (on Sunday) morning very optimistic. By about lap two or three I knew we had a very balanced car and we'd be competitive all day long, get a solid finish.", he continued, "I see guys get so worried about what other people think, what other people say and spend a lot of time in those areas. That's not what works for me. (I) don't watch television; don't watch or read any of the trade papers or magazines. Just ignore, ignore, ignore and focus on my world and what's going on with my race car. That's what I'll do through the rest of the Chase."<ref>{{cite web|last=Graves|first=Gary|title=Johnson notches Dover sweep to close in on Chase's top spot|url=http://usatoday30.usatoday.com/sports/motor/nascar/2009-09-27-johnson-dover_N.htm|work=USA Today|publisher=Gannett Company|date=September 28, 2009|accessdate=January 14, 2015}}</ref>

Martin, who finished second, was candid about the result, "We did really well to finish second. I just don't think we were in [Johnson's] league today."<ref>{{cite web|last=Schantz|first=Pete|title=Johnson beats Martin at Dover|url=http://articles.philly.com/2009-09-28/sports/25269032_1_chase-competitors-chase-drivers-restarts|work=[[The Philadelphia Inquirer]]|publisher=Interstate General Media|date=September 28, 2009|accessdate=January 14, 2015}}</ref> In the post-race press conference, Kenseth said of his result, "We didn't qualify very good, but we were really happy with our car. When the race started, I didn't think we were quite as good as we were yesterday (in practice), but we were able to have really good pit stops."<ref>{{cite web|title=Kenseth's 3rd-Place Finish Leads Team Valvoline in Dover|url=http://www.valvoline.com/racing/news/nascar/2930|work=Valvoline|date=September 27, 2009|accessdate=January 14, 2015}}</ref> Logano, who was involved in the biggest accident of the race, "The biggest thing was, I was fine the whole time, [but] I'm not really sure what happened. The spotter was clearing me low. When I got down there, they checked up going into the corner and I got tagged from behind."<ref>{{cite web|last=Aumann|first=Mark|title=Logano walks away from scary crash shaken but OK|url=
http://www.nascar.com/2009/news/headlines/cup/09/27/post.race.jlogano.crash.dover/index.html|work=NASCAR|publisher=Turner Sports Interactive, Inc.|date=September 28, 2009|accessdate=January 14, 2015|archiveurl=https://web.archive.org/web/20091012170203/http://www.nascar.com/2009/news/headlines/cup/09/27/post.race.jlogano.crash.dover/index.html|archivedate=October 12, 2009}}</ref>

The race result kept Martin in the lead of the Drivers' Championship with 5,400 points. Johnson, who won the race, stood in second, ten points behind Martin, and sixty-five ahead of Montoya. Kurt Busch moved into fourth position with 5,325 points. Stewart with fifth, Hamlin sixth, and Newman, Jeff Gordon, Biffle and Vickers followed in the top-ten positions. The final two positions available in the Chase for the Sprint Cup were occupied by Edwards in eleventh and Kahne in twelfth.<ref name=pointspostrace>{{cite web|title=Points Standings|url=http://www.nascar.com/races/cup/2009/28/data/standings_official.html|work=NASCAR|publisher=Turner Sports Interactive, Inc.|accessdate=January 14, 2015|archiveurl=https://web.archive.org/web/20091003120847/http://www.nascar.com/races/cup/2009/28/data/standings_official.html|archivedate=October 3, 2009}}</ref> In the Manufacturers' Championship, Chevrolet maintained their lead with 208. Toyota remained second with 163. Ford followed with 123 points, one point ahead of Dodge in fourth.<ref name=manufacturers/> 5.08 million people watched the race on television.<ref name=ratings/> The race took three hours, twenty-two minutes and eleven seconds to complete, and the margin of victory was 1.970 seconds.<ref name=results/>

==Results==
===Qualifying===
{| class="wikitable"
|-
|+ Qualifying results
|-
! scope="col" | Grid
! scope="col" | {{Tooltip|Car|Car number}}
! scope="col" | Driver
! scope="col" | Team
! scope="col" | Manufacturer
! scope="col" | {{Tooltip|Time|Time in seconds}}
! scope="col" | {{Tooltip|Speed|Speed in MPH}}
|-
! scope="row" |  1
|48 ||[[Jimmie Johnson]] ||[[Hendrick Motorsports]] ||[[Chevrolet]] ||22.878 ||157.356
|-
! scope="row" |  2
|42 ||[[Juan Pablo Montoya]] ||[[Earnhardt Ganassi Racing]] ||Chevrolet ||22.974 ||156.699
|-
! scope="row" |  3
|39 ||[[Ryan Newman]] ||[[Stewart-Haas Racing]] ||Chevrolet ||23.019 ||156.393
|-
! scope="row" |  4
|16 ||[[Greg Biffle]] ||[[Roush Fenway Racing]] ||[[Ford Motor Company|Ford]] ||23.035 ||156.284
|-
! scope="row" |  5
|00 ||[[David Reutimann]] ||[[Michael Waltrip Racing]] ||[[Toyota]] ||23.080 ||155.979
|-
! scope="row" |  6
|9 ||[[Kasey Kahne]] ||[[Evernham Motorsports]] ||[[Dodge]] ||23.096 ||155.871
|-
! scope="row" |  7
|24 ||[[Jeff Gordon]] ||[[Hendrick Motorsports]] ||Chevrolet ||23.110 ||155.777
|-
! scope="row" |  8
|33 ||[[Clint Bowyer]] ||[[Richard Childress Racing]] ||Chevrolet ||23.143 ||155.555{{smallsup|1}}
|-
! scope="row" |  9
|77 ||[[Sam Hornish, Jr.]] ||[[Team Penske]] ||Dodge ||23.152 ||155.494
|-
! scope="row" |  10
|98 ||[[Paul Menard]] ||[[Robert Yates Racing]] ||Ford ||23.161 ||155.434
|-
! scope="row" | 11
|20 ||[[Joey Logano]] ||[[Joe Gibbs Racing]] ||Toyota ||23.170 ||155.373
|-
! scope="row" | 12
|83 ||[[Brian Vickers]] ||[[Red Bull Racing Team]] ||Toyota ||23.173 ||155.353
|-
! scope="row" | 13
|11 ||[[Denny Hamlin]] ||Joe Gibbs Racing ||Toyota ||23.197 ||155.193
|-
! scope="row" | 14
|5 ||[[Mark Martin]] ||Hendrick Motorsports ||Chevrolet ||23.219 ||155.045
|-
! scope="row" | 15
|18 ||[[Kyle Busch]] ||Joe Gibbs Racing ||Toyota ||23.227 ||154.992
|-
! scope="row" | 16
|2 ||[[Kurt Busch]] ||Penske Championship Racing ||Dodge ||23.240 ||154.905
|-
! scope="row" | 17
|12 ||[[David Stremme]] ||Penske Racing ||Dodge ||23.251 ||154.832
|-
! scope="row" | 18
|171 ||[[David Gilliland]] ||[[The Racer's Group|TRG Motorsports]] ||Chevrolet ||23.280 ||154.639
|-
! scope="row" | 19
|44 ||[[A.J. Allmendinger]] ||[[Richard Petty Motorsports]] ||Dodge ||23.281 ||154.633
|-
! scope="row" | 20
|29 ||[[Kevin Harvick]] ||Richard Childress Racing ||Chevrolet ||23.285 ||154.606
|-
! scope="row" | 21
|6 ||[[David Ragan]] ||Roush Fenway Racing ||Ford ||23.290 ||154.573
|-
! scope="row" | 22
|14 ||[[Tony Stewart]] ||Stewart-Haas Racing ||Chevrolet ||23.296 ||154.533
|-
! scope="row" | 23
|17 ||[[Matt Kenseth]] ||Roush Fenway Racing ||Ford ||23.304 ||154.480
|-
! scope="row" | 24
|88 ||[[Dale Earnhardt, Jr.]] ||Hendrick Motorsports ||Chevrolet ||23.315 ||154.407
|-
! scope="row" | 25
|26 ||[[Jamie McMurray]] ||Roush Fenway Racing ||Ford ||23.333 ||154.288
|-
! scope="row" | 26
|43 ||[[Reed Sorenson]] ||Richard Petty Motorsports ||Dodge ||23.356 ||154.136
|-
! scope="row" | 27
|47 ||[[Marcos Ambrose]] ||[[JTG Daugherty Racing]] ||Toyota ||23.366 ||154.070
|-
! scope="row" | 28
|31 ||[[Jeff Burton]] ||Richard Childress Racing ||Chevrolet ||23.395 ||153.879
|-
! scope="row" | 29
|07 ||[[Casey Mears]] ||Richard Childress Racing ||Chevrolet ||23.406 ||153.807
|-
! scope="row" | 30
|99 ||[[Carl Edwards]] ||Roush Fenway Racing ||Ford ||23.444 ||153.557
|-
! scope="row" | 31
|7 ||[[Robby Gordon]] ||[[Robby Gordon Motorsports]] ||Toyota ||23.462 ||153.440
|-
! scope="row" | 32
|1 ||[[Martin Truex, Jr.]] ||Earnhardt Ganassi Racing ||Chevrolet ||23.470 ||153.387
|-
! scope="row" | 33
|78 ||[[Regan Smith]] ||[[Furniture Row Racing]] ||Chevrolet ||23.497 ||153.211
|-
! scope="row" | 34
|19 ||[[Elliott Sadler]] ||Richard Petty Motorsports ||Dodge ||23.513 ||153.107{{smallsup|1}}
|-
! scope="row" | 35
|66 ||[[Dave Blaney]] ||[[Prism Motorsports]] ||Toyota ||23.518 ||153.074
|-
! scope="row" | 36
|09 ||[[Mike Bliss]] ||[[Phoenix Racing (NASCAR team)|Phoenix Racing]] ||Dodge ||23.525 ||153.029
|-
! scope="row" | 37
|34 ||[[John Andretti]] ||[[Front Row Motorsports]] ||Chevrolet ||23.539 ||152.938
|-
! scope="row" | 38
|55 ||[[Michael Waltrip]] ||Michael Waltrip Racing ||Toyota ||23.549 ||152.873
|-
! scope="row" | 39
|82 ||[[Scott Speed]] ||Red Bull Racing Team ||Toyota ||23.588 ||152.620
|-
! scope="row" | 40
|187 ||[[Joe Nemechek]] ||[[NEMCO Motorsports]] ||Toyota ||23.677 ||152.046
|-
! scope="row" | 41
|96 ||[[Bobby Labonte]] ||[[Hall of Fame Racing]] ||Ford ||23.684 ||152.001
|-
! scope="row" | 42
|37 ||[[Tony Raines]] ||Front Row Motorsports ||Dodge ||23.755 ||151.547{{smallsup|1}}
|-
! scope="row" | 43
|36 ||[[Michael McDowell (racing driver)|Michael McDowell]] ||[[Prism Motorsports]] ||Toyota ||23.780 ||151.388
|-
|colspan="8"|<center>'''Failed to qualify'''</center>
|-
! scope="row" | 44
|4 ||[[Scott Wimmer]] ||[[Morgan-McClure Motorsports]] ||Chevrolet ||24.121 ||149.247
|-
|colspan="8"|{{center|{{small|''Source:<ref name=lineup/>''}}}}
|-
|colspan="7"|{{center|{{small|{{smallsup|1}} Moved to the back of the field for changing engines}}}}
|}

===Race===
{| class="wikitable" 
|-
|+ Race results
|-
! scope="col" | {{Tooltip|Pos|Position}}
! scope="col" | Grid
! scope="col" | {{Tooltip|Car|Car number}}
! scope="col" | Driver
! scope="col" | Team
! scope="col" | Manufacturer
! scope="col" | Laps Run
! scope="col" | Points
|-
! scope="row" | 1
|1 ||48 ||Jimmie Johnson ||Hendrick Motorsports ||Chevrolet ||400 ||195{{smallsup|2}}
|-
! scope="row" | 2
|14 ||5 ||Mark Martin ||Hendrick Motorsports ||Chevrolet ||400 ||170
|-
! scope="row" | 3
|23 ||17 ||Matt Kenseth ||Roush Fenway Racing ||Ford ||400 ||165
|-
! scope="row" | 4
|2 ||42 ||Juan Pablo Montoya ||Earnhardt Ganassi Racing ||Chevrolet ||400 ||160
|-
! scope="row" | 5
|16 ||2 ||Kurt Busch ||Penske Championship Racing ||Dodge ||400 ||160{{smallsup|1}}
|-
! scope="row" | 6
|7 ||24 ||Jeff Gordon ||Hendrick Motorsports ||Chevrolet ||400 ||150
|-
! scope="row" | 7
|19 ||44 ||A.J. Allemdinger ||Richard Petty Motorsports ||Dodge ||400 ||146
|-
! scope="row" | 8
|6 ||9 ||Kasey Kahne ||Evernham Motorsports ||Dodge ||400 ||142
|-
! scope="row" | 9
|22 ||14 ||Tony Stewart ||Stewart-Haas Racing ||Chevrolet ||400 ||138
|-
! scope="row" | 10
|3 ||39 ||Ryan Newman ||Stewart-Haas Racing ||Chevrolet ||400 ||139{{smallsup|1}}
|-
! scope="row" | 11
|30 ||99 ||Carl Edwards ||Roush Fenway Racing ||Ford ||400 ||130
|-
! scope="row" | 12
|20 ||29 ||Kevin Harvick ||Richard Childress Racing ||Chevrolet ||400 ||127
|-
! scope="row" | 13
|4 ||16 ||Greg Biffle ||Roush Fenway Racing ||Ford ||400 ||124
|-
! scope="row" | 14
|27 ||47 ||Marcos Ambrose ||JTG Daugherty Racing ||Toyota ||400 ||121
|-
! scope="row" | 15
|8 ||33 ||Clint Bowyer ||Richard Childress Racing ||Chevrolet ||400 ||118
|-
! scope="row" | 16
|28 ||31 ||Jeff Burton ||Richard Childress Racing ||Chevrolet ||400 ||115
|-
! scope="row" | 17
|29 ||07 ||Casey Mears ||Richard Childress Racing ||Chevrolet ||400 ||112
|-
! scope="row" | 18
|12 ||83 ||Brian Vickers ||Red Bull Racing Team ||Toyota ||400 ||109
|-
! scope="row" | 19
|10 ||98 ||Paul Menard ||Robert Yates Racing ||Ford ||400 ||106
|-
! scope="row" | 20
|24 ||88 ||Dale Earnhardt, Jr. ||Hendrick Motorsports ||Chevrolet ||399 ||103
|-
! scope="row" | 21
|5 ||00 ||David Reutimann ||Michael Waltrip Racing ||Toyota ||398 ||100
|-
! scope="row" | 22
|13 ||11 ||Denny Hamlin ||Joe Gibbs Racing ||Toyota ||398 ||97
|-
! scope="row" | 23
|41 ||96 ||Bobby Labonte ||Hall of Fame Racing ||Ford ||398 ||94
|-
! scope="row" | 24
|21 ||6 ||David Ragan ||Roush Fenway Racing ||Ford ||396 ||91
|-
! scope="row" | 25
|39 ||82 ||Scott Speed ||Red Bull Racing Team ||Toyota ||396 ||88
|-
! scope="row" | 26
|9 ||77 ||Sam Hornish, Jr. ||Team Penske ||Dodge ||395 ||85
|-
! scope="row" | 27
|37 ||34 ||John Andretti ||Front Row Motorsports ||Chevrolet ||395 ||82
|-
! scope="row" | 28
|25 ||26 ||Jamie McMurray ||Roush Fenway Racing ||Ford ||394 ||79
|-
! scope="row" | 29
|17 ||12 ||David Stremme ||Penske Racing ||Dodge ||385 ||76
|-
! scope="row" | 30
|34 ||19 ||Elliott Sadler ||Richard Petty Motorsports ||Dodge ||353 ||73
|-
! scope="row" | 31
|15 ||18 ||Kyle Busch ||Joe Gibbs Racing ||Toyota ||342 ||70
|-
! scope="row" | 32
|33 ||78 ||Regan Smith ||Furniture Row Racing ||Chevrolet ||337 ||67
|-
! scope="row" | 33
|32 ||1 ||Martin Truex, Jr. ||Earnhardt Ganassi Racing ||Chevrolet ||251 ||64
|-
! scope="row" | 34
|31 ||7 ||Robby Gordon ||Robby Gordon Motorsports ||Toyota ||181 ||61
|-
! scope="row" | 35
|26 ||43 ||Reed Sorenson ||Richard Petty Motorsports ||Dodge ||170 ||58
|-
! scope="row" | 36
|38 ||55 ||Michael Waltrip ||Michael Waltrip Racing ||Toyota ||84 ||55
|-
! scope="row" | 37
|35 ||66 ||Dave Blaney ||Prism Motorsports ||Toyota ||76 ||52
|-
! scope="row" | 38
|43 ||36 ||Michael McDowell ||Prism Motorsports ||Toyota ||74 ||49
|-
! scope="row" | 39
|18 ||171 ||David Gilliland ||TRG Motorsports ||Chevrolet ||60 ||51{{smallsup|1}}
|-
! scope="row" | 40
|36 ||09 ||Mike Bliss ||Phoenix Racing ||Dodge ||54 ||43
|-
! scope="row" | 41
|40 ||187 ||Joe Nemechek ||NEMCO Motorsports ||Toyota ||51 ||40
|-
! scope="row" | 42
|11 ||20 ||Joey Logano ||Joe Gibbs Racing ||Toyota ||30 ||37
|-
! scope="row" | 43
|42 ||37 ||Tony Raines ||Front Row Motorsports ||Dodge ||13 ||34
|- class="sortbottom"
|colspan="9"|{{center|{{small|''Source:<ref name=results/>''}}}}
|- class="sortbottom"
|colspan="9"|{{center|{{small|{{smallsup|1}} Includes five bonus points for leading a lap}}}}
|- class="sortbottom"
|colspan="9"|{{center|{{small|{{smallsup|2}} Includes ten bonus points for leading the most laps}}}}
|}

==Standings after the race==
{{col-start}}
{{col-2}}
;Drivers' Championship standings
{| class="wikitable "
|-
! scope="col" | {{Tooltip|Pos|Position}}
! scope="col" | +/–
! scope="col" | Driver
! scope="col" | Points
|-
| 1
|align="left"| [[File:1rightarrow blue.svg|10px]]
| '''Mark Martin'''
| style="text-align:center;"| '''5,400'''
|-
| 2
|align="left"| [[File:1rightarrow blue.svg|10px]]
| Jimmie Johnson
| style="text-align:center;"| 5,390 (−10)
|-
| 3
|align="left"| [[File:1uparrow green.svg|10px]] 1
| Juan Pablo Montoya
| style="text-align:center;"| 5,335 (−65)
|-
| 4
|align="left"| [[File:1uparrow green.svg|10px]] 1
| Kurt Busch
| style="text-align:center;"| 5,325 (−75)
|-
| 5
|align="left"| [[File:1uparrow green.svg|10px]] 1
| Tony Stewart
| style="text-align:center;"| 5,294 (−106)
|-
| 6
|align="left"| [[File:1downarrow red.svg|10px]] 3
| Denny Hamlin
| style="text-align:center;"| 5,292 (−108)
|-
| 7
|align="left"| [[File:1rightarrow blue.svg|10px]]
| Ryan Newman
| style="text-align:center;"| 5,290 (−110)
|-
| 8
|align="left"| [[File:1uparrow green.svg|10px]] 2
| Jeff Gordon
| style="text-align:center;"| 5,278 (−122)
|-
| 9
|align="left"| [[File:1rightarrow blue.svg|10px]]
| Greg Biffle
| style="text-align:center;"| 5,262 (−138)
|-
| 10
|align="left"| [[File:1downarrow red.svg|10px]] 2
| Brian Vickers
| style="text-align:center;"| 5,249 (−151)
|-
| 11
|align="left"| [[File:1rightarrow blue.svg|10px]]
| Carl Edwards
| style="text-align:center;"| 5,247 (−153)
|-
| 12
|align="left"| [[File:1rightarrow blue.svg|10px]]
| Kasey Kahne
| style="text-align:center;"| 5,211 (−189)
|- class="sortbottom"
|colspan="9"|{{center|{{small|''Source:<ref name=pointspostrace/>''}}}}
|}
{{col-2}}
[[File:Mark Martin 2007 Daytona 500.jpg|thumb|right|[[Mark Martin]] remained the points leader with 5,400 points, after finishing second in the race.]]
;Manufacturers' Championship standings
{|class="wikitable"
|-
! scope="col" | {{Tooltip|Pos|Position}}
! scope="col" | +/–
! scope="col" | Manufacturer
! scope="col" | Points
|-
| 1
|align="left"| [[File:1rightarrow blue.svg|10px]]
| Chevrolet
| style="text-align:center;"| 208
|-
| 2
|align="left"| [[File:1rightarrow blue.svg|10px]]
| Toyota
| style="text-align:center;"| 163 (−45)
|-
| 3
|align="left"| [[File:1uparrow green.svg|10px]] 1
| Ford
| style="text-align:center;"| 123 (−85)
|-
| 4
|align="left"| [[File:1downarrow red.svg|10px]] 1
| Dodge
| style="text-align:center;"| 122 (−86)
|- class="sortbottom"
|colspan="9"|{{center|{{small|''Source:<ref name=manufacturers/>''}}}}
|}

*<small>'''Note''': Only the top twelve positions are included for the driver standings. These drivers qualified for the [[Chase for the Sprint Cup]].</small>
{{col-end}}

==References==
{{reflist|30em}}

{{NASCAR next race|
  Series        = Sprint Cup Series|
  Season        = 2009 |
  Previous_race = [[2009 Sylvania 300]] |
  Next_race     = [[2009 Price Chopper 400]] |
}}
{{2009 NASCAR Sprint Cup |state=collapsed}}
{{Portal bar|2000s|Motorsport|Delaware|United States}}

[[Category:2009 in Delaware|AAA 400]]
[[Category:2009 NASCAR Sprint Cup Series|AAA 400]]
[[Category:NASCAR races at Dover International Speedway]]