{{Taxobox
| name = Kitti's hog-nosed bat
| fossil_range = Recent
| image = Craseonycteris thonglongyai.JPG
| status = VU
| status_ref =<ref name=IUCN>{{IUCN2008|assessor=Bates, P.|assessor2=Bumrungsri, S.|assessor3=Francis, C.|last-assessor-amp=yes|year=2008|id=5481|title=Craseonycteris thonglongyai|downloaded=28 January 2009}} Listed as Vulnerable</ref>
| trend = down
| status_system = iucn3.1
| regnum = [[Animal]]ia
| phylum = [[Chordate|Chordata]]
| classis = [[Mammal]]ia
| ordo = [[Chiroptera]]
| familia = '''Craseonycteridae'''
| familia_authority = Hill, 1974
| genus = '''''Craseonycteris'''''
| genus_authority = Hill, 1974
| species = '''''C. thonglongyai'''''
| binomial = ''Craseonycteris thonglongyai''
| binomial_authority = Hill, 1974
| range_map = Kitti's Hog-nosed Bat area.png
| range_map_caption = Kitti's hog-nosed bat range
}}
'''Kitti's hog-nosed bat''' (''Craseonycteris thonglongyai''), also known as the '''bumblebee bat''', is a [[vulnerable species]] of [[bat]] and the only extant member of the family '''Craseonycteridae'''. It occurs in western [[Thailand]] and southeast [[Burma]], where it occupies [[limestone]] caves along rivers.

Kitti's hog-nosed bat is the smallest species of bat and arguably the world's [[Smallest organisms#Mammals|smallest mammal]]. It has a reddish-brown or grey coat, with a distinctive pig-like snout. Colonies range greatly in size, with an average of 100 individuals per cave. The bat feeds during short activity periods in the evening and dawn, foraging around nearby forest areas for insects. Females give birth annually to a single offspring.

Although the bat's status in Burma is not well known, the Thai population is restricted to a single province and may be at risk for extinction. Its potential threats are primarily anthropogenic, and include habitat degradation and the disturbance of roosting sites.<ref name=IUCN/>

==Description==
[[File:Kitti's hog-nosed bat Stuffed specimen.jpg|thumb|240px|left|Stuffed specimen at the [[National Museum of Nature and Science]], [[Tokyo]], [[Japan]].]]
Kitti's hog-nosed bat is about {{convert|abbr=on|29|to|33|mm|in}} in length and {{convert|abbr=on|2|g|oz}} in mass.<ref>Donati, Annabelle, and Pamela Johnson. "Which mammal is the smallest?." I wonder which snake is the longest: and other neat facts about animal records. Racine, Wis.: Western Pub. Co., 1993. 8. Print.</ref><ref name=Edge>{{Cite web | url = http://www.edgeofexistence.org/mammals/species_info.php?id=49 | title = Bumblebee bat (''Craseonycteris thonglongyai'') | work = [[EDGE Species]] | accessdate = 2008-04-10}}</ref> hence the common name of "[[bumblebee]] bat". It is the smallest species of bat and may be the world's smallest mammal, depending on how size is defined. The main competitors for the title are small [[shrew]]s; in particular, the [[Etruscan shrew]] may be lighter at {{convert|abbr=on|1.2|to|2.7|g|oz}} but is longer, measuring {{convert|abbr=on|36|to|53|mm|in}} from its head to the base of the tail.<ref name=MammalSociety>{{Cite web | url = http://www.abdn.ac.uk/mammal/smallest.shtml | title = Mammal record breakers: The smallest! | work = The Mammal Society | accessdate = 2008-04-10 |archiveurl = https://web.archive.org/web/20070713081111/http://www.abdn.ac.uk/mammal/smallest.shtml |archivedate = July 13, 2007}}</ref>

The bat has a distinctive swollen, pig-like snout<ref name=Edge/> with thin, vertical nostrils.<ref name=Chipterologica>{{cite journal|title=''Craseonycteris thonglongyai'' (Chiroptera: Craseonycteridae) is a rhinolophoid: molecular evidence from cytochrome ''b''|journal=Acta Chiropterologica|year=2002|author=Hulva|author2=Horáček|last-author-amp=yes|volume=4|issue=2|pages=107–120|doi=10.3161/001.004.0201}}</ref> Its ears are relatively large, while its eyes are small and mostly concealed by fur.<ref name=AnimalDiversity>Goswami, A. 1999.  [http://animaldiversity.ummz.umich.edu/site/accounts/information/Craseonycteris_thonglongyai.html ''Craseonycteris thonglongyai''], Animal Diversity Web. Retrieved on 11 April 2008.</ref> Its teeth are typical of an [[insectivore|insectivorous]] bat.<ref name=AnimalDiversity/> The [[dentition|dental formula]] is 1:1:1:3 in the upper jaw and 2:1:2:3 in the lower jaw,<ref name=Chipterologica/> with large upper incisors.<ref name=AnimalDiversity/>

The bat's upperparts are reddish-brown or grey, while the underside is generally paler.<ref name=AnimalDiversity/> The wings are relatively large and darker in colour, with long tips that allow the bat to hover.<ref name=Edge/> Despite having two caudal [[vertebra]]e, Kitti's Hog-nosed Bat has no visible tail.<ref name=AnimalDiversity/> There is a large web of skin between the hind legs (the [[patagium|uropatagium]]) which may assist in flying and catching insects, although there are no tail bones or [[calcar]]s to help control it in flight.<ref name=Edge/><ref name=AnimalDiversity/><ref name=AnimalDiversity2>Meyers, P. 1997.  [http://animaldiversity.ummz.umich.edu/site/topics/mammal_anatomy/bat_wings.html Bat Wings and Tails], Animal Diversity Web. Retrieved on 12 April 2008.</ref>

==Range and distribution==
Kitti's hog-nosed bat occupies the limestone caves along rivers, within dry [[evergreen]] or [[deciduous]] forests.<ref name=Edge/> In Thailand, Kitti's hog-nosed bat is restricted to a small region of the [[Tenasserim Hills]] in [[Sai Yok District]], [[Kanchanaburi Province]], within the [[drainage basin]] of the [[Khwae Noi River]].<ref name=Edge/><ref name=Oryx>{{cite journal|title=Status of the world's smallest mammal, the bumble-bee bat ''Craseonycteris thonglongyai'', in Myanmar|journal=Oryx|date=October 2006|last=MJR Pereira|volume=40|issue=4|pages=456–463|doi=10.1017/S0030605306001268|first1=Maria João Ramos|last2=Rebelo|first2=Hugo|last3=Teeling|first3=Emma C.|last4=O'Brien|first4=Stephen J.|last5=MacKie|first5=Iain|last6=Bu|first6=Si Si Hla|last7=Swe|first7=Khin Maung|last8=Khin|first8=Mie Mie|last9=Bates|first9=Paul J.J. |author-separator=|displayauthors=1}}</ref> While the [[Sai Yok National Park]] in the [[Dawna Hills]] contains much of the bat's range, some Thai populations occur outside the park and are therefore unprotected.<ref name=Edge/>

Since the 2001 discovery of a single individual in Burma, at least nine separate sites have been identified in the limestone outcrops of the Dawna and [[Karen Hills]] outside the [[Salween River|Thanlwin]], [[Ataran River|Ataran]], and [[Gyaing River]]s of [[Kayin State|Kayin]] and [[Mon State]]s.<ref name=Oryx/> The Thai and Burmese populations are [[morphology (biology)|morphologically]] identical, but their [[animal echolocation|echolocation]] calls are distinct.<ref name=Oryx/> It is not known whether the two populations are [[Reproductive isolation|reproductively isolated]].<ref name=Oryx/>

==Behaviour==
Kitti's hog-nosed bat roosts in the caves of limestone hills, far from the entrance. While many caves contain only 10 to 15 individuals, the average group size is 100, with a maximum of about 500. Individuals roost high on walls or roof domes, far apart from each other.<ref name=IUCNpdf/> Bats also undertake seasonal migration between caves.

Kitti's hog-nosed bat has a brief activity period, leaving its roost for only 30 minutes in the evening and 20 minutes at dawn. These short flights are easily interrupted by heavy rain or cold temperatures.<ref name=IUCNpdf>Hutson, A. M., Mickleburgh, S. P. and Racey, P. A. (Compilers). 2001. [http://www.iucn.org/themes/ssc/actionplans/microchiropteranbats/microchiroptera.html Microchiropteran Bats: Global Status Survey and Conservation Action Plan]. IUCN/SSC Chiroptera Specialist Group. IUCN: Gland, Switzerland.</ref> During this period, the bat forages within fields of [[cassava]] and [[Kapok tree|kapok]] or around the tops of [[bamboo]] clumps and [[teak]] trees, within one kilometre of the roosting site.<ref name=Edge/><ref name=IUCNpdf/> The wings seem to be shaped for hovering flight, and the gut contents of specimens include spiders and insects that are presumably gleaned off foliage. Nevertheless, most prey is probably caught in flight.<ref name=IUCNpdf/> Main staples of the bat's diet include small [[fly|flies]] ([[Chloropidae]], [[Agromyzidae]], and [[Anthomyiidae]]), [[hymenoptera]]ns, and [[psocoptera]]ns.<ref name=IUCNpdf/>

Late in the dry season (around April) of each year, females give birth to a single offspring. During feeding periods, the young either stays in the roost or remains attached to the mother at one of her two [[Vestigiality|vestigial]] pubic nipples.<ref name=AnimalDiversity/><ref name=IUCNpdf/>

==Taxonomy==
Kitti's hog-nosed bat is the only extant species in the family Craseonycteridae, which is grouped in the superfamily Rhinolophoidea as a result of molecular testing. Based on this determination, the bat's closest relatives are members of the families [[Hipposideridae]] and [[Rhinopomatidae]].<ref name=Chipterologica/>

Kitti's hog-nosed bat was unknown to the world at large prior to 1974. Its common name refers to its discoverer, Thai zoologist [[Kitti Thonglongya]]. Thonglongya worked with a British partner, [[John Edward Hill|John E. Hill]], in classifying bats of Thailand; after Thonglongya died suddenly in February 1974, Hill formally described the species, giving it the binomial name ''Craseonycteris thonglongyai'' in honour of his colleague.<ref name=Mammalian>{{cite journal|title=Craseonycteris thonglongyai|journal=Mammalian Species|date=1981-12-03|author=J. E. Hill|author2=Susan E. Smith|last-author-amp=yes|volume=160|pages=1–4|doi=10.2307/3503984}}</ref><ref name=JoM>{{cite journal|title=Kitti Thonglongya, 1928-1974|journal=Journal of Mammalogy|date=February 1975|last=Schlitter|first=Duane A.|volume=56|issue=1|pages=279–280|doi=10.2307/1379641}}</ref>

==Conservation==
As of the species' most recent review in 2008, Kitti's hog-nosed bat is listed by the [[World Conservation Union|IUCN]] as [[Vulnerable species|vulnerable]], with a downward population trend.<ref name=IUCN/>

Soon after the bat's discovery in the 1970s, some roosting sites became disturbed as a result of tourism, scientific collection, and even the collection and sale of individuals as souvenirs. However, these pressures may not have had a significant effect on the species as a whole, since many small colonies exist in hard-to-access locations, and only a few major caves were disturbed. Another potential risk is the activity of local monks, who have occupied roost caves during periods of meditation.<ref name=IUCNpdf/>

Currently, the most significant and long-term threat to the Thai population could be the annual burning of forest areas, which is most prevalent during the bat's breeding season. In addition, the proposed construction of a pipeline from Burma to Thailand may have a negative impact.<ref name=IUCNpdf/> Threats to the Burmese population are not well known.<ref name=Edge/>

In 2007, Kitti's hog-nosed bat was identified by the [[EDGE Species|Evolutionarily Distinct and Globally Endangered]] <!-- (EDGE) --> project as one of its Top 10 "focal species".<ref>{{Cite news | title = Protection for 'weirdest' species | url = http://news.bbc.co.uk/2/hi/science/nature/6263331.stm | date = 2007-01-16 | accessdate = 2007-05-22 | work = [[BBC]]}}</ref>

==References==
{{reflist|30em}}

==External links==
{{commons category|Craseonycteris thonglongyai}}
*EDGE of Existence [http://www.edgeofexistence.org/mammals/species_info.php?id=49 ''(Bumblebee bat)''] - Saving the World's most Evolutionarily Distinct and Globally Endangered (EDGE) species
*[http://animaldiversity.ummz.umich.edu/site/accounts/information/Craseonycteris_thonglongyai.html Information and image at ADW]

{{Chiroptera}}

{{good article}}

{{DEFAULTSORT:Bat, Kitti's Hog-nosed}}
[[Category:Animals described in 1974|Kitti's Hog-nosed Bat]]
[[Category:Bats of Asia|Kitti's Hog-nosed Bat]]
[[Category:Mammals of Burma]]
[[Category:Mammals of Southeast Asia]]
[[Category:Mammals of Thailand]]
[[Category:EDGE species]]