{{About|the football championship game|other games|List of ACC Championship Games}}
{{Use mdy dates|date=October 2016}}
{{Infobox NCAA conference tournament
| name               = ACC Football Championship Game
| optional_subheader = Conference Football Championship
| defunct            = 
| image              =
| caption            = ACC Football Championship Game logo
| sport              = [[College football]]
| conference         = [[Atlantic Coast Conference]]
| number_of_teams    = <!-- Only 2 teams, so it likely does not need to be listed here -->
| current_stadium    = [[Camping World Stadium]]<!--Per template instructions, this is the MOST RECENT location-->
| current_location   = [[Orlando, Florida]]
| years              = 2005–present
| most_recent        = [[2016 ACC Championship Game|2016]]
| current_champion   = [[2016 Clemson Tigers football team|Clemson]]
| most_championships = [[Florida State Seminoles football|Florida State]] (4)
| television         = [[American Broadcasting Company|ABC]]/[[ESPN]]
| website            = [http://www.theacc.com/sports/m-footbl/acc-m-footbl-body.html TheACC.com Football]
| sponsors           = [[Dr Pepper]] (2005–present)
| all_stadiums =[[Camping World Stadium]] (2016)<br />[[Bank of America Stadium]] (2010–2015)<br />[[Raymond James Stadium]] (2008–2009)<br />[[Jacksonville Municipal Stadium]] (2005–2007)
| all_locations      = [[Orlando, Florida]] (2016)<br />[[Charlotte, North Carolina]] (2010–2015)<br />[[Tampa, Florida]] (2008–2009)<br />[[Jacksonville, Florida]] (2005–2007)
}}
The '''Dr Pepper ACC Football Championship Game''' is an American [[college football]] game held on the first Saturday in December by the [[Atlantic Coast Conference]] (ACC) each year to determine [[List of Atlantic Coast Conference football champions|its football champion]]. The game pits the champion of the Coastal Division against the champion of the Atlantic Division in a game that follows the conclusion of the regular season. The game's corporate sponsor is [[Dr Pepper]]. The current champion are the [[2015 Clemson Tigers football team|Clemson Tigers]] of the Atlantic Division.

==History==
Before the [[2004 NCAA Division I-A football season|2004 college football season]], the Atlantic Coast Conference determined its champion via round-robin play during the course of the regular season and there was no conference championship game. In 2004, the Atlantic Coast conference added two teams—[[Virginia Tech Hokies football|Virginia Tech]] and [[Miami Hurricanes football|Miami]]—expanding the league to 11 teams. At the time, college football teams were limited by the NCAA to 11&nbsp;regular-season games, three or four of which typically featured teams outside the home team's conference. Following the 2004&nbsp;season, the league added a 12th team—[[Boston College Eagles football|Boston College]]—and became eligible to hold a championship game at the conclusion of [[2005 NCAA Division I-A football season|the 2005 season]].

The conference was divided into two divisions of six&nbsp;teams each. The team with the best conference record in each division is selected to participate in the championship game. In the inaugural championship game, which took place at the end of the 2005 college football season, the [[Florida State Seminoles football|Florida State Seminoles]] defeated Virginia Tech 27–22 at [[Alltel Stadium]] in [[Jacksonville, Florida|Jacksonville]], Florida. In the 2006 game, two&nbsp;other teams faced off as [[Georgia Tech Yellow Jackets football|Georgia Tech]] played [[2006 Wake Forest Demon Deacons football team|Wake Forest]]. Wake defeated Georgia Tech 9–6. For the 2007 game, Jacksonville was awarded a one-year extension as host, and the game remained in Jacksonville. Virginia Tech returned to the ACC Football Championship game and faced off against [[Boston College Eagles football|Boston College]]. Tech won the game, 30–16, and returned to the championship in 2008 to defeat Boston College again 30–12. In 2009, [[Georgia Tech Yellow Jackets football|Georgia Tech]] defeated [[Clemson Tigers football|Clemson]], 39–34, but was forced to vacate the ACC championship by the NCAA.

Following the 2007 game the Gator Bowl Committee—organizers of the ACC Football Championship game in Jacksonville—announced they would not seek another contract extension due to falling attendance. With Jacksonville's withdrawal from future [[site selection]], the ACC selected [[Tampa, Florida|Tampa]], Florida and [[Charlotte, North Carolina|Charlotte]], North Carolina as future sites of the game. The 2008 and 2009 games were held in Tampa, while the 2010&nbsp;and 2011&nbsp;games were held in Charlotte. In 2008, the Coastal Division champion was the designated "home" team.

==Conference expansion==
In 1990, the eight-team Atlantic Coast Conference added Florida State to the league, creating a new nine-team ACC.<ref>"FSU to Battle for ACC Titles." Wire and Staff Reports, ''Philadelphia Daily News''. September 15, 1990. Page 45.</ref> Though Florida State was the only school added to the conference, some league officials discussed offering one or more other schools—[[Navy Midshipmen football|Navy]], [[Pittsburgh Panthers football|Pittsburgh]], [[Syracuse Orange football|Syracuse]], [[South Carolina Gamecocks football|South Carolina]], Miami, [[West Virginia Mountaineers football|West Virginia]], Boston College, [[Rutgers Scarlet Knights football|Rutgers]], or Virginia Tech—an offer to join the league.<ref>"ACC Considers 10 in Expansion Plans." Dan Caesar, ''St. Louis Post-Dispatch''. July 27, 1990. Page 2D.</ref> For various reasons, however, no other team was extended an offer. Throughout the 1990s, the Atlantic Coast Conference remained at nine members. Ironically, South Carolina was a charter member of the ACC that left in 1971.

The nearby [[Southeastern Conference]] (SEC), which also encompasses college football teams in the [[Southern United States|American South]], also expanded in 1990. Instead of adding one team, as did the ACC, the then 10-team SEC added two—the [[University of Arkansas]]<ref>[https://query.nytimes.com/gst/fullpage.html?res=9C0CE0DB1E3AF932A05754C0A966958260 Arkansas Set to Join S.E.C.] The Associated Press, The New York Times. July 31, 1990. Accessed March 13, 2008.</ref> and the [[University of South Carolina]].<ref>[https://query.nytimes.com/gst/fullpage.html?res=9C0CEFDA1739F935A1575AC0A966958260 South Carolina Joins the S.E.C.] The Associated Press, The New York Times. September 26, 1990. Accessed March 13, 2008.</ref> The expansion made the SEC the first 12-school football conference and thus the first eligible to hold a conference championship game under NCAA rules (the first game was held in 1992).<ref name="SEC">[http://www.secsports.com/index.php?change_well_id=9993&s About the Southeastern Conference] Accessed March 13, 2008. {{webarchive |url=https://web.archive.org/web/20071209184546/http://www.secsports.com/index.php?change_well_id=9993&s |date=December 9, 2007 }}</ref> The SEC enjoyed increased television ratings and revenue through the 1990s and by 2003 was earning over $100&nbsp;million annually, with revenues shared out among member schools.<ref name="ACCSEC">[http://www.centralohio.com/ohiostate/stories/20030708/football/611709.html ACC expansion doesn't concern members of SEC] Tim Vacek, Gannett News Service, centralohio.com. July 8, 2003. Accessed March 13, 2008.</ref>

Officials of other leagues took note of the financial boon that followed SEC expansion to twelve teams. Atlantic Coast Conference representatives began discussing expansion to twelve schools in the first years of the new century,<ref>[http://www.pittsburghlive.com/x/pittsburghtrib/s_137540.html Remote control: TV money a driving force for ACC expansion] Joe Starkey, ''Pittsburgh Tribune-Review''. June 1, 2003. Accessed March 13, 2008.</ref> who began publicly pursuing the possibility of expansion anew in 2003. On May 13, 2003, representatives voted in favor of extending invitations to three schools. The only certain school was the [[University of Miami]], while the other two spots were still being debated.<ref>[http://static.espn.go.com/ncaa/news/2003/0513/1553257.html ACC to ask Miami, two others to join conference] ESPN.com, May 13, 2003. Accessed March 9, 2009.</ref> Initially, the league favored admitting Miami, [[Syracuse University]], and [[Boston College]].<ref>[http://sportsillustrated.cnn.com/football/college/news/2003/05/15/saving_bigeast_ap/ At Miami's Mercy] The Associated Press, CNNSI.com. May 15, 2003. Accessed March 13, 2008.</ref> After a month of debate, however, the ACC elected to extend formal invitations to Miami, Boston College, and [[Virginia Polytechnic Institute and State University|Virginia Tech]], which joined after initially being overlooked.<ref>[http://www.hokiesports.com/wrestling/recaps/06272003aaa.html President Steger Regarding ACC Acceptance] Charles Steger, Hokiesports.com. June 27, 2003. Accessed March 13, 2008.</ref> This came years after these schools were considered for ACC membership in the early 1990s but nothing had ever came to fruition. Pittsburgh and Syracuse would also eventually join the ACC after rejections in 1990 and 2003, becoming members in 2013.

Miami and Virginia Tech began official ACC play with the [[2004 NCAA Division I-A football season|2004 college football season]].<ref>[http://nbcsports.msnbc.com/id/5335884/ Miami, Virginia Tech quietly join ACC] The Associated Press, MSNBC.com. July 2, 2004. Accessed March 13, 2008.</ref> After the league settled a lawsuit resulting from the departure of the three former [[Big East Conference (1979–2013)|Big East Conference]] teams,<ref>[http://sports.espn.go.com/ncaa/news/story?id=2052787 Conferences schedule games as part of settlement] The Associated Press, ESPN.com. May 4, 2005. Accessed March 13, 2008.</ref> Boston College began ACC play in [[2005 NCAA Division I-A football season|the 2005 season]].<ref>[http://www.washingtonpost.com/wp-dyn/content/article/2005/08/09/AR2005080901685.html After Ugly Breakup, BC Hopes for Fast Start in ACC] Mark Schlabach, ''The Washington Post''. August 10, 2005; Page E04. Accessed March 13, 2008.</ref> With the league officially at 12 teams, it became eligible to hold a conference championship football game.

==Site selection==
Even before the announcement proclaiming the ACC's expansion to 12&nbsp;teams, several cities and sports organizations were preparing bids to host the ACC Football Championship Game. The prospect of tens of thousands of visitors could provide a multimillion-dollar economic boost for a host city and region while requiring few, if any, additional facilities. One early contender was the city of Charlotte, North Carolina. Even before Virginia Tech, Miami, and Boston College were chosen as the ACC's picks to expand, Carolinas Stadium Corporation, the owner and operator of Charlotte's [[Bank of America Stadium|Ericsson Stadium]] (as it was called then) lobbied heavily for Charlotte's selection.<ref>"Charlotte wants title game." David Scott, ''The Charlotte Observer''. May 15, 2003. Page C3.</ref> Other early options included [[Orlando]], [[Tampa]], [[Atlanta]], and [[Jacksonville]].<ref>[http://www.fanblogs.com/acc/002921.php Nine cities vie for ACC Championship game] Kevin Donahue, fanblogs.com. May 10, 2004. Accessed April 24, 2008.</ref><ref>[http://www.theacc.com/sports/m-footbl/spec-rel/051104aab.html ACC Looks for Title-Game Host] The Associated Press, theACC.com. May 10, 2004. Accessed May 3, 2008.</ref><ref>"Tampa seeks to host ACC football championship". Doug Carlson, ''The Tampa Tribune''. January 29, 2004. Accessed May 9, 2008.</ref>

Shortly after negotiations for the location of the game began during the spring of 2004, the ACC announced that it had signed a new, seven-year television contract with [[American Broadcasting Company|ABC-TV]] and [[ESPN]].<ref>[http://www.theacc.com/sports/m-footbl/spec-rel/051204aaa.html ACC Reaches New Football Agreement With ABC Sports, ESPN] The Atlantic Coast Conference, theACC.com. May 12, 2004. Accessed May 3, 2008.</ref> As part of the deal, the ACC would earn over $40&nbsp;million in revenue a year in exchange for the networks' exclusive right to televise the ACC Football Championship Game along with several high-profile regular season games. Revenues would be divided among the 12 ACC member schools.<ref>[http://www.theacc.com/sports/m-footbl/spec-rel/051204aad.html Bigger League Means Bigger Money for Expanding ACC] Eddie Pells, the Associated Press, theACC.com. May 12, 2004. Accessed May 3, 2008.</ref>

In July 2004 the ACC began deliberations about which bid to accept.<ref>[http://www.theacc.com/sports/m-footbl/spec-rel/070104aaa.html ACC Sub-Committee Gathers For Site Selection Of 2005 ACC Football Championship Game] The Atlantic Coast Conference, theACC.com. July 1, 2004. Accessed May 3, 2008.</ref> On August 19, 2004, league officials announced that Jacksonville would host the game in 2005 and 2006. The league would then have the option to re-select Jacksonville for an additional one or two-year contract. Charlotte was the first runner-up in the competition.<ref>[http://sportsillustrated.cnn.com/2004/football/ncaa/08/19/bc.fbc.accchampionship.ap/ Jacksonville to host ACC championship game] The Associated Press, SI.com. August 19, 2004. Accessed April 24, 2008.</ref>

For its first three&nbsp;years, the championship game was held at [[EverBank Field]] (known as ''Alltel Stadium'' in 2005 and 2006 and ''Jacksonville Municipal Stadium'' in 2007). That contract expired after the [[2007 NCAA Division I-A football season|2007 season]].<ref>{{cite web|url=http://www.hokiesports.com/football/recaps/2007026aab.html|title=Jacksonville to host 2007 ACC football title game|date=February 6, 2007|publisher=hokiesports.com}}</ref> 
In December 2007, the ACC awarded the next four games to Tampa (first two) and Charlotte (next two).  [[Raymond James Stadium]] was the venue for the Tampa games in 2008 and 2009, while the [[Bank of America Stadium]] provided the venue for the Charlotte games in 2010 and 2011.<ref>{{cite news | url=http://www.wral.com/sports/story/2161744/ | title=ACC Football Title Games to Tampa, Charlotte | publisher=[[WRAL-TV|WRAL]].com | date=December 12, 2007 | accessdate=December 12, 2007}}</ref> Charlotte hosted the game again in 2012 and 2013. In February 2014 it was announced that Charlotte would continue to host the game through at least 2019.<ref>{{cite news | url=http://www.theacc.com/#!/news-detail/ACC-Football-Six-More-Years_02-28-14_7vvn51 | title=ACC, Charlotte look ahead to even better things | publisher=[[Atlantic Coast Conference|theacc]].com | date=February 28, 2014 | accessdate=February 28, 2014}}</ref> However, in response to North Carolina's [[Public Facilities Privacy & Security Act]] (HB2), the ACC voted in September 2016 to move the 2016 championship out of North Carolina.<ref name="historically bad">{{cite news | url=http://www.wralsportsfan.com/acc-pulls-championships-including-football-game-from-nc/16014297/ | title='Historically bad:' ACC pulls championships from NC | publisher=[[WRAL-TV|WRAL.com]] | date=September 14, 2016}}</ref>

==Team selection==
{{Location map+ | USA | width=450 | caption=[[File:Blue pog.svg|10px]] – Atlantic division<br>[[File:Red pog.svg|10px]] – Coastal division<br>[[File:Green pog.svg|10px]] – 2016 Championship Game site | relief=yes | places=
   {{Location map~ | USA | label=[[Boston College]] | position=right | mark=Blue pog.svg | link=Boston College | lat=42.2063 | long=-71.10133 }}
   {{Location map~ | USA | label=[[Clemson University|Clemson]] | position=left | mark=Blue pog.svg | link=Clemson University | lat=34.4042 | long=-82.5021 }}
   {{Location map~ | USA | label=[[Duke University|Duke]] | | position=top |mark=Red pog.svg | link=Duke University | lat=36.04 | long=-78.5620 }}
   {{Location map~ | USA | label=[[Florida State University|Florida State]] | position=right | mark=Blue pog.svg | link=Florida State University | lat=30.442 | long=-84.298 }}
   {{Location map~ | USA | label=[[Georgia Institute of Technology|Georgia Tech]] | position=left | mark=Red pog.svg | link=Georgia Institute of Technology | lat=33.4633 | long=-84.2341 }}
   {{Location map~ | USA | label=[[University of Louisville|Louisville]] | position=left | mark=Blue pog.svg | link=University of Louisville | lat=38.125407 | long=-85.453680 }}
   {{Location map~ | USA | label=[[University of Miami|Miami]] | position=right | mark=Red pog.svg | link=University of Miami | lat=25.721644 | long=-80.279267 }}
   {{Location map~ | USA  | label=[[University of North Carolina at Chapel Hill|North Carolina]] | position=bottom | mark=Red pog.svg | link=University of North Carolina at Chapel Hill | lat=35.5430 | long=-79.30 }}
   {{Location map~ | USA | label=[[North Carolina State University|NC State]] | position=right | mark=Blue pog.svg | link=North Carolina State University | lat=35.786 | long=-78.682 }}
   {{Location map~ | USA | label=[[University of Pittsburgh|Pittsburgh]] | position=top | mark=Red pog.svg | link=University of Pittsburgh | lat=40.2640 | long=-79.5712 }}
   {{Location map~ | USA | label=[[Syracuse University|Syracuse]] | position=left | mark=Blue pog.svg | link=Syracuse University | lat=43.03767 | long=-76.13399 }}
   {{Location map~ | USA | label=[[University of Virginia|Virginia]] | position=right | mark=Red pog.svg | link=University of Virginia | lat=38.035 | long=-78.505 }}
   {{Location map~ | USA | label=[[Virginia Tech]] | position=top | mark=Red pog.svg | link=Virginia Tech | lat=37.135 | long=-80.255 }}
   {{Location map~ | USA | label=[[Wake Forest University|Wake Forest]] | position=left | mark=Blue pog.svg | link=Wake Forest University | lat=36.135 | long=-80.277 }}
   {{Location map~ | USA | mark=Green pog.svg | link=Camping World Stadium | lat=28.539 | long=-81.403 }}
}}

Following the absorption of Virginia Tech and Miami into the ACC, questions arose about how an 11-team league could fairly select participants in the conference championship game.<ref>[http://www.theacc.com/genrel/070203aaa.html Transcript of Tuesday's Press Conference] {{webarchive |url=https://web.archive.org/web/20120213172740/http://www.theacc.com/genrel/070203aaa.html |date=February 13, 2012 }} The Atlantic Coast Conference, theacc.com. July 1, 2003. Accessed March 14, 2008.</ref> A divisional structure involving two six-team divisions competing for two championship-game slots would not be possible. In addition, the ACC could not continue to select its champion via round-robin play since there were now 11 teams and only seven or eight conference games available per team. Even the NCAA's addition of a 12th game to the regular season did little to relieve the conference's problem.<ref>[http://www.washingtonpost.com/wp-dyn/content/article/2005/04/28/AR2005042801872.html College Football Gets 12th Game] Liz Clarke, ''The Washington Post''. April 29, 2005. Accessed May 9, 2008.</ref> Prior to the 2004&nbsp;college football season, the ACC requested a waiver to the NCAA's rule requiring conferences to have 12-plus teams before having a conference championship game. Before the season began, however, the NCAA rejected the ACC's application,<ref>[http://sports.espn.go.com/ncf/news/story?id=1622429 Formatting league still up for discussion] Scripps Howard News Service, ESPN.com. September 24, 2008. Accessed May 9, 2008.</ref> and the league had to use a semi-round-robin format to select a champion during the 2004 football season. After that season, the inclusion of Boston College as the ACC's 12th team solved the problem of enabling the ACC to have a championship football game.

On October 18, 2004, the ACC announced its new football structure with two divisions. Each six-team division plays a [[Round-robin tournament|round-robin]] schedule within the division and a rotation of three conference games against teams from the opposing division. The two teams with the best conference records in each division earn places to the championship game.<ref>[http://www.theacc.com/genrel/101804aaa.html ACC Unveils Future League Seal, Divisional Names] The Atlantic Coast Conference, theacc.com. October 18, 2004. Accessed March 14, 2008.</ref> In the event of a tie in records within one division, divisional records and the results of head-to-head games are considered.<ref>[http://www.theacc.com/sports/m-footbl/spec-rel/082105aad.html Atlantic Coast Conference Football Divisional Tiebreaker] The Atlantic Coast Conference, theACC.com. Accessed May 9, 2008.</ref>

Also, in the games between the two divisions, each team has a permanent rival team that is played every year. Hence, every year, there are these football games: 
Georgia Tech vs. Clemson; North Carolina vs. North Carolina State; Louisville vs. Virginia; Syracuse vs. Pittsburgh; Duke vs. Wake Forest; Florida State vs. Miami; and Boston College vs. Virginia Tech.

Notre Dame joined the conference as a non-divisional member in 2014 and, while playing five ACC teams each season, is not eligible for the championship game.<ref>{{cite web|title=Notre Dame sets ACC schedule for 2014–16|author=Chip Patterson|publisher=CBSSports.com|date=December 20, 2013|accessdate=April 28, 2014|url=http://www.cbssports.com/collegefootball/eye-on-college-football/24381940/notre-dame-sets-acc-schedule-for-2014-16}}</ref>

===Divisions===
{{col-begin}}
{{col-break}}
'''Atlantic Division'''
*[[Boston College Eagles football|Boston College Eagles]]
*[[Clemson Tigers football|Clemson Tigers]]
*[[Florida State Seminoles football|Florida State Seminoles]]
*[[Louisville Cardinals football|Louisville Cardinals]]
*[[North Carolina State Wolfpack football|North Carolina State Wolfpack]]
*[[Syracuse Orange football|Syracuse Orange]]
*[[Wake Forest Demon Deacons football|Wake Forest Demon Deacons]]
{{col-break}}
'''Coastal Division'''
*[[Duke Blue Devils football|Duke Blue Devils]]
*[[Georgia Tech Yellow Jackets football|Georgia Tech Yellow Jackets]]
*[[Miami Hurricanes football|Miami Hurricanes]]
*[[North Carolina Tar Heels football|North Carolina Tar Heels]]
*[[Pittsburgh Panthers football|Pittsburgh Panthers]]
*[[Virginia Cavaliers football|Virginia Cavaliers]]
*[[Virginia Tech Hokies football|Virginia Tech Hokies]]
{{col-end}}
'''Non-divisional:''' 
*[[Notre Dame Fighting Irish football|Notre Dame]]

==Results==
{| class="wikitable sortable"
|-
!Year
!Atlantic Division
!Score
!Coastal Division 
!Venue, Location 
!Ticket Sales
!MVP
|-
|align=center|[[2005 ACC Championship Game|2005]]
|#22 '''[[2005 Florida State Seminoles football team|Florida State]]'''
|align=center|27–22
|#5 [[2005 Virginia Tech Hokies football team|Virginia Tech]]
|[[EverBank Field|Alltel Stadium]], [[Jacksonville, Florida]] 
|align=center|72,749
|[[Willie Reid (American football)|Willie Reid]] (Florida State)
|-
|align=center|[[2006 ACC Championship Game|2006]]
|#16 '''[[2006 Wake Forest Demon Deacons football team|Wake Forest]]'''
|align=center|9–6
|#22 [[2006 Georgia Tech Yellow Jackets football team|Georgia Tech]]
|Alltel Stadium, Jacksonville, Florida
|align=center|62,850
|[[Sam Swank]] (Wake Forest)
|-
|align=center|[[2007 ACC Championship Game|2007]]
|#12 [[2007 Boston College Eagles football team|Boston College]]
| align=center|16–30
|#5 '''[[2007 Virginia Tech Hokies football team|Virginia Tech]]'''
|Jacksonville Municipal Stadium, Jacksonville, Florida
|align=center|53,212
|[[Sean Glennon]] (Virginia Tech)
|-
|align=center|[[2008 ACC Championship Game|2008]]
|#20 [[2008 Boston College Eagles football team|Boston College]]
|align=center|12–30
|#25 '''[[2008 Virginia Tech Hokies football team|Virginia Tech]]'''
|[[Raymond James Stadium]], [[Tampa, Florida]] 
|align=center|53,927 
|[[Tyrod Taylor]] (Virginia Tech)
|-
|align=center|[[2009 ACC Championship Game|2009]]
|#25 [[2009 Clemson Tigers football team|Clemson]]
|align=center|34–39*
|#10 '''[[2009 Georgia Tech Yellow Jackets football team|Georgia Tech]]'''*
|Raymond James Stadium, Tampa, Florida
|align=center|44,897
|[[C. J. Spiller]] (Clemson)
|-
|align=center|[[2010 ACC Championship Game|2010]]
|#20 [[2010 Florida State Seminoles football team|Florida State]]
|align=center| 33–44
|#11 '''[[2010 Virginia Tech Hokies football team|Virginia Tech]]'''
|[[Bank of America Stadium]], [[Charlotte, North Carolina]] 
|align=center|72,379
|[[Tyrod Taylor]] (Virginia Tech)
|-
|align=center|[[2011 ACC Championship Game|2011]]
|#21 '''[[2011 Clemson Tigers football team|Clemson]]'''
| align=center|38–10
|#5 [[2011 Virginia Tech Hokies football team|Virginia Tech]]
|Bank of America Stadium, Charlotte, North Carolina
|align=center|73,675
|[[Tajh Boyd]] (Clemson)
|-
|align=center|[[2012 ACC Championship Game|2012]]
|#12 '''[[2012 Florida State Seminoles football team|Florida State]]'''
|align=center| 21–15
|[[2012 Georgia Tech Yellow Jackets football team|Georgia Tech]]
|Bank of America Stadium, Charlotte, North Carolina
|align=center|64,778
|[[James Wilder, Jr.]] (Florida State)
|-
|align=center|[[2013 ACC Championship Game|2013]]
|#1 '''[[2013 Florida State Seminoles football team|Florida State]]'''
|align=center|45–7
|#20 [[2013 Duke Blue Devils football team|Duke]]
|Bank of America Stadium, Charlotte, North Carolina
|align=center|67,694
|[[Jameis Winston]] (Florida State)
|-
|align=center|[[2014 ACC Championship Game|2014]]
|#2 '''[[2014 Florida State Seminoles football team|Florida State]]'''
|align=center|37–35
|#12 [[2014 Georgia Tech Yellow Jackets football team|Georgia Tech]]
|Bank of America Stadium, Charlotte, North Carolina
|align=center|64,808
|[[Dalvin Cook]] (Florida State)
|-
|align=center|[[2015 ACC Championship Game|2015]]
|#1 '''[[2015 Clemson Tigers football team|Clemson]]'''
|align=center|45–37
|#8 [[2015 North Carolina Tar Heels football team|North Carolina]]
|Bank of America Stadium, Charlotte, North Carolina
|align=center|74,514
|[[Deshaun Watson]] (Clemson)
|-
|align=center|[[2016 ACC Championship Game|2016]]
|#3 '''[[2016 Clemson Tigers football team|Clemson]]'''
|align=center|42–35
|#19 [[2016 Virginia Tech Hokies football team|Virginia Tech]]
|[[Camping World Stadium]], [[Orlando, Florida]]<ref> {{cite web |url=http://www.theacc.com/news/dr-pepper-acc-football-championship-game-set-for-orlando-09-29-2016|title=Dr Pepper ACC Football Championship Game Set For Orlando |publisher=theacc.com |date= }}</ref>
|align=center|50,628
|[[Deshaun Watson]] (Clemson)
|-
|}
<small>Winners are listed in bold. &nbsp; Rankings are from the [[Coaches Poll]] released prior to the game. <br>
<nowiki>*</nowiki>Georgia Tech was forced to vacate this win due to NCAA violations.<ref name="espn">{{cite web|last=Dinich |first=Heather |url=http://espn.go.com/blog/acc/post/_/id/26415/final-verdict-on-2009-acc-title-game-no-winner |title=Verdict on 2009 ACC title game: No winner – ACC Blog – ESPN |publisher=Espn.go.com |date= |accessdate=2012-11-28}}</ref>
</small>

==Results by team==
{| class="wikitable sortable"
|-
!Appearances
!School
!Wins 
!Losses 
!Pct.
!Year(s) Won
|-
|align=center|6
|[[Virginia Tech Hokies football|Virginia Tech]]
|align=center|3
|align=center|3
|align=center|{{winpct|3|3}}
|2007, 2008, 2010
|-
|align=center|5
|[[Florida State Seminoles football|Florida State]]
|align=center|4
|align=center|1
|align=center|{{winpct|4|1}}
|2005, 2012, 2013, 2014
|-
|align=center|4
|[[Clemson Tigers football|Clemson]]
|align=center|3
|align=center|1
|align=center|{{winpct|3|1}}
|2011, 2015, 2016
|-
|align=center|4
|[[Georgia Tech Yellow Jackets football|Georgia Tech]]
|align=center|{{sort|0.9|1*}}
|align=center|3
|align=center|{{winpct|0|3}}
|''2009''
|-
|align=center|2
|[[Boston College Eagles football|Boston College]]
|align=center|0
|align=center|2
|align=center|{{winpct|0|2}}
|
|-
|align=center|1
|[[Wake Forest Demon Deacons football|Wake Forest]]
|align=center|1
|align=center|0
|align=center|{{winpct|1|0}}
|2006
|-
|align=center|1
|[[Duke Blue Devils football|Duke]]
|align=center|0
|align=center|1
|align=center|{{winpct|0|1}}
|
|-
|align=center|1
|[[North Carolina Tar Heels football|North Carolina]]
|align=center|0
|align=center|1
|align=center|{{winpct|0|1}}
|
|-
|}

<small><nowiki>*</nowiki>Georgia Tech's win in 2009 was later vacated.</small>

* [[Louisville Cardinals football|Louisville]], [[Miami Hurricanes football|Miami]], [[North Carolina State Wolfpack football|North Carolina State]], [[Pittsburgh Panthers football|Pittsburgh]], [[Syracuse Orange football|Syracuse]], and [[Virginia Cavaliers football|Virginia]] have yet to make an appearance in an ACC Football Championship Game.

==See also==
{{Wikipedia books|Atlantic Coast Conference football championship games}}
{{Portal|ACC}}
[[List of NCAA Division I FBS Conference Championship games]]

==References==
{{Reflist|2}}

==External links==
* {{Official website|http://www.accchampionship.com}}

{{ACC Championship Game navbox}}
{{Atlantic Coast Conference football navbox}}
{{Atlantic Coast Conference championships navbox}}
{{NCAA Division I FBS conference championship game navbox}}

{{good article}}

[[Category:ACC Championship Game|*]]
[[Category:Recurring sporting events established in 2005]]