{{Use dmy dates|date=January 2014}}
{{Infobox journal
| title = Journal of the Royal Society of Medicine
| cover = 
| editor = [[Kamran Abbasi]]
| discipline = [[Medicine]]
| abbreviation = J. R. Soc. Med.
| formernames = Proceedings of the Royal Society of Medicine, Medico-Chirurgical Transactions
| publisher = [[Sage Publications]]
| country = United Kingdom
| frequency = Monthly
| history = 1809-present
| openaccess = [[Hybrid open access journal|Hybrid]]
| impact = 2.118
| impact-year = 2014
| website = https://www.sagepub.com/en-us/nam/journal-of-the-royal-society-of-medicine/journal202191
| link1 = http://jrs.sagepub.com/content/current
| link1-name = Online access
| link2 = http://jrs.sagepub.com/content/by/year
| link2-name = Online archive
| link3 = http://www.ncbi.nlm.nih.gov/pmc/journals/256/
| link3-name = Online archive at the [[National Library of Medicine]]
| JSTOR = 
| OCLC = 03722674
| LCCN = 78648718
| CODEN = JRSMD9
| ISSN = 0141-0768
| eISSN = 1758-1095
| ISSN2 = 0959-5287
| ISSN2label = ''Medico-Chirurgical Transactions'':
}}
The '''''Journal of the Royal Society of Medicine''''' is an [[open peer review|open peer-reviewed]] [[medical journal]]. It is the flagship journal of the [[Royal Society of Medicine]] with full [[editorial independence]]. Its continuous publication history dates back to 1809.<ref name=Dacie1978>{{citation |date=January 1978 |author=Dacie, John |title=Journal of the Royal Society of Medicine |volume=71 |issue=1 |page=4 |pmc=1436428 |pmid=20894216 |deadurl=no |journal=J R Soc Med}}</ref> Since July 2005 the [[editor-in-chief]] is [[Kamran Abbasi]],<ref name=JRSMAuthorGuidlines/> who succeeded Robin Fox who was editor for almost 10 years.<ref name=FoxAug2005/>

==History==
The journal was established in 1806 as the ''Medico-Chirurgical Transactions'' published by the [[Royal Medical and Chirurgical Society of London]]. It was renamed to ''Proceedings of the Royal Society of Medicine'' in 1907, following the merger that led to the formation of the Royal Society of Medicine<ref name=SpecialGMofRMCS1907/><ref name=MacAlister1907/><ref name=RSM1stGM1907/> and with volume numbering restarting at 1, before obtaining its current name in 1978.<ref name=Dacie1978/>

== Abstracting and indexing ==
The journal is abstracted and indexed in [[MEDLINE]]/[[PubMed]], [[Science Citation Index]], [[EMBASE]], [[CAB International]], and [[Elsevier BIOBASE|BIOBASE]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 2.118.<ref name=WoS>{{cite book |year=2015 |chapter=Journal of the Royal Society of Medicine |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==Content==
The journal describes itself as having "an international and multi-specialty readership that includes primary care and public health professionals".<ref name=JRSMAuthorGuidlines/> It claims to act as "a forum for debate, education, and entertainment for clinicians interested in UK medicine and relevant international developments and research. The aim of the journal is to influence clinical practice and policy making across the whole range of medicine".<ref name=JRSMAuthorGuidlines/> Each issue contains original research articles, [[editorial]]s, [[Review journal|review]]s, and [[essay]]s. The essay section brings together "think pieces" on current medical issues and medical history. The journal also includes [[book review]]s. Each issue also features a selection of commentaries from the [[James Lind]] Library, an online resource for patients and professionals that documents the evolution of fair tests of treatments in health care.<ref name=ChalmersDec2003/>

In 2006, the journal introduced open peer review, a system in which authors and reviewers know each other's identities on the assumption that this improves openness in scientific discourse. This made it one of the few medical journals in the world with open peer review.<ref name=AbbasiAug2006/>

==Editions==
The ''Journal of the Royal Society of Medicine'' is published monthly.<ref name=AboutJRSM/> The full text of each issue is available to subscribers online on the journal's website at the beginning of each month.<ref name=AbbasiMar2006/>

==Open access== 
In March 2006 all research articles published, as well as all other content more than three years old, were made available for free online. An agreement with [[PubMed Central]] was also announced, in which a digitised archive of the journal and its predecessor would be created, with issues dating back to 1809 available online for free.<ref name=AbbasiMar2006/>

==References==
{{reflist|30em|refs=
<ref name=AbbasiMar2006>{{citation |date=March 2006 |author=Abbasi, K |title=Open access for the JRSM |journal=Journal of the Royal Society of Medicine |volume=99 |issue=3 |page=101 |url=http://jrs.sagepub.com/content/99/3/101.full |issn=0141-0768|doi=10.1258/jrsm.99.3.101 |accessdate=8 November 2013}}</ref>

<ref name=AbbasiAug2006>{{citation |date=August 2006 |author=Abbasi, K |title=JRSM introduces open peer review |journal=Journal of the Royal Society of Medicine |volume=99 |issue=8 |page=379 |url=http://jrs.sagepub.com/content/99/8/379.full |issn=0141-0768 |doi=10.1258/jrsm.99.8.379 |accessdate=8 November 2013}}</ref>

<ref name=AboutJRSM>{{citation |title=About JRSM |publisher=Journal of the Royal Society of Medicine |url=http://jrs.sagepub.com/ |accessdate=8 November 2013}}</ref>

<ref name=ChalmersDec2003>{{citation |date=December 2003|author=Chalmers, Iain |title=The James Lind Initiative |journal=Journal of the Royal Society of Medicine |volume=96 |issue=12 |pages=575–576 |url=http://jrs.sagepub.com/content/96/12/575.full |issn=0141-0768 |doi=10.1258/jrsm.96.12.575|accessdate=8 November 2013 |pmid=14645604 |pmc=539653}}</ref>

<ref name=FoxAug2005>{{citation |date=August 2005 |author=Fox, Robin |title=Farewell from Fox |journal=Journal of the Royal Society of Medicine |volume=98 |issue=8 |page=340 |url=http://jrs.sagepub.com/content/98/8/340.full |issn=0141-0768 |doi=10.1258/jrsm.98.8.340|accessdate=8 November 2013}}</ref>

<ref name=JRSMAuthorGuidlines>{{citation |title=JRSM Manuscript Submission Guidelines |url=http://www.uk.sagepub.com/msg/jrs.htm |accessdate=8 November 2013}}</ref>

<ref name=MacAlister1907>{{citation |year=1907 |author=MacAlister, J.Y.W |title=Union of Medical Societies |journal=Medico-Chirurgical Transactions; Report of the Committee of Representatives |volume=90 |pages=cxxxvi–cxlvii |pmc=2038529 |pmid=20897096}}</ref>

<ref name=RSM1stGM1907>{{citation |date=13 June 1907 |title=The Royal Society of Medicine: The First General Meeting of the Fellows Under the New Charter |pages=cxlviii–clxii |pmc=2038540|volume=90 |journal=Medico-chirurgical transactions}}</ref>

<ref name=SpecialGMofRMCS1907>{{citation |date=14 June 1907 |title=Special General Meeting of the Royal Medical and Chirurgical Society of London |journal=Medico-Chirurgical Transactions |volume=90 |pages=cxxi–cxxiv |pmc=2038538 }}</ref>

}}

==External links==
* {{Official website}}
*[http://journals.sagepub.com/loi/jrs All Issues - Journal of the Royal Society of Medicine]

[[Category:Publications established in 1809]]
[[Category:SAGE Publications academic journals]]
[[Category:General medical journals]]
[[Category:English-language journals]]
[[Category:Monthly journals]]
[[Category:1809 establishments in the United Kingdom]]
[[Category:Academic journals associated with learned and professional societies of the United Kingdom]]