{{COI|date=March 2015}}
{{Contains Hebrew text}}
{{Infobox musical artist
| name                = Yaakov "Yanky" Lemmer
| image               = 
| image_size          = 
| landscape           = <!-- yes, if wide image, otherwise leave blank -->
| alt                 = Picture of Yaakov "Yanky" Lemmer
| caption             =
| background          = solo_singer
| birth_name          = Yaakov Lemmer
| native_name         = יעקב לעממער
| native_name_lang    = Hebrew
| alias               = "Yanky" or "Yanky Lemmer"
| birth_date          = {{birth date and age|1983|11|06}}
| birth_place         = [[Brooklyn, New York]]
| origin              = 
| death_date          =
| death_place         = 
| genre               = Hebrew [[Liturgy]], Yiddish [[folk music|folk]], [[Opera]], [[Broadway theatre|Broadway]], [[Music of Israel|Israeli]]
| occupation          = Singer, Head [[Hazzan|Cantor]], [[Special Education]] Therapist
| instrument          = Voice
| years_active        = 2005-present
| label               = [[Nigun Distributors]], [[International Music Company]]
| associated_acts     =
| website             = {{URL|http://www.cantorlemmer.com/#!}}
| notable_instruments = 
}}
'''Yaakov Lemmer''' is an internationally performing [[Jewish cantor]], singer, and concert artist born and raised in [[Brooklyn, New York]]. He is often recognized for his smooth [[tenor]] voice, his dramatic tone, and his ability to sing in several languages and styles. Trained by top cantors from a young age, Lemmer had years of practice to perfect his technique. Training and education through various teachers and educational institutes has given "Yanky" his own personal style and influence.<ref name="CL">{{Citation
 | last = Lemmer
 | first = Yaakov
 | title = Cantor Yaakov Lemmer: Biography
 | url=http://www.cantorlemmer.com/#!__page-1 
 | accessdate = 2 December 2013 }}</ref>
Lemmer is in demand as part of the international music and Jewish communities alike. From concerts in the [[United States]] to events and performances throughout [[Europe]] and [[Israel]], Yaakov Lemmer is described as a positive and powerful stage presence.

In September 2013 it was announced that he would follow the current cantor of [[Lincoln Square Synagogue]], a Modern Orthodox congregation in [[New York City]]. Lemmer is due to start full-time Head Cantor duties in 2014.<ref name="JW">{{Citation
 | last = Lipman
 | first = Steve
 | title = New Cantor, New Look
 | publisher = The Jewish Week
 | date = 17 September 2013
 | url=http://www.thejewishweek.com/features/new-york-minute/lincoln-square-gets-new-cantor-new-look 
 | accessdate = 30 November 2013 }}
</ref> The current Cantor, [[Lincoln Square Synagogue#Cantor Sherwood Goffin|Sherwood Goffin]], has been serving in the [[synagogue]] since its founding in 1965.<ref name="lss">"{{Citation
 | title = Meet Our Clergy: Chazzan Yanky Lemmer 
 | publisher = Lincoln Square Synagogue
 | year = 2013
 | url=http://lss.shulcloud.com/meet-our-clergy.html#lemmer 
 | accessdate = 3 December 2013 }}
</ref>

== Early life ==

[[File:Borough Park, Brooklyn.jpg|thumb|left|Borough Park]]
Yaakov Lemmer was born on November 6, 1983, in the [[Borough Park, Brooklyn|Borough Park]] neighborhood of Brooklyn, New York, to parents of [[Jewish]] descent. Borough Park is known for being a prominently [[Modern Orthodox|Orthodox]] neighborhood. This was where the young singer grew and developed. When asked about what inspired him to train in and become involved with religious music, Lemmer responded that it was his father's passion for music. Every Sunday night, for two hours, Jewish radio persona [[Charlie Bernhaut|Bernhaut]] helped to spark Lemmer's interest.<ref name="JW" />

Listening to well-known Jewish music and radio in his early life fueled a fire for music and liturgy. Lemmer recalls some of his biggest inspirations to be the greats of the Golden Age of cantorial music such as: [[Mordechai|Hershman]], [[Yossele Rosenblatt|Rosenblatt]], and [[Zavel|Kwartin]].<ref name="lss" /> Additionally, cantor Moshe Stern had a tremendous influence on his cantorial style. Lemmer's father would walk many miles to listen to Cantor Stern on his visiting Shabbat services.<ref name="jt" />

The young Lemmer received his first formal cantorial training at [[Young Israel of Beth El]], a local [[shul]] in Borough Park. Here he functioned as the main soloist in the choir, under the training of Cantors [[Benzion Miller]] and [[Azi Schwartz]]. As a result of his performance and skill, Lemmer received a scholarship to the Belz School of Jewish Music at [[Yeshiva University]]. He studied and practiced with cantors [[Joseph Malovany]] and Bernard Beer.<ref name="lss" /> Lemmer graduated with a master's degree in special education. His passion drove Lemmer to pursue his musical and cantorial career in addition to a job in education.<ref name="jt">{{Citation
 | last = Saffer
 | first = David
 | title = Chazan who hits high note with Broadway songs and opera, too
 | publisher = Jewish Telegraph
 | year = 2012
 | url = http://www.jewishtelegraph.com/prof_159.html
 | accessdate = 1 December 2013 }}</ref>

== Career ==

Yaakov Lemmer's career spans several continents and countries. He has been invited to sing at numerous events and has also performed at large synagogues and concert halls around the world. [[File:Young Israel Ft Lee Parker Old Palisade jeh.JPG|thumb|Young Israel- Ft. Lee, New Jersey]]

=== Cantorial positions ===

Yaakov Lemmer has held the position of [[Chazzan]] at four different synagogues since the start of his career. He currently holds the position of head Cantor at [[Lincoln Square Synagogue]]. In chronological order:

*Young Israel of [[Fort Lee, New Jersey|Fort Lee]], New Jersey{{citation needed|date=January 2014}}
*Congregation Anshe Sholom of [[New Rochelle, New York|New Rochelle]], New York<ref name="JW" />
*Congregation Ahavath Torah of [[Englewood, New Jersey|Englewood]], New Jersey<ref name="jt" />
*Lincoln Square Synagogue, [[New York City|New York]], New York<ref name="lss" />

=== Lemmer's big break ===

In early 2007, while participating as guest Cantor in a post [[Tu Bishvat]] concert at Young Israel of Beth El, Lemmer was recorded singing "Misratze B'rachmim." This video was uploaded to [https://www.youtube.com/watch?v=tWrXQv19-kQ Youtube] and quickly gained attention.<ref>{{cite AV media
 | people = Yaakov Lemmer, "Chazanaar" (producer, director)
 | title = Chazzan Yaakov Lemmer-Beth E-l 
 | medium = Youtube Video
 | publisher = User name: "Chazanaar"
 | location = Borough Park, New York City, New York, United States
 | date = 25 March 2007
 | url = https://www.youtube.com/watch?v=tWrXQv19-kQ }}</ref> Shortly after this event, Lemmer was invited to sing at the [[Jewish Culture Festival]] in [[Budapest, Hungary]]. At the event, he sang for a packed audience along with other cantors including [[Benzion Miller]]. This combination of these two nights elevated him to a high status within Jewish music circles. Lemmer himself describes [[YouTube]] as "the springboard" to his career.<ref name="jt" />

=== Traveling the world ===

Since being elevated quickly to a position of high visibility, Yaakov Lemmer has been in demand. Performances have included concerts and ceremonies in [[Poland]], [[England]], [[Israel]], [[Germany]], [[Denmark]], [[France]], [[Belgium]], and throughout the [[United States]].

==== Boston Jewish Music Festival ====

The [[Boston]] Jewish Music Festival holds concerts, lectures, and other educational events to promote Jewish culture. Events are held throughout the year. On March 12, 2011, a concert was held featuring "Yanky," among other Cantors and musicians. The concert, "Divine Sparks – Music to Ignite Your Soul: A Concert of Kavanah," received positive reviews from most in attendance. [[Kavanah]] is a [[Hebrew]] word that refers to a certain state of mind, often linked to prayer or other Jewish rituals. The word may also be understood to mean "concentration" or "intention."<ref name= "AF">{{Citation
 | last = Elman
 | first = Steve
 | title = Judicial Review # 6: "Divine Sparks" (Boston Jewish Music Festival Concert at Berklee): How Hot a Flame?
 | magazine = The Arts Fuse: Boston's Online Art Magazine
 | date = 6 April 2011
 | url = http://artsfuse.org/27943/judicial-review-6-%E2%80%9Cdivine-sparks%E2%80%9D-boston-jewish-music-festival-concert-at-berklee-how-hot-a-flame/
 | accessdate = 5 December 2013 }}</ref>
[[Bill Marx]] editor of [[The Arts Fuse]], a Boston-based online art magazine, had this to say about the concert:

''"All of the judges found the concert ambitious and fascinating, a valuable step in expanding the creative and cultural boundaries of "Jewish music." The cantors were generally admired, with the recognition that they sang with great devotion, spiritual power, and a proud exuberance."''<ref name= "AF" />

==== Lighting of the national menorah ====

Every year in [[Washington D.C.]], the national [[Menorah (Hanukkah)|menorah]] is lit on the first night of [[Hanukkah]]. Lemmer was invited to sing at the event on December 20, 2011. Lemmer joined two other well-known cantors, [[Yaakov Motzen]] and [[Jeffrey Nadel]], to sing traditional Hanukkah songs. Music for the event was played by the [[United States Marine Band]]. Since then, "Yanky" has been invited back twice more to sing at the event.<ref>{{cite web
 | title = National Menorah Lit in Washington
 | publisher = Gruntig!
 | date = 20 December 2011
 | url = http://www.gruntig.net/2011/12/national-menorah-lit-in-washington.html
 | format = Blog
 | accessdate = 3 December 2013 }}</ref>

==== 70th anniversary of Warsaw Ghetto uprising ====
April 19, 2013, marked the 70th anniversary of the [[Warsaw Ghetto uprising]]. A large event was held at the site of the old [[Warsaw Ghetto]] on the anniversary. Musicians included the [[Israeli Philharmonic Orchestra]], conducted by [[Zubin Metah]], as well as several soloists, and cantor Yaakov Lemmer.<ref>{{Citation
 | title = Israel Philharmonic plays Warsaw Ghetto Uprising tribute
 | publisher = Polskie Radio S.A 
 | date = 18 April 2013
 | url = http://www.thenews.pl/1/11/Artykul/133244,Israel-Philharmonic-plays-Warsaw-Ghetto-Uprising-tribute
 | accessdate = 6 December 2013}}</ref>
<ref>{{Citation
 | title = Koncert kantorów na inauguracji festiwalu "Warszawa Singera"
 | publisher = Polska Agencja Prasowa (PAP)
 | date = 25 August 2013
 | url = http://dzieje.pl/kultura-i-sztuka/koncert-kantorow-na-inauguracji-festiwalu-warszawa-singera-0
 | accessdate = 1 December 2013}}</ref>
<ref>{{Citation
 | last = Mtom 
 | first = Gasd
 | title = Koncert w 70. rocznicę wybuchu powstania w getcie
 | publisher = Polska Agencja Prasowa (PAP)
 | date = 19 April 2013
 | url = http://tvp.info/informacje/polska/koncert-w-70-rocznice-wybuchu-powstania-w-getcie/10790159
 | accessdate = 1 December 2013 }}</ref>
Lemmer has been to Poland several times. Lemmer's grandparents were Polish citizens.<ref name= "jt" />

== Personal life ==

Despite traveling the world and performing in front of thousands, Yaakov Lemmer still prefers to keep his personal life out of the public eye. He was born and raised in the Borough Park Neighborhood of [[New York City]] in 1983. He attended school, learned, and trained here. The scholarship earned by his vocal abilities enabled him to attend college in the city as well. Lemmer has several degrees including a [[Master of Education|Masters of Special Education]]. He currently resides in New York City with his wife and works as a [[Special Education]] [[Therapist]]. {{citation needed|date=December 2013}}

== Significant works ==

=== "Rainbow in the Night" ===

Debuting in December 2011, the song and the video was written by New York-based filmmaker [[Cecelia Margules]], a daughter of [[Holocaust]] survivors. Margules collaborated with producer and director Danny Finkelman. Yaakov Lemmer partnered with the others to contribute to the project vocally and through acting. Lemmer's vocals tie the video together with emotional lyrics and a powerful voice.<ref>{{Citation
 | last = Jspace Staff
 | title = The First Ever Holocaust Music Video is Released (VIDEO) 
 | publisher = Jspace News
 | date = 31 January 2012
 | url = http://www.jspace.com/news/articles/the-first-ever-holocaust-music-video-is-released-video/7298
 | accessdate = 30 November 2013}}</ref>
"Rainbow in the Night" was described by Margules as the "first ever music video depicting the Holocaust." The video was created in order to raise awareness about the Holocaust. It follows the experiences of a [[Jewish]] family in [[Poland]] before [[World War II]]. Through the video, the dirty reality of life in the [[Krakow Ghetto]] is shown. The grim conditions become unimaginably worse when the family ends up being sent to a [[concentration camp]]. The video was filmed on location in [[Krakow, Poland]] and [[Majdanek concentration camp]], also in Poland. The videos creators have expressed wishes that the video become "viral" in order to educate others, especially the current "fast-paced" contemporary generation.<ref>{{cite web
 | last = Campbell
 | first = Kay
 | title = 'Rainbow in the Night' Holocaust music video captures horror of World War II persecution
 | work = Living
 | publisher = Alabama Media Group
 | date = 23 April 2012
 | url = http://www.al.com/living/index.ssf/2012/04/rainbow_in_the_night_holocaust.html
 | accessdate = 4 December 2013}}</ref>
The video can be freely accessed on [https://www.youtube.com/watch?v=zSlWy1ckoAk#t=289 YouTube]<ref>{{cite AV media
 | people = Cecelia Margules (Writer), Danny Finkelman (Producer/Director)
 | title = "Rainbow in the Night" Official Music Video
 | medium = Youtube
 | publisher = Sparks Next
 | location = United States
 | date = 25 January 2012
 | url = https://www.youtube.com/watch?v=zSlWy1ckoAk#t=289
 | accessdate = 29 November 2013}}</ref>

== Discography ==

=== Bkoil ===

''[[Bkoil]]'' is a compilation album featuring Lemmer. It was released February 19, 2008, by [[Mizmor Music]]. The album features seven singles, the sixth single is performed by cantor Lemmer. It is entitled "Halben Chato'einu"<ref>{{cite web
 | title = Bkoil: By Various Cantors (Main) 
 | work = Product Information
 | publisher = 718Getasong Corp
 | date = 19 February 2008
 | url = http://www.mostlymusic.com/bkoil.html
 | accessdate = 7 December 2013}}</ref>

=== Vimaleh Mshaloseinu ===

[[Vimaleh Mshaloseinu]] is "Yanky's" debut studio album. Released January 27, 2010, it features eleven songs:<ref>{{cite web
 | title = Yaakov Lemmer: Vimaleh Mshaloseinu: by Yaakov Lemmer (Main)  
 | work = Product Information
 | publisher = 718Getasong Corp
 | date = 27  2008
 | url = http://www.mostlymusic.com/yaakov-lemmer-vimaleh-mshaloseinu.html
 | accessdate = 7 December 2013}}</ref>

 1. Ato Nigleiso
 2. Lo Tachmod
 3. Shema Yisroel
 4. Mein Zeiden's Chulem
 5. Misratze B'rachamim 
 6. Av Harachamim
 7. Ma Godlu Ma'asecho 
 8. Habeit Mishomayim
 9. Umipnei Chatoeinu  
 10. Zol Shoyn Kumen Di Geulah
 11. Lemmer 1928 version

=== Unity for Justice ===

"Unity for Justice" is a single released by [[Aderet Music Corporation]] featuring over 40 singers. It was put together as a charity project to help collect donations for a Jewish business owner. Yaakov Lemmer was one of the cantors invited to sing on the album.<ref>{{cite web
 | title =   
Unity For Justice: by Avraham Fried (Singer), Mordechai Ben David or MBD (Singer), Lipa Schmeltzer (Singer), Yaakov Shwekey (Singer), Benny Friedman (Singer), Mendy Werdyger (Singer), Yisroel Werdyger (Singer), Shloime Taussig (Singer), Berry Weber (Singer), Dovid Gabay (Singer), Aaron Razel (Singer), Ohad Moskowitz (Singer), Shloime Gertner (Singer), Shloime Dachs (Singer) 
 | work = Product Information
 | publisher = 718Getasong Corp
 | date = 6 October 2010
 | url = http://www.mostlymusic.com/unitysingle.html
 | accessdate = 7 December 2013}}</ref>

=== Rainbow in the Night ===

The audio-only single from the "Rainbow in the Night" video. Lemmer partnered with main producer Danny Finkelman to create the single. It as released on January 25, 2012. The single was labeled by [[Sparks Production]] and distributed by Aderet Music Corporation<ref>{{cite web
 | title = Rainbow in the Night: By Danny Finkelman (Producer, Main), Yaakov Lemmer (Singer)
 | work = Product Information
 | publisher = 718Getasong Corp
 | date = 25 January 2012
 | url = http://www.mostlymusic.com/unitysingle.html
 | accessdate = 7 December 2013}}</ref>

== References ==
{{reflist}}

{{Contemporary Jewish religious music}}

{{DEFAULTSORT:Lemmer, Yaakov}}
[[Category:Hazzans]]
[[Category:American male singers]]
[[Category:American Jews]]
[[Category:1983 births]]
[[Category:Living people]]