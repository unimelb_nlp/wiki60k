{{refimprove|date=April 2016}}

{|{{Infobox ship begin}}
{{Infobox ship image

|Ship caption= CS ''Pacific''
}}
{{Infobox ship career
|Ship name=''Pacific''
|Ship flag=[[Image:flag of Denmark.svg|100x35px|border]]
|Ship operator=[[GN Store Nord|Great Northern Telegraph Company]]
|Ship builder=[[Burmeister & Wain]]
|Ship yard number=230
|Ship launched=September 1903
|Ship fate=Scrapped, 1950
}}
{{Infobox ship characteristics
|Ship type= [[Cable ship]]
|Ship tonnage=1,570
|Ship length={{convert|264.6|ft|m|abbr=on}}
|Ship beam={{convert|35.8|ft|m|abbr=on}}
|Ship depth={{convert|21.5|ft|m|abbr=on}}
|Ship power=1,700 ihp
|Ship fuel=27 tons/coal/per day
|Ship propulsion= Twin screw. Triple expansion steam engines
}}
|}

'''CS ''Pacific''''' was a [[Cable layer|cable ship]] registered in [[Copenhagen]], Denmark owned by the [[GN Store Nord|Great Northern Telegraph Company]]. The steel vessel was built in 1903 in the shipyards of [[Burmeister & Wain]] and delivered that year for the purpose of laying and repairing submarine cable in the Far East networks. Historical records of this cable ship are kept in the [[M/S Maritime Museum of Denmark|National Maritime Museum of Denmark]]. Pictorial records of the ship are available through the [[DieselHouse]] interactive initiative.

==Operations==
The vessel was based in [[Shanghai]] where the Great Northern Telegraph China and Japan Extension Company had its central office in the  [[Great Northern Telegraph Building|Telegraph Building]] located in the Bund. In addition to the CS ''Pacific'' another cable ship called ''Store Nordiske'' was assisting in the general maintenance and repairs of the Chinese and Japanese links concerned mainly with the sections Vladivostok-Nagasaki, Nagasaki-Shanghai, Shanghai-Hong Kong involving more than 2000 [[nautical mile]]s of submarine cable.<ref>{{cite book|last1=Blundell|first1=Joseph Wagstaff|title=The manual of submarine telegraph companies|date=1872|location=London|page=62}}</ref>

The classification society [[Bureau Veritas]] was in charge of the regular survey of the ship.<ref>{{cite journal|title=Danmarks skibsliste|date=1930}}</ref>

In the decade of 1910's the CS Pacific underwent a major refit in the Shanghai Old Dock in order to get a long [[awning]] over the main deck and new rigging to support the wireless antennas.
The decks and upper bridge were regularly covered by large canvas to mitigate the effects of the hot temperatures and heavy rains of the [[humid subtropical climate]].
In 1932, Axel Ejner Christiansen, the Captain of the sister ship Store Nordiske died of [[cholera]].
Two minor incidents are recorded in the history of the vessel. Some damage occurred in 1904 during the journey from Copenhagen to Shanghai due to heavy storms. In 1939 a collision between the CS Pacific and Italian steamer Granatiere Padula is also recorded. The accident happened in the [[Huangpu River]] and caused little damage.

== Cable gear ==
To carry out her main duties, the ship was equipped with different machinery and cable gear supplied by the electrical company Johnson and Phillips
([[Charlton, London|Charlton]], Kent, UK)<ref>{{cite book|last1=Wilkinson|title=Submarine cable laying and repairing|date=1909|publisher=The electrician|location=London|pages=259|oclc=58038727}}</ref>
The manufacture of this heavy metal equipment took place in 1902 by the Telegraph Engineers Department of J&P.<ref>{{cite book|last1=Brooks|first1=Collins|title=The history of Johnson and Phillips; a romance of seventy-five years|date=1950|publisher=Privately printed|location=London|pages=186|oclc=6319111}}</ref>

Cable engines were mounted one forward and one aft of the machine. Cable drums to pick up and pay out the cable were installed in the spar deck where a [[dynamometer]] was also installed.

The ship was equipped with three cable tanks with a cone in the middle used as a water tank. The diameters of these tanks were 26&nbsp;ft, 31 1/2 and one 28&nbsp;ft respectively.<ref>{{cite book|last1=Haigh|first1=Kenneth Richardson|title=Cableships and submarine cables|date=1968|publisher=Adlard Coles|location=London|page=356|oclc=433859}}</ref>

Another interesting feature of the ship was the drum-room or pay-out office located at the stern deck. A junior engineer would check the percentage of cable slack using different graphs, calculator boards and the taut-wire gear. In the Testing Room the chief electrician and his assistants tested the cable using instruments like the  mirror [[galvanometer]]. The testing Room was located on the spar deck below the bridge.<br />

In addition to the cable machinery, the ship was equipped with other ancillary apparatus. A large search lamp was normally located in the fore-deck to assist in operations to shore-end jobs. Large buoys (conical type) were used as markers and stored on the main deck.

On the boat deck six small boats were available (one 18&nbsp;ft [[captain's gig|gig]], two 21&nbsp;ft cutters, two 23&nbsp;ft lifeboats and one 30&nbsp;ft steam launch)
The wireless room was located on the spar deck. The radio call sign of Pacific was OZI, later OZIB.

== Crew ==
Expert telegraph engineer Raymond-Barker wrote once that "cable-laying viewed in its entirety affords a striking instance of well-organised division of important duties"<ref>{{cite book|last1=Raymond-Barker|first1=Edward|title=Graphs in a cable ship drum-room:notes for junior assistants|date=1913|publisher=The electrical review|location=London|page=7}}</ref>

The core staff was composed of Captain, five officers and five engineers. In addition, sixty locally recruited Chinese sailors were the manpower for the many jobs carried out on board. The Captain or Commander was responsible for navigation and general logistics. Danish technicians were in charge of the whole supervision of cable-laying and fault-tracing. The engineers put a great effort in keeping the boilers and steam engines in good working order. The daily fuel consumption was 27 tons of coal approximately.

A memorable event for the crews of the CS Pacific and CS Store Nordiske was the Royal Visit of [[Frederick IX of Denmark|Prince Frederik]] to China in March 1930. Amongst the celebrations, the Great Northern Telegraph organised a ball and a dinner in the French Club to which the staff and the crew were invited.<ref>{{cite book|last1=Bramsen|first1=Christopher Bo|title=Open doors: Vilhelm Meyer and the establishment of General Electric in China|date=2013|publisher=Routledge|isbn=9781136847745|page=508}}</ref>

Amongst her captains were: E. Suenson (1905-1911), C. Tofte (1913–1915), H Petersen (1916-1918), H C A Petersen (1921-1923), Hans J. Christiansen (1919,1920, 1924-1931), J. B. Mathiasen (1932), E. Nielsen (1933)<ref>{{cite book|title=The Directory and Chronicle of China, Japan, Corea...|date=1933|publisher=The Hongkong Daily Press, Ltd.|location=Hongkong|pages=674}}</ref> and August Vilhelm Nickel.
In 1933, the Chief Officer was [[Axel Ingwersen]] winner of the 1924 winter [[Blue Water Medal]].

== WW1 and WW2 ==
In wartime the submarine cables and wireless stations suffered attacks in order to cut enemy's communications. This was the case in both wars and in the 1914-1918 conflict some telegraph facilities were seen as strategic targets.<ref>{{cite web|last1=Bruton|first1=Elizabeth|title=From Australia to Zimmermann: A Brief History of Cable Telegraphy during World War One|url=http://blogs.mhs.ox.ac.uk/innovatingincombat/files/2013/03/Innovating-in-Combat-educational-resources-telegraph-cable-draft-1.pdf|website=blog|publisher=MHS|accessdate=2016-04-06}}</ref> The case of [[Tabuaeran|Tabuaeran island]] is well-documented.
During this period the activity of the cable ships was mainly to repair the damage produced by belligerent actions.

In 1942 both the CS ''Pacific'' and CS ''Store Nordiske'' were put under British administration<ref>{{cite book|last1=Lund|first1=Christian|title=Årbog|date=1999|publisher=Handels-og Søfartsmuseet på Kronborg|page=Vol.58}}</ref> and the British company [[Cable & Wireless plc|Cable and Wireless]] took over the management of both ships.

William Elmgreen, fourth engineer of the ''Pacific'', captured this time in his personal diary in the following terms:

<blockquote>"On 9th April 1940, Denmark was invaded by Hitler's army. We still carried out repairs as usual, until May, when it was rumoured that some local Nazis were quietly planning to take over our ships. Both steamers immediately raised steam and left for Hong Kong. I was on the S/S Pacific and we arrived at Hong Kong four days later. We signed British Articles, pulled down our old Danish flag, and hoisted the Union Jack. Our neutrality markings were covered with war paint and we left immediately for a repair job on the high seas. On our main mast was a sealed document with this inscription: 'Thirty days from this date, the C/S-Pacific will be a Prize of War, belonging to his Majesty King George VI.' After this, the cable steamer, C/S Pacific was a Man-of-War, according to the Geneva Conference."<ref>{{cite web|title=My Years in China|last=Elmgreen|first=William|editor-last=Elmgreen|editor-first=John|url=http://www.talesofoldchina.com/library/elmswood.php|accessdate=2016-04-06|publisher=Self-published}}</ref></blockquote>

The relationship between the Great Northern Telegraph Company and Cable and Wireless has been properly researched by Prof. K Jacobsen in a collective volume.<ref>{{cite book|last1=Sevaldsen|first1=J. ed.|title=Britain and Denmark|date=2003|publisher=Museum Tusculanum Press|location=Copenhagen|isbn=87-7289-750-3|page=220}}</ref>

==Retirement and scrapping==
The ''Pacific'' was later stationed at [[Singapore]], relieving the British Cable Steamer, The ''Cable'', and then went on to Australia, returning to Singapore in 1941.
After 47 years of service the ship was sold for [[Danish krone|DKK]] 104,360 in [[Singapore]] to Agaarwal Bros and towed to [[Mumbai]] for breaking up.<ref>Cable and Wireless Archive held at the Telegraph Museum Porthcurno, DOC/E&ATC/14/51</ref>

== CS Pacific in art ==
In the early 20th century many Danish post-romantic artists were inspired by the world of sea and ships. In 1906 C V Bunch painted a marine inspired by the CS ''Pacific'' and ''Store Nordiske''. Another Danish naval painter, [[Vilhelm Arnesen]] (1865-1948), painted in 1928 an oil on canvas with the ''Pacific'' as main motive.

== References ==
{{Reflist}}

== External links ==
# [http://mfs.dk/en National Maritime Museum of Denmark]
# {{Cite web|url=http://www.gracesguide.co.uk/Johnson_and_Phillips|title=Johnson and Phillips|website=www.gracesguide.co.uk|access-date=2016-04-07}}
# {{Cite web|url=http://www.talesofoldchina.com/library/elmswood.php|title=Tales of Old China|website=www.talesofoldchina.com|access-date=2016-04-07}}
# {{Cite web|url=http://www.jmarcussen.dk/maritim/skibsliste/side.php?id=7380|title=Enkeltskibsoplysninger PACIFIC|website=www.jmarcussen.dk|language=da|access-date=2016-04-07}}
# {{Cite web|url=http://atlantic-cable.com/Cableships/Pacific/index.htm|title=History of the Atlantic Cable & Submarine Telegraphy - CS Pacific|website=atlantic-cable.com|access-date=2016-04-07}}
# {{Cite web|url=http://www.sbib.dk/skibslister.htm|title=Danmarks Skibslister|website=www.sbib.dk|access-date=2016-04-07}}

{{DEFAULTSORT:Pacific}}
<!--- Categories --->
[[Category:Ships of Denmark]]
[[Category:Submarine communications cables in the Pacific Ocean]]
[[Category:Cable laying ships]]
[[Category:1903 ships]]