{{Use Australian English|date=August 2015}}
{{Use dmy dates|date=February 2011}}
{{Geobox|River 
<!-- *** Heading *** --> 
| name = Patawalonga
| native_name = 
| other_name = Patawalonga Creek
| category = 
<!-- *** Names **** --> 
| etymology =
| nickname = ''The Pat''
<!-- *** Image *** --> 
| image = Patawalonga River and marina.jpg
| image_caption = The Patawalonga River near its [[mouth (river)|mouth]]
| image_size = 
<!-- *** Country *** --> 
| country = Australia
| state = South Australia
| region = Mid North
| district = Adelaide Plains
| municipality = 
<!-- *** Family *** --> 
| parent =
| tributary_left = 
| tributary_left1 =
| tributary_left2 =
| tributary_right = 
| tributary_right1 = 
| tributary_right2 =
| tributary_right3 =
| city_type = Towns
| city = [[Glenelg, South Australia|Glenelg]]
| landmark = 
<!-- *** Source *** --> 
| source = 
| source_location = near [[Adelaide Airport]]
| source_region = 
| source_country = 
| source_elevation = 
| source_coordinates =

| source1 = 
| source1_location = 
| source1_region = 
| source1_country = 
| source1_elevation = 
| source1_coordinates = 

| source_confluence =
| source_confluence_location =
| source_confluence_region = 
| source_confluence_country = 
| source_confluence_elevation = 
| source_confluence_coordinates = 
<!-- *** Mouth *** --> 
| mouth = Gulf St Vincent
| mouth_location = {{SAcity|Glenelg North}}
| mouth_region = 
| mouth_country = 
| mouth_elevation = 0
| mouth_coordinates = {{coord|34.9700|S|138.5136|E|source:wikidata_type:river_region:AU-SA|display=title,inline|format=dms}}
<!-- *** Dimensions *** --> 
| length = 7
| width = 
| depth = 
| volume = 
| watershed = 210
| watershed_ref =
| discharge = 
| discharge_location = 
| discharge_max = 
| discharge_min =
<!-- *** Free fields *** --> 
| free =
| free_type = [[Nature reserve]]
<!-- *** Maps *** --> 
| pushpin_map = Australia South Australia
| pushpin_map_relief = 1
| pushpin_map_caption = Location of the [[mouth (river)|river mouth]] in [[South Australia]]
<!-- *** Website *** --> 
| website = 
| commons = 
<!-- *** Footnotes *** --> 
| footnotes =
}}
The '''Patawalonga River''' is a river located in the [[Adelaide Plains]] district of the [[Mid North]] region in the Australian state of [[South Australia]]. 

==Course and features==
The Patawalonga River, sometimes called Patawalonga Creek, and known to local residents as ''the Pat'', is a short river of roughly {{convert|7|km}} in length that was, before European settlement, a tidal estuary.  The river is serviced by a {{convert|210|km2|adj=on}} [[drainage basin|catchment area]] that exists in metropolitan [[Adelaide]], with its mouth at the suburb of [[Glenelg, South Australia|Glenelg]].

The Patawalonga serves as an outlet for several creeks including the [[Keswick Creek|Keswick]], [[Brownhill Creek|Brownhill]] Creeks and [[Sturt River, Adelaide|Sturt Creek]] (also known as Sturt River), the latter being a former natural creek comprising for a significant part of its length now as a large concrete storm-drain.  

The catchment includes the Warriparinga Wetlands (opened 16 December 1998), an artificial wetlands situated near the suburb of [[Marion, South Australia|Marion]] designed to filter stormwater before it flows through Sturt Creek to the Patawalonga.

At its mouth the River's flow is regulated by barrages at [[Glenelg North, South Australia|Glenelg North]] and then flows past the [[Holdfast Shores]] marina development.

===Damming===
The first advocate for damming the Patawalonga was [[Thomas King (Australian politician)|Thomas King]] {{post-nominals|country=AUS|MP}}, a member of [[Parliament of South Australia|State Parliament]], who introduced a Bill to enable the Corporation of Glenelg to construct such a dam in 1876. Damming, apart from enabling the Patawalonga to be navigable and thus a safe harbour for yachts and other recreational watercraft, was seen as a means of reducing or removing the odour from the estuary of the river.  The dam was ultimately constructed in ''[[circa|ca.]]'' 1885.  King's service to his community is commemorated by a street and bridge over the river connecting Glenelg North with Glenelg, the "King Street Bridge".

The weir has not precluded the need to continually dredge the outlet for boating craft to pass.  A disagreement between the State Government and ferry operator Australian Ferries over the frequency dredging of sand and seaweed at the Patawalonga entrance led to the cancellation of the high-speed ferry service (featuring the Superflyte and, later, Enigma III vessels) between Glenelg and [[Kangaroo Island]] and [[Edithburgh, South Australia|Edithburgh]] on the [[Yorke Peninsula]].  The service had operated during summers from 1994 to November 2007.<ref>Messenger newspaper, 2 December 1998, p1</ref><ref>[[Adelaide Advertiser]], 25 December 1998, p9</ref>

===Water quality===
The Patawalonga is probably best known for its notorious odour, which has been a problem for the "Pat" ever since European settlement since the mid-19th century.  The odour arises from seaweed that grows in the shallow depths of the river estuary and, in more recent times, due to stormwater pollution.  

Dredging of the outlet beyond the weir to remove sand and seaweed build-up would at times cause the seaweed to float back to shore and rot on the beach, causing a stench.<ref>Sunday Mail newspaper, 24 January 1999, p19</ref>

Also, if too much fresh water flows into the Patawalonga it can kill off saltwater species of fish that exist in the lake - which, again, can result in an unpleasant odour,<ref>[[Adelaide Advertiser]], 24 January 2005, p1</ref><ref>[[Adelaide Advertiser]], 25 January 2005, p15</ref><ref>[[Adelaide Advertiser]], 1 June 2005, p10</ref> An event like this occurred on the weekend of 22–23 January 2005, and was reported on the front page of the ''Adelaide Advertiser'' as follows:
<blockquote>Residents woke yesterday to an "awful" stench and the sight of hundreds of seagulls converging on the area in a feeding frenzy.</blockquote>

===Barcoo Outlet===
From the 1970s onwards, increasing levels of rubbish and dirty stormwater would collect in front of the weir in the Patawalonga, bringing the notorious stench and unsightly view of debris in the water at [[Glenelg North, South Australia|Glenelg North]].  The debris and pollution had rendered the Patawalonga unusable for recreational activities such as a popular local "milk-carton" rowing regatta, which ended - along with all other recreational use such as swimming, [[water skiing]], [[sailboarding]] and [[dragon boat racing]]<ref>Messenger newspaper, 18 August 1999, p1</ref> - when local authorities closed the Patawalonga for such activities in 1987 due to concerns about public health due to pollution levels.<ref>[[Adelaide Advertiser]],18 February 2002, p12</ref>

Then [[Premier of South Australia|Premier]] [[Dean Brown]] commissioned a review in 1995 and the then Environment Minister [[David Wotton]] promised to swim in the Patawalonga with the Premier within a year, as this was the estimated time-frame for cleaning up "the Pat".<ref name="advdec272001p11">[[Adelaide Advertiser]], 17 December 2001, p11</ref>

Six years later, in December 2001 the Barcoo Outlet was completed.  The Outlet's intention was primarily to enable dirty stormwater from the catchment to be diverted away from the Patawonga Lake at an ultimate cost of approximately {{A$|30m}}.  The Outlet consists of an {{convert|885|m|adj=on}} pipeline diverting stormwater out of the final length of the Patawalonga and out to sea.  Symbolically, the then Premier [[John Olsen]] took a media-attended swim in the Patawalonga as the State Government lifted the ban on recreational use.<ref name="advdec272001p11" /><ref>[[Adelaide Advertiser]], 18 February 2002, p12</ref>

The Outlet project was initially dogged with controversy, due to failures to handle stormwater and pollution of Adelaide's beaches.  People north of the Outlet (which itself is 500m north of the Patawalonga's natural mouth) at [[West Beach, South Australia|West Beach]] complained that the coastal drift was seeing the pollution simply shifted to their beaches instead of into the Patawalonga Lake. The Outlet also failed to handle stormwater during heavy rainfall and the resulting in stormwater pollution entered the lake section of the Patawalonga in April (2x),<ref>[[Adelaide Advertiser]], 2 April 2002, p15</ref><ref>[[Adelaide Advertiser]], 26 April 2002, p11</ref> May<ref>[[Adelaide Advertiser]], 24 May 2002, p21</ref> and August 2002.<ref>[[Adelaide Advertiser]], 3 August 2002, p13</ref> Another failure in the Outlet in February 2003 resulted in minor flooding and consequent damage to homes in both Glenelg North and upstream [[Novar Gardens, South Australia|Novar Gardens]].<ref>[[Adelaide Advertiser]], 21 February 2003, p5</ref> 

The lower section of the Patawalonga at Glenelg North is now more-or less operated as a lake, with seawater at times circulated in through the river mouth and then out through the Barcoo Outlet to the north.  The Outlet is named after the frigate [[HMAS Barcoo|HMAS ''Barcoo'']], which ran aground at Glenelg North during a violent storm on 11 April 1948, which also destroyed most of the jetty at Glenelg.

===Flood of 2003===
Heavy rainfall and a malfunction in the weir resulted in the Patawalonga breaking its banks at Glenelg North on Friday, 27 June 2003 and flooding the homes of local residents.<ref>[[The Australian|The Weekend Australian]], 28 June 2003, p4</ref>  The situation became a major political issue with the Premier, [[Mike Rann]], declaring that he would establish a compensation fund for victims who had suffered water damage to their homes.  A local newspaper report suggested that 160 homes were affected and the cause of the flooding was the gates to the weir being kept closed during a stormwater flood to protect yachts harboured in the Patawalonga Lake.<ref>[[Adelaide Advertiser]], 26 November 2003, p1</ref> 145 residents made 150 claims upon the fund and at least {{A$|1.4m}} was paid to the victims, ultimately, by weir operator Baulderstone Hornibrook.<ref>[[Adelaide Advertiser]], 5 August 2004, p4</ref>  A class action was later launched by 70 residents for further compensation for 'stress and inconvenience'.<ref>[[Adelaide Advertiser]], 26 June 2006, page 11</ref>

==Etymology==
'Patawalonga', literally from the indigenous local [[Kaurna language]], is derived from 'Pattawilya + -ngga', the component parts being: Patta, which means a swamp gum tree (''[[Eucalyptus ovata]]'') and wilya meaning a branch while -ngga is a suffix used to indicate that the name is a location, patta-wilya-ngga the place of the branches of the swamp gum.  A reference in the Manning Index of South Australian History <ref>{{cite web | url=http://www.slsa.sa.gov.au/manning/pn/p/p3.htm#patawalonga | title=Place Names of South Australia - P, Patawalonga | publisher=State Library of South Australia | accessdate=16 November 2007}}</ref> suggests another meaning was "swamp of snakes" whilst another historian suggests it was a name given by an [[indigenous Australians|Indigenous Australian]] crew member of [[William Light|Colonel Light's]] ship meaning "boggy and bushy stretch, with fish".<ref name="historypat">{{cite web | url=http://historysouthaustralia.net/Pat.htm |title=Patawalonga & The Reedbeds, Holdfast Bay | publisher=historysouthaustralia.net | last=Crilly | first=K | accessdate=16 November 2007}}</ref>

In addition to names given above, the River was originally officially known as the "River Thames" and sometimes locally as the "Glenelg Creek". "River Thames" was the original name given in Light's 1836 city plan. Although the plan was approved by [[George Gawler|Governor Gawler]] the Indigenous name remained in common usage. Gawler encouraged colonists to collect information on Indigenous place names and he is known to have reinstated several Kaurna names in Adelaide.

==European discovery==
[[File:HMAS buffalo restaurant bow.jpg|right|thumb|upright|HMS ''Buffalo'' restaurant]]
The [[State Library of South Australia]] records that Colonel Light, sailing in a vessel called the ''Rapid'', discovered the Patawalonga River when sailing by and observing a river mouth when surveying the site for the city of Adelaide, via journal entry on 4 October 1836.<ref>{{cite web |title=A brief journal of the proceedings of William Light |page=12 |last=Light |first=William |year=1836 |publisher=[[State Library of South Australia]] |url=http://www.samemory.sa.gov.au/site/page.cfm?c=1138}}</ref>  

The river mouth served as the first significant river port for the colony of South Australia, with the [[Port River]] at [[Port Adelaide, South Australia|Port Adelaide]] comprising a shallow, mangrove river impassable - at that time - to large ships.

One historian records:<ref name="historypat" />

<blockquote>
The first boat constructed in the Patawalonga was the 22 ton cutter ''O.G.'' for the Colonial Secretary, [[Osmond Gilles]]. On the day it was launched in 1839, the boat was stranded until high tide. There was only 4 feet of water over the sand bar at the entrance.
</blockquote>

<blockquote>
Ships of over 300 tons, which were too big to enter Port Adelaide, discharged their passengers and cargoes at Glenelg. Floatable goods were pitched overboard and tided into the creek to the Customs House. The Customs House and flagstaff were erected in November 1839 for the accommodation of the Customs Officer and the crew of the two landing waiters. Pilots fees were still being collected on the Pat. fishing fleet of 35 vessels and for the landing of mail from the steamers until the 1880's. At that time it was called Port Glenelg.
</blockquote>

===HMS ''Buffalo''===
Perhaps the most iconic aspect of the Patawalonga River is the permanent mooring there of a replica of the {{HMS|Buffalo|1813|6}}, which made the six-month voyage carrying the first 400 settlers to South Australia in 1836.  The original ''Buffalo'' had also carried [[John Hindmarsh|Captain Hindmarsh]], captain of that vessel and, upon his arrival, the first [[Governor of South Australia]].  The ''Buffalo'' replica has served<ref>{{cite web | url=https://www.holdfast.sa.gov.au/Buffalo | publisher=Holdfast Bay Council | title=The Buffalo replica ship, Wigley Reserve, Glenelg | accessdate=11 March 2015}}</ref><ref>{{cite web | url=http://www.adelaidenow.com.au/messenger/west-beaches/paperwork-for-the-1-sale-of-glenelg-norths-buffalo-restaurant-lodged-with-holdfast-bay-council/story-fni9llx9-1226818808388 | publisher=Adelaide Messenger | title=Sale of Glenelg North's Buffalo Restaurant lodged with Holdfast Bay Council | accessdate=6 February 2014}}</ref> as a family and a la carte [[floating restaurant|restaurant floating]] on the Patawalonga.<ref>{{cite web | url=http://www.thebuffalo.com.au/ | publisher=Buffalo Restaurant | title=HMS Buffalo "The Historic ship in the Bay at Glenelg" | accessdate=19 November 2007}}</ref>

==See also==
{{stack|{{portal|Adelaide}}}}
*[[List of rivers of Australia#South Australia|List of rivers of South Australia]]

==References==
{{Reflist|30em}}

==Further reading==
* South Australian Government Department of Water, Land, Biodiversity and Conservation (DWLBC) website concerning the Patawalonga [http://www.dwlbc.sa.gov.au/urban/patawalonga]
* The DWLBC site also contains live indications of levels of the Patawalonga [http://www.dwlbc.sa.gov.au/urban/patawalonga/system/levels.html]
* [http://users.sa.chariot.net.au/~littoral/pat-ck/index.htm Friends of Patawalonga Creek website describing the Creek]

{{Rivers of South Australia|state=autocollapse}}

[[Category:Rivers of Adelaide]]