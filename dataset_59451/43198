__NOTOC__
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Model S
 | image=Martin Model S.JPG
 | caption=
}}{{Infobox Aircraft Type
 | type=Observation seaplane
 | national origin=United States
 | manufacturer=[[Glenn L. Martin Company|Martin]]
 | designer=[[Donald Wills Douglas, Sr.|Donald Douglas]]
 | first flight=1915
 | introduced=
 | retired=
 | status=
 | primary user=[[Aviation Section, U.S. Signal Corps]]
 | number built= 6 or 16<ref name="dispute"/>
 | developed from= [[Martin T]]
 | variants with their own articles=
}}
|}

The '''Martin S''' was a two-seat observation [[seaplane]] produced in the United States in 1915.<ref name="JEA">Taylor 1989, 635</ref> Designed along the same general lines as the preceding [[Martin T|Model T]],<ref name="IEA">''The Illustrated Encyclopedia of Aircraft'', 2432</ref> it was a largely conventional two-bay biplane with unstaggered wings of equal span. The fuselage was not directly attached to the lower wings, but was carried on struts in the interplane gap. The undercarriage consisted of a single large pontoon below the fuselage and outrigger floats near the wingtips.<ref name="JEA" /> The Model S was 23-year-old [[Donald Wills Douglas, Sr.|Donald Douglas]]'s first and only design for the Martin company, and it set three world altitude records and a flight duration record that stood for three years.<ref name="COF">"The Early Years of Douglas Aircraft, the 1920s"</ref>

Six, possibly fourteen, of these aircraft were operated by the [[Aviation Section, U.S. Signal Corps]],<ref name="dispute">Aero Files states six, and Baugher corroborates their serial numbers (S.C. 56-59, 94-95), while Taylor states fourteen. No additional serials for Martin S are given in Baugher. Aero Files does not list any aircraft for the Navy.</ref> and another two by the [[United States Navy]].<ref name="JEA" /><ref name="IEA" /> All of the Army aircraft, S.C. 56-59 and 94-95, were assigned to the first U.S. aviation unit based overseas, the [[2d Air Refueling Squadron|1st Company, 2d Aero Squadron]] at [[Fort Mills]], [[Corregidor]], in March and April 1916, where they used a radio transmitter with a range of 29 miles to adjust battery fire for the Coast Artillery.<ref name="hennapp14">{{cite web | last =Hennessey| first =Juliette| authorlink = | year =1958 | url= http://www.afhra.af.mil/shared/media/document/AFD-090602-017.pdf|title =The United States Army Air Arm, April 1861 to April 1917| work = USAF Historical Study No. 98 | publisher = AFHRA (USAF)| accessdate= 6 Mar 2011}}, pp. 152 and 165.</ref>

<!-- ==Development== -->
<!-- ==Operational history== -->
<!-- ==Variants== -->

==Operators==
;{{USA}}
*[[United States Army]]
*[[United States Navy]]
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications==
{{aerospecs
|ref=<!-- reference -->aerofiles.com
|met or eng?=<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->eng
|crew=Two, pilot and observer
|capacity=
|length m=9.02
|length ft=29
|length in=7
|span m=14.15
|span ft=46
|span in=5
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=
|height ft=
|height in=
|wing area sqm=
|wing area sqft=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=
|empty weight lb=
|gross weight kg=
|gross weight lb=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|eng1 number=1
|eng1 type=[[Hall-Scott A-5]]
|eng1 kw=<!-- prop engines -->93
|eng1 hp=<!-- prop engines -->125
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|max speed kmh=136
|max speed mph=85
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
;Notes
{{commons category|Martin S}}
{{reflist}}
;Bibliography
* {{cite web |title=Martin, Martin-Willard |work=aerofiles.com |url=http://www.aerofiles.com/_martin.html |accessdate=2008-10-21}}
* {{cite web |title=The Early Years of Douglas Aircraft, the 1920s |work=U.S. Centennial of Flight Commission website |url=http://www.centennialofflight.gov/essay/Aerospace/Douglas-early/Aero26.htm |accessdate=2008-10-21}}
* {{cite book |title=The Illustrated Encyclopedia of Aircraft |publisher=Aerospace Publishing|location=London |pages= }}
* {{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London |pages= }}
<!-- ==External links== -->

{{Martin aircraft}}

[[Category:United States military reconnaissance aircraft 1910–1919]]
[[Category:Floatplanes]]
[[Category:Martin aircraft|S]]
[[Category:Single-engined tractor aircraft]]
[[Category:Biplanes]]