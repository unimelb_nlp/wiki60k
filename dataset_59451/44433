{{Use mdy dates|date=April 2012}}
{{Infobox NCAA team season
  |Year=1936
  |Team=Alabama Crimson Tide
  |Image=
  |ImageSize=
  |Conference=Southeastern Conference
  |ShortConference=SEC
  |Record=8–0–1
  |ConfRecord=5–0–1
  |APRank=4
  |HeadCoach=[[Frank Thomas (American football)|Frank Thomas]]
  |HCYear = 6th
  |Captain =James Nesbit
  |StadiumArena=[[Bryant–Denny Stadium|Denny Stadium]]<br/>[[Legion Field]]
}}
{{1936 SEC football standings}}
The '''1936 Alabama Crimson Tide football team''' (variously "Alabama", "UA" or "Bama") represented the [[University of Alabama]] in the [[1936 college football season]]. It was the Crimson Tide's 43rd overall and 4th season as a member of the [[Southeastern Conference]] (SEC). The team was led by head coach [[Frank Thomas (American football)|Frank Thomas]], in his sixth year, and played their home games at [[Bryant–Denny Stadium|Denny Stadium]] in [[Tuscaloosa, Alabama|Tuscaloosa]] and [[Legion Field]] in [[Birmingham, Alabama|Birmingham]], Alabama. They finished the season with a record of eight wins, zero losses and one tie (8–0–1 overall, 5–0–1 in the SEC).

After Alabama opened the season with three consecutive [[shutout]]s against {{cfb link|year=1936|team=Howard Bulldogs|title=Howard}}, [[1936 Clemson Tigers football team|Clemson]] and [[1936 Mississippi State Maroons football team|Mississippi State]], they suffered their only blemish of the season, a scoreless tie against [[1936 Tennessee Volunteers football team|Tennessee]]. The Crimson Tide rebounded to win their final five games against {{cfb link|year=1936|team=Loyola Wolfpack|title=Loyola}}, [[1936 Kentucky Wildcats football team|Kentucky]], [[1936 Tulane Green Wave football team|Tulane]], [[1936 Georgia Tech Yellow Jackets football team|Georgia Tech]] and [[1936 Vanderbilt Commodores football team|Vanderbilt]]. Although they finished undefeated and ranked No. 4 in the final [[AP Poll]], Alabama did not receive an invitation to participate in a postseason [[bowl game]].

==Schedule==
{{CFB Schedule Start | time = no | rankyear = 1936 | tv = no | attend = yes }}
{{CFB Schedule Entry
| w/l          = w
| date         = September 26
| nonconf      = yes
| time         = no
| rank         = 
| opponent     = {{cfb link|year=1936|team=Howard Bulldogs|title=Howard}}
| site_stadium = [[Bryant–Denny Stadium|Denny Stadium]]
| site_cityst  = [[Tuscaloosa, Alabama|Tuscaloosa, AL]]
| tv           = no
| score        = 34–0
| attend       = 8,000
}}
{{CFB Schedule Entry
| w/l          = w
| date         = October 3
| nonconf      = yes
| time         = no
| rank         = 
| opponent     = [[1936 Clemson Tigers football team|Clemson]]
| site_stadium = Denny Stadium
| site_cityst  = Tuscaloosa, AL
| tv           = no
| score        = 32–0
| attend       = 6,000
}}
{{CFB Schedule Entry
| w/l          = w
| date         = October 10
| homecoming   = yes
| time         = no
| rank         = 
| opponent     = [[1936 Mississippi State Maroons football team|Mississippi State]]
| site_stadium = Denny Stadium
| site_cityst  = Tuscaloosa, AL
| gamename     = [[Alabama–Mississippi State football rivalry|Rivalry]]
| tv           = no
| score        = 7–0
| attend       = 17,000
}}
{{CFB Schedule Entry
| w/l          = t
| date         = October 17
| time         = no
| rank         = 
| opponent     = [[1936 Tennessee Volunteers football team|Tennessee]]
| site_stadium = [[Legion Field]]
| site_cityst  = [[Birmingham, Alabama|Birmingham, AL]]
| gamename     = [[Third Saturday in October]]
| tv           = no
| score        = 0–0
| attend       = 15,000
}}
{{CFB Schedule Entry
| w/l          = w
| date         = October 23
| away         = yes
| nonconf      = yes
| time         = no
| rank         = 
| opponent     = {{cfb link|year=1936|team=Loyola Wolfpack|title=Loyola}}
| site_stadium = [[Loyola Stadium]]
| site_cityst  = [[New Orleans|New Orleans, LA]]
| tv           = no
| score        = 13–6
| attend       = 6,000
}}
{{CFB Schedule Entry
| w/l          = w
| date         = October 31
| away         = yes
| time         = no
| rank         = 
| opponent     = [[1936 Kentucky Wildcats football team|Kentucky]]
| site_stadium = [[Stoll Field/McLean Stadium|McLean Stadium]]
| site_cityst  = [[Lexington, Kentucky|Lexington, KY]]
| tv           = no
| score        = 14–0
| attend       = 18,000
}}
{{CFB Schedule Entry
| w/l          = w
| date         = November 7
| time         = no
| rank         = 14
| opprank      = 10
| opponent     = [[1936 Tulane Green Wave football team|Tulane]]
| site_stadium = Legion Field
| site_cityst  = Birmingham, AL
| tv           = no
| score        = 34–7
| attend       = 18,000
}}
{{CFB Schedule Entry
| w/l          = w
| date         = November 14
| away         = yes
| time         = no
| rank         = 4
| opponent     = [[1936 Georgia Tech Yellow Jackets football team|Georgia Tech]]
| site_stadium = [[Bobby Dodd Stadium|Grant Field]]
| site_cityst  = [[Atlanta|Atlanta, GA]]
| tv           = no
| score        = 20–16
| attend       = &nbsp;
}}
{{CFB Schedule Entry
| w/l          = w
| date         = November 26
| time         = no
| rank         = 3
| opponent     = [[1936 Vanderbilt Commodores football team|Vanderbilt]]
| site_stadium = Legion Field
| site_cityst  = Birmingham, AL
| tv           = no
| score        = 14–6
| attend       = 25,000
}}
{{CFB Schedule End
| rank     = 
| timezone = 
| poll     = [[AP Poll]]
| hc       = yes
}}
*<small>Source: Rolltide.com: 1936 Alabama football schedule<ref name="1936schedule">{{cite web| url=http://www.rolltide.com/sports/m-footbl/archive/m-footbl-results-archive.html#1936 |title=1936 Alabama football schedule |work=RollTide.com |publisher=University of Alabama Department of Intercollegiate Athletics| accessdate=April 8, 2012}}</ref></small>

==Game summaries==

===Howard===
{{See also2|{{cfb link|year=1936|team=Howard Bulldogs}}}}
{{AFB game box start
|Visitor=Howard
|V1=0 |V2=0 |V3=0 |V4=0
|Host='''Alabama'''
|H1=0 |H2=6 |H3=14 |H4=14
|Date=September 26
|Location=Denny Stadium<br/>Tuscaloosa, AL
|Attendance=8,000
}}
*'''Source:'''<ref name="HU1">{{cite news |title=Bama gains momentum after uneventful start to whip Howard |url=https://news.google.com/newspapers?id=YOM-AAAAIBAJ&sjid=-UwMAAAAIBAJ&pg=6069%2C3710978 |first=Jay |last=Thornton |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=September 27, 1936 |page=6 |accessdate=April 8, 2012}}</ref>
{{AFB game box end}}
To open the 1936 season, Alabama outgained [[Howard Bulldogs football|Howard]] (now [[Samford University]]) in total yards, 294 to 12, and defeated the Bulldogs 34–0 at Denny Stadium.<ref name="HU1"/><ref name=a1>1936 Season Recap</ref> After a scoreless first, Joe Kilgrow scored on a four-yard touchdown run to give the Crimson Tide a 6–0 lead. Alabama then scored two touchdowns in each of the final two quarters to secure the 34–0 victory. Joe Riley scored both third-quarter touchdowns, first on a 27-yard run and the second on a 26-yard run. Kilgrow then scored both of the fourth-quarter touchdowns on a pair of one-yard runs.<ref name="HU1"/> The victory improved Alabama's all-time record against Howard to 14–0–1.<ref name="HAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Samford |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=2867 |accessdate=April 8, 2012}}</ref>
{{clear}}

===Clemson===
{{See also|1936 Clemson Tigers football team}}
{{AFB game box start
|Visitor=Clemson
|V1=0 |V2=0 |V3=0 |V4=0
|Host='''Alabama'''
|H1=12 |H2=7 |H3=7 |H4=6
|Date=October 3
|Location=Denny Stadium<br>Tuscaloosa, AL
|Attendance=6,000
}}
*'''Source:'''<ref name="CU1">{{cite news |title=Improved Crimson Tide swamps Clemson under wraps, 32–0 |first=Jay |last=Thornton |url=https://news.google.com/newspapers?id=ZeM-AAAAIBAJ&sjid=-UwMAAAAIBAJ&pg=6116%2C4054331 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 4, 1936 |page=8 |accessdate=April 8, 2012}}</ref>
{{AFB game box end}}
For the second week in a row Alabama [[shutout]] their opponent, and this time it was the [[Clemson University|Clemson]] [[Clemson Tigers football|Tigers]] of the [[Southern Conference]] 32–0 in Tuscaloosa.<ref name=a1/><ref name="CU1"/> The Crimson Tide took a 12–0 lead in the first quarter on touchdown runs of 15 yards by Gene Blackwell and of five-yards by Herman Caldwell.<ref name="CU1"/> They would then score a touchdown in each of the three remaining quarters for the 32–0 margin. The touchdown runs were scored by Charlie Holm in the second on a three-yard run, by Young Boozer in the third on a 39-yard run and finally by Joe Kilgrow in the fourth on a five-yard run.<ref name="CU1"/> The victory improved Alabama's all-time record against Clemson to 6–3.<ref name="CUAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Clemson |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=721 |accessdate=April 8, 2012}}</ref>
{{clear}}

===Mississippi State===
{{see also|1936 Mississippi State Maroons football team}}
{{AFB game box start
|Visitor=Mississippi State
|V1=0 |V2=0 |V3=0 |V4=0
|Host='''Alabama'''
|H1=0 |H2=7 |H3=0 |H4=0
|Date=October 10
|Location=Denny Stadium<br/>Tuscaloosa, AL
|Attendance=17,000
}}
*'''Source:'''<ref name="MSS1">{{cite news |title=Crimson Tide subdues Maroons, 7–0, before crowd of 17,000 |first=Jay |last=Thornton |url=https://news.google.com/newspapers?id=aOM-AAAAIBAJ&sjid=-UwMAAAAIBAJ&pg=6208%2C4401621 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 11, 1936 |page=6 |accessdate=April 8, 2012}}</ref>
{{AFB game box end}}
The Crimson Tide entered their annual game against their [[Alabama–Mississippi State football rivalry|long-time rival]], [[Mississippi State University|Mississippi State]] as a slight underdog that sought redemption against the [[Mississippi State Bulldogs football|Maroons]] after their [[1935 Alabama Crimson Tide football team#Mississippi State|20–7 loss the previous year]].<ref name="MSS2">{{cite news |title=Capacity crowd expected for homecoming grid classic Saturday |url=https://news.google.com/newspapers?id=aOM-AAAAIBAJ&sjid=-UwMAAAAIBAJ&pg=6306%2C4352617 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 9, 1936 |page=6 |accessdate=April 8, 2012}}</ref> In what was Alabama's annual [[homecoming]] game, they defeated the Maroons 7–0 at Denny Stadium.<ref name=a1/><ref name="MSS1"/> In a game dominated by both defenses, the only points of the game came in the second quarter. The touchdown was scored by Joe Kilgrow after he received a lateral pass from Joe Riley on a [[fake punt]] and returned it 83-yards for the score. The Alabama defense also starred and made eight [[interception]]s of State passes in the game.<ref name="MSS1"/> The victory improved Alabama's all-time record against Mississippi State to 18–5–2.<ref name="MSSAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Mississippi State |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=2049 |accessdate=April 8, 2012}}</ref>
{{clear}}

===Tennessee===
{{See also|1936 Tennessee Volunteers football team}}
{{AFB game box start
|Title=[[Third Saturday in October]]
|Visitor=Tennessee
|V1=0 |V2=0 |V3=0 |V4=0
|Host=Alabama
|H1=0 |H2=0 |H3=0 |H4=0
|Date=October 17
|Location=Legion Field<br/>Birmingham, AL
|Attendance=15,000
}}
*'''Source:'''<ref name="UT1">{{cite news |title=Vols tumble Tide from untied, unbeaten ranks in 0–0 draw |url=https://news.google.com/newspapers?id=bOM-AAAAIBAJ&sjid=-UwMAAAAIBAJ&pg=6535%2C4711852 |first=Jay |last=Thornton |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 18, 1936 |page=6 |accessdate=April 8, 2012}}</ref>
{{AFB game box end}}
Against [[Third Saturday in October|rival]] [[University of Tennessee|Tennessee]], Alabama battled the [[Tennessee Volunteers football|Volunteers]] to a 0–0 tie at Legion Field.<ref name=a1/><ref name="UT1"/> In a game once again dominated by both defenses, the closest either team came to a score was at the end of the second quarter when Alabama was in position for a touchdown at the Tennessee one-yard line. However, the official call time for the end of the period before Alabama could get another play off.<ref name="UT1"/> The tie brought Alabama's all-time record against Tennessee to 12–5–2.<ref name="TNAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Tennessee |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=3180 |accessdate=April 8, 2012}}</ref>
{{clear}}

===Loyola===
{{See also2|{{cfb link|year=1936|team=Loyola Wolfpack}}}}
{{AFB game box start
|Visitor='''Alabama'''
|V1=6 |V2=0 |V3=7 |V4=0
|Host=Loyola
|H1=6 |H2=0 |H3=0 |H4=0
|Date=October 23
|Location=Loyola Stadium<br>New Orleans, LA
|Attendance=6,000
}}
*'''Source:'''<ref name="LU1">{{cite news |title=Crimson Tide scored on passes to subdue Loyola threat, 13–6 |first=Jay |last=Thornton |url=https://news.google.com/newspapers?id=-IdhAAAAIBAJ&sjid=-UwMAAAAIBAJ&pg=6283%2C5017433 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 25, 1936 |page=8 |accessdate=April 8, 2012}}</ref>
{{AFB game box end}}
In what was the first road game of the season, Alabama made their first trip to [[New Orleans]] since the [[1921 Alabama Crimson Tide football team|1921 season]] and defeated the [[Loyola University New Orleans|Loyola]] [[Loyola Wolfpack football|Wolfpack]] of the [[Dixie Conference]] 13–6 on a Friday evening.<ref name=a1/><ref name="LU1"/><ref name="LU2">{{cite news |title=Crimson Tide squad entrains tonight for Friday Loyola tilt |url=https://news.google.com/newspapers?id=buM-AAAAIBAJ&sjid=-UwMAAAAIBAJ&pg=6285%2C4923620 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 22, 1936 |page=9 |accessdate=April 8, 2012}}</ref> After each team traded punts to start the game, Loyola scored first on a one-yard touchdown run by Clay Calhoun to take a 6–0 lead. Alabama responded on the third play of the drive that ensued when Joe Kilgrow threw a 20-yard pass to Erin Warren that was run an additional 42-yards for a 62-yard touchdown and tied the game at 6–6. After a scoreless second, Alabama scored the game-winning touchdown in the third after they received the ball at the Wolfpack 36-yard line due to a short [[Punt (gridiron football)|punt]].<ref name="LU1"/> On this drive, Joe Riley threw an 18-yard touchdown pass to James Nesbit to secure the win in what was Alabama's only all-time game against Loyola.<ref name="LU1"/><ref name="LUAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Loyola (LA) |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=1817 |accessdate=April 8, 2012}}</ref>
{{clear}}

===Kentucky===
{{See also2|[[1936 Kentucky Wildcats football team]]}}
{{AFB game box start
|Visitor='''Alabama'''
|V1=0 |V2=0 |V3=7 |V4=7
|Host=Kentucky
|H1=0 |H2=0 |H3=0 |H4=0
|Date=October 31
|Location=McLean Stadium<br>Lexington, KY
|Attendance=18,000
}}
*'''Source:'''<ref name="KY1">{{cite news |title=Joe Riley sparkles as Alabama turns back Kentucky, 14 to 0 |agency=Associated Press |url=https://news.google.com/newspapers?id=cOM-AAAAIBAJ&sjid=-UwMAAAAIBAJ&pg=6539%2C5339367 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 1, 1936 |page=6 |accessdate=April 8, 2012}}</ref>
{{AFB game box end}}
On what was their [[homecoming]] game, Alabama defeated the [[University of Kentucky|Kentucky]] [[Kentucky Wildcats football|Wildcats]] 14–0 at McLean Stadium.<ref name=a1/><ref name="KY1"/> After a scoreless first half that saw the Kentucky defense hold Alabama out of the endzone on four occasions from within their own ten-yard line, the Crimson Tide scored two second half touchdowns to win the game.<ref name="KY1"/> Joe Riley scored both Alabama touchdowns on a 27-yard run in the third and on a 21-yard [[Reverse (American football)|reverse]] in the fourth.<ref name="KY1"/> The victory improved Alabama's all-time record against Kentucky 15–1.<ref name="KYAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Kentucky |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=1628 |accessdate=April 8, 2012}}</ref>
{{clear}}

===Tulane===
{{see also|1936 Tulane Green Wave football team}}
{{AFB game box start
|Visitor= #10 Tulane
|V1=7 |V2=0 |V3=0 |V4=0
|Host= #14 '''Alabama'''
|H1=14 |H2=7 |H3=7 |H4=6
|Date=November 7
|Location=Legion Field<br/>Birmingham, AL
|Attendance=18,000
}}
*'''Source:'''<ref name="TU1">{{cite news |title=Crimson Tide reaches season's peak in Tulane romp, 34 to 7 |url=https://news.google.com/newspapers?id=cuM-AAAAIBAJ&sjid=-UwMAAAAIBAJ&pg=6085%2C5668300 |first=Jay |last=Thornton |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 8, 1936 |page=7 |accessdate=April 8, 2012}}</ref>
{{AFB game box end}}
In what was the first game Alabama was ranked and played against a ranked opponent, the Crimson Tide defeated the [[Tulane University|Tulane]] [[Tulane Green Wave football|Green Wave]] 34–7 at Legion Field.<ref name=a1/><ref name="TU1"/> The Greenies scored first on a 42-yard, Bill Mathis touchdown run in the first quarter to take an early 7–0 lead. However, Alabama responded with five unanswered touchdowns to win the game 34–7. In the first quarter, the Crimson Tide scored on a 54-yard Joe Riley touchdown run and followed that with a 17-yard Joe Kilgrow touchdown pass to Perron Shoemaker to give Alabama a 14–7 lead after one. After Kilgrow threw his second touchdown pass of the game to Erin Warren in the second, James Nesbit scored on a two-yard run to give the Crimson Tide a 28–7 leas as they entered the fourth quarter. In the fourth, Alabama's final points were scored when  [[Leroy Monsky]] [[Interception|intercepted]] a Tulane pass and returned it 25-yards for a touchdown.<ref name="TU1"/> The victory improved Alabama's all-time record against Tulane to 10–3–1.<ref name="TUAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Tulane |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=3267 |accessdate=April 8, 2012}}</ref>

This game was noted for being the first regular season Alabama game that was [[Broadcasting of sports events|broadcast]] nationally on two major radio networks.<ref name="TU2">{{cite news |title=Two chains to broadcast Tide game on national hook-up |url=https://news.google.com/newspapers?id=cOM-AAAAIBAJ&sjid=-UwMAAAAIBAJ&pg=6572%2C5464509 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 3, 1936 |page=7 |accessdate=April 8, 2012}}</ref> It was broadcast by [[CBS]] with [[Ted Husing]] as the announcer and by [[NBC]] with [[Bill Slater (broadcaster)|Bill Slater]] as the announcer.<ref name="TU2"/> The national broadcasts were made due to both teams being undefeated and ranked in the [[AP Poll]] as they entered the game.<ref name="TU2"/> The game was also the first in which the Crimson Tide defeated a ranked opponent.<ref>{{cite book |title=2011 Alabama Crimson Tide Football Record Book |year=2011 |chapter=Alabama vs. Ranked Teams |publisher=University of Alabama Athletics Media Relations Office |location=Tuscaloosa, Alabama |page=128}}</ref>
{{clear}}

===Georgia Tech===
{{see also|1936 Georgia Tech Yellow Jackets football team}}
{{AFB game box start
|Visitor= #4 '''Alabama'''
|V1=7 |V2=13 |V3=0 |V4=0
|Host=Georgia Tech
|H1=0 |H2=0 |H3=9 |H4=7
|Date=November 14
|Location=Grant Field<br>Atlanta, GA
}}
*'''Source:'''<ref name="GT1">{{cite news |title=Tide weathers late spree to sweep past Georgia Tech 20–16 |first=Ben |last=Green |url=https://news.google.com/newspapers?id=eOM-AAAAIBAJ&sjid=-UwMAAAAIBAJ&pg=6227%2C5990318 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 15, 1936 |page=8 |accessdate=April 8, 2012}}</ref>
{{AFB game box end}}
Although outgained in total yardage against [[Georgia Institute of Technology|Georgia Tech]], Alabama defeated the [[Georgia Tech Yellow Jackets football|Yellow Jackets]] 20–16 at Grant Field.<ref name=a1/><ref name="GT1"/> After the Crimson Tide scored on a Joe Kilgrow touchdown pass to Herman Caldwell in the first, Alabama scored two second-quarter touchdowns to take a 20–0 halftime lead. In the second, James Nesbit scored on a two-yard run and Kilgrow threw a 13-yard touchdown pass to Perron Shoemaker. The Yellow Jackets scored their first points early in the third after Hal Hughes was tackled for a [[Safety (American and Canadian football score)|safety]] on a [[Punt (gridiron football)|punt]] attempt. On the possession that ensued, T. F. Sims threw a 45-yard touchdown pass to M. J. Konemann to cut the Crimson Tide lead to 20–9. H. H. Appleby then scored the final points of the game with his short touchdown run in the fourth on a drive that featured a 71-yard run by Konemann.<ref name="GT1"/> The victory improved Alabama's all-time record against Georgia Tech to 10–10–2.<ref name="GTAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Georgia Tech |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=1273 |accessdate=April 8, 2012}}</ref>
{{clear}}

===Vanderbilt===
{{see also|1936 Vanderbilt Commodores football team}}
{{AFB game box start
|Visitor=Vanderbilt
|V1=6 |V2=0 |V3=0 |V4=0
|Host= #3 '''Alabama'''
|H1=0 |H2=0 |H3=7 |H4=7
|Date=November 26
|Location=Legion Field<br>Birmingham, AL
|Attendance=25,000
}}
*'''Source:'''<ref name="VU1">{{cite news |title=Crimson Tide ends unbeaten season, submerging Vandy 14–6 |first=Jay |last=Thornton  |url=https://news.google.com/newspapers?id=g-M-AAAAIBAJ&sjid=-UwMAAAAIBAJ&pg=6603%2C6584971 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 27, 1936 |page=9 |accessdate=April 8, 2012}}</ref>
{{AFB game box end}}
In the season finale on [[Thanksgiving (United States)|Thanksgiving Day]], Alabama defeated the [[Vanderbilt University|Vanderbilt]] [[Vanderbilt Commodores football|Commodores]] 14–6 defeat at Legion Field.<ref name=a1/><ref name="VU1"/> Vanderbilt scored first after the received the opening [[Kickoff (American football)|kickoff]] when Herbert Plasman scored on a ten-yard touchdown on their opening drive.<ref name="VU1"/> With the score still 6–0 in favor of the Commodores after halftime, the Crimson Tide scored a pair of second half touchdowns to win the game 14–6. In the third, Joe Kilgrow thew a 26-yard touchdown pass to Ben McLeod and in the fourth on a 12-yard Joe Riley touchdown pass to Kilgrow.<ref name="VU1"/> The victory improved Alabama's all-time record against Vanderbilt to 8–9.<ref name="VUAT">{{Cite web |last=DeLassus |first=David |title=Alabama vs Vanderbilt |publisher=College Football Data Warehouse |url=http://www.cfbdatawarehouse.com/data/div_ia/sec/alabama/opponents_records.php?teamid=3363 |accessdate=April 8, 2012}}</ref>
{{clear}}

==After the season==
Following their victory over Vanderbilt in the season finale, Alabama was in contention (along with [[1936 Pittsburgh Panthers football team|Pittsburgh]] and [[1936 LSU Tigers football team|LSU]]) for a place in the [[1937 Rose Bowl]] opposite [[1936 Washington Huskies football team|Washington]].<ref name="Rose1">{{cite news |title=Eastern Rose Bowl selection delayed till late in the week |agency=Associated Press |url=https://news.google.com/newspapers?id=hOM-AAAAIBAJ&sjid=-UwMAAAAIBAJ&pg=5773%2C6593204 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 29, 1936 |page=1 |accessdate=April 8, 2012}}</ref> In addition to the Rose, Alabama was also under consideration to compete in the [[1937 Sugar Bowl]] along with the aforementioned Pittsburgh, LSU and [[1936 Santa Clara Broncos football team|Santa Clara]].<ref name="Sugar1">{{cite news |title=Alabama included in Sugar Bowl prospects |agency=Associated Press |url=https://news.google.com/newspapers?id=hOM-AAAAIBAJ&sjid=-UwMAAAAIBAJ&pg=3918%2C6591358 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 29, 1936 |page=1 |accessdate=April 8, 2012}}</ref> However, on December 3, Rose Bowl officials announced the selection of Pittsburgh and Sugar Bowl officials announced their matchup of LSU and Santa Clara.<ref name="Rose2">{{cite news |title=Santa Clara and L.S.U. will clash in New Orleans |agency=Associated Press |url=https://news.google.com/newspapers?id=ieM-AAAAIBAJ&sjid=-UwMAAAAIBAJ&pg=5413%2C6877634 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=December 4, 1936 |page=1 |accessdate=April 8, 2012}}</ref> This resulted in Alabama not playing in a [[bowl game]] despite an undefeated record of 8–0–1 and a final poll ranking of No. 4.

===Awards===
After the season, James Nesbit and [[Art White|Arthur "Tarzan" White]] selected to various [[1936 College Football All-America Team]]s.<ref>{{cite book |title=2011 Alabama Crimson Tide Football Record Book |year=2011 |chapter=First-Team All-America |publisher=University of Alabama Athletics Media Relations Office |location=Tuscaloosa, Alabama |pages=163–172}}</ref>

===NFL Draft===
Several players that were [[Letterman (sports)|varsity lettermen]] from the 1936 squad were drafted into the [[National Football League Draft|National Football League (NFL)]] between the 1937 and 1939 drafts.<ref>{{cite web |url=http://www.pro-football-reference.com/colleges/alabama/drafted.htm |title=Alabama Drafted Players/Alumni |accessdate=April 7, 2012 |work=Sports Reference, LLC |publisher=Pro-Football-Reference.com}}</ref><ref name="NFLDraft">{{cite web |publisher=National Football League | url=http://www.nfl.com/draft/history/fulldraft?abbr=A&collegeName=Alabama&abbrFlag=0&type=school | title=Draft History by School–Alabama|accessdate=March 16, 2013}}</ref> These players included the following:
{| class="wikitable sortable" style="text-align:center"
! scope="col" | Year
! scope="col" | Round
! scope="col" | Overall
! scope="col" | Player name
! scope="col" | Position
! scope="col" | NFL team
|-
| [[1937 NFL Draft|1937]]
| 2 
| 14 
! {{Sortname|Arthur|White|Art White}}
| Back 
| [[1937 New York Giants season|New York Giants]]
|-
| rowspan=2|[[1938 NFL Draft|1938]]
| 2 
| 13 
! {{Sortname|Joe|Kilgrow|nolink=1}}
| Back 
| [[1938 Brooklyn Dodgers (NFL) season|Brooklyn Dodgers]]
|-
| 7 
| 53 
! {{Sortname|Leroy|Monsky}}
| Guard 
| Brooklyn Dodgers
|-
| [[1939 NFL Draft|1939]]
| 9 
| 73 
! {{Sortname|Lew|Bostick|nolink=1}}
| Guard 
| [[1939 Cleveland Rams season|Cleveland Rams]]
|}

==Personnel==
The 1936 coaching staff included former player [[Bear Bryant]] in his first year at a coaching position.<ref name="Coaches">{{cite book |title=2011 Alabama Crimson Tide Football Record Book |year=2011 |chapter=All-Time Assistant Coaches |publisher=University of Alabama Athletics Media Relations Office |location=Tuscaloosa, Alabama |pages=142–143}}</ref><ref>{{cite book |title=Coach: The Life of Paul "Bear" Bryant |last=Dunnavant |first=Keith |year=2005 |publisher=St. Martin's Press |location=New York, New York |isbn=9780312348762 |page=50 |url=https://books.google.com/books?id=GIl8s3l0RY8C&source=gbs_navlinks_s |accessdate=April 8, 2012}}</ref> Bryant came back to Alabama after serving as an assistant coach at [[Union University|Union]] for their spring practices in early 1936.<ref>{{cite book |title=The Titan of Tuscaloosa: The Tie Games and Career of Paul Bear Bryant |last=Shepard |first=David |year=2002 |publisher=iUniverse |location=Bloomington, Indiana |isbn=9780595243259 |pages=7–9 |url=https://books.google.com/books?id=gTNgcTyZAdIC&source=gbs_navlinks_s |accessdate=April 9, 2012}}</ref> He later went on to serve as head coach at [[Maryland Terrapins football|Maryland]], [[Kentucky Wildcats football|Kentucky]], [[Texas A&M Aggies football|Texas A&M]] and at Alabama. During his career Bryant won 323 games, appeared in 29 bowl games, won 15 conference championships and six national championships.<ref>{{cite web |url=http://espn.go.com/classic/biography/s/Bryant_Bear.html |title=Bear Bryant 'simply the best there ever was' |first=Mike |last=Puma |date=2007 |work=ESPN Internet Ventures |publisher=ESPN |accessdate=April 8, 2012}}</ref>

{{Col-begin}}
{{Col-2}}

===Varsity letter winners===
{| class="wikitable" border="1"
|-;
! Player
! Hometown
! Position
|-
| Young Boozer
| [[Dothan, Alabama]]
| [[Halfback (American football)|Halfback]]
|-
| Lewis Bostick
| [[Birmingham, Alabama]]
| [[Guard (American football)|Guard]]
|-
| Vic Bradford
| [[Memphis, Tennessee]]
| [[Quarterback]]
|-
| Herman Caldwell
| [[Tallassee, Alabama]]
| [[Back (American football)|Back]]
|-
| Joe Kilgrow
| [[Montgomery, Alabama]]
| [[Halfback (American football)|Halfback]]
|-
| Ben McLeod
| [[Leeksville, Mississippi]]
| [[Halfback (American football)|Halfback]]
|-
| [[Leroy Monsky]]
| [[Montgomery, Alabama]]
| [[Guard (American football)|Guard]]
|-
| Lamar Moyle
| [[Decatur, Alabama]]
| [[Center (American football)|Center]]
|-
| James Nesbet
| [[Bainbridge, Georgia]]
| [[Fullback (American football)|Fullback]]
|-
| William Peters
| [[Hammond, Indiana]]
| [[Guard (American football)|Guard]]
|-
| James Radford
| [[Hartford, Alabama]]
| [[Tackle (American football)|Tackle]]
|-
| Joe Riley
| [[Dothan, Alabama]]
| [[Halfback (American football)|Halfback]]
|-
| [[Hayward Sanford]]
| [[Plainview, Arkansas]]
| [[End (American football)|End]]
|-
| Joe Shepherd
| [[Tuscaloosa, Alabama]]
| [[Guard (American football)|Guard]]
|-
| Jim Tipton
| [[Blytheville, Arkansas]]
| [[Tackle (American football)|Tackle]]
|-
| Hilmon Walker
| [[Hattiesburg, Mississippi]]
| [[End (American football)|End]]
|-
| [[Art White|Arthur "Tarzan" White]]
| [[Atmore, Alabama]]
| [[Guard (American football)|Guard]]
|-
| [[Bill Young (offensive lineman)|Bill Young]]
| [[Pine Bluff, Arkansas]]
| [[Tackle (American football)|Tackle]]
|-
|colspan="3" style="font-size: 8pt" align="center"|'''Reference:'''<ref>{{cite book |title=2011 Alabama Crimson Tide Football Record Book |year=2011 |chapter=All-Time Tide Football Lettermen |publisher=University of Alabama Athletics Media Relations Office |location=Tuscaloosa, Alabama |pages=127–141}}</ref>
|}
{{Col-2}}

===Coaching staff===
{| class="wikitable" border="1" style="font-size:90%;"
|-
! Name !! Position !! Seasons at<br />Alabama !! Alma Mater
|-
| [[Frank Thomas (American football)|Frank Thomas]] || [[Head coach]] ||align=center| 6 || [[Notre Dame Fighting Irish football|Notre Dame]] (1923)
|-
| [[Bear Bryant]] || Assistant coach ||align=center| 1 || [[Alabama Crimson Tide football|Alabama]] (1935)
|-
| [[Paul Burnum]] || Assistant coach ||align=center| 7 || [[Alabama Crimson Tide football|Alabama]] (1922)
|-
| [[Hank Crisp]] || Assistant coach ||align=center| 16 || [[Virginia Tech Hokies football|VPI]] (1920)
|-
| [[Tilden Campbell]] || Assistant coach ||align=center| 1 || [[Alabama Crimson Tide football|Alabama]] (1935)
|-
| Jim Dildy || Assistant coach ||align=center| 2 || [[Alabama Crimson Tide football|Alabama]] (1934)
|-
| [[Harold Drew]] || Assistant coach ||align=center| 6 || [[Bates Bobcats football|Bates]] (1916)
|-
|colspan="4" style="font-size: 8pt" align="center"|'''Reference:'''<ref name="Coaches"/>
|}
{{Col-2}}
{{Col-end}}

==References==
'''General'''
{{refbegin}}
* {{cite web |url=http://www.rolltide.com/datadump/fls_files/files/football/1930s/1936.pdf |title=1936 Season Recap |work=RollTide.com |publisher=University of Alabama Department of Intercollegiate Athletics |accessdate=April 8, 2012 |format=PDF}}
{{refend}}

'''Specific'''
{{Reflist|30em}}

{{Alabama Crimson Tide football navbox}}

[[Category:Alabama Crimson Tide football seasons]]
[[Category:1936 Southeastern Conference football season|Alabama]]
[[Category:College football undefeated seasons]]
[[Category:1936 in Alabama|Crimson Tide]]