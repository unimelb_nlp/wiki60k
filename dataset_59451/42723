{{Use British English|date=March 2013}}
{|{{Infobox Aircraft Begin
|name           = EA-7 Optica
|image          = File:Edgley Optica Sywell 1.jpg<!--in the ''Image:filename'' format with no image tags-->
|caption        = Edgley Optica G-BOPO at the 2008 [[Sywell Aerodrome|Sywell]] Airshow
}}{{Infobox Aircraft Type
|type           = [[Observation aircraft|Observation]]
|national origin = United Kingdom
|manufacturer   = Edgley
|designer       = John Edgley
|first flight   = 14 December 1979
|introduction   = 
|retired        = 
|status         = 
|primary user   = 
|more users     = 
|produced       = 
|number built   = 22
|program cost   = 
|unit cost      = 
|developed from = 
|variants with their own articles = 
|developed into =
}}
|}

The '''Edgley EA-7 ''Optica''''' is a [[United Kingdom|British]] light [[aircraft]] designed for low-speed [[Observation aircraft|observation]] work, and intended as a low-cost alternative to [[helicopter]]s. The ''Optica'' has a loiter speed of {{convert|130|km/h|knots mph|abbr=on}} and a stall speed of {{convert|108|km/h|knots mph|abbr=on}}.

== Design and development ==

[[File:Brooklands-Aerospace Optica Srs 301.jpg|thumb|[[Farnborough Airshow|Farnborough]], 1990]]

[[File:Optica at Old Warden2.jpg|thumb|[[Old Warden Aerodrome|Old Warden]], June 2014]]

The Optica project began in 1974 with a company, Edgley Aircraft Limited, formed by John Edgley who, with a small team, designed and built the original prototype. In 1982, institutional investors bought into the project and set up a production line at [[Old Sarum Airfield]] in Wiltshire. Over the next three years, the company was built up to full manufacturing capability, the aircraft received UK certification, and the first customer aircraft was delivered. Despite this success, the additional investment necessary for the final phase of full production was not forthcoming, the business went into receivership, and John Edgley was forced out. With new owners aircraft on the production line were completed, and the Optica entered service.

The aircraft has an unusual configuration with a fully glazed forward cabin, reminiscent of an [[Aérospatiale Alouette III|Alouette helicopter]], that provides 270° panoramic vision and almost vertical downward vision for the pilot and two passengers. The aircraft has twin booms with twin rudders and a high-mounted tailplane. It is powered by a [[Lycoming O-540|Lycoming]] flat-six normally-aspirated engine situated behind the cabin and driving a fixed pitch [[ducted fan]]. Due to the ducted fan, the aircraft is exceptionally quiet. The aircraft has a fixed tricycle undercarriage with the nosewheel offset to the left. The wings are [[Wing configuration#Wing sweep|unswept]] and [[Wing configuration#Chord variation along span|untapered]]. The aircraft is of fairly standard all-metal construction with [[stressed skin]] of [[aluminium]].

The aircraft's distinctive appearance has led to it being known as the "bug-eye" in some popular reports.<ref name="Bug p1591">''Flight International'' 12 May 1979, p. 1591.</ref>

== Operational history ==
The Optica, powered by a 160&nbsp;hp (119&nbsp;kW) [[Lycoming IO-320]] engine, made its maiden flight on 14 December 1979<ref name="Bug p1594">''Flight International'' 12 May 1979, p.1594.</ref> when it was flown by Squadron Leader Angus McVitie, the chief pilot of the [[Cranfield School of Engineering|Cranfield College of Aeronautics]].<ref>[http://www.flightglobal.com/pdfarchive/view/1979/1979%20-%204689.html ''Flight International'' 29 December 1979, p.2111]</ref>

The Optica, upgraded to the more powerful [[Lycoming IO-540]],<ref name="Production p1111">''Flight International'' 21 April 1984,p.1111.</ref> entered production in 1983.  Edgley Aircraft Limited obtained its initial Civil Aviation Authority certification on 8 February 1985.<ref name="Janes p294">Taylor 1988, p.294.</ref>

A total of 22 Opticas have been manufactured, and construction of a twenty-third began but was not completed.<ref>{{cite web|url=https://www.flightglobal.com/news/articles/farnborough-edgley-seeks-buyer-for-optica-observati-401484|work=[[Flightglobal]] ''website''|title=FARNBOROUGH: Edgley seeks buyer for Optica observation aircraft|first=Kate|last=Sarsfield|date=16 July 2014}}</ref> Ten aircraft were destroyed in a fire at the factory.

The Optica went through several changes of ownership, until FLS Aerospace (Lovaux Ltd) took over the rights, together with the design and manufacturing rights to the Sprint: a two-seat ab-initio trainer that had been designed by Sydney Holloway in Cornwall UK at about the same time as the Optica. Lovaux had intended to develop both aircraft, with the Sprint intended as the military trainer for the UK forces.  Unfortunately, the Sprint was not adopted for this role, and Lovaux cancelled both projects.

The Optica and the Sprint together then passed through other owners until, in 2007, they were offered to John Edgley who formed a new company, AeroElvira Limited, with three former employees of Edgley Aircraft (Chris Burleigh, Fin Colson and Dave Lee) who at that time were working on both projects for the then-owners.  The new company successfully put G-BOPO back into service as a UK demonstrator, with a first return-to-service flight on 3 June 2008.<ref>[http://www.aeroelvira.co.uk/index.html Aero Elvira (Design Owners) - www.aeroelvira.co.uk] Access date: 23 March 2008</ref><ref>"[http://www.flyer.co.uk/news/newsfeed.php?artnum=778 Optica flies again in UK]". ''Flyer.co.uk''. 5 August 2008. Retrieved 25 August 2008.</ref>  In August 2016 Interflight Global plans to start a valuation of the dormant Optica programme with a view to relaunching production.<ref>{{Cite news|url=https://www.flightglobal.com/news/articles/farnborough-optica-moves-a-stop-closer-to-relaunch-427435/|title=FARNBOROUGH: Optica moves a stop closer to relaunch|last=|first=|date=13 July 2016|work=|newspaper=Flightglobal.com|access-date=23 October 2016|via=}}</ref>

== Accidents and incidents ==
On 15 May 1985, Optica G-KATY crashed, killing its Hampshire Constabulary pilot and his photographer passenger.<ref>[http://www.hants.gov.uk/hchs/optica.html Hampshire Council]</ref> The UK Department of Transport [[Air Accidents Investigation Branch]] found, inter alia, that: "There was no indication that either structural or mechanical failure had occurred or of flying control malfunction or jamming." and that "The final loss of control was caused by either the aircraft stalling in a turn at a high angle of bank, or the nose dropping or {{sic|inadver|tant|expected=inadvertent}} interference with the controls by the photographer alarmed by his apparent insecurity."<ref>{{cite web|last1=McKinley|first1=R C|title=Report on the accident to Edgley Optica G-KATY at Ringwood, Hampshire, on 15 May 1985|url=https://assets.digital.cabinet-office.gov.uk/media/5422f939ed915d137400072f/1-1986_G-KATY.pdf|website=cabinet-office.gov.uk|publisher=[[HMSO]]|date=1986|accessdate=2 January 2016|location=London|page=24}}, paras 6 & 10</ref>

On 11 March 1990, G-BMPL, while in flight, sustained damage to the ducted fan and hub assembly and minor airframe damage. The pilot performed a successful forced approach landing: there were no injuries and no further damage to the aircraft. A subsequent investigation discovered cracks resulting from metal fatigue on the fan hub. The manufacturer issued a service bulletin calling for a hub inspection before further flight, and the [[Civil Aviation Authority (United Kingdom)| UK Civil Aviation Authority]] issued a Mandatory Airworthiness Notice (No. 004-05-90). The Optica fan has now been replaced by one designed and manufactured by [[Hoffmann Propeller]].<ref>{{cite book | url=http://www.aaib.gov.uk/cms_resources.cfm?file=/Brooklands%20Aerospace%20Group%20plc%20OA7%20Optica,%20G-BMPL%2010-90.pdf | title=Brooklands Aerospace Group plc OA7 Optica, G-BMPL | author=[[Air Accidents Investigation Branch]] | year=1990}}</ref>

== Specifications ==
{{aircraft specifications

<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->

<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop

|ref= Jane's All the World's Aircraft, 1988–1989<ref name="Janes88-89">Taylor 1988, p. 294–5</ref>
|crew= one pilot
|capacity=two passengers
|length main= 8.15 m
|length alt= 26 ft 9 in
|span main= 12.0 m
|span alt= 39 ft 4 in
|height main= 2.31 m
|height alt= 7 ft 7 in
|area main= 15.8 m²
|area alt= 171 ft²
|airfoil= NASA GA(W)-1
|empty weight main= 948 kg
|empty weight alt= 2,090 lb
|loaded weight main=
|loaded weight alt=
|useful load main= 367 kg
|useful load alt= 810 lb
|max takeoff weight main= 1,315 kg
|max takeoff weight alt= 2,900 lb
|more general=

|engine (prop)=Textron [[Lycoming O-540|Lycoming IO-540-V4A5D]]
|type of prop= flat six piston engine
|number of props=1
|power main= 194 kW
|power alt= 200 hp
|power original=
|propeller or rotor?=propeller
|propellers=five bladed ducted fan
|number of propellers per engine=1
|propeller diameter main=
|propeller diameter alt=
|max speed main= 213 km/h
|max speed alt= 115 knots, 132 mph
|cruise speed main= 130 km/h
|cruise speed alt= 70 knots, 81 mph
|cruise speed more= "loiter speed" at 40% power
|stall speed main= 108 km/h
|stall speed alt= 58 knots, 67  mph
|never exceed speed main= 259 km/h
|never exceed speed alt= 140 knots, 161  mph
|range main= 1,056 km
|range alt= 570 nm, 656 mi
|ceiling main= 4,275 m
|ceiling alt= 14,000 ft
|climb rate main= 4.12 m/s
|climb rate alt= 810 ft/min
|loading main= 83.0 kg/m²
|loading alt= 17.0 lb/ft²
|thrust/weight=
|power/mass main= 0.148 kW/kg
|power/mass alt= 0.0897 hp/lb
|endurance= 8 h (at loiter speed)
|armament=
|avionics= 1 × IR/Camera Turret in recessed bay in nose. 1 x Skyshout Loudspeaker (Police Equipment)

}}

== See also ==
{{aircontent

|related=

|similar aircraft=
*[[HB-Flugtechnik HB-23|Brdischka HB23 Scanliner]]
*[[Heston JC.6]]
*[[Seabird Seeker]]

|lists=

|see also=

}}

== References ==
===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
*{{cite magazine |title=Optica—the bug-eyed observer | date =12 May 1979
|magazine=[[Flight International]] |volume=115 |number=3660 |pages=1591–1594 |publisher=IPC Business Press |id= |url= http://www.flightglobal.com/pdfarchive/view/1979/1979%20-%201661.html}}
*{{cite magazine  | date = 21 April 1984  | title = Optica enters production |magazine= [[Flight International]] | pages =1111–1114 | url = http://www.flightglobal.com/pdfarchive/view/1984/1984%20-%200733.html }}
*{{cite magazine |date=30 August 1986 |title= No cause found for Optica crash|magazine=[[Flight International]] |page=54 | url = http://www.flightglobal.com/pdfarchive/view/1986/1986%20-%202116.html }}
*{{cite book|author=Donald, David (Editor)|title = The Encyclopedia of World Aircraft|year= 1997|publisher = Aerospace Publishing|isbn = 1-85605-375-X}}
*{{cite book| last = Taylor| first = JWR (Editor)| title = Jane's All the World's Aircraft, 1988-1989 | year = 1988| publisher = Jane's Information Group| language = | isbn =0-7106-0867-5 }}
{{refend}}

== External links ==
{{external media
| align  = right
| image1 = [http://www.airliners.net/search/photo.search?regsearch=G-BMPL&distinct_entry=true (G-BMPL)]
| image2 = [http://www.airliners.net/search/photo.search?regsearch=G-BGMW&distinct_entry=true (G-BGMW)]
| image3 = [http://www.airliners.net/search/photo.search?regsearch=G-BOPN&distinct_entry=true (G-BOPN)]
}}
*{{Commons category-inline|Edgley Optica}}
*[http://www.flightglobal.com/pdfarchive/view/1980/1980%20-%203800.html Edgley Optica in the air] ''Flight International'' 1980
*[http://www.flightglobal.com/pdfarchive/view/1985/1985%20-%200741.html Optica Revisited] ''Flight International'' 1985
*[http://www.aeroelvira.co.uk/ AeroElvira]
*[http://www.optica.co.uk/ Optica]

{{Use dmy dates|date=January 2011}}

[[Category:British special-purpose aircraft 1980–1989]]
[[Category:Edgley aircraft]]
[[Category:Ducted fan-powered aircraft]]
[[Category:Monoplanes]]