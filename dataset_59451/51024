{{Infobox publisher
| image        = [[Image:Cornell-University-Press.png|220px|Cornell University Press]]
| parent       = [[Cornell University]]
| status       = 
| founded      = 1869
| founder      = 
| successor    = 
| country      = [[United States]]
| headquarters = [[Ithaca, New York]]
| distribution = 
| keypeople    = 
| publications = [[Book]]s
| topics       = 
| genre        = 
| imprints     = ILR Press
| revenue      = 
| numemployees = 
| nasdaq       = 
| url          = {{URL|cornellpress.cornell.edu}}
}}
[[File:ASA conference 2008 - 10.JPG|thumb|2008 conference booth]]
The '''Cornell University Press''', is a division of [[Cornell University]] housed in Sage House, the former residence of [[Henry W. Sage|Henry William Sage]]. It was first established in 1869 but inactive from 1884 to 1930, making it the first university [[publishing]] enterprise in the United States.<ref name=bishop127>{{Cite book|title=A History of Cornell|first=Morris|last=Bishop|publisher=Cornell University Press|year=1962|isbn=978-0-8014-0036-0|page=127|location=Ithaca, NY}}</ref><ref name="press">{{Cite web| url = http://www.cornellpress.cornell.edu/cup8_presshistory.html | title = The History of the Cornell University Press | publisher = Cornell University Press | accessdate = 2006-01-01}}</ref> 

The press was established in the College of the Mechanic Arts (as [[mechanical engineering]] was called in the 19th century) because engineers knew more about running [[steam-power]]ed [[printing press]]es than literature professors.<ref>{{Cite book|title=A History of Cornell|first=Morris|last=Bishop|publisher=Cornell University Press|year=1962|isbn=978-0-8014-0036-0|page=96|location=Ithaca, NY}}</ref> Since its inception,<ref name=bishop127/> the press has offered work-study [[financial aid (educational expenses)|financial aid]]: students with previous training in the printing trades were paid for [[typesetting]] and running the presses that printed textbooks, pamphlets, a weekly student journal, and official university publications.<ref>{{Cite book|title=A History of Cornell|first=Morris|last=Bishop|publisher=Cornell University Press|year=1962|isbn=978-0-8014-0036-0|pages=175–76|location=Ithaca, NY}}</ref>

Today, the press is one of the country's largest [[university press]]es.<ref name="factbook">{{Cite web| url = http://www.cornell.edu/about/facts/cornell_facts.pdf | title = 2009–10 Factbook | publisher = Cornell University | format = PDF | accessdate = 2009-12-27}}</ref> It produces approximately 150 nonfiction titles each year in various disciplines, including anthropology, Asian studies, biological sciences, classics, history, industrial relations, literary criticism and theory, natural history, philosophy, politics and international relations, veterinary science, and women's studies.<ref name="press" /><ref>{{Cite web| url = http://www.cornellpress.cornell.edu/cup8_authors.html | title = Cornell University Press: Information for Authors | publisher = Cornell University Press | accessdate = 2006-06-06}}</ref> Although the press has been subsidized by the university for most of its history, it is now largely dependent on book sales to finance its operations.<ref name=sun/>

In 2010, the [[Mellon Foundation]], whose President [[Don Michael Randel]] is a former Cornell [[provost (education)|Provost]], awarded to the press a $50,000 grant to explore new business models for publishing scholarly works in low-demand humanities subject areas. With this grant, a book series was published titled "Signale: Modern German Letters, Cultures, and Thoughts."  Only 500 hard copies of each book in the series will be printed, with extra copies manufactured on demand once the original supply is depleted.<ref name=sun>{{cite news|url=http://cornellsun.com/section/news/content/2010/09/21/tough-market-university-press-aims-streamline-production|title=In a Tough Market, University Press Aims to Streamline Production|work=Cornell Daily Sun|first=Jackie|last=Lam|date=September 21, 2010|accessdate=2010-09-22}}</ref>

==See also==
* [[:Category:Cornell University Press books]]

==References==
<references/>

==External links==
*[http://www.cornellpress.cornell.edu/ Cornell University Press Online]

{{Cornell}}
{{Authority control}}

[[Category:Cornell University|Press]]
[[Category:Publishing companies established in 1869]]
[[Category:Book publishing companies based in New York]]
[[Category:University presses of the United States]]
[[Category:1869 establishments in New York]]