{{Notability|date=December 2009}}

The '''''Bulletin of the [[Ireland|Irish]] Biogeographical Society''''' ({{ISSN|0332-1185}}, {{OCLC|14151931}}) publishes many scientific papers on [[entomology]] and also entomological catalogues as Occasional Supplements . A full indexed list is provided on the website.

The Irish Biogeographical Society was founded in 1975. It encourages the study of Irish [[biodiversity]] especially the distribution of the plants and animals. The Society is also interested in the [[biogeographical]] implications of colonization and history. From its [[inception]], the Society has published the annual Bulletin. In 2007, the thirty-first issue was produced. The series now contains an impressive [[data-base]] including information on [[Biota (ecology)|biota]] outside Ireland. In 1986, a series of Occasional Publications was initiated. The tenth volume appeared in 2007. The Occasional Publications provide a useful means of publishing the proceedings of conferences or large articles on particular groups. In 2005, another series (Macro Series) was established and it is suitable for major checklists and bibliographies. Two titles, First Supplement to A Bibliography of Irish Entomology and An Annotated Checklist of the Irish Butterflies and Moths (Lepidoptera), have now appeared in the new series.

==See also==
*[[List of entomology journals]]
*[[Algae]]
*[[Denticollis linearis]]
*[[Pegomya geniculata]]

==References==
{{reflist}}

==External links==
*[http://www.irishbiogeographicalsociety.com/ Website of The Irish Biogeographical Society]

[[Category:Biology journals]]
[[Category:Entomology journals and magazines]]
[[Category:Publications established in 1975]]
[[Category:Academic journals published by learned and professional societies]]


{{zoo-journal-stub}}