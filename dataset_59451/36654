{{Infobox Governor
|name = Samuel Turell Armstrong
|image = Samuel Turell Armstrong.png
|birth_date = {{birth date|1784|4|29|mf=y}}
|birth_place = [[Dorchester, Massachusetts]]
|death_date = {{death date and age|1850|3|26|1784|4|29|mf=y}}
|death_place = [[Boston, Massachusetts]]
|party = [[Democratic-Republican Party|Democratic-Republican]]<br>[[Whig Party (United States)|Whig]]
|spouse =
|profession =
|religion =
|order2 = 14th [[Lieutenant Governor of Massachusetts]]
|term_start2 = 1833
|term_end2 = 1836 <br><small>Acting Governor <br> March 1, 1835 – March 13, 1836</small>
|governor2 = [[Levi Lincoln, Jr.]]<br />[[John Davis (Massachusetts Governor)|John Davis]] 
|predecessor2 = [[Thomas L. Winthrop]]
|successor2 = [[George Hull (Massachusetts)|George Hull]]
|order3=6th [[Mayor of Boston, Massachusetts]]
|term3=1836
|predecessor3=[[Theodore Lyman (militiaman)|Theodore Lyman]]
|successor3=[[Samuel Atkins Eliot (politician)|Samuel A. Eliot]]
|office4=Member of the [[Massachusetts Senate]]
|term4=1839
|signature=SamuelTArmstrongSig.png
}}

'''Samuel Turell Armstrong''' (April 29, 1784 – March 26, 1850) was a [[United States of America|U.S.]] [[political figure]]. Born in 1784 in [[Dorchester, Massachusetts]], he was a printer and bookseller in [[Boston]], specializing in religious materials.  Among his works were an early [[stereotype (printing)|stereotype]] edition of [[Thomas Scott (commentator)|Scott's]] Family Bible, which was very popular, and ''[[The Panoplist]]'', a religious magazine devoted to missionary interests.

Armstrong began to withdraw from the printing business in 1825, and focused instead on politics.  He was active in Boston politics during the 1820s, twice winning a seat in the [[Massachusetts General Court]] (state legislature).  In 1833 he was elected [[Lieutenant Governor of Massachusetts]] as a [[Whig Party (United States)|Whig]], and served three consecutive annual terms.  For most of the last term he was Acting Governor after Governor [[John Davis (Massachusetts governor)|John Davis]] resigned to take a seat in the [[United States Senate]].  He lost a bid to be elected governor in his own right in 1836, but was elected [[Mayor of Boston]], a post he held for one year.

==Printer and bookseller==
Samuel Armstrong was born on April 29, 1784 in [[Dorchester, Massachusetts]] to John Armstrong and Elizabeth (Williams) Armstrong.  His father, a military man, died when he was ten, and his mother died three years later.  He was apprenticed to Manning and Loring, bookbinders and printers who were described as "the principal book-printers in the town" of [[Boston]].<ref name=NEHGS_137>"Samuel Turell Armstrong", p. 137</ref>  Following his apprenticeship he opened a print shop with a partner in Boston, but a few years later opened his own business in [[Charlestown, Massachusetts|Charlestown]].

In 1807 he was elected as a member of the [[Ancient and Honorable Artillery Company of Massachusetts]].  He was elected as the first sergeant of the Company in 1811.  He also served as the captain of the Warren Phalanx of Charlestown from 1811 to 1814.<ref>History of the Ancient and Honorable Artillery Company of Massachusetts.  Oliver Ayer Roberts. Boston. 1897. pg. 338.</ref>

In 1811 he returned the business to Boston, establishing a bookshop on [[Cornhill, Boston|Cornhill]].  His principal business was in the printing of religious tracts; his most notable work was in publishing ''[[The Panoplist]]'', a religious magazine devoted to missionary matters.<ref>"Samuel Turell Armstrong", pp. 137–138</ref>  Another major success was his printing of the first [[stereotype (printing)|stereotype]] edition of [[Thomas Scott (commentator)|Scott's]] Family Bible, a highly popular work that sold tens of thousands of copies.<ref>"Uriel Crocker", p. 320</ref><ref>Reno, p. 695</ref><ref>Herndon, p. 33</ref>  He also opened his bookshop for church-related activities, including fundraising for foreign missionary work.<ref>Hill, pp. 370, 406</ref>

After he moved to Boston Armstrong took on two apprentices, [[Uriel Crocker]] and [[Osmyn Brewster]].  In 1818, upon the end of their apprenticeship, Armstrong turned over operation of the printing business to them (which then became known as [[Crocker & Brewster]]) and focused his activities on the bookshop.  In 1825, he withdrew from the day-to-day operations of the business,<ref>Sprague, p. 150</ref> but would retain a financial stake until 1840. He continued to maintain a personal interest in the business until his death.<ref name=NEHGS138/>  The business was a significant financial success, and made Armstrong fairly wealthy.<ref name=NEHGS_137/>

==Church and politics==
Armstrong was a member of the [[Old South Meeting House|Old South Church]].  He served on its [[vestry]] (including as its secretary), and was chosen deacon in 1829.<ref>Hill, pp. 429, 489</ref>  When the church was formally incorporated in 1844 Armstrong was named as one of its proprietors.<ref>Hill, p. 504</ref>  In 1816 Armstrong discovered the original manuscript of the third volume of colonial Governor [[John Winthrop]]'s ''History of New England'' in the church's tower; the volume was presented to the [[Massachusetts Historical Society]].<ref>Drake, p. 7</ref>  He was also in part responsible for the church's loss of a perfect specimen of the 17th century [[Bay Psalm Book]], one of five that had been willed to the church by [[Thomas Prince]], an early minister of the congregation.  Writer Robert Wallace suggests that Armstrong's trade of two copies of the book in exchange for offers by the recipients, George Livermore and Edward Crowninshield, to rebind the other copies was naive, and was essentially a scam by Livermore and Crowninshield (both knowledgeable in the rare book trade) to acquire the valuable books for a bargain price.<ref>Eames, pp. ix–xiii</ref><ref>Wallace, pp. 95–106</ref>

[[Image:Armstrong_ScottBible.png|thumb|left|upright|Title page of an 1827 edition of Armstrong's printing of Scott's Bible]]
Armstrong was involved in civic affairs as early as 1812, when he served in a Boston militia company during the [[War of 1812]].  He entered state politics as a representative in [[Massachusetts General Court]] (state legislature), serving in that body from 1822–1823 and in 1828–1829.  From 1828 to 1830 he served on Boston's board of aldermen.<ref name=NEHGS138/>  In 1833 Armstrong was offered the nomination for [[Lieutenant governor of Massachusetts|lieutenant governor]] by the state [[Anti-Masonic Party]].  Since he refused to subscribe to their view that [[Freemasonry]] should be abolished, he rejected the offer.<ref>Niles, p. 55</ref>  He was, however, elected lieutenant governor on the [[Whig Party (United States)|Whig]] ticket, serving first under [[Levi Lincoln, Jr.]] and then [[John Davis (Massachusetts Governor)|John Davis]].  Whig newspapers used his working class roots to appeal to members of the [[Working Men's Party]], a third party founded on labor issues, describing him in the 1834 campaign as "a Mechanic and Workingman".<ref>Formisano, p. 148</ref>

When Davis resigned in March 1835 upon his election to the [[United States Senate]], Armstrong served as the [[Governor of Massachusetts|Acting Governor]] until 1836.<ref name=NEHGS138/>  In the 1836 campaign Armstrong sought the Whig nomination for governor, but it went instead to [[Edward Everett]] in a bid for support from the Anti-Masons.  Armstrong ended up running that year without party support,<ref>McCarthy, p. 525</ref> and came in a distant third behind Everett and perennial [[Democratic Party (United States)|Democratic]] candidate [[Marcus Morton]].<ref>Hart, p. 4:86</ref>

In contrast to his defeat at the state level, Armstrong was elected Mayor of Boston in 1836.<ref name=NEHGS138/>  The principal act of civic improvement during his one-year administration was the construction of iron fencing around the [[Boston Common]] and the widening of the promenade along [[Boylston Street]].  Although the contracts for the work had been drawn up by the preceding administration of [[Theodore Lyman (militiaman)|Theodore Lyman]], Armstrong oversaw the work, and also had the task of securing the relocation of remains in the [[Central Burying Ground (Boston, Massachusetts)|Central Burying Ground]] that were affected by the work.  This he accomplished, despite some resistance from several families, by offering at no cost several new granite tombs to the affected parties.<ref>Boston Transit Commission, p. 74</ref>

In 1839 Armstrong was elected to the [[Massachusetts Senate]], where he served a single term.  In 1845 he joined the [[New England Historical and Genealogical Society]], in whose affairs he remained involved until his death.  He died in 1850 in Boston,<ref name=NEHGS138/> and is buried in Cambridge's [[Mount Auburn Cemetery]].<ref>Linden, p. 262</ref>  He had married Abigail Walker of Charlestown in 1812; they had no children.<ref name=NEHGS138>"Samuel Turell Armstrong", p. 138</ref>

==See also==
* [[Timeline of Boston#1800s.E2.80.931840s|Timeline of Boston]], 1830s

==Notes==
{{reflist|2}}

==References==
*{{cite book|author=Boston Transit Commission|title=Annual Report of the Boston Transit Commission, 1895|publisher=City of Boston|year=1895|location=Boston|oclc=70937465|url=https://books.google.com/books?id=KkoAAAAAYAAJ&pg=PA74-IA17#v=onepage&f=false}}
*{{cite book|last=Drake|first=Samuel|title=A Review of Winthrop's Journal|url=https://books.google.com/books?id=cApprKsPpjsC&pg=PA7#v=onepage|location=Boston|publisher=Dutton and Wentworth|year=1854|oclc=2337648}}
*{{cite book|last=Eames|first=Wilberforce|title=The Bay Psalm Book|url=https://books.google.com/books?id=lHgqAAAAYAAJ&pg=PR9#v=onepage|publisher=Dodd, Mead|location=New York|year=1903|oclc=1203037}}
*{{cite book|last=Formisano|first=Ronald|title=For the People: American Populist Movements from the Revolution to The 1850s|year=2008|publisher=University of North Carolina Press|location=Chapel Hill, NC|isbn=9780807886113|oclc=233573309}}
*{{cite book|last=Hart|first=Albert Bushnell (ed)|title=Commonwealth History of Massachusetts|publisher=The States History Company|location=New York|year=1927|oclc=1543273}} (five volume history of Massachusetts until the early 20th century)
*{{cite book|last=Herndon|first=Richard|title=Men of Progress: One Thousand Biographical Sketches|url=https://books.google.com/books?id=5HFPAAAAYAAJ&pg=PA33#v=onepage|location=Boston|publisher=New England Magazine|year=1896|oclc=881008}}
*{{cite book|last=Hill|first=Hamilton|title=History of the Old South Church (Third Church) Boston: 1669–1884, Volume 2|url=https://books.google.com/books?id=EuSVOPIwcosC&pg=PA504#v=onepage|publisher=Houghton, Mifflin|location=Boston|year=1890|oclc=1182744}}
*{{cite book|last=Linden|first=Blanche|title=Silent City on a Hill: Picturesque Landscapes of Memory And Boston's Mount Auburn Cemetery|publisher=University of Massachusetts Press|year=2007|location=Amherst, MA|isbn=9781558495715|oclc=642981544}}
*{{cite journal|title=Samuel Turell Armstrong|journal=The New England Historical and Genealogical Register |volume=44|url=https://books.google.com/books?id=SkpCw3vLHwQC&pg=PA137#v=onepage&f=false|location=Boston|date=April 1890|oclc=12100816|issn=0028-4785|pages=137–140}}
*{{cite journal|title=Uriel Crocker|journal=The New England Historical and Genealogical Register |volume=42|url=https://books.google.com/books?id=jMC7zEgO9L4C&lpg=PA320&pg=PA320#v=onepage|location=Boston|date=July 1888|oclc=12100816|issn=0028-4785|page=320}}
*{{cite book|last=McCarthy|first=Charles|title=The Antimasonic Party: A Study of Political Antimasonry in the United States, 1827–1840|url=https://books.google.com/books?id=QapJAAAAMAAJ&pg=PA525#v=onepage|oclc=12321675|publisher=American Historical Association|location=Washington, DC|year=1903}}
*{{cite book|last=Niles|first=H|title=Niles' Weekly Register, Volume 45|url=https://books.google.com/books?id=Nr08AAAAIAAJ&pg=PA55#v=onepage|date=1834|location=Baltimore, MD|publisher=H. Niles|oclc=7329918|issn=2156-3616}}
*{{cite book|last=Reno|first=Conrad|title=Memoirs of the Judiciary and the Bar, Volume 3|url=https://books.google.com/books?id=9WswAQAAMAAJ&pg=PA695#v=onepage|publisher=Century Memorial Publishing|location=Boston|year=1901|oclc=426554681}}
*{{cite book|last=Sprague|first=Henry|title=An Old Boston Institution: A Brief History of the Massachusetts Charitable Fire Society|url=https://books.google.com/books?id=CR8pAAAAYAAJ&pg=PA151#v=onepage|location=Boston|year=1893|publisher=Little, Brown|oclc=4070625}}
*{{cite magazine|last=Wallace|first=Robert|title=A Very Proper Swindle|magazine=Life Magazine|date=November 22, 1954|url=https://books.google.com/books?id=S1IEAAAAMBAJ&lpg=PA100&pg=PA95#v=onepage|pages=95–106}}

==Further reading==
* {{cite journal|last=Silver|first=Rollo G|title=Belcher & Armstrong Set up Shop: 1805|journal=Studies in Bibliography |volume=4|date=1951–1952|pages=201–204}}

{{s-start}}
{{s-off}}
{{succession box
|before = [[Theodore Lyman (militiaman)|Theodore Lyman, Jr.]]
|title=[[Mayor of Boston, Massachusetts]]
|years= 1836
|after = [[Samuel Atkins Eliot (politician)|Samuel A. Eliot]]}}
{{succession box
|title=[[Lieutenant Governor of Massachusetts]]
|before= [[Thomas L. Winthrop]]
|after= [[George Hull (Massachusetts)|George Hull]]
|years=January 9, 1834 – January 13, 1836}}
{{s-bef
|before=[[John Davis (Massachusetts Governor)|John Davis]]
|as=Governor
}}
{{s-ttl
|title=[[Governor of Massachusetts|Acting Governor of Massachusetts]]
|years=March 1, 1835 – January 13, 1836}}
{{s-aft
|after=[[Edward Everett]]
|as=Governor
}}
{{s-end}}
{{Governors of Massachusetts}}
{{Lieutenant Governors of Massachusetts}}
{{BostonMayors}}

{{Authority control}}

{{good article}}

{{DEFAULTSORT:Armstrong, Samuel Turell}}
[[Category:Governors of Massachusetts]]
[[Category:Lieutenant Governors of Massachusetts]]
[[Category:Massachusetts State Senators]]
[[Category:Members of the Massachusetts House of Representatives]]
[[Category:Mayors of Boston]]
[[Category:1784 births]]
[[Category:1850 deaths]]
[[Category:Bookstores in Boston]]
[[Category:Massachusetts Democratic-Republicans]]
[[Category:Massachusetts Whigs]]
[[Category:American printers]]
[[Category:American book publishers (people)]]
[[Category:19th-century publishers]]
[[Category:Whig Party state governors of the United States]]
[[Category:19th-century American politicians]]