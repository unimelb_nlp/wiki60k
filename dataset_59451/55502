{{Infobox journal
| cover =
| discipline = [[International arbitration]], [[International law]]
| abbreviation = Am. Rev. Int. Arb.
| publisher = [[Juris Publishing]]
| country = United States
| frequency = Quarterly
| history = 1994
| openaccess =
| website = http://web.law.columbia.edu/students/student-services/learning/student-journals/american-review-international-arbitration
| ISSN = 1050-4109
}}
The '''''American Review of International Arbitration''''' is a quarterly [[academic journal]] covering [[international arbitration]]. It is run and edited by faculty members at [[Columbia University Law School]], as well as practitioners in international arbitration.<ref>{{cite web|url=http://arbitrationlaw.com/books/american-review-international-arbitration-aria-international-price |title=Arbitration Law |publisher=Juris |accessdate=2013-07-11}}</ref>

The journal publishes articles on [[international arbitration]]. It also publishes case summaries and reviews of new books in the field.<ref>{{cite web|url=http://web.law.columbia.edu/students/student-services/learning/student-journals/american-review-international-arbitration |title=American Review of International Arbitration |publisher=web.law.columbia.edu |date=2013-07-11 |accessdate=2013-07-11}}</ref>

{{As of|2012}}, the ''American Review of International Arbitration'' is the most cited international arbitration law journal in the world.,<ref>{{cite web|url=http://lawlib.wlu.edu/lj/index.aspx |title=Law Journals: Submissions and Ranking |publisher=Lawlib.wlu.edu |date=2013-07-11 |accessdate=2013-07-11}}</ref> and the fourth most cited for international trade.<ref>{{cite web|url=http://lawlib.wlu.edu/lj/index.aspx |title=Law Journals: Submissions and Ranking |publisher=Lawlib.wlu.edu |date=2013-07-11 |accessdate=2014-02-20}}</ref> The journal has been described by its peers as "a journal which all involved in the international arbitration process ought to give serious attention to."<ref>{{cite web|url=http://arbitrationlaw.com/books/american-review-international-arbitration-aria-international-price |title=Arbitration International, (1994 Volume 10 Issue 3 ) 367 |publisher=Arbitration International |accessdate=2013-07-11}}</ref>

The journal is edited by Professor [[George Bermann]],<ref>{{cite web|url=http://arbitrationlaw.com/books/american-review-international-arbitration-aria-international-price |title=Arbitration Law |publisher=Juris |accessdate=2013-07-11}}</ref> Professor Hans Smit and Professor Kabir Duggal at [[Columbia University Law School]].

== References ==

<references/>

== External links ==

* {{Official website|http://web.law.columbia.edu/students/student-services/learning/student-journals/american-review-international-arbitration}}

[[Category:International law journals]]
[[Category:Columbia Law School]]


{{US-poli-mag-stub}}