{{Italic title}}
{{Infobox Journal
| title = Gynecological Endocrinology
| cover =Gye front cover.jpg
| editor = Anrea R. Genazzani
| discipline = [[Gynaecology|Gynecology]], [[endocrinology]]
| abbreviation = Gynecol. Endocrinol.
| publisher = [[Informa]]
| country =
| history = 1987-present
| impact = 1.360
| impact-year = 2009
| website = http://www.informahealthcare.com/gye
| link1 = http://informahealthcare.com/toc/gye/current
| link1-name = Online access
| link2 = http://informahealthcare.com/loi/gye
| link2-name = Online archive
| RSS = http://informahealthcare.com/action/showFeed?ui=0&mi=3bd55n&ai=1lij&jc=gye&type=etoc&feed=rss
| ISSN = 0951-3590
| eISSN = 1473-0766
| CODEN = GYENER
| OCLC = 18175592
}}
'''''Gynecological Endocrinology''''' is a monthly [[Peer review|peer-reviewed]] [[medical journal]] covering experimental, clinical, and therapeutic aspects of the discipline. The journal includes papers related to the control and function of the different [[endocrine glands]] in women, the effects of reproductive events on the endocrine system, and the consequences of endocrine disorders on reproduction.<ref>{{cite web |url= http://informahealthcare.com/page/Description?journalCode=gye |title= Gynecological Endocrinology  Aims and Scope |accessdate=08/06/2010 |format= |work=}}</ref> It is the official journal of the [[International Society of Gynecological Endocrinology]].<ref>{{cite web |url=http://www.gynecologicalendocrinology.org/index.htm |title= International Society of Gynecological Endocrinology |accessdate= 08-06-2010 |format= |work=}}</ref>

''Gynecological Endocrinology'' is published by [[Informa]] and is edited by Anrea R. Genazzani ([[University of Pisa]], Italy).

The journal was established in 1987 and has a 2009 [[impact factor]] of 1.360, ranking it 44th out of 70 journals in the category "Obstetrics and Gynecology".<ref>''[[Journal Citation Reports]]'', 2010</ref>

== References ==
{{Reflist}}

[[Category:Obstetrics and gynaecology journals]]
[[Category:Endocrinology journals]]