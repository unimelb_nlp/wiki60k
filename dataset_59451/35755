{{good article}}
{{Infobox military conflict
| image      =
| caption    = An American 2.36-inch [[bazooka]] team takes aim at a North Korean tank during the Battle of Osan. On the right is [[Kenneth R. Shadrick]] who would later be reported as the first American killed in Korea.
| conflict   = Battle of Suwon Airfield
| partof     = [[Korean War]]
| date       = June 27, 1950
| place      = Over [[Seoul]] and [[Suwon]], [[South Korea]]
| result     = [[United Nations]] victory
| combatant1 = {{Flag|United States|1912}}
| combatant2 = {{Flag|North Korea}}
| commander1 = {{Flagicon|United States|1912}} [[James W. Little]]
| commander2 =
| strength1  = 5 [[F-82 Twin Mustang]]s<br />4 [[F-80C Shooting Star]]s
| strength2  = 5 [[Lavochkin La-7]]s <br />8 [[Ilyushin Il-10]]s
| units1     = {{Flagicon|United States|1912}} [[8th Fighter Wing]]
| units2     = {{Flagicon|North Korea}} [[1st Air Division (North Korea)|1st Air Division]]
| casualties1 = several aircraft damaged, one T-6 Texan destroyed
| casualties2 = 7 aircraft destroyed
| campaignbox = {{Campaignbox Korean War}}
}}
The '''Battle of Suwon Airfield''' was the first aerial battle of the [[Korean War]] occurring on June 27, 1950 over [[Kimpo Airfield]] and [[Suwon Air Base|Suwon Airfield]]. The battle, between aircraft of the [[United States]] and [[North Korea]], ended in a victory for the [[US Air Force]] after nine of its aircraft successfully shot down seven [[North Korean People's Air Force]] aircraft. It was the first direct engagement of the [[Air Battle of South Korea]].

With the outbreak of the war two days earlier, the US forces were attempting to evacuate US civilians and diplomats from the city of [[Seoul]], where a [[First Battle of Seoul|battle]] was ongoing between North and [[South Korea]]n forces. Transport aircraft and ships, escorted by US fighter planes, were attempting to bring civilians out of the country as fast as possible. During these missions on June 27, US forces were attacked by North Korean aircraft in two separate incidents in the Seoul area. Despite being outnumbered, the better-built American aircraft outmaneuvered the North Koreans, quickly shooting down half of the attacking force.

The actions were among few air-to-air battles in the early phase of the war as the North Koreans, realizing their aircraft were outmatched, quickly stopped using them aggressively against the UN. The battle also signaled a turning point in the [[Jet age]], as new, [[jet engine]] powered [[fighter aircraft]] had easily bested conventional fighters.

== Background ==
::''Main Article:  [[Korean War#Police Action: US intervention|Initial Phase of Korean War]]
On the morning of June 25, 1950, ten divisions of the [[North Korean People's Army]] launched a full-scale invasion of the nation's neighbor to the south, the [[Republic of Korea]]. The force of 89,000&nbsp;men moved in six columns, catching the [[Republic of Korea Army]] by surprise, resulting in a rout. The smaller South Korean army suffered from widespread lack of organization and equipment, and was unprepared for war.<ref>{{harvnb|Alexander|2003|p=1}}</ref> The numerically superior North Korean forces destroyed isolated resistance from the 38,000&nbsp;South Korean soldiers on the front before it began moving steadily south.<ref>{{harvnb|Alexander|2003|p=2}}</ref>

To prevent South Korea's collapse the [[United Nations Security Council]] voted to send military forces. The United States' [[United States Seventh Fleet|Seventh Fleet]] dispatched [[Task Force 77 (U.S. Navy)|Task Force 77]], led by the [[fleet carrier]] [[USS Valley Forge (CV-45)|USS ''Valley Forge'']]; the [[United Kingdom|British]] [[Far East Fleet]] dispatched several ships, including [[HMS Triumph (R16)|HMS ''Triumph'']], to provide air and naval support.<ref>{{harvnb|Malkasian|2001|p=23}}</ref> By June 27, the naval and air forces moving to Korea had authorization to attack North Korean targets with the goal of helping repel the North Korean invasion of the country.<ref>{{harvnb|Appleman|1998|p=38}}</ref> 

With the US forces accepting the North Korean attack as an act of war, it became imperative to evacuate civilians and American diplomats from Korea, as the forces of the north and south were battling across the peninsula. On June 27 the South Koreans were losing the [[First Battle of Seoul]].<ref name="Apple39">{{harvnb|Appleman|1998|p=39}}</ref> Most of South Korea's forces retreated in the face of the invasion. The North Koreans would capture the city the next day forcing the South Korean government and its shattered army to retreat further south.<ref>{{harvnb|Varhola|2000|p=2}}</ref>

In the meantime, US naval and air forces were evacuating US diplomats, military dependents, and civilians by ship and air transport, hoping to get American civilians out of the country "by any means." Civilians were being gathered at [[Suwon Air Base|Suwon Airfield]] and [[Kimpo Airfield]] in Seoul, before moving to [[Inchon]] and out of the country. These airlifts and convoys were being escorted by aircraft from the [[Far East Air Force (United States)|United States]], which was operating its aircraft from bases in Japan.<ref name="Apple39"/> The two Koreas had very small air forces of their own, with the North Koreans' 132 aircraft organized into the [[1st Air Division (North Korea)|1st Air Division]]. At the early phase in the war, these aircraft were used boldly to the North Koreans' advantage.<ref>{{harvnb|Futrell|1997|p=19}}</ref>

== Battle ==
=== First sortie ===
On the morning of June 27, a flight of five [[F-82 Twin Mustang]]s of the [[68th Fighter Squadron]] and [[339th Fighter Squadron]], [[8th Fighter Wing]] commanded by [[US Major]] [[James W. Little]] were escorting four [[C-54 Skymaster]] aircraft out of [[Gimpo International Airport|Kimpo]].<ref name="Fut12">{{harvnb|Futrell|1997|p=12}}</ref> Little, a [[flying ace]] with experience in [[World War II]], was the commander of the 339th Squadron.<ref name="DL7">{{harvnb|Dorr|Lake|1999|p=7}}</ref> The four transports were unarmed and filled with civilians from the Seoul area, en route to Japan.<ref name="MT">{{citation|url=http://militarytimes.com/citations-medals-awards/recipient.php?recipientid=25221 |title=Valor Awards for James Walter Little |publisher=[[Gannett Company]] |year=2011 |accessdate=2011-08-23}}</ref>

Around 12:00, a flight of five [[Korean People's Air Force]] (KPAF) [[Lavochkin La-7]] appeared at an altitude of {{convert|10,000|ft}}. The North Korean aircraft were headed for Kimpo Airfield with the intention of attacking US transports. Spotting the transport aircraft, the five North Korean planes immediately descended on them and began opening fire, scoring several hits.<ref name="MT">{{citation|url=http://militarytimes.com/citations-medals-awards/recipient.php?recipientid=25221 |title=Valor Awards for James Walter Little |publisher=[[Gannett Company]] |year=2011 |accessdate=2011-08-23}}</ref> The North Koreans then began opening fire on the five US fighters guarding the transport aircraft. Little ordered the US aircraft to return fire, and personally fired the first shot against the North Koreans.<ref name="Fut12"/>

The North Korean aircraft split off into two groups, with two climbing rapidly into the clouds and the remaining three descending. Two F-82s piloted by Lieutenant Charles B. Moran ([[United States military aircraft serials|tail number]] 46-357) and Lieutenant William G. Hudson (tail number 46-383) followed the ascending pair. The two North Korean aircraft maneuvered around Moran's aircraft and opened fire, damaging the tail of his plane. Hudson responded by attacking the lead plane, forcing it to ascend further. Hudson's shots struck the aircraft in the [[fuselage]] and right wing.<ref name="DL8">{{harvnb|Dorr|Lake|1999|p=8}}</ref>

The North Korean pilot subsequently bailed out of the aircraft, though the navigator remained in it and was killed when the aircraft crashed. Moran, in the menatime, had stalled while attempting to avoid the second North Korean plane, and when his aircraft recovered he was able to quickly shoot down the second North Korean aircraft which had accelerated in front of his.<ref name="DL8">{{harvnb|Dorr|Lake|1999|p=8}}</ref> Little's aircraft then dived to engage the three remaining North Korean aircraft, quickly shooting one down while attempting to assist Moran.<ref name="DL8"/>

The US planes, which were faster and more maneuverable than the North Korean aircraft, easily outperformed them. Within several minutes, three victories were claimed; one each by Little, Hudson, and Moran. The remaining two North Korean planes immediately fled.<ref name="Fut12"/> The US aircraft, in the meantime, suffered damage from the dogfight though none was shot down. Fire broke out in Little's [[cockpit]] but in spite of this he rallied the other aircraft and continued to escort them to their destination in Japan.<ref name="MT"/> 

Conflicting reports initially made it impossible to determine who had made the first kill, which would be the first North Korean plane shot down by the United Nations forces in the war. Later research indicated Hudson had successfully shot down the first aircraft.<ref name="Fut13">{{harvnb|Futrell|1997|p=13}}</ref> The exact timing of Hudson and Moran's victories remains unclear, and it is possible each scored a victory within seconds of the other.<ref name="DL8"/> For his determination in leading the flight, though, Little was awarded a [[Silver Star Medal]].<ref name="MT"/>

=== Second sortie ===
[[File:F-80s-36fbs-korea-1950.jpg|thumb|250px|[[Lockheed P-80 Shooting Star|F-80Cs]] of the [[8th Fighter-Bomber Group]] in Korea during the summer of 1950.]]
Word of the dogfight spread throughout the area, and an air alert was quickly posted over Seoul. In response, four [[F-80C Shooting Star]] aircraft of the [[35th Fighter Squadron|35th Fighter-Bomber Squadron]] under [[US Captain]] Raymond E. Schillereff <ref>Schillereff was later on the cover of Life Magazine July 17, 1950. He was later killed in a aircraft accident in the United States   August 8, 1951. </ref>were posted in the skies over Seoul to counter any North Korean aircraft appearing in the area. The F-80s were [[jet engine]]-powered [[fighter aircraft]] which would easily defeat all of the aircraft known in the North Koreans' air force.<ref name="Fut13"/>

Early in the afternoon, the North Korean aircraft returned in larger numbers to attack the air transports. A flight of eight [[Ilyushin Il-10]]s appeared in the airspace between Seoul and Inchon, attempting to ambush transport aircraft while still on the ground at the airfields. The F-80s spotted the North Korean planes and engaged them.<ref name="Fut13"/> The North Korean aircraft split into two groups of four and quickly destroyed a [[Republic of Korea Air Force]] [[T-6 Texan]] parked on the tarmac at Kimpo.<ref name="DL9">{{harvnb|Dorr|Lake|1999|p=9}}</ref><ref name="Apple44">{{harvnb|Appleman|1998|p=44}}</ref>

==== First US jet-aircraft victory ====
The four F-80 aircraft were able to attack the North Koreans from a greater distance. With a minimal amount of maneuvering, the US aircraft rushed the North Korean formation and quickly shot four of them down; two by Lieutenant Robert E. Wayne, one by Lieutenant Robert H. Dewald, and one by Schillereff. These victories were the first for US Air Force jet-powered fighters in history.<ref name="Fut13"/>

The four remaining North Korean pilots immediately retreated to the north. The aircraft likely staged at [[Pyongyang Air Base]], and informed their superiors. No additional North Korean aircraft were spotted in the area for the rest of the day.<ref name="Fut13"/> By the end of the day the US aircraft from the three squadrons had flown 163 sorties.<ref name="Apple39"/>

== Aftermath ==
{{Main|Air Battle of South Korea}}
The battle was the first air-to-air dogfight between the United Nations and North Korea in the war. The North Korean were unsuccessful in attempting to shoot down the air transports and in the end none of the refugees in the evacuation was ever injured by the North Koreans.<ref name="Fut12"/> In all, the operation saw 2,001 people, including 1,527 US nationals, evacuated from the peninsula ahead of the wider war.<ref name="Apple39"/> 

The battle was also considered a sign of the arrival of the [[Jet age]], the Il-10, which had been considered a high quality and effective conventional aircraft in [[World War II]] had been easily outmatched by the F-80 and its jet engine. The engagement was a rare example of an air-to-air battle at the early phase of the war, and North Korean forces became much more cautious when deploying their aircraft in battle, knowing they were easily outnumbered and outmatched by UN forces.<ref name="Fut13"/> For the next several months, the UN forces enjoyed [[air supremacy]], operating their air forces in support of ground combat virtually unopposed.<ref name="Apple257">{{harvnb|Appleman|1998|p=257}}</ref> The public perception of the F-80s, which had to this point been cautious, improved with news of the victories.<ref name="DL10">{{harvnb|Dorr|Lake|1999|p=10}}</ref>

== References ==
=== Citations ===
{{Reflist|2}}

=== Sources ===
{{Portal|United States Air Force}}
{{Refbegin}}
*{{citation|first=Bevin |last=Alexander |authorlink=Bevin Alexander |title=Korea: The First War We Lost |publisher=[[Hippocrene Books]] |year=2003|location=New York |isbn=978-0-7818-1019-7 }}
*{{citation|last=Appleman |first=Roy E. |title=South to the Naktong, North to the Yalu: United States Army in the Korean War |year=1998 |publisher=[[Department of the Army]] |isbn=978-0-16-001918-0 |url=http://www.history.army.mil/books/korea/20-2-1/toc.htm |location=[[Washington, D.C.]]}}
*{{citation|first=Robert F. |last=Dorr |authorlink=Robert F. Dorr |first2=John |last2=Lake |title=Korean War Aces |publisher=[[Osprey Publishing]] |year=1999 |isbn=978-1-85532-501-2|location=[[Oxford, England|Oxford]], United Kingdom}}
*{{citation|first=Robert F.|last=Futrell |title=The United States Air Force in Korea, 1950–1953|publisher=[[United States Government Printing Office]] |year=1997 |location=[[Washington, D.C.]] |isbn=978-0-16-048879-5}}
*{{citation|first=Carter |last=Malkasian |title=The Korean War |year=2001 |publisher=[[Osprey Publishing]]|location=Oxford, United Kingdom |isbn=978-1-84176-282-1}}
*{{citation|first=Michael J. |last=Varhola |authorlink=Michael J. Varhola |title=Fire and Ice: The Korean War, 1950–1953 |publisher=[[Da Capo Press]] |year=2000 |isbn=978-1-882810-44-4|location=[[Mason City, Iowa]]}}
{{Refend}}

{{coord missing|North Korea}}

{{DEFAULTSORT:Action of 27 June 1950}}
[[Category:Conflicts in 1950]]
[[Category:1950 in Korea]]
[[Category:Battles of the Korean War|Suwon Airfield]]
[[Category:Battles involving North Korea|Suwon Airfield]]
[[Category:Battles of the Korean War involving the United States|Suwon Airfield]]