{{italictitle}}
{{Infobox Journal
| cover	        =
| discipline	= [[Economics]], [[Statistics]]
| abbreviation = MLR
| editor        = 
| publisher	= [[Bureau of Labor Statistics]]  
| country	= [[United States|USA]]
| frequency	= Monthly
| history	= 1915–present
| openaccess	= yes
| website	= http://www.bls.gov/mlr
| link1         = http://www.bls.gov/mlr/#
| link1-name    = Online archive
| ISSN	        = 0098-1818
| JSTOR         = 00981818
}}

The '''Monthly Labor Review''' is published by the U.S. [[Bureau of Labor Statistics]]. Issues often focus on a particular topic. Most articles are by BLS staff.

Annually since 1969, the Lawrence R. Klein Award has been awarded to authors of articles appearing in the ''Monthly Labor Review'', generally one to BLS authors and one to non-BLS authors.<ref>[http://www.bls.gov/opub/mlr/2015/announcement/klein-award-recipients-announced-for-2014.htm Lawrence R. Klein Award recipients announced for 2014] on BLS web site</ref>

== History ==
In 1915, under commissioner [[Royal Meeker]], BLS began publishing the ''Monthly Review'', with a circulation of 8,000.  The name became ''Monthly Labor Review'' in 1918, and circulation rose to 20,000 in June 1920.<ref>Joseph Goldberg and William Moye. 1985. ''[http://www.bls.gov/opub/blsfirsthundredyears/ First hundred years of the Bureau of Labor Statistics]''. BLS Bulletin 2235.  p 110.  ISBN 0-935043-01-2</ref>
The journal has published its articles on the web for a decade. In 2008 the journal ceased to publish a bound paper edition, and now publishes only online.

== References ==
<references/>

== External links ==
* The [http://www.bls.gov/opub/mlr/mlrhome.htm Monthly Labor Review] web site

[[Category:Open access journals]]
[[Category:Monthly journals]]
[[Category:Bureau of Labor Statistics]]


{{econ-journal-stub}}