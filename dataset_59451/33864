{{For|the snail|Telescopium telescopium}}
{{Featured article}}
{{Use dmy dates|date=December 2014}}
{{infobox constellation
| name = Telescopium
| abbreviation = Tel
| genitive = Telescopii
| pronounce = {{IPAc-en|ˌ|t|ɛ|l|ᵻ|ˈ|s|k|oʊ|p|i|əm}},<br>genitive {{IPAc-en|ˌ|t|ɛ|l|ᵻ|ˈ|s|k|oʊ|p|i|.|aɪ}}
| symbolism = the [[Telescope]]
| RA = 19
| dec= −50
| family = [[La Caille Family|La Caille]]
| quadrant = SQ4
| areatotal = 252
| arearank = 57th
| numbermainstars = 2
| numberbfstars = 13
| numberstarsplanets = 0
| numberbrightstars = 0
| numbernearbystars = 2
| brighteststarname = [[Alpha Telescopii|α Tel]]
| starmagnitude = 3.49
| neareststarname = [[Gliese 754]]
| stardistancely = 19.35
| stardistancepc = 5.93
| numbermessierobjects = none
| meteorshowers =
| bordering = 
* [[Ara (constellation)|Ara]]
* [[Corona Australis]]
* [[Indus (constellation)|Indus]]
* [[Microscopium]] (corner)
* [[Pavo (constellation)|Pavo]]
* [[Sagittarius (constellation)|Sagittarius]]
| latmax = [[40th parallel north|40]]
| latmin = [[South Pole|90]]
| month = August
| notes=
}}

'''Telescopium''' is a minor [[constellation]] in the southern [[Celestial sphere|celestial hemisphere]], one of twelve named in the 18th century by French [[astronomer]] [[Nicolas-Louis de Lacaille]] and one of several depicting scientific instruments. Its name is a [[Latin]]ized form of the [[Ancient Greek|Greek]] word for [[telescope]]. Telescopium was later much reduced in size by [[Francis Baily]] and [[Benjamin Apthorp Gould|Benjamin Gould]].

The brightest star in the constellation is [[Alpha Telescopii]], a blue-white [[subgiant]] with an [[apparent magnitude]] of 3.5, followed by the orange giant star [[Zeta Telescopii]] at magnitude 4.1. [[Eta Telescopii|Eta]] and [[PZ Telescopii]] are two young star systems with [[debris disk]]s and [[brown dwarf]] companions. Telescopium hosts two unusual stars with very little hydrogen that are likely to be the result of two merged [[white dwarf]]s: [[PV Telescopii]], also known as HD 168476, is a hot blue [[extreme helium star]], while [[RS Telescopii]] is an [[R Coronae Borealis variable]]. [[RR Telescopii]] is a [[cataclysmic variable]] that brightened as a [[nova]] to magnitude 6 in 1948.

==History==
[[File:Sidney Hall - Urania's Mirror - Sagittarius and Corona Australis, Microscopium, and Telescopium.jpg|left|thumb|250px|Seen in the 1824 star chart set ''[[Urania's Mirror]]'' (in the lower right)]]
Telescopium was introduced in 1751–52 by [[Nicolas-Louis de Lacaille]] with the French name ''le Telescope'',{{sfn|Ridpath, ''Star Tales'' Lacaille}} depicting an [[aerial telescope]],{{sfn|Wagman|2003|p=299}} after he had observed and catalogued 10,000 southern stars during a two-year stay at the [[Cape of Good Hope]]. He devised 14 new constellations in uncharted regions of the [[Southern Celestial Hemisphere]] not visible from Europe. All but one honored instruments that symbolised the [[Age of Enlightenment]].{{sfn|Wagman|2003|pp=6–7}} Covering 40 degrees of the night sky,{{sfn|Wagman|2003|p=299}} the telescope stretched out northwards between Sagittarius and Scorpius.{{sfn|Ridpath, ''Star Tales'' Telescopium}} Lacaille had Latinised its name to ''Telescopium'' by 1763.{{sfn|Ridpath, ''Star Tales'' Lacaille}}

The constellation was known by other names. It was called ''Tubus Astronomicus'' in the eighteenth century, during which time three constellations depicting telescopes were recognised—[[Telescopium Herschelii|Tubus Herschelii Major]] between Gemini and Auriga and [[Tubus Herschelii Minor]] between Taurus and Orion, both of which had fallen out of use by the nineteenth century.{{sfn|Ellis|1997}} [[Johann Elert Bode|Johann Bode]] called it the ''Astronomische Fernrohr'' in his 1805 ''Gestirne'' and kept its size, but later astronomers [[Francis Baily]] and [[Benjamin Apthorp Gould|Benjamin Gould]] subsequently shrank its boundaries.{{sfn|Allen|1963|p=414}} The much-reduced constellation lost several brighter stars to neighbouring constellations: Beta Telescopii became [[Eta Sagittarii]], which it had been before Lacaille placed it in Telescopium,{{sfn|Wagman|2003|p=300}} Gamma was placed in Scorpius and renamed [[G Scorpii]] by Gould,{{sfn|Wagman|2003|p=300}} Theta Telescopii reverted to its old appellation of [[45 Ophiuchi|d Ophiuchi]],{{sfn|Wagman|2003|p=300}} and Sigma Telescopii was placed in Corona Australis. Initially uncatalogued, the latter is now known as [[HR 6875]].{{sfn|Wagman|2003|p=300}} The original object Lacaille had named Eta Telescopii—the open cluster [[Messier 7]]—was in what is now Scorpius, and Gould used the Bayer designation for a magnitude 5 star, which he felt warranted a letter.{{sfn|Wagman|2003|p=300}}

==Characteristics==
A small constellation, Telescopium is bordered by [[Sagittarius (constellation)|Sagittarius]] and [[Corona Australis]] to the north, [[Ara (constellation)|Ara]] to the west, [[Pavo (constellation)|Pavo]] to the south, and [[Indus (constellation)|Indus]] to the east, cornering on [[Microscopium]] to the northeast. The three-letter abbreviation for the constellation, as adopted by the [[International Astronomical Union]] in 1922, is 'Tel'.{{sfn|Russell|1922|p=469}} The official constellation boundaries, as set by [[Eugène Joseph Delporte|Eugène Delporte]] in 1930, are defined by a [[quadrilateral]]. In the [[equatorial coordinate system]], the [[right ascension]] coordinates of these borders lie between {{RA|18|09.1}} and {{RA|20|29.5}}, while the [[declination]] coordinates are between −45.09° and −56.98°.{{sfn|IAU, ''The Constellations'', Telescopium}} The whole constellation is visible to observers south of latitude [[33rd parallel north|33°N]].{{sfn|Ridpath, Constellations: Lacerta–Vulpecula}}{{efn|1=While parts of the constellation technically rise above the horizon to observers between 33°N and [[44th parallel north|44°N]], stars within a few degrees of the horizon are to all intents and purposes unobservable.{{sfn|Ridpath, Constellations: Lacerta–Vulpecula}}}}

==Features==

===Stars===
{{see also|List of stars in Telescopium}}
Within the constellation's borders, there are 57 stars brighter than or equal to [[apparent magnitude]] 6.5.{{efn|1=Objects of magnitude 6.5 are among the faintest visible to the unaided eye in suburban-rural transition night skies.{{sfn|The Bortle Dark-Sky Scale}}}}{{sfn|Ridpath, Constellations: Lacerta–Vulpecula}} With a magnitude of  3.5, [[Alpha Telescopii]] is the brightest star in the constellation. It is a blue-white [[subgiant]] of [[Stellar classification#spectral types|spectral type]] B3IV which lies around 250 [[light-year]]s away.{{sfn|Kaler, ''Alpha Telescopii''}} It is radiating nearly 800 times the [[solar luminosity|Sun's luminosity]], and is estimated to be 5.2±0.4 times as massive and have 3.3±0.5 times the Sun's radius.{{sfn|Hubrig|2009}} Close by Alpha Telescopii are the two blue-white stars sharing the designation of [[Delta Telescopii]]. Delta¹ Telescopii is of spectral type B6IV and apparent magnitude 4.9,{{sfn|SIMBAD HR 6934}} while Delta² Telescopii is of spectral type B3III and magnitude 5.1.{{sfn|SIMBAD HR 6938}} They form an [[Double star|optical double]],{{sfn|Ridpath|Tirion|2007|pp=242–43}} as the stars are estimated to be around 710 and 1190 light-years away respectively.{{sfn|van Leeuwen|2007}} The faint (magnitude 12.23) [[Gliese 754]], a [[red dwarf]] of spectral type M4.5V, is one of the nearest 100 stars to Earth at 19.3 light-years distant.{{sfn|SIMBAD LHS 60}} Its [[Orbital eccentricity|eccentric orbit]] around the Galaxy indicates that it may have originated in the Milky Way's [[thick disk]].{{sfn|Innanen 2010}}

At least four of the fifteen stars visible to the unaided eye are [[Giant star|orange giants]] of [[Stellar classification#Class K|spectral class K]].{{sfn|Bagnall|2012|pp=434–35}} The second brightest star in the constellation—at apparent magnitude 4.1—is [[Zeta Telescopii]], an orange subgiant of spectral type K1III-IV.{{sfn|Gray et al.|2006}} Around 1.53 times as massive as the Sun, it shines with 512 times its [[luminosity]].{{sfn|Liu et al. 2007}} Located 127 light years away from Earth, it has been described as yellow{{sfn|Ridpath|Tirion|2007|pp=242–43}} or reddish in appearance.{{sfn|Streicher|2009|pp=168–71}} [[Epsilon Telescopii]] is a [[binary star]] system:{{sfn|Ferreira|2009|pp=166–67}} the brighter component, Epsilon Telescopii A, is an orange [[giant star|giant]] of spectral type K0III with an [[apparent magnitude]] of +4.52,{{sfn|SIMBAD Epsilon Telescopii}} while the 13th magnitude companion, Epsilon Telescopii B, is 21 [[arcsecond]]s away from the primary, and just visible with a 15&nbsp;cm [[aperture]] telescope on a dark night.{{sfn|Ferreira|2009|pp=166–67}} The system is 417 light-years away.{{sfn|McDonald et al. 2012}} [[Iota Telescopii]] and [[HD 169405]]—magnitude 5 orange giants of spectral types K0III and K0.5III respectively{{sfn|SIMBAD Iota Telescopii}}{{sfn|SIMBAD HD 169405}}—make up the quartet.{{sfn|Bagnall|2012|pp=434–35}} They are around 370 and 497 light-years away from the Sun respectively.{{sfn|McDonald et al. 2012}} Another ageing star, [[Kappa Telescopii]] is a [[giant star#Yellow giants|yellow giant]] with a spectral type G9III and apparent magnitude of 5.18.{{sfn|SIMBAD Kappa Telescopii}} Around 1.87 billion years old, this star of around 1.6 solar masses has swollen to 11 times the Sun's diameter.{{sfn|da Silva et al. 2006}} It is approximately 293 light-years from Earth, and is another optical double.{{sfn|Ferreira|2009|pp=166–67}}

[[Xi Telescopii]] is an [[irregular variable]] star that ranges between magnitudes 4.89 and 4.94.{{sfn|AAVSO NSV 12783}} Located 1079 light-years distant, it is a [[red giant]] of spectral type M2III that has a diameter around 5.6 times the Sun's,{{sfn| Pasinetti Fracassini et al. 2001}} and a luminosity around 2973 times that of the Sun.{{sfn|McDonald et al. 2012}} Another irregular variable, [[RX Telescopii]] is a [[red supergiant]] that varies between magnitudes 6.45 and 7.47,{{sfn|AAVSO RX Telescopii}} just visible to the unaided eye under good viewing conditions. [[BL Telescopii]] is an [[Algol variable|Algol]]-like [[eclipsing binary]] system that varies between [[apparent magnitude]]s 7.09 and 9.08 over a period of just over 778 days (2 years 48 days).{{sfn|AAVSO BL Telescopii}} The primary is a [[yellow supergiant]] that is itself intrinsically variable.{{sfn|Zsoldos 1994}} Dipping from its baseline magnitude of 9.6 to 16.5,{{sfn|AAVSO RS Telescopii}} [[RS Telescopii]] is a rare [[R Coronae Borealis variable]]—an extremely hydrogen-deficient [[supergiant]] thought to have arisen as the result of the merger of two [[white dwarf]]s; fewer than 100 have been discovered as of 2012.{{sfn|Tisserand|2012}} The dimming is thought to be caused by carbon dust expelled by the star. As of 2012, four dimmings have been observed.{{sfn|Tisserand|2012}} [[PV Telescopii]] is a class B-type (blue) [[extreme helium star]] that is the prototype of a class of variables known as [[PV Telescopii variable]]s. First discovered in 1952, it was found to have a very low level of hydrogen. One theory of its origin is that it is the result of a merger between a helium- and a carbon-oxygen white dwarf. If the combined mass does not exceed the [[Chandrasekhar limit]], the former will accrete onto the latter star and ignite to form a supergiant. Later this will become an extreme helium star before cooling to become a white dwarf.{{sfn|Pandey 2006}}

While [[RR Telescopii]], also designated ''Nova Telescopii 1948'', is often called a [[Nova#Development|slow nova]], it is now classified as a [[symbiotic nova]] system composed of an M5III pulsating [[red giant]] and a white dwarf; between 1944 and 1948 it brightened by about 7 magnitudes before being noticed at apparent magnitude 6.0 in mid-1948.{{sfn|Robinson 1975}} It has since faded slowly to about apparent magnitude 12.{{sfn|Light Curve Generator}} [[QS Telescopii]] is a binary system composed of a white dwarf and [[main sequence]] donor star, in this case the two are close enough to be [[Tidal locking|tidally locked]], facing one another. Known as [[Polar (cataclysmic variable star)|polars]], material from the donor star does not form an [[accretion disk]] around the white dwarf, but rather streams directly onto it.{{sfn|Gerke|2006}} This is due to the presence of the white dwarf's strong [[magnetic field]].{{sfn|Traulsen|2011}}

[[File:NGC 6584.jpg|thumb|The globular cluster [[NGC 6584]], as observed with the [[Hubble Space Telescope]]]]
Although no star systems in Telescopium have confirmed planets, several have been found to have [[brown dwarf]] companions. A member of the 12-million-year-old [[Beta Pictoris moving group]] of stars that share a [[common proper motion]] through space,{{sfn|Smith 2009}} [[Eta Telescopii]] is a young [[A-type main-sequence star|white main sequence star]] of magnitude 5.0 and spectral type A0V.{{sfn|SIMBAD Eta Telescopii}} It has a [[debris disk]] and brown dwarf companion of spectral type M7V or M8V that is between 20 and 50 times as massive as Jupiter.{{sfn|Smith 2009}} The system is complex, as it has a common proper motion with (and is gravitationally bound to) the star [[HD 181327]], which has its own debris disk.{{sfn|Neuhäuser 2011}} This latter star is a [[F-type main-sequence star|yellow-white main sequence star]] of spectral type F6V of magnitude 7.0.{{sfn|SIMBAD HD 181327}} [[PZ Telescopii]] is another young star with a debris disk and substellar brown dwarf companion, though at 24 million years of age appears too old to be part of the Beta Pictoris moving group.{{sfn|Jenkins 2012}} [[HD 191760]] is a yellow subgiant—a star that is cooling and expanding off the [[main sequence]]—of spectral type G3IV/V. Estimated to be just over four billion years old, it is slightly (1.1 to 1.3 times) more massive as the Sun, 2.69 times as luminous, and has around 1.62 times its radius. Using the [[High Accuracy Radial Velocity Planet Searcher]] (HARPS) instrument on the [[ESO 3.6 m Telescope]], it was found to have a brown dwarf around 38 times as massive as Jupiter orbiting at an average distance of 1.35 AU with a period of 505 days. This is an unusually close distance from the star, within a range that has been termed the [[brown-dwarf desert]].{{sfn|Jenkins 2009}}

===Deep sky objects===
[[File:NGC 6845GALEX.jpg|thumb|The interacting galaxy system [[NGC 6845]], as observed with [[GALEX]]]]
The [[globular cluster]] [[NGC 6584]] lies near [[Theta Arae]] and is 45,000 light-years distant from Earth.{{sfn|Streicher|2009|pp=168–71}} It is an [[Oosterhoff type I]] cluster, and contains at least 69 variable stars, most of which are [[RR Lyrae variable]]s.{{sfn|Toddy et al.|2012|pp=63–71}} The [[planetary nebula]] [[IC 4699]] is of 13th magnitude and lies midway between Alpha and Epsilon Telescopii.{{sfn|Streicher|2009|pp=168–71}}

[[IC 4889]] is an [[elliptical galaxy]] of apparent magnitude 11.3, which can be found 2 degrees north-north-west of 5.3-magnitude [[Nu Telescopii]]. Observing it through a 40&nbsp;cm telescope will reveal its central region and halo.{{sfn|Bakich|2010|p=277}} The [[Telescopium group]] is group of twelve galaxies spanning three degrees in the northeastern part of the constellation, lying around 37 [[megaparsec]]s (120 million light-years) from our own galaxy.{{sfn|Streicher|2009|pp=168–71}} The brightest member is the elliptical galaxy [[NGC 6868]],{{sfn|Machacek|O'Sullivan|Randall|Jones|2010|pp=1316–32}} and to the west lies the [[spiral galaxy]]  (or, perhaps, [[lenticular galaxy]]) [[NGC 6861]].{{sfn|Streicher|2009|pp=168–71}} These are the brightest members of two respective subgroups within the galaxy group, and are heading toward a merger in the future.{{sfn|Machacek|O'Sullivan|Randall|Jones|2010|pp=1316–32}} Occupying an area of around 4[[arcminute|']] × 2', [[NGC 6845]] is an interacting system of four galaxies—two spiral and two [[lenticular galaxy|lenticular galaxies]]—that is estimated to be around 88 megaparsecs (287 million light-years) distant.{{sfn|Gordon|2003}} SN 2008da was a [[type II supernova]] observed in one of the spiral galaxies, [[NGC 6845A]], in June 2008.{{sfn|Morrell|2008}} [[SN 1998bw]] was a luminous supernova observed in the spiral arm of the galaxy [[ESO184-G82]] in April 1998, and is notable in that it is highly likely to be the source of the [[gamma-ray burst]] [[GRB 980425]].{{efn|1=chances of signals being unrelated is around 1 in 10,000.{{sfn|galama|1998}}}} {{sfn|galama|1998}}

==See also==
{{Commons category}}
* [[Telescopium Herschelii]]

==Notes==
{{Notelist}}

==References==
'''Citations'''
{{Reflist|30em}}
'''Sources'''
{{refbegin|30em}}
* {{cite book
  | last = Allen
  | first = Richard Hinckley
  | authorlink = Richard Hinckley Allen
  | year = 1963
  | origyear = 1899
  | title = Star Names: Their Lore and Meaning
  | edition = reprint
  | publisher = Dover Publications
  | location = New York City
  | isbn = 978-0-486-21079-7
  | ref = harv
  }}
* {{cite book
  | last = Bagnall
  | first = Philip M.
  | title = The Star Atlas Companion: What You Need to Know about the Constellations
  | publisher = Springer
  | location = New York
  | year = 2012
  | isbn = 978-1-4614-0830-7
  | ref = harv
  }}
* {{cite book
  | last = Bakich
  | first = Michael E.
  | year = 2010
  | title = 1,001 Celestial Wonders to See Before You Die
  | publisher = Springer Science + Business Media
  | isbn = 978-1-4419-1777-5
  | ref = harv
  }}
* {{cite journal | last1=da Silva | first1=L. | last2=Girardi | first2=L. | last3=Pasquini | first3=L. | last4=Setiawan | first4=J. | last5=von der Lühe | first5=O. | last6=de Medeiros | first6=J. R. | last7=Hatzes | first7=A. | last8=Döllinger | first8=M. P. | last9=Weiss | first9=A. | title=Basic Physical Parameters of a Selected Sample of Evolved Stars | journal=Astronomy & Astrophysics | volume=458 | issue=2 | pages=609–23 |date=2006 | doi=10.1051/0004-6361:20065105 | bibcode=2006A&A...458..609D |arxiv = astro-ph/0608160 |ref={{sfnRef|da Silva et al. 2006}}  }}
* {{cite journal
  | last = Ellis
  | first = Edward
  | year = 1997
  | title = Impressions of Cape skies – April 1995
  | journal = Journal of the British Astronomical Association
  | volume = 107
  | issue = 1
  | pages = 31–33
  | bibcode = 1997JBAA..107...31E
  | ref = harv
  }}
* {{cite journal
  | last = Ferreira
  | first = Lucas
  | volume = 68
  | issue = 4
  | pages = 166–67
  | date = 2009
  | title =  Deepsky Delights: Double Stars in Telescopium
  | journal= Monthly Notes of the Astronomical Society of Southern Africa
  | issn = 0024-8266
  | ref = harv
  |bibcode = 2009MNSSA..68..166F }}
* {{cite journal
  |author1=Galama, T. J. |author2=Vreeswijk, P.M. |author3=van Paradijs, J. |author4=Kouveliotou, C. |author5=Augusteijn, T. |author6=Böhnhardt, H. |author7=Brewer, J.P. |author8=Doublier, V. |author9=Gonzalez, J.-F. |author10=Leibundgut, B. |author11=Lidman, C. |author12=Hainaut, O.R. |author13=Patat, F. |author14=Heise, J. |author15=in't Zand, J. |author16=Hurley, K. |author17=Groot, P.J. |author18=Strom, R.G. |author19=Mazzali, P.A. |author20=Iwamoto, K. |author21=Nomoto, K. |author22=Umeda, H. |author23=Nakamura, T. |author24=Young, T. R. |author25=Suzuki, T. |author26=Shigeyama, T. |author27=Koshut, T. |author28=Kippen, M. |author29=Robinson, C. |author30=de Wildt, P. |author31=Wijers, R.A.M.J. |author32=Tanvir, N. |author33=Greiner, J. |author34=Pian, E. |author35=Palazzi, E. |author36=Frontera, F. |author37=Masetti, N. |author38=Nicastro, L. |author39=Feroci, M. |author40=Costa, E. |author41=Piro, L. |author42=Peterson, B.A. |author43=Tinney, C. |author44=Boyle, B. |author45=Cannon, R. |author46=Stathakis, R. |author47=Sadler, E. |author48=Begam, M. C. |author49=Ianna, P. | volume = 395
  | issue = 6703
  | pages = 670–72
  | date = 1998
  | title =  An Unusual Supernova in the Error Box of the γ-ray Burst of 25 April 1998
  | journal= Nature
  | doi= 10.1038/27150
  | bibcode=1998Natur.395..670G
  | ref = {{sfnRef|galama|1998}}
  |arxiv = astro-ph/9806175 }}
* {{cite journal |title= Polars Changing State: Multiwavelength Long‐Term Photometry and Spectroscopy of QS Telescopii, V834 Centauri, and BL Hydri |author1=Gerke, Jill R. |author2=Howell, Steve B. |author3=Walter, Frederick M. |journal=Publications of the Astronomical Society of the Pacific |volume=118 |issue= 843|year=2006|pages= 678–86 | doi=10.1086/503753 | ref={{sfnRef|Gerke|2006}} |arxiv = astro-ph/0603097 |bibcode = 2006PASP..118..678G }}
* {{cite journal|author1=Gordon, Scott |author2=Koribalski, Bärbel |author3=Jones, Keith |title= Australia Telescope Compact Array H I observations of the NGC 6845 galaxy group |journal= Monthly Notices of the Royal Astronomical Society |volume=342|issue=3|pages=939–50 |year=2003|bibcode=2003MNRAS.342..939G |ref={{sfnRef|Gordon|2003}} |arxiv = astro-ph/0303187 |doi = 10.1046/j.1365-8711.2003.06606.x }}
* {{cite journal
  | last1 = Gray
  | first1 = R. O.
  | last2 = Corbally
  | first2 = C. J.
  | last3 = Garrison
  | first3 = R. F.
  | last4 = McFadden
  | first4 = M. T.
  | last5 = Bubar
  | first5 = E. J.
  | last6 = McGahee
  | first6 = C. E.
  | last7 = O'Donoghue
  | first7 = A. A.
  | last8 = Knox
  | first8 = E. R.
  | title = Contributions to the Nearby Stars (NStars) Project: spectroscopy of stars earlier than M0 within 40 pc-The Southern Sample
  | journal = [[The Astronomical Journal]]
  | volume = 132
  | issue = 1
  | pages = 161–70
  | year = 2006
  | doi = 10.1086/504637
  | bibcode = 2006AJ....132..161G
  | arxiv = astro-ph/0603770
  | ref = {{sfnRef|Gray et al.|2006}}
  }}
*{{cite journal | last1=Hubrig | first1=S. | last2=Briquet | first2=M. | last3=De Cat | first3=P. | last4=Schöller | first4=M. | last5=Morel | first5=T. | last6=Ilyin | first6=I. | title=New magnetic field measurements of β Cephei stars and slowly pulsating B stars | journal=Astronomische Nachrichten | volume=330 | issue=4 | pages=317–329 |date=April 2009 | doi=10.1002/asna.200811187 | bibcode=2009AN....330..317H |arxiv = 0902.1314 | ref = {{harvid|Hubrig|2009}} }}
* {{cite journal |title = The Radial Velocity, Space Motion, and Galactic Orbit of GJ 754 |author1=Innanen, K. A. |author2=Flynn, C. | journal =Journal of the Royal Astronomical Society of Canada | volume= 104 |issue= 6 |pages=223–24 |year=2010 | bibcode= 2010JRASC.104..223I |ref= {{sfnRef|Innanen 2010}}  }}
* {{cite journal|author1=Jenkins, J.S. |author2=Pavlenko, Y. V. |author3=Ivanyuk, O. |author4=Gallardo, J. |author5=Jones, M. I. |author6=Day-Jones, A. C. |author7=Jones, H. R. A. |author8=Ruiz, M. T. |author9=Pinfield, D. J. |author10=Yakovina, L. |date=2012|title=Benchmark Cool Companions: Ages and Abundances for the PZ Telescopii System|journal=Monthly Notices of the Royal Astronomical Society|volume=420|issue=4|pages=3587–98 |bibcode=2012MNRAS.420.3587J |ref= {{sfnRef|Jenkins 2012}}
|arxiv = 1111.7001 |doi = 10.1111/j.1365-2966.2011.20280.x }}
* {{cite journal|author1=Jenkins, J. S. |author2=Jones, H.R.A. |author3=Goździewski, K. |author4=Migaszewski, C. |author5=Barnes, J. R. |author6=Jones, M. I. |author7=Rojo, P. |author8=Pinfield, D. J. |author9=Day-Jones, A.C. |author10=Hoyer, S. |title=First Results From the Calan–Hertfordshire Extrasolar Planet Search: Exoplanets and the Discovery of an Eccentric Brown Dwarf in the Desert  |  journal=Monthly Notices of the Royal Astronomical Society|volume=398 |issue=2 |pages= 911–17 | doi= 10.1111/j.1365-2966.2009.15097.x |ref= {{sfnRef|Jenkins 2009}}
|arxiv = 0905.2985 |bibcode = 2009MNRAS.398..911J |year=2009  }}
* {{cite journal|author1=Liu, Y. J. |author2=Zhao, G. |author3=Shi, J. R. |author4=Pietrzyński, G. |author5=Gieren, W. |date=2007|title=The abundances of nearby red clump giants|journal=Monthly Notices of the Royal Astronomical Society|volume=382|issue=2|pages=553–66|bibcode=2007MNRAS.382..553L |ref={{sfnRef|Liu et al. 2007}} |doi=10.1111/j.1365-2966.2007.11852.x}}
* {{Cite journal | last1 = Machacek | first1 = M. E. | last2 = O'Sullivan | first2 = E. | last3 = Randall | first3 = S. W. | last4 = Jones | first4 = C. | last5 = Forman | first5 = W. R. | title = The Mysterious Merger of NGC 6868 and NGC 6861 in the Telescopium Group | doi = 10.1088/0004-637X/711/2/1316 | journal = The Astrophysical Journal | volume = 711 | issue = 2 | pages = 1316–1332 | year = 2010 | pmid =  | pmc = | arxiv = 1001.2567| bibcode = 2010ApJ...711.1316M| ref = {{sfnRef|Machacek|O'Sullivan|Randall|Jones|2010}} }} <!-- {{sfn|Machacek|O'Sullivan|Randall|Jones|2010|p=xxx}} -->
* {{cite journal|author1=McDonald, I. |author2=Zijlstra, A. A. |author3=Boyer, M. L. |date=2012|title=Fundamental Parameters and Infrared Excesses of Hipparcos Stars|journal=Monthly Notices of the Royal Astronomical Society|volume=427|issue=1|pages=343–57|bibcode=2012MNRAS.427..343M|ref={{sfnRef|McDonald et al. 2012}} |doi=10.1111/j.1365-2966.2012.21873.x|arxiv = 1208.2037 }}
* {{cite journal | title =Supernova 2008da in NGC 6845A |author=Morrell, N. | year=2008 |journal = Central Bureau Electronic Telegrams |volume= 1412| page=1 |bibcode= 2008CBET.1412....1M | ref={{sfnRef|Morrell|2008}} }}
* {{cite journal | title = Further Deep Imaging of HR 7329 A (η Tel A) and its Brown Dwarf Companion B |author1=Neuhäuser, R. |author2=Ginski, C. |author3=Schmidt, T. O. B. |author4=Mugrauer, M. |journal =Monthly Notices of the Royal Astronomical Society| year=2011 | volume = 416 | issue = 2| pages=1430–35 | bibcode=2011MNRAS.416.1430N |ref={{sfnRef|Neuhäuser 2011}} |arxiv = 1106.1388 |doi = 10.1111/j.1365-2966.2011.19139.x }}
* {{cite journal  |author1=Pandey, Gajendra |author2=Lambert, David L. |author3=Jeffery, C. Simon |author4=Rao, N. Kameswara | title=An Analysis of Ultraviolet Spectra of Extreme Helium Stars and New Clues to Their Origins  | journal=The Astrophysical Journal| volume=638  | issue=1 | pages=454–71 |date= 2006 | doi=10.1086/498674 | bibcode=2006ApJ...638..454P|arxiv = astro-ph/0510161 |ref={{sfnRef|Pandey 2006}} }}
* {{cite journal|author1=Pasinetti Fracassini, L. E. |author2=Pastori, L. |author3=Covino, S. |author4=Pozzi, A. |date=2001|title=Catalogue of Apparent Diameters and Absolute Radii of Stars (CADARS) – Third edition – Comments and statistics|journal=Astronomy & Astrophysics|volume=367|issue=2|pages=521–24|bibcode=2001A&A...367..521P |ref={{sfnRef| Pasinetti Fracassini et al. 2001}} |doi=10.1051/0004-6361:20000451|arxiv = astro-ph/0012289 }}
* {{cite book
  | last1 = Ridpath
  | first1 = Ian
  | authorlink1 = Ian Ridpath
  | last2 = Tirion
  | first2 = Wil
  | authorlink2 = Wil Tirion
  | year = 2007
  | title = Stars and Planets Guide
  | publisher = Princeton University Press
  | location = Princeton, New Jersey
  | isbn = 978-0-691-13556-4
  | ref = harv
  }}
* {{cite journal | last=Robinson | first=E. L. | journal=[[The Astronomical Journal]] | volume=80 | number=7 | pages=515 | title=Preeruption Light Curves of Novae | bibcode=1975AJ.....80..515R |doi = 10.1086/111774 |ref={{sfnRef|Robinson 1975}} | year=1975 }}
* {{cite journal
  | last = Russell
  | first = Henry Norris
  |date=October 1922
  | title = The new international symbols for the constellations
  | journal = [[Popular Astronomy (US magazine)|Popular Astronomy]]
  | volume = 30
  | page = 469
  | bibcode = 1922PA.....30..469R
  | ref = harv
  }}
* {{cite journal | url=http://astro.berkeley.edu/~kalas/disksite/library/smith09a.pdf | title= Resolved Debris Disc Emission around Eta Telescopii: a Young Solar System or Ongoing Planet Formation?|author1=Smith, R. |author2=Churcher, L. J. |author3=Wyatt, M. C. |author4=Moerchen, M. M. |author5=Telesco, C. M. | journal = Astronomy & Astrophysics | year = 2009 | volume=493 |pages= 299–308 | doi= 10.1051/0004-6361:200810706 |bibcode=2009A&A...493..299S  | ref = {{sfnRef|Smith 2009}}
 |arxiv = 0810.5087 }}
* {{cite journal
  | last = Streicher
  | first = Magda
  | date = 2009
  | volume = 68
  | issue = 4
  | pages = 168–69
  | title = Deepsky Delights: A Spyglass Telescope
  | journal= Monthly Notes of the Astronomical Society of Southern Africa
  | issn = 0024-8266
  | ref = harv
  |bibcode = 2009MNSSA..68..168S }}
* {{cite journal |author1=Tisserand|author2=Clayton|author3=Welch|author4=Pilecki|author5=Wyrzykowski|author6=Kilkenny|title=The Ongoing Pursuit of R Coronae Borealis Stars: ASAS-3 Survey Strikes Again |year=2012 |bibcode=2013A&A...551A..77T |
journal =Astronomy & Astrophysics |volume = 551 | id = A77 |pages= 22 | ref= {{sfnRef|Tisserand|2012}} |arxiv = 1211.2475 |doi = 10.1051/0004-6361/201220713 }}
* {{cite journal
  | last1 = Toddy
  | first1 = Joseph M.
  | last2 = Johnson
  | first2 = Elliott W.
  | last3 = Darragh
  | first3 = Andrew N.
  | last4 = Murphy
  | first4 = Brian W.
  | year = 2012
  | title = New Variable Stars in the Globular Cluster NGC 6584
  | journal = Journal of the Southeastern Association for Research in Astronomy
  | volume = 6
  | issue =
  | pages =  63–71
  | arxiv = 1205.1034
  | ref = {{sfnRef|Toddy et al.|2012}}
  | bibcode = 2012JSARA...6...63T
  }}
* {{cite journal|last=Traulsen, I.; Reinsch, K.; Schwope, A. D.; Burwitz, V.; Dreizler, S.; Schwarz, R.; Walter, F. M.|date=2011|title=XMM-Newton observations of the X-ray soft polar QS Telescopii|journal=Astronomy and Astrophysics|volume=529|issue=A116|pages=7|doi= 10.1051/0004-6361/201016352 | bibcode=2011A&A...529A.116T | ref={{sfnRef|Traulsen|2011}} |first1=I.|last2=Reinsch|first2=K.|last3=Schwope|first3=A. D.|last4=Burwitz|first4=V.|last5=Dreizler|first5=S.|last6=Schwarz|first6=R.|last7=Walter|first7=F. M.|arxiv = 1103.4575 }}
* {{cite journal
 | first=F. | last=van Leeuwen
 | title=Validation of the new Hipparcos reduction
 | journal=Astronomy and Astrophysics
 | volume=474 | issue=2 | pages=653–64
 | date=November 2007 | bibcode=2007A&A...474..653V
 | doi=10.1051/0004-6361:20078357 | arxiv=0708.1752
 | ref = {{sfnRef|van Leeuwen|2007}}
 }}
* {{cite book
  | last = Wagman
  | first = Morton
  | year = 2003
  | title = Lost Stars: Lost, Missing and Troublesome Stars from the Catalogues of Johannes Bayer, Nicholas Louis de Lacaille, John Flamsteed, and Sundry Others
  | publisher = The McDonald & Woodward Publishing Company
  | location = Blacksburg, Virginia
  | isbn = 978-0-939923-78-6
  | ref = harv
  }}
* {{cite journal | title =The pulsations of yellow semi-regular variables II. The F supergiant in the high-latitude binary BL Telescopii |author =  Zsoldos, E. | journal =Astronomy and Astrophysics  |volume = 286 |pages= 870–74 |year=1994 |bibcode =1994A&A...286..870Z   | ref = {{sfnRef|Zsoldos 1994}}
 }}
{{refend}}

'''Online sources'''
{{refbegin|30em}}
* {{cite web | last=AAVSO | url=http://www.aavso.org/lcg | accessdate=5 September 2013 | title=AAVSO Light Curve Generator|ref={{sfnRef|Light Curve Generator}}}}
* {{cite web
  | title = BL Telescopii
  | author = Otero, Sebastian Alberto
  | work = AAVSO Website
  | publisher = American Association of Variable Star Observers
  | url = http://www.aavso.org/vsx/index.php?view=detail.top&oid=36332
  | date  =31 July 2006
  | accessdate = 14 July 2014
  | ref = {{sfnRef|AAVSO BL Telescopii}}
}}
* {{cite web
  | title = RS Telescopii
  | author = Otero, Sebastian Alberto
  | work = AAVSO Website
  | publisher = American Association of Variable Star Observers
  | url = http://www.aavso.org/vsx/index.php?view=detail.top&oid=36254
  | date  =13 February 2014
  | accessdate = 3 July 2014
  | ref = {{sfnRef|AAVSO RS Telescopii}}
  }}
* {{cite web
  | title = RX Telescopii
  | author = Otero, Sebastian Alberto
  | work = AAVSO Website
  | publisher = American Association of Variable Star Observers
  | url = http://www.aavso.org/vsx/index.php?view=detail.top&oid=36259
  | date  =11 November 2011
  | accessdate = 26 June 2014
  | ref = {{sfnRef|AAVSO RX Telescopii}}
  }}
* {{cite web
  | title = NSV 12783
  | author = Watson, Christopher
  | work = AAVSO Website
  | publisher = American Association of Variable Star Observers
  | url = http://www.aavso.org/vsx/index.php?view=detail.top&oid=36259
  | date  =3 May 2013
  | accessdate = 2 July 2014
  | ref = {{sfnRef|AAVSO NSV 12783}}
 }}
* {{cite web|url=http://www.skyandtelescope.com/resources/darksky/3304011.html?page=1&c=y|title=The Bortle Dark-Sky Scale|last=Bortle|first=John E.|date=February 2001|work=[[Sky & Telescope]]|publisher=Sky Publishing Corporation|accessdate=29 November 2014|ref = {{sfnRef|The Bortle Dark-Sky Scale}}}}
* {{cite web
  | title = Telescopium, constellation boundary
  | work = The Constellations
  | publisher = International Astronomical Union
  | url = http://www.iau.org/public/constellations/#tel
  | accessdate = 29 September 2012
  | ref = {{sfnRef|IAU, ''The Constellations'', Telescopium}}
  }}
* {{cite web
  | last = Kaler
  | first = Jim
  | title = Alpha Telescopii
  | work = Stars
  | publisher = University of Illinois
  | url = http://stars.astro.illinois.edu/sow/alphatel.html
  | accessdate = 29 September 2012
  | ref = {{sfnRef|Kaler, ''Alpha Telescopii''}}
  }}
* {{cite web
 | url=http://www.ianridpath.com/startales/lacaille.htm
 | title=Lacaille’s Southern Planisphere of 1756
 | work= Star Tales
 |author=[[Ian Ridpath]]
 |publisher=self-published
 | accessdate= 2 July 2014
 | ref = {{sfnRef|Ridpath, ''Star Tales'' Lacaille}}
}}
* {{cite web
 | url=http://www.ianridpath.com/constellations2.htm
 | title=Constellations: Lacerta–Vulpecula
 | work= Star Tales
 |author=[[Ian Ridpath]]
 |publisher=self-published
 | accessdate= 21 June 2014
 | ref = {{sfnRef|Ridpath, Constellations: Lacerta–Vulpecula}}
}}
* {{cite web
  | last = Ridpath
  | first = Ian
  | authorlink = Ian Ridpath
  | year = 1988
  | title = Telescopium
  | work = Star Tales
  | url = http://www.ianridpath.com/startales/telescopium.htm
  | accessdate = 29 September 2012
  | ref = {{sfnRef|Ridpath, ''Star Tales'' Telescopium}}
  }}
* {{cite web
  | title = Epsilon Telescopii – Star in Double System
  | work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?submit=display&bibdisplay=refsum&bibyear1=1850&bibyear2=%24currentYear&Ident=%403527004&Name=*+eps+Tel#lab_bib
  | accessdate = 30 September 2012
  | ref = {{sfnRef|SIMBAD Epsilon Telescopii}}
  }}
* {{cite web
  | title = Eta Telescopii
  | work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?submit=display&bibdisplay=refsum&bibyear1=1850&bibyear2=%24currentYear&Ident=%403527004&Name=*+eta+Tel#lab_bib
  | accessdate = 2 July 2014
  | ref = {{sfnRef|SIMBAD Eta Telescopii}}
  }}
* {{cite web
  | title = HD 169405
  | work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=HD+169405&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id
  | accessdate = 26 June 2014
  | ref = {{sfnRef|SIMBAD HD 169405}}
  }}
* {{cite web
  | title = HD 181327
  | work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=HD+181327&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id
  | accessdate = 2 July 2014
  | ref = {{sfnRef|SIMBAD HD 181327}}
  }}
* {{cite web
  | title = HR 6934
  | work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=HR+6934&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id
  | accessdate = 29 September 2012
  | ref = {{sfnRef|SIMBAD HR 6934}}
  }}
* {{cite web
  | title = HR 6938
  | work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=HR+6938&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id
  | accessdate = 29 September 2012
  | ref = {{sfnRef|SIMBAD HR 6938}}
  }}
* {{cite web
  | title = Iota Telescopii
  | work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?submit=display&bibdisplay=refsum&bibyear1=1850&bibyear2=%24currentYear&Ident=%403527004&Name=*+iota+Tel#lab_bib
  | accessdate = 26 June 2014
  | ref = {{sfnRef|SIMBAD Iota Telescopii}}
  }}
* {{cite web
  | title = Kappa Telescopii
  | work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  | url = http://simbad.u-strasbg.fr/simbad/sim-coo?Coord=18+52+39.64405-52+06+26.5372&CooFrame=ICRS&CooEqui=2000.0&CooEpoch=J2000&Radius.unit=arcmin&submit=query+around&Radius=2
  | accessdate = 30 September 2012
  | ref = {{sfnRef|SIMBAD Kappa Telescopii}}
  }}
* {{cite web
  | title = LHS 60 - High Proper Motion Star
  | work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=Gliese+754&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id
  | accessdate = 11 November 2014
  | ref = {{sfnRef|SIMBAD LHS 60}}
  }}
{{refend}}

{{Stars of Telescopium}}
{{navconstel}}
{{Sky|19|00|00|-|50|00|00|10}}

[[Category:Constellations]]
[[Category:Southern constellations]]
[[Category:Telescopium (constellation)| ]]
[[Category:Constellations listed by Lacaille]]