{{Use New Zealand English|date=October 2013}}
{{Use dmy dates|date=March 2017}}
{{Infobox Airline
| airline        = Air Safaris
| logo           = File:Air Safaris logo.jpg
| logo_size      = 200
| image          =  
| image_size     = 
| IATA           = –
| ICAO           = SRI<ref name="icao">Not an ICAO allocation – issued for domestic use by the Civil Aviation Authority of New Zealand</ref>
| callsign       = AIRSAFARI<ref>{{cite web|url=http://www.caa.govt.nz/fulltext/Documents/Callsigns.PDF |archiveurl=https://web.archive.org/web/20070808014205/http://www.caa.govt.nz/fulltext/Documents/Callsigns.PDF |archivedate=2007-08-08 |format=PDF |title=Operator Callsigns |website=Web.archive.org |accessdate=2017-03-11}}</ref>
| founded        = {{Start date|1970}}
| commenced      = 
| ceased         = 
| hubs           = Lake Tekapo
| fleet_size     = 8
| destinations   = 5 (charter)
| headquarters   = [[Lake Tekapo]], [[New Zealand]]
| key_people     = 
<div>
* Tim Rayward (Managing Director)
* Richard Rayward (CEO)<ref>[http://www.tourismexportcouncil.org.nz/member_listing/air-safaris/ ]{{dead link|date=March 2017}}</ref>
<div>
| website        = http://www.airsafaris.co.nz/
}}
'''Air Safaris''' is a New Zealand scenic flight and air charter company based at the [[Lake Tekapo Airport]] located 2.8&nbsp;km west of the town of [[Lake Tekapo (town)|Lake Tekapo]], off State Highway 8 in the [[Mackenzie District]] of [[New Zealand]].<ref>{{cite web|url=http://www.caa.govt.nz/Script/AvDocClientList.asp?Details_ID=1 |title=AvDocClientList &#124; Civil Aviation Authority of New Zealand websiteCaa.govt.nz |date= |accessdate=2017-03-11}}</ref><ref>[http://www.mtcooknz.com/mackenzie/product/?product=air-safaris-and-services-nz-ltd ]{{dead link|date=March 2017}}</ref> The airline operates from 3 bases the Lake Tekapo, Franz Josef and Glentanner/Mt Cook airports. The company logo is a stylised [[chamois]]; these are wild goat-like antelope which inhabits the region of the South Island High Country.

==History==
Air Safaris was established in 1970 at Mesopotamia station to take hunters and hikers into the mountain areas of the South Island. They moved their operations to Lake Tekapo in 1974.

The company has used a wide variety of aircraft: first the [[Cessna 180]], then later the larger Cessna 185, and in 1975 the Cessna 206 and 207. In 1978 the [[Pilatus Porter]]s arrived fitted with skis that could land and take off on glaciers and snowfields and as demand increased for scenic flights, the 15 seat [[GAF Nomad]] entered the fleet in 1981 for use at Lake Tekapo, Glentanner, Franz Josef and Milford Sound airports. On 17 December 1987 the airline purchased Air Timaru and continued to run its services from Timaru to Oamaru and Invercargill using a Piper PA31 Navajo aircraft.<ref>{{cite web|url=http://3rdlevelnz.blogspot.com.au/2012/08/air-timaru-friendly-line.html|title=3rd Level New Zealand: Air Timaru - "The Friendly Line"|first=Steve|last=L|date=5 August 2012|website=3rdlevelnz.blogspot.com.au|accessdate=11 March 2017}}</ref> Air Safaris were the first company in New Zealand to buy the [[Gippsland GA8|Gippsland Airvan]] in 2009.<ref name="pacificwingsmagazine.com">[http://pacificwingsmagazine.com/2009/04/01/air-safaris-southern-scenic-supremacy%E2%80%93part-2/] {{dead link|date=March 2017}}</ref>

From 1991 to 1996 one of the Nomads was used from [[Timaru Airport|Timaru]] to fly the [[Air Nelson]] scheduled service to [[Christchurch Airport|Christchurch]] as part of their [[Air New Zealand Link]] operation. The contract was for Air Safaris to provide two week day return services, Air Nelson also used them between [[Christchurch Airport|Christchurch]] and [[Hokitika Airport|Hokitika]] and [[Mount Cook Airlines]] for flights south of Christchurch as a back up capacity although all these services have since been discontinued.<ref>{{cite web|url=http://www.flightglobal.com/pdfarchive/view/1998/1998%20-%200252.html |title=1998 &#124; 0252 &#124; Flight Archive |website=Flightglobal.com |date=1998-02-03 |accessdate=2017-03-11}}</ref><ref>{{cite web|author= |url=http://3rdlevelnz.blogspot.com.au/2010/06/air-safaris-sole-schedule.html |title=3rd Level New Zealand: Air Safaris Sole Schedule |website=3rdlevelnz.blogspot.com.au |date=2010-06-18 |accessdate=2017-03-11}}</ref>

In 1996 Air Safaris won an award from the Civil Aviation Authority of New Zealand for safety. Director Kevin Ward said that the 1996 winning organisation, Air Safaris and Services (NZ), ''"...set standards, and fly to them.."''.<ref>{{cite web|url=http://www.flightglobal.com/pdfarchive/view/1998/1998%20-%200251.html |title=1998 &#124; 0251 &#124; Flight Archive |website=Flightglobal.com |date=1998-02-03 |accessdate=2017-03-11}}</ref><ref>{{cite news | title= The Right Way | work= [[Flight International]] | page= 33 | date= 28 January 1998}}</ref>

2002 Air Safaris along with the other members of ‘Tourism Flight Operators New Zealand’ were awarded the Director of Civil Aviation Organisation Award for their efforts towards increasing safety standards. The Director of Civil Aviation John Jones said the Tourism Flight Operators New Zealand was “pursuing standards that will qualify members for the Tourism New Zealand Qualmark brand.”<ref name="CAA website">[http://www.caa.govt.nz/safety_info/Directors_Awards.htm#2002 ]{{dead link|date=March 2017}}</ref>

The airline has recently completed a major development at [[Pukaki Airport|Pukaki]] airport near [[Twizel]] to enable it to offer scenic flights. It has involved construction of a sealed turning bay off the adjacent aircraft taxiway, and a parking area for company aircraft.<ref>{{cite web|url=http://www.stuff.co.nz/timaru-herald/news/8337133/Airline-set-to-take-wing-from-Twize |title=Airline set to take wing from Twizel |website=Stuff.co.nz |date=2013-02-22 |accessdate=2017-03-11}}</ref>

In 2013 Air Safaris added a Robinson R44 helicopter to their fleet and began operating scenic helicopter flights in addition to their fixed wing services.

==Accidents and incidents==
On 14 September 1998 an Air Safaris Cessna 177B Cardinal, ZK-DKL, crashed in the Mount Cook area while on a scenic flight, killing the pilot and 2 passengers on board. It struck a snow-covered mountain face 11&nbsp;km northeast of Mount Cook at 11.52 am. The aeroplane was damaged beyond repair. The cause of this accident has not been determined.<ref>{{cite web|url=http://aviation-safety.net/wikibase/wiki.php?id=41842 |title=ASN Aircraft accident 14-SEP-1998 Cessna 177B ZK-DKL |website=Aviation-safety.net |date= |accessdate=2017-03-11}}</ref><ref>{{cite web|url=http://planecrashinfo.com/1998/1998-48.htm |title=Accident Details |website=Planecrashinfo.com |accessdate=2017-03-11}}</ref><ref>{{cite web|url=http://aircrashed.com/accident/LAX98RA310.shtml |title=Aircrashed.com |website=Aircrashed.com |date= |accessdate=2017-03-11}}</ref>

==Fleet==
The [[GAF Nomad|GAF N24A Nomad]] is the largest aircraft in the fleet and is complemented by the smaller Cessna 208B and GA8 Airvan airliners.<ref>{{cite web|url=http://www.airsafaris.co.nz/fleet/ |title=New Zealand Tourist Flight Operators Air Safaris New Zealand |website=Airsafaris.co.nz |date= |accessdate=2017-03-11}}</ref> The 10 seater Pilatus PC-6 Turbo Porter was previously used from 1978 and the Cessna 185 and 206 were the first aircraft in Air Safaris fleet these have since been retired.<ref name="pacificwingsmagazine.com"/>

{| class="toccolours sortable" border="1" cellpadding="4" style="border-collapse:collapse"
|+ '''AirSafaris Fleet'''
|- style="background:lightblue;"
! Aircraft
! Number
! Passengers
! Registrations
|-
| [[Cessna 208B Grand Caravan]] 
| 1
| 13
| ZK-SRI
|-
| [[Gippsland GA8|GA8 Airvan]]
| 4
| 7
| ZK-SAE, SAF, SAU, SAZ
|-
| [[GAF Nomad|GAF N24A Nomad]]
| 3
| 15
| ZK-NMD, NME
|-
| [[Robinson R44]] 
| 1
| 3
| ZK-HCO
|}

==Services==
Air Safaris operate a range of services including: Scenic flights around the Mount Cook and Westland National Parks, charter flight services available from their two bases in Lake Tekapo and Franz Josef to many airports around New Zealand and other commercial flights including photography and filming, freight, survey and remote access work. The Lake Tekapo Link are Charter flights that are available from both [[Christchurch Airport|Christchurch]] and [[Queenstown Airport|Queenstown]] airports to [[Lake Tekapo]]. These are for passengers who wish to use the scenic flights of Air Safaris such as the Grand Traverse experience.<ref>{{cite web|title=Lake Tekapo Link: Christchurch-Tekapo-Queenstown|url=http://www.airsafaris.co.nz/scenic-flights/scenic-charters/|website=Air Safaris|accessdate=14 July 2015}}</ref>

==Gallery==
<gallery>
File:Air Safaris 3 Nomads.jpg|All 3 Air Safaris GAF Nomads together seen here at Glentanner airport, 1995
File:Air Safaris Grand Caravan.jpg|Air Safaris Grand Caravan at Tekapo, 2014
File:Gaf Nomad by Hangar.jpg|Air Safaris Nomad outside hangar at Tekapo, 2014
File:Air Safaris at CHC.jpg|Air Safaris Nomad on gate 3 at Christchurch Airport December 2014.
</gallery>

== See also ==
{{Portal|New Zealand|Companies|Aviation}}
* [[Transport in New Zealand#Air transport|Air transport in New Zealand]]
* [[List of airlines of New Zealand]]
*[[List of general aviation operators of New Zealand]]
{{Clear}}

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.airsafaris.co.nz}}

{{Airlines of New Zealand}}

[[Category:Airlines of New Zealand]]
[[Category:Airlines established in 1970]]