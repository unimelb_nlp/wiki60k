'''Environmental accounting''' is a subset of [[accountancy|accounting]] proper, its target being to incorporate both economic and environmental information. It can be conducted at the corporate level or at the level of a national economy through the [[System of Integrated Environmental and Economic Accounting]], a satellite system to the National Accounts of Countries{{ref|UN1}} (among other things, the National Accounts produce the estimates of Gross Domestic Product otherwise known as GDP).

Environmental accounting is a field that identifies resource use, measures and communicates costs of a company’s or national economic impact on the environment.  Costs include costs to clean up or [[Environmental remediation|remediate]] contaminated sites, environmental fines, penalties and taxes, purchase of pollution prevention technologies and waste management costs.

An environmental accounting system consists of environmentally differentiated conventional accounting and ecological accounting. Environmentally differentiated accounting measures effects of the [[natural environment]] on a [[company]] in [[monetary]] terms. Ecological accounting measures the influence a company has on the environment, but in physical measurements.

==Why environmental accounting?==

There are several advantages environmental accounting brings to business; notably, the complete costs, including environmental remediation and long term environmental consequences and externalities can be quantified and addressed.

More information about the statistical system of environmental accounts are available here: [[System of Integrated Environmental and Economic Accounting]].

== Subfields ==

Environmental accounting is organized in three sub-disciplines: global, national, and corporate environmental accounting, respectively. Corporate environmental accounting can be further sub-divided into environmental management accounting and environmental financial accounting.

* '''Global environmental accounting''' is an accounting methodology that deals areas includes energetics, ecology and economics at a worldwide level.
* '''National environmental accounting''' is an accounting approach that deals with economics on a country's level.

: Internationally, environmental accounting has been formalised into the System of Integrated Environmental and Economic Accounting, known as SEEA.{{ref|UN1}} SEEA grows out of the System of National Accounts. The SEEA records the flows of raw materials (water, energy, minerals, wood, etc.) from the environment to the economy, the exchanges of these materials within the economy and the returns of wastes and pollutants to the environment. Also recorded are the prices or shadow prices for these materials as are environment protection expenditures. SEEA is used by 49 countries around the world.{{ref|UN2}}

* '''Corporate environmental accounting''' focuses on the cost structure and environmental performance of a company.{{ref|EA}}
* '''Environmental management accounting''' focuses on making internal business strategy decisions.  It can be defined as:

:"''…the identification, collection, analysis, and use of two types of information for internal decision making:
:1)	Physical information on the use, flows and fates of energy, water and materials (including wastes) and
:2)	Monetary information on environmentally related costs, earnings and savings."''{{ref|Jasch}}

:As part of an environmental management accounting project in the State of Victoria, Australia, four case studies were undertaken in 2002 involving a school (Methodist Ladies College, Perth), plastics manufacturing company (Cormack Manufacturing Pty Ltd, Sydney), provider of office services (a service division of AMP, Australia wide) and wool processing (GH Michell & Sons Pty Ltd, Adelaide). Four major accounting professionals and firms were involved in the project; [[KPMG]] (Melbourne), [[Price Waterhouse Coopers]] (Sydney), Professor Craig Deegan, [[RMIT University]] (Melbourne) and BDO Consultants Pty Ltd (Perth). In February 2003, John Thwaites, The Victorian Minister for the Environment launched the report which summarised the results of the studies.<ref>[http://www.epa.vic.gov.au/bus/accounting/docs/final_report.pdf Environmental Management Accounting: An Introduction and Case Studies] (Adobe PDF file, 446KB)</ref>

:These studies were supported by the Department of Environment and Heritage of the [[Australian Federal Government]], and appear to have applied some of the principles outlined in the [[United Nations]] Division for Sustainable Development publication, ''Environmental Management Accounting Procedures and Principles'' (2001).

* '''Environmental financial accounting''' is used to provide information needed by external [[Stakeholder (corporate)|stakeholders]] on a company’s financial performance.  This type of accounting allows companies to prepare financial reports for investors, lenders and other interested parties.{{ref|EPA}}

==Examples of environmental accounting software==
* [http://www.ehsdata.com EHS Data's Environmental and Sustainability Accounting and Management System]
* [http://www.emisoft.com Emisoft's Total Environmental Accounting and Management System (TEAMS)]
* [https://www.nems.no add energy's NEMS Accounter]

== See also ==<!-- Please respect alphabetical order -->

{{Portal|Business and economics|Ecology|Environment}}

{{columns-list|3|
* [[Anthropogenic metabolism]]
* [[Carbon accounting]]
* [[Defensive expenditures]]
* [[Ecological economics]]
* [[Ecosystem Services]]
* [[Emergy|Emergy synthesis]]
* [[Environmental economics]]
* [[Environmental enterprise]]
* [[Environmental finance]]
* [[Environmental pricing reform]]
* [[Environmental profit and loss account]]
* [[Fiscal environmentalism]]
* [[Full cost accounting]] (FCA)
* [[Greenhouse gas emissions accounting]]
* [[Industrial metabolism]]
* [[Material flow accounting]]
* [[Material flow analysis]]
* [[Social metabolism]]
* [[Sustainability accounting]]
* [[System of Integrated Environmental and Economic Accounting]]
* [[Urban metabolism]]
}}

==References==

=== Notes ===
#{{note|UN1}} {{cite web | title = Handbook of National Accounting: Integrated Environmental and Economic Accounting 2003 | work = United Nations, European Commission, International Monetary Fund, Organistation for Economic Co-operation and Development and World Bank | url = http://unstats.un.org/unsd/envaccounting/seea2003.pdf}}
#{{note|EA}}{{cite web|title=Glossary of terminology and definitions |work=Environmental Agency, UK |url=http://www.environment-agency.gov.uk/business/444251/444754/252614/250569/?version=1&lang=_e |accessdate=2006-05-25 |deadurl=yes |archiveurl=https://web.archive.org/web/20060803104342/http://www.environment-agency.gov.uk/business/444251/444754/252614/250569/?version=1&lang=_e |archivedate=2006-08-03 |df= }} 
#{{note|EPA}}{{cite journal | author= Environmental Protection Agency | authorlink= | title= An introduction to environmental accounting as a business management tool: Key concepts and terms | journal= United States Environmental Protection Agency | year=1995}}
#{{note|Jasch}}{{cite journal | author= Jasch, C. | authorlink= | title= How to perform an environmental management cost assessment in one day | journal= Journal of Cleaner Production | year=2006| volume=14 |issue=14| pages=1194–1213 | doi= 10.1016/j.jclepro.2005.08.005}}
#{{note|UN1}} {{cite web | title = Handbook of National Accounting: Integrated Environmental and Economic Accounting 2003 | work = United Nations, European Commission, International Monetary Fund, Organistation for Economic Co-operation and Development and World Bank | url = http://unstats.un.org/unsd/envaccounting/seea2003.pdf}}
#{{note|UN2}} {{ cite web | title = Global Assessment of Environment Statistics and Environmental-Economic Accounting 2007| work = United Nations | url = http://unstats.un.org/unsd/statcom/doc07/Analysis_SC.pdf}}

=== Footnotes ===
{{reflist}}

===Further reading===
* Odum, H.T. (1996) Environmental Accounting: Emergy and Environmental Decision Making, Wiley, U.S.A.
* Tennenbaum, S.E. (1988) ''[http://www.esnips.com/doc/ac5215f1-e91d-4c30-8648-2eee1f31488c/Network_Energy_Thesis.pdf Network Energy Expenditures for Subsystem Production]'', MS Thesis. Gainesville, FL: University of FL, 131 pp. (CFW-88-08)

==External links==
*[http://unstats.un.org/unsd/envAccounting/default.htm United Nations Environmental Accounting]
*[http://www.gistindia.org  Green Accounting for Indian States Project]
*[http://www.environmentalscience.org/degree/environmental-mba-degree Environmental MBA Degree Info]
*[http://www.environmental-accounting.at Environmental Accounting in Austria] (Information about environmental accounts, structure, methods, legal basis, scope and application)
*[http://www.epa.vic.gov.au/bus/accounting/default.asp Environmental Management Accounting (EMA) Project], Victoria, Australia

{{Social accountability}}

[[Category:Environmental economics]]
[[Category:Sustainability metrics and indices]]
[[Category:Types of accounting]]