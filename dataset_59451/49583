{{DISPLAYTITLE:Caterpillar (''Alice's Adventures in Wonderland'')}}
{{Infobox character
|name        = Caterpillar
|series      = Alice
|image       = [[Image:Alice 05a-1116x1492.jpg|250px]]
|caption     = The Caterpillar using a [[hookah]]; an illustration by [[John Tenniel]]. The illustration is noted for its ambiguous central figure, whose head can be viewed as being a human male's face with a pointed nose and protruding chin or being the head end of an actual [[caterpillar]], with two "true" legs visible.<ref name=ambiguity>"And do you see its long nose and chin? At least, they ''look'' exactly like a nose and chin, don't they? But they really ''are'' two of its legs. You know a Caterpillar has got ''quantities'' of legs: you can see more of them, further down." Carroll, Lewis. ''The Nursery "Alice"''. Dover Publications (1966), p27.</ref>
|first       = ''[[Alice's Adventures in Wonderland]]''
|last        =
|cause       =
|creator     = [[Lewis Carroll]]
|portrayer   =
|episode     =
|nickname    = The Blue Caterpillar
|alias       = Hookah-Smoking Caterpillar, Absolem
|species     = [[Caterpillar]]
|gender      = Male
|age         =
|born        =
|death       =
|occupation  =
|nationality = [[Wonderland (fictional country)|Wonderland]]
}}

'''The Caterpillar''' (also known as the '''Hookah-Smoking Caterpillar''') is a [[fictional character]] appearing in [[Lewis Carroll]]'s book ''[[Alice's Adventures in Wonderland]]''.

==Appearance in ''Alice's Adventures in Wonderland''==
Introduced in Chapter Four ("Rabbit Sends in a Little Bill") and the main center of interest of Chapter V ("Advice from a Caterpillar"), the Caterpillar is a [[hookah]]-smoking [[caterpillar]] exactly three inches high (a height of which he argues in defense, against Alice's complaint). Alice does not like the Caterpillar when they first meet, because he does not immediately talk to her and when he does, it is usually in short, rather rude sentences, or [[problem solving|difficult questions]].

The original illustration by [[John Tenniel]] is something of a visual [[paradox]], wherein the caterpillar's human face appears to be formed from the head and legs of a naturalistic caterpillar.<ref name=ambiguity/>  In another allusion, the flowers on the right of the illustration appear to be a form of  [[Tobacco#Biology|tobacco]], while the caterpillar is smoking heavily.

==Other media==
The Caterpillar makes an appearance in a few other places outside ''[[Alice's Adventures in Wonderland]]'', such as ''[[American McGee's Alice]]'' and the novel ''[[The Looking-Glass Wars]]''; in both of these spin-offs he plays the role of an oracle.
* In the [[SyFy]] TV miniseries ''[[Alice (TV miniseries)|Alice]]'', the Caterpillar is the leader of the underground resistance to the [[Queen of Hearts (Alice's Adventures in Wonderland)|Queen of Hearts]].
* The Caterpillar also makes an appearance in "Curiouser and Curiouser," an episode of the television show ''[[Forever Knight]]'', in the form of a child's toy.
* In several [[Marvel Comics]] stories featuring [[Doctor Strange]], the magical entity [[Agamotto]] has appeared in a form resembling the Caterpillar.
* In popular music, the Caterpillar is mentioned in [[Jefferson Airplane]]'s "[[White Rabbit (song)|White Rabbit]]" (1967), a song containing many references to the book ''Alice's Adventures in Wonderland''.
* In one episode of ''[[Ouran High School Host Club]]'' called "Haruhi in Wonderland", Kyoya Otori is dressed as the Caterpillar.
* The Caterpillar appears in the Mad T Party at Disney's California Adventure park, where he plays the keyboard.
* The Caterpillar is also referenced in Melanie Martinez's song Mad Hatter.
* The Caterpillar turned moth is also a main male character in The Splintered Series by A.G. Howard, in this series his name is Morpheus

===Disney film===
{{infobox character
|colour  = #59E
|image   = [[File:CaterpillarDisney.jpg|200px|The Caterpillar as he appears in the 1951 [[Disney]] classic.]]
|name    = Blue Caterpillar
|creator = [[Lewis Carroll]]
|first   = ''[[Alice in Wonderland (1951 film)|Alice in Wonderland]]''
|voice   = [[Richard Haydn]] (original film) <br /> [[Corey Burton]] (''[[House of Mouse]]'')
|alias   =
}}
{{main article|Alice in Wonderland (1951 film)}}
His memorable phrase is a breathy "Whooo&nbsp;... are&nbsp;... you?". In the Disney animated film, this line is visualised as exhalations of smoke in the shapes "O", "R" and "U".  Alice remarks in the original story that the Caterpillar will one day [[Metamorphosis|turn into]] a [[butterfly]], and in both the 1999 television film and Disney's 1951 version he does so in Alice's presence. He is voiced by [[Richard Haydn]].

The Caterpillar in the Disney film is a blue creature who, as in the original Carroll story, smokes a hookah. He is seen as a very violent character as he yells at Alice quite often during the scenes in which they both appear. He blows smoke in Alice's face and when she needs assistance he ignores her. He is a quite mean character providing little to no assistance to Alice and ends up confusing her more while she is trapped in Wonderland. He then ignores her and turns into a butterfly and flutters away not caring whether or not Alice makes it out alive. He also instructs her to eat a mushroom but does not say what it does thus putting her into possible danger. He reappears one final time during the ending chase, still in butterfly form but once again smoking on his hookah, and again ignoring Alice when she asks for his help escaping the Queen of Hearts.<ref>{{Cite web|title = Caterpillar - Alice-in-Wonderland.net|url = http://www.alice-in-wonderland.net/resources/analysis/character-descriptions/caterpillar/|website = Alice-in-Wonderland.net|accessdate = 2015-10-20|language = en}}</ref>
[[File:Alice in Wonderland by Arthur Rackham - 05 - Advice from a Caterpillar.jpg|centre|thumb]]
{{Clear}}

===Tim Burton films===
{{main article|Alice in Wonderland (2010 film)}}
{{infobox character
|colour  = #59E
|image   =
|name    = Absolem the Caterpillar
|creator = Lewis Carroll
|first   = [[Alice in Wonderland (2010 film)|Alice in Wonderland]]
|voice   = [[Alan Rickman]]
|alias   =
}}
[[Alan Rickman]] voices the Caterpillar, who in this adaptation is named "Absolem".<ref name="script">{{cite web
|url=http://www.joblo.com/scripts/Alice%20in%20Wonderland.pdf
|title=Alice in Wonderland – Glossary of Terms/Script (early draft)
|work=Walt Disney Pictures
|publisher=JoBlo.com
|format=PDF
|accessdate=March 30, 2010}} (early draft of the film script, first started Feb. 2007)</ref> Rickman was filmed while recording his voice in a studio, but his face was not composited onto the character's face as originally planned.<ref name="animation">{{cite news|first=Fred|last=Topel|url =http://crushable.com/entertainment/alan-rickman-talks-about-alice-in-wonderland/|title=Alan Rickman talks about Alice in Wonderland|date =December 19, 2008|work =Crushable.com|accessdate=February 21, 2010}}</ref>

He appears five times in the movie. The first time is outside Wonderland, when a young man named Hamish Ascot is about to [[Marriage proposal|propose]] to Alice and she notices a blue caterpillar on his shoulder. The second time is when [[White Rabbit|Nivens McTwisp the White Rabbit]], [[Tweedledum and Tweedledee]], [[The Dormouse|Mallymkun the Dormouse]], and the [[Dodo (Alice's Adventures in Wonderland)|Uilleam the Dodo]] consider Alice's identity, and they consult him. Absolem appears in a thick cloud of hookah smoke, which he blows at Alice.  He appears again after Alice arrives at the [[White Queen (Through the Looking Glass)|White Queen]]'s Castle, and again to remind Alice of her previous visit to Wonderland. He blows smoke at her twice this time, and Alice asks him to stop it. At the end of the movie, Absolem, as a butterfly, appears on Alice's shoulder as she sets off for China.

Rickman reprises the role in ''[[Alice Through the Looking Glass (2016 film)|Alice Through the Looking Glass]]'' (2016), though he remains a butterfly, leading Alice to the looking-glass portal to Underland and informing her that matters require her urgent attention. He isn't seen again for the rest of the film but makes a cameo in the credits. This was one of his final film performances and the film is dedicated in his memory.
{{Clear}}

===''Once Upon a Time''===
* The Caterpillar appears in an episode of ''[[Once Upon a Time (TV series)|Once Upon a Time]]'', voiced by [[Roger Daltrey]].
* The Caterpillar appears in the ''[[Once Upon a Time in Wonderland]]'' episode "Forget Me Not" voiced by [[Iggy Pop]], wherein he operates a meeting-house for the transaction of criminal business. He is consulted twice by Alice for assistance in an adventure of her own.

==References==
{{reflist}}

{{Alice|state=expanded}}

[[Category:Lewis Carroll characters|Caterpillar]]
[[Category:Disney's Alice in Wonderland characters]]
[[Category:Fictional butterflies and moths]]
[[Category:Kingdom Hearts characters]]
[[Category:Fictional characters introduced in 1865]]