{{good article}}
{{Taxobox 
| name = Bare-tailed woolly opossum
| status = LC
| status_system = iucn3.1
| status_ref = <ref name=iucn>{{cite journal | author = Brito, D. | author2 = Astua de Moraes, D. | author3 = Lew, D. | author4 = Soriano, P. | author5 = Emmons, L. | title = ''Caluromys philander'' | journal = [[IUCN Red List of Threatened Species]] | volume= 2015 | page = e.T3649A22175720 | publisher = [[IUCN]] | year = 2015 | url = http://www.iucnredlist.org/details/3649/0 | doi = 10.2305/IUCN.UK.2015-4.RLTS.T3649A22175720.en | accessdate = 11 June 2016}}</ref>
| trend = unknown
| image = Cuíca-lanosa.jpg
| image_width = 250px
| regnum = [[Animalia]]
| phylum = [[Chordata]]
| classis = [[Mammal]]ia
| infraclassis = [[Marsupialia]]
| ordo = [[Didelphimorphia]]
| familia = [[Caluromyidae]]
| genus = ''[[Caluromys]]''
| subgenus = ''[[Caluromys]]''
| species = '''''C. philander'''''
| binomial = ''Caluromys philander''
| binomial_authority = ([[Carl Linnaeus|Linnaeus]], [[10th edition of Systema Naturae|1758]])
| subdivision_ranks = [[Subspecies]]
| subdivision =
*''C. p. affinis'' <small>Wagner, 1842</small>
*''C. p. dichurus'' <small>Wagner, 1842</small>
*''C. p. philander'' <small>Linnaeus, 1758</small>
*''C. p. trinitatis'' <small>[[Oldfield Thomas|Thomas]], 1894</small>
| synonyms = ''Didelphis philander'' <small>Linnaeus, 1758</small><br>
''Philander philander'' <small>[[Ángel Cabrera (naturalist)|Cabrera]], 1919</small>
| synonyms_ref = <ref name=gardner/>
| range_map = Bare-tailed Woolly Opossum area.png
| range_map_caption = Range of the bare-tailed woolly opossum
}}

The '''bare-tailed woolly opossum''' (''Caluromys philander'') is an [[opossum]] from [[South America]]. It was first described by Swedish zoologist [[Carl Linnaeus]] in 1758. The bare-tailed woolly opossum is characterized by a gray head, brown to gray coat, orange to gray underside and a partially naked tail. It is [[nocturnality|nocturnal]] (active mainly at night) and solitary; there is hardly any social interaction except between mother and juveniles and in mating pairs. The opossum constructs nests in tree cavities, and its litter size ranges from one to seven. Gestation lasts 25 days, and the juveniles exit the pouch after three months; weaning occurs a month later. The bare-tailed woolly opossum inhabits [[subtropic]]al forests, rainforests, secondary forests and plantations; its range extends from northern [[Venezuela]] to northeastern and southcentral [[Brazil]]. The [[IUCN]] classifies this opossum as [[least concern]].

== Taxonomy ==
The bare-tailed woolly opossum is one of the three members of ''[[Caluromys]]'', and is placed in the family [[Didelphidae]] in the [[marsupial]] order [[Didelphimorphia]]. It was first [[Species description|described]] by Swedish zoologist [[Carl Linnaeus]] as ''Didelphis philander'' in the [[10th edition of Systema Naturae|10th edition of ''Systema Naturae'']] (1758). It was given its present binomial name, ''Caluromys philander'', by American zoologist [[Joel Asaph Allen]] in 1900.<ref name=gardner/><ref name=MSW3>{{MSW3 Didelphimorphia | id = 10400021 | page = 4}}</ref> A 1955 revision of marsupial phylogeny grouped ''Caluromys'', ''[[Caluromysiops]]'', ''[[Dromiciops]]'' (monito del monte) and ''[[Glironia]]'' (bushy-tailed opossum) under a single subfamily, Microbiotheriinae, noting the dental similarities among these. A 1977 study argued that these similarities are the result of [[convergent evolution]], and placed ''Caluromys'', ''Caluromysiops'' and ''Glironia'' in a new subfamily, [[Caluromyinae]].<ref name="mammal">{{cite journal|last1=Larry|first1=Marshall|title=''Glironia venusta''|journal=[[Mammalian Species]]|issue=1978|pages=1–3|jstor=3504067|url=http://www.science.smith.edu/msi/pdf/i0076-3519-107-01-0001.pdf}}</ref> In another similar revision in 2009, the bushy-tailed opossum was placed in its own subfamily, Glironiinae.<ref>{{cite journal|title=Phylogenetic relationships and classification of didelphid marsupials, an extant radiation of New World metatherian mammals|first1=R.S.|last1=Voss|last2= Jansa|first2=S.A.|journal=[[Bulletin of the American Museum of Natural History]]|year=2009|volume=322|pages=1–177|doi=10.1206/322.1}}</ref>

The following four subspecies are recognized:<ref name=gardner>{{cite book|editor1-last=Gardner|editor1-first=A.L.|title=Mammals of South America|volume=1|date=2007|publisher=University of Chicago Press|location=Chicago, US|isbn=978-0-226-28242-8|pages=9–11|url={{Google Books|id=dbU3d7EUCm8C|page=9|plainurl=yes}}}}</ref><ref name=MSW3/>

*''C. p. affinis'' <small>Wagner, 1842</small>: Occurs in [[Mato Grosso]] ([[Brazil]]) and [[Bolivia]].
*''C. p. dichurus'' <small>Wagner, 1842</small>: Occurs in eastern and southeastern Brazil.
*''C. p. philander'' <small>Linnaeus, 1758</small>: Occurs to the east of [[Rio Negro (Amazon)|Rio Negro]] in Brazil, the [[Guianas]], and to the south of the [[Orinoco River]] in [[Venezuela]].
*''C. p. trinitatis'' <small>[[Oldfield Thomas|Thomas]], 1894</small>: Occurs in [[Trinidad]] and to the north of the Orinoco River in Venezuela.

The [[cladogram]] below, based on a 2016 study, shows the [[phylogenetic]] relationships of the bare-tailed woolly opossum.<ref>{{cite journal|last1=Amador|first1=L.I.|last2=Giannini|first2=N.P.|title=Phylogeny and evolution of body mass in didelphid marsupials (Marsupialia: Didelphimorphia: Didelphidae)|journal=Organisms Diversity & Evolution|date=2016|pages=1–17|doi=10.1007/s13127-015-0259-x|url=https://www.researchgate.net/publication/290197599_Phylogeny_and_evolution_of_body_mass_in_didelphid_marsupials_Marsupialia_Didelphimorphia_Didelphidae}}</ref>

{{clade| style=font-size:90%;line-height:100%;
|1={{clade
|1={{clade
    |1=[[Bushy-tailed opossum]] (''Glironia venusta'')
    |label2=[[Caluromyinae]]
    |2={{clade
       |1=[[Black-shouldered opossum]] (''Caluromyopsis irrupta'')
       |2={{clade
          |1=[[Derby's woolly opossum]] (''Caluromys derbianus'')
          |2={{clade
             |1='''Bare-tailed woolly opossum''' (''Caluromys philander'')
             |2=[[Brown-eared woolly opossum]] (''Caluromys lanatus'')
             }}
          }}
       }}
   }}
|2={{clade
   |1=[[Kalinowski's mouse opossum]] (''Hyladelphys kalinowskii'')
   |2={{clade
      |1=[[Marmosini]]
      |2={{clade
         |1=[[Didelphini]]
         |2=[[Thylamyini]]
         }}
      }}
   }}
}}
}}

==Description==
[[File:Caluromys philander.JPG|thumb|right|Close view of a bare-tailed woolly opossum]] 
[[File:Caluromys philander Schreber.jpg|thumb|right|A 1780 illustration by [[Johann Christian Daniel von Schreber]]]]
The bare-tailed woolly opossum is characterized by a brown to gray coat, gray head, orange to gray underside and a partially naked tail furry at the base.<ref name=eisenberg/> A distinctive, narrow dark brown stripe runs between the eyes and the ears, from the tip of the nose to the back of the ears. Similar but broad streaks run from brown rings around either eye. Grayish fur separates these stripes from one another. Ears are large and almost always hairless. The coat is thick, soft and woolly; the flanks may be grayer than the back. The dorsal hairs continue up to {{convert|5|–|7|cm|in}} onto the tail, after which it is naked, as the name suggests. The tail is dark brown towards the end, spotted with white and dark brown, terminating in a white or yellowish white tip.<ref name="Husson">{{cite book|last1=Husson|first1=A.M.|title=The Mammals of Suriname|date=1978|publisher=Brill|location=Leiden, Netherlands|isbn=978-90-04-05819-4|pages=9|url={{Google Books|id=1s8UAAAAIAAJ|page=9|plainurl=yes}}}}</ref>

The size appears to decrease from [[Venezuela]] to [[Suriname]]; the mean weight is {{convert|170|g|oz}} in Venezuela and {{convert|250|g|oz}} in Suriname. The head-and-body length is typically between {{convert|16|and|26|cm|in}}. The ears measure {{convert|3|to|3.5|cm|in}}, the tail {{convert|25|to|36|cm|in}} and the hind feet {{convert|3.2|to|3.9|cm|in}}.<ref name="eisenberg">{{cite book|last1=Eisenberg|first1=J.F.|last2=Redford|first2=K.H.|title=The Central Neotropics: Ecuador, Peru, Bolivia, Brazil|date=1999|publisher=University of Chicago Press|location=Chicago, US|isbn=978-0-226-19542-1|pages=79–80|url={{Google Books|id=p2MDAzCeQQoC|page=83|plainurl=yes}}}}</ref> The [[dental formula]] is {{DentalFormula|upper=5.1.3.4|lower=4.1.3.4}} – typical of didelphids.<ref name=Husson/>

==Ecology and behavior==
The bare-tailed woolly opossum is [[nocturnality|nocturnal]] (active mainly at night), and thus difficult to observe or capture. Nevertheless, it is one of the very few opossums that have been successfully studied in detail. A study showed that activity of bare-tailed woolly opossums can be affected by the extent of moonlight. While activity in males dropped from new moon to full moon (that is, with increasing exposure to moonlight), activity in females remained largely unaffected.<ref>{{cite journal|last1=Julien-Laferriere|first1=D.|title=The influence of moonlight on activity of woolly opossums (''Caluromys philander'')|journal=Journal of Mammalogy|date=1997|volume=78|issue=1|pages=251–5|doi=10.2307/1382659|url=http://jmammal.oxfordjournals.org/content/jmammal/78/1/251.full.pdf}}</ref> The opossum is [[arboreal]] (tree-living) and a good climber.<ref name=Husson/> A study showed that the tail, being [[prehensile tail|prehensile]], can act as an additional limb for locomotion, avoiding falls and carrying leaves to build nests.<ref>{{cite journal|last1=Dalloz|first1=M.F.|last2=Loretto|first2=D.|last3=Papi|first3=B.|last4=Cobra|first4=P.|last5=Vieira|first5=M.V.|title=Positional behaviour and tail use by the bare-tailed woolly opossum ''Caluromys philander'' (Didelphimorphia, Didelphidae)|journal=Mammalian Biology – Zeitschrift für Säugetierkunde|date=2012|volume=77|issue=5|pages=307–13|doi=10.1016/j.mambio.2012.03.001}}</ref> It builds nests with dry leaves in tree cavities.<ref name=eisenberg/>

Individuals tend to be aggressive to one another; hisses, grunts and even distress calls accompany agonistic behavior. Largely solitary, the only interactions observed are between mother and juveniles and in a mating pair. In a [[primary forest]] of [[French Guiana]], the mean [[home range]] size was calculated as {{convert|3|ha|sqmi}}. Ranges of both sexes overlapped extensively. The size of home ranges is influenced by environmental factors such as forage availability and individual needs.<ref>{{cite journal|last1=Julien-Laferrière|first1=D.|title=Use of space by the woolly opossum ''Caluromys philander'' Marsupialia, Didelphidae) in French Guiana|journal=Canadian Journal of Zoology|date=1995|volume=73|issue=7|pages=1280–9|doi=10.1139/z95-152}}</ref> 'Click's are a common vocalization, produced by the young as well as adults. Bare-tailed woolly opossums, like other ''Caluromys'' species, will bite on being handled or to escape predators.<ref name=eisenberg/> Predators include the [[jaguarundi]] and [[margay]].<ref>{{cite journal|last1=Bianchi|first1=R.|last2=Rosa|first2=A.F|last3=Gatti|first3=A.|last4=Mendes|first4=S.L|title=Diet of margay, ''Leopardus wiedii'', and jaguarundi, ''Puma yagouaroundi'', (Carnivora: Felidae) in Atlantic Rainforest, Brazil|journal=Zoologia (Curitiba, Impresso)|date=2011|volume=28|issue=1|pages=127–32|doi=10.1590/S1984-46702011000100018}}</ref>

===Diet===
An [[omnivore]], the bare-tailed woolly opossum feeds on fruits, vegetables, [[Gum (botany)|gum]], nectar, small birds and reptiles.<ref name=eisenberg/><ref name=Husson/> A study of the foraging behavior of the bare-tailed woolly opossum and the [[sympatric]] [[kinkajou]] showed that both feed on a variety of plants, choose plants by their abundance, show similar preferences, and favor certain plant parts at certain times of the year. A notable difference between the two was that while the kinkajou focused on plants with a wide distribution, the bare-tailed woolly opossum also fed on less common plants.<ref>{{cite journal|last1=Julien-Laferriere|first1=D.|title=Foraging strategies and food partitioning in the neotropical frugivorous mammals ''Caluromys philander'' and ''Potos flavus''|journal=Journal of Zoology|date=1999|volume=247|issue=1|pages=71–80|doi=10.1111/j.1469-7998.1999.tb00194.x}}</ref>

===Reproduction===
In French Guiana, females mate successfully after they are a year old. Females can have three litters a year, unless food is scarce.<ref name=eisenberg/> Gestation lasts 25 days – the longest among didelphomorphs; the young come out of the [[Pouch (marsupial)|pouch]] at three months and weaning occurs at four months.<ref name="nowak">{{cite book|last1=Nowak|first1=R.M.|title=Walker's Marsupials of the World|date=2005|publisher=Johns Hopkins University Press|location=Baltimore, US|isbn=978-0-8018-8211-1|pages=78–9|url={{Google Books|id=ldXtY8ppxSQC|plainurl=yes|page=78}}}}</ref> A study in French Guiana showed that development of the offspring is slow for the first 40 days, and then accelerates during the last 40 days.<ref>{{cite journal|last1=Atramentowicz|first1=M.|title=Growth of pouch young in the bare-tailed woolly opossum, ''Caluromys philander''|journal=Journal of Mammalogy|date=1995|volume=76|issue=4|pages=1213–9|doi=10.2307/1382614|jstor=1382614}}</ref> The litter size ranges from one to seven.<ref name=nowak/> Newborn weigh {{convert|200|mg|oz}}, and their weight increases to {{convert|11|g|oz}} after weaning.<ref>{{cite book|last1=Hayssen|first1=V.|last2=Tienhoven|first2=A.|last3=Tienhoven|first3=A.|title=Asdell's Patterns of Mammalian Reproduction: A Compendium of Species-specific Data|date=1993|publisher=Cornell University Press|location=Ithaca, US|isbn=978-0-8014-1753-5|pages=12–8|edition=Rev. 2nd|url={{Google Books|id=yQzSe71g2AcC|page=12|plainurl=yes}}}}</ref> After exiting the pouch, offspring are sheltered in nests, where the mother regularly visits them for nursing.<ref name=eisenberg/>

==Distribution and status==
The bare-tailed woolly opossum inhabits [[subtropic]]al forests, rainforests, [[secondary forest]]s and plantations; it prefers dense cover, though it can be seen on [[Canopy (biology)|canopies]] as well. It can occur up to an altitude of {{convert|1,200|–|1,800|m|ft}} above the sea level. The range extends from northern Venezuela eastward to northeastern and southcentral Brazil, and includes Guiana, French Guiana, [[Margarita Island]], Trinidad, and Suriname. The [[IUCN]] classifies the bare-tailed woolly opossum as [[least concern]], due to its wide distribution and presumably large population. The survival of this opossum is threatened by deforestation and habitat loss.<ref name=iucn/>

==References==
{{reflist|30em}}

==External links==
*{{wikispecies-inline|Caluromys philander}}
*{{commonscat-inline|Caluromys philander}}

{{Didelphimorphia|C.}}
{{portal bar|Mammals|Animals|Biology|South America}}
{{taxonbar}}

[[Category:Opossums]]
[[Category:Mammals of South America|Opossum, Bare-tailed Woolly]]
[[Category:Mammals of Bolivia|Opossum, Bare-tailed Woolly]]
[[Category:Mammals of Brazil|Opossum, Bare-tailed Woolly]]
[[Category:Mammals of French Guiana|Opossum, Bare-tailed Woolly]]
[[Category:Mammals of Guyana|Opossum, Bare-tailed Woolly]]
[[Category:Mammals of Suriname|Opossum, Bare-tailed Woolly]]
[[Category:Mammals of Trinidad and Tobago|Opossum, Bare-tailed Woolly]]
[[Category:Mammals of Venezuela|Opossum, Bare-tailed Woolly]]
[[Category:Fauna of the Amazon|Opossum, Bare-tailed Woolly]]
[[Category:Fauna of the Guianas|Opossum, Bare-tailed Woolly]]
[[Category:Animals described in 1758]]
[[Category:Taxa named by Carl Linnaeus]]