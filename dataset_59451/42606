<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Model K
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Flying boat]]
 | national origin=[[United States of America]]
 | manufacturer=[[Curtiss Aeroplane and Motor Company|Curtiss]]
 | designer=
 | first flight=1915
 | introduced=
 | retired=
 | status=
 | primary user=[[Imperial Russian Navy]]
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=at least 51
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= [[Curtiss Model F]]
 | variants with their own articles=
}}
|}
The '''Curtiss Model K''', also known as the '''Model 4''', was an [[United States of America|American]] single-engined [[flying boat]] of [[World War I]]. It was an enlarged derivative of Curtiss's [[Curtiss Model F|Model F]] and about 50 were built for export to the [[Russian Navy]].

==Design and development==
In 1914, the [[Curtiss Aeroplane and Motor Company|Curtiss Aeroplane Company]] developed its Model K, an enlarged development of its successful Model F [[flying boat]].  It was a three-[[interplane strut|bay]] [[biplane]] powered by a 150&nbsp;hp (112&nbsp;kW) Curtiss V-X engine mounted in a [[pusher configuration]] between the wings. Unlike the Model F, its wings were [[Stagger (aviation)|staggered]] and slightly swept, while its [[aileron]]s were mounted on the upper wing instead of between the wings.<ref name="Bowers p106">Bowers 1979, p. 106.</ref><ref name="Johnson p38">Johnson 2009, p. 38.</ref>

The first flight of the Model K was delayed by problems with its engine until January 1915, with it being claimed that the aircraft was the largest single-engined flying boat in the world at the time.<ref name="Johnson p38"/>

==Operational history==
While the Model K did not attract orders from home,<ref name="Bowers p107"/> attempts to export it were more successful, resulting in an order for at least 51 aircraft in both flying boat and landplane versions from the [[Imperial Russian Navy]] in 1914.<ref name="Aerofiles">[http://aerofiles.com/_curtx.html "Curtiss K through Z"]. ''Aerofiles''. 24 January 2009. Retrieved 5 March 2011.</ref>{{refn|Johnson states that the order, for 54 aircraft was placed in late 1915, with deliveries starting in 1916.<ref name="Johnson p38"/>|group=N}} The crated aircraft were shipped via [[Vancouver]] and [[Vladivostok]], resulting in serious delays in the aircraft being reassembled, such that many of them were unseaworthy due to their hulls having cracked.<ref name="Bowers p107"/><ref name="Johnson p38-9">Johnson 2009, pp. 38–39.</ref>

==Operators==
;{{flag|Russian Empire|1914}}
*[[Imperial Russian Navy]]

==Specifications==
{{Aircraft specs
|ref=Curtiss Aircraft 1907–1947<ref name="Bowers p107">Bowers 1979, p. 107.</ref>
|prime units?=imp
<!--
        General characteristics
-->
|crew=three
|capacity=
|length m=9.58
|length ft=
|length in=
|length note=
|span m=17.01
|span ft=
|span in=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=592
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=
|empty weight lb=2700
|empty weight note=
|gross weight kg=
|gross weight lb=3900
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Curtiss V-X]]
|eng1 type=
|eng1 kw=<!-- prop engines -->
|eng1 hp=150<!-- prop engines -->
|eng1 note=
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=70
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=364
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=150
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=

|power/mass=
|thrust/weight=

|more performance=
<!--
        Armament
-->
|armament=<!-- add (bulleted list) here or if you want to use the following specific parameters, remove this parameter-->
|guns= 
|bombs= 
|rockets= 
|missiles=

|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|other armament=

|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{commons category|Curtiss aircraft}}
===Notes===
{{reflist|group=N}}
===Citations===
{{reflist}}
===Bibliography===
{{refbegin}}
*Bowers, Peter M. ''Curtiss Aircraft 1907–1947''. London: Putnam, 1979. ISBN 0-370-10029-8.
*Johnson, E.R. ''[https://books.google.com/books?id=AtqOSxG9N1YC&printsec=frontcover#v=onepage&q&f=false American Flying Boats and Amphibious Aircraft: An Illustrated History]''. Jefferson, North Carolina, USA: McFarland & Company, 2009. ISBN 978-0-7864-3974-4.
<!-- insert the reference sources here -->
{{refend}}
<!-- ==External links== -->
{{Curtiss aircraft}}

[[Category:United States military utility aircraft 1910–1919]]
[[Category:Flying boats]]
[[Category:Curtiss aircraft|Model K]]
[[Category:Single-engined pusher aircraft]]
[[Category:Biplanes]]