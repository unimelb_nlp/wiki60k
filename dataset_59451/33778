{{for|the 1920s racing and speed record car|Sunbeam Tiger (1925)}}
{{featured article}}
{{Infobox automobile
| name = Sunbeam Tiger
| image = Sunbeam Tiger(3).jpg
| alt = Front left view of red car
| manufacturer = [[Rootes Group]]
| production = 1964–67<br />7083 built
| assembly = [[West Bromwich]], England
| class =[[Sports car]]
| body_style = 2-door [[Roadster (automobile)|roadster]]
| related = [[Sunbeam Alpine]]
| layout = [[FR layout]]
| engine = Tiger I: {{convert|260|cuin|L|1|abbr=on}} [[V8 engine|V8]] ([[Ford Windsor engine#260|Ford]])<br />Tiger II: {{convert|289|cuin|L|1|abbr=on}} [[V8 engine|V8]] ([[Ford Windsor engine#289|Ford]])
| transmission = Ford 4-speed [[Manual transmission|manual]]
| wheelbase = {{convert|86|in|mm|0|abbr=on|format=off}}{{sfnp|Robson|2012|p=111|ps=none}}
| length = {{convert|156|in|mm|0|abbr=on|format=off}}{{sfnp|Robson|2012|p=111|ps=none}}
| width = {{convert|60.5|in|mm|0|abbr=on|format=off}}{{sfnp|Robson|2012|p=111|ps=none}}
| height = {{convert|51.5|in|mm|0|abbr=on|format=off}}{{sfnp|Robson|2012|p=111|ps=none}}
| sp = uk
}}
The '''Sunbeam Tiger''' is a high-performance [[V8 engine|V8]] version of the British [[Rootes Group]]'s [[Sunbeam Alpine]] [[Roadster (automobile)|roadster]], designed in part by American car designer and racing driver [[Carroll Shelby]] and produced from 1964 until 1967. Shelby had carried out a similar V8 conversion on the [[AC Cobra]], and hoped to be offered the contract to produce the Tiger at his facility in America. Rootes decided instead to contract the assembly work to [[Jensen Motors|Jensen]] at [[West Bromwich]] in England, and pay Shelby a [[Royalties|royalty]] on every car produced.

Two major versions of the Tiger were built: the Mark I (1964–67) was fitted with the {{convert|260|cuin|L|1|abbr=on}} [[Ford Windsor engine#260|Ford V8]]; the Mark II, of which only 633 were built in the final year of Tiger production, was fitted with the larger Ford {{convert|289|cuin|L|1|abbr=on}} engine. Two [[prototype]] and extensively modified versions of the Mark I competed in the [[1964 24 Hours of Le Mans]], but neither completed the race. Rootes also entered the Tiger in European [[Rallying|rallies]] with some success, and for two years it was the [[American Hot Rod Association]]'s national record holder over a quarter-mile [[dragstrip|drag strip]].

Production ended in 1967 soon after the Rootes Group was taken over by [[Chrysler]], which did not have a suitable engine to replace the Ford V8. Owing to the ease and affordability of modifying the Tiger, there are few surviving cars in standard form.{{r|motortrend}}

==Background==
The Sunbeam Tiger was a development of the [[Sunbeam Alpine#Alpine Series I to V|Sunbeam Alpine series I]], introduced by the British manufacturer [[Rootes Group|Rootes]] in 1959.{{sfnp|Robson|2012|p=47|ps=none}} Rootes realised that the Alpine needed more power if it was to compete successfully in world markets, but lacked a suitable engine and the resources to develop one. The company therefore approached [[Ferrari]] to redesign the standard [[inline-four engine|inline-four cylinder engine]], recognising the sales cachet that "powered by Ferrari" would be likely to bring. Negotiations initially seemed to go well, but ultimately broke down.{{sfnp|Carroll|2001|p=10|ps=none}}

In 1962 racing driver and [[Formula One|Formula 1]] champion [[Jack Brabham]] proposed to Rootes competition manager Norman Garrad the idea of fitting the Alpine with a Ford V8 engine,{{efn|Jack Brabham floated the idea with Norman Garrad after he and [[Stirling Moss]] had co-driven an Alpine to second place overall in the Production Car Class of the [[Los Angeles Times Grand Prix]] held in October 1962 at [[Riverside International Raceway|Riverside]], California.{{sfnp|Carroll|2001|p=11|ps=none}}}} which Garrad relayed to his son [[Ian Garrad|Ian]], then the West Coast Sales Manager of Rootes American Motors Inc. Ian Garrad lived close to where [[Carroll Shelby]] had his Shelby American operation, which had done a similar V8 conversion for the British [[AC Cobra]].{{sfnp|Robson|2012|p=96|ps=none}}

==Initial prototypes==
According to journalist William Carroll, after measuring the Alpine's engine bay with "a 'precision' instrument of questionable antecedents"&nbsp;– a wooden [[yardstick]] – Ian Garrad despatched his service manager Walter McKenzie to visit the local new car dealerships, looking for a V8 engine that might fit.{{sfnp|Carroll|2001|p=11|ps=none}} McKenzie returned with the news that the Ford [[Ford Windsor engine#260|260 V8 engine]] appeared to be suitable,{{sfnp|Carroll|2001|p=11|ps=none}} which apart from its size advantage was relatively light at {{convert|440|lb|abbr=on}}.{{sfnp|Robson|2012|p=97|ps=none}} Ian Garrad asked Shelby for an idea of the timescale and cost to build a prototype, which Shelby estimated to be eight weeks and $10,000. He then approached Brian Rootes, head of sales for the Rootes Group, for funding and authorisation to build a prototype, to which Brian Rootes agreed.{{sfnp|Robson|2012|p=97|ps=}}

{{Quote box |width=25em |quoted=true |bgcolor=#FFFFF0 |salign=center |quote=Well all right, at that price when can we start? But for God's sake keep it quiet from Dad [Lord Rootes] until you hear from me. I'll work the $10,000 (£3,571) out some way, possibly from the advertising account.|source=Brian Rootes{{sfnp|Carroll|2001|p=13|ps=none}}}}
Ian Garrad, impatient to establish whether the conversion was feasible, commissioned racing driver and fabricator [[Ken Miles]] to build another prototype as quickly as he could. Miles was provided with a budget of $800, a Series II Alpine, a Ford V8 engine and a 2-speed [[automatic transmission]], and in about a week he had a running V8 conversion, thus proving the concept.{{sfnp|Robson|2012|pp=97, 99|ps=none}}

Shelby began work on his prototype, the white car as it came to be known, in April 1963,{{sfnp|Robson|2012|p=99|ps=none}} and by the end of the month it was ready for trial runs around [[Los Angeles]].{{sfnp|Robson|2012|p=103|ps=none}} Ian Garrad and John Panks, director of Rootes Motors Inc. of North America, tested an early version of the car and were so impressed that Panks wrote a glowing report to Brian Rootes: "we have a tremendously exciting sports car which handles extremely well and has a performance equivalent to an XX-K Jaguar{{efn|Panks was almost certainly referring to the [[E-type Jaguar]].{{sfnp|Robson|2012|p=103|ps=none}}}}&nbsp;... it is quite apparent that we have a most successful experiment that can now be developed into a production car."{{sfnp|Robson|2012|p=103|ps=none}}

Provisionally known as the Thunderbolt,{{r|ClassicCarMart}} the Shelby prototype was more polished than the Miles version, and used a Ford 4-speed [[manual transmission]]. The Ford V8 was only 3.5 inches longer than the Alpine's 4-cylinder engine it replaced, so the primary concern was the engine's width.{{sfnp|Clarke|2005|p=108|ps=none}} Like Miles, Shelby found that the Ford V8 would only just fit into the Alpine engine bay: "I think that if the figure of speech about the shoehorn ever applied to anything, it surely did to the tight squeak in getting that 260 Ford mill into the Sunbeam engine compartment. There was a place for everything and a space for everything, but positively not an inch to spare."{{sfnp|Robson|2012|p=99|ps=none}}{{sfnp|Shelby|Bentley|1965|p=218|ps=none}}

==Development==
[[File:Sunbeam Tiger Ford engine.jpg|thumb|alt=View of the cramped engine bay|left|Lack of space under the bonnet makes some maintenance tasks difficult.]]
All Rootes products had to be approved by [[William Rootes, 1st Baron Rootes|Lord Rootes]], who was reportedly "very grumpy" when he learned of the work that had gone into the Tiger project without his knowledge. But he agreed to have the Shelby prototype shipped over from America in July 1963 for him and his team to assess. He insisted on driving the car himself, and was so impressed that shortly after returning from his test drive he contacted [[Henry Ford II]] directly to negotiate a deal for the supply of Ford V8 engines.{{sfnp|Robson|2012|pp=103–4|ps=none}} Rootes placed an initial order for 3000,{{sfnp|Gunnell|2004|p=178|ps=none}} the number of Tigers it expected to sell in the first year,{{sfnp|Carroll|2001|p=47|ps=none}} the largest single order Ford had ever received for its engines from an automobile manufacturer.{{sfnp|Carroll|2001|p=34|ps=none}} Not only did Lord Rootes agree that the car would go into production, but he decided that it should be launched at the 1964 [[New York International Auto Show|New York Motor Show]], only eight months away, despite the company's normal development cycle from "good idea" to delivery of the final product being three to four years.{{sfnp|Robson|2012|p=104|ps=none}}

Installing such a large engine in a relatively small vehicle required some modifications, although the exterior sheet metal remained essentially the same as the Alpine's. Necessary chassis modifications included moving from the Burman [[recirculating ball]] steering mechanism to a more modern [[rack and pinion]] system.{{sfnp|Robson|2012|p=101|ps=none}}

Although twice as powerful as the Alpine, the Tiger is only about twenty per cent heavier,{{sfnp|Robson|2012|p=174|ps=}} but the extra weight of the larger engine required some minor suspension modifications. Nevertheless, the Tiger's front-to-back weight ratio is substantially similar to the Alpine's, at 51.7/48.3 front/rear.{{sfnp|Clarke|2005|p=8|ps=none}}

Shortly before its public unveiling at the New York Motor Show in April 1964 the car was renamed from Thunderbolt to Tiger, inspired by Sunbeam's [[Sunbeam Tiger (1925)|1925 land-speed-record holder]].{{r|SpeedChannel}}{{efn|The 1925 Sunbeam Tiger was the last car to be competitive as a land speed record holder and a circuit-racing car.{{sfnp|Holthusen|1986|p=33|ps=none}}}}

==Production==
[[File:Sunbeam tiger v8.jpg|thumb|right|alt=Photograph|The chrome strips either side of the Tiger logo show this to be a Series I car]]
Shelby had hoped to be given the contract to produce the Tiger in America, but Rootes was somewhat uneasy about the closeness of his relationship with Ford, so it was decided to build the car in England.{{sfnp|Robson|2012|p=105|ps=none}} The Rootes factory at [[Ryton plant|Ryton]] did not have the capacity to build the Tiger, so the company contracted the job to [[Jensen Motors|Jensen]] in West Bromwich.{{r|ClassicCarMart}} Any disappointment Shelby may have felt was tempered by an offer from Rootes to pay him an undisclosed royalty on every Tiger built.{{sfnp|Robson|2012|p=106|ps=none}}

Jensen was able to take on production of the Tiger because its assembly contract for the [[Volvo P1800]] had recently been cancelled. An additional factor in the decision was that Jensen's chief engineer Kevin Beattie and his assistant Mike Jones had previously worked for Rootes, and understood how the company operated.{{sfnp|Robson|2012|p=106|ps=none}} The first of 14 Jensen-built prototypes were based on the Series IV body shell, which became available at the end of 1963.{{sfnp|Robson|2012|p=107|ps=none}}

[[File:1960s left hand drive Sunbeam Tiger dash (cropped).jpg|thumb|left|alt=Photograph|The Tiger's interior is almost identical to the Alpine on which it is based.{{sfnp|Robson|2012|p=109|ps=none}}]]
The Tiger went into production in June 1964, little more than a year after the completion of the Shelby prototype.{{sfnp|Robson|2012|p=95|ps=}} Painted and trimmed bodies were supplied by [[Pressed Steel Company|Pressed Steel]] in Oxfordshire, and the engines and gearboxes directly from Ford in America.{{sfnp|Robson|2012|p=112|ps=none}} Installing the engine required some unusual manufacturing methods, including using a sledgehammer to bash in part of the already primed and painted bulkhead to allow the engine to be slid into place.{{sfnp|Clarke|2005|p=108|ps=none}} Jensen was soon able to assemble up to 300 Tigers a month,{{sfnp|Robson|2012|p=113|ps=none}} which were initially offered for sale only in North America.{{sfnp|Robson|2012|p=9|ps=none}} The first few Tigers assembled had to be fitted with a [[Borg-Warner]] 4-speed all-synchromesh manual gearbox, until Ford resolved its supply problems and was able to provide an equivalent unit as used in the [[Ford Mustang]].{{sfnp|Robson|2012|p=109|ps=none}}

Several performance modifications were available from dealers. The original 260 CID engine was considered only mildly tuned at {{convert|164|hp|abbr=on}}, and some dealers offered modified versions with up to {{convert|245|hp|abbr=on}} for an additional $250. These modifications were particularly noticeable to the driver above {{convert|60|mph|abbr=on}}, although they proved problematic for the standard suspension and tyres, which were perfectly tuned for the stock engine.{{sfnp|Clarke|2005|p=26|ps=none}} A 1965 report in the British magazine ''[[Motor Sport (magazine)|Motor Sport]]'' concluded that "No combination of an American V8 and a British chassis could be happier."{{sfnp|Carroll|2001|p=49|ps=none}}

==Versions==
[[File:Green Sunbeam Tiger 2 (reworked).jpg|thumb|alt=Dark-green open-top sports car|Apart from the bigger engine the changes to the Mark II Tiger were largely cosmetic: the most obvious are the speed stripes and the "egg crate" radiator grille.]]
Production reached 7128 cars over three distinct series. The factory only ever designated two, the Mark I and Mark II, but as the official Mark I production spanned the change in body style from the Series IV Alpine panels to the Series V panels, the later Mark I cars are generally designated Mark IA by Sunbeam Tiger enthusiasts.{{sfnp|Robson|2012|p=118|ps=}} The Mark II Tiger, fitted with the larger Ford {{convert|289|cuin|L|1|abbr=on}}, was intended exclusively for export to America and was never marketed in the UK,{{sfnp|Robson|2012|pp=125–7|ps=}} although six right-hand drive models were sold to the [[Metropolitan Police]] for use in traffic patrols and high-speed pursuits; four more went to the owners of important Rootes dealerships.{{sfnp|Hingston|2007|p=127|ps=none}}

All Tigers were fitted with a single Ford twin-choke [[carburettor]]. The [[compression ratio]] of the larger Mark II engine was increased from the 8.8:1 of the smaller block to 9.3:1.{{sfnp|Robson|2012|pp=110, 125|ps=none}} Other differences between the versions included upgraded valve springs (the 260 had developed a reputation for self-destructing if pushed beyond 5000&nbsp;rpm), an engine-oil cooler, an [[alternator (automotive)|alternator]] instead of a [[Electric generator#Vehicle-mounted generators|dynamo]], a larger single dry plate hydraulically operated clutch, wider ratio transmission, and some rear-axle modifications. There were also cosmetic changes: speed stripes instead of chrome strips down the side of the car, a modified radiator grille, and removal of the headlamp cowls.{{sfnp|Clarke|2005|p=125|ps=none}}{{sfnp|Langworth|Robson|1985|p=397|ps=none}}{{sfnp|Robson|2012|p=125|ps=none}} All Tigers were fitted with the same {{convert|4.5|in|abbr=on}} wide steel disc bolt-on wheels as the Alpine IV,{{sfnp|Robson|2012|pp=86, 110, 125|ps=none}} and Dunlop RS5 {{convert|4.90|x|13|in|abbr=on}} [[cross-ply tyre]]s.{{sfnp|Cheetham|2006|p=299|ps=none}} The lack of space in the Tiger's engine bay causes a few maintenance problems; the left bank of spark plugs is only accessible through a hole in the bulkhead for instance, normally sealed with a rubber bung, and the oil filter had to be relocated from the lower left on the block to a higher position on the right-hand side, behind the [[Alternator#Automotive alternators|generator]].{{sfnp|Clarke|2005|p=108|ps=none}}{{Clear}}

===Mark I===
{{Infobox automobile
| name         = Sunbeam Tiger Mark I
| image        = 1964 Sunbeam Tiger convertible (6105607813).jpg
| production   = 1964–67<br /> 6450 made{{sfnp|Robson|2012|p=110|ps=none}}
| engine         = {{convert|260|cuin|L|1|abbr=on}} [[Ford Windsor engine#260|Ford]] [[V8 engine|V8]]{{sfnp|Clarke|2005|p=8|ps=none}}
}}
The Ford V8 as fitted to the Tiger produced {{convert|164|bhp|abbr=on}} @ 4400&nbsp;rpm, sufficient to give the car a 0–{{convert|60|mph|abbr=on}} time of 8.6 seconds and a top speed of {{convert|120|mph|abbr=on}}.{{sfnp|Clarke|2005|p=8|ps=}}{{efn|The standard Series II Alpine had a top speed of 98.6 mph (158.7 km/h) and accelerated from 0 to 60&nbsp;mph (97&nbsp;km/h) in 13.6 seconds.{{r|Motor}}}}

The Girling-manufactured [[brakes]] used {{convert|9.85|in|mm|0|abbr=on}} discs at the front and {{convert|9|in|mm|0|abbr=on}} drums at the rear. The [[Suspension (vehicle)|suspension]] was independent at the front, using coil springs, and at the rear had a [[live axle]] and [[Leaf spring|semi-elliptic springs]]. Apart from the addition of a [[Panhard rod]] to better locate the rear axle, and stiffer front springs to cope with the weight of the V8 engine, the Tiger's suspension and braking systems are identical to that of the standard Alpine.{{sfnp|Clarke|2005|p=8|ps=none}} The fitting points for the Panhard rod interfered with the upright spare wheel in the boot, which was repositioned to lie horizontally beneath a false floor; the battery was moved from beneath the rear seat to the boot at the same time.{{sfnp|Robson|2012|p=108|ps=none}} The [[Curb weight|kerb weight]] of the car increased from the {{convert|2220|lbs|abbr=on}} of the standard Alpine to {{convert|2653|lbs|abbr=on}}.{{sfnp|Clarke|2005|p=8|ps=none}}

In 1964, its first year of production, all but 56 of the 1649 Mark I Tigers assembled were shipped to North America,{{sfnp|Robson|2012|p=118|ps=}} where it was priced at $3499.{{sfnp|Clarke|2005|p=8|ps=none}} In an effort to increase its marketability to American buyers the car was fitted with "Powered by Ford 260" badges on each front wing beneath the Tiger logo.{{sfnp|Robson|2012|p=117|ps=none}} The Mark I was unavailable in the UK until March 1965, when it was priced at £1446.{{sfnp|Robson|2012|p=120|ps=none}} It was also sold in South Africa for [[South African rand|R]]3350, badged as the Sunbeam Alpine 260.{{sfnp|Clarke|2005|p=69|ps=none}}

===Mark II===
{{Infobox automobile
| name = Sunbeam Tiger Mark II
| production = 1967<br /> 633 made{{sfnp|Gunnell|2004|p=178|ps=none}}{{sfnp|Robson|2012|p=633|ps=none}}{{efn|Other sources give estimates of the total number of Tiger IIs assembled as 536,{{r|ClassicOldCars}} 534,{{r|CatsWhiskers}} and 571.{{sfnp|Buckley|1999|p=235|ps=none}} Rootes allocated consecutive serial numbers to each car, and although it was well known that the last Tiger II to be produced was B-382100633{{r|CatsWhiskers}} the number of the first was for some time believed to be B-382100101, which subtracting one number from the other would give a production run of 533 inclusive. But the first production Mark II Tiger is now known to have the serial number B-382100001, suggesting a run of 633 cars.{{sfnp|Carroll|2001|p=56|ps=none}}}}
| engine = {{convert|289|cuin|L|1|abbr=on}} [[Ford Windsor engine#289|Ford]] [[V8 engine|V8]]{{sfnp|Clarke|2005|p=8|ps=none}}
}}
Priced at $3842, the Mark II Tiger was little more than a re-engined Mark IA; by comparison, a contemporary V8 [[Ford Mustang]] sold for $2898.{{r|ClassicCarMart}} The larger {{convert|289|cuin|L|1|abbr=on}} Ford engine improved the Tiger's 0–{{convert|60|mph|abbr=on}} time to 7.5 seconds, and increased the top speed to {{convert|122|mph|abbr=on}}.{{sfnp|Clarke|2005|p=98|ps=none}} Officially the Mark II Tiger was only available in the US, where it was called the Tiger II.{{sfnp|Robson|2012|p=9|ps=none}}{{sfnp|Langworth|Robson|1985|p=397|ps=none}} By the time the Mark II car went into production Chrysler was firmly in charge of Rootes, and the "Powered by Ford" shields were replaced by "Sunbeam V-8" badges.{{r|Road&Track}}{{Clear}}

==Demise==
[[File:1966 Sunbeam Tiger Roadster (22924220241) (cropped).jpg|thumb|Tiger with USA original equipment wheels and tyres]]
[[File:1966 Sunbeam Tiger Roadster (22887106856) (cropped).jpg|thumb]]
Rootes had always been insufficiently capitalised, and losses resulting from a damaging thirteen-week strike at one of its subsidiaries, British & Light Steel Pressings, coupled with the expense of launching the [[Hillman Imp]], meant that by 1964 the company was in serious financial difficulties. At the same time, Chrysler was looking to boost its presence in Europe, and so a deal was struck in June 1964 in which Chrysler paid £12.3 million ($34.44 million) for a large stake in Rootes, although not a controlling one.{{sfnp|Robson|2012|p=201|ps=none}}{{r|SpokaneChronical}} As part of the agreement Chrysler committed not to acquire a majority of Rootes voting shares without the approval of the UK government, which was keen not to see any further American ownership of the [[Automotive industry in the United Kingdom|UK motor industry]]. In 1967 Minister of Technology [[Anthony Wedgewood Benn]] approached [[British Motor Holdings|BMH]] and [[Leyland Motors|Leyland]] to see if they would buy out Chrysler and Rootes and keep the company British, but neither had the resources to do so.{{r|decline}} Later that year Chrysler was allowed to acquire a controlling interest in Rootes for a further investment of £20 million.{{sfnp|Robson|2012|pp=201–2|ps=none}}

Manufacturing a car powered by a competitor's engine was unacceptable to the new owner,{{sfnp|Robson|2012|p=128|ps=none}} but Chrysler's own [[Chrysler LA engine#273 V8|273 small-block V-8]] was too large to fit under the Tiger's [[Hood (vehicle)|bonnet]] without major modifications.{{sfnp|Clarke|2005|p=95|ps=none}} Compounding the problem, the company's small-block V8 engines had the [[distributor]] positioned at the rear, unlike the front-mounted distributor of the Ford V8. Chrysler's big-block V8 had a front-mounted distributor but was significantly larger.{{r|motortrend}} Shortly after the takeover Chrysler ordered that production of the Tiger was to end when Rootes' stock of Ford V8 engines was exhausted; Jensen assembled the last Tiger on 27 June 1967.{{sfnp|Robson|2012|p=126|ps=none}} Chrysler added its pentastar logo to the car's badging, and in its marketing literature de-emphasised the Ford connection, simply describing the Tiger as having "an American V-8 power train".{{sfnp|Carroll|2001|p=58|ps=none}}

Rootes' design director [[Roy Axe]] commented later that "The Alpine and Tiger were always oddballs in the [Rootes] range. I think they [Chrysler] didn't understand it, or have the same interest in it as the family cars&nbsp;– I think it was as simple as that."{{sfnp|Robson|2012|p=129|ps=none}}

The Tiger name was resurrected in 1972 when Chrysler introduced the [[Hillman Avenger#1972: Avenger Tiger|Avenger Tiger]], a limited-edition modified [[Hillman Avenger]] intended primarily for rallying.{{r|allparAvengerTiger}}

==Competition history==
{{Quote box |width=25em |quoted=true |bgcolor=#FFFFF0 |salign=center |quote=There is no doubt that the Tiger is somewhat misnamed, for it has nothing of the wild and dangerous man-eater about it and is really only as fierce as a pussy cat. A woman would find it easy to control.
|source=''[[Autocar]]'' roadtest, 1964{{sfnp|Robson|2012|p=122|ps=none}}}}
Three racing Tigers were constructed for the 1964 [[24 Hours of Le Mans]], a prototype and two that were entered in the race.{{r|SpeedChannel}} Costing $45,000 each,{{sfnp|Clarke|2005|pp=104–5|ps=none}} they were highly modified versions of the production cars, fitted with [[fastback]] [[coupe]] bodies produced by [[Lister Cars|Lister]].{{r|SpeedChannel}} But they were still steel [[monocoque]]s, and made the Le Mans Tigers {{convert|66|lb|abbr=on}} heavier than a road-going Tiger at {{convert|2615|lb|abbr=on}}, almost {{convert|600|lb|abbr=on}} more than the winning Ferrari.{{sfnp|Robson|2012|p=163|ps=none}} The standard Ford four-speed manual transmission was replaced with a [[BorgWarner]] T10 close-ratio racing transmission, which allowed for a top speed of {{convert|140|mph}}.{{efn|The Lister-bodied Tigers were timed during the race at {{convert|157.7|mph|abbr=on}},{{sfnp|Carroll|2001|p=41|ps=none}} but the fastest Fords and Ferraris were lapping more than thirty seconds quicker than the Tigers.{{sfnp|Robson|2012|p=163|ps=none}}}}

Both Tigers suffered early mechanical failures, and neither finished the race.{{efn|The first car suffered a piston failure after three hours and the second a broken crankshaft.{{sfnp|Robson|2012|p=164|ps=none}}}} The engines had been prepared by Shelby but had not been properly developed, and as a result overheated; Shelby eventually refunded the development cost to Rootes.{{sfnp|Robson|2012|pp=163–4|ps=none}} All three of the Le Mans Tigers have survived.{{r|SpeedChannel}}

Once Rootes had made the decision to put the Tiger into production an Alpine IV minus engine and transmission was shipped to Shelby, who was asked to transform the car into a racing Tiger. Shelby's competition Tiger made an early appearance in the B Production Class of Pacific Coast Division [[SCCA]] races, which resulted in some "highly successful" publicity for the new car.{{sfnp|Carroll|2001|p=51|ps=none}} But Shelby was becoming increasingly preoccupied with development work for Ford, and so the racing project was transferred to the Hollywood Sports Car dealership, whose driver Jim Adams achieved a third-place finish in the Pacific Coast Division in 1965. A Tiger driven by Peter Boulton and Jim Latta finished twelfth overall and first in the small GT class at the 1965 [[24 hours of Daytona|Dayton Continental]].{{sfnp|Carroll|2001|p=51|ps=none}} The Tiger was also raced on quarter-mile [[dragstrip|drag strip]]s, and for two years was the [[American Hot Rod Association]]'s national record holder in its class, reaching a speed of {{convert|108|mph|abbr=on}} in 12.95 seconds.{{sfnp|Carroll|2001|p=53|ps=none}}

Rootes entered the Tiger in European rallies, taking first, second and third places in the 1964 Geneva Rally.{{sfnp|Carroll|2001|p=51|ps=none}} Two Tigers took part in the 1965 [[Monte Carlo Rally]], one finishing fourth overall, the highest placing by a [[Front-engine, rear-wheel-drive layout|front-engined rear-wheel drive]] car, and the other eleventh.{{sfnp|Robson|2012|p=179|ps=none}} After finally having sorted out the engine overheating problem by fitting a forward-facing [[Hood scoop|air scoop]] to the bonnet, Rootes entered three Tigers in the 1965 [[Alpine Rally]], one of which crossed the finishing line as outright winner. Scrutineers later disqualified the car however, because it had been fitted with undersized cylinder head valves.{{sfnp|Robson|2012|pp=179, 182|ps=none}} By the end of the 1966 [[Acropolis Rally]] though, it had become clear that low-slung sports cars such as the Tiger were unsuited to the increasingly rough-terrain rally stages, and the car was withdrawn from competition soon after.{{efn|The Tiger had {{convert|5|in}} of ground clearance, a lot for a sports car but not for a rally car.{{sfnp|Clarke|2005|p=107|ps=none}}}} In the words of Ian Hall, who drove the Tiger in the Acropolis Rally, "I felt that the Tiger had just had it&nbsp;– it was an out of date leviathan".{{sfnp|Robson|2012|pp=186–7|ps=none}}

==In popular media==
[[File:Sunbeam Tiger 1966 view of rear.jpg|thumb|alt=Photograph|Rear view of a 1966 Sunbeam Tiger showing the twin exhausts]]
The 1965 Tiger Mark I gained some exposure on American television as the car of choice for [[Maxwell Smart]] in the spoof spy series ''[[Get Smart]]''.{{r|motortrendmovie}} The Tiger was used for the first two seasons in the [[opening credits]], in which Smart screeched to a halt outside his headquarters, and was used through the remainder of the series in several episodes.{{r|carguide}} Some of the scenes featured unusual modifications such as a retractable James Bond-style machine gun that could not have fitted under the Tiger's bonnet, so rebadged Alpine models were used instead.{{r|carguide}}

[[Don Adams]], who played the protagonist Maxwell Smart, gained possession of the Tiger after the series ended and later gave it to his daughters; it is reportedly on display at the [[Playboy Mansion]] in Los Angeles.{{r|carguide}} During its early years Rootes advertised the car extensively in ''[[Playboy]]'' magazine and lent a pink Tiger with matching interior to 1965 [[Playboy Playmate#Playmate of the Year|Playmate of the Year]] [[Jo Collins]] for a year.{{sfnp|Carroll|2001|p=52|ps=none}}

The Tiger also featured in the 2008 [[Get Smart (film)|film adaptation]] of the ''Get Smart'' TV series.{{r|SundayTimes}} A replica Tiger had to be constructed using a stock Sunbeam Alpine and re-created Tiger badging as no available Tiger could be found in Canada, where the film was produced. The production team recorded the sound of an authentic Tiger owned by a collector in Los Angeles{{r|sue}} and edited it into the film.{{r|motortrendmovie}}

==References==
'''Notes'''
{{notelist|notes=}}

'''Citations'''
{{reflist|25em|refs=

<ref name="allparAvengerTiger">
{{cite web |title=Hillman Avenger (Dodge Polara&nbsp;– 1500&nbsp;– 1800&nbsp;– Avenger) and Hillman Tiger |url=http://www.allpar.com/model/avengertiger.html |publisher=Allpar.com |accessdate=16 May 2013 |mode=cs2}}
</ref>

<ref name="carguide">
{{cite web |title=The ''Get Smart'' Cars
|url=http://www.carsguide.com.au/news-and-reviews/car-news/the_get_smart_cars <!-- http://www.carsguide.com.au/car-news/the-get-smart-cars-23523 -->
|archive-url=https://web.archive.org/web/20160311030655/http://www.carsguide.com.au/car-news/the-get-smart-cars-23523
|archive-date=11 March 2016
|accessdate=16 May 2013 |first=Chris |last=Riley |date=8 July 2008 |publisher=News Limited Community Newspapers |mode=cs2}}
</ref>

<ref name="CatsWhiskers">
{{cite web |last=Phelps |first=Peter |title=The HRO Mk II Tiger: Built, But Never For Sale! |magazine=Cats Whiskers |publisher=Sunbeam Tiger Owners Club |volume=74 |date=April 2007 |pages=16–17 |url=http://www.sunbeamtiger.co.uk/catswhiskers/CW74mk2.pdf
|archive-url=https://web.archive.org/web/20150111204038/http://www.sunbeamtiger.co.uk/catswhiskers/CW74mk2.pdf
|archive-date=11 January 2015
|format=PDF |accessdate=28 May 2013 |mode=cs2}}
</ref>

<ref name="ClassicCarMart">
{{citation |last=Moody |first=Keith |title=1966 Sunbeam Tiger |magazine=Classic Car Mart |date=September 2011
|url=http://www.classic-car-mart.co.uk/features/road-tests/162-tiger-sept.html
|archive-url=https://web.archive.org/web/20160313112628/http://classic-car-mart.co.uk/features/road-tests/162-tiger-sept.html
|archive-date=13 March 2016|accessdate=11 June 2013}}
</ref>

<ref name="ClassicOldCars">
{{cite web |title=Sunbeam Tiger Mk1, Mk2&nbsp;– Grab a tiger by its tail
|url=http://www.classicoldcars.info/classic-car-reviews/sunbeam-tiger-mk1-mk2-grab-a-tiger-by-its-tail/
|archive-url=https://web.archive.org/web/20120510094447/http://www.classicoldcars.info/classic-car-reviews/sunbeam-tiger-mk1-mk2-grab-a-tiger-by-its-tail/
|archive-date=10 May 2012|publisher=Classic Old Cars |accessdate=28 May 2013 |mode=cs2}}
</ref>

<ref name="decline">
{{citation |title=The Decline of the British Motor Industry: The Effects of Government Policy, 1945–79| last=Dunnett |first=Peter J. S. |format=ebook |url=https://books.google.com/books?id=hWb5J3r4TCMC |accessdate=5 April 2013| pages=1951–3 |series=Routledge Revivals}}
<!-- ebook, no isbn -->
<!-- also https://books.google.com.au/books?id=XfSzAAAAQBAJ -->
<!-- {{Citation | author1=Dunnett, Peter J. S | title=The decline of the British motor industry : the effects of government policy, 1945-1979 | publication-date=2011 | publisher=Routledge | isbn=978-1-136-64333-0 }} -->
</ref>

<ref name="Motor">
{{citation |title=The Sunbeam Alpine (Series II) |magazine=[[The Motor (magazine)|The Motor]] |volume= |pages= |date=28 December 1960}}
</ref>

<ref name="motortrend">
{{cite web |url=http://www.motortrend.com/news/12q2-1960-daimler-sp250-vs-1966-sunbeam-tiger/ <!-- http://www.motortrend.com/classic/features/12q2_1960_daimler_sp250_vs_1966_sunbeam_tiger/viewall.html -->
|accessdate=11 April 2013 |title=1960 Daimler SP250 vs 1966 Sunbeam Tiger&nbsp;– British Brawn |magazine=Motor Trend |first=Frank |last=Markus |date=February 2012 |mode=cs2}}
</ref>

<ref name="motortrendmovie">
{{citation |url=http://www.motortrend.com/features/auto_news/2008/112_0803_smarts_sunbeam/viewall.html
|archive-url=https://web.archive.org/web/20130222051748/http://www.motortrend.com/features/auto_news/2008/112_0803_smarts_sunbeam/viewall.html
|archive-date=22 February 2013|accessdate=14 April 2013 |title=Smart's Sunbeam: A behind-the-scenes look at the iconic car that stars in the new "Get Smart" movie |magazine=Motor Trend |date=March 2008}}
</ref>

<ref name="Road&Track">
{{citation |title=Sunbeam Tiger MK 2 |date=September 1967 |magazine=Road & Track |pages=82–83}}
</ref>

<ref name="SpeedChannel">
{{cite web |last=Melisson |first=Wouter |title=Sunbeam Tiger Le Mans Racers
|url=http://automotive.speedtv.com/article/vintage-sunbeam-tiger-le-mans-racer/
|archive-url=https://web.archive.org/web/20120125190956/http://automotive.speedtv.com/article/vintage-sunbeam-tiger-le-mans-racer/
|archive-date=25 January 2012|publisher=Fox Sports |accessdate=10 May 2013 |mode=cs2}}
</ref>

<ref name="SundayTimes">
{{citation |title=Big stunt. Get Smart crashes in&nbsp;- The big picture |newspaper=The Sunday Times |date=20 July 2008 |url=http://infoweb.newsbank.com/iw-search/we/InfoWeb?p_product=AWNB&p_theme=aggregated5&p_action=doc&p_docid=1220D386B00FB218&p_docnum=16&p_queryname=1 |subscription=yes}}
</ref>

<ref name="SpokaneChronical">
{{citation |title=British Firm Sale Planned by Chrysler |url=https://news.google.com/newspapers?nid=1338&dat=19650223&id=MFZYAAAAIBAJ&sjid=m_cDAAAAIBAJ&pg=7216,5567268 |accessdate=22 May 2013 |newspaper=Spokane Daily Chronicle|date=23 February 1965}}
</ref>

<!-- not used
<ref name="allpar">
{{citation |url=http://www.allpar.com/cars/adopted/sunbeam/tiger.html |contribution=Sunbeam Tiger: the car of Agent 86 |accessdate=14 April 2013 |publisher=Allpar.com}}
</ref>

<ref name="hemmings">
{{citation |url=http://www.hemmings.com/hmn/stories/2012/01/01/hmn_feature3.html |contribution=1967 Sunbeam Tiger Mk II |first=David |last=LaChance |accessdate=14 May 2013 |date=January 2012}}
</ref>
-->

<ref name="sue">
{{cite web |url=http://starcarcentral.com/getsmart.php5 |publisher=Star Car Central |title=Get Smart Tiger Sunbeam |accessdate=16 May 2013 |mode=cs2}}
</ref>
}}

'''Bibliography'''
{{refbegin}}
*{{citation |last=Buckley |first=Martin |title=Classic Cars A Celebration of the Motor Car From 1945 to 1975 |year=1999 |publisher=Lorenz Books |isbn=978-0-7548-0176-4}}
*{{citation |last=Carroll |first=William |title=Tiger, An Exceptional Motorcar |year=2001 |publisher=Auto Book Press |isbn=978-0-910390-26-2}}
*{{citation |last=Cheetham |first=Craig |title=Ultimate Performance Cars |year=2006 |publisher=MotorBooks International |isbn=978-0-7603-2571-1}}
*{{citation |last=Clarke |title=Sunbeam Tiger Limited Edition Extra, 1964–1967 |first=R. M. |year=2005 |publisher=Brookland Books |isbn=978-1-85520-685-4}}
*{{citation |last=Gunnell |first=John |title=Standard Guide to British Sports Cars |year=2004 |publisher=Krause Publications |isbn=978-0-87349-757-2}}
*{{citation |last=Hingston |first=Peter |title=The Enthusiast's Guide to Buying a Classic British Sports Car |year=2007 |publisher=Hingston Publishing Company |isbn=978-0-906555-25-5}}
*{{citation |last=Holthusen |first=Peter J. R. |title=The Land Speed Record: To the Sound Barrier and Beyond |year=1986 |publisher=G T Foulis & Co |isbn=978-0-85429-499-2}}
*{{citation |last1=Langworth |first1=Richard M. |last2=Robson |first2=Graham |title=Complete book of collectible cars, 1930–1980 |year=1985 |publisher=Beekman House |isbn=978-0-517-47934-6}}
*{{citation |last=Robson |first=Graham |title=Sunbeam Alpine and Tiger: The Complete Story |year=2012 |publisher=The Crowood Press |isbn=978-1-86126-636-1}}
*{{citation |last1=Shelby |first1=Carroll |last2=Bentley |first2=John |title=The Cobra Story |year=1965 |publisher=Trident Press |isbn=}}
{{refend}}

==External links==
{{commons category|Sunbeam Tiger}}
{{Portal|Cars}}
*{{cite web | url=http://www.jaylenosgarage.com/collections/pebble-beach/1965-sunbeam-tiger/ |website=Jay Leno's Garage |title=1965 Sunbeam Tiger |date=7 October 2011
|archiveurl=https://web.archive.org/web/20130710085037/http://www.jaylenosgarage.com/collections/pebble-beach/1965-sunbeam-tiger |archivedate=1 July 2013}}
*[https://www.youtube.com/watch?v=fyqo03zmzZw ''Jay Leno's Garage – 1965 Sunbeam Tiger''] (YouTube Video)
*[http://www.sunbeamtiger.co.uk/ The Sunbeam Tiger Owners Club]
*[http://www.stoa-tigerclub.com/ Sunbeam Tiger Owners Association]
*[http://www.tigersunited.com/ Tigers United]

[[Category:Rear-wheel-drive vehicles]]
[[Category:Sunbeam vehicles|Tiger]]
[[Category:Cars introduced in 1964]]
[[Category:1960s automobiles]]
[[Category:Roadsters]]
[[Category:Sports cars]]