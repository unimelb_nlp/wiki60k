<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
|name                             = Citation families
|image                            = File:Cessna Citation II.jpg
|caption                          = A [[Cessna Citation II]]
}}{{Infobox Aircraft Type
|type                             = [[Business jet]]
|manufacturer                     = [[Cessna]]
|first flight                     = 15 September 1969
|introduction                     = 1972
|number built                     = 7000
|variants with their own articles =[[Cessna Citation I|Citation I / I/SP]]<br>[[Cessna Citation II|Citation II/SII/Bravo]]<br>[[Cessna Citation III|Citation III-VI-VII]]<br>[[Cessna Citation V|Cessna Citation V/Ultra/Encore]]<br>[[Cessna Citation Excel|Citation Excel/XLS/XLS+]]<br>[[Cessna CitationJet|CitationJet/CJ series]]<br>[[Cessna Citation Mustang|Citation Mustang]]<br>[[Cessna Citation X|Citation X]]<br>[[Cessna Citation Sovereign|Citation Sovereign]]<br>[[Cessna Citation Latitude|Citation Latitude]]<br>[[Cessna Citation Longitude|Citation Longitude]]
}}
|}

The '''Cessna Citation''' is a market brand-name used by American manufacturer [[Cessna]] for its line of [[business jet]]s. Rather than one particular model of aircraft, the name applies to several "families" of [[turbofan]]-powered aircraft that have been produced over the years. Within each of the six distinct families, aircraft design improvements, market pressures and re-branding efforts have resulted in a number of variants, so that the Citation lineage has become quite complex. Military variants include the '''T-47''' and '''UC-35''' series aircraft.

The 7,000th Citation was delivered on June 27, 2016, forming the largest business jet fleet. The first Citation was put into service in 1972 and more than 35 million flight hours have been logged since.<ref>{{cite press release |url= http://txtav.com/en/newsroom/2016/06/cessna-delivers-milestone-7000th-citation-as-netjets-first-citation-latitude |title= Cessna delivers milestone 7,000th Citation as NetJets’ first Citation Latitude |date= June 27, 2016 |publisher= [[Textron Aviation]]}}</ref> 

==Citation product lineage overview==
<!--CAUTION! There are several forced lines in this listing, please do not edit them out.--><!--CAUTION! This section uses a LOT of named inline references, which will get severely screwed up if lines of text are moved around. This section is organized by lineage, not necessarily by production date. If you plan on editing, PLEASE discuss on the talk page first. Thanks. -->

*'''[[Cessna FanJet 500]]''', the prototype for the original Citation family, first flew September 15, 1969.<ref name="alnet500">[http://www.airliners.net/info/stats.main?id=157 The Cessna 500 & 501 Citation, Citation I & Citation I/SP at Airliners.net]</ref>
**'''[[Cessna Citation I|Citation I]]''' (Model 500) originally called the '''Citation 500''' before Cessna finally settled on Citation I, by which time the design had changed quite a bit from the FanJet 500. The original Citation I was one of the first light corporate jets to be powered by [[turbofan]] engines. Production ceased in 1985.<ref name="asn500">[http://aviation-safety.net/database/type/type-general.php?type=CE-500 Citation I info from Aviation Safety Network]</ref> [[File:Cessna citation 503CC.jpg|thumb|right|Oldest flying Citation I]]
**'''Citation I/SP''' (Model 501) single-pilot operations<ref name="asn501">[http://aviation-safety.net/database/type/type-general.php?type=CE-501 Citation I/SP info from Aviation Safety Network]</ref>
**'''[[Cessna Citation II|Citation II]]''' (Model 550) a larger stretched development of the Model 500 first produced in 1978. Initially replaced by the S/II in production, but was brought back and produced side-by-side with the S/II until the Bravo was introduced.<ref name="alnet550">[http://www.airliners.net/info/stats.main?id=160 The Cessna Citation II & Bravo from Airliners.net]</ref><ref name="asn550">[http://aviation-safety.net/database/type/type-general.php?type=CE-550 Citation II info from Aviation Safety Network]</ref>
***'''T-47''' (Model 552) is the military designation of the Citation II. The [[United States Navy|U.S. Navy]] procured 15 T-47A aircraft as radar system trainers, and the [[United States Department of Defense|DoD]] purchased five OT-47B models for drug interdiction reconnaissance.<ref name="gs-ot47b">[http://www.globalsecurity.org/military/systems/aircraft/ot-47b.htm OT-47B information from GlobalSecurity.org]</ref>
***'''Citation II/SP''' (Model 551) single-pilot operations<ref name="alnet550"/><ref name="asn551">[http://aviation-safety.net/database/type/type-general.php?type=CE-551 Citation II/SP info from Aviation Safety Network]</ref>
***'''Citation S/II''' (Model S550) incorporated a number of improvements, especially an improved wing. Concurrent production with the II until Citation V introduction in 1989.<ref name="alnet550"/><ref name="asns550">[http://aviation-safety.net/database/type/type-general.php?type=CE-S550 Citation S550 info from Aviation Safety Network]</ref>
***'''Citation Bravo''' (Model 550) updated II and S/II with new PW530A engines, landing gear and Primus 1000 avionics.<ref name="asn550bravo">[http://aviation-safety.net/database/type/type-general.php?type=CE-550B Citation Bravo info from Aviation Safety Network]</ref><ref name="aero-techbravo">[http://www.aerospace-technology.com/projects/cessna_bravo/ "Cessna Citation Bravo Light Business Jet Cessna Citation Bravo Light Business Jet, USA", ''Aerospace-Technology.com]{{Unreliable source?|reason=domain on WP:BLACKLIST|date=June 2016}}</ref>  The last Citation Bravo rolled off the production line in late 2006, ending a nearly 10 year production run of 337 aircraft.<ref name="ce-bravopress">Cessna Press Release [http://cessna.com/news/article.chtml?ID=uBQrpIyoB9YXpY2ThPrC3b6sq8ZFiDexBamZT6cjuJIaxwoT4m Recent Milestones for Cessna’s Citation Business Jet Programs] {{webarchive |url=https://web.archive.org/web/20080226115825/http://cessna.com/news/article.chtml?ID=uBQrpIyoB9YXpY2ThPrC3b6sq8ZFiDexBamZT6cjuJIaxwoT4m |date=February 26, 2008 }} July 17, 2006</ref>
**'''[[Cessna Citation V|Citation V]]''' (Model 560), growth variant of the Citation II/SP JT15D-5A<ref name="alnet560">[http://www.airliners.net/info/stats.main?id=162 The Cessna 560 Citation V, Ultra & Encore from Airliners.net]</ref><ref name="asn560">[http://aviation-safety.net/database/type/type-general.php?type=CE-560 Citation V, Ultra and Encore info from Aviation Safety Network]</ref>
***'''Citation Ultra''' (Model 560) upgraded Citation V with JT15D-5D, [[EFIS]] instruments<ref name="asn560"/> [[File:Marines-uc35-6766-070403-04cr-16.jpg|thumb|right|USMC UC-35D at [[Mojave Spaceport|Mojave]]]]
****'''UC-35A''' Army transport version of the V Ultra.
****'''UC-35C''' Marine Corps version of the V Ultra.<ref name="uc35c1">[http://somd.com/news/headlines/2006/3818.shtml "NAVAIR Oversees Final Marine Corps Cessna Citation Encore Delivery" May 24, 2006]</ref>
***'''Citation Encore''' (Model 560) upgraded Citation Ultra with PW535A engines, a heated wing leading edge, and improved trailing-link landing gear<ref name="asn560"/>
****'''UC-35B''' Army transport version of the Encore.
****'''UC-35D''' Marine Corps version of the Encore.<ref name="uc35c1"/>
****'''Citation Encore+''' (Model 560) upgraded Encore includes [[FADEC]] and a redesigned avionics.<ref name="asn560"/>

<!--CAUTION! There are several forced lines in this listing, please do not edit them out.-->
*'''[[Cessna Citation III|Citation III]]''' (Model 650) all-new design.<ref name="alnet650">[http://www.airliners.net/info/stats.main?id=163 The Cessna Citation III, VI & VII from Airliners.net]</ref><ref name="asn650">[http://aviation-safety.net/database/type/type-general.php?type=CE-650 Citation III and VI info from Aviation Safety Network]</ref><ref name="aero-tech3">[http://www.aerospace-technology.com/projects/Cessna "Cessna Citation CJ3 Business Jet Cessna Citation CJ3 Business Jet, USA", ''Aerospace-Technology.com]{{Unreliable source?|reason=domain on WP:BLACKLIST|date=June 2016}}</ref>
**'''Citation IV''' was a proposed upgrade of the III, but was cancelled by Cessna.<ref name="alnet650"/>
**'''Citation VI''' (Model 650) was a low-cost derivative of the III which had a different avionics suite and non-custom interior design.<ref name="alnet650"/><ref name="asn650"/>
**'''Citation VII''' (Model 650) was an upgrade of the III that was in production from 1992 to 2000.<ref name="alnet650"/><ref name="asn6507">[http://aviation-safety.net/database/type/type-general.php?type=CE-6507 Citation VII info from Aviation Safety Network]</ref>
<!--CAUTION! There are several forced lines in this listing, please do not edit them out.-->
*'''[[Cessna Citation X|Citation X]]''' (Model 750) (''X'' as in the Roman numeral for ''ten''), an all-new design, the fastest civilian aircraft in the world since the retirement of [[Concorde]].<ref name="alnetx">[http://www.airliners.net/info/stats.main?id=166 The Cessna Citation X from Airliners.net]</ref>  {{convert|24|ft|m}} of stand-up cabin space.<ref name="ce-citationx">[http://citationx.cessna.com/home.chtml Cessna Citation X web site] {{webarchive |url=https://web.archive.org/web/20070125062923/http://citationx.cessna.com/home.chtml |date=January 25, 2007 }}</ref>
**'''Citation X+''', originally called the '''Ten'''

<!--CAUTION! There are several forced lines in this listing, please do not edit them out.-->
[[File:cessna.560xl.citation.excel.arp.jpg|thumb|right|Cessna 560XL Citation Excel of the [[Swiss Air Force]]]]
*'''[[Cessna Citation Excel|Citation Excel]]''' (Model 560XL), utilized a shortened Citation X fuselage combined with the V Ultra's straight wing and the V's tail; used new PW545A engines.<ref name="alnet560xl">[http://www.airliners.net/info/stats.main?id=161 The Cessna 560XL Citation Excel from Airlines.net]</ref><ref name="asn560xl">[http://aviation-safety.net/database/type/type-general.php?type=CE-560E Citation Excel info from Aviation Safety Network]</ref>  Includes a stand-up cabin.
**'''Citation XLS''', evolved from the Excel
**'''Citation XLS+''' which includes [[FADEC]] (Full Authority Digital Engine Control) and a redesigned avionics system.<ref name="ce-xlsplus">[http://xlsplus.cessna.com/home.chtml Cessna XLS+ web site] {{webarchive |url=https://web.archive.org/web/20070127121329/http://xlsplus.cessna.com/home.chtml |date=January 27, 2007 }}</ref>
*'''[[Cessna Citation Sovereign|Citation Sovereign]]''' (Model 680), utilizes a stretched version of the Excel's fuselage with an all-new moderately swept wing.<ref name="alnet680">[http://www.airliners.net/info/stats.main?id=164 The Cessna 680 Citation Sovereign from Airliners.net]</ref><ref name="asn680">[http://aviation-safety.net/database/type/type-general.php?type=CE-680 Citation 680 Sovereign info from Aviation Safety Network]</ref>  Stand-up cabin is {{convert|24|ft|m}} long.<ref name="ce-sovereign">[http://sovereign.cessna.com/home.chtml Cessna Sovereign web site] {{webarchive |url=https://web.archive.org/web/20070126181527/http://sovereign.cessna.com/home.chtml |date=January 26, 2007 }}</ref>

[[File:Cessna 525B Citation CJ3 Opera Jet OM-OPA, LUX Luxembourg (Findel), Luxembourg PP1350972612.jpg|thumb|right|[[Cessna CitationJet|Cessna 525B CJ3]] behind a [[Citation Mustang]]]]

<!--CAUTION! There are several forced lines in this listing, please do not edit them out.-->
*'''[[Cessna CitationJet|CitationJet]]''' (Model 525) essentially an all-new design, the only carry-over being the Citation I's forward fuselage.<ref name="alnetcj">[http://www.airliners.net/info/stats.main?id=159 The Cessna CitationJet, CJ1 & CJ2 from Airliners.net]</ref>  The 525 series models all feature a shorter cabin; Not a stand-up.
**'''CJ1''' (Model 525) Improved version of the CitationJet<ref name="alnetcj"/>
***'''CJ1+''' (Model 525) Improved version of the CJ1 with new engines, avionics, and [[FADEC]]<ref name="ce-cj1plus">[http://cj1plus.cessna.com/home.chtml Cessna Citation CJ1+ web site] {{webarchive |url=https://web.archive.org/web/20080329082756/http://cj1plus.cessna.com/home.chtml |date=March 29, 2008 }}</ref><ref name="jobwerx-cj1plus">[http://www.jobwerx.com/news/Archives/textron_biz-id=947285_674.html "New Cessna Citation CJ1 Receives FAA Type Certification", ''Jobwerx News''] {{webarchive |url=https://web.archive.org/web/20061104073313/http://www.jobwerx.com/news/Archives/textron_biz-id=947285_674.html |date=November 4, 2006 }}</ref>
***'''[[Cessna Citation M2|Citation M2]]'''
**'''CJ2''' (Model 525A) Stretched version of the CJ1.<ref name="alnetcj"/>
***'''CJ2+''' (Model 525A) Improved version of the CJ2 with increased performance, improved avionics, and [[FADEC]].<ref name="ce-cj2plus">[http://cj2plus.cessna.com/home.chtml Cessna CJ2+ web site] {{webarchive |url=https://web.archive.org/web/20080404010223/http://cj2plus.cessna.com/home.chtml |date=April 4, 2008 }}</ref>
**'''CJ3''' (Model 525B) Extension of the CJ2.<ref name="ce-cj3">[http://cj3.cessna.com/home.chtml Cessna Citation CJ3 web site] {{webarchive |url=https://web.archive.org/web/20070407103231/http://cj3.cessna.com/home.chtml |date=April 7, 2007 }}</ref>
**'''CJ4''' (Model 525C) An extension of the CJ3, with new [[Williams FJ44|Williams FJ44-4]] engines and the moderately swept wing borrowed from the Sovereign.<ref name="ce-cj4">[http://cj4.cessna.com/home.chtml Cessna Citation CJ4 web site] {{webarchive |url=https://web.archive.org/web/20070126210302/http://cj4.cessna.com/home.chtml |date=January 26, 2007 }}</ref>  The first flight of the CJ4 is slated for the first half of 2008 with customer deliveries to follow in 2010.<ref>Cessna Press Release [http://cessna.com/news/article.chtml?ID=lvVAoZNMwJZVVLe8FIii05Yjj64qW3TxPpYcLLkZov42uFGAIq Cessna Launches Citation CJ4 at NBAA; Starts Show with 70 Orders] {{webarchive |url=https://web.archive.org/web/20061017081519/http://cessna.com/news/article.chtml?ID=lvVAoZNMwJZVVLe8FIii05Yjj64qW3TxPpYcLLkZov42uFGAIq |date=October 17, 2006 }} Cessna In the News, October 16, 2006</ref>
**'''[[Cessna 526 CitationJet|Model 526]]''' A twin-seat tandem military trainer developed by Cessna from the CitationJet for the [[Joint Primary Aircraft Training System|JPATS]] competition.<ref>{{cite book |last=Taylor |first= Michael J. H.|authorlink= |coauthors= |title= Brassey's World Aircraft & Systems Directory|year=1996 |publisher= Brassey's |location=London |isbn=1-85753-198-1|page=128}}</ref>

<!--CAUTION! There are several forced lines in this listing, please do not edit them out.-->
*'''[[Cessna Citation Mustang|Citation Mustang]]''' (Model 510), a new [[Very Light Jet]] (VLJ), even smaller and lighter than the CitationJet I.<ref name="ce-mustang">[http://mustang.cessna.com/home.chtml Cessna Citation Mustang web site] {{webarchive |url=https://web.archive.org/web/20070126193217/http://mustang.cessna.com/home.chtml |date=January 26, 2007 }}</ref>
*'''[[Cessna Citation Columbus|Citation Columbus]]''' (Model 850), a future intercontinental large cabin corporate jet. (Canceled)<ref name="ce-columbus">[http://columbus.cessna.com/ Cessna Citation Columbus web site]</ref>
*'''[[Cessna Citation Latitude|Citation Latitude]]''' (Model 680A) - The project was announced at the annual NBAA convention in October, 2011. It was launched as a larger aircraft than the Cessna Citation XLS+ and cheaper than the Cessna Citation Sovereign. The aircraft will seat 9, and feature twin Pratt & Whitney Canada PW306D turbofan engines. Like other Citations, the Citation Latitude will feature a cruciform tail and all metal fuselage.<ref>{{cite web|url=http://www.flightglobal.com/news/articles/cessna-gets-attitude-with-latitude-362864/ |title=Cessna gets attitude with Latitude |publisher=Flightglobal.com |date=2011-10-10 |accessdate=2012-10-19}}</ref>
*'''[[Cessna Citation Longitude|Citation Longitude]]''' (Model 700) - The project was announced in May 2012. It was perceived as the follow-on development to the now-canceled ''Citation Columbus''. Its fuselage cross-section (83.25 inch circular section) is from the ''Citation Latitude''. Cessna projected that first delivery would occur in late 2017. The aircraft will have a T-tail empennage, area-rule fuselage contouring, and 28.6° wing sweep. Powered by two Honeywell HTF7700L turbofan engines, rated at 7,550 lb (33.58 kN) thrust. The wings will incorporate moderate winglets. Construction will be aluminum for both wing and fuselage.<ref>[http://www.aviationweek.com/Article.aspx?id=/article-xml/awx_05_14_2012_p0-457353.xml Aviation Week & Space Technology, 14 May 2012 edition, ''Cessna Unveils Citation Longitude'']</ref>

{{chart/start}}
{{chart| | | | | | | 500 |7| | | | | |500=[[Cessna Citation I|500 Citation I]], 69-85}}
{{chart| | | | | | | |!| |:| | | | | |}}
{{chart| | | | | | | 550 |:| | | | | |550=[[Cessna Citation II|550 Citation II/Bravo]], 78–06}}
{{chart| 650 | | | | |!| |:| | | | | |650=[[Cessna Citation III|650 Citation III/VI/VII]], 83-00}}
{{chart| |:| | | |F| 560 |:| | | | | |560=[[Cessna Citation V|560 Citation V/Ultra/Encore]], 89-11}}
{{chart| |:| | | |:| | | |:| | | | | |}}
{{chart| |:| | | |:| | | 525 | | | | |525=[[Cessna CitationJet|525 CitationJet/CJ series]], 91-}}
{{chart| |:| | | |:| | | |!| | | | | |}}
{{chart| 750 |~| 56X | | |!| | | | | |750=[[Cessna Citation X|750 Citation X]], 96-|56X=[[Cessna Citation Excel|560XL Excel/XLS/XLS+]], 96-}}
{{chart| | | | | |:| | | |!| | | | | |}}
{{chart| | | | | 680 | | |!| | | | | |680=[[Cessna Citation Sovereign|680 Sovereign]], 04-}}
{{chart| | | | | |!| | | |!| | | 510 |510=[[Cessna Citation Mustang|510 Mustang]], 06-}}
{{chart| | | | | |!| | | 52M | | | | |52M=[[Cessna Citation M2|525 Citation M2]], 13-}}
{{chart| | | | | 68A | | | | | | | | |68A=[[Cessna Citation Latitude|680A Latitude]], 15-}}
{{chart| | | | | |:| | | | | | | | | |}}
{{chart| | | | | 800 | | | | | | | | |800=[[Cessna Citation Longitude|700 Longitude]], 17-}}
{{chart| | | | | |:| | | | | | | | | |}}
{{chart| | | | | hem | | | | | | | | |hem=[[Cessna Citation Hemisphere|Hemisphere]], 19-}}
{{chart/end}}

== Current Models ==
{| class="wikitable sortable"
|-
! Model
! Length !! Span !! Area 
! Sweep !! Inside !! Pax.
! MTOW !! Cruise !! Range
! Engines !! Thrust
|-
| [[Cessna Citation Mustang|510 Mustang]]<ref>{{cite web |url= http://cessna.txtav.com/en/citation/mustang |title= Citation Mustang}}</ref> 
| {{convert|40|ft|7|in|m|abbr=on}} || {{convert|43|ft|2|in|m||abbr=on}} || {{convert|210|sqft|sqm|abbr=on}}
| 11.0 ° || {{convert|55|in|m||abbr=on}} || 5
| {{convert|8,645|lb|kg|abbr=on}} || {{convert|340|knot|km/h|abbr=on}} || {{convert|1,200|nmi|km|abbr=on}}
| 2 [[Pratt & Whitney Canada PW600|PW615F-A]] || {{convert|{{#expr:2*1460}}|lbf|kN|abbr=on}}
|-
| [[Cessna Citation M2|525 Citation M2]]<ref>{{cite web |url= http://cessna.txtav.com/en/citation/m2 |title= Citation M2}}</ref>
| {{convert|42|ft|7|in|m|abbr=on}} || {{convert|47|ft|3|in|m||abbr=on}} || {{convert|240|sqft|sqm|abbr=on}}
| 0 ° || {{convert|58|in|m||abbr=on}} || 7
| {{convert|10,800|lb|kg|abbr=on}} || {{convert|404|knot|km/h|abbr=on}} || {{convert|1,540|nmi|km|abbr=on}}
| 2 [[Williams FJ44|FJ44-1AP-21]] || {{convert|{{#expr:2*1965}}|lbf|kN|abbr=on}}
|-
| [[Cessna CitationJet|525 Citation CJ3+]]<ref>{{cite web |url= http://cessna.txtav.com/en/citation/cj3 |title= Citation CJ3+}}</ref>
| {{convert|51|ft|2|in|m|abbr=on}} || {{convert|53|ft|4|in|m||abbr=on}} || {{convert|294|sqft|sqm|abbr=on}}
| 0 ° || {{convert|58|in|m||abbr=on}} || 9
| {{convert|13,870|lb|kg|abbr=on}} || {{convert|416|knot|km/h|abbr=on}} || {{convert|2,040|nmi|km|abbr=on}}
| 2 [[Williams FJ44|FJ44-3A]] || {{convert|{{#expr:2*2820}}|lbf|kN|abbr=on}}
|-
| [[Cessna CitationJet|525 Citation CJ4]]<ref>{{cite web |url= http://cessna.txtav.com/en/citation/cj4 |title= Citation CJ4}}</ref>
| {{convert|53|ft|4|in|m|abbr=on}} || {{convert|50|ft|10|in|m||abbr=on}} || {{convert|330|sqft|sqm|abbr=on}}
| 12.5 ° || {{convert|58|in|m||abbr=on}} || 10
| {{convert|17,110|lb|kg|abbr=on}} || {{convert|451|knot|km/h|abbr=on}} || {{convert|2,170|nmi|km|abbr=on}}
| 2 [[Williams FJ44|FJ44-4A]] || {{convert|{{#expr:2*3621}}|lbf|kN|abbr=on}}
|-
| [[Cessna Citation Excel|560XL Citation XLS+]]<ref>{{cite web |url= http://cessna.txtav.com/en/citation/xls |title= Citation XLS+}}</ref>
| {{convert|52|ft|6|in|m|abbr=on}} || {{convert|56|ft|4|in|m||abbr=on}} || {{convert|370|sqft|sqm|abbr=on}}
| 0 ° || {{convert|68|in|m||abbr=on}} || 9
| {{convert|20,200|lb|kg|abbr=on}} || {{convert|441|knot|km/h|abbr=on}} || {{convert|2,100|nmi|km|abbr=on}}
| 2 [[Pratt & Whitney Canada PW500|PW545C]] || {{convert|{{#expr:2*4119}}|lbf|kN|abbr=on}}
|-
| [[Cessna Citation Latitude|680A Citation Latitude]]<ref>{{cite web |url= http://cessna.txtav.com/en/citation/latitude |title= Citation Latitude}}</ref>
| {{convert|62|ft|3|in|m|abbr=on}} || {{convert|72|ft|4|in|m|abbr=on}} || {{convert|543|sqft|sqm|abbr=on}}
| 16.3 ° || {{convert|77|in|m||abbr=on}} || 9
| {{convert|30,800|lb|kg|abbr=on}} || {{convert|446|knot|km/h|abbr=on}} || {{convert|2,850|nmi|km|abbr=on}}
| 2 [[Pratt & Whitney Canada PW300|PW306D1]] || {{convert|{{#expr:2*5907}}|lbf|kN|abbr=on}}
|-
| [[Cessna Citation Sovereign|680 Citation Sovereign]]<ref>{{cite web |url= http://cessna.txtav.com/en/citation/sovereign |title= Citation Sovereign}}</ref>
| {{convert|63|ft|6|in|m|abbr=on}} || {{convert|72|ft|4|in|m|abbr=on}} || {{convert|543|sqft|sqm|abbr=on}}
| 16.3 ° || {{convert|68|in|m||abbr=on}} || 12
| {{convert|30,775|lb|kg|abbr=on}} || {{convert|460|knot|km/h|abbr=on}} || {{convert|3,190|nmi|km|abbr=on}}
| 2 [[Pratt & Whitney Canada PW300|PW306D]] || {{convert|{{#expr:2*5907}}|lbf|kN|abbr=on}}
|-
| [[Cessna Citation X|750 Citation X+]]<ref>{{cite web |url= http://cessna.txtav.com/en/citation/citation-x |title= Citation X+}}</ref>
| {{convert|73|ft|7|in|m|abbr=on}} || {{convert|69|ft|2|in|m|abbr=on}} || {{convert|527|sqft|sqm|abbr=on}}
| 37.0 ° || {{convert|68|in|m||abbr=on}} || 12
| {{convert|36,600|lb|kg|abbr=on}} || {{convert|528|knot|km/h|abbr=on}} || {{convert|3,460|nmi|km|abbr=on}}
| 2 [[Rolls-Royce AE3007|AE3007C2]] || {{convert|{{#expr:2*7034}}|lbf|kN|abbr=on}}
|}

==See also==
{{Aircontent
|related=
*[[Cessna Citation I]] / I/SP
*[[Cessna Citation II]]/SII/Bravo
*[[Cessna Citation III]]/VI/VII
*[[Cessna Citation V]]/Ultra/Encore
*[[Cessna Citation X]]
*[[Cessna Citation Excel]]/XLS/XLS+
*[[Cessna CitationJet]]/CJ series
*[[Cessna Citation Sovereign]]
*[[Cessna Citation Mustang]]
*[[Cessna Citation Columbus]]
*[[Cessna Citation Longitude]]

|similar aircraft=
*[[Beechjet 400]]
*[[Bombardier Challenger]]
*[[Dassault Falcon]]
*[[Hawker 800]]
*[[Learjet 31]]
*[[Learjet 35/36]]
*[[Learjet 45]]

|lists=

|see also=
}}

==References==
{{Reflist|30em}}

==External links==
{{Commons category}}
*[http://citation.cessna.com/ Cessna Citation home page]

{{Cessna}}

[[Category:Cessna aircraft|*]]
[[Category:United States civil aircraft]]
[[Category:United States business aircraft]]
[[Category:Twinjets]]
[[Category:Low-wing aircraft]]
[[Category:Cessna Citation family| ]]