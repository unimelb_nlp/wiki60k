{{use dmy dates|date=April 2016}}
'''Daur Zantaria''' ({{lang-ab|Даур Занҭариа}}) (25 May 1953 – 7 August 2001) was a writer and journalist from [[Abkhazia]], publishing both in [[Abkhaz language|Abkhaz]] and [[Russian language|Russian]] languages.

==Early life and education==
Zantaria was born on 25 May 1953 in the village of [[Tamysh]], [[Ochamchira District]]. He graduated with a gold medal from Tamysh high school in 1971 and with honours from the philological faculty of the [[Abkhazian State University|Sukhum State Pedagogical Institute]] in 1975.<ref name=mkra685>{{cite web|title=Зантария Даур Бадзович – прозаик, поэт, публицист|url=http://mkra.org/vidnye-deyateli/?ELEMENT_ID=685|website=Ministry for Culture of Abkhazia|accessdate=21 November 2015}}</ref>

==Literary career==
Zantaria published his first short story ''Kuasta'' in the magazine ''[[Alashara]]'' in 1976. In the following years, his short stories and poems appeared in ''Alashara'', the children's magazine ''[[Amtsabz]]'', the newspaper ''[[Apsny Kapsh]]'' and the almanac ''[[Literary Abkhazia]]''. In 1984, Zantaria entered the [[Union of Soviet Writers]]. That same year, he participated in a workshop for script writers in [[Moscow]] by [[Valentin Yezhov]] and wrote the script for the film ''Souvenir'', released in 1985.<ref name=mkra685/>

On 19 January 1991, Zantaria presided over the founding congress of Abkhazia's first post-Soviet political party [[Democratic Party of Abkhazia]]. He had conceived of the idea of founding the party along with historian [[Guram Gumba]] and Russian poet [[Aleksandr Bardodym]] during a visit by the latter in 1990. The party was not revived following the [[war in Abkhazia (1992–1993)|1992–1993 war with Georgia]].<ref name=ekho27336432>{{cite news|last1=Sharia|first1=Vitali|title="Демократическая Абхазия": вчера, сегодня, завтра|url=http://www.ekhokavkaza.com/content/article/27336432.html|accessdate=14 November 2015|agency=[[Echo of the Caucasus]]|date=30 October 2015}}</ref>

In the 1990s, Zantaria became a member of the [[Union of Abkhazian Writers]] and published stories, essays and articles for ''[[Novy Mir]]'', ''[[Druzhba Narodov]]'', ''[[Znamya]]'', ''[[Expert (magazine)|Expert]]'', ''[[Obshchey Gazeta]]'', as well as a number of novels and collections of short stories and poems.<ref name=mkra685/>

==Death==
Zantaria died on 7 August 2001. He lies buried in Tamysh.<ref name=mkra685/>

==References==
{{reflist}}

==External links==
* {{IMDb name|nm0953116}}

{{DEFAULTSORT:Zantaria, Daur}}
[[Category:1953 births]]
[[Category:2001 deaths]]
[[Category:20th-century poets]]
[[Category:21st-century poets]]
[[Category:Abkhaz poets]]
[[Category:Male poets]]
[[Category:Male screenwriters]]
[[Category:Male short story writers]]
[[Category:People from Ochamchira District]]
[[Category:Russian-language writers]]
[[Category:20th-century short story writers]]
[[Category:21st-century short story writers]]