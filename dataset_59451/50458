{{Infobox journal
| cover         =[[Image:AJP July2006.jpg|100px]] 
| editor        =David P. Jackson
| discipline    =Physics 
| peer-reviewed = 
| former_names  = 
| abbreviation  =Am. J. Phys.
| publisher     =American Association of Physics Teachers with American Institute of Physics 
| country       =United States of America 
| frequency     =Monthly 
| history       =1933–present 
| openaccess    = 
| license       = 
| impact        = 0.956
| impact-year   = 2014
| website       =http://ajp.aapt.org/ 
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          =1480178 
| LCCN          =2007233687
| CODEN         =AJPIAS
| ISSN          =0002-9505 
| eISSN         =1943-2909
| boxwidth      = 
}}
The '''''American Journal of Physics''''' is a monthly, [[peer-review]]ed [[scientific journal]] published by the [[American Association of Physics Teachers]] and the [[American Institute of Physics]]. The [[Editor in Chief|editor]] is David P. Jackson of [[Dickinson College]].<ref name=LCCN>"Current Frequency: Monthly, 2002; and Former Frequency varies, 1940-2001" 
 {{Cite web
  | title =Library catalog 
  | publisher =Library of Congress
  | date = Aug 22, 2007
  | url =http://scitation.aip.org/journals/doc/AJPIAS-home/AJPmasthead.pdf 
  | format =Online 
  | accessdate =2011-01-24}}</ref><ref name=masthead>
Confirmation of Editor, ISSN, CODEN,  and other relevant information. 
{{Cite web
  | title =Masthead 
  | publisher =American Association of Physics Teachers): 
  | year =2011
  | url =http://scitation.aip.org/journals/doc/AJPIAS-home/AJPmasthead.pdf 
  | accessdate =2011-01-24}}</ref><ref name=about/><ref name=welsley/>

==Aims and scope==
The focus of this journal is undergraduate and graduate level physics. The intended audience is college and university physics teachers and students. Coverage includes current research in physics, related topics, instructional laboratory equipment, laboratory demonstrations, teaching methodologies, lists of resources, book reviews. In addition, historical, philosophical and cultural aspects of physics are also covered.<ref name=about/>

==History==
The former title of this journal was ''American Physics Teacher'' (vol. 1, February 1933) ({{ISSN|0096-0322}}). It was a quarterly journal from 1933 to 1936, and then a bimonthly from 1937 to 1939. After volume 7 was published in December 1939,  the name of the journal was changed to its current title in February 1940. Hence, the publication begins under its new title with volume 8 in February 1940.<ref name=about>
{{Cite web
  | title =About this journal 
  | publisher =American Association of Physics Teachers 
  | year =2010 
  | url =http://ajp.aapt.org/about/about_the_journal 
  | accessdate =2011-01-24}}Brief description of this journal.</ref><ref name=welsley/><ref name=former>
Information pertaining to the former title "The American Physics Teacher".
{{Cite web
  | title =Library catalog 
  | publisher =National Library of Australia 
  | date = 
  | url =http://catalogue.nla.gov.au/Record/1605343?lookfor=title:(American%20Journal%20of%20Physics)&offset=2&max=156448 
  | format = 
  | accessdate =2011-01-24}}</ref>

==Abstracting and indexing==
This journal is indexed in the following databases:<ref name=welsley>
{{Cite web
  | title =Library catalog 
  | publisher =Wellesley College, Massachusetts  
  | date = 
  | url =http://www.worldcat.org/wcpa/oclc/1480178?page=frame&url=http%3A%2F%2Fluna.wellesley.edu%2Fsearch%2Fo1480178%26checksum%3D43313b8df1b5c44f4c3938d12603b13c&title=Wellesley+College&linktype=opac&detail=WEL%3AWellesley+College%3AAcademic+Library 
  | format =accessed via World Cat 
  | accessdate =2011-01-24}}Bibliographic information for this journal. Abstracting and indexing services are listed here. "v.8 (1940:Feb.)-v.36 (1968),v.59 (1991)"</ref>
*[[Mathematical Reviews]]
*[[Abstract Bulletin of the Institute of Paper Chemistry]] (PAPERCHEM in 1969)<ref>{{Cite journal
  | last =Bechtel 
  | first =Rosanna M. 
  | title =A brief history of the Abstract Bulletin of the Institute of Paper Science and Technology 
  | journal =Publishing Research Quarterly 
  | volume =11 
  | issue =03 
  | pages =145–151
  | publisher =Springerlink 
  | doi =10.1007/BF02680458 
  }}<!--| accessdate =2011-01-24--></ref>

*[[International Aerospace Abstracts]]
*[[SPIN bibliographic database|SPIN]]
*[[Chemical Abstracts]]
*[[Current Physics Index]]
*[[Current Index to Journals in Education]] (CSA Illumina - ERIC database)<ref>[http://www.csa.com/factsheets/eric-set-c.php ERIC] of CSA Illumina. "A print equivalent is no longer available. Historically,ERIC records were included in the print publications Resources in Education (1966-2002); and Current Index to Journals in Education (1969-2002)." 2009.</ref>
*[[Energy Research Abstracts]]
*[[Applied Science & Technology Index]] (H.W. Wilson Company)
*[[General Science Index]] ( H.W. Wilson Company)
*[[Computer & Control Abstracts]]
*[[Electrical & Electronics Abstracts]]
*[[Physics Abstracts. Science Abstracts. Series A]]

==See also==
*''[[European Journal of Physics]]''

==References==
{{Reflist}}

==External links==
* [http://scitation.aip.org/ajp/ ''American Journal of Physics'']
* [http://ajp.dickinson.edu ''American Journal of Physics'' editor's website]

[[Category:English-language journals]]
[[Category:Publications established in 1933]]
[[Category:Monthly journals]]
[[Category:American Institute of Physics academic journals]]
[[Category:Academic journals associated with learned and professional societies of the United States]]
[[Category:Physics education journals]]