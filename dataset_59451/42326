<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=14-13 Cruisair Senior
 |image=Bellanca 14-13-2 C-FGGX 02.JPG
 |caption=Bellanca 14-13-2
}}{{Infobox Aircraft Type
 |type=Civil utility aircraft
 |manufacturer=[[Bellanca]]
 |designer=
 |first flight= November 13, [[1945 in aviation|1945]]
 |introduced=
 |retired=
 |status=
 |primary user=
 |more users=
 |produced=
 |number built=Around 600
 |developed from= [[Bellanca 14-7]]
 |variants with their own articles= [[Bellanca 17-30]]
}}
|}
[[File:Bellanca 14-13-2 C-FGGX 03.JPG|thumb|right|Bellanca 14-13-2]]

The '''Bellanca 14-13 Cruisair Senior''' and its successors were a family of light aircraft that were manufactured in the [[United States]] by [[AviaBellanca Aircraft]] after [[World War II]].  They were a follow-up to the prewar [[Bellanca 14-7]] and its derivatives.

==Design and development==

While retaining the Bellanca 14-7's basic design, the 14-13 featured an enlarged cabin, a [[flat-4|horizontally opposed]] [[Franklin 6A4|Franklin 6A4-335-B3]] {{convert|150|hp|kW|0|abbr=on}} [[Piston engine|engine]] in place of the earlier models' Le Blond [[Radial engine|radial]] and an oval vertical endplate on each [[Tailplane|horizontal stabiliser]].  This latter feature gained the type the affectionate nickname "cardboard Constellation", because the arrangement was similar to the contemporary [[Lockheed Constellation]] airliner.<ref name="Plane and Pilot">[[#ref1978AircraftDirectory1977|''1978 Aircraft Directory'' 1977]], p. 20.</ref>

Taking its name from the Bellanca tradition of identifying the series from the wing area in square feet, dropping the final digit, while the second number was the aircraft's horsepower, again dropping the final digit, the 14-13 did not quite fit the naming convention. The Bellanca 14-13 wing was constructed of wood, while the [[fuselage]] was welded steel-tube framework with a fabric covering.<ref name="Plane and Pilot" />

The 14-13 was introduced in 1946; in its improved 14-13-3 version the aircraft remained in production until 1956.<ref name="Plane and Pilot" />

===Model 14-19===
A higher-performance design revision was granted FAA approval as the '''14-19 Cruisemaster''' on September 26, 1949.<ref name="Department of Transportation Federal Aviation Administration Aircraft Specification 1A3">"Specification 1A3." ''Department of Transportation Federal Aviation Administration Aircraft''.</ref> The new model featured structural upgrades, a {{convert|190|hp|kW|0|abbr=on}} Lycoming O435-A engine, an increased gross weight of {{convert|2600|lb|kg|0|abbr=on}}, hydraulically operated landing gear and flaps, and a deluxe interior. 99 of these airplanes were produced between 1949 and 1951. Externally, a near-look-alike to the earlier models, this version was distinguished by its larger, oval-shaped endplates.<ref name="Bellanca Champion Club Literature">Bellanca Champion Club Literature.</ref> All production ceased in 1956 as Bellanca wound up its operations.

===Model 14-19-2===
[[File:bellanca 14-19-2 cruisemaster n7600e built 1958 arp.jpg|thumb|Bellanca 14-19-2 Cruisemaster, built 1958]]
The 14-19 design was revived by [[AviaBellanca Aircraft|Northern Aircraft]] and granted FAA approval on January 7, 1957 as the '''14-19-2 Cruisemaster'''. The new model featured a {{convert|230|hp|kW|0|abbr=on}} [[Continental O-470|Continental O-470K]] engine, an increased gross weight of 2,700 pounds,<ref name="Department of Transportation Federal Aviation Administration Aircraft Specification 1A3"/> an updated instrument panel as well as new  paint and upholstery schemes.<ref name="1957 Bellanca 14-19-2 Owners Manual">1957 Bellanca 14-19-2 Owners Manual.</ref> A total of 104 of these aircraft were produced between 1957 and 1958.<ref name="Bellanca Champion Club Literature"/>

The company was renamed [[AviaBellanca Aircraft|Downer Aircraft]] in 1959. [[AviaBellanca Aircraft|Inter-Air]] acquired the production rights in 1962 and was renamed as the [[AviaBellanca Aircraft|Bellanca Sales Company]], a subsidiary of [[Miller Flying Service]].<ref name="Plane and Pilot"/> Further development of the design by Inter-Air resulted in the modernized [[Bellanca 17-30|Viking]] series introduced in 1962.<ref name="Palmer p. 51">Palmer 2001, p. 51.</ref>
[[File:Bellanca 14-19-3.jpg|thumb|Bellanca 14-19-3 Landing]]
[[File:BellancaPanel.jpg|thumb|An updated 14-19 instrument panel]]
[[File:Bellanca Cruissair.jpg|thumb|1946 model Bellanca 14-13 Cruisair Senior at the [[Western Canada Aviation Museum]]- note the large endplates of the initial 14-13]]

==Operational history==
Designed and produced in the post-World War II  era, the Bellanca 14-13 Cruisair Senior was aimed at a general aviation market. Pilot/owners were offered a combination of performance, low engine power and a modest price. Its performance and structural strength also made it attractive for utility work, but in many ways the Bellanca design was an anachronism, relying on a [[conventional landing gear]] configuration and wood-and-fabric construction that harkened back to an earlier age. Postwar economics along with a glut of surplus military aircraft precluded heavy sales although about 600 were produced.<ref name="Palmer p. 51"/>

Despite its introduction into a period where private aircraft sales were stagnant, the aircraft remained popular through all of its incarnations and today is considered a classic cabin monoplane and is much in demand.<ref>[http://www.pilotfriend.com/aircraft%20performance/Bellanca/13.htm "Bellanca 14-13 Cruisair history."] ''Pilotfriend.com'', 2009. Retrieved: May 17, 2009.</ref>

==Variants==
;14-13
:Initial model introduced in 1946<ref name="Plane and Pilot" />
;14-13-2
:Improved model introduced in 1947. Featured a longer-span stabilizer with smaller endplates and an external baggage door.<ref name="Plane and Pilot" />
;14-13-3
:Improved model introduced in 1948, which remained in production until 1956<ref name="Plane and Pilot" />
;14-13W
:"Wagon" version with [[plywood]]-lined cabin and removable rear seats
;14-19
:{{convert|190|hp|kW|0|abbr=on}} version introduced in 1949<ref name="Department of Transportation Federal Aviation Administration Aircraft Specification 1A3"/>
;14-19-2
:{{convert|230|hp|kW|0|abbr=on}} version introduced in 1957<ref name="Department of Transportation Federal Aviation Administration Aircraft Specification 1A3"/>
;14-19-3 
:{{convert|260|hp|kW|0|abbr=on}} [[tricycle gear]] version introduced in 1959 by Downer Aircraft<ref name="Plane and Pilot" />
;Downer 260B Model 14-19-3A 
:Last version built by Downer Aircraft priced at $19,500 in 1962 - Powered by a 260 hp (194 kW) [[Teledyne Continental Motors|Continental IO-470]]-F engine<ref>''Flying Magazine,'' November 1962, p. 24.</ref>
;Downer 260C Model 14-19-3C
:Version built by [[AviaBellanca Aircraft|Inter-Air]] with revised empennage and 260 hp (194 kW) Continental IO-470-F engine

==Specifications (14-13-2)==
{{aerospecs
|met or eng?=<!-- eng for US/UK aircraft, met for all others -->eng
|crew=one pilot
|capacity=three passengers
|length m=6.5
|length ft=21
|length in=4
|span m=10.42
|span ft=34
|span in=2
|dia m=<!-- helicopters -->
|dia ft=<!-- helicopters -->
|dia in=<!-- helicopters -->
|height m=1.91
|height ft=6
|height in=3
|wing area sqm=
|wing area sqft=
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|aspect ratio=<!-- sailplanes -->
|empty weight kg=
|empty weight lb=
|gross weight kg=975
|gross weight lb=2,150
|eng1 number=1
|eng1 type=[[Franklin 6A4|Franklin 6A4-150-B3]]
|eng1 kw=<!-- prop engines -->112
|eng1 hp=<!-- prop engines -->150
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|max speed kmh=266
|max speed mph=165
|max speed mach=<!-- for supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|range km=970
|range miles=600
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=4,900
|ceiling ft=16,000
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=<!-- related developments -->
* [[Bellanca 14-7]]
* [[Bellanca 17-30]]
|similar aircraft=<!-- similar or comparable aircraft -->
|sequence=<!-- designation sequence, if appropriate -->
|lists=<!-- related lists -->
|see also=<!-- other relevant information -->
* [[Giuseppe Mario Bellanca]]
}}

==References==

===Notes===
{{Reflist|30em}}

===Bibliography===
{{Refbegin}}
* Davisson, Budd. "We Fly the Cardboard Constellation." ''Air Progress Vintage Buyer's Guide'', 1989.
* Mondey, David. ''The Complete Illustrated Encyclopedia of the World's Aircraft''. Secaucus, New Jersey: Chartwell Books Inc, 1978. ISBN 0-89009-771-2.
* Palmer, Trisha, ed. "Bellanca Viking Series". ''Encyclopedia of the World's Commercial and Private Aircraft''. New York: Crescent Books, 2001. ISBN 0-517-36285-6.
* {{anchor|ref1978AircraftDirectory1977}}"Plane and Pilot." ''1978 Aircraft Directory''. Santa Monica, California: Werner & Werner Corp, 1977. ISBN 0-918312-00-0.
* Taylor, Michael J. H. ''Jane's Encyclopedia of Aviation''. London: Studio Editions, 1989, p.&nbsp;150. 
* ''World Aircraft Information Files''. London: Bright Star Publishing, File 890, Sheet 24.
{{Refend}}

==External links==
{{commons category|Bellanca 14-13}}
* [http://www.aerofiles.com/_bella.html aerofiles.com]
* [https://web.archive.org/web/20070610183306/http://www.nasm.si.edu/research/aero/aircraft/bellanc2.htm National Air and Space Museum website]
* [http://www.pimaair.org/Acftdatapics/Bellanca%2014-13-2.htm Arizona Aerospace Foundation website]

{{Bellanca}}

[[Category:United States civil utility aircraft 1940–1949]]
[[Category:Bellanca aircraft|14-13]]
[[Category:Single-engine aircraft]]