{{Use dmy dates|date=February 2016}}
{{More footnotes|date=February 2016}}
'''Maurice Noguès''' (31 October 1889 – 15 January 1934) was a [[France|French]] aviator from [[Brittany (administrative region)|Brittany]].

==Biography==
Noguès was born in [[Rennes]], to Marthe Vallée and Émile Noguès; his father was a Colonel in the [[artillery]]. He taught himself to fly in 1909, and served in various French air squadrons during World War I, receiving command of [[Escadrille 73]] in March 1918. During the war he received both the [[Croix de Guerre]], multiple citations, and the [[Legion of Honour]] (the highest decoration in France). Toward the end of the war, 29 July 1918, Noguès married Magdalene Gicquel.

Dissatisfied with city life, in 1922 he joined the [[CFRNA|Franco-Romanian Air Transport Company]] (CFRNA, later CIDNA), flying primarily the Paris–Strasbourg route, but including flights as far east as Moscow. In 1924 he received the Medal of Encouragement to Progress (''la médaille d'Encouragement au Progrès'') and the vermeil medal from the [[Aéro-Club de France]] for establishing the Bucarest–Constantinople–Ankara air route. In 1926 he joined the Transair Courier Company (''Compagnie des Messageries Transaériennes'') (later part of [[Air Orient]]) initiating flights to Syria and then Lebanon. He was the chief pilot for Air Orient and in 1931 he extended the Syrian route to [[Saigon]] in then [[French Indochina]].

When Air Orient merged to become [[Air France]] in 1933, Noguès became the executive vice president in charge of expanding the airline's routes. In December 1933 he took a prototype [[Dewoitine D.332]], named ''Emeraude'' on a test-of-concept flight to Saigon. On the last leg of the return flight in January he encountered a snowstorm over central France and crashed into a hill near [[Corbigny]].  Noguès and all nine passengers were killed, including [[Pierre Marie Antoine Pasquier|Pierre Pasquier]], the Governor-General of French Indochina,<ref>{{Cite web|author=Alonso, Anne |year=2006 |title=Cérémonies publiques, funérailles nationales et obsèques aux frais de l’État (1899-1943) |location=Paris |page=15 |url=http://143.126.211.224/chan/chan/pdf/sm/F21%204713-4714.pdf}}</ref> and Emmanuel Chaumé, the French Director General of Civil Aviation. The probable cause of the crash was excessive icing.<ref>{{Cite web|publisher=Aviation Safety Network |title=Accident Description |url=http://aviation-safety.net/database/record.php?id=19340115-0 |archiveurl=https://web.archive.org/web/20130528201057/http://aviation-safety.net/database/record.php?id=19340115-0 |archivedate=28 May 2013 |deadurl=no}}</ref> At the time he died, Noguès was actively working on both extending the Saigon–Hanoi route to Hong Kong and Canton, as well as the feasibility of a South Atlantic route to Brazil.

== Legacy ==
In 1938, Syria issued a postage stamp commemorating Noguès and the ten years of operations on the Paris-Damascus air route.<ref>Scott #C88 - Scott (2008) "Syria: Air Post Stamps" ''Scott 2009 Standard Postage Stamp Catalogue Volume 6'' (165th edition) [[Scott catalogue|Scott Publishing Co.]], Sidney, Ohio, pages&nbsp;392–393. ISBN 978-0-89487-422-2</ref> That same year Lebanon issued a postage stamp picturing him and commemorating the tenth anniversary of the first Marseille-Beirut flight, which was made by Noguès.<ref>Scott #C79 - Scott (2008) "Lebanon: Air Post Stamps" ''Scott 2009 Standard Postage Stamp Catalogue Volume 4'' (165th edition) [[Scott catalogue|Scott Publishing Co.]], Sidney, Ohio, page&nbsp;434. ISBN 978-0-89487-420-8</ref>

In 1947 Air France named the Paris–Saigon air route after him (Ligne Noguès). A street in [[Voisins-le-Bretonneux]] is named after him.

In 1951, France issued a postage stamp commemorating Noguès and his vision of airplane routes around the world.<ref>Scott #665 - Scott (2008) "France" ''Scott 2009 Standard Postage Stamp Catalogue Volume 2'' (165th edition) [[Scott catalogue|Scott Publishing Co.]], Sidney, Ohio, page&nbsp;1178. ISBN 978-0-89487-418-5</ref>

== Notes and references ==
{{Reflist}}

== Bibliography ==
* {{Cite book|author=Bejui, Dominique |year=1993 |title=Aviateurs d'Empire: L'épopée de l'aviation commerciale dans la France d'Outre-Mer |language=fr |location=Chanac, France |publisher=La Régordane |isbn=978-2-906984-15-8}}
* {{Cite book|author=Boca, Robert |year=1949 |title=Trois générations, deux guerres: Les Vallée, leurs enfants, leurs alliés |language=fr |location=[[Châtelaudren]], France |publisher=Impr. de Châtelaudren |oclc=493613295}}
* {{Cite encyclopedia|year=2004|title=Noguès Maurice, Émile|encyclopedia=Mémorial aéronautique: Qui était qui?, Volume 2 |language=fr|author=Catillon, Marcel |location=Paris |publisher=Nouvelles Editions Latines |isbn=978-2-7233-2053-5}}
* {{Cite book|author1=Collot, Gérard  |author2=Cornu, Alain  |lastauthoramp=yes |year=1992 |title=Ligne Noguès: Histoire aérophilatélique: Air Orient Air France, 1911-1941 |language=fr |location=Paris |publisher=B. Sinais}}
* {{Cite book|author=Kahn, Michèle |year=2007 |title=La tragédie de l'Émeraude: enquête |language=fr|location=Monoco|publisher=Éditions du Rocher |isbn=978-2-268-06316-4}}
{{Clear}}

{{DEFAULTSORT:Nogues, Maurice}}
[[Category:1889 births]]
[[Category:1934 deaths]]
[[Category:French aviators]]
[[Category:Victims of aviation accidents or incidents in France]]