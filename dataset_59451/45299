{{Infobox military person
|name= Sir William Coles
|image= 
|caption= 
|nickname= 
|birth_date= {{birth date|1913|07|26|df=yes}}  
|death_date= {{death date and age|1979|06|07|1913|07|26|df=yes}}  
|birth_place= 
|death_place= 
|allegiance= {{flag|United Kingdom}}
|serviceyears= 1938–1968
|rank= [[Air Marshal]]
|branch= {{air force|United Kingdom}}
|commands= [[RAF Technical Training Command|Technical Training Command]] (1966–68)<br/>[[No. 23 Group RAF|No. 23 (Training) Group]] (1960–63)<br/>[[RAF Middleton St. George]] (1950–51)<br/>[[No. 233 Squadron RAF|No. 233 Squadron]] (1944–45)<br/>[[No. 117 Squadron RAF|No. 117 Squadron]] (1943–44)
|battles= [[Second World War]]
|awards= [[Knight Commander of the Order of the British Empire]]<br/>[[Companion of the Order of the Bath]]<br/>[[Distinguished Service Order]]<br/>[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] & [[Medal bar|Bar]]<br/>[[Air Force Cross (United Kingdom)|Air Force Cross]]<br/>[[Distinguished Flying Cross (United States)|Distinguished Flying Cross]] (United States)
|laterwork= 
}}
[[Air Marshal]] '''Sir William Edward Coles''' {{postnominals|country=GBR|size=100%|sep=,|KBE|CB|DSO|DFC}} & [[Medal bar|Bar]], [[Air Force Cross (United Kingdom)|AFC]] (26 July 1913 – 7 June 1979) was a [[Royal Air Force]] officer who served as Air Officer Commanding-in-Chief [[RAF Technical Training Command|Technical Training Command]] from 1966 to 1968.

==RAF career==
Coles joined the Royal Air Force in 1938.<ref name=air>[http://www.rafweb.org/Biographies/Coles.htm Air of Authority – A History of RAF Organisation – Air Marshal Sir William Coles]</ref> He served in the [[Second World War]] as [[Officer Commanding]] [[No. 117 Squadron RAF|No. 117 Squadron]] and as Officer Commanding [[No. 233 Squadron RAF|No. 233 Squadron]].<ref name=air/> After the war he became a Staff Officer at the [[Air Ministry]] before joining the [[Central Flying School]].<ref name=air/> He was appointed Station Commander at [[RAF Middleton St. George]] in 1950, Chief Flying Instructor at the Central Flying School in 1951 and Air Adviser to the [[List of High Commissioners of the United Kingdom to Australia|High Commissioner to Australia]] in 1953.<ref name=air/> He went on to be Senior Air Staff Officer at Headquarters [[No. 3 Group RAF|No. 3 (Bomber) Group]] in 1957, Air Officer Commanding [[No. 23 Group RAF|No. 23 (Training) Group]] in 1960 and Director-General of RAF Personal Services in 1963.<ref name=air/> His last appointment was as Air Officer Commanding-in-Chief [[RAF Technical Training Command|Technical Training Command]] in 1966 before retiring in 1968.<ref name=air/>

In retirement he became President of the [[Not Forgotten Association]]<ref>[http://www.nfassociation.org/wp-content/uploads/2009/10/NFA-Annual-Review-2010.pdf Not Forgotten Association] Annual Review 2010</ref> and Controller of the [[RAF Benevolent Fund]].<ref>[http://www.flightglobal.com/pdfarchive/view/1969/1969%20-%203100.html World News] Flight International, 30 October 1969</ref>

==References==
{{reflist}}

{{s-start}}
{{s-mil}}
{{s-bef|before=[[Donald Randall Evans|Sir Donald Evans]]}} 
{{s-ttl|title=Air Officer Commanding-in-Chief [[RAF Technical Training Command|Technical Training Command]]|years=1966–1968}}
{{s-non|reason=Post disbanded}}
{{end}}

{{DEFAULTSORT:Coles, William}}
[[Category:1913 births]]
[[Category:1979 deaths]]
[[Category:Companions of the Distinguished Service Order]]
[[Category:Companions of the Order of the Bath]]
[[Category:Knights Commander of the Order of the British Empire]]
[[Category:Recipients of the Air Force Cross (United Kingdom)]]
[[Category:Recipients of the Distinguished Flying Cross and Bar (United Kingdom)]]
[[Category:Recipients of the Distinguished Flying Cross (United States)]]
[[Category:Royal Air Force air marshals]]
[[Category:Royal Air Force personnel of World War II]]