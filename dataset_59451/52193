{{Infobox journal
| title = Living Reviews in Relativity
| cover =
| editor = Bala R. Iyer
| discipline = [[Physics]], [[astrophysics]]
| abbreviation = Living Rev. Relat.
| publisher = [[Springer Science+Business Media]]
| frequency =
| history = 1998-present
| openaccess = Yes
| license = [[Creative Commons]]
| impact = 19.250
| impact-year = 2014
| website = http://relativity.livingreviews.org/
| link1 =
| link1-name =
| JSTOR =
| OCLC = 39510385
| LCCN =
| CODEN =
| ISSN = 1433-8351
| eISSN =
}}
'''''Living Reviews in Relativity''''' is a [[Peer review|peer-reviewed]] [[open-access]] [[scientific journal]] publishing [[Literature review|reviews]] on [[Theory of relativity|relativity]] in the areas of [[physics]] and [[astrophysics]]. It was founded by [[Bernard Schutz]] and published at the [[Max Planck Institute for Gravitational Physics]] from 1998-2015. After it was sold by [[Max Planck Society]] in June 2015, it is now published by the academic publisher [[Springer Science+Business Media]].

The articles in ''[[Living Reviews]]'' provide critical reviews of the current state of research in the fields they cover. Articles also offer annotated insights into the key literature and describe other available resources. ''[[Living Reviews]]'' is unique in maintaining a suite of high-quality reviews, which are kept up-to-date by the authors through regularly adding the latest developments and research findings. This is the meaning of the word "living" in the journal's title.<ref>{{cite news | url = http://relativity.livingreviews.org/About/concept.html | title =Living Reviews in Relativity: Journal Concept }}</ref>

== See also ==
* [[List of scientific journals in astronomy]]

== References ==
<references/>

== External links ==
* {{Official website|http://relativity.livingreviews.org/}}

{{Max Planck Society}}

[[Category:Astrophysics journals]]
[[Category:Open access journals]]
[[Category:Physics review journals]]
[[Category:Publications established in 1998]]
[[Category:English-language journals]]
[[Category:Max Planck Society]]
[[Category:Physics websites]]
[[Category:Springer Science+Business Media academic journals]]