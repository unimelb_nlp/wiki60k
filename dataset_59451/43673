{{Use dmy dates|date=December 2016}}
{{Use British English|date=December 2016}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Snark
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Fighter aircraft]]
 | national origin=[[United Kingdom]]
 | manufacturer=[[Sopwith Aviation Company]]
 | designer=
 | first flight=1919
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=3
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Sopwith Snark''' was a [[United Kingdom|British]] prototype [[fighter aircraft]] designed and built towards the end of the [[First World War]] to replace the [[RAF]]'s [[Sopwith Snipe]]s. A single engined [[triplane]], the Snark did not fly until after the end of the war, only three being built.

==Development and design==

In spring 1918, although the [[Sopwith Snipe]] had not yet entered service with the [[Royal Air Force]], the British [[Air Ministry]] drew up a specification (RAF Type I) for its replacement.  The specification asked for a fighter capable of operations at high altitude and powered by the [[ABC Dragonfly]] engine, which was an air-cooled [[radial engine]]  which had been ordered in large numbers based on promises of high performance and ease of production.

Sopwith produced two designs to meet this requirement, a [[biplane]], the [[Sopwith Snapper|Snapper]], and a [[triplane]], the Snark.  Sopwith received orders for three prototypes each of the Snapper and Snark,<ref name="Bruce v3 p52,56">Bruce 1969, pp. 52, 56.</ref> as well as orders for 300 of a Dragonfly powered version of the Snipe, the [[Sopwith Dragon]].  The Snark had a wooden [[monocoque]] fuselage like that of the [[Sopwith Snail]] lightweight fighter, and had equal span [[Interplane strut#Bays|single-bay]] wings with [[aileron]]s on each wing.  The wings had unequal spacing and [[Stagger (aviation)|stagger]], with the gap between the mid and upper wings less than that between the lower and mid wings to minimise the height of the aircraft.<ref name="Bruce v3 p51-2">Bruce 1969, pp. 51—52.</ref>

The Snark was fitted with what was, for the time, a very heavy armament for a single-seat fighter.  In addition to the normal two [[synchronization gear|synchronised]] [[Vickers gun]]s inside the fuselage, it had four [[Lewis gun]]s mounted under the lower wings, firing outside the propeller disc.  These guns were inaccessible to the pilot, and so could not be reloaded or unjammed in flight.<ref name="Bruce British p632">Bruce 1957, p.632.</ref>{{ref label|Note1|a|a}}

The first prototype was complete by October 1918, but flight-ready engines were not available until March 1919, and the Snark did not make its first flight until July 1919.<ref name="mason fighter p145"/> While it demonstrated reasonable performance and good maneuverability,<ref name="Bruce British p632"/> (although not as good as the earlier [[Sopwith Triplane]]<ref name="Bruce v3 p55"/>), by this time, it had been realised that the Dragonfly engine had serious problems, being prone to overheating and severe vibration, and plans for production of the Snark had been abandoned.<ref name="Bruce v3 p53">Bruce 1969, p.53.</ref>  The three Snarks continued in use for trials purposes until 1921.<ref name="Robertson pp229">Robertson 1970, p.229.</ref>

==Specifications==

{{aircraft specifications
|plane or copter?=plane<!-- options: plane/copter -->
|jet or prop?=prop<!-- options: jet/prop/both/neither -->
|ref=War Planes of the First World War: Fighters Volume Three <ref name="Bruce v3 p55">Bruce 1969, p.55.</ref><!-- the source(s) for the information -->
|crew=1
|capacity= <!-- the number of passengers carried in the case of a commercial aircraft-->
|payload main=
|payload alt=
|payload more=
|length main= 20 ft 6 in
|length alt=6.25 m
|span main=26 ft 6 in
|span alt=8.08 m
|height main=10 ft 10 in
|height alt=3.30 m
|area main= 322 sq ft
|area alt= 29.9 m²
|airfoil=
|empty weight main= 
|empty weight alt= 
|loaded weight main= 2,283 lb
|loaded weight alt= 1,038 kg
|useful load main= 
|useful load alt= 
|max takeoff weight main= 
|max takeoff weight alt= 
|max takeoff weight more=
|more general=
|engine (prop)=[[ABC Dragonfly]] IA
|type of prop= 9-cylinder air-cooled [[radial engine]] 
|number of props=1<!--  ditto number of engines-->
|power main= 360 hp <ref name="mason fighter p145">Mason 1992, p.145.</ref>
|power alt=269 kW
|power original=
|power more=
|max speed main= 130 mph
|max speed alt=113 knots, 209 km/h
|max speed more= at 3,000 ft (915 m)
|cruise speed main= 
|cruise speed alt=
|cruise speed more 
|stall speed main= 
|stall speed alt= 
|stall speed more=
|never exceed speed main= 
|never exceed speed alt= 
|range main= 
|range alt=
|ferry range main=
|ferry range alt=
|ferry range more=
|ceiling main= 
|ceiling alt= 
|climb rate main= 
|climb rate alt= 
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=
|power/mass alt=
|more performance=
|guns= <br />
**2× synchronised, forward firing [[.303 British|.303 in]] (7.7 mm) [[Vickers gun]]s
**4× .303 in [[Lewis gun]]s under wings
|bombs= 
|rockets= 
|missiles= 
|hardpoints= 
|hardpoint capacity=  
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=*[[Armstrong Whitworth Ara]]
*[[BAT Basilisk]]
*[[Nieuport Nighthawk]]
*[[Armstrong Whitworth Siskin|Siddeley Siskin]]
*[[Sopwith Dragon]]
*[[Sopwith Snapper]]
|lists=<!-- related lists -->
}}

==References==

===Notes===
* {{note label|Note1|a|a}} A similar installation of two Lewis guns on the lower wings of [[Sopwith Dolphin]] fighters was tested by [[No. 87 Squadron RAF]].<ref name="Robertson p105">Robertson 1970, p.105.</ref>
{{reflist}}

===Bibliography===
{{commons category|Sopwith Aviation Company}}
{{refbegin}}
*Bruce, J.M. ''British Aeroplanes 1914-18''. London:Putnam, 1957.
*Bruce, J.M. ''War Planes of the First World War: Fighters Volume Three''. London:Macdonald, 1969. ISBN 0-356-01490-8.
*Mason, Francis K. ''The British Fighter since 1912''. Annapolis, USA:Naval Institute Press, 1992. ISBN 1-55750-082-7.
*Robertson, Bruce. ''Sopwith-The Man and his Aircraft''. Letchworth, UK:Air Review, 1970. ISBN 0-900435-15-1.
{{refend}}
<!-- ==External links== -->
{{Sopwith Aviation Company aircraft}}

[[Category:British fighter aircraft 1910–1919]]
[[Category:Triplanes]]
[[Category:Sopwith aircraft|Snark]]