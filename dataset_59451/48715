{{multiple issues|
{{Orphan|date=September 2013}}
{{advert|date=April 2012}}
}}

{{Infobox company
| name             = Infopro Sdn. Bhd.
| type             = Private Limited
| logo             = 
| foundation       = 1987
| founder          = Chuah Wan Pin<br />Matthew Chuah Wan Hui
| add              = Block B3 Level 8 Leisure Commerce Square, 9 Jalan PJS 8/9, 46150 Petaling Jaya, Malaysia.
| location_city    = [[Petaling Jaya]], [[Selangor]]
| location_country = [[Malaysia]]
| key_people       = Chuah Wan Pin {{small|([[CEO]])}}<br /> Matthew Chuah {{small|([[Chief operating officer|COO]])}}
| num_employees    = 157 {{small|(as at April 2010}})
| industry         = [[Computer software]]
| products         = eICBA
| area_served      = Worldwide / [[Financial Sector]]
| subsid           = Infopro Pte Ltd, Singapore<br/>Infopro Systems International, Philippines<br/>Infopro International FZCO, UAE
| divisions        = Executive Office<br/>Organisational Process Quality Management<br/>Sales & Marketing <br/>Global Operations<br/>Corporate Office<br/>Finance
| homepage         = {{URL|http://www.infopro.com.my/}}
}}
'''Infopro Sdn Bhd''' ('Infopro') is an [[ISO 9001:2000]] and a [[CMMi]] Level 5 certified<ref>{{ cite web |title= MDeC honours 47 companies for their certifications |url=http://commtechasia.net/tech/features-mainmenu-38/360-mdec-honours-47-companies-for-their-certifications.html |date=2009-01-15}}</ref>  Malaysian based software house that specializes in [[core banking]] [[solution]] that provides front and back end banking [[solution]]s known as eICBA system. The eICBA system consists of core banking system known as ICBA (Integrated Computerised Banking System) and systems for Business Intelligence, ATM, e-Switch, Internet Banking and Telebanking. The core banking system also handles Islamic banking<ref>{{ cite web |title= IBS Islamic Banking |url=http://www.nzibo.com/IB2/Supplement.pdf |year=2005}}</ref> 

Infopro won the Ministry of International Trade and Industry (MITI)'s Export Excellence Award (Services) in 2010.<ref>{{ cite web |title= MITI Winners Of The Industry Excellence Award 2010 |url=http://www.miti.gov.my/cms/content.jsp?id=com.tms.cms.article.Article_70422f6c-c0a81573-30c830c8-c7b2fbfa}}</ref> Inntron, an information technology consultancy in the Banking and Finance sector globally ranked Infopro to be top 2 vendors in Malaysia<ref>{{ cite web |title= Inntron Top Ranking Malaysia |url=http://www.inntron.com/toprank_malaysia.htm |date=2011-11-26}}</ref>  and within the top 40 of core banking vendors in world<ref>{{ cite web |title= Inntron Core Banking Vendor Ranking |url=http://www.inntron.com/core_banking.html}}</ref>

==Overseas office profile==

Infopro is based in Malaysia and have regional offices in  [[China]], [[Singapore]], [[Philippines]] and [[United Arab Emirates]]. In addition to the above, Infopro is also represented by local software houses and local partners in, [[Ghana]] and [[Iran]].<ref>{{ cite web |title= Infopro kicks off projects in Iran and Malaysia|url=http://www.ibsintelligence.com/index.php?option=com_content&view=article&id=12863:infopro-kicks-off-projects-in-iran-and-malaysia&catid=2:news&Itemid=12 |date=2009-03-03}}</ref><ref>{{ cite news |title= Malaysian firm to sell software to Banks in Gulf|url=http://www.accessmylibrary.com/article-1G1-85295039/malaysian-firm-sell-software.html |date=2002-02-14 | work=BERNAMA The Malaysian National News Agency}}</ref>

==Infopro Organisation Structure==

Infopro employs a staff of more than 250, 70% technical staff, 20% support staff and the remaining 10% the business team.

==ICBA system ==

The ICBA version 1.0 was formally released in 1990 and the latest ICBA version is 9.0 -<ref>{{cite web |title=IBM Partner Sizing Guide |url=http://www-304.ibm.com/partnerworld/wps/sizing/sizingguide/view.jsp?guide_id=sgq63310550241808002%7C30&data_guide_private=n }}</ref> 

===ICBA product features===

ICBA system is a core banking system that integrates both front end banking such as teller functions and back end banking such as back office function, administration, customer information, accounting and data centre functions. The modules are packaged under ICBA system includes the following modules: -<ref>{{cite web |title= ICBA Modules |url=http://www.softscout.com/software/Banking-and-Financial/Bank-Management/ICBA-System.html}}</ref> 
* Customer Information Module
* Deposit Modules (such as Saving, Current, Fixed Deposit)
* Loan Modules (such as Term Loan, Hire Purchase, Leasing)
* Trade Finance Module
* Treasury Module
* Remittance Module
* Swift Modules
* Nostro Reconciliation Module
* Fixed Asset Module
* eBanking Modules
* General Ledger Module

The Customer Information Module is the central module for the ICBA system.<ref>{{cite web |title= Infopro to expand globally |url=https://news.google.com/newspapers?nid=1309&dat=19981015&id=zOYVAAAAIBAJ&sjid=thQEAAAAIBAJ&pg=4648,1493832 |date=1998-10-15}}</ref>

===Current eICBA deployments===

ICBA system has been installed in more than 82 financial institutions worldwide and has international presence in more than 27 countries including [[Afghanistan]]<ref>{{cite web |title= Afghan bank signs for ICBA |url=http://www.ibsintelligence.com/index.php?option=com_content&view=article&id=10053:afghan%20bank%20signs%20for%20icba&catid=175:2007&Itemid=32}}</ref> 

===ICBA Islamic Certification===

ICBA Islamic modules have been certified by a Syariah expert, Dr Mohd Daud Bakar who is an Associate Professor at the Department of Islamic and Family Law and also the Dean of the Centre for Postgraduate Studies at the International Islamic University Malaysia.<ref>{{cite web |title= Shariah Board Member Dr Mohd Daud Bakar |url= http://www.islamicbankasia.com/shariah/dmdb/Pages/default.aspx }}</ref>

==Infopro Online Support==

Support for eICBA is done via web based support homepage known as CSSi (Customer Support System – internet).

==External links==
* [http://www.infopro.com.my/ Infopro homepage]
* [http://cssi.infopro.com.my/ Infopro CSSi homepage]

== Sources ==
* http://solutions.oracle.com/partners/infopro
* http://www.accessmylibrary.com/coms2/summary_0286-20737183_ITM
* http://web10.bernama.com/kpdnhep/news.php?id=236180
* http://www.inntron.com/banksys/infopro.htm
* http://www.cpilive.net/v3/inside.aspx?scr=n&NID=1252&cat=CASE%20STUDY&pub=COMPUTER%20NEWS%20MIDDLE%20EAST&k=transformation,%20banking
* http://www.accessmylibrary.com/coms2/summary_0286-20737183_ITM
* http://www.ibsintelligence.com/index.php?option=com_content&view=article&id=10053:afghan%20bank%20signs%20for%20icba&catid=175:2007&Itemid=32
* http://www.dinarstandard.com/finance/IF_Tech100509.html
* http://www.mfqasia.com/clients.asp
* http://www.inntron.com/toprank_malaysia.htm
* http://www.inntron.com/core_banking.html

==Notes==
{{Reflist}}

[[Category:Software companies of Malaysia]]
[[Category:Banking software companies]]
[[Category:Software companies established in 1987]]
[[Category:1987 establishments in Malaysia]]
[[Category:Malaysian brands]]
[[Category:Privately held companies of Malaysia]]