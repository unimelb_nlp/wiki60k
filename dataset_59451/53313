{{about|the indoor arena in Raleigh, North Carolina|the baseball park in Pittsburgh, Pennsylvania|PNC Park|the baseball park in Moosic, Pennsylvania|PNC Field}}
{{redirect|RBC Center|the office tower in Toronto, Ontario|RBC Centre|the arena in Sarnia, Ontario|RBC Centre (Sarnia)}}
{{Infobox stadium
| stadium_name        = PNC Arena
| nickname            = 
| logo_image          = [[File:PNCArenalogo.png|200px]]
| image               = PNC Arena Raleigh.JPG
| caption             = PNC Arena South Entrance in 2013
| address             = 1400 Edwards Mill Road
| location            = [[Raleigh, North Carolina]]
| coordinates         = {{Coord|35|48|12|N|78|43|19|W|type:landmark_scale:2000|display=inline,title}}
| broke_ground        = July 22, 1997
| opened              = October 29, 1999
| renovated           = 2003, 2008, 2009
| closed              = 
| demolished          = 
| owner               = Centennial Authority
| operator            = [[Carolina Hurricanes|Gale Force Sports & Entertainment]]
| surface             = Multi-surface
| construction_cost   = [[United States dollar|$]]158 million<br>(${{formatprice|{{Inflation|US|158000000|1999}}}} in {{Inflation-year|US}} dollars{{inflation-fn|US}})
| architect           = Odell Associates, Inc.
| structural engineer = [[Geiger Engineers]]<ref>{{cite web |title=PNC Arena|url=http://www.geigerengineers.com/project.cfm?projcatID=4&projectID=9|publisher=Geiger Engineers|accessdate=February 5, 2013}}</ref>
| Mechanical Contractor   = John J. Kirlin, LLC.<ref>{{cite web |title=Portfolio: RBC Center|url=http://www.jjkllc.com/sports/entertainment-sports-arena.html|publisher=Kirlin mechanical contractors|accessdate=February 5, 2013}}</ref>
| project_manager     = [[Lend Lease Project Management & Construction|McDevitt Street Bovis, Inc.]]<ref name="info"/>
| general_contractor  = [[Hensel Phelps Construction|Hensel Phelps Construction Co.]]<ref name="info">{{cite web |title=PNC Arena: Info|url=http://www.thepncarena.com/arena_info|publisher=Centennial Authority|accessdate=February 5, 2013}}</ref>
| former_names        = Raleigh Entertainment & Sports Arena (1999–2002)<br>RBC Center (2002&ndash;2012)
| tenants             = [[Carolina Hurricanes]] ([[National Hockey League|NHL]]) (1999–present)<br>[[NC State Wolfpack]] ([[Atlantic Coast Conference|ACC]]) (1999&ndash;present)<br>[[Carolina Cobras]] ([[Arena Football League|AFL]]) (2000&ndash;2002) | seating_capacity    = [[Basketball]]: 19,722<ref name="info"/><br>[[Ice hockey]]: 18,680<ref name="info"/><br>[[Concerts]]: 19,500
| dimensions          = {{convert|700000|sqft|m2}}
| website             =http://www.thepncarena.com/
}}
'''PNC Arena'''<ref>{{cite news |title=RBC Center Out; PNC Arena In|first=Josh|last=Shaffer|url=http://www.newsobserver.com/2012/03/16/1933901/rbc-center-out-pnc-arena-in.html|newspaper=[[The News & Observer]]|publisher=[[The McClatchy Company]]|location=Raleigh|date=March 16, 2012|accessdate=April 30, 2012|archiveurl=http://www.freezepage.com/1335807722LNKVBRJQWP?url=http://www.newsobserver.com/2012/03/16/1933901/rbc-center-out-pnc-arena-in.html|archivedate=April 30, 2012}}</ref> (originally '''Raleigh Entertainment & Sports Arena''' and formerly the '''RBC Center''') is an [[list of indoor arenas|indoor arena]], located in [[Raleigh, North Carolina]]. The Arena seats 19,722 for basketball,<ref name="info"/> and 18,680 for [[ice hockey]],<ref name="info"/> including 59 suites, 13 loge boxes and 2,000 club seats. The building has three concourses and includes a 300-seat restaurant.

It is home to the [[Carolina Hurricanes]] of the [[National Hockey League]] and the [[NC State Wolfpack]] [[NC State Wolfpack men's basketball|men's basketball team]] of [[NCAA]] [[Division I (NCAA)|Division I]]. The arena neighbors [[Carter–Finley Stadium]], home of [[NC State Wolfpack football|Wolfpack Football]]; the [[North Carolina State Fair]]grounds; and [[Dorton Arena]] (on the Fairgrounds). The arena also hosted the [[Carolina Cobras]] of the [[Arena Football League (1987–2008)|Arena Football League]] from 2000 to 2002. It is the [[Atlantic Coast Conference#Facilities|fourth-largest arena in the ACC]] (after the [[Carrier Dome]], [[KFC Yum Center]], and the [[Dean Smith Center]]), and the third-largest designed specifically for basketball. Furthermore, it is the eighth-largest arena in the NCAA and the seventh-largest designed for basketball.

==History==
The idea of a new basketball arena to replace the Wolfpack's longtime home, [[Reynolds Coliseum]], first emerged in the 1980s under the vision of Wolfpack [[head coach|coach]] [[Jim Valvano]]. In 1989, the NCSU Trustees approved plans to build a 23,000 seat arena. The Centennial Authority was created by the [[North Carolina General Assembly]] in 1995 as the governing entity of the arena, then financed by state appropriation, local contributions, and University fundraising. The Centennial Authority refocused the project into a multi-use arena, leading to the 1997 relocation agreement of the Hurricanes (then the [[Hartford Whalers]]). Construction began that year and was completed in 1999 with an estimated cost of [[United States dollar|$]]158 million, which was largely publicly financed by a Hotel and Restaurant tax. The Hurricanes agreed to pay $60 million of the cost, and the state of North Carolina paid $18 million.  As part of the deal, the Hurricanes assumed operational control of the arena.

Known as the Raleigh Entertainment and Sports Arena (or ESA) from 1999 to 2002, it was renamed the RBC Center after an extended search for a corporate sponsor. [[RBC Bank]], the US division of the [[Royal Bank of Canada]], acquired 20-year [[naming rights]] for a reported $80 million.  On June 19, 2011, it was announced that [[PNC Financial Services]] bought US assets of RBC Bank and acquired the naming rights to the arena pending approval by the regulatory agencies.<ref>{{cite news |title=Goodbye RBC Center|first=David|last=Ranii|url=http://www.newsobserver.com/2011/06/20/1287481/goodbye-rbc-center.html|newspaper=[[The News & Observer]]|publisher=[[The McClatchy Company]]|location=Raleigh|date=June 20, 2011|accessdate=May 26, 2012}} {{Dead link|date=March 2017}}</ref> On December 15, 2011, the Centennial Authority, the landlord of the arena, approved a name change for the facility to PNC Arena.<ref>{{cite news |title=RBC Signs Coming Down at RBC Center|first=Jason|last=deBruyn|url=http://www.bizjournals.com/triangle/blog/2012/01/rbc-signs-coming-down-at-rbc-center.html|newspaper=[[American City Business Journals|Triangle Business Journal]]|date=January 25, 2012|accessdate=January 26, 2012}}</ref> The name change officially took place on March 15, 2012.<ref>{{cite press release |title=Hurricanes and PNC Bank to Introduce PNC Arena to Community on March 15|url=http://www.thepncarena.com/news/detail/hurricanes-and-pnc-bank-to-introduce-pnc-arena-to-community-on-march-15|publisher=Centennial Authority|date=February 23, 2012|accessdate=January 13, 2013}}</ref> On a normal hockey day, PNC Arena has more than 400 people on duty for security and concessions.

Raleigh experienced its first NHL game on October 29, 1999, when the Hurricanes hosted the [[New Jersey Devils]] on the building's opening night. PNC Arena hosted games of both the 2002 Stanley Cup Playoffs and Finals, however the Hurricanes lost in the final. On June 19, 2006, the Hurricanes were on home ice for a decisive [[2006 Stanley Cup Final#Game seven|game seven of the Stanley Cup Final]], defeating the [[Edmonton Oilers]] 3&ndash;1 to bring the franchise its first [[Stanley Cup]] and North Carolina its first major professional sports championship. The arena hosted the playoffs again in 2009, with the Hurricanes losing in the Eastern Conference Finals.<ref>http://www.hockey-reference.com/teams/CAR/2009.html</ref> On April 8, 2010, the Hurricanes and the NHL announced the arena would host the [[58th National Hockey League All-Star Game]] on January 30, 2011.

===Renovations===
A ribbon board was installed in 2003 which encircles the arena bowl.<ref>http://www.thepncarena.com/arena_info/fun_facts</ref> In 2008, the arena renovated its sound system. Clair Brothers Systems installed a combination of [[JBL]] line arrays to provide improved audio coverage for all events. In June 2009, video crews installed a new [[Daktronics]] [[High-definition video|HD]] scoreboard. It replaced the ten-year-old scoreboard that had been in the arena since its opening. The scoreboard is full [[LED]] and four-sided with full video displays, whereas the old scoreboard was eight-sided and four of those sides featured alternating static [[dot-matrix display]]s (very much outdated for today's standards). In addition, the scoreboard features an octagonal top section with full video capability, along with two rings above and below the main video screens; they are similar to the ribbon board encircling the arena.

In October 2015, architects met with the Centennial Authority to discuss a potential renovation which includes all-new entrances, a new rooftop restaurant and bar, covered tailgating sections, and moving the administrative offices elsewhere in the arena as a result. Project costs have not yet been decided, as the architects were given until May/June 2016 to come up with estimates. The Centennial Authority would have to approve the estimates before official voting on the project could begin.<ref>http://www.newsobserver.com/sports/college/acc/nc-state/article41563464.html</ref><ref>http://cardiaccane.com/2016/01/19/carolina-hurricanes-home-update-on-proposed-pnc-arena-renovations/</ref>

During the summer of 2016, the ribbon boards were upgraded and a second ribbon board was added to the upper level fascia. Static advertising signs inside the arena were replaced with LED video boards.
[[File:RBC Center Concourse.JPG|250px|right|thumb|One of the main concourses inside PNC Arena during a Hurricanes game in 2009.]]

===Notable Events===
In addition to hockey and college basketball, PNC Arena hosts a wide array of concerts, family shows, and other events each year. Past performers include Bruce Springsteen, Cher, Eric Clapton, Taylor Swift, Billy Joel, Justin Timberlake, Elton John, Lady Gaga, One Direction, Celine Dion, George Strait, Bon Jovi, Keith Urban, and many other artists. Family shows have included Ringling Brothers and Barnum & Bailey Circus, Sesame Street Live, Disney On Ice, and the Harlem Globetrotters.

PNC Arena hosted the [[Central Intercollegiate Athletic Association]] (CIAA) men's basketball tournament from 1999 to 2008.

The arena was a site for Rounds 1 and 2 of the [[2004 NCAA Men's Division I Basketball Tournament|2004]], [[2008 NCAA Men's Division I Basketball Tournament|2008]], and [[2014 NCAA Men's Division I Basketball Tournament|2014]] NCAA Men's Division I Basketball Tournament, respectively. Recent controversy in North Carolina due to the [[Public Facilities Privacy & Security Act]] House Bill 2, it was planned to host the first and second rounds for the [[2016 NCAA Men's Division I Basketball Tournament]] but has since been moved to another location.
[[Image:RBC Center.jpg|250px||right|thumb|An NC State college basketball game at PNC Arena in 2008.]]

==List of Concerts and events==
{|class="wikitable collapsible collapsed" style="text-align:center;"
!colspan="4"|List of concerts at the arena
|-
! width="175"| Artist
! width="250"| Event
! width="120"| Date
! width="230"| Opening act(s)
|-
|[[AC/DC]]
|'''[[Stiff Upper Lip World Tour]]'''
|April 1, 2001
|{{n/a}}
|-
|[[Alan Jackson]]
|
|October 27, 2017
|[[Lauren Alaina]]
|-
|[[Ariana Grande]]
|'''[[The Honeymoon Tour]]'''
|September 24, 2015
|[[Prince Royce]] & [[Who Is Fancy]]
|-
|[[The Avett Brothers]]
|
|December 31, 2015
|{{n/a}}
|-
|[[Avril Lavigne]]
|'''[[The Best Damn Tour]]'''
|July 30, 2008
|{{n/a}}
|-
|rowspan="2"|[[Backstreet Boys]]
|'''[[Into the Millennium Tour]]'''
|February 18, 2000
|{{n/a}}
|-
|'''[[Black & Blue Tour]]'''
|June 13, 2001
|[[Krystal Harris]] & [[Shaggy (musician)|Shaggy]]
|-
|[[Barry Manilow]]
|
|April 26, 2013
|{{n/a}}
|-
|[[Beyoncé]]
|'''[[The Beyoncé Experience]]'''
|July 28, 2007
|[[Robin Thicke]]
|-
|[[Billy Joel]]
|'''[[Billy Joel in Concert]]'''
|February 9, 2014
|{{n/a}}
|-
|[[The Black Eyed Peas]]
|'''[[The E.N.D. World Tour]]'''
|February 19, 2009
|[[Ludacris]] & [[LMFAO]]
|-
|[[The Black Keys]]
|'''Turn Blue Tour'''
|December 5, 2014
|[[St. Vincent (musician)|St. Vincent]]
|-
|[[Bob Segar|Bob Segar & the Silver Bullet Band]]
|'''Rock and Roll Never Forgets Tour'''
|April 27, 2013
|Big Daddy Love
|-
|rowspan="2"|[[Bon Jovi]]
|'''[[Bounce Tour]]'''
|March 21, 2003
|rowspan="2" {{n/a}}
|-
|'''[[Because We Can (concert tour)|Because We Can Tour]]'''
|November 6, 2013
|-
|[[Brantley Gilbert]]
|'''Let it Ride Tour'''
|October 30, 2014
|
|-
|rowspan="2"|[[Britney Spears]]
|'''[[Dream Within a Dream Tour]]'''
|December 14, 2001
|[[LFO (American band)|LFO]]
|-
|'''[[Femme Fatale Tour]]'''
|August 21, 2011
|[[Destinee & Paris]] & [[Pauly D|DJ Pauly D]]
|-
|rowspan="2"|[[Bruce Springsteen]] and the [[E Street Band]]
|'''[[Bruce Springsteen and the E Street Band Reunion Tour]]'''
|April 22, 2000
|rowspan="2" {{n/a}}
|-
|'''[[High Hopes Tour]]'''
|April 24, 2014
|-
|[[Bruno Mars]]
|'''[[The Moonshine Jungle Tour]]'''
|June 14, 2014
|[[Aloe Blacc]] & [[Pharrell Williams]]
|-
|[[Casting Crowns]]
|'''The Very Next Thing Tour'''
|March 10, 2017
|[[Danny Gokey]] & [[Unspoken (band)|Unspoken]]
|-
|[[Charlie Wilson]]
|'''Forever Charlie Tour'''
|February 18, 2015
|Kem Joe
|-
|[[Cher]]
|'''[[Dressed to Kill Tour (Cher)|Dressed to Kill Tour]]'''
|May 7, 2014
|[[Cyndi Lauper]]
|-
|[[Christina Aguilera]]
|'''[[Back to Basics Tour]]'''
|May 1, 2007
|[[Pussycat Dolls]] & [[Danity Kane]]
|-
|[[Dave Matthews Band]]
|'''Winter 2012 Tour'''
|December 12, 2012
|[[The Lumineers]]
|-
|[[Demi Lovato]]
|'''[[Demi World Tour]]'''
|September 12, 2014
|[[MKTO]]
|-
|rowspan="2"|[[Eagles (band)|Eagles]]
|'''[[Long Road Out of Eden Tour]]'''
|June 17, 2010
|[[Dixie Chicks]]
|-
|'''[[History of the Eagles]]'''
|February 28, 2014
|[[JD & The Straight Shot]]
|-
|[[Elton John]]
|'''[[Greatest Hits Tour (Elton John)|Greatest Hits Tour]]'''
|March 16, 2012
|{{n/a}}
|-
|[[Eric Church]]
|'''The Outsiders World Tour'''
|April 23, 2015
|[[Dwight Yoakam]], [[Brothers Osborne]]<br>[[Halestorm]] & [[Drive By Truckers]]
|-
|[[Eric Clapton]]
|'''50th Anniversary Tour'''
|April 3, 2013
|[[The Wallflowers]]
|-
|rowspan="3"|[[Garth Brooks]]
|rowspan="3"|'''[[The Garth Brooks World Tour with Trisha Yearwood]]'''
|March 11, 2016
|rowspan="3"|[[Trisha Yearwood]]
|-
|March 12, 2016
|-
|March 13, 2016
|- 
|[[Justin Timberlake]]
|'''[[The 20/20 Experience World Tour]]'''
|November 13, 2013
|[[The Weeknd]]
|-
|[[Lady Gaga]]
|'''[[The Monster Ball Tour]]'''
|September 19, 2010
|[[Semi Precious Weapons]]
|-
|rowspan="2"|[[Katy Perry]]
|'''[[California Dreams Tour]]'''
|June 14, 2011
|[[Robyn]] & DJ Skeet Skeet
|-
|'''[[The Prismatic World Tour]]'''
|June 22, 2014
|[[Capital Cities (band)|Capital Cities]] & [[Ferras]]
|-
|rowspan="2"|[[Keith Urban]]
|'''Escape Together World Tour'''
|June 19, 2009
|[[Sugarland]]
|-
|'''[[Get Closer 2011 World Tour]]'''
|June 25, 2011
|[[Jake Owen]]
|-
|[[Kelly Clarkson]] & [[Clay Aiken]]
|'''[[Independent Tour]]'''
|March 1, 2004
|[[The Beu Sisters]]
|-
|[[Kenny Chesney]]
|'''[[The Big Revival Tour]]'''
|May 28, 2015
|[[Jake Owen]] & [[Cole Swindell]]
|-
|[[Kevin Hart]]
|'''What Now'''
|May 1, 2015
|{{n/a}}
|-
|[[Luis Miguel]]
|'''[[México En La Piel Tour]]'''
|October 26, 2005
|{{n/a}}
|-
|[[Macklemore & Ryan Lewis]]
|'''The Heist Tour'''
|November 19, 2013
|{{n/a}}
|-
|rowspan="2"|[[Michael Bublé]]
|'''[[Crazy Love Tour]]'''
|July 9, 2010
|rowspan="2"|[[Naturally 7]]
|-
|'''[[To Be Loved Tour]]'''
|October 25, 2013
|-
|[[Miley Cyrus]]
|'''[[Bangerz Tour]]'''
|April 8, 2014
|[[Icona Pop]] & [[Sky Ferreira]]
|-
|[[Mötley Crüe]]
|'''[[Mötley Crüe Final Tour]]'''
|August 28, 2015
|[[Alice Cooper]]
|-
|[[Nickelback]]
|'''[[All the Right Reasons Tour]]'''
|September 3, 2006
|{{n/a}}
|-
|[[Nine Inch Nails]]
|'''[[Twenty Thirteen Tour]]'''
|October 21, 2013
|[[Godspeed You! Black Emperor]] & [[Explosions in the Sky]]
|-
|[[One Direction]]
|'''[[Take Me Home Tour (One Direction)|Take Me Home Tour]]'''
|June 22, 2013
|[[5 Seconds of Summer]]
|-
|[[Paul McCartney]]
|'''[[Driving World Tour]]'''
|October 7, 2002
|{{n/a}}
|-
|[[Prince (musician)|Prince]]
|'''[[Welcome 2]]'''
|March 23, 2011
|[[Anthony Hamilton (musician)|Anthony Hamilton]]
|-
|rowspan="2"|[[Red Hot Chili Peppers]]
|'''[[Stadium Arcadium World Tour]]''''
|January 22, 2007
|[[Gnarls Barkley]]
|-
|'''[[I'm With You World Tour]]'''
|April 4, 2012
|[[Santigold]]
|-
|[[Roger Waters]]
|'''[[The Wall Live (2010–13)|The Wall Live]]'''
|July 9, 2012
|{{n/a}}
|-
|[[Rush (band)|Rush]]
|'''[[Clockwork Angels Tour]]'''
|May 3, 2013
|{{n/a}}
|-
|rowspan="2"|[[Sam Smith (singer)|Sam Smith]]
|rowspan="2"|'''[[In the Lonely Hour Tour]]'''
|June 23, 2015
|rowspan="2"|[[Gavin James (singer-songwriter)|Gavin James]]
|-
|October 6, 2015
|-
|[[Stevie Nicks]]
|'''24 Karat Gold Tour'''
|March 19, 2017
|[[The Pretenders]]
|-
|rowspan="4"|[[Taylor Swift]]
|'''[[Fearless Tour]]'''
|May 1, 2010
|[[Kellie Pickler]] & [[Gloriana (band)|Gloriana]]
|-
|'''[[Speak Now World Tour]]'''
|November 17, 2011
|[[Needtobreathe|NEEDTOBREATHE]] & [[Danny Gokey]] 
|-
|'''[[The Red Tour]]'''
|September 13, 2013
|[[Ed Sheeran]] & [[Casey James]]
|-
|'''[[The 1989 World Tour]]'''
|June 9, 2015
|[[Vance Joy]]
|-
|[[TLC (group)|TLC]]
|'''[[FanMail Tour]]''' <br>{{small|First ticketed concert at the venue}}
|November 5, 1999 
|[[Ideal (group)|Ideal]]
|-
|rowspan="2"|[[Tim McGraw]] & [[Faith Hill]]
|'''[[Soul2Soul Tour]]
|July 15, 2000
|Keith Urban
|-
|'''[[Soul2Soul II Tour]]'''
|June 9, 2006
|{{n/a}}
|-
|[[Tina Turner]]
|'''[[Twenty Four Seven Tour]]'''
|October 8, 2000
|[[Joe Cocker]]
|-
|[[Trans-Siberian Orchestra]]
|'''TSO East 2016'''
|December 14, 2016
|{{n/a}}
|-
|[[Tom Petty and the Heartbreakers]]
|'''Hypnotic Eye Tour'''
|September 18, 2014
|[[Steve Winwood]]
|-
|[[Van Halen]]
|'''[[Van Halen 2007-2008 North American Tour|2008 North American Tour]]'''
|May 5, 2008
|[[Ryan Shaw]]
|-
|[[The Who]]
|'''[[The Who Hits 50!]]'''
|April 21, 2015
|[[Joan Jett#Joan Jett and the Blackhearts|Joan Jett and the Blackhearts]]
|-
|rowspan="3"|[[Winter Jam Tour Spectacular]]
|'''Winter Jam 2015'''
|March 13, 2015
|rowspan="3" {{n/a}}
|-
|'''Winter Jam 2016'''
|March 25, 2016
|-
|'''Winter Jam 2017'''
|January 22, 2017
|}

{|class="wikitable collapsible collapsed" style="text-align:center;"
!colspan="4"|List of other events at the arena
|-
! width="100"| Year
! width="290"| Event
|-
|2000
|[[SummerSlam (2000)|WWF Summerslam 2000]]
|-
|2002 
|[[2002 Stanley Cup Finals|Stanley Cup Finals]]
|-
|rowspan="2"|2004 
|[[2004 NHL Entry Draft|NHL Draft]]
|-
|[[Professional Bull Riders|PBR]] [[Built Ford Tough Series]] Tour <br>(formerly ''Bud Light Cup'')
|-
|2005 
|[[Jeopardy! College Championship]]
|-
|rowspan="3"|2006
|[[MEAC Men's Basketball Tournament]]
|-
|[[No Mercy (2006)]]
|-
|[[2006 Stanley Cup Finals|Stanley Cup Finals]]
|-
|rowspan="3"|2007 
|[[2007 MEAC Men's Basketball Tournament|MEAC Men's Basketball Tournament]]
|-
|[[2007 NCAA Women's Division I Basketball Tournament|NCAA Women's Division I Basketball Tournament]] <br>{{small|first and second round}}
|-
|[[Professional Bull Riders|PBR]] [[Built Ford Tough Series]] Tour <br>(formerly ''Bud Light Cup'')
|-
|rowspan="2"|2008
|[[2008 MEAC Men's Basketball Tournament|MEAC Men's Basketball Tournament]]
|-
|[[2008 NCAA Men's Division I Basketball Tournament|NCAA Men's Division I Basketball Tournament]] <br>{{small|first and second round}}
|-
|2009 
|[[2009 Stanley Cup Playoffs|Stanley Cup Playoffs]]
|-
|2011
|[[58th National Hockey League All-Star Game|National Hockey League All-Star game]]
|-
|2014
|[[2014 NCAA Men's Division I Basketball Tournament|NCAA Men's Division I Basketball Tournament]] <br>{{small|first and second round}}
|-
|2016
|[[2016 NCAA Men's Division I Basketball Tournament|NCAA Men's Division I Basketball Tournament]] <br>{{small|first and second round}}
|}

*''[[One Tree Hill (TV series)|One Tree Hill]]'' location shoot for the [[One Tree Hill (season 4)|season 4]] episode, "Some You Give Away". The Tree Hill Ravens were playing in the state championship game held at the arena.

==References==
{{reflist|33em}}

==Notes==
*NCSU Athletics. RBC Center Retrieved July 12, 2004 from [http://gopack.collegesports.com/facilities/esa.html].
*RBC Center: History. Retrieved July 12, 2004 from [http://www.rbccenter.com/about/10.asp].
*The Hockey News. 59.37 (2006): 6.
*Live Sound: Clair Systems Revamps RBC Center Audio with JBL Loudspeakers. Retrieved May 15, 2009 from [http://www.prosoundweb.com/article/clair_systems_revamps_rbc_center_audio_with_jbl_loudspeakers/]
*RBC Center Gets New Scoreboard. Retrieved June 7, 2009 from [http://www.gogoraleigh.com/2009/06/02/rbc-center-gets-new-scoreboard/]

==External links==
{{Commons category}}
*[http://www.thepncarena.com/ Official site]
*[http://www.gopack.com/facilities/pnc-arena.html PNC Arena - NC State Athletics]
{{s-start-collapsible|header={{s-sta|et}}}}
{{succession box
 | title = Home of the<br>[[Carolina Hurricanes]]
 | years = 1999 – present
 | before = [[Greensboro Coliseum]]
 | after = current
}}
{{succession box
 | title = Home of the<br>[[Carolina Cobras]]
 | years = 2000–2002
 | before = none
 | after = [[Charlotte Coliseum]]
}}
{{succession box
 | title = Host of the<br>[[Jeopardy! College Championship]]
 | years = 2005
 | before = [[Petersen Events Center]]
 | after = [[Galen Center]]
}}
{{succession box
 | title = Host of the<br>[[NHL All-Star Game]]
 | years = 2011
 | before = [[Bell Centre]]
 | after = [[Scotiabank Place]]
}}
{{end}}

{{NC State Wolfpack men's basketball navbox}}
{{Atlantic Coast Conference basketball venue navbox}}
{{Carolina Hurricanes}}
{{NHL Arenas}}
{{Carolina Cobras}}
{{PNC Financial Services Group}}
{{Music venues of North Carolina}}
{{Triangle sports venues}}

{{DEFAULTSORT:PNC Arena}}
[[Category:1999 establishments in North Carolina]]
[[Category:Sports venues completed in 1999]]
[[Category:Basketball venues in North Carolina]]
[[Category:Carolina Hurricanes arenas]]
[[Category:College basketball venues in the United States]]
[[Category:Indoor ice hockey venues in the United States]]
[[Category:National Hockey League venues]]
[[Category:NC State Wolfpack basketball venues]]
[[Category:Sports venues in Raleigh, North Carolina]]
[[Category:Royal Bank of Canada]]
[[Category:Arena football venues]]