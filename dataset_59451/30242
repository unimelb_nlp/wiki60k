{{featured article}}
{{Infobox officeholder
|name          = Henry Burnett
|image         = Henry Cornelius Burnett - Brady-Handy.jpg
|alt           = A man in his thirties with black hair and a black beard. He is wearing a white shirt and black jacket.
|office        = [[Congress of the Confederate States|Confederate States Senator]]<br>from [[Kentucky]]
|term_start    = February 18, 1862
|term_end      = May 10, 1865
|predecessor   = Constituency established
|successor     = Constituency abolished
|state1        = [[Kentucky]]
|district1     = {{ushr|Kentucky|1|1st}}
|term_start1   = March 4, 1855
|term_end1     = December 3, 1861
|predecessor1  = [[Linn Boyd]]
|successor1    = [[Samuel L. Casey|Samuel Casey]]
|birth_date    = {{birth date|1825|10|25}}
|birth_place   = [[Essex County, Virginia|Essex County]], [[Virginia]], [[United States|U.S.]]
|death_date    = {{nowrap|{{death date and age|1866|10|01|1825|10|5}}}}
|death_place   = [[Hopkinsville, Kentucky|Hopkinsville]], [[Kentucky]], [[United States|U.S.]]
|party         = [[Democratic Party (United States)|Democratic]]
|alma_mater    = [[University of Virginia]]
|religion      = [[Christian Church (Disciples of Christ)|Disciples of Christ]]
|signature     = Henry C. Burnett sig.jpg
|signature_alt = H.C. Burnett
|allegiance    = {{flag|Confederate States of America|1861}}
|branch        = {{army|CSA}}
|serviceyears  = 1861–1862
|rank          = [[File:Confederate_States_of_America_Colonel.png|35px]] [[Colonel (United States)|Colonel]]
|unit          = {{flagicon|Kentucky}} [[8th Kentucky Infantry]]
|battles       = [[American Civil War]]
}}
'''Henry Cornelius Burnett''' (October 25, 1825 – October 1, 1866) was a [[United States House of Representatives|U.S. Representative]] from the [[U.S. state|state]] of [[Kentucky]] and a [[Confederate States Senate|Confederate States senator]]. A lawyer by profession, Burnett had held only one public office—circuit court clerk—before being elected to Congress. He represented [[Kentucky's 1st congressional district]] immediately prior to the [[American Civil War|Civil War]]. This district contained the entire [[Jackson Purchase]] region of the state, which was more sympathetic to the [[Confederate States of America|Confederate]] cause than any other area of Kentucky. Burnett promised the voters of his district that he would have [[President of the United States|President]] [[Abraham Lincoln]] [[arraignment|arraigned]] for [[treason]]. Unionist newspaper editor [[George D. Prentice]] described Burnett as "a big, burly, loud-mouthed fellow who is forever raising [[point of order|points of order]] and objections, to embarrass the [[Republican Party (United States)|Republicans]] in the House".<ref>Craig, "Henry C. Burnett", p. 268</ref>

Besides championing the Southern cause in Congress, Burnett also worked within Kentucky to bolster the state's support of the Confederacy. He presided over a sovereignty convention in [[Russellville, Kentucky|Russellville]] in 1861 that formed a [[Confederate government of Kentucky|Confederate government]] for the state. The delegates to this convention chose Burnett to travel to [[Richmond, Virginia]] to secure Kentucky's admission to the Confederacy. Burnett also raised a Confederate [[regiment]] at [[Hopkinsville, Kentucky]], and briefly served in the [[Confederate States Army]]. Camp Burnett, a Confederate recruiting post two miles west of [[Clinton, Kentucky|Clinton]] in [[Hickman County, Kentucky]], was named after him.<ref name=camp>''Camp Burnett, Kentucky''</ref>

Burnett's actions were deemed treasonable by his colleagues in Congress, and he was [[Expulsion from the United States Congress|expelled]] from the House in 1861. He is one of only five members of the House of Representatives ever to be expelled.<ref name=foxnews>"Members of Congress Expelled From House"</ref> Following his expulsion, Burnett served in the [[Provisional Confederate Congress]] and the First and Second [[Confederate Senate]]s. He was indicted for treason after the war, but never tried. He returned to the practice of law, and died of [[cholera]] in 1866 at the age of 40.

==Early life and political career==
Henry Cornelius Burnett was born to Dr. Isaac and Martha F. (Garrett) Burnett on October 25, 1825, in [[Essex County, Virginia]].<ref name=trigg>''Trigg County, Kentucky Veterans''</ref><ref name=kye>Craig, ''The Kentucky Encyclopedia'' p. 144</ref> In his early childhood, he moved to [[Cadiz, Kentucky]], with his family.<ref name=kye /> He was educated in the common schools of the area and at an academy in Hopkinsville, Kentucky.<ref name=congbio>"Burnett, Herny Cornelius". United States Congress</ref> Following this, he studied law, was [[Admission to the bar in the United States|admitted to the bar]] in 1847, and commenced practice at Cadiz.<ref name=congbio /> He was a member of the Cadiz Christian Church.<ref>Perrin, p. 100</ref>

On April 13, 1847, Burnett married Mary A. Terry, the daughter of a prominent Cadiz merchant.<ref name=trigg /><ref name=kerr />  They had four children: John, Emeline, Henry, and Terry (who died shortly after birth).<ref name=trigg /> The younger Henry Burnett became a successful lawyer in [[Paducah, Kentucky|Paducah]] and, later, [[Louisville, Kentucky|Louisville]].<ref name=johnson>Johnson, p. 615</ref>

In the first election following the ratification of the [[Kentucky Constitution]] of 1850, Burnett was elected clerk of the [[circuit court]] of [[Trigg County, Kentucky]], defeating James E. Thompson.<ref name=allen>Allen, p. 279</ref><ref name=perrin47>Perrin, p. 47</ref> He resigned in 1853 to run for Congress.<ref name=perrin47 /> Later that year, he was elected as a [[Democratic Party (United States)|Democrat]] to the [[34th United States Congress|34th Congress]], succeeding [[Speaker of the United States House of Representatives|Speaker of the House]] [[Linn Boyd]].<ref name=kye /><ref name=allen /> He was re-elected to the three succeeding Congresses; during the [[35th United States Congress|35th Congress]], he chaired the Committee of Enquiry regarding the sale of [[Fort Snelling]] and served on the Committee on the [[District of Columbia]].

==Outset of the Civil War==
Burnett supported fellow Kentuckian [[John C. Breckinridge]] for president in the [[United States presidential election, 1860|1860 presidential election]], but Breckinridge lost to Abraham Lincoln. Lincoln had campaigned against the expansion of slavery beyond the states in which it already existed. His victory in the election resulted in seven Southern states declaring their [[Secession in the United States|secession]] from the Union. Despite this, most Americans believed the Union could still be saved. Burnett, however, disagreed. In the January 7, 1861 issue of Paducah's ''Tri-Weekly Herald'', he declared, "There is not the slightest hope of any settlement or adjustment of existing troubles."<ref name=craighcb266>Craig, "Henry C. Burnett", p. 266</ref> Despite his pessimism, Burnett endorsed the ill-fated [[Peace Conference of 1861]].<ref>Craig, "Henry C. Burnett", pp. 266–267</ref>

Following the rapid secessions of [[Mississippi]], [[Florida]], [[Alabama]], [[Georgia (U.S. state)|Georgia]], [[Louisiana]], and [[Texas]], Congress began preparing the nation for war, including by strengthening the [[United States Army|army]] and [[United States Navy|navy]] and raising funds for the treasury. Burnett attempted to circumvent these measures by proposing an amendment stipulating that none of these new appropriations could be used to subdue or make war against any of the southern states, but the amendment was defeated.<ref>Craig, "Henry C. Burnett", pp. 267–268</ref>

[[File:Hon. Burnett - NARA - 528754.jpg|thumb|Rep. Henry Cornelius Burnett]]
To avert war then, the [[Kentucky General Assembly]] called for a meeting of [[Border states (American Civil War)|border states]] to convene in [[Frankfort, Kentucky|Frankfort]] on May 27. Kentucky's twelve delegates to the convention were to be chosen by special election on May 4. However, after the Confederates fired on [[Fort Sumter]] on April 12, the secessionist candidates withdrew from the election. Expressing the view of the majority of these delegates, Burnett opined in the ''Tri-Weekly Herald'' that the convention would not occur. He was wrong; the convention was held as scheduled, but it failed to accomplish anything of significance.<ref>Craig, "Henry C. Burnett", pp. 269</ref>

==Special congressional elections of 1861==
President Lincoln called for special congressional elections to be held in Kentucky in June 1861. The voters of the First District's Southern Rights party called a meeting to be held May 29, 1861 at the [[Graves County, Kentucky|Graves County]] courthouse in [[Mayfield, Kentucky|Mayfield]]. The purpose of the meeting was ostensibly to re-nominate Burnett for his congressional seat, but some Unionists believed an ulterior motive was in play. [[George D. Prentice]], editor of the Unionist ''Louisville Journal'', wrote on May 21, 1861 that "the object of [the Mayfield Convention], though not officially explained, is believed to be the separation of the First District from Kentucky if Kentucky remains in the Union, and its annexation to [[Tennessee]]".<ref>Craig, "The Jackson Purchase Considers Secession" pp. 344–345, 348</ref>

[[File:Henry C. Burnett.jpg|thumb|left]]
Most of the records of the Mayfield Convention were lost, presumably in a fire that destroyed the courthouse in 1864. The most extensive surviving record comes from the notes of James Beadles, a Unionist observer of the proceedings. After a number of speeches were delivered, a majority committee chaired by Paducah circuit judge James Campbell presented a report containing seven resolutions. The resolutions declared the region's sympathy with the South, although it pledged to abide by Kentucky's present policy of neutrality. It condemned President Lincoln for waging an unjust war, and praised [[Governor of Kentucky|Governor]] [[Beriah Magoffin]] for refusing Lincoln's call for troops. The report also condemned the federal government for arming Union sympathizers in the state with so-called "Lincoln guns". A minority committee report was given by [[Ballard County, Kentucky|Ballard County]] resident and future U.S. Representative [[Oscar Turner (1867)|Oscar Turner]]. This report called Kentucky's neutrality "futile" and "cowardly," promised to fight off any invasion by the North, and recommended calling for aid from Tennessee and the Confederate States in the event of such an invasion. It further warned that if the entire state did not adopt this position, the Purchase region would secede and align itself with Tennessee.<ref>Craig, "The Jackson Purchase Considers Secession" pp. 347–352</ref>

Burnett, along with [[Lyon County, Kentucky|Lyon County]]'s [[Willis B. Machen]] and [[Union County, Kentucky|Union County]]'s Benjamin P. Cissell, initially endorsed Campbell's majority report. After some debate, Burnett proposed four resolutions in lieu of both reports. The resolutions condemned President Lincoln for the war against the South and the federal government for the provision of the "Lincoln guns". They also praised Governor Magoffin for rebuffing Lincoln's call for troops and encouraged him to drive away any Union invasion of the state. Burnett's resolutions were passed by large margins in preference to both the majority and minority reports.<ref>Craig, "The Jackson Purchase Considers Secession" pp. 352–353</ref>

Finally, the convention turned to the issue of nominating Burnett. Four others, including Turner, Machen, and Cissell, were also offered as nominees. Burnett received 124 of 155 votes on the first ballot and was chosen unanimously on the second ballot. In his acceptance speech, Burnett declared that he was undecided as to whether he would take the [[Oath of office#United States|oath of office]] if elected. This statement alluded to an earlier comment by Turner that "no man who is engaged in the cause of the South could go to Congress and take the oath of office without [[perjury|perjuring]] himself."<ref name=craigtjp353>Craig, "The Jackson Purchase Considers Secession" p. 353</ref> Burnett promised that if he did assume his seat, he was determined to arraign President Lincoln for treason.<ref name=craigtjp353 />

In the special elections, Burnett defeated [[Lawrence Trimble]] of Paducah. He was the only [[states' rights]] candidate elected in the statewide canvass. He won handily in the Jackson Purchase region, which was by far the most pro-Southern area of the state. However, outside the Purchase, he won only his home county of Trigg, and that by a slim margin of 20 votes.<ref>Craig, "Henry C. Burnett", p. 270</ref> (Besides the Purchase counties, the First District also included [[Caldwell County, Kentucky|Caldwell]], [[Crittenden County, Kentucky|Crittenden]], [[Hopkins County, Kentucky|Hopkins]], [[Livingston County, Kentucky|Livingston]], Lyon, Trigg, Union, and [[Webster County, Kentucky|Webster]] counties.)<ref name="craighcb266"/>

Burnett took his seat in the [[37th United States Congress|37th Congress]]; sources make no mention of his making good on his threat not to take the oath of office. Just days after the [[First Battle of Bull Run]], Burnett's fellow Kentuckian, [[John J. Crittenden]] proposed a resolution blaming the war on the disloyal Southerners and defining the war's aim as preservation of the Union without interference in the rights or institutions of the states. Burnett asked that the question be [[division (vote)|divided]]. His request was granted, but he only found one colleague willing to vote with him against blaming Southerners for the war.<ref>Rawley, p. 59</ref>

==Confederate military service and expulsion==
After Congress adjourned on August 6, 1861, Burnett returned home to Cadiz and spoke at a number of pro-Southern rallies. On September 4, 1861, Confederate [[Major general (United States)|Major General]] [[Leonidas Polk]] violated Kentucky's neutrality by ordering [[Brigadier general (United States)|Brigadier General]] [[Gideon Johnson Pillow]] to occupy [[Columbus, Kentucky|Columbus]].<ref>Harrison, ''The Civil War in Kentucky'', p. 12</ref> In response, [[Ulysses S. Grant]] captured Paducah on September 6, 1861.<ref>Harrison, ''The Civil War in Kentucky'', p. 13</ref> With neutrality no longer a tenable option, Burnett presided over a conference of Kentucky's Southern sympathizers that occurred at Russellville between October 29 and October 31, 1861. The self-appointed delegates to this conference called for a sovereignty convention on November 18, 1861 for the purpose of establishing a Confederate government for the state.<ref>Craig, "Henry C. Burnett", pp. 271–272</ref>

In the interim between the two conventions, Burnett traveled to Hopkinsville, where he and [[Colonel]] [[W.C.P. Breckinridge]] raised a Confederate regiment dubbed the [[8th Kentucky Infantry]].<ref name=kerr>Kerr, p. 68</ref><ref name=craighcb272>Craig, "Henry C. Burnett", p. 272</ref> On November 11, 1861, Burnett himself enlisted in the [[Confederate States Army]] at Camp Alcorn; he was chosen as colonel of the 8th Kentucky, but never took command.<ref name=trigg /><ref name=craighcb272 />

[[File:Clark House Russellville KY.png|thumb|left|The William Forst House in Russellville]]
The sovereignty convention gathered at the [[William Forst House]] in Russellville as scheduled on November 18, 1861. Burnett also presided over this convention.<ref name=congbio /> Fearing for the safety of the delegates, he first proposed postponing proceedings until January 8, 1862, but [[Scott County, Kentucky|Scott County]]'s [[George W. Johnson (governor)|George W. Johnson]] convinced the majority of the delegates to continue.<ref name=register13>Harrison in ''Register'', p. 13</ref> By the third day, the military situation was so tenuous that the entire convention had to be moved to a tower on the campus of [[Bethel College (Kentucky)|Bethel Female College]], a now-defunct institution in Russellville.<ref>Milliken, p. 222</ref>

The convention passed an ordinance of secession and established a provisional Confederate government for Kentucky.<ref>Harrison, ''The Civil War in Kentucky'', pp. 20–22</ref> Burnett, [[William Preston (Kentucky)|William Preston]] of [[Fayette County, Kentucky|Fayette County]] and [[William E. Simms]] of [[Bourbon County, Kentucky|Bourbon County]] were chosen as commissioners for the provisional government and were dispatched to Richmond, Virginia to negotiate with Confederate President [[Jefferson Davis]] to secure Kentucky's admission to the Confederacy.<ref name=harrison22>Harrison, ''The Civil War in Kentucky'', p. 22</ref> For reasons unexplained by the delegates, Dr. [[Luke P. Blackburn]], a native Kentuckian living in [[Mississippi]], was invited to accompany the commissioners.<ref name=brown85>Brown, p. 85</ref> Despite the fact that Kentucky's elected government in [[Frankfort, Kentucky|Frankfort]] had opposed secession, the commissioners convinced Davis to recommend Kentucky's admission to the Confederacy; the Confederate Congress officially admitted Kentucky on December 10, 1861.<ref name=harrison22 />

Following his successful mission to Richmond, Burnett joined the 8th Kentucky at [[Fort Donelson]].<ref name=craighcb273>Craig, "Henry C. Burnett", p. 273</ref> On February 16, 1862, [[Ulysses S. Grant]] led a combined Federal army-navy attack against the fort.<ref name=craighcb273 /> Most of the Confederate garrison was captured, including the 8th Kentucky, but Burnett escaped in [[General officer|General]] [[John B. Floyd]]'s retreat following the defeat.<ref name=kye /><ref name=johnson /> This battle ended Burnett's military service.<ref name=craighcb273 />

Burnett's subversive activities did not go unnoticed by his colleagues in Congress. He was absent when the body reconvened December 2, 1861. The following day, [[Indiana]] representative [[William M. Dunn|W. McKee Dunn]] introduced a resolution to expel Burnett from Congress. The resolution passed easily, removing Burnett from the seat he had occupied continuously since 1855.<ref name=craighcb273 />

==Confederate political service==
Burnett represented Kentucky in the Provisional Confederate Congress from November 18, 1861 to February 17, 1862, and served as a member of that body's Finance Committee.<ref name=congbio /><ref name=kdla>''Kentucky Members of the Confederate Congress (1861–1862)''</ref> He was then elected as a senator to the [[First Confederate Congress|First]] and [[Second Confederate Congress]]es, serving from February 19, 1862 to February 18, 1865.<ref name=congbio /> In the Confederate Senate, he served on the Engrossment and Enrollment and Military Affairs Committees.<ref name=kdla />

On March 29, Confederate president [[Jefferson Davis]] called on the Confederate Congress to pass a [[conscription]] bill. The bill would require a three-year term of service for all able-bodied white men between the ages of 18 and 35. At first, the bill was unpopular, but as the military situation grew more desperate for the Confederacy, both houses quickly passed it. Still, the measure caused some to question Davis' military decisions; among them was Burnett, usually one of Davis' staunchest allies. In an April 19, 1862 address to the legislature, Burnett denounced Davis' preference for those who were, like Davis himself, graduates of [[United States Military Academy|West Point]]. The speech drew such a vigorous positive response from the gallery that some of the most zealous had to be removed.<ref>Walther, pp. 339–340</ref>

Following the conclusion of the Civil War, Burnett sought an audience with President [[Andrew Johnson]], an old congressional colleague, but Johnson told him to go home.<ref name=craighcb274>Craig, "Henry C. Burnett", p. 274</ref> Burnett was indicted for treason at Louisville, but released on bond and never prosecuted.<ref name=craighcb274 /> He partnered with Judge John R. Grace and resumed the practice of law in Cadiz.<ref name=kerr /><ref name=craighcb274 /> He died of cholera in Hopkinsville on September 28, 1866.<ref name=kye /> Initially buried in the Old Cadiz Cemetery, he was moved to the East End Cemetery in Cadiz.<ref name=trigg /> His tombstone bears no mention of his Confederate service.<ref name=craighcb274 />

==References==
{{Reflist|30em}}

==Bibliography==
*{{cite book |last=Allen |first=William B. |title=A History of Kentucky: Embracing Gleanings, Reminiscences, Antiquities, Natural Curiosities, Statistics, and Biographical Sketches of Pioneers, Soldiers, Jurists, Lawyers, Statesmen, Divines, Mechanics, Farmers, Merchants, and Other Leading Men, of All Occupations and Pursuits |publisher=Bradley & Gilbert |year=1872 |url=https://books.google.com/?id=s_wTAAAAYAAJ |accessdate=2008-11-10 |page=279}}
*{{cite book |title=The Civil War in Kentucky: Battle for the Bluegrass |editor=Kent Masterson Brown |publisher=Savas Publishing Company |location=[[Mason City, Iowa]] |year=2000 |isbn=1-882810-47-3}}
*{{cite web |title=Burnett, Henry Cornelius, (1825–1866) |publisher=United States Congress |url=http://bioguide.congress.gov/scripts/biodisplay.pl?index=B001120 |accessdate=2008-11-10}}
*{{cite web |title=Camp Burnett, Kentucky |url=http://www.nps.gov/archive/vick/camptrail/sites/Kentucky-sites/CampBurnettKY.htm |publisher=National Park Service |accessdate=2008-11-10}}
*{{cite journal |last=Craig |first=Berry F. |title=Henry C. Burnett: Champion of Southern Rights |journal=The Register of the Kentucky Historical Society |volume=77 |date=August 1979 |pages=266–274}}
*{{cite book |last=Craig |first=Berry F. |editor=Kleber, John E. |others=Associate editors: [[Thomas D. Clark]], Lowell H. Harrison, and [[James C. Klotter]] |title=The Kentucky Encyclopedia |year=1992 |publisher=The University Press of Kentucky |location=[[Lexington, Kentucky]] |isbn=0-8131-1772-0 |chapter=Burnett, Henry C.}}
*{{cite journal |last=Craig |first=Berry F. |title=The Jackson Purchase Considers Secession: The 1861 Mayfield Convention |journal=The Register of the Kentucky Historical Society |volume=99 |issue=4 |date=Autumn 2001 |pages=339–361}}
*{{cite book |last=Harrison |first=Lowell H. |authorlink=Lowell H. Harrison |title=The Civil War in Kentucky |publisher=The University Press of Kentucky |location=[[Lexington, Kentucky]] |year=1975 |isbn=0-8131-0209-X |url=https://books.google.com/?id=TrpoH5NAH0QC}}
*{{cite journal |last=Harrison |first=Lowell Hayes |authorlink=Lowell H. Harrison |title=George W. Johnson and Richard Hawes: The Governors of Confederate Kentucky |journal=The Register of the Kentucky Historical Society |date=Winter 1981 |volume=79 |issue=1 |pages=3–39}}
*{{cite book |last=Johnson |first=E. Polk |title=A History of Kentucky and Kentuckians: The Leaders and Representative Men in Commerce, Industry and Modern Activities |publisher= Lewis Publishing Company |year=1912 |url=https://books.google.com/?id=FXQUAAAAYAAJ |accessdate=2008-11-10 |pages=614–615}}
*{{cite web |title=Kentucky Members of the Confederate Congress (1861–1862) |url=http://www.kdla.ky.gov/resources/kyconfedcongress.htm |publisher=Kentucky Department of Libraries and Archives |accessdate=2008-11-10}}
*{{cite book |last=Kerr |first=Charles |author2=William Elsey Connelley |author3=Ellis Merton Coulter  |title=History of Kentucky |publisher=The American Historical Society |year=1922 |url=https://books.google.com/?id=L_kTAAAAYAAJ |accessdate=2008-11-10 |page=68}}
*{{cite web |title=Members of Congress Expelled From House |publisher=FOX News |date=2002-07-25 |url=http://www.foxnews.com/story/0,2933,58683,00.html |accessdate=2008-11-10}}
*{{cite book |last=Milliken |first=Rena |editor=Kleber, John E. |others=Associate editors: [[Thomas D. Clark]], Lowell H. Harrison, and [[James C. Klotter]] |title=The Kentucky Encyclopedia |year=1992 |publisher=The University Press of Kentucky |location=[[Lexington, Kentucky]] |isbn=0-8131-1772-0 |chapter=Confederate State Government}}
*{{cite book |last=Perrin |first=William Henry |title=Counties of Christian and Trigg, Kentucky : historical and biographical |publisher=F.A. Battey Publishing Company |year=1884}}
*{{cite book |last=Rawley |first=James A. |title=Turning Points of the Civil War |publisher=Lincoln University of Nebraska Press |year=1989 |isbn=978-0-8032-8935-2}}
*{{cite book |title=Trigg County, Kentucky Veterans |publisher=Turner Publishing Company |year=2002 |isbn=1-56311-837-8 |url=https://books.google.com/?id=Qzu82QxUn1AC |accessdate=2008-11-10 |page=22}}
*{{cite book |last=Walther |first=Eric H. |title=William Lowndes Yancey and the Coming of the Civil War |publisher=Chapel Hill University of North Carolina Press |year=2006 |isbn=978-0-8078-3027-7}}

==External links==
{{Portal|Biography}}

{{s-start}}
{{s-par|us-hs}}
{{s-bef|before=[[Linn Boyd]]}}
{{s-ttl|title=Member of the [[List of United States Representatives from Kentucky|U.S. House of Representatives]]<br>from [[Kentucky's 1st congressional district]]|years=1855–1861}}
{{s-aft|after=[[Samuel L. Casey|Samuel Casey]]}}
|-
{{s-par|cs-sen}}
{{s-new|constituency}}
{{s-ttl|title=[[List of members of the Confederate Senate|Confederate States Senator (Class 3) from Kentucky]]|years=1862–1865|alongside=[[William E. Simms|William Simms]]}}
{{s-non|reason=Constituency abolished}}
{{s-end}}
{{USCongRep-start|congresses= 34th–37th [[United States Congress]] |state=[[Kentucky]]}}
{{USCongRep/KY/34}}
{{USCongRep/KY/35}}
{{USCongRep/KY/36}}
{{USCongRep/KY/37}}
{{USCongRep-end}}
{{CSSenators}}
{{Authority control}}<!--LC gives heading as Burnett, H. C. (Henry Clay), 1825-1866 with a variant form for Henry Cornelius; this is the correct heading-->

{{DEFAULTSORT:Burnett, Henry C.}}
[[Category:1825 births]]
[[Category:1866 deaths]]
[[Category:American Disciples of Christ]]
[[Category:American members of the Churches of Christ]]
[[Category:Confederate States Army officers]]
[[Category:Confederate States Senators]]
[[Category:Deaths from cholera]]
[[Category:Deputies and delegates of the Provisional Confederate Congress]]
[[Category:Expelled members of the United States House of Representatives]]
[[Category:Kentucky lawyers]]
[[Category:Members of the United States House of Representatives from Kentucky]]
[[Category:People from Essex County, Virginia]]
[[Category:People of Kentucky in the American Civil War]]
[[Category:Kentucky Democrats]]
[[Category:Democratic Party members of the United States House of Representatives]]
[[Category:19th-century American politicians]]