{{Infobox Dogbreed 
<!-- Put article text AFTER this infobox markup. See: -->
<!-- Wikipedia:WikiProject Dog breeds/Templates for more info.-->
| image=Typical Lucas Terriers.JPG
| image_caption = Typical Lucas Terriers
| name = Lucas Terriers
| country = [[England]]
|coat         = Fairly harsh, double, medium length
|color        = Tan or with combinations of tan, black, white or grey
|life_span    = 12–17 years
| fcigroup = 
| fcisection = 
| fcinum = 
| fcistd = 
| akcgroup = 
| akcstd = 
| ankcgroup = 
| ankcstd = 
| ckcgroup = 
| ckcstd = 
| kcukgroup = 
| kcukstd = 
| nzkcgroup = 
| nzkcstd = 
| ukcgroup = Terriers
| ukcstd = 
}}<!-- End Infobox -->

The '''Lucas Terrier''' is a small [[Dog breed|breed]] of [[dog]] of the terrier [[Dog type|type]] which originated in England in the late 1940s.<ref>{{cite journal|title=The Countryman's Weekly|date=10 April 1998}}</ref>  The breed was named by [[Sir Jocelyn Lucas, 4th Baronet]] and all living Lucas Terriers in the UK can trace their ancestry back to a small number of his original Lucas Terriers.<ref>{{cite web|title=Lucas Terrier Club Website FAQs|url=http://www.lucasterrierclub.co.uk/faqs-1.php}}</ref>

== Appearance ==

It is a sturdy, symmetrically built, working terrier like an old-fashioned Ilmer Sealyham type,<ref>{{cite book|title=Terriers - Their Training, Working & Management|isbn=978-1-4437-9699-6}}</ref>  created only by crossing an existing Lucas Terrier with a [[Norfolk Terrier]] or with a small [[Sealyham Terrier]].  A latter-day Norfolk Terrier mated with a latter-day Sealyham Terrier does not produce a Lucas Terrier because it will not trace back to the original Lucas Terrier lines.  All true Lucas Terriers have a direct connection to Sir Jocelyn Lucas's original Ilmer Kennel based Lucas Terriers.<ref>{{cite book|title=The Sealyham Terrier|publisher=Lightning Source UK Ltd|isbn=978-1-4437-8168-8}}</ref>   The Lucas Terrier is bred primarily for temperament<ref>{{cite journal|journal=Our Dogs (Christmas Number Supplement 1961)|year=1961}}</ref>  and companionship, and so should be friendly, with no aggressive tendencies towards people or other dogs, and not fearful or nervous while retaining the usual terrier traits.  The coat should be fairly harsh, weather resistant and of medium length.  It may be coloured or white, but the majority are tan, black and tan or saddle and tan.  Dogs should weigh 14–20&nbsp;lbs (6–9&nbsp;kg) and bitches 11–17&nbsp;lbs (5–8&nbsp;kg).

== History ==

[[Sir Jocelyn Lucas, 4th Baronet]], a long-time Sealyham breeder in the first half of the 20th Century, bred the first cross between a Norfolk Terrier and one of his own Ilmer Sealyhams.  Ilmer Sealyhams were smaller than the mainstream show Sealyhams of the time.<ref>{{cite journal|title=Dog World Annual|year=1967}}</ref>   He found the mix produced an intelligent and feisty dog with an excellent temperament, suitable for use as a working terrier but equally happy as a domestic pet, and so named the cross after himself.  Several distinct lines of Lucas Terrier were bred from the late 1940s, and these are still unbroken today.

In later years his extensive kennels were managed by the Hon Mrs Enid Plummer, and she ran the breeding programme for him as he became older and less involved.<ref>{{cite web|title=Enid Plummer|url=http://www.lucasterrierclub.co.uk/enid-plummer.html}}</ref>   Sir Jocelyn died in 1980.<ref>{{cite web|title=Sir Jocelyn Lucas|url=http://www.lucasterrierclub.co.uk/sir-jocelyn-lucas.html}}</ref>   Just prior to that, in the late 1970s, Enid Plummer moved to Cornwall with some remaining Lucas Terriers and continued breeding there until she died in 1986.

Miss Jumbo Frost took on the task of growing the breed from Enid Plummer, who she knew well, and her dedication, foresight and personal generosity carried Lucas Terriers through until she too died in 2009.<ref>{{cite web|title=Jumbo Frost|url=http://www.lucasterrierclub.co.uk/jumbo-frost.html}}</ref>   During that time she oversaw a transformation in the fortunes of the breed, formalised the breeding by establishing the Lucas Terrier Club, was instrumental in widening the gene pool and in setting a number of standards which are still followed today.

== The Original Club ==

The Lucas Terrier Club (LTC) is an informally run private organisation developed for the purposes of preserving and promoting the Lucas Terrier and to offer assistance to breeders in sourcing registered Lucas Terriers or dogs of parent breeds for the continuance of the breed.<ref>{{cite web|title=The Lucas Terrier Club|url=http://www.lucasterrierclub.co.uk/about-us.html}}</ref>  The Club holds and maintains a register of more than 700 past and present Lucas Terriers.

There are now Lucas Terriers in a number of other countries around the world.

== Other Clubs ==

In 1999 a breakaway Club, known as the [[Sporting Lucas Terrier]] Club,<ref>{{cite news|title=Country Life|newspaper=Country Life|date=21 October 1999}}</ref>  was formed to register and promote a terrier which has a degree of Lucas Terrier in its make-up but also has genes from other breeds.  In 2003 a separate Sporting Lucas Terrier Association was also formed.  The fact that these terrier clubs breed a type of dog with a very similar name to the original Lucas Terrier has created considerable confusion, but all three clubs co-exist perfectly happily while promoting distinctly different attributes in their dogs.  In North America the Lucas Terrier Club of America was established in 2006 http://www.lucasterrier.com  A separate American Lucas Terrier Society was formed in 2012.

== External links ==
{{commons category}}

The Lucas Terrier Club (formally established as a club in 1988) has by far the largest direct dog and breeder links dating back to those bred by Sir Jocelyn Lucas since the late 1940s.<ref>{{cite journal|title=I'm a Lucas Terrier, can you save me?|journal=Country Life|date=6 November 2013}}</ref>   The Club's website is http://www.lucasterrierclub.co.uk
* [http://www.lucasterrierclub.co.uk/ Lucas Terrier Club] (established 1988)
* [http://thesportinglucasterrierclub.com/ Sporting Lucas Terrier Club] (established 1999)
* [http://www.sportinglucasterrierassociation.co.uk/ Sporting Lucas Terrier Association] (established 2003)
* [http://lucasterrier.com/ Lucas Terrier Club of America] (established 2006)
* [http://www.americanlucasterriersociety.com/ American Lucas Terrier Society] (established 2012)

== References ==

{{reflist}}

[[Category:Dog breeds originating in England]]
[[Category:Terriers]]