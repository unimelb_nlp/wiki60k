{{Unreferenced|date=March 2015}}
{{Infobox school
|name                   = Donna High School
|motto                  = 
|streetaddress          = 1/4 Mile East Wood Avenue
|city                   = [[Donna, Texas|Donna]]
|state                  = [[Texas]]
|zipcode                = 78537
|country                = USA
|coordinates            = {{Coord|26|10|19.3|N|98|2|12.3|W|display=inline,title}}
|schooltype             = high school
|fundingtype            = [[State school|Public]]
|established            = 1911
|district               = [[Donna Independent School District]]
|superintendent         = Jesus Rene Reyna
|principal              = Nancy Lopez Castillo
|dean                   = Jessica Carrizales<br>Lisa McCandless
|grades                 = 9-12
|enrollment             = 1932
|enrollment_as_of       = September 2013
|language               = [[English language|English]]
|colors                 = Maroon and Gold {{Color box|maroon|border=darkgray}} {{Color box|gold|border=darkgray}}
|athletics              = 
|conference             = [[University Interscholastic League|UIL]] 30-5A
|song                   = "The Redskin Spirit" by Noe Caceres Boghs, Class of 1955
|fightsong              = "The Redskin Fight Song" sung to the tune of Go, You Northwestern
|nickname               = [[Redskin (slang)|Redskin]]s and Bravettes
|newspaper              = The Tomahawk
|yearbook               = The Warwhoop
|communities            = [[Donna, Texas]] and eastern [[Alamo, Texas]]
|feeders                = A.P. Solis Middle School W.A. Todd Middle School Sauceda MS Veteran MS
|homepage               = {{URL|dhs.donnaisd.net|Official website}}
}}

'''Donna High School''' is a 9th thru 12th grade campus in the [[Donna Independent School District]] located in [[Donna, Texas]]. It is located in the center of the [[Rio Grande Valley]].

== A brief history ==
The following information was adapted from the Barbara Edwards 1986 “ Donna School System” historical research report for the [[Hidalgo County Historical Commission]],“The History of the Indian Sweetheart” by Art Del Barrio, and the [[Handbook of Texas]]. 

In 1904 a long two roomed school building, the forerunner of the Donna school system, was built at [[Runn]]; located south of Donna. This building, known briefly as the “Hester School”, was equipped with desks and a large fireplace, first housed twenty-two students and one teacher. A disastrous flood in 1909 some years later damaged the building to such an extent that it was abandoned.  Later in 1910, a building would later be built near the  former “Hester School” site which is now part of [[Runn]] Elementary. 
This event caused most of the original settlers (Hesters, Ruthvens, Norwoods, Champions, Holloways, Vertrees and others) to move to present day Donna.
Donna students first attended classes on a central campus in the fall of 1911. A new building, constructed of yellow brick, housed all students from the first grade through high school. In 1913 the first class was graduated from this school.
Enrollment increased rapidly during those early years. The first Donna High School was completed and occupied in the fall of 1919. 

'''Donna High School No.1 (1919–1955)'''
In 1919, a three-story high school was opened on the corner of Eleventh Street and Hester across from the First Baptist Church. This complex stood where the Moye Elementary parking lot now stands. In addition, to regular classrooms, this school had a home economics lab with a separate dining room boasting of [[sterling silver]] flatware and Haviland [[china]], a manual training department and an eight piece orchestra, the foundation of today’s band. 
This newly constructed high school building continued to serve the needs of Donna students for a period of thirty-six years until 1955, when it was torn after a new high school was built. The roof of this school was blown off by Hurricane No. 11 in 1933 and many of the school  records were lost. 
The 1933 hurricane financially devastated the  school  that there was no money for extracurricular activities. As a result, the  Donna High School Junior Class of 1934-35 originated the  Indian Sweetheart Contest to raise money for the junior-senior banquet [[prom]].  Little did the students know that they were creating Donna High School’s longest and most respected [[tradition]] and honor for a female student which continues today.

'''''The Donna-[[Weslaco High School|Weslaco]] [[List of sports rivalries|rivalry]]'''''
On January 20, 1921, a motion was passed by the Donna Board of Trustees to establish the [[Weslaco Independent School District]] and students from the [[Weslaco, Texas|Weslaco]] area were separated from the Donna district to attend their respective campus. 
It was from this time of 1923 that the intense [[High school football|football]] rivalry between Weslaco and Donna began. Members of the Weslaco team, former Donna Redskins, plowed their own football field. When their former classmates arrived to play the game, they looked on plowed field instead of turf. The now Weslaco Panthers had the advantage of practicing on the plowed surface so they were able to win the game.
The Donna-Weslaco game for many years was played in the afternoon of [[Armistice Day]], November 11. It was one of the largest attended games throughout the [[Rio Grande Valley]] with people coming from throughout the area. In the earlier years, the hometown band and drill team would host the visitors with lunch served in the individual homes. The half-time fest ivies were shared by the schools and it usually was an elaborate entertainment for capacity crowds. It was a school holiday for both schools. The rivalry has been placed on hiatus since 2007 and will possibly be reinstated in 2012.

During [[World War II]] years, there was much patriotic activity. The students became involved in selling [[victory stamps]] and [[savings bonds]], collecting scrap metal, the band performed victory concerts, a [[Victory Corps]] was formed, the [[Fourth War Loan]] drive was supported and the inevitable memorial service was conducted.
Just prior to the war, on October 11, 1940, Donna High school organized its first [[Student Council]].  Some of the original charter members were Hazel Hart (Anderson), Jose Zuniga, Dorothea Gray (Wilkerson), Isaac Avila, and Gordon Wood.

'''Donna High School, No.2 (1955–1967)'''
A new high school was built on the southwest corner of the original school campus on Main Street. The students and faculty moved into this school on January 23. 1956 and it was dedicated on February 3, 1956. The building contained twenty-four classrooms with three central wings and a central office area. There was also a wing that included the library, superintendent’s office and the tax office.
In 1961, the Donna Redskins won the Class AA State Football Championship defeating the [[Quanah, Texas|Quanah]] Indians.  As of 2016 Donna remains the only Rio Grande Valley area high school to win a state championship.
The 1960s again saw Donna schools in an enlargement plan. Sixty acres of land had been purchased on FM 493 and again Donna undertook construction of a new high school.

'''Donna High School, No.3 (1967–1983)'''
This new high school opened in 1967 about the time the disastrous [[Hurricane Beulah]] struck. Wooden planks were put out for weeks for the students and teachers to enter the school area. This new school was the first totally air-conditioned school in the school district. There were twenty-four classrooms in the original building. Four classrooms were later added to west side of the former gym. In 1981, a cafeteria and additional classrooms were added. This school has fifty-five classrooms and currently serves as the Todd Middle School.

'''Donna High School, (No.4 1983– present)'''
March 1983 was the opening of  our present-day high school on ten acres of land on Wood Avenue. Early additions included a second gym, a career and technology building, a new cafeteria/classrooms and a west wing. In 1997, a new two story library and a 1,400 seat auditorium and performing arts center was added.  In 2009, a new 4 million state-of the-art Science building was added to the high school. 
Plans are underway for the creation of a second separate high school in 2014.

== Traditions and spirit ==
'''The Indian Sweetheart'''
This long-standing tradition started in 1934 after [[Hurricane No. 11]] in 1933 struck in an effort to raise morale and community spirit. In order to secure funds for the Junior-Senior Prom, an “Indian Sweetheart” contest was initiated to raise money. The class-nominated [[candidate]] receiving the most votes and pennies was declared Indian Sweetheart. This [[penny]] selection method was used from 1934 thru 1941. 
During [[World War II]], the money collected was no longer used for the Junior-Senior [[prom]], but would be used to help out the war effort. The class-nominated candidate whose respective class sold the most [[war bonds]] and stamps was declared Indian Sweetheart. The [[war bond]] raising selection method was used from 1942-1945.

In 1943, Mary Holloway was crowned Indian Sweetheart for the second consecutive year and remains the only lady to hold the title twice.
In 1945, Chrystelle Roberson held a [[Class reunion|reunion]] of the past Indian Sweethearts and unknowingly the tradition of the [[headdress]] began. Betty Maxine Farnsworth, the first Indian Sweetheart presented Chrystelle with a [[headdress]], created by Doris Jewell, which has become the symbol of the Indian Sweetheart.
In 1950, Gerry Jean Ennis, a [[polio]] and bed-stricken student became the only unanimously elected Indian Sweetheart in school history.
After the war years, the Indian Sweetheart became a vote among the classes.  Two junior and two senior girls were nominated by the [[Student Council]] to run in an election in which the entire student body would elect an Indian Sweetheart.
In 1961, the Class of 1949 donated an arm band and leg band to the Indian Sweetheart in memory of Barbara Lannart, 14th Indian Sweetheart who died in a car accident along with her husband and children.
In 1986, a [[protest]] from the students resulted in a change that allowed for any upcoming senior girl to run.
Today, any junior girl who meets the criteria specified in the Indian Sweetheart [[Constitution]] and [[Bylaws]],  can run for the honor. If elected, she is presented in an elaborate half-time ceremony with the Redskin [[School band|Band]] performing traditional music during the first home football game of her senior year.  After the game, a school dance is held in her honor by the [[Cheerleaders]].
She also becomes an honorary member of the Varsity [[Cheerleading]] Squad and an honorary [[Homecoming]] Duchess. During the year she serves as the ceremonial role model figurehead for the student body and the [[Donna, Texas|Donna]] community.

'''The Varsity Night Uniform'''
This Indian-maiden inspired uniform tradition began in 1948. The uniform is hand stitched and has virtually remained the same all these years albeit changes in skirt length. Along with this uniform, the girls wear [[moccasins]] all night long to complete this ensemble.
Only Varsity Cheerleaders are allowed to wear this uniform on Friday nights.  Until the late 1970s, there were only 4 cheerleaders and 1 Indian Sweetheart to spell out D-O-N-N-A.
Donna Varsity Cheerleaders do own a traditional high school outfit which is only used at the pep-rallies.
The Varsity Cheerleaders and Donna community take great pride in their outfit and its symbolic ties to Donna’s rich past which have become permanent Friday night sideline fixture.

'''The Sprinkling of the Dirt'''
The tradition of the “Sprinkling of the Dirt” began in the late 1940s according to Betty Lou Blackburn Scott, 13th Indian Sweetheart-1947. It started with a couple of superstitious football players that would bring Donna dirt to away games so they could always play on home soil.
The tradition was picked up the Varsity cheerleaders and continues to this day. So unbeknownst to opposing teams, the Cheerleaders sprinkle Donna dirt on the sideline so that our team never has to play an away game.

==See also==
{{portal|Texas|Schools}}

==External links==
* [http://dhs.donnaisd.net/ Official website]
{{Hidalgo County, Texas Schools}}
[[Category:High schools in Hidalgo County, Texas]]
[[Category:Educational institutions established in 1911]]
[[Category:Public high schools in Texas]]