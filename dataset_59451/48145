{{Refimprove|date=February 2014}}
The '''Environmental Protection Agency, (EPA Ghana)''' is an agency of Ministry of Environment, Science Technology and Innovation, established by EPA Act 490 (1994).<ref>{{cite web|url=http://www.epa.gov.gh |title=Welcome to the Ghana EPA |publisher=Epa.gov.gh |date=2014-02-07 |accessdate=2014-02-13}}</ref> The agency is dedicated to improving, conserving and promoting the country’s environment and striving for environmentally [[sustainable development]] with sound, efficient [[resource management]], taking into account social and equity issues. It oversees the [[implementation]] of the National Environment Policy.<ref>Ministry of Environment Science and Technology, ″National Environmental Policy 2012″, Accra, Ghana.</ref> EPA Ghana's mission is to manage, protect and enhance the country’s environment and seek common solutions to global environmental problems. Its mission is to be achieved through an integrated [[environmental planning]] and management system with broad [[public participation]], efficient implementation of appropriate programmes and technical services, advice on environmental problems and effective, consistent [[enforcement]] of environmental law and regulations. EPA Ghana is a [[regulatory body]] and a catalyst for change to sound environmental stewardship.

The agency began during a time of growing concern about the dangers to the environment from careless human activity, prompting the [[United Nations]] to convene a conference in Stockholm on the environment in June 1972. Guidelines for action were adopted at the conference, including the establishment of the [[United Nations Environment Programme]] (UNEP). The decision to establish the Environment Protection Council was a direct result of the recommendations of the [[Stockholm Conference]]. Before this decision, Ghana was elected by the General Assembly to the Governing Council of 58 nations set up to administer the affairs of the UNEP.

Before the Stockholm conference, Ghana had felt the need for [[environmental protection]] and prepared the ground for a body to deal with environmental matters in the country. Several organizations had begun initiatives in environmental work; the best-known were:
*The Scientific Committee on Problems of the Environment (SCOPE) of the [[Ghana Academy of Arts and Sciences]], established as the local counterpart of the [[Scientific Committee on Problems of the Environment|international body of the same name]]
*The Conservation Committee of the [[Council for Scientific and Industrial Research]] (CSIR)
*The Ghana Working Group on the Environment, an informal group of scientists united by a common concern about environmental matters
*National Committee on the Human Environment, formed by the [[Ministry of Foreign Affairs]] in 1971 as a result of concern expressed by the [[Economic Commission for Africa]] and the [[Organisation of African Unity]] about the need to conserve and protect Africa’s [[natural resource]]s

=={{anchor|History of the Environmental Protection Council}}Environmental Protection Council history==
The Environmental Protection Council (EPC) was established by the [[National Redemption Council]] government<ref>{{cite web|url=http://www.britannica.com/topic/National-Redemption-Council|title=National Redemption Council|website=Encyclopædia Britannica|accessdate=July 24, 2015}}</ref> led by [[Ignatius Kutu Acheampong]].<ref>{{cite web|url=http://www.britannica.com/biography/Ignatius-Kutu-Acheampong|title=Ignatius Kutu Acheampong|website=Encyclopædia Britannica|accessdate=July 24, 2015}}</ref> On 23 May 1973, the Government of the [[National Redemption Council]] announced the establishment of an Environmental Protection Council under Chairmanship of Professor E.A Boateng, first [[vice-chancellor]] of the [[University of Cape Coast]].<ref>{{cite web|url=http://www.ndpc.gov.gh/four-pillars/#environmental|title=Environmental Development|website=National Development Planning Commission|accessdate=July 24, 2015}}</ref>{{Failed verification|date=July 2015}}
On 23 January 1974 the [[head of state]]<ref>{{cite web|url=http://www.archontology.org/nations/ghana |title=Ghana: Archontology |publisher=Archontology.org |date=2009-06-26 |accessdate=2014-02-13}}</ref> signed NRC Decree 239, establishing the Environmental Protection Council. On 4 June, the Environmental Protection Council was established by [[attorney general]] Edward Nathaniel Moore on behalf of the Commissioner of Economic Planning.

==={{anchor|Leadership of Professor E. A. Boateng}}E. A. Boateng===
Easmon A. Boateng, the council's first chair,<ref>Environmental Protection Council ″Annual Report 1974-75″ Accra Ghana</ref> began work in a temporary office at the headquarters of the Council for Scientific and Industrial Research (CSIR) in [[Airport Residential Area]] in [[Accra]]. His secretary was F. K. A. Jiagge and he had ten junior staff, including a stenographer, two clerks, three drivers, a receptionist, a messenger and a night watchman. Two senior staff later joined the EPC: Joyce Aryee<ref>{{cite web|url=http://lindaakrasi.wordpress.com/meet |title=Meet first female CEO of Chamber of Mines &#124; lindaakrasi |publisher=Lindaakrasi.wordpress.com |date=2011-08-18 |accessdate=2014-02-13}}</ref> and Clement Dorme–Adzobu.<ref>{{cite web|url=http://www.exceed.tu-braunschweig.de/2917_prof-clement-dorm-adzobu-l |title=Prof. Clement Dorm-Adzobu launches guest lecture at ISW/TU Braunschweig « EXCEED |publisher=Exceed.tu-braunschweig.de |date=2012-04-17 |accessdate=2014-02-13}}</ref>
The council, made up of 16 members from government, organizations and universities, included E. Lartey (Council for Science and Industrial Research), D. M. Mills (Attorney General's Department), E. G. Beausoleil (Ministry of Health), S. K. P. Kanda (Ministry of Industries), B. K. Nketsia (Ministry of Foreign Affairs), M. Nicholas (Ministry of Agriculture), J. W. Boateng, (Ghana Water and Sewerage Corporation), A. Odjidja (Ghana Tourist Control Board), F.&nbsp;A. A. Acquaah (Meteorological Services Department), J. Bentum-Williams (Ministry of Land and Mineral Resources), P. N. K. Turkson (Ministry of Works and Housing), S. Al-Hassan M. K. Adu Badu (government representatives).<ref>EPC ″Annual Report for 1974-75″ Accra, Ghana.</ref> The EPC was part of the Ministry of Finance and Economic Planning. It moved from its temporary offices at Council for Science and Industrial Research headquarters to the Ministry of Works and Housing and then to Old Parliament House, where it remained until its 1978 move to the Ministry of Environment, Science, Technology and Innovation building. Construction of a permanent council headquarters began at the ministry in 1979, and it was completed in 1994.{{citation needed|date=July 2015}}

==={{anchor|Leadership of Dr B. W. Garbrah}}B. W. Garbrah===
Garbrah was appointed acting executive EPC chair in 1981. The council became part of the Ministry of Health, since it was thought that it would work better aligned with the health sector.

During a 1982 [[drought]],<ref>{{cite web|url=http://www.undp.org/content/dam/undp/library/AADAF2/ghana |accessdate=December 19, 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20140222202601/http://www.undp.org/content/dam/undp/library/AADAF2/ghana |archivedate=February 22, 2014 }}</ref> the council focused its attention on tree-planting and there was a national [[reforestation]] campaign<ref>[ftp://ftp.fao.org/docrep./FAO/004]{{dead link|date=February 2014}}</ref> in the wake of deforestation from 1972 into the 1980s.<ref>EPA (2004), State of Environment Report, Accra, Ghana.</ref> The EPC moved to the Ministry of Local Government and Rural Development to work with district assemblies to ensure environmental sustainability. In 1983, the government introduced a National Bushfire Campaign to minimize the [[wildfire|bushfire]]s occurring throughout the country. The council conducted sensitization programmes, and officers traveled throughout the country to minimise bushfire occurrence in collaboration with district assemblies and the [[Ghana National Fire Service]].

==={{anchor|Leadership of Christine Debrah}}Christine Debrah===
Debrah<ref>{{cite web|url=http://www.iisd.org/wcfsd/wpgbios.htm |title=Commissioner Biographies |publisher=Iisd.org |date=1996-10-02 |accessdate=2014-02-13}}</ref> was appointed executive chair of the EPC in 1985. She opened regional offices to bring [[environmental protection]] closer to the people, particularly in northern Ghana. The Northern Regional Office in Tamale, headed by Edward M. Telly, opened in 1988 and work began on more offices. Debrah emphasised environmental education, contributing to the debate on [[climate change]].<ref>Debrah, Lt. Col. Christine (Rtd), 1989, Address to the Conference on Implications of Climate Change for Africa, Howard University, Washington, DC, USA, 5 May 1989.</ref>
Her staff attended international conferences to deliberate on, and find solutions to, global and national environmental problems. For her contributions, Debrah was listed on the UNEP [[Global 500 Roll of Honour]] and is a member of the Climate Institute's board of advisors.<ref>{{cite web|url=http://www.climate.org/about/advisors.html|title=Board of Advisors|website=Climate Institute|access-date=July 25, 2015|archive-url=https://web.archive.org/web/20071130172015/http://www.climate.org/about/advisors.html|archive-date=November 30, 2007}}</ref>

==={{anchor|Leadership of Franciska Issaka}}Franciska Issaka===
In 1990, Franciska Issaka<ref>{{cite web|url=http://www.ozone.unep.org/meeting_documents |title=Meeting Documents - The Ozone Secretariat |publisher=Ozone.unep.org |date= |accessdate=2014-02-13}}</ref> was appointed acting chair of the EPC. She continued its expansion, employing more staff and in 1991 opening the Upper West Regional Office in [[Wa, Ghana|Wa]] headed by John Pwamang.<ref>{{cite web|url=http://www.thechronicle.com.gh/epa_allays_the_fear_over_random_gas |accessdate=December 19, 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20140222154544/http://www.thechronicle.com.gh/epa_allays_the_fear_over_random_gas |archivedate=February 22, 2014 }}</ref> Issaka ensured that all the regional offices had permanent locations.

The council brought together environmentalists and academicians to draft the 1991 National Environmental Action Plan,<ref>EPC, ″National Environmental Action Plan″, Vol. I, 1991, Accra, Ghana.</ref> which was later adopted by the government. A number of international environmental protocols were ratified at this time, including the [[Convention on Biological Diversity]]<ref>[http://www.cbd.int/doc/world/gh ]{{dead link|date=February 2014}}</ref> (signed in 1992 and ratified in 1994) and the United Nations Framework on Climate Change<ref>[https://web.archive.org/web/20160304041646/http://www.nlcap.net/NCAP/Ghana ]</ref> (signed in 1992). The National Ozone Office<ref>{{cite web|url=http://www.undp-gha.org/design/ourwork |title=UNDP :: United Nations Development Programme :: Official Website |publisher=Undp-gha.org |date= |accessdate=2014-02-13}}</ref> was established as part of the EPC in 1991 to end the import of ozone-depleting substances by the country after Ghana ratified the [[Montreal Protocol]] in 1989.

=={{anchor|Change of Council to Agency}}Council to agency==
[[File:Ghana EPA Head Office.JPG|thumbnail|alt=Tan, five-story building|EPA Ghana head office]]
After the 1992 constitution and national election, the [[National Democratic Congress (Ghana)|National Democratic Congress]] government of [[Jerry John Rawlings]] created the Ministry of Environment, Science and Technology the following year. A national environmental policy was produced, and the council was moved into the ministry to implement it. In June 1993 a five-year Ghana Environmental Resource Management project, sponsored by the [[World Bank]], was begun to ensure staff infrastructure. The agency expanded, and by December 1993 six regional offices were in operation: Western, Volta, Eastern, Ashanti, Northern and Upper West. The following year, three additional regional offices opened: Central, Greater Accra and Brong Ahafo.

On 30 December 1994, the Environmental Protection Council became the Environmental Protection Agency  in Act 490.<ref>{{cite web|url=http://www.epa.gov.gh/web/index.php/legislation/2014-03-14-10-01-43|title=EPA Act|website=Environemental Protection Agency, Ghana|access-date=July 25, 2015}}</ref> The act empowered the agency to legally prosecute environmental offences and sue for breaches of the law. A 13-member management board, headed by [[Arnold Quainoo]], was established.<ref>Roger Goking (2005). ''The History of Ghana'', Green Wood Histories of Modern Nations.</ref>

==={{anchor|Leadership of Dr Peter Acquah}}Peter Acquah===
[[File:Dr. Acquah.JPG|thumbnail|alt=Man standing at a podium in front of microphones|Former executive director Peter Acquah]]
Peter Claver Acquah was appointed acting executive director of EPA Ghana in 1994, and
the agency completed its integrated coastal-zone management strategy<ref>Armah, A. K., D. S. Amlalo (1998). ''Coastal Zone Profile of Ghana''. Ministry of Environment, Science & Technology/Large Marine Ecosystems Project of the [[Gulf of Guinea]]. Vii + 111pp.</ref> for Ghana 1997. In September 1997, the [[United Nations Environment Programme]] gave an "outstanding national ozone unit award" to EPA Ghana in recognition of its efforts in implementing the Montreal Protocol.{{citation needed|date=July 2015}} The Tarkwa Office was opened as part of the Western Regional Office to oversee mining problems in 1999, with Michael Sandow Ali its first head.

That year, a strategic plan was produced<ref>Environmental Protection Agency (1999), Five Year Strategic Plan 1999 to 2003, Accra, Ghana.</ref> to guide the agency's activities. In November 2000, the Capacity Development and Linkages for Environmental Impact Assessment in Africa (CLEIAA) project began.<ref>CLEIAA (2004), Towards Development of a Professional Register of Core [[Environmental Assessment]] Practitioners and specialists is sub Sahara Africa</ref>
Acquah opened the [[Tema]] office, headed by Yaw Safo Afriyie and later by Lambert Faabeloun, in 2001 before his resignation that year.

==={{anchor|Leadership of Jonathan Allotey}}Jonathan Allotey===
Allotey was appointed acting executive director in October 2001 by President John Kufour, whose [[National Patriotic Party]] won the 2000 elections. Allotey,<ref>{{cite web|url=http://www.kiteonline.net/cdmghana |title=CDM Ghana Website!!! |publisher=Kiteonline.net |date= |accessdate=2014-02-13}}</ref> former director of the Regional Programmes Division, was the first staff member to become head of the agency.

The government placed the agency under the Ministry of Local Government and Rural Development. A management board was appointed, chaired by Osagyefo Amoatia Ofori Panyin II<ref>{{cite web|url=http://www.kingosagyefuopanin.wordpress.com/ |title=King Osagyefuo Panin's Visit |publisher=Kingosagyefuopanin.wordpress.com |date= |accessdate=2014-02-13}}</ref> the [[Okyenhene]] of the [[Kibi, Ghana|Kyebi]] traditional area.

In 2001, a school opened in [[Amasaman]] for training national and international experts in [[environmental management]]. A mining reclamation bond<ref>[http://www.gsr.com/corporate/GSR_201109_Mine_Closure_Conference.pdf]{{dead link|date=February 2014}}</ref> was posted, so mining companies do not have to post a bond; if a company fails to reclaim a mine site, funds can be released for [[Mine reclamation|reclamation]].

Allotey led the team which produced a National Action Programme to Combat Drought.<ref>Environmental Protection Agency (2002), National Action Programme to Combat Drought. Accra, Ghana.</ref> The Ghana Technology Transfer Needs Assessment Report, introduced in 2005, was also produced by the agency's climate-change adaptation programme<ref>EPA (2004), Ghana: State of Environment Report, 2004, Accra, Ghana.</ref> which was launched by the Minister in with a team of experts preparing an atlas of the coast.<ref>EPA (2005), Environmental Sensitivity Atlas for the coastal areas of Ghana, Accra, Ghana.</ref>
The agency coordinates the work of the UNESCO [[Man and the Biosphere Programme]] in Ghana. In 2005 Daniel S. Amlalo, the agency's deputy executive director, was elected vice-chairman of the International Coordinating Council of Man and Biosphere.<ref>EPA (2005), Annual Report, 2005, Accra, Ghana.</ref> In May 2009 EPA Ghana hosted the International Association for Impact Assessment conference in Ghana, and Allotey was elected chairman of the association.<ref>[http://www.iaia.org/about/past-board-members]{{dead link|date=February 2014}}</ref> He resigned from the agency the following year.

==={{anchor|Leadership of Daniel Amlalo}}Daniel Amlalo===
[[File:Daniel Amlalo.JPG|thumbnail|alt=Man with glasses, standing at a podium with microphones|Daniel S. Amlalo, current executive director]]
Daniel Amlalo was appointed the agency's acting executive director on 1 December 2010, renovating EPA Ghana's offices and creating more offices at the Millennium Block. In 2011, the second five-year strategic plan was produced for 2011 to 2015.

Ghana’s second national communication under the [[United Nations Framework Convention on Climate Change]] was issued. In 2011, EPA Ghana produced guidelines for the environmental assessment and management of offshore oil and gas development. The government renamed the Ministry of Environment, Science, Technology and Innovation in January 2012 to promote innovation in science. The Cleaner Production Centre<ref>[http://www.alloexpat.com/ghana./government-inaugurates-national-cleaner]{{dead link|date=February 2014}}</ref> was dedicated in [[Tema]] by Minister Sherry Ayittey on 20 January 2012.

A capacity development mechanism project, aiming to improve internal communication within the agency to increase efficiency, began in 2012.<ref>{{cite web|url=http://www.ghananewsagency.org/.../epa-embarks-on-capacity-development-p |title=Ghana News Agency |publisher=Ghana News Agency |date= |accessdate=2014-02-13}}</ref> With financial support from the [[Canadian International Development Agency]], communications between the agency's head office and regional offices have been improved with [[Internet access|broadband Internet]] access.

Ghana celebrated the 25th anniversary of the Montreal Protocol<ref>{{cite web|url=http://www.undp.org/content/25-years-of-the-Montreal-Protocol.html |accessdate=December 19, 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20140222202559/http://www.undp.org/content/25-years-of-the-Montreal-Protocol.html |archivedate=February 22, 2014 }}</ref> in [[Ho, Ghana|Ho]] on 14 September 2012, with its theme "protecting the atmospheres for generations to come".{{citation needed|date=July 2015}} Winners of school competitions received prizes. [[Climate change]] issues were tackled, with the national climate-change adaptation strategy document produced that year.

Amlalo was appointed EPA Ghana executive director on 3 January 2013, and began the agency's modernization. Three new offices were opened in Nkwanta (Volta), Damongo (NR) and Wulensi (NR) in August 2013. The 2013 Man and Biosphere Programme in Africa elected him its chair.<ref>{{cite web|url=http://www.unesco.org/new/fileadmin/MULTIMEDIA/HQ/SC/pdf/AfriMAB_2013_meeting_report_en.pdf |title=General Assembly of the African network of Biosphere Reserves (AFRIMAB) |publisher=Unesco.org |accessdate=2014-02-13}}</ref>

{| class="wikitable"
|+ EPA Ghana leadership
|-
! Year !! Name !! Position
|-
| 1974-81 || E. A. Boateng || Executive chairman
|-
| 1981-85 || B. W. Garbrah || Acting executive chairman
|-
| 1985-90 || Christine Debrah || Executive chairperson
|-
| 1990-93 || Franciska Issaka || Acting executive chairperson
|-
| 1993-94 || Farouk Braimah || Acting executive chair
|-
| 1994-2001 || Peter Acquah || Executive director
|-
| 2001-10 || Jonathan Allotey || Executive director
|-
| 2010-now || Daniel Amlalo || Executive director
|}

=={{anchor|Activities of the Agency}}Activities==
[[File:Training School.png|thumbnail|alt=Modern building with angular roof|EPA training school in Amasaman, opened in 2001]]

* Environmental education
* Environmental impact assessment
* Strategic environmental assessment
* Environmental governance
* Monitoring of industry and mines
* Natural-resource management
* Legal compliance and enforcement
* Environmental performance rating and public disclosure
* Reporting on the state of the environment
* Research on environmental sustainability

==Regional offices==
[[File:EPA office locations.JPG|thumbnail|alt=See caption|Map of Ghana with EPA regional offices]]

{| class="wikitable"
|-
! Year !! Office !! First head
|-
| 1988 || Northern Region (Tamale) || Edward M Telly
|-
| 1991 || Upper West Region (Wa)|| John Pwamang
|-
| 1991 || Upper East Region (Bolgatanga) || Saaka O. Sulemana
|-
| 1991 || Eastern Region (Koforidua)|| Johnson Boanuh
|-
| 1991 || Volta Region (Ho) | William Owusu Adja
|-
| 1992 || Western Region (Sekondi)|| Albert Boateng
|-
| 1992 || Brong Ahafo Region (Sunyani) || Francis Zakari
|-
| 1993 || Central Region (Cape Coast) || Maxwell Nimako Williams
|-
| 1994 || Ashanti Region (Kumasi)|| Emmanuel Mante
|-
| 1996|| Greater Accra (Amasaman) || Fynn Williams
|-
| 1999 || Tarkwa || Michael Sandow Ali
|-
| 2001|| Tema || Yaw Osafo Afriyie
|}

=={{anchor|Projects undertaken}}Projects==
[[File:Waste Segregation.JPG|thumbnail|alt=People sitting on a stage|Introduction of waste-segregation project in Accra]]

The Ghana Environmental Resource Management Programme began in 1992 to protect the environment at international standards, and staff were sent overseas to study for master's degrees. New departments and a library were opened, and books and videos acquired. In 1994, a historical database of the environment was published with 2,145 records and [[environmental impact assessment]] guidelines and procedures were produced. An environmental-education strategy for Ghana was introduced on 22 November 1994.

An urban air-quality project by EPA Ghana and the [[United States Environmental Protection Agency]] to monitor [[air pollution]] in Accra began in 2004. Results showed that at six locations, roadside dust and vehicular emissions were the main contributors of airborne particulates.

In 1988, the [[World Bank]] launched a clean-air initiative for [[Sub-Saharan Africa]]n cities. After EPA Ghana officers found elevated blood-lead levels in schoolchildren and those working near roads, such as the police and young salespeople, leaded gasoline was phased out in December 2003. EPA Ghana, the UNEP and the [[Ghana Health Service]] monitored blood-lead levels in [[Accra]] and [[Kumasi]] in 2006 to assess changes in blood-lead levels after the phaseout of leaded gasoline. [[Blood samples]] were taken from the [[Ghana Police Service]] and Tema Oil Refinery personnel, tanker drivers and workers and tollbooth operators, and the populations were all within the [[World Health Organization]] limit of 20&nbsp;µg/dl.

A National Programme of Action (NPA) seeks to protect Ghana's marine environment. Land-based activities had increased marine pollution from industrial effluent and poorly-managed waste, with resource degradation and increased [[coastal erosion]]. The impacts of these activities compromised the capacity of the coast to support sustainable socioeconomic and [[ecosystem services]], such as tourism. With support from the [[Guinea Current]] marine-ecosystem project, the NPA is prepared to solve domestic sanitation, fisheries degradation, wetland and mangrove degradation, industrial pollution and coastal erosion with institutional capacity-building, educational and awareness programmes and regulatory activities.

EPA Ghana is a focus of the Man and Biosphere programmes. In 2004 the National MAB Committee was chartered, and it began in 2005. Ghana joined the International Coordinating Council (ICC) of the MAB. Each year, entries are received for the MAB Young Scientist Award Competition. After review, and the best are submitted to [[UNESCO]] for the competition; in 2010, four were submitted. A delegation from the National MAB Committee visited the Bia Biosphere Reserve.

A [[strategic environmental assessment]] (SEA) began in May 2003.<ref name="autogenerated1">[http://www.epa.gov.gh/SEA ]{{dead link|date=February 2014}}</ref> The assessment was incorporated into the Ghana Poverty-Reduction Strategy, and 52 district assemblies produced [[development plan]]s based on the SEA.<ref name="autogenerated1"/> An SEA manual was produced, and assessments were conducted for transport and water.

Integrated management of invasive aquatic weeds, with financial support from the [[African Development Bank]] from 2006 to 2011, produced a manual for mechanical and biological weed control. Total weed coverage in the Tano and [[Volta River]]s was {{convert|6066|ha|acre}}, and 20 community water-weed committees cleared and maintained {{convert|500|ha|acre}} of weedy areas by 2011.<ref>EPA ″Annual Report 2011″, Accra, Ghana.</ref> At the end of the project, two weed harvesters were purchased to clear all weeds in the Volta and were commissioned in 2012 by the Minister of Environment, Science and Technology.<ref>{{cite web|url=http://www.ghananewsagency.org/ayittey-commissions-two-weed-harvesters |title=Ghana News Agency |publisher=Ghana News Agency |date= |accessdate=2014-02-13}}</ref>

The 1992–93 [[drought]], which caused bush fires, prompted the EPC to solicit international aid for a lasting solution to the problem. The [[United Nations General Assembly]] passed resolution 39/68B, accepting Ghana's application. In 1984<!-- 1994? --> the [[United Nations Environment Programme]] (UNEP) and [[United Nations Development Programme]] (UNDP) added Ghana to their lists of countries receiving assistance to combat [[desertification]], and in 2002 a national action programme to combat drought and desertification was begun.<ref>EPA ″National Action Programme to Combat Drought and Desertification 2002″ Accra Ghana</ref> A project for developing [[drylands]] began in May 2006, with northern afforestation and increased production of [[guineafowl]] for income.

[[Noise pollution]] was widespread in Ghana's [[urban area]]s, with the chief culprits [[religious organization]]s, bar and restaurant operators who played loud music at night and music-cassette vendors. After complaints from the public, in September 2006 the agency purchased fifteen [[sound level meter]]s for distribution to its regional offices.<ref>EPA ″ Annual Report 2006″ Accra, Ghana</ref> With accurate measurements, noise pollution could be prosecuted. EPA Ghana has designated April 16 as National Noise Awareness Day<ref>[http://www.ghanaweb.com/EPA ]{{dead link|date=February 2014}}</ref> to alert the public to environmental and health implications of excess noise. A legal database project was begun in 2008 to group environmental laws in Ghana.

EPA Ghana has an ozone unit, tasked with phasing out [[Ozone depletion|ozone-depleting]] substances (ODS) after Ghana signed the Montreal Protocol, and information on ODS uss was collated for the multilateral fund and ozone secretariats. Refrigeration shops were monitored to ensure good practice and identify [[refrigerant]] brands on the market. Few shops had CFC12 and [[1,1,1,2-Tetrafluoroethane|R134a]], and shops with a mixture of refrigerants had them seized. [[R-406A]], a new refrigerant compatible with CFC12 and HFC134a systems, was found in use. Seminars, with more than 1,420 participants, were held on good refrigeration practice, [[hydrocarbon]] as an alternative refrigerant and hydrocarbon [[technology transfer]].<ref>EPA ″Progress Annual Report 2004-2010″, Accra, Ghana.</ref> The import of equipment using [[chlorofluorocarbon]]s (CFCs) was banned in 2010, and customs officers were trained in their detection. Applications for financial incentives from three cold-storage facilities were approved, and the facilities were inspected. EPA officers visited the three major foam-producing factories in Accra and Nsawam to ensure worker safety and environmental compliance.<ref>EPA ″Annual Report 2010″, Accra, Ghana.</ref>

A [[Waste sorting|waste-segregation]] project was introduced by the Minister of Environment Science and Technology.<ref>[http://www.myjoyonline.com/epa-kick-start-waste-separation]{{dead link|date=February 2014}}</ref> With support from Jakora Ventures, a private [[waste management]] company, 6,000 litter containers were obtained and distributed to 48 institutions in the ministries area. Each institution has three containers on each floor, and paper, plastic and food waste is placed in separate containers which are emptied daily. The paper and plastic will be recycled, and the food waste will be [[compost]]ed. If the project is successful, it will be extended to homes and markets.

== World Environment Day ==
[[World Environment Day]] is celebrated annually to increase awareness of national environmental problems. The EPC began the celebration in June 1975 with an exhibit entitled "Man and his Environment". Each year a different theme is chosen, and the celebration is rotated among Ghana's ten regions. In 2006 it was celebrated in Duase Ashanti Region with the theme "Your planet needs you; unite to combat climate change", and in 2010 it was celebrated at Osino in the Eastern Region with the theme "Many species, one planet, one future".

== {{anchor|40th Anniversary Celebration}}40th anniversary ==
[[File:40th Anniversary Forum.JPG|thumbnail|alt=Multicolored banner|40th-anniversary forum banner]]
[[File:Green City Bill board.JPG|thumbnail|alt=Plastic-on-wood sign in two horizontal sections|Green City Project sign in Danfa, near Accra]]
The agency celebrated its 40th anniversary in 2014. Begun on 29 January, the celebration included tree-planting, lectures, a school competition, awards and a dinner.
A Green City Project, a new head office, began the year and with a groundbreaking ceremony and the agency is seeking accreditation for a university to train environmental experts. UNEP executive director [[Achim Steiner]] planned to visit the agency, and individuals and companies who contributed to making Ghana an environmentally-friendly country were scheduled to receive awards.

==References==
{{Reflist|colwidth=30em}}

[[Category:Environment of Ghana]]
[[Category:Environmental agencies|Ghana]]