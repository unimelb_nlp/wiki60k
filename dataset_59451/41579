{{Infobox Military Conflict
|conflict=Battle of Baidoa
|partof=the [[War in Somalia (2006–2009)]]
|date=December 20&ndash;26, 2006
|image=
|place=[[Bay, Somalia]]
|caption=TFG forces patrol Burhakaba after ICU retreat.
|result=Decisive Ethiopian/TFG Victory
|combatant1=[[File:Somalia Islamic Courts Flag.svg|22px]] [[Islamic Courts Union]] <br>[[File:Somalia Islamic Courts Flag.svg|22px]] Pro-Islamist Militias<br>Foreign [[Mujahideen|fighters]]
|combatant2={{flagicon|Somalia}} [[Transitional Federal Parliament|Transitional Federal Government]] <br>{{flag|Ethiopia}}<ref>[http://news.bbc.co.uk/2/hi/africa/6207427.stm Ethiopia admits Somalia offensive] [[BBC]]</ref><ref>{{cite news
 |last=Ryu 
 |first=Alisha 
 |title=Ethiopian Jets Bomb Airports in Somalia 
 |publisher=[[Voice of America]] 
 |date=25 December 2006 
 |url=http://www.voanews.com/english/2006-12-24-voa4.cfm 
 |accessdate=2007-01-01 
 |archiveurl=https://web.archive.org/web/20061224130150/http://voanews.com/english/2006-12-24-voa4.cfm
 |archivedate=2006-12-24
 |deadurl=yes 
 |df= 
}}</ref>
|commander1=
|commander2=
|strength1= 5,000+ Islamist militia 
|strength2= 3,000+ TFG militia,<br>5,000+ Ethiopian soldiers
|casualties1=1,000 Somali Islamists dead<ref>[http://newsbox.msn.co.uk/article.aspx?as=adimarticle&f=uk_-_olgbtopnews&t=4023&id=4285141&d=20061226&do=http://newsbox.msn.co.uk&i=http://newsbox.msn.co.uk/mediaexportlive&ks=0&mc=5&ml=ma&lc=en&ae=windows-1252 Up to 1,000 Somali Islamists dead]</ref><br>800+ wounded<ref>[http://www.alertnet.org/thenews/newsdesk/L26387427.htm At least 800 war wounded in Somalia -Red Cross]</ref>
|casualties2=400 killed(Islamists claim)<ref>{{cite news 
  | title =Somalian militia says troops ready to launch attack against Ethiopian forces
  | publisher =[[Taipei Times]]
  | date =24 December 2006
  | url =http://www.taipeitimes.com/News/world/archives/2006/12/24/2003341766
  | accessdate = 2007-01-01}}</ref>
}}
{{Campaignbox War in Somalia (2006–2009)}}
{{Campaignbox Rise of ICU}}
The '''Battle of Baidoa''' began on December 20, 2006 when the [[Transitional Federal Parliament|Somali Transitional Federal Government]]'s forces (TFG) allied with [[Military of Ethiopia|Ethiopian forces]] stationed there attacked advancing [[Islamic Courts Union]] (ICU) forces along with 500 alleged [[Eritrea]]n troops and [[mujahideen]] arrayed against them.

The battle began with most reports hourly depicting the government forces defecting and its position on the verge of collapse. The TFG, along with its allies in the [[Juba Valley Alliance]] (JVA), had certainly been on the retreat since the June offensives of the ICU. By the opening of the battle in December, Baidoa was invested with attacks coming in at least three directions.

Strong Ethiopian reinforcements rapidly changed the battle from one in which the TFG was on the defensive, through a strong series of counterattacks, to an Ethiopian/TFG decisive victory. Ethiopian armor, artillery, and air forces proved instrumental against the ICU's militia-based army.

== Timeline ==
=== December 20, 2006 ===

On December 20, heavy shooting broke out between Somali government troops and Islamists {{convert|25|km|mi|abbr=on}} southeast of [[Baidoa]]<ref name=FIGHTING-ERUPTS-ON-SOMALI-FRONT>[http://www.alertnet.org/thenews/newsdesk/L20484024.htm Fighting erupts on Somali front near govt stronghold] Reuters</ref> where the Islamists claimed to have taken the government's military base in [[Daynuunay]]. The conflict thereafter moved north to the Islamist stronghold in [[Moode Moode]] <ref name=HEAVY-FIGHTING-ERUPTS-IN-SOMALIA>[http://news.bbc.co.uk/2/hi/africa/6195863.stm Heavy fighting erupts in Somalia] [[BBC]]</ref> Heavy weapons, including artillery, rockets and mortars were involved.<ref name=FEROCIOUS-FIGHTING-CONTINUES>[http://www.shabelle.net/news/ne1904.htm   Ferocious fighting continues closer to the government base of Baidoa] {{webarchive |url=https://web.archive.org/web/20070930000000/http://www.shabelle.net/news/ne1904.htm |date=September 30, 2007 }} Shabelle Media Network</ref><ref name=SOMALI-ISLAMIST-DOWNPLAYS-WAR-FEARS>[http://www.alertnet.org/thenews/newsdesk/L20540919.htm Somali Islamist downplays war fears amid clashes] Reuters</ref> Initial claims of casualties in this area were at least ten dead ICU militiamen and forty TFG soldiers wounded.<ref name=HEAVY-FIGHTING-RAGES-NEAR-BAIDOA>[http://somalinet.com/news/world/Somalia/5975 Heavy fighting rages near Baidoa, the government base] SomaliNet</ref> Later claims of ICU casualties by the TFG were 71 Islamic soldiers dead and 221 injured, including two dead foreign fighters. The TFG claimed its own casualties were 3 dead and 7 wounded while the ICU claimed to have killed 7 government soldiers.<ref name=SKIRMISHES-FOLLOW-AGREEMENT-FOR-PEACE-TALKS>[http://www.iht.com/articles/ap/2006/12/21/africa/AF_GEN_Somalia.php Skirmishes in Somalia follow agreement to resume peace talks] Associated Press</ref>

Fighting was reported on many fronts around the capital in [[Iidale]] village (55&nbsp;km south of Baidoa), [[Buulo Jadid]] (23&nbsp;km north of Baidoa, also spelled Bullo Jadid), and [[Manaas]] (30&nbsp;km southwest of Baidoa).<ref name=RESIDENTS-FLEE-FIGHTING>[http://www.alertnet.org/thenews/newsdesk/IRIN/63f763ff61716254b4609b59d57c3d5a.htm Residents flee fighting near Baidoa] Reuters</ref> One TFG death and numerous injured civilians were reported in Iidale.<ref name=SKIRMISHES-ON-FOR-SECOND-DAY>[http://www.shabelle.net/news/ne1900.htm Skirmishes On for the Second Day As EU Commission Lands At Baidoa] {{webarchive |url=https://web.archive.org/web/20070930000000/http://www.shabelle.net/news/ne1900.htm |date=September 30, 2007 }} Shabelle Media Network</ref> A later report raised the casualties to three soldiers killed and two injured. Thirteen trucks filled with Ethiopian reinforcements were reported en route to the fighting.<ref name=SOMALI-ISLAMIST-DOWNPLAYS-WAR-FEARS/>

An AFP report mentioned the TFG claimed the attack on Iidale was led by [[Abu Taha al-Sudan]], who is "wanted by Washington for carrying out [[1998 U.S. embassy bombings|attacks against its embassies in east Africa in 1998]] and against an [[Kenyan hotel bombing|Israeli-owned hotel in Kenya in 2002]]."

This report raised the death toll from the artillery duel in Iidale to 12, and added that the government captured 30 "armed vehicles" (presumably [[Technical (fighting vehicle)|technicals]]). It also contradicted the fall of Daynuunay to the ICU: "'The fighting is so fierce, but government forces are still controlling Daynuunay,' said Issak Adan Mursaley, a resident in Deynunay."<ref name=CLASHES-BROADEN-BETWEEN-SOMALI-ISLAMIST-AND-GOVERNMENT-TROOPS>[http://www.garoweonline.com/stories/publish/article_6576.shtml Clashes broaden between Somali Islamist and government troops ] AFP</ref>

Meanwhile, an EU peace-brokering commission led by [[Louis Michel]] landed at Baidoa and then Mogadishu to meet respectively with the TFG and ICU representatives.<ref name=SKIRMISHES-ON-FOR-SECOND-DAY/><ref name=EU-COMMISSIONER-LOUIS-MICHEL-ARRIVES-MOGADISHU>[http://www.shabelle.net/news/ne1906.htm EU commissioner Louis Michel arrives in Mogadishu] {{webarchive |url=https://web.archive.org/web/20070930000000/http://www.shabelle.net/news/ne1906.htm |date=September 30, 2007 }} Shabelle Media Networks</ref> Discussions yielded the agreement to meet in [[Khartoum]], [[Sudan]] at an unspecified future date.<ref name=SOMALI-GOVT-ISLAMIC-RIVALS-TO-HOLD-TALKS>[http://www.ndtv.com/morenews/showmorestory.asp?slug=Somali+govt%2C+militias+to+hold+peace+talks&id=98240 Govt, Islamic rivals to hold peace talks] Associated Press</ref>

In [[Dadaab]], [[Kenya]], UN Deputy High Commissioner for Refugees, [[Wendy Chamberlin]], said camps there accounted for 34,000 refugees fleeing the fighting and floods in Somalia, but that number is expected to grow to 80,000 if fighting continues. The [[World Food Programme]] (WFP) is attempting to provide relief, but floods and mud have hampered ground transportation.<ref name=POLITICAL-CRISIS-ESCALATES>[http://www.ndtv.com/morenews/showmorestory.asp?slug=Political+crisis+in+Somalia+escalates&id=98177 Political crisis in Somalia escalates] NDTV</ref>
Sheikh Mohamed Ibrahim Bilal, speaking for the ICU, claimed fighting was going its way in Iidale and Buulo Jadid, saying they captured two technicals, killed nine soldiers, and had taken prisoners in the fighting.<ref name=CHANGES-DATELINE-ADDS-CASUALTIES>[http://rawstory.com/news/2006/Changes_dateline_adds_casualties_qu_12202006.html Changes dateline, adds casualties, quotes, EU update (sic)] {{webarchive |url=https://web.archive.org/web/20080907000000/http://rawstory.com/news/2006/Changes_dateline_adds_casualties_qu_12202006.html |date=September 7, 2008 }} dpa German Press Agency</ref>

[[File:Battle-of-baidoa-12262006-0752.svg|500px|left|Battle of Baidoa, December 26, 2006]]

=== December 21, 2006 ===

On December 21, [[Puntland]] President [[Adde Muse]] claimed ICU casualties were heavy in the fighting around Baidoa, sustaining 75 dead and 125 wounded, along with the loss of 30 vehicles burned or captured.<ref name=ISLAMISTS-SUFFER-HEAVY-CASUALTIES-SAYS-PUNTLAND-PRESIDENT>[http://www.garoweonline.com/stories/publish/article_6590.shtml Islamists suffered heavy casualties, says Puntland president]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }} Garowe Online</ref>

Also reported on the 21st, fighting in Iidale and Daynuunay was said to have started the prior morning and continued through the next day. No end to the fighting seemed imminent, as both sides continued to gather reinforcements. Casualty figures were unavailable, but the numbers were expected to be in the hundreds easily. The report went on to refute the ICU claim of victory, and stated that the government was again in possession of Iidale and had killed foreign fighters. The government also captured dozens of Islamist students who took up arms, sufficient in quantity to fill three lorries.<ref name=SECOND-DAY-GOVERNMENT-MAY-HAVE-UPPER-HAND>[http://somalinet.com/news/world/Somalia/6009 Clashes enter the second day, government may have upper hand] {{webarchive |url=https://web.archive.org/web/20081011154459/http://somalinet.com/news/world/Somalia/6009 |date=October 11, 2008 }} SomaliNet</ref>

Sketchy, unattributed and unconfirmable reports stated Baidoa was on the "brink of collapse," and put the odds of fighters in the battle at 10-to-1 in favor of the ICU, with unconfirmed reports that the ICU are being provided with "high-tech weaponry, trainers and even fighters from many Muslim countries and Eritrea".<ref name=BAIDOA-ON-BRINK-OF-COLLAPSE>[http://somalinet.com/news/world/Somalia/6008 Baidao on the brink of collapse] SomaliNet</ref>

=== December 22, 2006 ===

By December 22, TFG refuted assertions of collapse when it claimed to have inflicted 700 casualties on the ICU. The ICU claimed to have killed more than 200 government troops.<ref name=CLASHES-ENTER-FOURTH-DAY>[http://somalinet.com/news/world/Somalia/6049 http://somalinet.com/news/world/Somalia/6049] SomaliNet</ref>

Contrary to ICU claims, the TFG said it was advancing. Reports put TFG troops in [[Safar Noles]], on the approaches to [[Dinsoor]].<ref name=WAR-BULLETIN-DECEMBER-22>[http://somalinet.com/news/world/Somalia/6072 Somalia: War bulletin - December 22nd] SomaliNet</ref>

Nearly 20 Ethiopian tanks were seen heading toward the front line. According to government sources Ethiopia has 20 [[T-55]] tanks and four attack helicopters in Baidoa. The tanks were reported to be splitting into two groups heading towards fighting in Daynuunay and Iidale.<ref name=ETHIOPIAN-TANKS-ROLL-IN-SOMALI-BATTLE>[http://www.alertnet.org/thenews/newsdesk/L22886890.htm Ethiopian tanks roll in Somali battle's fourth day] Reuters</ref> Ethiopia may have as many as 50 tanks and other armored vehicles in the country.<ref name=ETHIOPIAN-TANKS-ROLL-INTO-SOMALIA>[http://news.monstersandcritics.com/africa/news/article_1235885.php/Ethiopian_tanks_roll_into_Somalia Ethiopian Ethiopian tanks roll into Somalia] dpa - Deutsche Presse-Agentur</ref>

Civilian casualties were reported as dozens killed and over 200 wounded.<ref name=SOMALIA-DOZENS-KILLED-AS-FIGHTING-CONTINUES>[http://www.alertnet.org/thenews/newsdesk/IRIN/c4964eece779ca5fd21d8bfe250448cc.htm SOMALIA: Dozens killed as fighting continues in the south] IRIN</ref>

To the north, in [[Mudug]], 500 Ethiopian troops with eight tanks and 30 pickup trucks mounted with anti-aircraft guns were headed for [[Bandiradley]], an Islamic Courts stronghold in central Somalia, according to witnesses and Islamic Courts officials. The Council of Islamic Courts said they would send ground troops to attack on Saturday, instead of fighting from a distance with heavy weapons as they have been doing so far. "Our troops have not started to attack. From tomorrow the attack will start," said Ibrahim Shukri Abuu-Zeynab, an Islamic Courts spokesman.<ref name=SOMALI-CIVILIANS-FLEE-FIGHTING>[http://english.aljazeera.net/NR/exeres/663FB205-27CD-4C72-84DF-5362C36570F0.htm Somalia Civilians flee fighting] {{webarchive |url=https://web.archive.org/web/20061222165156/http://english.aljazeera.net/NR/exeres/663FB205-27CD-4C72-84DF-5362C36570F0.htm |date=December 22, 2006 }} Al Jazeera</ref>

A TFG press release stated a unit of 500 Eritrean troops with artillery and other heavy weapons had reinforced [[Buurhakaba|Burhakaba]]. It claimed TFG troops had successfully destroyed an ICU force in Dinsoor, killing all the commanders, forcing the rest to surrender, and resulting in five foreign fighters committing suicide rather than face capture.<ref name=GOVERNMENT-PRESS-RELEASE-WAR-SITUATION>[http://somalinet.com/news/world/Somalia/6073 Somalia: Government press release - war situation] SomaliNet</ref>

Nineteen bodies of Islamic fighters were found in Moode Moode by an [[Associated Press]] photographer. Reports that the ICU is forcing people to fight in Baidoa are claimed by a captured Islamic fighter.<ref name=CARNAGE-AS-SOMALIA-IN-STATE-OF-WAR>[http://edition.cnn.com/2006/WORLD/africa/12/21/somalia.fighting.ap/index.html Carnage as Somalia "in state of war] CNN</ref>

The Council of Islamic Courts leader said that Somalia was in "a state of war" with the [[Transitional Federal Parliament|TFG]] and Ethiopia.<ref name=THOUSANDS-FLEE-AS-WAR-ESCALATES>[http://edition.cnn.com/2006/WORLD/africa/12/22/somalia.ap/index.html Thousands flee as war escalates] CNN</ref>

=== December 23, 2006 ===

Fighting continued in and around [[Baidoa]], particularly at Iidale and Dinsoor; approximately 60 and 120 kilometres south of Baidoa respectively. Iidale was reportedly captured by the Islamists.<ref name=ISLAMISTS-CAPTURE-FRONTLINE-POSITION>[http://www.news.com.au/heraldsun/story/0,21985,20969685-5005961,00.html Islamists 'capture' frontline position] Sunday Herald Sun</ref><ref name=SOMALI-COURTS-CAPTURE-GOVERNMENT-POSITION>[http://www.middle-east-online.com/english/?id=18911 Somali Courts capture government position] Middle East Online</ref> 

Somali Information Minister of the TFG, Ali Jama, quoted that "The combined total from two fronts is over 500 Islamists killed since Wednesday,".<ref name=ETHIOPIAN-TANKS-MOVE-INTO-BATTLE-WITH-SOMALIA-ISLAMISTS>
[http://www.afp.com/english/news/stories/061222220311.i90y0vye.html Ethiopian tanks move into battle with Somalia Islamists]{{dead link|date=January 2017 |bot=InternetArchiveBot |fix-attempted=yes }} AFP</ref> He later went on to report that most of those that were killed were children sent by the ICU to fight in the fronts east and south of Baidoa.

The ICU vowed all out war against the [[Ethiopia]]n-backed transitional government, with Islamic commander Hassan Bullow stating that "This war is a religious obligation and we are here to fight for our religion against the enemies until we die." Islamic officials scorned the African Union and Arab League for doing nothing, saying "The world is silent today while Ethiopian forces are killing us inside our country, but tomorrow when we defeat them and chase them things will be changed, we will enter their territories and at that moment the world will shout".<ref name=ETHIOPIAN-TANKS-MOVE-INTO-BATTLE-CLASH-FEARS>[http://www.abc.net.au/news/newsitems/200612/s1817802.htm Ethiopian tanks move into battle with Somalia Islamists] ABC</ref><ref name=Somali-Islamists-urge-Muslims-to-join-war-on-Ethiopia>[http://today.reuters.com/news/articlenews.aspx?type=worldNews&storyID=2006-12-23T162651Z_01_L23110484_RTRUKOC_0_US-SOMALIA-CONFLICT.xml&pageNumber=1&imageid=&cap=&sz=13&WTModLoc=NewsArt-C1-ArticlePage3 Somali Islamists urge Muslims to join war on Ethiopia] Reuters</ref>
One resident saw Islamist fighters pushing toward Daynunay, the government's forward military base about {{convert|20|km|mi|abbr=on}} southeast of its encircled base at Baidoa.
"This morning, I heard sounds of rockets being fired from the frontline," Hassan Yusuf added. He said he saw three dead Ethiopian soldiers taken by Islamist fighters to a village close to Daynunay on Friday, while wounded Islamist fighters were being treated in nearby Buur Hakaba.

Islamists, for the first time, called upon international fighters to join their cause stating "We're saying our country is open to Muslims worldwide. Let them fight in Somalia and wage jihad, and God willing, attack [[Addis Ababa]]".<ref name="SOMALI-ISLAMISTS-URGE MUSLIM-FIGHTERS-TO-JOIN-JIHAD">[http://today.reuters.com/news/articlenews.aspx?type=newsOne&storyID=2006-12-23T092033Z_01_L23670097_RTRUKOC_0_US-SOMALIA-CONFLICT-ISLAMIST.xml&WTmodLoc=Home-C2-TopNews-newsOne-2 Somali Islamists urge Muslim fighters to join jihad] Reuters</ref>

ICU defense chief, Sheik [[Yusuf Mohammed Siad Indho-adde]], made a world-wide appeal for jihadists to come to Somalia, and claimed the ICU had taken [[Tiyoglow]], [[Bakol]] province.<ref>[http://www.shabelle.net/news/ne1930.htm Islamists call world Muslim fighters to wage their jihad war in Somalia] Shabelle Media Networks</ref> Independent journalists were also shown that [[Iidale]] had been taken by the ICU, where they were shown bodies of Ethiopian troops. The TFG denied the claims as "cheap propaganda."<ref name=ETHIOPIAN-BODIES-SEEN-IN-IDALE-TOWN>[http://somalinet.com/news/world/Somalia/6108 Ethiopian bodies seen in Idale town] {{webarchive |url=https://web.archive.org/web/20070117000000/http://somalinet.com/news/world/Somalia/6108 |date=January 17, 2007 }} SomaliNet</ref>

The [[International Committee of the Red Cross]] expressed their concern over those caught up in the fighting, quoting the figure of 200 wounded fighters being brought into hospitals on both sides.<ref name=ETHIOPIAN-TANKS-MOVE-INTO-BATTLE-WITH-SOMALIA-ISLAMISTS/>

=== December 24, 2006 ===

Islamist [[Abdulahi Gedow]], commander of forces in Burhakaba, claimed to have seized [[Gasarta]], less than 12&nbsp;km south of Baidoa.<ref name=ISLAMISTS-CLAIM-THEY-SEIZED-GASARTE>[http://www.shabelle.net/news/ne1935.htm  Somalia: Islamists claim they seized Gasarte quite closer to Baidoa] Shabelle Media Networks</ref> This would be an advance of 10&nbsp;km north of Daynunay (which is 22&nbsp;km southeast of Baidoa). Islamists claimed that five Ethiopian tanks were destroyed.<ref name=ETHIOPIAN-PLANES-BOMB-SOMALI-AREAS-WITNESSES>[http://today.reuters.com/news/articlenews.aspx?type=worldNews&storyID=2006-12-24T102258Z_01_L24148211_RTRUKOC_0_US-SOMALIA-CONFLICT.xml&WTmodLoc=IntNewsHome_C1_%5BFeed%5D-7 Ethiopian planes bomb Somali areas: witnesses] Reuters</ref>

TFG Defence Minister [[Barre Shire Hirale]] claimed to have retaken [[Iidale]] after a clash in which over 100 were killed.<ref name=OVER-100-KILLED-IN-CLASHES>[http://www.news24.com/News24/Africa/News/0,,2-11-1447_2048521,00.html Over 100 killed in clashes] SA</ref>

In Kismayo, 1,000 men were said to be leaving for the battle, presumably to fight on behalf of the ICU.<ref name=ETHIOPIA-FIGHTS-RIVAL-SOMALI-ISLAMISTS>[http://za.today.reuters.com/news/NewsArticle.aspx?type=topNews&storyID=2006-12-24T155352Z_01_BAN429351_RTRIDST_0_OZATP-SOMALIA-CONFLICT-20061224.XML Ethiopia fights rival Somali Islamists] Reuters</ref>

Ethiopian airstrikes hit targets across Somalia, including [[Dinsoor]] and [[Buurhakaba|Burhakaba]] in the Bay region as part of the counter-offensive in the battle. To the north, [[Bandiiradley|Bandiradley]] in [[Mudug]] and [[Beledweyne]] in [[Hiiraan|Hiiran]] were also struck.<ref name=ETHIOPIAN-AIRSTRIKES-TARGET-ISLAMISTS-IN-SOMALIA>[http://www.voanews.com/english/2006-12-24-voa4.cfm Ethiopian Airstrikes Target Islamists in Somalia] Voice of America</ref>

TFG Deputy Defense Minister [[Salad Ali Jele]] stated government forces had advanced within three kilometers of Bur-Hakaba and were poised to re-capture the town.<ref name=SOMALIA-CLASHES-SPREAD-IN-MUDUG-AND-HIRAN>[http://somalinet.com/news/world/Somalia/6115 Somalia: Clashes spread in Mudug and Hiran regions] {{webarchive |url=https://web.archive.org/web/20070216000000/http://somalinet.com/news/world/Somalia/6115 |date=February 16, 2007 }} SomaliNet</ref>

A Somalian website claimed that thousands of angry people rallied in the capital of Mogadishu in protest of the Ethiopian air strikes. The people took to the streets of Mogadishu, forcing business centers to close down and send militia to the front. It also claimed that hundreds of volunteers were registering to join the Islamists.<ref name=Anti-Ethiopia-rallies-in-Somalia-as-Ethiopian-air-raid-continue>[http://www.somalinet.com/news/world/Somalia/6122 Anti-Ethiopia rallies in Somalia] SomaliNet</ref>

=== December 25, 2006 ===

Ethiopian airstrikes hit the airports at Mogadishu and [[Bali-Dogle]].<ref>[http://vietbao.vn/The-gioi/Ethiopia-oanh-tac-san-bay-Somali/10990028/159/ Ethiopia oanh tạc sân bay Somali] {{vi}}</ref><ref>[http://vietbao.vn/The-gioi/Ethiopia-danh-bom-Somalia-de-day-lui-chien-binh/65078040/159/ Ethiopia đánh bom Somalia để đẩy lùi chiến binh] {{vi}}</ref>  The latter airport lies 115&nbsp;km northwest of Mogadishu in the district of [[Wanlaweyne]] about half-way between the capital and the front lines at Burhakaba.<ref name=SOMALIA-ETHIOPIAN-HELICOPTER-DOWN>[http://somalinet.com/news/world/Somalia/6136 Somalia: Ethiopian helicopter down, claim Islamists] {{webarchive |url=https://web.archive.org/web/20081011154515/http://somalinet.com/news/world/Somalia/6136 |date=October 11, 2008 }} SomaliNet</ref><ref>{{cite news|last=Duhul |first=Salad |title=Ethiopian jets bomb airports in Somalia |publisher=[[Yahoo!]], [[Associated press]] |date=2006-12-25 |url=https://news.yahoo.com/s/ap/20061225/ap_on_re_af/somalia |accessdate=2007-01-01 }}{{dead link|date=June 2016|bot=medic}}{{cbignore|bot=medic}}</ref> At least one person was reported dead and others wounded in the Mogadishu attack. The Islamic Courts tightened security in the capital as a result.<ref name=SOMALIA-ISLAMIC-COURTS-TIGHTEN-SECURITY-IN-CAPITAL>[http://somalinet.com/news/world/Somalia/6150 Somalia: Islamic Courts to tighten security in the capital] SomaliNet</ref>

Ethiopian forces were reported to have taken both [[Beledweyne]] and [[Buuloburde]] in [[Hiran, Somalia|Hiran]], with unconfirmed reports that "hundreds of Ethiopian tanks" were moving along the road towards [[Jowhar]].  This presents a threat of a major flanking of ICU positions in [[Tiyoglow]] and [[Buurhakaba|Burhakaba]] by striking towards the [[Shabeellaha Dhexe|Middle Shabelle]] area.  The Ethiopian forces were accompanied by Somali warlord [[Mohamed Omar Habeb]] 'Mohamed Dhere,' who wished to reestablish his control over Jowhar.<ref name=ICU-LOST-KEY-TOWNS-IN-SOMALIA>[http://somalinet.com/news/world/Somalia/6127 ICU lost key towns in Somalia] {{webarchive |url=https://web.archive.org/web/20070117000000/http://somalinet.com/news/world/Somalia/6127 |date=January 17, 2007 }} SomaliNet</ref>

Ethiopian forces had also reportedly encircled both Dinsoor and Burhakaba.<ref name=ETHIOPIAN-JETS-STRIKE-SOMALI-AIRPORTS>[http://www.alertnet.org/thenews/newsdesk/L25477410.htm Ethiopian jets strike Somali airports] Reuters</ref> Fighting had advanced near Dinsoor to Rama-Addey. The TFG asserted it was within a few kilometers of capturing Burhakaba.<ref name=ETHIOPIA-MAKING-PROGRESS-IN-SOMALIA-FIGHTING>[http://somalinet.com/news/world/Somalia/6154 Ethiopia making progress in Somalia fighting] SomaliNet</ref>

The ICU's "commando-type arm" under the command of veteran of Afghanistan, [[Adan Ayrow]] was said to be giving stiff resistance.<ref name=SOMALIA-WAR-BULLETIN-FOR-DECEMBER-25-2006>[http://somalinet.com/news/world/Somalia/6152 Somalia: War bulletin for December 25, 2006] {{webarchive |url=https://web.archive.org/web/20070110202517/http://somalinet.com/news/world/Somalia/6152 |date=January 10, 2007 }} SomaliNet</ref> Yet late in the day, fighting was reported within Dinsoor itself.  Dinsoor resident Mohammed Hassan reported that Ethiopian forces had taken the town.<ref name=INTENSE-FIGHTING-AROUND-DIINSOOR-REPORTED>[http://www.garoweonline.com/stories/publish/article_6667.shtml Somalia: Intense fighting around Diinsoor reported] Garowe Online</ref>

=== December 26, 2006 ===

ICU forces reportedly abandoned their positions in Burhakaba and Dinsoor after days of heavy fighting, leaving behind a number of heavy weapons and other military equipment.<ref name=ISLAMIC-COURTS-VACATE-BURHAKABA>[http://somalinet.com/news/world/Somalia/6156 Somalia: Breaking News - Islamic Courts vacate Burhakaba] SomaliNet</ref> Witnesses reported that ICU fighters were retreating from the Battle of Baidoa on numerous fronts and returning to Mogadishu in convoys.<ref name=ISLAMIC-FIGHTERS-QUITTING-SOMALIA-FRONT>[https://news.yahoo.com/s/ap/20061226/ap_on_re_af/somalia Islamic fighters quitting Somalia front] AP</ref> Islamic councilman Mohamoud Ibrahim Suley confirmed the withdrawal. Troops were also reported to have withdrawn to [[Daynuunay]]. Ethiopian troops arrived in [[Buurhakaba|Burhakaba]] after the Islamists vacated it.<ref name=ISLAMISTS-LOST-DINSOOR-AND-BURHAKABA>[http://www.somalinet.com/news/world/Somalia/6158 Somalia: Islamists lost Dinsor and Bur-Hakaba towns as Ethiopia advances] SomaliNet</ref> An Ethiopian government spokesman also confirmed that Ethiopian troops also arrived in [[Dinsoor]] unopposed.<ref name="ISLAMIC-FORCES-RETREAT-IN_SOMALIA">[http://edition.cnn.com/2006/WORLD/africa/12/26/somalia.ap/index.html Islamic forces retreat in Somalia] CNN</ref> Despite this, local militamen were seen by witnesses to be stealing boxes of food and medicine, further hampering any [[Diplomatic and humanitarian efforts in the Somali Civil War|humanitarian efforts]].<ref name=ETHIOPIAN-JETS-FIRE-ON_RETREATING-SOMALI-ISLAMISTS>[http://www.khaleejtimes.com/DisplayArticleNew.asp?xfile=data/theworld/2006/December/theworld_December695.xml&section=theworld Ethiopian jets fire on retreating Somali Islamists] {{webarchive |url=https://web.archive.org/web/20070506225553/http://www.khaleejtimes.com/DisplayArticleNew.asp?xfile=data/theworld/2006/December/theworld_December695.xml&section=theworld |date=May 6, 2007 }} Reuters</ref> Airstrikes also continued, this time on [[Leego]], just east of [[Buurhakaba|Burhakaba]]. Three people were reportedly killed in the space of thirty minutes.<ref name=ETHIOPIAN-JETS-FIRE-ON_RETREATING-SOMALI-ISLAMISTS/>

Ethiopia's Prime Minister, [[Meles Zenawi]], announced Ethiopia would likely withdraw its troops within a few weeks. He said their main goal was to damage the ICU's military capabilities, sense of invincibility and ensure a more balanced setting for peace talks.<ref name="ISLAMIC-FORCES-RETREAT-IN_SOMALIA"/> The [[United States]] also announced it would support Ethiopian military operations, saying the country has "genuine security concerns," although it was unclear in what capacity the US would support the nation.<ref name=STATE-DEPARTMENT:-U.S.-SUPPORTS-ETHIOPIAN-MILITARY>{{cite news
 |title=State Department: U.S. supports Ethiopian military 
 |publisher=[[CNN]] 
 |date=December 26, 2006 
 |url=http://www.cnn.com/2006/WORLD/africa/12/26/us.somalia.ap/index.html 
 |accessdate=2007-01-01 
 |archiveurl=https://web.archive.org/web/20070106005824/http://www.cnn.com/2006/WORLD/africa/12/26/us.somalia.ap/index.html 
 |archivedate=2007-01-06 
 |deadurl=yes 
 |df= 
}}</ref> The United Nations envoy to Somalia has urged an end to the fighting, and [[Qatar]], the current President of the UN Security Council, has proposed a draft statement calling for an immediate cease-fire and the withdrawal of all international forces.  Other nations, such as the US, Britain, France, and Russia, have objected to the statement, saying peace talks and agreement are necessary before troops can withdraw.<ref>{{cite news
 |title=U.N. Envoy seeks Security Council action on Somalia 
 |publisher=[[CNN]] 
 |date=December 27, 2006 
 |url=http://www.cnn.com/2006/WORLD/africa/12/26/somalia.ethiopia/index.html 
 |accessdate=2007-01-01 
 |archiveurl=https://web.archive.org/web/20070104173712/http://www.cnn.com/2006/WORLD/africa/12/26/somalia.ethiopia/index.html 
 |archivedate=2007-01-04 
 |deadurl=yes 
 |df= 
}}</ref>

== See also ==
* [[Military history of Africa]]
{{War in Somalia (2006–2009)}}

== References ==
{{reflist|2}}

{{coord missing|Somalia}}

{{DEFAULTSORT:Battle Of Baidoa}}
[[Category:2006 in Ethiopia]]
[[Category:2006 in Somalia]]
[[Category:Battles of the War in Somalia (2006–2009)|Baidoa]]
[[Category:Battles involving Ethiopia|Baidoa]]
[[Category:Battles involving Somalia|Baidoa]]
[[Category:Bay, Somalia]]
[[Category:December 2006 events]]