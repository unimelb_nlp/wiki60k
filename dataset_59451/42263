<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=AC-35
 | image=ACA AC-35.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Autogyro]]
 | national origin=[[United States of America]]
 | manufacturer=[[Autogiro Company of America]]
 | designer=
 | first flight=March 26, 1936
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= 
 | produced= 
 | number built=
 | program cost= 
 | unit cost= $15,000 for 1960s version<ref name="Autogiro Company of America AC-35"/>
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Autogiro Company of America AC-35''' was an early attempt to make a [[roadable aircraft]] in the United States during the 1930s. Although it was successfully tested, it did not enter production; a 1960s attempt to revive the aircraft in a non-roadable version also failed to achieve success.

==Design and development==
[[File:DOC-CAA.JPG|left|thumb|logo on side of the AC-35]]
The aircraft design process started in 1935. The Experimental Development Section of the [[Bureau of Air Commerce]] contracted the building of a roadable aircraft based around an PA-22 [[autogyro]] from ACA's parent company, [[Pitcairn-Cierva Autogiro Company|Pitcairn Autogiro Company]]. The vehicle could fly at high speed in the air, and drive at up to {{convert|25|mph|km/h|0|abbr=on}} on the ground with its rotors stowed. Six other companies were contracted to produce a roadable aircraft, but the AC-35 was the only one that met all the requirements.<ref name="Autogiro Company of America AC-35">{{cite web|title=Autogiro Company of America AC-35|url=http://www.nasm.si.edu/collections/artifact.cfm?id=A19500086000|accessdate=21 October 2010}}</ref><ref>{{cite book |title=From autogiro to gyroplane |last=Charnov |first=Bruce H. |year=2003 |publisher=Praeger |location=Westport, Connecticut |isbn=978-1-56720-503-9 |pages=137–141}}</ref>

The AC-35 had side-by-side seating with a small baggage compartment. The fuselage was a combination of steel tube in front, and wood construction in the tail with fabric covering overall. The engine was rear mounted with a shaft driven forward propeller. The vehicle had three equal size wheels (two in front, one in the rear). The rear wheel was shaft driven from the engine and the front wheels provided steering.<ref name="Autogiro Company of America AC-35"/>

==Operational history==
On March 26, 1936 the AC-35 was flown by test pilot James G. Ray with counter rotating propellers. These were later replaced with a single conventional propeller arrangement. On October 2, 1936, Ray landed the AC-35 in a downtown park in [[Washington, D.C.]] where it was displayed, On October 26, 1936 The aircraft was converted to roadable configuration.<ref name=Dawson>{{cite book |title=Realizing the dream of flight: biographical essays in honor of the centennial of flight, 1903–2003 |last=Dawson |first=Virginia |author2=Mark D. Bowles |year=2005 |publisher=National Aeronautics and Space Administration, NASA History Division, Office of External Relations |asin=B002Y26TM0  |page=70}}</ref> Ray drove it to the main entrance of the Commerce Building where it was accepted by [[John H. Geisse]], chief of the Aeronautics Branch.  It was driven to [[Bolling Field]] for additional testing and review by [[Hap Arnold]].<ref name=Brooks>{{cite book |title=Cierva Autogiros: The Development of Rotary-Wing Flight |last=Brooks |first=Peter W. |year=1988 |publisher=Smithsonian Institution Press |location=Washington, D.C. |isbn=978-0-87474-268-8 |pages=218–219}}</ref>

The aircraft was tested by the [[Autogiro Company of America]] at [[Naval Air Station Joint Reserve Base Willow Grove|Pitcairn Field]] until 1942. In 1950 the [[Bureau of Air Commerce]] transferred the AC-35 to the [[Smithsonian Institution]].<ref name="Autogiro Company of America AC-35"/>

==Variants==
In 1961, [[Skyway Engineering Company. Inc.]] in [[Carmel, Indiana]] licensed the AC-35 with an intent to produce a non-roadable variant, powered by a 135-hp [[Lycoming O-290]]-D2Bs engine. One example was built and test flown out of [[Terry Field]] in [[Indianapolis]], but did not go into production as the company failed.<ref name="Autogiro Company of America AC-35"/>

==Specifications (AC-35)==
{{Aircraft specs
|ref=ierva Autogiros: The Development of Rotary-Wing Flight<ref name=Brooks/>
|prime units?=imp
<!--
        General characteristics
-->
|genhide=
|crew=
|capacity=2
|length m=
|length ft=16
|length in=3
|length note=
|height m=
|height ft=
|height in=
|height note=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Pobjoy Cascade]]
|eng1 type=
|eng1 kw=
|eng1 hp=90
|eng1 note=
|power original=
|thrust original=
|more power=
|prop blade number=
|prop name=
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=
|rot number=1
|rot dia m=
|rot dia ft=34
|rot dia in=3
|rot area sqm=
|rot area sqft=
|rot area note=

<!--
        Performance
-->
|perfhide=
|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=75
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
* [[Buhl Aircraft Company]] ; [[Pitcairn Autogiro Company]]
|related=
|similar aircraft=
* [[Buhl A-1 Autogyro]] ; [[Hafner Rotabuggy]]
|lists=
}}

==Notes==
{{reflist}}

==External links==
* [http://aerofiles.com/_al.html Aerofiles - Autogiro]
*[https://books.google.com/books?id=6CcDAAAAMBAJ&pg=PA19&dq=Popular+Science+1932+plane&hl=en&ei=k79PTYCuA4P78Abo8_3EDg&sa=X&oi=book_result&ct=result&resnum=9&ved=0CEoQ6AEwCDge#v=onepage&q&f=true "Tilt Rotor Steers New Autogiro" - ''Popular Science Monthly'', October 1933, conceptual drawing based on information supplied by manufacture]

{{Flying cars}}

{{DEFAULTSORT:Autogiro Company Of America Ac-35}}
[[Category:Individual aircraft in the collection of the Smithsonian Institution]]
[[Category:Autogyros]]
[[Category:Roadable aircraft]]
[[Category:United States experimental aircraft 1930–1939]]
[[Category:Pitcairn aircraft]]