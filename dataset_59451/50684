{{for|the earlier journal of a similar title|Bell System Technical Journal}}
{{Infobox journal
| cover =
| discipline = [[Electrical engineering]], [[computer science]], [[telecommunication]]
| abbreviation = Bell Labs Techn. J.
| editor = Jane M. DeHaven
| publisher = [[John Wiley & Sons]]
| country =
| history = 1996–present
| frequency = Quarterly
| impact = 0.385
| impact-year = 2013
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1538-7305
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1538-7305/currentissue
| link1-name = Online access
| link2 = http://www.alcatel-lucent.com/bstj/
| link2-name = Online archive
| link3 = http://ieeexplore.ieee.org/xpl/RecentIssue.jsp?reload=true&punumber=6731002
| link3-name = Online archive
| ISSN = 1089-7089
| eISSN = 1538-7305
}}
The '''''Bell Labs Technical Journal''''' is the in-house [[scientific journal]] for scientists of [[Bell Labs]]/[[Alcatel-Lucent]]. It is published quarterly by [[John Wiley & Sons]]. The [[editor-in-chief]] is Jane M. DeHaven.

== Abstracting and indexing ==
The following abstracting and indexing services cover the journal:
{{columns-list|colwidth=30em|
*[[EBSCO Industries|EBSCO databases]]
*[[CSA (database company)|CSA Illumina]] 
*[[Compendex]]
*[[Current Contents]]/Engineering, Computing & Technology 
*[[Current Index to Statistics]]
*[[Expanded Academic ASAP]]
*[[FIZ Karlsruhe]] – CompuScience Database
*[[CSA (database company)|METADEX]]
*[[Science Citation Index|Science Citation Index Expanded]] 
*[[Scopus]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 0.385.<ref name=WoS>{{cite book |year=2014 |chapter=Bell Labs Technical Journal |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate= |series=[[Web of Science]] |postscript=.}}</ref>

== See also==
* ''[[TWX Magazine]]''
* ''[[Bell Labs Record]]''
* ''[[Bell System Technical Journal]]''

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://ieeexplore.ieee.org/xpl/RecentIssue.jsp?punumber=6731002}}
* [http://www.alcatel-lucent.com/bstj/ Bell System Technical Journal], 1922-1983
* [http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1538-7305/issues Bell Labs Technical Journal] Wiley Online Library, 1922-2014
* [https://archive.org/details/bstj-archives The Bell System Technical Journal (1922-1983)], Archive.org

[[Category:Engineering journals]]
[[Category:Bell Labs]]
[[Category:English-language journals]]
[[Category:Publications established in 1922]]
[[Category:Quarterly journals]]
[[Category:John Wiley & Sons academic journals]]
[[Category:Telephony]]