{{italic title}}
{{Infobox Journal
| title        = Leonardo
| cover        = [[Image:leonardolowres.jpg]]
| editor       = Roger F. Malina
| discipline   = [[Arts]]
| language     = English
| abbreviation = Leonardo
| publisher    = [[MIT Press]]
| country      = United States
| frequency    = Six issues per year including ''[[Leonardo Music Journal]]''
| history      = 1968-present
| openaccess   = 
| impact       = 
| impact-year  = 
| website      = http://www.leonardo.info/
| link1        = http://www.mitpressjournals.org/toc/leon/current
| link1-name   = Online access
| link2        = 
| link2-name   = 
| RSS          = 
| atom         = 
| JSTOR        = 0024094X
| OCLC         = 1755782
| LCCN         = 
| CODEN        = 
| ISSN         =  0024-094X 
| eISSN        = 1530-9282 
}}
'''''Leonardo''''' is a [[peer-reviewed]] [[academic journal]] published by the [[MIT Press]] covering the application of contemporary science and technology to the arts and music.<ref>[http://www.mitpressjournals.org/page/about/leon About ''Leonardo'']</ref>

== History ==
It was established in 1968 by artist and scientist [[Frank Malina]] in [[Paris]], [[France]].<ref>[http://www.leonardo.info/isast/leostory.html History of ''Leonardo'']</ref> ''Leonardo'' has published writings by artists who work with science- and technology-based art media for over 40 years. Journal operations were moved to the [[San Francisco Bay]] Area by Frank's son [[Roger Malina]], an astronomer and space scientist, who took over operations of the journal upon Frank Malina's death in 1981. In 1982, the [[International Society for the Arts Sciences and Technology]] was founded to further the aims of ''Leonardo'' by providing avenues of communication for artists working in contemporary media. The society also publishes ''[[Leonardo Music Journal]]'', the ''Leonardo Electronic Almanac'', ''Leonardo Reviews'', and the ''Leonardo Book Series''. All publications are produced in collaboration with the [[MIT Press]].

Other activities of the organization include an awards program and participation in annual conferences and symposiums such as the Space and the Arts Workshop and the annual College Art Association conference. Leonardo has a sister organization in France, the Association Leonardo, that publishes the ''Observatoire Leonardo'' website. While encouraging the innovative presentation of technology-based arts, the society also functions as an international meeting place for artists, educators, students, scientists and others interested in the use of new media in contemporary artistic expression.

The aims of the organization include the documentation of personal and innovative technologies developed by artists, similar to the way in which findings of scientists are documented in journal publications.<ref>Leonardo de Cardoso: ''Visual Music: An Ethnography of an Experimental Art in Los Angeles'', Master's Thesis, University of Texas at Austin, May 2010, [http://repositories.lib.utexas.edu/bitstream/handle/2152/ETD-UT-2010-05-1228/CARDOSO-MASTERS-REPORT.pdf#page=47 p. 39]</ref>

==See also==
* [[New Media]]
* [[Caterina Verde]], contributing artist

==References==
{{reflist}}

==External links==
* [http://www.leonardo.info/ Official website]
* [http://www.mitpressjournals.org/loi/leon ''Leonardo'' on the MIT Press website]

[[Category:Publications established in 1968]]
[[Category:MIT Press academic journals]]
[[Category:English-language journals]]
[[Category:Arts journals]]
[[Category:Bimonthly journals]]
[[Category:Academic journals associated with learned and professional societies]]


{{humanities-journal-stub}}