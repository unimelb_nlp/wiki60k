{{Infobox person
| name        = T. N. Manoharan
| Full Name   = Thothala Narayanasamy Manoharan Chowdhary
| father      = T.L. Narayana Swami Chowdhary
| mother      = T.N. Sarada Narayana Swami
| birth_date  = {{birth date and age|1956|04|7}}
| birth_place = [[India]]
| nationality = {{flagicon|India}}
| education   = [[University of Madras]]<br> [[Sri Venkateswara University]], Andhra Pradesh<br> Fellow [[Chartered Accountant]]<br> Law Graduate from Madras Law College, [[University of Madras]], India.
| occupation  = Founding Partner, Manohar Chowdhry & Associates
| current_position = Chairman of Canara Bank
| Residence   = Abiramapuram, Chennai
| title       = [[Padma Shri]], [[Chartered Accountant]] 
| credits     = [[Padma Shri]] Awardee (2010)<br /> Former president of [[Institute of Chartered Accountants of India|ICAI]], 
| URL         = http://www.mca.co.in/team1.php
}}

'''Padma Shri (CA.) T. N. Manoharan''' is an [[India]]n [[Chartered Accountant]] and a former president of  [[Institute of Chartered Accountants of India]] ([[Institute of Chartered Accountants of India|ICAI]]).<ref>{{cite web|url=http://www.thehindubusinessline.com/2009/10/27/stories/2009102751101900.htm|title= Mr T.N. Manoharan, past President, ICAI; Dept of Management Studies at N.M.S.S. Vellaisamy Nadar College, Madurai|date=27 October 2006|work=[[Business Line]]|accessdate=26 March 2010}}</ref>

==Career==
Manoharan was born in April 1956 in a [[Indian Independence Movement|freedom fighter]]'s family with agricultural background. He obtained a post graduate degree in Commerce from Sri Venkateshwara University and a law degree from [[Madras Law College]]. He became a Chartered Accountant in 1983 and set up his public practice in the same year. He is the founding partner of the accounting firm Manohar Chowdhry & Associates, [[Chennai]]. He was elected to the central council of ICAI in 2001 and its Vice President in 2005. He remained as a Central Council Member till 2007.<ref>{{cite web|url=http://www.icai.org/post.html?post_id=2010&c_id=238|title=KAMLESH SHIVJI VIKAMSEY AND T.N. MANOHARAN ELECTED PRESIDENT AND VICE-PRESIDENT OF ICAI - (05-02-05)|date=5 February 2005|work=ICAI|accessdate=26 March 2010}}</ref> He has also chaired various committees of the Institute. In 2006, he was elected as the President of the ICAI (for a one year term). He is also a notable [[freemason]].<ref>{{cite web|url=http://www.masonsouthernindia.org/craft/craft_trophymaster.html |title=TN Manoharan, recipient of Best Master Award in 2005}}</ref>The current ARGM of Chennai Area.

In 2009, Manoharan was one of the four members (as representatives of Government of India) appointed to the board of the [[Satyam scandal|scandal]] hit information technology company [[Mahindra Satyam|Satyam Computer Services]] for restructuring the company.<ref>{{cite news|url=http://economictimes.indiatimes.com/infotech/software/Raju-family-attempted-to-bid-for-Satyam-TN-Manoharan/articleshow/4952299.cms|title=Raju, family attempted to bid for Satyam: TN Manoharan|last= Hema Ramakrishnan|author2=Sai Deepika Amirapu |date=31 August 2009|work=[[The Economic Times]]|accessdate=26 March 2010}}</ref> As of 2010, he continues to be a member of the company's board. He has been a visiting faculty for Taxation at the Southern India Regional Council (SIRC) of [[Institute of Chartered Accountants of India|ICAI]] and the [[Reserve Bank of India]]'s staff college and has authored text books on Taxation.<ref>{{cite web|url=http://www.mahindrasatyam.com/corporate/boardmembers.asp|title=T. N. Manoharan profile|work=[[Mahindra Satyam]]|accessdate=26 March 2010}}</ref><ref>{{cite web|url=http://www.swpindia.com/latest_books.htm|title=Snowhite Publications|accessdate=26 March 2010}}</ref> He is very popular amongst CA Students and he is very much involved with students.<ref>{{cite news|url=http://www.hindu.com/edu/2009/10/26/stories/2009102650140500.htm|title= T.N. Manoharan with students | location=Chennai, India | work=The Hindu|date=26 October 2009}}</ref> In 2009, he won the [[CNN-IBN]] "Indian of the Year" award (along with his fellow board members of Mahindra Satyam) in the business category.<ref>{{cite web|url=http://sify.com/movies/bollywood/fullstory.php?id=14924450|title=A R Rahman is Indian of the Year 2009|last=[[Indo-Asian News Service|IANS]]|date=22 December 2009|work=[[Sify]]|accessdate=26 March 2010}}</ref> He was awarded the [[Padma Shri]] (India's fourth highest civilian honour) in 2010.<ref>{{cite web|url=http://india.gov.in/myindia/padmashri_awards_list1.php?start=70|title=2010 Padmashree Award winners|work=[[Government of India]]|accessdate=26 March 2010}}</ref> He is also a board member of a number of organisations including Nani Palkhiwala Foundation, Alpha Capital Management and [[Sahara India Pariwar|Sahara India]] Finance Corporation.<ref>{{cite news|url=http://economictimes.indiatimes.com/infotech/software/Priority-is-to-prevent-default-on-salaries-payments-Satyam-director/articleshow/3985461.cms|title=Priority is to prevent default on salaries, payments: Satyam director|last=Chandra Ranganathan|date=15 January 2009|accessdate=26 March 2010|work=The Times Of India}}</ref> He is very popular among students of Chartered Accountancy course, as he completed the course himself after a lot of struggle.
His teaching is tremendous. He was the Chairman of Canara Bank, one of the largest public sector lenders in India until his recent retirement from his services.

== References ==
{{Reflist}}

== External links ==
* [http://www.mca.co.in/team1.php Official webpage]

{{Padma Shri Awards}}

{{DEFAULTSORT:Manoharan, T. N.}}
[[Category:Living people]]
[[Category:1956 births]]
[[Category:Recipients of the Padma Shri in trade and industry]]
[[Category:Indian accountants]]
[[Category:Tamil businesspeople]]
[[Category:Businesspeople from Tamil Nadu]]
[[Category:Sri Venkateswara University alumni]]