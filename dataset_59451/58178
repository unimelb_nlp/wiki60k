{{Infobox journal
| title = Journal of Human Evolution
| cover = [[File:Journal of Human Evolution cover.gif]]
| editor = Sarah Elton, Mike Plavcan
| discipline = [[Evolutionary biology]] [[Anthropology]] [[Archaeology]]
|language = English| abbreviation = J. Hum. Evol.
| publisher = [[Elsevier]]
| country =
| frequency = Monthly
| history = 1972-present
| impact = 3.767
| impact-year = 2015
| website = http://www.elsevier.com/wps/find/journaldescription.cws_home/622882/description#description
| link2 = http://www.sciencedirect.com/science/journal/00472484
| link2-name = Online archive
| ISSN = 0047-2484
| OCLC = JHEVAT
| LCCN = 72623558
}}
The '''''Journal of Human Evolution''''' is a monthly [[peer-reviewed]] [[scientific journal]] in the field of evolution, specializing in human and primate evolution. The journal was established in 1972 and is published by [[Elsevier]] in print and online on [[ScienceDirect]].<ref name=":0">{{Cite web|title = Journal of Human Evolution|url = http://www.journals.elsevier.com/journal-of-human-evolution/?testing=a|website = Elsevier|accessdate = 2015-10-15}}</ref> As of November 2015, the journal was on its 88th volume.<ref>{{Cite web|title = Journal of Human Evolution - ScienceDirect.com|url = http://www.sciencedirect.com/science/journal/00472484|website = www.sciencedirect.com|accessdate = 2015-10-15}}</ref> The [[editors-in-chief]] are Sarah Elton ([[Durham University]]) and Mike Plavcan ([[University of Arkansas]]).

The journal content covers all aspects of human evolution, focusing on those topics in [[physical anthropology]], [[primatology]], [[paleoanthropology]], and [[paleolithic archaeology]].<ref name=":0" /> Topics include but are not limited to studies of human and primate fossils, comparative studies of living species through morphological and molecular evidence ([[Molecular anthropology|See Molecular Anthropology]]), and studies supporting fossil evidence of primate and human evolution through [[Stratigraphy (archaeology)|stratigraphy]] and [[taphonomy]].<ref name=":0" /> Additional research areas include interpretative analyses of new and previously described fossil or skeletal material, assessments of the [[phylogeny]] and [[paleobiology]] of primate species including the line of ''[[Homo]]'', and studies involving paleoecological and paleogeographical models of primate and [[human evolution]].<ref name=":0" /> In addition to articles and studies published in the journal, the journal makes available space to publish brief announcements of new or important discoveries.

== Features of this Journal ==

=== Content Access ===
In this journal, individual journal contributors maintain the ability to limit the reach of their published articles. Contributors can decide to limit access to their articles by choosing to publish their articles for subscribers only or to publish their articles as open access content, which allows the general public access to their articles.<ref name=":0" />

=== Featured Author Videos ===
The online version of the journal supports short videos of authors explaining their research.<ref>{{Cite web|title = Featured Author Videos|url = https://www.elsevier.com/books-and-journals/content-innovation/featured-author-videos|website = www.elsevier.com|accessdate = 2015-10-23}}</ref> These videos, in MP4 file format, are available to stream or for download, allowing them to be watched at a later time using any common media player.

=== AudioSlides ===
AudioSlides are a feature supported by the online version of the journal through ScienceDirect.<ref>{{Cite web|title = AudioSlides|url = https://www.elsevier.com/books-and-journals/content-innovation/audioslides|website = www.elsevier.com|accessdate = 2015-10-23}}</ref> This format features a short five-minute visual slide presentation with an audio voice over by the author, based on an article the author has published in the journal. AudioSlides presentations are not peer-reviewed and represent the opinion and views of the author of the author's published work.

=== Database Linking Tool ===
The journal, in digital format, has included a Database Linking tool.<ref>{{Cite web|title = Database Linking|url = https://www.elsevier.com/books-and-journals/content-innovation/data-base-linking|website = www.elsevier.com|accessdate = 2015-10-23}}</ref> Authors can choose to publish their data in the journal and link the data to public data repositories established in the field of research, which then can be easily accessed by other researchers in the field or closely associated fields and the general public.

=== Interactive 3D Models ===
The publisher, Elsevier has developed three dimensional visualization tools for this journal and others, which authors can choose to include 3-D models and scans within their published articles online.<ref>{{Cite web|title = 3D Models|url = https://www.elsevier.com/books-and-journals/content-innovation/3d-models|website = www.elsevier.com|accessdate = 2015-10-23}}</ref> These 3-D models allow for a greater complexity of ideas to be shown visually, which can enhance understanding of these concepts.

== Publication Details ==

=== Impact Factor and Other Measurements of Importance ===
According to the ''[[Journal Citation Reports]]'', the journal had a 2014 [[impact factor]] of 3.733, but in 2015, the impact factor increased to 3.767.<ref name=":0" /><ref name="WoS">{{cite book |year=2014 |chapter=Journal of Human Evolution |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref> The overall five-year impact factor of this journal is 4.131, as of 2015.<ref name=":0" /> For the year 2015, the [[Eigenfactor]] , the rating of the total importance of an academic journal, was 0.012.<ref name=":2">{{Cite web|title = Elsevier Journal Metrics Visualization. Helping Authors. Visualizing Key Metrics. Delivering Journal Insights.|url = http://journalinsights.elsevier.com/journals/0047-2484/article_influence|website = journalinsights.elsevier.com|accessdate = 2015-10-23}}</ref> Additionally, [[Article influence score|Article Influence]] is a ranking based on the Eigenfactor score, and is often comparable to the Impact Factor. The journal's Article Influence is 1.572 for 2015.<ref name=":2" /> The journal also has a [[SCImago Journal Rank]] of 2.145 for 2015. For 2015, the SNIP/IPP ranking for the journal is 1.211.<ref>{{Cite web|title = Elsevier Journal Metrics Visualization. Helping Authors. Visualizing Key Metrics. Delivering Journal Insights.|url = http://journalinsights.elsevier.com/journals/0047-2484/snip|website = journalinsights.elsevier.com|accessdate = 2015-10-23}}</ref> SNIP refers to Source-Normalized Impact per Paper and it is a means to measure impact of a particular journal by weighing the citations of a particular journal in comparison to the total number of citations in the given subject field. IPP (Impact per Publication) is a ratio comparing the number of citations in the a year to the number of citations in the previous three years divided by the total number of papers published in those three years.<ref>{{Cite web|title = Journal Metrics: Research analytics redefined|url = http://www.journalmetrics.com/ipp.php|website = www.journalmetrics.com|accessdate = 2015-10-23}}</ref>

=== Review Speed ===
The review speed refers to the time at which a manuscript, an article or other suitable publication, enters the journal's publication process and exits the process for publication.<ref name=":1" /> During this time it spends in review, the manuscript is reviewed, and returned to author for alterations, clarifications, and edits, based on recommendations of the reviewers. When the author is finished with edits, the manuscript is resubmitted for final review and if accepted, published. There may be multiple rounds between review and re-submission before a manuscript is accepted. The initial review period of the Journal of Human Evolution for the year 2015 lasts on average 8 weeks, while the review speed for the whole process averages 24 weeks to publication.<ref name=":1">{{Cite web|title = Elsevier Journal Metrics Visualization. Helping Authors. Visualizing Key Metrics. Delivering Journal Insights.|url = http://journalinsights.elsevier.com/journals/0047-2484/review_speed|website = journalinsights.elsevier.com|accessdate = 2015-10-22}}</ref>

=== Online Article Publication Time ===
For the year 2015, the time between the acceptance of the manuscript by the journal and the date when the manuscript first appears within the online database averages 8 weeks.<ref>{{Cite web|title = Elsevier Journal Metrics Visualization. Helping Authors. Visualizing Key Metrics. Delivering Journal Insights.|url = http://journalinsights.elsevier.com/journals/0047-2484/oapt|website = journalinsights.elsevier.com|accessdate = 2015-10-22}}</ref> The time between the first appearance of the article online and the article's final appearance online currently averages 15 weeks for 2015. The initial publication of an article online is not fully [[Pagination|paginated]], which takes more time but is useful for professionals in the field that may wish to cite the information from the article in their own work.

=== Author Information ===
Corresponding authors are found worldwide. The majority of contributing authors are found in the United States.<ref>{{Cite web|url=http://journalinsights.elsevier.com/journals/0047-2484/authors|title=Elsevier Journal Metrics Visualization. Helping Authors. Visualizing Key Metrics. Delivering Journal Insights.|website=journalinsights.elsevier.com|access-date=2016-07-07}}</ref> In the top ten countries for contributing author, Spain is second, followed by the United Kingdom. Germany, France, Australia, Canada, Israel, Italy and China. These numbers are generated from the last five years' authors and based on the primary authors' country of residence. It doesn't reflect the total number of contributing non-primary authors, as contributing authors are often in collaboration with colleagues at many universities and institutions across the globe.

== See also ==
* [[Elsevier]]
* [[List of Elsevier periodicals]]
* [[List of anthropology journals|List of Anthropology Journals]]

==References==
{{reflist}}

==External links==
* {{Official website|http://www.journals.elsevier.com/journal-of-human-evolution/}}

[[Category:Evolutionary biology journals]]
[[Category:Anthropology journals]]
[[Category:Elsevier academic journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1972]]
[[Category:English-language journals]]