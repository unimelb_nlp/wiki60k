{{Use dmy dates|date=September 2013}}
{{Use British English|date=September 2013}}
{{good article}}
{{Infobox military person
|name= Sir Richard Lawson Barrons
|image= Richard Barrons.jpg
|image_size= 
|alt= 
|caption= 
|nickname= 
|birth_date= {{Birth date and age|df=yes|1959|05|17}}
|birth_place= [[Northampton]], [[Northamptonshire]], England
|death_date= 
|death_place= 
|placeofburial= 
|allegiance= United Kingdom
|branch= [[British Army]]
|serviceyears= 1977–2016
|rank= [[General (United Kingdom)|General]]
|servicenumber= 504825
|unit= [[Royal Artillery]]
|commands= [[Joint Forces Command]]<br/>Deputy Commanding General, [[Multi-National Corps – Iraq]]<br/>[[39th Infantry Brigade (United Kingdom)|39th Infantry Brigade]]<br/>[[3rd Regiment Royal Horse Artillery]]
|battles= [[The Troubles]]<br/>[[Kosovo War]]<br/>[[Iraq War]]<br/>[[War in Afghanistan (2001–14)|War in Afghanistan]]
|awards= [[Knight Commander of the Order of the Bath]]<br/>[[Commander of the Order of the British Empire]]<br/>[[Queen's Commendation for Valuable Service]] (2)<br/>[[Legion of Merit|Officer of the Legion of Merit]] (United States)
|relations= 
|laterwork= Author, guest speaker<br/>Chairman of the [[Royal Artillery Museum]]
}}
[[General (United Kingdom)|General]] '''Sir Richard Lawson Barrons''', {{post-nominals|country=GBR|size=100%|sep=,|KCB|CBE|ADC Gen}} (born 17 May 1959)<ref>{{Cite web|url=http://www.freebmd.org.uk/cgi/information.pl?cite=WcV2Mcg7X%2BD9AeRVrTN3Bg&scan=1|title=Index entry|accessdate=26 October 2016|work=FreeBMD|publisher=ONS}}</ref><ref name="London Gazette">{{London Gazette |issue=47364 |date=31 October 1977 |startpage=13729 |supp=yes}}</ref> is a retired [[British Army]] officer. He was Commander [[Joint Forces Command]] from April 2013 until his retirement in April 2016.

Barrons' early career was spent in various [[Staff (military)|staff]] and field posts in the UK, across Europe, and in the [[Far East]]. He also spent time working at the [[Ministry of Defence (United Kingdom)|Ministry of Defence]] and in education. Sent to Germany in 1991, Barrons then served his first tour of duty in the [[Balkans]] in 1993. Returning to the UK, Barrons took up a staff position and went on to do a tour in [[Northern Ireland]] and then to become a [[Military Assistant]], first to the [[High Representative for Bosnia and Herzegovina]] and then to the [[Chief of the General Staff (United Kingdom)|Chief of the General Staff]]. Between 2000 and 2003, Barrons served again in the Balkans, in Afghanistan during the early days of [[International Security Assistance Force]], and then in a staff position in [[Basra]], [[Iraq]].

As a [[Brigadier (United Kingdom)|brigadier]] in 2003, Barrons served his second tour in Northern Ireland, this time as a brigade commander. In 2005, he was appointed to Assistant Chief of Staff, Commitments, a senior staff position. He was promoted to [[Major-General (United Kingdom)|major general]] in 2008 and deployed to Iraq for the second time, this time to [[Baghdad]], with responsibility for joint operations. He then served briefly with the [[NATO]] [[Allied Rapid Reaction Corps]] before being sent to Afghanistan for the second time, when he headed an ISAF reintegration unit to provide incentives for Taliban soldiers to surrender. He later became [[Deputy Chief of the Defence Staff|Deputy Chief of the Defence Staff (Operations)]].

==Military career==
Barrons was commissioned as a [[second lieutenant]] on probation as a university cadet into the [[Royal Regiment of Artillery]] on 2 September 1977 prior to reading [[Philosophy, Politics and Economics]] at [[The Queen's College, Oxford]] and becoming a full-time army officer on 21 June 1980.<ref name="London Gazette"/><ref name="MOD">{{cite press release |url=https://www.gov.uk/government/news/new-senior-military-officers-appointed |title=New senior military officers appointed |date=24 January 2013 |publisher=[[Ministry of Defence (United Kingdom)|Ministry of Defence]] |accessdate=24 January 2013}}</ref><ref>{{London Gazette |issue=48287 |date=22 August 1980 |startpage=12028 |supp=yes}}</ref> His commission was confirmed in 1981, with seniority from 17 May 1977 and he was promoted to [[first lieutenant#United Kingdom|lieutenant]] with seniority from 17 May 1979.<ref>{{London Gazette|issue=48554|supp=yes|startpage=3781|date=16 March 1981}}</ref> Between 1980 and 1990, he served in various positions across Europe and the Far East as well as in a staff position at the [[Ministry of Defence (United Kingdom)|Ministry of Defence]] in London. He was promoted to [[captain (British Army and Royal Marines)|captain]] on 19 November 1983.<ref>{{London Gazette|issue=49552|supp=yes|startpage=15767|date=28 November 1983}}</ref> and took a [[master's degree]] in Defence Administration in 1990, after which he attended the British Army's [[Staff College, Camberley]], in 1991.<ref name="MOD"/>

Barrons' first [[field officer]] promotion was to [[major (United Kingdom)|major]] in September 1991.<ref>{{London Gazette|issue=52691|supp=yes|startpage=16034|date=21 October 1991}}</ref> He was sent to [[Germany]] to take up a position as chief of staff, [[11th Armoured Brigade (United Kingdom)|11 Armoured Brigade]], which then deployed to the Balkans in 1993. Barrons then served briefly as Balkans desk officer at the Directorate of Military Operations and before becoming battery commander of B Battery, [[1st Regiment Royal Horse Artillery]] from 1994–1996, which included a tour of duty in [[Northern Ireland]].<ref name="MOD"/> He was promoted to [[lieutenant Colonel (United Kingdom)|lieutenant colonel]] on 30 June 1997.<ref>{{London Gazette|issue=54827|supp=yes|startpage=7831|date=7 July 1997}}</ref> After promotion, he served again in Bosnia, as [[Military Assistant]] (MA) to the [[High Representative for Bosnia and Herzegovina]] and then, back in the UK, as MA to the [[Chief of the General Staff (United Kingdom)|Chief of the General Staff]].<ref name="MOD"/>

He went on to command [[3rd Regiment Royal Horse Artillery]], headquartered in [[Hohne]] Germany, with which he deployed to the Balkans again in 2001. At the end of 2001, Barrons was appointed chief of staff of the [[3rd Mechanised Division (United Kingdom)|3rd (United Kingdom) Division]] and immediately deployed to [[Afghanistan]], where the division assisted in establishing the [[International Security Assistance Force]]. After serving in Afghanistan, Barrons returned to the UK to attend the [[Higher Command and Staff Course]],<ref name="MOD"/> before promotion to [[colonel (United Kingdom)|colonel]] in June 2002.<ref>{{London Gazette|issue=56620|supp=yes|startpage=7887|date=2 July 2002}}</ref> Barrons' next deployment was to [[Iraq]] in 2003 as chief of staff, [[Multi-National Division (South-East) (Iraq)|Multinational Division (South East)]], stationed in [[Basra]].<ref name="MOD"/>

===High command===
Barrons was promoted to [[Brigadier (United Kingdom)|brigadier]] on 31 December 2003, with seniority from 30 June 2003.<ref>{{London Gazette|issue=57168|supp=yes|startpage=123|date=6 January 2004}}</ref> Upon promotion, he was posted to [[Northern Ireland]], commanding [[39th Infantry Brigade (United Kingdom)|39 Infantry Brigade]] in [[Belfast]], a position he held for two years. After Northern Ireland, he was appointed Assistant Chief of Staff, Commitments in 2005, with day-to-day responsibility for British Army operations.<ref name="MOD"/>

Barrons attained [[general officer]] status in 2008, when he was promoted to the substantive rank of [[Major-General (United Kingdom)|major general]] and appointed Deputy Commanding General, [[Multi-National Corps – Iraq]].<ref>{{London Gazette|issue=58752|supp=yes|startpage=9836|date=1 July 2008}}</ref> He was posted to [[Baghdad]], where he had responsibility for overseeing joint operations conducted by [[Multi-National Force – Iraq|the multinational force]] and the [[Iraqi Army]].<ref name="ISAF">{{cite web |url=http://www.rs.nato.int/about/leadership/major-general-richard-barrons.html |title=Major General Richard Barrons CBE (UK Army) |publisher=[[Resolute Support Mission]] |accessdate=16 February 2011}}</ref> Having served in Iraq, he returned to the UK to take up a staff post in April 2009 as chief of staff to the NATO [[Allied Rapid Reaction Corps]] (ARRC), but the appointment was short-lived as, in October 2009, he deployed to Afghanistan at short notice to establish a force reintegration unit, part of an effort to persuade Taliban fighters to rejoin society by offering alternatives to fighting, such as jobs and training<ref name="ISAF"/><ref>{{cite news |last=Starkey |first=Jerome |author-link=Jerome Starkey |date=3 March 2010 |title=Major-General Richard Barrons puts Taleban fighter numbers at 36,000 |url=http://www.timesonline.co.uk/tol/news/world/afghanistan/article7047321.ece |newspaper=[[The Times]] |accessdate=16 February 2011 |subscription=yes}}</ref>—a role for which he was hand-picked by U.S. Army General [[Stanley A. McChrystal]], then commander of all troops in Afghanistan.<ref name="sun">{{cite news |url=http://www.thesun.co.uk/sol/homepage/news/campaigns/our_boys/2657167/British-Army-Chief-Major-General-Richard-Barrons-will-talk-Nato-foes-into-surrender.html |title=British Army Chief Major General Richard Barrons will talk Nato foes into surrender |last=Crick |first=Andy |date=28 September 2009 |newspaper=[[The Sun (United Kingdom)|The Sun]] |accessdate=16 February 2011}}</ref> Barrons defended the controversial scheme in interviews, saying that it was not "about buying insurgents off the battlefield" and that "the idea is that you get the whole community benefiting and turning against the insurgency".<ref>{{cite news |last=Riechmann |first=Deb |url=http://www.nbcnews.com/id/37070021/ |title=Afghan plan offers jobs, training to Taliban |date=5 October 2010 |publisher=[[MSNBC]] |agency=[[Associated Press]] |accessdate=17 February 2011}}</ref> In a later interview, Barrons also said "I am absolutely convinced it can be done, and that the time is right. This is an opportunity the Afghan people aren't going to get again. Most of them realize that, and are keen to take it now".<ref>{{cite news |url=http://www.washingtonpost.com/wp-dyn/content/article/2009/12/13/AR2009121302263.html |title=Afghan government not keeping promises to insurgents changing sides |last=Witte |first=Griff |date=14 December 2009 |newspaper=[[The Washington Post]] |accessdate=17 February 2011}}</ref>

Barrons' position, as of February 2011, was as [[Assistant Chief of the General Staff (United Kingdom)|Assistant Chief of the General Staff]].<ref name="beeb">{{cite news |url=http://www.bbc.co.uk/news/uk-12461211 |title=Army job loss e-mails: Soldiers get apology |date=15 February 2011 |publisher=[[BBC News]] |accessdate=16 February 2011}}</ref> In May 2011 he became [[Deputy Chief of the Defence Staff|Deputy Chief of the Defence Staff (Operations)]] in the rank of [[Lieutenant-general (United Kingdom)|lieutenant general]].<ref>[http://www.thetimes.co.uk/tto/life/courtsocial/article3053824.ece Service Appointments] The Times, 8 June 2011</ref> On 24 January 2013 it was announced that he was to be appointed Commander [[Joint Forces Command]] in April 2013.<ref name="MOD"/> As of 2015, Barrons was paid a salary of between £175,000 and £179,999 by the department, making him one of the 328 most highly paid people in the British public sector at that time.<ref>{{Cite web |url=https://www.gov.uk/government/uploads/system/uploads/attachment_data/file/492289/150K_senior_salaries.csv/preview |title=Senior officials 'high earners' salaries as at 30 September 2015 |date=17 December 2015 |publisher=[[Cabinet Office]] |accessdate=13 March 2016}}</ref> In April 2016, he handed over command of Joint Forces Command to General Sir [[Christopher Deverell]].<ref>{{cite web|url=https://www.gov.uk/government/news/the-secretary-of-state-announces-new-senior-appointments-in-the-armed-services|title=The Secretary of State announces new Senior Appointments in the Armed Services|publisher=Ministry of Defence|accessdate=29 January 2016}}</ref>

==Other work==
Barrons co-authored a book, ''The Business General'', published by [[Ebury Publishing|Vermilion]], with Deborah Tom in 2006.<ref name="book">{{cite web |url=https://www.penguin.co.uk/books/1028462/the-business-general/ |title=The Business General |publisher=[[Ebury Publishing|Vermilion]] |accessdate=16 February 2011}}</ref> He has also lectured as a guest speaker, including at the [[University of Oxford]].<ref name="MOD"/> As of 2010, Barrons was chairman of the [[Royal Artillery Museum]].<ref name="belfast">{{cite news |url=http://www.belfasttelegraph.co.uk/breakingnews/breakingnews_ukandireland/museum-to-show-hero-soldiers-medal-28547527.html |title=Museum to show hero soldier's medal |date=17 July 2010 |newspaper=[[Belfast Telegraph]] |accessdate=17 February 2011}}</ref>

In May 2013, he was appointed [[Colonel Commandant]] of the [[Honourable Artillery Company]].<ref>{{cite web |url=http://www.hac.org.uk/home/news/articles/medals-parade-31-july-2013/ |title=Medals Parade |publisher=[[Honourable Artillery Company]] |date=31 July 2013 |accessdate=30 May 2015}}</ref>

==Personal life==
Barrons is married with two daughters.

==Honours and decorations==
Barrons was appointed a [[Member of the Order of the British Empire]] (MBE) in 1993 "in recognition of service during operations in the former Republic of Yugoslavia",<ref name="ReferenceA">{{London Gazette|issue=53333|supp=yes|startpage=34|date=11 June 1993}}</ref> and was promoted to Officer of the Order of the British Empire (OBE) in the [[2000 New Year Honours]].<ref name="ReferenceB">{{London Gazette|issue=55710|supp=yes|startpage=6|date=31 December 1999}}</ref> On 29 April 2003, he was promoted to Commander of the Order of the British Empire (CBE) "in recognition of gallant and distinguished services in Afghanistan during the period 1 April 2002 to 30 September 2002".<ref name="ReferenceC">{{London Gazette|issue=56920|supp=yes|startpage=5273|date=29 April 2003}}</ref> He was appointed a [[Knight Commander of the Order of the Bath]] (KCB) in the [[2013 Birthday Honours]].<ref name=gaz31712>{{LondonGazette|issue=60534|supp=yes|startpage=2|endpage=|date=15 June 2013}}</ref>

In 2004, Barrons was awarded his first [[Queen's Commendation for Valuable Service]] for services in Iraq the previous year,<ref name="LG 23 April 2004">{{London Gazette|issue=57269|supp=yes|startpage=5133|date=23 April 2004}}</ref> his second coming in 2006 in recognition of his service in Northern Ireland in 2005.<ref name="LG 24 March 2006">{{London Gazette|issue=57936|supp=yes|startpage=4193|endpage=4194|date=24 March 2006}}</ref> He was also awarded the United States [[Legion of Merit]] (Degree of Officer) "in recognition of gallant and distinguished services during coalition operations in Iraq".<ref>{{London Gazette|issue=59554|supp=yes|startpage=18539|endpage=18540|date=24 September 2010}}</ref>

{| class="wikitable"
|-
|[[File:Order of the Bath UK ribbon.png|50px]] || Knight Commander of the Order of the Bath (KCB) || [[2013 Birthday Honours]].<ref name="gaz31712"/>
|-
|rowspan=3| [[File:Order of the British Empire (Military) Ribbon.png|50px]] || Commander of the Order of the British Empire (CBE) || 2003 'in recognition of gallant and distinguished services in Afghanistan during the period 1 April 2002 to 30 September 2002'.<ref name="ReferenceC"/>
|-
| Officer of the Order of the British Empire (OBE) || [[2000 New Year Honours]].<ref name="ReferenceB"/>
|-
| Member of the Order of the British Empire (MBE) || 1993 "in recognition of service during operations in the former Republic of Yugoslavia",<ref name="ReferenceA"/>
|-
|[[File:Us legion of merit officer rib.png|50px]] || [[Legion of Merit|Officer of the Legion of Merit]] (United States) || 2010 'in recognition of gallant and distinguished services during coalition operations in Iraq'.<ref>{{London Gazette |issue=59554 |date=24 September 2010 |startpage=18539 |endpage=18540 |supp=yes}}</ref>
|-
|[[File:UK Queen's Commendation for Valuable Service device.svg|50px]] || [[Queen's Commendation for Valuable Service]] || 2003<ref name="LG 23 April 2004"/><br/>2005 <ref name="LG 24 March 2006"/>
|}

==References==
{{reflist|30em}}

{{s-start}}
{{s-mil}}
{{s-bef|before=[[Bruce Brealey]]}}
{{s-ttl|title=Deputy Commanding General [[Multi-National Corps – Iraq]]|years=2008–2009}}
{{s-aft|after=[[William Moore (British Army officer)|Bill Moore]]}}
|-
{{s-bef|before=[[James Bucknall]]}}
{{s-ttl|title=[[Assistant Chief of the General Staff (United Kingdom)|Assistant Chief of the General Staff]]|years=2010–2011}}
{{s-aft|after=[[James Everard]]}}
|-
{{s-bef|before=[[Simon Mayall]]}}
{{s-ttl|title=[[Deputy Chief of the Defence Staff|Deputy Chief of the Defence Staff (Operations)]]|years=2011–2013}}
{{s-aft|after=[[James Everard]]}}
|-
{{s-bef | before = [[Stuart Peach|Sir Stuart Peach]]}}
{{s-ttl | title = Commander, [[Joint Forces Command]]| years = 2013–2016}}
{{s-aft|after=[[Christopher Deverell|Sir Christopher Deverell]]}} 
{{s-end}}

{{DEFAULTSORT:Barrons, Richard}}
[[Category:1959 births]]
[[Category:British Army generals]]
[[Category:Royal Artillery officers]]
[[Category:Graduates of the Royal Military Academy Sandhurst]]
[[Category:Alumni of The Queen's College, Oxford]]
[[Category:Graduates of the Staff College, Camberley]]
[[Category:Commanders of the Order of the British Empire]]
[[Category:Knights Commander of the Order of the Bath]]
[[Category:Recipients of the Commendation for Valuable Service]]
[[Category:Officers of the Legion of Merit]]
[[Category:People of the Bosnian War]]
[[Category:British military personnel of the Troubles]]
[[Category:British Army personnel of the War in Afghanistan (2001–2014)]]
[[Category:Living people]]