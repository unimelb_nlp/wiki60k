{{italic title}}
{{Infobox book series
|name = ''Les Rougon-Macquart, Histoire naturelle et sociale d'une famille sous le Second Empire''
|image = Emile Zola - La Fortune des Rougon.djvu{{!}}page=3
|caption = First page of ''[[La Fortune des Rougon]]'', the first book of the series
|books = 20 books, see [[#List of the novels|this list]]
|author = [[Émile Zola]]
|country = France
|language = French
|genre = [[Naturalism (literature)|Naturalism]]
|publisher = 
|pub_date = 1871–1893
|media_type = [[Printing|Print]]
}}
'''''Les Rougon-Macquart''''' is the collective title given to a cycle of twenty novels by [[France|French]] writer [[Émile Zola]]. Subtitled '''''Histoire naturelle et sociale d'une famille sous le Second Empire''''' (''Natural and social history of a family under the Second Empire''), it follows the lives of the members of the two titular branches of a fictional family living during the [[Second French Empire]] (1852–1870) and is one of the most prominent works of the French [[Naturalism (literature)|naturalism literary movement]].

==Influences==
[[File:Zola Balzac.jpg|thumb|150px|Zola, with the book of the Rougon-Macquart under his arm, salutes the statue of [[Honoré de Balzac|Balzac]].]]
Early in his life, Zola discovered the work of [[Honoré de Balzac]] and his famous cycle ''[[La Comédie humaine]]''. This had a profound impact on Zola, who decided to write his own, unique cycle. However, in 1869, he explained in ''Différences entre Balzac et moi'', why he would not make the same kind of book as Balzac:

<blockquote>In one word, his work wants to be the mirror of the contemporary society. My work, mine, will be something else entirely. The scope will be narrower. I don't want to describe the contemporary society, but a single family, showing how the race is modified by the environment. (...) My big task is to be strictly naturalist, strictly physiologist.<ref>[[Bibliothèque nationale de France]], Manuscrits, NAF 10345, f. 14-15. Available online here [http://seacoast.sunderland.ac.uk/~os0tmc/zola/diff.htm] (in French)</ref></blockquote>

As a [[Naturalism (literature)|naturalist]] writer, Zola was highly interested by science and especially the problem of [[heredity]] and [[evolution]]. He notably read and mentioned the work of the doctor [[Prosper Lucas]],<ref>In a note included in ''[[Une Page d'amour]]''. Text available at [http://fr.wikisource.org/wiki/Une_page_d%E2%80%99amour wikisource] (in French)</ref> [[Claude Bernard]], and [[Charles Darwin]]<ref>In ''Le Roman expérimental'' (1888), Zola talks extensively about Claude Bernard and mentions the work of Charles Darwin. Text available at [http://fr.wikisource.org/wiki/Le_Roman_exp%C3%A9rimental wikisource] (in French)</ref> as references for his own work. This led him to think that people are heavily influenced by [[heredity]] and their environment. He intended to prove this by showing how these two factors could influence the members of a family. In 1871, in the preface of ''[[La Fortune des Rougon]]'', he explained his intent:

<blockquote>The great characteristic of the Rougon-Macquarts, the group or family which I propose to study, is their ravenous appetite, the great outburst of our age which rushes upon enjoyment. Physiologically the Rougon-Macquarts represent the slow succession of accidents pertaining to the nerves or the blood, which befall a race after the first organic lesion, and, according to environment, determine in each individual member of the race those feelings, desires and passions&mdash;briefly, all the natural and instinctive manifestations peculiar to humanity&mdash;whose outcome assumes the conventional name of virtue or vice.<ref>Extract from the author's preface of ''La Fortune des Rougon''. Original text in French available at [http://fr.wikisource.org/wiki/La_Fortune_des_Rougon_-_Préface wikisource], translated text by [http://www.gutenberg.org/etext/5135 the Project Gutenberg]</ref></blockquote>

==Preparations==
[[File:Plan Rougon 1860.jpg|thumb|right|100px|Letter by Zola to his publisher]]
In a letter to his publisher, Zola stated his goals for the Rougon-Macquart: "1° To study in a family the questions of blood and environments. [...] 2° To study the whole Second Empire, from the coup d'état to nowadays."<ref name="here">[[:Image:Plan Rougon 1860.jpg]]. Text available online here [http://emilezola.free.fr/d_genese.htm]</ref></blockquote>

===Genealogy and heredity===
Since his first goal was to show how heredity can affect the lives of descendants, Zola started working on the Rougon-Macquart by drawing the family tree for the Rougon-Macquart. Though it was to be modified many times over the years, with some members appearing or disappearing, the original tree shows how Zola planned the whole cycle before writing the first book.

The tree provides the name and date of birth of each member, along with certain properties of his heredity and his life:

* The [http://en.wiktionary.org/wiki/prepotency prepotency] : The prepotency is a term used by [[Prosper Lucas|the doctor Lucas]]. It is part of a biological theory that tries to determine how heredity transmits traits through generations.<ref>''Traité philosophique et physiologique de l'hérédité naturelle dans les états de santé et de maladie du système nerveux: avec l'application méthodique des lois de la procréation au traitement général des affections dont elle est le principe...'' Lucas, Prosper (1805–1885) Available online as part of the online archive of the [[Bibliothèque nationale de France|BNF]] here [http://gallica.bnf.fr/ark:/12148/bpt6k86272g]</ref> Zola apply this theory to the mental state of his protagonists and uses terms from the work of the doctor Lucas: ''Election du père'' (Prepotency of the father, meaning the father is the main influence on the child), ''Election de la mère'' (Prepotency of the mother), ''Mélange soudure'' (Fusion of the 2 parents) or ''Innéité'' (No influence from either parent).
* Physical likeness: Whether the member looks like his mother or his father.
* Biographical information: his job and important facts of his life. Additionally, for members still living at the end of ''[[Le Docteur Pascal]]'', their place of living at the end of the cycle may be included. Otherwise, the date of death is included.

<gallery>
Image:Tableau heredite.jpg|An early tree showing the heredity. Each family has a color, and each child is influenced by one or more families.
Image:Zola - Arbre généalogique.jpg|The 1878 tree, published in a note included in ''[[Une Page d'amour]]''
Image:Arbre genealogique des Rougon-Macquart annoté.jpg|The 1892 and final tree, as part of the preparation work for ''[[Le Docteur Pascal]]''
</gallery>

''Note : The gallery does not include the tree made for ''[[La Bete Humaine]]''<ref>BNF, Manuscrits, NAF 10274, f. 581</ref> which included for the first time Jacques, the main protagonist of the book''<ref>Information found here http://expositions.bnf.fr/zola/grand/z075.htm (in French)</ref>
 
For example, the entry for Jean Macquart on the 1878 tree read : ''Jean Macquart, né en 1831 - Election de la mère - Ressemblance physique du père. Soldat'' (Jean Macquart, born in 1831 - Prepotency of the mother - Physical likeness to his father. Soldier)

===The study of the Second Empire===
[[File:Zola-Liste-des-romans-1872.jpg|thumb|150px|Note by Zola (1872) mentioning 17 ideas of book. Some would never be made, others were to be added later on.]]
To study the Second Empire, Zola thought of each novel as a novel about a specific aspect of the life in his time. For example, in the list he made in 1872, he intended to make a "[[political fiction|political novel]]", a "novel about the defeat", "a scientific novel", and a "novel about the [[Napoléon III#Italy|war in Italy]]". The first three ideas led to ''[[Son Excellence Eugène Rougon]]'', ''[[La Débâcle]]'', and ''[[Le Docteur Pascal]]'', respectively. However, the last idea would never be made into a book.{{citation needed|date=October 2014}}

Indeed, at the beginning, Zola didn't know exactly how many books he would write. In the first letter to his publisher, he mentioned "ten episodes".<ref name="here"/> In 1872, his list included seventeen novels, but some of them would never be made (such as the one on the war in Italy), whereas others were to be added later on.<ref>[[:Image:Zola-Liste-des-romans-1872.jpg]]</ref> In 1877, in the preface of ''[[L'Assommoir]]'', he stated that he was going to write "about twenty novels".<ref>Original text available at [http://fr.wikisource.org/wiki/L%27Assommoir_-_Pr%C3%A9face wikisource] (in French).</ref> In the end, he settled for twenty books.

==Story==
Almost all of the main protagonists for each novel are introduced in the first book, ''[[La Fortune des Rougon]]''. The last novel in the cycle, ''[[Le Docteur Pascal]]'', contains a lengthy chapter that ties up loose ends from the other novels. In between, there is no "best sequence" in which to read the novels in the cycle, as they are not in chronological order and indeed are impossible to arrange into such an order. Although some of the novels in the cycle are direct sequels to one another, many of them follow on directly from the last chapters of ''La Fortune des Rougon'', and there is a great deal of chronological overlap between the books; there are numerous recurring characters and several of them make "guest" appearances in novels centered on other members of the family.

===The Rougon-Macquart===
The Rougon-Macquart family begins with Adelaïde Fouque. Born in 1768 in the fictional [[Provence|Provençal]] town Plassans to middle-class parents (members of the French "[[bourgeoisie]]"), she has a slight [[intellectual disability]]. She marries Rougon, and gives birth to a son, Pierre Rougon. However, she also has a lover, the smuggler Macquart, with whom she has two children: Ursule and Antoine Macquart. This means that the family is split in three branches:

* The first, legitimate, one is the Rougons branch. They are the most successful of the children. Most of them live in the upper classes (such as Eugene Rougon who becomes a minister) or/and have a good education (such as Pascal, the doctor who is the main protagonist of ''[[Le Docteur Pascal]]'').
* The second branch is the low-born Macquarts. They are blue-collar workers (''[[L'Assommoir]]''), farmers (''[[La Terre]]''), or soldiers (''[[La Débâcle]]'').
* The third branch is the Mourets (the name of Ursule Macquart's husband). They are a mix of the other two. They are middle-class people and tend to live more balanced lives than the others.

Because Zola believed that everyone is driven by their heredity, Adelaide's children show signs of their mother's original deficiency. For the Rougon, this manifests as a drive for power, money, and excess in life. For the Macquarts, who live in a difficult environment, it is manifested by alcoholism (''[[L'Assommoir]]''), prostitution (''[[Nana (novel)|Nana]]''), and homicide (''[[La Bête humaine]]''). Even the Mourets are marked to a certain degree; in ''[[La Faute de l'Abbé Mouret]]'', the priest Serge Mouret has to fight his desire for a young woman.

<small><pre>

                                      ┌─ Eugène Rougon      ┌─ Maxime Saccard ──── Charles Saccard
                                      │     1811–?          │     1840–1873          1857–1873
                                      │                     │
                                      ├─ Pascal Rougon   ───┼─ Clotilde Saccard ── A son
                                      │     1813–1873       │     1847–?             1874–?
                                      │                     │
                 ┌─ Pierre Rougon ────┼─ Aristide Saccard───┴─ Victor Saccard
                 │    1787–1870       │     1815–?                1853–?
                 │                    │
                 │                    ├─ Sidonie Touché ────── Angélique Marie
                 │                    │     1818–?                1851–1869
                 │                    │
                 │                    └─ Marthe Mouret ───┐ ┌─ Octave Mouret ──────────── A son and a daughter
                 │                          1819–1864     │ │     1840–?
                 │                                        │ │
                 │                                        ├─┼─ Serge Mouret
                 │                                        │ │     1841–?
                 │                                        │ │
                 │                    ┌─ François Mouret ─┘ └─ Désirée Mouret
                 │                    │     1817–1864             1844–?
                 │                    │
Adélaïde Fouque ─┼─ Ursule Macquart ──┼─ Hélène Rambeau ────── Jeanne Grandjean
   1768–1873     │     1791–1839      │     1824–?                1842–1855
                 │                    │
                 │                    └─ Silvère Mouret
                 │                          1834–1851
                 │
                 │                    ┌─ Lisa Quenu ─────── Pauline Quenu
                 │                    │     1827–1863             1852–?
                 │                    │
                 │                    │                     ┌─ Claude Lantier ─────────── Jacques-Louis Lantier
                 │                    │                     │     1842–1876                   1864–1876
                 │                    │                     │
                 └─ Antoine Macquart ─┼─ Gervaise Coupeau  ─┼─ Jacques Lantier
                       1789–1873      │     1829–1869       │     1844–1870
                                      │                     │
                                      │                     ├─ Étienne Lantier ────────── A daughter
                                      │                     │     1846–?
                                      │                     │
                                      │                     └─ Anna Coupeau ─── Louis Coupeau
                                      │                           1852–1870                   1867–1870
                                      │
                                      └─ Jean Macquart ─────── Two children
                                            1831–?
</pre></small>

===View of France under Napoleon III===
As a naturalist, Zola also gave detailed descriptions of urban and rural settings, and different types of businesses. ''Le Ventre de Paris'', for example, has a detailed description of the central market in Paris at the time.

As a political reflection of life under [[Napoleon III]], the novel ''La Conquête de Plassans'' looks at how an ambitious priest infiltrates a small [[Provence]] town one family at a time, starting with the Rougons. ''La Débâcle'' takes place during the 1870 [[Franco-Prussian War]] and depicts Napoleon III's downfall. ''Son Excellence'' also looks at political life, and ''Pot-Bouille'' and ''Au Bonheur des Dames'' look at middle class life in Paris.

Note that Zola wrote the novels after the fall of Napoleon III.

==List of the novels==
In an "Introduction" of his last novel, ''Le Docteur Pascal'', Zola gave a recommended reading order, although it is not required, as each novel stands on its own.<ref name=vitz>The reading order recommended by Zola can be found in [[Ernest Alfred Vizetelly]]'s  ''[https://archive.org/details/emilezolanovelis00vizerich Emile Zola, novelist and reformer: an account of his life & work]'' (1904),  [https://archive.org/stream/emilezolanovelis00vizerich#page/348/mode/2up ppg.348-364].</ref>
{{col-begin}}
{{col-break}}
'''Publication order'''
#''[[La Fortune des Rougon]]'' (1871)
#''[[La Curée]]'' (1871–2)
#''[[Le Ventre de Paris]]'' (1873)
#''[[La Conquête de Plassans]]'' (1874)
#''[[La Faute de l'Abbé Mouret]]'' (1875)
#''[[Son Excellence Eugène Rougon]]'' (1876)
#''[[L'Assommoir]]'' (1877)
#''[[Une Page d'amour]]'' (1878)
#''[[Nana (novel)|Nana]]'' (1880)
#''[[Pot-Bouille]]'' (1882)
#''[[Au Bonheur des Dames]]'' (1883)
#''[[La Joie de vivre]]'' (1884)
#''[[Germinal (novel)|Germinal]]'' (1885)
#''[[L'Œuvre]]'' (1886)
#''[[La Terre]]'' (1887)
#''[[Le Rêve (novel)|Le Rêve]]'' (1888)
#''[[La Bête humaine]]'' (1890)
#''[[L'Argent]]'' (1891)
#''[[La Débâcle]]'' (1892)
#''[[Le Docteur Pascal]]'' (1893)
{{col-break}}
'''A recommended reading order'''<ref name=vitz/>
#''[[La Fortune des Rougon]]'' (1871)
#''[[Son Excellence Eugène Rougon]]'' (1876)
#''[[La Curée]]'' (1871-2)
#''[[L'Argent]]'' (1891)
#''[[Le Rêve (novel)|Le Rêve]]'' (1888)
#''[[La Conquête de Plassans]]'' (1874)
#''[[Pot-Bouille]]'' (1882)
#''[[Au Bonheur des Dames]]'' (1883)
#''[[La Faute de l'Abbé Mouret]]'' (1875)
#''[[Une Page d'amour]]'' (1878)
#''[[Le Ventre de Paris]]'' (1873)
#''[[La Joie de vivre]]'' (1884)
#''[[L'Assommoir]]'' (1877)
#''[[L'Œuvre]]'' (1886)
#''[[La Bête humaine]]'' (1890)
#''[[Germinal (novel)|Germinal]]'' (1885)
#''[[Nana (novel)|Nana]]'' (1880)
#''[[La Terre]]'' (1887)
#''[[La Débâcle]]'' (1892)
#''[[Le Docteur Pascal]]'' (1893)
{{col-end}}

==English translation==
All of the twenty novels have been translated into English under various titles and editions. Many of the translations are, however, out-of-print or [[bowdlerized]] from the 19th and early 20th century.

Recent original English translations (post-1970) are available for six<!--ARE YOU 100% SURE THE BOOKS YOU ADDED ARE NOT REPRINTS OLD TRANSLATIONS? THEY USUALLY ARE.-->teen of the twenty novels: 
<!--READ ME FIRST: ANYTHING NOT BY PENGUIN, OXFORD, MODERN LIBRARY - ARE PROBABLY OLD TRANSLATIONS IN PUBLIC DOMAIN REPRINTED. -->

{{col-begin}}
{{col-break}}
1. ''The Fortune of the Rougons'' (''La Fortune des Rougon'')
:* [[Brian Nelson (literature professor)|Brian Nelson]] (Oxford 2012)<ref>''The Fortune of the Rougons''; first trans. by [[Brian Nelson (literature professor)|Brian Nelson]] in 2012. Oxford Worlds Classics. ISBN 978-0-19-956099-8</ref>
2. ''The Kill'' (''La Curée'')
:* [[Brian Nelson (critic)|Brian Nelson]] (Oxford 2004)<ref>''The Kill'' (''La Curée''); first trans. by Brian Nelson in 2004. Oxford World's Classics. ISBN 978-0-19-953692-4 (re-issue 2008)</ref>
:* [[Arthur Goldhammer]] (Modern Library 2004)<ref>''The Kill''; first trans. bu Arthur Goldhammer in 2004. Modern Library. ISBN 978-0-679-64274-9 (2004)</ref> 
3. ''The Belly of Paris'' (''Le Ventre de Paris'')
:* [[Brian Nelson (critic)|Brian Nelson]] (Oxford 2007)<ref>''The Belly of Paris'' (''Le Ventre de Paris''); first trans. by Brian Nelson in 2007. Oxford World's Classics. ISBN 978-0-19-280633-8 (2008)</ref> 
:* [[Mark Kurlansky]] (Modern Library 2009)<ref>''The Belly of Paris''; first trans. by [[Mark Kurlansky]] in 2009. Modern Library. ISBN 978-0-8129-7422-5 (2009)</ref>
4. ''The Conquest of Plassans'' (''La Conquête de Plassans'')
:* [[Helen Constantine]] (Oxford 2014)<ref>''The Conquest of Plassans'' (''La Conquête de Plassans''); first trans. by Helen Constantine in 2014. Oxford World's Classics. ISBN 978-0199664788 (2014)</ref> 
5. ''The Sin of Father Mouret'' (''La Faute de l'Abbé Mouret'')
:* [[Sandy Petrey]] (Prentice-Hall 1969)<ref>''The Sin of Father Mouret''; first trans. by Sandy Petrey in 1969 by Prentice-Hall. Latest edition is University of Nebraska Press. ISBN 978-0-8032-9901-6 (1983)</ref>
7. ''The Dram Shop'' (''L'Assommoir'')
:* [[Margaret Mauldon]] (Oxford 1995)<ref>''L'Assommoir''; first trans. by Margaret Mauldon in 1995. Oxford World's Classics (re-issued 1999). ISBN 978-0-19-283813-1</ref> 
:* [[Robin Buss]] (Penguin 2000)<ref>''The Drinking Den'' (''L'Assommoir''); first trans. by Robin Buss in 2000. Penguin Classics. ISBN 978-0-14-044954-9 (re-issued 2004)</ref>
9. ''Nana''
:* [[George Holden (translator)|George Holden]] (Penguin 1972)<ref>''Nana''. first trans. by George Holden in 1972. Penguin Classics. ISBN 978-0-14-044263-2 (1972)</ref>
:* [[Douglas Parmee]] (Oxford 1992)<ref>''Nana''; first trans. by Douglas Parmee in 1992. Oxford World's Classics. ISBN 978-0-19-283670-0 (re-issue 1999)</ref> 
10. ''Pot Luck'' (''Pot-Bouille'')
:* [[Brian Nelson (critic)|Brian Nelson]] (Oxford 1999)<ref>''Pot Luck'' (''Pot-Bouille''); first trans. by Brian Nelson in 1999. Oxford World's Classics. ISBN 978-0-19-283179-8 (1999)</ref>
11. ''The Ladies Paradise'' (''Au Bonheur des Dames'')
:* [[Brian Nelson (critic)|Brian Nelson]] (Oxford 1995)<ref>''The Ladies Paradise'' (''Au Bonheur des Dames''); first trans. by Brian Nelson in 1995. Oxford World's Classics. ISBN 978-0-19-953690-0 (re-issued 2008)</ref> 
:* [[Robin Buss]] (Penguin 2001)<ref>''Au Bonheur des Dames''; first trans. by Robin Buss in 2001. Penguin Classics. ISBN 978-0-14-044783-5 (re-issued 2004)</ref>
{{col-break}}

<!--READ ME FIRST: ANYTHING NOT BY PENGUIN, OXFORD, MODERN LIBRARY - ARE PROBABLY OLD TRANSLATIONS IN PUBLIC DOMAIN REPRINTED. -->

13. ''Germinal''
:* [[Stanley Hochman]] (Signet 1970)<ref>''Germinal''. first trans. by Stanley Hochman in 1970. Signet Classics. ISBN 978-0-451-51975-7 (1970)</ref>
:*[[Peter Collier (professor)|Peter Collier]] (Oxford 1993)<ref>''Germinal'', first trans. by Peter Collier in 1993. Oxford World's Classics. ISBN 978-0-19-953689-4 (re-issued 2008)</ref> 
:*[[Roger Pearson (linguist)|Roger Pearson]] (Penguin 2004)<ref>''Germinal''; first trans. by Roger Pearson in 2004. Penguin Classics. ISBN 978-0-14-044742-2 (2004)</ref>
14. ''The Masterpiece'' (''L'Œuvre'')
:* [[Thomas walton (classicist)|Thomas Walton]] (Oxford 1993)<ref>''The Masterpiece'' (''L'Œuvre''); first trans. by Thomas Walton in 1993. Oxford World's Classics. ISBN 978-0-19-953691-7 (re-issued 2008)</ref>
15. ''The Earth'' (''La Terre'')
:* [[Douglas Parmee]] (Penguin 1980)<ref>''The Earth'' (''La Terre''); first trans. by Douglas Parmee in 1980. Penguin Classics. ISBN 978-0-14-044387-5 (re-issued 2002)</ref>
:* [[Brian Nelson (literature professor)|Brian Nelson]] (Oxford 2016) <ref>''The Earth''; first trans. by Brian Nelson in 2016. Oxford World's Classic. ISBN 978-0199677870</ref>
16. ''The Dream'' (''Le Rêve'')
:* [[Michael Glencross]] (Peter Owen Ltd 2005)<ref>''The Dream'', first trans. by Michael Glencross in 2005. Peter Owen Ltd. ISBN 978-0-7206-1253-0 (2005).</ref>
:*Andrew Brown ([[Hesperus Press]] 2005)<ref>''The Dream'', first trans. by Andrew Brown in 2005. [[Hesperus Press]]. ISBN 978-1-84391-114-2 (2005)</ref>
17. ''The Beast Within'' (''La Bête humaine'')
:* [[Leonard Tancock]] (Penguin 1977)<ref>''La Bete Humaine''; first trans. by Leonard Tancock in 1977. Penguin Books. ISBN 0-14-044327-4 (1977)</ref>
:* [[Roger Pearson (linguist)|Roger Pearson]] (Oxford 1999)<ref>''La Bete Humaine''; first trans. by Roger Pearson in 1999. Oxford World's Classics. ISBN 978-0-19-283814-8 (1999)</ref>
:* [[Roger Whitehouse]] (Penguin 2008)<ref>''The Beast Within'' (''La Bete Humaine''); first trans. by Roger Whitehouse in 2008. Penguin Classics. ISBN 978-0-14-044963-1 (2008)</ref>
18. ''Money'' (''L'Argent'')
:*[[Valerie Minogue]] (Oxford 2014)<ref>''Money''; first trans. by Valerie Minogue in 2014. Oxford World's Classics. ISBN 978-0199608379 (2014)</ref>
19. ''The Debacle'' (''La Débâcle'')
:* [[Leonard Tancock]] (Penguin 1973)<ref>''The Debacle'' (''La Debacle''); first trans. by Leonard Tancock in 1973. Penguin Classics. ISBN 978-0-14-044280-9 (1973)</ref>
:* [[Elinor Dorday]] (Oxford 2000)<ref>''La Débâcle'', first trans. by Elinor Dorday in 2000. Oxford World's Classics. ISBN 978-0-19-282289-5 (2000)</ref>
{{col-end}}

==Adaptions==
The BBC adapted the novels into a seven-hour radio drama series called ''Blood, Sex and Money by Emile Zola''. The "radical re-imagining" was broadcast in three parts on [[BBC Radio 4]] between November 2015 and October 2016.<ref name="Blood, Sex and Money BBC Radio 4">{{cite web |url= http://www.bbc.co.uk/programmes/b06pydll|title= Blood, Sex and Money by Emile Zola |author= <!--Staff writer(s); no by-line.--> |date= |website= [[BBC News Online]] |publisher= BBC | accessdate= 25 October 2016 }}</ref>

==References==
{{reflist|colwidth=30em}}

==External links==
* [http://www.well.com/user/jax/literature/Rougon-Macquart.html ''The Rougon-Macquart'' Novels of Emile Zola (for English-speaking Readers)] provides an American enthusiast's introduction, insights and synopses.

{{Les Rougon-Macquart}}
{{Émile Zola}}

{{Authority control}}

{{DEFAULTSORT:Rougon-Macquart}}
[[Category:Books of Les Rougon-Macquart| ]]
[[Category:Les Rougon-Macquart| ]]
[[Category:Novel series]]