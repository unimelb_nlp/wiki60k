{{Infobox journal
| title = Current Sociology
| cover = [[File:Current Sociology journal front cover.jpg]]
| editor = Eloisa Martin 
| discipline = [[Sociology]]
| former_names = 
| abbreviation = Curr. Sociol.
| publisher = [[Sage Publications]]
| country = 
| frequency = Bimonthly
| history = 1952-present
| openaccess = 
| license = 
| impact = 1.154
| impact-year = 2013
| website = http://www.sagepub.com/journals/Journal200820/title
| link1 = http://csi.sagepub.com/content/current
| link1-name = Online access
| link2 = http://csi.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 613085705
| LCCN = 55057288
| CODEN = 
| ISSN = 0011-3921
| eISSN = 1461-7064
}}
'''''Current Sociology''''' is a bimonthly [[peer-reviewed]] [[academic journal]] covering the field of [[sociology]]. It is an official journal of the [[International Sociological Association]]<ref>[http://www.isa-sociology.org/publ/is.htm Journal page at Society's website.] Retrieved 17 May 2011.</ref> and is published on their behalf by [[Sage Publications]] It was established in 1952.<ref name=encycl83>{{cite book |doi= 10.1111/b.9781405124331.2007.x |title=The Blackwell Encyclopedia of Sociology |year=2007 |editor1-last=Ritzer |editor1-first=George|isbn=9781405124331 |last=Hill |first=Michael R. |chapter=Timeline |url=http://www.blackwellreference.com/public/book.html?id=g9781405124331_9781405124331|page=83}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2013 [[impact factor]] is 1.154, ranking it 43rd out of 138 in the category "Sociology".<ref name=WoS>{{cite book |year=2013 |chapter=Journals Ranking by Impact: Sociology |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |work=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

==External links==
* {{Official website|http://www.sagepub.com/journals/Journal200820/title}}
* [http://www.isa-sociology.org/publ/cs.htm Journal page] on [[International Sociological Association]] website


[[Category:SAGE Publications academic journals]]
[[Category:Bimonthly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1952]]
[[Category:Sociology journals]]
[[Category:Academic journals associated with international learned and professional societies]]