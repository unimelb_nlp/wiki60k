{{Cleanup|date=August 2010}}

'''Adult Children of Alcoholics''' ('''ACA''' or '''ACOA''') is an organization that is intended to provide a forum to individuals who desire to recover from the effects of growing up in an alcoholic or otherwise [[dysfunctional family]]. ACA membership has few formal requirements. ACA does not receive any outside economic contributions, but is self-supporting through donations from its members. The organization is not related to any particular religion and has no political affiliation. World Service Organization published the ACA Fellowship Text (Steps and Traditions Book) in November 2006. Known as the Big Red Book, the ACA Text is a 646-page hardback book that outlines the problem and solution for recovering from the effects of growing up in an alcoholic or otherwise dysfunctional family.

==History and growth==
ACA (Adult Children of Alcoholics) started as "Post Teen" in Mineola, Long Island in 1973 and later changed its name to ACOA.  It started in New York City, USA appr. 1978. [[Tony A.]] was among the founders.
ACA is based on the 12 steps and 12 traditions of AA.<ref name="responsesidetherapy.com">[http://responsesidetherapy.com/pdf/ACA_Support_Groups.pdf Adult Children of Alcoholics Support Groups: A valuable adjunct in treating clients]; Martin R. Smith, Gladys T. Patterson; 1992.</ref>

The name is often ascribed to [[Janet G. Woititz]] (c. 1939 - June 7, 1994), an American psychologist and researcher best known for her writings and lectures about the adult children of alcoholic parents.,<ref>{{cite news|url=https://www.nytimes.com/1994/06/14/obituaries/janet-g-woititz-55-author-who-studied-alcoholics-children.html|title=Janet G. Woititz, 55, Author Who Studied Alcoholics' Children|work=[[The New York Times]]|author=Wolfgang Saxon|date=June 14, 1994|accessdate=June 3, 2015}}</ref> Including the  1983 book, ''Adult Children of Alcoholics''.<ref>{{cite book|author=Janet G. Woititz|title=Adult Children of Alcoholics: Expanded Edition|isbn=978-1-55874-112-6|year=1983|url=https://books.google.nl/books?id=NlihAgAAQBAJ&printsec=frontcover&dq=Adult+Children+of+Alcoholics&hl=en&sa=X&ei=MndvVZmxGYOO7AaQtoGYBg&redir_esc=y#v=onepage&q=Adult%20Children%20of%20Alcoholics&f=false|publisher=Health Communications}}</ref><ref name=NYTObit>Saxon, Wolfgang. [https://www.nytimes.com/1994/06/14/obituaries/janet-g-woititz-55-author-who-studied-alcoholics-children.html "Janet G. Woititz, 55, Author Who Studied Alcoholics' Children"], ''[[The New York Times]]'', June 14, 1994. Retrieved October 10, 2013. "Dr. Janet G. Woititz, a best-selling author, lecturer and counselor to the troubled offspring of alcoholics, died last Thursday at her home in Roseland, N.J. She was 55."</ref>

"In November 1989, there were [...] more than 1,300 meetings registered with Adult Children of Alcoholics (ACA) ..." <ref>"The ACOA
marketplace" (Lily Collet: The pseudonym of a Mill Valley, California writer whose work has been published in the New Yorker,
Mother Jones, The Washington Post, and elsewhere), The Family Therapy Networker (Editor: Richard Simon, PhD), January/February 1990, p31.</ref>

In appr. 2003 there were an estimated 40,000 members of ACA.<ref>Self-help organizations for alcohol and drug
problems: Toward evidence-based practice and policy; Keith Humphreys et al; Journal of substance abuse treatment 26 (2004);
</ref>

Today there are about 1,300 groups in the whole world, about 779 of these in USA and this number is growing by leaps and bounds on a daily basis. While ACoA does bear similarities to other twelve-step fellowships, on the whole, ACoA is a deeply therapeutic program that emphasizes on taking care of the self and re-parenting one's own wounded inner child with love. As cited in the ACA Red Book Solution... We go from "hurting, to healing, to helping." The focal point across the board is not to diminish nor sulk in how "sick" or "defected" we are (as several other 12-step fellowships might suggest through quite abrasive terminology). Rather, ACA's aim is to build oneself up, assuming personal responsibility while equivocally standing up for one's immovable right to deserve a healthful life and achieve it. The collective stance is not to wallow in victim-hood but to move into the practical application of seeing family dysfunction as a generational affliction and a pattern that can be healed. As a fellowship and through the support of ACA's literature, sponsors and peers, the fellow comes to learn that even the most wounded of them, was someone's son or daughter. The crux of the community and its mindfulness comes through with honest accounts of struggles and sincere compassion towards these.

==Organization==
ACA is organized along the lines of [http://www.adultchildren.org/lit/Steps.s 12 steps] and [http://www.adultchildren.org/lit/Traditions.s 12 traditions]. Regarding how, when and where [http://www.allone.com/12/aca meetings] are held and led, ACA works the same way as AA. 

"The vast majority of ACAs [http://www.allone.com/12/aca meet] informally, in school classrooms or church halls, in the evenings or over
weekends. Few frequent expensive treatment centres. They are sympathetic to, but not part of, the AA
movement. They meet in leaderless groups, pooling their resources of experience and insight, and reading
relevant literature to deepen those assets. For an ACA, this support group provides the extended family
and unconditional support which he or she never experienced. The group further provides practical help in
acquiring everyday interpersonal and coping skills, and,with them, the sense of self-efficacy—a basic
need, as Peele says. The group also provides a sense of community, a community of interest which there
are few neighbourhood groups nowadays to provide. This sense of community is another basic need, as
Peele argues. Membership comes from a felt need, not as a life sentence. AA puts it simply: 'People need
people.'"<ref>The Adult Children of Alcoholics movement: Help for the unseen victims of alcoholism. By:
Carney, T.F., Guidance & Counseling, 08315493, May91, Vol. 6, Issue 5</ref>

===ACA program===
What is the [http://www.adultchildren.org/lit/Problem.s problem]: "By attending these [http://www.allone.com/12/aca meetings] on a regular basis, you will come to see parental alcoholism or family dysfunction for what it is: a disease that infected you as a child and continues to affect you as an adult." <ref>[http://www.adultchildren.org/lit/Solution.s "The solution"]</ref>

The goal of working the program is emotional sobriety.<ref name="responsesidetherapy.com"/>

In 2006, ACA published a [http://www.shop.adultchildren.org/Fellowship-Text-Hard-Cover-100-1.htm book] of 646 pages, describing in details what the program is and how it works.

===The 12 steps===
ACA offers a program to recover from the effects of growing up in an alcoholic or otherwise dysfunctional family. This program is known as [[Twelve-step program#Twelve Steps|the 12 steps]]. The program is used by other fellowships, mainly [[Alcoholics Anonymous]].

==Recommendations==
"Around AA groups different subgroups have formed to support the families: [[Al-Anon/Alateen|Al-Anon]] [...] Alateen [...] as well as groups for the adult child, ACoA. Discovering
not being alone with ones problems is often a great help for these youths, and the contact with other youths with the same
worries can help them get out of minimizing and denying the problem."<ref>It will never happen to me - Growing Up With Addiction As Youngsters, Adolescents, Adults; Chapter 8, Resources; [[Claudia Black (author)|Claudia Black, Ph. D.]] (Quoted and translated from the Swedish translation of the book)</ref>

Dr. Janet G. Woititz, author of ''Adult Children of Alcoholics'', endorsed the ACA.<ref>Dr. Woititz's doctoral dissertation was on The Self Esteem of Children of Alcoholics. Dr. Janet Geringer Woititz obtained her Doctorate in Education from Rutgers University.</ref>

November, 1985 "ACA celebrated its one-year anniversary as an official organization in Southern California. But ACA meetings have been taking place on an informal basis in Los Angeles for more than five years, helping some of the estimated 50 million adults who come from homes of alcoholics. ACA may often be referred to through therapists and local meetings can be found through a brief online search. (See "External Links" below)<ref>Jim Crogan, [http://pqasb.pqarchiver.com/latimes/access/64574928.html?dids=64574928:64574928&FMT=ABS&FMTS=ABS:FT&type=current&date=Nov+21%2C+1985&author=JIM+CROGAN&pub=Los+Angeles+Times+%28pre-1997+Fulltext%29&desc=%60I+thought+I+left+it+all+behind+me+when+I+left+my+parents%27+house.+But+I+was+wrong.%27+Children+of+Alcoholics+Put+Painful+Pasts+Behind+Them&pqatl=google `I thought I left it all behind me when I left my parents' house. But I was wrong.' Children of Alcoholics Put Painful Pasts Behind Them]; Los Angeles Times; November 21, 1985.</ref>

==See also==
*[[Alcoholism in family systems]]

== References ==
{{Reflist}}

== External links ==
*[http://www.adultchildren.org/ Home page of ACA WSO]
*[http://www.adultchildrenofalcoholics.co.uk/ Home page of ACA UK]
*[http://www.aca-arizona.org/ Home page of ACA Arizona Intergroup and the ACA Arizona Retreat on Mingus Mountain]
*[http://www.socalaca.org/ Home Page of ACA Southern California Adult Children of Alcoholics]
{{DEFAULTSORT:Adult Children Of Alcoholics}}
[[Category:Twelve-step programs]]