{{Infobox airport
| name                   = Cairns Army Airfield (AAF)
| nativename             = Fort Rucker
| image                  = Cairns Army Airfield.jpg
| image-width            = 250
| caption                = [[National Agriculture Imagery Program|NAIP]] aerial image, 30 June 2006
| image2                 = Cairns_Army_Airfield.png
| image2-width           = 150
| caption2               = [[FAA]] diagram of runway area
| FAA                    = OZR
| IATA                   = OZR
| ICAO                   = KOZR
| type                   = Military
| owner                  = [[U.S. Army]]
| operator               = 
| location               = [[Fort Rucker]] / [[Dale County, Alabama]]
| built                  = 
| used                   = 
| elevation-m            = 92
| elevation-f            = 301
| coordinates            = {{coord|31|16|33|N|085|42|48|W|region:US-AL_type:airport|name=Cairns Army Airfield|display=inline,title}}
| website                = {{URL|http://www-rucker.army.mil/}}
| pushpin_map            = Alabama
| pushpin_label_position = bottom
| pushpin_label          = '''KOZR'''
| pushpin_mapsize        = 250
| pushpin_map_caption    = Location of Cairns Army Airfield
| r1-number              = 6/24
| r1-length-m            = 1,386
| r1-length-f            = 4,546
| r1-surface             = Asphalt
| r2-number              = 18/36
| r2-length-m            = 1,532
| r2-length-f            = 5,025
| r2-surface             = Asphalt
| footnotes              = Source: [[Federal Aviation Administration]]<ref name="FAA">{{FAA-airport|ID=OZR|use=PR|own=MR|site=00329.*A}}. Federal Aviation Administration. Effective 3 June 2010.</ref>
}}

'''Cairns Army Airfield''' {{airport codes|OZR|KOZR|OZR}} is a military [[airport]] forming a part of [[Fort Rucker]], in [[Dale County, Alabama|Dale County]], [[Alabama]], [[United States|USA]] and is owned by the [[United States Army]].<ref name=FAA />  The airfield is south of the town of [[Daleville, Alabama|Daleville]], which sits between it and the main post.

==History==
In September 1942, {{convert|1259|acre|km2|1}} south of Daleville were acquired for the construction of an airfield to support the training camp.  It was a training airfield as part of the [[United States Army Air Forces]] [[Third Air Force]] during World War II, then placed on inactive status with the war's end.
 
Needing a location to shoot all takeoffs and landings for the 1949 film ''[[Twelve O'Clock High]]'', including the spectacular [[B-17 Flying Fortress]] [[belly landing]] sequence early in the film, director Henry King selected Ozark since its dark runways more closely matched wartime bases in England as opposed to the light-colored runways at nearby [[Eglin Air Force Base]], Florida, the primary shoot location. Since the field had been allowed to overgrow during its inactive status, it was also an ideal location for the character Harvey Stovall to post-war reminisce about his World War II service (which is seen at the beginning of the film) before the crew mowed and dressed the field to start the rest of shooting.<ref>http://www.airforce-magazine.com/MagazineArchive/Pages/2011/January%202011/0111high.aspx</ref>

Released by the [[U.S. Air Force]] as excess, the field was subsequently acquired by the [[U.S. Army]] as part of the Fort Rucker complex in 1952.  It was known as Ozark Army Air Field until January 1959, when the name was changed to Cairns Army Air Field, named for U.S. Army Major General Bogardus Snowden "Bugs" Cairns, who was killed instantly when his [[H-13 Sioux]] helicopter crashed minutes after take off in dense woods northwest of the [[Fort Rucker]], Alabama headquarters on 9 December 1958. He was en route to Matteson Range to observe a firepower rehearsal in preparation for a full-scale [[armed helicopter]] display. He was commander of the Aviation Center and Commandant of the Aviation School.<ref>[http://www.globalsecurity.org/military/facility/cairns.htm Cairns Army Airfield]. Globalsecurity.org. Retrieved on 2010-11-25.</ref><ref>[http://archiver.rootsweb.ancestry.com/th/read/CAIRNS/2008-09/1222128129 RootsWeb: CAIRNS-L Re: [CAIRNS&#93; Frederick Augustus Cairns]. Archiver.rootsweb.ancestry.com. Retrieved on 2010-11-25.</ref>
<ref>{{Air Force Historical Research Agency}}</ref>

==Current use==
Fort Rucker is the Home of Army Aviation, where all of the Army's Aviators as well as many international and civilian personnel begin their rotary-wing flight training.

Cairns is the busiest airfield in the Army, training large numbers of Army aviators both day and night with an average annual traffic count of approximately 240,000 movements.

The Aviation Training Brigade consists of five battalions that conduct flight training at Fort Rucker at three training sites. 1st Battalion, 223d Aviation Regiment, Cairns Army Airfield and Knox Army Heliport conducts flight training in the CH-47, C-12 and conducts the Maintenance Test Pilot Courses, it also provides evaluations of flight training for contractors.

==Facilities==
Cairns AAF has two [[asphalt]] paved [[runway]]s: 6/24 is 4,546 by 150 feet (1,386 x 46 m) and 18/36 is 5,025 by 150 feet (1,532 x 46 m).<ref name="FAA" />

In 2012 the controllers handled 245,000 aircraft movements without incident. In an average day tower operators handle 800 to 1,000 movements. What makes Cairns unique is the density in which it operates. The airfield may be launching 70 to 120 aircraft in the morning and afternoon, and then 50 or 60 at night for training.

==See also==
{{Portal|United States Air Force|Military of the United States|World War II}}
* [[Alabama World War II Army Airfields]]
* [[Fort Rucker]], located at {{coord|31|20|37|N|85|42|29|W|region:US-AL_type:airport|name=Fort Rucker}}
* [[Lowe Army Heliport]], located at {{coord|31|21|21|N|085|45|04|W|region:US-AL_type:airport|name=Lowe Army Heliport}}
* [[Hanchey Army Heliport]], located at {{coord|31|20|46|N|085|39|16|W|region:US-AL_type:airport|name=Hanchey Army Heliport}}

==References==
{{Air Force Historical Research Agency}}
{{Reflist}}

==External links==
{{GeoGroupTemplate}}
* [http://www-rucker.army.mil/ Fort Rucker], official site
* [https://www.attc.army.mil/ U.S. Army Aviation Technical Test Center], official site
* [http://www.globalsecurity.org/military/facility/cairns.htm Cairns Army Airfield] at GlobalSecurity.org
* [http://creports.capnhq.gov/airfield/ViewAirport.asp?view=Thumb&state=AL&code=KOZR&filetype=_ Airfield photos for KOZR] from [[Civil Air Patrol]]
* [http://msrmaps.com/map.aspx?t=1&s=12&lat=31.2785&lon=-85.714&w=500&h=650&lp=---+None+--- Aerial image as of 18 February 1997] from [[USGS]] ''[[The National Map]]''
* {{FAA-diagram|00577}}
* {{FAA-procedures|OZR}}
* {{US-airport-mil|OZR}}

<!--Navigation box--><br />
{{ALMilitary|state=collapsed}}
{{USAAF Training Bases World War II}}
{{AL Airport}}

[[Category:1943 establishments in Alabama]]
[[Category:Airfields of the United States Army Air Forces in Alabama]]
[[Category:Airports in Alabama]]
[[Category:Buildings and structures in Dale County, Alabama]]
[[Category:United States Army airfields]]
[[Category:Transportation in Dale County, Alabama]]
[[Category:Airports established in 1943]]