{{Use mdy dates|date=January 2016}}
{{Infobox artist
|name= Sol Kjøk
|image=Sol Kjok Portrait.jpg
|caption=Sol Kjøk in her New York studio
|birth_name=Solveig Kjøk
|birth_date=16 March 1968
|birth_place=Lillehammer, Norway
|nationality=Norwegian, U.S. Resident
|education=Parsons School of Design, NYC<vr>University of Cincinnati-DAAP,br>Sorbonne, Paris<br>Universität Wien, Vienna
|known_for=Painting, drawing
}}
'''Sol Kjøk''' (born 16 March 1968) is a [[norwegians|Norwegian]]-born, NYC-based visual artist and founder of NOoSPHERE Arts, a nonprofit exhibition and performance venue on the [[Lower East Side]] in Manhattan, NYC.<ref>[http://www.aftenposten.no/kultur/article4152040.ece Norway's most popular gallery in New York]</ref> She lives and works at The Mothership NYC, the arts collective she founded in Brooklyn in 2005.<ref>{{cite web|url=http://www.fin-mag.no/filter/print/FIN-1|title=FIN|publisher=}}</ref> In 2015, she started Last Frontier NYC, a new collaborative arts platform for international artists and performers, calling it "a campfire where the creative tribe can share its stories."<ref>{{cite web|url=http://www.last-frontier.nyc/|title=ABOUT|publisher=}}</ref><ref>{{cite web|url=http://bedfordandbowery.com/2015/11/a-new-gallery-with-epic-views-of-shit-creek-will-host-the-next-wild-torus-event/|title=A New Gallery With Epic Views of Shit Creek Will Host the Next Wild Torus Event|publisher=}}</ref>

==Life and career==

Sol Kjøk was born in Norway, where she grew up partly in [[Lillehammer]], partly in [[Valdres]].<ref>{{cite web|url=http://issuu.com/byavis/docs/byavis_2015_10|title=Byavis 2015 10|publisher=}}</ref> She has drawn since her early [[childhood]] and had her first professional exhibition when she was sixteen.<ref name="americanscandinavian.org">{{cite web|url=http://americanscandinavian.org/sol-kjok/|title=Sol Kjøk|date=May 30, 2012|publisher=}}</ref> Since then, her work has been in shown in 100 + shows worldwide in some twenty countries, including eight solo exhibits in the United States and Europe.<ref name="americanscandinavian.org"/> After studying in Paris, Vienna, [[Medellín]] and the US, she obtained four graduate degrees, including a [[:fr:Magistère (diplôme)|Magistère]] from the [[Sorbonne]], [[The University of Paris]], a Master of Arts in [[art history]] from the [[University of Cincinnati]] and an MFA in Painting from [[Parsons The New School for Design]].<ref>{{cite web|url=http://blogs.newschool.edu/class-notes/2011/05/sol-kjok/|title=Sol Kjok ’98, MFA, Painting  - Class Notes|publisher=}}</ref><ref name="galleriramfjord.net">{{cite web|url=http://www.galleriramfjord.net/349632129|title=SOL KJØK (1968)|publisher=}}</ref> Kjøk has received over fifty awards and grants, including from the Tallinn International Drawing Triennial,<ref>{{cite web|url=http://aeqai.com/main/2012/08/sol-kj%C3%B8k-awarded/|title=Sol Kjøk awarded AEQAI|publisher=}}</ref> the [[National Academy Museum and School]] in New York,<ref>{{cite web|url=http://www.collegeart.org/membernews/grantsawardshonors-2010-09|title=Member News - College Art Association - CAA - Advancing the history, interpretation, and practice of the visual arts for over a century|first=College Art|last=Association|publisher=}}</ref> [[Robert Rauschenberg]] Foundation, [[Arts Council Norway]], Office for Contemporary Art Norway,<ref>{{cite web|url=http://www.oca.no/grants/international/recipients/2010/11/2010-11-november-sol-kj-k|title=OCA: Grants: Recipients|first=Dorian|last=Moore|publisher=}}</ref> Vederlagsfondet, The [[Puffin Foundation]], The Relief Fund for Visual Artists, [[Knut Hamsun]]'s Award, [[The George Sugarman Foundation]] and [[the American-Scandinavian Foundation]].<ref>{{cite web|url=http://www.solkjok.com/resume.html|title=Sol Kjøk: Pages|publisher=}}</ref> In addition, she has received project support from the Royal Norwegian Embassies in Beijing, Berlin, Manila, New York and Tallinn. She has taught in art schools and universities as well as lectured at museums and art centers throughout Europe and the US.<ref name="galleriramfjord.net"/><ref>{{cite web|url=http://www.seattleweekly.com/2006-04-12/arts/on-the-walls/|title=On the Walls|publisher=}}</ref><ref>[http://www.manifestgallery.org/exhibits_archive/zakic_kjok/press/zakic_kjok.release2-print.pdf Manifest Gallery Lecture/Exhibit]</ref> Her works, which originate as performance, are in the permanent collections of the [[Cincinnati Art Museum]], the [[:sv:Teckningsmuseet i Laholm|Teckningsmuseet]] in Laholm, Sweden, the [[Kunsthaus Tacheles]] in Berlin, Germany and The Osten Museum of Drawings in [[Skopje]], [[Republic of Macedonia|Macedonia]],<ref>[http://www.osten.com.mk/drawing.aspx?mid=2&smid=40&archive=true&aid=40&lang=2 Osten Prize]</ref> as well as numerous private and corporate collections throughout the world.<ref name="galleriramfjord.net"/> She was chosen as one of twenty-one [[contemporary artists]] to be featured alongside [[Old Master]]s in the widely used [[textbook]] Drawing Essentials by Deborah A. Rockman<ref name="auto">{{cite web|url=https://books.google.com/books?id=zFlLAQAAIAAJ|title=Drawing Essentials: A Guide to Drawing from Observation|first=Deborah A.|last=Rockman|date=January 1, 2009|publisher=Oxford University Press|via=Google Books}}</ref> published by [[Oxford University Press]].<ref name="auto"/><ref>{{cite web|url=http://udel.edu/~davebrin/Fall%202010_Art%20110_%20Line%20examples.htm|title=Fall 2010 Drawing 110 - Line Quality|publisher=}}</ref>

==Work==
[[File:01 Sol Kjok Diptych.jpg|thumb|left|''Strings Attached'' and ''Stitched with Its Color'', 2008-2013]]
[[File:02 Sol Kjok Getting-There.jpg|thumb|''Getting There'', 2014]]

Kjøk specializes in painting, drawing and performance with a focus on the [[nude (art)|nude]] [[figure drawing|figure]].<ref>{{cite web|url=http://www.isbns.sh/isbn/9783981250350|title=ISBN 9783981250350 > Sol Kjøk, entre sol et ciel: 24 April - 15 May 2009, Art House Tacheles Berlin by  Kunsthaus Tacheles Berlin > Compare Discount Book Prices & Save up to 90% > isbns.sh|publisher=}}</ref> New York art critic Meghan Dailey describes her core themes as "the [[human body|body]], its limits and the tension between its strength and its vulnerability."<ref>{{cite web|url=http://www.amazon.de/Sol-Kj%C3%B8k-ENTRE-SOL-CIEL/dp/3981250354|title=Sol Kjøk: ENTRE SOL ET CIEL|first1=Tacheles|last1=e.V|first2=Martin|last2=Reiter|first3=Donald|last3=Kuspit|first4=Meghan|last4=Dailey|first5=Gisella|last5=Sorrentino|first6=Joe|last6=Grant|first7=Petrea|last7=Frid|first8=Les|last8=Whyte|first9=Matilde|last9=Soligno|first10=Barbara|last10=Fragogna|date=January 1, 2009|publisher=Tacheles e.V.|via=Amazon}}</ref> In contrast to other artists who portray the nude at rest, Kjøk's bodies are usually in constant motion. This dynamic quality springs from her unique, highly athletic, immersive artistic technique. Her work begins in her studio, with she and her models engaging in an acrobatic process, "which incorporates picture taking, laborious posing and performance, cutting, [[collage|collaging]], rearranging and finally meditative reassemblage through drawing and painting..."<ref name="Swirling">{{cite web|url=http://www.amazon.com/Swirling-Sviv-Solveig-Kj%C3%B8k-Nov--Dec/dp/B0010YHOY0/ref=sr_1_1?ie=UTF8&qid=1424312565&sr=8-1&keywords=solveig%20kjok|title=Swirling|first1=Sol|last1=Kjøk|first2=Donald B.|last2=Kuspit|first3=Petrea|last3=Frid|date=January 1, 2001|publisher=Tegnerforbundet Galleri [The Drawing Art Association of Norw|via=Amazon}}</ref>

Kjøk, a marathoner, approaches large-scale wall drawings as an "extreme sport". Of her 2009 solo-exhibit at [[Kunsthaus Tacheles]], it was reported; "The wall work in Berlin was made in one week, during which Sol Kjøk lived in the gallery and worked virtually around the clock for seven intense consecutive days."<ref>Ida Svingen Mo,"Analog Amour" translated from Norwegian SNITT: Magasin for visuell kommunikasjon, No. 6, 2009 {{Interlanguage link multi|Snitt (magazine)|no|3=Snitt (tidsskrift)}}</ref> About the same exhibit, Meghan Dailey wrote, "For much of that period, she is alone ([[solitude]] being the optimal condition for maximum [[attentional control|concentration]]), eating and sleeping very little. In the course of working so intensely, notions of the [[self]] and of time collapse, until mentally and physically, Kjøk reaches a heightened state in which she feels a part of the piece."<ref>[http://buch-info.org/t/Sol_Kj%C3%B8k,_entre_sol_et_ciel Meghan Dailey, " Always Somebody Moving" in exhibition catalogue: Entre Sol et Ciel, Berlin: Art House Tacheles, 2009]</ref> Kjøk has completed "wall drawing as extreme sport" rounds in four countries.<ref>[http://hallandsposten.se/nojekultur/1.237371-visar-kroppar-i-rorelse Showing Bodies in Motion, Hallandsposten, July 4, 2008]</ref>

Although highly physical in both its subject and execution, Kjøk's art also incorporates many [[spirituality|spiritual]] themes. Of the paradoxically corporeal and spiritual quality of her work, [[Noel Kelly (curator)]] wrote "There is no doubt that these are sexual figures, However, the sometimes contorted [[physiognomy]] and physicality of the figures speaks of the [[sublime (philosophy)|sublime]]; a rapture wherein strength, tenderness, vigour and tempestuous urgency portray the essence of the human state."<ref>[http://www.amazon.com/Book-Swells-Sol-Kj%C3%B8k/dp/B00TYMO8PI/ref=sr_1_3?ie=UTF8&qid=1424876441&sr=8-3&keywords=Sol+Kjok Noel Kelly Book of Swells Exhibition Catalogue. Laholm, Sweden: Teckningsmuseet -The Nordic Museum of Drawing, 2008]</ref> Jason Franz of [[Xavier University]] echoed this, saying "Ms. Kjøk writes of love, but her images are not necessarily sexual; at least no more than any other human image. No, if they are born of love, and therefore evoke love, it is of a higher order."<ref>{{cite web|url=http://www.jasonfranz.com/writing/dreams.html|title=About|publisher=}}</ref> Kjøk herself acknowledges this duality in her [[artist's statement]], saying "I used to think that [[monasticism|monastic]] seclusion at a safe distance from my body was the way to [[knowledge]]. Now I am convinced that human beings are clothed in flesh for a reason: The path is through the skin. Opting for either [[Dionysus]] or [[Apollo]] is choosing the easy way out; the challenge is to maintain the balance between the two."<ref>{{cite web|url=http://www.solkjok.com/pages/globe_e|title=Sol Kjøk: Globe E|publisher=}}</ref> Despite these claims of a spiritual vision, some have found her work shockingly sensual
with one writer describing her figures as "a cross between the [[androgyny|androgynous]] representatives of some future race and the randy hippies of [[Alex Comfort]]'s [[The Joy of Sex|Joy of Sex]]".<ref>{{cite web|url=http://clatl.com/atlanta/something-weird-grows-in-brooklyn/Content?oid=1259502|title=Something weird grows in Brooklyn|first=Felicia|last=Feaster|publisher=}}</ref>

==Reception==

Kjøk's work has received numerous accolades from critics. The prominent American critic [[Donald Kuspit]] praised her, saying, "Sol Kjok's figures – male and female nudes – are exquisitely drawn, often down to the least detail of their muscular flesh and expressive faces, indicating that she is not only a master draftsperson but a student of the human condition." […] "Certain passages of Swirling are sheer linear [[ecstasy (emotion)|ecstasy]] – Kjøk seems to delight in the act of drawing itself, not simply in describing the figure."<ref name="Swirling"/>
[[File:03 Sol Kjok Push and Pull.jpg|thumb|''Push and Pull'', 2013]]
New York Times art critic [[D. Dominick Lombardi]] wrote admiringly, "Sol Kjøk's art is playful and energetic.... Then there is a universal strength; a [[pleasure]], a movement in her art that is clear and profound. It is obvious if you spend any time with her creations: Kjøk loves what she is doing. A feeling that emanates through every line, every form, every expression she records with obsessive precision..."<ref>{{cite web|url=http://collections.si.edu/search/results.htm?q=record_ID:siris_sil_773448|title=Strings of beads = perlestrenger : Sol Kjok - Collections Search Center, Smithsonian Institution|publisher=}}</ref>

Swedish writer Britte Montigny said, "It is expertly done. So dazzlingly accomplished that you all but miss the fact that Sol Kjøk's work also holds [[existentialism|existential]] questions with a full range of feelings, spanning from safety-seeking [[anxiety]] and [[fear]], to protective love and [[joy]] of life."<ref>
[http://hallandsposten.se/nojekultur/1.240293-ekvilibristisk-teknik-och-akrobatik Britte Montigny,"Equilibristic Technique and Acrobatics"  translated from Swedish Hallandsposten August 12, 2008]</ref>

In [[NYArts]], Helmer Lång wrote "This makes for a kind of [[conceptual art]] where the artist directs the action, in such a way that the result reads both dramatic and mystifying. The viewer constantly feels that there is much more to this than a simple juggling with the organic forms of the human body; there are also symbolic accents and even a philosophy of life."<ref name="Agile Attempts - NY Arts Magazine">{{cite web|url=http://www.nyartsmagazine.com/?p=5785|title=Agile Attempts - NY Arts Magazine|date=May 27, 2009|publisher=}}</ref>

==Selected exhibitions==

===Solo exhibitions===
[[File:04 Sol Kjok Waiting for the Sun.jpg|thumb|''Waiting for the Sun'', 2008]]

* 2013 A Red Song in the Night. Galleri Ramfjord, Oslo<ref>{{cite web|url=http://www.osloby.no/oslopuls/kunst_og_scene/Fra-Oslo-til-New-York---og-tilbake-6903316.html|title=Fra Oslo til New York - og tilbake|publisher=}}</ref><ref>{{cite web|url=http://www.galleriramfjord.net/349632129|title=SOL KJØK (1968)|publisher=}}</ref>
* 2009 Entre Sol et Ciel. [[Kunsthaus Tacheles]], Berlin<ref>{{cite web|url=http://www.norwegen.no/arkiv/OM-/Bildende-Kunst-/solkjok/#.VRSGJDu-SmG|title=Entre sol et ciel|publisher=}}</ref>
* 2008 Book of Swells. The Nordic Museum of Drawing ([[:sv:Teckningsmuseet i Laholm|Teckningsmuseet]]), Sweden<ref name="Agile Attempts - NY Arts Magazine"/><ref>{{cite web|url=http://teckningsmuseet.se/tidigare-utstallningar/|title=Tidigare utställningar - Teckningsmuseet|publisher=}}</ref><ref>{{cite web|url=http://www.amazon.com/Book-Swells-Sol-Kj%C3%B8k/dp/B00UIUP6JM/ref=sr_1_3?ie=UTF8&qid=1426084405&sr=8-3&keywords=Sol%20Kjok|title=Book of Swells|first1=Sol|last1=Kjøk|first2=Noel|last2=Kelly|date=January 1, 2008|publisher=Teckningsmuseet i Laholm [Nordic Drawing Museum]|via=Amazon}}</ref><ref>{{cite web|url=http://www.absolutearts.com/artsnews/2008/07/07/35092.html|title=Sol Kjøk: Book of Swells - Nordic Museum of Drawing - World Wide Arts Resources|publisher=absolutearts.com}}</ref>
* 2006 Swift and Slow. [[Nordic Heritage Museum]], Seattle
* 2006 Strings of Beads. Manifest Gallery, Cincinnati, OH
* 2005 Perlestrenger. Galleri 27, Oslo  
* 2001 Swirling. [[:no:Tegnerforbundet|Tegnerforbundet Galleri]], Oslo
* 1996 Allegro Non Troppo. Brodie Gallery, Cincinnati, OH

===Two-person exhibitions===

*2016 IN THE AIR with Peter Max-Jakobsen, Denise Bibro Fine Arts, New York
* 2014 IN THE AIR w. Peter Max-Jakobsen, Galleri Oxholm, Copenhagen  
* 2012 And the World Cracked Open with Anki King, NOoSPHERE Arts, NYC<ref>{{cite web|url=http://calendar.artcat.com/exhibits/17673|title=And the World Cracked Open - NOoSPHERE - ArtCat|publisher=}}</ref><ref>{{cite web|url=http://www.undo.net/it/mostra/143446|title=And the world cracked open NoOsphere New York|publisher=}}</ref>
* 2010 Un-Interrupted w. Erica Schreiner. Bill Hodges Gallery, NYC<ref>{{cite web|url=http://calendar.artcat.com/exhibits/12047|title=Un-Interrupted: Sol Kjøk and Erica Schreiner - Bill Hodges Gallery - ArtCat|publisher=}}</ref><ref>{{cite web|url=http://www.feminist.org/calendar/cal_details.asp?idSchedule=8726|title=Feminist Event Calendar - 9/9/2010: Un-Interrupted, NY|publisher=}}</ref>
*2006 The Human Form Dominates w. Boris Zakic, Manifest Gallery, Cincinnati, Ohio<ref>[http://www.manifestgallery.org/exhibits_archive/zakic_kjok/press/zakic_kjok.release2-print.pdf The Human Form Dominates w. Boris Zakic , Manifest Gallery, Cincinnati, Ohio]</ref><ref>{{cite web|url=http://www.worldcat.org/title/zakic-kjk-a-selection-1999-2001-paintings-by-boris-zakic-string-of-beads-drawings-by-sol-kjk-exhibition-january-27-february-24-2006/oclc/861787229|title=Zakic, Kjøk: a selection, 1999-2001 : paintings by Boris Zakic : string of beads : drawings by Sol Kjøk : [exhibition] January 27-February 24, 2006|first1=Sol|last1=Kjøk|first2=Boris|last2=Zakic|first3=Jason|last3=Franz|first4=|last4=Manifest Creative Research Gallery and Drawing Center|date=January 1, 2005|publisher=Manifest Creative Research Gallery and Drawing Center|via=Open WorldCat}}</ref>
* 2005 Skeins & Veins w. [[Håvard Homstvedt]]. Samuel S.T. Chen Fine Arts Center, CT

===Group exhibitions===
[[File:05 Sol Kjok Unfurling.jpg|thumb|''Unfurling'', 2008. Permanent wall drawing at Teckningsmuseet i Laholm, Sweden.]]

*2015 New Silk Road, Shaanxi Province Art Museum, Xi'an, China<ref>{{cite web|url=http://www.sxpam.org/Wen_Show.aspx?id=707|title=陕西美术博物馆——展览资讯|publisher=}}</ref><ref>{{cite web|url=http://www.norway.cn/News_and_events/Press-Release/Event-calendar/Sol-Kjok-at-the-2nd-Silk-Road-International-Arts-Festival/#.VxVfa0wrLIW|title=Sol Kjøk at the 2nd Silk Road International Arts Festival!|publisher=}}</ref>
*2015 You Can Feel It, Haus am Lutzowplatz, Berlin<ref>{{cite web|url=http://artefuse.com/2015/11/21/you-can-feel-it-124255/|title=You Can Feel It|date=November 21, 2015|publisher=}}</ref><ref>{{cite web|url=http://www.hal-berlin.de/ausstellung/jonny-star/|title=Kuratiert von Jonny Star : Haus am Lützowplatz|publisher=}}</ref><ref>{{cite web|url=https://axeldanielreinert.wordpress.com/2015/11/11/you-can-feel-it-haus-am-luetzowplatz/|title="YOU CAN FEEL IT" @ Haus am Lützowplatz|date=November 11, 2015|publisher=}}</ref>
* 2015 Kitchen Girls & Toy Boys curated by [[Jonny Star]] , Rush Arts Gallery, NYC<ref>{{cite web|url=http://rushphilanthropic.org/exhibition/kitchen-girls-toy-boys/|title=Kitchen Girls & Toy Boys  - Rush Philanthropic Arts Foundation|publisher=}}</ref><ref>{{cite web|url=http://www.sleek-mag.com/2015/02/24/new-york-new-gender-perspectives/|title=New York, New Gender Perspectives: Interview with Jonny Star – sleek mag|publisher=}}</ref><ref>{{cite web|url=https://www.youtube.com/watch?v=G7YPmVVOViE|title=Kitchen Girls & Toy Boys Curated by Jonny Star at RUSH Arts Gallery|first=|last=Rush Philanthropic Arts Foundation|date=March 10, 2015|publisher=|via=YouTube}}</ref>
* 2014 [[Salon d'Automne]] Juried Show, [[Grand Palais]], [[Champs-Elysées]] Paris 
* 2013 I Am My World. NO Gallery, NYC
* 2013 A House Full of Friends, NOoSPHERE Arts, NYC<ref>{{cite web|url=http://www.artparasites.com/reviews/lets-be-friends-968|title=Let's Be Friends! - Artparasites – How Art you today?|date=January 21, 2013|publisher=}}</ref>
* 2012 Voice of Drawing, Hobusepea Gallery, Tallinn<ref>{{cite web|url=http://joonistustriennaal.blogspot.com/2012/08/pressiteade-tana-kolmapaeval-15082012.html|title=5th International Drawing Triennial in Tallinn: BLACK and WHITE: tallinndrawingtriennial.ee: PRESSITEADE: Täna – kolmapäeval, 15.08.2012 kell 17.00 avatakse Hobusepea galeriis Tallinna Joonistustriennaali näitus “MANU PROPRIA”.|first=Joonistus|last=Triennaal|publisher=}}</ref>
* 2011 Nothing to Declare [[Jorge B. Vargas Museum & Filipiniana Research Center]], Manila, Philippines<ref>{{cite web|url=http://yuchengcomuseum.org/art-exhibits/nothing-declare/#|title=Nothing to Declare|publisher=}}</ref>
* 2010 Winter Salon, Elga Wimmer Gallery, NYC  
* 2010 International Selection by Lee Sun-Don, X-Power Gallery, Taipei, Taiwan  .
* 2009 A Book about Death. Emily Harvey Foundation Gallery, NYC<ref>{{cite web|url=http://fluxmuseum.org/a-book-about-death/a-book-about-death-participants.html|title=fluxmuseum - The Museum dedicated to Contemporary Fluxus Art and Fluxus Artists|publisher=}}</ref>
* 2009 DARKNESS DESCENDS: Norwegian Art Now. Traveling Show Chashama/Grossman Gallery, NYC+PA  
* 2009 London International Creative Award. [[Soho Theatre]], London
* 2009 Manu Propria:Tallinn Drawing Triennial.  Tallinn Art Hall, [[Kumu (museum)]] & other venues  
* 2009 Paper Design The Artcomplex Center of Tokyo, Japan  
* 2008 The Body as Image. [[Francis Levy#The Philoctetes Center|The Philoctetes Center]], NYC<ref>{{cite web|url=http://philoctetes.org/exhibitions/the_body_as_image|title=The Body as Image The Philoctetes Center|publisher=}}</ref>
* 2007 Remembering Ruth. Noho Gallery, NYC
* 2007 Her-Humanity:Transformative Agency. Casa Frela, NYC  
* 2006 Square Root of Drawing. [[Temple Bar Gallery and Studios]], Dublin<ref>{{cite web|url=http://www.templebargallery.com/gallery/exhibition/the-square-root-of-drawing|title=The Square Root of Drawing :: Temple Bar Gallery + Studios :: Dublin Ireland.|publisher=}}</ref>
* 2006 One Year Later. Romo Gallery, Atlanta, GA  
* 2005 Working Artists in Brooklyn. Romo Gallery, Atlanta, GA
* 2005 American Drawing Biennial. [[Muscarelle Museum of Art]],VA
* 2005 Terrestrial Domains. Manifest Gallery, Cincinnati, OH
* 2004 Norwegian Artists in New York. Trygve Lie Gallery, NYC<ref>[http://www.norway.org/NR/rdonlyres/E619E3DF52164332963E744C323B0C85/9669/non2004_02.pdf Norwegian Artists in New York]</ref>
* 2004 Festival Mira! Lubolo*. Casa de America, Madrid, Spain
* 2004 Identity Channel. [[National Museum of Contemporary Art (Romania)]] 
* 2002 Virtual Memorial. Le Musée Divisioniste, Cologne, Germany
* 2002 Hopscotch: Associative Leaps in the Construction of Narrative. [[Painted Bride Art Center]], Philadelphia, PA  
* 2001 Current Trends. Galleri Steen, Oslo, Norway 
* 2001 Figurative Explorations. Goldstrom Gallery, NYC  
* 2001 New Space – New Audience. CAA [[College Art Association]] 2001 Chicago.  
* 2001 International Show. Juried by Donald B. Kuspit. Visual Arts Center of New Jersey<ref>{{cite web|url=https://www.nytimes.com/2001/03/04/nyregion/art-review-a-lean-but-inviting-juried-show.html|title=A Lean but Inviting Juried Show|date=March 4, 2001|work=The New York Times}}</ref>
* 2000 Survey of Women Artists at the Millennium. [[A.I.R. Gallery]], NYC.
* 1998 Realism III. Juried by Sidney Goodman. CCC/Gallery Alexy, Philadelphia
* 1996 Contemporary Realism. Juried by Sidney Goodman. Gallery Alexy, Philadelphia

==Works in museums and public collections==

* [[Cincinnati Art Museum]], OH
* The Nordic Museum of Drawing ([[:sv:Teckningsmuseet i Laholm|Teckningsmuseet]]), Laholm, Sweden
* Osten Museum of Drawings, Skopje, Republic of Macedonia
* Broadway Stages, New York City
* [[The Kinsey Institute]], IN
* [[Kunsthaus Tacheles]], Berlin

==References==
{{reflist}}

==External links==
{{commons category}}
* {{Official website|http://www.solkjok.com }}
* [http://www.tegnerforbundet.no/medlemsweb/kjok/ Sol Kjøk's Online CV]
* [http://no-in-nyc.org/ NOoSPHERE Arts], ''nonprofit exhibition and performance venue''
* [https://www.facebook.com/MothershipNYC The Mothership NYC], ''international arts collective''
* [http://www.last-frontier.nyc/ Last Frontier NYC], ''collaborative arts platform''
* [https://www.youtube.com/channel/UCUat-hY4sGxZhJGWt5LuYtw NOoSPHERE Arts Youtube Channel]

{{DEFAULTSORT:Kjok, Sol}}
[[Category:Norwegian contemporary artists]]
[[Category:21st-century Norwegian painters]]
[[Category:American contemporary artists]]
[[Category:Norwegian painters]]
[[Category:Norwegian art]]
[[Category:Living people]]
[[Category:Parsons The New School for Design alumni]]
[[Category:American women artists]]
[[Category:21st-century women artists]]
[[Category:1968 births]]