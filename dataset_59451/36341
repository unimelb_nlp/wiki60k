{{Infobox song <!-- See Wikipedia:WikiProject_Songs -->
| Type           = [[Promotional recording|Promotional single]]
| Name           = And Then We Kiss
| Artist         = [[Britney Spears]]
| Album          = [[B in the Mix: The Remixes]]
| Cover          = And Then We Kiss.png
| Border         = yes
| Format         = {{flat list|
*[[Music download|Digital download]]
*[[Gramophone record|12"]]
}}
| Released       = {{start date|2005|10|31}}
| Recorded       = 2003
| Genre          = {{flatlist|
*[[Trance music|Euro-trance]] 
*[[Dance-rock]]}}
| Length         = {{duration|m=4|s=28}}
| Label          = [[Jive Records|Jive]]
| Writer         = {{flat list|
*Britney Spears
*Michael Taylor
*Paul Barry
}}
| Producer       = {{flat list|
*Mark Taylor
*[[Junkie XL]]
}}
| prev           = "[[I'm a Slave 4 U|I'm a Slave 4 U (Dave Audé Slave Driver Mix)]]"
| prev_no        = 5
| next           = "[[Everytime|Everytime (Valentin Remix)]]"
| next_no        = 7 
}}

"'''And Then We Kiss'''" is a song recorded by American singer [[Britney Spears]]. It was written by Spears, [[Mark Taylor (music producer)|Mark Taylor]] and Paul Barry, while production was handled by Taylor. The song did not make the final track listing of ''[[In the Zone]]'' and was later remixed by [[Junkie XL]] for inclusion on Spears' first remix album, ''[[B in the Mix: The Remixes]]'' (2005). It was also included in the EP released to promote the remix album, called ''[[B in the Mix: The Remixes#Key Cuts from Remixed|Key Cuts from Remixed]]''. The remix was released as the promotional single from the album in Australia and New Zealand. The original version by Taylor leaked online in September 2011.

The Junkie XL Remix of "And Then We Kiss" is an [[trance music|euro-trance]] song with influences of [[techno]] and usage of [[dance-rock]] guitars, synthesizers and symphonic strings. The lyrics speak about a kiss and the different sensations that a woman experiences, including trembling, crying and moaning. Junkie XL explained that he wanted to make the song a 2006 version of [[Depeche Mode]]'s "[[Enjoy the Silence]]". The Junkie XL Remix of "And Then We Kiss" was well received by music critics, with some noticing its potential to be a radio or club hit. The song failed to appear on any major charts. However, it peaked at number fifteen on the US [[Dance/Mix Show Airplay|''Billboard'' Hot Dance Airplay]]. The song appears in [[Dance Dance Revolution SuperNova 2 (2007 video game)|Dance Dance Revolution Supernova 2]].

==Background==
"And Then We Kiss" was written by Spears, [[Mark Taylor (music producer)|Mark Taylor]] and Paul Barry, while produced by Taylor.<ref>{{cite web|url=http://repertoire.bmi.com/title.asp?blnWriter=True&blnPublisher=True&blnArtist=True&page=1&keyid=7726617&ShowNbr=0&ShowSeqNbr=0&querytype=WorkID|title=BMI Repertoire&nbsp;— And Then We Kiss (Legal Title)|publisher=[[Broadcast Music, Inc.]]|accessdate=September 5, 2011}}</ref><ref name=muumuse>{{cite web|url=http://www.muumuse.com/2011/09/britney-spears-and-then-we-kiss-original-version-leaks.html/|title=Daily B: The Original Version of 'And Then We Kiss' Surfaces|last=Stern|first=Bradley|publisher=Muumuse.com. Bradley Stern|accessdate=September 5, 2011}}</ref> The song was recorded in the same sessions as "Breathe on Me", and was originally intended for ''[[In the Zone]]'' (2003). This version produced by Taylor features an [[electronica]] vibe reminiscent of [[Madonna (entertainer)|Madonna]]'s ''[[Ray of Light]]'' (1998), and contains a [[flamenco guitar]] with prominent lead vocals by Spears.<ref name=muumuse/> "And Then We Kiss" was set to be included as a UK and Japan bonus track on ''[[Britney & Kevin: Chaotic (EP)|Britney & Kevin: Chaotic]]'' (2005), but was replaced with "Over to You Now" for unknown reasons.<ref name=rca>{{cite news|url=http://www.rcalabelgroup.co.uk/artist_spotlight/britney_spears/1145/15/|title= Britney Spears. Chaotic&nbsp;— DVD out October 31st |date=October 18, 2005| archiveurl=https://web.archive.org/web/20051025120601/http://uk.britney.com/|archivedate=October 25, 2005 | accessdate=January 4, 2010|last=Staff|first=RCA|publisher=[[RCA/Jive Label Group]]. [[Sony Music Entertainment]]}}</ref>

The song was remixed by [[Junkie XL]] and released on her remix album ''[[B in the Mix: The Remixes]]'' (2005). In the album credits, both Taylor and Junkie XL were listed as producers of the song. All instruments, including [[guitar]], [[bass guitar]], [[synths]] and [[drums]], were played by Junkie XL. [[Audio mastering]] was done by Chaz Harper at Battery Mastering.<ref name=liner/> In September 2005, it was announced by ''[[Billboard (magazine)|Billboard]]'' that the remix would serve as the music for the ad campaign behind Spears' fragrance, ''[[Fantasy (fragrance)|Fantasy]]''.<ref name=fantasy>{{cite news|url=http://www.billboard.com/articles/news/61297/britney-junkie-xl-team-up-on-remix-disc |title= Britney, Junkie XL Team Up On Remix Disc|date=September 28, 2005|accessdate=January 4, 2010|last=Reporter|first=Billboard|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]}}</ref> The Junkie XL Remix was released as promotional single from ''B in the Mix: The Remixes'' on October 31, 2005 in Australia and New Zealand, as "And Then We Kiss".<ref name=muumuse/><ref name=itunesaus/><ref name=itunesnz/> The version of the song produced by Taylor remained unreleased for years, until a new mix of the song labeled as the original version leaked online on September 2, 2011. After suggestions that it may be a fake, Taylor confirmed its authenticity to Bradley Stern of Muumuse.com on September 5, 2011.<ref name=muumuse/>

==Composition==
{{Listen
|pos=right
|filename=Britney Spears_-_And Then We Kiss.ogg
|title="And Then We Kiss"
|description=A 26-second sample of the song's chorus, where Spears sings over a euro-trance melody.
|format=[[Ogg]]
}}
"And Then We Kiss" is four minutes and twenty-eight seconds long. It is an [[Eurodance|euro-trance]] song with influences of [[techno]] and usage of [[synthesizers]].<ref name=surprise>{{cite web|url=http://www.mtv.com/news/articles/1512630/20051031/spears_britney.jhtml|title=Surprise: Britney's Releasing An Album In Just Two Weeks|last=Vineyard|first=Jennifer|publisher=[[MTV]]. [[MTV Networks]]|date=November 8, 2005|accessdate=April 17, 2010}}</ref> The song blends [[dance-rock]] guitars and symphonic strings and closes with an orchestral overtone.<ref name=surprise/><ref name=rolling>{{cite journal|url=http://www.rollingstone.com/artists/britneyspears/albums/album/8801613/review/8878262/b_in_the_mix_the_remixes_deluxe_version|title=Rolling Stone : B In The Mix, The Remixes (Deluxe Version) : Review|date=November 28, 2005|work=[[Rolling Stone]]|publisher=[[Jann Wenner]]| accessdate=January 1, 2010|last=Walters|first=Barry|issn=0035-791X|archiveurl=http://www.rollingstone.com/music/albumreviews/b-in-the-mix-the-remixes-deluxe-version-20091216|archivedate=December 16, 2009}}</ref> Its lyrics talk about a kiss and the different sensations that the protagonist experiences, including trembling, crying and moaning. At the beginning she sings the lines "Lying alone / touching my skin" which suggest that the whole song may actually be a fantasy.<ref name=surprise/> Spears's vocals are much less prominent than on the original version.<ref name=muumuse/> In an interview with [[About.com]], Junkie XL said he wanted to turn the song "into a 2006 version of [[Enjoy the Silence]] with really electronic chunky beats and nice melodic guitar lines. Besides the fact that [Britney]'s singing on it, it could be a track off my album because it's the same vibe. I'm really happy with the end result and so are they."<ref>{{cite web|url=http://dancemusic.about.com/od/remixersproducers/a/JunkieXLInt_2.htm|title=Junkie XL Interview|last=Slomowicz|first=Ron|publisher=[[About.com]]. [[The New York Times Company]]|accessdate=March 9, 2011}}</ref>

==Reception==
[[File:Junkie-xl.jpg|thumb|left|175px|[[Junkie XL]] remixed "And Then We Kiss" for inclusion on ''B in the Mix: The Remixes''.]]

The Junkie XL Remix of "And Then We Kiss" received positive reviews from music critics. Jennifer Vineyard of [[MTV]] noted "And Then We Kiss" had "the potential to be a radio or club hit&nbsp;— if Jive were actively promoting [''B in the Mix'']".<ref name=surprise/> Barry Walters of ''[[Rolling Stone]]'' said the remix "brings a [[wiktionary:simpatico|simpatico]] blend of symphonic strings and dance-rock guitars" in its melody,<ref name=rolling/> while Spence D. of [[IGN]] noted Junkie XL makes "the remix an understated swatch of atmospheric neo-goth poing and staccato rhythms."<ref name=ign2>{{cite web|url=http://music.ign.com/articles/670/670585p1.html|title=Britney Spears B in the Mix: The Remixes|date=November 23, 2005|accessdate=September 18, 2011| last=D.|first=Spence|publisher=[[IGN]]. [[News Corporation]]}}</ref> A reviewer of [[Yahoo! Shopping]] considered the song "dreamy".<ref>{{cite news|url=https://shopping.yahoo.com/560474301-b-in-the-mix-the-remixes/|title=B In The Mix: The Remixes&nbsp;— Britney Spears (CD)|publisher=[[Yahoo! Shopping]]. [[Yahoo!]]|year=2005|accessdate=March 31, 2011}}</ref> Kurt Kirton of [[About.com]] said that the remixes of "And Then We Kiss", "[[Toxic (song)|Toxic]]", "Touch of My Hand", "[[Someday (I Will Understand)]]" and "[[...Baby One More Time (song)|...Baby One More Time]]" "hold their own";<ref name=about>{{cite news|url=http://dancemusic.about.com/od/reviews/fr/BritneyIntheMix.htm|title=Britney Spears&nbsp;— B In the Mix (The Remixes)|last=Kirton|first=Kurt|publisher=About.com. The New York Times Company|year=2005|accessdate=April 17, 2010}}</ref> Gregg Shapiro of the ''[[Bay Area Reporter]]'', however, said "the many flaws in Spears' reedy, cold and mechanical voice are brought to the forefront" in the remixes.<ref name=bayarea>{{cite news|url=http://www.ebar.com/arts/art_article.php?sec=music&article=100|title=Movers and shakers|last=Shapiro|first=Gregg|work=[[Bay Area Reporter]]|publisher=Benro Enterprises, Inc.|date=February 2, 2006|accessdate=April 17, 2010}}</ref> MTV writer Bradley Stern praised Junkie XL, writing, "the sublime remix found producer Junkie XL taking the (still) unreleased studio version of "And Then We Kiss" [...] and smoothing it over into one of Britney's most lush, mature musical moments of all time."<ref>{{cite web|url=http://buzzworthy.mtv.com/2011/10/10/britney-spears-remixes/|title=The 5 Best Britney Spears Remixes Ever!|date=October 10, 2011|last=Stern|first=Bradley|accessdate=October 10, 2011|publisher=[[MTV]]. [[MTV Networks]]}}</ref>

"And Then We Kiss" was not officially released as promotional single in the United States, therefore it was not eligible at the time to appear on [[Billboard Hot 100|''Billboard''{{'}}s Hot 100]]. Even so, promotional vinyls were sent to radio stations,<ref name=vinyl/> who started to play the song unofficially and it managed to garner enough airplay to appear on the [[Dance/Mix Show Airplay|Hot Dance Airplay]] chart of ''Billboard'' in early 2006.<ref name=hda/> It debuted at number 25 on the chart issue dated February 25, 2006, reaching a new position of 23 on the following issue.<ref name=hda>{{cite journal |year=2006 |title=Catching the Long Tail |journal=Billboard |volume=118 |issue=9 |pages=68 |publisher=Prometheus Global Media |issn=0006-2510 |doi= |pmid= |pmc= |url=https://books.google.com/books?id=4xQEAAAAMBAJ&pg=PA59 |accessdate= March 11, 2011}}</ref> After five weeks on the chart, "And Then We Kiss" reached a peak of 15, on the chart issue dated March 25, 2006.<ref name=billboardmagazine>{{cite journal |year=2006 |title=Culture Clash |journal=Billboard |volume=118 |issue=12 |pages=88 |publisher=Prometheus Global Media |issn=0006-2510 |doi= |pmid= |pmc= |url=https://books.google.com/books?id=GBYEAAAAMBAJ&pg=PA79 |accessdate= March 11, 2011}}</ref> The song spent a total of eleven weeks on the chart,<ref>{{cite journal |year=2006 |title=Inside Pitch |journal=Billboard |volume=118 |issue=18 |pages=55 |publisher=Prometheus Global Media |issn=0006-2510 |doi= |pmid= |pmc= |url=https://books.google.com/books?id=6hUEAAAAMBAJ&pg=PA51 |accessdate= March 11, 2011}}</ref> making its last appearance on the week of May 6, 2006.<ref name=bubbling>{{cite web|url=http://www.billboard.biz/bbbiz/charts/chart-search-results/singles/3066099|title=Hot Dance Airplay&nbsp;— Subscription Required|date=May 6, 2006|accessdate=June 12, 2011|work=Billboard|publisher=Prometheus Global Media}}</ref> Despite being released in Australia and New Zealand, "And Then We Kiss" failed to appear on major charts of both countries.<ref>{{cite web|url=http://australian-charts.com/showitem.asp?interpret=Britney+Spears&titel=And+Then+We+Kiss&cat=s|title=Australian-charts.com&nbsp;– Britney Spears&nbsp;– And Then We Kiss|publisher=[[ARIA charts|ARIA Top 50 Singles]]. Hung Medien|accessdate=March 19, 2012}}</ref>

==Track listing==
*'''Digital download'''<ref name=itunesaus>{{cite web|url=https://itunes.apple.com/au/album/and-then-we-kiss-single/id264787055|title=And Then We Kiss&nbsp;— Single by Britney Spears|publisher=Apple Store|date=October 31, 2005|accessdate=March 9, 2011}}</ref><ref name=itunesnz>{{cite web|url=https://itunes.apple.com/nz/album/and-then-we-kiss-single/id264787055|title=And Then We Kiss&nbsp;— Single by Britney Spears|publisher=Apple Store|date=October 31, 2005|accessdate=March 9, 2011}}</ref>
#"And Then We Kiss"&nbsp;&nbsp;– 4:28

*'''12" Vinyl'''<ref name=vinyl>{{cite AV media notes|title=And Then We Kiss|others=Britney Spears|date=2005|type=12" Vinyl liner notes|publisher=Jive Records|id=82876 76466 1}}</ref>
#"And Then We Kiss" (Junkie XL Remix)&nbsp;&nbsp;– 4:28
#"And Then We Kiss" (Junkie XL Remix Instrumental)&nbsp;&nbsp;– 4:28
#"And Then We Kiss" (Junkie XL Undressed Remix)&nbsp;&nbsp;– 4:41
#"And Then We Kiss" (Junkie XL Undressed Remix Instrumental)&nbsp;&nbsp;– 4:41

==Credits and personnel==
* Britney Spears&nbsp;— [[lead vocals]], [[Songwriter|songwriting]]
* Michael Taylor&nbsp;— songwriting
* Paul Barry&nbsp;— songwriting
* Mark Taylor&nbsp;— [[Record producer|producer]]
* Junkie XL&nbsp;— producer, remixer, all instruments
* Chaz Harper&nbsp;— [[audio mastering]]

Source:<ref name=liner>''B in the Mix: The Remixes'' liner notes. Jive Records (2005)</ref>

==Charts==
{| class="wikitable plainrowheaders"
|-
! Chart (2006)	
! Peak<br />position
|-
! scope="row" {{singlechart|Israelairplay|1|week=01|year=2006|artist=Britney Spears|song=And Then We Kiss |accessdate=September 24, 2016}}
|-
! scope="row"| U.S. [[Dance/Mix Show Airplay|''Billboard'' Hot Dance Airplay]]
| align="center"| 15 <ref>http://www.billboard.com/artist/297529/britney-spears/chart?page=1&f=348</ref>
|-
|}

==References==
{{reflist|30em}}

{{Britney Spears singles}}
{{good article}}

{{DEFAULTSORT:And Then We Kiss}}
[[Category:2005 singles]]
[[Category:Britney Spears songs]]
[[Category:Jive Records singles]]
[[Category:Song recordings produced by Mark Taylor (record producer)]]
[[Category:Songs written by Britney Spears]]
[[Category:Songs written by Mark Taylor (record producer)]]
[[Category:2005 songs]]