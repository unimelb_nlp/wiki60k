{{Use mdy dates|date=April 2016}}
{{Infobox military person
|name= Clay Beauford
|birth_date= {{Birth date|1846|9|27}}
|death_date= {{Death date and age|1905|2|1|1846|9|27}}
|birth_place= [[Washington County, Maryland]], [[United States]]
|death_place= [[Los Angeles, California]], [[United States]]
|placeofburial=Angelus-Rosedale Memorial Park
|placeofburial_label= Place of burial
|image= Clay Beauford.jpg
|caption= Clay Beauford in [[Tombstone, Arizona]], c. 1875
|nickname=
|birth_name= Welford Chapman Bridwell
|allegiance= [[United States|United States of America]]<br />[[Confederate States of America]]
|branch= [[United States Army]]
|serviceyears=1861&ndash;65 (CSA)<br />1869&ndash;1873 (USA)
|rank=[[First Sergeant (United States)|First Sergeant]]
|commands=
|unit=[[5th U.S. Cavalry]]
|battles=[[Indian Wars]]<br/>*[[Comanche Campaign]]<br/>*[[Apache Wars]]<br/>[[American Civil War]]<br/>*[[Battle of Gettysburg]]
|awards=[[Medal of Honor]]
|laterwork=Chief of the [[San Carlos Apache Police Department|San Carlos Apache Police]] (4 years)
}}

'''Clay Beauford''' (born '''Welford Chapman Bridwell'''; September 27, 1846 – February 1, 1905) was an [[United States|American]] army officer, scout and frontiersman. An ex-[[Confederate States of America|Confederate]] soldier in his youth, he later enlisted in the [[United States Army|U.S. Army]] and served with the [[5th U.S. Cavalry]] during the [[Indian Wars]] against the [[Plains Indians]] from 1869 to 1873. He acted as a guide for Lieutenant Colonel [[George Crook]] in his [[Apache Wars|"winter campaign" against the Apaches]] and received the [[Medal of Honor]] for his conduct.

From 1874 to 1877, Beauford served under [[indian agent]] [[John Clum]] as chief of scouts and captain of the [[San Carlos Apache Police Department|San Carlos Apache Police]].<ref name="Collins"/> He and Clum are credited for the capture of [[Geronimo]] at [[Ojo Caliente]] in 1877 and he is largely responsible for turning the San Carlos police into one of the most respected law enforcement agencies in the [[Southwestern United States]] during the [[Wild West|frontier era]].

He became a successful rancher and prospector in the years following his retirement. A popular pioneering figure during his lifetime, Beauford was briefly elected to [[Arizona Territorial Legislature|Arizona's territorial legislature]] in 1885 to represent [[Graham County, Arizona|Graham County]]. Mount Buford in [[Maricopa County]] is named in his honor.

==Early life==
Beauford was born Welford Chapman Bridwell<ref name="Collins">Collins, Charles. ''An Apache Nightmare: The Battle at Cibecue Creek''. Norman: University of Oklahoma Press, 1999. (pg. 237) ISBN 0-8061-3114-4</ref><ref name="Wagoner">Wagoner, Jay J. ''Arizona Territory, 1863-1912: A Political History''. Tucson: University of Arizona Press, 1970. (pg. 219)</ref><ref name="AIQ">''American Indian Quarterly''. Vol. 3. Berkeley: University of California, 1978. (pg. 116)</ref><ref>Owens, Ron. ''Medal of Honor: Historical Facts & Figures''. Paducah, Kentucky: Turner Publishing Company, 2004. (pg. 192) ISBN 1-56311-995-1</ref> in [[Washington County, Maryland]] on September 27, 1846,<ref name="HomeofHeroes">{{Cite web |url=http://www.homeofheroes.com/gravesites/states/pages_af/beauford_bridwell_ca.html |title=Clay Beauford |accessdate= |author=Sterner, C. Douglas |date= |year=1999 |work=Medal of Honor Recipient Gravesites In The State of California |publisher=HomeofHeroes.com }}</ref><ref name="HomeofHeroes-CT">{{Cite web |url=http://www.homeofheroes.com/moh/citations_1865_ind/beauford.html |title=MOH Citation for Clay Beauford |accessdate= |author=Sterner, C. Douglas |date= |year=1999 |work=MOH Recipients: Indian Campaigns |publisher=HomeofHeroes.com }}</ref><ref name="MilitaryTimes">{{Cite web |url=http://militarytimes.com/citations-medals-awards/recipient.php?recipientid=510|title=Military Times Hall of Valor: Clay Beauford |accessdate= |author=Army Times Publishing Company |date= |work=Awards and Citations: Medal of Honor |publisher=MilitaryTimes.com }}</ref> and later moved with his family to neighboring Virginia. At age 14, upon the start of the [[American Civil War]], he ran away from home to join the [[Confederate Army]]. He enlisted under an alias, Clay Beauford, partly because of his age and to avoid being brought back home by his father. Beauford initially spent the first year of the war as a [[drummer boy (military)|drummer boy]] with General [[Robert E. Lee]]'s [[Army of Northern Virginia]], however, he became a regular [[infantryman]] within a year.<ref name="Barnes">Barnes, Will Croft. ''Arizona Place Names''. Tucson: University of Arizona Press, 1960. (pg. 177)</ref> In 1863, he saw action at [[Battle of Gettysburg]] and was among the 4,500 men who took part in [[Pickett's Charge]]. He was wounded in at least three other engagements before the end of the war:<ref name="Thrapp">Thrapp, Dan L. ''Encyclopedia of Frontier Biography: A-F''. Vol. I. Norman: University of Nebraska Press, 1988. (pg. 168-169) ISBN 0-8032-9418-2</ref> a gunshot wound to his kneecap, a second to his left hand, and a third which penetrated near the stomach.

==Military career==
In 1869, while living in [[Nashville, Tennessee]], Beauford enlisted in the [[United States Army|U.S. Army]]. He was assigned to Company B of the [[5th U.S. Cavalry]]<ref name="HomeofHeroes"/><ref name="HomeofHeroes-CT"/><ref name="MilitaryTimes"/><ref name="Beyer">Beyer, Walter F. and Oscar Frederick Keydel, ed. ''Deeds of Valor: From Records in the Archives of the United States Government; how American Heroes Won the Medal of Honor; History of Our Recent Wars and Explorations, from Personal Reminiscences and Records of Officers and Enlisted Men who Were Rewarded by Congress for Most Conspicuous Acts of Bravery on the Battle-field, on the High Seas and in Arctic Explorations''. Vol. 2. Detroit: Perrien-Keydel Company, 1906. (pg. 539)</ref><ref name="O'Neal">O'Neal, Bill. ''Fighting Men of the Indian Wars: A Biographical Encyclopedia of the Mountain Men, Soldiers, Cowboys, and Pioneers Who Took Up Arms During America's Westward Expansion''. Stillwater, Oklahoma: Barbed Wire Press, 1991. (pg. 28) ISBN 0-935269-07-X</ref> and took part in a number of [[Indian Wars|indian campaigns]] in [[Kansas]], [[Nebraska]], and the [[Wyoming Territory]]. Within a year of his enlistment, he received [[Wiktionary:Honorable mention|honorable mention]] from his commanding officer, Lieutenant Colonel Thomas Duncan, for bravery at the [[Battle of Prairie Dog Creek]] on September 26, 1869.<ref>Price, George F. ''Across the Continent with the Fifth Cavalry''. New York: D. Van Nostrand, 1883. (pg. 673)</ref> Beauford remained with the 5th Cavalry throughout his military career and had risen to the rank of [[First Sergeant (United States)|first sergeant]] by the time he was posted to the [[Arizona Territory]] three years later.<ref name="AIQ"/><ref name="Barnes"/> He was praised by Captain Robert H. Montgomery on September 26, 1872, following a battle against the Apache in the [[Red Rock State Park|Red Rock]] area. He also served under Lieutenant Colonel George Crook during his "winter campaign" against renegade Apaches, particularly the Western Apache and Yavapai bands in the Tonto Basin, and helped guide Crook's columns during the expedition. Beauford was one of 22 men, 12 cavalrymen and 10 [[Apache Scouts]], who were cited for "gallant conduct during the campaigns and engagements with Apaches" awarded the [[Medal of Honor]];<ref name="Collins"/><ref name="HomeofHeroes"/><ref name="Beyer"/><ref name="O'Neal"/><ref name="Tate">Tate, Michael L. ''The Frontier Army in the Settlement of the West''. Norman: University of Oklahoma Press, 2001. (pg. 301) ISBN 0-8061-3386-4</ref> he and fellow troopers Sergeants [[James E. Bailey (Medal of Honor)|James Bailey]] and [[James H. Turpin]] were the only members of the 5th Cavalry to be recipients.<ref name="Yenne">Yenne, Bill. ''Indian Wars: The Campaign for the American West''. Yardley, Pennsylvania: Westholme Publishing, 2006. (pg. 148) ISBN 1-59416-016-3</ref> Beauford did not receive the award until April 12, 1875,<ref name="HomeofHeroes-CT"/><ref name="MilitaryTimes"/> two years after being discharged from service.<ref name="AIQ"/><ref name="Thrapp"/>

Beauford later claimed that his last year in the military was physically the hardest of his life being on almost constant patrol in the Arizona frontier. One of his later exploits was the capture of [[White Mountain Apache]] chieftain Toga-da-chuz and his family, including his son the future [[Apache Kid (Haskay-bay-nay-ntayl)|The Apache Kid]], which he brought back to the San Carlos reservation.<ref name="Thrapp"/><ref>Miller, Joseph, ed. ''The Arizona Story''. New York: Hastings House, 1952.</ref> He continued working as a civilian scout for the army until the spring of 1875.<ref name="AIQ"/>

==San Carlos Police Chief==
When [[Indian agent]] [[John Clum|John Philip Clum]] was appointed head of the [[San Carlos Apache Indian Reservation]] in 1874, Beauford accepted Clum's offer to become chief of the [[San Carlos Apache Police Department|San Carlos Apache Police]].<ref name="Collins"/><ref name="Wagoner"/><ref name="AIQ"/><ref name="Tate"/> Tasked with keeping the peace among the more than 4,000 Native Americans then living on the reservation, many of whom did not get along with each other, Beauford was considered ideal for the position given his years of experience as a guide and scout,<ref name="Head">Head, Tom and David Wolcott. ''Crime and Punishment in America''. New York: Facts on File, Inc., 2010. (pg. 114) ISBN 0-8160-6247-1</ref> his knowledge of the [[Apache language]], and his reputation among the Apache themselves. Through a skilled Indian fighter, he had "conceived a deep sympathy for them" and the Virginian's flamboyant personality "inspired Indians and was a natural leader".<ref>Stott, Jon C. ''Native Americans in Children's Literature''. Phoenix, Arizona: Oryx Press, 1995. ISBN 0-89774-782-8</ref><ref name="Moody">Moody, Ralph. ''Geronimo: Wolf of the Warpath''. New York: Sterling Publishing Company, Inc., 2006. (pg. 133-134, 136, 146) ISBN 1-4027-3184-1</ref>

Beauford, who had a "no-nonsense, hands-on, approach to supervision and administration",<ref>Barker, Michael L. ''Policing in Indian Country''. New York: Harrow and Heston, 1998. ISBN 0-911577-44-0</ref> worked well with Clum to establish the San Carlos police force as an independent agency from the U.S. Army. It initially consisted four [[Apache scouts]], however, this was later expanded to a permanent 25-man police force by the time he took command.<ref>French, Laurence, ed. ''Indians and Criminal Justice''. Totowa, New Jersey: Allanheld, Osmun & Co., 1982. (pg. 111) ISBN 0-86598-063-2</ref> These first recruits were hand-picked from the various tribes and bands on the reservation and armed with needle-guns and fixed ammunition.<ref name="Head"/> Beauford provided them with modest uniforms and taught them military drill formations. The small police force quickly became an excellent drill team and became very popular among the younger men on the reservation. Many were eager to join up and within a few months the reservation police had attracted hundreds of new officers. His efforts would eventually transform the San Carlos police department into one of the prominent law enforcement agencies in the American Southwest.<ref name="Moody"/>
{{Quote box
 | quote= "In this connection I desire to mention that Mr. Clay Beauford has rendered most able services as a guide and scout with the Indian police. He is brave and energetic, a thorough Indian fighter, and when once he strikes a trail he never stops until he is victor in the renegade camp."
 | source= — [[John Clum|John Philip Clum]], [[Indian agent|U.S. Indian Agent]] for the [[San Carlos Apache Indian Reservation]].<ref name="DOI">[[United States Department of the Interior|Department of the Interior]]. "Report of Superintendent and Agents". ''Annual Reports of the Department of the Interior''. Washington, D.C.: Government Printing Office, 1876. (pg. 415)</ref><ref name="Hagan">Hagan, William Thomas. ''Indian Police and Judges: Experiments in Acculturation and Control''. Yale University Press, 1966. (pg. 33, 35-36)</ref>
 | width= 30em
 | bgcolor= transparent
 | align= right
 | qalign= left
 | salign= right
}}
In December 1875, Beauford was the subject of a failed assassination attempt by Tonto Apache chief Disalin. Failing to kill John Clum and an agency clerk at the main building, Disalin made a last desperate bid to gun down the police chief. He found Beauford shortly after fleeing Clum's office. His first shot missed and, while moving closer to take a second shot, Disalin was shot and killed by an indian police officer.<ref name="Worcester">Worcester, Donald E. ''The Apaches: Eagles of the Southwest''. Norman: University of Oklahoma Press, 1992. (pg. 189-190) ISBN 0-8061-2397-4</ref><ref name="Robinson">Robinson, Charles III, ed. "From Tribal Law To White Man's Law". ''American Frontier Lawmen, 1850-1930''. University Park, Illinois: Osprey Publishing, 2005. (pg. 50-51) ISBN 1-84176-575-9</ref> Four months later, while investigating reports of intruders "prowling about the western border", he and a group of reservation police officers were involved in a gun battle which left 16 renegade Apaches dead.<ref>McClintock, James H. ''Arizona, Prehistoric - Aboriginal - Pioneer - Modern: The Nation's Youngest Commonwealth Within a Land of Ancient Culture''. Vol. 1. Chicago: The S. J. Clarke Publishing Co., 1916. (pg. 229)</ref> They brought 21 women and children back to San Carlos. Clum commended the actions of the officers and personally praised Beauford's abilities to [[United States Department of the Interior|Department of the Interior]].<ref name="DOI"/> He had a limited role in the concentration of Apaches from the Chiracahua and Fort Apache reservations at San Carlos.<ref name="Thrapp"/>

On April 21, 1877, he headed the 102-man Apache police force, accompanied by John Clum, which captured [[Geronimo]] at [[Ojo Caliente]]<ref name="Hagan"/><ref name="Worcester"/><ref name="Robinson"/><ref>Haley, James L. ''Apaches: A History and Culture Portrait''. Norman: University of Oklahoma Press, 1997. (pg. 313-315) ISBN 0-8061-2978-6</ref><ref>{{Cite news |title=Arizona History |author=[[Associated Press]] |newspaper=[[The Daily Courier (Arizona)|The Daily Courier]] |url=https://news.google.com/newspapers?id=DrAKAAAAIBAJ&sjid=Mk0DAAAAIBAJ&pg=5069,1755928&dq=clay-beauford&hl=en |date=April 16, 2000}}</ref> and oversaw the subsequent transfer of the Warm Springs (or Mimbres) band to San Carlos, both occurring without violence from either side. He officially retired on September 1, 1877, two weeks after Clum's resignation,<ref name="Moody"/> although he may have had occasional contact with the force up until at least 1880.<ref name="Thrapp"/>

==Later years==
Beauford became a cattle rancher after leaving the San Carlos reservation, establishing a homestead in [[Aravaipa Canyon]] he called "Spring Gardens".<ref name="UAB">"University of Arizona Bulletin" Tucson, Arizona: University of Arizona Press, 1933. (pg. 66)</ref> He also did some prospecting developing the "Arizona Mine" in the Aravaipa Mining District which he later sold at a good profit. In or around 1879, Beauford made the acquaintance of a young woman from [[Indianapolis, Indiana]], Cedonia Alexander, who was visiting relatives in [[Fort Thomas, Arizona|Fort Thomas]]. Their friendship eventually developed into a courtship and were married the following September. Prior to his engagement, he had his name legally changed back to his birth name, Welford Chapman Bridwell, by the [[10th Arizona Territorial Legislature|10th Territorial Legislative Assembly]].<ref name="Wagoner"/><ref name="Barnes"/> A very popular and well-known figure in Arizona during his lifetime, it was said that "probably no other Arizona pioneer was more widely known or had a larger circle of friends". Author and historian Dan L. Thrapp described Beauford in his ''Encyclopedia of Frontier Biography'' as being "tall, slender, broad-shouldered, companionable, could sing a ballad to his own banjo accompaniment, and fairly well controlled 'a quick and violent temper'."<ref name="Thrapp"/> Mount Buford, located 4 miles north of [[Kentuck Mountain]] in [[Maricopa County]], is named in his honor.<ref name="Barnes"/><ref name="UAB"/>

He had a brief political career being elected to the council (or "upper house") of [[Arizona Territorial Legislature|Arizona's territorial legislature]]<ref name="Tate"/> as a [[delegate]] for [[Safford, Arizona|Safford]], [[Graham County, Arizona|Graham County]] in the [[13th Arizona Territorial Legislature|13th Assembly]]<ref name="Wagoner"/> in 1885. It was during the session that Beauford was involved in a physical altercation in a [[Prescott, Arizona|Prescott]] [[Western saloon|saloon]]. While drinking in the saloon, he was approached by a French-born [[lobbyist]] for the Arizona Copper Company in [[Clifton, Arizona|Clifton]], Professor Arnold, who "cast aspersions" on Beauford for renouncing [[French American|his presumed French heritage]] by changing his name. Their argument escalated with Beauford finally striking Arnold to which the Frenchman challenged to a duel. Beauford agreed but the two were unable to agree on the weapons, Beauford choosing [[Colt revolver]]s while Arnold preferred [[Sabre|French sabers]], and with swords not readily available in Arizona the duel was eventually called off.<ref name="Thrapp"/>

In 1895, Beauford moved his family to [[Los Angeles, California]] where they raised their young children. His only son Walter died at an early age, however, his daughter Nina became the wife of eminent physician Dr. Arthur F. Maisch in 1903. Beauford died in Los Angeles on February 1, 1905, at age 58,<ref name="MilitaryTimes"/><ref name="Thrapp"/> and interred in Angelus-Rosedale Memorial Park.<ref name="HomeofHeroes"/>

==In popular culture==
Clay Beauford has appeared as a character in the following [[historical novel]]s:

*''The Bubbling Spring'' (1949) by [[Ross Santee]]
*''The Kings of San Carlos'' (1987) by [[James L. Haley]]
*''Doc Holliday's Gone: A Western Duo'' (2000) by Jane Candia Coleman

==Medal of Honor citation==
Rank and organization: First Sergeant, Company B, 5th U.S. Cavalry. Place and date: Winter of 1872-73. Entered service at: --. Birth: Washington County, Md. Date of issue: April 12, 1875.

'''Citation:'''

<blockquote>Gallant conduct during campaigns and engagements with Apaches.<ref name="AMOHW">{{Cite web
 |accessdate=June 29, 2009
 |url = http://www.history.army.mil/html/moh/indianwars.html
 |title = Medal of Honor recipients
 |work = Indian War Campaigns
 |publisher =[[United States Army Center of Military History]]
 |date = June 8, 2009}}</ref></blockquote>

==See also==
{{Portal|Biography|United States Army}}
*[[List of Medal of Honor recipients]]

==References==
{{Reflist}}

==Further reading==

===Articles===
*Dunlap, H.E. [http://uair.arizona.edu/system/files/usain/download/azu_h9791_a72_h6_03_03_w.pdf. "Clay Beauford-Welford C. Bridwell, Soldier Under Two Flags"]. ''Arizona Historical Review'' (October 1930)

===Books===
*Carmony, Neil B., ed. ''Apache Days and Tombstone Nights: John Clum's Autobiography, 1877-1887''. Silver City, New Mexico: High-Lonesome Books, 1997. ISBN 0-944383-41-6
*Clum, Woodworth. ''Apache Agent: The Story of John P. Clum''. Boston: Houghton Mifflin Company, 1936.
*Griffith, A. Kinney. ''Mickey Free, Manhunter''. Caldwell, Idaho: Caxton Printers, 1969.
*Lockwood, Frank C. ''The Apache Indians''. Lincoln: University of Nebraska Press, 1987. ISBN 0-8032-2878-3
*Radbourne, Allan. ''Mickey Free: Apache Captive, Interpreter, and Indian Scout''. Tucson: Arizona Historical Society, 2005. ISBN 0-910037-46-9
*Santee, Ross. ''Apache Land''. Lincoln, Nebraska: University of Nebraska Press, 1971. ISBN 0-8032-5737-6
*Seymour, Flora Warren. ''Indian Agents of the Old Frontier''. New York: Octagon Books, 1975. ISBN 0-374-97290-7
*Sweeney, Edwin Russell. ''From Cochise to Geronimo: The Chiricahua Apaches, 1874-1886''. Norman: University of Oklahoma Press, 2010. ISBN 0-8061-4150-6
*Thrapp, Dan L. ''Conquest of Apacheria''. Norman, Oklahoma: University of Oklahoma Press, 1967.
*Wilson, D. Ray. ''Terror on the Plains: A Clash of Cultures''. Dundee, Illinois: Crossroads Communications, 1999. ISBN 0-916445-47-X

==External links==
*{{Find a Grave|11094042|work=Claim to Fame: Medal of Honor recipients|accessdate=June 29, 2010}}
*[http://www.cmohs.org/recipient-detail/1554/beauford-clay.php Clay Beauford at CMOHS.org]

{{DEFAULTSORT:Beauford, Clay}}
[[Category:1846 births]]
[[Category:1905 deaths]]
[[Category:Members of the Arizona Territorial Legislature]]
[[Category:United States Army soldiers]]
[[Category:American military personnel of the Indian Wars]]
[[Category:United States Army Medal of Honor recipients]]
[[Category:People from Washington County, Maryland]]
[[Category:People from Virginia]]
[[Category:People from Safford, Arizona]]
[[Category:People from Los Angeles]]
[[Category:American Indian Wars recipients of the Medal of Honor]]
[[Category:Arizona pioneers]]