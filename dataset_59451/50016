{{Infobox television episode
 |image_size = 280px
 |image = Game of Thrones-S03-E02-Dark Wings, Dark Words.jpg <!-- Deleted image removed:  [[File:GOT Bolton Dark Wings Dark Words.jpg|280px]] -->
 |caption = Jaime and Brienne pointing their swords at the Bolton men.
 |title = Dark Wings, Dark Words
 |series = [[Game of Thrones]]
 |season = 3
 |episode = 2
 |director = [[Daniel Minahan]]
 |writer = {{Unbulleted list| [[Vanessa Taylor]]}}
 |producer =
 |music = [[Ramin Djawadi]]
 |photographer = Jonathan Freeman
 |editor = Frances Parker
 |production = 
 |airdate = {{Start date|2013|4|7}}
 |length = 57 minutes
 |guests = * [[Diana Rigg]] as Olenna Tyrell
* [[Ciarán Hinds]] as Mance Rayder 
* [[Mackenzie Crook]] as Orell
* [[Paul Kaye]] as Thoros of Myr
* [[Gwendoline Christie]] as Brienne of Tarth
* [[Noah Taylor]] as Locke
* [[Natalia Tena]] as Osha
* [[Michael McElhatton]] as Roose Bolton
* [[Iwan Rheon]] as Boy
* [[Kristofer Hivju]] as Tormund Giantsbane
* [[Finn Jones]] as Loras Tyrell
* [[Thomas Sangster|Thomas Brodie Sangster]] as Jojen Reed
* [[Ellie Kendrick]] as Meera Reed
* [[Ben Hawkey]] as Hot Pie
* John Stahl as Rickard Karstark
* [[Philip McGinley]] as Anguy
* [[Mark Stanley]] as Grenn
* [[Ben Crompton]] as Edd Tollett
* Luke Barnes as Rast
* [[Kristian Nairn]] as Hodor
* [[Art Parkinson]] as Rickon Stark
* Michael Shelford as Torturer
* Joe Purcell as Traveller
* Will Rastall as Tyrell Server
 |season_list =
 |prev = [[Valar Dohaeris]]
 |next = [[Walk of Punishment]]
 |episode_list = [[Game of Thrones (season 3)|''Game of Thrones'' (season 3)]]<br>[[List of Game of Thrones episodes|List of ''Game of Thrones'' episodes]]
}}
"'''Dark Wings, Dark Words'''" is the second episode of the [[Game of Thrones (season 3)|third season]] of [[HBO]]'s [[fantasy]] television series ''[[Game of Thrones]]'', and the 22nd episode of the series. Written by [[Vanessa Taylor]], and directed by [[Daniel Minahan]], it aired on April 7, 2013.<ref>{{Cite web|url=http://winter-is-coming.net/features/episode-guide/|title=Episode Guide|work=WinterIsComing.net|accessdate=March 25, 2013}}</ref>

The title is an in-universe old saying about messenger-ravens, referring to the fact that such urgently delivered messages are often bad news. In the episode, Robb Stark receives news of the death of Hoster Tully as well as the sack of Winterfell and the disappearances of Bran and Rickon.

==Plot==

===In King's Landing===
Sansa Stark ([[Sophie Turner (actress)|Sophie Turner]]) is invited to lunch by Lady Margaery Tyrell ([[Natalie Dormer]]) and her grandmother Lady Olenna ([[Diana Rigg]]). While there, Sansa is asked to tell them about King Joffrey Baratheon ([[Jack Gleeson]]), and whether he will be a good husband to Margaery. Sansa is reluctant to speak the truth about him, but she is eventually persuaded to tell them of his cruelty.

While being fitted for his wedding clothes, King Joffrey discusses his bride-to-be with his mother, Queen Cersei Lannister ([[Lena Headey]]). Afterward, he invites Margaery to his chamber to discuss his upcoming hunt. He grows impatient with her answers to his questions regarding her last husband, Renly Baratheon, and whether they consummated their marriage. Margaery informs him that they did not, and under further questioning she explains that Renly did not appreciate the "company of women". The conversation turns to Joffrey's new crossbow; Margaery displays her political aptitude by feigning interest, and Joffrey tells Margaery that he would enjoy watching her kill something with it.

Tyrion Lannister ([[Peter Dinklage]]) returns to his chamber to find Shae ([[Sibel Kekilli]]) waiting for him. He warns her of his father's promise to him regarding his whoring, but she is undaunted. She warns him that Lord Baelish has taken an interest in Sansa Stark, to which he replies that, now that she is no longer Joffrey's future queen, many men will take interest in her.

===Beyond the Wall===
Mance Rayder ([[Ciarán Hinds]]) continues to be distrustful of Jon Snow ([[Kit Harington]]), speaking to him while marching toward the wall. Along with Tormund Giantsbane ([[Kristofer Hivju]]) and Ygritte ([[Rose Leslie]]), they stop briefly to speak with Orell ([[Mackenzie Crook]]). Orell is revealed to be a warg, one who is capable of seeing through the eyes of animals. He awakens from his trance to tell Mance that he has seen the Fist of the First Men and the aftermath of the battle which took place there.

While marching back to the Wall, Samwell Tarly ([[John Bradley-West|John Bradley]]) falls from exhaustion. He is aided by Grenn ([[Mark Stanley]]) and Edd ([[Ben Crompton]]), before Lord Commander Jeor Mormont ([[James Cosmo]]) orders Rast (Luke Barnes) to ensure Sam reaches the Wall alive.

===In the North===
While heading toward the Wall, Bran Stark ([[Isaac Hempstead-Wright]]) has a dream where he is trying to shoot the three-eyed Raven, but is confronted by a boy who explains that it is impossible, as Bran is the Raven. When he awakens, Bran, Hodor ([[Kristian Nairn]]), Osha ([[Natalia Tena]]) and Rickon ([[Art Parkinson]]) continue their northern march. Later, while Hodor and Rickon are away, Osha suspects someone is following them and leaves to investigate. Bran is then confronted by the boy from his dream, who reveals that he is Jojen Reed ([[Thomas Sangster|Thomas Brodie Sangster]]), and a seer like Bran. He is accompanied by his sister, Meera ([[Ellie Kendrick]]). Jojen says that he too had the dream, and that they have been searching for Bran, believing he will play a critical role in the future.

===In an unknown location===
Theon Greyjoy ([[Alfie Allen]]) has been taken captive by a group of men and is being tortured for information. Despite Theon answering all questions truthfully, they continue the torture. After they leave, a boy ([[Iwan Rheon]]), who claims to have been sent by Theon's sister Yara Greyjoy, promises to aid him once the soldiers are sleeping.

===In the Riverlands===
King-in-the-North Robb Stark ([[Richard Madden]]) receives two letters, one informing him of the death of his grandfather, Lord Hoster Tully, and the other bearing the news that his home, Winterfell, has been put to the torch by the Iron Islanders. Furthermore, Bran and Rickon were not found among the ruins of Winterfell. He informs his mother, Lady Catelyn ([[Michelle Fairley]]) of this news, and they depart for Riverrun, her childhood home, for her father's funeral. While on the road, Lord Rickard Karstark (John Stahl) voices his displeasure with the funeral distraction. Later, Catelyn discusses her children with Queen Talisa ([[Oona Chaplin]]), recalling how she had prayed for Jon Snow to die when he was first brought to Winterfell. After Jon fell gravely ill, Catelyn prayed that she would love Jon if he recovered, but failed to make good on her promise after Jon's recovery. She admits that she feels responsible for what is happening to all of them.

Traveling north, Arya Stark ([[Maisie Williams]]), Gendry ([[Joe Dempsie]]), and Hot Pie ([[Ben Hawkey]]) are discovered by a small party led by Thoros of Myr ([[Paul Kaye]]), who suspects them of having escaped Harrenhal. He tells them that he and his men fight for the Brotherhood without Banners, and takes them to an inn to eat. Once finished, Arya, Gendry, and Hot Pie are leaving when another group of Brotherhood soldiers enter with a captive Sandor "The Hound" Clegane ([[Rory McCann]]), who recognizes Arya and reveals her true identity to Thoros and his men.

Brienne of Tarth ([[Gwendoline Christie]]) continues to transport Ser Jaime Lannister ([[Nikolaj Coster-Waldau]]) to King's Landing, hoping to trade him for Sansa and Arya. While travelling, they encounter a farmer who warns them of the danger in travelling the Kingsroad. Jaime warns Brienne that the farmer might give their presence away and must be killed, but Brienne refuses. Later when they are travelling across a bridge, Jaime seizes an opportunity to take one of Brienne's swords, but in the ensuing fight she gains the upper hand. Before their fight can be concluded, they are taken captive by Locke ([[Noah Taylor]]), a bannerman of Lord Roose Bolton ([[Michael McElhatton]]), aided by the farmer, who had recognized Jaime.

==Production==

===Writing===
"Dark Wings, Dark Words" was written by co-writer [[Vanessa Taylor]], who previously had written the episodes "[[Garden of Bones]]" and "[[The Old Gods and the New]]" for Season Two. This episode adapts the following chapters from [[George R. R. Martin]]'s ''A Storm of Swords'': Bran I, Sansa I, Jon II, Arya I,II and V and Jaime II and III.<ref name=Westeros.org>{{cite web|url=http://www.westeros.org/GoT/Episodes/Entry/Dark_Wings_Dark_Words/Book_Spoilers/#Book_to_Screen|title=EP302: Dark Wings Dark Words|last=Garcia|first=Elio|last2=Antonsson|first2=Linda|work=Westeros.org|date=April 17, 2013|accessdate=November 10, 2014}}</ref>

===Casting===
With this episode, Joe Dempsie (Gendry) is promoted to series regular, after guest starring in the first and second season. This episode also marks the first appearances of [[Diana Rigg]] (as Lady Olenna Tyrell), [[Mackenzie Crook]] (as Orell), [[Paul Kaye]] (as Thoros of Myr), [[Thomas Brodie-Sangster]] (as Jojen Reed), [[Ellie Kendrick]] (as Meera Reed), [[Philip McGinley]] (as Anguy), [[Noah Taylor]] (as Locke), and [[Iwan Rheon]] (as the cleaning boy attending Theon).

==Reception==

===Ratings===
"Dark Wings, Dark Words"'s first airing was seen by 4.27 million viewers. Taking into account the viewers of the later repeat, the figures rose to 5.54 million.<ref>{{cite web|url=http://tvbythenumbers.zap2it.com/2013/04/09/sunday-cable-ratings-game-of-thrones-wins-night-real-housewives-of-atlanta-kourtney-kim-take-miami-vikings-mad-men-more/177076/|title='Sunday Cable Ratings: 'Game of Thrones' Wins Night + 'Real Housewives of Atlanta', 'Kourtney & Kim Take Miami', 'Vikings', 'Mad Men' & More|last=Kondolojy|first=Amanda|publisher=TV By the Numbers|date=April 9, 2013|accessdate=April 9, 2013}}</ref> In the United Kingdom, the episode was seen by 0.988 million viewers on [[Sky Atlantic]], being the channel's second highest-rated broadcast that week.<ref>{{cite web|url=http://www.barb.co.uk/whats-new/weekly-top-10?|title=Top 10 Ratings (8 - 14 April 2013)|work=[[Broadcasters' Audience Research Board|BARB]]|accessdate=January 19, 2017}}</ref>

===Critical reception===
"Dark Wings, Dark Words" received very positive reviews from television critics, with [[review aggregator]] [[Rotten Tomatoes]] surveying 21 reviews of the episode and judging 90% of them to be positive. The website's critical consensus reads, "Burdened with character and plot reintroductions, 'Dark Wings, Dark Words' starts slow before revving up and delving into the real intrigues of the season."<ref>{{cite web|url=http://www.rottentomatoes.com/tv/game-of-thrones/s03/e02/|title=Dark Wings, Dark Words|work=[[Rotten Tomatoes]]|accessdate=May 3, 2016}}</ref> [[IGN]]'s Matt Fowler gave the episode an 8.5/10, writing "Not many big moments in this week's Game of Thrones, but a lot of new characters came into play."<ref>{{cite web|url=http://ca.ign.com/articles/2013/04/08/game-of-thrones-dark-wings-dark-words-review|title=Game of Thrones: "Dark Wings, Dark Words" Review|last=Fowler|first=Matt|publisher=[[IGN]]|date=April 7, 2013|accessdate=April 8, 2013}}</ref> David Sims, reviewing for [[The A.V. Club]], rated the episode with a B+ for newbies.<ref>{{cite web|url=http://www.avclub.com/articles/dark-wings-dark-words-for-newbies,95474/|title="Dark Wings, Dark Words" (for newbies)|last=Simms|first=David|publisher=[[The A.V. Club]]|date=April 7, 2013|accessdate=April 8, 2013}}</ref> Todd VanDerWerff, rating for experts, also gave the episode a B+.<ref>{{cite web|url=http://www.avclub.com/articles/dark-wings-dark-words-for-experts,95473/|title="Dark Wings, Dark Words" (for experts)|last=VanDerWerff|first=Todd|publisher=[[The A.V. Club]]|date=April 7, 2013|accessdate=April 8, 2013}}</ref>

== References ==
{{Reflist|2}}

== External links ==
{{wikiquotepar|Game_of_Thrones_(TV_series)#Dark_Wings,_Dark_Words_.5B3.02.5D|Dark Wings, Dark Words}}
* [http://www.hbo.com/game-of-thrones/episodes/3/22-dark-wings-dark-words/index.html "Dark Wings, Dark Words"] at [[HBO.com]]
* {{IMDb episode|2178772}}
* {{tv.com episode|game-of-thrones/dark-wings-dark-words-2676029}}

{{Game of Thrones Episodes}}

[[Category:2013 American television episodes]]
[[Category:Game of Thrones episodes]]