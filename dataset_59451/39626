[[File:U.S. Securities and Exchange Commission headquarters.JPG|thumb|US Security and Exchange Commission Office, in [[Washington, D.C.]], near [[Union Station (Washington)|Union Station]].]]
[[File:The Office of the Whistleblower(SEC) Symbol.jpg|thumb|Symbol of the SEC Office of the Whistleblower]]

The [[U.S. Securities and Exchange Commission]] (SEC) [[whistleblower]] program went into effect on July 21, 2010, when the [[President of the United States|President]] signed into law the [[Dodd-Frank Wall Street Reform and Consumer Protection Act]]. The same law also established a whistleblower incentive program at the [[Commodity Futures Trading Commission]], which is run by former senior SEC enforcement attorney<ref>https://www.linkedin.com/pub/christopher-ehrman/5/b3b/ba</ref> Christopher C. Ehrman.<ref>{{cite web |url= http://blogs.wsj.com/riskandcompliance/2013/10/18/qa-christopher-ehrman-director-cftcs-whistleblower-office/ |title=Q&A: Christopher Ehrman, Director, CFTC’s Whistleblower Office |author= Rachel Louise Ensign |work=WSJ}}</ref><ref>{{cite web |url=http://www.cftc.gov/PressRoom/PressReleases/pr6647-13 |title= CFTC Names Christopher Ehrman as Director of Whistleblower Office |work= cftc.gov}}</ref> The program rewards people who submit tips related to violations of the [[Commodity Exchange Act]].<ref>{{cite web |url= http://www.sec.gov/about/offices/owb/owb-faq.shtml |title= Frequently Asked Questions :: SEC Office of the Whistleblower |publisher= Sec.gov |date=2010-07-21 |accessdate= 2013-06-21}} {{PD-notice}}</ref> The SEC can make awards ranging from 10 to 30 percent of the monetary [[Sanctions (law)|sanctions]] collected, which are paid from its Investor Protection Fund.

==Overview==

While the SEC had in place a [[Bounty (reward)|bounty]] program for more than 20 years to reward [[whistleblowers]] for insider trading tips and complaints, an [[Office of the Inspector General]]'s 2013 report found the Commission had received very few applications from individuals seeking a bounty, and there were very few payments made under the program, possibly because the program was not widely recognized. The report also determined the program was not well-designed.<ref name="2013 evaluation">{{cite web|url=http://www.sec.gov/oig/reportspubs/511.pdf|title=Evaluation of the SEC's Whistleblower Program|date=January 18, 2013|publisher=US Securities and Exchange Commission|accessdate=November 23, 2015}} {{PD-notice}}</ref> The former Chief of the Office of the Whistleblower [[Sean McKessy]] left the agency in July 2016 after serving for five years and later joined a whistleblower law firm.<ref>{{cite web|title=Former SEC Whistleblower Office Chief Sean X. McKessy joins whistleblower law firm Phillips & Cohen|url=http://www.phillipsandcohen.com/2016/Former-SEC-Whistleblower-Office-Chief-Sean-X-McKessy-joins-whistleblower-law-firm-Phillips-Cohen.shtml|website=Phillips & Cohen LLP}}</ref><ref>https://dealbook.nytimes.com/2013/04/23/hazy-future-for-s-e-c-s-whistle-blower-office/?_r=0</ref> On January 17, 2012, the SEC whom had then named [[Jane A. Norberg]] to the Office’s Deputy Chief roll.<ref>{{cite web|url=http://www.sec.gov/News/PressRelease/Detail/PressRelease/1365171482232#.VBxbndiBHX4|title=SEC.gov - SEC Names Jane Norberg as Deputy Chief of Whistleblower Office|date=17 January 2012|work=sec.gov}}</ref> [[Jane A. Norberg]] took over for the departing [[Sean McKessy]] departed in July 2016.<ref>{{cite web |url= https://www.sec.gov/news/pressrelease/2016-136.html |title= SEC.gov Sean McKessy, Chief of Whistleblower Office, to Leave SEC |work= Securities and Exchange Commission}}</ref> when it was announced that Sean McKessy, Chief of Whistleblower Office Leave his office.<ref>{{cite web |url=
http://www.wsj.com/articles/head-of-sec-whistleblower-office-to-leave-this-month-1468001522 |title= Wsj.com Head of SEC Whistleblower Office to Leave This Month; Deputy Chief Jane Norberg will take over as acting chief |work=WSJ.com |author= Maria Armental}}</ref> The SEC named Jane A. Norberg Chief of the Office of the Whistleblower on September 28, 2016.<ref>{{cite web |url= https://www.sec.gov/news/pressrelease/2016-201.html |title= SEC.gov Jane Norberg Named Chief of SEC Whistleblower Office |work= Securities and Exchange Commission}}</ref>

Former director [[Sean McKessy]] once said he understood the public's skepticism about the program. "I view it as already having been a very significant success, but I understand that people want to see the deliverable. And the deliverable, in our view, is paying people for good information," he said. He added that he welcomes the opportunity to make payouts: "The more, the better, obviously. The higher the amounts, the better," he said.<ref>{{cite web|url=http://www.huffingtonpost.com/2012/05/31/sec-whistleblower-reward-payout_n_1560044.html|title=SEC Whistleblowers Waiting For Big Payouts As Rumors Of First Award Mount|work=The Huffington Post}}</ref> "We are likely to see more awards at a faster pace now that the program has been up and running and the tips we have gotten are leading to successful cases," McKessy told the ''Wall Street Journal'' in June 2013.<ref>{{cite web|url=http://www.forbes.com/sites/erikakelton/2013/06/26/sec-officials-on-whistleblower-rewards-the-best-is-yet-to-come/|title=SEC Whistleblower Rewards: Larger Ones Are Coming|author=Erika Kelton|work=Forbes}}</ref>

SEC Chair Mary Jo White called the whistleblower program a "game changer," in an April 2015 speech. "The program is a success – and we will work hard at the SEC to build on that success," she said.<ref>{{Cite web|title = SEC.gov {{!}} The SEC as the Whistleblower's Advocate|url = http://www.sec.gov/news/speech/chair-white-remarks-at-garrett-institute.html|website = www.sec.gov|accessdate = 2015-12-16}}</ref>

=== Appellate Court Challenges ===

On December 12 of 2015 a anonymous party filed the first petition for a writ of [[mandamus]] in John Doe v. SEC<ref>https://assets.documentcloud.org/documents/2647637/John-Doe-SEC-Whistleblower-Petition.pdf</ref> the motion is  a rarely used motion that asks for intervention of the [[United States Court of Appeals for the District of Columbia Circuit]] into the SEC negligent operation in dealing with the claim. The motion claims that the SEC whistleblowers office had been negligent in a case originally posted as available for reward in 2012, the motion goes on to state that the anonymous petitioner claims applied for the reward in October 2012 and three years later there had been no preliminary determination made in the case USCA #15-1444.<ref>http://kmblegal.com/whistleblower-law-blog/sec-whistleblower--program-delays-what-you-need-to-know</ref> the petition went into detailed facts about the backlog of the program, and was highly redacted. It was later rendered as moot <ref>http://www.nationallawjournal.com/id=1202749155207/SEC-Responds-to-Whistleblower-Who-Challenged-Agency-Delay?slreturn=20160724012824</ref> when the SEC issued an award to the anonymous party.

==Whistleblower process==

===Phase 1 – Intake/triage===

During Phase 1 of intake and triage, whistleblowers submit a complaint to the SEC. Designated Division of Enforcement staff review the complaint to determine whether it should be assigned for further investigation or based on their initial review no further action (NFA) is warranted. Whistleblowers can submit a complaint using SEC form TCR to the SEC.<ref name="2013 evaluation"/>

Online submissions are automatically uploaded into the SEC’s Tips, Complaints, and Referrals system. Complaints received by mail and fax are manually entered into the TCR system by the Tips, Complaints, and Referrals intake group. The Office of Market Intelligence (OMI), located within Enforcement, reviews all Tips, Complaints, and Referrals and whistleblower complaints Enforcement receives.<ref name="2013 evaluation"/>

OMI triages all Tips, Complaints, and Referrals received by Enforcement. When Office of Market Intelligence determines a complaint warrants further investigation, OMI assigns the complaint to one of the SEC's 11 regional offices, an Enforcement specialized unit, or an Enforcement Associate Director group located in the SEC's Headquarters. When it is determined that a complaint does not warrant further investigation or the complaint does not fall into Enforcement’s priorities, Office of Market Intelligence will designate the complaint as No Further Action. No Further Actions get a second review before a final decision is made to close the complaint. In some cases NFAs may be referred to an external government agency or other agency for action. On occasion the Office of the Whistleblower Chief will determine On occasion the Office of the Whistleblower Chief will determine that a whistleblower Tips, Complaints, and Referrals is sufficiently specific, timely and credible which results in the Tips, Complaints, and Referrals being expedited through the triage process and assigned to investigative staff by Office of Market Intelligence.<ref name="2013 evaluation"/>

===Phase 2 – Tracking===

During Phase 2, Office of the Whistleblower personnel monitor whistleblower submissions that are assigned to investigative staff. Also, during this Phase, Office of the Whistleblower ––tracks whistleblower cases to document the whistleblower’s cooperation and the content and helpfulness of whistleblower information, answers questions, and aids Enforcement staff by providing subject matter expertise regarding the whistleblower program.<ref name="2013 evaluation"/>

Furthermore, Office of the Whistleblower documents information needed to process whistleblower awards. The office conducts quarterly conference calls with investigative staff to reconcile items that are tracked, with work that is assigned and resourced, and to discuss the quality of each whistleblower complaint.<ref name="2013 evaluation"/>

A whistleblower complaint results in a successful action against a defendant if the monetary sanctions:
* Exceed $1 million.  The whistleblower may then be eligible for a monetary award if all statutory criteria are met.
* Do not exceed $1 million the whistleblower is not immediately eligible for a monetary award. However, if the case is aggregated with related SEC  actions that arise out of a common body of operative facts and the total monetary sanctions in the related SEC actions collectively exceed $1 million, then the whistleblower may be eligible for an award.<ref name="2013 evaluation"/>

===Phase 3 – Claim for an award===

During Phase 3, a whistleblower can claim an award if information he or she provided to the Office of the Whistleblower leads to, or significantly contributes to, a successful U.S. Securities and Exchange Commission action. This action could result in the whistleblower receiving a monetary award if the sanctions ordered are over $1 million. Office of the Whistleblower posts a Notice of Covered Action on its website for cases that result in monetary sanctions over $1 million. Whistleblowers have 90 days to submit a claim for an award using the Form WB-APP (application). Office of the Whistleblower’s website provides a notice date and a claim due date for each covered whistleblower action.<ref name="2013 evaluation"/>

Whistleblowers receive their awards from the Securities and Exchange Commission Investor Protection Fund, established pursuant to Section 922 of the Dodd-Frank Act.<ref name="2013 evaluation"/>

When the Office of the Whistleblower or Enforcement staff knows that a whistleblower has provided a tip that led or significantly contributed to a successful action, they should contact the whistleblower or the attorney for the whilstleblower and inform him or her that a Notice of Covered Action has been posted on its website in connection with the tip or information he or she provided. The Office of the Whistleblower should advise the whistleblower on the process and timeline to apply for the award.<ref name="2013 evaluation"/>

Office of the Whistleblower staff analyze the claims for awards to assess whether the whistleblower satisfied the eligibility and definitional requirements for an award. When a whistleblower is determined to have satisfied these criteria, Office of the Whistleblower then uses four positive and three negative factors to derive a recommended award range between 10 and 30 percent of the dollar amount that was collected in the action.<ref name="2013 evaluation"/>

Office of the Whistleblower process for its analyses includes reviewing and comparing the facts of a claim to the whistleblower statute and regulations, reviewing relevant databases for information regarding the case and subsequent enforcement action, interviewing Enforcement staff regarding the case and the whistleblower’s actions, interviewing the whistleblower and/or their counsel, and conducting due diligence and legal research to ensure proper consideration is given to each award claim.<ref name="2013 evaluation"/>

The positive factors considered in recommending an award’s percentage include the significance of the whistleblower information, assistance and cooperation from the whistleblower in the investigation and proceedings, any law enforcement interest advanced by a potentially higher award, and whether the whistleblower cooperated with the company’s internal compliance system in connection with the matter. The negative factors considered include the whistleblower’s culpability, an unreasonable delay in reporting wrongdoing, and the whistleblower’s interference with the company’s internal compliance system. Though Office of the Whistleblower considers both these positive and negative factors, the office has discretion in making award recommendations.<ref name="2013 evaluation"/>

When making an award recommendation, Office of the Whistleblower submits a recommendation package to Enforcement’s Claims Review Staff. They then meet with the Claims Review Staff. A preliminary determination is prepared and forwarded to the whistleblower. A whistleblower has 30 days to request a copy of the record the Claims Review Staff based its decision on and/or to request a meeting with Office of the Whistleblower staff.<ref name="2013 evaluation"/>

Should a whistleblower’s claim be denied in the preliminary determination phase and the whistleblower fail to submit a timely response, the preliminary determination becomes the SEC’s final order. If the whistleblower submits a timely response to appeal the preliminary determination decision, Office of the Whistleblower’s staff will assess the appeal and make a recommendation to the Claims Review Staff. The Office of the Whistleblower then meets again with the Claims Review Staff and the Claims Review Staff makes a proposed final determination. Office of the Whistleblower then notifies the Commission of the proposed final determination. The Commission has 30 days to review this determination. Any Commissioner can request within 30 days of receiving the proposed final determination notification, that the proposed final determination be reviewed by the Commission. If no Commissioner objects during the 30-day window, the proposed final determination becomes the final order and Office of the Whistleblower then provides a copy of the final order to the whistleblower or the whilstleblower's attorney.<ref name="2013 evaluation"/>

After the final order has been issued, if a whistleblower has gotten an award that falls between 10 and 30 percent of the monetary sanctions collected in the action, the process is complete and the amount is not subject to appeal. However, if the whistleblower did not receive an award, or the award percentage is outside the statutory 10 to 30 percent that is collected from an action, the whistleblower may appeal the decision at the Federal Court of Appeals level. The Office of the General Counsel handles these appeals for the Commission.<ref name="2013 evaluation"/>

Whistleblower awards are paid out of the Investor Protection Fund that was established in the Dodd-Frank Act. Payments from the IPF are made through the SEC’s Office of Financial Management and are based on amounts that were collected from each individual case. A single payment can be made to the whistleblower if all monies are collected at the time the final order is issued, or the payment can occur on a rolling basis if the monies are collected over time, after the final order is issued.<ref name="2013 evaluation"/>

To file a claim for a whistleblower award, one must file Form WB-APP, Application for Award for Original Information Provided Pursuant to Section 21F of the [[Securities Exchange Act of 1934]].<ref name="2013 evaluation"/>

===Updating whistleblowers about the status of their applications===

Below are the ways SEC Office of the Whistleblower communicates with whistleblowers after they submit an award application to the SEC:
* An acknowledgement or deficiency letter is sent to all applicants indicating whether the whistleblower’s application is procedurally correct.
* An SEC Office of the Whistleblower attorney who conducts a full review of a covered action generally communicates with whistleblowers who have submitted award applications under a covered action.
* A written notification of the Claims Review Staff’s preliminary determination and whistleblower rights in the awards claims process are sent to the applicant.
* There’s an opportunity for the whistleblower to request the record that was used by the Claims Review Staff in making the preliminary determination.
* There’s an opportunity for the whistleblower to request a meeting with Office of the Whistleblower to discuss the preliminary determination.
* If a claim is appealed to the Claims Review Staff, (1) a written acknowledgment of receipt of appeal and (2) the results of the appeal (i.e., proposed final determination), will be sent to the whistleblower.
* If the preliminary determination is not appealed to the Claims Review Staff and no award is made, OWB sends a letter enclosing the final order of the Commission to the whistleblower.
* Written notification of a proposed final determination being issued (whether pursuant to an appeal to the Claims Review Staff or by virtue of the preliminary determination becoming a proposed final determination under the statute in the case of an award being recommended).
* Notification of the Commission’s final order.<ref name="2013 evaluation"/>

==Office of the Inspector General Evaluation==

In the [[Office of the Inspector General]] Evaluation of the SEC's Whistleblower Program, January 18, 2013, the program was outlined by the Inspector General, providing some insight into its inner workings. Section 922 of the [[Dodd-Frank Wall Street Reform and Consumer Protection Act]] (Dodd-Frank), amended the [[Securities Exchange Act of 1934]] (Exchange Act) by adding Section 21F, "Securities Whistleblower Incentives and Protection." Section 21F directs the SEC to make monetary awards to eligible individuals who voluntarily provide original information that leads to successful SEC enforcement actions resulting in the imposition of monetary sanctions over $1 million, and certain related successful actions. The SEC can make awards ranging from 10 to 30 percent of the monetary sanctions collected, which are paid from the SEC’s Investor Protection Fund. In addition, Section 924(d) of [[Dodd-Frank]] directed the SEC to establish a separate office within the Commission to administer the whistleblower program. In February 2011, the Commission established the Office of the Whistleblower to carry out this function.<ref name="2013 evaluation"/>

On May 25, 2011, the SEC adopted final Regulation 21F to implement the provisions of Section 21F of the Exchange Act. Regulation 21F became effective on August 12, 2011. Among other things, Regulation 21F defines terms that are essential to the whistleblower’s program operations, establishes procedures for submitting tips and applying for awards, including appeals of SEC determinations, and whether and to whom to make an award; describes the criteria the SEC will consider in making award decisions, and implements Dodd-Frank’s prohibition against retaliation for whistleblowing.<ref name="2013 evaluation"/>

== Use of the Investor Protection Fund ==

In 2012, the Investor Protection Fund was used to pay one whistleblower award which amounted to approximately $46,000. As mentioned, the fund is also used to pay for OIG’s employee suggestion-program activities which amounted to $112,000 in fiscal year 2011 and $70,000 in fiscal year 2012. Even though some expenditures were paid from the fund, its balance has not substantially changed since the fund was established.<ref name="2013 evaluation"/>

===Establishment of the fund===

The Investor Protection Fund was established in the fourth quarter of fiscal year 2010 to be available to the U.S. Securities and Exchange Commission, without further appropriation or fiscal year limitation for paying awards to whistleblowers and funding the work activities of [[Office of Inspector General]]’s employee suggestion program.<ref name="2013 evaluation"/>

The SEC is required to annually request and obtain apportionments from the Office of Management and Budget to use these funds. OFM has developed policies and procedures for Investor Protection Fund that include a description of the [[whistleblower]] awards process, financial reporting requirements, budget request procedures, and procedures for replenishing the Investor Protection Fund.<ref name="2013 evaluation"/>

The Investor Protection Fund was established in August 2010 with approximately $452 million of non-exchange revenue that was transferred to the fund from the U.S. Securities and Exchange Commission’s disgorgement and penalties deposit fund.<ref name="2013 evaluation"/>

In the SEC’s fiscal year 2013 apportionment, nearly $452 million was still available in Investor Protection Fund. Since its establishment, the Investor Protection Fund’s balance has not fallen below $300 million, and no additional qualifying collections have been deposited into it.<ref name="2013 evaluation"/>

If the Investor Protection Fund balance drops below $300 million, Enforcement will replenish it by identifying qualifying receipts for deposit.<ref name="2013 evaluation"/>

The fund earns interest through short-term investments with the Bureau of Public Debt.<ref name="2013 evaluation"/>

==Freedom of Information Act exemption==

There are nine exemptions to the [[Freedom of Information Act]], which the SEC and other federal agencies can use to deny the release of certain information which the public may request. Exemption 3, 5 U.S.C. Section 552(b)(3)(B) or (b)(3) pertains to information that is prohibited from disclosure by another federal law.<ref name="2013 evaluation"/>

==Forms==
The following are some related SEC forms:
* "[http://www.sec.gov/about/forms/formtcr.pdf SEC form TCR]" (pdf), to submit information to the SEC Fax (703) 813-9322
* "[http://www.sec.gov/about/forms/formwb-app.pdf WB-APP SEC Whistleblower Award Application]" (pdf), to collect a reward from the SEC Fax (703) 813-9322

==References==
{{reflist|30em}}

==External links==
*[http://www.sec.gov/whistleblower SEC Office of the Whistleblower] 100 F Street NE, Washington, DC 20549-5631
*[http://www.sec.gov/about/offices/owb/owb-final-orders.shtml  Final Orders]
*[http://www.sec.gov/about/offices/owb/owb-awards.shtml Notice of Covered Actions]
*[http://www.sec.gov/about/offices/owb/annual-report-2012.pdf 2012 Annual Report on the Dodd-Frank Whistleblower Program]
*[http://www.sec-oig.gov/Reports/AuditsInspections/2013/511.pdf Inspectors report Jan 2013]
*[http://www.sec.gov/about/offices/owb/annual-report-2013.pdf 2013 Annual Report on the Dodd-Frank Whistleblower Program]
*[http://www.sec.gov/about/offices/owb/annual-report-2014.pdf 2014 Annual Report to the [[United States Congress]] on the Dodd-Frank Whistleblower Program]
*[https://online.wsj.com/articles/meet-the-secs-6-500-whistleblowers-1406591157 Meet the SEC's 6500 Whistleblowers]

[[Category:U.S. Securities and Exchange Commission]]
[[Category:Financial regulation in the United States]]
[[Category:Whistleblower support organizations]]