{{see also|Georgia Tech Yellow Jackets men's basketball}}

{{Infobox college basketball team|women=yes
|current = 2016-17 Georgia Tech Yellow Jackets women's basketball team
|name = Georgia Tech Yellow Jackets Basketball
|logo = Georgia Tech Outline Interlocking logo.svg
|logo_size = 150px
|university = Georgia Institute of Technology
|conference = Atlantic Coast Conference
|conference_short = ACC
|division =
|city = Atlanta
|stateabb = GA
|state = Georgia (U.S. state)
|coach = [[MaChelle Joseph]]
|tenure = 12th
|arena = [[Hank McCamish Pavilion]]
|capacity = 8600
|nickname = [[Georgia Tech Yellow Jackets|Yellow Jackets]]
|NCAAchampion =
|NCAArunnerup = 
|NCAAfinalfour = 
|NCAAeliteeight = 
|NCAAsweetsixteen = 2012
|NCAAtourneys = 1993, 2003, 2007, 2008, 2009, 2010, 2011, 2012, 2014
|conference_tournament = 
|conference_season = 
|h_pattern_b=_thinsidesonwhite
|h_body=C59353
|h_shorts=C59353
|h_pattern_s=_blanksides2
|a_pattern_b=_thinmidnightbluesides
|a_body=C59353
|a_shorts=C59353
|a_pattern_s=_midnightbluesides
|3_pattern_b=_vegasgoldsides
|3_body=00254C
|3_shorts=00254C
|3_pattern_s=_vegasgoldsides
}}

The '''Georgia Tech Yellow Jackets women's basketball''' team represents the [[Georgia Institute of Technology|Georgia Tech]] [[Georgia Tech Yellow Jackets|Yellow Jackets]] in [[NCAA Division I]] [[basketball]].<ref>{{Cite web|url=http://www.ramblinwreck.com/sports/w-baskbl/geot-w-baskbl-body.html|title=Georgia Tech Official Athletic Site - RamblinWreck.com|website=www.ramblinwreck.com|access-date=2016-04-13}}</ref> The team plays its home games in [[Alexander Memorial Coliseum]].

==2014–15 Roster==
{{CBB roster/Header|sex =w|year=2014|team=Georgia Tech Yellow Jackets| teamcolors=y|high_school=y}}
{{CBB roster/Player|sex=w|first=Imani|last=Tilford|link=n|num=0|pos=PG|ft=5|in=5|class=FR|rs=|home=Greenburgh, N.Y.|high_school=Woodlands}}
{{CBB roster/Player|sex=w|first=Aaliyah|last=Whiteside|link=n|num=2|pos=GF|ft=6|in=0|class=JR|rs=|home=Memphis, Tenn.|high_school=Memphis Central}}
{{CBB roster/Player|sex=w|first=Kaela|last=Davis|link=n|num=3|pos=G|ft=6|in=2|class=SO|rs=|home=Buford, Ga.|high_school=Buford}}
{{CBB roster/Player|sex=w|first=Katarina|last=Vuckovic|link=n|num=10|pos=F|ft=6|in=3|class=SO|rs=|home=Smeredevo, Serbia|high_school=Crvena Zvezda}}
{{CBB roster/Player|sex=w|first=Nariah|last=Taylor|link=n|num=11|pos=C|ft=6|in=5|class=JR|rs=|home=Indianapolis, Ind.|high_school=North Central}}
{{CBB roster/Player|sex=w|first=Erin|last=Garner|link=n|num=13|pos=F|ft=6|in=3|class=FR|rs=|home=Philadelphia, Pa.|high_school=Trenton Catholic Academy}}
{{CBB roster/Player|sex=w|first=Simina|last=Avraml|link=n|num=14|pos=C|ft=6|in=3|class=FR|rs=|home=Brasov, Romania|high_school=Andre Saguna}}
{{CBB roster/Player|sex=w|first=Zaire|last=O'Neil|link=n|num=21|pos=F|ft=5|in=11|class=FR|rs=|home=Newark, N.J.|high_school=Malcolm X Shabazz}}
{{CBB roster/Player|sex=w|first=Sydney|last=Wallace|link=n|num=23|pos=G|ft=5|in=8|class=SR|rs=|home=Johns Creek, Ga.|high_school=Northview High School}}
{{CBB roster/Player|sex=w|first=De'Ashia|last=Jones|link=n|num=24|pos=F|ft=5|in=10|class=FR|rs=|home=Newark, N.J.|high_school=Malcolm X Shabazz}}
{{CBB roster/Player|sex=w|first=Roddreka|last=Rogers|link=n|num=54|pos=F|ft=6|in=0|class=JR|rs=|home=Charlotte, N.C.|high_school=Myers Park}}
{{CBB roster/Player|sex=w|first=Antonia|last=Peresson|link=n|num=55|pos=G|ft=5|in=9|class=FR|rs=|home=Pordenone, Italy|high_school=Liceo Stefanini Yenezia}}
{{CBB roster/Footer|head_coach=[[MaChelle Joseph]]|asst_coach=M.L. Willis <br />Michael Wholey  <br />Deja Foster|roster_url=http://www.ramblinwreck.com/sports/w-baskbl/mtt/geot-w-baskbl-mtt.html|accessdate=2014-12-23}}

==Players==
{{See also|List of Georgia Institute of Technology athletes#Basketball}}
Many famous and talented players have played with the Yellow Jackets, including [[Niesha Butler]], [[Kisha Ford]], and [[Chioma Nnamaka]].

==Stadium==
{{Main|Alexander Memorial Coliseum}}
[[Image:Alexander Memorial Coliseum SW view.jpg|thumb|right|300px|[[Alexander Memorial Coliseum]] has been home to the Yellow Jackets since 1956.]]
The Alexander Memorial Coliseum (also nicknamed "The Thrillerdome") is an [[list of indoor arenas|indoor arena]] located in [[Atlanta, Georgia|Atlanta]], [[Georgia (U.S. state)|Georgia]]. It is the home of the Georgia Tech [[basketball]] teams and hosted the [[Atlanta Hawks]] of the [[National Basketball Association]] from 1968–1972 and again from 1997–1999.  Tech's women's volleyball team has occasionally used the facility as well, primarily for NCAA tournament games and other matches that draw crowds that would overflow the [[O'Keefe Gymnasium]].

During the 2011-12 basketball season, Alexander Memorial Coliseum was being rebuilt as the Henry F. McCamish Jr. Pavilion (McCamish Pavilion). Except for the final home game, the Georgia Tech women's basketball team played its home games at The Arena at Gwinnett Center (also known as Gwinnett Arena) in suburban Duluth, GA.

==Year by year results==

Conference tournament winners noted with # Source<ref name="Media Guide">{{cite web|last=|first=|date=|title=Media Guide|url=http://www.ramblinwreck.com/sports/w-baskbl/spec-rel/0910-media-guide.html|work=Georgia Tech|publisher=|accessdate=9 Aug 2013}}</ref>
{{CBB yearly record start with polls|=team}}
{{CBB yearly record subhead|name=Jim Culpepper|conference=ACC|startyear=1974|endyear=1980}}
{{CBB yearly record entry with polls|championship=|season=1974-75|name=Jim Culpepper|overall=6–16|conference=–| confstanding=|postseason=GAIAW State Tournament|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=1975-76|name=Jim Culpepper|overall=2–24|conference=–| confstanding=|postseason=GAIAW State Tournament|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=1976-77|name=Jim Culpepper|overall=19–8|conference=–| confstanding=|postseason=GAIAW State Tournament|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=1977-78|name=Jim Culpepper|overall=23–4|conference=–| confstanding=|postseason=GAIAW State Tournament|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=1978-79|name=Jim Culpepper|overall=18–8|conference=–| confstanding=|postseason=GAIAW State Tournament|APpoll= |Coachespoll= }}
|-style="background: #ffffdd;"
| colspan="8" align="center" | '''[[Atlantic Coast Conference]]'''
{{CBB yearly record entry with polls|championship=|season=1979-80|name=Jim Culpepper|overall=2–23|conference=1–7| confstanding=7th|postseason=GAIAW State Tournament|APpoll= |Coachespoll= }}
{{CBB yearly record subtotal|championship=|season=|name=Jim Culpepper|overall=70–83|confrecord =1–8| constanding=ACC|posteason=}}
{{CBB yearly record subhead|name=Benny Dees|conference=ACC|startyear=1980|endyear=1981}}
{{CBB yearly record entry with polls|championship=|season=1980-81|name=Benny Dees|overall=8–19|conference=1–6| confstanding=7th|postseason=|APpoll= |Coachespoll= }}
{{CBB yearly record subtotal|championship=|season=|name=Benny Dees|overall=8–19|confrecord =1–6| constanding=ACC|posteason=}}
{{CBB yearly record subhead|name=Bernadette McGlade|conference=ACC|startyear=1981|endyear=1988}}
{{CBB yearly record entry with polls|championship=|season=1981-82|name=Bernadette McGlade|overall=9–16|conference=2–9| confstanding=7th|postseason=|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=1982-83|name=Bernadette McGlade|overall=10–19|conference=2–11| confstanding=7th|postseason=|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=1983-84|name=Bernadette McGlade|overall=9–19|conference=1–13| confstanding=8th|postseason=|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=1984-85|name=Bernadette McGlade|overall=11–16|conference=0–14| confstanding=8th|postseason=|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=1985-86|name=Bernadette McGlade|overall=10–17|conference=1–13| confstanding=8th|postseason=|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=1986-87|name=Bernadette McGlade|overall=14–13|conference=5–9| confstanding=6th|postseason=|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=1987-88|name=Bernadette McGlade|overall=11–16|conference=3–11| confstanding=T-7th|postseason=|APpoll= |Coachespoll= }}
{{CBB yearly record subtotal|championship=|season=|name=Bernadette McGlade|overall=74–116|confrecord =14–80| constanding=ACC|posteason=}}
{{CBB yearly record subhead|name=Agnus Berenato|conference=ACC|startyear=1988|endyear=2003}}
{{CBB yearly record entry with polls|championship=|season=1988-89|name=Agnus Berenato|overall=14–14|conference=5–9| confstanding=6th|postseason=|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=1989-90|name=Agnus Berenato|overall=13–17|conference=4–10| confstanding=T-6th|postseason=|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=1990-91|name=Agnus Berenato|overall=15–13|conference=3–11| confstanding=7th|postseason=|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=1991-92|name=Agnus Berenato|overall=20–13|conference=6–10| confstanding=7th|postseason=NWIT Champions|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=1992-93|name=Agnus Berenato|overall=16–11|conference=8–8| confstanding=T-4th|postseason=NCAA First Round|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=1993-94|name=Agnus Berenato|overall=12–15|conference=5–11| confstanding=7th|postseason=|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=1994-95|name=Agnus Berenato|overall=14–16|conference=5–11| confstanding=6th|postseason=|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=1995-96|name=Agnus Berenato|overall=14–13|conference=5–11| confstanding=8th|postseason=|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=1996-97|name=Agnus Berenato|overall=15–12|conference=7–9| confstanding=7th|postseason=|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=1997-98|name=Agnus Berenato|overall=11–17|conference=3–13| confstanding=8th|postseason=|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=1998-99|name=Agnus Berenato|overall=13–14|conference=6–10| confstanding=6th|postseason=|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=1999-2000|name=Agnus Berenato|overall=17–14|conference=7–9| confstanding=6th|postseason=WNIT Semifinals|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=2000-01|name=Agnus Berenato|overall=14–15|conference=5–11| confstanding=8th|postseason=WNIT First Round|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=2001-02|name=Agnus Berenato|overall=15–14|conference=7–9| confstanding=T-5th|postseason=WNIT First Round|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=2002-03|name=Agnus Berenato|overall=20–11|conference=8–8| confstanding=T-4th|postseason=NCAA First Round|APpoll= |Coachespoll= }}
{{CBB yearly record subtotal|championship=|season=|name=Agnus Berenato|overall=223–209|confrecord =84–150| constanding=ACC|posteason=}}
{{CBB yearly record subhead|name=MaChelle Joseph|conference=ACC|startyear=2003|endyear=}}
{{CBB yearly record entry with polls|championship=|season=2003-04|name=MaChelle Joseph|overall=14–15|conference=5–11| confstanding=8th|postseason=|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=2004-05|name=MaChelle Joseph|overall=13–14|conference=4–10| confstanding=T-8th|postseason=|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=2005-06|name=MaChelle Joseph|overall=14–15|conference=2–12| confstanding=T-11th|postseason=|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=2006-07|name=MaChelle Joseph|overall=21–12|conference=9–5| confstanding=6th|postseason=NCAA Second Round|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=2007-08|name=MaChelle Joseph|overall=22–10|conference=7–7| confstanding=T-5th|postseason=NCAA First Round|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=2008-09|name=MaChelle Joseph|overall=22–10|conference=8–6| confstanding=T-5th|postseason=NCAA Second Round|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=2009-10|name=MaChelle Joseph|overall=23–10|conference=8–6| confstanding=4th|postseason=NCAA First Round|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=2010-11|name=MaChelle Joseph|overall=24–11|conference=9–8| confstanding=T-4th|postseason=NCAA Second Round|APpoll= |Coachespoll=24}}
{{CBB yearly record entry with polls|championship=|season=2011-12|name=MaChelle Joseph|overall=26–9|conference=12–4| confstanding=T-4th|postseason=NCAA Sixteen|APpoll=10|Coachespoll=15}}
{{CBB yearly record entry with polls|championship=|season=2012-13|name=MaChelle Joseph|overall=14–16|conference=7–11| confstanding=T-7th|postseason=|APpoll= |Coachespoll= }}
{{CBB yearly record entry with polls|championship=|season=2013-14|name=MaChelle Joseph|overall=20–12|conference=9–7| confstanding=7th|postseason=NCAA First Round|APpoll= |Coachespoll= }}
{{CBB yearly record subtotal|championship=|season=|name=MaChelle Joseph|overall=213–134|confrecord =80–87| constanding=ACC|posteason=}}
{{CBB yearly record end|overall=588–561}}

==References==
{{reflist}}

==External links==
* {{official website}}

{{Georgia Tech Yellow Jackets women's basketball navbox}}
{{Georgia Tech Navbox}}
{{Atlantic Coast Conference women's basketball navbox}}

[[Category:Georgia Tech Yellow Jackets women's basketball| ]]