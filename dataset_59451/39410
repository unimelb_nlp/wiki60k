{{For|other people named Chris Carter|Chris Carter (disambiguation)}}
{{Good article}}
{{Infobox person
| name        = Chris Carter
| image       = Chris Carter (July 2008).jpg
| caption     = Carter at the [[London]] premiere of ''[[The X-Files: I Want to Believe]]'' in July 2008.
| birth_date  = {{Birth date and age|1957|10|13|mf=y}}
| birth_place = [[Bellflower, California]], U.S.
| death_date  = 
| death_place = 
| notable_works = ''[[The X-Files]]''<br />''[[Millennium (TV series)|Millennium]]''
| occupation  = Writer, director, producer
| partner      = Dori Pierson (1983–present)
| alma_mater= [[California State University, Long Beach]]
}}

'''Chris Carter''' (born October 13, 1957) is an American television and film producer, director and writer. Born in [[Bellflower, California]], Carter graduated with a degree in journalism from [[California State University, Long Beach]] before spending thirteen years working for ''[[Surfing Magazine]]''. After beginning his television career working on television films for [[Walt Disney Studios (production)|Walt Disney Studios]], Carter rose to fame in the early 1990s after creating the [[science fiction]]-[[supernatural fiction|supernatural]] television series ''[[The X-Files]]'' for the [[Fox Broadcasting Company|Fox network]]. ''The X-Files'' earned high viewership ratings, and led to Carter's being able to negotiate the creation of future series.

Carter went on to create three more series for the network—''[[Millennium (TV series)|Millennium]]'', a doomsday-themed series which met with critical approval and low viewer numbers; ''[[Harsh Realm]]'', which was canceled after three episodes had aired; and ''[[The Lone Gunmen (TV series)|The Lone Gunmen]]'', a spin-off of ''The X-Files'' which lasted for a single season. Carter's film roles include writing both of ''The X-Files''{{'}} cinematic spin-offs—1998's successful ''[[The X-Files (film)|The X-Files]]'' and the poorly received 2008 follow-up ''[[The X-Files: I Want to Believe]]'', the latter of which he also directed—while his television credits have earned him several accolades including eight [[Primetime Emmy Award]] nominations. The [[Amazon Studios]] television series ''[[The After]]'' was his most recent series.<ref name="thr_Jul12">{{Cite web|url=http://www.hollywoodreporter.com/live-feed/chris-carter-after-99-episode-goal-718161|title=Chris Carter Modeling Amazon's 'The After' With 99-Episode Goal|work=The Hollywood Reporter |last=Ng |first=Philiana |date=July 12, 2014|accessdate=November 20, 2014}}</ref>

==Early life==
Chris Carter was born on October 13, 1957 in [[Bellflower, California]].{{sfn|Lovece|1996|p=2}} His father worked in the construction industry.{{sfn|Lowry|1995|p=7}} Carter has described his childhood as "fairly normal", and was fond of both [[Little League Baseball]] and surfing;{{sfn|Edwards|1996|p=9}} his surfing stance is [[Footedness#Footedness in board sports|goofy footed]].<ref name="salon">{{cite web |url=http://www.salon.com/2000/04/28/chriscarter/ |title=A close encounter with Chris Carter |publisher=''[[Salon (magazine)|Salon]]'' |first=Russ |last=Spencer |date=April 28, 2000 |accessdate=July 23, 2012}}</ref> He attended [[California State University, Long Beach]] in [[Long Beach, California|Long Beach]], graduated with a journalism degree in 1979. An avid surfer, he began writing for ''[[Surfing Magazine]]'', a [[San Clemente, California|San Clemente]]-based journal, eventually becoming its editor at the age of 28. Carter would work for the magazine for thirteen years,{{sfn|Edwards|1996|p=9}} and credits his tenure there for teaching him how to run a business. It was also at this time that Carter began taking an interest in pottery, making "hundreds of thousands of pieces" of dinnerware as a hobby. He has compared the process of making pottery to [[Zen]] meditations, although he has since thrown out most of his work.{{sfn|Edwards|1996|p=9}}

==Career==

===Starting in television===
In 1983, Carter began dating Dori Pierson, whom he had met through a cousin of hers who worked with him at ''Surfing Magazine''.<ref name="rovi bio">{{cite web |url=http://www.allrovi.com/name/chris-carter-p240986 |title=Chris Carter - Movie and Film Biography and Filmography |work=[[Allmovie]] |publisher=[[Allrovi]] |first=Jason |last=Buchanan |accessdate=July 19, 2012}}</ref> Pierson's connections at [[Walt Disney Studios (production)|Walt Disney Studios]] led to chairman [[Jeffrey Katzenberg]] hiring Carter on a standard contract.{{sfn|Lovece|1996|p=3}} Carter began writing television films for the studio, penning ''[[The B.R.A.T. Patrol]]'' in 1986 and ''[[Meet the Munceys]]'' in 1988. These scripts led to Carter being associated with contemporary youth comedy at the studio, and although he enjoyed the work he felt that his real strengths and interests lay in serious drama instead.{{sfn|Edwards|1996|p=9}}

Carter met the then-president of [[NBC]], [[Brandon Tartikoff]], at a company softball game in [[Brentwood, California]]. Tartikoff and Carter began talking between innings, and when Tartikoff eventually read some of Carter's script work, he brought him over to write for the network. There, Carter developed a number of unproduced [[television pilot]]s—''Cameo By Night'', featuring [[Sela Ward]]; ''[[Brand New Life (TV series)|Brand New Life]]'', which has been described as being similar to ''[[The Brady Bunch]]''; ''Copter Cop'', a science fiction series that was hampered by Tartikoff's injuries after a car accident; and ''Cool Culture'', influenced by Carter's passion for surfing and experience with ''Surfing Magazine''.{{sfn|Lowry|1995|pp=8–9}} During this time Carter would also work as a [[television producer|producer]] on ''[[Rags to Riches (TV series)|Rags to Riches]]'', a job he accepted in order to learn more about producing a series.{{sfn|Edwards|1996|p=10}}

[[Peter Roth (executive)|Peter Roth]], at that time the president of [[Stephen J. Cannell Productions]], obtained a copy of Carter's pilot script for ''Cool Culture'', and although the series was never picked up, Roth was interested in hiring Carter to work on the [[CBS]] series ''[[Palace Guard]]''. However, Roth would soon leave CBS to work for [[Fox Broadcasting Company|Fox]] as the head of its television production wing. Carter was among the first wave of new staff hired by Roth in 1992 to develop material for the network,{{sfn|Lowry|1995|p=9}} and he began work on a series based on his own childhood fondness for ''[[The Twilight Zone]]'' and ''[[Kolchak: The Night Stalker]]''.{{sfn|Edwards|1996|p=10}}

===''The X-Files'' and success===
[[File:Chris Carter by Gage Skidmore.jpg|thumb|right|alt=A man speaking into a microphone|Carter in July 2013.]]

Carter's new series would take its stylistic inspiration from ''Kolchak'', while thematically reflecting his experiences growing up during the [[Watergate scandal]]. Carter also drew inspiration from his friend [[John E. Mack]]'s survey of American beliefs in [[ufology]], which indicated that {{nowrap|three percent}} of the population believed they had been abducted by aliens.{{sfn|Edwards|1996|p=11}} Roth warmed to the idea upon hearing of the influence of ''Kolchak'', believing that vampires—one of the central antagonists of the original series—would be popular with audiences given the interest being shown in the upcoming film ''[[Interview with the Vampire (film)|Interview with the Vampire]]'', although Carter insisted on an extraterrestrial-focused series.{{sfn|Lowry|1995|p=10}} However, Carter had never been interested in science fiction before this point, professing to have briefly read one novel each by [[Ursula K. Le Guin]] and [[Robert A. Heinlein]].{{sfn|Lovece|1996|p=4}} Basing his characters instead on those found in the English television series ''[[The Avengers (TV series)|The Avengers]]'', Carter took an eighteen-page treatment for his new project—by now titled ''[[The X-Files]]''—to a pitch meeting at Fox, where it was soon rejected.{{sfn|Lowry|1995|p=11}} With the help of Roth, Carter was able to arrange a second pitch meeting, at which the network reluctantly agreed to greenlight a pilot for the series.{{sfn|Lovece|1996|pp=3–4}}

After finding the series' two starring leads in [[Gillian Anderson]] and [[David Duchovny]],{{sfn|Lovece|1996|p=4}} Carter was given a budget of {{nowrap|[[United States dollar|$]]2 million}} to produce a [[Pilot (The X-Files)|pilot episode]].{{sfn|Lovece|1996|p=47}} The series aired on Friday nights on the Fox network, being broadcast in tandem with ''[[The Adventures of Brisco County, Jr.]]'' in what was perceived to be an unpopular timeslot. The series earned relatively impressive [[Nielsen ratings]] for its Friday timeslot, and was given a full twenty-four episode order.{{sfn|Lovece|1996|p=4}} The series' popularity and critical acclaim built over the course of its second and third seasons, and saw it earning its first [[Golden Globe Award for Best Television Series – Drama]] and breaking the record for highest price paid by a network for rights to air re-runs, fetching $600,000 per episode from Fox's sister network [[FX (TV channel)|FX]].{{sfn|Lovece|1996|pp=6–7}} After Carter's initial three-year signing for Fox had ended, the success of the series allowed him to negotiate a five-year contract with several additional perks, including the guarantee of a feature film adaptation to be produced by the parent company's film studio, and the greenlighting of Carter's next television project.{{sfn|Lovece|1996|p=7}} In March 2015, it was confirmed that Carter is set to executive produce and write the screenplays for the revival of ''[[The X-Files]]'',<ref>{{cite web|url=http://bloody-disgusting.com/news/3337066/x-files-close-return-gillian-anderson-david-duchovny/|title="The X-Files" Returns w/ Gillian Anderson & David Duchovny?|work=Bloody Disgusting!}}</ref> which is set for a [[The X-Files (season 10)|six-episode event series]].<ref>{{cite web|url=http://bloody-disgusting.com/editorials/3337450/please-make-new-x-files-amazing/|title=A Plea to Make the New "X-Files" Limited Series Amazing -|work=Bloody Disgusting!}}</ref>

===''Millennium''===
Carter began work on a new series, ''[[Millennium (TV series)|Millennium]]''.<ref name="oic1"/> The genesis of this new project stemmed from "[[Irresistible (The X-Files)|Irresistible]]", a second-season episode of ''The X-Files'' which Carter had written, which focused on a sexually motivated serial killer with none of that series' usual supernatural trappings.<ref name="oic1">[[#Chaos|Order in Chaos]], 00:03–00:45</ref> Carter fleshed out the basis of the new show's protagonist, [[Frank Black (character)|Frank Black]], and travelled to [[Seattle]] for inspiration for a new setting. Influence was also drawn from the works of [[Nostradamus]], and the increasing popular interest in [[eschatology]] ahead of the coming millennium.<ref>[[#Chaos|Order in Chaos]], 00:48–01:51</ref> The central role of Black was eventually filled by [[Lance Henriksen]], and the series began airing in the Friday timeslot formerly occupied by ''The X-Files''.{{sfn|Genge|1997|pp=8–9}} "[[Pilot (Millennium)|Pilot]]", the début episode, was heavily promoted by Fox, and brought in over a quarter of the total audience during its broadcast.<ref name="Variety">{{cite news |url=http://www.variety.com/article/VR1117756490.html? |title= High-profile dramas skid on Fox, ABC|first=Josef |last=Adalian |date=October 11, 1998 |accessdate=July 20, 2012 |publisher=Variety Magazine}}</ref>

The series also attracted a high degree of critical appraisal, earning a [[People's Choice Awards|People's Choice Award]] for "Favorite New TV Dramatic Series" in its first year.<ref name="people">{{cite web |url=http://www.peopleschoice.com/pca/awards/nominees/index.jsp?year=1997 |title=People's Choice Awards 1997 Nominees |publisher=[[Procter & Gamble]] |accessdate=July 20, 2012}}</ref> At the beginning of the second season, Carter handed over control of the series to [[Glen Morgan]] and [[James Wong (producer)|James Wong]], with whom he had previously worked on both ''Millennium''{{'s}} first season and several seasons of ''The X-Files''.<ref name="fall">{{cite web |url=http://www.highbeam.com/doc/1G1-56443413.html |title=Fall Watch; 'Millennium' takes new turn |publisher=''[[The Boston Herald]]'' |first=Harvey |last=Soloman |date=September 18, 1997 |accessdate=July 20, 2012}} {{subscription required}}</ref> Despite its promising start, however, ratings for ''Millennium'' after the pilot remained consistently low, and it was cancelled after three seasons.<ref>{{cite web |url=http://www.salon.com/tech/feature/1999/09/09/virtual_season/index.html |title=It's not the end of the "Millennium," after all|work=[[Salon (magazine)|Salon]] |publisher=Salon Media Group |date=September 9, 1999 |accessdate=July 20, 2012 |last=Wen |first=Howard }}</ref>

===Feature films===
During the production of the fourth season of ''The X-Files'', work on the first feature film adaptation of the series began. Also titled ''[[The X-Files (film)|The X-Files]]'', Carter initially began a treatment for the script over Christmas holidays in Hawaii in 1996. Series producer [[Frank Spotnitz]] collaborated on the story outline at this time. Carter would later return to Hawaii for a ten-day stint in 1997 to begin fleshing out the finished script.{{sfn|Duncan|1998|pp=4–5}} Carter appointed frequent series director [[Rob Bowman (filmmaker)|Rob Bowman]] as director of the film,{{sfn|Duncan|1998|pp=5–6}} which went on to feature many of the series' regular cast, including Duchovny, Anderson, [[Mitch Pileggi]] and [[William B. Davis]].{{sfn|Duncan|1998|p=18}}

''The X-Files'' premiered on June 19, 1998, eventually making a worldwide gross of  $189,176,423.<ref>{{cite web |url=http://www.boxofficemojo.com/movies/?id=x-filesfightthefuture.htm |title=The X-Files (1998) |publisher=[[Box Office Mojo]] |accessdate=July 20, 2012}}</ref> The film currently holds a rating of 60 out of 100 on [[Review aggregator|review aggregation]] website [[Metacritic]], based on their weighted average of 23 reviews.<ref>{{cite web |url=http://www.metacritic.com/movie/the-x-files |title=The X Files Reviews, Ratings, Credits, and More |work=[[Metacritic]] |accessdate=July 20, 2012}}</ref> Fellow review aggregation site [[Rotten Tomatoes]] rates the film 64%, based on their analysis of 69 reviews.<ref>{{cite web |url=http://www.rottentomatoes.com/m/xfiles_fight_the_future/ |title=The X-Files - Fight the Future |work=[[Rotten Tomatoes]] |accessdate=July 20, 2012}}</ref>

Ten years after the success of the first film, and six years after ''The X-Files'' final season had finished, Carter would both write and direct a second feature film, titled ''[[The X-Files: I Want to Believe]]''.<ref name="iwtb">{{cite web |url=http://www.allmovie.com/movie/v423337 |title=The X-Files: I Want to Believes - Trailers, Reviews, Synopsis, Showtimes and Cast |work=[[Allmovie]] |publisher=[[Allrovi]] |first=Jeremy |last=Wheeler |accessdate=July 20, 2012}}</ref> Filmed in [[British Columbia]],<ref name="Province">{{cite web|url=http://www.canada.com/theprovince/news/story.html?id=3c696f83-c42f-4b57-9f7b-9372a358eea2&k=93084|title=Filming of the X-Files sequel wraps |date=March 12, 2008 |last=Schaefer |first= Glen |publisher=''[[Vancouver Province]]''|accessdate=July 20, 2012}}</ref> ''I Want To Believe'' was released on July 25, 2008;<ref name="iwtb"/> eventually grossing $68,369,434 worldwide.<ref>{{cite web |url=http://www.boxofficemojo.com/movies/?id=xfiles2.htm |title=The X-Files: I Want to Believe (2008) |publisher=[[Box Office Mojo]] |accessdate=July 20, 2012}}</ref> The film was received poorly by critics, holding ratings of 32 and 47 on Rotten Tomatoes and Metacritic respectively.<ref>{{cite web |url=http://www.rottentomatoes.com/m/x_files_2/ |title=The X-Files: I Want to Believe |work=[[Rotten Tomatoes]] |accessdate=July 20, 2012}}</ref><ref>{{cite web |url= http://www.metacritic.com/movie/the-x-files-i-want-to-believes |title=The X Files: I Want to Believe Reviews, Ratings, Credits, and More |work=[[Metacritic]] |accessdate=July 20, 2012}}</ref>

===Other work===
Carter has made several brief cameo roles as an actor—first appearing in ''The X-Files''{{'}} "[[Anasazi (The X-Files)|Anasazi]]" as a FBI agent;<ref>{{cite web |url=http://www.ew.com/ew/article/0,,295179_5,00.html |title=X Cyclopedia: The Ultimate Episode Guide, Season 2 |publisher=''[[Entertainment Weekly]]'' |date=November 29, 1996 |accessdate=July 28, 2012}}</ref> before portraying a member of a film audience in "[[Hollywood A.D.]]", a later episode of the same series.{{sfn|Shapiro|2000|p=241}} Carter also made a brief appearance in "[[Three Men and a Smoking Diaper]]", an episode of ''The Lone Gunmen''.<ref>{{Cite episode |title=[[Three Men and a Smoking Diaper]] |series=The Lone Gunmen |serieslink=The Lone Gunmen (TV series) |season =1 |number=5 |credits=[[Brian Spicer]] (director); Chris Carter (writer) |airdate=March 23, 2001 |network=[[Fox Broadcasting Company|Fox]]}}</ref>

In 1999, Carter began adapting the comic book series ''Harsh Realm'' as a television show, also titled ''[[Harsh Realm]]''. Carter's friend and frequent collaborator [[Daniel Sackheim]] had optioned the comics for adaptation in 1996. However, when the series first aired on October 8, 1999, the comics' writers Andrew Paquette and James Hudnall were given no writing credits for the work; the two then filed suit against Fox to be credited for their work.<ref name="fight">{{cite web |url=http://www.highbeam.com/doc/1G1-127720071.html |title=Comic Book Geeks Fight Chris Carter Over Harsh Realm |first1=Jim |last1=Rutenberg |first2=Peter |last2=Bogdanovich |date=October 18, 1999 |work=[[The New York Observer]] |accessdate=July 20, 2012}} {{subscription required}}</ref> ''Harsh Realm'' received disappointing viewing figures,<ref name="fight"/> and was cancelled after only three episodes had been broadcast.<ref>{{cite web |url=http://www.highbeam.com/doc/1G1-118542343.html |title=Carter series in Fox realm |first=Joseph |last=Adalian |date=June 7, 2004  |work=[[Variety (magazine)|Variety]] |accessdate=July 20, 2012}} {{subscription required}}</ref>

Two years later, Carter launched a spin-off of ''The X-Files'' titled ''[[The Lone Gunmen (TV series)|The Lone Gunmen]]'', a  series centred on three minor characters from the former series.<ref>{{cite web |url=http://www.highbeam.com/doc/1N1-0FF6780724DD894C.html |title=Conspiracy of One |work=[[Post-Tribune]] |first=Frazier |last=Moore |date=March 16, 2001 |accessdate=July 20, 2012}} {{subscription required}}</ref> ''The Lone Gunmen'' was cancelled after thirteen episodes, later receiving a coda in the form of a crossover episode with ''The X-Files''.{{sfn|Fraga|2010|pp=218–219}} Carter has since been involved with writing and directing the as-yet unreleased film ''Fencewalker'', set to feature [[Natalie Dormer]] and [[Katie Cassidy]].<ref>{{cite web |url=http://www.ew.com/ew/article/0,,20208676,00.html |title=Report: Carter Helming Secret Film |publisher=''[[Entertainment Weekly]]'' |date=June 25, 2008 |accessdate=July 21, 2012}}</ref> In 2011, he began working to develop ''Unique'', a police thriller television series;<ref>{{cite web |url=http://www.hollywoodreporter.com/news/x-files-creator-chris-carter-241782 |title='X-Files' Creator Chris Carter Plots Return to TV With Police Thriller |first=Lacey |last=Rose |publisher=[[The Hollywood Reporter]] |date=September 29, 2011 |accessdate=July 21, 2012}}</ref> the project was eventually dropped before completion.<ref>{{cite web |url=http://www.digitalspy.co.uk/ustv/news/a377650/x-files-creator-chris-carters-unique-series-not-going-forward.html |title='X-Files' creator Chris Carter's 'Unique' series 'not going forward' |first=Morgan |last=Jeffrey |publisher=[[Digital Spy]] |date=April 20, 2012 |accessdate=July 21, 2012}}</ref>

Carter next began work on the [[Amazon Studios]] television series ''[[The After]]''.<ref name="after">{{cite web |url=http://www.avclub.com/article/emx-filesem-creator-chris-carter-trying-again-with-86246 |title=X-Files creator Chris Carter trying again with another new sci-fi show that sounds like a lot of other sci-fi shows |first=Sean |last=O'Neal |work=[[The A.V. Club]] |date=October 4, 2012 |accessdate=December 24, 2013}}</ref> The pilot episode was made available for viewing on February 6, 2014;<ref name="afterpilot">{{cite web |url=http://www.avclub.com/article/amazon-has-put-up-10-new-pilots-for-your-enjoyment-107569 |title=Amazon has put up 10 new pilots for your enjoyment and judgment |last=O'Neal |work=[[The A.V. Club]] |date=February 6, 2014 |accessdate=March 19, 2014}}</ref> the series was green-lit the following month, but finally cancelled on January 5, 2015, without another episode beyond the pilot being shot.<ref name="thr_Jul12"/><ref name="aftergreenlight">{{cite web|url=http://www.rollingstone.com/movies/news/amazon-greenlights-tv-shows-by-jason-schwartzman-x-files-creator-20140312 |title=Amazon Greenlights TV Shows by Jason Schwartzman, 'X-Files' Creator |first=Kory |last=Grow |work=[[Rolling Stone]] |date=March 14, 2014 |accessdate=March 19, 2014}}</ref>

==Accolades==
Carter's work has earned him several accolades over his career, including eight nominations at the [[Primetime Emmy Award]]s.<ref name="emmy">{{cite web |url=http://www.emmys.com/shows/x-files |title=The X-Files {{!}} Emmys.com |publisher=[[Academy of Television Arts & Sciences]] |accessdate=July 21, 2012}}</ref> Carter has also received award nominations for the [[Directors Guild of America Award]]s,<ref name="DGA1"/><ref name="DGA2"/><ref name="DGA3"/> the [[Edgar Award]]s,<ref name="Edgar"/> and the [[British Academy Television Awards]].<ref name="Bafta"/>

{| class="wikitable plainrowheaders" style="font-size: 95%;"
|-
! scope="col"; width=210 | Award
! scope="col"; width=50|Year
! scope="col"; width=300 | Category
! scope="col"; width=180 | Work
! scope="col"; width=70 | Result
! scope="col"; width=40 | Ref
|-
!scope="row" rowspan=8 | [[Emmy Award]]s
| rowspan=2 | [[47th Primetime Emmy Awards|1995]]
| [[Primetime Emmy Award for Outstanding Writing for a Drama Series|Outstanding Writing for a Drama Series]]
| "[[Duane Barry]]"
| {{Nom}}
| <center><ref name="emmy"/></center>
|-
| [[Primetime Emmy Award for Outstanding Drama Series|Outstanding Drama Series]]
| ''[[The X-Files]]''
| {{Nom}}
| <center><ref name="emmy"/></center>
|-
| [[48th Primetime Emmy Awards|1996]]
| [[Primetime Emmy Award for Outstanding Drama Series|Outstanding Drama Series]]
| ''[[The X-Files]]''
| {{Nom}}
| <center><ref name="emmy"/></center>
|-
| rowspan=2 | [[49th Primetime Emmy Awards|1997]]
| [[Primetime Emmy Award for Outstanding Writing for a Drama Series|Outstanding Writing for a Drama Series]]
| "[[Memento Mori (The X-Files)|Memento Mori]]"
| {{Nom}}
| <center><ref name="emmy"/></center>
|-
| [[Primetime Emmy Award for Outstanding Drama Series|Outstanding Drama Series]]
| ''[[The X-Files]]''
| {{Nom}}
| <center><ref name="emmy"/></center>
|-
| rowspan=3 | [[50th Primetime Emmy Awards|1998]]
| [[Primetime Emmy Award for Outstanding Writing for a Drama Series|Outstanding Writing for a Drama Series]]
| "[[The Post-Modern Prometheus]]"
| {{Nom}}
| <center><ref name="emmy"/></center>
|-
| [[Primetime Emmy Award for Outstanding Directing for a Drama Series|Outstanding Directing for a Drama Series]]
| "[[The Post-Modern Prometheus]]"
| {{Nom}}
| <center><ref name="emmy"/></center>
|-
| [[Primetime Emmy Award for Outstanding Drama Series|Outstanding Drama Series]]
| ''[[The X-Files]]''
| {{Nom}}
| <center><ref name="emmy"/></center>
|-
!scope="row" rowspan=3 | [[Directors Guild of America Award]]s
| 1995
| [[Directors Guild of America Award for Outstanding Directing – Drama Series|Outstanding Directing for a Drama Series]]
| "[[The List (The X-Files)|The List]]"
| {{Nom}}
| <center><ref name="DGA1">{{cite web |url=http://www.dga.org/Awards/History/1990s/1995.aspx |title=Awards / History / 1995 - 48th Annual DGA Awards |publisher=[[Directors Guild of America]] |accessdate=July 21, 2012}}</ref></center>
|-
| 1998
| [[Directors Guild of America Award for Outstanding Directing – Drama Series|Outstanding Directing for a Drama Series]]
| "[[The Post-Modern Prometheus]]"
| {{Nom}}
| <center><ref name="DGA2">{{cite web |url=http://www.dga.org/Awards/History/1990s/1998.aspx |title=Awards / History / 1998 - 51st Annual DGA Awards |publisher=[[Directors Guild of America]] |accessdate=July 21, 2012}}</ref></center>
|-
| 1999
| [[Directors Guild of America Award for Outstanding Directing – Drama Series|Outstanding Directing for a Drama Series]]
| "[[Triangle (The X-Files)|Triangle]]"
| {{Nom}}
| <center><ref name="DGA3">{{cite web |url=http://www.dga.org/Awards/History/1990s/1999.aspx |title=Awards / History / 1999 - 52nd Annual DGA Awards |publisher=[[Directors Guild of America]] |accessdate=July 21, 2012}}</ref></center>
|-
!scope="row" | [[British Academy Television Awards]]
| [[British Academy Television Awards 1999|1999]]
| Best International Programme or Series
| ''[[The X-Files]]''
| {{Nom}}
| <center><ref name="Bafta">{{cite web |url=http://www.bafta.org/awards-database.html?sq=Chris+Carter |title=Awards Database - The BAFTA Site |publisher=[[British Academy of Film and Television Arts]] |accessdate=July 21, 2012}}</ref></center>
|-
!scope="row" | [[Edgar Award]]s
| 1995
| Best International Programme or Series
| "[[The Erlenmeyer Flask]]"
| {{Nom}}
| <center><ref name="Edgar">{{cite web |url=http://www.theedgars.com/edgarsDB/index.php |title=Edgar Award Winners and Nominees Database |accessdate=March 14, 2012 |publisher=[[Mystery Writers of America]]}} Note: Database does not allow direct linking to results. Use the drop-down menus to select "Best Episode in a TV Series" with the "Winners & Nominees" field checked for results. Results can be filtered by year, using "1995" for both year fields will narrow results to the precise year.</ref></center>
|-
|}

== Filmography ==

=== Film ===

{| class="wikitable"
|-
! scope="col" | Year
! scope="col" | Film
! scope="col" | Role
|-
! scope="row" | 1986
| ''[[The B.R.A.T. Patrol]]''
| Writer
|-
! scope="row" | 1988
| ''[[Meet the Munceys]]''
| Writer
|-
! scope="row" | 1998
| ''[[The X-Files (film)|The X-Files]]''
| Writer, producer
|-
! scope="row" | 2008
| ''[[The X-Files: I Want to Believe]]''
| Writer, producer, director, actor
|-
|}

=== Television ===

{| class="wikitable sortable plainrowheaders"
|-
! scope="col" | Series
! scope="col" | As creator
! scope="col" | As director
! scope="col" | As writer
! scope="col" | As actor
|-
! scope="row" | ''[[Rags to Riches (TV series)|Rags to Riches]]''
| —
| 1 episode
| 2 episodes
| —
|-
! scope="row" | ''[[The X-Files]]''
| All episodes
| 10 episodes
| 72 episodes
| 2 episodes
|-
! scope="row" | ''[[Millennium (TV series)|Millennium]]''
| All episodes
| —
| 7 episodes
| —
|-
! scope="row" | ''[[Harsh Realm]]''
| All episodes
| —
| 4 episodes
| —
|-
! scope="row" | ''[[The Lone Gunmen (TV series)|The Lone Gunmen]]''
| All episodes
| —
| 2 episodes
| 1 episode
|-
! scope="row" | ''[[The After]]''
| All episodes
| —
| 1 episode 
| —
|-
! scope="row" | ''[[The X-Files (season 10)|The X-Files]]''
| All episodes
| 3 episodes
| 3 episodes
| — 
|-
|}

==Footnotes==
{{reflist|30em}}

===References===

*{{cite video |people=Chris Carter, [[Ken Horton]], [[Frank Spotnitz]], [[Lance Henriksen]], [[Megan Gallagher]], [[David Nutter]], [[Mark Snow]], John Peter Kousakis, Mark Freeborn, [[Robert McLachlan (cinematographer)|Robert McLachlan]], [[Chip Johannessen]] and [[Thomas J. Wright]] |date=2004 |title= Order in Chaos: Making Millennium Season One |medium= DVD |publisher=[[20th Century Fox Home Entertainment|Fox Home Entertainment]] |location=[[Millennium (season 1)|Millennium: The Complete First Season]] |ref=Chaos}}
* {{cite book | last= Duncan |first= Jody | title = The Making of ''The X-Files'' Movie | publisher = HarperPrism | year = 1998 | isbn = 0-06-107316-4 |ref=harv}}
*{{Cite book |title=X-Files Confidential |first=Ted |last=Edwards |publisher=Little, Brown and Company |year=1996 |isbn=0-316-21808-1 |ref=harv}}
*{{cite book | year=2010 | first1=Erica |last1=Fraga | title=LAX-Files: Behind the Scenes with the Los Angeles Cast and Crew|publisher=CreateSpace|isbn=9781451503418 |ref=harv}}
*{{cite book | year=1997|first1=N. E. |last1=Genge | title=Millennium: The Unofficial Companion |publisher=Century|isbn=0-7126-7833-6 |ref=harv}}
*{{Cite book |title=The X-Files Declassified |first=Frank |last=Lovece |publisher=Citadel Press |year=1996 |isbn=0-8065-1745-X  |ref=harv}}
*{{Cite book |title=The Truth is Out There: The Official Guide to the X-Files |first=Brian |last=Lowry |publisher=Harper Prism |year=1995 |isbn=0-06-105330-9 |ref=harv}}
*{{cite book | year=2000 | last=Shapiro |first= Marc | title=All Things: The Official Guide to the X-Files Volume 6|publisher=Harper Prism|isbn=0-06-107611-2 |ref=harv}}

== External links ==

{{Commons category|Chris Carter (screenwriter)}}

* {{IMDb name|id=0004810|name=Chris Carter}}
* {{EmmyTVLegends name|chris-carter|Chris Carter}}
* Joiner, James (March 24, 2015). [http://www.thedailybeast.com/articles/2015/03/24/the-x-files-creator-chris-carter-on-the-show-s-summer-return.html "'The X-Files' Creator Chris Carter on Scully and Mulder's Return"]. ''[[The Daily Beast]]''

{{Authority control}}

{{DEFAULTSORT:Carter, Chris}}
[[Category:1957 births]]
[[Category:21st-century American writers]]
[[Category:American male screenwriters]]
[[Category:American television directors]]
[[Category:American television writers]]
[[Category:California State University, Long Beach alumni]]
[[Category:Film directors from California]]
[[Category:Film producers from California]]
[[Category:Science fiction film directors]]
[[Category:Living people]]
[[Category:People from Bellflower, California]]
[[Category:Television producers from California]]
[[Category:Writers from California]]
[[Category:Showrunners]]
[[Category:Male television writers]]