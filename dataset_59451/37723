'''Berenice of Cilicia''', also known as '''Julia Berenice''' and sometimes spelled '''Bernice''' (28 AD &ndash; after 81), was a Jewish [[Client state|client queen]] of the [[Roman Empire]] during the second half of the 1st century. Berenice was a member of the [[Herodian Dynasty]] that ruled the Roman province of [[Iudaea Province|Judaea]] between 39 BC and 92 AD. She was the daughter of King [[Agrippa I|Herod Agrippa I]] and a sister of King [[Agrippa II|Herod Agrippa II]].

What little is known about her life and background comes mostly from the early historian [[Flavius Josephus]], who detailed a history of the [[Jew|Jewish people]] and wrote an account of the [[First Jewish-Roman War|Jewish Rebellion]] of 67. [[Suetonius]], [[Tacitus]], [[Dio Cassius]], [[Aurelius Victor]] and [[Juvenal]], also tell about her. She is also mentioned in the [[Acts of the Apostles]] (26:30).
However, it is for her tumultuous love life that she is primarily known from the Renaissance. Her reputation was based on the bias of the Romans to the Eastern princesses, like [[Cleopatra]] or later [[Zenobia]].
After a number of failed [[marriage]]s throughout the 40s, she spent much of the remainder of her life at the court of her brother Herod Agrippa II, amidst rumors the two were carrying on an [[incest]]uous relationship. During the [[First Jewish-Roman War]], she began a [[affair|love affair]] with the future [[Roman Emperor|emperor]] [[Titus|Titus Flavius Vespasianus]]. However, her unpopularity among the Romans compelled Titus to dismiss her on his accession as emperor in 79. When he died two years later, she disappeared from the historical record.

== Early life ==
Berenice was born in 28<ref>Josephus writes that Berenice was sixteen at the time of her father's death, which fixes her birthdate on the year 28. See Josephus, ''Ant.'' [[wikisource:The Antiquities of the Jews/Book XIX#Chapter 9|XIX.9.1]]</ref> to [[Agrippa I|Herod Agrippa]] and [[Cypros]], as granddaughter to [[Aristobulus IV]] and great-granddaughter to [[Herod the Great]]. Her elder brother was [[Agrippa II]] (b. 27), and her younger sisters were [[Mariamne (daughter of Herod Agrippa I)|Mariamne]] (b. 34) and [[Drusilla (daughter of Agrippa I)|Drusilla]] (b. 38).<ref name="josephus-antiquities-xviii-5">Josephus, ''Antiquities of the Jews'' [[wikisource:The Antiquities of the Jews/Book XVIII#Chapter 5|XVIII.5.4]]</ref><ref name="josephus-antiquities-xix-9">Josephus, ''Antiquities of the Jews'' [[wikisource:The Antiquities of the Jews/Book XIX#Chapter 9|XIX.9.1]]</ref> According to [[Josephus]], there was also a younger brother called Drusus, who died before his teens.<ref name="josephus-antiquities-xviii-5" /> Her family constituted part of what is known as the [[Herodian Dynasty]], who ruled the Judaea Province between 39 BC and 92.
[[File:Festus window.JPG|thumb|250px|Berenice depicted with her brother [[Agrippa II]] during the trial of [[Paul of Tarsus|St. Paul]]. From a [[stained glass window]] in [[St Paul's Cathedral, Melbourne]].]]
[[Josephus]] records three short-lived marriages in Berenice's life, the first which took place sometime between 41 and 43, to [[Marcus Julius Alexander]], brother of [[Tiberius Julius Alexander]] and son of [[Alexander the Alabarch]] of [[Alexandria]].<ref name="josephus-antiquities-xix-5">Josephus, ''Antiquities of the Jews'' [[wikisource:The Antiquities of the Jews/Book XIX#Chapter 5|XIX.5.1]]</ref><ref name="ilan-julia-crispina">{{cite journal | last = Ilan | first = Tal | title = Julia Crispina, Daughter of Berenicianus, a Herodian Princess in the Babatha Archive: A Case Study in Historical Identification. | journal = The Jewish Quarterly Review: New Series | volume = 82 | pages = 361–381 | issue = 3/4| year = 1992
  | doi = 10.2307/1454863 | jstor = 1454863 | publisher = University of Pennsylvania Press }}</ref> On his early death in 44, she was married to her father's brother, [[Herod of Chalcis]],<ref name="josephus-antiquities-xix-9" /> with whom she had two sons, Berenicianus and Hyrcanus.<ref name="josephus-antiquities-xx-5">Josephus, ''Antiquities of the Jews'' [[wikisource:The Antiquities of the Jews/Book XX#Chapter 5|XX.5.2]]</ref> After her husband died in 48, she lived with her brother Agrippa for several years and then married [[Polemon II of Pontus]], king of [[Cilicia]], whom she subsequently deserted.<ref name="josephus-antiquities-xx-7">Josephus, ''Antiquities of the Jews'' [[wikisource:The Antiquities of the Jews/Book XX#Chapter 7|XX.7.3]]</ref> According to Josephus, Berenice requested this marriage to dispel rumors that she and her brother were carrying on an [[incest]]uous relationship, with Polemon being persuaded to this union mostly on account of her wealth.<ref name="josephus-antiquities-xx-7" /> However the marriage did not last and she soon returned to the court of her brother. Josephus was not the only ancient writer to suggest incestuous relations between Berenice and Agrippa. [[Juvenal]], in his [[Satire VI|sixth satire]], outright claims that they were lovers.<ref>Juvenal, ''Satires'' [[wikisource: Satire 6#ref footnote21.5E|VI]]</ref> Whether this was based on truth remains unknown.<ref name="macurdy-julia-berenice">{{cite journal | last = Macurdy | first = Grace H. | title = Julia Berenice | journal = The American Journal of Philology | volume = 56  | issue = 3 | pages = 246–253 | year = 1935  | doi = 10.2307/289676 | jstor = 289676 | publisher = The Johns Hopkins University Press}}</ref> Berenice indeed spent much of her life at the court of Agrippa, and by all accounts shared almost equal power. Popular rumors may also have been fueled by the fact that Agrippa himself never married during his lifetime.<ref name="macurdy-julia-berenice" />

Like her brother, Berenice was a [[client state|client ruler]] of the parts of the Roman Empire that lie in the present-day [[Syria]]. The [[Acts of the Apostles]] records that during this time, [[Paul the Apostle]] appeared before their court at [[Caesarea Maritima|Caesarea]].<ref>King James Bible, ''Acts'' [[wikisource: Bible (King James)/Acts#Chapter 25|25]], [[wikisource: Bible (King James)/Acts#Chapter 26|26]]</ref>

== Jewish-Roman wars ==

=== Great Jewish revolt ===
[[File:First century Iudaea province.gif|thumb|right|180px|Map of 1st century [[Iudaea Province|Judaea]].]]
In 64 emperor [[Nero]] appointed [[Gessius Florus]] as [[List of Hasmonean and Herodian rulers#Roman Procurators|procurator]] of the Judaea Province. During his administration, the Jews were systematically discriminated against in favour of the Greek population of the region.<ref name="josephus-wars-ii-14">Josephus, ''The War of the Jews'' [[wikisource:The War of the Jews/Book II#Chapter 14|II.14]]</ref> Tensions quickly rose to civil unrest when Florus plundered the treasury of the [[Second Temple|Temple]] of [[Jerusalem]] under the guise of imperial [[taxes]].<ref name="josephus-wars-ii-14" /> Following riots, the instigators were arrested and [[crucifixion|crucified]] by the Romans. Appalled at the treatment of her countrymen, Berenice travelled to Jerusalem in 66 to personally petition Florus to spare the Jews, but not only did he refuse to comply with her requests, Berenice herself was nearly killed during skirmishes in the city.<ref>Josephus, ''The War of the Jews'' [[wikisource:The War of the Jews/Book II#Chapter 15|II.15.1]]</ref> Likewise a plea for assistance to the legate of [[Syria]], [[Cestius Gallus]], met with no response.<ref name="ReferenceA">Josephus, ''The War of the Jews'' [[wikisource:The War of the Jews/Book II#Chapter 16|II.16.1]]</ref>

To prevent Jewish violence from further escalating, Agrippa assembled the populace and delivered a tearful speech to the crowd in the company of his sister,<ref name="ReferenceA" /> but the Jews alienated their sympathies when the insurgents burned down their palaces.<ref>Josephus, ''The War of the Jews'' [[wikisource:The War of the Jews/Book II#Chapter 17|II.17.6]]</ref> They fled the city to [[Galilee]] where they later gave themselves up to the Romans. Meanwhile, Cestius Gallus moved into the region with the [[Legio XII Fulminata|twelfth legion]], but was unable to restore order and suffered defeat at the battle of [[Beth-Horon]], forcing the Romans to retreat from Jerusalem.<ref>Josephus, ''The War of the Jews'' [[wikisource:The War of the Jews/Book II#Chapter 19|II.19.9]]</ref>

Emperor [[Nero]] then appointed [[Vespasian]] to put down the rebellion, who landed in Judaea with [[Legio V Macedonica|fifth]] and [[Legio X Fretensis|tenth legions]] in 67.<ref>Josephus, ''The War of the Jews'' [[wikisource:The War of the Jews/Book III#Chapter 1|III.1.2]]</ref> He was later joined by his son [[Titus]] at [[Acre, Israel|Ptolemais]], who brought with him the [[Legio XV Apollinaris|fifteenth legion]].<ref name="josephus-wars-iii-4-2">Josephus, ''The War of the Jews'' [[wikisource:The War of the Jews/Book III#Chapter 4|III.4.2]]</ref> With a strength of 60,000 professional soldiers, the Romans quickly swept across Galilee and by 69 marched on Jerusalem.<ref name="josephus-wars-iii-4-2" />

=== Affair with Titus ===

It was during this time that Berenice met and fell in love with Titus, who was eleven years her junior.<ref name="tacitus-histories-ii-2">Tacitus, ''Histories'' [[s:The Histories (Tacitus)/Book 2#2|II.2]]</ref> The Herodians sided with the [[Flavian dynasty|Flavians]] during the conflict, and later in 69, the [[Year of the Four Emperors]]&mdash;when the Roman Empire saw the quick succession of the emperors [[Galba]], [[Otho]] and [[Vitellius]]&mdash;Berenice reportedly used all her wealth and influence to support Vespasian on his campaign to become emperor.<ref>Tacitus, ''Histories'' [[s:The Histories (Tacitus)/Book 2#81|II.81]]</ref> When Vespasian was declared emperor on 21 December 69, Titus was left in Judaea to finish putting down the rebellion. The war ended in 70 with the destruction of the [[Second Temple#Destruction|Second Temple]] and the [[Siege of Jerusalem (70)#Destruction of Jerusalem|sack of Jerusalem]], with approximately 1 million dead, and 97,000 taken captive by the Romans.<ref>Josephus, ''The War of the Jews'' [[wikisource:The War of the Jews/Book VI#Chapter 6|VI.6.1]], [[wikisource:The War of the Jews/Book VI#Chapter 9|VI.9.3]]</ref> Triumphant, Titus returned to Rome to assist his father in the government, while Berenice stayed behind in Judaea.

It took four years until they reunited, when she and Agrippa came to Rome in 75. The reasons for this long absence are unclear, but have been linked to possible opposition to her presence by [[Gaius Licinius Mucianus]], a political ally of emperor Vespasian who died sometime between 72 and 78.<ref name="crook-titus-berenice">{{cite journal | last = Crook | first = John A. | title = Titus and Berenice | journal = The American Journal of Philology | volume = 72 | issue = 2 | pages = 162–175 | year = 1951  | doi = 10.2307/292544 | jstor = 292544 | publisher = The Johns Hopkins University Press}}</ref> Agrippa was given the rank of [[praetor]], while Berenice resumed her relationship with Titus, living with him at the palace and reportedly acting in every respect as his wife.<ref name="dio-romanhistory-lxv-15">Cassius Dio, ''Roman History'' [http://penelope.uchicago.edu/Thayer/E/Roman/Texts/Cassius_Dio/65*.html#66-15 LXV.15]</ref> The ancient historian [[Cassius Dio]] writes that Berenice was at the height of her power during this time,<ref name="dio-romanhistory-lxv-15" /> and if it can be any indication as to how influential she was, [[Quintilian]] records an [[anecdote]] in his ''Institutio Oratoria'' where, to his astonishment, he found himself pleading a case on Berenice's behalf where she herself presided as the [[judge]].<ref>Quintilian, ''Institutio Oratoria'' [http://penelope.uchicago.edu/Thayer/E/Roman/Texts/Quintilian/Institutio_Oratoria/4A*.html#1 IV.1]</ref> The Roman populace however perceived the Eastern Queen as an intrusive outsider, and when the pair was publicly denounced by [[Cynicism (philosophy)|Cynic]]s in the theatre, Titus caved in to the pressure and sent her away.<ref name="dio-romanhistory-lxv-15" />

Upon the accession of Titus as emperor in 79, she returned to Rome, but was quickly dismissed amidst a number of popular measures of Titus to restore his reputation with the populace.<ref>Suetonius, ''The Lives of Twelve Caesars'', Life of Titus [http://penelope.uchicago.edu/Thayer/E/Roman/Texts/Suetonius/12Caesars/Titus*.html#7 7]</ref> It is possible that he intended to send for her at a more convenient time.<ref name="crook-titus-berenice" /> However, after reigning barely two years as emperor, he suddenly died on 13 September 81.<ref>Suetonius, ''The Lives of Twelve Caesars'', Life of Titus [http://penelope.uchicago.edu/Thayer/E/Roman/Texts/Suetonius/12Caesars/Titus*.html#10 10], [http://penelope.uchicago.edu/Thayer/E/Roman/Texts/Suetonius/12Caesars/Titus*.html#11 11]</ref>

It is not known what happened to Berenice after her final dismissal from Rome.<ref name="crook-titus-berenice" /> Her brother Agrippa died around 92, and with him the Herodian Dynasty came to an end.

In modern history, her aspirations as a potential empress of Rome have led to her being described as a 'miniature [[Cleopatra VII|Cleopatra]]'.<ref>{{cite book |last=Mommsen |first=Theodor |title=The History of Rome, Book V. The Establishment of the Military Monarchy |url=http://www.gutenberg.org/etext/10705 |accessdate=2007-07-30 |year=1885 |isbn=1-153-70614-8 }}</ref>

== Berenice in books ==
Berenice appears in the [[Roman Mysteries]] book series. She shows up in [[The Enemies of Jupiter]], is mentioned in [[The Assassins of Rome]] and plays a fairly prominent role in [[Lion Feuchtwanger|Lion Feuchtwanger's]] historical novel, [[Josephus]] ([[The Jewish War]]).  The book ''Agrippa's Daughter'', by [[Howard Fast]], is about Berenice. She is also in The Last Disciple Series.

== Berenice in the arts ==
From the 17th century to contemporary times, there has been a long tradition of works of art (novels, dramas, operas, etc.) devoted to Berenice and her affair with the Roman Emperor Titus.<ref>Gabriele Boccaccini, ''Portraits of Middle Judaism in Scholarship and Arts'' (Turin: Zamorani, 1992); S. Akermann, ''Le mythe de Bérénice'' (Paris, 1978); Ruth Yordan, ''Berenice'' (London, 1974)</ref> The list includes:

* ''Lettres de Bérénice à Titus'' (1642), a French novel by [[Madeleine de Scudéry]]
* ''Bérénice'' (1648–50), a French novel by [[Jean Regnauld de Segrais]]
* ''Tite'' (1660), a French drama by [[Jean Magnon]]
* ''Il Tito'' (1666), an Italian opera by [[Antonio Cesti]] (mus.) and [[Nicola Beregani]] (libr.)
* ''[[Bérénice]]'' (1670), a French drama by [[Jean Racine]]
* ''[[Tite et Bérénice]]'' (1670), a French drama by [[Pierre Corneille]]
* ''Titus and Berenice'' (1676), an English drama by [[Thomas Otway]]
* ''[[Tito e Berenice]]'' (1714), an Italian opera by [[Antonio Caldara]] (mus.) and [[Carlo Sigismondo Capace]] (libr.)
* ''Berenice'' (1725), an Italian opera by [[Giuseppe Maria Orlandini]] (mus.) and [[Benedetto Pasqualigo]] (libr.). Also set to music by [[Niccolò Vito Piccinni]] (1766)
* ''Tito e Berenice'' (1776), an Italian opera by [[Raimondo Mei]] (mus.) and Carlo Giuseppe Lanfranchi-Rossi (libr.)
* ''Tito e Berenice'' (1782), a ballet by Paolino Franchi (chor.)
* ''Tito; o, La partenza di Berenice'' (1790), a ballet by [[Domenico Maria Gaspero Angiolini]] (mus. and chor.)
* ''Tito e Berenice'' (1793), an Italian opera by [[Sebastiano Nasolini]] (mus.) and [[Giuseppe Maria Foppa]] (libr.)
* ''Tito che abbandona Berenice'' (1828), a painting by [[Giuseppe Bezzuoli]]
* ''Titus et Bérénice'' (1860), a French opera by [[Leon-Gustave-Cyprien Gastinel]] (mus.) and [[Édouard Fournier]] (libr.)
* ''Berenice'' (1890), a German novel by [[Heinrich Vollrat Schumacher]]
* ''Cross Triumphant, The'' (1898), a historical fiction novel by [[Florence Morse Kingsley]]
* ''[[Bérénice (Magnard)|Bérénice]]'' (1909), a French opera by [[Alberic Magnard]] (mus. and libr.)
* ''Titus und die Jüdin'' (1911), a German drama by [[Hans Kyser]]
* ''Lost Diaries: From the Diary of Emperor Titus'' (1913), an English novel by [[Maurice Baring]]
* ''Bérénice, l’Hérodienne'' (1919), a French drama by [[Albert du Bois]]
* ''Bérénice'' (1920), incidental music by [[Marcel Samuel-Rousseau]]
* ''Berenice'' (1922), an English drama by [[John Masefield]]
* ''Bérénice'' (1934), a French parody by [[Noel Ouden]]
* ''Berinikah'' (1945), a Hebrew drama by [[Eisig Silberschlag]] and [[Carl de Haas]]
* ''Le reine de Césarée'' (1954), a French drama by [[Robert Brasillach]]
* ''Berenice, Princess of Judea'' (1959), an English novel by [[Leon Kolb]]
* ''Mission to Claudies'' (1963), an English novel by [[Leon Kolb]]
* ''Agrippa’s Daughter'' (1964), an English novel by [[Howard Fast|Howard Melvin Fast]]
* ''La pourpre de Judée: ou, Les délices du genre humain'' (1967), a French novel by [[Maurice Clavel]]
* ''Bérénice'' (1968), a French TV-film by [[Piere-Alain Jolivet]]
* ''Tito y Berenice'' (1970), a Spanish drama by [[Rene Marques]]
* ''Bérénice'' (1983), a French TV-film by [[Raoul Ruiz]]


The love story between Berenice and Titus is also the premise of ''[[La clemenza di Tito (Caldara)|La clemenza di Tito]]'' (1734), an Italian opera with music by [[Antonio Caldara]] and a libretto by [[Pietro Metastasio]] that was later set to music by more than 40 other composers, including [[Johann Adolph Hasse]] (1735), [[Giuseppe Arena]] (1738), [[Francesco Corradini]] (1747), [[Christoph Willibald Gluck]] (1752), [[Andrea Adolfati]] (1753), [[Niccolò Jommelli]] (1753), [[Ignaz Holzbauer]] (1757), [[Vincenzo Legrezio Ciampi]] (1757), [[Gioacchino Cocchi]] (1760), [[Marcello Bernardini]] (1768), [[Andrea Bernasconi]] (1768), [[Pasquale Anfossi]] (1769), and [[Wolfgang Amadeus Mozart]] (''[[La clemenza di Tito]]'', 1791). More recently it was used as the backdrop for the [[Caroline Lawrence]] novels the ''Assassins of Rome'' and the ''Enemies of Jupiter''. [[Lindsey Davis]] mentions it, though without making it the central plot line in novels such as ''[[Saturnalia (book)|Saturnalia]]''. It is also the stimulus for the new ballet piece by Kim Brandstrup, 'Invitus Invitam' which premiered in the [[Royal Opera House]] in October 2010.{{Citation needed|date=June 2011}}

== Notes ==
{{Reflist|2}}

== References ==
*{{cite journal | last = Ilan | first = Tal | title = Julia Crispina, Daughter of Berenicianus, a Herodian Princess in the Babatha Archive: A Case Study in Historical Identification. | journal = The Jewish Quarterly Review | volume = 82 | pages = 361–381 | issue = 3/4| year = 1992
  | doi = 10.2307/1454863 | jstor = 1454863 | publisher = University of Pennsylvania Press }}
*{{cite journal | last = Macurdy | first = Grace H. | title = Julia Berenice | journal = The American Journal of Philology | volume = 56  | issue = 3 | pages = 246–253 | year = 1935  | doi = 10.2307/289676 | jstor = 289676 | publisher = The Johns Hopkins University Press}}
*{{cite journal | last = Crook | first = John A. | title = Titus and Berenice | journal = The American Journal of Philology | volume = 72 | issue = 2 | pages = 162–175 | year = 1951  | doi = 10.2307/292544 | jstor = 292544 | publisher = The Johns Hopkins University Press}}

== External links ==
{{Commons category|Berenice (daughter of Herod Agrippa I)}}

=== Primary sources ===
*[[s:The Antiquities of the Jews|Josephus, ''Antiquities of the Jews'']], English translation
*[[s:The War of the Jews/Book II|Josephus, ''The War of the Jews'', Book II]], English translation
*[[s:The Histories (Tacitus)/Book 2|Tacitus, ''Histories'', Book 2]], English translation
*[http://penelope.uchicago.edu/Thayer/E/Roman/Texts/Cassius_Dio/65*.html#15 Cassius Dio, ''Roman History'', Book 65, Chapter 15], English translation
*[http://www.bib-arch.org/bar/article.asp?PubID=BSBA&Volume=36&Issue=1&ArticleID=26 "The 'New Cleopatra' and the Jewish Tax"] Biblical Archaeology Society

=== Images ===
*[http://www.wildwinds.com/coins/greece/judaea/berenice/i.html Coinage of Berenice at Wildwinds.com]

{{good article}}
{{New Testament people}}
{{Authority control}}

{{DEFAULTSORT:Berenice}}
[[Category:1st-century Romans]]
[[Category:Flavian dynasty]]
[[Category:Roman-era Jews]]
[[Category:Herodian dynasty]]
[[Category:28 births]]
[[Category:Year of death unknown]]
[[Category:Judean people]]
[[Category:Julii|Berenice]]
[[Category:Women in the New Testament]]