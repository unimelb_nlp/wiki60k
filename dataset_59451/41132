{{Orphan|date=August 2013}}

{{Infobox artist
| bgcolour      = #6495ED
| name          = Malcah Zeldis
| image         = 
| imagesize     = 
| caption       = 
| birth_name    = Mildred Brightman
| birth_date    = {{birth date and age|1931|9|22|}}
| birth_place   = [[Bronx, New York]]
| death_date    =
| death_place   = 
| nationality   = American
| field         = [[Painting]]
| training      = 
| movement      = [[Folk Art]]
| works         = 
| patrons       = 
| influenced by =
| influenced    =
| awards        = 
}}

'''Malcah Zeldis''' (born '''Mildred Brightman'''; September 22, 1931) is a [[Jewish]] American [[Folk painting|folk painter]].<ref name=niemann>{{Cite thesis|type=PhD|title=Malcah Zeldis: Her life and evolution of her work, 1959-1984|author= |last=Niemann |first=Henry Paul|year=1991 |publisher=[[New York University]] |place=New York|oclc=26615137}}</ref>  She is known for work that draws from a mix of biblical, historical, and autobiographical themes.

== Life and career ==

=== Early life ===
Malcah Zeldis was born in the Bronx, New York, and raised in a [[Jewish ghetto]] in [[Detroit]], [[Michigan]]. Her father faced work discrimination for his religion and the family was poor, but eventually moved to a middle-class neighborhood.<ref name=niemann /> However, Zeldis looks back on her years in Detroit fondly, stating that what she remembers best is the nature. She also remembers weekend visits to the [[Detroit Institute of Arts]] where she recalls being taken by brightly colored [[Flemish painting]]s full of small figures.<ref name=clarn>{{cite journal|last=Niemann|first=Henry|title=Malcah Zeldis: Her Art|journal=The Clarion|year=Summer 1988|volume=13|issue=2|pages=49–50}}</ref><!-- Note for future reviewers:  Citations to The Clarion refer to the magazine published by the American Folk Art Museum from 1971 to 1992, not to the British weekly by the same name that has an article in WP --> These paintings would later inspire her colorful works with many small figures.<ref>{{cite book|last=Patterson|first=Tom|title=Contemporary Folk Art: Treasures from the Smithsonian American Art Museum|year=2001|publisher=Watson-Guptill Publications|page=108}}</ref>

=== Israel ===
As a non-devout Jew, Zeldis felt disconnected from her people and wanted to explore her [[Cultural heritage|heritage]]. She moved to [[Israel]] in 1949 at the age of eighteen, becoming a [[Zionist]] and working on a [[kibbutz]].<ref name=weissman>{{cite journal|last=Weissman|first=Julia|title=Malcah Zeldis: A Jewish Folk Artist in the American Tradition|journal=The National Jewish Monthly|date=September 1975}}</ref>  It was here that Zeldis met her future husband, Hiram Zeldis.<ref>{{cite web|title=Malcah Zeldis|url=https://www.jewishvirtuallibrary.org/jsource/biography/Zeldis.html|publisher=Jewish Virtual Library}}</ref> The two went back to the US to marry, and then returned to the kibbutz. Zeldis began painting, yet had little confidence in the quality of her work. However, Aaron Giladi, a well known Israeli artist visited the kibbutz and praised Zeldis’s paintings. Zeldis was overwhelmed by his regard and his request for two paintings, saying, "I lost my voice from excitement—I couldn’t go to his lecture I was so emotionally upset. I heard afterwards that he said I was a great artist".<ref name=weissman />  Giladi’s compliments came with [[constructive criticism]]; his suggestion to paint larger intimidated Zeldis. After trying and failing to use larger canvases she stopped painting for a period of time, which was extended by childbirth and a permanent move to [[Brooklyn]], New York.<ref name=weissman />

=== Brooklyn ===
Zeldis finally resumed painting twenty-three years later, as her children grew older and her marriage ended. She enrolled in [[Brooklyn College]] as an [[Early childhood education|Early Childhood Studies]] major in 1970. The college had a "life experience" policy, which prompted Zeldis to submit her paintings despite continued apprehension over whether they were good enough.<ref name=weissman /> Much to her surprise, Zeldis's paintings were well received and her teacher introduced her work to an [[art critic]], who further suggested showing her work to dealers. This period was a turning point for Zeldis, as she realized that her lack of training was not a barrier to the art world. It was around this time that she observed Haitian [[folk art]] in a gallery. She found Haitian folk art very stylistically similar to her own, and finally believed that she was an artist.<ref name=clarn /> Zeldis began painting seriously and had a number of gallery shows. Her work also appeared in books such as the ''International Dictionary of Naive Art'' and ''Moments in Jewish Life: The Folk Art of Malcah Zeldis''.<ref name=weissman /><ref name=blustain>{{cite news|last=Blustain|first=Sarah|title=Daughter Reignites Her Mother's Painting Muse After 23-Year Hiatus|newspaper=[[The Forward]]|date=August 7, 1998}}</ref> Zeldis later illustrated a number of children's books in collaboration with her daughter, Yona Zeldis.<ref>{{cite news|last=Waisman|first=Scott|title=Every Picture Tells a Different Story|newspaper=[[The Forward]]|date=February 7, 1997}}</ref>

== Painting style ==
Zeldis's paintings are generally flat, lacking [[Body proportions|proportion]], and brightly colored with busy [[compositions]] featuring crisply defined figures. Critics{{who|date=July 2013}} describe her colors as being explosive.<ref name=hattigan>{{cite book|last=Hartigan|first=Lynda Roscoe|title=Made with Passion|year=1990|publisher=Smithsonian American Art Museum}}</ref>   Zeldis's works include everyday objects that ground the viewers in reality, yet include surprising subjects such as presidents, leading ladies, and biblical characters.<ref>{{cite journal|last=Rosenberg|first=Willa S.|title=Malcah Zeldis: Her Life|journal=The Clarion|year=Summer 1988|volume=13|issue=2|page=50}}</ref> Her whimsical images contain a number of storytelling devices and attempt to convey a narrative.<ref>{{cite journal|last=White|first=John Howell|author2=Kristin G. Congdon|title=Travel, Boundaries, and the Movement of Culture(s): Explanations for the Folk/Fine Art Quandary|journal=Art Education|date=May 1998|volume=51|issue=3|page=24}}</ref> After recovering from cancer in 1986, Zeldis was too weak to lift the [[masonite]] boards she regularly used, and instead painted on [[corrugated cardboard]] found in the street.<ref name=hattigan />

== Children's book illustration ==
Malcah Zeldis collaborated with her daughter, Yona Zeldis, to write and illustrate a number of [[children's books]]. Yona says, "The collaboration I really owe to her. She really wanted to do a children's book with me, and I said, 'No no no no no, I can't do that,' and she said, 'Yes you can.' She persisted and in effect brought me a contract." <ref name=blustain /> Zeldis and her daughter have written and illustrated ''Eve and Her Sisters: Women of the Old Testament'', ''God Sent a Rainbow and Other Bible Stories'', ''Anne Frank'', ''Sisters in Strength: American Women Who Made a Difference'', and ''Hammerin' Hank''.<ref name=blustain /><ref>{{cite journal|last=Johnson|first=Nancy J.|author2=Cyndi Giorgis|title=Children's Books: Memory, Memoir, Story|journal=The Reading Teacher|date=November 2000|volume=54|issue=3|page=342}}</ref>  Malcah Zeldis has also illustrated ''Honest Abe'' and ''Martin Luther King''.<ref>{{cite journal|last=Kaywell|first=Joan F.|author2=Kathleen Oropallo|title=Young Adult Literature: Modernizing the Study of History Using Young Adult Literature|journal=The English Journal|date=January 1998|volume=87|issue=1|page=105}}</ref>

== Selected Works ==
*[http://americanart.si.edu/images/1988/1988.74.14_1a.jpg ''Miss Liberty Celebration''], 1987, oil on corrugated cardboard, 54 1/2 x 36 1/2 in. (138.4 x 92.7&nbsp;cm), [[Smithsonian American Art Museum]], Gift of Herbert Waide Hemphill, Jr.<ref>{{cite web|title=Miss Liberty Celebration|url=http://americanart.si.edu/collections/search/artwork/?id=15517|publisher=Smithsonian American Art Museum}}</ref>
*[http://www.folkartmuseum.org/sites/folk/images/folk_4914_image.jpg ''Nude on a Couch''], 1973, oil on masonite, {{convert|39.5|x|57.5|in|cm}}, [[American Folk Art Museum]], gift of Marilyn Grais.<ref>{{cite web|title=Nude on a couch|url=http://www.folkartmuseum.org/?p=folk&t=images&id=4914|publisher=American Fold Art Museum|accessdate=20 July 2013}}</ref>
*[http://americanart.si.edu/images/1986/1986.65.159_1a.jpg ''Pieta''], 1973, oil on fiberboard, 26 x 22 in. (66.0 x 55.9&nbsp;cm.), Smithsonian American Art Museum, Gift of Herbert Waide Hemphill, Jr. and museum purchase made possible by Ralph Cross Johnson<ref>{{cite web|title=Pieta|url=http://americanart.si.edu/collections/search/artwork/?id=28367|publisher=Smithsonian American Art Museum}}</ref>
*[http://americanart.si.edu/images/1992/1992.37.11_1a.jpg ''Wake''], 1974, oil on panel, 23 1/2 x 31 7/8 in. (59.7 x 81.0&nbsp;cm.), Smithsonian American Art Museum, Gift of David L. Davies<ref>{{cite web|title=Wake|url=http://americanart.si.edu/collections/search/artwork/?id=32845|publisher=Smithsonian American Art Museum}}</ref>
*[http://americanart.si.edu/images/1998/1998.84.66_1a.jpg ''Miss America Beauty Pageant''], 1973, oil on masonite, 48 x 40 in. (121.9 x 101.6&nbsp;cm.), Smithsonian American Art Museum, Gift of Herbert Waide Hemphill, Jr.<ref>{{cite web|title=Miss America Beauty Pageant|url=http://americanart.si.edu/collections/search/artwork/?id=36568|publisher=Smithsonian American Art Museum}}</ref>
*[http://www.folkartmuseum.org/sites/folk/images/folk_4910_image.jpg ''In Shul], 1986, Oil on Masonite, {{convert|30.5|x|25.25|in|cm}}, [[American Folk Art Museum]], gift of the artist, dedicated to the memory of her father, Morris Brightman.<ref>{{cite web|title=In Shul|url=http://www.folkartmuseum.org/?p=folk&t=images&id=4910|publisher=American Folk Art Museum|accessdate=20 July 2013}}</ref>

== References ==
{{reflist|30em}}

== External links ==
* [http://www.folkartmuseum.org/zeldis Malcah Zeldis: New York Artist], an exhibition of Zeldis' art at the [[American Folk Art Museum]] in 2010.
*   Willa S. Rosenberg, [http://issuu.com/american_folk_art_museum/docs/clarion_13_3_sum1988 "Malcah Zeldis: Her Life"], ''The Clarion'', Summer 1988.
*  Henry Niemann, [http://issuu.com/american_folk_art_museum/docs/clarion_13_3_sum1988 "Malcah Zeldis: Her Art"], ''The Clarion'', Summer 1988.

{{DEFAULTSORT:Zeldis, Malcah}}
[[Category:American contemporary painters]]
[[Category:Living people]]
[[Category:1931 births]]
[[Category:American women painters]]
[[Category:Painters from New York]]
[[Category:Painters from Michigan]]
[[Category:People from the Bronx]]
[[Category:Artists from New York City]]
[[Category:Artists from Detroit]]
[[Category:Folk artists]]
[[Category:Brooklyn College alumni]]
[[Category:20th-century American painters]]
[[Category:21st-century American painters]]
[[Category:Cancer survivors]]
[[Category:American Jews]]
[[Category:20th-century women artists]]
[[Category:21st-century women artists]]