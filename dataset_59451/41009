{{about|a former cotton textile mill in [[Massachusetts]]|the [[Major league baseball|MLB]] player|Everett Mills}}
[[Image:The Everett Mills, Lawrence MA.jpg|thumb|right|250px|<center>The Everett Mill</center>]]
The '''Everett Mills''' are a group of buildings consisting of the Everett and Stone Mills in [[Lawrence, Massachusetts]]. A former cotton textile mill, it was the site of the start of the [[Bread and Roses strike]] in 1912 <ref>[http://brucewatsonwriter.com/bruce_watson_bread_roses.htm Bruce Watson's "Bread and Roses" - 30,000 strikers, 51 nationalities, 2 Months on Strike, Murder, Mayhem, Dynamite<!-- Bot generated title -->]</ref> and the former workplace of Robert Frost.

== The Essex Company and Lawrence Machine Shop ==

The Essex Company was founded in 1845. Before the city of Lawrence was even incorporated, the Essex Company laid plans for turning an area of farmland on the Merrimack River into the country’s first planned industrial city. The Essex Company built the Stone Mill, then known as the Lawrence Machine Shop from 1846-1848. It is one of the two oldest mill buildings in the city of Lawrence and one of the few mills ever built out of stone.<ref>[http://www.historic-structures.com/ma/lawrence/lawrence_machine.php Lawrence Machine Shop (Everett Mills), Lawrence Massachusetts<!-- Bot generated title -->]</ref> It originally housed a forge and a foundry and its purpose was to manufacture and repair the machinery that would be used by the new mill buildings. In the loft of the building, an experimental full set of worsted wool machinery was put together and tested – worsted wool would become one of the signature products of the Lawrence manufacturing industry.

In 1860, the Machine Shop was sold to the Everett Mills which was made up of several industrial buildings (including the precursor to the current Everett Mill building) surrounding the Machine Shop and was converted into a cotton mill. Later the building manufactured locomotives and fire engines.

== The Mill ==

Construction on the current Everett Mill main building commenced in 1909 and lasted for one year. When completed, it was the largest cotton mill under one roof in the world. With this addition the company employed 2,100 people (over half of whom were female), operated 4,728 looms and produced 1.2 million yards of cotton cloth every week. This was almost double what the mill had produced just the year before.<ref>Dorgan, Maurice B. (1918), ''Lawrence Yesterday and Today''</ref> They were nationally known for their high-quality ginghams, denims and shirtings,<ref>Lawrence Directory, for Year Ending Sep. '27</ref> referred to as “Everett Classics. ”<ref>[https://news.google.com/newspapers?id=9Z8LAAAAIBAJ&sjid=QFQDAAAAIBAJ&pg=5926,3028174&dq=everett+classics&hl=en The Evening Independent - Google News Archive Search<!-- Bot generated title -->]</ref>

In 1929, the Everett Mills was sold to the newly created Everett Mills Properties Incorporated at liquidation. The hard-won labor laws that went into effect as a result of the Bread And Roses strike made it cheaper to manufacture textiles in the unregulated Southern United States and Lawrence fell into decline.<ref>Dorgan, Maurice B. (1924), ''History of Lawrence Massachusetts''</ref> The Mill changed hands again in 1936 and was now owned by Russell T. Knight. For the next 45 years the mill building rented out parcels of real estate to smaller stores and manufacturing concerns including the Marum Knitting Hosiery Factory in 1938 and Cardinal Shoes in 1962 - which made 6-7,000 pairs of shoes daily. Both companies eventually outgrew their spaces and moved into their own facilities.<ref>''Everett Mills 26 Firms Fill Building'' Lawrence Eagle Tribune 1985</ref>

In 1981 the Everett and Stone Mills were purchased by Bertram Paley,<ref>''City's Everett Mills sold; may house high-tech firms'' Lawrence Eagle Tribune, July 24, 1981</ref> a native of nearby Lowell, also famous for its textile manufacturing industry. Shortly after the purchase it was revealed that Bert’s mother had briefly worked in the Everett Mill in the very early 1920s as a newly arrived immigrant named Frances Gidansky. Management of the Mill has stayed in the Paley family ever since.<ref>http://www.eagletribune.com/local/x1876448293/Bert-Paleys-daughter-Marianne-will-take-over-for-her-father-at-Everett-Mill</ref> For nearly 30 years the Paley family has shown a commitment to the revitalization of Lawrence.<ref>http://www.eagletribune.com/local/x1876448347/Mill-owner-Paley-called-Lawrence-development-pioneer</ref>

The Everett Mills currently houses social advocacy groups, health care services, light manufacturing, warehousing, high tech firms and a cafe. It is 700,000 square feet.<ref>[http://www.everettmills.com/wp/about-the-mill About The Mill | Everett Mills<!-- Bot generated title -->]</ref>

== Bread and Roses Strike ==
{{main|1912 Lawrence Textile Strike}}
In the early 1900 s the Everett Mills, like all the other Lawrence mills were staffed mainly with European immigrants who were underpaid and overworked. When a law was passed in Massachusetts reducing the maximum number of hours that could be worked weekly by women and children, the mill owners reduced their workers’ pay accordingly and the [[Lawrence textile strike|Bread and Roses]] strike of 1912 erupted. Believing that a workforce of women and ethnically self-segregated immigrants living paycheck to paycheck could never band together to achieve a common goal, the mill owners were proved wrong with the strike that shut down the mills for two months in the dead of winter. Originating with female Polish weavers at the Everett Mills who revolted upon receiving pay reduced by thirty-two cents, soon 20,000 millworkers were on strike. Helped by the [[Industrial Workers of the World]] labor union who organized meetings with representatives from all affected ethnic communities, fundraisers to support the millworkers while they were on strike, and highly visible protests meant to arouse nationwide support and outrage at national labor norms, the workers won pay increases and time-and-a-quarter for overtime work. The strike had far reaching consequences across the country when mill owners in other cities and states voluntarily raised pay in fear of similar uprisings by their own workers.

== Trivia ==

In 1882, poet [[Robert Frost]], a Lawrence resident, spent some time working as an office clerk at the Everett Mills. He likely wrote his poem “A Lone Striker” about his experience here.<ref>Faggen, Robert "The Cambridge companion to Robert Frost" 2001</ref>

Interviews for the 2011 [[Public Broadcasting Service|PBS]] [[American Experience]] documentary "Triangle Fire"<ref>http://www.pbs.org/wgbh/americanexperience/films/triangle/</ref> were shot on the sixth floor of the Everett Mills.

== References ==
{{Reflist}}

== External links ==
{{Commonscat|Everett Mills}}
* [http://robertfrostoutloud.com/ALoneStriker.html A lone striker]
* [http://www.historic-structures.com/ma/lawrence/lawrence_machine.php/ historic-structures.com]
* [http://brucewatsonwriter.com/bruce_watson_bread_roses.htm/ Bruce Watson]
* [https://news.google.com/newspapers?id=9Z8LAAAAIBAJ&sjid=QFQDAAAAIBAJ&pg=5926,3028174&dq=everett+classics&hl=en/ Everett Classics]
* [http://www.eagletribune.com/local/x1876448293/Bert-Paleys-daughter-Marianne-will-take-over-for-her-father-at-Everett-Mill/ Eagle Tribune]
* [http://www.eagletribune.com/local/x1876448347/Mill-owner-Paley-called-Lawrence-development-pioneer/ Eagle Tribune]

{{coord|42|42|26.1|N|71|9|10.8|W|region:US|display=title}}

[[Category:Buildings and structures in Lawrence, Massachusetts]]
[[Category:Cotton mills in the United States]]