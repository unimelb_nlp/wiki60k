{{Infobox book 
| name          = Christowell
| image         = Christowell by R D Blackmore - 1891 book cover.jpg
| caption       = Cover of the 1891 edition
| author        = [[R. D. Blackmore]]
| country       = United Kingdom
| language      = English
| genre         = 
| publisher     = 
| pub_date      = 1882
| pages         = 
| isbn          = 
}}
'''''Christowell: a Dartmoor tale''''' is a three-volume novel by [[R. D. Blackmore]] published in 1882. It is set in the fictional village of Christowell on the eastern edge of [[Dartmoor]].

==Title==
The title derives from the village of [[Christow]] on Dartmoor. Although Blackmore was keen to point out that "Christow is not my Christowell: though I took the name partly from it ... my Christowell is a compound of several places."<ref name="dunn225">Waldo Hilary Dunn, (1956), ''R. D. Blackmore : the author of Lorna Doone, a biography'', page 225</ref>

==Plot introduction==
The complex and picturesque life which goes on in the parish of Christowell is the theme of the novel.<ref name="literary">''The Literary World'', (1881), Volume 12, page 452</ref> The story begins with the garden where resides “Captain Larks,” alias Mr. Arthur, who is neither Mr. Arthur nor "Captain Larks,"<ref name="blackwood">''Blackwood's Magazine'', (1882), Volume 131, page 390</ref> but a mysterious soldier who renounced his own good name to save one who was his brother and fellow officer from disgrace.<ref name="oxford">''The Oxford Magazine'', (1883), Volume 1, page 184</ref> Misfortune has driven him into retirement, and so he lives among his flowers and fruit.<ref name="blackwood"/> Nobody knows anything about him, save the clergyman, Parson Short.<ref name="blackwood"/> Mr. Arthur has a daughter, Rose, who, after visiting him as a child during her holidays for several years, at last comes to live with him at his cottage.<ref name="blackwood"/> It is when she appears, however, that her father's troubles may be said to begin; for she falls in love with Jack Westcombe the son of a retired officer, whom Rose’s father declines to see,<ref name="blackwood"/> conscious of the cloud that rests on himself.<ref name="oxford"/>

Among other characters there are Pugsley the carrier, Sir Joseph Touchwood, who has made a fortune out of shoes supplied by contract to Lord Wellington's army, Julia Touchwood, and a Richard ("Dicky") Touchwood who achieves small honors at Cambridge, but greater ones at home as a rat-catcher.<ref name="literary"/> The villain of the plot is a Mr. Gaston who attempts every crime from murder to bribery to compass his ends, and succeeds in hoodwinking every one for some time and keeping Mr. Arthur out of his lawful inheritance.<ref name="oxford"/>

==Publication==
''Christowell'' was serialized in ''[[Good Words]]'' from January to December 1881.<ref name="cambio">"Richard Doddridge Blackmore" entry in ''The Cambridge Bibliography of English Literature: 1800-1900'', (1999), Cambridge University Press. ISBN 0521391008</ref> It was then published as a [[three-volume novel]] in 1882.<ref name="cambio"/>

==Reception==
The novel received fairly good reviews. ''[[The Oxford Magazine]]'' stated that the novel was "nearly equal" to his others, but mentioned the "weakness [which] lies in the artistic treatment of the details of the plot."<ref name="oxford"/> ''[[The Academy (periodical)|The Academy]]'' complained that "Blackmore's characters are too consistently clever", but netheless opined that "it is a book to be enjoyed leisurely".<ref name="academy">''The Academy'', (1881), Volume 20, page 451</ref> ''[[Blackwood's Magazine]]'' wrote that "he is never more entertaining than at this homely level, on page after page, which in other books we should skip, but which here we enjoy as we should a walk in the company of the most genial and gentle of humorists."<ref name="blackwood"/>

==References==
{{reflist}}

==External links==
*[http://gutenberg.net.au/ebooks10/1000711h.html Christowell], at Project Gutenberg Australia

{{R. D. Blackmore}}

[[Category:1882 novels]]
[[Category:Novels by Richard Doddridge Blackmore]]
[[Category:Novels set in Devon]]