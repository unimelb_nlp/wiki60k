{{other places|Corniche}}
[[File:Abu Dhabi Corniche.jpg|thumb|The Corniche in [[Abu Dhabi]] in the 2000s.]]
[[File:Abu Dhabi Skyline fron Corniche Rd.JPG|thumb|The Abu Dhabi skyline from the Corniche.]]
[[File:The Corniche as seen from the Marina Mall, Abu Dhabi, UAE.JPG|thumb|The Corniche as seen from the Marina Mall]]

'''The Corniche''' (or '''Corniche Road''') is located in the city of [[Abu Dhabi]], the capital of the [[United Arab Emirates]].<ref>{{cite web|url=http://visitabudhabi.ae/en/what.to.see/attractions/abu.dhabi.corniche.aspx |title=Abu Dhabi Corniche |publisher=[http://visitabudhabi.ae/ Visit Abu Dhabi] |location=UAE |accessdate=24 November 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20131202222330/http://visitabudhabi.ae/en/what.to.see/attractions/abu.dhabi.corniche.aspx |archivedate=2 December 2013 |df= }}</ref> It forms a sweeping curve on the western side of the main Abu Dhabi island and is replete with cycle paths, fountains and park areas. Between 2002 and 2003, land was reclaimed from the sea and the Corniche was extended. Some of the earlier landmarks were demolished in the process. Certain parts of the Corniche have significant deposition of sand, with people using the area as a public beach. Prior to the 1970s, the current area occupied by the Corniche was a beach, where dhows and ships used to anchor and transfer cargo or people; at the time, the [[Mina Zayed]] area was not yet constructed.

[[Marina Mall]] is located across from the Corniche and can be accessed using a narrow breakwater road. At Marina Mall, the UAE Flag is hoisted and holds the record for being one of the tallest Flag poles in the world.

[[File:Lulu Island (Nepenthes).jpg|thumb|right| Lulu Island from the Corniche]]

[[Lulu Island (Abu Dhabi)|Lulu Island]] is a tiny reclaimed island located about a kilometer from the corniche and the [[Emirates Palace Hotel]] is at the southern end. There are a number of skyscrapers at the northern end, with newer taller skyscrapers being built on the southern end.

==See also==
* [[Corniche]]
* [[Mina Zayed]]

==References==
{{reflist}}

{{coord missing|United Arab Emirates}}

{{UnitedArabEmirates-geo-stub}}

[[Category:Roads in the United Arab Emirates]]
[[Category:Geography of Abu Dhabi]]
[[Category:Tourist attractions in Abu Dhabi]]