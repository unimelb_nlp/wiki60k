{{Infobox journal
| title = Journal of Earth System Science
| formernames = Proceedings of the Indian Academy of Sciences (Earth  and Planetary Sciences)
| cover =
| abbreviation = J. Earth. Syst. Sci.
| editor = D. Shankar
| discipline = [[Earth system science]]
| publisher = [[Springer Science+Business Media]] on behalf of the [[Indian Academy of Sciences]]
| country = India
| frequency = Bimonthly
| history = 1978-present
| impact = 0.695
| impact-year = 2012
| website = http://www.springer.com/earth+sciences+and+geography/journal/12040
| link2 = http://link.springer.com/journal/volumesAndIssues/12040
| link2-name = Online archive
| ISSN = 0253-4126
| eISSN = 0973-774X
}}
The '''''Journal of Earth System Science''''' is a bimonthly [[Peer review|peer-viewed]] [[scientific journal]] that covers [[research]] on [[Earth system science]]. It was established in 1978 and is published by [[Springer Science+Business Media]] on behalf of the [[Indian Academy of Sciences]]. The [[editor-in-chief]] is D. Shankar ([[National Institute of Oceanography, India|National Institute of Oceanography]]).

== History ==
The ''Journal of Earth System Science'' was originally part of the ''Proceedings of the Indian Academy of Sciences''. This journal was established in 1934, but in 1978 it was split into three different journals: ''Journal of Earth System Science, [[Journal of Chemical Sciences]]'' and ''[[Proceedings - Mathematical Sciences|Proceedings of the Indian Academy of Sciences - Mathematical Sciences]]'', all of them continuing as "volume 87".

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Science Citation Index Expanded]]
* [[Scopus]]
* [[Astrophysics Data System]]
* [[Chemical Abstracts Service]]
* [[EBSCO Publishing|EBSCO databases]]
* [[CAB International]]
* [[Academic OneFile]]
* [[CAB Abstracts]]
* [[Current Contents]]/Physical, Chemical and Earth Sciences
* [[Environment Index]]
* [[GeoRef]]
* [[Global Health]]
* [[Indian Science Abstracts]]
* [[INIS Atomindex]]
* [[PASCAL (database)|PASCAL]]
* [[Petroleum Abstracts]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 0.695.<ref name=WoS>{{cite book |year=2013 |chapter=Journal of Earth System Science|title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
<references/>

== External links ==
* {{Official website|http://www.springer.com/earth+sciences+and+geography/journal/12040}}

[[Category:Springer Science+Business Media academic journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1978]]
[[Category:English-language journals]]
[[Category:Earth and atmospheric sciences journals]]
[[Category:Earth system sciences]]