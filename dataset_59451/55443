{{Infobox Organization
|name         =American Forensic Association
|image        = American Forensic Association (logo).jpg
|image_border = 
|size         =
|caption      = 
|map          = 
|msize        = 
|mcaption     = 
|abbreviation = AFA
|motto        = 
|formation    = December 10, 1949
|extinction   = 
|type         = Public Speaking
|status       = 
|purpose      = To seek greater understanding of the history and practice of reasoned discourse as a sound basis for public involvement.
|headquarters = 
|location     = [[University of Wisconsin]]<br>[[River Falls, WI]], United States
|region_served = United States
|membership   = 
|language     = English
|leader_title = President
|leader_name  =Daniel Cronn-Mills
|main_organ   = 
|parent_organization =
|affiliations = 
|num_staff    = 
|num_volunteers =
|budget       = 
|website      = http://www.americanforensics.org/
|remarks      =
}}
The '''American Forensic Association''' was founded in 1949 in Chicago. It took over the [[National Debate Tournament]] from the [[United States Military Academy]] in 1966 and runs the [[American Forensic Association National Individual Events Tournament]]. It is a national organization designed to promote excellence in [[public speaking]], individual events, and [[debate]].

==History==
In December 1949, the Association of Directors of Speech Activities, a group of instructors attending the annual meeting of the Speech Association of America, met in Chicago. These people became the Founders of the American Forensic Association. Their purpose was to have an organization to promote the interests of Directors of forensics programs and to encourage the development of forensics participation.
Perhaps the best illustration of the early efforts was the annual meeting of 1950. The North Central Accrediting Association had proposed that extra-curricular speech activities be eliminated from high schools. The association successfully countered this effort.
Another characteristic of forensic educators was illustrated in 1953. Selected for debate during that year was the topic: "Resolved: that the United States should recognize Communist China." Directors of forensic programs were summoned to testify before Congressional Committees and students at the service academies were prohibited from debating the topic, but debate went on.
In 1964, the Association founded The Journal of the American Forensic Association, renamed Argumentation and Advocacy: The Journal of the American Forensic Association in 1988. This journal marked an increasing emphasis on the encouragement of research by the Association. Many outstanding scholars have been editors of this journal.
In 1966, the National Debate Tournament was established. The Tournament continued the tradition of an end of the year championship tournament begun by the United States Military Academy in 1947. In 1977, the NDT was joined by the National Individual Events Tournament.
In 1979, the Association joined the Speech Communication Association in establishing the Summer Conference on Argumentation. The Conference has been an important element in encouraging the development of research in argumentation and advocacy.
Today, the Association conducts many activities in support of forensics directors and their students.

==Members==
*[[Cross Examination Debate Association]]
*[[National Parliamentary Debate Association]]
*[[American Debate Association]]
*[[International Debate Education Association]]

==Founders==

The following members of the American Forensic Association joined the Association of Directors of Speech Activities, which was the immediate forerunner of the AFA, prior to December 10, 1949. Meeting at three meetings at the annual convention of the Speech Association of America, the group founded the American Forensic Association and drafted its first Constitution. Those meetings occurred at the Stevens Hotel (now the Chicago Hilton and Towers) on South Michigan Street in Chicago, Illinois, in December 1949.<ref>http://www.americanforensics.org/about/founders/founders</ref>

* Otis J. Aggertt
* H. L. Ahrendts, Kearney State College, NE
* J. Jeffrey Auer, Oberlin College, OH
* Charles T. Battin, College of Puget Sound, Tacoma, WA
* Raymond Beard
* Paul F. Blackburn, Loyola University, Los Angeles, CA
* Gifford Blyton, University of Kentucky, Lexington
* Robert S. Bradley, S.J., Gonzaga University, Spokane, WA
* Richard C. Bland, Stetson University, DeLand, FL
* Paul Brandes
* Winston L. Brembeck, University of Wisconsin, Madison.
* Adeline Brockshire, Fairmount High School, Fairmount IN
* E. C. Buehler, University of Kansas, Lawrence
* Hulda Burdick, Muskegon High School, WI
* W. Arthur Cable, University of Arizona, Tucson
* Glenn R. Capp, Baylor University, Waco, TX
* Hugo A. Carlson, Augustana College, Sioux Falls, SD
* Paul Carmack, Ohio State University, Columbus
* Eugene C. Chenoweth, Indiana University, Bloomington
* Brother C. Cornelius, FSC, Christian Brothers Academy, Syracuse, NY
* Lionel Crocker, Denison University, Granville, OH
* H. Barrett Davis, Lehigh University, Bethlehem, PA
* Lt. Lawrence Dean, Valley Forge Military Academy, Wayne, PA
* Elnora Drafahl, New York State Teachers College, Albany
* Wayne Eubank
* William W. Evans, United States Naval Academy, Annapolis, MD
* Thorrel B. Fest, University of Colorado, Boulder
* Austin J. Freeley, Boston University, MA
* J. V. Garland
* Hal W. Goetsch, Blair High School, WI
* Ellen H. Gould, Alabama College for Women, Montevallo
* Rev. R. J. Grennan, Champion High School, Prairie du Chien, WI
* Annabel Hagood, University of Alabama, Tuscaloosa
* Hugo Hellman, Marquette University, Milwaukee, WI
* George F. Henigan, George Washington University, Washington, DC
* George Holm, Kent State College, OH
* Ben W. Hope
* James N. Irby, Jr., Spring Hill College, AL
* Bruno E. Jacob, Ripon College, WI
* Lt. Col. Chester L. Johnson, United States Military Academy, West Point, NY
* Carroll P. Lahman, Pasadena College, CA
* Chris R. Layton
* Paul E. Lull
* Ralph Y. McGinnis
* James McMonis
* Robert C. Martin, Lake Forest College, IL
* Charles Masten, Kansas State Teacher's College, Emporia
* J. Edmond Mayer, Alhambra High School, CA
* Ralph Micken, Illinois State Normal College, Normal
* Edd Miller
* Glen Mills, Northwestern University, Evanston, IL
* Eugene C. Moulton, Carroll College, Waukesha, WI
* William G. Nagel, Washington High School, Massilon, OH
* Lloyd R. Newcomer, Whitman College, Walla Walla, WA
* Alan Nichols, University of Southern California, Los Angeles
* Egbert Ray Nichols, University of Redlands, CA
* E. Ray Nichols, Jr., University of Oregon, Eugene
* Lawrence. E. Norton, Bradley University, Peoria, IL
* Clarence L. Nystrom, Wheaton College, IL
* Karsten J. Ohnstad, Ripon College, WI
* Guy Eugene Oliver
* Donald Olson, University of Nebraska, Lincoln
* David Potter, University of Akron, OH
* Samuel Prichard, University of Maine, Orono
* W. Charles Redding, University of Southern California, Los Angeles
* Maude Ross, Miami High School, OK
* A. Westley Rowland, Alma College, Alma MI
* Ralph N. Schmidt, Utica College, NY
* Ervin H. Schwierburt, College of Idaho, Caldwell
* Forrest L. Seal, DePauw University, Greencastle, IN
* Franklin Shirley
* Malcolm O. Sillars, Iowa State College, Ames
* Solomon Simonson, New York State Teacher's College, Fredonia
* George L. Sixby, Arkansas State Teacher's College, Conway
* Paul W. Smith, Pasadena City College, CA
* Leonard Sommer, University of Notre Dame, IN
* I. H. Sparling, Eureka College, IL
* C. Wharton Talley, University of Southern Illinois, Carbondale
* Gordon Thomas
* Wayne N. Thompson, Chicago Undergraduate Division, University of Illinois
* William H. Veatch, Washington State College, Pullman
* Seymour M. Vinocur, University of Washington, Seattle
* Grace Walsh, State Teacher's College, Eau Claire, WI
* Otis M. Walter, Jr., University of Houston, TX
* J. C. Wetherby, Duke University, Durham, NC
* Hudson F. Wilcox, Enid High School, O

==References==
{{Reflist}}

==External links==
*{{Official website|http://www.americanforensics.org/}}

==Publications and Research==
The American Forensics Association publishes several scholarly journals including:
* Argumentation and Advocacy <ref>http://www.argumentationandadvocacy.com/</ref>
* The Journal of the American Forensic Association <ref>http://americanforensics.org/publications.html</ref>
* The AFA Newsletter

==Collegiate Competitions==
The American Forensics Association holds two collegiate national tournaments annually.  The tournaments brings students from across the nation to compete for national championships in both individual events and debate. Students reach the tournaments through a rigorous at-large and district qualification system verified by organizational officers. Since their inception, the tournaments have served hundreds of colleges and universities and thousands of students. The tournaments include:
* National Individual Events Tournament (commonly known as AFA-NIET) <ref>https://sites.google.com/site/afanietnew2/</ref>
* National Debate Tournament (commonly known as AFA-NDT) <ref>http://groups.wfu.edu/NDT/</ref>

[[Category:Educational organizations based in the United States]]
[[Category:Student debating societies]]