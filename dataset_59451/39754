{{Infobox person
|name          = Thomas Wesley Benoist
|image         = Thomas Wesley Benoist.jpg
|image_size    = 200
|caption       = 
|birth_name    = 
|birth_date    = {{Birth date|df=yes|1874|12|29}}
|birth_place   = [[Irondale, Missouri]]
|death_date    = {{death date and age|df=yes|1917|6|14|1874|12|29}}
|death_place   = [[Sandusky, Ohio]]
|death_cause   = Streetcar accident
|resting_place = [[Hopewell Cemetery]],<br/>[[Hopewell, Washington County, Missouri]]
|resting_place_coordinates = 
|residence     = 
|nationality   = [[United States]]
|ethnicity     = 
|citizenship   = 
|other_names   = 
|known_for     = 
|education     =
|alma_mater    = 
|employer      = 
|occupation    = Aviator<br />Aircraft designer<br/>Aircraft manufacturer<br/>Airline entrepreneur
|home_town     = 
|title         = 
|salary        = 
|networth      = 
|height        = 
|weight        = 
|term          = 
|predecessor   = 
|successor     = 
|party         = 
|boards        = 
|religion      = 
|spouse        = 
|partner       = 
|children      = 
|parents       = 
|relations     = 
|signature     = 
|website       = 
|footnotes     = 
}}

'''Thomas W. Benoist ''' (29 December 1874 – 14 June 1917) was an [[United States|American]] aviator and aircraft manufacturer. In an aviation career of only ten years, he formed the world{{'}}s first aircraft parts distribution company, established one of the leading early American aircraft manufacturing companies and a successful flying school, and from January to April 1914 operated the world{{'}}s first scheduled [[airline]].<ref>{{cite book |title=Bulletin of the Missouri Historical Society, Volumes 31-32 |author=Missouri Historical Society}}</ref><ref name="Roos">[http://www.airandspacemuseum.org/BenoistPaper.htm airandspacemuseum.org Roos, Frederick W., "The Brief, Bright Aviation Career of St. Louis's Tom Benoist,"  American Institute of Aeronautics and Astronautics, Inc., 2005.]</ref>

== Biography ==

=== Early life ===
Thomas Wesley Benoist was born on 29 December 1874 in [[Irondale, Missouri|Irondale]], [[Missouri]], the son of Pierre E. Benoist and the former Anna S. Gregory.<ref name="Find-a-Grave Benoist">[http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GSln=Benoist&GSfn=Thomas&GSby=1874&GSbyrel=in&GSdy=1917&GSdyrel=in&GScntry=4&GSob=n&GRid=27522729&df=all& Find-A-Grave Memorial Thomas Wesley Benoist (1874-1917)]</ref> One of the first industrialists in [[St. Louis, Missouri|St. Louis]], Missouri, he was a successful businessman in the [[automobile]] industry by 1904.<ref name="Roos"/>

=== Louisiana Purchase Exposition ===
In 1904, Benoist was among the sponsors of an unsuccessful [[lighter-than-air]] flying machine somewhat similar to a [[helicopter]] which the [[balloonist]] [[John Berry (balloonist)|John Berry]] attempted to fly at that year{{'}}s [[Louisiana Purchase Exposition]], also known as the St. Louis World{{'}}s Fair. While attending the fair, he observed an [[observation balloon]] tethered at an altitude of 1,000 feet (305 meters) and [[Glider (aircraft)|glider]] demonstrations by [[William Avery (aviator)|William Avery]], an associate of the noted aviation pioneer [[Octave Chanute]]. Despite the failure of Berry{{'}}s aircraft, what Benoist saw at the fair piqued his interest in aviation, and he oriented his future career toward it.<ref name="Roos"/>

=== Aerosco ===
In 1907, Benoist in partnership with [[E. Percy Noel]] founded the [[Aeronautic Supply Company]], known as [[Aerosco]], the world{{'}}s first aircraft parts distributor. At first, Aerosco limited itself to dealing in raw materials and parts for use in aviation experiments, but it soon expanded to sell kits allowing customers to assemble complete airplanes, including those by leading manufacturers of the day, such as [[Blériot Aéronautique|Blériot]] [[monoplane]]s, [[Glenn Curtiss|Curtiss]] [[biplane]]s, [[Farman Aviation Works|Farman]] biplanes, and [[Wright Flyer]]s. It also sold a wide range of books on aviation topics.<ref name="Roos"/>

=== Aviator ===
[[File:Thomas W. Benoist first flight.jpg|300px|thumb|right|<center>Benoist making his first flight, in a Gill-Curtiss biplane at Kinloch Field in [[Kinloch, Missouri|Kinloch]], [[Missouri]], on 18 September 1910.</center>]]Benoist soon purchased a Curtiss-type airplane built by [[Howard Gill]] and learned to fly it, making his first flight on 18 September 1910 at the [[Kinloch Park]] Aero Club field in [[Kinloch, Missouri|Kinloch]], Missouri.<ref>{{cite book|title=Lion of the valley: St. Louis, Missouri, 1764-1980|author=James Neal Primm}}</ref><ref>{{cite book|title=AAHS journal, Volume 43|year=1998}}</ref> He gave flying exhibitions in the [[Midwestern United States|Midwestern]] and [[Southern United States|Southern]] United States, but an injury he suffered in a flying mishap during one of them prevented him from taking part in an international aviation meet in mid-October 1910.

=== Aerosco Flying School and Benoist Aircraft Company ===
In March 1911, Benoist established the Aerosco Flying School at [[Kinloch Field]], and it soon drew students from throughout the United States; it later was renamed the Benoist Flying School. At around the same time, he bought out his partner and moved the original Aerosco company to a larger facility in a suburb of St. Louis, renaming it the [[Benoist Aircraft|Benoist Aircraft Company]]. With the name change, he reoriented the company from dealing in aviation parts and kits for aircraft by other manufacturers to building airplanes of original design. As an intermediate step, he designed and manufactured a version of the Curtiss-Gill airplane he had purchased in 1910.  The flying school and manufacturing concern were both so successful that Benoist airplanes and pilots soon were appearing all over the United States.<ref name="Roos"/><ref>{{cite book|title=American flying boats and amphibious aircraft: an illustrated history|author=E. R. Johnson}}</ref><ref name="Homan">{{cite book|title=Women Who Fly|author1=Lynn M. Homan |author2=Thomas Reilly |author3=Rosalie M. Shepherd }}</ref>

On 20 October 1911, the Benoist Aircraft factory burned to the ground, destroying five complete airplanes, many tools, machinery, and all of the company{{'}}s files. Although the loss was not insured, Benoist bounced back quickly, opening a new factory nearby, bringing aviator [[Tony Jannus]] &ndash; who would soon become its chief pilot &ndash; into the company in November 1911, and designing and building the first Benoist airplane of completely original design, the [[Benoist XII Headless|Type XII Headless]], before the end of 1911.<ref name="Roos"/><ref name="Homan"/>

By 1912, Benoist Aircraft was one of the leading aircraft companies in the world.<ref>{{cite book|title=Benoist: Thomas W. Benoist, Benoist Airplane Company, Benoist students and pilots|author=Reginald D Woodcock}}</ref> The Type XII Headless made history when, piloted by Jannus, it carried [[Albert Berry (parachutist)|Albert Berry]] over Kinloch Field on 1 March 1912 and Berry made the world{{'}}s first successful [[parachute]] jump from an airplane. Improvements in the Type XII led to the development of the [[Benoist Land Tractor Type XII|Land Tractor Type XII]] later in the year, which, configured as a [[floatplane]], set a distance record for overwater flight in a journey of 1,973 miles (3,177&nbsp;km) down the [[Missouri River|Missouri]] and [[Mississippi River|Mississippi]] rivers from [[Omaha, Nebraska|Omaha]], [[Nebraska]], to [[New Orleans, Louisiana|New Orleans]], [[Louisiana]], between 6 November and 16 December 1912. Jannus performed 42 aerial exhibitions during the trip, exposing thousands of people in the central and southern United States to aviation.<ref name="Roos"/>

In December 1912, Benoist Aircraft produced its first [[flying boat]], the [[Benoist XIII Lake Cruiser|Type XIII Lake Cruiser]], which the company demonstrated widely during the summer of 1913.    A larger [[Benoist XIV|Type XIV]] flying boat soon followed.<ref name="Roos"/> 

During the [[Great Lakes Reliability Cruise]] in 1913, Benoist entered three aircraft, flown by [[Antony Jannus]], [[Hugh Robinson (aviator)|Hugh Robinson]], and Benoist himself.<ref>{{cite news|last1=Noel|first1=E. Percy|title=Three entries made in Aero and Hydro Cruise|url=https://books.google.com/books?id=psw7AQAAMAAJ&pg=PA166-IA1&lpg=PA166-IA1&dq=Great+Lakes+Reliability+Cruise&source=bl&ots=J7sGegtSP2&sig=tho8RZ9vC9QRy6cl8LsQ9YnOmZ8&hl=en&sa=X&ved=0ahUKEwi4zdrrqMXSAhVCeSYKHXyFApsQ6AEIOjAF%20-%20v=onepage&q=Great%20Lakes%20Reliability%20Cruise&f=false#v=snippet&q=Great%20Lakes%20Reliability%20Cruise&f=false|accessdate=8 March 2017|agency=Aero and Hydro|issue=Volume VI No 1|date=5 April 1913|page=3}}</ref><ref>{{cite news|last1=Noel|first1=E. Percy|title=Aero and Hydro Great Lakes Reliabilit Cruise Entries to Date|url=https://books.google.com/books?id=psw7AQAAMAAJ&pg=PA166-IA1&lpg=PA166-IA1&dq=Great+Lakes+Reliability+Cruise&source=bl&ots=J7sGegtSP2&sig=tho8RZ9vC9QRy6cl8LsQ9YnOmZ8&hl=en&sa=X&ved=0ahUKEwi4zdrrqMXSAhVCeSYKHXyFApsQ6AEIOjAF%20-%20v=onepage&q=Great%20Lakes%20Reliability%20Cruise&f=false#v=snippet&q=Great%20Lakes%20Reliability%20Cruise&f=false|accessdate=8 March 2017|agency=Aero and Hydro|issue=Volume VI No 9|date=31 May 1913|page=166}}</ref> Throughout the spring and summer, [[Aero and Hydro]] (a newsletter published by Benoist's partner E. Percy Noel) promoted the Reliability Cruise, listing Benoist aircraft as the first three entrants.

=== First scheduled airline ===
In 1913, [[Percival E. Fansler]] brought in Benoist to start an air passenger service using Benoist Aircraft{{'}}s new [[flying boat]]s to connect [[St. Petersburg, Florida|St. Petersburg]] and [[Tampa, Florida|Tampa]], [[Florida]], two cities that otherwise were a day{{'}}s travel apart at the time. Benoist signed a three-month contract to provide the service with the St. Petersburg Board of Trade on 17 December 1913, subsidizing 50% of the costs for starting the airline. Benoist initiated the service, the [[St. Petersburg-Tampa Airboat Line]], using a [[Benoist XIV]] [[flying boat]], on 1 January 1914. It was the first scheduled airline service in the world.<ref>{{cite journal|magazine=Flying|date=December 1953}}</ref> Two Benoist XIVs provided twice-daily service across [[Tampa Bay]] and by the time the initial contract expired on 31 March 1914 had transported 1,204 passengers without injury, losing only four days to mechanical problems. A decline in business led the airline to shut down in late April 1914 and sell its two flying boats.<ref name="Roos"/>

=== Transatlantic flight ambition ===

In early 1913, Benoist and Jannus initiated the development of a large new flying boat capable of [[transatlantic flight]]. When in early 1914 the ''[[Daily Mail]]'' of [[London]] offered a [[USD|$]]50,000 prize for the first transatlantic flight of under 72 hours, the two men developed the [[Benoist XV|Type XV]] flying boat, capable of remaining aloft for 40 hours with six passengers on board. It was ready to fly in 1915, but by then the outbreak of [[World War I]] in late July 1914 had made a transatlantic attempt impossible. Benoist Aircraft and the [[St. Louis Car Company]] jointly proposed the construction of 5,000 Type XVs for the [[United Kingdom]] for use on [[antisubmarine]] patrols, but the British preferred [[Curtiss Aeroplane and Motor Company|Curtiss]] flying boats and nothing came of the idea.<ref name="Roos"/>

=== Financial troubles and later designs ===

Unable to secure a large contract for its airplanes during the war, Benoist Aircraft began to experience financial problems by 1915. To reduce costs, Benoist moved the company first to [[Chicago, Illinois|Chicago]], [[Illinois]], and then to [[Sandusky, Ohio|Sandusky]], [[Ohio]], where it affiliated with the [[Roberts Motor Company]], which was Benoist{{'}}s preferred source for aircraft engines. Benoist designed the [[Benoist Type XVI|Type XVI]] flying boat and [[Benoist Type XVII|Type XVII]] landplane, both of which appeared in 1916.<ref name="Roos"/>

=== Death ===
On 14 June 1917, Benoist died when he struck his head against a [[telephone pole]] while stepping off a [[streetcar]] in front of the Roberts Motor Company in Sandusky.<ref name="Roos"/><ref>{{cite web|title=Welcome to Flight City|url=http://www.mohistory.org/Flight_City/HTML/Collectionbenoist.html|accessdate=18 September 2011}}</ref> With him gone and facing continued financial problems, the Benoist Aircraft Company and the Roberts Motor Company both went out of business in early 1918. Benoist Aircraft had built just over 100 airplanes in its history by the time it ceased operations.<ref name="Roos"/>

Benoist is buried at [[Hopewell Cemetery]] in [[Hopewell, Washington County, Missouri|Hopewell]] in [[Washington County, Missouri|Washington County]], Missouri.<ref name="Find-a-Grave Benoist"/>

==Gallery==
<gallery>
File:Thomas W. Benoist.jpg|
</gallery>

== References ==
{{Reflist|2}}

== External links ==
*[http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GSln=Benoist&GSfn=Thomas&GSby=1874&GSbyrel=in&GSdy=1917&GSdyrel=in&GScntry=4&GSob=n&GRid=27522729&df=all& Photo of grave of Thomas W. Benoist]

{{DEFAULTSORT:Benoist, Thomas}}
[[Category:1874 births]]
[[Category:1917 deaths]]
[[Category:People from Washington County, Missouri]]
[[Category:Businesspeople from St. Louis]]
[[Category:Aviation history of the United States]]
[[Category:Aviators from Missouri]]
[[Category:Aircraft designers]]
[[Category:American aviation businesspeople]]
[[Category:Road incident deaths in Ohio]]
[[Category:Burials in Missouri]]