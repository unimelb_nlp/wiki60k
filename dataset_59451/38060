{{Dablink|For other uses, see [[Blah (disambiguation)|Blah]].}}

{{Infobox single <!-- See Wikipedia:WikiProject_Songs -->
| Name           = Blah Blah Blah
| Artist         = [[Kesha]] featuring [[3OH!3]]
| Cover          = Kesha_Blah_Blah_Blah_Sony_Music.jpeg
| Album          = [[Animal (Kesha album)|Animal]]
| Released       = {{Start date|2010|02|19}}
| Recorded       = 2009; Lotzah Matzah Studios, New York City, New York
| Format         = {{hlist|[[Compact Disc single|CD single]]|[[music download|digital download]]}}
| Genre          = {{hlist|[[Electropop]]|[[dance-pop]]}}
| Length         = 2:52
| Label          = [[RCA Records|RCA]]
| Writer         = {{hlist|Kesha Sebert|[[Benny Blanco|Benjamin Levin]]|[[Neon Hitch]]|Sean Foreman}}
| Producer       = Benny Blanco
| Chronology     = [[Kesha]] singles
| Last single    = "[[Tik Tok]]"<br>(2009)
| Next single    = "[[Dirty Picture]]"<br />(2010)
| This single    = "'''Blah Blah Blah'''"<br>(2010)
| Misc           =
{{ Extra chronology
| Artist =  [[3OH!3]] singles
| Type = singles
| Last single = "Still Around" <br>(2009)
| This single = "'''Blah Blah Blah'''"<br />(2010)   
| Next single = "[[My First Kiss (song)|My First Kiss]]" <br>(2010)
}}
}}

"'''Blah Blah Blah'''" is a song by American recording artist [[Kesha]] from her debut album, ''[[Animal (Kesha album)|Animal]]'' (2010). Produced by [[Benny Blanco]], and co-written by Kesha, Blanco, [[Neon Hitch]] and Sean Foreman, it was released as the album's second single on February 19, 2010, and features [[3OH!3]]. Initial writing of the song took place when Kesha, Blanco, Hitch and Foreman were discussing which sex talked more and which one was more "obnoxious." The song is a midtempo [[electropop]] song that speaks of men in the same way that they have talked about women in the music industry. The lyrics depict a woman who would rather have sex than listen to a man speak and features blatant come-ons throughout the song.

The single achieved commercial success by reaching the top five in [[Australia]] and [[Canada]], whilst charting within the top ten in the [[United States]] and [[New Zealand]]. The song became Kesha's second top ten single in Australia, Canada and the United States. It has gone on to sell over two million copies in the United States as well as being certified two times platinum in Canada.

The music video for "Blah Blah Blah" was directed by [[The Malloys|Brendan Malloy]]. The video follows similar suit to the song's lyrics. It depicts Kesha getting hit on by a variety of different men and she continually rejects them. Kesha and 3OH!3 performed the song on [[American Idol (season 9)|ninth season]] of ''[[American Idol]]'' to promote the single.

== Writing and inspiration ==
"Blah Blah Blah" was written by Kesha alongside [[Neon Hitch]], Sean Foreman and [[Benny Blanco]] who also produced the song. Kesha said that the song originated from a discussion they had in the studio on the politics of female-male relationships which Kesha later explained, "The song came about when the people that wrote it — me, Benny Blanco, Neon Hitch and [3OH!3's] Sean Foreman — all got in a room, and they were talking about how chicks talk too much," [...] "And me and Neon were like, 'No, no, no, guys talk too much," [...] "So, we had this war of who were more obnoxious, chicks or dudes. And the song kind of came around from that conversation. I think I make a pretty fair point both in this video and in the song, that dudes are way more annoying."<ref name="mtv">{{cite web|url=http://www.mtv.com/news/articles/1631486/20100208/_kesha_.jhtml|title=Kesha Gets Into Battle Of The Sexes With 3Oh!3 For 'Blah Blah Blah' Video|author=Vena, Jocelyn|date=2010-02-08|work=[[MTV News]]|publisher=[[MTV Networks]] ([[Viacom]])|accessdate=2010-02-10}}</ref>

==Composition==
{{listen
  | filename = Ke$ha ft. 3OH!3 Blah Blah Blah sample.ogg
  | title       = "Blah Blah Blah"
  | description = "Blah Blah Blah" is a midtempo [[electropop]] song composed in [[D minor]]. Its lyrics depict a woman that would rather have sex than talk and featured blatant come-ons.
  | format      = [[Ogg]]
  | pos = left
}}
"Blah Blah Blah" is a midtempo [[electropop]] song;<ref>{{cite web |url=http://www.billboard.com/articles/columns/viral-videos/958923/keha-gives-blah-performance-on-idol|title=Ke$ha Gives 'Blah' Performance on 'Idol'|author=Skripnokov, Iyla|date=2010-03-18|work=Billboard|publisher=Prometheus Global Media|accessdate=2012-02-24}}</ref> the song utilizes synthesizers and drum machines in its production. According to sheet music published at Musicnotes.com by Kobalt Music Publishing, the single is set in [[Meter (music)|compound time]] with a moderate beat rate of 120 [[Tempo|beats per minute]]. It is written in the key of [[D minor]] and Kesha's vocal range in the song spans from the note of D<sub>3</sub> to the note of D<sub>5</sub>.<ref name="BlahComp">{{cite web|title=Kesha, 'Blah Blah Blah' – Composition Sheet Music|work=''Musicnotes.com''|publisher=Kobalt Music Publishing America, Inc.}}</ref> Lyrically, "Blah Blah Blah" depicts a woman who would rather have sex than listen to a man speak as the singer finds talkative men annoying; blatant come-ons such as "show me where your dick's at" and "I wanna be naked" leave this literal meaning unambiguous. Kesha herself has been frank about this straightforward interpretation of the lyrics.<ref name="mtv" /> The lyrics have been interpreted as a shot at the way men objectify women by speaking about them the way that they do in the music industry. "<ref name="mtv" /> Andrew Burgess of MusicOMH felt that the line "I don't really care where you live at, just turn around boy, let me hit that. Don't be a little bitch with your chit-chat; just show me where your dick's at" was a way of belittling her male peers.<ref name="MusicOMH" /> Fraser McAlpine of [[BBC]] noted that it was cultural progress that a woman can now "sing a song[...] as dirty [...] as her male peers".<ref name="BBCBlah" />

== Critical reception ==
[[File:3OH!3 at Bamboozle Left 2008.jpg|right|thumb|165px|Critics were divided over the pairing of Kesha and 3OH!3 (pictured), with some calling 3OH!3's appearance superfluous and noting she out performed them.]]
Fraser McAlpine of the BBC was impressed with the song saying "it's a sign of cultural progress that a modern pop lass can sing a song which is exactly as dirty as the kind of song her male peers would risk." He compared Kesha's vocal delivery to that of [[Eminem]], he also compared the hook, "you tah-tah-tawkin' that Blah Blah Blah"  to "[[Bad Romance]]" by [[Lady Gaga]], stating that the song was a "rural, farm-girl spin" of it.<ref name=BBCBlah>{{cite web|url=http://www.bbc.co.uk/blogs/chartblog/2010/02/keha_ft_3oh3_blah_blah_blah.shtml|title=BBC – Chart Blog: Ke$ha ft 3Oh3! – 'Blah Blah Blah'|author=McAlpine, Fraser|date=2010-02-03|work=[[BBC Online|BBC Chart Blog]]|publisher=[[BBC|British Broadcasting Corporation]]|accessdate=2010-02-08}}</ref> Jim Farber of the New York ''[[Daily News (New York)|Daily News]]'' stated that the track "could become the 'whatever' [[anthem]] of the season."<ref>{{cite news|url=http://www.nydailynews.com/entertainment/music/2010/01/05/2010-01-05_keha_cashes_in_on_superfizzy_pop_with_animal.html|title=Kesha, 'Animal'|author=Farber, Jim|date=2010-01-05|work=[[Daily News (New York)|Daily News]]|accessdate=2010-01-19}}</ref> It was described by the ''[[Winnipeg Free Press]]'' as a "hard pop-hop cut with pogo-worthy beats."<ref name="blahblahreview">{{cite web|url=http://www.winnipegfreepress.com/entertainment/music/pop-and-rock-81862532.html|title=Kesha – Animal (RCA)|date=2010-01-16|work=[[Winnipeg Free Press]]|publisher=FP Canadian Newspapers|accessdate=2010-01-19}}</ref> Daniel Brockman of ''[[The Phoenix (newspaper)|The Phoenix]]'' thought that Kesha "intone[d] in a lusty, disturbingly carefree tone" on the song.<ref name="Brockman">{{cite web|url=http://thephoenix.com/Boston/music/95211-kesha-animal-2010/|title=Kesha <nowiki>&#124;</nowiki> Animal|author=Brockman, Daniel|date=2010-01-08|work=[[The Phoenix (newspaper)|The Phoenix]]|publisher=[[Phoenix Media/Communications Group]]|accessdate=2010-01-17}}</ref>

[[Ann Powers]] of the ''[[Los Angeles Times]]'' said that "Blah Blah Blah" was one of those "moments on ''Animal'' that are nearly as experimental as an [[Animal Collective]] record, but instead of some wistful, [[Brian Wilson]]-loving artiste at the song's center, there's this girl, rolling her eyes and snapping her gum."<ref name="Powers">{{cite news|url=http://www.latimes.com/entertainment/news/la-et-kesha5-2010jan05,0,4048424.story|title=Kesha is a wisecracking 'Animal'|author=Powers, Ann|date=2010-01-05|work=[[Los Angeles Times]]|publisher=[[Tribune Company]]|accessdate=2010-01-17}}</ref> Melanie Bertoldi from ''[[Billboard (magazine)|Billboard]]'' magazine also thought that the verse by 3OH!3 "severely slow[ed] down the momentum and never quite gel[ed] with Kesha's catty, aggressive delivery," but praised the song for its "danceability".<ref>{{cite web |url=http://www.billboard.com/articles/review/1069687/keha-featuring-3oh3-blah-blah-blah|title=Kesha Blah Blah Blah on Billboard|author=Bertoldi, Melanie|date=2010-02-26|work=Billboard|publisher=Nielsen Business Media|accessdate=2010-02-28|archiveurl=http://www.billboard.com/new-releases/ke-ha-featuring-3oh-3-blah-blah-blah-1004071466.story|archivedate=2012-03-01}}</ref> Andrew Burgess of [[MusicOMH]] said that while "3OH!3 makes a lame attempt to assert the case for male equality [on the song], Kesha comes off as so infectiously dominating, it's hard to take [3OH!3] seriously."<ref name="MusicOMH">{{cite web|url=http://www.musicomh.com/albums/kesha_0110.htm|title=Kesha – Animal|author=Burgess, Andrew|date=2010-02-01|work=[[MusicOMH]]|accessdate=2010-01-30}}</ref> Mayer Nissim of [[Digital Spy]] gave the song two out of five stars, stating that it was not as catchy as her debut, slamming the lyrics for being "thick" and adding that she failed to deliver on many lines, calling them "faux-outrageous, faux-feminist trash".<ref name="dss">{{cite web|last=Nissim|first=Mayer|title=Ke$ha ft. 3OH!3: 'Blah Blah Blah'|url=http://www.digitalspy.co.uk/music/singlesreviews/a203582/keha-ft-3oh3-blah-blah-blah.html|work=[[Digital Spy]]|publisher=([[Hachette Filipacchi Médias|Hachette Filipacchi UK]])|accessdate=2010-06-24|date=2010-03-01}}</ref>  Melinda Newman of [[HitFix]] criticized the song, calling 3OH!3 "slumming" and "wretched."<ref>{{cite web|url=http://www.hitfix.com/blogs/the-beat-goes-on/posts/review-ke-ha-s-animal-aims-straight-for-the-party-girls|title=Album review: Ke$ha's 'Animal' aims straight for the party girls|author=Newman, Melinda|date=2010-01-04|publisher=[[HitFix]]|accessdate=2012-05-13}}</ref>

== Chart performance ==
[[File:Kesha Showbox SoDo.jpg|thumb|left|Kesha performing "Blah Blah Blah" at the [[Get Sleazy Tour]]]]
In the United States, "Blah Blah Blah" debuted on the [[Billboard Hot 100|''Billboard'' Hot 100]] at position seven, and on the [[Hot Digital Songs]] chart at number two, selling a total of 206,000 downloads, both chartings would become the songs' peaks.<ref name="Hot 100">{{cite web|url=http://www.billboard.com/articles/news/959757/keha-holds-on-hot-100-doubles-down-on-digital-songs|title=Kesha Holds On Hot 100; Doubles Down On Digital Songs |author=Pietroluongo, Silvio |work=Billboard|publisher=Nielsen Business Media|date=2010-01-13|accessdate=2010-01-14}}</ref> On ''Billboard's'' [[Mainstream Top 40 (Pop Songs)|Pop chart]], the song reached a peak of eleven.<ref name="sc_Billboardpopsongs_Ke$ha"/> The song has received Platinum certification by the [[Recording Industry Association of America]] for sales of 1,000,000 units and as of February 2011, the song has sold more than two million digital copies in the United States alone.<ref name="2mill">{{cite web |url=http://new.music.yahoo.com/blogs/chart_watch/74334/week-ending-feb-20-2011-songs-gaga-defies-odds/ |title="Blah, Blah, Blah" (featuring 3OH!3) (2,074,000)|author=Grein, Paul|date=2011-02-23|work=Yahoo Music|publisher=[[Yahoo!]]|accessdate=2010-02-24}}</ref> as of June 2011, the song has sold 2,132,000 copies in the United States.<ref>{{cite web |url=http://new.music.yahoo.com/blogs/chart_watch/74376/week-ending-june-12-2011-songs-the-odd-couples/;_ylt=Askn45yJb4ZNNSvWL3KHqrMPwiUv|title=Week Ending June 12, 2011. Songs: The Odd Couples|author=Grein, Paul|date=2011-05-15|work=Chart Watch|publisher=Yahoo! Inc|accessdate=2011-05-15}}</ref> In Canada, "Blah Blah Blah" debuted on the [[Canadian Hot 100]] at position three, which became its peak.<ref name="CA 100">{{cite web|url=http://www.billboard.com/charts/2010-01-23/canadian-hot-100|title=Canadian Hot 100 – Week of January 23, 2010|date=2010-01-23|work=Billboard|publisher=Nielsen Business Media|accessdate=2010-01-15}}</ref> In March 2010 the single was certified double platinum by the [[Canadian Recording Industry Association]] (CRIA) for sales of 160,000 units.<ref name="cria"/>

In Australia, "Blah Blah Blah" entered the Australian chart at position seven on the week of January 24, 2010.<ref name="sc_Australia_Ke$ha feat. 3OH!3"/> The following week the song moved to position four, where it held the spot for three consecutive weeks. On the song's fifth week on the chart, it reached its peak at position three where it held the spot for one week.<ref name="sc_Australia_Ke$ha feat. 3OH!3"/> The song was certified platinum by the [[Australian Recording Industry Association]] (ARIA) for sales of 70,000 units.<ref name="ARIA Top 50"/> In New Zealand, the song debuted and peaked at position seven. It later went on to receive gold certification by the [[Recording Industry Association of New Zealand]] (RIANZ) for sales of 7,500 units.<ref name="NZGold"/> "Blah Blah Blah" made its debut onto the [[UK Singles Chart]] at number eleven on the issue dated February 7, 2010<ref name="uKentry1">{{cite web|url=http://www.chartstats.com/chart.php?week=20100213|title=Chart Stats – Singles Chart For 13/02/2010|work= ''Chartstats.com''|publisher=Chart Stats|accessdate=2010-05-10}}</ref> with sales of 27,161.<ref>Jones, Alan (2010-02-08). "Album sales jump as Alicia tops the charts". [[Music Week]] ([[United Business Media]])</ref>

== Music video ==
[[File:Blah Blah Blah. Video.jpg|right|thumb|220px|Kesha in the music video, duct taping a man's mouth closed in response to his attempt to hit on her.]]
The music video for the song was directed by [[The Malloys|Brendan Malloy]].<ref>{{cite web|url=http://www.mtv.com/videos/keha/487452/blah-blah-blah.jhtml|work=[[MTV Networks]]|publisher=([[MTV]])|title=Kesha – "Blah Blah Blah"|accessdate=2010-03-20}}</ref> It premiered on February 23, 2010, on [[Vevo]].<ref>{{cite web|url=http://www.vevo.com/watch/kesha/blah-blah-blah/USRV81000007|title=Kesha Blah Blah Blah Music Video Debut|work=[[Vevo]]}}</ref> Both members of 3OH!3 make an appearance in the video.<ref>{{cite web|url=http://www.people.com/people/article/0,,20341364,00.html|title=First Look at Ke$ha's Latest Video Shoot|last=Messer|first=Lesley|date=2010-02-03|work=''[[People (magazine)|People]]'' magazine|publisher=[[Time Inc.]]|accessdate=2010-02-04}}</ref> Kesha told [[MTV]] that the video primarily involves men (described as "douchey") hitting on Kesha, while she declines their advances. "At one point," Kesha says "I get to be strapped to this harness and bouncing around everywhere, and it was really cool [...] The whole concept of the video was a bunch of douche-y guys macking on me as usual, and me making them eat their toupees or other various items".<ref name="mtv"/> Melanie Bertoldi of ''[[Billboard (magazine)|Billboard]]'' said that the clip was "thoroughly entertaining".<ref name="bbvideo">{{cite web|url=http://www.billboard.com/articles/columns/viral-videos/959268/keha-crushes-mens-hearts-in-blah-blah-blah-video|title=Ke$ha Crushes Men's Hearts In 'Blah Blah Blah' Video o|last=Bertoldi|first=Melaine|date=2010-02-24|work=Billboard|publisher=Nielsen Business Media|accessdate=2010-02-26|archiveurl=http://www.billboard.com/column/viralvideos/ke-ha-crushes-men-s-hearts-in-blah-blah-1004070943.story|archivedate=2012-03-01}}</ref>

In the first scene, outside of a club, Kesha is hit on by comedian [[Bret Ernst]]. He tries to convince Kesha that they would make a good couple, meanwhile Kesha texts someone, describing the man as a "major douchemaster". The next scene has Kesha at a bar playing pool near a man, where she duct-tapes him, later pulling down his pants, after describing him as a "tool bag" in a text message. In an arcade, Kesha pushes a man away who is making conversation with her. The third suitor attempts to serenade Kesha with a guitar, to which Kesha replies by stuffing paper in his mouth. In the final sequence, a man tries to talk to Kesha in a bowling alley. She loses interest when his toupée falls off his head and she shoves the hairpiece into his mouth, in vein of the previous arcade scene. The video ends with Kesha and 3OH!3 singing and dancing together in the bowling lanes.<ref name="bbvideo"/><ref>{{cite web|url=http://www.mtv.com/news/articles/1632550/20100224/_kesha_.jhtml|title=Ke$ha Teaches Boys A Lesson (Or Four) In 'Blah Blah Blah' Video|last=Vena|first=Jocelyn|date=2010-02-24|work=MTV News|publisher=MTV Networks (Viacom)|accessdate=2010-02-25}}</ref>

== Live performances==
The song was first performed live for ''MTV Push'', a program broadcast by [[MTV Networks]] worldwide.<ref>{{cite web|url=http://www.columbia.co.uk/news/15023/0/ |title=Watch Ke$ha on MTV Push! |work=''columbia.com'' |publisher=[[Columbia Records]] |accessdate=2009-11-06 |deadurl=yes |archiveurl=https://web.archive.org/web/20100101132048/http://www.columbia.co.uk:80/news/15023/0 |archivedate=2010-01-01 |df= }}</ref><ref>{{cite web|url=http://www.mtvnetworks.co.uk/worldstage |title=MTV NETWORKS INTERNATIONAL ROLLS-OUT MTV WORLD STAGE & MTV PUSH |work=MTV Networks |publisher=Viacom |format=Press release |accessdate=2009-11-17 |archiveurl=http://www.webcitation.org/5qi6dooPA?url=http%3A%2F%2Fwww.mtvnetworks.co.uk%2Fworldstage |archivedate=2010-06-23 |deadurl=yes |df= }}</ref> It was also performed on January 18, 2010, at ''[[MuchOnDemand]]'', broadcast on Canadian cable [[Music television|music channel]], [[MuchMusic]].<ref>{{cite web|url=http://www.muchmusic.com/tv/mod30/video.aspx?bid=4899&eid=42626 |title=Ke$ha – Jan 18, 10' |work=[[MuchMusic]] |publisher=[[CTVglobemedia]] |accessdate=2010-01-21 }}{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> Kesha performed for the first time a heavily [[censorship|censored]] version of the song on the [[American Idol (season 9)|ninth season]] of ''[[American Idol]]'' with 3OH!3 on March 17, 2010.<ref name="AmericanIDOL">{{cite news|url=http://www.nydailynews.com/entertainment/americanidol/2010/03/18/2010-03-18_american_idol_2010_top_11_lacey_brown_is_voted_off_as_kesha_david_cook_orianthi_.html|title=Ke$ha performing on American Idol|work=Daily News|accessdate=2010-03-18 | location=New York | first=Soraya | last=Roberts | date=2010-03-18}}</ref> In the United Kingdom, Kesha made two appearances to perform the song. The first was on February 18, 2010, ''[[Alan Carr: Chatty Man]]''.<ref>{{cite web|url=http://www.channel4.com/programmes/alan-carr-chatty-man/episode-guide/series-3/episode-3|title=Series 3 <nowiki>&#124;</nowiki> Episode 3 <nowiki>&#124;</nowiki> Alan Carr: Chatty Man|date=2010-02-18|work=[[Alan Carr: Chatty Man]]| publisher=[[Channel 4]]|accessdate=2010-02-19}}</ref> It was followed by a performance on breakfast television show [[GMTV]], on February 19.<ref>{{cite web|title=KE$ha Television Appearance <nowiki>&#124;</nowiki> What's on GMTV|date=2010-02-12|work=[[GMTV]] |publisher=[[ITV plc|ITV]]}}</ref> She also performed the song in a set for BBC [[Radio 1's Big Weekend]],<ref>{{cite web|url=http://www.bbc.co.uk/radio1/bigweekend/2010/artists/kesha/|title=BBC – Radio 1's Big Weekend – Ke$ha|publisher=BBC|accessdate=2010-08-06}}</ref> as well as ''Willkommen Bei Mario Barth Live'' in Germany and ''[[So You Think You Can Dance Australia|So You Think You Can Dance]]'' in Australia.<ref>{{cite web|url=http://www.tvcentral.com.au/2010/02/09/keha-to-perform-on-so-you-think-you-can-dance-australia/ |title=Ke$ha to perform on ‘So You Think You Can Dance Australia’ |date=2010-02-09 |author=Cameron |work=TV Central |accessdate=2010-09-02 |archiveurl=http://www.webcitation.org/5sSHQ5QZo?url=http%3A%2F%2Fwww.tvcentral.com.au%2F2010%2F02%2F09%2Fkeha-to-perform-on-so-you-think-you-can-dance-australia%2F |archivedate=2010-09-03 |deadurl=yes |df= }}</ref>

== Track listing ==
{{col-begin}}
{{col-2}}
*'''CD single'''<ref name="CDsingle"/>
# "Blah Blah Blah"&nbsp;– 2:52

*'''Digital download'''<ref name="Digitaldownload"/>
# "Blah Blah Blah"&nbsp;– 2:52
# "Tik Tok" (Joe Bermudez Club Mix)&nbsp;– 5:09

{{col-2}}
*'''UK iTunes digital download&nbsp;– EP'''<ref name="EP"/>
# "Blah Blah Blah"&nbsp;– 2:52
# "Tik Tok" (Joe Bermudez Club Mix)&nbsp;– 5:09
# "Tik Tok" (Trey Told 'Em Remix)&nbsp;– 4:14
{{col-end}}

== Credits and personnel ==
*Songwriting&nbsp;– Kesha Sebert, Benny Blanco, Neon Hitch, Sean Foreman
*Production&nbsp;– Benny Blanco
*Instruments and programming&nbsp;– Benny Blanco
*Recording&nbsp;– Benny Blanco
*Engineering&nbsp;– Benny Blanco

Source <ref name="book">{{cite AV media notes|url=http://itunes.apple.com/us/album/animal/id344796445 |title=''Animal'' digital album booklet via iTunes |work=[[RCA]]}}</ref>

== Charts and certifications ==
{{col-begin}}
{{col-2}}

===Charts===
{| class="wikitable sortable"
!Chart (2010)
!Peak<br>position
|-
{{singlechart|Australia|3|artist=Ke$ha feat. 3OH!3|song=Blah Blah Blah|accessdate=2010-06-03}}
|-
{{singlechart|Austria|21|artist=Ke$ha feat. 3OH!3|song=Blah Blah Blah|accessdate=2010-06-03}}
|-
{{singlechart|Flanders|45|artist=Ke$ha feat. 3OH!3|song=Blah Blah Blah|accessdate=2010-06-03}}
|-
{{singlechart|Wallonia|31|artist=Ke$ha feat. 3OH!3|song=Blah Blah Blah|accessdate=2010-06-03}}
|-
{{singlechart|Billboardcanadianhot100|3|artist=Ke$ha|artistid=1308564|accessdate=2010-05-10}}
|-
{{singlechart|Billboardeuropeanhot100|36|artist=Ke$ha|artistid=1308564|accessdate=2010-06-03}}
|-
{{singlechart|France|27|artist=Ke$ha feat. 3OH!3|song=Blah Blah Blah|accessdate=2010-06-03}}
|-
{{singlechart|Germany|11|artist=Kesha feat. 3OH!3|song=Blah Blah Blah|accessdate=2010-06-03}}
|-
{{singlechart|Ireland|18|year=2010|week=10|artist=Ke$ha|accessdate=2010-06-03}}
|-
|Luxembourg ([[Billboard charts#International charts|''Billboard'']])<ref name=lux>{{cite web|title=Ke$ha - Char History - Luxembourg|url={{BillboardURLbyName|artist=ke$ha|chart=Luxembourg}}|work=Billboard|publisher=Prometheus Global Media|accessdate=16 March 2013}}</ref>
|align="center"|9
|-
|-
{{singlechart|Dutch40|32|year=2010|week=10|artist=Ke$ha|accessdate=2011-02-23}}
|-
{{singlechart|New Zealand|7|artist=Ke$ha feat. 3OH!3||song=Blah Blah Blah|accessdate=2010-06-03}}
|-
{{singlechart|Scotland|7|artist=Kesha|song=Blah Blah Blah|date=2010-02-13|accessdate=2011-04-30}}
|-
|South Korea ([[Gaon Chart|GAON International]])<ref name="korea">{{cite web|url=http://gaonchart.co.kr/digital_chart/index.php?nationGbn=E&current_week=10&current_year=2010&chart_Time=week|title=Gaon Digital Chart|accessdate=March 17, 2014}}</ref>
|align="center"|1
|-
{{singlechart|Sweden|38|artist=Ke$ha feat. 3OH!3|song=Blah Blah Blah|accessdate=2010-06-03}}
|-
{{singlechart|Switzerland|30|artist=Ke$ha feat. 3OH!3|song=Blah Blah Blah|accessdate=2010-06-03}}
|-
{{singlechart|UKchartstats|11|artist=Kesha|song=Blah Blah Blah|songid=34745|accessdate=2010-07-15}}
|-
{{singlechart|Billboardhot100|7|artist=Ke$ha|artistid=1308564|accessdate=2010-06-03}}
|-
{{singlechart|Billboarddanceclubplay|37|artist=Ke$ha|song=Blah Blah Blah|artistid=1308564|accessdate=2010-11-03}}
|-
{{singlechart|Billboardpopsongs|11|artist=Ke$ha|song=Blah Blah Blah|artistid=1308564|accessdate=2010-11-03}}
|-
{{singlechart|Billboardrhythmic|17|artist=Ke$ha|song=Blah Blah Blah|artistid=1308564|accessdate=2010-11-03}}
|}

{{col-2}}

===Year-end charts===
{| class="wikitable sortable" border="1"
|-
!scope="col" style="width:16em;"|Chart (2010)
!scope="col"|Position
|-
| Australia ARIA Charts<ref>{{cite web|url=http://www.aria.com.au/pages/aria-charts-end-of-year-charts-top-100-singles-2010.htm |title=ARIA Charts – End Of Year Charts – Top 100 Singles 2010 |publisher=Australian Recording Industry Association |accessdate=2011-01-14 |deadurl=yes |archiveurl=https://web.archive.org/web/20101205172929/http://aria.com.au/pages/aria-charts-end-of-year-charts-top-100-singles-2010.htm |archivedate=2010-12-05 |df= }}</ref>
|style="text-align:center;"|41
|-
| Canadian Hot 100<ref>{{cite web|url=http://www.billboard.com/charts/year-end/2010/canadian-hot-100?begin=41&order=position|title=Best Of 2010 Canadian Hot 100 Songs|work=Billboard|publisher=Prometheus Global Media|accessdate=2011-01-14}}</ref>
|style="text-align:center;"|43
|-
|UK Singles Chart<ref>{{cite web|url=http://www.ukchartsplus.co.uk/UKChartsPlusYE2010.pdf|title=End Of Year Charts: 2010|publisher=[[UKChartsPlus]]|accessdate=2011-08-10}}</ref>
|style="text-align:center;"|122
|-
| US ''Billboard'' Hot 100<ref>{{cite web|url=http://www.billboard.com/charts/year-end/2010/hot-100-songs?begin=51&order=position|title=Best Of 2010 Hot 100 Songs|work=Billboard|publisher=Prometheus Global Media|accessdate=2011-01-14}}</ref>
|style="text-align:center;"|51
|}

===Certifications===
{| class="wikitable" border="1"
|-
! Region
! [[List of music recording certifications|Certification]]
|-
| [[Australian Recording Industry Association|Australia]]
| Platinum<ref name="ARIA Top 50">{{cite web|url=http://www.aria.com.au/pages/httpwww.aria.com.auaccreds2010.htm |publisher=[[Australian Recording Industry Association]] |title=ARIA Charts&nbsp;— Accreditations&nbsp;– 2010 Singles |accessdate=2010-03-28 |deadurl=yes |archiveurl=http://www.webcitation.org/64wvbyh3C?url=http%3A%2F%2Fwww.aria.com.au%2Fpages%2Fhttpwww.aria.com.auaccreds2010.htm |archivedate=2012-01-25 |df= }}</ref>
|-
|[[Canada]]
|2× Platinum<ref name="cria">{{cite web|url=http://www.cria.ca/gold/0310_g.php |title=Gold & Platinum certifications |publisher=[[Canadian Recording Industry Association]] |accessdate=2010-04-12 |deadurl=yes |archiveurl=https://web.archive.org/web/20120308151222/http://www.cria.ca/gold/0310_g.php |archivedate=2012-03-08 |df= }}</ref>
|-
|[[New Zealand]]
|Gold<ref name="NZGold">{{cite web|url=http://www.radioscope.net.nz/index.php?option=com_content&task=view&id=77&Itemid=63 |title=May 2, 2010: Radio Scope |publisher=[[Recording Industry Association of New Zealand]] |accessdate=2010-05-27 |archiveurl=https://web.archive.org/web/20100523022854/http://www.radioscope.net.nz/index.php?option=com_content&task=view&id=77&Itemid=63 |archivedate=2010-05-23 |deadurl=yes |df= }}</ref>
|-
|[[Sverigetopplistan|Sweden]]
|Gold<ref>{{cite web|url=http://www.sverigetopplistan.se/index.html|title=Sverigetopplistan: Kesha - Blah Blah Blah|publisher=Sverigetopplistan. Grammofonleverantörernas förening|language=Swedish|accessdate=2013-07-18|format=''To access certification, one must search (Sök) for "Blah Blah Blah" or "Kesha Blah Blah Blah" and click the "Visa" button.''}}</ref>
|-
|[[Recording Industry Association of America|United States]]
|2× Platinum<ref name="US Certs">{{cite web|url=http://www.riaa.com/goldandplatinumdata.php?resultpage=1&table=SEARCH_RESULTS&action=&title=&artist=Ke$ha&format=&debutLP=&category=&sex=&releaseDate=&requestNo=&type=&level=&label=&company=&certificationDate=&awardDescription=&catalogNo=&aSex=&rec_id=&charField=&gold=&platinum=&multiPlat=&level2=&certDate=&album=&id=&after=&before=&startMonth=1&endMonth=1&startYear=1958&endYear=2010&sort=Artist&perPage=25 |title=Gold & Platinum: Kesha Singles and Albums |publisher=[[Recording Industry Association of America]] |accessdate=2010-10-05 |deadurl=yes |archiveurl=https://web.archive.org/web/20130915103138/http://www.riaa.com/goldandplatinumdata.php?resultpage=1&table=SEARCH_RESULTS&action=&title=&artist=Ke$ha&format=&debutLP=&category=&sex=&releaseDate=&requestNo=&type=&level=&label=&company=&certificationDate=&awardDescription=&catalogNo=&aSex=&rec_id=&charField=&gold=&platinum=&multiPlat=&level2=&certDate=&album=&id=&after=&before=&startMonth=1&endMonth=1&startYear=1958&endYear=2010&sort=Artist&perPage=25 |archivedate=2013-09-15 |df= }}</ref>
|}
{{col-end}}

==Radio date and release history==
{| class="wikitable"
|-
! Region
! Date
! Format
|-
|United States
|February 2, 2010
||[[Rhythmic contemporary|Rhythmic]]<ref name="airplay">{{cite web|url=http://www.fmqb.com/Article.asp?id=69239#210|title=Available For Airplay: 2/2 Rhythm Crossover|work=[[FMQB]]|publisher=Friday Morning Quarterback|accessdate=2010-03-01}}</ref>
|-
|rowspan=2|Australia<ref name="CDsingle">{{cite web|url=http://www.jbhifionline.com.au/music/pop-rock/blah-blah-blah/480766|title=Blah Blah Blah – Kesha (Australia CD Single)|work=[[JB Hi-Fi]]|publisher=jbhifionline.com.au|accessdate=2010-02-01}}</ref><ref name="Digitaldownload">{{cite web|url=http://www.bandit.fm/poparazzi/kesha/store/|title=Kesha (Australia Digital Download Release)|work=''bandit.fm'' |publisher=[[Sony Music Entertainment]]|accessdate=2010-02-21}}</ref>
|rowspan=2|February 19, 2010
|[[Compact Disc single|CD single]]
|-
|rowspan="3"|[[music download|Digital download]]
|-
|South Korea
|February 26, 2010<ref>{{cite web |url=http://www.sonymusic.co.kr/music/album.asp?albumid=103785 |title=Blah Blah Blah (Single) |work=Sony Star |publisher=Sony Music Entertainment Korea Inc |language=Korean |accessdate=2011-04-05}}</ref>
|-
|rowspan=2|United Kingdom
|February 28, 2010<ref name="EP">{{cite web|url=http://itunes.apple.com/gb/album/blah-blah-blah-feat-3oh-3-ep/id356287821|title=Ke$ha: Blah Blah Blah (feat. 3OH!3) (United Kingdom EP)|work=''[[Apple Inc.|Apple Inc]]''|publisher=[[iTunes Store]]|accessdate=2010-03-01}}</ref>
|-
|March 1, 2010<ref>{{cite web|url=http://hmv.com/hmvweb/displayProductDetails.do?ctx=280;0;-1;-1;-1&sku=575900|title=Kesha – Blah Blah Blah (United Kingdom CD Release)|publisher=[[HMV Group]]|accessdate=2010-02-09}}</ref>
|rowspan="2"|CD single
|-
|Germany<ref>{{cite web|url=http://www.amazon.de/dp/B0036VSD7C|title= Blah Blah Blah (German Cd Release)|publisher=[[Amazon.com|Amazon]]|work=''Amazon.de''|author=Amazon Mp3|accessdate=2010-04-23}}</ref>
|April 23, 2010
|}

== References ==
{{Reflist|colwidth=30em}}

== External links ==
* [http://www.vevo.com/watch/kesha/blah-blah-blah/USRV81000007 Blah Blah Blah – Ke$ha featuring 3OH!3 (Video)]
{{good article}}

{{Kesha singles}}
{{3OH!3 songs}}

{{DEFAULTSORT:Blah Blah Blah (Song)}}
[[Category:2010 singles]]
[[Category:Kesha songs]]
[[Category:3OH!3 songs]]
[[Category:Songs about sexuality]]
[[Category:Songs written by Benny Blanco]]
[[Category:Song recordings produced by Benny Blanco]]
[[Category:Songs written by Kesha]]
[[Category:Songs written by Nathaniel Motte]]
[[Category:Music videos directed by The Malloys]]
[[Category:Songs written by Sean Foreman]]