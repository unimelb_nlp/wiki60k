{{Orphan|date=October 2013}}

{{Infobox company
| name = <big>The Get Schooled Foundation</big>
| logo = 
| type = 
| founder = Viacom and Bill & Melinda Gates Foundation, with significant support from AT&T 
| foundation = 2009 with 501(c)(3) status granted in 2010
| location = New York City
| key_people = Marie Groark, Executive Director
| industry = Nonprofit
| products = 
| revenue = 
| operating_income = 
| net_income = 
| num_employees =
| parent =
| subsid =
| homepage = [https://www.getschooled.com/ getschooled.com]
| footnotes = 
}}

'''The Get Schooled Foundation''' (also '''Get Schooled''' or '''getschooled.com''') is a [[non-profit organization]] dedicated to using the power of media, technology and popular culture to motivate and inspire young people, their families and teachers to improve high school graduation rates and college-going rates. Get Schooled combines powerful national content and programming (on-air, on-line and on the ground), with school-based engagement strategies to ensure both a broad reach and a measurable impact.<ref>[https://getschooled.com/about About Us], Get Schooled, getschooled.com. Accessed 2012-8-24.</ref>

==About==
Get Schooled focuses on two areas that have the most direct impact on students’ long-term educational success:  '''attendance and college affordability'''.  Get Schooled works with schools, school districts and cities around the country to improve attendance rates among middle and high school students.  It also focuses on efforts aimed at increasing the number of young people who have the tools and information they need to access financial aid for college.

==School Attendance==
Get Schooled is one of the few national education organizations focusing on school attendance as a driver of student success.  In May 2012, it released a study on the scope and consequence of chronic absenteeism in partnership with [[Johns Hopkins University]].<ref>[https://getschooled.com/attendance-counts/report Chronic Absenteeism Report], ''The Importance of Being in School: A Report on Absenteeism in the Nation's Public Schools''. Accessed 2012-8-24.</ref>  The report found only a handful of states measure and report on chronic absenteeism, which the report defines as missing at least 10 percent of school days in a given year, or about 18 days.<ref>Balfanz & Byrnes 2012, p. 3.</ref> It estimates that '''10 to 15 percent of students nationwide are chronically absent and that adds up to 5 million to 7.5 million students who miss enough school to be at severe risk of dropping out or failing to graduate from high school'''.<ref>Balfanz & Byrnes 2012, p. 3.</ref>

The data problem is structural and runs from the school to the state to the federal level. At the school level, chronic absenteeism is largely masked by daily attendance rates. A school can report a 90 percent average daily attendance rate and have 40 percent of students chronically  absent, because on different days different students make up the 90 percent. Schools know that students are missing but don’t look at the data by student to show individual absenteeism rates.<ref>Balfanz & Byrnes 2012, p. 3.</ref>

'''Key findings include:''' 
* Students who are chronically absent in one year will likely be so in subsequent years and may miss more than half a year of school over four or five years.<ref>Balfanz & Byrnes 2012, p. 13.</ref>
* Urban schools often have chronic absentee rates as high as one third of students, while poor rural areas are in the 25 percent range.<ref>Balfanz & Byrnes 2012, p. 4.</ref>
* While the problem affects youth of all backgrounds, children in poverty are more likely to be chronically absent. In [[Maryland]], chronic absentee rates for poor students were more than 30 percent, compared to less than 12 percent for students from more affluent families.<ref>Balfanz & Byrnes 2012, p. 20.</ref>
* Chronically absent students tended to be concentrated in a relatively small number of schools. In [[Florida]], 52 percent of chronically absent students were in just 15 percent of schools.<ref>Balfanz & Byrnes 2012, p. 21.</ref>
* In some school districts, kindergarten absenteeism rates are nearly as high as those in high school.<ref>Balfanz & Byrnes 2012, p. 18.</ref>

The magnitude of the problem is likely understated as [http://new.every1graduates.org/robert-balfanz/ Balfanz] and his researchers could find chronic absenteeism reported for only six states: [[Georgia (U.S. state)|Georgia]], [[Florida]], [[Maryland]], [[Nebraska]], [[Oregon]] and [[Rhode Island]]. Several states, including [[California]] and [[New York (state)|New York]], do not even collect the individual data needed to calculate chronic absenteeism.<ref>Balfanz & Byrnes 2012, p. 3.</ref>

The impact of these missed days is dramatic – students are less likely to score well on achievement tests and less likely to graduate.<ref>Balfanz & Byrnes 2012, p. 27.</ref> Students who miss 10 percent of school days on average score in the 30th percentile on standardized reading and math tests, compared to those with zero absences, scoring in the 50th percentile.<ref>Balfanz & Byrnes 2012, p. 27.</ref>

Looking at data from multiple states and school districts, the researchers found that consistently high chronic absenteeism was the strongest predictor of dropping out of high school, stronger even than course failures, suspension or test scores. Data from [[Georgia (U.S. state)|Georgia]] showed a very strong relationship between attendance in grades 8-10 and graduation. There was as much as a 50 percentage-point difference in graduation rates for students who missed five or fewer days compared to those who missed 15 or more days.<ref>Balfanz & Byrnes 2012, p. 9.</ref>

These findings have been extrapolated into a user-friendly [https://getschooled.com/attendance-counts attendance calculator] that allows users to see a personalized view of the impact of missed days on the likelihood of graduating and on math and reading achievement tests.<ref>[https://getschooled.com/attendance-counts Attendance Calculator], Get Schooled, getschooled.com. Accessed 2012-8-24.</ref>

==Programs==
Get Schooled has several programs it uses to support high school students.

'''Digital Platform:'''  Get Schooled is basically a one-stop shop for high school students who need college and financial aid info.  As you consume content on the site about calculating your GPA, making it through high school or applying to college, you earn points. You can cash those points in at the Get Schooled reward store stocked with items for school and life.<ref>{{cite news|last1=Graham|first1=Janay|title=Contributor|url=http://www.huffingtonpost.com/entry/582ba33fe4b0a904ef59ed6c?timestamp=1480377220848|publisher=Huffington Post|date=11/20/2016}}</ref>

'''College Text Hotline''' gives personalized help on how to apply for and pay for college including things like the FAFSA, scholarship, loans and general college guidance<ref>{{cite news|last1=Resmovitz|first1=Joy|title=Hey Kids Text Your Way to College|url=http://www.latimes.com/local/education/la-me-edu-helping-students-pay-for-college-one-text-message-at-a-time-20150927-story.html|publisher=LA Times|date=09/27/2015}}</ref>

'''Snapchat College Tours''' give students a student led tour of college campuses from around the country including HBCU's, ivy league colleges as well as larger and smaller public colleges and universities.<ref>{{cite news|last1=Tesema|first1=Martha|title=DJ Khaled is going to be the college tour guide you've always wanted|url=http://mashable.com/2016/11/07/dj-khaled-get-schooled-snapchat-tours/|publisher=Mashable|date=11/07/2016}}</ref>

'''Get Schooled badges''' expose students to targeted content that prepares them for college.  In 2016, Get Schooled announced a set of badges branded "Khaled Keys" inspired by their partnership with DJ Khaled.<ref>{{cite news|last1=Havens|first1=Lyndsey|title=DJ Khaled Named National Spokesperson for Get Schooled, Will Offer His 'Keys To Success'|url=http://www.billboard.com/articles/columns/hip-hop/7549095/dj-khaled-interview-national-spokesperson-get-schooled|publisher=Billboard}}</ref>

'''Get Schooled also offers scholarships and grants''' to schools and students throughout the school year.  The Taco Bell Foundation for Teens has been a major funder of these grants.<ref>{{cite news|last1=Bergado|first1=Gabe|title=I went to a Taco Bell-sponsored high school graduation at the TRL studio|url=http://www.dailydot.com/irl/taco-bell-graduate-for-mas/|publisher=Daily Dot|date=06/14/2015}}</ref>

== Celebrity Support ==

Get Schooled has tapped into the power of celebrity to encourage more students to graduate from high school and get the education they need to succeed.  Celebrity ambassadors have included:

:<ref>[[Kendrick Lamar]]</ref>  '''Kendrick Lamar''' recognized the success Get Schooled had with Mount Pleasant High School in Providence RI in boosting attendance.<ref>{{cite news|last1=Coleman|first1=Vernon|title=Kendrick Lamar Play Principal for the Day at Rhode Island High School|url=http://www.xxlmag.com/news/2013/05/kendrick-lamar-plays-principal-for-the-day-at-rhode-island-high-school/|publisher=XXL Mag|date=05/11/2013}}</ref>  He also traveled to Bethel, Alaska {along with James Harden} to recognize the huge jump in college application rates there.<ref>{{cite news|last1=Tardio|first1=Andres|title=Kendrick Lamar Vists Alaska School with Get Schooled Foundation|url=http://hiphopdx.com/news/id.25342/title.kendrick-lamar-visits-alaska-school-with-get-schooled-foundation|accessdate=09/06/2013|publisher=Hip Hop DX}}</ref>

:<ref>[[DJ Khaled]]</ref>  '''DJ Khaled''' joined Get Schooled at Carol City Middle School in Miami, FL to recognize their 8% attendance increase.<ref>{{cite news|last1=Gonzalez|first1=Noel|title=Principal for a Day:  DJ Khaled Snapchats his visit to Miami middle school|url=http://www.miamiherald.com/entertainment/celebrities/article55681745.html|accessdate=01/20/2016|publisher=Miami Herald}}</ref>  In 2016, he announced that he would be a national spokesperson for Get Schooled during the 2016-17 school year.<ref>{{cite news|last1=Goodwin|first1=Bruce|title=DJ Khaled Teaches Kids His Major Keys for Get Schooled Campaign|url=http://theurbandaily.com/3316982/dj-khaled-get-schooled/|accessdate=10/18/2016|publisher=The Urban Daily}}</ref>

:<ref>[[Daddy Yankee]]</ref> '''Daddy Yankee''' recognized the 4% attendance boost at Providence Career and Technical Academy.<ref>{{cite web|title=Daddy Yankee is Guest Principal for the Day|url=https://providenceschools.wordpress.com/2015/05/29/daddy-yankee-is-guest-principal-for-the-day-as-pcta/|website=providenceschools.wordpress.com|publisher=Providence Public Schools|accessdate=05/29/2015}}</ref>

==References==
<references/>

==External links==
* Johns Hopkins University School of Education's Center for Social Organization of Schools:  http://education.jhu.edu/research/crre/
* DefJam RapStar & Get Schooled Challenge:  http://online.wsj.com/article/PR-CO-20110222-910530.html
* LinkedIN: http://www.linkedin.com/company/get-schooled-foundation
* White House Commencement Challenge:  http://www.whitehouse.gov/commencement
* GetSchooled College Affordability Challenge: http://www.mtv.com/shows/get_schooled/series.jhtml
* Wake Up & Get Schooled: http://www.pbs.org/newshour/rundown/2011/12/celebrity-calls-urge-students-to-get-up-get-schooled.html
* My College Dollars Scholarship Search Tool: http://www.fastcompany.com/1809070/mtv-my-college-dollars-jason-rzepka
* Attendance Research - New York Times & Huffington Post Coverage:  http://www.nytimes.com/2012/05/17/education/up-to-15-percent-of-students-chronically-skip-school-johns-hopkins-finds.html and http://www.huffingtonpost.com/2012/05/17/absent-students-chronic-absenteeism-dropouts_n_1522673.html
* Lincoln High School as winner: http://www.detroitnews.com/article/20120514/ENT09/205140416/1409/rss36
* Aki Kurose as winner:  http://www.seattlepi.com/news/article/WA-middle-school-wins-attendance-contest-2753489.php
* East High School as winner:  http://www.kcci.com/School-Earns-Hollywood-Movie-Sneak-Peek/-/9357770/7309246/-/34fkud/-/index.html

[[Category:Education in New York City]]