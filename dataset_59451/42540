{|{{Infobox Aircraft Begin
 |name=Cessna 350 Corvalis
 |image=Cessna 350 Corvalis photo D Ramey Logan.jpg
 |caption=<!--image caption; if it isn't descriptive it should be left blank-->
}}{{Infobox Aircraft Type
 |type=Personal aircraft
 |manufacturer=[[Cessna]]
 |designer=
 |first flight=
 |introduced=
 |retired=
 |status=Production completed 2010<ref name="AvWeb29Mar11">{{Cite news|url = https://www.youtube.com/watch?v=YR-tRGCclMY|title = Video: Cessna's New Corvalis TTx at Sun 'n Fun|accessdate = 30 March 2011|last = Bertorelli|first = Paul|authorlink = |date=March 2011| work = AvWeb}}</ref>
 |primary user=<!--please list only one-->
 |more users=<!--up to three more. please separate with <br/>.-->
 |produced= 2000-2010
 |number built=
 |unit cost=[[United States Dollar|US$]]558,200 (2010)<ref name="CessnaPricing">{{cite web|url=http://se.cessna.com/single-engine/cessna-350/cessna-350-pricing.html |title=Cessna 350 Pricing and Cost |accessdate=26 August 2010 |last=[[Cessna|Cessna Aircraft]] |authorlink= |year=2009 |deadurl=yes |archiveurl=https://web.archive.org/web/20110708133056/http://se.cessna.com/single-engine/cessna-350/cessna-350-pricing.html |archivedate=8 July 2011 |df= }}</ref>
 |variants with their own articles=[[Cessna 400]]
}}
|}

[[File:Cessna350N2546W01.jpg|thumb|right|Columbia 350 at [[Sun 'n Fun]] 2006]]
[[File:Cessna350N2546W02.jpg|thumb|View of the nose of the aircraft]]
The '''Cessna 350 Corvalis''' is a composite construction, single-engine, normally aspirated, fixed-gear, low-wing general aviation aircraft built by [[Cessna]] Aircraft until the end of 2010.<ref name="AvWeb29Mar11" /><ref name="Cessna350">{{cite web|url=http://www.cessna.com/single-engine/cessna-350.html |title=Low Wing. High performance. |accessdate=2009-01-17 |last=[[Cessna]] |authorlink= |date=January 2009 |deadurl=yes |archiveurl=https://web.archive.org/web/20090120115629/http://www.cessna.com:80/single-engine/cessna-350.html |archivedate=2009-01-20 |df= }}</ref>

The aircraft was formerly called the '''Columbia 350''' when it was  built by [[Columbia Aircraft]].<ref name="Textron">{{cite web|url=http://investor.textron.com/phoenix.zhtml?c=110047&p=irol-newsArticle&ID=1081833&highlight= |title=Textron's Cessna Aircraft Company to Acquire Assets of Columbia Aircraft |accessdate=2007-11-28 |last=Textron |authorlink= |date=November 2007 }}{{dead link|date=January 2017 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>

==Design and development==

===Columbia 300===
Developed as the Model LC40-550FG (for ''[[Lancair]] Certified, Model 40, Continental 550 engine, Fixed Gear'') and marketed under the name '''Columbia 300''', the aircraft was [[type certificate|certified]] on September 18, 1998.<ref name="A00003SE">{{cite web|url = http://www.airweb.faa.gov/Regulatory_and_Guidance_Library/rgMakeModel.nsf/0/2c9a807e8a15cec9862573d20051899d/$FILE/A00003SE.pdf|title = TYPE CERTIFICATE DATA SHEET A00003SE Revision 22|accessdate = 2008-09-23|last = [[Federal Aviation Administration]]|authorlink = |date=January 2008}}</ref> Deliveries began in February 2000.<ref>{{cite web|url = http://www.airliners.net/aircraft-data/stats.main?id=263|title = The Lancair LC-40 Columbia 300/350/400|accessdate = 2015-09-08|last = Airliners.net|authorlink = |date = }}</ref>

The 300 is powered by a [[Continental IO-550|Teledyne Continental Model IO-550-N]] engine of 310 horsepower (230&nbsp;kW) at 2700 rpm. The aircraft's maximum takeoff weight is 3400&nbsp;lb (1542&nbsp;kg) and the maximum landing weight is 3230&nbsp;lb (1465&nbsp;kg).<ref name="A00003SE"/>

The 300 has a certified airframe life of 25,200 flight hours.<ref name="A00003SE"/>

===Columbia 350===
The Columbia 300 was upgraded with a [[glass cockpit]] and other improvements developed for the turbocharged [[Cessna 400|Columbia 400]]. It was certified on March 30, 2003 as the Model LC42-550FG (for ''[[Lancair]] Certified, Model 42, Continental 550 engine, Fixed Gear'') and marketed as the Columbia 350.<ref name="A00003SE" />

Like the 300, the 350 is powered by a [[Continental IO-550|Teledyne Continental IO-550-N]] powerplant producing 310 horsepower (230&nbsp;kW) at 2700 rpm. The 350 has the same takeoff and landing weights as the 300; maximum takeoff weight is 3400&nbsp;lb (1542&nbsp;kg) and the maximum landing weight is 3230&nbsp;lb (1465&nbsp;kg).<ref name="A00003SE" />

Like the 300 and 400, the 350 has a certified airframe life of 25200 flight hours.<ref name="A00003SE" />

Initially sold simply as the Cessna 350, the aircraft was given the marketing name ''Corvalis'' by Cessna on 14 January 2009. The name is a derivation of the town of [[Corvallis, Oregon]] which is west of the [[Bend, Oregon]] location of the Cessna plant that built the aircraft, prior to closing the plant and relocating production to [[Independence, Kansas]] in 2009.<ref name="Cessna350" /><ref name="Corvalis">{{cite web|url=http://www.cessna.com/NewReleases/FeaturedNews/NewReleaseNumber-1192260253604.html |archive-url=https://web.archive.org/web/20090118052457/http://www.cessna.com:80/NewReleases/FeaturedNews/NewReleaseNumber-1192260253604.html |dead-url=yes |archive-date=2009-01-18 |title=Cessna Debuts 350 Corvalis and 400 Corvalis TT |accessdate=2009-01-17 |last=[[Cessna]] |authorlink= |date=January 2009 }}</ref><ref name="AvWeb29Apr09">{{cite web|url = http://www.avweb.com/avwebflash/news/CessnaWillSuspendColumbusProgramAndCloseBendFactory_200284-1.html|title = Cessna Will Suspend Columbus Program, Close Bend Factory|accessdate = 2009-04-30|last = Grady|first = Mary|authorlink = |date=April 2009}}</ref><ref name="FlyingMagMay09">{{cite web|url = http://www.flyingmag.com/news/1510/cessna-closes-oregon-factory-suspends-large-jet-program.html|title = Cessna Closes Oregon Factory; Suspends Large-Jet Program|accessdate = 2009-05-11|last = Phelps|first = Mark|authorlink = |date=May 2009}}</ref>

In April 2009 Cessna announced that it would close the Bend, Oregon factory where the Cessna 350 was produced and move production to Independence, Kansas, with the composite construction moved to [[Mexico]]. The production line was restarted in October 2009 at the Cessna Independence paint facility, initially at a rate of one aircraft in six months. This was to allow the new workers, plus the 30 employees transferred from Bend, to gain experience and also allow Cessna the opportunity to retail its unsold inventory of Cessna 350s and 400s. The company had anticipated moving the 350/400 production into a permanent facility by the end of 2009.<ref name="AvWeb29Apr09" /><ref name="AvWeb09Oct09">{{cite web|url = http://www.avweb.com/avwebflash/news/cessna_corvalis_production_201287-1.htmll|title = Cessna Resumes Corvalis Production, Not In Bend |accessdate = 2009-10-12|last = Pew|first = Glenn|authorlink = |date=October 2009}} {{Dead link|date=October 2010|bot=H3llBot}}</ref>

In December 2010 a Cessna 400 that was being test flown at the factory developed a fuel leak, the cause of which was determined to be that the aircraft had "suffered a significant structural failure in the wing during a production acceptance flight test. The wing skin disbonded from the upper forward wing spar. The length of the disbond was approximately 7 feet." As a result, the FAA issued an Emergency [[Airworthiness Directive]] affecting seven Cessna 400s and one 350, all on the production line. The AD did not affect any customer aircraft in service, but did delay deliveries.<ref name="Avweb11Dec10">{{Cite news|url = http://www.avweb.com/avwebflash/news/Composite_Issue_Stops_Corvalis_Deliveries_203778-1.html|title = Composite Issue Stops Corvalis Deliveries|accessdate = 13 December 2010|last = Niles|first = Russ|authorlink = |date=December 2010| work = AvWeb}}</ref><ref name="FAAAD2010-26-53">{{Cite web|url = http://rgl.faa.gov/Regulatory_and_Guidance_Library/rgAD.nsf/0/4E1AE86FA2B07CD5862577F50076B24E?OpenDocument |title = Airworthiness Directive AD 2010-26-53 |accessdate = 13 December 2010|last = [[Federal Aviation Administration]] |authorlink = |date=December 2010}}</ref>

In March 2011 Cessna announced that the model was out of production and removed marketing information from its website.<ref name="AvWeb29Mar11" />

==Specifications (Cessna 350)==
Source: Cessna Aircraft<ref>{{cite web|url=http://www.cessna.com/single-engine/cessna-350/cessna-350-specifications.html |title=The Cessna 350 |accessdate=2008-09-24 |last=Cessna Aircraft |authorlink= |year=2008 |deadurl=yes |archiveurl=https://web.archive.org/web/20081205083333/http://www.cessna.com:80/single-engine/cessna-350/cessna-350-specifications.html |archivedate=2008-12-05 |df= }}</ref>

{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->

<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop

<!-- Now, fill out the specs.  Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For additional lines, end your alt units with a right parenthesis ")" and start a new, fully formatted line beginning with an asterisk "*" -->
|crew=one pilot
|capacity=three passengers
|length main=25 ft 2 in
|length alt=7.67 m
|span main=36 ft 1 in
|span alt=11.0 m
|height main=9 ft 0 in
|height alt=2.74 m
|area main=141 sq ft
|area alt=13.1 m²
|empty weight main=2,300 lb
|empty weight alt=1,000 kg
|loaded weight main=<!-- lb-->
|loaded weight alt=<!-- kg-->
|max takeoff weight main=3,400 lb
|max takeoff weight alt=1,500 kg
|engine (prop)=[[Teledyne Continental Motors IO-550-N]]  (TCM IO-550-N) 
|type of prop=[[flat-6|flat-6 engine]]
|number of props=1
|power main=310 hp
|power alt=230 kW
|max speed main=191 knots
|max speed alt=220 mph, 354 km/h
|range main=1,496 mi
|range alt=1,300 nmi, 2,038 km 
|ceiling main=18,000 ft
|ceiling alt=5,486 m
|climb rate main=1,500 ft/min
|climb rate alt=7.6 m/s
|loading main=25.5 lb/(sq ft)
|loading alt=125 kg/m²
|power/mass main=0.091 hp/lb
|power/mass alt=150 W/kg
}}

==References==
{{reflist|30em}}

==External links==
{{commons category-inline|Cessna 350}}

{{Cessna}}

[[Category:Cessna aircraft|350]]
[[Category:Columbia Aircraft Manufacturing Corporation aircraft|350]]
[[Category:United States civil utility aircraft 2000–2009]]
[[Category:Single-engined tractor aircraft]]
[[Category:Low-wing aircraft]]