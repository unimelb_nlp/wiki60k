{{Use dmy dates|date=April 2012}}
{{Infobox military person
|name=Albert Carter
|image=
|caption=
|birth_date=2 June 1892
|death_date={{d-da|22 May 1919|2 June 1892}}
|birth_place=[[Point de Bute, New Brunswick]], [[Canada]]
|death_place=[[Shoreham-by-Sea|Shoreham, Sussex]]
|placeofburial=Old Shoreham Cemetery
|nickname=Nick
|allegiance={{UK}}
|branch=[[Royal Flying Corps]]
|serviceyears=1911–1919
|rank= [[Major (rank)|Major]]
|unit=No. 19 Squadron
|commands=
|battles=[[World War I]]
|awards= [[Distinguished Service Order|Distinguished Service Order & Bar]]<br/>[[Croix de Guerre]] (France)
|relations=
|laterwork=
}}
'''Albert Desbrisay Carter''' [[Distinguished Service Order|DSO & Bar]] (2 June 1892 – 22 May 1919) was a [[Canada|Canadian]] [[World War I]] [[flying ace]] credited with 28 victories.<ref name="theaero">[http://www.theaerodrome.com/aces/canada/carter1.php www.thearodrome.com]. Accessed 2 September 2008.</ref>

==Early life and career==
Albert Desbrisay Carter was born in [[Point de Bute, New Brunswick]].<ref name="theaero"/><ref name="BEAces68">{{cite book |title= British and Empire Aces of World War I |page= 68 }}</ref>

Carter originally joined the 13th Reserve Battalion of the Canadian Expeditionary Force<ref name="WWI.com">[http://www.firstworldwar.com/bio/carter.htm firstworldwar.com]. Accessed 2 September 2008.</ref> and was commissioned in March 1911.<ref name="BEAces68"/> In May 1917, although he already held the rather senior rank of major, he was transferred to the [[Royal Flying Corps]] Number 1 School of Military Aeronautics. On 7 July 1917, he was remanded to Number 1 Training Squadron. Later that month he moved on to Number 20 Training Squadron. He finished his training with Number 56 Training Squadron in August.{{cn|date=November 2014}}

==Service as a fighter pilot==
On 1 October 1917, Carter received an assignment to No. 19 Squadron, where he remained until war's end.<ref name="WWI.com"/> On 31 October, he opened his career with an [[Albatros D.V]] destroyed and another German plane driven down out of battle. On 13 November 1917 he became an ace on the third of the six victories he would score that month. Also in November, he became a flight leader.<ref name="BEAces68"/> By the end of the year, on 29 December, he would score his fifteenth and final triumph flying a [[Société Pour L'Aviation et ses Dérivés|SPAD]].<ref name="theaero"/>

[[File:Dolphin (Canadian Air Force).jpg|thumb|260px|Sopwith Dolphin (Canadian markings)]]
His next victories would not come for another two and a half months. He had an opportunity when he engaged enemy two-seaters, but was thwarted by a broken gunsight on his new[[Sopwith Dolphin]].<ref>{{cite book |title= Dolphin and Snipe Aces of World War I |page= 14 }}</ref> Then, on 15 March 1918,  he destroyed one [[Pfalz D.III]] and sent another one down out of control. He would score an even dozen times flying the Dolphin, with his final success falling in flames on 16 May 1918. His final tally was 14 enemy driven down out of control and 14 destroyed. Seven of his victories were shared with other pilots. Twenty of his 28 victims were enemy fighters.<ref name="theaero"/>

On 18 March, Major Carter was shot down by German ace Lieutenant (Leutnant) [[Paul Billik]]. Carter fell behind German lines, survived the crash, and was captured. He finished his war in a prisoner of war camp. He received the [[Distinguished Service Order]], while in prison, on 18 July. It was followed by the unprecedented bestowal of a Bar,<ref name="Snipe24">{{cite book |title= Dolphin and Snipe Aces of World War I |page= 24 }}</ref> equivalent to a second award, on 16 September 1918. He was repatriated on 13 December 1918.<ref name="WWI.com"/>

==Post World War I==
Carter then joined the [[Canadian Air Force (1918-1920)|Canadian Air Force]] and was posted to No. 123 Squadron . On 22 May 1919, Carter was killed in a flying accident while test flying a [[Fokker D.VII]];<ref name="theaero"/> the plane broke up under him. He was buried at Old Shoreham Cemetery, [[Shoreham-by-Sea|Shoreham, Sussex]], England.<ref name="Snipe24"/>

==Text of citations==
===Distinguished Service Order===
"Maj. Albert Desbrisay Carter, Infy., and R.F.C.
For conspicuous gallantry and devotion to duty. He destroyed two enemy aeroplanes, drove down several others out of control, and on two occasions attacked enemy troops from a low altitude. He showed great keenness and dash as a patrol leader."{{cn|date=November 2014}}

===Bar to Distinguished Service Order===
"Maj. Albert Desbrisay Carter, D.S.O., New Brunswick R., and R.A.F.
For conspicuous gallantry and devotion to duty as a fighting pilot. In three and a half months he destroyed thirteen enemy machines. He showed the utmost determination, keenness and dash, and his various successful encounters, often against odds, make up a splendid record.{{cn|date=November 2014}}

Carter also received the [[Belgian Croix de Guerre]].<ref name="theaero"/>

==References==
{{Reflist}}

===Bibliography===
* ''British and Empire Aces of World War I''. Christopher Shores, Mark Rolfe. Osprey Publishing, 2001. ISBN 1-84176-377-2, ISBN 978-1-84176-377-4.
* ''Dolphin and Snipe Aces of World War I''. Norman Franks. Osprey Publishing, 2002. ISBN 1-84176-317-9, ISBN 978-1-84176-317-0.
{{wwi-air}}


{{DEFAULTSORT:Carter, Albert Desbrisay}}

[[Category:Canadian World War I flying aces]]
[[Category:People from Westmorland County, New Brunswick]]
[[Category:DesBrisay family|Albert Carter]]
[[Category:Royal Flying Corps officers]]
[[Category:1892 births]]
[[Category:1919 deaths]]
[[Category:Recipients of the Croix de guerre (Belgium)]]
[[Category:Canadian Expeditionary Force officers]]
[[Category:Companions of the Distinguished Service Order]]
[[Category:World War I prisoners of war held by Germany]]