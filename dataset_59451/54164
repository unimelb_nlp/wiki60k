{{Use dmy dates|date=October 2012}}
{{Infobox Australian place | type = suburb
| name     = Croydon Park
| city     = Adelaide
| state    = SA
| image    =St margaret marys.jpg
| caption  =St Margaret Mary's Primary School on Torrens Road
| lga      =City of Port Adelaide Enfield
| postcode = 5008
| est      = 
| pop      = 4,000
| pop_year = approx.
| pop_footnotes = {{citation needed|date=January 2014}}
| area     = 
| stategov = [[Electoral district of Croydon (South Australia)|Croydon]]
| fedgov   = [[Division of Adelaide|Adelaide]]
| near-nw  = [[Woodville Gardens, South Australia|Woodville Gardens]]
| near-n   = [[Ferryden Park, South Australia|Ferryden Park]]
| near-ne  = [[Regency Park, South Australia|Regency Park]]
| near-w   = [[Kilkenny, South Australia|Kilkenny]]
| near-e   = [[Dudley Park, South Australia|Dudley Park]]
| near-sw  = [[West Croydon, South Australia|West Croydon]]
| near-s   = [[Croydon, South Australia|Croydon]]
| near-se  = [[Renown Park, South Australia|Renown Park]]
| dist1    = 
| location1= 
}}
'''Croydon Park''' is a north-western [[suburb]] of [[Adelaide]] 7&nbsp;km from the [[Central Business District|CBD]], in the state of [[South Australia]], [[Australia]] and is within the [[City of Port Adelaide Enfield]]. It is adjacent to [[Dudley Park, South Australia|Dudley Park]], [[Devon Park, South Australia|Devon Park]], [[Renown Park, South Australia|Renown Park]], [[Regency Park, South Australia|Regency Park]], [[Ferryden Park, South Australia|Ferryden Park]] [[Kilkenny, South Australia|Kilkenny]], and [[West Croydon, South Australia|West Croydon]]. The post code for Croydon Park is 5008. It is bounded to the south by Lamont Street and [[Torrens Road, Adelaide|Torrens Road]], to the north by [[Regency Road, Adelaide|Regency Road]] and in the east and west by Harrison Road and Goodall Avenue respectively. Croydon Park is predominantly a residential suburb, with a warehousing presence on the northern edges near Regency Road.

== Facilities ==
The suburb is not served by a public primary school. Croydon Park Primary School was closed at the end of 1997 due to declining enrolments. The former site at Hudson Avenue was later developed into housing. The closest primary schools are Challa Gardens Primary School, to the west in Kilkenny, or Brompton Primary School in [[Brompton, South Australia|Brompton]]. However, St. Margaret Mary's, a Catholic primary school operates within the suburb. The local high school was Croydon High School (closed 2007), in adjacent West Croydon. The Douglas Mawson Institute of TAFE is located on the western end of the suburb. Polonia Reserve, the base of State League [[football (soccer)]] team [[Croydon Kings]] is located on the eastern side of Croydon Park.

==Demography==

As of 2011 Census, Croydon Park had a  population of 3,998: 49.7% female and 50.3% male. The median age of the Croydon Park population was 36 years, 1 year below the national median of 37. 47.5% of people living in Croydon Park were born in Australia. The other top responses for country of birth were Vietnam 10.4%, India 4.8%, Greece 2.9%, Italy 2.8%, China 2.1%.

40.3% of people spoke only English at home; the next most common languages were, 14.5% Vietnamese, 6.8% Greek, 4.4% Italian, 2.1% Khmer, 2% Punjabi.
The religious make up of Croydon Park is 26.1% Catholic, 15.4% No Religion, 12.5% Buddhism, 10.4% Eastern Orthodox, 4.7% Islam.

40.7% of people are married, 38.6% have never married and 9.1% are divorced and 3.1% are separated. 50.9% of the people living in Croydon Park are employed full-time, 33% are working on a part-time basis.

The main occupations of people from Croydon Park are Labourers 17.9%, Professionals 14.8%, Technicians and Trades Workers 14.3%, Clerical and Administrative Workers 13.5%, Community and Personal Service Workers 11.8%, Sales Workers 9%, Machinery Operators And Drivers 9%, and Managers 6.7%. Croydon Park has an unemployment rate of 10.6%.

== History ==
The Croydon Park area was originally a post war returned soldier housing estate. It subsequently attracted waves of European immigration. Over time, the children of these immigrants have largely vacated the area, leaving a relatively elderly population. This is reflected in the declining enrolment rates at nearby schools, with Croydon Park Primary School and Croydon Primary School in the nearby suburb of [[Croydon, South Australia|Croydon]], both being closed at the end of 1997. Today however, the area has started to attract interest from many home buyers due to its close proximity to the CBD.

== Transport ==
[[Image:Days croydon park.jpg|right|thumb|300px|Days Road]]
The 300, 230 and 232 bus routes serve Regency Road. The 230 and 232 also serve Pym Street and Days and Harrison Roads. The 251, 252, 253 and 254 serve Torrens Road. All of the bus routes except 300 travel between Adelaide's CBD and Arndale Central Shopping Centre. The 300 bus is a cross metropolitan circular bus service.

{{unreferenced|date=January 2008}}
{{coord|34.886|S|138.567|E|format=dms|type:city_region:AU-SA|display=title}}
{{City of Port Adelaide Enfield suburbs}}

[[Category:Suburbs of Adelaide]]