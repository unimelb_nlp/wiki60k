{{other uses|José de Alencar (disambiguation)}}
{{Distinguish2|former Brazilian Vice President [[José Alencar]]}}
{{Infobox writer
| name         = José de Alencar
| image        = Jose de Alencar.png
| imagesize    = 200px
| alt          =
| caption      = José de Alencar, c. 1870
| pseudonym    = Erasmo <br> Ig
| birth_name   = José Martiniano de Alencar
| birth_date   = {{birth date|1829|5|1|mf=y}}
| birth_place  = [[Fortaleza]], [[Empire of Brazil]]
| death_date   = {{death date and age|1877|12|12|1829|5|1|mf=y}}
| death_place  = [[Rio de Janeiro]], [[Empire of Brazil]]
| occupation   = [[Lawyer]], [[politician]], [[orator]], [[novelist]], [[dramatist]]
| nationality  = Brazilian
| citizenship  =
| education    =
| alma_mater   = [[University of São Paulo]]
| period       =
| genre        =
| subject      =
| movement     = [[Romanticism]]
| notableworks = ''[[The Guarani|O Guarani]]'', ''[[Senhora (novel)|Senhora]]'', ''[[Lucíola]]'', ''[[Iracema]]'', ''[[Ubirajara (novel)|Ubirajara]]''
| spouse       = Georgina Augusta Cochrane
| partner      =
| children     = Augusto de Alencar, [[Mário de Alencar]]
| relatives    = [[José Martiniano Pereira de Alencar]], [[Leonel Martiniano de Alencar, Baron of Alencar|Leonel Martiniano de Alencar]]
| influences   =
| influenced   =
| awards       =
| signature    =
| website      =
| portaldisp   =
}}
'''José Martiniano de Alencar''' (May 1, 1829 – December 12, 1877) was a Brazilian [[lawyer]], [[politician]], [[orator]], [[novelist]] and [[dramatist]]. He is considered to be one of the most famous and influential Brazilian [[Romanticism|Romantic]] novelists of the 19th century, and a major exponent of the literary tradition known as "[[Indianism (arts)|Indianism]]". Sometimes he signed his works with the [[pen name]] '''Erasmo'''.

He is patron of the 23rd chair of the [[Academia Brasileira de Letras|Brazilian Academy of Letters]].

==Biography==
[[Image:Casa José de Alencar (by Tom Junior).jpg|thumb|left|The house where José de Alencar was born and lived in until 1844, in the Messejana neighborhood, in [[Fortaleza]]]]

José Martiniano de Alencar was born in what is today the ''[[bairro]]'' of [[Messejana, Fortaleza|Messejana]], [[Fortaleza]], [[Ceará]], on May 1, 1829, to former priest (and later politician) [[José Martiniano Pereira de Alencar]] and his cousin Ana Josefina de Alencar. Moving to [[São Paulo]] in 1844, he graduated in [[Law]] at the [[Faculdade de Direito da Universidade de São Paulo]] in 1850 and started his career in law in [[Rio de Janeiro]]. Invited by his friend [[Francisco Otaviano]], he became a collaborator for the journal ''Correio Mercantil''. He also wrote many [[chronicle]]s for the ''Diário do Rio de Janeiro'' and the ''Jornal do Commercio''. Alencar would compile all the chronicles he wrote for these newspapers in 1874, under the name ''Ao Correr da Pena''.

It was in the ''Diário do Rio de Janeiro'', during the year of 1856, that Alencar gained notoriety, writing the ''Cartas sobre A Confederação dos Tamoios'', under the pseudonym '''Ig'''. In them, he bitterly criticized the [[A Confederação dos Tamoios|homonymous poem]] by [[Gonçalves de Magalhães, Viscount of Araguaia|Gonçalves de Magalhães]]. Even the Brazilian Emperor [[Pedro II of Brazil|Pedro II]], who esteemed Magalhães very much, participated in this polemic, albeit under a pseudonym. Also in 1856, he wrote and published under ''[[feuilleton]]'' form his first romance, ''[[Five Minutes (novel)|Cinco Minutos]]'', that received critical acclaim. In the following year, his breakthrough novel, ''[[The Guarani|O Guarani]]'', was released; it would be adapted into [[Il Guarany|a famous opera]] by Brazilian composer [[Antônio Carlos Gomes]] 13 years later. ''O Guarani'' would be first novel of what is informally called Alencar's "[[Indianism (arts)|Indianist]] Trilogy" – a series of three novels by Alencar that focused on the foundations of the Brazilian nation, and on its indigenous peoples and culture. The other two novels, ''[[Iracema]]'' and ''[[Ubirajara (novel)|Ubirajara]]'', would be published on 1865 and 1874, respectively. Although called a trilogy, the three books are unrelated in its plots.

Alencar was affiliated with the [[Conservative Party (Brazil)|Conservative Party of Brazil]], being elected as a general deputy for Ceará. He was the Brazilian Minister of Justice from 1868 to 1870, having famously opposed the abolition of slavery.<ref>https://www.hedra.com.br/livros/cartas-a-favor-da-escravidao</ref> He also planned to be a senator, but Pedro II never appointed him, under the pretext of Alencar being too young;<ref>RODRIGUES, Antonio Edmilson Martins; FALCON, Francisco José Calazans. ''José de Alencar: O Poeta Armado do Século XIX''. [S.l.]: FGV Editora, 2001.</ref> with his feelings hurt, he would abandon politics later.

He was very close friends with the also famous writer [[Machado de Assis]], who wrote an article in 1866 praising his novel ''[[Iracema]]'', that was published the year before, comparing his Indianist works to [[Gonçalves Dias]], saying that "Alencar was in prose what Dias was in poetry". When Assis founded the [[Academia Brasileira de Letras|Brazilian Academy of Letters]] in 1897, he chose Alencar as the patron of his chair.

[[File:Monumento a José de Alencar.jpg|thumb|right|Monument to José de Alencar in [[Rio de Janeiro]], Brazil.]]
{{Portal|Latin America|Brazil|Literature}}

In 1864 he married Georgina Augusta Cochrane, daughter of an eccentric British aristocrat. They would have six children – [[Augusto de Alencar|Augusto]] (who would be the Brazilian [[Ministry of External Relations (Brazil)|Minister of External Relations]] in 1919, and also the Brazilian ambassador on the United States from 1920 to 1924), Clarisse, Ceci, Elisa, [[Mário de Alencar|Mário]] (who would be a journalist and writer, and a member of the Brazilian Academy of Letters) and Adélia. (It is implied that Mário de Alencar was actually an illegitimate son of Machado de Assis, a fact that inspired Assis to write his famous novel ''[[Dom Casmurro]]''.<ref>[http://veja.abril.com.br/110899/p_138.html Mário de Alencar: Machado de Assis' son?] {{pt icon}}</ref>)

Alencar died in [[Rio de Janeiro]] in 1877, a victim of [[tuberculosis]]. A theatre in Fortaleza, the [[Theatro José de Alencar]], was named after him.

==Works==

===Novels===
* ''[[Five Minutes (novel)|Cinco Minutos]]'' ([[1856 in literature|1856]])
* ''[[A Viuvinha]]'' ([[1857 in literature|1857]])
* ''[[The Guarani|O Guarani]]'' ([[1857 in literature|1857]])
* ''[[Lucíola]]'' ([[1862 in literature|1862]])
* ''[[Diva (José de Alencar novel)|Diva]]'' ([[1864 in literature|1864]])
* ''[[Iracema]]'' ([[1865 in literature|1865]])
* ''[[As Minas de Prata]]'' ([[1865 in literature|1865]] — [[1866 in literature|1866]])
* ''[[O Gaúcho]]'' ([[1870 in literature|1870]])
* ''[[A Pata da Gazela]]'' ([[1870 in literature|1870]])
* ''[[O Tronco do Ipê]]'' ([[1871 in literature|1871]])
* ''[[A Guerra dos Mascates]]'' ([[1871 in literature|1871]] — [[1873 in literature|1873]])
* ''[[Til (novel)|Til]]'' ([[1871 in literature|1871]])
* ''[[Sonhos d'Ouro]]'' ([[1872 in literature|1872]])
* ''[[Alfarrábios]]'' ([[1873 in literature|1873]])
* ''[[Ubirajara (novel)|Ubirajara]]'' ([[1874 in literature|1874]])
* ''[[O Sertanejo]]'' ([[1875 in literature|1875]])
* ''[[Senhora (novel)|Senhora]]'' ([[1875 in literature|1875]])
* ''[[Encarnação (novel)|Encarnação]]'' ([[1893 in literature|1893]] — posthumous)

===Theatre plays===
* ''[[O Crédito]]'' ([[1857 in literature|1857]])
* ''[[Verso e Reverso]]'' ([[1857 in literature|1857]])
* ''[[O Demônio Familiar]]'' ([[1857 in literature|1857]])
* ''[[As Asas de um Anjo]]'' ([[1858 in literature|1858]])
* ''[[Mãe]]'' ([[1860 in literature|1860]])
* ''[[A Expiação]]'' ([[1867 in literature|1867]])
* ''[[O Jesuíta]]'' ([[1875 in literature|1875]])

===Chronicles===
* ''[[Ao Correr da Pena]]'' ([[1874 in literature|1874]])

===Autobiography===
* ''[[Como e Por Que sou Romancista]]'' ([[1873 in literature|1873]])

===Critics and polemics===
* ''Cartas sobre A Confederação dos Tamoios'' ([[1856 in literature|1856]])
* ''Cartas Políticas de Erasmo'' ([[1865 in literature|1865]] — [[1866 in literature|1866]])
* ''O Sistema Representativo'' ([[1866 in literature|1866]])

==References==
{{Reflist}}

==External links==
{{Wikisourcelang|pt|Autor:José de Alencar|José de Alencar (original works in Portuguese)}}
{{Commons category|José de Alencar}}
*{{Wikiquotelang|pt|José de Alencar}}
* {{Gutenberg author |id=Alencar,+José+Martiniano+de | name=José Martiniano de Alencar}}
* {{Internet Archive author |sname=José de Alencar |sopt=w}}
* {{Librivox author |id=274}}
* [http://www.academia.org.br/abl/cgi/cgilua.exe/sys/start.htm?sid=239 José de Alencar's biography at the official site of the Brazilian Academy of Letters] {{pt icon}}
* [http://www.portalmessejana.com.br/noticias.php?exibir=destaque&id_noticia=3561 A biography of Alencar at the official site of Messejana] {{pt icon}}

{{S-start}}
{{Succession box|title=[[File:Lorbeerkranz.png|20px]]<br> [[Academia Brasileira de Letras|Brazilian Academy of Letters]] – [[Academia Brasileira de Letras#Original patrons|Patron of the 23rd chair]]
|before=New creation|after=[[Machado de Assis]] (founder)|years=}}
{{S-end}}

{{José de Alencar}}
{{Patrons and members of the Brazilian Academy of Letters}}
{{Romanticism}}

{{Authority control}}

{{DEFAULTSORT:Alencar, Jose De}}
[[Category:José de Alencar| ]]
[[Category:1829 births]]
[[Category:1877 deaths]]
[[Category:Brazilian male novelists]]
[[Category:Brazilian male writers]]
[[Category:Brazilian lawyers]]
[[Category:Brazilian autobiographers]]
[[Category:Brazilian dramatists and playwrights]]
[[Category:Male dramatists and playwrights]]
[[Category:People from Fortaleza]]
[[Category:People from Ceará]]
[[Category:Portuguese-language writers]]
[[Category:University of São Paulo alumni]]
[[Category:Writers from Rio de Janeiro (city)]]
[[Category:19th-century Brazilian people]]
[[Category:Patrons of the Brazilian Academy of Letters]]
[[Category:19th-century deaths from tuberculosis]]
[[Category:Brazilian essayists]]