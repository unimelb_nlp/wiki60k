{{Infobox journal
| title = Annual Review of Political Science
| cover = 
| discipline = [[Political science]]
| abbreviation = Annu. Rev. Polit. Sci.
| editors =  [[Margaret Levi]]<br />[[Nancy Rosenblum]]
| publisher = [[Annual Reviews]]
| country = [[United States]]
| frequency = Annual
| history = 1998–present
| impact = 3.457
| impact-year = 2015
| website = http://www.annualreviews.org/journal/polisci
| link1 = http://www.annualreviews.org/loi/polisci
| link1-name = Online access
| ISSN = 1094-2939
| eISSN = 1545-1577
| OCLC = 42836185
| LCCN = 98643699
}}
'''''Annual Review of Political Science'''''  is an annual [[peer-reviewed]] [[academic journal]] published by [[Annual Reviews]], covering significant developments in the field of [[political science]], including political theory and philosophy, international relations, political economy, political behavior, American and comparative politics, public administration and policy, and methodology.<ref>{{cite web |title=Journal home: about this journal |url=http://www.annualreviews.org/journal/polisci |website=annualreviews.org |publisher=[[Annual Reviews]] |access-date=22 January 2016}}</ref> It was established in 1998 and its [[editors-in-chief]] are [[Margaret Levi]] ([[Center for Advanced Study in the Behavioral Sciences|Center for Advanced Study in the Behavioral Sciences, Stanford University]]) and [[Nancy Rosenblum]] ([[Harvard University]]).<ref>{{cite web |title=Editorial committee members for political science |url=http://www.annualreviews.org/action/doSearch?startPage=0&target=database&pubCode=directory&dbSearchType=staffDir&RefSeriesKey=polisci&categoryCode=edcom |website=annualreviews.org |publisher=[[Annual Reviews]] |access-date=22 January 2016}}</ref>

According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 3.457, ranking it 3rd out of 163 journals in the category "Political Science".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Political Science |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

==See also==
*[[Annual Reviews (publisher)#List of titles|Annual Reviews:List of titles]]
*[[List of political science journals]]

==References==
{{Reflist|30em}}

==External links==
*{{Official|http://www.annualreviews.org/journal/polisci }}

{{DEFAULTSORT:Annual Review of Political Science}}
[[Category:Annual Reviews academic journals|Political Science]]
[[Category:Annual journals]]
[[Category:Publications established in 1998]]
[[Category:English-language journals]]
[[Category:Political science journals]]