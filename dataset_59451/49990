{{Infobox person
| name                      = Lana Parrilla
| image                     = Lana Parrilla SDCC 2014.jpg
| image_size                = 
| alt                       = 
| caption                   = Parrilla at the 2014 Comic-Con International.
| birth_name                = Lana Maria Parrilla
| birth_date                = {{Birth date and age|1977|07|15}}
| birth_place               = [[Brooklyn]], [[New York (state)|New York]], United States
| occupation                = Actress
| years_active              = 1999–present
| parents                   = [[Sam Parrilla]]<br>Dolores Dee Azzara
| relatives                 = [[Candice Azzara]] (aunt)
| awards                    = [[Alma Award]], [[Imagen Award]]
}}
'''Lana Maria Parrilla''' (born July 15, 1977) is an American actress. Parrilla is best known for her roles on television and radio. She was a regular cast member in the fifth season of the [[American Broadcasting Company|ABC]] sitcom ''[[Spin City]]'' from 2000 to 2001. She later starred in ''[[Boomtown (2002 TV series)|Boomtown]]'' (2002-2003), ''[[Windfall (TV series)|Windfall]]'' (2006), ''[[Swingtown]]'' (2008) and as Doctor Eva Zambrano in the short-lived medical drama ''[[Miami Medical]]'' (2010). She also played the role of Sarah Gavin during the fourth season of the Fox series ''[[24 (TV series)|24]]'' in 2005. In 2011, Parrilla began starring as [[Queen (Snow White)|The Evil Queen]]/[[Regina Mills]] in the [[American Broadcasting Company|ABC]] fantasy drama series, ''[[Once Upon a Time (TV series)|Once Upon a Time]]''. In 2016 Parrilla won a [[Teen Choice Awards|Teen Choice Award]] for [[Teen Choice Award for Choice TV Actress Fantasy/Sci-Fi|Choice TV Actress: Fantasy/Sci-Fic]].

==Early life==
Parrilla was born in [[Brooklyn]]. Her father, [[Sam Parrilla]] (1943–94), was a [[Puerto Rico|Puerto Rican]]-born baseball player who played professionally for 11 seasons (1963–73), including one season with the [[Major League Baseball|Major League]] [[Philadelphia Phillies]] in 1970 as an outfielder.<ref>{{cite web |url=http://www.baseball-reference.com/players/p/parrisa01.shtml |title=Sam Parrilla Statistics and History  - Baseball-Reference.com |publisher=Sports Reference |work=Baseball-Reference.com }}</ref> Her mother is an American painter of [[Sicily|Sicilian]] descent who works in banking. Parrilla has one older sister, Deena, and a nephew named Sammy.<ref name=sister>{{cite web |url=http://onceuponatimeabc.filminspector.com/2014/03/lana-parrilla-and-her-sister-deena.html |title=Lana Parrilla and her sister Deena}}</ref> She is also the niece of character actress [[Candice Azzara]]. Parrilla's parents divorced when she was three years old. She spent her first ten years living with her mother, and then lived with her father Lana Parrilla was attacked at ten years old by a dog, which left her a visible scar on the right side of her upper lip . During the time she lived with her father, he was too protective to allow her to attend a performing arts school, which delayed her acting career.<ref name="lanaparrilladaily.net">{{cite web|url=http://www.lanaparrilladaily.net/lana/biography/ |title=Biography &#124; Lana Parrilla Daily - Your newest 24/7 Source For All Things Lana Parrilla |publisher=Lana Parrilla Daily |date= |accessdate=2015-06-07}}</ref> Parrilla lived with her father until his murder in 1994, when she was 16 and he was 50. Her father was shot once in the chest by a 15-year-old female assailant at point blank range and later died from the wound.<ref name=sister /><ref name="lanaparrilladaily.net"/> After the death of her father, Parrilla moved in with her mother in [[Burbank, California]]. Parrilla visited [[Granada]] in 2007 to learn Spanish. After high school she moved to [[Los Angeles]] and attended [[Beverly Hills Playhouse]] to study acting. She also studied voice for ten years. Parrilla then began to be cast in small parts and later on, larger ones.

==Career==

===1999—2010===
[[File:Lana Parrilla 2012-2.jpg|upright|thumb|Parrilla in March 2012]]
In her early career, Parrilla appeared in several movies, including ''[[Very Mean Men]]'' (2000), ''Spiders'' (2000), and ''Frozen Stars'' (2003). She made her television debut in 1999, on the UON sitcom ''[[Grown Ups (1999 TV series)|Grown Ups]]''. In 2000, she joined the cast of the ABC comedy series ''[[Spin City]]'', playing Angie Ordonez for one season. She left the show in 2001. After that she joined [[Donnie Wahlberg]] and [[Neal McDonough]] in the short lived crime drama ''[[Boomtown (2002 TV series)|Boomtown]]'', for which she received the [[Imagen Award]] for Best Supporting Actress,<ref name=OUAT>[http://beta.abc.go.com/shows/once-upon-a-time/bios/evil-queen BIO:Evil Queen Played by Lana Parrilla]</ref> for her portrayal of Teresa, a paramedic. Initially a success, ''Boomtown'' began to struggle, and Parrilla's character became a police academy rookie, to tie her more closely to the rest of the show. "Boomtown" was cancelled just two episodes into its second season.

Parrilla guest-starred in a number of television dramas, including ''[[JAG (TV series)|JAG]]'', ''[[Six Feet Under (TV series)|Six Feet Under]]'', ''[[Covert Affairs]]'', ''[[Medium (TV series)|Medium]]'', ''[[The Defenders (2010 TV series)|The Defenders]]'' and ''[[Chase (2010 TV series)|Chase]]''. She had a recurring role in 2004 as Officer Janet Grafton in ''[[NYPD Blue]]''. In 2005, Parrilla took a recurring guest role on the fourth season of the Fox series ''[[24 (television)|24]]'' as Sarah Gavin, a [[Counter Terrorist Unit]] agent. After just six episodes, Lana was made a regular cast member; but in the thirteenth episode, her character was written out after she  tried to thwart another character's promotion from temporary to permanent CTU head [[Michelle Dessler]] ([[Reiko Aylesworth]]).

In 2006, Parrilla starred in the [[NBC]] summer series ''[[Windfall (television series)|Windfall]]'' alongside [[Luke Perry]], fellow former ''24'' cast member [[Sarah Wynter]], and Parilla's former ''Boomtown'' castmate [[Jason Gedrick]]. In 2007, she guest starred as Greta during the third season of ABC's ''[[Lost (TV series)|Lost]]''  in the episodes "[[Greatest Hits (Lost)|Greatest Hits]]" and "[[Through the Looking Glass (Lost)|Through the Looking Glass]]" In 2008, she had a leading role on the [[Lifetime (TV network)|Lifetime]] movie ''The Double Life of Eleanor Kendall,''  in which she played Nellie, a divorcee whose identity has been stolen.<ref>[http://www.mylifetime.com/movies/the-double-life-of-eleanor-kendall MOVIES The Double Life of Eleanor Kendall]</ref> Also in 2008, she starred in the [[CBS]] summer series ''[[Swingtown]]'' as Trina Decker, a woman who is part of a [[Swinging (sexual practice)|Swinging]] couple. In 2010, Parrilla had a female lead role in the [[Jerry Bruckheimer]]-produced ''[[Miami Medical]]'' on [[CBS]], which had a short run towards the end of the 2009–10 television season before it was canceled in July 2010.<ref>{{cite web|url=http://tvseriesfinale.com/tv-show/miami-medical-canceled-season-two-15522/ |title=Miami Medical canceled, no season two &#124; canceled + renewed TV shows |publisher=TV Series Finale |date=2010-05-21 |accessdate=2014-06-03}}</ref> ''Windfall'', ''Swingtown'' and ''Miami Medical'' were all canceled after 13 episodes.

===2011—present===
[[File:Lana Parrilla 2012.jpg|left|thumb|277x277px|Parrilla in 2012]]
In February 2011, she was cast as Mayor Regina Mills/[[The Evil Queen]], in the ABC adventure fantasy drama pilot, ''[[Once Upon a Time (TV series)|Once Upon a Time]]'' created by [[Edward Kitsis]] and [[Adam Horowitz]].<ref>{{cite web|last=Andreeva |first=Nellie |url=http://www.deadline.com/2011/02/several-actors-board-broadcast-pilots/ |title=Several Actors Board Broadcast Pilots |publisher=Deadline.com |date=2011-02-28 |accessdate=2014-06-03}}</ref> The series debuted in October 2011.<ref name=OUAT/> The pilot episode was watched by 12.93 million viewers and achieved an adult 18–49 rating/share of 4.0/10 during the first season, receiving generally favorable reviews from critics.<ref>{{cite web|url=http://www.metacritic.com/tv/once-upon-a-time |title=Once Upon a Time - Season 1 Reviews |publisher=Metacritic |date= |accessdate=2014-06-03}}</ref>

Parrilla's performance also received positive reviews from critics. In 2012 and 2013, she was regarded as a promising contender for an Emmy Award in the [[Primetime Emmy Award for Outstanding Supporting Actress in a Drama Series|Outstanding Supporting Actress in a Drama Series]] category,<ref>{{cite web|author=Paula Hendrickson |work= Variety |url=http://articles.chicagotribune.com/2012-06-21/entertainment/sns-201206211800reedbusivarietynvr1118055657-20120621_1_lana-parrilla-emmys-regina |title=Lana Parrilla in 'Once Upon a Time': Road to the Emmys 2012: The Supporting Actor & Actress - Chicago Tribune |publisher=Articles.chicagotribune.com |date=2012-06-21 |accessdate=2014-06-03}}</ref><ref>{{cite web|author=07/19/2013 2:48 pm EDT |url=http://www.huffingtonpost.com/2013/07/19/emmy-latino-snubs_n_3624683.html |title=Emmy Latino Snubs For Lana Parrilla, Jimmy Smits, and More |publisher=Huffingtonpost.com |date=2013-07-19 |accessdate=2014-06-03}}</ref><ref>{{cite web|author=Marc Cuenco |url=http://tv.aol.com/2013/12/01/14-emmy-worthy-gifs-of-once-upon-a-times-lana-parilla/ |title=14 Emmy-Worthy GIFs of 'Once Upon a Time' Star Lana Parilla |publisher=Tv.aol.com |date=2013-12-01 |accessdate=2014-06-03}}</ref><ref>{{cite web|author=Team TVLine / June 6, 2012, 6:25 AM PDT |url=http://tvline.com/2012/06/06/emmy-awards-2012-supporting-drama-actress-nominations-contenders-dream-photos/ |title=[PHOTOS&#93; Emmy Nominations 2012 — Awards Show’s Supporting Actress In Drama Contenders |publisher=TVLine |date=2012-06-06 |accessdate=2014-06-03}}</ref><ref>{{cite web|last=Snetiker |first=Marc |url=http://www.hollywood.com/news/tv/34278443/2012-emmy-awards-nominees-our-predictions |title=2012 Emmy Awards Nominees: Our Predictions! &#124; TV News |publisher=Hollywood.com |date=2012-07-18 |accessdate=2014-06-03}}</ref><ref>{{cite web|url=http://uk.eonline.com/photos/5489/emmy-contenders-supporting-actress-drama/199022 |title=Lana Parrilla, Once Upon a Time from Emmy Contenders: Supporting Actress, Drama &#124; E! Online UK |publisher=Uk.eonline.com |date=2012-07-02 |accessdate=2014-06-03}}</ref> though she did not receive a nomination. She won the [[TV Guide|TV Guide Award]] for Favorite Villain and the [[ALMA Award]] for Outstanding Actress in a Drama Series in 2012.<ref>{{cite web|url=http://www.tvguide.com/News/TV-Guide-Magazine-1045829.aspx |title=TV Guide Magazine Fan Favorites Awards Winners Revealed! - Today's News: Our Take |publisher=TVGuide.com |date=2012-04-10 |accessdate=2014-06-03}}</ref><ref>{{cite web|author=09/21/2012 10:18 pm |url=http://www.huffingtonpost.com/2012/09/21/alma-awards-2012-winners-_n_1905245.html |title=ALMA Awards 2012: Winners And Show Highlights (VIDEO, PHOTOS) |publisher=Huffingtonpost.com |date=2012-09-21 |accessdate=2014-06-03}}</ref> Parrilla also received a nomination for [[Saturn Award for Best Supporting Actress on Television|Best Supporting Actress on Television]] from the [[38th Saturn Awards]].<ref>{{cite web|last=Goldberg |first=Matt |url=http://collider.com/saturn-award-nominations-2012/148931/ |title=Saturn Award Nominations 2012 &#124; Collider &#124; Page 148931 |publisher=Collider |date= |accessdate=2015-06-07}}</ref>

==Personal life==
[[File:CFC Annual BBQ Fundraiser 2014 (14993057109).jpg|thumb|Parrilla with her husband Fred Di Blasio in 2014]]
Parrilla became engaged to boyfriend Fred Di Blasio on April 28, 2013, while in Israel.<ref>{{cite web |url=http://www.latimes.com/entertainment/gossip/la-et-mg-lana-parrilla-engaged-fred-di-blasio-once-upon-a-time-20130506,0,4170875.story |title='Once Upon a Time's' Lana Parrilla engaged to Fred Di Blasio |publisher=[[Los Angeles Times]] |work=latimes.com |last=Saad |first=Nardine |date=May 7, 2013 }}</ref> The two were married July 5, 2014, shortly before Parrilla began filming the fourth season of ''Once Upon a Time.'' <ref>{{cite web|url=http://www.usmagazine.com/celebrity-news/news/lana-parrilla-married-once-upon-a-time-star-secretly-weds-2014307 |title=Lana Parrilla Is Married: Once Upon a Time Star Secretly Weds |work=[[Us Weekly]] Magazine |last=McRady |first=Rachel |date=July 30, 2014}}</ref> Parrilla confirmed the news on her Twitter account on August 1, 2014.<ref>{{cite web|url=https://twitter.com/LanaParrilla/status/495216015094337536 |title=Well, @fred_diblasio I think this says it all... Signed, #MrsDiBlasio |publisher=[[Twitter]] |work=twitter.com |last=Parrilla |first=Lana |date=August 1, 2014}}</ref>

==Filmography==

===Film===
{| class="wikitable sortable"
|-
! Year
! Title
! Role
! class="unsortable"| Notes
|-
| 2000
| ''[[Very Mean Men]]''
| Teresa
|  
|-
| 2000
| ''Spiders''
| Marci Eyre
|
|-
|-
| 2003
| ''Frozen Stars''
| Lisa Vasquez
| 
|-
| 2005
| ''One Last Ride''
| Antoinette
| 
|-
| 2008
| ''The Double Life of Eleanor Kendall''
| Nellie
| 
|}

===Television===
{| class="wikitable sortable"
|-
! Year
! Title
! Role
! class="unsortable"| Notes
|-
| 1999
| ''[[Grown Ups (1999 TV series)|Grown Ups]]''
| Waitress
| Episodes: "Truth Be Told", "J Says"
|-
| 2000
| ''[[Rude Awakening (TV series)|Rude Awakening]]''
| Nurse Lorna
| Episode: "Telltale Heart pt.1"
|-
| 2000–01
| ''[[Spin City]]''
| Angie Ordonez
| Series regular, 21 episodes
|-
| rowspan="2"| 2002
| ''[[JAG (TV series)|JAG]]''
| Lt. Stephanie Donato
| Episode: "Head to Toe"
|-
| ''[[The Shield]]''
| Sedona Tellez
| Episode: "Circles"
|-
| 2002–03
| ''[[Boomtown (2002 TV series)|Boomtown]]''
| Teresa Ortiz
|[[Imagen Award|Imagen Award for Best Supporting Actress]]
|-
| rowspan="2"| 2004
| ''[[NYPD Blue]]''
| Officer Janet Grafton
| Recurring role, 3 episodes
|-
| ''[[Six Feet Under (TV series)|Six Feet Under]]''
| Maile
| Episodes: "Terror Starts at Home", "The Dare"
|-
| 2005
| ''[[24 (TV series)|24]]''
| [[Sarah Gavin]]
| Series regular, 12 episodes
|-
| rowspan="2"| 2006
| ''[[Windfall (TV series)|Windfall]]''
| Nina Schaefer
| Series regular, 13 episodes
|-
| ''[[Lost (TV series)|Lost]]''
| [[Greta (Lost)|Greta]]
| Episodes: "[[Greatest Hits (Lost)|Greatest Hits]]", "[[Through the Looking Glass (Lost)|Through the Looking Glass]]"
|-
| rowspan="2"| 2008
| ''[[Swingtown]]''
| Trina Decker
| Series regular, 13 episodes
|-
| ''The Double Life of Eleanor Kendall''
| Nellie
| Television film
|-
| rowspan="4"| 2010
| ''[[Miami Medical]]''
| [[Miami Medical#Cast|Dr. Eva Zambrano]]
| Series regular, 13 episodes
|-
| ''[[Covert Affairs]]''
| Julia Suarez
| Episode: "South Bound Suarez"
|-
| ''[[Medium (TV series)|Medium]]''
| Lydia Halstrom
| Episode: "The Match Game"
|-
| ''[[The Defenders (2010 TV series)|The Defenders]]''
| Betty
| Episode: "Las Vegas vs. Johnson"
|-
| 2011
| ''[[Chase (2010 TV series)|Chase]]''
| Isabella
| Episode: "Narco: Part 1"
|-
| 2011–present
| ''[[Once Upon a Time (TV series)|Once Upon a Time]]''
| [[Regina Mills]] <br> [[Queen (Snow White)|The Evil Queen]]
|[[Teen Choice Award|Teen Choice Award for Choice TV Actress: Sci-Fi/Fantasy]]<br/>[[ALMA Award|ALMA Award for Outstanding TV Actress - Drama]]<br/>[[TV Guide Award|TV Guide Award for Favorite Villain]]<br/>Nominated — [[Teen Choice Award|Teen Choice Award for Choice TV Villain]]<br/>Nominated — [[Teen Choice Award|Teen Choice Award for Choice TV Villain]]<br/>Nominated — [[Saturn Award for Best Supporting Actress on Television]]<br/>Nominated — [[TV Guide Award|TV Guide Award for Favorite Villain]]
|}

==Awards and nominations==
{|class="wikitable sortable"
|-
!Year
!Award
!Nominated work
!Result
|-
|2003
|[[Imagen Award]] for Best Supporting Actress
|''[[Boomtown (2002 TV series)|Boomtown]]''
|{{won}}
|-
|2008
|[[ALMA Award]] for Outstanding Actress in a Drama Television Series
|''[[Swingtown]]''
|{{nom}}
|-
|rowspan="4"| 2012
|[[TV Guide Award]] for Favorite Villain
|''[[Once Upon a Time (TV series)|Once Upon a Time]]''
|{{won}}
|-
|[[Saturn Award for Best Supporting Actress on Television]]
|''Once Upon a Time''
|{{nom}}
|-
|[[Teen Choice Award]] for Choice TV Villain
|''Once Upon a Time''
|{{nom}}
|-
|ALMA Award for Outstanding TV Actress - Drama
|''Once Upon a Time''
|{{won}}
|-
|rowspan="3"| 2013
|NHMC Impact Award
|''Once Upon a Time''
|{{won}}
|-
|Teen Choice Awards for Choice TV Villain
|''Once Upon a Time''
|{{nominated}}
|-
|Imagen Foundation Award for Best Actress/Television
|''Once Upon a Time''
|{{nominated}}
|-
|2016
|Teen Choice Award for Choice TV Actress: Sci-Fi/Fantasy
|''Once Upon a Time''
|{{won}}
|}

==References==
{{Reflist|3}}

==External links==
{{Commons category|Lana Parrilla}}
* {{Twitter}}
* {{IMDb name|id=0663469|name=Lana Parrilla}}
* {{TV Guide person|lana-parrilla/161175|Lana Parrilla}}

{{Teen Choice Award for Choice TV Actress Fantasy/Sci-Fi}}
{{Authority control}}

{{DEFAULTSORT:Parrilla, Lana}}
[[Category:1977 births]]
[[Category:Actresses from New York City]]
[[Category:American television actresses]]
[[Category:American film actresses]]
[[Category:American people of Puerto Rican descent]]
[[Category:American people of Sicilian descent]]
[[Category:Living people]]
[[Category:People from Brooklyn]]
[[Category:20th-century American actresses]]
[[Category:21st-century American actresses]]
[[Category:Actresses of Italian descent]]
[[Category:Hispanic and Latino American actresses]]