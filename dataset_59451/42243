__NOTOC__
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=Model F
 |image=Arrow Model F.jpg
 |caption=
}}{{Infobox Aircraft Type
 |type=Recreational aircraft
 |manufacturer=[[Arrow Aircraft and Motors|Arrow Aircraft and Motor Corporation]]
 |designer=
 |first flight=1934
 |introduced=
 |retired=
 |status=
 |primary user=
 |more users=
 |produced=
 |number built=103
 |variants with their own articles=
}}
|}
The '''Arrow Model F''' or the '''Arrow Sport V-8''' was a two-seat low-wing braced [[monoplane]] aircraft built in the [[United States]] between 1934 and 1938. It was built originally to a request by the US [[Bureau of Air Commerce]] to investigate the feasibility of using automobile engines to power aircraft. Accordingly, the Model F was fitted with a modified [[Ford]] [[V8 engine]]. Like the [[Arrow Sport]] before it, the Model F seated its pilot and passenger side-by-side in an open [[cockpit]] and was marketed for $1500.<ref>{{cite web|title=Arrow Sport F|url=http://www.planeandpilotmag.com/aircraft/international-aircraft-directory/single-engine-aircraft/arrow-sport-f.html|accessdate=5 May 2011}}</ref>

A preserved example is on display at [[San Francisco International Airport]].

==Development==
The Arrow Sport F was specifically built to accommodate the low-cost, yet heavy Arrow F V-8 engine, an aircraft modification of the Ford V-8. The engine was designed by Ford Engineer David E. Anderson with an aluminum oil pan, aluminum cylnders, and a 2:1 gear reduction to drive the prop at reasonable rpm ranges. The engine weighed 402&nbsp;lbs for 85&nbsp;hp vrs 182&nbsp;lbs for an equivalent Continental aircraft engine.<ref>{{cite journal|magazine=Sport Aviation|date=5 May 1958}}</ref>
<!-- ==Operational history== -->

==Variants==
*Arrow Sport F Master – Open cockpit
*Arrow Sport F Coupe – Closed cockpit variant <ref>{{cite journal|magazine=Air Trails|date=Summer 1971|page=24}}</ref>
*Arrow Sport F De Lux Coupe – Closed cockpit with advanced instruments.
<!-- ==Aircraft markings== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications==
{{Aircraft specs
|ref=American Airplanes and Engines for 1938<ref>''Aviation'' February 1938, pp. 36–37.</ref>
|prime units?=imp
<!--
        General characteristics
-->
|genhide= 

|crew=1
|capacity=1 passenger
|length m=
|length ft=21
|length in=4
|span m=
|span ft=36
|span in=7
|height m=
|height ft=8
|height in=10
|wing area sqm=
|wing area sqft=180.5
|empty weight kg=
|empty weight lb=1097
|gross weight kg=
|gross weight lb=1675
|fuel capacity={{convert|20|USgal|impgal L|abbr=on}}
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name= Ford V-8
|eng1 type=water-cooled converted automobile engine
|eng1 kw=<!-- prop engines -->
|eng1 hp=82<!-- prop engines -->


|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=100
|max speed note=at sea level
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=90<!-- if max speed unknown -->
|cruise speed kts=
|stall speed mph=40
|range km=
|range miles=300
|range nmi=
|ceiling m=
|ceiling ft=12000
|climb rate ms=
|climb rate ftmin=800
|more performance=

|avionics=
}}

==References==
{{Reflist}}
*{{cite magazine|title=American Airplanes and Engines for 1938|magazine=[[Aviation Week & Space Technology|Aviation]] |date=February 1938 |volume=37| issue=2|pages=35–66 |url=http://archive.aviationweek.com/issue/19380201/#!&pid=34 |registration=y}}
* {{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London |pages=81 }}

==External links==
{{commons category|Arrow Model F}}
* [http://www.aerofiles.com/_al.html aerofiles.com]

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|sequence=<!-- designation sequence, if appropriate -->
|lists=<!-- related lists -->
|see also=<!-- other relevant information -->

}}

[[Category:United States sport aircraft 1930–1939]]
[[Category:Low-wing aircraft]]
[[Category:Single-engined tractor aircraft]]
[[Category:Arrow Aircraft and Motors aircraft|Model F]]