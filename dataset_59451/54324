{{Use dmy dates|date=June 2015}}
{{Use Australian English|date=June 2015}}
'''Laurence Hotham Howie''' (22 August 1876 – 18 October 1963) was a South Australian sculptor painter and art teacher.

==History==
Laurence was born in [[Norwood, South Australia]] the eldest of five children of George Cullen Howie and his wife Clara Jane Howie (née Hotham), who had emigrated from Scotland. His father died in 1883 and his mother took the children to stay with her father, the Rev. John Hotham, in [[Port Elliot, South Australia|Port Elliot]], where she ran a small private school.<ref>David Dolan, [http://adb.anu.edu.au/biography/howie-laurence-hotham-6751/text11667 'Howie, Laurence Hotham (1876–1963)'], ''Australian Dictionary of Biography'', National Centre of Biography, Australian National University, published first in hardcopy 1983, accessed online 7 February 2015</ref>

He was educated at Port Elliot and at [[Prince Alfred College]] in Adelaide, where James Ashton conducted art classes. After leaving school he enrolled in the [[School of Design (Adelaide)|School of Design]], where he soon became an assistant teacher. After qualifying as an art teacher he studied at the [[Royal College of Art]], [[South Kensington]], London, specialising in [[wood carving]] and [[china painting]].

The School of Design was taken over by the Education Department from the Board of the Public Library, Museum, and Art Gallery in 1909, and its director of over 30 years, [[H. P. Gill]], became principal of the [[Adelaide School of Art]],<ref>{{cite news |url=http://nla.gov.au/nla.news-article56733145 |title=Concerning People |newspaper=[[The Register (Adelaide)|The Register]] |location=Adelaide |date=1 July 1909 |accessdate=8 February 2015 |page=5 |publisher=National Library of Australia}}</ref> and Howie was appointed his assistant. He joined the Army engineers in 1915 and served in Egypt with the [[Royal Australian Engineers|13th Field Corps of Engineers]] in North Africa and the Western Front. He was appointed official war artist after the cessation of hostilities, and in France made a series of sketches which were later used in designing the dioramas in the [[Australian War Memorial]], Canberra.

Gill retired in July 1915 and died at sea in May 1916. [[J. Christie Wright]] succeeded him as Principal, reorganising and renaming the school as the South Australian School of Arts and Crafts.<ref>{{cite news |url=http://nla.gov.au/nla.news-article124873944 |title=School of Arts |newspaper=[[Daily Herald (Adelaide)|Daily Herald]] |location=Adelaide |date=17 April 1916 |accessdate=8 February 2015 |page=3 |publisher=National Library of Australia}}</ref> Wright was killed in 1917 and Howie was appointed director of the school, retiring in 1944, to be replaced by [[John Goodchild (painter)|John Goodchild]].

He was an active member of the [[South Australian Society of Arts]] and its president 1927–32 and 1935–37.

==Family==
He married Janet Johnstone Isabella Davidson (ca.1877 – 16 March 1948) on 17 July 1919; they had a home at 48 Foster street, Parkside. They had two daughters:
*Mary Hotham Howie (16 September 1920 – 2015)
*Janet Winifred Howie ( – ) married Keith Walkem Flint (1920–2011) of Toorak Gardens

== References ==
{{Reflist}}

{{DEFAULTSORT:Howie, Laurence Hotham}}
[[Category:Australian painters]]
[[Category:Australian sculptors]]
[[Category:1876 births]]
[[Category:1963 deaths]]
[[Category:20th-century Australian painters]]
[[Category:20th-century Australian sculptors]]
[[Category:People from Port Elliot, South Australia]]