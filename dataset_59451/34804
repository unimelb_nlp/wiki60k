{{Use British English|date=September 2012}}
{{Use dmy dates|date=September 2012}}
{{good article}}
{{Infobox Football club season
| club               = [[West Bromwich Albion F.C.|West Bromwich Albion]]
| season             = 1885–86
| manager            = None<ref group="nb">Albion did not have a secretary-manager until 1890 and did not appoint a full-time manager until 1948.</ref><ref>Matthews (2005) pp. 257–258.</ref>
| chairman           = Henry Jackson<ref>Matthews (2005) p. 267.</ref>
| stadium            = [[Stoney Lane]]
| league             = <!--N/A-->
| league result      = <!--N/A-->
| cup1               = [[FA Cup]]
| cup1 result        = Runners-up
| cup2               = [[Birmingham Senior Cup]]
| cup2 result        = Winners
| cup3               = [[Staffordshire Senior Cup]]
| cup3 result        = Winners
| league topscorer   = N/A<ref name="noleague" group="nb">There was no league football in England until [[1888–89 Football League|1888–89]].</ref><ref>McOwan pp. 19–21.</ref>
| season topscorer   = [[George Woodhall]] (13)<ref group="nb" name="StatsCriteria">Includes all matches in the [[FA Cup]] and [[Staffordshire Senior Cup]], as well as the [[Birmingham Senior Cup]] from the third round onwards.</ref><ref name="Bham Cup reserves" group="nb">Full details of the first two rounds of the Birmingham Senior Cup, which were contested by Albion's reserve team, are not available and thus not included in the statistics.</ref>
| highest attendance = 8,137 (vs [[Old Carthusians F.C.|Old Carthusians]], 23 January 1886)<ref group="nb" name="StatsCriteria"/><ref name="Bham Cup reserves" group="nb"/>
| lowest attendance  = 800 (vs [[Stoke Free Wanderers F.C.|Stoke Free Wanderers]], 30 January 1886)<ref group="nb" name="StatsCriteria"/><ref name="Bham Cup reserves" group="nb"/>
| average attendance = 3,953<ref group="nb" name="StatsCriteria"/><ref name="Bham Cup reserves" group="nb"/>
| prevseason = [[1884–85 West Bromwich Albion F.C. season|1884–85]]
| nextseason = [[1886–87 West Bromwich Albion F.C. season|1886–87]]
}}
The 1885–86 season was the eighth season in the history of [[West Bromwich Albion F.C.|West Bromwich Albion Football Club]]. In what was their inaugural season as a professional club, Albion moved to the [[Stoney Lane]] ground after leaving their previous home at Four Acres. The team also changed the colour of its [[kit (association football)|kit]], wearing blue and white striped jerseys for the first time. As league football had not been introduced in England at the time,<ref name="noleague" group="nb"/> the team competed solely in cup competitions and [[exhibition match|friendly matches]] throughout the season, playing 52 matches in total.

West Bromwich Albion won two regional cup competitions in 1885–86. They defeated [[Walsall Swifts F.C.|Walsall Swifts]] by a single goal in the replayed final of the [[Birmingham Senior Cup]], while in the [[Staffordshire Senior Cup]] a replay was again required as [[Stoke City F.C.|Stoke]] were beaten 4–2 following a goalless draw in the original tie. Albion also progressed through seven rounds to reach the [[FA Cup Final]] for the first time, becoming the first [[English Midlands|Midlands]] team to do so. However, after initially drawing 0–0 with [[Blackburn Rovers F.C.|Blackburn Rovers]], they lost 2–0 in the replay.

==Off the field==
Football was an amateur game until July 1885, when [[the Football Association]] decided to legalise payments to players. West Bromwich Albion held a committee meeting the following month, at which it was decided that the club should become professional. Albion's first professionals earned 10 [[Shilling (British coin)|shillings]] (50 [[Penny (British decimal coin)|pence]]) per week, with no training allowance provided.<ref>Matthews (1987) p. 11.</ref>

After deciding not to renew the lease on the [[West Bromwich Albion F.C. former grounds|Four Acres]], Albion moved to their fifth ground, [[Stoney Lane]], in time for the 1885–86 season. The ground was located close to the Plough and Harrow [[public house]], which served as the club's headquarters at the time.<ref name="Inglis">Inglis p. 173.</ref> Albion took out an initial seven-year lease on the Stoney Lane site, paying an annual rent of [[pound sterling|£]]28 to its owner, a local [[undertaker]] named Mr Webb.<ref name="Inglis"/> The pitch was returfed and levelled, with ashes spread around the perimeter, and a wooden grandstand was built that came to be known as 'Noah's Ark'.<ref name="Matthews 2007 p66">Matthews (2007) p. 66.</ref> The ground cost £370 to build, a sum that was offset by a number of friendly matches that took place early in the season.<ref name="Matthews 2007 p66"/>

During their early years, West Bromwich Albion had played in whatever coloured [[Kit (association football)|kit]] was available locally. However, at a committee meeting held in September 1885, the club decreed that blue and white striped jerseys should be adopted on a permanent basis. With the exception of a brief trial of scarlet and blue broad stripes early in the 1889–90 season, the team have worn these colours ever since,<ref>Matthews (1987) p. 241.</ref> although initially the blue was of a lighter shade; the navy blue stripes were not introduced until after the [[First World War]].<ref name="Historical Kits">{{cite web
| title = Historical Football kits&nbsp;– West Bromwich Albion
| url = http://www.historicalkits.co.uk/West_Bromwich_Albion/West_Bromwich_Albion.htm
| publisher = historicalkits.co.uk
| accessdate =26 February 2010}}</ref><ref>Morris p. 12.</ref>

Due to the club's financial situation, the [[reserve team]] had their wages halved early on in the season, and by January 1886 the payments made to reserve players were withdrawn altogether, although there was a possibility of them receiving a bonus at the end of the season if funds permitted. This resulted in Albion's second team refusing to play against Small Heath Alliance and the game was cancelled. Some of the players were suspended as a result of their actions, but were later re-instated.<ref>Matthews (1987) p. 243.</ref>

==FA Cup==
{{See also|1885–86 FA Cup|1886 FA Cup Final}}
[[File:Jem Bayliss.jpg|thumb|upright|right|[[Jem Bayliss]] scored Albion's first FA Cup hat-trick.]]
In their third season in the [[FA Cup]], West Bromwich Albion were [[home advantage|drawn at home]] in every round prior to the semi-final. In the first two rounds, they defeated [[Aston Unity F.C.|Aston Unity]] 4–1 and [[Wednesbury Old Athletic F.C.|Wednesbury Old Athletic]] 3–2. The team then received a [[bye (sports)|bye]] to the fourth round, where they beat [[Wolverhampton Wanderers F.C.|Wolverhampton Wanderers]] by a 3–1 scoreline. [[Old Carthusians F.C.|Old Carthusians]] were defeated by a single goal in the fifth round. A [[hat-trick]] from [[Jem Bayliss]]—the first by an Albion player in the FA Cup<ref>Matthews (2007) pp. 392–393.</ref>—contributed to a 6–0 quarter-final victory over [[Old Westminsters F.C.|Old Westminsters]], putting the club into the FA Cup semi-final for the first time.

The semi-final took place at [[Aston Lower Grounds]] and was against one of Albion's local rivals, [[Birmingham City F.C.|Small Heath Alliance]]. Albion won 4–0—[[Arthur Loach]] and [[George Woodhall]] each scoring twice—to become the first [[English Midlands|Midlands]] club to reach the [[FA Cup Final]].<ref>Matthews (1987) p. 201.</ref> After the game, Small Heath supporters invaded the pitch and then pelted missiles at vehicles bound for West Bromwich, causing several injuries.<ref name="McOwan p17">McOwan p. 17.</ref> In [[1886 FA Cup Final|the final]], Albion met holders [[Blackburn Rovers F.C.|Blackburn Rovers]] at the [[The Oval|Kennington Oval]], drawing 0–0. No [[extra time]] was played,<ref name="McOwan p17"/> so a replay was arranged at [[County Cricket Ground, Derby|Derby Cricket Ground]], the first time that an FA Cup Final match had taken place outside London.<ref>Matthews (1987) p. 171.</ref> Albion were beaten 2–0 as Rovers lifted the FA Cup for the third year in succession. This was the first of three successive FA Cup Finals in which West Bromwich Albion participated.

{| class="wikitable"
|+
!Round!!Date!!Opponent!!Venue!!Result<ref group="nb" name="Score order">West Bromwich Albion's score is listed first, regardless of the venue or result.</ref>!!Goalscorers!!Attendance
|-
|align="center"|1||align="right"|31 October 1885||[[Aston Unity F.C.|Aston Unity]]||align="center"|[[Stoney Lane|H]]||align="center" bgcolor="PaleGreen"|4–1||[[Tommy Green (footballer born 1863)|T Green]] 2, [[George Woodhall|Woodhall]] 2||align="right"|4,027
|-
|align="center"|2||align="right"|21 November 1885||[[Wednesbury Old Athletic F.C.|Wednesbury Old Athletic]]||align="center"|H||align="center" bgcolor="PaleGreen"|3–2||[[Arthur Loach|Loach]] 2, [[George Bell (footballer)|G Bell]]||align="right"|3,578
|-
|align="center"|3||align="center" colspan="6"|Albion received a [[bye (sports)|bye]] to round four
|-
|align="center"|4||align="right"|2 January 1886||[[Wolverhampton Wanderers F.C.|Wolverhampton Wanderers]]||align="center"|H||align="center" bgcolor="PaleGreen"|3–1||[[George Bell (footballer)|G Bell]], [[Tommy Green (footballer born 1863)|T Green]], [[Arthur Loach|Loach]]||align="right"|5,196
|-
|align="center"|5||align="right"|23 January 1886||[[Old Carthusians F.C.|Old Carthusians]]||align="center"|H||align="center" bgcolor="PaleGreen"|1–0||[[George Bell (footballer)|G Bell]]||align="right"|8,137
|-
|align="center"|6||align="right"|13 February 1886||[[Old Westminsters F.C.|Old Westminsters]]||align="center"|H||align="center" bgcolor="PaleGreen"|6–0||[[Jem Bayliss|Bayliss]] 3, [[George Bell (footballer)|G Bell]] 2, [[George Woodhall|Woodhall]]||align="right"|5,884
|-
|align="center"|SF||align="right"|6 March 1886||[[Birmingham City F.C.|Small Heath Alliance]]||align="center"|[[Aston Lower Grounds|N]]||align="center" bgcolor="PaleGreen"|4–0||[[Arthur Loach|Loach]] 2, [[George Woodhall|Woodhall]] 2||align="right"|4,100
|-
|align="center"|F||align="right"|3 April 1886||[[Blackburn Rovers F.C.|Blackburn Rovers]]||align="center"|[[The Oval|N]]||align="center" bgcolor="Khaki"|0–0||—||align="right"|15,156
|-
|align="center"|F([[Replay (sports)|R]])||align="right"|10 April 1886||[[Blackburn Rovers F.C.|Blackburn Rovers]]||align="center"|[[County Cricket Ground, Derby|N]]||align="center" bgcolor="Pink"|0–2||—||align="right"|16,144
|-
|}

<small>Source for match details:</small><ref>Matthews (1987) p. 157.</ref>

==Birmingham Senior Cup==

Due to a congested fixture list, Albion fielded a reserve side in the first two rounds of their fifth [[Birmingham Senior Cup]] campaign, beating [[Sparkhill Alliance F.C.|Sparkhill Alliance]] 6–0 and [[Burton Swifts F.C.|Burton Swifts]] 4–1. [[Tommy Green (footballer born 1863)|Tommy Green]] scored a [[hat-trick]] in the third round match away at [[Notts Rangers F.C.|Notts Rangers]], as Albion ran out 7–2 winners. [[Port Vale F.C.|Burslem Port Vale]] were defeated 5–0 in the semi-final, which took place at a neutral venue, [[Aston Lower Grounds]]. The team drew 1–1 with [[Walsall Swifts F.C.|Walsall Swifts]] in the final, which was also at Aston Lower Grounds. The replay took place at the same venue and [[George Woodhall]] scored the only goal of the game as Albion won the trophy for the first time.<ref name="Birmingham Cup">Matthews (1987) pp. 202–203.</ref>

{| class="wikitable"
|+
!Round!!Date!!Opponent!!Venue!!Result<ref group="nb" name="Score order"/>!!Goalscorers!!Attendance
|-
|align="center"|1<ref name="Bham Cup reserves" group="nb"/>||align="right"|1885||[[Sparkhill Alliance F.C.|Sparkhill Alliance]]||align="center"|?||align="center" bgcolor="PaleGreen"|6–0||?||align="right"|?
|-
|align="center"|2<ref name="Bham Cup reserves" group="nb"/>||align="right"|1885||[[Burton Swifts F.C.|Burton Swifts]]||align="center"|?||align="center" bgcolor="PaleGreen"|4–1||?||align="right"|?
|-
|align="center"|3||align="right"|5 December 1885||[[Notts Rangers F.C.|Notts Rangers]]||align="center"|A||align="center" bgcolor="PaleGreen"|7–2||[[George Woodhall|Woodhall]], [[Tommy Green (footballer born 1863)|T Green]] 3, [[Arthur Loach|Loach]] 2, [[George Bell (footballer)|G Bell]]||align="right"|520
|-
|align="center"|SF||align="right"|16 January 1886||[[Port Vale F.C.|Burslem Port Vale]]||align="center"|[[Aston Lower Grounds|N]]||align="center" bgcolor="PaleGreen"|5–0||[[Tommy Green (footballer born 1863)|T Green]] 2, [[George Timmins|Timmins]], [[Arthur Loach|Loach]], [[Fred Bunn|Bunn]]||align="right"|3,000
|-
|align="center"|F||align="right"|13 March 1886||[[Walsall Swifts F.C.|Walsall Swifts]]||align="center"|[[Aston Lower Grounds|N]]||align="center" bgcolor="Khaki"|1–1||[[Tommy Green (footballer born 1863)|T Green]]||align="right"|4,000
|-
|align="center"|F([[Replay (sports)|R]])||align="right"|12 April 1886||[[Walsall Swifts F.C.|Walsall Swifts]]||align="center"|[[Aston Lower Grounds|N]]||align="center" bgcolor="PaleGreen"|1–0||[[George Woodhall|Woodhall]]||align="right"|10,000
|}

<small>Source for match details:</small><ref name="Birmingham Cup"/>

==Staffordshire Senior Cup==
Albion, taking part in the [[Staffordshire Senior Cup]] for the fourth time, defeated [[Stafford Rangers F.C.|Stafford Rangers]] 4–0 in the first round replay (following a goalless draw). In round two, a [[George Woodhall]] hat-trick helped to achieve a 5–2 win against [[Leek F.C.|Leek]], after which Albion were handed a [[bye (sports)|bye]] to the fourth round. They then beat [[Stoke Free Wanderers F.C.|Stoke Free Wanderers]] 5–0, while in the semi-final [[Burton Wanderers F.C.|Burton Wanderers]] were defeated 3–0 at [[Stoke City F.C.|Stoke]]'s [[Victoria Ground]]. The final took place at the same venue as Stoke themselves provided the opposition. The match finished goalless and Albion won the replay 4–2 at Stoney Lane in front of 5,500 supporters. Two goals from [[Jem Bayliss]] and one each from [[Tommy Green (footballer born 1863)|Tommy Green]] and George Woodhall gave Albion their second victory in the competition.<ref name="Staffs Cup">Matthews (1987) pp. 205–206.</ref>

{| class="wikitable"
|+
!Round!!Date!!Opponent!!Venue!!Result<ref group="nb" name="Score order"/>!!Goalscorers!!Attendance
|-
|align="center"|1||align="right"|24 October 1885||[[Stafford Rangers F.C.|Stafford Rangers]]||align="center"|[[Stoney Lane|H]]||align="center" bgcolor="Khaki"|0–0||—||align="right"|3,000
|-
|align="center"|1([[Replay (sports)|R]])||align="right"|7 November 1885||[[Stafford Rangers F.C.|Stafford Rangers]]||align="center"|A||align="center" bgcolor="PaleGreen"|4–0||[[Arthur Loach|Loach]] 2, [[George Bell (footballer)|G Bell]] 2||align="right"|2,500
|-
|align="center"|2||align="right"|9 January 1886||[[Leek F.C.|Leek]]||align="center"|H||align="center" bgcolor="PaleGreen"|5–2||[[Arthur Loach|Loach]], [[George Woodhall|Woodhall]] 3, [[George Bell (footballer)|G Bell]]||align="right"|1,000
|-
|align="center"|3||align="center" colspan="6"|Albion received a [[bye (sports)|bye]] to round four
|-
|align="center"|4||align="right"|30 January 1886||[[Stoke Free Wanderers F.C.|Stoke Free Wanderers]]||align="center"|H||align="center" bgcolor="PaleGreen"|5–0||[[George Bell (footballer)|G Bell]], [[Jem Bayliss|Bayliss]], [[Arthur Loach|Loach]], [[George Woodhall|Woodhall]] 2||align="right"|800
|-
|align="center"|SF||align="right"|17 April 1886||[[Burton Wanderers F.C.|Burton Wanderers]]||align="center"|[[Victoria Ground|N]]||align="center" bgcolor="PaleGreen"|3–0||[[Jem Bayliss|Bayliss]] 2, [[Tommy Green (footballer born 1863)|T Green]]||align="right"|6,000
|-
|align="center"|F||align="right"|3 April 1886||[[Stoke City F.C.|Stoke]]||align="center"|A||align="center" bgcolor="Khaki"|0–0||—||align="right"|3,000
|-
|align="center"|F([[Replay (sports)|R]])||align="right"|10 May 1886||[[Stoke City F.C.|Stoke]]||align="center"|H||align="center" bgcolor="PaleGreen"|4–2||[[Jem Bayliss|Bayliss]] 2, [[Tommy Green (footballer born 1863)|T Green]], [[George Woodhall|Woodhall]]||align="right"|5,500
|}

<small>Source for match details:</small><ref name="Staffs Cup"/>

==Friendlies and benefit matches==

After the move to Stoney Lane, Albion held four [[exhibition game]]s to help cover the cost of the ground.<ref name="Willmore p19">Willmore p. 19.</ref> The first of these was a 4–1 win against [[Third Lanark A.C.|Third Lanarkshire Rifle Volunteers]] on 5 September 1885,<ref group="nb" name="Third Lanark">Willmore (p. 19.) records the date of the game as 4 September.</ref> in front of 2,122 spectators. [[Tommy Green (footballer born 1863)|Tommy Green]] scored a hat-trick, including the first goal to be scored at Stoney Lane.<ref name="Willmore p19"/><ref>Matthews (1987) p. 214.</ref> As league football had yet to be established, the club also played in a number of other [[friendly match]]es throughout the season. Their 31 friendly matches included 21 wins and 9 defeats, while one match against [[Bolton Wanderers F.C.|Bolton Wanderers]] was abandoned with the game still scoreless. Albion's joint biggest friendly victories of the season were their 7–0 wins against [[Birmingham City F.C.|Small Heath Alliance]], [[Aston Unity F.C.|Aston Unity]] and [[Halliwell F.C.|Halliwell]]. The season's heaviest friendly defeat was the 0–7 reverse at home to [[Preston North End F.C.|Preston North End]]. Albion also took part in the [[Walsall Senior Cup]] and [[Birmingham Charity Cup]], but entered a reserve team for both competitions.<ref>Matthews (1987) pp. 204 & 207.</ref>

{| class="wikitable"
|+
!Date!!Opponent!!Venue!!Result<ref group="nb" name="Score order"/>
|-
|align="right"|27 July 1885||[[Wednesbury Old Athletic F.C.|Wednesbury Old Athletic]]||align="center"|A||align="center" bgcolor="Pink"|0–1
|-
|align="right"|22 August 1885||[[Birmingham City F.C.|Small Heath Alliance]]||align="center"|A||align="center" bgcolor="PaleGreen"|7–0
|-
|align="right"|5 September 1885<ref group="nb" name="Third Lanark" /><ref name="Willmore p19"/>||[[Third Lanark A.C.|Third Lanarkshire Rifle Volunteers]]||align="center"|H||align="center" bgcolor="PaleGreen"|4–1
|-
|align="right"|12 September 1885||[[Aston Villa F.C.|Aston Villa]]||align="center"|H||align="center" bgcolor="PaleGreen"|5–0
|-
|align="right"|19 September 1885||[[Birmingham Excelsior F.C.|Birmingham Excelsior]]||align="center"|H||align="center" bgcolor="PaleGreen"|4–2
|-
|align="right"|21 September 1885||[[Walsall F.C.|Walsall Swifts]]||align="center"|A||align="center" bgcolor="Pink"|0–3
|-
|align="right"|26 September 1885||[[Wednesbury Old Athletic F.C.|Wednesbury Old Athletic]]||align="center"|H||align="center" bgcolor="PaleGreen"|2–1
|-
|align="right"|3 October 1885||[[Northwich Victoria F.C.|Northwich Victoria]]||align="center"|A||align="center" bgcolor="Pink"|1–2
|-
|align="right"|5 October 1885||[[Great Bridge Unity F.C.|Great Bridge Unity]]||align="center"|A||align="center" bgcolor="PaleGreen"|4–0
|-
|align="right"|10 October 1885||[[Blackburn Olympic F.C.|Blackburn Olympic]]||align="center"|A||align="center" bgcolor="PaleGreen"|3–2
|-
|align="right"|17 October 1885||[[Stoke City F.C.|Stoke]]||align="center"|H||align="center" bgcolor="PaleGreen"|3–1
|-
|align="right"|2 November 1885||[[Wolverhampton Wanderers F.C.|Wolverhampton Wanderers]]||align="center"|H||align="center" bgcolor="PaleGreen"|3–0
|-
|align="right"|14 November 1885||[[Notts County F.C.|Notts County]]||align="center"|A||align="center" bgcolor="Pink"|3–4
|-
|align="right"|23 November 1885||[[Burnley F.C.|Burnley]]||align="center"|H||align="center" bgcolor="Pink"|0–3
|-
|align="right"|28 November 1885||[[Aston Villa F.C.|Aston Villa]]||align="center"|A||align="center" bgcolor="PaleGreen"|5–4
|-
|align="right"|12 December 1885||[[Derby Midland F.C.|Derby Midland]]||align="center"|A||align="center" bgcolor="PaleGreen"|5–3
|-
|align="right"|19 December 1885||[[Aston Unity F.C.|Aston Unity]]||align="center"|H||align="center" bgcolor="PaleGreen"|7–0
|-
|align="right"|26 December 1885||[[Blackburn Olympic F.C.|Blackburn Olympic]]||align="center"|H||align="center" bgcolor="PaleGreen"|4–0
|-
|align="right"|28 December 1885||[[Bolton Wanderers F.C.|Bolton Wanderers]]||align="center"|H||align="center" bgcolor="Silver"|0–0<ref group="nb">Match abandoned</ref>
|-
|align="right"|6 February 1886||[[Aston Villa F.C.|Aston Villa]]||align="center"|H||align="center" bgcolor="PaleGreen"|3–2
|-
|align="right"|20 February 1886||[[Nottingham Forest F.C.|Nottingham Forest]]||align="center"|H||align="center" bgcolor="PaleGreen"|1–0
|-
|align="right"|27 February 1886||[[Derby Junction F.C.|Derby Junction]]||align="center"|H||align="center" bgcolor="PaleGreen"|5–0
|-
|align="right"|20 March 1886||[[Notts County F.C.|Notts County]]||align="center"|H||align="center" bgcolor="PaleGreen"|3–0
|-
|align="right"|27 March 1886||[[Stoke City F.C.|Stoke]]||align="center"|A||align="center" bgcolor="PaleGreen"|3–1
|-
|align="right"|19 April 1886||[[Aston Villa F.C.|Aston Villa]]<ref group="nb">Benefit match for Albion and Villa players</ref>||align="center"|A||align="center" bgcolor="Pink"|1–3
|-
|align="right"|26 April 1886||[[Halliwell F.C.|Halliwell]]||align="center"|H||align="center" bgcolor="PaleGreen"|7–0
|-
|align="right"|1 May 1886||[[Preston North End F.C.|Preston North End]]||align="center"|H||align="center" bgcolor="Pink"|0–7
|-
|align="right"|8 May 1886||[[Blackburn Rovers F.C.|Blackburn Rovers]]||align="center"|H||align="center" bgcolor="Pink"|2–5
|-
|align="right"|15 May 1886||[[Preston North End F.C.|Preston North End]]||align="center"|H||align="center" bgcolor="PaleGreen"|1–0
|-
|align="right"|22 May 1886||[[Bolton Wanderers F.C.|Bolton Wanderers]]||align="center"|A||align="center" bgcolor="Pink"|1–3
|-
|align="right"|29 May 1886||[[Aston Villa F.C.|Aston Villa]]||align="center"|H||align="center" bgcolor="PaleGreen"|3–1
|-
|}

<small>Source for match details:</small><ref>Matthews (1987) pp. 210 & 214.</ref>

==See also==
*[[1885–86 in English football]]

==Footnotes==
{{reflist|group="nb"}}

==References==
;Citations
{{reflist|2}}

;Sources
*{{cite book | last = Inglis | first = Simon | title = The Football Grounds of England and Wales | origyear = 1983 | edition = Paperback| year = 1984 | publisher = Collins Willow}}<!--No ISBN for this book-->
*{{cite book| last=Morris | first=Peter | title=West Bromwich Albion: Soccer in the Black Country | publisher=Heinemann | year=1965}}<!--No ISBN for this book-->
*{{cite book | last=McOwan | first=Gavin | title=The Essential History of West Bromwich Albion | publisher=Headline|year=2002 |isbn=0-7553-1146-9}}
*{{cite book | last = Matthews | first = Tony |author2=Mackenzie, Colin | title = Albion! A Complete Record of West Bromwich Albion 1879–1987  | publisher = Breedon Books | year = 1987 | isbn = 0-907969-23-2}}
*{{cite book | last = Matthews | first = Tony | title = The Who's Who of West Bromwich Albion | publisher = Breedon Books | year = 2005 | isbn = 1-85983-474-4}}
*{{cite book | last=Matthews | first=Tony | title=West Bromwich Albion: The Complete Record | publisher=Breedon Books| year=2007 | isbn=978-1-85983-565-4}}
*{{cite book| last=Willmore |first=G.A. | title=West Bromwich Albion: The First Hundred Years | publisher=Readers Union | year=1980}}

{{West Bromwich Albion F.C. seasons}}
{{1885–86 in English football}}

{{DEFAULTSORT:1885-86 West Bromwich Albion F.C. season}}
[[Category:West Bromwich Albion F.C. seasons]]
[[Category:English football clubs 1885–86 season|West Bromwich Albion]]