{{Infobox royalty
|name            =Sviatoslav the Brave
|title           =Grand Prince of Kiev
|image           =Sviatoslav_by_Eugene_Lanceray_1886.JPG
| image_size = 250 px
|caption         =Sviatoslav I by Eugene Lanceray (1886)
|reign           =945–972
|coronation      =964
|othertitles     =
|full name       =Sviatoslav Igorevich
|predecessor     =[[Igor of Kiev|Igor]]
|successor       =[[Yaropolk I of Kiev|Yaropolk I]]
|spouse 1        =Predslava
|spouse 2        =[[Malusha]]
|spouse 3        =
|spouse 4        =
|spouse 5        =
|spouse 6        =
|issue           =''With unknown woman:''<br> [[Yaropolk I of Kiev|Yaropolk I]]<br>[[Oleg of Drelinia|Oleg]]<br><br>  ''With [[Malusha]]:''<br> [[Vladimir I of Kiev|Vladimir the Great]]
|dynasty         =[[Rurik Dynasty]]
|royal anthem    =
|father          =[[Igor of Kiev|Igor]]
|mother          =[[Olga of Kiev|Saint Olga]] <small>(regent 945-964)</small>
|birth_date      =942?
|birth_place     =[[Kiev]]
|death_date      =March 972 [aged ~30]
|death_place     =[[Khortytsia|The island of Khortytsa]] [[Dnieper]]
|date of burial  =
|place of burial =
}}  '''Sviatoslav I Igorevich''' ([[Old East Slavic]]: С~тославъ / Свѧтославъ<ref>{{cite web|url=http://litopys.org.ua/ipatlet/ipat04.htm |title=E.g. in the '&#39;Primary Chronicle'&#39; under year 970 |publisher=Litopys.org.ua |date= |accessdate=2013-07-06}}</ref> Игорєвичь, ''Sventoslavŭ / Svantoslavŭ Igorevičǐ''; [[Old Norse language|Old Norse]]:  ''Sveinald Ingvarsson'') (c. 942 – March 972), also spelled '''Svyatoslav''' was a Grand prince of Kiev<ref>{{cite web|url=http://www.britannica.com/EBchecked/topic/576078/Svyatoslav-I |title=Svyatoslav I (prince of Kiev) |publisher=Britannica.com |date= |accessdate=2012-06-17}}</ref><ref>{{cite web|url=http://www.britannica.com/EBchecked/topic/631547/Vladimir-I |title=Vladimir I (grand prince of Kiev)|publisher=Britannica.com |date= |accessdate=2012-06-17}}</ref> famous for his persistent campaigns in the east and south, which precipitated the collapse of two great powers of Eastern Europe, [[Khazars|Khazaria]] and the [[First Bulgarian Empire]]. He also conquered numerous [[East Slavs|East Slavic]] tribes, defeated the [[Alans]] and attacked the [[Volga Bulgaria|Volga Bulgars]],<ref>A History of Russia: Since 1855, Walter Moss, pg 29</ref><ref>[http://www.world-history.ru/countries_about/572/2054.html Khazarian state and its role in the history of Eastern Europe and the Caucasus] [[:ru:Новосельцев, Анатолий Петрович|A.P. Novoseltsev]], [[Moscow]], [[Nauka (publisher)|Nauka]], 1990. {{ru icon}}</ref> and at times was allied with the [[Pechenegs]] and [[Hungarian people|Magyars]].

His decade-long reign over the [[Kievan Rus']] was marked by rapid expansion into the [[Volga River]] valley, the [[Pontic-Caspian steppe|Pontic steppe]], and the [[Balkans]]. By the end of his short life, Sviatoslav carved out for himself the largest state in [[Europe]], eventually moving his capital in 969 from [[Kiev]] (modern-day Ukraine) to [[Pereyaslavets]] (identified as the modern village of [[Nufăru]], [[Romania]]<ref>{{cite book|last1=Stephenson|first1=Paul|title=Byzantium's Balkan Frontier: A Political Study of the Northern Balkans, 900-1204|date=2000|publisher=Cambridge University Press|isbn=9780521770170|page=56|url=https://books.google.com.au/books?id=ILiOI0UgxHoC&printsec=frontcover&dq=Pereyaslavets++Nuf%C4%83ru,+Romania&hl=en&sa=X&ved=0CCIQ6AEwAWoVChMI8s_loNy3yAIVJTGmCh18AQj3#v=onepage&q=%20Pereyaslavets&f=false}}</ref>) on the [[Danube]]. In contrast with his mother's conversion to [[History of Christianity#High Middle Ages (800–1299)|Christianity]], Sviatoslav remained a staunch [[paganism|pagan]] all of his life. Due to his abrupt death in ambush, his conquests, for the most part, were not consolidated into a functioning empire, while his failure to establish a stable succession led to a [[fratricide|fratricidal]] feud among his three sons, resulting in two of them being killed.

==Name==
The ''[[Primary Chronicle]]'' records Sviatoslav as the first ruler of the [[Kievan Rus']] with a name of [[Slavic languages|Slavic]] origin (as opposed to his predecessors, whose names had [[Old Norse]] forms). The name ''Sviatoslav'', however, is not recorded in other medieval Slavic countries. Nevertheless, ''Sveinald'' is the [[Old East Norse]] cognate with the Slavic form as attested in the Old East Norse patronymic of Sviatoslav's son [[Vladimir I of Kiev|Vladimir]]: ''Valdamarr Sveinaldsson''. This [[patronymic]] naming convention continues in [[Icelandic language|Icelandic]] and in [[East Slavic languages]]. Even in Rus', it was attested only among the members of the [[Rurik Dynasty|house of Rurik]], as were the names of Sviatoslav's immediate successors: [[Vladimir I of Kiev|Vladimir]], [[Yaroslav I the Wise|Yaroslav]], and [[Mstislav I of Kiev|Mstislav]].<ref>
{{cite book
| last1 = Литвина
| first1 = А. Ф.
| last2 = Успенский
| first2 = Федор Борисович
| title = Выбор имени у русских князей в X-XVI вв: династическая история сквозь призму антропонимики
| trans-title = The choice of personal names for the Russian princes of the 10th-16th centuries: a dynastic history through the prism of anthroponymy
| url = https://books.google.com/books?id=J2VmAAAAMAAJ
| series = Труды по филологии и истории: Именослов, имя
| language = Russian
| location = Moscow
| publisher = Индрик [Indrik]
| publication-date = 2006
| page = 43
| isbn = 5-85759-339-5
| access-date = 2016-08-25
| quote = 
}}
</ref>{{qn|date=August 2016}} Some scholars see the name of Sviatoslav, composed of the Slavic roots for "holy" and "glory", as an artificial derivation combining the names of his predecessors [[Oleg of Novgorod|Oleg]] and [[Rurik]] (whose names mean "holy" and "glorious" in [[Old Norse]], respectively).<ref>
See {{lang|ru|А.М. Членов. К вопросу об имени Святослава, in Личные имена в прошлом, настоящем и будущем: проблемы антропонимики}} (Moscow, 1970).
</ref>

==Early life and personality==
Virtually nothing is known about Sviatoslav's childhood and youth, which he spent reigning in [[Velikiy Novgorod|Novgorod]]. Sviatoslav's father, [[Igor, Grand Prince of Kiev|Igor]], was killed by the [[Drevlyans|Drevlians]] around 945, and his mother, [[Olga of Kiev|Olga]], ruled as [[regent]] in [[Kiev]] until Sviatoslav reached maturity (ca. 963).<ref>If Olga was indeed born in 879, as the [[Primary Chronicle]] seems to imply, she should have been about 65 at the time of Sviatoslav's birth. There are clearly some problems with chronology.</ref> Sviatoslav was tutored by a [[Varangians|Varangian]] named Asmud.<ref>''Primary Chronicle'' entry for 968</ref> The tradition of employing Varangian tutors for the sons of ruling princes survived well into the 11th century. Sviatoslav appears to have had little patience for administration. His life was spent with his ''[[druzhina]]'' (roughly, "company") in permanent warfare against neighboring states. According to the Primary Chronicle, he carried on his expeditions neither wagons nor kettles, and he boiled no meat, rather cutting off small strips of horseflesh, game, or beef to eat after roasting it on the coals. Nor did he have a tent, rather spreading out a horse-blanket under him and setting his saddle under his head, and all his retinue did likewise.<ref>Cross and Sherbowitz-Wetzor, ''Primary Chronicle'', p. 84.</ref>

[[File:Slav warrior from Solntsev book.jpg|upright|thumb|Illustration of Sviatoslav wearing a [[vyshyvanka]], by [[Fedor Solntsev]]]]
Sviatoslav's appearance has been described very clearly by [[Leo the Deacon]], who himself attended the meeting of Sviatoslav with [[John I Tzimiskes]]. Following Deacon's memories, Sviatoslav was a [[Eye color#Blue|blue-eyed]] male of average height but of stalwart build, much more sturdy than Tzimiskes. He shaved his blond head and his beard<!-- (or possibly just had a wispy beard) --> but wore a bushy mustache and a [[chupryna|sidelock]] as a sign of his nobility.<ref>For the alternative translations of the same passage of the Greek original that say that Sviatoslav may have not shaven but wispy beard and not one but two sidelocks on each side of his head, see e.g. Ian Heath "The Vikings (Elite 3)", Osprey Publishing 1985; ISBN 978-0-85045-565-6, p.60 or David Nicolle "Armies of Medieval Russia 750–1250 (Men-at-Arms 333)" Osprey Publishing 1999; ISBN 978-1-85532-848-8, p.44</ref> He preferred to dress in white, and it was noted that his garments were much cleaner than those of his men, although he had a lot in common with his warriors. He wore a single large gold earring bearing a [[Carbuncle (gemstone)|carbuncle]] and two [[pearl]]s.<ref>Vernadsky 276–277. The sidelock is reminiscent of Turkic hairstyles and practices and was later mimicked by [[Cossack]]s.</ref>

==Religious beliefs==
Sviatoslav's mother, [[Olga of Kiev|Olga]], converted to [[Eastern Orthodoxy|Eastern Orthodox Christianity]] at the court of [[Byzantine Empire|Byzantine]] Emperor [[Constantine Porphyrogenitus]] in 957,<ref>Based on his analysis of ''[[De Ceremoniis]]'', [[Alexander Nazarenko]] hypothesizes that Olga hoped to orchestrate a marriage between Sviatoslav and a Byzantine princess. If her proposal was peremptorily declined (as it most certainly would have been), it is hardly surprising that Sviatoslav would look at the Byzantine Empire and her Christian culture with suspicion. Nazarenko 302.</ref>  at the approximate age of 67. However, Sviatoslav remained a [[Paganism|pagan]] all of his life. In the treaty of 971 between Sviatoslav and the Byzantine emperor John I Tzimiskes, the Rus' are swearing by [[Perun]] and [[Veles (god)|Veles]].<ref name="Balzer1992">{{cite book|last1=Froianov|first1=I. Ia.|author2=A. Iu. Dvornichenko|author3=Iu. V. Krivosheev|editor=Marjorie Mandelstam Balzer|title=Russian Traditional Culture: Religion, Gender, and Customary Law|url=https://books.google.com/books?id=YJmqeYPEbdwC&pg=PA4|accessdate=19 February 2017|year=1992|publisher=M.E. Sharpe|isbn=978-1-56324-039-3|page=4|chapter=The Introduction of Christianity in Russia and the Pagan Traditions}}</ref> According to the Primary Chronicle, he believed that his warriors ([[druzhina]]) would lose respect for him and mock him if he became a Christian.<ref>Primary Chronicle _____.</ref> The allegiance of his warriors was of paramount importance in his conquest of an empire that stretched from the Volga to the Danube.

==Family==
[[Image:The mother of the Russian sovereign Svjatoslav, Olga along with her escort from the Chronicle of John Skylitzes.jpg|thumb|left|Svjatoslav's mother, Olga, with her escort in [[Constantinople]], a miniature from the late 11th century chronicle of [[John Skylitzes]].]]
Very little is known of Sviatoslav's family life. It is possible that he was not the only (or the eldest) son of his parents. The [[Rus'-Byzantine Treaty (945)|Russo-Byzantine treaty of 945]] mentions a certain Predslava, Volodislav's wife, as the noblest of the Rus' women after Olga. The fact that Predslava was Oleg's mother is presented by [[Vasily Tatishchev]]. He also speculated that Predslava was of a Hungarian nobility. [[George Vernadsky]] was among many historians to speculate that Volodislav was Igor's eldest son and heir who died at some point during Olga's regency. Another chronicle told that Oleg (? - 944?) was the eldest son of Igor. At the time of Igor's death, Sviatoslav was still a child,<!--"a baby", following the Primary Chronicle.--> and he was raised by his mother or under her instructions. Her influence, however, did not extend to his religious observance.

[[File:Svatoslav titularnik.png|thumb|upright|Sviatoslav I in the ''Tsarsky Titulyarnik'', 1672]]
Sviatoslav had several children, but the origin of his wives is not specified in the chronicle. By his wives, he had [[Yaropolk I of Kiev|Yaropolk]] and [[Oleg of Drelinia|Oleg]].<ref>Shared maternal paternity of Yaropolk and Oleg is a matter of debate by historians.</ref> By [[Malusha]], a woman of indeterminate origins,<ref>She is traditionally identified in Russian historiography as [[Dobrynya]]'s sister; for other theories on her identity, [[Malusha|see here]].</ref> Sviatoslav had [[Vladimir I of Kiev|Vladimir]], who would ultimately break with his father's paganism and [[Christianization of Kievan Rus'|convert Rus' to Christianity]]. [[John Skylitzes]] reported that Vladimir had a brother named [[Sfengus]]<!--Sven?-->; whether this Sfengus was a son of Sviatoslav, a son of Malusha by a prior or subsequent husband, or an unrelated Rus' nobleman is unclear.<ref>Indeed, Franklin and Shepard advanced the hypothesis that Sfengus was identical with [[Mstislav of Tmutarakan]]. Franklin and Shepard 200-201.<!--what is the exact Greek term used by Skylitzes? the language is notorious for its lack of precise terms concerning kinship.--></ref>

==Eastern campaigns==
[[Image:Sviatoslav1.png|thumb|left|The Kievan Rus' at the beginning of Sviatoslav's reign (in red), showing his sphere of influence to 972 (in orange)]]
Shortly after his accession to the throne, Sviatoslav began campaigning to expand Rus' control over the Volga valley and the [[Pontic-Caspian steppe|Pontic steppe]] region. His greatest success was the conquest of [[Khazars|Khazaria]], which for centuries had been one of the strongest states of [[Eastern Europe]]. The sources are not clear about the roots of the conflict between Khazaria and Rus', so several possibilities have been suggested. The Rus' had an interest in removing the Khazar hold on the [[Volga trade route]] because the Khazars collected duties from the goods transported by the Volga. Historians have suggested that the Byzantine Empire may have incited the Rus' against the Khazars, who fell out with the Byzantines after the persecutions of the [[History of the Jews in Russia and the Soviet Union#Early history|Jew]]s in the reign of [[Romanos I|Romanus I Lecapenus]].<ref name="Rus">"Rus", ''Encyclopaedia of Islam''</ref>

Sviatoslav began by rallying the [[East Slavs|East Slavic]] vassal tribes of the Khazars to his cause. Those who would not join him, such as the [[Vyatichs]], were attacked and forced to pay tribute to the Kievan Rus' rather than to the Khazars.<ref>Christian 345. It is disputed whether Sviatoslav invaded the land of Vyatichs that year. The only campaign against the Vyatichs explicitly mentioned in the Primary Chronicle is dated to 966.</ref> According to a legend recorded in the Primary Chronicle, Sviatoslav sent a message to the Vyatich rulers, consisting of a single phrase: "I want to come at you!" ([[Old East Slavic]]: "хощю на вы ити")<ref>[http://litopys.org.ua/ipatlet/ipat03.htm Russian Primary Chronicle (ПСРЛ. — Т. 2. Ипатьевская летопись. — СПб., 1908) for year 6472.] The chronicler may have wished to contrast Sviatoslav's open declaration of war to stealthy tactics employed by many other early medieval conquerors.</ref> This phrase is used in modern Russian (usually misquoted as "Иду на вы") and in modern Ukrainian ("Іду на ви") to denote an unequivocal declaration of one's intentions. Proceeding by the [[Oka River|Oka]] and Volga rivers, he attacked [[Volga Bulgaria]]. He employed [[Oghuz Turks|Oghuz]] and [[Pechenegs|Pecheneg]] mercenaries in this campaign, perhaps to counter the superior [[cavalry]] of the Khazars and Bulgars.<ref>For Sviatoslav's reliance on nomad cavalry, ''see, e.g.'', Franklin and Shepard 149; Christian 298; Pletneva 18.</ref>

[[File:Svyatoslav chorikovasky.jpg|thumb| ''Sviatoslav's Council of War'' by [[Boris Chorikov]] ]]
Sviatoslav destroyed the Khazar city of [[Sarkel]] around 965, possibly sacking (but not occupying) the Khazar city of [[Kerch]] on the [[Crimea]] as well.<ref>Christian 298. The Primary Chronicle is very succinct about the whole campaign against the Khazars, saying only that Sviatoslav "took their city and Belaya Vezha".</ref> At Sarkel he established a Rus' settlement called Belaya Vyezha ("the white tower" or "the white fortress", the East Slavic translation for "Sarkel").<ref>The town was an important trade center located near the [[portage]] between the Volga and [[Don River (Russia)|Don Rivers]]. By the early 12th century, however, it had been destroyed by the [[Kipchaks]].</ref> He subsequently destroyed the Khazar capital of [[Atil]].<ref>''See, generally'' Christian 297–298; Dunlop ''passim''.</ref> A visitor to Atil wrote soon after Sviatoslav's campaign: "The Rus' attacked, and no grape or raisin remained, not a leaf on a branch."<ref>Logan (1992), p. 202</ref> The exact chronology of his Khazar campaign is uncertain and disputed; for example, [[Mikhail Artamonov]] and [[David Christian (historian)|David Christian]] proposed that the sack of Sarkel came after the destruction of Atil.<ref>Artamonov 428; Christian 298.</ref>

Although [[Ibn Hawqal|Ibn Haukal]] reports the sack of [[Samandar (city)|Samandar]] by Sviatoslav, the Rus' leader did not bother to occupy the Khazar heartlands north of the [[Caucasus Mountains]] permanently. On his way back to Kiev, Sviatoslav chose to strike against the [[Ossetians]] and force them into subservience.<ref>The campaign against the Ossetians is attested in the Primary Chronicle. The [[Novgorod First Chronicle]] specifies that Sviatoslav resettled the Ossetians near Kiev, but Sakharov finds this claim dubitable.</ref> Therefore, Khazar successor statelets continued their precarious existence in the region.<ref>The [[Mandgelis Document]] refers to a Khazar potentate in the [[Taman Peninsula]] around 985, long after Sviatoslav's death. [[Georgios Kedrenos|Kedrenos]] reported that the Byzantines and Rus' collaborated in the conquest of a Khazar kingdom in the Crimea in 1016, and still later, [[Ali ibn al-Athir|Ibn al-Athir]] reported an unsuccessful attack by [[Al-Fadhl ibn Muhammad|al-Fadl ibn Muhammad]] against the Khazars in the Caucasus in 1030. For more information on these and other references, ''see'' [[Khazars#Late references to the Khazars]].</ref> The destruction of Khazar imperial power paved the way for Kievan Rus' to dominate north-south trade routes through the steppe and across the [[Black Sea]], routes that formerly had been a major source of revenue for the Khazars. Moreover, Sviatoslav's campaigns led to increased Slavic settlement in the region of the [[Saltovo-Mayaki]] culture, greatly changing the demographics and culture of the transitional area between the forest and the steppe.<ref>Christian 298.</ref>

==Campaigns in the Balkans==
{{main article|Sviatoslav's invasion of Bulgaria}}
[[File:63-manasses-chronicle.jpg|thumb|left|upright|Sviatoslav invading Bulgaria, Manasses Chronicle]]
The annihilation of Khazaria was undertaken against the background of the Rus'-Byzantine alliance, concluded in the wake of [[Rus'-Byzantine War (941)|Igor's Byzantine campaign]] in 944.<ref>Most historians believe the Greeks were interested in the destruction of Khazaria. Another school of thought essentializes the report of [[Yahya of Antioch]] that, prior to the Danube campaign, the Byzantines and the Rus' were at war. See Sakharov, chapter I.</ref> Close military ties between the Rus' and Byzantium are illustrated by the fact, reported by John Skylitzes, that a Rus' detachment accompanied Byzantine Emperor [[Nikephoros II|Nikephoros Phokas]] in his victorious naval expedition to [[Crete]].

In 967 or 968,<ref>The exact date of Sviatoslav's Bulgarian campaign, which likely did not commence until the conclusion of his Khazar campaign, is unknown.</ref> Nikephoros sent to Sviatoslav his agent, [[Kalokyros]], with the task of talking Sviatoslav into assisting him in a war against [[First Bulgarian Empire|Bulgaria]].<ref>[[Mikhail Tikhomirov]] and [[Vladimir Pashuto]], among others, assume that the Emperor was interested primarily in diverting Sviatoslav's attention from [[Chersonesos Taurica|Chersonesos]], a Byzantine possession in the [[Crimea]]. Indeed, Leo the Deacon three times mentions that Sviatoslav and his father Igor controlled [[Strait of Kerch|Cimmerian Bosporus]]. If so, a conflict of interests in the Crimea was inevitable. The Suzdal Chronicle, though a rather late source, also mentions Sviatoslav's war against Chersonesos. In the [[Siege of Dorostolon|peace treaty of 971]], Sviatoslav promised not to wage wars against either [[Constantinople]] or Chersonesos. Byzantine sources also report that Kalokyros attempted to persuade Sviatoslav to support Kalokyros in a coup against the reigning Byzantine emperor. As remuneration for his help, Sviatoslav was supposed to retain a permanent hold on Bulgaria. Modern historians, however, assign little historical importance to this story. Kendrick 157.</ref> Sviatoslav was paid 15,000 pounds of gold and set sail with an army of 60,000 men, including thousands of Pecheneg mercenaries.<ref>All figures in this article, including the numbers of Sviatoslav's troops, are based on the reports of Byzantine sources, which may differ from those of the Slavonic chronicles. Greek sources report Khazars and "Turks" in Sviatoslav's army as well as Pechenegs. As used in such Byzantine writings as ''[[De Administrando Imperio]]'' by Constantine Porphyrogenitus, "Turks" refers to [[Hungarian people|Magyars]]. The Rus'-Magyar alliance resulted in the Hungarian expedition against the second largest city of the empire, [[Thessalonica]], in 968.</ref><ref name="Treadgold509">W. Treadgold, ''A History of the Byzantine State and Society'', 509</ref>

[[Battle of Silistra|Sviatoslav defeated]] the Bulgarian ruler [[Boris II of Bulgaria|Boris II]]<ref>Boris II was captured by the Byzantines in 971 and carried off to [[Constantinople]] as a prisoner.</ref> and proceeded to occupy the whole of northern Bulgaria. Meanwhile, the Byzantines bribed the Pechenegs to [[Siege of Kiev (968)|attack and besiege Kiev]], where Olga stayed with Sviatoslav's son Vladimir. The siege was relieved by the ''druzhina'' of [[Siege of Kiev (968)|Pretich]], and immediately following the Pecheneg retreat, Olga sent a reproachful letter to Sviatoslav. He promptly returned and defeated the Pechenegs, who continued to threaten Kiev.

{{Campaignbox Russo-Byzantine Wars}}

Sviatoslav refused to turn his Balkan conquests over to the Byzantines, and the parties fell out as a result. To the chagrin of his [[boyar]]s and his mother (who died within three days after learning about his decision), Sviatoslav decided to move his capital to [[Pereyaslavets]] in the mouth of the Danube due to the great potential of that location as a commercial hub. In the Primary Chronicle record for 969, Sviatoslav explains that it is to Pereyaslavets, the centre of his lands, "all the riches flow: gold, silks, wine, and various fruits from [[Greece]], silver and horses from [[Hungary]] and [[Bohemia]], and from Rus' furs, wax, honey, and slaves".

[[Image:Persecution of Russ by the Byzantine army John Skylitzes.jpg|thumb|Pursuit of Sviatoslav's warriors by the Byzantine army, a miniature from 11th century chronicles of [[John Skylitzes]].]]
In summer 969, Sviatoslav left Rus' again, dividing his dominion into three parts, each under a nominal rule of one of his sons. At the head of an army that included Pecheneg and Magyar auxiliary troops, he invaded Bulgaria again, devastating [[Thrace]], capturing the city of [[Plovdiv|Philippopolis]], and massacring its inhabitants. Nikephoros responded by repairing the defenses of Constantinople and raising new squadrons of armored cavalry. In the midst of his preparations, Nikephoros was overthrown and killed by [[John I Tzimiskes|John Tzimiskes]], who thus became the new Byzantine emperor.<ref>Kendrick 158</ref>

[[Image:Svyatoslav2.jpg|thumb|left|''[[Madrid Skylitzes]]'', meeting between [[John I Tzimiskes|John Tzimiskes]] and Sviatoslav.]]
John Tzimiskes first attempted to persuade Sviatoslav into leaving Bulgaria, but he was unsuccessful. Challenging the Byzantine authority, Sviatoslav crossed the Danube and laid siege to [[Edirne|Adrianople]], causing panic on the streets of Constantinople in summer 970.<ref>Simultaneously, [[Otto I, Holy Roman Emperor|Otto I]] attacked Byzantine possessions in the south of Italy. This remarkable coincidence may be interpreted as an evidence of the anti-Byzantine German-Russian alliance. See: Manteuffel 41.</ref> Later that year, the Byzantines launched a counteroffensive. Being occupied with suppressing a revolt of [[Bardas Phokas the Younger|Bardas Phokas]] in [[Asia Minor]], John Tzimiskes sent his commander-in-chief, [[Bardas Skleros]], who defeated the coalition of Rus', Pechenegs, Magyars, and Bulgarians in the [[Battle of Arcadiopolis (970)|Battle of Arcadiopolis]].<ref>Grekov 445–446. The Byzantine sources report the enemy casualties to be as high as 20,000, a figure modern historians find to be highly improbable.</ref> Meanwhile, John, having quelled the revolt of Bardas Phokas, came to the Balkans with a large army and promoting himself as the liberator of Bulgaria from Sviatoslav, penetrated the impracticable mountain passes and shortly thereafter captured [[Marcianopolis]], where the Rus' were holding a number of Bulgar princes hostage.

[[File:64-manasses-chronicle.jpg|thumb|Siege of Durostorum in Manasses Chronicle]]
Sviatoslav retreated to [[Silistra|Dorostolon]], which the Byzantine armies [[Siege of Dorostolon|besieged for sixty-five days]]. Cut off and surrounded, Sviatoslav came to terms with John and agreed to abandon the Balkans, renounce his claims to the southern Crimea, and return west of the [[Dnieper River]]. In return, the Byzantine emperor supplied the Rus' with food and safe passage home. Sviatoslav and his men set sail and landed on [[Berezan Island]] at the mouth of the Dnieper, where they made camp for the winter. Several months later, their camp was devastated by famine, so that even a horse's head could not be bought for less than a half-[[Ukrainian hryvnia|grivna]], reports the Kievan chronicler of the Primary Chronicle.<ref>Franklin and Shepard 149–150</ref> While Sviatoslav's campaign brought no tangible results for the Rus', it weakened the Bulgarian statehood and left it vulnerable to the attacks of [[Basil II|Basil the Bulgar-Slayer]] four decades later.

==Death and aftermath==
[[Image:Konchina Svyatoslava.jpg|thumb|left|upright|''The Death of Sviatoslav'' by [[Boris Chorikov]]]]
Fearing that the peace with Sviatoslav would not endure, the Byzantine emperor induced the Pecheneg [[Khan (title)|khan]] [[Kurya (khan)|Kurya]] to kill Sviatoslav before he reached Kiev. This was in line with the policy outlined by [[Constantine VII]] Porphyrogenitus in ''[[De Administrando Imperio]]'' of fomenting strife between the Rus' and the Pechenegs.<ref>Constantine VII pointed out that, by virtue of their controlling the Dnieper cataracts, the Pechenegs may easily attack and destroy the Rus' vessels sailing along the river.</ref> According to the Slavic chronicle, [[Sveneld]] attempted to warn Sviatoslav to avoid the [[Khortytsia|Dnieper rapids]], but the prince slighted his wise advice and was ambushed and slain by the Pechenegs when he tried to cross the cataracts near [[Khortytsia|Khortitsa]] early in 972. The Primary Chronicle reports that his skull was [[skull cup|made into a chalice]] by the Pecheneg khan.<ref>The use of a defeated enemy's skull as a drinking vessel is reported by numerous authors through history among various steppe peoples, such as the [[Scythians]]. Kurya likely intended this as a compliment to Sviatoslav; sources report that Kurya and his wife drank from the skull and prayed for a son as brave as the deceased Rus' warlord. Christian 344; Pletneva 19; Cross and Sherbowitz-Wetzor 90.</ref>

Following Sviatoslav's death, tensions between his sons grew. A war broke out between his legitimate sons, Oleg and [[Yaropolk I of Kiev|Yaropolk]], in 976, at the conclusion of which Oleg was killed. In 977 [[Vladimir I of Kiev|Vladimir]] fled Novgorod to escape Oleg's fate and went to [[Scandinavia]], where he raised an army of [[Varangians]] and returned in 980. Yaropolk was killed, and Vladimir became the sole ruler of Kievan Rus'.

==Art and literature==
[[Image:Akimov 1773.jpg|thumb|[[Ivan Akimov]]. ''Sviatoslav's Return from the Danube to His Family in Kiev'' (1773)]]
Sviatoslav has long been a hero of [[Belarusians|Belarusian]], [[Russians|Russian]], and [[Ukrainians|Ukrainian]] patriots due to his great military successes. His figure first attracted attention of Russian artists and poets during the [[Russo-Turkish War (1768–1774)]], which provided obvious parallels with Sviatoslav's push towards Constantinople. Russia's southward expansion and the imperialistic ventures of [[Catherine II of Russia|Catherine II]] in the Balkans seemed to have been legitimized by Sviatoslav's campaigns eight centuries earlier.

Among the works created during the war was [[Yakov Knyazhnin]]'s tragedy ''Olga'' (1772). The Russian playwright chose to introduce Sviatoslav as his protagonist, although his active participation in the events following Igor's death is out of sync with the traditional chronology. Knyazhnin's rival [[Nikolai Nikolev]] (1758–1815) also wrote a play on the subject of Sviatoslav's life. [[Ivan Akimov]]'s painting ''Sviatoslav's Return from the Danube to Kiev'' (1773) explores the conflict between military honour and family attachment. It is a vivid example of [[Nicolas Poussin|Poussinesque]] rendering of early medieval subject matter.

Interest in Sviatoslav's career increased in the 19th century. [[Klavdiy Lebedev]] depicted an episode of Sviatoslav's meeting with [[John I Tzimiskes|Emperor John]] in his well-known painting, while [[Eugene Lanceray]] sculpted an [[equestrian sculpture|equestrian statue]] of Sviatoslav in the early 20th century.<ref name=Lanceray>E. A Lanceray. ''"[http://www.sgu.ru/rus_hist/?wid=699 Sviatoslav on the way to [[Tsargrad]].]"'', [http://www.sgu.ru/rus_hist/ The Russian History in the Mirror of the Fine Arts] {{ru icon}}</ref> Sviatoslav appears in the 1913 poem of [[Velimir Khlebnikov]] ''Written before the war'' (#70. Написанное до войны)<ref>{{cite web|url=http://lib.rus.ec/b/142777/read |title=Велимир Хлебников Творения |publisher=Lib.rus.ec |date= |accessdate=2012-06-17}}</ref> as an epitome of militant Slavdom:

{| style="margin:auto;"
|-
|Знаменитый сок Дуная, ||| Pouring the famed juice of the Danube
|-
|Наливая в глубь главы, ||| Into the depth of my head,
|-
|Стану пить я, вспоминая ||| I shall drink and remember
|-
|Светлых клич: "Иду на вы!". ||| The cry of the bright ones: "I come at you!"<ref>Cooke, Raymond Cooke. ''Velimir Khlebnikov: A Critical Study''. Cambridge University Press, 1987. Pages 122–123</ref>
|}

Sviatoslav is the villain of the novel ''The Lost Kingdom, or the Passing of the Khazars'', by Samuel Gordon,<ref>London: Shapiro, Vallentine, 1926</ref> a fictionalized account of the destruction of Khazaria by the Rus'. The Slavic warrior figures in a more positive context in the story "Chernye Strely Vyaticha" by Vadim Viktorovich Kargalov; the story is included in his book ''Istoricheskie povesti''.<ref>(Moscow: Det. lit., 1989).</ref>

In 2005, reports circulated that a village in the [[Belgorod]] region had erected a monument to Sviatoslav's victory over the Khazars by the Russian sculptor [[Vyacheslav Klykov]]. The reports described the 13-meter tall statue as depicting a Rus' cavalryman trampling a supine Khazar bearing a [[Star of David]] and [[Kolovrat (symbol)|Kolovrat]]. This created an outcry within the [[History of the Jews in Russia and the Soviet Union|Jewish community]] of Russia. The controversy was further exacerbated by Klykov's connections with [[Pamyat]] and other anti-Semitic organizations, as well as by his involvement in the "letter of 500", a controversial appeal to the Prosecutor General to review all Jewish organizations in Russia for extremism.<ref>[http://www.xeno.sova-center.ru/6BA2468/6BB4208/706B4D8?print=on Alexander Verkhovsky. Anti-Semitism in Russia: 2005. Key Developments and New Trends<!-- Bot generated title -->]</ref> The Press Center of the Belgorod Regional Administration responded by stating that a planned monument to Sviatoslav had not yet been constructed but would show "respect towards representatives of all nationalities and religions."<ref>[http://www.interfax-religion.com/?act=news&div=600 "The Federation of Jewish Communities protests against the presence of a Star of David in a new sculpture in Belgorod"], ''Interfax'', November 21, 2005; Kozhevnikova, Galina, [http://xeno.sova-center.ru/6BA2468/6BB4208/6E811ED "Radical nationalism and efforts to oppose it in Russia in 2005"]; [http://www.fjc.ru/news/newsArticle.asp?AID=329123 "FJC Russia Appeal Clarifies Situation Over Potentially Anti-Semitic Monument"] ([[Federation of Jewish Communities of the CIS]] Press Release), November 23, 2005; Dahan, David, "Jews protest trampled Star of David statue", ''European Jewish Press'', November 22, 2005</ref> When the statue was unveiled, the shield bore a twelve-pointed star.

Svyatoslav is the main character of the books "Knyaz" ("Князь") and "The Hero" ("Герой"), written by Russian writer [[Alexander Mazin]].

On 7 November 2011 Ukrainian fisherman Sergei Pjankow fished up a one metre long frankish sword from the waters of the Dnieper not far from the spot where Svyatoslav is believed to have been killed in 972. The handle is made out of four different metals including gold and silver, and it is very possible that it belonged to Sviatoslav himself.<ref>[http://www.magazine-rest.in.ua/en/news/on_khortitsa_found_the_sword_of_prince_svyatoslav.html On Khortitsa found the sword of Prince Svyatoslav] Rest in Ukraine</ref>

==See also==
*[[List of Russian rulers]]
*[[List of Ukrainian rulers]]

==References==
*[[Mikhail Artamonov|Artamonov, Mikhail]] ''Istoriya Khazar''. [[Leningrad]], 1962.
*[[Wilhelm Barthold|Barthold, W.]]. "Khazar". ''[[Encyclopaedia of Islam]]'' (Brill Online). Eds.: P. Bearman, Th. Bianquis, C. E. Bosworth, E. van Donzel and W.P. Heinrichs. Brill, 1996.
*Chertkov A. D. ''{{lang|ru-Latn|Opisanie voin velikago kniazya Svyatoslava Igorevicha}}''. Moscow, 1843.
*Chlenov, A. M. ({{lang|ru|А. М. Членов}}.) "{{lang|ru-Latn|K Voprosu ob Imeni Sviatoslava}}." ''{{lang|ru-Latn|Lichnye Imena v proshlom, Nastoyaschem i Buduschem Antroponomiki}}'' ("{{lang|ru|К вопросу об имени Святослава". ''Личные имена в прошлом, настоящем и будущем: проблемы антропонимики}}'') (Moscow, 1970).
*[[David Christian (historian)|Christian, David]]. ''A History of Russia, Mongolia and Central Asia.'' Blackwell, 1999.
*Cross, S. H., and O. P. Sherbowitz-Wetzor. ''The Russian Primary Chronicle: Laurentian Text''. Cambridge, Mass.: Medieval Academy of America, 1953.
*[[Dunlop, D. M.]] ''History of the Jewish Khazars.'' Princeton Univ. Press, 1954.
*Franklin, Simon and [[Jonathan Shepard]]. ''The Emergence of Rus 750-1200.'' London: Longman, 1996. ISBN 0-582-49091-X.
*[[Peter Benjamin Golden|Golden, P. B.]] "Rus." ''[[Encyclopaedia of Islam]]'' (Brill Online). Eds.: P. Bearman, Th. Bianquis, C. E. Bosworth, E. van Donzel and W. P. Heinrichs. Brill, 2006.
*[[Boris Grekov|Grekov, Boris]]. ''Kiev Rus''. tr. Sdobnikov, Y., ed. Ogden, Denis. Moscow: Foreign Languages Publishing House, 1959
* {{citation | last = Hanak | first = Walter K. | contribution = The Infamous Svjatoslav: Master of Duplicity in War and Peace? | title = Peace and War in Byzantium: Essays in Honor of George T. Dennis, S.J. | editor1-first = Timothy S. |editor1-last = Miller | editor2-first = John | editor2-last = Nesbitt | publisher = The Catholic University of America Press | year = 1995 | url = http://www.deremilitari.org/RESOURCES/ARTICLES/hanak.htm | isbn = 978-0-8132-0805-3 }}
*Kendrick, Thomas D. ''A History of the Vikings''. Courier Dover Publications, 2004. ISBN 0-486-43396-X
*Logan, Donald F. ''The Vikings in History'' 2nd ed. Routledge, 1992. ISBN 0-415-08396-6
*Manteuffel Th. "{{lang|fr|Les tentatives d'entrainement de la Russie de Kiev dans la sphere d'influence latin}}". ''{{lang|la|Acta Poloniae Historica}}.'' Warsaw, t. 22, 1970.
*[[Alexander Nazarenko|Nazarenko, A. N.]] ({{lang|ru|А.Н. Назаренко}}). ''{{lang|ru-Latn|Drevniaya Rus' na Mezhdunarodnykh Putiakh}}'' (''{{lang|ru|Древняя Русь на международных путях}}''). Moscow, Russian Academy of Sciences, World History Institute, 2001. ISBN 5-7859-0085-8.
*[[Pletneva, Svetlana]]. ''Polovtsy'' Moscow: Nauka, 1990.
*[[Andrey Nikolayevich Sakharov|Sakharov, Andrey]]. ''The Diplomacy of Svyatoslav''. Moscow: [[Nauka (publisher)|Nauka]], 1982. ([http://www.hrono.ru/libris/saharov00.html online])
*[[Orest Subtelny|Subtelny, Orest]]. ''Ukraine: A History''. Toronto: University of Toronto Press, 1988. ISBN 0-8020-5808-6
*[[Vernadsky, G. V.]] ''The Origins of Russia.'' Oxford: Clarendon Press, 1959.

==Notes==
{{Commons category}}
{{reflist|30em}}
{{refend}}
{{refbegin|30em}}
{{refend}}

{{s-start}}
{{s-hou|Rurikovich||942||972||name=Sviatoslav I of Kiev|Sviatoslav I Igorevich}}
{{s-reg}}
{{s-bef|before=[[Olga of Kiev|Olga]] <small>(regent)</small>}}
{{s-ttl|title=[[Rulers of Kievan Rus|Prince of Kiev]]|years=960s–972}}
{{s-aft|after=[[Yaropolk I of Kiev|Yaropolk I Sviatoslavich]]}}
{{s-pre}}
{{s-bef|before=[[Igor of Kiev|Igor]]}}
{{s-ttl|title=[[Rulers of Kievan Rus|Prince of Kiev]]|years=945–960s}}
{{s-aft|after=[[Yaropolk I of Kiev|Yaropolk I]]}}
{{s-bef | before=[[Olga of Kiev|Olga]]}}
{{s-ttl | title=[[Rulers of Kievan Rus']]|years=945–972}}
{{s-aft | after=[[Yaropolk I of Kiev|Yaropolk]]}}
{{s-end}}
{{featured article}}
{{Gardariki}}
{{Authority control}}

{{DEFAULTSORT:Sviatoslav 01 Of Kiev}}
[[Category:Medieval Russian people]]
[[Category:Medieval Ukrainian people]]
[[Category:Medieval child rulers]]
[[Category:Murdered Russian monarchs]]
[[Category:Rulers of Kievan Rus']]
[[Category:Rurikids]]
[[Category:Rurik dynasty]]
[[Category:940s births]]
[[Category:972 deaths]]
[[Category:10th-century conflicts]]
[[Category:10th-century princes in Rus']]
[[Category:Slavic pagans]]