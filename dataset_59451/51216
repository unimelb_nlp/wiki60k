{{italictitle}}
{{Infobox journal
| cover =
| discipline = [[Economics]]
| abbreviation = ERE
| editor = I.J. Bateman
| publisher = [[Springer Science+Business Media|Springer]]
| country = Netherlands
| frequency = Monthly
| history = 1991-present
| openaccess =
| website = http://www.springer.com/economics/environmental/journal/10640?detailsPage=aimsAndScopes
| link1 = http://www.springerlink.com/content/0924-6460
| link1-name = Online access
| ISSN = 0924-6460
}}
'''''Environmental and Resource Economics''''' (''ERE'') is a [[peer-review]]ed [[academic journal]] covering [[environmental economics]] published monthly in three volumes per year. It is the official journal of the [[European Association of Environmental and Resource Economists]]. Since 1991, it has had a growing influence upon the field of [[Environmental Economics]].<ref>{{cite web |url=http://www.env-econ.net/2008/01/top-journals-in.html |title=www.env-econ.net }}</ref>

The journals' main interest is the application of economic theory and methods to environmental issues and problems that require detailed analysis in order to improve management strategies. Areas of particular concern include evaluation and development of instruments of environmental policy; cost-benefit and cost effectiveness analysis; sectoral environmental policy impact analysis; modelling and simulation; institutional arrangements; resource pricing and the valuation of environmental goods; and environmental quality indicators. Special issues are occasionally dedicated to particular topics.

The editors of the journal encourage a pluralistic approach to both theoretical and applied contributions. The publication of empirically based, policy-oriented research is given a high priority in the journal in order to further critical discussion. ''Environmental & Resource Economics'' also entertains papers with an interdisciplinary approach, where this helps to improve knowledge of the real world complexities present, provided that the analysis retains links to or components of economic thinking. The journal publishes research that is likely to be of interest to economists, economic geographers and other academics, professionals and officials with a working interest in environmental matters.

==References==
<references/>

==Related links==
* [http://www.eaere.org/ European Association of Environmental and Resource Economists] (EAERE)

{{DEFAULTSORT:Environmental And Resource Economics}}
[[Category:Economics journals]]
[[Category:Resource economics]]
[[Category:Environmental social science journals]]


{{econ-journal-stub}}