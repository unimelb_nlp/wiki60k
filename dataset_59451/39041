{{Infobox person
| name        = Solomon D. Butcher
| image       = Solomon Butcher 2.jpg
| alt         = Black-and-white photo of man with dark hair, bushy Van Dyke beard, balding forehead
| caption     = Butcher, ca. 1901
| birth_name  = 
| birth_date  = January 24, 1856
| birth_place = [[Burton, West Virginia]]
| death_date  = March 18, 1927 (aged 71)
| death_place = [[Greeley, Colorado]]
| residence   = [[Custer County, Nebraska]]
| other_names = 
| occupation  = Photographer
| known_for   = Photographs of Nebraska homesteaders
| signature   = Solomon Butcher signature.jpg
}}

'''Solomon D. Butcher''' (January 24, 1856 &ndash; March 18, 1927) was an itinerant photographer who spent most of his life in central [[Nebraska]], in the [[Great Plains]] region of the United States.  A settler under the [[Homestead Act]], he began in 1886 to produce a photographic record of the history of white settlement in the region.  Over 3,000 of his negatives survive; more than 1,000 of these depict [[sod house]]s.  Butcher wrote two books incorporating his photographs: ''Pioneer History of Custer County and Short Sketches of Early Days in Nebraska'' (1901), and ''Sod Houses, or the Development of the Great American Plains'' (1904).

Butcher was unable to achieve financial success as a farmer, as a photographer, or in a number of other schemes later in his life, and at the time of his death felt that he had been a failure.  However, the number and scope of his photographs of Nebraska pioneer life have made them a valuable resource to students of that period of history, and they have become a staple of historical texts and popular works alike.  His oeuvre has been described as "the most important chronicle of the saga of homesteading in America".<ref name=nshs-bio/>

==Early life==

Solomon D. Butcher<ref name=middle/> was born on January 24, 1856, the oldest child of Thomas Jefferson Butcher and Esther (Ullom) Butcher, in [[Burton, West Virginia|Burton]] in [[Wetzel County, West Virginia|Wetzel County]], in what was then the state of [[Virginia]] but later became part of [[West Virginia]].  In 1860, his family moved to [[LaSalle County, Illinois]], where his father worked for the [[Illinois Central Railroad]].<ref name=carter-1/><ref name=wnona/>  The family remained there for nearly twenty years.  Butcher finished high school in 1874 and was briefly apprenticed to a [[Tintype|tintypist]], who taught him the business of photography.  In the winter term of 1875&ndash;76, he attended the Henry Military School in [[Henry, Illinois]].  He then worked as a travelling salesman for a firm in [[Clyde, Ohio]] until 1880.<ref name=carter-1/><ref name=custer-143-145/>

In 1880, Thomas Jefferson Butcher announced that he was leaving his secure job with the railroad and moving west, to establish a [[Homestead Act|homestead]] in [[Custer County, Nebraska]].<ref name=carter-1/>  Although Solomon Butcher had a good job, he had grown tired of his work, and "had already thought seriously of seeking my fortune in the great west".<ref name=custer-143-145/>  In March 1880, a party consisting of Butcher, his father, his brother George, and his brother-in-law J. R. Wabel started westward in two covered wagons.  After seven weeks, they arrived in northeastern Custer County, where they occupied homesteads near the north bank of the [[Loup River|Middle Loup River]], west-northwest of present-day [[Sargent, Nebraska|Sargent]].<ref name=carter-1-2-4/>

The party began construction of a [[sod house]], and Butcher quickly came to rue his decision to go west: "I soon came to the conclusion that any man that would leave the luxuries of a boarding house, where they had hash every day, and a salary of $125 a month to lay Nebraska sod for 75 cents a day... was a fool."<ref name=custer-145-146/>  Upon the completion of the house, Butcher and his father returned to Illinois to bring his mother and his youngest brother to Nebraska.  However, Butcher did not return with them: he stayed in Illinois for several months, returning to Nebraska with only three days left to construct a dwelling on his homestead; failure to do so would mean forfeiting his claim.  Butcher, his father, and two of his brothers built and occupied a [[Dugout (shelter)|dugout]], saving the claim.  Two weeks later, however, he once again went back east, moving to [[Milwaukee, Wisconsin]] and abandoning his homestead.<ref name=carter-2-3/><ref name=custer-146-47-50/>  "I would not have remained and kept batch for five years for the whole of Custer county," he declared.<ref name=custer-150/>

[[File:Lillie Butcher.jpg|thumb|upright=0.7|alt=Photograph of woman with short wavy hair, looking straight into camera|Lillie Butcher, ca. 1901]]

Butcher attended medical college in [[Minneapolis]] in the winter and spring of 1881&ndash;2.  There, he met Lillie M. (Barber) Hamilton, a young widow working as a nurse at the hospital.  The two were married in May 1882.<ref name=custer-150/><ref name=carter-3/><ref name=chrisman/>

Soon thereafter, Butcher once again decided that the West was the place for him.  "I had just seen enough of the wild west to unfit me for living contentedly in the East",<ref name=custer-150/> he wrote.  In October 1882, the couple returned to Custer County, where they moved in with his father.  During that winter, he worked as a schoolteacher.<ref name=carter-3/>

==Photographer on the Plains==

Butcher was able to save some of his teacher's salary, and to borrow enough more to open the first photography studio in Custer County.  The studio was housed in a [[wattle and daub|lath-and-adobe]] building, measuring {{convert|18|x|28|ft|m}}, with a dirt floor and with cotton sheeting in lieu of glass to cover the windows and the skylights.  As a backdrop for the photos, he used an old cloth wagon cover.  The cloth had been gnawed by rats and was full of holes, which Lillie patched.  To keep the patches from showing up in photographs, Butcher attached two coil springs from an old bed to the ceiling, then hung the backdrop from them.  Before taking a picture, he plucked the backdrop so that it oscillated on the springs; the motion, combined with the long exposure time required, blurred the backdrop so that the patches would not be visible in the photograph.<ref name=carter-3/><ref name=gaston/><ref name=custer-150-151/>

Photography alone was not enough to pay the bills.  Butcher opened a post office in his studio, which he named "Jefferson" after his father.<ref name=perkey/>  This proved less than lucrative: his postal income came from stamp cancellations, and in the first three months amounted to 68 cents.<ref name=gaston/>  He also did farm work for his father, at a wage of 50 to 75 cents per day.<ref name=custer-151/>

In December 1884, the town of Walworth was established near Butcher's homestead.  Butcher, his wife, and their two children moved there and built a sod house.  In Walworth, Butcher found a business partner, A. W. Darling, who supplied the money to put up a frame building for a studio.  However, Walworth did not last: it had been established at a time when rains had been unusually good, and the resumption of normal dry conditions led to the town's demise.  Butcher's family had to leave their sod house after six weeks of residence.  He and Darling were forced to sell their building, which was moved to the town of West Union; there, they rented it for five years.<ref name=carter-4/><ref name=custer-153/>

==''Pioneer History''==

In the spring of 1886, Butcher conceived the idea of writing an illustrated history of Custer County.  This, he thought, would be the key to fame and riches.  "At last, Eureka! Eureka!  I had found it.  I was so elated that I lost all desire for rest and had to take morphine to make me sleep."<ref name=custer-153/>  To embark upon this project, he needed financing.  Unlike Butcher, his father had succeeded as a farmer, running a gristmill and a freighting business as well; it was to him that Butcher turned for assistance.  Thomas Jefferson Butcher was initially skeptical; but after his son had arranged to photograph 75 homesteads, he agreed to provide a team and wagon for the project.<ref name=carter-5/>

[[File:Solomon D. Butcher 1886 cropped.jpg|thumb|left|alt=Dark-haired man with small moustache; operating large bellows camera|Butcher, ca. 1886]]

In June 1886, Butcher took the first photograph for the book.  He met a certain amount of skepticism&mdash;"Some called me a fool, others a crank..."<ref name=custer-153/>  He began his work only thirteen years after the establishment of the first homestead in the county,<ref name=gaston-86-7/> when it could hardly be said to have a history.<ref name=welsch/>  He persevered: from 1886 to 1892, he took over 1,500 photographs and recorded over 1,500 narratives.<ref name=custer-153/>

Hard times struck Custer County in the early 1890s.  Crops failed in 1890; good crops in the following two years were offset by low prices.  The harvest was small in 1893, and the spring and summer of 1894 were almost entirely devoid of rain.  The county also partook of the nationwide depression that began with the [[Panic of 1893]].<ref name=gaston-154-5/>  Butcher was an early victim of this agricultural and economic collapse: in 1892, he lost his farm, and was forced to suspend his history project.<ref name=carter-5-6/>

The [[People's Party (United States)|Populist Party]] and its predecessor, the [[Farmers' Alliance]], were strong in Custer County in the 1890s, carrying elections for a decade beginning in 1889.<ref name=gaston-297-8/>  Butcher attached himself to the movement and, in 1896, was elected Justice of the Peace and Clerk of the Election for West Union.  Rising farm prices and a generally improving economy allowed him to secure a home and to get himself nearly out of debt, and in 1899 he was about to resume work on the history.<ref name=carter-6/><ref name=custer-153-4/>

In 1899, however, Butcher's house burned down, destroying its entire contents, including the pioneer narratives and the photographic prints.  Fortunately for the history project, the glass negatives were stored in a granary, and escaped the fire.  The house had carried no insurance, however, and Butcher was once again left penniless.<ref name=chrisman/><ref name=carter-6/>

Butcher persevered, again setting himself to the task of compiling pioneer narratives.  He secured the assistance of George B. Mair, the editor of the ''[[Callaway, Nebraska|Callaway]] Chronicle'', in editing these accounts and in preparing the manuscript.<ref name=chrisman/>  To cover the cost of engraving, typesetting, and publishing, he recruited Ephraim Swain Finch, an early settler of Custer County and now a wealthy rancher.  Finch, whom Butcher knew through his Populist activities, agreed to underwrite these expenses; moreover, he placed an advertisement in the ''Custer County Chief'', assuring readers that orders for Butcher's forthcoming book would be filled.<ref name=carter-6/>

[[File:First train into Broken Bow, Nebraska.jpg|thumb|alt=Steam locomotive partly on small railroad bridge; several men standing on and around it|Butcher photo: first train to arrive in [[Broken Bow, Nebraska|Broken Bow]], 1886]]

Orders for the book began flowing in.  The first edition of 1000 copies sold out before its delivery date, in the summer of 1901; a second printing, of either 500 or 1000 copies, was issued before Christmas of that year.  The book, titled ''Pioneer History of Custer County and Short Sketches of Early Days in Nebraska'', included 200 engravings in its more than 400 pages.<ref name=chrisman/><ref name=carter-6/><ref name=mosaic/>

==Later career==

Encouraged by the success of ''Pioneer History'', Butcher began planning similar photographic histories of [[Buffalo County, Nebraska|Buffalo]] and [[Dawson County, Nebraska|Dawson]] counties, which border Custer County on the south.  He moved to [[Kearney, Nebraska|Kearney]], the county seat of Buffalo County, in 1902; there, he opened a photography studio together with his son Lynn.  He roamed more widely still, through Colorado, Utah, and Wyoming; carrying his equipment in a wagon, he made negatives on the site, then shipped these to Kearney, where Lynn and several women employees made the prints and mailed them back to the customers.  Butcher ''pere et fils'' also ran a postcard business, making over 2 million cards for the local trade.  In 1904, he published a second book, ''Sod Houses; or, The Development of the Great American Plains'', at the urging of a lawyer who hoped to use Butcher's photographs and accounts to sell land in Nebraska.<ref name=gaston/><ref name=carter-6-7/><ref name=survey/><ref name=kampinen-82/>  In 1909, he visited [[Yellowstone National Park]] and produced a set of 100 [[stereoscopy|stereographic]] postcards.<ref name=yellow/>

Butcher abandoned the history of Buffalo and Dawson counties after spending more than a thousand dollars on the project.  Discontented with his profession as photographer, which had failed to make him a fortune or even to put him on a sound financial footing, he turned his efforts elsewhere.  In 1911, he turned the Kearney studio over to his son and began work as an agent for the Standard Land Company.  He gave [[stereopticon]] lectures throughout Buffalo and Dawson counties promoting the company's irrigated lands in south Texas, and made plans to move there himself.<ref name=carter-6-7/>

[[File:Frederic Schreyer house by Solomon Butcher.jpg|thumb|alt=Three adults and two small children in front of two-story house: lower story sod-walled, upper story with clapboard siding|Butcher photo: Frederic Schreyer and family, Custer County, 1880s.]]

Before he could move, however, he had to dispose of his thousands of {{convert|6.5|x|8.5|in|adj=on}} glass-plate negatives.  He also had to get himself out of debt once again.  In an attempt to accomplish both of these at once, he offered his collection of negatives to the [[Nebraska State Historical Society]].  To Addison Sheldon, head of the Society's Legislative Reference Bureau, he wrote, "Now is the time to buy me cheap, when I need the money so badly."<ref name=welsch/><ref name=kampinen-82/>  In November 1911, he and Sheldon signed a contract for the sale of the negatives for $1000.  Butcher was to receive $100 down; the rest would be paid after the Nebraska legislature passed a bill appropriating funds for the purchase.<ref name=carter-7-8/>

Unfortunately for Butcher, a feud was raging between Sheldon and Clarence S. Paine, secretary of the Historical Society.  Sheldon was an ardent Populist; Paine, a follower of conservative Democrat [[Julius Sterling Morton|J. Sterling Morton]].  Sheldon believed that Paine had used underhanded tactics to displace his predecessor as secretary; Paine believed that Sheldon was scheming to bring the Society under control of the University of Nebraska regents.<ref name=centennial-346-7/>  At the legislature's next biennial session, in 1913, Sheldon had an appropriations bill introduced to pay for the Butcher purchase.  The bill passed the House unanimously, but ran into stiff opposition in the Senate, probably at Paine's instigation.  In the end, Butcher was forced to accept a compromise payment of only $600.<ref name=carter-8-9/>

Butcher's Texas land deals came to nothing.  In 1915, he moved to [[Broken Bow, Nebraska|Broken Bow]], back in Custer County.  In December of that year, Lillie Butcher, who had suffered ill health for many years, died.  For a short time in early 1916, Butcher worked for Sheldon, annotating his collection of negatives and adding narratives that had not been included in ''Pioneer History''.  In 1917, he married Mrs. Laura M. (Brachear) Nation.<ref name=gaston/><ref name=carter-9/>

Butcher briefly worked as a travelling salesman for a grain and flour mill.  However, he abandoned this for less practical schemes.  He invented what he described as an "electromagnetic oil detector", applying the principles of [[dowsing]] to the discovery of oil.  In 1921, he planned a photographic expedition to Central America to produce material for a series of travelogue lectures, and tried to interest Sheldon in it; Sheldon was skeptical, and the expedition was never launched.  In about 1924, he began marketing a [[patent medicine]], consisting chiefly of alcohol, dubbed "Butcher's Wonder of the Age".<ref name=carter-9/>

[[File:Solomon Butcher 1.jpg|thumb|alt=Man, almost entirely bald with a little white hair behind the temples; no facial hair|Butcher, ca. 1919]]

In 1926, the Butchers moved to [[Greeley, Colorado]]; Butcher died there on March 18, 1927.<ref name=carter-9/>

==Works==

Butcher's work received little notice outside of Nebraska during his lifetime.  Although there was a market for photos depicting the romance of the Wild West, the public preferred mountains and canyons to open prairie.  Later in his life, popular taste inclined toward the modern and the sophisticated; images of rustics gathered around sod houses were out of fashion.<ref name=kampinen-77-8/>  His photographs had little to recommend them from an artistic standpoint.  Biographer John Carter describes him as unconcerned with aesthetics, and with no more than adequate technical abilities.  "Unquestionably he was not a prairie [[Alfred Stieglitz|Stieglitz]]."<ref name=carter-10/>

Recognition of Butcher's work came only later, when the history of the settlement of the Plains began to be written.  His photographs became staples of textbooks and popular works dealing with the homestead era.  According to Carter, "They are the images that we conjure up when we think of plains settlement."<ref name=carter-9-10/>

Butcher has been compared to painter [[George Catlin]], who painted [[Native Americans in the United States|Native Americans]] in the 1830s, and to Nebraska writer and photographer [[Wright Morris]], who depicted rural Nebraska in the 1930s and 1940s.  Like them, Butcher recognized that an era in Plains history was passing, and tried to document it visually before it was gone.<ref name=morris/><ref name=cyclo/>  Even during his short career, the face of Nebraska changed; the sod houses of his earlier photographs are increasingly supplanted by frame buildings in his later ones.<ref name=carter-12/>

Butcher did not confine himself to recording events that took place when he was present.  He was not above re-enacting historical events for a photo: for example, the 1878 lynching of two Custer County homesteaders at the behest of rancher Print Olive, or the cutting of another rancher's fences by homesteaders.<ref name=chrisman/><ref name=olive/>  The latter photograph has been uncritically accepted by many historians as documentation of the actual event, although a closer examination reveals that the wire-cutters are made out of wood.<ref name=wire/>  Butcher also did not hestitate to retouch photos.  He photographed a hill in [[Cherry County, Nebraska|Cherry County]] that had been important to early setters because of its cedar trees; since the trees had been cut long before he got there with his camera, he scratched trees on the bare hill on his negative.<ref name=trees/>  To illustrate Ephraim Swain Finch's account of how he had battled an 1876 infestation of grasshoppers, he posed Finch in his cornfield, then incised scores of dots and specks into the negative to depict the flying insects.<ref name=bugs/>  When he sought to illustrate a large flock of ducks, but realized that the birds were flying too fast for his camera to record, he photographed the scene without the waterfowl and then scratched dozens of ducks into the negative.<ref name=ducks/>

Butcher retouched for his own purposes as well.  On one occasion, he damaged a spot on a negative, producing a hole in a photograph of a sod house.  Rather than undertaking a round trip of {{convert|60|mi|km|round=10|spell=in}} to re-shoot the scene, he concealed the damage by inking a crudely drawn turkey on the negative.  Upon seeing the finished product, the homesteader expressed wonderment, declaring that he owned no white turkeys; but he was persuaded to put aside his doubts, since the camera was incapable of lying.<ref name=turkey/>

[[File:Reyner sod house by Solomon Butcher.jpg|thumb|alt=Man standing on roof of dilapidated sod house with axe or similar implement|Butcher photo: Demolition of the "old Reyner sod house" in 1904, following construction of a new [[Queen Anne style architecture in the United States|Queen Anne]] house on the property.<ref name=reyner/>]]

Butcher's determination to record a vanishing era led him to photograph every detail of life in the homestead era.  According to the editor of a 1965 edition of ''Pioneer History'', "There was nothing too inconsequential for him to direct his camera upon."<ref name=chrisman/>  The Butcher collection at the Nebraska State Historical Society consists of nearly 3,500 negatives; nearly 1,500 were taken in Custer County, and more than 1,000 show sod houses.<ref name=kampinen-83/>

The number and scope of Butcher's photographs has made them a valuable resource to the historian of the period.  Nebraska folklorist [[Roger Welsch]] conducted a detailed analysis of Butcher's sod-house photos, including furnishings, farm equipment, animals, etc., for his 1968 ''Sod Walls'',<ref name=welsch/> which initiated present-day investigations of sod-house construction and living.<ref name=kampinen-6/>  Studies based on the photographs continue: in the 21st century, digital image processing enabled researchers to see details inside doors and windows, which appeared as nothing more than dark oblongs in the original prints.<ref name=digital/>

Although Butcher never achieved financial success or artistic recognition, and died believing himself a failure, his work has endured.<ref name=nshs-bio/>  According to one writer, "No other photographer captured settlement in the Great Plains with such insight into the experience of homesteading."<ref name=cyclo/>

==Notes==
<!-- References are sorted alphabetically by "name=" -->

<references>

<ref name=bugs>McAsh (1991), pp. 33-4.  The [http://memory.loc.gov/award/nbhips/lca/126/12600v.jpg photograph with hand-drawn grasshoppers] is reproduced at the Library of Congress's American Memory website.</ref>

<ref name=carter-1>Carter (1985), p. 1.</ref>

<ref name=carter-1-2-4>Carter (1985), pp. 1-2, 4.</ref>

<ref name=carter-2-3>Carter (1985), pp. 2-3.</ref>

<ref name=carter-3>Carter (1985), p. 3.</ref>

<ref name=carter-4>Carter (1985), p. 4.</ref>

<ref name=carter-5>Carter (1985), p. 5.</ref>

<ref name=carter-5-6>Carter (1985), pp. 5-6.</ref>

<ref name=carter-6>Carter (1985), p. 6.</ref>

<ref name=carter-6-7>Carter (1985), pp. 6-7.</ref>

<ref name=carter-7-8>Carter (1985), pp. 7-8.</ref>

<ref name=carter-8-9>Carter (1985), pp. 8-9.</ref>

<ref name=carter-9>Carter (1985), p. 9.</ref>

<ref name=carter-9-10>Carter (1985), pp. 9-10.</ref>

<ref name=carter-10>Carter (1985), p. 10.</ref>

<ref name=carter-12>Carter (1985), p. 12.</ref>

<ref name=centennial-346-7>Diffendal (1978), pp. 346-7.</ref>

<ref name=chrisman>Chrisman (1965), pp. v-viii.</ref>

<ref name=custer-143-145>Butcher (1901), pp. 143-5.</ref>

<ref name=custer-145-146>Butcher (1901), pp. 145-6.</ref>

<ref name=custer-146-47-50>Butcher (1901), pp. 146-7, 150.</ref>

<ref name=custer-150>Butcher (1901), p. 150.</ref>

<ref name=custer-150-151>Butcher (1901), pp. 150-1.</ref>

<ref name=custer-151>Butcher (1901), p. 151.</ref>

<ref name=custer-153>Butcher (1901), p. 153.</ref>

<ref name=custer-153-4>Butcher (1901), pp. 153-4.</ref>

<ref name=cyclo>Koelling, Jill Marie.  [http://plainshumanities.unl.edu/encyclopedia/doc/egp.art.008 "Butcher, Solomon (1856-1927)".]  [http://plainshumanities.unl.edu/encyclopedia/ ''Encyclopedia of the Great Plains''.]  Retrieved 2012-06-15.</ref>

<ref name=digital>[http://nebraskahistory.org/lib-arch/research/photos/digital/history.htm "Revealing History: Using Digital Technology to Learn More about Our Past".]  [http://nebraskahistory.org/index.htm Nebraska State Historical Society.]  Retrieved 2013-04-15.</ref>

<ref name=ducks>Carter (1985), p. 14.The [http://memory.loc.gov/award/nbhips/lca/135/13546v.jpg photograph with hand-drawn ducks] is reproduced at the Library of Congress's American Memory website.</ref>

<ref name=gaston>Gaston and Humphrey (1919), pp. 962-67.</ref>

<ref name=gaston-86-7>Gaston and Humphrey (1919), pp. 86-7.</ref>

<ref name=gaston-154-5>Gaston and Humphrey (1919), pp. 154-5.</ref>

<ref name=gaston-297-8>Gaston and Humphrey (1919), pp. 297-8.</ref>

<ref name=kampinen-6>Kampinen (2008), p. 6.</ref>

<ref name=kampinen-77-8>Kampinen (2008), pp. 77-8.</ref>

<ref name=kampinen-82>Kampinen (2008), p. 82.</ref>

<ref name=kampinen-83>Kampinen (2008), p. 83.</ref>

<ref name=middle>Sources differ on Butcher's middle name.  Chrisman (1965, p. v) and McKee (2010) give it as "Devoe".  A [http://nebraskahistory.org/lib-arch/research/manuscripts/family/solomon-butcher.htm capsule biography] at the [http://www.nebraskahistory.org/index.shtml Nebraska State Historical Society] gives it as "DeVore".  Searching the [http://catalog.loc.gov/webvoy.htm Library of Congress online catalog] for "Butcher, Solomon" yields "Devore".  On the title pages of Butcher (1901) and Butcher (1904), his name is given as "Solomon D. Butcher".  Butcher's tombstone also lists only his middle initial (see [[:File:Butcher, Solomon and Lillie tombstone 1.JPG|photo]]).</ref>

<ref name=morris>Jacobson, Joanne.  [http://digitalcommons.unl.edu/cgi/viewcontent.cgi?article=1334&context=greatplainsquarterly "Time and Vision in Wright Morris's Photographs of Nebraska".]  [http://digitalcommons.unl.edu/greatplainsquarterly/ ''Great Plains Quarterly''.]  Spring 1987: Vol. 7, pp. 3-21.</ref>

<ref name=mosaic>George-Bloomfield, Suzanne.  "Solomon Butcher: Prairie Photographer".  In ''A Prairie Mosaic: An Atlas of Central Nebraska's Land, Culture, and Nature'', ed. by Steven J. Rothenberger and Suzanne George-Bloomfield.  Published by University of Nebraska&mdash;Kearney, 2000.  pp. 112-115.</ref>

<ref name=nshs-bio>[http://nebraskahistory.org/lib-arch/research/photos/highlite/butcher/bio.htm "Solomon D. Butcher Biography".]  [http://www.nebraskahistory.org/index.shtml Nebraska State Historical Society.]  Retrieved 2012-06-07.</ref>

<ref name=olive>Howell, Alice Shaneyfelt.  [http://bchs.us/btales_198501.html "The Mitchell-Ketchum Tragedy".]  [http://bchs.us/index.html Buffalo County Historical Society.]  Retrieved 2013-04-15.</ref>

<ref name=perkey>Perkey, Elton A.  ''Perkey's Nebraska Place Names''.  Nebraska State Historical Society, 1995.  Revised edition, 2003.  p. 46.</ref>

<ref name=reyner>Butcher (1904), pp. 18-19.</ref>

<ref name=survey>[http://www.nebraskahistory.org/histpres/reports/custer_county.pdf "Nebraska Historic Buildings Survey: Custer County".]  [http://www.nebraskahistory.org/index.shtml Nebraska State Historical Society.]  Retrieved 2011-07-19.</ref>

<ref name=trees>McAsh (1991), p. 33.  The [http://memory.loc.gov/award/nbhips/lca/101/10130v.jpg photograph with hand-drawn trees] is reproduced at the Library of Congress's American Memory website.</ref>

<ref name=turkey>Carter (1985), p. 14; McAsh (1991), pp. 35-6.  The [http://memory.loc.gov/award/nbhips/lca/283/2836v.jpg "turkey" photograph] is reproduced at the Library of Congress's American Memory website.</ref>

<ref name=welsch>Welsch (1968), pp. vii-xv.</ref>

<ref name=wire>See "Settlers Taking the Law in Their Own Hands" at [http://nebraskahistory.org/lib-arch/research/photos/highlite/butcher/photos.htm "Solomon D. Butcher: Photographs of the Nebraska Homestead Experience".]  [http://nebraskahistory.org/index.htm Nebraska State Historical Society.]  Retrieved 2013-04-15.</ref>

<ref name=wnona>Carter (1985, p. 1) gives the Butchers' place of residence in Illinois as "Winona".  Butcher (1901, p. 143) does not name a hometown, only giving LaSalle County.  There is presently a [[Wenona, Illinois|Wenona]] in LaSalle County, lying on the Illinois Central Railroad ([http://www.wenona.org/ "Welcome to Wenona, Illinois"]; retrieved 2012-06-15); there does not appear to be a "Winona, Illinois".</ref>

<ref name=yellow>[http://yellowstonestereoviews.com/publishers/butcher.html "Solomon D. Butcher".]  [http://yellowstonestereoviews.com/views.html The Yellowstone Stereoview Page.]  Retrieved 2012-06-07.</ref>

</references>

==References==

Butcher, S. D. (1901).  [http://www.unz.org/Pub/ButcherSolomon-1965 ''Pioneer History of Custer County, and Short Sketches of Early Days in Nebraska''.]  Broken Bow, Nebraska.  Available online at [http://www.unz.org/ unz.org.]

Butcher, Solomon D. (1904).  [http://www.unz.org/Pub/ButcherSolomon-1904 ''Sod Houses, or the Development of the Great American Plains''.]  Kearney, Nebraska and Chicago, Illinois: Western Plains Publishing Co.  Available online at [http://www.unz.org/ unz.org.]

Carter, John (1985).  ''Solomon D. Butcher: Photographing the American Dream''.  Lincoln, Nebraska: University of Nebraska Press.

Chrisman, Harry E. (1965).  Introduction to Solomon D. Butcher, ''Pioneer History of Custer County, Nebraska, with which is combined Sod Houses of the Great American Plains''.  Denver: Sage Books.  pp. v-viii.

Diffendal, Annie Polk (1978).  [http://www.nebraskahistory.org/publish/publicat/history/full-text/1978-3-NSHS_100_Years.pdf "A Centennial History of the Nebraska State Historical Society, 1878-1978".]  [http://www.nebraskahistory.org/index.shtml Nebraska State Historical Society.]  Retrieved 2012-06-10.

Gaston, W. L. and A. R. Humphrey (1919).  ''[https://archive.org/details/historyofcusterc00gast History of Custer County, Nebraska.]''  Lincoln, Nebraska: Western Publishing and Engraving Company.

Kampinen, Andrea L. (2008). [http://athenaeum.libs.uga.edu/handle/10724/12697 "The sod houses of Custer County, Nebraska".]  Masters thesis, University of Georgia.  Retrieved 2011-10-22.

McAsh, Heather (1991).  "Remnants of Power: Tracing Cultural Influences in the Photography of Solomon D. Butcher".  ''American Studies'', vol. 32, no. 2 (Fall 1991), pp.&nbsp;29–39.  Available online [http://www.jstor.org/stable/40643593 via JSTOR].  Retrieved 2012-06-05.

McKee, Jim (2010).  [http://journalstar.com/news/local/article_45f8d86c-07bc-11df-9ea9-001cc4c03286.html "Jim McKee: Photographer Butcher 'saved' the sod house".]  [http://journalstar.com/ ''Lincoln Journal Star''.]  2010-01-24.  Retrieved 2012-06-15.

Welsch, Roger L. (1968).  ''Sod Walls: The Story of The Nebraska Sod House''.  Broken Bow, Nebraska: Purcells, Inc.

==External links==
*[http://memory.loc.gov/ammem/award98/nbhihtml/pshome.html "Prairie Settlement 1862&ndash;1912".]  Includes links to a number of digitized Butcher photos, and an index of photographic subjects.  At [http://memory.loc.gov/ammem/index.html American Memory, Library of Congress.]
*[http://nebraskahistory.org/lib-arch/research/photos/highlite/butcher/photos.htm "Solomon D. Butcher: Photographs of the Nebraska Homestead Experience".]  Selection of Butcher photos.  At [http://www.nebraskahistory.org/index.shtml Nebraska State Historical Society.]

{{good article}}

{{Authority control}}
{{DEFAULTSORT:Butcher, Solomon D.}}
[[Category:1856 births]]
[[Category:1927 deaths]]
[[Category:People from Custer County, Nebraska]]
[[Category:People from LaSalle County, Illinois]]
[[Category:People from Wetzel County, West Virginia]]
[[Category:Photographers from Nebraska]]
[[Category:Photographers from West Virginia]]