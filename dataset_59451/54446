{{refimprove|date=May 2016}}
{{Use dmy dates|date=October 2012}}
{{Use Australian English|date= May 2016}}
{{Infobox Australian place
|type=suburb
| name = North Haven		
| state = SA
| image     = Floating dock at North Haven.jpg
| caption   = Floating dock at North Haven
| pop           = 5798 
| pop_year      = {{censusAU|2011}}
| pop_footnotes = <ref name= Census2011>{{Census 2011 AUS|id= SSC40505 |name= North Haven (SA)  |accessdate=10 May 2016|quick=on}}</ref>
| est       = 1975<ref name=PLB>{{cite web|title=Search result for " North Haven (SUB)" (Record no SA0050639) with the following layers being selected - “Suburbs and Localities” |url=http://maps.sa.gov.au/plb/#|work=Property Location Browser
|publisher=Government of South Australia|date= |accessdate=11 May 2016}}</ref>
| postcode  = 5018<ref name=postcode>{{cite web|title=Postcode for North Haven, South Australia |url= http://www.postcodes-australia.com/areas/sa/adelaide/north+haven |publisher= postcodes-australia.com |accessdate=11 May 2016}}</ref>
| elevation =
| elevation_footnotes = 
| timezone = [[UTC9:30|ACST]]
| utc = +9:30
| timezone-dst = [[UTC10:30|ACST]]
| utc-dst = +10:30
| dist1     = 18
| dir1      = northwest 
| location1 = [[Adelaide city centre]]
| dist2    = 
| dir2     = 
| location2= 
| lga       = [[City of Port Adelaide Enfield]]<ref name=PLB/>  
| stategov  = [[electoral district of Port Adelaide | Port Adelaide]]
<ref name=ECSA>{{cite web|title= Port Adelaide electorate boundaries as of 2014 |url=http://www.ecsa.sa.gov.au/component/edocman/?task=document.download&id=1047&Itemid=0|publisher=Electoral Commission SA|accessdate=11 May 2016}}</ref>
| fedgov    = [[Division of Port Adelaide | Port Adelaide]]<ref name=AEC>{{cite web|title=Federal electoral division of Port Adelaide |url=http://www.aec.gov.au/profiles/sa/files/2011/2011-aec-a4-map-sa-port-adelaide.pdf|publisher=Australian Electoral Commission|accessdate=11 May 2016}}</ref>
| maxtemp   = 22.4
| mintemp   = 11.2
| rainfall  = 451.1
| latd= 34.787510
| longd= 138.492160
| near-n             = [[Outer Harbor, South Australia| Outer Harbor]]
| near-ne            = Outer Harbor
| near-e             = Outer Harbor<br/>Osborne
| near-se            = Osborne
| near-s             = [[Osborne, South Australia| Osborne]]
| near-sw            = ''Gulf St Vincent''
| near-w             = ''[[Gulf St Vincent]]''
| near-nw            = ''Gulf St Vincent''
| footnotes          = Location<ref name=postcode/><br/>Coordinates<ref name=PLB/><br/> Climate<ref>{{cite web|title=Monthly climate statistics: Summary statistics PARAFIELD AIRPORT (nearest station) |url= http://www.bom.gov.au/climate/averages/tables/cw_023013.shtml |publisher=Commonwealth of Australia , Bureau of Meteorology |accessdate=11 May 2016}}</ref><br/>Adjoining suburbs<ref name=PLB/>
}}
'''North Haven''' is a north-western [[suburb]] of [[Adelaide]] 20&nbsp;km from the [[Central Business District|CBD]], in the state of [[South Australia]], [[Australia]] and falls under the [[City of Port Adelaide Enfield]]. It is adjacent to [[Osborne, South Australia|Osborne]] and [[Outer Harbor, South Australia|Outer Harbor]]. The postcode for North Haven is 5018. It is bounded to the north and east by Oliver Rogers and Victoria Road, to the south by Marmora Terrace and the west by [[Gulf St Vincent]].

A small residential area that lies north of the Gulf Point Marina is often referred to as being a part of Outer Harbor (though it still lies within the boundaries of North Haven).

==History==
North Haven originally started as a [[Subdivision (land)|private sub-division]] in Section 769 in the [[Cadastral divisions of South Australia|cadastral unit]] of [[Hundred of Port Adelaide]].  Its creation in 1976 was originally opposed by the [[Postmaster General of Australia]] due to “size & duplication of name else in Australia.”  Its boundaries have been altered as follows since 1976 - boundary with the suburb of Outer Harbor, addition of land from the suburb of Osborne and other ‘unnamed land’, and the addition of a ‘portion of Outer Harbor’ in February 2007.<ref name=PLB/>

==Facilities==
<!-- Missing image removed: [[Image:http://www.great-australian-homes.com/images/North_Haven.jpg]] -->

The suburb is served by a primary school, North Haven Primary School, and the local high school is Ocean-View High School, in nearby [[Taperoo, South Australia|Taperoo]]. The western coastal side of the suburb is also graced by a newly constructed Surf Lifesaving Club (completed in 2007), and the Gulf Point Marina which is home to a large sailing community. The marina, lined with exclusive and desirable houses, is protected by  two artificial breakwaters. Attached to the marina is the Cruising Yacht Club of South Australia, one of Adelaide's largest yacht clubs, whilst around the entrance to the Port River lies the Royal South Australian Yacht Squadron. There is a retirement village on Lady Gowrie Road, and there is a small shopping centre on Osborne Road.

The local cricket club is the North Haven Cricket Club which fields numerous teams in Adelaide and Suburban Cricket Association.

==Transport==
The 330 bus services both Lady Gowrie and Osborne Roads and Osborne and Victoria Roads. The [[Outer Harbor railway line]] terminates at [[Outer Harbor railway station]], which is actually in North Haven. The suburb is serviced by two more train stations, [[North Haven railway station]] and [[Osborne railway station]].

==Governance==
North Haven is located within the federal [[division of Port Adelaide]], the state [[electoral district of Port Adelaide]] and the local government area of the [[City of Port Adelaide Enfield]].<ref name=AEC/><ref name=ECSA/><ref name=PLB/>
[[Image:Cruising Yacht Club of South Australia.jpg|center|800px|thumb|360° panorama of the Cruising Yacht Club of South Australia, located at North Haven]]
==References==
{{reflist}}
==External links==
{{Commons category}}
* [http://www.rsays.com.au Royal South Australian Yacht Squadron]
* [http://www.cycsa.com.au Cruising Yacht Club of SA Inc.]

{{City of Port Adelaide Enfield suburbs}}
{{Adelaide landmarks}}

[[Category:Suburbs of Adelaide]]
[[Category:Lefevre Peninsula]]