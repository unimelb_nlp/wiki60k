{{Infobox Journal
| cover        = [[File:ACS Photonics first cover.jpg]]
| editor       = [[Harry Atwater]]
| discipline   = [[Optical science]], [[Material science]], [[Nano science]]
| abbreviation = ACS Photonics
| publisher    = [[American Chemical Society]]
| country      = United States
| frequency    = Monthly
| history      = 2014–present
| impact       = 5.404
| website      = http://pubs.acs.org/journal/apchd5
| RSS          = http://feeds.feedburner.com/acs/apchd5
| CODEN        = APCHD5
| eISSN         = 2330-4022
}}
'''''ACS Photonics''''' is a monthly, [[peer review|peer-reviewed]], [[scientific journal]], first published in January 2014 by the [[American Chemical Society]]. The current [[editor in chief]] is [[Harry Atwater|Harry A. Atwater]] ([[California Institute of Technology]]). The interdisciplinary journal publishes original research articles, letters, reviews and perspectives.<ref name=about>{{Cite web
  | title = ACS Photonics - About
  | work = ACS Photonics
  | publisher =American Chemical Society
  | date = January 2014
  | url =http://pubs.acs.org/page/apchd5/about.html
  | accessdate =2014-06-27}}</ref>
<ref name=cassi>{{Cite web
  | title = CAS
  | work = CAS CASSI
  | publisher =American Chemical Society
  | date = June 2014
  | url =http://cassi.cas.org/publication.jsp?P=eCQtRPJo9AQyz133K_ll3zLPXfcr-WXfsPGsx4VSBh4lnP3OBb7bWDLPXfcr-WXfMs9d9yv5Zd9dPe2jm4Ja1TLPXfcr-WXfMs9d9yv5Zd-7B8qhjzjEag
  | format = 
  | accessdate =2014-06-27}}</ref>

==Scope==
The focus of ''ACS Photonics'' is the science of [[photonics]] and light-matter interactions. The areas of research reported in the journal are:<ref name=about/>

*Molecular and [[nanophotonics]]
*Solid state inorganic materials for [[optoelectronics]]
*Polymer and organic optoelectronic materials
*[[Plasmonics]] and [[Photonic metamaterial|optical metamaterials]]
*[[Photonic crystal]]s
*Mesoscale photonics and optoelectronics
*[[Nonlinear optics]] and materials
*[[Quantum optics]] and single-photon processes
*[[Flexible electronics]] and displays
*[[Silicon photonics]]
*Optical switching, memory, and data storage
*[[Lasers]], [[quantum electronics]], and [[optical amplifier]]s
*[[Light-emitting diode|LEDs]] and solid-state lighting
*Photonics for energy materials
*[[Biophotonics]]
*[[Micro-Opto-Electro-Mechanical Systems|Micro]]- and nano-optoelectromechanical systems
*Modeling and simulation of photonic processes

==Abstracting and indexing==
''ACS Photonics'' is indexed in the following databases:<ref name=about/><ref name=cassi/><ref name=mat-sci>{{Cite web
  | title = Thomson Reuters Master Journal List
  | work = 
  | publisher =Thomson Reuters
  | date =August 2010 
  | url = http://ip-science.thomsonreuters.com/cgi-bin/jrnlst/jlresults.cgi?PC=MASTER&ISSN=2330-4022
  | format = 
  | accessdate =2010-08-13}}</ref>

*[[Chemical Abstracts Service]] – [[Chemical Abstracts Service#CASSI|CASSI]]
*[[Chemistry Citation Index]]
*[[Science Citation Index Expanded]] 
*[[Current Contents]] – Engineering, Computing & Technology
*[[Current Contents]] - Physical, Chemical & Earth Sciences
*[[Scopus]]
*[[MEDLINE]]/[[PubMed]]

According to the 2015 Journal Citation Reports published by Thomson Reuters on June 13, 2016, ACS Photonics has received its first impact factor of 5.404.<ref>{{Cite web|url=http://app.acspubs.org/e/es?s=1913652004&e=170156&elq=2b46e00191724278bdaa96cd0ff9b8fb&elqaid=3008&elqat=1&elqTrackId=3e1a3b7205ad40e89b479e4c3651471e|title=Inaugural Impact Factor for ACS Photonics|website=app.acspubs.org|access-date=2016-06-15}}</ref>
<references group="http://pubs.acs.org/journal/apchd5" />.

==References==
{{Reflist}}

==External links==
* {{Official website|http://pubs.acs.org/journal/apchd5}}

[[Category:American Chemical Society academic journals]]
[[Category:Optics journals]]
[[Category:Nanotechnology journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 2014]]
[[Category:2014 establishments in the United States]]