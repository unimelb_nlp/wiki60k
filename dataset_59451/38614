{{good article}}
{{Geobox | Protected Area
<!-- *** Name section *** -->
| name                        = Brainard Homestead State Park
| native_name                 = 
| other_name                  = 
| other_name1                 = 
<!-- *** Category *** -->
| category                    = [[List of Connecticut state parks|Connecticut State Park]]
| category_iucn               = 
<!-- *** Image *** -->
| image                       = EastHaddamCT BrainardHomesteadSP.jpg
| image_alt                   = 
| image_caption               = 
<!-- *** Etymology *** --->
| etymology_type              = Named for
| etymology                   = 
<!-- *** Country etc. *** -->
| country                     = {{flag|United States}}
| state                       = {{flag|Connecticut}}
| region_type                 = County
| region                      = [[Middlesex County, Connecticut|Middlesex]]
| region1                     = 
| district_type               = Town
| district                    = [[East Haddam, Connecticut|East Haddam]]  
| district1                   = 
| district2                   = 
| city                        = 
<!-- *** Geography *** -->  
| location =  
| location_note               = 
| lat_d = 41    | lat_m = 28    | lat_s = 01    | lat_NS = N
| long_d = 72   | long_m = 27   | long_s = 45   | long_EW = W
| coordinates_note = <ref name=gnis/>
| elevation_imperial          = 226
| elevation_round             = 0
| elevation_note              = <ref name=gnis>{{cite gnis|205681|Brainard Homestead State Park}}</ref>
| area_unit                   = acre
| area_imperial               = 25
| area_type                   = 
| area_round                  = 0
| area_note = <ref name=DEEP/>
| area1_imperial = | area1_type = 
| length_imperial = | length_orientation = 
| width_imperial = | width_orientation = 
| highest = | highest_location = | highest_location_note = 
| highest_lat_d = | highest_lat_m = | highest_lat_s = | highest_lat_NS = 
| highest_long_d = | highest_long_m = | highest_long_s = | highest_long_EW =
| highest_elevation_imperial = 
| lowest = | lowest_location = | lowest_location_note = 
| lowest_lat_d = | lowest_lat_m = | lowest_lat_s = | lowest_lat_NS = 
| lowest_long_d = | lowest_long_m = | lowest_long_s = | lowest_long_EW =
| lowest_elevation_imperial = 
<!-- *** Nature *** -->
| biome = | biome_share = | biome1 = | biome1_share = 
| geology = | geology1 = | plant = | plant1 = 
| animal = | animal1 =
<!-- *** People *** -->
| established_type     = Established
| established          = Unspecified
| established_note     =  
| management_body      = Connecticut Department of Energy and Environmental Protection
| management_location =
| management_lat_d = | management_lat_m = | management_lat_s = | management_lat_NS = 
| management_long_d = | management_long_m = | management_long_s = | management_long_EW =
| management_elevation =
| visitation = | visitation_year = | visitation_note =
<!-- *** Map section *** -->
| map                         = Connecticut Locator Map.PNG
| map_caption                 = Location in Connecticut
| map_locator                 = Connecticut
| map_first                   = 
<!-- *** Website *** -->
| website                     = [http://www.ct.gov/deep/cwp/view.asp?a=2716&q=445284#BrainardHomestead Brainard Homestead State Park]
}}

'''Brainard Homestead State Park''', and alternatively '''Brainerd Homestead State Park''', is a {{convert|25|acre|ha|adj=on}} undeveloped [[state park]] located in the town of [[East Haddam, Connecticut]], United States. A farmhouse was built on the site by Timothy Green in 1842 before being purchased by Selden Tyler Brainerd in March 1854. The ownership of the property was willed to Geraldine W. Hayden. Upon her death in 1929, the property was willed to the State of Connecticut with the condition that William Brainerd be able to use the property for life. William Brainerd died in 1936, the buildings were later dismantled, but the Brainard Homestead State Park was established prior to May 1, 1932. The undeveloped park is said to offer bird watching, sports fields and hiking according to the [[Connecticut Department of Energy and Environmental Protection]]. As of 2012, the fields were noted to be actively farmed and there were no established trails for hiking.

== History ==
The known history of Brainard Homestead State Park begins in 1842 when Timothy Green constructed a farmhouse on the property. Green never lived in the farmhouse, but leased it to Jonathan Morgan. In March 1854, the property was sold to Selden Tyler Brainerd. Brainerd with his wife, Harriet, raised five children in the {{convert|30|ft|m}} by {{convert|25|ft|m}} farmhouse. At some point ownership of the property was transferred to Geraldine W. Hayden in a will.<ref name=geo />  In March 1929, Hayden died and willed the property to the State of Connecticut so that the homestead could be utilized as a memorial to her grandfather. A report by the Connecticut State Park and Forest Commission noted that it was not an ideal situation due the concerns about identifying a proposed site in advance and establishing the adaptability of the land for public use. The report also notes that Mrs. Hayden was unknown to the commissioners and despite the land having significant liabilities was waiting to become a state park by 1930.<ref>{{cite web | url=https://books.google.com/books?id=tNxWAAAAMAAJ&pg=RA7-PA18&lpg=RA7-PA18&dq=Geraldine+W.+Hayden&source=bl&ots=z--EE29-d1&sig=TDRSORCWWxTOr7vjTl58EIFYGIA&hl=en&sa=X&ei=mh53VLG7JbiIsQT9rIHwBg&ved=0CDIQ6AEwAw#v=onepage&q=Geraldine%20W.%20Hayden&f=false | title=Report of the State Park and Forest Commission to the Governor (1930) | publisher=State Park and Forest Commission | date=1930 | accessdate=27 November 2014 | pages=15}}</ref> The delay in its transformation stemmed from a request that William Brainerd, one of Selden and Harriet's sons, have life use of the property. William Brainerd died in 1936 and the terms of the will were completed. At a later date, the State of Connecticut dismantled the buildings and established the Brainard Homestead State Park.<ref name=geo /> 

Land use records show that two fields on the farm were once leased and used by Mortimer Gelston, but the fields surrounding or on the Brainard Homestead State Park continue to be actively farmed.<ref name=geo>{{cite web | url=http://www.geocaching.com/geocache/GC1B759_brainard-homestead?guid=6b0e87f9-0bcf-4bfa-b863-40847158c3cd | title=Brainard Homestead | publisher=Geocaching.com | work=From data obtained from East Haddam Town Clerk, who provided a history of the homestead compiled by Karl P. Stofko in 1981 | accessdate=26 May 2014}}</ref> Leary writes that the park preserves the Brainard Homestead and notes that the foundations and cellar pits are visible and provides a picture of the fieldstone foundation.<ref name="leary">{{cite book | title=A Shared Landscape: A Guide & History of Connecticut's State Parks & Forests | publisher=Friends of the Connecticut State Parks, Inc. | author=Leary, Joseph | year=2004 | pages=97| isbn=0974662909}}</ref> 

The establishment date of the Brainard Homestead State Park is unknown, but it predates the death of William Brainard. The 1934 ''State of Connecticut Register and Manual'' lists the Brainard Homestead State Park as the 39th State Park and consists of 25 acres.<ref name=vol34>{{cite web | url=https://archive.org/details/register34conn | title=State of Connecticut Register and Manual (1934) | publisher=State of Connecticut | accessdate=26 May 2014 | pages=227}}</ref> Though it is unspecific, the 1932 ''State of Connecticut Register and Manual'' notes that there were 40 state parks as of May 1, 1932.<ref name=vol32>{{cite web | url=https://archive.org/details/register34conn | title=State of Connecticut Register and Manual (1932) | publisher=State of Connecticut | accessdate=26 May 2014 | pages=757}}</ref>

== Activities ==
The [[Connecticut Department of Energy and Environmental Protection]]  website highlights three activities for Brainard Homestead State Park: bird watching, field sports and hiking. In 2004, Leary noted that the open fields were perfect for field sports and picnics.<ref name=leary /> Use of field for sports seem to have ended soon after because a [[geocache]] placed within Brainard Homestead State Park and referenced on the Geocaching website notes that the fields were used for farming.<ref name=geo /> In 2012, the "The A to Z of CT State Parks " [[Tumblr]] website noted that the fields were unusable and were currently being farmed.<ref name="atoz">{{cite web | url=http://connecticutatoz.tumblr.com/post/24289582997/brainard-homestead-state-park | title=Brainard Homestead State Park | publisher=The A to Z of CT State Parks | date=2012 | accessdate=26 May 2014}}</ref> The Connecticut Department of Energy and Environmental Protection website continues to note the park's "field sports" use as of 2014.<ref name=DEEP /> Also "The A to Z of CT State Parks" states that there were no established trails for hiking.<ref name=atoz /> Leary writes that there are limited views of the [[Connecticut River]] in winter, but the foliage in summer "precludes much of a vista."<ref name=leary /> Al Braden writes in his book that the park provides access to the Salmon River with parking, boat ramp and docks, but this is the "Salmon River Boat Launch East Haddam" and not a part of the state park.<ref>{{cite web | url=https://books.google.com/books?id=uW48m8ifpmAC&pg=PA94&dq=Brainard+Homestead+State+Park&hl=en&sa=X&ei=BcmCU9KKHoTRsQSmh4DoAg&ved=0CDcQ6AEwAg#v=onepage&q=Brainard%20Homestead%20State%20Park&f=false | title=The Connecticut River: A Photographic Journey into the Heart of New England | publisher=Wesleyan University Press | date=2010 | accessdate=26 May 2014 | author=Braden, Al | pages=94}}</ref><ref>{{cite web | url=http://www.ct.gov/deep/cwp/view.asp?A=2686&Q=384278 | title=Salmon River Boat Launch East Haddam | publisher=Connecticut Department of Energy and Environmental Protection | accessdate=26 May 2014}}</ref>

== Notes ==
Despite the family name being Brainerd, the State of Connecticut has used the Brainard spelling since at least 1934.<ref name=geo /><ref name=vol34 /> Official publications to date list the property as Brainard Homestead State Park and as such is not a simple typographical error.<ref name=DEEP />

==References==
{{reflist|refs=
<ref name=DEEP>{{cite web |url=http://www.ct.gov/deep/cwp/view.asp?a=2716&q=445284#BrainardHomestead |title=Brainard Homestead State Park |work=State Parks and Forests |publisher=Connecticut Department of Energy and Environmental Protection |accessdate=March 3, 2013}}</ref>
}}

==External links==
*[http://www.ct.gov/deep/cwp/view.asp?a=2716&q=445284#BrainardHomestead Brainard Homestead State Park] Connecticut Department of Energy and Environmental Protection

{{Protected Areas of Connecticut}}

[[Category:State parks of Connecticut]]
[[Category:Parks in Middlesex County, Connecticut]]
[[Category:East Haddam, Connecticut]]