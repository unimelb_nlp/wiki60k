{{Orphan|date=November 2016}}

'''Kenyan crime fiction''' is a genre of [[crime fiction]] that is set in the country of [[Kenya]],  and   usually written by Kenyan authors. According to G.J. Demko of Dartmouth College, "The fundamental premise of all [crime fiction] is a society that is ordered and real but becomes disordered as a result of a crime imposed on that society. In the normal case, a hero arrives - an officer of the law, a [[private detective]] or an amateur sleuth - and via logical [[deductive reasoning|deduction]], hard work or luck, solves the crime, identifies the perpetrator, and order is restored."<ref>{{cite web|last=Demko|first=G.J.|title=The International Diffusion and Adaptation of the Crime Fiction Genre|url=http://www.dartmouth.edu/~gjdemko/nyaag.htm|work=Blog|accessdate=2 December 2013}}</ref>  However, Kenyan crime fiction differs slightly from Demko’s definition because of the following reasons: crime is often so pervasive in societies portrayed in Kenyan crime novels that it becomes almost a part of the order (i.e., police [[bribes]], corrupt government, etc), and many times crimes are not conclusively solved by the end of the stories, mimicking the real-life cyclical nature of crime.

== Popular Works ==

*''[[Son of Women]]'' by Charles Mangua
*''[[Kill Me Quick]]'' by [[Meja Mwangi]]
*''[[Petals of Blood]]'' by [[Ngugi wa Thiong’o]]
*''[[Black Star Nairobi]]'' by [[Mukoma wa Ngugi]]
*''[[My Life in Crime]]'', ''[[My Life with a Criminal]]'', and ''[[My Life in Prison]]'' by John Kiriamiti
*''[[Links of a Chain]]'' by Monica Genya
*''The Mystery Smugglers'' by [[Mwangi Ruheni]]
*''Shortcut to Hell'' by Paul Kitololo

== Context ==

=== Sociopolitical background of Kenyan crime fiction ===

Understanding the legacy of Kenya’s colonial and [[postcolonial]] history is necessary in examining the historical, social and economic elements of Kenyan crime fiction. In 1895 [[Great Britain]] took up Kenya as a colony and named it the [[East Africa Protectorate]]. In 1963, Kenya gained its independence from the [[British Crown]] and effectively became a republic the following year. The country’s first president, [[Jomo Kenyatta]], a [[Kikuyu people|Kikuyu]], took over property that belonged to wealthy white settlers and redistributed it amongst Kenyans, favoring the Kikuyu in particular. Although he claimed that the country had achieved “political stability” by strengthening its economy through tourism and other reforms, underlying social tensions between the Kikuyu and other minority groups (which counted altogether comprised a 70% majority) remained.

Also, Kenya’s rapid population growth, combined with the migration of people from rural areas to the cities, contributed to high rates of unemployment and law and disorder in urban areas. In 1978, after Kenyatta died, [[Daniel arap Moi]] began his infamous 24-year siege of power. Over the subsequent decades, economic growth declined and dropped under 4%, a dramatic drop from the growth rate of 6% during the early years of Kenyatta’s presidency.<ref>{{cite web|title=About Kenya: Economy|url=http://kenya.um.dk/en/about-kenya-new/economy-new/|publisher=Ministry of Foreign Affairs of Denmark|accessdate=2 December 2013}}</ref>  According to Robert Shaw, a Nairobi economist quoted by the BBC, "From the 1990s, economic growth and the standard of living have declined or stagnated…. Moi's government promised many things but did very little - especially in the department of good governance."<ref>{{cite web|last=Phombeah|first=Gray|title=Moi's Legacy to Kenya|url=http://news.bbc.co.uk/2/hi/africa/2161868.stm|publisher=BBC News: World Edition|accessdate=2 December 2013|date=5 August 2002}}</ref>  As a result, many families fell below the poverty line, and the economic stagnation contributed to an already growing dissatisfaction aggravated by government inefficiency, corruption and ethnic tensions.

== Themes ==

Common themes of postcolonial Kenyan crime fiction include:

=== Corruption ===

Corruption in Kenya spans from Kenyatta to Moi to Mwai Kibaki, and is a very prominent theme found in many Kenyan crime fiction novels. According to the [[Corruption Perceptions Index]], Kenya ranks 139th out of 179 nations. Often corruption manifests as bribes or informal payments to "get things done." However, larger forms of corruption are also present, which are portrayed in crime fiction novels such as Ngugi wa Thiong’o’s ''[[Petals of Blood]]''. In ''Petals of Blood'', Ngugi examines the failure of the postcolonial regime to uphold the principles that drove Kenyan independence: honor, populism and helping the poor. The Member of Parliament in the novel blatantly ignores the requests of the people of Ilmorog, and the businessman Kimeria takes advantage of Wanja and rapes her, symbolizing the African elite’s rape of the continent. Mukoma wa Ngugi’s novel ''Black Star Nairobi'' also highlights how the Kenyan presidential candidates urged young men of different ethnic groups to riot, loot and kill after the 2007 presidential election in the country. In the novel, such rhetoric prompts someone to bomb the Norfolk Hotel.

=== Portrayal of women ===

Kenyan crime fiction generally portrays women as secondary characters, usually as people to be dominated or as sexual objects. In Meja Mwangi’s “[[Kill Me Quick]],” we are presented with two female characters, both only appear once, and both are only mentioned for their relationship to the main characters. Sarah, Razor’s girlfriend, is there for Razor to have sex with in front of everyone, and Delilah, Mania’s girlfriend, mentions her job is a barmaid, which we learn is another term for prostitute. Nici Nelson comments that “All the female characters in Mwangi’s “Kill Me Quick” are no more than ‘screws’ of the main characters.<ref>{{cite journal|last=Nelson|first=Nici|title=Representations of Men and Women, City and Town in Kenyan Novels of the 1970s and 1980s.|journal=African Languages and Cultures|year=1992|volume=9|issue=2|pages=145–168|doi=10.1080/09544169608717807}}</ref>
In Charles Mangua’s Son of Women, we learn that Dodge’s mother and the woman who raised him were both prostitutes, as is the women he falls in love with. These three women are the only ones mentioned in the story for any length.
The opening lines of the novel contain the lines,

“I like to bite women-- beautiful women. Women with tits that bounce. If you do not like the idea you are the type I am least interested in."<ref>{{cite book|last=Mangua|first=Charles|title=Son of Woman|year=1971|publisher=East African Publishing House|location=Nairobi|isbn=9789966465528|page=3}}</ref>
From the beginning, Dodge is telling the reader that “women with tits that bounce” are the only important kind of women, and if you don’t fit that description, you aren’t beautiful and you don’t matter. This theme continues throughout the novel.

=== Socioeconomic status conflicts ===

Prevalent in postcolonial Kenyan crime fiction are conflicts that arise as a result of [[socioeconomic]] status differences. This theme is rooted in the real-life disparity between the rich and poor in postcolonial Kenya, and the interactions between the two social strata. Interestingly, socioeconomic standing in postcolonial Kenya tended to correlate with racial differences, with non-Kenyans largely being the richest and Kenyans accounting for a majority of the poor.

Socioeconomic status conflicts are addressed in postcolonial Kenyan author John Kiriamiti's popular novel ''[[My Life in Crime]]''. When describing a heist in which he is involved, narrator Jack Zollo remarks, "A little distance away, the Whiteman stopped and put his head through the window as if he was suspicious. Then he seemed to think G.G. was harmless and at a slow speed he approached the trap . . . The windscreen of the 404 broke into pieces. The Whiteman's hands were in the air, his eyes wide open. He asked us not to shoot him but to take the whole cash."<ref>{{cite book|last=Kiriamiti|first=John|title=My Life in Crime|year=1984|publisher=Spear Books|location=Kenya|page=53}}</ref> In addition to capitalizing "Whiteman" in order to situate the robbed as a racial "other," the narrator also relays that the "Whiteman" is of a higher social class by mentioning his [[Peugeot 404]] and "whole cash." Wealth and race are clearly inflated and serve as a motivation for less fortunate Kenyans like Zollo to rob those in better socioeconomic standing, who tend to be of a different race.

== Writing style ==

Some writing stylistic differences are present in Kenyan novels. Often, writers use [[exposition (narrative)|exposition]], summarization, and [[description]], rather than the principle [[Show, don't tell|show, don’t tell]] of fiction writing. For example, Monica Genya’s ''[[Links of a Chain]]'' is written in [[third person omniscient]], and Genya’s writing focuses on telling the audience about detailed actions and elaborate backstories. Reviewer for [[World Literature Today]] James Gibbs writes on the novel’s perception of reality: “The author is more interested in action than character or community. As in other examples of the convention, there is, for example, little attempt to show the repercussions of violence on the wider community, and agents undertake casually dangerous assignments with inadequate backup. There is almost no attempt to explore the impact of brutal killings and scenes of carnage on the perpetrators and witnesses. The youthful supersleuth moves away from scene after scene of carnage with little more than a gulp."<ref>{{cite journal|last=Gibbs|first=James|title=Reviewed Work: ''Links of a Chain'' by Monica Genya|journal=World Literature Today|year=1998|volume=71|issue=2|page=438|jstor=40153219|doi=10.2307/40153219}}</ref>

== References ==

{{reflist}}

[[Category:Crime fiction]]
[[Category:Novels set in Kenya]]