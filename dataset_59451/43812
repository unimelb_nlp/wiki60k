{|{{Infobox Aircraft Begin
  |name          =VEF I-17
  |image         = Irbitis_I-17.jpg
  |caption       = VEF I-17 at the VEF factory, 1940.
}}{{Infobox Aircraft Type
  |type          = [[Trainer aircraft]]
  |national origin=[[Latvia]]
  |manufacturer  = [[VEF]]
  |designer      = [[Kārlis Irbītis]]
  |first flight  = 1940
  |introduced    = 1940
  |retired       = 
  |status        = 
  |primary user  =[[Latvian Air Force]]
  |more users    =[[Luftwaffe]]
  |produced      =
  |number built  = 6
  |unit cost     =
}}
|}
'''VEF I-17''' was a [[Latvia]]n [[trainer aircraft]] (intended also as a [[Fighter aircraft|fighter]]) designed in 1939 by [[Kārlis Irbītis]].The I-17 was test flown in early 1940 and almost immediately accepted by [[Latvian Air Force]]. It was produced by the [[VEF]] factory in [[Riga]].

==Design and development==
In 1939 Latvia ordered 39 [[Hawker Hurricane]] fighters from [[United Kingdom]] thus there was a need for [[monoplane]] pilots in Latvia. For this purpose Kārlis Irbītis designed the VEF I-17 among other trainer aircraft. Due to the start of [[Second World War]] in September 1939, the British Hawker Hurricanes never arrived in Latvia and this was an inducement for the Latvian Air force to encourage Latvian aircraft development instead. Due to the pressures of the war, the I-17 prototype was accepted almost without testing and serial production was started.<ref>Edvins Bruvelis. Latvijas Aviacijas Vesture</ref>

Six examples of the I-17 were built and there was an order for another six but that was halted by the [[Soviet occupation of Latvia in 1940|Soviet occupation of Latvia]] in June 1940.

After the occupation the I-17 was tested by Soviet [[Red Army]] and some had Soviet [[Shvetsov M-11|M-11]] engines installed. After the occupation of Latvia by [[Nazi Germany]] in July 1941, the I-17 was also examined and tested by the [[Luftwaffe]] and, like the [[VEF I-16]], was used by the aviation school in [[Torun]].<ref>{{cite web|url=http://www.bruninieki.narod.ru/vefi17.html |title=VEF I√17 Latvian aircraft |publisher=Bruninieki.narod.ru |date=2007-07-26 |accessdate=2014-03-20}}</ref>

The further fate of the VEF I-17s produced is unknown.

==Specifications (I-17)==
{{Aircraft specs
|ref=airwar.ru<ref>{{cite web|url=http://www.airwar.ru/enc/other2/i17.html |title=Irbitis I-17 |publisher=Airwar.ru |date= |accessdate=2014-03-20}}</ref>
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=one passenger
|length m=8.20
|length ft=
|length in=
|length note=
|span m=9.80
|span ft=
|span in=
|span note=
|height m=2.90
|height ft=
|height in=
|height note=
|wing area sqm=19.20
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=790
|empty weight lb=
|empty weight note=
|gross weight kg=1140
|gross weight lb=
|gross weight note=
|fuel capacity=<!-- {{convert|XX|l}} -->
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Menasco Pirate C4]]
|eng1 type=four cylinder, air-cooled, inline, [[four stroke]] [[aircraft engine]]
|eng1 kw=
|eng1 hp=125

|prop blade number=2
|prop name=fixed pitch
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=230
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=592
|range miles=
|range nmi=
|range note=
|endurance=
|ceiling m=5900
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=59.4
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

== References ==
{{commons category|VEF aircraft}}
{{reflist}}
{{VEF aircraft}}

[[Category:Latvian aircraft 1930–1939]]
[[Category:Latvian military trainer aircraft 1930–1939]]
[[Category:Aircraft manufactured in Latvia]]
[[Category:Aircraft 1930–1939]]
[[Category:Propeller aircraft]]
[[Category:Low-wing aircraft]]
[[Category:Monoplanes]]