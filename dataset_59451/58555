{{Infobox Journal
| cover		= [[Image:jasms cover.gif]]
| discipline	= [[Chemistry]]
| abbreviation	= J. Am. Soc. Mass Spectrom.
| frequency     = monthly
| history	= 1990–present
| impact        = 2.945
| impact-year   = 2014
| website	= http://www.springerlink.com/content/1044-0305
| publisher	= [[Springer Science+Business Media]]
| country	= United States
| ISSN		= 1044-0305
}}

The '''''Journal of the American Society for Mass Spectrometry''''' (usually abbreviated as ''J. Am. Soc. Mass Spectrom.'', often nicknamed "''JASMS''"), is a [[peer-review]]ed [[scientific journal]] published since 1990, by [[Springer Science+Business Media]] since 2011; prior to then it was published by [[Elsevier]]. The monthly journal is the official publication of the [[American Society for Mass Spectrometry]] and publishes original research papers covering all aspects of mass spectrometry.

[[Michael L. Gross (chemist)|Michael L. Gross]] ([[Washington University in St. Louis]]) has been Editor-in-Chief since inception of the journal. Associate editors are Veronica Bierbaum ([[University of Colorado at Boulder]]), [[Jennifer S. Brodbelt]] ([[University of Texas at Austin]]), Kelsey Cook ([[University of Tennessee]]), Joseph Loo ([[UCLA]]), and Richard A. J. O'Hair ([[University of Melbourne]]).  The managing editor is Joyce Neff (Washington University in St Louis).<ref name="JASMS Editors">{{cite web|url=http://www.asms.org/docs/jasms/editorial-board-2013.pdf?sfvrsn=0|title=JASMS Editorial Board 2013|accessdate=24 July 2013}}</ref>

Papers are freely available one year after publication.  The journal is indexed on [[MEDLINE]].

==References==
{{reflist}}

[[Category:Delayed open access journals]]
[[Category:Chemistry journals]]
[[Category:Mass spectrometry journals]]
[[Category:Publications established in 1990]]
[[Category:English-language journals]]


{{biochem-journal-stub}}