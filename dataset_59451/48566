{{Infobox organization
|name         = Hope For The Warriors CFC #27800
|image        = Hope_For_The_Warriors_logo.png
|motto        = No sacrifice forgotten, nor need unmet.
|formation    = 2006
|type         = [[501(c)#501.28c.29.283.29|501(c)(3)]] [[corporation]]#20-5182295
|purpose      = Assistance for US service members and their families that have been affected by injuries or death while serving
|leader_title = President/CEO
|leader_name  = Robin Kelleher
|num_staff    = 35 full-time employees
|website      = http://www.hopeforthewarriors.org/
}}

'''Hope For The Warriors''' is a national nonprofit organization in the United States that provides assistance to combat wounded service members, their families, and families of those killed in action. The organization focuses on those involved in [[Operation Iraqi Freedom]] and [[Operation Enduring Freedom]] and their families. Hope For The Warriors recently received a Four-Star Rating from [[Charity Navigator]]<ref>{{citation
  | title = Hope For The Warriors
  | publisher = Charity Navigator
  | accessdate = 2011-11-09
  | url = http://www.charitynavigator.org/index.cfm?bay=search.summary&orgid=13101}}</ref> and was highlighted by George W. Bush in the Warrior Open.<ref>{{citation |title=Military Orgs |publisher=George W. Bush Presidential Center |accessdate=2011-11-17 |url=http://www.warrioropen.com/military-orgs}}</ref>

==About==

Hope For The Warriors was co-founded in 2006 by Robin Kelleher and Shannon Maxwell. They decided to found an advocacy group after Maxwell's husband, Tim Maxwell, was wounded in Iraq and suffered a [[traumatic brain injury]] (TBI).<ref name="CampLejeune">{{citation
  | title = Hope For The Warriors
  | publisher = WTOC 11
  | accessdate = 2011-11-09
  | url = http://www.wtoc.com/story/12854048/hope-for-the-warriors}}</ref> Hope For The Warriors provides aid to veterans in multiple forms, including building or refurbishing homes and providing professional development.<ref name="CampLejeune"/><ref>{{citation
 |title=Homes for the wounded, families 
 |publisher=Marine Corps Base CAMP LEJEUNE, North Carolina 
 |accessdate=2011-11-09 
 |url=http://www.lejeune.usmc.mil/mcb/topStorywarriorhouse.asp 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20120301090321/http://www.lejeune.usmc.mil/mcb/topStorywarriorhouse.asp 
 |archivedate=2012-03-01 
 |df= 
}}</ref> It raises awareness of returning veterans by hosting Run For The Warriors in different areas of the United States,<ref>{{citation
  | title = Harmon Meadow Run For The Warriors
  | publisher = Kintera.org
  | accessdate = 2011-11-09
  | url = http://run.kintera.org/faf/home/default.asp?ievent=456720&lis=1&kntae456720=6FF09C38301C4F65933B43C0CD9E75F0}}</ref> and by hosting athletes in Team Hope For The Warriors, who run or [[handcycle]] in the Marine Corps Marathon and the New York City Marathon or compete in triathlons, in order to raise money for the non-profit organization.<ref>{{citation
  | title = ASA and Hope For The Warriors Recognize Wounded Warriors Sacrifices During ANESTHESIOLOGY 2011
  | publisher = NewsWise
  | accessdate = 2011-11-09
  | url = http://www.newswise.com/articles/asa-and-hope-for-the-warriors-recognize-wounded-warriors-sacrifices-during-anesthesiology-2011}}</ref><ref>{{citation
 |title=Team Hope For The Warriors 
 |publisher=Hope For The Warriors 
 |accessdate=2011-11-09 
 |url=http://www.hopeforthewarriors.org/mcm2010.html 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20120425230738/http://www.hopeforthewarriors.org/mcm2010.html 
 |archivedate=2012-04-25 
 |df= 
}}</ref><ref>{{citation
  | title = 2011 Team Hope For The Warriors 2011 ING NYC Marathon
  | publisher = Kintera
  | accessdate = 2011-11-14
  | url = http://team.kintera.org/faf/home/default.asp?ievent=455012}}</ref> Hope For The Warriors was a charity partner for both the 2011 [[Marine Corps Marathon]] and the 2011 [[New York City Marathon]].<ref>{{citation
  | title = MCM Charity Partners
  | publisher = Marine Corps Marathon
  | accessdate = 2011-11-14
  | url = http://www.marinemarathon.com/Register/MCM_Charity_Partners.htm}}</ref><ref>{{citation
  | title = Silver Charities
  | publisher = The ING New York City Marathon
  | accessdate = 2011-11-14
  | url = http://www.ingnycmarathon.org/charities/charity_silver_bronze.shtm}}</ref>

Hope For The Warriors was mentioned on Salon.com in an article called [http://www.salon.com/2011/11/24/how_to_give_back_this_thanksgiving/ "How to give back this Thanksgiving"]. The article cites Hope For The Warriors as a worthy military charity to donate to based on its Charity Navigator rating.<ref>{{citation
  | title = How to give back this Thanksgiving
  | publisher = Salon.com
  | accessdate = 2011-11-30
  | url = http://www.salon.com/2011/11/24/how_to_give_back_this_thanksgiving/}}</ref>

==Notable Board Members==
The Hope For The Warriors website lists all of the nonprofit's board of directors.<ref>{{citation
  | title = Board of Directors
  | publisher = Hope For The Warriors
  | accessdate = 2011-11-30
  | url = http://www.hopeforthewarriors.org/story/18882788/board-of-directors}}</ref> Some of its most notable board members include [[Jack Marin]], [http://www.defense.gov/news/newsarticle.aspx?id=66049 Capt. Dan Moran],<ref>{{citation
  | title = Face of Defense: Wounded Warrior Helps Fellow Veterans
  | publisher = U.S. Department of Defense
  | accessdate = 2011-11-30
  | url = http://www.defense.gov/news/newsarticle.aspx?id=66049}}</ref> General [[Charles Krulak]], General [[Richard Cody]], [[Charlie Summers]], and [[Gary Sinise]].

==References==
{{reflist}}

[[Category:United States military support organizations]]
[[Category:Organizations established in 2006]]
[[Category:Charities based in Virginia]]