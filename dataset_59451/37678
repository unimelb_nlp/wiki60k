{{DISPLAYTITLE:Ben Franklin (''The Office'')}}
{{Infobox television episode
| title        =Ben Franklin
| series       =[[The Office (U.S. TV series)|The Office]]
| image        =
| caption      =
| season       =3
| episode      =15 <!-- THIS IS THE 15TH EPISODE OF THE SEASON. HOUR-LONG EPISODES ARE CONSIDERED TWO EPISODES. PLEASE DO NOT CHANGE EPISODE NUMBERS IN THIS ARTICLE. -->
| airdate      =February 1, 2007
| production   =315
| writer       =[[Mindy Kaling]]
| director     =[[Randall Einhorn]]
| guests       =*[[Creed Bratton]] as [[Creed Bratton (character)|Creed Bratton]]
*[[Rashida Jones]] as [[Karen Filippelli]]
*[[David Koechner]] as [[Todd Packer]]
*[[Andy Daly]] as Gordon
*Jackie Debatin as Elizabeth
*[[Craig Robinson (actor)|Craig Robinson]] as [[Darryl Philbin]]
*[[Robert R. Shafer|Bobby Ray Shafer]] as [[Bob Vance (fictional character)|Bob Vance]]
| episode_list =[[List of The Office (U.S. TV series) episodes|List of ''The Office'' (U.S.) episodes]]
| season_list  =
| prev         =[[The Return (The Office)|The Return]]
| next         =[[Phyllis' Wedding]]
}}

"'''Ben Franklin'''" is the fifteenth episode of the [[The Office (U.S. season 3)|third season]] of the [[United States|American]] [[comedy]] [[television program|television series]] ''[[The Office (U.S. TV series)|The Office]]'', and the show's forty-third episode overall. Written by [[Mindy Kaling]], who also acts in the show as [[Kelly Kapoor]], and directed by [[Randall Einhorn]], the episode first aired in the [[United States]] on February 1, 2007, on [[NBC]].  "Ben Franklin" received 5.0/13 in the ages 18–49&nbsp;[[demographic]] of the [[Nielsen ratings]], and was watched by an estimated audience of 10.1&nbsp;million viewers,<ref name="Tan"/> and the episode received mixed reviews among critics.

In the episode, the employees from the office prepare for [[Phyllis Vance|Phyllis Lapin]]'s ([[Phyllis Smith]]) wedding. [[Michael Scott (The Office)|Michael Scott]] ([[Steve Carell]]), acting under advice from [[Todd Packer]] ([[David Koechner]]), instructs [[Dwight Schrute]] ([[Rainn Wilson]]) to hire a stripper for the men and delegates [[Jim Halpert]] ([[John Krasinski]]) to hire one for the women. While Dwight hires a stripper, Jim ends up hiring a [[Ben Franklin]] impersonator ([[Andy Daly]]) instead.

==Plot==
The office plays host to a bridal shower for [[Phyllis Vance|Phyllis Lapin]] ([[Phyllis Smith]]) while [[List of characters from The Office (US T.V. series)#Bob Vance|Bob Vance's]] ([[Robert R. Shafer]]) bachelor party is held in the warehouse. Though [[Michael Scott (The Office)|Michael Scott]] ([[Steve Carell]]) is aware that the company's [[sexual harassment]] policy forbids strippers in the workplace, [[Todd Packer]] ([[David Koechner]]) convinces him that it is okay to hire a stripper for the bachelor party if he also gets a male stripper for the bridal shower. [[Pam Beesly]] ([[Jenna Fischer]]) remarks to [[Jim Halpert]] ([[John Krasinski]]) that he has seemed tired at work. He explains that he has been having long talks with [[Karen Filippelli]] ([[Rashida Jones]]) the last five nights. Pam assumes Jim is using "talk" as a euphemism for sex, making the conversation turn awkward.

Michael delegates the hiring of the strippers to [[Dwight Schrute]] ([[Rainn Wilson]]) and Jim. While Dwight locates [[List of characters from The Office (US T.V. series)#Elizabeth the Stripper|a stripper named Elizabeth]] (Jackie Debatin) for Bob Vance's party, Jim orders an educational [[Benjamin Franklin]] impersonator ([[Andrew Daly]]) as a joke. Pam and Karen have fun gently heckling the impersonator. Jim tells Dwight that the impersonator is the real Ben Franklin. Dwight says he's "99% sure" this is not so but asks questions to the impersonator that he believes only the real Ben Franklin would know. The well-practiced impersonator answers each question correctly without hesitation, much to Dwight's anger and disbelief. In the break room, Karen confronts Pam about [[Casino Night|Jim and Pam's kiss]] and asks if she is still interested in Jim. Made nervous by the questioning, Pam uses misleading wording, babbles uncontrollably, and at the end of the conversation blurts out "I'm sorry." Karen finds her behavior baffling but not suspicious.

At the bachelor party, Bob Vance refuses a [[lap dance]], so Michael volunteers. During the lap dance, Michael becomes concerned that this is cheating on [[Jan Levenson]] ([[Melora Hardin]]) and brings the show to an abrupt close. Dwight decides to get the "three hours of work" they paid the stripper for by having her answer calls. After consulting with "Ben Franklin" and the stripper, Michael decides to tell Jan about the lap dance. However, Jan is less upset by his unfaithfulness to her than by his violation of company policy. Relieved, Michael makes lovey dovey talk to Jan even as she suggests firing him over the incident.

The Ben Franklin impersonator flirts with Pam at her desk. Jim hears about this and teasingly asks if things are getting serious. This makes Pam embarrassed by her lack of a boyfriend, and she asks [[Ryan Howard (The Office)|Ryan Howard]] ([[B. J. Novak]]) to set her up with some of his friends from business school.

==Production==
"Ben Franklin" was the second episode of the series directed by [[Randall Einhorn]]. Einhorn had previously directed "[[Initiation (The Office)|Initiation]]", as well as the summer [[Spin-off (media)|spin-off]] [[webisodes]] "[[The Accountants]]". "Ben Franklin" was written by [[Mindy Kaling]], who acts on the show as customer service representative [[Kelly Kapoor]].<ref name="NBC">{{cite web |url=http://www.nbc.com/The_Office/episodes/season4/3015/ |title=<nowiki>"Ben Franklin" &#124; Season 3 &#124; 02/01/2007</nowiki> |accessdate=2008-07-28 |work= |publisher=[[NBC]] |date= }}</ref>  The episode was the sixth of the series written by Kaling.

[[Jackie Debatin]], who appeared in "Ben Franklin" as Elizabeth, is used to playing strippers and hookers. Debatin had previously been a stripper on ''[[Friends]]'', a stripper on ''[[That '70s Show]]'', a stripper on ''[[Two and a Half Men]]'', a madam on ''[[Veronica Mars]]'', and a call girl on ''[[Boston Legal]]''. In an interview, Debatin said playing Elizabeth was "probably the best experience I have had in TV work", because the cast and crew were "down to earth, fun, grateful to be there".<ref name="Debatin">{{cite web |url=http://twocentsbranchoffice.blogspot.com/2008/05/scrantonbranchoffice-exclusive_15.html |title= ScrantonBranchOffice Exclusive Interview – Jackie Debatin |accessdate=2008-07-28 |last=Debatin |first=Jackie |coauthors= |date=2008-05-15 |work= |publisher=TheTwoCents}}</ref> Although actor [[Andrew Daly]], who played Gordon the [[Ben Franklin]] impersonator, had already known [[John Krasinski]], [[Angela Kinsey]], [[B. J. Novak]] and [[Kate Flannery]], he said that ''The Office'' cast and crew "could not have been more welcoming to me." Daly especially liked it when the actors "depart[ed] from the script and improvised a little."<ref name="Daly">{{cite web |url=http://twocentsbranchoffice.blogspot.com/2007/09/two-cents-five-questions-with_06.html |title= Two Cents & Five Questions With...  |accessdate=2008-07-28 |last=Daly |first=Andy |coauthors= |date=2007-09-06 |work= |publisher=TheTwoCents}}</ref>

==Reception==
"Ben Franklin" received 5.0/13 in the ages 18–49&nbsp;demographic of the [[Nielsen ratings]]. This means that five percent of all households with an 18- to 49-year-old living in it watched the episode, and 13 percent had their television tuned to the channel at any point. The episode was watched by an estimated audience of 10.1&nbsp;million viewers.<ref name="Tan">{{cite web |url=http://www.officetally.com/the-office-nielsen-ratings/2/ |title=''The Office'' Nielsen Ratings |accessdate=2008-07-28 |last=Tan |first=Jeannie |coauthors= |date=2006-09-26 |work= |publisher=OfficeTally.com}}</ref> "Ben Franklin" is one of only a handful of other episodes of The Office to reach over 10 million viewers, the others being the show's [[Pilot (The Office)|pilot episode]], "[[The Injury]]," "[[Traveling Salesmen]]," "[[The Return (The Office)|The Return]]," and "[[Stress Relief (The Office)|Stress Relief]]," of which the latter reached over 20 million viewers.

"Ben Franklin" received mixed, but mostly good, reviews from critics. [[IGN]]'s Brian Zoromski wrote that "''The Office''  was in truly excellent form this week." Zoromski went on to credit the "great progression" in the Jim-Pam-Karen love triangle and Michael being "his completely inept self" as parts of the episode that made it "one of the best ''Office'' episodes this season".<ref name="Zoromski">{{cite web |url=http://tv.ign.com/articles/760/760917p1.html |title=''The Office'': "Ben Franklin" Review |accessdate=2008-07-28 |last=Zoromski |first=Brian |coauthors= |date=2007-02-02 |work= |publisher=[[IGN]]}}</ref> Michael Sciannamea of [[TV Squad]] was less enthusiastic about the episode, writing that although it was "solid", "not much happened here other than the Jim-Karen-Pam triangle."<ref name="Sciannamea">{{cite web |url= http://www.tvsquad.com/2007/02/02/the-office-ben-franklin/ |title= ''The Office'': "Ben Franklin" |accessdate=2008-07-28 |last=Sciannamea |first=Michael |coauthors= |date=2007-02-02 |publisher=[[TV Squad]]| archiveurl= https://web.archive.org/web/20080828200012/http://www.tvsquad.com/2007/02/02/the-office-ben-franklin/| archivedate= 28 August 2008 <!--DASHBot-->| deadurl= no}}</ref>  Abby West, of ''[[Entertainment Weekly]]'', praised the love triangle, saying "The Pam/Karen confrontation was as uncomfortable as I could possibly hope for. It's so gratifying to see Pam's armor cracking."  West also praised the work of the supporting cast.<ref>{{cite news |url=http://www.ew.com/ew/article/0,,20010759,00.html |title=The Naked Truth  |accessdate=2008-07-29 |last=West |first=Abby |coauthors= |date= 2007-02-04|work= |publisher=''[[Entertainment Weekly]]''}}</ref>

==Notes==

*This episode begins [[Andy Bernard]]'s 4-episode absence from the office, as he has gone to anger management. He later returns in "[[The Negotiation]]".

==References==
{{Reflist}}

==External links==
*[https://web.archive.org/web/20121113211839/http://www.nbc.com/the-office/episode-guide/season-3/59065/ben-franklin/episode-315/59302/ "Ben Franklin"] at NBC.com
*{{IMDb episode|0954759|Ben Franklin}}
*{{Tv.com episode|959030}}

{{Theofficeus}}
{{TheofficeusEpisodes}}

{{good article}}

[[Category:The Office (U.S. season 3) episodes]]
[[Category:2007 television episodes]]