{{good article}}
{{Infobox ice hockey player
| image = Frank_"Mr._Zero"_Brimsek.jpg
| image_size = 320x500
| position = [[Goaltender]]
| played_for = [[Boston Bruins]]<br>[[Chicago Black Hawks]]
| shoots =
| catches = Left  
| height_ft = 5
| height_in = 9
| weight_lb = 170
| birth_date = {{birth date|1915|9|26|mf=y}}
| birth_place = [[Eveleth, Minnesota|Eveleth]], [[Minnesota|MN]], [[United States|USA]]
| death_date = {{death date and age|1998|11|11|1915|9|26}}
| death_place = [[Virginia, Minnesota|Virginia]], [[Minnesota|MN]], [[United States|USA]]
| career_start = 1938
| career_end = 1950
| halloffame = 1966
}}
'''Francis Charles "Mr. Zero" Brimsek''' (September 26, 1915 &ndash; November 11, 1998) was an [[Americans|American]] professional [[ice hockey]] [[goaltender]] who played ten seasons in the [[National Hockey League]] (NHL) for the [[Boston Bruins]] and [[Chicago Black Hawks]]. Brimsek started his career playing exhibition games with the [[Pittsburgh Yellow Jackets]] in 1934–35. The [[Detroit Red Wings]] initially owned Brimsek's rights before they traded them to the Bruins in 1937. In [[1938–39 NHL season|1938–39]], Brimsek made his NHL debut with the Bruins since their starting goalie, [[Tiny Thompson]], sustained an injury during an exhibition match. Initially deemed as a short term call-up, Brimsek soon found himself as the Bruins' starting goalie. He went on to record six [[shutout (ice hockey)|shutouts]] in his first seven games which earned him the moniker of "Mr. Zero".

Brimsek spent his first nine NHL seasons with the Bruins and during this time, he received numerous individual awards. He won the [[Calder Memorial Trophy]], the [[Vezina Trophy]] twice, and he was named to the [[NHL All-Star Team]] eight times (twice on the First Team and six times on the Second Team). He was also a member of two [[Stanley Cup]] championships ([[1939 Stanley Cup Finals|1939]] and [[1941 Stanley Cup Finals|1941]]). During [[World War II]], Brimsek left the NHL for two years in order to serve with the [[United States Coast Guard]] skating with the vaunted [[U.S. Coast Guard Cutters (ice hockey)|Coast Guard Cutters]] which featured fellow [[Eveleth, Minnesota|Eveleth]] native [[John Mariucci]]. After being discharged from the Coast Guard, Brimsek returned to the NHL for the [[1945–46 NHL season|1945–46 season]] where it was noted that his skills were not as sharp as before he left. Despite this, he still managed to be one of the best goalies in the league. Brimsek was traded to the Black Hawks following the [[1948–49 NHL season|1948–49]] season due to personal reasons. He spent one season with the Black Hawks before retiring from professional hockey.

One of the most accomplished American goalies of all-time, Brimsek held the record for most wins, and shutouts recorded by an American netminder at the time of his retirement. His wins record stood for 54 years while his shutouts record stood for 61 years. His eight berths to the NHL All-Star Team rank him second all-time among all goalies. In 1966, he was inducted into the [[Hockey Hall of Fame]], the first American goalie to be inducted; and in 1973, he was part of the inaugural class of the [[United States Hockey Hall of Fame]]. In 1998, Brimsek was ranked number 67 on [[List of 100 greatest hockey players by The Hockey News|''The Hockey News''' list of the 100 Greatest Hockey Players]], the highest ranked American goaltender.

==Background==
Brimsek was born in the hockey hotbed of [[Eveleth, Minnesota|Eveleth]], [[Minnesota]] on September 26, 1915.<ref>{{cite news |url=https://news.google.com/newspapers?nid=2250&dat=19721213&id=G09lAAAAIBAJ&sjid=x5MNAAAAIBAJ&pg=1724,4237776|work=The Ely Echo|title=Hockey Hall of  Fame develops|date=1972-12-13|page=18|accessdate=2013-10-15}}</ref> His parents were of [[Slovenes|Slovene]] descent.<ref>{{cite news |url=https://news.google.com/newspapers?nid=1129&dat=19381221&id=lbhRAAAAIBAJ&sjid=a2kDAAAAIBAJ&pg=4491,4578372|work=[[The Pittsburgh Post-Gazette]]|title=Brimsek only 36 minutes from new hockey shutout record|last=Sell|first=Jack|date=1938-12-21|page=44|accessdate=2014-03-14}}</ref> The town of Eveleth produced at that time four other hockey players who would play in the [[National Hockey League]] (NHL): [[Mike Karakas]], [[Sam LoPresti]], [[Al Suomi]] and [[John Mariucci]].<ref>{{cite news |url=http://www.evelethyouthhockey.com/page/show/48034-history-of-eveleth-hockey|work=Eveleth Youth Hockey|title=History of Eveleth Hockey|accessdate=2016-01-10}}</ref>  Brimsek and Karakas played on the same baseball team in high school.<ref name="one on one">{{cite news |url=http://www.hhof.com/htmlSpotlight/spot_oneononep196604.shtml|work=Hockey Hall of Fame|title=One on one with Frank Brimsek|accessdate=2013-10-14}}</ref> Brimsek first started playing hockey when his brother, John, the second-string [[goalie (ice hockey)|goalie]] on the Eveleth High School team, expressed his desire to be a [[defenseman]] instead. John was moved to his desired position, while Frank replaced him.<ref name="book">{{cite book|last=Fischler|first=Stan|title=Boston Bruins: Greatest Moments and Players|publisher=Sports and Publishing LLC|year=2001|location=Champaign, Illinois|pages=41, 238|isbn=1582613745}}</ref> Soon, Brimsek found himself spending most of his spare time on the Eveleth rinks playing hockey. Unlike most of his friends who wanted to be high-scoring [[forward (ice hockey)|forwards]], Brimsek never showed any desire to play any other position except for goalie. Just before winter, Brimsek 
and his friends would get on a dry lot, and they would practice shooting at him.<ref>{{cite news |url=https://news.google.com/newspapers?nid=1946&dat=19800304&id=YJ0xAAAAIBAJ&sjid=pqQFAAAAIBAJ&pg=1460,2083131|work=[[The Montreal Gazette]]|title=U.S hockey gold stirs memory of Mr. Zero|last=Carroll|first=Dink|date=1980-03-04|page=36|accessdate=2013-10-14}}</ref> After graduating from high school, Brimsek went to play for the [[St. Cloud State Teachers College]] hockey team.<ref>{{cite news |url=http://www.ushockeyhalloffame.com/page/show/815934-frank-c-mr-zero-brimsek|work=United States Hockey Hall of Fame|title=Frank C. "Mr. Zero" Brimsek|accessdate=2014-03-14}}</ref> He also graduated from college with a machine shop student's degree.<ref>{{cite news |url=https://news.google.com/newspapers?nid=1129&dat=19360121&id=CbpRAAAAIBAJ&sjid=i2kDAAAAIBAJ&pg=4371,3072847|work=The Pittsburgh Post-Gazette|title=Yellow Jacket players quite handy lot to have around, whether they're on ice skates or not|last=Taggart|first=Bert P.|date=1936-01-21|page=20|accessdate=2014-03-14}}</ref>

==Playing career==
===Early career===
In the fall of 1934, Brimsek was invited to the [[Detroit Red Wings]] training camp for a shot at playing in the [[National Hockey League]] (NHL). [[Jack Adams]], the Red Wings' coach and manager, made a bad impression on Brimsek. He felt that Adams had a habit of favoritism.<ref name="one on one"/> This led him to try out for another professional team, the [[Baltimore Orioles (ice hockey)|Baltimore Orioles]] of the [[Eastern Amateur Hockey League]] (EAHL). Unfortunately for Brimsek, the Orioles decided to cut him. Disappointed, Brimsek hitchhiked back to Eveleth.<ref name="book"/> On his way back home, he had a chance meeting with the owner of the [[Pittsburgh Yellow Jackets]], John H. Harris.<ref name="Harris">{{cite news |url=https://news.google.com/newspapers?nid=1601&dat=19451017&id=rPE6AAAAIBAJ&sjid=fyoMAAAAIBAJ&pg=727,20819870|work=[[Toronto Daily Star]]|title=How Pittsburgh John out-witted (heels) of hockey!|last=Perlove|first=Joe|date=1945-10-17|page=8|accessdate=2014-03-14}}</ref> The Yellow Jackets were in need for a goaltender and Harris proceeded to sign Brimsek to the team.<ref name="book"/> Brimsek started for the Yellow Jackets for the first time in 1934–35 by playing 16 games exhibition games. Out of those 16 games, he won 14 of them.<ref name="hr brimsek">{{cite news |url=http://www.hockey-reference.com/players/b/brimsfr01.html|work=Sports Reference|title=Frank Brimsek|accessdate=2013-10-14}}</ref>

The next season, the Yellow Jackets joined the EAHL.<ref>{{cite news |url=https://news.google.com/newspapers?nid=1946&dat=19351029&id=iYg1AAAAIBAJ&sjid=-ZgFAAAAIBAJ&pg=7004,3456391|work=The Montreal Gazette|title=U.S Hockey League to operate with 5 clubs|date=1935-10-29|page=13|accessdate=2014-03-14}}</ref> Brimsek finished with the most wins and shutouts of any goal tender in the league with 20 and 8 respectively. At the end of the season, he was named to the league's Second All-Star Team and he was awarded the George L. Davis Trophy for having the lowest [[goals against average]] (GAA).<ref name="one on one"/> Impressed by Brimsek, Harris wanted to protect his interests in the goalie so he ironically had the Red Wings put Brimsek on their protected list. Harris then tried to get the Red Wings to call Brimsek up.<ref name="Harris"/> However, the Red Wings wanted Brimsek to first play one year for their amateur team in [[Pontiac, Michigan|Pontiac]], but Brimsek turned down the offer. Brimsek opted to stay with the Yellow Jackets instead.<ref>{{cite news |url=https://news.google.com/newspapers?nid=1144&dat=19371210&id=R2IbAAAAIBAJ&sjid=P0wEAAAAIBAJ&pg=4000,3094789|work=[[The Pittsburgh Press]]|title=Ex-Jacket clears all but 17 shots|last=Burcky|first=Claire M.|date=1937-12-10|page=54|accessdate=2014-03-14}}</ref> Harris then shopped Brimsek around the NHL until he was accepted by the [[Boston Bruins]] in October 1937. The Bruins were already well established in net with future hall-of-famer [[Tiny Thompson]]. This led to Brimsek being assigned by the Bruins to the [[Providence Reds]] of the [[International-American Hockey League]] (IAHL) for the [[1937–38 AHL season|1937–38 season]]. In his only full season with the Reds, Brimsek helped his team win the [[Calder Cup]] and he was named to the league's First All-Star Team.<ref name="one on one"/>

===Boston Bruins===
====Pre World War II====
Brimsek started the [[1938–39 AHL season|1938–39 season]] with the Reds, but he would not stay long with them. During an NHL exhibition game, Thompson got injured and it was unlikely that Thompson would recuperate in time for the beginning of the [[1938–39 NHL season|regular season]]. Needing a replacement, Brimsek was called up by the Bruins.<ref>{{cite news |url=https://news.google.com/newspapers?nid=1129&dat=19361220&id=j7hRAAAAIBAJ&sjid=a2kDAAAAIBAJ&pg=3767,5081205|work=The Pittsburgh Post-Gazette|title=Brimsek strong contender for coveted hockey trophy|date=1938-12-20|page=18|accessdate=2014-03-15}}</ref> In his NHL debut, Brimsek helped his team defeat the [[Toronto Maple Leafs]] by a score of 3–2. He started in net for one more game, a 4–1 victory against the Red Wings, before being sent back down to the Reds as Thompson recovered. [[Art Ross]], the Bruins' coach and general manager, had seen enough of Brimsek during his two stints in Boston to contemplate promoting Brimsek full-time with the Bruins.<ref>{{cite news |url=https://news.google.com/newspapers?nid=1144&dat=19381031&id=cSEbAAAAIBAJ&sjid=I0wEAAAAIBAJ&pg=1737,4227254&hl=en|work=The Pittsburgh Press|title=Sports Stew–Served Hot|last=Burcky|first=Claire M.|date=1938-10-31|page=22|accessdate=2016-01-11}}</ref> Thompson was traded to the Red Wings after playing only five games with the Bruins.<ref>{{cite news |url=https://news.google.com/newspapers?nid=1243&dat=19381125&id=dypPAAAAIBAJ&sjid=zh8EAAAAIBAJ&pg=5325,1415934|work=[[The Bulletin (Bend)|The Bend Bulletin]]|title=Sport tabloids|date=1938-11-25|page=12|accessdate=2014-03-15}}</ref> Ross then proceeded to promote Brimsek as the team's new starting goalie.<ref>{{cite news |url=https://news.google.com/newspapers?nid=1928&dat=19381213&id=kyQgAAAAIBAJ&sjid=gWoFAAAAIBAJ&pg=3245,5812056|work=[[The Lewiston Daily Sun]]|title=Bruins march on as Frankie Brimsek gets his fourth shutout win|date=1938-12-13|page=41|accessdate=2014-03-15}}</ref> This did not sit well with Bruins fans as Thompson was a favorite and he was the reigning [[Vezina Trophy]] winner.<ref name="one on one"/>

In Brimsek's first game as the starting goalie, his team fell 2–0 to the [[Montreal Canadiens]] in [[Montreal]]. Meanwhile, on that same night, Thompson won his first game with the Red Wings.<ref name="brimsek overcomes">{{cite news |url=https://news.google.com/newspapers?nid=1946&dat=19381214&id=4CIyAAAAIBAJ&sjid=ZqgFAAAAIBAJ&pg=6527,1977404|work=The Montreal Gazette|title=Brimsek overcomes hostility of Boston's fandom|last=McNeil|first=Marc T.|date=1938-12-14|page=16|accessdate=2014-03-15}}</ref> Also, Brimsek wore red hockey pants instead of the team's colors, and he was wearing Thompson's former [[jersey number]], No. 1. These little details did not help him improve his image with the fans.<ref name="NY Times">{{cite news |url=https://www.nytimes.com/1998/11/13/sports/frankie-brimsek-85-a-hall-of-fame-goalie.html|work=[[The New York Times]]|title=Frankie Brimsek, 85, a Hall of Fame goalie|last=Goldstein|first=Richard|date=1998-10-13|accessdate=2014-03-15}}</ref><ref name="book2">{{cite book|last=Keane|first=Kerry|title=Tales from the Boston Bruins|publisher=Sports and Publishing LLC|year=2003|location=Champaign, Illinois|page=23|isbn=1582615659}}</ref> The next game yielded a more positive result for Brimsek, as he [[shutout (ice hockey)|shutout]] the [[Chicago Black Hawks]]. This did not earn him the acknowledgement of Bruins fans yet as when he made his first appearance in Boston, the fans greeted him coldly. However, after shutting out his opponents for the second straight game, the fans warmed up to him immediately.<ref name="brimsek overcomes"/> Brimsek earned six shutouts in his first seven games, leading to the fans and the media calling him by the nickname of "Mr. Zero". During that seven game span, he also set a then modern-NHL record for longest shutout streak of 231 minutes and 54 seconds.<ref name="book2"/> At the end of the regular season, Brimsek had backstopped the Bruins to a first-place finish in the league. Brimsek finished the season with the most wins (33), shutouts (10) and the lowest GAA (1.56) in the league.<ref name="hr brimsek"/> In the playoffs, Brimsek and his team defeated the [[New York Rangers]] before beating the Maple Leafs in the [[1939 Stanley Cup Finals]].<ref>{{cite news |url=http://www.hockey-reference.com/leagues/NHL_1939.html|work=Sports Reference|title=1938–39 NHL season summary|accessdate=2013-10-14}}</ref> Adding to his [[Stanley Cup]] victory, Brimsek was awarded the [[Calder Memorial Trophy]], the Vezina Trophy, and he was named to the [[NHL First All-Star Team]].<ref name="hr brimsek"/>

The [[1939–40 NHL season|following season]], Brimsek finished first in the league in wins again and he was named to the [[NHL Second All-Star Team]]. This would be his first of six berths to the NHL Second All-Star Team.<ref name="hr brimsek"/> The Bruins were eliminated in the playoffs by the eventual Stanley Cup champions, New York Rangers, in the semi-finals.<ref>{{cite news |url=http://www.bostonglobe.com/sports/2013/05/14/bruins-rangers-playoff-history/qKI4HVYSY6Xo7lhDzJ1yGN/story.html|work=[[The Boston Globe]]|title=Bruins-Rangers playoff history|date=2013-05-13|accessdate=2014-03-14}}</ref> In [[1940–41 NHL season|1940–41]], Brimsek backstopped the Bruins to their third consecutive first-place finish in the league. The Bruins made it to the [[Stanley Cup Finals|1941 Stanley Cup Finals]] and were matched up with the Red Wings. The Red Wings fell in four games in a best-of-seven series giving Brimsek his second and last Stanley Cup victory.<ref>{{cite news |url=http://www.hockey-reference.com/leagues/NHL_1941.html|work=Sports Reference|title=1940–41 NHL season summary|accessdate=2015-05-04}}</ref> For his efforts during the regular season, Brimsek was named to the NHL Second All-Star Team for the second year in a row.<ref name="one on one"/> Continuing on the previous season's success, Brimsek won the Vezina Trophy and was named to the NHL First All-Star Team for the second time in his career.<ref>{{cite news |url=https://news.google.com/newspapers?nid=1913&dat=19420318&id=g6g0AAAAIBAJ&sjid=32kFAAAAIBAJ&pg=2677,5978582&hl=en|work=[[Lewiston Evening Journal]]|title=Frankie Brimsek wins Vezina Trophy for second time|date=1942-03-18|page=5|accessdate=2015-04-29}}</ref> However, Brimsek's team could not replicate their playoff success as they were eliminated by the Red Wings in the semi-finals.<ref>{{cite news |url=http://www.hockey-reference.com/leagues/NHL_1942.html|work=Sports Reference|title=1941–42 NHL season summary|accessdate=2015-05-04}}</ref>

====World War II and aftermath====
Following the outbreak of [[World War II]], three of the Bruins' best [[Forward (ice hockey)|forwards]] decided to join the [[Royal Canadian Air Force]] midway through the [[1941–42 NHL season|1941–42 season]].<ref>{{cite news |url=https://news.google.com/newspapers?nid=2194&dat=19420128&id=270vAAAAIBAJ&sjid=ttsFAAAAIBAJ&pg=4117,5225895&hl=en|work=[[Ottawa Citizen]]|title=Boston's famous "Kraut" Line to enter Royal Canadian Air Force|date=1942-01-28|page=12|accessdate=2015-04-29}}</ref> These three forwards made up the [[Kraut Line]], one of the best lines in the NHL. Despite missing three of their best players, the Bruins managed to advance to the [[1943 Stanley Cup Finals|Stanley Cup Finals]] the [[1942–43 NHL season|next season]]. In the finals, they were eliminated by the Red Wings, four games to none.<ref>{{cite news |url=https://news.google.com/newspapers?nid=1499&dat=19430409&id=dU4aAAAAIBAJ&sjid=_iIEAAAAIBAJ&pg=2809,3947032&hl=en|work=[[Milwaukee Journal Sentinel]]|title=Wings win Stanley Cup in four straight|date=1943-04-09|page=40|accessdate=2015-04-29}}</ref> Due to his play in the regular season, Brimsek was again named to the NHL Second All-Star Team. However, it was popular opinion at the time that Brimsek deserved the spot on the First All-Star Team over [[Johnny Mowers]], including Mowers' own coach and general manager, Jack Adams.<ref>{{cite news |url=https://news.google.com/newspapers?id=NslGAAAAIBAJ&sjid=H_gMAAAAIBAJ&pg=1758,2016926&hl=en|work=[[The Day (New London)|The Day]]|title=Brimsek rated far over Mowers as goaltender|last=King|first=Bill|date=1943-01-03|page=8|accessdate=2015-04-29}}</ref><ref>{{cite news |url=https://news.google.com/newspapers?id=RK5TAAAAIBAJ&sjid=czgNAAAAIBAJ&pg=5776,3397174&hl=en|work=[[Leader-Post]]|title=Along the sport byways|last=Edwards|first=Charlie|date=1943-04-08|page=17|accessdate=2015-04-29}}</ref>

Brimsek decided to help the war effort next season by joining the [[United States Coast Guard]]. During his time with the Coast Guard, he played with the Coast Guard Cutters hockey team in [[Curtis Bay, Baltimore|Curtis Bay]], [[Maryland]], and later served in the [[South Pacific Ocean|South Pacific]].<ref name="CoastGuard">{{cite web |url=http://www.uscg.mil/history/faqs/Frank_Brimsek.asp |title=Frank "Mr. Zero" Brimsek |publisher=United States Coast Guard |accessdate=2012-05-19}}</ref> After the war ended, Brimsek returned to the Bruins in time for the [[1945–46 NHL season|1945–46 season]]. However, Brimsek was not as sharp as he was before due to having not played any professional hockey for two years.<ref>{{cite news |url=https://news.google.com/newspapers?nid=2194&dat=19460112&id=0vwuAAAAIBAJ&sjid=E9wFAAAAIBAJ&pg=6043,1768917&hl=en|work=Ottawa Citizen|title=Brimsek finds it discouraging|last=Koffman|first=Jack|date=1946-01-12|page=10|accessdate=2016-01-11}}</ref><ref name="book"/> In his first season back, Brimsek guided the Bruins to the [[1946 Stanley Cup Finals|Finals]], matching up with the Montreal Canadiens. The Bruins were defeated in five games with three games requiring [[overtime (ice hockey)|overtime]]. Brimsek was applauded for his performance in the playoffs, compensating for his team's weak defence.<ref>{{cite news |url=https://news.google.com/newspapers?id=4S4rAAAAIBAJ&sjid=95gFAAAAIBAJ&pg=6134,1409352&dq=dink+carroll+%7C+crease+%7C+montreal&hl=en|work=The Montreal Gazette|title=No travel hardship|last=Caroll|first=Dink|date=1946-04-09|page=13|accessdate=2015-04-29}}</ref><ref>{{cite news |url=https://news.google.com/newspapers?id=jZJjAAAAIBAJ&sjid=QXoNAAAAIBAJ&pg=1421,3734088&hl=en|work=[[The StarPhoenix|Saskatoon Star Phoenix]]|title=Canadiens defeat Bruins 6–3, to take Stanley Cup|date=1946-04-10|page=13|accessdate=2015-04-29}}</ref> This marked Brimsek's fourth appearance in the Finals and it would be his last. At the end of the season, Brimsek was also named to the NHL Second All-Star Team, the fifth time in his career.

Brimsek remained with the Bruins for three more seasons. He was named to the NHL Second All-Star team twice more and was selected to play in the inaugural [[NHL All-Star Game]] in [[1st National Hockey League All-Star Game|1947]].<ref>{{cite web |url=http://www.hockey-reference.com/allstar/NHL_1947_roster.html|title=1947 NHL All-Star Game Rosters|publisher=Sports Reference|accessdate=2015-05-04}}</ref> The Bruins made the playoffs all three seasons but were eliminated in the semi-finals in all of them.<ref>{{cite news |url=http://www.hockey-reference.com/leagues/NHL_1947.html|work=Sports Reference|title=1946–47 NHL season summary|accessdate=2015-05-04}}</ref><ref>{{cite news |url=http://www.hockey-reference.com/leagues/NHL_1948.html|work=Sports Reference|title=1947–48 NHL season summary|accessdate=2015-05-04}}</ref><ref>{{cite news |url=http://www.hockey-reference.com/leagues/NHL_1949.html|work=Sports Reference|title=1948–49 NHL season summary|accessdate=2015-05-04}}</ref> Brimsek was also a finalist for the [[Hart Trophy]] in [[1947–48 NHL season|1947–48]], finishing behind [[Buddy O'Connor]] of the Rangers.<ref name="hr brimsek"/> Personal problems plagued Brimsek during his final years in Boston. His 10-month-old son had died in January 1949, and his coach and longtime teammate, [[Dit Clapper]], had resigned from his coaching duties.<ref>{{cite news |url=https://news.google.com/newspapers?nid=1338&dat=19490125&id=M-ZXAAAAIBAJ&sjid=YPYDAAAAIBAJ&pg=3675,3009926&hl=en|work=[[Spokane Daily Chronicle]]|title=Son of hockey star claimed by asthma|date=1949-01-25|page=45|accessdate=2016-01-11}}</ref> It did not also help that the Boston crowd would occasionally boo Brimsek for his play.<ref name="one on one"/> After the [[1948–49 NHL season|1948–49 season]], Brimsek requested a trade from Boston to Chicago in order to be closer to home, and to the new blueprint business he had started there.<ref>{{cite news |url=https://news.google.com/newspapers?nid=1144&dat=19490403&id=yV8bAAAAIBAJ&sjid=Rk0EAAAAIBAJ&pg=2262,1873183&hl=en|work=The Pittsburgh Press|title=Bruins' goalie asks to be traded|date=1949-04-03|page=124|accessdate=2015-04-30}}</ref> Boston granted his request and he was traded to the Chicago Black Hawks in exchange for cash.<ref name="hr brimsek"/> Brimsek only played one season with the Black Hawks, recording 22 wins, 38 losses and 10 ties in 70 games played.<ref name="hr brimsek"/> The team finished last in the standings.<ref>{{cite news |url=http://www.hockey-reference.com/leagues/NHL_1950.html|work=Sports Reference|title=1949–50 NHL season summary|accessdate=2015-05-04}}</ref> Brimsek's lone season with the Hawks was the only season where his team did not reach the playoffs. He retired at the conclusion of the season.<ref name="one on one"/>

==Retirement and legacy==
Brimsek played a [[stand-up style]] of goaltending. Goalies playing this style usually stay on their feet instead of dropping down on their knees to make a save. Brimsek is also remembered for having a quick catching hand and for "taking the feet out" of opposing players that were being a nuisance in front of his net.<ref>{{cite news |url=http://www.legendsofhockey.net/LegendsOfHockey/jsp/LegendsMember.jsp?mem=p196604&page=bio&list|work=Hockey Hall of Fame|title=Frank Brimsek|accessdate=2015-06-16}}</ref> After retiring, Brimsek returned to [[Minnesota]] and worked as a railroad engineer.<ref name="one on one"/> Even after his retirement, Brimsek regularly received fan mail.<ref name="NY Times"/> In 1966, Brimsek was inducted into the [[Hockey Hall of Fame]], the first American-born goalie to be inducted, and in 1973 he was an inaugural inductee of the [[United States Hockey Hall of Fame]]. Brimsek died on November 11, 1998 in [[Virginia, Minnesota|Virginia]], Minnesota, leaving behind his wife, Peggy, his two daughters, Chris and Karen, and his five grandchildren.<ref name="NY Times"/>

In 1998, Brimsek was ranked number 67 on [[List of 100 greatest hockey players by The Hockey News|''The Hockey News''' list of the 100 Greatest Hockey Players]], the highest ranked American goaltender.<ref>{{cite news |url=http://www.thehockeynews.com/blog/the-top-100-nhl-players-of-all-time-throwback-style/|work=[[The Hockey News]]|title=The top 100 NHL players of all-time, throwback style|last=Kay|first=Jason|date=2015-04-02|accessdate=2015-05-04}}</ref> An annual award, given to the top high school goaltender in Minnesota, is given in Brimsek's honor.<ref>{{cite web |url=http://www.minnesotahockey.org/page/show/85988-minnesota-high-school-goalie-award-winners|title=Minnesota high school goalie award winners|publisher=Minnesota Hockey|accessdate=2015-04-30}}</ref> Brimsek's 252 wins and 40 shutouts each stood for a long time as the most ever recorded by an American netminder. His wins record was finally broken by [[Tom Barrasso]] on February 15, 1994 and his shutouts record has only been equaled by [[John Vanbiesbrouck]] and [[Jonathan Quick]] .<ref>{{cite news |url=https://news.google.com/newspapers?nid=1914&dat=19940214&id=9RIgAAAAIBAJ&sjid=6mUFAAAAIBAJ&pg=3017,2975120&hl=en|work=[[Sun Journal (Lewiston)|Sun Journal]]|title=Pens blank Flyers|page=10|date=1994-02-14|accessdate=2015-04-30}}</ref><ref>{{cite news |url=http://www.thehockeynews.com/articles/27375-THNcom-Top-10-Americanborn-players.html|work=The Hockey News|title=American-born players|last=Grigg|first=John|date=2009-07-29|accessdate=2015-05-04}}</ref> In addition, Brimsek's eight berths to the [[NHL All-Star Team]] are the second most among goalies in history, behind only [[Glenn Hall]]'s ten berths.<ref>{{cite news |url=https://news.google.com/newspapers?nid=1774&dat=19660513&id=ekc0AAAAIBAJ&sjid=vWUEAAAAIBAJ&pg=7109,3412730&hl=en|work=[[Sarasota Herald-Tribune]]|title=Howe, Hall record choices for hockey All-Star Team|date=1966-05-13|page=18|accessdate=2015-05-04}}</ref>

==Career statistics==

===Regular season===
{| BORDER="0" CELLPADDING="0" CELLSPACING="0" width="75%"
|- bgcolor="#e0e0e0"
! colspan="3" bgcolor="#ffffff" |
! rowspan="99" bgcolor="#ffffff" |
|- bgcolor="#e0e0e0" ALIGN="center"
! [[Season (sports)|Season]]
! Team
! League
! GP
! W
! L
! T
! MIN
! GA
! [[Shutout#Ice hockey|SO]]
! [[Goals against average|GAA]]
|- ALIGN="center"
| 1934–35
| [[Pittsburgh Yellow Jackets]]
| X-Games
| 16
| 14
| 2
| 0
| 960
| 39
| 1
| 2.44
|- ALIGN="center" bgcolor="#f0f0f0"
| 1935–36
| Pittsburgh Yellow Jackets
| [[Eastern Amateur Hockey League|EAHL]]
| 38
| 20
| 16
| 2
| 2280
| 74
| 8
| 1.95
|- ALIGN="center"
| 1936–37
| Pittsburgh Yellow Jackets
| EAHL
| 47
| 19
| 23
| 5
| 2820
| 142
| 3
| 3.02
|- ALIGN="center" bgcolor="#f0f0f0"
| [[1937–38 AHL season|1937–38]]
| [[Providence Reds]]
| [[IAHL]]
| 48
| 25
| 16
| 7
| 2950
| 86
| 5
| 1.75
|- ALIGN="center"
| [[1938–39 AHL season|1938–39]]
| Providence Reds
| IAHL
| 9
| 5
| 2
| 2
| 570
| 18
| 0
| 1.89
|- ALIGN="center" bgcolor="#f0f0f0"
| [[1938–39 NHL season|1938–39]]
| [[Boston Bruins]]
| [[National Hockey League|NHL]]
| 43
| 33
| 9
| 1
| 2610
| 68
| 10
| 1.56
|- ALIGN="center"
| [[1939–40 NHL season|1939–40]]
| Boston Bruins
| NHL
| 48
| 31
| 12
| 5
| 2950
| 98
| 6
| 1.99
|- ALIGN="center" bgcolor="#f0f0f0"
| [[1940–41 NHL season|1940–41]]
| Boston Bruins
| NHL
| 48
| 27
| 8
| 13
| 3040
| 102
| 6
| 2.01
|- ALIGN="center"
| [[1941–42 NHL season|1941–42]]
| Boston Bruins
| NHL
| 47
| 24
| 17
| 6
| 2930
| 115
| 3
| 2.35
|- ALIGN="center" bgcolor="#f0f0f0"
| [[1942–43 NHL season|1942–43]]
| Boston Bruins
| NHL
| 50
| 24
| 17
| 9
| 3000
| 176
| 1
| 3.52
|- ALIGN="center"
| 1943–44
| [[U.S. Coast Guard Cutters (ice hockey)|Coast Guard Cutters]]
|[[Eastern Hockey League|EAHL]]
| 27
| 19
| 6
| 2
| 1620
| 83
| 1 
| 3.07
|- ALIGN="center" bgcolor="#f0f0f0"
| [[1945–46 NHL season|1945–46]]
| Boston Bruins
| NHL
| 34
| 16
| 14
| 4
| 2040
| 111
| 2
| 3.26
|- ALIGN="center"  
| [[1946–47 NHL season|1946–47]]
| Boston Bruins
| NHL
| 60
| 26
| 23
| 11
| 3600
| 175
| 3
| 2.92
|- ALIGN="center" bgcolor="#f0f0f0"
| [[1947–48 NHL season|1947–48]]
| Boston Bruins
| NHL
| 60
| 23
| 24
| 13
| 3600
| 168
| 3
| 2.80
|- ALIGN="center"  
| [[1948–49 NHL season|1948–49]]
| Boston Bruins
| NHL
| 54
| 26
| 20
| 8
| 3240
| 147
| 1
| 2.72
|- ALIGN="center" bgcolor="#f0f0f0"
| [[1949–50 NHL season|1949–50]]
| [[Chicago Blackhawks|Chicago Black Hawks]]
| NHL
| 70
| 22
| 38
| 10
| 4200
| 244
| 5
| 3.49
|- ALIGN="center" bgcolor="#e0e0e0"
! colspan="3" align="center" | NHL totals<ref name="hr brimsek"/>
! 514
! 252
! 182
! 80
! 31,210
! 1404
! 40
! 2.70
|}

===Playoffs===
{| BORDER="0" CELLPADDING="0" CELLSPACING="0" width="75%"
|- bgcolor="#e0e0e0"
! colspan="3" bgcolor="#ffffff" |
! rowspan="99" bgcolor="#ffffff" |
|- bgcolor="#e0e0e0" ALIGN="center"
! Season
! Team
! League
! GP
! W
! L
! T
! MIN
! GA
! SO
! GAA
|- ALIGN="center" 
| 1935–36
| Pittsburgh Yellowjackets
| EAHL
| 8
| 4
| 3
| 1
| 480
| 19
| 2
| 2.36
|- ALIGN="center" bgcolor="#f0f0f0"
| 1937–38
| Providence Reds
| IAHL
| 7
| 5
| 2
| 0
| 515
| 16
| 0
| 1.86
|- ALIGN="center" 
| 1938–39
| Boston Bruins
| NHL
| 12
| 8
| 4
| —
| 863
| 18
| 1
| 1.25
|- ALIGN="center" bgcolor="#f0f0f0"
| 1939–40
| Boston Bruins
| NHL
| 6
| 2
| 4
| —
| 360
| 15
| 0
| 2.50
|- ALIGN="center" 
| 1940–41
| Boston Bruins
| NHL
| 11
| 8
| 3
| —
| 678
| 23
| 1
| 2.04
|- ALIGN="center" bgcolor="#f0f0f0"
| 1941–42
| Boston Bruins
| NHL
| 5
| 2
| 3
| —
| 307
| 16
| 0
| 3.13
|- ALIGN="center" 
| 1942–43
| Boston Bruins
| NHL
| 9
| 4
| 5
| —
| 560
| 33
| 0
| 3.54
|- ALIGN="center" bgcolor="#f0f0f0"
| 1943–44
| Coast Guard Cutters
| X-Games
| 5
| 4
| 0
| —
| 300
| 4
| 1
| 0.80
|- ALIGN="center"  
| 1945–46
| Boston Bruins
| NHL
| 10
| 5
| 5
| —
| 651
| 29
| 0
| 2.67
|- ALIGN="center" bgcolor="#f0f0f0"
| 1946–47
| Boston Bruins
| NHL
| 5
| 1
| 4
| —
| 343
| 16
| 0
| 2.80
|- ALIGN="center"  
| 1947–48
| Boston Bruins
| NHL
| 5
| 1
| 4
| —
| 317
| 20
| 0
| 3.79
|- ALIGN="center" bgcolor="#f0f0f0"
| 1948–49
| Boston Bruins
| NHL
| 5
| 1
| 4
| —
| 316
| 16
| 0
| 3.04
|- ALIGN="center" bgcolor="#e0e0e0"
! colspan="3" align="center" | NHL totals<ref name="hr brimsek"/>
! 68
! 32
! 36
! —
! 4395
! 186
! 2
! 2.54
|}

== Awards ==
{{col-start}}
{{col-2}}
;[[Eastern Amateur Hockey League (1933-1953)|EAHL]]<ref name="one on one"/>
{| class="wikitable"
|-
! Award
! Year(s)
|-
| EAHL Second All-Star Team
| 1936
|-
| George L. Davis Trophy
| 1936
|}

;[[American Hockey League|IAHL]]<ref name="one on one"/>
{| class="wikitable"
|-
! Award
! Year(s)
|-
| IAHL First All-Star Team
| [[1937-38 AHL season|1938]]
|-
| [[Calder Cup]]
| 1938
|}

{{col-2}}
;[[National Hockey League|NHL]]<ref name="hr brimsek"/>
{| class="wikitable"
|-
! Award
! Year(s)
|-
| [[Calder Memorial Trophy]]
| [[1938-39 NHL season|1939]]
|-
| [[NHL All-Star Game]]
| [[1947 NHL All-Star Game|1947]], [[1948 NHL All-Star Game|1948]]
|-
| [[NHL First All-Star Team]]
| 1939, [[1941-42 NHL season|1942]]
|-
| [[NHL Second All-Star Team]]
| [[1939-40 NHL season|1940]], [[1940-41 NHL season|1941]], [[1942-43 NHL season|1943]], [[1945-46 NHL season|1946]], [[1946-47 NHL season|1947]], [[1947-48 NHL season|1948]]
|-
| [[Stanley Cup]]
| [[1939 Stanley Cup Finals|1939]], [[1941 Stanley Cup Finals|1941]]
|-
| [[Vezina Trophy]]
| 1939, 1942
|}
{{col-end}}

==References==
{{reflist|2}}

==External links==
* {{Legendsmember|Player|P196604|Frank Brimsek}}
* {{hockeydb|12122}}

{{s-start}}
{{succession box | before = [[Cully Dahlstrom]] | title = Winner of the [[Calder Memorial Trophy]] | years = [[1938–39 NHL season|1939]] | after = [[Kilby MacDonald]] }}
{{succession box | before = [[Tiny Thompson|Cecil Thompson]] | title = Winner of the [[Vezina Trophy]] | years = [[1938–39 NHL season|1939]]| after = [[Dave Kerr|David Kerr]]}}
{{succession box | before = [[Turk Broda]] | title = Winner of the [[Vezina Trophy]] | years = [[1941–42 NHL season|1942]]| after = [[Johnny Mowers]]}}
{{s-end}}

{{DEFAULTSORT:Brimsek, Frank}}
[[Category:1915 births]]
[[Category:1998 deaths]]
[[Category:American ice hockey goaltenders]]
[[Category:American military personnel of World War II]]
[[Category:American people of Slovenian descent]]
[[Category:Boston Bruins players]]
[[Category:Calder Trophy winners]]
[[Category:Chicago Blackhawks players]]
[[Category:Hockey Hall of Fame inductees]]
[[Category:Ice hockey people from Minnesota]]
[[Category:Pittsburgh Yellow Jackets (EHL) players]]
[[Category:Sportspeople from Eveleth, Minnesota]]
[[Category:Stanley Cup champions]]
[[Category:United States Hockey Hall of Fame inductees]]
[[Category:Vezina Trophy winners]]
[[Category: United States Coast Guard personnel]]