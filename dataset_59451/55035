{{Infobox Political post
|post            = Vice President
|body            = the Republic of Abkhazia
|insignia        = Coat of arms of Abkhazia.svg
|insigniasize    = 125px
|insigniacaption = [[Coat of arms of Abkhazia]]
|image           = 
|incumbent       = [[Vitali Gabnia]]
|incumbentsince  = 25 September 2014
|residence       = [[Sukhumi]]
|appointer       = 
|termlength      = 5 years
|inaugural       = [[Valery Arshba]]
|formation       = 26 November 1994
|website         = 
}}
{{Politics of Abkhazia}}
The '''Vice President of the Abkhaz Republic''', an [[partially recognized state]], internationally regarded as a part of [[Georgia (country)|Georgia]], is the first in the presidential line of succession, becoming the new [[President of Abkhazia]] upon the death, resignation, or removal of the President. Additionally, the Vice President would assume the presidency in case the President becomes incapable of carrying out the presidential duties.

==Eligibility==
According to the article 54 of the [[Wikisource:Constitution of Abkhazia|Constitution of Abkhazia]], a citizen of Abkhazia, no younger than 35 years old and no older than 65 years old, who is in possession of suffrage, may be elected Vice President. The Vice President shall not be member of the [[Abkhazian People's Assembly|Parliament]], or hold any other offices in state or public bodies as well as in businesses.

==Election==
The Vice President is elected simultaneously with the President. A candidate for Vice President is nominated by a candidate for President.

==Duties==
The Vice-President executes individual assignments on a commission of the President and acts for the President in his absence or in case when it is impossible for the President to attend to his duties.

==List of Vice Presidents of Abkhazia==

On 25 December 2013, Mikhail Logua announced his resignation as Vice President for health reasons.<ref name=apress10925>{{cite news|title=Вице-президент Михаил Логуа намерен оставить свой пост|url=http://apsnypress.info/news/10925.html|accessdate=8 June 2014|agency=Apsnypress|date=25 December 2013}}</ref>

{| class="wikitable" style="text-align:center" 
|- align=center bgcolor=#ebebeb 
!rowspan=2|№ 
!rowspan=2|Name<br>{{small|(Birth–Death)}} 
!colspan=2|Tenure 
!rowspan=2|[[President of Abkhazia|President]]
|- align=center bgcolor=#ebebeb 
!Took Office 
!Left Office
|-
! 1
| [[Valery Arshba]]<br>{{small|(1949–)}}
| 5 January 1995
| 12 February 2005
| [[Vladislav Ardzinba]]
|-
! 2
| [[Raul Khadjimba]]<br>{{small|(1958–)}}
| 14 February 2005
| 28 May 2009
| rowspan=3|[[Sergei Bagapsh]]
|-
| colspan=2|''Vacant''
| 28 May 2009
| 12 February 2010
|-
! rowspan|3
| rowspan|[[Alexander Ankvab]]<br>{{small|(1952–)}}
| 12 February 2010 <ref name="apress100213">{{cite news|url=http://www.apsnypress.info/news2010/February/13.htm|script-title=ru:Выпуск №69-70|date=13 February 2010|publisher=[[Apsnypress]]|language=Russian|accessdate=13 February 2010}}</ref>
| 29 May 2011
|-
| colspan=2|''Vacant''
| 29 May 2011
| 26 September 2011
| rowspan=3|[[Alexander Ankvab]]
|-
! rowspan|4
| rowspan|[[Mikhail Logua]]<br>{{small|(1970–)}}
| 26 September 2011
| 25 December 2013
|-
| colspan=2 rowspan=2|''Vacant''
| 25 December 2013
| 1 June 2014
|-
| 1 June 2014
| 25 September 2014
| ''[[Valeri Bganba]]''
|-
! 5
| [[Vitali Gabnia]]
| 25 September 2014
| Incumbent
| [[Raul Khadjimba]]
|-
|}

==See also==
*[[President of Abkhazia]]
*[[Prime Minister of Abkhazia]]
*[[Minister for Foreign Affairs of Abkhazia]]

==References==
{{reflist}}

==External links==
*[https://web.archive.org/web/20061107051831/http://www.abkhaziagov.org:80/en/ President of the Republic of Abkhazia. Official site]

[[Category:Politics of Abkhazia]]
[[Category:Vice presidents|Abkhazia]]
[[Category:Vice Presidents of Abkhazia| ]]


{{abkhazia-poli-stub}}