[[File:Largecover.gif|thumb|300px|Internet Research, an academic journal  published by Emerald]]

'''''Internet Research''''' (INTR) is a [[peer-reviewed]] academic journal, published by [[Emerald Group Publishing]].

== History ==
The journal was founded as ''Electronic Networks'' in 1990, with first articles published in 1991. Articles from 1991 onward are available online at the journal's website. Emerald acquired the journal from Mecklermedia in 1995.

== Editors-in-chief ==
Editors-in-chief of the journal have included
* 2016 – 2011: Bernard J. Jansen, [[Pennsylvania State University|The Pennsylvania State University]], [[Qatar Computing Research Institute]]
* 2011 – 1998: David G. Schwartz, [[Bar-Ilan University]], Israel
* 1998 – 1995: John Peters, Emerald

== Notable Articles ==
There have been several highly cited articles published in Internet Research, including the 1992 article, "World‐wide web: the information universe",<ref>Tim Berners‐Lee, Robert Cailliau, Jean‐François Groff, Bernd Pollermann (1992) [http://www.emeraldgrouppublishing.com/products/backfiles/pdf/backfiles_sample_5.pdf World‐wide web: the information universe]. Internet Research (republished 2010) 20:4, 461-471.</ref> which is one of the first academic articles where [[Sir Tim Berners-Lee]] mentions the phrase "World-wide web".

== Impact Factor ==
The 2015 [[impact factor]] is 3.017,<ref>2014 Journal Citation Reports® (Thomson Reuters, 2016)</ref><ref>{{cite web|title=Internet Research|url=http://www.emeraldgrouppublishing.com/products/journals/journals.htm?id=intr|publisher=Emerald|accessdate=15 June 2016}}</ref> which is the highest impact factor of any Emerald journal.<ref>Private Correspondence with Emerald journal manager to Dr. Jansen, 15 June 2016</ref>

== Indexing ==
INTR is indexed in, among others, the following [[indexing and abstracting service]]s:
* [[Social Science Citation Index]]
* [[Scopus]]
* [[EBSCOhost]]
* [[Elsevier BV]]
* [[Emerald Group Publishing Limited]]
* [[OCLC]]
* [[Ovid Technologies|Ovid]]
* [[ProQuest]]
* [[Thomson Reuters]]
* [[VINITI Database RAS|VINITI RAN]]

== References ==
{{Reflist}}

== External links ==
* http://www.emeraldinsight.com/loi/intr

[[Category:Library science journals]]
[[Category:Information science]]
[[Category:Articles created via the Article Wizard]]