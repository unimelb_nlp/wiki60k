{{pp-semi-indef}}{{Infobox military person
| name          =Nicole M. E. Malachowski
| image         = USAF pilot Nicole Malachowski.jpg
| caption       =
| birth_date          = {{birth date and age|mf=yes|1974|09|26}}
| death_date          = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} -->
| placeofburial_label =
| placeofburial =
| birth_place   =
| death_place   =
| placeofburial_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| nickname      = FiFi
| allegiance    = {{flagicon|United States}} United States
| branch        = {{Air force|United States}}
| serviceyears  = 1996 to Present
| rank          = [[File:US-O6 insignia.svg|20px]] [[Colonel (United States)|Colonel]]
| unit          =
| commands      = [[333d Fighter Squadron]]
| battles       = [[Operation Iraqi Freedom]]
| awards        = *[[Meritorious Service Medal (United States)|Meritorious Service Medal]] (2)<br />
*[[Air Medal]]
*[[Commendation Medal#Air Force|Air Force Commendation Medal]] (2)
*[[Achievement Medal|Air Force Achievement Medal]] (3)
*[[Combat Readiness Medal]]
*[[National Defense Service Medal]]
*[[Armed Forces Expeditionary Medal]]
*[[Kosovo Campaign Medal]]
*[[Global War on Terrorism Service Medal]]
*[[Korea Defense Service Medal]]
*[[Air and Space Campaign Medal]]
| relations     =
| laterwork     =
}}

'''Nicole Margaret Ellingwood Malachowski'''<ref name="amu">{{cite web |url=http://www.amuonline.com/military/AMU_08gradinsert.pdf |title=Class of 2008 |publisher=American Military University |format=PDF |archiveurl=http://wayback.archive.org/web/20090301000000*/http://www.amuonline.com/military/AMU_08gradinsert.pdf |archivedate=2009-03-01 |accessdate=October 25, 2012}}</ref> (born September 26, 1974) is a [[United States Air Force]] officer and the first female pilot selected to fly as part of the [[U.S. Air Force Thunderbirds|USAF Air Demonstration Squadron]], better known as the Thunderbirds. Her [[aviator call sign]] is "FiFi".<ref name=FirstFemaleThunderbird>{{cite news
| url=http://www.af.mil/news/story.asp?id=123028864
| title=First woman Thunderbird pilot proud to serve
| first=Sara |last=Wood |date=October 11, 2006
| publisher=[[United States Department of Defense]]
| accessdate=July 12, 2007
}}</ref>  Her first public performance was in March 2006 and she spent the 2006 and 2007 air show seasons flying the Number 3 (Right Wing) aircraft in the diamond formation.  Prior to attending the [[U.S. Air Force Academy]] and joining the USAF, Colonel Malachowski was a [[Civil Air Patrol]] Cadet.

Between September 1, 2008 and August 31, 2009, Malachowski was on special assignment, participating in the [[White House Fellows]] Program for the Class of 2008–2009, assigned to the [[General Services Administration]].<ref name="gsa">{{cite web |url=http://www.veterantributes.org/TributeDetail.asp?ID=337 |title=Nicole M. Malachowski |publisher=Veterans Tributes |archiveurl=https://web.archive.org/web/20091125134628/http://www.veterantributes.org/TributeDetail.asp?ID=337 |archivedate=2009-11-25 |accessdate=October 25, 2012}}</ref> In September 2015 Malachowski returned to the White House as executive director of its Joining Forces initiative for supporting veterans, service members, and military families, succeeding U. S. Army Colonel Steve Parker.<ref>{{cite web|url=http://www.militarytimes.com/story/military/2015/10/07/five-questions-nicole-malachowski/73201284/|title=Jining Forces' new leader has cockpit, White House experience|publisher=Military Times|accessdate=October 5, 2016}}</ref>

==Early years, education, and personal biography==
Nicole Malachowski was born '''Nicole Ellingwood''' in [[Santa Maria, California]], to Cathy and Robert Ellingwood.<ref name=LVRJ_Lake>{{cite web
|accessdate=December 24, 2008
|url=http://www.reviewjournal.com/lvrj_home/2005/Jun-17-Fri-2005/news/26737697.html
|title=Female Thunderbird: First in Flight; Western High School grad makes history
|first=Richard |last=Lake
|date=June 17, 2005
|work=Las Vegas Review-Journal
}}</ref>  In high school, she was a cadet member of the Nevada Wing of the [[Civil Air Patrol]]<ref name=WomensMemorial>
{{cite web|accessdate=December 24, 2008
|url=http://womensmemorial.org/News/ladythunderbird.html
|title=Lady Thunderbird |date=November 2008
|publisher=Women In Military Service For America Memorial Foundation, Inc.
}}</ref> and participated in [[AFJROTC]], where she was rated cadet colonel, the highest rank a cadet could achieve.  She started working on her pilot's license before graduating from high school.<ref name=LVRJ_Lake/> She graduated from [[Western High School (Las Vegas, Nevada)|Western High School]] in [[Las Vegas, Nevada|Las Vegas]] in 1992.

She earned a Bachelor of Science degree in Management, with a minor in French,<ref name=WhiteHouse_20080626/> from the [[United States Air Force Academy]], graduating 124th of 922 in the Class of 1996.<ref name="gsa"/>  While at the academy she was both a pilot and cadet instructor pilot in the academy's [[Schweizer SGS 2-33|TG-4]] glider program. She also earned a [[Master of Arts]] degree from [[American Military University]] in National Security Policy,<ref name=WhiteHouse_20080626/> and a second in National Security and Strategic Studies from the [[United States Naval War College]], where she graduated with highest distinction.<ref name="joining forces"/>

Malachowski is married to Lieutenant Colonel Paul G. Malachowski (USAF, retired), a former F-15E Weapons System Operator. The couple met while both were serving as aircrew in the [[48th Fighter Wing]] at [[RAF Lakenheath]] in the United Kingdom.<ref name=LVRJ_Lake/> In April 2010 she was admitted to the Mother and Infant Care Center (MICC) of the [[National Naval Medical Center]] in [[Bethesda, Maryland]], in expectation of the birth of twins. After a confinement of nine weeks, she gave birth to daughter Norah and son Garrick on June 6, 2010.<ref>[http://community.intellicontact.com/p/flygirls/newsletters/may2/posts/a-little-good-news-from-paul-malachowski "A Little Good News from Paul Malachowski"]{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}. Wings Across America archive. FlyGirls Newsletter. Intellicontact.com.{{dead link|date=January 2012}}</ref>

==Professional career==
[[File:Maj Nicole Malachowski, USAF Thunderbirds.jpg|thumb|left|USAF Thunderbirds portrait]]

Malachowski attended undergraduate pilot training at [[Columbus Air Force Base|Columbus AFB]], [[Mississippi]]. She finished fourth in her class and selected the only [[F-15E Strike Eagle]] slot allotted to her class. After meeting height requirements for fighter pilots,<ref>Lake (June 17, 2005).  "The Air Force's requirements state that fighter pilots must be at least 5 feet 4 inches tall."</ref> she trained at [[Seymour Johnson Air Force Base|Seymour Johnson AFB]], [[North Carolina]], with the [[4th Fighter Wing]].

[[File:Lt Col Nicole Malachowski, USAF Female F-15E Fighter Pilot and The First Lady of Thunderbirds Air Demonstration Squadron.webm|thumb|Nicole Malachowski, The First Lady Pilot of Thunderbirds Airshow Team]]

She served two operational tours at RAF Lakenheath, England, with the [[48th Fighter Wing]]; assignment to the [[4th Fighter Wing]] at [[Seymour Johnson AFB]]; and as an Air Liaison Officer supporting the [[2nd Infantry Division (United States)|2nd Infantry Division]] at [[Camp Red Cloud]], South Korea. During her second tour at RAF Lakenheath, Malachowski deployed for four months in early 2005 in support of [[Operation Iraqi Freedom]] and [[Operation Enduring Freedom]], flying 26 combat missions. She applied and was accepted as a Thunderbird pilot in June 2005. She completed transition training to the [[F-16 Fighting Falcon]] with the [[56th Fighter Wing]] at [[Luke Air Force Base]], [[Arizona]] and flew with the Thunderbird Team based at [[Nellis AFB]], Nevada in November 2005 until November 2007.

After successfully completing her tour with the USAF Thunderbirds in November 2007, including approximately 140 performances, Malachowski served on staff of the Commander, [[United States Air Force Warfare Center]], at [[Nellis Air Force Base]], Nevada, to June 2008.

[[File:WASP Congressional Gold Medal.jpg|thumb|right|Major Malachowski (far right) at July 2009 White House ceremony honoring WASPs.]]
Malachowski was selected to participate as a [[White House Fellow]] in Washington, D.C., from September 1, 2008 to August 31, 2009, working in the General Services Administration with the Presidential Transition Support Team and as deputy chief of staff.<ref name="gsa"/><ref name=WhiteHouse_20080626>{{cite press release|accessdate=December 24, 2008
|url=http://georgewbush-whitehouse.archives.gov/news/releases/2008/06/20080626-25.html
|author=Office of the Press Secretary
|date=June 2008
|title= President Bush Appoints 2008–2009 Class of White House Fellows
|publisher=The White House
}}</ref><ref name="pub">{{cite web | url= https://www.govexec.com/federal-news/fedblog/2008/10/thunderbird-lands-at-gsa/38327/| title = First Woman Thunderbird Lands at GSA| work=FedBlog |publisher = U.S. General Services Administration |first=Tom |last=Shoop|date=October 7, 2008| accessdate =October 25, 2012}}<br/>{{cite web|url=http://emailwire.com/release/16706-First-Woman-Thunderbird-Lands-at-GSA.html |title=First Woman Thunderbird Lands at GSA| work=GSA|publisher=emailwire.com|format=Press release |date=October 8, 2008|accessdate=October 25, 2012}}</ref> Malachowski has been an advocate of recognition as veterans of women pilots who served during World War II. On July 1, 2009, she participated in a White House ceremony at which legislation (S.614) awarding a [[Congressional Gold Medal]] to former pilots of the [[Women Airforce Service Pilots]] (WASP) was signed into law by President [[Barack Obama]]. Although she had to use a wheelchair due to a broken left leg, Lt Col Malachowski delivered remarks during the ceremony held March 10, 2010, in the [[United States Capitol]] awarding Deanie Bishop Parrish the medal on behalf of all 1,102 WASP pilots.<ref name="mcdc">{{cite web | url= http://www.mcclatchydc.com/2010/03/10/90172_a90167/the-wasps-of-world-war-ii.html|title = Gallery: The WASPS of World War II| publisher = McClatchy.com| accessdate=August 14, 2010}}</ref><ref>{{cite web|url=http://www.wingsacrossamerica.us/deanie.htm |title=Deanie Bishop Parrish |publisher=Wingsacrossamerica.us |accessdate=January 15, 2012}}</ref><ref>{{cite web|url=http://www.wingsacrossamerica.us/wings/index.htm |title=Wings Across America - main page |publisher=wingsacrossamerica.us }}. Includes YouTube video of 2010 Malachowski remarks.</ref>

Malachowski served as deputy commander of the 4th Operations Support Squadron, [[4th Operations Group]], until November 18, 2011, when she took command of the [[333d Fighter Squadron]] at [[Seymour Johnson Air Force Base]], [[North Carolina]].<ref>{{cite web|url=http://www.seymourjohnson.af.mil/photos/media_search.asp?q=Nicole+Malachowski&btnG.x=32&btnG.y=9 |title=Change of Command ceremony, 333d Fighter Squadron |publisher=Seymourjohnson.af.mil |accessdate=January 15, 2012}}</ref>

She completed her assignment with the 333d in May, 2013 and reported to the [[Naval War College]] in Newport, RI as a student.<ref name=LastFlightSJ/>

==Assignments==
#December 1996 – March 1998: Student, Undergraduate Pilot Training Class 98-03, [[Columbus Air Force Base|Columbus AFB]], Mississippi.
#March 1998 – January 1999: Student, F-15E Formal Training Unit, [[333d Fighter Squadron]], [[Seymour Johnson AFB]], North Carolina.
#January 1999 – October 2000: F-15E pilot, Ground Training Officer, Standardization and Evaluation Liaisons Officer [[492d Fighter Squadron]], [[RAF Lakenheath]], England.
#October 2000 – January 2003: F-15E Instructor Pilot, Chief of Life Support, Assistant Chief of Scheduling, Weapons Flight Electronic Combat Pilot, Functional Check Flight Pilot, Supervisor of Flying, [[336th Fighter Squadron]], [[Seymour Johnson AFB]], North Carolina.
#January 2003 – January 2004: Army Liaison Officer, Chief of Standardization and Evaluation, Deputy Director of Main Air Support Operations Center, ASOC Fighter Duty Officer, 604th Air Support Operations Squadron, Camp Red Cloud, South Korea.
#January 2004 – March 2004: Student, F-15E TX-2, Class 04-BTE2, 333d Fighter Squadron, Seymour Johnson AFB, North Carolina.
#March 2004 – July 2005: F-15E Instructor Pilot, C-Flight Commander, Supervisor of Flying, [[494th Fighter Squadron]], RAF Lakenheath, United Kingdom.
#August 2005 – October 2005: Student, F-16C/D TX-2, Class 05-ATC, [[61st Fighter Squadron]], [[Luke AFB]], Arizona.
#November 2005 – November 2007: pilot, [[USAF Air Demonstration Squadron]], [[Nellis AFB]], Nevada.
#December 2007 – June 2008: Deputy Chief, Commander's Action Group, [[United States Air Force Warfare Center]], Nellis AFB.
#September 2008 – September 2009: General Services Administration, [[White House Fellow]]
#September 2009 to ??:  SAF/IA Weapons Division, Chief, International Developmental Fighter Programs
#?? to November 2011:  Director of Operations, 4th Operations Support Squadron, Seymour-Johnson AFB, NC
#November 2011 to May 2013:  Commander, [[333d Fighter Squadron]], Seymour Johnson AFB, NC
#June 2013 – June 2014: Student, Naval War College, Newport, RI<ref name=LastFlightSJ>{{cite web|last1=Crolley|first1=Brittain|title=First female Thunderbird takes last flight at SJ|url=http://www.seymourjohnson.af.mil/news/story.asp?id=123348476|publisher=[[USAF]]|accessdate=21 July 2014}}</ref>
#September 2015 - May 2016: Executive Director of Joining Forces initiative, White House Fellow<ref name="joining forces">{{cite web|last1=Tchen|first1=Tina|title=Joining Forces Announces New Executive Director|url=https://www.whitehouse.gov/blog/2015/05/28/joining-forces-announces-new-executive-director|publisher=whitehouse.gov|accessdate=5 October 2016}}</ref><ref>[https://www.whff.org/announcements/first-lady-thanks-whfs-col-nicole-malachowski-and-maj-andy-anderson/ First Lady thanks WHFs Col. Nicole Malachowski and Maj. Andy Anderson] {{webarchive |url=https://web.archive.org/web/20161009195939/https://www.whff.org/announcements/first-lady-thanks-whfs-col-nicole-malachowski-and-maj-andy-anderson/ |date=October 9, 2016 }}. Whitehouse.voe, retrieved 5 October 2016</ref>

==Flight Information==
:Rating: [[U.S. Air Force Aeronautical Ratings|Senior Pilot]]
:Flight hours: More than 2,100
:Aircraft flown: [[Laister-Kauffman TG-4]], [[McDonnell Douglas F-15E Strike Eagle]], [[General Dynamics F-16 Fighting Falcon]]

==Awards and decorations==

Nicole Malachowski's ribbons as of August 1, 2009:<ref name="gsa"/>
{|
|-
|{{ribbon devices|number=1|type=oak|name=Meritorious Service ribbon|width=60}}
|[[Meritorious Service Medal (United States)|Meritorious Service Medal]] with bronze [[oak leaf cluster]]
|-
|{{ribbon devices|number=0|type=oak|name=Air Medal ribbon|width=60}}
|[[Air Medal]]
|-
|{{ribbon devices|number=1|type=oak|name=Air Force Commendation ribbon|width=60}}
|[[Commendation Medal#Air Force|Air Force Commendation Medal]] with bronze oak leaf cluster
|-
|{{ribbon devices|number=2|type=oak|name=Air Force Achievement ribbon|width=60}}
|[[Achievement Medal|Air Force Achievement Medal]] with two bronze oak leaf clusters
|-
|{{ribbon devices|number=5|type=oak|name=Outstanding Unit ribbon|width=60}}
|[[Outstanding Unit Award|Air Force Outstanding Unit Award]] with silver oak leaf cluster
|-
|{{ribbon devices|number=0|type=oak|name=Organizational Excellence ribbon|width=60}}
|[[Organizational Excellence Award|Air Force Organizational Excellence Award]]
|-
|{{ribbon devices|number=0|type=oak|name=Combat Readiness Medal ribbon|width=60}}
|[[Combat Readiness Medal]]
|-
|{{ribbon devices|number=0|type=service-star|name=National Defense Service Medal ribbon|width=60}}
|[[National Defense Service Medal]]
|-
|{{ribbon devices|number=0|type=service-star|name=AFEMRib|width=60}}
|[[Armed Forces Expeditionary Medal]]
|-
|{{ribbon devices|number=0|type=service-star|name=KosovoRib|width=60}}
|[[Kosovo Campaign Medal]]
|-
|{{ribbon devices|number=0|type=oak|name=Global War on Terrorism Service ribbon|width=60}}
|[[Global War on Terrorism Service Medal]]
|-
|{{ribbon devices|number=0|type=oak|name=Korea Defense Service ribbon|width=60}}
|[[Korea Defense Service Medal]]
|-
|{{ribbon devices|number=0|type=oak|name=Air and Space Campaign ribbon|width=60}}
|[[Air and Space Campaign Medal]]
|-
|{{ribbon devices|number=0|type=oak|ribbon=Air Force Overseas Short Tour Service Ribbon.svg|width=60}}
|[[Overseas Service Ribbon#Air Force|Air Force Overseas Short Tour Service Ribbon]]
|-
|{{ribbon devices|number=0|type=oak|ribbon=Air Force Overseas Long Tour Service Ribbon.svg|width=60}}
|[[Overseas Service Ribbon#Air Force|Air Force Overseas Long Tour Service Ribbon]]
|-
|{{ribbon devices|number=3|type=oak|ribbon=Air Force Longevity Service ribbon.svg|width=60}}
|[[Air Force Longevity Service Award]] with three bronze oak leaf clusters
|-
|{{ribbon devices|number=0|type=service-star|ribbon=Air Force Training Ribbon.svg|width=60}}
|[[Air Force Training Ribbon]]
|}

===Other achievements===
:2002: Distinguished graduate of the Air Force's [[Squadron Officer School]]
:2008: Inducted into the Women in Aviation, International, Pioneer Hall of Fame<ref name=WAI_200803>{{cite web|date=March 2008
|url=http://www.wai.org/08conference/2008_conf_phof.cfm
|title=2008 WAI Pioneer Hall of Fame Inductees
|publisher=Women in Aviation}}
</ref>

==Promotion dates==
{| class="wikitable"
|+ Promotions
! Insignia !! Rank !! Date
|-
||[[File:US-O6 insignia.svg|20px]]||[[Colonel (United States)|Colonel]]||February 2015<ref name="2015ColProm">{{cite web | url=http://www.military.com/daily-news/2015/02/11/air-force-colonel-and-major-promotions.html|title = Air Force Releases Colonel, Major Promotion Lists|date=February 11, 2015 |publisher = military.com| accessdate=September 10, 2015}}</ref>
|-
||[[File:US-O5 insignia.svg|20px]]||[[Lieutenant colonel (United States)|Lieutenant Colonel]]||January 2010<ref name="prom110">{{cite web | url= http://www.airforcetimes.com/news/2010/04/airforce_march_officerpromos_043010/|title = March officer promotions| publisher = airforcetimes.com| accessdate=August 14, 2010}}</ref>
|-
||[[File:US-O4 insignia.svg|20px]]||[[Major (United States)|Major]]||April 1, 2006
|-
||[[File:US-O3 insignia.svg|15px]]||[[Captain (United States)|Captain]]||May 29, 2000
|-
||[[File:US-O2 insignia.svg|6px]]||[[First Lieutenant (United States)|First Lieutenant]]||May 29, 1998
|-
||[[File:US-OF1B.svg|6px]]||[[US Second Lieutenant|Second Lieutenant]]||May 29, 1996
|}

==References==
{{Reflist|2}}
*{{cite journal|accessdate=December 24, 2008
|url=http://womanpilot.com/?p=73
|title=Major Nicole Malachowski and Major Samantha Weeks
|date=April 2, 2008
|journal=Woman Pilot}}

==External links==
{{Portal|United States Air Force}}
* [https://web.archive.org/web/20100902111355/http://www.whitehouse.gov/about/fellows/2008-2009 2008–2009 Class of White House Fellows]
* [http://www.cnn.com/2005/US/06/16/woman.thunderbird/index.html CNN.com: Woman joins Air Force Thunderbirds]
* [http://www.f-16.net/news_article1400.html F-16.net article – Thunderbirds' first female pilot announced with new 2006 pilots]
* [http://www.f-16.net/news_article1411.html F-16.net article – Becoming Thunderbird is dream come true for Nevada native]
* [http://www.airforcetimes.com/legacy/new/1-292925-2172262.php Air Force Times – Thunderbirds to get first female pilot] — 2006-10-13
* [http://www.lvrj.com/living/25677524.html Pilot to leave Nellis for White House Fellows Program]

{{Use mdy dates|date=June 2012}}

{{DEFAULTSORT:Malachowski, Nicole}}
[[Category:1974 births]]
[[Category:Living people]]
[[Category:People of the Civil Air Patrol]]
[[Category:American aviators]]
[[Category:Female aviators]]
[[Category:Aviators from California]]
[[Category:United States Air Force Thunderbirds pilots]]
[[Category:United States Air Force Academy alumni]]
[[Category:United States Air Force officers]]
[[Category:Women in warfare post-1945]]
[[Category:Women in the United States Air Force]]
[[Category:Recipients of the Air Medal]]
[[Category:People from Santa Maria, California]]
[[Category:White House Fellows]]
[[Category:American female aviators]]