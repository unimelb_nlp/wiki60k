{{Orphan|date=January 2012}}

The '''Dr. Horacio E. Oduber Hospital''' (abbreviated as HOH) is a 320-bed Catholic [[hospital]] on the island of [[Aruba]], founded in 1976 by the non-profit foundation ''Stichting Ziekenverpleging Aruba''. It is the only hospital on the island of approximately 120,000 inhabitants.<ref>[http://www.skipr.nl/actueel/id2829-vumc-versterkt-band-met-arubaans-ziekenhuis.html VUmc versterkt band met Arubaans ziekenhuis], 4 December 2009</ref>

==History==

The hospital is named after the first physician of Aruban origin, who used to treat patients in his home (Quinta del Carmen) before the first hospital of Aruba, San Pedro de Verona was founded by catholic nuns in 1920. San Pedro de Verona hospital was converted into a nursing home in 1977 when Dr. Horacio Oduber Hospital started receiving its first patients. Between 1938 and 1985 the Lago oil refinery in San Nicolas owned a second hospital, who served primarily its employees, their families and privately insured citizens.  It was not until 1985 when the Lago hospital was closed, that HOH became the only hospital on the island. HOH is a general hospital and a level III  trauma center.<ref>[http://www.arubahospital.com/OverhetHOH/Theorganisation/Geschiedenis/tabid/61/language/nl-NL/Default.aspx Geschiedenis<!-- Bot generated title -->] {{webarchive |url=https://web.archive.org/web/20120426091323/http://www.arubahospital.com/OverhetHOH/Theorganisation/Geschiedenis/tabid/61/language/nl-NL/Default.aspx |date=April 26, 2012 }}</ref>

==Location and accessibility==

HOH is located in the area of [[Eagle Beach]] in Oranjestad, Aruba, right in front of the low-rise hotel area, and within 5 minutes of the high-rise hotel-strip at Palm Beach. The hospital is easily accessible by major highways, including the Sasaki-corridor and the Kibaima-Tanki Flip roadway, which connects the largest urban areas of Aruba. The furthest urban areas are within 20 minutes of HOH by ground ambulance, while the distance to most rural areas can be up to 45 minutes by road. The hospital has an active helipad with all procedures in place to receive patients by police and navy helicopters.<ref>[http://arubaherald.com/911/1881-woman-airlifted-to-hospital.html Woman airlifted to hospital<!-- Bot generated title -->]</ref>

==Medical staff==

The medical disciplines available in HOH include general medicine, general surgery, internal medicine, neurology, neuro-surgery, orthopedic surgery, pediatry, gynaecology, cardiology, nephrology, gastro-enterology, anesthesiology, ophthalmology, beriatric surgery, otolaryngology, psychiatry, oncology, pathology, intensive care medicine, plastic surgery, dermatology, obstetrics, pulmonology, radiology and urology.<ref>[http://www.azv.aw/index.php?option=com_content&task=view&id=6&Itemid=8 azv aruba<!-- Bot generated title -->]</ref>
Specialized physicians are either contracted by HOH or work privately as independent consultants.

As Aruba does not produce its own physicians, most medical staff at HOH are trained in the Netherlands, while the rest are trained in other countries of Europe or Latin America. However HOH is a training facility for residents, medical students and nurses from selected Dutch universities and a locally established vocational nursing school.<ref>[http://www.epiaruba.com/salubridad-servicio Salubridad & Servicio « Colegio EPI Aruba<!-- Bot generated title -->]</ref><ref>[https://books.google.com/books?id=xN7nNRr4lGIC&pg=PA42#v=onepage&q&f=false La salud en las Américas - Google Boeken<!-- Bot generated title -->]</ref> The nursing and paramedical staff at HOH include individuals from Aruba, Curaçao, Sint Martin, Surinam, the Netherlands, Belgium, Scotland, Germany, the UK, Canada, Colombia, Venezuela, Costa Rica, Mexico among other countries, which creates a typical multi-lingual environment. Patients and staff communicate in English, Dutch, Spanish and Papiamento. The official language of documentation is Dutch and English.

==Services==

===Diagnostic departments===
Electrophysiology, ultrasound, x-ray, mammography, CAT-scan, MRI, performance tests, laparoscopy, endoscopy, bronchoscopy, coronary angiography,  pathology, serology, clinical chemistry, microbiology, among others.

===Treatment facilities===
Rehabilitation center, physical therapy unit,  obstetrical unit, walk-in clinic, emergency department, out-patient clinic, wound care center, diabetes center, pain clinic, oncology day-care unit,  surgical unit (5 operating rooms),  bloodbank,  post-anestesia unit, hyperbaric chambers (operated by the nearby privatily owned HOPE-clinic).<ref>[http://www.arubahospital.com/Home/tabid/38/Default.aspx Horacio Oduber Hospital – Homepage] {{webarchive |url=https://web.archive.org/web/20150203145144/http://www.arubahospital.com/Home/tabid/38/Default.aspx |date=February 3, 2015 }}</ref>

===Admission departments===
Psychiatric unit, intensive care unit, medium care unit, coronary care unit, short-stay center, medical wards, surgical wards, isolation nursing units, multiple specialty wards, maternity ward, pediatric ward, basic neonatological  care.

==Referral of care and inter-hospital agreements==

HOH, through the public health insurance agency AZV, has agreements with selected tertiary referral hospitals in Colombia and sends out several patients a year for specialized treatment to nearby cities like Bucaramanga, Medellin, Cali and Barranquilla.<ref>[http://www.gobierno.aw/index.asp?nmoduleid=19&wgid=8&sc=0&spagetype=21&nPageID=1198&nCMSPageType=1 Aruba Papiamento - Cuido medico<!-- Bot generated title -->] {{webarchive |url=https://web.archive.org/web/20120426091324/http://www.gobierno.aw/index.asp?nmoduleid=19&wgid=8&sc=0&spagetype=21&nPageID=1198&nCMSPageType=1 |date=April 26, 2012 }}</ref> Common referrals are related to intervention-cardiology, thoracic surgery, cardiovascular surgery, complex neurosurgery, neonatological intensive care, high risk obstetry and perinatology, some cases of oncology, complex trauma surgery and reconstructive surgery and treatment that can only be given in a burn center. Oncological cases are sent to the radio-therapy facility of the Sint Elisabeth Hospital in Curaçao. Divers with decompression sickness are sent to Bonaire to the Fundashon Mariadal in Bonaire. Candidates for organ transplantation or complex maxillofacial surgery and patients with growth and puberty disorders are also referred to the mainland or the Netherlands.<ref>[http://www.azv.aw azv aruba<!-- Bot generated title -->]</ref>
At the same time, HOH frequently receives patients from smaller hospitals on nearby islands like Bonaire, St. Martin, Saba, Statia and occasionally from Curaçao as particularly intensive care beds and neonatology incubators are limited in this region.  
HOH also receives daily referrals from the ''Intituto Medico San Nicolas (IMSAN)'', a large out-patient facility in the San Nicolas area, which attends approximately 16.000 patients through its emergency department. 
Patient transfer from and to Aruba are conducted by Colombian, American or Aruban air ambulance planes.<ref>[http://www.tiara-air.com Tiara Air Aruba<!-- Bot generated title -->]</ref>

==Recent developments==

A recently renovated wing houses a combined intensive care, medium care and coronary car unit, which has a flexible capacity of 12 beds.<ref>[http://www.24ora.com/local-mainmenu-5/18903-cu-renobacion-y-expancion-di-icu-cu-lo-cuminsa-pronto-hospital-ta-redobla-capacidad-di-cuido-intensivo.html Hospital ta redobla capacidad di cuido intensivo<!-- Bot generated title -->]</ref><ref>[http://www.amigoe.com/english/94733-renovated-intensive-care-unit-open-again- Renovated intensive care unit open again<!-- Bot generated title -->]</ref>
There are existing plans to renovate the main building and expand the hospital with a three story wing, which will house all services related to mother and child care and a new out-patient clinic.<ref>[http://www.diario.aw/2011/06/hospital-lo-tin-mas-espacio-pa-brinda-mas-cuido-y-servicio-na-pashentnan/ Hospital lo tin mas espacio pa brinda mas cuido y servicio na pashentnan — Diario Online<!-- Bot generated title -->]</ref>

A large expansion and renovation project started in September 2014 and is due to finish in 2019. It is said to be one of the largest construction projects in the history of Aruba. The project includes a new emergency department, three times the size of the current one, an expansion of the operating theaters and the post anesthesia unit and the construction of a new hybrid cathlab.<ref>http://www.24ora.com/politica-mainmenu-18/93251-minister-schwengle-a-yega-na-un-acuerdo-riba-un-sala-di-operacion-nobo-pa-hospital</ref> A new six-story building will arise, which will remain physically separated from the current five-story tower. <ref>http://www.24ora.com/local-mainmenu-5/91887-renobacion-di-salanan-di-operacion-lo-inclui-hybrid-cath-lab-</ref> The 10000 square meters of new space will house a new outpatient clinic, three hospitalisation wards of thirty patient rooms each, and a new 'mother and child center' that will house the delivery suits, the pediatrics ward and the maternity ward. The old building will be renovated. <ref>[http://www.dutchcaribbeanlegalportal.com/news/latest-news/2064-renovatie-en-nieuwbouwplannen-van-het-horacio-oduber-hospitaal-hoh Renovatie- en nieuwbouwplannen van het Horacio Oduber Hospitaal (HOH)]</ref>

==References==
{{reflist|33em}}

{{coord|12.5420|N|70.0577|W|source:wikidata|display=title}}

[[Category:Hospitals in the Netherlands]]
[[Category:Buildings and structures in Aruba]]
[[Category:1976 establishments in the Netherlands Antilles]]
[[Category:1970s establishments in Aruba]]
[[Category:Hospitals established in 1976]]
[[Category:Oranjestad, Aruba]]