{{about|a term used in the United States and the United Kingdom|the term used for an ethnic group in Southern Africa|Coloured|other use|color (disambiguation)}} 
'''Colored''' is an ethnic descriptor historically used in the [[United States]] (predominantly during the [[Jim Crow]] era) and the [[United Kingdom]]. In the US, the term denoted non-"[[White people|white]]" individuals generally.<ref>{{cite book|title=Statistical Abstract of the United States|date=1934|publisher=Department of the Treasury, United States|page=554|url=https://www.google.com/books?id=LIZGAQAAIAAJ&pg=PA554}}</ref> The meaning was essentially the same in the UK, with "coloured" thus equivalent to "[[Person of color|people of colour]]".<ref>{{cite book|authors=Nicholas Deakin, Brian Cohen, Julia McNeal|title=Colour, citizenship and British society: based on the Institute of Race Relations report|date=1970|publisher=Panther Books|page=57|url=https://www.google.com/books?id=tZUJAQAAIAAJ|accessdate=15 January 2017}}</ref> However, usage of the appellation "colored" in the American South gradually came to be restricted to "[[negro]]es".<ref>{{cite book|last1=Trigger|first1=Bruce G.|title=Northeast|date=1978|publisher=Smithsonian Institution|page=290|url=https://www.google.com/books?id=EX_hAAAAMAAJ|accessdate=15 January 2017}}</ref> Following the [[Civil Rights Movement]], "colored" and "negro" gave way to "[[Black people|black]]" and (in the US) "[[African American]]". According to the [[Merriam-Webster]] dictionary, the word ''colored'' was first used in the 14th century, but with a meaning other than race or ethnicity.<ref>{{cite web|url=http://www.merriam-webster.com/dictionary/colored |title=Colored &#124; Definition of Colored by Merriam-Webster |website=Merriam-webster.com |date= |accessdate=2016-04-28}}</ref>

In other English-speaking countries, the term – often spelled '''coloured'''<ref name="BBC1" /> – has varied meanings. In [[South Africa]], [[Namibia]], [[Botswana]] , [[Zambia]] and [[Zimbabwe]], the name [[coloured]] (often capitalized) refers both to a specific ethnic group of complex mixed origins, which is considered neither black nor white, and in other contexts (usually lower case) to people of mixed race. In [[United Kingdom|British]] usage, the term refers to "a person who is wholly or partly of non-white descent" and its use may be regarded as antiquated or offensive,<ref name="BBC1">{{cite news|title=Is the word 'coloured' offensive?|url=http://news.bbc.co.uk/2/hi/uk_news/magazine/6132672.stm|publisher=BBC News |work=Magazine|accessdate=August 18, 2012|date=November 9, 2006|quote=In times when commentators say the term is widely perceived as offensive, a Labour MP lost no time in condemning it "patronising and derogatory"}}</ref><ref>{{cite web|title=Definition of coloured in English|work=Oxford Dictionaries|url=http://oxforddictionaries.com/definition/english/coloured |accessdate=18 August 2012|quote=In Britain it was the accepted term until the 1960s, when it was superseded (as in the US) by black. The term coloured lost favour among black people during this period and is now widely regarded as offensive except in historical contexts}}</ref> and other terms are preferable, particularly when referring to a single ethnicity.

==History in United States==
In 1851, an article in ''[[The New York Times]]'' referred to the "colored population".<ref>{{cite journal|title=New York Times|page=3|date=September 18, 1851}}</ref>  
In 1863, the War Department established the [[Bureau of Colored Troops]].

The first 12 [[United States Census]] counts enumerated '"colored" people, who totaled nine million in 1900. The census counts of 1910–1960 enumerated "negroes".
NPR reported that the "use of the phrase "colored people" peaked in books published in 1970".<ref>{{cite web|last1=Malesky|first1=Kee|title=The Journey From 'Colored' To 'Minorities' To 'People Of Color' Facebook Twitter Google+ Email|url=http://www.npr.org/sections/codeswitch/2014/03/30/295931070/the-journey-from-colored-to-minorities-to-people-of-color|website=NPR|accessdate=5 February 2017}}</ref>
"It's no disgrace to be colored", the black entertainer [[Bert Williams]] famously observed early in the century, "but it is awfully inconvenient."<ref>Neilly, Herbert L. [https://books.google.com/books?id=lMF8I4oHGZEC&pg=PA237&lpg=PA237&dq=williams+%22It%E2%80%99s+no+disgrace+to+be+colored%22&source=bl&ots=VsfhbvP6KF&sig=y0dXJuWvhDlL9wYqfSOJ0PN5hPw&hl=en&sa=X&ei=LcTyUMqUL8PG0QGSiYHwCQ&ved=0CE4Q6AEwBA#v=onepage&q=williams%20%22It%E2%80%99s%20no%20disgrace%20to%20be%20colored%22&f=false Black Pride: The Philosophy and Opinions of Black Nationalism: A Six-Volume History of Black Culture in Two Parts] AuthorHouse, 2005; ISBN 1418416657, page 237 (Google Books)</ref>

"Colored people lived in three neighborhoods that were clearly demarcated, as if by ropes or turnstiles", wrote Harvard professor [[Henry Louis Gates, Jr.]] about growing up in [[Racial segregation in the United States|segregated]] [[West Virginia]] in the 1960s. "Welcome to the Colored Zone, a large stretched banner could have said... Of course, the colored world was not so much a neighborhood as a condition of existence."<ref name="GrowingUp">Gates Jr, Henry Louis, [http://www.americanheritage.com/content/growing-colored Growing Up Colored], ''American Heritage Magazine'', Summer 2012, Volume 62, Issue 2</ref> "For most of my childhood, we couldn't eat in restaurants or sleep in hotels, we couldn't use certain bathrooms or try on clothes in stores", recalls Gates. His mother retaliated by not buying clothes she was not allowed to try on. He remembered hearing a white man deliberately [[Society for the Prevention of Calling Sleeping Car Porters "George"|calling his father by the wrong name]]. "'He knows my name, boy,' my father said after a long pause. 'He calls all colored people George.'" When Gates's cousin became the first black cheerleader at the local high school, she was not allowed to sit with the team and drink Coke from a glass, but had to stand at the counter drinking from a paper cup.<ref name="GrowingUp" /> Professor Gates also wrote about his experiences in his 1995 book, ''Colored People: A Memoir''.<ref>Gates Jr, Henry Louis, ''Colored People: A Memoir'', (Vintage, 1995), ISBN 067973919X.</ref>

In the 21st century, "colored" is generally regarded as an offensive term.<ref name="BBC1" /><ref>{{cite web|title=Derogatory Racial Terms to Avoid in Public|url=http://racerelations.about.com/od/understandingrac1/a/racialnamestoavoid_2.htm|website=racerelations.about.com|publisher=About.com|accessdate=14 February 2015|quote=Some people may think it's okay to simply shorten that phrase ["people of color"] to "colored," but they're mistaken. Like "Oriental," "colored" harkens back to an era of exclusion, a time when Jim Crow was in full force, and blacks used water fountains marked "colored" and sat in the "colored" sections of busses, beaches and restaurants. In essence, the term stirs up painful memories.}}</ref>

The term lives on in the [[National Association for the Advancement of Colored People]], generally called NAACP.<ref name="BBC1" /> In 2008 Carla Sims, its communications director, said "the term 'colored' is not derogatory, [the NAACP] chose the word 'colored' because it was the most positive description commonly used [in 1909, when the association was founded]. It's outdated and antiquated but not offensive."<ref>{{cite web|url = http://blogs.mercurynews.com/aei/2008/11/12/lohan-calls-obama-colored-naacp-says-no-big-deal#ftnb|title = Lohan calls Obama ‘colored’, NAACP says no big deal|publisher = Mercury News|date=November 12, 2008}}</ref>

==See also==
{{Portal|United States}}
{{div col|colwidth=30em}}
*[[Anglo#Africa|Anglo-African term]]
*[[Black people]]
*[[Casta]]
*[[Coloured]]
*[[Free people of color]]
*[[Person of color]]
*[[United States Colored Troops]]
*[[Negro]]
*[[Nigger]]
*[[Nigga]]
{{div col end}}

==References==
{{Reflist|30em}}

{{Ethnic slurs}}

[[Category:African-American society|+]]
[[Category:History of the United States]]
[[Category:Race in the United States]]
[[Category:Anti-African and anti-black slurs]]
[[Category:Person of color]]