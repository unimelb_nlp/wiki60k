{{good article}}
{{Infobox album  <!-- See Wikipedia:WikiProject_Albums -->
| Name        = 9.0: Live
| Type        = live
| Artist      = [[Slipknot (band)|Slipknot]]
| Cover       = Slipknot-9.0_Live_Cover.jpg
| Border      = yes
| Alt         = Arms protrude from the right side and move towards the left.
| Released    = November 1, 2005
| Recorded    = 2004–05
| Genre       = {{hlist|[[Heavy metal music|Heavy metal]]|[[alternative metal]]|{{nowrap|[[nu metal]]}}<!--- Please don't add any additional tags without a source -->}}
| Length      = 78:07
| Label       = [[Roadrunner Records|Roadrunner]]
| Producer    = {{hlist|[[Slipknot (band)|Slipknot]]|[[Joey Jordison]]}}
<!-- Don't add reviews to the infobox, the reviews parameter has been deprecated. -->
| Last album  = ''[[Vol. 3: (The Subliminal Verses)]]''<br />(2004)
| This album  = '''''9.0: Live'''''<br />(2005)
| Next album  = ''[[All Hope Is Gone]]''<br />(2008)
| Misc        = {{Singles
  | Name = 9.0: Live
  | Type = live
  | Single 1       = [[The Nameless]]
  | Single 1 date  = November 1, 2005
  }}
}}
'''''9.0: Live''''' is the first [[live album]] by American [[heavy metal music|metal]] band [[Slipknot (band)|Slipknot]]. The band recorded the two-disc album during a 2004–05 world tour that promoted their third studio album ''[[Vol. 3: (The Subliminal Verses)]]''.  Released by [[Roadrunner Records]] on November 1, 2005, ''9.0: Live'' features tracks from Slipknot's first three studio albums: ''[[Slipknot (album)|Slipknot]]'', ''[[Iowa (album)|Iowa]]'', and ''[[Vol. 3: (The Subliminal Verses)]]''. Many of the included tracks are rarely played live; "Skin Ticket" from the album ''Iowa'' was its first live performance. ''9.0: Live'' peaked in the top twenty in album sales for Austria and the United States, and was [[RIAA certification|certified gold]] in the United States. Critical reception was generally positive, with Adrien Begrand of ''[[PopMatters]]'' calling it a "very worthy live album".<ref name=Pop />

==Recording and production==
While producing their second DVD ''[[Disasterpieces]]'' in 2002, the band members of Slipknot were inspired to produce a live album after noticing how well they performed when they knew they were being recorded.<ref name=MTV/> Two years later, in 2004, Slipknot promoted ''Vol. 3: (The Subliminal Verses)'' during a world tour which included 233 concerts across 34 countries in 28 months;<ref name="blab"/><ref name="Voliminal">{{cite video |title= [[Voliminal: Inside the Nine]] |date= 2006 |people= Shawn Crahan (Director) |publisher= Roadrunner Records |medium= DVD}}</ref> music for the live album was recorded during the tour.<ref name=MTV>{{cite news |first= Jon|last= Wiederhorn|title= Slipknot Cap A Year Of Destruction With 9.0: Live; More Stone Sour On Tap |url=http://www.mtv.com/news/articles/1510831/20051003/slipknot.jhtml?headlines=true|publisher= [[MTV]]|date= 2005-10-03|accessdate=2008-07-03}}</ref> The tracks on ''9.0: Live'' were compiled from performances in [[Singapore]], [[Tokyo]], [[Osaka]], [[Las Vegas, Nevada|Las Vegas]], [[Phoenix, Arizona|Phoenix]], [[New York City]], and [[Dallas]].<ref>{{cite news |title = Slipknot: '9.0 Live' Certified Gold |publisher = Roadrunner Records |date = 2006-01-06 |url = http://www.roadrunnerrecords.com/blabbermouth.net/news.aspx?mode=Article&newsitemID=46452 |accessdate = 2007-07-03}}</ref>

Percussionist [[Shawn Crahan]] said the band made an effort to pay more attention to detail than usual during the tour, noting, "when you've got a microphone hanging onto your every note, you tend to give maybe 115 percent instead of 110 percent".<ref name=MTV/> The album begins with a staged vocal introduction which was recorded before a concert, informing the audience that the band would not be performing, in an effort to incite anger in the crowd.<ref name=Pop/> ''9.0: Live'' includes tracks from the band's first three studio albums, and the banned track "Purity" which was removed from the band's debut album, ''Slipknot'', due to [[Slipknot (album)#Controversy|copyright issues]].<ref>{{Cite book |last= Arnopp |first= Jason |title= Slipknot: Inside the Sickness, Behind the Masks |publisher= Ebury |year= 2001 |isbn= 0-09-187933-7|pages=159–61 |postscript= <!--None-->}}</ref> It also contains tracks that are rarely played live, such as "Iowa" and "Get This",<ref name=OMH/> as well as the only live performance of "Skin Ticket".<ref>{{cite video |date= 2005|title= Slipknot – 9.0: Live|medium= CD|publisher= [[Roadrunner Records|Roadrunner]]}}</ref>

==Promotion==
Before the album's release, a sample from the live recording of "[[The Nameless]]" was made available on the Internet through the band's record label.<ref>{{cite news |title = Slipknot: Live Album Sample Posted Online |publisher = [[Blabbermouth.net]] |date = 2005-10-13 |url = http://www.roadrunnerrecords.com/blabbermouth.net/news.aspx?mode=Article&newsitemID=42847 |accessdate = 2008-08-31}}</ref> Slipknot attended a signing session at a [[Best Buy]] store in New York City the day of ''9.0: Live''{{'}}s release, on November 1, 2005.<ref name="blab">{{cite news |title = Slipknot To Sign Copies Of Live Album In New York City  |publisher = [[Blabbermouth.net]] |date = 2005-10-09 |url = http://www.roadrunnerrecords.com/blabbermouth.net/news.aspx?mode=Article&newsitemID=42685 |accessdate = 2008-08-31}}</ref> A music video featuring the live recording of "The Nameless" was created to promote the album.<ref name=MTV/> Head of marketing at Roadrunner Records, Bob Johnsen, stated that the price of ''9.0: Live'' was reduced in an effort to give "added value", resulting in the double-disc album being "two hours of music for the price of one".<ref name=bostonglobereview/> Johnsen continued, stating that as with most live albums, ''9.0: Live'' targeted the "band's most hard-core fans. It's a complete immersion in the band".<ref name=bostonglobereview/> The album booklet includes 24 pages, most with pictures of band members.<ref name=bostonglobereview/>

==Reception==
{{Album ratings
| rev1 = [[Allmusic]]
|rev1score = {{rating|4|5}}<ref name=AMG />
| rev2 = [[Blabbermouth.net]]
|rev2score = 8/10<ref name="BlabReview">{{cite web |first=Don |last=Kaye |title=Review: ''9.0 Live'' |url=http://www.blabbermouth.net/cdreviews/live/ |publisher=[[Blabbermouth.net]] |accessdate=30 August 2016}}</ref>
| rev3 = ''[[musicOMH]]''
|rev3score = favorable<ref name=OMH />
| rev4 = ''[[PopMatters]]''
|rev4score = 7/10<ref name=Pop />
| rev5 = ''[[Revolver (magazine)|Revolver]]''
|rev5score = favorable<ref name="RevolverReview">{{cite web |url=http://www.revolvermag.com/content/slipknot-best-rest |title=Slipknot: The Best Of The Rest |author=Jon Wiederhorn |work=[[Revolver (magazine)|Revolver]] |accessdate=2010-01-29| archiveurl= https://web.archive.org/web/20100210052200/http://www.revolvermag.com/content/slipknot-best-rest| archivedate= 10 February 2010 <!--DASHBot-->| deadurl= no}}</ref>
| rev6 = ''[[Rolling Stone]]''
|rev6score = {{rating|3|5}}<ref name=RS />
}}
Critical reception of ''9.0: Live'' was generally positive. Reviewing for [[Allmusic]], Johnny Loftus commented that the fans' relationship with Slipknot is "what unifies the performances" on the live album.<ref name=AMG>{{cite web |url= {{Allmusic|class=album|id=r804720/review|pure_url=yes}}|title= 9.0 Live Review|accessdate=2008-05-15 |author= Johnny Loftus|publisher=[[Allmusic]]}}</ref> He said that throughout the band's history, they have never compromised, and they had become "metal stars the real way, through relentless touring, embracing fan support, and penning some truly brutal songs".<ref name=AMG/> ''[[Rolling Stone]]''{{'}}s reviewer Christian Hoard wrote that the music featured on the album resembled a "new-school [[Motörhead]]", with its "scary-clown rap-metal bullshit getting steamrolled by big riffs and speed-punk beats".<ref name=RS/> However, he noted the songs sounded similar to their recorded performances; Hoard called it the "songs' samey-ness".<ref name=RS>{{cite web |url= http://www.rollingstone.com/artists/slipknot/albums/album/8655931/review/8719191/90_live|title= 9.0 Live Review|accessdate=2008-05-15|author= Christian Hoard|date=2005-11-04|publisher=''[[Rolling Stone]]''|archiveurl=https://web.archive.org/web/20070620201400/http://www.rollingstone.com/artists/slipknot/albums/album/8655931/review/8719191/90_live|archivedate=2007-06-20|deadurl=yes}}</ref> Adrien Begrand of ''[[PopMatters]]'' called ''9.0: Live'' a "very worthy live album", and complimented the band for gaining success the "old-fashioned way, building a strong reputation as an extremely potent live act".<ref name=Pop>{{cite web |url= http://www.popmatters.com/music/reviews/s/slipknot-90live.shtml|title= Slipknot: 9.0 Live|accessdate=2008-05-15 |author= Adrien Begrand |date= 2005-11-30|publisher=[[PopMatters]]| archiveurl= https://web.archive.org/web/20080506130344/http://www.popmatters.com/music/reviews/s/slipknot-90live.shtml| archivedate= 6 May 2008 <!--DASHBot-->| deadurl= no}}</ref> Begrand noted the band's relationship with their "extremely devoted fans" as a strong point, and that the band's fans are "arguably the most fervently loyal bunch since the early days of Metallica two decades ago".<ref name=Pop/> However, he complained that it was distracting to have the band perform in a variety of undisclosed locations, rather than the one set throughout.<ref name=Pop/>  Tom Day of ''[[musicOMH]]'' wrote that the song "[[Before I Forget (song)|Before I Forget]]" is a "true gem and grinds out with a level of devastation that will make you green with envy if you weren't at these shows", and  that drummer [[Joey Jordison]] took "centre stage" throughout the performance.<ref name=OMH>{{cite web |url= http://www.musicomh.com/albums/slipknot_1105.htm|title= Slipknot – 9.0: Live (Roadrunner)|accessdate=2008-05-17 |author= Tom Day|publisher=musicOMH| archiveurl= https://web.archive.org/web/20080522145633/http://www.musicomh.com/albums/slipknot_1105.htm| archivedate= 22 May 2008 <!--DASHBot-->| deadurl= no}}</ref> Blair Fischer of the ''[[Chicago Tribune]]'' gave the album "three volume levels", writing, "The most amazing feat is that nine genetic defectives can congeal for such synchronous brain-damaged fury".<ref>{{cite news |first= Blair|last= Fischer|title= Slipknot – ''9.0: Live''|publisher=''[[Chicago Tribune]]'' |work= Music|page= 55|date= 2005-12-16}}</ref> ''[[Billboard (magazine)|Billboard]]'' reviewer Christa Titus wrote that Slipknot was "relentless in its delivery" of their live performances, calling the album "an overwhelming frenzy of sound and fury".<ref name=BillRev/> Titus predicted the album would chart highly.<ref name=BillRev>{{Cite journal |last=Titus |first=Christa L. |publication-date=2005-11-05|title=9.0: Live |periodical=[[Billboard (magazine)|Billboard]]|volume=117 |issue=5| issn=0006-2510 |author2=Jonathan Cohen}}</ref>

Some critics commented that the album is not as appealing to audiences who are unfamiliar with the band. Saul Austerlitz from ''[[The Boston Globe]]'' wrote that the album was "intended to cater primarily to rabid fans", commenting that those who are not fans of Slipknot will probably "find the experience of listening to both discs of ''9.0: Live'' roughly comparable to being hit in the head repeatedly with a two-by-four two hours of sludgy, indistinguishable songs, punctuated by profane outbursts about how the idiot media [...] has ignored and abused them".<ref name=bostonglobereview>{{cite news|title=Two Live Albums in One? A Format Returns, with Twists|publisher=''[[The Boston Globe]]''|first= Saul|last= Austerlitz|date=2006-01-15|page=N1}}</ref>

''9.0: Live'' debuted at number 17 on the [[Billboard 200|''Billboard'' 200]] charts in the United States, selling 42,000&nbsp;copies in its first week.<ref>{{cite news |first= Margo|last= Whitmire|title= Now that's what I call highly popular music|publisher= ''Ventura County Star''|work= Life, Arts and Living|page= 6|date= 2005-11-11}}</ref> The album also premiered in the top 50 in five other countries.<ref>Note: See "Reception" section for specific citations</ref> On December 9, 2005, the [[Recording Industry Association of America]] certified ''9.0: Live'' [[RIAA certification|gold]] in the United States.<ref>{{cite web|url=http://www.riaa.com/goldandplatinumdata.php?&artist=slipknot|title=Gold and Platinum database|publisher=[[Recording Industry Association of America]]|accessdate=2008-01-25}}</ref>

==Track listing==
All songs credited to [[Slipknot (band)|Slipknot]] and, as noted, were recorded live.

;Disc one
{{ordered list
 |"[[Vol. 3: (The Subliminal Verses)|The Blister Exists]] &nbsp; – 6:24
 |"[[Slipknot (album)|(sic)]]"&nbsp; – 3:52
 |"[[Iowa (album)|Disasterpiece]]"&nbsp; – 6:47
 |"[[Before I Forget (song)|Before I Forget]]"&nbsp; – 4:24
 |"[[Left Behind (Slipknot song)|Left Behind]]"&nbsp; – 3:44
 |"[[Slipknot (album)|Liberate]]"&nbsp; – 3:48
 |"[[Vermilion (song)|Vermilion]]"&nbsp; – 5:56
 |"[[Vol. 3: (The Subliminal Verses)|Pulse of the Maggots]]"&nbsp; – 5:06
 |"[[Slipknot (album)|Purity]]"&nbsp; – 5:12
 |"[[Slipknot (album)|Eyeless]]"&nbsp; – 4:19
 |"Drum Solo"&nbsp; – 3:58
 |"[[Slipknot (album)|Eeyore]]"&nbsp; – 2:16
}}
;Disc two
{{ordered list|start=13
 |"[[Vol. 3: (The Subliminal Verses)|Three Nil]]"&nbsp; – 5:03
 |"[[The Nameless]]"&nbsp; – 5:28
 |"[[Iowa (album)|Skin Ticket]]"&nbsp; – 6:03
 |"[[Iowa (album)|Everything Ends]]"&nbsp; – 5:03
 |"[[The Heretic Anthem]]"&nbsp; – 4:08
 |"[[Iowa (album)|Iowa]]"&nbsp; – 6:37
 |"[[Duality (song)|Duality]]"&nbsp; – 6:07
 |"[[Spit It Out (Slipknot song)|Spit It Out]]"&nbsp; – 5:29
 |"[[Iowa (album)|People = Shit]]"&nbsp; – 5:53
 |"Get This"&nbsp; – 2:44
 |"[[Wait and Bleed]]"&nbsp; – 3:44
 |"[[Slipknot (album)|Surfacing]]"&nbsp; – 5:50
}}

==Personnel==
Aside from their real names, members of the band are referred to by numbers zero through eight.<ref name=Allbio>{{cite web |url= {{Allmusic|class=artist|id=p41591|pure_url=yes}}|title= Biography|accessdate=2008-07-31 |work= Slipknot|publisher= [[Allmusic]]}}</ref>
{{col-begin}}
{{col-2}}

*(#0) [[Sid Wilson]]&nbsp;– [[Phonograph#Turntable technology|turntables]]
*(#1) [[Joey Jordison]]&nbsp;– [[Drum kit|drums]]
*(#2) [[Paul Gray (Slipknot)|Paul Gray]]&nbsp;– [[Bass guitar|bass]], [[backing vocals]]
*(#3) [[Chris Fehn]]&nbsp;– [[Percussion instrument|custom percussion]], backing vocals, co-lead vocals {{small|(track 14)}}
*(#4) [[James Root|Jim Root]]&nbsp;– [[Electric guitar|guitars]]
*(#5) [[Craig Jones (musician)|Craig Jones]]&nbsp;– [[Sampler (musical instrument)|samples]], [[Sampling (music)|media]],  [[Keyboard instrument|keyboard]]
*(#6) [[Shawn Crahan]]&nbsp;– custom percussion, backing vocals
*(#7) [[Mick Thomson]]&nbsp;– guitars
*(#8) [[Corey Taylor]]&nbsp;– [[Singing|vocals]]

{{col-2}}

* Dave Nichols&nbsp;– recording
* [[Colin Richardson]]&nbsp;– mixing
* [[Matt Hyde (British producer)|Matt Hyde]]&nbsp;– assistant mixing
* [[Ted Jensen]]&nbsp;– mastering
* [[Monte Conner]]&nbsp;– A&R
* Cory Brennan&nbsp;– management
* Rick Roskin&nbsp;– North America booking agent
* John Jackson&nbsp;– worldwide booking agent
* Michael Boland&nbsp;– design
* Shigeo Kikuchi&nbsp;– photography
* Eddie Sung&nbsp;– black and white images
* Ash Newell&nbsp;– back cover image
* Bob Johnsen&nbsp;– marketing
* Ed Rivadavia&nbsp;– digital marketing

{{col-end}}

==Chart positions==
{|class="wikitable sortable"
!Chart (2005)
!Peak<br>position
|-
|[[ARIA Charts|Australian Albums Charts]]<ref>{{cite web|url=http://www.australian-charts.com/showitem.asp?interpret=Slipknot&titel=9.0:+Live&cat=a|title=Slipknot Australian Charts|publisher=australian-charts.com|accessdate=2008-02-09}}</ref>
|align="center"|26
|-
|[[Ö3 Austria Top 40|Austrian Albums Chart]]<ref>{{cite web|url=http://austriancharts.at/showitem.asp?interpret=Slipknot&titel=9.0:+Live&cat=a|title=Slipknot Austrian Charts|publisher=austriancharts.at|accessdate=2008-02-07|language=German}}</ref>
|align="center"|18
|-
|[[Ultratop|Belgium Albums Chart]] ([[Flanders]])<ref>{{cite web|url=http://www.ultratop.be/nl/showitem.asp?interpret=Slipknot&titel=9.0:+Live&cat=a|title=Slipknot Belgium (Flanders) Charts|publisher=ultratop.be|language=Dutch}}</ref>
|style="text-align:center;"|53
|-
|Belgium Albums Chart ([[Wallonia]])<ref>{{cite web|url=http://www.ultratop.be/fr/showitem.asp?interpret=Slipknot&titel=9.0:+Live&cat=a|title=Slipknot Belgium (Wallonia) Charts|publisher=ultratop.be|language=French}}</ref>
|style="text-align:center;"|61
|-
|[[MegaCharts|Dutch Albums Charts]]<ref>{{cite web|url=http://dutchcharts.nl/showitem.asp?interpret=Slipknot&titel=9.0:+Live&cat=a|title=Slipknot Dutch Charts|publisher=dutchcharts.nl|accessdate=2008-02-07|language=Dutch}}</ref>
|align="center"|71
|-
|[[Syndicat National de l'Édition Phonographique|French Albums Chart]]<ref>{{cite web|url=http://lescharts.com/showitem.asp?interpret=Slipknot&titel=9.0:+Live&cat=a|title=Slipknot French Charts|publisher=lescharts.com|accessdate=2008-02-09|language=French}}</ref>
|align="center"|41
|-
|[[Media Control Charts|German Albums Chart]]<ref>{{cite web|url=http://www.musicline.de/de/chartverfolgung_summary/artist/Slipknot/?type=longplay|title=Slipknot German Charts|accessdate=2008-02-09|publisher=musicline.de|language=German}}</ref>
|align="center"|24
|-
|[[Irish Albums Chart]]<ref>{{cite web|url=http://irish-charts.com/showinterpret.asp?interpret=Slipknot|title=Slipknot Irish Charts|publisher=irish-charts.com}}</ref>
|style="text-align:center;"|56
|-
|[[Oricon|Japanese Albums Chart]]<ref>{{cite web|url=http://www.oricon.co.jp/prof/97082/products/621723/1/|title=Slipknot Japanese Charts|accessdate=2016-09-25|publisher=oricon ME inc.|language=Japanese}}</ref>
|style="text-align:center;"|23
|-
|[[Sverigetopplistan|Swedish Albums Chart]]<ref>{{cite web|url=http://swedishcharts.com/showitem.asp?interpret=Slipknot&titel=9.0:+Live&cat=a|title=Slipknot Swedish Charts|publisher=swedishcharts.com|accessdate=2008-02-07}}</ref>
|align="center"|40
|-
|[[Swiss Music Charts|Swiss Albums Chart]]<ref>{{cite web|url=http://hitparade.ch/showitem.asp?interpret=Slipknot&titel=9.0:+Live&cat=a|title=Slipknot Swiss Charts|publisher=hitparade.ch|accessdate=2008-02-07}}</ref>
|align="center"|43
|-
|[[UK Albums Chart]]<ref>{{cite web|url=http://www.theofficialcharts.com/artist/_/slipknot|title=Slipknot|publisher=[[The Official Charts Company]]}}</ref>
|style="text-align:center;"|53
|-
|US [[Billboard 200|''Billboard'' 200]]<ref>{{cite web|url={{BillboardURLbyName|artist=slipknot|chart=all}}|title=9.0: Live Slipknot|publisher=''Billboard''|accessdate=2008-02-07}}</ref>
|align="center"|17
|}

==References==
{{reflist|2}}

{{Slipknot}}

[[Category:2005 live albums]]
[[Category:Slipknot (band) live albums]]
[[Category:Roadrunner Records live albums]]