{{Use dmy dates|date=May 2013}}
{{Use British English|date=May 2013}}
{{Infobox Military Structure
|name=Royal Air Force Station Wye
|ensign=[[File:Ensign of the Royal Air Force.svg|90px]]
|partof=
|location= Located near [[Wye, Kent]], [[England]]
|image=
|caption=
|type=Airfield
|code=
|built=1916
|builder=Vickers / RFC
|materials=
|height=
|used=1916–1919
|demolished=
|condition=Closed
|commanders=
|occupants=[[Royal Flying Corps]]<br>[[Royal Air Force]]
|battles=[[First World War]]
|events=
|map_type=Kent
|coordinates = {{coord|51.1917|0.9255|type:landmark_region:GB-KEN|format=dms|display=inline}}
}}
'''Royal Air Force Station Wye''' was a temporary [[First World War]] training airfield at [[Wye, Kent]].<ref name="Lee" />

==History==
Wye aerodrome was opened in May 1916 by the [[Royal Flying Corps]] as a training airfield, it had a grass landing field and was located on {{convert|86|acre|ha}} of low-lying meadow between the main [[A28 road|Canterbury to Ashford road]] and the [[Ashford to Ramsgate (via Canterbury West) line|railway line]].<ref name="Lee" /> No. 20 Reserve Squadron moved from nearby Dover on 1 June 1916, it operated the [[Avro 504]] biplane trainer, the [[Royal Aircraft Factory RE.8]] a two-seat biplane reconnaissance and bomber and the [[Royal Aircraft Factory B.E.2]] reconnaissance biplane.<ref name="Lee" /> In January 1917 No. 51 Reserve Squadron arrived as the demand for aircrew for the [[Western Front (World War I)|Western Front]] increased and in May 1917 a third squadron (No. 66 Reserve Squadron) was formed from personnel and equipment from the two squadrons.<ref name="Lee" /> At the end of May the Reserve Squadrons (now renamed Training Squadrons) moved out of Wye when the aerodrome was allocated for use as an Anglo-American training airfield.<ref name="Lee" />

Three metal-clad aeroplane sheds were erected to join the original portable [[Bessonneau hangar]] and in May 1917 [[No. 65 Squadron RAF|65 Squadron]] was based with [[Sopwith Camel]]s for four months before it moved to France.<ref name="Jefford" /> No. 86 Squadron moved in from Dover with [[Sopwith Pup]] and [[Sopwith Camel]] biplane fighters to train at the end of 1917.<ref name="Lee" />  In December 1917 No. 42 Training Squadron arrived and continued to train British pilots which were joined in the mid-1918 by Americans trainees.<ref name="Lee" /> Following the [[Armistice with Germany]] the Americans departed but the training carried on, albeit not at the same pace until the training squadron was disbanded on 1 February 1919.<ref name="Lee" />

Between February and May 1919 the aerodrome was used by [[No. 3 Squadron RAF|3 Squadron]] when it returned from France.<ref name="Jefford" /> RAF Wye was declared surplus to requirements in October 1919 and was restored to agricultural use.<ref name="Lee" /> 

==Royal Flying Corps/Royal Air Force units and aircraft==
{|class="wikitable"
|-
!Unit
!Dates
!Aircraft
!Notes
|-
|[[No. 3 Squadron RAF]]
|15 February 1919 -<br>2 May 1919<ref name="Delve"/>
|[[Sopwith Camel]]
|On return from France as [[wikt:Cadre|cadre]]<ref name="Jefford" />
|-
|[[No. 61 Squadron RAF|No. 61 Squadron RFC]]
|5 July 1916 -<br>24 August 1916<ref name="Delve"/>
|None
|Used personnel from No. 20 Reserve Squadron<ref name="Jefford" />
|-
|[[No. 65 Squadron RAF|No. 65 Squadron RFC]]
|29 May 1917 -<br>24 October 1917<ref name="Delve"/>
|[[Sopwith Camel]]
|Moved to France<ref name="Jefford" />
|-
||[[No. 86 Squadron RAF|No. 86 Squadron RFC]]
|17 September 1917 -<br>16&nbsp;December&nbsp;1917<ref name="Delve"/>
|Various
|<ref name="Jefford" />
|-
|No. 20 Training Squadron RFC
|24 July 1916 -<br>1 June 1917<ref name="Delve" />
|[[Avro 504]],<br>[[Royal Aircraft Factory BE.2|BE.2c]],<br>[[Royal Aircraft Factory RE.8|RE.8]]
|Also known as 20 Reserve Squadron<ref name="Sturtivant277" />
|-
|No. 42 Training Squadron RFC/RAF
|16 December 1917 -<br>1 February 1919<ref name="Delve"/>
|Various
|Also known as 42 Reserve Squadron<ref name="Sturtivant278" />
|-
|No. 51 Training Squadron RFC
|8 January 1917 -<br>14 May 1917<ref name="Delve"/>
|Various
|Also known as 51 Reserve Squadron<ref name="Delve" />
|-
| No. 66 Reserve Squadron RFC
| 1 May 1917 -<br>10 May 1917<ref name="Delve"/>
| Various
|
|}

==Accidents and incidents==
On 15 March 1917 Captain [[Oliver Bryson]] was awarded the [[Albert Medal (lifesaving)|Albert Medal]] for his rescue of his passenger following a crash at Wye Aerodrome:

{{Quotation|On the 15th March, 1917, Captain (then Lieutenant) Bryson, with Second Lieutenant Hillebrandt as passenger, was piloting an aeroplane at Wye Aerodrome when, owing to a sideslip, the machine crashed to the ground and burst into flames. On disentangling himself from the burning wreckage Captain Bryson at once went back into the flames, dragged Lieutenant Hillebrandt from the machine, and notwithstanding his own injuries, which were undoubtedly aggravated by his gallant efforts to rescue his brother officer from the fire, endeavoured to extinguish the fire on Lieutenant Hillebrandt's clothing.  Lieutenant Hillebrandt succumbed to his injuries a few; days later.|London Gazette<ref name="albertmedal" />}}

==References==
;Notes
{{reflist|refs=
<ref name="Delve">Delve 2005, p. 267</ref>
<ref name="Jefford">Jefford 1985, p. 176</ref>
<ref name="Lee">Lee 2010, pp. 327-328</ref>
<ref name="Sturtivant277">Sturtivant 2007, p. 277</ref>
<ref name="Sturtivant278">Sturtivant 2007, p. 278</ref>
<ref name="albertmedal">{{LondonGazette |issue=30472 |date=11 January 1918 |startpage=732 |supp= |accessdate=17 February 2011}}</ref>
}}

;Bibliography
{{refbegin}}
*{{cite book |last= Delve|first= Ken|authorlink= |coauthors= |title= The Military Airfields of Britain. Southern England: Kent, Hampshire, Surrey and Sussex |year=2005 |publisher= The Crowood Press Ltd |location=Ramsbury |isbn=1-86126-729-0}}
* {{cite book |last= Jefford |first= C.G. |title= RAF Squadrons |year=1988 |publisher= Airlife Publishing Ltd |isbn= 1-85310-053-6}}
* {{cite book |last= Lee |first= David W. |title= Action Stations Revisited, Volume 3 South East England |year=2010 |publisher= Crecy Publishing Ltd |isbn= 978-0-85979-110-6}}
*{{cite book |last=Sturtivant |first=Ray | title = RAF Flying Training and Support Units since 1912 | publisher = Air-Britain | year = 2007 | isbn = 0-85130-365-X}}
{{refend}}

{{DEFAULTSORT:Wye}} 
[[Category:Royal Air Force stations in Kent]]
[[Category:Royal Flying Corps airfields]]
[[Category:Royal Flying Corps airfields in Kent]]
[[Category:Borough of Ashford]]