<!--Do not convert this article using Ref Converter, as it doesn't work with the pre-formatted infobox.  Any conversion must be done manually. -->
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = W 33
  |image = File:Junkers aircraft Bremen.jpg
  |caption = The W 33 ''Bremen'' after its historic Atlantic crossing.
}}{{Infobox Aircraft Type
  |type = Transport
  |manufacturer = [[Junkers]]
  |designer =
  |first flight = 17 June 1926<ref name="Kay">Kay p.64</ref>
  |introduced =Herman Pohlmann<ref>{{cite web|url=http://www.geocities.com/hjunkers/ju_who_p.htm |accessdate=2016-06-22 |last=Zoeller |first=Horst |coauthors= |date= |format= |work= |publisher=The Hugo Junkers Homepage |pages= |quote= |deadurl=unfit |archiveurl=https://web.archive.org/web/20091027072819/http://www.geocities.com/hjunkers/ju_who_p.htm |archivedate=October 27, 2009 }}</ref>
  |retired =
  |status =
  |primary user =
  |more users =
  |produced = 1927-34
  |number built = 199
  |unit cost =
  |variants with their own articles = [[Junkers W 34]]
}}
|}

The '''Junkers W 33''' was a German single-engine transport aircraft. It was aerodynamically and structurally advanced for its time (1920s), a clean, low-wing all-metal cantilever monoplane. Almost 200 were produced. It is remembered in [[aviation history]] for the first east–west non-stop heavier-than-air crossing of the Atlantic.

==Design and development==
The Junkers W 33<ref name="Turner">{{Harvnb|Turner|1971|pages=39–40}}</ref> was a transport development of the 1919 four-seat airliner, the [[Junkers F 13]].  The latter was a very advanced aircraft when built, an aerodynamically clean all-metal cantilever (without external bracing) monoplane.  Even later in the 1920s, it and other Junkers types were unusual as unbraced monoplanes in a biplane age, with only Fokker's designs of comparable modernity.  Like all Junkers designs from the  [[Junkers J 7|J 7]] fighter onwards,<ref name="Turner"/>  it used an aluminium alloy [[duraluminum]] structure covered with Junkers' characteristic corrugated dural skin.  The wings had the same span as the F 13, though the platform was a little different, and the length was the same as the F 13FE.<ref name="Turner"/>  The fuselage, though, was flatter-topped than that of the F 13.<ref name="Turner"/> A large port-side door gave access to the freight compartment.<ref name="Turner"/>  The  228&nbsp;kW Junkers L5 upright inline water-cooled engined was also the same as in the F 13FE, though much more powerful than the BMW motors of the F 13A, giving improved weightlifting compared with that early model. The designer of the W 33 was Herman Pohlmann.<ref>{{cite web|url=http://www.geocities.com/hjunkers/ju_who_p.htm |accessdate=2016-06-22 |last=Zoeller |first=Horst |coauthors= |date= |format= |work= |publisher=The Hugo Junkers Homepage |pages= |quote= |deadurl=unfit |archiveurl=https://web.archive.org/web/20091027072819/http://www.geocities.com/hjunkers/ju_who_p.htm |archivedate=October 27, 2009 }}</ref>

The cockpit and undercarriage were of their time, the former enclosed with two seats, and the latter fixed and divided with a tailwheel.  The Junkers '''W''' letter labelled<ref>{{Harvnb|Turner|1971|page=10}}</ref> the type as a seaplane (floatplane), but in practice W 33s flew as both at different times. The prototype W 33, registered ''D-921'', first flew, as a seaplane, from [[Leopoldshafen]] on the river Elbe near Dessau on 17 June 1926.<ref name="Kay"/>

Production began in 1927 and ran until 1934 with 198 production machines built.  Most of these were built at the Junkers works at [[Dessau]], but a small number were assembled at Junker's Swedish subsidiary AB Flygindustri at [[Limhamn]] near [[Malmö]] and in the USSR.<ref name="Gunston p131">Gunston 1995, p.131.</ref>  The Swedish plant had been set up in the early 1920s to avoid the post-war restrictions on aircraft building, which included civil types during 1921-2. The Russian works at [[Fili (Moscow)|Fili]] near Moscow was used initially to build the [[Junkers H 21|H 21]] and [[Junkers H 22|H 22]] fighters for the Red Army.   There were over 30 W 33 variants, so only a few are listed below.

==Operational history==
[[File:Junkers W33.jpg|thumb|right|The Transatlantic W 33 on display at Bremen airport]]
[[File:Junkers W33 hydroplane.jpg|right|thumb|Junkers W 33 first prototype ''D-921'' at the ''Deutschen Seeflug'' competition, July 1926]]

The first two W33 prototypes competed very soon after their first flights at the ''Deutschen Seeflug'' seaplane competition at [[Warnemünde]] in July 1926. The first prototype W 33, ''D-921'', flew as no.7 and came second in the contest. The second prototype, a W 33a, flew as no.8.<ref name="Kay"/>

W 33s were used by many operators across the world in the late 1920s and 1930s.<ref name="Turner"/>  They served as general transports and specialised mailplanes. [[Deutsche Luft Hansa]] had only four, which ran mail from 1929. Others were used as survey aircraft and crop-sprayers. Later, the Luftwaffe used some as trainers.

The [[Colombian Air Force]] used Junkers W 33, W 34 and K 43 in the [[Colombia-Peru War]] (1932–1933).<ref name="Leticia p3-4">von Rauch 1984, pp.3—4.</ref> The [[Ethiopian Air Force]] had one W 33c during the [[Second Italo-Abyssinian War]].

One Junkers W 33g was used by the Swedish Air Force from 1933-5 as an air ambulance and known as the transport type [[List of military aircraft of Sweden|Trp2]]. It is possible that this aircraft was assembled at Linhamm, as were four W 33s bound for Australia.<ref name=GY/>  After World War II, this aircraft served in a utility role with the Swedish Air Force squadron F2 flying out of Hägernäs.  From there, in June 1952, it participated in the search and rescue operations during the famous Catalina Affair, in which two Soviet MiG-15s engaged and shot down a Swedish intelligence aircraft (a C-47) and then later another SAR aircraft, a PBY Catalina.<ref>http://fly.historicwings.com/2012/06/the-catalina-affair/</ref>

Russian-registered W 33s (17 of them, mostly assembled in Russia at Fili from imported parts) were designated PS-4 for Passazhirskii Samolyot (passenger aircraft or airliner). At least 9 Russian-built W 33s appeared on that country's pre-war civil register.<ref name=GY/><ref>http://www.mojobob.com/roleplay/hero/pulp/resources/vehicles/junkers.html</ref>

===Aeronautical distinctions===
The Junkers W 33 set numerous records, and one made the first east-to-west crossing of the Atlantic by airplane.<ref name="Turner"/> The North Atlantic had been first crossed, non-stop and by a heavier-than-air craft, by [[John Alcock (aviator)|Alcock]] and [[Arthur Whitten Brown|Brown]] in 1919 in a [[Vickers Vimy]]. They flew west to east, with the prevailing winds from [[St. John's, Newfoundland and Labrador|St. John's, Newfoundland]], to [[Clifden]], Ireland, in just under 16 hrs. The east-to-west flight was always more challenging, but became feasible as aircraft performance improved through the 1920s. On April 12–13, 1928, W 33 [[Bremen (aircraft)|''D-1167 Bremen'']] flew from [[Baldonnel, Ireland|Baldonnel]] near Dublin in Ireland to [[Greenly Island, Canada]], off [[Labrador]] in 37 hours. Strong westerly winds took them north of their intended destination, New York, but they landed safely. The crew were [[Hermann Köhl|Köhl]], [[Ehrenfried Günther Freiherr von Hünefeld|von Hünefeld]] and [[James Fitzmaurice (pilot)|Fitzmaurice]]. The aircraft is now on display at [[Bremen Airport]], Germany.

A W 33 set class C world  records for both endurance of 52 h 22 m and distance (4661&nbsp;km or 2896&nbsp;mi) on one flight at Dessau between 3 and 5 August 1927, piloted by [[Johann Risztics]] and Edzard. A little earlier Fritz Loose and W.N. Schnabele had set another Class C record for duration and distance, this time carrying a useful 500&nbsp;kg load. They stayed aloft for 22 h 11 m and travelled 2736&nbsp;km (1701&nbsp;mi). At about the same time, the W33 set a similar pair of records in Class Cbis (Seaplanes).<ref>[http://www.flightglobal.com/pdfarchive/view/1927/1927%20-%200847.html ''Flight'', 3 November 1927, p.763]</ref>

A W 33 (much modified and sometimes referred to as a W 34) established a world altitude record on 26 May 1929, piloted by Willy Neuenhofen at 41,800&nbsp;ft (12740 m).<ref name="Turner"/>

There were failures, too. The first Swedish-assembled W33 was ready by May 1930, and was delivered two months later to Mitsubishi in Japan. This aircraft was used in an attempt in 1932 to fly across the Pacific Ocean to the US, but the attempt failed and the aircraft disappeared. Neither aircraft parts nor any survivors were found, although the search lasted over six months.{{citation needed|date=July 2012}}

==Accidents and incidents==
* 15 May 1932 - D-1925 ''Atlantis'' flown by Hans Bertram and Adolph Klausman landed on the [[Kimberley (Western Australia)|Kimberly coast]] of [[Western Australia]] while attempting to fly from [[Kupang]] to [[Darwin, Northern Territory|Darwin]] due to a navigational error, the crew were not [[1932 Kimberley rescue|rescued]] until June 1932.<ref name="Atlantis1932">{{cite book|title=Atlantis is missing... a true account. Australia's most bizarre air rescue|last=Winter|first=Barbara|year=1979|publisher=Angus & Robertson Publishers|location=Hong Kong|isbn=0-207-14233-5}}</ref>
* 29 October 1932 – D-2017 ''Marmara'' of [[Luft Hansa]] was on a freight flight from [[Croydon Airport|Croydon]] to [[Cologne]] when it crashed off the [[Kent]] coast.<ref name=Flight031132>{{cite magazine|title=Airport News - Croydon |magazine=Flight |issue=3 November 1932 |page=1027 |url=http://www.flightglobal.com/pdfarchive/view/1932/1932%20-%201107.html}}</ref>

==Variants==
;Junkers W 33:-b.-c,-dd and -f powered by a 310&nbsp;hp [[Junkers L5]] water-cooled inline engine.
;Junkers W 33:-c3e and -he powered by a 340&nbsp;hp [[Junkers L5G]] water-cooled inline engine.
;Junkers W 33:-dGao powered by a 540&nbsp;hp [[Siemens Sh 20]] radial engine.
;[[Junkers W 34]]: A 6-passenger version of the W 33 powered by a variety of [[radial engine]]s. Used by many airlines, including Luft Hansa<ref name=Turner/> and by the Luftwaffe, chiefly as a trainer but also for communications and transport duties until the end of [[World War II]].<ref name=Kay2/>
;[[Junkers K 43]]: Swedish-built bombing and reconnaissance version, equipped with openings for machine guns in the cabin roof and floor.<ref name=Kay2/>
;PS-3: Most Russian-registered W 33s had this designation.
;PS-4: W 33s built in the Soviet Union carried this designation.
;Trp2: Swedish Air Force designation.<ref name=Kay2/>

==Operators==

===Civil operators===
;{{flag|Brazil}}
*[[Syndicato Condor]]<ref name="Turner"/>
;{{flag|Canada|1921}}
*[[Canadian Airways]]<ref name="Grany ">Grant 2004, pp.70—75.</ref>
;{{GER}}
*[[Deutsche Luft Hansa]]<ref name="Turner"/>
;{{flag|Iceland|old}}: One on civil register<ref name=GY>http://www.airhistory.org.uk/gy/reg_index.html</ref> W.33d, "Súlan" (The Gannet)
;{{JPN}}: Two on civil register.<ref name=GY/>
;{{SWE}}: Three on civil register.<ref name=GY/>
;{{USSR}}
Soviet designation PS-4
*[[Deruluft]]<ref name="Gunston p131"/>
*[[Dobrolyot]]<ref name="Gunston p131"/>

===Military operators===
{{refimprove section|date=July 2012}}

;{{flag|Argentina}}
*[[Argentine Air Force]]{{citation needed|date=June 2014}}
;{{COL}}
*[[Colombian Air Force]]<ref name="Turner"/>
;{{flag|Ethiopia|1897}}
*[[Ethiopian Air Force]]
;{{flag|Germany|Nazi}}
*''[[Luftwaffe]]''<ref name=Kay2/>
;{{flag|Iran|1925}}
*[[Imperial Iranian Air Force]]
;{{flag|Mongolia|1924}}
*[[Mongolian People's Army|Mongolian People's Army Air Force]] operated 1 aircraft
;{{SWE}}
*[[Swedish Air Force]]<ref name=Kay2>Kay pp.72-5</ref>
;{{USSR}}
*[[Soviet Air Force]]<ref name="Gunston p131"/>

==Specifications (Landplane)==
{{Aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop
<!-- Now, fill out the specs.  Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For additional lines, end your alt units with )</li> and start a new, fully formatted line with <li> -->
|ref=Turner<ref name="Turner"/>
|crew=2-3
|length main=10.50 m
|length alt=34 ft 5.5 in
|span main=17.75 m
|span alt=58 ft 2.75 in
|height main=3.53 m
|height alt=11 ft 7 in
|area main=43.0 m²
|area alt=462.8 ft²
|empty weight main=1,220 kg
|empty weight alt=2,689 lb
|loaded weight main=
|loaded weight alt=
|max takeoff weight main=2,500 kg
|max takeoff weight alt=5,511 lb
|more general= cargo hold volume 15.7 m<sup>3</sup>, (169.4 cu ft): could carry 830 kg (1,830 lb) cargo
|engine (prop)=[[Junkers L 5]]
|type of prop=[[inline engine (aviation)|inline engine]]
|number of props=1
|power main=228 kW
|power alt=310 hp
|max speed main=180 km/h
|max speed alt= 120 mph
|cruise speed main= 150 km/h
|cruise speed alt= 93 mph
|stall speed main=
|stall speed alt=
|range main=1,000 km
|range alt=620 mi
|ceiling main=4,300 m
|ceiling alt=14,100
|climb rate main=
|climb rate alt=
|loading main=
|loading alt=
|power/mass main=
|power/mass alt=
|more performance=
|armament=
}}

==References==

===Notes===
{{Reflist}}

===Bibliography===
{{Refbegin}}
*Grant, Robert S. "Metal Marvels: Junkers W33s and W34s in the Canadian Bush". ''Air Enthusiast'' Number 110, March/April 2004. Stamford Lincs, UK:Key Publishing. {{ISSN|0143-5450}}. pp.&nbsp;70–75.
*[[Bill Gunston|Gunston, Bill]]. ''The Osprey Encyclopedia of Russian Aircraft 1875-1995''. London:Osprey, 1995. ISBN 1-85532-405-9.
*{{cite book |title= Junkers Aircraft & engines 1913-1945|last=Kay|first=Antony| year=2004|volume= |publisher=Putnam Aeronautical Books |location=London |pages =28–38|isbn=0-85177-985-9}}
*{{Cite book | last=Turner | first=P. St.John |author2=Nowarra, Heinz| year=1971 | title=Junkers: an aircraft album No.3| edition= | publisher=Arco Publishing Inc| location=New York | isbn=0-668-02506-9}}
*von Rauch, Georg. "A South American Air War...The Leticia Conflict". ''[[Air Enthusiast]]'' Number 26, December 1984-March 1985. Bromley Kent UK: Pilot Press. {{ISSN|0143-5450}}. pp.&nbsp;1–8.
{{Refend}}

==External links==
{{commons category|Junkers W 33}}

{{Junkers aircraft}}
{{RLM aircraft designations}}
{{Swedish transport aircraft}}

[[Category:German airliners 1920–1929]]
[[Category:German cargo aircraft 1920–1929]]
[[Category:Junkers aircraft|W33]]
[[Category:Single-engined tractor aircraft]]
[[Category:Low-wing aircraft]]