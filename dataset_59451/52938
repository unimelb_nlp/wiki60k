{{Redirect-distinguish|Systematic Botany Monographs|Monographs in Systematic Botany}}
{{for|the scientific discipline|Plant systematics}}
{{Infobox journal
| title = Systematic Botany
| cover = 
| editor = James F. Smith
| discipline = [[Botany]]
| former_names = 
| abbreviation = Syst. Bot.
| publisher = [[American Society of Plant Taxonomists]]
| country = United States
| frequency = Quarterly
| history = 1976–present
| openaccess = 
| license = 
| impact = 1.897
| impact-year = 2010
| website = http://www.aspt.net/publications/sysbot/
| link1 = http://www.bioone.org/
| link1-name = Online access via [[BioOne]]
| link2 = http://www.ingentaconnect.com/content/aspt/sb
| link2-name = Online access via [[IngentaConnect]]
| JSTOR = 03636445
| OCLC = 2531771
| LCCN = 76646396 
| CODEN = SYBODA
| ISSN = 0363-6445
| eISSN = 1548-2324
}}
'''''Systematic Botany''''' is a [[peer review|peer-reviewed]] [[scientific journal]] covering the study of [[Systematics|systematic]] [[botany]]. It is published quarterly by the [[American Society of Plant Taxonomists]].<ref name="official">{{cite web |url=http://www.aspt.net/publications/sysbot/ |title=Systematic Botany |publisher=[[American Society of Plant Taxonomists]]|accessdate=23 Mar 2012}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2010 [[impact factor]] of 1.897.<ref name=WoS>{{cite book |year=2012 |chapter=Systematic Botany |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2012-03-28 |series=Web of Science |postscript=.}}</ref>

''Systematic Botany'' was established in spring 1976 under founding [[editor-in-chief]] [[William Louis Culberson]] ([[Duke University]]).<ref>{{Cite journal | doi =  | title = Front Matter | journal = Systematic Botany | volume = 1 | issue = 1 | year = 1976 | pmid =  | pmc = }}</ref> The current editor-in-chief is James F. Smith ([[Boise State University]]).<ref name="official"/>

The ''American Society of Plant Taxonomists'' also publish the peer-reviewed [[Taxonomy (biology)|taxonomic]] [[Monograph#Taxonomy (systematic biology)|monograph]] series, ''Systematic Botany Monographs'' since 1980.<ref>{{cite web|url=http://www.scholarly-societies.org/history/SystematicBotanyMonogr.html|title=History of Scholarly Societies: Systematic Botany Monographs  |publisher=[[University of Waterloo]]|accessdate=24 Mar 2012}}</ref>

==Abstracting and indexing==
''Systematic Botany'' is abstracted and indexed in [[Agricola (database)|Agricola]],  [[Agris (database)|Agris]], [[BioOne]], [[PubMed]], [[Scirus]], and [[Science Citation Index Expanded]].

== References ==
{{reflist|30em}}

== External links ==
* {{official website|http://www.aspt.net/systematic-botany#.Uv9cTkaPN9A}}
* [http://herbarium.lsa.umich.edu/SBMweb/ ''Systematic Botany Monographs'']

[[Category:Publications established in 1976]]
[[Category:Botany journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Academic journals published by learned and professional societies]]