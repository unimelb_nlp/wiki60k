{{Orphan|date=January 2012}}

{{Location map
|Turkey
|label='''Çukuriçi Höyük'''
|label_size=100
|lat=37.939722
|long=27.340833
|marksize=9
|position=right
|width=280
|float=right
|caption='''Çukuriçi Höyük'''

}}
'''Çukuriçi Höyük''' ([[Turkish language|Turkish]]: "mound in the pit") is a prehistoric [[Tell (archaeology)|Tell]]settlement in western [[Turkey]]. It is situated about 1 Kilometer from [[Ephesos]] in the province of [[Izmir Province|Izmir]]. Excavation of the site began in 2006.<ref>http://www.oeai.at/index.php/praehistorische-forschungen.html</ref>

==History==

[[File:Cukurici grabung.jpg|thumb|The settlement after excavation 2009 (Photo: N. Gail, OeAI)]]

'''Çukuriçi Höyük''' is situated in what is today a very fertile plain surrounded by flourishing fruit plantations. Excavations have revealed that this hill, which is clearly visible in the landscape, is in fact a prehistoric tell (Höyük): through millennia-long usage as a settled site on the same location, massive occupation layers were created which have accumulated into an artificial hill. This type of settlement is typical for the Neolithic and Bronze Age periods (8th–2nd millennium B.C.) from the orient to south-east Europe; in western Anatolia, however, only a few tells have so far been excavated and systematically investigated.
At Çukuriçi Höyük it has been possible up until now to excavate five settlements lying on top of each other, from several periods and with differing dimensions. These offer a preliminary insight into the history of this settlement hill.
Already at the end of the 7th millennium B.C. (late Neolithic/early Chalcolithic) the site was used as a settlement: as a house built with stone socles and loam walls attests, people probably settled here permanently. The diet at this early stage was already varied, and was primarily based on domestic animal husbandry of pigs, sheep, and goats as well as cattle. The diet was supplemented by the hunting of rabbits, foxes, red deer and aurochs, as well as the collecting of shellfish. The technical specialisation of the people who settled here is proven by, amongst other things, high-quality ceramic vessels and beaten stone utensils, which were predominantly produced out of imported [[Obsidian]].<ref>StTroica 18, 2008, 251–273.</ref> This early settlement was vacated after its destruction, the reasons for which are still unclear, and apparently abandoned.<ref>ÖJh 77, 2008, 91–106</ref>
The next secure usage of the site which has been identified so far is attested approximately 1,500 years later, in the developed 4th millennium B.C. The tell would now be continuously occupied without a break until about 2,500 B.C., when in this Early Bronze Age period it was again abandoned.
The Early Bronze Age was an era of great and lasting changes in the Aegean region and in south-east Europe. Presumably triggered by the demand for metals and their trade, as well as the resulting prosperity from these activities, the first proto-urban centres and large fortified settlements arose. The role of western Anatolia during this important phase of development of human society is still in many respects unclear. On Çukuriçi Höyük in this period large single-room and multi-room buildings, with massive stone socles and mudbrick walls, were constructed. The most recent of these settlements was violently destroyed, most probably by an earthquake, and was not reconstructed. The spectrum of finds points to numerous differing and specialised activities within the settlement. Of particular significance is the manufacture of copper objects: the production of these objects can be demonstrated, from the initial casting in a prefabricated mould up to the finishing process in a forge. A distinct change in the inhabitants’ diet, in comparison with the previous period, can be observed: at Çukuriçi Höyük now, above all, sheep and goats were kept as domestic animals, while rabbits, foxes, wild boar and fallow deer were hunted, supplemented by the hunting of wolves, bears and various types of birds. The sea was also increasingly exploited as a source of food: in addition to the utilization of shellfish, already attested for the earlier periods, in the Early Bronze Age there is also evidence for the catching of fish, in both shallow and deep waters.<ref>A. Galik – B. Horejs, 2009</ref>

==Surroundings==

The reconstruction of the prehistoric landscape, the original course of the coastline, the ecological resources available, and finally the climatic conditions and their changes over the course of millennia constitute the central groups of questions with regard to the investigation of the hill in its micro-regional environment. Against this background it becomes clear that the settlement history can only be understood with the involvement of the most varied scientific disciplines. The research team which has been active at Çukuriçi Höyük since 2007 is therefore composed of individuals from the following disciplines: [[archaeology]], [[archaeometallurgy]], [[archaeozoology]], [[archaeobotany]], [[anthropology]], [[climatology]] and [[physics]] as well as [[paleogeography]] and [[geology]]/[[mineralogy]].<ref>http://www.oeai.at/index.php/praehistorische-forschungen.html</ref><ref>http://www.barbarahorejs.at/?page_id=16&langswitch_lang=en</ref>

==Significance==

The excavations at Çukuriçi Höyük have brought to light the oldest settlement near Ephesos, which began at least 8,200 years ago. Geological drilling at the foot of the hill allows the expectation of the discovery of additional settlement phases, which should extend even further back in time. With Çukuriçi Höyük, one of the oldest sites – not only in the surroundings of Ephesos – has now been rediscovered. Crucial questions regarding the dissemination of the Neolithic lifestyle from central Anatolia as far as south-east Europe constitute only one aspect of the investigation of the tell. The function of the hill in the 4th millennium, and the cultural developments that took place in this period, which ultimately led to the new epoch of the Bronze Age, comprise an additional broad area of research with many unanswered questions. Finally, Çukuriçi Höyük, with its well preserved settlement remains of the Early Bronze Age, offers the potential to understand more completely the lasting cultural changes which occurred in the early 3rd millennium B.C. Furthermore, its location at the intersection between the Anatolian and Aegean cultural regions enables the investigation of extensive contacts and relationships,<ref>Universitätsforschungen zur Prähistorischen Archäologie 169,2009, 358–368</ref> without which many developments in human prehistory are unthinkable.

==References==
<references/>

==External links==
* [http://www.barbarahorejs.at/?page_id=16&langswitch_lang=en The axcavator's homepage]
* [http://www.oeai.at/index.php/praehistorische-forschungen.html Homepage of the Austrian Archaeological Institute (OeAI)]
* [http://www.aegeobalkanprehistory.net Further information on the project]

==Literature==

* B. Horejs, Çukuriçi Höyük. A New Excavation Project in the Eastern Aegean. www.aegeobalkanprehistory.net (6.2.2008) ISBN 978-80-223-2376-5.
* M. Bergner – B. Horejs – E. Pernicka, Zur Herkunft der Obsidianartefakte vom Çukuriçi Höyük, StTroica 18, 2008, 251–273.
* B. Horejs mit Beiträgen von F. Galik und U. Thanheiser, Erster Grabungsbericht zu den Kampagnen 2006–2007 am Çukuriçi Höyük, ÖJh 77, 2008, 91–106.
* B. Horejs, Metalworkers at the Çukuriçi Höyük? An Early Bronze Age Mould and a “Near Eastern weight” from Western Anatolia, in: T. L. Kienlin – B. Roberts (Hrsg.), Metals and Societies. Studies in honour of Barbara S. Ottaway, Universitätsforschungen zur Prähistorischen Archäologie 169 (Bonn 2009) 358–368.
* B. Horejs, Çukuriçi Höyük, in: J. Koder – S. Ladstätter, Ephesos 2008, Kazı Sonuçları Toplantası 31, 2009, 321–22. 330–31.
* B. Horejs, Neues zur Frühbronzezeit in Westanatolien, in: F. Blakolmer u. a. (Hrsg.), Österreichische Forschungen zur Ägäischen Bronzezeit 2009. Tagung Salzburg, 6.–7.3.2009 (in print).
* B. Horejs, Çukuriçi Höyük. Neue Ausgrabungen auf einem Tell bei Ephesos, in: A. Kazim Oz – S. Aybek (Hrsg.), Festschrift (in print).
* B. Horejs, Çukuriçi Höyük. A Neolithic and Bronze Age Settlement in the Region of Ephesos, in: M. Özdoğan u. a. (Hrsg.), The Neolithic in Turkey. Second Enlarged Edition (in print).
* B. Horejs – M. Mehofer – E. Pernicka, Metallhandwerker im frühen 3. Jt. v. Chr. – Neue Ergebnisse vom Çukuriçi Höyük, IstMitt 60, 2010 (in print).
* A. Galik – B. Horejs, Çukuriçi Höyük – Different Aspects of its Earliest Settlement Phase, in: R. Krauß (Hrsg.), Beginnings. New Approaches in Researching the Appearing of the Neolithic between Northwestern Anatolia and the Carpathian Basin. Workshop held at Istanbul Department of the German Archaeological Institute, April 8–9, 2009 (in print).

{{coord|37|55|45|N|27|21|34|E|region:TR-35_type:landmark_source:kolossus-dewiki|display=title}}

{{DEFAULTSORT:Cukurici Hoyuk}}
[[Category:Archaeological sites in the Aegean Region]]
[[Category:Izmir Province]]
[[Category:Neolithic]]
[[Category:Chalcolithic]]
[[Category:Bronze Age sites]]