{{Refimprove|date=July 2007}}

{{Infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name         = Seola
| title_orig   = 
| translator   = 
| image        = SEOLA2 SM80.JPG
| caption      = ''Seola'' first edition spine cover.
 (There is no front cover artwork with the first edition novel.)
| author       = [[Ann Eliza Smith]]
| cover_artist = 
| country      = United States
| language     = English
| series       = 
| genre        = [[Christian novel]]
| publisher    = [[Lee and Shepard]] & [[Charles T. Dillingham]]
| release_date = 1878
| media_type   = Print ([[Hardcover|Hardback]])
| pages        = 251 pp ''(first edition)''
| preceded_by  = 
| followed_by  = 
}}
[[File:SEOLA3 SM80.JPG|thumb|right]]
'''''Seola''''' is an [[antediluvian]] novel published in 1878, written by [[Ann Eliza Smith]]. The publishers of the novel are Boston: Lee and Shepard, New York: Charles T. Dillingham.

The majority of the novel purports to be a translation of an ancient scroll diary written by a woman named Seola, who is identified as the wife of Japheth. The [[Book of Genesis]] indicates that [[Noah]] had three sons named [[Ham (son of Noah)|Ham]], [[Shem]] and [[Japheth]]. In the appendix section of the novel, Ann Smith describes how she was inspired to write the fantasy. She writes:

<blockquote>
''"Seola is a fantasy, revealed to the writer while listening to the performance of an extraordinary musical composition. It was sudden and unforeseen as the landscape which sometimes appears to a benighted traveller, for one instant only, illumined by the lightning's flash.''<ref>A book review publisher called, "Good Company-Sunday Afternoon: A magazine for the household" released an article on the novel Seola in 1878. The magazine believed the compositions that motivated Smith to write the novel was the prelude to the “Creation”, one of Strauss’s waltzes, a scrap of a symphony of Saint Saens, and Wagner’s Centennial March, all played simultaneously in a small room.</ref><br/>
<br/>
''It does not therefore pretend to be either history or theology, but yet the theory upon which the story is founded is in strict accordance with the sacred writings of the [[Hebrews]] and traditions of other ancient nations."''
</blockquote>

Some of her research into the ancient traditions of these nations can be found in her first published work, ''From Dawn to Sunrise''. The appendix and notes section at the end of the novel ''Seola'' explain certain passages within the story and how they are supported by real ancient texts. Portions of the story can be perceived as extrapolations from the [[Haggada]], the [[Mahabharata]], [[Book of Enoch|The Book of Enoch]] and the creation myths of Greek mythology.

==Plot==
The greatest discovery of the nineteenth century is found by accident. A team of archaeologists uncover one of the most ancient burial tombs of all time. Inside the tomb they find something far greater than gold or gems, they find a diary of a person who lived more than four thousand years ago. The team pool all their knowledge together in order to translate the scroll diary before it disintegrates in the foreign air.

The diary of Seola is about a girl's struggle to resist a wicked world. Her resolve to remain loyal to God is so strong she influences a fallen angel to repentance. The diary is unique because it gives a detailed account on how the Great Deluge started. One of the planets in the solar system becomes unstable and its destruction causes the waters above the expanse to fall.

The beginning entry of the journal gave no doubt as to the era the individual claims to come from. The entry reads, “West Bank of the [[Euphrates]], first moon-evening, after Adam, four cycles”. The author of the journal identifies herself as Seola, daughter of Aleemon and Lebuda. Her father was the son of [[Lamech (father of Noah)|Lamech]] and his father was [[Methuselah]]. Aleemon had a passion for study and the preservation of historical records. This desire kept him near a grand city which contained a wealth of knowledge on the history of the world written down on scrolls. The city’s name was [[Sippar]]a and it was also known as the city of the Sun. This desire also endangered the lives of his family. It so happened that the city was the royal seat of the one who ruled the planet. This ruler was known as [[Lucifer]] the Light Bearer, King of the Sun. He and his kind were ruling the earth for over 1100 years, since the days of [[Jared (ancestor of Noah)|Jared]]. These beings were known as the [[Deva (Hinduism)|Devas]]. The Devas were angelic spirit beings that materialized into human form. Their superior powers enabled them to dominate and instill fear into the human race. They sought after the most beautiful women of mankind and took them as wives. Through the union of the mortal female and the angelic being came male children of large stature. These offspring were known as the [[Darvand]]s. The Darvands were ruthless bullies with strength that none could match.

Seola begins her journal at the request of her father. Her first entries are common and uneventful because the family has been relocated to an isolated section of the forest away from Sippara and their life is peaceful with the isolation. The tranquility eventually comes to an end because the Devas discover their private sanctuary. The threat to the family is neither by wealth or possession but by way of beauty.

==Derivative works==
In 1924, an anonymous author published a revision of ''Seola'' under the title ''[[Angels and Women]].'' The content within the revised edition depends upon the teachings of the [[Jehovah’s Witnesses]] (called "[[Jehovah's Witnesses|Bible Students]]" before 1931); the Appendix quotes passages from their published works: ''Scenario of the [[The Photo-Drama of Creation]]'', ''[[The Harp of God]]'', ''[[Studies in the Scriptures]]''  Voll. I, II, IV-VII and ''[[A Desirable Government]]''.

Most names and places in the book were changed. In ''Angels and Women'', Seola and Lucifer are named ''Aloma'' and ''Satanas'', and the city of Sippara is called ''Balonia''.

The ''Golden Age Journal'' (now ''[[Awake magazine]]'')  of July 30, 1924, p.&nbsp;702, recommended that its readers purchase a copy of ''Angels and Women'':

{{cquote|“ANGELS AND WOMEN” is the title of a book just off the press.  It is a reproduction and revision of the novel, “Seola” which was written in 1878, and which deals with conditions prior to the flood. [[Pastor Russell]] read this book with keen interest, and requested some of his friends to read it because of its striking harmony with the Scriptural account of the sons of God described in the sixth chapter of Genesis. Those sons of God became evil, and debauched the human family prior to, and up to, the time of the great deluge. We call attention to this book because we believe it will be of interest to Bible Students, who are familiar with the machinations of the devil and the demons and the influence exercised by them prior to the flood and also now in this evil day. The book throws light on the subject and is believed, will aid those who carefully consider it to avoid the baneful effects of spiritism, now so prevalent in the world. The book is revised and published by a personal friend of Pastor Russell, and one who was close to him in his work. It is published by the A. B. Abac Company, New York city.}}

In 1977, in his ''Secrets of the Lost Races'', [[Rene Noorbergen]] claims that in 1950, a certain "Dr. Philip Gooch" gave information about an ancient diary written on a scroll which told of the events leading up to the deluge to Aaron J. Smith, leader of the Oriental Archaeological Research Expedition to the supposed location of [[Noah's Ark]] on top of [[Mount Ararat]]. Gooch supposedly wrote:

"The diary was written by Noah's daughter-in-law. The author of the Journal called herself Amoela and she claims to have been a student of [[Methuselah]]. He taught her about the history that transpired from the creation of [[Adam (Bible)|Adam]] to the deluge. Her youngest son [[Javan]] placed the completed scroll diary in his mother's tomb after she died in the 547th year of her life. The diary was placed in a crystal quartz case, with tempered gold hinges and clasps. The crystal case was then found by a high ranking Mason in the latter part of the nineteenth century. The original and the translation of the diary were stored in an unknown [[Masonic Lodge]]."

It is apparent that these details were derived by some means from either ''Seola'' or ''Angels and Women''.

An original first edition of Seola has sold for more than $3000 US Dollars on ebay.com. A first edition version of the 1924 Angels and Women has sold for more than $400 US Dollars on the addall.com book search website.

==Notes==
<references/>

==External links==
*[https://archive.org/stream/seolaxxx00smitiala/seolaxxx00smitiala_djvu.txt Full text of ''Seola''] (uncorrected)
*[https://books.google.com/books?id=VXhMAAAAMAAJ&pg=PA574&dq=Seola Good Company: Sunday Afternoon -a book review (on page 574) published in 1878]

[[Category:1878 novels]]
[[Category:19th-century American novels]]