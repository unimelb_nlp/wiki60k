{{Use dmy dates|date=October 2012}}
{{Infobox Australian place | type = suburb
| name     = Happy Valley
| city     = Adelaide
| state    = SA
| image    = Windebanks rd, hv and abpk.jpg
| caption  = Western end of Windebanks Drive
| lga      = City of Onkaparinga
| postcode = 5159
| pop      = 11,332
| pop_year  = {{CensusAU|2011}}
| pop_footnotes = <ref name="census11">{{Census 2011 AUS|id=SSC40273|name=Happy Valley (State Suburb) |accessdate=24 October 2013|quick=on}}</ref>
| area     = 
| stategov = [[electoral district of Fisher|Fisher]]
| fedgov   = [[Division of Kingston|Kingston]]
| near-nw  = [[O'Halloran Hill, South Australia|O'Halloran Hill]]
| near-n   = [[Flagstaff Hill, South Australia|Flagstaff Hill]]
| near-ne  = [[Aberfoyle Park, South Australia|Aberfoyle Park]]
| near-w   = [[Reynella East, South Australia|Reynella East]]
| near-e   = [[Chandlers Hill, South Australia|Chandlers Hill]]
| near-sw  = [[Woodcroft, South Australia|Woodcroft]]
| near-s   = [[Onkaparinga Hills, South Australia|Onkaparinga Hills]]
| near-se  = [[Clarendon, South Australia|Clarendon]]
| dist1    = 20
| location1= Adelaide
}}
'''Happy Valley''' is a metropolitan [[List of Adelaide suburbs|suburb]]  of [[Adelaide]], [[South Australia]]. It is located 20&nbsp;km south of the [[Central Business District]] of [[Adelaide]].

Within the suburb is the [[Happy Valley Reservoir]] accompanied by South Australia's largest water treatment plant, responsible for supplying water to much of the Adelaide metropolitan area. 

Although it is now encompassed by suburbs, it still retains a relatively semi-rural character due to retention of native flora and vegetation surrounding the Happy Valley Reservoir, as well as parklands and golf courses along its border.

==History==
[[Image:Happy valley dr, happy valley.jpg|thumb|Happy Valley Drive]]
{{See also|Old Reynella, South Australia#Hurtle Vale|l1=Hurtle Vale}}

In November 1844, Daniel George Brock recorded in his diary details of a journey south from Adelaide. On his third day he rode past [[Thomas Shuldham O'Halloran|Thomas O’Halloran’s]] farm, on the hill noting that the land had little running water and was suitable for wheat farming. Some few kilometres to the east he came to Happy Valley. There, he noted, were ‘several substantial stone buildings, among which is a neat little chapel’.

Originally known by the [[Kaurna people|Kaurna]] name of "Warekila" meaning 'place of changing winds', Happy Valley, a source suggested, was given its name by Edward Burgess, one of the first settlers in the area. Burgess, a staunch [[Methodism|Methodist]], arrived at [[Glenelg, South Australia|Holdfast Bay]] on 20 January 1837. At some time during the next two years, Burgess made his home at Happy Valley and began farming. He was not alone: the [[South Australian Company]] had also purchased significant amounts of land in the area and was offering it to the settlers.

By 1866, Happy Valley was described as ‘an agricultural settlement lying near Dashwood’s Gully, a good district road connecting the two places. It lies near the postal village of [[O'Halloran Hill, South Australia|O’Halloran Hill]]. There is a public pound and a Forester’s court in this place.’ By this time, too, wheat farming had been joined by wine grape growing. The settlement itself, although spread over a fair distance, incorporated an array of trades, a licensed school and chapel.

Within another twenty years, there had been a large increase in the area planted to vines and in the production of wine. The Douglas family, for example, were important grape growers in the area. They and others like them witnessed the formation of large wineries such as Horndale, Vale Royal and Mount Hurtle, that were funded by significant investors. Richard Cholmondeley, for example, helped fund the growth of Vale Royal and Horndale cellars.

Between 1892 and 1896, the construction of the Happy Valley Reservoir was undertaken, and although it was a source of local employment, it also inundated a large number of the area’s farms. Many of the local farming and grape growing families moved to other nearby landholdings and continued their work.

Following construction of the reservoir, the original Happy Valley township, school and cemetery were completely flooded requiring their relocation. The township was moved some 4&nbsp;km to the east while the cemetery, which is still in use today, was moved to the west and relocated alongside the base of the dam wall in what is now the suburb of [[O'Halloran Hill, South Australia|O'Halloran Hill]]. The school, originally located on Candy road, was relocated south to two acres of land on Red Hill Road (later renamed Education road) which was donated by local farmer Harry Mason. While some students attended O'Halloran Hill or Clarendon schools for the 18 months that the Happy Valley school was closed, some did not attend any school until it was re-opened on 26 September 1898. The school closed in December 1979 and re-opened on a new site on the other side of the road directly opposite.<ref>[http://www.happyvalley.sa.edu.au/history.htm Happy Valley school history]</ref> 

Myths abound that the spire of the original church steeple protrudes from the water when the Happy Valley reservoir's capacity is low, and that the town exists, submerged in the depths. This is incorrect, as all buildings were demolished and all salvageable materials — primarily fruit trees and vines — sold off before the area was flooded.<ref>'EO' Correspondence File GRG53/16 Box 283 File 3863/3921 Held by State Records of South Australia</ref> Staff responsible for the site at the time of the rumoured church sightings explain that a pump frame was then installed in the location in question which was visible at low water. At any rate, maps of pre-reservoir land parcels show that the location of the original church was not next to the 'scour' tower, but rather under the present dam wall, opposite the current church at the Candy's Road/Chandler's Hill Road junction.<ref>Mullins, Barbara and Carmichael, E., ''Happy Was Our Valley'', Douglas Book Committee, 1982.</ref> Between 2002 and 2004, the Reservoir was drained when it underwent a major renovation as part of A$22 million rehabilitation project aimed at enhancing the Reservoir to meet guidelines of best practice for dam management.<ref name="SA Water">{{cite web | accessdate =24 September 2005 | url =http://www.sawater.com.au/SAWater/Education/OurWaterSystems/Water+Storage+%28Reservoirs%29.htm | title = Water Storage (Reservoirs): Happy Valley Reservoir | year =2005 | publisher =[[SA Water]] }}</ref> With the lowering of the water level during renovations exposing the original Happy Valley township for the first time, archaeologists took the opportunity to excavate the site. Very little was found apart from scattered bricks and the foundations of several buildings of which only the Post Office was identified.<ref>Article in [[The Advertiser (Adelaide)|The Advertiser]] in 2002 (help required: date unknown at this time) As nothing of interest was found no research was published.</ref>

In 1959, the name Happy Valley was first applied to a subdivision of Section 501, hundred of Noarlunga. By the 1970s, the pressure of further subdivision was beginning to erode the area’s heritage. In 1983, the City of Happy Valley – formerly the District Council of Meadows – was created.

==Country Fire Service==
The Happy Valley CFS<ref>http://users.chariot.net.au/~cphappy/public_html/index.html</ref> established in 1939 is currently located on Glory Ct just off of Education Rd in Happy Valley. It is an entirely volunteer based brigade with a mixture of active fire fighters, cadets and auxiliary fire fighters and members. On average Happy Valley CFS will respond to over 250+ calls per year ranging from fixed alarms and structure fires to major bushfires and strike team deployments. The suburbs primarily covered by the brigade are Happy Valley, Woodcroft, Onkaparinga Hills, Aberfoyle Park and Flagstaff Hill. Also, Happy Valley CFS will respond into the Metropolitan Fire Service (MFS) areas to assist with any emergencies the MFS may need assistance with. Happy Valley CFS also trains with the MFS to cover the [[Flinders Medical Centre]] in [[Bedford Park, South Australia|Bedford Park]]. Both turn out to any incidents at the hospital with the first arriving service taking control of the incident. 

Three fire appliances are maintained by the brigade: one 2000 litre two wheel drive Urban Pumping truck used for urban emergencies (Pumper); one 3000 litre four wheel drive Rural truck that is fitted with a larger capacity pump, suitable for structure fires and incidents requiring large volumes of water (34P) which is supplied by the South Australian State Government; and a smaller 280 litre quick response vehicle that was purchased by the brigade(QRV).<ref>http://www.fire-brigade.asn.au/Station_Display.asp?Service_Code=SACFS&Station_Code=HPPY</ref>

==See also==
* [[List of Adelaide suburbs]]

==References==
{{reflist}}
# {{note|onk}}Happy Valley- European History and Heritage [http://www.onkaparingacity.com/history/viewsuburb.asp?content=happy]

{{Commons category}}
{{Coord|-35.083|138.567|format=dms|type:city_region:AU-SA|display=title}}
{{City of Onkaparinga suburbs}}

[[Category:Suburbs of Adelaide]]