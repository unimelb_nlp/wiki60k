{{Infobox company
|name = Wison Group (惠生集团)
|logo = [[File:Wison Group Logo.gif|200px]]
|type = [[Private company|Private]]/[[Public company|Public]] (Wison Engineering)
|foundation = {{Start date|df=yes|1997}}
|location = [[Shanghai, China]]
|area_served = Worldwide
| industry = [[Engineering]]<br>[[Energy]]
| products = Engineering, [[Project Management]], Industrial Gas, [[Onshore (hydrocarbons)|Onshore]] and [[Offshore drilling|Offshore]] Energy Projects
| num_employees = ~3,000
| subsid = Wison Engineering Ltd., Wison Offshore & Marine, Ltd., Horton Wison Deepwater Inc, and Genor BioPharma Co. Ltd.
| homepage = [http://www.wison.com]
| traded_as = [http://www.wison-engineering.com/en/ir/stock.php 2236] HKEx
| intl = yes
}}

'''Wison Group''' ([[Chinese language|Chinese]]: 惠生集团) is an engineering, procurement, and construction company, or EPC firm which focuses on the industries of [[energy]] and [[high-technology]].<ref name=BusinessWeekWison>{{Cite web|url=http://investing.businessweek.com/research/stocks/private/snapshot.asp?privcapId=61761621 |title=Wison Group Holding Limited: Private Company Information |publisher=Bloomberg Businessweek |accessdate=25 June 2012}}</ref> The company attracted significant international coverage in the first half of 2012 by winning a contract to “engineer, procure, construct, install, and commission” (EPCIC) the world’s first “floating [[LNG]] liquefaction, [[regasification]], and storage unit” off the coast of Colombia.<ref name="Offshoremag" /> Wison also formed a consortium with [[Hyundai]] Engineering & construction Co. to sign a contract with [[PDVSA]], Venezuela’s state-owned oil company, to expand PDVSA's Puerto la Cruz refinery.<ref name=ReutersVenezuela>{{Cite web|url=http://www.reuters.com/article/2012/06/27/venezuela-wison-refinery-idUSL2E8HRGYL20120627?feedType=RSS&feedName=industrialsSector|title=China's Wison wins Venezuela refinery upgrade project |publisher=Reuters |accessdate=15 July 2012}}</ref>

==History==
* '''1997''' Wison Chemical Engineering Co., Ltd. was established. The company focused on domestic petrochemical projects.<ref name=21stcentury/>
* '''2003''' Wison Group was established.<ref name=21stcentury/> Following this expansion, Wison Group moved into its current location at [[Zhangjiang Hi-tech Park]] in [[Pudong New Area]], [[Shanghai]].<ref name=ArchitectureStudio>{{Cite web|url= http://www.architecture-studio.fr/en/projects/shn4/wison_chemical_headquarters.html |title=Wison Chemical Headquarters |publisher=Architecture Studio |accessdate=25 July 2012}}</ref>
* '''2006''' Wison Offshore & Marine was founded.<ref name=WOMArticle>{{Cite web|url=http://www.china5e.com/show.php?contentid=192592 |title= Wison Marine Engineering Co., Ltd. announced that its Houston subsidiary, officially changed its name |publisher=China5e |accessdate=25 July 2012 |language=Chinese }}</ref>
* '''2008''' Wison signed an [[EPC (contract)|EPC]] contract with [[CNOOC]] [[Royal Dutch Shell|Shell]] for an Ethylene Cracking Furnace Debottlenecking Project, providing Wison with its first foreign-funded project.<ref name=21stcentury/> The company focused on producing CO and other gases.
* '''2011''' Wison Group Clean Energy Co., was established in [[Nanjing, China]]. Wison Engineering, together with [[Daelim]] of [[South Korea]], signed a MDI project contract with [[BASF]].
* '''2012''' Wison Engineering was officially listed on the Main Board of [[The Stock Exchange of Hong Kong Limited]] (“HKEx”) in December 28, 2012 and stock code is [http://www.wison-engineering.com/en/ir/stock.php 2236]<ref>http://www.wison.com/news_details.php?lang=en&cid=19&id=682</ref>

==Operations==
The Group is composed of four different business sectors: Engineering Services, Offshore & Marine, Clean Energy, and Emerging Business.<ref name=BusinessWeekWison/>

===Engineering Services===
Wison Engineering Ltd.<ref name=21stcentury>{{Cite web|url=http://www.21cbh.com/HTML/2012-7-10/4ONDE3XzQ3MTc4OQ.html |title=Private enterprises to break through Wison "alternative energy road
 |publisher=21st Century Aggregated Financial News Search |accessdate=25 July 2012 |language=Chinese }}</ref> provides engineering, construction and technical services for engineering projects in [[petrochemical]] industries.<ref name=BusinessWeekWison/> The Group works with foreign and domestic clients as a provider of design, procurement, construction management, and project planning consultation work. Partners and long-term clients include [[Royal Dutch Shell|Shell]]<ref name=shell>{{Cite web|url=http://www.icis-china.com/chemease/news/3244291,0,0,0,0.htm
|title= Shell and Wison will develop a new generation of gasification technology |publisher=ICIS Chemease |accessdate=25 July 2012 |language=Chinese }}</ref>
and [[BASF]].<ref name=BASFcontract>{{Cite web|url=http://www.sh.chinanews.com/PageUrl/20115191541257.html
|title= Wison Engineering and BASF signed a joint contract for 400,000 tons of MDI integrated project|publisher=Shanghai China News |accessdate=25 July 2012 |language=Chinese }}</ref>

As the largest private sector chemical [[engineering, procurement and construction]] management ("EPC") service provider in China in terms of revenue for 2011, as estimated by CMAI (Shanghai) Limited (CMAI), an independent industry consultant. The company specializes in the provision of construction and technical services for engineering installations in the [[petrochemical]], [[coal-to-chemical]] and [[refining]] industries. From project planning and consultation to design, procurement and construction phase project management, as well as start-up and operational services, the company provides solutions to domestic and foreign clients. Aside from the provision of EPC service, the Company manufactures integrated piping systems comprising heat-resistant alloy tubes and fittings through its wholly owned subsidiary, Wison (Yangzhou) Chemical Machinery Co., Ltd.

Recognized as a “High and New Technology Enterprise” by the government of China, Wison Engineering is committed to R&D and technology innovation in the areas of [[ethylene]] process integration technologies, coal-to-chemical technologies, [[coal pollution mitigation]] utilization technologies and energy saving technologies.  The Company has developed various proprietary technologies, including HS-I, HS-II and HS-III cracking furnace proprietary technologies, certain MTO light olefins separation technologies and WMTO process technologies.<ref>http://www.wison.com/business_products.php?lang=en&cid=24</ref>

===Offshore and Marine===
Wison Offshore & Marine<ref name=Offshoremag>{{Cite web|url=http://www.offshore-mag.com/articles/2012/05/wison-gets-epcic-contract.html |title=Wison Gets EPCIC Contract |publisher=Offshore Magazine |accessdate=25 June 2012}}</ref> is a division of the Wison Group in the upstream [[oil and gas]] industry.<ref name=WOMArticle/> This subsidiary focuses on design, project management, construction and operations of offshore oil projects. These include fixed [[Oil platform|platform]]s and [[modules]], offshore lifting equipment [[mooring (oceanography)|mooring]] systems, and other types of oil and gas developments.<ref name=OilVoiceCX15>{{Cite web|url=http://www.oilvoice.com/n/BPZ_Energy_provides_Block_Z1_oil_production_volumes_and_Corvina_Field_update/5b8abed45585.aspx |title=BPZ Energy provides Block Z-1 oil production volumes and Corvina Field update |publisher=OilVoice |accessdate=25 July 2012}}</ref>
Wison Offshore & Marine operates two fabrication yards that are dedicated to the construction of offshore oil and gas facilities.<ref name="Offshoremag" />

===Clean Energy===
Wison (Nanjing) Clean Energy Co., Ltd. is a company within the Wison Group that utilizes coal as [[feedstock]] and uses production technologies to provide customers with a supply of chemicals, including [[hydrogen]], [[carbon monoxide]], [[sulfur]], [[methanol]] and butyl-octyl alcohol in an eco-friendly manner.<ref name=shell/> Wison Clean energy holds long-term contracts will domestic and international chemical corporations, including [[Celanese]], [[BASF]]-YPC, DMAC, and [[China Bluestar|Bluestar]].<ref name=BASFcontract/>

===Emerging Business===
In 2007, Gener BioPharma Co., Ltd., a company with a focus on creating novel human [[vaccines]] through [[antibody]] research, development, manufacturing, and commercialization, was introduced under the Wison Group name.<ref name=BusinessWeekGenor>{{Cite web|url=http://investing.businessweek.com/research/stocks/private/snapshot.asp?privcapId=108757359 |title=Company Overview of Genor BioPharma Co., Ltd |publisher=Bloomberg Business Week |accessdate=16 July 2012}}</ref>

==Facilities==
Wison Group Headquarters are in Shanghai, China, at the [[Zhangjiang Hi-Tech Park]]. The building was designed by [[Architecture-Studio]], an award-winning French Architecture firm based in [[Paris, France]].<ref name=ArchitectureStudio/> The firm also has a shipbuilding yard in [[Nantong]], [[China]],  on the banks of the [[Yangtze River]] and is building a second fabrication facility in [[Zhoushan]], [[China]], that will be focused on the construction of onshore and offshore modules, large floating production hulls, topsides and jackets.

==Corporate social responsibility==
In 2011, the Wison Group introduced the “Wison Shinai Prize in Literature.”<ref name=CNTVLiterature>{{Cite web|url=http://news.cntv.cn/20110425/106839.shtml |title=Xinghua set up the Wison Shinai Prize in Literature |publisher=CNTV News |accessdate=15 July 2012|language=Chinese}}</ref> The prize was introduced to encourage and promote innovation in Chinese schools. The first place prize is 100,000 yuan.

In 2005, Wison Group founded the Wison Art center at their headquarters in Shanghai to support local and international art.<ref name=CNTVLiterature/> The center has shown works from famous painters including Chen Danqung and Chinese contemporary sculptor [[Wu Weishan]].

==Major projects==

In March, 2011, Wison Engineering, a subsidiary of Wison Group, finished construction on the world's largest single production capacity of an [[ethylene]] [[steam cracking|cracking]] furnace.<ref name=Ethylenecracker>{{Cite web|url= http://www.icis-china.com/chemease/news/3265486,0,0,0,0.htm |title= The world's largest ethylene cracker capacity successfully feeding |publisher=ICIS Chemease |accessdate=25 July 2012 |language=Chinese }}</ref> The plant, with a production capacity is up to 192,000 tons/year, was handed over to [[BASF]]-YPC. The old record of 150,000 tons/year was also held by a cracking plant that Wison Group constructed.<ref name=Ethylenecracker/>

In June, 2012, Wison Offshore & Marine, a subsidiary to Wison Group, was awarded the contract to build the 'world's first floating LNG Liquefaction, Regasification, and Storage unit.'<ref name="Offshoremag" /> Referred to as a FLRSU, the production unit will be delivered to [[Exmar]] under a contract with [[Pacific Rubiales Energy Corp.]] to start operation in Q4 2014 off the coast of [[Colombia]]. It will be a non-propelled barge, and will have a capacity of converting 69.5 MMcf/d of natural gas into LNG.<ref name="Offshoremag" /> It will be built in the Wison Offshore & Marine fabrication facility in Nantong, China. Additional assistance will be provided by the [[Houston, Texas]] branch of Wison Offshore & Marine.<ref name=BusinessWeekHorton>{{Cite web|url= http://investing.businessweek.com/research/stocks/private/snapshot.asp?privcapId=128041970
|title=Horton Wison Deepwater, Inc: Private Company Information |publisher=Bloomberg Businessweek |accessdate=25 June 2012}}</ref> [[Black & Veatch]] will provide engineering and procurement of the topside liquefaction equipment.<ref name=TankerOperatorExmar>{{Cite web|url=http://www.tankeroperator.com/news/todisplaynews.asp?NewsID=3677 |title=Exmar turns the corner |publisher=Tanker Operator |accessdate=25 July 2012}}</ref>

==See also==
{{portal|Companies}}
* [[Zhangjiang Hi-tech Park]]
* [[List of Chinese companies]]
* [[Architecture-Studio]]
* [[Shanghai]]

==References==
{{Reflist|2}}

==External links==
* [http://www.wison.com/ Official website]

[[Category:Companies established in 1997]]
[[Category:Energy engineering and contractor companies]]
[[Category:Companies based in Shanghai]]