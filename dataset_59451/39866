{{Use British English|date=August 2016}}
{{Use dmy dates|date=August 2016}}
{{Infobox military person
|name= Thomas Welch Horton
|birth_date={{birth date and age|1919|12|29|df=y}}
|death_date=
|birth_place= [[Masterton, New Zealand]]
|death_place=
|image=Wing Commander Tom Horton in 1945.jpg
|caption=Wing Commander Thomas W. Horton in 1945
|nickname= "Tom"
|allegiance=New Zealand (1939–47) <br/> United Kingdom (1948–66)
|serviceyears= 1939–1966 (27 years)
|rank=[[Wing commander (rank)|Wing Commander]]
|branch= {{plainlist|
* Royal NZ Air Force (1939–47)
* Royal Air Force (1948–66)
}}
|servicenumber = {{plainlist|
* 39920 (RNZAF)
* 59071 (RAF)
}}
|current position=
|commands= {{plainlist|
* [[No. 105 Squadron RAF]]
* [[No. 203 Squadron RAF]]
}}
|unit=
|battles={{plainlist|
* [[World War II]]
* [[Cold War]]
}}
|awards= {{plainlist|
* [[Distinguished Service Order]]
* [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross & Bar]]
}}
|relations=
|laterwork=
}}
'''Thomas Welch Horton''' [[Distinguished Service Order|DSO]], [[Distinguished Flying Cross (United Kingdom)|DFC and bar]] (born 29 December 1919), is a retired [[Royal New Zealand Air Force|Royal New Zealand Air Force (RNZAF)]] officer, pilot, and combat veteran who served with the [[Royal Air Force]] (RAF) in a number of significant engagements during the [[Second World War]]. He was a member of [[No. 88 Squadron RAF]] and flew anti-ship missions in the [[Bristol Blenheim]] and [[Douglas A-20 Havoc|Douglas Boston]]. Horton also served with and later commanded [[No. 105 Squadron RAF]] flying the [[de Havilland Mosquito]] in the [[Pathfinder Force]] (PFF) that marked targets for destruction by following [[bomber stream|groups of heavy bombers]].

After the war, Horton was commissioned with the Royal Air Force and commanded [[No. 203 Squadron RAF]] flying [[maritime patrol]] missions in the [[Lockheed P-2 Neptune|Neptune MR.1]]. He served on the [[United Kingdom]]'s [[Air Ministry]] staff and as a [[liaison officer]] in the [[The Pentagon|Pentagon]] to the [[North Atlantic Treaty Organization]] (NATO). Horton retired with the rank of [[wing commander (rank)|wing commander]] in 1966. 

==Early years==
Horton was born on 29 December 1919 in [[Masterton, New Zealand]], the son of T. H. Horton.{{r|Evening Post 1944}} He grew up in Masterton and attended [[Wairarapa College|Wairarapa High School]]. Horton learned to fly in a [[de Havilland DH.60 Moth|de Havilland Gipsy Moth]] [[biplane]] at the Wairarapa & Ruahine [[Aero Club]] where he was selected in July 1937 for training as part of the civil reserve of pilots.{{r|Evening Post 1937}} He worked in a law office before joining the Royal New Zealand Air Force on 26 October 1939.{{r|"Thompson 1956"}}{{r|Hanson 2001}}

==Air Force career==
<!-- [[File:Douglas Boston III 88 Sqn RAF at Attlebridge c1941.jpg|thumb|No. 88 Squadron [[Douglas DB-7|Douglas Boston IIIs]] at [[RAF Attlebridge]], 1941-1942
]]-->
===No. 88 Squadron===
Horton received additional flight training at [[Blenheim, New Zealand]] in the [[Vickers Vildebeest]] then headed to England at the end of April 1940 aboard the SS ''Mataroa''. At [[RAF Benson]], he trained in the [[Fairey Battle]] and was assigned to No. 88 Squadron RAF where he flew anti-ship patrols from [[RAF Sydenham]] in [[Northern Ireland]]. The squadron moved to [[RAF Swanton Morley]] in July 1941 where Horton transitioned to the Bristol Blenheim and flew more anti-ship patrols.{{r|Homewood 2012}}

{{Quote box 
   |quoted = yes
   |salign = left
   |quote = '''The devotion of the attacks on Rotterdam ... are beyond all praise.'''
   |author = [[Winston Churchill]]
   |source =''Message to the surviving aircrews''{{r|Pigott 2005}}
   |align=left
   |width=28%
}}
In 1942, [[Flight Lieutenant]] Horton was awarded the Distinguished Flying Cross for his skill and bravery on anti-ship missions including the hazardous low-level attack on shipping at [[Allied bombing of Rotterdam|Rotterdam]] on 28 August 1941.{{r|Evening Post 1942}}<ref name=AFL>{{London Gazette |issue= 35654 |supp=y |startpage= 3410 |date=4 August 1942|accessdate=5 March 2017}}</ref> On at least three missions, he successfully returned to base after one of his aircraft's engines had been disabled by [[Anti-aircraft warfare|anti-aircraft]] fire.{{r|"Hanson 2001"}}

Horton transitioned to the Douglas Boston and flew more anti-ship strikes from [[RAF Attlebridge]] northwest of [[Norwich]], [[Norfolk]], England.{{r|"Hanson 2001"}} He participated in a number of [[Circus offensive|Circus missions]] in which RAF bombers, escorted by friendly fighters, were used to draw out Luftwaffe fighters to their destruction. After completing his first combat tour, Horton spent a year as an instructor teaching [[Instrument flight rules|instrument  flying]].{{r|Homewood 2012}}

===Pathfinder Force===
[[File:De Havilland DH-98 Mosquito ExCC.jpg|right|thumb|Mosquito B Mk IV [[United Kingdom military aircraft serials|serial]] ''DK338'' before delivery to [[No. 105 Squadron RAF|105 Squadron]]]]
In July 1943, Horton was assigned to No. 105 Squadron RAF where he flew the de Havilland Mosquito light bomber from [[RAF Marham]] in Norfolk as part of the Pathfinder Force (PFF).{{r|Homewood 2012}} The Pathfinders specialized in locating and marking targets with [[flare]]s thereby improving the accuracy of the following main bomber force. No. 105 Squadron was part of the [[No. 8 Group RAF|No. 8 (Pathfinder Force) Group]]. For Horton, this meant a change from low-level daylight to high altitude nighttime missions.{{r|No 8 Group}}

The squadron utilized precision navigation aids such as the [[Oboe (navigation)|"Oboe"]] system that allowed the Pathfinders to accurately mark targets despite the industrial haze and cloud cover that obscured the area by night. Horton also dropped bombs, including the {{convert|4000|lb|adj=on}} [[Blockbuster bomb|"cookie"]], from his Mosquito. He participated in the [[Battle of the Ruhr]] in 1943 and protected the [[Normandy landings]] in 1944.{{r|Homewood 2012}} In 1944, [[Squadron Leader]] Horton was awarded a citation [[Medal bar|bar]] to his Distinguished Flying Cross for demonstrating "great courage and determination" on his missions with No. 105 Squadron.{{r|Flight Global 1944}} In June of the following year, he took command of the squadron.{{r|Barrass 105 Sqdn}} On 21 September 1945, [[Wing Commander (rank)|Wing Commander]] Horton received the Distinguished Service Order.{{r|AWC_DSO}} The citation recognized his "sound judgement and fine leadership as a flight commander".{{r|Hanson 2001}} ''FlightGlobal'' included Horton in their 1945 photo presentation of Pathfinder leaders.{{r|"Flight Global 1945"}}

Horton completed his World War II service with 111 sorties, 84 of them with the Pathfinders, and returned to New Zealand in 1946. He separated from the RNZAF on 10 April 1947.{{r|"Hanson 2001"}}

===RAF career===
[[File:No. 203 Squadron RAF June 1954.png|thumb|left|No 203 Squadron RAF in 1954]]
The United Kingdom's Air Ministry offered Horton an appointment to a permanent [[Commission (document)|commission]] with seniority for his wartime service if he would accept a position with the Royal Air Force.<ref name=FL_SL>{{London Gazette |issue= 38169 |supp=y |startpage= 133 |date=6 January 1948|accessdate=5 March 2017}}</ref> Horton left New Zealand and returned to England in late 1947 where he began his RAF service on 1 January 1948.{{r|Hanson 2001}} After several staff appointments, including the Air Ministry in London and [[RAF Coastal Command]] headquarters, Horton took command of No. 203 squadron RAF from December 1952 to January 1955.{{r|Barrass 2016}} The squadron relocated in late 1952 to [[RAF Topcliffe]] and re-equipped with the Neptune MR.1 to perform North Atlantic Ocean maritime and anti-submarine patrols during the [[Cold War]] with the [[Eastern Bloc]] (the [[Soviet Union]] and its [[satellite state]]s).{{r|"No 203 Squadron"}}

In 1955, Horton returned to the Air Ministry and served with the department of the Chief of the Air Staff.<ref name=FG1955-1861>
{{cite web |title=Royal Air Force Appointments |url=https://www.flightglobal.com/pdfarchive/view/1955/1955%20-%201861.html |date=30 December 1955 |website=Flightglobal/Archive |publisher=Reed Business Information |location=London, England |p=984 |accessdate=11 March 2017}}</ref> Horton was promoted to RAF wing commander in 1956<ref name=FG1956-0096>
{{cite web |title=Squadron Leader to Wing Commander |url=https://www.flightglobal.com/FlightPDFArchive/1956/1956%20-%200096.PDF |date=20 January 1956 |website=Flightglobal/Archive |publisher=Reed Business Information |location=London, England |p=96 |accessdate=11 March 2017}}</ref> and in 1964 was assigned to the staff of the North Atlantic Treaty Organization (NATO) Military Commitee at the Pentagon in the United States.{{r|Nakayama 2013}} From 1959 to 1960, Horton served in a dual capacity as senior air staff officer at [[RAF Gibraltar]] and British [[Air attaché|air attaché]] to [[Rabat, Morocco]].{{r|Steinberg 1961}} Horton retired from the RAF with the rank of wing commander on 29 December 1966 with a total of 27 years of military service.<ref name=WC>{{London Gazette |issue= 44231 |supp=y |startpage= 820 |date=20 January 1967|accessdate=5 March 2017}}</ref>

==Honours==
[[File:Thomas Welch Horton DSO Certificate 1945.jpg|thumb|right|150px|DSO certificate presented by George VI]]
Horton was awarded the Distinguished Flying Cross in 1942,<ref name=AFL/> and a citation bar to his DFC in 1944.<ref name=DFC_bar>{{London Gazette |issue= 36656 |supp=y |startpage= 3774 |date=15 August 1944 |accessdate=5 March 2017}}</ref> In 1945, he was made a companion of the Distinguished Service Order.<ref name=AWC_DSO>{{London Gazette |issue=37277 |supp=y |startpage=4706 |date=21 September 1945 |accessdate=5 March 2017}}</ref> [[King George VI]] formally presented Horton with this honour at an [[investiture]] ceremony at the [[Court of St James's]] on 21 September 1945.<ref name=Warrant>{{cite |title=T. W. Horton Distinguished Service Order Warrant of Appointment |date=21 September 1945 |publisher=Government of the United Kingdom |location=London, England}}</ref> For his service, Horton received the following [[campaign medal|campaign]] and commemorative medals: [[1939–1945 Star]], [[Air Crew Europe Star]], [[Defence Medal (United Kingdom)|Defence Medal]], [[War Medal 1939–1945]], [[Queen Elizabeth II Coronation Medal]], and the [[New Zealand War Service Medal]].{{r|Homewood 2012}}

In 2013, Horton was recognized for his contributions by [[Mike Moore (New Zealand politician)|Mike Moore]], New Zealand's ambassador to the United States, during the 99th anniversary of the [[Australia and New Zealand Army Corps]] [[Anzac Day|(ANZAC) Day]].{{r|Nakayama 2013}} ANZAC Day is a national day of remembrance in Australian and New Zealand that broadly commemorates all Australians and New Zealanders "who served and died in all wars, conflicts, and peacekeeping operations" and "the contribution and suffering of all those who have served".<ref>{{cite web |title=ANZAC Day |url=https://www.awm.gov.au/commemoration/anzac/|publisher=Australian War Memorial |accessdate=11 March 2017}}</ref><ref>{{cite web |title=Anzac Day Today |url=http://www.anzac.govt.nz/today/index.html|work=Anzac.govt.nz |publisher=New Zealand Ministry for Culture and Heritage |accessdate=11 March 2017}}</ref> Horton met with Mr. & Mrs. Moore again in 2014 at the 100th anniversary of ANZAC Day.{{r|Lawn 2014}}

==Personal life==
[[File:Ken Chilstrom and Tom Horton 2014.jpg|left|thumb|WWII aviators [[Kenneth O. Chilstrom|Ken Chilstrom]] and Tom  Horton at an [[Old Bold Pilots Association|OBPA]] luncheon in 2014]]
Horton married in December 1943 and has one daughter, Gail, and one son, Peter. His wife of 68 years, Beris, died in October 2011.{{r|Homewood 2012}} Horton remains interested in aviation and attends luncheons with fellow pilots. During a 2012 interview, he expressed a desire to visit a restored de Havilland Mosquito at the nearby [[Military Aviation Museum]], but age has made travel increasingly difficult for him.{{r|Homewood 2012}} {{As of|2014}}, Horton resides in Virginia in the United States.{{r|Lawn 2014}}
{{clear}}

== See also ==
{{Portal|Biography|World War II}}
*[[Hughie Edwards]] - Fellow commander and pilot with No. 105 Squadron RAF
*[[John Wooldridge]] - Fellow commander and pilot with No. 105 Squadron RAF
*[[List of New Zealand military personnel]]
*[[RAF Bomber Command aircrew of World War II]]
{{Clear}}

==References==
{{Reflist|30em|refs=
<ref name="No 203 Squadron">{{cite web |title=203(R) Squadron |url=http://www.raf.mod.uk/organisation/203Squadron.cfm |publisher=Royal Air Force |location=Whitehall, London |accessdate=12 March 2017}}</ref>

<ref name="Barrass 2016"> {{cite web |last=Barrass |first=M.B. |title=Squadron Commanding Officers, No 203 Squadron |url=http://www.rafweb.org/Squadrons/COs/OCs_201-220.htm#3 |date=4 November 2016 |work=Air of Authority - A History of RAF Organisation |accessdate=12 March 2017}}</ref>

<ref name="Barrass 105 Sqdn"> {{cite web |last=Barrass |first=M. B.  |title=105 Squadron |url=http://www.rafweb.org/Squadrons/COs/OCs_101-120.htm#5 |date=17 February 2017 |work=Air of Authority - A History of RAF Organisation |accessdate=25 March 2017}}</ref>

<ref name="Evening Post 1937">{{cite newspaper |last= |first= |title=Reserve of Pilots |url=http://paperspast.natlib.govt.nz/newspapers/EP19370722.2.118 |volume=CXXIV |p=11 |issue=19 |date=22 July 1937 |publisher=The Evening Post |location=Wellington, New Zealand |accessdate=9 March 2017}}</ref>

<ref name="Evening Post 1942">{{cite newspaper |last= |first= |title=Air Force Honours |url=http://paperspast.natlib.govt.nz/newspapers/EP19420806.2.67 |volume=CXXXIV |p=5 |issue=19 |date=6 August 1944 |publisher=The Evening Post |location=Wellington, New Zealand |accessdate=9 March 2017}}</ref>

<ref name="Evening Post 1944">{{cite newspaper |last= |first= |title=Twelve Awards |url=http://paperspast.natlib.govt.nz/newspapers/EP19440816.2.73 |volume=CXXXVIII |p=6 |issue=40 |date=16 August 1944 |publisher=The Evening Post |location=Wellington, New Zealand |accessdate=9 March 2017}}</ref>

<ref name="Flight Global 1944"> {{cite web |last= |first= |url=https://www.flightglobal.com/FlightPDFArchive/1944/1944%20-%202065.PDF |title=Bar to Distinguished Flying Cross |p=381 |date=5 October 1944 |website=Flightglobal/Archive |publisher=Reed Business Information |location=London, England |access-date= 8 March 2017}}</ref>

<ref name="Hanson 2001">{{cite book |last=Hanson |first=Colin Morris |title=By Such Deeds: Honours and Awards in the Royal New Zealand Air Force, 1923-1999 |date=2001 |publisher=Volplane Press |location=Christchurch, New Zealand |pp=259–60 |isbn=9780473073015}}</ref>

<ref name="Homewood 2012">{{cite web |last=Homewood |first=Dave |title=The Wings Over New Zealand Show Aviation Podcast |url=http://www.cambridgeairforce.org.nz/WONZShow/2012/08/episode-26-wc-tom-horton-dso-dfc-pff/ |work=Episode 26 – W/C Tom Horton DSO, DFC, (pff) |date=May 2012 |accessdate=8 March 2017}}</ref>

<ref name="Flight Global 1945">{{cite web |last= |first= |url=https://www.flightglobal.com/pdfarchive/view/1945/1945%20-%202217.html |title=Portraits from the Famous Pathfinder Force of Bomber Command |date=8 November 1945 |website=Flightglobal/Archive |publisher=Reed Business Information |location=London, England |access-date= 7 August 2016 |p=500}}</ref>

<ref name="Lawn 2014"> {{cite web |last=Lawn |first=Connie |title=Ambassador Moore meets Wing Commander Thomas Welch Horton |url=http://www.scoop.co.nz/stories/HL1404/S00233/ambassador-moore-meets-wing-commander-thomas-welch-horton.htm |date=30 April 2014 |accessdate=8 March 2017}}</ref>

<ref name="Thompson 1956">{{cite web |last=Thompson |first=H. L. |url=http://nzetc.victoria.ac.nz/tm/scholarly/tei-WH2-2RAF-c9.html |title=New Zealanders With The Royal Air Force (Vol. II) |date=1956 |website=New Zealand Electronic Text Collection |publisher=Historical Publications Branch |location=Wellington, New Zealand |p=252 |access-date= 7 August 2016}}</ref>

<ref name="Nakayama 2013">{{cite web |last=Nakayama |first=Misato |url=https://www.washdiplomat.com/PouchArticle/cms/component/content/article/78-october-19-2013/198-051514anzac4.html |title=Australia, New Zealand Mark 99th ANZAC Day Observance |date=19 October 2013 |publisher=The Washington Diplomat |location=Washington, D.C. |p= |accessdate=10 March 2017}}</ref>

<!-- May use this reference at some point
<ref name="Rawlings 1984">{{cite book |ref=harv |title=History of the Royal Air Force |first=John Dunstan Richard |last=Rawlings |location=New York |publisher=Crescent Books |year=1984 |p=219 |isbn=978-0-517-46249-2}}</ref>
-->

<ref name="Pigott 2005">{{cite book |ref=harv |title=On Canadian Wings: A Century of Flight |first=Peter |last=Pigott  |location=Toronto, Canada |publisher=Dundurn Press|year=2005 |p=99 |isbn=978-1550025491}}</ref>

<ref name="No 8 Group">{{cite web |last= |first= |title=Bomber Command No.8 (Pathfinder Force) Group |url=http://www.raf.mod.uk/history/bombercommandno8group.cfm |date= |publisher=Royal Air Force |location=Whitehall, London |accessdate=13 March 2017}}</ref>

<ref name="Steinberg 1961">{{cite book |ref=harv |title=The Statesman's Year-Book: Statistical and Historical Annual of the States of the World for the Year 1961 |editor-last=Steinberg |editor-first=S.H. |first=|last= |location=Toronto, Canada |publisher=Palgrave Macmillan |year=1961 |p=1248 |isbn=978-0-230-27090-9}}</ref>

}}

==External links==
* {{cite web |last= |first= |title=ASN Wikibase Occurrence # 70258 |url=http://aviation-safety.net/wikibase/wiki.php?id=70258 |date=16 September 2016 |publisher=Flight Safety Foundation |location=Alexandria, Virginia |work=Aviation Safety Network |accessdate=13 March 2017}}
* {{cite web |last=Dawson |first=Andy |title=105 Squadron |url=http://www.mossie.org/squadrons/105_squadron.htm |date=6 October 2016 |accessdate=10 March 2017 |work=The Mosquito Page}}

{{DEFAULTSORT:Horton, Thomas Welch}}
[[Category:Companions of the Distinguished Service Order]]
[[Category:New Zealand World War II pilots]]
[[Category:People from Masterton]]
[[Category:Recipients of the Distinguished Flying Cross and Bar (United Kingdom)]]
[[Category:Royal Air Force officers]]
[[Category:Royal Air Force pilots of World War II]]
[[Category:Royal New Zealand Air Force personnel]]
[[Category:1919 births]]
[[Category:Living people]]