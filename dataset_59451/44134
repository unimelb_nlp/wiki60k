{{Redirect|Berlin-Schönefeld|the suburban municipality|Schönefeld}}
{{Use dmy dates|date=May 2014}}
{{Infobox airport
| name         = Berlin Schönefeld Airport
| nativename   = ''<small>Flughafen Berlin-Schönefeld</small>''
| image        = SXF Logo.svg
| image-width  = 250
| image2       = FLUGHAFEN SCHONEFELD BERLIN GERMANY JUNE 2013 (9025967808).jpg
| image2-width = 250
| IATA         = SXF
| ICAO         = EDDB, <s>ETBS</s>
| type         = Public
| owner        =
| operator     = [[Flughafen Berlin Brandenburg GmbH]]
| city-served  = [[Berlin]], [[Germany]]
| location     = [[Schönefeld]]
| focus_city   = 
* [[Condor Flugdienst|Condor]]
* [[easyJet]]
* [[Germania (airline)|Germania]]
*[[Ryanair]]
| elevation-f  = 157
| elevation-m  = 48
| coordinates  = {{coord|52|22|43|N|013|31|14|E|region:DE|display=inline,title}}
| website      = [http://www.berlin-airport.de/en/travellers-sxf/index.php berlin-airport.de]
| pushpin_map            = Germany Berlin
| pushpin_label_position =
| pushpin_label          = SXF
| pushpin_map_alt        =
| pushpin_mapsize        =
| pushpin_image          =
| pushpin_map_caption    = Location relative to Berlin
| metric-rwy   = y
| r1-number    = 07L/25R
| r1-length-f  = 11,881
| r1-length-m  = 3,600
| r1-surface   = [[Asphalt]]
| stat-year    = 2016
| stat1-header = Passengers
| stat1-data   = {{increase}} 11,652,922
| footnotes    = <small>Sources: German [[Aeronautical Information Publication|AIP]] at [[European Organisation for the Safety of Air Navigation|EUROCONTROL]]</small><ref name="AIP">{{cite web|url=http://www.berlin-airport.de/en/press/press-releases/2017/2017-01-12-Traffic-Report-December-2016/index.php |title=EAD Basic |publisher=Ead.eurocontrol.int |date=}}</ref>
}}

'''Berlin Schönefeld Airport''' (''{{Audio|Flughafen_Berlin-Schönefeld.ogg|Flughafen Berlin-Schönefeld}}'') {{Airport codes|SXF|EDDB}} is the secondary [[international airport]] of [[Berlin]], the capital of [[Germany]]. It is located {{convert|18|km|abbr=on}} southeast<ref name="AIP" /> of Berlin near the town of [[Schönefeld]] in the state of [[Brandenburg]] and borders Berlin's southern boundary. It is the smaller of the two [[List of airports in Berlin|airports in Berlin]], after [[Berlin Tegel Airport]], and is a base for [[Condor Flugdienst|Condor]], [[easyJet]] and [[Ryanair]].

Schönefeld Airport was the major civil airport of [[East Germany]] (GDR) and the only airport of formerly [[East Berlin]]. It is planned to incorporate part of Schönefeld's existing infrastructure into the new [[Berlin Brandenburg Airport]]<ref>{{cite web|url=http://www.berlin-airport.de/EN/BBI/FlughafenDerZukunft/Aus3Mach1/zukunftschoenefeld.html |title=The future lies in Schoenefeld |publisher=Berlin-airport.de |date=}}</ref> when it opens, which is scheduled sometime during 2018 as of January 2017.<ref>[http://www.sz-online.de/nachrichten/dann-halt-2018-3594163.html sz-online.de - "No new capital city airport in 2017"] (German) 21 January 2017</ref> It has been announced that Schönefeld's terminals are planned to be used until at least 2023 to handle low-cost carriers as part of the new airport.<ref name="newplans"/>

==History==
[[File:Bundesarchiv DH 2 Bild-F-04180, Berlin-Schönefeld, Flughafenbau, Il14.jpg|thumb|Construction of [[Interflug]]'s new maintenance hangar in 1961]]
[[File:Tupolev Tu-134A-3, CCCP-65783, Aeroflot.jpg|thumb|[[Eastern bloc]] airlines [[TAROM]], [[Aeroflot]] and [[Interflug]] in 1990]]

===First years and World War II===
{{Unreferencedsect|date=April 2017}}
On 15 October 1934 construction begun to build three 800m long runways to serve the [[Henschel & Son|Henschel]] aircraft plant in Schönefeld. By the end of the [[Second World War]], over 14,000 aircraft had been built. On 22 April 1945, the facilities were occupied by Soviet troops, and the plant was dismantled and demolished. By late 1947, the railway connection had been repaired and agricultural machinery was built and repaired on the site.

In 1946, the [[Soviet Air Forces]] moved from [[Johannisthal Air Field]] to Schönefeld, including the civil airline [[Aeroflot]]. In 1947, the [[Soviet Military Administration in Germany]] approved the construction of a civilian airport at the site.

A stipulation of the [[Four Power Agreement on Berlin|Four Power Agreement]] following World War II was a total ban on German carriers' participation in air transport to Berlin, where access was restricted to US, British, French and Soviet airlines. Since Schönefeld airport was located outside the city boundaries of Berlin, this restriction did not apply. Thus, aircraft of the East German flag carrier [[Interflug]] could use Schönefeld airport, while West German [[Lufthansa]] was denied access to Tegel or [[Berlin Tempelhof Airport|Tempelhof]] airports.

===Development after German reunification===
Berlin Schönefeld Airport has seen a major increase in passenger numbers over recent years, which was caused by the opening of bases for both [[easyJet]] and [[Germanwings]]. In 2008, the airport served 6.6 million passengers.

Following [[German reunification]] in 1990, operating three separate airports became increasingly cost prohibitive, leading the [[Abgeordnetenhaus von Berlin|Berlin legislature]] to pursue plans for a single airport that would be more efficient and would decrease the amount of [[aircraft noise]] from airports within the city. Therefore, it was decided to build [[Berlin Brandenburg Airport]] at the current site of Schönefeld Airport, originally scheduled to open in late 2012. For various reasons, mainly issues with the fire alarm/safety system, the opening has been postponed to 2018 or later.

The new airport will share only one runway with the existing one – the current runway will become the north runway of the new airport. Most of the old Schönefeld Airport, including the [[airport terminal|terminal]] and [[airport ramp|apron]] areas, will undergo complete [[Urban renewal|urban redevelopment]] following its closure. Part of the old apron area will be used by the future new passenger terminal of the German government used for state visits and other state flight operations.<ref>{{cite web|url=http://www.berliner-zeitung.de/hauptstadtflughafen/flughafen-berlin-brandenburg-regierungsterminal-in-der-warteschleife,11546166,23722270.html |title=Flughafen Berlin Brandenburg: Regierungsterminal in der Warteschleife &#124; Hauptstadtflughafen – Berliner Zeitung |language=de |publisher=Berliner-zeitung.de |date=}}</ref>

At the start of the winter season in 2012 [[Germanwings]] left Schönefeld for Berlin-Tegel to maintain closer operations within the [[Lufthansa Group]] there.<ref>{{cite web|url=http://www.spiegel.de/reise/aktuell/germanwings-zieht-von-flughafen-schoenefeld-nach-berlin-tegel-a-853854.html |title=Germanwings zieht nach Berlin-Tegel |publisher=Spiegel.de |date=4 September 2012}}</ref> However, to provide competition for Ryanair's new routes, Germanwings announced a return to Schönefeld in addition to their Tegel operations from October 2015.<ref name="4Ureturns">{{cite web|url=http://www.airliners.de/germanwings-konkurrenz-ryanair/35139|title=Germanwings stellt sich Konkurrenz durch Ryanair|work=airliners.de|accessdate=4 June 2015}}</ref>

[[Aer Lingus]] also announced it would switch airports within Berlin, from Schönefeld to Tegel, by March 2015.<ref>{{cite web|url=http://ch-aviation.com/portal/news/35424-aer-lingus-to-switch-berlin-flights-from-schnefeld-to-tegel|title=Aer Lingus to switch Berlin flights from Schönefeld to Tegel|work=ch-aviation|accessdate=4 June 2015}}</ref> Meanwhile, [[Ryanair]] announced the establishment of their sixth German base in Schönefeld by 27 October 2015 by deploying five aircraft to the airport and adding 16 new routes.<ref>{{cite web|url=http://www.airliners.de/ryanair-basis-berlin/35089|title=Ryanair will mit Basis in Berlin Fluggastzahlen deutlich steigern|work=airliners.de|accessdate=4 June 2015}}</ref>

On 2 May 2015, the first planes departing from the airport became the first commercial flights to use the southern runway of nearby [[Berlin Brandenburg Airport]], which temporarily became Schönefeld's only runway while its own, which will become the northern runway of the new airport, was renovated.<ref>{{cite web|url=http://www.aero.de/news-21622/BER-Erste-Flieger-starten-auf-suedlicher-Bahn.html|title=aero.de - Luftfahrt-Nachrichten und -Community|work=aero.de|accessdate=4 June 2015}}</ref>

In 2016 the Schönefeld Airport extension will be concluded. Terminal B will be extended by approx. 600 square metres and the baggage area is also to be enlarged by 40 per cent. In the form of Terminal D2, an entirely new arrival terminal is being built west of Terminal D. Spanning almost 3,800 square metres, this building will feature three baggage carousels and the coach parking area is also being relocated to a new area in P6<ref>{{cite web |url=http://www.berlin-airport.de/en/press/press-releases/2016/2016-04-15-construction-work-b96a/index.php |title=Construction work at Schönefeld Airport: Upgrading of federal highway B96a, car park P4 closed |publisher=Flughafen Berlin Brandenburg GmbH |date=15 April 2016 |accessdate =26 April 2016}}</ref>

The airport is still seeing exceptionally high growth of passenger numbers with Berlin's economic growth. As of November 2016, the airport operates near full capacity despite several additions to the infrastructure in recent years.<ref name="newplans"/>

==Terminals==
[[File:Flughafen Berlin-Schoenefeld Große Wartehalle.JPG|thumb|Terminal A main hall]]
[[File:17-03-14-Streik_Flugplatz_Schönefeld_SXF_RalfR-RR7_8140.jpg|thumb|Terminal D check-in area]]
Schönefeld Airport consists of the four terminals A, B, C and D.<ref>{{cite web|url=http://www.bvg.de/index.php/de/binaries/asset/download/475155/file/1-1 |title=Schönefeld Airport layout |date=}}</ref> These terminals are located next to each other but have separate landside areas; however they are connected through a joint airside concourse. Terminal C has no check-in facilities, it is used exclusively for passengers clearing security checks to enter the airside boarding gates. Due to a lack of space, there are not as many facilities as those at many other international airports. There are some shops, however, including duty-free, newsagents, a few restaurants such as [[Burger King]] and a single airline lounge.

===Terminals A and B===
The main building is the original part of the airport. It houses check-in for Terminals A and B. Terminal A features check-in counters A01–A18, with the largest user being [[Ryanair]] alongside several other airlines like [[Aeroflot]]. Terminal B, located in a side wing, was originally reserved for transit passengers to and from West Berlin who took advantage of cheaper air fares and package tours arranged by an East German travel agency. Nowadays, it is used exclusively by [[EasyJet]] with check-in counters B20–B29 and has been refurbished in recent years. The airside consists of three [[jet bridge]]s as well as several walk-boarding aircraft stands located at ''Pier 3a'', an extension that was opened in 2005.

===Terminal C===
Terminal C was originally built to accommodate flights to [[Israel]]. It was reconfigured in 2008 to handle sightseeing trips and flights in connection with special events.<ref>[http://www.air-service-berlin.de/index.php5?page=306-0-0-0-1-1-2&lang=2 Event and Show Terminal C] {{webarchive |url=https://web.archive.org/web/20110705112226/http://www.air-service-berlin.de/index.php5?page=306-0-0-0-1-1-2&lang=2 |date=5 July 2011 }}</ref> It was further reconfigured in 2015 to provide access to all terminal gates. To reduce congestion in other terminals, it now houses additional security checkpoints for passengers who have checked-in or only carry hand luggage.<ref>[http://www.berlin-airport.de/de/unternehmen/aktuelles/news/2015/2015-11-17-terminal-c/index.php]</ref>

===Terminal D===
Terminal D was opened in December 2005 due to rapidly growing passenger numbers. Being nearly identical to Terminal C at [[Berlin Tegel Airport]], it features check-in counters D40–D57, which are mainly used by [[Condor Flugdienst|Condor]], [[Germania (airline)|Germania]], and [[Norwegian Air Shuttle]]. It does not feature jet bridges but several walk-boarding stands. In November 2016, the new 4500sqm large arrivals area ''D2'' has been opened right next to Terminal D.<ref name="newplans">[http://www.airliners.de/am-flughafen-schoenefeld-ankunftshalle/40150 airliners.de - "New arrivals hall to be opened at Schönefeld Airport"] (German) 25 November 2016</ref>

==Airlines and destinations==
===Passenger===
The following airlines operate regular scheduled and charter flights at Berlin Schönefeld Airport:<ref>{{cite web|url=http://www.berlin-airport.de/de/reisende-sxf/reisebuchung/flugplansuche/index.php |title=Archived copy |accessdate=2015-03-29 |deadurl=yes |archiveurl=https://web.archive.org/web/20150317062902/http://www.berlin-airport.de:80/de/reisende-sxf/reisebuchung/flugplansuche/index.php |archivedate=17 March 2015 |df=dmy }}</ref>

<!--PLEASE DO NOT ADD OR REMOVE ROUTES WITHOUT GIVING A VALID SOURCE. EXACT DATES ARE MANDATORY FOR NEW ROUTES TO BE ADDED HERE. ALSO ADD INLINE CITATIONS IF POSSIBLE.-->
{{Airport-dest-list
|3rdcoltitle = Check-in
<!-- -->
|[[Aeroflot]] | [[Sheremetyevo International Airport|Moscow-Sheremetyevo]] | A
<!-- -->
|[[Aeroflot]] <br>{{nowrap|operated by [[Rossiya Airlines]]}} | [[Pulkovo Airport|St Petersburg]] | A
<!-- -->
|[[Air Algérie]] | '''Seasonal:''' [[Houari Boumediene Airport|Algiers]] | A
<!-- -->
|[[Air VIA]] | '''Seasonal charter:''' [[Burgas Airport|Burgas]], [[Varna Airport|Varna]] | D
<!-- -->
|[[Astra Airlines]] | '''Seasonal charter:''' [[Ben Gurion Airport|Tel Aviv-Ben Gurion]] | D
<!-- -->
|[[Azur Air (Germany)]] | '''Charter:''' [[Antalya Airport|Antalya]], [[Gran Canaria Airport|Gran Canaria]] (begins 1 May 2017),<ref>[http://www.anextour.de Anex Tour booking system] 15 December 2016</ref> [[Palma de Mallorca Airport|Palma de Mallorca]], [[Punta Cana International Airport|Punta Cana]] | A
<!-- -->
|[[Belavia]] | [[Minsk National Airport|Minsk]] | A
<!-- -->
|{{nowrap|[[Bulgarian Air Charter]]}} | '''Seasonal charter:''' [[Burgas Airport|Burgas]], [[Varna Airport|Varna]] | D
<!-- -->
|[[Condor Flugdienst|Condor]] | [[Agadir–Al Massira Airport|Agadir]], [[Fuerteventura Airport|Fuerteventura]], [[Gran Canaria International Airport|Gran Canaria]], [[Hurghada International Airport|Hurghada]], [[Tenerife South Airport|Tenerife-South]] <br>'''Seasonal:''' [[Antalya Airport|Antalya]], [[Dalaman Airport|Dalaman]], [[Heraklion International Airport|Heraklion]], [[Palma de Mallorca Airport|Palma de Mallorca]], [[Rhodes International Airport|Rhodes]] | D
<!-- -->
|[[easyJet]] | [[Amsterdam Airport Schiphol|Amsterdam]], [[Athens International Airport|Athens]], [[Barcelona El Prat Airport|Barcelona]], [[EuroAirport Basel-Mulhouse-Freiburg|Basel/Mulhouse]], [[Bordeaux–Mérignac Airport|Bordeaux]], [[Bristol Airport|Bristol]], [[Budapest Ferenc Liszt International Airport|Budapest]], [[Copenhagen Airport|Copenhagen]], [[Edinburgh Airport|Edinburgh]], [[Geneva International Airport|Geneva]], [[Glasgow Airport|Glasgow]], [[Larnaca Airport|Larnaca]], [[Lisbon Portela Airport|Lisbon]], [[Liverpool John Lennon Airport|Liverpool]], [[Gatwick Airport|London-Gatwick]], [[London Luton Airport|London-Luton]], [[Lyon-Saint Exupéry Airport|Lyon]], [[Málaga Airport|Málaga]], [[Manchester Airport|Manchester]], [[Malpensa Airport|Milan-Malpensa]], [[Marseille Provence Airport|Marseille]], [[Naples Airport|Naples]], [[Newcastle Airport|Newcastle]], [[Nice Côte d'Azur Airport|Nice]], [[Palma de Mallorca Airport|Palma de Mallorca]], [[Paris-Orly Airport|Paris-Orly]], [[Pisa International Airport|Pisa]], [[Pristina International Airport|Pristina]], [[Salzburg Airport|Salzburg]], [[Ben Gurion International Airport|Tel Aviv-Ben Gurion]], [[Tenerife South Airport|Tenerife-South]], [[Thessaloniki International Airport|Thessaloniki]], [[Venice Marco Polo Airport|Venice]], [[Vienna International Airport|Vienna]], [[Zürich Airport|Zürich]] <br>'''Seasonal:''' [[Bastia Airport|Bastia]] (begins 26 June 2017), [[Cagliari-Elmas Airport|Cagliari]], [[Catania-Fontanarossa Airport|Catania]], [[Corfu International Airport|Corfu]], [[Dubrovnik Airport|Dubrovnik]], [[Faro Airport|Faro]], [[Heraklion International Airport|Heraklion]], [[La Palma Airport|La Palma]], [[Olbia – Costa Smeralda Airport|Olbia]], [[Pula Airport|Pula]] (begins 27 June 2017),<ref>http://www.easyjet.com/en/</ref> [[Rhodes International Airport|Rhodes]], [[Split Airport|Split]], [[Toulouse–Blagnac Airport|Toulouse]], [[Varna Airport|Varna]] (begins 28 June 2017) | A, B
<!-- -->
|[[EgyptAir]] | [[Cairo International Airport|Cairo]] | A
<!-- -->
|[[Freebird Airlines]] | '''Charter:''' [[Antalya Airport|Antalya]] | A
<!-- -->
|[[Germania (airline)|Germania]]| [[Beirut–Rafic Hariri International Airport|Beirut]], [[Fuerteventura Airport|Fuerteventura]] (begins 21 October 2017),<ref name="routesonline.com">http://www.routesonline.com/news/38/airlineroute/271173/germania-schedules-new-berlin-routes-for-w17/</ref> [[Gran Canaria International Airport|Gran Canaria]] (begins 25 October 2017),<ref name="routesonline.com">http://www.routesonline.com/news/38/airlineroute/271173/germania-schedules-new-berlin-routes-for-w17/</ref> [[Paphos International Airport|Paphos]], [[Sharm El Sheikh International Airport|Sharm El Sheikh]] (begins 20 October 2017),<ref name="routesonline.com">http://www.routesonline.com/news/38/airlineroute/271173/germania-schedules-new-berlin-routes-for-w17/</ref> [[Tehran Imam Khomeini International Airport|Tehran-Imam Khomeini]] <br>'''Seasonal:''' [[Antalya Airport|Antalya]], [[Milas-Bodrum Airport|Bodrum]], [[Burgas Airport|Burgas]], [[Hurghada International Airport|Hurghada]], [[Ibiza Airport|Ibiza]], [[Rovaniemi Airport|Rovaniemi]], [[Tenerife South Airport|Tenerife-South]] | D
<!-- -->
|[[Norwegian Air Shuttle]] | [[Bergen Airport, Flesland|Bergen]], [[Copenhagen Airport|Copenhagen]], [[Oslo Airport, Gardermoen|Oslo-Gardermoen]], [[Stockholm-Arlanda Airport|Stockholm-Arlanda]] <br>'''Seasonal:''' [[Stavanger Airport, Sola|Stavanger]] | D
<!-- -->
|[[Norwegian Air Shuttle]] <br>{{nowrap|operated by [[Norwegian Air International]]}} | [[Gran Canaria Airport|Gran Canaria]], [[Tenerife South Airport|Tenerife-South]] | D
<!-- -->
|[[Jet2.com]] | [[Leeds Bradford International Airport|Leeds/Bradford]] | D
<!-- -->
|[[Pegasus Airlines]] | [[Sabiha Gökçen International Airport|Istanbul-Sabiha Gökçen]] <br>'''Seasonal:''' [[Antalya Airport|Antalya]]| A
<!-- -->
|[[Ryanair]] | [[Alicante Airport|Alicante]], [[Athens International Airport|Athens]], [[Barcelona-El Prat Airport|Barcelona]], [[Bari Karol Wojtyla Airport|Bari]], [[Belfast International Airport|Belfast-International]], [[Il Caravaggio International Airport|Bergamo]], [[Billund Airport|Billund]] (begins 29 October 2017),<ref name='''Airlineroute'''>{{cite web|url=http://www.routesonline.com/news/38/airlineroute/271685/ryanair-w17-new-routes-as-of-05mar17/|title=Ryanair W17 new routes as of 05MAR17|publisher='''Routesonline'''|accessdate=5 March 2017}}</ref> [[Bologna Guglielmo Marconi Airport|Bologna]], [[Bratislava Airport|Bratislava]], [[Brussels Airport|Brussels]], [[Henri Coandă International Airport|Bucharest]], [[Budapest Ferenc Liszt International Airport|Budapest]], [[Catania Airport|Catania]], [[Cologne Bonn Airport|Cologne/Bonn]], [[Dublin Airport|Dublin]], [[East Midlands Airport|East Midlands]], [[Fuerteventura Airport|Fuerteventura]], [[Glasgow Airport|Glasgow]], [[Gran Canaria Airport|Gran Canaria]], [[Kerry Airport|Kerry]] (begins 2 November 2017),<ref name='''Airlineroute'''>{{cite web|url=http://www.routesonline.com/news/38/airlineroute/271685/ryanair-w17-new-routes-as-of-05mar17/|title=Ryanair W17 new routes as of 05MAR17|publisher='''Routesonline'''|accessdate=5 March 2017}}</ref> [[John Paul II International Airport Kraków–Balice|Kraków]] (begins 29 October 2017),<ref>http://www.routesonline.com/news/38/airlineroute/271685/ryanair-w17-new-routes-as-of-05mar17/</ref> [[Lanzarote Airport|Lanzarote]], [[Lisbon Portela Airport|Lisbon]], [[London Stansted Airport|London-Stansted]], [[Lviv Danylo Halytskyi International Airport|Lviv]] (begins 8 September 2017), [[Madrid-Barajas Airport|Madrid]], [[Málaga Airport|Málaga]], [[Malta International Airport|Malta]], [[Manchester Airport|Manchester]], [[Nis Constantine the Great Airport|Niš]], [[Falcone–Borsellino Airport|Palermo]], [[Palma de Mallorca Airport|Palma de Mallorca]], [[Pisa International Airport|Pisa]], [[Podgorica Airport|Podgorica]], [[Francisco de Sá Carneiro Airport|Porto]], [[Riga International Airport|Riga]], [[Rome-Ciampino Airport|Rome-Ciampino]], [[Santander Airport|Santander]], [[Seville Airport|Seville]], [[Sofia Airport|Sofia]], [[Tenerife South Airport|Tenerife-South]], [[Thessaloniki Airport|Thessaloniki]], [[Timișoara Traian Vuia International Airport|Timișoara]], [[Toulouse–Blagnac Airport|Toulouse]], [[Treviso Airport|Treviso]], [[Valencia Airport|Valencia]], [[Verona Airport|Verona]], [[Vilnius International Airport|Vilnius]] <br>'''Seasonal:''' [[Ovda Airport|Eilat-Ovda]] (begins 30 October 2017), [[Rzeszow-Jasionka Airport|Rzeszów]], [[Shannon Airport|Shannon]], [[Zadar Airport|Zadar]] | D
<!-- -->
|[[Transavia]] | [[Munich Airport|Munich]] (ends 26 October 2017),<ref>http://www.aero.de/news-26054/Transavia-loest-Basis-in-Muenchen-auf.html</ref> [[Rotterdam The Hague Airport|Rotterdam]] | A
<!-- -->
|[[Transavia France]] | [[Nantes Atlantique Airport|Nantes]] | A
<!-- -->
|[[Tunisair]] | [[Djerba–Zarzis International Airport|Djerba]], [[Enfidha – Hammamet International Airport|Enfidha]] | A
<!-- -->
|[[Up (Airline)|UP]] <br>{{nowrap|operated by [[El Al]]}}<!--DO NOT CHANGE THIS FORMAT. IT IS NOT "EL AL OPERATED BY UP" AS UP IS THE BRAND, THE OPERATING CARRIER IS EL AL--> | [[Ben Gurion International Airport|Tel Aviv-Ben Gurion]] | D
<!-- -->
|[[Wizz Air]] | [[Chișinău International Airport|Chișinău]], [[Cluj International Airport|Cluj-Napoca]], [[David the Builder Kutaisi International Airport|Kutaisi]], [[Lviv Danylo Halytskyi International Airport|Lviv]] (begins 18 June 2017), [[Skopje "Alexander the Great" Airport|Skopje]], [[Tuzla International Airport|Tuzla]] | D
<!-- -->
|[[WOW air]] | [[Keflavík International Airport|Reykjavík-Keflavík]] | D
<!-- -->
}}

===Cargo===
{{Airport-dest-list
<!-- -->
|[[FedEx Feeder]] <br>{{nowrap|operated by [[ASL Airlines Ireland]]}}| [[Gdańsk Lech Wałęsa Airport|Gdańsk]], [[Paris-Charles de Gaulle Airport|Paris-Charles de Gaulle]]
<!-- -->
|[[West Air Sweden]] | [[Cologne Bonn Airport|Cologne/Bonn]]
<!-- -->
}}

==Other facilities==
The head office of [[Private Wings]] is located in the General Aviation Terminal (Allgemeine Luftfahrt) on the property of Schönefeld Airport.<ref>"[http://www.private-wings.de/fileadmin/user_upload/SXF_Anfahrt_GAT_27_02_09.pdf Anfahrt GAT Schönefeld]." [[Private Wings]]. Retrieved on 7 January 2013.</ref><ref>"[http://www.private-wings.de/fileadmin/website/01_home/Anfahrt_SXF.pdf Access Business Aviation Center/GAT]." [[Private Wings]]. Retrieved on 7 January 2013.</ref><ref>"[http://www.private-wings.de/en/imprint.html Imprint]." [[Private Wings]]. Retrieved on 7 January 2013. "Postal adress:{{sic}} PRIVATE WINGS Flugcharter GmbH Chief executive officers: Peter Paul Gatz und Andreas Wagner Flughafen Berlin – Schönefeld 12521 Berlin, Germany" and "Delivery address: Private Wings Flugcharter GmbH Waßmannsdorfer Straße 12529 Schönefeld (ehemals Diepensee)"</ref> Before its demise, the [[East German]] airline company [[Interflug]] had its headquarters on the airport property.<ref>"World Airline Directory." ''[[Flight International]]''. 26 March 1988. [http://www.flightglobal.com/pdfarchive/view/1988/1988%20-%200770.html?search=Interflug 82]. "Head Office: DDR-1189, Berlin-Schönefeld Flughafen, German Democratic Republic."</ref><ref>"World Airline Directory." ''[[Flight International]]''. 26 March 1970. [http://www.flightglobal.com/pdfarchive/view/1970/1970%20-%200534.html 484]. "Head Office: Zentralflughafen. Berlin-Schonefeld, 1189. German Democratic Republic."</ref>

==Statistics==
[[File:Berlin - Schonefeld (SXF - EDDB) AN1059122.jpg|thumb|Aerial view prior to the start of the construction of [[Berlin Brandenburg Airport]]]]
[[File:Airbus A320-231, LY-COS WOW Force Two, WOW air (X9).jpg|thumb|A [[WOW air]] [[Airbus A320-200]] at Berlin Schönefeld Airport with [[Berlin Brandenburg Airport]] in the background]]
{| class="wikitable" style="text-align: right; width:200px;" align="center"
|+
|-
! style="width:75px"| !! {{nowrap|Passengers}}
|-
!2000
| 2,209,444
|-
!2001
| {{nowrap|{{decrease}} 1,915,110}}
|-
!2002
| {{decrease}} 1,688,028
|-
!2003
| {{increase}} 1,750,921
|-
!2004
| {{increase}} 3,382,106
|-
!2005
| {{increase}} 5,075,172
|-
!2006
| {{increase}} 6,059,343
|-
!2007
| {{increase}} 6,331,191
|-
!2008
| {{increase}} 6,638,162
|-
!2009
| {{increase}} 6,797,158
|-
!2010
| {{increase}} 7,297,911
|-
!2011
| {{decrease}} 7,113,989
|-
!2012
| {{decrease}} 7,097,274
|-
!2013
| {{decrease}} 6,727,306
|-
!2014
| {{increase}} 7,292,517
|-
!2015
| {{increase}} 8,526,268
|-
!2016
| {{increase}} 11,652,922
|-
| colspan=5 align="right"| <small>''Source: ADV''<ref>{{cite web|url=http://www.adv.aero|title=Flughafenverband ADV – Unsere Flughäfen: Regionale Stärke, Globaler Anschluss|author=Flughafenverband ADV|publisher=|accessdate=4 June 2015}}</ref></small>
|}

==Ground transportation==

===Train===
[[File:Bahnhof_Berlin-Schoenefeld_Flughafen_Gebaeude.jpg|thumb|The airport's railway station]]
[[File:Schoenefeld Bhf Anbindung.png|thumb|Express Connections to Berlin City]]
Berlin Schönefeld Airport is served by [[Berlin Schönefeld Flughafen station|Berlin-Schönefeld Flughafen railway station]], a short walking distance through a ground-level covered walkway from the airport terminals A-D. [[Berlin S-Bahn]] lines [[S9 (Berlin)|S9]] and [[S45 (Berlin)|S45]] run every twenty minutes, but to reach [[Berlin Hauptbahnhof|Berlin's Central Station]] (''Berlin Hbf'') without changing trains, there are regional trains (''RE'' Regional-Express or ''RB'' Regional-Bahn) running every 30 minutes. These direct trains call at [[Berlin Ostbahnhof]], [[Berlin Alexanderplatz station|Alexanderplatz]], [[Berlin Friedrichstraße station|Friedrichstrasse]], [[Berlin Hauptbahnhof|Central Station]] (after 30 minutes) and [[Berlin Zoologischer Garten railway station|Zoologischer Garten]].

===Car===
The airport can be reached via the nearby motorway [[Bundesautobahn 113|A113]] (Exit ''Schönefeld Süd'') which itself is connected to motorways [[Bundesautobahn 100|A100]] which leads to Berlin city center and [[Bundesautobahn 10|A10]] which circles around Berlin and connects further to all directions.

===Bus===
The airport is linked by local [[Berliner Verkehrsbetriebe|BVG]] bus lines 162 (towards [[Adlershof]]) and 171 (towards [[Neukölln]]). Additionally the X7 bus service provides a connection to the [[Berlin U-Bahn|Berlin U-Bahn network]] at [[Rudow (Berlin U-Bahn)|Rudow Station]]<ref>{{cite web|url=http://www.bvg.de/index.php/en/17105/name/Bus.html |title=Berlin bus lines. Retrieved 23 December 2009 |publisher=Bvg.de |date=}}</ref> At night, the underground replacement bus [[U7 (Berlin U-Bahn)|N7]] is available.

==Accidents and incidents==
*On 14 August 1972, an [[Ilyushin Il-62]] aircraft of [[Interflug]] ([[aircraft registration|registered]] DM-SEA) '''[[1972 Königs Wusterhausen air disaster|crashed]]''' near [[Königs Wusterhausen]] shortly into a flight to [[Burgas]], killing all 156 passenger and crew on board.
*On 22 November 1977, a [[Tupolev Tu-134]] aircraft of Interflug (registration DM-SCM) crashed upon landing at Schönefeld Airport due to a falsely configured [[autopilot]]. There were no fatalities among the 74 passenger and crew, but the aircraft was damaged beyond repair.<ref>{{cite web|url=http://aviation-safety.net/database/record.php?id=19771122-1 |title=Interflug accident of 1977 at the Aircraft Accident Database. Retrieved 23 December 2009 |publisher=Aviation-safety.net |date= |accessdate=10 January 2012}}</ref>
*On 19 August 1978, [[LOT Polish Airlines Flight 165 hijacking|LOT Polish Airlines Flight 165]], a [[LOT Polish Airlines|LOT]] flight from [[Gdańsk Lech Wałęsa Airport]] to Schönefeld (carried out on a Tupolev Tu-134, registration SP-LGC),was hijacked and forced to land at Tempelhof Airport in West Berlin, thus having been used as a means for escaping the [[Eastern Bloc]]. In these cases, perpetrators were usually not charged by Western authorities.<ref>{{cite web|url=http://aviation-safety.net/database/record.php?id=19780830-0 |title=LOT highjacking at the Aircraft Accident Database. Retrieved 23 December 2009 |publisher=Aviation-safety.net |date= |accessdate=10 January 2012}}</ref>
*On 12 December 1986, [[Aeroflot Flight 892]] an Aeroflot Tupolev Tu-134 (registration CCCP-65795) coming from [[Minsk International Airport|Minsk Airport]] crashed in [[Bohnsdorf|Berlin Bohnsdorf]] on its approach towards Schönefeld airport, after having attempted to land on a runway that was temporary blocked for construction work, killing 72 of the 82 passengers and crew on board.<ref>{{cite web|url=http://aviation-safety.net/database/record.php?id=19861212-0 |title=Aeroflot accident of 1986 at the Aviation Accident Database. Retrieved 23 December 2009 |publisher=Aviation-safety.net |date= |accessdate=10 January 2012}}</ref>
*On 17 June 1989, an Ilyushin Il-62 aircraft of Interflug (registration DDR-SEW) bound for Moscow crashed shortly after take-off into a field near the airport and caught fire. 21 people on board as well as one person on the ground were killed. The East German authorities feared an act of sabotage due to the anniversary of the [[Uprising of 1953 in East Germany|East German uprising]], which led to a delayed aid for injured people. West German rescuers offering help were denied access to the scene. The cause for the accident was later given as a jammed rudder due to a manufacturing defect.<ref>{{cite web|url=http://aviation-safety.net/database/record.php?id=19890617-2 |title=Interflug accident of 1989 at the Aviation Accident Database. Retrieved 23 December 2009 |publisher=Aviation-safety.net |date= |accessdate=10 January 2012}}</ref>
*On 28 March 2000, a [[Boeing 737 Classic|Boeing 737-300]] of Germania (registration D-AGES) operating a charter flight on behalf of [[LTU International|LTU]] from [[Tenerife South Airport]] to Schönefeld was the subject of an attempted hijack in mid-flight. A passenger forced his way into the cockpit, where he attacked the pilot, leading to a sudden loss of altitude. The perpetrator was restrained and the flight continued to Berlin.<ref>{{cite web|url=http://aviation-safety.net/database/record.php?id=20000328-0 |title=Germania attempted highjacking at the Aircraft Accident Database. Retrieved 23 December 2009 |publisher=Aviation-safety.net |date= |accessdate=10 January 2012}}</ref>
*On 19 June 2010, a 1944-built, historic [[Douglas DC-3]] D-CXXX of [[2010 Berlin Air Services DC-3 crash|Berlin Air Services crashed]] shortly after take off on a local sightseeing flight, causing 7 injuries but no fatalities.<ref name=ASN190610>{{cite web|url=http://aviation-safety.net/database/record.php?id=20100619-0 |title=Accident description |publisher=Aviation Safety Network |accessdate=20 June 2010}}</ref>

==See also==
* [[Transport in Germany]]
* [[List of airports in Germany]]
* [[Berlin Brandenburg Airport]]

==References==
{{Reflist|30em}}

==External links==
{{commonscat-inline|Berlin Schönefeld Airport}}
* [http://www.berlin-airport.de/en/travellers-sxf/index.php Official website]
* {{NWS-current|EDDB}}
* {{ASN|SXF}}

{{Portalbar|Berlin|Germany|East Germany<!--In its time period-->|Aviation}}
{{Airports in Germany}}

{{DEFAULTSORT:Berlin Schonefeld Airport}}
[[Category:Airports established in 1934]]
[[Category:Airports in Brandenburg]]
[[Category:Aviation in Berlin|Schonefeld Airport]]
[[Category:Buildings and structures in Treptow-Köpenick|Schonefeld Airport]]<!--PARTLY IN BERLINER TERRITORY-->
[[Category:Military facilities of the Soviet Union in Germany]]
[[Category:Dahme-Spreewald]]