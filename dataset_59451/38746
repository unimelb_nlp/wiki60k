{{other uses}}
{{Use mdy dates|date=November 2016}}
{{good article}}
{{Use American English|date=January 2014}}
{{Infobox film
| name           = Bringing Up Baby
| image          = BabyPoster2.jpg
| image_size     =
| caption        = Original theatrical poster
| producer       = [[Cliff Reid]]<br>Howard Hawks
| director       = [[Howard Hawks]]
| writer         = [[Dudley Nichols]]<br>[[Hagar Wilde]]<br>[[Robert A. McGowan|Robert McGowan]] (uncredited)<br>Gertrude Purcell (uncredited)
| based on       = {{Based on|''Bringing Up Baby''<br>1937 short story in ''[[Collier's]]''|[[Hagar Wilde]]}}
| starring       = [[Katharine Hepburn]]<br>[[Cary Grant]]<br>[[Charles Ruggles]]<br>[[Walter Catlett]]<br>[[May Robson]]<br>[[Fritz Feld]]<br>[[Barry Fitzgerald]]
| music          = [[Roy Webb]] (musical director)<br>[[Jimmy McHugh]]<br>[[Dorothy Fields]] (original writers of ''I Can't Give You Anything but Love, Baby'')
| cinematography = [[Russell Metty]]
| editing        = [[George Hively]]
| studio         = [[RKO Radio Pictures]]
| distributor    = RKO Radio Pictures
| released       = {{Film date|1938|2|16}}<small>(San Francisco)</small>{{sfn|Hanson|1993|p=235}}
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = $1,073,000
| gross          = $1,109,000
}}

'''''Bringing Up Baby''''' is a 1938 American [[screwball comedy film]] directed by [[Howard Hawks]], starring [[Katharine Hepburn]] and [[Cary Grant]], and released by [[RKO Pictures|RKO Radio Pictures]]. The film tells the story of a paleontologist in a number of predicaments involving a scatterbrained woman and a leopard named Baby.  The screenplay was adapted by [[Dudley Nichols]] and [[Hagar Wilde]] from a short story by Wilde which originally appeared in ''[[Collier's Weekly]]'' magazine on April 10, 1937.

The script was written specifically for Hepburn, and was tailored to her personality. Filming began in September 1937 and wrapped in January 1938; it was over schedule and over budget. Production was frequently delayed due to uncontrollable laughing fits between Hepburn and Grant. Hepburn struggled with her comedic performance and was coached by her co-star, vaudeville veteran [[Walter Catlett]]. A tame leopard was used during the shooting; its trainer was off-screen with a whip for all its scenes.

Although it has a reputation as a flop upon its release, ''Bringing up Baby'' was moderately successful in many cities and eventually made a small profit after its re-release in the early 1940s. Shortly after the film's premiere, Hepburn was infamously labeled box-office poison by the Independent Theatre Owners of America and would not regain her success until ''[[The Philadelphia Story (film)|The Philadelphia Story]]'' two years later. The film's reputation began to grow during the 1950s, when it was shown on television.

Since then, the film has received acclaim from both critics and audience for its zany antics and pratfalls, absurd situations and misunderstandings, perfect sense of comic timing, completely screwball cast, series of lunatic and hare-brained misadventures, disasters, light-hearted surprises and romantic comedy.<ref>{{Cite web|title = Bringing Up Baby (1938)|url = http://www.filmsite.org/brin.html|website = www.filmsite.org|access-date = February 8, 2016}}</ref> Nowadays, it is considered [[List of films considered the best|one of the greatest films ever made]].

In 1990 ''Bringing Up Baby'' was selected for preservation in the [[National Film Registry]] of the [[Library of Congress]] as "culturally, historically, or aesthetically significant", and it has appeared on a number of greatest-films lists, ranking at 88th on the [[American Film Institute]]'s [[AFI's 100 Years...100 Movies (10th Anniversary Edition)|100 greatest American films of all time]] list.

==Plot==
David Huxley ([[Cary Grant]]) is a mild-mannered [[paleontologist]]. For the past four years, he has been trying to assemble the skeleton of a ''[[Brontosaurus]]'' but is missing one bone: the "intercostal [[clavicle]]". Adding to his stress is his impending marriage to the dour Alice Swallow (Virginia Walker) and the need to impress Elizabeth Random ([[May Robson]]), who is considering a million-dollar donation to his museum.

The day before his wedding, David meets Susan Vance ([[Katharine Hepburn]]) by chance on a golf course. She is a free-spirited young lady, and (unknown to him at first) Mrs. Random's niece. Susan's brother, Mark, has sent her a tame leopard from [[Brazil]] named Baby (Nissa) to give to their aunt. (The leopard is native to Africa and Asia but not to South America.) Susan thinks David is a [[zoologist]] (rather than a paleontologist), and persuades David to go to her country home in Connecticut to help bring up Baby (which includes singing "[[I Can't Give You Anything but Love, Baby|I Can't Give You Anything But Love]]" to soothe the leopard). Complications arise since Susan has fallen in love with David and tries to keep him at her house as long as possible to prevent his marriage.

David finally receives the intercostal clavicle, but Susan's dog George ([[Skippy (dog)|Asta]]) takes it out of its box and buries it. Susan's aunt, Elizabeth Random, arrives. The dowager is unaware of David's identity, since Susan has introduced him as "Mr. Bone". Baby and George run off, and Susan and David mistake a dangerous leopard who was being driven to be euthanized from a nearby circus (also portrayed by Nissa) for Baby, and let it out of the cage.

[[File:Grant Hepburn Bringing up baby.jpg|thumb|left|alt=Cary Grant and Katharine Hepburn in adjacent jail cells|David and Susan in jail.]]
After considerable running around, David and Susan are jailed by a befuddled town policeman, Constable Slocum ([[Walter Catlett]]), for breaking into the house of Dr. Fritz Lehman ([[Fritz Feld]]) (where they had cornered the circus leopard). When Slocum does not believe their story, Susan tells him they are members of the "Leopard Gang"; she calls herself "Swingin' Door Susie", and David "Jerry the Nipper".{{Efn|"Jerry the Nipper" was Irene Dunne's nickname for Grant's character in ''[[The Awful Truth]]'', which also featured Asta}} David fails to convince the constable that Susan makes things up "from motion pictures she's seen". Eventually, Alexander Peabody ([[George Irving (American actor)|George Irving]]) shows up to verify everyone's identity. Susan, who during a police interview contrived to sneak out a window, unwittingly drags the irritated circus leopard into the jail. David saves her, using a chair to shoo the big cat into a cell.

Some time later Susan finds David, who has just been jilted by Alice because of her, on a high platform at his brontosaurus reconstruction at the museum. After showing him the missing bone which she'd found by trailing George for three days, Susan, against his warnings, climbs a tall ladder next to the dinosaur to be closer to him. She tells David that her Aunt has given her the million dollars, and she wants to donate it to the museum, but David is more interested in telling her that the day spent with her was the best day of his life. Unconsciously swaying the ladder from side to side upon hearing David's further words of endearment and love, Susan tells him that she loves him too, then notices that the ladder is on the verge of falling over. Frightened, she climbs onto and over the skeleton, but just before the dinosaur bones collapse David grabs her hand, she dangles below him, and he lifts her onto the platform. Regrettably surveying the wreckage of his work, David soon accepts the destruction and chaos, gives in, and hugs and kisses Susan.

==Cast==
{{col begin}}{{col break|col=60ee}}
;Credited
* [[Cary Grant]] as Dr. David Huxley (alias Mr. Bone)
* [[Katharine Hepburn]] as Susan Vance
* [[May Robson]] as Elizabeth Carlton Random, Susan's aunt
* [[Charles Ruggles]] as Major Horace Applegate, a big-game hunter
* [[Walter Catlett]] as Constable Slocum
* [[Barry Fitzgerald]] as Aloysius Gogarty, Mrs. Random's gardener
* [[Fritz Feld]] as Dr. Fritz Lehman, an affluent psychiatrist
* Virginia Walker as Alice Swallow, David's fiancée
* [[George Irving (American actor)|George Irving]] as Alexander Peabody, Mrs. Random's lawyer
* [[Leona Roberts]] as Hannah Gogarty, Aloysius' wife and Mrs. Random's servant
* [[Tala Birell]] as Mrs. Lehman, Dr. Lehman's wife
* [[John Kelly (actor, born 1901)|John Kelly]] as Elmer, Constable Slocum's assistant
{{col break}}
;Uncredited
* [[D'Arcy Corrigan]] as Professor LaTouche
* [[Billy Bevan]] as Tom, the barkeeper
* [[Billy Franey]] as the butcher
* [[Dick Lane (TV announcer)|Dick Lane]] as Circus manager
* [[Ward Bond]] as a motorcycle policeman
* [[Jack Carson]] as a circus roustabout

;Animal actors
* [[Skippy (dog)|Skippy]] as George, Mrs. Random's dog
* Nissa as Baby and the circus leopard
{{col-end}}

==Production==

===Development and writing===
[[File:Hawks portrait crop.png|thumb|left|Director [[Howard Hawks]] began working on the film after plans to adapt "[[Gunga Din]]" were delayed.]]
In March 1937 [[Howard Hawks]] signed a contract at RKO for an adaptation of [[Rudyard Kipling]]'s "[[Gunga Din]]", which had been in pre-production since the previous fall. When RKO was unable to borrow [[Clark Gable]], [[Spencer Tracy]] and [[Franchot Tone]] from [[Metro-Goldwyn-Mayer]] for the film and the adaptation of "Gunga Din" was delayed, Hawks began looking for a new project. In April 1937 he read a short story by [[Hagar Wilde]] in ''Collier's'' magazine called "Bringing Up Baby" and immediately wanted to make a film from it,{{sfn|Mast|1988|p=4}} remembering that it made him laugh out loud.{{sfn|Eliot|2004|p=175}} RKO bought the screen rights in June{{sfn|Mast|1988|p=5}} for $1,004, and Hawks worked briefly with Wilde on the film's treatment.{{sfn|McCarthy|1997|p=246}} Wilde's short story differed significantly from the film: David and Susan are engaged, he is not a scientist and there is no dinosaur, intercostal clavicle or museum. However, Susan gets a pet panther from her brother Mark to give to their Aunt Elizabeth; David and Susan must capture the panther in the Connecticut wilderness with the help of Baby's favorite song, "I Can't Give You Anything but Love, Baby".{{sfn|Mast|1988|p=5}}

Hawks then hired screenwriter [[Dudley Nichols]], best known for his work with director [[John Ford]], for the script; Wilde would develop the characters and comedic elements of the script, while Nichols would take care of the story and structure. Hawks worked with the two writers during summer 1937, and they came up with a 202-page script.{{sfn|McCarthy|1997|p=247}} Wilde and Nichols wrote several drafts together, beginning a romantic relationship and co-authoring the [[Fred Astaire]] and [[Ginger Rogers]] film ''[[Carefree (film)|Carefree]]'' a few months later.{{sfn|Mast|1988|p=5}} The ''Bringing Up Baby'' script underwent several changes, and at one point there was an elaborate pie fight, inspired by [[Mack Sennett]] films. Major Applegate had an assistant and food taster named Ali (which was intended to be played by [[Mischa Auer]]), but this character was replaced with Aloysius Gogarty. The script's final draft had several scenes in the middle of the film in which David and Susan declare their love for each other which Hawks cut  during production.{{sfn|Mast|1988|p=6}}

Nichols was instructed to write the film for Hepburn, with whom he had worked on John Ford's ''[[Mary of Scotland (film)|Mary of Scotland]]'' in 1936.{{sfn|Leaming|1995|p=348}} Barbara Leaming alleged that Ford had an affair with Hepburn, and claims that many of the characteristics of Susan and David were based on Hepburn and Ford.{{sfn|Leaming|1995|pp=348–349}} Nichols was in touch with Ford during the screenwriting, and the film included such members of the [[John Ford Stock Company]] as [[Ward Bond]], [[Barry Fitzgerald]], [[D'Arcy Corrigan]] and associate producer Cliff Reid.{{sfn|Leaming|1995|pp=348–9}} John Ford was a friend of Hawks, and visited the set. The round glasses Grant wears in the film are reminiscent of Harold Lloyd and of Ford.{{sfn|Leaming|1995|p=349}}

Filming was initially scheduled to begin on September 1, 1937 and wrap on October 31, but was delayed for several reasons. Production had to wait until mid-September to clear the rights for "I Can't Give You Anything but Love, Baby" for $1,000. In August Hawks hired gag writers [[Robert A. McGowan|Robert McGowan]] and Gertrude Purcell{{sfn|Mast|1988|p=29}} for uncredited script rewrites, and McGowan added a scene inspired by the comic strip ''[[Harold Knerr#Dinglehoofer und His Dog Adolph|Professor Dinglehoofer and his Dog]]'' in which a dog buries a rare dinosaur bone.{{sfn|Mast|1988|p=6}} RKO paid [[King Features]] $1,000 to use the idea for the film on September 21.{{sfn|Mast|1988|p=7}}

====Unscripted ad-lib by Grant====
It is debated by some whether ''Bringing Up Baby'' is the first fictional work (apart from [[pornography]]) to use the word ''[[gay]]'' in a [[homosexual]] context.<ref>{{cite web |url=http://www2.lib.virginia.edu/exhibits/censored/film.html |title=Censored Films and Television at University of Virginia online |publisher=University of Virginia Library |accessdate=March 9, 2014}}</ref>{{sfn|Boswell|2009|p=43}} In one scene, Cary Grant's character is wearing a woman's [[Marabou (fashion)|marabou]]-trimmed [[négligée]]; when asked why, he replies exasperatedly "Because I just went gay all of a sudden!" (leaping into the air at the word ''gay''). As the term ''gay'' did not become familiar to the general public until the [[Stonewall riots]] in 1969,{{sfn|Russo|1987|p=47}} it is debated whether the word was used here in its original sense (meaning "happy"<ref name="Gay">{{cite web |url=http://www.etymonline.com/index.php?term=gay |last=Harper |first=Douglas |authorlink=Douglas Harper |title=Gay |work=Online Etymology dictionary |date=2001–2013 }}</ref>) or is an intentional, joking reference to homosexuality.<ref name="Gay"/>

In the film, the line was an [[Ad libitum|ad-lib]] by Grant and not in any version of the original script.{{sfn|Mast|1988|p=8}} According to [[Vito Russo]] in ''[[The Celluloid Closet]]'' (1981, revised 1987), the script originally had Grant's character say "I...I suppose you think it's odd, my wearing this. I realize it looks odd...I don't usually...I mean, I don't own one of these". Russo suggests that this indicates that people in [[Hollywood]] (at least in Grant's circles) were familiar with the slang connotations of the word; however, neither Grant nor anyone involved in the film suggested this.{{sfn|Russo|1987|p=47}}

===Casting===
[[File:Bringing up baby film still.jpg|thumb|alt=Katharine Hepburn and Cary Grant (in round glasses), looking off-screen|Hepburn and Grant in their second of four film collaborations]]
After briefly considering Hawks' cousin [[Carole Lombard]] for the role of Susan Vance, Katharine Hepburn was chosen to play the wealthy New Englander because of her background and similarities to the character. RKO agreed to the casting, but had reservations because of Hepburn's salary and lack of box-office success for several years.{{sfn|McCarthy|1997|p=247}} Producer Lou Lusty said, "You couldn't even break even if a Hepburn show cost eight hundred grand."{{sfn|Mast|1988|p=7}} At first, Hawks and producer [[Pandro S. Berman]] could not agree on who to cast in the role of David Huxley. Hawks initially wanted silent-film comedian [[Harold Lloyd]]; Berman rejected Lloyd and [[Ronald Colman]], offering the role to [[Robert Montgomery (actor)|Robert Montgomery]], [[Fredric March]] and [[Ray Milland]] (all of whom turned it down).{{sfn|Eliot|2004|pp=176–177}}

Hawks' friend [[Howard Hughes]] finally suggested Cary Grant for the role.{{sfn|Eliot|2004|p=174}} Grant had just finished shooting his breakthrough romantic comedy ''[[The Awful Truth]]'',{{sfn|McCarthy|1997|p=247}} and Hawks may have seen a rough cut of the unreleased film.{{sfn|Mast|1988|p=7}} Grant then had a non-exclusive, four-picture deal with RKO for $50,000 per film, and Grant's manager used his casting in the film to renegotiate his contract, earning him $75,000 plus the bonuses Hepburn was receiving.{{sfn|Eliot|2004|pp=176–177}} Grant was initially concerned about being able to play an intellectual character and took two weeks to accept the role, despite the new contract. Hawks built Grant's confidence by promising to coach him throughout the film, instructing him to watch Harold Lloyd films for inspiration.{{sfn|Eliot|2004|p=178}} Grant met with Howard Hughes throughout the film to discuss his character, which he said helped his performance.{{sfn|Eliot|2004|p=178}}

Hawks obtained character actors [[Charlie Ruggles]] on loan from [[Paramount Pictures]] for Major Horace Applegate and Barry Fitzgerald on loan from [[The Mary Pickford Corporation]] to play gardener Aloysius Gogarty.{{sfn|McCarthy|1997|p=247}} Hawks cast Virginia Walker as Alice Swallow, David's fiancée; Walker was under contract to him and later married his brother [[William Hawks]].{{sfn|McCarthy|1997|p=248}} As Hawks could not find a panther that would work for the film, Baby was changed to a leopard so they could cast the trained leopard Nissa, who had worked in films for eight years, making several B-movies.{{sfn|Mast|1988|p=7}}

===Filming===
Shooting began September 23, 1937 and was scheduled to end November 20, 1937{{sfn|McCarthy|1997|p=254}} on a budget of $767,676.{{sfn|McCarthy|1997|p=250}} Filming began in-studio with the scenes in Susan's apartment, moving to the Bel Air Country Club in early October for the golf-course scenes.{{sfn|Mast|1988|p=7}} The production had a difficult start due to Hepburn's struggles with her character and her comedic abilities. She frequently overacted, trying too hard to be funny,{{sfn|McCarthy|1997|p=250}} and Hawks asked vaudeville veteran Walter Catlett to help coach her. Catlett acted out scenes with Grant for Hepburn, showing her that he was funnier when he was serious. Hepburn understood, acted naturally and played herself for the rest of the shoot; she was so impressed by Catlett's talent and coaching ability that she insisted he play Constable Slocum in the film.{{sfn|McCarthy|1997|pp=250–251}}{{sfn|Mast|1988|p=261}}

[[File:Bringing up baby publicity photo.jpg|thumb|left|200px|alt=Katharine Hepburn, smiling, and leopard looking off-camera|Katharine Hepburn and Nissa in a publicity photo; at one point, Nissa lunged at Hepburn and was only stopped by the trainer's whip.]]

Most shooting was done at the Arthur Ranch in the San Francisco Valley, which was used as Aunt Elizabeth's estate for interior and exterior scenes.{{sfn|Mast|1988|p=7}} Beginning at the Arthur Ranch shoot,{{sfn|Mast|1988|p=8}} Grant and Hepburn often [[ad-lib]]bed their dialogue and frequently delayed production by making each other laugh.{{sfn|McCarthy|1997|p=251}} The scene where Grant frantically asks Hepburn where his bone is was shot from 10 am until well after 4 pm because of the stars' laughing fits.{{sfn|McCarthy|1997|p=252}} After one month of shooting Hawks was seven days behind schedule. During the filming, Hawks would refer to four different versions of the film's script and make frequent changes to scenes and dialogue.{{sfn|Mast|1988|p=8}} His leisurely attitude on set and shutting down production to see a horse race contributed to the time it took to film,{{sfn|McCarthy|1997|p=252}} and he took twelve days to shoot the Westlake jail scene instead of the scheduled five.{{sfn|Mast|1988|p=8}} Hawks later facetiously blamed the setbacks on his two stars' laughing fits and having to work with two animal actors.{{sfn|McCarthy|1997|p=252}}

The terrier George was played by Skippy, known as Asta in ''[[The Thin Man]]'' film series and co-starring with Grant (as Mr. Smith) in ''The Awful Truth''. The tame leopard Baby and the escaped circus leopard were both played by a trained leopard, Nissa. The big cat was supervised by its trainer, Olga Celeste, who stood by with a whip during shooting. At one point, when Hepburn spun around (causing her skirt to twirl) Nissa lunged at her and was subdued when Celeste cracked her whip. Hepburn wore heavy perfume to keep Nissa calm and was unafraid of the leopard, but Grant was terrified; most scenes of the two interacting are done in close-up with a stand-in. Hepburn played upon this fear by throwing a toy leopard through the roof of Grant's dressing room during production{{sfn|McCarthy|1997|p=252}} There were also several news reports about Hawks' difficulty filming a live leopard, and some scenes required [[Rear projection effect|rear-screen projection]].<ref name="Baby DVD. Special Features 2005">''Bringing Up Baby'' DVD. Special Features. Peter Bogdanovich Audio Commentary. Turner Home Entertainment. 2005.</ref>

Hawks and Hepburn had a confrontation one day during shooting. While Hepburn was chatting with a crew member, Hawks yelled "Quiet!" until the only person still talking was Hepburn. When Hepburn paused and realized that everyone was looking at her, she asked what was the matter; Hawks asked her if she was finished imitating a parrot. Hepburn took Hawks aside, telling him never to talk to her like that again since she was old friends with most of the crew. When Hawks (an older friend of the crew) asked a lighting tech who he would rather drop a light on, Hepburn agreed to behave on set. A variation of this scene, with Grant yelling "Quiet!", was incorporated into the film.{{sfn|Mast|1988|p=261}}{{sfn|McCarthy|1997|p=253}}

The Westlake Street set was shot at 20th Century Fox Studios.{{sfn|Mast|1988|p=29}} Filming was eventually completed on January 6, 1938 with the scenes outside Mr. Peabody's house. RKO producers expressed concerns about the film's delays and expense, coming 40 days over schedule and $330,000 over budget, and also disliked Grant's glasses and Hepburn's hair.{{sfn|McCarthy|1997|p=253}} The film's final cost was $1,096,796.23, primarily due to overtime clauses in Hawks', Grant's and Hepburn's contracts.{{sfn|McCarthy|1997|p=254}} The film's cost for sets and props was only $5,000 over budget, but all actors (including Nissa and Skippy) were paid approximately double their initial salaries. Hepburn's salary rose from $72,500 to $121,680.50, Grant's salary from $75,000 to $123,437.50 and Hawks' salary from $88,046.25 to $202,500. The director received an additional $40,000 to terminate his RKO contract on March 21, 1938.{{sfn|Mast|1988|p=14}}

===Post-production and previews===
Hawks' editor, George Hively, cut the film during production and the final prints were made a few days after shooting ended.{{sfn|McCarthy|1997|p=254}} The first cut of the film (10,150 feet long){{sfn|Mast|1988|p=12}} was sent to the [[Motion Picture Production Code|Hayes Office]] in mid-January.{{sfn|Mast|1988|p=13}} Despite several [[double entendre]]s and sexual references it passed the film,{{sfn|McCarthy|1997|p=254}} overlooking Grant saying he "went gay" or Hepburn's reference to George urinating. The censor's only objections were to the scene where Hepburn's dress is torn, and references to politicians (such as [[Al Smith]] and [[Jim Farley]]).{{sfn|Mast|1988|p=13}}

Like all Hawks' comedies, the film is known for its fast pace (despite being filmed primarily in long medium shots, with little cross-cutting). Hawks told Peter Bogdanovich, "You get more pace if you pace the actors quickly within the frame rather than cross cutting fast".<ref name="Baby DVD. Special Features 2005"/>

By February 18, the film had been cut to 9,204 feet.{{sfn|Mast|1988|p=13}} It had two advance previews in January 1938, where it received either As or A-pluses on audience-feedback cards. Producer Pandro S. Berman wanted to cut five more minutes, but relented when Hawks, Grant and Cliff Reid objected.{{sfn|Mast|1988|p=13}} At the film's second preview, the film received rave reviews and RKO expected a hit.{{sfn|McCarthy|1997|p=254}} The film's musical score is minimal, primarily Grant and Hepburn singing "I Can't Give You Anything But Love, Baby". There is incidental music in the Ritz scene, and an arrangement of "I Can't Give You Anything But Love, Baby" during the opening and closing credits by musical director [[Roy Webb]].{{sfn|Mast|1988|p=9}}

=={{anchor|Reception and Box Office}}Reception and box office==
The film received good advance reviews; [[Otis Ferguson]] of ''[[The New Republic]]'' thought the film very funny, praising Hawks' direction.{{sfn|Mast|1988|p=268}} ''[[Variety (magazine)|Variety]]'' praised the film, singling out Hawks' pacing and direction, calling Hepburn's performance "one of her most invigorating screen characterizations" and saying Grant "performs his role to the hilt";{{sfn|Mast|1988|p=266}} their only criticism was the length of the jail scene.{{sfn|Mast|1988|p=267}} ''[[Film Daily]]'' called it "literally a riot from beginning to end, with the laugh total heavy and the action fast."<ref>{{cite journal |date=February 11, 1938 |title=Reviews of the New Films |journal=[[Film Daily]] |page=12}}</ref> ''[[Harrison's Reports]]'' called the film "An excellent farce" with "many situations that provoke hearty laughter,"<ref>{{cite journal |date=February 19, 1938 |title=Bringing Up Baby |journal=[[Harrison's Reports]] |page=31}}</ref> and [[John Mosher (writer)|John Mosher]] of ''[[The New Yorker]]'' wrote that both stars "manage to be funny" and that Hepburn had never "seemed so good-natured."<ref>{{cite journal |last=Mosher |first=John |authorlink=John Mosher (writer) |date=March 5, 1938 |title=The Current Cinema |journal=[[The New Yorker]] |pages=61–62}}</ref> However, [[Frank S. Nugent]] of the ''[[New York Times]]'' disliked the film, considering it derivative and cliché-ridden, a rehash of dozens of other screwball comedies of the period. He labeled Hepburn's performance "breathless, senseless, and terribly, terribly fatiguing",{{sfn|Mast|1988|p=265}} and added, "If you've never been to the movies, ''Bringing Up Baby'' will be new to you&nbsp;– a zany-ridden product of the goofy-farce school. But who hasn't been to the movies?"{{sfn|Laham|2009|p=29}}

Despite ''Bringing Up Baby''{{-'}}s reputation as a flop, it was successful in some parts of the U.S. The film premiered on February 16, 1938 at the [[Golden Gate Theatre]] in [[San Francisco]] (where it was a hit), and was also successful in [[Los Angeles]], [[Portland, Oregon|Portland]], [[Denver]], [[Cincinnati]] and [[Washington, D.C.]]. However, it was a financial disappointment in the Midwest, as well as most other cities in the country, including NYC; to RKO's chagrin, the film's premiere in [[New York City|New York]] on March 3, 1938 at [[Radio City Music Hall]] made only $70,000 and it was pulled after one week{{sfn|McCarthy|1997|p=255}} in favor of ''[[Jezebel (film)|Jezebel]]'' with [[Bette Davis]].{{sfn|Brown|1995|p=140}}

During its first run, ''Bringing Up Baby'' made $715,000 in the U.S. and $394,000 in foreign markets for a total of $1,109,000;{{sfn|Mast|1988|p=14}} its reissue in 1940 and 1941 made an additional $95,000 in the US and $55,000 in foreign markets.{{sfn|McCarthy|1997|p=255}} Following its second run, the film made a profit of $163,000.{{sfn|Mast|1988|p=14}} Due to its perceived failure, Hawks was released early from his two-film contract with RKO{{sfn|Mast|1988|p=14}} and ''[[Gunga Din (film)|Gunga Din]]'' was eventually directed by [[George Stevens]].{{sfn|McCarthy|1997|p=257}} Hawks later said the film "had a great fault and I learned an awful lot from that. There were ''no'' normal people in it. Everyone you met was a screwball and since that time I learned my lesson and don't intend ever again to make everybody crazy".{{sfn|McCarthy|1997|p=256}} The director went on to work with RKO on three films over the next decade.{{sfn|Mast|1988|p=16}} Long before ''Bringing Up Baby''{{-'}}s release, Hepburn had been branded "box-office poison" by Harry Brandt (president of the Independent Theatre Owners of America) and thus was allowed to buy out her RKO contract for $22,000.{{sfn|Eliot|2004|pp=180–1}}{{sfn|McCarthy|1997|pp=255–7}} However, many critics marveled at her new skill at [[low comedy]]; [[Life magazine|''Life'' magazine]] called her "the surprise of the picture".{{sfn|Mast|1988|p=15}} Hepburn's former boyfriend Howard Hughes bought RKO in 1941, and sold it in 1959; when he sold the company, Hughes retained the copyright to six films (including ''Bringing Up Baby'').{{sfn|Mast|1988|p=16}}

==Legacy==
''Bringing Up Baby'' was the second of four films starring Grant and Hepburn; the others were ''[[Sylvia Scarlett]]'' (1935), ''[[Holiday (1938 film)|Holiday]]'' (1938) and ''[[The Philadelphia Story (film)|The Philadelphia Story]]'' (1940). The film's concept was described by philosopher [[Stanley Cavell]] as a "definitive achievement in the history of the art of film."{{sfn|Cavell|1981|p=1}} Cavell noted that ''Bringing Up Baby'' was made in a tradition of romantic comedy with inspiration from ancient Rome and Shakespeare.{{sfn|Mast|1988|p=3}} Shakespeare's ''[[Much Ado About Nothing]]'' and ''[[As You Like It]]'' have been cited in particular as influences on the film and the screwball comedy in general, with their "haughty, self-sufficient men, strong women and fierce combat of words and wit."{{sfn|Carr|2002|p=48}} Hepburn's character has been cited as an early example of the [[Manic Pixie Dream Girl]] film archetype.<ref>{{cite web |last1=Bowman |first1=Donna |last2=Gillette |first2=Amelie |last3=Hyden |first3=Steven |last4=Murray |first4=Noel |last5=Pierce |first5=Leonard |last6=Rabin |first6=Nathan |date=August 4, 2008 |title= Wild things: 16 films featuring Manic Pixie Dream Girls |work=[[The A.V. Club]] |url=http://www.avclub.com/article/wild-things-16-films-featuring-manic-pixie-dream-g-2407 |accessdate=March 27, 2014}}</ref>

The popularity of ''Bringing Up Baby'' has increased since it was shown on television during the 1950s, and by the 1960s film analysts (including the writers at ''[[Cahiers du Cinema]]'' in France) affirmed the film's quality. In a rebuttal of fellow ''New York Times'' critic Nugent's scathing review of the film at the time of release, [[A. O. Scott]] has said that you'll "find yourself amazed at its freshness, its vigor, and its brilliance-qualities undiminished after sixty-five years, and likely to withstand repeated viewings."{{sfn|Laham|2009|p=29}} [[Leonard Maltin]] stated that it is now "considered the definitive screwball comedy, and one of the fastest, funniest films ever made; grand performances by all."{{sfn|Laham|2009|p=29}}

''Bringing Up Baby'' has been adapted several times. Hawks recycled the nightclub scene in which Hepburn's dress is torn and Grant walks behind her in the 1964 comedy, ''[[Man's Favorite Sport]]''. Peter Bogdanovich's 1972 film  ''[[What's Up, Doc? (1972 film)|What's Up, Doc?]]'', starring [[Barbra Streisand]], was intended as an homage to the film, and has contributed to its reputation.{{sfn|McCarthy|1997|p=256}} In the commentary track for ''Bringing Up Baby'', Bogdanovich discusses how the coat-ripping scene in ''What's Up, Doc?'' was based on the scene in which Grant's coat and Hepburn's dress are torn in ''Bringing Up Baby''.<ref name="Baby DVD. Special Features 2005"/> The 1987 film ''[[Who's That Girl? (1987 film)|Who's That Girl?]]'', starring [[Madonna (entertainer)|Madonna]], is also loosely based on ''Bringing Up Baby''.<ref>{{cite news |url=http://www.washingtonpost.com/wp-srv/style/longterm/movies/videos/whosthatgirlpghinson_a0c943.htm |title='Who's That Girl' (PG) |date=August 8, 1987 |publisher=The Washington Post |accessdate=March 9, 2014}}</ref>

In 1990 (the registry's second year), ''Bringing Up Baby'' was selected for preservation in the [[National Film Registry]] of the [[Library of Congress]] as "culturally, historically, or aesthetically significant". ''[[Entertainment Weekly]]'' voted the film 24th on its list of greatest films. In 2000, readers of ''[[Total Film]]'' magazine voted it the 47th-greatest comedy film of all time. ''[[Premiere (magazine)|Premiere]]'' ranked Cary Grant's performance as Dr. David Huxley 68th on its list of 100 all-time greatest performances,<ref>''Premiere''. "The 100 Greatest Characters of All Time". Hachette Filipacchi Media U.S., April 2004, retrieved November 18, 2013.</ref> and ranked Susan Vance 21st on its list of 100 all-time greatest movie characters.<ref>''Premiere''. "The 100 Greatest Characters of All Time". Hachette Filipacchi Media U.S., April 2006, retrieved November 18, 2013.</ref>

The [[National Society of Film Critics]] also included ''Bringing Up Baby'' in their "100 Essential Films", considering it to be arguably the director's best film.{{Sfn|Carr|2002|p=48}}

The film is recognized by [[American Film Institute]] in these lists:

* 1998: [[AFI's 100 Years...100 Movies]] – #97<ref>{{cite web|last= |first= |title=AFI's 100 Years...100 Movies |url=http://www.afi.com/Docs/100Years/movies100.pdf |publisher=[[American Film Institute]] |date= |accessdate=July 17, 2016}}</ref>
* 2000: [[AFI's 100 Years...100 Laughs]] – #14<ref>{{cite web|last= |first= |title=AFI's 100 Years...100 Laughs |url=http://www.afi.com/Docs/100Years/laughs100.pdf |publisher=[[American Film Institute]] |date= |accessdate=July 17, 2016}}</ref>
* 2002: [[AFI's 100 Years...100 Passions]] – #51<ref>{{cite web|last= |first= |title=AFI's 100 Years...100 Passions |url=http://www.afi.com/Docs/100Years/passions100.pdf |publisher=[[American Film Institute]] |date= |accessdate=July 17, 2016}}</ref>
* 2005: [[AFI's 100 Years...100 Movie Quotes]]:
** Dr. David Huxley: "It isn't that I don't like you, Susan, because after all, in moments of quiet, I'm strangely drawn toward you; but, well, there haven't been any quiet moments!" – Nominated<ref>{{cite web|url=http://www.afi.com/Docs/100Years/quotes400.pdf |title=AFI's 100 Years...100 Movie Quotes Nominees |format=PDF |date= |accessdate=July 17, 2016}}</ref>
* 2007: [[AFI's 100 Years...100 Movies (10th Anniversary Edition)]] – #88<ref>{{cite web|last= |first= |title=AFI's 100 Years...100 Movies (10th Anniversary Edition) |url=http://www.afi.com/Docs/100Years/100Movies.pdf |publisher=[[American Film Institute]] |date= |accessdate=July 17, 2016}}</ref>
* 2008: [[AFI's 10 Top 10]]:
** Nominated Romantic Comedy Film<ref>{{cite web|url=http://connect.afi.com/site/DocServer/10top10.pdf?docID=381&AddInterest=1781 |title=AFI's 10 Top 10 Nominees |format=PDF |date= |accessdate=August 19, 2016 |deadurl=bot: unknown |archiveurl=https://web.archive.org/web/20110716071937/http://connect.afi.com/site/DocServer/10top10.pdf?docID=381&AddInterest=1781 |archivedate=July 16, 2011 |df=mdy }}</ref>

==References==

===Notes===
{{notelist}}

===Citations===
{{reflist|3}}

===Bibliography===
{{Refbegin|colwidth=30em}}
* {{cite book|ref=harv|last=Boswell|first=John|title=Christianity, Social Tolerance, and Homosexuality: Gay People in Western Europe from the Beginning of the Christian Era to the Fourteenth Century|url=https://books.google.com/books?id=v-MR5_AdG68C|date=February 15, 2009|publisher=University of Chicago Press|isbn=978-0-226-06714-8}}
* {{cite book|last=Brown|first=Gene|title=Movie time: a chronology of Hollywood and the movie industry from its beginnings to the present|url=https://books.google.com/books?id=PrcUAQAAIAAJ|date=November 1, 1995|publisher=Macmillan|isbn=978-0-02-860429-9|ref=harv}}
* {{cite book|last=Carr|first=Jay|title=The A List: The National Society of Film Critics' 100 Essential Films|url=https://books.google.com/books?id=Ku4DJBYzPRgC&pg=PA48|date=January 2002|publisher=Da Capo Press|isbn=978-0-306-81096-1|ref=harv}}
* {{cite book |last=Cavell |first=Stanley |author1-link=Stanley Cavell |year=1981 |title=Pursuits of Happiness: The Hollywood Comedy of Remarriage |publisher=Harvard University Press |url=https://books.google.com/books?id=wws5ObJsUv0C |isbn=978-0-674-73906-2 |ref=harv}}
* {{cite book|last=Eliot |first=Marc |title=Cary Grant: A Biography |publisher=Harmony Books |location=New York |year=2004 |isbn=978-0-307-20983-2 |ref=harv}}
* {{cite book |editor-last=Hanson |editor-first=Patricia King |date=1993 |title=The American Film Institute Catalog of Motion Pictures Produced in the United States: Feature Films, 1931-1940 |location=Berkeley and Los Angeles |publisher=University of California Press |isbn=978-0-520-07908-3 |ref=harv}}
* {{cite book|last=Laham|first=Nicholas|title=Currents of Comedy on the American Screen: How Film and Television Deliver Different Laughs for Changing Times|url=https://books.google.com/books?id=CsqL5zpWX7cC&pg=PA29|date=January 1, 2009|publisher=McFarland|isbn=978-0-7864-5383-2|ref=harv}}
* {{cite book|last=Leaming |first=Barbara |title=Katharine Hepburn |publisher=Crown Publishers, Inc. |location=New York |year=1995 |isbn=978-0-87910-293-7 |ref=harv}}
* {{cite book|last=Mast |first=Gerald |title=Bringing Up Baby. Howard Hawks, director |publisher=Rutgers University Press |location=New Brunswick and London |year=1988 |isbn=978-0-8135-1341-6 |ref=harv}}
* {{cite book|last=McCarthy |first=Todd |title=Howard Hawks: The Grey Fox of Hollywood |publisher=Grove Press |location=New York |year=1997 |isbn=978-0-8021-3740-1 |ref=harv}}
* {{cite book|last=Russo|first=Vito|authorlink=Vito Russo|title=The Celluloid Closet: Homosexuality in the Movies|url=https://books.google.com/books?id=f6YwSZlsyJMC|date=September 20, 1987|publisher=HarperCollins|isbn=978-0-06-096132-9|ref=harv}}
{{Refend}}

==Further reading==
* {{cite book|last=Swaab|first=Peter|title=Bringing Up Baby|url=https://books.google.com/books?id=qb8MAAAACAAJ|date=January 4, 2011|publisher=British Film Institute|isbn=978-1-84457-070-6}}

==External links==
{{wikiquote}}
{{commons category|Bringing Up Baby}}
* {{IMDb title|0029947|Bringing Up Baby}}
* {{Allmovie title|7142|Bringing Up Baby}}
* {{tcmdb title|568|Bringing Up Baby}}
* {{AFI film|id=6702|title=Bringing Up Baby}}
* [http://www.pbs.org/wnet/americanmasters/database/grant_c.html Pauline Kael analysis]
* [http://www.moviediva.com/MD_root/reviewpages/MDBringingUpBaby.htm ''Bringing Up Baby'' at moviediva]
* [http://www.carygrant.net/reviews/baby.html Reprints of historic reviews, photo gallery at CaryGrant.net]
* [http://ia600409.us.archive.org/33/items/Romance_339/Romance45-07-24114BringingUpBaby.mp3 ''Bringing Up Baby''] on Theater of Romance: July 24, 1945

{{Howard Hawks}}
{{Dudley Nichols}}

[[Category:1938 films]]
[[Category:1930s romantic comedy films]]
[[Category:American romantic comedy films]]
[[Category:American films]]
[[Category:American screwball comedy films]]
[[Category:American black-and-white films]]
[[Category:Comedy of remarriage films]]
[[Category:English-language films]]
[[Category:Films directed by Howard Hawks]]
[[Category:Films set in Connecticut]]
[[Category:RKO Pictures films]]
[[Category:United States National Film Registry films]]
[[Category:Screenplays by Dudley Nichols]]
[[Category:Films produced by Cliff Reid]]