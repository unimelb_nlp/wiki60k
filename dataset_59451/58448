{{Italic title}}
The '''''Journal of Purchasing and Supply Management''''' is a quarterly [[peer review|peer-reviewed]] [[academic journal]] published by [[Elsevier]]. It covers research in the field of [[purchasing]] and [[Supply management (procurement)|supply management]].<ref>{{cite web |title=About the journal |work=Journal of Purchasing and Supply Management |url=http://www.elsevier.com/wps/find/journaldescription.cws_home/642704/description#description/ |publisher=Elsevier}}</ref> The journal also publishes a yearly special issue containing selected papers from the annual meeting of the [[International Purchasing & Supply Education & Research Association]]. It was established in 1994 as the ''European Journal of Purchasing & Supply Management''.<ref>{{cite web |url=http://www.sciencedirect.com/science/journal/09697012 |title=European Journal of Purchasing & Supply Management |publisher=Elsevier |work=[[ScienceDirect]] |accessdate=2011-02-27}}</ref> The [[Editor-in-chief|editors-in-chief]] are Alessandro Ancarani and George A. Zsidisin. Previous editors are Finn Wynstra, Christine Harland, and the founding editor, Richard Lamming.

== Abstracting and indexing ==
The ''Journal of Purchasing and Supply Management'' is abstracted and indexed by [[Scopus]] and the [[Social Sciences Citation Index]].

== References ==
{{Reflist}}

== External links ==
* {{Official|http://www.elsevier.com/wps/find/journaldescription.cws_home/642704/description#description/}}

[[Category:Business and management journals]]
[[Category:Publications established in 1994]]
[[Category:Elsevier academic journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]