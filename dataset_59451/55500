{{Italic title}}
{{Infobox Journal
|title        = American Psychologist
|cover = [[File:Cover_Image_for_American_Psychologist,_Vol_71_Correct_Size.jpg.gif]]
|editor       = Anne E. Kazak
|discipline   = [[Psychology]]
|publisher    = [[American Psychological Association]]
|country      = United States
|frequency    = 9/year
|history      = 1946–present
|impact       = 5.454
|abbreviation = Am. Psychol.
|impact-year  = 2015
|website      = http://www.apa.org/pubs/journals/amp/index.aspx
|link1        = http://psycnet.apa.org/index.cfm?fa=browsePA.volumes&jcode=amp
|link1-name   = Online access
|link2        = 
|link2-name   = 
|RSS          = http://content.apa.org/journals/amp.rss
|OCLC         = 1435230
|ISSN         = 0003-066X
|eISSN        = 1935-990X
}}
'''''American Psychologist''''' is the official [[peer-reviewed]] [[academic journal]] of the [[American Psychological Association]]. The journal publishes timely high-impact articles of broad interest. Papers include empirical reports and scholarly reviews covering science, practice, education, and policy.<ref>{{Cite journal|last=Kazak|first=A.E.|date=2016|title=Opening Editorial 2016: Changes in scope and structure.|url=|journal=American Psychologist|volume=71|issue=1|pages=1–2|doi=10.1037/a0039995|pmid=26766761|access-date=}}</ref>

The current [[editor-in-chief]] is Anne E. Kazak, PhD, ABPP.

==Abstracting and indexing==
According to the [[Journal Citation Reports]], the journal's 2015 [[impact factor]] is 5.454, ranking it 10th out of 129 journals in the category "Psychology, Multidisciplinary."<ref name=WoS2>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Psychology, Multidisciplinary |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2016-06-27 |series=Web of Science |postscript=.}}</ref>

== Special issues == 
The journal has published several special issues with content especially pertinent to the current events of the time. Some of the special issues include:<ref>{{cite web |date=9 January 2013| url=http://www.apa.org/pubs/journals/amp/index.aspx |title=American Psychologist|publisher=[[American Psychological Association]] |accessdate=2013-01-09}}</ref>

* Aging in America (May–June 2016)
* School Bullying and Victimization (May–June 2015)
* Cancer and Psychology (February–March 2015)
* Peace Psychology (October 2013)
* 9/11 Ten Years Later (September, 2011)
* Psychology and Global Climate Change (May–June, 2011)

== See also ==
* [[Developmental Psychology (journal)|Developmental Psychology]]
* [[Journal of Abnormal Psychology]]
* [[Journal of Experimental Psychology]]
* [[Journal of Personality and Social Psychology]]

== References ==
{{reflist}}

==External links==
* {{Official website|http://www.apa.org/pubs/journals/amp/index.aspx}}

[[Category:English-language journals]]
[[Category:Publications established in 1946]]
[[Category:American Psychological Association academic journals]]
[[Category:Psychology journals]]
[[Category:Psychotherapy journals]]