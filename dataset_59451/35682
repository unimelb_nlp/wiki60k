{{Infobox journal
| title = The Accounting Review
| cover = [[File:Acc Rev cover.gif]]
| editor = Mark L. DeFond
| discipline = [[Accounting]]
| abbreviation = Account. Rev.
| publisher = [[American Accounting Association]]
| country = United States
| frequency = Bimonthly
| history = 1926–present
| openaccess =
| license =
| impact = 2.319
| impact-year = 2012
| website = http://aaahq.org/pubs/acctrev.htm
| link1 = http://aaajournals.org/toc/accr/current
| link1-name = Online access
| link2 = http://aaajournals.org/loi/accr
| link2-name = Online archive
| JSTOR = 00014826
| OCLC = 865227376
| LCCN = 29008222
| CODEN = ACRVAS
| ISSN = 0001-4826
| eISSN = 1558-7967
}}
'''''The Accounting Review''''' is a bimonthly [[peer-reviewed]] [[academic journal]] published by the [[American Accounting Association]] (AAA) that covers [[accounting]] with a scope encompassing any accounting-related subject and any [[Accounting research|research]] methodology.''The Accounting Review'' is one of the oldest accounting journals, and recent studies considered it to be one of the leading academic journals in accounting.

''The Accounting Review'' was established in 1926. In its early decades, the journal tended to publish articles that would be of interest to [[Accountant|accounting practitioners]], but over time it shifted towards a preference for [[mathematical model|quantitative model]] building and [[mathematical rigor]]. In the 1980s the AAA began to publish two other journals, ''Issues in Accounting Education'' and ''Accounting Horizons'', that were more relevant to accounting educators and accounting practitioners respectively, to allow ''The Accounting Review'' to focus more heavily on [[Quantitative research|quantitative]] articles.

== Overview and history ==
''The Accounting Review'' is a bimonthly peer-reviewed academic journal covering accounting,<ref name="Editorial Policy" /> and is the flagship journal of the American Accounting Association.<ref>{{cite web |title=Prof appointed senior editor of journal |url=http://www.utexas.edu/know/2007/11/15/kachelmeier-accolade/ |publisher=The University of Texas at Austin |accessdate=3 February 2014}}</ref><ref>{{cite web |last=Aubin |first=Dena |title=Accounting research needs a jolt - Waymire |url=http://aaahq.org/newsroom/Reuters_AccountingResearchNeedsJolt05_2012.pdf |publisher=American Accounting Association |accessdate=3 February 2014}}</ref><ref name=HJ /> Its current Senior Editor is Mark L. DeFond (University of Southern California).<ref name="Main">{{cite web |title = The Accounting Review|url = http://aaahq.org/research/aaa-journals/the-accounting-review|publisher = American Accounting Association|accessdate = 21 January 2016}}</ref> The journal's scope encompasses any accounting-related subject and any research methodology:<ref name="Editorial Policy">{{cite web |title=Editorial Policy and Style Information |url=http://www.allentrack.net/AAA/Editorial_Policies/ACCR.pdf |accessdate=13 January 2014}}</ref> as of 2010 the proportions of papers accepted for publication across subject areas and research methods was very similar to the proportion of papers received for review.<ref name="K 2010">{{cite journal |last=Kachelmeier |first=Steven J. |title=Annual Report and Editorial Commentary for The Accounting Review |journal=The Accounting Review |year=2010 |volume=85 |issue=6 |pages=2173–2203 |doi=10.2308/accr.00000003}}</ref>

Submissions to ''The Accounting Review''  are reviewed by [[editorial board]] members and ''ad hoc'' reviewers. In 2009, the journal received over 500 new submissions a year, and about 9% of the decision letters sent to authors were acceptances or conditional acceptances.<ref name="K 2010" />

===Establishment to 1960s===
''The Accounting Review'', launched in 1926 by [[William Andrew Paton]],<ref name="Paton Chicago Tribune">{{cite web |title=Accounting Pioneer, Teacher William A. Paton |url=http://articles.chicagotribune.com/1991-05-01/news/9102080479_1_american-accounting-association-certified-public-accountants-mr-paton |work=Chicago Tribune |accessdate=3 February 2014}}</ref> is one of the oldest academic journals in accounting.<ref name="FGT 1991">{{cite journal |last=Fleming |first=Robert J. |first2=Samuel P. |last2=Grace |first3=Joel E. |last3=Thompson |title=Tracing the Evolution of Research in the Accounting Review through its Leading Authors: the 1946-1965 Period |journal=The Accounting Historians Journal |year=1991 |volume=18 |issue=2 |pages=27–53 |jstor=40698054 }}</ref><ref>{{cite journal |title=William A. Paton Honored by AICPA as Outstanding Educator of the Century |journal=Dividend |year=1988 |volume=19 |issue=1 |page=17 |url=https://books.google.com/books?id=4Uo8AAAAMAAJ&pg=PA17 |editor1-first=Pringle |editor1-last=Smith}}</ref> The American Association of University Instructors of Accounting, which later became the American Accounting Association, originally proposed that the association publish a ''Quarterly Journal of Accountics'', but the proposal did not see fruition, and ''The Accounting Review'' was subsequently born.<ref name=HJ>{{cite journal |last=Heck |first=Jean L. |title=An Analysis of the Evolution of Research Contributions by The Accounting Review, 1926-2005 |page=109 |journal=Accounting Historians Journal |year=2007 |volume=34 |url=http://www.trinity.edu/rjensen/395wptar/web/tar395wp.htm |accessdate=13 January 2014 |issue=2}}</ref> Paton served as editor and production manager in the journal's first three years.<ref>{{cite web |title=William A. Paton |url=http://fisher.osu.edu/departments/accounting-and-mis/the-accounting-hall-of-fame/membership-in-hall/william-a.-paton/ |publisher=Fisher College of Business |accessdate=3 February 2014}}</ref>

In the first few decades following the journal's establishment, leading authors in ''The Accounting Review'' tended to write articles that would be of interest to [[Accountant|accounting practitioners]].<ref name = "FGT 1991" /> The journal published articles that focused on accounting [[education]] and issues related to particular [[Industry|industries]] and [[Trade association|trade groups]], with many articles using [[anecdotal evidence]] and hypothetical illustrations.<ref name=HJ /> The longest-serving editor during this period was [[Eric Kohler]], an accounting practitioner;<ref name=HJ /> Kohler served as editor from 1928 to 1942.<ref>{{cite web |title=Eric Louis Kohler |url=http://fisher.osu.edu/departments/accounting-and-mis/the-accounting-hall-of-fame/membership-in-hall/eric-louis-kohler/ |publisher=Fisher College of Business |accessdate=3 February 2014}}</ref>

From the 1940s to the 1960s, ''The Accounting Review'' published articles of greater diversity, and leading authors during this period tended to have less practical accounting experience and more formal education. During this period, the three individuals that accounted for most of the editorial duties of the journal were [[A. C. Littleton]] (1944-1947), Frank Smith (1950-1959) and Robert Mautz (1960-1962), all of whom either had practical accounting experience, or were leading authors prior to 1945, when the journal was oriented towards the accounting practice.<ref name = "FGT 1991" />

===1960s to present===
In the 1960s, the journal shifted towards a preference for [[mathematical model|quantitative model]] building including [[econometric]] models and [[time series]] models, and accepted more articles by non-accountants who contributed ideas from other disciplines in solving accounting-related problems. Since the late 1970s, accounting [[professor]]s have opined that the journal was sacrificing relevance for mathematical rigor, and by 1982, accounting researchers realized that mathematical analysis and empirical research were a necessary condition for articles to be accepted.<ref name=HJ />

In the 1980s, the AAA began to publish two other journals, ''Issues in Accounting Education'' and ''Accounting Horizons''. ''Issues in Accounting Education'', first published in 1983, was created to better serve accounting educators, while ''Accounting Horizons'', first published in 1987, focused more on issues facing accounting practitioners. This permitted the journal "to focus more heavily on quantitative papers that became increasingly difficult for practitioners and many teachers of accounting to comprehend".<ref name=HJ />

Between the 1980s and the 2000s, with the rise of databases such as [[Compustat]] and [[EDGAR]] and software such as [[SAS (software)|SAS]], articles became mathematically more rigorous with increasingly sophisticated [[statistical]] analyses, and accounting practitioners comprised a decreasing proportion of authors in the journal.<ref name=HJ />

== Impact ==
According to the ''[[Journal Citation Reports]]'', the journal had a 2012 [[impact factor]] of 2.319, ranking it 6th out of 89 journals in the category "Business, Finance".<ref name=WoS>{{cite book |year=2013 |chapter=Journals Ranked by Impact: Business, Finance |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref> Recent studies on accounting research and on [[Doctor of Philosophy|doctoral programs]] in accounting considered ''The Accounting Review'' to be one of six leading accounting journals,<ref>{{cite journal |last=Oler |first=Derek K. |first2=Mitchell J. |last2=Oler |first3=Christopher J. |last3=Skousen |title=Characterizing Accounting Research |journal=Accounting Horizons |year=2010 |volume=24 |issue=4 |pages=635–670 |doi=10.2308/acch.2010.24.4.635}}</ref><ref>{{cite journal |last=Coyne |first=Joshua G. |author2=Scott L. Summers |author3=Brady Williams |author4=David A. Wood  |title=Accounting Program Research Rankings by Topical Area and Methodology |journal=Issues in Accounting Education |year=2010 |volume=25 |issue=4 |pages=631–654 |url=http://www.emeraldinsight.com/bibliographic_databases.htm?id=17002430 |accessdate=20 January 2014 |doi=10.2308/iace.2010.25.4.631}}</ref><ref>{{cite journal |last=Stephens |first=Nathaniel M. |first2=Scott L. |last2=Summers |first3=Brady |last3=Williams |first4=David A. |last4=Wood |title=Accounting Doctoral Program Rankings Based on Research Productivity of Program Graduates |journal=Accounting Horizons |year=2011 |volume=25 |issue=1 |pages=149–181 |doi=10.2308/acch.2011.25.1.149}}</ref> and it is also one of the journals used by the ''[[Financial Times]]'' to compile its business school research rank.<ref name="FT 45 Journals">{{cite web |title=45 Journals used in FT Research Rank |url=http://www.ft.com/intl/cms/s/2/3405a512-5cbb-11e1-8f1f-00144feabdc0.html |work=Financial Times |accessdate=13 January 2014}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in the [[Social Sciences Citation Index]], [[Current Contents]]/Social & Behavioral Sciences,<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2014-01-31}}</ref> and [[Scopus]].<ref name=Scopus>{{cite web |url=http://cdn.elsevier.com/assets/excel_doc/0003/148548/title_list.xlsx |title=Scopus title list |format=[[Microsoft Excel]] |publisher=[[Elsevier]] |work= |accessdate=2014-01-31}}</ref>

== References ==
{{reflist |30em}}

== External links==
* {{Official website |http://aaajournals.org/}}

{{Good article}}

{{DEFAULTSORT:Accounting Review, The}}
[[Category:Publications established in 1926]]
[[Category:Accounting journals]]
[[Category:English-language journals]]
[[Category:Academic journals published by learned and professional societies]]
[[Category:Bimonthly journals]]