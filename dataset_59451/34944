{{good article}}
{{Infobox NCAA team season
| Year = 1943
| Team = Alabama Crimson Tide
| Conference = Southeastern Conference
| ShortConference = SEC
| Record = 0–0
| ConfRecord = 0–0
| HeadCoach = [[Frank Thomas (American football)|Frank Thomas]]
}}
{{1943 SEC football standings}}
The '''1943 Alabama Crimson Tide football team''' was to represent the [[University of Alabama]] in the [[1943 college football season]]; however, the season was canceled due to the effects of [[World War II]]. In February 1943, the Army instituted a policy that prohibited their cadets from participation in intercollegiate athletics. Unsure if a season would occur, head coach [[Frank Thomas (American football)|Frank Thomas]] proceeded through spring practice as if it would be played. By summer, only two Alabama players were available to compete on the squad as a result of the Army prohibition on its trainees competing in intercollegiate athletics, and on August 23, 1943, the University announced its decision to cancel the 1943 season. The cancellation marked only the third time since the inaugural [[1892 Alabama Cadets football team|1892 season]] that Alabama did not field a football team.

Although not officially sanctioned by the University, an independent team called the '''Alabama Informals''' was organized in October 1943. Coached by former Crimson Tide player [[Mitchell Olenski]], the Informals were composed of 17-year-old and draft deferred students ineligible for military service. The Informals were allowed to play their games at [[Bryant–Denny Stadium|Denny Stadium]] and utilize the equipment of the Crimson Tide football team. The squad lost to {{cfb link|year=1943|team=Howard Bulldogs|title=Howard}}, defeated the [[Marion Military Institute]] twice and finished the season with an overall record of two wins and one loss (2–1).

At the conclusion of the season, SEC officials met in an effort to bring a full football schedule back for the 1944 season. By May 1944, all SEC schools, with the exception of Vanderbilt, indicated they would field teams for the 1944 season. Football officially returned on September 30, 1944, when the Crimson Tide played LSU to a tie in their season opener.

{{TOC limit|3}}

==1943 Crimson Tide==
In February 1943, the [[United States Department of War]] announced they would take over both classroom space and athletic facilities at 271 colleges and universities to be utilized for the training of [[United States Army]] soldiers.<ref name="DoW1">{{cite news |title=Army forecasts abandonment of college football |first=Bob |last=Considine |agency=International News Service |page=13 |url=https://news.google.com/newspapers?id=VE1PAAAAIBAJ&sjid=c00DAAAAIBAJ&pg=7231%2C4857950 |newspaper=St. Petersburg Times |publisher=Google News |date=February 13, 1943 |accessdate=November 26, 2012}}</ref><ref name="DoW2">{{cite news |title=College leaders are grimly determined to carry on |first=Harold |last=Claassen |agency=Associated Press |page=B1 |url=https://news.google.com/newspapers?id=DActAAAAIBAJ&sjid=r9QFAAAAIBAJ&pg=1984%2C2284079 |newspaper=The Miami News |publisher=Google News |date=February 13, 1943 |accessdate=November 26, 2012}}</ref> As part of the Department's order, only students under 18 years of age or those with [[4-F (US Military)|4-F draft classifications]] were permitted to compete in intercollegiate athletics.<ref name="DoW1"/><ref name="DoW2"/> At the time of the announcement, coach Thomas was quoted as saying:<blockquote>"Army–Navy programs not figured at all in our plans for athletics next fall."<ref name="DoW2"/></blockquote>As such, preparations continued towards fielding a team for the 1943 season. On March 8, spring practice commenced at Denny Stadium and 55 student-athletes reported the first day.<ref name="Practice1">{{cite news |title=Tiders start spring drills |page=11 |url=https://news.google.com/newspapers?id=WNU-AAAAIBAJ&sjid=wkwMAAAAIBAJ&dq=alabama%20football&pg=6341%2C2655696 |newspaper=The Tuscaloosa News |publisher=Google News |date=March 7, 1943 |accessdate=November 26, 2012}}</ref> At that time coach Thomas acknowledged he did not know how many of his players would be eligible to play in the fall due to rules the prohibited active-duty servicemen playing intercollegiate football.<ref name="Practice1"/> As they entered practice, only 15 [[Letterman (sports)|lettermen]] returned to the squad from the [[1942 Alabama Crimson Tide football team|1942 team]]. These players included: Jack Aland, Johnny August, Bill Baughman, Andy Bires, Charley Compton, [[Ted Cook (American football)|Ted Cook]], Leon Fichman, Ted McKosky, Jim McWhorter, Norman Mosley, Mitchell Olenski, Kenny Reese, Lou Scales, John Staples and [[Don Whitmire]].<ref name="Practice1"/> Two weeks into the practices, coach Thomas held the first [[Exhibition game|scrimmage]] of the spring on March 22.<ref name="Practice2">{{cite news |title=Tiders hold scrimmage drill |page=7 |url=https://news.google.com/newspapers?id=ZtU-AAAAIBAJ&sjid=wkwMAAAAIBAJ&pg=6376%2C3258143 |newspaper=The Tuscaloosa News |publisher=Google News |date=March 23, 1943 |accessdate=November 26, 2012}}</ref>

On March 26, Herbert Chapman, Billy DeWitt, James Grantham, Henry "Red" Jones, Jim McWhorter and Lou Scales became the first Alabama players to be called into active duty from the enlisted reserve corps.<ref name="Enlisted1">{{cite news |title=Six Tide gridders called to Army |page=7 |url=https://news.google.com/newspapers?id=adU-AAAAIBAJ&sjid=wkwMAAAAIBAJ&pg=5545%2C3375605 |newspaper=The Tuscaloosa News |publisher=Google News |date=March 26, 1943 |accessdate=November 26, 2012}}</ref> As they were now enlisted as active servicemen, they were all ineligible to play in the fall for the Crimson Tide.<ref name="Enlisted1"/> In June, the SEC developed a plan to allow its member schools to discontinue athletic teams due to the war efforts, but retain the overall structure of the conference.<ref name="SEC1">{{cite news |title=Southeastern heads talking of duration conference freeze |agency=Associated Press |page=9 |url=https://news.google.com/newspapers?id=lbtSAAAAIBAJ&sjid=gX0DAAAAIBAJ&pg=4684%2C6609986 |newspaper=The Evening Independent |publisher=Google News |date=June 26, 1943 |accessdate=November 26, 2012}}</ref>

By August, the prospect of Alabama fielding a football team for the 1943 season was in doubt. On August 17, coach Thomas spoke to a civic group in [[Birmingham, Alabama|Birmingham]] and stated he did not think the school would field a team in 1943 due to the unwillingness of the Army to change their policy that prohibited their cadets from participating in intercollegiate athletics.<ref name="Disband1">{{cite news |title=Alabama on verge of abandoning grid for duration |agency=United Press |page=6 |url=https://news.google.com/newspapers?id=6oxJAAAAIBAJ&sjid=LQwNAAAAIBAJ&pg=1290%2C97301 |newspaper=The News and Courier |publisher=Google News |date=August 8, 1943 |accessdate=November 26, 2012}}</ref> On August 23, 1943, the University Physical Education and Athletics Committee officially canceled the 1943 season.<ref name="WWII">{{cite news |title=Alabama withdraws |agency=Associated Press |page=18 |url=https://news.google.com/newspapers?id=D38tAAAAIBAJ&sjid=BpkFAAAAIBAJ&pg=4509%2C4230686 |newspaper=The Gazette |location=Montreal, Quebec |publisher=Google News |date=August 27, 1943 |accessdate=November 26, 2012}}</ref><ref name="WWII2">{{cite news |title=Intercollegiate football abandoned at University |page=1 |url=https://news.google.com/newspapers?id=olQ_AAAAIBAJ&sjid=_UwMAAAAIBAJ&pg=2377%2C2159822 |newspaper=The Tuscaloosa News |publisher=Google News |date=August 23, 1943 |accessdate=November 26, 2012}}</ref> The decision was made at that time because only two Alabama players were available to compete on the squad.<ref name="WWII2"/> As the season was canceled, coach Thomas spent his time leading [[war bond]] drives and serving as president of the Tuscaloosa Exchange Club during the time the season was originally scheduled.<ref name="Thomas3">{{cite book |last=Scott |first=Richard |title=Legends of Alabama Football |year=2013 |publisher=Skyhorse Publishing |isbn= 9781613216989 |url=https://books.google.com/books?id=yFBlAgAAQBAJ&lpg=PT37&dq=1943%20ALabama%20football%20season&pg=PT37#v=onepage&q=1943%20ALabama%20football%20season&f=false |accessdate=March 13, 2014}}</ref> The cancellation marked only the third time since 1892 that Alabama did not field a football team. The only other seasons the Crimson Tide did not field teams were in [[1898 Alabama Crimson Tide football team|1898]] due to University policy that prohibited athletic teams from traveling off campus to compete and again in [[1918 Alabama Crimson Tide football team|1918]] due to the effects of [[World War I]].<ref name="WWII"/>

===Schedule===
At the time of the cancellation of the season, Alabama had four games scheduled: a pair against [[1943 LSU Tigers football team|LSU]], and one each against [[1943 Tulane Green Wave football team|Tulane]] and [[1943 Georgia Bulldogs football team|Georgia]].<ref name="WWII2"/>
{{CFB Schedule Start | time = no | rank = no | ranklink = no | rankyear = no | tv = no | attend = no }}
{{CFB Schedule Entry
| w/l          = 
| date         = September 25
| away         = yes
| time         = no
| rank         = no
| opponent     = [[1943 LSU Tigers football team|LSU]]
| site_stadium = [[Tiger Stadium (LSU)|Tiger Stadium]]
| site_cityst  = [[Baton Rouge, Louisiana|Baton Rouge, LA]]
| gamename     = [[Alabama–LSU football rivalry|Rivalry]]
| tv           = no
| score        = N/A
}}
{{CFB Schedule Entry
| w/l          = 
| date         = October 16
| away         = yes
| time         = no
| rank         = no
| opponent     = [[1943 Tulane Green Wave football team|Tulane]]
| site_stadium = [[Tulane Stadium]]
| site_cityst  = [[New Orleans|New Orleans, LA]]
| tv           = no
| score        = N/A
}}
{{CFB Schedule Entry
| w/l          = 
| date         = October 23
| time         = no
| rank         = no
| opponent     = [[1943 Georgia Bulldogs football team|Georgia]]
| site_stadium = [[Legion Field]]
| site_cityst  = [[Birmingham, Alabama|Birmingham, AL]]
| tv           = no
| score        = N/A
}}
{{CFB Schedule Entry
| w/l          = 
| date         = November 13
| time         = no
| rank         = no
| opponent     = [[1943 LSU Tigers football team|LSU]]
| site_stadium = Legion Field
| site_cityst  = Birmingham, AL
| gamename     = [[Alabama–LSU football rivalry|Rivalry]]
| tv           = no
| score        = N/A
}}
{{CFB Schedule End
| rank     = no
| timezone = 
| hc       = no
}}

===NFL Draft===
Several players that were [[Letterman (sports)|varsity lettermen]] from the 1942 squad, scheduled to play as part of the 1943 team, were drafted into the [[National Football League Draft|National Football League (NFL)]] in the 1944 draft.<ref>{{cite web |url=http://www.pro-football-reference.com/colleges/alabama/drafted.htm |title=Alabama Drafted Players/Alumni |accessdate=April 7, 2012 |work=Sports Reference, LLC |publisher=Pro-Football-Reference.com}}</ref><ref name="NFLDraft">{{cite web |publisher=National Football League | url=http://www.nfl.com/draft/history/fulldraft?abbr=A&collegeName=Alabama&abbrFlag=0&type=school | title=Draft History by School–Alabama|accessdate=March 16, 2013}}</ref> These players included the following:
{| class="wikitable sortable" style="text-align:center"
! scope="col" | Year
! scope="col" | Round
! scope="col" | Overall
! scope="col" | Player name
! scope="col" | Position
! scope="col" | NFL team
|-
| rowspan=5|[[1944 NFL Draft|1944]]
| 9 
| 78 
! {{Sortname|Mitchell|Olenski}}
| Tackle 
| [[1944 Brooklyn Tigers season|Brooklyn Tigers]]
|-
| 9 
| 82 
! {{Sortname|Don|Whitmire}}
| Tackle 
| [[1944 Green Bay Packers season|Green Bay Packers]]
|-
| 22
| 221
! {{Sortname|Ted|Cook|Ted Cook (American football)}}
| End
| Brooklyn Tigers
|-
| 27
| 279
! {{Sortname|Andy|Bires|nolink=1}}
| End
| [[1944 New York Giants season|New York Giants]]
|-
| 27
| 281
! {{Sortname|Jack|McKewan|nolink=1}}
| Tackle
| [[1944 Chicago Bears season|Chicago Bears]]
|}

==Alabama Informals==
{{Infobox NCAA team season
| Year = 1943
| Prev year=none
| Next year=none
| Team = Alabama Informals
| Conference = Independent
| ShortConference = Independent
| Record = 2–1
| ConfRecord = 2–1
| HeadCoach = [[Mitchell Olenski]]
| StadiumArena = [[Bryant–Denny Stadium|Denny Stadium]]
}}
Although Alabama officially did not participate as part of the 1943 college football season, a team composed of 17-year old and draft deferred students was organized as the Alabama Informals in October 1943.<ref name="AI1">{{cite news |title=New colleges set grid hopes |page=7 |url=https://news.google.com/newspapers?id=KN0-AAAAIBAJ&sjid=AU0MAAAAIBAJ&pg=6456%2C439303 |newspaper=The Tuscaloosa News |agency=Associated Press |publisher=Google News |date=January 12, 1944 |accessdate=June 23, 2012}}</ref><ref name="AI2">{{cite news |title=Tide Informals prepare for chest charity game |page=11 |url=https://news.google.com/newspapers?id=2lQ_AAAAIBAJ&sjid=_UwMAAAAIBAJ&pg=6407%2C4786755 |newspaper=The Tuscaloosa News |agency=Associated Press |publisher=Google News |date=October 31, 1943 |accessdate=June 23, 2012}}</ref> Not officially sanctioned by the University, the Informals were allowed to utilize both equipment and the facilities of the Crimson Tide.<ref name="AI4">{{cite news |title=Howard to play Alabama Informals in chest game |page=1 |url=https://news.google.com/newspapers?id=2VQ_AAAAIBAJ&sjid=_UwMAAAAIBAJ&pg=3842%2C4690295 |newspaper=The Tuscaloosa News |publisher=Google News |date=October 29, 1943 |accessdate=November 26, 2012}}</ref> The team was led by head coach [[Mitchell Olenski]], and John Gresham and Al Alois served as assistant coaches.<ref name="AI4"/> At the time of its creation, the Alabama Informals squad was the second created by a SEC school forced to abandon their football team for the year after [[1943 Vanderbilt Commodores football team|Vanderbilt]].<ref name="AI5">{{cite news |title=Alabama resumes Informals football |page=6 |url=https://news.google.com/newspapers?id=MI1JAAAAIBAJ&sjid=LQwNAAAAIBAJ&pg=3187%2C7052606 |agency=Associated Press |newspaper=The News and Courier |publisher=Google News |date=October 30, 1943 |accessdate=November 26, 2012}}</ref>

===Schedule===
In addition to the three games played, the Informals were also scheduled to compete against Draper Prison at the [[Cramton Bowl]] on November 27.<ref name="DP1">{{cite news |title=Informal–Draper Prison game is cancelled |page=7 |url=https://news.google.com/newspapers?id=7lQ_AAAAIBAJ&sjid=_UwMAAAAIBAJ&pg=4761%2C5698858 |newspaper=The Tuscaloosa News |agency=Associated Press |publisher=Google News |date=November 23, 1943 |accessdate=June 23, 2012}}</ref> The game was canceled by University officials that stated the students on the team needed to focus on final examinations instead.<ref name="DP1"/>

{{CFB Schedule Start | time = no | rank = no | ranklink = no | rankyear = no | tv = no | attend = yes }}
{{CFB Schedule Entry
| w/l          = l
| date         = November 6
| time         = no
| rank         = no
| opponent     = {{cfb link|year=1943|team=Howard Bulldogs|title=Howard}}
| site_stadium = [[Bryant–Denny Stadium|Denny Stadium]]
| site_cityst  = [[Tuscaloosa, Alabama|Tuscaloosa, AL]]
| tv           = no
| score        = 6–42
| attend       = 7,000
}}
{{CFB Schedule Entry
| w/l          = w
| date         = November 12
| away         = yes
| time         = no
| rank         = no
| opponent     = [[Marion Military Institute]]
| site_stadium = Perry County H.S. Stadium
| site_cityst  = [[Marion, Alabama|Marion, AL]]
| tv           = no
| score        = 31–12
| attend       = &nbsp;
}}
{{CFB Schedule Entry
| w/l          = w
| date         = November 20
| time         = no
| rank         = no
| opponent     = Marion Military Institute
| site_stadium = Denny Stadium
| site_cityst  = Tuscaloosa, AL
| tv           = no
| score        = 19–13
| attend       = &nbsp;
}}
{{CFB Schedule End
| rank     = no
| timezone = 
| hc       = no
| ncg      = no
}}

===Game summaries===

====Howard====
{{AFB game box start
|Visitor='''Howard'''
|V1=14 |V2=7 |V3=7 |V4=14
|Host=Alabama
|H1=0 |H2=0 |H3=0 |H4=6
|Date=November 6
|Location=Denny Stadium<br/>Tuscaloosa, AL
|Attendance=7,000
}}
*'''Source:'''<ref name="How1">{{cite news |title=Seadogs slap Informals 42–6 as 7,000 see game |url=https://news.google.com/newspapers?id=4FQ_AAAAIBAJ&sjid=_UwMAAAAIBAJ&pg=6460%2C5071353 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 7, 1943 |page=11 |accessdate=June 23, 2012}}</ref>
{{AFB game box end}}
The Informals first opponent was a team composed of players that were part of the [[V-12 Navy College Training Program]] at Howard College (now [[Samford University]]).<ref>{{cite book |title=2011 Samford Football Media Guide |year=2011 |publisher=Samford University |location=Homewood, Alabama |page=152 |url=http://grfx.cstv.com/photos/schools/samf/sports/m-footbl/auto_pdf/2011-12/misc_non_event/2011_FB_MG_Records.pdf |accessdate=June 23, 2012 |format=PDF}}</ref> In the game, the Seadogs won 42–6 with the majority of gate receipts collected for the Tuscaloosa Service Center War Chest.<ref name="AI2"/><ref name="How1"/> The Howard squad featured two former Crimson Tide players: Bill Harris at tackle and Billy Dabbs at fullback.<ref name="How2">{{cite news |title=Former Tiders now Seadogs |url=https://news.google.com/newspapers?id=3FQ_AAAAIBAJ&sjid=_UwMAAAAIBAJ&pg=6448%2C4893973 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 2, 1943 |page=7 |accessdate=November 27, 2012}}</ref>

Howard scored six touchdowns in the game. Cecil Duffee and Billy Dabbs scored on runs of 19 and 16-yards in the first quarter; Duffee scored on a 34-yard reception from Tris Mock in the second quarter; Stephenson scored on an eight-yard run in the third quarter; and Charley Spier scored on a 40-yard [[interception]] return in the fourth quarter.<ref name="How1"/> The only Alabama points of the contest came in the fourth quarter, down 35–0 when John Wade threw a 10-yard touchdown pass to Barton Greer.<ref name="How1"/> Although not included as part of Alabama's all-time record, this is the only loss Alabama ever had against Howard.<ref name="How1"/>
{{clear}}

====at Marion====
{{AFB game box start
|Visitor='''Alabama'''
|V1=6 |V2=6 |V3=12 |V4=7
|Host=Marion
|H1=0 |H2=6 |H3=0 |H4=6
|Date=November 12
|Location=Perry County H.S. Stadium<br/>Marion, AL
}}
*'''Sources:'''<ref name="@M1">{{cite news |title=Informals top Marion Cadets |url=https://news.google.com/newspapers?id=51Q_AAAAIBAJ&sjid=_UwMAAAAIBAJ&pg=6615%2C5343016 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 15, 1943 |page=12 |accessdate=June 23, 2012}}</ref><ref name="@M2">{{cite news |title=Alabama's Informals score first victory |url=https://news.google.com/newspapers?id=6P1PAAAAIBAJ&sjid=ClUDAAAAIBAJ&pg=2678%2C1328525 |agency=Associated Press |publisher=Google News Archives |newspaper=The Evening Independent |date=November 13, 1943 |page=11 |accessdate=November 27, 2012}}</ref>
{{AFB game box end}}
After their loss against Howard, the Informals traveled to play the [[Marion Military Institute]] Cadets at Perry County High School Stadium.<ref name="AI3">{{cite web |url=http://mmiarchivist.blogspot.com/2009/04/when-mi-played-university-of-alabama-in.html |title=When MI played the University of Alabama in football! |date=April 27, 2009 |work=Marion Military Institute Archives |publisher=MMI Foundation |accessdate=June 23, 2012}}</ref> Against the Cadets, Alabama scored at least one touchdown in all four quarters for the 31–12 victory.<ref name="@M1"/><ref name="AI3"/> The Informals scored first on an 80-yard Barton Greer touchdown run, only to see the Cadets tie the game at 6–6 on the next possession on a 40-yard touchdown pass.<ref name="@M1"/> Alabama responded with a 20-yard touchdown run on a [[Reverse (American football)|reverse]] by Whitey Blanchiak to take a 12–6 halftime lead.<ref name="@M1"/> In the third quarter, the Informals extended their lead to 24–6 after Lowell Edmondson scored on a 35-yard touchdown pass from Greer and on a four-yard Frank MacAlpine touchdown run in the third quarter.<ref name="@M1"/> In the fourth, Greer scored on a run for Alabama and Marion scored the final points of the game on a [[Punt (gridiron football)|punt]] returned for a touchdown in their 31–12 loss.<ref name="@M1"/>
{{clear}}

====Marion====
{{AFB game box start
|Visitor=Marion
|V1=13 |V2=0 |V3=0 |V4=0
|Host='''Alabama'''
|H1=0 |H2=12 |H3=7 |H4=0
|Date=November 20
|Location=Denny Stadium<br/>Tuscaloosa, AL
}}
*'''Source:'''<ref name="M1">{{cite news |title=Informals rally to trip Cadets |url=https://news.google.com/newspapers?id=7FQ_AAAAIBAJ&sjid=_UwMAAAAIBAJ&pg=6487%2C5605724 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 21, 1943 |page=10 |accessdate=June 23, 2012}}</ref>
{{AFB game box end}}
After their victory over Marion, the Informals defeated the Cadets for the second consecutive week at Denny Stadium 19–13.<ref name="AI3"/><ref name="M1"/> After they took a 7–0 lead on an 85-yard drive, the Cadets extended it to 13–0 by the end of the first quarter when Jimmy Scruggs scored on a touchdown reception.<ref name="M1"/> The Informals responded with a pair of second-quarter touchdowns to make the halftime score 13–12. Touchdowns were scored on a Frank MacAlpine run and on a 21-yard Whitey Blanchiak reception from Barton Greer.<ref name="M1"/> Alabama then scored the go-ahead touchdown in the third quarter on a 55-yard [[interception]] return for a touchdown.<ref name="M1"/>
{{clear}}

===Roster===
{| class="toccolours" style="text-align: left;"
|-
! colspan="9" style="background:#A32638; color:white; text-align:center;"|'''Alabama Informals 1943 roster'''<ref name="Roster">{{cite news |title=Alabama Informals' roster |url=https://news.google.com/newspapers?id=3FQ_AAAAIBAJ&sjid=_UwMAAAAIBAJ&pg=6607%2C4894759 |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=November 2, 1943 |page=7 |accessdate=November 27, 2012}}</ref>
|-
|
| style="font-size:95%; vertical-align:top;"| '''Quarterbacks'''
* Farris Deep
* Tom Edwards
* Dwight Evens
* Sam Oliveri

'''Running backs'''
* Whitey Blanciak
* Jim Callahan
* Don Dahlene
* Lowell Edmondson
* Barton Greer
* Joe Marion
* Frank McAlpine
* Billy Mills
* John Wade
* Bob Woolridge

| style="width: 25px;" |
|  style="font-size:95%; vertical-align:top;"| '''Ends'''
* Pete Crow
* Herman Dow
* Al Judd
* Bill Monheimer
* William Stanton

'''Tackles'''
* George Kachickas
* Carl Licht
* Read Northern
* Nick Terlizzi
* Joe Triolo

'''Guards'''
* Oscar Jones
* Shorty Lackie
* Lorrin Loeb
* Bob Okin
* Gus Ross
* Art Sizemore

| style="width: 25px;" |
|  style="font-size:95%; vertical-align:top;"| '''Center'''
* Bert Bertini
* Hudon Conway

'''Coaching staff'''
* Mitchell Olenski – Head coach
* Al Alois – Assistant coach
* John Gresham – Assistant coach

|}

==Aftermath==
On December 10, 1943, SEC officials met in [[Nashville, Tennessee|Nashville]] in an effort to bring a full football schedule back for the 1944 season.<ref name="1944One">{{cite news |title=SEC scraps peace-time eligibility restrictions: Football revival hinted |url=https://news.google.com/newspapers?id=xEcsAAAAIBAJ&sjid=CcsEAAAAIBAJ&pg=6391%2C3263102 |agency=Associated Press |publisher=Google News Archives |newspaper=The Spartanburg Herald |date=December 11, 1943 |page=6 |accessdate=November 27, 2012}}</ref> In order to have enough students eligible to participate on a team, the SEC changed its eligibility restrictions to allow for any civilian to play as long as they had not played four years of college football or professionally.<ref name="1944One"/> At that time, Alabama along with [[Tennessee Volunteers football|Tennessee]] and [[Vanderbilt Commodores football|Vanderbilt]] indicated they might reform their respective teams and resume conference play for the 1944 season.<ref name="1944One"/><ref name="1944resume">{{cite news |title=Grid resumption seen for SEC |page=7 |url=https://news.google.com/newspapers?id=DlU_AAAAIBAJ&sjid=_UwMAAAAIBAJ&pg=5688%2C7386513 |newspaper=The Tuscaloosa News |agency=Associated Press |publisher=Google News |date=December 7, 1943 |accessdate=November 26, 2012}}</ref> On January 12, 1944, all SEC members, with the exceptions of [[Ole Miss Rebels football|Mississippi]] and [[Mississippi State Maroons football|Mississippi State]], indicated their intent to field football teams the following fall.<ref name="1944Two">{{cite news |title=Signs point to revival of Dixie gridiron loop |url=https://news.google.com/newspapers?id=euleAAAAIBAJ&sjid=71MNAAAAIBAJ&pg=3418%2C2744767 |agency=Associated Press |publisher=Google News Archives |newspaper=The Youngstown Daily Vindicator |date=January 12, 1943 |page=11 |accessdate=November 27, 2012}}</ref> On May 19, 1944, every SEC school with the exception of Vanderbilt (who fielded an informal team) agreed to play a full conference schedule the following fall.<ref name="1944resume2">{{cite news |title=South to field 11 grid teams |page=7 |url=https://news.google.com/newspapers?id=kd0-AAAAIBAJ&sjid=AU0MAAAAIBAJ&pg=5889%2C5641511 |newspaper=The Tuscaloosa News |agency=Associated Press |publisher=Google News |date=May 19, 1944 |accessdate=November 26, 2012}}</ref> Football officially returned to Alabama for the first time since the 1942 season on September 30, 1944, when the Crimson Tide played LSU to a tie in their season opener.<ref name="LSU1">{{cite news |title=Crimson given outside chance |url=https://news.google.com/newspapers?id=2OU-AAAAIBAJ&sjid=E00MAAAAIBAJ&pg=6468%2C3490229 |agency=Associated Press |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=September 27, 1944 |page=7 |accessdate=March 3, 2014}}</ref><ref name="LSU2">{{cite news |title=Alabama–LSU grid battle ends in 27–27 deadlock |url=https://news.google.com/newspapers?id=2-U-AAAAIBAJ&sjid=E00MAAAAIBAJ&pg=6496%2C3634673 |agency=Associated Press |publisher=Google News Archives |newspaper=The Tuscaloosa News |date=October 1, 1944 |page=6 |accessdate=November 27, 2012}}</ref>

==References==
{{Reflist|30em}}

{{Alabama Crimson Tide football navbox}}

[[Category:1943 Southeastern Conference football season|Alabama]]
[[Category:Alabama Crimson Tide football seasons]]
[[Category:1943 in Alabama|Crimson Tide]]