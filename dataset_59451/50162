{{Use dmy dates|date=July 2013}}
{{BLP sources|date=January 2008}}

{{Infobox prime minister
| image =
| imagesize = 170px
| name=Gennady Leonid-ipa Gagulia
| office=Head of the Presidential Administration
| term_start=15 December 2003
| term_end=18 June 2004
| president=[[Vladislav Ardzinba]]
| predecessor=[[Miron Agrba]]
| successor=
| order2=5th
| office2=Prime Minister of Abkhazia
| term_start2=29 November 2002
| term_end2=8 April 2003
| president2=[[Vladislav Ardzinba]]
| predecessor2=[[Anri Jergenia]]
| successor2=[[Raul Khajimba]]
| order3=1st
| office3=Prime Minister of Abkhazia
| term_start3=January 1995
| term_end3=29 April 1997
| president3=[[Vladislav Ardzinba]]
| predecessor3=
| successor3=[[Sergei Bagapsh]]
| birth_date={{Birth date and age|1948|01|04|df=y}}
| birth_place=[[Lykhny]], [[Gudauta district]], [[Abkhazian ASSR]], [[Georgian SSR]], [[Soviet Union]]
| party=
| nationality = [[Abkhaz people|Abkhaz]]
| religion= 
| spouse=
}}

'''Gennady (Genadi) Leonid-ipa Gagulia''' ({{lang-ab|Геннадии Леонид-иҧа Гагәлиа}}, {{lang-ka|გენადი გაგულია}}; born 4 January 1948) is a two-time former Prime Minister of [[Abkhazia]] (January 1995 – April 1997, November 2002 – April 2003) and the current head of the Chamber of Commerce and Industry.

==Early career==

In 1972 he graduated from Civil Engineering Department of the Belarusian Polytechnic Institute. From 1973 to 1977 he worked at Stroymaster as superintendent, chief engineer, head of the construction site. From 1977 to 1986 he was Deputy Director of Works Catering Lake Riza, From 1986 to 1991 he was the chairman of the Gudauta district consumer cooperatives. From 1991 to 1992 he worked at  the State Committee for Foreign Economic Relations,  Council of Ministers - Abkhaz ASSR. From 1992 to 1995 He was Deputy Chairman of the Council of Ministers of Abkhazia. 
During the Georgian-Abkhaz war in the early 1990s, Gagulia was a member of the [[Abkhazian Defense Committee]], which was responsible for, among other things, the distribution of food, at a time when Abkhazian forces only controlled a section of the country.{{Citation needed|date=December 2009}}

==First period as prime minister==

Gennady Gagulia was the first person to fill the post of prime minister which came into existence with the adoption of the [[Constitution of Abkhazia]] in 1994. Gagulia was appointed in January 1995.<ref name=lakoba>{{cite book
 | last = Лакоба
 | first = Станислав
 | authorlink =
 | coauthors =
 | title = "Абхазия после двух империй XIX—XXI вв." // 21st Century COE Program Slavic Eurasian Studies — No. 5.
 | publisher = Slavic Research Center, Hokudai University
 | year = 2004
 | location = Sapporo
 | pages =
 | url = http://src-h.slav.hokudai.ac.jp/coe21/publish/no5_ses/contents.html
 | doi =
 | id =
 | isbn = }}</ref>

As prime minister Gagulia had a reputation for being the most strongly pro-Russian of Abkhazia's prime ministers. He steadfastly opposed both reunification with Georgia and the withdrawal of Russian troops from the Georgian-Abkhaz border. He has also alluded to the possibility of Abkhazia unifying with Russia.{{Citation needed|date=December 2009}}

Gagulia was also heavily involved in negotiations over the future of Abkhazia, choosing to handle negotiations himself, rather than sending envoys, as has generally been the case with other Abkhaz leaders. In this role, he has met with a number of foreign leaders, including the Presidents of Russia [[Vladimir Putin]] and of Georgia [[Eduard Shevardnadze]].{{Citation needed|date=December 2009}}

In 1997, Gagulia resigned from the prime ministership, citing health reasons. In between his two stints as prime minister, Gagulia was the chair of the [[Chamber of Commerce and Industry of Abkhazia]]. He was also deputy prime minister under [[Anri Jergenia]].{{Citation needed|date=December 2009}} When Jergenia was fired on 29 November 2002, Gagulia was appointed prime minister for the second time.<ref name=IWPR>{{cite news
 |last= 
 |first= 
 |author=Inal Khashig 
 |title=Abkhaz prime minister ousted 
 |work=War and Peace in the Caucasus 
 |pages=204–205 
 |publisher=[[Institute for War & Peace Reporting]] 
 |date=2 December 2002 
 |url=http://www.iwpr.net/pdf/100crs_eng.pdf 
 |accessdate=2008-07-10 
 |archiveurl=https://web.archive.org/web/20080614183132/http://www.iwpr.net/pdf/100crs_eng.pdf 
 |archivedate=14 June 2008 
 |deadurl=yes 
 |df=dmy 
}}</ref>

==Second resignation as prime minister and further political career==

In the evening of 7 April 2003, Gagulia's government filed for resignation. Early in the morning of that day, nine prisoners had escaped, four of which had been sentenced to death due to their involvement in the [[2001 Kodori crisis]].<ref name="uzel36186">{{cite news|url=http://www.kavkaz-uzel.ru/articles/36186/|script-title=ru:Правительство Абхазии уходит в отставку|last=Gordienko|first=Anatoly|date=9 April 2003|publisher=[[Nezavisimaya Gazeta]] / Caucasian Knot|language=Russian|accessdate=21 December 2009}}</ref> President Ardzinba initially refused to accept Gagulia's resignation, but was forced to agree on 8 April.<ref name="civil4016">{{cite news|url=http://www.civil.ge/eng/article.php?id=4016|title=Breakaway Abkhaz Government in Crisis|date=9 April 2003|publisher=[[Civil Georgia]]|accessdate=21 December 2009}}</ref> Vice President [[Valery Arshba]] denied on 8 April that the government's resignation was due to the prison escape, and stated that instead it was caused by the opposition's plans to hold protest rallies on 10 April.<ref name="civil4010">{{cite news|url=http://www.civil.ge/eng/article.php?id=4010|title=Abkhaz de facto Premier Remains on Post|date=8 April 2003|publisher=[[Civil Georgia]]|accessdate=21 December 2009}}</ref>

On 15 December 2003, Gagulia was appointed head of the Presidential administration, succeeding [[Miron Agrba]].<ref name="uzel48035">{{cite news|url=http://www.kavkaz-uzel.ru/articles/48035/|script-title=ru:Экс-премьер-министр Абхазии назначен руководителем администрации президента республики|last=Kuchuberia|first=Anzhela|date=17 December 2003|publisher=Caucasian Knot|language=Russian|accessdate=20 December 2009}}</ref> On 18 June 2004, Gagulia resigned from his post, stating "There is a certain scenario to the presidential elections in Abkhazia. I don't match this scenario and thus I prefer to step down."<ref name="uzel57244">{{cite news|url=http://www.kavkaz-uzel.ru/articles/57244/|script-title=ru:Администрации Президента Республики Абхазия|last=Kuchuberia|first=Anzhela|date=18 June 2004|publisher=Caucasian Knot|language=Russian|accessdate=20 December 2009}}</ref>

It had been suggested that Ardzinba may have also favoured Gagulia to replace him as President, but Ardzinba instead decided to back then-Prime Minister [[Raul Khajimba]].

On 24 June 2004, Gagulia was again appointed head of the Chamber of Commerce and Industry of Abkhazia, succeeding [[Yuri Aqaba]].<ref name="uzel57654">{{cite news|url=http://www.kavkaz-uzel.ru/articles/57654/|script-title=ru:Бывший руководитель администрации президента Абхазии Геннадий Гагулия возглавил Торгово-промышленную палату республики|last=Kuchuberia|first=Anzhela|date=25 June 2004|publisher=Caucasian Knot|language=Russian|accessdate=20 December 2009}}</ref> Gagulia has remained in that position till the present day.

==References==

{{Reflist|2}}

{{S-start}}
{{s-off}}
{{Succession box| before=[[Sokrat Jinjolia]] | title=[[Prime Minister of Abkhazia]] | years=1995 &ndash; 1997 | after=[[Sergei Bagapsh]]}}
{{Succession box| before=? | title=Head of the Chamber of Commerce and Industry | years=? &ndash; ? | after=?}}
{{Succession box| before=[[Anri Jergenia]] | title=[[Prime Minister of Abkhazia]] | years=29 November 2002 &ndash; 8 April 2003 | after=[[Raul Khajimba]]}}
{{Succession box| before=[[Miron Agrba]] | title=Head of the Presidential Administration | years=15 December 2003 &ndash; 18 June 2004 | after=?}}
{{Succession box| before=[[Yuri Aqaba]] | title=Head of the Chamber of Commerce and Industry | years=24 June 2004 &ndash; ''Present'' | after=''Incumbent''}}
{{S-end}}
{{Prime Ministers of Abkhazia}}

{{DEFAULTSORT:Gagulia, Gennady}}
[[Category:1948 births]]
[[Category:Living people]]
[[Category:People from Gudauta District]]
[[Category:Prime Ministers of Abkhazia]]
[[Category:Heads of the Presidential Administration of Abkhazia]]