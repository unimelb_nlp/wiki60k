<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox aircraft begin
 | name=ASK 14
 | image=File:Schleicher ASK-14.jpg
 | caption=
}}{{Infobox aircraft type
 | type=[[Motor glider]]
 | national origin=
 | manufacturer=[[Alexander Schleicher GmbH & Co]]
 | designer=[[Rudolf Kaiser]]
 | first flight=
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=66
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= [[Schleicher Ka 6E]]
 | variants with their own articles=
}}
|}
[[File:ASK 14 Motorglider.png|thumb|right|ASK 14 at Wallduern Airfield/Germany]]
[[File:ASK14 in Günterode.JPG|right|thumb|ASK 14 partially disassembled for [[Aircraft maintenance|maintenance]]]]
[[File:VH-GYP Schleicher ASK 14 (6773038185).jpg|thumb|right|ASK 14 in Australia]]
[[File:G-BSIY (8705919229).jpg|thumb|right|British ASK 14]]
The '''Schleicher ASK 14''' is a [[West Germany|West German]] [[low-wing]], single-seat [[motor glider]] that was designed by [[Rudolf Kaiser]] and produced by [[Alexander Schleicher GmbH & Co]].<ref name="SD">{{Cite web|url = http://www.sailplanedirectory.com/PlaneDetails.cfm?PlaneID=17|title = ASK-14 Schleicher |accessdate = 24 July 2011|last = Activate Media|authorlink = |year = 2006}}</ref><ref name="SoaringNov83">Said, Bob: ''1983 Sailplane Directory, [[Soaring Magazine]]'', page 131. Soaring Society of America, November 1983. USPS 499-920</ref>

The aircraft is often referred to as the '''Schleicher AS-K 14''' or the '''Schleicher ASK-14''', the ''AS'' for ''Alexander Schleicher'' and the ''K'' for ''Kaiser''. The US [[Federal Aviation Administration]] designates the aircraft as the ''Schleicher ASW-14'', while [[Transport Canada]]  and the [[United Kingdom]] [[Civil Aviation Authority (United Kingdom)|Civil Aviation Authority]] call it the ''Schleicher ASK 14''.<ref name="SD" /><ref name="SoaringNov83" /><ref name="FAAReg">{{Cite web|url = http://registry.faa.gov/aircraftinquiry/AcftRef_Results.aspx?Mfrtxt=&Modeltxt=ASK+14&PageNo=1|title = Make / Model Inquiry Results - ASK 14|accessdate = 24 July 2011|last = [[Federal Aviation Administration]]|authorlink = |date=July 2011}}</ref><ref name="TCCAR">{{Cite web|url = http://wwwapps2.tc.gc.ca/Saf-Sec-Sur/2/ccarcs/aspscripts/en/quicksearch.asp|title = Canadian Civil Aircraft Register|accessdate = 24 July 2011|last = [[Transport Canada]]|authorlink = |date=July 2011}}</ref><ref name="GINFO">{{Cite web|url = http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=summary&aircrafttype=ASK%2014|title = GINFO Search Results Summary|accessdate = 24 July 2011|last = [[Civil Aviation Authority (United Kingdom)]]|authorlink = |date=July 2011}}</ref>

==Design and development==
The ASK 14 was developed as a low-wing motorized version of the [[Schleicher Ka 6E]]. The powerplant is a {{convert|19|kW|hp|0|abbr=on}} [[Hirth F10 K19]] four-cylinder, [[two-stroke]] engine, made by [[Hirth]] and driving a fully feathering [[propeller (aircraft)|propeller]].<ref name="SD" /><ref name="SoaringNov83" />

The aircraft is built from wood and covered with doped [[aircraft fabric covering]]. The {{convert|14.4|m|ft|1|abbr=on}} span wing employs a [[NACA]] 63-618 [[airfoil]] at the [[wing root]] transitioning to a NACA 63-615 section at the [[wing tip]] and features [[Spoiler (aeronautics)|spoilers]]. The monowheel [[landing gear]] is retractable. The [[cockpit]] is covered by a [[bubble canopy]] that gives all-around visibility.<ref name="SD" /><ref name="SoaringNov83" />

Due to its Ka 6E heritage the handling of the ASK 14 was described by [[Soaring Magazine]] in 1983 as "superb".<ref name="SoaringNov83" />

The ASK 14 was never [[Type certificate|type certified]] and aircraft registered in the United States were in the ''Experimental - Racing/Exhibition '' category. ASW 14s in Canada are in the category ''CAR Standard 507.03(5)(a) Aircraft Held a Flight Permit - Private Aircraft prior 01/89''. Those in the UK have an [[European Aviation Safety Agency|EASA]] [[Certificate of Airworthiness]].<ref name="FAAReg" /><ref name="TCCAR" /><ref name="GINFO" />

==Operational history==
ASK 14s finished second, third and fourth in the single-place class at the first international motorglider competition, that was held at [[Feuerstein Castle|Burg Feuerstein]], [[West Germany]] in 1970.<ref name="SD" />

In 1971 former [[Soaring Magazine]] editor Bennett Rogers set the first [[United States]] motorgliding record in an ASK 14, flying {{convert|330.5|km|mi|0|abbr=on}} out-and-return from [[Rosamond, California]], US.<ref name="SoaringNov83" />

In July 2011 there were still eight ASK 14s registered with the FAA in the US, two with the UK Civil Aviation Authority and one registered with Transport Canada.<ref name="FAAReg" /><ref name="TCCAR" /><ref name="GINFO" />
<!-- ==Variants== -->
<!-- ==Aircraft on display== -->

==Specifications (ASK 14) ==
{{Aircraft specs
|ref=Sailplane Directory and Soaring<ref name="SD" /><ref name="SoaringNov83" />
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=
|crew=one
|capacity=
|length m=
|length ft=
|length in=
|length note=
|span m=14.4
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=12.68
|wing area sqft=
|wing area note=
|aspect ratio=16.8:1
|airfoil=root: [[NACA]] 63-618, tip: NACA 63-615
|empty weight kg=245
|empty weight lb=
|empty weight note=
|gross weight kg=360
|gross weight lb=
|gross weight note=
|fuel capacity=
|more general=

<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Hirth F10 K19]]
|eng1 type=four cylinder, [[two-stroke]]
|eng1 kw=19<!-- prop engines -->
|eng1 hp=<!-- prop engines -->
|prop blade number=2<!-- propeller aircraft -->
|prop name=fully feathering
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=
|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio= 29:1 at {{convert|81|km/h|mph|0|abbr=on}}
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|sink rate ms=0.75
|sink rate ftmin=
|sink rate note= at {{convert|72|km/h|mph|0|abbr=on}}
|lift to drag=
|wing loading kg/m2=28.6
|wing loading lb/sqft=
|wing loading note=
|more performance=
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
* [[List of gliders]]
|related=<!-- related developments -->
|similar aircraft=
*[[Akaflieg München Mü23 Saurier]]
|lists=<!-- related lists -->
}}

==References==
{{Reflist}}

==External links==
{{Commons category-inline|ASK 14}}
{{Schleicher}}

[[Category:German sailplanes 1960–1969]]
[[Category:Schleicher aircraft]]