{{about|the game based on FOX's television series|the arithmetical card game|24 Game}}
{{Use mdy dates|date=December 2014}}
{{Infobox video game
|title = 24: The Game
|image = [[Image:24 - The Game.jpg|200px]]
|developer = [[Guerrilla Cambridge|SCE Studio Cambridge]]
|publisher = {{vgrelease|EU|[[Sony Computer Entertainment|Sony Computer Entertainment Europe]]|NA|[[2K Games]]}}
|designer = Katie Sorrell
|composer = [[Sean Callery]] recorded by the Nimrod Studio Orchestra at Abbey Road
|writer = Duppy Demetrius<br/>Chris Sorrell
|artist = Rob Hill
|released = {{vgrelease|NA|February 27, 2006|EU|March 17, 2006|AUS|April 22, 2006}}
|genre = [[Third-person shooter]]
|modes = [[Single-player]]
|platforms = [[PlayStation 2]]
}}
'''''24: The Game''''' is a [[third-person shooter]] video game, based on the [[Fox Broadcasting Company|FOX]] television series, ''[[24 (TV series)|24]]''. The game was [[video game developer|developed]] by [[Sony Computer Entertainment]]'s [[SCE Studio Cambridge|Cambridge]] Studios and was [[video game publisher|published]] by [[2K Games]] for [[PlayStation 2]]. It was announced on March 30, 2005 and was released in North America exclusively on February 27, 2006. The player controls many characters from the television series at different points in the game. The missions in the game involve elements of [[third-person shooter]], [[Racing game|driving]] and [[puzzle video game|puzzle games]]. The musical score was composed by [[Sean Callery]], while the script was written by Duppy Demetrius and the series production team.

''24: The Game'' makes extensive use of actors' voices and likenesses from the TV series, as well as using a script and music score from the same production team. Casting and voice production for ''24: The Game'' were organized and handled by [[Blindlight]]. The events contained in the game are set in [[Los Angeles]] between the [[24 (season 2)|second]] and [[24 (season 3)|third]] seasons of the show. The story features three plotlines that overlap around a character from [[Jack Bauer]]'s past named Peter Madsen. Although the game received a mixed reception from critics, it received a [[British Academy of Film and Television Arts|BAFTA award]] nomination for [[British Academy Film Awards|its screenplay elements]].

==Gameplay==
Like the series, the game takes place over 24 hours and has the same start and end clocks for each hour, marking the start and end of each part of the game. The hours are broken down into 58 separate missions, of which there are three broad types, each described in further detail below. Some missions are objective based, while others have a time-restricted element.<ref name="ign walkthrough">{{cite web |url=http://guides.ign.com/guides/738161/page_4.html|title=24: The Game Guide & Walkthrough – PlayStation 2 (PS2) – IGN (Walkthrough)|archiveurl=https://web.archive.org/web/20070527075639/http://guides.ign.com/guides/738161/page_4.html|archivedate=May 27, 2007|deadurl=yes|accessdate=December 31, 2011|publisher=[[IGN]]}}</ref> Each mission is graded out of 100 points, based on the quality of performance, number of objectives completed, shooting accuracy and so on. A grade of 90 points or higher rewards the player by unlocking some form of bonus feature, including three movies (interviews with cast about the game, TV promos), 98 images (wallpaper-like images of main characters) and 23 characters (3D models that the player can view).<ref name="ign rankings">{{cite web|url=http://guides.ign.com/guides/738161/page_5.html|title=24: The Game Guide & Walkthrough – PlayStation 2 (PS2) – IGN (Mission Rankings)|archiveurl=https://web.archive.org/web/20090705173634/http://guides.ign.com/guides/738161/page_5.html|archivedate=July 5, 2009|deadurl=yes|accessdate=December 31, 2011|publisher=IGN}}</ref>

Most of the game's missions take place in [[third-person shooter|third-person shooting]] format,<ref name="ign walkthrough"/> combined with the use of a [[cover system]].<ref name="ign strategies">{{cite web|url=http://guides.ign.com/guides/738161/page_2.html|title=24: The Game Guide & Walkthrough – PlayStation 2 (PS2) – IGN (Basic Strategies)|archiveurl=https://web.archive.org/web/20070719050209/http://guides.ign.com/guides/738161/page_2.html|archivedate=July 19, 2007|deadurl=yes|accessdate=December 31, 2011|publisher=IGN}}</ref> When a character is behind a low object like a crate, or at the edge of a corner or doorframe, they can press a button to use the object or edge as cover. Another button will allow the character to peer around and enter a targeting mode, while releasing the button will quickly return them to cover. There is also a [[stealth game|stealth]] mode which has the character stoop, walk silently (unless running), duck behind any low obstacles they can hide behind and be less detectable by sound. A character can sneak up behind an enemy using stealth mode and perform a silent "stealth takedown" by breaking his neck.<ref name="ign strategies"/>

A variety of weapons are available including handguns, automatic weapons and shotguns, with various ammunition types also in existence.<ref name="ign weaponry">{{cite web|url=http://guides.ign.com/guides/738161/page_3.html|title=24: The Game Guide & Walkthrough – PlayStation 2 (PS2) – IGN (Weaponry)|archiveurl=https://web.archive.org/web/20090501200730/http://guides.ign.com/guides/738161/page_3.html|archivedate=May 1, 2009|deadurl=yes|accessdate=December 31, 2011|publisher=IGN}}</ref> Ammunition is limited in the game and weapons must be reloaded, although additional ammunition can be collected.<ref name="ign strategies"/> Players can also call out to enemies who will sometimes surrender and can then be captured (put in handcuffs) with their ammunition also collected. There are also civilians in the game who can also be called out to and then rescued by the player. Health bars for enemies, vehicles or friendly subjects that must be protected are also sometimes shown. There are health packs which can be picked up, as well as health stations mounted on walls which can be used. Body armor is also sometimes provided or found in levels. A small [[HUD (computer gaming)|heads-up-display]] features a [[Automap|radar/map]] with an arrow showing the character's direction, red dots for enemies, green dots for civilians, and yellow stars for objectives. The direction and field of vision of enemies is shown on the radar.<ref name="ign walkthrough"/>

In third-person missions and occasionally some other types, a [[Personal digital assistant|PDA]] is available. In the PDA the player can view their list of objectives (which can change over the course of a mission), maps of the area if available, a help screen and a list of weapons held with information on each.<ref name="ign strategies"/> Characters also typically have [[Mobile phone|cell phones]] which they sometimes receive calls on during missions. Some missions involve sniping using [[first-person shooter]] gameplay. These missions resemble the use of a sniper rifle during normal missions but the player cannot switch to a third-person view.<ref name="ign walkthrough"/>

There are several missions in which the player controls a motor vehicle.<ref name="ign walkthrough"/> Though the [[game engine]] is the same as the third-person mode, these missions are based on being in a vehicle. They usually involve getting to a certain location in a certain time period, often while avoiding pursuers while heading to the end point. Weapons can not be used while in a vehicle. While the PDA is not available in these mission types, a map of the city is. Cell phone calls are also sometimes received in these missions.

===Minigames===
[[Image:24 the game interrogation.jpg|250px|thumb|right|One of the interrogation mini games in progress, showing a suspect being broken]]
''24'''s [[minigame]]s serve as interludes from the main mission, and are primarily interrogation sequences or [[puzzle video game|computer puzzles]]. They are presented both as single puzzles in third-person missions, and as timed stand-alone missions.<ref name="ign walkthrough"/> The computer puzzles include maze puzzles, where a player must select a coloured path from a starting box to an ending box and sometimes a second and third box, are used to represent bypassing locks and other computer functions. Letter sequence puzzles, where a player swaps adjacent pairs of a series of random letters until they are in the correct sequence, are used to represent [[Cryptanalysis|code-breaking]] such as for encrypted files or a locked door. Colour/symbol matching puzzles, where a cursor randomly moves across a field of coloured squares and the player must press the controller button that corresponds to that coloured square, are used to represent unscrambling files from hard drives and other similar functions. A number of other puzzles are used only once, including puzzles to simulate [[file transfer|transferring files]] over a damaged network, [[infrared]] scanning of buildings, radar-like scanning of areas, and creating a network link across the entire planet.<ref name="gamespot review"/>

The interrogation minigame involves the players character interrogating a suspect. A graph shows the suspect's "stress level" which oscillates in a sort of pseudo-[[sine wave]]. The player must choose to act either aggressively, calmingly or neutrally towards the suspect. These actions will raise, lower or maintain the stress level. A horizontal target range is highlighted on the chart, which the player must aim for the stress level to be within in order to advance the interrogation. A marker next to the graph indicates how advance the interrogation. The player must complete interrogation within a set time limit to succeed.<ref name="official site"/>

==Plot==

''24: The Game'' takes place between the events of the [[24 (season 2)|second]] and [[24 (season 3)|third]] seasons. In a similar way to the TV series, it can be split up into three sections or chapters. Section one revolves around an attack on [[List of fictional Vice Presidents of the United States#P|Vice President]] [[Jim Prescott]], while section two covers an attack on the Counter Terrorist Unit (CTU). Section three covers a major terrorist attack and attempt to gain access to nuclear weapons.<ref name="ign walkthrough"/> A large number of characters from seasons two and three feature in ''24: The Game'', with each using the original actor's likeness and voice acting. Main characters returning include [[Jack Bauer]], [[Kim Bauer|Kimberly "Kim" Bauer]], [[Tony Almeida]], [[Michelle Dessler]], [[Chase Edmunds]], [[David Palmer (character)|David Palmer]], [[Max (24 character)|Max]], [[Kate Warner (24 character)|Kate Warner]],  [[Chloe O'Brian]], and [[Ryan Chappelle]], with Peter Madsen being voiced by [[Christian Kane]].<ref name="official site">{{cite web|url=http://www.2kgames.com/24/site.html|title=24 The Game (Official site)|archiveurl=https://web.archive.org/web/20060409004945/http://www.2kgames.com/24/site.html|archivedate=April 9, 2006|deadurl=yes|accessdate=March 4, 2008|publisher=[[2K Games]]}}</ref>

The game begins with Jack Bauer waiting outside a ship in [[Port of Los Angeles|Los Angeles harbor]] where terrorists are going to release a [[ricin]] bomb in the water supply. A CTU team member triggers an alarm causing Jack and his team to storm the ship,<ref name="GameSpy Review"/> discovering the whole ship's crew dead in a cargo hold. He later learns of an [[assassination]] attempt on [[Jim Prescott|Vice President Prescott]] through undercover agent [[Chase Edmunds]]. Foiling the attack, Jack discovers that the mastermind behind the attempt is an enemy from his past known as Peter Madsen.<ref name="ign walkthrough"/>

A [[Sarin]] gas attack on an [[Los Angeles County Metropolitan Transportation Authority|L.A. Metro]] station lures CTU agents away from their headquarters. While distracted, terrorists activate an [[Electromagnetic pulse|EMP]], attacking and taking over the CTU LA main building, holding the staff members hostage. The terrorists execute these hostages, including data analyst Sean Walker and eventually escape with a stolen hard drive. Jack runs into Peter Madsen, who has kidnapped Jack's daughter, [[Kim Bauer]], forcing Jack to do "errands" for the terrorist cell. One of these errands is to sneak into a [[National Security Agency|NSA]] building and retrieve confidential data for the terrorists. Jack manages to find and rescue Kim and recover the stolen hard drive with the help of undercover agent [[Chase Edmunds]].<ref name="ign walkthrough"/>

A major [[earthquake]] occurs in Los Angeles, caused by terrorists detonating explosives at focal points (places where [[fault (geology)|fault lines]] intersect). [[Kate Warner (24 character)|Kate Warner]] is also kidnapped by the terrorist cell, along with Governor James Radford who is kidnapped for assassination but is then rescued by the CTU. A conspiracy involving Radford in the day's attacks is uncovered by the CTU and Radford is killed by the terrorist cell because he attempts to back out. Fort Lesker, [[U.S. Military]] base and the [[epicenter]] of the earthquakes, is attacked and taken over by terrorists, who then begin stealing [[weapons-grade]] plutonium before attempting to smuggle the weapons out of the U.S. to the [[Middle East]]. Kate Warner's father is forced to aid the terrorists by helping them smuggle the weapons with his customs passes. Jack finally kills Madsen when he tries to escape by shooting his speedboat with an [[Zastava M80|M-80 assault rifle]], causing it to explode. He also shoots and kills Max, the man behind the events of Season 2 and The Game, who was holding Kate Warner hostage, saving her life but in doing so Max manages to shoot Jack once in the stomach before dying. As a result, Chase Edmunds takes Jack to the hospital via [[helicopter]].<ref name="ign walkthrough"/>

== Characters ==
{{Col-begin}}{{Col-2}}'''Main characters'''
{| Class = "wikitable"
|- bgcolor="#CCCCCC"
! Voiced !! Character
|-
|-
|-
|[[Kiefer Sutherland]] || [[Jack Bauer]]
|-
|[[Elisha Cuthbert]] || [[Kim Bauer]]
|-
|[[Dennis Haysbert]] || [[David Palmer (24 character)|David Palmer]]
|-
|[[Reiko Aylesworth]] || [[Michelle Dessler]]
|-
|[[Carlos Bernard]] || [[Tony Almeida]]
|-
|[[James Badge Dale]] || [[Chase Edmunds]]
|-
|[[Mary Lynn Rajskub]] || [[Chloe O'Brian]]
|-
|[[Zachary Quinto]] || [[Adam Kaufman (24 character)|Adam Kaufman]]
|-
|[[Glenn Morshower]] || [[Agent Aaron Pierce]]
|-
|[[Thomas Kretschmann]] || Max
|-
|[[Paul Schulze]] || [[Ryan Chappelle]]
|-
|[[Alan Dale]] || [[VP Jim Prescott]]
|-
|[[Daniel Dae Kim]] || [[Agent Tom Baker]]
|-
|[[Sarah Clarke]] || [[Nina Myers]]
|-
|[[Sarah Wynter]] || [[Kate Warner]]
|-
|[[Christian Kane]] || Madsen
|-
|[[Andreas Katsulas]] || Governor Radford 
|-
|[[Tom Sizemore]] || [[Sid Wilson]]
|}{{Col-end}}

== Development ==
''24: The Game'' was announced on March 30, 2005 by [[Sony Computer Entertainment Europe]] through a licensing deal with [[Twentieth Century Fox]] to bring ''24'' to the [[PlayStation 2]],{{Citation needed|date=April 2016}} and Sony Computer Entertainment's [[SCE Studio Cambridge|Cambridge]] Studio was confirmed to be the developer of the game.<ref name="gamespot firstlook"/> The announcement described how the game would sit between seasons two and three of the TV series, answering many questions left unanswered by the TV show storyline. Also included was the news that the game would feature the voices and likenesses of many of the cast of the TV show. By the time of the announcement the music score had been prepared by [[Sean Callery]], while a script had been created by Duppy Demetrius in collaboration with the 24 production team.{{Citation needed|date=April 2016}} The announcement also stated that sound effects and noises from the TV series would be used in the game.{{Citation needed|date=April 2016}} The press release was accompanied by a press-only video featuring [[Kiefer Sutherland]] describing some of the story and gameplay elements.<ref name="gamespot firstlook">{{cite web |url=http://www.gamespot.com/articles/24-the-game-first-look/1100-6121328/ |title=24: The Game First Look |accessdate=November 19, 2014 |author=Brad Shoemaker |date=March 30, 2005 |publisher=[[GameSpot]]}}</ref>

The game made an appearance later that May at the [[E3 (Electronic Entertainment Expo)|E3]] show in 2005, where four sections were demonstrated: a third-person shootout, an interrogation scene, a computer tech minigame and a driving sequence. Some of the intermediate cut-scenes were also shown at this point. The interrogation scene received praise, while the driving segment was let down by poor mechanics and physics.<ref>{{cite web |url=http://www.gamespot.com/articles/24-the-game-e3-2005-hands-on-impressions/1100-6125713/ |title=24: The Game E3 2005 Hands-On Impressions |accessdate=November 19, 2014 |author=Alex Navarro |date=May 18, 2005 |publisher=GameSpot}}</ref> Originally 100 missions were proposed, with only 58 making it into the final game. In an interview, Mark Green stated that the game style was influenced by the way ''[[The Lord of the Rings: The Two Towers (video game)|Lord of the Rings: The Two Towers]]'' and ''[[Enter the Matrix]]'', while gameplay influences were described as ''[[James Bond 007: Everything or Nothing]]'' and ''[[Alias (video game)|Alias]]''.<ref name="eurogamer interview">{{cite web |url=http://www.eurogamer.net/articles/i_24_markgreen |title=24: The Game Interview |accessdate=March 4, 2008 |author=Tom Bramwell |date=January 26, 2006 |publisher=[[Eurogamer]]}}</ref>

It was originally planned to be released worldwide in the northern fall of 2005{{Citation needed|date=April 2016}} but was delayed. The game was released in North America on February 27, 2006{{Citation needed|date=April 2016}} in Europe on March 17, 2006 (March 16, 2006 in Ireland, due to [[St. Patrick's Day]]), and in [[Australia]] on April 22, 2006.{{Citation needed|date=April 2016}} The musical score to the game, performed by the Nimrod Studio Orchestra and recorded at [[Abbey Road Studios]] in London,<ref>{{cite web |url=http://www.soundonsound.com/sos/may06/articles/nimrod.htm |title=Recording 24: The Game|accessdate=March 14, 2008 |date=May 2006 |author=Tom Flint |publisher=[[Sound on Sound]]}}</ref> was made available for digital download after the game was released.<ref name="ignmusic">{{cite web|url=http://www.ign.com/articles/2006/03/08/24-the-game-soundtrack-revealed|title=24: The Game Soundtrack Revealed|author=IGN Music|date=March 8, 2006|publisher=IGN|accessdate=November 19, 2014}}</ref>

==Reception==
{{Video game reviews
|MC = 62 of 100<ref name="metacritic">{{cite web |url=http://www.metacritic.com/game/playstation-2/24-the-game |title=24: The Game for PlayStation 2 Reviews |accessdate=March 4, 2008 |publisher=[[Metacritic]]}}</ref>
|Edge = 6 of 10<ref>{{cite journal |title=24: The Game |author=Edge staff |issue=160 |date=March 2006 |page=88 |magazine=[[Edge (magazine)|Edge]]}}</ref>
|EGM = 6.67 of 10<ref>{{cite journal |title=24: The Game |author=EGM staff |issue=202 |date=April 2006 |page=100 |magazine=[[Electronic Gaming Monthly]]}}</ref>
|EuroG = 6 of 10<ref name="Eurogamer Review">{{cite web |url=http://www.eurogamer.net/articles/r_24thegame_ps2 |title=24: The Game |accessdate=March 17, 2008 |date=March 8, 2006 |author=Rob Purchese |publisher=Eurogamer}}</ref>
|GI = 7.5 of 10<ref>{{cite journal |title=24: The Game |issue=156 |date=April 2006 |page=122 |magazine=[[Game Informer]]}}</ref>
|GamePro = {{Rating|3|5}}<ref>{{cite web |url=http://www.gamepro.com/sony/ps2/games/reviews/52393.shtml |title=Review: 24: The Game |accessdate=November 19, 2014 |author=Ouroboros |date=March 1, 2006 |publisher=''[[GamePro]]'' |archiveurl=https://web.archive.org/web/20060826221228/http://www.gamepro.com/sony/ps2/games/reviews/52393.shtml |archivedate=August 26, 2006 |deadurl=yes}}</ref>
|GameRev = C<ref>{{cite web |url=http://www.gamerevolution.com/review/24 |title=24 [The Game] Review |accessdate=November 19, 2014 |author=JP Hurh |date=April 19, 2006 |publisher=[[Game Revolution]]}}</ref>
|GSpot = 6.2 of 10<ref name="gamespot review">{{cite web |url=http://www.gamespot.com/reviews/24-the-game-review/1900-6145381/ |title=24: The Game Review |accessdate=November 19, 2014 |author=Alex Navarro |date=March 3, 2006 |publisher=GameSpot}}</ref>
|GSpy = {{Rating|2|5}}<ref name="GameSpy Review">{{cite web |url=http://ps2.gamespy.com/playstation-2/24/696842p1.html |title=24: The Game |accessdate=March 4, 2008 |author=Eduardo Vasconcellos |date=March 17, 2006 |publisher=[[GameSpy]] |archiveurl=https://web.archive.org/web/20080212162239/http://ps2.gamespy.com/playstation-2/24/696842p1.html |archivedate=February 12, 2008 <!--DASHBot--> |deadurl=no}}</ref>
|GT = 7.1 of 10<ref>{{cite web |url=http://www.gametrailers.com/gamepage.php?id=1563 |title=24: The Game Review |accessdate=March 28, 2016 |date=March 24, 2006 |publisher=[[GameTrailers]] |archiveurl=https://web.archive.org/web/20070415094149/http://www.gametrailers.com/gamepage.php?id=1563 |archivedate=April 15, 2007 |deadurl=yes}}</ref>
|GameZone = 6.5 of 10<ref>{{cite web |url=http://www.gamezone.com/reviews/24_the_game_ps2_review |title=24: The Game - PS2 - Review |accessdate=November 19, 2014 |author=Aceinet |date=March 19, 2006 |publisher=[[GameZone]] |archiveurl=https://web.archive.org/web/20081005005918/http://ps2.gamezone.com/gzreviews/r26172.htm |archivedate=October 5, 2008 |deadurl=no}}</ref>
|IGN = 4.2 of 10<ref name="ign review">{{cite web |url=http://www.ign.com/articles/2006/03/02/24-the-game |title=24: The Game |accessdate=November 19, 2014 |author=Chris Roper |date=March 2, 2006 |publisher=IGN}}</ref>
|OPM = {{Rating|4|5}}<ref>{{cite journal |title=24: The Game |date=April 2006 |page=81 |magazine=[[Official U.S. PlayStation Magazine]]}}</ref>
|rev1 = ''[[The A.V. Club]]''
|rev1Score = C<ref name="avclub">{{cite web |url=http://www.avclub.com/article/24-the-game-9212 |title=24: The Game |accessdate=November 19, 2014 |author=Chris Dahlen |date=March 21, 2006 |publisher=''[[The A.V. Club]]'' |archiveurl=https://web.archive.org/web/20060322193311/http://www.avclub.com/content/node/46555 |archivedate=March 22, 2006 |deadurl=no}}</ref>
|rev2 = ''[[Detroit Free Press]]''
|rev2Score = {{Rating|2|4}}<ref name="detroit">{{cite news |url=http://www.freep.com/apps/pbcs.dll/article?AID=/20060409/ENT06/604090323/1044 |title='24: The Game' |accessdate=March 28, 2016 |author=Ryan Huschka |date=April 9, 2006 |newspaper=[[Detroit Free Press]] |archiveurl=https://web.archive.org/web/20060629234519/http://www.freep.com/apps/pbcs.dll/article?AID=/20060409/ENT06/604090323/1044 |archivedate=June 29, 2006 |deadurl=yes}}</ref>
}}

The game received "mixed" reviews according to video game [[review aggregator]] [[Metacritic]].<ref name="metacritic"/><!--The game was nominated for a [[BAFTA]] Games Award in the Best Screenplay category in 2006, losing out to ''[[Psychonauts]]''.<ref name="bafta">{{cite web |url=http://www.bafta.org/awards/video-games/nominations/?year=2006|title=Past Winners and Nominees – Video Games – 2006|accessdate=December 29, 2011|publisher=[[BAFTA]]}}</ref>-->

Eduardo Vasconcellos of [[GameSpy]] praised the game's storyline and high-quality voice work, but criticized the "jagged" visuals that caused character renderings to look "off". He also complained of the lack of responsiveness in the controls, the "disjointed and awkward" camera angles, and the slowness with which some enemies react to the player.<ref name="GameSpy Review"/>

Items from the TV series such as multi-perspective screens (as shown on the box cover) were popular for illustrating multiple viewpoints of an objective.<ref name="GameSpy Review" /> The use of episode start and end graphics to mark the start and end of missions was also liked. The script was generally described as being a redeeming feature of the game, although fans of the series may find that the viewpoints, motives and reasoning of the villains isn't covered well.<ref name="ign review"/> Cutscenes were singled out for particular praise by [[IGN]], highlighting good use of camera angles and tight focus. Voice acting was generally popular, although some lines suffered from poor direction or emphasis.<ref name="ign review"/> In contrast, device and ambiance sounds were well received with gunfire getting a particular mention.<ref name="gamespot review"/>

[[Game mechanic|Gameplay mechanics]] were singled out as being particularly poor. The third-person shooter sections suffered from poor camera-angle management, making targeting enemies feel "awkward and disjointed".<ref name="GameSpy Review"/> Enemy [[artificial intelligence]] for these sections was described as predictable and basic, adopting repeated firing stances or ignoring the character despite being shot repeatedly.<ref name="ign review" /> Sniper variants of this fared better. The vehicle sections were also disappointing, with physics and vehicle handling being rated poorly. Vehicles were described as feeling "slow and clapped out", with pursuits described as being "boring".<ref name="Eurogamer Review"/> The use of AI in these sections were heavily criticized for being predictable and simplistic, with enemy vehicles having no regard for their own safety.<ref name="GameSpy Review"/> The puzzle games were also received badly, being described as "15 shades of easy and 20 shades of terrible".<ref name="gamespot review"/> Interrogation scenes were, in contrast, well liked, with reviewers finding the dialogue during these scenes entertaining.<ref name="GameSpy Review"/>

''[[The Times]]'' gave the game four stars out of five and said it was "more for ''24'' fans, who will instantly love it — so long as they don’t mind another TV addiction to feed."<ref name="times">{{cite news |url=http://www.thetimes.co.uk/tto/arts/article2409682.ece |title=24 — The Game |accessdate=November 19, 2014 |author=James Jackson |date=March 25, 2006 |newspaper=[[The Times]] |archiveurl=https://web.archive.org/web/20070409130729/http://entertainment.timesonline.co.uk/tol/arts_and_entertainment/whats_on/listings/article743693.ece |archivedate=April 9, 2007 |deadurl=no}}{{subscription required}}</ref> ''[[The Sydney Morning Herald]]'' gave it three-and-a-half stars out of five, stating, "Newcomers to the trials and tribulations of Jack Bauer are unlikely to be impressed by this game's generic shooting and driving action. But fans of ''24'' will be quaking, thanks to the authentic atmosphere and gripping story, set between season two and three of the TV series."<ref name="sydney">{{cite news |url=http://www.smh.com.au/news/games/24-the-game/2006/03/14/1142098465824.html |title=24: The Game |accessdate=November 19, 2014 |author=Jason Hill |date=March 16, 2006 |newspaper=[[The Sydney Morning Herald]]}}</ref>  However, ''[[The A.V. Club]]'' gave it a C and stated that "For once, you'll wish you could skip the action to get to the cutscenes."<ref name="avclub"/> ''[[Detroit Free Press]]'' gave it a similar score of two stars out of four and said: "The controls are just too unpolished to make it worthwhile, though. Movements are flaky, and aiming is far too loose. Sometimes, I got into a tight firefight where I couldn't hit a terrorist a few feet in front of me. And the driving missions are worse."<ref name="detroit"/>

{{clear}}

==References==
{{Reflist|30em}}

==External links==
*{{moby game|id=/ps2/24-the-game}}

{{24 (TV series)}}
{{Good article}}

[[Category:2006 video games]]
[[Category:24 (TV series)]]
[[Category:Guerrilla Cambridge games]]
[[Category:PlayStation 2 games]]
[[Category:PlayStation 2-only games]]
[[Category:Single-player-only video games]]
[[Category:Terrorism in fiction]]
[[Category:Video games based on television series]]
[[Category:Video games developed in the United Kingdom]]
[[Category:Video games set in Los Angeles]]