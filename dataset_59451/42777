{{Use dmy dates|date=December 2016}}
{{Use British English|date=December 2016}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name= Ultra-light Helicopter
 | image= Fairey Ult Hc LeB 1957.jpg
 | caption= The fourth Ultra-light Helicopter exhibited on a lorry at the 1957 Paris Air Salon
}}{{Infobox Aircraft Type
 | type= Light Army helicopter
 | national origin= United Kingdom
 | manufacturer= [[Fairey Aviation Company]]
 | designer=
 | first flight= 14 August 1955
 | introduced=
 | retired= 1959
 | status= Retired
 | primary user= The manufacturer
 | number built= 6
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Fairey Ultra-light Helicopter''' was a small British military [[helicopter]] intended to be used for [[reconnaissance]] and [[casualty evacuation]], designed by the [[Fairey Aviation Company]].

The Ultra-light had been conceived of as a straightforward, low cost and easily transportable helicopter. It lacked any tail rotor due to the decision to propel the rotorcraft using unconventional [[tip jet]]s positioned at the ends of the [[rotor blade]]s. It had been selected amongst various competing projects to meet a [[Ministry of Supply]] requirement for a lightweight helicopter to be used by the [[British Army]] for aerial observation purposes. Early trials with prototypes proved promising, however political factors ultimately undermined the project.

The Ultra-light found itself a casualty of the British defence economies of the later 1950s, as well as of intense competition from rival firms who had their own light rotorcraft projects, in particular the [[Saunders-Roe Skeeter]]. While Fairey attempted to proceed with development of the Ultra-light independently, promoting the type towards the civil market and achieving appropriate [[type certification]] for such use, no orders were ultimately received. The firm shelved the project, choosing to concentrate on the larger [[Fairey Rotodyne]] instead, which shared some design features.

==Development==
===Origins===
During the early 1950s, the [[British Army]] was considerable interested in the potential use of compact [[helicopter]]s in the observation and aerial observation roles.<ref name = "wood 111">Wood 1975, p. 111.</ref> In 1953, there was a requirement issued by the British [[Ministry of Supply]] which sought for a low-cost two-seat helicopter, which would be suitable for reconnaissance, casualty evacuation and [[Trainer (aircraft)|training]] duties.<ref name="HAT"/> This specification was considered to be quite demanding, calling for it to be capable of high speeds and quick climb rates even under tropical conditions. The rotorcraft was also required to be transportable on the back of a standard Army three-ton truck, constricting the dimensions of the prospective vehicle considerably.<ref name="HAT">{{Harvnb|Taylor|1974|pp=398–404}}</ref>

Further requirements for the prospective light helicopter included a flight endurance of one hour along with the potential for carrying light cargos such as fuel and tools as well as stretcher-bound wounded troops.<ref name = "wood 111"/> An initial request for a rear-facing observer's seat was present early on, but was discarded in later revisions. At this time, newly developed [[gas turbine]]s were beginning to appeal both to helicopter designers and to prospective operators, the British Army made the use of such an engine one of its requirements.<ref name = "wood 111"/>

There was also an interest in producing rotorcraft that made use of tip-driven [[Helicopter rotor|rotor]]s, which had the advantage of avoiding the [[torque]] that would be produced by rotors driven by a conventional [[transmission (mechanics)|transmission]] as well as the consequential requirement to incorporate a complicated and vulnerable torque-compensating [[tail rotor]].<ref name="HAT"/> Some contemporary designs, such as the [[Sud-Ouest Djinn]], [[bleed air|bled]] high pressure air from the engine directly to the tips; Fairey had been conducting work into a drive where this air was mixed with fuel at the tips and burned. Such methods had already been flight tested on the [[Fairey Jet Gyrodyne]], and Fairey decided that the technique would be suitable for a compact rotorcraft.<ref name="HAT"/><ref name = "wood 111 112 114">Wood 1975, pp. 111-112, 114.</ref>

===Competition and selection===
A diverse range of entries were submitted in response to the issuing of the requirement; amongst these were [[Fairey Aviation]] with its Ultra-light Helicopter harnessing [[Tip jet]] propulsion, [[Saunders-Roe]] with a smaller version of the [[Saunders-Roe Skeeter]], the [[Bristol Aeroplane Company]]'s proposed ''190'', a [[ducted fan|ducted rotor]] proposal by [[Hunting Aircraft|Percival Aircraft]], [[Short Brothers]] proposed the larger [[Short SB.8]], and a [[ram jet]]-powered proposal by [[Germany|German]] helicopter pioneer [[Raoul Hafner]].<ref name = "wood 111 112">Wood 1975, pp. 111-112.</ref>

A suitable powerplant for a compact rotorcraft had been identified in the form of the [[Turbomeca Palouste]] [[turbojet]] engine, for which arrangements had already been independently reached with British firm [[Blackburn Aircraft|Blackburn]] to produce the engine [[Licensed production|under licence]] from [[Turbomeca]].<ref name = "wood 113 114">Wood 1975, pp. 113-114.</ref> Fairey adopted the Palouste engine, which provided compressed air through the hollow [[rotor blade]]s to the tip jets. According to aviation author Derek Wood: "the Fairy Ultra Light was a masterpiece of simplicity. It dispensed with a tail rotor since the tip-driven main rotor was torqueless and it eliminated the shafts, gears, and clutches associated with piston-engined designs".<ref name = "wood 114">Wood 1975, p. 114.</ref>

In response to the detailed design submission that Fairey had produced for their proposal, the Ministry decided to award the firm a contract to produce a total of four development aircraft for demonstration and flight testing purposes; the company later decided to construct a further two more rotorcraft as a private venture.<ref name="HAT"/>

==Design==
[[File:Fairy Ultra-light (6262722737).jpg|thumb|Fairey Ultra-light Helicopter, preserved and on display at the Midland Air Museum, September 2011]]
The Fairey Ultra-light Helicopter emerged as a compact side-by-side two-seater, powered by a single [[Turbomeca Palouste]] [[turbojet]] engine, which was produced under licence by [[Blackburn Aircraft|Blackburn]].<ref name="HAT"/> It was mounted semi-externally at floor level, behind the seats. The engine was furnished with an oversized [[Gas compressor|compressor]] for the purpose of providing air to the [[Tip jet|tip-burners]] at 40 [[Pounds per square inch|psi]] (275 [[kPa]]); this arrangement eliminated the necessity for a tail rotor in order to counteract torque, which was not a factor with the tip jet configuration adopted.<ref name="HAT"/> The Palouste engine was by far the most costly element of the Ultra-light, reportedly comprising in excess of 50 per cent of the craft's purchase cost.<ref name = "wood 114"/>

The Ultra-light was constructed around a light [[alloy]] box that placed the [[rubber]]-mounted rotor pylon at the centre of the upper surface; a tip-driven twin-blade teetering rotor unit was set upon the rotor pylon.<ref name="HAT"/> The rotor diameter was 28&nbsp;ft 3.5 inches, while the overall weight of the Ultra-light was only 1,800&nbsp;lb. Fuel was housed within a single [[Self-sealing fuel tank|crash-proof fuel tank]] set within the box.<ref name = "wood 114"/>

The alloy box structure was attached to and carried a [[box girder]] tail boom on which the engine, a horizontal [[tailplane]] and [[vertical stabilizer]] and [[rudder]] surfaces were mounted.<ref name="HAT"/> The [[rudder]] projected beneath the boom into the jet efflux and provided effective [[Helicopter flight controls|yaw control]] even when the aircraft was stationary. The [[Landing gear|undercarriage]] was a simple pair of tubular [[steel]] skids directly attached to the box structure, porter bars could be attached to the skids to allow the rotorcraft to be carried by hand.<ref name="HAT"/><ref name = "wood 114"/>

==Operational history==
In August 1955, the first prototype Ultra-light helicopter conducted its [[maiden flight]]. It made an public appearance at the September [[Society of British Aircraft Constructors]] (SBAC) show at [[Farnborough Airshow|Farnborough]] that year.<ref name="HAT"/> During the Farnborough demonstration, the Ultra-light performed a demonstration of its ability to operate from the back of a three-ton truck, one of the high-profile requirements that had been issued by the British Army.<ref name = "wood 114"/>

After its completion, several modifications were made to the first prototype. This machine originally had only the rudder at the end of the boom, but a horizontal tailplane bearing end-plate [[fin]]s was later added. A less obvious change was the addition of [[hydraulics|hydraulically]]-assisted [[cyclic pitch]] control. The second prototype had a modified cabin which allowed for stretcher-borne casualties to be carried. A pair of Ultra-lights were equipped with a slightly larger diameter rotor (32&nbsp;ft/9.75&nbsp;m rather than 28&nbsp;ft/8.6&nbsp;m) for the purpose of improving performance in situations where compactness was not at a premium.<ref name="HAT"/> It had been recognised that that there was a need for the rotor blades to have a longer lifespan.<ref name = "wood 114"/>

There were many demonstrations of the Ultra-light's capabilities performed at airshows, during military exercises and at sea, including a deployment aboard the [[U and V-class destroyer|U-class destroyer]] {{HMS|Grenville|R97|6}}.<ref name="HAT"/> However, the British Army had become more focused on the [[Saunders-Roe Skeeter]] and addressing that rotorcraft's shortcomings, such as [[ground resonance]] and engine issues.<ref name = "wood 115">Wood 1975, p. 115.</ref> According to Wood, the rival Skeeter had made a good early impression in Germany and had attracted the offer of an military order for the type from the German government, but that this was on the condition that the Skeeter was in turn adopted by the British armed forces as well. Thus, the decision was taken in [[Whitehall#Government buildings|Whitehall]] to concentrate on the Skeeter, effectively abandoning the Ministry requirement that the Ultra-light was being developed towards meeting.<ref name = "wood 115"/>

Having become aware that the British Army was not longer likely to order the Ultra-light, Fairey instead promoted the type for its potential use within the Royal Navy.<ref name = "wood 115"/> While some trials were conducts, including one prototype being successfully tested for suitability on board an [[aircraft carrier]], the Navy appeared to lack enthusiasm for the Ultra-light and it became clear that the Ministry would not be providing any further funding for its development. Recognising that the prospects for a military order from the British services to have become very limited, the company decided to re-orientate the Ultra-light to conform with the preferences and needs of civil operators instead.<ref name = "wood 115"/>

Fairey proceeded to develop a dedicated model of the Ultra-light for the civil market, specifically designed to be capable of conducting functions such as communications activities and [[agricultural aircraft|crop spraying]].<ref name = "wood 115"/> Accordingly, a civil registered example of the type was exhibited at the 1957 [[Paris Air Show]] at [[Paris Le Bourget Airport]]. During 1958, civil [[Airworthiness certificate|airworthiness certification]] was achieved for the Ultra-light, effectively clearing the type for use by civilian operators.<ref name="HAT"/>

Despite considerable interest from abroad, particularly from customers in the USA and Canada, no orders for the type were received and only the six development Ultra-lights were constructed before the programme was ultimately abandoned in 1959.<ref name="HAT"/> A major factor in Fairey deciding to terminate work on the programme, aside from the lack of orders, was the limited resources available to the company, and the increasing levels of work on the then-promising [[Fairey Rotodyne]], a much larger [[Gyrodyne|compount gyrocopter]] that also made use of tip jets and some of features of the Ultra-light.<ref name = "wood 115"/>

<!-- ==Variants== -->
[[File:Fairey UL Helicopter G-APJJ BAG 18.06.15R edited-2.jpg|thumb|right|The preserved sixth Fairey Ultra-light Helicopter, on static display at the [[Midland Air Museum]] in 2015.]]

==Survivors==
The second prototype of the Ultra-light, which featured a modified nose so that it could accommodate a stretcher for its intended use as a casualty evacuation helicopter. In 1977, it was rediscovered in a derelict condition on a farm near [[Harlow]] and was promptly transferred to [[The Helicopter Museum (Weston)|The Helicopter Museum]], being moved to Weston-super-Mare in May 1979 and put into storage prior to its restoration. A set of original rotor blades, an engine and the original tail unit were acquired. It is currently undergoing long-term restoration at the museum.<ref>[http://www.helicoptermuseum.co.uk/fairey.htm "Fairey."] ''The Helicopter Museum'', Retrieved: 5 February 2017.</ref>

The sixth prototype to be completed, ''G-APJJ'', was tested by the [[Royal Aircraft Establishment]] at Thurleigh, Bedford and by the Royal Navy, before being retired. After periods of lengthy storage it was given for exhibition at the [[Midland Air Museum]] at [[Coventry Airport]] located at Baginton, to the south of the city.<ref>Ellis 2012, p. 257.</ref><ref>[http://www.midlandairmuseum.co.uk/aircraftlist.php "Aircraft Listing."] ''Midland Air Museum'', Retrieved: 5 February 2017.</ref>

==Specifications==
<!--Use one OR other of the two specification templates. Delete the template code of the one you do not use. aero-specs is designed to handle the specification of gliders and lighter-than-air craft well. They each have their own documentation. In aircraft-specifications the parameter "xxxx more" allows for the addition of a qualifier to the value eg "at low level", "unladen". -->
{{aerospecs
|ref=''Fairey Aircraft since 1915''<ref>{{harvnb|Taylor|1974|p=404}}</ref><!-- reference -->
|met or eng?=eng<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->
|crew=2
|capacity=
|length m=4.57
|length ft=15
|length in=0
|span m=
|span ft=
|span in=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=2.49
|height ft=8
|height in=2
|wing area sqm=
|wing area sqft=
|empty weight kg=435
|empty weight lb=959
|gross weight kg=817
|gross weight lb=1,800
|eng1 number=1
|eng1 type=[[Turbomeca Palouste|Turbomeca Palouste BnPe.2]] [[turbojet]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=<!-- prop engines -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|max speed kmh=
|max speed mph=
|cruise speed kmh=153
|cruise speed mph=95
|range km=290
|range miles= (under tropical conditions) 180
|endurance h= max, tropical 2.5 <!-- if range unknown -->
|endurance min= <!-- if range unknown -->
|ceiling m=1,463
|ceiling ft=hovering, out of ground effect 4,800
|climb rate ms=4.83
|climb rate ftmin=sea level 950
|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
* [[Sud-Ouest Djinn]]
|lists=<!-- related lists -->
* [[List of rotorcraft]]
}}

==References==
{{commons category|Fairey Ultra-light Helicopter}}

===Citations===
{{reflist}}

===Bibliography===
{{refbegin}}
* {{cite book |title= Fairey Aircraft since 1915|last= Taylor|first=H.O. |authorlink= |coauthors= |year=1974 |publisher=Putnam Publishing |location=London |isbn= 0-370-00065-X|page= |pages= |url=|ref=harv }}
* Wood, Derek. ''Project Cancelled''. Macdonald and Jane's Publishers, 1975. ISBN 0-356-08109-5.
* {{cite book |title= Wrecks & Relics 23rd Edition|last = Ellis|first = Ken|year=2012|publisher=Crecy Publishing|location = Manchester|isbn = 978-0-85979-172-4}}
{{refend}}

==External links==
* [http://discovery.nationalarchives.gov.uk/details/r/C296675 Collection of test reports on the Ultra-light Helicopter kept at the National Archives]

{{Fairey aircraft}}

{{DEFAULTSORT:Fairey Ultra-Light Helicopter}}
[[Category:British military utility aircraft 1950–1959]]
[[Category:British helicopters 1950–1959]]
[[Category:Fairey aircraft|Ultra-light Helicopter]]
[[Category:Tipjet-powered helicopters]]