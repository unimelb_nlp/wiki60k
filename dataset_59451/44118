{{Infobox airport
| name         = Arendal Airport, Rådhuskaien
| nativename   = 
| image        =
| image-width  = 
| caption      =
| IATA         =
| ICAO         =
| type         = Public
| owner        = Arendal Municipality
| operator     = Arendal Port Authority
| city-served  = [[Arendal]], [[Norway]]
| location     = Rådhuskaien, Arendal
| metric-elev  = yes
| elevation-f  = 0
| elevation-m  = 0
| coordinates  = {{coord|58.4560|N|008.7672|E|region:NO|display=inline,title}}
| pushpin_relief         = yes
| pushpin_map            = Norway
| pushpin_mapsize        = 300
| pushpin_label          = Rådhuskaien
| pushpin_label_position = top
| pushpin_map_caption    = 
| metric-rwy   = yes
| r1-number    =
| r1-length-m  =
| r1-length-f  = 
| r1-surface   = Water
}}

'''Arendal Airport, Rådhuskaien''' ({{lang-no|Arendal flyvehavn, Rådhuskaien}}) was a [[water aerodrome]] in [[Arendal]], [[Norway]], which operated between 1935 and 1939. Situated at Rådhuskaien, it served the scheduled coastal [[seaplane]] service operated by [[Norwegian Air Lines]].

==History==
The first water aerodrome in Arendal was a provisional facility built by the [[Royal Norwegian Navy Air Service]] in 1920. Arendal Naval Air Station was constructed as a landing site off the island of [[Merdø]], several kilometers from the town center. It was manned for 54 days between 12 July and 11 September before being closed. The Navy Air Service resumed use of the station for some weeks during late 1939.<ref>{{cite book |last=Hafstad |first=Bjørn |last2=Arheim |first2=Tom |title=Marinens flygevåpen 1912–1944 |publisher=TankeStreken |year=2003 |isbn=82-993535-1-3 |language=Norwegian |page=38}}</ref>

The need for an airport arose again in 1935, when Norwegian Air Lines commenced a coastal air service from Oslo to Bergen. The airline's [[Junkers Ju 52]] ''Havørn'' landed in the town on 11 June on a trial route. The first period the aircraft anchored at buoy and passengers and cargo were transported there with a boat. The better the conditions a floating dock was construction at Rådhuskaien, the docks off [[Arendal Town Hall]]. The route was taken over by the Ju 52 ''Ternen'' from 1936, following the [[Havørn Accident|''Havørn'' Accident]].<ref name=frostrup>{{cite book |last=Frøstrup |first=Johan Christian |title=Det var en gang – Arendal i tekst og bilder |publisher=P.M. Danielsen |year=1996  |location=Arendal |isbn=8291495025 |language=Norwegian |page=122–123 |url=http://www.nb.no/nbsok/nb/716ed97c56af3db267a38f460a43677d}}</ref>

Meanwhile, planning of a land airport for [[Agder]] was launched. Arendal Municipal Council proposed that the town be selected as host, and proposed Vessøyslettene as a suitable location in May 1935. [[Kristiansand Airport, Kjevik]] was selected as Agder's main airport, opening in 1939. This caused the coastal service past Agder to be terminated.<ref name=frostrup /> Arendal would ultimately receive a general aviation airport with the 1996 opening of [[Arendal Airport, Gullknapp]].<ref>{{cite news |title=For tunge passasjerer årsak til flyhavari ved Arendal |agency=[[Norwegian News Agency]] |date=15 January 1997 |language=Norwegian}}</ref>

==Facilities==
The airport was situated on the public docks in the town center of Arendal at Rådhuskaien off [[Arendal Town Hall]]. It consisted of floating docks and a small operations building. The aircraft used Byfjorden as their runway.<ref name=frostrup />

==References==
{{reflist}}

{{Airports in Norway}}
{{Portal bar|Aviation|Norway}}

{{DEFAULTSORT:Arendal Airport, Radhuskaien}}
[[Category:Airports in Aust-Agder]]
[[Category:Water aerodromes in Norway]]
[[Category:Arendal]]
[[Category:1935 establishments in Norway]]
[[Category:1939 disestablishments in Norway]]
[[Category:Airports established in 1935]]