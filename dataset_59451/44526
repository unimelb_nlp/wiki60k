{{Infobox book  | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:_Books -->
| name           = Nick of the Woods
| title_orig     =
| translator     =
| image          = Robert Montgomery Bird Die Gefahren der Wildnis Vogel 001.jpg
| image_size     = 
| caption  = 1883 illustration from German edition of novel ''Die Gefahren der Wildnis'' by [[Hermann Vogel (German illustrator)|Hermann Vogel]]
| author         = [[Robert Montgomery Bird]]
| cover_artist   =
| country        = United States
| language       = English
| series         = 
| genre          = 
| pages          = Two vol. (1837 ed.) (240 pg. Vol. I; 246 pg. Vol. II); 1837 English ed. is Vol. I-III
| publisher      = [[Mathew Carey|Carey]], Lea and Blanchard
| release_date   = March 1837 (United States)<ref name="birdbio">Foust, Clement Edgar. [https://books.google.com/books?id=w19BAQAAMAAJ&pg=PA96#v=onepage&q&f=false The Life and Dramatic Works of Robert Montgomery Bird] (1919), p. 96 (month of publication), p. 155 (Mrs. Bird reports the German translation sold over 10,000 copies; [[Samuel W. Pennypacker]], who was a Governor of [[Pennsylvania]] (1903-07), reportedly "sat up all night" reading the book as a child)</ref>
| isbn           = <!-- NA -->
| preceded_by    = 
| followed_by    = 
}}

'''''Nick of the Woods; or, The Jibbenainesay ''''' is an 1837 novel by American author [[Robert Montgomery Bird]].  Noted today for its savage depiction of Native Americans, it was Bird's most successful novel and a best-seller at the time of its release.<ref name="ashgate1">Weinstock, Jeffrey [https://books.google.com/books?id=NI1zBAAAQBAJ&pg=PA437#v=onepage&q&f=false The Ashgate Encyclopedia of Literary and Cinematic Monsters], p. 437 (2014)</ref>

==Publication==

The novel was eventually published in twenty-three editions in English, and four translations, including a best-selling German translation by [[Gustav Höcker]].<ref name="birdbio"/><ref name="hart">Hart, James D. [https://books.google.com/books?id=ZHrPPt5rlvsC The Popular Book: A History of America's Literary Taste], p. 80 (1951)</ref>  The long popularity of the novel is evidenced by the fact that [[Mark Twain]] referenced the main character of the book in 1883's ''[[Life on the Mississippi]]'', presuming the audience would know the reference.<ref name="birdbio"/><ref name="hart"/>

==Plot and reception==

The novel is set in [[Kentucky]] in the 1780s and revolves around the mysterious figure of "Nick of the Woods", dressed as a monster, who seeks to avenge the death of his family by killing numerous Indians, carving a cross on the body of all he slays.  "Nick" is revealed to be Nathan Slaughter, a [[Quakers|Quaker]] by day who should by nature and creed avoid all violence.  Bird's brutal depiction of Native Americans (the [[Shawnee]]) was quite hostile, and in part a reaction to the more positive representation of Indians by [[James Fennimore Cooper]] in the [[Leatherstocking Tales]].<ref name="ashgate1"/><ref name="shade">Lubbers, Klaus. [https://books.google.com/books?id=PPsliWfwJmUC&pg=PA279#v=onepage&q&f=false Born for the Shade: Stereotypes of the Native American in United States Literature and the Visual Arts, 1776-1894], pp. 279-82 (1994)</ref>

The novel has been called a "prominent example of the American Gothic form."<ref name="ashgate1"/>  ''The Columbia Companion to American History on Film'', which dubs Nathan Slaughter "a one-man genocide squad", also credits the novel for popularizing the mode of unintelligent Indian speaking ("Me Inju-man! ... Me kill all white man!") used by many later authors and in movies.<ref name="columbia">Rollins, Peter C. (ed.) [https://books.google.com/books?id=xB1rhm6Ke2UC&pg=PA277#v=onepage&q&f=false The Columbia Companion to American History on Film], pp. 277-78 (2003)</ref>

==Adaptations==

The novel was also adapted for the stage in at least three versions, the most popular one by Louisa Medina.<ref name="snod">Snodgrass, Mary Ellen. [https://books.google.com/books?id=zEAcjezGec4C&pg=PA28#v=onepage&q&f=false Encyclopedia of Gothic Literature], pp. 27-28 (2005)</ref><ref name="performing">Hall, Roger A. [https://books.google.com/books?id=dHitfBUfKdEC&pg=PA29&dq=#v=onepage&q&f=false Performing the American Frontier, 1870-1906], p. 29 (2001)</ref>  The Medina version debuted at the [[Bowery Theatre]] in New York on February 5, 1838 to great success, although a fire burned down the house after two weeks.  It returned to the Bowery in 1839 when it re-opened.  The role of Nick became a lifetime starring role for actor [[Joseph Proctor]].<ref name="bank">Bank, Rosemarie K. [https://books.google.com/books?id=b9jDYosmZnAC&pg=PA72&dq=#v=onepage&q&f=false Theatre Culture in America, 1825-1860], p. 72 (1997)</ref>  In his introduction to ''Victorian Melodramas'' (1976), James L. Smith called the play "the most successful American melodrama for more than half a century."<ref name="wom">Rodriguez, Miriam Lopez (ed.) [https://books.google.com/books?id=_525vTl53sgC&pg=PA35#v=onepage&q&f=false Women's Contribution to Nineteenth-century American Theatre], p. 35 (2004)</ref>

==References==
{{reflist|2}}

==External links==
* [http://www.gutenberg.org/ebooks/13970 Nick of the Woods] at gutenberg.org
* [https://archive.org/details/nickofwoodsorjib01bird Nick of the Woods, Vol. I, 1837, Carey, Lea & Blanchard], at archive.org
* [https://archive.org/details/nickofwoodsorjib02bird Nick of the Woods, Vol. II, 1837, Carey, Lea & Blanchard], at archive.org
* [https://books.google.com/books?id=AE4EAAAAQAAJ&pg=RA1-PA255&dq=#v=onepage&q&f=false Nick of the Woods, Vol I-III, 1837] (English edition, edited by [[William Harrison Ainsworth|W. Harrison Ainsworth]]), at Google Books
* [https://books.google.com/books?id=0dVZAAAAcAAJ Nick of the Woods. A drama in three acts], Medina play, at Google Books
* [https://www.library.upenn.edu/exhibits/rbm/bird/case6.html Nick of the Woods entry], at [[Penn Library]] Exhibition on Robert Montgomery Bird

[[Category:Novels set in Kentucky]]
[[Category:1837 novels]]
[[Category:Novels set in the 1780s]]
[[Category:Novels set in the American colonial era]]
[[Category:19th-century American novels]]
[[Category:Fictional characters introduced in 1837]]