{{redirect|Black Angus|the steakhouse|Black Angus Steakhouse}}
{{Use dmy dates|date=November 2015}}
{{Infobox cattle breed
| name          = Angus 
| image         = File:Angus cattle 18.jpg
| image_size    = 280px
| image_alt     = A black angus bull seen here side on
| image_caption = A black angus bull
| status        = Least Concern
| altname       =Aberdeen Angus
| country       = [[Scotland]]
| distribution  = Europe and Australasia
| standard      = 
| use           = Beef
| nickname      =Doddies
Hummlies
| maleweight    =850 kg
| femaleweight  =550 kg
| maleheight    =
| femaleheight  =
| skincolor     =
| coat          = Black or Red
| horn          =  Polled
| subspecies    = taurus
| note          =
}}'''Angus cattle''', known as '''Aberdeen Angus''' in most parts of the world, are a [[List of cattle breeds|breed of cattle]] commonly used in [[beef]] production. They were developed from cattle native to the counties of [[Aberdeenshire (historic)|Aberdeenshire]] and [[Angus, Scotland|Angus]] in Scotland.<ref>''Encyclopædia Britannica'' 15th Ed. Vol.10 p.1280</ref>

Angus cattle are naturally [[Polled livestock|polled]] and solid black or red even though the [[udder]] may be white. The native colour is black, but more recently red colours have emerged.<ref name=":0">{{cite web|url=http://www.ansi.okstate.edu/breeds/cattle/redangus/|title=Oklahoma State University Red Angus breed profile}}</ref> The UK registers both in the same herd book, but in the US they are regarded as two separate breeds&nbsp;– Red Angus and Black Angus. Black Angus is the most common breed of beef cattle in the US, with 324,266 animals registered in 2005.<ref name="faq">{{cite web|last=American Angus Association |title=Angus - FAQs |publisher=angus.org |url=http://www.angus.org/pubs/faqs.htm |doi= |accessdate=23 September 2006 |archiveurl=https://web.archive.org/web/20060924011236/http://www.angus.org/pubs/faqs.htm |archivedate=24 September 2006 |deadurl=yes |df=dmy }}</ref><ref>{{cite web | last = Virginia Cooperative Extension | title = Beef Cattle Breed Associations Seedstock List | publisher = VirginiaTech. | url = http://www.ext.vt.edu/news/periodicals/livestock/aps-97_10/aps-831.html | doi = | accessdate = 23 September 2006 |archiveurl = https://web.archive.org/web/20060902111840/http://www.ext.vt.edu/news/periodicals/livestock/aps-97_10/aps-831.html |archivedate = 2 September 2006}}
</ref> In 2014, the British Cattle Movement Service named Angus the UK's most popular native beef breed, and the second most popular beef breed overall.<ref>{{cite news|title=Aberdeen-Angus breed increases influence on British Beef industry|url=http://www.aberdeen-angus.co.uk/news/aberdeen-angus-breed-increases-influence-on-british-beef-industry/|accessdate=29 April 2015|issue=16 March 2015|publisher=Aberdeen Angus Cattle Society}}</ref>

==History==

===Scotland===
Aberdeen Angus cattle have been recorded in Scotland since at least the 16th century in the country's North East.<ref>{{Cite web|url=http://www.britannicrarebreeds.co.uk/breedinfo/cow_aberdeenangus.php |title=Britannic Rare Breeds - Angus Cattle |date= |accessdate=25 June 2015 |website=Britannic Rare Breeds |publisher= |last= |first= |deadurl=yes |archiveurl=https://web.archive.org/web/20150620090121/http://www.britannicrarebreeds.co.uk/breedinfo/cow_aberdeenangus.php |archivedate=20 June 2015 |df=dmy }}</ref> For some time before the 1800s, the hornless cattle in [[Aberdeenshire]] and [[Angus, Scotland|Angus]] were called ''Angus doddies''. In 1824, [[William McCombie]] of Tillyfour, [[Member of Parliament|M.P.]] for South Aberdeenshire, began to improve the stock and is regarded today as the father of the breed.<ref name=":0" /> Many local names emerged, including ''doddies'' or ''hummlies''. The first herd book was created in 1862, and the society was formed in 1879. This is considered late, given that the cattle gained mainstream acceptance in the middle of the eighteenth century. The cattle became commonplace throughout the British Isles in the middle of the 20th century.<ref>{{Cite web|url = http://www.thecattlesite.com/breeds/beef/7/aberdeen-angus/|title = The Cattle Site - Angus Breeds|date = |accessdate = 25 June 2015|website = The Cattle Site|publisher = |last = |first = }}</ref>

===Argentina===
As stated in the fourth volume of the Herd Book of the UK's Angus, this breed was introduced to Argentina in 1879 when "''Don'' Carlos Guerrero" imported one bull and two cows for his ''Estancia'' "Charles" located in [[Juancho]], [[Partido de General Madariaga]], [[Provincia de Buenos Aires]]. The bull was born on April 19, 1878; named "Virtuoso 1626" and raised by Colonel Ferguson. The cows were named "Aunt Lee 4697" raised by J. James and "Cinderela 4968" raised by R. Walker and were both born in 1878, on January 31 and April 23, respectively.<ref>[http://www.charlesdeguerrero.com/historia.php Historia de la Cabaña Charles de Guerrero, criadora de Angus desde 1879]</ref>

===Australia===
Angus cattle were first introduced to [[Tasmania]] (then known as Van Diemen's Land) in the 1820s and to the southern mainland in 1840. The breed is now found in all Australian states and territories with 62,000 calves registered with Angus Australia in 2010.<ref>http://www.angusaustralia.com.au/Flyers/AngusCattleinAustralia.pdf</ref>

===Canada===
In 1876 William Brown, a professor of agriculture and then superintendent of the experimental farm at [[Guelph, ON|Guelph, Ontario]], was granted permission by the government of Ontario to purchase Aberdeen Angus cattle for the [[Ontario Agricultural College]]. The herd comprised a yearling bull, Gladiolus, and a cow, Eyebright, bred by the Earl of Fife and a cow, Leochel Lass 4th, bred by R.O. Farquharson. On January 12, 1877, Eyebright gave birth to a calf, sired by Sir Wilfrid. It was the first to be born outside of Scotland. The OAC went on to import additional additional bulls and cows, eventually began selling Aberdeen Angus cattle in 1881.<ref>{{cite news|title=First Herd of Aberdeen-Angus Established by OAC in 1876|work=Kitchener-Waterloo Record|date=6 March 1954|page=2|format=Microfilm}}</ref>

===United States===
On 17 May 1873, George Grant brought four Angus bulls, without any cows, to [[Victoria, Kansas]]. These were seen as unusual as the normal American cattle consisted of [[Shorthorn]]s and [[Texas Longhorn|Longhorns]], and the bulls were used only in crossbreeding. However, the farmers noticed the good qualities of these bulls and afterwards, many more cattle of both sexes were imported.<ref name="legends">
{{cite book
 | last = Burke
 | first = Tom
 | authorlink =
 |author2=Kurt Schaff |author3=Rance Long
  | editor =
 | others =
 | title = Angus Legends: Volume 1
 | origyear = 2004
 | url =
 | accessdate =
 | edition =
 | year = 2004
 | publisher =
 | location =
 | language =
 | id =
 | doi =
 | page = 17
 | chapter = The Birth of the Breed
 | chapterurl =
 | quote =
 }}
</ref>

On 21 November 1883, the [[American Angus Association]] was founded in [[Chicago]], [[Illinois]].<ref name="aaa">{{cite web
 | last = American Angus Association
 | title = Angus History
 | publisher = angus.org
 | url = http://www.angus.org/ang_hist.htm
 | doi =
 | accessdate = 2 October 2006 | archiveurl= https://web.archive.org/web/20060924002712/http://www.angus.org/ang_hist.htm| archivedate= 24 September 2006 | deadurl= no}}
</ref> The first herd book was published on March 1885.<ref name="legends" /> At this time both red and black animals were registered without distinction. However, in 1917 the Association barred the registering of red and other colored animals in an effort to promote a solid black breed.<ref name="red">{{cite web
 | last = Red Angus Association of America
 | title = History of Red Angus
 | publisher = redangus.org
 | url = http://redangus.org/association/history/
 | doi =
 | accessdate = 2 October 2006 | archiveurl= https://web.archive.org/web/20060924003230/http://redangus.org/association/history/| archivedate= 24 September 2006 | deadurl= no}}
</ref>

The [[Red Angus Association of America]] was founded in 1954 by breeders of Red Angus cattle. It was formed because the breeders had had their cattle struck off the herd book, for not conforming to the changed breed standard regarding colour.<ref name="red" /> {{Clear}}

===Germany===
A separate breed was cross bred in Germany called the [[German Angus cattle|German Angus]]. It is a cross between the Angus and several different cattle such as the [[German Black Pied Cattle]], [[Gelbvieh]], and [[Fleckvieh cattle|Fleckvieh]]. The cattle are usually larger than the Angus and appear in black and red colours.<ref>{{Cite web|url = http://interboves.com/eng/breeds.html|title = German Angus cattle information.|date = |accessdate = 10 August 2015|website = Interboves|publisher = |last = |first = }}</ref>

== Characteristics ==
Because of their native environment, the cattle are very hardy and can survive the Scottish winters, which are typically harsh, with snowfall and storms. Cows typically weigh {{convert|550|kg|lb}} and bulls weigh {{convert|850|kg|lb}}.<ref name=":3">{{Cite web|url = http://www.rbst.org.uk/layout/set/print/Rare-and-Native-Breeds/Cattle/Aberdeen-Angus-Native|title = Aberdeen Angus (Native) |date = |accessdate = 25 June 2015|work = Factsheet |publisher =Rare Breeds Survival Trust |location =  Kenilworth, Warwickshire |author = RBST}}</ref> Calves are usually born smaller than is acceptable for the market, so crossbreeding with dairy cattle is needed for veal production.<ref name=":3" /> The cattle are naturally [[Polled livestock|polled]] and black in colour. They typically mature earlier than other native British breeds such as the [[Hereford (cattle)|Hereford]] or [[North Devon cattle|North Devon]]. However, in the middle of the 20th century a new strain of cattle called the Red Angus emerged.<ref>{{Cite web|url = http://www.britannica.com/animal/Angus-breed-of-cattle|title = Encyclopaedia Britannica - Cattle Breeds|date = |accessdate = 25 June 2015|website = Encyclopaedia Brittanica|publisher = |last = |first = }}</ref><ref name=":2">{{Cite web|url = http://assets.redangus.org/media/Documents/Association/Red_Angus_History_Brochure.pdf|title = Red Angus History|date = |accessdate = 2 August 2015|website = |publisher = |last = |first = |page = 2}}</ref> The United States does not accept Red Angus cattle into herd books, while the UK and Canada do.<ref name=":2" /> Except for their colour genes, there is no genetic difference between black and red Angus, but they are regarded as different breeds in the US. However, there have been claims that black angus are more sustainable to cold weather, though unconfirmed.<ref name=":2" />

The cattle have a large muscle content and are regarded as medium-sized. The meat is very popular in [[Japan]] for its [[Marbled meat|marbling]] qualities.<ref name=":1">{{Cite web|url = http://www.dpi.nsw.gov.au/agriculture/livestock/beef/breeding/breeds/angus|title = New South Wales Agriculture - Angus cattle|date = |accessdate = 25 June 2015|website = |publisher = |last = |first = }}</ref>[[Image:Red-angus.jpg|thumb|250px|Mixed herd of Black and Red Angus]]

===Genetic disorders===
There are four [[recessive gene|recessive]] defects that can affect calves worldwide. A recessive defect occurs when both parents carry a recessive gene that will affect the calf. One in four calves will show the defect even when both parents carry the defective gene. The four recessive defects in the Black Angus breed that are currently managed with DNA tests are: Arthrogryposis Multiplex (AM), referred to as ''curly calf'', which lowers the mobility of joints; Neuropathic Hydrocephalus (NH), sometimes known as ''water head'', which causes an enlarged malformed skull; Contractural Arachnodactyly (CA), formerly referred to by the name of "Fawn Calf Syndrome", which reduces mobility in the hips; and [[Dwarfism]], which impacts the size of calves. Both parents need to carry the genes for a calf to be affected with one of these disorders.<ref>{{cite web| last=Denholm|first=Laurence|title=Congenital contractural arachnodactyly ('fawn calf syndrome') in Angus cattle |url= http://www.dpi.nsw.gov.au/__data/assets/pdf_file/0011/336944/Congenital-contractural-arachnodactyly-in-Angus-cattle.pdf|format=PDF |publisher=NSW Department of Trade and Investment PrimeFact 1015 May 2010}}</ref><ref>Vidler, Adam, ''Defects on rise as gene pool drains'', p. 63, The Land, Rural Press, North Richmond, NSW</ref><ref>[http://www.hpj.com/archives/2009/apr09/apr27/Anothergeneticdefectaffects.cfm Another genetic defect affects Angus cattle] Retrieved on 29 May</ref>
Because of this, the American Angus Association will remove the carrier cattle from the breed in an effort to reduce the number of cases.<ref>{{cite web|url=http://www.angus.org/pub/CA/CAInfo.aspx/ |title=American Angus Association |publisher=Angus.org |accessdate=14 May 2012}}</ref>

Between 2008 and 2010, the American Angus Association reported worldwide [[Dominance (genetics)|recessive]] genetic disorders in Angus cattle. It has been shown that a small minority of Angus cattle can carry [[osteoporosis]].<ref>{{cite web |title = Heritable Birth Defects in Angus Cattle|url = http://www.appliedreprostrategies.com/2010/august/pdfs/3-1_whitlock.pdf|format = PDF|publisher = Appliedreprostrategies.com|last = Whitlock|first = Brian K.|accessdate = 24 August 2015}}</ref> A further defect called notomelia, a form of [[polymelia]] ("many legs") was reported in the Angus breed in 2010.<ref>{{cite web |title=Denholm L et al(2010) Polymelia (supernumerary limbs) in Angus calves|url= http://www.flockandherd.net.au/cattle/reader/polymelia.html}}</ref>

==Uses==
The main use of Angus cattle is for [[beef]] production and consumption. The beef can be marketed as superior due to its marbled appearance. This has led to many markets, including Australia, Japan and the United Kingdom to adopt it into the mainstream.<ref name=":1" /> Angus cattle can also be used in [[crossbreeding]] to reduce the likelihood of [[dystocia]] (difficult calving), and because of their dominant polled gene, they can be used to crossbreed to create polled calves.<ref>{{cite web
 | last =
 | title = Angus
 | publisher = Cattle Today
 | url = http://cattle-today.com/angus.htm
 | accessdate = 29 October 2006 | archiveurl= https://web.archive.org/web/20061017084329/http://cattle-today.com/angus.htm| archivedate= 17 October 2006 | deadurl= no}}
</ref>[[Image:blackangus.jpg|thumb|250px|Angus calf with its mother]]

=== Commercial ===
Starting in the early 2000s, the American [[fast food]] industry began running a [[public relations]] campaign to promote the supposedly superior quality of Angus beef. Beginning in 2006, [[McDonald's]] commenced testing on hamburgers made with Angus beef in several regions in the US. After this test, the company said that customer response to the burgers was positive<ref>{{cite web|last=Weston |first=Nicole |url=http://www.slashfood.com/2007/03/08/new-angus-third-pounders-at-mcdonalds/ |title=New Angus Third-Pounders at McDonald's |publisher=Slashfood |date=8 March 2007 |accessdate=14 May 2012}}</ref> and began selling the burger made with Angus beef in all US locations in July 2009.<ref>{{cite web
 | title = McDonald's to debut $4 Angus burger
 | publisher = MSNBC / The Associated Press
 | url = http://www.msnbc.msn.com/id/31686986/
 | accessdate = 1 July 2009 }}
</ref> In response to the test in the US, [[McDonald's|McDonald's Australia]] began selling two Angus burgers, the Grand Angus and the Mighty Angus, using [[Australia]]n-bred Angus, in their restaurants.<ref>{{cite web|url = http://mcdonalds.com.au/#/angus-beef|title = McDonald's - Angus Beef|publisher = McDonald's Australia}}</ref>

The American Angus Association created the "Certified Angus Beef" (CAB) standard in 1978. The purpose of this standard was to promote the idea that Angus beef was of higher quality than beef from other breeds of cattle. Cattle are eligible for "Certified Angus Beef" evaluation if they are at least 51% black and exhibit Angus influence, which include black [[Simmental Cattle|Simmental cattle]] and crossbreds. However, they must meet all 10 of the following criteria, which were refined in January 2007 to further enhance product consistency, in order to be labeled "Certified Angus Beef" by USDA Graders:<ref>{{cite web
 | last =
 | title = Angus FAQs
 | publisher = American Angus Association
 | url = http://www.angus.org/Pub/FAQs.aspx
 | accessdate = 2 August 2013}}
</ref>

* Modest or higher degree of marbling
* Medium or fine marbling texture
* "A" maturity
* 10 to 16 square-inch ribeye area
* Less than 1,000-pound hot carcass weight
* Less than 1-inch fat thickness
* Moderately thick or thicker muscling
* No hump on the neck exceeding 5&nbsp;cm (2")
* Practically free of capillary rupture
* No [[darkcutter|dark cutting]] characteristics
* Usually black or red in color

==See also==
* [[List of cattle breeds originating in Scotland|List of Cattle breeds originating in Scotland]]
* [[List of cattle breeds]]

==References==
{{reflist|30em}}

==External links==
{{Commons category|Angus cattle}}
* [http://www.ansi.okstate.edu/breeds/cattle/angus/ History of the Angus cattle] - Oklahoma State University
* [http://www.ksre.ksu.edu/library/agec2/mf2664.pdf Factors Affecting the Selling Price of Purebred Angus Bulls]

===Breed associations===
Unless otherwise stated, the associations below register both red and black animals.
* [http://www.aberdeen-angus.co.uk/ Official website of the Aberdeen Angus Cattle Society]
* [[Irish Angus Cattle Society Ltd.]]

Australia:
* [http://www.angusaustralia.com.au/ The Angus Society of Australia]

Canada:
* [http://www.cdnangus.ca/ Canadian Aberdeen Angus Association]
* [http://www.ontarioangus.com Ontario Angus Association (Canada)]

Denmark:
* [http://www.danskangus.dk/ Danish Aberdeen Angus Association]

New Zealand:
* [http://angusnz.com/ New Zealand Angus Association]

Portugal
* [http://www.angus.com.pt/ Angus Portugal]

US:
* [http://www.angus.org/ American Angus Association] - responsible for black Angus registrations, but does not register red Angus cattle.
* [http://redangus.org/ Red Angus Association] - responsible for red Angus registrations, and will also register black Angus cattle.
* [http://www.alotangus.org A*L*O*T Angus Association] (Arkansas, Louisiana, Oklahoma, Texas) -- represents the owners of black Angus cattle in four states.
* [http://iowaangus.org/ Iowa Angus Association]—represents the owners of black Angus cattle.
* [http://www.angus1.com/mvaa/ Miami Valley Angus Association]—represents the owners of black Angus cattle.
* [http://www.texasangus.com/ Texas Angus Association]—represents the owners of black Angus cattle in the State of Texas.

{{British livestock|state=collapsed}}

[[Category:Cattle breeds originating in Scotland]]
[[Category:Beef cattle breeds]]
[[Category:Cattle breeds]]