{{Infobox journal
| title = Annali dell'Istituto Superiore di Sanità
| cover = [[File:2007 cover Ann ISS.jpg]]
| former_name =
| abbreviation = Ann. Ist. Super. Sanità
| discipline = [[Health sciences]]
| editor = [[Enrico Alleva]]
| publisher = [[Istituto Superiore di Sanità]]
| country = Italy
| history = 1965-present
| frequency = Quarterly
| openaccess = Yes
| license = 
| impact = 0.773
| impact-year = 2013
| ISSN = 0021-2571
| eISSN =
| CODEN = AISSAW
| JSTOR = 
| LCCN = 71419690
| OCLC = 476359621
| website = http://www.iss.it/anna/index.php?lang=2
| link2 = http://www.iss.it/publ/index.php?lang=1&tipo=3
| link2-name = Online archive
}}
The '''''Annali dell'Istituto Superiore di Sanità''''' is a quarterly [[peer-reviewed]] [[open access]] [[scientific journal]] covering [[biomedicine]], the [[health sciences]], and [[translational research]]. It is published by the [[Istituto Superiore di Sanità]] (ISS) and was established in 1965. It was the successor to the ''Rendiconti Istituto Superiore di Sanità'', which were established in 1938 and published until 1962. The [[editor-in-chief]] is [[Enrico Alleva]] (ISS).

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Index Medicus]]/[[MEDLINE]]/[[PubMed]],<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/7502520 |title=Annali dell'Istituto Superiore di Sanità |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |format= |accessdate=2014-12-24}}</ref> [[EMBASE]]/[[Excerpta Medica]], [[SCIELO]] , [[Science Citation Index Expanded]], and [[The Zoological Record]].<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2014-12-24}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 0.773.<ref name=WoS>{{cite book |year=2014 |chapter=Annali dell'Istituto Superiore di Sanità |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|1=http://www.iss.it/anna/index.php?lang=2}}

{{DEFAULTSORT:Annali dell'Istituto Superiore Di Sanita}}
[[Category:Open access journals]]
[[Category:Public health journals]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Academic journals published by independent research institutes]]