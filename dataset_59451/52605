{{Infobox journal
| title = Polyhedron
| cover = [[File:Polyhedron (journal).gif]]
| editor = G. Christou, C.E. Housecroft
| discipline = [[Inorganic chemistry]]
| formernames = Journal of Inorganic and Nuclear Chemistry
| abbreviation = Polyhedron
| publisher = [[Elsevier]]
| country = 
| frequency = 18/year
| history = 1955–present
| openaccess = 
| license =
| impact = 2.108
| impact-year = 2015
| website = http://www.journals.elsevier.com/polyhedron/
| link1 = http://www.sciencedirect.com/science/journal/02775387
| link1-name = Online access
| link2 = http://www.sciencedirect.com/science/journal/00221902
| link2-name = Archives of ''Journal of Inorganic and Nuclear Chemistry'' (1955–1981)
| JSTOR = 
| OCLC = 771157632
| LCCN = 82643589
| CODEN = PLYHDE
| ISSN = 0277-5387
| eISSN = 
}}
'''''Polyhedron''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] covering the field of [[inorganic chemistry]]. It was established in 1955 as the '''''Journal of Inorganic and Nuclear Chemistry''''' and is published by [[Elsevier]].

== Abstracting and indexing ==
''Polyhedron'' is abstracted and indexed in:
{{columns-list|colwidth=30em|
*[[BIOSIS]]
*[[Chemical Abstracts]]
*[[Current Contents]]/Physics, Chemical, & Earth Sciences
*[[Inspec]]
*[[Science Citation Index]]
*[[Scopus]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 2.011.<ref name=WoS>{{cite book |year=2015 |chapter=Polyhedron |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official Website|http://www.journals.elsevier.com/polyhedron/}}

[[Category:Inorganic chemistry journals]]
[[Category:Elsevier academic journals]]
[[Category:Publications established in 1955]]
[[Category:English-language journals]]