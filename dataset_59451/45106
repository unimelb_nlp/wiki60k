{{DISPLAYTITLE:One Hundred (''Aqua Teen Hunger Force'')}}
{{Infobox television episode
| title        = One Hundred
| series       = Aqua Teen Hunger Force
| image        = [[File:Aqua Teen Hunger Force - One Hundred.jpg|250px]]
| caption      = [[Frylock]], [[Meatwad]], and [[Master Shake|Shake]] depicted in a [[Scooby-Doo]]-like style.
| season       = 7
| episode      = 12
| airdate      = May 2, 2010
| production   = 712<ref name=Season7list>[http://wiki.adultswim.com/xwiki/bin/Aqua+Teen+Hunger+Force/Season+9 Official ''Aqua Teen Hunger Force'' Season 7 Episode Guide] from [[Adult Swim]].</ref>
| writer       = Dave Willis<br>Matt Maiellaro
| director     = [[Dave Willis]]<br>[[Matt Maiellaro]]
| guests       =
* [[Robert Smigel]] as One Hundred
* Nick Weidenfeld as a television executive 
* [[Tom Savini]] as a Cop
* [[Amber Nash]] as Tabitha
| episode_list = [[Aqua Teen Hunger Force (season 7)|''Aqua Teen Hunger Force'' (season 7)]]<br>[[List of Aqua Teen Hunger Force episodes|List of ''Aqua Teen Hunger Force'' episodes]]
| prev        = Kangarilla and the Magic Tarantula
| next        = [[Allen (Aqua Unit Patrol Squad 1)|Allen Part One]]
}}

"'''One Hundred'''" is the twelfth and final episode of the [[Aqua Teen Hunger Force (season 7)|seventh season]] of the animated television series ''[[Aqua Teen Hunger Force]]'' and is the 100th episode of the series overall.<ref name="Press release">{{cite web|title=PRESS RELEASES: ATHF: Aqua Teen Press Release|url=http://video.adultswim.com/press-releases/athf-aqua-teen-press-release.html|publisher=[[Adult Swim Video]]|accessdate=22 May 2011}}</ref> "One Hundred" originally aired in the [[United States]] on May 2, 2010, on [[Adult Swim]]. In the episode [[Frylock]] obsesses about the number [[100 (number)|100]] while [[Master Shake]] attempts to put ''Aqua Teen Hunger Force'' into [[Broadcast syndication|syndication]], until the episode abruptly turns into a parody of ''[[Scooby-Doo]]''.

This episode marks the second time an Adult Swim original series made it to [[100 episodes]], the first being ''[[Space Ghost Coast to Coast]]''. "One Hundred" strongly parodies the [[Hanna-Barbera]] cartoon, ''Scooby-Doo''. This is the final episode of the series to premiere branded as an ''Aqua Teen Hunger Force'' episode before the series started using [[Aqua Teen Hunger Force#Alternative titles|alternative titles]] for each season as a [[running gag]]. This episode received a positive review by Ramsey Isler of [[IGN]], and was the third highest rated program the night of its debut. This episode has been made available on [[DVD]], and other forms of home media, including [[Video on demand|on demand]] streaming on [[Hulu|Hulu Plus]].

==Plot==
[[Frylock]] is obsessed with the number [[100 (number)|100]]. [[Master Shake]] then flies off to California to meet with television executives, and an animated version of [[Dana Snyder]], the actual voice actor who provides the voice of Master Shake, pops out of him, demanding [[Broadcast syndication|syndication]] money, claiming that it is the hundredth episode, so he deserves it. A television executive replies that because ''Aqua Teen Hunger Force'' is only eleven minutes long, they only have fifty half-hours of material. Dana Snyder then storms out of the room claiming that he will be back in another eight years with another fifty half-hours of material ready.

Shortly after Master Shake returns home, One Hundred, a giant yellow monster in the shape of the number 100, appears and sends Master Shake, Frylock, and [[Meatwad]] into a different world, which features the main characters in a [[Hanna-Barbera]]-like style. They investigate a haunted house, where Dr. Weird, a monster (whose costume Carl wore half naked on the Internet),  Handbanana, Dr. Wongburger, the Cybernetic Ghost of Christmas Past from the Future, Billy Witchdoctor.com, and Willie Nelson all make brief non-speaking cameo appearances. Carl makes a brief appearance as well. They all run into a [[villain of the week|monster]], soon after Frylock unmasks the monster, who turns out to be One Hundred in disguise. Shake tells him that he failed to put ''Aqua Teen Hunger Force'' in syndication, telling him that he even tried to double the episodes to make them fit in a half-hour time slot.

The [[closing credits]] feature Dana Snyder once again talking to a television executive, in an attempt to put ''Aqua Teen Hunger Force'' into syndication, but the television executive walks out on him, telling him that nothing he says is funny.

==Production==
[[File:Robert Smigel.jpg|thumb|right|180px|Robert Smigel provided the voice of One Hundred.]]
"One Hundred" was written and directed by series creators [[Dave Willis]] and [[Matt Maiellaro]] (credited as Comedian Matt "Crag Hartin Fan Club" Maiellaro) who have written and directed every episode of the series. It originally aired in the United States on [[Cartoon Network|Cartoon Network's]] late night programing block, [[Adult Swim]], on May 2, 2010. This episode features guest appearances from [[Robert Smigel]] who voiced One Hundred, Nick Weidenfeld who voiced himself, [[Tom Savini]] who voiced an unnamed cop, and [[Amber Nash]] who voiced Master Shake's girlfriend Amber Nash.<ref name=OneHundred>Credits found at the end of ''Aqua Teen Hunger Force'' episode "One Hundred".</ref>

"One Hundred" is the final episode of [[Aqua Teen Hunger Force (season 7)|season seven]] and is the [[100 episodes|100th episode]] of ''Aqua Teen Hunger Force'',<ref name="Press release"/> and marking the second time an Adult Swim original made it to 100 episodes. The first original Adult Swim series to reach 100 episodes was ''[[Space Ghost Coast to Coast]]'', which made a total of 110 episodes before officially ending in 2008.{{citation needed|date=December 2011}}

This is the final episode of the series to officially premiere under the ''Aqua Teen Hunger Force'' name. As the series started using [[Aqua Teen Hunger Force#Alternative titles|alternative titles]] as a [[running gag]], starting with the next season.<ref>{{cite web|last=Franich|first=Darren|title='Aqua Teen Hunger Force' changes title to 'Aqua Unit Patrol Squad 1'|url=http://popwatch.ew.com/2011/04/26/aqua-teen-hunger-force-new-title/?replytocom=1565577|publisher=''[[Entertainment Weekly]]''|accessdate=22 May 2011}}</ref> The following episode, "[[Allen (Aqua Unit Patrol Squad 1)|Allen Part One]]", was the first episode of the series to premiere under an alternative title.<ref>[http://video.adultswim.com/promos/aqua-unit-patrol-squad-1-season-premiere.html Adult Swim promo officially branding the following episode, "Allen Part One", an ''Aqua Unit Patrol Squad 1'' episode.]</ref>

==Cultural references==
"One Hundred" makes reference to the 2007 [[psychological thriller]] film, ''[[The Number 23]]'', when Frylock is obsessed with the number [[100 (number)|100]]. "One Hundred" strongly parodies the [[Hanna-Barbera]] cartoon, ''[[Scooby-Doo]]''. During the parody Meatwad becomes "Meaty Meaty Moo", a parody of the character [[Scooby-Doo (character)|Scooby-Doo]], and Tabitha, a female character resembling [[Daphne Blake|Daphne]]/[[Velma Dinkley|Velma]] from Scooby-Doo, then joins them.<ref name=IGN>{{cite web|last=Isler|first=Ramsey|title=Aqua Teen Hunger Force: "One Hundred" Review Adult Swim's longest running original series hits the big 100.|url=http://tv.ign.com/articles/108/1087353p1.html|publisher=[[IGN]]|date=3 May 2010|accessdate=13 September 2010}}</ref>

==Reception==
"One Hundred" was the highest rated episode of season seven. In its original American broadcast on May 2, 2010, "One Hundred" was watched by 989,000 viewers, making it the third most watched Adult Swim program of that night, behind the season premiere of ''[[The Boondocks (TV series)|The Boondocks]]'' and a repeat of ''[[Family Guy]]''.<ref>{{cite web|last=Seidman|first=Robert|title=Season Three Premiere of "The Boondocks" Sets Series Ratings Records|url=http://tvbythenumbers.zap2it.com/2010/05/04/season-three-premiere-of-the-boondocks-sets-series-ratings-records/50510/|publisher=TV by the Numbers|date=4 May 2010|accessdate=13 September 2010}}</ref>

The episode then jumps to a [[Scooby-Doo]] parody, which Ramsey Isler of [[IGN]] says is the "funniest stuff ATHF has done in a long time". IGN gave the episode a 7.8. out of 10, generally classifying it as "good", but comments on how oddly dark things went, when they had a character show up and state his desire to behead and rape a female character in that episode.  After some dialog, the character grabs the female and takes her off to the woods.  They then show someone talking to one of the network people from before, who criticized them for it.<ref name=IGN/>

==Home release==
{{See also|Aqua Teen Hunger Force#Home releases|l1=List of Aqua Teen Hunger Force DVDs}}

"One Hundred" was released on [[DVD]] in [[Region 1]] as part of the ''Aqua Unit Patrol Squad 1: Season 1'' DVD set on October 11, 2011, along with six other episodes from season seven and the entire eighth season. The set was released and distrusted by [[Adult Swim]] and [[Warner Home Video]], and features "Terror Phone 3" as a special feature, the set also features completely uncensored audio on every episode.<ref name=AUPS1Season1>{{cite web|title=Aqua Unit Patrol Squad 1 Volume 1 (Aqua Teen Season 8)|url=http://www.madman.com.au/catalogue/view/14662/aqua-unit-patrol-squad-1-volume-1-aqua-teen-season-8|publisher=[[Madman Entertainment]]|accessdate=20 June 2013}}</ref> The set was later released in [[Region 4]] by [[Madman Entertainment]] on November 30, 2011.<ref name="AUPS1Season1"/> This episode is also available in [[High-definition video|HD]] and [[Standard-definition television|SD]] on [[iTunes]], the [[Xbox Live Marketplace]], and [[Amazon Instant Video]].<ref>[https://itunes.apple.com/us/tv-season/aqua-unit-patrol-squad-1-season/id434683718 ''Aqua Unit Patrol Squad 1'', Season 1] at [[iTunes]].</ref><ref>[http://marketplace.xbox.com/en-US/TVShow/Aqua-Unit-Patrol-Squad-1/Season/1/eba8bd9a-792d-4c9f-9f18-c4098d9d4a77 ''Aqua Unit Patrol Squad 1'': Season 1] at the [[Xbox Live Marketplace]].</ref><ref>[http://www.amazon.com/gp/product/B009XPAZ76/ref=pd_sim_mov_aiv_1 ''Aqua Unit Patrol Squad 1'' Season 1] at [[Amazon Instant Video]].</ref>

In 2015 this episode, along with the rest of the seventh season, was made available for [[Video on demand|on-demand]] streaming on [[Hulu|Hulu Plus]],<ref>[http://www.hulu.com/grid/aqua-teen-hunger-force?video_type=episode AQUA TEEN HUNGER FORCE: EPISODES] at [[Hulu]].</ref> as part of a deal made with Hulu and Turner Broadcasting.<ref name=HuluTurner>{{cite web|last1=Spangler|first1=Todd|title=Hulu Pacts With Turner for Exclusive Rights to Cartoon, Adult Swim, TNT, TBS Shows|url=http://variety.com/2015/digital/news/hulu-turner-cartoon-adult-swim-tnt-tbs-1201478327/|publisher=[[Variety (magazine)|Variety]]|accessdate=11 May 2015}}</ref>

==References==
{{Reflist|2}}

==External links==
{{Wikiquote|Aqua Teen_Hunger Force (Season 7)#One Hundred|<br>"One Hundred"}}
* {{imdb episode|1676239|One Hundred}}
* {{tv.com episode|aqua-something-you-know-whatever/one-hundred-1341216|One Hundred}}

{{Aqua Teen Hunger Force}}

[[Category:Aqua Teen Hunger Force episodes]]
[[Category:2010 American television episodes]]
[[Category:Scooby-Doo parodies]]
[[Category:Rape in fiction]]
[[Category:Necrophilia in fiction]]