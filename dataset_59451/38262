{{good article}}
{{Infobox University Boat Race
| name=  88th Boat Race
| winner = Cambridge
| margin = 5 lengths
| winning_time= 21 minutes 6 seconds
| date= {{Start date|1936|4|4|df=y}}
| umpire = Francis Escombe<br>(Cambridge)
| prevseason= [[The Boat Race 1935|1935]]
| nextseason= [[The Boat Race 1937|1937]]
| overall =47&ndash;40
| women_winner = Oxford
}}
The '''88th Boat Race''' took place on 4 April 1936.  Held annually, the Boat Race is a [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]]. Umpired by the former Boat Race coach Francis Escombe, Cambridge won by five lengths in a time of 21 minutes 6 seconds.   The record thirteenth consecutive victory took the overall record in the event to 47&ndash;40 in Cambridge's favour.  The heaviest crew up to that year in Boat Race history, Cambridge were the first to weigh more than an average of 13&nbsp;[[Stone (unit)|st]] (82.4&nbsp;kg) per individual.

==Background==
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 20 August 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  The race was first held in 1829, and since 1845 has taken place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 20 June 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref><ref>{{Cite web | url = http://theboatraces.org/the-course | title = The Course| accessdate = 24 July 2014 | publisher = The Boat Race Company Limited}}</ref>  The rivalry is a major point of honour between the two universities; it is followed throughout the United Kingdom and, as of 2014, broadcast worldwide.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford&ndash;Cambridge Boat Race crew|date=6 April 2014|publisher=[[CBC News]] |url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=20 August 2014}}</ref>  Cambridge went into the race as reigning champions, having won the [[The Boat Race 1935|1935 race]] by four and a half lengths, and led overall with 46 victories to Oxford's 40 (excluding the [[The Boat Race 1877|"dead heat" of 1877]]).<ref name=results>{{Cite web | url = http://theboatraces.org/results| publisher = The Boat Race Company Limited | title = Boat Race – Results| accessdate = 25 August 2014}}</ref><ref>{{Cite web | url= http://theboatraces.org/classic-moments-the-1877-dead-heat | publisher = The Boat Race Company Limited | title = Classic moments – the 1877 dead heat | accessdate =20 August 2014}}</ref>

Oxford were coached by William Rathbone (who rowed for Oxford in the [[The Boat Race 1926|1926]] and [[The Boat Race 1927|1927 races]]), and former Cambridge rowers John Houghton Gibbon (who had rowed for the Light Blues in the [[The Boat Race 1899|1899]] and [[The Boat Race 1900|1900 races]]) and [[Kenneth Payne]] (who rowed for Cambridge in the [[The Boat Race 1932|1932]] and [[The Boat Race 1934|1934 races]]).  Cambridge's coaches were F. E. Hellyer (who had rowed for the Light Blues in the [[The Boat Race 1910|1910]] and [[The Boat Race 1911|1911 races]]), B. C. Johnstone, D. H. E. McCowen (who had rowed in the [[The Boat Race 1932|1932 race]]) and C. H. Rew (who had coached the Light Blues the previous year).<ref name=again/><ref>Burnell, pp. 110&ndash;111</ref>  The race was umpired by Francis Jerram Escombe who had coached Cambridge fifteen times between 1904 and 1934, and Oxford the previous year.<ref>Burnell, pp. 49, 110</ref> Both boats were made by Sims and both crews used [[Lola Aylings|Ayling's]] oars.<ref name=again/>

The rowing correspondent for ''[[The Times]]'' suggested that on arrival at [[Putney]], "Cambridge were almost certainly the fastest crew ever to come to the tideway.  Oxford were equally certain one of the worst".<ref name=again>{{Cite news | title = Boat Race Day &ndash; Cambridge again favourites | date = 4 April 1936 | work = [[The Times]] | issue = 47341 | page =15 }}</ref>  He went on to report that while Cambridge still looked "remarkably neat", they "have actually got slower";  at the same time Oxford "improved their pace, if not their appearance, in a measure that most critics would have thought quite impossible".<ref name=again/>

==Crews==
The Cambridge crew weighed an average of 13&nbsp;[[Stone (unit)|st]] 0.5&nbsp;[[Pound (mass)|lb]] (82.6&nbsp;kg), {{convert|3.75|lb|kg|1}} per rower more than their opponents, the heaviest crew ever at the time.<ref name=again/>  It was the first time in the race history that a crew weighed more than an average of 13&nbsp;[[Stone (unit)|st]] (82.4&nbsp;kg).<ref>Burnell, p. 44</ref>  Oxford saw two participants with Boat Race experience return to the crew in number five B. J. Sciortino and [[Stroke (rowing)|stroke]] D. M. de R. Winser.  Cambridge's crew contained five former [[Blue (university sport)|Blues]], four of whom were making their third appearance including [[Coxswain (rowing)|cox]] [[Noel Duckworth]],<ref name=burn75/> and [[Jack Wilson (rower)|Jack Wilson]] and [[Ran Laurie]], who would win the Coxless pairs Gold medal at the [[Rowing at the 1948 Summer Olympics|1948 Olympics]]. Three of the race participants were registered as non-British: Oxford's J. S. Lewes and S. R. C. Wood, along with Cambridge's [[Thomas Cree]] were all Australian.<ref>Burnell, p. 39</ref>

{| class=wikitable
|-
! rowspan="2"| Seat
! colspan="3"|  Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
! colspan="3"|  Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]] || [[Thomas Cree|T. S. Cree]] || [[Jesus College, Cambridge|Jesus]] || 12 st 2 lb || M. G. C. Ashby || [[New College, Oxford|New College]] || 12 st 0 lb
|-
| 2 || [[Hugh Mason (rower)|H. W. Mason]] || [[Trinity Hall, Cambridge|Trinity Hall]] || 12 st 8 lb || J. S. Lewes  || [[Christ Church, Oxford|Christ Church]] || 12 st 7.5 lb
|-
| 3 || G. M. Lewis || [[Pembroke College, Cambridge|Pembroke]] || 13 st 0 lb || K. V. Garside  || [[St John's College, Oxford|St John's]] || 12 st 12 lb
|-
| 4 || [[David Burnford|D. W. Burnford]] || [[Jesus College, Cambridge|Jesus]] || 13 st 4 lb || S. R. C. Wood  || [[Brasenose College, Oxford|Brasenose]]  || 12 st 7 lb
|-
| 5 || [[McAllister Lonnon|M. P. Lonnon]]  || [[Trinity College, Cambridge|3rd Trinity]]  || 13 st 6 lb || B. J. Sciortino (P) || [[University College, Oxford|University]] || 12 st 11 lb
|-
| 6 || [[Desmond Kingsford|D. G. Kingsford]] || [[Pembroke College, Cambridge|Pembroke]] || 13 st 7 lb ||  [[John Sturrock|J. D. Sturrock]]  || [[Magdalen College, Oxford|Magdalen]]  || 14 st 1.5 lb
|-
| 7 || [[Jack Wilson (rower)|J. H. T. Wilson]] (P)  || [[Pembroke College, Cambridge|Pembroke]] || 13 st 0 lb || [[John Cherry (rower)|J. C. Cherry]]  || [[Brasenose College, Oxford|Brasenose]] || 13 st 7 lb
|-
| [[Stroke (rowing)|Stroke]] ||  [[Ran Laurie|W. G. R. M. Laurie]]  || [[Selwyn College, Cambridge|Selwyn]]  || 13 st 6 lb|| D. M. de R. Winser  || [[Corpus Christi College, Oxford|Corpus Christi]] || 11 st 12 lb
|-
| [[Coxswain (rowing)|Cox]] || [[Noel Duckworth|J. N. Duckworth]]  || [[Jesus College, Cambridge|Jesus]] || 8 st 7 lb || M. A. Kirke ||  [[Keble College, Oxford|Keble]] || 8 st 7 lb
|-
!colspan="7"|Source:<ref name=burn75>Burnell, p. 75</ref><br>(P) &ndash; boat club president<ref>Burnell, pp. 50, 52</ref>
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]] along which the Boat Race is contested]]
Oxford won the [[coin flipping|toss]] and elected to start from the Surrey station, handing the Middlesex side of the river to Cambridge.<ref name=dodd329>Dodd, p. 329</ref>  Umpire Escombe started the race at 11:40&nbsp;a.m in relatively calm water.<ref name=burn75/>  Cambridge made the quicker start, [[stroke rate|out-rating]] the Dark Blues, but struggled in patches of rougher water such that Oxford held a slight lead by the end of the Fulham Wall.  With the bend in the river against them, the Dark Blues held a half-length lead as the crews passed the Mile Post and, according to former Oxford rower E. P. Evans, writing in ''[[The Manchester Guardian]]'', "were going well ... and seemed to be forging ahead at every stroke".<ref name=Evans>{{Cite news | title = Another Cambridge victory | first = E. P. | last = Evans | date = 6 April 1936 | page = 4 | work = [[The Manchester Guardian]]}}</ref>  At [[Harrods Furniture Depository]], Cambridge stroke [[Ran Laurie]] pushed on to reduce Oxford's advantage as the long bend in the river favoured them: the crews passed under [[Hammersmith Bridge]] with Cambridge trailing by half a length.<ref name=Evans/>

A series of spurts from Cambridge combined with intelligent steering from their cox, pushing the Dark Blue boat out of the tide, resulted in a reversal of fortune with Cambridge leading, and by the end of [[Chiswick Eyot]], they were nearly clear of Oxford.  Passing Chiswick Steps four seconds ahead, the Light Blues continued to increase their advantage, passing below [[Barnes Railway Bridge|Barnes Bridge]] four lengths ahead.<ref name=again/>  They passed the finishing post five lengths ahead in a time of 21 minutes 6 seconds.<ref name=dodd329/>  It was the largest winning margin since the [[The Boat Race 1929|1929 race]] and the slowest winning time since the [[The Boat Race 1925|1925 race]] when Oxford sank in adverse conditions.<ref name=results/><ref name=beach>{{Cite news | title = Light Blues win by five lengths| work = [[The Observer]] | date = 5 April 1936 | page = 19 | first = William Beach|last = Thomas| authorlink = William Beach Thomas}}</ref>  It was Cambridge's thirteenth consecutive victory, a record streak in the history of the Boat Race, and took the overall record in the event to 47&ndash;40 in their favour.<ref name=results/>  E. P. Evans wrote that "no praise is too much for the gallant Oxford crew" while of Cambridge he noted "though they may not have produced their best form in the early part of the race they made up for it later".<ref name=again/>  The rowing correspondent for ''[[The Times]]'' stated that "it was an exciting race over the first half of the course" and praised the Light Blues saying "all credit is due to the Cambridge crew for the manner in which they recovered themselves after being led through such bad water".<ref>{{Cite news | title = The Boat Race &ndash; Cambridge win again| date = 6 April 1936 | page = 6 | issue = 47342 | work = [[The Times]]}}</ref>  [[William Beach Thomas]], writing in ''[[The Observer]]'', claimed "Cambridge were described as the best crew in the chronicles of rowing kings, and Oxford the very worst".<ref name=beach/>

==References==
'''Notes'''
{{reflist|30em}}

'''Bibliography'''
*{{Cite book | title = One Hundred and Fifty Years of the Oxford and Cambridge Boat Race | first = Richard | last = Burnell | authorlink = Dickie Burnell | year=1979| isbn= 0950063878 | publisher = Precision Press}}
*{{Cite book | title = The Oxford & Cambridge Boat Race | first = Christopher| last = Dodd | isbn= 0091513405 | publisher =Stanley Paul |year= 1983}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1936}}
[[Category:1936 in English sport]]
[[Category:The Boat Race]]
[[Category:April 1936 sports events]]
[[Category:1936 in rowing]]