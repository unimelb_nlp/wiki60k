{{Infobox organization
|name         = Bioscientifica
|image        = Bio Landscape CMYK.png
|image_border = 
|size         = 250px
|motto        = Profit for good
|formation    = 1996
|status       = A company limited by guarantee and incorporated in England and Wales
|business= Biomedical publishing, event management, and association management
|headquarters =  Euro House, 22 Apex Court, Woodlands, Bradley Stoke, Bristol BS32 4JT, United Kingdom 
|leader_title = Managing Director 
|leader_name  = Ian Russell
|parent_organization = [[Society for Endocrinology]]
|website      = http://www.bioscientifica.com/
}}

Established in 1996, '''Bioscientifica Ltd''' is the commercial subsidiary of the [[Society for Endocrinology]], and provides [[publishing]], events, and [[association management]] services to biomedical societies, and to the pharmaceutical industry.<ref>{{Cite web |url=https://www.opencompany.co.uk/company/03190519/bioscientifica-limited |title= Bioscientifica Listing on Open Company |accessdate=2014-08-28}}</ref><ref>{{Cite web |url=http://www.bioscientifica.com/about/default.aspx|title= Bioscientifica Website |accessdate=2014-08-28}}</ref> Although the company generates profits, these are redistributed to Bioscientifica's partner societies to fund biomedical research and practice.<ref>{{Cite web |url=http://www.endocrinology.org/media/1770/aug-dec2014sfefinancialstatements.pdf |title= Bioscientifica Profit Distribution |accessdate=2015-03-02}}</ref>

== Publishing ==
Bioscientifica publishes [[academic journals]] and [[case reports]] focused on [[endocrinology]], and its intersecting disciplines. The company also publishes free-to-read conference abstracts via its BiosciAbstracts platform.<ref>{{Cite web |url=http://www.biosciabstracts.com/|title= BiosciAbstracts |accessdate=2016-02-21}}</ref> According to the 2015 ''[[Journal Citation Reports]]'', five of Bioscientifica’s publications have an [[impact factor]]: ''[[Endocrine-Related Cancer]]'', ''[[Journal of Endocrinology]]'', ''[[Journal of Molecular Endocrinology]]'', ''[[European Journal of Endocrinology]]'', and ''[[Reproduction (journal)]]''.<ref name=WoS>{{cite book |year=2015 |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]] |postscript=.}}</ref> In June 2016, one of Bioscientifica's open access journals, ''[[Endocrine Connections]]'', became indexed in Thomson Reuters’ Science Citation Index Expanded. This journal will receive its first impact factor in 2017.<ref>{{Cite web |url=http://www.bioscientifica.com/news/news/endocrine-connections-is-now-indexed-in-science-citation-index-expanded/|title= Endocrine Connections - SCIE indexing |accessdate=2016-08-03}}</ref>

Bioscientifica was an early proponent of open access. In February 2012, the company was among the first 24 publishers to sign the STM Association’s Sustainable [[Open Access]] Statement.<ref>{{Cite web |url=http://www.stmassoc.org/2012_02_02_Publishers_Support_Sustainable_Open_Acces |title= STM Statement on Sustainable Open Access |accessdate=2014-08-28}}</ref><ref>{{Cite web |url= http://www.stm-assoc.org/publishers-support-sustainable-open-access/ |title= STM Association |accessdate=2014-08-28}}</ref> Bioscientifica currently publishes three open access journals, supports [[gold open access]] publication in its subscription journals, and makes all journal content free to read online 12 months after publication.

As part of its charitable remit, Bioscientifica participates in the [[HINARI]] programme, through which the publisher provides free access to its journals in developing countries.<ref>{{Cite web |url=http://extranet.who.int/hinari/en/partners.php?category=publisher |title= HINARI Partner List |accessdate=2014-08-28}}</ref>

== Association Management ==
Bioscientifica currently provides Association Management and [[Secretariat (administrative office)|secretariat]] services to the following societies:
* British Society for Paediatric Endocrinology and Diabetes  
* [[European Society of Endocrinology]]
* [[European Society for Paediatric Endocrinology]]
* UK and Ireland Neuroendocrine Tumour Society<ref>{{Cite web |url=http://www.bioscientifica.com/societies/|title= Bioscientifica Client List |accessdate=2016-02-21}}</ref>

== Event Management ==
Bioscientifica manages academic conferences for biomedical societies. Their event services include:

* Scientific and social programme management
* Accommodation management
* Abstract management and publication
* Event marketing and press
* Conference websites and mobile apps
* Coordination of grants and awards
* Liaison with CPD accreditors
* Sponsorship and exhibition sales
* Budget management and bookkeeping<ref>{{Cite web |url=http://www.bioscientifica.com/event-management/|title= Bioscientifica event services|accessdate=2016-02-21}}</ref>

Several of the events managed by Bioscientifica have won industry awards. Recent awards include:

* 2015: 'Best Association Conference Outside London' at the Association Excellence Awards.<ref>{{Cite web |url=http://www.associationexcellenceawards.co.uk/winners.html |title= Association Excellence Awards - 2015 Winners |accessdate=2015-03-02}}</ref>
* 2013: 'Best Association Conference' prize at the fourth Annual Conference Awards<ref>{{Cite web |url=http://www.conferenceawards.co.uk/shortlist_summaries/ss%20assoc.html |title= Conference Awards - 2013 Winners |accessdate=2014-08-28}}</ref>

== Professional Memberships ==
Bioscientifica is a member of the following professional bodies:
* Association of British Professional Conference Organisers<ref>{{Cite web |url=http://www.abpco.org/abpco-members/full-members/|title=ABPCOMember List |accessdate=2016-02-21}}</ref> 
* [[Association of Learned and Professional Society Publishers]]<ref>{{Cite web |url=http://alpsp.org/Ebusiness/AboutAlpsp/ListofMembers.aspx |title= ALPSP Member List |accessdate=2014-08-28}}</ref> 
* [[Committee on Publication Ethics]]<ref>{{Cite web |url= http://publicationethics.org/taxonomy/term/661|title= COPE |accessdate=2015-05-21}}</ref>
* Independent Scholarly Publishers Group<ref>{{Cite web |url= http://www.accucoms.com/publishers/independent-scholarly-publishers-group-ispg/|title= ISPG |accessdate=2015-03-03}}</ref>
* [[International Association of Scientific, Technical, and Medical Publishers]]<ref>{{Cite web |url= http://www.stm-assoc.org/membership/our-members/|title= STM Association Member List |accessdate=2015-03-03}}</ref> 
* [[International Congress and Convention Association]]<ref>{{Cite web |url=http://members.iccaworld.com/|title= ICCA Membership Directory |accessdate=2016-02-21}}</ref> 

==Governance==
Bioscientifica is governed by its Board of Directors, which is composed of [[Society for Endocrinology]] Officers, industry professionals, and members of Bioscientifica's Executive team. The Board is currently made up of the following members.
*Chair: Adrian Clark 
*Karen Chapman
*Barbara McGowan
*Ian Russell
*Pat Barter

==References==
{{reflist|30em}}

== External links ==
* {{Official website|http://www.bioscientifica.com}}

[[Category:Academic publishing companies]]
[[Category:Publishing companies established in 1996]]
[[Category:Companies based in Bristol]]
[[Category:Publishing companies of the United Kingdom]]