{{Infobox military person
| name          = Harold Douglas Briggs
| image         = 
| caption       = 
| nickname      = 
| birth_date    = {{birth date|1877|09|29|df=yes}}
| birth_place   = Hylton, [[Sunderland, Tyne and Wear|Sunderland]], England
| death_date    = {{death date and age|1944|09|13|1877|09|29|df=yes}} 
| death_place   = [[London]], England
| placeofburial = 
| allegiance    = United Kingdom
| branch        = [[Royal Navy]]<br/>[[Royal Air Force]]
| serviceyears  = 
| rank          = [[Brigadier General]]
| servicenumber = 
| unit          = 
| commands      = {{HMS|Resolution|09|6}} (1920–21)<br/>{{HMS|Courageous|50|6}} (1919–20)<br/>{{HMS|St. Vincent|1908|6}} (1919)<br/>[[No. 9 Group RAF|No. 9 (Operations) Group]] (1918–19)<br/>[[No. 12 Group RAF|No. 12 Group]] (1918)<br/>[[RAF Cranwell]] (1918)<br/>[[RNAS Vendome]] (1916–17)
| battles       = [[First World War]]
| awards        = [[Companion of the Order of St Michael and St George]]<br/>[[Officer of the Legion of Honour]] (France)<br/>[[Order of the Rising Sun|Order of the Rising Sun, 3rd Class]] (Japan)
| relations     =
| laterwork     =
}}
[[Brigadier General]] '''Harold Douglas Briggs''', {{postnominals|country=GBR|size=100%|CMG}} (29 September 1877 – 13 September 1944) was a senior [[Royal Navy]] and [[Royal Air Force]] officer who played a leading role in British naval aviation during the [[First World War]].

==Background==
Briggs was born 29 September 1877.  After joining the Royal Navy as a young man, Briggs showed steady career progression and by 1908 was on the staff of the Admiral Commanding Coast Guards and Reserves. Towards the end of 1911 he was promoted to [[Commander (Royal Navy)|commander]] and at the end of 1912 he became the executive officer on {{HMS|Bellerophon|1907|6}}.

In 1915 Briggs was appointed Officer-in-Command of Air Stations under the Admiral Commanding the East Coast of England. Later that year he joined the staff of the Inspecting Captain of Air Training. In November Briggs was promoted to [[Captain (Royal Navy)|acting wing captain]] and the following January he became the Inspecting Captain of Air Training himself. After only three months in post Briggs was reassigned again, this time as the Officer Commanding [[RNAS Vendome]], a [[Royal Naval Air Service]] flight training school in France. In September 1917 he was recalled to his former post and again served as Inspecting Captain of Air Training.

==Royal Air Force==
By the start of 1918 the preparations to create the Royal Air Force were well underway and Briggs was given the RAF rank of temporary brigadier-general in mid-February when he joined the [[Air Ministry]].  On 1 April the RAF came into being and Briggs was appointed the [[General Officer Commanding]] both [[RAF Cranwell]] and [[No. 12 Group RAF|No. 12 Group]].<ref>http://www.rafweb.org/Biographies/Briggs.htm</ref>

In 1919 Briggs returned to the Navy and was given command of the dreadnought {{HMS|St. Vincent|1908|6}}.<ref>The Navy List (August, 1919). p. 901</ref> Briggs retired from the Navy in 1922,<ref>{{London Gazette |issue=32729 |date=14 July 1922 |startpage=5278 |accessdate=14 December 2012 }}</ref> and the RAF granted him the honorary rank of brigadier general.<ref>{{London Gazette |issue=32791 |date=30 January 1923 |startpage=712 |accessdate=14 December 2012 }}</ref>

Briggs died at a nursing home in London on 13 September 1944.

==Notes==
{{reflist|30em}}

==External links==
*[http://www.rafweb.org/Biographies/Briggs.htm Air of Authority – A History of RAF Organisation – Brigadier General H D Briggs]
*[http://www.dreadnoughtproject.org/tfs/index.php/Harold_Douglas_Briggs The Dreadnought Project – Harold Douglas Briggs]

{{s-start}}
{{s-mil}}
{{s-bef|before=[[John Luce (Royal Navy officer)|John Luce]]<br/><small>As Commodore of the Central Depot and Training Establishment</small>}} 
{{s-ttl|title=[[General Officer Commanding]] [[No. 12 Group RAF|No. 12 Group]]|years=1918–1919}}
{{s-aft|after=[[Francis Rowland Scarlett|Francis Scarlett]]}}
{{s-end}}

{{DEFAULTSORT:Briggs, Harold Douglas}}
[[Category:1877 births]]
[[Category:1944 deaths]]
[[Category:Royal Navy officers]]
[[Category:Royal Air Force generals of World War I]]
[[Category:Companions of the Order of St Michael and St George]]
[[Category:People from Sunderland (district)]]