<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name=XP-34
  |image= File:Wedell-Williams XP-34 artists concept.jpg
  |caption= Artist's impression of the XP-34
}}{{Infobox Aircraft Type
  |type= [[Fighter aircraft]]
  |national origin= United States
  |manufacturer= [[Jimmy Wedell|Wedell-Williams Air Service Corporation]]
  |designer=
  |first flight= 
  |introduced= 
  |retired= 
  |status=
  |primary user= 
  |more users = 
  |produced=
  |number built= 
  |unit cost= 
  |developed from= [[Wedell-Williams Model 45]]
  |variants with their own articles= 
  |developed into= 
}}
|}

The '''Wedell-Williams XP-34''' was a [[fighter aircraft]] design submitted to the [[United States Army Air Corps]] (USAAC) before [[World War II]] by [[Marguerite Clark]] Williams, widow of millionaire [[Harry Palmerston Williams|Harry P. Williams]], former owner and co-founder of the [[Jimmy Wedell|Wedell-Williams Air Service Corporation]].

==Design and development==
Derived from an original proposal made in 1932, the XP-34 was based on a design by [[air racing|air racer]] [[Jimmy Wedell]], who was considered, "one of the most noted race plane designers of its day".<ref name= "Jones p. 80">Jones 1975, p. 80.</ref> The aircraft was a direct result of the development of Wedell's most successful designs, the [[Wedell-Williams Model 44|Model 44]] and [[Wedell-Williams Model 45|Model 45]].<ref name="Dorr and Donald 1990, p. 61"/> The forward fuselage was intended to be metal, the after part and control surfaces covered in [[aircraft fabric|fabric]].<ref name= "Jones p. 80"/>

The interest expressed from the USAAC was based on the success of the private racing aircraft in the 1930s that were reaching 300&nbsp;mph speeds in competition, a performance level not achieved by standard aircraft types in service in the U.S. military.<ref name="Dorr and Donald 1990, p. 61">Dorr and Donald 1990, p. 61.</ref>

On 1 October 1935, the USAAC ordered a full set of drawings and issued the XP-34 designation. It soon became apparent, however, with its original 700&nbsp;hp (522&nbsp;kW) [[Pratt & Whitney]] [[Pratt & Whitney R-1535|R1535]] Twin Wasp engine, the anticipated performance of the XP-34 would be insufficient compared to designs already in production.

Wedell-Williams suggested substituting the 900&nbsp;hp (671&nbsp;kW) [[Pratt & Whitney R-1830|XR-1830]] instead. Although the promise of high speed was still there, other considerations such as the complete redesign of the airframe to accommodate a heavier and more powerful engine were considered impractical with the new design subsequently rejected by the Air Corps before any aircraft were built.<ref name="Dorr and Donald 1990, p. 61"/>

==Specifications (proposed)==
{{Aerospecs
|ref= ''U.S. Fighters''
|met or eng?= eng
|crew=1 pilot
|capacity=
|length m=7.2 
|length ft=23
|length in=6 
|span m=8.45
|span ft=27
|span in=8½
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->

As proposed, the XP-34 would have had a [[wingspan|span]] of 27' 8½" (8.45 m),  height 10' 9" (3.28 m), a gross weight of 4,250 lb (1927 kg), and a projected speed of 286 mph at 10,000 ft (460 km/h at 3050 m).<ref name= "Jones p. 80"/>
|height m=3.28
|height ft=10
|height in=9
|wing area sqm=
|wing area sqft=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|empty weight kg= 
|empty weight lb=
|gross weight kg=1,928
|gross weight lb=4,250
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|eng1 number=1
|eng1 type=[[Pratt & Whitney]] [[Pratt & Whitney R-1830 Twin Wasp|XR1830-C]] air-cooled [[radial engine|radial]]
|eng1 kw=671
|eng1 hp=900 
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|max speed kmh=496
|max speed mph=308
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}

==See also==
{{Aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
*[[Wedell-Williams Model 44]]
*[[Wedell-Williams Model 45]]
|similar aircraft=<!-- similar or comparable aircraft -->
|sequence=<!-- designation sequence, if appropriate -->
|lists=<!-- related lists -->
*[[List of military aircraft of the United States]]
}}

==References==
{{commons category|Wedell-Williams XP-34}}
;Notes
{{reflist}}
;Bibliography
{{refbegin}}
* Dorr, Robert F. and Donald, David. ''Fighters of the United States Air Force.'' London: Temple, 1990. ISBN 0-600-55094-X.
* Jones, Lloyd S. ''U.S. Fighters, Army-Air Force: 1925 to 1980s''. Los Angeles: Aero Publishers Incorporated, 1975. ISBN 0-8168-9200-8.
{{refend}}

{{USAF fighters}}

[[Category:United States fighter aircraft 1930–1939|Wedell-Williams P-34]]
[[Category:Single-engine aircraft]]
[[Category:Propeller aircraft]]
[[Category:United States experimental aircraft 1930–1939|Wedell-Williams P-34]]