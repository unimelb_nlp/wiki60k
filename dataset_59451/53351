[[image:Zscore.JPG|thumb|Example of an [[Microsoft Excel|Excel]] spreadsheet that uses Altman Z-score to predict the [[probability]] that a firm will go into [[bankruptcy]] within two years ]]
The '''Z-score formula for predicting bankruptcy''' was published in 1968 by [[Edward I. Altman]], who was, at the time, an Assistant Professor of Finance at [[New York University]]. The formula may be used to predict the probability that a firm will go into [[bankruptcy]] within two years.  Z-scores are used to predict corporate defaults and an easy-to-calculate control measure for the [[financial distress]] status of companies in academic studies.  The Z-score uses multiple corporate income and balance sheet values to measure the financial health of a company.

==Estimation of the formula==
The Z-score is a linear combination of four or five common business ratios, weighted by coefficients.  The coefficients were estimated by identifying a set of firms which had declared bankruptcy and then collecting a [[pair-matched sample|matched sample]] of firms which had survived, with matching by industry and approximate size (assets).

Altman applied the statistical method of [[discriminant analysis]] to a dataset of publicly held manufacturers. The estimation was originally based on data from publicly held manufacturers, but has since been re-estimated based on other datasets for private manufacturing, non-manufacturing and service companies.

The original data sample consisted of 66 firms, half of which had filed for bankruptcy under Chapter 7.  All businesses in the database were manufacturers, and small firms with assets of <&nbsp;$1 million were eliminated.

The original Z-score formula was as follows:<ref>realequityresearch.dk/Documents/Z-Score_Altman_1968.pdf</ref>

: Z = 1.2X<sub>1</sub> + 1.4X<sub>2</sub> + 3.3X<sub>3</sub> + 0.6X<sub>4</sub> + 1.0X<sub>5</sub>.

: X<sub>1</sub> = Working Capital / Total Assets.  Measures liquid assets in relation to the size of the company.

: X<sub>2</sub> = Retained Earnings / Total Assets.  Measures profitability that reflects the company's age and earning power.

: X<sub>3</sub> = Earnings Before Interest and Taxes  / Total Assets. Measures operating efficiency apart from tax and leveraging factors.  It recognizes operating earnings as being important to long-term viability.

: X<sub>4</sub> = Market Value of Equity / Book Value of Total Liabilities. Adds market dimension that can show up security price fluctuation as a possible red flag.

: X<sub>5</sub> = Sales / Total Assets.  Standard measure for total asset turnover (varies greatly from industry to industry).

Altman found that the ratio profile for the bankrupt group fell at -0.25 avg, and for the non-bankrupt group at +4.48 avg.

==Precedents==
Altman's work built upon research by accounting researcher [[William Beaver]] and others.  In the 1930s and on, Mervyn and others had collected matched samples and assessed that various accounting ratios appeared to be valuable in predicting bankruptcy. Altman's Z-score is a customized version of the discriminant analysis technique of [[R. A. Fisher]] (1936).

William Beaver's work, published in 1966 and 1968, was the first to apply a statistical method, [[t-tests]] to predict bankruptcy for a pair-matched sample of firms.  Beaver applied this method to evaluate the importance of each of several accounting ratios based on univariate analysis, using each accounting ratio one at a time.  Altman's primary improvement was to apply a statistical method, discriminant analysis, which could take into account multiple variables simultaneously.

==Accuracy and effectiveness==
In its initial test, the Altman Z-Score was found to be 72% accurate in predicting bankruptcy two years before the event, with a Type II error (false negatives) of 6% (Altman, 1968). In a series of subsequent tests covering three periods over the next 31 years (up until 1999), the model was found to be approximately 80%–90% accurate in predicting bankruptcy one year before the event, with a Type II error (classifying the firm as bankrupt when it does not go bankrupt) of approximately 15%–20% (Altman, 2000).<ref>[http://pages.stern.nyu.edu/~ealtman/Zscores.pdf Predicting Financial Distress of Companies: Revisiting the Z-SCORE and ZETA Models]</ref>

From about 1985 onwards, the Z-scores gained wide acceptance by auditors, management accountants, courts, and database systems used for loan evaluation (Eidleman). The formula's approach has been used in a variety of contexts and countries, although it was designed originally for publicly held manufacturing companies with assets of more than $1 million. Later variations by Altman were designed to be applicable to privately held companies (the Altman Z'-Score) and non-manufacturing companies (the Altman Z"-Score).

Neither the Altman models nor other balance sheet-based models are recommended for use with financial companies. This is because of the opacity of financial companies' balance sheets and their frequent use of off-balance sheet items. There are market-based formulas used to predict the default of financial firms (such as the [[Merton Model]]), but these have limited predictive value because they rely on market data (fluctuations of share and options prices to imply fluctuations in asset values) to predict a market event (default, i.e., the decline in asset values below the value of a firm's liabilities).<ref>[http://pages.stern.nyu.edu/~ealtman/Zscores.pdf Predicting Financial Distress of Companies:Revisiting the Z-SCORE and ZETA Models]</ref>

==Original z-score component definitions variable definition ==

'''NOTE:''' The use of " / " is a stand-in for division (÷)

: X<sub>1</sub> = Working Capital / Total Assets

: X<sub>2</sub> = Retained Earnings / Total Assets

: X<sub>3</sub> = Earnings Before Interest and Taxes  / Total Assets

: X<sub>4</sub> = Market Value of Equity / Total Liabilities

: X<sub>5</sub> = Sales / Total Assets

'''Z score bankruptcy model:'''

: Z = 1.2X<sub>1</sub> + 1.4X<sub>2</sub> + 3.3X<sub>3</sub> + 0.6X<sub>4</sub> + .999X<sub>5</sub>

'''Zones of Discrimination:'''

: Z > 2.99 -“Safe” Zone
 
: 1.81 < Z < 2.99 -“Gray” Zone

: Z < 1.81 -“Distress” Zone

==Z-score estimated for private firms==

'''NOTE:''' The use of " / " is a stand-in for division (÷)

X<sub>1</sub> = (Current Assets &minus; Current Liabilities) / Total Assets

X<sub>2</sub> = Retained Earnings / Total Assets

X<sub>3</sub> = Earnings Before Interest and Taxes  / Total Assets

X<sub>4</sub> = Book Value of Equity / Total Liabilities

X<sub>5</sub> = Sales / Total Assets

'''Z' Score bankruptcy Model:'''

: Z'  = 0.717X<sub>1</sub> + 0.847X<sub>2</sub> + 3.107X<sub>3</sub> + 0.420X<sub>4</sub> + 0.998X<sub>5</sub>

'''Zones of Discrimination:'''

Z' > 2.9 -“Safe” Zone

1.23 < Z' < 2.9 -“Grey” Zone

Z' < 1.23 -“Distress” Zone

==Z-score estimated for non-manufacturers & emerging markets==

'''NOTE:''' The use of " / " is a stand-in for division (÷)

: X<sub>1</sub> = (Current Assets &minus; Current Liabilities) / Total Assets
 
: X<sub>2</sub> = Retained Earnings / Total Assets 
: X<sub>3</sub> = Earnings Before Interest and Taxes / Total Assets
: X<sub>4</sub> = Book Value of Equity / Total Liabilities

'''Z-Score bankruptcy model:'''
Z = 6.56X<sub>1</sub> + 3.26X<sub>2</sub> + 6.72X<sub>3</sub> + 1.05X<sub>4</sub><ref>http://people.stern.nyu.edu/ealtman/IRMC2014ZMODELpaper1.pdf</ref>

'''Z-Score bankruptcy model (Emerging Markets):'''
Z = 3.25 + 6.56X<sub>1</sub> + 3.26X<sub>2</sub> + 6.72X<sub>3</sub> + 1.05X<sub>4</sub>

'''Zones of discriminations:'''
: Z > 2.6 -“Safe” Zone
: 1.1 < Z < 2.6 -“Grey” Zone
: Z < 1.1 -“Distress” Zone

==See also==
*[[Standard score]]
*[[Z-test]]
*[[Z-factor]]
*[[Ohlson o-score]]

==References==
{{cite journal  | last = Altman  | first = Edward I.  | authorlink = Edward I. Altman  | title = Predicting Financial Distress of Companies  | journal = Retrieved on September 4th, 2009 from http://pages.stern.nyu.edu/~ealtman/Zscores.pdf  | pages = 15–22  | date = July 2000}}

{{cite journal  | last = Altman  | first = Edward I.  | authorlink = Edward I. Altman  | title = Financial Ratios, Discriminant Analysis and the Prediction of Corporate Bankruptcy  | journal = Journal of Finance  | pages = 189–209  | date = September 1968 | doi = 10.1111/j.1540-6261.1968.tb00843.x }}

{{cite journal  | last = Altman  | first = Edward I.  | authorlink = Edward I. Altman  | title = Revisiting Credit Scoring Models in a Basel II Environment  | journal = Prepared for "Credit Rating: Methodologies, Rationale, and Default Risk", London Risk Books 2002  | date = May 2002  | url = http://www.stern.nyu.edu/fin/workpapers/papers2002/pdf/wpa02041.pdf}}

{{cite journal  | last = Eidleman  | first = Gregory J.  | title = Z-Scores – A Guide to Failure Prediction  | journal = The CPA Journal Online  | date = 1995-02-01  | url = http://www.nysscpa.org/cpajournal/old/16641866.htm }}

{{cite journal  | last = Fisher  | first = Ronald Aylmer  | title = The Use of Multiple Measurements in Taxonomic Problems  | journal = Annals of Eugenics  | volume = 7  | page = 179  | year = 1936  | url = http://www.nysscpa.org/cpajournal/old/16641866.htm | doi=10.1111/j.1469-1809.1936.tb02137.x}}

[http://pages.stern.nyu.edu/~ealtman/3-%20CopCrScoringModels.pdf The Use of Credit Scoring Modules and the Importance of a Credit Culture] by Dr. Edward I Altman, [[New York University Stern School of Business|Stern School of Business]], New York University.
{{Reflist}}

==Further reading==
* Caouette, John B; Edward I Altman, Paul Narayanan (1998). ''Managing Credit Risk - the Next Great Financial Challenge'', [[John Wiley & Sons]]: New York. ISBN 978-0-471-11189-4
* {{cite journal |last= Mare |first= Davide |last2= Moreira |first2= Fernando |last3= Rossi |first3= Roberto |date= 2016 |title= Nonstationary Z-score measures |ssrn= 2688367| journal= European Journal of Operational Research | volume = forthcoming |doi= 10.1016/j.ejor.2016.12.001 |access-date= }}

==External links==
* [http://www.investingcalculator.org/investment/altman-z-score-calculator/ Altman Z-Score Calculator]

[[Category:Financial ratios]]
[[Category:Bankruptcy]]