{{Infobox book | 
| name          = The Pursuit of the House-Boat
| title_orig    = 
| translator    = 
| image         = Pursuit of the house boat.jpg
| caption       = Front cover of the first edition
| author        = [[John Kendrick Bangs]]
| illustrator   = [[Peter Newell]]<ref name=HDL/>
| cover_artist  = 
| country       = United States

| series        = [[Associated Shades]]
| genre         = [[Fantasy novel]]
| publisher     = [[Harper & Brothers]]
| pub_date      = 1897

| media_type    = Print ([[hardcover]])
| pages         = 204 pp
| oclc          = 225196
| congress      = PZ3.B224 Pu PS1064.B3<ref name=LCC>[https://lccn.loc.gov/06006121 "The pursuit of the house-boat"]. LC Online Catalog. Library of Congress. Retrieved 2016-08-04. <br>&nbsp; Links include electronic copy at HDL.</ref>
| preceded_by   = [[A House-Boat on the Styx]]
| followed_by   = [[The Enchanted Type-Writer]]
}}

'''''The Pursuit of the House-Boat''''' is an 1897 novel by [[John Kendrick Bangs]], and the second one to feature his [[Associated Shades]] take on the afterlife.

The original full title was ''The Pursuit of the House-Boat: Being Some Further Account of the Divers Doings of the Associated Shades, Under the Leadership of Sherlock Holmes, Esq.''<ref name=HDL>[https://catalog.hathitrust.org/Record/000427297 Catalog record (New York: Harper, 1897)]. HathiTrust Digital Library (hathitrust.org). Retrieved 2016-08-04.</ref> and it has also been titled ''In Pursuit of the House-Boat'' and ''Pursuit of the House-Boat''.

There are 12 chapters in the book. They were first published as a serial, under the full-title and including the Newell illustrations, in ''[[Harper's Weekly]]'' from February 6 to April 24, 1897.<ref>"The Pursuit of the House-Boat". ''Harper's Weekly'', vol. 41 part 1 (Jan-Jun 1897), pp. 136–37 and passim. <br>&nbsp; [http://hdl.handle.net/2027/pst.000020241117?urlappend=%3Bseq=142 Page 136 (February 6, 1897)], from original at Pennsylvania State University, at [[HathiTrust Digital Library]] (HDL.handle.net). Retrieved 2016-09-02.</ref>

==Plot summary==

After the House-Boat was [[wiktionary:Hijacked|hijacked]] by [[Captain Kidd]] at the end of ''[[A House-Boat on the Styx]]'', the various members of its club decided that in order to track it down, a detective would have to be called in. So they hired [[Sherlock Holmes]], who, at the time of the book's publication, had indeed been declared dead by his creator.

{{clear}}

==References==
{{reflist|25em}}

*{{cite book | last=Bleiler | first=Everett | authorlink=Everett F. Bleiler | title=The Checklist of Fantastic Literature | location=Chicago | publisher=Shasta Publishers | year=1948 | page=40}}

{{clear}}

==External links==
{{Portal |Speculative fiction}}
{{Gutenberg|no=3169|name=The Pursuit of the House-Boat}}
*Read the text of the book at http://www.classicreader.com/booktoc.php/sid.1/bookid.1226/

{{Associated_Shades}}
{{Sherlock Holmes by others}}

{{DEFAULTSORT:Pursuit Of The House-Boat}}
[[Category:1897 novels]]
[[Category:Bangsian fantasy]]
[[Category:Fantasy novels]]
[[Category:1890s fantasy novels]]
[[Category:Harper & Brothers books]]


{{1890s-fantasy-novel-stub}}