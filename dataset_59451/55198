{{Infobox journal
| title = ACM Transactions on Algorithms
| cover = 
| editor = [[Aravind Srinivasan]]
| discipline = [[Algorithms]]
| abbreviation = ACM Trans. Algor.
| publisher = [[Association for Computing Machinery]]
| frequency = Quarterly
| history = 2005–present
| impact = 0.400
| impact-year = 2013
| website = http://talg.acm.org/
| link1 = http://portal.acm.org/talg
| link1-name = Online access
| link2 = http://portal.acm.org/talg/archive
| link2-name = Online archive
| ISSN = 1549-6325
| eISSN = 1549-6333
| OCLC = 723357572
| LCCN = 2004212232
}}
'''''ACM Transactions on Algorithms''''' ('''''TALG''''') is a quarterly [[peer-reviewed]] [[scientific journal]] covering the field of [[algorithms]]. It was established in 2005 and is published by the [[Association for Computing Machinery]]. The [[editor-in-chief]] is Aravind Srinivasan  ([[University of Maryland]]). The journal was created when the [[editorial board]] of the [[Elsevier#Resignation of editorial boards|''Journal of Algorithms'']] resigned out of protest to the pricing policies of the publisher, [[Elsevier]].<ref>{{Cite web |title=Journal of Algorithms Resignation |url=http://www.cs.colorado.edu/~hal/jalg.html |last1=Gabow |first1=Hal |publisher=Department of Computer Science, [[University of Colorado Boulder]] |accessdate=2015-05-29}}</ref> Apart from regular submissions, the journal also invites selected papers from the [[Symposium on Discrete Algorithms | ''ACM-SIAM Symposium on Discrete Algorithms (SODA)'']].

==Abstracting and indexing==
The journal is abstracted and indexed in the [[Science Citation Index Expanded]],<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-03-16}}</ref> [[Current Contents]]/Engineering, Computing & Technology,<ref name=ISI/> and [[Scopus]].<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2015-03-16}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 0.400.<ref name=WoS>{{cite book |year=2014 |chapter=ACM Transactions on Algorithms |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== Past editors ==
The following persons have been editors-in-chief of the journal:<ref>{{Cite web |title=ACM Transactions on Algorithms (TALG) |url=http://dl.acm.org/citation.cfm?id=J982&picked=prox&cfid=678852433&cftoken=99494052 |website=ACM Digital Library |publisher=Association for Computing Machinery |accessdate=2015-05-29}}</ref> 
* Harold Gabow (2005-2008)
* Susanne Albers (2008-2014)
* Aravind Srinivasan (2014-present)

== See also ==
*''[[Algorithmica]]''
*[[Algorithms (journal)|''Algorithms'' (journal)]]

==References==
{{Reflist}}

==External links==
* {{Official website|http://talg.acm.org/}}

[[Category:Association for Computing Machinery academic journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 2005]]
[[Category:English-language journals]]
[[Category:Computer science journals]]