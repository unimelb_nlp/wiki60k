{{Infobox bibliographic database
| title       = Web of Science
| image       = [[File:Web of Science Logo.png]]
| caption     = 
| producer    = Clarivate Analytics
| country     = United States
| history     = 
| languages   = 
| providers   =  
| cost        = 
| disciplines = Science, social science, arts, humanities (supports 256 disciplines) 
| depth       = Citation indexing,  author, topic title, subject keywords, abstract,  periodical title, author's address, publication year 
| formats     = Full text articles, reviews, editorials, chronologies, abstracts, proceedings (journals and book-based ), technical papers 
| temporal    = 1900 to present 
| geospatial  = 
| number      = 90 million + 
| updates     = 
| p_title     = 
| p_dates     = 
| ISSN        =
| web         = http://ipscience.thomsonreuters.com/product/web-of-science/
| titles      = 
}}
'''Web of Science''' (previously known as '''Web of Knowledge''') is an online subscription-based scientific [[citation index]]ing service originally produced by the [[Institute for Scientific Information]] (ISI), now maintained by [[Clarivate Analytics]] (previously the Intellectual Property and Science business of [[Thomson Reuters]]), that provides a comprehensive citation search. It gives access to multiple databases that reference cross-disciplinary research, which allows for in-depth exploration of specialized sub-fields within an [[academic discipline|academic or scientific discipline]].<ref>Drake, Miriam A. Encyclopedia of Library and Information Science. New York, N.Y.: Marcel Dekker, 2004.</ref>

==Background and history==
A citation index is built on the fact that citations in science serve as linkages between similar research items, and lead to matching or related scientific literature, such as [[academic journal|journal articles]], [[conference proceedings]], abstracts, etc. In addition, literature which shows the greatest impact in a particular field, or more than one discipline, can be easily located through a citation index. For example, a paper's influence can be determined by linking to all the papers that have cited it. In this way, current trends, patterns, and emerging fields of research can be assessed. [[Eugene Garfield]], the "father of citation indexing of academic literature,"<ref>Jacso, Peter. The impact of Eugene Garfield through the prizm of Web of Science. Annals of Library and Information Studies, Vol. 57, September 2010, P. 222. [http://nopr.niscair.res.in/bitstream/123456789/10235/4/ALIS%2057%283%29%20222-247.pdf PDF]</ref> who launched the [[Science Citation Index]] (SCI), which in turn led to the Web of Science,<ref>Garfield, Eugene, Blaise Cronin, and Helen Barsky Atkins. The Web of Knowledge: A Festschrift in Honor of Eugene Garfield. Medford, N.J.: Information Today, 2000.</ref> wrote:

{{Quote|Citations are the formal, explicit linkages between papers that have particular points in common. A citation index is built around these linkages. It lists publications that have been cited and identifies the sources of the citations. Anyone conducting a literature search can find from one to dozens of additional papers on a subject just by knowing one that has been cited. And every paper that is found provides a list of new citations with which to continue the search.
The simplicity of citation indexing is one of its main strengths.<ref>Garfield, Garfield, Eugene. Citation indexing: Its theory and application in science, technology, and humanities. New York: Wiley, 1979, P. 1. [http://garfield.library.upenn.edu/ci/chapter1.PDF PDF]</ref>}}

===Search and analysis===
Web of Science is described as a unifying research tool which enables the user to acquire, analyze, and disseminate database information in a timely manner. This is accomplished because of the creation of a common vocabulary, called [[Ontology (information science)|ontology]], for varied search terms and varied data. Moreover, search terms generate related information across categories.

Acceptable content for Web of Science is determined by an evaluation and selection process based on the following criteria: impact, influence, timeliness, [[peer review]], and geographic representation.<ref name=describe/>

Web of Science employs various search and analysis capabilities. First, citation indexing is employed, which is enhanced by the capability to search for results across disciplines. The influence, impact, history, and [[methodology]] of an idea can be followed from its first instance, notice, or referral to the present day. This technology points to a deficiency with the [[Index term|keyword]]-only method of searching.

Second, subtle trends and patterns relevant to the literature or research of interest, become apparent. Broad trends indicate significant topics of the day, as well as the history relevant to both the work at hand, and particular areas of study.

Third, trends can be [[mathematical modeling|graphically]] represented.<ref name=describe>[http://thomsonreuters.com/content/science/pdf/Web_of_Knowledge_factsheet.pdf Overview and Description]. ISI Web of Knowledge. Thomson Reuters. 2010. Accessed on 2010-06-24</ref><ref>{{cite web|url=http://wokinfo.com/realfacts/qualityandquantity/|title=Web of Knowledge > Real Facts > Quality and Quantity|accessdate = 2010-05-05}}</ref>

==Coverage==
[[File:Web of Science Core Collection.png|thumb|Entering a search query on Web of Science.]]
Expanding the coverage of Web of Science, in November 2009 Thomson Reuters introduced ''Century of Social Sciences''. This service contains files which trace social science research back to the beginning of the 20th century,<ref name=InfoToNov2009>"''Thomson Reuters introduces century of social sciences''". Information Today 26.10 (2009): 10. General OneFile. Web. 23 June 2010.  [http://find.galegroup.com/gps/infomark.do?&contentSet=IAC-Documents&type=retrieve&tabID=T003&prodId=IPS&docId=A211794482&source=gale&srcprod=ITOF&userGroupName=mlin_c_marlpl&version=1.0  Document URL].</ref><ref name=ComLibNov2009>Thomson Reuters introduces century of social sciences." Computers in Libraries 29.10 (2009): 47. General OneFile. Internet. 23 June 2010. [http://find.galegroup.com/gps/infomark.do?&contentSet=IAC-Documents&type=retrieve&tabID=T003&prodId=IPS&docId=A211236981&source=gale&srcprod=ITOF&userGroupName=mlin_c_marlpl&version=1.0 Document URL]</ref> and Web of Science now has indexing coverage from the year 1900 to the present.<ref name=oview>
{{Cite web |title =Overview - Web of Science| publisher =Thomson Reuters| year = 2010
  | url =http://thomsonreuters.com/products_services/science/science_products/a-z/web_of_science
  | format =Overview of coverage gleaned from promotional language.  
  | accessdate =2010-06-23}}</ref><ref name=UoOL>
{{Cite web| last = Lee| first =Sul H.| title =Citation Indexing and ISI's Web of Science 
  | publisher =The University of Oklahoma Libraries| year =2010
  | url =http://www.ou.edu/webhelp/librarydemos/isi/ | format =Discussion of finding literature manually. Description of [[citation index]]ing, and Web of Science.| accessdate =2010-06-23}}</ref> {{As of|2014|09|03}}, the  multidisciplinary coverage of the Web of Science encompasses over 50,000 scholarly books, 12,000 journals and 160,000 conference proceedings.<ref name="Web of Science-2014">{{cite web|url=http://wokinfo.com/citationconnection/realfacts/#regional|title=Web of Knowledge - Real Facts - IP & Science - Thomson Reuters|first=Thomson|last=Reuters|publisher=|accessdate=10 December 2016}}</ref> The selection is made on the basis of [[impact factor|impact evaluations]] and comprise [[open-access journal]]s, spanning multiple [[academic discipline]]s. The coverage includes: the [[science]]s, [[social science]]s, [[the arts|arts]], and humanities, and goes across disciplines.<ref name=oview/><ref name=facts/> However, Web of Science does not index all journals, and its coverage in some fields is less complete than in others.

Furthermore, as of September 3, 2014 the total file count of the Web of Science was 90 million records, which included over a billion cited references. This citation service on average indexes around 65 million items per year, and it is described as the largest accessible citation database.<ref name=facts>[http://wokinfo.com/citationconnection/  Bulleted fact sheet]. Thomson Reuters. 2014.</ref>

Titles of foreign-language publications are translated into English and so cannot be found by searches in the original language.<ref name=harvard-search>{{Cite web
  |title =Some Searching Conventions
  | publisher =President and Fellows of Harvard College   | date = December 3, 2009   | url =http://hcl.harvard.edu/research/guides/citationindex/#some   | format =    | accessdate =2010-06-23}}</ref>

==Citation databases==
[[File:Web of science next generation.png|thumb|Web of Science databases.]]
Web of Science consist of seven online databases:<ref name=included/><ref name="Web of Science-2013">[http://wokinfo.com/media/pdf/WoSFS_08_7050.pdf Jo Yong-Hak. Web of Science. Thomson Reuters, 2013]</ref>
*[[Conference Proceedings Citation Index]] covers more than 160,000 conference titles in the Sciences starting from 1990 to the present day
*[[Science Citation Index Expanded]] covers more than 8,500 notable journals encompassing 150 disciplines. Coverage is from the year 1900 to the present day.
*[[Social Sciences Citation Index]] covers more than 3,000 journals in social science disciplines.  Range of coverage is from the year 1900 to the present day.
*[[Arts & Humanities Citation Index]] covers more than  1,700 arts and humanities journals starting from 1975. In addition, 250 major scientific and social sciences journals are also covered. 
*[[Index Chemicus]] lists more than 2.6 million compounds. The time of coverage is from 1993 to present day.
*[[Current Chemical Reactions]] indexes over one million reactions, and the range of coverage is from 1986 to present day. The '' INPI '' archives from 1840 to 1985 are also indexed in this database.
*[[Book Citation Index]] covers more than 60,000 editorially selected books starting from 2005.

===Regional databases===
Since 2008, the Web of Science hosts a number of regional citation indices. 
The [[Chinese Science Citation Database]], produced in partnership with the [[Chinese Academy of Sciences]], was the first one in a language other than English.<ref>{{cite web |title=Chinese Science Citation Database |url=http://thomsonreuters.com/en/products-services/scholarly-scientific-research/scholarly-search-and-discovery/chinese-science-citation-database.html}}</ref>
It was followed in 2013 by the [[SciELO Citation Index]], covering Brazil, Spain, Portugal, the Caribbean and South Africa, and more 12 countries of [[Latin America]];<ref>{{cite web |title=Thomson Reuters Collaborates with SciELO to Showcase Emerging Research Centers within Web of Knowledge |url=http://thomsonreuters.com/en/press-releases/2013/thomson-reuters-collaborates-with-scielo-to-showcase-emerging-research-centers-within-web-of-knowledge.html}}</ref>
by the [[Korea Citation Index]] (KCI) in 2014, with updates from the [[Korea Research Foundation|South Korean National Research Foundation]].,<ref>{{cite web |title=Thomson Reuters Collaborates with National Research Foundation of Korea to Showcase the Region's Research in Web of Science |url=http://thomsonreuters.com/en/press-releases/2014/tr-collaborates-with-national-research-foundation-of-korea.html}}</ref> and by the Russian Science Citation index in 2015.<ref>{{cite web|url=http://wokinfo.com/products_tools/multidisciplinary/rsci/|title=RSCI - IP & Science - Thomson Reuters|first=Thomson|last=Reuters|publisher=|accessdate=10 December 2016}}</ref>

=== Contents ===
The seven [[citation index|citation indices]] listed above contain references which have been cited by other articles. One may use them to undertake cited reference search, that is, locating articles that cite an earlier, or current publication. One may search citation databases by topic, by author, by source title, and by location. Two chemistry databases,  ''Index Chemicus'' and  ''Current Chemical Reactions'' allow for the creation of structure drawings, thus enabling users to locate [[chemical compound]]s and reactions.

===Abstracting and indexing===
The following  types of literature are indexed: scholarly books, [[peer review]]ed journals, original research articles, reviews, editorials, chronologies, abstracts, as well as other items. Disciplines included in this index are  [[agriculture]], [[biological sciences]], [[engineering]], medical and [[life sciences]], [[physics|physical]] and [[chemical sciences]], [[anthropology]], law, [[library science]]s, [[architecture]], dance, music, film, and theater. Seven citation databases encompasses coverage of the above disciplines.<ref name=UoOL/><ref name="Web of Science-2014" /><ref name=included>
{{Cite web |title=Coverage - Web of Science |publisher=Thomson Reuters |year=2010 |url=http://thomsonreuters.com/products_services/science/science_products/a-z/web_of_science |format=Overview of coverage gleaned from promotional language. |accessdate=2010-06-23}}</ref>

==Limitations in the use of citation analysis==
{{main article|Citation analysis| San Francisco Declaration on Research Assessment}}
As with other scientific approaches, [[scientometrics]] and [[bibliometrics]] have their own limitations. Recently, a criticism was voiced pointing toward certain deficiencies of the [[journal impact factor]] (JIF) calculation process, based on Thomson Reuters Web of Science, such as: journal citation distributions usually are highly skewed towards established journals; journal impact factor properties are field-specific and can be easily manipulated by editors, or even by changing the editorial policies; this makes the entire process essentially non-transparent.<ref name="Declaration">[http://am.ascb.org/dora/ San Francisco Declaration on Research Assessment: Putting science into the assessment of research, December 16, 2012]</ref>

Regarding the more objective journal metrics, there is a growing view that for greater accuracy it must be supplemented with [[article-level metrics]] and peer-review.<ref name="Declaration" /> Thomson Reuters replied to criticism in general terms by stating that "no one metric can fully capture the complex contributions scholars make to their disciplines, and many forms of scholarly achievement should be considered."<ref>Thomson Reuters Statement Regarding the San Francisco Declaration on Research Assessment [http://researchanalytics.thomsonreuters.com/]</ref>

== See also ==
{{div col|2}}
* [[CSA (database company)|CSA databases]]
* [[Dialog (online database)]]
* [[Energy Citations Database]]
* [[Energy Science and Technology Database]]
* [[ETDEWEB]]
* [[Google Scholar]]
* [[h-index]]
* [[List of academic journal search engines]]
* [[Materials Science Citation Index]]
* [[PASCAL (database)|PASCAL database]]
* [[PubMed Central]]
* [[ResearchGate]]
* [[SciELO]]
* [[Scopus]]
* [[VINITI Database RAS]]
* [[Open Citations Corpus]]
{{div col end}}

== References ==
<!---Following References Need To Be Better Placed In Article?---<ref name=dbase-List>{{Cite web| last =''ISI Web of Knowledge''| title =Suite of databases| publisher =Thomson Reuters| year =2010| url = http://thomsonreuters.com/products_services/science/science_products/a-z/isi_web_of_knowledge?parentKey=555184 | format =List of databases that are part of the Web of Knowledge suite.| accessdate =2010-06-24}}</ref><ref name=AtoZ>{{Cite web| last = ISI Web of Knowledge platform| title =Available databases A to Z| publisher =Thomson Reuters| year =2010| url =http://wokinfo.com/products_tools/products/ | format =Choose databases on method of discovery and analysis| accessdate =2010-06-24}}</ref><ref>[http://wokinfo.com/media/pdf/SSR1103443WoK5-2_web3.pdf Thomson Reuters Web of Knowledge. Thomson Reuters, 2013.]</ref>--->
{{Reflist|30em}}

==External links==
* [http://wokinfo.com/ About Web of Science]
* [http://webofknowledge.com Web of Science]
* [https://web.archive.org/web/20110521161422/http://hcl.harvard.edu/research/guides/citationindex/ Searching the Citation Indexes (Web of Science)] Harvard College Library. 2010. (archive)
* [http://video.mit.edu/watch/web-of-science-12339/ MIT Web of Science video tutorial]. 2008.

{{Thomson Reuters}}

{{DEFAULTSORT:Web Of Science}}
[[Category:Bibliographic databases and indexes]]
[[Category:Online databases]]
[[Category:Thomson Reuters]]