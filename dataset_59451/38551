{{Taxobox
| image = Rhina ancylostoma-2.jpg
| status = VU 
| status_system = IUCN3.1
| status_ref =<ref name="iucn">{{IUCN |assessor=McAuley, R. |assessor2=L.J.V. Compagno |last-assessor-amp=yes |year=2003 |id=41848 |title=''Rhina ancylostoma'' |version=2012.2}}</ref>
| regnum = [[Animal]]ia
| phylum = [[Chordate|Chordata]]
| classis = [[Chondrichthyes]]
| subclassis = [[Elasmobranchii]]
| ordo = [[Rajiformes]]
| familia = [[Rhinidae]]
| familia_authority = [[Johannes Peter Müller|J. P. Müller]] and [[Friedrich Gustav Jakob Henle|Henle]], 1841
| genus = '''''Rhina'''''
| genus_authority = [[Marcus Elieser Bloch|Bloch]] & [[Johann Gottlob Theaenus Schneider|J. G. Schneider]], 1801
| species = '''''R. ancylostoma'''''
| binomial = ''Rhina ancylostoma''
| binomial_authority = [[Marcus Elieser Bloch|Bloch]] & [[Johann Gottlob Schneider|J. G. Schneider]], 1801
| range_map = Rhina ancylostoma rangemap.png
| range_map_caption = Range of the bowmouth guitarfish<ref name="last and stevens"/>
| synonyms = ''Rhina cyclostomus'' <small>Swainson, 1839</small>
}}
The '''bowmouth guitarfish''' (''Rhina ancylostoma''), also called the '''shark ray''' or '''mud skate''', is a [[species]] of [[Batoidea|ray]] and a member of the [[family (biology)|family]] [[Rhinidae]]. Its evolutionary affinities are not fully resolved, though it may be related to [[Rhinobatidae|true guitarfish]]es and [[Skate (fish)|skate]]s. This rare species occurs widely in the [[tropical]] coastal waters of the western [[Indo-Pacific]], at depths of up to {{convert|90|m|ft|abbr=on}}. Highly distinctive in appearance, the bowmouth guitarfish has a wide and thick body with a rounded snout and large shark-like [[dorsal fin|dorsal]] and [[caudal fin|tail fins]]. Its mouth forms a W-shaped undulating line, and there are multiple thorny ridges over its head and back. It has a dorsal color pattern of many white spots over a bluish gray to brown background, with a pair of prominent black markings over the [[pectoral fin]]s. This large species can reach a length of {{convert|2.7|m|ft|abbr=on}} and weight of {{convert|135|kg|lb|abbr=on}}.

Usually found near the [[sea floor]], the bowmouth guitarfish prefers sandy or muddy areas near underwater structures. It is a strong-swimming [[predator]] of [[bony fish]]es, [[crustacean]]s, and [[mollusc]]s. This species [[viviparous|gives live birth]] to litters of two to eleven pups, which are nourished during [[gestation]] by [[yolk]]. The [[International Union for Conservation of Nature]] (IUCN) has assessed the bowmouth guitarfish as [[Vulnerable species|Vulnerable]] because it is widely caught by [[artisan fishing|artisanal]] and [[commercial fishing|commercial fisheries]] for its valuable fins and meat. It is viewed as a nuisance by [[trawl]]ers, however, because its bulk and thorny skin cause it to damage netted catches. [[Habitat degradation]] and [[habitat destruction|destruction]] pose an additional, significant challenge to this ray's survival. The bowmouth guitarfish adapts well to captivity and is displayed in [[public aquarium]]s.

==Taxonomy and phylogeny==
German [[naturalist]]s [[Marcus Elieser Bloch]] and [[Johann Gottlob Schneider]] described the bowmouth guitarfish in their 1801 ''Systema Ichthyologiae''. Their account was based on a {{convert|51|cm|in|abbr=on}} long [[type specimen|specimen]], now lost, collected off the [[Coromandel Coast]] of India. The genus name ''Rhina'' comes from the [[Ancient Greek|Greek]] ''rhinos'' ("snout"); the [[specific name (zoology)|specific epithet]] ''ancylostoma'' is derived from the Greek ''ankylos'' ("curved" or "crooked") and ''stoma'' ("mouth").<ref name="bloch and schneider"/><ref name="paepke"/> Although Block and Schneider wrote the epithet as ''ancylostomus'' and that form appears in some literature, most modern sources regard the correct form to be ''ancylostoma''.<ref name="cas"/> Other [[common name]]s for this species include shark ray, mud skate, shortnose mud skate, bow-mouthed angel fish, and bow-mouthed angel shark.<ref name="fishbase"/>

The evolutionary relationships between the bowmouth guitarfish and other rays are debated. [[Morphology (biology)|Morphological]] evidence generally points to a close relationship between ''Rhina'' and ''[[Rhynchobatus]]'', which are a group of rays known as the wedgefishes that also have large, shark-like fins. Morphological analyses have tended to place these two genera [[basal (phylogenetics)|basally]] among rays, though some have them as basal to just the [[guitarfish]]es (Rhinobatidae) and [[Skate (fish)|skate]]s (Rajidae) while others have them basal to all other rays except [[sawfish]]es (Pristidae).<ref name="nishida"/><ref name="mceachran and aschliman"/><ref name="aschliman et al"/> A 2012 study based on [[mitochondrial DNA]] upheld ''Rhina'' and ''Rhynchobatus'' as sister [[taxon|taxa]] related to the guitarfishes, but also unexpectedly found that they formed a [[clade]] with the sawfishes rather than the skates.<ref name="naylor et al"/>

In terms of classification, Bloch and Schneider originally placed the bowmouth guitarfish in the order Abdominales, a now-obsolete grouping of fishes defined by the positioning of their [[pelvic fin]]s directly behind the [[pectoral fin]]s.<ref name="bloch and schneider"/> Modern sources have included it variously in the order [[Rajiformes]], Rhinobatiformes, Rhiniformes, or the newly proposed Rhinopristiformes.<ref name="mceachran and aschliman"/><ref name="naylor et al"/> The placement of the bowmouth guitarfish in the family Rhinidae originates from the group "Rhinae", consisting of ''Rhina'' and ''Rhynchobatus'', in [[Johannes Peter Müller|Johannes Müller]] and [[Friedrich Gustav Jakob Henle|Jakob Henle]]'s 1841 ''Systematische Beschreibung der Plagiostomen''.<ref name="muller and henle"/> Later authors have also assigned this species to the family Rhinobatidae or Rhynchobatidae.<ref name="mceachran and aschliman"/><ref name="compagno and last"/> [[Joseph S. Nelson|Joseph Nelson]], in the 2006 fourth edition of ''[[Fishes of the World]]'', placed this species as the sole member of Rhinidae in the order Rajiformes, which is supported by morphological but not molecular data.<ref name="aschliman et al"/><ref name="nelson"/>

==Description==
[[File:Rhina ancylostoma georgia.jpg|thumb|upright=1.2|left|The rounded head, humpbacked profile, and large fins of the bowmouth guitarfish give it a unique appearance.]]
The bowmouth guitarfish is a heavily built fish growing to {{convert|2.7|m|ft|abbr=on}} long and {{convert|135|kg|lb|abbr=on}} in weight.<ref name="last and stevens"/><ref name="fishbase"/> The head is short, wide, and flattened with an evenly rounded snout; the front portion of the head, including the medium-sized eyes and large [[spiracle]]s, is clearly distinct from the body. The long nostrils are transversely oriented and have well-developed skin flaps on their anterior margins. The lower jaw has three protruding lobes that fit into corresponding depressions in the upper jaw.<ref name="last and stevens"/><ref name="randall and hoover"/> There are around 47 upper and 50 lower tooth rows arranged in winding bands; the teeth are low and blunt with ridges on the crown. The five pairs of ventral [[gill slit]]s are positioned close to the lateral margins of the head.<ref name="last and stevens"/><ref name="smith et al"/>

The body is deepest in front of the two tall and falcate (sickle-shaped) [[dorsal fin]]s. The first dorsal fin is about a third larger than the second and originates over the [[pelvic fin]] origins. The second dorsal fin is located midway between the first dorsal and the [[caudal fin]]. The broad and triangular [[pectoral fin]]s have a deep indentation where their leading margins meet the head. The [[pelvic fin]]s are much smaller than the pectoral fins, and the [[anal fin]] is absent. The tail is much longer than the body and ends in a large, crescent-shaped [[caudal fin]]; the lower caudal fin lobe is more than half the length of the upper.<ref name="last and stevens"/><ref name="compagno and last"/><ref name="randall and hoover"/>

The entire dorsal surface of the bowmouth guitarfish has a grainy texture from a dense covering of tiny [[dermal denticle]]s. A thick ridge is present along the midline of the back, which bears a band of sharp, robust thorns. There are also a pair of thorn-bearing ridges in front of the eyes, a second pair running from above the eyes to behind the spiracles, and a third pair on the "shoulders". This species is bluish to brownish gray above, lightening towards the margins of the head and over the pectoral fins. There are prominent white spots scattered over the body and fins, a white-edged black marking above each pectoral fin, and two dark transverse bands atop the head between the eyes. The underside is light gray to white. Young rays are more vividly colored than adults, which are browner with fainter patterning and proportionately smaller spots.<ref name="last and stevens"/>

==Distribution and habitat==
While uncommon, the bowmouth guitarfish is widely distributed in the coastal [[tropical]] waters of the western [[Indo-Pacific]]. In the Indian Ocean, it is found from [[KwaZulu-Natal]] in South Africa to the [[Red Sea]] (including the [[Seychelles]]), across the [[Indian subcontinent]] and [[Southeast Asia]] (including the [[Maldives]]), to [[Shark Bay]] in [[Western Australia]]. Its Pacific range extends northward to Korea and southern Japan, eastward to New Guinea, and southward to [[New South Wales]].<ref name="last and stevens"/><ref name="randall and hoover"/> Found between {{convert|3|and|90|m|ft|abbr=on|-1}} deep, this ray spends most of its time near the [[sea floor]] but can occasionally be seen swimming in midwater. It favors sandy or muddy [[habitat]]s, and can also be found in the vicinity of rocky and [[coral reef]]s and [[shipwreck]]s.<ref name="last and stevens"/><ref name="michael"/>

==Biology and ecology==
[[File:Tigershark2.jpg|thumb|The tiger shark preys on the bowmouth guitarfish.]]
The bowmouth guitarfish is a strong swimmer that propels itself with its tail like a shark. It is [[nocturnal|more active at night]] and is not known to be territorial.<ref name="ferrari and ferrari"/> This species feeds mainly on [[demersal fish|demersal]] [[bony fish]]es such as [[Sciaenidae|croakers]] and [[crustacean]]s such as [[crab]]s and [[shrimp]]; [[bivalve]]s and [[cephalopod]]s are also consumed. Its bands of flattened teeth allow it to crush hard-shelled prey.<ref name="compagno and last"/><ref name="raje"/> Curiously, two bowmouth guitarfishes examined in a 2011 [[stable isotope]] study were found to have fed on [[pelagic zone|pelagic]] rather than demersal animals, in contrast to previous observations.<ref name="borrell et al"/>

The [[tiger shark]] (''Galeocerdo cuvier'') is known to prey on the bowmouth guitarfish.<ref name="simpfendorfer et al"/> The ray is protected by the thorns on its head and back, and it may ram perceived threats.<ref name="fishbase"/> [[Parasite]]s documented from this species include the [[tapeworm]]s ''[[Carpobothrium rhinei]]'',<ref name="sarada et al"/> ''[[Dollfusiella michiae]]'',<ref name="campbell and beveridge"/> ''[[Nybelinia southwelli]]'',<ref name="palm and walter"/> ''[[Stoibocephalum arafurense]]'',<ref name="cielocha and jensen"/> and ''[[Tylocephalum carnpanulatum]]'',<ref name="butler"/> the [[leech]] ''[[Pontobdella macrothela]]'',<ref name="de silva"/> the [[trematode]] ''[[Melogonimus rhodanometra]]'',<ref name="bray et al"/> the [[monogenea]]ns ''[[Branchotenthes robinoverstreeti]]''<ref name="bullard and dippenaar"/> and ''[[Monocotyle ancylostomae]]'',<ref name="zhang et al"/> and the [[copepod]]s ''[[Nesippus vespa]]'',<ref name="dippenaar et al"/> ''[[Pandarus cranchii]]'', and ''[[Pandarus smithii|P. smithii]]''.<ref name="izawa"/> There is a record of a bowmouth guitarfish being cleaned by [[bluestreak cleaner wrasse]]s (''Labroides dimidiatus'').<ref name="michael"/>

Reproduction in the bowmouth guitarfish is [[viviparous]], with the developing [[embryo]]s sustained to term by [[yolk]]. Adult females have a single functional [[ovary]] and [[uterus]]. The litter size varies between two and eleven pups, and newborns measure {{convert|45|-|51|cm|in|abbr=on}} long.<ref name="michael"/><ref name="devadoss and matcha"/><ref name="last et al"/> [[Sexual maturity]] is attained at lengths of {{convert|1.5|-|1.8|m|ft|abbr=on}} for males and over {{convert|1.8|m|ft|abbr=on}} in females. Females grow larger than males.<ref name="last and stevens"/><ref name="raje"/>

==Human interactions==
[[File:shark ray newport.jpg|thumb|"Sweet Pea", a female bowmouth guitarfish at the Newport Aquarium.]]
Throughout its range, the bowmouth guitarfish is caught incidentally or intentionally by [[artisan fishing|artisanal]] and [[commercial fishing|commercial fisheries]] using [[trawl]]s, [[gillnet]]s, and line gear.<ref name="iucn"/> The fins are extremely valuable due to their use in [[shark fin soup]], and are often the only parts of the fish kept and brought to market. However, the meat may also be sold fresh or dried and salted, and it is highly esteemed in India.<ref name="fishbase"/><ref name="raje"/> When caught as [[bycatch]] in trawls, the bowmouth guitarfish is considered a nuisance because its strength and rough skin make it difficult to handle, and as the heavy ray thrashes in the net it can damage the rest of the catch.<ref name="last and stevens"/> In Thailand, the enlarged thorns of this species are used to make bracelets.<ref name="fowler et al"/>

The [[International Union for Conservation of Nature]] (IUCN) has assessed the bowmouth guitarfish as [[Vulnerable species|Vulnerable]]. It is threatened by fishing and by [[habitat destruction]] and [[habitat degradation|degradation]], particularly from [[blast fishing]], [[coral bleaching]], and [[siltation]]. Its numbers are known to have declined substantially in Indonesian waters, where it is one of the large rays targeted by a mostly unregulated gillnet fishery. The IUCN has given this species a regional assessment of [[Near Threatened]] in Australian waters, where it is not a targeted species but is taken as bycatch in bottom trawls. The installation of [[turtle excluder device]]s on some Australian trawlers has benefited this species.<ref name="iucn"/> Since it is rare and faces many conservation threats, the bowmouth guitarfish has been called "the [[panda]] of the aquatic world".<ref name="underwatertimes"/>

It is a popular subject of [[public aquarium]]s and fares relatively well, with one individual having lived for seven years in captivity.<ref name="last and stevens"/><ref name="michael"/> In 2007, the [[Newport Aquarium]] in [[Kentucky]] initiated the world's first [[captive breeding]] program for this species.<ref name="underwatertimes"/> Newport Aquarium announced in January 2014 that the female, "Sweet Pea", had become pregnant and given birth to seven pups.<ref name="aquariumworks"/> By February 2014, all seven pups had died.<ref>{{cite web | url=http://local12.com/news/features/top-stories/stories/newport-aquarium-says-shark-ray-pups-died-8807.shtml | title=Newport Aquarium says shark ray pups died | publisher=WKRC-TV | date=26 February 2014 | accessdate=26 February 2014}}</ref> On January 7, 2016, Sweet Pea gave birth to nine shark pups<ref>{{cite web | url=http://www.fox19.com/story/30910069/newport-aquarium-shark-ray-gives-birth-to-nine-pups | title=Newport Aquarium shark ray gives birth to nine pups | publisher=FOX19NOW | date=7 January 2016 | accessdate=10 February 2016}}</ref> which were eating on their own and still gaining weight by February 10, 2016.<ref>{{cite web | url=http://aquariumworks.org/2016/02/10/shark-ray-pups-reach-milestones/ | title=Shark Ray Pups Reach Milestones | publisher=aquariumworks.org | date=10 February 2016 | accessdate=10 February 2016}}</ref> Newport Aquarium later announced that the pups would be moved into a coral reef exhibit where they can be viewed by the public starting on June 24.<ref>{{Cite web|url=http://www.wlwt.com/news/rare-shark-ray-pups-to-move-to-exhibit-at-newport-aquarium/40194054|title=Rare shark ray pups to move to exhibit at Newport Aquarium|last=Ferrell|first=Nikki|access-date=2016-06-25}}</ref> The species also bred at the [[Marine Life Park|S.E.A. Aquarium]] in [[Singapore]] in 2015.<ref>{{cite web | url=http://www.straitstimes.com/singapore/sea-aquarium-successfully-breeds-shark-ray-pup-a-vulnerable-species/ | title=S.E.A. Aquarium successfully breeds shark ray pup, a vulnerable species | publisher=''[[The Straits Times]]'' | date=5 May 2016 | accessdate=12 August 2016}}</ref>

==References==
{{reflist|colwidth=30em|refs=

<ref name="aquariumworks">{{cite news |url=http://aquariumworks.org/2014/01/29/newport-aquariums-sweet-pea-the-first-documented-shark-ray-to-breed-in-captivity-gives-birth-to-seven-pups/ |date=January 29, 2014 |title=Newport Aquarium’s Sweet Pea, the First Documented Shark Ray to Breed in Captivity, Gives Birth to Seven Pups |periodical=Aquarium Works |publisher=Newport Aquarium |accessdate=February 1, 2014}}</ref>

<ref name="aschliman et al">{{cite book |author1=Aschliman, N.C. |author2=Claeson, K.M. |author3=McEachran, J.D. |year=2012 |chapter=Phylogeny of Batoidea |editor1=Carrier, J.C. |editor2=Musick, J.A. |editor3=Heithaus, M.R. |title=Biology of Sharks and Their Relatives |edition=second |pages=57–98 |publisher=CRC Press |isbn=1439839247}}</ref>

<ref name="bloch and schneider">{{cite book |author1=Bloch, M.E. |author2=Schneider, J.G. |year=1801 |title=Systema Ichthyologiae Iconibus CX Ilustratum |publisher=Berolini |page=352}}</ref>

<ref name="borrell et al">{{cite journal |title=Trophic ecology of elasmobranchs caught off Gujarat, India, as inferred from stable isotopes |author1=Borrell, A. |author2=Cardona, L. |author3=Kumarran, R.P. |author4=Aguilar, A. |journal=ICES Journal of Marine Science |volume=68 |issue=3 |pages=547–554 |doi=10.1093/icesjms/fsq170 |year=2011}}</ref>

<ref name="bray et al">{{cite journal |doi=10.1007/BF00009239 |author1=Bray, R.A. |author2=Brockerhoff, A. |author3=Cribb, T.H. |title=''Melogonimus rhodanometra '' n. g., n. sp. (Digenea: Ptychogonimidae) from the elasmobranch ''Rhina ancylostoma'' Bloch & Schneider (Rhinobatidae) from the southeastern coastal waters of Queensland, Australia |journal=Systematic Parasitology |volume=30 |issue=1 |date=January 1995 |pages=11–18}}</ref>

<ref name="bullard and dippenaar">{{cite journal |title=''Branchotenthes robinoverstreeti'' n. gen. and n. sp. (Monogenea: Hexabothriidae) from Gill Filaments of the Bowmouth Guitarfish, ''Rhina ancylostoma'' (Rhynchobatidae), in the Indian Ocean |author1=Bullard, S.A. |author2=Dippenaar, S.M. |journal=The Journal of Parasitology |volume=89 |issue=3 |date=June 2003 |pages=595–601 |pmid=12880262 |doi=10.1645/0022-3395(2003)089[0595:BRNGAN]2.0.CO;2 }}</ref>

<ref name="butler">{{cite journal |doi=10.1071/ZO9870343 |title=Taxonomy of Some Tetraphyllidean Cestodes From Elasmobranch Fishes |author=Butler, S.A. |journal=Australian Journal of Zoology |volume=35 |issue=4 |pages=343–371 |year=1987}}</ref>

<ref name="campbell and beveridge">{{cite journal |title=''Oncomegas aetobatidis'' sp nov (Cestoda: Trypanorhyncha), a re-description of ''O. australiensis'' Toth, Campbell & Schmidt, 1992 and new records of trypanorhynch cestodes from Australian elasmobranch fishes |author1=Campbell, R.A. |author2=Beveridge, I. |journal=Transactions of the Royal Society of South Australia |volume=133 |pages=18–29 |year=2009}}</ref>

<ref name="cas">{{cite web |editor=Eschmeyer, W.N. |title=''ancylostomus, Rhina'' |publisher=Catalog of Fishes |url=http://research.calacademy.org/research/ichthyology/catalog/fishcatget.asp?tbl=species&spid=7020 |accessdate=May 14, 2013}}</ref>

<ref name="cielocha and jensen">{{cite journal |title=''Stoibocephalum'' n. gen. (Cestoda: Lecanicephalidea) from the sharkray, ''Rhina ancylostoma'' Bloch & Schneider (Elasmobranchii: Rhinopristiformes), from northern Australia |author1=Cielocha, J.J. |author2=Jensen, K. |journal=Zootaxa |volume=3626 |issue=4 |pages=558–568 |year=2013 |doi=10.11646/zootaxa.3626.4.9}}</ref>

<ref name="compagno and last">{{cite book |chapter=Rhinidae |author1=Compagno, L.J.V. |author2=Last, P.R. |pages=1418–1422 |editor1=Carpenter, K.E. |editor2=Niem, V.H. |title=FAO Identification Guide for Fishery Purposes: The Living Marine Resources of the Western Central Pacific |publisher=Food and Agricultural Organization of the United Nations |year=1999 |isbn=92-5-104302-7}}</ref>

<ref name="de silva">{{cite journal |title=The occurrence of ''Pontobdella (Pontobdellina) macrothela'' Schmarda and ''Pontobdella aculeata'' Harding in the Wadge Bank |author=de Silva, P.H.D.H. |journal=Spolia Zeylanica |volume=30 |issue=1 |pages=35–39 |year=1963}}</ref>

<ref name="devadoss and matcha">{{cite journal |title=Some observations on the rare bow-mouth guitar fish ''Rhina ancylostoma'' |author1=Devadoss, P. |author2=Batcha, H. |journal=Indian Council of Agricultural Research Marine Fisheries Information Service Technical and Extension Series |volume=138 |pages=10–11 |year=1995}}</ref>

<ref name="dippenaar et al">{{cite journal |title=Cytochrome oxidase I sequences reveal possible cryptic diversity in the cosmopolitan symbiotic copepod ''Nesippus orientalis'' Heller, 1868 (Pandaridae: Siphonostomatoida) on elasmobranch hosts from the KwaZulu-Natal coast of South Africa  |author1=Dippenaar, S.M. |author2=Mathibela, R.B. |author3=Bloomer, P. |journal=Experimental Parasitology |volume=125 |issue=1 |pages=42–50 |year=2010 |doi=10.1016/j.exppara.2009.08.017 |pmid=19723521}}</ref>

<ref name="ferrari and ferrari">{{cite book |title=Sharks |author1=Ferrari, A. |author2=Ferrari, A. |publisher=Firefly Books |year=2002 |isbn=1-55209-629-7 |page=203}}</ref>

<ref name="fishbase">{{cite web |editor1=Froese, R. |editor2=Pauly, D. |year=2011 |url=http://www.fishbase.org/summary/Rhina-ancylostoma.html |title=''Rhina ancylostoma'', Bowmouth guitarfish |publisher=FishBase |accessdate=May 14, 2013}}</ref>

<ref name="fowler et al">{{cite book |editor=Fowler, S.L. |editor2=Reed, T.M. |editor3=Dipper, F.A. |year=2002 |title=Elasmobranch Biodiversity, Conservation and Management: Proceedings of the International Seminar and Workshop, Sabah, Malaysia, July 1997 |publisher=IUCN SSC Shark Specialist Group |isbn=2-8317-0650-5 |page=112}}</ref>

<ref name="izawa">{{cite journal |title=Redescription of eight species of parasitic copepods (Siphonostomatoida, Pandaridae) infecting Japanese elasmobranchs |author=Izawa, K. |journal=Crustaceana |volume=83 |issue=3 |pages=313–341 |doi=10.1163/001121609X12591347509329 |year=2010}}</ref>

<ref name="last and stevens">{{cite book |title=Sharks and Rays of Australia |author1=Last, P.R. |author2=Stevens, J.D. |edition=second |publisher=Harvard University Press |year=2009 |isbn=0-674-03411-2 |pages=299–300}}</ref>

<ref name="last et al">{{cite book |title=Sharks and Rays of Borneo |publisher=CSIRO Publishing |year=2010 |isbn=978-1-921605-59-8 |pages=146–147 |author1=Last, P.R. |author2=White, W.T. |author3=Caire, J.N. |author4=Dharmadi |author5=Fahmi |author6=Jensen, K. |author7=Lim, A.P.F. |author8=Manjaji-Matsumoto, B.M. |author9=Naylor, G.J.P. |author10=Pogonoski, J.J. |author11=Stevens, J.D. |author12=Yearsley, G.K. }}</ref>

<ref name="mceachran and aschliman">{{cite book |chapter=Phylogeny of Batoidea |author1=McEachran, J.D.  |author2=N. Aschliman |pages=79&ndash;113 |title=Biology of Sharks and Their Relatives |editor=Carrier, L.C. |editor2=J.A. Musick |editor3=M.R. Heithaus |publisher=CRC Press |year=2004 |isbn=0-8493-1514-X}}</ref>

<ref name="michael">{{cite book |author=Michael, S.W. |title=Reef Sharks & Rays of the World |publisher=Sea Challengers |year=1993 |isbn=0-930118-18-9 |page=71}}</ref>

<ref name="muller and henle">{{cite book |author1=Müller, J. |author2=Henle, F.G.J. |year=1841 |title=Systematische Beschreibung der Plagiostomen (volume 2) |publisher=Veit und Comp. |page=110}}</ref>

<ref name="naylor et al">{{cite journal |author1=Naylor, G.J. |author2=Caira, J.N. |author3=Jensen, K. |author4=Rosana, K.A. |author5=Straube, N. |author6=Lakner, C. |year=2012 |chapter=Elasmobranch phylogeny: A mitochondrial estimate based on 595 species |title=The Biology of Sharks and Their Relatives |pages=31–57 |edition=second |editor1=Carrier, J.C. |editor2=Musick, J.A. |editor3=Heithaus, M.R. |publisher=CRC Press |isbn=1-4398-3924-7}}</ref>

<ref name="nelson">{{cite book |title=Fishes of the World |author=Nelson, J.S. |edition=fourth |publisher=John Wiley |year=2006 |isbn=0-471-25031-7 |pages=71&ndash;74}}</ref>

<ref name="nishida">{{cite journal |author=Nishida, K. |year=1990 |title=Phylogeny of the suborder Myliobatoidei |journal=Memoirs of the Faculty of Fisheries, Hokkaido University |volume=37 |pages=1–108}}</ref>

<ref name="paepke">{{cite book |title=Bloch's Fish Collection in the Museum für Naturkunde der Humboldt-Universität zu Berlin: An Illustrated Catalog and Historical Account |author=Paepke, H.J. |publisher=Koeltz Scientific Books |year=2000 |isbn=3-904144-16-2 |pages=122–123}}</ref>

<ref name="palm and walter">{{cite journal |title=''Nybelinia southwelli'' sp. nov. (Cestoda, Trypanorhyncha) with the re-description of ''N. perideraeus'' (Shipley & Hornell, 1906) and synonymy of ''N. herdmani'' (Shipley & Hornell, 1906) with ''Kotorella pronosoma'' (Stossich, 1901) |author1=Palm, H.W. |author2=Walter, T. |journal=Bulletin of the Natural History Museum Zoology Series |volume=65 |issue=2 |pages=123–131 |year=1999}}</ref>

<ref name="randall and hoover">{{cite book |title=Coastal Fishes of Oman |author1=Randall, J.E. |author2=Hoover, J.P. |publisher=University of Hawaii Press |year=1995 |isbn=0-8248-1808-3 |page=42}}</ref>

<ref name="raje">{{cite journal |title=Skate fishery and some biological aspects of five species of skates off Mumbai |author=Raje, S.G. |journal=Indian Journal of Fisheries |volume=53 |issue=4 |pages=431–439 |year=2006}}</ref>

<ref name="sarada et al">{{cite journal |title=Studies on a new species ''Carpobothrium rhinei'' (Cestoda: Tetraphyllidea) from ''Rhina ancylostomus'' from Waltair coast |author1=Sarada, S. |author2=Lakshmi, C.V. |author3=Rao, K.H. |journal=Uttar Pradesh Journal of Zoology |volume=15 |issue=2 |pages=127–129 |year=1995}}</ref>

<ref name="smith et al">{{cite book |title=Smith's Sea Fishes |author1=Smith, J.L.B. |author2=Smith, M.M. |author3=Heemstra, P.C. |publisher=Struik |year=2003 |isbn=1-86872-890-0 |pages=128–129}}</ref>

<ref name="simpfendorfer et al">{{cite journal |doi=10.1023/A:1011021710183 |author1=Simpfendorfer, C.A. |author2=Goodreid, A.B. |author3=McAuley, R.B. |title=Size, sex and geographic variation in the diet of the tiger shark, ''Galeocerdo cuvier'', from Western Australian waters |journal=Environmental Biology of Fishes |volume=61 |pages=37–46 |year=2001}}</ref>

<ref name="underwatertimes">{{cite news |date=February 1, 2007 |url=http://www.underwatertimes.com/news.php?article_id=47826103159 |title=Newport Aquarium Launches World’s First Shark Ray Breeding Program, Adds Rare Male Shark Ray |periodical=UnderwaterTimes |accessdate=January 22, 2009}}</ref>

<ref name="zhang et al">{{cite journal |doi=10.1023/A:1022581523683 |title=A list of monogeneans from Chinese marine fishes |author1=Zhang, J.Y. |author2=Yang, T.B. |author3=Liu, L. |author4=Ding, X.J. |journal=Systematic Parasitology |volume=54 |issue=2 |pages=111–130 |year=2003 |pmid=12652065}}</ref>

}}

{{good article}}

[[Category:Rajiformes]]
[[Category:Fish of the Red Sea]]
[[Category:Fish of the Indian Ocean]]
[[Category:Fish of the Pacific Ocean]]
[[Category:Monotypic fish genera]]