<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout and guidelines. -->
{|{{Infobox aircraft begin
 |name= F-111B
 |image= F-111B CVA-43 approach July1968.jpg
 |caption= F-111B, BuNo 151974, approaching the {{USS|Coral Sea|CV-43}} in July 1968.
 |alt=White variable geometry-wing jet aircraft landing on carrier
}}{{Infobox aircraft type
 |type= [[interceptor aircraft|Interceptor]]
 |national origin= [[United States]]
 |manufacturer= [[General Dynamics]] and [[Grumman]]
 |designer=
 |first flight= 18 May 1965
 |introduced=
 |retired=
 |primary user= [[United States Navy]]
 |more users=
 |number built= 7
 |status=
 |unit cost= [[United States dollar|US$]]8 million<ref>[http://www.time.com/time/magazine/article/0,9171,840925,00.html "Aircraft: Takeoff for the F-111"]. ''Time'', 19 May 1967.</ref>
 |developed from= [[General Dynamics F-111 Aardvark]]
 |variants with their own articles =
}}
|}

The '''General Dynamics/Grumman F-111B''' was a long-range [[carrier-based aircraft|carrier-based]] [[interceptor aircraft]] that was planned to be a follow-on to the [[McDonnell Douglas F-4 Phantom II|F-4 Phantom II]]. The F-111B was developed in the 1960s by [[General Dynamics]] in conjunction with [[Grumman Corporation|Grumman]] for the [[United States Navy]] (USN) as part of the joint Tactical Fighter Experimental (TFX) with the [[United States Air Force]] (USAF) to produce a common fighter for the services that could perform a variety of missions. It incorporated innovations such as variable-geometry wings, afterburning turbofan engines, and a long-range radar and missile weapons system.

Designed in parallel with the [[General Dynamics F-111 Aardvark|F-111 "Aardvark"]], which was adopted by the Air Force as a strike aircraft, the F-111B suffered development issues and changing Navy requirements for an aircraft with maneuverability for [[dogfight]]ing. The F-111B was not ordered into production and the F-111B prototypes were used for testing before being retired. The F-111B would be replaced by the smaller and lighter [[Grumman F-14 Tomcat]], which carried over the engines, [[AN/AWG-9|AWG-9]]/[[AIM-54 Phoenix|Phoenix weapons system]], and similar swing-wing configuration.

==Development==

===Background===
{{main|General Dynamics F-111 Aardvark#Development}}

The F-111B was part of the 1960s TFX program. The USAF's [[Tactical Air Command]] (TAC) was largely concerned with the [[fighter-bomber]] and [[Interdictor|deep strike/interdiction]] roles. The aircraft would be a follow-on to the [[Republic F-105 Thunderchief|F-105 Thunderchief]]. In June 1960, the USAF issued a specification for a long-range interdiction/strike aircraft able to penetrate [[Soviet Union|Soviet]] air defenses at very low altitudes and very high speeds to deliver tactical nuclear weapons against crucial targets.<ref name=Gunston_p12-3>Gunston 1978, pp. 12–13.</ref> Meanwhile, the U.S. Navy sought a long-range, high-endurance [[interceptor aircraft|interceptor]] to defend its [[aircraft carrier]] battle groups against long-range [[anti-ship missile]]s launched from [[Soviet Union|Soviet]] jet bombers ([[Tupolev Tu-16]], [[Tupolev Tu-22]], later [[Tupolev Tu-22M]]) and submarines. The Navy needed a Fleet Air Defense (FAD) aircraft with a more powerful radar, and longer range missiles than the [[McDonnell Douglas F-4 Phantom II|F-4 Phantom II]] to intercept both enemy bombers and missiles.<ref>Thomason 1998, pp. 3–5.</ref>

===Tactical Fighter Experimental (TFX)===

The Air Force and Navy requirements appeared to be different. However, on 14 February 1961, the new U.S. Secretary of Defense, [[Robert McNamara]], formally directed that the services study the development of a single aircraft that would satisfy both requirements. Early studies indicated the best option was to base the Tactical Fighter Experimental (TFX) on the Air Force requirement and a modified version for the Navy.<ref name=Gunston_p8-15>Gunston 1978, pp. 8, 10–15.</ref> In June 1961, Secretary McNamara ordered the go ahead on TFX despite Air Force and the Navy efforts to keep their programs separate.<ref name=Eden_p196-7>Eden 2004, pp. 196–197.</ref> The USAF and the Navy could only agree on swing-wing, two seat, twin engine design features. The USAF wanted a tandem seat aircraft for low level penetration, while the Navy wanted a shorter, high altitude interceptor with side by side seating.<ref name=Gunston_p8-15/> Also, the USAF wanted the aircraft designed for 7.33&nbsp;g with Mach&nbsp;2.5 speed at altitude and Mach&nbsp;1.2 speed at low level with a length of approximately 70&nbsp;ft (21.3 m). The Navy had less strenuous requirements of 6&nbsp;g with Mach&nbsp;2 speed at altitude and high subsonic speed (approx. Mach 0.9) at low level with a length of {{convert|56|ft|m|sigfig=3|abbr=on}}.<ref name=Gunston_p8-15/><ref name=Miller_p11-5>Miller 1982, pp. 11–15.</ref> The Navy also wanted a 48-inch (122&nbsp;cm) radar dish for long range and a maximum takeoff weight of 50,000&nbsp;lb (22,700&nbsp;kg).<ref name=Gunston_p16-7>Gunston 1978, pp. 16–17.</ref> So McNamara developed a basic set of requirements for TFX based largely on the Air Force's requirements. He changed to a 36-inch (91.4&nbsp;cm) dish for compatibility and increased the maximum weight to approximately {{convert|60000|lb|kg|sigfig=3|abbr=on}} for the Air Force version and {{convert|55000|lb|kg|sigfig=3|abbr=on}} for the Navy version. Then on 1&nbsp;September&nbsp;1961 he ordered the USAF to develop it.<ref name=Miller_p11-5/><ref name=Gunston_p16-7/>

A [[request for proposal]] (RFP) for the TFX was provided to industry in October 1961. In December of that year [[Boeing]], [[General Dynamics]], [[Lockheed Corporation|Lockheed]], [[McDonnell Aircraft|McDonnell]], [[North American Aviation|North American]] and [[Republic Aviation|Republic]] submitted their proposals. The proposal evaluation group found all the proposals lacking, but the best should be improved with study contracts. Boeing and General Dynamics were selected to enhance their designs. Three rounds of updates to the proposals were conducted with Boeing being picked by the selection board. Instead Secretary McNamara selected General Dynamics' proposal in November 1962 due to its greater commonality between Air Force and Navy TFX versions. The Boeing aircraft versions shared less than half of the major structural components. General Dynamics signed the TFX contract in December 1962. A Congressional investigation followed, but did not change the selection.<ref name=Gunston_p18-20>Gunston 1978, pp. 18–20.</ref>

===Design phase===
[[File:F-111B NAN7-65.jpg|thumb|F-111B, BuNo 151970 in flight over Long Island, New York in 1965|alt=Black-and-white photo of jet aircraft flying above scattered clouds with wings swept back.]]

The Air Force F-111A and Navy F-111B variants used the same airframe structural components and TF30-P-1 turbofan engines. They featured side by side crew seating in an escape capsule, as required by the Navy. The F-111B's nose was {{convert|8.5|ft|m|sigfig=3|sp=us}} shorter due to its need to fit on existing carrier elevator decks, and had {{convert|3.5|ft|m|sigfig=3|sp=us}} longer wingtips to improve on-station endurance time. The Navy version would carry an [[AN/AWG-9]] [[Pulse-Doppler radar]] and six [[AIM-54 Phoenix]] missiles. The Air Force version would carry the AN/APQ-113 attack radar and the AN/APQ-110 terrain-following radar and air-to-ground armament.<ref name=Baug_F-111A>Baugher, Joe. [http://www.joebaugher.com/usaf_fighters/f111_1.html "General Dynamics F-111A."] ''General Dynamics F-111 Aardvark,'' 23 December 1999.</ref>

Lacking experience with carrier-based fighters, General Dynamics teamed with [[Grumman]] for assembly and test of the F-111B aircraft. In addition, Grumman would also build the F-111A's aft fuselage and the landing gear. The first test F-111A was powered by YTF30-P-1 turbofans and used a set of ejector seats as the escape capsule was not yet available.<ref name=Baug_F-111A/> It first flew on 21 December 1964.<ref name=Eden_p197>Eden 2004, p. 197.</ref> The first F-111B was also equipped with ejector seats and first flew on 18 May 1965.<ref name=Grumman_F-111B>Baugher, Joe. [http://www.joebaugher.com/usaf_fighters/f111_4.html "General Dynamics/Grumman F-111B"] ''General Dynamics F-111 Aardvark,'' 7 November 2004.</ref> To address stall issues in certain parts of the flight regime, the F-111's engine inlet design was modified in 1965–66, ending with the "Triple Plow I" and "Triple Plow II" designs.<ref name=Gunston_p25-7>Gunston 1978, pp. 25–27.</ref> The F-111A achieved a speed of Mach&nbsp;1.3 in February 1965 with an interim intake design.<ref name=Baug_F-111A/><ref name=Gunston_p25-7/>

===F-111B===
[[File:F-111B fighters over Long Island c1965.jpg|thumb|left|F-111Bs, BuNo 151970 and 151971, over Long Island during testing]]

The weight goals for both F-111 versions proved to be overly optimistic.<ref name=Miller_p52>Miller 1982, p. 52.</ref> Excessive weight plagued the F-111B throughout its development. The prototypes were far over the requirement weight. Design efforts reduced airframe weight but were offset by the addition of the escape capsule. The additional weight made the aircraft underpowered. Lift was improved by changes to the wing control surfaces. A higher thrust version of the engine was planned.<ref>Thomason 1998, p. 43.</ref> During the congressional hearings for the aircraft, Vice Admiral Thomas F. Connolly, then Deputy Chief of Naval Operations for Air Warfare, responded to a question from Senator [[John C. Stennis]] as to whether a more powerful engine would cure the aircraft's woes, saying, "There isn't enough power in all [[Christendom]] to make that airplane what we want!"<ref>[http://www.time.com/time/magazine/article/0,9171,828470,00.html "Tests & Testimony."] ''Time magazine'', 22 March 1968.</ref>

With the F-111B program in distress, Grumman began studying improvements and alternatives. In 1966, the Navy awarded Grumman a contract to begin studying advanced fighter designs. Grumman narrowed down these designs to its Model 303 design.<ref>Spick 2000, pp. 71–72.</ref> With this the F-111B's end appeared near by mid-1967.<ref name=Miller_p54>Miller 1982, p. 54.</ref> By May 1968 both Armed Services committees of Congress voted not to fund production and in July 1968 the DoD ordered work stopped on F-111B.<ref name=Gunston_p35>Gunston 1978, p. 35.</ref> A total of seven F-111Bs were delivered by February 1969.<ref name=Logan_pp254-5>Logan 1998, pp. 254–255.</ref>

===Replacement===
[[File:F-14A Tomcat of VF-84 in flight in 1986.jpg|thumb|The F-14 that Grumman proposed as a replacement for the F-111B, was designed around the same engine/radar/missile combination.]]

The F-111B's replacement, the [[Grumman F-14 Tomcat]], which derived from Grumman's initial Model 303 design, reused the [[Pratt & Whitney TF30|TF30]] engines from the F-111B, though the Navy planned on replacing them with an improved engine later.<ref>Spick 2000, pp. 72–74, 112.</ref> Although lighter than the F-111B, it was still the largest and heaviest U.S. fighter to takeoff and land from an aircraft carrier.<ref>Gunston and Spick 1983, p. 112.</ref> Its size was a consequence of the requirement to carry the large AWG-9 radar and AIM-54 Phoenix missiles, both from the F-111B, while exceeding the F-4's maneuverability.<ref>Thomason 1998, p. 54.</ref> While the F-111B was armed only for the interceptor role, the Tomcat incorporated an internal [[M61 Vulcan]] cannon, provisions for Sidewinder and Sparrow air-to air missiles, and provisions for bombs.<ref>[http://www.globalaircraft.org/planes/f-14_tomcat.pl "F-14 Tomcat."] ''GlobalAircraft.org.'' Retrieved: 15 November 2010.</ref><ref>Colucci, Frank. [http://www.hobbyfanatics.com/index.php?/topic/153-building-the-bombcat-the-dml-f-14d-in-1144th-scale/ "Building the Bombcat."] "hobbyfanatics.com,'' 31 July 2003. Retrieved: 15 November 2010.</ref> While the F-111B did not reach service, land-based F-111 variants were in service with the [[U.S. Air Force]] for many years and with the [[Royal Australian Air Force]] until 2010.

==Design==

The F-111B was an all-weather interceptor aircraft intended to defend U.S. Navy carrier battle groups against bombers and anti-ship missiles.<ref name=Thomason_p15-6>Thomason 1998, pp. 15–16.</ref> The F-111 features [[variable-sweep wing|variable geometry wings]], an internal weapons bay and a cockpit with side by side seating. The cockpit is part of an [[escape crew capsule]].<ref name=Eden_p196-201>Eden 2004, pp. 196–201.</ref> The wing sweep varies between 16 degrees and 72.5 degrees (full forward to full sweep).<ref name=Miller_p80>Miller 1982, p. 80.</ref> The airframe consisted mostly of aluminum alloys with steel, titanium and other materials also used.<ref name=Logan_p17-8>Logan 1998, pp. 17–18.</ref> The fuselage is a semi-monocoque structure with stiffened panels and [[honeycomb structures|honeycomb sandwich]] panels for skin.<ref name=Miller_p80/><ref name=Logan_p17-8/> The F-111B was powered by two [[Pratt & Whitney TF30]] afterburning turbofan engines and included the [[AN/AWG-9]] radar system for controlling the [[AIM-54 Phoenix]] air-to-air missiles.<ref name=Logan_p254-7>Logan 1998, pp. 254–257.</ref> Poor visibility over the nose made the aircraft more difficult to handle for carrier operations.<ref name="mc_f14designevolution">{{cite web|last1=Ciminera|first1=Mike|title=F-14 Design Evolution|url=https://www.youtube.com/watch?v=SsUCixAeZ0A#t=550.526632|website=Youtube - Peninsula Srs Videos|publisher=Youtube|accessdate=30 October 2016}}</ref>

The F-111 offered a platform with the range, payload, and Mach-2 performance to intercept targets quickly, but with swing wings and turbofan engines, it could also loiter on station for long periods. The F-111B would carry six AIM-54 Phoenix missiles, its main armament. Four of the Phoenix missiles mounted on wing pylons and two in the weapons bay.<ref name=Thomason_p15-6/> The missile pylons added significant drag when used.<ref name="mc_f14designevolution"/>

==Operational history==
[[File:F-111B CVA-43 launch July1968.jpg|thumb|F-111B, BuNo 151974, being launched from USS ''Coral Sea'' in July 1968. It was the only F-111B to perform carrier operational trials.]]

===Flight testing===
Flight tests on the F-111B continued at Point Mugu and China Lake even after the program had been terminated.<ref name=Grumman_F-111B/> In July 1968 the pre-production F-111B serial number 151974 was used for carrier trials aboard [[USS Coral Sea (CV-43)|USS ''Coral Sea'']]. The evaluation was completed without issue.<ref name=Thomason_p53>Thomason 1998, p. 53.</ref>

Hughes continued Phoenix missile system development with four F-111Bs.<ref name=Thomason_p53/> In all two F-111Bs were lost in crashes and a third seriously damaged.<ref name=Logan_pp254-5/>  The F-111B's last flight was with ''151792'' from California to New Jersey in mid-1971. The seven F-111Bs flew 1,748 hours over 1,173 flights.<ref name=Thomason_p53>Thomason 1998, p. 53.</ref>

==Variants==
F-111B numbers 1 to 3 were initial prototypes; and No. 4 and 5 were prototypes with lightened airframes.<ref name=Miller_p52-5/> No. 6 and 7 had lightened airframes and improved TF30-P-12 engines and were built to near production standard.<ref name=Miller_p52-5>Miller 1982, pp. 52–55.</ref> These were also approximately {{convert|2|ft|m|abbv=off|sigfig=1}} longer due to an added section between the cockpit and radome.<ref>Logan 1998, pp. 254–256.</ref> The first five aircraft included Triple Plow I intakes. The last two had Triple Plow II intakes.<ref>Logan 1998, p. 254.</ref> The first three B-models were fitted with ejection seats and the remainder included the escape crew capsule.<ref name=Thomason_p16>Thomason 1998, p. 16.</ref>

{| class="wikitable" style="text-align:center;"
|+ '''List of F-111Bs'''<ref name=Miller_p52-5/><ref name=Thomason_p16>Thomason 1998, pp. 20–26, 33, 42, 44, 46.</ref><ref>Logan 1998, pp. 258–260.</ref>
|-
!Number !! Serial number !! Description !! Location or fate
|-
| 1 || 151970 || style="text-align:left;"| Prototype with heavy airframe, TF30-P-3 engines || style="text-align:left;"| After flight test use was scrapped in December 1969.
|-
| 2 || 151971 || style="text-align:left;"| Prototype with heavy airframe, TF30-P-3 engines || style="text-align:left;"| Used for Hughes missile testing. Lost in a crash on 11 September 1968.
|-
| 3 || 151972 || style="text-align:left;"| Prototype with heavy airframe, TF30-P-3 engines || style="text-align:left;"| Was damaged and retired. Was used for jet blast testing at NATF, [[NAES Lakehurst]], NJ and was probably scrapped there.
|-
| 4 || 151973 || style="text-align:left;"| Prototype with lightened airframe, TF30-P-3 engines || style="text-align:left;"| Destroyed in double engine failure crash on 21 April 1967.
|-
| 5 || 151974 || style="text-align:left;"| Prototype with lightened airframe, TF30-P-3 engines || style="text-align:left;"| Crash landed at [[NAS Point Mugu]], CA in October 1968. Was dismantled at [[NAS Moffett Field]], CA in 1970.
|-
| 6 || 152714 || style="text-align:left;"| Pre-production version, TF30-P-12 engines || style="text-align:left;"| Used for Hughes missile tests. Retired in 1969. Removed from inventory in 1971 and used for parts. Was photographed in 2008 in a [[Mojave, California]] scrapyard.<ref>[http://www.airliners.net/photo/USA---Navy/General-Dynamics-F-111B/1419009/L/ Photograph of a F-111B in a scrapyard near Mojave port].</ref>
|-
| 7 || 152715 || style="text-align:left;"| Pre-production version, TF30-P-12 engines || style="text-align:left;"| Retired and stored at [[NAWS China Lake]], CA (awaiting restoration).<ref>U.S. Naval Museum of Armament & Technology</ref>
|}

[[File:F-111B CVA-43 July1968.jpg|thumb|F-111B, BuNo 151974, on the USS ''Coral Sea'' in July 1968. It crash landed at NAS Point Mugu, California on 11 October 1968 and was subsequently scrapped.|alt= An F-111B on the deck of an aircraft carrier being towed.]]

==Operators==
;{{USA}}
* [[United States Navy]]

==Specifications (F-111B pre-production)==
[[File:Gerneral Dynamics F-111B 3-view drawing.png|right|400px|alt= A line drawing of the F-11B showing front, top, and side view.]]
[[File:F-111B 151974 Moffett Field wind tunnel.jpg|thumb|F-111B, BuNo 151974, at NAS Moffett Field, California during full-scale wind tunnel flight control tests|alt= A F-111B on support inside a large wind tunnel]]
[[File:GD F-111B 152714 USN DM 22.04.71 edited-3.jpg|thumb|right|The first pre-production F-111B ''152714'' in storage at [[Davis Monthan Air Force Base|Davis Monthan AFB]] in 1971]]

For pre-production aircraft #6 & #7:
{{Aircraft specifications
<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]].
     Please answer the following questions: -->
|plane or copter?=plane
|jet or prop?=jet

<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item does not apply, like capacity, leave it blank. For additional lines, end your alt units with a right parenthesis ")" and start a new, fully-formatted line beginning with an asterisk "*" 
-->
|ref=Thomason,<ref>Thomason 1998, pp. 55–56.</ref> Miller,<ref name=Miller_p66_80>Miller 1982, pp. 66, 80.</ref> Logan<ref>Logan 1998, pp. 302–303.</ref>
|crew=2 (pilot and weapons system operator)
|length main=68 ft 10 in
|length alt=20.98 m
|span main=<br/>
** '''Spread:''' 70 ft (21.3 m)
** '''Swept:''' 33 ft 11 in
|span alt=10.34 m
|height main=15 ft 9 in
|height alt=4.80 m
|area main=<br/>
** '''Spread:''' 655.5 ft² (60.9 m²)
** '''Swept:''' 550 ft²
|area alt= 51.1 m²
|airfoil=[[NACA airfoil|NACA 64-210.68]] root, NACA 64-209.80 tip
|empty weight main=46,100 [[pound (mass)|lb]]
|empty weight alt=20,910 kg
|loaded weight main=79,000 lb
|loaded weight alt=35,800 kg
|max takeoff weight main=88,000 lb
|max takeoff weight alt=39,900 kg
|more general=

|engine (jet)=[[Pratt & Whitney TF30|Pratt & Whitney TF30-P-3]]
|type of jet=[[turbofan]]s
|number of jets=2
|thrust main=10,750 [[pound-force|lbf]]
|thrust alt=47.8 kN
|afterburning thrust main=18,500 lbf
|afterburning thrust alt=82.3 kN

|max speed main=[[mach number|Mach]] 2.2
|max speed alt=1,450 mph, 2,330 km/h
|max speed more=
|cruise speed main=
|cruise speed alt=
|cruise speed more=
|range main=2,100 mi
|range alt=1,830 nmi, 3,390 km
|range more=; with 6 AIM-54 missiles and 23,000 lb fuel internal
|combat radius main=<!-- 1,330 [[statute mile|mi]] -->
|combat radius alt=<!-- 1,160 nmi, 2,140 km -->
|ferry range main=3,200 mi
|ferry range alt=2,780 nmi, 5,150 km
|ferry range more=; with 2 x 450 gal external tanks
|ceiling main=65,000 ft
|ceiling alt=19,800 m
|climb rate main=21,300 ft/min
|climb rate alt=108 m/s
|loading main=<br/>
** '''Spread:''' 120 lb/ft² (586 kg/m²)
** '''Swept:''' 144 lb/ft²
|loading alt=703 kg/m²
|thrust/weight=0.47
|more performance=

|guns=1× [[M61 Vulcan]] 20 mm (0.787 in) [[gatling gun|Gatling cannon]] (seldom fitted)
|hardpoints= 6 underwing pylons for [[Aircraft ordnance|ordnance]] and external fuel tanks
|hardpoint capacity=<!-- 31,500 lb (14,300 kg) -->
|bombs=
|rockets=
|missiles= 6 x [[AIM-54 Phoenix]] long range air-air missiles (four under wings, two in weapons bay)
|avionics=*[[AN/AWG-9]] [[Pulse-Doppler radar]]
}}

== See also ==
{{Commons category|General Dynamics F-111B}}
{{Portal|United States Navy|Aviation}}
{{Aircontent
|see also=
|related=
* [[General Dynamics F-111 Aardvark]]
* [[General Dynamics EF-111A Raven]]
* [[General Dynamics F-111C]]
* [[General Dynamics F-111K]]
|similar aircraft=
* [[AFVG]]
* [[Avro Canada CF-105 Arrow]]
* [[Dassault Mirage G]]
* [[Grumman F-14 Tomcat]]
* [[Lavochkin La-250]]
* [[Lockheed YF-12]]
* [[McDonnell F-101 Voodoo|McDonnell F-101B]]
* [[McDonnell Douglas F-4 Phantom II]]
* [[North American A-5 Vigilante|North American NR-349]]
* ''[[Operational Requirement F.155]]'' (UK)
* [[Tupolev Tu-28]]
* [[Yakovlev Yak-28P]]
|lists=
* [[List of bomber aircraft]]
* [[List of military aircraft of the United States]]
}}

==References==
;Notes
{{Reflist|2}}

;Bibliography
{{refbegin}}
* Eden, Paul, ed. "General Dynamics F-111 Aardvark/EF-111 Raven". ''Encyclopedia of Modern Military Aircraft''. London: Amber Books, 2004. ISBN 1-904687-84-9.
* [[Bill Gunston|Gunston, Bill]]. ''F-111''. New York: Charles Scribner's Sons, 1978. ISBN 0-684-15753-5.
* Logan, Don. ''General Dynamics F-111 Aardvark''. Atglen, PA: Schiffer Military History, 1998. ISBN 0-7643-0587-5.
* Miller, Jay. ''General Dynamics F-111 "Arardvark"''. Fallbrook, California: Aero Publishers, 1982. ISBN 0-8168-0606-3.
* Neubeck, Ken. ''F-111 Aardvark Walk Around''. Carrollton, Texas: Squadron/Signal Publications, 2009. ISBN 978-0-89747-581-5
* Thornborough, Anthony M. ''F-111 Aardvark''. London: Arms and Armour, 1989. ISBN 0-85368-935-0.
* Thornborough, Anthony M. and Peter E. Davies. ''F-111 Success in Action''. London: Arms and Armour Press Ltd., 1989. ISBN 0-85368-988-1.
* Wilson, Stewart. ''Combat Aircraft since 1945''. Fyshwick, Australia: Aerospace Publications, 2000. ISBN 1-875671-50-1.
* Winchester, Jim, ed. "General Dynamics FB-111A". "Grumman/General Dynamics EF-111A Raven". ''Military Aircraft of the Cold War'' (The Aviation Factfile). London: Grange Books plc, 2006. ISBN 1-84013-929-3.
{{refend}}

==External links==
{{Commons category|General Dynamics F-111B}}
* [http://www.f-111.net/ F-111.net]
* [http://www.globalsecurity.org/military/systems/aircraft/f-111.htm F-111 page on GlobalSecurity.org]
* [http://www.aero-web.org/locator/manufact/genedyna/f-111.htm Aviation Enthusiast's List of F-111 Survivors]
* [http://www.usscoralsea.net/pages/f111.php F-111B page on usscoralsea.net]

{{Grumman aircraft}}
{{Convair/GD aircraft}}
{{USAF fighters}}

{{DEFAULTSORT:General Dynamics-Grumman F-111B}}
[[Category:General Dynamics aircraft|F-111B]]
[[Category:Grumman aircraft|F-111B]]
[[Category:United States fighter aircraft 1960–1969]]
[[Category:Twinjets]]
[[Category:High-wing aircraft]]
[[Category:Variable-sweep-wing aircraft]]
[[Category:Carrier-based aircraft]]
[[Category:Cancelled military aircraft projects of the United States]]