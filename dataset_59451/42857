__NOTOC__
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=Skyfarer
 |image=General Aircraft Corp. Skyfarer.jpg
 |caption=
}}{{Infobox Aircraft Type
 |type=Two-seat cabin monoplane
 |manufacturer=[[General Aircraft Corporation|General Aircraft]]
 |designer=[[Otto C. Koppen]]
 |first flight=1940s
 |introduced=
 |retired=
 |status=
 |primary user=
 |more users=
 |produced=
 |number built=18
 |variants with their own articles=
}}
|}

The '''General Aircraft G1-80 Skyfarer''' was a 1940s [[United States|American]] two-seat cabin [[monoplane]] aircraft built by the [[General Aircraft Corporation]] of [[Lowell, Massachusetts]].

==Development==
The General Aircraft Corporation was established to build an aircraft designed by Doctor [[Otto C. Koppen]] from the [[Massachusetts Institute of Technology]]. The aircraft was the '''G1-80 Skyfarer''', a two-seat cabin high-wing braced monoplane with a light alloy basic structure and a mixed steel [[Tube-and-fabric construction|tube and fabric covering]]. It had an unusual tail unit, a cantilever tailplane with the elevator mounted on the upper surface of the tail with aluminum endplate fins and no movable rudders.
It was powered by a 75&nbsp;hp (56&nbsp;kW) [[Avco Lycoming GO-145-C2]] geared air-cooled four-cylinder engine.<ref>[http://www:aerofoilengineering.com/KR/sportaviation/Kr180-4.HTM Engine description] {{webarchive |url=https://web.archive.org/web/20060326000000/http://www |date=March 26, 2006 }}</ref>

The aircraft incorporated aerodynamic control principles covered by patents issued to [[Fred Weick]], an early aeronautical engineer who went on to design and market the [[Ercoupe]].  Since it had no [[rudder]]s (or rudder pedals), it was simpler to fly (it had a single control wheel, which controlled the [[aileron]]s and [[Elevator (aircraft)|elevator)]], and was considered [[Spin (flight)|spin-proof]].  The aircraft was certified in 1941 with a placard that stated that the aircraft was characteristically incapable of spinning.<ref>{{cite news|newspaper=The Washington Post|title=Auto Driver Can Solo New Plane In Two Hours|date=1 June 1941|author=C.B. Allen}}</ref> The company also made a comment to Popular Science in a [https://books.google.com/books?id=9iYDAAAAMBAJ&pg=PA80&dq=popular+science+%22September+1941%22&hl=en&ei=gZKRTKi3N82nnAeDrsDGCA&sa=X&oi=book_result&ct=result&resnum=8&ved=0CEgQ6AEwBw#v=onepage&q=popular%20science%20%22September%201941%22&f=true September 1941 article, with first public photos] that an "average" person could learn to fly the Skyfarer ''...in an hour or so..''.

It was anticipated that many aircraft would be ordered and built, but the United States became involved in the [[World War II|Second World War]] and the Skyfarer program was abandoned after 17 examples had been built. The rights and tooling passed to Grand Rapids Industries, who built two aircraft before stopping production.<ref>[http://www.bassace.com/ercoupe/ercoupe_history.htm Ercoupe history website]</ref> The company became a manufacturer of the [[CG-4|Waco CG-4A]] troop glider.

==Variants==
L.W. DuVon and Dr. David O. Kime of  [[Westmar University|Western Union College]] convinced the type holder Grand Rapids Industries, to give the equipment, tools and one of the finished planes to the college. They then found local investors who formed Mars Corporation in 1945. The aircraft was later licensed as the Mars M1-80 Skycoupe with a 100&nbsp;hp engine. One example was built and production plans were estimated to be as high as 75 planes in its first year. The glut of aircraft produced after the war left little market for the aircraft. The facility to manufacture the aircraft was sold by 1946.<ref>{{cite news|newspaper=Le Mars daily Sentinel|title=Skycoupe returns to Le Mars|date=September 10, 2004|author=Sue Morris}}</ref>

The aircraft NC29030 resides in the [[Plymouth County, Iowa]] Historical Museum<ref>{{cite news|newspaper= Le Mars daily Sentinel|title=Mars Skycoupe makes final stop|date=November 23, 2004|author=Beverly Van Buskirk}}</ref><ref>{{cite book|title=U.S. Civil Aircraft Series| volume= 8|author=Joseph P. Juptner}}</ref>

==Specifications (G1-80 Skyfarer) ==
{{aerospecs
|ref=
|met or eng?=<!-- eng for US/UK aircraft, met for all others -->eng
|crew=one pilot
|capacity=one passenger, seated side-by-side with pilot
|length m=6.71
|length ft=22
|length in=0
|span m=9.58
|span ft=31
|span in=5
|height m=2.64
|height ft=8
|height in=8
|wing area sqm=11.27
|wing area sqft=121.3
|empty weight kg=404
|empty weight lb=890
|gross weight kg=612
|gross weight lb=1350
|eng1 number=1
|eng1 type=[[Avco Lycoming GO-145|Avco Lycoming GO-145-C2]] flat-four piston engine
|eng1 kw=56
|eng1 hp=75
|eng2 number=
|max speed kmh=
|max speed mph=144
|cruise speed kmh=161<!-- if max speed unknown -->
|cruise speed mph=100<!-- if max speed unknown -->
|range km=563
|range miles=350
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=3050
|ceiling ft=10,000
|climb rate ms=
|climb rate ftmin=
}}

==References==
* The Illustrated Encyclopedia of Aircraft (Part Work 1982-1985), 1985, Orbis Publishing
{{reflist}}

==External links==
{{commons category|General Skyfarer}}
* [https://books.google.com/books?id=htkDAAAAMBAJ&pg=PA16&dq=popular+mechanic+antitank+1941&hl=en&ei=wuSaTJmOHMGYnAf8vJH0Dg&sa=X&oi=book_result&ct=result&resnum=2&ved=0CDcQ6AEwAQ#v=onepage&q&f=true ''No Spin Plane Easy For Auto Drivers To Fly'', October 1941] early article on Skyfarer
* [http://www.lemarssentinel.com/story/1076105.html ''Skycoupe Returns To LeMars''] one of the only known Skyfarers today
* [http://1000aircraftphotos.com/Contributions/Shumaker/3027.htm "No. 3027. General Aircraft G1-80 Skyfarer (NC29030 c/n 17)"] ''Dan Shumaker Collection''

[[Category:United States civil utility aircraft 1940–1949]]
[[Category:High-wing aircraft]]
[[Category:General Aircraft Corporation aircraft]]
[[Category:Single-engined tractor aircraft]]