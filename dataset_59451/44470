{{Other uses|Bleak House (disambiguation)}}
{{Use British English|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{infobox book | 
| name          = Bleak House
| title_orig    =
| translator    =
| image         = Bleakhouse serial cover.jpg
| image_size    = 200px
| caption = Cover of first serial, March 1852
----
<small>Illustration from the New York Public Library Berg Collection</small>
| author        = [[Charles Dickens]]
| illustrator   = [[Hablot Knight Browne]] ([[Phiz]])
| cover_artist  = [[Hablot Knight Browne]] ([[Phiz]])
| country       = England
| language      = English
| series        = 
| genre         = [[Novel]]
| publisher     = [[Bradbury & Evans]]
| published  = Serialised 1852–3; book form 1853
| english_release_date =
| media_type    =
| pages         =
| isbn          =
| preceded_by   =[[David Copperfield]] (1849–50)
| followed_by   = [[A Child's History of England]] (1852-–4)
}}

'''''Bleak House''''' is one of [[Charles Dickens]]'s major novels, first published as a [[Serial (literature)|serial]] between March 1852 and September 1853. The novel has many characters and several sub-plots, and the story is told partly by the novel's heroine, Esther Summerson, and partly by an [[omniscient narrator]]. At the centre of ''Bleak House'' is the long-running legal case, [[Jarndyce and Jarndyce]], which came about because someone wrote several conflicting wills. This legal case is used by Dickens to satirise the English judicial system, and he makes use of his earlier experiences as a law clerk, and as a litigant seeking to enforce copyright on his earlier books.''{{citation needed|date=February 2016}}

Though the legal profession criticised Dickens's satire as exaggerated, this novel helped support a judicial reform movement, which culminated in the enactment of [[Judicature Acts|legal reform]] in the 1870s.<ref>James Oldham, [http://scholarship.law.georgetown.edu/cgi/viewcontent.cgi?article=1591&context=facpub A Profusion of Chancery Reform], Law and History Review</ref>

There is some debate among scholars as to when ''Bleak House'' is set. The English legal historian Sir [[William Searle Holdsworth|William Holdsworth]] sets the action in 1827;<ref>
Holdsworth, William S. ''Charles Dickens as a Legal Historian''. Yale University Press, 1928.</ref> however, reference to preparation for the building of a [[railroad|railway]] in Chapter LV suggests the 1830s.

==Synopsis==

Sir Leicester Dedlock and his wife Lady Honoria live on his estate at Chesney Wold. Unknown to Sir Leicester, Lady Dedlock had a lover, Captain Hawdon, before she married – and had a daughter by him. Lady Dedlock believes her daughter is dead.<ref>{{Cite book|title = Bleak House|last = Dickens|first = Charles|publisher = The Penguin Group|year = 2003|isbn = 978-0-141-43972-3|location = New York|pages = 21}}</ref>

The daughter, Esther, is in fact alive, and being raised by Miss Barbary, Lady Dedlock's sister.  Esther does not know Miss Barbary is her aunt. After Miss Barbary dies, John Jarndyce becomes Esther's guardian and assigns the Chancery lawyer "Conversation" Kenge to take charge of her future.  After attending school for six years, Esther moves in with him at Bleak House.

[[File:Bleak House frontispiece2.jpg|upright|thumb|left|Bleak House]]

Jarndyce simultaneously assumes custody of two other wards, Richard Carstone and Ada Clare (who are both his and one another's distant cousins). They are beneficiaries in one of the wills at issue in ''[[Jarndyce and Jarndyce]]''; their guardian is a beneficiary under another will, and the two wills conflict. Richard and Ada soon fall in love, but though Mr Jarndyce does not oppose the match, he stipulates that Richard must first choose a profession. Richard first tries a career in medicine, and Esther meets Allan Woodcourt, a physician, at the house of Richard's tutor. When Richard mentions the prospect of gaining from the resolution of ''Jarndyce and Jarndyce'', John Jarndyce beseeches him never to put faith in what he calls "the family curse".

Meanwhile, Lady Dedlock is also a beneficiary under one of the wills. Early in the book, while listening to the reading of an affidavit by the family solicitor, Mr Tulkinghorn, she recognises the handwriting on the copy. The sight affects her so much she almost faints, which Tulkinghorn notices and investigates. He traces the copyist, a pauper known only as "Nemo," in London. Nemo has recently died, and the only person to identify him is a street-sweeper, a poor homeless boy named Jo, who lives in a particularly grim and poverty-stricken part of the city known as Tom-All-Alone's.  
[[File:Bleak House 10.jpg|upright|thumb|right|Consecrated ground]]

Lady Dedlock is also investigating, disguised as her maid, Mademoiselle Hortense. She pays Jo to take her to Nemo's grave. Meanwhile, Tulkinghorn is concerned Lady Dedlock's secret could threaten the interests of Sir Leicester, and watches her constantly, even enlisting her maid to spy on her. He also enlists Inspector Bucket to run Jo out of town, so that there are no loose ends that might connect Nemo to the Dedlocks.

Esther sees Lady Dedlock at church and talks with her later at Chesney Wold – though neither woman recognises their connection. Later, Lady Dedlock does discover that Esther is her child.  However Esther has become sick (possibly with [[smallpox]], since it permanently disfigures her) after nursing the homeless boy Jo.  Lady Dedlock waits until she has recovered before telling her the truth. Though Esther and Lady Dedlock are happy to be reunited, Lady Dedlock tells Esther they must never acknowledge their connection again.

On her recovery, Esther finds that Richard, having failed at several professions, has disobeyed his guardian and is trying to push ''Jarndyce and Jarndyce'' to conclusion in his and Ada's favour. In the process, Richard loses all his money and declines in health. He and Ada have secretly married, and Ada is pregnant. Esther has her own romance when Mr Woodcourt returns to England, having survived a shipwreck, and continues to seek her company despite her disfigurement. Unfortunately, Esther has already agreed to marry her guardian, John Jarndyce.

Hortense and Tulkinghorn discover the truth about Lady Dedlock's past. After a confrontation with Tulkinghorn, Lady Dedlock flees her home, leaving a note apologising for her conduct. Tulkinghorn dismisses Hortense, who is no longer of any use to him. Feeling abandoned and betrayed, Hortense kills Tulkinghorn and seeks to frame Lady Dedlock for his murder. Sir Leicester, discovering his lawyer's death and his wife's flight, suffers a catastrophic stroke, but he manages to communicate that he forgives his wife and wants her to return.

[[File:Bleak House 25.jpg|thumb|left|Attorney and Client]]

Inspector Bucket, who has previously investigated several matters related to ''Jarndyce and Jarndyce'', accepts Sir Leicester's commission to find Lady Dedlock. At first he suspects Lady Dedlock of the murder, but is able to clear her of suspicion after discovering Hortense's guilt, and asks Esther's help to find her.  Lady Dedlock has no way to know of her husband's forgiveness or that she has been cleared of suspicion, and she wanders the country in cold weather before dying at the cemetery of her former lover Captain Hawdon (Nemo). Esther and Bucket find her there.

Progress in ''Jarndyce and Jarndyce'' seem to take a turn for the better when a later will is found, which revokes all previous wills and leaves the bulk of the estate to Richard and Ada. Meanwhile, John Jarndyce cancels his engagement to Esther, who becomes engaged to Mr Woodcourt. They go to Chancery to find Richard. On their arrival, they learn that the case of ''Jarndyce and Jarndyce'' is finally over, but the costs of litigation have entirely consumed the estate. Richard collapses, and Mr Woodcourt diagnoses him as being in the last stages of [[tuberculosis]]. Richard apologises to John Jarndyce and dies.  Jarndyce takes in Ada and her child, a boy whom she names Richard. Esther and Woodcourt marry and live in a Yorkshire house which Jarndyce gives to them. The couple later raise two daughters.

Many of the novel's subplots focus on minor characters. One such subplot is the hard life and happy, though difficult, marriage of Caddy Jellyby and Prince Turveydrop. Another plot focuses on George Rouncewell's rediscovery of his family and his reunion with his mother and brother.

==Characters in ''Bleak House''==
As usual, Dickens drew upon many real people and places but imaginatively transformed them in his novel (see character list below for the supposed inspiration of individual characters).

Although not a character, the ''Jarndyce and Jarndyce'' case is a vital part of the novel.  It is believed to have been inspired by a number of real-life  Chancery cases involving wills, including those of [[Charles Day (boot blacking manufacturer)|Charles Day]] and [[William Jennens]],<ref>Dunstan, William. "The Real Jarndyce and Jarndyce." The Dickensian 93.441 (Spring 1997): 27.</ref> and of [[Charlotte Turner Smith|Charlotte Smith]]'s father-in-law Richard Smith.<ref>Jacqueline M. Labbe, ed. ''The Old Manor House'' by Charlotte Turner Smith, Peterborough, Ont.: Broadview Press, 2002 ISBN 978-1-55111-213-8, Introduction p. 17, note 3.</ref>

===Major characters===
* '''[[Esther Summerson]]'''  is the heroine. She is Dickens's only female narrator. Esther is raised as an orphan by Miss Barbary, (who is in fact her aunt). She does not know her parents' identity.  Miss Barbary holds macabre vigils on Esther's birthday each year, telling her that her birth is no cause for celebration, because the girl is her mother's "disgrace."<ref>{{Cite book|title = Bleak House|last = Dickens|first = Charles|publisher = The Penguin Group|year = 2003|isbn = 978-0-141-43972-3|location = New York|pages = 30}}</ref> Because of her cruel upbringing she is self-effacing, self-deprecating and grateful for every trifle. The discovery of her true identity provides much of the drama in the book. Finally it is revealed that she is the illegitimate daughter of Lady Dedlock and Nemo (Captain Hawdon).
* '''Honoria, Lady Dedlock'''  is the haughty mistress of Chesney Wold. The revelation of her past drives much of the plot. Before her marriage, Lady Dedlock had an affair with another man and bore his child. Lady Dedlock discovers the child's identity (Esther Summerson), and because she has revealed that she had a secret predating her marriage, she has attracted the noxious curiosity of Mr Tulkinghorn, who feels bound by his ties to his client, Sir Leicester, to pry out her secret. At the end of the novel, Lady Dedlock dies, disgraced in her own mind and convinced that her husband can never forgive her moral failings.
* '''John Jarndyce'''  is an unwilling party in ''Jarndyce and Jarndyce'', guardian of Richard, Ada, and Esther, and owner of Bleak House. [[Vladimir Nabokov]] called him "one of the best and kindest human beings ever described in a novel".<ref>Vladimir Nabokov, "Bleak House," ''Lectures on Literature''. New York: Harcourt Brace Jovanovich, 1980. p. 90.</ref> A wealthy man, he helps most of the other characters, motivated by a combination of goodness and guilt at the mischief and human misery caused by ''Jarndyce and Jarndyce'', which he calls "the family curse." At first, it seems possible that he is Esther's father, but he disavows this shortly after she comes to live under his roof. He falls in love with Esther and wishes to marry her, but gives her up because she is in love with Mr Woodcourt.
* '''Richard Carstone''' is a ward of Chancery in ''Jarndyce and Jarndyce''. Straightforward and likeable but irresponsible and inconstant, Richard falls under the spell of ''Jarndyce and Jarndyce''. At the end of the book, just after ''Jarndyce and Jarndyce'' is finally settled, he dies, tormented by his imprudence in trusting to the outcome of a Chancery suit.

[[File:Bleak House 02.jpg|upright|thumb|right|The little old lady]]
* '''Ada Clare'''  is another young ward of Chancery in ''Jarndyce and Jarndyce''. She falls in love with Richard Carstone, a distant cousin. They later marry in secret and she has Richard's child.
* '''Harold Skimpole''' is a friend of Jarndyce "in the habit of sponging his friends" (Nuttall). He is irresponsible, selfish, amoral, and without remorse. He often refers to himself as "a child" and claims not to understand human relationships, circumstances, and society – but actually understands them very well, as he demonstrates when he enlists Richard and Esther to pay off the bailiff who has arrested him on a writ of debt. He believes that Richard and Ada will be able to acquire credit based on their expectations in ''Jarndyce and Jarndyce'' and declares his intention to start "honoring" them by letting them pay some of his debts. This character is commonly regarded as a portrait of [[James Henry Leigh Hunt|Leigh Hunt]]. "Dickens wrote in a letter of 25 September 1853, 'I suppose he is the most exact portrait that was ever painted in words! ... It is an absolute reproduction of a real man.' A contemporary critic commented, 'I recognised Skimpole instantaneously; ... and so did every person whom I talked with about it who had ever had Leigh Hunt's acquaintance.'"<ref>Page, Norman, editor, ''Bleak House'', Penguin Books, 1971, p. 955 (note 2 to Chapter 6).</ref> [[G. K. Chesterton]] suggested that Dickens "may never once have had the unfriendly thought, 'Suppose Hunt behaved like a rascal!'; he may have only had the fanciful thought, 'Suppose a rascal behaved like Hunt!'".
* '''Lawrence Boythorn'''  is an old friend of John Jarndyce's; a former soldier who always speaks in superlatives; very loud and harsh, but goodhearted. Boythorn was once engaged to (and very much in love with) a woman who later left him without giving him any reason. That woman was in fact, Miss Barbary, who abandoned her former life (including Boythorn) when she took Esther from her sister. Boythorn is also a neighbour of Sir Leicester Dedlock's, with whom he is engaged in an epic tangle of lawsuits over a right-of-way across Boythorn's property that Sir Leicester asserts the legal right to close. He is thought to be based on the writer [[Walter Savage Landor]].{{Citation needed|date = February 2016}}
* '''Sir Leicester Dedlock''' is a crusty [[baronet]], very much older than his wife. Dedlock is an unthinking conservative who regards the ''Jarndyce and Jarndyce'' lawsuit as a mark of distinction worthy of a man of his family lineage. On the other hand, he is shown as a loving and devoted husband towards Lady Dedlock, even after he learns about her secret. 
* '''Mr Tulkinghorn'''  is Sir Leicester's lawyer. Scheming and manipulative, he seems to defer to his clients but relishes the power his control of their secrets gives him. He learns of Lady Dedlock's past and tries to control her conduct, to preserve the reputation and good name of Sir Leicester. He is murdered, and his murder gives Dickens the chance to weave a detective plot into the closing chapters of the book.
* '''Mr Snagsby''' is the timid and hen-pecked proprietor of a law-stationery business who gets involved with Tulkinghorn and Bucket's secrets. He is Jo's only friend. He tends to give half-crowns to those he feels sorry for.
* '''Miss Flite''' is an elderly eccentric. Her family has been destroyed by a long-running Chancery case similar to ''Jarndyce and Jarndyce'', and her obsessive fascination with Chancery veers between comedy and tragedy. She owns a large number of little birds which she says will be released "on the day of judgement."{{Citation needed|date = February 2016|reason = chapter?}}
* '''William Guppy''' is a law clerk at Kenge and Carboy. He becomes smitten with Esther and makes an offer of marriage (which she refuses). Later, after Esther learns that Lady Dedlock is her mother, she asks to meet Mr Guppy to tell him to stop investigating her past. He fears the meeting is to accept his offer of marriage (which he does not want to pursue now she is disfigured). He is so overcome with relief when she explains her true purpose that he agrees to do everything in his power to protect her privacy in the future.
* '''Inspector Bucket''' is a detective who undertakes several investigations throughout the novel, most notably the investigation of the murder of Mr Tulkinghorn. He is notable in being one of the first detectives in English fiction.<ref name="DET">Roseman, Mill ''et al.'' ''Detectionary''. New York: Overlook Press, 1971. ISBN 0-87951-041-2</ref> This character is probably based on Inspector [[Charles Frederick Field]] of the then recently formed Detective Department at [[Scotland Yard]].<ref>[http://www.ric.edu/rpotter/ Site of Dr Russell Potter, Rhode Island College] Biography of Inspector Field</ref> Dickens wrote several journalistic pieces about the Inspector and the work of the detectives in ''[[Household Words]]''.
* '''Mr George''' is a former soldier (having served under Nemo) who owns a London shooting-gallery and is a trainer in sword and pistol. The prime suspect in the murder of Mr Tulkinghorn, he is exonerated and his true identity is revealed, against his wishes. He is George Rouncewell, son of the Dedlocks' housekeeper, Mrs Rouncewell, who welcomes him back to Chesney Wold. He ends the book as body-servant to the stricken Sir Leicester Dedlock.
* '''Caddy Jellyby''' is a friend of Esther's, secretary to her mother. Caddy feels ashamed of her own "lack of manners," but Esther's friendship heartens her. Caddy falls in love with Prince Turveydrop, marries him, and has a baby.
* '''Krook''' is a rag and bottle merchant and collector of papers. He is the landlord of the house where Nemo and Miss Flite live and where Nemo dies. He seems to subsist on a diet of gin. Krook dies from a case of [[spontaneous human combustion]], something that Dickens believed could happen, but which some critics (such as the English essayist [[George Henry Lewes]]) denounced as outlandish.{{Citation needed|date = February 2016}} Amongst the stacks of papers obsessively hoarded by the illiterate Krook is the key to resolving the case of ''Jarndyce and Jarndyce''.
* '''Jo''' is a young boy who lives on the streets and tries to make a living as a [[crossing sweeper]]. Jo was the only person with whom Nemo had any real connection. Nemo expressed a paternal sort of interest in Jo, (something that no human had ever done). Nemo would share his meagre money with Jo, and would sometimes remark, "Well, Jo, today I am as poor as you," when he had nothing to share. Jo is called to testify at the inquiry into Nemo's death, but knows nothing of value. Despite this, Mr Tulkinghorn pays Mr Bucket to harry Jo and force him to keep "moving along" [leave town] because Tulkinghorn fears Jo might have some knowledge of the connection between Nemo and the Dedlocks. Jo ultimately dies from a disease (pneumonia, a complication from an earlier bout with smallpox which Esther also catches and from which she almost dies).
* '''Allan Woodcourt'''  is a surgeon and a kind, caring man who loves Esther deeply. She in turn loves him but feels unable to respond, not only because of her prior commitment to John Jarndyce, but also because she fears her illegitimacy will cause his mother to object to their connection.
* '''Grandfather Smallweed''' is a moneylender, a mean, bad-tempered man who shows no mercy to people who owe him money and who enjoys inflicting emotional pain on others. He lays claim to the deceased Krook's possessions because Smallweed's wife is Krook's only living relation, and he also drives Mr George into bankruptcy by calling in debts. It has been suggested that his description (together with his grandchildren) fits that of a person with [[progeria]],<ref>{{cite journal | last1 = Singh | first1 = V | year = 2010 | title = Reflections: neurology and the humanities. Description of a family with progeria by Charles Dickens | url = | journal = Neurology | volume = 75 | issue = 6| page = 571 | doi = 10.1212/WNL.0b013e3181ec7f6c | pmid = 20697111 }}</ref> although people with progeria only have a life expectancy of 14 years, while Grandfather Smallweed is very old.<ref>Ewell Steve Roach & Van S. Miller (2004). Neurocutaneous Disorders. Cambridge University Press. p. 150. ISBN 978-0-521-78153-4.</ref>
* '''Mr Vholes''' is  a Chancery lawyer who takes on Richard Carstone as a client, squeezes out of him all the litigation fees he can manage to pay, and then abandons him when Jarndyce and Jarndyce comes to an end.
* '''Conversation Kenge''' is a Chancery lawyer who represents John Jarndyce. His chief foible is his love of grand, portentous, and empty rhetoric.

===Minor characters===
* '''Mr Gridley''' is an involuntary party to a suit in Chancery (based on a real case, according to Dickens's preface), who repeatedly seeks in vain to gain the attention of the Lord Chancellor. He threatens Mr Tulkinghorn and then is put under arrest by Inspector Bucket, but dies, his health broken by his Chancery ordeal.
* '''Nemo''' (Latin for "nobody") is the alias of Captain James Hawdon, a former officer in the British Army under whom Mr George once served. Nemo is a [[law-writer]] who makes fair copies of legal documents for Snagsby and lodges at Krook's rag and bottle shop, eventually dying of an opium overdose. He is later found to be Lady Dedlock's former lover, and the father of Esther Summerson.  
* '''Mrs Snagsby''' is Mr Snagsby's highly suspicious and curious wife, who has a "vinegary" personality and incorrectly suspects Mr Snagsby of keeping many secrets from her: she suspects he is Jo's father.
* '''Guster''' is the Snagsbys' maidservant, prone to fits.
* '''Neckett''' is a debt collector – called "Coavinses" by debtor Harold Skimpole because he works for that business firm.
* '''Charley''' is Coavinses' daughter, hired by John Jarndyce to be a maid to Esther. Called "Little Coavinses" by Skimpole.
* '''Tom''' is Coavinses' young son.
* '''Emma''' is Coavinses' baby daughter.
* '''Mrs Jellyby''' is Caddy's mother, a "telescopic philanthropist" obsessed with an obscure African tribe but having little regard for the notion of charity beginning at home.  It's thought Dickens wrote this character as a criticism of female activists like [[Caroline Chisholm]].
* '''Mr Jellyby''' is Mrs Jellyby's long-suffering husband.
* '''Peepy Jellyby''' is the Jellybys' young son.
* '''Prince Turveydrop''' is a dancing master and proprietor of a dance studio.
* '''Old Mr Turveydrop''' is a master of deportment who lives off his son's industry.
* '''Jenny''' is a brickmaker's wife. She is mistreated by her husband and her baby dies. She then helps her friend look after her own child.
* '''Rosa''' is a favourite lady's maid of Lady Dedlock whom Watt Rouncewell wishes to marry. The proposal ends in nothing when Mr Rouncewell's father asks that Rosa be sent to school to become a lady worthy of his son's station. Lady Dedlock questions the girl closely regarding her wish to leave, and promises to look after her instead. In some way, Rosa is a stand-in for Esther in Lady Dedlock's life.
* '''Hortense''' is lady's maid to Lady Dedlock.  Her character is based on the Swiss maid and murderer [[Maria Manning]].<ref>{{cite web|url=http://www.fidnet.com/~dap1955/dickens/dickens_london_map.html |title=Dickens' London map |publisher=Fidnet.com |date= |accessdate=15 February 2013}}</ref>
* '''Mrs Rouncewell''' is housekeeper to the Dedlocks at Chesney Wold.
* '''Mr Robert Rouncewell''', the adult son of Mrs Rouncewell, is a prosperous [[ironmaster]].
* '''Watt Rouncewell''' is Robert Rouncewell's son.
* '''Volumnia''' is a cousin of the Dedlocks, given to screaming.
* '''Miss Barbary''' is Esther's godmother and severe childhood guardian.
* '''Mrs Rachael Chadband''' is a former servant of Miss Barbary's.
* '''Mr Chadband''' is an oleaginous preacher, husband of Mrs Chadband.
* '''Mrs Smallweed''' is the wife of Mr Smallweed senior and sister to Krook. She is suffering from dementia.
* '''Young Mr (Bartholemew) Smallweed''' is the grandson of the senior Smallweeds and friend of Mr Guppy.
* '''Judy Smallweed''' is the granddaughter of the senior Smallweeds.
* '''Tony Jobling''', who adopts the alias Mr Weevle, is a friend of William Guppy.
* '''Mrs Guppy''' is Mr Guppy's aged mother.
* '''Phil Squod''' is Mr George's assistant.
* '''Matthew Bagnet''' is a military friend of Mr George's and a dealer in musical instruments.
* '''Mrs Bagnet''' is the wife of Matthew Bagnet.
* '''Woolwich''' is the Bagnets' son.
* '''Quebec''' is the Bagnets' elder daughter.
* '''Malta''' is the Bagnets' younger daughter.
* '''Mrs Woodcourt''' is Allan Woodcourt's widowed mother.
* '''Mrs Pardiggle''' is a woman who does "good works" for the poor, but cannot see that her efforts are rude and arrogant, and do nothing at all to help. She inflicts her activities on her five small sons, who are clearly rebellious.
* '''Arethusa Skimpole''' is Mr Skimpole's "Beauty" daughter.
* '''Laura Skimpole''' is Mr Skimpole's "Sentiment" daughter.
* '''Kitty Skimpole''' is Mr Skimpole's "Comedy" daughter.
* '''Mrs Skimpole''' is Mr Skimpole's ailing wife, who is weary of her husband and his way of life.

==Analysis and criticism==
{{refimprove section|date=April 2016}}
Much criticism of ''Bleak House'' focuses on its unique narrative structure: it is told both by a third-person omniscient narrator and a first-person narrator (Esther Summerson). The omniscient narrator speaks in the present tense and is a dispassionate observer.  Esther Summerson tells her own story in the past tense (like David in ''[[David Copperfield (novel)|David Copperfield]]'' or Pip in ''[[Great Expectations]]''), and her narrative voice is characterised by modesty, consciousness of her own limits, and willingness to disclose to us her own thoughts and feelings. These two narrative strands never quite intersect, though they do run in parallel. Nabokov felt that letting Esther tell part of the story was Dickens's "main mistake" in planning the novel<ref>Nabokov, Vladimir, "Bleak House," ''Lectures on Literature''. New York: Harcourt Brace Jovanovich, 1980. pp. 100–102.</ref> Alex Zwerdling, a scholar from Berkeley, after observing that "critics have not been kind to Esther," nevertheless thought Dickens's use of Esther's narrative "one of the triumphs of his art".<ref>Alex Zwerdling. "Esther Summerson Rehabilitated" ''PMLA'', Vol. 88, No. 3 (May 1973), pp. 429–439</ref>

[[File:Tom All Alones.2.jpg|upright|thumb|left|Tom All Alones]]

Esther's portion of the narrative is an interesting case study of the Victorian ideal of feminine modesty. She introduces herself thus: "I have a great deal of difficulty in beginning to write my portion of these pages, for I know I am not clever" (chap. 3). This claim is almost immediately belied by the astute moral judgement and satiric observation that characterise her pages. In the same introductory chapter, she writes: "It seems so curious to me to be obliged to write all this about myself! As if this narrative were the narrative of MY life! But my little body will soon fall into the background now" (chap. 3). This does not turn out to be true.

For most readers and scholars, the central concern of ''Bleak House'' is its indictment of the English Chancery court system. Chancery or equity courts were one half of the English civil justice system, existing side-by-side with law courts. Chancery courts heard actions having to do with wills and estates, or with the uses of private property. By the mid-nineteenth century, English law reformers had long criticised the delays of Chancery litigation, and Dickens found the subject a tempting target. (He already had taken a shot at law-courts and that side of the legal profession in his 1837 novel ''The Posthumous Papers of the Pickwick Club'' or ''[[The Pickwick Papers]]'').  Scholars – such as the English legal historian Sir [[William Searle Holdsworth]], in his 1928 series of lectures ''Charles Dickens as a Legal Historian'' published by Yale University Press – have made a plausible case for treating Dickens's novels, and ''Bleak House'' in particular, as primary sources illuminating the history of English law.

Dickens claimed in the preface to the book edition of ''Bleak House'' that he had "purposely dwelt upon the romantic side of familiar things". And some remarkable things do happen: One character, Krook, smells of brimstone and eventually dies of [[spontaneous human combustion]]. This was highly controversial. The nineteenth century saw the increasing triumph of the scientific worldview. Scientifically inclined writers, as well as doctors and scientists, rejected spontaneous human combustion as legend or superstition. When the instalment of ''Bleak House'' containing Krook's demise appeared, the literary critic [[George Henry Lewes]] accused Dickens of "giving currency to a vulgar error".<ref>Hack, Daniel (2005). ''The Material Interests of the Victorian Novel'', p. 49. University of Virginia Press.</ref> Dickens vigorously defended the reality of spontaneous human combustion and cited many documented cases, as well as his own memories of coroners' inquests that he had attended when he had been a reporter. In the preface of the book edition of ''Bleak House'', Dickens wrote: "I shall not abandon the facts until there shall have been a considerable Spontaneous Combustion of the testimony on which human occurrences are usually received."

[[George Gissing]] and [[G. K. Chesterton]] are among those literary critics and writers who consider ''Bleak House'' to be the best novel that Charles Dickens wrote. As Chesterton put it: "''Bleak House'' is not certainly Dickens' best book; but perhaps it is his best novel". [[Harold Bloom]], in his book ''The Western Canon'', considers ''Bleak House'' to be Dickens's greatest novel. Daniel Burt, in his book ''The Novel 100: A Ranking of the Greatest Novels of All Time'', ranks ''Bleak House'' number 12.

==Locations of Bleak House==
[[Image:Bleak-house-broadstairs.jpg|180px|thumb|Bleak House in [[Broadstairs]], Kent, where Dickens wrote [[David Copperfield (novel)|David Copperfield]] and other novels.]]

The house named [[Bleak House, Broadstairs|Bleak House]] in [[Broadstairs]], is not the original. Dickens stayed with his family at this house (then called Fort House), for at least one month every summer from 1839 until 1851. However, there is no evidence that it formed the basis of the fictional Bleak House, particularly as it is located so far from the location of the fictional one.

The house is located on top of the cliff on Fort Road, and was renamed Bleak House after his death, in his honour.<ref>{{Cite news|url=http://www.dailymail.co.uk/news/article-2007820/Charles-Dickenss-Bleak-House-holiday-home-sale-2m.html |title=What the Dickens? Author's Bleak House holiday home up for sale at £2m |publisher=www.dailymail.co.uk |accessdate=17 June 2013 |location=London |deadurl=yes |archiveurl=https://web.archive.org/web/20130509122707/http://www.dailymail.co.uk/news/article-2007820/Charles-Dickenss-Bleak-House-holiday-home-sale-2m.html |archivedate=9 May 2013 }}</ref>  It is the only four storey grade II listed mansion in Broadstairs.

Dickens locates the fictional Bleak House in St Albans, Hertfordshire, where he wrote some of the book. An 18th-century house in Folly Lane, St Albans, has been identified as a possible inspiration for the titular house in the story since the time of the book's publication and was known as Bleak House for many years.<ref>http://www.hertsmemories.org.uk/content/herts-history/topics/literary_hertfordshire/charles-dickens</ref>

==Adaptations==
In the late nineteenth century, actress [[Fanny Janauschek]] acted in a stage version of ''Bleak House'' in which she played both Lady Dedlock and her maid Hortense.  The two characters never appear on stage at the same time. In 1876 John Pringle Burnett's play, ''Jo'' found success in London with his wife, [[Jennie Lee (stage actor)|Jennie Lee]] playing Jo, the [[crossing-sweeper]].<ref>Jennie Lee, Veteran Actress, Passes Away. '' Lowell Sun,'' 3 May 1930, p. 18</ref> In 1893, [[Jane Coombs]] acted in a version of ''Bleak House''.<ref>Mawson, Harry P. "Dickens on the Stage." In [http://www.books.google.com/books?id=NwBEAQAAIAAJ&printsec=frontcover#v=onepage&q&f=false ''The Theatre Magazine''] {{webarchive |url=https://web.archive.org/web/20140310101448/http://www.books.google.com/books?id=NwBEAQAAIAAJ&printsec=frontcover#v=onepage&q&f=false |date=10 March 2014 }}, February 1912, p. 48. Accessed 26 January 2014.</ref>

A 1901 short film, ''[[The Death of Poor Joe]]'', is the oldest known surviving film featuring a [[Charles Dickens]] character (Jo in ''Bleak House'').<ref name="BBC">{{cite news|url=http://www.bbc.co.uk/news/entertainment-arts-17298021 |title=Earliest Charles Dickens film uncovered |accessdate=9 March 2012 |work=BBC News |date=9 March 2012}}</ref>

In the silent film era, ''Bleak House'' was filmed [[Bleak House (1920 film)|in 1920]] and 1922. The latter version featured [[Sybil Thorndike]] as Lady Dedlock.<ref>Pitts, Michael R. (2004). [https://books.google.com/books?id=xSmWVsDh8WEC&pg=PA82 ''Famous Movie Detectives III''], pp. 81–82. Scarecrow Press.</ref>

In 1928, a short film made in the UK in the [[Phonofilm]] sound-on-film process starred [[Bransby Williams]] as Grandfather Smallweed.<ref>Guida, Fred (2000; 2006 repr.). [http://www.books.google.com/books?id=JpvP1yPvsZoC ''A Christmas Carol and Its Adaptations''] {{webarchive |url=https://web.archive.org/web/20140310101450/http://www.books.google.com/books?id=JpvP1yPvsZoC |date=10 March 2014 }}, p. 88. McFarland.</ref>

In 1998, [[BBC Radio 4]] broadcast a radio adaptation of five hour-long episodes, starring [[Michael Kitchen]] as John Jarndyce.<ref>{{cite web|url=http://www.bbc.co.uk/programmes/b00801n9|title=BBC Radio 7 - Bleak House, Episode 1|work=BBC}}</ref>

The [[BBC]] has produced three television adaptations of ''Bleak House''. The first serial, ''[[Bleak House (1959 TV serial)|Bleak House]]'', was broadcast in 1959 in eleven half-hour episodes.<ref>{{cite web|url=http://imdb.com/title/tt0224837/ |title="Bleak House" (1959) |publisher=IMDb.com |date= |accessdate=15 February 2013}}</ref> The second ''[[Bleak House (1985 TV serial)|Bleak House]]'', starring [[Diana Rigg]] and [[Denholm Elliott]], aired in 1985 as an eight-part series.<ref>{{cite web|url=http://imdb.com/title/tt0088485/ |title="Bleak House" (1985) (mini) |publisher=IMDb.com |date= |accessdate=15 February 2013}}</ref> In 2005, the third ''[[Bleak House (2005 TV serial)|Bleak House]]'' was broadcast in fifteen episodes starring [[Anna Maxwell Martin]], [[Gillian Anderson]], [[Denis Lawson]], [[Charles Dance]], and [[Carey Mulligan]].<ref>{{cite web|url=http://imdb.com/title/tt0442632/ |title="Bleak House" (2005) |publisher=IMDb.com |date= |accessdate=15 February 2013}}</ref> It won a [[Peabody Award]] that same year because it "created “appointment viewing,” soap-style, for a series that greatly rewarded its many extra viewers."<ref>[http://www.peabodyawards.com/award-profile/bleak-house 65th Annual Peabody Awards], May 2006.</ref>

==Musical references==
[[Charles Jefferys]] wrote the words for and [[Charles William Glover]] wrote the music for songs called ''Ada Clare''<ref>{{cite web|url=http://nla.gov.au/nla.mus-an9781836|title=Digital Collections - Music - Glover, Charles William, 1806-1863. Ada Clare [music] : "Bleak House" lyrics|publisher=}}</ref> and ''Farewell to the Old House'',<ref>{{cite web|url=http://nla.gov.au/nla.mus-an5639480|title=Digital Collections - Music - Glover, Charles William, 1806-1863. Farewell to the old house [music] : the song of Esther Summerson|publisher=}}</ref> which are inspired by the novel.

[[Anthony Phillips]] included a piece entitled "Bleak House" on his 1979 [[Progressive Rock]] release, "[[Sides (album)|Sides]]." The form of the lyrics roughly follows the narrative of Esther Summerson, and is written in her voice.<ref>
{{cite web|url=http://www.anthonyphillips.co.uk/lyrics/sides.htm#bleak|title=Anthony Phillips Official Website - Lyrics - Sides}}</ref>

==Original publication==
Like most Dickens novels, ''Bleak House'' was published in 20 monthly instalments, each containing 32 pages of text and two illustrations by [[Phiz]] (the last two being published together as a double issue). Each cost one shilling, except for the final double issue, which cost two shillings.{{citation needed|date=January 2013}}
{|
|-
! Instalment
! Date of publication
! Chapters
|-
! I
| March 1852
| 1–4
|-
! II
| April 1852
| 5–7
|-
! III
| May 1852 || 8–10
|-
! IV
| June 1852 || 11–13
|-
! V
| July 1852 || 14–16
|-
! VI
| August 1852 || 17–19
|-
! VII
| September 1852 || 20–22
|-
! VIII
| October 1852 || 23–25
|-
! IX
| November 1852 || 26–29
|-
! X
| December 1852 || 30–32
|-
! XI
| January 1853 || 33–35
|-
! XII
| February 1853 || 36–38
|-
! XIII
| March 1853 || 39–42
|-
! XIV
| April 1853 || 43–46
|-
! XV
| May 1853 || 47–49
|-
! XVI
| June 1853 || 50–53
|-
! XVII
| July 1853 || 54–56
|-
! XVIII
| August 1853 || 57–59
|-
! XIX–XX
| September 1853 || 60–67
|}

==References==
{{portal|Charles Dickens|Literature|Novels}}
{{Reflist|30em}}

==Sources==
* Crafts, Hannah; Gates, Jr, Henry Louis (Ed). ''The Bondswoman's Narrative''. Warner Books, 2002. ISBN 0-7628-7682-4
* "Blackening Bleak House: Hannah Crafts's ''The Bondwoman's Narrative''," in ''In Search of Hannah Crafts: Critical Essays on the Bondwoman's Narrative,'' eds. Henry Louis Gates, Jr. and Hollis Robbins.  Basic/Civitas, 2004. ISBN 0-465-02708-3
* Calkins, Carroll C. (Project Editor). ''Mysteries of the Unexplained''. Pleasantville, New York: The Reader's Digest Association, Inc., 1982.
* Holdsworth, William S. ''Charles Dickens as a Legal Historian''. Yale University Press, 1928. Contains detailed information on the workings of the [[Court of Chancery]].
* [http://www.communitywalk.com/bleak_house_map/map/204596 Bleak House Map]

==External links==
{{Wikisource}}
{{commons category|Bleak House}}
* [https://archive.org/stream/bleakhouse00dickiala#page/n9/mode/2up ''Bleak House''] at [[Internet Archive]].
* {{Gutenberg|no=1023|name=Bleak House}}
* {{FadedPage|id=20161103|name=Bleak House}}
* [http://www.ric.edu/faculty/rpotter/darkplates_index.html Dark Plates] The ten "dark plates" executed by H.K. Browne for ''Bleak House''.
* {{Gutenberg|no=872|name=Reprinted Pieces}} "The Detective Police", "Three Detective Anecdotes", "On Duty with Inspector Field". Last piece first published in ''Household Words'', June 1841.
* {{Librivox book | title=Bleak House | author=Charles Dickens}}

{{Charles Dickens}}
{{Bleak House}}
{{Nuttall|title=Skimpole, Harold}}

{{Authority control}}

[[Category:1853 novels]]
[[Category:British novels adapted into films]]
[[Category:English novels]]
[[Category:Novels by Charles Dickens]]
[[Category:Novels first published in serial form]]
[[Category:Novels set in the 19th century]]
[[Category:Victorian novels]]
[[Category:Law in fiction]]
[[Category:Novels set in London]]
[[Category:Novels adapted into television programs]]