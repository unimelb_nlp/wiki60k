'''Text and conversation''' is a [[theory]] in the field of [[organizational communication]] illustrating how [[communication]] makes up an [[organization]]. In the theory's simplest explanation, an organization is created and defined by communication. Communication "is" the organization and the organization exists because communication takes place.  The theory is built on the notion, an organization is not seen as a physical unit holding communication.<ref name="Miller, 2005">Miller, 2005.</ref>  Text and conversation theory puts communication processes at the heart of organizational communication and postulates, an organization doesn’t contain communication as a "causal influence,"<ref name="Miller, 2005" /> but is formed by the communication within. This theory is not intended for direct application, but rather to explain how communication exists. The theory provides a [[conceptual framework|framework]] for better understanding organizational communication.

Since the foundation of organizations are in communication,<ref name="Taylor, 1999">Taylor, 1999.</ref>  an organization cannot exist without communication, and the organization is defined as the result of communications happening within its context. Communications begin with individuals within the organization discussing [[beliefs]], [[goal]]s, [[structure]]s, [[plans]] and relationships. These communicators achieve this through constant development, delivery, and translation of "text and conversation." The theory proposes mechanisms of communications are "[[literary work|text]] and "[[conversation]]."

== Definitions ==
The foundation of this theory is the concepts of text and conversation. Text is defined as the content of [[interaction]], or what is said in an interaction. Text is the meaning made available to individuals through a [[Face-to-face (philosophy)|face-to-face]] or electronic mode of communication. Conversation is defined as what is happening behaviorally between two or more participants in the communication process. Conversation is the exchange or interaction itself.<ref name="Taylor, 1999" />

The process of the text and conversation exchange is [[reciprocal determinism|reciprocal]]: text needs conversation and vice versa for the process of communication to occur. Text, or content, must have context to be effective and a conversation, or discourse, needs to have a beginning, middle and end. Individuals create the beginning, middle and end by using [[punctuation]], [[bracketing]] or [[framing (social sciences)|framing]]. When conversation is coupled with text, or meaning, communication occurs.<ref name="Taylor, 1999" /> Taylor submits this process is a translation process of: translation of text to conversation and the [[translation]] of conversation into text.<ref name="Miller, 2005" />
*"text" = content and meaning
*"conversation" = [[discourse]] and exchange
<!-- Deleted image removed: [[Image:Textandconversation.jpg|thumb|400px|center|Figure 1: Communication as the intersection of communication and text]] -->

== Theorist ==
<!-- Deleted image removed: [[File:James renwick taylor.jpg|thumb|James R. Taylor]] -->

[[James R. Taylor]], introduced text and conversation theory in 1996 with [[François Cooren]], Giroux and Robichaud and then further explored the theory in 1999.  Taylor drew on the work of sociologist and educator [[John Dewey| John Dewey’s]] pragmatic view society exists not "by" but "in" communication.  Taylor followed the same principle, putting communication as the essence of an organization.
He was born in 1928 and is [[Professor Emeritus]] at the Department of Communication of the [[Université de Montréal]], which he founded in the early 1970s. Drawing from research in fields of [[Industrial and organizational psychology|organizational psychology]] ([[Karl E. Weick]]), [[ethnomethodology]] ([[Harold Garfinkel]]), Deirdre Boden), [[Phenomenology (psychology)|phenomenology]] ([[Alfred Schütz]]) and collective minding ([[Edwin Hutchins]]), Taylor formed the original text and conversation theory. This line of thought has come to be known as "The Montreal School" of organizational communication, sometimes referred to as TMS, and has been acknowledged as an original theory by authors such as [[Haridimos Tsoukas]], Linda Putman, and Karl E. Weick.
*Taylor said,"...organization emerges in communication, which thus furnishes not only the site of its appearance to its members, but also the surface on which members read the meaning of the organization to them." Taylor argues communication is the "site and emergence of organization."<ref name="Taylor, 1999" />

== Foundational theories ==

=== Structuration theory ===
"[[Structuration theory]]" identifies how text and conversation theory evolved from this communication construct.  Proposed by [[Anthony Giddens, Baron Giddens|Anthony Giddens]] (1984) in ‘’The Constitution on Society,’’ structuration theory, originated in the discipline of sociology.  Giddens’ theory has been adapted to the field of communication, particularly organizational communication; specifically, how and why [[structural change]]s are possible and the duality of formal and informal communication.
This theory is based on concepts of [[structure and agency]].  structure is defined as rules and resources of an organization; agency is the free will to choose to do otherwise than prescribed through structure.
*"structure":  is rules and resources, the reason we do things because of the structure of how we were raised (culture, sociological and [[physiological]]).  Giddens (1984) explains these rules as recipes or procedures for accomplishing tasks within an organization.  Resources have two subsets: allocative and authoritative, which can be leveraged to accomplish desired outcomes.  Allocative are quantitative resources, while authoritative are qualitative.
*"[[Agency (sociology)|agency]]": is the free will to choose to do otherwise. Agency is the reason people do things, because they have a choice<ref>Gidden, 1984.</ref>  This is the process individuals internalize actions and make choices, rather than making decisions because the structure says they should.  Structure is based on the formal organization and accepted policy. Agency is informal communication and individually based.
*"[[Dualism]]": mutually exclusive answer (i.e., either/or)
*"[[Dualism|Duality]]": mutually constitutive answer (i.e., both/and)
*"[[Structuration]]": society itself is located in a [[duality of structure]] in which the enactments of agency become structures that, across time, produce possibilities for agency enactment.
*Another way explain it is structure is the context.
Structuration theory identifies [[structure and agency]] as coexisting. Formal rules and resources impact informal communication and discourse. This duality and coexistence ensures a cyclical nature between structure and agency, which has a cause and effect:  new structure and agency is created from the causal relationships of previous structure and agency decisions. The concept to understanding structuration is to understand to duality of structure <ref>Hoffman and Cowan, 2010.</ref> The similarity of Giddens’ theory and conversation and text theory is a mutual-existing and causal relationship of communication. The main difference, between the two, is structuration theory explains how communication impacts the organization, text and conversation, by means of structure and agency. Giddens' construct of structuration explains how mutually causal relationships constitute the essence of an organization. This concept illustrates how communication within an organization depends on the translation of meaning.

=== Conversation theory ===
"[[Conversation theory]]", proposed by [[Gordon Pask]] in the 1970s, identifies a framework to explain how [[scientific theory]] and interactions formulate the "construction of knowledge"<ref>Pask, 1975.</ref>
Conversation Theory is based on the idea [[social system]]s are symbolic and language-oriented.  Additionally, these systems are based on responses and interpretations, and the meaning interpreted by individuals via communication<ref>Pask, 1975, 1976.</ref>
This theory is based on interaction between two or more individuals, with unlike perspectives<ref>Scott, 2001.</ref>  The significance of having unlike perspectives is that it enables a distinctive standpoint:  it permits the ability to study how people identify differences and understand meaning.  Additionally, these differences create shared and consensual pockets of interactions and communications as discussed in [[Structure-Organization-Process]].<ref>Maturana and Varela, 1980.</ref>
Another idea of conversation theory is learning happens by exchanges about issues, which assists in making [[knowledge]] explicit.  In order for this to happen, Pask organized three levels of conversation, according to:<ref name="cortland1">{{cite web|url=http://web.cortland.edu/andersmd/learning/Pask.htm |title=Conversation Theory&nbsp;— Gordon Pask |publisher=Web.cortland.edu |date= |accessdate=2011-10-12}}</ref>
*"[[Natural language]]": general discussion
*"[[Object language]]s": for discussing the subject matter
*"[[Metalanguages]]": for talking about learning/language
Additionally, to facilitate learning, Pask proposed two types of learning strategies.<ref name="cortland1" />
*"Serialists": progress through a structure in a sequential fashion
*"[[Holist]]s": look for higher order relations
Ultimately, Pask found versatile learners neither favor one approach over the other. Rather, they understand how both approaches are integrated into the structure of learning. The similarities of conversation theory and text and conversation theory are they both focus on the foundational aspects of meaning. Specifically, how and why meaning is established and interpreted amongst individuals. However, the difference between the two theories is conversation theory specifically focuses on the dynamics of two people. Text and conversation theory is typically applied to at least two people. Conversation theory emphasizes the construct of knowledge of meaning and the [[Causality|cause and effect]] relationship that occurs as a result of [[Autodidacticism|self-learning]] from communication, based on meaning.

== Factors ==

=== Meaning ===
{{See also|Meaning-making}}
"Meaning management" is the control of "context" and "message" to accomplish a desired communication effect. According to Fairhurst, leaders are change agents<ref name=fair2011>{{harvnb|Fairhurst|2011}}</ref> Leaders define the value of the organization and shape communication by implementing unique organizational communication approaches. Within an organization, leaders and managers establish the framework for communication, which helps to manage meaning. "Leaders" provide information to followers, such as the organizations’ mission, vision, values, as well as its [[collective identity]] <ref>Fairhurst, 1997.</ref>  Contrary to leaders, "managers" are responsible for day to day [[problem solving]]. Their core framing tasks are solving problems and stimulating others to find solutions.<ref name=fair2011/>

Individuals, regardless of positional authority, can manage meaning. Meaning management is to communicate with a specific goal by controlling the context and message<ref name=fair2011/> Individuals utilizing meaning management are communicating and shaping the meaning by using the power of framing.

=== Culture ===
"[[Culture]]" is a unique set of behaviors, including language, belief and customs learnt from being raised in [[social group]]s or by joining a particular group throughout time. Culture defines context and is the social totality that defines behavior, knowledge, beliefs and [[social learning (social pedagogy)|social learning]]. It is a set of shared values characterizing a specific organization. Fairhurst identifies culture as defining events, people, objects, and concepts.<ref name=fair2011/> Communication and culture are intertwined. Shared language of a group links together individuals and joins common cultures. Culture influences [[mental model]]s. "Mental models" are the images in your mind about other people, yourself, substance and events.<ref name=fair2011/>

Additionally, culture defines [[social interaction]]s and how individuals and groups interpret and apply context. Organizations with good communication foundation are able to interpret and differentiate individuals’ cultural discourses, as well as creatively combine and constrain these discourses.<ref name="Fairhurst, 2007">Fairhurst, 2007.</ref> Ir defines the ideological basis for people and lays the foundation for how they frame and can be observed and described, but not controlled. It is defined by the group or individual accepting the specific patterns of behavior, knowledge, or beliefs  Individuals can shape culture and make changes over time, as long as they are clear about specific attitudes and behaviors that are desired <ref name="Fairhurst, 2007"/>  As [[Weick]] and Sutcliffe (2007) discussed, culture can be changed through symbols, values, and content&nbsp;— organizations shape culture.  An [[organizational culture]] emerges from a set of expectations that matter to people, from things like [inclusion, exclusion, praise, positive feelings, [[social support]], isolation, care, indifference, excitement and anger <ref>Weick and Suitcliffe, 2007.</ref>  Individuals are shaped by an organization's culture.  However, an organization has its own culture. According to Martin (1985), within that organizational culture, three forms of culture can result: integration, differentiation and fragmentation.
*"[[Social integration|Integration]]" (bring people together)
*"[[Differentiation (sociology)|Differentiation]]" (act or process by which people undergo change toward more specialized function)
*"[[Fragmentation (sociology)|Fragmentation]]" (process of state of breaking or being broken into smaller parts)
With Integration, all organizational members consistently share values and assumptions about work. As a result the members of the organization share uniquely organizational experiences and thus, a unique culture<ref name="Martin, 1985">Martin, 1985.</ref>
If differentiation occurs, cultures are not unitary. Sub-groups consistently share values and assumptions about work.  Members tend to operate in different areas, different projects and at different levels of the hierarchy.<ref name="Martin, 1985" />
Cultures are often ambiguous if fragmentation happens. Individuals are interconnected with some members and disconnected with others. This creates inconsistently shared values and assumptions about the organization<ref name="Martin, 1985" /> As a result friendship/romantic as well as enemy/competitor type relationships are cut across an organization’s sub-groups.

=== Structure ===
Individuals who understand the structure and inner working of their organizations can leverage knowledge toward achieving communication goals. Likewise, organizations can also leverage their hierarchical structures to achieve targeted outcomes.
Two types of structures exist within an organization.
*"[[Hierarchical]]" (formal hierarchical structure, typical flow/pyramid chart)
*"Network" (informal structure, based on relationships, go to people, subject-matter experts)
Goldsmith and Katzenback (2008) explained organizations must understand the informal organization. For example, of being a part of an informal or formal structure, it is important for managers to learn to recognize signs of trouble in order to shape context as they attempt to coordinate meaning and solve day-to-day problems. Specific implications for [[organizational learning]] include enhanced performance, coordinated activity and structure, [[division of labor]] and collective goal setting <ref name="BEA07">Bryan et al, 2007.</ref>
While a formal organization is visually represented by a typical hierarchical structure, it visually shows how formal responsibilities are spread, as well as job dispersal and the flow of information<ref>Goldsmith and Katzenback, 2008.</ref>  In contrast, the informal organization embodies how people network to accomplish the job, via social relationships and connections or [[subject-matter expert]]s that are not represented on the organizational chart<ref>Goldsmith and Katzenbach, 2008.</ref>  By leveraging this informal organization, people within the organization are able to use their [[social network]] to access and shape the [[decision-making process]]es quicker, as well as establish cross-structural collaboration amongst themselves.
Additionally, by understanding and using both structures, leaders and managers are able to learn more about their people.  Interpreting all forms of communication, verbal and visual, whether you are a supervisor or a subordinate is invaluable.  The hierarchical and network structures can allow an organization to recognize signs of trouble from people, accomplish core framing tasks, and to be able to communicate with mindfulness and meaning.  By unlocking the value of an organization's structure, leaders and managers can use this knowledge to boost performance or achieve specific goals.<ref name="BEA07"/> Signs of trouble can be emotional, hidden, physical, or in plain sight.

=== Knowledge ===
Knowing individuals’ personalities, conflict tendencies, as well as their unique circumstances help an organization to understand its [[mental model]]s and cultural discourse.  Additionally, by noticing abnormalities and not being blind to details, an organization should be able to recognize signs of trouble within day-to-day operations and management, whether it is fraud, lack of maintenance standards, [[sexual harassment]], or even a poor framework for communication.
Understanding and the ability to recognize signs of trouble empower managers to employ the rules of reality construction:  control the context, define the situation, apply ethics, interpret uncertainty, and design the response, which leads to communicating by a structured way of thinking.<ref name=fair2011/>

Ultimately, by understanding how an organization works, you enhance communication collectively.<ref name="BEA07" /> Additionally, by knowing how employees and relationships are shaped and the context that defines how each person interacts with one another, you can shape contagious emotions.
Basic building blocks of Taylor’s theories is the relationship of text and conversation, and how that relationship requires a "two-step translational process"<ref name="Miller, 2005" />
*translation One:  From text to conversation
*translation Two: From conversation to text
Following this translational process, text and conversation is transferred to organizational communication. If context, or text, defines the organization then ongoing introductions and meaning are crucial to define what is meant by the term organization.<ref name="Miller, 2005" />
To examine this further, Taylor defined "six degrees of separation" to understand organizational communication:<ref>Taylor et al, 1996.</ref>
*First Degree of Separation: Intent of speaker is translated into action and embedded in conversation.
*Second Degree of Separation: Events of the conversation are translated into a narrative representation, making it possible to understand the meaning of the exchange.
*Third Degree of Separation: The text is transcribed (objectified) on some permanent or semi-permanent medium (e.g., the minutes of a meeting are taken down in writing).
*Fourth Degree of Separation: A specialized language is developed to encourage and channel subsequent texts and conversations(e.g., lawyers develop specific ways of talking in court, with each other, and in documents).
*Fifth Degree of Separation: The texts and conversations are transformed into material and physical frames (e.g., laboratories, conference rooms, organizational charts, procedural manuals).
*Sixth Degree of Separation: The standardized form is disseminated and diffused to a broader public (e.g., media reports and representations of organizational forms and practices).

== Impact ==
This theory uses interactions of text and conversation to construct networks of relationships. By doing so, the theory enables a deep understanding of personal communication within an organization. Additionally, it explains how that communication ends up actually defining the organization, rather than the individuals within the organization. Taylor’s theory places more importance on personal communication, rather than individuals. The practical application, as a result, is communication behaviors can constitute how and what we think of an organization. Additionally, by manipulating communication processes, not only could structure be altered, but the entire organization could be changed as well<ref name="Heath et al, 2006">Heath et al, 2006.</ref> whether change is beneficial or negative, is based on desired meaning, or context and message, people within the organization want to exchange and translate.

Taylor stresses the importance and impact of dialogue, specifically relating to how people interact with one another and interpret context. Taylor explains in Heath et al. (2006) that virtuous reasoning embodies entire discussions. Additionally, he points out dialogue should not prevent issues that arise from debate<ref name="Heath et al, 2006"/> Since 1993, Taylor’s theory has been the focus of more than six organizational communication books. Additionally, Taylor’s ideas are referred to as "The Montreal School" of organizational communication<ref name="cortland1" /> Within the field of communication, TMS has been recognized for its contributions to organizational communication as well as related disciplines. Books focusing on text and conversation theory have sold internationally<ref name="cortland1" /> One to the largest and simplest contributions this theory provided the communication academic field was the ability to describe and characterize and organization. From this, people could better understand and fully construct and organization’s identity.

== Weakness ==
According to Nonaka and Takeuchi (1995), [[organizational learning]] is the study of how collectives adapt to, or fail to adapt to, their environments.  It utilizes tacit knowledge and explicit knowledge.
*"[[Tacit Knowledge]]": personal, contextual, subjective, implicit, and unarticulated
*"[[Explicit Knowledge]]": codified, systematic, formal, explicit, and articulated
Ultimately, organizational learning achieves enhanced performance, coordinated activity and structure, and achievement of collective goals by externalization and internalization.
*"[[Externalization]]": getting key workers to make their tacit knowledge the organization’s explicit knowledge that can be shared
*"[[Internalization]]": getting the organization’s explicit knowledge to become workers’ tacit knowledge
Text and conversation theory places significant challenges and burdens on the organization to articulate knowledge.  Whether knowledge is passed directly by individuals, up and down or horizontally on the formal or informal organizational structure, there is no guarantee text has proper context to be effective as conversation.  Additionally, conversation codes are influenced by how the organization ensures knowledge carriers pass information and communicate with purpose, message, and meaning.
How information is passed can be unclear, and consistently has to adapt to new challenges.  Some of these challenges, or factors, include how individuals and an organization adapt to meaning, culture, structure, and knowledge, in order to communicate.
Ultimately, within the organization itself, people are impacted by bias’ on group and individual levels.

"Problems with Group Learning"
*Responsibility bias:  belief of group members’ that someone else in the group will do the work
*[[Social desirability bias]]:  group members are reluctant to provide critical assessments for fear of losing face or relational status
*Hierarchical mum effect:  subordinates’ reluctance to provide negative feedback for fear of harming identifies of superiors
*[[Groupthink]]:  failure to consider decision alternatives
*Identification/[[ego defense]]:  highly identified group members begin to associate their identify with their group membership and will in turn refuse to see the group as wrong, and themselves by extension
"Problems with Individual Learning"
*[[Confirmation bias]]:  individuals seeks to confirm their own ideas, guesses and beliefs rather than seek dis-confirming information
*[[Hindsight bias]]:  individuals tend to forget when their predictions are wrong
*[[Fundamental attribution error]]:  individuals tend to attribute others shortcomings to their character, while attributing their own shortcomings to external forces

== See also ==
* [[Tacit knowledge]]
* [[Explicit knowledge]]
* [[Conversation theory]]
* [[Mental model]]
* [[Organizational structure]]
* [[Organizational culture]]
* [[Organizational communication]]
* [[Sensemaking]]
* [[Structuration theory]]

== References ==
{{reflist|colwidth=30em}}

== Bibliography ==

*Giddens, A. (1986).  Constitution of society:  Outline of the theory of structuration, University of California Press; Reprint edition (January 1, 1986) ISBN 0-520-05728-7
*Hoffman, M. F., & Cowan, R. L. (2010). Be Careful What You Ask For: Structuration Theory and Work/Life Accommodation. Communication Studies, 61(2), 205-223. {{doi|10.1080/10510971003604026}}
*Gordon Pask, Conversation, cognition and learning. New York: Elsevier, 1975.
*Gordon Pask, The Cybernetics of Human Learning and Performance, Hutchinson. 1975
*Gordon Pask, Conversation Theory, Applications in Education and Epistemology, Elsevier, 1976.
*Scott, B. (2001). Gordon Pask's Conversation Theory: A Domain Independent Constructivist Model of Human Knowing. Foundations of Science, 6(4), 343-360.
*Maturana,H. and F.J. Varela: 1980, Autopoiesis and Cognition. Reidel, Dordrecht, Holland.
*Conversation Theory&nbsp;– Gordon Pask overview from web.cortland.edu: http://web.cortland.edu/andersmd/learning/Pask.htm
*{{citation|last=Fairhurst|first=G. T.|year=2011|title=The power of framing:  creating the language of leadership|location=San Francisco|publisher=Jossey-Bass}}
*Fairhurst, G. T., Jordan, J., & Neuwirth, K. (1997). Why are we here? Managing the meaning of an organizational mission statement. ‘’Journal of Applied Communication Research’’, 25(4), 243-263.
*Weick, K. E., & Sutcliffe, K. M. (2007). Managing the unexpected:  resilient performance in an age of uncertainty (2nd ed.). San Francisco: Jossey-Bass.
*Martin, J. & Meyerson, D. 1985. Organizational cultures and the denial, masking and amplification of ambiguity. Research Report No. 807, Graduate School of Business, Stanford University, Stanford.
*Goldsmith, M., & Katzenbach, J. (2007 February 14).  Navigating the "informal" organization [Electronic version].  Business Week: http://www.businessweek.com/careers/content/feb2007/ca20070214_709560.htm
*Bryan, L. L., Matson, E., & Weiss, L. M. (2007). Harnessing the power of informal employee networks. ‘’McKinsey Quarterly’’, (4), 44-55.
*Miller, K. (2005). Communication theories: Perspectives, processes, and contexts (2nd Ed.) Columbus, OH: McGraw Hill.
*Taylor, J.R., Cooren, F., Giroux, N., & Robichaud, D. (1996). The communicational basis of organization: Between the conversation and the text. Communication Theory, 6, 1-39.
*Heath, R. L., Pearce, W., Shotter, J., Taylor, J. R., Kersten, A., Zorn, T., & ... Deetz, S. (2006). THE PROCESSES OF DIALOGUE: Participation and Legitimation. ‘’Management Communication Quarterly’’, 19(3), 341-375. {{doi|10.1177/0893318905282208}}
*Welcome to Jim Taylor and Elizabeth Van Every's Website: http://www.taylorvanevery.com/
*Nonaka, I. & Takeuchi, H. (1995). The knowledge-creating company. New York: Oxford University Press
*Giddens, A. (1991). Modernity and self-identity: self and society in the late modern age. Stanford: Stanford University Press.

{{Communication studies|state=expanded}}

[[Category:Communication theory]]