{{Infobox journal
| title = Neuropsychopharmacology
| cover = [[File:NeuropsychopharmacologyCover.jpg]]
| editor = William A. Carlezon Jr.
| discipline = [[Neuropsychopharmacology]]
| abbreviation = Neuropsychopharmacology
| publisher = [[Nature Publishing Group]]
| country =
| frequency = Monthly
| history = 1987-present
| openaccess = [[Delayed open access journal|Delayed]], after 12 months
| impact = 7.048
| impact-year = 2014
| website = http://www.nature.com/npp/
| link1 =
| link1-name =
| link2 = http://www.nature.com/npp/archive/index.html
| link2-name = Online archive
| JSTOR =
| OCLC = 815994337
| LCCN =
| CODEN = NEROEW
| ISSN = 0893-133X
| eISSN = 1740-634X
}}
'''''Neuropsychopharmacology''''' is a [[peer-reviewed]] [[scientific journal]] published by the [[Nature Publishing Group]]. It has been the official publication of the [[American College of Neuropsychopharmacology]] since 1987. The journal covers topics in [[neuropsychopharmacology]], including [[clinical research|clinical]] and [[basic science]] research into the [[brain]] and behavior, the properties of agents acting within the [[central nervous system]], and [[drug development|drug targeting and development]].

== Abstracting and indexing ==
The journal is abstracted and indexed in:<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2014-02-07}}</ref><ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/8904907 |title=Neuropsychopharmacology |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |format= |accessdate=2014-02-07}}</ref><ref name=Scopus>{{cite web |url=http://cdn.elsevier.com/assets/excel_doc/0003/148548/title_list.xlsx |title=Scopus title list |format=[[Microsoft Excel]] |publisher=[[Elsevier]] |work= |accessdate=2014-02-07}}</ref>
{{columns-list|colwidth=30em|
* [[Science Citation Index]]
* [[Current Contents]]/Life Sciences
* [[BIOSIS Previews]]
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]
* [[Scopus]]
}}
According to the ''[[Journal Citation Reports]]'', the journal had a 2014 [[impact factor]] of 7.048.<ref name=WoS>{{cite book |year=2015 |chapter=Neuropsychopharmacology |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

In addition to monthly issues, the journal also publishes a special 13th issue in January, called ''Neuropsychopharmacology Reviews'', focusing on a special theme. The journal also produces free [[podcast]]s to highlight research reports that may be of particular interest to the public; a podcast is also released in January to complement the reviews issue.

== References ==
{{reflist}}

== External links ==
{{Portal|Neuroscience}}
* {{Official website|http://www.nature.com/npp/index.html}}
* [http://www.nature.com/nppr/index.html Neuropsychopharmacology Reviews]

{{Georg von Holtzbrinck Publishing Group}}

{{DEFAULTSORT:Neuropsychopharmacology (Journal)}}
[[Category:Neuroscience journals]]
[[Category:Psychiatry journals]]
[[Category:Nature Publishing Group academic journals]]
[[Category:Publications established in 1993]]
[[Category:Pharmacology journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]