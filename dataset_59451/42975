<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Hiro H2H
 | image=Hiro_H2H_flying_boat.jpg
 | caption=H2H1 on a launching trolly.
}}{{Infobox Aircraft Type
 | type=Patrol Flying boat
 | national origin=[[Japan]]
 | manufacturer=[[Hiro Naval Arsenal]]
 | designer=
 | first flight=1930
 | introduced=1932
 | retired=
 | status=
 | primary user=[[Imperial Japanese Navy]]
 | number built=17
 | developed from= [[Supermarine Southampton]]
 | variants with their own articles=
}}
|}
The '''Hiro H2H''' (or '''Navy Type 89 Flying boat''') was a [[Japan]]ese patrol [[flying boat]] of the 1930s.  Designed and built by the [[Hiro Naval Arsenal]], it was a twin-engined biplane that was operated by the [[Imperial Japanese Navy]].

==Development and design==
In 1929, the Imperial Japanese Navy purchased a single example of the metal hulled [[Supermarine Southampton]] II metal-hulled flying boat,<ref name="Andrews p110">Andrews and Morgan 1987, p.110.</ref> and after evaluation, it was passed onto the [[Hiro Naval Arsenal]] (who designed the wooden [[Hiro H1H]] flying boat based on the [[Felixstowe F.5]]), to study its advanced metal hull structure.  Following this study, Hiro designed a new flying boat, closely resembling the Southampton.<ref name="Mikesh p95-6">Mikesh and Abe 1990, pp. 95-96.</ref>

The new aircraft was a twin-engined [[biplane]], with an all-metal hull, and fabric covered metal wing and tail structures. It was powered by two [[Hiro Type 14]] water-cooled [[W engine]]s.  The first prototype was completed in 1930, and following successful testing was ordered into production, with 13 aircraft being built by Hiro and a further four by [[Aichi Kokuki|Aichi]]. Later aircraft were powered by more powerful (600-750&nbsp;hp (448-560&nbsp;kW)) [[Hiro Type 90]] engines.<ref name="Mikesh p96-7">Mikesh and Abe 1990, pp.96-97.</ref>

==Operational history==
It entered service in 1932 as the Type 89 Flying boat, with the [[Japanese military aircraft designation systems|short designation]] H2H1.  Type 89 Flying Boats entered service in time for the [[Shanghai Incident]], and along with Hiro's earlier [[Hiro H1H|H1H]], served in front line service until the early years of the [[Second Sino-Japanese War]].<ref name="Mikesh p97"/>

==Specifications (Early version)==

{{aircraft specifications
|plane or copter?=plane<!-- options: plane/copter -->
|jet or prop?=prop<!-- options: jet/prop/both/neither -->
|ref=Japanese Aircraft 1910-1941 <ref name="Mikesh p97">Mikesh and Abe 1990, p.97.</ref><!-- the source(s) for the information -->
|crew=6
|capacity= <!-- the number of passengers carried in the case of a commercial aircraft-->
|payload main=
|payload alt=
|payload more=
|length main= 16.28 m
|length alt=53 ft 5¼ in
|span main=22.14 m
|span alt=72 ft 7¾ in
|height main=6.13 m
|height alt=20 ft 1¼in
|area main= 120.5 m²
|area alt= 1,297 sq ft
|airfoil=
|empty weight main= 4,368 kg
|empty weight alt= 9,629 lb
|loaded weight main= 6,500 kg
|loaded weight alt= 14,330 lb
|useful load main= 
|useful load alt= 
|max takeoff weight main= 
|max takeoff weight alt= 
|max takeoff weight more=
|more general=
|engine (prop)=[[Hiro Type 14]]
|type of prop= 12-cylinder water-cooled [[W engine]]<!-- meaning the type of propeller driving engines -->
|number of props=2<!--  ditto number of engines-->
|power main= 550 hp
|power alt=410 kW
|power original=
|power more=
|propeller or rotor?=<!-- options: propeller/rotor -->
|propellers=
|number of propellers per engine= 
|propeller diameter main=
|propeller diameter alt= 
|max speed main= 192 km/h
|max speed alt=104 knots, 119 mph
|max speed more= 
|cruise speed main= 130 km/h
|cruise speed alt=70 knots, 80.5 mph
|cruise speed more at 1,000 m (3,300 m)
|stall speed main= 
|stall speed alt= 
|stall speed more=
|never exceed speed main= 
|never exceed speed alt= 
|range main= 
|range alt=
|ferry range main=
|ferry range alt=
|ferry range more=
|ceiling main= 4,320 m
|ceiling alt= 9,840 ft
|climb rate main= 
|climb rate alt= 
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=
|power/mass alt=
|more performance=*'''Endurance:''' 14½ hours
*'''Climb to 3,000 m (14,100 ft):''' 19 min
|guns= 4× 7.7 mm machine guns (two in bow, one each in port and starboard mid-ships stations)
|bombs= Two 250 kg (550 lb) bombs
|rockets= 
|missiles= 
|hardpoints= 
|hardpoint capacity=  
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
*[[Hiro H1H]]
*[[Supermarine Southampton]]

|similar aircraft=
*[[Naval Aircraft Factory PN]]
*[[Saunders A.14]]
*[[Supermarine Scapa]]
*[[Hall PH]]<!-- similar or comparable aircraft -->
|lists=
*[[List of military aircraft of Japan]]
*[[List of seaplanes and flying boats]]
<!-- related lists -->
}}

==References==

===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
*Andrews, C.F. and Morgan, E.B. ''Supermarine Aircraft since 1914''. London:Putnam, 1987. ISBN 0-85177-800-3.
*Mikesh, Robert C. and Abe, Shorzoe. ''Japanese Aircraft 1910-1941''. London:Putnam, 1990. ISBN 0-85177-840-2.
{{refend}}

==External links==
*[http://www.hikotai.net/datasheets/h2h.htm Hiro H2H]
{{Hiro Naval Arsenal}}
{{Japanese Navy Flying Boats}}

[[Category:Japanese patrol aircraft 1930–1939]]
[[Category:Flying boats]]
[[Category:Hiro aircraft|H2H]]
[[Category:Twin-engined tractor aircraft]]
[[Category:Biplanes]]