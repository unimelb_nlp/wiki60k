{{Infobox character
| colour      = yellow
| colour text = 
| name        =
| series      = [[Great Expectations]]
| image       = "This chap murdered his master.".jpeg
| caption     = Wemmick shows the [[death mask]] of an executed murderer to Pip in Jaggers's office, by [[John McLenan]]
| first       = 
| last        = 
| cause       = 
| creator     = [[Charles Dickens]]
| portrayer   = 
| episode     = 
| nickname    = 
| alias       = 
| species     = 
| gender      = Male
| occupation  = Clerk
| title       = 
| family      = 
| significantother = 
| children    = 
| relatives   = 
| religion    = 
| nationality = British
}}

'''John Wemmick''' is a fictional character in [[Charles Dickens]]'s novel ''[[Great Expectations]]''. He is Mr Jaggers's clerk and the protagonist Pip's friend.<ref name="Sparknotes">{{cite web|title=Great Expectations Character List |url=http://www.sparknotes.com/lit/greatex/characters.html |publisher=SparkNotes |accessdate=16 April 2010}}</ref> Some scholars consider him to be the "most modern man in the book".<ref name="Desner">{{cite web|title=Great Expectations: The Tragic Comedy of John Wemmick |url=http://ariel.synergiesprairies.ca/ariel/index.php/ariel/article/view/987/961 |publisher=University of Calgary Press |accessdate=16 April 2010}}</ref><ref>Pickrel, Paul. ''Great Expectations.''</ref> Additionally, Wemmick is noted as one of Dickens's "most successful" split characters, insofar as Wemmick's character represents an exploration of the "relationship between public and private spheres in a divided existence".<ref name="Lecker">Lecker, Barbara. ''Studies in English Literature, 1500-1900.'' Rice University.</ref>

==Profession==

John Wemmick is a bill collector for the lawyer Mr Jaggers. The job requires a demanding, uncaring attitude, a personality the working Wemmick takes on. To impress and stay in the favour of his boss, Mr Jaggers, he berates Jaggers's clients with disdain. He is described as having "the same air of knowing something to everybody else's disadvantage, as his master had".<ref name="Lecker" /> His professional attitude contrasts with Wemmick's more outwardly pleasant home and personal life. Jaggers was a self centered man who did not seem to pay Wemmick well, and when Pip tried to buy a boat he had made fun of him, calling the young boy poor.

==Portable property==
Wemmick often ventures to [[Newgate Prison]] to speak with prisoners currently being represented by Jaggers, or already condemned to die after Jaggers's appointment to them. When Wemmick talks to a prisoner that has been condemned to die, he does his best to take whatever valuable artifacts they may have with them off their hands. This he calls their "portable property". Wemmick does this out of a sense of necessity, given his financially challenged status.<ref name="Cliffsnotes">{{cite web|title=Great Expectations: Character Analysis: Jaggers and Wemmick |url=http://www.cliffsnotes.com/study_guide/literature/Great-Expectations-Character-Analysis-Jaggers-and-Wemmick.id-118,pageNum-750.html |publisher=CliffsNotes |accessdate=17 April 2010 |deadurl=yes |archiveurl=https://web.archive.org/web/20100329025547/http://www.cliffsnotes.com/study_guide/literature/Great-Expectations-Character-Analysis-Jaggers-and-Wemmick.id-118,pageNum-750.html |archivedate=March 29, 2010 }}</ref>

==Relation with Pip==

At one point in the novel, Wemmick advises Pip to acquire his benefactor Magwitch's "portable property". He argues that despite Pip's noble intentions to help Magwitch, the pragmatic course of action would be to prepare for failure. In acquiring Magwitch's "portable property," Pip would at least be guaranteed his money. After he sends back Magwitch's pocketbook, Pip feels glad despite Wemmick's advice. In the end, Pip forfeits all that Magwitch intended for him to have.

==Personal life==
[[File:The Aged P., by Sol Eytinge, Jr..jpg|thumb|Mr. Wemmick and "The Aged P.", illustration by [[Sol Eytinge Jr.]]]]
Wemmick owns a house in [[Walworth]] which is modelled as a castle, complete with a drawbridge, cannon and moat. Wemmick feels protected from the harsh realities of his profession by his house. As Wemmick tells Pip, "the office is one thing, and private life is another. When I go into the office, I leave the Castle behind me." He lives with his father, who is referred to as "The Aged Parent", "The Aged P.", or simply "The Aged".  

He is engaged to marry Miss Skiffins, a source of joy in his life. His behaviour with Miss Skiffins is another indication of Wemmick's split character status. In his personal life Wemmick, for the first time, also reveals a "sexuality which Dickens comically depicts in his relationship with the brightly apparelled but wooden Miss Skiffins." Pip approves of Wemmick's behaviour around Miss Skiffins, insofar as it humanizes him. This is contrasted with Pip's observation of Wemmick's behaviour in the presence of Jaggers, which he compares to his behaviour around Miss Skiffins by saying "there were twin Wemmicks and this was the wrong one."<ref name="Lecker2">Lecker, Barbara. ''The Split Characters of Charles Dickens.'' Rice University.</ref>

==References==

<references/>
{{Great Expectations}}

{{DEFAULTSORT:Wemmick, John}}
[[Category:Fictional characters introduced in 1861]]
[[Category:Great Expectations characters]]
[[Category:Fictional clerks]]