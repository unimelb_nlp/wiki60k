{{Infobox military conflict
|conflict    = Battle of Vijithapura
|partof      = [[Dutthagamani]]'s campaign against [[Elara (monarch)|Elara]]
|image       = 
|caption     = 
|date        = 162 or 161 BC
|place       = [[Vijithapura]] (Vijitha Nagara)
|coordinates =
|map_type    = 
|latitude    = 
|longitude   = 
|map_size    = 
|map_caption = 
|map_label   = 
|territory   = City of Vijithapura captured by Dutthagamani's army
|result      = Decisive victory for Dutthagamani's army
|status      = 
|combatant1  = Army of Dutthagamani
|combatant2  = Army of Elara
|combatant3  = 
|commander1  = Dutthagamani
|commander2  = Unknown
|commander3  = 
|strength1   = Unknown
|strength2   = Unknown
|strength3   = 
|casualties1 = Unknown
|casualties2 = Unknown
|casualties3 =
|notes       = 
}}

The '''Battle of Vijithapura''' was a decisive and major battle in the campaign carried out by Sri Lankan king [[Dutthagamani]] against the invading South Indian king [[Elara (monarch)|Elara]]. The battle is documented in detail in the ancient chronicles of the country. However, they only provide the viewpoint of Dutthagamani and his army, and details are scarce on Elara's side.

After launching a campaign to regain the country from Elara, Dutthagamani captured a number of his strongholds before coming to the fortified city of [[Vijithapura]]. A four-month siege ensued, followed by a large assault where Dutthagamani's champions and royal elephant played a major part.<ref>{{cite news |url = http://www.island.lk/2009/08/13/features5.html |title = Elephants a national treasure |last = Edirisuriya |first = Chandra |date = 2009-08-13 |work = The Island |accessdate = 2009-11-05}}</ref> The chronicles focus a lot on these ten champions, and vividly describe some unusual "tests" that Dutthagamani carried out to find out their skills.

The battle ended in victory for Dutthagamani's forces and considerably weakened Elara's army, ultimately leading to his defeat and death. The exact location of Vijithapura is unknown, though historians have made some speculations on this. The battle is still regarded by Sri Lankans as a legendary event in the country's history, and has even been compared with victories of the [[Sri Lanka Army]] during the country's [[Sri Lankan Civil War|civil war]].

== Records ==
Dutthagamani's campaign against Elara is given in detail in the ancient chronicles of [[Sri Lanka]]; ''[[Mahavamsa]]'', ''[[Dipavamsa]]'', ''[[Rajavaliya]]'' and ''[[Thupavamsa]]''. All of them describe the battle in detail, and apply a high importance to it. Dutthagamani is a hero in these chronicles, and his campaign is depicted as a "holy war" aimed at restoring Buddhism in the country. Therefore, these accounts are favourably biased to him, and the description of the Battle of Vijithapura, along with the rest of the campaign, is a mix of fact and legend. However, historians agree that the basic facts from these chronicles are accurate. The one sided accounts given in the chronicles mean that there is very little information to be obtained on Elara and his armies. According to Orientalist [[Wilhelm Geiger]], who translated the ''Mahavamsa'', the problem is "not what is said but what is left unsaid".<ref>Siriweera (2004), p. 31</ref>

== Background ==
At the time of the battle, [[Elara (monarch)|Elara]] was the king of [[Anuradhapura Kingdom|Anuradhapura]]. He was a [[Chola Dynasty|Chola]] prince from [[South India]], who had defeated the Sinhalese ruler [[Asela of Sri Lanka|Asela]] in an invasion. Although an invader, Elara is described as a just ruler who had even patronized [[Buddhism]].<ref name="Siriweera 2004, p. 33">Siriweera (2004), p. 33</ref> Most of the country came under this [[Tamil people|Tamil]] king's rule, while his rival [[Kavan Tissa of Ruhuna|Kavan Tissa]], a [[Sinhala people|Sinhala]] king from [[Kingdom of Ruhuna|Ruhunu]] in the south of the country, organized a resistance against him. Kavan Tissa's son, [[Dutthagamani of Sri Lanka|Dutthagamani]], ascended to the throne after the death of his father.<ref>Siriweera (2004), p. 30</ref>

Soon after he became the king in Ruhuna, Dutthagamani launched a campaign against Elara with the intention of "restoring and glorifying Buddhism" in the country.<ref>Wijesooriya (2006), p. 58</ref> After setting out from [[Magama]] and crossing the [[Mahaweli river]], Dutthagamani captured a number of forts and cities that were under Elara, and killed several of his generals.<ref name="Abesekara 1998, p. 31">Abesekara (1998), p. 31</ref> The ancient chronicles refer to all of the chieftains or generals defeated by Dutthagamani as ''Demalas'' (Tamils). However, it is unlikely that all of them were indeed Tamils, and it is possible that one of them&mdash;whose name is given as Dighabaya&mdash;may even have been a stepbrother of Dutthagamani himself who had later joined Elara.<ref>Siriweera (2004), p. 32</ref>

=== Vijithapura ===
After these victories, Dutthagamani's army marched on to the "great fortress of [[Vijithapura]]".<ref name="Abesekara 1998, p. 31"/> Dutthagamani followed a road between [[Sigiriya]] and [[Minneriya]] to take his army there; a road that had been used by [[Pandukabhaya]], a previous ruler, in his military campaigns as well.<ref>Geiger (1994), p. 291</ref>

The city of Vijithapura, which the ''Mahavamsa'' refers to as Vijitha Nagara, had been founded nearly three hundred years ago by the brother in law of king [[Panduvasdeva of Sri Lanka|Panduvasudeva]].<ref name="Wright 1999, p. 24">Wright (1999), p. 24</ref> By the time of the battle, it had become a well-fortified stronghold of Elara. It is said to have been surrounded by three moats and a wall with a height of 18 cubits.<ref name="Moratuwagama 1996, p. 227">Moratuwagama (1996), p. 227</ref> The wall had four wrought iron gates on the north, south, east and west. The ''Rajavaliya'' describes Vijithapura as a fortress second only to [[Anuradhapura]].<ref name="Senaveratna 1997 p. 125">Senaveratna (1997) p. 125</ref>

The control of Vijithapura was essential to both sides. The loss of the stronghold would be a largely demoralizing factor for Elara's forces and would significantly reduce their capability to resist Duthhagamani's advance. For Dutthagamani's forces, the capture of the city would mean that they could easily move on to Anuradhapura.<ref>Senaveratna (1997) p. 124</ref>

== Siege ==
Surviving troops of Elara's forces from previous battles retreated to Vijithapura, further strengthening its defenses.<ref>Moratuwagama (1996), p. 226</ref> Dutthagamani's army also arrived and pitched camp close to the fortress. The open stretch of land where they camped later came to be known as Khandavara Pitthi or Kandavurupitiya.<ref name="Senaveratna 1997 p. 125" /> They carried out regular assaults against the fortress while the defenders also made occasional [[sorties]], but none of them were able to sway the battle in favour of either side. After laying siege on the city for four months, plans were laid to launch an assault using the entire army. Dutthagamani's army was led by his ten champions or generals, known as the "[[Ten Giant Warriors]]", who were to play a significant part in the battle to come.<ref name="Senaveratna 1997 p. 126">Senaveratna (1997) p. 126</ref>

=== Testing the warriors ===
[[File:Flag of Dutthagamani.png|thumb|Dutthagamani's flag.]]

The ancient chronicles mention two tests that Dutthagamani planned to find out these warriors' skill before the battle. For the first test, Dutthagamani asked the warriors to drink a large cauldron of [[Palm wine|toddy]], intending to test their strength. When all others refused, [[Suranimala]] stepped forward and drank the entire cauldron without any effort.<ref>Abesekara (1998), p. 32</ref> The second test was to test [[Ten giant warriors#Nandimithra|Nandimithra]], the commander of the army. Dutthagamani had his royal elephant, [[Kandula (elephant)|Kandula]], infuriated and set on Nandimithra. However, the warrior stood his ground and taking the elephant by its tusks, pushed it to the ground.<ref name="Senaveratna 1997 p. 125" /><ref>Abesekara (1998), p. 33</ref> Thus clearing all doubts as to the abilities and skill of his warriors, Dutthagamani sounded the war drums and raising his flags, started the assault to take Vijithapura.<ref name="Moratuwagama 1996, p. 227"/>

== Final assault ==
Dutthagamani's army attacked all four gates of the city simultaneously. He led the main assault on the southern gate with Nandimithra, Suranimala and the elephant Kandula, while the attacks on the northern and western gates were led by Bharana, Khanjadeva, Phussadeva and Labhiyavasabha. The eastern gate was attacked by Mahasona, Gothaimbara, Theraputthabhaya and Velusumana.<ref name="Senaveratna 1997 p. 126"/> The defenders of the eastern gates were routed by Velusumana after a cavalry attack, and Elara's forces withdrew into the city.<ref name="Moratuwagama 1996, p. 227"/>

Elara's archers, shooting from the walls, inflicted heavy casualties on the attackers, while soldiers on top of the walls prevented any attempt to breach the wall by puring down molten metal on them.<ref name="Moratuwagama 1996, p. 227" /> The elephant Kandula, attempting to break the southern gate, was injured in such an attack. After tending to his injuries and protecting him using thick animal hides, Dutthagamani encouraged Kandula and drove him against the wall. The wall was breached and Dutthagamani's army entered the city.<ref name="Wright 1999, p. 24"/> The ten champions, unwilling to enter through an opening made by another, destroyed the wall themselves in different places and broke into the city.<ref>Moratuwagama (1996), p. 228</ref> Led by them, Dutthagamani's army destroyed the defenders and took control of the fortress city of Vijithapura.<ref>Abesekara (1998), p. 36</ref> The survivors retreated to Anuradhapura.<ref>Senaveratna (1997) p. 129</ref>

== Aftermath ==
The capture of Vijithapura paved the way for Dutthagamani's army to advance on to Anuradhapura, and they proceeded immediately afterwards, capturing two more of Elara's strongholds on the way.<ref>Moratuwagama (1996), p. 229</ref> In the battle for Anuradhapura, Dutthagamani killed Elara in single combat and became the king of Anuradhapura, bringing the entire country under his rule.<ref name="Siriweera 2004, p. 33"/>

== Modern culture and studies ==
[[File:KaduGaGala.jpg|right|thumb|Kadu Ga Gala, Anuradhapura:<br> The stone which may have been used by Dutthagamani's soldiers to sharpen their swords]]

The battle of Vijithapura is a legendary battle in Sri Lankan history and a significant milestone in Dutthagamani's campaign to restore Buddhism in the country. It is often referred to as ''Vijithapura maha satana'' (the great battle of Vijithapura). After the ending of the [[Sri Lankan Civil War]] in 2009, General [[Sarath Fonseka]], the then [[Commander of the Army (Sri Lanka)|commander]] of the [[Sri Lanka Army]], compared several battles they fought to that of Vijithapura.<ref>{{cite news|url=http://www.nation.lk/2009/06/28/militarym.htm |title=Tigers in INGO clothing |last=Perera |first=Tissa Ravindra |date=2009-06-28 |work=The Nation |accessdate=2009-11-05 |archiveurl=https://web.archive.org/web/20091002065343/http://www.nation.lk/2009/06/28/militarym.htm |archivedate=2009-10-02 |deadurl=yes |df= }}</ref>

The exact location of the Vijithapura fortress is uncertain. A village with the same name near the ancient [[Kalawewa]] reservoir may have been the place where the battle took place. There is an ancient temple here as well as a granite stone that locals believe to have been used by Dutthagamani's soldiers to sharpen their swords<ref>
{{cite news |url = http://www.sundayobserver.lk/2007/08/26/tra02.asp |title = The little ocean of Rajarata |last = Perera |first = Supun |date = 2007-08-26 |work = Sunday Observer |accessdate = 2009-11-05}}</ref> However, other historians and archaeologists believe that the location is close to [[Kaduruwela]] near [[Polonnaruwa]], where the ruins of an ancient fortress have been found.<ref>{{cite news|url=http://www.nation.lk/2009/04/12/eye18.html |title=A destiny fulfilled |last=de Silva |first=Theja |date=2009-04-12 |work=The Nation |accessdate=2009-11-05 |archiveurl=https://web.archive.org/web/20091004022249/http://www.nation.lk/2009/04/12/eye18.html |archivedate=2009-10-04 |deadurl=yes |df= }}</ref><ref>Siriweera (2004), p. 107</ref>

== References ==

=== Citations ===
{{Reflist |colwidth = 20em}}

=== Sources ===
{{refbegin}}
* {{cite book |last = Abesekara |first = E. A. |title = ගැමුණු රජ සහ දස මහා යෝධයෝ—Gemunu Raja Saha Dasa Maha Yodhayo (King Gemunu and the Ten Giant Warriors) |publisher = M. D. Gunasena and Company |year = 1998 |ISBN = 955-21-0009-7 |language = Sinhala}}
* {{cite book |last = Geiger |first = Wilhelm |title = Mahāvaṃsa, the great chronicle of Ceylon |publisher = Asian Educational Services |year = 1994 |ISBN = 978-81-206-0218-2 |url = https://books.google.com/books?id=nX2af3kcregC&pg=PA291#v=onepage&q=&f=false}}
* {{cite book |last = Moratuwagama |first = H. M. |title = සිංහල ථුපවංසය—Sinhala Thupavansaya (Sinhala Thupavamsa) |publisher = Rathna Publishers |year = 1996 |ISBN = 955-569-068-5 |language = Sinhala}}
* {{cite book |last = Senaveratna |first = John M. |title = The story of the Sinhalese from the most ancient times up to the end of "the Mahavansa" or Great dynasty: Vijaya to Maha Sena, B.C. 543 to A.D.302 |publisher = Asian Educational Services |year = 1997 |ISBN = 978-81-206-1271-6 |url = https://books.google.com/books?id=X9TeEcMi0e0C&pg=PA124#v=onepage&q=&f=false}}
* {{cite book |last = Siriweera |first = W. I. |title = History of Sri Lanka |publisher = Dayawansa Jayakodi & Company |year = 2004 |ISBN = 955-551-257-4}}
* {{cite book |last = Wijesooriya |first = S. |title = A Concise Sinhala Mahavamsa |publisher = Participatory Development Forum |year = 2006 |ISBN = 955-9140-31-0}}
* {{cite book |last = Wright |first = Arnold |title = Twentieth century impressions of Ceylon: its history, people, commerce, industries, and resources |publisher = Asian Educational Services |year = 1999 |ISBN = 978-81-206-1335-5 |url = https://books.google.lk/books?id=eUF_rS8FEoIC&pg=PA24#v=onepage&q=&f=false}}
{{refend}}

{{Armed Conflicts Involving Sri Lanka}}

{{Good article}}

[[Category:Battles involving Sri Lanka|Vijithapura]]
[[Category:Battles of Antiquity|Vijithapura]]
[[Category:Military history of Sri Lanka|Vijithapura]]
[[Category:2nd-century BC conflicts|Vijithapura]]
[[Category:2nd century BC in Asia]]