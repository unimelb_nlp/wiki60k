{{Use dmy dates|date=April 2012}}
<!-- This article is a part of [[Wikipedia:WikiProject Biography/Military]]. -->
{{Infobox military person
| name = John Herbert Towne Letts
| image =
| caption =
| birth_date = {{Birth date|df=yes|1897|6|10}}
| death_date = {{Death date and age|df=yes|1918|10|11|1897|6|10}}
| birth_place = Lincoln, England
| death_place = Belle Vue Aerodrome, France
| placeofburial_label =
| placeofburial = [[Bac-du-Sud British Cemetery]], [[Bailleulval]], France
| placeofburial_coordinates = {{coord|50|13|30|N|2|36|11|E|display=inline,title}}
| nickname =
| allegiance = United Kingdom
| branch = British Army<br/>Royal Air Force
| serviceyears = 1915–1918
| rank = Captain
| unit = [[Lincolnshire Regiment]]<br/>[[No. 27 Squadron RAF|No. 27 Squadron RFC]]<br/>[[No. 48 Squadron RAF|No. 48 Squadron RFC]]<br/>[[No. 87 Squadron RAF]]
| commands =
| battles = World War I<br/>{{*}}[[Western Front (World War I)|Western Front]]
| awards = [[Military Cross]]
| relations =
| laterwork =
}}
Captain '''John Herbert Towne Letts''' {{post-nominals|country=GBR|MC}} was a British [[World War I|First World War]] [[flying ace]] credited with thirteen confirmed victories.<ref name="theaerodrome">{{cite web |url=http://www.theaerodrome.com/aces/england/letts.php |title=John Herbert Towne Letts |work=The Aerodrome |year=2015 |accessdate=6 May 2015 }}</ref>

==Early life==
Letts was the only child of Walter John Letts, a railway superintendent, and Charlotte Helen (née Robertson) of Steep Hill House, Lincoln. He was educated at Aldeburgh Lodge, Suffolk, Roydon Hall, Norfolk, and at [[Lancing College]], Sussex, where he excelled in sport, representing the school in swimming, football and cricket, and was a sergeant in the [[Officers' Training Corps]].<ref name="lancing">{{cite web |url= http://www.hambo.org/lancing/view_man.php?id=309 |title=John Herbert Towne Letts |work=Lancing College War Memorial |year=2015 |accessdate=6 May 2015}}</ref><ref name="FlightObit">{{cite journal |url=http://www.flightglobal.com/pdfarchive/view/1918/1918%20-%201261.html |title=Personals: Casualties |issue=515 |volume=X |page=1262 |journal=[[Flight International|Flight]] |date=7 November 1918 |accessdate=6 May 2015}}</ref>

==Military service==
In mid-1915 he left school to attend the [[Royal Military College, Sandhurst]],<ref name="FlightObit"/> and was commissioned as a second lieutenant in the [[Lincolnshire Regiment]] on 26 January 1916.<ref>{{London Gazette |date=25 January 1916 |issue=29450 |startpage=1007 |nolink=yes}}</ref> He was immediately seconded to the Royal Flying Corps, and posted to No. 1 Reserve Squadron at [[RAF Gosport]] to begin his flying training. On 19 March he made his first solo flight, in a Maurice Farman [[Farman MF.7|Longhorn]], after only four hours of dual instruction. His second solo flight, later the same day, ended when he crashed into the side of a shed. Letts' first attempt to gain his Royal Aero Club's Aviator's Certificate was abandoned after his engine failed, but he passed on his second attempt the next day, 24 March, flying a Maurice Farman biplane at the Military School in [[Farnborough, Hampshire|Farnborough]].<ref name="lancing"/> Letts was appointed a flying officer on 4 May 1916.<ref>{{London Gazette |date=16 May 1916 |supp=yes |issue=29585 |startpage=4940 |nolink=yes}}</ref>

Following advanced flying training at the [[Central Flying School]] at [[Upavon]], on 15 June Letts was posted to No. 27 Squadron in France, to fly the [[Martinsyde G.100]] fighter-bomber. He was invalided back to England on 11 August with an injured knee, and on 19 October was posted to No. 47 Reserve Squadron at [[RAF Waddington]], Lincolnshire, to serve as an instructor. He was reassigned to No. 48 Squadron, flying the new two-seater [[Bristol F.2 Fighter|Bristol F2b]] fighter, on 12 February 1917, and in March the squadron was sent to France.<ref name="lancing"/> On 5 April Letts was appointed a [[flight commander]] with the rank of temporary captain,<ref>{{London Gazette |date=27 April 1917 |supp=yes |issue=30038 |startpage=4034 |nolink=yes}}</ref> replacing [[William Leefe-Robinson]], who had been shot down and captured.<ref name="lancing"/>

On the afternoon of 9 April Letts and his observer Lieutenant Harold George Collins, accompanied by an aircraft piloted by Captain [[Alan Wilkinson (RAF officer)|Alan Wilkinson]] with 2nd Lieutenant [[Laurence W. Allen]], engaged five aircraft from ''[[Jagdgeschwader 1 (World War I)|Jagdgeschwader 1]]'' — Richthofen's "Flying Circus" — in a [[dogfight]] over [[Arras]]. They jointly claimed two aircraft shot down, even though Letts aircraft was "cut to ribbons", and Collins killed.<ref name="FlightObit"/> Letts next victory came on 4 May, when he and observer 2nd Lieutenant L. Speller shared in the destruction of an [[Albatros D.III]] over [[Pelves]] with pilot 2nd Lieutenant H. Smithers and observer [[Air Mechanic 2nd Class|AM2]] Valentine Reed. On 11 May, in a fight with aircraft from  ''[[Jagdstaffel 11]]'', Letts and Jameson, with Smithers and AM2 Rutherford, shared an Albatros D.III shot down over [[Biache-Saint-Vaast|Biache-Dury]], but Letts' aircraft was badly damaged, and two other British aircraft were also shot down. Letts managed to nurse his aircraft back over the British front lines before crash landing. The next day he claimed two more enemy aircraft, driving down an  Albatros D.III over [[Beaumont-en-Cambrésis|Beaumont]] with 2nd Lt. Jameson in the morning, and another D.III driven down over Izel with Lt. Allen in the afternoon, bringing his total to six.<ref name="theaerodrome"/><ref name="lancing"/>

On 24 May Letts and Allen were attacked by four enemy aircraft over  [[Vitry-en-Artois|Vitry]], and claimed to have destroyed two while driving the other two off, though Allen was wounded. They were officially credited with one victory.<ref name="theaerodrome"/><ref name="lancing"/> Letts was subsequently awarded the [[Military Cross]],<ref name="FlightObit"/> which was [[gazetted]] on 16 August. The citation read:
:2nd Lieutenant (Temporary Captain) John Herbert Towne Letts, Lincolnshire Regiment and Royal Flying Corps.
::"For conspicuous gallantry and devotion to duty. He attacked four large two seaters, driving two down out of control and forcing the remaining two down. He has helped to destroy eight machines, and throughout has set a splendid example."<ref>{{London Gazette |date=14 August 1917 |supp=yes |issue=30234 |startpage=8374 |nolink=yes}}</ref>

Letts, with observer Lieutenant C. A. Malcomson, shared with Smithers and Jameson in the destruction of another Albatros D.III over [[Douai]] on 27 May. On 5 June Letts and Jameson drove down another two-seater over  [[Riencourt]]–[[Cagnicourt]]. On 17 August he and Jameson were attacked by eight Albatros D.IIIs east of [[Nieuwpoort, Belgium|Nieuport]]. The combined fire of Jameson and 2nd Lieutenant L. H. Tanner, the observer of 2nd Lieutenant Alan Craddock Simpson, sent one enemy aircraft down in a spin until it broke apart in mid-air.<ref name="theaerodrome"/><ref name="lancing"/> On 26 July Letts was promoted to lieutenant in his regiment.<ref>{{London Gazette |date=22 February 1918 |supp=yes |issue=30543 |startpage=2470 |nolink=yes}}</ref> On 22 August, flying with observer Lieutenant Harold R. Power, Letts and two other Bristol Fighters intercepted ten [[Gotha bomber]]s off Zeebrugge, returning from a raid on the English coast.<ref name="FlightObit"/> In the battle that followed Power was hit by enemy fire, and the barrel of his gun swung round and struck Letts on the head. Despite suffering from a [[concussion]] he returned to base, discovering on landing that Power was dead. On 4 September Letts and observer 2nd Lieutenant John Frost, destroyed two [[Albatros D.V]]s over [[Ghistelles]], and on 15 September he and Frost pursued and eventually drove down another enemy aircraft north-east of [[Dixmude]].<ref name="theaerodrome"/><ref name="lancing"/>

Letts returned to England on 19 September, briefly serving at the Aeroplane Experimental Station at [[RAF Martlesham Heath|Martlesham Heath]], and then as an instructor at the School of Air Fighting from October 1917. He eventually returned to front-line service at his own request, being posted to No. 42 Training Depot Squadron on 1 October 1918, before returning to France on 10 October to serve as a flight commander in No. 64 Squadron. However on arrival he was as ordered to join No. 87 Squadron to fly the [[Sopwith Dolphin]]. The following day Letts borrowed an [[Royal Aircraft Factory S.E.5|SE.5a]]  from No. 32 Squadron. Shortly after taking off the aircraft rolled, then plunged to earth killing him instantly.<ref name="lancing"/><ref name="FlightObit"/><ref name="ATT">Shores ''et.al.'' (1990), p.238.</ref>

He is buried in grave VI. A.30 in Bac-du-Sud British Cemetery, [[Bailleulval]], [[Pas-de-Calais]], France.<ref>{{cite web |url= http://www.cwgc.org/find-war-dead/casualty/18874/LETTS,%20JOHN%20HERBERT%20TOWNE |title=Casualty Details: Letts, John Herbert Towne |work=Commonwealth War Graves Commission |year=2015 |accessdate=6 May 2015}}</ref><ref>{{cite web |url= http://twgpp.org/information.php?id=2430846 |title=Letts, John Herbert Towne |work=The War Graves Photographic Project |year=2015 |accessdate=6 May 2015}}</ref>

==References==
;Citations
{{reflist|30em}}
;Bibliography
* {{cite book |first1=Christopher F. |last1=Shores |first2=Norman |last2=Franks |authorlink2=Norman Franks |first3=Russell F. |last3=Guest |title=Above the Trenches: a Complete Record of the Fighter Aces and Units of the British Empire Air Forces 1915–1920 |location=London, UK |publisher=Grub Street |year=1990 |isbn=978-0-948817-19-9 |lastauthoramp=yes}}

{{DEFAULTSORT:Letts, John}}
[[Category:1897 births]]
[[Category:1918 deaths]]
[[Category:People from Lincoln, England]]
[[Category:People educated at Lancing College]]
[[Category:Graduates of the Royal Military College, Sandhurst]]
[[Category:Royal Lincolnshire Regiment officers]]
[[Category:Royal Flying Corps officers]]
[[Category:Royal Air Force personnel of World War I]]
[[Category:British World War I flying aces]]
[[Category:Recipients of the Military Cross]]
[[Category:Aviators killed in aviation accidents or incidents in France]]