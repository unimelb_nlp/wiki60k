{{other uses}}
{{infobox book | 
| name          = L'Argent
| title_orig    = 
| translator    = 
| image         = <!--prefer 1st edition-->
| caption = 
| author        = [[Émile Zola]]
| illustrator   = 
| cover_artist  = 
| country       = France
| language      = French
| series        = [[Les Rougon-Macquart]]
| genre         = [[Novel]]
| publisher     = Charpentier & Fasquelle (book form)
| release_date  = 1890-1891 (serial) & 1891 (book form)
| english_release_date =
| media_type    = Print ([[Serial (literature)|Serial]], [[Hardcover|Hardback]] & [[Paperback]])
| pages         = 
| preceded_by   = [[La Bête Humaine]]
| followed_by   = [[La Débâcle]]
}}
'''''L'Argent''''' ("Money") is the eighteenth novel in the [[Les Rougon-Macquart|Rougon-Macquart]] series by [[Émile Zola]]. It was serialized in the periodical ''[[Gil Blas (periodical)|Gil Blas]]'' beginning in November 1890 before being published in novel form by Charpentier et Fasquelle in March 1891.

The novel focuses on the financial world of the [[Second French Empire]] as embodied in the [[Paris]] [[Paris Bourse|Bourse]] and exemplified by the fictional character of Aristide Saccard. Zola's intent was to show the terrible effects of [[speculation]] and fraudulent company promotion, the culpable negligence of company directors, and the impotency of contemporary financial laws.

Aristide Saccard (b. 1815 as Aristide Rougon) is the youngest son of Pierre and Félicité Rougon. He is first introduced in ''[[La Fortune des Rougon|La fortune des Rougon]].'' ''L'argent'' is a direct sequel to ''[[La Curée|La curée]]'' (published in 1871), which details Saccard's first rise to wealth using underhanded methods. Sensing his unscrupulous nature, his brother Eugène Rougon prompts Aristide to change his surname from Rougon to Saccard.

Aristide's other brother Pascal is the main character of ''[[Le Docteur Pascal|Le docteur Pascal]]''. He also has two sisters: Sidonie, who appears in ''La curée'', and Marthe, one of the protagonists of ''[[La Conquête de Plassans|La conquête de Plassans]].''

==Plot summary==

The novel takes place in 1864-1869, beginning a few months after the death of Saccard's second wife Renée (see ''La curée''). Saccard is [[bankruptcy|bankrupt]] and an outcast among the Bourse [[financier]]s. Searching for a way to reestablish himself, Saccard is struck by plans developed by his upstairs neighbor, the engineer Georges Hamelin, who dreams of restoring [[Christianity]] to the [[Middle East]] through great public works: rail lines linking important cities, improved roads and transportation, renovated eastern [[Mediterranean Sea|Mediterranean]] ports, and fleets of modern ships to move goods around the world.

Saccard decides to institute a financial establishment to fund these projects. He is motivated primarily by the potential to make incredible amounts of money and reestablish himself on the Bourse. In addition, Saccard has an intense rivalry with his brother Eugène Rougon, a powerful Cabinet minister who refuses to help him after his bankruptcy and who is promoting a more liberal, less [[Roman Catholic Church|Catholic]] agenda for the Empire. Furthermore, Saccard, an intense [[Antisemitism|anti-Semite]], sees the enterprise as a strike against the [[Jew]]ish bankers who dominate the Bourse. 
From the beginning, Saccard's Banque Universelle (Universal Bank) stands on shaky ground. In order to manipulate the price of the stock, Saccard and his confreres on the syndicate he has set up to jumpstart the enterprise buy their own stock and hide the proceeds of this illegal practice in a dummy account fronted by a [[Straw man (law)|straw man]].

While Hamelin travels to [[Istanbul|Constantinople]] to lay the groundwork for their enterprise, the Banque Universelle goes from strength to strength. Stock prices soar, going from 500 francs a share to more than 3,000 francs in three years. Furthermore, Saccard buys several newspapers which serve to maintain the illusion of legitimacy, promote the Banque, excite the public, and attack Rougon.

The novel follows the fortunes of about 20 characters, cutting across all social strata, showing the effects of stock market speculation on rich and poor. The financial events of the novel are played against Saccard's personal life. Hamelin lives with his sister Caroline, who, against her better judgment, invests in the Banque Universelle and later becomes Saccard's mistress. Caroline learns that Saccard fathered a son, Victor, during his first days in Paris. She rescues Victor from his life of abject poverty, placing him in a charitable institution. But Victor is completely unredeemable, given over to greed, laziness, and thievery. After he attacks one of the women at the institution, he disappears into the streets, never to be seen again.

Eventually, the Banque Universelle cannot sustain itself. Saccard's principal rival on the Bourse, the Jewish financier Gundermann, learns about Saccard's financial trickery and attacks, losing stock upon the market, devaluing its price, and forcing Saccard to buy millions of shares to keep the price up. At the final collapse, the Banque holds one-fourth of its own shares worth 200 million francs. The fall of the Banque is felt across the entire financial world. Indeed, all of France feels the force of its collapse. The effects on the characters of ''L'argent'' are disastrous, including complete ruin, suicide, and exile, though some of Saccard's syndicate members escape and Gundermann experiences a windfall. Saccard and Hamelin are sentenced to five years in prison. Through the intervention of Saccard's brother Eugène Rougon, who doesn't want a brother in jail, their sentences are commuted and they are forced to leave France. Saccard goes to [[Belgium]], and the novel ends with Caroline preparing to follow her brother to [[Rome]].

==Historical background==
Because the financial world is closely linked with politics, ''L’argent'' encompasses many historical events, including:
*The 1860 [[Druze]] massacre of [[Maronite Church|Maronite Christians]] in [[Syria]]
*France's [[French intervention in Mexico|invasion]] of [[Mexico]] (1861–1867)
*The construction of the [[Suez Canal]], opened in 1869
*The [[Austro-Prussian War]], including the [[Battle of Königgrätz]] at [[Sadová|Sadowa]] in 1866
*The [[Third Italian War of Independence]] (1866)
*The [[Exposition Universelle (1867)|Universal Exposition]] of 1867
*The publication of ''[[Das Kapital]]'' in 1867 and the advent of [[Marxism]]
*The 1882 collapse of the [[Union Générale]] bank

By the end of the novel, the stage is set for the [[Franco-Prussian War]] (1870–1871) and the fall of the Second Empire.

== Relation to the Other Rougon-Macquart Novels ==

Zola's plan for the Rougon-Macquart novels was to show how [[heredity]] and environment worked on members of one family over the course of the [[Second French Empire|Second Empire]]. All of the descendants of Adelaïde Fouque (Tante Dide), Saccard's grandmother, demonstrate what today would be called [[Obsessive-compulsive disorder|obsessive-compulsive]] behaviors to varying degrees. Saccard is obsessed with money and the building of wealth, to which everything in his life holds second place. In ''Le docteur Pascal,'' Zola describes the influence of heredity on Saccard as an "adjection" in which the natures of his avaricious parents are commingled.

Two other members of the Rougon-Macquart family also appear in ''L'argent'': Saccard's sons Maxime (b. 1840) and Victor (b. 1853). If his father's obsession is with building wealth, Maxime's obsession is with keeping it. A widower, Maxime (who played a central role in ''La curée'') lives alone in opulence he does not share. In ''Le docteur Pascal'', Maxime is described as prematurely aged, afraid of pleasure and indeed of all life, devoid of emotion, and cold, characteristics introduced in ''L'argent''. Maxime is described as a "dissemination" of characteristics, having the moral prepotency of his father and the pampered [[egotism]] of his mother (Saccard's first wife).

Victor, on the other hand, brought up in squalor, is the furthest extreme Zola illustrates of the Rougon family's degeneracy. Like his great-grandmother Tante Dide, Victor suffers from [[Neuralgia|neuralgic]] attacks. Unlike Jacques Lantier (his second cousin, see ''[[La Bête Humaine|La bête humaine]]''), he is unable to control his criminal impulses, and his disappearance into the streets of Paris is no surprise. Victor is described as a "fusion" of the lowest characteristics of his parents (his mother was a [[prostitution|prostitute]]).

In ''Le docteur Pascal'' (set in 1872), Zola tells us that Saccard returns to Paris, institutes a newspaper, and is again making piles of money.

Rougon is the protagonist of ''[[Son Excellence Eugène Rougon]],'' the events of which predate ''L'argent.'' Saccard's daughter Clotilde (b. 1847) is the main female character in ''Le docteur Pascal.''

==Translations==
''L'Argent'' has had the following translations into English:
* ''Money'', translated by [[Benjamin Tucker]], 1891
* ''Money'', translated by [[Ernest A. Vizetelly]], 1894 (new edition 1904; reprinted 1991 and 2007)
* ''Money'', translated by Valerie Minogue, 2014, published by [[Oxford University Press]] in their Oxford World's Classics series

==Adaptations==
*In 1928, a [[silent film]] adaptation of ''[[L'Argent (1928 film)|L'Argent]]'' was directed by [[Marcel L'Herbier]]. It used only the skeleton of the plot and it updated the setting to Paris in the 1920s.
* The 1936 French film ''[[L'Argent (1936 film)|L'Argent]]'' was directed by [[Pierre Billon (director)|Pierre Billon]].
*A three-part television adaptation of the book was directed by [[Jacques Rouffio]] for French TV in 1988.
*Shunt, a British theatre company, in 2009 created a piece of immersive theatre loosely based on the book, called 'Money'.

==References==
*Brown, F. ''Zola: A Life'' (New York: [[Farrar, Straus & Giroux]], 1995)
*Zola, E. ''L'argent'', translated as ''Money'' by E. A. Vizetelly (1894)
*Zola, E. ''Le doctor Pascal'', translated as ''Doctor Pascal'' by E. A. Vizetelly (1893)

==External links==
{{Gutenberg|no=17516|name=L'argent}} (French)
*{{IMDb title|id=0090657|title=L'Argent}} (1988) (TV) (French) featuring [[Miou-Miou]]
*[http://www.mondialbooks.com/emile-zola-novels/emile-zola-money-rougon-macquart-preface-vizetelly.html Preface] to the original English translation (1928) by Ernest A. Vizetelly
*[https://books.google.com/books?id=w71EAAAAYAAJ&printsec=frontcover&dq=zola+money&cd=2#v=onepage&q=&f=false  Money] at Google Books.  Full original English translation (1902) by Ernest A. Vizetelly
*[http://www.communitywalk.com/largent_map/map/1554772 L'Argent Map]

{{Les Rougon-Macquart}}

{{Authority control}}

{{DEFAULTSORT:Argent, L'}}
[[Category:1891 novels]]
[[Category:Novels by Émile Zola]]
[[Category:Books of Les Rougon-Macquart]]
[[Category:Novels first published in serial form]]
[[Category:Works originally published in Gil Blas (periodical)]]
[[Category:1860s in fiction]]
[[Category:French novels adapted into films]]