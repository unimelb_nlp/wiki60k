{{no footnotes|date=December 2014}}
{{Use dmy dates|date=September 2012}}
{{Infobox locomotive
| name             	= Barry Railway Class K
| powertype        	= Steam
| image            	= 
| alt              	= 
| caption          	= 
| designer         	= J. H. Hosgood
| builder          	= [[Cooke Locomotive and Machine Works]], Paterson, New Jersey, USA
| ordernumber      	= 
| serialnumber     	= 
| buildmodel       	= 
| builddate        	= 1899
| totalproduction  	= 5
| rebuilder        	= 
| rebuilddate      	= 
| numberrebuilt    	= 
| whytetype        	= [[0-6-2T]]
| uicclass         	= C1 n2t
| gauge            	=  {{track gauge|uksg|allk=on}}
| driverdiameter   	= {{convert|4|ft|3|in|m|3|abbr=on}}
| trailingdiameter 	= {{convert|3|ft|6|in|m|3|abbr=on}}
| minimumcurve     	= 
| wheelbase        	= {{convert|15|ft|5|in|m|3|abbr=on}}
| length           	= 
| width            	= 
| height           	= 
| axleload         	= 
| weightondrivers  	= 
| locoweight       	= {{long ton|56|5}} ({{convert|56|LT|5|Lcwt|ST|1|abbr=on|disp=output only}})
| fueltype         	= [[Coal]]
| fuelcap          	= 
| watercap         	= 
| sandcap          	= 
| boiler           	= 
| boilerpressure   	= {{convert|160|psi|MPa|2|abbr=on}}
| feedwaterheater  	= 
| firearea         	= 
| tubearea         	= 
| fluearea         	= 
| tubesandflues    	= 
| fireboxarea      	= 
| totalsurface     	= 
| superheatertype  	= 
| superheaterarea  	= 
| cylindercount    	= Two outside
| cylindersize     	= {{convert|18|x|26|in|0|abbr=on}}
| valvegear        	= 
| valvetype        	= [[Richardson balanced slide valve|Richardson]]  
| valvetravel      	= 
| valvelap         	= 
| valvelead        	= 
| transmission     	= 
| maxspeed         	= 
| poweroutput      	= 
| tractiveeffort   	= {{convert|22030|lbf|kN|2|abbr=on}}
| factorofadhesion 	= 
| trainheating     	= 
| locobrakes       	= 
| locobrakeforce   	= 
| trainbrakes      	= 
| safety           	= 
| operator         	= {{ubl|[[Barry Railway]]| → [[Great Western Railway]]}}
| operatorclass    	= 
| powerclass       	= 
| numinclass       	= 
| fleetnumbers       	= 
| officialname     	= 
| nicknames        	= Yankees
| axleloadclass    	= 
| locale           	= 
| deliverydate     	= 1899
| firstrundate     	= 
| lastrundate      	= 
| withdrawndate    	= 1927–1932
| scrapdate        	= 
| disposition      	= All [[scrap]]ped
| notes            	=
}}

'''Barry Railway Class K''' were [[0-6-2T]] steam [[tank engine]]s of the [[Barry Railway]] in [[South Wales]].  They were designed by [[J. H. Hosgood]] and built by an American company, [[Cooke Locomotive and Machine Works]] of [[Paterson, New Jersey]].  At the time the Barry wanted to order these locomotives, British manufacturers already had a full order book.  In order not to face an indefinite wait, invitations to tender were advertised in the United States.  Hosgood’s aim was to have a tank engine equivalent to the “[[Barry Railway Class B1|Class B1]]”.  However, because of his desire for a speedy delivery, he agreed to certain compromises in the design.  The order was placed in April 1899 and was delivered later that year.

==Traffic duties==
Although originally intended for hauling main line mineral traffic, they proved to be very heavy on coal and water and therefore not a feasible prospect for this kind of work.  They were therefore assigned other duties.  Two of the class were sent to Hafod shed for banking duties on trains on the gradients between Trehafod Junction and Pontypridd and between Treforest Junction and Tonteg.  The other three were assigned to hauling coal trains between Cadoxton Yard and Barry Docks.  Later on, two of these were assigned to Hafod, joining the first two, for banking duties and the fifth was retained at Barry as shed pilot.

==Heavy on coal and water==
When tests were originally carried out, it was found necessary to stop two or three times while taking empty wagons up to the Rhondda.  According to one driver, it was not advisable to pass a single water column for fear of running short before the next one.

==Special train==
Every year, on Good Friday, the Directors would organise an orchestral concert in Barry and arrange a special train from Trehafod to carry the company’s employees and their families to the concert.  As the “K Class” was vacuum fitted, they were the only engines stationed at Hafod shed suitably equipped to haul a passenger train.  This tradition took place in the early 1900s.

==Withdrawal==
The locomotives passed to the [[Great Western Railway]] in 1922 but were withdrawn between 1927 and 1932.  None survived into [[British Railways]] ownership and none have been preserved.

==Numbering==
{| class="wikitable" style=text-align:center
! Year !! Quantity !! Manufacturer !! Serial numbers !! Barry numbers !! GWR numbers !! Notes
|-
| 1899 || 5 || [[Cooke Locomotive and Machine Works]] || 2482–2486 || 117–121 || 193–197 || 
|}

==References==
{{reflist}}

*{{cite book |last=Barrie |first= D. S. M. |title=The Barry Railway (reprint with addenda and amendments) |publisher=Oakwood Press |year=1983 |page=198 |isbn=0853612366 |ref=harv}}
*{{RCTS-LocosGWR-10 |pages=K46–K47}}
*{{cite book |title=Rails to Prosperity – The Barry & After 1884–1984 |last=Miller |first=Brian J. |publisher=Regional Publications (Bristol) Ltd |year=1984 |pages=18–19 |isbn=0906570174 |ref=harv}}
*{{cite book |title=The Barry Railway – Diagrams and Photographs of Locomotives, Coaches and Wagons |last=Mountford |first=Eric R. |publisher=Oakwood Press |location=Headington |year=1987 |page=18 |isbn=0853613559 |ref=harv}}
*{{cite book |title=Great Western Absorbed Engines |last=Russell |first=J. H. |publisher=Oxford Publishing Company |year=1978 |pages=45–46 |isbn=0902888749 |ref=harv}}


{{GWR Locomotives}}
{{GWR absorbed locos 1922 on}}

[[Category:Barry Railway locomotives|K]]
[[Category:0-6-2T locomotives]]
[[Category:Cooke locomotives]]
[[Category:Railway locomotives introduced in 1899]]
[[Category:Standard gauge steam locomotives of Great Britain]]
[[Category:Scrapped locomotives]]