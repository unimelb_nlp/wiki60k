{{Use dmy dates|date=February 2013}}

'''Mulesing''' is the removal of strips of wool-bearing skin from around the breech ([[buttocks]]) of a sheep to prevent [[Myiasis|flystrike (myiasis)]].<ref name="Model Code">
{{cite book
|url = http://www.publish.csiro.au/nid/22/pid/5389.htm
|format = PDF
|title = The Sheep
|edition = 2nd
|series = Primary Industries Report Series
|pages = 17–23
|work = Model Code of Practice for the Welfare of Animals
|publisher = [[CSIRO]] Publishing
|author = Primary Industries Ministerial Council
|year=2006 |accessdate=2008-03-01
|isbn = 0-643-09357-5}}</ref>
The wool around the buttocks can retain feces and urine, which attracts flies. The
scar tissue that grows over the wound does not grow wool, so is less likely to attract the flies that cause flystrike. Mulesing is a common practice in Australia for this purpose, particularly on highly wrinkled [[Merino]] sheep.<ref name="Model Code"/> Mulesing is considered by some to be a skilled [[Surgery|surgical]] task<ref name = "DPI SOP">{{cite web
|url = http://www.dpi.nsw.gov.au/agriculture/livestock/animal-welfare/general/other/livestock/sop/sheep/mulesing
|title = Standard Operating Procedures – sheep Mulesing
|publisher = New South Wales Department of Primary Industries
|accessdate = 2008-03-01}}</ref> although it may be performed by unskilled persons. Mulesing can only affect flystrike on the area cut out and has no effect on flystrike on any other part of the animal's body.

Mulesing is a controversial practice. The [[National Farmers Federation]] says that "mulesing remains the most effective practical way to eliminate the risk of 'flystrike' in sheep" and that "without mulesing up to 3,000,000 sheep a year could die a slow and agonising death from flystrike".<ref name="NFF">{{cite web|url=http://www.nff.org.au/get/2432305991.doc|title=Mulesing of Sheep
|publisher=National Farmers Federation
| date= |accessdate=2008-09-10}}</ref> The [[Australian Veterinary Association]] (AVA) "recognises the welfare implications of mulesing of sheep. However, in the absence of more humane alternatives for preventing breech strike, the AVA accepts that the practice of mulesing should continue as a sheep husbandry procedure". The AVA also supports the use of [[analgesics]] and the accreditation of mulesing practitioners.<ref name="AVA">{{cite web|url=http://avacms.eseries.hengesystems.com.au/AM/Template.cfm?Section=Search&template=/CM/HTMLDisplay.cfm&ContentID=6301 |title=AVA policies
|publisher=Australian Veterinary Association
| date= |accessdate=2008-09-10}}</ref> The [[RSPCA Australia|Australian Royal Society for the Prevention of Cruelty to Animals]] accepts mulesing when the risk of flystrike is very high, when it is done properly, and even then only as a last resort.<ref>{{cite web|title=What is mulesing and what are the alternatives?|url=http://kb.rspca.org.au/What-is-mulesing-and-what-are-the-alternatives_113.html|date= 30 April 2010|accessdate=12 April 2011|publisher=[[RSPCA Australia]]}}</ref> The [[animal rights]] organisation [[PETA]] strongly opposes mulesing, says the practice is cruel and painful, and that more humane alternatives exist,<ref name="AGE_ridicule">{{cite news |first=Jesse |last=Hogan |title=Farmers ridicule US wool ban |url=http://www.theage.com.au/articles/2004/10/15/1097784011310.html |publisher=''[[The Age]]'' |location= Melbourne|date=15 October 2004 |accessdate=2008-03-01 }}</ref> and claim that sheep can be spared maggot infestation through more humane methods, including special diets and spray washing.<ref>http://www.peta.org/issues/animals-used-for-clothing/mulesing.aspx</ref>

In July 2009, representatives of the [[Australian wool industry]] scrapped an earlier promise, made in November 2004, to phase out the practice of mulesing in Australia by 31 December 2010.<ref name = "Model Code"/><ref name=Countryman-2009-07>{{cite web
|title=AWI scraps mulesing deadline
|url=http://www.countryman.com.au/article/2581.html
|author=Bob Garnant
|work=Countryman
|publisher=West Australian Newspapers Pty Ltd
|date=30 July 2009
|accessdate = 2010-01-18}}  {{webarchive |url=https://web.archive.org/web/20091018045528/http://www.countryman.com.au/article/2581.html |date=18 October 2009 }}</ref><ref name="AWGA2004">{{cite web | url=http://www.australianwoolgrowers.com.au/news2004/news081104.html | title = In the News | date = 8 November 2004 | accessdate = 2007-01-09 | author = Peter Wilkinson | publisher = Australian Wool Growers Association |archiveurl = https://web.archive.org/web/20060924000727/http://www.australianwoolgrowers.com.au/news2004/news081104.html |archivedate = 24 September 2006}}</ref> Mulesing is being phased out in [[New Zealand]].<ref name="ABC - NZ Bare Breech">{{cite news|last1=de Landgrafft|first1=Tara|title=New Zealand farmers on the ball with bare breech breeding|url=http://www.abc.net.au/site-archive/rural/wa/content/2006/s1905645.htm|accessdate=4 February 2017|work=ABC Rural - Country Hour|agency=Australian Broadcasting Corp.|date=24 April 2007}}</ref>

==History==
Mulesing is named after John WH Mules, who developed the practice.<ref name="BBC_row">{{cite news | url=http://news.bbc.co.uk/2/hi/programmes/crossing_continents/4699931.stm | title=Australian wool in animal rights row | publisher=''[[BBC]]'' | date=20 July 2005 | accessdate=2007-11-07 | first=Sue | last=Ellis}}</ref> While shearing a [[Domestic sheep|ewe]] which had suffered several [[flystrike]]s, Mules's hand slipped and his [[Sheep shearing|blade shears]] removed some skin from her hind end. After performing this procedure on his other sheep, Mules noticed that it prevented the occurrence of flystrike. The procedure was refined, experimented with, and demonstrated to reduce flystrike. It was approved for use in Australia in the 1930s. In Australia, it is thought that the fly primarily responsible for flystrike, ''[[Lucilia cuprina]]'', was introduced from South Africa in the nineteenth century.<ref name = "AWI Blowfly">{{cite book |title = Battling the blowfly – plan for the future |url = http://www.wool.com.au/mediaLibrary/attachments/Publications/insight_Blowfly_211106.pdf |format = pdf | date = 3 June 2006| accessdate = 2007-01-09 | ISBN = 1-920908-21-8 | author = Jules Dorrian | publisher = Australian Wool Innovation }}</ref>

Originally, mulesing was carried out on sheep after they were [[wean]]ed because it was considered "too rough" for lambs. However, lambs appear to cope with the procedure better than older sheep as the actual area of [[skin fold]] removed on young lambs is quite small, and they are protected for an extra year as well. For young lambs older than two months, the discomfort period seems to last for approximately two weeks by which time healing is almost, if not entirely, complete. Current codes of practice ban mulesing for sheep over 12 months of age.<ref name = "Model Code" />

==Method==
Mulesing is a procedure which, in Australia, is carried out by a person who has completed the mandatory accreditation and training programme, usually a professional mulesing contractor.<ref name = "Model Code"/>

While the lamb is under restraint (typically in a marking cradle), the wrinkled skin in the animal's breech ([[rump (animal)|rump]] area) is cut away from the perianal region down to the top of the hindlimbs. Originally, the procedure was typically performed with modified wool-trimming metal shears, but now there are similar metal shears designed specifically for mulesing. In addition, the tail is docked and the remaining stump is sometimes skinned.<ref name="sheepguide">{{cite book |title= Sheep Production Guide |last=Livestock & Grain Producers Assoc. |year=1978 |publisher= Macarthur Press |location=Parramatta }}</ref> The cuts are executed to avoid affecting underlying muscle tissue.

The [[New South Wales]] Department of Primary Industries states in the Standard Operating Procedures that, "While the operation causes some pain, no pre or post operative pain relief measures are used". [[Antiseptics]], [[anaesthesia]] and [[painkillers]] are not required by Australian law during or after the procedure but are often applied, as the procedure is known to be painful to the animal.<ref name = "Model Code"/><ref name = "DPI SOP"/> Products have been approved for pain relief during the procedure, including Tri-Solfen. The minor use permit for Tri-Solfen<ref>http://permits.apvma.gov.au/PER8660.PDF</ref> makes the product available for use by both veterinarians and sheep industry employees, such as mulesing contractors and graziers.<ref name="AVA 2005">{{cite web |url=http://www.ava.com.au/news.php?c=0&action=show&news_id=120 |title = AVA welcomes mulesing pain relief | author = Eddie Ripard | date = 29 August 2005 | accessdate=2007-01-10 | publisher = Australian Veterinary Association |archiveurl = https://web.archive.org/web/20060927185238/http://www.ava.com.au/news.php?c=0&action=show&news_id=120 |archivedate = 27 September 2006}}</ref>

After a heavy mules, non-wooled skin around the [[anus]] (and [[vulva]] in ewes) is pulled tight, the cut heals and results in smooth scar tissue that does not get fouled by [[feces|fæces]] or [[urine]]. Most sheep have a light mules which does not leave the skin bare, but simply removes the skin wrinkle leaving a reduced area to grow wool and stain.<ref name="sheepguide" />

When managed according to the standards, policies and procedures developed by the [[CSIRO]], [[domestic sheep|lamb]]s are normally mulesed a few weeks after birth. The operation usually takes less than a minute. Standard practice is to do this operation simultaneously with other procedures such as [[Earmark (agriculture)|ear marking]], [[docking (animal)|tail docking]], and [[vaccination]]. Because the procedure removes skin, not any underlying flesh or structure, there is little blood loss from the cut other than a minor oozing on the edges of the cut skin.

Mulesed lambs should be released onto clean [[pasture]]. The ewes and suckling lambs should receive minimal disturbance until all wounds are completely healed (about four weeks). Observation should be carried out from a distance.<ref name = "Model Code"/>

Mulesing should be completed well before the flystrike season, or else chemical protection should be provided to reduce risk to the lambs and ewes.

Lambs that are slaughtered soon after weaning generally do not need mulesing because they can be protected by chemical treatment for the short time they are at risk.<ref>
{{cite journal
|author1=Morley, F.H.W. |author2=Johnstone, I.L.
 |lastauthoramp=yes | title = Mulesing operation-a review of development and adaptation.
| publisher = Proceedings of the Second National Symposium-Sheep Blowfly and FlyStrike in Sheep, Sydney.
| year = 1983
}}</ref>

==Comparison to crutching==
Mulesing is different from [[crutching]]. Crutching is the mechanical removal of wool around the tail, [[anus]] (and [[vulva]] in ewes) in breeds of sheep with woolly points where this is necessary. Mulesing is the removal of skin to provide permanent resistance to breech strike in [[Merino]] sheep. Other breeds tend to have less loose skin, and wool, so close to the tail and may have less dense wool.

Crutching has to be repeated at regular intervals as the wool grows continuously. Frequent crutching of Merinos reduces the incidence of flystrike, but not as much as mulesing.{{Citation needed|date=July 2009}}

At the time mulesing was invented, crutching was done with blade shears. In Australia, these have been almost universally replaced with [[sheep shearing|machine shears]]. Hand shears were being used when Mules inadvertently carried out the procedure during crutching. Mulesing would not inadvertently occur using modern machine shears.

==Opposition to mulesing==
Some [[animal rights]] activists consider unanesthetised mulesing to be inhumane and unnecessary. They have also argued that mulesing may mask genetic susceptibility to flystrike allowing for genetic weaknesses to be continued.<ref name="Animal Liberation">{{cite web | title=Mulesing | url=http://www.animalliberation.com.au/issues/mulesing.html | publisher = Animal Liberation (WA) Inc | accessdate = 2007-01-09}}</ref>

Proponents of mulesing are largely from Australia where severe and often fatal flystrike is common.

In October 2004, American fashion retailer [[Abercrombie & Fitch Co.]] responded to pressure from PETA to boycott Australian merino wool due in part to the use of mulesing in Australia. The National Farmers' Federation responded by stating "Abercrombie and Fitch does not use Australian wool".<ref name="AGE_ridicule" /> Then, in December 2008, one of the world's largest retailers, [[Liz Claiborne]], (in which PETA is a shareholder)<ref>Land Newspaper, "PETA pulling Wool", p. 18, Rural Press, Richmond, 18 December 2008</ref> banned the use of Australian Merino wool in its products in opposition to the mulesing practice, at the time an Australian Wool Innovations spokesman said "the company did not purchase any Australian wool".<ref>[http://sl.farmonline.com.au/news/nationalrural/wool/general/peta-claims-another-retailer-joins-wool-boycott/1388691.aspx PETA claims another retailer joins wool boycott]</ref> In June 2009, British department store chain John Lewis joined the wool boycott.<ref>{{cite news|url=http://news.smh.com.au/breaking-news-national/uk-retailer-bans-aussie-mulesing-wool-20100621-yrn9.html|title=UK retailer bans Aussie mulesing wool|date= 21 June 2010|accessdate=21 June 2010|work=The Sydney Morning Herald|first=Petrina|last=Berry}}</ref> The international fashion retailer New Look also refuses to stock products made from Australian Merino wool.<ref>[http://www.newlookgroup.com/sites/default/files/attachments/animal_protection_and_product_testing_policy_0.pdf New Look Animal Welfare and Animal Protection Policy] 9 May 2012</ref> The campaign by PETA also seeks to draw attention to Australia's [[live export|live sheep export trade]]. PETA's campaign has hurt the Australian wool industry with several American and European clothing retailers agreeing to the boycott.<ref name = "Pink2">{{cite news | url=http://www.smh.com.au/news/fashion/redfaced-pinks-uturn/2007/01/17/1168709798426.html | title=Red-faced Pink's u-turn on wool ban | date=17 January 2007 | accessdate=2007-01-17 | author=[[Australian Associated Press]] | publisher=The [[Sydney Morning Herald]]}}</ref><ref>{{cite journal | title=Helsinki protest against Australian cruelty to sheep | journal=Blog.anta.net | url=http://blog.anta.net/2008/03/26/helsinki-protest-against-australian-cruelty-to-sheep}} (ed. Thor Kottelin)</ref><ref name="wool-industry-granted-reprieve">{{cite news | url=http://www.theage.com.au/news/national/wool-industry-granted-reprieve/2008/03/19/1205602488530.html |title=Wool industry granted reprieve | date=20 March 2008 | author=Lorna Edwards | accessdate=2009-07-08 | publisher=[[The Age]] | location=Melbourne}}</ref>

Australian interior furnishing wholesaler Instyle Contract Textiles endorses the cessation of mulesing. In early 2008, the company signed an exclusive worldwide agreement with The SRS Company to source wool from non-mulesed Merino sheep that have been bred specifically to be naturally resistant to [[flystrike]].<ref>[http://www.instyle.com.au/news/news_mulesing.html Instyle signs first ethical + environmental wool supply agreement]</ref><ref>Staton, J: "Ethical Wool Emerging, but what is it?", p. 23, Australian Farm Journal, September 2008</ref>

The controversy reignited after a television programme aired in [[Sweden]]. This programme alleged that a lobbying consultant, Kevin Craig, acting on behalf of the Australian Wool and Sheep Industry Taskforce offered a Swedish activist a free trip to Australia if the activist agreed not to go on camera nor do an interview. As a consequence, all clothing manufacturers and retailers in Sweden banned the purchase of wool from sheep that have been mulesed.{{Citation needed|date=December 2008}}  Since then, the Swedish Agriculture Minister, Eskil Erlandsson, has said that "he was satisfied that Australia appeared to be responding to international concerns about mulesing and that bans or boycotts were not necessary". "But in the long run we hope there is going to be a final end to all sorts of mulesing."<ref name="wool-industry-granted-reprieve" />

Some European retailers have agreed to lift their ban on Australian merino wool if pain relief is used during mulesing. The retailers have not been named in an effort to avoid any backlash.<ref name="wool-industry-granted-reprieve" />

In order to help comply with the 2010 deadline to phase out mulesing, Western Australia's governmental research stations ceased mulesing their sheep on 1 April 2008.<ref name="WA">{{cite news|title=WA mulesing ends in three weeks |url=http://www.news.com.au/heraldsun/story/0,21985,23349611-5005961,00.html |publisher=[[Herald Sun]] |date=10 March 2008 |accessdate=2008-03-10 |deadurl=yes |archiveurl=https://web.archive.org/web/20080315045211/http://www.news.com.au/heraldsun/story/0,21985,23349611-5005961,00.html |archivedate=15 March 2008 }}</ref>

The Australian Wool Innovation (AWI) has pledged to phase out mulesing by 2010, but PETA has accused the Australian wool industry of trying to extend this deadline.<ref>[http://www.weeklytimesnow.com.au/article/2008/11/26/29891_latest-news.html PETA warns on mulesing delay]</ref> On 27 July 2009, the Australian wool industry dumped its long-standing pledge to phase out mulesing by the end of 2010, a move that was harshly criticized by animal welfare groups and led to criticism by some farmers.<ref name="AGE">{{cite news | title=Mulesing deadline abandoned| url=http://www.theage.com.au/national/mulesing-deadline-abandoned-20090727-dytn.html | publisher=[[The Age]] | date=28 July 2009 | accessdate=2009-07-28 | location=Melbourne | first=Darren | last=Gray}}</ref> The AWI maintains that pursuing a deadline approach to eliminating mulesing was not based on "sound health and welfare science" and risked a serious deterioration in the welfare of sheep. Alternative methods of mulesing, such as using clips and intradermals, were "not sufficiently developed to support a wholesale cessation of the procedure in 2010", AWI said.<ref name="SMH">{{cite news | title=Wool industry abandons mulesing deadline| url=http://www.smh.com.au/environment/wool-industry-abandons--mulesing-deadline-20090728-dzxr.html |publisher=[[The Sydney Morning Herald]] | date=28 July 2009 | accessdate=2009-11-14}}</ref>

Present (2011) approach of AWI is that it "supports all woolgrowers in their choice of best practice animal health and hygiene in flystrike control", including the practice of mulesing without pain relief,<ref name="AWI on flystrike prevention">[http://www.wool.com/Grow_Animal-Health_Flystrike-prevention.htm AWI on flystrike prevention]</ref> but aims to provide animal welfare improvements such as pain relief and antiseptics. 
<ref name="AWI’s flystrike management approach">[http://www.wool.com/Grow_Animal-Health_Flystrike-prevention_Management.htm AWI’s flystrike management approach]</ref><ref name="Improved surgery">[http://www.wool.com/Grow_Animal-Health_Flystrike-prevention_Welfare-improved-surgery.htm Welfare-improved surgery with pain relief]</ref>

==Alternatives==
Alternatives to mulesing must meet health standards for both the lamb and its handlers in addition to being safe for consumption as [[mutton|meat]] or [[wool|textile]].

===Breeding programs===
Merino sheep bred on selection principles may be more resistant to flystrike because they are plain bodied (lower skin wrinkling around the breech). Studies have shown that flystrike is lower in plain bodied sheep. However, mulesed animals had consistently lower flystrike than unmulesed regardless of body type.<ref>{{Cite book
  | first = J.L.
  | last = Smith
  | author-link = 
  | first2 = H.G.
  | last2 = Brewer
  | first3 = T.
  | last3 = Dyall
  | author2-link = 
  
  | contribution = 
  | contribution-url = 
  | series = 18
  | year = 
  | pages = 334–337
  | place = 
  | publisher = Association Advancement of Animal Breed Genetics
  | url = http://www.aaabg.org/proceedings18/files/smith334.pdf
  | doi = 
  | id = }}</ref>

The resistance of plain-bodied Merino sheep to flystrike arose from field investigations by Australian scientists, Drs H.R. Seddon and H.B. Belschner, in the early 1930s. Non-mulesed Merino ewe bodies were graded as plain (A class), wrinkly (B class) and very wrinkly (C class). The plain-bodied (A class) Merino ewes were much less susceptible to flystrike than wrinkly-bodied Merinos (B and C class).

In these Merino flocks, the sheep are plainer than the plain-bodied (A class) Merino ewes and therefore more resistant to flystrike. The rams and semen from these studs are used in over 3,000 of the 45,000 Merino farms in Australia.  Using breeding principles, wrinkle-skinned Merino flocks which require mulesing have been transformed into plain-bodied and mules-free flocks within five years.<ref name=ABC-2007-04>{{cite web
|title=New Zealand farmers on the ball with bare breech breeding
|url=http://www.abc.net.au/rural/wa/content/2006/s1905645.htm
|author=Tara De Landgrafft
|work=ABC Rural News |publisher=Australian Broadcasting Corporation
|date=24 April 2007
|accessdate = 2007-05-01}}</ref><ref>Watts, J: "The Genetic Alternative to Mulesing Plain-Bodied Sheep", ''Australian Farm Journal'', p. 43-45, May 2008.</ref><ref>Watts, J: "Fly Strike Resistance. Non-Mulesed Merinos", ''Australian Farm Journal'', p. 52-55, July 2008.</ref><ref>Watts, J: "Breeding Mules-Free Merinos Within Five Years", ''Australian Farm Journal'', p. 45-47, October 2008.</ref><ref name="AWI Alternatives"/>

===Non-surgical alternatives===
Several non-surgical alternatives are currently being researched:

* Insecticides: Any number of insecticides are now available for prevention of fly strike.<ref>"Insecticide works better than mulesing" ABC Rural News http://www.abc.net.au/site-archive/rural/news/content/201301/s3674132.htm</ref> and even early reviews proclaimed the effectiveness of using dip across the whole animal, rather than cutting one small portion that left the rest of the animal still susceptible "dipping is still the most cost effective means of protecting sheep from flystrike". <ref>Heath (1994) quoted in CJC Phillips research in Animal Welfare 2009"A review of mulesing and other methods to control flystrike (cutaneous myiasis) in sheep" https://espace.library.uq.edu.au/view/UQ:233179/UQ233179_fulltext.pdf</ref>
* Topical protein-based treatments which kill wool follicles and tighten skin in the breech area (''intradermal injections'')<ref name="AWI Alternatives">{{cite web |title = Alternatives to mulesing | url = http://www.wool.com.au/Animal_Health/Alternatives_to_mulesing/page__2050.aspx | work = Animal Health | publisher = Australian Wool Innovation | accessdate = 2007-01-09}}</ref>
* Biological control of [[Blow-fly|blowflies]].<ref name="AWI Blowfly"/>
* Plastic clips on the sheep's skin folds which act like castration bands, removing the skin (''breech clips'').<ref name="AWI Alternatives"/>
* [[Tea tree oil]] as a 1% formulation dip where tests have shown a 100% kill rate of first stage maggots and a strong repellent effect against adult flies, which prevented eggs being laid on the wool for up to six weeks.<ref>{{cite web |title = Tea tree oil beats fly strike and sheep lice | url = http://sl.farmonline.com.au/news/state/livestock/sheep/tea-tree-oil-beats-fly-strike-and-sheep-lice/2430626.aspx | work = Animal Health | publisher = Stock & Land | accessdate = 2012-01-26}}</ref>

==See also==
*[[Agriculture in Australia]]
*[[Overview of discretionary invasive procedures on animals]]

==References==
{{Reflist|2}}

[[Category:Agriculture in Australia]]
[[Category:Cruelty to animals]]
[[Category:Animal welfare]]
[[Category:Sheep]]