{{Use dmy dates|date=November 2016}}
{{Use Australian English|date=November 2016}}
[[File:Thomas Henry Brooker 2.jpeg|thumb|right]]

'''Thomas Henry Brooker''' (30 December 1850 – 11 July 1927) was a politician in colonial [[South Australia]]. He was a member of the [[South Australian House of Assembly]] from 1890 to 1905, representing [[electoral district of West Torrens|West Torrens]] (1890-1902) and [[electoral district of Port Adelaide|Port Adelaide]] (1902-1905). He was Minister for Education and Minister for Industry in the [[John Jenkins (Australian politician)|Jenkins]] ministry from May 1901 to March 1902.

==History==
T. H. Brooker was born in [[Kensington]], England, and arrived at [[Port Adelaide]] with his parents,  William Brooker (c. 1826 –  24 January 1909), bricklayer, and his wife Jane, née Gemmell, on the ''Caroline'' in April 1855, and spent the greater part of his life in the West Torrens district.<ref name=death>{{cite news |url=http://nla.gov.au/nla.news-article55041817 |title=Death of Mr. T. H. Brooker |newspaper=[[South_Australian_Register|The Register (Adelaide, SA : 1901 - 1929)]] |location=Adelaide, SA |date=12 July 1927 |accessdate=9 September 2015 |page=10 |publisher=National Library of Australia}}</ref> His father was severely injured in a building collapse at Port Adelaide, and had to abandon his trade in 1875 for a position as Hindmarsh [[poundkeeper]].<ref name=Parsons>Parsons, Ronald ''Hindmarsh Town'' Corporation of the Town of Hindmarsh, South Australia ISBN 0 9598793 0 7</ref>

For 15 years he worked for [[Thomas Hardy (winemaker)|Thomas Hardy]] at Bankside (now [[Torrensville, South Australia|Torrensville]] and [[Underdale, South Australia|Underdale]], learning a good deal about primary production. Subsequently he worked as a salesman and wood merchant at [[Ridleyton, South Australia|Ridleyton]].

In 1885 he was elected a Councillor for Brompton Ward in the Hindmarsh Corporation, and acted in that capacity for five years. In December 1891 he was elected without opposition as Mayor of Hindmarsh, succeeding [[Joseph Vardon]], and served for one year. He was elected as an Independent Liberal to the [[South Australian House of Assembly]] [[multi-member]] seats of [[Electoral district of West Torrens|West Torrens]] from [[South Australian colonial election, 1890|April 1890]] to March 1902 and [[Electoral district of Port Adelaide|Port Adelaide]] from May 1902 to May 1905,<ref>[http://www2.parliament.sa.gov.au/formermembers/Detail.aspx?pid=3655 Thomas Brooker: SA Parliament]</ref> holding his seat during the regime of four Governments. In 1898 he was made Government Whip in succession to (later Sir) [[Richard Butler (Australian politician)|Richard Butler]], and in 1901 followed [[Lee Batchelor]] as Minister of Education and Industry in the [[John Jenkins (Australian politician)|Jenkins]] ministry. He resigned his portfolio in 1902 when the size of Cabinet was reduced. He was a Vice President of the Homestead League, and a supporter of the [[Village Settlements (South Australia)|Village Settlement]] scheme.<ref name=death/>

On retiring from politics Brooker, with [[Joseph Vardon]] and [[William Charlick]], helped establish the Fruit and Produce Exchange, of which he became the secretary in 1903, and played a substantial part in the development of the East End Market.

==Other interests==
In the early 1880s he helped to start the Hindmarsh Literary Society. For many years he was a member of the Adelaide Licensing Bench. He was a member of the Board of Governors of the [[Adelaide Botanic Garden|Botanic Garden]] for nearly 34 years, and Chairman for 30 years, having succeeded Sir [[Henry Ayers]], and was responsible for much of its development. He was a lifelong member of the [[Churches of Christ in Australia|Church of Christ]], Robert street. [[Hindmarsh, South Australia|Hindmarsh]], filling many positions, including trustee and elder of the church, and superintendent of the Sunday school, reputedly the first Church of Christ Sunday school in Australia.<ref name=Parsons/>

Brooker was a great sports lover, proficient at cricket and tennis. He was a keen fisherman, and although never a footballer, he was one of the founders of the [[West Torrens Football Club]], and for 13 years its President.<ref name=death/>

==Family==
Brooker married Emma Tume (c. 1850 – 28 March 1920) on 6 March 1870; their children included:<!--six surviving children-->
*William Henry Brooker, who married Nellie Inez Alvorado on 29 September 1897 
*Elizabeth May Brooker, who married William Lambert Glastonbury on 8 April 1892
*Ethel Emma Brooker, died aged 3.
*Thomas Hardy Brooker, died aged 8 months
*Hamilton Thompson Brooker, who married Susannah Parsons on 11 July 1912
*Eva Brooker, died aged 5 days.
*Daisy Gemmell Brooker, who married Edward Charles Clarke on 8 December 1910
*Ella Myrtle Brooker, who married Clarence Ferdinand Rainsford on 9 January 1907
*Ivy Muriel Brooker, who married Charles Stuart Munro on 25 March 1909
*Doris Fern Brooker, who married Alick D. W. Lawrie on 15 March 1913

His grandchildren included Dr. Kevin Glastonbury and Mr. Vivian M. Brooker of [[Braybrook, Victoria]].

After his wife died in 1920, Brooker lived with his daughter Doris Lawrie at Aveland avenue. North Norwood.

His siblings included William Brooker ( – 10 November 1931), who married Elizabeth Mary Brown on 27 October 1870, and were parents of Rev. W. C. Brooker; John Brooker (10 September 1861 – 1 March 1947), who had a substantial preserved fruit business in [[Croydon, South Australia|Croydon]];<ref>{{cite news |url=http://nla.gov.au/nla.news-article55883696 |title=Brookers' Founder Dies |newspaper=[[Sunday_Mail_(Adelaide)|The Mail (Adelaide, SA : 1912 - 1954)]] |location=Adelaide, SA |date=1 March 1947 |accessdate=9 September 2015 |page=1 |publisher=National Library of Australia}}</ref> and Mary Brooker (c. 1867 – January 1929) who married James Walter Snook on 6 October 1890.

==See also==
*[[Hundred of Brooker]]
==References==
{{reflist}}

==Further reading==
*Marlene J. Cross, 'Brooker, Thomas Henry (1850–1927)', Australian Dictionary of Biography, National Centre of Biography, Australian National University, http://adb.anu.edu.au/biography/brooker-thomas-henry-5371/text9087, published first in hardcopy 1979, accessed online 9 September 2015.
{{DEFAULTSORT:Brooker, Thomas}}
[[Category:Members of the South Australian House of Assembly]]
[[Category:1850 births]]
[[Category:1927 deaths]]