{{taxobox
 | status              = "Critically endangered, if not extinct"
 | status_ref          = <ref name=CAC115>Carleton and Arroyo-Cabrales, 2009, p.&nbsp;115</ref>
 | image               = Oryzomys peninsulae skull dorsal.png{{!}}upright
 | image_alt           = Skull, seen from above, on a black background, with the number "8" next to it. On the braincase, the number 146618 and the female symbol are written.
 | image_caption       = Skull of ''Oryzomys peninsulae'', seen from above<ref name=GplI>Goldman, 1918, plate&nbsp;I</ref>
 | regnum              = [[Animal]]ia
 | phylum              = [[Chordate|Chordata]]
 | classis             = [[Mammal]]ia
 | ordo                = [[Rodent]]ia
 | familia             = [[Cricetidae]]
 | genus               = ''[[Oryzomys]]''
 | species             = '''''O.&nbsp;peninsulae'''''
 | binomial            = ''Oryzomys peninsulae''
 | binomial_authority  = [[Oldfield Thomas|Thomas]], 1897
 | synonyms_ref        = <ref name=CAC122>Carleton and Arroyo-Cabrales, 2009, p.&nbsp;122</ref>
 | synonyms            = 
* ''Oryzomys peninsulæ'' <small>Thomas, 1897{{#tag:ref|The [[International Code of Zoological Nomenclature]] mandates that specific names first published with a [[typographic ligature|ligature]] such as ''æ'' are to be corrected (International Commission on Zoological Nomenclature, 1999, Art.&nbsp;32.5.2).|group="fn"}}</small>
* ''Oryzomys palustris peninsulae'': <small>Hershkovitz, 1970</small>
* ''Oryzomys couesi peninsularis'': <small>Alvarez-Castañeda, 1994{{#tag:ref|[[Incorrect subsequent spelling]] (Alvarez-Castañeda, 1994, p.&nbsp;99; Carleton and Arroyo-Cabrales, 2009, p.&nbsp;122).|group="fn"}}</small>
* ''Oryzomys couesi peninsulae'': <small>Alvarez-Castañeda and Cortés-Calva, 1999</small>
 | range_map           = Oryzomys distribution W Mexico.png
 | range_map_alt       = Map of western Mexico with a green mark on the southern tip of the Baja California peninsula, an orange mark off the coast of Nayarit, a pink area inland in the southwest, and a red area along the Pacific coast north to Sonora.
 | range_map_caption   = Distribution of ''Oryzomys peninsulae'' (in dark green) and other western Mexican ''Oryzomys''
}}

'''''Oryzomys peninsulae''''', also known as the '''Lower California rice rat''',<ref>Goldman, 1918, p.&nbsp;45</ref> is a species of [[rodent]] from western [[List of mammals of Mexico|Mexico]]. Restricted to the southern tip of the [[Baja California peninsula]], it is a member of the genus ''[[Oryzomys]]'' of family [[Cricetidae]]. Only about twenty individuals, collected around 1900, are known, and subsequent destruction of its riverine habitat may have driven the species to [[extinction]].

Medium in size for its genus, it was first described as a separate species, but later lumped into other, widespread species until it was reinstated as separate in 2009. It is distinctive in fur color—grayish brown on the forequarters and reddish brown on the hindquarters—and in some dimensions of its skull, with a high [[braincase]], robust [[zygomatic arch]]es (cheekbones), and long [[incisive foramen|incisive foramina]] (perforations of the [[palate]] between the [[incisor]]s and the [[molar (tooth)|molars]]).

==Taxonomy==
''Oryzomys peninsulae'' was first collected in 1896 and [[Oldfield Thomas]] described it in 1897 as a full species of ''[[Oryzomys]]''.<ref name=T548>Thomas, 1897, p.&nbsp;548</ref> It was retained as a distinct species related to ''[[Oryzomys couesi|O.&nbsp;couesi]]'' and ''[[Oryzomys palustris|O.&nbsp;palustris]]'' until 1971, when [[Philip Hershkovitz]] swept it, and other outlying populations of the same species group, as subspecies under an expanded concept of ''O.&nbsp;palustris''.<ref name=CAC122-SE914/> [[Raymond Hall]] concurred in the second edition (1981) of ''Mammals of North America'', arguing that ''O.&nbsp;peninsulae'' differed less from mainland ''Oryzomys'' populations (currently [[scientific classification|classified]] as ''O.&nbsp;couesi mexicanus'') than some other forms he included in ''O.&nbsp;palustris'' differed from each other.<ref>Hall, 1981, pp.&nbsp;610–611</ref> After studies of the contact zone between North American ''O.&nbsp;palustris'' and Central American ''O.&nbsp;couesi'' in southern [[List of mammals of Texas|Texas]] and northeastern [[Tamaulipas]] (by Benson and Gehlbach in 1979 and Schmidt and Engstrom in 1994) made clear that the two are distinct from each other, ''O.&nbsp;peninsulae'' remained as a [[subspecies]] of ''O.&nbsp;couesi''.<ref name=CAC122-SE914>Carleton and Arroyo-Cabrales, 2009, p.&nbsp;122; Benson and Gehlbach, 1979, p.&nbsp;227; Schmidt and Engstrom, 1994, p.&nbsp;914</ref> In 2009, Michael Carleton and Joaquín Arroyo-Cabrales reviewed the classification of western Mexican ''Oryzomys'' and used [[morphology (biology)|morphological]] and [[morphometrical]] data to characterize four distinct ''Oryzomys'' species in the region. ''O.&nbsp;peninsulae'' and another isolated population, ''[[Oryzomys nelsoni|O.&nbsp;nelsoni]]'' from the [[Islas Marías]], were both retained as separate species, as was ''[[Oryzomys albiventer|O.&nbsp;albiventer]]'' from [[montane]] mainland Mexico. They kept the population in the coastal lowlands as a subspecies, ''O.&nbsp;couesi mexicanus'', of ''Oryzomys couesi''.<ref name=CAC94>Carleton and Arroyo-Cabrales, 2009, p.&nbsp;94</ref>

The genus ''Oryzomys'' currently includes about eight species distributed from the eastern [[List of mammals of the United States|United States]] (''O.&nbsp;palustris'') into northwestern [[List of South American mammals|South America]] (''[[Oryzomys gorgasi|O.&nbsp;gorgasi]]'').<ref name=CAC106>Carleton and Arroyo-Cabrales, 2009, p.&nbsp;106</ref> ''O.&nbsp;peninsulae'' is part of the ''O.&nbsp;couesi'' section, which is centered on the widespread Central American ''O.&nbsp;couesi'' and also includes various other species with more limited and peripheral distributions.<ref name=CAC117>Carleton and Arroyo-Cabrales, 2009, p.&nbsp;117</ref> Many aspects of the [[systematics]] of this section remain unclear and it is likely that the current classification underestimates the group's true diversity.<ref name=CAC107>Carleton and Arroyo-Cabrales, 2009, p.&nbsp;107</ref> ''Oryzomys'' was previously a much larger genus, but most species were progressively removed in various studies, culminating in contributions by Marcelo Weksler and coworkers in 2006 that excluded more than forty species from the genus.<ref>Weksler et al., 2006, table&nbsp;1</ref> ''Oryzomys'' and many of the species removed from it are classified in the tribe [[Oryzomyini]] ("rice rats"), a diverse assemblage of American rodents of over a hundred species,<ref>Weksler, 2006, p.&nbsp;3</ref> and on higher taxonomic levels in the subfamily [[Sigmodontinae]] of family [[Cricetidae]], along with hundreds of other species of mainly small rodents.<ref>Musser and Carleton, 2005</ref>

==Description==
[[File:Oryzomys peninsulae skull ventral.png|thumb|left|upright|alt=Skull, seen from below, on a black background.|Skull of ''Oryzomys peninsulae'', seen from below<ref name=GplI/>]]
''Oryzomys peninsulae'' is a medium-sized member of the genus, smaller than ''O.&nbsp;albiventer'' but larger than ''O.&nbsp;couesi mexicanus''. Its fur is grayish-brown on the forequarters, but reddish-brown on the hindquarters; this coloration pattern is unique among western Mexican ''Oryzomys''.<ref name=CAC122/> The underparts are a dirty white, the feet white above, and the tail dark or brownish above and dirty white below.<ref name=M274>Merriam, 1901, p.&nbsp;274</ref>

In the [[skull]], the [[braincase]] is high, the [[zygomatic arch]]es (cheekbones) are broad and squared, and the [[incisive foramen|incisive foramina]], which perforate the [[palate]] between the [[incisor]]s and the [[molar (tooth)|molars]], are long and broad. The upper incisors are [[orthodont]], with their cutting edge nearly vertical.<ref name=CAC122/> Morphometrically, the skull of ''O.&nbsp;peninsulae'' is sharply distinct from other western Mexican ''Oryzomys''.<ref>Carleton and Arroyo-Cabrales, 2009, pp.&nbsp;98–104</ref>

In fourteen specimens measured by Carleton and Arroyo-Cabrales, total length was {{convert|227|to|305|mm|in|abbr=on}}, averaging {{convert|265.6|mm|in|abbr=on}}; head and body length was {{convert|113|to|152|mm|in|abbr=on}}, averaging {{convert|128.9|mm|in|abbr=on}}; tail length was {{convert|114|to|156|mm|in|abbr=on}}, averaging {{convert|136.8|mm|in|abbr=on}}; hindfoot length was {{convert|29|to|34|mm|in|abbr=on}}, averaging {{convert|32.0|mm|in|abbr=on}}; and occipitonasal length (a measure of total skull length) was {{convert|27.8|to|34.3|mm|in|abbr=on}}, averaging {{convert|31.5|mm|in|abbr=on}}.<ref>Carleton and Arroyo-Cabrales, 2009, table&nbsp;2</ref>

==Distribution, ecology, and status==
Twenty-one specimens of ''O.&nbsp;peninsulae'' are known: six were caught at [[Santa Anita, Baja California Sur|Santa Anita]] in 1896 by D.&nbsp;Coolidge, and [[Edward William Nelson]] and [[Edward Alphonso Goldman]] obtained fifteen additional individuals in 1906 at [[San José del Cabo]].<ref>Carleton and Arroyo-Cabrales, 2009, pp.&nbsp;114–115</ref> The two localities, which are about {{convert|13|km|mi|abbr=on}} apart, were both located along the Río San José, a river in southernmost [[Baja California Sur]], near the southern tip of the [[Baja California peninsula]]. Like other ''Oryzomys'' species, ''O.&nbsp;peninsulae'' is [[semiaquatic]], spending much of its time in the water, but suitable habitat for such a species is estimated to be no more than {{convert|13|km2|sqmi|abbr=on}} on the arid Baja California peninsula.<ref>Carleton and Arroyo-Cabrales, 2009, p.&nbsp;115; Alvarez-Castañeda, 1994, p.&nbsp;99</ref>

Río San José no longer exists, having fallen prey to irrigation projects, and touristic development of its estuary has resulted in pollution. Biologists working in the area in 1979 and from 1991 to 1993 failed to find ''O.&nbsp;peninsulae'', casting doubt on its continued existence. The lack of records for over a century, small distribution, and destruction of the only known habitat led Carleton and Arroyo-Cabrales to consider the [[conservation status]] of ''O.&nbsp;peninsulae'' as "critically endangered, if not extinct". They noted that the status of the species had previously been obscured because it had been [[Lumpers and splitters|lumped]] for decades with ''O.&nbsp;couesi'', a widely distributed and secure species.<ref name=CAC115/>

==Origin==
[[File:Oryzomys palustris in vegetation.jpg|thumb|right|alt=A rat, grayish above and pale below, seen from above and from the front, among reed and leaf litter|The [[marsh rice rat]] (''Oryzomys palustris''), a relative of ''O.&nbsp;peninsulae'' from the eastern United States]]
It is uncertain how ''Oryzomys peninsulae'' arrived at its recent locale in Baja California Sur. In 1922, Nelson suggested that it may have been [[introduced species|introduced]] from another part of Mexico in a shipment of farm products, but this hypothesis is disproved by the clear differentiation from other western Mexican ''Oryzomys'' that the species exhibits.<ref name=CAC107/>

The species's range may be [[Relict (biology)|relictual]] in nature:<ref name=CAC107/> while ''Oryzomys'' is currently found along the eastern coast of the [[Gulf of California]] only as far north as coastal southern [[Sonora]],<ref name=CACf7>Carleton and Arroyo-Cabrales, 2009, fig.&nbsp;7</ref> the past distribution of the genus may have extended further northward, perhaps even into the southwestern [[List of mammals of the United States|United States]], and from there south into Baja California. Subsequent disappearnce of ''Oryzomys'' from the northern regions would have led to its observed [[disjunct distribution]], with ''O.&nbsp;peninsulae'' isolated on the peninsula. This possibility is supported by the relatively close resemblance between ''O.&nbsp;peninsulae'' and ''O.&nbsp;couesi mexicanus'', from coastal western Mexico.<ref>Carleton and Arroyo-Cabrales, 2009, pp.&nbsp;107–108</ref>

Alternatively, the ancestor of ''Oryzomys peninsulae'' may have arrived by [[oceanic dispersal|rafting]] during the late [[Miocene]], about six million years ago, when the southern tip of the Baja California peninsula was an island located near what is now [[Nayarit]] and [[Jalisco]] in western Mexico. Some plants and birds from the area may have a similar [[biogeography|biogeographic]] heritage.<ref name=CAC108>Carleton and Arroyo-Cabrales, 2009, p.&nbsp;108</ref>

==Footnotes==
{{reflist|group="fn"}}

==References==
{{reflist|colwidth=30em}}

==Literature cited==
* Alvarez-Castañeda, S.T. 1994. [http://www.jstor.org/stable/3672202 Current status of the rice rat, ''Oryzomys couesi peninsularis''] (subscription required). The Southwestern Naturalist 39(2):99–100.
* Benson, D.E. and Gehlbach, F.R. 1979. [http://www.jstor.org/stable/1379783 Ecological and taxonomic notes on the rice rat (''Oryzomys couesi'') in Texas] (subscription required). Journal of Mammalogy 60(1):225–228.
* Carleton, M.D. and Arroyo-Cabrales, J. 2009. [http://digitallibrary.amnh.org/dspace/bitstream/2246/6035/4/331-03-carleton.pdf Review of the ''Oryzomys couesi'' complex (Rodentia: Cricetidae: Sigmodontinae) in Western Mexico]. Bulletin of the American Museum of Natural History 331:94–127.
* Goldman, E.A. 1918. [https://books.google.com/books?id=HKYrAAAAYAAJ&pg=PA45 The rice rats of North America]. North American Fauna 43:1–100.
* Hall, E.R. 1981. [https://books.google.com/books?id=brXwAAAAMAAJ Mammals of North America. Volume II.] Caldwell, New Jersey: The Blackburn Press, pp.&nbsp;601–1181, 1–90 (index). ISBN 1-930665-31-8
* International Commission on Zoological Nomenclature. 1999. [http://www.iczn.org/iczn/index.jsp International Code of Zoological Nomenclature. 4th ed.] London: The International Trust for Zoological Nomenclature. ISBN 0-85301-006-4
* Merriam, C.H. 1901. [http://biodiversitylibrary.org/page/8873139 Synopsis of the rice rats (genus ''Oryzomys'') of the United States and Mexico]. Proceedings of the Washington Academy of Sciences 3:273–295.
* Musser, G.G. and Carleton, M.D. 2005. Superfamily Muroidea. Pp.&nbsp;894–1531 in Wilson, D.E. and Reeder, D.M. (eds.). [http://www.departments.bucknell.edu/biology/resources/msw3/browse.asp?id=13000793 Mammal Species of the World: A Taxonomic and Geographic Reference. 3rd ed.] Baltimore: The Johns Hopkins University Press, 2&nbsp;vols., 2142&nbsp;pp. ISBN 978-0-8018-8221-0
* Schmidt, C.A. and Engstrom, M.D. 1994. [http://www.jstor.org/stable/1382473 Genic variation and systematics of rice rats (''Oryzomys palustris'' species group) in southern Texas and northeastern Tamaulipas, Mexico] (subscription required). Journal of Mammalogy 75(4):914–928.
* Thomas, O. 1897. [http://biodiversitylibrary.org/page/19214019 Descriptions of new rats and rodents from America]. Annals and Magazine of Natural History (6)20:544–553.
* Weksler, M. 2006. [http://hdl.handle.net/2246/5777 Phylogenetic relationships of oryzomyine rodents (Muroidea: Sigmodontinae): separate and combined analyses of morphological and molecular data]. Bulletin of the American Museum of Natural History 296:1–149.
* Weksler, M., Percequillo, A.R. and Voss, R.S. 2006. [http://hdl.handle.net/2246/5815 Ten new genera of oryzomyine rodents (Cricetidae: Sigmodontinae)]. American Museum Novitates 3537:1–29.

{{Oryzomyini nav}}

{{taxonbar}}
{{featured article}}

[[Category:Oryzomyini|Peninsulae]]
[[Category:Endemic fauna of Mexico]]
[[Category:Fauna of the Baja California Peninsula]]
[[Category:Mammals of Mexico]]
[[Category:Animals described in 1897]]
[[Category:Taxa named by Oldfield Thomas]]