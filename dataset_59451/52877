{{Infobox journal
| title = Sociological Methods & Research
| cover = [[File:Sociological Methods & Research journal front cover image.gif]]
| editor = Christopher Winship 
| discipline = [[Sociology]]
| former_names =
| abbreviation = Socio. Meth. Res.
| publisher = [[SAGE Publications]]
| country =
| frequency = Quarterly
| history = 1972-present
| openaccess = 
| license = 
| impact = 2.292 
| impact-year = 2013
| website = http://www.uk.sagepub.com/journals/Journal200867?siteId=sage-uk&prodTypes=any&q=Sociological+Methods+%26+Research&fs=1
| link1 = http://smr.sagepub.com/content/current
| link1-name = Online access
| link2 = http://smr.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 0049-1241
| eISSN = 1552-8294
| OCLC = 470191559
| LCCN = 72626764
}}
'''''Sociological Methods & Research''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that covers research in the field of [[sociology]]. The journal's [[editor-in-chief]] is Christopher Winship ([[Harvard University]]). It was established in 1972 and is currently published by [[SAGE Publications]].

== Abstracting and indexing ==
''Sociological Methods & Research'' is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2013 [[impact factor]] is 2.292, ranking it 7 out of 138 journals in the category "Sociology"<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Sociology |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=Web of Science |postscript=.}}</ref> and 6 out of 45 journals in the category "Social Sciences, Mathematical Methods".<ref name=WoS1>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Social Sciences, Mathematical Methods |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=Web of Science |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://smr.sagepub.com/}}

{{DEFAULTSORT:Sociological Methods and Research}}
[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Sociology journals]]
[[Category:Publications established in 1972]]