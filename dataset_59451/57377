{{Infobox journal
| cover = 
| editor = [[Peter Brusilovsky]]
| discipline = [[Educational Technology]]
| former_names = 
| abbreviation = IEEE Trans. Learning Tech.
| publisher = [[Institute of Electrical and Electronics Engineers]]
| country =
| frequency = Quarterly
| history = 2008–present
| openaccess = 
| license = 
| impact = 1.283
| impact-year = 2014
| website = https://www.computer.org/web/tlt/about-tlt
| link1 = https://www.computer.org/csdl/trans/lt/index.html
| link1-name = Online access
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 
| LCCN = 
| CODEN =
| ISSN = 1939-1382
| eISSN = 
}}
'''''IEEE Transactions on Learning Technologies''''' is a quarterly [[peer-reviewed]] [[scientific journal]] covering [[educational technology]] and applications. It was established in 2008 and is published by the [[IEEE Computer Society]] and [[IEEE Education Society]]. The [[editor-in-chief]] is [[Peter Brusilovsky]] ([[University of Pittsburgh]]). According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.283.<ref name=WoS>{{cite book |year=2015 |chapter=IEEE Transactions on Learning Technologies |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

According to its scope, The ''IEEE Transactions on Learning Technologies'' covers all advances in learning technologies and their applications, including but not limited to the following topics: innovative online learning systems; [[intelligent tutoring systems]]; [[educational games]]; [[simulation]] systems for education and training; [[collaborative learning]] tools; learning with [[mobile devices]]; [[wearable devices]] and interfaces for learning; personalized and [[adaptive learning]] systems; tools for formative and [[summative assessment]]; tools for [[learning analytics]] and [[educational data mining]]; [[ontologies]] for learning systems; [[Canon (basic principle)|standards]] and [[web services]] that support learning; [[authoring tools]] for learning materials; computer support for peer tutoring; learning via computer-mediated inquiry, field, and lab work; [[social learning theory|social learning]] techniques; [[social networks]] and infrastructures for learning and [[knowledge sharing]]; and creation and management of [[learning objects]].

According to Google Scholar,  ''IEEE Transactions on Learning Technologies'' belongs to the top-10 journals in [[educational technology]].<ref>{{cite web |title =Top publications - Educational Technology |year = 2016 |url = https://scholar.google.com/citations?view_op=top_venues&hl=en&vq=eng_educationaltechnology |accessdate = 2016-03-08 }}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|1=https://www.computer.org/web/tlt}}

[[Category:IEEE academic journals|Transactions on Multimedia]]
[[Category:Computer science journals]]
[[Category:Educational technology journals]]
[[Category:Education journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 2008]]
[[Category:Academic journals published by learned and professional societies]]
[[Category:English-language journals]]