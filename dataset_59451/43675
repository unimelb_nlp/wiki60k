{{Use dmy dates|date=December 2016}}
{{Use British English|date=December 2016}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Sociable
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=Two-seat tractor [[biplane]]
 | national origin=[[United Kingdom]]
 | manufacturer=[[Sopwith Aviation Company]]
 | designer=
 | first flight=17 February 1914
 | introduced=1914
 | retired=1914
 | status=
 | primary user=[[Royal Naval Air Service]]
 |more users = 
 | number built=1
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Sopwith Sociable''' (or sometimes '''Churchill''' or '''Tweenie'''<ref name="Robertson p211">Robertson 1970, p. 211.</ref>) was a [[United Kingdom|British]] single-engined two-seat [[tractor configuration]] [[biplane]] designed and built by [[Sopwith Aviation Company|Sopwith]] for the [[Royal Naval Air Service]].<ref name="rn" />

==Design and development==
The Sociable, so called because the crew were seated side-by side rather than in tandem, was ordered by the British Admiralty for use as a training aircraft by the Royal Naval Air Service. It was two-bay biplane powered by a {{convert|100|hp|kW|0|abbr=on}} [[Gnome Monosoupape]] radial.<ref name="rn" /> The Sociable was given [[United Kingdom military aircraft serials|serial number]] ''149'' by the Admiralty and first flew from Brooklands on 17 February 1914.<ref name="rn" />

==Operational history==
Two days after its first flight, the Sociable was delivered to [[RAF Hendon|Hendon]] on 19 February 1914.<ref name="rn" /> The next day the [[Lords Commissioners of the Admiralty|First Lord of the Admiralty]] [[Winston Churchill]] flew in it as a passenger;<ref name="rn" /> it afterwards gained the nickname the "Sopwith Churchill". It was based at Eastchurch when on 25 March 1914 it spun into the ground on take-off.<ref name="rn" />

Repaired by Sopwith it was delivered to [[No. 203 Squadron RAF|No. 3 Squadron RNAS]] in Belgium in September 1914.<ref name="rn" /> It was fitted with an additional fuel tank and a bomb rack and was used on an abortive attempt to bomb a German airship shed at Cologne on 22 September 1914.<ref name="rn" /> It was transferred to  [[No. 201 Squadron RAF|No. 1 Squadron RNAS]] but broke an axle on take-off from Antwerp, damaging the landing gear and badly damaging the upper wing.<ref name="rn" /> While awaiting repair at Antwerp it was abandoned following the advance of [[Siege of Antwerp (1914)|German troops]].<ref name="rn" />

==Operators==
;{{UK}}
*[[Royal Naval Air Service]]

==Specifications==
{{Aircraft specs
|ref=Sopwith - The Man and His Aircraft<ref name="Robertson specs">Robertson 1970, pp. 234–235, 238–239.</ref>
|prime units?=imp
<!--
        General characteristics
-->
|genhide=

|crew=2
|length ft=24
|length in=3
|length note=
|span m=
|span ft=36
|span in=0
|span note=
|height m=
|height ft=9
|height in=0
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=

|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=
|empty weight lb=960
|empty weight note=
|gross weight kg=
|gross weight lb=1640
|gross weight note=
|fuel capacity={{convert|25|impgal|L|abbr=on}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Gnome Monosoupape]]
|eng1 type=[[rotary engine]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=100<!-- prop engines -->

|prop blade number=2<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=8<!-- propeller aircraft -->
|prop dia in=6<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=90
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=3 hr<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|more performance=

}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=
*[[List of aircraft of the Royal Naval Air Service]]
}}

==References==
{{commons category|Sopwith Aviation Company}}

===Notes===
{{reflist|refs=
<ref name="rn">Sturtivant and Page 1992, p. 39</ref>
}}

===Bibliography===
{{refbegin}}
* Robertson, Bruce. ''Sopwith - The Man and His Aircraft''. Letchworth, UK: Air Review, 1970. ISBN 0-900435-15-1.
* Ray Sturtivant and Gordon Page ''Royal Navy Aircraft Serials and Units 1911–1919'' [[Air-Britain]], 1992. ISBN 0-85130-191-6
{{refend}}

{{Sopwith Aviation Company aircraft}}

[[Category:British military utility aircraft 1910–1919]]
[[Category:Sopwith aircraft|Sociable]]