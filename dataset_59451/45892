{{other uses|1,000 years (disambiguation)}}
{{Infobox album| <!-- See Wikipedia:WikiProject_Albums -->
| Name        = 1,000 Years
| Type        = Studio
| Artist      = [[The Corin Tucker Band]]
| Cover       = Album_cover_for_the_album_1,000_Years_by_Corin_Tucker_Band.jpg
| Released    = October 5, 2010
| Recorded    = Little Golden Book Studio, Portland, OR, early 2010
| Genre       = [[Alternative rock]], [[indie rock]]
| Length      = 40:50
| Label       = [[Kill Rock Stars]]
| Producer    = Seth Lorinczi
| Last album  = 
| This album  = '''''1,000 Years''''' <br /> (2010)
| Next album  = ''[[Kill My Blues]]''<br />(2012)
| Misc ={{Singles
| Name          = 1,000 Years
| Type          = studio
| Single 1      = Doubt
| Single 1 date = September 7, 2010
}}}}

'''''1,000 Years''''' is the first album by [[The Corin Tucker Band]], released on October 5, 2010, and the first album Tucker released since [[Sleater-Kinney]] went on "hiatus" in 2006. She recorded the album along with Seth Lorinczi and Julianna Bright of both Golden Bears and [[Circus Lupus]], as well as Sara Lund of Hungry Ghost and [[Unwound]]. Lorinzci was also the album's producer. The only single released from ''1,000 Years'' was "Doubt".

The album received generally positive reviews from critics, many of whom noted that the album was stylistically much more muted and intimate than her work with Sleater-Kinney. As of August 2012, the album has sold about 8,000 copies. It peaked at #9 on the Top Heatseekers Chart, and at #49 on the Top Independent Albums Chart, both on October 23, 2010.

==Background and recording==
Tucker told ''[[The Portland Mercury]]'' that she was recording the album in April 2010, and said it was "definitely more of a middle-aged mom record, in a way. It's not a record that a young person would write."<ref name=PM>{{cite web | url=http://www.portlandmercury.com/portland/words-and-guitar/Content?oid=2416788 | title=Words and Guitar | publisher=Index Newspapers | work=Portland Mercury | date=April 8, 2010 | accessdate=28 July 2013 | author=Lannamann, Ned}}</ref> The origins of these songs lie in material Tucker wrote for live performances in early 2009 in Portland, after which many people encouraged her to make her own album.<ref name=KRS>{{cite web |url=http://www.killrockstars.com/press/krs520/ |title=1,000 Years KRS Press Release |last1=Borolla |first1=Caroline |last2= |first2= |date= |website=Kill Rock Stars Website |publisher= |accessdate=27 July 2013}}</ref> Tucker said that she wanted to create something both quiet and powerful in the making of this album.<ref name=KRS/> Tucker feels that Seth Lorinczi lent the album many of its creative ideas, and has stated, "He brought so many amazing ideas to the songs, it was an entirely new foray." Some of the album's tracks were written initially for the [[Twilight: New Moon]] soundtrack.<ref>{{cite web |url=http://www.pastemagazine.com/articles/2010/07/the-corin-tucker-bands-album-gets-a-title-tracklis.html |title=The Corin Tucker Band's album gets a title, tracklist |last1=Eanet |first1=Lindsay |last2= |first2= |date=July 13, 2010 |website=Pastemagazine.com |publisher=[[Paste (magazine)]] |accessdate=27 July 2013}}</ref> The songs in question include closer "Miles Away".<ref name=Pitchfork>{{cite web |url=http://pitchfork.com/reviews/albums/14700-1000-years/ |title=1,000 Years Review |last1=Raber |first1=Rebecca |last2= |first2= |date=October 8, 2010 |website=[[Pitchfork.com]] |publisher=[[Pitchfork Media]] |accessdate=27 July 2013}}</ref>

==Production==
Some critics described Seth Lorinczi's production on ''1,000 Years'' as lo-fi, with [[Slant (magazine)|Slant]] noting a lack of refinement in the production, especially on "Doubt" and "Dragon".<ref name=Slant/> Tucker, in an interview with [[The Aquarian Weekly]], wrote that Lorinzci's approach to production was to "make each song complete sonically," adding that, in her new project, she was trying to build on her past role as a member of Sleater-Kinney.<ref>{{cite web|url=http://www.theaquarian.com/2010/10/20/interview-with-corin-tucker-band-to-debut-in-1000-years/|title=Interview with Corin Tucker: Band To Debut In ‘1,000 Years’|publisher=}}</ref>

==Composition==

===Music===
Tucker said that her influences for this album included [[The Lion and the Cobra]], as well as [[The Slits]], [[The Raincoats]], and [[The English Beat]].<ref name=PM/> She also named [[Patti Smith]], [[Chrissie Hynde]] and [[Sinead O’Connor]] as influences in an interview with [[PopMatters]].<ref>{{cite web | url=http://www.popmatters.com/pm/feature/135227-ballad-of-a-working-mom-an-interview-with-corin-tucker/ | title=Ballad of a Working Mom: An Interview with Corin Tucker | work=PopMatters | date=January 26, 2011 | accessdate=28 July 2013 | author=Schumer, Ben}}</ref> Tucker's singing on the album is much more subdued and intimate than it was on her work with Sleater-Kinney, when her voice was distinctive for its strong, surging sound.<ref>{{Cite web |url=http://www.npr.org/2011/01/21/130731848/corin-tucker-1-000-years-of-emotional-longing |title=Corin Tucker: '1,000 Years' Of Emotional Longing |last=Tucker  |first=Ken |website=NPR |publication-date=2011-01-21}}</ref> However, she occasionally returns to the full force of her "banshee" voice for which she was previously well-known, for example on "Riley".<ref name=Pitchfork/>

1,000 Years has been compared, both favorably and unfavorably, to Tucker’s work with Sleater-Kinney, with the general conclusion being that the album’s style differs significantly from that of Sleater-Kinney. For example, Pitchfork Media wrote that “fans jonesing for a Sleater-Kinney fix will be disappointed.”<ref name=Pitchfork/>  [[Rolling Stone]] and [[Drowned in Sound]] noted the increased usage of piano and cello, among other instruments, on this album as opposed to in Sleater-Kinney's music. DiS singled out the organ on ‘Handed Love’ as well as the military rhythm of 'Half a World Away' as examples of this.<ref name=Drowned/> 

===Lyrics===
It has been noted that Tucker's marriage seems to have influenced the album's lyrics, in particular those of "Half a World Away" and "It's Always Summer." <ref name=Pitchfork/> Regarding the album's lyrics, Ann Powers of the [[Los Angeles Times]] wrote that they discuss the process of coming to recognize changes in one's reality and surroundings.<ref>{{cite web | url=http://latimesblogs.latimes.com/music_blog/2010/10/album-review-the-corin-tucker-bands-1000-years.html | title=Album review: The Corin Tucker Band's '1,000 Years' | work=[[Los Angeles Times]] | date=October 5, 2010 | accessdate=28 July 2013 | author=Powers, Ann}}</ref>

==Tour==
Tucker toured on both U.S. coasts to support the 1,000 Years album, in addition to dates in other parts of the country. Additionally, on May 3, 2011, Corin opened for [[M. Ward]] at the Crystal Ballroom, in Portland, Oregon.<ref>{{cite web | url=http://www.punknews.org/bands/thecorintuckerband | title=The Corin Tucker Band | work=[[Punknews.org]] | accessdate=31 July 2013}}</ref> In a Kill Rock Stars press release, Tucker stated that she was no longer able to tour for as long as she once could, and that "We’re definitely playing shows, but it’s more like a week here, a week there."<ref name=KRS/>

==Reception==
{{Album ratings
|rev1 = [[Allmusic]]
|rev1score = {{Rating|3.5|5}}<ref name=Allmusic>{{cite web |url=http://www.allmusic.com/album/1000-years-mw0002024147 |title=1,000 Years Review |last1=Phares |first1=Heather|last2= |first2= |date=October 5, 2010 |website=Allmusic.com |publisher=[[Rovi Corporation]] |accessdate=27 July 2013}}</ref>
|rev2 = Cokemachineglow
|rev2score = {{rating|8|10}}<ref>{{cite web|url=http://www.cokemachineglow.com/record_review/5812/corintuckerband-1000years-2010 |archiveurl=https://web.archive.org/web/20120513082843/http://www.cokemachineglow.com/record_review/5812/corintuckerband-1000years-2010 |archivedate=2012-05-13 |title=The Corin Tucker Band: 1,000 Years |work=Cokemachineglow |date=3 December 2010 |accessdate=31 July 2013 |author=Zoladz, Lindsay |deadurl=no |df= }}</ref>
|rev3 = [[Robert Christgau]]
|rev3score = (A)<ref>[http://robertchristgau.com/get_artist.php?name=corin Robert Christgau review]</ref>
|rev4 = [[New Musical Express|NME]]
|rev4score = {{rating|8|10}}<ref>{{cite web|url=http://www.nme.com/reviews/the-corin-trucker-band/11609|title=Album Review: The Corin Tucker Band - 1000 Years (Kill Rock Stars) - NME|publisher=}}</ref>
|rev5 = No Ripcord
|rev5Score = {{rating|8|10}}<ref>{{cite web|url=http://www.noripcord.com/reviews/music/corin-tucker-band/1000-years|title=1,000 Years|publisher=}}</ref>
|rev6 = [[The A.V. Club]]
|rev6Score = (B)<ref>{{cite web|url=http://www.avclub.com/articles/the-corin-tucker-band-1000-years,45945/|title=The Corin Tucker Band: 1,000 Years|date=5 October 2010|publisher=}}</ref>
|rev7 = [[Entertainment Weekly]]
|rev7Score = (C+)<ref>{{cite web|url=http://www.ew.com/ew/article/0,,20430453,00.html|title=1,000 Years|publisher=}}</ref>
|rev8 = [[Slant Magazine]]
|rev8Score = {{rating|3|5}}<ref name=Slant>[http://www.slantmagazine.com/music/review/the-corin-tucker-band-1000-years/2272#sthash.QBT2Fyk3.dpuf The Corin Tucker Band: 1,000 Years]</ref>
|rev9 = [[Pitchfork Media]]
|rev9Score = (6.5/10)<ref name=Pitchfork/>
|rev10 = [[Rolling Stone]]
|rev10Score = {{rating|3.5|5}}<ref>{{cite web|url=http://www.rollingstone.com/music/albumreviews/1-000-years-20101005|title=1,000 Years|publisher=}}</ref>
|rev11 = [[Drowned In Sound]]
|rev11Score = {{rating|8|10}}<ref name=Drowned>{{cite web|url=http://drownedinsound.com/releases/15726/reviews/4141226|title=Album Review: The Corin Tucker Band - 1,000 Years|publisher=}}</ref>
}}<!-- Automatically generated by DASHBot-->
The album received mostly positive reviews; on [[Metacritic]] its weighted average is 76 out of 100, indicating "generally favorable reviews."<ref>{{cite web | url=http://www.metacritic.com/music/1000-years/the-corin-tucker-band | title=1,000 Years | work=[[Metacritic]] | accessdate=31 July 2013}}</ref> The most positive review was from [[Robert Christgau]], who gave it an A and called it "A deep, pained, sober, subtle album". In addition to giving the album an A, Christgau ranked 1,000 Years as the 11th best album of 2010 in his year-end list.<ref>{{cite web | url=http://bnreview.barnesandnoble.com/t5/Rock-Roll/The-Dean-s-List-Christgau-s-Best-of-2010/ba-p/4023 | title=The Dean's List: Christgau's Best of 2010 | publisher=Barnesandnoble.com | work=Barnes and Noble | date=January 14, 2011 | accessdate=28 July 2013 | author=Christgau, Robert}}</ref> This contrasted starkly with the most negative review, a C+ from EW's [[Whitney Pastorek]], who said that, on the album, {{quote|Corin Tucker's voice — always so uniquely emotive in the punkier contexts of S-K — looms uncomfortably over songs that sound scrapbooked from other '90s-centric acts ([[Liz Phair]], [[Pavement (band)|Pavement]]) but never take on a form of their own.}}

With regard to the album's only single "Doubt," Adam Kivel of [[Consequence of Sound]] praised the song on the basis that Tucker's characteristic "howl" made a return on the track.<ref name=CoS>{{cite web |url=http://consequenceofsound.net/2010/10/album-review-the-corin-tucker-band-1000-years/ |title=Album Review: The Corin Tucker Band 1,000 Years |last1=Kivel |first1=Adam |last2= |first2= |date=October 12, 2010 |website=[[Consequence of Sound]] |publisher= |accessdate=26 July 2013}}</ref> Similarly, Thom Gibbs of [[Drowned in Sound]] praised the song because it features Tucker in more familiar vocal form.<ref name=Drowned/> Kivel also noted that the song was stripped down and lacked the same wild energy as Sleater-Kinney's music. Kivel also compared the sound of "Thrift Store Coats" to that of the [[Fiery Furnaces]], and described "Dragon" as "dramatic" while noting that it was dominated by cello.<ref name=CoS/>

==Track listing==
#1,000 Years&nbsp;– 3:47
#Half a World Away&nbsp;– 3:02
#It's Always Summer&nbsp;– 3:37
#Handed Love&nbsp;– 3:17
#Doubt&nbsp;– 3:23
#Dragon&nbsp;– 3:56
#Riley&nbsp;– 3:14
#Pulling Pieces&nbsp;– 4:28
#Thrift Store Coats&nbsp;– 2:52
#Big Goodbye&nbsp;– 4:46
#Miles Away&nbsp;– 4:34

== Chart positions ==
{|class="wikitable"
!align="left"|Chart (2010)
!align="center"|Peak<br/>position
|-
|align="left"|Top Heatseekers
|align="center"|9<ref>{{Cite web |url=http://www.billboard.com/artist/299656/corin-tucker/chart?f=324 |title=Corin Tucker |website=Billboard}}</ref>
|-
|align="left"|Top Independent Albums
|align="center"|49<ref>{{Cite web |url=http://www.billboard.com/artist/299656/corin-tucker/chart |title=Corin Tucker |website=Billboard}}</ref>
|-
|}

== Personnel ==
*Julianna Bright – [[vocals]]
*Douglas Jenkins – [[cello]]
*[[Corin Tucker]] – composer, guitar (acoustic), guitar (electric), main personnel, primary artist, vocals
*Seth Lorinczi – [[bass guitar|bass]], engineer, guitar (acoustic), guitar (electric), keyboards, main personnel, producer, string arrangements
*Sara Lund – drums, percussion
*Kate O'Brien-Clarke – violin
*Sung Kim – artwork

== References ==
<!--- See http://en.wikipedia.org/wiki/Wikipedia:Footnotes on how to create references using  tags which will then appear here automatically -->
{{Reflist|2}}

== External links ==
*{{Allmusic|class=album|id=mw0002024147}}
*{{Metacritic album|id=1000-years/the-corin-tucker-band}}
*{{Discogs master|287933}}
*[http://www.npr.org/2011/01/21/130731848/corin-tucker-1-000-years-of-emotional-longing Corin Tucker: 1,000 Years of emotional longing], [[NPR]]

[[Category:Articles created via the Article Wizard]]
[[Category:Kill Rock Stars albums]]
[[Category:2010 debut albums]]
[[Category:Corin Tucker albums]]
[[Category:English-language albums]]