{{Infobox military person
|name=James Catanach
|image=RAAF airmen BBC ANZAC Hour Radio show 1942 (AWM photo SUK10345).jpg
|caption=RAAF airmen on the set of the BBC's ''Anzac Hour'' radio show, 1942. Catanach is in the centre (in uniform).
|birth_date={{Birth date|1921|11|28|df=yes}}
|death_date={{Death date and age|1944|3|29|1921|11|28|df=yes}}
|placeofburial= Poznan Old Garrison Cemetery, [[Poland]]
|birth_place=[[Melbourne]], [[Victoria (Australia)|Victoria]], Australia
|death_place=
|nickname=Jim
|birth_name=James Catanach
|allegiance={{flag|Australia}}
|branch={{air force|Australia}}
|serviceyears=1940–44
|rank=[[Squadron Leader]]
|servicenumber=Aus.400364
|unit=[[No. 455 Squadron RAAF]]
|commands=
|battles=[[World War II]]
*[[Western Front (World War II)|Channel Front]] ([[POW]])
|battles_label=
|awards=[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]], [[Mentioned in despatches]]
{{plainlist|
}}
|relations=
}}

'''James Catanach''' (28 November 1921&nbsp;– 29 March 1944), known as "Jim" or Jimmy, was an Australian [[Handley Page Hampden]] bomber pilot who was taken prisoner during the [[Second World War]]. Reportedly the youngest [[squadron leader]] bomber pilot in the [[Royal Australian Air Force]] at the age of twenty he is notable for the part he took in the 'Great Escape' from [[Stalag Luft III]] in March 1944 and as one of the men re-captured and subsequently shot by the ''[[Gestapo]]''.

==Pre-war life==
Catanach was born in [[Melbourne]], [[Victoria (Australia)|Victoria]], Australia the son of Ruby and William Catanach a successful jeweller. He attended [[Brighton Grammar School]] from 1929 to 1931 and then [[Geelong Grammar School]] where he spent three years in the cadet corps. After graduating in 1938 he went to work with his older brother, Bill, in the family business.<ref name=brighton/><ref>Vance (2001), pp. 116–117</ref>

==War service==
James Catanach was a salesman until he joined the [[Royal Australian Air Force]] on 18 August 1940, to learn to fly.<ref name=nomroll>{{cite web |url=http://www.ww2roll.gov.au/Veteran.aspx?ServiceId=R&VeteranId=1042971 |title=Catanach, James |work=World War Two Nominal Roll |publisher=Australian Government|accessdate=29 August 2015}}</ref><ref name=edlington>{{cite web|url=http://www.defence.gov.au/news/raafnews/editions/4605/history/story01.htm |title=The great crime: Aussies among murder victims |author= Edlington, David |work=Air Force News |edition=4605 |accessdate=29 August 2015}}</ref> Meanwhile, his brother enlisted in the Army.<ref name=Vance117>Vance (2001), p. 117</ref> On completion of basic initial training at Somers and Narrandera Catanach was posted to Canada where he learned to fly and received his pilot’s wings in June 1941 being commissioned [[pilot officer]].<ref name=brighton>{{cite web |url=http://www.brightongrammar.vic.edu.au/site/DefaultSite/filesystem/documents/James%20Catanach.pdf |publisher=Brighton Grammar School |title=James Catanach DFC |accessdate=29 August 2015}}</ref> He was subsequently posted to Great Britain to fly with [[RAF Bomber Command]]. Initially flying with [[No. 144 Squadron RAF]], he was transferred to [[No. 455 Squadron RAAF]] after completing nine missions.<ref name=Vance117/>

No. 455 Squadron RAAF formed at [[RAF Swinderby]], in [[Lincolnshire]] and had received [[Handley Page Hampden]] bombers by the time the bulk of the Australian personnel arrived on 1 September 1941, having departed Australia by sea on 15 June. Initially assigned to [[No. 5 Group RAF]], Bomber Command in a bomber role, its first operation took place while the squadron was still forming, when a single Hampden attacked Frankfurt at night on 29 August. In doing so, according to the Australian War Memorial, the squadron had the distinction of becoming the "first Australian squadron to bomb Germany". Following this, the squadron increased its operational tempo, undertaking several mine laying operations off the coast of occupied France, as well attacking industrial targets in Germany.<ref name=AWM>{{cite web |url=http://www.awm.gov.au/units/unit_11153.asp |publisher=Australian War Memorial |title=No. 455 Squadron RAAF |work=Second World War, 1939–1945 units |accessdate=29 August 2015}}</ref> These missions were flown by Jim Catanach.<ref name=DFC-card>{{cite web |url=http://static.awm.gov.au/images/collection/items/ACCNUM_LARGE/RCDIG1068969/RCDIG1068969--901-.JPG |publisher=Australian War Memorial |title=Distinguished Flying Cross card – James Catanach |accessdate=29 August 2015}}</ref>

[[File:Hampden I 455 Sqn RAAF in flight 1942.jpg|thumb|305px|A No. 455 Squadron RAAF Hampden, 1942]]

In February 1942, the squadron took part in an unsuccessful attack on the German battleships ''Scharnhorst'' and ''Gneisenau'', before being re-roled as a torpedo-bomber squadron and transferred to [[RAF Coastal Command]] on 26 April 1942; they subsequently deployed a detachment briefly to Vaenga (now [[Severomorsk]]) in the Soviet Union in September.<ref>Moyle (1989), pp.47–49</ref> Catanach had completed nine operations with RAF Bomber Command before the transfer.<ref name=brighton/>  The detachment was to operate in support of convoys bound for Russia, which were at the time suffering heavy losses. However, three of the 16 Hamptons were lost prior to arrival, one of them flown by Catanach. After completing one anti-shipping sweep with the Russians the remaining aircraft were handed over to the [[Soviet Air Forces]] with the RAAF crews instructing the Soviets on their operation. Following the completion of this task the squadron returned to [[RAF Sumburgh]]. In June 1942, Catanach was promoted squadron leader, becoming reputedly the youngest in the Royal Australian Air Force to hold that rank.<ref name=brighton/><ref name=edlington/><ref>Moyle (1989), p.49</ref>

==Prisoner of war==
Catanach and his crew took off in [[Handley Page Hampden]] serial number "AT109" from [[RAF Sumburgh]] at 20:40 hours on the night of 4/5 September 1942 for Vaenja, Northern Russia, via Afrikanda. Nearing the end of the long flight his aircraft was holed in the petrol tanks by ground fire or heavy machine gun fire from a German trawler and Catanach force landed under fire on the shoreline near [[Kirkenes]] (Northern Norway) as he closed in on Murmansk<ref>Moyle (1989), pp.44–47</ref> avoiding ditching in the Arctic waters and saving the lives of his crew.<ref>{{cite web |url=https://www.awm.gov.au/people/P10306095/collection-items/ |publisher=Australian War Memorial |title=James Catanach DFC |work=Collection |accessdate=29 August 2015}}</ref><ref>Moyle (1989), pp.46–48</ref>
Captured immediately by a nearby German patrol, he and his crew became prisoners of war and Catanach was eventually put into prisoner of war camp [[Stalag Luft III]] in the province of Lower Silesia near the town of [[Sagan, Germany|Sagan]] (now [[Żagań]] in [[Poland]]).<ref name=Raebel57>Raebel (1997), p.57</ref><ref name=Herington495>Herington (1963), p.495</ref>

=='Great Escape'==
[[File:Model Stalag Luft III.jpg|thumb|right|305px|Model of Stalag Luft III prison camp.]]
Catanach was one of the 76 men who escaped the prison camp on the night of 24–25 March 1944 in the escape now famous as "[[Stalag Luft III|the Great Escape]]".<ref name=Raebel57/><ref name=Herington495/>

He was fluent in German and took trouble to learn conversational Norwegian from Scandinavian prisoners in the prison camp.<ref name=brighton/> He teamed up with two Norwegians, [[Halldor Espelid]] and [[Nils Fuglesang|Nils Jørgen Fuglesang]], and the New Zealander [[Arnold George Christensen]] who was of Scandinavian descent, in a group heading for Denmark and possibly ultimately [[Sweden during World War II|neutral Sweden]]. Catanach and Christensen reached Berlin<ref>Andrews (1976), p.55</ref> as they were seen there by other escapers before changing trains to  Hamburg which they also reached successfully only to be caught on the next leg of their rail journey from Hamburg to the naval town of Flensburg on the Danish border.
Nearing the border  suspicious policemen insisted on carefully examining their papers, checking their briefcases which contained newspapers and escape rations. Close inspection of their clothing revealed they were wearing altered greatcoats.<ref name=edlington/> Although the four escapers has split up to pretend to be travelling individually they were all in the same railway carriage, more policemen arrived and closely examined every passenger, soon arresting all four suspects. The escapers were taken to Flensburg prison.<ref name="Walker 2015">Walker (2015)</ref>

The four airmen were handed over to the Kiel Gestapo<ref>Read (2012), pp.23–34</ref> and after interrogation were told that they would be taken by road back to prison camp.<ref>{{cite web |url=http://lgcorneille-lyc.spip.ac-rouen.fr/IMG/pdf/Nils_Jorgen_FUGLESANG.pdf |publisher=LG Corneille |title=Photos of the 4 escapers after arrest |accessdate=29 August 2015}}</ref> On 29 March 1944, two or three black sedan cars arrived, Catanach was taken in the first car with three Gestapo agents including SS-Sturmbannfuhrer Johannes Post<ref>Read (2012), pp.24–32</ref> a senior officer based there. Post had his driver stop the car in the countryside outside Kiel about 1630 hours and called Catanach out into a field where he promptly shot him.<ref>Andrews (1976), pp.169–172</ref> The second (and possibly a third) car drew up in the same place shortly afterwards and Post told his agents to get Christensen, Espelid and Fuglesang out, stating that they should take a  break before their long drive.  As the airmen walked into the field they almost stumbled over Catanach's body as they were also shot.<ref name="Walker 2015"/><ref>Andrews (1976), pp.177–178</ref><ref>Read (2012), pp.30–34</ref><ref>Andrews (1976) p.196</ref><ref>Burgess (1990), p.270</ref><ref>{{cite web|url=http://www.pegasusarchive.org/pow/cSL_3_Fifty.htm|work=Pegasus Archive|title=Stalag Luft III: The Fifty|accessdate=28 August 2015}}</ref>

Catanach was one of the 50 escapers who had been listed by SS-Gruppenfuhrer [[Arthur Nebe]] to be killed<ref>Andrews (1976), p.34</ref>  so was amongst those [[Stalag Luft III murders|executed and murdered]] by the ''Gestapo''.<ref>Feast (2015), p.140</ref><ref>Andrews (1976), p.205</ref> Originally his remains were buried at Sagan, but he is now buried in part of the Poznan Old Garrison Cemetery.<ref>{{cite web|url=http://www.cwgc.org/find-war-dead/casualty/2194215/CATANACH,%20JAMES|title=Catanach, James |work=CWGC—Casualty Details|author=Reading Room Manchester|publisher=Commonwealth War Graves Commission|accessdate=29 August 2015}}</ref>

The Australian press maintained a chase for information and justice for their murdered airmen.<ref>{{cite news |url=http://trove.nla.gov.au/ndp/del/article/56436025|title=War Crimes Trial: Australians Murdered |newspaper=The Morning Bulletin |location=Rockhampton, Queensland |date=28 February 1946 |accessdate=29 August 2015}}</ref>

Strangely his name was not on the list of murdered officers which was published by newspapers on 20 May 1944.<ref>Western Morning News, Dundee Courier, Yorkshire Post, etc. 20/05/1944</ref>

[[File:The50Memorial.jpg|thumb|305px|Memorial to "The Fifty" down the road toward Żagań (Catanach at left)]]

==Awards==
The [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] was awarded on 23 June 1942 to Acting Flight Lieutenant James Catanach (Aus.400364), Royal Australian Air Force, No. 455 Squadron RAAF.<ref>{{London Gazette |issue=35609 | supp=yes |date=23 June 1942 |startpage=2818|endpage=2819 }}</ref> On three occasions he brought his aircraft and crew home seriously despite severe battle damage. He had made bombing attacks on Essen, Cologne, Lubeck, Hamburg, Kiel and Lorient.<ref name=brighton/><ref name=nomroll/><ref name=DFC-card/><ref>{{cite web |url=https://www.awm.gov.au/people/rolls/R1522198/ |publisher=Australian War Memorial |title= DFC award James Catanach |accessdate=29 August 2015}}</ref>

His conspicuous bravery as a prisoner was recognized by a [[Mention in Despatches]] as none of the other relevant decorations then available could be awarded posthumously. It was published in a supplement to the ''London Gazette'' on 8 June 1944.<ref>{{London Gazette |issue=36544 | supp=yes |date=2 June 1944 |startpage=2642|endpage=2643 }}</ref><ref>{{cite web |url=https://www.awm.gov.au/people/rolls/R1522199/ |publisher=Australian War Memorial |title= MID award James Catanach DFC |accessdate=29 August 2015}}</ref><ref>{{cite web |url=http://static.awm.gov.au/images/collection/items/ACCNUM_LARGE/RCDIG1068969/RCDIG1068969--902-.JPG |publisher=Australian War Memorial |title=Mention in Despatches card – James Catanach |accessdate=29 August 2015}}</ref>

His awards were presented posthumously to his father at Government House, [[Melbourne]], [[Victoria (Australia)|Victoria]], on 16 September 1944.<ref name=DFC-card/>

==Other victims==
{{main||Stalag Luft III murders}}

The ''Gestapo'' executed a group of 50 of the recaptured prisoners representing almost all of the nationalities involved in the escape. Post-war investigations saw a number of those guilty of the murders tracked down, arrested and tried for their crimes.<ref>Read (2012), pp.294–297</ref><ref>Vance (2000), p.310</ref><ref>Andrews (1976), p.188 and 199</ref>

{|class="wikitable" align="right"
|Nationalities of the 50 executed
|-
|{{Flagicon|UK}} 21 British
|-
|{{Flagicon|Canada|1921}} 6 Canadian
|-
|{{flagicon|Poland}} 6 Polish
|-
|{{flagicon|Australia}} 5 Australian
|-
|{{flagicon|South Africa|1928}} 3 South African
|-
|{{flagicon|New Zealand}} 2 New Zealanders
|-
|{{flagicon|Norway}} 2 Norwegian
|-
|{{flagicon|Belgium}} 1 Belgian
|-
|{{flagicon|Czechoslovakia}} 1 Czechoslovak
|-
|{{flagicon|France}} 1 Frenchman
|-
|{{flagicon|Greece|old}} 1 Greek
|-
|{{flagicon|Lithuania}} [[Romualdas Marcinkus|1 Lithuanian]]
|}

==References==
;Notes
{{Reflist|2}}

;Bibliography

* {{cite book|author=Andrews, Allen |year=1976|title=Exemplary Justice|publisher=Harrap|isbn=0-245-52775-3}}
* {{cite book|author1=[[William Ash (pilot)|Ash, William]]|author2=Foley, Brendan|title=Under the Wire: The Wartime Memoir of a Spitfire Pilot, Legendary Escape Artist and 'Cooler King'|url=https://books.google.com/books?id=quefAAAAMAAJ|year=2005|publisher=Bantam|isbn=978-0-593-05408-6}}
* {{cite book|author=[[Paul Brickhill|Brickhill, Paul]]|title=The Great Escape|url=https://books.google.com/books?id=KEWdJvOdZ2wC|year=2004|publisher=W.W. Norton & Company|isbn=978-0-393-32579-9}}
* {{cite book|author=[[Alan Burgess|Burgess, Alan]]|title=The Longest Tunnel: The True Story of World War II's Great Escape|url=https://books.google.com/books?id=sy3t5zLmXrYC|year=1990|publisher=Naval Institute Press|isbn=978-1-59114-097-9}}
* {{cite book|author=[[Albert P. Clark|Clark, Albert P.]]|title=33 Months as a POW in Stalag Luft III: A World War II Airman Tells His Story|url=https://books.google.com/books?id=5f2fAAAAMAAJ|year=2005|publisher=Fulcrum Pub.|isbn=978-1-55591-536-0}}
* {{cite book|author=Durand, Arthur A.|title=Stalag Luft III: The Secret Story|url=https://books.google.com/books?id=aQdSPgAACAAJ|year=1989|publisher=Patrick Stephens Limited|isbn=978-1-85260-248-2}}
* {{cite book|author=Feast, Sean |year=2015|title=The Last of the 39-ers|publisher=Grub Street|isbn=978-1909166158}}
* {{cite book|last=Herington|first=John|title=Air Power Over Europe, 1944–1945|volume=Volume IV|series=Australia in the War of 1939–1945. Series 3&nbsp;– Air.|location=Canberra|publisher=Australian War Memorial|year=1963|edition=1st|url=https://www.awm.gov.au/collection/RCDIG1070212/|oclc=3633419}}
* {{cite book|author=Moyle, Harry |year=1989|title=The Hampden File|publisher=Air Britain|isbn=0-85130-128-2}}
* {{cite book|last=Raebel|first=Geoffrey W.|title=The RAAF in Russia: 455 Squadron, 1942|location=Loftus, New South Wales|publisher=Australian Military History Publications|year=1997|isbn=0-9586693-5-X}}
* {{cite book |author=Read, Simon |year=2012 |title=Human Game: The True Story of the 'Great Escape' Murders and the Hunt for the Gestapo Gunmen  |publisher=Berkley |isbn=978-042525-273-4 }}
* {{cite book|last=Vance|first=Jonathan F.|year=2001|title=A Gallant Company|url=https://books.google.com.au/books?id=ehf0ESmep-8C&printsec=frontcover&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false|publisher=Pacifica Military|isbn=978-0-935-55347-5}}
* {{cite book|author=Walker, Frank |year=2015|title=Commandos: Heroic and Deadly ANZAC Raids |publisher=Import Aus|isbn=978-0733631535}}

==External links==
* [http://www.mmpubs.com/catalog/lessons-from-history-c-4.html ''Project Lessons from the Great Escape (Stalag Luft III),''] by Mark Kozak-Holland. The prisoners formally structured their work as a project. This [http://lessons-from-history.com/node/33 ''book''] analyses their efforts using modern project management methods.

{{DEFAULTSORT:Catanach, James}}
[[Category:Royal Australian Air Force officers]]
[[Category:Royal Australian Air Force personnel of World War II]]
[[Category:Australian World War II pilots]]
[[Category:World War II prisoners of war held by Germany]]
[[Category:1920 births]]
[[Category:1944 deaths]]
[[Category:Australian military personnel killed in World War II]]
[[Category:Participants in the Great Escape from Stalag Luft III]]
[[Category:Australian escapees]]
[[Category:Extrajudicial killings in World War II]]
[[Category:Australian prisoners of war]]
[[Category:Executed military personnel]]
[[Category:Australian people executed abroad]]
[[Category:People executed by Nazi Germany]]
[[Category:People executed by Germany by firearm]]
[[Category:People educated at Geelong Grammar School]]