'''The Broselow Pediatric Emergency Tape''', also called the '''Broselow Tape''', is a color-coded tape measure that is used throughout the world for pediatric emergencies. The Broselow Tape relates a child’s [[Body weight|height as measured by the tape to his/her weight]] to provide medical instructions including medication [[Dose (biochemistry)|dosages]], the size of the [[Medical equipment|equipment]] that should be used, and the level of shock [[voltage]] when using a [[Defibrillation|defibrillator]]. Particular to children is the need to calculate all these therapies for each child individually. In an emergency the time required to do this detracts from valuable time needed to evaluate, initiate, and monitor patient treatment.<ref>{{cite journal |vauthors=Luten R, Wears RL, Broselow J, Croskerry P, Joseph MM, Frush K |title=Managing the unique size-related issues of pediatric resuscitation: reducing cognitive load with resuscitation aids |journal=Academic Emergency Medicine |volume=9 |issue=8 |pages=840–7 |date=August 2002 |pmid=12153892 |doi=10.1197/aemj.9.8.840}}</ref> The Broselow Tape is designed for children up to approximately 12 years of age who have a maximum weight of roughly 36&nbsp;kg (80 pounds). The Broselow Tape is recognized in most medical textbooks and publications as a standard for the emergency treatment of children.<ref name="google">[https://www.google.com/search?q=broselow+tape&btnG=Search+Books&tbm=bks&tbo=1  Google Book Search for Broselow Tape references]{{MEDRS|date=September 2014}}</ref>

==History==
Emergency physicians [[James Broselow]] and Robert Luten struggled with solving these issues related to the emergency treatment of children in the early 1980s. The result was the invention by Dr. Broselow of a home-made prototype version of the tape in 1985.  Dr. Broselow joined with Dr. Luten, an academic physician from the [[University of Florida]] and member of the newly formed [[Pediatric advanced life support|Pediatric Advanced Life Support (PALS)]] subcommittee, to do the foundational studies upon which the tape was based and to develop and update the tape over the years.<ref>{{cite journal  |vauthors=Luten RC, Wears RL, Broselow J, etal |title=Length-based endotracheal tube and emergency equipment in pediatrics |journal=Annals of Emergency Medicine |volume=21 |issue=8 |pages=900–4 | date=August  1992 |pmid=1497153 |doi=10.1016/S0196-0644(05)82924-5}}</ref><ref>{{cite journal |vauthors=Lubitz DS, Seidel JS, Chameides L, Luten RC, Zaritsky AL, Campbell FW |title=A rapid method for estimating weight and resuscitation drug dosages from length in the pediatric age group |journal=Annals of Emergency Medicine |volume=17 |issue=6 |pages=576–81 |date=June 1988 |pmid=3377285 |doi=10.1016/S0196-0644(88)80396-2}}</ref>

The tape provides pre-calculated medication doses effectively eliminating the potential errors associated with pediatric emergent dosing preparation and administration. This benefit has had major implications in recent years given the prevalence and magnitude of medication errors. [[Medical error]]s are a greater threat to children than adults because their organs are smaller and still developing.  An estimated 35% of pediatric patients<ref>{{Cite journal|last=Kaufmann|first=Jost|last2=Laschat|first2=Michael|last3=Wappler|first3=Frank|date=2016-10-27|title=Medication Errors in Pediatric Emergencies|journal=Deutsches Ärzteblatt International|volume=109|issue=38|pages=609–616|doi=10.3238/arztebl.2012.0609|issn=1866-0452|pmc=3471264|pmid=23093991}}</ref> are incorrectly dosed by EMS providers. Tenfold mathematical errors due to incorrect calculations are a much greater threat to children than adults.  Due to the high level of incorrect calculation errors, alternative [https://www.dosebygrowth.com pediatric emergency tapes] that can be customized have gained popularity. A tenfold adult overdose of a standard adult medication would require multiple [[syringe]]s and tends, therefore, to be obvious to a caregiver, effectively warning of the error.  In contrast, for a small child both a 1x correct dose and a 10x [[Drug overdose|overdose of a drug]] can be administered in the same syringe thus providing no clue as to a potential error. Furthermore, pediatric emergency care is especially prone to error due to the chaotic nature and stress associated with the emergency setting.<ref>{{cite book |last1=Park |first1=Kyung S. |chapter=Human error |editor1-last=Salvendy |editor1-first=Gavriel |title=Handbook of human factors and ergonomics |location=New York |publisher=Wiley |year=1997 |pages=150–73 |edition=2nd |isbn=978-0-471-11690-5}}</ref>

==Design==
The original Broselow tape was divided into 25&nbsp;kg zones for medication doses and eight color zones for equipment selection. Subsequent versions of the tape combined dosing and equipment zones such that the eight color zones contained both dosing and equipment information, thus creating a simple visual system for medication and equipment which is used in most hospitals and ambulances.[[File:BTape3.jpg|thumb|The Broselow Tape and associated system]]

The following list identifies which color zones correlate with each estimated weight zone in kilograms (kg) and pounds (lbs).
{| class="wikitable"
|-
! Color
! Estimated Weight 
(in kilograms)
! Estimated Weight
(in pounds) 
|-
| Grey
| 3–5&nbsp;kg
| 6-11&nbsp;lbs
|-
| Pink
| 6–7&nbsp;kg
| 13-15&nbsp;lbs 
|-
| Red	
| 8–9&nbsp;kg
| 17-20&nbsp;lbs
|-
| Purple 
| 10–11&nbsp;kg
| 22-24&nbsp;lbs
|-
| Yellow
| 12–14&nbsp;kg
| 26-30&nbsp;lbs
|-
| White
| 15–18&nbsp;kg
| 33-40&nbsp;lbs
|-
| Blue
| 19–23&nbsp;kg
| 42-50&nbsp;lbs
|-
| Orange 
| 24–29&nbsp;kg
| 53-64&nbsp;lbs
|-
| Green
| 30–36&nbsp;kg
| 66-80&nbsp;lbs
|}

==Usage==
To use the Broselow Tape effectively the child must be lying down.<ref>{{cite web |last=Frush |first=Karen |title=Study Packet for the Correct Use of the Broselow™ Pediatric Emergency Tape |publisher=Duke University Medical Center |url=http://www.ncdhhs.gov/dhsr/EMS/pdf/kids/DEPS_Broselow_Study.pdf}}</ref> Use one hand to hold the red end of the tape so it is even with the child’s head. (Remember: “red to head”). While maintaining one hand on the red portion at the top of the child’s head, use your free hand to run the tape down the length of the child’s body until it is even with his/her heels (not toes). The tape that is level with the child’s heels will provide his/her approximate weight in kilograms and his/her color zone.

{{multiple image
|align = right
|direction = vertical
|header =
|width = 200

|image1 = BTape1.jpg
|caption1 = Properly measuring a child with the Broselow Tape
|image2 = BTape2.jpg 
|caption2 = Color coded equipment drawers based on the Broselow Tape
}}

==Accuracy of the tape and the effect of obesity==
The Broselow Tape is based on the relationship between weight and length across all ages; each color zone estimates the 50th [[percentile]] weight for length, which for practical purposes estimates the [[Body weight#Ideal body weight|ideal body weight (IBW)]] for emergency dosing. Because of the recent [[obesity epidemic]], concerns have been raised as to the accuracy of the tape to determine acceptable weights and subsequently acceptable doses of emergency medications.

The most recent version of the Broselow Tape incorporates updated length/weight zones based on the most current [[National Health and Nutrition Examination Survey]] data set.{{citation needed|date=September 2014}} Utilizing this data set to examine Broselow Tape predictions of actual body weight with the revised zones reveals that approximately 65% of the time the patient’s measured length places them in the correct zone for actual weight. Of the remaining 35%, ~20% fall into the heavier Broselow-Luten zone above and 13% fall into the lighter zone below, with < 1% [[outlier]]s falling greater than 1 zone from predicted. If the healthcare provider incorporates a visual estimate of [[Habitus (sociology)#Body habitus|body habitus]] into the prediction, the accuracy of the estimate of actual patient weight is improved as confirmed in multiple studies. Specifically, for drug dosing the patient’s length-based dosing zone can be adjusted up one color zone if the child appears [[overweight]]. Thus incorporating a visual estimate of whether the child is over-weight provides a simple method to predict actual patient weight that appears to be clinically relevant given the rise in [[Obesity in the United States|obesity in the U.S.]].{{citation needed|date=September 2014}}

Although some medications are best dosed by actual body weight (e.g., [[Suxamethonium chloride|succinylcholine)]], most [[Cardiopulmonary resuscitation|resuscitation]] medications are distributed in [[Body composition|lean body mass]] (e.g., [[epinephrine]], [[sodium bicarbonate]], [[calcium]], [[magnesium]], etc.) so that IBW as accurately predicted by length, not the actual body weight, would appear preferable for dosing. For most resuscitation medications, the optimal dose is not known and doses based on IBW or actual weight are likely equally effective.{{citation needed|date=September 2014}}

::::::::::'''The recent 2010 PALS guidelines<ref>{{cite journal  |vauthors=Kleinman ME, Chameides L, Schexnayder SM, etal |title=Part 14: pediatric advanced life support: 2010 American Heart Association Guidelines for Cardiopulmonary Resuscitation and Emergency Cardiovascular Care |journal=Circulation |volume=122 |issue=18 Suppl 3 |pages=S876–908 | date=November  2010 |pmid=20956230 |doi=10.1161/CIRCULATIONAHA.110.971101}}</ref> comment on this issue:'''
::::::There are no data regarding the safety or efficacy of adjusting the doses of resuscitation medications
::::::in obese patients. Therefore, regardless of the patient’s habitus, use the actual body weight for 
::::::calculating initial resuscitationdrug doses or use a body length tape with pre-calculated doses. 
::::::::::::::::(Class IIb, LOE C)

Studies on the accuracy of predicting [[Tracheal tube|endotracheal tube]] sizes consistently demonstrate the superiority of length predictions over other methods. Unlike medication dosing, body habitus therefore does not affect the accuracy of the prediction.{{citation needed|date=September 2014}}

{{Quote box
|title = The following is the manufacturer recommendation for how to use of the Broselow tape. Utilizing clinical judgment applied to each situation:
|quote = 
# Measure child to identify weight/color zone.
# If a child appears overweight consider utilizing one zone higher for dosing only. 
# Always use the tape measured length zone for equipment selection regardless of body habitus.
|width = 50%
|align = center
}}

Multiple studies have been conducted regarding the effectiveness of the Broselow Tape. Recently, in 2012, a study with 572 enrolled subjects published that paramedic Broselow measurements correlated well with both scale and ED measurements, underscoring its utility in the prehospital setting.<ref>{{cite journal|last1=Heyming|first1=Theodore|last2=Bosson|first2=Nichole|last3=Kurobe|first3=Aileen|last4=Kaji|first4=Amy H.|last5=Gausche-Hill|first5=Marianne|title=Accuracy of Paramedic Broselow Tape Use in the Prehospital Setting|journal=Prehospital Emergency Care|date=6 June 2012|volume=16|issue=3|pages=374–380|doi=10.3109/10903127.2012.664247}}</ref>   In spite of the debates about accuracy in actual weight estimation and its relevance as noted above, the tape still remains the best tool for predicting actual body weight.<ref>{{cite journal |vauthors=Meguerdichian MJ, Clapper TC |title=The Broselow tape as an effective medication dosing instrument: a review of the literature |journal=Journal of Pediatric Nursing |volume=27 |issue=4 |pages=416–20 | date=August  2012 |pmid=22579781 |doi=10.1016/j.pedn.2012.04.009}}</ref>

==References==
{{reflist}}

[[Category:Medical testing equipment]]