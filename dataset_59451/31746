{{Use mdy dates|date=January 2014}}
{{featured article}}
{{highway detail hatnote|Interstate 70}}
{{Infobox road|state=WV|route=70|type=I
|map_notes=Location of I-70 (in red) in West Virginia
|map_alt=A map showing the path of I-70 through West Virginia
|length_mi=14.45|length_round=2
|length_ref=<ref name="FHWA Log">{{cite web|last=Adderly|first=Kevin|url=http://www.fhwa.dot.gov/reports/routefinder/table1.cfm|title=Table 1: Main Routes of the Dwight D. Eisenhower National System Of Interstate and Defense Highways|date=January 19, 2012|work=FHWA Route Log and Finder List|publisher=[[Federal Highway Administration]]|accessdate=December 18, 2013}}</ref>
|established=1963<ref name="tunnel" />
|direction_a=West|direction_b=East
|terminus_a={{jct|state=OH|I|70}} at [[Ohio]] state line
|junction={{jct|state=WV|US|250}} in [[Wheeling, West Virginia|Wheeling]]<br>{{jct|state=WV|I|470}} in Wheeling
|terminus_b={{jct|state=PA|I|70}} at [[Pennsylvania]] state line
|previous_type=WV|previous_route=69|next_type=WV|next_route=71
}}

'''Interstate 70''' ('''I-70''') is a portion of the [[Interstate Highway System]] that runs from  near [[Cove Fort, Utah]], at a junction with [[Interstate 15 in Utah|Interstate 15]] to [[Baltimore, Maryland]]. It crosses the [[Northern Panhandle of West Virginia|Northern Panhandle]] of [[West Virginia]] through [[Ohio County, West Virginia|Ohio County]] and the city of [[Wheeling, West Virginia|Wheeling]]. This segment is the shortest of all states' through which I-70 passes, crossing West Virginia in only {{convert|14.45|mi}}.  The longest segment is [[Interstate 70 in Colorado|Colorado's]], which measures {{convert|451.04|mi}}. The [[Fort Henry Bridge]] carries I-70 from [[Wheeling Island]] across the [[Ohio River]] and into downtown Wheeling before the freeway enters the [[Wheeling Tunnel]]. [[Interstate 470 (Ohio–West Virginia)|I-470]], a southerly [[bypass (road)|bypass]] of Wheeling and the lone auxiliary Interstate Highway in West Virginia, is intersected near [[Elm Grove, West Virginia|Elm Grove]]. Before crossing into Pennsylvania, I-70 passes [[The Highlands (Wheeling, West Virginia)|The Highlands]], a major shopping center in the panhandle, and the [[Bear Rock Lakes Wildlife Management Area]]. On average, between 27,000 and 53,000&nbsp;vehicles use the freeway every day.

The first road that entered Wheeling was a [[post road]] completed in 1794 that connected Wheeling to [[Morgantown, West Virginia|Morgantown]]. The [[National Road]] was the first interstate road, completed in 1818, that connected Wheeling to [[Cumberland, Maryland]]. When the [[United States Numbered Highway System]] was created in 1926, the National Road was designated [[U.S. Route 40 in West Virginia|U.S. Route&nbsp;40]]. The I-70 designation was brought to the Northern Panhandle with the passage of the [[Federal Aid Highway Act of 1956]], and it was built as a [[controlled-access highway]], bypassing portions of the old National Road. The first portions of I-70 in West Virginia were opened in 1963, and construction was completed in 1971.

==Route description==
Entering [[West Virginia]] from [[Ohio]], I-70 crosses the western channel of the [[Ohio River]] onto [[Wheeling Island]], the most populated island along the Ohio River.<ref name="Hay2008">{{cite book|last=Hay|first=Jerry M.|title=Ohio River Guidebook|url=https://books.google.com/books?id=-CWFN4uL8L8C&pg=PA22|accessdate=December 13, 2013|year=2008|publisher=Inland Waterways Books|isbn=978-1-60585-217-1|page=22}}</ref>  The freeway passes above a light commercial zone, and has an interchange with Zane Street. [[U.S. Route 40 in West Virginia|US&nbsp;40]] and {{nowrap|[[U.S. Route 250 in West Virginia|US 250]]}} become [[concurrency (road)|concurrent]] with I-70 at this interchange, before traveling east toward the [[Fort Henry Bridge]].<ref name="gmaps" /> The bridge crosses the main channel of the river and the [[Greater Wheeling Trail]], a [[rail trail]] that parallels the eastern banks of the river.<ref name="Conservancy2007">{{cite book|last=[[Rails-to-Trails Conservancy]]|title=Rail-Trails Mid-Atlantic: Delaware, Maryland, Virginia, Washington DC and West Virginia|url=https://books.google.com/books?id=bZXeKlRerFUC&pg=PA149|accessdate=February 6, 2011|date=January 2007|publisher=Wilderness Press|isbn=978-0-89997-427-9|page=150}}</ref> Elevated above the city of Wheeling, a complex interchange provides access to the downtown area and [[Benwood, West Virginia|Benwood]]. Traveling eastbound, US&nbsp;40 departs the freeway at this interchange and becomes concurrent with [[West Virginia Route 2]] (WV&nbsp;2) northbound. US&nbsp;40 and WV&nbsp;2 travel through downtown Wheeling on a [[one-way pair]], the southbound lanes passing under I-70 and the northbound lanes passing over I-70.<ref name="gmaps" /> After the interchange, I-70 enters the approximately {{convert|1/4|mi|m|adj=mid|-long}} [[Wheeling Tunnel]] which passes through Wheeling Hill.<ref>{{cite web|last=Weingroff|first=Richard |work=Highway History|url=http://www.fhwa.dot.gov/highwayhistory/data/page07.cfm|title=Part VII: Miscellaneous Interstate Facts|year=1998|publisher=Federal Highway Administration|accessdate=December 13, 2013}}</ref> Immediately east of the tunnel, a [[Interchange (road)#Directional T interchange|directional T interchange]] provides access to WV&nbsp;2 southbound near homes north of the highway. US&nbsp;250 departs I-70 at this interchange. A [[stub ramp]] present at this interchange would have carried WV 2 north of I-70 had it been extended.<ref name="gmaps" />

[[File:Wheeling Tunnel overview 1994.jpg|thumb|left|An overview of Wheeling Hill|alt=An overhead view of Wheeling hill with several roads surrounding it.]]
As I-70 curves to the south, it intersects US&nbsp;40 and {{nowrap|[[West Virginia Route 88|WV 88]]}} with the ramps from the eastbound lanes of US&nbsp;40 and WV&nbsp;88 crossing underneath I-70, parallel to [[Wheeling Creek (West Virginia)|Wheeling Creek]]. The interchange just west of the Wheeling Tunnel and this interchange are complicated by the fact that both are abutted by hills. [[Wheeling Jesuit University|Wheeling Jesuit University's]] southeastern border is formed by the freeway as I-70 approaches the neighborhood of [[Elm Grove, West Virginia|Elm Grove]]. Washington Avenue provides access to the college as the highway continues south before meeting the eastern terminus of I-470, which is a [[bypass (road)|bypass]] of Wheeling and the only auxiliary interstate highway in West Virginia.<ref name="gmaps" /><ref name="fhwa log 3di">{{cite web |author= Staff |url= http://www.fhwa.dot.gov/reports/routefinder/table3.htm |title= Table&nbsp;3: Interstate Routes in Each of the 50&nbsp;States, District of Columbia, and Puerto Rico |date= October 31, 2002 |work= Route Log and Finder List |publisher=Federal Highway Administration |accessdate=December 14, 2013}}</ref> Between the directional T interchange and I-470, I-70 is paralleled by the eastern branch of the Greater Wheeling Trail.<ref name="Conservancy2007"/> A final interchange within Wheeling [[city limits]] provides access to US&nbsp;40 and WV&nbsp;88. Leaving the city, the highway turns further east and enters a deep valley.<ref name="gmaps" /> The highway climbs over Two-Mile Hill,<ref name="twomile hill">{{cite news|url=http://www.news-register.net/page/content.detail/id/50788.html |title=Five-Vehicle Crash Closes Interstate 70 |last=Junkins |first=Casey |date=July 20, 2007 |work=[[Wheeling News-Register]] |archiveurl=http://www.webcitation.org/5wEtfgMWE?url=http://www.news-register.net/page/content.detail/id/50788.html |archivedate=February 4, 2011 |accessdate=January 24, 2011 |deadurl=yes |df=mdy-all }}</ref> and intersects Cabela Drive (County Route 65), which provides access to [[The Highlands (Wheeling, West Virginia)|The Highlands]], a large shopping destination.<ref name="gmaps" /><ref name="highlands">{{cite news|url=http://wowktv.com/story.cfm?func=viewstory&storyid=12290|archiveurl=http://www.webcitation.org/5wEtkKkP0|archivedate= February 4, 2011|title=The Highlands Still on Track to Be 'Destination Location'|last=Novotney|first=Steve|date=July 13, 2006|publisher=[[WOWK-TV]]|location=Huntington, W. Va.|accessdate=January 24, 2011}}</ref> Past The Highlands, I-70 continues northeast though woodlands to an interchange with the Dallas Pike (County Route 41). I-70 passes north of the [[Bear Rock Lakes Wildlife Management Area]] before crossing the [[Pennsylvania]] state line into [[Washington County, Pennsylvania|Washington County]] southwest of [[West Alexander, Pennsylvania|West Alexander]].<ref name="gmaps" /><ref>{{cite web|url=http://www.wvdnr.gov/Hunting/D1WMAareas.shtm#1d1|title=West Virginia Wildlife Management Areas|last=Wildlife Resources Section|year=2003|publisher=[[West Virginia Division of Natural Resources]]|accessdate=December 14, 2013}}</ref>

Out of the ten states I-70 passes through, the {{convert|14.45|mi|km|adj=mid|-long}} segment in West Virginia is the shortest. By comparison, the longest stretch of I-70 through a single state is the {{convert|451.04|mi|km|adj=mid|-long}} segment in [[Colorado]].<ref name="FHWA Log" /> Every year, the [[West Virginia Department of Transportation]] (WVDOT) conducts a series of surveys on its highways in the state to measure traffic volume. This is expressed in terms of [[annual average daily traffic]] (AADT), a measure of traffic volume for any average day of the year. In 2012, WVDOT calculated that as few as 27,000&nbsp;vehicles traveled over the Fort Henry Bridge over the Ohio River, and as many as 53,000&nbsp;vehicles used the highway near its junction with US&nbsp;40 in Elm Grove.<ref name="aadt12">{{cite map|url=http://www.transportation.wv.gov/highways/programplanning/preliminary_engineering/traffic_analysis/trafficvolume/interstatecounts/Documents/I70_Ohio_Pa_12.pdf|publisher=West Virginia Department of Transportation|accessdate=December 12, 2013|year=2012|format=PDF|title=I-70: Ohio to Pennsylvania|scale=Scale not given}}</ref> These counts are of the portion of the freeway in West Virginia and are not reflective of the entire Interstate. As part of the [[Interstate Highway System]],<ref>{{cite journal|last=Slater|first=Rodney E.|date=Spring 1996|title=The National Highway System: A Commitment to America's Future|journal=Public Roads|volume=59|issue=4|url=http://www.fhwa.dot.gov/publications/publicroads/96spring/p96sp2.cfm|accessdate=January 24, 2011}}</ref> the entire route is listed on the [[National Highway System (United States)|National Highway System]], a system of roads that are important to the nation's economy, defense, and mobility.<ref name="fhwa map">{{cite map|publisher=Federal Highway Administration|title=National Highway System: West Virginia|url=http://www.fhwa.dot.gov/planning/national_highway_system/nhs_maps/west_virginia/wv_WestVirginia.pdf|accessdate=December 12, 2013|date=October 1, 2012|format=PDF|scale=2.5&nbsp;in ≈ 40&nbsp;mi (6.4&nbsp;cm ≈ 64.4&nbsp;km)}}</ref>

==History==
[[File:Interstate 70 near Wheeling West Virginia.JPG|thumb|right|I-70 westbound near the campus of Wheeling Jesuit University|alt=An asphalt highway heading toward some wooded hills.]]
The first recorded road to reach what was then Wheeling, Virginia, was a [[post road]] linking it with [[Morgantown, West Virginia|Morgantown]], to the southeast. The post road was completed in 1794. The [[National Road]] was the first interstate road that served Wheeling, linking the town to [[Cumberland, Maryland|Cumberland]], [[Maryland]], in the east. The National Road started construction under order of then President [[Thomas Jefferson]] in 1806 and was completed in 1818.<ref name="Hoch2001">{{cite book|last=Hoch|first=Bradley R.|title=Lincoln Trail in Pennsylvania: A History and Guide|url=https://books.google.com/books?id=hQofzefw7KwC&pg=PA163|accessdate=January 3, 2014|date=August 1, 2001|publisher=Penn State Press|location=University Park, Pa.|isbn=978-0-271-05841-2|page=163}}</ref><ref name="matter of fact">{{cite book|last=Staff|title=As a Matter of Fact|date=January 1998|publisher=[[West Virginia Division of Highways]]|pages=I‑2, II‑8|oclc=45763179}}</ref> In 1926 the [[United States Numbered Highway System]] was established, and the National Road through the Northern Panhandle was [[numbered highways in the United States|designated]] US&nbsp;40.<ref>{{cite news|url=http://search.proquest.com/docview/286242511?accountid=3357|title=Old And New In West Virginia Wheeling Pegs Its Future To A Remarkable Supply Of Victorian-Style Buildings|last=Spencer-Smith|first=Susan|date=April 10, 1988|work=[[The Philadelphia Inquirer]]|accessdate=December 13, 2013|subscription=yes|via=[[ProQuest]]}}</ref> US&nbsp;40 linked [[Vallejo, California]], in the west to [[Atlantic City, New Jersey]], in the east.<ref name="old US route map">{{cite map |publisher= [[Bureau of Public Roads]] |title= United States System of Highways Adopted for Uniform Marking by the American Association of State Highway Officials |url= http://texashistory.unt.edu/ark:/67531/metapth298433/m1/1/zoom/ |date= November 11, 1926 |accessdate= December 18, 2013 |scale= 1:7,000,000 |cartography= U.S. Geological Survey |oclc= 32889555}}</ref> Passage of the [[Federal Aid Highway Act of 1956]] formed the Interstate Highway System,<ref name=lewis>{{cite book|last=Lewis|first=Tom|year=1997|title=Divided Highways: Building the Interstate Highways, Transforming American Life|location=New York|publisher=Viking|pages=120–1, 136–7|isbn=0-670-86627-X}}</ref> designating as I-70 a then unconstructed [[controlled-access highway]] across the panhandle by 1957.<ref>{{cite map|publisher=Public Roads Administration|title=Official Route Numbering for the National System of Interstate and Defense Highways as Adopted by the American Association of State Highway Officials|url=http://www.ajfroggie.com/roads/yellowbook/numbering-1957.jpg|accessdate=December 13, 2013|date=August 14, 1957|cartography=Public Roads Administration|scale=Scale not given}}</ref> Since it was constructed as a separate controlled-access highway, much of I-70 is separate from the old National Road and US&nbsp;40.<ref name=gmaps />

The first portion of what is now known as I-70 to be completed across West Virginia was the Fort Henry Bridge across the main channel of the Ohio River, built in 1955.<ref name="nbi bridge 1">{{NBI|structurenumber=00000000035A061|datakey=709183|linkwork=yes|linkpub=no|accessdate=January 24, 2011}}</ref> WVDOT began obtaining [[right-of-way (transportation)|right-of-way]] for I-70 in 1961.<ref>{{cite news|title=Plans For 70 Right-Of-Way Given by SRC|last=Staff|date=December 9, 1961|work=Charleston Daily Mail|agency=Associated Press|page=8<!-- Can't be 100% sure though, poor quality scan -->}}</ref> The Wheeling Tunnel, linking downtown Wheeling and the Fort Henry Bridge to the eastern suburb of Elm Grove, was completed in 1967<ref>{{cite news|url=http://search.proquest.com/docview/117945930?accountid=3357|title=Free-Wheeling, W.Va.|page=368|last=Terry|first=Bob|date=June 18, 1967|work=[[The New York Times]]|accessdate=December 13, 2013|subscription=yes|via=ProQuest}}</ref> at a cost of $7&nbsp;million (equivalent to ${{formatprice|{{inflation|US-NGDPPC|7000000|1967|r=-5}}}} in {{inflation-year|US-NGDPPC}}).{{inflation-fn|US-NGDPPC}}<ref name="tunnel">{{cite news|url=https://news.google.com/newspapers?id=JzReAAAAIBAJ&sjid=Z2ANAAAAIBAJ&pg=4510,3549368&dq=interstate+70+west+virginia&hl=en|title=West Virginia Plans Major I-70 Projects|date=February 4, 1963|work=The Washington Observer|page=1|accessdate=January 23, 2011|location=Washington, Pennsylvania}}</ref> The bridge that carries I-70 from the Ohio state line onto Wheeling Island was completed in 1968.<ref name="nbi bridge 2">{{NBI|structurenumber=00000000035A059|datakey=709182|linkwork=no|linkpub=no|accessdate=January 24, 2011}}</ref>  Construction of I-70 across the panhandle was almost completed in September 1971, with only one of the two [[carriageway]]s completed in the final {{convert|1+1/5|mi|m|adj=mid|-long|spell=in}} segment of freeway near Elm Grove.<ref>{{cite news|title=Governor Sees Little Need For Major Tax Hike|last=Mellace|first=Bob|date=September 4, 1971|work=Charleston Daily Mail|page=1}}</ref> Then Governor [[Arch A. Moore, Jr.]] and [[United States Senator|Senator]] [[Jennings Randolph]] were present for the opening of this $17&nbsp;million (equivalent to ${{formatprice|{{inflation|US-NGDPPC|17000000|1971|r=-5}}}} in {{inflation-year|US-NGDPPC}}){{inflation-fn|US-NGDPPC}} portion of freeway.<ref>{{cite news|title=Interstate 70 Link To Open|last=Staff|date=August 30, 1971|work=Weirton Daily Times|page=3|agency=[[United Press International]]}}</ref> The second carriageway was completed by the end of 1971.<ref name="matter of fact" />

The Fort Henry Bridge, the [[Interstate 470 Bridge|Vietnam Veterans Memorial Bridge]] (which carries [[Interstate 470 (Ohio–West Virginia)|I-470]]), and the [[Wheeling Suspension Bridge]] were all closed in January 2005, stopping any traffic from Ohio or Wheeling Island from entering mainland West Virginia for a few days because barges broke loose during heavy flooding along the Ohio River.<ref name="floods closed">{{cite news|url=https://news.google.com/newspapers?id=AvkrAAAAIBAJ&sjid=EW0FAAAAIBAJ&pg=5545,382023&dq=fort-henry-bridge&hl=en|title=West Virginia Governor Declares State of Emergency Due to Flood|last=Schelzig|first=Erik|date=January 7, 2005|work=[[Kentucky New Era]]|location=Hopkinsville, Ky.|accessdate=January 29, 2011|agency=Associated Press}}</ref> The Wheeling Tunnel was closed for reconstruction work in 2007,<ref name="eb overdue">{{cite news|url=http://www.news-register.net/page/content.detail/id/501445.html?nav=515 |title=Ready or Not, Tunnel to Open |last=Connors |first=Fred |archiveurl=http://www.webcitation.org/5wEtr0ZYw?url=http://www.news-register.net/page/content.detail/id/501445.html?nav=515 |archivedate=February 4, 2011 |date=October 27, 2007 |work=[[The Intelligencer & Wheeling News Register]] |accessdate=January 24, 2011 |deadurl=yes |df=mdy-all }}</ref> 2008,<ref name="round 2">{{cite news|url=http://www.whsv.com/westvirginiaap/headlines/25801504.html |title=Motorists Warned to Avoid Wheeling Tunnel |date=July 23, 2008 |archiveurl=http://www.webcitation.org/5wEu7JoIc?url=http://www.whsv.com/westvirginiaap/headlines/25801504.html |archivedate=February 4, 2011 |publisher=[[WHSV-TV]] |location=Harrisonburg, Va. |accessdate=January 24, 2011 |deadurl=yes |df=mdy-all }}</ref> and 2010,<ref name="wb closed">{{cite news|url=http://www.news-register.net/page/content.detail/id/533937.html |archiveurl=http://www.webcitation.org/5wEtu4EIE?url=http://www.news-register.net/page/content.detail/id/533937.html |archivedate=February 4, 2011 |title=Tube Closed Until October |last=Johnson |first=J.W., Jr. |date=February 2, 2010 |work=The Intelligencer & Wheeling News Register |accessdate=January 24, 2011 |deadurl=yes |df=mdy-all }}</ref> causing motorists who wished to travel through on I-70 to detour. The two detour routes were city streets in downtown Wheeling and the I-470 loop.<ref name="round 2" />

After traffic issues during the 2008 reconstruction work on the Wheeling Tunnel, local politicians suggested closing the twin tunnels altogether and building the freeway over Wheeling Hill instead.<ref name="kill tunnel">{{cite news|url=http://www.news-register.net/page/content.detail/id/504217.html|title=Tunnel Removal Good Idea, But Not Feasible|last=Connors|first=Fred|date=January 6, 2008|work=The Intelligencer & Wheeling News Register|accessdate=January 24, 2011}}</ref> After opposition from the [[National Association for the Advancement of Colored People]], which represented the affected Wheeling Hill residents,<ref name="naacp">{{cite news|url=http://www.theintelligencer.net/page/content.detail/id/506968.html|title=NAACP Takes Issue With Tunnel Cut Plan|last=Connors|first=Fred|date=March 12, 2008|work=The Intelligencer & Wheeling News Register|accessdate=January 24, 2011}}</ref> the suggestions were dropped. The cost of completing the tunnel replacement project was estimated at between $60 and $80&nbsp;million.<ref name="nix the tunnels">{{cite news|url=http://www.wtov9.com/news/15346669/detail.html |title=Talk Of Eliminating Wheeling Tunnels Moves Forward |last=Lo |first=Jasmine |date=February 19, 2008 |archiveurl=http://www.webcitation.org/5wEtx6clV?url=http://www.wtov9.com/news/15346669/detail.html |archivedate=February 4, 2011 |publisher=[[WTOV]] |location=Steubenville, Ohio |accessdate=January 24, 2011 |deadurl=yes |df=mdy-all }}</ref> The total $13.7&nbsp;million cost of the tunnel reconstruction project was over double the original bid of $5.7&nbsp;million, due in part to the work delays.<ref name="double">{{cite news|url=http://www.theintelligencer.net/page/content.detail/id/539151.html|title=Answer Tunnel Cost Questions|last=Staff|date=June 26, 2010|work=The Intelligencer & Wheeling News Register|accessdate=January 24, 2011}}</ref>

==Exit list==
{{jcttop|exit|state=WV|county=Ohio|exit_ref=<ref name="wvdot exit log">{{cite web|last=Staff|url=http://www.transportation.wv.gov/highways/interstate_interchanges/Pages/InterstateInterchanges.aspx#I70|title=West Virginia Interstate 70 Interchanges|publisher=West Virginia Department of Transportation|accessdate=December 12, 2013}}</ref>|length_ref=<ref name=gmaps>{{google maps|url=https://maps.google.com/maps?f=d&source=s_d&saddr=I-70+E&daddr=I-70+E&hl=en&geocode=FS9wYwId_Acw-w%3BFTjPYwIdfF8z-w&gl=us&mra=prv&sll=40.043649,-80.656128&sspn=0.041133,0.058966&ie=UTF8&ll=40.068614,-80.636902&spn=0.164473,0.235863&z=12|title=Interstate 70 in West Virginia|accessdate=January 23, 2011}}</ref>|location_ref=<ref name="wvdot 2011">{{cite map|publisher=West Virginia Department of Transportation|title=General Highway Map: Ohio County, West Virginia|url=ftp://129.71.206.174/county_maps/Ohio_1_of_1.pdf|accessdate=December 20, 2013|date=January 1, 2011|scale=1&nbsp;in ≈ 1&nbsp;mi (1&nbsp;cm ≈  0.633&nbsp;km)|series=[http://www.transportation.wv.gov/highways/programplanning/gti/GIS/MAPS/Pages/WVCountyMaps.aspx West Virginia County Maps]|section=C3-E3}}</ref>}}
{{jctbridge|exit|location_special=[[Ohio River]]|mile=0.0|bridge=Ohio–West Virginia state line<br />{{jct|state=OH|I|70|dir1=west}} continues into Ohio}}
{{WVint|exit
|location_special=[[Wheeling Island]]
|type=concur
|mile=0.3
|exit=0
|road={{jct|state=WV|US|40|dir1=west|US|250|dir2=north|name2=Zane Street|location1=[[Wheeling Island]]}}
|notes=West end of US&nbsp;40 / US&nbsp;250 overlap; westbound exit and eastbound entrance
}}
{{jctbridge|exit|location_special=[[Ohio River]]|mile=0.5|bridge=[[Fort Henry Bridge]]}}
{{WVint|exit
|location=Wheeling
|lspan=8
|type=concur
|mile=0.7
|exit=1A
|road={{jct|state=WV|US|40|dir1=east|WV|2|dir2=north|name2=Main Street|location1=[[Wheeling, West Virginia|Downtown Wheeling]]}}
|notes=East end of US&nbsp;40 overlap
}}
{{WVint|exit|mile=0.8|mile2=1.1|bridge=[[Wheeling Tunnel]]}}
{{WVint|exit
|type=concur
|mile=1.3
|exit=1B
|road={{jct|state=WV|US|250|dir1=south|to2=to|WV|2|dir2=south|city1=South Wheeling|city2=Moundsville}}
|notes=East end of US&nbsp;250 overlap
}}
{{WVint|exit
|mile=1.9
|exit=2A
|road={{jct|state=WV|US|40|to2=to|WV|88|dir2=north|location1=[[Oglebay Park]]}}
|notes=
}}
{{WVint|exit
|mile=2.6
|exit=2B
|road={{jct|state=WV|CR|70|denom1=1|county1=Ohio|name1=Washington Avenue}}
|notes=Access to [[Wheeling Jesuit University]]
}}
{{WVint|exit
|mile=4.5
|type=incomplete
|exit=4
|road={{jct|state=WV|WV|88|dir1=south|name1={{nowrap|[[U.S. Route 40 in West Virginia|US 40]]}}|city1=Elm Grove}}
|notes=Westbound exit is via exit 5
}}
{{WVint|exit
|type=incomplete
|mile=4.8
|exit=5A
|road={{jct|state=WV|I|470|dir1=west|location1=[[Columbus, Ohio|Columbus]]}}
|notes=Westbound exit and eastbound entrance
}}
{{WVint|exit
|mile=5.3
|exit=5
|road={{jct|state=WV|US|40|city1=Elm Grove|city2=Triadelphia}}
|notes=Westbound access to WV&nbsp;88
}}
{{WVint|exit
|location=none
|mile=9.5
|exit=10
|road={{jct|state=WV|CR|65|county1=Ohio|name1=Cabela Drive}}
|notes=Access to [[The Highlands (Wheeling, West Virginia)|The Highlands]] shopping complex
}}
{{WVint|exit
|location=none
|mile=10.9
|exit=11
|road={{jct|state=WV|CR|41|county1=Ohio|name1=Dallas Pike}}
|notes=
}}
{{jctbridge|exit|location_special=&nbsp;|mile=14.45|bridge=West Virginia–Pennsylvania state line<br />{{jct|state=PA|I|70|dir1=east}} continues into Pennsylvania}}
{{Jctbtm|keys=concur,incomplete}}

==References==
{{reflist|30em}}

==External links==
{{Attached KML|display=title,inline}}
* {{commons category inline}}
* [http://www.aaroads.com/guide.php?page=i0070wawv Interstate 70 in West Virginia] on AA Roads

{{state detail page browse|type=I|route=70|state=West Virginia|statebefore=Ohio|stateafter=Pennsylvania}}
{{subject bar|portal1=U.S. Roads|portal2=West Virginia}}

[[Category:Interstate 70|West Virginia]]
[[Category:Interstate Highways in West Virginia|70]]
[[Category:Transportation in Ohio County, West Virginia]]