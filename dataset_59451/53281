{{Infobox stadium
| name = Doak Field
| nickname = The Doak
| fullname = Doak Field at Dail Park
| image = Doak Field.jpg
| image_size = 220
| location = [[Raleigh, North Carolina|Raleigh]], [[North Carolina]]
| coordinates = {{coord|35|47|14|N|78|40|46|W|type:landmark|display=title,inline}}
| broke_ground =
| opened = 1966
| closed =
| renovated = 2004
| demolished =
| owner = [[North Carolina State University]]
| operator = North Carolina State University
| surface = Grass
| construction_cost = 
| architect = 
| dimensions = Left Field - {{convert|320|ft|m|abbr=on}} <br> Center Field - {{convert|400|ft|m|abbr=on}} <br> Right Field - {{convert|330|ft|m|abbr=on}}
| tenants = [[North Carolina State University]] ([[National Collegiate Athletic Association|NCAA baseball]])<br>[[Atlantic Coast Conference Baseball Tournament]] (1974, 1980)
| seating_capacity = 3,000 <small>(2,500 chairback seats)</small>
}}

'''Doak Field''' (or '''The Doak''') is a [[baseball]] venue in [[Raleigh, North Carolina]], [[United States]].  It opened in 1966 and is home to the [[North Carolina State University]] [[North Carolina State Wolfpack|Wolfpack]] [[college baseball]] team of the [[NCAA]]'s [[List of NCAA Division I baseball programs|Division I]] [[Atlantic Coast Conference]] (ACC).<ref name=bd>{{cite web|title=Doak Field at Dail Park|url=http://www.ballparkdigest.com/200903101548/college-baseball/visits/doak-field-at-dail-park-north-carolina-state-wolfpack|work=Ballpark Digest|accessdate=19 August 2011}}</ref>  It is named for [[Charles Doak]], who was the head coach of the NC State baseball team from 1924&ndash;1939.<ref name=yearbook12>{{cite web|title=2012 NC State Baseball Yearbook |url=http://www.packyearbooks.com/2011-12-baseball-yearbook/files/2.html |work=PackYearbooks.com |publisher=NC State Sports Information |accessdate=2 July 2012 |archiveurl=http://www.webcitation.org/68rfHLNqO?url=http://www.packyearbooks.com/2011-12-baseball-yearbook/files/2.html |archivedate=2012-07-02 |deadurl=yes |df= }}</ref>  The stadium is located on NC State's [[North Carolina State University Main Campus#Central Campus|Central Campus]], behind Lee and Sullivan residence halls.<ref name=campusmap>{{cite web|title=Central Campus Precinct Map|url=http://www.ncsu.edu/campus_map/central.htm|work=NCSU|accessdate=19 August 2011}}</ref> The diamond is in the north/northwest corner of its block, which is bounded by Thurman Drive (third base, north/northeast); Dail Park and the residence halls (left field, east/southeast); Sullivan Drive (right field, south/southwest); and Varsity Drive (first base, west/northwest).  Its seating capacity is 2,500 spectators, with an overflow capacity of 3,000.  The largest crowd at Doak Field since its 2004 renovation was 3,109 on April 28, 2007, in a series finale between NC State and its rival UNC.<ref>{{Cite web|title = North Carolina Rides Six-Run Eighth Inning To 9-3 Win Over NC State|url = http://www.gopack.com/sports/m-basebl/spec-rel/042807aab.html|accessdate = 2015-06-04}}</ref>  Doak Field hosted the [[Atlantic Coast Conference Baseball Tournament]] in both [[1974 Atlantic Coast Conference Baseball Tournament|1974]] and in [[Atlantic Coast Conference Baseball Tournament|1980]].  NC State won the championship in 1974, while [[Clemson Tigers baseball|Clemson]] won in 1980.<ref name=facil>{{cite web|title=Doak Field at Dail Park |url=http://www.gopack.com/facilities/doak-field.html |work=Go Pack |accessdate=19 August 2011 |archiveurl=http://www.webcitation.org/68roVh26R?url=http://www.gopack.com/facilities/doak-field.html |archivedate=2012-07-02 |deadurl=yes |df= }}</ref>

Prior to 1966, the Wolfpack played their home games at [[Riddick Stadium]], which was primarily a football facility.<ref name=riddick>{{cite web|title=Riddick Stadium |url=http://www.ncsu.edu/facilities/buildings/ridd-sta.html |work=NCSU.edu |accessdate=2 June 2012 |archiveurl=http://www.webcitation.org/687vR7K64?url=http://www.ncsu.edu/facilities/buildings/ridd-sta.html |archivedate=2012-06-02 |deadurl=yes |df= }}</ref>

[[File:Doak Field at Dail Park Press Box.JPG|thumb|Doak Field at Dail Park]]

In May 2004, the stadium underwent a [[USD|$]]6 million renovation which included leveling the playing field, a new drainage system, new grandstands, a new [[press box]], and new concessions and bathroom facilities.<ref name=facil />

In 2013, the Wolfpack ranked 33rd among [[List of NCAA Division I baseball programs|Division I baseball programs]] in attendance, averaging 1,994 per home game.<ref name=13att>{{cite web|last=Cutler |first=Tami |title=2013 Division I Baseball Attendance - Final Report |url=http://www.sportswriters.net/ncbwa/news/2013/attendance130611.pdf |work=Sportswriters.net |publisher=NCBWA |accessdate=July 20, 2013 |archiveurl=http://www.webcitation.org/6IGG6kpVy?url=http://www.sportswriters.net/ncbwa/news/2013/attendance130611.pdf |archivedate=July 20, 2013 |date=June 11, 2013 |deadurl=yes |df= }}</ref> In 2014, the Wolfpack ranked 45th in attendance, averaging 1,344 per home game.<ref>{{Cite web|title = http://www.sportswriters.net/ncbwa/news/2014/attendance140331.pdf|url = http://www.sportswriters.net/ncbwa/news/2014/attendance140331.pdf|website = www.sportswriters.net|accessdate = 2015-06-04}}</ref>

The student section of the grandstands along the [[third base]] line is known as Avent's Army. It is named for NC State baseball coach [[Elliott Avent]].<ref name=army>{{cite web|title=Sign Up for Avent's Army |url=http://www.gopack.com/sports/m-basebl/spec-rel/031009aab.html |work=Go Pack |accessdate=16 August 2011 |archiveurl=http://www.webcitation.org/60z2ubIB8?url=http://www.gopack.com/sports/m-basebl/spec-rel/031009aab.html |archivedate=2011-08-16 |deadurl=yes |df= }}</ref>

==See also==
* [[List of NCAA Division I baseball venues]]

==References==
{{reflist}}

==External links==
* [http://www.gopack.com/ViewArticle.dbml?DB_OEM_ID=9200&ATCLID=518479 Doak Field page on GoPack.com]{{dead link|date=December 2016 |bot=InternetArchiveBot |fix-attempted=yes }}

{{North Carolina State University}}
{{NC State Wolfpack baseball navbox}}
{{Atlantic Coast Conference baseball venue navbox}}
{{North Carolina NCAA Division I college baseball venue navbox}}
{{Triangle sports venues}}

[[Category:NC State Wolfpack sports venues]]
[[Category:NC State Wolfpack baseball]]
[[Category:College baseball venues in the United States]]
[[Category:Sports venues in Raleigh, North Carolina]]
[[Category:Baseball venues in North Carolina]]
[[Category:Sports venues completed in 1966]]