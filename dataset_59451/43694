{{Infobox Weapon
|is_missile=yes
|name=SSM-N-9 Regulus II
|image=[[File:Graybackmissle.jpg|300px]]
|caption={{USS|Grayback|SSG-574}} preparing to launch a ''Regulus II'' missile
|origin= 
|type=Cruise missile
|used_by=
|manufacturer=[[Vought|Chance Vought]]
|unit_cost=
|propellant=
|production_date=1956
|service=
|engine=1x [[General Electric J79]]-GE-3 turbojet <br /> 1x [[Pratt & Whitney Rocketdyne|Rocketdyne]] solid-fueled rocket<ref name = "reg II">Parsch, Andreas. [http://www.designation-systems.net/dusrm/m-15.html "Vought SSM-N-9/RGM-15 Regulus II."] ''Directory of U.S. Military Rockets and Missiles'', 2001. Retrieved: 6 January 2013.</ref>
|engine_power={{convert|15600|lbf|kN|abbr=on}} + {{convert|135000 |lbf|kN|abbr=on}}<ref name = "reg II"/>
|weight={{convert|23000|lb|kg}}<ref name = "reg II"/>
|length={{convert|57|ft|6|in|m}}<ref name = "reg II"/>
|diameter={{convert|50|in|m}}<ref name = "reg II"/>
|wingspan={{convert|20|ft|1|in|m}}<ref name = "reg II"/>
|speed=M 2.0<ref name = "reg II"/>
|vehicle_range={{convert|1000|nmi|km|0}}<ref name = "reg II"/>
|ceiling={{convert|59000|ft|m}}<ref name = "reg II"/>
|altitude=
|filling=  [[Mark 27 nuclear bomb|W27 warhead]]<ref name = "reg II"/>
|guidance=Inertial<ref name = "reg II"/>
|detonation= air burst or surface burst (air burst – fireball does not reach the ground, usually at least 10,000 feet in altitude, surface burst – fireball touches the ground, less than 10,000 feet in altitude)
|launch_platform=SSG and SSGN class submarines, cruisers
}}
The '''SSM-N-9 Regulus II''' [[cruise missile]] was a supersonic [[guided missile]] armed with a nuclear warhead, intended for launching from surface ships and submarines of the [[U.S. Navy]] (USN).<ref name = "reg">Koch, Charles A. [http://www.angelfire.com/realm3/roynagl/regulus.htm "Regulus II cruise missile".] ''Regulus II Cruise Missile. Retrieved: 6 January 2013.</ref>

==History==
The limitations of the [[SSM-N-8 Regulus|Regulus I]] were well known by the time it entered service in 1955, so the Navy issued a specification for a surface-launched supersonic shipborne cruise missile, equipped to carry a nuclear warhead, that had greater range, accuracy and resistance to countermeasures.

Development of the Regulus II was well under way when the program was canceled in favor of the [[UGM-27 Polaris]] SLBM (Submarine-Launched Ballistic Missile) system, which gave unprecedented accuracy as well as allowing the launch submarine to stay submerged and covert. Prototype and initial production missiles were later converted to '''Vought KD2U-1''' supersonic target drones for the U.S. Navy and the [[U.S. Air Force]], which used the KD2U-1 during tests of the Boeing IM-99/[[CIM-10 Bomarc]] SAM (Surface to Air Missile).<ref name = "reg"/>

The SSM-N-9a Regulus II was redesignated as the '''RGM-15A''' in June 1963, nearly five years after the missile program had been terminated. At the same time the KD2U-1 target drone was redesignated as the '''Vought MQM-15A'''. Some targets equipped with landing gear were redesignated as '''Vought GQM-15A'''s.<ref name = "reg II"/>

==Design and development==
[[File:SSM-N-9 Regulus II missile launch c1957.jpg|thumb|Regulus II test launch in 1957. The swept-forward Ferri-style intake can be seen, a design note found on a number of contemporary designs like the [[Republic F-105 Thunderchief|F-105]].]]
The major drawback of the original Regulus was the use of radio-command guidance, which required a constant radio link with the launch ship / submarine that was relatively easy to interfere with. The earlier missile also suffered from restricted range which required the launch ship to launch the missile close to the target and remain exposed until the missile hit the target. To alleviate these drawbacks, the Regulus II was designed with an [[inertial navigation system]], which required no further input from the launch ship / boat after launch, and a greater range through improved aerodynamics, larger fuel capacity, and a lower specific fuel consumption from its [[jet engine]].<ref name = "reg"/>

Prototype missiles were built, designated  '''XRSSM-N-9 ''Regulus II''''', with retractable landing gear, to allow multiple launches, and [[Wright J65]]-W-6 engines and [[Aerojet|Aerojet General]] booster, which restricted them to subsonic flight. The first flight of the XRSSM-N-9 took place in May 1956. Beginning in 1958, testing was carried out with the  '''XRSSM-N-9a''', equipped with the [[General Electric J79]]-GE-3 [[turbojet]] and a [[Pratt & Whitney Rocketdyne|Rocketdyne]] [[solid-fueled rocket]] booster to allow the entire flight envelope to be explored. Evaluation and training missiles with retractable undercarriage were produced as the '''YTSSM-N-9a''' and '''TSSM-N-9a''' respectively.<ref name = "reg II"/>

After land-based testing, trials including test missile firings were carried out on board the {{USS|King County|LST-857}}, which had been modified with the replica of a [[submarine]] missile hangar and launching system.

The SSM-N-9 Regulus II missile was intended to be launched from the deck of an SSG (guided missile submarine), and the missile most likely would have been deployed on the two [[Grayback-class submarine]]s and the {{USS|Halibut|SSGN-587}}, which were designed for the missile, and possibly eventually on four heavy [[cruiser]]s that had deployed with Regulus I and 23 other submarines potentially available for conversion. Carrying two Regulus II missiles in a hangar integral with the hull (more on surface ships), submarines and ships equipped with the ''Regulus II'' would have been equipped with the SINS (Ship's [[Inertial navigation system|Inertial Navigation System]]), allowing the [[control system]]s of the missiles to be aligned accurately before launching.<ref name = "reg"/>

[[File:King County AG-857.jpg|thumb|The hangar for the supersonic Regulus II cruise missile being installed aboard {{USS|King County|AG-157}}, a [[Landing Ship, Tank]] (LST) converted to an experimental guided-missile testing ship, on 5 April 1957. {{USS|YD-33}} is doing the lifting.]]

Forty-eight test-flights of Regulus II prototypes were carried out, 30 of which were successful, 14 partially successful and only four failures. A production contract was signed in January 1958 and the only submarine launch was carried out from USS ''Grayback'' in September 1958.<ref name = "reg"/>

Due to the high cost of the missiles (approx one million dollars each), budgetary pressure, and the emergence of the SLBM, the Regulus missile program was terminated on 19 November 1958. Support for the program was finally withdrawn on 18 December 1958, when [[Secretary of the Navy]] [[Thomas S. Gates]] cancelled the project. At the time of the cancelation, Vought had completed 20 missiles with 27 more on the production line.<ref name = "reg"/>

==Description==
The airframe followed contemporary aircraft construction techniques, with weight savings from the use of advanced materials and the short airborne life of the missile. The fuselage was essentially tubular, tapering to a point at the nose, housing the guidance equipment, warhead and systems equipment. The engine was fed with air through a distinctive wedge shaped intake under the center fuselage. Its swept wings attached to its fuselage at the middle position, roughly halfway along its length, and a large swept fin attached to the top of the fuselage at the rear which was sometimes augmented by a large ventral fin at the extreme rear of the fuselage.

Primary control of the Regulus II was through the use of elevons fitted to the outer half of the wing trailing-edges, as the missile was not fitted with a tailplane, and a rudder fitted to the trailing edge of the fin. Flaps were fitted to the inner half of the trailing edge for use during takeoff. Additional stability and control in pitch was provided by small trapezoidal canard foreplanes near the nose of the fuselage.

To launch the missile, the carrier vessel had to surface and deploy the missile and launch apparatus, which consisted of a zero length launcher. Once deployed, the missile had to be linked to the submarine or ships navigation system to align the inertial navigation system and input target co-ordinates. With the navigation system ready and launch authorisation given, the missile engine would be run-up to full power with afterburner and the large solid-fuelled rocket booster ignited, immediately the missile would leave the zero length launcher and  continue to the target autonomously.

==Regulus Target Drones==
Suitable missiles from the development program and production line were converted to supersonic [[target drone]]s as the KD2U-1, later redesignated as the MQM-15A and GQM-15A. These targets were used for training of [[BOMARC]] [[surface-to-air missile]] crews firing from [[Santa Rosa Island, Florida]], and controlled by the [[Montgomery Air Defense Sector]], [[Gunter Air Force Base]], [[Montgomery, Alabama]]. The KD2U-1 targets were launched from the [[Eglin Gulf Test Range]] base near [[Ft. Walton Beach, Florida]]. Drone flights at Eglin commenced on 3 September 1959, making 46 flights with 13 missiles. After the BOMARC tests the remaining missiles were moved to [[Naval Station Roosevelt Roads]], [[Puerto Rico]] by 30 September 1961, where flights were begun to test Tartar, Terrier, and Talos [[surface-to-air missile]]s. Upon completion of the testing in Puerto Rico in 1963, the Regulus II drones were moved to [[NAS Point Mugu]], [[California]], where they remained in use until December 1965.<ref name = "reg"/>

==Surviving Regulus II missiles==
;[[Frontiers of Flight Museum]], [[Dallas Love Field]], Texas
:A Regulus II missile
;Point Mugu Missile Park, [[Naval Air Station Point Mugu]], California
:The museum's collection includes both a Regulus and a Regulus II missile
;[http://www.memorialmuseum.org The U.S. Veterans Memorial Museum], Huntsville, Alabama
:A Regulus II missile handling training device (non-flyable)

===Variants===
[[File:SSM-N-9 Regulus II drawings.PNG|thumb|upright=1.6]]
; SSM-N-9 Regulus II: The basic designation of the missile, pre-1964.
; SSM-N-9a Regulus II: The designation of production missiles, pre-1964.
; XRSSM-N-9 ''Regulus II'': Prototype missiles fitted with retractable landing gear for land based development flights, powered by [[Wright J65]] turbojet engines and [[Aerojet|Aerojet General]] booster rockets.
; XRSSM-N-9a: Prototype missiles fitted with retractable landing gear for land based development flights, powered by [[General Electric J79]] [[turbojet]] engines and [[Pratt & Whitney Rocketdyne|Rocketdyne]] booster rockets.
;YTSSM-N-9a: Development Training missiles with retractable landing gear.
;TSSM-N-9a: Production training missiles with retractable landing gear.
;KD2U-1:Pre-1962 designation of the target drone version of the Regulus II
;RQM-15A: KD2U-1Regulus II target re-designated in April 1963.
;MQM-15A: KD2U-1 Target drones used for research and as targets for the [[CIM-10 Bomarc|IM-99 Bomarc]] [[Surface-to-air missile|SAM]].
;GQM-15A: Gear equipped KD2U-1 target drones re-designated.

==Operators==
* [[United States Navy]]
* [[United States Air Force]]

==See also==
* [[List of missiles]]
* [[SSBN Deterrent Patrol insignia]]

==References==
{{reflist}}

==External links==
* [http://www.carolinasaviation.org/collections/missiles/regulus.html Carolinas Aviation Museum]
* [http://www.designation-systems.net/dusrm/m-15.html]
* [http://www.globalsecurity.org/wmd/systems/regulus2.htm]
* [http://aboutsubs.com/halibut.htm]
* [http://www.vectorsite.net/twcruz_3.html#m3]
* [http://www.angelfire.com/realm3/roynagl/regulus.htm#regulus]
* [http://www.astronautix.com/lvs/regulus2.htm]
* [http://sobchak.wordpress.com/2009/12/11/ad-vought-regulus-ii-1950s/]
* [http://www.navsource.org/archives/14/26idx.htm]
* [http://www.corbisimages.com/Enlargement/UKD2883INP.html]



{{US missiles}}
{{USN missiles}}
{{USN drones}}

{{DEFAULTSORT:Ssm-N-9 Regulus Ii}}
[[Category:Nuclear cruise missiles of the United States]]
[[Category:Nuclear cruise missiles of the United States Navy]]
[[Category:Cruise missiles of the Cold War]]
[[Category:Nuclear missiles of the Cold War]]
[[Category:Nuclear history of the United States]]
[[Category:Cold War weapons of the United States]]
[[Category:Vought]]