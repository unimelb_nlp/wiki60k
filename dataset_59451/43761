<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name= Rysachok
 | image=Rysachok at MAKS-2011 airshow (2).jpg
 | caption=Rysachove at MAKS '11 airshow
}}{{Infobox Aircraft Type
 | type=Twin engine [[utility aircraft]]
 | national origin= [[Russia]]
 | manufacturer= [[Technoavia]]
 | designer=
 | first flight=3 December 2010
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=Five airframes, two static, by 2011
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Technoavia Rysachok''' (Ru:Рысачок, {{lang-en|Racehorse}}) is a general purpose, twin [[turboprop]]-powered engined light [[utility aircraft]], designed and built in [[Russia]] by [[Technoavia]]. Certification is expected 2012; by autumn 2011 three flight pre-production examples had been completed and flown.

==Design and development==

The Rysachok programme development began in 2006. It was designed as a twin engine conversion trainer but is now seen more generally as an [[Antonov An-2]] replacement in medical, survey, parachuting, navigator and air engineer training and other light transport rôles.<ref name=JAWA11/>

The Rysachok is a conventionally laid out [[low wing]], twin engine [[monoplane]].  Outboard of the engines the wings have constant [[chord (aircraft)|chord]] and blunt [[wing tip]]s; inboard, the chord increases toward the [[fuselage]] via sweep on the [[trailing edge]]. The starboard [[aileron]] carries a [[trim tab]] and the wing has two-section [[flap (aircraft)|flaps]].  The [[tailplane]], mounted at the top of the fuselage, has constant chord.  The [[fin]] is straight edged and swept. All the tail surfaces have trim tabs and are [[balanced rudder|horn balanced]].<ref name=JAWA11/>

The Rysachok has a flat sided fuselage with, on each side, a cockpit crew door and four square windows. On the port side the rearmost window is in a wide, sliding freight door behind the wing.  Nineteen can be carried in an all-passenger configuration; the navigation/engineer training layout allows for up to nine students and instructors with appropriate repeat instrumentation. The Rysachok can carry six stretcher cases and a medical attendant in its medical evacuation form.<ref name=JAWA11/>

Current pre-production aircraft are powered by 580&nbsp;kW (778&nbsp;hp) [[Walter M601]]F turboprop engines,<ref name=JAWA11/> later production models will have 596&nbsp;kW (800&nbsp;hp) [[General Electric H80]] turboprops a development of the M601. The Rysachok has a [[tricycle undercarriage]] with inward retracting single mainwheels and a forward retracting twin nosewheel.<ref name=JAWA11/>

The Rysachok first flew on 3 December 2010.<ref name=JAWA11/> By mid-2011, five pre-production airframes had been completed.<ref name=FlightG/> Two were for static testing, two are at the TsAGI flight research institute and a fifth is undergoing flight trials for certification, expected in 2012.<ref name=FlightG/> Reports on orders differ: Jane's 2011/12<ref name=JAWA11/> notes a 2008 order for thirty from a "government transport agency" but Flightglobal's<ref name=FlightG/> more recent account claims "no firm orders" in mid-2011.

The Technoavia Rysachok made its first flight with [[General Electric H80]] engines in March 2014.<ref name=GE/> A 2014 report predicted that production would start in 2015.<ref name=Volga/>
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (prototype) ==
[[File:Рысачок на МАКС-2011.jpg|thumb|Technoavia Rysachok demonstrated at [[MAKS Airshow|MAKS]] 2011]]
{{Aircraft specs
|ref=All the World's Aircraft<ref name=JAWA11/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=1
|capacity=up to 19 passengers
|length m=12.44
|length note=
|span m=18.00
|height m=5.38
|height note=
|wing area m=30.50
|wing area note=gross
|aspect ratio=10.6
|airfoil=
|empty weight kg=2770
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=5700
|max takeoff weight note=
|fuel capacity=1,900 L (502 US gal; 418 Imp gal); maximum weight 1,520 kg (2,620lb)
|more general=
*'''Maximum payload:1,570 kg (3,461 lb)''' 
<!--
        Powerplant
-->
|eng1 number=2
|eng1 name=[[Walter M601]]F
|eng1 type=turboprop
|eng1 kw=580
|eng1 note=
|power original=
|more power=

|prop blade number=5
|prop name=
|prop dia m=2.30
|prop dia note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=391
|cruise speed note=maximum, economical 251 km/h (156 mph; 135 kn)
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=2000
|range note=
|ferry range km=2480
|ferry range note=
|endurance=9 hr 30 min
|ceiling m=6000
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=186.9
|wing loading note=

|power/mass=

|more performance=
|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{commons category|Rysuchok}}
{{reflist|refs=

<ref name=JAWA11>{{cite book |title= Jane's All the World's Aircraft 2011–12|last= Jackson |first= Paul |coauthors= |edition= |year=2011|publisher=IHS Jane's|location= Coulsdon, Surrey|isbn=978-0-7106-2955-5|pages=500–1}}</ref>

<ref name=FlightG>{{cite web |url=http://www.flightglobal.com/news/articles/maks-tskb-progress-flight-tests-new-twin-turboprop-361088/|title=MAKS: TsKB Progress flight tests new twin-turboprop |author= |date=22 August 2011 |work=Flightglobal |publisher= |accessdate=11 December 2011}}</ref>

<ref name=GE>{{cite web|url=http://www.ainonline.com/aviation-news/business-aviation/2014-05-21/ge-turboprops-come-closer-taking-pratt-dominance|title=GE Turboprops Come Closer To Taking On Pratt Dominance}} Reports show the Production will start in 2015.</ref>

<ref name=Volga>{{cite web|url=http://www.ruaviation.com/news/2014/8/9/2526/print/|title=Russian Aviation|accessdate=6 January 2017}}</ref>

}}

==External links==
* http://en.samspace.ru/products/national_economy_products&consumer_goods/rysachok/

[[Category:Twin-engined tractor aircraft]]
[[Category:Russian civil utility aircraft 2010–2019]]
[[Category:Progress aircraft|Rysachok]]
[[Category:Low-wing aircraft]]
[[Category:Turboprop aircraft]]