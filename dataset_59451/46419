{{ WAP assignment | course = Wikipedia:USEP/Courses/Introduction to Mass Communications (Chad Tew) | university = University of Southern Indiana | term = 2012 Q3 | project = WikiProject Journalism }} 
{{Infobox person
| honorific_prefix          = 
| name                      = Eliseo Barrón
| honorific_suffix          = 
| native_name               = Eliseo Barrón Hernández
| native_name_lang          = Spanish
| image                     = 
| image_size                = 
| alt                       = 
| caption                   = 
| birth_name                = 
| birth_date                = 1973
| birth_place               = Torreón, Coahuila, Mexico
| disappeared_date          = 25 May 2009
| disappeared_place         = Home in Gómez Palacio, Tlahualilo Municipality, Durango, Mexico
| disappeared_status        = 
| death_date                = 26 May 2009 (Age 36)
| death_place               = 
| death_cause               = Gunshot wound
| body_discovered           = Gómez Palacio, Tlahualilo Municipality, Durango, Mexico
| resting_place             = 
| resting_place_coordinates = <!-- {{Coord|LAT|LONG|type:landmark|display=inline}} -->
| monuments                 = 
| residence                 = 
| nationality               = Mexican
| other_names               = 
| ethnicity                 = <!-- Ethnicity should be supported with a citation from a reliable source -->
| citizenship               = 
| education                 = 
| alma_mater                = Instituto Tecnológico Agropecuario número 10, Agricultural engineering
| occupation                = Journalist
| years_active              = 10+
| employer                  = ''Milenio-La Opinión''
| organization              = 
| agent                     = 
| known_for                 = 
| notable_works             = 
| style                     = 
| influences                = 
| influenced                = 
| home_town                 = 
| salary                    = 
| net_worth                 = <!-- Net worth should be supported with a citation from a reliable source -->
| height                    = <!-- {{height|m=}} -->
| weight                    = <!-- {{convert|weight in kg|kg|lb}} -->
| television                = 
| title                     = 
| term                      = 
| predecessor               = 
| successor                 = 
| party                     = 
| movement                  = 
| opponents                 = 
| boards                    = 
| religion                  = <!-- Religion should be supported with a citation from a reliable source -->
| denomination              = <!-- Denomination should be supported with a citation from a reliable source -->
| criminal_charge           = <!-- Criminality parameters should be supported with citations from reliable sources -->
| criminal_penalty          = 
| criminal_status           = 
| spouse                    = 
| partner                   = Married
| children                  = 2 daughters
| parents                   = 
| relatives                 = 
| callsign                  = 
| awards                    = 
| signature                 = 
| signature_alt             = 
| signature_size            = 
| module                    = 
| module2                   = 
| module3                   = 
| module4                   = 
| module5                   = 
| module6                   = 
| website                   = <!-- {{URL|Example.com}} -->
| footnotes                 = 
| box_width                 = 
}}''' Eliseo Barrón Hernández ''' (1973 - 27 May 2009) was a crime journalist for [[Milenio]]'s ''La Opinión de Torreón'', [[Torreón, Coahuila|Torreón]], [[Coahuila]], [[Mexico]], who was brutally beaten in front of his family in his home in [[Gómez Palacio, Durango|Gómez Palacio]], [[Tlahualilo Municipality|Tlahualilo]], [[Durango]] on 25 May 2009, abducted, and later found dead in an irrigation ditch two days later with a gunshot wound to his head.<ref name=nytimes>{{cite web|last=Lacey |first=Marc |url=https://query.nytimes.com/gst/fullpage.html?res=9D03E2DE123FF93AA15756C0A96F9C8B63 |title=WORLD BRIEFING - THE AMERICAS - Mexico - Reward Offered For Help in Solving Journalist's Murder - Brief - NYTimes.com |publisher=New York Times |date=2009-05-29 |accessdate=2012-11-18}}</ref><ref name=ahorasi>{{cite web|language=Spanish|url=http://www.ahorasi.com/hallan-muerto-a-reportero-eliseo-barron-hernandez-secuestrado-en-norte-de-mexico/#more-8246 |title=Hallan muerto a reportero Eliseo Barrón Hernández secuestrado en norte de México |publisher=Ahora Si |date= |accessdate=2012-11-18}}</ref><ref name=cpj>{{cite web| url=http://cpj.org/killed/2009/eliseo-barron-hernandez.php |title=Eliseo Barrón Hernández - Journalists Killed |publisher=Committee to Protect Journalists |date=2009-05-25 |accessdate=2012-11-18}}</ref><ref name=rsf>{{cite web|url=http://en.rsf.org/mexico-crime-reporter-abducted-and-killed-27-05-2009,33162.html |title=Crime reporter abducted and killed in Durango state |publisher=Reporters Without Borders |date=2009-05-27 |accessdate=2012-11-18}}</ref>

Barrón was the second reporter killed in the state of Durango, Mexico in a one-month period, and he was one of three reporters who were killed during the same year and who were reporting about police corruption in the state of Durango. The other two are Carlos Ortega Samper and [[Bladimir Antuna|Bladimir Antuna García]].<ref name="cpj2">{{cite web| url=http://cpj.org/killed/2009/carlos-ortega-samper.php |title=Carlos Ortega Samper - Journalists Killed |publisher=Committee to Protect Journalists |date=2009-05-03|accessdate=2012-11-18}}</ref><ref name=cpjbag>{{cite web|url=http://cpj.org/killed/2009/bladimir-antuna-garcia.php|title=Bladimir Antuna García|publisher=Committee to Protect Journalists|date=2009-11-02|accessdate=2012-11-18}}</ref>

== Personal ==
Eliseo Barrón, born 1973, and the fifth of seven children of Adelaido Barrón Pérez and María de Lourdes Fernández.<ref name=mileniobio>{{cite web|language=Spanish |url=http://www.milenio.com/cdb/doc/impreso/8590583 |archive-url=https://archive.is/20130128190817/http://www.milenio.com/cdb/doc/impreso/8590583 |dead-url=yes |archive-date=2013-01-28 |title=Caen homicidas de Eliseo Barrón; son de Los Zetas |publisher=Milenio.com |date=2009-06-12 |accessdate=2012-11-18 }}</ref> He studied agricultural engineering at Instituto Tecnológico Agropecuario número 10.<ref name=mileniobio /> He was 36 years old at the time of his murder. He was beaten in front of his wife and their two young daughters, who were ages 1 and 3, at their family home in [[Gómez Palacio, Durango]] before his abduction.<ref name=rsf /> Torreón, Coahuila and Gómez Palacio, Durango are part of a large metropolitan area in Mexico.

== Career ==
Eliseo Barrón began his career in journalism with the newspaper [[Zócalo]] in [[Acuña, Coahuila|Acuña]], Coahuila, where he worked for six months, after graduating from college.<ref name=mileniobio /> Afterwards, he worked as a reporter and photographer for ''La Opinión'' in Toreeón for over 10 years. He often covered drug trafficking and was killed for writing about a police corruption scandal two days before his death in May 2009.<ref name=rsf /><ref name=ifex>{{cite web|url=http://www.ifex.org/mexico/2009/05/27/comm_barron_killed/ |title=Crime reporter found dead in Durango |publisher=IFEX |date=2009-05-27 |accessdate=2012-11-18}}</ref> It was reported that 302 police and at least 20 others were fired as a result of coverage about the corruption, which is why it is seen as a motive for his murder.<ref name=nytimes /><ref name=ahorasi />

Barrón collaborated with fellow crime journalist Carlos Ortega Samper, who had been killed just less than a month before Eliseo.<ref name="rsf" />

== Death ==
{{Location map+|Mexico
 |AlternativeMap = Mexico States blank map.svg
 | float = left
 | position = left
 | width = 300
 |caption= Mentioned locations within Mexico relative to the capital Mexico City.
 |alt = THE ADDED CITY is located in Mexico.
 |places=
  {{Location map~ | Mexico 
  |lat_deg=19 |lat_min=25 |lat_sec=57 |lat_dir=N
  |lon_deg=99 |lon_min=07 |lon_sec=59 |lon_dir=W 
  |position=left 
  |background=#FFFFFF 
  |label=Mexico City
  }}
{{Location map~ | Mexico 
  |lat_deg=25 |lat_min=34 |lat_sec=1.9934 |lat_dir=N 
  |lon_deg=103 |lon_min=29 |lon_sec=48.1348 |lon_dir=W 
  |position=right 
  |background=#FFF 
  |label= Gomez Palacio
  }}
{{Location map~ | Mexico 
  |lat_deg=24 |lat_min=0 |lat_sec=22.7743 |lat_dir=N 
  |lon_deg=104 |lon_min=38 |lon_sec=41.1914 |lon_dir=W 
  |position=left 
  |background=#FFF 
  |label= Durango
  }}

}}Barrón was in his home in Gómez Palacio on 25 May 2009, when about eight men broke into his home at around 8 p.m., brutally beat the journalist while his wife and two children were present, and then abducted him. He was missing for one day. On 26 May 2009, his body was found in a ditch in the same locality with a gunshot wound to his head, at which time he was pronounced dead.<ref name=nytimes /><ref name=borderreporter>{{cite web|url=http://borderreporter.com/2009/05/journalist-turns-up-dead/ |title=Journalist Turns up Dead - Border Reporter – News That Crossed The Line |publisher=Borderreporter.com |date=2009-05-26 |accessdate=2012-11-18}}</ref> The [[Los Angeles Times|LA Times]] reports that a note was found on his dead body saying "This happened to me for giving information to soldiers and for writing too much."<ref name=latimes>{{cite web|author=Tony Cohan|author2=Tamsin Mitchell |url=http://articles.latimes.com/2010/feb/15/opinion/la-oe-cohan15-2010feb15 |title=Mexico's Killing Fields |publisher=Los Angeles Times |date=15 February 2010 |accessdate=2012-11-18}}</ref>

Banners placed around the city of Torreón on the day of Barrón's funeral at first indicated that [[Joaquín Guzmán Loera|Joaquín “El Chapo” Guzmán]] of the [[Sinaloa Cartel]] had taken responsibility for Barrón's death as the banners threatened journalists and soldiers.<ref name=cpj /> However, [[SEDENA]] detained five drug cartel members in Gómez Palacio and announced on 11 June 2009 that one of the men involved in the murder said [[Raúl Lucio Hernández Lechuga|Raúl "Lucifer" Hernández]] of [[Los Zetas]] had ordered Barrón's murder. Three out of the five detained members of Los Zetas admitted to having participated in his murder, as well as Martha Georgina Correa Alvarado,<ref name=elsiglodetorreon>{{cite web|language=Spanish|publisher=El Siglo de Durango |url=http://www.elsiglodetorreon.com.mx/noticia/448255.trasladan-a-presuntos-asesinos-de-periodista-a-saltillo.html |title=Trasladan a presuntos asesinos de periodista a Saltillo / Durango |date=2009-07-21 |accessdate=2012-11-18}}</ref><ref name=milenio>{{cite news|language=Spanish |url=http://www.milenio.com/cdb/doc/noticias2011/0040b737a39dc5ab49b345a8c16f80c6 |archive-url=https://archive.is/20130128174917/http://www.milenio.com/cdb/doc/noticias2011/0040b737a39dc5ab49b345a8c16f80c6 |dead-url=yes |archive-date=2013-01-28 |title=El Lucifer ordenó matar a Eliseo Barrón para intimidar a la prensa |publisher=Milenio.com |date=2009-06-13 |accessdate=2012-11-18 }}</ref><ref name=vanguardia>{{cite web|language=Spanish |url=http://www.vanguardia.com.mx/detienen_a_asesinos_de_reportero_eliseo_barron_hernandez-362824.html |title=Detienen a asesinos de reportero Eliseo Barrón Hernández |publisher=Vanguardia |date=2009-06-12 |accessdate=2012-11-18 }}{{dead link|date=December 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref><ref name=cpjsuspects>{{cite web|author=Five suspects detained in Mexican journalist's killing |url=http://cpj.org/2009/06/five-suspects-detained-in-mexican-journalists-kill.php |title=Five suspects detained in Mexican journalist's killing - Committee to Protect Journalists |publisher=Cpj.org |date=2009-06-12 |accessdate=2012-11-18}}</ref><ref name=ifexsuspects>{{cite web|url=http://www.ifex.org/mexico/2009/06/17/suspects_arrested/ |title=Five suspects detained in connection with journalist's murder, one admits to firing fatal shot |publisher=IFEX |date=2009-06-12 |accessdate=2012-11-18}}</ref> They were later order to stand trial for the murder.<ref name=informador>{{cite web|language=Spanish|url=http://www.informador.com.mx/mexico/2009/133868/6/inician-juicio-a-presuntos-homicidas-de-periodista-mexicano.htm |title=Inician juicio a presuntos homicidas de periodista mexicano :: El Informador |publisher=Informador.com.mx |date=2012-06-12 |accessdate=2012-11-18}}</ref> who was deputy director of in the Directorate of Forensic Services.<ref name=mileniobio />

== Context ==
Corruption in Durango was highlighted by the event in 2009.

[[Sinaloa]], Durango, and Chihuahua form a "Golden Triangle" of the drug trade in Mexico, and Los Zetas have moved into the Durango and Torreón areas.<ref>{{cite web|url=http://www.borderlandbeat.com/2011/09/battle-for-durango-and-torreon.html |title=The Battle for Durango and Torreon |publisher=[[Borderland Beat]] |date=2011-09-03 |accessdate=2012-11-18}}</ref> In 2009, those areas were controlled by "El Chapo" Guzmán, who was reported by [[Forbes]] magazine to be one of the most powerful drug cartel leaders of Mexico.<ref name=lacroix>{{cite web|url=http://www.la-croix.com/Religion/S-informer/Actualite/Au-Mexique-l-Eglise-craint-des-represailles-des-cartels-de-la-drogue-_NG_-2009-04-24-534010 |title=Au Mexique, l'Eglise craint des représailles des cartels de la drogue |language=Spanish |publisher=La-Croix.com |date=2009-04-24 |accessdate=2012-11-18}}</ref>

In mid-April 2009, Mgr. Hector Gonzalez Martinez, a [[Roman Catholic Archdiocese of Durango|Roman Catholic Archbishop]] in Durango, indicated that "El Chapo" Guzmán had made Durango his home and said Mexican authorities were doing nothing about it. His statement was in support of an ecclesiastical statement that showed that the [[Mexican Drug War]] was having an impact on the priesthood in both deaths and in causing hundreds of priests vacating their parishes. His statement brought to public attention Guzmán's presence in the Durango area.<ref name=lacroix /> By 2011, the same Archibishop said that Los Zetas were now in control of Durango <ref>{{cite web|last=Byrne |first=Edward V. |url=http://www.mexicogulfreporter.com/2011/11/los-zetas-rule-durango-and-zacatecas.html |title=Mexico and Gulf Region Reporter: Los Zetas rule Durango and Zacatecas states, says gutsy Mexican archbishop |publisher=Mexicogulfreporter.com |date=2011-11-24 |accessdate=2012-11-18}}</ref>

== Impact ==
Eliseo Barrón was the second journalist killed in Durango in a one-month period. Carlos Ortega Samper was murdered in Santa María El Oro, Durango, Mexico on 3 May 2009. Ortega had also been reporting on this corruption case.<ref name=cpj2 /> Barrón's collaborator Bladimir Antuna García had originally revealed the corruption case and he was murdered later on 2 November 2009.<ref name=cpjbag />

== Reactions ==
A reward of $380,000 was offered from the federal prosecutors to anyone that would help in solving the murder mystery of this case.<ref name=nytimes />

[[Koïchiro Matsuura]], who is director-general of UNESCO, said "I trust that the Mexican authorities, both local and Federal, will do everything to elucidate this crime, the latest in a long list of attacks on journalists in this country. Letting such attacks go unpunished only encourages more crime."<ref name=unesco>{{cite web|url=http://portal.unesco.org/ci/en/ev.php-URL_ID=28718&URL_DO=DO_TOPIC&URL_SECTION=201.html |title=Director-General condemns murder of Mexican journalist Eliseo Barrón Hernández |publisher=Portal.unesco.org |date=2009-06-04 |accessdate=2012-11-18}}</ref>

==See also==
*[[Mexican Drug War]]
*[[List of journalists killed in Mexico]]

== References ==
{{Reflist}}

{{Mexican Drug War}}

{{DEFAULTSORT:Barron, Eliseo}}
[[Category:Articles created via the Article Wizard]]
[[Category:1973 births]]
[[Category:2009 deaths]]
[[Category:Deaths by firearm in Mexico]]
[[Category:Assassinated Mexican journalists]]
[[Category:Journalists killed in the Mexican Drug War]]
[[Category:Murder in 2009]]
[[Category:Writers from Coahuila]]
[[Category:People from Torreón]]