{{Use mdy dates|date=November 2013}}
{{good article}}
{{Infobox MLB All-Star Game
| image = [[File:2007 MLB All-Star Game Logo.svg|150px]]
| year = 2007
| visitor = '''[[American League]]'''
| top1 = 0
| top2 = 0
| top3 = 0
| top4 = 0
| top5 = 2
| top6 = 1
| top7 = 0
| top8 = 2
| top9 = 0
| visitor_r = 5
| visitor_h = 10
| visitor_e = 0
| home = [[National League]]
| bot1 = 1
| bot2 = 0
| bot3 = 0
| bot4 = 0
| bot5 = 0
| bot6 = 1
| bot7 = 0
| bot8 = 0
| bot9 = 2
| home_r = 4
| home_h = 9
| home_e = 1
| date = July 10, 2007
| venue = [[AT&T Park]]
| city = San Francisco, California
| VisitorManager = [[Jim Leyland]]
| VisitorManagerTeam = [[Detroit Tigers|DET]]
| HomeManager = [[Tony La Russa]]
| HomeManagerTeam = [[St. Louis Cardinals|STL]]
| MVP = [[Ichiro Suzuki]]
| MVPTeam = [[Seattle Mariners|SEA]]
| television = [[Major League Baseball on Fox|Fox]]
| tv_announcers = [[Joe Buck]] and [[Tim McCarver]]
| radio = [[Major League Baseball on ESPN Radio|ESPN]]
| radio_announcers = [[Dan Shulman]] and [[Dave Campbell (infielder)|Dave Campbell]]
| attendance = 43,965
| firstpitch = [[Willie Mays]]
}}

The '''2007 Major League Baseball All-Star Game''' was the 78th midseason exhibition between the [[all-star game|all-stars]] of the [[American League]] (AL) and the [[National League]] (NL), the two leagues comprising [[Major League Baseball]]. The game was held on July 10, 2007, at [[AT&T Park]], the home of the NL's [[San Francisco Giants]]. It marked the third time that the Giants hosted the All Star Game since moving to San Francisco for the 1958 season. The [[1961 Major League Baseball All-Star Game (first game)|1961]] and [[1984 Major League Baseball All-Star Game|1984]] All Star Games were played at the Giants former home [[Candlestick Park]], and the fourth overall in the [[San Francisco Bay Area|Bay Area]], with the Giants [[Bay Bridge Series|bay area rivals]] the [[Oakland Athletics]] hosting once back in [[1987 Major League Baseball All-Star Game|1987]], and the second straight held in an NL ballpark.

The American League defeated the National League by a score of 5–4. [[Ichiro Suzuki]] won the MVP award for the game for hitting the first [[inside-the-park home run]] in All-Star history. As per the 2006 [[Collective bargaining|Collective Bargaining Agreement]], the [[2007 American League Championship Series|American League champion]] (which eventually came to be the [[Boston Red Sox]]) received [[Home advantage|home field advantage]] in the [[2007 World Series]].<ref name="CBA">{{cite web |url=http://mlb.mlb.com/NASApp/mlb/news/article.jsp?ymd=20061024&content_id=1722211&vkey=news_mlb&fext=.jsp&c_id=mlb |title=MLB, union announce new labor deal |first=Barry M. |last=Bloom |publisher=MLB.com |date=October 25, 2006 |accessdate=October 30, 2006}}</ref> The victory was the 10th consecutive (excluding the [[2002 Major League Baseball All-Star Game|2002 tie]]) for the AL, and their 11-game unbeaten streak is only beaten by the NL's 11-game winning streak from 1972 to 1982 in All-Star history.

==Background==
As with each All-Star Game since 1970, the eight starting position players (with no [[designated hitter]] due to playing in an NL stadium) of each league were elected by fan balloting. The remaining players were selected by a players' vote, each league's team manager, and [[All-Star Final Vote|a second fan balloting]] to add one more player to each roster. In all, 32&nbsp;players were selected to each league's team, not including players who decline to play due to injuries or personal reasons.

The Giants were awarded the game on February 9, 2005.<ref name="Giants">{{cite web |url=http://www.mlb.com/news/article.jsp?ymd=20050208&content_id=940943&vkey=news_mlb&fext=.jsp&c_id=null |title=Giants to host 2007 All-Star Game |first=Barry M. |last=Bloom |publisher=MLB.com |date=February 9, 2005 |accessdate=June 6, 2007}}</ref> The game marked the first time since 1962 that one league hosted consecutive All-Star Games, after [[Pittsburgh]], Pennsylvania, hosted the game in [[2006 Major League Baseball All-Star Game|2006]].

The game was the fifth straight All-Star Game to decide home-field advantage in the [[World Series]].<ref name="CBA"/> The AL entered the game on a ten-game unbeaten streak (nine wins, with one tie in [[2002 Major League Baseball All-Star Game|2002]]). The NL was looking for their first win since the [[1996 Major League Baseball All-Star Game|1996 game]] in [[Philadelphia]].

==Fan balloting==

===Starters===
Balloting for the 2007 All-Star Game starters (excluding pitchers) began on April 27 and continued through June 28.  The top vote-getters at each position and the top three among outfielders, are named the starters for their respective leagues. The results were announced on July 1.<ref name="starters">{{cite web |url=http://mlb.mlb.com/news/article.jsp?ymd=20070701&content_id=2059925&vkey=allstar2007&fext=.jsp |title=Best of veterans, youngsters at Classic |first=Barry M. |last=Bloom |publisher=MLB.com |date=July 1, 2007 |accessdate=July 1, 2007| archiveurl= https://web.archive.org/web/20070703180830/http://mlb.mlb.com/news/article.jsp?ymd=20070701&content_id=2059925&vkey=allstar2007&fext=.jsp| archivedate= July 3, 2007 <!--DASHBot-->| deadurl= no}}</ref> About 18.5&nbsp;million votes were cast by close to twelve million fans.<ref name="starters"/> [[Alex Rodriguez]] was the leading vote-getter with 3,890,515&nbsp;votes, easily outpacing his [[New York Yankees|Yankees]] teammate [[Derek Jeter]] by over 700,000&nbsp;votes. [[Ken Griffey, Jr.]] was the top vote-getter in the [[National League]], with 2,986,818&nbsp;votes.<ref name="starters"/>

===Final roster spot===
After the rosters were announced, a second round of fan voting, the [[Monster (website)|Monster]] [[All-Star Final Vote]], was commenced to determine the occupant of the final roster spot for each team.<ref name="finalvote">{{cite web |url=http://mlb.mlb.com/news/article.jsp?ymd=20070701&content_id=2060174&vkey=allstar2007&fext=.jsp |title=Monster All-Star Final Vote is under way |first=Mark |last=Newman |publisher=MLB.com |date=July 1, 2007 |accessdate=July 1, 2007| archiveurl= https://web.archive.org/web/20070703180200/http://mlb.mlb.com/news/article.jsp?ymd=20070701&content_id=2060174&vkey=allstar2007&fext=.jsp| archivedate= July 3, 2007 <!--DASHBot-->| deadurl= no}}</ref> This round lasted until July 5. [[Chris Young (pitcher)|Chris Young]] and [[Hideki Okajima]] were elected to represent the National League and American League, respectively, in the All-Star Game as first time All-Stars.<ref>{{cite web|url=http://mlb.mlb.com/news/article.jsp?ymd=20070705&content_id=2069109&vkey=allstar2007&fext=.jsp|title=Young, Okajima win Final Vote|accessdate=July 5, 2007|date=July 5, 2007|author=Newman, Mark|publisher=MLB Advanced Media, L.P.}}</ref> All ten players included in the balloting were [[pitcher]]s, a first for the event.

{| class="wikitable"
|-
! Player
! Team
! Pos.
! Experience<br>(All Star/<br>Seasons)
! Player
! Team
! Pos.
! Experience<br>(All Star/<br>Seasons)
|-
! colspan="4" style="background-color:#FFCCCC;"| American League
! colspan="4" style="background-color:#D0E7FF;"| National League
|-
|style="background:yellow;"| '''[[Hideki Okajima]]'''
| [[Boston Red Sox|BOS]]
| [[Relief pitcher|RP]]
| align="center"| (0/1)
|style="background:yellow;"| '''[[Chris Young (pitcher)|Chris Young]]'''
| [[San Diego Padres|SD]]
| [[Starting pitcher|SP]]
| align="center"| (0/4)
|-
| [[Jeremy Bonderman]]
| [[Detroit Tigers|DET]]
| SP
| align="center"| (0/5)
| [[Carlos Zambrano]]
| [[Chicago Cubs|CHC]]
| SP
| align="center"| (2/7)
|-
| [[Pat Neshek]]
| [[Minnesota Twins|MIN]]
| RP
| align="center"| (0/2)
| [[Roy Oswalt]]
| [[Houston Astros|HOU]]
| SP
| align="center"| (2/7)
|-
| [[Kelvim Escobar]]
| [[Los Angeles Angels of Anaheim|LAA]]
| SP
| align="center"| (0/11)
| [[Brandon Webb]]
| [[Arizona Diamondbacks|ARZ]]
| SP
| align="center"| (1/5)
|-
| [[Roy Halladay]]
| [[Toronto Blue Jays|TOR]]
| SP
| align="center"| (4/10)
| [[Tom Gorzelanny]]
| [[Pittsburgh Pirates|PIT]]
| SP
| align="center"| (0/3)
|}

==Rosters==
Players in ''italics'' have since been inducted into the [[National Baseball Hall of Fame]].
{{col-begin}}
{{col-2}}

===American League===
{| class="wikitable" style="font-size: 100%; text-align:center;"
|-
!colspan="4"|Elected starters
|-
!Position!!width="150"|Player!!width="100"|Team!!All-Star Games
|-
|[[Catcher|C]]||''[[Iván Rodríguez]]''||[[Detroit Tigers|Tigers]]||14
|-
|[[First baseman|1B]]||[[David Ortiz]]||[[Boston Red Sox|Red Sox]]||4
|-
|[[Second baseman|2B]]||[[Plácido Polanco]]||[[Detroit Tigers|Tigers]]||1
|-
|[[Third baseman|3B]]||[[Alex Rodriguez]]||[[New York Yankees|Yankees]]||11
|-
|[[Shortstop|SS]]||[[Derek Jeter]]||[[New York Yankees|Yankees]]||align="center"|8
|-
|[[Outfielder|OF]]||[[Vladimir Guerrero]]||[[Los Angeles Angels of Anaheim|Angels]]||8
|-
|[[Outfielder|OF]]||[[Magglio Ordóñez]]||[[Detroit Tigers|Tigers]]||6
|-
|[[Outfielder|OF]]||[[Ichiro Suzuki]]||[[Seattle Mariners|Mariners]]||7
|-
|}

{| class="wikitable" style="font-size: 100%; text-align:center;"
|-
!colspan="4"|Pitchers
|-
!Position!!width="150"|Player!!width="100"|Team!!All-Star Games
|-
|[[Pitcher|P]]||[[Josh Beckett]]||[[Boston Red Sox|Red Sox]]||1
|-
|[[Pitcher|P]]||[[Dan Haren]]||[[Oakland Athletics|Athletics]]||1
|-
|[[Pitcher|P]]||[[Bobby Jenks]] {{Ref|noplay|a}}||[[Chicago White Sox|White Sox]]||2
|-
|[[Pitcher|P]]||[[John Lackey]] {{Ref|noplay|a}}||[[Los Angeles Angels|Angels]]||1
|-
|[[Pitcher|P]]||[[Gil Meche]] {{Ref|noplay|a}}||[[Kansas City Royals|Royals]]||1
|-
|[[Pitcher|P]]||[[Hideki Okajima]] {{Ref|final|b}} {{Ref|noplay|a}}||[[Boston Red Sox|Red Sox]]||1
|-
|[[Pitcher|P]]||[[Jonathan Papelbon]]||[[Boston Red Sox|Red Sox]]||2
|-
|[[Pitcher|P]]||[[J. J. Putz]]||[[Seattle Mariners|Mariners]]||1
|-
|[[Pitcher|P]]||[[Francisco Rodríguez (Venezuelan pitcher)|Francisco Rodríguez]]||[[Los Angeles Angels|Angels]]||2
|-
|[[Pitcher|P]]||[[CC Sabathia]]||[[Cleveland Indians|Indians]]||3
|-
|[[Pitcher|P]]||[[Johan Santana]]||[[Minnesota Twins|Twins]]||3
|-
|[[Pitcher|P]]||[[Justin Verlander]]||[[Detroit Tigers|Tigers]]||1
|}

{| class="wikitable" style="font-size: 100%; text-align:center;"
|-
!colspan="4"|Reserves
|-
!Position!!width="150"|Player!!width="100"|Team!!All-Star Games
|-
|[[Catcher|C]]||[[Víctor Martínez (baseball)|Víctor Martínez]]||[[Cleveland Indians|Indians]]||2
|-
|[[Catcher|C]]||[[Jorge Posada]]||[[New York Yankees|Yankees]]||5
|-
|[[First baseman|1B]]||[[Justin Morneau]]||[[Minnesota Twins|Twins]]||1
|-
|[[Second baseman|2B]]||[[Brian Roberts]]||[[Baltimore Orioles|Orioles]]||2
|-
|[[Third baseman|3B]]||[[Mike Lowell]]||[[Boston Red Sox|Red Sox]]||4
|-
|[[Shortstop|SS]]||[[Carlos Guillén]]||[[Detroit Tigers|Tigers]]||2
|-
|[[Shortstop|SS]]||[[Michael Young (baseball)|Michael Young]] {{Ref|noplay|a}}||[[Texas Rangers (baseball)|Rangers]]||4
|-
|[[Outfielder|OF]]||[[Carl Crawford]]||[[Tampa Bay Devil Rays|Devil Rays]]||2
|-
|[[Outfielder|OF]]||[[Torii Hunter]]||[[Minnesota Twins|Twins]]||2
|-
|[[Outfielder|OF]]||[[Manny Ramírez]]||[[Boston Red Sox|Red Sox]]||11
|-
|[[Outfielder|OF]]||[[Alex Ríos]]||[[Toronto Blue Jays|Blue Jays]]||2
|-
|[[Outfielder|OF]]||[[Grady Sizemore]]||[[Cleveland Indians|Indians]]||2
|-
|}
{{col-2}}

===National League===
{| class="wikitable" style="font-size: 100%; text-align:center;"
|-
!colspan="4"|Elected starters
|-
!Position!!width="150"|Player!!width="100"|Team!!All-Star Games
|-
|[[Catcher|C]]||[[Russell Martin]]||[[Los Angeles Dodgers|Dodgers]]||1
|-
|[[First baseman|1B]]||[[Prince Fielder]]||[[Milwaukee Brewers|Brewers]]||1
|-
|[[Second baseman|2B]]||[[Chase Utley]]||[[Philadelphia Phillies|Phillies]]||2
|-
|[[Third baseman|3B]]||[[David Wright]]||[[New York Mets|Mets]]||2
|-
|[[Shortstop|SS]]||[[José Reyes (shortstop)|José Reyes]]||[[New York Mets|Mets]]||2
|-
|[[Outfielder|OF]]||[[Carlos Beltrán]]||[[New York Mets|Mets]]||4
|-
|[[Outfielder|OF]]||[[Barry Bonds]]||[[San Francisco Giants|Giants]]||14
|-
|[[Outfielder|OF]]||''[[Ken Griffey, Jr.]]''||[[Cincinnati Reds|Reds]]||13
|-
|}

{| class="wikitable" style="font-size: 100%; text-align:center;"
|-
!colspan="4"|Pitchers
|-
!Position!!width="150"|Player!!width="100"|Team!!All-Star Games
|-
|[[Pitcher|P]]||[[Francisco Cordero]]||[[Milwaukee Brewers|Brewers]]||1
|-
|[[Pitcher|P]]||[[Brian Fuentes]] {{Ref|inj|c}}||[[Colorado Rockies|Rockies]]||3
|-
|[[Pitcher|P]]||[[Cole Hamels]]||[[Philadelphia Phillies|Phillies]]||1
|-
|[[Pitcher|P]]||[[Trevor Hoffman]]||[[San Diego Padres|Padres]]||6
|-
|[[Pitcher|P]]||[[Roy Oswalt]] {{Ref|noplay|a}}||[[Houston Astros|Astros]]||3
|-
|[[Pitcher|P]]||[[Jake Peavy]]||[[San Diego Padres|Padres]]||2
|-
|[[Pitcher|P]]||[[Brad Penny]]||[[Los Angeles Dodgers|Dodgers]]||2
|-
|[[Pitcher|P]]||[[Takashi Saito]]||[[Los Angeles Dodgers|Dodgers]]||1
|-
|[[Pitcher|P]]||[[Ben Sheets]]||[[Milwaukee Brewers|Brewers]]||3
|-
|[[Pitcher|P]]||''[[John Smoltz]]'' {{Ref|inj|d}}||[[Atlanta Braves|Braves]]||8
|-
|[[Pitcher|P]]||[[José Valverde]] {{Ref|noplay|a}}||[[Arizona Diamondbacks|Diamondbacks]]||1
|-
|[[Pitcher|P]]||[[Billy Wagner]]||[[New York Mets|Mets]]||4
|-
|[[Pitcher|P]]||[[Brandon Webb]] {{Ref|noplay|a}}||[[Arizona Diamondbacks|Diamondbacks]]||2
|-
|[[Pitcher|P]]||[[Chris Young (pitcher)|Chris Young]] {{Ref|final|b}}||[[San Diego Padres|Padres]]||1
|}

{| class="wikitable" style="font-size: 100%; text-align:center;"
|-
!colspan="4"|Reserves
|-
!Position!!width="150"|Player!!width="100"|Team!!All-Star Games
|-
|[[Catcher|C]]||[[Brian McCann (baseball)|Brian McCann]]||[[Atlanta Braves|Braves]]||2
|-
|[[First baseman|1B]]||[[Derrek Lee]]||[[Chicago Cubs|Cubs]]||2
|-
|[[First baseman|1B]]||[[Albert Pujols]] {{Ref|noplay|a}}||[[St. Louis Cardinals|Cardinals]]||6
|-
|[[First baseman|1B]]||[[Dmitri Young]]||[[Washington Nationals|Nationals]]||2
|-
|[[Second baseman|2B]]||[[Orlando Hudson]]||[[Arizona Diamondbacks|Diamondbacks]]||1
|-
|[[Second baseman|2B]]||[[Freddy Sanchez]]||[[Pittsburgh Pirates|Pirates]]||2
|-
|[[Third baseman|3B]]||[[Miguel Cabrera]]||[[Florida Marlins|Marlins]]||4
|-
|[[Shortstop|SS]]||[[J. J. Hardy]]||[[Milwaukee Brewers|Brewers]]||1
|-
|[[Outfielder|OF]]||[[Matt Holliday]]||[[Colorado Rockies|Rockies]]||2
|-
|[[Outfielder|OF]]||[[Carlos Lee]]||[[Houston Astros|Astros]]||3
|-
|[[Outfielder|OF]]||[[Aaron Rowand]]||[[Philadelphia Phillies|Phillies]]||1
|-
|[[Outfielder|OF]]||[[Alfonso Soriano]]||[[Chicago Cubs|Cubs]]||6
|-
|}
{{col-end}}

{{note|noplay|a}}These players did not see action in the game.<br>
{{note|final|b}}Voted onto the roster through the [[All-Star Final Vote]].<br>
{{note|inj|c}}Unable to play due to injury. [[Brandon Webb]] took his roster spot.<ref>{{cite web|url=http://sports.espn.go.com/mlb/news/story?id=2929165|title=Webb replaces Fuentes, will make second All-Star appearance|agency=[[Associated Press]]|publisher=ESPN|date=July 8, 2007|accessdate=May 22, 2008}}</ref><br>
{{note|inj|d}}Unable to play due to injury. [[Roy Oswalt]] took his roster spot.<ref>{{cite news|url=http://www.washingtonpost.com/wp-dyn/content/article/2007/07/05/AR2007070502166.html|title=Smoltz to Miss Next Start, All-Star Game|work=The Washington Post|page=E7|date=July 6, 2007|accessdate=May 22, 2008}}</ref>

==Managers==
[[National League]]: ''[[Tony La Russa]]''<br />
[[American League]]: [[Jim Leyland]]

==Game==
''[[O Canada]]'' was played by members of the [[San Francisco Symphony]]. ''[[The Star-Spangled Banner]]'' was sung by [[Chris Isaak]].<ref name="TBOrecap">{{cite web|url=http://www.tbo.com/sports/MGBL2HJ7Z3F.html|title=Selig Still Won't Commit to Witnessing History|author=Gaddis, Carter|publisher=TBO.com|date=July 11, 2007|accessdate=October 17, 2007 |archiveurl = https://web.archive.org/web/20080615202823/http://www.tbo.com/sports/MGBL2HJ7Z3F.html |archivedate = June 15, 2008}}</ref> Before the game, there was a tribute to former [[San Francisco Giants]] slugger [[Willie Mays]]. Mays threw the [[ceremonial first pitch]] to [[New York Mets]] [[shortstop]] [[José Reyes (shortstop)|José Reyes]]. [[Paula Cole]] sang ''[[God Bless America]]'' during the [[seventh-inning stretch]]. The first pitch was thrown by the National League's starter, [[Jake Peavy]] at 8:54 [[Eastern Daylight Time|EDT]]<ref name="TBOrecap" />  The game was completed in 3 hours, 6 minutes under an overcast sky and a gametime temperature of 68 degrees Fahrenheit.<ref>{{cite web|url=http://sports.espn.go.com/mlb/boxscore?gameId=270710132|title=American League vs. National League – Boxscore|publisher=ESPN|date=July 20, 2007|accessdate=July 14, 2008}}</ref>

===Umpires===
Umpires for the game were announced on June 14.<ref name=umps>{{cite web|url=http://mlb.mlb.com/news/article.jsp?ymd=20070614&content_id=2026346&vkey=allstar2007&fext=.jsp|title=Umpires, official scorers announced for 78th All-Star Game|publisher=MLB.com|date=June 14, 2007|accessdate=July 19, 2007}}</ref> [[Bruce Froemming]], the most tenured current umpire in Major League Baseball, was named crew chief for the game. It was also revealed that day that Froemming would retire following the 2007 season.<ref>{{cite web|url=http://www.sportsline.com/mlb/story/10225957|title=Veteran Froemming set to retire after 50 years in pro ball|publisher=CBS Sportsline|date=June 14, 2007|accessdate=July 19, 2007}}</ref>

{|class="wikitable"
|-
!Position !! Umpire !! MLB seasons
|-
| Home Plate || [[Bruce Froemming]] || 37
|-
| First Base || [[Charlie Reliford]] || 18
|-
| Second Base || [[Mike Winters]] || 18
|-
| Third Base || [[Kerwin Danley]] || 16
|-
| Left Field || [[Ted Barrett]] || 9
|-
| Right Field || [[Bill Miller (umpire)|Bill Miller]] || 8
|}

===Starting lineups===
{{All-Star Starting Lineup
| DH = no
| Road = '''[[American League]]'''|Home='''[[National League]]'''
| RP1 = [[Ichiro Suzuki]]|RT1=[[Seattle Mariners|Mariners]]|Rpos1=[[Center fielder|CF]]
| RP2 = [[Derek Jeter]]|RT2=[[New York Yankees|Yankees]]|Rpos2=[[Shortstop|SS]]
| RP3 = [[David Ortiz]]|RT3=[[Boston Red Sox|Red Sox]]|Rpos3=[[First baseman|1B]]
| RP4 = [[Alex Rodriguez]]|RT4=[[New York Yankees|Yankees]]|Rpos4=[[Third baseman|3B]]
| RP5 = [[Vladimir Guerrero]]|RT5=[[Los Angeles Angels of Anaheim|Angels]]|Rpos5=[[Right fielder|RF]]
| RP6 = [[Magglio Ordóñez]]|RT6=[[Detroit Tigers|Tigers]]|Rpos6=[[Left fielder|LF]]
| RP7 = [[Iván Rodríguez]]|RT7=[[Detroit Tigers|Tigers]]|Rpos7=[[Catcher|C]]
| RP8 = [[Plácido Polanco]]|RT8=[[Detroit Tigers|Tigers]]|Rpos8=[[Second baseman|2B]]
| RP9 = [[Dan Haren]]|RT9=[[Oakland Athletics|Athletics]]|Rpos9=[[Pitcher|P]]
| HP1 = [[José Reyes (shortstop)|José Reyes]]|HT1=[[New York Mets|Mets]]|Hpos1=[[Shortstop|SS]]
| HP2 = [[Barry Bonds]]|HT2=[[San Francisco Giants|Giants]]|Hpos2=[[Left fielder|LF]]
| HP3 = [[Carlos Beltrán]]|HT3=[[New York Mets|Mets]]|Hpos3=[[Center fielder|CF]]
| HP4 = [[Ken Griffey, Jr.]]|HT4=[[Cincinnati Reds|Reds]]|Hpos4=[[Right fielder|RF]]
| HP5 = [[David Wright]]|HT5=[[New York Mets|Mets]]|Hpos5=[[Third baseman|3B]]
| HP6 = [[Prince Fielder]]|HT6=[[Milwaukee Brewers|Brewers]]|Hpos6=[[First baseman|1B]]
| HP7 = [[Russell Martin]]|HT7=[[Los Angeles Dodgers|Dodgers]]|Hpos7=[[Catcher|C]]
| HP8 = [[Chase Utley]]|HT8=[[Philadelphia Phillies|Phillies]]|Hpos8=[[Second baseman|2B]]
| HP9 = [[Jake Peavy]]|HT9=[[San Diego Padres|Padres]]|Hpos9=[[Pitcher|P]]
}}

===Game summary===
[[File:Barry Bonds, Dan Haren 2007 MLB All-Star Game.jpg|thumb|300px|[[Barry Bonds]] (batter) vs [[Dan Haren]] (pitcher)]]
The National League got things started in the bottom of the first when [[José Reyes (shortstop)|José Reyes]] led off with a base hit off American League starter [[Dan Haren]] and proceeded to [[stolen base|steal]] second.<ref name="playbyplay">{{cite web|url=http://sports.espn.go.com/mlb/playbyplay?gameId=270710132|title=All-Star Game Play-by-Play|publisher=ESPN|date=July 10, 2007|accessdate=October 17, 2007| archiveurl= https://web.archive.org/web/20070917070348/http://sports.espn.go.com/mlb/playbyplay?gameId=270710132| archivedate= September 17, 2007 <!--DASHBot-->| deadurl= no}}</ref> He scored on an [[run batted in|RBI]] single by [[Ken Griffey, Jr.]] to give the NL a 1–0 lead.<ref name="playbyplay"/> [[Barry Bonds]] nearly gave his hometown fans something to cheer for in the bottom of the third when, with Reyes on second, he lofted a high fly ball to left field, but it was snared at the warning track by [[Magglio Ordóñez]].<ref name="playbyplay"/> The AL nearly tied the game in the fourth when [[Alex Rodriguez]] attempted to score on a two-out single by [[Iván Rodríguez]]. However, the throw to home plate by Griffey allowed [[Russell Martin]] to tag Rodriguez out at the plate to end the inning.<ref name="playbyplay"/> The AL would score one inning later when, after [[Chris Young (pitcher)|Chris Young]] issued a leadoff walk to [[Brian Roberts]], [[Ichiro Suzuki]] hit a long fly ball off the right field wall. Instead of caroming straight to Griffey, the ball took an unusual bounce off a sign and ricocheted to Griffey's right. This allowed Ichiro to score on what became the first and only [[inside-the-park home run]] in All-Star Game history.<ref>{{cite web|url=http://mlb.mlb.com/news/article.jsp?ymd=20070710&content_id=2080667&vkey=allstar2007&fext=.jsp|title=Ichiro runs into record book|accessdate=July 10, 2007|date=July 10, 2007|author=Brock, Corey|publisher=MLB.com| archiveurl= https://web.archive.org/web/20070713174247/http://mlb.mlb.com/news/article.jsp?ymd=20070710&content_id=2080667&vkey=allstar2007&fext=.jsp| archivedate= July 13, 2007 <!--DASHBot-->| deadurl= no}}</ref> The homer gave the AL a 2–1 lead and resulted in Ichiro's MVP win.

The lead would be augmented in the sixth when [[Carl Crawford]] hit a line drive that just cleared the right field wall for a home run.<ref name="playbyplay"/> Though it appeared a fan may have [[interference (baseball)|reached over the wall]] to catch it, NL manager [[Tony La Russa]] did not challenge the umpires' call.<ref>{{cite web|url=http://mlb.mlb.com/news/gameday_recap.jsp?ymd=20070710&content_id=2079841&vkey=recap&fext=.jsp&c_id=mlb |title=Ichiro leads the way in AL victory|accessdate=July 10, 2007|date=July 10, 2007|author=Haft, Chris|publisher=MLB.com}}</ref> The NL got a run back in the bottom of the inning when [[Carlos Beltrán]] led off with a triple and scored on a [[sacrifice fly]] by Griffey.<ref name="playbyplay"/> The AL added some insurance runs in the eighth when [[Víctor Martínez (baseball)|Víctor Martínez]] hit a two-run home run just inside the left field foul pole to give the AL a 5–2 lead.<ref name="playbyplay"/>

The American League's closers then entered the game, with [[Jonathan Papelbon]] pitching a scoreless bottom of the eighth.<ref name="playbyplay"/> In the ninth, [[J. J. Putz|J.&nbsp;J. Putz]] tried to earn the save and began by inducing a weak pop-up and striking out [[Brian McCann (baseball)|Brian McCann]].<ref name="playbyplay"/> Pinch-hitter [[Dmitri Young]] rolled a ground ball deep in the hole to [[Brian Roberts]], but he could not come up with it. [[Alfonso Soriano]] followed with a two-run home run to right field to cut the NL's deficit to one.<ref name="playbyplay"/> After Putz walked [[J. J. Hardy]], AL manager [[Jim Leyland]] replaced him with [[Francisco Rodríguez (Venezuelan pitcher)|Francisco Rodríguez]].<ref name="playbyplay"/> However, Rodriguez had trouble consistently locating his pitches and walked [[Derrek Lee]] on a check-swing 3–2 pitch and then [[Orlando Hudson]] to load the bases.<ref name="playbyplay"/> In a move that drew criticism,<ref>{{cite web|url=http://www.stltoday.com/blogs/sports-bird-land/2007/07/pcd-wednesday-the-pujols-kerfuffle/#more-13279 |title=PCD Wednesday: The Pujols Kerfuffle |accessdate=July 11, 2007 |date=July 11, 2007 |author=Goold, Derrick |publisher=STLtoday.com |archiveurl=https://web.archive.org/web/20070714220614/http://www.stltoday.com/blogs/sports-bird-land/2007/07/pcd-wednesday-the-pujols-kerfuffle/#more-13279 |archivedate=July 14, 2007 |deadurl=no |df=mdy }}</ref> La Russa elected not to pinch-hit his last player on the bench, [[Albert Pujols]], and instead let [[Aaron Rowand]] hit. Rowand lofted a fly ball to right field that was caught by [[Alex Ríos]] to close the game,<ref name="playbyplay"/> earning the American League their tenth consecutive victory.

{{Linescore
| Time = 5:54&nbsp;pm ([[Pacific Time Zone|PT]])|Date=July 10, 2007|Location=AT&T Park, San Francisco, California|Compact=no
| Road = '''[[American League]]'''|RoadAbr=AL
| R1 = 0|R2=0|R3=0|R4=0|R5=2|R6=1|R7=0|R8=2|R9=0
| RR = 5|RH=10|RE=0
| Home = [[National League]]|HomeAbr=NL
| H1 = 1|H2=0|H3=0|H4=0|H5=0|H6=1|H7=0|H8=0|H9=2
| HR = 4|HH=9|HE=1
| RSP = [[Dan Haren]] |HSP=[[Jake Peavy]]
| WP = [[Josh Beckett]] (1–0) |LP=[[Chris Young (pitcher)|Chris Young]] (0–1) |SV=[[Francisco Rodríguez (Venezuelan pitcher)|Francisco Rodríguez]] (1)
| RoadHR = [[Ichiro Suzuki]] (1), [[Carl Crawford]] (1), [[Víctor Martínez (baseball)|Víctor Martínez]] (1) |HomeHR=[[Alfonso Soriano]] (1) | HRH=yes
}}

==Home Run Derby==
[[File:McCovey Cove 2007.jpg|thumb|[[McCovey Cove]]]]
{{main|2007 Major League Baseball Home Run Derby}}
The [[State Farm Insurance|State Farm]] [[Home Run Derby]] was held the night before the All-Star Game, July 9, and broadcast on [[ESPN]]. Four players from each league competed to hit as many home runs as they could in each round to advance and eventually win the contest. This year, a five-swing swing-off would be used to break ties occurring in any round. This became necessary when [[Albert Pujols]] and [[Justin Morneau]] tied for fourth in the first round. The champion of last year's Derby, [[Ryan Howard]] of the [[Philadelphia Phillies]], competed even though he was not named to the NL All-Star roster.<ref name="Howard">{{cite web |url=http://mlb.mlb.com/news/article.jsp?ymd=20070702&content_id=2062619&vkey=allstar2007&fext=.jsp |title=Howard will defend HR Derby crown |first=Ken |last=Mandel |publisher=MLB.com |date=July 2, 2007 |accessdate=July 6, 2007}}</ref>

In the finals, [[Vladimir Guerrero]] of the [[Los Angeles Angels of Anaheim]] defeated [[Alex Ríos]] of the [[Toronto Blue Jays]] by a score of 3–2.<ref name=Guerrero>{{cite web|url= http://mlb.mlb.com/news/article.jsp?ymd=20070709&content_id=2078659&vkey=allstar2007&fext=.jsp|title=Vlad captures first Derby crown|publisher=MLB.com|author=Bloom, Barry M.|date=July 10, 2007|accessdate=July 15, 2007}}</ref> Guerrero hit 17 home runs in all, second only to Ríos' 19. Guerrero also hit the longest blast of the competition, a {{convert|503|ft|m|sing=on}} drive to left field that just missed hitting a giant replica glove and baseball set up on the concourse beyond the left field bleachers.

<!-- Image with inadequate rationale removed: [[File:2007moneyball.jpg|thumb|right|Gold ball that was used for the 2007 State Farm Home Run Derby]] -->AT&T Park is distinguished by having the [[San Francisco Bay]] beyond its right field bleachers. The body of water located adjacent to the ballpark is known as [[McCovey Cove]], named for legendary Giants slugger [[Willie McCovey]]. McCovey Cove is known for having many fans sitting in the water in [[kayak]]s and boats hoping to retrieve a long home run ball hit there. Though dozens of fans waited in the cove during the Derby, no home runs were actually hit into the water, either on the fly or off the promenade next to the right field seats, though, one foul ball hit by [[Prince Fielder]] did reach the water.<ref name="water">{{cite web|url=http://mlb.mlb.com/news/article.jsp?ymd=20070710&content_id=2079099&vkey=allstar2007&fext=.jsp|title=Fans pack McCovey Cove for Derby|publisher=MLB.com|author=Urban, Mychael|date=July 10, 2007|accessdate=July 15, 2007}}</ref> This was largely due to the three left-handed competitors all exiting in the first round, as well as wind currents blowing toward left field. Prior to the All-Star break, a total of 58 home runs were hit into the cove on the fly during the park's history.<ref>{{cite web|url=http://sanfrancisco.giants.mlb.com/sf/ballpark/splashhits.jsp|title=Splash Hits|accessdate=July 15, 2007| archiveurl= https://web.archive.org/web/20070804021330/http://sanfrancisco.giants.mlb.com/sf/ballpark/splashhits.jsp| archivedate= August 4, 2007 <!--DASHBot-->| deadurl= no}}</ref>

Gold balls were utilized whenever any player had one out remaining during his round. Any home runs hit with the balls meant Major League Baseball and State Farm would pledge to donate money to the [[Boys & Girls Clubs of America]]. Due to the change in sponsors from [[Century 21 Real Estate|Century 21]] to State Farm, each ball's value was reduced to US$17,000 to reflect the 17,000 State Farm agents in the United States and Canada.<ref name=Guerrero/> In all, twelve gold ball home runs were hit, which, along with a $50,000 "bonus" constituted $254,000 raised for charity.<ref name=gold>{{cite web|url=http://mlb.mlb.com/news/article.jsp?ymd=20070709&content_id=2078069&vkey=allstar2007&fext=.jsp|title=Gold balls go deep for good cause|publisher=MLB.com|author=Newman, Mark|date=July 10, 2007|accessdate=July 15, 2007}}</ref>

{| class="wikitable" style="font-size: 100%; text-align:center;"
|-
!colspan="7"|[[AT&T Park]], San Francisco—A.L. 42, N.L. 32
|-
!Player!!Team!!Round 1!!Round 2!!Subtotal!!Finals!!Total
|-
|align="left"|{{flagicon|DOM}} '''''[[Vladimir Guerrero]]'''''||'''''[[Los Angeles Angels of Anaheim|Los Angeles (AL)]]'''''||'''''5'''''||'''''9'''''||'''''14'''''||'''''3''''' {{Ref|outs|a}}||'''''17'''''
|-
|align="left"|[[Alex Ríos]]||[[Toronto Blue Jays|Toronto]]||5||12 ||17||2 ||19
|-
|align="left"|[[Matt Holliday]]||[[Colorado Rockies|Colorado]]||5||8||13||–||13
|-
|align="left"|[[Albert Pujols]]||[[St. Louis Cardinals|St. Louis]]||4 {{Ref|swingoff|b}}||9 ||13||– ||13
|-
|align="left"|[[Justin Morneau]]||[[Minnesota Twins|Minnesota]]||4||–||4||–||4
|-
|align="left"|[[Prince Fielder]]||[[Milwaukee Brewers|Milwaukee]]||3||–||3||–||3
|-
|align="left"|[[Ryan Howard]]||[[Philadelphia Phillies|Philadelphia]]||3||–||3||–||3
|-
|align="left"|[[Magglio Ordóñez]]||[[Detroit Tigers|Detroit]]||2||–||2||–||2
|}

{{note|outs|a}}Recorded only seven of ten outs before hitting winning home run.<br>
{{note|swingoff|b}}Advanced after defeating Morneau 2–1 in a swing-off.

==Futures Game==
The 2007 [[XM Satellite Radio|XM]] [[All-Star Futures Game]] took place on July 8, showcasing the top [[Minor League Baseball|minor league]] prospects from all thirty teams' farm systems. The contest is seven innings regardless of the score with pitchers limited to no more than one inning of work. The World team defeated the United States by a score of 7–2.<ref name="futures">{{cite web|url=http://mlb.mlb.com/news/article.jsp?ymd=20070708&content_id=2076058&vkey=allstar2007&fext=.jsp |title=Futures spoils belong to the World |date=July 8, 2007 |accessdate=July 12, 2007 |author=Castrovince, Anthony |publisher=MLB.com}}</ref> [[Chin-Lung Hu]] of the [[Los Angeles Dodgers]] organization won the [[Larry Doby]] MVP award after driving in two runs on a single and double, plus a stolen base and a run.<ref name="futures"/>

{{Linescore
| Road = '''World'''|RoadAbr=Wor
| R1 = 2|R2=0|R3=1|R4=1|R5=0|R6=1|R7=2|RR=7|RH=8|RE=0
| Home = United States|HomeAbr=USA
| H1 = 0|H2=0|H3=1|H4=0|H5=1|H6=0|H7=0|HR=2|HH=5|HE=1
| RSP = |HSP=
| WP = [[Rick VandenHurk]] (1–0) |LP=[[Jeff Niemann]] (0–1) |SV=
| RoadHR = [[Joey Votto]] (1), [[James Van Ostrand]] (1) |HomeHR=[[Justin Upton]] (1), [[John Whittleman]] (1)
}}

==Footnotes and references==
{{Reflist}}

==External links==
{{Commons category|2007 Major League Baseball All-Star Game}}
*[http://mlb.mlb.com/NASApp/mlb/mlb/events/all_star/y2007/index.jsp Official website of the All-Star Game]
*[http://www.baseball-reference.com/boxes/NLS/NLS200707100.shtml 2007 Major League Baseball All-Star Game at Baseball Reference]

{{MLBAllStarGame}}
{{2007 MLB season by team}}
{{Major League Baseball on Fox}}
{{Major League Baseball on ESPN Radio}}

[[Category:Major League Baseball All-Star Game]]
[[Category:2007 Major League Baseball season|All-Star Game]]
[[Category:Baseball in San Francisco]]
[[Category:Sports competitions in San Francisco]]
[[Category:2007 in sports in California|Major League Baseball All Star Game]]
[[Category:2007 in San Francisco]]
[[Category:Sports competitions in California]]
[[Category:July 2007 sports events]]