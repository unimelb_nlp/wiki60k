{{Infobox publisher
| image = 
| parent = 
| status = Defunct (2013)
| founded = {{Start date|1983}}
| founder = Marion Berghahn
| successor = [[Bloomsbury Publishing]]
| country = United Kingdom
| headquarters = [[Oxford]]
| distribution = [[Macmillan Publishers]] (Except United States), [[Palgrave Macmillan]] (United States)
| keypeople = 
| publications = [[Book]]s, [[academic journal]]s
| topics = 
| genre = Academic books and [[Academic journal|journals]]
| imprints = 
| revenue = 
| numemployees = 
| nasdaq = 
| url = {{URL|http://www.bergpublishers.com}}
}}
'''Berg Publishers''' was an [[academic publishing]] company based in [[Oxford]], [[England]] that was founded in 1983 by [[Berghahn Books|Marion Berghahn]].<ref name="urlFDUK">{{cite web |url=http://www.fduk.co.uk/case-study-berg.asp |title=BERG PUBLISHERS: LOCAL PUBLISHER SET FOR GREAT THINGS |format= |work= |accessdate=}}</ref> Berg published [[monograph]]s, [[textbook]]s, and [[reference work]]s as well as [[academic journal]]s. Concentrations were [[fashion]], [[design]], [[anthropology]], [[history]], and [[cultural studies]].<ref name="urlAbout Berg">{{cite web |url=http://www.bergpublishers.com/AboutUs/tabid/530/Default.aspx |title=About Berg |format= |work= |accessdate=2008-05-30}}</ref>

== History ==
[[Book Industry Communication]] (BIC), a trade standards group for electronic commerce and supply chain efficiency, awarded Berg its BIC Product Data Excellence Gold Award in 2007-2008<ref name="url">{{cite web |first=Kathryn |last=Earle |date=13 December 2006 |title=Social History Society places 'Cultural and Social History' with Berg |url=http://liblicense.crl.edu/ListArchives/0612/msg00052.html |type=open email |publisher=LIBLICENSE |accessdate=25 September 2013}}</ref> and its e4books project accredited Berg in 2008.<ref name="urlBerg Publishers">{{cite web |url=http://www.bergpublishers.com/Home/tabid/36/Default.aspx |title=Berg Publishers |format= |work= |accessdate=2008-05-31}}</ref> Berg won the [[Independent Publishers Guild]]'s 2008 Publishing Technology E-Publishing Award for its collection of profitable digital strategies in March 2008.<ref name="urlindependent publishers guild&nbsp;— The Independent Publishing Awards 2008">{{cite web |first=Bridget |last=Shine |title=The Independent Publishing Awards 2008 |date=March 2008 |url=http://www.ipg.uk.com/cgi-bin/scribe?showinfo=pp126 |work=Independent Publishers Guild |deadurl=yes |archiveurl=https://web.archive.org/web/20080430002938/http://www.ipg.uk.com/cgi-bin/scribe?showinfo=pp126 |archivedate=30 April 2008 |accessdate=25 September 2013}}</ref>

As of March 2008, Berg published thirteen journals.<ref name="urlSociety for Scholarly Publishing&nbsp;— Member News Releases">{{cite web |url=http://sspnet.org/News/Member_News_Releases/spage.aspx |title=Society for Scholarly Publishing&nbsp;— Member News Releases |accessdate=2008-05-30}}</ref> In September 2008, [[Bloomsbury Publishing]] agreed to buy Oxford International Publishers, trading as Berg Publishers.<ref>press release {{cite web |url= http://www.bloomsbury.com/rebranding-continuum-berg-bcp/ |format= |accessdate= 2012-11-23}}</ref> Since 2013, all Berg titles are published under the Bloomsbury name (under the [[Imprint (trade name)|imprint]] Bloomsbury Academic).

== References ==
{{Reflist|30em}}

== External links ==
* {{Official website|http://www.bergpublishers.com/Home/tabid/36/Default.aspx}}

[[Category:Academic publishing companies]]
[[Category:Book publishing companies of England]]
[[Category:Publishing companies established in 1983]]
[[Category:1983 establishments in England]]
[[Category:2013 disestablishments in England]]
[[Category:Defunct publishing companies]]
[[Category:Publishing companies disestablished in 2013]]