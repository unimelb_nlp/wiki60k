{{Infobox journal
| title = Business History
| cover =
| editor = Ray Stokes
| discipline = [[Business history]]
| former_names =
| abbreviation = Bus. Hist.
| publisher = [[Taylor & Francis]]
| country =
| frequency = 8/year
| history = 1958-present
| openaccess =
| license =
| impact = 0.564
| impact-year = 2013
| website = http://www.tandfonline.com/action/journalInformation?journalCode=fbsh20
| link1 = http://www.tandfonline.com/toc/fbsh20/current
| link1-name = Online access
| link2 = http://www.tandfonline.com/loi/fbsh20
| link2-name = Online archive
| JSTOR =
| OCLC = 42884007
| LCCN = 65034644
| CODEN =
| ISSN = 0007-6791
| eISSN = 1743-7938
}}
'''''Business History''''' is a [[peer-reviewed]] [[academic journal]] covering the field of [[business history]]. It was established in 1958 by [[University of Liverpool|Liverpool University Press]]<ref name=fifty>{{cite web |last1=Wilson |first1=John F. |title=50 years of ''Business History'' |url=http://www.tandf.co.uk/journals/pdf/announcements/fbsh_anni.pdf |website=Business History |publisher=Taylor and Francis |accessdate=4 September 2014 |format=pdf |date=2008}}</ref> and is now published by [[Taylor and Francis]]. The [[editor-in-chief]] is [[Raymond G Stokes| Ray Stokes]] ([[University of Glasgow]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[ABI/Inform]]
* [[British Humanities Index]]
* [[EBSCOhost]]
* [[EconLit]]
* [[GEOBASE]]
* [[Geographical Abstracts]]
* [[PAIS International]]
* [[Scopus]]
* [[Sociological Abstracts]]
* [[Social Sciences Citation Index]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 0.564.<ref name=WoS>{{cite book |year=2014 |chapter=Business History |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]] |postscript=.}}</ref>

==References==
{{reflist}}

==External links==
* {{Official website|1=http://www.tandfonline.com/action/journalInformation?journalCode=fbsh20}}

[[Category:History journals]]
[[Category:Taylor & Francis academic journals]]
[[Category:Publications established in 1958]]
[[Category:English-language journals]]
[[Category:Business and management journals]]
[[Category:History of business]]

{{business-journal-stub}}
{{history-journal-stub}}