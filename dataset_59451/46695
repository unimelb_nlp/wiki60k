{{Infobox scientist
| name                    = William J. Breed
| image                   = 
| image_size              = 
| caption                 = 
| birth_date              = {{birth date|1928|8|3|mf=y}}
| birth_place             = [[Massillon, Ohio]]
| death_date              = {{death date and age|2013|1|22|1928|8|03|df=y}}
| death_place             = [[Flagstaff, Arizona]]
| residence               = 
| citizenship             =
| nationality             = 
| ethnicity               =
| field                   = [[Geomorphology]]<br>[[Geology]]<br>[[Vertebrate]][[Paleontology]]
| work_institution        = [[U.S. Geological Survey]]<br>[[Museum of Northern Arizona]]<br>Nature Expeditions International
| alma_mater              = [[Massillon Washington High School]]<br>[[Denison University]]<br>[[University of Arizona]]
| doctoral_advisor        = 
| doctoral_students       = 
| known_for               = [[Grand Canyon]]<br>[[Northern Arizona]]<br>[[Dilophosaurus]]<br>[[Lystrosaurus]]
| author_abbreviation_bot = 
| author_abbreviation_zoo = 
| prizes                  = [[Fulbright Scholarship]] (1957-58)<br>[[National Science Foundation]] - Antarctic Service Medal (1977)<br>Gladys Cole Award - [[Geological Society of America]](1982)
| religion                = 
| footnotes               = 
}}
'''William J. "Bill" Breed''' (August 3, 1928 - January 22, 2013) was an American [[geologist]], [[paleontologist]], naturalist and author in Northern [[Arizona]]. He was a renowned expert on the geology of the [[Grand Canyon]].<ref name="azdailysun">[http://azdailysun.com/news/local/obituaries/william-breed-renowned-mna-geologist-dies-in-flagstaff/article_41857e06-1387-53d0-bf6f-f3c469222f65.html ''Arizona Daily Sun'', January 24, 2013, A2]</ref>

== Personal life ==

William J. Breed was born August 3, 1928 in [[Massillon, Ohio]], son of Grace Amelia (née Snyder) and Earl Fremont Breed. After graduating from [[Massillon Washington High School]], he served in the U.S. Army Corps of Military Police in South Korea and Japan (1946–48).  He received his [[Bachelor of Arts|B.A.]] from [[Denison University]], then his [[Bachelor of Science|B.S.]] and [[Master of Science|M.S.]] from [[University of Arizona]], finishing in 1960.  He married Carol S. Breed daughter of [[James Neilson (director)]] and Mrs. Ruth Swope (née Rogers) in 1965. The couple had one daughter, Amelia, who joined the family of four girls (Linda, Laura, Grace, Pamela) from Carol's first marriage.<ref>''Arizona Daily Sun'', Dec. 18, 1965</ref>

== Career ==

Bill Breed was a [[Fulbright Scholar]] at [[Canterbury University]] in [[Christchurch]], New Zealand to study [[geomorphology]] in 1957-58.<ref name="CantonRepository">''Canton Repository'', March 12, 1957</ref>

Bill Breed was a protégé of [[Edwin D. McKee]].  Breed was Curator of Geology (1960–78), Head, Geology Department (1978–81) at the [[Museum of Northern Arizona]]. In 1969, Breed and [[Edwin H. Colbert]] were two of the four vertebrate paleontologists in Antarctica who helped solidify the acceptance of [[continental drift]] theory, by finding a 220-million-year-old fossil of a ''[[Lystrosaurus]]''.  In the 1970s, Breed completed numerous publications on the [[Grand Canyon]], including a geologic cross section map of the Grand Canyon, San Francisco Peaks and Verde Valley; and the book, ''Geology of the Grand Canyon''.<ref>[http://azdailysun.com/news/local/obituaries/william-breed-renowned-mna-geologist-dies-in-flagstaff/article_41857e06-1387-53d0-bf6f-f3c469222f65.html Arizona Daily Sun, January 24, 2013, A2;]</ref>

A species of ''[[Dilophosaurus]], (D. breedorum)'', discovered by Sam Welles in Arizona in 1964 was named after William J. Breed and his wife at the time Carol S. Breed.<ref>Welles, S. P. (1984). "Dilophosaurus wetherilli (Dinosauria, Theropoda), osteology and comparisons". Palaeontogr. Abt. A 185: 85–180.</ref> This name came out in a private publication distributed by Pickering,<ref>Olshevsky, George (1999-12-05). "Dinosaur Genera List corrections #126". Dinosaur Mailing List Archives. Cleveland Museum of Natural History. Retrieved 2008-06-25.</ref> but has not been accepted in other reviews of the genus.<ref>Gay, Robert (2001). "New specimens of Dilophosaurus wetherilli (Dinosauria: Theropoda) from the early Jurassic Kayenta Formation of northern Arizona". Western Association of Vertebrate Paleontologists annual meeting volume Mesa, Arizona 1: 1.</ref><ref>Tykoski, R. S. & Rowe, T. (2004). "Ceratosauria". In: Weishampel, D.B., Dodson, P., & Osmolska, H. (Eds.) ''The Dinosauria'' (2nd edition). Berkeley: University of California Press. Pp. 47–70 ISBN 0-520-24209-2</ref><ref>[[Dilophosaurus]]</ref>

After leaving the [[Museum of Northern Arizona]] in 1981, Breed became a naturalist guide for Nature Expeditions International and Betchart Expeditions leading trips to Alaska, New Zealand, [[Galapagos]], [[Namibia]], and Australia.  He became Curator Emeritus of the Museum of Northern Arizona in 2004.  In January 2013, shortly before his death, Breed was named a Distinguished Fellow of the Museum of Northern Arizona.<ref name="azdailysun" />

Breed was an active environmentalist with [[Ted Danson]] <ref>[http://newsweek.com/2011/03/20/my-favorite-mistake.html "My Favorite Mistake"] ''Newsweek''</ref> and many others.

== Awards ==

[[Fellow]] of the [[Geological Society of America]], the A.A.A.S, the Arizona Academy of Science, and the [[Museum of Northern Arizona]].<ref name="Museumof">Museum of Northern Arizona Archive</ref><br>
[[National Science Foundation]] - Antarctic Service Medal (1977)<ref name="Museumof" /><br>
[[Geological Society of America]] - Gladys Cole Award (1982)<ref name="Museumof" /><br>
Distinguished Citizen Award from [[Massillon Washington High School]] in 2005.<ref name="Museumof" />

== Publications ==

Breed wrote or contributed to more than 80 scientific publications and popular magazines.<ref name="Museumof" />
*1964: "Metacoceras bowmani, a new species of Nautiloid from the Toroweap Formation (Permian) of Arizona," ''[[Journal of Paleontology]]''; v.38, No.5, pp 877–880 (with Halsey W. Miller Jr)
*1965: "An exotic occurrence of fresh water drum fish," ''[[Plateau (journal)|Plateau]]'' v. 37, No.3, p.&nbsp;5.
*1967: "Evolution of the Colorado River in Arizona" 'Museum of Northern Arizona Bulletin' #44, pp 67 (with [[Edwin D. McKee]], R.F. Wilson, and Carol S. Breed.)
*1969: "A Pliocene river channel near Doney Crater, Arizona" ''Journal of Arizona Academy of Sciences'', vol. 5, No.3, pp.&nbsp;177–181.
*1969: "The Chuar Group of the Grand Canyon, Arizona." ''Geological Society of America Rocky Mountain Section, Abstracts with Programs,'' May 7–11, 1969, pp.&nbsp;23–24. (with [[Trevor D. Ford]])
*1970: "Hopi Pahos at the South Pole" ''Plateau'' V. 42, No. 4, pp.&nbsp;125
*1970: "Triassic Tetrapods from Antarctica: Evidence of Continental Drift" ''[[Science (journal)|Science]]'' V. 169, No. 3951, pp.&nbsp;1197–1201 (with David H Elliot, [[Edwin H. Colbert]], James Jensen and Jon S. Powell)
*1971: "Last Chance for Rainbow Bridge?" ''Outdoor Arizona'', pp.&nbsp;38–41
*1974: ''Geology of the Grand Canyon'', by William J. Breed and Evelyn C. Roat 
*1974: "Red Mountain, Erosion or Explosion," ''Plateau'', Vol. 46, No. 3, pp.&nbsp;120–122.
*1975: ''Geologic Cross Section of the Grand Canyon - San Francisco Peaks - Verde Valley Region'' Zion Natural History Association, Springdale, Utah
*1976: "Our "Unchanging" Grand Canyon", ''[[Arizona Highways]]'', Vol. 52, No. 5, p.&nbsp;12-15.
*1977: "Chitinozoans from the Late Precambrian Chuar Group of the Grand Canyon, Arizona" ''[[Science (journal)|Science]]'', Vol. 195, No. 4279, pp.&nbsp;676–679 (with Bonnie Bloeser, Wiliam J. Schopf and Robert J. Horodyski)

== References ==

{{reflist}}

== External links and references ==

* ''Arizona Daily Sun'', January 24, 2013, A2 http://azdailysun.com/news/local/obituaries/william-breed-renowned-mna-geologist-dies-in-flagstaff/article_41857e06-1387-53d0-bf6f-f3c469222f65.html
*''Newsweek'': [http://newsweek.com/2011/03/20/my-favorite-mistake.html My Favorite Mistake]

{{Authority control|VIAF=73915523}}

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box.  Just press "Save page". -->

{{DEFAULTSORT:Breed, William J.}}
[[Category:1928 births]]
[[Category:2013 deaths]]
[[Category:Denison University alumni]]
[[Category:American paleontologists]]
[[Category:American geologists]]
[[Category:University of Arizona alumni]]
[[Category:People from Stark County, Ohio]]
[[Category:People from Massillon, Ohio]]
[[Category:Writers from Tucson, Arizona]]
[[Category:People from Flagstaff, Arizona]]
[[Category:Fulbright Scholars]]
[[Category:Fellows of the Geological Society of America]]