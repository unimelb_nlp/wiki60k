{{Infobox Chinese-language singer and actor
| name                    = Lai Ying Tong
| image                   =
| image_size              = 
| alt                     = 
| tradchinesename         = 賴映彤
| simpchinesename         = 赖映彤
| pinyinchinesename       = Lài Yìingtóng
| jyutpingchinesename     = laai6 jing2 tung4
| birth_date               = January 4
| birth_place             = [[Hong Kong]]
| othername               = Siu Tung (小彤)
| occupation              = [[Songwriter]]/[[Arranger]]/[[Performer]]/[[Music Education]]
| genre                   = [[Cantopop]]
| instrument              = [[Musical keyboard|Keyboard/Piano]]
| label                   =  Kingdom C Production Limited
| yearsactive             = 2008 - Present
| associatedact           = [[:zh-tw:C AllStar|C AllStar]]
| website                 = <!-- {{URL|example.com}} -->
}}
{{Chinese name|[[Lai (Chinese surname)|Lai]]}}

'''Lai Ying Tong''' ({{zh|t=賴映彤}}; born 4 January), known professionally as '''Siu Tung''' (Chinese: 小彤), is a Hong Kong [[Cantopop]] songwriter,arranger and performer as well as music educator. She graduated from the [[Chinese University of Hong Kong]] with a Master of Arts in Music. Lai is currently a signed musician of Kingdom C Production Limited and a part-time music lecturer at [[:zh-tw:香港知專設計學院|Hong Kong Design Institute]] and [[:zh-tw:伯樂音樂學院|Baron School of Music]].<ref name="Instructor Lai Ying Tong's Biography. Baron School of Music. Web. Retrieved 01 Apr. 2015.">[http://www.bsm.com.hk/b5/iproduction_19.htm] "Instructor Lai Ying Tong's Biography." Baron School of Music. Web. Retrieved 01 Apr. 2015.</ref> Lai is also a columnist for [http://www.manfredwong.com/index.php Headline Music Magazine].<ref name="fb">[https://www.facebook.com/104324439639886/posts/565918943480431] 賴映彤. "HEADLINES 與眾筆彤 關西行 （上）." Headlines Magazine Facebook Page, 6 Nov. 2013. Retrieved 01 Apr. 2015.</ref>

Lai works closely with the a cappella group [[:zh-tw:C AllStar|C AllStar]]. She always plays keyboards in their concerts, so she has earned the name 'the 5th member of C AllStar'.<ref name="Instructor Lai Ying Tong's Biography. Baron School of Music. Web. Retrieved 01 Apr. 2015."/>

In 2012, Lai was awarded the fourth '[[Joseph Koo]] New Generation Award' at the Annual [[Composers and Authors Society of Hong Kong|CASH]] Awards Presentation. In 2010, her hit song 'Love Stairs' (天梯 Tianti) was one of the top ten gold songs<ref>[[2010 RTHK Top 10 Gold Songs Awards]] 2010 RTHK Top 10 Gold Songs Awards. Wikipedia. Web. Retrieved 01 Apr. 2015.</ref><ref name="2011 Jade Solid Gold Best Ten Music Awards Presentation">[[2011 Jade Solid Gold Best Ten Music Awards Presentation]] 2011 Jade Solid Gold Best Ten Music Awards Presentation. Wikipedia. Web. Retrieved 01 Apr. 2015.</ref> at the [[RTHK Top 10 Gold Songs Awards|2010 RTHK Top 10 Gold Songs Awards]]  as well as [[Jade Solid Gold Best Ten Music Awards Presentation|TVB Jade Solid Gold Best Ten Music Awards]]. In 2011, 'Lover Stairs' also won the Best Melody as well as the [[Composers and Authors Society of Hong Kong|CASH]]  Best Song award at the [[Composers and Authors Society of Hong Kong|CASH]]  Golden Sail Music Awards presentation. As of March 2015, the viewership of the official Music Video of 'Love Stairs' exceeded 10 million, which tops all the Cantopop Music Videos.<ref name="天梯 C AllStar 原裝MV (1080P HD). Perf. C AllStar. YouTube. KingdomCmusic, 25 Nov. 2010. Web. Retrieved 01 Apr. 2015.">[https://www.youtube.com/watch?v=eBvarz3DY00] 天梯 C AllStar 原裝MV (1080P HD). Perf. C AllStar. YouTube. KingdomCmusic, 25 Nov. 2010. Web. Retrieved 01 Apr. 2015.</ref>

== Works ==

=== 2010 ===

* Make It Mellow – [[:zh-tw:C AllStar|C AllStar]] (composer/arranger)
* iSing – [[:zh-tw:C AllStar|C AllStar]] (composer)
* 80後時代曲 (80 hou shidai qu) – [[:zh-tw:C AllStar|C AllStar]] (co-composer/arranger)
* 我們的電車上 (Women de dianche shang) – [[:zh-tw:C AllStar|C AllStar]] (arranger)
* Make It Groovy – [[:zh-tw:C AllStar|C AllStar]] (composer/arranger)
* 白色信件 (Let It Snow) – [[:zh-tw:C AllStar|C AllStar]] (arranger)
* iSing (A cappella) – [[:zh-tw:C AllStar|C AllStar]] (composer/arranger)
* 裙下之臣 (Qun xia zhi chen) – [[:zh-tw:C AllStar|C AllStar]] (co-arranger)
* 0809 – [[:zh-tw:C AllStar|C AllStar]] (arranger)
* To Be Continued... – [[:zh-tw:C AllStar|C AllStar]] (composer/arranger)
* 我們結婚了 (Women jiehunie) – [https://zh.wikipedia.org/zh-mo/%E8%91%89%E6%96%87%E8%BC%9D 葉文輝 (Barry Ip)] feat. [[:zh-tw:范萱蔚|范萱蔚 (Percy Fan)]] (arranger)
* 我們結婚了 (嫁嫁嫁... KaKaKa) (Women jiehunie (Jia Jia Jia...KaKaKa) – [https://zh.wikipedia.org/zh-mo/%E8%91%89%E6%96%87%E8%BC%9D 葉文輝 (Barry Ip)] feat. [[:zh-tw:范萱蔚|范萱蔚 (Percy Fan)]] (arranger)

===2011===

* 全情關注 (Quan qing Guanzhu) –  [[:zh-tw:C AllStar|C AllStar]] (composer/arranger)
* 我們的胡士托 (Women de hu shi tuo) – [[:zh-tw:C AllStar|C AllStar]] (composer/arranger)
* iSing (what I mean) – [[:zh-tw:C AllStar|C AllStar]] (composer)
* We will live as one – [[:zh-tw:C AllStar|C AllStar]] (composer/arranger)
* 韆鞦萬世 (Qianqiu wanshi) – [[:zh-tw:C AllStar|C AllStar]] (arranger)
* 天梯 (Love Stairs/Tianti) – [[:zh-tw:C AllStar|C AllStar]] (composer/co-arranger)
* 時間之光 (Shijian zhi quang) – [[:zh-tw:C AllStar|C AllStar]] (arranger)
* 足球先生 (Zuqiu xiansheng) – [[:zh-tw:C AllStar|C AllStar]] (composer/arranger)
* 小確幸 (Xiao que xing) – [[:zh-tw:C AllStar|C AllStar]] (composer/arranger)
* 天梯 (Love Stairs/Tianti) string version – [[:zh-tw:C AllStar|C AllStar]] (composer/co-arranger)
* 雨天沒有你 (Yutian meiyou ni) – [[:zh-tw:C AllStar|C AllStar]] (arranger)
* 2013 的約定 (2013 de yueding ) – [[:zh-tw:C AllStar|C AllStar]] (arranger)
* Love Life – [[:zh-tw:C AllStar|C AllStar]] (arranger)
* We Are Kitchee! – [[:zh-tw:C AllStar|C AllStar]] (arranger)
* 愛在當下 (香港電台【太陽計劃 2011】活動主題曲) ("Love The Moment" RTHK 2011 Solar Project theme song) – [[G.E.M. (singer)|G.E.M.]], [[:zh-tw:C AllStar|C AllStar]] (composer)

===2012===

* 從結束中開始 (Cong jieshu zhong kaishi) ) – [[:zh-tw:C AllStar|C AllStar]] (co-composer/arranger)
* 切膚之痛 (Qie fu zhi tong) – [[:zh-tw:C AllStar|C AllStar]] (composer/co-arranger)
* 少數 (Shao shu) – [[:zh-tw:C AllStar|C AllStar]] (arranger)
* 直到你肯定 (Zhidao ni kending) – [[:zh-tw:C AllStar|C AllStar]] (arranger)
* 一切從簡 (Yi qie cong jian) – [[:zh-tw:C AllStar|C AllStar]] (arranger)
* 不知不覺已大個 (Bu zhi bu jue yi da ge) – [[:zh-tw:C AllStar|C AllStar]] (arranger)
* 從當天到今天 (Cong dang tian dao jin tian) – [[:zh-tw:C AllStar|C AllStar]] (arranger)
* 第一步 (Di yi bu) (Dr.Martens advertising song) – [[:zh-tw:C AllStar|C AllStar]] (composer/arranger)
* Lost and Found – [http://www.electonemusic.com/artists/tiffany-chan.html 陳蔚琦(Tiffany Chan)] (arranger)

===2013===

* 混沌 (Hun dun) – [[:zh-tw:C AllStar|C AllStar]] (co-arranger)
* 音樂殖民地 (Yin yue zhi min di) – [[:zh-tw:C AllStar|C AllStar]] (composer/arranger)
* 老調兒 (Lao diao er) – [[:zh-tw:C AllStar|C AllStar]] (composer/arranger)
* 她結他 (Ta jie ta) – [[:zh-tw:C AllStar|C AllStar]] feat. [[:zh-tw:SimC|SimC]] (arranger)
* 騷動 (Sao dong) – [[:zh-tw:C AllStar|C AllStar]] (composer/arranger)
* 大同 (Da tong) – [[:zh-tw:C AllStar|C AllStar]] (composer/arranger)
* 我們的胡士托 (紅館版) (Women de hu shi tuo (HK Coliseum version)) – [[:zh-tw:C AllStar|C AllStar]] (composer/arranger)
* 六神合體 (Liu shen he ti) – [[:zh-tw:C AllStar|C AllStar]] (composer/arranger)
* It's All Gone – [http://www.electonemusic.com/artists/tiffany-chan.html 陳蔚琦(Tiffany Chan)] (arranger)

===2014===

* 時日如飛 (Shi ri ru fei) – [[:zh-tw:C AllStar|C AllStar]] (arranger)
* 星海 (Xing hai) – [[:zh-tw:C AllStar|C AllStar]] (composer/arranger)
* 大同 (Da tong) Jazz Mix – [[:zh-tw:C AllStar|C AllStar]] (composer/arranger)
* 城惶城恐 (Cheng huang cheng kong) – [[:zh-tw:C AllStar|C AllStar]] (composer/arranger)
* 粉紅色的一生 (Fen hong se de yi sheng) - [[Pink Dot SG#Pink Dot HK|Pink dot Hong Kong]] theme song – [[:zh-tw:C AllStar|C AllStar]] (arranger)

===2015===

* 戰場上的最後探戈 (Zhan chang shang de zui hou tan ge) – [[:zh-tw:C AllStar|C AllStar]] feat. [[:zh-tw:顏卓靈|顏卓靈 (Cherry Ngan)]](composer/arranger)
* 煙花非花 (Yan hua fei hua) – [[:zh-tw:C AllStar|C AllStar]] feat. [[Kenny Bee|鍾鎮濤 (Kenny Bee)]] (co-arranger)
* 時間之光 (Shi jian zhi guang) – [[:zh-tw:C AllStar|C AllStar]] feat. [[Sammi Cheng|鄭秀文 (Sammi Cheng)]] (arranger)
* SuperStar (SuperXmaStar Mix) – [[:zh-tw:C AllStar|C AllStar]] feat. [[:zh-tw:Super Girls|Super Girls]] (co-arranger)
* 上車咒 (Shang che zhou) – [[:zh-tw:C AllStar|C AllStar]] (co-arranger)

== Concerts ==

{|class="wikitable"
|- style="background:snow; align=center
|style="width:13%"|'''Date'''||style="width:40%"|''Concert Name''||style="width:10%"|'''No. of Show(s)'''||style="width:23%"|'''Venue'''||style="width:14%"|'''Role'''
|-
|7/24/2011 || [[:zh-tw:C AllStar|C AllStar]] x HKAO 「Post 80s' Symphony Generation Concert」<ref name="URBTIX 16695">[http://timable.com/en/event/16695] URBTIX. "C AllStar x HKAO 80後管弦時代曲音樂會." Timable. Web. Retrieved 01 Apr. 2015.</ref> || '''1''' || [[Sha Tin Town Hall]] || Keyboardist
|-
|11/9/2011 || 903id club Live [[:zh-tw:C AllStar|C AllStar]] x [[Eman Lam]] Concert <ref name="C AllStar X 林二汶 - 拉闊合奏會 (影像精華重溫). Perf. C AllStar and Eman Lam. YouTube. Gary C AllStar, 19 Dec. 2011. Web. Retrieved 01 Apr. 2015.">[https://www.youtube.com/watch?v=2Wn-FTGo40U] C AllStar X 林二汶 - 拉闊合奏會 (影像精華重溫). Perf. C AllStar and Eman Lam. YouTube. Gary C AllStar, 19 Dec. 2011. Web. Retrieved 01 Apr. 2015.</ref> || '''1''' || [[Kowloonbay International Trade & Exhibition Centre|Rotunda 3, KITEC]] || Keyboardist
|-
|12/27/2011 || Neway Music Live X [[:zh-tw:C AllStar|C AllStar]] Concert <ref name="Average Guy 27478">[http://timable.com/event/27478] Average Guy. "Neway Music Live X C AllStar 音樂會." Timable. Web. Retrieved 01 Apr. 2015.</ref> || '''1''' || [[Kowloonbay International Trade & Exhibition Centre|Rotunda 3, KITEC]]  || Keyboardist
|-
|11/23-24/2012 || '''[[:zh-tw:C AllStar|C AllStar]] C AllLive Concert 2012''' <ref name="Hong Kong Ticketing 55601">[http://timable.com/en/event/55601] Hong Kong Ticketing. "C AllStar 2012 C AllLive 演唱會." Timable. Web. Retrieved 01 Apr. 2015.</ref>|| '''2''' || [[Kowloonbay International Trade & Exhibition Centre#Star Hall|Star Hall, KITEC]]  || Keyboardist
|-
|5/22/2013 || Fruitips presents [[:zh-tw:C AllStar|C AllStar]] Juicy Concert <ref name="Average Guy 109944">[http://timable.com/en/event/109944] Average Guy. "C AllStar Juicy 演唱會." Timable. Web. Retrieved 01 Apr. 2015.</ref> || '''1''' || [http://www.polyu.edu.hk/cpeo/jca/index.php?lang=en Jockey Club Auditorium, Poly U]  || Music Director, Keyboardist
|-
|3/23/2014 || '''[[:zh-tw:C AllStar|C AllStar]] 'Women de hu shi tuo' Concert 2014''' <ref name="Katrine - C AllStar to Hold Concert at Hong Kong Coliseum">[http://www.jaynestars.com/music/c-allstar-to-hold-concert-at-hong-kong-coliseum] Katrine. "C AllStar to Hold Concert at Hong Kong Coliseum." JayneStarscom, 29 Jan. 2014. Retrieved 01 Apr. 2015.</ref>|| '''1''' || [[Hong Kong Coliseum]] || Keyboardist
|-
|8/2/2014 || '''[[:zh-tw:C AllStar|C AllStar]] 'Women de hu shi tuo  Concert 2014' – Macau Stop'''<ref name="CotaiTicketing 534767">[http://timable.com/zh-hk/event/534767] CotaiTicketing. "C AllStar《我們的胡士托》演唱會2014 澳門站." Timable. Web. Retrieved 01 Apr. 2015.</ref> || '''1''' || [[:zh-tw:威尼斯人劇院|Venetian Theatre, The Venetian Macao]]  || Keyboardist

|}

== Musicals ==

{|class="wikitable"
|- style="background:snow; align=center
|style="width:13%"|'''Date'''||style="width:40%"|''Musical's Name''||style="width:10%"|'''No. of Show(s)'''||style="width:23%"|'''Venue'''||style="width:14%"|'''Role'''
|-
|6-9/10/2011 || [[:zh-tw:C AllStar|C AllStar]] X [[:zh-tw:健吾|Kengo Ip (健吾)]] 《摘星天梯》(Zhai Xing Tianti) <ref name="URBTIX 18928">[http://timable.com/event/18928] URBTIX. "《摘星天梯》C AllStar X 健吾 首個流行音樂劇場." Timable. Web. Retrieved 01 Apr. 2015.</ref> || '''4''' || [[Sheung Wan Civic Centre|Sheung Wan Civic Centre, Theatre]]  || Music Director, Keyboardist
|-
|25-28/4/2013 ||《音樂情書》Heartbeat Delivered <ref name="'《音樂情書》Heartbeat Delivered. Perf. C AllStar. YouTube. BravoTheatre的頻道, 27 Mar. 2013. Web. Retrieved 01 Apr. 2015.">[https://www.youtube.com/watch?v=Q-4oSDaYq3c] 《音樂情書》Heartbeat Delivered. Perf. C AllStar. YouTube. BravoTheatre的頻道, 27 Mar. 2013. Web. Retrieved 01 Apr. 2015.</ref> || '''6''' || [[Sheung Wan Civic Centre|Sheung Wan Civic Centre, Theatre]]  || Music Director, Keyboardist
|}

== Solo Performance ==

{|class="wikitable"
|- style="background:snow; align=center
|style="width:15%"|'''Date'''||style="width:32%"|''Concert Name''||style="width:10%"|'''No. of Show(s)'''||style="width:17%"|'''Venue'''||style="width:15%"|'''Role'''||style="width:24%"|'''Note'''
|-
|12/13/2012 || [[Swire Properties]] presents Music@Via Fiori<ref name="Bitone">[http://bitetone.com/2012/10/09/%E9%A6%99%E6%B8%AF-%E6%96%B0%E4%B8%96%E4%BB%A3%E5%94%B1%E4%BD%9C%E4%BA%BA%E3%80%8C%E5%94%B1%E3%80%8D%E6%89%80%E6%AC%B2%E8%A8%80-%E6%B8%AF%E5%B3%B6%E6%9D%B1%E4%B8%AD%E5%BF%83%E9%9C%B2%E5%A4%A9/] Bitone. "[香港] 新世代唱作人「唱」所欲言 @ 港島東中心露天廣場.", 9 Oct. 2012. Web. Retrieved 01 Apr. 2015.</ref> || '''1''' || [[One Island East]] || Performer (Singer) || featuring [[:zh-tw:C AllStar|C AllStar]]
|-

|}

==References==
{{reflist}}
* [[:zh-tw:C AllStar|C AllStar's Wikipedia Page (in Chinese)]]
* [http://www.hket.com/eti/article/e0c91d93-76d2-4745-b12e-7c7f3dbcace9-194151 "【專訪】岑寧兒×賴映彤 我歌寫我心." Hong Kong Economic Times. Ed. 家歷 區. Hong Kong Economic Times, 7 Nov. 2012. Web. Retrieved 01 Apr. 2015.]
* [http://www.cash.org.hk/files/pdf/publications/cashflow/Interview/Interview-LaiYingTong.pdf "Winner of the 4th Joseph Koo New Generation Award Lai Ying Tong – On Building Her Stairs to Music." Composers and Authors Society of Hong Kong. Manfred Wong. CASH FLOW 70 (2012): 22-26. Jan. 2013. Web. Retrieved 01 Apr. 2015.]
* [http://mojim.com/%E8%B3%B4%E6%98%A0%E5%BD%A4.html?t4 "賴映彤." 【 賴映彤 】 【 團體 】 【 歌詞 】共有 74筆相關歌詞. Mojim.com. Web. Retrieved 01 Apr. 2015.]

== External links ==
* [https://www.facebook.com/tung.siu.735?fref=ts Official Facebook Account]
* [https://instagram.com/tungmusic/ Official Instagram Account]
* [http://weibo.com/u/1730824574?topnav=1&wvr=6&topsug=1 Official Sina Weibo Account]
* [http://www.cash.org.hk/en/media-centre/video_item.php?id=8&sid=66 Video Footage of The 4th Joseph Koo New Generation Award (2012)]
* [http://www.cash.org.hk/tc/media-centre/video_item.php?id=2&sid=114 Video Interview with Lai Ying Tong by CASH]
* [http://callstar.hk/ C AllStar's Official Website]
* [[:zh-tw:C AllStar|C AllStar's Wikipedia (In Chinese)]]
* [[:zh-tw:Make it Happen|C AllStar's debut album Make it Happen's Wikipedia (In Chinese)]]

{{DEFAULTSORT:Lai, Ying Tong}}
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:Cantopop]]
[[Category:Hong Kong songwriters]]
[[Category:Hong Kong musicians]]
[[Category:Music educators]]
[[Category:Hong Kong pianists]]
[[Category:Alumni of the Chinese University of Hong Kong]]