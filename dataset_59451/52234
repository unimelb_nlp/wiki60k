{{confuse|Edinburgh Mathematical Notes}}
{{Infobox journal
| title = Mathematical Notes
| cover =
| editor = [[Victor P. Maslov]]
| discipline = [[Mathematics]]
| abbreviation = Math. Notes
| formernames = Mathematical Notes of the Academy of Sciences of the USSR
| publisher = [[Springer Science+Business Media]] on behalf of the [[Russian Academy of Sciences]]
| country = Russia
| frequency = Monthly
| history = 1967-present
| openaccess = Yes
| license =
| impact = 0.295
| impact-year = 2011
| website = http://www.springer.com/mathematics/journal/11006
| link1 = http://www.springerlink.com/content/0001-4346/
| link1-name = Online access
| link2 =
| link2-name =
| JSTOR =
| OCLC = 754641284
| LCCN = sn92025928
| CODEN =
| ISSN = 0001-4346
| eISSN = 1573-8876
}}
'''''Mathematical Notes''''' is a [[Peer review|peer-reviewed]] [[mathematical journal]] published by [[Springer Science+Business Media]] on behalf of the [[Russian Academy of Sciences]] that covers all aspects of [[mathematics]]. It is an English language translation of the Russian-language journal '''''Matematicheskie Zametki''''' ({{lang-ru|Математические заметки}}) and is published simultaneously with the Russian version.

The journal was established in 1967 as ''Mathematical Notes of the Academy of Sciences of the USSR'' and obtained its current title in 1991. The current [[editor-in-chief]] is [[Victor P. Maslov]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2011 [[impact factor]] of 0.295.<ref name=WoS>{{cite book |year=2013 |chapter=Mathematical Notes |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2013-01-20 |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

==External links==
* {{Official website|http://www.springer.com/mathematics/journal/11006}}

{{Use dmy dates|date=December 2010}}

[[Category:Mathematics journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1967]]
[[Category:Russian Academy of Sciences academic journals]]
[[Category:Russian-language journals]]