{{Infobox GP2 Asia round report
| Country                   = UAE
| Grand Prix                = Abu Dhabi
| Round_No                  = 1
| Season_No                 = 2
| Season                    = 2011
| Image                     = Circuit Yas-Island.svg
| Caption                   = Yas Marina Circuit
| Year                      = 2011
| Location                  = [[Yas Marina Circuit]], [[Abu Dhabi]], [[United Arab Emirates]]
| Course                    = Permanent racing facility
| Course_mi                 = 2.939
| Course_km                 = 4.730
| Date_r1                   = 11 February
| Laps_r1                   = 36
| Pole_driver_country       = France
| Pole_driver               = [[Romain Grosjean]]
| Pole_team                 = [[DAMS]]
| Pole_time                 = 1:36.041
| First_driver_country_r1   = France
| First_driver_r1           = [[Jules Bianchi]]
| First_team_r1             = [[ART Grand Prix|Lotus ART]]
| Second_driver_country_r1  = France
| Second_driver_r1          = [[Romain Grosjean]]
| Second_team_r1            = [[DAMS]]
| Third_driver_country_r1   = Italy
| Third_driver_r1           = [[Davide Valsecchi]]
| Third_team_r1             = [[Team Air Asia]]
| Fast_driver_country_r1    = France
| Fast_driver_r1            = [[Jules Bianchi]]
| Fast_team_r1              = [[ART Grand Prix|Lotus ART]]
| Fast_time_r1              = 1:38.141
| Fast_lap_r1               = 31
| Date_r2                   = 12 February
| Laps_r2                   = 26
| First_driver_country_r2   = Monaco
| First_driver_r2           = [[Stefano Coletti]]
| First_team_r2             = [[Trident Racing]]
| Second_driver_country_r2  = Czech Republic
| Second_driver_r2          = [[Josef Král]]
| Second_team_r2            = [[Arden International]]
| Third_driver_country_r2   = Sweden
| Third_driver_r2           = [[Marcus Ericsson]]
| Third_team_r2             = [[iSport International]]
| Fast_driver_country_r2    = France
| Fast_driver_r2            = [[Jules Bianchi]]
| Fast_team_r2              = [[ART Grand Prix|Lotus ART]]
| Fast_time_r2              = 1:37.745
| Fast_lap_r2               = 23
}}

The '''2011 Abu Dhabi GP2 Asia round''' was the first round of the [[2011 GP2 Asia Series season]]. It was held on February 11 and 12, 2011 at [[Yas Marina Circuit]] in [[Abu Dhabi]], [[United Arab Emirates]].

==Classification==

===Qualifying===
{| class="wikitable" style="font-size: 95%"
!Pos
!No
!Name
!Team
!Time
!Grid
|-
!1
|9
|{{flagicon|FRA}} [[Romain Grosjean]]
|[[DAMS]]
|1:36.041
|1
|-
!2
|5
|{{flagicon|FRA}} [[Jules Bianchi]]
|[[ART Grand Prix|Lotus ART]]
|1:36.448
|2
|-
!3
|27
|{{flagicon|ITA}} [[Davide Valsecchi]]
|[[Team AirAsia]]
|1:36.501
|3
|-
!4
|23
|{{flagicon|ESP}} [[Dani Clos]]
|[[Racing Engineering]]
|1:36.614
|4
|-
!5
|17
|{{flagicon|NED}} [[Giedo van der Garde]]
|[[Addax Team|Barwa Addax Team]]
|1:36.658
|5
|-
!6
|1
|{{flagicon|SWE}} [[Marcus Ericsson]]
|[[iSport International]]
|1:36.659
|6
|-
!7
|25
|{{flagicon|GBR}} [[Max Chilton]]
|[[Carlin Motorsport|Carlin]]
|1:36.693
|7
|-
!8
|3
|{{flagicon|CZE}} [[Josef Král]]
|[[Arden International]]
|1:36.724
|8
|-
!9
|6
|{{flagicon|MEX}} [[Esteban Gutiérrez]]
|[[ART Grand Prix|Lotus ART]]
|1:36.741
|9
|-
!10
|22
|{{flagicon|FRA}} [[Nathanaël Berthon]]
|[[Racing Engineering]]
|1:36.825
|10
|-
!11
|2
|{{flagicon|GBR}} [[Sam Bird]]
|[[iSport International]]
|1:36.830
|11
|-
!12
|11
|{{flagicon|ROM}} [[Michael Herck]]
|[[Coloni Motorsport|Scuderia Coloni]]
|1:36.863
|12
|-
!13
|15
|{{flagicon|GBR}} [[Oliver Turvey]]
|[[Ocean Racing Technology]]
|1:36.875
|13
|-
!14
|16
|{{flagicon|FRA}} [[Charles Pic]]
|[[Addax Team|Barwa Addax Team]]
|1:36.961
|14
|-
!15
|20
|{{flagicon|SUI}} [[Fabio Leimer]]
|[[Rapax]]
|1:37.047
|15
|-
!16
|8
|{{flagicon|VEN}} [[Johnny Cecotto Jr.]]
|[[Super Nova Racing]]
|1:37.129
|16
|-
!17
|21
|{{flagicon|ITA}} [[Julián Leal]]
|[[Rapax Team|Rapax]]
|1:37.145
|17
|-
!18
|4
|{{flagicon|GBR}} [[Jolyon Palmer]]
|[[Arden International]]
|1:37.305
|18
|-
!19
|18
|{{flagicon|MON}} [[Stefano Coletti]]
|[[Trident Racing]]
|1:37.329
|19
|-
!20
|14
|{{flagicon|ITA}} [[Andrea Caldarelli]]
|[[Ocean Racing Technology]]
|1:37.345
|20
|-
!21
|26
|{{flagicon|BRA}} [[Luiz Razia]]
|[[Team AirAsia]]
|1:37.426
|21
|-
!22
|27
|{{flagicon|RUS}} [[Mikhail Aleshin]]
|[[Carlin Motorsport|Carlin]]
|1:37.535
|24{{ref|1|1}}
|-
!23
|7
|{{flagicon|Malaysia}} [[Fairuz Fauzy]]
|[[Super Nova Racing]]
|1:37.572
|26{{ref|2|2}}
|-
!24
|10
|{{flagicon|NOR}} [[Pål Varhaug]]
|[[DAMS]]
|1:37.632
|22
|-
!25
|19
|{{flagicon|VEN}} [[Rodolfo González]]
|[[Trident Racing]]
|1:37.669
|23
|-
!26
|12
|{{flagicon|GBR}} [[James Jakes]]
|[[Coloni Motorsport|Scuderia Coloni]]
|1:37.741
|25
|-
|}

'''Notes'''
#{{Note|1}} – Aleshin was handed a three gird position penalty for crossing the finish line more than once at the end of the session.<ref name="Ale-Fau">{{cite news|url=http://gp2series.com/News-Room/News/2011/02_Feb/Drivers-penalised-for-tomorrows-Abu-Dhabi-feature-Race/|title=Drivers penalised for tomorrow's Abu Dhabi Feature Race|work=[[GP2 Series]]|publisher=GP2 Motorsport Limited|date=10 February 2011}}</ref>
#{{Note|2}} – Fauzy was handed a ten grid position penalty for causing a collision.<ref name="Ale-Fau"/>

===Race 1===
{| class="wikitable" style="font-size:95%"
|-
! Pos !! No !! Driver !! Team !! Laps !! Time/Retired !! Grid !! Points
|-
!1
|5
|{{flagicon|FRA}} '''[[Jules Bianchi]]'''
|'''[[ART Grand Prix|Lotus ART]]'''
|33
|1:22:04.643
|2
|'''11'''
|-
!2
|9
|{{flagicon|FRA}} '''[[Romain Grosjean]]'''
|'''[[DAMS]]'''
|33
| +6.681
|1
|'''8'''
|-
!3
|27
|{{flagicon|ITA}} '''[[Davide Valsecchi]]'''
|'''[[Team AirAsia]]'''
|33
| +12.794
|3
|'''6'''
|-
!4
|1
|{{flagicon|SWE}} '''[[Marcus Ericsson]]'''
|'''[[iSport International]]'''
|33
| +14.001
|6
|'''5'''
|-
!5
|17
|{{flagicon|NED}} '''[[Giedo van der Garde]]'''
|'''[[Addax Team|Barwa Addax Team]]'''
|33
| +15.198
|5
|'''4'''
|-
!6
|3
|{{flagicon|CZE}} '''[[Josef Král]]'''
|'''[[Arden International]]'''
|33
| +20.601
|8
|'''3'''
|-
!7
|2
|{{flagicon|GBR}} '''[[Sam Bird]]'''
|'''[[iSport International]]'''
|33
| +24.412
|11
|'''2'''
|-
!8
|18
|{{flagicon|MON}} '''[[Stefano Coletti]]'''
|'''[[Trident Racing]]'''
|33
| +36.809
|19
|'''1'''
|-
!9
|16
|{{flagicon|FRA}} [[Charles Pic]]
|[[Addax Team|Barwa Addax Team]]
|33
| +37.411
|14
|
|-
!10
|20
|{{flagicon|SUI}} [[Fabio Leimer]]
|[[Rapax]]
|33
| +40.252
|15
|
|-
!11
|19
|{{flagicon|VEN}} [[Rodolfo González]]
|[[Trident Racing]]
|33
| +48.587
|23
|
|-
!12
|25
|{{flagicon|GBR}} [[Max Chilton]]
|[[Carlin Motorsport|Carlin]]
|33
| +51.866{{ref|1|1}}
|7
|
|-
!13
|11
|{{flagicon|ROM}} [[Michael Herck]]
|[[Coloni Motorsport|Scuderia Coloni]]
|33
| +59.963
|12
|
|-
!14
|4
|{{flagicon|GBR}} [[Jolyon Palmer]]
|[[Arden International]]
|33
| +1:07.206
|18
|
|-
!15
|8
|{{flagicon|VEN}} [[Johnny Cecotto Jr.]]
|[[Super Nova Racing]]
|33
| +1:08.136
|16
|
|-
!16
|14
|{{flagicon|ITA}} [[Andrea Caldarelli]]
|[[Ocean Racing Technology]]
|33
| +1:37.978
|20
|
|-
!17
|12
|{{flagicon|GBR}} [[James Jakes]]
|[[Coloni Motorsport|Scuderia Coloni]]
|32
|DNF
|25
|
|-
!18
|15
|{{flagicon|GBR}} [[Oliver Turvey]]
|[[Ocean Racing Technology]]
|32
| +1 Lap
|13
|
|-
!Ret
|22
|{{flagicon|FRA}} [[Nathanaël Berthon]]
|[[Racing Engineering]]
|24
|DNF
|10
|
|-
!Ret
|21
|{{flagicon|ITA}} [[Julián Leal]]
|[[Rapax Team|Rapax]]
|19
|DNF
|17
|
|-
!Ret
|7
|{{flagicon|MYS}} [[Fairuz Fauzy]]
|[[Super Nova Racing]]
|19
|DNF
|26
|
|-
!Ret
|6
|{{flagicon|MEX}} [[Esteban Gutiérrez]]
|[[ART Grand Prix|Lotus ART]]
|18
|DNF
|9
|
|-
!Ret
|27
|{{flagicon|RUS}} [[Mikhail Aleshin]]
|[[Carlin Motorsport|Carlin]]
|10
|DNF
|24
|
|-
!Ret
|23
|{{flagicon|ESP}} [[Dani Clos]]
|[[Racing Engineering]]
|0
|DNF
|4
|
|-
!Ret
|26
|{{flagicon|BRA}} [[Luiz Razia]]
|[[Team AirAsia]]
|0
|DNF
|21
|
|-
!Ret
|10
|{{flagicon|NOR}} [[Pål Varhaug]]
|[[DAMS]]
|0
|DNF
|22
|
|-
|colspan=8|{{center|''Fastest lap: [[Jules Bianchi]] ([[ART Grand Prix|Lotus ART]]) 1:38.141 (lap 31)''}}
|}

'''Notes'''
#{{Note|1}} – Chilton was penalised 20 seconds for failing to respect the track limits.<ref>{{cite news|url=http://gp2series.com/News-Room/News/2011/02_Feb/Max-Chilton-penalised/|title=Max Chilton penalised|work=[[GP2 Series]]|publisher=GP2 Motorsport Limited|date=11 February 2011}}</ref>

===Race 2===
{| class="wikitable" style="font-size:95%"
|-
! Pos !! No !! Driver !! Team !! Laps !! Time/Retired !! Grid !! Points
|-
!1
|18
|{{flagicon|MON}} '''[[Stefano Coletti]]'''
|'''[[Trident Racing]]'''
|26
|43:02.819
|1
|'''6'''
|-
!2
|3
|{{flagicon|CZE}} '''[[Josef Král]]'''
|'''[[Arden International]]'''
|26
| +2.629
|3
|'''5'''
|-
!3
|1
|{{flagicon|SWE}} '''[[Marcus Ericsson]]'''
|'''[[iSport International]]'''
|26
| +3.323
|5
|'''4'''
|-
!4
|27
|{{flagicon|ITA}} '''[[Davide Valsecchi]]'''
|'''[[Team AirAsia]]'''
|26
| +11.531
|6
|'''3'''
|-
!5
|11
|{{flagicon|ROM}} '''[[Michael Herck]]'''
|'''[[Coloni Motorsport|Scuderia Coloni]]'''
|26
| +14.687
|13
|'''2'''
|-
!6
|20
|{{flagicon|SUI}} '''[[Fabio Leimer]]'''
|'''[[Rapax]]'''
|26
| +17.175
|10
|'''1'''
|-
!7
|8
|{{flagicon|VEN}} [[Johnny Cecotto Jr.]]
|[[Super Nova Racing]]
|26
| +25.351
|15
|
|-
!8
|5
|{{flagicon|FRA}} [[Jules Bianchi]]
|[[ART Grand Prix|Lotus ART]]
|26
| +29.434{{ref|1|1}}
|8
|'''1'''
|-
!9
|19
|{{flagicon|VEN}} [[Rodolfo González]]
|[[Trident Racing]]
|26
| +30.947
|11
|
|-
!10
|4
|{{flagicon|GBR}} [[Jolyon Palmer]]
|[[Arden International]]
|26
| +32.897
|14
|
|-
!11
|14
|{{flagicon|ITA}} [[Andrea Caldarelli]]
|[[Ocean Racing Technology]]
|26
| +33.396
|16
|
|-
!12
|6
|{{flagicon|MEX}} [[Esteban Gutiérrez]]
|[[ART Grand Prix|Lotus ART]]
|26
| +33.643
|22
|
|-
!13
|12
|{{flagicon|GBR}} [[James Jakes]]
|[[Coloni Motorsport|Scuderia Coloni]]
|26
| +34.526
|17
|
|-
!14
|22
|{{flagicon|FRA}} [[Nathanaël Berthon]]
|[[Racing Engineering]]
|26
| +37.426{{ref|1|1}}
|19
|
|-
!15
|7
|{{flagicon|MYS}} [[Fairuz Fauzy]]
|[[Super Nova Racing]]
|26
| +38.094
|21
|
|-
!16
|26
|{{flagicon|BRA}} [[Luiz Razia]]
|[[Team AirAsia]]
|26
| +38.830
|25
|
|-
!17
|21
|{{flagicon|ITA}} [[Julián Leal]]
|[[Rapax Team|Rapax]]
|26
| +39.280
|20
|
|-
!18
|25
|{{flagicon|GBR}} [[Max Chilton]]
|[[Carlin Motorsport|Carlin]]
|26
| +42.205
|12
|
|-
!19
|15
|{{flagicon|GBR}} [[Oliver Turvey]]
|[[Ocean Racing Technology]]
|26
| +42.677
|18
|
|-
!20
|10
|{{flagicon|NOR}} [[Pål Varhaug]]
|[[DAMS]]
|26
| +43.137
|26
|
|-
!21
|16
|{{flagicon|FRA}} [[Charles Pic]]
|[[Addax Team|Barwa Addax Team]]
|26
| +45.493{{ref|1|1}}
|9
|
|-
!22
|23
|{{flagicon|ESP}} [[Dani Clos]]
|[[Racing Engineering]]
|26
| +47.035
|24
|
|-
!23
|17
|{{flagicon|NED}} [[Giedo van der Garde]]
|[[Addax Team|Barwa Addax Team]]
|26
| +49.926
|4
|
|-
!24
|27
|{{flagicon|RUS}} [[Mikhail Aleshin]]
|[[Carlin Motorsport|Carlin]]
|23
|DNF
|23
|
|-
!Ret
|2
|{{flagicon|GBR}} [[Sam Bird]]
|[[iSport International]]
|21
|DNF
|2
|
|-
!Ret
|9
|{{flagicon|FRA}} [[Romain Grosjean]]
|[[DAMS]]
|0
|DNF
|7
|
|-
|colspan=8|{{center|''Fastest lap: [[Jules Bianchi]] ([[ART Grand Prix|Lotus ART]]) 1:37.745 (lap 23)''}}
|}

'''Notes'''
#{{Note|1}} – Bianchi, Pic and Berthon were penalised 20 seconds for not respecting yellow flags.<ref>{{cite news|url=http://gp2series.com/News-Room/News/2011/02_Feb/Post-race-penalties/|title=Post race penalties|work=[[GP2 Series]]|publisher=GP2 Motorsport Limited|date=12 February 2011}}</ref>

==References==
<references/>

{{GP2 Asia round report
| Name_of_round            = Abu Dhabi GP2 Asia round
| Year_of_round            = 2011
| Previous_round_in_season = [[2009–10 Bahrain 2nd GP2 Asia round]]
| Next_round_in_season     = [[2011 Imola GP2 Asia Series round|2011 Imola GP2 Asia round]]
| Previous_year's_round    = [[2009–10 Abu Dhabi 2nd GP2 Asia round]]
| Next_year's_round        = None
}}

{{Use dmy dates|date=March 2011}}

[[Category:2011 in Emirati sport]]
[[Category:GP2 Asia Series]]