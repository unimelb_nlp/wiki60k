{|{{Infobox Aircraft Begin
|name           = NiD.52 & NiD.72
|image          = Nieuport-Delage NiD 52.jpg
|caption        = A NiD 52 of the French Air Force<!--Image caption; if it isn't descriptive, please skip-->
}}{{Infobox Aircraft Type
|type           = Fighter
|national origin = [[France]] 
|manufacturer   = [[Nieuport-Delage]]
|designer       = <!--Only appropriate for single designers, not project leaders-->
|first flight   = 1927<!--If this hasn't happened, skip this field!-->
|introduction   = 1929<!--Date the aircraft entered or will enter military or revenue service-->
|retired        = <!--Date the aircraft left service. If vague or more than a few dates, skip this -->
|primary user   = [[Spain]] 
|produced       = <!--Years in production (eg. 1970–1999) if still in active use but no longer built -->
|number built   = 135
|program cost   = <!--Total program cost-->
|unit cost      = <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
|developed from = [[Nieuport-Delage NiD 42]]<!--The aircraft which formed the basis for this aircraft-->
|variants with their own articles = <!--Variants OF this aircraft-->
}}
|}

The '''Nieuport-Delage NiD 52''' was a [[France|French]] fighter aircraft of the 1920s. A single-engined [[sesquiplane]], it served with the [[Spanish Air Force]], being operated by both sides of the [[Spanish Civil War]].<ref>Taylor and Alexander 1969, pp. 117-118.</ref>
{{TOC limit|limit=2}}

==Development and design==
In 1924 [[Nieuport]] produced a design for a single-seat [[sesquiplane]] fighter of mixed construction, the [[Nieuport-Delage NiD 42]], which was ordered in small numbers for the French air force, entering service in 1927.<ref name="Donald world p688">Donald 1997, p. 688.</ref> Nieuport developed three refined versions in 1927, the all-metal NiD-52 and NiD-72 and the mixed construction [[Nieuport-Delage NiD 62]]. Like the NiD 42 on which it was based, the NiD 52 was powered by a [[Hispano-Suiza 12Hb]] [[V12 engine]], but the wooden [[monocoque]] rear fuselage was replaced by an equivalent made of [[duralumin]] and the wooden wing ribs by light alloy, while retaining a fabric covering. Both the main and secondary wings were of reduced area compared to the NiD-42, and an enlarged tail fitted in an attempt to improve the aircraft's handling. Armament remained two 7.7&nbsp;mm [[Vickers machine gun]]s.<ref name="Donald World p689">Donald 1997, p.689.</ref><ref name="AI Feb90 p80-1">Green and Swanborough 1990, pp. 80–81.</ref>

The prototype NiD 52 flew in late 1927,<ref name="AI Feb90 p80">Green and Swanborough 1990, p.80.</ref> with the NiD 72, which was similar to the NiD 52 but had duralumin skinning on the wing and had a further reduced wing area, and the NiD 62 flying in January 1928.<ref name="AI Feb90 p 81">Green and Swanborough 1990, p. 81.</ref>  Although France preferred the cheaper NiD 62, purchasing it in large numbers, the NiD 52 won a competition for a new fighter for [[Spain]] in 1928,<ref name="Donald World p689"/> purchasing a licence for the construction of 125 aircraft to be built by [[Hispano-Suiza]] in their factory at [[Guadalajara, Spain|Guadalajara]].<ref name="AI Feb90 p 81"/> The similar NiD 72 was ordered in small numbers by Belgium and Brazil.<ref name="AI Feb90 p83">Green and Swanborough 1990, p. 83.</ref>

==Operational history==

===Spain===
The Spanish Air Force started to take deliveries of NiD 52s in 1930, production continuing until 1933, equipping three fighter units, ''Grupo'' 11, ''Grupo'' 1 and ''Grupo'' 13. The "Hispano-Nieuport" (as it was known) was unpopular in Spanish service, being described as heavy and unresponsive, while it was slower than expected, with Spanish aircraft only able to reach 225&nbsp;km/h (140&nbsp;mph) compared with the 260&nbsp;km/h (162&nbsp;mph) claimed by Nieuport. Losses to accidents were heavy, with only 56 remaining when the [[Spanish Civil War]] broke out on 18 July 1936.<ref name="AI Feb90 p83"/>  
 
The majority of the surviving Hispano-Nieuport remained in government hands during the civil war, with only 11 falling into Nationalist hands, including three aircraft that mistakenly landed in Nationalist territory on 21 July. The Republican forces were strengthened by Hispano-Suiza building a further 10 aircraft from spares in August–September 1936.<ref name="AI Feb 90 p83,92">Green and Swanborough 1990, pp. 83, 92.</ref>

Until more modern fighters could be obtained, the elderly Hispano-Nieuport was an important part of Republican fighter strength, with Republican and Nationalist NiD 52s facing each other in combat several times in the early months of the war,<ref name="Valverde data">Valverde, M.R. [http://usuarios.multimania.es/mrodval/fi1815.htm "Republica Nacional: Nieuport Delage NI-52"(In Spanish).]  ''Aviones de la guerra civil Española '',  5 April 2005. Retrieved: 21 May 2010.</ref> which resulted in at least one case of one Republican unit of NiD 52s attacking another, resulting in the loss of a Hispano-Nieuport.<ref name="AI Feb90 p82">Green and Swanborough 1990, p. 92.</ref> The Republican NiDs soon found themselves outclassed by more modern [[Fiat CR.32]] and [[Heinkel He 51]] fighters operated by the Italian [[Aviazione Legionaria]] and the German [[Condor Legion]] supporting the Nationalists, with it being claimed that three NiD 52s could just about hold their own against a single Fiat CR.32. Despite this, most losses were from accidents, not combat, particularly when being flown by foreign Volunteer pilots not used to the difficult handling.<ref name="AI Feb90 p92-3">Green and Swanborough 1990, pp. 92–93.</ref>

The NiD 52 was withdrawn from the front line during the winter of 1936–37, being relegated to training and coastal patrol, although they were briefly pressed back into combat following the [[Battle of Guadalajara]], being used to attack the retreating Italians.<ref name="AI Feb 90 p93">Green and Swanborough 1990, p. 93.</ref> No NiD 52s survived the war.<ref name="Valverde data"/>

===Brazil===
When the [[Constitutionalist Revolution]] began in Brazil on 9 July 1932, Brazil had two NiD 72s still servicable, both of which served with the ''Legalista'' forces loyal to President [[Getúlio Vargas]]. On 21 August 1932, however, Captain Adherbal de Costa Oliveria defected to the ''Constitucionalist'' rebels. Both NiD 72s were heavily used until the end of the uprising.<ref name="AI Feb90 p83"/><ref name="Flores">Flores 1988, pp. 71–73.</ref>

==Variants==
;Nieuport-Delage NiD 52
:Single-seat fighter aircraft.
;Nieuport-Delage NiD 72
:Improved version of the NiD 52. Three aircraft were delivered to Belgium in 1929, four modified aircraft were delivered to Brazil in 1931. One aircraft modified from a NiD-62 was powered by a [[Hispano-Suiza 12Lb]] V-12 engine.
;Nieuport-Delage NiD 82
:This prototype was powered at first by a 600 hp (447 kW) [[Hispano-Suiza 12Lb]] engine. Later it was fitted with a 500 hp (373 kW) [[Lorraine 12Ha Pétrel]] engine.

==Operators==
;{{BEL}}: [[Belgian Air Force]] (3 NiD.72, evaluation only)
;{{BRA}}: [[Brazilian Air Force]] (NiD.72)
;{{flag|Spain|1931}}: [[Spanish Republican Air Force]] (NiD.52)
;{{ESP}}: [[Spanish Air Force]] (NiD.52)
[[Romanian Air Force]] (3 NiD.72, wooden fuselage)

==Specifications (NiD 52)==
{{aircraft specifications
|plane or copter?=plane
|jet or prop?=prop
|ref=The Complete Book of Fighters <ref name="complete fighters p439-440">Green and Swanborough 1994, pp. 439–440.</ref> 
|crew=1
|capacity=
|length main= 7.64 m
|length alt= 25 ft {{frac|0|3|4}} in
|span main= 12.00 m
|span alt= 39 ft {{frac|4|1|2}} in
|height main= 3.00 m
|height alt= 9 ft 10⅛ in
|area main=  27.8m²
|area alt= 299 ft²
|airfoil=
|empty weight main= 1,360 kg
|empty weight alt=  2,998 lb
|loaded weight main=  1,800 kg
|loaded weight alt= 3,968&nbsp;lb
|useful load main=  
|useful load alt=  
|max takeoff weight main= 
|max takeoff weight alt= 
|more general=
|engine (prop)=[[Hispano-Suiza 12Hb]]
|type of prop=liquid-cooled [[V12 engine|V-12]] engine
|number of props=1
|power main= 373 kW
|power alt= 500 hp
|power original=
|max speed main= 260 km/h
|max speed alt= 141 knots, 162 mph
|max speed more= at 1,800 m (5,900 ft)
|cruise speed main=  
|cruise speed alt=  
|stall speed main=  
|stall speed alt=  
|never exceed speed main=  
|never exceed speed alt=  
|range main= 400 km <ref name="aviafrance">[http://www.aviafrance.com/577.htm "Nieuport-Delage NiD-52" (in French).] ''Aviafrance''. . Retrieved: 5 April 2008.</ref>
|range alt=  216 nmi, 248 mi 
|ceiling main= <!--m -->8,200 m
|ceiling alt= <!--ft -->26,900 ft<ref name="combat p117-8">Taylor and Alexander 1969, pp. 117–118.</ref>
|climb rate main= 
|climb rate alt= 
|loading main=  
|loading alt=  
|thrust/weight=
|power/mass main= <!--W/kg -->
|power/mass alt= <!--hp/lb -->
|more performance=*'''Climb to 5,000 m (16,400 ft):''' 13.5 min
|guns= 2 × 7.7 mm [[machine gun]]s
|avionics=
}}

==See also==
{{aircontent|
|related=
|similar aircraft=
|sequence=
|lists=
* [[List of Interwar military aircraft]]
* [[List of aircraft of the Spanish Republican Air Force]]
|see also=
}}

==References==

===Notes===
{{Reflist|30em}}

===Bibliography===
{{Refbegin}}
* Donald, David (editor). ''The Encyclopedia of World Aircraft''. London: Aerospace Publishing. 1997. ISBN 1-85605-375-X.
* Flores Jr, Jackson. "The Brazilian Air War". ''[[Air Enthusiast]]'', Thirty-five, January–April 1988, pp.&nbsp;64–73. Bromley, UK: FineScroll. ISSN 0143-5450.
* Green, William and Gordon Swanborough. ''The Complete Book of Fighters''. New York: Smithmark, 1994. ISBN 0-8317-3939-8.
* Green, William and Gordon Swanborough. "A Gallic Rarity ... The 'One-and-a-Half' Nieuport-Delage". ''[[Air International]]'',  Vol. 38, No. 2, February 1990, pp.&nbsp;75–83, 92–93, 97. Bromley, UK: Tri-Service Press.  ISSN 0306-5634.
* Taylor, John W. R. and Jean Alexander. ''Combat Aircraft of the World''. New York: G.P. Putnam's Sons, 1969. ISBN 0-71810-564-8.
{{Refend}}

==External links==
{{commons category|Nieuport-Delage NiD 52}}
*[http://usuarios.lycos.es/mrodval/GC181701.HTM NiD 52 in action] (Spanish)
*[http://1000aircraftphotos.com/Contributions/GermanPostcards/4525.htm Photo]

{{Nieuport aircraft}}

[[Category:French fighter aircraft 1930–1939]]
[[Category:Nieuport aircraft|NiD 052]]
[[Category:Sesquiplanes]]