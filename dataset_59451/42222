<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=An-10
 |image= File:Aeroflot An-10A CCCP-11213 Monino 1992-6-4.png
|caption=An Aeroflot An-10A at Monino in 1992
}}{{Infobox Aircraft Type
 |type=[[Airliner]]
 |manufacturer=[[Antonov]]
 |designer=[[N. S. Trunchenkov]] &  [[V. N. Ghel'prin]]<ref name="Gordon An-12">Gordon,Yefim & Komissarov, Dmitry. Antonov An-12. Midland. Hinkley. 2007. ISBN 1-85780-255-1 ISBN 978-1-85780-255-9</ref>
 |first flight=7 March 1957<ref name="Gordon An-12"/>
 |introduced=1959<ref>{{cite web
 |url=http://www.aviation.ru/An/ 
 |title=Antonov 
 |work=Aviation.ru 
 |date=2004-05-24 
 |accessdate=2006-06-24 
 |archiveurl=https://web.archive.org/web/20060624152555/http://www.aviation.ru/An/ 
 |archivedate=24 June 2006 
 |deadurl=yes 
 |df= 
}}</ref>
 |retired=1972
 |status=
 |primary user=[[Aeroflot]]
 |more users=
 |produced=
 |number built=104<ref name="Gordon An-12"/>
 |unit cost=
 |variants with their own articles= 
 |developed into = [[Antonov An-12]]
}}
|}

The '''Antonov An-10''' ([[NATO reporting name]]: '''Cat''') was a four-engined [[turboprop]] passenger transport aircraft designed in the [[Soviet Union]].<ref name="Gordon An-12"/>

==Design and development==
[[File:1958 CPA 2194.jpg|thumb|An-10 on a 1958 Soviet postage stamp]]
Development of a four-engined airliner intended for use on routes from 500 to 2000 kilometers (310 to 1,262 miles) began at the end of 1955. Inspired by the '''Izdeliye N''' (''Izdeliye'' – article or product) passenger version of the [[Antonov An-8]], the Antonov design bureau developed the '''Izdeliye U''' ("U" for "Universal"), a four-engined aircraft with a similar layout to the An-8, but with increased dimensions and a circular-section pressurised fuselage.<ref name="Gordon An-12"/> Early in the design process the choice of engines was between the [[Kuznetsov NK-4]] and he [[Ivchenko AI-20]], and despite superior performance the Kuznetsov NK-4 was eliminated and the Ivchenko AI-20 selected, partly due to the Central Committee of the Communist Party of Ukraine which wanted as much as possible produced in Ukraine, where the Ivchenko factory was.<ref name="Gordon An-12"/>

The first prototype flew on 7 March 1957, revealing poor directional stability which led to a taller vertical fin, and later to hexagonal auxiliary fins at the tips of the tailplane. Entering production at Zavod (factory) No.64, [[Voronezh]] in 1957, the initial three aircraft were delivered with Kuznetsov NK-4 engines, due to non-availability of the Ivchenko AI-20 engines. From 1958, production aircraft were delivered with the Ivchenko AI-20A engines which boasted a longer service life and comparable performance compared to the Kuznetsov engines.<ref name="Gordon An-12"/>
The new aircraft was displayed to the public for the first time in July 1957; the design was approved for mass production after testing was completed in June 1959. [[Aeroflot]] began operations with the An-10 from 22 July 1959 on the Moscow – [[Simferopol International Airport|Simferopol]] route.

Configured with 85 seats, the cabin was spacious and well-appointed with comfortable seats widely spaced, giving plenty of legroom, but due to the low cabin floor and wide diameter, there was much unusable space which limited baggage and cargo volume. The inefficient use of cabin volume contributed greatly to the low payload/TOW ratio which was much lower than that of the contemporary [[Ilyushin Il-18]], but which was still higher than the [[Tupolev Tu-104]]. A later production version, the '''An-10A''', addressed some of the efficiency concerns by increasing the number of seats from 85 to 89 and 100 (in the two versions of the An-10A), then to 117–118 and finally 132 through reducing seat pitch and changing the cabin layout.<ref name="Gordon An-12"/> Powered by Ivchenko AI-20K engines the An-10A demonstrated superior performance and an increased maximum payload of 14.5 Tonnes (31,970&nbsp;lb). The auxiliary endplate fins eventually gave way to improved splayed ventral fins under the rear fuselage. The directional stability was now acceptable and the new ventral fins also improved longitudinal stability at high g and on landing approach, as well as delaying the onset of Mach buffet to M0.702. Due to being sited in an area of flow separation, the new ventral fins also caused unpleasant vibrations. Following results of flight tests and at least two fatal crashes, an effective tailplane deicing system was retrofitted to all remaining aircraft.<ref name="Gordon An-12"/>

==Operational history==
A total of 104 aircraft were built, including the prototype and static test airframes, entering service with the [[Ukrainian Civil Aviation Directorate]] of [[Aeroflot]] from 27 April 1959, proving popular due to large cargo volume (when fitted with reduced seating) and excellent field performance, making the aircraft suitable for use on small undeveloped airfields. The Antonov Bureau simultaneously developed and produced the [[Antonov An-8]] medium military transport, the An-10 civil airliner and military paratroop transport, as well as the [[Antonov An-12]] military cargo transport.<ref name="Gordon An-12"/>

On 22 April 1962 an An-10A piloted by A. Mitronin achieved a world record 500&nbsp;km closed loop speed record averaging {{convert|730.6|km/h|mph}}.<ref name="Gordon An-12"/>

The performance of the An-10 and An-10A was relatively good and was shown to have lower seat/mile cost than the contemporary Tupolev Tu-104 jet airliner, but continued to exhibit control and stability problems, culminating in a fatal crash on 16 November 1959 when CCCP-11167 dived into the ground while on approach to [[Lviv|L'vov]] airport, due to reduced stability, abrupt nose drops at low airspeed and reduced elevator authority at low airspeed. On 26 February 1960, again at L'vov, CCCP-11180 crashed at the same location due to reduced longitudinal stability and control authority caused by icing of the tailplane.<ref name="Gordon An-12"/>

Military use of the An-10 was fairly extensive with 45 '''An-10TS''' built for the VTA, 16 flown exclusively by military units and the remaining 38 loaned to the Ministry of Civil Aviation, as well as the flyable aircraft remaining after withdrawal from Aeroflot service.<ref name="Gordon An-12"/>

On 18 May 1972, while descending to [[Kharkiv International Airport]] an An-10 [[Aeroflot Flight 1491|crashed]], killing eight crew and 113 passengers. An investigation revealed [[metal fatigue|fatigue cracking]] of the wing centre section stringers on many of the remaining aircraft. Following this accident, Aeroflot ceased operating the An-10.<ref>{{cite book |title=Aviation Disasters Second Edition |last1=Gero |first1=David |publisher=Patrick Stephens Limited |year= 1996 |page=105 }}</ref>

After withdrawal from Aeroflot service on 27 August 1972, 25 An-10A aircraft which were in good condition were transferred to the VVS ([[Soviet Air Force]]) and other MAP (Ministry of Aircraft Production) units.<ref name="Gordon An-12"/> These remaining An-10As were retired by 1974.

A few examples have been preserved as exhibits in museums, and several have been converted into children's theatres (at Kiev, Samara and Novocherkassk).

==Operators==
; {{USSR}}
* [[Aeroflot]]
* [[Soviet Air Force]]

==Variants==
* '''Izdeliye U''' – The in-house designation of the four-engined passenger aircraft derived from the '''Izdeliye N''' An-8 project.<ref name="Gordon An-12"/>
* '''An-10''' – The designation of the prototype and initial production versions fitted with Kuznetsov NK-4 or Ivchenko Ai-20A engines.<ref name="Gordon An-12"/>
* '''An-10A''' – Production aircraft from December 1959 with increased seating, decreased empty weight/increased payload and Ivchenko AI-20K engines.<ref name="Gordon An-12"/>
* '''An-10AS''' – several aircraft modified for small package cargo transport with no seats.<ref name="Gordon An-12"/>
* '''An-10TS''' – (''Transport/Sanitarny'' – transport/ambulance) 45 Aircraft ordered for the VTA (''Voyenno-Transportnaya Aviatsiya'' – transport air arm), with 38 loaned to the Ministry of Civil Aviation.<ref name="Gordon An-12"/>
* '''An-10KP''' – (''Komandny Punkt'' – command post) A single aircraft (CCCP-11854) modified as an airborne command post for use at [[Sperenburg]] air base, near Berlin in the [[East Germany|DDR]].<ref name="Gordon An-12"/>

==Accidents and incidents==
Over its life, the An-10 experienced 12 accidents,<ref>[http://aviation-safety.net/database/dblist.php?field=typecode&var=041%&cat=%1&sorteer=datekey&page=1 Aviation Safety Network Database]</ref> with 370 fatalities. The An-10 carried more than 35 million passengers and 1.2 million tons of cargo, and its competitor, the Il-18 suffered from 51 fatal accidents with 1359 fatalities over the same period.

{| class="wikitable"
|-
!Date ||Tail number ||Location (all in the Soviet Union) ||Casualties
!Brief description
|-
|align="right"|16 Nov 1959 || CCCP-11167 || [[Lviv]] || 40/40
| fell into dive on landing due to icing on wings
|-
|align="right"|26 Feb 1960 || CCCP-11180 || [[Lviv]] || 32/33
| fell into dive on landing due to icing on wings
|-
|align="right"|27 January 1962|| CCCP-11148 || [[Ulyanovsk]] || 13/14
| crashed on takeoff due to reversing propeller 
|-
|align="right"|28 July 1962 || CCCP-11186 || [[Sochi]] || 81/81
| collision into mountain due to controller error
|-
|align="right"|2 August 1963|| CCCP-11193 || [[Syktyvkar]] || 7/7
| ingress of ice into engines during training flight
|-
|align="right"|8 August 1968 || CCCP-11172 || [[Mirny Airport|Mirny]] || 0/no data
| ran off runway, struck a vehicle
|-
|align="right"|12 October 1969 || CCCP-11169 || Mirny || 0/no data
| landed short of runway
|-
|align="right"|15 May 1970 || CCCP-11149 || [[Kishinev]] || 11/11
| loss of control during training flight during simulated missed approach on two engines
|-
|align="right"|8 August 1970 || CCCP-11188 || [[Kishinev]] || 1/114 
| emergency landing in field due to in-flight fire 
|-
|align="right"|31 March 1971 || CCCP-11145 || [[Voroshilovgrad]] || 65/65
| crashed on approach due to wing separation
|-
|align="right"|12 October 1971 || CCCP-11137 || [[Kishinev]] || no data
|rough landing
|-
|align="right"|[[Aeroflot Flight 1491|18 May 1972]] || CCCP-11215 || [[Kharkov]] || 122/122
|both wings fell off during landing, metal fatigue
|}

==Specifications (An-10A)==
[[File:Antonov An-10 Cat two-view silhouette.png|right|thumb]]
{{aerospecs
|ref=Soviet Transport Aircraft since 1945<ref name="Stroud p65">Stroud 1968, p.63.</ref>
|met or eng?=<!-- eng for US/UK aircraft, met for all others -->met
|crew=five
|capacity=100 passengers
|length m=34
|length ft=111
|length in=6.5
|span m=38
|span ft=124
|span in=8
|height m=9.8
|height ft=32
|height in=1.75
|wing area sqm=121.73
|wing area sqft=1310.3
|empty weight kg=29,800<ref name="Gunston Russian p24">Gunston 1995, p. 24.</ref>
|empty weight lb=65,697
|gross weight kg=55,100<ref name="Gunston Russian p24"/>
|gross weight lb=121,473
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|eng1 number=4
|eng1 type=[[Ivchenko AI-20]] turboprop
|eng1 kw=<!-- prop engines -->2,984
|eng1 hp=<!-- prop engines -->4,000
|max speed kmh=715
|max speed mph=444
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=600–660<!-- if max speed unknown -->
|cruise speed mph=373–410<!-- if max speed unknown -->
|range km=4075
|range miles=2532
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=11,000
|ceiling ft=36,100
|climb rate ms=
|climb rate ftmin=
}}

==See also==
{{aircontent
|related= 
* [[Antonov An-12]]
|similar aircraft=
* [[Bristol Britannia]]
* [[Ilyushin Il-18]]
* [[Lockheed L-100 Hercules]]
* [[Vickers Vanguard]]
|lists=
* [[List of airliners]]
|see also=
}}

==References==
{{reflist}}

==Other sources==
* Gordon,Yefim and Dmitry Komissarov, . ''Antonov An-12''. Midland. Hinkley. 2007. ISBN 1-85780-255-1 ISBN 978-1-85780-255-9.
* [[Bill Gunston|Gunston, Bill]]. ''The Osprey Encyclopedia of Russian Aircraft 1875–1995''. London:Osprey, 1995. ISBN 1-85532-405-9.
* Stroud, John. ''Soviet Transport Aircraft since 1945''. London:Putnam, 1968. ISBN 0-370-00126-5.

==External links==
{{Commons category|Antonov An-10}}
*[https://web.archive.org/web/20040903112740/http://www.airdisaster.com:80/cgi_bin/aircraft_detail.cgi?aircraft=Antonov+AN-10 Airdisaster.com]

{{Antonov aircraft}}

{{DEFAULTSORT:Antonov An-010}}
[[Category:Antonov aircraft|An-010]]
[[Category:Soviet airliners 1950–1959]]
[[Category:Four-engined tractor aircraft]]
[[Category:High-wing aircraft]]
[[Category:Turboprop aircraft]]