{{Use dmy dates|date=July 2014}}
{{good article}}
{{Infobox Airliner accident| 
name=2007 Balad aircraft crash| 
Date= {{start-date|9 January 2007}}|
Type=Cause in dispute|
Site=[[Balad, Iraq|Balad]], Iraq|
Fatalities=34<ref name="RIANovosti2">{{cite news |title= Moldovan plane that crashed in Iraq was downed – eyewitness |url= http://en.rian.ru/world/20070112/58977626.htm |publisher= Russian News and Information Agency Novosti |accessdate=22 January 2007 }}</ref> |
Injuries=1|
Aircraft Type=[[Antonov An-26]]|
Operator=[[AerianTur-M]]|
Tail Number=ER-26068<ref name="arms smuggling">[http://www.tiraspoltimes.com/node/545 Moldova plane crash in Iraq tied to insurgency arms smuggling] {{webarchive |url=https://web.archive.org/web/20070212182846/http://www.tiraspoltimes.com/node/545 |date=12 February 2007 }} – [[Tiraspol Times]] & Weekly Review. Retrieved 26 February 2007.</ref>|
Passengers=30<ref name="RIANovosti2"/>|
Crew=5<ref name="RIANovosti2"/>|
Survivors=1<ref name="RIANovosti2"/>|}}

The '''2007 Balad aircraft crash''' was a 9 January 2007 airplane incident involving an [[Antonov]] [[Antonov An-26|An-26]] airliner, which crashed while attempting to land at the [[Joint Base Balad]] in [[Balad, Iraq|Balad]], Iraq, which was at that time operated by the [[United States Air Force]].<ref name="US base and Kulak">[http://www.cbsnews.com/stories/2007/01/09/ap/world/mainD8MHSIGG8.shtml 32 Killed in Cargo Plane Crash in Iraq] – CBS News. Retrieved 28 January 2007.</ref> The crash killed 34 people aboard and left one passenger critically injured. Officials claim the crash was caused by poor weather conditions, but other sources claim that this is a cover-up and the plane was actually shot down by a [[Surface-to-air missile|missile]].<ref name="RIANovosti2"/><ref name="arms smuggling"/><ref name="Turkish Daily News"/>

==Aircraft==
The aircraft was an [[Antonov]] [[Antonov An-26|An-26B-100]], registration number ER-26068.<ref name="ASN">[http://aviation-safety.net/database/record.php?id=20070109-0 ASN Aircraft accident descriptionAntonov 26B-100 ER-26068] – [[Aviation Safety Network]]. Retrieved 26 February 2007.</ref> It made its first flight in 1981, and was powered by two [[Ivchenko]] AI-24VT engines.<ref name="ASN"/> An-26s are a twin-engined light [[turboprop]] [[Cargo aircraft|transport aircraft]] derived from the [[Antonov An-24]], with particular attention made to potential military use. It has a modified rear fuselage with a large cargo ramp.

==Background==
The aircraft, which took off from [[Adana]], Turkey,<ref name="latimes">{{cite news|title=Plane crash in Balad, Iraq, kills at least 31, mostly Turks|url=http://articles.latimes.com/2007/jan/10/world/fg-crash10|work=Los Angeles Times  |publisher= Times Wire Services|date= 10 January 2007|accessdate=27 May 2009}}</ref>
at about 0400 [[UTC]],<ref name="takeoff time">[http://www.cnn.com/2007/WORLD/meast/01/09/iraq.crash/index.html Turkey: Iraq plane crash kills 30] – [[CNN]]. Retrieved 26 February 2007.</ref>
was owned by the Moldovan company [[AerianTur-M]], and on the day of the accident had been chartered to a Turkish construction company, Kulak,<ref name="US base and Kulak"/> who had been contracted to build a new hangar at the air base.<ref name="new hangar">[http://www.todayszaman.com/tz-web/detaylar.do?load=detay&link=39828 Delayed passport saves construction worker's life] – Today's Zaman. Retrieved 27 February 2007.</ref> The aircraft hired by BSA Aviation Ltd (charterer) was carrying both cargo and passengers; a total of 1,289&nbsp;kg (2,842&nbsp;lb) of cargo was on board, compared with the 5,000&nbsp;kg (11,000&nbsp;lb) capacity.<ref name="cargo">[http://www.turkeydailynews.com/news/125/ARTICLE/2082/2007-01-10.html Weather blamed for plane crash near Baghdad] – Turkey Daily News. Retrieved 26 February 2007.</ref> Turkish authorities told [[CNN Türk]] television that of the passengers, there were 29 Turkish workers, three Moldovans, a Russian, a [[Ukraine|Ukrainian]], and an American on board, even though this totals one more than the number of people known to be on board.<ref name="RIANovosti1">{{cite news |title= Moldovan plane crashes in Iraq killing 30 Turkish workers |url= http://en.rian.ru/world/20070109/58745901.html |publisher= Russian News and Information Agency Novosti |accessdate=22 January 2007 | archiveurl= https://web.archive.org/web/20070112032313/http://en.rian.ru/world/20070109/58745901.html| archivedate= 12 January 2007 | deadurl= no}}</ref> Later, the Russian consul general in Antalya said the Russian and the Ukrainian also had Moldovan citizenship.<ref name="RIANovosti2"/> Most of those on board were construction workers who worked at the base. Brig. Gen. Robin Rand, commander of the [[332nd Air Expeditionary Wing]], said "These brave civilian-contract employees were in Iraq helping us accomplish our mission, and their loss is a tragedy," adding "Our condolences go out to the families in their time of loss."<ref name="newsblaze">[http://newsblaze.com/story/20070116124526tsop.nb/topstory.html U.S. Military Responds to Civilian Aircraft Crash] – NewsBlaze. Retrieved 27 May 2009.</ref>

==Crash==
The plane crashed at 0700 UTC,<ref>[https://web.archive.org/web/20071023101310/http://www.rfi.fr/actufr/afp/001/mon/070109191419.bgu3pm39.asp Un avion transportant des ouvriers turcs s'écrase en Irak: 34 tués] – [[Radio France Internationale]] – 9 January 2007. Retrieved 3 July 2007. ''In French''</ref> about {{convert|2.5|km|mi|abbr=on}} away from [[Balad Air Base]], the main hub of US military logistics in Iraq, while attempting to land.<ref name="time and location">[http://www.alertnet.org/thenews/newsdesk/L0988354.htm Thirty-one die in Iraq plane crash-Turkish official] – [[Reuters]] AlertNet. Retrieved 26 February 2007.</ref> An anonymous ministry official told the [[Associated Press]] that the pilot had already aborted one landing attempt due to poor weather conditions.<ref name="Aero-News">[http://www.aero-news.net/index.cfm?ContentBlockID=524ea8b9-5b15-4a51-a97d-532772a3b9b8 Cargo Plane Down In Northern Iraq] – [[Aero-News]]. Retrieved 28 January 2007.</ref> Although the aircraft was said to have crashed due to [[fog]], one eyewitness, a relative of one of the deceased, said that he watched a missile strike the right hand side of the fuselage while standing just 300–400 meters (1000-1300&nbsp;ft) from where the aircraft went down.<ref name="RIANovosti2"/> The man also said that multiple other eyewitnesses also saw the aircraft get shot down. İsmail Kulak, a partner in the ownership of the Kulak Construction Company, was among the dead.<ref name="İsmail Kulak">[https://web.archive.org/web/20070125005717/http://english.sabah.com.tr/716B421A793646BA97D9A62798C2BD87.html Cargo plane carrying Turkish workers crashes in Baghdad] [https://web.archive.org/web/20070125005717/http://english.sabah.com.tr/716B421A793646BA97D9A62798C2BD87.html The plane carrying workers crashed 200 meters to runway] – SABAH Newspaper (English version). Retrieved 27 May 2009.</ref>

==Emergency response==
Because the aircraft crashed in a military base, the emergency response was supplied by the [[U.S. Army]] and the [[U.S. Air Force]]. Ground ambulance response was by the 206th Area Support Medical Company, which is an US Army National Guard from Missouri. Eight ambulances responded with support from the base QRF. Helicopters from the Air Force's 64th Expeditionary Rescue Squadron transported the dead from the scene.<ref name="newsblaze"/> Of the 35 passengers and crew members on board the flight, two individuals were pulled alive from the wreckage. One died after being transported by an Army ground ambulance to the Air Force Theater Hospital.<ref name="United States Air Force Air Combat Command website">[http://www.acc.af.mil/news/story.asp?id=123037826 Nellis Airmen respond to civilian aircraft crash] – United States Air Force. Retrieved 18 January 2007.</ref> The other survivor, a Turk named Abdülkadir Akyüz,<ref name=" Abdülkadir Akyüz">[https://web.archive.org/web/20070927203530/http://www.turkishdailynews.com.tr/article.php?enewsid=64418 Lone survivor of Baghdad crash to return home] – Turkish Daily News. Retrieved 27 May 2009.</ref> was carried by an Army ground ambulance to the Air Force Theater Hospital, where he received life-saving emergency surgery.<ref name="newsblaze"/>

==Reaction from the Islamic Army in Iraq==
{{see also|Islamic Army in Iraq}}
The day after the accident, the [[Iraqi insurgency (2003–11)|insurgent group]] [[Islamic Army in Iraq]], using a web site known by authorities to be used by the group, claimed that they shot the plane down. The statement said that their members had "opened fire on a plane trying to land at an American base near Balad from different directions, using medium-range weapons... With the help of God, they were able to shoot it down."<ref name="Turkish Daily News"/>

==Investigation==
[[File:Balad crash recorders.jpg|thumb|right|250px|The recovered flight recorders from the crashed aircraft]]
After the wreckage was photographed ''[[in situ]]'', the army hauled it away on [[flatbed truck]]s to the base, where it is presently secured.<ref name="newsblaze"/> As well as the ongoing question of fog, [[Ahmed al-Mussawi]], spokesman for the Iraqi transport ministry, said one day after the crash that "It must have been technical failure or a lack of aviation experience (on the part of the crew),".<ref name="Ahmed al-Mussawi">[http://www.iraqupdates.com/p_articles.php/article/13368 Iraq blames technical error for plane crash] – Iraq Updates. Retrieved 27 February 2007.</ref> The crash is under investigation by the [[Government of Iraq|Iraqi government]], [[Government of America|American government]] and [[Government of Moldova|Moldovan government]], but the [[Government of Turkey|Turkish government]] has been denied permission to join the investigative team.<ref name="Zaman">[http://www.todayszaman.com/tz-web/detaylar.do?load=detay&link=101194 The puzzle of the Moldovan plane crash continues] – Today's Zaman. Retrieved 25 February 2007.</ref> The Air Force and the Army say they are willing to help with the investigation.<ref name="Turkish Daily News">[https://web.archive.org/web/20070930171005/http://www.turkishdailynews.com.tr/article.php?enewsid=63845 Islamic Army in Iraq claims responsibility for downing] – Turkish Daily News. Retrieved 28 January 2007.</ref><ref name="newsblaze"/><ref name="CCT">[http://www.contracostatimes.com/mld/cctimes/news/16425303.htm Cargo plane from Turkey crashes in Iraq, killing 34] – ''Contra Costa Times''. Retrieved 27 May 2009.</ref> Ali Ariduru, deputy head of the Turkish aviation authority, said initial information indicated there was no technical malfunction on the plane.<ref name="CCT"/>

There is confusion as to the whereabouts of the aircraft's [[Flight Data Recorder]] and [[Cockpit Voice Recorder]] (FDR and CVR, commonly referred to as "black boxes"). The [[Turkish Foreign Ministry]] stated they have been shipped to Antonov's [[Kiev]] headquarters, but [[Minister of Transportation for Turkey|Turkish Minister of Transportation]] [[Binali Yıldırım]] claims they are still in Iraq, with the rest of the debris.<ref name="Zaman"/> All that is confirmed is that they have been recovered, which occurred on 30 January.<ref name="black box recovery date">{{cite web
|url=http://www.izvestia.ru/news/news125175/
|title=Flight data recorders of Moldovan An-27 are found in Iraq
|work=[[Izvestia]]
|date=30 January 2007
|accessdate=9 March 2007|language=ru}} [https://translate.google.com/translate?hl=en&sl=ru&u=http://www.izvestia.ru/news/news125175/&sa=X&oi=translate&resnum=10&ct=result&prev=/search%3Fq%3DER-26068%26hl%3Den%26safe%3Doff%26sa%3DG View translated version.]</ref> <!--  Commented out because image was deleted: [[Image:Werwer_2695765.jpg|thumb|Caption|Antonov An-26B a few minutes before take off at Adana Airport]] -->

==See also==
*[[List of accidents and incidents involving commercial aircraft]]

==References==
{{Reflist|2}}

==External links==
*[http://www.airliners.net/search/photo.search?regsearch=ER-26068 Pre-accident pictures of the crashed plane]
*[http://www.slugsite.com/archives/772 Post-accident pictures of the crashed plane]

{{Aviation accidents and incidents in 2007}}

{{coord missing|Iraq}}

{{DEFAULTSORT:Balad Aircraft Crash 2007}}
[[Category:2007 in Iraq]]
[[Category:Aviation accidents and incidents in Iraq]]
[[Category:Airliner accidents and incidents involving fog]]
[[Category:Aviation accidents and incidents in 2007]]
[[Category:Accidents and incidents involving the Antonov An-26]]
[[Category:January 2007 events]]