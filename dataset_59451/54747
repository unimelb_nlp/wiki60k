{{infobox book | 
| name          = The Battle of the Strong: A Romance of Two Kingdoms
| title_orig    = 
| translator    = 
| image         = The Battle of the Strong.jpg
| caption = 
| author        = [[Sir Gilbert Parker, 1st Baronet|Gilbert Parker]]
| illustrator   = 
| cover_artist  = 
| country       = 
| language      = English
| series        = 
| genre         = [[Novel]]
| publisher     = 
| pub_date      = 1898
| english_pub_date =
| media_type    = Print ([[Hardcover]])
| pages         = 466 pp
| isbn          = 
| preceded_by   = 
| followed_by   = 
}}

'''''The Battle of the Strong''''' is an 1898 novel by [[Sir Gilbert Parker, 1st Baronet|Gilbert Parker]]. It was first published in serial format in ''[[The Atlantic|The Atlantic Monthly]]'' starting in January 1898,<ref name="1898-coming">[http://digital.library.cornell.edu/cgi/t/text/pageviewer-idx?c=atla;cc=atla;rgn=full%20text;idno=atla0081-1;didno=atla0081-1;view=image;seq=00035;node=atla0081-1%3A1 The Battle of the Strong (first part)], ''[[The Atlantic|The Atlantic Monthly]]'' (Vol. 81, Issue 483, pp. 29-41) (January 1898)</ref> and as a single volume late in the same year. It was ranked as the tenth-highest best selling book overall in the United States for 1898,<ref name="year">Hackett, Alice Payne. [https://books.google.com/books?id=3QpFAAAAMAAJ&q=%22battle+of+the+strong%22 Fifty years of best sellers, 1895-1945], p. 14 (1945)</ref> and appeared as high as Number 2 on the monthly bestseller list published in ''[[The Bookman (New York)|The Bookman]]'' in early 1899.<ref name="bets">[https://books.google.com/books?id=4k0DAAAAYAAJ&pg=PA600#v=onepage&q&f=false The Best Selling Books], ''[[The Bookman (New York)]]'' (February 1899), p.600 (listing the book at Number 2, after ''[[The Day's Work]]'' by [[Rudyard Kipling]])</ref>  The book is set in the [[Channel Islands]], primarily during the period 1781-95, and opens with attempted invasion of [[Jersey]] by France in the [[Battle of Jersey]].<ref name="chan1">(3 December 1898). [https://news.google.com/newspapers?id=8xlMAAAAIBAJ&sjid=Uy4DAAAAIBAJ&pg=5209,4964411& "The Battle of the Strong": Latest and Most Brilliant Efforts by the Canadian Novelist], ''Quebec Saturday Budget''</ref><ref name="nyt2">(5 November 1898). [https://query.nytimes.com/mem/archive-free/pdf?res=F10A12F93F5C11738DDDAC0894D9415B8885F0D3 Gilbert Parker's "The Battle of the Strong" (book review)], ''[[The New York Times]]''</ref><ref name="sum">Nield, Jonathan. [https://books.google.com/books?id=904G29jMdzIC&pg=PA180&lpg=PA180#v=onepage&q&f=false A Guide to the Best Historical Novels and Tales], p. 180 (1929)</ref>

The title is derived from [[Ecclesiastes]] 9:11, "the race is not to the swift, nor the battle to the strong."<ref name="title">[https://books.google.com/books?id=R9MmAQAAIAAJ&pg=PA168#v=onepage&q&f=false Springfield City Library Bulletin, September 1916], p. 168 (1916) (also, Parker includes the quote at the outset of the book, making the derivation clear)</ref>

[[Willis Steell]] and [[Edward Everett Rose]] adapted the novel into a play in 1900, which starred [[Maurice Barrymore]] and [[Marie Burroughs]].<ref name="dramat">(28 November 1900). [https://query.nytimes.com/mem/archive-free/pdf?res=F20C16F8385A16738DDDA10A94D9415B808CF1D3 Various Dramatic Topics], ''[[The New York Times]]''</ref>  Of the play, Parker later remarked that "the adaption, however, was lacking much, and though Miss Marie Burroughs and Maurice Barrymore played in it, success did not attend its dramatic life."<ref name="intro">[https://books.google.com/books?id=1ug8AQAAIAAJ&pg=PR9&lpg=PR9#v=onepage&q&f=false The Works of Gilbert Parker: The battle of the strong], p. ix (introduction to 1913 reprint)</ref>

==References==
{{reflist|2}}

==External links==
* [http://www.gutenberg.org/ebooks/6236 The Battle of the Strong] full text at [[Project Gutenberg]]
* [https://books.google.com/books?id=S_7uYh8IGWoC&printsec=frontcover&dq=#v=onepage&q&f=false The Battle of the Strong] scan of 1898 novel at Google Books

{{DEFAULTSORT:Battle of the Strong, The}}
[[Category:1898 novels]]
[[Category:Novels set in the Channel Islands]]
[[Category:Works originally published in The Atlantic (magazine)]]


{{1890s-novel-stub}}