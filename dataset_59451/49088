{{Use dmy dates|date=March 2012}}
{{Use British English|date=March 2012}}
{{Infobox military person
|name=Sir Charles Knowles, Bt
|birth_date= 14th March 1832
|death_date= 3rd March 1917
|image= Vice Admiral Sir Charles G F Knowles Bt-1.jpg
|caption= Vice-Admiral Sir Charles G F Knowles
|birth_place= Vaynor Park, Berriew
|death_place =  [[Oxford]]
|nickname=
|residence = [[England]]
|nationality = [[Kingdom of Great Britain|British]]
|allegiance={{flag|Kingdom of Great Britain}}
|serviceyears=1845-1887
|rank=[[Vice-Admiral (United Kingdom)]]
|branch= {{navy|Kingdom of Great Britain}}
|commands={{HMS|Investigator|1864|6}}<br>{{HMS|Lapwing|1872|6}}<br>{{HMS|Blanche|1877|6}}<br>{{HMS|Shannon|1885|6}}
|unit=

|awards=
|relations=[[Sir Francis Howe Seymour Knowles, 5th Baronet|Francis Knowles]] (son)
}}

[[File:Charles G F Knowles as Midshipman on HMS Queen.jpg|thumb|Charles G F Knowles as Midshipman on HMS Queen]]

[[Vice-Admiral]] '''Sir Charles George Frederick Knowles, 4th Baronet''' (14 March 1832 – 3 March 1917) was an officer of the [[Royal Navy]], who saw service during the Second Burmese War<ref name="Debrett's">{{cite book|title=Debrett's Peerage and Baronetage 1995|publisher=Debrett's Peerage Ltd Macmillan reference books|isbn=0-333-41776-3|pages=B 504 B505}}</ref>  and in command on the Niger expedition<ref name="Debrett's"/>  and quelling uprising at Santa Cruz,<ref name="Debrett's"/>  eventually rising to the rank of [[vice-admiral]].<ref name="Debrett's"/>

==Family and early life==
This family is descended from Charles Knollys, titular 4th Earl of Banbury temp James 11.<ref name="Debrett's"/> Knowles was born on 14 March 1832 at Vaynor Park,  Berriew,  Montgomeryshire, Wales, the son of Sir Francis Charles Knowles 3rd Baronet and his wife Emma Pocock, daughter of Sir [[George Pocock]], 2nd Baronet.<ref name="Debrett's"/> He was the fourth of his line since his great grandfather, [[Sir Charles Knowles, 1st Baronet|Sir Charles Knowles]], admiral, was created a baronet for purely naval services in 1765.<ref name="Debrett's"/>  His grandfather, [[Sir Charles Knowles, 2nd Baronet|Sir Charles Knowles]], followed his own father's career, rising to Admiral, though his son, Knowles' father Sir Francis Charles Knowles, discarded a life in the service to devote himself to the pursuit of science, and succeeded in attaining to the blue ribbon of the scientific world - a fellowship of [[the Royal Society]] in 1830.<ref name="Debrett's"/><ref name="Naval Miscellany" />  On the maternal side also, Knowles had a very distinguished naval pedigree, for his mother was the granddaughter of Admiral Sir [[George Pocock]], the victor of the Havanna, who had been shipmate with the first [[Sir Charles Knowles, 1st Baronet|Sir Charles Knowles]] whilst midshipmen at the defeat of the Spanish fleet at Cape Pissaro 1718, commanded by his kinsman, Admiral [[Lord Torrington]]. It was into a century of general peace that the third [[admiral]] of his line was to pursue a more peaceful naval career than that of his forebears. With a fleet larger than any two rivals combined, there were to be no major battles fought, just localised military action buttressed by the [[Royal Navy]].

==Naval Service==
Knowles joined the navy on 21 May 1845 at the age of fourteen as [[midshipman]] and served as a naval cadet in [[HMS Queen]] from 21 May 1845 - 22 Aug 1846, [[HMS Constance]] from 23rd Aug 1846 - 8 Dec 1849 and the sloop [[HMS Contest]] from 9 Dec 1849 - 21 May 1851 when appointed acting mate whilst in Shanghai, aged twenty, six years since joining, and mate 21 May 1851;<ref name="Naval Miscellany">{{cite book|title=Naval Miscellany Vol 7|publisher=Ashgate Publishing Ltd|isbn=978-0-7546-6431-4}}</ref>
He was promoted an acting lieutenant in a death vacancy in [[HMS Fox]] on 8 Oct 1852 under the command of Commodore (later Ad SIr ) [[George Lambert (Royal Navy officer)|George Lambert]] during the latter part of the [[Second Anglo-Burmese war]] of 5 April 1852 - 20 December 1852;<ref name="Debrett's"/><ref name="Naval Miscellany">{{cite book|title=Naval Miscellany Vol 7|publisher=Ashgate Publishing Ltd|isbn=978-0-7546-6431-4}}</ref> Burmese Medal;<ref name="Naval Miscellany">{{cite book|title=Naval Miscellany Vol 7|publisher=Ashgate Publishing Ltd|isbn=978-0-7546-6431-4}}</ref>

He was appointed Lieutenant 7th Feb 1853; serving in the East Indies, joining [[HMS Wolverene]] at Chatham 17 June 1854-26 Nov 1855; [[HMS Sappho]] on the west coast of Africa 22 Jan 1856-10 March 1856 when discharged from hospital; [[HMS Raven]] as Lt Commander 20 March 1856 – 17 May 1856; [[HMS Prometheus]] on the west coast of Africa 18 May 1856-15 Sept 1857; [[HMS Archer]] at Woolwich 21 May 1858 – 9 June 1859;  He received the thanks of the admiralty for the salvage of [[HMS Ardent]] when stranded on the west coast of Africa in 1858;<ref name="Naval Miscellany">{{cite book|title=Naval Miscellany Vol 7|publisher=Ashgate Publishing Ltd|isbn=978-0-7546-6431-4}}</ref> [[HMS Royal Adelaide]] at Devenport 6 April-25 June 1860, [[HMS Terrible]] in the mediterranean 26 June 1860 – 20 May 1861, [[HMS St Vincent]]  a 120 gun first rate ship of the line used as a training ship at Portsmouth 27 Aug 1861-20 Jan 1864;

[[File:1864 Knowles at Bida Niger-2.jpg|thumb|left|''At Bida on the Niger 1864, Knowles is the officer in the flat cap seated on the left of the chief with an interpreter in a green turban; T.V. Robins artist]] Command of a "gunboat" in the nineteenth century provided valuable experience for the Royal Navy's junior officers and he was put in command of [[HMS Investigator (1861)|HMS ''Investigator'']], a 149-ton wooden paddle survey vessel with two gun armament launched 16 November 1861, in the [[ascent of the Niger]] of August–October 1864;<ref name="Debrett's"/>  to meet with the explorer Dr [[William Balfour Baikie]] and was invalided from Ascension Island 1864;<ref name="Naval Miscellany">{{cite book|title=Naval Miscellany Vol 7|publisher=Ashgate Publishing Ltd|isbn=978-0-7546-6431-4}}</ref> He kept a detailed journal in which he recorded the places visited, a study of the people met and a narrative of the voyage. A resume was published in the Royal Geographical Society Journal in January 1865 and  Part of  "The Journal of Lieutenant Charles Knowles in the river Niger, 1864" published in The Naval Miscellany;<ref name="Naval Miscellany" /> On 1 March 1865 he was appointed [[Commander]], and on 11 Sept 1865 he was appointed Inspecting Commander of the South Yarmouth Division of the [[Her Majesty's Coastguard|Coastguard]] in [[HMS Irresistible (1859)|HMS Irresistible]].

On 29 November 1872 he was appointed Captain and given command of [[HMS Lapwing]], a plover class wooden screw gunvessal on 3 June 1870 engaged in the protection of the Newfoundland fisheries until 14 Nov 1872. He gained the thanks of the admiralty dated 4 April 1872 for service on the coast of Cuba during the Cuban insurrection of 1870-71,<ref name="Debrett's"/><ref name="Naval Miscellany">{{cite book|title=Naval Miscellany Vol 7|publisher=Ashgate Publishing Ltd|isbn=978-0-7546-6431-4}}</ref>  and received the thanks dated May 31, 1873 of the Colonial Service for fishery service off the coast of Newfoundland when in command of the Lapwing.<ref name="Debrett's"/><ref name="Naval Miscellany">{{cite book|title=Naval Miscellany Vol 7|publisher=Ashgate Publishing Ltd|isbn=978-0-7546-6431-4}}</ref>

Knowles joined at Sheerness as Senior Naval Officer of the Barbados Division in the West Indies in command of [[HMS Blanche]], a 1760-ton, 6 gun Eclipse class wooden screw sloop (launched in 1867 and sold off for breaking in 1886), from 4th Sept 1877 - 26 November 1881; He was thanked by the admiralty for services in quelling the uprising in the Danish Island of Santa Cruz in 1880;<ref name="Debrett's"/>  then appointed Captain in command of [[HMS Shannon]], the first British [[armoured cruiser]] and last Royal Navy [[ironclad]] to be built with a retractable propellor to reduce drag when under sail (launched 1875 and scrapped 1899), ship of first reserve, coastguard, Greenock on the north coast of Ireland from 4 August 1885; He finally retired from active service on 14 March 1887, having spent a total of fourteen years and two hundred and twenty days at sea; Rear-Admiral 1st Jan 1889; Vice-Admiral 18th Jan 1894.

==Family and issue==
Knowles married firstly Elizabeth Chapman in 1861 and had two sons and three daughters. He married secondly Mary Ellen Thomson on 11 June 1882, the grand daughter of the Hon [[Joseph Howe]], Lt Gov of Nova Scotia. They had three sons and two daughters. He succeeded to the baronetcy on the death of his father in 1892 and died aged 87 at Oxford on 3 March 1917 and is buried at Ryde on the Isle of Wight. He was succeeded as baronet by his eldest son from the second marriage, [[Sir Francis Knowles, 5th Baronet|Francis Knowles]].<ref name="Debrett's"/>

==Notes==
{{reflist}}

==References==
*The Illustrated London News, Sat February 24, 1866
*Debrett's Peerage and Baronetage 1995
*Naval Miscellany Vol 7

{{S-start}}
{{S-reg|gb-bt}}
{{S-bef| before=[[Sir Francis Charles Knowles, 3rd Baronet|Francis C Knowles]]}}
{{S-ttl| title=[[Knowles Baronets|Baronet]]<br />(of Lovell Hill) | years='''1892-1917'''}}
{{S-aft| after=[[Sir Francis Howe Seymour Knowles, 5th Baronet|Francis HS Knowles]]}}
{{S-end}}

{{DEFAULTSORT:Knowles, Charles, 4th Baronet}}
[[Category:Royal Navy admirals]]
[[Category:Knollys family]]
[[Category:Baronets in the Baronetage of Great Britain]]
[[Category:1765 establishments in Great Britain]]