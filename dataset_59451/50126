{{Infobox rail
|railroad_name = ''{{lang|en|Abkhazian railway}}''<br>საქართველოს რკინიგზის აფხაზეთის მონაკვეთი
|logo_filename = 
|logo_size = 
|system_map = Abkhazia summary map.png
|map_caption = Map of Abkhazia, with its railway lines.
|map_size = 300px
|marks = 
|image = Railway_station_Suhum.jpg
|image_size = 300px
|image_caption = [[Sukhumi railway station]], the biggest in [[Abkhazia]].
|locale = * [[Abkhazia]] (de facto)
* [[Georgia (country)|Georgia]] (de jure)
|start_year = 1992
|end_year = present
|predecessor_line = [[Georgian Railways]] ([[USSR]])
|successor_line = 
|length = 
|gauge = {{RailGauge|1520mm}}
|old_gauge = 
|hq_city = [[Sukhumi]]
|website = [http://azhd.ru/ Press here]
}}
{{Adler-Sukhumi railway line}}

'''Abkhazian railway''' is a rail operator in the [[International recognition of Abkhazia and South Ossetia|partially recognised state]] of [[Abkhazia]], controlled by [[Russian Railways]].

==Main information==
Consists of a {{convert|101|km|abbr=on}} rail line along the [[Black Sea]] coast.<ref>[http://infojd.ru/12/abhjd1.html АБХАЗСКАЯ ЖЕЛЕЗНАЯ ДОРОГА: общие сведения], {{ru icon}}</ref> Built to {{RailGauge|1520mm|allk=on}}, it connected [[Russia]]'s [[North Caucasus Railway]] with [[Georgian Railways]] prior to 1992. This connection was severed as a result of the [[War in Abkhazia (1992–93)|War in Abkhazia]]. The railway is administered by the ''Abkhazskaya Zheleznaya Doroga'' ({{lang-ru|Абхазская Железная Дорога}}, {{lang-ab|Аҧсны Аиҳаамҩа}}) company.

As of 2016, there is a daily long-distance train between [[Moscow]] and [[Sukhumi]], and some suburb trains between [[Adlersky City District|Adler]] and [[Gagra]].

==History==
After the [[dissolution of the Soviet Union]] and damaging of the [[Transcaucasian Railway]] lines, the Samtredskoye part to the west of the [[Inguri River]] came under control of the Abkhazian railway.

The bridge over the [[Inguri River]] was blown up on 14 August 1992, which was the day when [[Georgia (country)|Georgian]] forces entered Abkhazia and is the date considered as the start of the [[War in Abkhazia (1992–93)|War in Abkhazia]]. The pretext for sending the Georgian National Guard to Abkhazia in 1992 was to protect the railroad.<ref name=eurasia>EURASIA INSIGHT, [http://www.eurasianet.org/departments/insight/articles/eav080505.shtml ABKHAZIA AND GEORGIA: READY TO RIDE ON THE PEACE TRAIN?] ({{webarchive |url=https://web.archive.org/web/20080512110134/http://www.eurasianet.org/departments/insight/articles/eav080505.shtml |date=May 12, 2008 }}), 8.5.2005</ref> The bridge was subsequently restored but blown up again in 1993, after the end of the war.

The track between Achigvara and the [[Inguri River]] was dismantled. The rest of the railway line also suffered greatly during the war. After the war ended, traffic was restored along the line. The railway system of Abkhazia was isolated in the 1990s, due to the blockade imposed by Russia.

On 25 December 2002, the [[Sochi]]-[[Sukhumi]] [[elektrichka]] train made its first run since the war, which let to Georgian protests.<ref>[https://archive.is/20071223212230/http://www.chairman.parliament.ge/ge/visits/2003/20_01_ru.htm Официальный визит Председателя Парламента Грузии Нино Бурджанадзе в Российскую Федерацию], {{ru icon}}</ref> As the number of Russian tourists greatly increased in the 2000s, the [[Psou]]-[[Sukhumi]] section was mainly repaired by Russia in 2004 and on 10 September 2004 the Moscow-Sukhumi train first arrived in the capital of Abkhazia.

The Ochamchire-Sukhumi, Sochi-Sukhumi and Tkvarcheli-Sukhumi [[elektrichka]]s, that had operated at various times from 1993, no longer operated by 2007 due to various infrastructure problems. The last of the elektrichka, [[Gudauta]]-Sukhumi, was closed down on the end of 2007.<ref>[http://zkvzhd.abhazia.com/?p=news&id=66 Прекращено движение поезда Сухум - Гудаута] {{webarchive |url=https://web.archive.org/web/20080920232409/http://zkvzhd.abhazia.com/?p=news&id=66 |date=September 20, 2008 }}, 8.1.2008</ref> The Adler-Gagra train service was resumed on 26 June 2010 by the Don-Prigorod company.<ref>[http://www.don-prigorod.ru/news/ Don-Prigorod news] {{webarchive |url=https://web.archive.org/web/20100404014703/http://www.don-prigorod.ru/news/ |date=April 4, 2010 }}, 30.06.2010</ref>

There have been proposals to restore destroyed parts of the railway and re-establish rail traffic between Russia and the Trans-Caucasian countries of Armenia and Georgia. The alternative route through Azerbaijan is significantly longer and not available at all, in the case of Armenia, due to the [[Nagorno-Karabakh War|Nagorno-Karabakh conflict]]. Abkhazia and Russia signed a protocol on repairing the Abkhazian stretch in October 1995,<ref name="jt9664">{{cite news|title=RUSSIAN-ABKHAZIA DEAL DASHES TBILISI'S HOPES.|url=http://www.jamestown.org/single/?tx_ttnews%5Btt_news%5D=9664&tx_ttnews%5BbackPid%5D=209&no_cache=1|accessdate=5 August 2016|work=Monitor|issue=1:119|publisher=[[Jamestown Foundation]]|date=24 October 1995}}</ref> but Georgia has long tied the restoration of rail traffic with the return of refugees to Abkhazia.

On 15 May 2009, the [[President of Abkhazia]], [[Sergei Bagapsh]], announced, that Abkhazia's railway and airport would be transferred to [[Russia]] with management rights for ten years, a decision, which caused a negative outcry in [[Abkhazia]]. According to the Abkhaz tycoon and opposition party leader, [[Beslan Butba]], this has led to growing anti-Russian sentiment in [[Abkhazia]].<ref>{{ru icon}} [http://www.regnum.ru/news/1166546.html В Абхазии наблюдается тенденция роста антироссийских настроений: интервью лидера партии ЭРА Беслана Бутбы]. [[REGNUM News Agency|Regnum]], 22.5.2009</ref>

In late 2012 and early 2013, the new [[Georgia (country)|Georgia]]n government under Prime Minister [[Ivanishvili]] repeatedly proposed to revamp the Abkhazian Railway and getting it hooked on the [[Georgian Railways]], specifically to appease [[Armenia]], and enabling a commercial link to [[Russia]].<ref>{{ru icon}} [http://www.kommersant.ru/doc/2058128/print Это часть нашей стратегии по деизоляции Абхазии] 02.11.2012</ref> This sparked domestic and international discussion in Armenia (the country with the most commercial interest in such a connection),<ref>[http://www.todayszaman.com/columnist-304901-opening-the-abkhaz-railway-who-stands-to-benefit-who-will-lose-out-1.html Opening the Abkhaz railway: Who stands to benefit, who will lose out? (1)] {{webarchive |url=https://web.archive.org/web/20130602074513/http://www.todayszaman.com/columnist-304901-opening-the-abkhaz-railway-who-stands-to-benefit-who-will-lose-out-1.html |date=June 2, 2013 }} 23.01.2013</ref><ref>[http://www.todayszaman.com/columnist-305476-opening-the-abkhaz-railway-who-stands-to-benefit-who-will-lose-out-2.html Opening the Abkhaz railway: Who stands to benefit, who will lose out? (2)] {{webarchive |url=https://web.archive.org/web/20130602074644/http://www.todayszaman.com/columnist-305476-opening-the-abkhaz-railway-who-stands-to-benefit-who-will-lose-out-2.html |date=June 2, 2013 }} 29.01.2013</ref> in Azerbaijan (which has fears it enables Russia with a more efficient military transport to its base in [[Gyumri]], Armenia) and in Russia (Russian Railways owning the Armenian-based South Caucasus Railways).<ref>[http://vestnikkavkaza.net/analysis/politics/34297.html Abkhazia: Once again about the rail road] 29.11.2012</ref> The Abkhaz authorities first reacted dismissively to cooperate with such initiative, but later changed their tone.<ref>[http://www.abkhazworld.com/articles/analysis/939-azerbaijans-contribution-to-the-isolation-of-abkhazia.html  Azerbaijan's contribution to the isolation of Abkhazia] 22.01.2013</ref> Azerbaijan shortly threatened with consequences for the [[Kars–Tbilisi–Baku railway]] connection, due completion at the end of 2013, and suggested raising the gas price charged to Georgia. In Georgia there is still sharp opposition to reopen this railway link.<ref>[http://www.todayszaman.com/news-310915-russian-railway-in-caucasus-a-threat-to-baku-tbilisi-ankara-initiated-projects.html Russian railway in Caucasus a threat to Baku-Tbilisi-Ankara initiated projects] {{webarchive |url=https://web.archive.org/web/20130531031859/http://www.todayszaman.com/news-310915-russian-railway-in-caucasus-a-threat-to-baku-tbilisi-ankara-initiated-projects.html |date=May 31, 2013 }} 27.03.2013</ref><ref>[http://old.cacianalyst.org/?q=node/5915 Georgia's PM: South Caucasus Railway can be reopened] 02.03.2013</ref>

Currently, there is only one train connection from the Russian Federation to Abkhazia. The train from [[Moscow]] to [[Sukhumi]] operates daily at the high touristic season in summer and twice a week at the low season. The additional trains from [[Belgorod]] and [[St. Petersburg]] operate during the touristic season in summer.<ref>https://rasp.yandex.ru/station/9620203?span=schedule</ref>

==Photos==
<gallery>
File:Passenger train in Psyrtskha, Abkhazia.JPG|Passenger train in Psyrtskha.
File:Vokzal-1.jpg|Sukhumi railway station.
File:Gagra train tunnel.jpg|Rail tunnel in [[Gagra]] was part of the project, that enabled a rail link to be established between Georgia and Russia via [[Abkhaz ASSR]] in the 1940s.
</gallery>

==References==
{{reflist}}

==External links==
{{Portal|Railways}}
*[https://web.archive.org/web/20080216134955/http://zkvzhd.abhazia.com:80/ Абхазская Железная Дорога] (''Abkhazian railway'') {{ru icon}}
*[http://infojd.ru/12/abhjd_index.html История абхазской железной дороги] (''History of the Abkhazian railway'') {{ru icon}}

{{Russian Railways}}
{{Rail transport in Europe}}

{{DEFAULTSORT:Abkhazian railway}}
[[Category:Rail transport in Abkhazia]]