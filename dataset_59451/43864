<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name = VE-7
 |image = File:VOUGHT VE-7 USAF.JPG
 |caption = Mechanics work on a VE-7
}}{{Infobox Aircraft Type
 |type = Fighter & Trainer
 |manufacturer = [[Lewis & Vought Corporation]]
 |designer = [[Chance M. Vought]]
 |first flight = 1917
 |introduced = 
 |retired =
 |status =
 |primary user = [[United States Navy]]
 |more users = [[United States Army Air Service]]
 |produced = 1918-1928
 |number built = 128
 |unit cost = 
 |variants with their own articles = 
}}
|}

The '''Vought VE-7 "Bluebird"''' was an early [[biplane]] of the [[United States]]. First flying in 1917, it was designed as a two-seat trainer for the [[United States Army]], then adopted by the [[United States Navy]] as its very first [[fighter aircraft]]. In 1922, a VE-7 became the first plane to take off from an American [[aircraft carrier]].<ref name="fighters">[Lloyd S. Jones, ''U.S. Naval Fighters'' (Fallbrook CA: Aero Publishers, 1977, ISBN 0-8168-9254-7), pp. 11-13]</ref>

==Design and development==
The [[Lewis & Vought Corporation]] was formed just months after the U.S. entered World War I, with the intention of servicing war needs. The company's trainer was patterned after successful European designs; for instance, the engine was a Wright [[Hispano Suiza]] of the type used by the French [[Société Pour L'Aviation et ses Dérivés|Spad]]s. In practice, the VE-7's performance was much better than usual for a trainer, and comparable to the best fighters, and the Army ordered 1,000 of an improved design called the VE-8. However, the contract was cancelled due to the end of the war.<ref name="fighters"/>

However, the Navy was very interested in the VE-7, and received the first machine in May 1920. Production orders soon followed, in fact beyond what the fledgling Vought organization could handle, and the [[Naval Aircraft Factory]] was pressed into service. In all, 128 VE-7s were built.<ref name="fighters"/>

[[File:Vought VE-7 - 1917.jpg|thumb|left|Vought VE-7 - McCook Field, Ohio 1917]]

The fighter version of the VE-7 was designated VE-7S. It was a single-seater, the front cockpit being faired over and a {{convert|.30|in|mm|2|abbr=on}} [[Vickers machine gun|Vickers]] [[machine gun]] mounted over it on the left side and synchronized to fire through the propeller. Some planes, designated VE-7SF, had floatation gear consisting of inflatable bags stowed away, available to help keep the plane afloat when [[ditching]] at sea.<ref name="fighters"/>

The ''Bluebird'' won the 1918 Army competition for advanced training machines.<ref>Janes Fighting Aircraft of World War I by Michael John Haddrick Taylor (Random House Group Ltd. 20 Vauxhall Bridge Road, London SW1V 2SA, 2001, ISBN 1-85170-347-0), page 252.</ref>

The '''VE-8''' variant completed in July 1919 had a [[340hp Wright-Hispano H]] engine, reduced overall dimensions, increased wing area, a shorter faired [[Cabane strut|cabane]], and two Vickers guns. Two were completed. Flight test results were disappointing, the aircraft was overweight, with heavy controls, inadequate stability and sluggish performance.<ref name="complete">The Complete Book of Fighters [http://ec1.images-amazon.com/images/I/21RSY79BDHL.jpg cover] Editors: William Green & Gordon Swanborough (Barnes & Noble Books New York, 1998, ISBN 0-7607-0904-1), pp. 336-337</ref>

The '''VE-9''' variant, first delivered to the Navy on 24 June 1922, was essentially an improved VE-7, with most of the improvements in the fuel system area. Four of the 21 ordered by the U.S. Navy were unarmed observation float seaplanes for battleship catapult use.<ref name="complete"/>

==Operational history==
The VE-7s equipped the Navy's first two fighter squadrons [[VF-1]] and [[VF-6|VF-2]]. A VE-7 flown by Lieutenant Virgil C. Griffin made history on October 17, 1922 when it took off from the deck of the newly commissioned carrier {{USS|Langley|CV-1|2}}. The VE-7s were the Navy's frontline fighters for several years, with three still assigned to the ''Langley'' in 1927; all were retired the following year.<ref name="fighters"/>

==Variants==
*'''VE-7''' (1918) - 14 built for the U.S. Army Air Service; 39 built for the U.S. Navy; (one of two known, built at McCook Field. Reportedly four more were built by Springfield Co)
*'''VE-7F''' (1921) - 29 built for the U.S. Navy
*'''VE-7G''' (1921) - One converted from VE-7 for U.S. Marine Corps, 23 converted from VE-7 for U.S. Navy
*'''VE-7GF''' (1921) - One converted from VE-7
*'''VE-7H''' (1924) - Nine built for the U.S. Navy
*'''VE-7S''' (1925) - One converted from VE-7
*'''VE-7SF''' (1925) - 11 built for the U.S. Navy
*'''VE-7SH''' - One VE-7SF converted into a floatplane.
*'''VE-8''' (1918) - Four ordered by the U.S. Army on October 11, 1918; two were canceled; 340&nbsp;hp Wright-Hispano H engine installed, two Vickers guns, wingspan decreased to {{convert|31|ft|m|abbr=on}}, wing area increased to {{convert|307|sqft|m2|abbr=on}}, shortened to {{convert|21|ft|4|in|m|abbr=on}}, speed increased to {{convert|140|mph|km/h|abbr=on}}, loaded weight increased to {{convert|2435|lb|kg|abbr=on}}<ref name="complete"/>
*'''VE-9''' (1921) - Two converted from VE-7 for U.S. Army; speed increased to {{convert|119|mph|km/h|abbr=on}}, service ceiling increased to {{convert|18840|ft|m|abbr=on}}
*'''VE-9''' (1927) - 22 built for the U.S. Army, 17 built for the U.S. Navy. (U.S. Army used same designation as U.S. Navy)
*'''VE-9H''' (1927) - Four unarmed observation float seaplanes built for the U.S. Navy battleships, modified vertical tail surfaces for improved catapult and water stability<ref name="complete"/>
*'''VE-9W''' - canceled
*A scratch-built replica of the ''Bluebird'' was completed in early 2007 by volunteers of the [http://www.vought.com/heritage/foundation/index.html Vought Aircraft Heritage Foundation].

==Operators==
;{{flag|United States|1912}}
*[[United States Army Air Service]]
*[[United States Navy]]

==Specifications (VE-7)==
{{aircraft specifications
|plane or copter?=plane
|jet or prop?=prop
|ref=Janes Fighting Aircraft of World War I by Michael John Haddrick Taylor (Random House Group Ltd. 20 Vauxhall Bridge Road, London SW1V 2SA, 2001, ISBN 1-85170-347-0), 320 pp.
|crew=two
|capacity=
|payload main=
|payload alt=
|length main=24 ft 5.375 in
|length alt=7.45 m
|span main=34 ft 4 in
|span alt=10.47 m
|height main=8 ft 7.5 in 
|height alt=2.63 m
|area main=284.5 ft²
|area alt=26.43 m²
|airfoil=
|empty weight main=1,392 lb
|empty weight alt=631 kg
|loaded weight main=1,937 lb 
|loaded weight alt=879 kg
|useful load main= 
|useful load alt= 
|max takeoff weight main=
|max takeoff weight alt= 
|engine (prop)=[[Hispano-Suiza 8|Wright-Hispano E-3]]
|type of prop=2-blade, 8 ft 8 in (2.64 m) diameter propeller
|number of props=1
|power main=180 hp
|power alt=134 kW
|power original=
|max speed main=106 mph
|max speed alt=171 km/h
|cruise speed main=
|cruise speed alt=
|stall speed main= 
|stall speed alt= 
|never exceed speed main= 
|never exceed speed alt= 
|range main=290 mi
|range alt=467 km
|ceiling main=15,000 ft
|ceiling alt=4,600 m
|climb rate main=738 ft/min
|climb rate alt=225 m/min
|loading main=
|loading alt=
|power/mass main=
|power/mass alt=
|armament=(VE-7S) 1 [[Vickers machine gun|Vickers]] .30 in (7.62 mm) machine gun synchronize to fire through the propeller
|avionics=
}}

==References==
;Notes
{{Reflist}}
;Bibliography
*[http://www.aerofiles.com/_vot.html K.O. Eckland's Aerofiles]; accessed 13 May 2007

==External links==
{{commons category|Vought VE Bluebird}}
*[http://www.voughtaircraft.com/newsFactGallery/factsheets/company/companyHeritage.htm Vought Aircraft Industries Inc. Company Heritage]

{{Vought aircraft}}
{{USN fighters}}

[[Category:Vought aircraft|V-007]]
[[Category:Military aircraft of World War I]]
[[Category:United States fighter aircraft 1920–1929|VE-7, Vought]]
[[Category:Single-engined tractor aircraft]]
[[Category:Biplanes]]
[[Category:Carrier-based aircraft]]