{{Infobox organization
|name           = Commission on Isotopic Abundances and Atomic Weights
|image          = CIAAW_logo.png
|image_border   =
|size           = 150px
|alt            = CIAAW logo
|caption        =
|map            =
|msize          =
|malt           =
|mcaption       =
|abbreviation   = CIAAW
|motto          =
|formation      = {{start date and age|1899}}
|extinction     =
|type           = International scientific organization
|status         =
|purpose        = To provide internationally recommended values of isotopic composition and atomic weights of elements
|region_served  = Worldwide
|membership     =
|language       = English
|leader_title   = Chairman
|leader_name    = Juris Meija
|leader_title2  = Secretary
|leader_name2   = Thomas Prohaska
|main_organ     =
|parent_organization = [[IUPAC]] (since 1920)
|affiliations   =
|num_staff      =
|num_volunteers =
|budget         =
|website        = http://www.ciaaw.org
|remarks        =
}}

The '''Commission on Isotopic Abundances and Atomic Weights''' ('''CIAAW''') is an international scientific committee of the [[International Union of Pure and Applied Chemistry]] (IUPAC) under its [[IUPAC Inorganic Chemistry Division|Division of Inorganic Chemistry]]. Since 1899, it is entrusted with periodic critical evaluation of [[atomic weight]]s of [[chemical element]]s and other cognate data, such as the [[Isotope|isotopic]] composition of elements.<ref>{{cite web |url=http://www.nobelprize.org/nobel_prizes/chemistry/laureates/1914/present.html|title=Nobel Prize in Chemistry 1914 - Presentation|publisher=Nobelprize.org|date=November 11, 1915}}</ref> The biennial CIAAW Standard Atomic Weights are accepted as the authoritative source in science and appear worldwide on the [[periodic table]] wall charts.<ref>{{cite web |url=http://www.oxfordreference.com/view/10.1093/oi/authority.20110803100007944|title=IUPAC, Oxford Reference}}</ref>

The use of CIAAW Standard Atomic Weights is also required legally, for example, in calculation of calorific value of natural gas (ISO 6976:1995), or in gravimetric preparation of primary reference standards in gas analysis (ISO 6142:2006). In addition, the current definition of [[kelvin]], the SI unit for thermodynamic temperature, makes direct reference to the isotopic composition of oxygen and hydrogen as recommended by CIAAW.<ref>{{cite web|url=http://www.bipm.org/cc/CIPM/Allowed/94/CIPM-Recom2CI-2005-EN.pdf|title=Clarification of the definition of the kelvin, unit of thermodynamic temperature|publisher=BIPM|year=2005}}</ref> The latest CIAAW report was published in February 2016.<ref>{{cite journal |author1=Juris Meija |author2=Tyler B. Coplen |author3=Michael Berglund |author4=Willi A. Brand |author5=Paul De Bièvre |author6=Manfred Gröning |author7=Norman E. Holden |author8=Johanna Irrgeher |author9=Robert D. Loss |author10=Thomas Walczyk |author11=Thomas Prohaska | title = Atomic weights of the elements 2013 (IUPAC Technical Report) | url = http://www.degruyter.com/view/j/pac.2016.88.issue-3/pac-2015-0305/pac-2015-0305.xml | journal = [[Pure and Applied Chemistry|Pure Appl. Chem.]] | year = 2016 | volume = 88 | pages = 265–291 | issue = 3 | doi=10.1515/pac-2015-0305}}</ref>

== Establishment ==
{{float_begin|side=right}}
|- align = "center"
| {{Css Image Crop|Image = U.S._Government_FW_Clarke_Public_Domain.jpg|bSize = 250|cWidth = 120|cHeight = 180|oTop = 60|oLeft = 60}}
| {{Css Image Crop|Image = Karl Seubert.jpg|bSize = 150|cWidth = 120|cHeight = 180|oTop = 10|oLeft = 20}}
| {{Css Image Crop|Image = Thomas Edward Thorpe.jpg|bSize = 200|cWidth = 120|cHeight = 180|oTop = 5|oLeft = 35}}
|- align = "left"
| [[Frank W. Clarke]] (USA)
| [[Karl Seubert]] (Germany)
| [[Thomas Edward Thorpe|Sir Edward Thorpe]] (UK)
{{float_end|caption= The inaugural members of the International Committee on Atomic Weights}}

Although the atomic weight had taken on the concept of a constant of nature like the speed of light, the lack of agreement on accepted values created difficulties in trade. Quantities measured by chemical analysis were not being translated into weights in the same way by all parties and standardization became an urgent matter.<ref>{{cite book|url=https://books.google.com/books?id=hq7bOBG6YSgC&pg=PA40&dq| author=E. Crawford | title=Nationalism and Internationalism in Science, 1880-1939 (p.40)| year= 1992| publisher= Cambridge University Press}}</ref> With so many different values being reported, the [[American Chemical Society]] (ACS), in 1892, appointed a permanent committee to report on a standard table of atomic weights for acceptance by the Society. Clarke, who was then the chief chemist for the U.S. Geological Survey, was appointed a committee of one to provide the report. He presented the first report at the 1893 annual meeting and published it in January 1894.<ref name="ciaaw-history">{{cite web|url=http://www.iupac.org/publications/ci/2004/2601/1_holden.html|title=Atomic Weights and the International Committee—A Historical Review|publisher=Chemistry International|year=2004}}</ref>

In 1897, the German Society of Chemistry, following a proposal by [[Hermann Emil Fischer]], appointed a three-person working committee to report on atomic weights. The committee consisted of Chairman Prof. [[Hans Heinrich Landolt|Hans H. Landolt]] (Berlin University), Prof. [[Wilhelm Ostwald]] (University of Leipzig), and Prof. [[Karl Seubert]] (University of Hanover). This committee published its first report in 1898, in which the committee suggested the desirability of an international committee on atomic weights. On 30 March 1899 Landolt, Ostwald and Seubert issued an invitation to other national scientific organizations to appoint delegates to the International Committee on Atomic Weights. Fifty-eight members were appointed to the Great International Committee on Atomic Weights, including [[Frank Wigglesworth Clarke|Frank W. Clarke]].<ref>L.M. Dennis, Frank Wigglesworth Clarke (National Academy of Sciences 1932) at [http://books.nap.edu/html/biomems/fclark.pdf p.143]</ref> The large committee conducted its business by correspondence to Landolt which created difficulties and delays associated with correspondence among fifty-eight members. As a result, on 15 December 1899, the German committee asked the International members to select a small committee of three to four members.<ref>{{cite journal |author1=H. Landolt |author2=W. Ostwald |author3=K. Seubert | title = Zweiter Bericht der Commission für die Festsetzung der Atomgewichte | url = http://onlinelibrary.wiley.com/doi/10.1002/cber.19000330270/abstract | doi = 10.1002/cber.19000330270 | journal = [[Berichte der deutschen chemischen Gesellschaft|Ber.]] | year = 1900 | volume = 22 | pages = 1847–1883 | issue = 2}}</ref> In 1902, Prof. Frank W. Clarke (USA), Prof. Karl Seubert (Germany), and Prof. [[Thomas Edward Thorpe]] (UK) were elected, and the International Committee on Atomic Weights published its inaugural report in 1903 under the chairmanship of Prof. Clarke.<ref>{{cite journal | author = F.W. Clarke | title = Report of the International Committee on Atomic Weights | url = http://pubs.acs.org/doi/abs/10.1021/ja02003a001 | doi = 10.1021/ja02003a001 | journal = [[Journal of the American Chemical Society|J. Am. Chem. Soc.]] | year = 1903 | volume = 25 | pages = 1–5 | issue = 1}}</ref>

== Function ==
Since 1899, the Commission periodically and critically evaluates the published scientific literature and produces the Table of Standard Atomic Weights. In recent times, the Table of Standard Atomic Weights has been published biennially. Each recommended standard atomic-weight value reflects the best knowledge of evaluated, published data. In the recommendation of standard atomic weights, CIAAW generally does not attempt to estimate the average or composite isotopic composition of the Earth or of any subset of terrestrial materials. Instead, the Commission seeks to find a single value and symmetrical uncertainty that would include almost all substances likely to be encountered.<ref>{{cite journal |author1=Michael E. Wieser |author2=Michael Berglund | title = Atomic weights of the elements 2007 (IUPAC Technical Report) | url = http://media.iupac.org/publications/pac/2009/pdf/8111x2131.pdf | journal = [[Pure and Applied Chemistry|Pure Appl. Chem.]] | year = 2009 | volume = 81 | pages = 2131–2156 | issue = 11 | doi=10.1351/PAC-REP-09-08-03}}</ref>

== Notable decisions ==
Many notable decisions have been made by the Commission over its history. Some of these are highlighted below.

[[File:Inaugural Atomic Weights report 1903.png|left|thumb|380px|alt=Atomic Weights| The inaugural 1903 report of the International Atomic Weights Commission]]

=== International atomic weight unit: H=1 or O=16 ===
Though Dalton proposed setting the atomic weight of hydrogen as unity in 1803, many other proposals were popular throughout the 19th century. By the end of the 19th century, two scales gained popular support: H=1 and O=16. This situation was undesired in science and in October 1899, the inaugural task of the International Commission on Atomic Weights was to decide on the international scale and the oxygen scale became the international standard.<ref>Theodore W. Richards, International Atomic Weights (Proceedings of the American Academy of Arts and Sciences, 1900) [http://www.jstor.org/stable/20020992]</ref> The endorsement of the oxygen scale created significant backlash in the chemistry community, and the inaugural Atomic Weights Report was thus published using both scales. This practice soon ceded and the oxygen scale remained the international standard for decades to come. Nevertheless, when the Commission joined the IUPAC in 1920, it was asked to revert to the H=1 scale, which it rejected.

=== Modern unit: <sup>12</sup>C=12 ===
With the discovery of oxygen isotopes in 1929, a situation arose where chemists based their calculations on the average atomic mass (atomic weight) of oxygen whereas physicists used the mass of the predominant isotope of oxygen, oxygen-16. This discrepancy became undesired and a unification between the chemistry and physics was necessary.<ref>{{cite journal | author = F.W. Aston | title = The Unit of Atomic Weight | url = http://www.nature.com/nature/journal/v128/n3234/abs/128731a0.html | doi = 10.1038/128731a0 | journal = [[Nature (journal)|Nature]] | year = 1931 | volume = 128 | pages = 731 | issue = 3234|bibcode = 1931Natur.128..731. }}</ref> In the 1957 Paris meeting the Commission put forward a proposal for a carbon-12 scale.<ref>{{cite journal | author = Edward Wichers | title = Report on Atomic Weights for 1956-1957 | url = http://pubs.acs.org/doi/abs/10.1021/ja01549a001 | doi = 10.1021/ja01549a001 | journal = [[Journal of the American Chemical Society|J. Am. Chem. Soc.]] | year = 1958 | volume = 80 | pages = 4121–4124 | issue = 16}}</ref> The carbon-12 scale for atomic weights and nuclide masses was approved by IUPAP (1960) and IUPAC (1961) and it is still in use worldwide.<ref>[http://www.britannica.com/EBchecked/topic/41803/atomic-weight Encyclopædia Britannica]</ref>

=== Uncertainty of the atomic weights ===
In the early 20th century, measurements of the atomic weight of lead showed significant variations depending on the origin of the sample. These differences were considered to be an exception attributed to lead isotopes being products of the natural radioactive decay chains of uranium. In 1930s, however, [[Malcolm Dole]] reported that the atomic weight of oxygen in air was slightly different from that in water.<ref>{{cite journal | author = Malcolm Dole | title = The Relative Atomic Weight of Oxygen in Water and in Air | url = http://pubs.acs.org/doi/abs/10.1021/ja01315a511 | doi = 10.1021/ja01315a511 | journal = [[Journal of the American Chemical Society|J. Am. Chem. Soc.]] | year = 1935 | volume = 57 | pages = 2731-2731 | issue = 12}}</ref> Soon thereafter, [[Alfred Nier]] reported natural variation in the isotopic composition of carbon. It was becoming clear that atomic weights are not constants of nature. At the Commission’s meeting in 1951, it was recognized that the isotopic-abundance variation of sulfur had a significant effect on the internationally accepted value of an atomic weight. In order to indicate the span of atomic-weight values that may apply to sulfur from different natural sources, the value ± 0.003 was attached to the atomic weight of sulfur. By 1969, the Commission had assigned uncertainties to all atomic-weight values.

[[File:IUPAC Periodic Table of the Elements 2011.jpg|right|thumb|360px|alt=Atomic Weights|Excerpt of the IUPAC Periodic Table of the Elements 2011 showing the interval notation of the standard atomic weights of boron, carbon, and nitrogen]]

=== Interval notation ===
At its meeting in 2009 in Vienna, the Commission decided to express the standard atomic weight of hydrogen, carbon, oxygen, and other elements in a manner that clearly indicates that the values are not constants of nature.<ref>{{cite web|url=http://www.scientificamerican.com/article.cfm?id=mass-migration-chemists|title=Mass Migration: Chemists Revise Atomic Weights of 10 Elements |journal=Scientific American|date=16 December 2010}}</ref><ref>{{cite web|url=http://www.rsc.org/chemistryworld/News/2010/December/20121001.asp|title=Atomic weights change to reflect natural variations|journal=Chemistry World|publisher=RSC|year=2010}}</ref> For example, writing the standard atomic weight of hydrogen as [1.007 84, 1.008 11] shows that the atomic weight in any normal material will be greater than or equal to 1.007 84 and will be less than or equal to 1.008 11.<ref>{{cite web|url=http://www.iupac.org/publications/ci/2011/3302/2_coplen.html|title=Atomic Weights: No Longer Constants of Nature|author1=Tyler B. Coplen |author2=Norman E. Holden |journal=Chemistry International|publisher=IUPAC|year=2011}}</ref>

== Affiliations and name ==
*[[International Union of Pure and Applied Chemistry]] (IUPAC) from 1920
*International Association of Chemical Societies (IACS) from 1913-1919

The Commission on Isotopic Abundances and Atomic Weights has undergone many name changes:

*The Great International Committee on Atomic Weights (1899-1902)
*International Committee on Atomic Weights (1902-1920)
*IUPAC Commission on Atomic Weights (1920-1922)
*IUPAC Commission on Chemical Elements (1922-1930)
*IUPAC Commission on Atomic Weights (1930-1979)
*IUPAC Commission of Atomic Weights and Isotopic Abundances (1979-2002)
*IUPAC Commission of Isotopic Abundances and Atomic Weights (2002–present)

== Notable members ==
[[File:Richards Theodore William lab.jpg|thumb|220px|The Harvard chemist [[Theodore William Richards|Theodore W. Richards]], a member of the International Atomic Weights Commission, was awarded the 1914 Nobel Prize in Chemistry for his work on atomic weight determination]]
Since its establishment, many notable chemists have been members of the Commission. Notably, eight Nobel laureates have served in the Commission: [[Henri Moissan]] (1903-1907), [[Wilhelm Ostwald]] (1906-1916), [[Francis William Aston]], [[Frederick Soddy]], [[Theodore William Richards]], [[Niels Bohr]], [[Otto Hahn]] and [[Marie Curie]].

Richards was awarded the 1914 Nobel Prize in Chemistry "in recognition of his accurate determinations of the atomic weight of a large number of chemical elements"<ref>{{cite web|url=http://www.nobelprize.org/nobel_prizes/chemistry/laureates/1914/|title=The Nobel Prize in Chemistry 1914}}</ref> while he was a member of the Commission.<ref>{{cite journal | title = Nobel Prize for Richards; Chemistry Award for 1914 Goes to the Harvard Investigator | url = http://select.nytimes.com/gst/abstract.html?res=F3071EF8385B17738DDDAA0994D9415B858DF1D3 | journal = [[New York Times]] | date = 13 Nov 1915}}</ref> Likewise, [[Francis William Aston|Francis Aston]] was a member of the Commission when he was awarded the 1922 Nobel Prize in Chemistry for his work on isotope measurements.<ref>{{cite journal|author=F.W. Aston|title=Report of the International Committee on Chemical Elements: 1923|url=http://pubs.acs.org/doi/abs/10.1021/ja01657a001|doi=10.1021/ja01657a001|journal=[[Journal of the American Chemical Society|J. Am. Chem. Soc.]] | year = 1923 | volume = 45 | pages = 867–874 | issue = 4|display-authors=etal}}</ref> Incidentally, the 1925 Atomic Weights report was signed by three Nobel laureates.<ref>{{cite journal | author = F.W. Aston | title = International Atomic Weights 1925 | url = http://pubs.acs.org/doi/pdf/10.1021/ja01680a001 | doi = 10.1021/ja01680a001 | journal = [[Journal of the American Chemical Society|J. Am. Chem. Soc.]] | year = 1925 | volume = 47 | pages = 597–601 | issue = 3|display-authors=etal}}</ref>

Among other notable scientists who have served on the Commission were [[Georges Urbain]] (discoverer of [[lutetium]]), [[André-Louis Debierne]] (discoverer of [[actinium]]), [[Marguerite Perey]] (discoverer of [[francium]]), [[Georgy Flyorov]] (namesake of the element [[flerovium]]),<ref>{{cite web|url=http://www.ciaaw.org/historical-members.htm|title=Past and Current Membership Summary, CIAAW}}</ref> [[Robert Whytlaw-Gray]] (first isolated [[radon]]), and [[Arne Ölander]] (Secretary and Member of the [[Nobel Committee for Chemistry]]).

=== Chairmen of the Commission ===
Since its establishment, the chairmen of the Commission have been:
{{columns-list|3|
* [[Hans Heinrich Landolt|Hans H. Landolt]] {{flagicon|GER|empire|size=20px}} (1899-1901)
* [[Frank W. Clarke]] {{flagicon|USA|size=20px}} (1902-1921)
* [[Georges Urbain]] {{flagicon|FRA|size=17px}} (1922-1929)
* [[Gregory P. Baxter]] {{flagicon|USA|size=20px}} (1930-1949)
* [[Edward Wichers]] {{flagicon|USA|size=20px}} (1950-1959)
* [[Tomas Batuecas]] {{flagicon|ESP|size=20px}} (1960-1963)
* [[Edward Wichers]] {{flagicon|USA|size=20px}} (1964-1969)
* [[Norman Greenwood]] {{flagicon|AUS|size=20px}} (1970-1975)
* [[Étienne Roth]] {{flagicon|FRA|size=17px}} (1976-1979)
* Norman E. Holden {{flagicon|USA|size=20px}} (1980-1983)
* [[Raymond Martin (academic)|Raymond L. Martin]] {{flagicon|AUS|size=20px}} (1984-1987)
* [[John Robert de Laeter|John de Laeter]] {{flagicon|AUS|size=20px}} (1988-1991)
* Klaus G. Heumann {{flagicon|GER|size=18px}} (1992-1995)
* Ludolf Schultz {{flagicon|GER|size=18px}} (1996-2001)
* Philip Taylor {{flagicon|BEL|size=17px}} (2002-2003)
* Tiping Ding {{flagicon|CHN|size=17px}} (2004-2007)
* Roberto Gonfiantini {{flagicon|ITA|size=17px}} (2008-2009)
* Willi A. Brand {{flagicon|GER|size=18px}} (2010-2013)
* Juris Meija {{flagicon|CAN|size=20px}} (2014-present)
}}

In 1950, the renowned Spanish chemist [[Enrique Moles Ormella|Enrique Moles]] became the first Secretary of the Commission when this position was created.

==See also==
* [[Atomic mass unit]]
* [[Committee on Data for Science and Technology]]

== References ==
{{reflist|3}}

== External links ==
*{{Official website|http://www.ciaaw.org}}
*[http://dx.doi.org/10.1515/pac-2015-0305 Atomic weights of the elements 2013 (IUPAC Technical Report)]
*[http://dx.doi.org/10.1515/pac-2015-0503 Isotopic compositions of the elements 2013 (IUPAC Technical Report)]
*[http://www.iupac.org/publications/pac/83/2/0397/ Isotopic compositions of the elements 2009 (IUPAC Technical Report)]

{{Authority control}}

[[Category:Chemistry organizations]]
[[Category:International scientific organizations]]
[[Category:Standards organizations]]
[[Category:Scientific organizations established in 1899]]