{{Infobox journal
| title = Journal of Medical Economics
| cover = Journal of Medical Economics JME.jpg
| editor = K. Lee
| discipline = [[Health economics]], [[outcomes research]]
| abbreviation = J. Med. Econ.
| publisher = [[Informa]]
| country =
| frequency = Monthly
| history = 1998-present
| openaccess =
| website = http://www.tandfonline.com/action/journalInformation?show=aimsScope&journalCode=ijme20
| link2 = http://www.tandfonline.com/loi/ijme20
| link2-name = Online archive
| JSTOR =
| OCLC = 214329134
| LCCN =
| CODEN =
| ISSN = 1369-6998
| eISSN = 1941-837X
}}
The '''''Journal of Medical Economics''''' is a monthly [[peer-reviewed]] [[academic journal]] that covers [[econometric]] assessments of novel therapeutic and medical device interventions. It is published by [[Routledge]] and was established in 1998. The [[editor-in-chief]] is K. Lee.

==Abstracting and indexing==
The journal is abstracted and indexed in:
*[[CAB Abstracts]]<ref name=CABAB>{{cite web |url=http://www.cabi.org/publishing-products/online-information-resources/cab-abstracts/ |title=Serials cited |work=[[CAB Abstracts]] |publisher=[[CABI (organisation)|CABI]] |accessdate=2017-02-11}}</ref>
*[[Embase]]<ref name=Embase>{{cite web |url=http://www.elsevier.com/solutions/embase/coverage |title=Embase Coverage |publisher=[[Elsevier]] |work=Embase |accessdate=2017-02-11}}</ref>
*[[Index Medicus]]/[[MEDLINE]]/[[PubMed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/9892255 |title=Journal of Medical Economics |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=2017-02-11}}</ref>
*[[Science Citation Index Expanded]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Clarivate Analytics]] |work=Intellectual Property & Science |accessdate=2017-02-11}}</ref>
*[[Scopus]]<ref name=Scopus>{{cite web |url=https://www.scopus.com/sourceid/15951 |title=Source details: Journal of Medical Economics |publisher=[[Elsevier]] |work=Scopus preview |accessdate=2017-02-11}}</ref>

==References==
{{Reflist}}

==External links==
*{{Official website|1=http://www.tandfonline.com/action/journalInformation?show=aimsScope&journalCode=ijme20}}

[[Category:Economics journals]]
[[Category:General medical journals]]
[[Category:Publications established in 1998]]
[[Category:Monthly journals]]
[[Category:Taylor & Francis academic journals]]
[[Category:English-language journals]]


{{econ-journal-stub}}
{{med-journal-stub}}