{{About|the journal|the discipline|Organizational studies}}
{{Infobox journal
| title = Organization Studies
| cover = [[File:Organization Studies.jpg]]
| editor = Frank den Hond, Robin Holt
| discipline = [[Management]], [[organization studies]]
| former_names = 
| abbreviation = Organ. Stud. 
| publisher = [[Sage Publications]]
| country = 
| frequency = Monthly
| history = 1980-present
| openaccess = 
| license = 
| impact = 2.504
| impact-year = 2013
| website = http://www.sagepub.com/journals/Journal201657/title
| link1 = http://oss.sagepub.com/content/current
| link1-name = Online access
| link2 = http://oss.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 0170-8406
| eISSN = 1741-3044
| OCLC = 643034352
| LCCN = 82020032
}}
'''''Organization Studies''''' is a monthly [[peer-reviewed]] [[academic journal]] thatcovers the field of [[organization studies]]. The journal's [[editors-in-chief]] are Frank den Hond and Robin Holt. It was established in 1980 and is published by [[Sage Publications]] on behalf of [[European Group for Organizational Studies]].

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2013 [[impact factor]] is 2.504, ranking it 30th out of 172 journals in the category "Management".<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Management |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |work=[[Web of Science]] |postscript=.}}</ref> The journal is on the ''[[Financial Times]]'' top 45 list, along with only six other management journals.<ref>{{cite web|author=|url=http://www.ft.com/cms/s/2/3405a512-5cbb-11e1-8f1f-00144feabdc0.html |title=45 Journals used in FT Research Rank |publisher=FT.com |date=2012-02-22 |accessdate=2012-11-02}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.sagepub.com/journals/Journal201657/title}}
* [http://www.egosnet.org/jart/prj3/egosnet/main.jart European Group for Organizational Studies]

{{DEFAULTSORT:Organization Studies (journal)}}
[[Category:Business and management journals]]
[[Category:English-language journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1980]]
[[Category:SAGE Publications academic journals]]