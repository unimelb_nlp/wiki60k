{{Use mdy dates|date=December 2015}}
{{Infobox person
|birth_name = Whitney Moore Young Jr.
|birth_date = {{Birth date|1921|7|31|mf=y}}
|birth_place = [[Shelby County, Kentucky]], U.S.
|death_date = {{Death date and age|1971|3|11|1921|7|31}}
|death_place = [[Lagos]], [[Nigeria]]
|image = Whitney Young at White House, January 18, 1964.jpg
|caption = Whitney Young at the White House, 1964
|other_names = Whitney Young
|movement = [[National Urban League]], 
[[American Civil Rights Movement]], 
[[March on Washington]], 
[[National Association of Social Workers]], 
[[National Association for the Advancement of Colored People]]
|organization = [[National Association of Social Workers]], National Urban League
|monuments =  [[Whitney Young Memorial Bridge]], [[Clark Atlanta University]] School of Social Work, [[Whitney M. Young Jr. Service Award]], [[Whitney Young Magnet High School|Whitney Young High School in Chicago]], [[Whitney M. Young High School|Whitney M. Young High School in Cleveland]], and many other schools
|religion = [[Unitarian Universalist]]
|alma_mater =[[Kentucky State University]], [[Massachusetts Institute of Technology|MIT]], [[University of Minnesota]]
|awards =  [[Rockefeller Foundation]] Grant, [[Presidential Medal of Freedom]] 
}}

'''Whitney Moore Young Jr.''' (July 31, 1921 – March 11, 1971) was an American [[civil rights]] leader.  He spent most of his career working to end [[employment discrimination in the United States]] and turning the [[National Urban League]] from a relatively passive [[civil rights]] organization into one that aggressively worked for equitable access to socioeconomic opportunity for the historically disenfranchised.

==Early life and career==

Young was born in [[Shelby County, Kentucky]], on July 31, 1921, to educated parents. His father, Whitney M. Young, Sr., was the president of the [[Lincoln Institute (Kentucky)|Lincoln Institute]], and served twice as the president of the Kentucky Negro Educational Association.<ref>{{cite web | url=http://www.uky.edu/Libraries/NKAA/record.php?note_id=477 | title =Notable Kentucky African Americans Database Young, Whitney M., Sr. Search Results | author = | publisher =[[University of Kentucky]]}}</ref> Whitney's mother, Laura Young, was a teacher who served as the first female postmistress in Kentucky (second in the United States), being appointed to that position by President [[Franklin D. Roosevelt]] in 1940.<ref>{{cite web | url=http://www.uky.edu/Libraries/NKAA/record.php?note_id=1181 | title =Notable Kentucky African Americans Database Search Results Young, Laura R. | author = | publisher =University of Kentucky}}</ref><ref>{{cite book | url=https://books.google.com/books?id=Zb8DAAAAMBAJ&pg=PA8&lpg=PA8&dq=laura+young+first+female+postmaster+in+Kentucky&source=bl&ots=Up7BQ9cA8G&sig=OgL6fXWQ1r4DrqIDTT2aRVPMKXY&hl=en&sa=X&ei=kGFdUaLnNMr9qAGVrIDYBA&ved=0CEsQ6AEwBg#v=onepage&q=laura%20young%20first%20female%20postmaster%20in%20Kentucky&f=false | title =Jet This Week In Black History | publisher =[[Johnson Publishing Company]] | work =[[Jet (magazine)|Jet]] | date =October 5, 1978}}</ref> Young enrolled in the Lincoln Institute at the age of 13, graduating as his class valedictorian, with his sister Margaret becoming salutatorian, in 1937.<ref>{{cite web | url=http://ir.library.oregonstate.edu/xmlui/bitstream/handle/1957/8312/Andrew%20seher.pdf?sequence=1 | title =Leading the Way to Real Progress: The Portland Urban League, 1964-1970 | author =Andrew M. Sheer | publisher =oregonstate.edu}}</ref>

Young earned his [[bachelor of science]] in [[social work]] from [[Kentucky State University]], a historically black institution.<ref>{{cite web | url=http://www.wmich.edu/hhs/giving/stories/whitney-young.html | title =Whitney Young Scholar Award | author = | publisher =[[Western Michigan University]]}}</ref> Young had aspirations of becoming a doctor at Kentucky State. During this time at Kentucky State, Young was also a forward on the university's basketball team, and was a member of [[Alpha Phi Alpha]] fraternity, where he served as the vice president.<ref>{{cite book | url=https://books.google.com/books?id=Ptso3AuC2hoC&pg=PA333&lpg=PA333&dq=whitney+young+president+of+his+senior+class&source=bl&ots=gjKrCXZtYT&sig=Dzk7KgSPnZf4zjmYdP4Kr6_ykPI&hl=en&sa=X&ei=Kk9dUc_4KZCOrQGpxICADw&ved=0CFMQ6AEwBzgK#v=onepage&q=whitney%20young%20president%20of%20his%20senior%20class&f=false | title =Black Leaders of the Twentieth Century | publisher =[[University of Illinois Press]] | author =John Pope Franklin, August Meier | date =June 1, 1982}}</ref> He became the president of his senior class, and graduated in 1941.<ref>{{cite web | url=http://mlk-kpp01.stanford.edu/kingweb/about_king/encyclopedia/young_whitney.html | title =King Encyclopedia Young, Whitney Moore (1921-1971) | publisher =stanford.edu}}</ref>

During [[World War II]], Young was trained in electrical engineering at the [[Massachusetts Institute of Technology]]. He was then assigned to a road construction crew of black soldiers supervised by [[U.S. South|Southern]] white officers. After just three weeks, he was promoted from private to first sergeant, creating hostility on both sides. Despite the tension, Young was able to mediate effectively between his white officers and black soldiers angry at their poor treatment. This situation propelled Young into a career in race relations.

After the war, Young joined his wife, [[Margaret Buckner Young|Margaret]], at the [[University of Minnesota]], where he earned a master's degree in social work in 1947 and volunteered for the [[Saint Paul, Minnesota|St. Paul]] branch of the National Urban League. He was then appointed as the industrial relations secretary in that branch in 1949.<ref>{{cite web | url=http://www.nyul.org/files/WMY_Scholarship_app10.pdf | title =Whitney M. Young Jr. Scholarship | author = | publisher =New York Urban League}}</ref>

In 1950, Young became president of the National Urban League's [[Omaha, Nebraska]] chapter. In that position, he helped get black workers into jobs previously reserved for whites. Under his leadership, the chapter tripled its number of paying members. While he was president of the Omaha Urban League, Young taught at the [[University of Nebraska]] from 1950 to 1954, and [[Creighton University]] from 1951 to 1952.<ref>{{cite web | url=http://www.harvardsquarelibrary.org/unitarians/young.html | title =Whitney Moore Young Jr.: Social Work Administrator | author =Thomas Blair | publisher =harvardsquarelibrary.org}}</ref>

In 1954, he took up his next position, as the first dean of social work at [[Clark Atlanta University|Atlanta University]].<ref>W. Peebles-Wilkins, 1995, "Young, Whitney Moore Jr.," ''Encyclopedia of Social Work'' (19th ed., Vol. 3, R.L. Edwards, Ed.), Washington, DC: NASW Press, pp. 2618-2619, see [http://www.naswfoundation.org/pioneers/y/WYoung.htm], accessed February 15, 2015.</ref><ref>Office of the Dean, 2015, "Brief History of the Whitney M. Young Jr. School of Social Work," Atlanta, GA:Clark Atlanta University (Whitney M. Young Jr. School of Social Work), see [www.cau.edu/school-of-social-work/_files/msw_catalog.pdf], accessed February 15, 2015.</ref> There, Young supported [[Alumnus/a|alumni]] in their [[boycott]] of the Georgia Conference of Social Welfare in response to low rates of African-American employment within the organization.{{when|date=February 2015}}<ref>{{cite web | url=http://www.cswe.org/cms/57119.aspx | title =Whitney M. Young Jr. (1921–1971) | author = | publisher =[[Council on Social Work Education]]}}</ref> Young and his wife Margaret were the first blacks to join the United Liberal Church (since 1965, named the [[Unitarian Universalist Congregation of Atlanta]]), {{when|date=February 2015}} and Whitney would eventually join its Board of Trustees.{{citation needed|date=February 2015}} Due in part to the Youngs' influence,the church stopped having its annual picnics at segregated parks and became "integrated not just desegregated."{{attribution needed|date=February 2015}} Many in the congregation were active in the civil rights movement, and the Reverend Martin Luther King Jr., then assistant to his father at nearby Ebenezer Baptist Church, was a pulpit guest.<ref>{{cite web | url=http://www.uuca.org/about-us/our-mission-history/ | title =Our Mission & History | author = | publisher =UU Congregation of Atlanta}}</ref>
 
In 1960, Young was awarded a [[Rockefeller Foundation]] grant for a postgraduate year at [[Harvard University]].{{citation needed|date=February 2015}}  In the same year, he joined the [[National Association for the Advancement of Colored People|NAACP]] and rose to become state president,{{where|date=February 2015}}{{when|date=February 2015}}<ref>{{cite web | url=http://www.kysu.edu/academics/collegesAndSchools/whitneyyoungschoolofhonors/whoiswhitneyyoung.htm | title =Who is Whitney M. Young Jr.? | author = | publisher =Kentucky State University}}</ref> where he was also a close friend of [[Roy Wilkins]], its [[executive director]].{{citation needed|date=February 2015}}

==Executive Director of National Urban League==

In 1961, at age 40, Young became Executive Director of the National Urban League. He was unanimously selected by the National Urban League's Board of Directors, succeeding [[Lester Granger]] on October 1, 1961.<ref>{{cite book | url=https://books.google.com/books?id=NrEDAAAAMBAJ&pg=PA5&lpg=PA5&dq=Whitney+Young+rockefeller+foundation+grant&source=bl&ots=o40IDhlZlW&sig=RS1O9L_68HI5LCuOcMWIKaDL-qY&hl=en&sa=X&ei=pS5dUenzDcfjqAHs94DYBg&ved=0CEwQ6AEwBjge#v=onepage&q=Whitney%20Young%20rockefeller%20foundation%20grant&f=false | title = Whitney Young Succeeds Urban League's Granger | publisher =Johnson Publishing Company | work =Jet | date =February 9, 1961}}</ref> Within four years he expanded the organization from 38 employees to 1,600 employees; and from an annual budget of $325,000 to one of $6,100,000. Young served as President of the Urban League until his death in 1971.

The Urban League had traditionally been a cautious and moderate organization with many white members. During Young's ten-year tenure at the League, he brought the organization to the forefront of the [[American Civil Rights Movement]]. He both greatly expanded its mission and kept the support of influential white business and political leaders. In an 1964 interview with [[Robert Penn Warren]] for the book ''[[Who Speaks for the Negro?]]'', Young expressed the mission of the Urban League not as ground-level activism in itself but as the supplement and complement of the activities of all other organizations; he states, "we are the social engineers, we are the strategists, we are the planners, we are the people who work at the level of policy-making, policy implementation, the highest echelons of the corporate community, the highest echelons of the governmental community – both at the federal, state and local level – the highest echelons of the labor movement."<ref>{{cite web|last1=Robert Penn Warren Center for the Humanities|title=Whitney Young|url=http://whospeaks.library.vanderbilt.edu/interview/whitney-young|website=Robert Penn Warren's ''Who Speaks for the Negro?'' Archive|accessdate=March 11, 2015}}</ref> As part of the League's new mission, Young initiated programs like "Street Academy", an alternative education system to prepare high school dropouts for college, and "New Thrust", an effort to help local black leaders identify and solve community problems.

Young also pushed for federal aid to cities, proposing a domestic "[[Marshall Plan]]". This plan, which called for $145 billion in spending over 10 years, was partially incorporated into [[President of the United States|President]] [[Lyndon B. Johnson|Lyndon B. Johnson's]] [[War on Poverty]]. Young described his proposals for integration, social programs, and affirmative action in his two books, ''To Be Equal'' (1964) and ''Beyond Racism'' (1969).

As executive director of the League, Young pushed major corporations to hire more blacks. In doing so, he fostered close relationships with CEOs such as [[Henry Ford II]], leading some blacks to charge that Young had sold out to the white establishment. Young denied these charges and stressed the importance of working within the system to effect change. Still, Young was not afraid to take a bold stand in favor of civil rights. For instance, in 1963, Young was one of the organizers of the [[March on Washington for Jobs and Freedom|March on Washington]] despite the opposition of many white business leaders.

[[File:Whitney Young and Lyndon Johnson.jpg|thumb|upright|Young receives "[[Lyndon B. Johnson#Senate Democratic leader|The Treatment]]" from President Johnson (1966)]]

Despite his reluctance to enter politics himself, Young was an important advisor to Presidents [[John F. Kennedy|Kennedy]], [[Lyndon B. Johnson|Johnson]], and [[Richard Nixon|Nixon]]. In 1968, representatives of President-elect Richard Nixon tried to interest Young in a [[United States Cabinet|Cabinet]] post, but Young refused, believing that he could accomplish more through the Urban League.<ref>Nancy J. Weiss, ''Whitney M. Young Jr. and the Struggle for Civil Rights'', Princeton University Press, 1990, p. 192.</ref>

Young had a particularly close relationship with President Johnson, and in 1969, Johnson honored Young with the highest civilian award, the [[Presidential Medal of Freedom]]. Young, in turn, was impressed by Johnson's commitment to civil rights.

Despite their close personal relationship, Young was frustrated by Johnson's attempts to use him to balance [[Martin Luther King Jr.|Martin Luther King's]] opposition to the increasingly unpopular [[Vietnam War]].<ref>Weiss (1990), p. 163.</ref> Young publicly supported Johnson's war policy, but came to oppose the war after the end of Johnson's presidency.

In 1968, Herman B. Ferguson and Arthur Harris were convicted of conspiring to murder Young as part of what was described as a "black revolutionary plot."  The trial took place in the [[New York State Supreme Court]], with [[Paul Balsam|Justice Paul Balsam]] presiding.<ref>''See'' "Paul Balsam Dead; A Justice In Queens," ''[[The New York Times]]'', December 24, 1972, [https://select.nytimes.com/gst/abstract.html?res=F00612FE3E59107A93C6AB1789D95F468785F9 available for purchase at]</ref>

==Leadership at the National Association of Social Workers (NASW)==

Young served as President of the [[National Association of Social Workers]] (NASW), from 1969 to 1971. He took office at a time of fiscal instability in the association and uncertainty about President Nixon's continuing commitment to the "War on Poverty" and to ending the war in Vietnam. At the 1969 NASW Delegate Assembly Young stated,

<blockquote>First of all, I think the country is in deep trouble. We, as a country have blazed unimagined trails technologically and industrially. We have not yet begun to pioneer in those things that are human and social… I think that social work is uniquely equipped to play a major role in this social and human renaissance of our society, which will, if successful, lead to its survival, and if it is unsuccessful, will lead to its justifiable death.
:—''NASW News'', May 1969</blockquote>

[[File:Whitney Young NYWTS.jpg|thumb|right| Whitney Young]]

Mr. Young spent his tenure as President of NASW ensuring that the profession kept pace with the troubling social and human challenges it was facing. ''NASW News'' articles document his call to action for social workers to address social welfare through poverty reduction, race reconciliation, and putting an end to the War in Vietnam. In the ''NASW News'', July 1970, he challenged his professional social work organization to take leadership in the national struggle for social welfare:

<blockquote>The crisis in health and welfare services in our nation today highlights for NASW what many of us have been stressing for a long time: inherent in the responsibility for leadership in social welfare is responsibility for professional action. They are not disparate aspects of social work but merely two faces of the same coin to be spent on more and better services for the people who need our help. It is out of our belief in this broad definition of responsibility for social welfare that NASW is taking leadership in the efforts to reorder our nation's priorities and future direction, and is calling on social workers everywhere to do the same.<ref>{{cite web | url=http://www.nasw-pa.org/displaycommon.cfm?an=1&subarticlenbr=192&printpage=true | title =Social Worker of the Past: Whitney M. Young Jr. | publisher =nasw-pa.org}}</ref></blockquote>

The ''NASW News'', May 1971, tribute to Young noted that "As usual Whitney Young was preparing to do battle on the major issues and programs facing the association and the nation. And he was doing it with his usual aplomb-dapper, self-assured, ready to deal with the "power" people to bring about change for the powerless."<ref>{{cite web | url=http://www.socialworkers.org/whitneyyoung/WhitneyYoungTeachingGuide.pdf | title =Leadership Lesson From Whitney M. Young | author = | publisher =National Association of Social Workers | year =2009}}</ref>

Mr. Young was also well known in the profession of Social Work for being the Dean of the school of Social Work at [[Clark Atlanta University]], which now bears his name. The school has a solid history of Social work, graduating leaders in the profession and having created and founded the "Afro-Centric" prospective of Social Work, a frequently used theory practice in urban areas. In his last column as President for NASW, Young wrote, "whatever we do we should tell the public what we are doing and why. They have to hear from social workers as much as they hear from reporters and government officials."

==Death==
On March 11, 1971, Whitney Young died of a heart attack after swimming with friends in [[Lagos]], [[Nigeria]], where he was attending a conference sponsored by the African-American Institute.<ref>{{cite web|url=http://www.ket.org/cgi-local/fw_comment.exe/db/ket/dmps/Programs?do=topic&topicid=LOUL110045&id=LOUL|title=KET -  Et Cetera: Whitney M. Young Jr., Civil Rights Leader|author=David Hempy|publisher=|accessdate=December 18, 2015}}</ref> President Nixon sent a plane to Nigeria to collect Young's body and traveled to Kentucky to deliver the [[eulogy]] at Young's funeral.

==Legacy==

Whitney Young's legacy, as President Nixon stated in his eulogy, was that "he knew how to accomplish what other people were merely for".<ref>Richard Nixon: [http://www.presidency.ucsb.edu/ws/index.php?pid=2940 Eulogy Delivered at Burial Services for Whitney M. Young Jr., in Lexington, Kentucky.<!-- Bot generated title -->]</ref> Young's work was instrumental in breaking down the barriers of segregation and inequality that held back African Americans.

===Namesakes===

Many sites across the country are named after Young or have memorials dedicated to him. For instance, in 1973, the East Capitol Street Bridge in [[Washington, D.C.]], was renamed the [[Whitney Young Memorial Bridge]] in his honor. Young's birthplace ([[Whitney Young Birthplace and Museum]]) in Shelby County, Kentucky is a designated [[National Historic Landmark]], with a museum dedicated to Young's life and achievements.

Young was honored on a United States postage stamp as part of its ongoing Black Heritage series.

The Whitney Young School of Honors and Liberal Studies at [[Kentucky State University]] was named after him. Also, [[Clark Atlanta University]] named its School of Social Work, where Whitney Young served as Dean, in Young's honor. The Whitney M. Young School of Social Work is well known for founding the "Afro-Centric" perspective of social work.

The [[Boy Scouts of America]] created the [[Whitney M. Young Jr. Service Award]] to recognize outstanding services by an adult individual or an organization for demonstrated involvement in the development and implementation of Scouting opportunities for youth from rural or low-income urban backgrounds. In 1973, The African American MBA Association at The Wharton School, University of Pennsylvania held its first Annual Whitney M. Young Jr. Memorial Conference. After 38 years, the Whitney M. Young Jr. Memorial Conference<ref>{{cite web|url=http://www.wmyconference.com|title=WMY Conference 2015 - The New Black: Creating Impact in Business and Society|publisher=|accessdate=December 18, 2015}}</ref> is the longest student-run conference held at The Wharton School.

Schools named after Young include [[Whitney M. Young Magnet High School]] in Chicago, [[Whitney M. Young Gifted & Talented Leadership Academy]] in [[Cleveland]], [[Ohio]] and Whitney M. Young Elementary in [[Dallas]], [[Texas]].

The Whitney M. Young Health Center in Albany, New York was also named after him.<ref>{{cite web|url=http://www.wmyhealth.org/about-us/news/|title=Whitney M Young Health :: News|publisher=|accessdate=December 18, 2015}}</ref>

==In movies==

The documentary, ''The Powerbroker: Whitney Young's Fight for Civil Rights'', directed by Christine Khalafian and Taylor Hamilton, chronicles Young's rise from segregated Kentucky to the national movement for civil rights. The film includes archival footage, photos, and interviews compiled by Young's niece, award-winning journalist Bonnie Boswell Hamilton. Interviews include [[Henry Louis Gates Jr.]], [[Ossie Davis]], [[Julian Bond]], [[Roy Innis]], [[Vernon Jordan]], [[Donald Rumsfeld]], and [[Dorothy Height]].

==See also==
* [[African-American Civil Rights Movement in Omaha, Nebraska]]
* [[Big Six (civil rights)]]
* [[List of civil rights leaders]]

==References==
{{reflist|30em}}

==Further reading==
* {{cite news |last=Williams |first=Rudi |title=Whitney M. Young Jr.: Little Known Civil Rights Pioneer |url=http://www.defense.gov/news/newsarticle.aspx?id=43988 |work=DoD News |publisher=U.S. Department of Defense |date=February 1, 2002 |accessdate=February 25, 2015}}

==External links==
{{Commons category}}
* [http://www.cr.nps.gov/nr/travel/civilrights/ky2.htm Whitney M. Young Birthplace]
* {{Find a Grave|1136}}
* [http://wmy.wharton.upenn.edu/ Whitney M. Young Memorial Conference] at the [[Wharton School of Business]]
* [http://www.lbjlib.utexas.edu/johnson/archives.hom/oralhistory.hom/YoungW/YoungW.asp Oral History Interview with Whitney Young, from the Lyndon Baines Johnson Library]
* [http://www.whitneyyoungfilm.com ''One Handshake at a Time'' documentary website]
* [http://www.ket.org/cgi-local/fw_comment.exe/db/ket/dmps/Programs?do=topic&topicid=LOUL110045&id=LOUL Whitney M. Young Jr., Civil Rights Leader. retrieved from Louisville Life]

{{NUL presidents}}
{{African-American Civil Rights Movement}}

{{Authority control}}

{{DEFAULTSORT:Young, Whitney}}
[[Category:1921 births]]
[[Category:1971 deaths]]
[[Category:20th-century African-American activists]]
[[Category:Accidental deaths in Nigeria]]
[[Category:African-American life in Omaha, Nebraska]]
[[Category:Activists for African-American civil rights]]
[[Category:American military personnel of World War II]]
[[Category:American Unitarians]]
[[Category:Burials at Ferncliff Cemetery]]
[[Category:Deaths by drowning]]
[[Category:Harvard University alumni]]
[[Category:Kentucky State University alumni]]
[[Category:Massachusetts Institute of Technology alumni]]
[[Category:People from New Rochelle, New York]]
[[Category:People from Omaha, Nebraska]]
[[Category:People from Shelby County, Kentucky]]
[[Category:Presidential Medal of Freedom recipients]]
[[Category:University of Minnesota alumni]]
[[Category:Activists from New York]]