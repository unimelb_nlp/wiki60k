{{Infobox scientist
| name        = Paul T. Baker 
| native_name = 
| native_name_lang = 
| image       =         <!--(filename only, i.e. without "File:" prefix)-->
| image_size  = 
| alt         = 
| caption     = 
| birth_date  = {{birth date |1927|02|28}}
| birth_place = [[Burlington, Iowa|Burlington]], [[Iowa]], U.S
| death_date  = {{death date and age |2007|11|29 |1927|02|28}}
| death_place = [[Chapel Hill, North Carolina|Chapel Hill]], [[North Carolina]]
| death_cause = 
| resting_place = 
| resting_place_coordinates =  <!--{{coord|LAT|LONG|type:landmark|display=inline,title}}-->
| other_names = 
| residence   = 
| citizenship = 
| nationality = [[American people|American]]
| fields      = [[Anthropology]]
| workplaces  = [[Pennsylvania State University]]
| patrons     = 
| education   = 
| alma_mater  = [[Harvard University]]
| thesis_title = Man in the Desert: A Study of the Racial and Morphological Factors in Man's Tolerance of Heat.
| thesis_url  =         <!--(or  | thesis1_url  =   and  | thesis2_url  =  )-->
| thesis_year = 1956
| doctoral_advisor = [[William W. Howells]]
| academic_advisors =
| doctoral_students = 
| notable_students = 
| known_for   = 
| influences  =
| influenced  =
| awards      = 
| author_abbrev_bot = 
| author_abbrev_zoo = 
| spouse      =         <!--(or | spouses = )-->
| partner     =         <!--(or | partners = )-->
| children    = 
| signature   =         <!--(filename only)-->
| signature_alt = 
| website     =         <!--{{URL|www.example.com}}-->
| footnotes   = 
}}
'''Paul Thornell Baker''' (February 28, 1927 – November 29, 2007) was Evan Pugh Professor Emeritus of Anthropology at The [[Pennsylvania State University]], and was “one of the most influential biological anthropologists of his generation, contributing substantially to the transformation of the field from a largely descriptive to a hypothesis-driven science in the latter half of the 20th century. He pioneered multidisciplinary field science, firmly established a place for biological anthropology and human population biology in national and international science, and trained a host of graduate students in good science, who, in turn, continued his commitment to collaborative research.”<ref>Garruto, Ralph (2008)</ref>

== Biography ==

After serving in the U.S. army from 1945-1947, during World War II, Baker began his undergraduate studies at the [[University of Miami]]   and completed his BA at the [[University of New Mexico]]   in 1951.<ref>Garruto, Ralph (2008).</ref> He  obtained his PhD from [[Harvard University]] in 1956.<ref name="Obit">{{cite web |url=http://news.psu.edu/story/192054/2007/11/29/evan-pugh-professor-emeritus-dies-age-80 |title=Evan Pugh professor emeritus dies at age 80 |newspaper=news.psu.edu|date=November 29, 2007|accessdate= October 22, 2015}}</ref><ref>A Tribute (2009)</ref>

Baker was employed by the U.S. Army Climatic Research Laboratory, a part of the Quartermaster Corps U.S. in Natick, Massachusetts where he conducted “heat stress research on military personnel at Fort Lee, Virginia (hot-wet) and in the Yuma Desert (hot-dry), and cold stress at Fort Churchill, Canada, on Hudson Bay”. His  PhD dissertation focused on his research, lasting from 1952-1956, in Yuma and Fort Lee.<ref>Garruto, Ralph (2008)</ref>
In 1957, Baker obtained a position at Penn State University in their biophysics laboratory. In 1958, he moved to the Department of Sociology and Anthropology.<ref>Garruto, Ralph (2008)</ref>

==Research==
In 1962 Baker was awarded funding from the U.S. Army Research and Development Command to do research in Peru. Baker was an active member of the International Biological Programme (IBP) in the U.S., and internationally in Peru.<ref>A Tribute (2009)</ref>
Baker conducted research into the high altitude adaptation of Andean Quechua-speaking residents began in Cuzco and Chinchero in 1962 and was extended to the District of Nuñoa on the Peruvian altiplano in 1964. It continued into the late 1960s.<ref>A Tribute (2009)</ref>

“Baker was invited to participate in a 1964 Wenner-Gren Foundation symposium at Burg Wartenstein in Austria that was designed to plan the Human Adaptability (HA) research component of the IBP at the worldwide level.  It was organized by Joseph S. Weiner, Convener of the IBP/HA, and strongly supported by Lita Fejos Osmundsen, then the head of Wenner-Gren.  As Baker later wrote: “I now find it difficult to recapture in words the excitement I felt about the ideas, the people, and the potential scientific results of the HA effort.””<ref>A Tribute (2009)</ref>
“In the United States, Baker was appointed to the National Academy of Sciences/NRC IBP National Committee charged to oversee studies in this country.  The Nuñoa high altitude research, which was a part of the U.S. IBP, was followed by studies of migrants from the Andes to the coast of Peru, and both projects resulted in comprehensive studies on all facets of human adaptation in these Peruvian communities.  Baker also edited the synthesis of the international high-altitude studies”.<ref>A Tribute (2009)</ref>

“As the Peruvian research was winding down in the mid-1970s and the synthesis of the work was completed, Baker initiated a new project on Pacific migration and modernization of Samoans in the context of health and diseases of Westernization.  The project focused on comparisons of migrant and sedentary populations based on the experience of migrants from the earlier Peruvian studies.  The principal research questions centered on adaptation of Pacific Islanders to a Western life-style and its concomitant effects on health, which included a prevalence of obesity, adult-onset diabetes, and cardiovascular disease. As with the high-altitude research, the Samoan studies were concerned with adaptation, health, and the biocultural bases of human responses to environmental stress.  In the Nuñoa project, the identified stresses were high-altitude hypoxia and cold; in the Samoan project, the stress was exposure to a Westernized or modernized environment, through in situ acculturation or migration”<ref>A Tribute (2009)</ref>

== Achievements <ref>A Tribute (2009)</ref>==
*President of the [[American Association of Physical Anthropologists]] (1969–71) 
*President of the [[Human Biology Council]] (now Association) (1974–77) 
*President of the International Association of Human Biologists (1980–90) 
*He was elected to the [[National Academy of Sciences]] in 1980

;Honors
*Huxley Memorial Lecturer and Medalist (1981)
*Gorjanovic-Krambergeri Medalist (1985)
*The Yugoslavian Order of the Golden Star with Necklace (1988)
* Distinguished Service Award of the [[American Anthropological Association]] (1989)
* Mahatma Gandhi Freedom Award of the [[College of William and Mary]] (1991)
* Charles R. Darwin Lifetime Achievement Award of the American Association of Physical Anthropologists (1993) 
* Franz Boas Distinguished Achievement Award of the [[Human Biology Association]] (2000)

== Published books ==

*1966 With J. S. Weiner, eds. The Biology of Human Adaptability, 1st ed. New York: Clarendon Press.
*1976 With M. A. Little, eds. Man in the Andes: A Multidisciplinary Study of High Altitude Quechua. Stroudsburg, Pa.: Dowden, Hutchinson and Ross.
*1977 Ed. Human Population Problems in the Biosphere: Some Research Strategies and Designs. MAB Technical Notes 3. Paris: UNESCO .
*1978 Ed. The Biology of High Altitude Peoples. Cambridge, Eng.: Cambridge University Press.
*1982 Ed. Human Population and Biosphere Interactions in the Central Andes, vol. II, State of Knowledge Reports on Andean Ecosystems. Mountain Research and Development, vol. 2. Boulder, CO : UNESCO, International Mountain Society.
*1986 With J. M. Hanna and T. S. Baker, eds. The Changing Samoans: Behavior and Health in Transition. New York: Oxford University Press.
*1988 With G. A. Harrison, J. M. Tanner, and D. Pilbeam. Human Biology: An Introduction to Human Evolution, Variation, Growth, and Ecology, 3rd ed. Oxford, Eng.: Oxford University Press.

== Notes ==

{{Reflist}}

==References==
*Garruto, Ralph M., Gary D. James, and Michael A. Little. Paul Thornell Baker, A Biographical Memoir. National Academy of Sciences, 2009. 
*A Tribute to Paul Thornell Baker. Columbus: 33rd Annual Meeting of the, 2008.

{{Authority control}}

{{DEFAULTSORT:Baker, Paul T.}}
[[Category:1927 births]]
[[Category:2007 deaths]]
[[Category:American anthropologists]]
[[Category:Pennsylvania State University faculty]]
[[Category:Harvard University alumni]]
[[Category:University of Miami alumni]]
[[Category:University of New Mexico alumni]]
[[Category:Members of the United States National Academy of Sciences]]
[[Category:People from Burlington, Iowa]]