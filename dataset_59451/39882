<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]] and [[Wikipedia:WikiProject Biography/Military]]. -->
{{Use dmy dates|date=March 2017}}
{{Use British English|date=March 2017}}
{{Infobox military person
| name          =William John Charles Kennedy-Cochran-Patrick
| image         =
| caption       =
| birth_date          = <!-- {{Birth date and age|df=yes|YYYY|MM|DD}} -->25 May 1896
| death_date          = <!-- {{Death date and age|df=yes|YYYY|MM|DD|YYYY|MM|DD}} -->26 September 1933 (aged 37)
| placeofburial_label = 
| placeofburial = 
| birth_place  =[[Beith]], [[Ayrshire]], [[Scotland]]
| death_place  =Baragwanath Airport, [[Johannesburg]], [[South Africa]]
| placeofburial_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| nickname      =
| allegiance    ={{UK}}
| branch        =[[File:Flag of the British Army.svg|23px]] [[British Army]]<br />{{air force|UK}}
| serviceyears  =1915–1919
| rank          =[[Major]]
| unit          =[[The Rifle Brigade]]<br />[[Royal Flying Corps]]
*No. 1 Aeroplane Depot
*[[No. 70 Squadron RAF|No. 70 Squadron]]
*[[No. 23 Squadron RAF|No. 23 Squadron]]
| commands      =[[No. 60 Squadron RAF|No. 60 Squadron]]
| battles       = [[World War I|First World War]]
| awards        =[[Distinguished Service Order]], [[Military Cross]] & [[medal bar|Bar]]
| relations     =
| laterwork     =Aerial surveys in South America, Burma, Iraq, and Africa
}}
Major '''William John Charles Kennedy-Cochran-Patrick''' [[Distinguished Service Order|DSO]], [[Military Cross|MC]] & [[medal bar|Bar]] (25 May 1896 – 26 September 1933) was a [[World War I|First World War]] [[flying ace]], credited with 21 aerial victories. He was the leading ace flying the [[Spad VII]] fighter, and of No. 23 Squadron.<ref name=aerodrome>{{cite web|url=http://www.theaerodrome.com/aces/scotland/kennedy.php|title=William John Charles Kennedy-Cochran-Patrick|work=aerodrome.com|accessdate=8 September 2009}}</ref> He later flew aerial surveys on three continents.<ref name=Burke1131>{{cite book |title=Burke's Landed Gentry of Great Britain |page= 1131 }}</ref>

==Early life and service==
Kennedy-Cochran-Patrick was born in [[Beith, Ayrshire]], Scotland on 25 May 1896,<ref name=aerodrome /> the only son of Neil James Kennedy-Cochran-Patrick (later Sir Neil) and Eleonora Agnes Kennedy-Cochran-Patrick of Woodside and Ladyland.<ref name=Burke1131 /><ref>{{cite journal|url=http://www.flightglobal.com/pdfarchive/view/1933/1933%20-%200735.html|title=Deaths|volume=XXV, No. 40|issue=1293|work=[[Flight (magazine)|Flight]]|editor=Stanley Spooner|page=xvii|date=5 October 1933}}</ref> He had three sisters, all younger.<ref name=Rootsweb>{{cite web|url=http://wc.rootsweb.ancestry.com/cgi-bin/igm.cgi?op=REG&db=cwh001&id=I10736|title=Hunter Surname DNA Project Worldwide Database - includes others related to Dr. Johnson Calhoun Hunter, born 1787|work=[[Rootsweb]]|publisher=[[ancestry.com]]|accessdate=8 September 2009}}</ref>

He attended [[Wellington College, Berkshire|Wellington College]] in Berkshire and [[Trinity College, Cambridge]]<ref name=Burke1131 /> before training at the [[Royal Military College, Sandhurst]].<ref name=aerodrome />

==Flying service==
Kennedy-Cochran-Patrick qualified as a pilot in April 1915. He was so skilled a flier that he was assigned as chief [[test pilot]] to No. 1 Aeroplane Depot at [[Saint-Omer]], [[France]].<ref>{{cite book |title= Spad VII Aces of World War I |page=45  }}</ref> He was officially seconded to the [[Royal Flying Corps]] from [[The Rifle Brigade]] on 11 June 1915.<ref name=Burke1131 /><ref>{{LondonGazette|issue=29206|supp=|startpage=6169|date=25 June 1915|accessdate=2009-09-10}}</ref>

On 17 March 1916, he was promoted from second lieutenant to lieutenant while staying seconded to the RFC.<ref>{{LondonGazette|issue=29818|supp=|startpage=10860|date=10 November 1916|accessdate=2009-09-10}}</ref> On 26 April, he achieved his first victory despite his test pilot status. He used [[Nieuport]] No. 5172 to attack an [[LVG]] C-type three times. The LVG crash-landed with a dead crew.<ref name=aerodrome /><ref name=Spads46>{{cite book |title= Spad VII Aces of World War I |page=46 }}</ref> He was awarded the [[Military Cross]] on 16 May 1916, for this capture of an enemy plane.<ref name=LG1>{{LondonGazette|issue=29584|supp=|startpage=4930|date=16 May 1916|accessdate=2009-09-10}}</ref>

As a result of his victory and award, he was transferred to [[No. 70 Squadron RAF|No. 70 Squadron]] to fly [[Sopwith 1½ Strutter]]s. He scored victories two and three on 14 and 15 September 1916, having his observer killed on both occasions.<ref name=Spads46 />

[[File:Spad S.VII NMUSAF.jpg|thumb|right|SPAD S.VII at the [[National Museum of the United States Air Force|National Museum of the US Air Force]]]]
He was promoted to Captain and transferred to [[No. 23 Squadron RAF|No. 23 Squadron]] in early 1917 to fly [[Spad VII]]s.<ref name=Spads46 /> He shot a double on 22 April to become an ace. He won twice more in April, and again on 2 May. His next victory, on 11 May, would be his most notable; he set afire the [[Albatros D.III]] of ''Jasta'' 5's triple-ace ''Offz. Stlvtr.'' [[Edmund Nathanael]], its fuselage falling away from its wings and plunged in flames to earth.<ref name=Spads46 /><ref name=LG1/>

It was also during this stretch that he was entrusted by Major General [[Hugh Trenchard]] to evaluate the new [[Spad XIII]] at La Bonne Maison Aerodrome. He turned in his report to Trenchard on 1 May 1917.<ref>{{cite book |title=''SPAD XII/XIII aces of World War I'' |page=  }}</ref>

Kennedy-Cochran-Patrick claimed three more times in May, four times in June, and five in July. After his final victory, on 16 July 1917, he was promoted to acting Major on 22 July,<ref name=flight1>{{cite web|url=http://www.flightglobal.com/pdfarchive/view/1917/1917%20-%201329.html|title=Flight Magazine, 9 August 1917|publisher=Flight Global Archive|accessdate=2009-09-11}}</ref> and given command of [[No. 60 Squadron RAF|No. 60 Squadron]].<ref name=Spads46 />

In lieu of a second award, he was awarded a Bar to his Military Cross on 14 August 1917.<ref name=LG2>{{LondonGazette|issue=30234|supp=yes|startpage=8355|date=14 August 1917|accessdate=2009-09-10}}</ref> A month later, he was awarded the [[Distinguished Service Order]].<ref name=LG3>{{LondonGazette|issue=30287|supp=yes|startpage=9557|date=17 September 1917|accessdate=2009-09-10}}</ref>

He was [[Mentioned in Despatches]] on 7 November 1917 by Field Marshal [[Sir Douglas Haig]]. He was denoted as a lieutenant doing a job two grades his senior; he was a temporary major.<ref name=flight1/>

At the end of 1917 he was transferred back to England to the Training Directorate of the Air Board. From there, he returned to No. 1 AD in 1918.<ref name=Burke1131 /><ref name=Spads46 />

His war time tally was 1 captured, 6 and 4 shared destroyed, 9 and 1 shared 'out of control'.<ref>Above The Trenches, Shores 1990, page 111</ref>

==Postwar life==
He resigned his commission on 9 July 1919.<ref>{{LondonGazette|issue=31441|supp=yes|startpage=8684|date=8 July 1919|accessdate=2009-09-11}}</ref> He became a major in the General Reserve of Officers the same day.<ref>{{LondonGazette|issue=31441|supp=yes|startpage=8686|date=8 July 1919|accessdate=2009-09-11}}</ref>

He married Natalie Bertha Tanner of Larches, Rusherville, Kent on 20 July 1924.<ref name=Burke1131 /> They had a single son two years later, Neil Aylmer Kennedy-Cochran-Patrick (later Neil Aylmer Hunter).<ref name=Rootsweb />

He went on to fly a lot of aerial survey work in the postwar years. He carried out surveys in South America, Burma, Iraq, and Africa.<ref name=Burke1131 /> He established his own company, The Aircraft Operating Company of South Africa Pty Ltd. He had a contract for a {{convert|20000|sqmi|km2|adj=on}} air route survey in [[Northern Rhodesia]] (now [[Zambia]]).<ref name=aaa>{{cite web|url=http://www.ab-ix.co.uk/zs-aaa.pdf|title=Prewar Aircraft Register South Africa|publisher=Air-Britain Information Exchange|accessdate=2009-09-11}}</ref>

He was elected to membership of the [[Royal Aero Club]] on 10 December 1930.<ref>{{cite web|url=http://www.flightglobal.com/pdfarchive/view/1930/untitled0%20-%201560.html|title=Flight Magazine, 19 December 1930|publisher=Flight Global Archive|accessdate=2009-09-11}}</ref>

Then, on 26 September 1933, he took off from Baragwanath Airport near [[Johannesburg]], [[South Africa]], flying a [[de Havilland DH.84]] Dragon, registration ZS-AEF. He [[stall (flight)|stalled out]] at {{convert|250|ft|m}} after making a steep turn.<ref name=aaa/> The resulting crash killed him and his passenger, Sir Michael Oppenheimer.<ref name=aerodrome /><ref>{{cite web|url=http://aviation-safety.net/wikibase/dblist.php?Year=1933|title=ASN Aviation Safety Network Wikibase, 1933|work=Aviation Safety Network|publisher=Flight Safety Foundation|accessdate=2009-10-11}}</ref>

==Honours and awards==
'''[[Military Cross]] (MC)'''

{{quote|2nd Lt. William John Charles Kennedy-Cochran-Patrick, Rif. Brig, and R.F.C.

For conspicuous skill and determination. He climbed and attacked an enemy machine at 14,000 feet and, although he failed in his first and second attacks, he went for it again a third time, shot both pilot and observer and brought it down. He followed it down and landed alongside.<ref name=LG1/>}}
 
'''Military Cross (MC) Bar'''

{{quote|Lt. (T./Capt.) William John Charles Kennedy-Cochran-Patrick, M.C., Rif. Bde. and R.F.C.
   
For conspicuous gallantry in attacking hostile aircraft. Within two months he brought down two hostile machines in flames, and four others completely out of control. In addition, he has driven several others down in a damaged condition.<ref name=LG2/>}}

'''[[Distinguished Service Order]] (DSO)'''

{{quote|Lt. (T./Capt.) William John Charles Kennedy-Cochran-Patrick, M.C., Bif. Bde. and R.F.C.

For conspicuous gallantry and devotion to duty on numerous occasions in destroying and driving down hostile machines, frequently engaging the enemy with great dash and a fine offensive spirit when encountered in superior numbers. By his cool judgment and splendid fearlessness he has instilled confidence in all around him, his brilliant leadership being chiefly responsible for his numerous successes.<ref name=LG3/>}}

==Notes==
{{reflist|2}}

==References==
* {{cite book|last=Dewar|first=Peter Beauclerk|title=Burke's Landed Gentry of Great Britain: Together With Members of the Titled and Non-titled Contemporary Establishment|year=2001|edition=19th|publisher=Burke's Peerage & Gentry|location=Wilmington, Delaware, USA|isbn=978-0-9711966-0-5}}
* {{cite book|last=Guttman|first=Jon|title=Spad VII Aces of World War I|year=2001|publisher=Osprey Publishing|location=|isbn=978-1-84176-222-7}}
* Guttman, Jon.(2002) ''SPAD XII/XIII Aces of World War I.'' Osprey Publishing. ISBN 978-1-84176-316-3.

{{DEFAULTSORT:Kennedy-Cochran-Patrick, William John Charles}}
[[Category:1896 births]]
[[Category:1933 deaths]]
[[Category:British World War I flying aces]]
[[Category:Companions of the Distinguished Service Order]]
[[Category:Graduates of the Royal Military College, Sandhurst]]
[[Category:People from North Ayrshire]]
[[Category:Recipients of the Military Cross]]
[[Category:Rifle Brigade officers]]
[[Category:Royal Flying Corps officers]]
[[Category:Aviators killed in aviation accidents or incidents]]