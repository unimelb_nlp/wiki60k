{{Infobox person
|name          = Patrick H. Jones
|image         = File:Patrick Henry Jones circa 1860-1870.jpg
|caption       = Jones circa 1860-1870
|birth_name    = Patrick Henry Jones
|birth_date    = {{birth date|1830|11|20}}
|birth_place   = [[County Westmeath, Ireland|County Westmeath]], [[Ireland]]
|death_date    = {{death date and age|1900|7|23|1830|11|20}}
|death_place   = [[Port Richmond, Staten Island|Port Richmond]], [[New York (state)|New York]] 
|death_cause   = [[Cardiac failure]]
|resting_place = St. Peter's Cemetery, [[West New Brighton, New York]]
|resting_place_coordinates = 
|residence     = 
|nationality   = [[Irish-American]]
|other_names   = 
|known_for     = Served as New York City postmaster from 1869-1872; an officer in the "Irish Rifles" and "Hardtack regiment" during the [[American Civil War]]. 
|education     =
|alma_mater    = 
|employer      = 
|occupation    = Lawyer, public servant and postmaster 
|home_town     = [[Cattaraugus County, New York]]
|title         = 
|salary        = 
|networth      = 
|height        = 
|weight        = 
|term          = 
|predecessor   = 
|successor     = 
|party         = [[Republican Party (United States)|Republican]]
|boards        = 
|religion      = [[Catholic]]
|spouse        = 
|partner       = 
|children      = 4 sons
|parents       = 
|relations     = 
|signature     = 
|website       = 
|footnotes     = 
}}

'''Patrick Henry Jones''' (November 20, 1830 – July 23, 1900) was an American lawyer, public servant and [[Postmaster of New York City]] during the mid-to late 19th century. In 1878, he was involved in the [[Alexander T. Stewart]] bodysnatching case when he was contacted by the kidnappers to act as an intermediary between themselves and the Stewart estate. When negotiations stalled between the Stewart family's lawyer [[Henry Hilton]], he assisted Stewart's widow in negotiating for the return of her husband's body.<ref>Lardner, James and Thomas Reppetto. ''NYPD: A City and Its Police''. New York: Henry Holt & Co., 2000. (pg. 80-81) ISBN 978-0-8050-6737-8</ref>

Jones also had a successful military career serving with the [[Union Army]] during the [[American Civil War]], being involved in thirty major battles and countless skirmishes, and reaching the rank of [[Brigadier general (United States)|brigadier general]] before the war's end. He was one of ten [[Irish-Americans]] to become brigade commanders and one of four Irish born officers to become a divisional commander.<ref>Mahin, Dean B. ''The Blessed Place of Freedom: Europeans in Civil War America''. Dulles, Virginia: Brassey's Inc., 2003. (pg. 24-25, 225) ISBN 1-57488-523-5</ref>

==Biography==

===Early life and legal career===
Born in [[County Westmeath, Ireland]] on November 20, 1830, Jones attended grammar school in [[Dublin]] for three years until emigrating with his family to the United States in 1840. They settled on a farm in [[Cattaraugus County, New York]] where Jones would spend most of his childhood. Because of his poor background, he received only a limited education at the Union School in [[Ellicottville]]. In 1850, the 20-year-old Jones became involved in journalism and traveled as a correspondent throughout the Western States for a leading New York journal. He later became the local editor for the ''Buffalo Republic'' and one of the editors of the ''Buffalo Sentinel''.<ref name="Harlow">Harlow, S.R. and S.C. Hutchins. ''Life Sketches of State Officers, Senators, and Members of the Assembly of the State of New York in 1868''. Albany: Weed, Parsons & Company, 1868. (pg. 55-58)</ref>

Eventually, Jones decided to pursue a career in law and later studied under the firm of [[Addison Rice]]. He was admitted to the bar in 1856 and afterwards practiced law with Addison in Ellicottville <ref name="Article">"Obituary. Patrick Henry Jones.". <u>New York Tribune.</u> 25 Jul 1900</ref><ref name="Article2">"Obituary.". <u>New York Sun.</u> 25 Jul 1900</ref><ref name="BDC">Biographical Directory Company. ''Biographical Directory of the State of New York, 1900''. New York: Park Row Building, 1900. (pg. 233)</ref><ref name="CR">Johnson, Alfred S., ed. ''Cyclopedic Review of Current History, 1900''. Vol. 10. Boston: Current History Company, 1900. (pg. 685)</ref><ref name="Warner">Warner, Ezra J. ''Generals in Blue: Lives of the Union Commanders''. Baton Rouge: Louisiana State University Press, 1964. (pg. 254-255) ISBN 0-8071-0822-7</ref> as a full partner. By 1860, he had established himself as one of the most prominent lawyers in [[western New York]]. A lifelong [[Democratic Party (United States)|Democrat]], he became disillusioned by the party's support of [[American Civil War#Secession begins|Southern succession from the Union]] and, in May 1861, decided to join the military in defense of the United States.<ref name="Harlow"/><ref name="UST">[[New York Republican Party]]. ''The Union State Ticket: Personal Character and Military Services. Gallantry Which, Under The First Napoleon, Would Have Made French Marshals''. New York: Baker & Godwin Printers, 1865. (pg. 19-21)</ref>

===Military service===
Upon the outbreak of the [[American Civil War]], Jones readily joined the [[Union Army]]. On July 7, 1861, he enlisted with the [[37th New York Volunteer Infantry Regiment|37th New York Volunteers]], popularly known as the "Irish Rifles", under Colonel [[John McCunn|John H. McCunn]]. He was initially a [[Private (rank)|private]] but quickly found himself elected to the rank of [[second lieutenant#United States|second lieutenant]] by the men in the regiment. He was present at the [[First Battle of Bull Run]], only two weeks after his enlistment, but saw no action as the unit was held in reserve. Jones displayed "gallant conduct" during his first years with the regiment and soon rose in rank to [[first lieutenant#United States|first lieutenant]] and [[adjutant]] on November 4, 1861, and then [[Major (United States)|major]] on January 21, 1862.<ref name="Warner"/><ref name="UST"/><ref name="Eicher">Eicher, John H. and David J. Eicher. ''Civil War High Commands''. Stanford, California: Stanford University Press, 2001. (pg. 324) ISBN 0-8047-3641-3</ref> He and the "Irish Rifles" later joined General [[Samuel P. Heintzelman]]'s [[III Corps (ACW)|III Corps]], as part of the 3rd Brigade of the 3rd Division under Brigadier General [[Philip Kearny]], during the [[Peninsula campaign]] and participated in [[Battle of Williamsburg|Williamsburg]], [[Seven Pines]] and the [[Seven Days Battles|Seven Days campaign]]. Jones was also present when the 37th New York Volunteers, then attached to the 3rd Brigade of the 1st Division of the [[III Corps (ACW)|III Corps]], were at the [[Second Battle of Bull Run]].<ref name="Harlow"/> In May and June, he began suffering from [[malaria]] and was hospitalized in [[Washington, D.C.]] on September 26 remaining there until the end of October.<ref name="Welsh">Welsh, Jack D. ''Medical Histories of Union Generals''. Kent, Ohio: Kent State University Press, 2005. (pg. 184-185) ISBN 0-87338-853-4</ref> He was also briefly an officer to Major General [[Franz Sigel]] prior to the retirement of General [[Ambrose Burnside]] and the reorganization of the [[Army of the Potomac]].<ref name="Harlow"/>

On October 8, 1862, Jones was promoted to the rank of [[colonel]] and transferred to the [[154th New York Volunteer Infantry Regiment|154th New York Volunteers]],<ref name="Eicher"/> or the "Hardtack regiment", then under command by his former law partner Colonel Addison Rice. Rice had founded the regiment and, within two months of its arrival in Northern Virginia, he turned over his command to Jones.<ref>Dunkelman, Mark H. ''War's Relentless Hand: Twelve Tales of Civil War Soldiers''. Baton Rouge: Louisiana University Press, 2006. (pg. 24-25) ISBN 978-0-8071-3190-9</ref> The 154th was part of General [[Oliver O. Howard]]'s [[XI Corps (ACW)|XI Corps]] during the [[Battle of Chancellorsville]], attached to the 1st Brigade of the 2nd Division commanded by Brigadier General [[Adolph von Steinwehr]], where Jones was wounded in the right hip when his unit was ambushed and overrun by Confederate troops under [[Stonewall Jackson|Thomas "Stonewall" Jackson]] resulting in his capture. Jones remained a [[prisoner-of-war]] for five months until being released in a [[prisoner exchange]] in October 1863;<ref name="Warner"/><ref name="Eicher"/> other accounts claim he was paroled for medical reasons <ref name="UST"/> on May 15 and again hospitalized in Washington during June where had an attack of [[amaurosis]] two months later. After his release from hospital in [[Anapolis, Maryland]], Jones was placed in charge of paroled prisoners before returning to active duty <ref name="Welsh"/> and accompanied the XI Corps upon being transferred to [[Tennessee]].<ref name="Harlow"/><ref name="Eicher"/> The unit was present at [[Chattanooga]] during the [[Battle of Missionary Ridge|assault on Missionary Ridge]] the next month, however they did not participate in the battle.<ref name="Warner"/>

In April 1864, Jones took a brief leave of absence to recover his health. When the XI and [[XII Corps (Union Army)|XII Corps]] were combined to form the Major General [[William T. Sherman]]'s [[XX Corps (ACW)|XX Corps]] during that spring, although still in poor health, Jones accepted command of the 2nd Brigade of the 2nd Division ("Geary's White Stars") during the [[Atlanta campaign]], the [[Sherman's March to the Sea|March to the Sea]] and the [[Carolinas Campaign|Carolinas campaign]].<ref name="UST"/> On May 8, he was seriously injured in a skirmish at Buzzard's Roost (Mill Creek Gap) when his horse fell off a cliff during the [[Battle of Rocky Face Ridge]].<ref name="Eicher"/> He was cleared to return to duty a month later, however his surgeon recommended "a change of climate and mode of living" allowing Jones to take another sick leave in December. He resumed his command during the final weeks of the campaign and was present at the surrender of Confederate General [[Joseph E. Johnston]] in March 1865.<ref name="Harlow"/><ref name="Warner"/><ref name="Eicher"/>

On April 18, Jones accepted a commission as [[brigadier general (United States)|brigadier-general]] on the personal recommendations of Generals [[Joseph Hooker|Joseph "Fighting Joe" Hooker]] and Oliver Howard. President [[Abraham Lincoln]] telegraphed General Sherman to inquire about their requests to which Sherman praised his "gallant services in the field" during the campaign.<ref name="UST"/> Rejoining his old unit at [[Goldsboro, North Carolina]], he remained at that rank for the rest of the war and resigned on June 17, 1865.<ref name="Harlow"/><ref name="Warner"/><ref name="Eicher"/><ref name="Welsh"/>

===Stewart bodysnatching case===
After his resignation, Jones resumed his law practice in Ellicottville and in [[New York state election, 1865|November 1865]] was elected on the [[New York Republican Party|Republican]] ticket as [[Clerk of the New York Court of Appeals]]. He remained in this office until December 31, 1868.<ref name="Harlow"/><ref name="UST"/> On August 13, 1868, Jones was appointed by Gov. [[Reuben E. Fenton]] as [[Register of New York City]] (the local [[recorder of deeds]]) to fill the vacancy caused by the death of [[Charles G. Halpine]], remaining in this office until the end of the year with the understanding that the fees of the office be paid over to Halpine's widow.<ref>[https://query.nytimes.com/mem/archive-free/pdf?res=9B06E6DC1E3CE13BBC4C52DFBE668383679FDE ''Appointment of Register of the City of New-York In Place of the Late Gen. Halpine''] in NYT on August 14, 1868</ref> On April 1, 1869, Jones was appointed by President [[Ulysses S. Grant]] as [[Postmaster of New York City]]  and was eventually succeeded by [[Thomas L. James]] upon his resignation in 1872.<ref name="Eicher"/> From 1875 to 1877, Jones was again Register of New York City, elected in November 1874 on the Republican ticket. Afterwards he resumed his private practice.<ref name="BDC"/><ref name="CR"/>

In January 1877, Jones was contacted by Henry G. Romaine requesting that he act as an intermediary between himself and the Stewart estate. Romaine had been involved in stealing the body of [[Alexander T. Stewart]] three months earlier and was demanding a ransom from his widow Cornelia. In several signed letters to Jones, Romaine offered the return of Stewart's body for $250,000 and was instructed to contact him through [[personal ads]] in the ''[[New York Herald]]''. He also received a small packet, express delivered from Canada, which included the knobs and two of the silver handles from Stewart's casket as well as a small strip of velvet and triangular piece of paper. Jones immediately reported the incident to the NYPD Chief of Police [[George W. Walling]] and, after a meeting with Walling and [[Henry Hilton]], Jones agreed to proceed in the negotiations.<ref name="Asbury">[[Herbert Asbury|Asbury, Herbert]]. ''The Gangs of New York: An Informal History of the New York Underworld''. New York: Alfred A. Knopf, 1928. (pg. 203-204) ISBN 1-56025-275-8</ref><ref name="Walling">Walling, George W. ''Recollections of a New York Chief of Police: An Official Record of Thirty-eight Years as Patrolman, Detective, Captain, Inspector and Chief of the New York Police''. New York: Caxton Book Concern, 1887. (pg. 230-234)</ref>

On February 5, Jones placed a personal ad offering to open negotiations. A letter was sent six days later, postmarked [[Boston]], in which Romaine offered the return of Stewart's body on several conditions which included payment of $250,000 in cash, that the body would be delivered to an arranged spot 25 miles from [[Montreal, Canada]] only to Jones and Hilton, Jones would hold the money to turn it over to a representative and for "both parties to maintain forever an unbroken silence in regard to the transaction". Hilton refused to agree to these terms and broke off negotiations. Jones was then told to deliver these demands to Stewart's widow himself, but Jones refused to participate any further.<ref name="Asbury"/><ref name="Walling"/>

A year later, after further negotiations by Hilton had failed, Jones was approached by Stewart's widow to negotiate on her behalf. Romaine agreed to return the body for $100,000 and, while the distraught widow favored to accept the offer, Jones persuaded her to allow him to continue negotiations and was eventually able to get Romaine to accept an offer of $20,000. He later oversaw the delivery of the ransom money as well as the return of the body in [[Westchester County, New York]].<ref name="Eicher"/><ref name="Asbury"/><ref name="Walling"/>

===Later years===
Jones suffered serious medical problems in his old age, specifically [[deafness]] and [[diarrhea|chronic diarrhea]], which his physician blamed on his exposure to artillery fire and his time in the swamps along the [[Chickahominy River]] during his military service. He also suffered from constant pain in his [[sciatic nerve|right sciatic nerve]], attributed to his old war wound suffered at Chancellorsville, as well as his wound in the [[gluteal region]]. Almost every spring and fall, he suffered from [[chills]] and [[fever]] lasting a week or two and confined him to bed. He also had occasional episodes of [[jaundice]] for which he took [[calomel]], [[milk of magnesia|citrus of magnesia]] and an unknown type of Indian medicine.<ref name="Welsh"/>

In October 1886, a medical examination showed that the scar where the bullet had entered, located two inches below the [[greater trochanter|trochanter major]], was the "size of a [[Dime (United States coin)|ten-cent piece]]" while the exit wound four inches back and two inches below the first scar. His [[liver]] and [[spleen]] were both enlarged by [[percussion (medicine)|percussion]], his skin looked [[anemic]], the [[conjunctiva]] of his eyes were yellowish, and had suffered [[emaciated|considerable weight loss]].<ref name="Welsh"/>

His condition had worsened after another examination in August 1898 and his health continued to decline over the next two years. In early-July 1900, Jones began suffering from severe [[gastroenteritis]]. His condition did not respond to therapeutic treatments as he lost control of his bowels and was unable to keep down solid food, living on [[scalded milk]] and [[brandy]]. On July 18, 1900, Jones died from [[cardiac failure]], indirectly caused by his gastroenteritis, at his home in [[Port Richmond, New York]].<ref name="Welsh"/> His funeral was held at St. Mary's Church two days later and then buried in St. Peter's Cemetery. Jones was survived by his wife and four sons.<ref name="Article"/><ref name="Article2"/>

==See also==
{{portal|American Civil War}}
*[[List of American Civil War generals#Union-J|List of American Civil War generals]]

==References==
{{Reflist}}

==Further reading==
* Dunkelman, Mark H. ''Patrick Henry Jones: Irish American, Civil War General, and Gilded Age Politician'' (Louisiana State University Press, 2015). 251 pp.
*Dunkelman, Mark H. and Michael Winey. ''The Hardtack Regiment: An Illustrated History of the 154th Regiment, New York State Infantry Volunteers''. Rutherford, New Jersey: Fairleigh Dickinson University Press, 1981. ISBN 0-8386-3007-3
*Dunkelman, Mark H. ''Brothers One and All: Esprit de Corps in a Civil War Regiment''. Baton Rouge: Louisiana State University Press, 2006. ISBN 0-8071-2978-X
*Fanebust, Wayne. ''The Missing Corpse: Grave Robbing a Gilded Age Tycoon''. Westport, Connecticut: Greenwood Publishing Group, 2005. ISBN 0-275-98762-0

==External links==
*[http://www.civilwarinteractive.com/Biographies/BiosPatrickHenryJones.htm CivilWarInteractive.com - Patrick Henry Jones]
*{{Find a Grave|5892265|accessdate=2009-05-06}}

{{s-start}}
{{s-legal}}
{{succession box | before = [[Frederick A. Tallmadge]] | title = [[Clerk of the New York Court of Appeals|Clerk of the Court of Appeals]] | years = 1866–1868 | after = [[Edwin O. Perrin]]}}
{{s-end}}

{{ClerkNYSCoAppeal}}

{{DEFAULTSORT:Jones, Patrick Henry}}
[[Category:1830 births]]
[[Category:1900 deaths]]
[[Category:American Civil War prisoners of war]]
[[Category:American people of Irish descent]]
[[Category:Criminals from New York City]]
[[Category:People from Manhattan]]
[[Category:Union Army generals]]
[[Category:Irish emigrants to the United States (before 1923)]]
[[Category:Clerks of the New York Court of Appeals]]
[[Category:Postmasters of New York City]]