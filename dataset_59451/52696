{{About|the definition of the terms ''publish'' and ''publication'' in copyright law|more general discussion|publishing}}
[[File:PrintMus 038.jpg|thumb|220px|Printing]]

To '''publish''' is to make [[Content (media)|content]] available to the [[general public]].<ref name="bc">[http://www.wipo.int/treaties/en/ip/berne/trtdocs_wo001.html#P98_14701 Berne Convention, article 3(3)]. URL last accessed 2010-05-10.</ref><ref name="ucc">[http://ipmall.info/hosted_resources/lipa/copyrights/The%20Universal%20Copyright%20Convention%20_Geneva%20Text--September.pdf Universal Copyright Convention, Gevena text (1952), article VI]. URL last accessed 2010-05-10.</ref> While specific use of the term may vary among countries, it is usually applied to text, images, or other [[audio-visual]] content on any traditional medium, including paper ([[newspaper]]s, [[magazine]]s, [[Mail-order catalog|catalog]]s, etc.). The word '''''publication''''' means the act of [[publishing]], and also refers to any printed copies.

==Legal definition and copyright==
"Publication" is a [[wikt:term of art|technical term]] in legal contexts and especially important in [[copyright law|copyright legislation]].  An author of a work generally is the initial owner of the [[copyright]] on the work. One of the copyrights granted to the author of a work is the exclusive right to publish the work.

In the [[United States]], publication is defined as:
:the distribution of copies or phonorecords of a work to the public by sale or other transfer of ownership, or by rental, lease, or lending. The offering to distribute copies or phonorecords to a group of people for purposes of further distribution, public performance, or public display, constitutes publication. A public performance or display of a work does not of itself constitute publication.
:To perform or display a work "publicly" means &ndash;
::(1) to perform or display it at a place open to the public or at any place where a substantial number of people outside of a normal circle of a family and its social acquaintances is gathered; or
::(2) to transmit or otherwise communicate a performance or display of the work to a place specified by clause (1) or to the public, by means of any device or process, whether the members of the public capable of receiving the performance or display receive it in the same place or in separate places and at the same time or at different times.
:&mdash;[[:s:United States Code/Title 17/Chapter 1/Section 101#publication|17 USC 101]]

:The US Copyright Office provides further guidance in Circular 40 [http://copyright.gov/circs/circ40.pdf], which states: "When the work is reproduced in multiple copies, such as in reproductions of a painting or castings of a statue, the work is published when the reproductions are publicly distributed or offered to a group for further distribution or public display".

Generally, the right to publish a work is an exclusive right of copyright owner ([[:s:United States Code/Title 17/Chapter 1/Sections 105 and 106|17 USC 106]]), and violating this right (e.g. by disseminating copies of the work without the copyright owner's consent) is a [[copyright infringement]] ([[:s:United States Code/Title 17/Chapter 5/Section 501|17 USC 501(a)]]), and the copyright owner can demand (by suing in court) that e.g. copies distributed against his will be confiscated and destroyed ([[:s:United States Code/Title 17/Chapter 5/Sections 502 and 503|17 USC 502, 17 USC 503]]). Exceptions and limitations are written into copyright law, however; for example, the exclusive rights of the copyright owner eventually expire, and even when in force, they don't extend to publications covered by [[fair use]] or certain types of uses by libraries and educational institutions.

The definition of "publication" as "distribution of copies to the general public with the consent of the author" is also supported by the [[Berne Convention for the Protection of Literary and Artistic Works|Berne Convention]], which makes mention of "copies" in article 3(3), where "published works" are defined.<ref name="bc"/> In the [[Universal Copyright Convention]], "publication" is defined in article VI as "the reproduction in tangible form and the general distribution to the public of copies of a work from which it can be read or otherwise visually perceived."<ref name="ucc"/> Many countries around the world follow this definition, although some make some exceptions for particular kinds of works. In Germany, §6 of the ''Urheberrechtsgesetz'' additionally considers works of the visual arts (such as sculptures) "published" if they have been made permanently accessible by the general public (i.e., erecting a sculpture on public grounds is publication in Germany).<ref name="de">[http://bundesrecht.juris.de/urhg/__6.html German UrhG, §6], in German. URL last accessed 2007-05-29.</ref> Australia and the UK (as the U.S.) do not have this exception and generally require the distribution of copies necessary for publication. In the case of sculptures, the copies must be even three-dimensional.<ref name="au">[http://www.comlaw.gov.au/ComLaw/Legislation/ActCompilation1.nsf/bodylodgmentattachments/62632B5B1514AEB0CA2570DC000DF45C?OpenDocument#para2.454 Australian Copyright Act, section 29: Publication]. URL last accessed 2007-05-29.</ref><ref name="uk">[http://www.opsi.gov.uk/acts/acts1988/Ukpga_19880048_en_11.htm#mdiv175 Copyright, Designs and Patents Act 1988 (c. 48), section 175], [[Copyright law of the United Kingdom]]. URL last accessed 2007-05-29.</ref>

==Biological classification==
{{Unreferenced section|date=October 2008}}
In [[biological classification]] ([[Taxonomy (biology)|taxonomy]]), the ''publication'' of the description of a [[taxon]] has to comply with some rules. The definition of the "publication" is defined in [[nomenclature codes]]. Traditionally there were the following rules:
*The publication must be generally available.
*The date of publication is the date the published material became generally available.

[[Electronic publication]] with some restrictions is permitted for publication of scientific names of fungi since 1 January 2013.<ref name="Hawksworth 2011">{{cite journal | last1 = Hawksworth | first1 = D. L.  | year = 2011 | title = A new dawn for the naming of fungi: impacts of decisions made in Melbourne in July 2011 on the future publication and regulation of fungal names | url = | journal = [[MycoKeys]] | volume = 1 | issue = | pages = 7–20 | doi = 10.3897/mycokeys.1.2062 }}</ref>

==Types of publication==
{{expand section|date=September 2016}}

===Material types===
There is an enormous variety of material types of publication, some of which are:
*[[Book]]: Pages attached together between two covers, to allow a person to read from or write in.
*[[Breaking News|Bulletin]]: Information written in short on a flyer or inside another publication for public viewing.  Bulletins are also brief messages or announcements broadcast to a wide audience by way of TV, radio, or internet.
*[[Book]]let: Leaflet of more than one sheet of paper, usually attached in the style of a book.
*[[Broadside (printing)|Broadside]]: A large single sheet of paper printed on one side, designed to be plastered onto walls. Produced from 16th - 19th cent. Became obsolete with the development of newspapers and cheap novels.
*[[Flyer (pamphlet)|Flyer]] or handbill: A small sheet of paper printed on one side, designed to be handed out free
*[[Folded leaflet|Leaflet]]: Single sheet of paper printed on both sides and folded.
*[[Journal]]: A book with blank pages inside, to allow you to write down any personal information.  Another word for a newspaper or similar publication.
*[[Newsletter]]: A bulletin, leaflet, pamphlet, or newspaper distributed to a specific audience. 
*[[Newspaper]]: A publication of several pages printed with news, sports, information, and advertising. Newspapers may be published and distributed daily, weekly, monthly, quarterly, or annually. 
*[[Magazine]]: A book with front and back paper covers, printed with information and advertising.  Some magazines are published and distributed every week or every month.
*[[Pamphlet]]: Can be a leaflet, booklet or saddle-stapled booklet.

<ref>Some information courtesy of Merriam-Webster, ©2016. http://www.merriam-webster.com</ref>

===Content types===
Types of publication can also be distinguished by content:
* [[Brochure]]: an informative document made for advertizing products or services, usually in the form of a pamphlet or leaflet.
* [[Tract (literature)|Tract]]: a religious or political argument written by one person and designed to be distributed free, usually in the form of a booklet or pamphlet, but sometimes longer.
* [[Monograph]]: a long research publication written by one person.

==Unpublished works==
A work that has not undergone publication, and thus is not generally available to the public, or for [[citation]] in scholarly or legal contexts, is called an '''unpublished work'''. In some cases unpublished works are widely cited, or circulated via informal means.<ref>{{cite web |url= http://linguistics.byu.edu/faculty/henrichsenl/apa/APA14.html |title=APA REFERENCE STYLE: Unpublished Sources |work=linguistics.byu.edu |year=2002 |accessdate=7 March 2012}}</ref> An [[author]] who has not yet published a work may also be referred to as being unpublished.

The status of being unpublished has specific significance in the legal context, where it may refer to the [[non-publication of legal opinions in the United States]]

==References==
{{reflist|30em}}

==External links==
{{Wiktionary|publication}}
*[http://papers.ssrn.com/sol3/papers.cfm?abstract_id=897643 RayMing Chang, Publication Does Not Really Mean Publication: The Need to Amend the Definition of Publication in the Copyright Act, 33 AM. INTELL. PROP. L. ASS'N  Q.J. 225]: This article analyzes the definition of publication in the U.S. [[Copyright Act of 1976]] and finds strong support for the proposition that electronic dissemination (e.g., "Internet publishing") of works does not result in publication under American copyright law.  This article argues that the definition of publication needs to be amended to explicitly include electronic dissemination.

[[Category:Publications|*]]