{{About||the form of Greek language|Pontic Greek|one of the language families in Caucasus|Northwest Caucasian languages|other uses|Pontic (disambiguation)}}
{{Infobox language family
|name=Pontic 
|acceptance=controversial
|region=Eurasia
|familycolor=superfamily
 |family   = Proposed language family
|child1=[[Northwest Caucasian languages|Northwest Caucasian]]
|child2=[[Indo-European languages|Indo-European]]
|glotto=none
}}

'''Pontic''' is a proposed [[language family]] or [[macrofamily]], comprising the [[Indo-European languages|Indo-European]] and [[Northwest Caucasian languages|Northwest Caucasian]] language families, with '''Proto-Pontic''' being its reconstructed [[proto-language]].

==History of the proposal==
The internal reconstruction of the Indo-European proto-language done by [[Émile Benveniste]] and [[Winfred P. Lehmann]] has set Proto-Indo-European (PIE) typologically quite apart from its daughters. In 1960, Aert Kuipers noticed the parallels between a Northwest Caucasian language, [[Kabardian language|Kabardian]], and PIE. It was Paul Friedrich in 1964, however, who first suggested that PIE might be [[phylogenetically]] related to [[North Caucasian languages|Proto-Caucasian]]. In 1981, Colarusso examined typological parallels involving consonantism, focusing on the so-called [[Laryngeal theory|laryngeals]] of PIE and in 1989, he published his reconstruction of Proto-Northwest Caucasian (PNWC). Eight years later, the first results of his comparative work on PNWC and PIE were published in his article ''Proto-Pontic: Phyletic Links Between Proto-Indo-European and Proto-Northwest Caucasian'', an event which may be considered the actual beginning of the hypothesis.

==Evidence==

Examples of similarities that have been noted include:

* Nasal negating particles in both families:
** [[PIE]] ''*n-'': [[Germanic languages|Germanic]] ''un-'', [[Romance language|Romance]] ''in-'',  [[Russian language|Russian]] ''ne-''.
** NWC: [[Ubykh language|Ubykh]] ''m-'', [[Abkhaz language|Abkhaz]] ''m-''.
* A [[Grammatical case|case]] variously named "accusative", "oblique" or "objective", marked with nasal suffixes:
** [[PIE]] accusative ''*-m'', reflected e.g. in [[Latin]] ''luna'' 'moon' (nom.) vs ''lunam'' (acc.), or Ancient Greek ''ἄνθρωπος'' (''anthropos, ''nom.) vs. ''ἄνθρωπον'' (''anthropon'', acc.).
** NWC: Ubykh ''k<sup>w</sup>æy'' 'well (water source)' ([[absolutive case|abs.]]) vs ''k<sup>w</sup>æyn'' ([[oblique case|obl.]]).

==References==
* {{cite journal | author=Colarusso, John | authorlink = John Colarusso | title=Proto-Pontic: Phyletic links between Proto-Indo-European and Proto-Northwest Caucasian | journal=Journal of Indo-European Studies|year=1997|volume=25|pages=119&ndash;51}}

[[Category:Proposed language families]]
[[Category:Indo-European linguistics]]
[[Category:Pre-Indo-Europeans]]
[[Category:Indo-European languages]]