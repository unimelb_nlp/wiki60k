The '''Atlas of Genetics and Cytogenetics in Oncology and Haematology''', created in 1997 by [[Jean-Loup Huret]] (with bioinformatics by Philippe Dessen) is a collection of resources on [[gene]]s, [[Chromosome abnormality|chromosomes anomalies]], [[leukemia]]s, [[solid tumour]]s, and [[cancer]]-prone diseases. The project is accessible through Internet and is made of encyclopedic-style files, as well as traditional overviews, links towards websites and databases devoted to cancer and/or genetics, case reports in [[haematology]]. It also encompasses teaching items in various languages.<ref>{{cite journal|pmc=29834 | pmid=11125120 | volume=29 | issue=1 | title=Atlas of Genetics and Cytogenetics in Oncology and Haematology, updated |date=January 2001 | journal=Nucleic Acids Res. | pages=303–4 | doi=10.1093/nar/29.1.303}}</ref>

The Atlas is part of the [[genome project]] and participates in research on [[cancer epidemiology]]. It is written and used by practicians, researchers and teachers in genetics, haematology and pathology. The Atlas is also used by students and the general public.<ref>http://www.pedsoncologyeducation.com/GeneralBrainTumorHereditary.asp</ref>

The Atlas is financially supported by many organizations, including the [[Ligue Nationale contre le Cancer]], the [[National Cancer Institute]], the [[European Regional Development Fund]] or the Ministry of Research and Ministry of National Education in France.

==References==
{{reflist}}

==Other articles==
* [[HONcode]]

==Sources==
* Cath Brooksbank. Nature Reviews Cancer 1, 179 (2001); {{doi|10.1038/35106056}} [http://www.nature.com/nrc/journal/v1/n3/full/nrc1201-179b.html]

== External links ==
* [http://atlasgeneticsoncology.org The database] (atlasgeneticsoncology.org)
* [http://nar.oxfordjournals.org/content/29/1/303.abstract http://nar.oxfordjournals.org/content/29/1/303.abstract]
* [http://www.medmatrix.org/_SPages/Oncology.asp On MedMatrix]

[[Category:Genetics journals]]