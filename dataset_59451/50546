{{Infobox journal
| cover = [[File:Antiviral Res cover (2007).gif]]
| discipline = [[Antiviral drug]]s
| abbreviation = Antiviral Res.
| editor = M. Bray
| publisher = [[Elsevier]]
| country =
| frequency = Monthly
| history = 1981–present
| impact = 4.909
| impact-year = 2015
| openaccess =
| website = http://www.journals.elsevier.com/antiviral-research/
| link1 = http://www.sciencedirect.com/science/journal/01663542
| link1-name = Online access
| ISSN = 0166-3542
| eISSN =
| OCLC = 610588089
}}
'''''Antiviral Research''''' is a monthy [[Peer review|peer-reviewed]] [[medical journal]] published by [[Elsevier]] covering research on all aspects of the development of [[antiviral drug|drugs]], [[vaccine]]s and [[immunotherapy|immunotherapies]] against [[virus]]es of animals and plants. The journal was established in 1981 and is an official publication of the [[International Society for Antiviral Research]]. The [[editor-in-chief]] is Mike Bray.

== Abstracting and indexing ==
''Antiviral Research'' is abstracted and indexed in Abstracts on Hygiene and Communicable Diseases, [[Elsevier BIOBASE]], [[BIOSIS]], [[Current Contents]]/Life Sciences, [[EMBASE]], [[MEDLINE]], [[PASCAL (database)|PASCAL]], [[FRANCIS]], [[Science Citation Index]], [[Scopus]], and Tropical Diseases Bulletin. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 3.938.<ref name=WoS>{{cite book |year=2015 |chapter=Antiviral Research |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.journals.elsevier.com/antiviral-research/}}

[[Category:Microbiology journals]]
[[Category:Elsevier academic journals]]
[[Category:Publications established in 1981]]
[[Category:Pharmacology journals]]
[[Category:Monthly journals]]
[[Category:Academic journals associated with learned and professional societies]]