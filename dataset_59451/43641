<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{{Use dmy dates|date=December 2016}}
{{Use British English|date=December 2016}}
{|{{Infobox Aircraft Begin
 | name=Swallow
 | image=Slingsby Swallow LR.jpg
 | caption=Slingsby Type 45 'Swallow'
}}{{Infobox Aircraft Type
 | type=Intermediate [[sailplane]]
 | national origin=[[United Kingdom]]
 | manufacturer=[[Slingsby Aviation|Slingsby Sailplanes Ltd.]]
 | designer=
 | first flight=11 October 1957
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=c.117
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Slingsby Type 45 Swallow''' was designed as a club sailplane of reasonable performance and price. One of the most successful of Slingsby's gliders in sales terms, over 100 had been built when production was ended by a 1968 factory fire.<ref name="JAWA66">{{harvnb|Taylor|1966|p=401}}</ref><ref name="Ell1">{{harvnb|Ellison|1971|pp=265–6}}</ref>

==Design and development==

The Slingsby Swallow was a wooden-framed aircraft, covered in a mixture of plywood and fabric.  Its high mounted, cantilever, straight-tapered and square-tipped wing had 3.3<sup>o</sup> dihedral.  It was Gaboon plywood-skinned and built around a single spruce spar, with a leading-edge torsion box. Its unbalanced ailerons were fabric covered; there were no flaps but dive brakes could be extended in pairs above and below the wings.  The prototype had a 12 m span wing, but all production aircraft had their performance enhanced by an extension to 13.05 m.<ref name="JAWA66"/>

The forward fuselage was a plywood semi-monocoque, with the perspex enclosed cockpit immediately ahead of the wing.<ref name="JAWA66"/>  The strong curvature of the single piece canopy in front of the pilot was later reduced, in Mk.2 aircraft, by extending it forwards.<ref name="Ell2">{{harvnb|Ellison|1971|p=214}}</ref>  In both versions, access was by removal of the canopy and a surrounding fuselage fairing. The whole fuselage was flat sided, but at the rear fabric covering was used. Fixed tail surfaces were plywood skinned and control surfaces fabric covered.<ref name="JAWA66"/>  Fin and rudder were noticeably straight edged, the unbalanced rudder extending down to the keel.  The slightly tapered tailplane was mounted on top of the fuselage and placed far enough forward that the rudder hinge was behind the elevator trailing edge, so that no cutout for rudder movement was needed.<ref name="Ell2"/>  The Swallow used a conventional glider undercarriage, a combination of a rubber sprung skid from nose to below the wing leading edge, plus a fixed, unsprung monowheel below mid wing and a small skid at the rear.<ref name="JAWA66"/>

==Operational history==

The Swallow flew for the first time on 11 October 1957<ref name="Ell2"/> and it remained in production for 11 years.<ref name="Ell1"/>  About 115-120 Swallows were completed,<ref name="Ell1"/><ref name ="Caw">[http://rcawsey.co.uk/t45.htm List of Swallows]</ref> though two were destroyed at the factory in a fire.  Part of the uncertainty lies with kits issued by Slingsby for construction abroad. The short-span first prototype was later rebuilt as the [[Reussner Swift]].  The [[RAF]] used five Swallows, known as Swallow T.X. Mk.1, in its [[Air Training Corps]].  Approximately nine Swallows were used by branches of the [[Royal Air Force Gliding and Soaring Association]] at airfields across the world,<ref name="Ell1"/> and the equivalent [[Royal Navy]] association had four.<ref name ="Caw"/>  25 were sold in Spain, nine in Pakistan and four in Burma.  Most of the rest flew with clubs in the UK, though a few went to [[Commonwealth of Nations|Commonwealth]] countries and the USA.
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications ==
{{Aircraft specs
|ref={{harvnb|Taylor|1966|p=401}}The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs du Monde Volume II,<ref name=Shenstone>{{cite book|last=Shenstone|first=B.S.|title=The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs du Monde Volume II|year=1963|publisher=Organisation Scientifique et Technique Internationale du Vol a Voile (OSTIV) and Schweizer Aero-Revue|location=Zurich|pages=34–36|edition=1st|author2=K.G. Wilkinson |language=English, French, German}}</ref> The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs du Monde<ref name=Shenstone>{{cite book|last=Shenstone|first=B.S.|title=The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs dans Le Monde|year=1958|publisher=Organisation Scientifique et Technique Internationale du Vol a Voile (OSTIV) and Schweizer Aero-Revue|location=Zurich|pages=115–120|edition=1st |author2=K.G. Wilkinson |author3=Peter Brooks|language=English, French, German}}</ref>
|prime units?=kts
<!--
        General characteristics
-->
|crew=1
|length m=7.04
|span m=13.05
|height m=1.58
|wing area sqm=13.55
|wing area sqft=
|wing area note=
|aspect ratio=<!-- sailplanes -->
|airfoil=[[NACA airfoil|NACA 63<sub>3</sub>-618]] at root, [[NACA airfoil|NACA 4412 (mod.)]] at tip<ref name="Ell2"/>
|empty weight kg=193
|empty weight note= (eqipped)
|max takeoff weight kg=318
|more general=
<!--
        Performance
-->
|stall speed kmh=62
|never exceed speed kmh=227
|never exceed speed note=<ref name="Ell2"/><br/>
*'''Rough air speed max:''' {{convert|75|kn|km/h mph|abbr=on|1}}
*'''Aerotow speed:''' {{convert|75|kn|km/h mph|abbr=on|1}}
*'''Winch launch speed:''' {{convert|70|kn|km/h mph|abbr=on|1}}
*'''Terminal velocity:''' with full airbrakes {{convert|118|kn|km/h mph|abbr=on|0}}
|g limits=+5 -2.5 at {{convert|61|kn|km/h mph|abbr=on|1}}
|roll rate=<!-- aerobatic -->
|glide ratio=26:1 at {{convert|43|kn|km/h mph|abbr=on|1}} and {{convert|285|kg|lb|abbr=on|1|disp=flip}}  AUW
|sink rate ms=0.7
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=minimum, at {{convert|36|kn|km/h mph|abbr=on|1}} and {{convert|285|kg|lb|abbr=on|1|disp=flip}}  AUW
|wing loading kg/m2=23.4

|more performance=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=[[List of gliders]]
}}

==In popular culture==
A balsa wood model of the Swallow was featured in [[James May's Toy Stories]] season 1 Christmas special called "Flight Club," which aired on 23 December 2012 on [[BBC HD]].

==References==

===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
*{{cite book|last=Shenstone|first=B.S.|title=The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs du Monde Volume II|year=1963|publisher=Organisation Scientifique et Technique Internationale du Vol a Voile (OSTIV) and Schweizer Aero-Revue|location=Zurich|pages=34–36|edition=1st|author2=K.G. Wilkinson |language=English, French, German}}
*{{cite book|last=Shenstone|first=B.S.|title=The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs dans Le Monde|year=1958|publisher=Organisation Scientifique et Technique Internationale du Vol a Voile (OSTIV) and Schweizer Aero-Revue|location=Zurich|pages=115–120|edition=1st |author2=K.G. Wilkinson |author3=Peter Brooks|language=English, French, German}}
*{{cite book |title= British Gliders and Sailplanes|last=Ellison|first=Norman| year=1971|volume=|publisher=A & C Black Ltd|location=London |isbn=978-0-7136-1189-2|ref=harv}}
*{{cite book |title= Jane's All the World's Aircraft 1966-67|last= Taylor |first= John W R |coauthors= |edition= |year=1966|publisher= Sampson Low, Marston &Co. Ltd|location= London|isbn=|ref=harv }}

{{refend}}

<!-- ==External links== -->
{{Slingsby aircraft}}

[[Category:British sailplanes 1950–1959]]
[[Category:Slingsby aircraft|Swallow]]