{{Orphan|date=May 2014}}

'''Ganesh Chandra Jena''' (born 5 May 1972) is a mountaineer from [[Odisha]], India. He reached the peak of [[Mount Everest]] on 18 May 2011 as the first male from the eastern state of [[Orissa, India|Orissa]].<ref name="orissa" /> Two other Indians in his team were Amit Kumar from Haryana and Jackie Jack from Rajasthan. The team was headed by Anshu Jamsenpa, 32.<ref>{{cite news |date=2011-05-18 |title=India's Ganesh scales Mt. Everest |url=http://zeenews.india.com/news/nation/india-s-ganesh-scales-mt-everest_707295.html |newspaper=zeenews.india.com |accessdate=2014-11-12}}</ref>
After [[Kalpana Dash]], Ganesh Jena achieved this feat as the second Oriya and the first male person from Odisha<ref name="orissa">{{cite web|url=http://orissa.gov.in/e-magazine/orissaannualreference/ORA-2011/pdf/101-103.pdf |title=BIO-DATA OF ODIA EVEREST MOUNTAINEERS - Odisha |publisher=orissa.gov.in |date= |accessdate=2014-05-15 |deadurl=yes |archiveurl=https://web.archive.org/web/20141112194303/http://orissa.gov.in/e-magazine/orissaannualreference/ORA-2011/pdf/101-103.pdf |archivedate=2014-11-12 |df= }}</ref><ref>{{cite web|url=http://www.dailypioneer.com/state-editions/bhubaneswar/marathoner-ganesh-jena-felicitated.html |title=Marathoner Ganesh Jena felicitated |publisher=Dailypioneer.com |date=2013-06-05 |accessdate=2014-11-12}}</ref>
“Willpower, patience and determination hold the key to success. My target is to scale the Everest twice in one season,” he said to media persons after his successful Everest mission.<ref name=telegraph>{{cite web|url=http://www.telegraphindia.com/1110601/jsp/orissa/story_14055042.jsp |title=The Telegraph - Calcutta (Kolkata) &#124; Orissa &#124; Govt felicitates Everest climber |publisher=Telegraphindia.com |date=2011-06-01 |accessdate=2014-11-12}}</ref>

==Personal life==
He was born to Late Kantaru Jena in Sitapur, Paralakhemundi of [[Gajapati district]] in Odisha. Currently he resides in [[Bhubaneshwar]] and serves for [[Bhubaneswar Development Authority]].<ref name=odishasamaya>{{cite web|url=http://odishasamaya.com/news/denied-leave-ganesh-jena-calls-off-mt-kanchenjunga-mission/ |title=Denied leave, Ganesh Jena calls off Mt Kanchenjunga mission |publisher=Odisha Samaya |date=2014-04-06 |accessdate=2014-11-12}}</ref><ref name=orisports/>

He is always inclined towards adventure sports since his youth and moves slowly but steadily towards his goal despite of poor financial conditions. He has participated in many more adventure sports.<ref name=telegraph/><ref name=orisports/>

His mountaineering achievements are:
* 1996 - Mt. [[Bandarpunch]] Peak (21673&nbsp;ft.)
* 1998 & 2005 - Mt. [[Stok Kangri]] Peak (two occasions)
* 1998 - Mt. Ladakhi Peak (5662 m)
* 1998 - Mt. Shetidhar Peak (5293 m)
* 1999 - Mt. [[Mamostong Kangri]] (24,400&nbsp;ft.) Peak
* 1999 - Mt. [[Khardung La]] and Mt. [[Stok Kangri]] Peak
* 2000 - Mt. Gulap Kangri Peak
* 2007 - Mt. Friendship Peak (5340 m)
* 2002 - Mt. [[Saser Kangri]] Peak (7672 m) organised by Y.A.M.A, Chandigarh.
* 18 May 2011 - World's Highest Peak, Mount Everest (8848 m)<ref name="orissa" /><ref name=orisports>{{cite web|url=http://www.orisports.com/PersonDetails.aspx?pId=Mjg1 |title=Orisports.com |publisher=Orisports.com |date= |accessdate=2014-05-15}}</ref>
* 1 August 2015 - Mount Elbrus, Moscow, Russia (5642 m) <ref name=dnaElbrus>{{cite web|url=http://www.dnaindia.com/locality/bhubaneswar/ganesh-jena-becomes-first-odia-scale-mount-elbrus-moscow-66097|title=Ganesh Jena becomes first Odia to scale Mount Elbrus in Moscow |publisher=DNA India|date=2015-08-03|accessdate=2016-07-18}}</ref>

Other Adventure Expeditions to his credit are:
* 2005 - Motor Cycle Expedition (Chandigarh to Khardungla - World's highest Motorable Pass via Kargil, Srinagar and back)
* 1999 – 7&nbsp;km. Adventure Canal Swimming Competition.
* 1999 – 500&nbsp;km Adventure Cycle Expedition from Bhubaneswar to Chandigarh and back.
* 1995 & 1996 – 160&nbsp;km adventure Coastal Trekking from Konark, Odisha to Gopalpur, Odisha organised by Department of Sports & Youth Services.
* May 29, 2013 - participated in Everest Marathon as a single participant from India.<ref name=eodishasamacharElbrus>{{cite web|url=http://eodishasamachar.com/en/mountaineer-ganesh-chandra-jena-to-start-mount-elbrus-expedition-from-july-26/ |title=Mountaineer Ganesh Chandra Jena to start Mount Elbrus expedition from July 26 |publisher=eodishasamachar |date=2015-07-20 |accessdate=2016-07-18}}</ref>

He participates in National Adventure Festival at Chandigarh since 1995. He was Guest Instructor for National Adventure Festival from 2002 to 2009.<ref name="orissa" /><ref name=orisports/>
Ganesh was honoured with ‘Adamaya Sahas Puruskar’ by Haryana government in 2013.<ref>{{cite web|url=http://www.orissadiary.com/CurrentNews.asp?id=47934 |title=Haryana govt felicitates Odisha mountaineer Ganesh Chandra Jena, Odisha Current News, Odisha Latest Headlines |publisher=Orissadiary.com |date=2014-02-12 |accessdate=2014-05-15}}</ref>
He was to begin his Mount Kanchenjunga Mission from April 1, 2014 which was expected to end on May 31. To a great embarrassment to his supporters and the whole state he was denied leave by his employer and he had to abort the mission.<ref name=odishasamaya/>

On July 26, 2015, Ganesh Jena started climbing the 5642 meter height Mount Elbrus in Moscow, Russia. A six-member team from India headed by Ganesh unfurled tiranga (the national flag) at 8:45 PM on 1 August 2015. Earlier in a grand ceremony former Sports and Youth Services director Dr Bimalendu Mohanty and former assistant director Ashok Mohanty flagged off Jena. He is the first Indian climber who did a traverse climbing (to climb a mountain by one route and come down by another) of the Elbrus from South side and returned back to North side.<ref name=dnaElbrus/><ref name=eodishasamacharElbrus/><ref name=orissadiaryElbrus>{{cite web|url=http://www.orissadiary.com/CurrentNews.asp?id=60433#sthash.NmuNu0r3.dpuf|title=Odisha mountaineer Ganesh Jena is all set to scale Mount Elbrus in Russia|publisher=orissadiary.com |date=2015-07-22 |accessdate=2015-07-22}}</ref>

==References==
{{Reflist}}

{{DEFAULTSORT:Jena, Ganesh}}
[[Category:1972 births]]
[[Category:Living people]]
[[Category:Oriya people]]