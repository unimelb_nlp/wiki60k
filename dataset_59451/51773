{{Infobox journal
| italic title = force
| title = Journal of Chemical Technology & Biotechnology
| cover = [[File:Journal of Chemical Technology and Biotechnology cover.gif]]
| abbreviation = J. Chem. Technol. Biotechnol.
| formernames = Journal of the Society of Chemical Industry, Journal of Applied Chemistry, Journal of Applied Chemistry and Biotechnology
| discipline = [[Biotechnology]], [[chemical engineering|chemical technology]]
| editor = Jack Melling
| publisher = [[John Wiley & Sons]]
| history = 1882-present
| frequency = Monthly
| impact = 2.349
| impact-year = 2014
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1097-4660
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1097-4660/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1097-4660/issues
| link2-name = Online archive
| ISSN = 0268-2575
| eISSN = 1097-4660
| LCCN = 86641339
| CODEN = JCTBED
| OCLC = 488589990
}}
The '''''Journal of Chemical Technology & Biotechnology''''' is a monthly [[Peer review|peer-reviewed]] [[scientific journal]] that was established in 1882 as the '''''Journal of the Society of Chemical Industry'''''. In 1950 it changed its title to '''''Journal of Applied Chemistry''''' and volume numbering restarted at volume 1. In 1971 the journal changed the title to '''''Journal of Applied Chemistry and Biotechnology''''' and in 1983 it obtained the current title. It covers chemical and biological technology relevant for economically and environmentally sustainable industrial processes. The journal is published by [[John Wiley & Sons]] on behalf of the [[Society of Chemical Industry]].

==Impact factor==
According to the [[Journal Citation Reports]] it received an [[impact factor]] of 2.349, ranking it 72nd out of 162 journals in the category "Biotechnolog & applied Microbiology",<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Biotechnology & applied Microbiology |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition= Sciences |accessdate= |work=Web of Science |postscript=.}}</ref> 54th out of 157 journals in the category "Chemistry, multidisciplinary",<ref name=WoS1>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Chemistry, multidisciplinary |title=2014 Journal Citation Reports |publisher=Thomson Reuters |edition= Sciences |accessdate= |work=Web of Science |postscript=.}}</ref> and 39th out of 134 journals in the category "Engineering, chemical".<ref name=WoS2>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Engineering, chemical |title=2014 Journal Citation Reports |publisher=Thomson Reuters |edition= Sciences |accessdate= |work=Web of Science |postscript=.}}</ref>

== References ==
{{reflist|35em}}

==External links==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1097-4660}}

{{DEFAULTSORT:Journal of Chemical Technology and Biotechnology}}
[[Category:Chemistry journals]]
[[Category:Publications established in 1882]]
[[Category:English-language journals]]
[[Category:Monthly journals]]
[[Category:John Wiley & Sons academic journals]]
[[Category:Biotechnology journals]]