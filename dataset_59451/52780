{{Infobox institute
|name        = Royal Society of Edinburgh
|image       = [[File:Arms of the Royal Society of Edinburgh.jpg|200px]]
|image_name  = 
|image_size  = 250px
|image_alt   = 
|caption     = Arms of the Royal Society of Edinburgh
|latin_name  = 
|motto       = Societas Regalis Edinburgi
|founder     = [[Colin Maclaurin]] and [[Alexander Monro, primus]] (instrumental in founding the Philosophical Society of Edinburgh)<br />[[William Cullen]], [[Alexander Monro, secundus]] and [[Principal William Robertson|William Robertson]] (instrumental in obtaining the royal charter)
|established = 1737 – diverged from the [[Royal Medical Society]]<br />1783 – received royal charter
|mission     = Scotland's National Academy
|focus       = [[science]] and [[technology]]<br />[[The arts|arts]]<br />[[humanities]]<br />[[social science]]<br />[[business]]<br />[[public service]]
|president   = Dame [[Jocelyn Bell Burnell]]
|chairman    = 
|head_label  = Chief Executive
|head        = Dr William Duncan
|faculty     = 
|adjunct_faculty =
|staff       = 34
|key_people  = Prof [[Alan Alexander]], General Secretary
|budget      = £5.9 million
|endowment   = 
|debt        = 
|num_members =Over 1600 Fellows
|subsidiaries = RSE Scotland Foundation<br />RSE Young Academy of Scotland
|owner       = Registered charity No. SC000470
|non-profit_slogan =
|former_name = Philosophical Society of Edinburgh
|location    = [[New Town, Edinburgh|New Town]], [[Edinburgh]], Scotland
|coor        = 
|address     = 22–26 George Street, Edinburgh, EH2 2PQ
|website     = {{URL|rse.org.uk}}
}}
[[File:Trans Royal Society of Edinburgh cover.jpg|thumb|thumb|The cover of a 1788 volume of the journal ''Transactions of the Royal Society of Edinburgh''. This is the issue where [[James Hutton]] published his ''Theory of the Earth''.]]
The '''Royal Society of Edinburgh''' is Scotland's [[national academy]] of science and letters. It is a registered charity, operating on a wholly independent and non-party-political basis and providing public benefit throughout Scotland.  It was established in 1783. {{As of|2014}} it has more than 1,500 Fellows.<ref name="Scotsman 5Feb2014">{{cite news |url=http://www.scotsman.com/news/first-female-chief-for-royal-society-of-edinburgh-1-3295531 |title=First female chief for Royal Society of Edinburgh |first=Chris |last=Marshall |work=The Scotsman |date=5 February 2014 |accessdate=7 December 2016}}</ref>

The Society covers a broader selection of fields than the [[Royal Society]] of London including [[literature]] and [[history]].<ref>[http://www.rse.org.uk/cms/files/archive/rse_nls_collection.pdf List of RSE material] held at the [[National Library of Scotland]]</ref><ref>[http://www.scholarly-societies.org/history/1783rse.html Notes on the Royal Society of Edinburgh from the Scholarly Societies project, University of Waterloo Library] (includes information on the journals of the society)</ref> Fellowship includes people from a wide range of disciplines – science & technology, arts, humanities, medicine, social science, business and public service.

==Presidents of the Royal Society of Edinburgh==
Presidents of the Royal Society of Edinburgh have included:
{{columns-list|2|
#[[Henry Scott, 3rd Duke of Buccleuch|The Duke of Buccleuch]] (1783–1812)
#Sir [[Sir James Hall, 4th Baronet|James Hall]] (1812–1820)
#Sir [[Walter Scott]] (1820–1832)
#Sir [[Thomas Makdougall Brisbane]] (1832–1860)
#[[George Campbell, 8th Duke of Argyll|The Duke of Argyll]] (1860–1864)
#Principal Sir [[David Brewster]] (1864–1868)
#Sir [[Robert Christison]] (1869–1873)
#Sir [[William Thomson, 1st Baron Kelvin|William Thomson]] (later Lord Kelvin) (1873–1878)
#Rev [[Philip Kelland]] (1878–1879)
#[[James Moncreiff, 1st Baron Moncreiff|Lord Moncreiff of Tullibole]] (1879–1884)
#[[Thomas Stevenson]] (1884–1885)
#Sir William Thomson (later Lord Kelvin) (1886–1890)
#Sir [[Douglas Maclagan]] (1890–1895)
#Lord Kelvin (1895–1907)
#Principal Sir [[William Turner (university principal)|William Turner]] (1908–1913)
#Professor [[James Geikie]] (1913–1915)
#Dr [[John Horne]] (1915–1919)
#Professor [[Frederick Orpen Bower]] (1919–1924)
#Sir [[Alfred Ewing]] (1924–1929)
#Sir [[Edward Sharpey Schafer]] (1929–1934)
#Sir [[D'Arcy Wentworth Thompson]] (1934–1939)
#Professor Sir [[Edmund Whittaker]] (1939–1944)
#Professor Sir [[William Wright Smith]] (1944–1949)
#Professor [[James Kendall (chemist)|James Kendall]] (1949–1954)
#Professor [[James Ritchie (naturalist)|James Ritchie]] (1954–1958)
#Professor [[J. Norman Davidson]] (1958–1959)
#Professor Sir [[Edmund Hirst]] (1959–1964)
#Professor J. Norman Davidson (1964–1967)
#Professor [[Norman Feather]] (1967–1970)
#Sir [[Maurice Yonge]] (1970–1973)
#[[John Cameron, Lord Cameron|Lord Cameron]] (1973–1976)
#Professor [[Robert Allan Smith]] (1976–1979)
#[[Sir Kenneth Blaxter]] (1979–1982)
#Sir [[John Atwell]] (1982–1985)
#Sir [[Alwyn Williams (geologist)|Alwyn Williams]] (1985–1988)
#Professor [[Charles Kemball]] (1988–1991)
#Professor Sir [[Alastair Currie]] (1991–1993)
#Dr [[Thomas L. Johnston]] (1993–1996)
#Professor [[Malcolm Jeeves]] (1996–1999)
#Sir [[William Stewart (scientist)|William Stewart]] (1999–2002)
#[[Lord Sutherland of Houndwood]] (2002–2005)
#Sir [[Michael Atiyah]] (2005–2008)
#[[Lord Wilson of Tillyorn]] (2008–2011)
#Sir [[John Peebles Arbuthnott]] (2011–October 2014)
#Dame [[Jocelyn Bell Burnell]] (October 2014–)
}}

==Awards and medals==
===Fellowship===
{{main|Fellow of the Royal Society of Edinburgh|:Category:Fellows of the Royal Society of Edinburgh}}
[[Fellow of the Royal Society of Edinburgh|Fellowship of the Royal Society of Edinburgh]] is an award in its own right<ref>{{cite web|url=http://www-history.mcs.st-and.ac.uk/Societies/FRSEchron.html|title=Fellows of the Royal Society of Edinburgh|publisher=[[MacTutor History of Mathematics archive]]|website=www-history.mcs.st-and.ac.uk|first1=John J. |last1=O'Connor |first2=Edmund F. |last2=Robertson|authorlink2=Edmund F. Robertson|year=2016}}</ref> that entitles [[fellow]]s to use of the [[initialism]] or [[post-nominal letters]] [[FRSE]] in official titles.

===Royal Medals===
The Royal Medals are awarded annually, preferably to people with a Scottish connection, who have achieved distinction and international repute in either Life Sciences, Physical and Engineering Sciences, Arts, Humanities and Social Sciences or Business and Commerce. The Medals were instituted in 2000 by Queen Elizabeth II, whose permission is required to make a presentation. <ref name=medallists> {{cite web|url = http://www.royalsoced.org.uk/665_RoyalMedals.html|title= Royal Medals|publisher= Royal Society of Scotland|accessdate = 4 December 2014}} </ref>

Past winners<ref name=medallists/> include:
{{colbegin||35e,}}
*2014: Professor [[Tom W. B. Kibble|W.B. Kibble]] and Professor [[Richard G. Morris|Richard G.M. Morris]] <ref> {{cite web|url = http://www.royalsoced.org.uk/cms/files/press/2014/Royal-Medallists-and-Prize-Winners-2014.pdf| title= Academic excellence recognised as RSE announces Royal Medals and Prizes|publisher= RSE|accessdate = 4 December 2014}} </ref>
*2013: Sir [[John Cadogan]], Professor Michael Ferguson and Sir [[Ian Wood (businessman)|Ian Wood]] <ref> {{cite web|url = http://www.royalsoced.org.uk/cms/files/press/2013/Prizes_Medals_Press_Release_March_2013l.pdf| title= New RSE Royal Medal lists and Prize Winners Announced|publisher= RSE|accessdate = 4 December 2014}} </ref>
*2012: Professor David Milne and Sir [[Edwin Southern]] <ref> {{cite web| url = http://www.royalsoced.org.uk/cms/files/fellows/bulletin/12Jul.pdf| title= Royal Medals 2012|publisher= RSE|accessdate = 4 December 2014}} </ref>
*2011: [[Helena Kennedy, Baroness Kennedy of The Shaws|Baroness Helena Kennedy]], [[Noreen Murray]] and Professor Desmond Smith <ref> {{cite web|url = http://www.royalsoced.org.uk/cms/files/press/2011/royal_medals.pdf| title= HRH The Duke of Edinburgh to present RSE Royal Medals to Baroness Helena Kennedy and Professor Desmond Smith|publisher= RSE|accessdate = 4 December 2014}} </ref>
*2010: Professor Sir [[Fraser Stoddart]] and Dr James MacMillan
*2009: Sir [[James Mirrlees]], Professor [[Wilson Sibbett]] and Professor [[Karen Vousden]]
*2008: Professor [[Roger Fletcher (mathematician)|Roger Fletcher]], Right Reverend [[Richard Holloway]] and Professor Sir [[David Lane (oncologist)|David Lane]]
*2007: Professor Sir David Carter, Professor John David M H Laver and Sir [[Tom McKillop|Thomas F W McKillop]]
*2006: Sir [[John M. Ball]] and Sir [[David Jack (scientist)|David Jack]]
*2005: Sir [[David Edward]] and Professor [[William G. Hill]]
*2004: Sir [[Philip Cohen (British biochemist)|Philip Cohen]], Sir [[Neil MacCormick]] and Professor [[Robin Milner]]
*2003: Sir [[Paul Nurse]], [[Lord Mackay of Clashfern]] and Sir [[Michael Atiyah]]
*2002: Professor Sir Alfred Cuschieri, Professor Sir [[Alan T. Peacock|Alan Peacock]] and Professor [[John Mallard|John R Mallard]]
*2001: Professor Sir [[James Black (pharmacologist)|James Black]], Professor [[Tom Devine]] and Professor A Ian Scott 
*2000: Professor Sir [[Kenneth Murray (biologist)|Kenneth Murray]], Professor [[Peter Higgs]] and [[Walter Perry|The Rt. Hon The Lord Perry of Walton]]
{{colend}}
===Lord Kelvin Medal===
The Lord Kelvin Medal is the Senior Prize for Physical, Engineering and Informatics Sciences. It is awarded annually to a person who has achieved distinction nationally and internationally, and who has contributed to wider society by the accessible dissemination of research and scholarship. Winners receive a silver medal and are required to deliver a public lecture in Scotland. The award is named after [[William Thomson, 1st Baron Kelvin]] (1824–1907), who was a famous mathematical physicist and engineer, and Professor of Natural Philosophy at the University of Glasgow. Senior Prize-winners are required to have a Scottish connection but can be based anywhere in the world.

===Keith Medal===
{{main|Keith Medal}}
The Keith medal has been historically awarded every four years for a scientific paper published in the society's scientific journals, preference being given to a paper containing a discovery. It is awarded alternately for papers on Mathematics or Earth and Environmental Sciences. The medal was founded in 1827 as a result of a bequest by  Alexander Keith of Dunottar, the first Treasurer of the Society.<ref> {{cite web|url = http://www.royalsoced.org.uk/661_KeithMedal.html|title= Keith Medal|publisher= Royal Society of Scotland|accessdate = 4 December 2014}} </ref>

===Makdougall Brisbane Prize===
{{See|List of Makdougall Brisbane Prize winners}}
The Makdougall Brisbane Prize has been awarded biennially, preferably to people working in Scotland with no more than fifteen years postdoctoral experience, for particular distinction in the promotion of scientific research and is awarded sequentially to research workers in the Physical Sciences, Engineering Sciences and Biological Sciences. The prize was founded in 1855 by Sir Thomas Makdougall Brisbane, the long-serving fourth President of the Society. <ref> {{cite web|url = http://www.royalsoced.org.uk/662_MakdougallBrisbanePrize.html|title= Makdougall Brisbane Prize|publisher= Royal Society of Scotland|accessdate = 4 December 2014}} </ref>

Recipients include:
{{colbegin||35em}}
*1859 - [[Roderick Murchison]]
*1860-62 - [[William Seller]]
*1866-68 - [[Alexander Crum Brown]] and [[Thomas Richard Fraser]] (joint)
*1870-72 - [[George Allman (natural historian)|George Allman]]
*1874-76 - [[Alexander Buchan (meteorologist)|Alexander Buchan]]
*1876-78 - [[Archibald Geikie]]
*1878-80 - [[Charles Piazzi Smyth]]
*1880-82 - [[James Geikie]]
*1884-86 - [[John Murray (oceanographer)|Sir John Murray]]
*1886-88 - [[Archibald Geikie]] (only Fellow twice awarded the prize)
*1888-90 - [[Ludwig Becker (astronomer)|Ludwig Becker]]
*1892-94 - [[James Walker (chemist)|James Walker]]
*1894-96 - [[John Gray McKendrick]]
*1898-1900 - [[Ramsay Traquair]]
*1902-04 - [[John Dougall (mathematician)|John Dougall]]
*1904-06 - [[Jakob Karl Ernst Halm]]
*1908-10 - [[Ernest Wedderburn]]
*1914-16 - Robert Houstoun
*1916-18 - [[Abercrombie Lawson]]
*1918-20 - [[Joseph Wedderburn]]
*1920-22 - [[William Thomas Gordon]] 
*1922-24 - [[Herbert Stanley Allen]] 
*1927 - [[Charles Morley Wenyon]] protozoologist
*1930-32 - [[Alexander Aitken]]
*1934-36 - [[Ernest Masson Anderson]]
*1936-38 - [[David Meredith Seares Watson]]
*1938-40 - [[Edward Lindsay Ince]]
*1940-42 - [[William Wright Smith]]
*1942-44 - [[Max Born]]
*1952-54 - [[William Charles Osman Hill]]
*1954-56 - [[Maurice Yonge]]
*1964-66 - [[Daniel Edwin Rutherford]]
*1966-68 - [[James Norman Davidson]]
*1968-70 - [[Norman Feather]]
*1972-74 - [[David Paton Cuthbertson]]
*1999 - [[Anne Neville]]
*2001 - [[Dario Alessi]]
{{colend}}

==History==
[[File:20160816 Royal So. Edin. front hall.jpg|thumb|Front Hall of  Royal Society of Edinburgh building]]1909–present – 22–24 George Street, purchased from the Edinburgh Life Assurance Company with the assistance of a grant of £25,000 from the [[Scottish Office]]

At the start of the 18th century, [[Edinburgh]]'s intellectual climate fostered many clubs and societies (see [[Scottish Enlightenment]]).  Though there were several that treated the arts, sciences and medicine, the most prestigious was the Society for the Improvement of Medical Knowledge, commonly referred to as the Medical Society of Edinburgh, co-founded by the mathematician [[Colin Maclaurin]] in 1731.

Maclaurin was unhappy with the specialist nature of the Medical Society,<ref>{{cite web|url=http://www-groups.dcs.st-and.ac.uk/~history/Societies/RSE.html|title=The Royal Society of Edinburgh|publisher=School of Mathematics and Statistics, University of St Andrews|accessdate=22 September 2010}}</ref> and in 1737 a new, broader society, the Edinburgh Society for Improving Arts and Sciences and particularly Natural Knowledge was split from the specialist medical organisation, which then went on to become the [[Royal Medical Society]].

The cumbersome name was changed the following year to the Edinburgh Philosophical Society. With the help of [[University of Edinburgh]] professors like [[Joseph Black]], [[William Cullen]] and [[John Walker (naturalist)|John Walker]], this society transformed itself into the Royal Society of Edinburgh in 1783 and in 1788 it issued the first volume of its new journal ''Transactions of the Royal Society of Edinburgh''.<ref>{{cite web |title=Learned Journals |url=https://www.royalsoced.org.uk/555_LearnedJournals.html |publisher=[[The Royal Society of Edinburgh]] |accessdate=28 August 2015}}</ref>

As the end of the century drew near, the younger members such as Sir [[James Hall, 4th Baronet|James Hall]] embraced [[Lavoisier]]'s new nomenclature and the members split over the practical and theoretical objectives of the society. This resulted in the founding of the [[Wernerian Society]] (1808–58), a parallel organisation that focused more upon natural history and scientific research that could be used to improve Scotland's weak agricultural and industrial base. Under the leadership of Prof. [[Robert Jameson]], the Wernerians first founded ''Memoirs of the Wernerian Natural History Society'' (1808–21) and then the ''Edinburgh Philosophical Journal'' (1822), thereby diverting the output of the Royal Society's ''Transactions''. Thus, for the first four decades of the 19th century, the RSE's members published brilliant articles in two different journals. By the 1850s,  the society once again unified its membership under one journal.

During the 19th century the society produced many scientists whose ideas laid the foundation of the modern sciences. From the 20th century onward, the society functioned not only as a focal point for Scotland's eminent scientists, but also the arts and humanities.  It still exists today and continues to promote original research in Scotland.

In February 2014, Dame [[Jocelyn Bell Burnell]] was announced as the society's first female president, taking up her position in October.<ref name="Scotsman 5Feb2014"/>

===Location===
[[File:The Royal Society Building, George Street, Edinburgh.jpg|thumb|The Royal Society building, at the junction of George Street and Hanover Street in the [[New Town, Edinburgh]]]]

The Royal Society has been housed in a succession of locations:<ref name="Waterston">{{cite web|url=http://www.rse.org.uk/cms/files/archive/homes.pdf|title=The Home of the Royal Society of Edinburgh|last=Waterston|first=Charles D|year=1996|work=Extracted from the Year Book, R.S.E., 1996|publisher=The Royal Society of Edinburgh|accessdate=1 July 2012|location=Edinburgh}}</ref>
* 1783–1807 – College Library, [[University of Edinburgh]]
* 1807–1810 – Physicians' Hall, [[George Street, Edinburgh|George Street]]; the home of the [[Royal College of Physicians of Edinburgh]]
* 1810–1826   – 40–42 George Street; shared with the [[Society of Antiquaries of Scotland]] from 1813
* 1826–1908 – the Royal Institution (now called the [[Royal Scottish Academy Building]]) on [[the Mound]]; shared, at first, with the [[Board of Manufactures]] (the owners), the Institution for the Encouragement of the Fine Arts in Scotland and the [[Society of Antiquaries of Scotland]]
* 1908–09 – University premises at High School Yards

==References==
{{reflist|35em}}

{{commons category}}

{{Edinburgh}}

{{DEFAULTSORT:Royal Society Of Edinburgh}}
[[Category:Royal Society of Edinburgh| ]]
[[Category:National academies of sciences|Scotland]]
[[Category:National academies of arts and humanities|Scotland]]
[[Category:1783 establishments in Scotland]]
[[Category:Learned societies of Scotland]]
[[Category:Organisations based in Edinburgh with royal patronage|Society of Edinburgh]]
[[Category:New Town, Edinburgh]]
[[Category:Organisations based in Edinburgh]]
[[Category:Organizations established in 1783]]
[[Category:Organisations supported by the Scottish Government]]
[[Category:Scientific organizations established in 1783]]