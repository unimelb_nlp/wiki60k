<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Nexth
 | image=Aero & Tech Nexth prototype I-X012.jpg
 | caption=Nexth prototype
}}{{Infobox Aircraft Type
 | type=[[Ultralight aviation|European FAI microlight class]] and [[light-sport aircraft]]<ref name="WDLA11" />
 | national origin=[[Italy]]<ref name="WDLA11" />
 | manufacturer=[[Aero & Tech]]<ref name="WDLA11" />
 | designer= [[Morelli Luca]]<ref name="Volare10">[http://www.aeroandtech.com/IT/mediagallery/archive/VOLAREMARZO2012.pdf Vola il NEXT-H Biposto di Aero & Tech], Volare magazine n° 10, March 2012, (page needed)</ref>
 | first flight= February 2012<ref name="WDLA15"/>
 | introduced=2011<ref name="WDLA11"/>
 | retired=
 | status=In production<ref name="Volare10"/>
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built= 
 | program cost= <!--Total program cost-->
 | unit cost= [[Euro|€]]60,000 (forecast price in 2011)<ref name="WDLA11" />
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Aero & Tech Nexth''' (or sometimes '''Next-H'''<ref name="Volare10"/>) is an [[Italy|Italian]] [[ultralight aircraft]] designed by  [[Morelli Luca]]<ref name="Volare10"/> and produced by [[Aero & Tech]] of [[Fossato di Vico]]. Introduced at the Aero show held in [[Friedrichshafen]] in 2011, the aircraft is intended to be supplied as a complete ready-to-fly aircraft.<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 12. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref><ref name="WDLA15">Tacke, Willi; Marino Boric; et al: ''World Directory of Light Aviation 2015-16'', page 12. Flying Pages Europe SARL, 2015. {{ISSN|1368-485X}}</ref>

==Design and development==
The Nexth complies with the [[Fédération Aéronautique Internationale]] microlight rules.<ref name="WDLA11"/><ref name="WDLA15"/> A later version will comply with US [[light-sport aircraft]] rules.<ref name="WDLA11"/><ref name="WDLA15"/>

The design features a cantilever [[mid-wing]], two seats in [[side-by-side configuration]], retractable [[tricycle landing gear]] made from [[titanium]] and a single {{convert|100|hp|kW|0|abbr=on}} [[Rotax 912ULS]] four cylinder, liquid and air-cooled, [[four stroke]] engine in [[tractor configuration]]. Cockpit access is by two gull wing doors hinged at the top.<ref name="WDLA11"/><ref name="WDLA15"/> The aircraft was designed to meet aerobatic category requirements, including +9 and -4.5 [[g-force|g]].<ref name="WDLA15"/><ref name="experience">{{cite web|url = http://www.aeroandtech.com/EN/experience/|title = Why Nexth? |accessdate = 28 January 2015|last = Aero & Tech|year = 2011}}</ref>

The aircraft fuselage is an [[aluminum]] sheet covered steel space frame, based on [[Formula One]] racing construction techniques. Its {{convert|7.90|m|ft|1|abbr=on}} span wing employs [[winglets]] and [[Flap (aircraft)|flaps]]. Fuel is carried in a single fuselage tank and totals {{convert|130|l}}, giving a range of over {{convert|1700|km|mi|0|abbr=on}}.<ref name="WDLA11"/>

Marino Boric, writing in the ''World Directory of Leisure Aviation'' notes that the Nexth [[fuselage]] is an unusual multi-faceted shape reminiscent of stealth fighter design. The prototype was painted black, which adds to the resemblance.<ref name="WDLA11"/><ref name="WDLA15"/>

==Specifications (Nexth) ==
{{Aircraft specs
|ref=Bayerl and Aero & Tech<ref name="WDLA11" /><ref name="experience"/>
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=one passenger
|length m=
|length ft=
|length in=
|length note=
|span m=7.90
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=9.23
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=472.5
|gross weight lb=
|gross weight note=
|fuel capacity={{convert|130|l}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Rotax 912ULS]]
|eng1 type=four cylinder, liquid and air-cooled, [[four stroke]]
|eng1 kw=75<!-- prop engines -->
|eng1 hp=<!-- prop engines -->

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=275
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=245
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=65
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=1700
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=+9/-4.5
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{reflist}}

==External links==
*{{Official website|http://www.aeroandtech.com/EN/}}

{{DEFAULTSORT:Aero and Tech Nexth}}
[[Category:Italian ultralight aircraft 2010–2019]]
[[Category:Single-engine aircraft]]