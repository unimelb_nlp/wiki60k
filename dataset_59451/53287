{{Infobox college coach
| name = Jim Grobe
| image = Jim Grobe.jpg
| alt = 
| caption = 
| sport = [[American football|Football]]
| current_title = 
| current_team = 
| current_conference = 
| current_record = 
| contract =
| birth_date = {{Birth date and age|1952|2|17}}
| birth_place = [[Huntington, West Virginia]]
| death_date = 
| death_place = 
| alma_mater = [[University of Virginia]]
| player_years1 = 1971–1972
| player_team1 = [[Ferrum Panthers football|Ferrum]]
| player_years2 = 1973–1974
| player_team2 = [[Virginia Cavaliers football|Virginia]]
| player_positions = [[Guard (American and Canadian football)|Guard]], [[linebacker]]
| coach_years1 = 1975
| coach_team1 =  [[Virginia Cavaliers football|Virginia]] ([[Graduate assistant|GA]])
| coach_years2 = 1976–1977
| coach_team2 = [[Liberty High School (Bedford, Virginia)|Liberty HS (Bedford, VA)]]
| coach_years3 = 1978
| coach_team3 = [[Emory and Henry Wasps football|Emory and Henry]] (LB)
| coach_years4 = 1979–1983
| coach_team4 = [[Marshall Thundering Herd football|Marshall]] (LB)
| coach_years5 = 1984–1994
| coach_team5 = [[Air Force Falcons football|Air Force]] (LB)
| coach_years6 = 1995–2000
| coach_team6 = [[Ohio Bobcats football|Ohio]]
| coach_years7 = 2001–2013
| coach_team7 = [[Wake Forest Demon Deacons football|Wake Forest]]
| coach_years8 = 2016
| coach_team8 = [[Baylor Bears football|Baylor]]
| overall_record = 117–121–1 (college)
| bowl_record = 4–2
| tournament_record = 
| championships = 1 [[Atlantic Coast Conference|ACC]] (2006)
| awards = [[Associated Press College Football Coach of the Year Award|AP College Football Coach of the Year]] (2006)<br>[[Bobby Dodd Coach of the Year Award]] (2006)<br>[[Sporting News College Football Coach of the Year|Sporting News College Football COY]] (2006)<br>[[Mid-American Conference football awards#Coach of the Year|MAC Coach of the Year]] (1996)<br>[[Atlantic Coast Conference football honors#Coach of the Year|ACC Coach of the Year]] (2006)
| coaching_records = 
}}
'''Jim Britt Grobe''' (born February 17, 1952) is an [[American football]] coach and former player. He was most recently the head football coach at [[Baylor University]].<ref>{{Cite news|url=http://www.baylorbears.com/sports/m-footbl/spec-rel/053016aaf.html|title=Jim Grobe Named Baylor's Acting Head Football Coach|access-date=January 8, 2017}}</ref> From 2001 to 2013, Grobe served as the head footballcoach at [[Wake Forest University]]. In 2006, he was named [[Atlantic Coast Conference football honors#Coach of the Year|ACC Coach of the Year]] by a unanimous vote and [[Associated Press College Football Coach of the Year Award|AP Coach of the Year]] for coaching [[2006 Wake Forest Demon Deacons football team|Wake Forest]] to an 11–2 regular season and the Atlantic Coast Conference Championship.

==Playing career==
Grobe earned his undergraduate degree (B.S.) in education from the [[University of Virginia]] in 1975 and earned a master's degree in guidance and counseling from Virginia in 1978.  As a player at Virginia in 1973 and 1974, Grobe played [[middle guard]] (1973) and [[linebacker]] (1974). He was a two-year starter for the [[Virginia Cavaliers football|Virginia Cavaliers]] and was named Academic All-[[Atlantic Coast Conference|ACC]].

Before enrolling at Virginia, Grobe spent two seasons with [[Ferrum College]], then known as Ferrum Junior College, where he played linebacker on the undefeated Coastal Conference championship team. Grobe earned the Catlin Citizenship Award and the Big Green Award. In the fall of 2002, Grobe was inducted into the Ferrum College Hall of Fame.

==Coaching career==

===Wake Forest===

====2006 season====
In 2006, Grobe led [[2006 Wake Forest Demon Deacons football team|Wake Forest]] to a school record 11 wins with a perfect 6–0 road record.  His Wake Forest team also won the Atlantic Coast Conference championship by virtue of defeating [[2006 Georgia Tech Yellow Jackets football team|Georgia Tech]], 9–6, in the [[2006 ACC Championship Game|conference title game]]. The Demon Deacons earned their first trip to a [[Bowl Championship Series|BCS]] [[bowl game]] and played [[2006 Louisville Cardinals football team|Louisville]] in the [[2007 Orange Bowl|Orange Bowl]].  Grobe was named the [[Atlantic Coast Conference football honors#Coach of the Year|ACC Coach of the Year]], receiving 80 out of 80 votes from the league's media and making him the sixth Wake Forest coach to win the award.  Grobe was also awarded the [[Bobby Dodd Coach of the Year Award]] and the [[Associated Press College Football Coach of the Year Award|AP Coach of the Year]] in 2006.

On February 27, 2007, Grobe signed a 10-year contract extension through 2016.<ref>{{cite web |url=http://sports.espn.go.com/espn/wire?section=ncf&id=2782271 |title=Grobe inks new 10-year contract with Wake Forest |publisher=[[ESPN.com]] |agency=[[Associated Press]] |date=February 27, 2007 |accessdate=September 25, 2010}}</ref>

====Resignation====
Grobe resigned from Wake Forest on December 2, 2013.<ref>{{cite web |url=http://espn.go.com/college-football/story/_/id/10069197/wake-forest-demon-deacons-coach-jim-grobe-resigns |title=Wake Forest's Jim Grobe resigns |author= |date=December 2, 2013 |work= |publisher=[[ESPN.com]] |accessdate=December 2, 2013}}</ref>

===Baylor University===
On May 30, 2016, Grobe was hired as Baylor's head coach for the 2016 season.  Coach Grobe led the Baylor Bears to their 7th consecutive bowl game, the Motel 6 Cactus Bowl in Arizona. Baylor entered the game as heavy underdogs to the Boise St Broncos (10-2 record), but despite being out-manned & overlooked by Vegas odds makers the Bears dominated the Broncos and rolled to an easy 31-12 victory.

==Family==
Grobe and his wife Holly have two sons, Matt and Ben, and three grandchildren. Matt was named Head Coach of the Men's Golf Team at Marshall University in 2012. Ben is Assistant Director of Football Operations at The University of North Carolina at Charlotte.

==Head coaching record==
{{CFB Yearly Record Start | type = coach | team = | conf = | bowl = | poll = both}}
{{CFB Yearly Record Subhead
 | name      = [[Ohio Bobcats football|Ohio Bobcats]]
 | conf      = [[Mid-American Conference]]
 | startyear = 1995
 | endyear   = 2000
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[1995 NCAA Division I-A football season|1995]]
 | name         = [[1995 Ohio Bobcats football team|Ohio]]
 | overall      = 2–8–1
 | conference   = 1–6–1
 | confstanding = 9th
 | bowlname     = 
 | bowloutcome  = 
 | bcsbowl      = 
 | ranking      = 
 | ranking2     = 
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[1996 NCAA Division I-A football season|1996]]
 | name         = [[1996 Ohio Bobcats football team|Ohio]]
 | overall      = 6–6
 | conference   = 5–3
 | confstanding = 4th
 | bowlname     = 
 | bowloutcome  = 
 | bcsbowl      = 
 | ranking      = 
 | ranking2     = 
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[1997 NCAA Division I-A football season|1997]]
 | name         = [[1997 Ohio Bobcats football team|Ohio]]
 | overall      = 8–3
 | conference   = 6–2
 | confstanding = T–2nd <small>(East)</small>
 | bowlname     = 
 | bowloutcome  = 
 | bcsbowl      = 
 | ranking      = 
 | ranking2     = 
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[1998 NCAA Division I-A football season|1998]]
 | name         = [[1998 Ohio Bobcats football team|Ohio]]
 | overall      = 5–6
 | conference   = 5–3
 | confstanding = T–3rd <small>(East)</small>
 | bowlname     = 
 | bowloutcome  = 
 | bcsbowl      = 
 | ranking      = 
 | ranking2     = 
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[1999 NCAA Division I-A football season|1999]]
 | name         = [[1999 Ohio Bobcats football team|Ohio]]
 | overall      = 5–6
 | conference   = 5–3
 | confstanding = T–3rd <small>(East)</small>
 | bowlname     = 
 | bowloutcome  = 
 | bcsbowl      = 
 | ranking      = 
 | ranking2     = 
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[2000 NCAA Division I-A football season|2000]]
 | name         = [[2000 Ohio Bobcats football team|Ohio]]
 | overall      = 7–4
 | conference   = 5–3
 | confstanding = T–3rd <small>(East)</small>
 | bowlname     = 
 | bowloutcome  = 
 | bcsbowl      = 
 | ranking      = 
 | ranking2     = 
}}
{{CFB Yearly Record Subtotal
 | name       = Ohio
 | overall    = 33–33–1
 | confrecord = 27–20–1
}}
{{CFB Yearly Record Subhead
 | name      = [[Wake Forest Demon Deacons football|Wake Forest Demon Deacons]]
 | conf      = [[Atlantic Coast Conference]]
 | startyear = 2001
 | endyear   = 2013
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[2001 NCAA Division I-A football season|2001]]
 | name         = [[2001 Wake Forest Demon Deacons football team|Wake Forest]]
 | overall      = 6–5
 | conference   = 3–5
 | confstanding = 7th
 | bowlname     = 
 | bowloutcome  = 
 | bcsbowl      = 
 | ranking      = 
 | ranking2     = 
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[2002 NCAA Division I-A football season|2002]]
 | name         = [[2002 Wake Forest Demon Deacons football team|Wake Forest]]
 | overall      = 7–6
 | conference   = 3–5
 | confstanding = 7th
 | bowlname     = [[2002 Seattle Bowl|Seattle]]
 | bowloutcome  = W 
 | bcsbowl      = 
 | ranking      = 
 | ranking2     = 
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[2003 NCAA Division I-A football season|2003]]
 | name         = [[2003 Wake Forest Demon Deacons football team|Wake Forest]]
 | overall      = 5–7
 | conference   = 3–5
 | confstanding = 7th
 | bowlname     = 
 | bowloutcome  = 
 | bcsbowl      = 
 | ranking      = 
 | ranking2     = 
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[2004 NCAA Division I-A football season|2004]]
 | name         = [[2004 Wake Forest Demon Deacons football team|Wake Forest]]
 | overall      = 4–7
 | conference   = 1–7
 | confstanding = T–10th
 | bowlname     = 
 | bowloutcome  = 
 | bcsbowl      = 
 | ranking      = 
 | ranking2     = 
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[2005 NCAA Division I-A football season|2005]]
 | name         = [[2005 Wake Forest Demon Deacons football team|Wake Forest]]
 | overall      = 4–7
 | conference   = 3–5
 | confstanding = T–4th <small>(Atlantic)</small>
 | bowlname     = 
 | bowloutcome  = 
 | bcsbowl      = 
 | ranking      = 
 | ranking2     = 
}}
{{CFB Yearly Record Entry
 | championship = conference
 | year         = [[2006 NCAA Division I-A football season|2006]]
 | name         = [[2006 Wake Forest Demon Deacons football team|Wake Forest]]
 | overall      = 11–3
 | conference   = 6–2
 | confstanding = 1st <small>(Atlantic)</small>
 | bowlname     = [[2007 Orange Bowl|Orange]]
 | bowloutcome  = L
 | bcsbowl      = yes
 | ranking      = 17
 | ranking2     = 18
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[2007 NCAA Division I-A football season|2007]]
 | name         = [[2007 Wake Forest Demon Deacons football team|Wake Forest]]
 | overall      = 9–4
 | conference   = 5–3
 | confstanding = T–2nd <small>(Atlantic)</small>
 | bowlname     = [[2007 Meineke Car Care Bowl|Meineke Car Care]]
 | bowloutcome  = W 
 | bcsbowl      = 
 | ranking      = 
 | ranking2     = 
}}
{{CFB Yearly Record Entry
 | championship =
 | year         = [[2008 NCAA Division I-A football season|2008]]
 | name         = [[2008 Wake Forest Demon Deacons football team|Wake Forest]]
 | overall      = 8–5
 | conference   = 4–4
 | confstanding = T–3rd <small>(Atlantic)</small>
 | bowlname     = [[2008 EagleBank Bowl|EagleBank]]
 | bowloutcome  = W 
 | bcsbowl      = 
 | ranking      =
 | ranking2     = 
}}
{{CFB Yearly Record Entry
 | championship =
 | year         = [[2009 NCAA Division I-A football season|2009]]
 | name         = [[2009 Wake Forest Demon Deacons football team|Wake Forest]]
 | overall      = 5–7
 | conference   = 3–5
 | confstanding = 4th <small>(Atlantic)</small>
 | bowlname     = 
 | bowloutcome  = 
 | bcsbowl      = 
 | ranking      =
 | ranking2     = 
}}
{{CFB Yearly Record Entry
 | championship =
 | year         = [[2010 NCAA Division I FBS football season|2010]]
 | name         = [[2010 Wake Forest Demon Deacons football team|Wake Forest]]
 | overall      = 3–9
 | conference   = 1–7
 | confstanding = 6th <small>(Atlantic)</small>
 | bowlname     = 
 | bowloutcome  = 
 | bcsbowl      = 
 | ranking      =
 | ranking2     = 
}}
{{CFB Yearly Record Entry
 | championship =
 | year         = [[2011 NCAA Division I FBS football season|2011]]
 | name         = [[2011 Wake Forest Demon Deacons football team|Wake Forest]]
 | overall      = 6–7
 | conference   = 5–3
 | confstanding = T–2nd <small>(Atlantic)</small>
 | bowlname     = [[2011 Music City Bowl|Music City]]
 | bowloutcome  = L
 | bcsbowl      = 
 | ranking      =
 | ranking2     = 
}}
{{CFB Yearly Record Entry
| championship =
 | year         = [[2012 NCAA Division I FBS football season|2012]]
 | name         = [[2012 Wake Forest Demon Deacons football team|Wake Forest]]
 | overall      = 5–7
 | conference   = 3–5
 | confstanding = 4th <small>(Atlantic)</small>
 | bowlname     = 
 | bowloutcome  = 
 | bcsbowl      = 
 | ranking      =
 | ranking2     = 
}}
{{CFB Yearly Record Entry
| championship =
 | year         = [[2013 NCAA Division I FBS football season|2013]]
 | name         = [[2013 Wake Forest Demon Deacons football team|Wake Forest]]
 | overall      = 4–8
 | conference   = 2–6
 | confstanding = 6th <small>(Atlantic)</small>
 | bowlname     = 
 | bowloutcome  = 
 | bcsbowl      = 
 | ranking      =
 | ranking2     =
}}
{{CFB Yearly Record Subtotal
 | name       = Wake Forest
 | overall    = 77–82
 | confrecord = 42–62
}}
{{CFB Yearly Record Subhead
 | name      = [[Baylor Bears football|Baylor Bears]]
 | conf      = [[Big 12 Conference]]
 | startyear = 2016
 | endyear   = 
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[2016 NCAA Division I FBS football season|2016]]
 | name         = [[2016 Baylor Bears football team|Baylor]]
 | overall      = 7–6
 | conference   = 3–6
 | confstanding = T–6th
 | bowlname     = [[2016 Cactus Bowl (December)|Cactus]]
 | bowloutcome  = W
 | bcsbowl      =
 | ranking      = 
 | ranking2     = 
}}
{{CFB Yearly Record Subtotal
 | name       = Baylor 
 | overall    = 7–6 <!-- As of games through 12/28/16 -->
 | confrecord = 3–6
}}
{{CFB Yearly Record End
 | overall  = 117–121–1 <!-- As of games through 12/28/16 -->
 | bcs      = 
 | poll     = two
 | polltype = 
}}

==Coaching tree==
Assistant coaches under Jim Grobe who became NCAA head coaches:

* [[Troy Calhoun]]: [[Air Force Falcons|Air Force]] (2007–present)
* [[Tim DeRuyter]]: [[Fresno State Bulldogs football|Fresno State]] (2012–2016)
* [[Dean Hood]]: [[Eastern Kentucky Colonels football|Eastern Kentucky]] (2008–2015)
* [[Brian Knorr]]: [[Ohio Bobcats football|Ohio]] (2001–2004)
* [[Brad Lambert]]: [[Charlotte 49ers football|Charlotte]] (2011–present)

==References==
{{Reflist}}

==External links==
{{Portal|Biography|College football}}
* [http://wakeforestsports.collegesports.com/sports/m-footbl/mtt/grobe_jim00.html Wake Forest profile]
* {{CFBCR|925|Jim Grobe}}

{{Ohio Bobcats football coach navbox}}
{{Wake Forest Demon Deacons football coach navbox}}
{{Baylor Bears football coach navbox}}
{{Big 12 Conference football coach navbox}}
{{Navboxes
|title = Jim Grobe—awards and honors
|list1 =
{{AFCA Coach of the Year}}
{{Associated Press College Football Coach of the Year Award}}
{{Bobby Dodd Award winners}}
{{Sporting News College Football Coach of the Year}}
}}

{{DEFAULTSORT:Grobe, Jim}}
[[Category:Living people]]
[[Category:1952 births]]
[[Category:Air Force Falcons football coaches]]
[[Category:American football defensive linemen]]
[[Category:American football linebackers]]
[[Category:Baylor Bears football coaches]]
[[Category:Emory and Henry Wasps football coaches]]
[[Category:Ferrum Panthers football players]]
[[Category:High school football coaches in the United States]]
[[Category:Marshall Thundering Herd football coaches]]
[[Category:Ohio Bobcats football coaches]]
[[Category:Sportspeople from Huntington, West Virginia]]
[[Category:Virginia Cavaliers football coaches]]
[[Category:Virginia Cavaliers football players]]
[[Category:Wake Forest Demon Deacons football coaches]]