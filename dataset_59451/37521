{{Infobox album <!-- See Wikipedia:WikiProject_Albums -->
| Name       = Be Someone Else
| Type       = studio
| Artist     = [[Slimmy]]
| Cover      = Be someone else.jpg
| Alt        = 
| Released   = June 14, 2010<ref name="iTunes">{{Cite web |url=https://itunes.apple.com/pt/album/be-someone-else/id376044988 |title=Be Someone Else on iTunes |publisher=[[iTunes]] |last=|first=|year=2010 |accessdate=7 May 2011}}</ref>
| Recorded   = 2006 - 2010<br>- Estúdios da Aguda, [[Portugal]] <br> - Wrong Planet Studios, United Kingdom<ref name="Ionline">{{Cite web |url=http://www.ionline.pt/conteudo/52260-be-someone-else-segundo-album-slimmy-apresentado-sexta-feira-no-porto |title=Segundo álbum de Slimmy apresentado sexta-feira no Porto |publisher=Ionline |last=|first=|date= |accessdate=7 May 2011}}</ref>
| Genre      = 
| Length     = 38:53 (standard edition) <br> 73:16 (deluxe edition)
| Label      = iPlay
| Producer   = Quico Serrano, [[Mark J Turner]]<ref name="Ionline"/>
| Last album = ''[[Slimmy Unplugged]]'' <br> (2008)
| This album = '''''Be Someone Else''''' <br> (2010)
| Next album = 
| Misc        = 
{{Singles
  | Name           = Be Someone Else
  | Type           = Studio album
  | Single 1       = [[Be Someone Else (song)|Be Someone Else]]
  | Single 1 date  = January 1, 2010
  | Single 2       = [[The Games You Play]]
  | Single 2 date  = March 6, 2010 ([[iTunes]] only)
  | Single 3       = [[I Can't Live Without You In This Town]]
  | Single 3 date  = June 14, 2010 (promo only)
}}
}}
'''''Be Someone Else''''' is the second [[studio album]] by Portuguese [[singer-songwriter]] [[Slimmy]]. [[Saul Davies]] was first set to be the producer of the album, however, such collaboration wasn't possible and [[Slimmy]] started working on the album with his longtime producers Quico Serrano and [[Mark J Turner]]. Other musicians joined Slimmy for the recording of the album: Paulo Garim in the [[Bass guitar|bass]] and Tó-Zé in the [[drums]], who already worked with slimmy in ''Beatsound  Loverboy'', and Gustavo Silva, in the keyboards and Daniel Santos in the guitar as guest musicians. He mentioned that all production process happened because of him and that he was the one with the "last word" on his projects, not the bands or producers that he worked with. Originally due for release on May 2010, the album's release was pushed back to 14 June 2010. The album was released in three formats: the physical standard edition, the physical deluxe edition with 2 discs and the [[Digital data|digital]] format featuring 10 tracks.

Slimmy defined the album as "freedom, with a bit of "teasing" and "sexual", an album full of strong songs dedicated to his fans. The album is essentially a rock album, a completely opposite of ''Beatsound Loverboy'', which features a more [[Electronic music|electronic]] sound, keeping, however, the same connection between [[rock music|rock]] and [[Electro music|electro]] music. In an interview with ''JN'' Slimmy declared that the album feels more organic and less electronic because in ''Beatsound Loverboy'', there was no one else to play the songs but him. While maintaining his irreverence, Slimmy guarantees, however, that what matters is to make music that people intending to sing and lyrics that people can understand. He also said that he already received [[criticism]] for not being a singer with a proper style, but "I try to provide different sensations to people.

Slimmy's most significant [[Promotion (marketing)|promotion]] marked the beginning of "A Very Slimmy Tour", starting on 18 February 2011 at the Kastrus River Klub in Esposende and ending on 30 April 2011 at the Pitch Club in [[Porto]]. Slimmy's "Be Someone Else Tour" began on 6 May 2011 with its opening show at the Academy Week in Mirandela. Neither of the album's singles, "Be Someone Else" and "The Games You Play", were particularly successful, charting anywhere. Critical response to the album was generally favorable, with critics praising Quico Serrano and Mark Turner's ''Be Someone Else''{{'}}s polished production, calling the album more direct and [[humanized]] than ''Beatsound Loverboy''. "Be Someone Else" was a commercial disappointment, the album didn't managed to chart on any official [[chart]] company to date. Two music videos were released from the album: "The Games You Play", produced by Ana Andrade, Carla Fardilha, Clara Araújo Teixeira and Helena Oliveira,  premiered on 17 November 2009 on [[YouTube]] and "Be Someone Else", produced by Riot Films and premiered on 27 June 2010.

==Background and development==

===Production===
[[Slimmy]] started working on the album with his longtime producers Quico Serrano and Mark Turner by the third quarter of 2009.<ref name="Ionline"/> [[Saul Davies]] was first set to be the producer of the album, however, Davies was on tour with the band [[James (band)|James]] at the time, making such collaboration impossible.<ref name="palco principal entrevista">{{Cite web |url=http://palcoprincipal.sapo.pt/noticias/Noticia/slimmy_em_entrevista_as_vezes_chamam_me_o_marylin_manson_portugues/0003450 |title=Slimmy em entrevista: "Às vezes, chamam-me o Marilyn Manson português" |publisher=Palco Principal |last=Novais|first=Sara|date=25 June 2010 |accessdate=7 May 2011}}</ref> Other musicians joined Slimmy for the recording of the album: Paulo Garim in the [[Bass guitar|bass]] and Tó-Zé in the [[drums]], who already worked with slimmy in ''Beatsound  Loverboy'', and Gustavo Silva, in the keyboards and Daniel Santos in the guitar as guest musicians.<ref name="palco principal 2">{{Cite web |url=http://palcoprincipal.sapo.pt/noticias/Noticia/novo_album_de_slimmy_chama_se_be_someone_else_e_chega_em_maio/0002886 |title=Novo álbum de Slimmy chama-se "Be Someone Else" e chega em Maio |publisher=Palco Principal |last=Novais|first=Sara|date=23 March 2010 |accessdate=7 May 2011}}</ref> Slimmy commented that most songs on the album are re-recordings which were recorded in [[Portugal]] in 2006 and 2007,<ref name="Som à letra">{{Cite web |url=http://somaletra.blogspot.com/2010/07/nao-faco-as-coisas-para-chocar.html |title=Slimmy - não faço coisas para chocar |publisher=Som à Letra |last=|first=|date=2 July 2010 |accessdate=8 May 2011}}</ref> however, these suffered some changes during the recording sessions at the Wrong Planet Studios in the [[UK]], to improve their [[sound quality]].<ref name="myspace">{{Cite web |url=https://www.myspace.com/slimmyuk/blog |title=Slimmy's blog |publisher=[[MySpace]] |last=Fernandes|first=Paulo|year=2010 |accessdate=7 May 2011}}</ref>

In an interview with Susana Faria of ''JPN'', Slimmy defined the album as "freedom, with a bit of "teasing" and "sexual", an album full of strong songs dedicated to my fans".<ref name="JPN interview">{{Cite web |url=http://jpn.icicom.up.pt/2010/10/15/slimmy_tens_mesmo_de_dar_mais_de_ti_se_quiseres_ser_alguem_diferente.html |title=Slimmy: "Tens mesmo de dar mais de ti se quiseres ser alguém diferente" |publisher=[[JPN]] |last=Faria|first=Susana|date=15 October 2010 |accessdate=7 May 2011}}</ref> During this same interview, Slimmy talked about the album: {{cquote|"It's funny that "Be Someone Else" reminds me 2001 when I was trying to battle alone against everything and everyone, I couldn't even get members to play with me in the band. Noticed that I just had to give more of me if I wanted to be somebody different. The "Be Someone Else" is intended to be that someone different, not if only because it is different, but because I have a dream. Regard as a message to people to worry less about others and more about themselves and go after what they really want."<ref name="JPN interview"/>}}

===Concept and music===
{{Quote box|quote="This album is about the [[experiences]] that I lived or the places where I've been through, but very timeless experiences." |source=&mdash;[[Slimmy]] during interview with ''Jornal Metro''<ref name="Som à letra"/> |salign=center |width=25% |align=left |style=padding:8px;}}
The album is essentially a rock album, a completely opposite of ''Beatsound Loverboy'', which features a more [[Electronic music|electronic]] sound, keeping, however, the same connection between [[rock music|rock]] and [[Electro music|electro]] music. Slimmy has been influenced by rock artists such as [[Placebo (band)|Placebo]], IAMX and [[Kings of Leon]].<ref name="JPN interview"/> Slimmy explained that ''Be Someone Else'' sounds different from ''Beatsound Loverboy'' because of its complex and structured production.<ref name="JPN interview"/> Slimmy mentioned that ''Be Someone Else'' is less [[individualist]] than ''Beatsound Loverboy'', he declares that this album is like "a message about being who you are", while his previous album talked about his past experiences, specially about his experience in [[London]].<ref name="JPN interview"/> In an interview with ''Palco Principal'', Slimmy was asked if he missed the "One Man Show" times. He declared that "Doesn't matter how big the bands that play with me are, I will always feel like "one", because, in this new record, I was the one who spent nights working on the album, not my band". He also mentioned that all production process happened because of him and that he was the one with the "last word" on his projects, not the bands or producers that he worked with.<ref name="palco principal entrevista"/> During this same interview, Slimmy confessed that is always good to have someone with great ideas to work with, in this case, he was talking about him. He explained: "if there were five persons creating music, there would be a lot of mess, that's why do it all and I'm glad people trust in me for that".<ref name="palco principal entrevista"/>

{{listen
 | filename    = Slimmy I Can't Live Without You In This Town.ogg
 | title       = "I Can't Live Without You In This Town"
 | description = A 23 second sample of Slimmy's "I Can't Live Without You In This Town" featuring the break sung by Slimmy.
 | format      = [[Ogg]]
}}
"I Can't Live Without You In This Town", an electro rock ballad, makes a markable difference from the other tracks, some critics called the [[Refrain|chorus]] of the song memorable.<ref name="Palco Principal"/> The song is dedicated to a girl Slimmy met in [[Texas]] in 2004.<ref name="Som à letra"/> The album also features a song entitled "Together 4ever" remixed by DJ Ride. The song "Beatsound Loverboy", included on the ''Beatsound Loverboy Remixes'' EP, was also remixed by DJ Ride.<ref name="youclubvideo"/>

In an interview with ''JN'' Slimmy declared that the album feels more organic and less [[Electronic music|electronic]] because in ''Beatsound Loverboy'', there was no one else to play the songs but him.<ref name="JN interview"/> He explained that with the trio now completely formed, it is a whole different [[concept]], everything sounds different, and their presence on stage is more [[energy|energetic]] and trustful.<ref name="JN interview"/> Slimmy also declared that SLIMMY is no longer a solo project, but also belongs to those who follow it.<ref name="JN interview">{{Cite web |url=http://www.jn.pt/PaginaInicial/Cultura/Interior.aspx?content_id=1610414&page=-1 |title=Slimmy: "Sinto-me um estrela de rock" |publisher=[[Jornal de Notícias|JN]] |last=R. Alves|first=Tiago|date=5 July 2010 |accessdate=8 May 2011}}</ref> In an interview with ''Jornal Metro'', Slimmy said that the album reflects the maturity, greater stability in the mind. While maintaining his irreverence, Slimmy guarantees, however, that what matters is to "make music that people intending to sing and lyrics that people can understand. I do not do things to shock", he also said that he already received [[criticism]] for not being a singer with a proper style, but "I try to provide different sensations to people. When I sing alive, I sound much more rock, but in this album, there are many different [[spirit]] moods".<ref name="Som à letra"/>

== Release and promotion ==
Originally due for release on May 2010, the album's release was pushed back to 14 June 2010.<ref name="Ionline"/> It was released in three formats: the physical standard edition, featuring 10 tracks;<ref name="Fnac 1">{{Cite web |url=http://www.fnac.pt/Slimmy-Be-Someone-Else-sem-especificar/a323466?PID=6&Mn=-1&Mu=-13&Ra=-28&To=0&Nu=2&Fr=0 |title=Slimmy - Be Someone Else (Standard edition) |publisher=Fnac.pt |last=|first=|date=June 2010 |accessdate=7 May 2011}}</ref> the physical deluxe edition with 2 discs, the first disc featuring the standard 10 track listing plus a bonus remix of "Together 4ever" and the second disc, released in digipack format together with the other CD, featuring remixes of 8 songs from ''Beatsound Loverboy'';<ref name="Fnac 2">{{Cite web |url=http://www.fnac.pt/Slimmy-Be-Someone-Else-2CD-sem-especificar/a306369?PID=6&Mn=-1&Mu=-13&Ra=-28&To=0&Nu=1&Fr=0 |title=Slimmy - Be Someone Else (Deluxe edition) |publisher=Fnac.pt |last=|first=|date=June 2010 |accessdate=7 May 2011}}</ref> and the [[Digital data|digital]] format featuring 10 tracks.<ref name="iTunes"/> Both standard and deluxe edition's jewel case CD contained a booklet with alternative covers.<ref name="Fnac 2"/>

In an interview with ''Palco Principal'', on March 23, 2010, Slimmy revealed the title of the album.<ref name="palco principal 2"/> He explained: "As a first resort, when I wrote the lyrics of the song ["Be Someone Else"], I just wanted to warn those who not work, but still waiting for results. If you want to be somebody, you have to give much of yourself, is not enough to "give less and expect more". However, over the years, I realized that music was also for the kind of people that bring us down. Life is not a race. We must be ourselves, open our eyes and always look forward."<ref name="palco principal entrevista"/>

[[File:Slimmy performing during the Be Someone Else tour.jpg|left|thumb|Slimmy performing during the "Be Someone Else Tour", on 6 May 2011.]]
While being interviewed by ''Palco Principal'', Slimmy declared he was bit "unsure" about the promotion process of the album, he affirmed that he already had shows to make in [[France]] and [[Germany]], but his real intention was to tour [[Portugal]] intensively, because he felt he had no success in his country.<ref name="palco principal entrevista"/>  The first promotional concert toke place at the Cidade do [[Porto]] Shopping on 26 March 2010, where Slimmy sang a few songs from his album, including "The Games You Play" and "Be Someone Else".<ref name="Portugal Rebelde">{{Cite web |url=http://portugalrebelde.blogspot.com/2010/03/slimmy-be-someone-else.html |title=SLIMMY {{!}} "Be Someone Else" |publisher=Portugal Rebelde |last=|first=|date=22 March 2010 |accessdate=7 May 2011}}</ref> Slimmy also promoted his album in [[Albufeira]], at the Algarve Shopping's [[Fnac]] on November 27, 2010.<ref>{{Cite web |url=https://www.facebook.com/event.php?eid=168041799889079 |title=SLIMMY - NOITE ADERENTE |publisher=Slimmy |last=|first=|date=April 2011 |accessdate=8 May 2011}}</ref> Slimmy's next significant [[Advertising campaign|promotion]] marked the beginning of "A Very Slimmy Tour", where Slimmy performed songs from his two albums. It started on 18 February 2011 at the Kastrus River Klub in Esposende<ref name="slimmy tour">{{Cite web |url=https://www.facebook.com/event.php?eid=187986404567585 |title=A Very Slimmy Tour |publisher=Facebook |last=|first=|date=February 2011 |accessdate=7 May 2011}}</ref> and ended on 30 April 2011 at the Pitch Club in [[Porto]].<ref name="slimmy tour 2">{{Cite web |url=https://www.facebook.com/notes/1bigo-artistas-e-eventos/slimmy-este-s%C3%A1bado-no-pitch-club-porto/10150175575959380 |title=A Very Slimmy Tour |publisher=Facebook |last=|first=|date=April 2011 |accessdate=7 May 2011}}</ref> The first music video to be released was "The Games You Play", produced by Ana Andrade, Carla Fardilha, Clara Araújo Teixeira and Helena Oliveira. It premiered on 17 November 2010 on [[YouTube]].<ref name="YouTube 1">{{Cite web |url=https://www.youtube.com/watch?v=tUBLJOeG9-A&feature=related |title=Slimmy: "The Games You Play" at YouTube |publisher=Youtube |last=|first=|year=2010 |accessdate=7 May 2011}}</ref> A music video was also made for "Be Someone Else", produced by Riot Films. It premiered on 27 June 2010 on [[YouTube]].<ref name="YouTube 2">{{Cite web |url=https://www.youtube.com/watch?v=HAGjW4WHV6Q&feature=related |title=Slimmy: "Be Someone Else" at YouTube |publisher=Youtube |last=|first=|year=2010 |accessdate=7 May 2011}}</ref> Slimmy's "Be Someone Else Tour" began on 6 May 2011 with its opening show at the Academy Week in [[Mirandela]]. Future show dates will be added on 1bigo's [[Facebook]] page.<ref name="1bigo">{{Cite web |url=https://www.facebook.com/event.php?eid=207523935954233 |title=Slimmy - Be Someone Else tour |publisher=1bigo (Facebook)|last=|first=|year=2011 |accessdate=8 May 2011}}</ref> Slimmy also performed at the Queima das Fitas in [[Viana do Castelo Municipality|Viana do Castelo]] on 15 May 2011.<ref>{{Cite web |url=https://www.facebook.com/event.php?eid=193231174053593 |title=Slimmy na Queima das fitas de Viana do Castelo |publisher=Slimmy (Facebook)|last=|first=|year=2011 |accessdate=8 May 2011}}</ref>

=== Singles ===
"Be Someone Else" was unveiled as the album's [[lead single]]. The song was written by Fernandes and produced by Quico Serrano and Mark Turner.<ref name="Ionline"/> It was released to [[MySpace]] on 1 January 2010. "The Games You Play", also written by Fernandes and produced by Quico Serrano and Mark Turner, was released as the second single of the album on 6 March 2010 on the [[iTunes Store]].<ref name="The Games You Play at itunes">{{Cite web |url=https://itunes.apple.com/pt/album/the-games-you-play/id376203105?i=376203106 |title=The Games You Play Slimmy  |publisher=iTunes|last=|first=|date=March 2010 |accessdate=7 May 2011}}</ref> "The Games You Play" premiered on the Antena 3 radio station on 20 September 2009<ref>{{Cite web |url=http://slimmyfans-pt.blogs.sapo.pt/43726.html |title=Novo single - The Games You Play |publisher=Sapo.pt|last=|first=|date=January 2009 |accessdate=8 May 2011}}</ref> and was released on Slimmy's MySpace profile on 30 September 2009 as a promotional song.<ref name="iTunes songs">{{Cite web |url=https://www.myspace.com/slimmyuk/music/songs/the-games-you-play-57681579 |title=Slimmy: "The Games You Play" at iTunes |publisher=MySpace |last=Fernandes|first=Paulo|date=January 2007 |accessdate=7 May 2011}}</ref> Neither of the album's singles were particularly successful, charting anywhere. Despite not being released as a single, "I Can't Live Without You in This Town" is available for digital purchase at [[Amazon.com|Amazon]] since June 14, 2010.<ref name="amazon">{{Cite web |url=http://www.amazon.com/dp/B003PQQZMI |title=I Can't Live Without You In This Town |publisher=Amazon|last=|first=|date= |accessdate=2 July 2011}}</ref> It charted at number 152 at the ''Metropolis Chart'',<ref name="chart"/> and it was featured on the eighth season of the Portuguese soap opera ''[[Morangos com Açúcar]]''.

== Critical reception ==
{{Album ratings
| rev1 = A Trompa
| rev1Score = Favorable<ref name="A Trompa"/>
| rev2 = CDGO
| rev2Score = Positive<ref name="CDGO"/>
| rev3 = Palco Principal
| rev3Score = Favorable<ref name="Palco Principal"/>
}}
Since its release, the album has received favorable reviews from contemporary music critics. ''A Trompa'' gave the album a favorable review, commenting that "[the album is] fast and oriented to be devoured in dance floors or other places of similar excitement and socializing" it "clearly evokes a [[modern rock]] that welcomes people to it". ''A Trompa'' also praised Quico Serrano and Mark Turner's polished production, calling the album more direct and humanized then ''Beatsound Loverboy''.<ref name="A Trompa">{{Cite web |url=http://a-trompa.net/shots/shot-de-de-slimmy |title=Review: Be Someone Else de Slimmy |publisher=A Trompa |last=|first=|date=6 March 2011 |accessdate=7 May 2011}}</ref> ''CDGO'' gave the album a positive review and commented that the album is more eclectic and that "it will surely satisfy the fans and other lovers of [[Electronic music|electronic]] music.
Slimmy emphasizes again his unmistakable style, both musical and his image, proving to be one of the most promising musicians of the new generation of national music."<ref name="CDGO">{{Cite web|url=http://www.cdgo.com/artigoDetalhe.php?idArtigo=5154421 |title=Review: A maioridade de Slimmy |publisher=CDGO |last= |first= |date=June 2010 |accessdate=8 May 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110526020818/http://www.cdgo.com/artigoDetalhe.php?idArtigo=5154421 |archivedate=26 May 2011 |df= }}</ref> Ágata Ricca from ''Palco Principal'' also gave the album a favorable review and stated that "[the album] marks the difference with the previous one, due to Slimmy's mature and reflective attitude" and that the album does not "change Slimmy's past, it just emphasize it and strengthen it". Ágata Ricca also praised the song "I Can't Live Without You In This Town" for its markable difference from the other tracks, calling the chorus of the song memorable. She also said that some other tracks of the album are pretty "danceable".<ref name="Palco Principal">{{Cite web |url=http://palcoprincipal.sapo.pt/artigos/Artigo/review_a_maioridade_de_slimmy |title=Review: A maioridade de Slimmy |publisher=Palco Principal |last=Ricca|first=Ágata|date=6 March 2011 |accessdate=7 May 2011}}</ref>

== Track listing ==
{{Track listing
| collapsed       = 
| headline        = Standard edition
| extra_column    = 
| total_length    = 
| all_writing     = 
| all_lyrics      = Paulo Fernandes
| all_music       = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos<ref name="iTunes"/>
| writing_credits = yes
| lyrics_credits  = yes
| music_credits   = yes
| title1          = Be Someone Else
| note1           = 
| writer1         = [[Slimmy|Paulo Fernandes]]
| lyrics1         = 
| music1          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra1          = 
| length1         = 3:22
| title2          = Glad I'm Lonely
| note2           = 
| writer2         = Paulo Fernandes
| lyrics2         = 
| music2          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra2          = 
| length2         = 3:46
| title3          = My Flipside 
| note3           = 
| writer3         = Paulo Fernandes
| lyrics3         = 
| music3          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra3          = 
| length3         = 3:15
| title4          = Nightout
| note4           = 
| writer4         = Paulo Fernandes
| lyrics4         = 
| music4          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra4          = 
| length4         = 3:19
| title5          = The Games You Play
| note5           = 
| writer5         = Paulo Fernandes
| lyrics5         = 
| music5          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra5          = 
| length5         = 3:29
| title6          = You Give it a Shot
| note6           = 
| writer6         = Paulo Fernandes
| lyrics6         = 
| music6          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra6          = 
| length6         = 3:20
| title7          = [[I Can't Live Without You In This Town]]
| note7           = 
| writer7         = Paulo Fernandes
| lyrics7         = 
| music7          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra7          = 
| length7         = 3:56
| title8          = So Out Of Control
| note8           = 
| writer8         = Paulo Fernandes
| lyrics8         = 
| music8          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra8          = 
| length8         = 3:22
| title9          = Touch (Can Only Feel You) 
| note9           = 
| writer9         = Paulo Fernandes
| lyrics9         = 
| music9          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra9          = 
| length9         = 4:23
| title10          = I'm Open Doors
| note10           = 
| writer10         = Paulo Fernandes
| lyrics10         = 
| music10          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra10          = 
| length10         = 3:28
}}
{{track listing
| headline        = [[iTunes]] bonus track
| writing_credits = no
| title11         = Together 4ever
| note11          = DJ Ride Mix<ref name="youclubvideo">{{Cite web|url=http://www.youclubvideo.com/audio/112544/slimmy-together-4ever-dj-ride-mix |title=Slimmy: "Together 4ever" |publisher=youclubvideo.com |last= |first= |year=2010 |accessdate=7 May 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110412083857/http://www.youclubvideo.com:80/audio/112544/slimmy-together-4ever-dj-ride-mix |archivedate=12 April 2011 |df= }}</ref>
| length11        = 2:57
}}

=== Deluxe edition ===
{{Track listing
| collapsed       = yes
| headline        = Disc 1 (''Be Someone Else'')
| extra_column    = 
| total_length    = 
| all_writing     = 
| all_lyrics      = 
| all_music       = 
| writing_credits = yes
| lyrics_credits  = yes
| music_credits   = yes
| title1          = Be Someone Else
| note1           = 
| writer1         = [[Slimmy|Paulo Fernandes]]
| lyrics1         = 
| music1          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra1          = 
| length1         = 3:22
| title2          = Glad I'm Lonely
| note2           = 
| writer2         = Paulo Fernandes
| lyrics2         = 
| music2          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra2          = 
| length2         = 3:46
| title3          = My Flipside 
| note3           = 
| writer3         = Paulo Fernandes
| lyrics3         = 
| music3          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra3          = 
| length3         = 3:15
| title4          = Nightout
| note4           = 
| writer4         = Paulo Fernandes
| lyrics4         = 
| music4          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra4          = 
| length4         = 3:19
| title5          = The Games You Play
| note5           = 
| writer5         = Paulo Fernandes
| lyrics5         = 
| music5          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra5          = 
| length5         = 3:29
| title6          = You Give it a Shot
| note6           = 
| writer6         = Paulo Fernandes
| lyrics6         = 
| music6          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra6          = 
| length6         = 3:20
| title7          = I Can't Live Without You In This Town
| note7           = 
| writer7         = Paulo Fernandes
| lyrics7         = 
| music7          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra7          = 
| length7         = 3:56
| title8          = So Out Of Control
| note8           = 
| writer8         = Paulo Fernandes
| lyrics8         = 
| music8          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra8          = 
| length8         = 3:22
| title9          = Touch (Can Only Feel You) 
| note9           = 
| writer9         = Paulo Fernandes
| lyrics9         = 
| music9          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra9          = 
| length9         = 4:23
| title10          = I'm Open Doors
| note10           = 
| writer10         = Paulo Fernandes
| lyrics10         = 
| music10          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra10          = 
| length10         = 3:28
| title11         = Together 4ever
| note11          = DJ Ride Mix<ref name="youclubvideo" />
| length11        = 2:57
}}

{{Track listing
| collapsed       = yes
| headline        = Disc 2 (''Beatsound Loverboy Remixes'')
| extra_column    = 
| total_length    = 
| all_writing     = 
| all_lyrics      = 
| all_music       = 
| writing_credits = yes
| lyrics_credits  = yes
| music_credits   = yes
| title1          = Inside The One 
| note1           = Robot Português remix
| writer1         = [[Slimmy|Paulo Fernandes]]
| lyrics1         = 
| music1          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra1          = 
| length1         = 3:39
| title2          = Bloodshot Star 
| note2           = Players Please remix
| writer2         = Paulo Fernandes
| lyrics2         = 
| music2          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra2          = 
| length2         = 5:40
| title3          = Beatsound Loverboy  
| note3           = DJ Ride remix
| writer3         = Paulo Fernandes
| lyrics3         = 
| music3          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra3          = 
| length3         = 4:16
| title4          = Set Me On Fire 
| note4           = Beatbender remix
| writer4         = Paulo Fernandes
| lyrics4         = 
| music4          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra4          = 
| length4         = 3:49
| title5          = Show Girl 
| note5           = Bullycau remix
| writer5         = Paulo Fernandes
| lyrics5         = 
| music5          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra5          = 
| length5         = 4:35
| title6          = You Should Never Leave Me Alone
| note6           = You Should Never Leave mash-up remix
| writer6         = Paulo Fernandes
| lyrics6         = 
| music6          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra6          = 
| length6         = 3:58
| title7          = Inside The One 
| note7           = Bitcrusher remix
| writer7         = Paulo Fernandes
| lyrics7         = 
| music7          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra7          = 
| length7         = 3:42
| title8          = Bloodshot Star 
| note8           = Dean Rosenweig remix
| writer8         = Paulo Fernandes
| lyrics8         = 
| music8          = Paulo Fernandes, Paulo Garim, Tó-Zé, Gustavo Silva, Daniel Santos
| extra8          = 
| length8         = 4:44
}}

== Personnel ==
Taken from iOnline.<ref name="Ionline"/>
;Performance Credits
{{col-begin}}
*Paulo Fernandes – main [[vocals]], [[guitar]]
*Paulo Garim – [[Bass guitar|bass]]
*Tó-Zé – [[drums]]
{{col-end}}

;Technical credits
{{col-begin}}
*Quico Serrano – [[Record producer|producer]]
*Mark Turner – producer
{{col-end}}

==Charts==

===Songs===
{| class="wikitable" 
|- bgcolor="#efefef"
! bgcolor="lightsteelblue" width="25" rowspan="2"| Year
! bgcolor="lightsteelblue" width="170" rowspan="2"| Title
! bgcolor="lightsteelblue" colspan="1"| Chart Positions
|- bgcolor="#efefef"
! bgcolor="#e2e2e2" width="40"| <small>[[Portugal|POR]]</small>
|-
| 2010
| "I Can't Live Without You In This Town"
| align="center" |152<ref name="chart">{{Cite web |url=http://ww1.rtp.pt/icmblogs/rtp/metropolis/?k=%23-152.rtp&post=30009 |title=152 Metropolis  |publisher=[[Rádio e Televisão de Portugal|RTP]] |last=|first=|date=12 January 2011 |accessdate=11 May 2011}}</ref>
|}

== Release history ==
{|class="wikitable"
|-
! Region
! Date
! Label
! Format
! Edition(s)
|-
| Worldwide<ref name="iTunes"/>
| June 14, 2010
| rowspan="5"|iPlay
| [[Music download|Digital Download]]
| Standard edition
|-
| [[Portugal]]<ref name="a vida é um palco">{{Cite web|url=http://avidaeumpalco.com/index.php/2010/06/17/slimmy-lana-be-someone-else/ |archive-url=https://archive.is/20130117105843/http://avidaeumpalco.com/index.php/2010/06/17/slimmy-lana-be-someone-else/ |dead-url=yes |archive-date=January 17, 2013 |title=Slimmy lança Be Someone Else |publisher=A Vida é um Palco |last= |first= |date=June 17, 2010 |accessdate=7 May 2011 }}</ref>
| rowspan="3"|June 21, 2010 
| rowspan="4"|[[Compact disc|CD]]
| rowspan="4"|Standard edition, Deluxe edition
|-
|[[France]]<ref>{{Cite web |url=http://recherche.fnac.com/Search/SearchResult.aspx?SCat=0%211&Search=slimmy&sft=1&submitbtn=Ok |title=Slimmy Be Someone Else à la Fnac |publisher=Fnac.fr |last=|first=|date=June 2010|accessdate=8 May 2011}}</ref>
|-
|[[Belgium]]<ref>{{Cite web|url=http://www.fnac.be/nl/Catalog/Detail.aspx?cIndex=1&catalog=dischi&categoryN=Muziek&category=popRock&product=10581343 |title=Slimmy Be Someone Else - Fnac.be |publisher=Fnac.be |last= |first= |date=June 2010 |accessdate=8 May 2011 }}{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>
|-
| [[Spain]]<ref>{{Cite web |url=http://musica.fnac.es/a457565/SLIMMY-Be-someone-else-sin-especificar?PID=6&Mn=-1&Ra=-28&To=0&Nu=2&Fr=0 |title=Slimmy Be Someone Else - Fnac.es |publisher=Fnac.es |last=|first=|date=July 2010|accessdate=8 May 2011}}</ref>
| July 20, 2010 
|}

== References ==
{{reflist|2}}

== External links ==
* [https://itunes.apple.com/pt/album/be-someone-else/id376044988 "Be Someone Else"] at [[iTunes]].
* [http://www.slimmymusic.com/ Slimmymusic.com], Slimmy's official website.

{{Slimmy}}

{{good article}}

[[Category:2010 albums]]
[[Category:Slimmy albums]]