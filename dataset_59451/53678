{{unreferenced|date=January 2015}}
'''''Nostro''''' and '''''vostro''''' (from [[Italian language|Italian]], ''nostro'' and ''vostro''; English, 'ours' and 'yours') are accounting terms used to distinguish an account held for another entity from an account another entity holds. The entities in question are almost always, but need not be, [[bank]]s.

==Origin==

{{Empty section|date=January 2015}}

==Understanding terminology==

===Definition===
The terms ''nostro'' and ''vostro'' are used, mainly by banks, when one bank keeps money at another bank.  Both banks need to keep records of how much money is being kept by one bank on behalf of the other.  In order to distinguish between the two sets of records of the same balance and set of transactions, banks refer to the accounts as ''nostro'' and ''vostro''. Speaking from the point of view of the bank whose money is being held at another bank:
{{anchor|anchor1}}
* A ''nostro'' is our account of ''our'' money, held by the ''other bank''
* A ''vostro'' is our account of ''other bank'' money, held by ''us''

A ''vostro'' account is the same as any other bank account.  It is a record of money held by a bank or owed to a bank by a third party (an individual, company or bank).  The ''nostro'' account is a way of the bank whose money it is, keeping track of how much is being held by the other bank.  It is similar to an individual keeping a detailed record of every payment in and out of his or her bank account so that she/he knows the balance at any point in time.

===Base description===
In normal usage:
* A ''nostro'' account will be in foreign currency (it is a record of funds held by a bank in another country in the currency of that country) i.e. a bank in country A keeping a record of money held by a bank in country B, in the currency of country B.
* A ''vostro'' account will be in the local currency of the bank where the money is being held i.e. it is the bank in Country B's record of the money kept by the bank from country A with it.  

For these accounts the domestic bank is acting like a custodian or managing the accounts of a foreign counterpart. These accounts are utilised for facilitating the settlements of Forex and foreign trades.

A ''client'' bank elects to open an account with another ''facilitator'' bank.  The ''facilitator'' bank will assist the ''client'' bank in making payments in its country's currency, usually using its own  access to primary clearing arrangements (generally with the Central Bank in the country where the currency is considered a local currency).  In some cases the ''facilitator'' bank may not be a primary clearing member but they will have the ability to make payments in local currency, possibly through another bank in the same country.

==Accounting==

===Conventions===
A bank counts a ''nostro'' account with a debit balance as a cash asset in its balance sheet.  Conversely, a ''vostro'' account with a credit balance (i.e. a deposit) is a liability, and a ''vostro'' with a debit balance (a loan) is an asset. Thus in many banks a credit entry on an account ("CR") is regarded as negative movement, and a debit ("DR") is positive - the reverse of usual commercial accounting conventions.

With the advent of computerised accounting, nostros and vostros just need to have opposite signs within any one bank's accounting system; that is, if a nostro in credit has a positive sign, then a vostro in credit must have a negative sign. This allows for a reconciliation by summing all accounts to zero (a trial balance) - the basic premise of [[Double-entry bookkeeping system|double-entry bookkeeping]].

===Typical usage===
Nostro accounts are mostly commonly used for currency settlement, where a bank or other financial institution needs to hold balances in a currency other than its home accounting unit.

For example: First National Bank of ''A'' does some transactions (loans, foreign exchange, etc.) in USD, but banks in ''A'' will only handle payments in AUD.  So FNB of ''A'' opens a USD account at foreign bank Credit Mutuel de ''B'', and instructs all counter-parties to settle transactions in USD at "account no. 123456 in name of ''FNBA'', at ''CMB'', ''X'' Branch".  FNBA maintains its own records of that account, for reconciliation; this is its ''nostro'' account.  CMB's record of the same account is the ''vostro'' account.

Now, FNBA sells AUD1,000,000 to ''C'' (a counterparty who has an AUD account with FNBA, and a USD account with CMB) for a net consideration of USD2,000,000. FNBA will make the following entries in its own accounting system:

{| class="wikitable"
| (Internal) FX AUD trading account
| 1,000,000 DR
| AUD Account in name of '''C'''
| 1,000,000 CR
|-
| USD Nostro at CMB (FNBA's nostro)
| 2,000,000 '''DR'''
| (Internal) FX USD trading account
| 2,000,000 CR
|}

Over at CMB, they record the following transaction:

{| class="wikitable"
| USD Account in name of '''C'''
| 2,000,000 DR
| USD Account in name of FNBA (CMB's vostro)
| 2,000,000 '''CR'''
|}
?????
[This is somewhat simplified; in reality C may not have an account with FNBA's corresponding bank, and will make settlement by cheque or some form of [[electronic funds transfer]] (EFT). In this case CMB will make entries on several other accounts, such as a Teller's receiving account, or a clearing account with the third bank that the cheque was written on.]

==Related expressions==
There is also the notion of a ''loro'' account ("theirs"), which is a record of an account held by a second bank on behalf of a third party; that is, my record of ''their'' account with you. In practice this is rarely used, the main exception being complex syndicated financing.

In the same style as [[#anchor1|above]]:
* A ''loro'' is our account of ''their'' money, held by us

==See also==
* [[Correspondent account]]

==External links==
* [http://www.gethow.org/nostro-accounts Nostro Accounts in Forex Market] at GetHow
* [http://www.gethow.org/vostro-accounts Vostro Accounts in Forex Market] at GetHow
* [http://www.investopedia.com/terms/n/nostroaccount.asp Nostro Accounts - Definition] at Investopedia
* [http://www.investopedia.com/terms/v/vostroaccount.asp Vostro Accounts - Definition] at Investopedia

[[Category:Banking terms]]