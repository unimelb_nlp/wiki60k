{{Infobox United Nations
| name = United Nations Economic Commission for Europe
| type = Primary Organ - Regional Branch
| acronyms = ECE
| head = ''Executive Secretary of ECE''<br />
: {{flagicon|Denmark}} [[Christian Friis Bach]]
: (2014 - present)<ref>{{cite web|url=http://www.unece.org/press/execsec/current_exec.html|title=Executive Secretary|publisher=UNECE}}</ref>
| status = Active
| established = 1947
| website = [http://www.unece.org/ ECE Website]
| parent = [[United Nations Economic and Social Council|ECOSOC]]
| map = UNECE.png
| map_caption = Map showing United Nations Economic Commission for Europe members
}}

The '''[[United Nations]] Economic Commission for [[Europe]]''' ('''UNECE''' or '''ECE''') was established in 1947<ref>{{cite web|url=http://www.unece.org/oes/history/history.htm|title=Inception|publisher=UNECE}}</ref><ref name="UN_ARES46I_page2">{{UN document |docid=A-RES-46(I) |type=Resolution |body=General Assembly |session=-1 |resolution_number=46 |highlight=rect_103,429_452,537 |page=2 |accessdate=2008-07-09|title=Economic Reconstruction of Devastated Areas|date=11 December 1946}}</ref> to encourage economic cooperation among its member States. It is one of five regional commissions under the administrative direction of United Nations headquarters. It has 56 member States, and reports to the [[United Nations Economic and Social Council]] (ECOSOC). Besides countries in Europe, it also includes Canada, the Central Asian republics, Israel and the United States of America. The UNECE secretariat headquarters is in [[Geneva]], [[Switzerland]], and has an approximate budget of [[US$]]50 million.<ref>http://www.theyworkforyou.com/whall/?id=2006-04-18a.1.1</ref>

==Member states==
The 56 member countries are listed below. 18 of the UNECE's member countries are recipients of [[official development assistance]].<ref>UNECE, The UNECE Report on Achieving the Millennium Development Goals, 2011</ref>

{| class="sortable wikitable" style="font-size:90%; text-align:left;"
|+ style="padding-top:1em;" |Member countries
! Countries !! Date of membership
|-
|{{flag|Albania}}
|{{dts|14 December 1955}}
|-
|{{flag|Andorra}}
|{{dts|28 July 1993}}
|-
|{{flag|Armenia}}
|{{dts|30 July 1993}}
|-
|{{flag|Austria}}
|{{dts|14 December 1955}}
|-
|{{flag|Azerbaijan}}
|{{dts|30 July 1993}}
|-
|{{flag|Belarus}}
|{{dts|28 March 1947}}
|-
|{{flag|Belgium}}
|{{dts|28 March 1947}}
|-
|{{flag|Bosnia and Herzegovina}}
|{{dts|22 May 1992}}
|-
|{{flag|Bulgaria}}
|{{dts|14 December 1955}}
|-
|{{flag|Canada}}
|{{dts|9 August 1973}}
|-
|{{flag|Croatia}}
|{{dts|22 May 1992}}
|-
|{{flag|Cyprus}}
|{{dts|20 September 1960}}
|-
|{{flag|Czech Republic}}
|{{dts|28 March 1947}}
|-
|{{flag|Denmark}}
|{{dts|28 March 1947}}
|-
|{{flag|Estonia}}
|{{dts|17 September 1991}}
|-
|{{flag|Finland}}
|{{dts|14 December 1955}}
|-
|{{flag|Macedonia}}
|{{dts|8 April 1993}}
|-
|{{flag|France}}
|{{dts|28 March 1947}}
|-
|{{GEO}}
|{{dts|30 July 1993}}
|-
|{{flag|Germany}}
|{{dts|18 September 1973}}
|-
|{{flag|Greece}}
|{{dts|28 March 1947}}
|-
|{{flag|Hungary}}
|{{dts|14 December 1955}}
|-
|{{flag|Iceland}}
|{{dts|28 March 1947}}
|-
|{{flag|Ireland}}
|{{dts|14 December 1955}}
|-
|{{flag|Israel}}
|{{dts|26 July 1991}}
|-
|{{flag|Italy}}
|{{dts|14 December 1955}}
|-
|{{flag|Kazakhstan}}
|{{dts|31 January 1994}}
|-
|{{flag|Kyrgyzstan}}
|{{dts|30 July 1993}}
|-
|{{flag|Latvia}}
|{{dts|17 September 1991}}
|-
|{{flag|Liechtenstein}}
|{{dts|18 September 1990}}
|-
|{{flag|Lithuania}}
|{{dts|17 September 1991}}
|-
|{{flag|Luxembourg}}
|{{dts|28 March 1947}}
|-
|{{flag|Malta}}
|{{dts|1 December 1964}}
|-
|{{flag|Moldova}}
|{{dts|2 March 1992}}
|-
|{{flag|Monaco}}
|{{dts|27 May 1993}}
|-
|{{flag|Montenegro}}
|{{dts|28 June 2006}}
|-
|{{flag|Netherlands}}
|{{dts|28 March 1947}}
|-
|{{flag|Norway}}
|{{dts|28 March 1947}}
|-
|{{flag|Poland}}
|{{dts|28 March 1947}}
|-
|{{flag|Portugal}}
|{{dts|14 December 1955}}
|-
|{{flag|Romania}}
|{{dts|14 December 1955}}
|-
|{{flag|Russia}}
|{{dts|28 March 1947}}
|-
|{{flag|San Marino}}
|{{dts|30 July 1993}}
|-
|{{flag|Serbia}}
|{{dts|1 November 2000}}
|-
|{{flag|Slovakia}}
|{{dts|28 March 1947}}
|-
|{{flag|Slovenia}}
|{{dts|22 May 1992}}
|-
|{{flag|Spain}}
|{{dts|14 December 1955}}
|-
|{{flag|Sweden}}
|{{dts|28 March 1947}}
|-
|{{flag|Switzerland}}
|{{dts|24 March 1972}}
|-
|{{flag|Tajikistan}}
|{{dts|12 December 1994}}
|-
|{{flag|Turkey}}
|{{dts|28 March 1947}}
|-
|{{flag|Turkmenistan}}
|{{dts|30 July 1993}}
|-
|{{flag|Ukraine}}
|{{dts|28 March 1947}}
|-
|{{flag|United Kingdom}}
|{{dts|28 March 1947}}
|-
|{{flag|United States}}
|{{dts|28 March 1947}}
|-
|{{flag|Uzbekistan}}
|{{dts|30 July 1993}}
|}

== Committee on Economic Cooperation and Integration ==
This Committee promotes a policy, financial and regulatory environment conducive to economic growth, innovative development and higher competitiveness in the UNECE region, focusing mainly on countries with economies in transition. Its main areas of work are innovation and competitiveness policies, intellectual property, financing innovative development, entrepreneurship and enterprise development, and public-private partnerships.

== Committee on Environmental Policy ==
UNECE’s concern with problems of the environment dates back at least to 1971, when the group of Senior Advisors to the UNECE governments on environmental issues was created which led to the establishment of the Committee on Environmental Policy, which now meets annually. The Committee provides collective policy direction in the area of environment and sustainable development, prepares ministerial meetings, develops international environmental law and supports international initiatives in the region. CEP works to support countries to enhance their environmental governance and transboundary cooperation as well as strengthen implementation of the UNECE regional environmental commitments and advance sustainable development in the region.

Its main aim is to assess countries' efforts to reduce their overall pollution burden and manage their natural resources, to integrate environmental and socioeconomic policies, to strengthen cooperation with the international community, to harmonize environmental conditions and policies throughout the region and to stimulate greater involvement of the public and environmental discussions and decision-making.

CEP is the overall governing body of UNECE environmental activities. The Committee's work is based on several strategic pillars:<ref>http://www.unece.org/env/</ref> 
* Providing the secretariat to the "Environment for Europe" process and participating in the regional promotion of Agenda 21; 
* Developing and carrying-out of [[UNECE Environmental Performance Reviews]] in the UNECE countries non-members of OECD;<ref>[http://www.unece.org/env/epr.html - EPR Programme]</ref>
* Overseeing UNECE activities on [[environmental monitoring]], assessment and reporting;
* Increasing the overall effectiveness of UNECE multilateral environmental agreements (MEAs) and facilitating the exchange of experience on MEAs' implementation. See UNECE [[Espoo Convention]], [[Aarhus Convention]], [[Convention on Long-Range Transboundary Air Pollution]], [[Convention on the Protection and Use of Transboundary Watercourses and International Lakes]] and [[Convention on the Transboundary Effects of Industrial Accidents]].
* Participating and/or facilitating the exchange of experience in a number of cross-sectoral activities undertaken under the leadership of UNECE (e.g. [[education for sustainable development]], transport, health and environment, green building), or in partnership with other organizations (e.g. environment and security initiative, European environment and health process).

== Committee on Housing and Land Management ==
In 1947, UNECE set up a Panel on Housing Problems, which later evolved into the Committee on Human Settlements and after the reform in 2005/2006 into the Committee on Housing and Land Management. The Committee is an intergovernmental body of all UNECE member States. It provides a forum for the compilation, dissemination and exchange of information and experience on housing, urban development, and land administration policies; & in areas such as [[Birmingham]], a more fiscal issue-[[UK]].<ref>{{cite web|url=http://www.unece.org/hlm/ |title=Archived copy |accessdate=September 28, 2009 |deadurl=yes |archiveurl=https://web.archive.org/web/20091008114943/http://www.unece.org/hlm/ |archivedate=October 8, 2009 }}</ref>

== Inland Transport Committee ==
The UNECE Transport Division has been providing secretariat services to the [[World Forum for Harmonization of Vehicle Regulations]] (WP.29). In addition to acting as secretariat to the World Forum, the Vehicle Regulations and Transport Innovations section serves as the secretariat of the Administrative Committee for the coordination of work, and of the Administrative/Executives Committees of the three Agreements on vehicles administered by the World Forum.<ref>{{cite web|url=http://www.unece.org/trans/main/welcwp29.htm?expandable%3D99 |title=Archived copy |accessdate=October 12, 2010 |deadurl=yes |archiveurl=https://web.archive.org/web/20100926071254/http://www.unece.org/trans/main/welcwp29.htm?expandable=99 |archivedate=September 26, 2010 }}</ref>

== Conference of European Statisticians ==
The UNECE [http://www.unece.org/stats/ Statistical Division] provides the secretariat for the Conference and its expert groups, and implements the statistical work programme of the UNECE. The Conference brings together chief statisticians from national and international statistical organizations around the world, meaning that the word "European" in its name is no longer an accurate description of its geographical coverage. The Statistical Division helps member countries to strengthen their statistical systems, and coordinates international statistical activities in the UNECE region and beyond through the Conference and its Bureau, and the [http://www1.unece.org/stat/platform/display/disaarchive Database of International Statistical Activities]. The Statistical Division develops guidelines and [http://www1.unece.org/stat/platform/display/TRAINSTATS training materials] on statistical methodology and practices, in response to demands from member countries. It works with different groups of specialists from national and international statistical organizations, and organizes meetings and on-line forums for statistical experts to exchange experiences on a wide range of topics. The UNECE Statistical Division also provides technical assistance to South-East European, East European, Caucasus and Central Asian countries.

The division also provides:

1) [http://w3.unece.org/pxweb/ Free on-line data] on the 56 UNECE member countries in Europe, Central Asia and North America in both English and Russian, on economic, gender, forestry and transport statistics.

2) A biennial overview of key statistics for member countries: [https://web.archive.org/web/20120424035214/http://www.unece.org/stats/profiles2011.html UNECE Countries in Figures].

3) A [http://www1.unece.org/stat/platform set of wikis] to support collaboration activities and disseminate information about good practices.

UNECE conducted the {{visible anchor|Fertility and Family Survey}} in the 1990s in 23 member States, with over 150,000 participants, with hundreds of resulting scientific publications.<ref>{{cite web|url=http://www.unece.org/pau/ffs/ffs.html|title=Fertility and Family Survey (FFS)|website=unece.org|accessdate=2016-01-31}}</ref> This activity has hence continued in the form of the [[Generations and Gender Programme]].<ref>{{cite web|url=http://www.edac.eu/indicators_desc.cfm?v_id=117|title=Fertility and Family Survey (standard country tables) , FFS|website=edac.eu, the European Data Center for Work and Welfare|accessdate=2016-02-05}}</ref>

==Executive Secretaries==
{| class="wikitable sortable" 
!'''Years'''
!'''Country'''
!'''Executive Secretary 
|-
|1947–1957 
|{{flag|Sweden}}             
|[[Gunnar Myrdal]]
|-
|1957–1960 
|{{flag|Finland}} 
|[[Sakari Tuomioja]]
|-
|1960–1967 
|rowspan=2|{{flag|Yugoslavia}}
||[[Vladimir Velebit]]
|-
|1968–1982 
|[[Janez Stanovnik]]
|-
|1983–1986 
|{{flag|Finland}} 
|[[Klaus Sahlgren]]
|-
|1987–1993 
|{{flag|Austria}} 
|[[Gerald Hinteregger]]
|-
|1993–2000 
|{{flag|France}} 
|[[Yves Berthelot]]
|-
|2000–2001 
|{{flag|Poland}} 
|[[Danuta Hübner]]
|-
|2002–2005
|{{flag|Slovakia}} 
|[[Brigita Schmögnerová]]<ref>{{cite web|url=http://www.unece.org/press/pr2002/02gen03e.html|title=Secretary-General Appoints Brigita Schmögnerová as New Executive Secretary of Economic Commission for Europe|publisher=UNECE}}</ref>
|-
|2005–2008 
|{{flag|Poland}} 
|[[Marek Belka]]<ref>{{cite web|url=http://www.unece.org/press/pr2005/05gen_p11e.html|title=Secretary-General appoints Marek Belka of poland as Executive Secretary of Economic Commission for Europe|publisher=UNECE}}</ref>
|-
|2008–2012 
|{{flag|Slovakia}} 
|[[Ján Kubiš]]<ref>{{cite web|url=http://www.unece.org/press/pr2008/08gen_p03e.html|title=Secretary-General appoints Ján KUBIŠ of Slovakia to head United Nations Economic Commission for Europe |publisher=UNECE}}</ref>
|-
|2012–2014 
|{{flag|Bosnia and Herzegovina}} 
|[[Sven Alkalaj]]<ref>{{cite web|url=http://www.unece.org/index.php?id=29606|title=EXCOM welcomes Executive Secretary|publisher=UNECE}}</ref>
|-
|2014 
|rowspan=2|{{flag|Denmark}} 
|[[Michael Møller]] (acting)<ref>{{cite web|url=http://www.unece.org/index.php?id=35372|title=Acting Director-General of UNOG Michael Møller takes on functions of Acting Executive Secretary of UNECE|publisher=UNECE}}</ref>
|-
|2014–present 
|[[Christian Friis Bach]]<ref>{{cite web|url=http://www.unece.org/index.php?id=36030|title=The Secretary-General appoints Christian Friis Bach of Denmark as the next Executive Secretary of UNECE|publisher=UNECE}}</ref>
|}

== ''Statistical Journal of the United Nations Economic Commission for Europe''  ==
{{Infobox journal
| title         = Statistical Journal of the United Nations Economic Commission for Europe
| abbreviation  = Stat. J. U.N. Econ. Comm. Eur.
| publisher     = [[IOS Press]]
| country       = [[Netherlands]]
| history       = 1982-2007
| website       = http://www.unece.org/stats/publications/journal.e.html
| OCLC          = 900948641
| LCCN          = 84642632
| ISSN          = 0167-8000
| CODEN         = SJUED4
}}
From 1982 to 2007 the [[IOS Press]] published the ''Statistical Journal of the United Nations Economic Commission for Europe '' on behalf of the UNECE.<ref>{{cite web | title = Statistical Journal of the United Nations Economic Commission for Europe  | url = https://www.econbiz.de/Record/statistical-journal-of-the-united-nations-economic-commission-for-europe/10000471595 | publisher = [[EconBiz]] | accessdate = 11 June 2015 }}</ref><ref>{{cite web | title = Publications: STATISTICAL JOURNAL of the UNECE | url = http://www.unece.org/stats/publications/journal.e.html | publisher = UNECE | accessdate = 11 June 2015 }}</ref>

==See also==
* [[Europe]]
* [[European Union]]
* [[Council of Europe]]
* [[Organization for Security and Co-operation in Europe|OSCE]]
* [[OECD]]
* [[Official statistics]]
* [[United Nations System]]
* [[United Nations Economic Commission for Latin America and the Caribbean]] (overlapping membership)
* [[United Nations Economic and Social Commission for Asia and the Pacific]] (overlapping membership)
* [[UN/CEFACT]] - Trade facilitaion via standardised business communication.
* [[UN/LOCODE]] - location codes, maintained by UNECE
* [[International E-road network]], numbered by UNECE
* [[World Forum for Harmonization of Vehicle Regulations]]
* [[North American Union]]
* [[International Motor Insurance Card System#Green card system|Green card system]] - motor insurance scheme of UNECE
* [[International Union of Tenants|IUT]]

==References==
{{Reflist|30em}}

==External links==
{{commons category|United Nations Economic Commission for Europe}}
* [http://w3.unece.org/pxweb UNECE Statistical Database]
* [http://www.unece.org/highlights/unece_weekly/unece_weekly_index.htm UNECE Weekly]
* [http://www.unece.org UN Economic Commission for Europe]
* [http://ec.europa.eu/enterprise/library/enterprise-europe/issue14/articles/en/enterprise09_en.htm EU and UNECE cooperation].
* [http://www.unece.org/trans/main/welcwp29.htm UNECE vehicle regulations]

{{ECOSOC}}
{{Vehicle regulations}}
{{Portal bar|United Nations|Europe|Economy}}

{{Authority control}}
{{DEFAULTSORT:United Nations Economic Commission For Europe}}
[[Category:United Nations Development Group]]
[[Category:United Nations Economic and Social Council|Economic Commission for Europe]]
[[Category:Organizations established in 1947]]
[[Category:Vehicle law]]
[[Category:United Nations Economic Commission for Europe| ]]
[[Category:Organisations based in Geneva]]