{{Infobox Weapon
|name= AMX-30E
|image= [[Image:AMX30 afar.jpg|300px]]
|caption= An AMX-30E on display at the Museum of Armored Vehicles of El Goloso, in Spain
|origin= France/Spain
|type= [[Main battle tank]]
<!-- Type selection -->
|is_vehicle= yes
<!-- Service history -->
|service= 1970&ndash;2002
|used_by= Spain
<!-- Production history -->
|designer= [[Nexter|GIAT]]
|design_date= 1974
|manufacturer= [[Santa Bárbara Sistemas]]
|production_date= 1974&ndash;83 (manufactured in Spain)
|number= 299
|variants= AMX-30EM1 & AMX-30EM2
<!-- General specifications -->
|weight= 36&nbsp;[[tonne]]s (39.68&nbsp;[[short ton]]s)
|length= 9.48&nbsp;meters (31.1&nbsp;ft (hull length)
|width= 3.1&nbsp;meters (10.17&nbsp;ft)
|height= 2.87&nbsp;meters (9.42&nbsp;ft)
|crew= 4
<!-- Vehicle/missile specifications -->
|armour= 80&nbsp;millimeters (3.15&nbsp;in)
|primary_armament= [[105 mm Modèle F1]] [[tank gun]] (4.13&nbsp;in)
|secondary_armament= Co-axial [[M2 Browning machine gun]] & roof-mounted [[MG1S3A]] [[7.62×51mm NATO]] [[general-purpose machine gun]]
|engine= [[Hispano-Suiza]] HS-110 12-cylinder {{convert|670.7|hp|abbr=on}}
|pw_ratio= 20&nbsp;metric&nbsp;horsepower&nbsp;to&nbsp;tonne (16.9&nbsp;hp/STon)
|transmission= 5SD-200D&nbsp;5-speed
|suspension= [[Torsion beam suspension|Torsion bar]]
|clearance= .45&nbsp;meters (1.48&nbsp;ft)
|fuel_capacity= {{convert|962|L|impgal U.S.gal|1}}
|vehicle_range= {{convert|400|km|mi|sp=us}}
|speed= 65&nbsp;kilometers&nbsp;per&nbsp;hour (40.39&nbsp;mph)
}}
{{Spanish tanks}}

The '''AMX-30E''' (''E'' stands for ''España'', Spanish for Spain) is a Spanish [[main battle tank]] based on France's [[AMX-30]].  Although originally the Spanish government sought to procure the German [[Leopard 1]], the AMX-30 was ultimately awarded the contract due to its lower price and the ability to manufacture it in Spain.  280 units were manufactured by [[Santa Bárbara Sistemas]] for the [[Spanish Army]], between 1974 and 1983.

First acquired in 1970, the tank was to supplement Spain's fleet of [[M47 Patton|M47]] and [[M48 Patton]] United States tanks and to reduce Spain's reliance on American equipment in its army.  The first 19 AMX-30 tanks were acquired from France in 1970, while another 280 were assembled in Spain.  It was Spain's first [[Mass production|mass-produced]] [[tank]] and developed the country's industry to the point where the government felt it could produce a tank on its own, leading to the development of the [[Lince (tank)|Lince]] tank project in 1985. It also offered Santa Bárbara Sistemas the experience which led to the production of the [[Leopard 2E]] in late 2003.  Although final assembly was carried out by Santa Bárbara Sistemas, the production of the AMX-30E also included other companies in the country. Total production within Spain amounted to as much as 65% of the tank.

Spain's AMX-30E fleet went through two separate modifications in the late 1980s, a modernization program and a reconstruction program.  The former, named the AMX-30EM2 (150 tanks), sought to modernize and improve the vehicle's automotive characteristics, while the latter, or the AMX-30EM1  (149 tanks), resulted in a more austere improvement of the tank's [[Tank#Tactical mobility|power plant]] by maintaining the existing engine and replacing the [[Transmission (mechanics)|transmission]].  Both programs extended the vehicle's lifetime.  Spain's fleet of AMX-30EM1s was replaced in the late 1990s by the German [[Leopard 2A4]], and the AMX-30EM2s were replaced by [[B1 Centauro|Centauro]] wheeled [[Tank destroyer|anti-tank vehicle]]s in the early 21st century.

Although 19 AMX-30Es were deployed to the [[Spanish Sahara]] in 1970, the tank never saw combat.  In 1985 Indonesia expressed interest in the AMX-30E, while in 2004 the Spanish and [[Colombia]]n governments discussed the possible sale of around 40 AMX-30EM2s. Both trade deals fell through.

==Background==
In 1960, Spain's tank fleet was composed mainly of American [[M47 Patton]] tanks, with some newer [[M48 Patton]] tanks.<ref>Manrique & Molina, ''La Brunete'', pp. 31–69</ref>  The M47s had been acquired by the Spanish army in the mid-1950s,<ref>Zaloga, ''The M47 and M48 Patton Tanks'', p. 36</ref> replacing the previous fleet of 1930s-vintage [[Panzer I]], [[T-26]] and [[Panzer IV]] tank designs.<ref>Manrique & Molina, ''La Brunete'', p. 34</ref>  During the 1957-58 [[Ifni War]], the United States' ban on the usage of American ordnance supplied earlier as military aid to Spain<ref>Vidal, ''Ifni 1957–1958'', p. 70</ref> pushed Spain to look for alternative equipment which could be freely employed in the [[Spanish Sahara]].<ref name=MazarrasaAMX57>Mazarrasa, ''Carro de Combate AMX-30E'', p. 57</ref>

In the early 1960s, Spain looked towards its European neighbors for a new tank. The Spanish government first approached [[Krauss-Maffei]], the German manufacturer of the [[Leopard 1]],<ref name=MazarrasaAMX57 /> and the company applied for an export license from the German Economics Ministry.<ref>Rudnick, ''Atlantic Relations'', p. 200</ref> Spain's status as a non-[[NATO]] country meant that the decision to grant the export license had to be reviewed by the ''Bundessicherheitsrat'' (Federal Security Council), or the BSR, which was responsible for the coordination of the national defense policy.  Ultimately, the council ruled that Krauss-Maffei could sign an export contract with Spain.<ref>Rudnick, ''Atlantic Relations'', p. 201</ref>  The deal was, however, stalled by pressure from the United Kingdom's [[Labour Party (UK)|Labour Party]]<ref>Rudnick, ''Atlantic Relations'', p. 203</ref> on the basis that the Leopard's 105-millimeter (4.13&nbsp;in) [[Royal Ordnance L7|L7 tank gun]] was British technology.<ref>Rudnick, ''Atlantic Relations'', p. 199</ref>  Meanwhile, Spain tested the French AMX-30 between 2 and 10 June 1964.<ref name=MazarrasaAMX57 />

The Leopard 1 and the AMX-30 originated from a joint tank development program known as the Europanzer.<ref name=Hilmes17>Hilmes, ''Modern German Tank Development, 1956–2000'', p.17</ref>  For a tank, the AMX-30 had a low silhouette; the height of the tank was 2.28&nbsp;meters (7.48&nbsp;ft),<ref>Ogorkiewicz, ''AMX-30 Battle Tank'', p. 5</ref> compared to the Leopard's 2.6&nbsp;meters (8.53&nbsp;ft).<ref name=Jerchel28>Jerchel, ''Leopard 1 Main Battle Tank 1965–1995'', p. 28</ref>  In terms of lethality, the AMX-30's ''Obus G'' [[high-explosive anti-tank warhead|high-explosive anti-tank]] (HEAT) round was one of the most advanced projectiles at the time.<ref name=Orgokiewicz8>Ogorkiewicz, ''AMX-30 Battle Tank'', p. 8</ref>  Because HEAT warheads become less efficient during spin stabilization induced by the [[rifling]] of a tank-gun barrel,<ref>Ferrari, ''The "Hows" and "Whys" of Armour Penetration'', p. 87</ref> the ''Obus G'' was designed so that the [[shaped charge]] warhead was mounted on ball bearings within an outer casing, allowing the round to be spin stabilized through the rifling of the gun without affecting the warhead inside.  The ''Obus G'' was designed to penetrate up to 400&nbsp;millimeters (15.75&nbsp;in) of [[steel]] [[Vehicle armour|armor]].<ref name=Orgokiewicz8 />  On the other hand, the Leopard was armed with the [[L7A3]] [[tank gun]], capable of penetrating the frontal armor of most contemporary tanks.<ref>Lathrop & McDonald, ''M60 Main Battle Tank 1960–91'', p. 5</ref>  Although the Leopard boasted greater armor than the AMX-30<ref name=Hilmes17 />—partially accounting for the weight difference between the two tanks—the latter was sold at a cheaper price.<ref>Rudnick, ''Atlantic Relations'', p. 204</ref>

{| class="wikitable" border=0 cellspacing=0 cellpadding=3 style="border-collapse:collapse; text-align:left;" summary="Characteristics of the Patton series, AMX-30 and Leopard tanks"
|- style="vertical-align:bottom; border-bottom:1px solid #999;"
!
! style="text-align:left;" | M47 Patton<ref>Mazarrasa, ''Blindados en España'', p. 88</ref>
! style="text-align:left;" | M48 Patton<ref>Zaloga, ''The M47 and M48 Patton Tanks'', p. 29</ref>
! style="text-align:left;" | Leopard<ref name=Jerchel28 />
! style="text-align:left;" | AMX-30<ref>Ogorkiewicz, ''AMX-30 Battle Tank'', p. 20</ref>
|-
! style="text-align:right;" | Weight
| 46.44 [[Tonne|t]] (51.19&nbsp;[[short ton]]s)
| 53.5&nbsp;t (58.97&nbsp;tons)
| 42.2&nbsp;t (46.52&nbsp;tons)
| 36&nbsp;t (39.68&nbsp;tons)
|-
! style="text-align:right;" | Gun
| 90&nbsp;mm L/48 M36 rifled (3.54&nbsp;inches)
| 90&nbsp;mm L/48 M41 rifled (3.54&nbsp;in)
| 105&nbsp;mm [[L7A3]] rifled (4.13&nbsp;in)
| 105&nbsp; L/56 F2 rifled (4.13&nbsp;in)
|-
! style="text-align:right;" | Ammunition
| 71 rounds
| 62 rounds
| 55 rounds
| 50 rounds
|-
! style="text-align:right;" | Road&nbsp;range
| {{convert|128|km|mi|abbr=on}}
| {{convert|482.8|km|mi|abbr=on}}
| {{convert|600|km|mi|abbr=on}}
| 500–600&nbsp;km (310.69-372.82&nbsp;mi)
|-
! style="text-align:right;" | Engine output
| 810&nbsp;[[Horsepower|PS]] (595.75&nbsp;kW)
| 750&nbsp;PS (551.62&nbsp;kW)
| 830&nbsp;PS (610&nbsp;kW)
| 680&nbsp;PS (500.14&nbsp;kW)
|-
! style="text-align:right;" | Maximum speed
| 48&nbsp;km/h (29.83&nbsp;mph)
| 48.28&nbsp;km/h (30&nbsp;mph)
| 62&nbsp;km/h (38.53&nbsp;mph)
| 65&nbsp;km/h (40.39&nbsp;mph)
|}

In May 1970, the Spanish government decided to sign a contract with the French company [[GIAT]] to begin production of the AMX-30.<ref name=MazarrasaAMX57 /> However, it was not the advantages of the French vehicle itself that influenced the decision. Rather, it was the UK's unwillingness to sell their L7 tank-gun, the low cost of the AMX-30, and the French offer to allow Spain to manufacture the tank, that led the Spanish Army to favor the French armored vehicle.<ref>Rudnick, ''Atlantic Relations'', pp. 203–204</ref>

==Production==
On 22 June 1970, the Spanish Ministry of Defense signed an agreement of military cooperation with France, which outlined plans for the future acquisition of around 200 tanks for the Spanish Army.  Of these, 180 were to be manufactured under license in Spain and 20 were to be manufactured by France.  Ultimately, GIAT was contracted to manufacture 19 tanks.  These were delivered to the [[Spanish Legion]]'s ''Bakali'' company, deployed in the Spanish Sahara.<ref>Manrique & Molina, ''La Brunete'', p. 69</ref>  The first six AMX-30s were delivered by rail to the Spanish border city of [[Irun|Irún]], in the [[Basque Country (autonomous community)|Basque Country]], and then transferred{{how|date=June 2015}} to Bilbao.  Finally, they were shipped by the [[Spanish Navy]], on the transport ''Almirante Lobo'', to [[El Aaiún]] in the Spanish Sahara.  This unit existed until 1975, when it was disbanded and its tanks transferred to the ''Uad-Ras'' Mechanized Infantry Regiment.<ref>Mazarrasa, ''Carro de Combate AMX-30E'', pp. 58–59</ref>

This agreement laid the foundations for the upcoming tank plant at the industrial polygon of ''Las Canteras'', near the town of [[Alcalá de Guadaíra]].  Several parts of the tank were subcontracted to other Spanish companies, including ''Astilleros Españoles'' (turret), ''Boetticher'', ''Duro Felguera'' and [[E. N. Bazán]].  The grade of local production varied per batch.  The first 20 tanks were to have 18% of each vehicle manufactured in Spain; the next 40 would have 40% of the vehicle manufactured in Spain.  The other 120 had 65% of the tank manufactured in the country.  Production began in 1974, at a rate of five tanks per month, and ended on 25 June 1979.  The first five tanks were delivered to the ''Uad Ras'' Mechanized Infantry Regiment on 30 October 1974.  This batch also replaced the [[M41 Walker Bulldog]] light tanks and [[M48 Patton]] tanks in the Armored Cavalry Regiment ''Villaviciosa'' and the Armored Infantry Regiment ''Alcázar de Toledo'', receiving 23 and 44 tanks, respectively.<ref>Mazarrasa, ''Carro de Combate AMX-30E'', pp. 59–60</ref>

[[Image:AMX30 bustle.jpg|left|thumb|Close-up of the AMX-30E's turret bustle, grenade launchers and commander's hatch]]

On 27 March 1979, prior to the end of production of the first batch, the Spanish Army and Santa Bárbara Sistemas signed a contract for the production and delivery of a second batch of 100 AMX-30Es.<ref name=Mazarrasa60>Mazarrasa, ''Carro de Combate AMX-30E'', p. 60</ref>  In 1980, after the 200th AMX-30E was delivered to the Spanish Army, the tank's patent was awarded to Spain.<ref>Manrique & Molina, ''La Brunete'', p. 73</ref>  This allowed minor modifications to be done to the vehicle without having to consult GIAT. It also meant that the degree of local construction of each vehicle augmented considerably.  Production of the second batch lasted between 1979 and 1983.  By the time production ended, the Spanish Army fielded 299 AMX-30Es (280 produced between 1974 and 1983, and 19 delivered from France in 1970) and 4 training vehicles delivered in 1975.  Santa Bárbara Sistemas also manufactured 18 ''Roland España'' (denominated AMX-30RE) anti-air vehicles and 10 AMX-30D [[armored recovery vehicle]]s.<ref>Mazarrasa, ''Carro de Combate AMX-30E'', pp. 60–61</ref>  The average cost per tank, in the first batch, was 45&nbsp;million [[Spanish peseta|peseta]]s (US$642,800).  Cost per tank increased during the second batch to 62&nbsp;million pesetas (885,700 dollars).<ref name=Mazarrasa60 />

Although brand new, the AMX-30E entered service with automotive issues, including problems with the antiquated 5SD-200D transmission.  Consequently, as the first production batch began to end, the Spanish Army and Santa Bárbara Sistemas began to study possible upgrades.  The main objectives were to increase the power and the reliability of the power pack, an improvement to the tank's firepower and accuracy, as well as to increase the vehicle's ballistic protection and overall survivability.  A number of modernization packages were proposed, including a suggestion to mount the AMX-30E's turret on a Leopard 1's chassis.  Other options included swapping the existing power pack for a new American diesel engine and transmission or exchanging the power pack for a new German diesel engine and transmission.  More austere versions of these same options were offered, pairing the existing [[HS-110]] engine with the already mentioned transmissions.  Another prototype was produced using the Leopard's more modern tracks, and another similar prototype mounted a new 12.7-millimeter (0.5&nbsp;in) [[machine gun]] for the loader's position. France's GIAT also offered to modernize Spain's AMX-30Es to AMX-30B2 standards, a modernization being applied to French AMX-30s.<ref>Mazarrasa, ''Carro de Combate AMX-30E'', pp. 78–80</ref>

===Modernization===
Ultimately, a mixed solution named ''Tecnología Santa Bárbara-Bazán'' (Santa Bárbara-Bazán Technology) (or TSB) was chosen.<ref>Mazarrasa, ''Carro de Combate AMX-30E'', p. 80</ref>  The improvement of the tank's mobility entailed replacing the HS-110 diesel engine with an [[MTU Friedrichshafen|MTU]] 833 Ka-501 diesel engine, producing 850&nbsp;metric&nbsp;horsepower (625&nbsp;kW), and the transmission with a German ZF LSG-3000, compatible with engines of up to 1,500&nbsp;metric&nbsp;horsepower (1103&nbsp;kW).  The first 30 engines were to have 50% of the engine manufactured in Spain; the rest, 73% were to be produced indigenously.<ref>Pérez-Guerra, ''Spanish AMX-30 MBT upgrade program'', p. 500</ref>  This new engine gave the modernized tank a power ratio of 23&nbsp;metric&nbsp;horsepower per tonne (21.13&nbsp;hp/S/T).  The new engine was coupled with the AMX-30B2's improved torsion-bar suspension, which used larger diameter torsion-bars and new shocks.<ref>Mazarrasa, ''Carro de Combate AMX-30E'', p. 82</ref>

[[Image:AMX30 side.jpg|right|thumb|A side view of the AMX-30E on display at El Goloso]]

To improve the tank's firepower, the gun mount around the loader's turret hatch was modified to allow the installation of a 12.7-millimeter (0.5&nbsp;in) machine gun, while the main gun's firepower was augmented through the introduction of the new CETME437A [[Kinetic energy penetrator|armor-piercing, fin-stabilized discarding sabot]] (APFSDS).  The gun's accuracy was improved through the installation of the new Mark&nbsp;9 modification A/D fire control system, designed by [[Hughes Aircraft Company]].  The new system allowed firing during the day and during night operations, and increased the likelihood of a first round impact.  The fire control system was also modernized through the exchange of the old M282 gunner's periscope with a new periscope and a new [[Nd:YAG laser|Nd:YAG]] [[laser rangefinder]].  A new ballistics computer, the NSC-800, was issued, as well as a new digital panel for the gunner, designed and manufactured by the Spanish company INISEL.  The tank commander also received a control unit that allowed the choice of ammunition for the gun and provided information on the ballistics of the round and the target to be engaged.  As a result, the loader received a presentation unit to display information on which round to load into the gun's breech and to communicate ballistic data received, including [[angular velocity]], wind velocity, gun elevation and vehicle inclination.  The fire control system also allowed for the future upgrade to a more sophisticated stabilization system for the tank's main gun.  Survivability improvements included the addition of new steel side-skirts, a new smoke generating system linked to the engine and a new fire suppression system.<ref>Mazarrasa, ''Carro de Combate AMX-30'', pp. 81–83</ref>

One hundred fifty AMX-30Es received this modernization package and were designated AMX-30EM2s.  The program began in 1989 and ended in 1993.<ref name=Mazarrasa85>Mazarrasa, ''Carro de Combate AMX-30E'', p. 85</ref>  Ultimately, Spain's AMX-30EM2s were replaced by brand-new [[B1 Centauro|Centauro]] anti-tank vehicles, which were partially manufactured in Spain, in the early 21st century.<ref>''Defensa firma un contrato de 200 millones de euros con Finmeccanica'', El País</ref>

===Reconstruction===
The other 149 AMX-30Es were reconstructed to improve their mobility.  The reconstruction consisted of the replacement of the original French transmission with the American Allison CD-850-6A.  Furthermore, several parts of the tank, such as the brakes, were renovated in order to bring them up to their original standards.  The CD-850-6A was an automatic transmission, with a triple differential providing two forward velocities and one reverse velocity.  However, the new transmission resulted in a new problem.  The excessive heat produced by the transmission reduced the vehicle's range.  The reconstruction of the 149 AMX-30Es began in 1988, and these were designated AMX-30EM1s.<ref>Mazarrasa, ''Carro de Combate AMX-30E'', pp. 83–85</ref>  In the early 1990s Spain received a large number of [[M60 Patton]] tanks, replacing its fleet of M47s and M48s,<ref>Candil, ''Spain's Armor Force Modernizes'', p. 41</ref> as well as its AMX-30EM1s.<ref>''Defensa gastar a 916 millones en destruir 480 carros de combate'', El País</ref>

==Export==
In the mid-1980s [[Indonesia]] approached Spain in an attempt to procure armaments for the modernization of its armed forces.  Of the possible armaments for sale, Indonesia expressed interest in the procurement of the AMX-30.<ref> Yarnóz, ''Indonesia trata de modernizar su Ejército con armamento español'', El País</ref>  Although this deal fell through, in 2004 the Spanish and [[Colombia]]n governments agreed on the sale of between 33 and 46 second-hand AMX-30EM2s, which had recently been replaced in the Spanish Army.<ref>González, ''España ayuda a Colombia a luchar contra la guerrilla con tanques, obuses y aviones'', El País</ref>  However, the deal was canceled after [[José María Aznar]] was replaced by [[José Luis Rodríguez Zapatero]] as prime minister of Spain—the new Spanish government declared that Spain didn't even have enough AMX-30EM2s in working condition to sell to Colombia.<ref>''El Gobierno del PP vendió aviones y barcos militares a Colombia y Venezuela'', El País</ref>

==References==

===Notes===
{{Reflist|colwidth=30em}}

===Sources===
{{Portal|Tank}}
{{refbegin}}
* {{cite news
 | title = Defensa firma un contrato de 200 millones de euros con Finmeccanica 
 | work = El País
 | language = Spanish
 | date = 12 February 2002
 | url = http://www.elpais.com/articulo/economia/Defensa/firma/contrato/200/millones/euros/Finmeccanica/elpepieco/20020212elpepieco_12/Tes
 | accessdate = 2008-08-12 }}
* {{cite news
 | title = Defensa gastar a 916 millones en destruir 480 carros de combate
 | work = El País
 | language = Spanish
 | date = 27 January 1992
 | url = http://www.elpais.com/articulo/espana/MINISTERIO_DE_DEFENSA/PODER_EJECUTIVO/_GOBIERNO_PSOE_/1989-1993/Defensa/gastar/916/millones/destruir/480/carros/combate/elpepiesp/19920127elpepinac_16/Tes
 | accessdate = 2008-08-12 }}
* {{cite news
 | title = El Gobierno del PP vendió aviones y barcos militares a Colombia y Venezuela
 | work = El País
 | language = Spanish
 | date = 31 March 2005
 | url = http://www.elpais.com/articulo/espana/Gobierno/PP/vendio/aviones/barcos/militares/Colombia/Venezuela/elpepiesp/20050331elpepinac_7/Tes
 | accessdate = 2008-08-12 }}
* {{cite journal
 | last = Candil
 | first = Antonio J.
 | title = Spain's Armor Force Modernizes
 | journal = [[Armor (magazine)|Armor]]
 | pages = 3
 | publisher = US Army Armor Center
 | location = Fort Knox, Kentucky
 | date = 1 January 2006
 }}
*{{cite book
  | last = de Mazarrasa
  | first = Javier
      | title = Blindados en España 2ª Parte: La Dificil Postguerra 1939–1960
  | publisher = Quiron Ediciones
  | year = 1994
  | location = Valladolid, Spain
  | language = Spanish
  | page = 184
  | isbn = 84-87314-10-4}}
*{{cite book
  | last = de Mazarrasa
  | first = Javier
      | title = Carro de Combate AMX-30E
  | publisher = Aldaba Ediciones
  | year = 1990
    | language = Spanish
  | page = 104
  | isbn = 84-86629-29-2}}
* {{cite news
 | last = Gonzáles
 | first = Miguel
 | title = España ayuda a Colombia a luchar contra la guerrilla con tanques, obuses y aviones
 | work = El País
 | language = Spanish
 | date = 26 February 2004
 | url = http://www.elpais.com/articulo/espana/Espana/ayuda/Colombia/luchar/guerrilla/tanques/obuses/aviones/elpepiesp/20040226elpepinac_24/Tes
 | accessdate = 2008-08-12 }}
* {{cite journal
  | last = Hilmes
  | first = Rolf
  | title = Battle Tanks for the Bundeswehr: Modern German Tank Development, 1956–2000
  | journal = Armor
  | issue = January–February 2001
  | pages = 6
  | publisher = Fort Knox: US Army Armor Center
  | date = January 1, 2001
  | issn = 0004-2420}}
*{{cite book
  | last = Jerchel
  | first = Michael
      | title = Leopard 1 Main Battle Tank 1965–1995
  | publisher = Osprey
  | year = 1995
  | location = Oxford, United Kingdom
  | page = 48
  | isbn = 1-85532-520-9}}
*{{cite book
  | last = Lathrop
  | first = Richard
    | author2 = John McDonald
  | title = M60 Main Battle Tank 1960–91
  | publisher = Osprey
  | year = 2003
  | location = Oxford, United Kingdom
  | page = 48
  | isbn = 1-84176-551-1}}
*{{cite book
  | last = Manrique
  | first = José María
    | author2 = Lucas Molina
  | title = La Brunete 1ª Parte
  | publisher = Quiron Ediciones
    | location = Valladolid, Spain
  | language = Spanish
  | page = 80
  | isbn = 84-96016-28-5}}
*{{cite book
  | last = Ogorkiewicz
  | first = Richard M.
      | title = AMX-30 Battle Tank
  | publisher = Profile Publications Ltd.
  | date = December 1973
  | location = Berkshire, United Kingdom
    | isbn = }}
* {{cite journal
  | last = Perez-Guerra
  | first = Jaime
  | title = Spanish AMX-30 MBT upgrade program
  | journal = Jane's International Defence Review
  | publisher = Jane's
  | date = 1 April 1987
        }}
* {{cite journal
 | last = Rudnick
 | first = David
 | title = Atlantic Relations: Policy Co-ordination and Conflict (The Case of the Leopard Tank)
 | journal = International Affairs
 | pages = 11
 | publisher = Royal Institute of International Affairs
 | location = London, United Kingdom
 | date = April 1976
 }}
*{{cite book
  | last = Zaloga
  | first = Steven J.
      | title = The M47 and M48 Patton Tanks
  | publisher = Osprey
  | year = 1999
  | location = Oxford, United Kingdom
  | page = 48
  | isbn = 1-85532-825-9}}
* {{cite news
 | last = Yarnóz
 | first = Carlos
 | title = Indonesia trata de modernizar su Ejército con armamento español
 | work = El País
 | language = Spanish
 | date = 12 March 1985
 | url = http://www.elpais.com/articulo/espana/SERRA/_NARCIS_/PSC-PSOE/INDONESIA/ESPANA/INDONESIA/Indonesia/trata/modernizar/Ejercito/armamento/espanol/elpepiesp/19850312elpepinac_8/Tes
 | accessdate = 2008-08-12 }}
{{refend}}

{{featured article}}

[[Category:Post–Cold War main battle tanks]]
[[Category:Main battle tanks of Spain]]
[[Category:Main battle tanks of the Cold War]]
[[Category:Military vehicles 1970–1979]]

[[es:AMX-30]]