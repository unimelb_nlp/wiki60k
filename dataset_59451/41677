{{Infobox Officeholder
|name                   = Ibrahim Bako
|image                  = 
|width                  = 150px
|office1                = {{small|Acting}} General Officer Commanding, [[1st Division (Nigeria)|1st Mechanized Division]], Nigerian Army
|term_start1            = 
|term_end1              = 
|predecessor1           = 
|successor1             = 
|office2                = 
|term_start2            = 
|term_end2              = 
|predecessor2           =
|successor2             = 
|branch =[[File:Flag of the Nigerian Army Headquarters.svg|21px]] [[Nigerian Army]]
|serviceyears = 1961 – 1983
|rank = [[Brigadier]]
|alma_mater=[[Nigerian Defence Academy|Nigerian Military Training College]]<br>[[Royal Military Academy, Sandhurst|RMA Sandhurst]]
}}

Brigadier '''Ibrahim Bako''' (1941<ref name=Leadership-NG>{{cite web|last1=Augustine|first1=Agbo-Paul|title=We’ve Forgiven Our Father’s Killers – Prof Bako|url=http://leadership.ng/features/interviews/311481/weve-forgiven-our-fathers-killers-prof-bako|website=Leadership Nigeria|publisher=Leadership Nigeria|accessdate=9 January 2015}}</ref> – December 31, 1983) was a senior officer in the [[Nigerian Army]] who played a principal role in two Nigerian military coups: the [[1966 Nigerian counter-coup|July 1966 counter-coup]] and the [[1983 Nigerian coup d'état|December 1983]] coup. The 1983 coup ousted the democratic government of [[Shehu Shagari]] while the July 1966 coup ousted the military government of General [[Ironsi]]. Bako was killed while attempting to arrest President [[Shehu Shagari]] during the [[1983 Nigerian coup d'état|December 1983 coup d'état]].

==Career==
Ibrahim Bako was commissioned into the Nigerian Army in 1963 as a Lieutenant<ref name=Leadership-NG /> after graduating from the [[Royal Military Academy, Sandhurst]]. Bako (then a Lt Colonel) served as a logistics officer on the National Census Board for the 1973 census.<ref name=Siollun15>{{cite book|last1=Siollun|first1=Max|title=Soldiers of Fortune: A History of Nigeria (1983-1993)|publisher=Cassava Republic Press|isbn=9789785023824|pages=15|edition=2013}}</ref> At a point in his career, Ibrahim Bako led the [[Nigerian Army]] contingent that facilitated the transfer of about 100 former guerrillas from the Zimbabwean bushes (after the liberation struggle) for selection and training at the [[Nigerian Defence Academy]], [[Kaduna]] in 1980. Those 100 former guerrillas formed the pioneer corp of the post-independence [[Zimbabwe National Army]].<ref name=Beegeagle /> As of December 31, 1983, Bako was Director of the Army Faculty at the [[Armed Forces Command and Staff College, Jaji]], and acting GOC 1 Mechanised Division, Kaduna.<ref name=Leadership-NG />

==Role in July 28, 1966 coup==
The July 28, 1966 mutiny (often called the [[Nigerian Counter-Coup of 1966]]) was a violent overthrow of General [[Aguiyi-Ironsi]]'s military government, which came into power after the abortive January 15 coup, spearheaded by Major [[Emmanuel Ifeajuna]] and Lt Col [[Kaduna Nzeogwu]]. A group of military officers of northern Nigerian origin (including then Lts Ibrahim Bako, [[Shehu Musa Yar'Adua]], Captain [[Joe Garba]], Lt Col [[Murtala Muhammed]], Lt [[Theophilus Danjuma]], among others) conspired and mutinied against General Ironsi's military government.<ref name=OPV>{{cite book|last1=Siollun|first1=Max|title=Oil, Politics and Violence: Nigeria's Military Coup Culture (1966-1976)|publisher=Algora Publishing|isbn=9780875867106|pages=99}}</ref> Among the casualties of the mutiny were General [[Aguiyi Ironsi]] and Lt Colonel [[Adekunle Fajuyi]]. During reconnaissance for the counter coup, then Lt Col [[Murtala Mohammed]] would drive to Ibadan (where Bako was stationed along with others like then Lt [[Jerry Useni]]), Muhammed would often drive into town from Lagos, pick up Ibrahim Bako and Abdullai Shelleng at a pre-arranged location and drive around without stopping while they discussed their counter-coup plan.<ref name=Gamji-Omoigui>{{cite web|last1=Omoigui|first1=Nowa|title=Operation Aure (2): Planning to Overthrow General Ironsi|url=http://www.gamji.com/nowa/nowa26.htm|website=Gamji|publisher=Gamji|accessdate=5 January 2015}}</ref>

==Role in December 31, 1983 coup==
Ibrahim Bako (then Director of the Army Faculty at the Armed Forces Command and staff College, Jaji)<ref name=Beegeagle /> and acting GOC 1 Mechanised Division, Kaduna,<ref name=Leadership-NG /> was tasked by the coup conspirators with arresting President Shehu Shagari presumably after Shagari's Brigade of Guards had been neutralized (without violence as planned) by Col Tunde Ogbeha. Author [[Max Siollun]] notes that Bako was chosen for the arresting role because Bako's father was a personal friend to Shagari. Unknown to Bako was the fact that the coup plot had been leaked to President Shagari, whose guards were on high alert. After arriving at the Presidential residence (in non-military attire) with an armed detachment to arrest the President,<ref name=Omoiugui>{{cite web|last1=Omoigui|first1=Nowa|title=The palace coup of August 27, 1985  Part I|url=http://www.waado.org/nigerdelta/nigeria_facts/MilitaryRule/Omoigui/PalaceCoup-1985.htm|publisher=Uhrobo Historical Society|accessdate=5 January 2015}}</ref> Bako was shot dead while sitting in the passenger side of a [[Unimog]] utility truck<ref name=Beegeagle>{{cite web|title=Nigerian Army Museum: a slice of Nigerian military history|url=https://beegeagle.wordpress.com/2012/03/24/nigerian-army-museum-a-slice-of-nigerian-military-history/|website=Beegeagle's Blog|publisher=Beegeagle|accessdate=5 January 2015}}</ref> in an ensuing firefight between troops from Bako's detachment and the Brigade of Guards soldiers under the command of Captain Augustine Anyogo.<ref name=Siollun>{{cite book|last1=Siollun|first1=Max|title=Soldiers of Fortune: A History of Nigeria (1983-1993)|publisher=Cassava Republic Press|isbn=9789785023824|pages=16-17|edition=2013}}</ref> The Unimog utility truck that Bako was killed in is on display at the Nigerian Army Museum in [[Zaria]], Nigeria.

The senior military officers involved in the 1983 coup were:<ref name=Siollun13>{{cite book|last1=Siollun|first1=Max|title=Soldiers of Fortune: A History of Nigeria (1983-1993)|publisher=Cassava Republic Press|isbn=9789785023824|pages=13|edition=2013}}</ref>
*Major General [[Muhammadu Buhari]] (General Officer Commanding, 3rd Armored Division, Jos) 
*Major General [[Ibrahim Babangida]] (Director of Army Staff Duties and Plans)
*Brigadier Ibrahim Bako (Brigade Commander)
*Brigadier [[Sani Abacha]] (Commander, 9th Mechanized Brigade)
*Brigadier [[Tunde Idiagbon]] (Military Secretary, Army)
*Lt Colonel Aliyu Mohammed (Director of Military Intelligence)
*Lt Colonel Halilu Akilu
*Lt Colonel David Mark
*Lt Colonel [[Tunde Ogbeha]]
*Major Sambo Dasuki (Military Assistant to the Chief of Army Staff, Lt-General Wushishi)
*Major Abdulmumuni Aminu
*Major Lawan Gwadabe
*Major Mustapha Jokolo (Senior Instructor, Basawa Barracks - Zaria)
*Major Abubakar Umar

Major General Buhari's Supreme Military Council (SMC) observed a minute of silence for the slain Brigadier Bako during SMC's first meeting.<ref name=NYT>{{cite web|last1=May|first1=Clifford|title=DEPOSED NIGERIAN PRESIDENT IS UNDER ARREST|url=https://www.nytimes.com/1984/01/04/world/deposed-nigerian-president-is-under-arrest.html|website=New York Times|publisher=New York Times|accessdate=5 January 2015}}</ref>

==Conflicting accounts of Bako's death during the December 31, 1983 coup==
Bako's son (Professor Ibrahim Ado Bako) claimed in a January 2014 interview with Leadership of Nigeria, that Bako was killed by fellow coup conspirators who were not aligned with Bako's intention to conduct a bloodless coup. Professor Bako asserted that the "top brass of the army and their cohorts did not agree with what he (my father) wanted to do because they wanted the coup to be bloody. They now ordered a Lt. Col to shoot him and he died instantly on the scene at the present Area 1 bridge side in Abuja".<ref name=Leadership-NG />

==References==
{{Reflist}}

{{DEFAULTSORT:Bako, Ibrahim}}
[[Category:Nigerian Army officers]]
[[Category:Participants in the 1966 Nigerian counter-coup]]
[[Category:Participants in the 1983 Nigerian military coup]]
[[Category:Participants of coups in Nigeria]]
[[Category:Instructors at the Nigerian Armed Forces Command and Staff College]]
[[Category:Graduates of the Royal Military Academy Sandhurst]]