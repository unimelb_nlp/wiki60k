<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=CL-475
 | image=Lockheed CL-475.jpg
 | caption=CL-475 flight testing at Edwards AFB
}}{{Infobox Aircraft Type
 | type=Experimental helicopter
 | national origin=United States
 | manufacturer=[[Lockheed Corporation|Lockheed]]
 | designer=Irven Culver
 | first flight=2 November 1959
 | introduced=
 | retired=
 | status=On display
 | primary user=
 | number built=1
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Lockheed CL-475''' (registration N6940C)<ref name="Francillon 1987">Francillon 1987</ref> is a two-seat, single-engine, light helicopter developed by [[Lockheed Corporation|Lockheed]] to explore rigid rotor technology. The CL-475 has a three-bladed main rotor and a two-bladed tail rotor. Only one was built.<ref name="Lockheed" />

==Design and development==
In 1958 Irv Culver presented to the Lockheed management an idea for how to rigidly attach the rotor blades of a helicopter to the hub.  In 1920, [[Juan de la Cierva]] had tried the same concept, but had trouble controlling the rotor, because of excessive gyroscopic moments.  Culver's research led him to believe that there was a way to control the excessive pitch and roll moments by incorporating a feedback system into the rotor. Culver's solution to high control moments was a device known as the "compliance factor".  It kept the blades forward less than a degree, which would apply a corrective feathering input to the opposite blade.  This essentially was the moment feedback system. Before presenting his ideas, he had built a radio-controlled model that demonstrated the feasibility of the concept.  Lockheed gave him use of part of a flight test hangar, a flight test engineer and two mechanics.<ref>Cefaratt, Gil, "Lockheed: The People Behind the Story", Turner Publishing Company, 2002, ISBN 978-1-56311-847-0.</ref>

The CL-475 is a two-seat helicopter with a fabric-covered steel and aluminum structure. The glazed cockpit provides side-by-side seating for two occupants. The landing gear is designed in a tricycle configuration, with two large wheels mounted alongside the bottom of the fuselage, and a nosewheel mounted underneath the cockpit. The helicopter is powered by a 140&nbsp;hp (104&nbsp;kW), four-cylinder, air-cooled [[Lycoming O-360|Lycoming O-360-A1A]] piston engine. Designed to test a rigid-rotor concept, it originally utilized a two-bladed wooden rotor.<ref name="Lockheed" />

After completion at Burbank, the CL-475 was taken to [[Rosamond Lake]] on [[Edwards Air Force Base]] in the [[Mojave Desert]] for testing. It was first flown on 2 November 1959, but the pilot reported severe vibrations. For six months, Lockheed experimented with three and four-bladed wooden rotors, but stability was finally achieved by using metal blades in a three-blade configuration and the addition of a gyroscopic control ring connected directly to the swashplate.<ref name="Francillon 1987"/> In the mid-1960s, the helicopter was test flown by a number of government and military agencies and the military. The stability offered by the rigid rotor control system made the helicopter easy to fly,<ref name="Lockheed" /> and the lessons learned from the CL-475 rigid rotor were later used to develop the XH-51 and AH-56A Cheyenne.

In 1975, Lockheed donated the CL-475 to the [[National Air and Space Museum]]. The helicopter was loaned to the [[United States Army Aviation Museum]] at Fort Rucker, Alabama,<ref name="Francillon 1987"/> but is currently in the museum's storage.<ref name="AAM">United States Army Aviation Museum. ''[http://www.armyavnmuseum.org/museum/collection/rw1.html Rotary Wing Collection]''. United States Army Aviation Museum Association. 2 January 2003. Accessed on 13 July 2009</ref>

==Specifications==
{{aerospecs
|ref=<ref name="Lockheed">Francillon 1982, pp. 414-415</ref>
|met or eng?=<!-- eng for US/UK aircraft, met for all others -->eng

|crew=2
|capacity=
|length m=
|length ft=
|length in=
|span m=
|span ft=
|span in=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->1
|rot dia m=<!-- helicopters -->9.75
|rot dia ft=<!-- helicopters -->32
|rot dia in=<!-- helicopters -->0
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=2.82
|height ft=9
|height in=3
|wing area sqm=
|wing area sqft=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|empty weight kg=737
|empty weight lb=1625
|gross weight kg=907
|gross weight lb=2000
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->

|eng1 number=1
|eng1 type=[[Lycoming O-360|Lycoming VO-360-A1A]] four-cylinder air-cooled piston engine
|eng1 kw=<!-- prop engines -->104
|eng1 hp=<!-- prop engines -->140
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|max speed kmh=145
|max speed mph=90
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|range km=120
|range miles=75
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=610
|ceiling ft=2,000
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->

|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=

|related=<!-- related developments -->
* [[Lockheed XH-51]]

|similar aircraft=<!-- similar or comparable aircraft -->

|lists=<!-- related lists -->
* [[List of rotorcraft]]
}}

==References==
;Notes
{{reflist}}

;Bibliography
{{commons category|Lockheed CL-475}}
{{refbegin}}
* {{cite book |last= Francillon |first= René J. |title=:Lockheed Aircraft since 1913 |year=1982 |publisher=Putnam & Company |location=London |pages= |isbn=0-370-30329-6}}
* Francillon, René J. ''Lockheed Aircraft Since 1913''. Annapolis, Md: Naval Institute Press, 1987. ISBN 978-0-85177-805-1
{{refend}}

{{Lockheed Martin aircraft}}

[[Category:Lockheed aircraft|CL-0475]]
[[Category:United States experimental aircraft 1950–1959]]
[[Category:United States helicopters 1950–1959]]
[[Category:Single-engined piston helicopters]]
[[Category:Rigid rotor helicopters]]