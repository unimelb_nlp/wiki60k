{{Use dmy dates|date=October 2012}}
{{Infobox Australian place
| type                = suburb
| name                = Broadview
| city                = Adelaide
| state               = sa
| image               = 
| caption             =
| pop                 = 3,994
| pop_year = {{CensusAU|2006}}
| pop_footnotes       = <ref name=ABS2011>{{Census 2011 AUS |id =SSC40070 |name=Broadview (State Suburb) |accessdate=21 February 2015 | quick=on}}</ref>
| pop2                = 3,597
| pop2_year = {{CensusAU|2006}}
| pop2_footnotes      = <ref name=ABS2006>{{Census 2006 AUS |id =SSC41181 |name=Broadview (State Suburb) |accessdate=15 April 2011 | quick=on}}</ref>
| est                 = 1915<ref name=Manning>{{cite web |url=http://www.slsa.sa.gov.au/manning/pn/b/b26.htm#broadview |title=Place Names of South Australia |author= |date= |work=The Manning Index of South Australian History |publisher=State Library of South Australia |accessdate=27 April 2011}}</ref>
| postcode            = 5083<ref name=Postcodes>{{cite web |url=http://www.postcodes-australia.com/areas/sa/adelaide/broadview |title=Broadview, South Australia (Adelaide) |author= |date= |work=Postcodes-Australia |publisher=Postcodes-Australia.com |accessdate=27 April 2011}}</ref>
| area                = 
| dist1               = 6
| dir1                = N
| location1           = [[Adelaide city centre]]<ref name=Postcodes/>
| lga                 = [[City of Port Adelaide Enfield]]<br>[[City of Prospect]]
| stategov            = [[Electoral district of Enfield|Enfield]] <small>''(2011)''</small><ref>{{cite web|url=http://www.ecsa.sa.gov.au/apps/news/?year=2010 |title=Electoral Districts - Electoral District for the 2010 Election |author= |date= |work= |publisher=Electoral Commission SA |accessdate=27 April 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110822191822/http://www.ecsa.sa.gov.au/apps/news/?year=2010 |archivedate=22 August 2011 |df=dmy }}</ref>
| fedgov              = [[Division of Adelaide|Adelaide]] <small>''(2011)''</small><ref>{{cite web |url=http://apps.aec.gov.au/esearch |title=Find my electorate |author= |date=15 April 2011 |work= |publisher=Australian Electoral Commission |accessdate=27 April 2011}}</ref>
| near-n              = [[Clearview, South Australia|Clearview]]
| near-ne             = [[Greenacres, South Australia|Greenacres]]
| near-e              = [[Manningham, South Australia|Manningham]]
| near-se             = [[Walkerville, South Australia|Walkerville]]
| near-s              = [[Collinswood, South Australia|Collinswood]]
| near-sw             = [[Nailsworth, South Australia|Nailsworth]]
| near-w              = [[Sefton Park, South Australia|Sefton Park]]
| near-nw             = [[Enfield, South Australia|Enfield]]
}}

'''Broadview''' is a [[Suburbs and localities (Australia)|suburb]] of [[Adelaide]], [[South Australia]]. It is located in the [[City of Port Adelaide Enfield]] and the [[City of Prospect]].

==History==
The suburb was laid out in 1915 by C. H. Angas and K. D. Bowman.<ref name=Manning/>

Broadview Post Office did not open until 20 November 1945 and closed in 1987.<ref name = "Post Office">{{Cite web  | last = Premier Postal History  | title = Post Office List  | publisher = Premier Postal Auctions | url = https://www.premierpostal.com/cgi-bin/wsProd.sh/Viewpocdwrapper.p?SortBy=SA&country= | accessdate = 26 May 2011}}</ref>

==Geography==
Broadview lies astride [[Regency Road, Adelaide|Regency Road]] and has Hampstead Road as its eastern boundary.<ref name=UBD>{{cite book |title=Adelaide and surrounds street directory|last= |first= |authorlink= |coauthors= |edition= 47th|year=2009 |publisher=UBD |location= |isbn=978-0-7319-2336-6 |page= |pages= |url= |accessdate=}}</ref>

==Demographics==
The 2006 Census by the [[Australian Bureau of Statistics]] counted 3,597 persons in Broadview on census night. Of these, 46.4% were male and 53.6% were female.<ref name=ABS2006/>

The majority of residents (74.8%) are of Australian birth, with an additional 4.3% identifying [[England]] as their country of birth.<ref name=ABS2006/>

The age distribution of Broadview residents is similar to that of the greater Australian population. 70.9% of residents were over 25 years in 2006, compared to the Australian average of 66.5%; and 29.1% were younger than 25 years, compared to the Australian average of 33.5%.<ref name=ABS2006/>

==Facilities and attractions==
===Parks===
'''Broadview Oval''' is located between Collingrove Avenue and McInnes Avenue and includes a playground and croquet lawns which are the home to the Broadview Croquet Club.<ref name=UBD/> The oval is the home ground for the Broadview Football Club, the "Tigers", who play in the [[South Australian Amateur Football League]]. Adjacent to the oval and croquet lawns are [[tennis]] courts which is the home of the Broadview Tennis Club.

==Transportation==
===Roads===
The suburb is serviced by [[Regency Road, Adelaide|Regency Road]] and by Hampstead Road, which forms its eastern boundary.<ref name=UBD/>

===Public transport===
Broadview is serviced by public transport run by the [[Adelaide Metro]].<ref name=Metro>{{cite web|url=http://timetables.adelaidemetro.com.au/ttsearch.php |title=Public Transport in Adelaide |author= |date=12 January 2011 |work=Adelaide Metro official website |publisher=Dept. for Transport, Energy and Infrastructure, Public Transport Division |accessdate=27 April 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110426182058/http://timetables.adelaidemetro.com.au/ttsearch.php |archivedate=26 April 2011 |df=dmy }}</ref>

==See also==
*[[List of Adelaide suburbs]]

==References==
{{reflist}}

==External links==
*{{cite web |url=http://www.portenf.sa.gov.au |title=City of Port Adelaide Enfield|author= |date= |work=Official website |publisher=City of Port Adelaide Enfield |accessdate=27 April 2011}}
*{{cite web |url=http://www.prospect.sa.gov.au |title=City of Prospect |author= |date= |work=Official website |publisher=City of Prospect |accessdate=27 April 2011}}

{{Coord|-34.873|138.614|format=dms|type:city_region:AU-SA|display=title}}
{{City of Port Adelaide Enfield suburbs}}
{{City of Prospect suburbs}}

[[Category:Suburbs of Adelaide]]
[[Category:Populated places established in 1915]]