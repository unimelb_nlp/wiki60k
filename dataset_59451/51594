{{Redirect|IPSN||Institut de protection et de sûreté nucléaire}}
'''IPSN''', the '''IEEE/ACM International Conference on Information Processing in Sensor Networks''', is an [[academic conference]] on [[sensor networks]] with its main focus on [[information processing]] aspects of sensor networks. IPSN draws upon many disciplines including [[signal processing|signal]] and [[image processing]], [[information theory|information]] and [[coding theory]], [[computer networking|networking]] and [[Protocol (computing)|protocols]], [[distributed algorithms]], [[wireless communication]]s, [[machine learning]], [[embedded systems]] design, and [[data base]]s and [[information management]].

==IPSN Events==
IPSN started in 2001, and following is a list of IPSN events from 2001 to 2014:
* [http://ipsn.acm.org/2014/ 13th IPSN 2014], Berlin, Germany, April 15–17, 2014
* [http://ipsn.acm.org/2013/ 12th IPSN 2013], Philadelphia, PA, USA, April 8–11, 2013
* [http://ipsn.acm.org/2012/ 11th IPSN 2012], Beijing, China, April 16–19, 2012
* [http://ipsn.acm.org/2011/ 10th IPSN 2011], Chicago, IL, USA, April 12–14, 2011
* [http://ipsn.acm.org/2010/ 9th IPSN 2010], Stockholm, Sweden, April 12–16, 2010
* [http://ipsn.acm.org/2009/ 8th IPSN 2009], San Francisco, California, USA, April 13–16, 2009
* [http://ipsn.acm.org/2008/ 7th IPSN 2008], (Washington U.) St. Louis, Missouri, USA, April 22–24, 2008
* [http://www.cse.wustl.edu/~lu/ipsn07.html 6th IPSN 2007], (MIT) Cambridge, MA, USA, April 25–27, 2007
* [http://www.cs.virginia.edu/~ipsn06/ 5th IPSN 2006], (Vanderbilt) Nashville, Tennessee, USA, April 19–21, 2006
* [http://www.ece.wisc.edu/~ipsn05/ 4th IPSN 2005], (UCLA) Los Angeles, CA, USA, April 25–27, 2005
* [http://ipsn04.cs.uiuc.edu/ 3rd IPSN 2004], (UC Berkeley) Berkeley, CA, USA, April 26–27, 2004
* [http://www.parc.com/events/ipsn03/ 2nd IPSN 2003], (Xerox PARC) Palo Alto, CA, USA, April 22–23, 2003
* [http://www2.parc.com/spl/projects/cosense/csp/index.html CSP Workshop 2001], (Xerox PARC) Palo Alto, CA (see [[IPSN#History|history subsection]] for name explanation)

==Ranking==
Although there is no official ranking of [[academic conference]]s on [[wireless sensor networks]],
IPSN is widely regarded by researchers as one of the two (along with [[SenSys]]) most prestigious conferences focusing on [[sensor network]] research. [[SenSys]] focuses more on system issues while IPSN on  algorithmic and theoretical considerations. The [[acceptance rate]] for 2006 was 15.2% for oral presentations, 25% overall (25 papers +17 poster presentations, out of 165 submissions accepted).

==History==
IPSN started off as a workshop at [[Xerox Palo Alto Research Center]] in 2001, and it was initially called ''Collaborative Signal Processing Workshop'' (CSP Workshop). Following the success of the first event, in 2003, the workshop defined its focus more on [[sensor networks]] and was renamed ''International Workshop on Information Processing in Sensor Networks'' (IPSN). The event kept the name [[acronym]] IPSN from 2003 onwards but the full name changed from ''International Symposium on Information Processing in Sensor Networks'' (2003 - 2004) to ''International Conference on Information Processing in Sensor Networks'' (2005 - 2007). It is expected that IPSN would keep the full name ''International Conference on Information Processing in Sensor Networks'' for the coming years.

==SPOTS Track==
In 2005 IPSN introduced a separate track on '''Sensor Platforms, Tools and Design Methods''' (SPOTS) to the conference. The focus of the IPSN track is more on [[information processing]] [[algorithms]], and the focus of SPOTS track is on platform tools and design methods for [[network embedded sensors]].

==See also==
* [[Wireless sensor network]]

==External links==
*[http://www.informatik.uni-trier.de/~ley/db/conf/ipsn/index.html IPSN Bibliography] (from DBLP)
*[http://www.cs.ucsb.edu/~almeroth/conf/stats/] Networking Conferences Statistics

{{Wireless Sensor Network}}

[[Category:Wireless sensor network]]
[[Category:Computer networking conferences]]