{{Infobox journal
| title = International Journal of Damage Mechanics
| cover = [[File:International Journal of Damage Mechanics front cover image.jpg]]
| editor = Chi L.Chow 
| discipline = [[Engineering]]
| former_names = 
| abbreviation = Int. J. Damage Mech.
| publisher = [[Sage Publications]]
| country = 
| frequency = 8/year
| history = 1992-present
| openaccess =
| license =
| impact = 1.721
| impact-year = 2013
| website = http://www.sagepub.com/journals/Journal201580/title
| link1 = http://ijd.sagepub.com/content/current
| link1-name = Online access
| link2 = http://ijd.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 44486409
| LCCN = 92640135 
| CODEN = IDMEEH
| ISSN = 1056-7895 
| eISSN = 1530-7921
}}
The '''''International Journal of Damage Mechanics''''' is a [[peer-reviewed]] [[scientific journal]] covering the fields of [[engineering]] and [[materials science]]. The [[editor-in-chief]] is Chi L. Chow ([[University of Michigan]]). It was established in 1992 and is published by [[Sage Publications]].

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Academic Search|Academic Search Premier]], [[Current Contents]], the [[Material Science Citation Index]], [[Mechanical Engineering Abstracts]], [[Scopus]], and the [[Science Citation Index Expanded]]. According to the ''[[Journal Citation Reports]]'', its 2013 [[impact factor]] is 1.721, ranking it 92nd out of 251 journals in the category "Materials Science, Multidisciplinary"<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Materials Science, Multidisciplinary |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |work=[[Web of Science]] |postscript=.}}</ref> and 39th out of 138 journals in the category "Mechanics".<ref name=WoS2>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Mechanics |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |work=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.sagepub.com/journals/Journal201580/title}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1992]]
[[Category:Engineering journals]]