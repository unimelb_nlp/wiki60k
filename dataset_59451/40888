'''Robert Henry Lindsay''' (April 23, 1868 – March 19, 1938) was a Canadian painter.

== Personal life ==
Lindsay was born at [[Prescott, Ontario]] on April 23, 1868. His father, George Lindsay, relocated his family to Brockville while Robert was a young child.<ref name="RHL" /> Robert Lindsay married Margaret Ellen Boucher, at [[Carleton Place, Ontario|Carleton Place]] on 30 September 1907.<ref>[http://homepages.rootsweb.ancestry.com/~maryc/lanark07.htm Marriage Notice, Carleton Place, Lanark County, Ontario]</ref> Robert Lindsay was an outdoors-man who like [[cycling]].<ref name="Recorder" /> He was interested in [[Rowing (sport)|rowing]] and was made the first honorary member of the Brockville Rowing Club.<ref name="Recorder" /> Apart from his profession as an artist and a studio painter, he had as a hobby [[wood carving]].<ref name="Recorder" /> Lindsay died at [[Brockville, Ontario]] on March 19, 1938.

==Artist==
From early in his life, Robert Lindsay was interested in art. In addition to his schooling, he attended the Brockville [[Mechanics' Institutes|Mechanics' Institute]]. The Mechanics’ Institute included an art school that provided graphic arts education where Lindsay developed his skills.<ref Name="Library" /> He took a position as a painter in the [http://heritagebrockville.com/industry/node/304 James Smart Manufacturing Company]<ref name="RHL" /> While employed at Smart's he studied painting and sketching under Percy F. Woodcock, R.C.A. at the Brockville Ontario Government School of Art. This led him to a career as a professional artist and painter in oils and watercolour.<ref name="Recorder" /> In 1911 Lindsay became a part-time art instructor at the school and later succeeded Woodcock as the art instructor at the Ontario Government School of Art in Brockville,<ref name="RHL" /><ref>[http://books.google.ca/books?id=SKN_AWPejTsC&pg=PA263&lpg=PA263&dq=From+Drawing+to+Visual+Culture+lindsay&source=bl&ots=NCPmZlAB-5&sig=UBmbSRfMcbkVI4iQWKmSosNM--k&hl=en&sa=X&ei=v-EnUf7RDvSB0QHE4YDAAw&ved=0CC8Q6AEwAA#v=onepage&q=From%20Drawing%20to%20Visual%20Culture%20lindsay&f=false ''From Drawing to Visual Culture, A history of art in Canada''], Harold Pearse, Editor McGill University Press, 2006 Notes to page 100</ref> a position that he held for 12 years.<ref name="Recorder" /> He also taught drawing at the [https://brockvillehistoryalbum.wordpress.com/tag/st-albans-school/ St. Alban's Boys School].<ref name="RHL" /> He sketched with Harold A. Pearl of Toronto<ref>[https://archive.org/stream/annualopenexhibi1923ontauoft/annualopenexhibi1923ontauoft_djvu.txt Exhibition Catalogue Entry] ,Nos.192,193</ref> particularly in the Newboro-Westport District of Leeds County. He maintained a studio in Brockville from which he carried on a commercial art business as well as his artistic endeavours.<ref name="Recorder" />

He was a contributing member<ref name="Recorder">[http://www.rootsweb.ancestry.com/~onbdhs/HistoryMatters0902.pdf Recorder and Times Reprinted Articles]</ref> of the [http://www.theartsclub.org/en/the-club.html Arts Club of Montreal]. He became a member at its inception in 1912.<ref name="RHL">[http://www.pro.rcip-chin.gc.ca/bd-dl/aac-aic-eng.jsp?emu=en.aich:/Proxapp/ws/aich/user/wwwe/Record&upp=0&m=1&w=NATIVE%28%27ARTIST%20ph%20words%20%27%27Robert%20Henry%20Lindsay%27%27%27%29&order=native%28%27every%20AR%27%29&bio=LINDSAYRobertHenry.html Bio in Dictionary of Canadian Artists]</ref> He participated in the [[Montreal Museum of Fine Arts#The beginnings|Montreal Art Association]] Spring Exhibitions from 1911 through to 1934.<ref name="NL&AofC">[http://collectionscanada.gc.ca/ourl/res.php?url_ver=Z39.88-2004&url_tim=2013-03-07T20%3A17%3A20Z&url_ctx_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Actx&rft_dat=179181&rfr_id=info%3Asid%2Fcollectionscanada.gc.ca%3Apam&lang=eng National Archives Info]</ref><ref name="Westb">[http://www.westbridge-fineart.com/auctions/bio.php?artistID=68 Westbridge Bio]</ref> Robert H. Lindsay exhibited both oils and watercolours with the [[Royal Canadian Academy of Arts]],<ref name="Westb"  /> [[Ontario Society of Artists]], and the [[Montreal Museum of Fine Arts#The beginnings|Montreal Art Association]].<ref name="Recorder" /><ref name="Westb" /> In addition to his regular participation in these shows in Montreal and Toronto, he is known to have had shows in Ottawa, Winnipeg, Chicago and, Belfast, Ireland.<ref name="RHL" /><ref name="Recorder" />
Lindsay traveled extensively, painting on the east coast of the United States and in Ireland.<ref name="Recorder" /> During his extensive travels he also made a point of attending famous art galleries and museums to satisfy his love for the aesthetic and to appreciate the scope of artistic achievement.<ref name="Recorder" />

==Brockville Library==
Robert Lindsay, who had the benefit of the Mechanics' Institute library, became a trustee<ref name="Recorder" /> <ref Name="Library" /> of the Brockville library.  When the [[Carnegie library|Carnegie Library]] was constructed as the permanent home of the collections he became the Vice-chair of the Brockville Public Library Board.<ref Name="Library">[http://www.rootsweb.ancestry.com/~onbdhs/brockville_library.html Library History]</ref> He had drawn many pencil sketches of Brockville which are now of historical value. Following his death in 1938 his widow, Margaret Boucher Lindsay, presented about forty of his pencil sketches to the library.<ref name="RHL" />

==Collections==
Works of Robert H. Lindsay have been displayed at:
Exhibitions at D & E Lake Ltd., Toronto Gallery
*  [http://www.delake.com/06_Dec_January_Painted_Panels.pdf Exhibition December 2006 Small Panels]
*  [http://www.delake.com/07_03_April_May_Decades_of_Canadian_Teens___Twenties.pdf Exhibition spring 2007, Works from 1913 - 1929]

Works of Robert H. Lindsay are held at:
*National Library and Archives of Canada<ref name="NL&AofC" />

==See also==
*[[Canadian Art]]
*[[List of Canadian artists]]
*[[List of Canadian painters]]
*[[Carnegie library]]
*[[List of Carnegie libraries in Canada]]

== References ==
<references />

==External images==
* [http://www.askart.com/askart/artist.aspx?artist=126479 Ask Art Image]
* [http://www.westbridge-fineart.com/site/past_auctions_artist.php?artistID=68 Example Works]
* [http://www.arcadja.com/auctions/en/lindsay_robert_henry/artist/42838/ Works on Auction]

==External links==
* [http://www.brockvillerowingclub.ca/history/history.html Brockville Rowing Club]
* [http://www.archivescanada.ca/english/search/ItemDisplay.asp?sessionKey=999999999_142&l=0&lvl=1&v=0&coll=0&itm=264725&rt=1&bill=1 Biographical Sketch]
* [http://www.rootsweb.ancestry.com/~onbdhs/HistoryMatters0902.pdf Article about R.H. Lindsay]
* [http://www.westbridge-fineart.com/auctions/bio.php?artistID=68 Westbridge Gallery Artist Biographical note]
* [http://www.pro.rcip-chin.gc.ca/bd-dl/aac-aic-eng.jsp?emu=en.aich:/Proxapp/ws/aich/user/wwwe/Record&upp=0&m=1&w=NATIVE%28%27ARTIST%20ph%20words%20%27%27Robert%20Henry%20Lindsay%27%27%27%29&order=native%28%27every%20AR%27%29&bio=LINDSAYRobertHenry.html A Dictionary of Canadian Artists]
* [http://www.brockvillelibrary.ca/history.htm Brockville Public Library History]
* [http://www.theartsclub.org/en/the-club.html Arts Club of Montreal]
* [http://ontariosocietyofartists.org/ Ontario Society of Artists]
* [http://www.askart.com/AskART/artists/search/Search_Grid.aspx?searchtype=BOOKS&artist=126479 Books References]

{{DEFAULTSORT:Lindsay, Robert Henry}}
[[Category:1868 births]]
[[Category:1938 deaths]]
[[Category:19th-century Canadian painters]]
[[Category:20th-century Canadian painters]]
[[Category:Artists from Ontario]]
[[Category:People from Leeds and Grenville United Counties]]