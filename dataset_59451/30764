{{Infobox coin
| Country             = United States
| Denomination        = Bust Dollar
| Value               = 1.00
| Unit                = [[United States dollar|U.S. dollars]]
| Mass                = 26.96
| Diameter            = 39–40
| Edge                = Lettered
| Composition =
  {{plainlist |
* 89.2% [[Silver|Ag]]
* 10.8% [[Copper|Cu]]
  }}
| Years of Minting    = 1795–1804
| Obverse             = 1796 dollar obverse.jpg
| Obverse Design      = Bust of [[Liberty (goddess)|Liberty]]
| Obverse Design Date = 1795
| Reverse             = 1796 dollar reverse.jpg
| Reverse Design      = A [[bald eagle]] 
| Reverse Design Date = 1795
| Reverse2             = Bust dollar reverse.jpeg
| Reverse2 Design      = A [[bald eagle]] in heraldic form
| Reverse2 Design Date = 1798
}}

The '''Draped Bust dollar''' is a [[Dollar coin (United States)|United States dollar coin]] minted from 1795 to 1803, and was reproduced, dated 1804, into the 1850s. The design succeeded the [[Flowing Hair dollar]], which began mintage in 1794 and was the first silver dollar struck by the [[United States Mint]]. The designer is unknown, though the distinction is usually credited to artist [[Gilbert Stuart]]. The model is also unknown, though [[Ann Willing Bingham]] has been suggested.

In October 1795, newly appointed [[Director of the United States Mint|Mint Director]] [[Elias Boudinot]] ordered that the legal fineness of .892 (89.2%) silver be used for the dollar rather than the unauthorized fineness of .900 (90%) silver that had been used since the denomination was first minted in 1794. Due largely to a decrease in the amount of silver deposited at the Philadelphia Mint, coinage of silver dollars declined throughout the latter years of the 18th&nbsp;century. In 1804, coinage of silver dollars was halted; the last date used during regular mint production was 1803.

In 1834, silver dollar production was temporarily restarted to supply a [[USS Peacock (1813)#Diplomatic missions|diplomatic mission]] to Asia with a special set of [[proof coin]]s. Officials mistakenly believed that dollars had last been minted with the date 1804, prompting them to use that date rather than the date in which the coins were actually struck. A limited number of [[1804 dollar]]s were struck by the Mint in later years, and they remain rare and valuable.

== Background ==

[[File:Henry William DeSaussure.JPG|thumb|left|upright|Henry William de Saussure was Director of the Mint when production began on the Draped Bust dollar.]]

Coinage began on the first [[Dollar coin (United States)|United States silver dollar]], known as the [[Flowing Hair dollar]], in 1794 following the construction and staffing of the [[Philadelphia Mint]]. The [[Coinage Act of 1792]] called for the silver coinage to be struck in an [[alloy]] consisting of 89.2% silver and 10.8% copper.{{sfn|Julian|1993|p=30}} However, Mint officials were reluctant to strike coins with the unusual fineness, so it was decided to strike them in an unauthorized alloy of 90% silver instead.{{sfn|Julian|1993|p=35}} This caused depositors of silver to lose money when their metal was coined.{{sfn|Julian|1993|p=35}} During the second year of production of the Flowing Hair dollar, it was decided that the denomination would be redesigned.{{sfn|Julian|1993|p=40}} It is unknown what prompted this change or who suggested it, though [[Numismatics|numismatic]] historian R.W. Julian speculates that [[Henry William de Saussure]], who was named [[Director of the United States Mint|Director of the Mint]] on July 9, 1795, may have suggested it, as he had stated a redesign of the American coinage as one of his goals before taking office.{{sfn|Julian|1993|p=219}} It is also possible that the Flowing Hair design was discontinued owing to much public disapproval.{{sfn|Julian|1993|p=40}}

=== Design ===

Though the designer of the coin is unknown, artist [[Gilbert Stuart]] is widely acknowledged to have been its creator;{{sfn|Julian|1993|p=40}} Mint Director [[James Ross Snowden]] began researching the early history of the United States Mint and its coinage in the 1850s, during which time he interviewed descendants of Stuart who claimed that their ancestor was the designer.{{sfn|Julian|1993|p=40}} It has been suggested that Philadelphia socialite [[Ann Willing Bingham]] posed as the model for the coin.{{sfn|Julian|1993|p=41}} Several sketches were approved by Mint engraver [[Robert Scot]] and de Saussure and sent to [[President of the United States|President]] [[George Washington]] and [[United States Secretary of State|Secretary of State]] [[Thomas Jefferson]] to gain their approval.{{sfn|Julian|1993|p=41}}

After approval was received, the designs were sent to artist John Eckstein to be rendered into plaster models;{{sfn|Julian|1993|p=41}} during that time, plaster models were used as a guide to cutting the dies, which was done by hand.{{sfn|Julian|1993|p=41}} Eckstein, who was dismissed by Walter Breen as a "local artistic hack"{{sfn|Breen|1988|p=425}} and described by a contemporary artist as a "thorough-going drudge"{{sfn|Julian|1993|p=41}} due to his willingness to carry out most painting or sculptural tasks at the request of clients, was paid thirty dollars for his work preparing models for both the obverse Liberty and reverse eagle and wreath.{{sfn|Taxay|1983|p=106}} After the plaster models were created, the engravers of the Philadelphia Mint (including Scot) began creating hubs that would be used to make dies for the new coins.{{sfn|Julian|1993|p=41}}

== Production ==

It is unknown exactly when production of the new design began, as precise records relating to design were not kept at that time.{{sfn|Julian|1993|p=41}} R.W. Julian, however, places the beginning of production in either late September or early October 1795,{{sfn|Julian|1993|p=41}} while Taxay asserts that the first new silver dollars were struck in October.{{sfn|Taxay|1983|p=107}} In September 1795, de Saussure wrote his resignation letter to President Washington.{{sfn|Julian|1993|p=42}} In his letter, de Saussure mentioned the unauthorized silver standard and suggested that Congress be urged to make the standard official, but this was not done.{{sfn|Julian|1993|p=42}} In response to de Saussure's letter, Washington expressed his displeasure in the resignation, stating that he had viewed de Saussure's tenure with "entire satisfaction".{{sfn|Whittmore|1897|p=80}} As de Saussure's resignation would not take effect until October, the president was given time to select a replacement.{{sfn|Julian|1993|p=42}}

[[File:Elias Boudinot.jpg|thumb|left|upright|Elias Boudinot assumed his duties as Director of the Mint on October 28, 1795.]]

The person chosen to fill the position was statesman and former congressman [[Elias Boudinot]]. Upon assuming his duties at the Mint on October 28, Boudinot was informed of the silver standard that had been used since the first official silver coins were struck.{{sfn|Julian|1993|p=42}} He immediately ordered that this practice be ceased and that coinage would begin in the 89.2% fineness approved by the Coinage Act of 1792.{{sfn|Julian|1993|p=42}} The total production of 1795 dollars (including both the Flowing Hair and Draped Bust types) totalled 203,033.{{sfn|Yeoman|2010|pp=207–208}} It is estimated that approximately 42,000 dollars were struck bearing the Draped Bust design.{{sfn|Yeoman|2010|p=208}} Boudinot soon ordered that production of minor denominations be increased. Later, assayer Albian Cox died suddenly from a stroke in his home on November 27, 1795, leaving the vital post of assayer vacant.{{sfn|Julian|1993|p=42}} This, together with Boudinot's increased focus on smaller denominations, as well as a lull in private bullion deposits (the fledgling Mint's only source of bullion), caused a decrease in silver dollar production in 1796.{{sfn|Julian|1993|p=43}} The total mintage for 1796 was 79,920,{{sfn|Yeoman|2010|p=208}} which amounts to an approximate 62% reduction from the previous year's total.

Bullion deposits continued to decline, and in 1797, silver dollar production reached the lowest point since 1794 with a mintage of just 7,776 pieces.{{sfn|Yeoman|2010|p=208}} During this time, silver deposits declined to such an extent that Thomas Jefferson personally deposited 300 [[Spanish dollar]]s in June 1797.{{sfn|Julian|1993|p=43}} In April 1797, an agreement was reached between the Mint and the [[First Bank of the United States|Bank of the United States]].{{sfn|Julian|1993|p=44}} The Bank agreed to supply the Mint with foreign silver on the condition that the Bank would receive their deposits back in silver dollars.{{sfn|Julian|1993|p=44}} The Mint was closed between August and November 1797 due to the annual [[yellow fever]] epidemic in Philadelphia; that year's epidemic took the life of the Mint's treasurer, Dr. Nicholas Way.{{sfn|Taxay|1983|p=123}} In November 1797, the Bank deposited approximately $30,000 worth of French silver.{{sfn|Julian|1993|p=44}} In early 1798, the reverse was changed from the small, perched eagle to a heraldic eagle similar to that depicted on the [[Great Seal of the United States]].{{sfn|Julian|1993|p=44}} The agreement reached with the Bank of the United States along with other bullion depositors (including Boudinot) led to an increase in the number of silver dollars coined;{{sfn|Julian|1993|p=44}} mintage for both the small and heraldic eagle types totalled 327,536.{{sfn|Yeoman|2010|p=209}} Mintage numbers for the dollar remained high through 1799, with 423,515 struck that year.{{sfn|Yeoman|2010|p=210}}

Toward the end of the 18th&nbsp;century, many of the silver dollars produced by the Mint were shipped to and circulated or melted in China in order to satisfy the great demand for silver bullion in that nation.{{sfn|Julian|1993|p=45}} In 1800, silver deposits once again began to decline, and the total silver dollar output for that year was 220,920.{{sfn|Yeoman|2010|p=210}} In 1801, following complaints from the public and members of [[United States Congress|Congress]] regarding the lack of small change in circulation, Boudinot began requesting that silver depositors receive smaller denominations rather than the routinely requested silver dollars, in an effort to supply the nation with more small change.{{sfn|Julian|1993|p=46}} Production dropped to 54,454 silver dollars in 1801 and 41,650 in 1802, after Boudinot was able to convince many depositors to accept their silver in the form of small denominations.{{sfn|Yeoman|2010|p=210}} Although silver bullion deposits at the Mint had increased, Boudinot attempted to end silver dollar production in 1803, favoring half dollars instead.{{sfn|Julian|1993|p=46}} Mintage of the 1803 dollar continued until March 1804, when production of silver dollars ceased entirely.{{sfn|Yeoman|2010|p=210}} In total, 85,634 dollars dated 1803 were struck.{{sfn|Yeoman|2010|p=210}} Following a formal request from the Bank of the United States, Secretary of State [[James Madison]] officially suspended silver dollar and [[Eagle (United States coin)|gold eagle]] production in 1806, although minting of both had ended two years earlier.{{sfn|Julian|1993|p=431}}

== 1804 dollars ==

{{main article|1804 dollar}}

[[File:1804 dollar type I obverse.jpeg|thumb|A Class I 1804 dollar]]
In 1831, Mint Director [[Samuel Moore (congressman)|Samuel Moore]] filed a request through the Treasury asking president [[Andrew Jackson]] to once again allow the coinage of silver dollars; the request was approved on April 18.{{sfn|Julian|1993|p=432}} In 1834, [[Edmund Roberts (diplomat)|Edmund Roberts]] was selected as an American commercial representative to Asia, including the kingdoms of [[Muscat, Oman|Muscat]] and [[Rattanakosin Kingdom|Siam]].{{sfn|Julian|1993|p=433}} Roberts recommended that the dignitaries be given a set of [[proof coin]]s.{{sfn|Julian|1993|p=433}} The State Department ordered two sets of "specimens of each kind <nowiki>[of coins]</nowiki> now in use, whether of gold, silver, or copper".{{sfn|Breen|1988|p=431}} Though the minting of dollars had been approved in 1831, none had been struck since 1804.{{sfn|Julian|1993|p=433}} After consulting with Chief Coiner [[Adam Eckfeldt]] (who had worked at the Mint since its opening in 1792), Moore determined that the last silver dollars struck were dated 1804.{{sfn|Julian|1993|p=433}} Unknown to either of them, the last production in March 1804 was actually dated 1803.{{sfn|Julian|1993|p=433}} Since they believed that the last striking was dated 1804, it was decided to strike the presentation pieces with that date as well. It is unknown why the current date was not used, but R.W. Julian suggests that this was done to prevent coin collectors from being angered over the fact that they would be unable to obtain the newly dated coins.{{sfn|Julian|1993|p=433}}

The first two 1804 dollars (as well as the other coins for the sets) were struck in November 1834.{{sfn|Julian|1993|p=433}} Soon, Roberts' trip was expanded to [[Indo-China]] (then known as Annam) and [[Japan]], so two additional sets were struck.{{sfn|Julian|1993|p=433}} The pieces struck under the auspices of the Mint are known as Class I 1804 dollars, and eight of that type are known to exist today.{{sfn|Yeoman|2010|p=211}} Roberts left for his trip in April 1835, and he presented one set each to the [[Sultan of Muscat]] and the [[King of Siam]].{{sfn|Julian|1993|p=434}} The gift to the Sultan of Muscat was part of an exchange of [[diplomatic gift]]s that resulted in the Sultan presenting the [[National Zoological Park (United States)|Washington Zoo]] with a full-grown lion and lioness.{{sfn|Breen|1988|p=431}} Roberts fell ill in [[Bangkok]] and was taken to [[Macao]], where he died in June 1835.{{sfn|Julian|1993|p=434}} Following Roberts' death, the remaining two sets were returned to the Mint without being presented to the dignitaries.{{sfn|Julian|1993|p=434}}

=== Collecting ===

Most coin collectors became aware of the 1804 dollar in 1842, when [[Jacob R. Eckfeldt]] (son of Adam Eckfeldt) and William E. Du Bois published a book entitled ''A Manual of Gold and Silver Coins of All Nations, Struck Within the Past Century''.{{sfn|Julian|1993|p=434}} In the volume, several coins from the Mint's coin cabinet, including an 1804 dollar, were reproduced by tracing a [[pantograph]] stylus over an [[electrotype]] of the coins.{{sfn|Julian|1993|p=434}} In May 1843, numismatist Matthew A. Stickney was able to obtain an 1804 dollar from the Mint's coin cabinet by trading a rare pre-federal United States gold coin.{{sfn|Julian|1993|p=434}} Due to an increase in the demand for rare coins, Mint officials, including Director Snowden, began minting an increasing number of coin restrikes in the 1850s.{{sfn|Julian|1993|p=434}} Several 1804 dollars were struck, and some were sold for personal profit on the part of Mint officials.{{sfn|Julian|1993|p=435}} When he discovered this, Snowden bought back several of the coins.{{sfn|Julian|1993|p=435}} One such coin, which Snowden later added to the Mint cabinet, was struck over an 1857 [[shooting thaler]] and became known as Class II, the only such piece of that type known to exist today.{{sfn|Julian|1993|p=435}} Six pieces with edge lettering applied after striking became known as Class III dollars.{{sfn|Yeoman|2010|p=211}}{{sfn|Julian|1993|p=435}}

By the end of the 19th&nbsp;century, the 1804 dollar had become the most famous and widely discussed of all American coins.{{sfn|Julian|1993|p=465}} In 1867, one of the original 1804 dollars was sold at auction for $750 (${{formatnum:{{Inflation|US|750|1867}}}} today).{{sfn|Julian|1993|p=479}} Seven years later, on November 27, 1874, a specimen sold for $700 (${{formatnum:{{Inflation|US|700|1874}}}} today).{{sfn|Julian|1993|p=479}} In the early 20th&nbsp;century, coin dealer B. Max Mehl began marketing the 1804 dollar as the "King of American Coins".{{sfn|Julian|1993|p=481}} The coins continued to gain popularity throughout the 20th&nbsp;century, and the price reached an all-time high in 1999, when an example [[Coin grading|graded]] Proof-68 was sold at auction for $4,140,000.{{sfn|Yeoman|2010|p=211}}

== References ==

{{reflist|20em}}

== Bibliography ==

{{refbegin}}
* {{cite book
  | last = Breen
  | first = Walter
  | authorlink = Walter Breen
  | year = 1988
  | title = Walter Breen's Complete Encyclopedia of U.S. and Colonial Coins
  | publisher = Doubleday
  | location = New York, NY
  | isbn = 978-0-385-14207-6
  | ref = harv
  }}
* {{cite book
  | last = Julian
  | first = R.W.
  | editor-last = Bowers
  | editor-first = Q. David
  | editorlink = Q. David Bowers
  | year = 1993
  | title = Silver Dollars & Trade Dollars of the United States
  | publisher = Bowers and Merena Galleries
  | location = Wolfeboro, New Hampshire
  | isbn = 0-943161-48-7
  | ref = harv
  }}
* {{cite book
  | last = Taxay
  | first = Don
  | year = 1983
  | origyear = 1966
  | title = The U.S. Mint and Coinage
  | edition = reprint
  | publisher = Sanford J. Durst Numismatic Publications
  | location = New York, NY
  | isbn = 978-0-915262-68-7
  | ref = harv
  }}
* {{cite book
  | last = Whittmore
  | first = Henry
  | year = 1897
  | title = The Heroes of the American Revolution and Their Descendants
  | publisher = The Heroes of the Revolution Publishing Company
  | location = New York, NY
  | url = https://books.google.com/books?id=lxARAQAAMAAJ&printsec=frontcover#v=onepage&q&f=false
  | ref = harv
  }}
* {{cite book
  | last = Yeoman
  | first = R.S.
  | authorlink = R.S. Yeoman
  | year = 2010
  | title = A Guide Book of United States Coins
  | edition = 63rd
  | publisher = Whitman Publishing
  | location = Atlanta, GA
  | isbn = 978-0-7948-2767-0
  | ref = harv
  }}
{{refend}}

{{s-start}}
{{succession box | title = [[United States dollar coin|Dollar Coin of the United States]] (1795–1804) <hr /> {{nobold|Concurrent with:}} <br /> [[Flowing Hair Dollar]] (1795)
| before = [[Flowing Hair Dollar]] | after = [[Gobrecht Dollar]] | years =
}}
{{s-end}}

{{Coinage (United States)}}
{{US currency and coinage}}

{{Featured article}}

[[Category:1795 introductions]]
[[Category:Liberty symbols]]
[[Category:United States dollar coins]]
[[Category:United States silver coins]]