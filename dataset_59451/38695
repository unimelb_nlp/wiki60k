{| {{Infobox ship begin}}
{{Infobox ship image
| Ship image   = [[File:Provence-3.jpg|300px]]
| Ship caption = ''Provence'' in harbor
}}
{{Infobox ship class overview
| Builders = 
| Operators = 
| Class before = {{sclass-|Courbet|battleship|4}}
| Class after =* {{sclass-|Normandie|battleship|4}} (planned)
* {{sclass-|Dunkerque|battleship|4}} (actual)
| Cost = 
| Built range = 1912–1916
| In service range = 
| In commission range = 1916–1953
| Total ships planned = 
| Total ships completed = 3
| Total ships lost = 1
| Total ships retired = 2
}}
{{Infobox ship characteristics
| Hide header = 
| Header caption = 
| Ship type = [[Battleship]]
| Ship displacement =* Normal: {{convert|23936|MT|LT}}
* Full load: {{convert|26000|MT|LT}}
| Ship length = {{convert|166|m|ftin|abbr=on}}
| Ship beam = {{convert|26.9|m|ftin|abbr=on}}
| Ship draft = {{convert|9.8|m|ftin|abbr=on}}
| Ship power =18–24 [[boiler]]s; {{convert|29000|shp|abbr=on|lk=in}}
| Ship propulsion = 4 shafts; steam turbines 
| Ship speed = {{convert|20|kn|lk=in}}
| Ship range = {{convert|4600|nmi|abbr=on|lk=in}} at {{convert|10|kn}}
| Ship crew =*34 officers
*139 petty officers
*1,020 enlisted
| Ship armament =*10 × [[340mm/45 Modèle 1912 gun]]s
*After 1935 (''Lorraine'' only)
*  8 × 340 mm/45 Modèle 1912 guns
* 22 × 1 [[Canon de 138 mm Modèle 1910 Naval gun|138.6&nbsp;mm Mle 1910]] guns
*  4 × {{convert|47|mm|abbr=on}} guns
*  4 × {{convert|450|mm|abbr=on}} [[torpedo tube]]s
*After 1935:
* 14 × 1 [[Canon de 138 mm Modèle 1910 Naval gun|138.6&nbsp;mm Mle 1910]] guns
*  8 × [[Canon de 75 mm modèle 1924|75mm/50 Modèle 1922 gun]]s
| Ship armor =
* [[Belt armor|Belt]]: {{convert|270|mm|abbr=on}}
* [[Deck (ship)|Deck]]s: {{convert|40|mm|abbr=on}}
* [[Conning tower]]: {{convert|314|mm|abbr=on}}
* [[Turret]]s: {{convert|340|mm|abbr=on}}
* [[Casemate]]s: {{convert|170|mm|abbr=on}}
| Ship notes=
}}
|}

The '''''Bretagne''-class battleships''' were the first [[Dreadnought#The "super-Dreadnoughts"|"super-dreadnoughts"]] built for the [[French Navy]] during the [[World War I|First World War]]. The class comprised three vessels: [[French battleship Bretagne|''Bretagne'']], the [[lead ship]], [[French battleship Provence|''Provence'']], and [[French battleship Lorraine|''Lorraine'']]. They were an improvement of the previous {{sclass-|Courbet|battleship|4}}, and mounted ten {{convert|340|mm|abbr=on|1}} guns instead of twelve {{convert|305|mm|abbr=on|0}} guns as on the ''Courbet''s. A fourth was ordered by the [[Greek Navy]], though work was suspended due to the outbreak of the war. The three completed ships were named after French provinces.

The three ships saw limited service during World War I, and were primarily occupied with containing the Austro-Hungarian Navy in the [[Adriatic Sea]]. After the war, they conducted training cruises in the Mediterranean and participated in [[Non-intervention in the Spanish Civil War|non-intervention patrols]] off Spain during the [[Spanish Civil War]]. After the outbreak of [[World War II]], the ships were tasked with convoy duties and anti-commerce raider patrols until the fall of France in June 1940. ''Bretagne'' and ''Provence'' were sunk by the British [[Royal Navy]] during the [[Attack on Mers-el-Kébir]] the following month; ''Provence'' was later raised and towed to [[Toulon]], where she was again [[Scuttling of the French fleet in Toulon|scuttled in November 1942]]. ''Lorraine'' was disarmed by the British in Alexandria and recommissioned in 1942 to serve with the [[Free French Naval Forces]]. She provided gunfire support during [[Operation Dragoon]], the invasion of southern France, and shelled German fortresses in northern France. She survived as a gunnery training ship and a floating barracks until the early 1950s, before being broken up for scrap in 1954. ''Bretagne'' and ''Provence'' were scrapped in 1952 and 1949, respectively.

== Design ==
By 1910, France had yet to lay down a single [[dreadnought battleship]]; Britain had by then completed ten dreadnoughts and five [[battlecruiser]]s, with eight and three more of the two types, respectively, under construction. Germany had built eight dreadnoughts and one battlecruiser and the United States had six built and four more building. Late that year, the French Navy laid down the first of the four {{sclass-|Courbet|battleship|4}} ships. To remedy the inferiority of the French fleet, the government passed the ''Statut Naval'' on 30 March 1912, authorizing a force of twenty-eight battleships, to be in service by 1920. The first three ships were to be laid down in 1912.{{sfn|Jordan & Dumas|p=8}}

The ''Bretagne'' class were replacements for the battleships [[French battleship Carnot|''Carnot'']], [[French battleship Charles Martel|''Charles Martel'']] and [[French battleship Liberté|''Liberté'']]. They were developed from the ''Courbet'' class, and were built with the same [[Hull (watercraft)|hulls]].{{sfn|Gardiner & Gray|p=198}} The primary reason for the decision to use the same hull design as the ''Courbet'' class was limitations of French shipyards. The ''Courbet''-class ships were the largest possible ships that could fit in existing dockyards and refitting basins. The ''Conseil supérieur de la Marine'' (''CSM''), the French naval high command, ordered the construction department to prepare designs for a {{convert|23500|MT|sp=us|adj=on}} ship armed with twelve {{convert|340|mm|abbr=on}} guns in six twin [[gun turret]]s.{{sfn|Dumas|p=74}}

The additional weight of the 340&nbsp;mm turrets compared to the {{convert|305|mm|abbr=on}} of the ''Courbet''-class ships imposed insurmountable problems for the designers. To incorporate six turrets with the same arrangement of the earlier vessels, with four on the [[wikt:centerline|centerline]] in [[superfire|superfiring pairs]] and two wing turrets [[amidships]] would have required an additional {{convert|3000|MT|LT}} displacement as well as a significant increase in the length of the hull. After several other proposals, the ''CSM'' chose a design with five twin turrets, all mounted on the centerline.{{sfn|Dumas|p=74}} This would achieve the same broadside of ten guns, despite the reduction in the number of guns.{{sfn|Whitley|p=40}} The width of the [[armored belt]] was reduced by {{convert|20|mm|abbr=on}} to compensate for the increased weight of the main battery.{{sfn|Dumas|p=74}}

''Provence'' was the first ship of the class to be laid down, which she was on 21 May 1912 at the ''[[Arsenal de Lorient]]''. ''Bretagne'' was laid down at the [[Brest Arsenal|''Arsenal de Brest'']] shipyard in [[Brest, France|Brest]] on 22 July 1912. ''Lorraine'' followed at the ''[[Ateliers et Chantiers de la Loire]]'' shipyard in [[St. Nazaire]] almost six months later on 7 November 1912.{{sfn|Dumas|p=79}} Due to the outbreak of [[World War I]] in the summer of 1914, French industrial capacity was redirected to the army and work slowed on the ships.{{sfn|Hore|p=76}} The [[Greek Navy]] ordered a battleship to be named ''Vasilefs Konstantinos'' to the same design from AC de St Nazaire Penhoet. Work began in June 1914 but ceased on the outbreak of war in August and never resumed. The contract dispute was settled in 1925.{{sfn|Gardiner & Gray|p=384}}

=== General characteristics ===
[[File:Bretagne Brassey's.png|thumb|left|As depicted in Brassey's Naval Annual 1915]]

The ships were {{convert|164.9|m|sp=us}} [[length at the waterline|long at the waterline]] and {{convert|166|m|abbr=on}} [[length overall|long overall]]. They had a [[beam (nautical)|beam]] of {{convert|26.9|m|abbr=on}} and a [[draft (hull)|draft]] of between {{convert|8.9|m|abbr=on}} and {{convert|9.8|m|abbr=on}}.{{sfn|Gardiner & Gray|p=198}} At the designed load, the ships displaced {{convert|23936|MT|LT}}, and at full combat load, this increased to {{convert|26000|MT|LT}}.{{sfn|Dumas|p=79}} The crew included 34 officers, 139 [[petty officer]]s, and 1,020 enlisted men, for a total crew of 1,193. The vessels carried a number of smaller boats, including two {{convert|10|m|abbr=on}} steamboats, three {{convert|11|m|abbr=on}} patrol boats, one {{convert|13|m|abbr=on}} long boat, three {{convert|10.5|m|abbr=on}} [[dinghy|dinghies]], two {{convert|5|m|abbr=on}} dinghies, two {{convert|8.5|m|abbr=on}} whaleboats, and two {{convert|5.6|m|abbr=on}} [[lifeboat (rescue)|lifeboat]]s.{{sfn|Dumas|p=83}}

The ships' propulsion systems consisted of four [[Parsons Marine Steam Turbine Company|Parsons]] [[steam turbine]]s. ''Bretagne'' was equipped with twenty-four [[Niclausse boiler]]s; ''Lorraine'' had the same number of [[Belleville boiler|Belleville]] [[Water-tube boiler|boilers]]. ''Provence'' was equipped with eighteen ''[[Guyot du Temple]]'' boilers. All three ships were coal-fired. The turbines each drove a single [[Screw propeller|screw]] and were rated at a total of {{convert|29000|shp|lk=on}}.{{sfn|Dumas|p=81}} This provided a top speed of {{convert|20|kn|lk=in}}.{{sfn|Gardiner & Gray|p=198}} The four ships could carry {{convert|900|MT|abbr=on}} of coal, though additional spaces could be used for coal storage, for up to {{convert|2680|MT|abbr=on}}. At maximum speed, the ships could steam for {{convert|600|nmi|lk=in}}; at {{convert|13|kn|abbr=on}}, the range increased significantly to {{convert|2800|nmi|abbr=on}} A further reduction in speed to {{convert|10|kn|abbr=on}} correspondingly increased the range to {{convert|4600|nmi|abbr=on}}.{{sfn|Dumas|p=81}}

The ships were modified several times in the interwar period. In 1919, ''Bretagne'' was equipped with a heavy tripod mast; ''Provence'' and ''Lorraine'' were given tripod masts in the early 1920s. Four of ''Bretagne''{{'}}s boilers were converted to oil-firing in 1924, and half of ''Provence''{{'}}s boilers in the rear boiler room were similarly converted in 1927. ''Bretagne'' subsequently had six new oil-fired boilers to replace the rest of the old coal-fired boilers, and her direct drive turbines were replaced with Parsons geared turbines. ''Provence'' was similarly modified in 1931–1934. ''Lorraine''{{'}}s propulsion system was improved in a similar pattern.{{sfn|Whitley|pp=41–42}} In 1935, "Provence" had her center main battery turret was replaced with an aircraft catapult and a hangar for three aircraft. These were initially [[Gourdou-Leseurre]] GL-819 and [[Potez 452]] [[seaplane]]s, though they were replaced with the [[Loire 130]] [[flying boat]]. In March–May 1944, the aircraft installation was removed.{{sfn|Whitley|p=42}}

=== Armament ===
[[File:WWII, Europe, Near Toulon, France, "Weapons ^amp, Fortificcations - Long Range Naval Gun" - NARA - 195329.tif|thumb|right|One of ''Provence''{{'}}s turrets used as a shore battery]]

The ship's main battery consisted of ten [[340mm/45 Modèle 1912 gun]]s in five twin [[gun turret]]s.{{sfn|Dumas|p=83}} The turrets were mounted all on the centerline, with two in a [[superfire|superfiring pair]] forward of the [[conning tower]], one amidships between the two [[wikt:funnel|funnels]], and two superfiring aft of the rear [[superstructure]].{{sfn|Dumas|p=75}} These guns had a maximum elevation of 12&nbsp;degrees, with a range of {{convert|14500|m|abbr=on}}; this was a result of the ''CSM''{{'}}s belief that the decisive battle range would only be {{convert|6000|m|abbr=on}} and that fleets would not engage at ranges longer than {{convert|8000|m|abbr=on}}.{{sfn|Jordan & Dumas|p=12}} ''Lorraine'' was modified in 1917 to increase the elevation of the guns to 18&nbsp;degrees, which correspondingly increased the range to {{convert|21100|m|abbr=on}}. ''Bretagne'' and ''Provence'' were similarly modified after the end of the war in 1919.{{sfn|Jordan & Dumas|p=13}} Each gun was supplied with 100 rounds of ammunition, stored in shell rooms located beneath the [[propellant]] magazines.{{sfn|Whitley|p=41}} The guns were controlled centrally by {{convert|4.57|m|abbr=on}} [[Barr and Stroud|Barr & Stroud]] [[rangefinder]]s; each turret was also equipped with a {{convert|2|m|abbr=on}} independent rangefinder,{{sfn|Dumas|p=83}} which were located on top of the turret roofs in an armored hood.{{sfn|Whitley|p=41}}

Twenty-two [[Canon de 138 mm Modèle 1910 Naval gun|Canon de 138 mm Modèle 1910]] guns were mounted in [[casemate]]s along the length of the ship's hull. They were expected to be used offensively to attack the upper works of enemy battleships, as well as to defend against [[torpedo boat]] attacks.{{sfn|Jordan & Dumas|pp=12–13}} The secondary battery fire control consisted of two central directors four rangefinders, which were located abreast of the superfiring turrets, fore and aft. The ships carried seven [[QF 3 pounder Hotchkiss|47&nbsp;mm M1885 Hotchkiss]] quick-firing guns.{{sfn|Whitley|p=41}} Two were placed on the conning tower and one was placed on each main battery turret. During World War I, a pair of {{convert|75|mm|abbr=on}} guns were added. The ships' armament were rounded out by four {{convert|450|mm|abbr=on}} [[torpedo tube]]s.{{sfn|Dumas|p=83}} The tubes were submerged in the ships' hulls.{{sfn|Whitley|p=39}}

In the interwar period, all three ships had their armament rearranged. In 1919–1920, ''Bretagne'' had the four forwardmost of her 138&nbsp;mm guns removed, along with the 75&nbsp;mm and two of the 47&nbsp;mm guns. In their place, four [[Canon de 75 modèle 1897|75&nbsp;mm mle 1897 guns]] were installed on the forward superstructure. Twenty-four {{convert|8|mm|abbr=on}} [[Hotchkiss machine gun|Hotchkiss]] machine guns were installed on the forecastle deck in 1927. The four rearmost 138&nbsp;mm guns were removed during this refit, along with the 75&nbsp;mm guns, which were replaced with eight [[Canon de 75 mm modèle 1924|75&nbsp;mm mle 1922]] anti-aircraft guns. Sixteen [[13.2 mm Hotchkiss machine gun]]s, in quadruple mounts, were also added.{{sfn|Whitley|pp=41–42}} ''Provence'' had her four forward 138&nbsp;mm guns removed in 1919, and was equipped similarly to ''Bretagne''. In 1931–1934, she received the same eight 75&nbsp;mm guns as ''Bretagne'' did, and in 1940, three quadruple mounts of 13.2&nbsp;mm guns were fitted. ''Lorraine'' followed a similar pattern, though in 1935, her center main battery turret was removed; an aircraft catapult was fitted in its place. At this time, four {{convert|100|mm|abbr=on}} Model M1930 guns were added, along with two of the 13.2&nbsp;mm quadruple mounts. In 1940, the 100&nbsp;mm guns were removed to arm the new battleship [[French battleship Richelieu|''Richelieu'']], and eight 75&nbsp;mm M1922 guns replaced them. In March–May 1944, fourteen {{convert|40|mm|abbr=on}} and twenty-five {{convert|20|mm|abbr=on}} guns in single mounts were added, and the quadruple 13.2&nbsp;mm guns were removed.{{sfn|Whitley|p=42}}

=== Fire control ===
The ''Bretagne''s were provided with {{convert|4.57|m|adj=on}} [[Barr and Stroud]] [[rangefinder]]s. Each turret had {{convert|2|m|adj=on}} rangefinder under an armoured hood at the rear of the turret. Between the wars, [[Fire-control system#Naval fire control|fire-control directors]] were added for the main, secondary and anti-aircraft armament. The rangefinder on the forward superfiring turret was replaced by a {{convert|8.2|m|adj=on}} instrument.{{sfn|Dumas|pp=83, 158, 163–164}}

=== Armor ===
The ships' main armored belt was {{convert|270|mm|abbr=on}} thick amidships and reduced to {{convert|160|mm|abbr=on}} on either end of the ship. Above the belt, the secondary battery casemates were armored with {{convert|170|mm|abbr=on}} thick steel plate. Horizontal protection consisted of three armored decks; the main deck was {{convert|30|to|40|mm|abbr=on}} thick. The upper and lower decks were both 40&nbsp;mm thick. Sloped armor {{convert|70|mm|abbr=on}} thick connected the main deck to the armored belt. Each of the main battery [[barbette]]s that housed the lower turret assemblies were armored with {{convert|248|mm|abbr=on}} thick steel. The forward-most and rear-most turrets had {{convert|340|mm|abbr=on}} thick sides. The superfiring turrets were less well protected, with 270&nbsp;mm thick sides. The amidships turret was the most heavily armored, with {{convert|400|mm|abbr=on}} thick sides. The conning tower was protected with {{convert|314|mm|abbr=on}} thick armor plating.{{sfn|Gardiner & Gray|p=198}} The total weight of armor was {{convert|7614|MT|abbr=on}}.{{sfn|Whitley|p=41}}

== Construction ==
{| class="wikitable plainrowheaders"
|-
! scope="col" | Ship
! scope="col" | Builder
! scope="col" | Laid down
! scope="col" | Launched
! scope="col" | In Service
! scope="col" | Fate
|-
! scope="row" | {{ship|French battleship|Bretagne||2}}
| Arsenal de Brest, [[Brest, France|Brest]]{{sfn|Dumas|p=79}}
| 22 July 1912{{sfn|Dumas|p=79}}
| 21 April 1913{{sfn|Dumas|p=79}}
| 10 February 1916{{sfn|Dumas|p=79}}
| Sunk by the [[Royal Navy]] at [[Attack on Mers-el-Kébir|Mers-el-Kébir]], 4 July 1940{{sfn|Dumas|p=165}}
|-
! scope="row" | {{ship|French battleship|Lorraine||2}}
| AC de la Loire, [[Saint-Nazaire]]{{sfn|Dumas|p=79}}
| 7 November 1912{{sfn|Dumas|p=79}}
| 30 September 1913{{sfn|Dumas|p=79}}
| 10 March 1916{{sfn|Dumas|p=79}}
| Scrapped beginning January 1954{{sfn|Dumas|p=165}}
|-
! scope="row" | {{ship|French battleship|Provence||2}}
| Arsenal de Lorient, [[Lorient]]{{sfn|Dumas|p=79}}
| 21 May 1912{{sfn|Dumas|p=79}}
| 20 April 1913{{sfn|Dumas|p=79}}
| 1 March 1916{{sfn|Dumas|p=79}}
| {{plainlist |
* [[Scuttled]] at [[Scuttling of the French fleet in Toulon|Toulon]], 27 November 1942
* [[Marine salvage|Refloated]], 11 July 1943, and eventually [[ship breaking|scrapped]]{{sfn|Dumas|p=165}}
  }}
|-
! scope="row" | [[List of battleships of Greece#Vasilefs Konstantinos|''Vasilefs Konstantinos'']]
| AC de la Loire, Saint-Nazaire{{sfn|Gardiner & Gray|p=384}}
| 12 June 1914{{sfn|Gardiner & Gray|p=384}}
| —
| —
| Work halted, August 1914{{sfn|Gardiner & Gray|p=384}}
|}

== Service history ==
All three ships of the class entered service with the French Navy in 1916. ''Bretagne'' and ''Lorraine'' were assigned to the 1st Division of the 1st Battle Squadron, while ''Provence'' served as the fleet [[flagship]] for the entirety of the First World War. They were deployed to guard the southern end of the [[Adriatic Sea]], based in [[Argostoli]] and [[Corfu]], to block a possible sortie by the Austro-Hungarian fleet. The three ships largely remained in port, though ''Provence'' was repeatedly used to intimidate the government of Greece, which favored Germany during the war.{{sfn|Whitley|p=42}} In January 1919, after the end of the war, ''Lorraine'' was sent to [[Cattaro]] to guard the Austro-Hungarian fleet. She joined her sisters in [[Toulon]] in June 1919; later that year the ships formed the Eastern Mediterranean Fleet until 1921.{{sfn|Whitley|p=43}}

Financial problems forced the French Navy to reduce its battleship force to four active vessels. ''Lorraine'' and ''Provence'' were reduced to reserve status in 1922, and the latter went into drydock for a major overhaul.{{sfn|Whitley|p=43}} ''Lorraine'' returned to service with the Mediterranean Squadron in 1923.{{sfn|Whitley|p=44}} ''Bretagne'' remained in service and conducted training cruises in the Mediterranean and along the coast of North Africa during the 1920s and 1930s. In 1934, ''Bretagne'' and ''Provence'' were assigned to the 2nd Squadron, based on France's Atlantic coast. In 1936, they joined the [[Non-intervention in the Spanish Civil War|non-intervention patrols]] off Spain during the [[Spanish Civil War]].{{sfn|Whitley|p=43}} At the outbreak of [[World War II]] in September 1939, ''Bretagne'' and ''Provence'' were based in Toulon with the 2nd Squadron, while ''Lorraine'' was assigned to the Atlantic Squadron.{{sfn|Whitley|pp=43–44}}

[[File:Provence-1.jpg|thumb|left|US Navy recognition photo of ''Provence'']]

After undergoing a refit in the early months of the war, ''Provence'' conducted anti-raider patrols with Force Y, based in [[Casablanca]]. ''Bretagne'' was also overhauled early in the war; in March 1940 she escorted a convoy to [[Halifax Regional Municipality|Halifax]] and returned with another convoy loaded with aircraft for the French Air Force. She was then ordered to join ''Lorraine'' in Force X, to be based in [[Alexandria]] to operate in concert with the British [[Mediterranean Fleet]]. Two weeks later, ''Bretagne'' was instead ordered to steam at high speed to [[Bizerte]], to join the [[Force de Raid]] with ''Provence''. The ships put in at [[Mers-el-Kébir]] and remained there until after the fall of France in June 1940.{{sfn|Whitley|p=44}} To prevent the ships of the French Navy from falling into the hands of the occupying Germans, British [[Prime Minister]] [[Winston Churchill]] ordered the neutralization of all French warships. [[Force H]] was to deliver an ultimatum to the ships based at Mers-el-Kébir; on 3 July, the British squadron arrived outside the harbor and demanded that the ships sail with them to Britain or they would be sunk.{{sfn|O'Hara et al.|pp=31–32}}

The British and French negotiated for several hours, and culminated in the British [[Attack on Mers-el-Kébir|opening fire on the French fleet]]. In the span of ten minutes, ''Bretagne'' was sunk and ''Provence'' was badly damaged.{{sfn|O'Hara et al.|p=32}} ''Bretagne'' was hit by at least four {{convert|15|in|abbr=on}} shells from {{HMS|Hood|51|6}}, {{HMS|Resolution|09|2}} and {{HMS|Barham|04|2}} and exploded, killing the vast majority of her crew. ''Provence'' was set on fire and sank to the bottom of the harbor,{{sfn|Whitley|p=44}} though she was subsequently raised and transferred to Toulon, where she was later [[Scuttling of the French fleet in Toulon|scuttled in 1942]] to prevent her from being seized by the Germans. They nevertheless salvaged the ship starting in July 1943.{{sfn|Gardiner & Chesneau|p=257}} Two of her main guns were emplaced as coastal batteries outside Toulon. ''Lorraine'' was disarmed in Alexandria until December 1942, when she joined the [[Free French Naval Forces]]. She served as a training ship for much of 1943 until a major refit at the end of the year to prepare her to participate in [[Operation Dragoon]], the invasion of southern France. She provided gunfire support during the landings before steaming to Britain for a minor refit. She remained in Britain until March 1945, when she bombarded German-held fortresses in northern France.{{sfn|Whitley|p=44}}

After the end of the war, ''Lorraine'' served as a gunnery training ship in Toulon. She was then used as a [[barracks ship]] until February 1953, when she was stricken from the [[naval register]] and sold for scrapping at the end of the year. She was broken up for scrap outside Toulon the following year. ''Bretagne'' remained at the bottom of Mers-el-Kébir until she was raised for scrapping in 1952 and broken up. ''Provence'' was raised in April 1949 and scrapped.{{sfn|Whitley|p=44}}

==See also==
{{portal|Battleships}}
*[[List of ships of the Second World War]]
*[[List of ship classes of the Second World War]]

== Footnotes ==
{{reflist|20em}}

== References ==
{{Commons category|Bretagne class battleships}}

* {{cite book
  | last = Dumas
  | first = Robert
  | year = 1986
  | editor-last = Lambert
  | editor-first = Andrew D
  | chapter = The French Dreadnoughts: The 23,500 ton Bretagne Class
  | title = Warship
  | volume = X
  | pages = 74–85, 158–165
  | publisher = Conway Maritime Press
  | location = London
  | isbn = 978-0-85177-449-7
  | ref = {{sfnRef|Dumas}}
  }}
* {{cite book
  | last1 = Dumas
  | first1 = Robert
  | last2 = Guiglini
  | first2 = Jean
  | year = 1980
  | language = French
  | title = Les cuirassés français de 23,500 tonnes
  | publisher = Editions de 4 Seigneurs
  | location = Grenoble, France
  | oclc = 7836734
  | ref = {{sfnRef|Dumas & Guiglini}}
  }}
* {{cite book
  | editor1-last = Gardiner
  | editor1-first = Robert
  | editor2-last = Chesneau
  | editor2-first = Roger
  | year = 1980
  | title = Conway's All the World's Fighting Ships, 1922–1946
  | publisher = Naval Institute Press
  | location = Annapolis, MD
  | isbn = 978-0-87021-913-9
  | ref = {{sfnRef|Gardiner & Chesneau}}
  }}
* {{cite book
  | editor1-last = Gardiner
  | editor1-first = Robert
  | editor2-last = Gray
  | editor2-first = Randal
  | year = 1985
  | title = Conway's All the World's Fighting Ships: 1906–1921
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-0-87021-907-8
  | ref = {{sfnRef|Gardiner & Gray}}
  }}
* {{cite book
  | last = Hore
  | first = Peter
  | year = 2006
  | title = Battleships of World War I
  | location = [[London]]
  | publisher = Southwater Books
  | isbn = 978-1-84476-377-1
  | ref = {{sfnRef|Hore}}
  }}
* {{cite book
  | last1 = Jordan
  | first1 = John
  | last2 = Dumas
  | first2 = Robert
  | year = 2009
  | title = French Battleships 1922–1956
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-1-59114-416-8
  | ref = {{sfnRef|Jordan & Dumas}}
  }}
* {{cite book
  | last1 = O'Hara
  | first1 = Vincent P.
  | last2 = Dickson
  | first2 = W. David
  | last3 = Worth
  | first3 = Richard
  | year = 2010
  | title = On Seas Contested: The Seven Great Navies of the Second World War
  | publisher = Naval Institute Press
  | location = Annapolis, MD
  | isbn = 978-1-59114-646-9
  | ref = {{sfnRef|O'Hara et al.}}
  }}
* {{cite book
  | last = Whitley
  | first = M. J.
  | year = 1998
  | title = Battleships of World War II
  | location = Annapolis
  | publisher = Naval Institute Press
  | isbn = 978-1-55750-184-4
  | ref = {{sfnRef|Whitley}}
  }}

{{Bretagne class battleship}}
{{WWIFrenchShips}}
{{WWIIFrenchShips}}
{{Good article}}

[[Category:Battleship classes]]
[[Category:Bretagne-class battleships| ]]
[[Category:World War I battleships of France|Bretagne class battleship]]
[[Category:World War II battleships of France|Bretagne class battleship]]