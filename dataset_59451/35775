{{good article}}
{{Use mdy dates|date=March 2017}}
{{Infobox single
| Name = Adam's Song
| Cover = Blink-182 - Adam's Song cover.jpg
| Alt = A photograph, designed like a film strip, of three men standing in a room illuminated by a solitary lightbulb.
| Artist = [[Blink-182]]
| from Album = [[Enema of the State]]
| Released = {{start date|2000|9|5}}
| Format = {{flat list|
*[[CD single]]
*[[DVD]]
}}
| Recorded = 1999
| Genre = <!--- All of these genres have at least one source. Please do NOT remove any of these genres. --->{{flat list|
*[[Alternative rock]]
*[[pop punk]]
}}
| Length = 4:09
| Label = [[Music Corporation of America|MCA]]
| Writer = {{flat list|
*[[Mark Hoppus]]
*[[Tom DeLonge]]
}}
| Producer = [[Jerry Finn]]
| Last single = "[[All the Small Things]]"<br />(2000)
| This single = "'''Adam's Song'''"<br>(2000)
| Next single = "[[Man Overboard (Blink-182 song)|Man Overboard]]"<br>(2000)
| Misc =
}}
"'''Adam's Song'''" is a song recorded by the American [[rock music|rock]] band [[Blink-182]] for its third studio album, ''[[Enema of the State]]'' (1999). It was released as the third and final single from ''Enema of the State'' on September 5, 2000 through [[MCA Records]]. "Adam's Song" shares writing credits between the band's guitarist [[Tom DeLonge]] and bassist [[Mark Hoppus]], but Hoppus was the primary composer of the song. The track concerns [[suicide]] and [[Depression (mood)|depression]]. It incorporates a [[piano]] in its bridge section, and was regarded as one of the most serious songs the band had written to that point.

Hoppus was inspired by the loneliness he experienced while on tour; while his bandmates had [[significant other]]s to return home to, he was single. He was also influenced by a teen suicide letter he read in a magazine. The song takes the form of a suicide note, and contains lyrical allusions to the [[grunge]] band [[Nirvana (band)|Nirvana]]. "Adam's Song" was one of the last songs to be written and recorded for ''Enema of the State'', and it was nearly left off the album. Though Hoppus worried the subject matter was too depressing, his bandmates were receptive to its message. The song was produced by [[Jerry Finn]].

"Adam's Song" peaked at number two on the US ''[[Billboard (magazine)|Billboard]]'' [[Hot Modern Rock Tracks]] chart; it was also a top 25 hit in [[Canada]] and [[Italy]], but did not replicate its success on other charts. It received praise from music critics, who considered it a change of pace from the trio's more lighthearted singles. The single's [[music video]], a hit on [[MTV]], was directed by [[Liz Friedlander]]. Though the song was intended to inspire hope to those struggling with depression, it encountered controversy when a student of [[Columbine High School]] committed suicide with the track on repeat in 2000.

==Background==
{{Quote box |width=30em |align=left | quote = Tom and Travis always had girlfriends waiting back home, so they had something to look forward to at the end of the tour. But I didn't, so it was always like, I was lonely on tour, but then I got home and it didn't matter because there was nothing there for me anyway. | source =—[[Mark Hoppus]], reflecting on writing "Adam's Song"<ref name="rstruth"/>}}
Beginning in the summer of 1997, Blink-182 would enter an extended period of touring to support their second studio album, ''[[Dude Ranch (album)|Dude Ranch]]''. The group had played a handful of dates on the [[List of Warped Tour lineups by year|Vans Warped Tour 1996]], a lifestyle tour promoting skateboarding and [[punk rock]] music. However, upon ''Dude Ranch''{{'s}} release and popularity, Blink-182 would play every date of the 1997 tour worldwide with the bands [[NOFX]] and [[Social Distortion]].{{sfn|Hoppus|2001|p=79}} The group were gone from their hometown of [[San Diego]] for nearly nine months straight beginning in late 1997.{{sfn|Hoppus|2001|p=80}} "When we did our longest tour stretch, it was right when I started dating my fiancee," recalled vocalist/guitarist [[Tom DeLonge]]. "We were all new and in love, and I had to leave. It was just, "Hey, I'll see you in nine months." It was really hard."{{sfn|Hoppus|2001|p=81}}

[[File:Mark Hoppus 2004.jpg|thumb|125px|right|Bassist [[Mark Hoppus]], the song's lyricist, was inspired by a teen suicide note as well as touring-related loneliness.]]

Bassist Mark Hoppus penned "Adam's Song" to vent these frustrations and the loneliness he experienced on the tour; while the other members had longtime girlfriends to return home to, Hoppus was single.{{sfn|Hoppus|2001|p=83}} "When you're on tour, you're so lonely," Hoppus said. "You hang out with all your bros and it's a great time and everything, but everybody wants to come home and have a girlfriend. And every time we'd fly home, Tom and [former drummer] [[Scott Raynor|Scott [Raynor]]] always had girlfriends waiting for them at the airport, and I didn't. It's about me being depressed and lonely out on tour, and not really having anything to come home to."<ref name=Tempo>{{cite web|author =Woodlief, Mark|url=http://www.mtv.com/news/871773/blink-182-slow-down-tempo-speed-up-charts/|title=Blink-182 Slow Down Tempo, Speed Up Charts|publisher=MTV|accessdate=March 18, 2015}}</ref> The couplet "I couldn't wait til I got home/To pass the time in my room alone" originally ended "to get off the plane alone."<ref name="rstruth">{{cite web|last=Edwards|first=Gavins|url=http://www.rollingstone.com/music/news/the-half-naked-truth-about-blink-182-20000803|title=The Half Naked Truth About Blink-182|publisher=''[[Rolling Stone]]''|date=August 3, 2000|accessdate=July 18, 2012}}</ref>

Hoppus said the song's inspiration came from "reading a magazine where some teenage kid had killed himself and left a letter for his family."<ref name="lat2">{{cite news|title=Psst&nbsp;... Blink-182 Is Growing Up|work=[[Los Angeles Times]]|date= May 30, 1999|author =Hochman, Steve|accessdate=March 24, 2016|url=http://articles.latimes.com/1999/may/30/entertainment/ca-42373}}</ref> Untrue online rumors purported that the song was inspired by a friend from Hoppus' high school years who committed suicide, or a play titled ''Adam's Letter'' that has the same focus.{{sfn|Shooman|2010|p=76}} A fictional suicide note was a part of the script for ''Adam's Letter'', a play that wasn't written until two years after "Adam's Song" came out. John Cosper, the writer behind ''Adam's Letter'', said: {{quote|"The play was written in 2001 with a different title, ''Final Word''. It was renamed and released on the web as ''Adam's Letter'' in around 2005. The actual suicide note was written by me as a part of the play script. As it was written two years after the blink song, there is absolutely no connection whatsoever between the note and the writing of the song. By the same token, the naming of the central character was a coincidence. The name goes back to the original script; I had no knowledge of Blink-182 or their music at that time."{{sfn|Shooman|2010|p=76}}}}
In his memoir ''Can I Say'', drummer [[Travis Barker]] wrote that the song's title was taken from a "sketch on ''[[Mr. Show]]'' about a band that writes a song with that name encouraging one particular fan to kill himself."{{sfn|Barker|Edwards|2015}} [[David Cross]], co-creator of ''Mr. Show'', confirmed this, commenting, "They were fans of the show and that was a knowing tribute that I thought was pretty cool."<ref>{{cite news|title=Monsters of Mock: David Cross on the Music of "Mr. Show"|date=July 23, 2015|publisher=''[[Pitchfork Media]]''|author =Berman, Stuart|accessdate=March 24, 2016|url=http://pitchfork.com/features/interview/9690-monsters-of-mock-david-cross-on-the-music-of-mr-show/}}</ref>

==Recording and production==
"Adam's Song" was among the last tracks composed and recorded for ''Enema of the State'', and was nearly absent from the final album. The band were halfway finished with recording when Hoppus developed the idea.<ref name="kerrang15">{{cite journal|title=Blink-182: Inside ''Enema''|pages=24–25|date=September 16, 2015|work=[[Kerrang!]]|issue=1586}}</ref> Though he worried it was "a bit too far and depressing for what we were trying to do," his bandmates were receptive towards the idea:

{{quote|"I remember the day I played ["Adam's Song"] for Tom and Travis, and they were like, "Wow, that's a pretty heavy song. It's really good." [There] was never even a question of whether or not to put it on the record, or was that a "real" Blink song, or was that the right direction for us to go. Whatever song we write, if it's a good song, we'll put it on the record. If we wanted to write a song about — I don't know, people starving somewhere, we would."<ref name="enemaofthestage">{{cite web |url=http://www.mtv.com/bands/archive/b/blink00/index3.jhtml |title=MTV Music – blink–182: enema of the stage |publisher=[[MTV News]]|author =John Norris|archiveurl=https://web.archive.org/web/20021230040315/http://www.mtv.com/bands/archive/b/blink00/index3.jhtml|archivedate=December 30, 2002|accessdate=March 24, 2016}}</ref>}}

Although vocals would usually take many alternate takes to complete, Hoppus completed much of the vocal track for "Adam's Song" in a single take. "It's in a pretty high register for me, so I just blasted it out one night after dinner. That's, like, 90 per cent of what's on the final track," he told ''[[Kerrang!]]''.<ref name="kerrang15"/> The idea to include [[piano]] in the track came without much forethought; "We realized, 'Well, this part here could sound rad if we put piano in here.' So we tried it out, and it sounded rad," said Hoppus.<ref name=Tempo/> The piano was performed by session musician [[Roger Joseph Manning, Jr.]],<ref name="linernotes">{{cite AV media notes | title=Enema of the State | year=1999 | others=[[Blink-182]] | type=liner notes | publisher=[[MCA Records]] | location=[[United States|US]] | id=MCD 11950}}</ref> best known for his work with [[Beck]].<ref>{{cite news|url=http://www.washingtontimes.com/news/2015/feb/3/roger-manning-jr-from-jellyfish-to-beck-grammy-nom/?page=all|title=Roger Manning Jr.: Jiggling from Jellyfish to the Grammys with Beck|work=[[The Washington Times]]|date=February 3, 2015|accessdate=March 23, 2015|author =Keith Valcourt}}</ref>

==Composition==
{{Listen
 |filename     = Adam's Song.ogg
 |title        = "Adam's Song"
 |description  = A sampling of the song's second verse, illustrating the song's suicidal lyrics and Barker's drum pattern
}}
"Adam's Song" was a departure from the content of the band's previous singles, in favor of a slower tempo and more depressing lyrics.<ref name=Tempo /> Brian Wallace of [[MTV]] wrote that Blink-182 "explores new ground on "Adam's Song," setting aside their normal pop-punk punch for a more [[emo]]-influenced approach."<ref name=Brian>{{cite web |url=http://www.mtv.com/news/515428/blink-182-clean-up-their-act-on-new-lp/ |title=Blink-182 Clean Up Their Act On New LP |publisher=[[MTV News]] |last=Wallace |first=Brian |date=June 21, 1999}}</ref> The song is a [[pop punk]]<ref>{{cite web |url=http://www.stubhub.com/blink-182-tickets/performer/1247/ |title=Blink 182 Tickets |publisher=[[StubHub!]] |quote=In 1999, Blink-182 released Enema of the State, which spawned some of the greatest pop punk anthems of all time, such as "Adam's Song," "What's My Age Again," and "All the Small Things."}}</ref> and [[alternative rock]]<ref>{{cite web|author =Michael Gallucci|url=http://diffuser.fm/2000-alt-rock-videos/|title=Top 10 Alt-Rock Videos From 2000|work=Diffuser.fm|accessdate=March 18, 2015}}</ref> track composed in the [[Key (music)|key]] of [[C major]] and is set in [[time signature]] of [[common time]] with a [[tempo]] of 136 [[beats per minute]]. Hoppus' vocal range spans from G<sub>3</sub> to G<sub>4</sub>.<ref name=sheet>{{cite web|url=http://www.musicnotes.com/sheetmusic/mtdFPE.asp?ppn=MN0063900&|title=Blink-182 "Adam's Song" Guitar Tab|work=Music Notes|publisher=[[EMI Music Publishing]]|accessdate=February 24, 2014}}</ref>

The song begins with the narrator contemplating suicide with the lyrics "I never thought I'd die alone."<ref>{{cite news|title=Blink Song Played At Suicide|date=May 8, 2000|author =Craig Rosen|publisher=[[LAUNCH Media]]}}</ref> The lyrics continue: "I'm too depressed to go on / You'll be sorry when I'm gone."<ref name=Tempo/> "Adam's Song" includes a reference to "[[Come as You Are (Nirvana song)|Come as You Are]]" by [[Nirvana (band)|Nirvana]]. "Come as You Are" by Nirvana includes the lyrics "Take your time, hurry up, the choice is yours, don't be late". "Adam's Song", in turn, includes the lyrics "I took my time, I hurried up, The choice was mine, I didn't think enough".<ref>{{cite web |url=http://www.usatoday.com/story/life/music/2014/04/09/nirvana-influences-lil-wayne-adam-levine-demi-lovato/7500695/ |title=How Nirvana begat Lil Wayne&nbsp;... and Demi Lovato? |publisher=''[[USA Today]]'' |authors=Korina Lopez, David Oliver |date=April 9, 2014}}</ref>

Barker's drum track was labeled by [[Drummerworld]] as "one of the most creative beats of his career," and mainly consists of the same basic beat repeated in sections throughout the verses.<ref name="drums"/> The first measure begins with the [[kick drum]] and [[splash cymbal]] playing on the downbeat, followed by a hit on the bell of the [[ride cymbal]] on beat two, preceding an open [[Hi-hat (instrument)|hi-hat]] that rings out for a full count on beat three.<ref name="drums"/> "The kick, [[Snare drum|snare]], and [[floor tom]] are all hit simultaneously on beat four, followed by [[floor tom]] hits on the last two sixteenth-note triplets of beat four."<ref name="drums"/> The snare is hit on beats two and four, respectively.<ref name=drums>{{cite web|url=http://www.drummerworld.com/Drumclinic/travisbarkeradam.html|title=Transcription of "Adam's Song" from Enema of the State|publisher=[[Drummerworld]].com|author =Rich Lackowski|accessdate=February 24, 2014}}</ref> The song "gradually builds to a powerful, piano-laden [[crescendo]],"<ref name=Brian /> and the song's final chorus and conclusion take a more uplifting view of the world: "Tomorrow holds such better days / Days when I can still feel alive/ When I can't wait to get outside."<ref name=Tempo/> DeLonge noted that over six guitar parts were recorded for the "gigantic, sad" choruses, but upon [[Audio mixing (recorded music)|mixing]], only four were used. "The extra ones didn't really do anything besides make it a little more unclear what was going on."<ref>{{cite news|title=Greatest Guitar Albums of All Time: ''Enema of the State''|work=[[Guitar World]]|date=August 2006}}</ref>

==Commercial performance==
"Adam's Song" was mainly a commercial success in the United States, but it also was a top 25 hit in Canada and Italy as well. In the US, it debuted on ''Billboard''{{'s}} [[Modern Rock Tracks]] chart at number 38 in the issue dated March 18, 2000.<ref name="bb">{{cite journal|title=Modern Rock Tracks|work=[[Billboard (magazine)|Billboard]]|page=79|volume=112|issue= 12|issn=0006-2510|date=March 18, 2000|url=https://books.google.com/books?id=nA4EAAAAMBAJ&pg=PA79|accessdate=March 24, 2016}}</ref> Over the following weeks, it gradually ascended the chart to a peak of number two in the issue dated April 29.<ref name="bb1">{{cite journal|title=Modern Rock Tracks|work=[[Billboard (magazine)|Billboard]]|page=91|volume=112|issue= 18|issn=0006-2510|date=April 29, 2000|url=https://books.google.com/books?id=EA8EAAAAMBAJ&pg=PA91|accessdate=March 24, 2016}}</ref> It remained at that position for seven weeks, held off the top position by "[[Otherside]]" by the [[Red Hot Chili Peppers]],<ref name="bb1"/> and "[[Kryptonite (3 Doors Down song)|Kryptonite]]" by [[3 Doors Down]].<ref name="bb2">{{cite journal|title=Modern Rock Tracks|work=[[Billboard (magazine)|Billboard]]|page=159|volume=112|issue= 22|issn=0006-2510|date=May 27, 2000|url=https://books.google.com/books?id=WQ8EAAAAMBAJ&pg=PA159|accessdate=March 24, 2016}}</ref> On May 13, the single peaked at number one on the [[Bubbling Under the Hot 100]] chart.<ref>{{cite news|title=Bubbling Under the Hot 100: May 13, 2000|work=Billboard|url=http://www.billboard.com/biz/charts/2000-05-13/bubbling-under-hot-100-singles}}</ref> In ''[[CMJ New Music Report]]'', a trade magazine that contained exclusive charts of non-commercial and college radio airplay and independent and trend-forward retail sales, "Adam's Song" was a number one hit on their Commercial Alternative Cuts chart in the issue dated May 15, 2000.<ref name="cmj"/> The song made its sole appearance on ''Billboard''{{'s}} [[Hot 100 Airplay]] chart on that same date, peaking at number 79.<ref name="bb7"/> The song's last appearance on the Modern Rock Tracks chart came on September 9, 2000;<ref name="bb5">{{cite journal|title=Modern Rock Tracks|work=[[Billboard (magazine)|Billboard]]|page=97|volume=112|issue= 37|issn=0006-2510|date=September 9, 2000|url=https://books.google.com/books?id=aREEAAAAMBAJ&pg=PA99|accessdate=March 24, 2016}}</ref> as a whole, it spent 26 weeks on the chart.<ref name="bb6"/> In the ''Billboard'' issue for July 19, 2003, [[Nielsen Broadcast Data Systems]] recognized the single with the BDS Certified Award for 100,000 radio [[spin (radio)|spins]].<ref name="bb3">{{cite journal|title=BDSCertified Spin Awards|work=[[Billboard (magazine)|Billboard]]|page=65|volume=115|issue= 29|issn=0006-2510|date=July 19, 2003|url=https://books.google.com/books?id=WhEEAAAAMBAJ&pg=PA65|accessdate=March 24, 2016}}</ref> The song later made an appearance on ''Billboard''{{'s}} [[Rock Digital Songs]] at position 38 shortly after the release of the band's sixth album, ''[[Neighborhoods (Blink-182 album)|Neighborhoods]]'', in October 2011.<ref name="hds"/>

In Canada, the single debuted on the Rock Report chart, compiled by ''[[RPM (magazine)|RPM]]'', on May 15, 2000 at number 26.<ref name="can2">{{cite journal| title       = ''RPM'' Top 30 Rock Report| volume = 71| issue= 2| journal      = [[RPM (magazine)|RPM]]| date        = May 15, 2000| url        =http://www.bac-lac.gc.ca/eng/discover/films-videos-sound-recordings/rpm/Pages/image.aspx?Image=nlc008388.7257&URLjpg=http%3a%2f%2fwww.collectionscanada.gc.ca%2fobj%2f028020%2ff4%2fnlc008388.7257.gif&Ecopy=nlc008388.7257| publisher   = [[Ottawa]]:  [[Library and Archives Canada]]| oclc        = 352936026| format      = PDF| accessdate  = March 24, 2016}}</ref> Over the ensuing weeks, its position fluctuated, but it reached a peak of number 20 on June 12, 2000.<ref name="can1"/> It last appeared on the chart on July 24 at number 29 before dropping out.<ref name="can3">{{cite journal| title       = ''RPM'' Top 30 Rock Report| volume = 71| issue= 12| journal      = [[RPM (magazine)|RPM]]| date        = July 24, 2000| url        =http://www.bac-lac.gc.ca/eng/discover/films-videos-sound-recordings/rpm/Pages/image.aspx?Image=nlc008388.9951&URLjpg=http%3a%2f%2fwww.collectionscanada.gc.ca%2fobj%2f028020%2ff4%2fnlc008388.9951.gif&Ecopy=nlc008388.9951| publisher   = [[Ottawa]]:  [[Library and Archives Canada]]| oclc        = 352936026| format      = PDF| accessdate  = March 24, 2016}}</ref> In Italy, the single reached a peak of number 21 and spent three weeks on the charts.<ref name="Italy"/> In [[New Zealand]], the song reached a peak of number 39 and spent six weeks on the chart,<ref name="NZ"/> while in [[Germany]], the single fared poorly, spending only one week and reaching a peak of 98.<ref name="ger"/>

==Critical reception==
"Adam's Song" is generally considered one of the band's more serious songs, one "that hints at the emotional maturity they'd show on later releases," particularly their [[Blink-182 (album)|eponymous 2003 release]].<ref name="billboardreview2014"/> Richard Harrington of ''[[The Washington Post]]'' deemed the song "a powerful exploration of exhaustion and depression."<ref name="wp">{{cite news|url=http://www.washingtonpost.com/wp-dyn/articles/A31210-2004Jun10.html|title=Seriously, Blink-182 Is Growing Up|author =Richard Harrington|date=June 11, 2004|publisher=''[[The Washington Post]]'' |accessdate=February 25, 2014}}</ref> Alex Pappademas, writing for ''[[Spin (magazine)|Spin]]'', compared the song to the music of [[Weezer]].<ref name="spin">{{cite journal|title=Reviews: Blink-182 – ''The Mark, Tom and Travis Show: The Enema Strikes Back''|work=[[Spin (magazine)|Spin]]|page=222|volume=16|issue= 12|issn=0886-3032|date=December 2000}}<!--|accessdate=March 24, 2016--></ref> Katy Kroll of ''Billboard'' recognized it among her top 10 singles of 2000, calling it "a good old-fashioned depressing song with mainstream flair."<ref name="bb4">{{cite journal|title=The Year in Music: 2000 – Critics' Choice|work=[[Billboard (magazine)|Billboard]]|page=YE-49|volume=112|issue= 53|issn=0006-2510|date=December 30, 2000|url=https://books.google.com/books?id=ehEEAAAAMBAJ&pg=RA1-PA7|accessdate=March 24, 2016}}</ref> Geoff Boucher, writing for the ''[[Los Angeles Times]]'', called it "a poignant essay on a teen mulling over suicide";<ref name="lat">{{cite news|title=A Really Great Song Needs Angst ''and'' Humor|work=[[Los Angeles Times]]|date= June 10, 2001|author =Geoff Boucher|accessdate=March 24, 2016|url=http://articles.latimes.com/2001/jun/10/entertainment/ca-8530}}</ref> conversely, Steve Appleford of the ''Los Angeles Times'' dubbed it a "moving if unremarkable examination."<ref name="lat1">{{cite news|title=Blink-182 Pleases Its Fans, But Shuns Deeper Waters
|work=[[Los Angeles Times]]|date= November 1, 1999|author =Steve Appleford|accessdate=March 24, 2016|url=http://www.post-gazette.com/frontpage/2004/05/21/Concert-Preview-Another-side-of-Blink-182/stories/200405210164}}</ref> Scott Mervis of the ''[[Pittsburgh Post-Gazette]]'' called it a "rare departure from the usual Blink fare."<ref name="ppg">{{cite news|title=Concert Preview: Another side of Blink-182|work=[[Pittsburgh Post-Gazette]]|date= May 21, 2004|author =Scott Mervis|accessdate=March 24, 2016|url=http://www.post-gazette.com/frontpage/2004/05/21/Concert-Preview-Another-side-of-Blink-182/stories/200405210164}}</ref>

Writers for ''[[The A.V. Club]]'' listed it among other suicide-related songs in 2009, describing it as "surprisingly affecting, especially when the band reaches the bombastic chorus, and when the song describes suicide's crushing aftermath."<ref name="avc">{{cite news|title=Don't try to wake me in the morning: 36 (mostly excellent) songs to soundtrack your suicide|work=[[The A.V. Club]]|date= August 31, 2009|author1=Christopher Bahn |author2=Jason Heller |author3=Stephanie McNutt |author4=Chris Mincher |author5=Josh Modell |author6=Sean O'Neal |author7=Keith Phipps |author8=Leonard Pierce |author9=Vadim Rizov |author10=Kyle Ryan |accessdate=March 24, 2016|url=http://www.avclub.com/article/dont-try-to-wake-me-in-the-morning-36-mostly-excel-32292}}</ref> In a retrospective review, Chris Payne of ''Billboard'' wrote, "Stylistically, it's also a Blink breakthrough: rather than putting their heads down and plowing through at breakneck speed, the band dials back the verses and interludes to let them breathe a bit. The resulting chorus achieves an arena-worthy feel not achieved anywhere else on ''Enema of the State''."<ref name=billboardreview2014>{{cite journal| last =Payne| first =Chris| date =May 30, 2014| title =Blink-182's 'Enema of the State' at 15: Classic Track-by-Track Album Review| journal =[[Billboard (magazine)|Billboard]] | publisher =[[Prometheus Global Media]]| url =http://www.billboard.com/articles/review/6106269/blink-182-enema-of-the-state-15-anniversary-classic-track-by-track-album-review |accessdate = May 30, 2014}}</ref>

==Controversy==
The song caused a controversy in 2000 when it was set to replay indefinitely on a nearby stereo as 17-year-old Greg Barnes, a teenager who attended [[Columbine High School]] and had lost one of his best friends in the [[Columbine High School massacre|massacre]] the previous year, hanged himself in the garage of his family's home.<ref>{{cite web|url=http://www.sptimes.com/News/050600/Worldandnation/Athlete_s_suicide_sho.shtml|title=Athlete's Suicide Shocks Columbine|work=[[St. Petersburg Times]]|date=May 6, 2000|accessdate=July 6, 2013}}</ref><ref name=extras.denverpost>{{cite web|url=http://extras.denverpost.com/news/col0506.htm |title=Song only clue to student's despair|publisher=[[The Denver Post]]|authors=David Ollinger, Neil H. Devlin, Karen Augé, Marilyn Robinson|accessdate=July 6, 2013}}</ref> Both Hoppus and DeLonge were sympathetic but stressed the song's meaning:
{{quote|Hoppus: "I was actually out shopping, and management called me up and told me the story of what happened, and I was like, "But that's an anti-suicide song!" It felt awful. I mean, the things that the kid had had to go through in his life were very saddening, and then to end it that way was really depressing. But "Adam's Song", the heart of the song is about having hard times in your life, being depressed, and going through a difficult period, but then finding the strength to go on and finding a better place at the other side of that."<ref name=enemaofthestage /><ref name=Percy>{{cite web |url=http://extras.denverpost.com/news/col0629a.htm |title=Song linked to suicide on playlist |publisher=[[The Denver Post]] |author =Percy Ednalino |date=June 29, 2000}}</ref>

DeLonge: "It affected us really strongly because that song was a song of hope. When we were writing it, we knew specifically that we did not want kids to think it was something that we thought was cool or rad. We didn't endorse it in any way."<ref>{{cite news|title=Punk's Earnest New Mission|date=January 4, 2004|author =[[Michael Azerrad]]|url=https://www.nytimes.com/2004/01/04/arts/punk-s-earnest-new-mission.html|work=[[The New York Times]]|accessdate=March 24, 2016}}</ref>}}

Mark Hoppus also told interviewers he received fan mail following the song's release from fans that had contemplated suicide, but decided not to go through with it after hearing the song.{{sfn|Shooman|2010|p=77}} ''[[Rolling Stone]]'' compared the controversy to that of [[Ozzy Osbourne]]'s "[[Suicide Solution]]", which similarly was playing as a teen committed suicide.<ref name=newrs>{{cite book|title=[[The Rolling Stone Album Guide|The New Rolling Stone Album Guide]]|editor1-last=Brackett|editor1-first=Nathan|editor2-last=Hoard|editor2-first=Christian|publisher=[[Simon & Schuster]]|year=2004|isbn=0-7432-0169-8|page=85}}</ref>

==Music video==
The song's [[music video]] was directed by [[Liz Friedlander]]{{sfn|Shooman|2010|p=76}} and debuted on [[MTV]]'s ''[[Total Request Live]]'' on March 7, 2000.<ref>{{cite web |url=http://www.mtv.com/news/1426185/blink-182-preps-new-tour-video/ |title=Blink-182 Preps New Tour, Video |publisher=MTV |last=Mancini |first=Rob |date=March 7, 2000}}</ref> It consists of performance footage of the trio in a warehouse in front of a wall decorated with photographs. In between verses, the photos' origins are explored through the different perspectives of individuals near the band. As the band prepares to play a show, a man has a conversation with a girl and is subsequently left alone. In another, while DeLonge and Hoppus read magazines inside a late-night convenience store, a [[Depression (mood)|melancholy]] woman attempts to make a call via a pay phone. Other montages show the trio in the company of friends and practicing, a man looking out upon the sea, and a solitary man deserted by others at an outdoor restaurant. The final montage consists of personal photos from the band's past.<ref>{{cite web |url=https://www.youtube.com/watch?v=2MRdtXWcgIw |title=blink-182 - Adam's Song |publisher=[[YouTube]] |date=June 16, 2009}}</ref>

==In popular culture, covers and live performances==
The song was covered in 2014 by the [[math rock]] group [[Dads (band)|Dads]], for the compilation EP ''I Guess This is Growing Up''.<ref>{{cite web |url=http://www.altpress.com/news/entry/listen_to_a_new_blink_182_tribute_album_i_guess_this_is_growing_up |title=Listen to a new Blink-182 tribute album, 'I Guess This Is Growing Up'
|work=[[Alternative Press (music magazine)|Alternative Press]]|author = Crane, Matt |date=April 21, 2014|accessdate=March 23, 2016}}</ref> In 2015, a mashup combining "Adam's Song" with "[[Hotline Bling]]" by [[Drake (rapper)|Drake]] surfaced online. Called "Hotline Blink", the mashup received a positive review from Loren DiBlasi of [[MTV]], who called the mashup "[[hip hop music|hip-hop]]-meets-pop-punk-nostalgia" and wrote "Trust us, you'll want to watch again and again and again."<ref name=Loren>{{cite web |url=http://www.mtv.com/news/2680390/drake-blink-182-hotline-blink/ |title=Drake Meets Blink-182 In This Life-Changing 'Hotline Blink' Mashup |publisher=[[MTV News]] |author =Loren DiBlasi |date=December 3, 2015}}</ref> In an article about the mashup, Tyler Sharp of ''[[Alternative Press (music magazine)|Alternative Press]]'' wrote "In a bizarre, unexpected way, the result is tolerable—maybe even enjoyable?"<ref>{{cite web |url=http://www.altpress.com/news/entry/this_is_what_happens_when_you_mix_blink_182_and_drake |title=This is what happens when you mix Blink-182 and Drake |publisher=''[[Alternative Press (music magazine)|Alternative Press]]'' |author =Tyler Sharp |date=November 30, 2015}}</ref> Following the death of [[DJ AM|DJ AM (born Adam Michael Goldstein)]], a close friend of Barker, Hoppus wrote on [[Reddit]] that "Adam's Song" is "too hard" to perform and may be permanently retired from Blink-182's set list.<ref>{{cite web |url=http://www.nme.com/blogs/nme-blogs/everything-we-learned-from-blink-182s-ama-on-reddit |title=Everything We Learned From Blink-182's AMA On Reddit |publisher=[[NME]] |author =Jordan Bassett |date=November 26, 2015}}</ref> The song was also covered by Ciel Noir in 2016.<ref>https://www.youtube.com/watch?v=F_xJYmxl9nU</ref> Midnight Green, A UK based band, released their version, using a female vocalist on March 24th 2017.

==Formats and track listing==
All songs written by Mark Hoppus and Tom DeLonge, except where noted. Live tracks recorded in November 1999 at the [[Universal Amphitheatre]], Los Angeles, California.<ref name="cd"/>

{{col-begin}}
{{col-2}}
;US/UK CD single <small>(155 742-2)</small><ref>{{Cite AV media notes |title=Adam's Song|year=2000|type=CD|publisher=MCA Records |id=155 742-2}}</ref>
#"Adam's Song" <small>([[Radio Edit]])</small> – 3:35
#"Going Away to College" <small>(Live)</small> – 3:46
#"Adam's Song" <small>(Live)</small> – 4:53
#"Adam's Song" <small>(Video)</small> – 4:09
{{col-2}}
;German CD single <small>(155 743-2	)</small><ref>{{Cite AV media notes |title=Adam's Song|year=2000|type=CD|publisher=MCA Records |id=155 743-2}}</ref>
#"Adam's Song" <small>(Radio Edit)</small> – 3:35
#"Going Away to College" <small>(Live)</small> – 3:46

;Australian CD single <small>(155 752-2)</small><ref name="cd">{{cite web | url=http://www.allmusic.com/album/adams-song-mw0000388258| title=Adam's Song – Blink-182 | work=[[AllMusic]] | publisher=[[Rovi Corporation]] | accessdate=March 24, 2016}}</ref>
#"Adam's Song" <small>(Radio Edit)</small> – 3:35
#"Going Away to College" <small>(Live)</small> – 3:46
#"Adam's Song" <small>(Live)</small> – 4:53
#"Wendy Clear" <small>(Live)</small> – 2:46
{{col-end}}

== Credits and personnel ==
Credits adapted from the [[liner notes]] for ''Enema of the State''.<ref name="linernotes"/>
;Locations
*Recorded at [[Signature Sound]] and Studio West in San Diego, California, Mad Hatter Studios and the Bomb Factory in [[Los Angeles]], California, [[Conway Recording Studios]] in Hollywood, California, and Big Fish Studios in [[Encinitas, California|Encinitas]], California.
*Mixed at Conway Recording Studios in Hollywood, California, and South Beach Studios in [[Miami]], [[Florida]].
*Mastering at Bernie Grundman Mastering in Hollywood, California.
;Personnel
{{col-start}}
{{col-2}}
*[[Mark Hoppus]] – [[bass guitar]], [[Singing|vocals]]
*[[Tom DeLonge]] – [[guitar]]s, vocals
*[[Travis Barker]] – [[Drum kit|drums]], [[Percussion instrument|percussion]]
*[[Roger Joseph Manning, Jr.]]&nbsp;– [[keyboard instrument|keyboards]]
*[[Jerry Finn]]&nbsp;– [[Sound recording and reproduction|production]]
*[[Tom Lord-Alge]]&nbsp;– mixing engineer
*Sean O'Dwyer&nbsp;– [[Audio engineering|recording engineer]]
{{col-2}}
*Darrel Harvey&nbsp;– [[Audio engineering|assistant engineer]]
*John Nelson&nbsp;– assistant engineer
*Robert Read&nbsp;– assistant engineer
*Mike Fasano&nbsp;– drum technician
*Rick DeVoe&nbsp;– management
*Gary Ashley&nbsp;– [[Artists and repertoire|A&R]]
*[[Brian Gardner]]&nbsp;– [[mastering engineer]]
{{col-end}}

==Charts==
{{col-begin}}
{{col-2}}

=== Weekly charts ===
{| class="wikitable sortable plainrowheaders" style="text-align:center;"
|-
!scope="col"| Chart (2000)
!scope="col"| Peak<br />position
|-
!scope="row"|Australia ([[ARIA Charts|ARIA]])<ref>{{cite book|last=Ryan|first=Gavin|title=Australia's Music Charts 1988–2010|year=2011|publisher=Moonlight Publishing|location=Mt. Martha, VIC, Australia}}</ref>
|72
|-
!scope="row" | Canada Rock Report (''[[RPM (magazine)|RPM]]'')<ref name="can1">
{{cite journal
| title       = ''RPM'' Top 30 Rock Report
| volume = 71
| issue= 6
| journal      = [[RPM (magazine)|RPM]]
| date        = June 12, 2000
| url        = http://www.bac-lac.gc.ca/eng/discover/films-videos-sound-recordings/rpm/Pages/image.aspx?Image=nlc008388.7254&URLjpg=http%3a%2f%2fwww.collectionscanada.gc.ca%2fobj%2f028020%2ff4%2fnlc008388.7254.gif&Ecopy=nlc008388.7254
| publisher   = [[Ottawa]]:  [[Library and Archives Canada]]
| oclc        = 352936026
| format      = PDF
| accessdate  = March 21, 2016
}}
</ref>
| style="text-align:center;"|20
|-
!scope="row"{{singlechart|Germany|98|artist=Blink 182|song=Adam's Song|accessdate=July 18, 2012|refname=ger}}
|-
!scope="row"{{singlechart|Italy|21|artist=Blink 182|song=Adam's Song|accessdate=February 1, 2015|refname=Italy}}
|-
!scope="row"{{singlechart|New Zealand|39|artist=Blink 182|song=Adam's Song|accessdate=July 18, 2012|refname=NZ}}
|-
!scope="row"{{singlechart|Billboardbubbling100|1|artist=blink-182|song=Adam's Song|artistid={{BillboardID|blink-182}}|accessdate=July 18, 2012}}
|-
!scope="row"|US Commercial Alternative Cuts (''[[CMJ]]'')<ref name="cmj">{{cite journal|title=Commercial Alternative Cuts|work=[[CMJ New Music Monthly]]|date=May 15, 2000|page=15|volume=62|issue=666|issn=0890-0795|url=https://books.google.com/books?id=XyE_79kpWJoC&pg=PA15}}</ref>
| style="text-align:center;"|1
|-
!scope="row" | US [[Radio Songs|Hot 100 Airplay]] (''[[Billboard (magazine)|Billboard]]'')<ref name="bb7">{{cite web|url={{BillboardURLbyName|artist=blink-182|chart=Radio Songs}}|title=blink-182 – Chart History|accessdate=January 18, 2014|work=Billboard|publisher=Prometheus Global Media}}</ref>
| style="text-align:center;"|79
|-
!scope="row" | US [[Hot Modern Rock Tracks]] (''[[Billboard (magazine)|Billboard]]'')<ref name="bb6">{{cite web|url={{BillboardURLbyName|artist=blink-182|chart=Alternative Songs}}|title=blink-182 – Chart History|accessdate=January 18, 2014|work=Billboard|publisher=Prometheus Global Media}}</ref>
| style="text-align:center;"|2
|}
{| class="wikitable sortable plainrowheaders" style="text-align:center;"
|-
!scope="col"| Chart (2011)
!scope="col"| Peak<br />position
|-
!scope="row"|US [[Rock Digital Songs]] (''[[Billboard (magazine)|Billboard]]'')<ref name="hds">{{cite web |url={{BillboardURLbyName|artist=blink-182|chart=Rock Digital Songs}} |title=blink-182 - Chart history |publisher=''[[Billboard (magazine)|Billboard]]''}}</ref>
| style="text-align:center;"|38
|}
{{col-2}}

===Year-end charts===
{| class="wikitable sortable plainrowheaders" style="text-align:center;"
|-
!scope="col"| Chart (2000)
!scope="col"| Peak<br />position
|-
!scope="row" |US Hot Modern Rock Tracks (''Billboard'')<ref name="Year-End-Mod-Rock">{{cite journal|title=The Year in Music: 2000 – Hot Modern Rock Tracks|work=[[Billboard (magazine)|Billboard]]|page=88|volume=112|issue= 53|issn=0006-2510|date=December 30, 2000|url=https://books.google.com/books?id=ehEEAAAAMBAJ|accessdate=March 20, 2016}}</ref>
| style="text-align:center;"|7
|}
{{col-2}}
{{col-end}}

==References==

{{Reflist|30em}}

===Bibliography===
* {{Cite book
 | last = Hoppus
 | first = Anne
 | title = Blink-182: Tales from Beneath Your Mom
 | date = October 1, 2001
 | publisher = MTV Books / [[Pocket Books]]
 | isbn = 0-7434-2207-4
 | ref = harv
 }}
* {{Cite book
 | last = Shooman
 | first = Joe
 | title = Blink 182: The Bands, the Breakdown and the Return
 | year = 2010
 | publisher = Independent Music Press
 | isbn = 978-1-906191-10-8
 | ref = harv
 }}
* {{Cite book
 | authorlink1=Travis Barker
 | last1=Barker
 | first1=Travis
 | last2=Edwards
 | first2=Gavin
 | title=Can I Say: Living Large, Cheating Death, and Drums, Drums, Drums
 | publisher=William Morrow
 | year=2015
 | isbn=978-0-06-231942-5
 | ref = harv
 }}

==External links==
* {{youtube|2MRdtXWcgIw|Official music video for "Adam's Song"}}
* {{MetroLyrics song|blink-182|adams-song}}<!-- Licensed lyrics provider -->

{{Blink-182}}

[[Category:1999 songs]]
[[Category:2000 singles]]
[[Category:Blink-182 songs]]
[[Category:Rock ballads]]
[[Category:Songs about loneliness]]
[[Category:Songs about suicide]]
[[Category:Songs written by Mark Hoppus]]
[[Category:Music videos directed by Liz Friedlander]]
[[Category:21st-century controversies]]
[[Category:Songs written by Tom DeLonge]]