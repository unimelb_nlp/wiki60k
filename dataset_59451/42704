<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=DV-1 Skylark
 | image=Skylark DV1.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Ultralight aircraft]] and [[Light-sport aircraft]]
 | national origin=[[Czech Republic]]
 | manufacturer=[[Dova Aircraft]]
 | designer=
 | first flight=
 | introduced=
 | retired=
 | status=In production
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= [[Euro|€]]65,000 (assembled, 2015)
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Dova DV-1 Skylark''' is a [[Czech Republic|Czech]] [[ultralight aircraft|ultralight]] and [[light-sport aircraft]] produced by [[Dova Aircraft]] of [[Paskov]]. The aircraft is supplied as a kit for [[Homebuilt aircraft|amateur construction]] or as a complete ready-to-fly-aircraft.<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 42. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref><ref name="WDLA15">Tacke, Willi; Marino Boric; et al: ''World Directory of Light Aviation 2015-16'', page 43. Flying Pages Europe SARL, 2015. {{ISSN|1368-485X}}</ref>

==Design and development==
[[File:ILA 2008 PD 672.JPG|thumb|right|Skylark showing canopy operation]]
The DV-1 was designed to comply with the [[Fédération Aéronautique Internationale]] microlight rules and US light-sport aircraft rules. It features a cantilever [[low-wing]], a [[T-tail]], a two-seats-in-[[side-by-side configuration]] enclosed cockpit under a [[bubble canopy]], fixed [[tricycle landing gear]] with [[wheel pants]] and a single engine in [[tractor configuration]].<ref name="WDLA11" /><ref name="WDLA15"/><ref name="SLSA">{{cite web|url = http://www.sportpilot.org/learn/slsa/|title = EAA's Listing of Special Light-Sport Aircraft|accessdate = 31 May 2012|last = [[Experimental Aircraft Association]]|date = 2012}}</ref>

The aircraft is made from [[aluminum]] sheet. Its {{convert|8.14|m|ft|1|abbr=on}} span wing has an area of {{convert|9.44|m2|sqft|abbr=on}} and is equipped with [[Flap (aircraft)|flaps]] and [[winglet]]s. Standard engines available are the {{convert|80|hp|kW|0|abbr=on}} [[Rotax 912UL]], {{convert|100|hp|kW|0|abbr=on}} [[Rotax 912ULS]], {{convert|115|hp|kW|0|abbr=on}} [[turbocharged]] [[Rotax 914]] and the [[BMW 1100]] [[four-stroke]] powerplants.<ref name="WDLA11" /><ref name="WDLA15"/>

The design is an accepted [[Federal Aviation Administration]] special light-sport aircraft.<ref name="FAASLSA">{{cite web|url = https://www.faa.gov/aircraft/gen_av/light_sport/media/SLSA_Directory.xlsx|title = SLSA Make/Model Directory|accessdate = 8 February 2017|last = [[Federal Aviation Administration]]|date = 26 September 2016}}</ref>

==Operational history==
By February 2017 seven examples had been [[Aircraft registration|registered]] in the United States with the [[Federal Aviation Administration]].<ref name="FAAReg">{{cite web|url = http://registry.faa.gov/aircraftinquiry/AcftRef_Results.aspx?Mfrtxt=&Modeltxt=DV-1&PageNo=1|title = Make / Model Inquiry Results|accessdate = 10 February 2017|last = [[Federal Aviation Administration]]|date = 10 February 2017}}</ref>

==Variants==
;DV-1 Skylark
:Model with a [[T-tail]] and [[winglet]]s.<ref name="WDLA15"/>
;DV-2 Infinity
:Model with a [[cruciform tail]] and no winglets. It is almost {{convert|20|km/h|mph|0|abbr=on}} faster than the DV-1.<ref name="WDLA15"/>
<!-- ==Aircraft on display== -->

==Specifications (DV-1 Skylark) ==
[[File:Skylark (D-MPSH) 03.jpg|thumb|right|Dova DV-1 Skylark]]
{{Aircraft specs
|ref=Bayerl<ref name="WDLA11" />
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=one passenger
|length m=
|length ft=
|length in=
|length note=
|span m=8.14
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=9.44
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=275
|empty weight lb=
|empty weight note=
|gross weight kg=472.5
|gross weight lb=
|gross weight note=
|fuel capacity={{convert|90|l}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Rotax 912ULS]]
|eng1 type=four cylinder, liquid and air-cooled, [[four stroke]] [[aircraft engine]]
|eng1 kw=75<!-- prop engines -->
|eng1 hp=<!-- prop engines -->

|prop blade number=
|prop name=
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=240
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=210
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=65
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=5.9
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=50.1
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==References==
{{reflist}}

==External links==
{{commons category|Dova DV-1 Skylark}}
*{{Official website|http://www.dovaaircraft.cz/}}

[[Category:Czech ultralight aircraft 2000–2009]]
[[Category:Homebuilt aircraft]]
[[Category:Light-sport aircraft]]
[[Category:Single-engine aircraft]]
[[Category:T-tail aircraft]]