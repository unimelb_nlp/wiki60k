{{mergefrom|Oscillator phase noise|discuss=Talk:Phase noise#Merger proposal|date=October 2016}}
In [[signal processing]], '''phase noise''' is the [[frequency domain]] representation of rapid, short-term, random fluctuations in the [[phase (waves)|phase]] of a [[waveform]], caused by [[time domain]] instabilities ("[[jitter]]").<ref>{{FS1037C}}</ref>  Generally speaking, [[radio frequency]] engineers speak of the phase noise of an [[oscillator]], whereas [[digital system]] engineers work with the jitter of a clock.

==Definitions==
Historically there have been two conflicting yet widely used definitions for phase noise. Some authors define phase noise to be the [[spectral density]] of a signal's phase only,<ref>{{Citation |first=J. |last=Rutman |first2=F. L. |last2=Walls |title=Characterization of frequency stability in precision frequency sources |journal=Proceedings of the IEEE |volume=79 |issue=6 |pages=952–960 |date=June 1991 |url=http://www.umbc.edu/photonics/Menyuk/Phase-Noise/rutman_ProcIEEE_910601.pdf |issn= |doi=10.1109/5.84972 }}</ref> while the other definition refers to the phase spectrum (which pairs up with the amplitude spectrum, see [[spectral density#Related concepts]]) resulting from the [[spectral estimation]] of the signal itself.<ref>{{Citation |first=A. |last=Demir |first2=A. |last2=Mehrotra |first3=J. |last3=Roychowdhury |title=Phase noise in oscillators: a unifying theory and numerical methods for characterization |journal=IEEE Trans. on Circuits and Systems I: Fundamental Theory and Applications |volume=47 |issue=5 |pages=655–674 |date=May 2000 |issn=1057-7122 |doi=10.1109/81.847872 }}</ref> Both definitions yield the same result at offset frequencies well removed from the carrier. At close-in offsets however, the two definitions differ.<ref>{{Citation |first=R. |last=Navid |first2=C. |last2=Jungemann |first3=T. H. |last3=Lee |first4=R. W. |last4=Dutton |author4-link=Robert W. Dutton |title=Close-in phase noise in electrical oscillators |journal=Proc. SPIE Symp. Fluctuations and Noise |location=Maspalomas, Spain |year=2004 |issn= |doi= }}</ref>

The [[IEEE]] defines phase noise as {{math|ℒ(''f''){{=}}''S''<sub>φ</sub>(''f'')/2}} where the "phase instability" {{math|''S''<sub>φ</sub>(''f'')}} is the one-sided spectral density of a signal's phase deviation.<ref>{{Citation |first2=Eva. S. |last2=Ferre-Pikal |first1=John R. |last1=Vig |first3=J. C. |last3=Camparo |first4=L. S. |last4=Cutler |first5=L. |last5=Maleki |first6=W. J. |last6=Riley |first7=S. R. |last7=Stein |first8=C. |last8=Thomas |first9=F. L. |last9=Walls |first10=J. D. |last10=White |id=IEEE Std 1139-1999 |title=IEEE Standard Definitions of Physical Quantities for Fundamental Frequency and Time Metrology &ndash; Random Instabilities |publisher=IEEE |date=26 March 1999 |isbn=0-7381-1754-4 |doi= |ref=CITEREFIEEE1999 }}, see definition 2.7.</ref>  Although {{math|''S''<sub>φ</sub>(''f'')}} is a one-sided function, it represents "the double-sideband spectral density of phase fluctuation".<ref>{{Harvnb|IEEE|1999|p=2}}, stating &#x2112;(''f'') "is one half of the double-sideband spectral density of phase fluctuations."</ref>  The phase noise expression {{math|ℒ(''f'')}} is pronounced "script ell of f".<ref>{{Harvnb|IEEE|1999|p=2}}</ref>

==Background==
An ideal [[electronic oscillator|oscillator]] would generate a pure [[sine wave]].  In the frequency domain, this would be represented as a single pair of [[Dirac delta function]]s (positive and negative conjugates) at the oscillator's frequency, i.e., all the signal's [[Power (physics)|power]] is at a single frequency.  All real oscillators have [[phase modulated]] [[Electronic noise|noise]] components.  The phase noise components spread the power of a signal to adjacent frequencies, resulting in noise [[sidebands]].  Oscillator phase noise often includes low frequency [[flicker noise]] and may include [[white noise]].

Consider the following noise-free signal:
:''v''(''t'') = ''A''cos(2&pi;''f''<sub>0</sub>''t'').
Phase noise is added to this signal by adding a [[Stochastic Process|stochastic process]] represented by φ to the signal as follows:
:''v''(''t'') = ''A''cos(2&pi;''f''<sub>0</sub>''t'' + &phi;(''t'')).

Phase noise is a type of [[cyclostationary noise]] and is closely related to [[jitter]]. A particularly important type of phase noise is that produced by [[Oscillator phase noise|oscillators]].

Phase noise (ℒ(''f'')) is typically expressed in units of [[dBc]]/Hz, and it represents the noise power relative to the carrier contained in a 1&nbsp;Hz bandwidth centered at a certain offsets from the carrier. For example, a certain signal may have a phase noise of -80&nbsp;dBc/Hz at an offset of 10&nbsp;kHz and -95&nbsp;dBc/Hz at an offset of 100&nbsp;kHz. Phase noise can be measured and expressed as single sideband or double sideband values, but as noted earlier, the IEEE has adopted the definition as one-half of the double sideband PSD.

==Jitter conversions==
Phase noise is sometimes also measured and expressed as a power obtained by integrating ℒ(''f'') over a certain range of offset frequencies. For example, the phase noise may be -40&nbsp;dBc integrated over the range of 1&nbsp;kHz to 100&nbsp;kHz. This Integrated phase noise (expressed in degrees) can be converted to jitter (expressed in seconds) using the following formula:
<math>\text{Jitter (seconds}) = \text{Phase error (degrees)}/(360\times \text{Frequency (Hertz)})</math>

In the absence of [[1/f noise]] in a region where the phase noise displays a –20 dBc/decade slope, the
rms cycle jitter can be related to the phase noise by:<ref>{{Citation |url=http://cp.literature.agilent.com/litweb/pdf/5990-3108EN.pdf |last=Poore |first=Rick |title=An Overview of Phase Noise and Jitter |publisher=Agilent Technologies |date=May 17, 2001 |doi= }}</ref>

<math>\sigma^2_c = \frac{f^2 \mathcal{L}\left(f\right)}{f_{osc}^3}</math>

Likewise:

<math>\mathcal{L}\left(f\right) = \frac{f_{osc}^3 \sigma^2_c}{f^2}</math>

==Measurement==
Phase noise can be measured using a [[spectrum analyzer]] if the phase noise of the device under test (DUT) is large with respect to the spectrum analyzer's [[local oscillator]]. Care should be taken that observed values are due to the measured signal and not the shape factor of the spectrum analyzer's filters. Spectrum analyzer based measurement can show the phase-noise power over many decades of frequency e.g. 1&nbsp;Hz to 10&nbsp;MHz.  The slope with offset frequency in various offset frequency regions can provide clues as to the source of the noise, e.g. low frequency [[flicker noise]] decreasing at 30 dB per decade (=9 dB per octave).<ref>{{Citation |last=Cerda |first=Ramon M. |title=Impact of ultralow phase noise oscillators on system performance |date=July 2006 |journal=[[RF Design]] |volume= |issue= |pages=28&ndash;34 |url=http://rfdesign.com/mag/607RFDF2.pdf |issn= |doi= }}</ref>

Phase noise measurement systems are alternatives to spectrum analyzers. These systems may use internal and external references and allow measurement of both residual and additive noise.  Additionally, these systems can make low-noise, close-to-the-carrier, measurements.

==Spectral purity==
The sinewave output of an ideal [[electronic oscillator|oscillator]] is a single line in the frequency spectrum. Such perfect spectral purity is not achievable in a practical oscillator. Spreading of the spectrum line caused by phase noise must be minimised in the local oscillator for a [[superheterodyne receiver]] because it defeats the aim of restricting the receiver frequency range by filters in the IF (intermediate frequency) amplifier.

==See also==
*[[Allan variance]]
*[[Flicker noise]]
*[[Leeson's equation]]
*[[Maximum time interval error]]
*[[Noise spectral density]]
*[[Spectral density]]
*[[Spectral phase]]
*[[Opto-electronic oscillator]]

==References==
{{reflist}}

==Further reading==
*{{Citation |last=Rubiola |first=Enrico |year=2008 |title=Phase Noise and Frequency Stability in Oscillators |publisher=Cambridge University Press |isbn=978-0-521-88677-2 |doi= |ref=none}}
*{{Citation |last=Wolaver |first=Dan H. |year=1991 |title=Phase-Locked Loop Circuit Design |publisher=Prentice Hall |isbn=0-13-662743-9 |doi= |ref=none}}
*{{Citation |first=M. |last=Lax |title=Classical noise. V. Noise in self-sustained oscillators |journal=[[Physical Review]] |volume=160 |issue=2 |pages=290&ndash;307 |date=August 1967 |doi=10.1103/PhysRev.160.290 |bibcode = 1967PhRv..160..290L |ref=none}}
*{{Citation |first=A. |last=Hajimiri |first2=T. H. |last2=Lee |url=http://loveboat.stanford.edu/papers/JSSC98FEB-ali.pdf |title=A general theory of phase noise in electrical oscillators |journal=IEEE Journal of Solid-State Circuits |volume=33 |issue=2 |date=February 1998 |pages=179–194 |doi=10.1109/4.658619 |ref=none}}
* {{Citation |first=R. |last=Pulikkoonattu |title=Oscillator Phase Noise and Sampling Clock Jitter |date=June 12, 2007 |url=http://documents.epfl.ch/users/p/pu/pulikkoo/private/report_pn_jitter_oscillator_ratna.pdf |publisher=ST Microelectronics |location=Bangalore, India |series=Tech Note |doi= |accessdate=March 29, 2012 |ref=none}}
*{{Citation |first=A. |last=Chorti |first2=M. |last2=Brookes |title=A spectral model for RF oscillators with power-law phase noise |journal=IEEE Trans. on Circuits and Systems I: Regular Papers |volume=53 |issue=9 |date=September 2006 |pages=1989–1999 |doi=10.1109/TCSI.2006.881182 |ref=none}}
*{{Citation |first=Ulrich L. |last=Rohde |first2=Ajay K. |last2=Poddar |first3=Georg |last3=Böck |title=The Design of Modern Microwave Oscillators for Wireless Applications |publisher=John Wiley & Sons |location=New York, NY |date=May 2005 |isbn=0-471-72342-8 |ref=none}}
*  Ulrich L. Rohde ,  A New and Efficient Method of Designing Low Noise Microwave Oscillators, https://depositonce.tu-berlin.de/bitstream/11303/1306/1/Dokument_16.pdf
* Ajay Poddar, Ulrich Rohde, Anisha Apte, “ How Low Can They Go, Oscillator Phase noise model, Theoretical, Experimental Validation, and Phase Noise Measurements”, IEEE Microwave Magazine, Vol. 14, No. 6, pp. 50-72, September/October 2013.  
* Ulrich Rohde, Ajay Poddar, Anisha Apte, “Getting Its Measure”, IEEE Microwave Magazine, Vol. 14, No. 6, pp. 73-86, September/October 2013
* U. L. Rohde, A. K. Poddar, Anisha Apte, “Phase noise measurement and its limitations”, [[Microwave Journal]], pp. 22-46, May 2013
* A. K. Poddar, U.L. Rohde,   “Technique to Minimize Phase Noise of Crystal Oscillators”, [[Microwave Journal]], pp. 132-150,  May 2013.
* A. K. Poddar, U. L. Rohde, and E. Rubiola, “Phase noise measurement: Challenges and uncertainty”, 2014 IEEE IMaRC, Bangalore, Dec  2014.
{{Noise}}
{{Prone to spam|date=August 2014}}
{{Z148}}<!--     {{No more links}}

       Please be cautious adding more external links.

Wikipedia is not a collection of links and should not be used for advertising.

     Excessive or inappropriate links will be removed.

 See [[Wikipedia:External links]] and [[Wikipedia:Spam]] for details.

If there are already suitable links, propose additions or replacements on
the article's talk page, or submit your link to the relevant category at
DMOZ (dmoz.org) and link there using {{Dmoz}}.

-->
{{DEFAULTSORT:Phase Noise}}
[[Category:Oscillators]]
[[Category:Frequency domain analysis]]
[[Category:Telecommunication theory]]
[[Category:Noise (electronics)]]