{{Use British English|date=August 2013}}
{{Use dmy dates|date=November 2012}}
{{Infobox album
| Name        = Made in the Dark
| Type        = studio
| Artist      = [[Hot Chip]]
| Cover       = Hot Chip - Made in the Dark.png
| Alt         = Pale blue album cover embossed with a brown circle made up from smaller parts. The words "Hot Chip" and, below it, "Made in the Dark" are displayed in the top left hand corner.
| Released    = {{Start date|2008|2|4|df=yes}}
| Recorded    =
| Genre       = {{flatlist|
* [[Indietronica]]
* [[alternative dance]]
}}
| Length      = 53:57
| Label       = {{flatlist|
* [[EMI]]
* [[Astralwerks]]
* [[DFA Records|DFA]]
}}
| Producer    = Hot Chip
| Last album  = ''[[The Warning (Hot Chip album)|The Warning]]''<br>(2006)
| This album  = '''''Made in the Dark'''''<br>(2008)
| Next album  = ''[[One Life Stand]]''<br>(2010)
| Misc        = {{Singles
  | Name           = Made in the Dark
  | Type           = studio
  | Single 1       = [[Shake a Fist]]
  | Single 1 date  = 23 October 2007
  | Single 2       = [[Ready for the Floor]]
  | Single 2 date  = 28 January 2008
  | Single 3       = [[One Pure Thought]]
  | Single 3 date  = 12 May 2008
  | Single 4       = Hold On
  | Single 4 date  = 21 September 2008<ref>{{cite web |url=http://www.amazon.co.uk/dp/B001LP0NPU |title=Hold On/Touch Too Much Remixes: Hot Chip:  MP3 Downloads |publisher=[[Amazon.co.uk]] |accessdate=8 April 2015}}</ref>
  }}
}}

'''''Made in the Dark''''' is the third studio album by English [[indietronica]] band [[Hot Chip]]. The album, comprising 13 songs, was released on 4 February 2008 on the [[EMI]] label.<ref>{{cite web| last = Maher| first = Dave| title = Hot Chip's New Album Made in the Dark | publisher = [[Pitchfork Media]]| url =http://www.pitchforkmedia.com/article/news/46336-hot-chips-new-album-imade-in-the-darki| accessdate = 3 July 2008| date=12 October 2007|archiveurl = https://web.archive.org/web/20080512015850/http://www.pitchforkmedia.com/article/news/46336-hot-chips-new-album-imade-in-the-darki |archivedate = 12 May 2008|deadurl=yes}}</ref> It peaked at number four on the [[UK Album Chart]],<ref name="UKchart">{{cite web| last = | first =| title = Singer Johnson tops album chart | publisher = [[BBC]]| url =http://news.bbc.co.uk/1/hi/entertainment/7237767.stm| accessdate = 3 July 2008| date=10 February 2008}}</ref> number 25 on the Australian album charts,<ref name="aus"/> and entered at number 109 on the U.S. [[Billboard 200|''Billboard'' 200]].<ref name="Billboard"/> Several singles have been released from the album, including "[[Shake a Fist]]", "[[Ready for the Floor]]", which reached number six on the [[UK Singles Chart]], and "[[One Pure Thought]]".<ref>{{cite web| last = | first =| title =BBC News: Adele's album debuts at chart top | publisher = [[BBC]]| url =http://news.bbc.co.uk/1/hi/entertainment/7225256.stm| accessdate = 3 July 2008| date=3 February 2008}}</ref>

A defining feature of the album is the strong presence of romantic ballads.<ref name="Interview2"/> The ballad "Made in the Dark" was described as "sublime" by one critic, although not all the ballads received universal praise.<ref name="Rolling"/> Alexis Taylor, the main contributor to the lyrics, stated that he was proud of the album lyrically and felt that feelings of love and happiness, partly the result of his recent marriage, had contributed to the album's romantic tone.<ref name="Sun"/><ref name="Manchester"/>

Critics stated that songs such as "Ready for the Floor" and "Bendable Poseable" were reminiscent of their previous release, ''[[The Warning (Hot Chip album)|The Warning]]''.<ref name="Omh"/> The style of the album was not considered as big a leap forward as the changes evident between ''Coming on Strong'' (2004) to ''The Warning'' (2006). It was said that Hot Chip had honed their music by using quirks of their musical style to make more accomplished music, however, some critics felt that the album lacked focus, containing too many varied elements; it was described as "loveable but flawed".<ref name="pitchforkalbum"/>

== Production ==
[[File:Al Doyle by Fred Benenson (cropped).jpg|thumb|right|Al Doyle]]
Hot Chip often record their music in a bedroom. However, the band took a different approach in recording ''Made in the Dark'' to make it sound "not quite so homemade".<ref name="Manchester"/> Recording took place in a variety of locations, including in-studio and live venues, to make "different acoustic spaces to be obvious to the listener".<ref name="Manchester"/> Lead vocalist Alexis Taylor said there was tension between the band's members, as the band did not want to continue creating music in the same way, but also did not want to "throw away a songwriting and production partnership that has worked before."<ref name="Sun"/> Taylor and vocalist Joe Goddard worked on some of the songs together. Several songs were [[overdubbing|overdubbed]] with parts of the rest of the band playing.<ref name="Sun"/>

In regards to editing, Taylor said that Hot Chip have "never really been too good at bothering to get rid of little imperfections in the music"; he felt they added personality and said that "it's good not to be too dogmatic about it if that's what suits the song".<ref name="bed"/> Hot Chip used a variety of vocal structures, including layered vocals, where two takes of the same vocals were added together, changed by idiosyncrasies from the way it had been sung, and the doubling of a single performance, shifted out of beat.<ref name="bed"/>

Drummer Felix Martin said ''Made in the Dark'' was a "true group effort". The process began with Taylor and Goddard creating lyrics, then Goddard would produce parts of tracks, which Alexis would then add "lyrical content and melodies and so on that he's thought of while he's on the bus or in the bath or wherever he happened to be". The other three members of Hot Chip, Owen Clarke, Al Doyle, and Felix Martin would then "have some influence on the way the songs are put together."<ref name="battle"/>

Some equipment used to create the album remained the same as previous albums—Goddard used Steinberg Cubase SX3 on his laptop and Doyle and Martin worked on songs using Apple Logic in their studio.<ref name="bed"/> To create the chorus for "Ready for the Floor", Goddard used plug-ins from Arturia, such as Moog Modular. He used two sound channels to control noise and melody, and placed the noise channel in Cubase and had it follow the melody, to make it "punchier".<ref name="bed"/> With "Bendable Poseable", Goddard recorded live percussion parts with a Shure Beta 57A mic going directly into Cubase and "fashioned them into a jittery, three-minute loop". This was emailed to Taylor, who then recorded the main vocals for the song.<ref name="bed"/> To create the beat in "Shake a Fist" Martin used the Elektron Machinedrum.<ref name="battle">{{cite web| last = Lucas| first = John| title = Hot Chip battles boredom | publisher = ''[[The Georgia Straight]]''| url=http://www.straight.com/article-141075/hot-chip-battles-boredom| accessdate = 24 September 2008| date=17 April 2008}}</ref>

Hot Chip enjoy being a live band and have said it is "something [they've] always tried to do". As the group contains "a lot of different types of musicianship", the band "have to be spontaneous whenever [they] can."<ref name="Sun"/><ref name="bed"/> Doyle, the guitarist, stated that Hot Chip change the original recordings when playing their music live.<ref name="Sun"/> At the London-based studio called the Strongroom, "One Pure Thought", "Hold On" and "Shake a Fist" were recorded live, instead of [[multitrack recording|on multitrack]], and were subsequently pieced together.<ref name="Sun"/> This was the first occasion that Hot Chip had recorded music in a studio environment.<ref name="bed">{{cite web| last = Murphy| first = Bill| title = Bedknobs & Boom-Chiks | publisher = ''Remix magazine''| url =http://remixmag.com/artists/electronic/remix_bedknobs_boomchiks/| accessdate = 10 August 2008| date=1 March 2008}}</ref>

Taylor described ''Made in the Dark'' as "a real mix of different things" and said that Hot Chip was "learning how to be a different kind of band" while making the album: {{blockquote|Everyone around us would write songs whilst touring, then go into a studio and record [songs] at the end of a year's touring. We were the opposite. We would make the songs in a short time and then would have to learn how to play them live.<ref name="Sun"/>}}

=== Album title ===
Several titles were considered during production, including "Shot Down in Flames" and "IV". The former was rejected because Martin thought it sounded like a title [[The Beta Band]] would use. Taylor supported the name "IV" because he liked "giving people the wrong impression all the time",<ref name="Interview2"/> and defended his opinion, saying "if [people] give us any time, they would see that we're very serious about comedy ... and serious things as well".<ref name="Interview2"/> The album was called "Made in the Dark" because it was a title the band agreed on.<ref name="Interview2"/> Taylor considered the eponymous track to be one of his favourite songs, and thought it was nice to name the album after a thoughtful song, in contrast to ''[[Coming on Strong (album)|Coming on Strong]]'' and ''The Warning'', which he described as being "big, slightly jokey, macho phrases".<ref name="Interview2"/>

=== Artwork ===
Darren Wall (Wallzo) and Owen Clarke designed the artwork after several graphical experiments. After the initial experimentation, Wall wanted to create a cover that was "more brooding and conservative" and formed a list of ideas that the band had responded positively to. The list included dual colour illustrations, circles, and [[verdigris]]—the green coating formed on copper during oxygenation. Wall amalgamated the ideas to create the image used on the album's cover, which was named "The Artifact". The image was embossed on metallic copper card to give a "tactile feel" that would imply the album was "an object rather than illustration-based design."<ref>{{cite web|last=Lucas|first=Gavin |title =What, No Jewel Case? - Hot Chip - Made in the Dark|publisher = ''[[Creative Review]]''|url =http://www.creativereview.co.uk/crblog/what-no-jewel-case/|accessdate = 11 August 2008|date=18 February 2008}}</ref>

== Musical style ==
[[File:Hot Chip 33 by David Koppe.jpg|thumb|300px|right|Hot Chip performing songs from ''Made in the Dark'' on tour]]
In an interview with [[Pitchfork Media]] in October 2007, Taylor said there would be an equal proportion of electronic elements to live material, as the band doesn't "do things by adding one thing and taking something else away".<ref name="Interview2">{{cite web| last = Maher| first = Dave| title = Pitchfork: Hot Chip's Taylor Talks LP, Alicia Keys, Rilo Remixes | publisher =[[Pitchfork Media]] | url =http://www.pitchforkmedia.com/article/news/46351-hot-chips-taylor-talks-lp-alicia-keys-rilo-remixes| accessdate = 3 July 2008| date= 15 October 2008|archiveurl = https://web.archive.org/web/20080406035056/http://www.pitchforkmedia.com/article/news/46351-hot-chips-taylor-talks-lp-alicia-keys-rilo-remixes |archivedate = 6 April 2008|deadurl=yes}}</ref> The album contained [[maximalist]] and [[minimalist]] songs; several tracks on the album were influenced by [[rock music|rock]] and [[heavy metal music]], and the track "Wrestlers" started taking a new direction because the band was "wrestling with the idea of making an [[R. Kelly]] kind of slick R and B number" and ultimately "[sounded] more like [[Randy Newman]]'s "[[Short People]]".<ref name="Interview2"/> He said, "if the press release says it's faster and rockier it doesn't account for the fact that there are more ballads on this record than any other record."<ref name="Interview2"/> Taylor said that feelings of [[happiness]] and [[love]] influenced the album's [[romance (love)|romantic]] feel.<ref name="Manchester"/>

Goddard considered varying styles and influences a key factor in the band's music.<ref name="Sun"/> He explained to ''[[The Sun (newspaper)|The Sun]]'' that creating music could be difficult because a member could introduce a different influence.<ref name="Sun"/> Goddard and Doyle said that clashes and restlessness during recording led to "unpleasant" periods of silence, but ultimately thought the clashes created "something more interesting because you have these different voices and not one person dictating".<ref name="Sun"/>

Martin told ''[[The Georgia Straight]]'' that the group are "afflicted with something akin to musical attention-deficit disorder" and said that the group "get bored quite easily [...] with [their] own records at times". He elaborated by saying that the group aren't "really interested in reproducing the same sound" because they don't find it exciting.<ref name="battle"/>

Taylor stated Hot Chip "didn't set out to make something with one mood" and that he thought the band's style of "jump[ing] all over the place stylistically" made sense as a record.<ref name="Interview2"/> In an interview with ''The Georgia Straight'', Martin expressed that Hot Chip didn't want to create a "'classic' record that would have a particular sound" as they wanted to make music that was "quite experimental and out-there".<ref name="battle"/> ''Made in the Dark'' was intended to represent the "whole live sound of the band" and they are "a band as much as originally having been a duo".<ref name="Interview2"/>

=== Influences ===
The album is influenced by music Goddard and Taylor listened to during their childhood and adolescence, such as [[Prince (musician)|Prince]]'s ''[[Sign o' the Times (album)|Sign o' the Times]]'' and [[The Beatles]]' ''[[White Album]]''.<ref name="Interview2"/> Taylor explained why Hot Chip's albums "go from one mood to another so readily", by saying "[[Eclecticism in music|Eclectic music]] has been our first musical background" and that he and Goddard had different musical interests when they were younger.<ref name="Interview2"/> ''Made in the Dark'' is influenced by contemporary artists such as [[Black Dice]] and [[Will Oldham]].<ref name="Interview2"/> Taylor appreciated Oldham for his minimalism of "just [[acoustic guitar]] and [[Pump organ|harmonium]] and voice for the whole record" and wanted Hot Chip to emulate him.<ref name="Interview2"/>

== Lyrics ==
[[File:Alexi Taylor of Hot Chip.jpg|thumb|right|Alexis Taylor]]
Taylor stated in an interview with ''[[The Sun (newspaper)|The Sun]]'' that he was "so proud of [the album] lyrically" and thought the song "Made in the Dark" contained the best lyrics he had ever written.<ref name="Sun"/> Taylor explained that it was "very easy" for him to focus on the "more serious lyrics, like a love song very dear to [his] heart" but stated that "Wrestlers" was "equally as important and feels like a very different style of music than we have ever got down on record before."<ref name="Sun"/> He discussed "Wrestlers" in the band's interview with ''The Sun'', explaining that the song is "musically and lyrically quite direct" because "it doesn't have thousands of layers", in contrast to the song "Bendable Poseable".<ref name="Sun"/> The concept of the song "Wrestlers", originates from a [[text message]] from [[James Murphy (electronic musician)|James Murphy]] of [[LCD Soundsystem]]. After Doyle toured with Murphy, Murphy sent a text message that said, "Sorry you can't have Al back, I'll wrestle you for him. And I'll beat you because I'm bigger and stronger than you."<ref name="Sun"/> Goddard explained that "the words in that song are about wrestling", and the band "have no doubt" that Murphy would defeat them in a wrestling fight.<ref name="Sun"/>

The song "[[Ready for the Floor]]" contains an allusion to the 1989 film, ''[[Batman (1989 film)|Batman]]'', with the line, "You're my number one guy". In an interview with ''The Fader'' magazine, Taylor said the reference was a result of thinking about the Batman film, which has many things that Taylor is fond of, such as the Prince soundtrack. He commented that sometimes those items "seep into what we're writing about" and said that he likes to reference "in an oblique way". He conjectured that he had included the line to say something to "everyone in the band, particularly to Joe [Goddard], 'You're my number one guy, why is there any problems between any of us?' "<ref name="Fader">{{cite web| last = | first = | title = Video: Hot Chip: "Ready for the Floor" | publisher = ''The Fader''| url =http://www.thefader.com/articles/2007/12/5/hot-chip-s-ready-for-floor-video-explained-kind-of| accessdate = 3 July 2008|date=12 May 2007}}</ref>

"Shake a Fist" was written by Taylor after Goddard took a legal herbal substance during the [[Glastonbury Festival]] which produced feelings akin to teleportation.<ref name="Sun"/> Goddard said, "it was a brilliant time" and told Alexis to write down words to go with his experience.<ref name="Sun"/> The song features a [[Sampling (music)|voice sample]] from [[Todd Rundgren]].<ref name="Sun"/>

"Out at the Pictures" is an oblique homage to inexpensive British pub chain [[Wetherspoons]], who are thanked in the album inlay.

== Featured collaborators ==
In March 2008, Hot Chip re-recorded several songs from ''Made in the Dark'', with one of their "all-time heroes", [[Robert Wyatt]].<ref name="Sun"/> Doyle said the band had wanted it to happen "for ages" and that people would see it as a surprise [[collaboration]].<ref name="Sun"/> Taylor said, "I crave confusion and quite like people to be surprised. I try to do that in the words and music and try to be what people don't think we are. Robert is someone we have liked for much longer than people realise. We feel closer to people in a different field, age or era."<ref name="Sun"/> The album features Emma Smith, who had previously joined with Hot Chip to play violin and saxophone on their first two albums.<ref name="Interview2"/>

=== Kylie Minogue ===
[[File:Kylie on tour wearing blue - KYLIEX2008.jpg|thumb|Kylie Minogue]]
In October 2007, [[MTV]] reported that Hot Chip was planning to give "[[Ready for the Floor]]" to [[Kylie Minogue]].<ref>{{cite web|last =Thorogood |first =Tom |title = Hot Chip to work with Kylie?|publisher = [[MTV]]|url =http://www.mtv.co.uk/news/hot-chip/40207-hot-chip-to-work-with-kylie|accessdate = 3 July 2008| date=11 May 2007}}</ref> Other reports incorrectly suggested that [[Hot Chip]] had written ''Made in the Dark'''s second single, "[[Ready for the Floor]]" specifically for Minogue.<ref name="mistake"/> Taylor explained that it was a misunderstanding; "It started because someone asked me if we would ever write for Kylie. I said that we'd been asked to write for her but we'd never got round to it. And I said if we had to give her one of our songs, "Ready for the Floor" would probably be the most suitable. From that, I got misquoted. We didn't write "Ready for the Floor" for Kylie, didn't send it to her and she never heard it."<ref name="Manchester">{{cite web| last = Ryan| first = Gary| title = Geeky date with Hot Chip | publisher = ''[[Manchester Evening News]]''| url =http://www.citylife.co.uk/news_and_reviews/news/802_geeky_date_with_hot_chip| accessdate = 3 July 2008| date=14 February 2008}}</ref>

After the first rumour circulated, Joe Goddard created a reverse rumour, saying that Minogue had written a track for Hot Chip. He told ''[[NME]]'', "Kylie wrote a song for us, She sent it through our management to us&nbsp;– it was totally bizarre. It was the beginnings of a track&nbsp;– I think she wrote it and just thought, 'This would be perfect for Hot Chip', or possibly for a collaboration. It's a crazy song. It's industrial and clanging and even has farmyard animal noises on it. It's the kind of music you'd never normally associate with Kylie. When I'm allowed to send it around it's going to change a few people's ideas about her. Maybe it's one for our next album, after ''Made in the Dark''."<ref name="mistake">{{cite web|last = |first = |title = Kylie Minogue writes song for Hot Chip|publisher = ''[[NME]]''|url =http://www.nme.com/news/hot-chip/33324|accessdate = 3 July 2008| date=21 December 2007}}</ref> Taylor later admitted that it was a joke created to fool people because the band were tired of people phoning them up to ask why Minogue had apparently rejected a song she did not hear.<ref name="Manchester"/> Goddard said, "We've been telling lots of lies and rumours about it because we found it quite funny."<ref name="Sun">{{cite web|last =Swift |first =Jacqui |title =I like people to be surprised|work = ''[[The Sun (newspaper)|The Sun]]''|url =http://www.thesun.co.uk/sol/homepage/showbiz/sftw/article748806.ece|accessdate = 3 July 2008|date=1 February 2008}}</ref>

In January 2010, Goddard stated that "at the beginning there was some contact. As I recall, we were going to do a session with one of her writers" but both Hot Chip and Minogue were too busy at the time. He added "there was a grain of truth in the beginning, but then it all snowballed into some big silly thing". When asked, in 2010, about giving a song to Minogue, he stated that it would be "amazing".<ref>{{cite web|last =Nissim |first =Mayer |title =Hot Chip: 'We'd love to work with Kylie'|work =[[Digital Spy]]|url =http://www.digitalspy.co.uk/music/news/a200276/hot-chip-wed-love-to-work-with-kylie.html|accessdate = 30 January 2010|date=29 January 2010}}</ref>

== Release and reception ==
''Made in the Dark'' charted for 23 weeks in over 10 different charts, entering the [[UK Album Chart]] at number four,<ref name="UKchart"/> the [[Billboard charts#top|''Billboard'' Top Heatseekers]] chart at number one and the [[Dance/Electronic Albums|''Billboard'' Top Electronic Albums]] chart at number two.<ref name="Billboard">{{cite web| last = | first = | title = Artist Chart History - Hot Chip | publisher = ''[[Billboard (magazine)|Billboard]]''| url ={{BillboardURLbyName|artist=hot chip|chart=all}}| accessdate = 3 July 2008}}</ref> The album, according to [[Nielsen SoundScan]] data reported by ''[[Billboard (magazine)|Billboard]]'', has sold 47,000 copies and has been certified Gold for UK sales.<ref>{{cite web| last =Ayers| first =Michael D.| title =Hot Chip Inspired By Susan Boyle To Take 'One Life Stand' | publisher = ''[[Billboard (magazine)|Billboard]]''| url =http://www.billboard.com/articles/news/959741/hot-chip-inspired-by-susan-boyle-to-take-one-life-stand| accessdate = 31 January 2010| date=14 January 2010}}</ref><ref name="ukcert">[http://www.bpi.co.uk/certifiedawards/search.aspx "Certified Awards Search"]. [[British Phonographic Industry]]. 3 October 2008. Retrieved 13 February 2009.</ref>

"[[Shake a Fist]]", the first single to be released from the album, was released on 12" [[vinyl single]] at the beginning of October 2007 but did not chart. The second single released, "[[Ready for the Floor]]", charted for 24 weeks in five different charts, peaking at number six on the UK Singles Top 75.<ref name="UK"/>

=== Promotion ===
In January 2008, in order to promote ''Made in the Dark'', Hot Chip "locked [themselves] away" to practice in anticipation of an American tour, where material from their new album would be played. The band are not accustomed to such an intensive rehearsal regime, as they "usually just rehearse for two days and then go on tour".<ref name="Sun"/>

=== Critical reception ===
<!-- there should be no more then 10 album reviews in the infobox Per WP:Albums. To add or remove an album, discuss it on the talk page first -->
{{Album ratings
|MC = 78/100<ref>{{cite web|title=''Made In The Dark'' by Hot Chip|url=http://www.metacritic.com/music/made-in-the-dark/hot-chip|publisher=[[Metacritic]]|accessdate=14 October 2016|quote=}}</ref>
|rev1=[[AllMusic]]
|rev1Score={{Rating|4|5}}<ref name="AMG" />
|rev2=''[[Blender (magazine)|Blender]]''
|rev2Score={{Rating|4|5}}<ref>{{cite web|author=Lim, Dennis |title=Made In The Dark - Blender |url=http://www.blender.com/guide/reviews.aspx?id=4983 |date=5 February 2008 |work=[[Blender (magazine)|Blender]] |accessdate=15 July 2009 |deadurl=yes |archiveurl=https://web.archive.org/web/20081228210655/http://www.blender.com/guide/reviews.aspx?id=4983 |archivedate=28 December 2008 }}</ref>
|rev3=''[[The Guardian]]''
|rev3Score={{Rating|3|5}}<ref name="GuardReview" />
|rev4=[[MusicOMH]]
|rev4Score={{Rating|3.5|5}}<ref name="Omh" />
|rev5=''[[The Observer]]''
|rev5Score={{Rating|4|5}}<ref name="ObsReview" />
|rev6=[[Pitchfork Media]]
|rev6score=(7.0/10)<ref name="pitchforkalbum" />
|rev7=''[[Q (magazine)|Q]]''
|rev7Score={{Rating|4|5}}<ref>{{cite journal |date=March 2008 |journal=[[Q (magazine)|Q]] |pages=101}}</ref>
|rev8=''[[Rolling Stone]]''
|rev8Score={{Rating|4|5}}<ref name="Rolling" />
|rev9=''[[The Times]]''
|rev9Score={{Rating|4|5}}<ref name="Times" />
|rev10=''[[Uncut (magazine)|Uncut]]''
|rev10Score={{Rating|4|5}}<ref>{{cite web |title=Hot Chip - Made In The Dark - Review - Uncut.co.uk |url=http://www.uncut.co.uk/music/hot_chip/reviews/10980 |work=[[Uncut (magazine)|Uncut]] |accessdate=15 July 2009}}</ref>
}}
The critical reception to the album was generally favourable. Based on 40 reviews, review aggregate website [[Metacritic]] reported a rating of 78 out of 100.<ref name="Metacritic">{{cite web|last =|first = |title =Made in the Dark |publisher = [[Metacritic]]|url =http://www.metacritic.com/music/artists/hotchip/madeintheidark?q=made%20in%20the%20dark|accessdate = 3 July 2008}}</ref> There were mixed comments about the ballads; two reviewers noted a disparity between the energy of the ballads to different songs. ''[[Drowned in Sound]]'' commented that, "ballads remain a strong suit, particularly the easy grace of the title track, but more often than not sit awkwardly next to the more toothsome numbers and feel under-produced by comparison"<ref>{{cite web|last =Denney|first =Alex |title =Hot Chip: Made in the Dark|publisher = ''[[Drowned in Sound]]''|url =http://www.drownedinsound.com/release/view/12240|accessdate = 3 July 2008|date=|archiveurl =https://web.archive.org/web/20080609040422/http://www.drownedinsound.com/release/view/12240 <!--Added by H3llBot-->|archivedate =9 June 2008}}</ref> with similar comments from [[AllMusic]] who said, "''Made in the Dark'''s main weakness might be its ballads, but that may just be in comparison to its many energetic moments, which are so addictive that it feels like a forced come-down whenever the band slows things down."<ref name="AMG">{{cite web|last =Phares|first =Heather |title =Made in the Dark Review|publisher = [[AllMusic]]|url ={{Allmusic|class=album|id=r1309996|pure_url=yes}}|accessdate = 3 July 2008|date=}}</ref> However, ''[[The Observer]]'' gave a positive evaluation of the ballads; "Hot Chip have had a happy way with a subliminal power ballad. And ''Made in the Dark'' can boast four of the best."<ref name="ObsReview">{{cite web|last =Thompson|first =Ben |title =Hot Chip, Made in the Dark |publisher = ''[[The Observer]]''|url =https://www.theguardian.com/music/2008/jan/20/electronicmusic.shopping|accessdate = 3 July 2008|date=20 January 2008}}</ref> [[Pitchfork Media]] rated the album as 7/10 with a mixed review, describing it as a "patchy, turbulent record" due to the use of many different individual components and also said that it was a "good record but not a great one".<ref name="pitchforkalbum">{{cite web|last =Pytlik|first =Mark|title =Hot Chip - Made in the Dark |publisher = [[Pitchfork Media]]|url =http://www.pitchforkmedia.com/article/record_review/48450-made-in-the-dark|accessdate = 3 July 2008|date=5 February 2008|archiveurl =https://web.archive.org/web/20080607105808/http://www.pitchforkmedia.com/article/record_review/48450-made-in-the-dark <!--Added by H3llBot-->|archivedate =7 June 2008}}</ref> Martin responded to the criticism made by Pitchfork Media:
{{blockquote|I think the fact that we've managed to be successful, in getting good chart positions in the U.K. and at the same time making a record that is actually quite weird and confusing to even a site like Pitchfork—the guy doesn't seem to actually get what we're trying to do—it's kind of cool to me.<ref name="battle"/>}}

Pitchfork Media, despite their initial rating, went on to list the album number 23 on their list of the fifty best albums of 2008, and would later state that "its bold charms have lent it a fond longevity."<ref>http://pitchfork.com/reviews/albums/16706-in-our-heads/</ref> Another element that caused mixed reception was the use of a Todd Rundgren sample in "Shake a Fist", which musicOMH.com called "delightful" but ''[[The Guardian]]'' described it as grating.<ref name="Omh">{{cite web|last =Ahmad|first =Jamil |title =Hot Chip - Made in the Dark |publisher = musicOMH.com|url =http://www.musicomh.com/albums/hot-chip-2_0108.htm|accessdate = 3 July 2008}}</ref><ref name="GuardReview">{{cite web|last =Macpherson|first =Alex |title =Hot Chip, Made in the Dark |publisher = ''[[The Guardian]]''|url =https://www.theguardian.com/music/2008/feb/01/electronicmusic.shopping|accessdate = 3 July 2008|date=1 February 2008}}</ref> musicOMH.com, whose description of the album was positive, said that tracks "Ready for the Floor" and "Bendable Poseable" had elements reminiscent of previous album ''The Warning''.<ref name="Omh"/> ''[[The Times]]'' said that although the execution was "novel" and the song "Made in the Dark" was "exquisite", that much of ''Made in the Dark'' "seems to spring from sticky relationship issues". Comparisons were also made to [[Paul McCartney]]'s ''[[McCartney II]]'' album with songs like "Wrestlers", "Bendable Poseable", "Whistle for Will" and "We're Looking for a Lot of Love", which were described as having the "airless proto-electronica" of ''[[McCartney II]]''.<ref name="Times">{{cite web|last =Paphides|first =Pete|title =Hot Chip: Made in the Dark |publisher = ''[[The Times]]''|url =http://entertainment.timesonline.co.uk/tol/arts_and_entertainment/music/cd_reviews/article3283938.ece|accessdate = 3 July 2008|date=1 February 2008}}</ref>

In regards to lyrics, [[Allmusic]] said the album "boasts some of Hot Chip's most [[kinetic energy|kinetic]] music, with rhythms and melodies that are just as hyper-articulate as the [[word play]]."<ref name="AMG"/> ''[[Rolling Stone]]'' summarised the album as having "catchy tunes, monster grooves, and lyrics resolving the heartfelt and the smartass".<ref name="Rolling">{{cite web|last =Hermes|first =Will |title =Made in the Dark Review |publisher = ''[[Rolling Stone]]''|url =http://www.rollingstone.com/reviews/album/18193914/review/18206653/made_in_the_dark?rating=11|accessdate = 3 July 2008|date=7 February 2008|archiveurl=https://web.archive.org/web/20081229225345/http://www.rollingstone.com/reviews/album/18193914/review/18206653/made_in_the_dark?rating=11|archivedate=29 December 2008}}</ref>

==Track listing==
All songs written by Hot Chip, except "Shake a Fist", co-written by [[Todd Rundgren]].
{{Track list
|title1 = Out at the Pictures 
|length1 = 4:26
|title2 = [[Shake a Fist]]  
|length2 = 5:11
|title3 = [[Ready for the Floor]] 
|length3 = 3:52
|title4 = Bendable Poseable 
|length4 = 3:46
|title5 = We're Looking for a Lot of Love 
|length5 = 4:43
|title6 = Touch Too Much 
|length6 = 4:05
|title7 = Made in the Dark 
|length7 = 3:00
|title8 = [[One Pure Thought]] 
|length8 = 4:53
|title9 = Hold On 
|length9 = 6:20
|title10 = Wrestlers 
|length10 = 3:45
|title11 = Don't Dance 
|length11 = 4:42
|title12 = Whistle for Will 
|length12 = 2:23
|title13 = In the Privacy of Our Love 
|length13 = 2:52
}}
;iTunes bonus tracks<ref>{{cite web |url=https://itunes.apple.com/gb/album/made-in-the-dark/id272000331 |title=Made In the Dark by Hot Chip |work=[[iTunes Store]] UK |publisher=[[Apple Inc.]] |accessdate=31 March 2011}}</ref>
#<li value=14>"So Deep" – 2:34
#"With Each New Day" – 2:58

;iTunes deluxe version bonus tracks<ref>{{cite web |url=https://itunes.apple.com/gb/album/made-in-dark-deluxe-version/id295242053 |title=Made In the Dark (Deluxe Version) by Hot Chip |work=[[iTunes Store]] UK |publisher=[[Apple Inc.]] |accessdate=31 March 2011}}</ref>
#<li value=14>"Touch Too Much" ([[Theo Keating|Fake Blood]] Remix) – 5:52
#"Hold On" ([[Switch (house DJ)|Switch]] LDN Remix) – 4:01
#"Touch Too Much" ([[Ewan Pearson]] Remix) – 9:32
#"Wrestlers" (Video) – 3:59
#West Coast Tour Documentary – 17:25

;Japanese bonus tracks
#<li value=14>"Bubbles They Bounce" – 5:53
#"My Brother Is Watching Me" – 3:49

;Special edition bonus DVD
#"Shake a Fist" (Live at [[Melt! festival|Melt!]])
#"[[Boy from School|And I Was a Boy from School]]" (Live at Melt!)
#"Hold On" (Live at the [[Electric Ballroom]])
#"One Pure Thought" (Live at [[Glastonbury Festival]])
#"[[Over and Over (Hot Chip song)|Over and Over]]" (Live at Glastonbury Festival)

== Personnel ==
* Dan Carey&nbsp;– [[Audio mixing (recorded music)|mixing]]
* Owen Clarke&nbsp;– [[Art design|design]]
* Jonathan Digby&nbsp;– [[Audio engineering|engineer]]
* Ian Dowling&nbsp;– engineer (assistant)
* Matt Edwards&nbsp;– [[A&R]]
* Joe Goddard&nbsp;– mix control
* James Shaw&nbsp;– engineer (assistant)
* Darren Simpson&nbsp;– engineer (assistant)
* Alexis Smith&nbsp;– mixing (assistant)
* Emma Smith&nbsp;– [[violin]], [[saxophone]]
* Wallzo&nbsp;– design
* Alexis Taylor – vocals, synthesizer, guitar, percussion, piano
* Joe Goddard – vocals, synthesizer, percussion
* Owen Clarke – guitar, bass
* Al Doyle – guitar, synthesizer, percussion, backing vocals
* Felix Martin – drum machines

== Chart positions ==

=== Album ===

{| class="wikitable sortable"
!Chart
!Peak<br />position
|-
|[[ARIA Charts|Australian Albums Chart]]<ref name="aus">{{cite web| last = | first = | title = Made in the Dark - Australian chart position | publisher = Australian-charts.com | url =http://australian-charts.com/showitem.asp?interpret=Hot+Chip&titel=Made+In+The+Dark&cat=a| accessdate = 3 July 2008}}</ref>
|align="center"|25
|-
|[[Ultratop|Belgium Albums Chart]]<ref>{{cite web| last = | first = | title = Made in the Dark - Belgium chart position | publisher = Ultratop.be | url =http://www.ultratop.be/nl/showitem.asp?interpret=Hot+Chip&titel=Made+In+The+Dark&cat=a| accessdate = 3 July 2008|language=nl}}</ref>
|align="center"|25
|-
|[[Syndicat National de l'Edition Phonographique|French Albums Chart]]<ref>{{cite web| last = | first = | title = Made in the Dark - French chart position | publisher = Lescharts.com | url =http://lescharts.com/search.asp?cat=a&search=made+in+the+dark| accessdate = 3 July 2008|language=fr}}</ref>
|align="center"|89
|-
|[[GfK Entertainment|German Albums Chart]]<ref name="Germany">{{Cite web|url=http://www.musicline.de/de/chartverfolgung_summary/artist/Hot+Chip/longplay | title=Made in the Dark - German chart position|accessdate=23 August 2008 |publisher=Musicline.de|language=de}}</ref>
|align="center"|34
|-
|[[Irish Albums Chart]]<ref name="irma">{{cite web| last = | first = | title = Made in the Dark - Irish chart position | publisher = Irma.ie | url =http://www.irma.ie/charts_archive/week06_08.asp| accessdate = 3 July 2008}}</ref>
|align="center"|9
|-
|[[Sverigetopplistan|Swedish Albums Chart]]<ref>{{cite web| last = | first = | title = Made in the Dark - Swedish chart position | publisher = Swedishcharts.com | url =http://swedishcharts.com/showitem.asp?interpret=Hot+Chip&titel=Made+In+The+Dark&cat=a| accessdate = 3 July 2008}}</ref>
|align="center"|35
|-
|[[UK Albums Chart]]<ref name="UKchart"/>
|align="center"|4
|-
|U.S. [[Billboard 200|''Billboard'' 200]]<ref name="Billboard"/>
|align="center"|109
|-
|U.S. [[Top Heatseekers]]<ref name="Billboard"/>
|align="center"|1
|-
|U.S  [[Top Electronic Albums]]<ref name="Billboard"/>
|align="center"|2
|-
|}

=== Singles ===
{| class="wikitable"
! rowspan="2"| Song
! colspan="4"| Chart peak positions
|-
! width="55"|<small>[[Ö3 Austria Top 40|Austrian Singles Chart]]<br /></small>
! width="55"|<small>[[Irish Singles Chart]]<br /></small>
! width="55"|<small>[[UK Singles Chart]]<br /></small>
! width="55"|<small>[[UK Download Chart]]<br /></small>
|-
|"[[Shake a Fist]]"
|align="center"|—
|align="center"|—
|align="center"|—
|align="center"|—
|-
|"[[Ready for the Floor]]"
|align="center"|73<br><ref>{{cite web| last = | first = | title = Made in the Dark - Austrian chart position | publisher = Austrian-charts.com | url =http://austriancharts.at/showitem.asp?interpret=Hot+Chip&titel=Ready+For+The+Floor&cat=s| accessdate = 3 July 2008|language=de}}</ref>
|align="center"|22<br><ref name="irma"/>
|align="center"|6<br><ref name="UK">{{cite web| last = | first = | title = Ready for the Floor - UK chart position| publisher = The Official Charts | url =http://www.officialcharts.com/search/singles/ready%20for%20the%20floor/|accessdate = 21 February 2016}}</ref>
|align="center"|9<br><ref>{{cite web| last = | first = | title = Ready for the Floor - UK Download chart position| publisher = The Official Charts | url =http://www.officialcharts.com/charts/singles-downloads-chart/20080203/7000/|accessdate = 21 February 2016}}</ref>
|-
|"[[One Pure Thought]]"
|align="center"|—
|align="center"|—
|align="center"|53
|align="center"|—
|-
|"[[Wrestlers]]"
|align="center"|—
|align="center"|—
|align="center"|—
|align="center"|—
|-
|}
"—" denotes releases that did not chart.

== References ==
{{Reflist|30em}}

== External links ==
* {{MusicBrainz release|id=430826de-7856-412a-a644-3344e0b9d09e|name=Made in the Dark}}
* [http://www.last.fm/music/Hot%20Chip/Made%20in%20the%20dark ''Made in the Dark''] at [[Last.fm]]

{{Hot Chip}}

{{featured article}}

[[Category:2008 albums]]
[[Category:Hot Chip albums]]