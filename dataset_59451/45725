{{other people | Edward Warner}}
{{Use mdy dates|date=November 2011}}
'''Edward Pearson Warner''' (November 9, 1894, [[Pittsburgh, Pennsylvania|Pittsburgh]] – July 11, 1958, [[Duxbury, Massachusetts|Duxbury]]) was an [[United States|American]] pioneer in [[aviation]] and a teacher in [[aeronautical engineering]]. Besides that he was also a writer, scientist and a Statesman, a member of the [[Civil Aeronautics Board]] at its founding in 1938, a Delegate of the United States to the 1944 [[Chicago Conference]] for the [[Convention on International Civil Aviation]], and an international civil servant. Edward Warner's achievements are commemorated by the world's civil aviation community in the international award that bears his name.

==Biography==
Edward Warner grew up in Boston, Massachusetts, where he went to the [[Volkmann School]]. He studied at [[Harvard University]] in 1916 and specialized in mathematics. After graduating he went on to the [[Massachusetts Institute of Technology]] (MIT), where he graduated in 1917 in [[mechanical engineering]] with additional credits in [[naval architecture]].

At the end of [[World War I]], he was appointed Chief Physicist of the [[National Advisory Committee for Aeronautics]] (NACA) in charge of aerodynamic research at the station at [[Langley Field]], which NACA had just established. From 1920 to 1926, he was an associate professor of aeronautics at MIT. From 1926 to 1929, he served as the first [[Assistant Secretary of the Navy (AIR)]].

After [[World War II]], Edward Warner became one of the leading persons in the raising of [[civil aviation]], realizing his dream of a world air transport system based on international co-operation, which is now known as the [[International Civil Aviation Organization]] (ICAO). He became the first President of the Council of ICAO during its provisional status from 1945 to 1947, and continued as President until his retirement in 1957.

In 1956, ICAO received from the City of [[Genoa]] its famed [[Christopher Columbus Award]], which included a sum of more than $7,000, for ICAO's efforts in the development of international co-operation in air transportation. The ICAO Council subsequently decided to use these funds to establish its own continuing series of awards which would commemorate Dr. Edward Warner's great spirit and service to international civil aviation and give special recognition to individuals or institutions whose contribution towards the further development of civil aviation is outstanding. This became the [[Edward Warner Award]].

==Awards==
*[[Wright Brothers Medal]] 1932
*[[Daniel Guggenheim Medal]] 1949
*[[Wright Brothers Memorial Trophy]] 1956

==External links==
*[http://www.icao.int/icao/en/biog/warner.htm Warner Bio]

{{s-start}}
{{s-gov}}
{{succession box|
 before=New office|
 title=[[Assistant Secretary of the Navy (AIR)]]|
 after=[[David Sinton Ingalls]]|
 years=	July 7, 1926 &ndash; March 15, 1929
}}
{{s-end}}
{{USSecNavy}}

{{Authority control}}

{{DEFAULTSORT:Warner, Edward Pearson}}
[[Category:1894 births]]
[[Category:1958 deaths]]
[[Category:Harvard University alumni]]
[[Category:American aerospace engineers]]
[[Category:International Civil Aviation Organization people]]
[[Category:Wright Brothers Medal recipients]]
[[Category:United States Assistant Secretaries of the Navy]]