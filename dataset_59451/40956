[[File:Heather Spohr 2010.jpg|thumb|Spohr in 2010]]

'''Heather Spohr''' (born June 27, 1979) is an [[United States|American]] [[Wiktionary:blogger|blogger]] and philanthropist whose award-winning blog, [http://www.thespohrsaremultiplying.com/ The Spohrs Are Multiplying] initially became popular as she detailed her family’s experiences dealing with a high risk pregnancy, an extended NICU stay, and the difficulties of caring for a premature baby.

==Biography & Career==
Born and raised in [[Newbury Park, California]], Spohr (''née'' Buchanan) graduated from [[Newbury Park High School]] in 1997. She went on to receive a degree in Communication from the [[University of Southern California]] in 2000. While at USC she participated in many clubs and organizations, including [[Delta Gamma]] Sorority.

Prior to beginning her career as a blogger, Spohr worked in A&R for [[Verve Records|Verve Music Group]] and as a Sales Executive with the [[Los Angeles Dodgers]].

She is married to writer Michael Spohr with whom she has had three children, Madeline Alice (November 11, 2007 – April 7, 2009),<ref>{{cite web|author=Heather |url=http://thespohrsaremultiplying.com/email-update/shes-here/ |title=She’s Here — The Spohrs Are Multiplying |publisher=Thespohrsaremultiplying.com |date=2007-11-12 |accessdate=2012-05-22}}</ref> Annabel Violet (born January 22, 2010),<ref>{{cite web|url=http://thespohrsaremultiplying.com/binky/here-comes-the-sun/ |title=Here Comes The Sun — The Spohrs Are Multiplying |publisher=Thespohrsaremultiplying.com |date=2010-01-23 |accessdate=2012-05-22}}</ref> and James Asher (born May 30, 2013).<ref>{{cite web|url=http://www.thespohrsaremultiplying.com/family-and-friends/more-love/|publisher=Thespohrsaremultiplying.com|accessdate=1 June 2013|title=More Love}}</ref>

==The Spohrs Are Multiplying==

Blogging since 2002, Spohr started The Spohrs Are Multiplying in June 2007 in order to keep friends and family informed about her high risk pregnancy with her first daughter, Madeline (her previous blog has since been folded into TSAM). Following Madeline’s premature birth, Spohr continued to blog and attracted a growing readership as she detailed the sixty-eight days Madeline spent in the Neonatal Intensive Care Unit, as well happier times once Madeline went home.

Humorous as well as poignant, a post of Spohr’s about Madeline’s love for NBC’s [[Today (NBC program)|Today Show]] host [[Matt Lauer]] was featured on the msnbc.com website in February 2009.<ref>{{cite web|url=http://allday.todayshow.com/_news/2009/02/27/4378134-mad-for-matt-baby-has-a-crush-on-today-host |title=allDAY - Mad for Matt: Baby has a crush on TODAY host |publisher=Allday.todayshow.com |date= |accessdate=2012-05-22}}</ref>

Madeline died unexpectedly in April 2009 due to complications related to her prematurity.<ref>{{cite web|author=Heather |url=http://thespohrsaremultiplying.com/the-famous-madeline/madeline-alice-spohr/ |title=Madeline Alice Spohr — The Spohrs Are Multiplying |publisher=Thespohrsaremultiplying.com |date=2009-04-07 |accessdate=2012-05-22}}</ref> Since then Heather has continued to chronicle her family’s story as they journey through grief, and has given a voice to families who have suffered the devastation of losing a child.

The Spohrs Are Multiplying reaches over 560,000 people monthly<ref>{{cite web|url=https://advertisers.federatedmedia.net/explore/view/thespohrsaremultiply |title=Explore sites in the Federated Media network |publisher=Advertisers.federatedmedia.net |date= |accessdate=2012-05-22}}</ref> and has received multiple awards, including top spots with Babble.com<ref>{{cite web|url=http://www.babble.com/babble-50/mommy-bloggers/the-spohrs-are-multiplying/ |title=Top 50 Mom Blogs: all the Best Mom Blogs, including The Spohrs Are Multiplying |publisher=Babble.com |date= |accessdate=2012-05-22}}</ref> and TheBump.com.<ref>{{cite web|url=http://pregnant.thebump.com/extras/mommy-blog-awards/articles/mommy-blog-awards-judge-heather-sphor.aspx |title=Mommy Blogger Judge: Heather Spohr |publisher=Pregnant.thebump.com |date=2010-11-01 |accessdate=2012-05-22}}</ref> Spohr and the site have been featured on numerous media outlets, including CNN,<ref>{{cite web|author= |url=https://www.youtube.com/watch?v=L_-F-tGoHK4 |title=Heather Spohr on Headline News |publisher=YouTube |date=2009-12-20 |accessdate=2012-05-22}}</ref> The Washington Post,<ref>{{cite news|last=Garfinkle |first=Stacey |url=http://voices.washingtonpost.com/parenting/2009/04/a_little_girls_death_a_communi.html |title=A Little Girl's Death; A Community Inspired - On Parenting |publisher=Voices.washingtonpost.com |date=2009-04-14 |accessdate=2012-05-22}}</ref> The Huffington Post<ref>{{cite news|url=http://www.huffingtonpost.com/michelle-lamar/maddie-spohr-tragic-death_b_185769.html |title=Michelle Lamar: Maddie Spohr: Tragic Death Inspires Web Community |publisher=Huffingtonpost.com |date= 2011-11-17|accessdate=2012-05-22}}</ref> and WomensHeath.gov<ref>{{cite web|url=http://www.womenshealth.gov/spotlight/2010/5.cfm |title=Interview with a Mom Surviving Loss, Heather Spohr << Spotlight on Women's Health << |publisher=Womenshealth.gov |date=2010-05-01 |accessdate=2012-05-22}}</ref> among others.

==Friends of Maddie and March of Dimes==

A fervent supporter of the [[March of Dimes]], Heather is nationally recognized as one of the organization’s top fundraisers.<ref>{{cite web|author=Heather |url=http://thespohrsaremultiplying.com/friends/because-of-you/ |title=Because Of You — The Spohrs Are Multiplying |publisher=Thespohrsaremultiplying.com |date=2009-10-12 |accessdate=2012-05-22}}</ref> She is also the President and Co-Founder of “Friends of Maddie,” a charitable organization that provides financial assistance to families who are suffering financial hardship due to the loss of a child. She has spoken at numerous conferences,<ref>{{cite web|author=Elisa CamahortComments (39) |url=http://www.blogher.com/announcing-blogher-09-community-keynote-selections |title=Announcing the BlogHer '09 Community Keynote Selections |publisher=BlogHer |date=2009-06-15 |accessdate=2012-05-22}}</ref><ref>{{cite web|url=http://2010.evoconference.com/check-out-our-speakers/ |title=Speakers « Evo 10 |publisher=2010.evoconference.com |date=2010-02-04 |accessdate=2012-05-22}}</ref><ref>[http://www.mom2summit.com/speakers/2011-speakers/ ] {{webarchive |url=https://web.archive.org/web/20110103063934/http://www.mom2summit.com/speakers/2011-speakers/ |date=January 3, 2011 }}</ref> on CNN/Headline News,<ref>{{cite web|author= |url=https://www.youtube.com/watch?v=L_-F-tGoHK4 |title=Heather Spohr on Headline News |publisher=YouTube |date=2009-12-20 |accessdate=2012-05-22}}</ref> and before members of Congress.<ref>{{cite web|url=http://thespohrsaremultiplying.com/heather/the-spohrs-go-to-washington/ |title=The Spohrs Go To Washington — The Spohrs Are Multiplying |publisher=Thespohrsaremultiplying.com |date=2009-10-13 |accessdate=2012-05-22}}</ref>

==Other Ventures==

A regular panelist for the online video series [[Momversation]], Heather is also a featured blogger at [[Babble.com]].

== References ==
<!--- See http://en.wikipedia.org/wiki/Wikipedia:Footnotes on how to create references using <ref></ref> tags which will then appear here automatically -->
{{Reflist}}

== External links ==
* The Spohrs Are Multiplying [http://www.thespohrsaremultiplying.com/]
* Friends of Maddie [http://www.friendsofmaddie.org]
* Heather Spohr's Professional Site [http://heatherspohr.com/]
* Heather Spohr on KTLA [https://www.youtube.com/watch?v=i5cGvWq_mdE&feature=player_embedded]

{{DEFAULTSORT:Spohr, Heather}}
[[Category:1979 births]]
[[Category:Living people]]
[[Category:American bloggers]]
[[Category:Articles created via the Article Wizard]]
[[Category:People from Newbury Park, California]]