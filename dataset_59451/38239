{{good article}}
{{Use British English|date=November 2014}}
{{Use dmy dates|date=November 2014}}
{{Infobox University Boat Race
| name= 65th Boat Race
| winner = Cambridge
| margin = 2 and 1/2 lengths
| winning_time= 19 minutes 20 seconds
| overall = 30–34
| umpire = [[Frederick I. Pitman]]<br>(Cambridge)
| date= {{Start date|1908|4|4|df=y}}
| prevseason= [[The Boat Race 1907|1907]]
| nextseason= [[The Boat Race 1909|1909]]
}}

The '''65th Boat Race''' took place on 4 April 1908.  Held annually, the Boat Race is a [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]].  Cambridge were reigning champions, having won the [[The Boat Race 1907|previous year's race]]. In a race umpired by [[Frederick I. Pitman]], Cambridge won by two-and-a-half lengths in a time of 19 minutes 20 seconds.  It was their third consecutive victory and their sixth win in seven races, taking the overall record in the event to 34&ndash;30 in Oxford's favour.

==Background==
[[File:Muttlebury SD Vanity Fair 1890-03-22.jpg|right|thumb|Former [[Cambridge University Boat Club]] rower [[Stanley Muttlebury]] coached the Light Blues.]]
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 20 August 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  The race was first held in 1829, and since 1845 has taken place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 20 June 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref><ref>{{Cite web | url = http://theboatraces.org/the-course | title = The Course| accessdate = 24 July 2014 | publisher = The Boat Race Company Limited}}</ref>  The rivalry is a major point of honour between the two universities; it is followed throughout the United Kingdom and, as of 2015, broadcast worldwide.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford&ndash;Cambridge Boat Race crew|date=6 April 2014|publisher=[[CBC News]] |url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=20 August 2014}}</ref>  Cambridge went into the race as reigning champions, having won the [[The Boat Race 1907|1907 race]] by four-and-a-half lengths,<ref name=results>{{Cite web | url = http://theboatraces.org/results| publisher = The Boat Race Company Limited| title = Boat Race – Results| accessdate = 20 August 2014}}</ref> while Oxford led overall with 34 victories to Cambridge's 29 (excluding the [[The Boat Race 1877|"dead heat" of 1877]]).<ref>{{Cite web | url= http://theboatraces.org/classic-moments-the-1877-dead-heat | publisher = The Boat Race Company Limited | title = Classic moments – the 1877 dead heat | accessdate =20 August 2014}}</ref>

Cambridge's coaches were L. H. K. Bushe-Fox, Francis Escombe (for the fifth consecutive year), [[Stanley Muttlebury]], five-time Blue between 1886 and 1890, and David Alexander Wauchope (who had rowed in the [[The Boat Race 1895|1895 race]]).  Oxford were coached by [[Harcourt Gilbey Gold]] (Dark Blue president for the [[The Boat Race 1900|1900 race]] and four-time Blue) and R. P. P. Rowe, who had rowed four times between 1889 and 1892.<ref>Burnell, pp. 110&ndash;111</ref>  For the fifth year the umpire was old [[Eton College|Etonian]] [[Frederick I. Pitman]] who rowed for Cambridge in the [[The Boat Race 1884|1884]], [[The Boat Race 1885|1885]] and [[The Boat Race 1886|1886 races]].<ref>Burnell, pp. 49, 108</ref>

==Crews==
The Cambridge crew weighed an average of 12&nbsp;[[Stone (unit)|st]] 3.25&nbsp;[[Pound (mass)|lb]] (77.5&nbsp;kg), {{convert|3.5|lb|kg|1}} per rower more than their opponents.<ref name=burn69>Burnell, p. 69</ref>   Oxford's crew contained four members with previous Boat Race experience, including [[Alister Kirby]] and [[Sir Albert Gladstone, 5th Baronet|Albert Gladstone]] who were rowing in their third race.  Six of the Dark Blue crew were educated at Eton College.  Cambridge also saw four members return, with [[Douglas Stuart (rower)|Douglas Stuart]] and [[Eric Powell (rower)|Eric Powell]] making their third appearances in the event.<ref>Burnell, pp. 68&ndash;69</ref>  Light Blue number two George Eric Fairbairn was following in his uncle's footsteps:<ref>{{Cite web | url = http://oa.anu.edu.au/obituary/fairbairn-george-eric-1450 | publisher = Obituaries Australia | title = Fairbairn, George Eric (1888–1915) | accessdate = 20 January 2015}}</ref> [[Steve Fairbairn]] rowed for Cambridge four times in the 1880s.<ref>{{Cite web | url = http://adb.anu.edu.au/biography/fairbairn-stephen-steve-1080 |publisher = Obituaries Australia | title = Fairbairn, Stephen (Steve) (1862–1938) | accessdate = 20 January 2015 | first =Michael |last=D. De B. Collins Persse}}</ref> Oxford's number three, Australian [[Collier Cudmore]], was the only non-British participant registered in the race.<ref>Burnell, p. 39</ref>

Former Oxford rower and author George Drinkwater assessed the Cambridge crew as "better and stronger than in the previous year".<ref name=drink119>Drinkwater, p. 119</ref>  Conversely, "misfortune dogged the [Oxford] crew": firstly their number five of 1907, [[James Angus Gillan]] was available but forbidden to row by his doctors.  [[Influenza]] then swept through the crew, before Kirby was struck down by [[jaundice]]; despite not fully recovering, he took part in the race.<ref name=drink120/>

[[File:Stuart DCR Vanity Fair 1907-03-13.jpg|upright|thumb|right|[[Douglas Stuart (rower)|Douglas Stewart]] rowed at [[Stroke (rowing)|stroke]] for Cambridge.]]
{| class="wikitable"
|-
! rowspan="2" scope="col| Seat
! colspan="3" scope="col| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
! colspan="3" scope="col| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]] || Hon. R. P. Stanhope || [[Magdalen College, Oxford|Magdalen]] || 9&nbsp;st 10&nbsp;lb || [[Frank Jerwood|F. H. Jerwood]] || [[Jesus College, Cambridge|Jesus]] || 11&nbsp;st 10&nbsp;lb
|-
| 2 || [[Collier Cudmore|C. R. Cudmore]] || [[Magdalen College, Oxford|Magdalen]]  || 12&nbsp;st 0&nbsp;lb || G. E. Fairbairn || [[Jesus College, Cambridge|Jesus]] || 11&nbsp;st 13&nbsp;lb
|-
| 3 ||  E. H. L. Southwell ||  [[Magdalen College, Oxford|Magdalen]] || 12&nbsp;st 3&nbsp;lb || [[Oswald Carver|O. A. Carver]] || [[Trinity College, Cambridge|1st Trinity]] || 12&nbsp;st 10&nbsp;lb
|-
| 4 || A. E. Kitchin ||  [[St John's College, Oxford|St John's]] || 12&nbsp;st 7&nbsp;lb || H. E. Kitching || [[Trinity Hall, Cambridge|Trinity Hall]] || 13&nbsp;st 2&nbsp;lb
|-
| 5 || [[Alister Kirby|A. G. Kirby]] (P) ||  [[Magdalen College, Oxford|Magdalen]]|| 13&nbsp;st 7&nbsp;lb || J. S. Burn || [[Trinity College, Cambridge|1st Trinity]] || 12&nbsp;st 10.5&nbsp;lb
|-
| 6 || A. McCulloch ||  [[University College, Oxford|University]] || 12&nbsp;st 9.5&nbsp;lb || [[Edward Gordon Williams|E. G. Williams]] || [[Trinity College, Cambridge|3rd Trinity]] || 13&nbsp;st 0.5&nbsp;lb
|-
| 7 || H. R. Barker ||  [[Christ Church, Oxford|Christ Church]] || 12&nbsp;st 0.5&nbsp;lb || [[Eric Powell (rower)|E. W. Powell]] (P) || [[Trinity College, Cambridge|3rd Trinity]] || 11&nbsp;st 6&nbsp;lb
|-
| [[Stroke (rowing)|Stroke]] || [[Sir Albert Gladstone, 5th Baronet|A. C. Gladstone]] || [[Christ Church, Oxford|Christ Church]] || 11&nbsp;st 3.5&nbsp;lb || [[Douglas Stuart (rower)|D. C. R. Stuart]] || [[Trinity Hall, Cambridge|Trinity Hall]] || 11&nbsp;st 2&nbsp;lb
|-
| [[Coxswain (rowing)|Cox]] || A. W. F. Donkin || [[Magdalen College, Oxford|Magdalen]] || 8&nbsp;st 7&nbsp;lb || [[Richard Boyle (rower)|R. F. R. P. Boyle]] || [[Trinity Hall, Cambridge|Trinity Hall]] || 8&nbsp;st 10&nbsp;lb
|-
!colspan="7"|Source:<ref name=burn69>Burnell, p. 69</ref><br>(P) &ndash; boat club president<ref>Burnell, pp. 50&ndash;51</ref>
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]] along which the Boat Race is contested]]
Oxford won the toss and elected to start from the Surrey station, handing the Middlesex side of the river to Cambridge.<ref name=ross228/>  In a strong head-wind, umpire Pitman started the race at 3:30&nbsp;p.m.  The Light Blues made the better start, and slightly [[stroke rate|out-rating]] Oxford, began to pull away.  Rough water favoured Cambridge's style of rowing, and they were clear of Oxford by the time they passed the Mile Post.  The crews passed [[Harrods Furniture Depository]] with the Light Blues two lengths ahead and although the Dark Blues spurted again and again, they failed to make any inroads in the Cambridge lead, who began to pull away once again after passing under [[Barnes Railway Bridge|Barnes Bridge]].<ref name=drink120>Drinkwater, p. 120</ref>  They passed the finishing post leading by two and a half lengths in a time of 19 minutes 20 seconds.<ref name=ross228>Ross, p. 228</ref>  It was Cambridge's third consecutive victory, and their sixth in seven years, and took the overall record to 34&ndash;30 in Oxford's favour.  The winning time was the fastest since the [[The Boat Race 1902|1902 race]] and it was the narrowest margin of victory since the [[The Boat Race 1901|1901 race]].<ref name=results/>  Drinkwater described the race as "a much finer struggle than those of the two previous years".<ref name=drink119/>

==References==
'''Notes'''
{{reflist|30em}}

'''Bibliography'''
*{{Cite book | title = One Hundred and Fifty Years of the Oxford and Cambridge Boat Race | first = Richard | last = Burnell | authorlink = Dickie Burnell | year=1979| isbn= 978-0-9500638-7-4 | publisher = Precision Press}}
*{{Cite book | title = The University Boat Race &ndash; Official Centenary History | first = G. C. |last = Drinkwater | first2= T. R. B. |last2=Sanders |publisher = Cassell & Company, Ltd.| year = 1929}}
*{{Cite book | title = The Boat Race | first = Gordon |last = Ross | authorlink=Gordon Ross (writer)|publisher = The Sportmans Book Club| year = 1956| isbn=978-0-333-42102-4}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1908}}
[[Category:1908 in English sport]]
[[Category:The Boat Race]]
[[Category:April 1908 sports events]]
[[Category:1908 in rowing]]