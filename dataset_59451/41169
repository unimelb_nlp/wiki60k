{{Formal|date=January 2015}}

The '''curse of 39''' refers to the belief in some parts of [[Afghanistan]] that the number 39 is cursed or a badge of shame as it is purportedly linked with [[prostitution]].<ref name="WSJ">{{cite news|url=https://www.wsj.com/article/SB10001424052702303654804576347201745546630.html|title=A Symbol of Paid Companionship, No. 39 Is Afghans' Loneliest Number|last=Nissenbaum|first=Dion|work=The Wall Street Journal|date=2011-06-15}}</ref> 

== Origins ==
{{See|Prostitution in Afghanistan}}

The cause of the number's undesirability is unclear, but it has widely been claimed to have been associated with a pimp, allegedly living in the western city of [[Herat]], who was nicknamed "39" after the registration plate of his expensive car and the number of his apartment.<ref name="Guardian" /> The number is said to translate into ''morda-gow'', literally meaning "dead cow" but a well-known slang term for a pimp. Others have blamed corrupt police officials for spreading the rumour in order to  charge between $200–$500 to change a "39" plate.<ref name="NPR" /> 

Officials have, in turn, blamed car dealers and "those who work for the mafia [who] started the rumours about 39 so they could buy cars with 39 plates cheaper and sell them back for higher prices after changing the plates", according to Abdul Qader Samoonwal of Kabul's Traffic and License Registration department.<ref name="NPR" /> The problem is made worse by the fact that Afghanistan is a largely illiterate country in which rumours and superstitions thrive.<ref name="WSJ" />

== Perception ==
=== Vehicles ===
[[File:Street scene in Kabul-2012.jpg|thumb|300px|Vehicles in [[Kabul]], [[Afghanistan]]]]

[[Vehicle registration plate]]s incorporating the number are seen as so undesirable that vehicles and apartments bearing the numerals are said to be virtually unsellable in the capital, [[Kabul]].<ref name="Reuters">{{cite news|url=http://www.reuters.com/article/2011/06/15/uk-afghanistan-number-idUSLNE75E01T20110615|title=Cursed number "39" haunts Afghan car owners|last=Shalizi|first=Hamid|agency=Reuters|date=2011-06-15}}</ref> The drivers of such vehicles have reported receiving abuse and derision from pedestrians and other drivers, and some have had their registration plates altered to disguise the numbers. One such driver, Zalmay Ahmadi, told ''[[The Guardian]]'': "[W]hen I drive around all the other cars flash their lights, beep their horns and people point at me. All my classmates now call me Colonel 39."<ref name="Guardian">{{cite news|url=https://www.theguardian.com/world/2011/jun/15/afghanistan-curse-of-number-39|title=The curse of number 39 and the steps Afghans take to avoid it|last=Boone|first=Jon|work=The Guardian|date=2011-06-15}}</ref> 

A taxi driver, Ahmad Ghafor, said that he found that "it gets worse when I have women customers in the car. Other drivers signal to me or blow their horn saying 'shall we pay you to drop these ladies to my place?'"<ref name="BBC">{{cite news|url=http://www.bbc.co.uk/news/13815417|title=Count me out|last=Qadiry|first=Tahir|publisher=BBC News|date=2011-06-18}}</ref> 

The issue caused particular problems in Kabul after the [[Iranian_calendars#Modern_calendar_.28Solar_Hejri.29|Persian New Year]] of March 2011, as the government started to issue registration plates beginning with 39. (To compound the problem, this happened to be 1390 in the [[Iranian calendar]]).{{Update after|2012|04|reason=Please change to past tense, as the current Iranian calendar year will have finished.}} Despite the threat of penalties, many drivers refused to register their cars while the number 39 still appeared on them.<ref name="NPR">{{cite news|url=http://www.npr.org/2011/05/21/136496527/forget-unlucky-13-in-afghanistan-beware-39|title=Forget Unlucky 13. In Afghanistan, Beware 39|author=Shafi, Ahmad|author2=Sharifi, Najib|publisher=NPR|date=2011-05-21}}</ref>

=== Cellphone numbers ===
Cellphone owners have encountered similar problems, forcing them to hide their caller ID or switch their numbers altogether. One man with "39" in his [[Telephone number|phone number]] told the BBC: "I receive lots of anonymous calls asking if I have got prostitutes. I am known as Mr 39 amongst my friends."<ref name="BBC" />

=== Other appearances of 39 ===
Some 39-year-old Afghans are said to refer to themselves as being "one less than 40" or "one year to 40." During the [[Afghan parliamentary election, 2010|2010 parliamentary elections]], one candidate, Mullah Tarakhil, had the misfortune to be listed 39th on the ballot; two people were killed when his guards opened fire on civilians after a traffic accident, said to have been in reaction to people taunting the candidate over his number.<ref name="WSJ" /> 

== Reactions ==
Afghan government officials and numerologists have tried to scotch the claims about 39, though with little success. Gen. Assadullah, the head of the traffic department in Kabul, described the problem as "nonsense" as 39 is "just a number." He noted that there is no religious prohibition against the number and his department has sought to reassure the public by noting where Muslims can find the number 39 in the [[Quran]] and even publishing a formula by which the number can be derived from the name "[[Allah]]."<ref name="WSJ" /> 

Sediq Afghan, a famous numerologist, has likewise complained that people "only see the negative side" of the number and has called it "a sickness for Afghans". He told television viewers that associating the number with pimps "is a sin because 57 [[Sura]]s from our Quran contain the number 39."<ref name="NPR" /> The popular television satire show ''Danger Bell'' highlighted the issue but only succeeded in publicising it even further.<ref name="NPR" /> 

Some car dealers have been able to profit from it, as the problem exists mainly in Kabul; one dealer told ''[[The Wall Street Journal]]'' that he "could knock several thousand dollars off the purchase price of a car in Kabul with 39 on its plate and then turn around to sell it for a profit in the surrounding provinces."<ref name="WSJ" />

Owners of vehicles with "39" in their registration plates have sought to fix the problem themselves. Many have "edited" their own plates by painting or taping over the offending digits, or altering them to make the number 3 look like an 8, or even covering over the entire plate. One driver told [[NPR]]: "I have no choice but to drive this car since I earn my living working here. But I have to cover the 39 plate with a blue sheet. I do this to protect the dignity of this organization and also of myself."<ref name="NPR" />

==See also==
*[[Prostitution in Afghanistan]]
*[[Triskaidekaphobia]]
*[[Tetraphobia]]

==References==
{{reflist}}

==External links==
*[http://www.bbc.co.uk/news/world-asia-15769990 "Loya jirga: Afghan elders reject 'pimp's number 39'"], ''BBC News'', 17 November 2011

{{Superstitions}}

[[Category:Curses]]
[[Category:Numerology]]
[[Category:Sexuality in Afghanistan]]
[[Category:Urban legends]]
[[Category:Superstitions about numbers]]
[[Category:Superstitions of Afghanistan]]