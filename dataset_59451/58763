{{Infobox journal
| title         = Libertarian Communism
| cover         = Libertarian-Communism-Journal-1974-Issue-5.png
| editor        = 
| country       = United Kingdom
| frequency     = 
| history       = 1972–1976

}}
'''''Libertarian Communism''''' was a [[socialist]] [[academic journal|journal]] founded in 1974 and produced in part by members of the [[Socialist Party of Great Britain]].<ref>"papers relating to Libertarian Communism (a splinter group of the SPGB) including journals and miscellaneous correspondence, 1970-1980 (1 box)"[[Socialist Party of Great Britain|"Socialist Party of Great Britain" at Archives Hub at the Great Research Centre]]</ref>

==History==
During the 1960s the [[Socialist Party of Great Britain]] was enthused by a healthy influx of new recruits initially politicised by the [[CND]] marches, [[Vietnam War|Vietnam]] and the [[May 1968 in France|May Events of 1968]]. The boost to Party membership and activity at this
time was considerable.

Influenced by the prevailing political climate, some members who joined in this period wanted to change the emphasis of the Party’s propaganda efforts towards taking a more positive attitude to [[trade unionism|industrial struggles]], [[Claimants Union]]s and [[Tenants Association]]s but also to [[women's liberation]] and [[squatting]], arguing that the Party had developed a somewhat idealist conception of how [[class consciousness|socialist consciousness]] arises, being divorced from the day-to-day struggles of workers. To this effect fifteen activists from the ‘sixties generation’ signed a mini-[[manifesto]] in 1973 entitled “Where We Stand” which was circulated inside the Party. Although these ‘rebels’ in the Party were never a homogenous group, many more long-standing and traditional Party members felt uncomfortable with their line of argument.{{citation needed|date=September 2007}}

===Publication===
One particular group of these activists published an internal discussion bulletin, which, in 1974, converted itself into an externally oriented journal called ''Libertarian Communism''. This was produced with the aid of non-members and supported the idea of [[workers' council]]s. It openly attacked as ‘[[Karl Kautsky|Kautsky]]ite’ the Party’s traditional conception of the socialist revolution being facilitated through ‘bourgeois’ democracy and parliament. At the same time another group of younger members, based mainly in [[Aberdeen]] and [[Edinburgh]], was keen that the Party express support for such things as higher student grants (on the grounds that the Socialist Party was always prepared to support demands for higher wages) but the arguments of this group found no more favour with the majority in the Party than those put by the group around ''Libertarian Communism''. Indeed, both of these groups were to be charged and then expelled for issuing literature that contradicted official Party policy.{{citation needed|date=September 2007}}

==Aftermath==
The prominent activists of the time who were either expelled or left of their own volition typically became involved in [[single-issue campaigns]] or the [[radical feminist]] movement. However, one network of former members — those based around ''Libertarian Communism'', who were critical of the Party’s revolutionary strategy and attracted by [[council communism|council communist]] ideas — created an organisation called '''Social Revolution''' along with the Aberdeen and Edinburgh activists, which later joined the [[Solidarity (UK)|Solidarity]] group. Some years later a number of these activists were also involved in the foundation of the [[Wildcat (UK)|Wildcat]] council communist group and one of its successors, [[Subversion (UK)|Subversion]].

==Bibliography==
# “Critical theory and revolutionary practice” no. 1, October 1972
# Unnamed journal, (undated and unnumbered but annotated no. 2, April 1973)
# Unnamed journal, (undated and unnumbered but annotated no. 3, September 1973)
# “Revolutionary Communism”, no.4 (no date but late 1973)
# “Libertarian Communism”, no.5, April 1974
# “Libertarian Communism”, no.6, June 1974
# “Libertarian Communism”, no. 7, January 1975
# “Libertarian Communism”, no.8 (undated but June 1975
# “Libertarian Communism”, no.9 December 1975
# “Libertarian Communism”, no.10, July 1976

==References==
*{{cite journal
  | last = DAP
  | title = Getting Splinters
  | journal = Socialist Standard
  | volume = 100
  | issue = 1198
  | pages = 38–41
  | publisher = Socialist Party of Great Britain
  | date = June 2004
  | issn =  0037-8259
}}
{{reflist}}

{{DEFAULTSORT:Libertarian Communism (Journal)}}
[[Category:Socialist Party of Great Britain breakaway groups]]
[[Category:Publications established in 1974]]
[[Category:Political science journals]]