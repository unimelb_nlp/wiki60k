[[File:HASTAClogohoriz.jpg|thumb|HASTAC logo]]

'''HASTAC''' (Humanities, Arts, Science and Technology Alliance and Collaboratory, pronounced "haystack") is a virtual organization of more than 14,000 individuals and institutions dedicated to innovative new modes of learning and research in higher education, K-12, and lifelong learning.  HASTAC network members contribute to the community by sharing work and ideas with others via the open-access website,<ref>{{cite web|url=http://www.hastac.org |title=hastac.org |publisher=hastac.org |date= |accessdate=2016-05-27}}</ref> by hosting HASTAC conferences and workshops online or in their region, by initiating conversations, or by working collaboratively with others in the HASTAC network.

HASTAC administers the annual $2 million MacArthur Foundation Digital Media and Learning Competition.<ref>{{cite web|author=MacArthur Foundation |url=http://www.macfound.org/programs/learning/ |title=MacArthur Foundation Digital Media and Learning Competition |publisher=MacArthur Foundation |date= |accessdate=2012-12-10}}</ref> The 2011 Competition, “Badges for Lifelong Learning,” launched in collaboration with the [[Mozilla Foundation]], focused on digital badges as a means to inspire learning, confirm accomplishment, or validate the acquisition of knowledge or skills.

Since its inception, HASTAC has been funded by [[Digital Promise]], the [[National Science Foundation]], the [[John D. and Catherine T. MacArthur Foundation]], and [[Duke University]].

== Founding ==
HASTAC was founded in 2002 by [[Cathy N. Davidson]], Ruth F. DeVarney Professor of English, John Hope Franklin Humanities Institute Professor of Interdisciplinary Studies and Co-director of the PhD Lab in Digital Knowledge at Duke University and co-founder of the [[John Hope Franklin Humanities Institute]] at [[Duke University]],<ref>{{cite web|author=Cathy Davidson |url=http://www.cathydavidson.com/about/ |title=Cathy N. Davidson, Author of Now You See It |publisher=Cathy Davidson|date= |accessdate=2012-12-12}}</ref> and [[David Theo Goldberg]], Director of the [[University of California]]'s statewide Humanities Research Institute ([[UCHRI]]).<ref>{{cite web|author=UCHRI |url=http://uchri.org/about/staff/dr-david-theo-goldberg/ |title=David Theo Goldberg, UCHRI |publisher=UCHRI |date= |accessdate=2012-12-12}}</ref>

At a meeting of humanities leaders held by the [[Mellon Foundation]] in 2002, it was clear that Davidson and Goldberg had each, independently, been working on a variety of projects with leading scientists and engineers dedicated to expanding the innovative uses of technology in research, teaching, and electronic publishing.  They resolved to contact others who were building and analyzing the social and ethical dimensions of new technologies and soon formed the HASTAC consortium with a dozen or so other noted educators, scientists, and technology designers.

==Membership==

According to its website, “HASTAC members are motivated by the conviction that the digital era provides rich opportunities for informal and formal learning and for collaborative, networked research that extends across traditional disciplines, across the boundaries of the academy and the community, across the "two cultures" of humanism and technology, across the divide of thinking versus making, and across social strata and national borders."<ref>{{cite web|author=Login Contact |url=http://www.hastac.org/about-hastac |title=About |publisher=HASTAC |date= |accessdate=2012-08-20}}</ref>

HASTAC's members include professors, students, public intellectuals, artists, educators, software or hardware designers, scientists, gamers, programmers, librarians, and social and political organizers. Specializations include the full range of the humanities and social sciences, the arts, journalism, digital humanities, computational fields, sciences, information science, and engineering, plus those interested in intellectual property issues  and public policy on a local or global scale.

==Programs==

=== HASTAC Scholars program ===
In 2008, HASTAC initiated the HASTAC Scholars Program, an annual fellowship program that recognizes graduate and undergraduate students who are engaged in innovative work across the areas of technology, the arts, the humanities, and the social sciences. As of 2012, 522 people at 120 institutions have been named HASTAC Scholars, functioning as links between their home institutions and the virtual community they foster on the HASTAC site. "HASTAC scholars participate in and encourage conversations often by blogging, tweeting, podcasting, or co-hosting forums with HASTAC scholars
from other universities, among other various means of connection".<ref>Singletary, Kim Alecia. "Interdisciplinary intellect HASTAC and the commitment to encourage collective intelligence". Arts and Humanities in Higher Education (2012), vol 11: pp. 109-119. p. 113.</ref>

=== HASTAC/MacArthur Foundation Digital Media and Learning Competition ===
Created in 2007, the HASTAC/MacArthur Foundation Digital Media and Learning Competition  is designed to find and inspire the most innovative uses of new media in support of [[Connected Learning|connected learning]].<ref>{{cite web|author=Digital Media and Learning Research Hub |url=http://connectedlearning.tv |title=Connected Learning}}</ref>  Awards have recognized individuals, for-profit companies, universities, and community organizations using new media to transform learning. Information about Digital Media and Learning Competition winners can be found on HASTAC.

The Digital Media and Learning Competition is funded by the MacArthur Foundation and administered by HASTAC. For the fourth competition, other partners included the Bill & Melinda Gates Foundation and Mozilla.

=== Digital Publication Projects ===
Digital Publication Projects: Michigan Series in Digital Humanities@digitalculturebooks and the UM/HASTAC Digital Humanities Publication Prize
The University of Michigan Press and HASTAC launched The University of Michigan Series in Digital Humanities@digitalculturebooks and the UM/HASTAC Digital Humanities Publication Prize in December 2009.<ref>{{cite web|author=University of Michigan Press |url=http://blog.press.umich.edu/2009/12/the-university-of-michigan-series-in-digital-humanitiesdigitalculturebooks-and-the-umhastac-digital |title=The University of Michigan Series in Digital Humanities@digitalculturebooks and the UM/HASTAC Digital Humanities Publication Prize}}</ref>  Series editors include Julie Thompson Klein and Tara McPherson; advisory board includes Cathy N. Davidson, Daniel Herwitz, and Wendy Chun (Brown). 
<ref>{{cite web|author=University of Michigan |url=http://www.digitalculture.org |title=Announcing digitalhumanities @ digitalculturebooks}}</ref>  Initial 2012 winners were Jentery Sayers and Sheila Brennan.<ref>{{cite web|url=http://hastac.org/blogs/nancyholliman/2012/06/15/jentery-sayers-sheila-brennan-awarded-university-michigan-presshastac  |title=Jentery Sayers and Sheila Brennan awarded University of Michigan Press/HASTAC prize  Press Release, June 15, 2012}}</ref>

==Events==

=== Conferences ===
HASTAC member organizations organize international conferences for the community.

* Eight HASTAC member institutions coordinated '''InFORMATION Year''' in 2006-2007, a year of webcast programming each focusing on a theme for one month (InCommon, InCommunity, Interplay, Interaction, InJustice, Integration, Invitation and Innovation).  All culminated in '''Electronic Techtonics''' an international Interface conference cohosted by Duke University and RENCI (the Renaissance Computing Institute) on April 19–21, 2007. All events were webcast and archived versions are available free on hastac.org for nonprofit educational purposes.<ref>{{cite web|author=HASTAC |url=http://hastac.org/informationyear |title=HASTAC InFORMATION Year Archived Resources |publisher=HASTAC |date= |accessdate=2012-12-12}}</ref> 
* '''HASTAC II: Techno-Travels''' was held in 2008 at University of California Humanities Research Institute (UCHRI); University of California, Irvine; and the University of California, Los Angeles.<ref>{{cite web|author=HASTAC |url=http://hastac.org/hastac08conference |title=HASTAC II Conference Proceedings |publisher=HASTAC |date= |accessdate=2013-04-26}}</ref>
* '''HASTAC III: Traversing Digital Boundaries''' was organized by the Institute for Computing in Humanities, Arts, and Social Science (I-CHASS) at the University of Illinois at Urbana-Champaign.<ref>{{cite web|author=HASTAC 2013 Conference |url=http://www.chass.uiuc.edu/Events/Entries/2009/4/19_HASTAC_III.html |title=HASTAC III |publisher=HASTAC 2009 Conference Organizers |date= |accessdate=2013-04-26}}</ref>
* '''HASTAC 2010: Grand Challenges and Global Innovations''' hosted by I-CHASS was a free, entirely virtual event held in a multiplicity of digital spaces instigated from sites across the globe.<ref>{{cite web|author=HASTAC 2010 Conference |url=http://www.chass.uiuc.edu/hastac2010/HASTAC_2010/Welcome.html |title=HASTAC 2010 |publisher=HASTAC 2010 Conference Organizers |date= |accessdate=2013-04-26}}</ref>
* '''HASTAC 2011: Digital Scholarly Communication''' was held at the University of Michigan at Ann Arbor.<ref>{{cite web|author=HASTAC 2011 Conference |url=http://www.hastac2011.org/ |title=HASTAC 2011 |publisher=HASTAC 2011 Conference Organizers |date= |accessdate=2013-04-26}}</ref> (digital proceedings)<ref>{{cite web|url=http://hastac.org/content/hastac-2011-digital-scholarly-communication |title=Digital Scholarly Communication: Conference Proceedings from HASTAC 2011 |publisher=HASTAC |date= |accessdate=2014-04-17}}</ref>
* '''HASTAC 2013: The Storm of Progress''' was held at York University and in downtown Toronto, Canada on April 25–28, 2013.<ref>{{cite web|author=HASTAC 2013 Conference |url=http://hastac2013.org/ |title=The Storm of Progress |publisher=HASTAC 2013 Conference Organizers |date= |accessdate=2012-12-12}}</ref>

=== Mozilla's Drumbeat Festival: Learning, Freedom and the Open Web ===
HASTAC hosted the "Storming the Academy" tent, which discussed and workshopped open learning and peer-to-peer assessment strategies, ideas, and lessons, at the Mozilla Drumbeat Festival in Barcelona on Nov. 3-5, 2010.

=== THATCampRTP ===
On October 16, 2010, HASTAC hosted and helped to organize THATCamp RTP at Duke University's John Hope Franklin Humanities Institute.<ref>{{cite web|url=http://www.fhi.duke.edu/ |title=fhi.duke.edu |publisher=fhi.duke.edu |date=2014-03-19 |accessdate=2014-04-17}}</ref>  It was the first area  THATCamp<ref>{{cite web|url=http://thatcamp.org/ |title=thatcamp.org |publisher=thatcamp.org |date=2014-03-31 |accessdate=2014-04-17}}</ref>  for the Research Triangle Park area of North Carolina.

===Peer-to-Peer Pedagogy: Workshop on Collaborative Learning Across Disciplines, Ages, and Institutions in Higher Education ===
HASTAC held a public workshop on September 10, 2010 at the John Hope Franklin Center at Duke University. It included an "unpanel," an unconference, and breakout sessions during which participants discussed, explored and modeled the benefits and challenges of peer-to-peer collaborative pedagogies from the specific perspective of young scholars, emphasizing evaluation and grading. Participants included academics, practitioners, and HASTAC Scholars.

==References==
<references/>

{{DEFAULTSORT:Hastac}}
[[Category:Information technology organisations]]
[[Category:Digital humanities]]
[[Category:Humanities organizations]]
[[Category:Computing and society]]