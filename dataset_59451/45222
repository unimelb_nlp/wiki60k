{{Infobox aviator
| name             = Paul F. Bikle
| image            = Paul F. Bikle, cropped.jpg
| image_size       = 
| alt              = 
| caption          = Director Paul F. Bikle, at NASA's Flight Research Center (FRC) in 1959 (from NASA Photo [[:File:Paul F. Bikle.jpg|E-4875]])
| full_name        = 
| native_name      = 
| native_name_lang = 
| birth_name       =
| birth_date       = {{Birth date|1916|06|05|df=y}} 
| birth_place      = [[Wilkinsburg, Pennsylvania]], U.S.
| death_date       = {{Death date and age|1991|01|19|1916|06|05|df=y}}
| death_place      = [[Salinas, California]], U.S.
| death_cause      = 
| resting_place    = 
| resting_place_coordinates = <!-- {{Coord|LATITUDE|LONGITUDE|type:landmark}} -->
| monuments        = 
| nationality      = [[United States]]
| education        = [[University of Detroit]], B.S. 1939
| spouse           = 
| relatives        = 
| known_for        = Inspiring the [[Rogallo wing]] used in many modern hang-gliders
| first_flight_date     = 
| first_flight_aircraft = 
| famous_flights   = He established two world soaring records on 25 February 1961 while flying his [[Schweizer SGS 1-23E]] near [[Lancaster, California]], achieving an altitude of {{convert|46267|ft|abbr=on}} feet and a total-altitude-gained mark of {{convert|42300|ft|abbr=on}}.
| license_date     = 
| license_place    = 
| air_force        = 
| battles          = 
| rank             = 
| awards           = 
| memorials        = 
| first_race       = <!-- For racing aviators -->
| racing_best      = <!-- For racing aviators -->
| racing_aircraft  = <!-- For racing aviators -->
| website          = <!-- {{URL|example.com}} -->
| child            = 
| module           = 
}}

'''Paul F. Bikle''' (5 June 1916 [[Wilkinsburg, Pennsylvania]] – 19 January 1991 [[Salinas, California]]) Director of the U.S. [[National Aeronautics and Space Administration]] (NASA) [[Dryden Flight Research Facility]] from 1959 until 1971, and author of more than 40 technical publications, has been associated with major aeronautical research programs including the [[supersonic]] X-15 rocket plane, and also was a world record setting [[Glider (sailplane)|glider]] pilot.

==Civilian career==

Before graduating from the [[University of Detroit]] in 1939 with a BSc. degree in [[aeronautical engineering]], Bikle's activity in the student chapter of the Institute of the Aeronautical Sciences (IAS), and the school [[gliding]] and [[flying club]]s, led to [[flight instruction]] and a pilot's license from the C.A.A., the predecessor to the [[FAA]]. He later became a fellow in the [[American Institute of Aeronautics and Astronautics]], the successor to the IAS. Mr. Bilke worked for [[Taylorcraft Aircraft]] in [[Ohio]] before beginning his government service career.

==Air Force career==

His career with the [[U.S. Air Force]] began in 1940 when he was appointed an aeronautical engineer at [[Wright Field]]. In 1944 he was named Chief of the Aerodynamics Branch in the [[Flight Test]] Division there. While working closely with other government agencies in establishing the first flying qualities specifications for aircraft, he also wrote AAF Technical Report 50693 ''Flight Test Methods'', which was used as a standard manual for conducting flight tests for more than five years. During the [[World War II]] years he was involved in more than 30 test projects and flew over 1,200 hours as an engineering observer.

In 1947 Bikle was appointed Chief of the Performance Engineering Branch, and directed tests of the [[XB-43 Jetmaster]], the first U.S. jet bomber; the [[Convair XC-99]], and the [[North American F-86A Sabre]]. With the transfer of this part of the flight test mission to the newly formed Air Force Flight Test Center at [[Edwards AFB]], he advanced to Assistant Chief of the Flight Test Engineering Laboratory in 1951.

==NASA career==

Paul Bikle was technical director of the Air Force Flight Test Center at [[Edwards Air Force Base]] in September 1959, when he was named Director of the NASA Flight Research Center (FRC) at Edwards, California. In July 1962 he was awarded the NASA Medal for Outstanding Leadership for his part in directing flight operations and research activities on the highly successful rocket-powered [[X-15]] program. After Paul's retirement on 31 May 1971, The FRC became the [[Dryden Flight Research Center]] in 1976.

During his nearly 12 years with NASA he was responsible for several major aeronautical research programs, including those involving the X-15, the supersonic [[XB-70]], the fleet of wingless [[lifting bodies]] that contributed to development of the Space Shuttles, and the Lunar Landing Research Vehicle that paved the way for successful [[Moon landing]]s by [[Project Apollo]] astronauts.

Just before Christmas 1961 Paul Bikle gave a directive to [[Charles Richard]] to quickly and cheaply design and build what would become the template for the [[Rogallo wing]] used for a high percentage of modern hang gliders. Following successful test flying of the [[Paresev]] (paraglider research vehicle), many prospective hang glider pilots made their own hang gliders with various fuselage solutions; including the use of the triangle control bar, parallel-bar, full-cockpit, tri-cycle undercarriage and powered hang-gliders. Paul Bikle's directive synergistically birthed a wing that would dramatically change personal aviation in powered and un-powered forms.

==Soaring==
Bikle was a veteran of 23 years of [[Lift (soaring)|soaring]], and was president of the [[Soaring Society of America]] (SSA). He established two world soaring records on 25 February 1961 while flying his [[Schweizer SGS 1-23E]]<ref name="Schweizer183">Schweizer, Paul A: ''Wings Like Eagles, The Story of Soaring in the United States'', page 183. Smithsonian Institution Press, 1988. ISBN 0-87474-828-3</ref> near [[Lancaster, California]], achieving an altitude of {{convert|46267|ft|abbr=on}} feet and a total-altitude-gained mark of {{convert|42300|ft|abbr=on}}.<ref name="Schweizer183"/> Both marks were certified by the [[National Aeronautic Association]], and the [[Fédération Aéronautique Internationale]]. Bikle still holds the record for height gain.<ref>[http://www.fai.org/record-gliding FAI web site on the gain in height record]</ref> He flew a [[Prue Standard]] to a world record distance of {{convert|557|mi|km|0|abbr=on}} in 1963, the longest ever made by a sailplane up to that date.<ref name="SoaringAug74">Rogers, Bennett: ''1974 Sailplane Directory, Soaring Magazine'', page 34. Soaring Society of America, August 1974. USPS 499-920</ref> He became a member of the [[Soaring Hall of Fame]] in 1960 and was awarded the FAI [[Lilienthal Gliding Medal]], the highest award in international soaring, in 1962.<ref>{{cite web |url=http://www.fai.org/awards/award.asp?id=10 |title=The Lilienthal Gliding Medal}}</ref>

While director of the Dryden Flight Research Facility, Bikle designed and completed his own sailplane, the [[Bikle T-6]], flying it to fifth place in the 1970 US Nationals.<ref name="SD">{{Cite web|url = http://www.sailplanedirectory.com/PlaneDetails.cfm?PlaneID=162|title = HP-13 and HP-14 HP Aircraft, LLC |accessdate = 27 April 2011|last = Activate Media|authorlink = |year = 2006}}</ref><ref name="SoaringNov83">Said, Bob: ''1983 Sailplane Directory, [[Soaring Magazine]]'', page 37. [[Soaring Society of America]] November 1983. USPS 499-920</ref>

===Other soaring accomplishments===
* [[Barringer Trophy]] 1952, 1953, 1954, 1955, 1956
* U.S. Diamond badge #3 (International #7) 1953;
* Symons Three Lennie award #11
* Eaton Trophy 1964
* Tissandier Diploma 1968
* Tuntland Award 1970, 1971
* [[OSTIV]] Plaque/Klemperer Award 1972
* World/National Competition
* Smirnoff Derby
* Sailplane Performance Studies/Tests

==References==
{{reflist|2}}
* [http://history.nasa.gov/x15/bikle.html NASA X-15 Biography]
* [http://www.dfrc.nasa.gov/Gallery/Photo/Directors/HTML/E-4875.html NASA Dryden Home > Collections > Photo Home > Directors > Photo # E-4875]
* [http://www.soaringmuseum.org/halloffame/halloffamebios.html Soaring Hall of Fame Biographies]

{{Recipients of Lilienthal Gliding Medal}}

{{DEFAULTSORT:Bikle, Paul}}
[[Category:1916 births]]
[[Category:1991 deaths]]
[[Category:American aviators]]
[[Category:Aviators from Pennsylvania]]
[[Category:American aviation writers]]
[[Category:Aviation pioneers]]
[[Category:Gliding in the United States]]
[[Category:Glider pilots]]
[[Category:Former world record holders in gliding]]
[[Category:People from Allegheny County, Pennsylvania]]
[[Category:University of Detroit Mercy alumni]]
[[Category:Lilienthal Gliding Medal recipients]]
[[Category:20th-century American writers]]