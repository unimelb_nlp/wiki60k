{{Use dmy dates|date=May 2014}}
{{Use British English|date=May 2014}}
{{good article}}
{{Infobox Horse race
|class           = 2011 Grand National
|horse race      = [[Grand National]]
|image           = [[File:Owner Mr Trevor Hemmings.svg|100px]]
|caption         = 
|location        = [[Aintree Racecourse]]
|date            = 9 April 2011
|winning horse   = [[Ballabriggs]] 
|starting price  = 14/1
|winning jockey  = [[Jason Maguire]]
|winning trainer = Donald McCain Jr.
|winning owner   = [[Trevor Hemmings]]
|conditions      = Good (good to soft in places)<ref name=guardian>{{cite news | title = Two horses die as gruelling Grand National takes its toll at Aintree | publisher = The Guardian | url = https://www.theguardian.com/sport/2011/apr/09/grand-national-2011-two-deaths | accessdate = 9 April 2011 | location=London | first=Greg | last=Wood | date=9 April 2011}}</ref>
|previous        = [[2010 Grand National|2010]]
|next            = [[2012 Grand National|2012]]
}}
{{ external media
| align  = right
| width  = 275px
| video1 = [http://news.bbc.co.uk/sport1/hi/horse_racing/13025566.stm Replay of the 2011 Grand National in full]<ref>{{cite news | title = Full replay of the race | publisher = BBC Sport | url = http://news.bbc.co.uk/sport1/hi/horse_racing/13025566.stm | accessdate = 9 April 2011 | date=9 April 2011}}</ref> BBC Sport(UK & Ireland only)
}}

The '''2011 Grand National''' (known as the [[John Smith's Brewery|John Smith's]] Grand National for sponsorship reasons) was the 164th renewal of the world-famous [[Grand National]] [[Horse racing|horse race]] held at [[Aintree Racecourse]] near [[Liverpool]], [[England]].

The showpiece [[steeplechase]] began at 4:15&nbsp;pm [[British Summer Time|BST]] on 9 April 2011, the final day of the three-day annual meeting. The maximum permitted field of forty runners competed for prize money totalling a record [[Pound sterling|£]]950,000, making it the highest valued [[National Hunt racing|National Hunt race]] in the United Kingdom.<ref>{{cite news | title = Order of running | publisher = Aintree Racecourse | url = http://www.aintree.co.uk/pages/order-of-running/ | accessdate = 9 April 2011}}</ref>

Nineteen of the forty participants completed the 4½-mile course; of the 21 who did not, two suffered fatal falls on the first circuit, reigniting debates over the safety of the event. Irish horse [[Ballabriggs]] won the race, securing the first-place prize money of £535,135 and a first Grand National win for [[Horse trainer|trainer]] [[Donald McCain, Jr.]], the son of four-time winning trainer [[Ginger McCain]]. Owned by [[Trevor Hemmings]], Ballabriggs was ridden by Irish [[jockey]] [[Jason Maguire]] and was sent off at odds of 14/1.<ref name=bbc1>{{cite news | title = Ballabriggs powers to National win | publisher = BBC Sport | url = http://news.bbc.co.uk/sport1/hi/other_sports/horse_racing/9451559.stm | accessdate = 9 April 2011 | first=Frank | last=Keogh | date=9 April 2011}}</ref> The pairing completed the race in 9 minutes 1.2 seconds, the second-fastest time in Grand National history.<ref name=telegraph>{{cite news | title = Emotions run high at Aintree, but thrilling race is marred by death of two horses | publisher = The Telegraph | url = http://www.telegraph.co.uk/sport/horseracing/8440759/Grand-National-2011-emotions-run-high-at-Aintree-but-thrilling-race-is-marred-by-death-of-two-horses.html | accessdate = 9 April 2011 | location=London | first=Jonathan | last=Liew | date=9 April 2011}}</ref>

== Race card ==
On 2 February 2011 Aintree released the names of 102 horses submitted to enter, including 34 [[Republic of Ireland|Irish]]-trained and three [[France|French]]-trained horses. Ten were trained by [[Paul Nicholls (horse racing)|Paul Nicholls]], including a leading contender in Niche Market; nine were handled by Irish trainer [[Willie Mullins]], and three by [[Jonjo O'Neill]], the trainer of last year's winner. Ballabriggs, another leading contender, was trained by [[Donald McCain, Jr.]], the son of [[Ginger McCain]] who trained [[Red Rum]] to three National victories in the 1970s and returned with another winner, [[Amberleigh House]], in [[2004 Grand National|2004]].<ref>{{cite news | title = February list of 102 entries | publisher = Aintree Racecourse | url = http://www.aintree.co.uk/news/102-entries-for-the-2011-john-smiths-grand-national/ | accessdate = 2 February 2011}}</ref>

20 contenders were withdrawn in the first scratchings. After a second scratchings deadline on 24 March, 74 horses remained on the list of entrants, with the top weight handicap of 11 [[Stone (Imperial mass)|st]] 10 [[Pound (mass)|lb]] allocated to last year's winner [[Don't Push It]]. The official odds on 24 March placed Mullins-trained The Midnight Club at 10/1 favourite. Backstage and Oscar Time were given joint-second favourite odds of 12/1.<ref name=still-in-it>{{cite news | title = 74 still on target for Grand National | publisher = Aintree Racecourse | url = http://www.aintree.co.uk/news/74-still-on-target-for-grand-national/ | accessdate = 24 March 2011}}</ref>

At the five-day deadline on 4 April, nine further withdrawals since the second scratchings left a total of 65 contenders still in the running to compete. Nina Carberry, the sister of [[1999 Grand National|1999]] winner [[Paul Carberry]], became the first female jockey to take a third ride in the Grand National. Four amateur jockeys lined up to compete. Official odds on favourite The Midnight Club were cut to 8/1, while What A Friend replaced Oscar Time as a joint-second favourite with Backstage on odds of 11/1.<ref name=fivedays>{{cite news | title = 65 aiming for Grand National glory | publisher = Aintree Racecourse | url = http://www.aintree.co.uk/news/65_aiming_for_john_smiths_grand_national_glory/ | accessdate = 4 April 2011}}</ref>

On 7 April, Aintree declared the final confirmed list of 40 runners and four reserves as follows. The reserves would replace any withdrawals prior to 9 am on 8 April.<ref>{{cite news | title = Field declared for 2011 Aintree race | publisher = BBC Sport | url = http://news.bbc.co.uk/sport1/hi/other_sports/horse_racing/9445742.stm#entries | accessdate = 7 April 2011 | date=7 April 2011}}</ref> None of the reserves, however, were required.

{| class="wikitable sortable"
|-
!'''Number'''
!'''Horse'''
!'''Age'''
!'''Weight ([[Stone (Imperial mass)|st]]-[[Pound (mass)|lb]])'''
!'''[[Starting price|SP]]'''
!'''[[Jockey]]'''
!'''[[Horse trainer|Trainer]]'''
!'''Owner'''
|-
|1
|'''[[Don't Push It]]''' <small>([[Republic of Ireland|IRE]])</small>
|11
|11-10
|{{Odds|9|1}}
|[[Tony McCoy]]
|[[Jonjo O'Neill]]
|[[J. P. McManus]]
|-
|2
|'''[[Tidal Bay]]''' <small>([[Republic of Ireland|IRE]])</small>
|10
|11-09
|{{Odds|28|1}}
|Brian Hughes
|Howard Johnson
|Mr. & Mrs. [[Graham Wylie]]
|-
|3
|'''What A Friend'''
|8
|11-06
|{{Odds|12|1}}
|[[Daryl Jacob]]
|[[Paul Nicholls (horse racing)|Paul Nicholls]]
|Ged Mason & [[Alex Ferguson|Sir Alex Ferguson]]
|-
|4
|'''Vic Venturi''' <small>([[Republic of Ireland|IRE]])</small>
|11
|11-06
|{{Odds|50|1}}
|[[Andrew Lynch (jockey)|Andrew Lynch]]
|[[Dessie Hughes]] <small>([[Republic of Ireland|IRE]])</small>
|Seamus Dunne
|-
|5
|'''Majestic Concorde''' <small>([[Republic of Ireland|IRE]])</small>
|8
|11-05
|{{Odds|20|1}}
|Mr. Robbie McNamara
|[[Dermot Weld]] <small>([[Republic of Ireland|IRE]])</small>
|Dr. Ronan Lambe
|-
|6
|'''Or Noir de Somoza''' <small>([[France|FRA]])</small>
|9
|11-05
|{{Odds|50|1}}
|[[Barry Geraghty]]*
|[[David Pipe (racehorse trainer)|David Pipe]]
|Filsal Stadeg Racing
|-
|7
|'''Dooneys Gate''' <small>([[Republic of Ireland|IRE]])</small>
|10
|11-04
|{{Odds|50|1}}
|Mr. Patrick Mullins
|[[Willie Mullins]] <small>([[Republic of Ireland|IRE]])</small>
|Mrs. Jackie Mullins
|-
|8
|'''Big Fella Thanks'''
|9
|11-01
|{{Odds|12|1}}
|[[Graham Lee (jockey)|Graham Lee]]
|Ferdy Murphy
|Crossed Fingers Partnership
|-
|9
|'''The Tother One''' <small>([[Republic of Ireland|IRE]])</small>
|10
|11-00
|{{Odds|50|1}}
|Mr. Ryan Mahon
|[[Paul Nicholls (horse racing)|Paul Nicholls]]
|Graham Roach
|-
|10
|'''[[Ballabriggs]]''' <small>([[Republic of Ireland|IRE]])</small>
|10
|11-00
|{{Odds|14|1}}
|[[Jason Maguire]]
|[[Donald McCain, Jr.]]
|[[Trevor Hemmings]]
|-
|11
|'''The Midnight Club''' <small>([[Republic of Ireland|IRE]])</small>
|10
|10-13
|{{Odds|15|2}} F
|[[Ruby Walsh]]
|[[Willie Mullins]] <small>([[Republic of Ireland|IRE]])</small>
|Mrs. Susannah Ricci
|-
|12
|'''Niche Market''' <small>([[Republic of Ireland|IRE]])</small>
|10
|10-13
|{{Odds|16|1}}
|Harry Skelton
|[[Paul Nicholls (horse racing)|Paul Nicholls]]
|Graham Regan
|-
|13
|'''Silver by Nature''' ([[Gray (horse)|grey]])
|9
|10-12
|{{Odds|9|1}}
|Peter Buchanan
|Ms. Lucinda Russell
|[[Geoff Brown (businessman)|Geoff Brown]]
|-
|14
|'''Backstage''' <small>([[France|FRA]])</small>
|9
|10-12
|{{Odds|16|1}}
|[[Paul Carberry]]
|[[Gordon Elliott (racehorse trainer)|Gordon Elliott]] <small>([[Republic of Ireland|IRE]])</small>
|MPR & Capranny Syndicate
|-
|15
|'''Chief Dan George''' <small>([[Republic of Ireland|IRE]])</small>
|11
|10-12
|{{Odds|40|1}}
|Paddy Aspell
|Jimmy Moffatt
|Maurice Chapman
|-
|16
|'''Calgary Bay''' <small>([[Republic of Ireland|IRE]])</small>
|8
|10-10
|{{Odds|33|1}}
|Hadden Frost
|Ms. [[Henrietta Knight (racehorse trainer)|Henrietta Knight]]
|Mrs. Camilla Radford
|-
|17
|'''Killyglen''' <small>([[Republic of Ireland|IRE]])</small>
|9
|10-10
|{{Odds|66|1}}
|[[Robbie Power]]
|Stuart Crawford <small>([[Republic of Ireland|IRE]])</small>
|David McCammon
|-
|18
|'''Oscar Time''' <small>([[Republic of Ireland|IRE]])</small>
|10
|10-09
|{{Odds|14|1}}
|[[Sam Waley-Cohen|Mr. Sam Waley-Cohen]]
|Martin Lynch <small>([[Republic of Ireland|IRE]])</small>
|Robert Waley-Cohen & S. & [[Martin Broughton|M. Broughton]]
|-
|19
|'''Quinz''' <small>([[France|FRA]])</small>
|7
|10-08
|{{Odds|14|1}}
|[[Richard Johnson (jockey)|Richard Johnson]]
|[[Philip Hobbs]]
|Andrew Cohen
|-
|20
|'''Becauseicouldntsee''' <small>([[Republic of Ireland|IRE]])</small>
|8
|10-08
|{{Odds|16|1}}
|[[Davy Russell]]
|Noel Glynn <small>([[Republic of Ireland|IRE]])</small>
|Noel Glynn
|-
|21
|'''[[Comply or Die]]''' <small>([[Republic of Ireland|IRE]])</small>
|12
|10-08
|{{Odds|25|1}}
|[[Timmy Murphy]]
|[[David Pipe (racehorse trainer)|David Pipe]]
|[[David Johnson (racehorse owner)|David Johnson]]
|-
|22
|'''Quolibet''' <small>([[France|FRA]])</small>
|7
|10-08
|{{Odds|100|1}}
|[[Mark Walsh (jockey)|Mark Walsh]]
|[[Jonjo O'Neill]]
|[[J. P. McManus]]
|-
|23
|'''Grand Slam Hero''' <small>([[Republic of Ireland|IRE]])</small>
|10
|10-07
|{{Odds|66|1}}
|[[Aidan Coleman]]
|[[Nigel Twiston-Davies]]
|Walters Plant Hire Ltd.
|-
|24
|'''State of Play'''
|11
|10-06
|{{Odds|28|1}}
|Paul Moloney
|Evan Williams
|Mr. & Mrs. William Rucker
|-
|25
|'''King Fontaine''' <small>([[Republic of Ireland|IRE]])</small>
|8
|10-06
|{{Odds|80|1}}
|Denis O'Regan
|Malcolm Jefferson
|[[Trevor Hemmings]]
|-
|26
|'''In Compliance''' <small>([[Republic of Ireland|IRE]])</small>
|11
|10-05
|{{Odds|66|1}}
|[[Leighton Aspell]]
|[[Dessie Hughes]] <small>([[Republic of Ireland|IRE]])</small>
|Westerly Breeze Syndicate
|-
|27
|'''Hello Bud''' <small>([[Republic of Ireland|IRE]])</small>
|13
|10-05
|{{Odds|20|1}}
|[[Sam Twiston-Davies]]
|[[Nigel Twiston-Davies]]
|Seamus Murphy
|-
|28
|'''West End Rocker''' <small>([[Republic of Ireland|IRE]])</small>
|9
|10-05
|{{Odds|33|1}}
|[[Robert Thornton (jockey)|Robert Thornton]]
|[[Alan King (horse racing)|Alan King]]
|Barry Winfield & Tim Leadbeater
|-
|29
|'''Santa's Son''' <small>([[Republic of Ireland|IRE]])</small>
|11
|10-05
|{{Odds|100|1}}
|[[Jamie Moore (jockey)|Jamie Moore]]
|Howard Johnson
|Douglas Pryde & Jim Beaumont
|-
|30
|'''Bluesea Cracker''' ([[mare]]) <small>([[Republic of Ireland|IRE]])</small>
|9
|10-04
|{{Odds|25|1}}
|[[Andrew McNamara (jockey)|Andrew McNamara]]
|James Motherway <small>([[Republic of Ireland|IRE]])</small>
|[[J. P. McManus]]
|-
|31
|'''That's Rhythm''' <small>([[France|FRA]])</small>
|11
|10-04
|{{Odds|50|1}}
|James Reveley
|Martin Todhunter
|Don't Tell Henry
|-
|32
|'''Surface to Air'''
|10
|10-04
|{{Odds|100|1}}
|Tom Messenger
|Chris Bealby
|Tim Urry
|-
|33
|'''Piraya''' <small>([[France|FRA]])</small>
|8
|10-04
|{{Odds|100|1}}
|Johnny Farrelly
|[[David Pipe (racehorse trainer)|David Pipe]]
|Terry Neill
|-
|34
|'''Can't Buy Time''' <small>([[Republic of Ireland|IRE]])</small>
|9
|10-04
|{{Odds|33|1}}
|Richie McLernon
|[[Jonjo O'Neill]]
|[[J. P. McManus]]
|-
|35
|'''Character Building''' <small>([[Republic of Ireland|IRE]])</small>
|11
|10-04
|{{Odds|25|1}}
|[[Nina Carberry|Ms. Nina Carberry]]
|John Quinn
|Patricia Thompson
|-
|36
|'''Ornais''' <small>([[France|FRA]])</small>
|9
|10-04
|{{Odds|100|1}}
|Nick Scholfield
|[[Paul Nicholls (horse racing)|Paul Nicholls]]
|The Stewart family
|-
|37
|'''Arbor Supreme''' <small>([[Republic of Ireland|IRE]])</small>
|9
|10-03
|{{Odds|20|1}}
|David Casey
|[[Willie Mullins]] <small>([[Republic of Ireland|IRE]])</small>
|[[J. P. McManus]]
|-
|38
|'''Royal Rosa''' <small>([[France|FRA]])</small>
|12
|10-03
|{{Odds|100|1}}
|Paul Gallagher
|Howard Johnson
|Mr. & Mrs. Graham Wylie
|-
|39
|'''Skippers Brig''' <small>([[Republic of Ireland|IRE]])</small>
|10
|10-02
|{{Odds|33|1}}
|Dominic Elsworth
|[[Nicky Richards]]
|Ashelybank Investments Ltd.
|-
|40
|'''Golden Kite''' <small>([[Republic of Ireland|IRE]])</small>
|9
|10-02
|{{Odds|66|1}}
|Shane Hassett
|[[Adrian Maguire]] <small>([[Republic of Ireland|IRE]])</small>
|Dr. Anthony Calnan
|-
|R1
|Always Waining <small>([[Republic of Ireland|IRE]])</small>
|10
|10-02
|
|
|Peter Bowen
|Mr. & Mrs. Peter Douglas
|-
|R2
|Faasel <small>([[Republic of Ireland|IRE]])</small>
|10
|10-01
|
|
|[[David Pipe (racehorse trainer)|David Pipe]]
|Jim Ennis
|-
|R3
|Le Beau Bai <small>([[France|FRA]])</small>
|8
|10-01
|
|
|Richard Lee
|Glass Half Full Syndicate
|-
|R4
|Giles Cross <small>([[Republic of Ireland|IRE]])</small>
|9
|10-00
|
|
|Victor Dartnall
|KCMS Partnership
|}
<small>*[[Barry Geraghty]] rode Or Noir de Somoza after his original jockey, [[Tom Scudamore]], withdrew due to an injury sustained in a race the day prior to the National.</small>

==Finishing order==
Ten-year-old [[Ballabriggs]] led for much of the race, and the gelding eventually saw off a strong run-in challenge from amateur jockey [[Sam Waley-Cohen]] on Oscar Time who secured second place. Third was [[2010 Grand National|2010]] winner [[Tony McCoy]] on [[Don't Push It]], twelve lengths behind the second. 15/2 favourite The Midnight Club made a mistake at the third fence and finished sixth.<ref name=bbc1/>

State of Play, the eleven-year-old 28/1 shot trained by Welshman Evan Williams, finished in the top four for the third successive National.<ref>{{cite news | title = Evan Williams praise for State of Play | publisher = BBC Sport | url = http://news.bbc.co.uk/sport1/hi/horse_racing/13026660.stm | accessdate = 9 April 2011 | date=9 April 2011}}</ref>

Nineteen runners completed the course, including three of the six 100/1 shots. This was the highest number of finishers since [[2005 Grand National|2005]] when twenty-one horses passed the finishing post.

{| class="wikitable sortable"
|-
!'''Position'''
!'''Horse'''
!'''[[Jockey]]'''
!'''[[Starting price|SP]]'''
!'''Distance'''
!'''Prize money'''
|-
|'''1'''
|[[Ballabriggs]]
|[[Jason Maguire]]
|{{Odds|14|1}}
|Won by 2¼ [[Horse length|length]]s
|align=right|£535,135
|-
|'''2'''
|Oscar Time
|[[Sam Waley-Cohen|Mr. Sam Waley-Cohen]]
|{{Odds|14|1}}
|12 lengths
|align=right|£201,590
|-
|'''3'''
|[[Don't Push It]]
|[[Tony McCoy]]
|{{Odds|9|1}}
|2 lengths
|align=right|£100,890
|-
|'''4'''
|State of Play
|Paul Moloney
|{{Odds|28|1}}
|7 lengths
|align=right|£50,445
|-
|'''5'''
|Niche Market
|Harry Skelton
|{{Odds|16|1}}
|4 lengths
|align=right|£25,270
|-
|'''6'''
|The Midnight Club
|[[Ruby Walsh]]
|{{Odds|15|2}} F
|13 lengths
|align=right|£12,635
|-
|'''7'''
|Big Fella Thanks
|[[Graham Lee (jockey)|Graham Lee]]
|{{Odds|12|1}}
|A head
|align=right|£6,270
|-
|'''8'''
|Surface to Air
|Tom Messenger
|{{Odds|100|1}}
|19 lengths
|align=right|£3,230
|-
|'''9'''
|Skippers Brig
|Dominic Elsworth
|{{Odds|33|1}}
|8 lengths
|-
|'''10'''
|Backstage
|[[Paul Carberry]]
|{{Odds|16|1}}
|½ length
|-
|'''11'''
|King Fontaine
|Denis O'Regan
|{{Odds|80|1}}
|25 lengths
|-
|'''12'''
|Silver by Nature
|Peter Buchanan
|{{Odds|9|1}}
|5 lengths
|-
|'''13'''
|In Compliance
|Leighton Aspell
|{{Odds|66|1}}
|8 lengths
|-
|'''14'''
|Bluesea Cracker
|Andrew McNamara
|{{Odds|25|1}}
|16 lengths
|-
|'''15'''
|Character Building
|Ms. Nina Carberry
|{{Odds|25|1}}
|12 lengths
|-
|'''16'''
|Golden Kite
|Shane Hassett
|{{Odds|66|1}}
|A distance
|-
|'''17'''
|Chief Dan George
|Paddy Aspell
|{{Odds|40|1}}
|20 lengths
|-
|'''18'''
|Royal Rosa
|Paul Gallagher
|{{Odds|100|1}}
|A distance
|-
|'''19'''
|Piraya
|Johnny Farrelly
|{{Odds|100|1}}
|Last to complete
|}
<ref name=order>{{cite news | title = Finishing order & jockey comments  | publisher = BBC Sport | url = http://news.bbc.co.uk/sport1/hi/other_sports/horse_racing/9452474.stm | accessdate = 9 April 2011 | date=9 April 2011}}</ref><ref name=prizemoney>{{cite news | title = Aintree's full Grand National brochure publication | publisher = Aintree Racecourse | url = http://www.aintree.co.uk/docLib/2009_pages010109.pdf | accessdate = 9 April 2011}}</ref>

==Non-finishers==
[[File:Aintreenationalcropped.jpg|thumb|right|Overview of the 4½-mile ''National Course'' at Aintree with thirty fences.]]
Ten horses fell during the race, four unseated their riders, two were brought down by other fallers and five were pulled up.<ref name=guardian/>

The 100/1 outsider Santa's Son had led the field over the [[Canal Turn]], but the short-distance runner eventually fell out of contention and jockey [[Jamie Moore (jockey)|Jamie Moore]] pulled him up before the 27th fence. Killyglen fell at the 27th, having been close to leader Ballabriggs at the beginning of the second circuit.<ref name=bbclive>{{cite news | title = The race as it happened | publisher = BBC Sport | url = 
http://news.bbc.co.uk/sport1/hi/other_sports/horse_racing/9450439.stm | accessdate = 9 April 2011 | first=Oliver | last=Brett | date=9 April 2011}}</ref>

Ornais and Dooneys Gate both suffered fatal falls on the first circuit. Ornais incurred a [[cervical fracture]] at the fourth fence (a plain 4&nbsp;ft 10 inch obstacle) and Dooneys Gate fractured his [[Vertebral column|thoracolumbar]] at fence six (the 5&nbsp;ft [[Becher's Brook]]). Aintree had made significant modifications to its ''National Course'' in recent years, including improving veterinary facilities and reducing the severity of some fences, but another notable change was highlighted in this race — that the course has been widened to allow more fences to be bypassed if necessary. As the remaining contenders on the second circuit approached the 20th fence, arrowed signposts and marshals waving chequered flags signalled them to bypass on the outside as Ornais' body was covered by a tarpaulin on the landing side. Two jumps later and they were again diverted, this time around the famous Becher's Brook, where veterinary staff attended to Dooneys Gate.<ref name=guardian/> This was the first time since the modern course was finalised in the 1880s that only 28 fences were jumped.<ref name=bbc1/>

{| class="wikitable sortable"
|-
!'''Fence'''
!'''Horse'''
!'''[[Jockey]]'''
!'''[[Starting price|SP]]'''
!'''Fate'''
|-
|'''1'''
|That's Rhythm
|James Reveley
|{{Odds|50|1}}
|Fell
|-
|'''2'''
|Becauseicouldntsee
|[[Davy Russell]]
|{{Odds|16|1}}
|Fell
|-
|'''2'''
|Vic Venturi
|Andrew Lynch
|{{Odds|50|1}}
|Brought down
|-
|'''4'''
|Calgary Bay
|Hadden Frost
|{{Odds|33|1}}
|Fell
|-
|'''4'''
|Ornais
|Nick Scholfield
|{{Odds|100|1}}
|Fell
|-
|'''6''' <small>([[Becher's Brook]])</small>
|Or Noir de Somoza
|[[Barry Geraghty]]
|{{Odds|50|1}}
|Fell
|-
|'''6''' <small>([[Becher's Brook]])</small>
|Dooneys Gate
|Mr. Patrick Mullins
|{{Odds|50|1}}
|Fell
|-
|'''6''' <small>([[Becher's Brook]])</small>
|The Tother One
|Mr. Ryan Mahon
|{{Odds|50|1}}
|Fell
|-
|'''6''' <small>([[Becher's Brook]])</small>
|West End Rocker
|[[Robert Thornton (jockey)|Robert Thornton]]
|{{Odds|33|1}}
|Brought down
|-
|'''10'''
|Tidal Bay
|Brian Hughes
|{{Odds|28|1}}
|Unseated rider
|-
|'''11'''
|Quoilbet
|Mark Walsh
|{{Odds|100|1}}
|Unseated rider
|-
|'''13'''
|Grand Slam Hero
|[[Aidan Coleman]]
|{{Odds|66|1}}
|Fell
|-
|'''15''' <small>([[The Chair (Aintree Racecourse)|The Chair]])</small>
|Quinz
|[[Richard Johnson (jockey)|Richard Johnson]]
|{{Odds|14|1}}
|Pulled up
|-
|'''18'''
|Can't Buy Time
|Richie McLernon
|{{Odds|33|1}}
|Fell
|-
|'''24''' <small>([[Canal Turn]])</small>
|Majestic Concorde
|Mr. Robbie McNamara
|{{Odds|20|1}}
|Unseated rider
|-
|'''27'''
|What A Friend
|Daryl Jacob
|{{Odds|12|1}}
|Pulled up
|-
|'''27'''
|Santa's Son
|[[Jamie Moore (jockey)|Jamie Moore]]
|{{Odds|100|1}}
|Pulled up
|-
|'''27'''
|Killyglen
|Robert Power
|{{Odds|66|1}}
|Fell
|-
|'''28'''
|[[Comply or Die]]
|[[Timmy Murphy]]
|{{Odds|25|1}}
|Pulled up
|-
|'''28'''
|Arbor Supreme
|David Casey
|{{Odds|20|1}}
|Fell
|-
|'''29'''
|Hello Bud
|[[Sam Twiston-Davies]]
|{{Odds|20|1}}
|Pulled up
|}
<ref name=order/>

==Broadcasting==
The Grand National has the status of being an event of significant national interest within the United Kingdom and thus is listed on the [[Ofcom Code on Sports and Other Listed and Designated Events]]. The race therefore must be covered live on free-to-air [[terrestrial television]] in the UK. The [[BBC]] aired the race on radio for the 80th consecutive year and on television for the 52nd year.

{{Quote box
 |quote  = '''As they race towards the elbow, Ballabriggs is being tackled now by Oscar Time... but Jason Maguire is getting another tune out of Ballabriggs! Ballabriggs with a hundred yards left to go, he's three lengths in front, he's going to outstay them I reckon. He's drifting towards the outside, but he's done enough to win. Ballabriggs and Donald McCain, it's the McCain family back with another National. Ballabriggs, Jason Maguire the winner!'''
 |source = BBC commentator [[Jim McGrath (Australian commentator)|Jim McGrath]] describes the climax of the race<ref>{{cite news | title = Watch the closing stages of the 2011 Grand National (UK only) | publisher = BBC Sport | url = http://news.bbc.co.uk/sport1/hi/horse_racing/13025564.stm | accessdate = 9 April 2011 | date=9 April 2011}}</ref>
 |width  = 35%
 |quoted = 1
 |align  = right
}}

[[Clare Balding]] presented the BBC's television coverage, supported by Rishi Persad and retired jockey [[Richard Pitman]], which was broadcast on [[BBC One]] and, for the second year, [[BBC HD]]. Former National-winning jockeys [[Richard Dunwoody]] and [[Mick Fitzgerald]] provided expert analysis, while betting news was provided by Gary Wiltshire and [[John Parrott]]. [[Suzi Perry]] was due to be providing soundbites from spectators but did not take part in the programme. Her place was taken by last-minute replacement [[Dan Walker (sports journalist)|Dan Walker]] who had been at Aintree to present ''[[Football Focus]]'', aired prior to the National.

The race commentary team was led by [[Jim McGrath (Australian commentator)|Jim McGrath]], who called the winner home for the 14th consecutive year; he was supported by [[Ian Bartlett]] and [[Darren Owen]].<ref name=broadcast>{{cite news | title = BBC racing coverage | publisher = BBC Sport | url = http://news.bbc.co.uk/sport1/hi/other_sports/horse_racing/4552838.stm | accessdate = 9 April 2011 | date=9 April 2011}}</ref>

As well as being streamed to UK viewers on [[BBC Online]], [[BBC Radio 5 Live]] also aired the race, presented by [[Mark Chapman (broadcaster)|Mark Chapman]].<ref name=broadcast/>

The BBC later received some criticism for failing to mention the deaths of Ornais and Dooneys Gate until the end of its broadcast.<ref name=telegraph/> A spokesman later said: "During the race and the re-run [the fatalities were] covered with as much sensitivity as possible."<ref>{{cite news | title = Aintree day of horror as TV audience of millions see two horses die at the National | publisher = Daily Mail | url = http://www.dailymail.co.uk/sport/racing/article-1375310/Aintree-day-horror-TV-audience-millions-horses-die-National.html?ito=feeds-newsxml | accessdate = 10 April 2011 | location=London | first=Stephen | last=Davies | date=10 April 2011}}</ref>

==Quotes==
Selected quotes of participating jockeys speaking after the race:<ref name=order/>

{{Quote box
 |quote  = '''He missed the first by about five foot!'''
 |source = James Reveley, who rode That's Rhythm and fell at the first fence
 |width  = 35%
 |quoted = 1
 |align  = right
}}
*'''[[Jason Maguire]]''' ([[Ballabriggs]], 1st): "He loved it... I got him to the front to get him relaxed and put breathers into him and that helped him get the trip."
*'''[[Sam Waley-Cohen]]''' (Oscar Time, 2nd): "The plan was always to come with one smooth run. We knew he had a huge jump in him so I was just trying to get him to pop a little bit and save and not get too keen. He did everything I asked of him."
*'''[[Tony McCoy]]''' ([[Don't Push It]], 3rd): "He ran a great race and all credit to the horse. He was off the bridle a little bit early and he got a little bit low at a couple of fences late on which was energy-sapping."
*'''Paul Moloney''' (State of Play, 4th): "He's an incredible horse - he's just a little bit slower this year because time is catching up with him. He couldn't lay up with the pace and the ground was probably a little slower than ideal, but he's a fabulous horse."
*'''Harry Skelton''' (Niche Market, 5th): "He ran an absolute blinder and has given me a tremendous ride."
*'''Denis O'Regan''' (King Fontaine, 11th): "My horse ran a blinder. I was a bit slow earlier and nearly got brought down. He jumped like a buck. He's only a novice and will be an ideal spin next year."
*'''Andrew McNamara''' (Bluesea Cracker, 14th): "The ground was a bit quick for her."
*'''Nina Carberry''' (Character Building, 15th): "I was nearly brought down at [[Becher's Brook|Becher's]] and that was his race over."
*'''Hadden Frost''' (Calgary Bay, fell at the 4th fence): "He jumped the fence fine, but landed a bit steep, which any horse could do. We were just behind Ballabriggs at the time so we were in the right place. Maybe we'll come back and try again."
*'''Robbie McNamara''' (Majestic Concorde, unseated rider at 24th): "We were too keen early. I was just starting to get him switched off when we came into the [[Canal Turn]] and got in too close and tipped up."
*'''[[Timmy Murphy]]''' ([[Comply or Die]], pulled up before 28th): "I would say that's his last race. He got very tired and he's not getting any younger, but he's a special horse to me and it's nice to think he's ended up at the course where we achieved our finest hour."

==Aftermath==
The unusually warm and sunny weather conditions were credited with helping the 2011 meeting set a Grand National attendance record. A crowd of 70,291 people attended the main Saturday race day, and a total of 153,583 attended over the course of the three-day meeting, beating the previous record of 151,660 set in [[2005 Grand National|2005]].<ref>{{cite news | title = Aintree 2011 set new records for Liverpool course | publisher = Click Liverpool | url = http://www.clickliverpool.com/sport/other-sport/1213039-aintree-2011-set-new-records-for-liverpool-course.html | accessdate = 14 April 2011}}</ref>

The Grand National is always a major event for bookmakers, particularly in the United Kingdom. An estimated £300 million in bets were said to have been placed on the 2011 race,<ref>{{cite news | title = Why the Grand National sees Welsh women bet as much as men | publisher = Wales Online | url = http://www.walesonline.co.uk/news/wales-news/2011/04/09/why-the-grand-national-sees-welsh-women-bet-as-much-as-men-91466-28488657/ | accessdate = 9 April 2011}}</ref> including some from as far afield as Australia, Bermuda and Kazakhstan, with British troops in Afghanistan also joining in. It is estimated that nearly half of the adult population of the UK bets on the Grand National.<ref>{{cite news | title = Grand National punters stake £300m | publisher = Daily Finance | url = http://www.dailyfinance.co.uk/2011/04/08/grand-national-punters-stake-300m/ | accessdate = 8 April 2011}}</ref>

[[File:Ballabriggs cropped.jpg|thumb|right|[[Ballabriggs]], pictured two weeks after his Grand National win.]]
The race received a significant amount of negative media coverage over the two equine fatalities, which were more publicly noticeable than in prior Nationals due to two fences being bypassed for the first time. Those watching the race on television were given clear views of the tarpaulin-covered body of Ornais at the fourth fence, and an aerial shot at Becher's showed veterinary staff attending to the fatally-injured Dooneys Gate, while the remaining runners diverted around them.<ref>{{cite news | title = Horse racing industry must respond to anti-Grand National agenda | publisher = The Telegraph | url = http://www.telegraph.co.uk/sport/horseracing/8457360/Horse-racing-industry-must-respond-to-anti-Grand-National-agenda.html | accessdate = 17 April 2011 | location=London | first=Charlie | last=Brooks | date=17 April 2011}}</ref><ref name=mccoy>{{cite news | title = Jockey Tony McCoy defends Grand National at Aintree | publisher = BBC Sport | url = http://news.bbc.co.uk/sport1/hi/horse_racing/13079492.stm | accessdate = 14 April 2011 | date=14 April 2011}}</ref>

Andrew Taylor, director of the animal rights group [[Animal Aid]], called for an outright ban of the Grand National, saying: "It's a deliberately hazardous, challenging and predictably lethal event."<ref>{{cite news | title = Calls grow to ban Grand National | publisher = MSN | url = http://news.uk.msn.com/uk/articles.aspx?cp-documentid=156960331 | accessdate = 10 April 2011}}</ref> The [[Royal Society for the Prevention of Cruelty to Animals|RSPCA]]'s equine consultant David Muir stated: "I was gutted that two horses died... What I will do now is go back and have a look at each element, with the BHA and the racecourse management, to see if the jump contributed to what happened, look at the take-off and landing side and so on, and see if the evidence suggests something can be done about it. I'm trying to make the race better, safer where I can, but the one thing I can never do is eliminate risk: that's always going to be there." Muir did however add: "There's no way I'm going to get the National banned."<ref>{{cite news | title = Grand National deaths prompt RSPCA involvement in Aintree review | publisher = Guardian | url = 
https://www.theguardian.com/sport/2011/apr/12/grand-national-aintreee-rspca-deaths | accessdate = 12 April 2011 | location=London | first=Chris | last=Cook | date=12 April 2011}}</ref>

Aintree's managing director Julian Thick said: "We are desperately sad at the accidents during the running of the Grand National today and our thoughts go out to the connections of Ornais and Dooneys Gate... Only the best horses and the best jockeys are allowed to enter and all horses are inspected by the vet when they arrive at Aintree to ensure that they are fit to race. This year we had 20 horse-catchers, at least two attendants at each of the 16 fences, four stewards to inspect the course, two [[British Horseracing Authority|BHA]] course inspectors, ten vets, 50 ground staff and 35 ground repair staff... We work closely with animal welfare organisations, such as the RSPCA and [[World Horse Welfare]], to make sure we are up to date with the latest thinking and research... and to make sure that the horses are looked after properly and that the race is run as safely as possible."<ref>{{cite news | title = Grand National result and round up | publisher = Aintree Racecourse | url = http://www.aintree.co.uk/news/grand-national-result-and-round-up/ | accessdate = 10 April 2011}}</ref>

Veteran trainer [[Ginger McCain]] queried the suggestion of reducing the size of the fences in the aftermath of the race. McCain said: "You don't make things better by making it easier. Its speed that does the damage."<ref>{{cite news | title = Ginger McCain queries smaller fences  | publisher = BBC Sport | url = http://news.bbc.co.uk/sport1/hi/other_sports/horse_racing/9453497.stm | accessdate = 10 April 2011 | date=10 April 2011}}</ref> Champion jockey [[Tony McCoy]] also defended the National, saying, "I personally don't think the sport could be in any better shape for horses or jockeys."<ref name=mccoy/> Ornais's owner, Andy Stewart, later said: "We're still grieving but I think this whole hyped up situation regarding the Grand National is totally wrong... National Hunt racing is safer and compliant with every single sport that goes along. My son snowboarded in France and, unfortunately, he had an accident and he'll never walk again. Why don't we just get on with it and enjoy the sport?"<ref>{{cite news | title = Stewart leaps to National defence | publisher = Press Association | url = https://www.google.com/hostednews/ukpress/article/ALeqM5iMfdkmAg9GkjD94FYIXmiD4TSVwQ?docId=N0508021302451841757A | accessdate = 10 April 2011}}</ref>

Winning jockey Jason Maguire was subsequently handed a five-day ban by the stewards for excessive use of the whip on Ballabriggs.<ref>{{cite news | title = Jason Maguire and Ballabriggs battle way to glory at Aintree | publisher = The Telegraph | url = 
http://www.telegraph.co.uk/sport/horseracing/8440199/Grand-National-2011-Jason-Maguire-and-Ballabriggs-battle-way-to-glory-at-Aintree.html | accessdate = 9 April 2011 | location=London | first=Marcus | last=Armytage | date=9 April 2011}}</ref> The horse's trainer, [[Donald McCain, Jr.]], indicated that he will likely return to Aintree for the [[2012 Grand National|2012 National]] seeking the first back-to-back wins since [[Red Rum]] in 1974: "I'd have thought next season will revolve around the National and he may well go to the [[Becher Chase]] [in December] as well."<ref>{{cite news | title = It's Ballabrilliant! Ginger's so proud after McCain Jnr nabs National | publisher = Daily Mail | url = 
http://www.dailymail.co.uk/sport/racing/article-1375492/Ginger-McCain-proud-son-Donald-wins-Grand-National-Ballabriggs.html | accessdate = 11 April 2011 | location=London | first=Marcus | last=Townend | date=11 April 2011}}</ref>

== See also ==
*[[Horseracing in Great Britain]]
*[[List of British National Hunt races]]
*[[2011 Cheltenham Gold Cup]]

== References ==
{{Reflist|30em}}

{{Grand National}}

[[Category:Grand National| 2011]]
[[Category:2011 in horse racing|Grand National]]
[[Category:2011 in English sport|Grand National]]
[[Category:21st century in Merseyside]]