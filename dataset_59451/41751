[[File:Rhodesian Brushstroke pattern.jpg|thumb|Rhodesian Brushstroke [[camouflage pattern]]]]
'''Rhodesian Brushstroke''' is a [[camouflage]] [[camouflage pattern|pattern]] issued to members of the [[Rhodesian Security Forces]] from 1965 until its replacement by a vertical [[Lizard (camouflage)|lizard stripe]] in 1980. It was the default camouflage appearing on battledress of the [[Rhodesian Army]] and [[British South Africa Police]], although used in smaller quantities by [[INTAF]] personnel. The design was also worn by [[South African]] [[South African Special Forces|special operations]] units during raids on [[Mozambique]].{{sfn|Stiff|2000|p=86}} Rhodesian Brushstroke is currently used by the [[Zimbabwe National Army]].{{sfn|Abbot|2014|p=47}}

==Development and history==

Rhodesian Brushstroke is similar to the [[United Kingdom]]'s [[Disruptive Pattern Material]] (DPM). It consists of large, contrasting, shapes tailored to break up the outline of an object. Like most [[Disruptive coloration|disruptive]] camouflage, the pattern is dependent on [[countershading]], utilising hues with high-intensity contrast or noticeable differences in [[chromaticity]].{{sfn|Baumbach|2012|pp=80–81}}

Prior to [[Rhodesia]]'s [[Unilateral Declaration of Independence]], enlisted ranks in the Rhodesian Army wore a somber [[khaki drill]].{{sfn|Ambush Valley Games|2012|p=124}} The [[Battle of Sinoia]] and the outbreak of the [[Rhodesian Bush War]] prompted security forces to devise a more appropriate uniform especially designed for the region. This incorporated a three colour, high contrast, disruptive fabric with green and brown strokes on a sandy background.{{sfn|Ambush Valley Games|2012|pp=124–126}} Early shortages of textile and equipment were overcome with [[South Africa]]n and [[Portugal|Portuguese]] technical assistance, and a home industry for the new battledress developed.{{sfn|Shortt|2003|p=35}} The pattern was supposedly designed by Di Cameron of David Whitehead Textiles.

===Usage===

The basic uniform was trousers with wide belt loops for a stable belt and button cargo pockets.  The shirt was designed to be worn tucked into the trousers, or worn loose.  Ranks, name tapes, unit or branch of service patches were sewn on.  A jacket of slightly heavier material was issued for cooler temperatures, and had distinctive hard elbow pads.  Towards the last few years of the brush war, one-piece suits became more prominent, and not just for aircrew and vehicle users.  Rhodesian camouflage suits commonly took the form of [[Boilersuit|coverall]]s, but these were probably too hot for the climate and uniform regulations in the field were quite lax.{{sfn|Cocks|2009|pp=136–137}} Servicemen often modified their uniforms to shorten the sleeves while others wore privately acquired T-shirts in a bewildering variety of unofficial spinoffs on the army brushstroke.{{sfn|Ambush Valley Games|2012|pp=124–126}} Long camouflage trousers were ignored in favour of drab running shorts.{{sfn|Shortt|2003|p=35}} Despite acknowledging that the pattern itself was exceptional, security forces also criticised the fabric's mediocre quality and cited the [[NATO]] clothing worn by foreign volunteers as being "far superior".{{sfn|Cocks|2009|pp=136–137}}

Pilfered Rhodesian fatigues occasionally turned up in the hands of the [[Zimbabwe People's Revolutionary Army]] (ZIPRA), which used it to impersonate government troops,{{sfn|Petter-Bowyer|2003|p=125}} and the [[People's Movement for the Liberation of Angola]] (MPLA).{{sfn|Venter|2013|p=364}} [[3 South African Infantry Battalion]], the [[South African Special Forces Brigade]], and [[1 Parachute Battalion]] were also offered Rhodesian camouflage during clandestine operations in that country.{{sfn|Scholtz|2013|pp=36–40}} These stocks were withdrawn at [[Zimbabwe]]an [[Southern Rhodesian general election, 1980|independence in 1980]].{{sfn|Hutcheson|2000}} A handful may have been re-issued to [[fifth column]]s between 1981 and 1984.{{sfn|Stiff|2002|p=116}}

The [[Zimbabwe National Army]] adopted Rhodesian Brushstroke at some point prior to the [[Second Congo War]]. It is now produced on a khaki base for the dry season savanna and a pale green base for the rainy season verdure.{{sfn|Abbot|2014|p=47}}

While developing a new disruptive camouflage in the 2000, the [[United States Marine Corps]] (USMC) evaluated over 100 existing patterns, and judged Rhodesian Brushstroke to be one of the three best schemes previously developed, along with [[CADPAT]] and [[tigerstripe]].{{sfn|Delta Gear|2012}} None of the candidates were adopted because there was some indication that Marines would be better served by something more distinctive than a preexisting design. The [[MARPAT]] [[digital camouflage]] eventually introduced by the USMC in 2002 remains in use today, and blends elements from all three camouflages.{{sfn|Delta Gear|2012}}

==See also==
*[[Denison smock]]
*[[Jigsaw camouflage]]
*[[Lizard (camouflage)]]

==Notes and references==
;References
{{reflist}}

;Online sources
{{refbegin}}
*{{cite web|title=SAAF (1978–1980) Waterkloof (Military Transport Workshop) – Rhodesia – Camps |last=Hutcheson |first=Thomas |url=http://sadf.sentinelprojects.com/bg2/tickey.html |location=Newcastle upon Tyne |publisher=Sentinel Projects |date=2000 |accessdate=8 November 2014 |ref=harv |deadurl=yes |archiveurl=https://web.archive.org/web/20150127074555/http://sadf.sentinelprojects.com/bg2/tickey.html |archivedate=27 January 2015 }}
*{{cite web|title=Camouflage Facts |last=Delta Gear |first=Inc |url=http://www.deltagearinc.com/library/camouflage-facts.php |location=Birmingham |publisher=Delta Gear Inc. |date=2012 |accessdate=8 November 2014 |ref=harv |deadurl=yes |archiveurl=https://web.archive.org/web/20150127074647/http://www.deltagearinc.com/library/camouflage-facts.php |archivedate=27 January 2015 }}
{{refend}}

;Bibliography
{{refbegin}}
*{{cite book
|title=The Silent War: South African Recce Operations 1969–1994
|last=Stiff
|first=Peter
|location=Alberton
|publisher=Galago
|year=2000|origyear=1999
|isbn=978-0620243001
|ref=harv}}
*{{cite book
|title=Modern African Wars: The Congo 1960–2002
|last=Abbot
|first=Peter
|location=Oxford
|publisher=[[Osprey Publishing]]
|date=February 2014
|isbn=978-1782000761
|ref=harv}}
*{{cite book
|title=Advances in Military Textiles and Personal Equipment
|last=Baumbach
|first=Johannes
|editor-last=Sparks
|editor-first=Emma
|location=Cambridge
|publisher=[[Woodhead Publishing]]
|year=2012
|isbn=978-1845696993
|ref=harv}}
*{{cite book
|title=Bush Wars: Africa 1960–2010
|last=Ambush Valley Games
|first=(various)
|location=Oxford
|publisher=Osprey Publishing
|year=May 2012
|isbn=978-1849087698
|ref=harv}}
*{{cite book
|title=The Special Air Service
|last=Shortt
|first=James
|location=Oxford
|publisher=Osprey Publishing
|year=2003|origyear=1981
|isbn=978-0850453966
|ref=harv}}
*{{cite book
|title=Fireforce: One Man's War in the Rhodesian Light Infantry
|last=Cocks
|first=Chris
|location=Johannesburg
|publisher=30° South Publishers
|edition=Fourth
|date=June 2009
|origyear=1988
|isbn=978-0-9584890-9-6
|ref=harv}}
*{{cite book
|title=Winds of Destruction: the Autobiography of a Rhodesian Combat Pilot
|last=Petter-Bowyer
|first=P. J. H.
|date=November 2005
|origyear=2003
|location=Johannesburg
|publisher=30° South Publishers
|isbn=978-0-9584890-3-4
|ref=CITEREFPetter-Bowyer2003}}
*{{cite book
|title=Portugal's Guerrilla Wars in Africa: Lisbon's Three Wars in Angola, Mozambique and Portuguese Guinea 1961–74
|last=Venter
|first=Al J
|location=Solihull
|publisher=Helion and Company
|year=2013
|isbn=978-1909384576
|ref=harv}}
* {{cite book
|title=The SADF in the Border War 1966–1989
|last=Scholtz
|first=Leopold
|year=2013
|location=Cape Town
|publisher=Tafelberg
|isbn=978-0-624-05410-8
|ref=harv}}
*{{cite book
|title=Cry Zimbabwe: Independence Twenty Years on
|last=Stiff
|first=Peter
|location=Alberton
|publisher=Galago
|year=2002
|isbn=978-1919854021
|ref=harv}}
{{refend}}
{{Camouflage}}

[[Category:1965 introductions]]
[[Category:Camouflage]]
[[Category:Military camouflage]]
[[Category:Military of Rhodesia]]
[[Category:Military of Zimbabwe]]
[[Category:Military uniforms]]