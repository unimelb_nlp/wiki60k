{{good article}}
{{Infobox scientist
|name= Hugh Bradner
|birth_date= {{Birth date|1915|11|05}}
|birth_place= [[Tonopah, Nevada]]
|death_date= {{Death date and age|2008|05|05|1915|11|05}}
|death_place= [[San Diego, California]]
|death_cause= [[pneumonia]]
|image= Hugh Bradner.gif
|image_size = 240px
|caption= Bradner's identification badge photo from Los Alamos
|nickname= Brad
|nationality= [[United States]]
|alma_mater= {{nowrap|[[Miami University]] - A.B. (1937)<br />[[California Institute of Technology]] - Ph.D. (1941)}}
|doctoral_advisor  = [[William Vermillion Houston]]
|fields= [[Engineering]], [[physics]], and [[geophysics]]
|awards= Miami University Medal (1960)<br />Sc.D. (Honorary), Miami University (1961)
|workplaces= {{nowrap|Champion Paper & Fiber Co. (1936–1937)<br />[[California Institute of Technology]] (1938–1941)<br />US Naval Ordnance Laboratory (1941–1943)<br />[[University of Chicago]] (1943–1943)<br />[[Los Alamos National Laboratory]] (1943–1946)<br />[[University of California, Berkeley]] (1946–1961)<br />[[University of California, San Diego]] (1961–1980)}}
}}

'''Hugh Bradner''' (November 5, 1915 – May 5, 2008) was an [[United States|American]] [[physicist]] at the [[University of California, Berkeley|University of California]] who is credited with inventing the [[neoprene]] [[wetsuit]], which helped to revolutionize [[scuba diving]].

A graduate of Ohio's [[Miami University]], he received his doctorate from [[California Institute of Technology]] in [[Pasadena, California]], in 1941. He worked at the [[US Naval Ordnance Laboratory]] during [[World War II]], where he researched [[naval mines]]. In 1943, he was recruited by [[Robert Oppenheimer]] to join the [[Manhattan Project]] at the [[Los Alamos Laboratory]]. There, he worked with scientists including [[Luis Walter Alvarez|Luis Alvarez]], [[John von Neumann]] and [[George Kistiakowsky]] on the development of the [[high explosives]] and [[exploding-bridgewire detonator]]s required by [[atomic bomb]]s.

After the war, Bradner took a position studying [[high-energy physics]] at the [[University of California, Berkeley]], under [[Luis Walter Alvarez|Luis Alvarez]]. Bradner investigated the problems encountered by [[frogmen]] staying in cold water for long periods of time. He developed a [[neoprene]] suit which could trap the water between the body and the neoprene, and thereby keep them warm. He became known as the "father of the wetsuit."<ref name=sfc>{{cite news |first=Michael |last=Taylor|title=Hugh Bradner, UC's inventor of wetsuit, dies |url=http://www.sfgate.com/cgi-bin/article.cgi?f=/c/a/2008/05/11/BANR10KEF8.DTL|work= [[San Francisco Chronicle]] |publisher= |date=May 11, 2008 |accessdate=May 23, 2008}}</ref>

Bradner worked on the 1951 [[Operation Greenhouse]] nuclear test series on [[Enewetak Atoll]] in the [[Marshall Islands]]. He joined the Scripps Institute of Geophysics and Planetary Physics as a [[geophysicist]] in 1961. He remained there for the rest of his career, becoming a full professor in 1963, and retiring in 1980. In retirement, continued to work both on oceanographic research, as well as on the [[DUMAND]] deep ocean neutrino astronomy project.

==Early life==
Hugh Bradner was born in [[Tonopah, Nevada]], on November 5, 1915,<ref name=times>{{cite news |first=Michael |last=Taylor|title=Hugh Bradner, Physicist who worked on the Manhattan Project and invented the neoprene wetsuit |url=http://www.timesonline.co.uk/tol/comment/obituaries/article3979597.ece|work= [[The Times]]  |date=May 21, 2008 |accessdate=May 23, 2008 | location=London}}</ref> but he was raised in [[Findlay, Ohio]].<ref name=sfc/> His father, Donald Byal Bradner, was briefly director of the Chemical Warfare Service at [[Maryland]]'s [[Edgewood Arsenal]]. His mother was Agnes Claire Bradner née Mead. He had an older brother, Mead Bradner.<ref>{{cite web |url=http://scilib.ucsd.edu/sio/oral/Bradner.pdf |title=Oral History of Hugh Bradner  |publisher=[[University of California]]  |accessdate=September 25, 2013 }}</ref> Bradner graduated from Ohio's [[Miami University]] in 1936 and later received his doctorate from [[California Institute of Technology]] in [[Pasadena, California]], in 1941,<ref name=sfc/> writing his thesis on "Electron-optical studies of the photoelectric effect" under the supervision of [[William Vermillion Houston]].<ref>{{cite web |url=http://thesis.library.caltech.edu/3222/ |title=Electron-optical studies of the photoelectric effect |publisher=Caltech  |accessdate=September 25, 2013 }}</ref>

==Manhattan Project==
[[File:Marjorie Bradner Los Alamos ID.png|thumb|right|Marjorie Bradner's identification badge photo from Los Alamos]]
After receiving his doctorate from Caltech, Bradner worked at the [[US Naval Ordnance Laboratory]] where he researched [[naval mines]] until 1943. He was recruited by [[Robert Oppenheimer]] to join the [[Manhattan Project]] in 1943 at the [[Los Alamos Laboratory]] in [[New Mexico]], which helped to develop the first [[atomic bomb]]. Bradner helped to develop a wide range of technology needed for the bomb, including research on the [[high explosives]] and [[exploding-bridgewire detonator]]s needed to implode the atomic bomb, developed the bomb's triggering mechanism, and even helped design the new town around the [[laboratory]]. He worked closely with some of the most scientists including [[Luis Walter Alvarez|Luis Alvarez]], [[John von Neumann]] and [[George Kistiakowsky]]. He witnessed the [[Trinity test]], the first [[nuclear weapons test]], at [[Holloman Air Force Base|Alamogordo]] on July 16, 1945.<ref name=sfc/><ref name=times/>

Bradner met his future wife, Marjorie Hall Bradner, who was also working as a secretary on the Manhattan Project at the Los Alamos  Laboratory. The couple were married in Los Alamos in 1943. Security at the [[top secret]] facility was so tight that neither Bradner's nor Hall's parents were allowed to attend the ceremony, though Oppenheimer was among the [[wedding]] guests.<ref name=times/> The couple remained together for over 65 years until she died on April 10, 2008 at the age of 89.<ref name=sfc/>

==Wetsuit==
After the war, Bradner took a position studying [[high-energy physics]] at the [[University of California, Berkeley]] under [[Luis Walter Alvarez|Luis Alvarez]], whom he had worked with at the Manhattan Project. He remained at the University until 1961. He worked on the 1951 atomic bombing test on [[Enewetak Atoll]] in the [[Marshall Islands]], which was part of the [[Operation Greenhouse]] nuclear test series.<ref name=times/>

[[File:Гидрокостюм для подводной охоты 9 мм.jpg|thumb|left|240px|A man wearing a modern wetsuit]]
Bradner's job at Berkeley required him to do a number of underwater dives. He had previously talked to [[United States Navy]] [[frogmen]] during [[World War II]] concerning the problems of staying in cold water for long periods of time, which causes the diver to lose large amounts of body heat quickly. He worked on developing a new suit that would counter this in the basement of his family's home on Scenic Avenue in [[Berkeley, California]],<ref name=sfc/> and researched the new [[wetsuit]] at a conference in [[Coronado, California]], in December 1951.<ref name=times/> According to the San Francisco Chronicle, the wetsuit was invented in 1952.<ref name=sfc/> Bradner and other engineers founded the Engineering Development Company (EDCO) in order to develop it.<ref name=times/> He and his colleagues tested several versions and [[prototypes]] of the wetsuit at the [[Scripps Institution of Oceanography]] in [[La Jolla, California]].<ref name=sfc/> Scripps scientist and [[engineer]] [[Willard Bascom]] advised Bradner to use [[neoprene]] for the suit material, which proved successful.<ref name=times/> He found that it  "would trap the water between the body and the neoprene, and the water would heat up to body temperature and keep you warm".<ref name=sfc/>

A 1951 letter showed that Bradner clearly understood that the insulation in such a suit was not provided by the water between the suit and the skin, but rather that this layer of water next to the skin, if trapped, would quickly heat to skin temperature, if the material in the suit were insulative. Thus, the suit only needed to limit purging by fresh cold water, and it did not need to be dry to work.<ref name=Rainey>{{cite web |url=http://scilib.ucsd.edu/sio/hist/rainey_wet_suit_pursuit.pdf |title=Wet Suit Pursuit: Hugh Bradner's Development of the First Wet Suit |accessdate=December 24, 2009 |last=Rainey |first=C |work=UC San Diego, Scripps Institution of Oceanography Archives, Scripps Institution of Oceanography}}</ref> He applied for a [[Patent|U.S. patent]] for the wetsuit, but his patent application was turned down due to its similar design with the [[flight suit]]. The United States Navy also did not adopt the new wetsuits because of worries that the neoprene in the wetsuits might make its swimmers easier to spot by underwater [[sonar]] and, thus, could not exclusively profit from his invention.<ref name=times/>

Bradner and his company, EDCO, tried to sell his wetsuits in the [[consumer]] market. However, he failed to successfully penetrate the wetsuit market the way others have done - including [[Bob Meistrell]] and [[Bill Meistrell]], the founders of [[Body Glove]], and [[Jack O'Neill (businessman)|Jack O'Neill]]. Various claims have been made over the years that it was the O'Neill or the Meistrell brothers who actually invented the wetsuit instead of Bradner, but recent researchers have concluded that it was Bradner who created the original wetsuit, and not his competitors. In 2005 the ''[[Los Angeles Times]]'' concluded that Bradner was the "father of the wetsuit",<ref name=sfc/><ref name=times/> and a research paper published by Carolyn Rainey at the Scripps Institution of Oceanography in 1998 provided corroborating evidence.<ref name=sfc/>

==Later career and life==
Bradner joined the Scripps Institute of Geophysics and Planetary Physics as a [[geophysicist]] in 1961.<ref name=times/>
He became a full professor in 1963 and retired in 1980.<ref name=sfc/> He remained interested in [[oceanography]], [[scuba diving]], seashell collecting and the outdoors throughout his later years,<ref name=sfc/> and continued to work both on oceanographic research, as well as on the [[DUMAND]] deep ocean neutrino astronomy project, which combined his two careers in physics and oceanography.<ref>{{cite web |url=http://www.inp.demokritos.gr/nestor/2nd/files/001_019_resvanis.pdf |title=NESTOR - A Neutrino Particle Astrophysics Ubnderwater Laboratory for the Mediterranean |publisher=[[NESTOR Project]]  |accessdate=September 25, 2013 }}</ref><ref>{{cite web |url=http://www.phys.hawaii.edu/~dumand/roberts_92_rmp.pdf |publisher=University of Hawaii |title=The Birth of High-Energy Neutrino Astronomy: A Personal History of the DUMAND Project |accessdate=September 25, 2013 }}</ref>

Hugh Bradner died at the age of 92 at his home in [[San Diego, California]], on May 5, 2008, from complications of [[pneumonia]]. He was survived by his daughter, Bari Cornet, three grandchildren and one great-granddaughter.<ref name=sfc/>

==References==
{{Reflist|30em}}

==External links==
* [http://scrippsnews.ucsd.edu/Releases/?releaseID=908 Scripps Institute: Renowned Physicist and Inventor of Wetsuit: Hugh Bradner dies]
{{Subject bar
| portal1=World War II
| portal2=Physics
| portal3=History of science
| portal4=Biography
| portal5=Nuclear technology
| portal6=Underwater diving
}}
{{Authority control}}

{{DEFAULTSORT:Bradner, Hugh}}
[[Category:1915 births]]
[[Category:2008 deaths]]
[[Category:American physicists]]
[[Category:University of California, Berkeley faculty]]
[[Category:University of California, San Diego faculty]]
[[Category:Miami University alumni]]
[[Category:California Institute of Technology alumni]]
[[Category:Manhattan Project people]]
[[Category:Los Alamos National Laboratory personnel]]
[[Category:American inventors]]
[[Category:Infectious disease deaths in California]]
[[Category:Deaths from pneumonia]]
[[Category:Scripps Institution of Oceanography]]