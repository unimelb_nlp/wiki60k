{{Infobox person
|name          = Charles McDonnell
|image         = McDonnell One of the Finest card.jpg
|image_size    = 
|caption       = 
|birth_name    = 
|birth_date    = {{birth date|1841|11|18}}
|birth_place   = [[Manhattan, New York]], [[United States]]
|death_date    = {{death date and age|1888|8|15|1841|11|18}}
|death_place   = Manhattan, New York
|death_cause   = [[Heart attack]]
|resting_place = [[Birmingham, Connecticut]], [[United States]]
|nationality   = [[Irish-American]]
|other_names   = Lightning Charlie
|known_for     = NYPD police captain responsible for the capture of a number of high-profile criminals, most notably, [[procuress]] [[Jane the Grabber]] in 1875. 
|education     =
|alma_mater    = 
|employer      = [[New York City Police Department]]
|occupation    = Police officer
|home_town     = 
}}

'''Charles McDonnell''' (November 18, 1841 – August 14, 1888) was an American law enforcement officer and [[police captain]] in the [[New York City Police Department]]. Popularly known as "Lightning Charlie", he was responsible for a number of high-profile arrests during the 1870s/80s, including those of [[procuress]] [[Jane the Grabber]] and gambler Samuel S. Brewster.

==Biography==
Charles McDonnell was born at his home on [[Worth Street (Manhattan)|Anthony Street]] in [[Manhattan, New York]] on November 18, 1841. He attended the local Sixth Ward public school and later went to work as a [[Newspaper delivery|newsboy]] and later employed as a folder in the pressroom of a morning newspaper. He eventually joined the [[New York City Police Department]] and was officially appointed a patrolman on January 21, 1863. Assigned to the Sixth Precinct, he was still a [[rookie]] when the [[New York Draft Riots]] broke out months later and was on constant duty in the Fourth and Sixth Precincts.<ref name="Article">"Death Of Capt. M'Donnell.; He Expires Unexpectedly In His Own Station House". <u>New York Times.</u> 15 Aug 1888</ref> On the evening of July 15, 1863, McDonnell was badly wounded while battling an estimated 400 rioters looting a building at the corner of [[Mott Street|Mott]] and [[Centre Street (Manhattan)|Centre Streets]]. During the fighting, he was "smashed in the face and terribly cut" then knocked down but ignored his injuries and rejoined the squad. He was later commended for his actions.<ref>Barnes, David M. ''The Draft Riots in New York, July, 1863: The Metropolitan Police, Their Services During Riot Week, Their Honorable Record''. New York: Baker & Godwin, 1863. (pg. 44-45)</ref>

On December 2, 1864, McDonnell won promotion to roundsman for his brave conduct in arresting a negro burglar. He slowly rose through the ranks after being promoted to sergeant on November 11, 1867 and finally captain on February 25, 1870. His first command was the old Twenty-Eighth precinct and, during the next four years, served as precinct captain of the Twentieth, Twenty-First and Thirty-First Precincts. On November 11, 1874, McDonnell was transferred to the Eighth Precinct where he remained for the rest of his career. In 1875, during the "grabber scandal", he captured notorious kidnapper-procuress [[Jane the Grabber]].<ref>Costello, Augustine E. ''Our Police Protectors: History of the New York Police from the Earliest Period to the Present Time''. New York: A.E. Costello, 1885. (pg. 125)</ref><ref>[[Herbert Asbury|Asbury, Herbert]]. ''The Gangs of New York: An Informal History of the New York Underworld''. New York: Alfred A. Knopf, 1928. (pg. 183) ISBN 1-56025-275-8</ref> On the day of the last-ever police parade, he arrested negro gambler Samuel S. Brewster for the murder of rival gambler Eibo Williams on July 16, 1887.<ref name="Article"/>

In the winter of that year, McDonnell suffered a severe attack of inflammatory rheumatism but had apparently recovered while vacationing with his wife and sister at their summer home in [[Derby, Connecticut]]. He had been suffering from [[dyspepsia]], aggravated by bronchial trouble, in previous years. McDonnell returned to the city on August 13, 1888, reportedly "looking well and hearty", but intended to arrange to extend his leave of absence and return to Derby the following evening. He was on duty at the stationhouse that day and, leaving orders for a morning wake up call at 9 am, went to bed in the sergeants' room on the second floor at around 10:00 pm. He was seen "sleeping peacefully" by a Sergeant Reilly at 6 am, however he was found dead three hours later when a doorman came to wake him up. A doctor was summoned, but physicians found that he had been dead for several hours. McDonnell's body was moved to his home on King Street home while his wife was informed by telegraph and requested to return to the city.<ref name="Article"/>

His funeral was to be held the next morning at St. Anthony's Roman Catholic Church on [[Sullivan Street]] and then taken to [[Birmingham, Connecticut]] for burial. Then acting Superintendent [[Thomas F. Byrnes]] called a meeting of police captains in order to make arraignments for the funeral services. Inspector Thomas S. Steers, Captains Josiah A. Westervelt, Thomas Reilly, William H. Clinchy and Henry D. Hooker all contributed to the eulogy, Westervelt, Reilly and Clinchy being selected as pall bearers along with Captains Ira S. Garland, John J. Brogan and Gorman. A floral cross was provided by the department and laid on the coffin and crape was worn on the sleeves of all officers above the rank of roundsman for a month.<ref name="Article2">"The Dead Police Captain.; Arrangements By His Associates To Honor His Memory". <u>New York Times.</u> 16 Aug 1888</ref>

A police battalion numbering 350 policemen, accompanied by the Sixty-Ninth Regiment music band, escorted the hearse from King Street to [[Grand Central Station]] <ref name="Article3">"Capt. M'Donnell's Funeral". <u>New York Times.</u> 17 Aug 1888</ref> and commanded by Inspector Steers, Captains [[Anthony J. Allaire]] and Smith. The officers which led the eight individual battalions included Captains Michael J. Murphy, James McLaughlin, Edward Carpenter, Alexander B. Warts, Thomas M. Ryan, Henry Hooker, Philip Cassidy and Peter Yule. The battalion planned to march from Kings Street to [[MacDougal Street]], [[Houston Street (Manhattan)|Houston Street]], [[Fifth Avenue|South Fifth Avenue]], [[Washington Square Park]], [[Fifth Avenue]], [[40th Street (Manhattan)|Fortieth Street]], [[Park Avenue (Manhattan)|Park Avenue]] and [[Forty-Second Street]].<ref name="Article2"/> On the day of the funeral, the procession took a different route and Inspector Steers dismissed the police escort after reaching [[39th Street (Manhattan)|Thirty-Ninth Street]] due to the extreme heat. Services at St. Anthony's Church were also canceled so that McDonnell's body could be loaded onto a train and taken to Birmingham that same day.<ref name="Article3"/>

==References==
{{Reflist}}

{{DEFAULTSORT:McDonnell, Charles}}
[[Category:1841 births]]
[[Category:1888 deaths]]
[[Category:American people of Irish descent]]
[[Category:New York City Police Department officers]]
[[Category:People from Manhattan]]