{{italic title}}
{{unsourced|date=March 2017}}
[[Image:WildeSavilesCrime.jpg|frame|right|195px|The cover of the first edition]]

'''''Lord Arthur Savile's Crime and Other Stories''''' is a collection of short semi-comic mystery stories that were written by [[Oscar Wilde]] and published in 1891. It includes:
*″Lord Arthur Savile's Crime″
*″[[The Canterville Ghost]]″
*″The Sphinx Without a Secret″
*″The Model Millionaire″

In later editions, another story, ″[[The Portrait of Mr. W. H.]]″, was added to the collection.

__TOC__
==Contents==

==="Lord Arthur Savile's Crime"===
This story was first published in ''[[The Court and Society Review]]'', in late 1887. The main character, Lord Arthur Savile, is introduced by Lady Windermere to Mr Septimus R. Podgers, a [[Chiromancy|chiromantist]], who reads his palm and tells him that it is his destiny to be a murderer. Lord Arthur wants to marry, but decides he has no right to do so until he has committed the murder.

His first attempted murder victim is his elderly Aunt Clementina, who suffers from heartburn. Pretending it is medicine, Lord Arthur gives her a capsule of poison, telling her to take it only when she has an attack of heartburn. Reading a telegram in Venice some time later, he finds that she has died and victoriously returns to London to learn that she has bequeathed him some property. Sorting through the inheritance, his future wife Sybil Merton finds the poison pill, untouched; thus Lord Arthur's aunt died from natural causes and he finds himself in need of a new victim. After some deliberation, he obtains a bomb from a jovial German, disguised as a carriage-clock, and sends it anonymously to a distant relative, the Dean of Chichester. When the bomb goes off, however, the only damage done seems like a novelty trick, and the Dean's son spends his afternoons making tiny, harmless explosions with the clock. In despair, Lord Arthur believes that his marriage plans are doomed, only to encounter the same palm-reader who had told his fortune late at night on the bank of the River Thames. Realising the best possible outcome, he pushes the man off a parapet into the river where he dies. A verdict of suicide is returned at the inquest and Lord Arthur happily goes on to marry. In a slight twist, the palmister is denounced as a fraud, leaving the moral of the story to show the power of suggestion.
The story was the basis of the second part of the three-part 1943 film ''[[Flesh and Fantasy]]''.

==="The Canterville Ghost"===
The first of Wilde's stories to be published, appearing in the magazine ''The Court and Society Review'' in February 1887. When a family from the United States buys Canterville Chase, they are told it is haunted by a horrible spirit, but this does not deter them in the slightest. Indeed, when they find a recurring blood stain on the floor, and hear creaking chains in the night, even seeing the [[ghost]] himself, all they do is clean up the blood and insist that the ghost oil his manacles if he is going to keep living in the house. This perturbs the ghost to no end, and he does everything he can to try to frighten the family.

Nothing the ghost does scares them, though the two twins (who enjoy heckling him) do manage to scare the ghost when they erect a fake ghost for him to find. Seeing him sitting alone and depressed, the daughter pities him and offers her help in trying to get him released from haunting. He takes her to the ghostly realm, where she and Death meet, but this meeting, and what goes on during it, is not described. She succeeds in her mission, and the Canterville Ghost disappears, his skeleton being found where it was chained in a hidden room centuries ago. The family buries the skeleton, and the daughter marries a duke, wearing a ruby necklace the ghost had given her before his release.

==="The Sphinx Without a Secret"===
This very short story was first published in ''The World'', in May 1887. In the story, Lord Murchison recounts to his old friend a strange tale of a woman he had loved and intended to marry, but was now dead. She had always been very secretive and mysterious, and he one day followed her to see where she went, discovering her stealthfully going to a [[boarding house]]. He suspected there was another man, and confronted her the next day. She confessed to having been there, but said nothing happened. He did not believe her and left; she died some time later.

He went to the boarding house to speak to the owner, and she confirmed she had rented the room and that all the lady ever did was come to it and sit alone for a few hours at a time, reading or doing nothing.

After telling his story, he asks his friend if he believes it — that her secret really was that she had no secret — and his friend said he was certain of it. Lord Murchison ends with the reply: "I wonder."

==="The Model Millionaire"===
First published in ''The World'' in June 1887. Hughie Erskine is in love and wants to marry, but the girl's father will not allow it, since Erskine has no money. Erskine's friend Alan Trevor is a painter, and he visits him at his studio one day to find him with a pitiable beggar — the model for his painting. Erskine only has one coin, on which he depends for transportation, but he decides he can walk for a couple of weeks and gives the beggar the coin.

The beggar is in reality an immensely wealthy [[baron]], having a portrait of himself as a beggar done for fun. He is so impressed by Erskine's generosity that he gives him £10,000, enough for the girl's father to consent to his proposal.

==Film adaptations==
''Lord Arthur Savile's Crime's'' was turned into [[Lord Arthur Savile's Crime (film)|a 1920 Hungarian film]] directed by [[Pál Fejös]].
It is also the basis of one of the three stories composing [[Julien Duvivier]]'s ''[[Flesh and Fantasy]]'' (1943)

==See also==
{{Portal|Oscar Wilde}}
* [[Music based on the works of Oscar Wilde]]
{{Clear}}

==External links==
{{Gutenberg|no=773|name=Lord Arthur Savile's Crime and Other Stories}} (plain text)
*[https://archive.org/search.php?query=Lord%20Arthur%20Savile%27s%20crime%20AND%20mediatype%3Atexts ''Lord Arthur Savile's Crime and Other Stories''] at [[Internet Archive]] (scanned books illustrated)
* {{librivox book | title=Lord Arthur Savile's Crime and Other Stories | author=Oscar Wilde}}
{{Oscar Wilde|state=collapsed}}

{{DEFAULTSORT:Lord Arthur Savile's Crime And Other Stories}}
[[Category:1891 short story collections]]
[[Category:Fantasy short story collections]]
[[Category:Works by Oscar Wilde]]