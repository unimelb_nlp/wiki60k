{{Primary sources|date=April 2008}}

{{ Infobox OS
| name                   = SharpOS
| logo                   = [[Image:SharpOS Splash Screen.png|150px]]
| screenshot             = [[Image:SharpOS Screenshot.png|300px]]
| caption                = SharpOS v0.0.0.75
| developer              = SharpOS Project
| family                 = [[.NET Framework|.NET]] [[C Sharp (programming language)|C#]] <ref name=family>{{cite web |url=http://www.sharpos.org/redmine/wiki/3/About_SharpOS |title=SharpOS - Info About SharpOS |accessdate=2008-04-13 |work=SharpOS Project}}</ref>
| supported_platforms    = [[X86 architecture|x86]]
| source_model           = [[Open source]]
| latest_release_version = 0.0.1
| latest_release_date    = January 1, 2008 <ref name=m1>{{cite web |url=http://www.sharpos.org/redmine/wiki/3/M1_Announcement |title=M1 Announcement  |accessdate=2008-04-14 |work=SharpOS Project}}</ref>
| working_state          = Discontinued 
| ui                     = [[Command line interface]]
| license                = [[GNU General Public License#Version_3|GNU General Public License Version 3]] with  the [[GPL linking exception]] <ref>{{cite web |url=http://www.sharpos.org/redmine/wiki/3/License_Agreement |title=SharpOS - License Agreement |accessdate=2008-04-13 |work=SharpOS Project |archiveurl = http://web.archive.org/web/20080317191510/http://www.sharpos.org/redmine/wiki/3/License_Agreement <!-- Bot retrieved archive --> |archivedate = 2008-03-17}}</ref>
}}

'''SharpOS''' was an [[open source]] [[.NET Framework|.NET]]-[[C Sharp (programming language)|C#]] based operating system <ref name=family/> that was developed by a group of volunteers and presided over by a team of six project administrators (Mircea-Cristian Racasan, Bruce Markham, Johann MacDonagh, Sander van Rossen, Jae Hyun and William Lahti).<ref>{{cite web|url=http://sourceforge.net/projects/SharpOS |title=SourceForge.net - SharpOS Project Details |accessdate=2008-04-14 |work=SourceForge |deadurl=yes |archiveurl=https://web.archive.org/web/20080229055646/http://sourceforge.net/projects/sharpos |archivedate=February 29, 2008 }}</ref> It is no longer in active development, and resources have been moved to the [[MOSA]] project. This operating system is only one of three [[C Sharp (programming language)|C#]] based [[Operating System|operating systems]] released under an [[open source]] license.<ref>{{cite web |url=http://lab.obsethryl.eu/content/cosmos-one-opensource-csharp-c-based-kernels |accessdate=2008-04-14 |work=obsethryl's lab |title=Cosmos, one of the Opensource CSharp (C#) Based Kernels |quote=The two projects working on this the opensource way are SharpOS (licensing: GPLv3 + runtime exception, hosting: Sourceforge) and Cosmos (licensing: BSD style, hosting: CodePlex).}}</ref><ref>{{cite web |url=http://www.flingos.co.uk/Releases |accessdate=2015-05-30 |work=FlingOS™ website |title=FlingOS, one of the opensource CSharp (C#) based kernels |quote=The kernel is written in C# and has reached a fairly advanced stage. }}</ref> SharpOS has only one public version available <ref name=m1/> and a basic [[command line interface]].

==History==
SharpOS began in November 2006 <ref>{{cite web |url=http://www.sharpos.org/redmine/wiki/3/Draft_Publicity_Article |title=SharpOS - Draft Publicity Article |accessdate=2008-04-13 |work=SharpOS Project |quote=Only one year and two months ago |archiveurl = http://web.archive.org/web/20080306203308/http://www.sharpos.org/redmine/wiki/3/Draft_Publicity_Article <!-- Bot retrieved archive --> |archivedate = 2008-03-06}}</ref> as a public discussion on the [[Mono (software)|Mono]] development [[Electronic mailing list|mailing list]] as a thread called ''Operating System in C#''. After attracting many participants, Michael Schurter created the SharpOS.org [[wiki]] and [[Electronic mailing list|mailing list]] to continue the discussion at a more relevant location. Soon after, the core developers (Bruce Markham, William Lahti, Sander van Rossen and Mircea-Cristian Racasan) decided that they would design their own [[AOT compiler|AOT (Ahead-Of-Time) compiler]] to allow the [[operating system]] to  run its [[boot sequence]] without using another [[programming language]]. Once the [[AOT compiler]] was far enough into development the team then started to code the [[kernel (computer science)|kernel]], this was met with long periods of inactivity and few active developers due to lack of interest in unsafe [[kernel (computer science)|kernel]] [[Computer programming|programming]].<ref>{{cite web |url=http://lab.obsethryl.eu/content/sharpos-stream-c-sharp-c-kernels |accessdate=2008-04-14 |work=obsethryl's lab |title=SharpOS in the stream of C sharp (C#) kernels |quote=}} - See Question 1</ref> On 1 January 2008 the SharpOS team made their first [[Milestone (project management)|milestone release]] public,<ref name=m1/> this is the first version of the [[software]] to appear in the SharpOS [[SourceForge]] package repository available for general public use.<ref>{{cite web |url=http://sourceforge.net/project/showfiles.php?group_id=196652 |title=SourceForge.net - SharpOS Project Files |accessdate=2008-04-15 |work=SourceForge}}</ref>

==Notes==
{{Reflist|2}}

==See also==
{{Portal|Free software}}
*[[Singularity (operating system)]]
*[[Cosmos (operating system)]]
*[[FlingOS]]

==External links==
* [http://www.sharpos.org SharpOS Official Website]  <---- No longer SharpOS Project this site is a tree trimming company :(
* [http://lab.obsethryl.eu/content/sharpos-stream-c-sharp-c-kernels Interview with SharpOS team]
* [http://lists.sourceforge.net/lists/listinfo/sharpos-developers sharpos-developers mailing list]
* [http://www.flingos.co.uk FlingOS website]

{{DEFAULTSORT:Sharpos (Operating System)}}
[[Category:Operating system kernels]]
[[Category:Free software operating systems]]
[[Category:Beta software]]
[[Category:Microkernel-based operating systems]]
[[Category:Microkernels]]
{{MicroKernel}}
{{Operating system}}