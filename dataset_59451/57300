{{Infobox journal
| title = Horizons: The Journal of the College Theology Society
| cover = [[File:Horizons_Journal_Cover_June_2013.jpg]]
| discipline = [[Theology]], [[religious studies]]
| abbreviation = Horizons
| editor = Anthony J. Godzieba
| publisher = [[Cambridge University Press]] on behalf of the College Theology Society, [[Villanova University]]
| country = United States
| history = 1974-present
| frequency = Biannual
| website = http://journals.cambridge.org/action/displayJournal?jid=HOR
| link1 = http://journals.cambridge.org/action/displayIssue?jid=HOR&tab=currentissue
| link1-name = Online access
| link2 = http://journals.cambridge.org/action/displayBackIssues?jid=HOR
| link2-name = Online archive
| ISSN = 0360-9669
| eISSN = 2050-8557
| JSTOR =
| OCLC = 858609197
| LCCN = 77648693
}}
'''''Horizons: The Journal of the College Theology Society''''' is a biannual [[peer-reviewed]] [[academic journal]] established in 1974 and published by [[Cambridge University Press]] on behalf of the College Theology Society ([[Villanova University]]).<ref>{{cite web |url=http://ulrichsweb.serialssolutions.com/title/1398330428090/75143 |title=Horizons (Villanova) &#124; Ulrichs Global Serials Directory |publisher=ulrichsweb.com |date=2014-01-17 |accessdate=2014-04-24}}</ref> While rooted in the Catholic tradition of "faith seeking understanding", the journal covers a range of topics in [[theology]] and [[religious studies]], including [[Catholic theology]], as well as [[Christianity]] and religious experience more generally. The [[editor-in-chief]] is Anthony J. Godzieba (Villanova University).

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Arts and Humanities Citation Index]]
* [[Scopus]]
* [[ATLA Religion Database]]
* [[ATLA Catholic Periodical & Literature Index]]
* Index Theologicus
* [[New Testament Abstracts]]
* [[Religious and Theological Abstracts]]
}}

== References ==
{{reflist}}

== External links ==
* {{Official website|1=http://journals.cambridge.org/action/displayJournal?jid=HOR}}
* [http://www.collegetheology.org/ College Theology Society]

[[Category:Catholic studies journals]]
[[Category:Biannual journals]]
[[Category:Cambridge University Press academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1974]]
[[Category:Academic journals associated with universities and colleges of the United States]]


{{reli-journal-stub}}