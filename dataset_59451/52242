{{Infobox journal
| title = Matrix Biology
| cover = 
| editor = [[Bjorn Olsen]]
| discipline = [[Biology]]
| abbreviation = Matrix Biol.
| formernames = Matrix: Collagen and Related Research, Collagen and Related Research
| publisher = [[Elsevier]]
| country = 
| frequency = 8/year
| history = 1981–present
| openaccess =
| impact = 3.558
| impact-year = 2009
| website = http://www.elsevier.com/wps/find/journaldescription.cws_home/601342/description#description
| link1 = http://www.sciencedirect.com/science/journal/0945053X
| link1-name = Online access
| link2 = 
| link2-name =
| JSTOR = 
| OCLC = 30091752
| LCCN = sn94043255
| CODEN = MTBOEC
| ISSN = 0945-053X
| eISSN = 1569-1802
}}
'''''Matrix Biology''''' is a [[peer review|peer-reviewed]] [[scientific journal]] in the field of [[matrix biology]]. The journal is published 8 times per year by [[Elsevier]]. The journal was established in 1981 as ''Collagen and Related Research''<ref>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/8102998 |title=Collagen and Related Research |publisher=[[United States National Library of Medicine]] |work=NLM Catalog |accessdate=2011-03-29}}</ref> and renamed to ''Matrix: Collagen and Related Research'' in 1988,<ref>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/8906139 |title=Matrix : Collagen and Related Research |publisher=[[United States National Library of Medicine]] |work=NLM Catalog |accessdate=2011-03-29}}</ref> before obtaining its current name in 1994.<ref>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/9432592 |title=Matrix Biology |publisher=[[United States National Library of Medicine]] |work=NLM Catalog |accessdate=2011-03-29}}</ref> It is an official journal of the [[American Society for Matrix Biology]] and the [[International Society for Matrix Biology]]. The current [[editor-in-chief]] is [[Renato V. Iozzo]] ([[Thomas Jefferson University]]).<ref>{{cite web |url=http://www.iozzolab.com/ |accessdate=23 March 2016}}</ref>

== Abstracting and indexing ==
''Matrix Biology'' is abstracted and indexed in [[Elsevier BIOBASE|BIOBASE]], [[Biochemistry and Biophysics Citation Index]], [[Biological & Agricultural Index]], [[Biological Abstracts]], [[BIOSIS Previews]], [[Chemical Abstracts Service]], [[Current Advances in Ecological and Environmental Sciences]], [[Current Awareness in Biological Sciences]], [[Current Contents]], [[EMBASE]], [[EMBiology]], [[Genetics Abstracts]], [[MEDLINE]], [[Science Citation Index]], and [[Scopus]].<ref name="abstracting">{{cite web |url=http://www.elsevier.com/wps/find/journalabstracting.cws_home/601342/abstracting#abstracting |title=Matrix Biology - Elsevier |accessdate=29 March 2011}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2009 [[impact factor]] of 3.558.<ref name=WoS>{{cite web |url=http://isiwebofknowledge.com |title=Web of Science |year=2010 |accessdate=2011-03-30}}</ref>

==References==
{{reflist}}

==External links==
* {{official website|http://www.elsevier.com/wps/find/journaldescription.cws_home/601342/description#description}}

[[Category:Publications established in 1981]]
[[Category:Elsevier academic journals]]
[[Category:Biology journals]]
[[Category:English-language journals]]