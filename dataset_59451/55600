{{Italic title}}
{{Refimprove|date=November 2016}}
'''''L'Année Sociologique''''' is an [[academic journal]] of [[sociology]] established in 1898 by [[Émile Durkheim]], who also served as its editor. It was published annually until 1925, changing its name to ''Annales Sociologiques'' between 1934 and 1942. After [[World War II]] it returned to its original name. Durkheim established the journal as a way of publicizing his own research and the research of his students and other scholars working within his new sociological [[paradigm]].

== History ==
Until 1969 a part of the journal was classified into three groups: (1) [[anthropology]] and [[sociology]], (2) empirical qualitative sociology, and (3) empirical quantitative sociology. The rest was dedicated to [[book review]]s.<ref>{{Cite journal |last=Kando |first=Thomas M. |date=1976-01-01 |title=L'Annee Sociologique: From Durkheim to Today |url=http://www.jstor.org/stable/1388781 |journal=The Pacific Sociological Review |volume=19 |issue=2 |pages=147–174 |doi=10.2307/1388781}}</ref> Durkheim wanted to use the journal to describe contents of the books reviewed.<ref>{{Cite journal |last=Smith |first=Philip |date=2014-02-19 |title=The Cost of Collaboration: Reflections Upon Randall Collins' Theory of Collective Intellectual Production via Émile Durkheim: A Biography |url=https://muse.jhu.edu/article/537353 |journal=Anthropological Quarterly |volume=87 |issue=1 |pages=245–254 |doi=10.1353/anq.2014.0012 |issn=1534-1518}}</ref> 

==Modern journal==
Although originally influenced by Émile Durkheim, the modern journal is based on accuracy and researthe [[history of the social sciences]]. Originally publishing in French only, the journal now also publishes translations from French and articles written in English.<ref name=":0">{{Cite web |url=http://www.cairn-int.info/about_this_journal.php?ID_REVUE=E_ANSO |title=About L’Année sociologique |website=Cairn International |access-date=2016-11-01}}</ref>

==See also==
*''[[American Journal of Sociology]]''
*[[List of sociology journals]]

==References==
<references />

==External links==
* [http://www.cairn.info/revue-l-annee-sociologique.htm ''L'Année Sociologique''] on [[Cairn.info]]

{{Authority control}}

{{Émile Durkheim|state=collapsed}}

{{DEFAULTSORT:Annee Sociologique}}
[[Category:Sociology journals]]
[[Category:Multilingual journals]]
[[Category:Publications established in 1898]]
[[Category:Émile Durkheim]]
[[Category:History of sociology]]

{{socialscience-journal-stub}}
{{socio-stub}}