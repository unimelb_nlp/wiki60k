{{Infobox journal|
title = Algebra & Number Theory
}}
'''''Algebra & Number Theory''''' (ISSN 1937-0652) is a peer-reviewed [[mathematics]] journal published by the nonprofit organization [[Mathematical Sciences Publishers]].<ref>[http://msp.org Mathematical Sciences Publishers]</ref>
It was launched on January 17, 2007 with the goal of "providing an alternative to the current range of commercial specialty journals in [[algebra]] and [[number theory]], an alternative of higher quality and much lower cost."<ref>[http://listserv.nodak.edu/cgi-bin/wa.exe?A2=ind0701&L=nmbrthry&T=0&P=2999 Announcement email to the NMBRTHRY email list]</ref>

The journal publishes original research articles in [[algebra]] and [[number theory]], interpreted broadly, including [[algebraic geometry]] and [[arithmetic geometry]], for example.<ref>[http://msp.berkeley.edu/ant/about/journal/about.html "About the journal" at the ANT website]</ref>
Issues are published both online and in print.

== Editorial board ==
The Managing Editor is [[Bjorn Poonen]] of [[Massachusetts Institute of Technology|MIT]], and the Editorial Board Chair is [[David Eisenbud]] of [[University of California at Berkeley|U. C. Berkeley]].<ref>[http://msp.berkeley.edu/ant/about/journal/editorial.html "Editorial board" at the ANT website]</ref>

==See also==
* [[Jonathan Pila]]

== References ==
{{reflist}}

== External links ==
* [http://msp.berkeley.edu/ant/about/cover/cover.html ''Algebra & Number Theory'']
* [http://www.mathscipub.org/ Mathematical Sciences Publishers]

{{DEFAULTSORT:Algebra and Number Theory}}
[[Category:Mathematics journals]]
[[Category:Publications established in 2007]]
[[Category:Mathematical Sciences Publishers academic journals]]