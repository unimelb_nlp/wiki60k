{{advert|date=August 2015}}

'''Diavolo | Architecture in Motion''' (simply known as '''Diavolo''') is an American dance company founded by [[Jacques Heim (choreographer)|Jacques Heim]] in 1992. The company's movement style encompasses modern dance, acrobatics, and gymnastics. Diavolo has been based in Los Angeles ever since its founding, and has toured across the United States as well as Europe, Asia, and Latin America.

==The company==
Upon completion of his MFA (Dance and Choreography) at the [[California Institute of the Arts]], [[Jacques Heim (choreographer)|Jacques Heim]] started Diavolo | Architecture in Motion, which performs acrobatic and modern dance movement set on a variety of oversized structures. These oversized architectural structures serve as a backdrop to the dramatic movement that create metaphors for humanity's everyday struggles—relationships, the absurdity of life, and the attempt to maintain our humanity in an increasingly technological world.

Alongside his position as Artistic Director of Diavolo, Heim was hired in 2005 by [[Cirque du Soleil]] to choreograph a show in Las Vegas called [[Kà]]. Heim has also choreographed for television shows, movies, as well as special events worldwide. On television, his work appeared on BBC America's [[Dancing with the Stars (U.S. TV series)]],<ref>Brett Love, [http://www.aoltv.com/2008/03/25/dancing-with-the-stars-week-2-results/ "Dancing with the Stars: Week Two Results"] ''HuffPost TV'', 03/25/08.</ref> as well as Bravo's [[Step It Up and Dance]].<ref>http://www.imdb.com/name/nm3045034/ Jacques Heim IMDB Page.</ref>

However, most of Heim's time is spent choreographing for and touring with Diavolo. The company's repertoire presents dance that is redefined "through dynamic movement, enlightening communities through trust, teamwork, and individual expression" as articulated in the company's mission statement.<ref name="diavolo">http://www.diavolo.org "Who We Are: Diavolo Aesthetic."</ref>
Beginning with ''Bonjour/Sleepwalking'', which was choreographed in 1992, Heim has created and continues to create pieces that epitomize the Diavolo aesthetic emphasized in the mission statement.

Diavolo made its European debut at the [[Edinburgh Festival Fringe]] in 1995, and in 1998, the company opened the performance series at the new [[J. Paul Getty Museum|Getty Center Museum]] in Los Angeles. Diavolo's first full-evening length work, ''Catapult'', was created in 1999, which was also the year of the first full North American tour for the company.

In spring 2002, Diavolo created a second smaller company to perform in a [[cabaret]]-style show, which ran for eight weeks at the New Shinagawa Prince Hotel in [[Tokyo]], [[Japan]].

Diavolo recently collaborated with the [[Los Angeles Philharmonic]] to create a trilogy of dance works performed at the [[Hollywood Bowl]]. In 2007, ''Foreign Bodies'' premiered, set to the music of [[Esa-Pekka Salonen]], followed by ''Fearful Symmetries'' in 2010, set to the music of [[John Adams (composer)|John Adams]]. In 2013, ''Fluid Infinities'', set to the music of [[Philip Glass]], completed the trilogy on the Hollywood Bowl stage.<ref>http://www.diavolo.org/ "Who We Are: About Diavolo."</ref>

Diavolo also engages in community work and educational outreach, which includes holding classes, workshops, and seminars for children and adults. Many of the workshops are regularly held at schools, hospitals, and juvenile detention centers.

==Repertoire==
The most notable pieces in Diavolo's repertoire are: ''Catapult'' (1999), ''Trajectoire'' (1999, 2001), ''Humachina'' (2002, 2006), ''L'Espace Du Temps'', The Trilogy [''Foreign Bodies'' (2007), ''Fearful Symmetries'' (2010), ''Fluid Infinities'' (2013)], and ''Transit Space'' (2012).

Demo footage of the company's repertoire can be found on the company's YouTube page (linked below).

A full list of Diavolo repertoire can be found on the company's [http://www.diavolo.org  website].

===Corporate work===
Diavolo Creative Productions is the corporate sector of the company. It has created performance events for corporate clients such as [[Wells Fargo| Wells Fargo Bank]], [[Honda]], Sebastian Inc. and [[General Motors]].<ref>http://www.diavolo.org "Who We Are: About Diavolo."</ref>

==Educational and community outreach==
While educational and community outreach has been a part of Diavolo since its inception in 1992, the program began to grow in 1998 and continues to do so with a great deal of educational performances and instructional programming. At the launch of its pilot programs in 2012, The Diavolo Institute received funding from the James Irvine Foundation, the W.M. Keck Foundation, and others.

The Diavolo Institute provides dance education and active arts participation for low-income youth and their families in the Los Angeles area. The program includes highlights such as: ''L.A. Familia'' (a multi-generational dance night that uses dance and everyday movement to get entire families working together), ''L.A. Unity'' (a regular workshop series that culminates in school and community performances on the distinct Diavolo set pieces), and ''T.R.U.S.T.'' (an interactive in-school assembly show, featuring trust exercises and student participation).

In 2010, Diavolo received an American Masterpieces grant with the University of Georgia Research Foundation from the [[National Endowment for the Arts]] to fund educational activities.
Diavolo received another grant from the National Endowment for the Arts in 2012.<ref>http://www.nea.gov/grants/recent/12grants/12cham.php?disc=Challenge%20America "Germantown Performing Arts Center" to support Diavolo Dance Theater.</ref>

==Awards and recognition==
In 1995 during its European debut at the [[Edinburgh Festival Fringe]], Diavolo was named "Best of the Fest" by [[The Independent|The London Independent]] and "Critics Choice" by [[The Guardian]].<ref>http://popejoypresents.com/performance-guides/show-guide-for-diavolo "Show Guide for Diavolo: The Company."</ref>

The Dance Resource Center of Greater Los Angeles presented the 1995 [[Lester Horton]] Dance Award to Diavolo Dance Theater for outstanding achievement in performance/company. The [[Carpenter Performing Arts Center| Carpenter Performing Arts Center CSULB]] honored Diavolo Dance Theater as the Arts in Education Hero of the year 2006 presented for Diavolo's support and dedication to bringing the arts to young people. On September 28, 2007, Diavolo was recognized as a cultural treasure of [[Los Angeles]] and was presented the Certificate of Recognition from the City of Los Angeles.

[[Los Angeles Times]] dance critic Lewis Segal reviewed Diavolo's 2007 and 2010 Hollywood Bowl performances with the LA Philharmonic, saying that Diavolo exemplifies one of "those rare events that define the art of this city, when the levels of vision and support are equally exceptional….[Diavolo] makes precisely coordinated feats look improvisational, even reckless….To say Diavolo is exciting is redundant."<ref>Lewis Segal, [http://latimesblogs.latimes.com/culturemonster/2010/09/dance-review-fearful-symmetries-diavolo-meets-john-adams-at-hollywood-bowl.html "Dance Review: Diavolo Meets John Adams at the Hollywood Bowl"], ''The Los Angeles Times'', 09/10/10.</ref><ref>Lewis Segal, [http://articles.latimes.com/2007/sep/06/news/wk-diavolo6 "'Foreign Bodies': Life on the Edge"], ''The Los Angeles Times'', 09/06/07.</ref>

==References==
{{reflist}}

*
*
*
*

==External links==
* [http://www.diavolo.org/ Official Website]
* [http://www.facebook.com/diavolo.dance Facebook Page]
* [https://www.youtube.com/user/DiavoloDanceTheater Youtube Page]
* [https://twitter.com/diavolodance Twitter]

[[Category:Dance companies in the United States]]
[[Category:Dance in California]]