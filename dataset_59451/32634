{{about|the Australian soldier|the American politician (1830–1907)|James E. Newland}}
{{EngvarB|date=August 2014}}
{{Use dmy dates|date=August 2014}}
{{Infobox military person
|name= James Ernest Newland
|image= James Newland A02614.JPG
|image_size= 250px
|alt= A head and shoulders portrait of a man in military uniform.
|caption= Captain James Newland c.1918
|nickname= 
|birth_date= {{birth date|df=yes|1881|08|22}}
|birth_place= [[Highton, Victoria]]
|death_date= {{death date and age|df=yes|1949|3|19|1881|8|22}}
|death_place= [[Caulfield, Victoria]]
|placeofburial= 
|allegiance= Australia
|branch= [[Australian Army]]
|serviceyears= 1899–1902<br/>1903–1909<br/>1910–1941
|rank= [[Lieutenant Colonel]]
|unit= [[12th Battalion (Australia)|12th Battalion]] (1914–18)
|commands= 
|battles= [[Second Boer War]]<br/>First World War
* [[Gallipoli Campaign]]
** [[Landing at Anzac Cove]]
* [[Western Front (World War I)|Western Front]]
** [[Battle of the Somme]]
** [[Battle of Pozières]]
** [[Battle of Mouquet Farm]]
** [[Battle of Arras (1917)|Battle of Arras]]
[[Second World War]]
|awards= [[Victoria Cross]]<br/>[[Meritorious Service Medal (United Kingdom)|Meritorious Service Medal]]<br/>[[Mentioned in Despatches]]
|relations= 
|laterwork= 
}}
'''James Ernest Newland''', [[Victoria Cross|VC]] (22 August 1881&nbsp;– 19 March 1949) was an Australian soldier, policeman and a [[List of Australian Victoria Cross recipients|recipient]] of the [[Victoria Cross]], the highest decoration for gallantry "in the face of the enemy" that can be awarded to members of the British and [[Commonwealth of Nations|Commonwealth]] armed forces. Newland was awarded the Victoria Cross following three separate actions in April 1917, during attacks against German forces retreating to the [[Hindenburg Line]]. While in command of a [[Company (military unit)|company]], Newland successfully led his men in several assaults on German positions and repulsed subsequent counter-attacks.

Born in the [[Victoria (Australia)|Victorian]] town of [[Highton, Victoria|Highton]], Newland joined the Australian military in 1899 and saw active service during the [[Second Boer War]]. He continued to serve in the [[Australian Army]]'s permanent forces on his return to Australia, and completed several years' service in the artillery. Transferring to the militia in 1907, Newland became a police officer in Tasmania before re-joining the permanent forces in 1910. Following the outbreak of the First World War, he was appointed to the [[First Australian Imperial Force|Australian Imperial Force]] and was among the first wave of men to [[Landing at Anzac Cove|land at Gallipoli]]. In the days following the landing, Newland was wounded and evacuated to Egypt where he was commissioned as a second lieutenant.

Transferring to the Western Front in 1916, Newland was [[mentioned in despatches]] for his leadership while commanding a company during an attack at [[Battle of Mouquet Farm|Mouquet Farm]]. He was wounded twice more during the war and medically discharged in March 1918; he returned to service with the permanent army. Newland held various appointments between the two world wars, and retired a lieutenant colonel in 1941. He died of heart failure in 1949.

==Early life==
Newland was born in the [[Geelong]] suburb of [[Highton, Victoria]], on 22 August 1881 to William Newland, a labourer, and his wife Louisa Jane (née Wall).<ref name="ADB">{{Australian Dictionary of Biography|last=Staunton|first=Anthony|year=1988|id=A110011b|title=Newland, James Ernest (1881–1949)|accessdate=14 December 2008}}</ref> In 1899, he enlisted in the [[Australian Army|Commonwealth Military Forces]] and was assigned to the 4th Battalion, [[Australian Commonwealth Horse]], as a [[Private (rank)|private]].<ref name="VCs46">{{Harvnb|Gliddon|2000|p=46}}</ref><ref>{{cite web|url=http://www.awm.gov.au/nominalrolls/boer/person.asp?p=11888|title=James Ernest Newland|accessdate=14 December 2008|work=Boer War Nominal Roll|publisher=Australian War Memorial}}</ref> The unit later embarked for South Africa, where Newland saw active service in [[Cape Town]] during the [[Second Boer War]].<ref name="ADB"/>

Returning to Australia in 1902, Newland re-settled in Victoria and joined the [[Royal Australian Artillery]] in July the following year.<ref name="AF">{{Harvnb|Staunton|2005|p=75}}</ref> He served in the artillery for over four years, before transferring to the [[Citizens Military Force|militia]] in September 1907. In 1909, he became a police officer in the [[Tasmania Police|Tasmanian Police Force]], where he remained until August 1910, when he re-enlisted in the permanent army. He was posted to the [[Australian Instructional Corps]]; he served with this unit until the outbreak of the First World War. In a ceremony at [[Sheffield, Tasmania]], on 27 December 1913, Newland married Florence May Mitchell.<ref name="ADB"/>

==First World War==
On 17 August 1914,<ref name="Embark">{{cite web|url=http://www.awm.gov.au/cms_images/awm8/23_29_1/pdf/0335.pdf|title=James Ernest Newland|accessdate=18 December 2008|format=PDF|work=First World War Embarkation Roll|publisher=Australian War Memorial}}</ref> Newland transferred to the newly raised [[First Australian Imperial Force|Australian Imperial Force]] following the [[British Empire]]'s declaration of war on Germany and her allies.<ref name="AF"/> Assigned to the [[12th Battalion (Australia)|12th Battalion]], he was made its [[regimental quartermaster sergeant]] and embarked from Hobart aboard HMAT ''Geelong'' on 20 October, bound for Egypt.<ref name="Embark"/> Following a brief stop in Western Australia,<ref name="12th">{{cite web|url=http://www.awm.gov.au/units/unit_11199.asp|title=12th Battalion|accessdate=18 December 2008|work=Australian military units|publisher=Australian War Memorial}}</ref> the troopship arrived at its destination seven weeks later.<ref name="VCs46"/> The 12th Battalion spent the following four months training in the Egyptian desert.<ref name="12th"/>

At the commencement of the [[Gallipoli Campaign]], the [[3rd Brigade (Australia)|3rd Australian Brigade]]—of which the 12th Battalion was part—was designated as the covering force for the [[Landing at Anzac Cove|ANZAC landing]], and as such was the first unit ashore on 25 April 1915, at approximately 04:30.<ref name="12th"/> Newland was wounded in the days following the landing, suffering a gunshot wound to his arm, and was evacuated to the 1st General Hospital.<ref name="AIF">{{cite web|url=https://aif.adfa.edu.au/showPerson?pid=222371|title=James Ernest Newland|accessdate=19 December 2008|work=The AIF Project|publisher=Australian Defence Force Academy}}</ref> While at the hospital, he was commissioned as a [[second lieutenant]] on 22 May,<ref name="VCs46"/> before returning to the 12th Battalion four days later.<ref name="AIF"/>

[[File:Newland VC outdoor P02939.014.JPG|thumb|alt=An informal portrait of a man in military uniform sitting down with his head cocked slightly to the side.|upright|left|175px|Outdoor portrait of Captain James Newland]]

Newland was engaged in operations on the Gallipoli Peninsula until 9 June, when he was withdrawn from the area and placed in command of the 12th Battalion's transport elements stationed in Egypt.<ref name="ADB"/> Promoted to [[lieutenant]] on 15 October, he was hospitalised for ten days in November due to [[dengue fever]].<ref name="AIF"/> Following the [[Allies of World War I|Allied]] evacuation of Gallipoli in December, the 12th Battalion returned to Egypt where Newland continued as transport officer.<ref name="12th"/> Promoted to [[Captain (armed forces)|captain]] on 1 March 1916, he was made [[adjutant]] of the 12th Battalion fifteen days later. It embarked for France and the [[Western Front (World War I)|Western Front]] later that month.<ref name="ADB"/>

Disembarking at [[Marseilles]],<ref name="12th"/> the 12th Battalion was initially posted to the [[Fleurbaix]] sector of France. After involvement in minor operations, it transferred to the [[Somme (department)|Somme]] in July,<ref name="VCs46"/> where it participated in the [[Battle of Pozières]], its first major French action.<ref name="12th"/> Newland was posted to command A Company from 8 August, and was subsequently moved to [[Sausage Valley]] along with the rest of the 12th Battalion in preparation for an attack on [[Battle of Mouquet Farm|Mouquet Farm]].<ref name="VCs46"/>

Mouquet Farm was a ruined complex connected to several German strongpoints, and formed part of the [[Thiepval]] defences.<ref name="VCs46"/> On 21 August, Newland led his company in an assault on a series of trenches slightly north east of the farm. By 18:30, the company had captured its objectives and several of Newland's men rushed off in pursuit of the retreating Germans. Newland immediately stopped them and organised the company into a defensive position; the trench was consolidated by 05:00 the next morning. Praised for his "...&nbsp;great coolness and courage under heavy fire" during the attack, he was recommended for the [[Military Cross]].<ref>{{cite web|url=http://www.awm.gov.au/cms_images/awm28/1/8P2/0004.pdf|title=Recommendation for James Ernest Newland to be awarded a Military Cross|accessdate=23 December 2008|format=PDF|work=Recommendations: First World War|publisher=Australian War Memorial}}</ref> The award, however, was downgraded to a [[mention in despatches]],<ref name="ADB"/><ref>{{cite web|url=http://www.awm.gov.au/cms_images/awm28/1/8P2/0003.pdf|title=Recommendation for James Ernest Newland to be awarded a Mention in Despatches|accessdate=23 December 2008|format=PDF|work=Recommendations: First World War|publisher=Australian War Memorial}}</ref> the announcement of which was published in a supplement to the ''[[London Gazette]]'' on 4 January 1917.<ref>{{LondonGazette|issue=29890|date=4 January 1917|startpage=255|supp=yes|accessdate=23 December 2008}}</ref>

Following its involvement at [[Pozières]] and Mouquet Farm, the 12th Battalion was briefly transferred to the [[Ypres]] sector in Belgium in September, before returning to Bernafay Wood on the Somme late the following month.<ref name="VCs46"/><ref name="12th"/> Newland was admitted to the 38th Casualty Clearing Station with [[pyrexia]] on 4 December. He was moved to the 2nd General Hospital at [[Le Havre]], and returned to the 12th Battalion two weeks later following recuperation. On the same day, he was attached to the headquarters of the 2nd Australian Brigade for duty as a staff officer. He was granted leave on 21 January 1917 on completion of this stint.<ref name="AIF"/>

Re-joining the 12th Battalion, Newland once again assumed command of A Company. On 26 February 1917, he was tasked with leading it during the 12th Battalion's attack on the village of La Barque during the German retreat to the [[Hindenburg Line]].<ref name="VCs46"/> At Bark Trench, a position on the north side of the centre of La Barque,<ref>{{Harvnb|Gliddon|2000|p=79}}</ref> the company encountered a German strongpoint and Newland received a gunshot wound to the face. He was admitted to the 1st Australian Field Ambulance, and returned to the 12th Battalion on 25 March after a period of hospitalisation at the 7th Stationary Hospital in Boulogne.<ref name="AIF"/>

===Victoria Cross===
By early April 1917, there remained three German-held outpost villages—[[Boursies]], Demicourt and [[Hermies]]—between the area to the south of the [[I Anzac Corps]] position and the Hindenburg Line.<ref name="VCs44">{{Harvnb|Gliddon|2000|p=44}}</ref> An attack by the [[1st Division (Australia)|1st Australian Division]] to capture them was planned for 9 April, the same day the British offensive opened at [[Battle of Arras (1917)|Arras]].<ref name="AF"/> For his actions on three separate occasions during the assault, Newland was awarded the Victoria Cross.<ref name="VCrec">{{cite web|url=http://www.awm.gov.au/cms_images/awm28/1/22/0040.pdf|title=Recommendation for James Ernest Newland to be awarded a Victoria Cross|accessdate=24 December 2008|format=PDF|work=Recommendations: First World War|publisher=Australian War Memorial}}</ref>

[[File:James Newland VC medals.jpg|thumb|right|250px|James Newland's medals at the Australian War Memorial, Canberra.]]

On the night of 7/8 April, the 12th Battalion was tasked with the capture of Boursies, on the [[Bapaume]]–[[Cambrai]] road.<ref name="VCs44"/> The attack was a feint to mislead the German forces on the direction from which Hermies was to be assaulted.<ref name="AF"/> Leading A Company as well as an attached platoon from B Company,<ref name="VCs44"/> Newland began his advance on the village at 03:00. The company was soon subject to heavy rifle and machine gun fire from a derelict mill approximately {{convert|400|m|yd}} short of the village, and began to suffer heavy casualties. Rallying his men, Newland charged the position and bombed the Germans with grenades. The attack dislodged the Germans, and the company secured the area and continued its advance.<ref name="AF"/>

Throughout 8 April, the Australians were subjected to heavy shellfire from German forces. At approximately 22:00,<ref name="AF"/> the Germans launched a fierce counter-attack under the cover of a barrage of bombs and [[trench mortar]]s against A Company's position at the mill.<ref name="VCrec"/> They had some initial success and entered the forward posts of the mill, which were occupied by a platoon of Newland's men under the command of Sergeant [[John Whittle]].<ref>{{Harvnb|Gliddon|2000|p=77}}</ref> Newland, bringing up a platoon from the battalion's reserve company, charged the attackers and re-established the lost ground with Whittle's assistance.<ref name="ADB"/><ref name="VCrec"/> The 12th Battalion was relieved by the [[11th Battalion (Australia)|11th Battalion]] on 10 April, having succeeded in capturing Boursies at the cost of 240 casualties, of which 70 were killed or missing.<ref name="VCs44"/>

After a four-day reprieve from the frontline, the 12th Battalion relieved the 9th Battalion at Lagnicourt on 14 April. Around dawn the next day, the Germans launched a severe counter-attack against the 1st Australian Division's line.<ref>{{Harvnb|Gliddon|2000|pp=44–45}}</ref> Breaking through, they forced back the 12th Battalion's D Company, which was to the right of Newland's A Company.<ref>{{Harvnb|Staunton|2005|p=77}}</ref> Soon surrounded and under attack on three sides,<ref name="VCs45">{{Harvnb|Gliddon|2000|p=45}}</ref> Newland withdrew the company to a sunken road which had been held by Captain [[Percy Cherry]] during the capture of the village three weeks earlier, and lined the depleted company out in a defensive position on each bank.<ref name="AF"/>

[[File:Newland VC P02939.013.JPG|thumb|alt=A full length portrait of a man in military uniform wearing two military medals and leaning on a swagger stick.|upright|left|175px|Captain James Newland ca. 1918]]

The German forces attacked Newland's company several times during the battle, but were repulsed each time. During one of the assaults, Newland observed that the German attack was weakening and gathered a party of twenty men. Leading the group, he charged the Germans and seized forty as [[Prisoner of war|prisoners]].<ref>{{cite web|url=http://www.awm.gov.au/cms_images/awm28/1/22/0041.pdf|title=Recommendation for James Ernest Newland to be awarded a Victoria Cross (Cont)|accessdate=24 December 2008|format=PDF|work=Recommendations: First World War|publisher=Australian War Memorial}}</ref> As reinforcements from the 9th Battalion began to arrive, a combined counter-attack was launched and the line recaptured by approximately 11:00.<ref name="AF"/> During the engagement, the 12th Battalion suffered 125 casualties, including 66 killed or missing.<ref name="VCs45"/> Newland and Whittle were both awarded the Victoria Cross for their actions at Boursies and Lagnicourt; they were the only two permanent members of the Australian military to receive the decoration during the war. At 35 years and 7 months old, Newland was also the oldest Australian Victoria Cross recipient of the First World War.<ref name="VCs45"/>

The full citation for Newland's Victoria Cross appeared in a supplement to the ''London Gazette'' on 8 June 1917:<ref>{{LondonGazette|issue=30122|date=8 June 1917|startpage=5702|supp=yes|accessdate=24 December 2008}}</ref>

{{quote|''War Office, 8th&nbsp;June, 1917.''

His Majesty the KING has been graciously pleased to approve of the award of the Victoria Cross to the undermentioned Officers, Non-commissioned Officers and Men: —

Capt. James Ernest Newlands,{{sic}} Inf. Bn., Aus. Imp. Force.<ref>Note: Newland's name was incorrectly given as "Newlands", which was amended in a later issue of the ''London Gazette'': {{LondonGazette|issue=30212|date=21 July 1917|startpage=7869|supp=yes|accessdate=24 December 2008}}</ref>

For most conspicuous bravery and devotion to duty, in the face of heavy odds, on three separate occasions.

On the first occasion he organised the attack by his company on a most important objective, and led personally, under heavy fire, a bombing attack. He then rallied his company, which had suffered heavy casualties, and he was one of the first to reach the objective.

On the following night his company, holding the captured position, was heavily counter-attacked. By personal exertion, utter disregard of fire, and judicious use of reserves, he succeeded in dispersing the enemy and regaining the position.

On a subsequent occasion, when the company on his left was overpowered and his own company attacked from the rear, he drove off a combined attack which had developed from these directions.

These attacks were renewed three or four times, and it was Capt. Newland's tenacity and disregard for his own safety that encouraged the men to hold out.

The stand made by this officer was of the greatest importance, and produced far-reaching results.}}

===Later war service===
In early May 1917, the 12th Battalion was involved in the British and Australian attempt to capture the village of [[Bullecourt]].<ref name="AF"/> While engaged in this operation on 6 May, Newland was wounded for the third and final time of the war by a gunshot to his left armpit. Initially admitted to the 5th Field Ambulance, he was transferred to No 1 Red Cross Hospital, [[Le Touquet]], the next day. The injury necessitated treatment in England, and Newland was shipped to a British hospital eight days later.<ref name="AIF"/>

On recovering from his wounds, Newland attended an investiture ceremony at [[Buckingham Palace]] on 21 July, where he was decorated with his Victoria Cross by King [[George V]].<ref name="AF"/> Later the same day, Newland boarded a ship to Australia. It arrived in Melbourne on 18 September, and Newland travelled to Tasmania. He was discharged from the Australian Imperial Force as medically unfit on 2 March 1918.<ref name="AIF"/>

==Later life==
[[File:VC Anzac Day 1927 P02349.005.JPG|thumb|alt=Lines of men in rows of four wearing military uniforms or suits and military medals.|right|250px|A group of Victoria Cross recipients lined up to march on [[Anzac Day]] in Melbourne, 1927. Newland is front row, far left.]]

Following his discharge, Newland retained the rank of captain and returned to service with the permanent military forces.<ref name="ADB"/> Between the two world wars, he held various appointments in the army, including adjutant and [[quartermaster]] of the [[8th Battalion (Australia)|8th]], 49th, 52nd, 38th and 12th Battalions, as well as area officer and recruiting officer.<ref name="AF"/> In 1924, Newland's wife Florence died of [[tuberculosis]]. On 30 April 1925, he married Heather Vivienne Broughton in a ceremony at St Paul's Anglican Church, [[Bendigo]]; the couple would later have a daughter.<ref name="ADB"/> Promoted to [[major]] on 1 May 1930, Newland was awarded the [[Meritorious Service Medal (United Kingdom)|Meritorious Service Medal]] in 1935.<ref name="AF2">{{Harvnb|Staunton|2005|p=76}}</ref>

Following the outbreak of the Second World War, Newland was seconded for duties as quartermaster instructor at the [[4th Division (Australia)|4th Division]] headquarters.<ref name="VCs47">{{Harvnb|Gliddon|2000|p=47}}</ref> On 10 May 1940, he assumed his final army appointment as quartermaster, A Branch, at Army Headquarters in Melbourne. He served in this position until August 1941, when he was placed on the retired list with the honorary rank of [[lieutenant colonel]].<ref name="AF2"/>

In retirement, Newland served as Assistant Commissioner of the [[Australian Red Cross|Australian Red Cross Society]] in the [[Northern Territory]] during the later months of 1941.<ref name="AF"/> He joined the inspection staff at the Ammunition Factory in [[Footscray, Victoria|Footscray]] on 2 January 1942. At his home in [[Caulfield, Victoria]], on 19 March 1949, he died suddenly of heart failure at the age of 67.<ref name="ADB"/> He was accorded a funeral with full military honours, and was buried at [[Brighton Cemetery]]. In 1984, Newland's daughter, Dawn, donated her father's medals to the [[Australian War Memorial]] in [[Canberra]],<ref name="VCs47"/> where they currently reside.<ref>{{cite web |url=http://www.awm.gov.au/virtualtour/valour.asp |title=Victoria Crosses at the Memorial |accessdate=28 December 2008 |work=Hall of Valour |publisher=Australian War Memorial}}</ref>
{{clear}}

==Notes==
{{Reflist|30em}}

==References==
* {{Cite book|last=Gliddon|first=Gerald|year=2000|title=Arras & Messines 1917|series=[[VCs of the First World War]]|publication-place=Sparkford, England|publisher=Wrens Park Publishing|isbn=0-905778-61-8|ref=harv}}
* {{Cite book|last=Staunton|first=Anthony|year=2005|title=Victoria Cross: Australia's Finest and the Battles they Fought|publication-place=Prahran, Victoria, Australia|publisher=Hardie Grant Books|isbn=1-74066-288-1|ref=harv}}

==External links==
* {{cite web|url=http://www.diggerhistory.info/pages-vc/newland-vc.htm|archiveurl=https://web.archive.org/web/20080509081506/http://www.diggerhistory.info/pages-vc/newland-vc.htm|archivedate=2008-05-09|title=Newland VC|accessdate=11 December 2008|work=VC Recipients|publisher=Diggerhistory.info}}
* {{cite web|url=http://www.brightoncemetery.com/HistoricInterments/150Names/newlandj.htm|title=Newland, James Earnest|accessdate=11 December 2008|work=150 Years: 150 Lives|publisher=Brightoncemetery.com}}

{{Featured article}}

{{DEFAULTSORT:Newland, James}}
[[Category:1881 births]]
[[Category:1949 deaths]]
[[Category:Australian Army officers]]
[[Category:Australian military personnel of the Second Boer War]]
[[Category:Australian military personnel of World War I]]
[[Category:Australian military personnel of World War II]]
[[Category:Australian police officers]]
[[Category:Australian World War I recipients of the Victoria Cross]]
[[Category:People from Victoria (Australia)]]
[[Category:Recipients of the Meritorious Service Medal (United Kingdom)]]