{{Infobox Journal
| title        = Anthropology of Consciousness
| cover        = 
| editor       = Nicole Torres, Gary Moore
| discipline   = [[consciousness]]
| language     = [[English language|English]]
| abbreviation = AOC
| publisher    = [[American Anthropological Association|AAA]]
| country      = [[United States of America|U.S.A]]
| frequency    = [[semiannual]]
| history      = 1990–present
| openaccess   = 
| license      =
| impact       = 
| impact-year  = 
| website      = [http://www.sacaaa.org/anthropologyofconsciousness.asp]
| link1        = 
| link1-name   = 
| link2        = 
| link2-name   = 
| RSS          = 
| atom         = 
| JSTOR        = 
| OCLC         = 60640215
| LCCN         = 
| CODEN        = 
| ISSN         = 1053-4202
| eISSN        = 
}}
{{Anthropology}}
'''''Anthropology of Consciousness''''' is the primary publication of the Society for the Anthropology of Consciousness, published by [[American Anthropological Association]] (AAA) since 1990.

The current editors, Nicole Torres and Gary Moore, began the position in August 2015.

Prior to joining the AAA, the Society was called "Association for the Anthropological
Study of Consciousness" (AASC), and published the '''''AASC Newsletter''''' and '''''AASC Quarterly'''''<ref>{{cite journal | title = From the editor | author = | journal = Anthropology of Consciousness | date = March–June 1990 | volume = 1 | issue = 1-2 | page = 3 | doi = 10.1525/ac.1990.1.1-2.3.1 }}</ref> and earlier newsletters were also published.<ref>{{cite journal | url = http://www.sacaaa.org/history.asp| title = Boulders in the Stream | author = Stephan A. Schwartz | journal = Journal of the Society for the Anthropology of Consciousness | date = March–June 2001}}</ref>

==Access==
The journal is available online through [[AnthroSource]], and abstracted in the following journals or CD-ROM services.
* [[Abstracts in Anthropology]], from Volume 6, 1995.
* [[Anthropological Literature]], from Volume 6, 1995.
* [[Exceptional Human Experience]], from Volume 1, 1990 (selective).
* [[Sociological Abstracts]], from Volume 6, 1995.

== References ==
{{reflist}}

== External links ==
* [http://sacaaa.org/ Society for the Anthropology of Consciousness]
* [https://web.archive.org/web/20110608072327/http://www.wiley.com/bw/journal.asp?ref=1053-4202 Wiley journal entry]
* [http://www.anthrosource.net/loi/ac AnthroSource entry]

[[Category:Publications established in 1990]]
[[Category:English-language journals]]
[[Category:Biannual journals]]


{{anthropology-journal-stub}}