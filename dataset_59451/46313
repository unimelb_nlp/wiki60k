{{refimprove|date=February 2013}}
{{Infobox music festival
| name =Audiotistic Music Festival
| image =The Rootslyrics at Audiotistic 2009.jpg
| caption =Audiotistic 2009
| location =[[Southern California]], USA
| years_active =1997 to 2003, 2009 to 2011, 2013-present
| founders =Meelo Solis, [[Insomniac Events]] (recent owner)
| dates =
| genre = [[Electronic dance music]], [[rap]] and [[hip hop music|hip hop]], [[drum & bass]], [[dubstep]], etc.
| attendance =
| capacity =
| website =[http://audiotisticfestival.com/ Official website]
}}
'''Audiotistic Music Festival''' is a music festival that occurred annually from 1997 to 2003, and from 2009 to 2011. It has been held in [[southern California]], USA, in various venues, such as the National Orange Show Events Center in [[San Bernardino, California|San Bernardino]], the [[Los Angeles Memorial Sports Arena|L.A. Sports Arena]], and the [[Long Beach Convention and Entertainment Center|Long Beach Convention Center]]. Audiotistic is known for sporting a diverse assortment of genres, ranging from rap and hip hop to electronic genres drum & bass and dubstep.

==History==

Audiotistic Music Festival began in 1997 from promoter Meelo Solis. He ran it with his brother Ben, and investment bankers Darin Feinstein and Bevan Cooney, with U.S. Concerts.<ref name="articles.latimes">http://articles.latimes.com/2002/apr/11/news/wk-weingarten11</ref> It began as a mostly techno, strictly underground event. After failing to hold the event from 2004-2008, Solis teamed up with Insomniac CEO Pasquale Rotella to bring Audiotistic back to Southern California. The first Audiotistic Festival drew in 2,500 people.<ref name="articles.latimes" /> In 2001, the festival drew in 15,000; the next year it packed 35,000 into the National Orange Show Events Center. Despite the size of the crowd, police reported no incidents, and fans seemed remarkably controlled.<ref name="articles.latimes_a">http://articles.latimes.com/2002/apr/15/entertainment/et-baltin15</ref> By 2010, the festival was drawing in 25,000.<ref name="latimesblogs.latimes">http://latimesblogs.latimes.com/lanow/2010/07/san-bernardino-police-arrest-more-than-two-dozen-at-rave.html</ref> The festival drew a smaller number in 2011 totaling 19,000 but was lauded as a success.{{citation needed|date=February 2013}}

In 2002, in addition to its music lineup, Audiotistic tried to plug into [[Los Angeles]] [[skateboard]] culture by erecting a makeshift skate park inside the Orange Center's pavilion that was free to use for anyone willing to lug their board along. The park was designed by pro skater Chad Muska, whose company, Circa Footwear, also hosted a $5,000 best trick contest at the event.<ref name="articles.latimes" />

==Controversies==

Following the death of a 15-year-old girl at Insomniac's [[Electric Daisy Carnival]] in [[Los Angeles]] in 2010,<ref name="latimes">http://www.latimes.com/news/local/la-me-rave-death-20100630,0,1484205.story</ref> Insomniac enacted a new 18 & over policy for all shows within the United States.<ref name="insomniac">{{cite web|url=http://insomniac.com/newsDetails.php?news%3D10 |title=Archived copy |accessdate=2013-01-25 |deadurl=yes |archiveurl=https://web.archive.org/web/20130303163000/http://insomniac.com/newsDetails.php?news=10 |archivedate=2013-03-03 |df= }}</ref> Despite the increase in average age of attendees at the event, [[San Bernardino, California|San Bernardino]] police still arrested more than two dozen partygoers. Twenty-one people were arrested on suspicion of felony drug and other offenses, and several more were arrested on suspicion of misdemeanor crimes, police said.<ref name="latimesblogs.latimes" /> Similarly, in 2011, 43 arrests were reported. Of those, 35 involved possession or sale of illegal narcotics, according to the police department.<ref name="latimesblogs.latimes_a">http://latimesblogs.latimes.com/music_blog/2011/08/insomniacs-audiotistic-festival-explodes-without-any-major-blow-ups.html</ref>

On May 21, 2012, Rotella announced via Twitter that there would be no Audiotistic that summer, and that it would not come back until 2013.<ref name="globaldanceelectronichq">{{cite web|url=http://www.globaldanceelectronichq.com/2012/05/audiotistic-2012-not-happening-till.html |title=Archived copy |accessdate=2013-01-25 |deadurl=yes |archiveurl=https://web.archive.org/web/20120606061928/http://www.globaldanceelectronichq.com:80/2012/05/audiotistic-2012-not-happening-till.html |archivedate=2012-06-06 |df= }}</ref>

==Line-ups==

===2002===
Audiotistic featured [[OutKast]] and the Roots. Along with leading alternative hip hop artists such as Oakland's [[Blackalicious]] and Black Star, and turntablists including QBert and DJ Swamp, Audiotistic also showcased techno and its numerous offshoots.<ref name="articles.latimes" /> Other acts included Mos Def, King Britt, Talib Kweli, house DJ Roy Davis Jr., trance DJ Dan. On the drum-and-bass stage, Craze, DJ Markie and MC Skibadee warmed up the crowd for Roni Size and Dynamite with a blend of fast-paced beats and Jamaican dance-hall vocals.<ref name="articles.latimes_a" />

===2003===
Headliners featured Nas, Big Boi (of OutKast) and the [[Chemical Brothers]]. Other acts included Talib Kweli, King Britt, Sandra Collins, Blackalicious and Donald Glaude. DJ King Britt delivered a mix of down-tempo, jazzy grooves, while hip hop act Blackalicious also stood out for its blend of funk hip-hop and positive beats.<ref name="articles.latimes_b">http://articles.latimes.com/2003/oct/20/entertainment/et-baltin20</ref>

===2009===
{| class="wikitable"
|-
! Progressive Orange Pavilion !! Electro Citrus Building !! Open Air Hip Hop Stage !! Drum & Bass
|-
| David Delano & Modern Romance, Joaquin Bamaca, Lenny Vega, DJ Micro, Donald Glaude, Menno De Jone, Thrillseekers, Above & Beyond (live featuring Zoe Johnston) || Kuru, DJ Sin, Thee Mike B, Villains, DJ Funk, Flosstradamus, Junior Sanchez, Does It Offend You, Yeah? (DJ Set), Chromeo (live), Armand Van Helden, Le Castlevania, Kill The Noise || Peepshow, Far East Movement, Luckyiam, Them Jeans, Amanda Blank, The Cool Kids, Reflection Eternal (featuring Talib Kweli & DJ Hi-Tek), Z Trip, The Roots || Subflo, Hazen, Raw, AAron Lacrate, Shimon, Mike Relm, Goldie, Ed Rush & Optical, Lemonde and Dillinja
|}{{citation needed|date=February 2013}}

===2010===
{| class="wikitable"
|-
! The Boombox !! Speaker Temple !! Sound Arcade !! Bass Frequency
|-
| DJ Sin, Eskmo, The Cool Kids, N.A.S.A., Talib Tweli, Nick Catchdubs, Bassnectar, Kid Cudi, Rusko, Daedelus, Nosaj Thing || Lenny V., Sonic C, Chris Lake, Donald Glaude & Kill The Noise, Wippenberg, Cosmic Gate, Marco V || We A.R.E., Dances With White Girls, Flosstradamus, AC Slater, Harvard Bass, Riva Starr, [[A-Trak]], Treasure Fingers || Dumbsteppaz, Camo UFO's, Star Eyes, Breakage, Craze, Nero, Ed Rush & Optical, TC
|}

===2011===
[[Wolfgang Gartner]] filled in for [[Sebastian Ingrosso]], who had to cancel his Audiotistic appearance at the last minute, due to him needing to receive minor surgery.<ref name="electrodancemedia">http://electrodancemedia.com/blog/hip-hop-and-dance-music-join-forces-for-audiotistic{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>

{| class="wikitable"
|-
! Treble Frequency !! Speaker Temple !! Bass Frequency !! The Boombox
|-
| Crunch Theory, Zoo Brazil, Morten Breum, Bobby Burns, [[Bingo Players]], [[Chris Lake]], [[Porter Robinson]], [[Flux Pavilion]] & [[Doctor P]], [[Diplo (DJ)|Diplo]], [[Crystal Castles (band)|Crystal Castles]], [[Wolfgang Gartner]] || Norin & Rad, Menno De Jong, Marcel Woods, Jaytech, [[Mat Zo]], Arty, Super8 & Tab, Showtek || Deco, Fury, Flinch, [[Plastician]], [[Ed Rush]] & [[Optical (artist)|Optical]], Lazaro Casanova, Bobby Burns, [[Benga (musician)|Benga]], [[Datsik (musician)|Datsik]], [[DJ Fresh]] || DJ Sin, King Fantastic, Thee Mike B, Araabmuzik, Nick Catchdubs, Hoodie Allen, Chiddy Bang, AC Slater, Daedelus, The Cool Kids, Craze, Lil B, Major Lazer
|}

==See also==
*[[List of electronic music festivals]]

==References==
{{reflist}}

==External links==
{{Commons category|Audiotistic Music Festival}}
* [http://audiotisticfestival.com/ Official website]
* [http://www.electricdaisycarnival.com Electric Daisy Carnival Official website]
* [http://www.insomniacevents.com Insomniac Events]

{{Insomniac Events}}
{{Electronic music festival}}
[[Category:Music festivals established in 1997]] 
[[Category:Rock festivals in the United States]]
[[Category:Electronic music festivals in the United States]]