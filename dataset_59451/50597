{{Infobox journal
| title = Artibus Asiae
| cover = [[File:Cover page of Artibus Asiae.jpg|200px]]
| editor = [[Amy McNair]]
| discipline = [[Art]], [[archeology]]
| publisher = [[Museum Rietberg Zurich]]
| country = Switzerland
| frequency = Biannual
| history = 1925–present
| openaccess = [[Delayed open access journal|Delayed]] (after 5 years)
| website = http://www.artibusasiae.com
| JSTOR = 00043648
| ISSN = 0004-3648
| OCLC = 61966661
}}
'''''Artibus Asiae''''' is a biannual [[academic journal]] specialising in the [[art]]s and [[archaeology]] of Asia. Along with the ''[[Ostasiatische Zeitschrift]]'' (founded in 1912) it was one of the most successful journals in its field in the German-speaking part of Europe.<ref>See Walravens, Hartmut, ''Ostasiatische Zeitschrift (1912–1943), Mitteilungen der Gesellschaft für Ostasiatische Kunst (1926-1943): Bibliographie und Register'', Harrassowitz, Wiesbaden, 2000, XII.</ref> The first number of ''Artibus Asiae'' appeared in 1925. While earlier issues contained articles in German, French and English, today's contributions are mainly in English. ''Artibus Asiae'' is owned and published by the [[Museum Rietberg]] in [[Zurich]]. ''Artibus Asiae'' also published occasional monographs since 1937.

== History ==
The first volume of the journal was published by the Avalun-Verlag Hellerau-Dresden in 1925 and was edited by Carl Hentze (1883–1975) and Alfred Salmony (1890–1958).<ref>For a biography of Salmony see Walravens, Hartmut, "Salmony, Alfred". In: ''Neue Deutsche Biographie'', Band 22, Duncker & Humblot, Berlin, 2005, 386-387. Salmonys editorial role is also mentioned in Li, Weijia, ''China und China-Erfahrung in Leben und Werk von Anna Seghers'', Lang Verlag, Bern, 2010, 33.</ref> The early volumes appeared in four issues each, up to vol. 59. All subsequent volumes were published in two parts. 

The typographer, publisher and later [[editor-in-chief]] Richard Hadl (1876–1944) had worked for the [[Leipzig]]-based publisher ''Drugulin'' as a director since 1922.<ref>''Artibus Asiae'', 1945 (vol. 8 no. 1), pp. i–iii.</ref> ''Drugulin'' was one of the leading publishing houses and known for their wide array of unusual typesets. Hadl established his own publishing house, "Offizin Richard Hadl", in 1926.<ref>Note: The first book in his publishing house was "Schriftproben der Offizin Richard Hadl", Berlin, 1929.</ref> and published five volumes of the journal ''Artibus Asiae''.<ref>"Artibus Asiae" vol. 3, no. 2-3 (1928–1929) was still published by the Avalun-Verlag, but already printed by the Offizin Richard Hadl. From vol. 3, no. 4 through vol. 8, no. 1, Hadl acted as editor-in-chief in Leipzig.</ref>

During the [[Second World War]] all publishing activities were moved to [[Switzerland]] and the journal would only appear irregularly.<ref>Volume 7 appeared in 1937, Volume 8 no. 1 was published in 1940 and Volume 8 no. 2-4 only in 1945.</ref> Vol. 8 no. 1 was the first issue to be published in Switzerland, printed by the ''Kommissionsverlag Braus Riggenbach'' in [[Basel]]. All further volumes were published by ''Artibus Asiae'' in Ascona, where Hadl and his co-worker and publisher, Luise C. Tarabori-Flesch from [[Trier]] had settled in 1938/39. After Hadel had died in 1944,<ref>For Hadl's obituary see ''Artibus Asiae'', 1945 (vol. 8 no. 1), pp. i–iii</ref> Alfred Salmony became editor-in-chief and revived the journal after the war years<ref>Waterbury, Florence, "Alfred Salmony: 1890-1958", in: ''Archives of the Chinese Art Society of America'', 1958 (vol. 12), 9.</ref> until his death in 1958.<ref>Obituaries include http://www.nytimes.com/1993/01/14/obituaries/alexander-soper-88-historian-of-asian-art.html; [[Gustav Ecke|Ecke, Gustav]], "In Memoriam Alfred Salmony", in: ''Ars Orientalis'' 1961 (vol. 4), 453; "Alfred Salomy (1890–1958)", in: ''College Art Journal'', 1958 (vol. 18, no. 1), 77 and Waterbury, Florence, in: ''Asian Perspectives'', 1958 (vol. 2 no. 1), 7.</ref>

''Artibus Asiae'''s link to the current owner, the [[Museum Rietberg]], was established through the museums's former director [[Elsy Leuzinger]], who edited an issue (vol. 20 no. 1, 1957) to commemorate the founding donor of the Museum Rietberg, [[Eduard von der Heydt]].<ref>An issue for von der Heydt's 70st{{Clarify|date=October 2013|reason=Should this be '70th', '71st' or something else? Subtraction from his birth date makes '80th' seem most likely.}} birthday was published in 1962 (vol. 25 no. 1).</ref> In 1985 (from vol. 46 on), the [[Arthur M. Sackler]] Foundation started to sponsor the journal. The [[Museum Rietberg]] was granted a special publication endowment in 1991 (vol. 51) and it henceforth became the owner of both the journal and the monograph series.

== Editors ==
The following persons have served as editors-in-chief of ''Artibus Asiae'':
{| class="wikitable"
|-
! Name !! Place !! Volumes !! Years
|-
| Carl Hentze & Alfred Salmony || Antwerp and Cologne || vol. 1–vol. 4 no. 3 || 1925–1932
|-
| Richard Hadl || Leizpig and Ascona || vol. 4 no. 4–vol. 8 || 1925–1945
|-
| Alfred Salmony || [[Institute of Fine Arts]], [[New York University]] || vols. 9–20 || 1945–1957
|-
| [[Alexander Soper]] || [[Institute of Fine Arts]], [[New York University]] || vols. 21–52 || 1958–1992
|-
| Thomas Lawton || [[Arthur M. Sackler Gallery]], Washington D.C. || vols. 53–61 || 1993–2001
|-
| François Louis || [[Bard Graduate Center]], New York || vol. 62–vol. 68 no. 1 || 2002–2008
|-
| Amy McNair || [[University of Kansas]] || vol. 68 no. 2–present || 2008–present
|}

== Artibus Asiae monographs ==
Longer articles submitted to ''Artibus Asiae'' were often split into parts and published in several numbers of the journal. ''Artibus Asiae'' started to publish monographs on selected topics in 1937 to allow more lengthy contributions to the field.<ref>For a list of published monographs, see http://www.artibusasiae.com/monographs/</ref> They are conceived as a supplemental series to the journal and present a broad range of lavishly illustrated studies.  Early monographs were on topics only remotely related to the arts, such as publications on the Tibetan grammar books Sum cu pa and Rtags kyi ‘ajug<ref>Rol Pai Rdo Rje; Johannes Schubert, ''Tibetische Nationalgrammatik: Das Sum cu pa und Rtags kyi ‘ajug pa'', Artibus Asiae Supplementum vol. 1, 1937.</ref> or on Chinese literature.<ref>Wolfram Eberhard, ''Die Chinesische Novelle des 17.-19. Jahrhunderts: Eine Soziologische Untersuchung'', Artibus Asiae Supplementum vol. 9, 1938</ref> 

== References ==
{{reflist|35em}}

== External links ==
* {{Official website|http://www.artibusasiae.com}}

[[Category:Publications established in 1925]]
[[Category:Visual art journals]]
[[Category:Art history journals]]
[[Category:Asian studies]]
[[Category:Biannual journals]]
[[Category:English-language journals]]