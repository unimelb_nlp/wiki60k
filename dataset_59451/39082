{{Good article}}
{{Use dmy dates|date=July 2012}}
{{Infobox sportsperson
| headercolor = lightblue
| name =  Terry Bywater
| image = Australia v GB in final 8859.JPG
| image_size = 
| caption = Terry Bywater at Gliders & Rollers World Challenge on 21 July 2012
| birth_name = Terrance Bywater
| fullname = 
| nickname = 
| nationality = {{GBR}}
| residence = [[Cleveland, England|Cleveland]], [[North Yorkshire]]
| birth_date = {{birth date and age|1983|02|28|df=yes}}
| birth_place = [[Middlesbrough]], [[England]]
| death_date = 
| death_place = 
| height =  {{convert|1.80|m|ftin}} (2009)
| weight =  {{convert|74|kg|stlb}} (2009)
| website =  <!-- {{URL|www.example.com}} !-->
| country = Great Britain
| sport = [[Wheelchair basketball]]
| event = Men's team
| collegeteam = 
| club = Sheffield Steelers
| team = Bulldogs
| turnedpro = 
| coach = 
| retired = 
| coaching = 
| worlds = 
| regionals = 
| nationals = 
| olympics = 
| paralympics = 
| highestranking = 
| pb = 
|medaltemplates= {{MedalSport |Wheelchair basketball}}
{{MedalCountry | {{GBR}} }}
{{MedalCompetition|[[Paralympic Games]]}}
{{MedalBronze | [[2004 Summer Paralympics|2004 Athens]]|[[Wheelchair basketball at the 2004 Summer Paralympics|Men's Wheelchair basketball]]}}
{{MedalBronze | [[2008 Summer Paralympics|2008 Beijing]]|[[Wheelchair basketball at the 2008 Summer Paralympics|Men's Wheelchair basketball]]}}
}}
'''Terrance "Terry" Bywater'''<ref name="Channel 4 Paralympics">{{cite news|url=http://paralympics.channel4.com/the-athletes/athleteid=483/index.html |title=Terry Bywater&nbsp;— The Athletes&nbsp;— Paralympics |publisher=Channel4 |date=28 February 1983 |accessdate=27 July 2012}}</ref> (born 28 February 1983) is a British [[wheelchair basketball]] player. He participated in the [[2000 Summer Paralympics]], where his team came in fourth place; in the [[2004 Summer Paralympics]], where he won a bronze medal and was the highest scorer for Great Britain; the [[2008 Summer Paralympics]], winning another bronze medal; and the [[2012 Summer Paralympics]], where his team again came in fourth place.<ref name="Paralympics.org.uk">{{cite news|url=http://www.paralympics.org.uk/gb/athletes/terry-bywater |title=Terry Bywater&nbsp;— British Paralympic Association |publisher=Paralympics.org.uk |date=11 June 2012 |accessdate=27 July 2012}}</ref><ref name="BBC-rues"/>

==Personal==
Bywater was born on 28 February 1983 in [[Dormanstown]], [[England]] and currently lives in [[Cleveland, England|Cleveland]], [[North Yorkshire]].<ref name="Paralympics.org.uk"/> {{As of|2009}}, he weighs {{convert|74|kg|stlb}} and is {{convert|1.8|m|ftin}} tall.<ref name="Guardian 2009">{{cite news|url=https://www.theguardian.com/lifeandstyle/2009/jan/11/terry-bywater-paralympic-basketball |title=Basketball: Terry Bywater's Paralympic fitness file &#124; Life and style &#124; The Observer |publisher=Guardian |date= 11 January 2009|accessdate=27 July 2012 |location=London}}</ref> He was born without a [[tibia]] and a [[fibula]] in his left leg, which was amputated when he was two.<ref name="GBWBA">{{cite news|url=http://www.gbwba.org.uk/gbwba/index.cfm/gb-teams/gb-players/gb-men/great-britain-mens-wheelchair-basketball-player-terry-bywater |title=Great Britain Men's wheelchair basketball player Terry Bywater |publisher=British Wheelchair Basketball |date=28 February 1983 |accessdate=27 July 2012}}</ref> He has a son, Benjamin Bywater.<ref name="GBWBA"/>

==Wheelchair basketball==
Bywater began playing wheelchair basketball at the age of 13 at an open day in [[Middlesbrough]] with the Teesside Lions.<ref name="GBWBA"/><ref name="BBC Disability Sport">{{cite news |url=http://www.bbc.co.uk/sport/0/disability-sport/18116485 |title=Terry Bywater column: Paralympic World Cup is a big test for GB |work=BBC Sport|publisher=BBC |date=20 May 2012 |accessdate=27 July 2012}}</ref> He later played with them.<ref name="Paralympics.org.uk"/> After playing for a year, he was selected for the Great Britain Under-23 team and began training in 1993. He made his debut at the Sydney [[2000 Summer Paralympics]],<ref name="BBC Disability Sport"/> and finished fourth.<ref name="Paralympics.org.uk"/> He participated in the [[2008 Summer Paralympics]] in Beijing, where he won a bronze medal, and was the top Great Britain scorer in the event.<ref name="Channel 4 Paralympics"/> He currently plays for the Sheffield Steelers. He once played for [[CD Ilunion|C. D. Fundosa]] in [[Spain]], along with many other European clubs. Bywater returned to England so he could play with the Super League Club Sheffield Steelers throughout the 2011—12 season. He is a [[4.5 point player]].<ref name="GBWBA"/>

His first championships were the 2001/2002 European Championships in [[Amsterdam]], [[Netherlands]], where he finished fourth. In 2002 he went to the World Championships in [[Kitakyushu]] in [[Japan]], where he finished second (silver). He participated in the 2003 European Championships in [[Sassari]], [[Italy]], and won bronze. He competed in the 2005 European Championships in [[Paris]], [[France]], and won silver.<ref name="GBWBA"/> He competed in the 2006 World Championships in Amsterdam and was fifth place, and in 2007 he participated in the European Championships in [[Wetzlar]], Netherlands, and received a silver medal. Two years later, he won bronze in the European Championships of [[Adana]] in [[Turkey]]. In 2010, for the first time, he participated in the [[World Wheelchair Basketball Championships]] in [[Birmingham]], and was fifth place.<ref name="GBWBA"/> He won gold in the 2011 European Championships in [[Nazareth]], Israel.<ref name="GBWBA"/><ref>{{cite news|url=http://www.bbc.co.uk/sport/0/disability-sport/14961518 |title=European glory for GB men's wheelchair basketball team |publisher=BBC|work=BBC Sport |date=17 September 2011 |accessdate=28 July 2012}}</ref> At the 2012 Summer Olympics in London, the wheelchair basketball team lost to the [[United States at the 2012 Summer Paralympics|United States]], and finished in fourth position, after losing to [[Canada at the 2012 Summer Paralympics|Canada]], missing out of the finals. He said that not winning a medal at the Paralympics was the "worst moment" of his career.<ref name="BBC-rues">{{cite news|url=http://www.bbc.co.uk/sport/0/disability-sport/19536101|title=Paralympics 2012: Terry Bywater rues 'worst moment' of career|publisher=BBC|work=BBC Sport|date=9 September 2012|accessdate=16 September 2012}}</ref><ref name="BBCS2">{{cite news|url=http://www.bbc.co.uk/sport/0/disability-sport/19533038|title=Paralympics 2012: GB miss out on wheelchair basketball bronze|publisher=BBC|work=BBC Sport|date=8 September 2012|accessdate=17 September 2012}}</ref>

==References==
<references />
{{commons category|Terry Bywater}}

{{DEFAULTSORT:Bywater, Terry}}
[[Category:1983 births]]
[[Category:Living people]]
[[Category:British men's wheelchair basketball players]]
[[Category:People from Middlesbrough]]
[[Category:Paralympic bronze medalists for Great Britain]]
[[Category:Wheelchair basketball players at the 2012 Summer Paralympics]]
[[Category:Paralympic wheelchair basketball players of Great Britain]]
[[Category:Wheelchair basketball players at the 2000 Summer Paralympics]]
[[Category:Wheelchair basketball players at the 2008 Summer Paralympics]]
[[Category:Wheelchair basketball players at the 2004 Summer Paralympics]]
[[Category:Medalists at the 2004 Summer Paralympics]]
[[Category:Medalists at the 2008 Summer Paralympics]]