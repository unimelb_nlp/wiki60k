{{stack begin}}
{{Taxobox
| image = Boletus curtisii 17378.jpg
| image_caption =
| image_width = 234px
| regnum = [[Fungi]]
| divisio = [[Basidiomycota]]
| classis = [[Agaricomycetes]]
| ordo = [[Boletales]]
| familia = [[Boletaceae]]
| genus = ''[[Boletus]]''
| species = '''''B. curtisii'''''
| binomial = ''Boletus curtisii''
| binomial_authority = [[Berk.]] (1853)
| synonyms_ref = <ref name="urlMycoBank: Boletus curtisii"/>
| synonyms = ''Ceriomyces curtisii'' <small>(Berk.) [[William Alphonso Murrill|Murrill]] (1909)</small><br>
''Pulveroboletus curtisii'' <small>(Berk.) [[Rolf Singer|Singer]] (1947)</small>
}}
{{mycomorphbox
| name = ''Boletus curtisii''
| whichGills = adnate
| capShape = convex
| hymeniumType = pores
| stipeCharacter = bare
| ecologicalType = mycorrhizal
| sporePrintColor = olive-brown
| howEdible = edible
}}
{{stack end}}
'''''Boletus curtisii''''' is a species of [[fungus]] in the [[Boletaceae]] family. It produces small- to medium-sized [[basidiocarp|fruit bodies]] ([[mushroom]]s) with a convex [[pileus (mycology)|cap]] up to {{convert|9.5|cm|in|abbr=on}} wide atop a slender [[stipe (mycology)|stem]] that can reach a length of {{convert|12|cm|in|abbr=on}}. In young specimens, the cap and stem are bright golden yellow, although the color dulls to brownish when old. Both the stem and cap are slimy or sticky when young. On the underside of the cap are small circular to angular pores. The mushroom is [[edible mushroom|edible]], but not appealing. It is found in eastern and southern North America, where it grows in a [[mycorrhiza]]l association with [[hardwood]] and [[conifer]] trees. Once [[classification (biology)|classified]] as a species of ''[[Pulveroboletus]]'', the yellow color of ''B.&nbsp;curtisii'' is a result of [[pigment]]s chemically distinct from those responsible for the yellow coloring of ''Pulveroboletus''.

==Taxonomy==
The species was first [[species description|described]] scientifically by English mycologist [[Miles Joseph Berkeley]] in 1853.<ref name="Berkeley 1853"/> The [[botanical name|specific epithet]] ''curtisii'' honors [[Moses Ashley Curtis]],<ref name="Metzler 1992"/> who collected the [[type (biology)|type]] material from [[South Carolina]].<ref name="Berkeley 1853"/>

American mycologist [[William Murrill]] called it ''Ceriomyces curtisii'' in 1909,<ref name="Murrill 1909"/> but ''Ceriomyces'' (as defined by Murrill in 1909) has since been subsumed into ''[[Boletus]]''.<ref name="Kirk 2008"/> In his 1947 [[monograph]] on boletes of [[Florida]], [[Rolf Singer]] transferred the species to the genus ''[[Pulveroboletus]]'', and made it the [[type species|type]] of his newly described [[section (biology)|section]] ''Cartilaginei'', which featured species with a glutinous or sticky stem, and a leather-colored to brownish [[hymenophore]].<ref name="Singer 1947"/> Species in ''Pulveroboletus'' are characterized by the presence of pigments based on the chemical structure of [[pulvinic acid]], a yellow-orange compound found in some species of [[Boletales]].<ref name="Gill 1987"/> The pigments responsible for the color of ''B.&nbsp;curtisii'' are, however, entirely different from the pulvinic acid compounds found in ''Pulveroboletus'' species, which invalidates the [[chemotaxonomy|chemotaxonomical]] rational for generic placement in ''Pulveroboletus''.<ref name="Bröckelmann 2004"/> [[Otto Kuntze]] once placed the species in ''[[Suillus]]'', but it lacks the [[partial veil]] and glandular dots associated with that genus.<ref name="Kuo 2010"/> [[William Chambers Coker]] and Alma Beers considered [[Charles Horton Peck]]'s ''Boletus inflexus'' (described from [[New York (state)|New York]] in 1895<ref name="Peck 1895"/>) as well as [[Henry Curtis Beardslee]]'s 1915 ''B.&nbsp;carolinensis'' to be the same species as ''B.&nbsp;curtisii''.<ref name="Coker 1943"/> Coker and Beer's suggested synonymy, however, is not recognized by the [[Taxonomy (biology)|taxonomical]] authorities [[MycoBank]] or [[Index Fungorum]].<ref name="urlMycoBank: Boletus curtisii"/><ref name="urlFungorum"/>

[[Wally Snell]] once considered ''[[Boletus carolinensis]]'' to be the sames species as ''B.&nbsp;curtisii''. He claimed that the former species was then considered distinct from the latter by virtue of an even, instead of [[wikt:reticulate|reticulate]] (netlike) stem, although they were otherwise quite similar in appearance and spore size and shape.<ref name="Snell 1934"/> Snell explained that although neither the English nor the Latin text of Berkeley's original description mentioned a reticulated stem, a later (1872) description by Berkeley characterized the stem as ''reticulato''.<ref name="Berkeley 1872"/> Snell thought that this might have been an error in transcription, or an error in the species account, as [[herbarium]] specimens that he had examined lacked this feature.<ref name="Snell 1934"/> He changed his mind a couple of years later, when he found a small amount of reticulation in material collected by Peck.<ref name="Snell 1936"/>

==Description==
[[File:Boletus curtisii 173783.jpg|thumb|left|View of the cap underside, showing the inrolled margin, small white pores, and droplets of golden yellow liquid.]]
The [[Pileus (mycology)|cap]] is {{convert|3|–|9.5|cm|in|abbr=on}} wide, and initially obtuse to convex in shape before becoming broadly convex to nearly flat when mature. The cap margin has a narrow band of sterile tissue that in young fruit bodies is curved inwards. The cap surface is somewhat sticky when fresh, smooth, and bright yellow to orange-yellow, sometimes with brownish tints or whitish areas in age. The whitish [[trama (mycology)|flesh]] does not change color when exposed to air, and has no distinctive odor or taste. On the underside of the cap, the pore surface is initially whitish to [[buff (color)|buff]] or pale yellow, but becomes duller and darker at maturity, often depressed near the stem in age. Unlike some other [[bolete]]s, ''B.&nbsp;curtisii'' does not turn blue when bruised or injured. The pores are circular to angular, and there are 2–3 per mm; the tubes are 6–12&nbsp;mm deep.<ref name="Bessette 2001"/> Young fruit bodies usually have droplets of golden yellow liquid on the pore surface (sometimes abundantly so), although this is rarely observed in older specimens.<ref name="Snell 1970"/>

The stem is {{convert|6|–|12|cm|in|abbr=on}} long, {{convert|0.6|–|1.3|cm|in|1|abbr=on}} thick, and roughly equal in width throughout. Its surface is sticky and glutinous when fresh, somewhat scurfy near the apex (covered with loose scales) but smooth below. It is pale yellow to yellow down to the base, which is sheathed with a cottony white [[mycelium]]. The stem can be either solid or hollow. The mushroom lacks a [[partial veil]] and a [[annulus (mycology)|ring]]. The [[spore print]] is olive-brown.<ref name="Bessette 2001"/> The mushroom is [[edible mushroom|edible]], but not appealing.<ref name="Metzler 1992"/>
[[File:Boletus curtisii 174402.jpg|thumb|right|The smooth, yellowish spores have an ellipsoid to somewhat ventricose shape. 1000x magnification; divisions are 1&nbsp;μm]]
[[Spore]]s are 9.5–17 by 4–6&nbsp;[[micrometre|μm]], [[wikt:ellipsoid|ellipsoid]] to somewhat [[wikt:ventricose|ventricose]] (inflated on one side), smooth, and yellowish.<ref name="Bessette 2001"/> The [[basidia]] (spore-bearing cells) are four-spored, measuring 25–32 by 6–10.8&nbsp;μm. The [[cystidia]] lining the inside of the tubes are shaped like [[seta#Fungal setae|setae]] (i.e., thick walled and thorn-like) and have dimensions of 43–86 by 6.5–11&nbsp;μm. All [[hypha]]e lack [[clamp connection]]s.<ref name="Singer 1947"/>

===Similar species===
''[[Retiboletus retipes]]'' is somewhat similar in appearance, but is distinguished by a more orange to orange-yellow color, a lack of sliminess, and a distinctly reticulated stalk.<ref name="Metzler 1992"/>

==Habitat and distribution==
The fruit bodies of ''B.&nbsp;curtisii'' grow singly, scattered, or in small groups on the ground in [[coniferous forest|coniferous]] or [[mixed forest|mixed woods]], often with pines. Fruit bodies generally appear from August to November. The [[range (biology)|geographical distribution]] of the fungus is limited to eastern and southern North America. In the United States, it occurs from [[New England]] south to [[Florida]], and west to [[Texas]].<ref name="Bessette 2001"/> The species was newly reported from Mexico in 2001.<ref name="Jiménez 2001"/>

==Pigments==
The fruit bodies of ''Boletus curtisii'' contain a unique series of [[derivative (chemistry)|derivatives]] of the molecule [[canthin-6-one]].  Before this discovery, canthin-6-one [[alkaloid]]s were only known from higher plants.<ref name="Bröckelmann 2004"/> Among the canthin-6-one derivatives are the [[pigment]]s that give the mushroom its bright yellow color, including two [[optically active]] [[sulfoxide]]s named curtisin and 9-deoxycurtisin.<ref name="Jiang 2011"/> Spraying a fruit body with [[methanol]] causes the pigments to dissolve and makes the color wash away—a phenomenon unknown in other bolete mushrooms.<ref name="Bröckelmann 2004"/> Additionally, spraying fruit bodies with [[acetone]] results in a green-yellow [[fluorescence]] visible in daylight.

==See also==
{{Portal|Fungi}}
*[[List of Boletus species|List of ''Boletus'' species]]
*[[List of North American boletes]]

==References==
{{Reflist|colwidth=30em|refs=

<ref name="Berkeley 1853">{{cite journal |vauthors=Berkeley MJ, Curtis MA |title=Centuries of North American fungi |journal=Annals and Magazine of Natural History |year=1853 |volume=12 |issue=2 |pages=417–35 (see p. 429) |url=http://biodiversitylibrary.org/page/22191863}}</ref>

<ref name="Berkeley 1872">{{cite journal |author=Berkeley MJ. |title=Notices of North American fungi |journal=Grevillea |volume=1 |issue=3 |year=1872 |page=35 |url=http://www.cybertruffle.org.uk/cyberliber/59649/0001/003/0035.htm}}</ref>

<ref name="Bessette 2001">{{cite book |title=North American Boletes |vauthors=Bessette AE, Roody WC, Bessette AR |year=2000 |publisher=Syracuse University Press |location=Syracuse, New York |pages=106–7 |isbn=978-0-8156-0588-1}}</ref>

<ref name="Bröckelmann 2004">{{cite journal |vauthors=Bröckelmann MG, Dasenbrook J, Steffan B, Steglich W, Wang YK, Raabe G, Fleischhauer A |title=An unusual series of thiomethylated canthin-6-ones from the North American mushroom ''Boletus curtisii'' |journal=European Journal of Organic Chemistry |year=2004 |volume=2004 |issue=23 |pages=4856–63 |doi=10.1002/ejoc.200400519 |url=http://ims.sxu.edu.cn/docs/20100614075956819186.pdf |format=PDF}}</ref>

<ref name="Coker 1943">{{Cite book |vauthors=Coker WC, Beers A |title=The Boleti of North Carolina |edition=reprint |publisher=Courier Dover Publications |location=New York, New York |year=1972 |origyear=1943 |url=https://books.google.com/books?id=A3E9EwWpoBgC&pg=PA49 |isbn=0-486-20377-8}}</ref>

<ref name="Gill 1987">{{cite journal |vauthors=Gill M, Steglish W |year=1987 |title=Pigments of fungi (Macromycetes) |journal=Progress in the Chemistry of Organic Natural Products |volume=51 |pages=1–17 |pmid=3315906 |doi=10.1007/978-3-7091-6971-1_1}}</ref>

<ref name="Jiang 2011">{{cite journal |vauthors=((Jiand M-K)), Feng T, ((Liu J-K)) |title=N-containing compounds of macromycetes |journal=Natural Product Reports |year=2011 |volume=28 |pages=783–808 |doi=10.1039/C0NP00006J}}</ref>

<ref name="Jiménez 2001">{{cite journal |vauthors=Jiménez JG, Ocañas FG |title=Conocimiento de los hongos de la familia Boletaceae de México |trans_title=Knowledge of the fungi family Boletaceae of Mexico |journal=Ciencia UANL |year=2001 |volume=4 |issue=3 |pages=336–44 |language=Spanish |url=http://eprints.uanl.mx/411/1/conocim_hongos.pdf |issn=1405-9177}}</ref>

<ref name="Kirk 2008">{{cite book |vauthors=Kirk PM, Cannon PF, Minter DW, Stalpers JA |title=Dictionary of the Fungi |edition=10th |publisher=CAB International |location=Wallingford, UK |year=2008 |page=128 |isbn=978-0-85199-826-8}}</ref>

<ref name="Kuo 2010">{{cite book |vauthors=Kuo M, Methven A |title=100 Cool Mushrooms |publisher=University of Michigan Press |location=Ann Arbor, Michigan |year=2010 |page=31 |isbn=978-0-472-03417-8 |url=https://books.google.com/books?id=jhll7DJcnrwC&pg=PA31}}</ref>

<ref name="Metzler 1992">{{cite book |vauthors=Metzler V, Metzler S |title=Texas Mushrooms: A Field Guide |publisher=University of Texas Press |location=Austin, Texas |year=1992 |page=217 |isbn=0-292-75125-7 |url=https://books.google.com/books?id=HRtfvVigMmsC&pg=PA217}}</ref>

<ref name="Murrill 1909">{{cite journal |author=Murrill WA. |title=The Boletaceae of North America: II |journal=Mycologia |year=1909 |volume=1 |issue=4 |pages=140–58 |jstor=3753125 |doi=10.2307/3753125}}</ref>

<ref name="Peck 1895">{{cite journal |author=Peck CH. |title=New species of fungi |journal=Bulletin of the Torrey Botanical Club |year=1895 |volume=22 |pages=198–211 (see p. 207) |url=http://biodiversitylibrary.org/page/709006 |doi=10.2307/2478162}}</ref>

<ref name="Singer 1947">{{cite journal |author=Singer R. |title=The Boletoideae of Florida with notes on extralimital species III |journal=American Midland Naturalist |year=1947 |volume=37 |issue=1 |pages=1–135 (see pp. 18–19) |jstor=2421647 |doi=10.2307/2421647}}</ref>

<ref name="Snell 1934">{{cite journal |author=Snell WH. |title=Notes on Boletes. III |journal=Mycologia |year=1934 |volume=26 |issue=4 |pages=348–59 |jstor=3754231 |url=http://www.cybertruffle.org.uk/cyberliber/59350/0026/004/0348.htm |doi=10.2307/3754231}}</ref>

<ref name="Snell 1936">{{cite journal|author=Snell WH. |title=Notes on Boletes. IV |journal=Mycologia |year=1936 |volume=28 |issue=1 |pages=13–23 |jstor=3754063 |url=http://www.cybertruffle.org.uk/cyberliber/59350/0028/001/0013.htm |doi=10.2307/3754063}}</ref>

<ref name="Snell 1970">{{cite book |vauthors=Snell WH, Dick EA |title=The Boleti of Northeastern North America |publisher=J. Cramer |location=Lehre, Germany |page=62}}</ref>

<ref name="urlFungorum">{{cite web |title=Homotypic synonyms: ''Boletus curtisii'' Berk. |url=http://www.indexfungorum.org/Names/HomoSpecies.asp?RecordID=210465 |publisher=[[Index Fungorum]]. [[CAB International]] |accessdate=2012-08-21}}</ref>

<ref name="urlMycoBank: Boletus curtisii">{{cite web |title=''Boletus curtisii'' Berk. 1853 |url=http://www.mycobank.org/MycoTaxo.aspx?Link=T&Rec=210465 |publisher=[[MycoBank]]. International Mycological Association |accessdate=2011-10-14}}</ref>

}}

==External links==
*{{IndexFungorum|210465}}
*[http://www.mushroomexpert.com/boletus_curtisii.html Mushroom Expert] Description and images
*[http://mushroomobserver.org/observer/observation_search?pattern=Boletus+curtisii Mushroom Observer] Images

{{Good article}}

[[Category:Boletus|curtisii]]
[[Category:Fungi described in 1853]]
[[Category:Fungi of North America]]
[[Category:Edible fungi]]
[[Category:Taxa named by Miles Joseph Berkeley]]