{{Infobox military person
| name             = John Basilone
| birth_date       = {{Birth date|1916|11|4}}
| death_date       = {{Death date and age|1945|2|19|1916|11|4}}{{KIA}}
| birth_place      = [[Buffalo, New York]], U.S.<!--born in Buffalo, New York, where his family lived at the time; family moved back to Raritan, New Jersey, in 1918 and he was raised there--><ref name="Brady" /><ref name="NJ.com" />
| death_place      = [[Iwo Jima]], [[Japan]]
| placeofburial    = [[Arlington National Cemetery]]
| placeofburial_label = Place of burial
| image            = [[File:John Basilon Medal of Honor 1943.png|205px]] 
| caption          = John Basilone receiving the Medal of Honor in 1943
| nickname         = "Manila"
| spouse           = Lena Mae Riggi (1944–1945)
| allegiance       = {{flag|United States of America|1912}}
| branch           = {{flag|United States Army}}<br />{{flag|United States Marine Corps}}
| serviceyears     = 1934–37, 1940-45
| rank             = [[File:USMC-E7.svg|25px]] [[Gunnery Sergeant]]
| commands         = 
| unit             = USA: D Company, [[16th Infantry Regiment (United States)|16th Infantry]]<br />USA: D Company, [[31st Infantry Regiment (United States)|31st Infantry]]<br />USMC: D Company, [[7th Marine Regiment (United States)|7th Marines]] <br />USMC: C Company, [[27th Marine Regiment (United States)|27th Marines]]
| battles          = [[World War II]]
* [[Guadalcanal Campaign]]
* [[Battle of Iwo Jima]]
| awards           = [[Medal of Honor]]<br />[[Navy Cross (United States)|Navy Cross]]<br />[[Purple Heart Medal]]
| laterwork        = 
}}

'''John Basilone''' (November 4, 1916 – February 19, 1945) was a [[United States Marine Corps]] [[Gunnery Sergeant]] who was [[killed in action]] during [[World War II]]. He received the [[Medal of Honor]] for heroism above and beyond the call of duty during the  [[Guadalcanal Campaign|Battle of Guadalcanal]] and the [[Navy Cross (United States)|Navy Cross]] posthumously for extraordinary heroism during the [[Battle of Iwo Jima]]. He was the only enlisted Marine to receive both of these decorations in World War II.

He enlisted in the Marine Corps on June 3, 1940, after serving three years in the [[United States Army]] with duty in the [[Philippines]]. He was deployed to [[Guantánamo Bay]], [[Cuba]], and in August 1942, he took part in the invasion of [[Guadalcanal]]. In October, he, and two other Marines manning two other machine guns, held off approximately 3,000 Japanese soldiers until the attack ceased. In February 1945, he was killed in action on the first day of the invasion of Iwo Jima, after he single handedly destroyed an enemy blockhouse and led a Marine tank under fire safely through a minefield.

He has received many honors including being the namesake for streets, military locations, and two [[United States Navy]] destroyers.

==Early life==
Basilone was born in his parents' home on November 4, 1916 in [[Buffalo, New York]],<ref name="NJ.com" /> the sixth of ten children. His first five siblings were born in [[Raritan, New Jersey]], before the family moved to Buffalo when John was born; they returned to Raritan in 1918.<ref name="Brady">[https://books.google.com/books?id=hwZXnM6jNSgC&q=Buffalo#v=snippet&q=Buffalo&f=false Brady, 2010]</ref> His father, Salvatore Basilone, emigrated from {{w|Colle Sannita}}, in the province of {{w|Benevento}}, Italy in 1903 and settled in Raritan. His mother, Dora Bencivenga, was born in 1889 and grew up in [[Manville, New Jersey]], but her parents, Carlo and Catrina, also came from Benevento. His parents met at a church gathering and married three years later. Basilone grew up in the nearby Raritan Town (now Borough of Raritan) where he attended St. Bernard Parochial School. After completing middle school at the age of 15, he dropped out prior to attending high school.<ref name=Brady79-80>Brady, 2010, pp. 79-80</ref>

==Military service==

Basilone worked as a golf caddy for the local country club before joining the military. He enlisted in the United States Army July 1934<ref name=Brady80>Brady, 2010, p. 80</ref> and completed his three-year enlistment with service in the [[Philippines]], where he was a champion [[Boxing|boxer]].<ref>[[United States Senate|U.S. Senate]].  [http://www.gpo.gov/fdsys/pkg/CREC-2005-11-18/pdf/CREC-2005-11-18-pt1-PgS13334-2.pdf "Congressional Record"], November 18, 2005, S13334-5.  Accessed on September 02, 2010.</ref> In the Army, Basilone was initially assigned to the [[16th Infantry Regiment (United States)|16th Infantry]] at [[Fort Jay]], before being discharged for a day and reenlisting and being assigned to the [[31st Infantry Regiment (United States)|31st Infantry]].<ref>{{cite book |last=Tatum |first=Chuck |title=Red Blood, Black Sand: Fighting Alongside John Basilone from Boot Camp to Iwo Jima |url=https://books.google.com/books?id=hX2CgJkYFN0C&pg=PT66&dq=John+Basilone+%2216th+Infantry%22&hl=en&sa=X&ei=-vcgUdvEH6GE2gXh2IDoDg&ved=0CDMQ6AEwAQ#v=onepage&q=John%20Basilone%20%2231st%20Infantry%22&f=false |accessdate=17 February 2013 |year=2012 |publisher=Penguin |isbn=9781101585061 |page=66}}</ref><ref>{{cite journal |last1=Alexander |first1=Colonel Joseph H. |year=2010 |title=Real Marines Behind HBO's The Pacific |journal=[[Naval History (magazine)|Naval History]] |volume=24 |issue=2 |pages=26–27 |publisher=[[United States Naval Institute]] |url=http://www.mydigitalpublication.com/publication/?i=32367&p=28 |accessdate=17 February 2013}}</ref>

After he was released from active duty, he returned home and worked as a truck driver in [[Reisterstown, Maryland]].<ref name=whoswho>{{Cite web |accessdate=October 20, 2007 |url=http://www.tecom.usmc.mil/HD/Whos_Who/Basilone_J.htm |title=Gunnery Sergeant John Basilone, USMC |work=Who's Who in Marine Corps History |publisher=History Division, United States Marine Corps}}</ref> After driving trucks for a few months, he wanted to go back to [[Manila]], and believed he could get there faster by serving in the Marines than in the Army.

===U.S. Marine Corps===
He enlisted in the Marine Corps in 1940, from [[Baltimore, Maryland]]. He went to recruit training at [[Marine Corps Recruit Depot Parris Island]], followed by training at [[Marine Corps Base Quantico]] and [[Marine Corps Air Station New River|New River]]. The Marines sent him to Guantánamo Bay, Cuba for his next assignment, and then to Guadalcanal in the Solomon Islands as a member of "D" Company, [[1st Battalion 7th Marines|1st Battalion, 7th Marines]], [[1st Marine Division (United States)|1st Marine Division]].<ref name=whoswho />

====Guadalcanal====
In October 1942, during the [[Battle for Henderson Field]], his unit came under attack by a regiment of approximately 3,000 soldiers from the Japanese [[2nd Division (Imperial Japanese Army)|Sendai Division]]. On October 24, Japanese forces began a frontal attack using machine guns, grenades, and mortars against the American [[M1917 Browning machine gun|heavy machine guns]]. Basilone commanded two sections of machine guns that fought for the next two days until only Basilone and two other Marines were left standing.<ref>United States Government. Medal of Honor citation.</ref><ref>United States Postal Service.  [http://www.usps.com/communications/news/stamps/2005/sr05_053.htm "Four Distinguished Marines Saluted on U.S. Postage Stamps"], November 10, 2005.  Accessed September 02, 2010.</ref> Basilone moved an extra gun into position and maintained continual fire against the incoming Japanese forces. He then repaired and manned another machine gun, holding the defensive line until replacements arrived. As the battle went on, ammunition became critically low. Despite their supply lines having been cut off by enemies in the rear, Basilone fought through hostile ground to resupply his heavy machine gunners with urgently needed ammunition. When the last of it ran out shortly before dawn on the second day, Basilone held off the Japanese soldiers attacking his position using his pistol and a machete. By the end of the engagement, Japanese forces opposite their section of the line were virtually annihilated. For his actions during the battle, he received the United States military's highest award for valor, the Medal of Honor.<ref name=AMOHW>{{Cite web |accessdate=2009-06-08 |url=http://www.history.army.mil/html/moh/wwII-a-f.html |title=Medal of Honor recipients |work=World War II (A – F) |publisher=[[United States Army Center of Military History]] |date=2009-06-08 |archiveurl=https://web.archive.org/web/20080616211617/http://www.history.army.mil/html/moh/wwII-a-f.html |archivedate=June 16, 2008}}</ref>

Afterwards, [[Private First Class]] Nash W. Phillips, of [[Fayetteville, North Carolina]], recalled from the battle for Guadalcanal:

{{quote|Basilone had a machine gun on the go for three days and nights without sleep, rest, or food. He was in a good emplacement, and causing the Japanese lots of trouble, not only firing his machine gun, but also using his pistol.<ref name=whoswho />}}

====War bond tours and marriage====
In 1943, Basilone returned to the United States and participated in [[War bond#World War II|war bond tours]]. His arrival was highly publicized and his hometown held a parade in his honor when he returned. The homecoming parade occurred on Sunday, September 19 and drew a huge crowd with thousands of people, including politicians, celebrities and the national press. The parade made national news in [[Life magazine|''Life'' magazine]] and [[Fox Movietone News]].<ref>"[https://books.google.com/books?id=MlcEAAAAMBAJ&pg=PA126&dq=Basilone&as_pt=MAGAZINES&ei=iM2mS6PfM6WCywTvn5SeBg&cd=1#v=onepage&q=Basilone&f=false Life Goes to a Hero's Homecoming], Life Magazine, p. 126, Oct. 11, 1943.</ref> After the parade, he toured the country raising money for the war effort and achieved celebrity status. Although he appreciated the admiration, he felt out of place and requested to return to the operating forces fighting the war. The Marine Corps denied his request and told him he was needed more on the home front.  He was offered a commission, which he turned down, and was later offered an assignment as an instructor, but refused this as well.  He requested again to return to the war and this time the request was approved. He left for [[Camp Pendleton, California]], for training on December 27. On July 3, 1944, he reenlisted in the Marine Corps.<ref>[http://ww2db.com/person_bio.php?person_id=85]</ref> While stationed at Camp Pendleton, he met his future wife, Lena Mae Riggi, who was a Sergeant in the [[United States Marine Corps Women's Reserve|Marine Corps Women's Reserve]].<ref>{{cite web |url=http://womenmarines.wordpress.com/2010/06/18/sgt-lena-mae-riggi-basilone/ |title=Sgt Lena Mae (Riggi) Basilone |work=Women Marines Association}}</ref>  They were married at St. Mary's Star of the Sea Church in [[Oceanside, California|Oceanside]], on July 10, with a reception at the Carlsbad Hotel.<ref>[http://www.carlsbadbythesea.org/community/]</ref> They honeymooned at an onion farm near [[Portland, Oregon|Portland]].<ref name=paradewebsite>{{Cite web |accessdate=October 5, 2005 |url=http://www.basiloneparade.com/ |title=The Story of Gunnery Sergeant John Basilone Part 3 |publisher=John Basilone Parade Website}}</ref>

====Iwo Jima====
[[File:John Basilone headstone Arlington National Cemetery section 12 site 384.JPG|thumb|right|John Basilone's headstone in Arlington National Cemetery]]
After his request to return to the fleet was approved, he was assigned to "C" Company, 1st Battalion, [[27th Marine Regiment]], [[5th Marine Division (United States)|5th Marine Division]]. On February 19, 1945, the first day of [[Battle of Iwo Jima|invasion of Iwo Jima]], he was serving as a machine gun section leader on Red Beach II. While the Marines landed, the Japanese concentrated their fire at the incoming Marines from heavily fortified [[blockhouse]]s staged throughout the island. With his unit pinned down, Basilone made his way around the side of the Japanese positions until he was directly on top of the blockhouse. He then attacked with grenades and demolitions, single-handedly destroying the entire strong point and its defending garrison. He then fought his way toward [[Central Field (Iwo Jima)|Airfield Number 1]] and aided a Marine [[tank]] that was trapped in an enemy [[land mine|mine field]] under intense [[mortar (weapon)|mortar]] and [[artillery]] barrages. He guided the heavy vehicle over the hazardous terrain to safety, despite heavy weapons fire from the Japanese. As he moved along the edge of the airfield, he was killed by Japanese mortar shrapnel.<ref>[[Charles "Chuck" Tatum|Tatum, Charles W.]]  ''Searching for "Manila John" Basilone'', November 10, 1994, p. 28.</ref><ref>Simpson, Ross W. "The Day My Hero Died", ''Semper Fi magazine'', vol. 69, no. 4 (July – August 2013), p. 22.</ref> His actions helped Marines penetrate the Japanese defense and get off the landing beach during the critical early stages of the invasion. He was posthumously awarded the Marine Corps' second-highest decoration for valor, the [[Navy Cross (United States)|Navy Cross]], for extraordinary heroism during the battle of Iwo Jima.<ref name=hallofvalor>{{Hall of Valor|1829|accessdate=February 25, 2010}}</ref>

Based on his research for the book and mini-series ''[[The Pacific (TV miniseries)|The Pacific]]'', author [[Hugh Ambrose]] suggested that Basilone was not killed by a mortar, but by small arms fire that hit him in the right groin, the neck and nearly took off his left arm.<ref>{{cite book |last1=Ambrose |first1=Hugh |url=https://books.google.com/books?id=zuTGM9XgosgC&pg=PT100&lpg=PT100&dq=hugh+ambrose+john+basilone+mortar&source=bl&ots=Lpnbx83Ujc&sig=9MFi8XAxhLEuM1p3PTLNYecO0OM&hl=en&sa=X&ved=0ahUKEwit5LaVqN_KAhWDthoKHY2wAGQQ6AEIOzAF#v=snippet&q=basilone&f=false |title=The Pacific |chapter=notes |publisher=Penguin |date=2010 |isbn=9781101185841}}</ref>

====Burial====
He was interred in [[Arlington National Cemetery]] in Section 12, Grave 384, grid Y/Z 23.5.<ref name=ANC>{{Cite web |accessdate=November 23, 2005 |url=http://www.arlingtoncemetery.net/johnbasi.htm |title=John Basilone,Gunnery Sergeant, United States Marine Corps |publisher=Arlington National Cemetery Website |date=March 26, 2006}}</ref> Lena M. Basilone died June 11, 1999, at the age of 86, and was buried at [[Riverside National Cemetery]] in [[Riverside, California]].<ref>{{FAG|334448|Sgt Lena Mae ''Riggi'' Basilone}}</ref> Lena's obituary notes that she never remarried, and was buried still wearing her wedding ring.<ref>{{Cite web |accessdate=May 5, 2010 |url=http://www.johnbasilonestampcampaign.com/imgs/Articles/ex-marine_Lena_Basilone_dies.jpg |title=Ex-Marine Lena Basilone Obituary}}</ref>

==Awards and decorations==
GySgt. Basilone's military awards include:
<ref>{{cite web |url=http://pacificwrecks.com/valor/moh/basilone/2010/basilone-ribbons.html#axzz2vwoIJLh9 |title=Raritan Public Library "Basilone Room"Gy/Sgt John Basilone ribbons |publisher=PacificWrecks.com |accessdate=14 March 2014}}</ref>
<center>
{|
|-
| {{Ribbon devices|number=0|type=award-star|ribbon=Medal of Honor ribbon.svg|width=106 | alt=A light blue ribbon with five white five pointed stars}}
| {{Ribbon devices|number=0|type=award-star|ribbon=Navy Cross ribbon.svg|width=106}}
| {{Ribbon devices|number=0|type=award-star|ribbon=Purple Heart BAR.svg|width=106}}
|-
| {{Ribbon devices|number=1|type=service-star|ribbon=Ribbon, U.S. Navy Presidential Unit Citation.svg|width=106}}
| {{Ribbon devices|number=0|type=award-star|ribbon=Marine Corps Good Conduct ribbon.svg|width=106}}
| {{Ribbon devices|number=1|type=service-star|ribbon=American Defense Service ribbon.svg|width=106}}
|-
| {{Ribbon devices|number=0|type=award-star|ribbon=American Campaign Medal ribbon.svg|width=106}}
| {{Ribbon devices|number=2|type=service-star|ribbon=Asiatic-Pacific Campaign ribbon.svg|width=106}}
| {{Ribbon devices|number=0|type=|ribbon=World War II Victory Medal ribbon.svg|width=106}}
|-
|colspan="3" align="center"|[[File:USMC Rifle Sharpshooter badge.png|106px]]
|}
{| class="wikitable" style="text-align:center;"
|-
| colspan="2" | [[Medal of Honor]]
| colspan="2" | [[Navy Cross (United States)|Navy Cross]]
| colspan="2" | [[Purple Heart Medal]]
|-
| colspan="2" | [[Presidential Unit Citation (United States)|Navy Presidential Unit Citation]] w/ one [[Service star|{{frac|3|16}}" bronze star
]]
| colspan="2" | [[Good Conduct Medal (United States)|Marine Corps Good Conduct Medal]]
| colspan="2" | [[American Defense Service Medal]] w/ [[Service star|{{frac|3|16}}" bronze star]]
|-
| colspan="2" | [[American Campaign Medal]]
| colspan="2" | [[Asiatic–Pacific Campaign Medal]] w/ two [[Service star|{{frac|3|16}}" bronze stars]] 
| colspan="2" | [[World War II Victory Medal]]
|}</center>

===Medal of Honor citation===
Basilone's Medal of Honor citation reads as follows:

The [[President of the United States]] in the name of [[United States Congress|The Congress]] takes pride in presenting the MEDAL OF HONOR to
<center>SERGEANT <br />
'''JOHN BASILONE''' <br />
UNITED STATES MARINE CORPS
</center>
for service as set forth in the following CITATION:
[[File:Moh right.gif|90px|right|Medal of Honour]]
<blockquote>

For extraordinary heroism and conspicuous gallantry in action against enemy Japanese forces, above and beyond the call of duty, while serving with the 1st Battalion, 7th Marines, 1st Marine Division in the Lunga Area, Guadalcanal, Solomon Islands, on 24 and 25 October 1942. While the enemy was hammering at the Marines' defensive positions, Sgt. BASILONE, in charge of 2 sections of heavy machine guns, fought valiantly to check the savage and determined assault. In a fierce frontal attack with the Japanese blasting his guns with grenades and mortar fire, one of Sgt. BASILONE'S sections, with its gun crews, was put out of action, leaving only 2 men able to carry on. Moving an extra gun into position, he placed it in action, then, under continual fire, repaired another and personally manned it, gallantly holding his line until replacements arrived. A little later, with ammunition critically low and the supply lines cut off, Sgt. BASILONE, at great risk of his life and in the face of continued enemy attack, battled his way through hostile lines with urgently needed shells for his gunners, thereby contributing in large measure to the virtual annihilation of a Japanese regiment. His great personal valor and courageous initiative were in keeping with the highest traditions of the [[Department of the Navy|U.S. Naval Service]].<ref name=AMOHW /><br /> <center> [[Franklin D. Roosevelt|''FRANKLIN D. ROOSEVELT'']]</center></blockquote>

===Navy Cross===
Basilone's Navy Cross citation reads as follows:

The [[President of the United States]] takes pride in presenting the NAVY CROSS posthumously to
<center> GUNNERY SERGEANT <br />
'''JOHN BASILONE''' <br />
UNITED STATES MARINE CORPS
</center>
for service as set forth in the following CITATION:
[[File:Navycross.jpg|70px|right|Navy Cross]]
<blockquote>

For extraordinary heroism while serving as a Leader of a Machine-Gun Section, Company C, 1st Battalion, 27th Marines, 5th Marine Division, in action against enemy Japanese forces on Iwo Jima in the [[Volcano Islands]], 19 February 1945. Shrewdly gauging the tactical situation shortly after landing when his company's advance was held up by the concentrated fire of a heavily fortified Japanese [[blockhouse]], Gunnery Sergeant BASILONE boldly defied the smashing bombardment of heavy caliber fire to work his way around the flank and up to a position directly on top of the blockhouse and then, attacking with grenades and demolitions, single handedly destroyed the entire hostile strong point and its defending garrison. Consistently daring and aggressive as he fought his way over the battle-torn beach and up the sloping, gun-studded terraces toward Airfield Number 1, he repeatedly exposed himself to the blasting fury of exploding shells and later in the day coolly proceeded to the aid of a friendly [[tank]] which had been trapped in an enemy [[land mine|mine field]] under intense [[mortar (weapon)|mortar]] and [[artillery]] barrages, skillfully guiding the heavy vehicle over the hazardous terrain to safety, despite the overwhelming volume of hostile fire. In the forefront of the assault at all times, he pushed forward with dauntless courage and iron determination until, moving upon the edge of the airfield, he fell, instantly killed by a bursting mortar shell. Stouthearted and indomitable, Gunnery Sergeant BASILONE, by his intrepid initiative, outstanding skill, and valiant spirit of self-sacrifice in the face of the fanatic opposition, contributed materially to the advance of his company during the early critical period of the assault, and his unwavering devotion to duty throughout the bitter conflict was an inspiration to his comrades and reflects the highest credit upon Gunnery Sergeant BASILONE and the [[U.S. Department of the Navy|United States Naval Service]]. He gallantly gave his life in the service of his country.</blockquote>

<center>For the [[President of the United States|President]],<br />
[[James Forrestal|''JAMES FORRESTAL'']]<br />
[[United States Secretary of the Navy|Secretary of the Navy]]</center>

==Other honors==
Basilone has received numerous honors, including the following:
[[File:USS Basilone christening I03377.jpg|thumb|right|Sgt. Lena Mae Basilone, USMC(WR), widow of John Basilone, prepares to christen the destroyer USS ''Basilone'' (December 21, 1945)]]

===Navy===

* USS ''Basilone'' (DD 824): The United States Navy commissioned {{USS|Basilone|DD-824|6}}, a {{Sclass|Gearing|destroyer}}, in 1949. The ship's keel was laid down on July 7, 1945, in [[Orange, Texas]], and [[Ship naming and launching|launched]] on December 21, 1945. His widow, Sergeant Lena Mae Basilone, sponsored the ship.<ref name=danfs>{{Cite web |accessdate=2007-10-21 |url=http://www.history.navy.mil/danfs/b3/basilone-i.htm |title=Basilone |work=Dictionary of American Naval Fighting Ships |publisher=[[Naval History & Heritage Command]], Department of the Navy}}</ref>
* USS ''John Basilone'' (DDG 122): The future USS [[USS John Basilone (DDG-122)|''John Basilone'' (DDG 122)]] will be commissioned in 2019.
* A plaque at the [[United States Navy Memorial]] in [[Washington, D.C.]];<ref name=MacGillis>{{Cite news |accessdate=February 25, 2010 |url=http://www.washingtonpost.com/wp-dyn/content/article/2006/05/28/AR2006052801090.html |title=Honoring One Marine To Remember Them All: WWII Hero Gets Plaque at Navy Memorial |author=MacGillis, Alec |date=May 29, 2006 |page=B01 |work=Washington Post}}</ref>

===Marine Corps===
Marine Corps Base Camp Pendleton:

* An entry point onto the base from Interstate 5 called "Basilone Road";<ref name=GlobalSecurity>{{Cite web |accessdate=February 25, 2010 |url=http://www.globalsecurity.org/military/facility/camp-pendleton.htm |title=Camp Pendleton |publisher=GlobalSecurity.org |date=April 26, 2005}}</ref>
* A section of U.S. [[Interstate 5]] running through the base called "Gunnery Sergeant John Basilone Memorial Highway";<ref name="memhigh">{{Cite web |accessdate=February 25, 2010 |url=http://www.sen.ca.gov/leginfo/bill-6-dec-1998/CURRENT/SCR/FROM0000/SCR0025/T990406.TXT |title=California State Senate Legislation |work=SCR 25 Gunnery Sergeant John Basilone Memorial Freeway |date=April 6, 1999}} {{Dead link|date=April 2012|bot=H3llBot}}</ref>
* A [[Drop zone|parachute landing zone]] called "Basilone Drop Zone".<ref name=BasiloneDZ>{{Cite web |accessdate=February 25, 2010 |url=http://www.marsoc.usmc.mil/news/paraloft.htm |title=Special Ops Marines conquer skies |publisher=[[United States Marine Corps]] |author=Lance Corporal Stephen C. Benson |date=November 14, 2007 |archiveurl=https://web.archive.org/web/20080313175244/http://www.marsoc.usmc.mil/news/paraloft.htm |archivedate=March 13, 2008}}</ref>
* During the Crucible portion of Marine Corps Recruit Training on the West Coast, there is an obstacle named "Basilone's Challenge" that consists of carrying ammunition cans filled with concrete up a steep, wooded hill.<ref>http://www.tecom.marines.mil/News/News-Article-Display/Article/528426/company-c-fights-up-hill-battle-basilones-challenge/</ref>

===Public===

In 1944, Army Barracks from Washington State were moved to a site in front of Hansen Dam in Pacoima, California and rebuilt as 1,500 apartments for returning GIs.  This development was named the "Basilone Homes" and was used until about 1955. The site is now a golf course.
[[File:Basilone Memorial Bridge Dedication Sign 1951.jpg|thumb|right|upright|Dedication sign for the [[Basilone Bridge|Basilone Memorial Bridge]]]]

Public honors include: The memorial parade for Basilone along Somerset Street in his hometown of Raritan has been held since 1981,<ref name="NJ.com">{{Citation |last=Eugene |first=Paik |title=Annual John Basilone Parade stirs feelings of patriotism, Jersey pride |newspaper=The Star-Ledger |date=September 25, 2011 |url=http://www.nj.com/news/index.ssf/2011/09/annual_john_basilone_parade_st.html |accessdate=2011-09-25 |quote=}}</ref><ref>{{cite web |url=http://www.nj.com/somerset/index.ssf/2014/09/raritan_to_honor_its_fallen_son_with_33rd_annual_john_basilone_memorial_parade.html#incart_river |title=Raritan to honor its fallen son with 33rd Annual John Basilone Memorial Parade |work=NJ.com}}</ref> a residential building at [[Montclair State University]], the football field at [[Bridgewater-Raritan High School]] is called "Basilone Field" and on the wall of the fieldhouse next to the field is a mural honoring Basilone; the Knights of Columbus Council #13264 in his hometown is named in his honor;<ref name=KOCC13264>{{Cite web |accessdate=February 26, 2010 |url=http://www.knightsofcolumbuscouncils.com/13264%20Sgt.%20John%20Basilone%20Council/ |title=Knights of Columbus councils}}</ref> an overpass at the Somerville Circle in Somerville, New Jersey on U.S. Highway 202 and 206 that goes under it, the [[New Jersey Turnpike]] bridge across the [[Raritan River]] is named the "[[Basilone Bridge]]",<ref name=findagrave>{{Find a Grave|19842|work=Claim to Fame: Medal of Honor recipients|accessdate=October 21, 2007}}</ref> the new [[John Basilone Veterans Memorial Bridge|Bridge]] that crosses the Raritan River in Raritan at First Avenue and Canal Street, a memorial statue featuring him holding a water-cooled Browning machine gun is located at the intersections of Old York Road and Canal Street in Raritan (childhood friend Phillip Orlando sculpted the statue), a bust in Little Italy San Diego at Fir and India Streets (the war memorial is dedicated to residents of Little Italy who served in World War II and Korea, and the area is called Piazza Basilone),<ref name=piazza>{{Cite web |accessdate=August 19, 2010 |url=http://www.littleitalysd.com/points-of-interest/piazza-basilone/ |title=Piazza Basilone |publisher=The Little Italy Association |date=}}</ref> the Order of the Sons of Italy In America Lodge #2442 in [[Bohemia, New York]] is named in his honor,<ref>{{Cite web |accessdate=February 26, 2010 |url=http://www.osia2442.org/ |title=Sgt. John Basilone Lodge 2442 – OSIA}}</ref> and the [[General John Frelinghuysen House#Raritan Public Library|Raritan Public Library]] has the Basilone Room where memorabilia about him is kept.<ref name=Brady84>Brady, 2010, p. 84</ref>

* On November 10, 2005, the [[U.S. Postal Service]] issued the "Distinguished Marines" stamps honoring four Marine Corps heroes including Basilone.<ref name=stamp>{{Cite web |accessdate=February 25, 2010 |url=http://shop.usps.com/cgi-bin/vsbv/postal_store_non_ssl/browse_content/stampReleaseDisplay.jsp?OID=8610 |title=Distinguished Marines |work=Postal Store |publisher=[[United States Postal Service]] |archivedate=December 20, 2005 |archiveurl=https://web.archive.org/web/20051220102101/http://shop.usps.com/cgi-bin/vsbv/postal_store_non_ssl/browse_content/stampReleaseDisplay.jsp?OID=8610}}</ref><ref>{{Cite web |accessdate=February 6, 2009 |url=http://www.johnbasilonestampcampaign.com/award_list.html |title=John Basilone Stamp Campaign}}</ref>
* In 2011, Basilone was inducted into the [[New Jersey Hall of Fame]].<ref>{{cite web |title=Raritan's World War II hero John Basilone inducted into NJ Hall of Fame |url=http://www.nj.com/messenger-gazette/index.ssf/2011/01/raritans_world_war_ii_hero_john_basilone_inducted_into_nj_hall_of_fame.html |website=NJ.com |publisher=The Messenger-Gazette |accessdate=10 November 2015 |date=20 January 2011}}</ref><ref>{{cite web |title=JOHN BASILONE – Historical – Raritan (1916–1945) |url=http://njhalloffame.org/hall-of-famers/2011-inductees/john-basilone/ |website=New Jersey Hall of Fame |accessdate=10 November 2015}}</ref>

===In media===

The 1967 film ''[[First to Fight (film)|First to Fight]]'' features [[Chad Everett]] as "Shanghai Jack" Connell, a character based on "Manila John" Basilone.

''The Pacific'' (2010 miniseries): Basilone along with two other Marines became the basis of a 10-part [[HBO]] miniseries ''[[The Pacific (TV series)|The Pacific]]''.<ref>{{Cite web |accessdate=February 25, 2010 |url=http://www.hbo.com/the-pacific#/the-pacific/about.html/eNrjcmbOYM5nLtQsy0xJzXfMS8ypLMlMds7PK0mtKFHPz0mBCQUkpqf6JeamcjIyskknlpbkF+QkVtqWFJWmsjGyMQIAWCcXOA== |title=The Pacific |publisher=Home Box Office (HBO) |date=}}</ref> Actor [[Jon Seda]] stars as Basilone.

==See also==
* [[List of historically notable United States Marines]]
* [[List of Medal of Honor recipients for World War II#B|List of Medal of Honor recipients for World War II]]

{{Portal bar|Biography|United States Marine Corps|World War II}}

==References==
: {{Marine Corps}}
: {{DANFS}}
{{Reflist|2}}

{{Refbegin}}
* {{Cite book |last=Brady |first=James |authorlink=James Brady (columnist) |year=2010 |title=Hero of the Pacific: The Life of Marine Legend John Basilone |publisher=Wiley |url=https://books.google.com/books?id=hwZXnM6jNSgC |isbn=978-0-470-37941-7}}
{{Refend}}

==Further reading==
* {{Cite web |accessdate=February 6, 2009 |url=http://www.johnbasilonestampcampaign.com/life-death_of_manilla_john.html |title=The Life and Death of 'Manila John' |first=William Douglas |last=Lansford |work=Leatherneck Magazine |format=reprinted by the John Basilone Stamp Campaign}}
* {{Cite book |last1=Proser |first1=Jim |authorlink= |last2=Cutter |first2=Jerry |year=2004 |title="I'm Staying with My Boys..." The Heroic Life of Sgt. John Basilone, USMC |publisher=Lightbearer Communications Company |location= |isbn=0-9755461-0-4}}

==External links==
{{Commons category}}
* {{Cite web |accessdate=October 5, 2005 |url=http://www.basiloneparade.com/ |title=John Basilone Parade Website}}
* {{Cite web |accessdate=February 26, 2010 |url=http://www.sgtjohnbasilone.com/ |title=Sgt. John Basilone Foundation}}
* {{Cite web |accessdate=March 21, 2010 |url=http://www.marinesidphillips.com/john-basilone.htm |title=John Basilone's Medal of Honor Ceremony remembered by Marine Sidney Phillips}}
* [https://www.mcu.usmc.mil/historydivision/Pages/Who's%20Who/A-C/Basilone_J.aspx United States Marine Corps History Division Gunnery Sergeant John Basilone Biography]
* {{YouTube|-FsKhy2uM9w|John Basilone meets the Mayor of New York}}
* [http://www.lylefrancispadilla.com/basilone.html Medal of Honor Recipients Depicted in Film]

{{US Marine Corps navbox}}
{{good article}}

{{Authority control}}

{{DEFAULTSORT:Basilone, John}}
[[Category:1916 births]]
[[Category:1945 deaths]]
[[Category:American male boxers]]
[[Category:American military personnel killed in World War II]]
[[Category:American military personnel of Italian descent]]
[[Category:American people of Italian descent]]
[[Category:Battle of Iwo Jima]]
[[Category:Burials at Arlington National Cemetery]]
[[Category:People from Buffalo, New York]]
[[Category:People from Raritan, New Jersey]]
[[Category:People from Reistertown, Maryland]]
[[Category:Recipients of the Navy Cross (United States)]]
[[Category:United States Marine Corps Medal of Honor recipients]]
[[Category:United States Marines]]
[[Category:World War II recipients of the Medal of Honor]]