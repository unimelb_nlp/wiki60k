[[Image:Baffin Island Northeast Coast 1997-08-07.jpg|thumb|right|North American Arctic]]
The '''Arctic Institute of North America''' is a [[multi-disciplinary]] [[research institute]] and educational organization located in the [[University of Calgary]]. It is mandated to study the North American and [[Arctic cooperation and politics|circumpolar Arctic]] in the areas of [[natural science]], [[social science]], arts and the humanities.  In addition, it acquires, preserves and disseminates information on environmental, physical, and social conditions in the North. The institute was created in 1945 by a Canadian [[Act of Parliament]] as a [[non-profit]] membership organization, and also incorporated in the State of [[New York (state)|New York]].<ref>{{cite web|url=http://www.arctic.ucalgary.ca/index.php?page=mandate|title=Mandata|publisher=arctic.ucalgary.ca|accessdate=2008-10-15| archiveurl= https://web.archive.org/web/20081029190946/http://www.arctic.ucalgary.ca/index.php?page=mandate| archivedate= 29 October 2008 <!--DASHBot-->| deadurl= no}}</ref>

==History==
The idea of the institute began in the early 1940s when a group of Canadians discussed ways that Canada could increase administrative, scientific and technical expertise in the [[Arctic]]. By 1944, a binational organization that included Canada and the United States, with room for Greenland, Newfoundland, and Labrador was established. Offices were opened at [[McGill University]] in [[Montreal]], [[Quebec, Canada]]. [[Geophysics]]t [[Lawrence Gould]] was selected as acting director, replaced in 1945 by Lincoln Washburn.<ref name="macdonald">{{cite journal|last=MacDonald|first=Robert|date=December 2005|title=Challenges and Accomplishments: A Celebration of the Arctic Institute of North America|journal=Arctic|volume=58|issue=4|pages=1|url=http://pubs.aina.ucalgary.ca/arctic/Arctic58-4-440.pdf}}</ref> The initial budget was $10,000.<ref name="MacDonald, p. 441">MacDonald, p. 441</ref>

One of the most important programs of the AINA was to establish a library.  In 1955, there were over a 1,000 acquisitions.  In 1961, there were over 4,800 volumes and 476 serials. There were 7,500 volumes in 1966. Another notable program was the 1948 launch of the journal ''Arctic'' which published three issues annually until 1951, after which it became a quarterly publication.<ref name="MacDonald, p. 441" />

In 1975, the institute moved to the [[University of Calgary]] where it has remained.

Dr. [[Karla Jessen Williamson]] became its first woman and first [[Inuit]] Executive Director in 2000.<ref>{{cite journal|last=Dickerson|first=Mark O.|date=June 2000|title=The Challenge of Change|journal=Arctic|volume=53|issue=2|url=http://pubs.aina.ucalgary.ca/arctic/Arctic53-2-iii.pdf|doi=10.14430/arctic840}}</ref>

==ASTIS database==
This database is produced by the Arctic Institute of North America. The focus of the '''''Arctic Science and Technology Information System (ASTIS)''''' database is references for publications and research projects about northern Canada. It contains 70,000 records. Geographic subject coverage encompasses the three territories, the northern parts of seven provinces and the adjacent marine areas.   Format coverage includes abstracts, indexed subject terms,  geographic terms, and links to 16,000 online publications.<ref>[http://www.arctic.ucalgary.ca/index.php?page=astis_database ASTIS database]. AINA. August 2010.</ref>

==Academic journal==
{{main article|Arctic (journal)}}
The Arctic Institute of North America publishes a peer reviewed, scientific journal, entitled "''Arctic''". The journal publishes scholarly articles, book reviews, and notable people on all topics related to the polar and subpolar regions of the world.<ref name=home>{{cite web
  | title =Home page 
  | work =AINA is a multi-disciplinary research institute of the University of Calgary, Alberta, Canada 
  | publisher =Arctic Institute of North America (AINA)
  | date =August 2010 
  | url =http://www.arctic.ucalgary.ca/index.php?page=arctic_journal 
  | accessdate =2010-08-15| archiveurl= https://web.archive.org/web/20100827154032/http://www.arctic.ucalgary.ca/index.php?page=arctic_journal| archivedate= 27 August 2010 <!--DASHBot-->| deadurl= no}}</ref><ref name=authors>{{cite web
  | title =Guide for authors 
  | work =
  | publisher =Arctic Institute of North America (AINA)
  | date =August 2010 
  | url =http://www.arctic.ucalgary.ca/index.php?page=guide_for_authors 
  | accessdate =2010-08-15}}</ref><ref name=archive>{{cite web
  | title =Contents
  | work =Abstracts for back issues Volume 1 Number 1 (1948) 
  | publisher =Arctic Institute of North America (AINA)
  | date =August 2010 
  | url = http://www.arctic.ucalgary.ca/index.php?page=arctic_contents
  | accessdate =2010-08-15}}</ref>

==See also==

*[[Arctic policy of Canada]]

==References==
{{Reflist}}

==External links==
* [http://www.arctic.ucalgary.ca/ Official website]
{{University of Calgary}}

[[Category:McGill University]]
[[Category:University of Calgary]]
[[Category:1945 establishments in Quebec]]
[[Category:Arctic research]]