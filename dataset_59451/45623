[[Image:Vilhelm Swedenborg.jpg|thumb|Vilhelm Swedenborg in 1908]]
'''Gustaf Vilhelm Emanuel Swedenborg''' (17 June 1869 in Kvidinge – 1943 in [[Drottningholm]])<ref>[http://runeberg.org/adelskal/1923/1203.html 1197 (Sveriges ridderskaps och adels kalender / 1923 – fyrtiosjätte årgången)]. Runeberg.org (2012-01-20). Retrieved on 2012-03-14 (in Swedish)</ref> son of lieutenant colonel Gustaf Erik Oscar Swedenborg and Maria Therése Fock,<ref>[http://www.genealogi.se/portratt/comment.php?id=50579 Nättidningen RÖTTER – för dig som släktforskar! (Porträttfynd)]. Genealogi.se (in Swedish). Retrieved on 2012-03-14.</ref> was a member of the Swedish military and an aeronaut, today remembered as a reservist on [[Salomon August Andrée]]s failed [[S. A. Andrée's Arctic Balloon Expedition of 1897|Northpole-expedition]] in 1897 and for being one of the first Swedish balloonists. He was also the first person to hold an aeronautical certificate in Sweden, issued in 1905.<ref>{{cite web|url=http://www.ballong.org/ballongfakta/vissteduatt.php |title=Visste du att... |accessdate=April 26, 2009 |deadurl=unfit |archiveurl=https://web.archive.org/web/20070410072439/http://www.ballong.org/ballongfakta/vissteduatt.php |archivedate=April 10, 2007 }}. ballong.org (in Swedish)</ref>

== Participation in Andrées expedition ==
Swedenborg became a reserve member of the expedition in the autumn of 1896. The background for this might be that he was the son-in-law of the famous polar explorer Adolf Erik Nordenskiöld. He was also a relative of the Swedish philosopher Emanuel Swedenborg, and his famous last name gave a certain prestige to the expedition.
Swedenborg left for Paris in March 1897 together with [[Knut Frænkel]] where they received training in ballooning by [[Henri Lachambre]], the manufacturer of the balloon "The Eagle" that was to be used in the polar expedition.

However, the expedition never needed his efforts, and after the balloon took off from [[Danes Island]], he went home to Sweden where he followed a military career. He also became a famous balloonist and among others, he flew together with Hans Frænkel, the brother of [[Knut Frænkel]].<ref>Lars Westberg [http://www.stockholmskallan.se/php/fupload/SMF/SD/SSMB_0005838_01.pdf Lättare an luften]. stockholmskallan.se  (in Swedish)</ref> Together with Hans Frænkel and Eric Unge he set a Swedish record in long-distance flying with balloon. In 14½ hours they flew 760&nbsp;km. This record remains unbeaten.<ref>{{cite web|url=http://www.vastrasicklao.se/Nyheter/2009/svenskballongpionjarboddepakvarnholmen_20090301.html |title=Svensk ballongpionjär bodde på Kvarnholmen |accessdate=March 14, 2012 |deadurl=unfit |archiveurl=https://web.archive.org/web/20100820202839/http://www.vastrasicklao.se/Nyheter/2009/svenskballongpionjarboddepakvarnholmen_20090301.html |archivedate=August 20, 2010 }}. vastrasicklao.se (2009-03-01) (in Swedish)</ref>

When the remains of the Andrée-expedition were found in 1930, the dead were transported to [[Sweden]]. The aging lieutenant colonel Swedenborg (he was retired in 1921) was part of the honour guard when the bodies were received in Sweden; a display of heraldic grief unmatched until the assassination of Olof Palme almost 60 years later.

== Literature ==
Vilhelm Swedenborg:''Över och under molnen'', Stockholm 1939, Gebers forlag, 200 pages.

== References ==
{{reflist|35em}}

== See also ==
[[S. A. Andrée's Arctic balloon expedition of 1897]]

{{Authority control}}

{{DEFAULTSORT:Swedenborg, Vilhelm}}
[[Category:Swedish balloonists]]
[[Category:1869 births]]
[[Category:1943 deaths]]