{{good article}}
{{italic title}}
{{taxobox
|image = 20120904Schwarzerle Reilingen01.jpg
| status = LC | status_system = IUCN3.1
| status_ref = <ref name = IUCN>{{IUCN|version= 2014.2|assessor= Participants of the FFI|assessor2= IUCN SSC Central Asian regional tree Red Listing workshop, Bishkek, Kyrgyzstan (11–13 July 2006)|year= 2007|id= 63517|title= ''Alnus glutinosa''|downloaded= 8 October 2014}}</ref>
|regnum = [[Plant]]ae
|unranked_divisio = [[Angiosperms]]
|unranked_classis = [[Eudicots]]
|unranked_ordo = [[Rosids]]
|ordo = [[Fagales]]
|familia = [[Betulaceae]]
|genus = ''[[Alder|Alnus]]''
|subgenus = ''Alnus''
|species = '''''A. glutinosa'''''
|binomial = ''Alnus glutinosa''
|binomial_authority = ([[Carl Linnaeus|L.]]) [[Joseph Gaertner|Gaertn.]]<ref name=IPNI_Ag/>
|range_map = Alnus glutinosa range.svg
|range_map_caption = Distribution map
|synonyms=
  {{Specieslist
  |Alnus glutinosa ''var.'' vulgaris|Spach,  nom. inval.
  |Alnus vulgaris|Hill, nom. inval.
  |Betula alnus ''var.'' glutinosa|L.
  |Betula glutinosa|(L.) Lam.
  }}
|synonyms_ref=<ref name="WCSP_6364">{{Cite web |title=''Alnus glutinosa''|work=[[World Checklist of Selected Plant Families]] |publisher=[[Royal Botanic Gardens, Kew]] |url=http://apps.kew.org/wcsp/namedetail.do?name_id=6364 |accessdate=2014-08-31}}</ref>
}}

'''''Alnus glutinosa''''', the '''common alder''', '''black alder''', '''European alder''' or just '''alder''', is a [[species]] of [[tree]] in the [[family (biology)|family]] [[Betulaceae]], [[native plant|native]] to most of Europe, southwest Asia and northern Africa. It thrives in wet locations where its association with the bacterium ''[[Frankia alni]]'' enables it to grow in poor quality soils. It is a medium size, short-lived tree growing to a height of up to 30 metres (100&nbsp;ft). It has short-stalked rounded leaves and separate male and female flower in the form of catkins. The small, rounded fruits are cone-like and the seeds are dispersed by wind and water.

The common alder provides food and shelter to wildlife, with a number of insects, lichens and fungi being completely dependent on the tree. It is a pioneer species, colonising vacant land and forming mixed forests as other trees appear in its wake. Eventually common alder dies out of woodlands because the seedlings need more light than is available on the forest floor. Its more usual habitat is forest edges, swamps and riverside corridors. The timber has been used in underwater foundations and for manufacture into paper and fibreboard, for smoking foods, for joinery, turnery and carving. Products of the tree have been used in [[ethnobotany]], providing folk remedies for various ailments, and research has shown that extracts of the seeds are active against pathogenic bacteria.

==Description==
[[File:Alnus glutinosa.jpg|thumb|240px|Foliage]]
[[File:Alkottar.jpg|240px|thumb|Male inflorescence (left) and mature cones (right)]]
''Alnus glutinosa'' is a tree that thrives in moist soils, and grows under favourable circumstances to a height of {{convert|20|to|30|m}} and exceptionally up to {{convert|37|m|ft}}.<ref>{{cite web|url=http://www.mluv.brandenburg.de/cms/detail.php/lbm1.c.189715.de |title=Spitzenbäume |publisher=Land [[Brandenburg]] |accessdate=2009-01-19 |deadurl=yes |archiveurl=https://web.archive.org/web/20160303172157/http://www.mluv.brandenburg.de/cms/detail.php/lbm1.c.189715.de |archivedate=2016-03-03 |df= }}</ref> Young trees have an upright habit of growth with a main axial stem but older trees develop an arched crown with crooked branches. The base of the trunk produces [[adventitious roots]] which grow down to the soil and may appear to be propping the trunk up. The bark of young trees is smooth, glossy and greenish-brown while in older trees it is dark grey and fissured. The branches are smooth and somewhat sticky, being scattered with resinous warts. The buds are purplish-brown and have short stalks. Both male and female catkins form in the autumn and remain dormant during the winter.<ref name=Vedel>{{cite book |title=Trees and Bushes in Woods and Hedgerows |last=Vedel |first=Helge |last2=Lange |first2=Johan |year=1960 |publisher=Methuen |isbn=978-0-416-61780-1 |pages=143–145 }}</ref>

The leaves of the common alder are short-stalked, rounded, up to {{convert|10|cm|0|abbr=on}} long with a slightly wedge-shaped base and a wavy, serrated margin. They have a glossy dark green upper surface and paler green underside with rusty-brown hairs in the angles of the veins. As with some other trees growing near water, the common alder keeps its leaves longer than do trees in drier situations, and the leaves remain green late into the autumn. As the Latin name ''glutinosa'' implies, the buds and young leaves are sticky with a [[resin]]ous gum.<ref name=Vedel/><ref name=tfl>[http://www.treesforlife.org.uk/forest/species/alder.html Trees for Life Species Profile: ''Alnus glutinosa'']</ref><ref name=flora>[http://ip30.eti.uva.nl/BIS/flora.php?selected=beschrijving&menuentry=soorten&id=1770 Flora of NW Europe: ''Alnus glutinosa'']</ref>

The species is [[plant sexuality|monoecious]] and the flowers are wind-pollinated; the slender cylindrical male [[catkin]]s are pendulous, reddish in colour and {{convert|5|to|10|cm|0|abbr=on}} long; the female flowers are upright, broad and green, with short stalks. During the autumn they become dark brown to black in colour, hard, somewhat woody, and superficially similar to small [[conifer]] cones. They last through the winter and the small winged seeds are mostly scattered the following spring. The seeds are flattened reddish-brown nuts edged with webbing filled with pockets of air. This enables them to float for about a month which allows the seed to disperse widely.<ref name=Vedel/><ref name=tfl/><ref name=flora/>

Unlike some other species of tree, common alders do not produce shade leaves. The respiration rate of shaded foliage is the same as well-lit leaves but the rate of [[Assimilation (biology)|assimilation]] is lower. This means that as a tree in woodland grows taller, the lower branches die and soon decay, leaving a small [[Crown (botany)|crown]] and unbranched trunk.<ref name=Claessens/>

==Taxonomy==
''Alnus glutinosa'' was first described by [[Carl Linnaeus]] in 1753, as one of two varieties of alder (the other being ''A. incana''), which he regarded as a single species ''Betula alnus''.<ref name=IPNI_Bag/> In 1785, [[Jean-Baptiste Lamarck]] treated it as a full species under the name ''Betula glutinosa''.<ref name=IPNI_Bg/> Its present scientific name is due to [[Joseph Gaertner]], who in 1791 accepted the separation of alders from [[birch]]es, and transferred the species to ''Alnus''.<ref name=IPNI_Ag/> The epithet ''glutinosa'' means "sticky", referring particularly to the young shoots.<ref name=Coom94/>

Within the genus ''Alnus'', the common alder is placed in subgenus ''Alnus'' as part of a closely related group of species including the grey alder, ''[[Alnus incana]]'',<ref name=Chen04/> with which it hybridizes to form the hybrid ''A.'' × ''hybrida''.<ref name=Stac10/>

==Distribution and habitat==
The common alder is native to almost the whole of continental Europe (except for both the extreme north and south) as well as the [[United Kingdom]] and [[Ireland]]. In Asia its range includes [[Turkey]], [[Iran]] and [[Kazakhstan]], and in Africa it is found in [[Tunisia]], [[Algeria]] and [[Morocco]]. It is [[Naturalisation (biology)|naturalised]] in the [[Azores]].<ref>{{cite web |url=http://rbg-web2.rbge.org.uk/cgi-bin/nph-readbtree.pl/feout?FAMILY_XREF=&GENUS_XREF=Alnus&SPECIES_XREF=glutinosa&TAXON_NAME_XREF=&RANK= |title=''Alnus glutinosa'' |work=Flora Europaea |publisher=Royal Botanic Garden Edinburgh |accessdate=2014-08-09}}</ref> It has been introduced, either by accident or by intent, to [[Canada]], the [[United States]], [[Chile]], [[South Africa]], [[Australia]] and [[New Zealand]]. Its natural habitat is in moist ground near rivers, ponds and lakes but it can also grow in drier locations and sometimes occurs in mixed woodland and on forest edges. It tolerates a range of soil types and grows best at a [[pH]] of between 5.5 and 7.2. Because of its association with the nitrogen-fixing bacterium ''Frankia alni'', it can grow in nutrient-poor soils where few other trees thrive.<ref name=GISD/>

==Ecological relationships==
[[File:Alder nodules2.JPG|thumb|Nodules on the roots caused by the bacterium ''[[Frankia alni]]'']]
[[File:Eriophyes inangulis N on upper surface.JPG|thumb|Galls on the leaves caused by the mite ''[[Eriophyes inangulis]]'']]
The common alder is most noted for its [[symbiotic]] relationship with the bacterium ''[[Frankia alni]]'', which forms nodules on the tree's [[root]]s. This bacterium [[nitrogen-fixing|absorbs nitrogen]] from the air and fixes it in a form available to the tree. In return, the bacterium receives carbon products produced by the tree through [[photosynthesis]]. This relationship, which improves the [[Fertility (soil)|fertility]] of the soil, has established the common alder as an important [[pioneer species]] in [[ecological succession]].<ref>{{cite journal |author1=Schwencke, J. |author2=Caru, M. |year=2001 |title=Advances in actinorhizal symbiosis: Host plant-''Frankia'' interactions, biology, and application in arid land reclamation: A review |journal=Arid Land Research and Management |volume=15 |issue=4 |pages=285–327 |doi=10.1080/153249801753127615 }}</ref>

The common alder is susceptible to ''[[Phytophthora alni]]'', a recently evolved species of [[oomycete]] plant pathogen probably of hybrid origin. This is the causal agent of phytophthora disease of alder which is causing extensive mortality of the trees in some parts of Europe.<ref>[http://www.forestry.gov.uk/PDF/fcin6.pdf/$FILE/fcin6.pdf Phytophthora Disease of Alder]</ref> The symptoms of this infection include the death of roots and of patches of bark, dark spots near the base of the trunk, yellowing of leaves and in subsequent years, the death of branches and sometimes the whole tree.<ref name=GISD>{{cite web |url=http://www.issg.org/database/species/ecology.asp?si=1669&fr=1 |title=''Alnus glutinosa'' (tree) |date=2010-08-27 |work=Global Invasive Species Database |publisher= IUCN SSC Invasive Species Specialist Group |accessdate=2014-08-04}}</ref> ''[[Taphrina alni]]'' is a fungal [[plant pathogen]] that causes alder tongue gall, a chemically induced distortion of female catkins. The gall develops on the maturing fruits and produces spores which are carried by the wind to other trees. This gall is believed to be harmless to the tree.<ref>Ellis, Hewett A. (2001). ''Cecidology''. Vol.16, No.1. p. 24.</ref> Another, also harmless, gall is caused by a midge, ''[[Eriophyes inangulis]]'', which sucks [[sap]] from the leaves forming [[Cutaneous condition#Pustule|pustules]].<ref name=TfL/>

The common alder is important to wildlife all year round and the seeds are a useful winter food for birds. Deer, sheep, hares and rabbits feed on the tree and it provides shelter for livestock in winter.<ref name=GISD/> It shades the water of rivers and streams, moderating the water temperature, and this benefits fish which also find safety among its exposed roots in times of flood. The common alder is the foodplant of the larvae of a number of different [[Butterfly|butterflies]] and [[moth]]s<ref>{{cite book |title=A field guide to caterpillars of butterflies and moths in Britain and Europe |last=Carter |first=David James |last2=Hargreaves |first2=Brian |year=1986 |publisher=Collins |isbn=978-0-00-219080-0 }}</ref> and is associated with over 140 species of plant-eating insect.<ref name=TfL/> The tree is also a [[Host (biology)|host]] to a variety of [[moss]]es and [[lichen]]s which particularly flourish in the humid moist environment of streamside trees. Some common lichens found growing on the trunk and branches include tree lungwort (''[[Lobaria pulmonaria]]''), ''Menneguzzia terebrata'' and ''Stenocybe pullatula'', the last of which is restricted to alders.<ref name=TfL>{{cite web |url=http://www.treesforlife.org.uk/forest/species/alder.html |title=Common or black alder |date=2012-11-26 |publisher=Trees for life |author =Featherstone, Alan Watson |accessdate=2014-08-07}}</ref> Some 47 species of [[Mycorrhiza|mycorrhizal fungi]] have been found growing in [[symbiosis]] with the common alder, both partners benefiting from an exchange of nutrients. As well as several species of ''[[Naucoria]]'', these symbionts include ''[[Russula alnetorum]]'', the milkcaps ''[[Lactarius obscuratus]]'' and ''[[Lactarius cyathula]]'', and the alder roll-rim ''[[Paxillus filamentosus]]'', all of which grow nowhere else except in association with alders. In spring, the catkin cup ''[[Ciboria amentacea]]'' grows on fallen alder catkins.<ref name=TfL/>

As an introduced species, the common alder can affect the ecology of its new locality. It is a fast-growing tree and can quickly form dense woods where little light reaches the ground, and this may inhibit the growth of native plants. The presence of the nitrogen-fixing bacteria and the annual accumulation of leaf litter from the trees also alters the nutrient status of the soil. It also increases the availability of phosphorus in the ground, and the tree's dense network of roots can cause increased sedimentation in pools and waterways. It spreads easily by wind-borne seed, may be dispersed to a certain extent by birds and the woody fruits can float away from the parent tree. When the tree is felled, regrowth occurs from the stump, and logs and fallen branches can take root.<ref name=GISD/> ''A. glutinosa'' is classed as an environmental weed in New Zealand.<ref>{{cite book |last=Clayson |first=Howell |title=Consolidated list of environmental weeds in New Zealand|publisher=Department of Conservation|location=Wellington|date=May 2008|isbn=978-0-478-14412-3}}</ref>

==Cultivation and uses==
[[File:Alnus glutinosa MHNT.BOT.2004.0.10a.jpg|thumb|[[Infrutescence]] and [[Achene]]s]]

The common alder is used as a pioneer species and to stabilise river banks, to assist in flood control, to purify water in waterlogged soils and to moderate the temperature and nutrient status of water bodies. It can be grown by itself or in mixed species plantations, and the nitrogen-rich leaves falling to the ground enrich the soil and increase the production of such trees as [[Juglans|walnut]], [[Douglas fir]] and [[Populus|poplar]] on poor quality soils. Although the tree can live for up to 160 years, it is best felled for timber at 60 to 70 years before [[heart rot]] sets in.<ref name=Claessens>{{cite journal |author1=Claessens, Hugues |author2=Oosterbaan, Anne |author3=Savill, Peter |author4=Rondeux, Jacques |year=2010 |title=A review of the characteristics of black alder (''Alnus glutinosa'' (L.) Gaertn.) and their implications for silvicultural practices |journal=Forestry |volume=83 |issue=2 |pages=163–175 |doi=10.1093/forestry/cpp038 }}</ref>

On marshy ground it is important as [[Coppicing|coppice-wood]], being cut near the base to encourage the production of straight poles. It is capable of enduring clipping as well as marine climatic conditions and may be cultivated as a fast-growing [[windbreak]]. In woodland natural regeneration is not possible as the seeds need sufficient nutrients, water and light to germinate. Such conditions are rarely found at the forest floor and as the forest matures, the alder trees in it die out.<ref name=PFAF/><ref>{{cite journal|last1=Kajba, D. & Gračan, J.|title=''Alnus glutinosa''|date=2003|journal=[[EUFORGEN]] Technical guidelines for genetic conservation and use for black alder|pages=4 p.|url=http://www.euforgen.org/fileadmin/templates/euforgen.org/upload/Publications/Technical_guidelines/854_Technical_guidelines_for_genetic_conservation_and_use_for_Black_alder__Alnus_glutinosa_.pdf}}</ref> The species is cultivated as a specimen tree in parks and gardens, and the [[cultivar]] 'Imperialis' has gained the [[Royal Horticultural Society]]'s [[Award of Garden Merit]].<ref>{{cite web |url=https://www.rhs.org.uk/Plants/99028/i-Alnus-glutinosa-i-Imperialis/Details |title=''Alnus glutinosa'' Imperialis |publisher=Royal Horticultural Society |accessdate=2014-08-06}}</ref>

===Timber===
The wood is soft, white when first cut, turning to pale red; the knots are attractively mottled. The timber is not used where strength is required in the construction industry, but is used for paper-making, the manufacture of [[Fiberboard|fibreboard]] and the production of energy.<ref name=Claessens/> Under water the wood is very durable and is used for [[deep foundations]] of buildings. The piles beneath the [[Rialto]] in Venice, and the foundations of several medieval cathedrals are made of alder. The Roman architect [[Vitruvius]] mentioned that the timber was used in the construction of the causeways across the  [[Ravenna]] marshes.<ref>{{cite web |url=http://www.thornr.demon.co.uk/bran/alderims.html |title=The Alder Tree |author =Paterson, J. M. |work=A Tree in Your Pocket |accessdate=2014-08-03}}</ref> The wood is used in joinery, both as solid timber and as [[Wood veneer|veneer]], where its grain and colour are appreciated, and it takes dye well. As the wood is soft, flexible and somewhat light, it can be easily worked as well as split. It is valued in turnery and carving, in making furniture, window frames, [[clog (shoe)|clogs]], toys, blocks, pencils and bowls.<ref name=Vedel/>

===Tanning and dyeing===
The bark of the common alder has long been used in tanning and dyeing. The bark and twigs contain 16 to 20% [[tannic acid]] but their usefulness in tanning is limited by the strong accompanying colour they produce.<ref name=Botanical/> Depending on the [[mordant]] and the methods used, various shades of brown, fawn, and yellowish-orange hues can be imparted to wool, cotton and silk. Alder bark can also used with [[Iron(II) sulfate|iron sulphate]] to create a black dye which can substitute for the use of [[sumac]]h or [[gall]]s.<ref>{{cite book |title=Natural Dyes and Home Dyeing |last=Adrosko |first=Rita J. |year=2012 |publisher=Courier Dover Publications |isbn=978-0-486-15609-5 |pages=41–42}}</ref> The [[Lapland (region)|Laplanders]] are said to chew the bark and use their [[saliva]] to dye leather. The shoots of the common alder produce a yellowish or cinnamon-coloured dye if cut early in the year. Other parts of the tree are also used in dyeing; the catkins can yield a green colour and the fresh-cut wood a pinkish-fawn colour.<ref name=Botanical/>

===Other uses===
It is also the traditional wood that is burnt to produce [[Smoking (food)|smoked]] fish and other smoked foods, though in some areas other woods are now more often used. It supplies high quality [[charcoal]].<ref name=Vedel/>

The leaves of this tree are sticky and if they are spread on the floor of a room, their adhesive surface is said to trap fleas.<ref name=Botanical/>

Chemical constituents of ''Alnus glutinosa'' include [[hirsutanonol]], [[oregonin]], [[genkwanin]],<ref>{{cite journal|last1=O'Rourke|first1=Ciara|last2=Sarker|first2=Satyajit D.|last3=Stewart|first3=Fiona|last4=Byres|first4=Maureen|last5=Delazar|first5=Abbas|last6=Kumarasamy|first6=Yashodharan|last7=Nahar|first7=Lutfun|title=Hirsutanonol, oregonin and genkwanin from the seeds of ''Alnus glutinosa'' (Betulaceae)|journal=Biochemical Systematics and Ecology|volume=33|issue=7|pages=749–752|issn=0305-1978|doi=10.1016/j.bse.2004.10.005}}</ref> [[rhododendrin]] {3-(4-hydroxyphenyl)-l-methylpropyl-β-D-glucopyranoside} and [[Dicarboxylic acid|glutinic acid]] (2,3-pentadienedioic acid).<ref name=Sati>{{cite journal |author1=Sati, Sushil Chandra |author2=Sati, Nitin |author3=Sati, O. P. |year=2011 |title=Bioactive constituents and medicinal importance of genus ''Alnus'' |journal=Pharmacognosy Review |volume=5 |issue=10 |pages=174–183 |doi=10.4103/0973-7847.91115 |pmc=3263052 |pmid=22279375}}</ref>

==Health==
Pollen from the common alder, along with that from birch and [[hazel]], is one of the main sources of tree pollen allergy. As the pollen is often present in the atmosphere at the same time as that of birch, hazel, hornbeam and oak, and they have similar physicochemical properties, it is difficult to separate out their individual effects. In central Europe, these tree pollens are the second most common cause of allergic conditions after grass pollen.<ref>{{cite web |url=http://www.alles-zur-allergologie.de/Allergologie/Artikel/3710/Allergen,Allergie/Erle/ |title=Erle: Schwarzerle, ''Alnus glutinosa'' |work=Alles zur Allergologie |language=German |accessdate=2014-08-05}}</ref>

The bark of common alder has traditionally been used as an [[astringent]], a [[cathartic]], a [[hemostatic]], a [[febrifuge]], a [[Herbal tonic|tonic]] and a restorative (a substance able to restore normal health). A decoction of the bark has been used to treat swelling, inflammation and rheumatism, as an [[emetic]], and to treat [[pharyngitis]] and [[sore throat]].<ref name=Sati/> Ground up bark has been used as an ingredient in toothpaste, and the inner bark can be boiled in vinegar to provide a skin wash for treating dermatitis, lice and scabies. The leaves have been used to reduce breast discomfort in nursing mothers and folk remedies advocate the use of the leaves against various forms of cancer.<ref name=PFAF>{{cite web |url=http://www.pfaf.org/user/plant.aspx?latinname=Alnus+glutinosa |title=''Alnus glutinosa'' - (L.)Gaertn. |year=2012 |publisher=Plants For A Future |accessdate=2014-08-05}}</ref> Alpine farmers are said to use the leaves to alleviate rheumatism by placing a heated bag full of leaves on the affected areas. Alder leaves are consumed by cows, sheep, goats and horses though pigs refuse to eat them. According to some people, consumption of alder leaves causes blackening of the tongue and is harmful to horses.<ref name=Botanical>{{cite web |url=https://www.botanical.com/botanical/mgmh/a/alder019.html |title=Alder, Common |author =Grieve, M. |work=Botanical.com: A Modern Herbal |accessdate=2014-08-05}}</ref>

In a research study, extracts from the seeds of the common alder have been found to be active against all the eight pathogenic bacteria against which they were tested, which included ''[[Escherichia coli]]'' and [[Methicillin-resistant Staphylococcus aureus|methicillin-resistant ''Staphylococcus aureus'']] (MRSA). The only extract to have significant [[antioxidant]] activity was that extracted in [[methanol]]. All extracts were of low toxicity to [[brine shrimp]]s. These results suggest that the seeds could be further investigated for use in the development of possible anti-MRSA drugs.<ref>{{cite journal |author1=Middleton, P. |author2=Stewart, F. |author3=Al-Qahtani, S. |author4=Egan, P. |author5=O'Rourke, C. |author6=Abdulrahman, A. |author7=Byres, M. |author8=Middleton, M. |author9=Kumarasamy, Y. |author10=Shoeb, M. |author11=Nahar, L. |author12=Delazar, A. |author13=Sarker, S. D. |year=2005 |title=Antioxidant, Antibacterial Activities and General Toxicity of ''Alnus glutinosa'', ''Fraxinus excelsior'' and ''Papaver rhoeas'' |journal=Iranian Journal of Pharmaceutical Research |volume=4 |issue=2 |pages=101–103 |url=http://ijpr.sbmu.ac.ir/index.php/daru/article/view/article_620_4.html }}</ref>

== Details of Alder structure and galls==
<gallery perrow="4" widths="168" heights="168" caption="Details ">
Image:Briesetal bei Briese.JPG|Alder carr in Germany
Image:Unterspreewald-Gross-Wasserburger-Spree-01.jpg|Trees in winter, Germany
Image:Alnus glutinosa R0015202.JPG|Buds
Image:Alnus glutinosa R0015198.JPG|Bark
File:Taphrina amentorum tongue gall.JPG|Alder Tongue Gall fungus, ''[[Taphrina alni]]''
File:Taphrina amentorum gall.JPG|Detail - Alder Tongue Gall
File:Black alder in spring.JPG|Black alder in [[Ås, Akershus|Ås]], Norway
File:Alnus glutinosa, Munkholmen.jpg|Black alder defies harsh conditions in the Swedish archipelago
</gallery>

==References==
{{Portal|Trees}}
{{reflist|3|refs=

<ref name=Chen04>{{cite journal |last1=Chen |first1=Zhiduan |last2=Li |first2=Jianhua |date=2004 |title=Phylogenetics and Biogeography of ''Alnus'' (Betulaceae) Inferred from Sequences of Nuclear Ribosomal DNA ITS Region |journal=International Journal of Plant Sciences |volume=165 |pp=325–335 |doi=10.1086/382795 |lastauthoramp=yes }}</ref>

<ref name=Coom94>{{Cite book |last=Coombes |first=Allen J. |year=1994 |title=Dictionary of Plant Names |publication-place=London |publisher=Hamlyn Books |isbn=978-0-600-58187-1 |page=8 }}</ref>

<ref name=IPNI_Ag>{{Cite web |title=''Alnus&nbsp;glutinosa'' |work=[[International Plant Names Index|The International Plant Names Index]] |url=http://www.ipni.org/ipni/idPlantNameSearch.do?id=107664-1 |accessdate=2014-08-31 }}</ref>

<ref name=IPNI_Bag>{{Cite web |title=''Betula&nbsp;alnus''&nbsp;var.&nbsp;''glutinosa'' |work=[[International Plant Names Index|The International Plant Names Index]] |url=http://www.ipni.org/ipni/idPlantNameSearch.do?id=305022-2 |accessdate=2014-08-31 }}</ref>

<ref name=IPNI_Bg>{{Cite web |title=''Betula&nbsp;glutinosa'' |work=[[International Plant Names Index|The International Plant Names Index]] |url=http://www.ipni.org/ipni/idPlantNameSearch.do?id=295095-1 |accessdate=2014-08-31 }}</ref>

<ref name=Stac10>{{Cite book |last=Stace |first=Clive |authorlink = Clive Stace |year=2010 |title=New Flora of the British Isles |edition=3rd |publication-place=Cambridge, UK |publisher=Cambridge University Press |isbn=978-0-521-70772-5 |page=296}}</ref>
}}

==External links==
* [http://www.euforgen.org/species/alnus-glutinosa/ ''Alnus glutinosa''] - distribution map, genetic conservation units and related resources. [[European Forest Genetic Resources Programme]] (EUFORGEN)

{{Commons category|Alnus glutinosa|position=left}}

{{taxonbar}}
{{Authority control}}

[[Category:Alnus|glutinosa]]
[[Category:Trees of Europe]]
[[Category:Trees of Asia]]
[[Category:Flora of the Mediterranean]]
[[Category:Flora of Europe]]
[[Category:Flora of North Africa]]
[[Category:Flora of Western Asia]]
[[Category:Plants described in 1753]]
[[Category:Trees of humid continental climate]]
[[Category:Trees of mild maritime climate]]
[[Category:Garden plants of Europe]]
[[Category:Plants used in bonsai]]
[[Category:Ornamental trees]]