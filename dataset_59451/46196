{{Infobox person
| name = Dr. Carol Remmer Angle
| image = Carol Angle 2012.JPG
| imagesize = 160px
| caption = Carol Remmer Angle, 2012.
| birth_date = {{birth date and age|1927|12|20}}
| occupation = Pediatrician, Nephrologist, Toxicologist
| spouse = Dr. William Angle (deceased 1993)
| children = Dr. Marcia Angle<br /> [http://uvahealth.com/doctors/physicians/511 Dr. John F. Angle]<br /> [http://monicaangle.com/Main.html Monica Angle]
}}

'''Carol Remmer Angle''' is an American pediatrician, [[nephrologist]], and [[toxicologist]]. Dr. Angle is known as one of the nation's leading researchers on [[lead poisoning]].<ref>{{cite news|last=Buttry|first=Stephen|title=Authority on lead poisoning now focuses on her garden|newspaper=Omaha World-Herald|date=15 January 2002|location=News|page=2B|quote=In more than 40 years at the University of Nebraska Medical Center, Angle became one of the nation's leading researchers of lead poisoning, tying elevated blood levels of lead to various environmental causes.}}</ref>  She is professor emeritus at the [[University of Nebraska Medical Center]] (UNMC) in Omaha, Nebraska.  Dr. Angle joined UNMC in 1971<ref>{{cite web|last=McMaster |first=Andrea |title=2008 Legends Honored |url=http://app1.unmc.edu/publicaffairs/todaysite/sitefiles/today_full.cfm?match=5141 |publisher=University of Nebraska Medical Center |accessdate=4 December 2012 |location=UNMC News |date=21 November 2008 |quote=Through much of her career, she has been active in the National Foundation Birth Defects Treatment Center and she also has served on the editorial board of the Journal of Toxicology -- Clinical Toxicology and the Journal of Toxicology and Environmental Health. |deadurl=yes |archiveurl=https://web.archive.org/web/20160304022608/http://app1.unmc.edu/publicaffairs/todaysite/sitefiles/today_full.cfm?match=5141 |archivedate=4 March 2016 |df= }}</ref> and was one of the first women to serve as chair of an academic medical department (pediatrics).<ref>{{cite news|title=First Chairwoman Named by N.U. College of Medicine|newspaper=Sunday World-Herald|date=1 March 1981|agency=Douglas County Historical Society|page=10–B|quote=For the first time in its 100-year history, the University of Nebraska College of Medicine has a woman heading one of its departments. Dr. Carol R. Angle who has been on the N.U. faculty since 1954, has been named chairman of the department of pediatrics.}}</ref>  She also served as chief of pediatric [[nephrology]], director of the pediatric intensive care unit, and director of medical [[toxicology]].  In 1957, Dr. Angle along with Dr. Matilda McIntire, founded one of the country’s first poison control centers.<ref>{{cite news|last=Bradley|first=Qianna|title=Center's 50 years of saving lives Poison facts|newspaper=Omaha World-Herald|date=4 April 2007|quote=The center was begun in 1957 under the leadership of Drs. Matilda McIntire and Carol Angle}}</ref>  Dr. Angle is a founding member and a prior president of the [[American Association of Poison Control Centers]].<ref name="OmahanHeads">{{cite news|title=Omahan Heads Poison Agency|newspaper=Omaha World-Herald|date=10 January 1975|page=4|quote=Dr. Carol Angle, pediatrics professor at the University of Nebraska Medical Center, has assumed the presidency of the American Association of Poison Control Centers.}}</ref> For forty years, Dr. Angle served as an expert for [[NIEHS]], [[National Institutes of Health]]<ref>{{cite news|title=First Chairwoman Named by N.U. College of Medicine|newspaper=Sunday World-Herald|date=1 March 1981|agency=Douglas County Historical Society|page=10–B|quote=Dr. Angle, whose research focuses on environmental health related to children, is a member of a research review section for the National Institutes of Health.}}</ref> and [[U.S. Environmental Protection Agency]]  panels investigating heavy metal toxicity. Dr. Angle continues as a toxicology consultant, reviewer and editor.

== Education and training ==
[[Wellesley College]]; [[Cornell Medical College|Cornell Medical School]]; New York Hospital Pediatric, Internship and Residency; [http://www.nebraskamed.com/ University of Nebraska Hospital], Residency<ref>{{cite news|title=none|newspaper=Omaha World-Herald|date=10 July 1954|quote=Dr. Angle, who comes from Oakdale, Long Island, originally is a graduate of Wellesley College and Cornell Medical School.  She took two years of pediatric training at New York Hospital of Cornell Medical Center and a third year at Childrens Hospital.}}</ref>

== Offices held and honors ==
* Director, Medical Education, [http://childrensomaha.org/ Children's Memorial Hospital], Omaha, Nebraska, 1954-1967<ref name="WareDoris">{{cite news|last=Ware|first=Doris Ann|title=Pediatrics and Poison Her Specialties|newspaper=Omaha World-Herald|date=18 January 1970|page=7–E|quote=She was director of medical education at Children's Memorial Hospital from 1954 until 1967, and director of the Nebraska Master Poison Control Center from 1957 until 1966.}}</ref> 
* Director, [http://www.nebraskapoison.com/ Nebraska Master Poison Control Center], 1957-1966<ref>{{cite news|title=Sedative-Poisoned Children Will Participate n Study|newspaper=Omaha World-Herald|date=25 March 1966|page=8|quote=Dr. Carol R. Angle, director of the Poison Control Center at the hospital, will head the project.}}</ref> 
* State Coordinator, Nebraska Master Poison Control Center, 1957-1966<ref name="WareDoris" /> 
* Director, Pediatric Renal Clinic, University of Nebraska Hospital & Clinics, 1966-1984<ref name="FirstChairwoman">{{cite news|title=First Chairwoman Named by N.U. College of Medicine|newspaper=Sunday World-Herald|date=1 March 1981|agency=Douglas County Historical Society|page=10–B|quote=She is clinical director of the N.U. Medical Center's pediatric renal clinic and the Nebraska Birth Defects Clinic.}}</ref>
* Director, Pediatric Intensive Care Unit, University of Nebraska Hospital, 1968-1974<ref>{{cite news|last=Ware|first=Doris Ann|title=Pediatrics and Poison Her Specialties|newspaper=Omaha World-Herald|date=18 January 1970|page=7–E|quote=Dr. Angle is director of the pediatric intensive care unit and the pediatric renal clinic at the university, and is associate editor of the national journal, Clinical Toxicology.}}</ref>
* Program Chairman, American Association of Poison Control Centers, 1977-1979<ref name="OmahanHeads" /> 
* Professor, Department of Pediatrics, University of Nebraska College of Medicine, 1971-1998<ref>{{cite news|title=Two Leaving N.U. Medical Posts|newspaper=Omaha World-Herald|date=27 March 1980|page=39|quote=Dr. Carol Angle, professor of pediatrics, will serve as acting chairman of the pediatrics department, starting April 1.}}</ref> 
* Director, National Foundation Birth Defects Treatment Center, Children's Memorial Hospital, 1974-1981<ref name="FirstChairwoman" />
* Member, Toxicology Advisory Board, U.S. Consumer Product Safety Commission, 1978-1982<ref>{{cite news|title=Adviser Named|newspaper=Sunday World-Herald|date=17 June 1979|page=14–B|quote=Dr. Carol Angle, a pediatrics professor at the University of Nebraska Medical Center, has been named a member of the Consumer Product Safety Commission's toxicology advisory board.}}</ref> 
* Chairman, Department of Pediatrics, University of Nebraska College of Medicine, 1981-1985<ref>{{cite news|title=Dr. Angle Selected|newspaper=Omaha World-Herald|date=18 March 1982|page=4|quote=Dr. Carol Angle, chairman of the department of pediatrics at the University of Nebraska Medical Center, has been selected as president-elect of the metals specialty section of the Society of Toxicology.}}</ref>
* Member, [http://www.niehs.nih.gov/about/boards/naehsc/index.cfm National Advisory Environmental Health Sciences Council], NIH, 1984-1987
* Director, Clinical Toxicology, University of Nebraska Medical Center, 1985-1998<ref>{{cite web|last=Setton|first=Dolly|title=The Berkshire Bunch|url=http://www.forbes.com/global/1998/1012/0114028a.html|publisher=Forbes.com|accessdate=11 December 2012|date=12 October 1998|quote=Dr. Angle still practices medicine, as director of clinical toxicology at the University of Nebraska Medical Center.}}</ref> 
* Editor-in-Chief, Journal of Toxicology - Clinical Toxicology, 1989-2002<ref>{{cite web|last=McMaster |first=Andrea |title=2008 Legends Honored |url=http://app1.unmc.edu/publicaffairs/todaysite/sitefiles/today_full.cfm?match=5141 |publisher=University of Nebraska Medical Center |accessdate=4 December 2012 |location=UNMC News |date=21 November 2008 |quote=Through much of her career, she has been active in the National Foundation Birth Defects Treatment Center and she also has served on the editorial board of the Journal of Toxicology -- Clinical Toxicology and the Journal of Toxicology and Environmental Health. |deadurl=yes |archiveurl=https://web.archive.org/web/20160304022608/http://app1.unmc.edu/publicaffairs/todaysite/sitefiles/today_full.cfm?match=5141 |archivedate=4 March 2016 |df= }}</ref><ref>{{cite news|last=Buttry|first=Stephen|title=From cannonballs to gasoline, lead's history is long About this story|newspaper=Omaha World-Herald|date=15 January 2002|location=News|page=1B|quote=Angle, who still edits the Journal of Toxicology - Clinical Toxicology, and her colleagues began studying the health effects of emissions from Omaha's industries, which also included an Asarco refinery that eventually closed in 1997.}}</ref>  
* Professor Emeritus, Department of Pediatrics, University of Nebraska College of Medicine, 1999–present<ref>{{cite web|title=ACMT Awards|url=http://www.acmt.net/awards.html|publisher=American College of Medical Toxicology|accessdate=4 December 2012|location=Matthew J. Ellenhorn Award: Past Recipients|quote=A professor emeritus for the UNMC Department of Pediatrics, Dr. Angle joined the UNMC medical staff in 1971 and served in a number of roles including chairman of the department of pediatrics.}}</ref> 
* Honor Award, [http://www.acmt.net/awards.html Matthew J. Ellenhorn Award], 2003<ref>{{cite web|title=ACMT Awards|url=http://www.acmt.net/awards.html|publisher=American College of Medical Toxicology|accessdate=4 December 2012|location=Matthew J. Ellenhorn Award: Past Recipients}}</ref> 
* Honor Award, University of Nebraska Medical Center Legends Award, 2008 [http://app1.unmc.edu/publicaffairs/todaysite/sitefiles/today_full.cfm?match=5141]<ref>{{cite web|last=McMaster |first=Andrea |title=2008 Legends Honored |url=http://app1.unmc.edu/publicaffairs/todaysite/sitefiles/today_full.cfm?match=5141 |publisher=University of Nebraska Medical Center |accessdate=4 December 2012 |pages=UNMC News |date=21 November 2008 |deadurl=yes |archiveurl=https://web.archive.org/web/20160304022608/http://app1.unmc.edu/publicaffairs/todaysite/sitefiles/today_full.cfm?match=5141 |archivedate=4 March 2016 |df= }}</ref>

== Published works (partial list) ==
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| title = Congenital bowing and angulation of long bones 
| journal = Pediatrics 
| volume = 13 
| issue = 3 
| pages = 257–268 
| year = 1954 
| pmid = 13155074
}}
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| title = Poison Control Outlines: Toxicity of Insecticides and Herbicides 
| journal = The Nebraska state medical journal 
| volume = 48 
| pages = 644–646 
| year = 1963 
| pmid = 14089947
}}
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| last2 = McIntire | first2 = M. S. 
| title = Lead Poisoning During Pregnancy. Fetal Tolerance of Calcium Disodium Edetate 
| journal = American journal of diseases of children (1960) 
| volume = 108 
| pages = 436–439 
| year = 1964 
| pmid = 14186666
 | doi=10.1001/archpedi.1964.02090010438016
}}
* {{cite journal | author = Angle CR | year = 1966 | title = Acute renal failure | url = | journal = J Lancet | volume = 86 | issue = | pages = 355–362 }}
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| last2 = McIntire | first2 = M. S. 
| title = Evaluation of a poison information center 
| journal = The Journal-lancet 
| volume = 86 
| issue = 7 
| pages = 363–365 
| year = 1966 
| pmid = 5939583
}}
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| last2 = McIntire | first2 = M. S. 
| last3 = Moore | first3 = R. C. 
| title = Cloverleaf skull: Kleeblattschädel-deformity syndrome 
| journal = American journal of diseases of children (1960) 
| volume = 114 
| issue = 2 
| pages = 198–202 
| year = 1967 
| pmid = 4951548
 | doi=10.1001/archpedi.1967.02090230128018
}}
* {{cite journal |vauthors=Angle CR, McIntire MS, Zetterman RA | year = 1968 | title = CNS symptoms in childhood poisoning | url = | journal = Clin Toxicol | volume = 1 | issue = | pages = 19–29 }}
* {{cite journal |vauthors=Angle CR, McIntire MS, Meile R | year = 1968 | title = Neurologic sequelae of poisoning in children | url = | journal = J Pediat | volume = 73 | issue = | pages = 531–539 | doi=10.1016/s0022-3476(68)80268-9}}
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| last2 = McIntire | first2 = M. S. 
| title = Persistent dystonia in a brain-damaged child after ingestion of phenothiazine 
| journal = The Journal of Pediatrics 
| volume = 73 
| issue = 1 
| pages = 124–126 
| year = 1968 
| pmid = 5658620
 | doi=10.1016/s0022-3476(68)80050-2
}}
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| last2 = Glyn | first2 = M. 
| title = The value of a pediatric high intensity care unit 
| journal = The Nebraska state medical journal 
| volume = 54 
| issue = 11 
| pages = 737–740 
| year = 1969 
| pmid = 4242184
}}
* {{cite journal | author = Angle CR | year = 1971 | title = Symposium on iron poisoning | url = | journal = Clin Tox | volume = 4 | issue = | pages = 525–527 }}
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| last2 = McIntire | first2 = M. S. 
| title = Red cell lead, whole blood lead, and red cell enzymes 
| journal = Environmental Health Perspectives 
| volume = 7 
| pages = 133–137 
| year = 1974 
| pmid = 4364646 
| pmc = 1475113
 | doi=10.1289/ehp.747133
}}
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| last2 = Wermers | first2 = J. 
| title = Human poisoning with flea-dip concentrate 
| journal = Journal of the American Veterinary Medical Association 
| volume = 165 
| issue = 2 
| pages = 174–175 
| year = 1974 
| pmid = 4837827
}}
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| last2 = McIntire | first2 = M. S. 
| last3 = Vest | first3 = G. 
| title = Blood lead of Omaha school children--topographic correlation with industry, traffic and housing 
| journal = The Nebraska medical journal 
| volume = 60 
| issue = 4 
| pages = 97–102 
| year = 1975 
| pmid = 48205
}}
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| title = Locomotor skills and school accidents 
| journal = Pediatrics 
| volume = 56 
| issue = 5 
| pages = 819–822 
| year = 1975 
| pmid = 1196740
}}
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| last2 = McIntire | first2 = M. S. 
| last3 = Brunk | first3 = G. 
| doi = 10.1080/15287397709529587 
| title = Effect of anemia on blood and tissue lead in rats 
| journal = Journal of Toxicology and Environmental Health 
| volume = 3 
| issue = 3 
| pages = 557–563 
| year = 1977 
| pmid = 926207 
| pmc = 
}}
* {{cite journal |vauthors=Angle CR, Trembath EJ, Strond W | year = 1977 | title = The myelodysplasia and hydrocephalus program in Nebraska: A 15 year review of cost and benefits, Park I. | url = | journal = Nebr Med J | volume = 62 | issue = | pages = 359–361 }}
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| last2 = Trembath | first2 = E. J. 
| last3 = Strond | first3 = W. L. 
| title = The myelodysplasia and hydrocephalus program in Nebraska: A 15 year review of costs and benefits 
| journal = The Nebraska medical journal 
| volume = 62 
| issue = 11 
| pages = 391–393 
| year = 1977 
| pmid = 412114
}}
* Angle CR and McIntire MS:  Lead, mercury and cadmium:  toxicity in children.  ''Paediatrician'' 6:204-225, 1977.
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| last2 = McIntire | first2 = M. S. 
| title = Low level lead and inhibition of erythrocyte pyrimidine nucleotidase 
| journal = Environmental research 
| volume = 17 
| issue = 2 
| pages = 296–302 
| year = 1978 
| pmid = 233817
 | doi=10.1016/0013-9351(78)90032-4
}}
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| last2 = McIntire | first2 = M. S. 
| doi = 10.1080/15287397909529795 
| title = Environmental lead and children: The Omaha study 
| journal = Journal of Toxicology and Environmental Health 
| volume = 5 
| issue = 5 
| pages = 855–870 
| year = 1979 
| pmid = 583166 
| pmc = 
}}
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| last2 = Stohs | first2 = S. J. 
| last3 = McIntire | first3 = M. S. 
| last4 = Swanson | first4 = M. S. 
| last5 = Rovang | first5 = K. S. 
| title = Lead-induced accumulation of erythrocyte pyrimidine nucleotides in the rabbit 
| journal = Toxicology and applied pharmacology 
| volume = 54 
| issue = 1 
| pages = 161–167 
| year = 1980 
| pmid = 7394785 | doi=10.1016/0041-008x(80)90017-4
}}
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| title = The Department of Pediatrics, University of Nebraska Medical Center 
| journal = The Nebraska medical journal 
| volume = 66 
| issue = 3 
| pages = 53–54 
| year = 1981 
| pmid = 7231593
}}
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| last2 = McIntire | first2 = M. S. 
| last3 = Swanson | first3 = M. S. 
| last4 = Stohs | first4 = S. J. 
| title = Erythrocyte nucleotides in children--increased blood lead and cytidine triphosphate 
| journal = Pediatric research 
| volume = 16 
| issue = 4 Pt 1 
| pages = 331–334 
| year = 1982 
| pmid = 7079004 | doi=10.1203/00006450-198204000-00019
}}
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| last2 = O'Brien | first2 = T. P. 
| last3 = McIntire | first3 = M. S. 
| title = Adolescent self-poisoning: A nine-year followup 
| journal = Journal of developmental and behavioral pediatrics : JDBP 
| volume = 4 
| issue = 2 
| pages = 83–87 
| year = 1983 
| pmid = 6874961
 | doi=10.1097/00004703-198306000-00001
}}
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| last2 = Marcus | first2 = A. 
| last3 = Cheng | first3 = I. H. 
| last4 = McIntire | first4 = M. S. 
| title = Omaha childhood blood lead and environmental lead: A linear total exposure model 
| journal = Environmental research 
| volume = 35 
| issue = 1 
| pages = 160–170 
| year = 1984 
| pmid = 6489285 | doi=10.1016/0013-9351(84)90123-3
}}
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| last2 = Swanson | first2 = M. S. 
| last3 = Stohs | first3 = S. J. 
| last4 = Markin | first4 = R. S. 
| title = Abnormal erythrocyte pyrimidine nucleotides in uremic subjects 
| journal = Nephron 
| volume = 39 
| issue = 3 
| pages = 169–174 
| year = 1985 
| pmid = 2983249 | doi=10.1159/000183366
}}
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| last2 = Kuntzelman | first2 = D. R. 
| doi = 10.1080/15287398909531241 
| title = Increased erythrocyte protoporphyrins and blood lead—a pilot study of childhood growth patterns 
| journal = Journal of Toxicology and Environmental Health 
| volume = 26 
| issue = 2 
| pages = 149–156 
| year = 1989 
| pmid = 2921779 
| pmc = 
}}
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| last2 = Thomas | first2 = D. J. 
| last3 = Swanson | first3 = S. A. 
| title = Toxicity of cadmium to rat osteosarcoma cells (ROS 17/2.8): Protective effect of 1 alpha,25-dihydroxyvitamin D3 
| journal = Toxicology and applied pharmacology 
| volume = 103 
| issue = 1 
| pages = 113–120 
| year = 1990 
| pmid = 2315924
 | doi=10.1016/0041-008x(90)90267-x
}}
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| last2 = Thomas | first2 = D. J. 
| last3 = Swanson | first3 = S. A. 
| title = Lead inhibits the basal and stimulated responses of a rat osteoblast-like cell line ROS 17/2.8 to 1 alpha,25-dihydroxyvitamin D3 and IGF-I 
| journal = Toxicology and applied pharmacology 
| volume = 103 
| issue = 2 
| pages = 281–287 
| year = 1990 
| pmid = 2330589
 | doi=10.1016/0041-008x(90)90230-r
}}
* {{cite journal |vauthors=Angle CR, Thomas DJ, Swanson SA | year = 1993 | title = Osteotoxicity of cadmium and lead in HOS TE 85 and ROS 17/2.8 cells:  Relation to metallothionein induction and mitochondrial binding | url = | journal = BioMetals | volume = 5 | issue = | pages = 179–184 }}
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| last2 = Manton | first2 = W. I. 
| last3 = Stanek | first3 = K. L. 
| title = Stable isotope identification of lead sources in preschool children--the Omaha Study 
| journal = Journal of toxicology. Clinical toxicology 
| volume = 33 
| issue = 6 
| pages = 657–662 
| year = 1995 
| pmid = 8523488
 | doi=10.3109/15563659509010624
}}
* Angle CR, Swanson SA:  Arsenite enhances homocysteine-induced proliferation of fibroblasts in human aortic smooth muscle cells in B12 (Cobalamin) deficient media.  Submitted to Environmental Health Perspectives, July 1997.
* {{Cite journal 
| last1 = Angle | first1 = C. R. 
| title = Pitfalls of correlation of childhood blood lead and cognitive development 
| journal = Journal of toxicology. Clinical toxicology 
| volume = 40 
| issue = 4 
| pages = 521–522 
| year = 2002 
| pmid = 12217008
}}

== References ==
{{reflist}}

{{DEFAULTSORT:Angle, Carol Remmer}}
[[Category:American pediatricians]]
[[Category:Toxicologists]]
[[Category:Living people]]
[[Category:1927 births]]
[[Category:Place of birth missing (living people)]]
[[Category:University of Nebraska Medical Center faculty]]
[[Category:Wellesley College alumni]]
[[Category:Weill Cornell Medical College alumni]]