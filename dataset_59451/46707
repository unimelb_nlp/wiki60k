{{COI|date=December 2015}}
{{Infobox scientist
| name              = Andrew Briggs
| image             = Andrew Briggs for Wiki.jpg
| image_size        = 180px
| alt =
| caption           = Andrew Briggs, [[Pokhara]], 2011
| birth_name        = George Andrew Davidson Briggs
| birth_date        = {{birth date and age|1950|6|3|df=y}}
| birth_place       = [[Dorchester, Dorset|Dorchester]], Dorset, England
| residence         = [[United Kingdom]]
| nationality       = [[British people|British]]
| fields =  {{plainlist |* [[Materials Science]]  }}
| workplaces =  {{plainlist |* [[University of Oxford]]* [[University of Cambridge]]* [[Ecole polytechnique fédérale de Lausanne]]  }}
| alma_mater =  {{plainlist |* [[St Catherines College Oxford|St. Catherine’s College, Oxford]]* [[Queens’ College, Cambridge]]  }}
| doctoral_advisor  = [[David Tabor]]
| academic_advisors =
| doctoral_students = 
| known_for =  {{plainlist |* [[Acoustic Microscopy]]* [[Quantum]] [[Nanomaterials]]  }}
| author_abbrev_bot =
| author_abbrev_zoo =
| awards =  {{plainlist |* Holliday Prize * Buehler Technical Paper Merit Award for Excellence * Metrology for World Class Manufacturing Award * Honorary Fellow of the [[Royal Microscopical Society]]}}
| website = {{url|http://www.materials.ox.ac.uk/peoplepages/briggs.html}}
| spouse =  {{plainlist |* Diana née Johnson (m. 1981)}}
| children =  {{plainlist |* Felicity (b. 1983)* Elizabeth (b. 1985)}}
}}
'''George Andrew Davidson Briggs''' (born 1950) (known as '''Andrew Briggs''') is a British scientist.

He is Professor of [[Nanomaterials]] in the [[Department of Materials, University of Oxford|Department of Materials]] at the University of Oxford. He is best known for his early work in [[acoustic microscopy]] and his current work in materials for quantum technologies.<ref name="php">{{cite web|url=http://www.materials.ox.ac.uk/peoplepages/briggs.html |title=University of Oxford |publisher=Materials.ox.ac.uk |date= |accessdate=2014-01-20}}</ref>

== Early life and education ==

He was born in [[Dorchester, Dorset|Dorchester]], Dorset, son of [[David Briggs (English Headmaster)|David Briggs]],<ref name=WhoIsWho>[http://www.ukwhoswho.com/app?service=externalpagemethod&page=ArticleDisplay&method=view&sp=S/oupww/whoswho/U10000304 Who's Who]</ref> a classics teacher at [[Bryanston School]] Dorset, and later headmaster of [[King's College School|King’s College School]] Cambridge, and Mary (née Lormer),<ref name=WhoIsWho /> whose former maths pupils include [[Sir Timothy Gowers]] and [[Andrew Wiles|Sir Andrew Wiles]].  

He was educated at the [[Leys School]]<ref name=WhoIsWho /> Cambridge, he studied physics at [[St Catherines College Oxford|St. Catherine’s College, Oxford]],<ref name=WhoIsWho /> from 1968-1971 as the [[Clothworkers]]’ Scholar.<ref name=WhoIsWho /> From 1973-1976 he undertook research for a Ph.D. at the [[Cavendish Laboratory]].<ref name=WhoIsWho /> From 1976-1979 he studied [[Theology]] at [[Ridley Hall]]<ref name=WhoIsWho /> and [[Queens’ College, Cambridge]],<ref name=WhoIsWho /> where he won the Chase Prize for Greek.

== Career ==

From 1971-1973 after graduating from his first degree, he taught Physics and Religious Education at [[Canford School]],<ref name=WhoIsWho /> Dorset. In 1979 he was a Research Assistant in the Engineering Department at Cambridge University.<ref name=WhoIsWho /> In 1980 moved to Oxford as a Research Fellow in the Department of [[Metallurgy]]<ref name=WhoIsWho /> and from 1981 Lecturer in Physics at St Catherine’s College.<ref name=WhoIsWho /> In 1984 he was appointed Lecturer in Metallurgy and Science of Materials<ref name=WhoIsWho /> at the University of Oxford, in 1996 Reader in Materials, and in 1999 Professor of Materials.<ref name=WhoIsWho /> 

In 2002 he was elected to the newly created Chair of [[Nanomaterials]]<ref name="WhoIsWho" /> at the University of Oxford.<ref name="php" />  From 2002-2009 he was Director of the Quantum Information Processing Interdisciplinary Research Collaboration,<ref>{{cite web|url=http://www.materials.ox.ac.uk/research/qipirc.html |title=Quantum Information Processing Interdisciplinary Research Collaboration |publisher=Materials.ox.ac.uk |date=2004-04-01 |accessdate=2014-01-20}}</ref> and [[EPSRC]] Professorial Research Fellow.<ref name="JTF">{{cite web|url=http://www.templeton.org/who-we-are/our-team/board-of-advisors/andrew-briggs |title=John Templeton Foundation |publisher=Templeton.org |date= |accessdate=2014-01-20}}</ref> Since 2010 he has also been responsible for the preparation and evaluation of grant proposals to [[Templeton Foundation|Templeton World Charity Foundation]] which serves as a philanthropic catalyst for discoveries relating to the Big Questions of human purpose and ultimate reality.<ref name="JTF" /> He has initiated a large number of research projects and related activities around the world, in topics such as spiritual discovery through science, science as a component of theology, the power of information, freedom and free enterprise, and character development.

He has published over 575 books, papers, and articles; the majority in internationally reviewed journals.<ref name="php" /><ref name="JTF" /> His scientific research since taking up the Chair of Nanomaterials in 2002 has concentrated on materials with potential for building [[quantum computers]]. These include molecules in which the quantum states of electron and nuclear spins can be controlled with exquisite precision. Having established the key necessary phenomena in ensembles of large numbers of spins, since 2013 he has focussed on harnessing quantum properties in devices. He has also shown how the materials and techniques developed for technologies can be used for investigating the nature of reality in the context of quantum theory.

== Fellowships, memberships, and overseas appointments ==

*1984 Fellow of Wolfson College, Oxford<ref name=WhoIsWho />
*1992-2002 Professeur invité, [[Ecole Polytechnique Fédérale de Lausanne]]<ref name=WhoIsWho />
*2002 Visiting Professor, the University of New South Wales<ref name=WhoIsWho />
*2003 Professorial Fellow of St Anne’s College, Oxford, and Emeritus Fellow, Wolfson College, Oxford<ref name=WhoIsWho />
*2004 Fellow of the Institute of Physics<ref name=WhoIsWho />
*2005 Guest Professor, State Key Laboratory for Nanotechnology, [[Wuhan]], China<ref name=WhoIsWho />
*2011 [http://www.ae-info.org/ae/User/Briggs_Andrew Member Academia Europaea]
*2013 [http://www.scienceandchristianbelief.org/editorial_board.php Member International Society for Science and Religion].

== Awards and honours ==

* 1986 Holliday Prize, Institute of Metals, ''‘for his outstanding research and development in the field of scanning acoustic microscopy and for the application of this novel technique to the solution of materials problems.’''<ref name="php" /><ref>{{cite web|url=http://www.iom3.org/content/past-winners |title=Holliday Prize past winners |publisher=Iom3.org |date= |accessdate=2014-01-20}}</ref>
* 1994 Buehler Technical Paper Merit Award for Excellence. ''“Depth measurements of short cracks in perspex with the scanning acoustic microscope.”'' Materials Characterization 31, 115-126 (1993), reprinted in Materials Characterization 39, 653-644 (1997).
* 1999 Metrology for World Class Manufacturing Awards: Winner (with Dr O.V. Kolosov), Category 1, Frontier Science and Measurement. “Ultrasonic Force Microscopy (UFM)”, ''‘Kolosov and Briggs have demonstrated the effect on various materials and shown that UFM is capable of both high resolution and quantitative measurement.’''<ref name="php" />
* 1999 Honorary Fellow of the Royal Microscopical Society. ''‘This award is in recognition of your many outstanding achievements in various scanned probe microscopy techniques and their applications to the study of the mechanical and structural properties of surfaces over a very wide dimensional scale. Your recent development of the ultrasonic force microscope is an example of your innovative achievements.’''<ref name="php" /><ref>{{cite web|author=Royal Microscopical Society |url=http://www.rms.org.uk/About/AboutTheSoc/HonoraryFellows |title=Honorary Fellows of the Royal Microscopical Society |publisher=Rms.org.uk |date= |accessdate=2014-01-20}}</ref>
* 2007 Oxfordshire Science Writing Competition: 2nd Prize for article ''‘Molecules are Real.’''

== Other activities ==

Peer Review College of the [[Engineering and Physical Sciences Research Council]]; Fellowships Committee of The Royal Commission for the Exhibition of 1851; Engineering Review Panel of the [[Newton International Fellowships]]; Board of Management of the Ian Ramsey Centre; Advisory Board of the McDonald Centre; Board of Electors to the Wilde Lectureship in Natural and Comparative Religion; [[Liveryman]] of the [[Worshipful Company of Clothworkers]] and [[Freeman of the City of London]]; Editorial Board of [[Science & Christian Belief]]; International Board of Advisors of the [[John Templeton Foundation]].<ref name="JTF" />

== Interests ==

Andrew Briggs is a practising [[Christian]].<ref name=WhoIsWho />

== Bibliography ==

* ''An Introduction to Scanning Acoustic Microscopy''. Royal Microscopical Society Handbook 12, Oxford University Press (1985). Andrew Briggs.
* ''Acoustic Microscopy''. Oxford: Clarendon Press (1992). Andrew Briggs.
* ''The Science of New Materials''. Oxford: Blackwell (1992). Ed Andrew Briggs.
* ''Advances in Acoustic Microscopy 1''. New York: Plenum Press (1995). Ed Andrew Briggs.
* ''Advances in Acoustic Microscopy 2''. New York: Plenum Press (1996). Eds Andrew Briggs and Walter Arnold.
* [http://ukcatalogue.oup.com/product/9780199232734.do#.Umvv_pErf1o ''Acoustic Microscopy, 2nd Edition''. Oxford: Clarendon Press (2010). G.A.D. Briggs and O.V. Kolosov.]
* ''[[The Penultimate Curiosity|The Penultimate Curiosity: How science swims in the slipstream of ultimate questions]]''. Roger Wagner and Andrew Briggs, Oxford University Press (2016)

== References ==

{{reflist}}

== External links ==
* [https://web.archive.org/web/20131218185539/http://www.st-annes.ox.ac.uk/about/people/profile?tx_oxford_pi1%5Bstaff%5D=16&cHash=f6bab191dd18b96e1cb9e48b80fa8070 St Anne’s College, Oxford profile]
* [https://www.wolfson.ox.ac.uk/content/1142-prof-george-briggs Wolfson College, Oxford profile]
* [http://www.debretts.com/people/biographies/browse/b/22722/(George)%20Andrew%20Davidson+BRIGGS.aspx Debrett’s People of Today]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}
* [http://www.ae-info.org/ae/User/Briggs_Andrew/CV Academia Europaea]
* [http://www.scienceandchristianbelief.org/editorial_board.php Science and Christian Belief Editorial Board]
* [http://www.issr.org.uk/meet-issr-members/member/?member_id=192 International Society for Science and Religion ]

{{DEFAULTSORT:Briggs, Andrew}}
[[Category:1950 births]]
[[Category:People educated at The Leys School]]
[[Category:Alumni of St Catherine's College, Oxford]]
[[Category:Alumni of Queens' College, Cambridge]]
[[Category:British physicists]]
[[Category:British materials scientists]]
[[Category:British Christians]]
[[Category:Members of Academia Europaea]]
[[Category:Living people]]
[[Category:Academics of the University of Oxford]]
[[Category:Fellows of Wolfson College, Oxford]]
[[Category:Fellows of St Anne's College, Oxford]]
[[Category:Fellows of the Institute of Physics]]
[[Category:Fellows of the Royal Microscopical Society]]