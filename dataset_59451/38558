{{Other ships|USS Boxer}}
{{Use dmy dates|date=October 2012}}
{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=[[File:USS Boxer (CVA-21) underway off Korea in July 1953.jpg|300px|USS ''Boxer'']]
|Ship caption=''Boxer'' off the coast of Korea, 1953.
}}
{{Infobox ship career
|Hide header=
|Ship country=United States
|Ship flag={{USN flag|1969}}
|Ship name= USS ''Boxer''
|Ship namesake= HMS Boxer (1812)
|Ship ordered=1943
|Ship awarded= 
|Ship builder=[[Newport News Shipbuilding]]
|Ship original cost=
|Ship yard number=
|Ship way number=
|Ship laid down=13 September 1943
|Ship launched=14 December 1944
|Ship sponsor=
|Ship christened=
|Ship completed=
|Ship acquired=
|Ship commissioned=16 April 1945
|Ship recommissioned=
|Ship decommissioned=1 December 1969
|Ship maiden voyage=
|Ship in service=
|Ship out of service=
|Ship renamed=
|Ship reclassified=*CV to CVA October 1952
*CVA to CVS 15 November 1955
*CVS to LPH 30 January 1959
|Ship refit=
|Ship struck=1 December 1969
|Ship reinstated=
|Ship homeport=
|Ship identification=
|Ship motto=
|Ship nickname=
|Ship honors=Eight [[battle star]]s
|Ship captured=
|Ship fate=Sold for scrapping in February 1971 and scrapped at [[Kearny, New Jersey]]
|Ship status=
|Ship notes=
|Ship badge=
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship class={{Sclass-|Essex|aircraft carrier}}
|Ship displacement={{Ticonderoga class aircraft carrier displacement}}
|Ship length={{Ticonderoga class aircraft carrier length}}
|Ship beam={{Ticonderoga class aircraft carrier beam}}
|Ship height=
|Ship draft={{Ticonderoga class aircraft carrier draught}}
|Ship power=
|Ship propulsion={{Ticonderoga class aircraft carrier propulsion}}
|Ship speed={{Ticonderoga class aircraft carrier speed}}
|Ship range=
|Ship troops=
|Ship complement={{Ticonderoga class aircraft carrier complement}}
|Ship sensors=
|Ship EW=
|Ship armament={{Ticonderoga class aircraft carrier armament}}
|Ship armor={{Ticonderoga class aircraft carrier armour}}
|Ship aircraft={{Ticonderoga class aircraft carrier aircraft}}
|Ship aircraft facilities=
|Ship notes=
}}
|}

'''USS ''Boxer'' (CV/CVA/CVS-21, LPH-4)''' was one of 24 {{Sclass-|Essex|aircraft carrier}}s of the [[United States Navy]], and the fifth ship to be named for {{HMS|Boxer|1812|6}}. She was launched on 14 December 1944 and christened by the daughter of a [[US Senator]] from [[Louisiana]].

[[Ship commissioning|Commissioned]] too late to see any combat in World War II, ''Boxer'' spent much of her career in the Pacific Ocean seeing 10 [[Tour of duty|tours]] in the western Pacific. Her initial duties involved mostly training and exercises, including launching the first carrier-based [[jet aircraft]], but [[demobilization]] prevented much activity in the late 1940s. At the outbreak of the [[Korean War]], she was used as an aircraft transport before arriving off Korean waters as the third U.S. carrier to join the force. She supported the [[Inchon landings]] and subsequent invasion of [[North Korea]], and was among the ships that provided support during the Chinese counteroffensive against an under-prepared and spread out United Nations (UN) force. She saw three subsequent combat tours in Korea conducting [[close air support]] and [[strategic bombing]] in support of UN ground troops fighting along the [[38th parallel north|38th parallel]], as the battles lines had largely solidified by this time. She was awarded eight [[battle star]]s for her service in Korea.

After the Korean War, ''Boxer'' saw a variety of duties, including as an [[anti-submarine warfare carrier]] and an [[amphibious assault]] platform. She participated in a number of training exercises including [[Operation Hardtack I|Operation Hardtack]] and [[Operation Steel Pike]], as well as several contingencies including [[Operation Powerpack]] and the [[Cuban Missile Crisis]]. In her later years, she served as a pickup ship for spacecraft during the [[Apollo program]] as well as an aircraft transport to troops during the [[Vietnam War]].

Although she was extensively modified internally as part of her conversion to an [[Landing Platform Helicopter]] (LPH), external modifications were minor, so throughout her career ''Boxer'' retained the classic appearance of a World War II ''Essex''-class ship. She was decommissioned on 1 December 1969 after 25 years of service and sold for scrap.

== Construction ==
{{main|Essex-class aircraft carrier}}

''Boxer'' was one of 24 ''Essex''-class ships to be completed, among the largest and most numerous [[capital ship]]s produced for World War II.<ref name="Essex">{{citation|title=''Essex''-class Aircraft Carrier |url=http://www.history.navy.mil/photos/usnshtp/cv/cv9cl.htm |publisher=[[United States Navy]] |date=6 June 2006 |accessdate=30 November 2011}}</ref> She was ordered  in 1943.<ref name="Ticonderoga">{{citation|url=http://www.history.navy.mil/photos/usnshtp/cv/cv14cl.htm |title=''Ticonderoga''-class Aircraft Carrier |publisher=[[United States Navy]] |date=8 October 2001 |accessdate=30 November 2011}}</ref>

[[File:USS Boxer launch 1945.jpg|thumb|left|''Boxer'' is launched, 14 December 1944.]]
The ship was one of the [[Essex-class aircraft carrier#The "long-hull" Essexes|"long-hull"]] designs of the class, which had begun production after March 1943. This "long hull" variant involved lengthening the bow above the waterline into a "clipper" form. The increased rake and flare provided deck space for two quadruple 40&nbsp;mm mounts; these units also had the [[flight deck]] slightly shortened forward to provide better arcs of fire.<ref name="Sowinski 1980 30">{{harvnb|Sowinski|1980|p=30}}</ref> Of the ''Essex''-class ships laid down after 1942, only {{USS|Bon Homme Richard|CV-31|2}} followed the original "short bow" design. The later ships have been variously referred to as the "long-bow units",<ref name="Sowinski 1980 30"/><ref>{{harvnb|Raven|1988|p=42}}</ref> the "long-hull group",<ref>{{harvnb|Fahey|1950|p=5}}</ref><ref name=Fried151>{{harvnb|Friedman|1983|p=151}}</ref> or the "''Ticonderoga'' class".<ref name="Ticonderoga"/><ref name=StJohn11>{{harvnb|St. John|2000|p=11}}</ref> However, the U.S. Navy never maintained any institutional distinction between the long-hull and short-hull members of the ''Essex'' class, and postwar refits and upgrades were applied to both groups equally.<ref name="StJohn13"/>

Like other "long-hull" ''Essex''-class carriers, ''Boxer'' had a [[Displacement (ship)|displacement]] of {{convert|27100|t}}. She had an [[Length overall|overall length]] of {{convert|888|ft}}, a [[Beam (nautical)|beam]] of {{convert|93|ft}} and a [[draft (hull)|draft]] of {{convert|28|ft}}.<ref name="StJohn14">{{harvnb|St. John|2000|p=14}}</ref> The ship was powered by eight 600 psi [[Babcock & Wilcox]] boilers, and Westinghouse geared [[steam turbines]] that developed {{convert|150,000|shp|kW|lk=in}} that turned four propellers. Like other ''Essex''-class carriers, she had a maximum speed of {{convert|33|kn|lk=in}}. The ship had a total crew complement of 3,448. Like other ''Essex''-class ships, she could be armed with 12 [[5-inch (127 mm)/38 caliber guns]] arrayed in four pairs and four single emplacements, as well as eight quadruple [[Bofors 40 mm gun]]s and 46 [[Oerlikon 20 mm cannon]]s. However, unlike her [[sister ship|sisters]], ''Boxer'' was armed instead with 72 40&nbsp;mm guns and 35 20&nbsp;mm cannons.<ref name=StJohn15>{{harvnb|St. John|2000|p=15}}</ref>

Her [[keel]] was laid on 13 September 1943 by the [[Newport News Shipbuilding Company]] at its facility in [[Newport News, Virginia]].<ref name="StJohn15"/> She was the fifth ship of the US Navy to be named ''Boxer'', after {{HMS|Boxer|1812|6}}, which had been captured by the U.S. during the [[War of 1812]]. The last ship to bear the name had been a [[USS Boxer (1905)|training ship]] in 1905.<ref name="StJohn13">{{harvnb|St. John|2000|p=13}}</ref> The new carrier was launched on 14 December 1944 and she was [[Ship naming and launching|christened]] by Ruth D. Overton, the daughter of [[U.S. Senator]] [[John H. Overton]]. The ship's cost is estimated at $68,000,000 to $78,000,000.<ref name="StJohn15"/>

==Service history==
[[File:FJ-1 VF-5A CV-21 Mar48 NAN3-63.jpg|thumb|[[North American FJ-1 Fury]] launches from ''Boxer'' in March 1948, the first time an all-jet aircraft was launched from an American aircraft carrier at sea.<ref group="N" name="jet"/>]]
''Boxer'' was commissioned on 16 April 1945 under the command of Captain D. F. Smith. She subsequently began [[sea trial]]s and a [[shakedown cruise]]. Before these were complete, the [[Empire of Japan]] surrendered on [[V-J Day]], marking the end of World War II before ''Boxer'' could participate. She joined the [[United States Pacific Fleet|Pacific Fleet]] at San Diego in August 1945 and the next month she steamed for [[Guam]], becoming the [[flagship]] of [[Task Force 77 (U.S. Navy)|Task Force 77]], a position she held until 23 August 1946. During this tour, she visited Japan, [[Okinawa]], the [[Philippines]], and China.<ref name="StJohn15"/>

She returned to San Francisco on 10 September 1946, embarked [[Carrier Air Group 19]] flying the [[Grumman F8F Bearcat]] fighter. With this complement, ''Boxer'' began a series of peacetime patrols and training missions off the coast of California during a relatively uneventful period during 1947.<ref name="StJohn17">{{harvnb|St. John|2000|p=17}}</ref> In spite of manning difficulties brought on by the [[demobilization]] of the US military after World War II, ''Boxer'' remained active in Pacific readiness drills around the West Coast and Hawaii. In 1948, she conducted a number of short cruises with [[US Navy Reserve]] personnel. On 10 March 1948, a [[North American FJ-1 Fury]] launched from ''Boxer'', the first such launch of an [[jet aircraft|all-jet aircraft]] from an American carrier,<ref group="N" name="jet">The first aircraft with a jet engine to operate from an aircraft carrier was the unusual composite propeller-jet [[Ryan FR Fireball]], but it normally flew primarily under prop power during takeoff and landing. The first-ever all-jet carrier operations were conducted on 4 December 1945 using a [[de Havilland Vampire|de Havilland Sea Vampire]] piloted by [[Royal Navy]] [[Captain (naval)|Captain]] [[Eric Brown (pilot)|Eric "Winkle" Brown]] from {{HMS|Ocean|R68|6}}. The first all-jet aircraft to take off and land from an American carrier was a [[McDonnell FH-1 Phantom|McDonnell XFD-1 Phantom]] on 21 July 1946 from {{USS|Franklin D. Roosevelt|CV-42|6}}, but the tests were not conducted under operational conditions.</ref> which allowed subsequent tests of jet aircraft carrier doctrine. For the remainder of 1948 and 1949, she participated in numerous battle drills and acted as a training carrier for jet aircraft pilots.<ref name="StJohn18">{{harvnb|St. John|2000|p=18}}</ref>

She was dispatched to the Far East on another tour on 11 January 1950. She joined the [[United States Seventh Fleet|7th Fleet]] in the region, making a goodwill visit to [[South Korea]] and entertaining South Korean president [[Syngman Rhee]] and his wife [[Franziska Donner]]. and at the end of the tour returned to San Diego on 25 June 1950, the same day as the outbreak of the [[Korean War]].<ref name="StJohn18"/> At the time, she was overdue for a maintenance overhaul, but she did not have time to complete it before being dispatched again.<ref name="Mar87"/>

===Korean War===
[[File:USS Boxer (CV-21) F-51 Mustangs.jpg|thumb|left|''Boxer'' brings aboard [[United States Air Force]] [[North American F-51 Mustang]]s at [[Naval Air Station Alameda]], [[California]], for transportation to East Asia in July 1950 during the early days of the [[Korean War]].]]
[[File:F4U-4 VF-884 CV-21 4Sep1951.jpeg|thumb|left|[[Vought F4U Corsair]]s from Air Group 101 depart from ''Boxer'' for a mission in Korea, 1951. One of these ("416") survived the war and is airworthy as of 2016.<ref>{{Cite web|url=http://www.koreanwarhero.com|title=Korean War Hero Official Website - Jim Tobul Airshows|website=www.koreanwarhero.com|access-date=2016-10-27}}</ref>]]
With the outbreak of the Korean War, the U.S. forces in the Far East had an urgent need for supplies and aircraft. The only aircraft carriers near Korea were {{USS|Valley Forge|CV-45|6}} and {{HMS|Triumph|R16|6}}.<ref name="Mar35">{{harvnb|Marolda|2007|p=35}}</ref> ''Boxer'' was ordered into service to ferry aircraft from California to the fighting on the [[Korean Peninsula]]. She made a record-breaking crossing of the Pacific Ocean,<ref name="Mar65">{{harvnb|Marolda|2007|p=65}}</ref> leaving [[Alameda, California]] on 14 July 1950 and arriving at [[Yokosuka, Japan]] on 23 July, a trip of 8 days and 7 hours. She carried one hundred forty-five [[North American P-51 Mustang]]s and six [[Stinson L-5 Sentinel]]s of the [[United States Air Force]] destined for the [[Far East Air Force (United States)|Far East Air Force]] as well as 19 Navy aircraft, 1,012 Air Force support personnel, and {{convert|2000|t}} of supplies for the United Nations troops fighting the [[North Korea]]n invasion of South Korea, including crucially needed spare parts and ordnance.<ref name="StJohn18"/> Many of this equipment had been taken from [[Air National Guard]] units in the United States because of a general shortage of materiel.<ref name="Mar299">{{harvnb|Marolda|2007|p=299}}</ref> She began her return trip from Yokosuka on 27 July and arrived back in California on 4 August, for a trip of 7 days, 10 hours and 36 minutes, again breaking the record for a trans-Pacific cruise.<ref name="StJohn18"/> She carried no jet aircraft, though, because they were deemed too fuel inefficient for the initial defense mission in Korea.<ref name="Mar37">{{harvnb|Marolda|2007|p=37}}</ref> By the time ''Boxer'' arrived in Korea, the UN forces had established superiority in the air and sea.<ref name="Alex126">{{harvnb|Alexander|2003|p=126}}</ref>

After rapid repairs in California, ''Boxer'' embarked [[Carrier Air Group 2]], flying the [[Vought F4U Corsair]] propeller driven fighter-bomber, and departed again for Korea on 24 August, this time in a combat role.<ref name="StJohn19"/> She had 110 aircraft aboard, intended to complement the hundreds of aircraft already operating in Korea.<ref name="Alex195">{{harvnb|Alexander|2003|p=195}}</ref> En route to the peninsula, the carrier narrowly avoided [[Typhoon Kezia]] which slowed her trip.<ref name="Mar87">{{harvnb|Marolda|2007|p=87}}</ref> She was the fourth aircraft carrier to arrive in Korea to participate in the war, after ''Triumph'' and ''Valley Forge'' had arrived in June and {{USS|Philippine Sea|CV-47|6}} followed in early August.<ref name="Mar39">{{harvnb|Marolda|2007|p=39}}</ref> She arrived too late to participate in the [[Battle of Pusan Perimeter]], but instead she was ordered to join a flotilla of 230 US ships which would participate in [[Operation Chromite]],<ref name="Mar201">{{harvnb|Marolda|2007|p=201}}</ref> the UN counterattack at [[Inchon]]. On 15 September, she supported the landings by sending her aircraft in a [[close air support]] role, blocking North Korean reinforcements and communication to prevent them from countering the attack.<ref name="StJohn19"/><ref name="Mar304">{{harvnb|Marolda|2007|p=304}}</ref> However, early in the operation, her propulsion system was damaged when a [[reduction gear]] in the ship's engine broke, a casualty of her overdue maintenance. The ship's engineers worked around the problem to keep the carrier in operation, but she was limited to 26 knots.<ref name="Mar87"/>

[[File:USS Boxer fire 1952.jpg|thumb|250px|Crews battle a fire aboard ''Boxer''{{'}}s flight deck in August 1952. The fire, started by a fuel tank, killed 8 and damaged 18 aircraft.]]
She continued this role as the UN troops [[Second Battle of Seoul|recaptured Seoul]] days later. ''Boxer'' continued this support as UN troops advanced north and into North Korea, but departed for the United States on 11 November for refit and overhaul.<ref name="StJohn19"/> US military commanders believed the war in Korea was over, and had ordered a number of other carriers out of the area<ref name="Mar168"/> and were subsequently under-prepared at the beginning of the [[Chosin Reservoir Campaign]] when the Chinese [[People's Liberation Army]] entered the war against the UN.<ref name="StJohn19">{{harvnb|St. John|2000|p=19}}</ref> Battlefield commanders requested ''Boxer'' return to Korea as soon as possible,<ref name="Mar167">{{harvnb|Marolda|2007|p=167}}</ref> but she did not immediately return as commanders feared it might reduce the Navy's ability to respond if another conflict or emergency broke out elsewhere.<ref name="Mar168">{{harvnb|Marolda|2007|p=168}}</ref>

''Boxer''{{'}}s propulsion problems required extensive repair so she returned to San Diego to conduct them.<ref name="Mar307">{{harvnb|Marolda|2007|p=307}}</ref> Upon arrival, she offloaded Air Group 2, which then embarked for Korea again aboard ''Valley Forge''.<ref name="Mar310">{{harvnb|Marolda|2007|p=310}}</ref> After a repair and refit in California, ''Boxer'' was prepared for a second tour in Korea. She embarked [[CVG-101|Carrier Air Group 101]]. The group was composed of Navy Reserve squadrons from [[Dallas, Texas]], [[Glenview, Cook County, Illinois|Glenview, Illinois]], [[Memphis, Tennessee]] and [[Olathe, Kansas]], and most of its pilots were reservists who had been called to [[active duty]]. She rejoined Task Force 77, and began operations in Korea on 29 March 1951, and her squadrons were the first Naval Reserve pilots to launch strikes in Korea. Most of these missions were [[airstrikes]] against Chinese ground forces along the [[38th parallel north|38th parallel]], and this duty lasted until 24 October 1951.<ref name="CV21"/> During this time, the carrier operated around "Point Oboe", an area {{convert|125|mi}} off the coast of [[Wonsan]]. They would withdraw another {{convert|50|mi}} east when they needed replenishment or refueling.<ref name="Mar301">{{harvnb|Marolda|2007|p=301}}</ref> A large destroyer screen protected the carriers, though [[MiG-15]] attacks against them did not occur.<ref name="Alex357">{{harvnb|Alexander|2003|p=357}}</ref>

After another period of rest and refits, ''Boxer'' departed California 8 February 1952 for her third tour in Korea, with Carrier Air Group 2 embarked, consisting of F9F in VF-24, F4U in VF 63 and VF-64, and AD in VF-65.<ref>''The Sea War in Korea'', Cagle and Manson, US Naval Institute, 1957, page 500</ref> Rejoining Task Force 77, her missions during this tour consisted primarily of [[strategic bombing]] against targets in North Korea, as the front lines in the war had largely solidified along the 38th Parallel. On 23 and 24 June, her planes conducted [[Attack on the Sui-ho Dam|strikes against the Sui-ho hydro-electric complex]] in conjunction with {{USS|Princeton|CV-37|2}}, {{USS|Bon Homme Richard|CV-31|2}} and ''Philippine Sea''.<ref name="Mar332">{{harvnb|Marolda|2007|p=332}}</ref>

[[File:USS Boxer (CVA-21) in port in 1954.jpg|thumb|left|''Boxer'' in San Francisco after her return from the Korean War in November 1953.]]
On 5 August 1952, a fire broke out on the hangar deck of ''Boxer'' at 05:30 when a fuel tank of an aircraft caught fire while the ship was conducting combat operations in the [[Sea of Japan]]. The fire raged on the carrier's hangar deck for 4–5 hours before being extinguished. The final total of casualties was 8 dead, 1 missing, 1 critically injured, 1 seriously burned and some 70 overcome by smoke.  Of the 63 who had gone over the side, all were rescued and returned to the ship. Eighteen aircraft, mostly [[Grumman F9F Panther|Grumman F9F-2 Panther]]s, were damaged or destroyed. She steamed for Yokosuka for emergency repairs from 11 to 23 August.<ref name="CV21"/> She returned to the Korean theatre, and from 28 August to 2 September she tested a new weapons system, with six radio guided [[Grumman F6F Hellcat]]s loaded with {{convert|1000|lb|kg|adj=on}} bombs guided to targets, resulting in two hits and one near miss. They are considered to be the first [[guided missile]]s to be launched from a carrier in combat. On 1 September her aircraft also took part in a large bombing mission of an oil refinery near Aoji, on the [[Manchuria]]n border.<ref name="Mar333">{{harvnb|Marolda|2007|p=333}}</ref> She returned to San Francisco for more extensive repairs on 25 September. In October 1952, she was re-designated CVA-21, denoting an "attack aircraft carrier."<ref name="CV21"/>

Following extensive repairs, she steamed for Korea again on 30 March 1953, and resumed operations a month later with her Corsairs embarked.<ref name="CV21"/> Her missions around this time were generally strategic bombing missions, however the effectiveness of these final missions were mixed, with some failing to achieve strategic results.<ref name="Mar348">{{harvnb|Marolda|2007|p=348}}</ref> She also provided close air support for UN troops for the final weeks of the war before an armistice was reached at [[Panmunjom]] in July 1953, ending major combat operations in Korea. During this time, the two sides often conducted costly attacks in order to strengthen their bargaining positions at the negotiating table. ''Boxer'' remained in Korean waters until November 1953.<ref name="Mar342">{{harvnb|Marolda|2007|p=342}}</ref> She received eight [[battle star]]s for her service in Korea.<ref name="StJohn19"/>

In 1951 she appears in the film ''Submarine Command'', with William Bendix, and William Holden, then carrying a compliment of helicopters.

=== Post-Korea ===
Following the Korean War, ''Boxer'' returned to the United States. She conducted a tour of the Pacific throughout 1954 which was relatively uneventful, followed by a rest in the United States and another tour in the Pacific in late 1955 and early 1956, which was similarly uneventful.<ref name="USN">{{citation|url=http://www.history.navy.mil/photos/sh-usn/usnsh-b/cv21.htm |title=USS Boxer 1945–1971 |publisher=United States Navy |date=7 October 2001 |accessdate=14 December 2011}}</ref>

[[File:USS Boxer LPH-4 loaded with helicopters of the 1st Cavalry Division, 1965.jpg|thumb|left|''Boxer'' loaded with 200 helicopters of the 1st Cavalry Division bound for the [[Vietnam War]], 1965]]
She was converted to an [[anti-submarine warfare carrier]] in early 1956, re-designated CVS-21. She completed another tour of the western Pacific in late 1956 and early 1957, which was her tenth and final deployment to the area. In late 1957, the navy began experimenting with the concept of a carrier operating entirely with [[attack helicopter]]s, and ''Boxer'' was used to test the concept.<ref name="USN"/>

In 1958, ''Boxer'' was the flagship during [[Operation Hardtack I|Operation Hardtack]], a series of [[nuclear weapons]] tests in the central Pacific.<ref name="USN"/> Later that year, she was transferred to the [[United States Atlantic Fleet|Atlantic Fleet]], and became part of a new [[amphibious assault]] squadron with four [[Landing Ship Tank]] vessels equipped with helicopter platforms.<ref name="USN"/> The experimental concept would allow for rapid deployment of [[US Marine Corps]] personnel and helicopter squadrons. For the remainder of 1958 elements of this force were organized aboard ''Boxer'' and she was reclassified LPH-4, denoting a "[[Landing Platform Helicopter]]", on 30 January 1959.<ref name="CV21"/>

For the next 10 years, ''Boxer'' operated mainly out of the Caribbean as an amphibious assault carrier. During this duty, she was on station during the 1962 [[Cuban Missile Crisis]]. In 1964, she undertook her first tour to the Mediterranean when she took part in [[Operation Steel Pike]], the largest amphibious exercise in history.<ref name="USN"/>

[[File:USS Boxer (LPH-4) at anchor off Vieques Island on 5 December 1966 (USN 1125049).jpg|thumb|''Boxer'' as an experimental helicopter carrier operating off [[Puerto Rico]] in 1966.]]
With two [[Landing Ship Dock]]s, ''Boxer'' was dispatched to [[Hispaniola]] on 29 August 1964 on a humanitarian mission to aid [[Haiti]] and the [[Dominican Republic]] whose infrastructure had been damaged by [[Hurricane Cleo]]. The ships provided medical aid and helped to evacuate civilians displaced by the storm.<ref name="CV21"/> On 27 April 1965 ''Boxer'' returned to the Dominican Republic with [[HMM-264|Helicopter Squadron 264]] and a complement of Marines. They evacuated about 1,000 US nationals from the country in the wake of a revolution in the country. It was a part of [[Operation Powerpack]] which would eventually see the US occupation of that country.<ref name="CV21"/> Later in 1965, she was used as a transport vessel for the [[Vietnam War]]. The carrier transported 200 helicopters of the US Army's [[US 1st Cavalry Division|1st Cavalry Division]] to [[South Vietnam]]. She made a second trip to Vietnam in early 1966 when she transported Marine Corps aircraft to South Vietnam. However, she did not participate in combat operations during that war.<ref name="USN"/>

On 26 February 1966, ''Boxer'' recovered [[AS-201]], an unmanned test flight of the [[Apollo program]] which had launched from [[Cape Canaveral Air Force Station|Cape Kennedy, Florida]] aboard a [[Saturn 1B]] rocket. The capsule had landed {{convert|200|mi}} east of [[Ascension Island]] and one of ''Boxer'''s helicopters picked it up.<ref name="CV21"/> From 16&ndash;17 March 1966, ''Boxer'' was the designated Atlantic prime recovery ship for [[Gemini 8]], although {{USS|Leonard F. Mason}} recovered the spacecraft and two crewmen.<ref>Blair, Don ''Splashdown!: NASA and the Navy'''' (2004) Turner Publishing Company, p. 29.</ref>

She was decommissioned on 1 December 1969 after 25 years of service, and she was stricken from the [[Naval Vessel Register]]. 
She was sold for scrap on 13 March 1971.<ref name="CV21">{{citation|title=USS Boxer (CV-21)|work=Dictionary of American Fighting Ships and United States Naval Aviation, 1910–1995 |url=http://www.navy.mil/navydata/navy_legacy_hr.asp?id=38 |publisher=US Navy |accessdate=13 December 2011}}</ref>

==See also==
{{Portal|United States Navy}}
* [[List of aircraft carriers]]
* [[List of aircraft carriers of the United States Navy]]
* [[List of World War II ships]]

==References==

=== Notes ===
{{reflist|group=N}}

=== Citations ===
{{reflist|2}}

=== Sources ===
{{Refbegin}}
{{DANFS|http://www.history.navy.mil/research/histories/ship-histories/danfs/b/boxer-v.html}}
*{{citation|first=Bevin |last=Alexander |authorlink=Bevin Alexander |title=Korea: The First War We Lost |publisher=Hippocrene Books |year=2003 |isbn=978-0-7818-1019-7 |location=New York City}}
*{{citation|last= Fahey |first= James |authorlink=James Charles Fahey |title= The Ships and Aircraft of the U.S. Fleet (Sixth Edition) |year= 1950 |publisher= Ships and Aircraft |location= Washington, D.C. |isbn= 978-0-87021-647-3 }}
*{{citation|last= Friedman |first= Norman |title= U.S. Aircraft Carriers: An Illustrated Design History  |year= 1983 |publisher= Naval Institute Press |location= Annapolis, Maryland |isbn= 0-87021-739-9 }}
*{{citation|first=Edward |last=Marolda |title=The US Navy in the Korean War |year=2007 |publisher=Naval Institute Press |isbn=978-1-59114-487-8 |location=Annapolis, Maryland}}
*{{citation|last= Raven |first= Alan |title= Essex-Class Carriers |year= 1988 |publisher= Naval Institute Press |location= Annapolis, Maryland |isbn= 978-0-87021-021-1 }}
*{{cite book|last= Sowinski|first= Lawrence|chapter= Champions of the Pacific: The Essex Class Carriers |title= Warship, Volume II|editor1-last=Gardiner |editor1-first=Robert|editor2-last=Preston |editor2-first=Antony |year= 1980 |publisher= Naval Institute Press |location= Annapolis, Maryland |isbn= 0-87021-976-6}}
*{{citation|last= St. John |first= Philip|title= USS Boxer |year= 2000 |publisher= Turner Publishing Company |location= Nashville, Tennessee |isbn= 978-1-56311-610-0}}
{{refend}}

==External links==
{{Commons category|USS ''Boxer'' (CV-21)}}
*[http://www.ussboxer.com USS ''Boxer'' Veterans Association]
*[http://www.history.navy.mil/photos/sh-usn/usnsh-b/cv21.htm Navy photographs of ''Boxer'' (CV-21)]
*[https://www.flickr.com/photos/telstar/2265110/in/set-56935/ USS ''Boxer'' in drydock] at [[Hunter's Point Naval Shipyard]]

<!-- non-breaking space to keep AWB drones from altering the space before the navbox-->

{{Essex class aircraft carrier}}
{{Good article}}

{{DEFAULTSORT:Boxer (CV-21)}}
[[Category:Essex-class aircraft carriers]]
[[Category:1944 ships]]
[[Category:World War II aircraft carriers of the United States]]
[[Category:Cold War aircraft carriers of the United States]]
[[Category:Cold War amphibious assault ships of the United States]]
[[Category:Korean War aircraft carriers of the United States]]
[[Category:Helicopter carriers]]
[[Category:Ship fires]]
[[Category:Ships built in Newport News, Virginia]]