'''Jorge Grundman''' Isla (born 1961) is a Spanish classical composer, [[Musicology|musicologist]], musician and professor who has helped to recover the music of [[Robert Kahn (composer)|Robert Kahn]] and [[Adalbert Gyrowetz]] among others through the non-profit music foundation created by him.

==Musical development==
Grundman began in the world of music when he was 12 years old and completed his first composition at the age of 14 while still at school. He started his studies in [[solfeggio]], piano and choir at the [[Madrid Royal Conservatory]] with Carmen Ledesma at the department of Professor [[Joaquín Soriano]]. At the beginning of his career, Grundman took part in pop groups where he was the singer, keyboard player, composer and [[Arrangement|arranger]]. His song "Yo lo intentaría una vez más" appeared in 1983 on Spanish FM radio charts. He left the musical scene until 1999 when he released "The Sons Of The Cold", a tune that topped the New Age charts on the old [[MP3.com]] site. From this moment on and due the popularity of the ''Payback for Playback'' program that MP3.com was running, Grundman started to collect funds with his music for [[Doctors Without Borders]]. He founded a record label named Non Profit Music and later a foundation with the same name, destined to promote contemporary consonant music<ref name="FanfareMagazine">''[[Fanfare (magazine)|Fanfare]]'', issue 33-4, March/April 2010</ref> and to collect funds for humanitarian causes.

His works have been performed and broadcast mainly in the United States, Canada, London, France, Japan, Brazil and Spain.

==Performers==
There are many soloists and ensembles which have performed the music of Grundman such as [[Brodsky Quartet]], [[Ara Malikian]], Daniel del Pino, Susana Cordón, Jirí Bárta, B3 Classic Trio, Arbós Trío, Blau Kamara Quartet, The Winchester Orchestra of San José, Orquesta Sinfónica Nacional of Ecuador, Orquesta de Cámara de España or Non Profit Music Chamber Orchestra. His pieces have been premiered in music halls such as Auditorio Nacional de Música of Madrid, Teatro Nacional of Brasilia, Trinity Cathedral of California, West Valley College Theater or Iglesia La Dolorosa of Quito, among others.

==Style==
The music Grundman writes do not belong to any avant-garde style. More near to tonal music than neo-romantic his music is predominant emotive due his extensive use of tonal and consonant minor chords. In his childhood days he was member of several pop groups in Spain and later he turn his music writing to the New Age music. He has also written music for film and TV shows, and just when he appears at the classical scene he makes a fusion of all this elements with his personal touch. His music has been compared with Sir [[Malcolm Arnold]],<ref>ClassicsToday, ''Four Sad Seasons Over Madrid'', NPM 0903, 2009</ref> [[Federico Mompou]] or even the film composer [[John Barry (composer)|John Barry]].<ref>MusicWeb International, "No Seasons", 2009</ref> In an interview with Phillip Scott for Fanfare Magazine, Grundman talks about the consonant music he writes: "...In the second half of the 20th century, many composers preferred to write for a small group of individuals and lost contact with the broader audience. The idea of music as a communicative art got ignored. Yet music is the only art that does not exist until it is performed. To achieve its maximum potential, the composer, the performer, and the audience have to speak the same language. In many countries, that cerebral approach to composing is now being forgotten and composers are returning to consonant music. By the way, I don’t mean consonant simply in its harmonic sense, but in the sense that all participants of the musical experience—composer, performer, and audience—are on the same level...".<ref name="FanfareMagazine" /> Also, his music has been featured in the magazine Sinfonía Virtual <ref>Virtual Symphony, Issue Nº24, January 2013, ISSN 1886-9505. http://www.sinfoniavirtual.com/revista/024/jorge_grundman_fenomenologia_concertante.php</ref> labeling it as "Fenomenología Concertante".

==Recent works==
In April 2011 Grundman is commissioned by the XVII Festival Internacional de Música of Toledo<ref>Forum Clasico http://www.forumclasico.es/RITMOOnLine/MusicaViva/tabid/61/ID/288/XVII-Festival-Internacional-de-Musica-de-Toledo.aspx</ref> and writes his sonata for flute and piano called ''Warhol in Springtime''. According to the sheet music this music was made like a collage of Andy Warhol [[Pop art]] pictures and must have a duration of 15 minutes, as a tribute to the Warhol famous sentence. On the other hand, his sonata for violin and piano, ''What Inspires Poetry'', dedicated to the composer [[Marjan Mozetich]], is broadcast on many American radio stations.

Grundman in 2011 dedicated to the Ukrainian pianist and composer [[Nikolai Kapustin]] ''A Walk across Adolescence'' for violin, cello and piano, and the composer accepted the dedication of Grundman´s work. The piece was premiered that same year at Toledo, Jaén, Badajoz, Madrid and many other sites by Daniel del Pino, Roman Patocka and Jirí Bárta or the Trío Arbós, which has been recently recorded it for ''Play it Again'', the last recording by the Arbós Trio.

In 2012, the [[Brodsky Quartet]] together with soprano Susana Cordon and percusionist Jaime Fernández Soriano recorded his string quartets: Surviving a Son's Suicide and God's Sketches.

In 2013, he recovered the chamber Latin oratorio with his "A Mortuis Resurgere: The Resurrection of Christ" for Soprano and String Quartet which was performed several times in this year and recorded again by Brodsky Quartet and Susana Cordón for Chandos Records.

==Principal works==
* Adagio for Clarinet in Bb, Cello and Piano (2003)
* String Trio Nº 3 (2005)
* Adagio for Flute, Cello and Piano (2005)
* Largo for Violin and String Orchestra (2006)
* Kyrie for Choir (2006)
* ''Concerto Sentido'' for Violin, Viola, Cello and String Orchestra (2007). Dedicated to Ara Malikian
* ''Four Sad Seasons Over Madrid'' for Soprano, Violin, Piano and String Orchestra (2008)
* ''What Inspires Poetry''. Sonate for Violin and Piano (2008). Dedicated to Marjan Mozetich
* ''Surviving a Son's Suicide''. String Quartet (2009)
* ''Warhol in Springtime''. Sonate for Flute and Piano (2011). Dedicated to Nikolai Kapustin
* ''A Walk across Adolescence''. Piano Trio (2011). Dedicated to Nikolai Kapustin
* ''God's Sketches'' for String Quartet, Soprano and Mallets (2011). Dedicated to Susana Cordón
* ''On Blondes and Detectives''. Cliché Music for String Quartet (2012). Dedicated to Brodsky Quartet
* ''Piano Quintet: The Toughest Decission of God'' (2012). Dedicated to Daniel del Pino
* ''A Mortuis Resurgere: The Resurrection of Christ'' for Soprano and String Quartet (2013)

==Awards==
The music of Jorge Grundman has been awarded or award nominated in many occasions. Grundman has won awards such as the Narcissus Awards of the United States in 2005 and the NAR 2004 nomination awards as “Best Contemporary Instrumental Album” for his work "We are the forthcoming past, take care of it". Also he has received the Best Instrumental Song at 12th [[Independent Music Awards]] for his string quartet "On Blondes and Detectives" dedicated to Brodsky Quartet.<ref>[http://www.independentmusicawards.com/ima/2013/12th-annual-independent-music-awards-winners-announced "12th Annual Independent Music Awards Winners Announced!"] Independent Music Awards, 11 June 2013. Retrieved on 4 Sept. 2013.</ref> In addition and for the first time in International Songwriting Competition 13-year history, a composer from Spain, Jorge Grundman, has won First Place in a category, taking home the prestigious award in Instrumental Music for his string quartet "On Blondes And Detectives".<ref>[http://www.timesofearth.com/read/spanish-artist-wins-major-us-music-competition.html "Spanish artist wins major US competition"] The Times of Earth.  Retrieved on 25 June 2014</ref> Grundman is also the first Spanish composer who has received two prizes from the Boston Metro Opera. His [[monodrama]] "Four Sad Seasons Over Madrid" for Soprano, Violin, Piano and String Orchestra received in 2014 the BMO Concert Award; and his [[monodrama]] "God's Sketches" for String Quartet, Soprano and Mallets has received the BMO Director's Choice Award.<ref>[http://contempofest.bostonmetroopera.com/uploads/1/3/3/4/1334805/2014_results_packet.pdf "Boston Metro Opera International Contempo Festival"] Boston Metro Opera.  Retrieved on 25 June 2014</ref> In addition, many of his music has been nominated directly or included in award nomination albums, like in 2013 the award nomination as Best Classical Album for "God's Sketches" performed by Brodsky Quartet, Susana Cordón and Jaime Fernández Soriano at 5th Spanish Independent Music Awards. In 2012 the album which includes his piano trio A Walk across Adolescence, "Play It Again", performed by Trio Arbos was award nominated as Best Classical Album at 4th Spanish Music Awards. In 2010 the album "No Seasons" performed by Ara Malikian, Daniel del Pino and Susana Cordon, which includes the monodrama written by Grundman "Four Sad Seasons Over Madrid" for Soprano, Violin, Piano and String Orchestra was nominated as Best Classical Performers at Spanish Music Awards. In addition in 2008 the album "Meeting with a friend" performed by Ara Malikian which includes his Concerto Sentido for Violin, Viola, Cello and String Orchestra was nominated also as Best Classical Performers in the Spanish Music Awards.

==References==
{{reflist}}

==External links==
* [http://www.grundman.org Biography, performances, discography and work index]
* [http://www.nonprofitmusic.org Non Profit Music Foundation]
* [http://connection.ebscohost.com/c/interviews/73747723/composer-vs-music-writer-vision-jorge-grundman Interview with the composer]
* [http://www.sinfoniavirtual.com/revista/024/jorge_grundman_fenomenologia_concertante.php Sinfonía Virtual. Jorge Grundman. Fenomenología Concertante]
* [http://open.spotify.com/user/grundman/playlist/3HiUgzA6rtW4f1MLLZtg4k Music of Jorge Grundman on Spotify]
* [https://www.youtube.com/watch?v=pQ7EsOQyzP8 A Walk across Adolescence performed by Trio Arbos]
* [https://www.youtube.com/watch?v=SkvARtNsCao What Inspires Poetry performed by Ara Malikian and Daniel del Pino]
* [https://www.youtube.com/watch?v=XwgVJ2ng4mU Four Sad Seasons Over Madrid performed by Susana Cordon, Ara Malikian, Daniel del pino and String Orchestra]

{{Authority control}}

{{DEFAULTSORT:Grundman, Jorge}}
[[Category:1961 births]]
[[Category:Place of birth missing (living people)]]
[[Category:Living people]]
[[Category:21st-century classical composers]]
[[Category:20th-century classical composers]]
[[Category:Spanish classical composers]]
[[Category:Spanish male classical composers]]
[[Category:Spanish musicologists]]
[[Category:20th-century Spanish musicians]]