<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name = A-17 / Nomad
 |image = Northrop A-17 front three-quarters view.jpg
 |caption = Northrop A-17
}}{{Infobox Aircraft Type
 |type = Ground attack
 |manufacturer = [[Northrop Corporation|Northrop]]
 |designer = [[Jack Northrop]]
 |first flight =
 |introduced = 1935
 |retired =
 |produced =
 |number built = 411
 |status =
 |unit cost =
 |primary user = [[United States Army Air Corps]]
 |more users = [[Swedish Air Force]] <br/>[[Royal Canadian Air Force]] <br/>[[South African Air Force]] 
 |developed from = [[Northrop Gamma]]
 |variants with their own articles = [[Douglas A-33]]
}}
|}

The '''Northrop A-17''', a development of the [[Northrop Gamma]] 2F was a two-seat, single-[[engine]], [[monoplane]], attack [[bomber]] built in 1935 by the [[Northrop Corporation]] for the [[U.S. Army Air Corps]]. A-17s used by air forces of the British Commonwealth designated the aircraft as the "Nomad."
{{TOC limit|limit=2}}

==Development and design==
The Northrop Gamma 2F was an attack bomber derivative of the Northrop Gamma transport aircraft, developed in parallel with the Northrop Gamma 2C, (of which one was built), designated the [[Northrop YA-13|YA-13]] and [[Northrop XA-16|XA-16]]. The Gamma 2F had a revised tail, cockpit canopy and wing flaps compared with the Gamma 2C, and was fitted with new semi-retractable landing gear. It was delivered to the [[United States Army Air Corps]] for tests on 6 October 1934, and after modifications which included fitting with a conventional fixed landing gear, was accepted by the Air Corps.<ref name="Pelletierpt1p63-64">Pelletier ''Air Enthusiast'' May–June 1998. pp. 63-64.</ref> A total of 110 aircraft were ordered as the A-17 in 1935.<ref name="boeingA17">[http://www.boeing.com/history/mdc/a-17.htm "A-17/8A Light Attack Bomber."] ''Boeing''. Retrieved: 11 February 2008.</ref>

The resulting A-17 was equipped with perforated flaps, and had fixed landing gear with partial fairings. It was fitted with an internal fuselage bomb bay that carried fragmentation bombs and well as external bomb racks.

Northrop developed a new landing gear, this time completely retractable, producing the A-17A variant. This version was again purchased by the Army Air Corps, who placed orders for 129 aircraft.<ref name="Pelletierpt1p65">Pelletier ''Air Enthusiast'' May–June 1998, p. 65.</ref> By the time these were delivered, the Northrop Corporation had been taken over by [[Douglas Aircraft Company]], export models being known as the Douglas Model 8.

==Operational history==
[[File:A-17A cockpit - USAFM.jpg|thumb|A-17A cockpit]]

===United States===
The A-17 entered service in February 1936, and proved a reliable and popular aircraft.<ref name="Pelletierpt1p64-65">Pelletier ''Air Enthusiast'' May–June 1998, pp. 64–65.</ref> However, in 1938, the Air Corps decided that attack aircraft should be multi-engined, rendering the A-17 surplus to requirements.<ref name="a17A fact">[http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?fsID=343 Fact Sheet - A-17A] ''National Museum of the United States Air Force''. Retrieved: 12 February 2008.</ref>

From 14 December 1941, A-17s were used for coastal patrols by the [[59th Bombardment Squadron]] (Light) on the Pacific side of the [[Panama Canal]].<ref>Conaway, William. [http://www.planesandpilotsofww2.totalh.com/panama/vibchistorytem.htm "VI Bombardment Command History."] ''Planes and Pilots Of World War Two.'' Retrieved: 6 August 2011.</ref>

The last remaining A-17s, used as utility aircraft, were retired from USAAF service in 1944.<ref name="Pelletierpt1p67">Pelletier ''Air Enthusiast'' May–June 1998, p. 67.</ref>

===Other countries===

====Argentina====
Argentina purchased 30 Model 8A-2s in 1937 and received them between February and March 1938. Their serial numbers were between 348 and 377. These remained in frontline service until replaced by the [[I.Ae. 24 Calquin]], continuing in service as trainers and reconnaissance aircraft until their last flight in 1954.<ref name="Pelletierpt2p2">Pelletier ''Air Enthusiast'' September/October 1998, p. 2.</ref><ref name="Nuñez Padin">Bontti 2003, p. 21.</ref>

====Peru====
Peru ordered ten Model 8A-3Ps, these being delivered from 1938 onwards. These aircraft were used in combat by Peru in the [[Ecuadorian-Peruvian war]] of July 1941.<ref name="Pelletierpt2p6">Pelletier ''Air Enthusiast'' September/October 1998, p. 6.</ref>  The survivors of these aircraft were supplemented by 13 [[Douglas A-33|Model 8A-5]]s from Norway (see below), delivered via the United States in 1943 (designated '''A-33''').  These remained in service until 1958.<ref name="Pelletierpt2p6"/>

====Sweden====
The Swedish government  purchased a licence for production of a Mercury-powered version, building 63 B 5Bs and 31 B 5Cs, production taking place from 1938 to 1941. They were replaced in service  with the [[Swedish Air Force]] by [[Saab 17|SAAB 17]]s from 1944.<ref name="Pelletierpt2p12-13">Pelletier ''Air Enthusiast'' September/October 1998, pp. 12–13.</ref> The Swedish version was used as a [[dive bomber]] and as such it featured prominently in the 1941 film ''[[Första divisionen]]''.

====The Netherlands====
The Netherlands, in urgent need of modern combat aircraft, placed an order for 18 Model 8A-3Ns in 1939, with all being delivered by the end of the year. Used in a [[Fighter aircraft|fighter]] role for which they were unsuited, the majority were destroyed by [[Luftwaffe]] attacks on 10 May 1940, the first day of the [[Battle of the Netherlands|German invasion]].<ref name="Pelletierpt2p3-4">Pelletier ''Air Enthusiast'' September/October 1998, pp. 3–4.</ref>

====Iraq====
Iraq purchased 15 Model 8A-4s, in 1940. They were destroyed in the [[Anglo-Iraqi War]] in 1941.<ref name="Pelletierpt2p3">Pelletier ''Air Enthusiast'' September/October 1998, p. 3.</ref>

====Norway====
{{main|Douglas A-33}}
Norway ordered 36 [[Douglas A-33|Model 8A-5N]]s in 1940. These were not ready by the time of the [[Operation Weserübung|German Invasion of Norway]] and were diverted to the Norwegian training camp in Canada, which became known as ''[[Little Norway]]''.<ref name="Pelletierpt2p4">Pelletier ''Air Enthusiast'' September/October 1998, p. 4.</ref> Norway decided to sell 18 of these aircraft as surplus to Peru, but these were embargoed by the United States, who requisitioned the aircraft, using them as trainers, designating them the '''A-33'''. Norway sold their surviving aircraft to Peru in 1943.<ref name="Pelletierpt2p4-6">Pelletier ''Air Enthusiast'' September/October 1998, pp. 4, 6.</ref>

====Great Britain====
In June 1940, 93 ex-USAAC aircraft were purchased by France, and refurbished by Douglas, including being given new engines.<ref name="Pelletierpt2p3"/> These were not delivered before the [[fall of France]] and 61 were taken over by the [[British Purchasing Commission]] for the British Commonwealth use under the name '''Northrop Nomad Mk I'''.

====South Africa====
The RAF assessed the Northrop Nomad Mk Is as "obsolete" and sent them to [[South Africa]] for use as trainers, serialed AS440 to AS462, AS958 to AS976 and AW420 to AW438.<ref name="a17A fact"/><ref name="Donaldamericanp212">Donald 1995, p. 212.</ref>

====Canada====
The [[Royal Canadian Air Force]] received 32 Nomads that had been part of a French order of 93 aircraft. When [[Battle of France|France fell]] in 1940, this order was taken over by Great Britain who transferred 32 of the aircraft to Canada where they were used as advanced trainers and target tugs as part of the [[British Commonwealth Air Training Plan]].<ref name="a17A fact">[http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?id=2925 "Fact Sheet - A-17A."] ''National Museum of the United States Air Force''. Retrieved: 1 March 2008.</ref><ref>[http://www.historyofwar.org/articles/weapons_Northrop_A-17.html Northrop A-17] Retrieved: 16 October 2013</ref>
These were serialed 3490 to 3521; all were assigned to No. 3 Training Command RCAF.<ref name="Pelletierpt2p2"/>

==Variants==
[[File:A-17A 36-0207 USAFM.jpg|thumb|A-17A '''36-0207''']]
;A-17
:Initial production for USAAC.  Fixed gear, powered by 750&nbsp;hp (560&nbsp;kW) [[Pratt & Whitney R-1535]]-11 Twin Wasp Jr engine; 110 built.
;A-17A
:Revised version for USAAC with retractable gear and 825&nbsp;hp (615&nbsp;kW) R-1535-13 engine; 129 built.
;A-17AS
:Three seat staff transport version for USAAC. Powered by [[Pratt & Whitney R-1340]] Wasp engine; two built.
;Model 8A-1
:Export version for Sweden. Fixed gear. Two Douglas built prototypes (Swedish designation '''B 5A'''), followed by 63 licensed built (by [[AB Svenska Järnvägsverkstädernas Aeroplanavdelning|ASJA]]) '''B 5B''' aircraft powered by 920&nbsp;hp (686&nbsp;kW) [[Bristol Mercury]] XXIV engine; 31 similar '''B 5C''' built by [[Saab AB|SAAB]].  One 8A-1 was also purchased by [[Bristol Aeroplane Company|Bristol Aeroplane Co.]] in 1937 which was modified to test its new [[Bristol Hercules|Hercules]] radial engine.<ref>[http://www.flightglobal.com/pdfarchive/view/1937/1937%20-%202787.html "Something Up Its Sleeve."] ''Flight International'', 7 October 1937, p. 359.</ref>
;Model 8A-2
:Version for Argentina. Fitted with fixed gear, ventral gun position and powered by 840&nbsp;hp (626&nbsp;kW) [[Wright R-1820]]-G3 Cyclone; 30 built.
;Model 8A-3N
:Version of A-17A for Netherlands. Powered by 1,100&nbsp;hp (820&nbsp;kW) [[Pratt & Whitney R-1830]] Twin Wasp engine; 18 built.
;Model 8A-3P
:Version of A-17A for Peru. Powered by 1,000&nbsp;hp (746&nbsp;kW) R-1820 engine; ten built.
;Model 8A-4
:Version for Iraq, powered by a 1,000&nbsp;hp (746&nbsp;kW) R-1820-G103 engine; 15 built.
;Model 8A-5N
:Version for Norway, powered by 1,200&nbsp;hp (895&nbsp;kW) R-1830 engine; 36 built. Later impressed into USAAF service as '''[[Douglas A-33]]'''.

==Survivors==
*A-17A, U.S. Army Ser. No. ''36-0207'' c/n 234, ex-3rd Attack Group (Barksdale Field), On display at the [[National Museum of the United States Air Force]] at [[Wright-Patterson AFB]] in [[Dayton, Ohio]]<ref>[http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?id=343 "36-0207."] ''National Museum of the United States Air Force''. Retrieved: 6 August 2011.</ref>
*8A-3P '''4??''', ex-31<sup>o</sup> Escuadrón de Ataque y Reconocimiento. On display at the FAP museum, Las Palmas Peruvian Air Force Base.<ref>{{cite web|url=http://www.flankers-site.co.uk/Peru_2007.html |title=Museum FAP 8A-3P |publisher=Flankers-site.co.uk |date= |accessdate=2013-11-17}}</ref><ref>[http://www.webcitation.org/query?url=http://www.geocities.com/kwii62/8a.htm&date=2009-10-26+01:34:35 "8A-3P on display."] ''geocities.com.'' Retrieved: 6 August 2011.</ref>
* Nomad A-17A RCAF 3521 crashed in [[Lake Muskoka]], Ontario December 13, 1940. The wreck was found in July 2010. The crew's remains were recovered and recovery of the aircraft by the RCAF has been completed.<ref>{{cite web|url=http://www.rcaf-arc.forces.gc.ca/en/article-template-standard.page?doc=nomad-aircraft-recovery-completed/i2kafllh |title=Nomad Aircraft Recovery Completed|publisher=Royal Canadian Air Force Public Affairs |date= |accessdate=2014-11-22}}</ref>  The recovered aircraft will be put on display at the [[National Air Force Museum of Canada]], [[CFB Trenton|Trenton, Ontario]].<ref>[http://www.thestar.com/news/canada/2014/10/28/downed_second_world_war_plane_recovered_from_lake_muskoka.html Downed Second World War plane recovered from Lake Muskoka]</ref>

==Operators==
[[File:A-17 operators.png|thumb|Operators of the A-17]]
*{{ARG}}
** [[Fuerza Aérea Argentina]]
*** ''Grupo "A" de la [[Escuela de Aplicación de Aviación]]'' ("'A' Group, School of Aviation Administration"),  [[BAM El Palomar]]
*** ''[[Regimiento Aéreo Nº3]] de Bombardeo Liviano'' ("No.3 'Liviano' (Light Bombing) Air Regiment"), [[BAM El Plumerillo]]
*{{flag|Canada|1921}}
**[[Royal Canadian Air Force]]
*** No. 3 Training Command
*{{flag|Iraq|1924}} <ref name="Pelletierpt2p3">Pelletier ''Air Enthusiast'' September/October 1998, p. 3.</ref>
*{{NLD}}
**''[[Luchtvaartafdeeling]]''
*{{NOR}}
**[[Little Norway|Norwegian Training Unit]] <ref name="Pelletierpt2p4-6">Pelletier ''Air Enthusiast'' September/October 1998, pp. 4, 6.</ref>
*{{PER}}
*{{flag|South Africa|1928}}
*{{SWE}}
**[[Swedish Air Force]]
*{{flag|United States|1912}}
**[[United States Army Air Corps]]
***[[General Headquarters Air Force]]
***[[3d Operations Group|3d Attack Group]], [[Barksdale Field]]
***[[17th Training Group|17th Attack Group]], [[March Field]]
***[[16th Pursuit Group]], [[Albrook Field]]
****[[74th Bombardment Squadron|74th Attack Squadron]]

==Specifications (A-17)==
{{aircraft specifications
|plane or copter?=plane
|jet or prop?=prop
|ref=McDonnell Douglas Aircraft since 1920<ref name="Frnc doug p222">Francillon 1979, p. 222.</ref>
|crew=two (pilot and gunner)
|capacity=
|payload main=
|payload alt=
|length main= 31 ft 8⅝ in
|length alt= 9.67 m 
|span main= 47 ft 8½ in
|span alt= 14.54 m 
|height main= 11 ft 10½ in
|height alt= 3.62 m
|area main= 363 sq ft
|area alt=33.7m² 
|airfoil=
|empty weight main= 4,874 lb
|empty weight alt= 2,211 kg
|loaded weight main= 7,337 lb
|loaded weight alt= 3,328 kg
|useful load main= 
|useful load alt= 
|max takeoff weight main= 
|max takeoff weight alt=  
|more general=
|engine (prop)= [[Pratt & Whitney R-1535]]-11 Twin Wasp Jr
|type of prop=two-row  air-cooled [[radial engine]]
|number of props=1
|power main= 750 hp
|power alt= 560 kW
|power original=
|max speed main= 206 mph
|max speed alt=179 knots, 332 km/h 
|cruise speed main= 170 mph
|cruise speed alt=149 knots, 274 km/h 
|stall speed main= 
|stall speed alt= 
|never exceed speed main= 
|never exceed speed alt= 
|range main= 650 mi
|range alt=565 [[nautical mile|nmi]], 1,046 km
|ceiling main= 19,400 ft
|ceiling alt= 5,915 m
|climb rate main= 1,350 ft/min
|climb rate alt= 6.9 m/s
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=
|power/mass alt=
|more performance=
|armament=
* 4 × 0.3 in (7.62 mm) fixed forward [[M1919 Browning machine gun]]s
* 1 × 0.3 in (7.62 mm) trainable rear machine gun
* Internal bay for bombs
* External wing bomb racks (total bomb load 1,200 lb/544 kg)
|avionics=
}}

==See also==
{{portal|Aviation}}
* [[Lick Observatory]]
{{aircontent
|related=
* [[Northrop Gamma]]
* [[Northrop YA-13]]
* [[Northrop BT]]
* [[Douglas A-33]]
|similar aircraft=
* [[Blackburn Skua]]
* [[Fairey Battle]]
|lists =
* [[List of aircraft of World War II]]
* [[List of military aircraft of the United States]]
* [[List of aircraft of the Royal Air Force]]
|see also=
}}

==References==

===Notes===
{{Reflist|colwidth=30em}}

===Bibliography===
{{Refbegin}}
* Bontti, Sergio and Jorge Núñez Padín, eds. "Northrop 8A-2 (in Spanish)".  ''Serie Fuerza Aérea Argentina #8,''  October 2003.
* Donald, David, ed. ''American Warplanes of World War II''. London: Aerospace, 1995. ISBN 1-874023-72-7.
* Francillon, René J. ''McDonnell Douglas Aircraft since 1920''. London: Putnam, 1979. ISBN 0-370-00050-1.
* Pelletier, Alain J. "Northrop's Connection: The unsung A-17 attack aircraft and its legacy - Part 1". ''[[Air Enthusiast]],'' No. 75, May–June 1998, pp.&nbsp;62–67. Stamford, Lincolnshire: Key Publishing. {{ISSN|0143-5450}}.
* Pelletier, Alain J. "Northrop's Connection: The unsung A-17 attack aircraft and its legacy - Part 2". ''[[Air Enthusiast]],'' No. 77, September/October 1998, pp.&nbsp;2–15. Stamford, Lincolnshire: Key Publishing. {{ISSN|0143-5450}}.
* Widfeldt, Bo and Åke Hall. ''B 5 Störtbombepoken'' (in Swedish). Nässjö, Sweden: Air Historic Research AB U.B., 2000. ISBN 91-971605-7-1.
{{Refend}}

==External links==
{{Commons category}}
* [http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?fsID=343 National Museum of the United States Air Force]
* [https://books.google.com/books?id=rNoDAAAAMBAJ&pg=PA326&dq=Popular+Science+1935+plane+%22Popular+Mechanics%22&hl=en&ei=r81ETsS3KI3isQLY3dX5BQ&sa=X&oi=book_result&ct=result&resnum=7&ved=0CEUQ6AEwBjgU#v=onepage&q&f=true "Bullet Nose Fighter Flies 200 Miles An Hour" ''Popular Mechanics'', September 1937]

{{Northrop aircraft}}
{{Douglas aircraft}}
{{USAF attack aircraft}}

[[Category:Northrop aircraft|A-17]]
[[Category:United States attack aircraft 1930–1939|A-17, Northrop]]
[[Category:Single-engined tractor aircraft]]
[[Category:Low-wing aircraft]]