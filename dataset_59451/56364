{{Infobox journal
 | title        = Computational Geometry
 | cover        = 
 | abbreviation = Comput. Geom.
 | discipline   = [[Computational geometry]]
 | editor       = {{plainlist|1=
*[[Jörg-Rüdiger Sack]]
*[[Kurt Mehlhorn]]
}}
 | publisher    = [[Elsevier]]
 | frequency    = Quarterly
 | history      = 
 | impact       = 0.597
 | impact-year  = 2014
 | url          = http://www.journals.elsevier.com/computational-geometry
 | ISSN         = 0925-7721
 | eISSN        = 1879-081X
 | CODEN        = 
 | LCCN         = 
 | OCLC         = 
 | link1        = http://www.sciencedirect.com/science/journal/09257721
 | link1-name   = Online access
}}

'''''Computational Geometry''''', also known as '''''Computational Geometry: Theory and Applications''''', is a [[peer review|peer-reviewed]] [[mathematics journal]]  for research in theoretical and applied [[computational geometry]], its applications, techniques, and design and analysis of geometric algorithms. All aspects of computational geometry are covered, including the numerical, graph theoretical and combinatorial aspects, as well as  fundamental problems in various areas of application of computational geometry: in [[computer graphics]], [[pattern recognition]], [[image processing]], [[robotics]], [[electronic design automation]], [[Computer-aided design|CAD]]/[[Computer-aided manufacturing|CAM]], and [[geographical information systems]].

The journal was founded in 1991 by [[Jörg-Rüdiger Sack]] and [[Jorge Urrutia Galicia|Jorge Urrutia]].<ref>{{citation|title=Computational Geometry: Theory and Applications |url=http://www.computingreviews.com/review/review_review.cfm?review_id=115947 |journal=[[ACM Computing Reviews]] |date=September 1993 |at=Review #CR115947 |first=Joseph |last=O’Rourke |authorlink=Joseph O'Rourke (professor) }}{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}.</ref>
It is indexed by ''[[Mathematical Reviews]]'', [[Zentralblatt MATH]], [[Science Citation Index]], and [[Current Contents]]/Engineering, Computing and Technology.

==References==
{{reflist}}

==External links==
*{{Official website|http://www.journals.elsevier.com/computational-geometry}}

{{Publications in computational geometry}}
[[Category:Mathematics journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Computational geometry]]


{{math-journal-stub}}