{{Use dmy dates|date=March 2015}}
{{Use British English|date=March 2015}}
[[File:TRES18481.jpg|230px|thumb|plate from ''Transactions of the Entomological Society'' for 1848]]
The '''Royal Entomological Society''' is devoted to the study of [[insect]]s. Its aims are to disseminate information about insects and improving communication between [[entomology|entomologists]].

The society was founded in 1833 as the '''Entomological Society of London'''. It had many antecedents beginning as the [[Society of Entomologists of London]].

==History==
The foundation of the society began with a meeting of "gentlemen and friends of entomological science", held on 3 May 1833 in the [[British Museum]] convened by [[Nicholas Aylward Vigors]] with the presidency of [[John George Children]]. Those present were the Reverend [[Frederick William Hope]], [[Cardale Babington]], [[William Yarrell]], [[John Edward Gray]], [[James Francis Stephens]], [[Thomas Horsfield]], [[George Thomas Rudd]] and [[George Robert Gray]]. Letters of  [[Adrian Hardy Haworth]], [[George Bennett (naturalist)|George Bennett]] and [[John Curtis (entomologist)|John Curtis]] were read where they expressed their regrets to be unable to attend the meeting.

They decided that a society should be created for the promotion of the science of entomology in its various branches and it should be called the Entomological Society of London. J.G. Children, F.W. Hope, J.F. Stephens, W Yarrell and G Rudd were elected to form a committee, with G.R. Gray as secretary. J.G. Children became the first president and [[William Kirby (entomologist)|William Kirby]] (1759–1850) was made honorary president for life. The real date of the foundation of the society was more probably on 22 May 1833, when the members met in Thatched House Tavern, on St James's Street. During this meeting, [[George Robert Waterhouse]] (1810–1888) was elected librarian and curator of  the insects and records. As of this meeting, foreign honorary members were elected: [[Johann Christoph Friedrich Klug|Johann Cristoph Friedrich Klug]] (1775–1856), [[Wilhem de Haan]] (1801–1855), [[Jean Victoire Audouin|Victor Audouin]] (1797–1841), [[Johann Ludwig Christian Gravenhorst]] (1777–1857), [[Christian Rudolph Wilhelm Wiedemann]] (1770–1840), [[Carl Eduard Hammerschmidt]] (1800–1874) and [[Alexandre Louis Lefèbvre de Cérisy]] (1798–1867). [[William Blundell Spence|William Blandell Spence]] (1813–1900) received the  task of maintaining of the relations with continental entomologists.{{Citation needed|date=July 2007}}

The society started to assemble a  library, an early addition being the personal library of Adrian Hardy Haworth (1767–1833), purchased by [[John Obadiah Westwood]] (1805–1893) on behalf of the society. The insect collection also increased.

In September 1834, the society numbered 117 honorary members and 10 full members. Women were allowed membership and profited from the same rights as the men. A publication commenced in November 1834 under the title ''Transactions of Entomological Society of London''.

Secretary G.R. Gray resigned in the same year then and was replaced by J.O. Westwood. Under the impulse of this last entomologist, who had many functions, the society made great strides. It was in particular attended regularly by [[Charles Darwin]] (1809–1882) on his return from the voyage on [[HMS Beagle|H.M.S. Beagle]]: he became a member of the council and vice-president in 1838. J.O. Westwood left his functions in 1848 and was replaced by [[Edward Doubleday]] (1810–1849) and [[William Frederick Evans]]. They in their turn were soon replaced. In 1849, a secretary charged to collect the minutes of the meetings was named in the person of [[John William Douglas]] (1814–1905), a position he kept until 1856. He was assisted in 1851-1852 by [[Henry Tibbats Stainton]] (1822–1892), in 1853-1854 by [[William Wing]] (1827–1855), in 1855-1856 by [[Edwin Shepherd]] who then replaced J.W. Douglas in his position. [[Edward Wesley Janson]] (1822–91), a natural history agent, publisher and entomologist was Curator of the Entomological Society collections from 1850–63 and librarian from 1863–74.

[[Edward Mason Janson]] (1847–1880) took over the post of curator from [[Frederick Smith (entomologist)|Frederick Smith]] (1805–1879) who then left to work in the British Museum. H.T. Stainton, who was involved more and more in the life of the society, seemed to have some problems working with E.M. Janson. He was replaced by W Wing in 1852. In this year, the society moved from its building at 17, Old Bond Street to 12, Bedford Row. The following year, three of the four most responsible for the society were replaced: Edward Newman (1801–1876) took the place of J.O. Westwood as president, Samuel Stevens (1817–1899) took the place of W Yarrell as  treasurer and W Wing the place of H.T. Stainton as secretary.

==Structure and activities==
The society's patron is [[Elizabeth II|Her Majesty The Queen]] and its vice-patron is [[John Palmer, 4th Earl of Selborne|The Earl of Selborne]]. The society is governed by its council, which is chaired by the society's president, according to a set of by-laws. The members of council, the president and the other officers are elected from the society's fellowship and membership. The aim of the Royal Entomological Society is the improvement and diffusion of entomological science. This is achieved through publications, scientific meetings, supporting and funding entomological expeditions, and public events.<ref>{{cite web|url=http://www.royensoc.co.uk/about/our_aims.htm|title=About the Society - our aims|publisher=}}</ref> The society maintains an entomological library at its headquarters in [[St Albans]], UK.<ref>{{cite web|url=http://www.royensoc.co.uk/library/about.htm|title=About the Library|publisher=}}</ref> and convenes over 15 special interest groups, covering a range of scientific fields within entomology.<ref>{{cite web|url=http://www.royensoc.co.uk/library/about.htm|title=About the Library|publisher=}}</ref>

With the support of over 60 partner organisations,<ref>{{cite web|url=http://www.nationalinsectweek.co.uk/about/partners.htm|title=Our Partners - National Insect Week|publisher=}}</ref> the society organises [[National Insect Week]], a biennial initiative to engage the public with the importance of insects and entomology, through hundreds of events and activities across the UK.<ref>{{cite web|url=http://www.nationalinsectweek.co.uk/|title=National Insect Week|publisher=}}</ref>

==Publications {{anchor|Antenna}}==
[[File:Staphylinidae cover.jpg|right|thumb|Volume 12 Part 5: The Staphylinidae (rove beetles) of Britain and Ireland]]

The society publishes seven scientific journals:
* ''[[Agricultural and Forest Entomology]]''
* ''[[Ecological Entomology]]''
* ''[[Insect Conservation and Diversity]]''
* ''[[Insect Molecular Biology]]''
* ''[[Medical and Veterinary Entomology]]''
* ''[[Physiological Entomology]]''
* ''[[Systematic Entomology]]''

Members and fellows receive the quarterly entomological news journal ''Antenna''.

The society also publishes a series of [[Royal Entomological Society Handbooks|handbooks]] on the identification of insects. The aim of these handbooks is to provide illustrated identification keys to the insects of Britain, together with concise morphological, biological and distributional information. The series also includes several Check Lists of British Insects. All books contain line drawings, with the most recent volumes including colour photographs. In recent years, new volumes in the series have been published by [[Field Studies Council]], and benefit from association with the [[AIDGAP]] identification guides and [[Synopses of the British Fauna]].

==Fellowship==
The Royal Entomological Society has an international membership and invites applications for Fellowship from those who have made a substantial contribution to entomology, through publications or other evidence of achievement. Applications are referred to a Committee of Council, who then forward a recommendation to Council. Fellows are entitled to make use of the title "Fellow of the Royal Entomological Society" and the suffix "FRES" may be regarded as an academic qualification.<ref>RES, 2009. [http://www.royensoc.co.uk Website of the Royal Entomological Society of London/]</ref>

==Awards==

As is customary, the RES gives various awards.<ref>{{cite web|url=http://www.royensoc.co.uk/about/awards.htm|title=Awards|publisher=}}</ref>  These include:

* [[RES Goodman Award]]
* [[Marsh Award for Insect Conservation]]
* [[Alfred Russel Wallace Award]]
* [[J.O. Westwood Medal]]
* [[Wigglesworth Memorial Lecture]]

==Badge==
[[File:Royal Entomological Society badge.jpg|right|thumb|Royal Entomological Society badge]]

On the foundation of the Entomological Society in 1833 William Kirby was made Honorary Life President and ''[[Stylops melittae]]'' (then known as ''Stylops kirbyi'') was adopted as the society’s symbol. The seal was first used for a letter by the society to William Kirby, which was signed by the President and 30 members in 1836 to thank him for presenting the society with a cabinet containing his entire insect collection. William Kirby was responsible for classifying the [[Strepsiptera]] as a separate order. The society's badge has remained almost unchanged since its first use.<ref>President Address - Hugh Loxdale, 3 May 2006</ref>

==Officers==

===Honorary life Presidents===
* 1833-1850: [[William Kirby (entomologist)|William Kirby]]
* 1883-1893: [[John Obadiah Westwood]]
* 1933-1943: [[Edward Bagnall Poulton]]

===Presidents===
The following persons have been presidents of the society:<ref>List of Fellows and members, Royal Entomological Society, 2002</ref>
{{Div col|colwidth=30em}}
* 1833-1834: [[John George Children]]
* 1835-1836: [[Frederick William Hope]]
* 1837-1838: [[James Francis Stephens]]
* 1839-1840: [[Frederick William Hope]]
* 1841-1842: [[William Wilson Saunders]]
* 1843-1844: [[George Newport]]
* 1845-1846: [[Frederick William Hope]]
* 1847-1848: [[William Spence (entomologist)|William Spence]]
* 1849-1850: [[George Robert Waterhouse]]
* 1852-1853: [[John Obadiah Westwood]]
* 1853-1854: [[Edward Newman (entomologist)|Edward Newman]]
* 1855-1856: [[John Curtis (entomologist)|John Curtis]]
* 1856-1857: [[William Wilson Saunders]]
* 1858-1859: [[John Edward Gray]]
* 1860-1861: [[John William Douglas]]
* 1862-1863: [[Frederick Smith (entomologist)|Frederick Smith]]
* 1864-1865: [[Francis Polkinghorne Pascoe]]
* 1866-1867: [[John Lubbock, 1st Baron Avebury]]
* 1868-1869: [[Henry Walter Bates]]
* 1870-1871: [[Alfred Russel Wallace]]
* 1874-1875: [[William Wilson Saunders]]
* 1878:      [[Henry Walter Bates]]
* 1879-1880: [[John Lubbock, 1st Baron Avebury]]
* 1881-1882: [[Henry Tibbats Stainton]]
* 1883-1884: [[Joseph William Dunning]]
* 1885-1886: [[Robert McLachlan (entomologist)|Robert McLachlan]]
* 1887-1888: [[David Sharp (entomologist)|David Sharp]]
* 1889-1890: [[Lord Thomas de Grey Walsingham]]
* 1891-1892: [[Frederick DuCane Godman]]
* 1893-1894: [[Henry John Elwes]]
* 1895-1896: [[Raphael Meldola]]
* 1897-1898: [[Roland Trimen]]
* 1899-1900: [[George Henry Verrall]]
* 1901-1902: [[William Weekes Fowler]]
* 1903-1904: [[Edward Bagnall Poulton]]
* 1905-1906: [[Frederick Merrifield]]
* 1907-1908: [[Charles Owen Waterhouse]]
* 1909-1910: [[Frederick Augustus Dixey]]
* 1911-1912: [[Francis David Morice]]
* 1913-1914: [[George Thomas Bethune-Baker]]
* 1915-1916: [[Nathaniel Charles Rothschild]]
* 1917-1918: [[Charles Joseph Gahan]]
* 1919-1920: [[James John Walker (entomologist)|James John Walker]]
* 1921-1922: [[Lionel Walter Rothschild]]
* 1923-1924: [[Edward Ernest Green]]
* 1927-1928: [[James Edward Collin]]
* 1929-1930: [[Karl Jordan]]
* 1931-1932: [[Harry Eltringham]]
* 1933-1934: [[Edward Bagnall Poulton]]
* 1934-1935: [[Sheffield Airey Neave]]
* 1936-1937: [[Augustus Daniel Imms]]
* 1938-1939: [[John Claud Fortescue Fryer]]
* 1940-1941: [[Kenneth Gloyne Blair]]
* 1942-1943: [[P. A. Buxton|Patrick Alfred Buxton]]
* 1943-1944: [[Edward Alfred Cockayne]]
* 1945-1946: [[Geoffrey Douglas Hale Carpenter]]
* 1947-1948: [[Carrington Bonsor Williams]]
* 1949-1950: [[Sir Vincent Brian Wigglesworth]]
* 1951-1952: [[Norman Denbigh Riley]]
* 1953-1954: [[P. A. Buxton|Patrick Alfred Buxton]]
* 1955-1956: [[Wilfrid John Hall]]
* 1957-1958: [[Owain Westmacott Richards]]
* 1959-1960: [[Boris Uvarov|Boris Petrovitch Uvarov]]
* 1961-1962: [[Hope Professor of Zoology|George Copley Varley]]
* 1963-1964: [[Sir Vincent Brian Wigglesworth]]
* 1965-1966: [[Eric Omar Pearson]]
* 1967-1968: [[John Stodart Kennedy]]
* 1969-1970: [[Howard Everest Hinton]]
* 1971-1972: [[Colin Gasking Butler]]
* 1973-1974: [[Anthony David Lees]]
* 1975-1976: [[Donald Livingston Gunn]]
* 1977-1978: [[John David Gillett]]
* 1979-1980: [[Reginald Charles Rainey]]
* 1981-1982: [[Helmut Fritz van Emden]]
* 1983-1984: [[Richard Southwood|Sir Thomas Richard Edmund Southwood]]
* 1985-1986: [[Trevor Lewis (entomologist)|Trevor Lewis]]
* 1987-1988: [[Victor Frank Eastop]]
* 1989-1990: [[Jack P. Dempster]]
* 1991-1992: [[Cyril Clarke|Sir Cyril Astley Clarke]]
* 1993-1994: [[Miriam Rothschild|Miriam Louisa Rothschild]]
* 1995-1996: [[Richard Lane (entomologist)|Richard Lane]]
* 1997-1998: [[Walter M. Blaney]]
* 1999-2000: [[Roger L. Blackman]]
* 2001-2002: [[Michael Claridge|Michael Frederick Claridge]]
* 2002-2004: [[Christopher Peter Haines]]
* 2004-2006: [[Hugh Loxdale|Hugh David Loxdale]]
* 2006-2008: [[Jim Hardie]]
* 2008-2010: [[Linda M. Field]]
* 2010-2012: [[Stuart Edward Reynolds]]
* 2012-2014: [[Jeremy A. Thomas]]
* 2014-2016: [[John A. Pickett]]
* 2016-2018: [[Michael Hassell]]
{{div col end}}

==See also==
*[[:Category:Fellows of the Royal Entomological Society|Fellows of the Royal Entomological Society (of London)]]
*[[Royal Entomological Society Handbooks]]

==References==
<references/>

==External links==
{{commonscat}}
* {{Official website|http://www.royensoc.co.uk/}}
* [http://www.nationainsectweek.co.uk National Insect Week]
* [http://www.biodiversitylibrary.org/bibliography/11516 BHL] Digitised ''Transactions''
* [https://books.google.com/books?id=EMwEAAAAQAAJ&printsec=frontcover&dq=transactions+entomological#PPP4,M1 Google Books] Volume 1 of the ''Transactions''
* [http://www.royensoc.co.uk/publications/index.htm Royal Entomological Society publications page] (includes a selection of out of print handbooks available as downloads)

{{Science and technology in the United Kingdom}}

[[Category:Royal Entomological Society| ]]
[[Category:City of St Albans]]
[[Category:Entomological organisations]]
[[Category:Learned societies of the United Kingdom]]
[[Category:Organisations based in Hertfordshire]]
[[Category:Organizations established in 1833]]
[[Category:Organisations based in London with royal patronage]]
[[Category:Science and technology in Hertfordshire]]
[[Category:1833 establishments in England]]