{{Infobox scientist
| name = In-Young Ahn
| image = Dr. In-Young Ahn at the Korean Antarctic Station, King Sejong in October 2015.jpg
| caption = Ahn at [[King Sejong Station]]
| birth_name = 
| nationality = [[South Korea]]n
| fields = Antarctic [[Benthic zone|Marine Ecology]]
| workplaces = South Korean Polar Research Institute (KOPRI)
| alma_mater = [[State University of New York at Stony Brook]]
| known_for = First South Korean woman to visit Antarctica <br> First female Antarctic station leader from Asia
| awards = 
| website = [https://www.researchgate.net/profile/In-Young_Ahn In-Young Ahn on ResearchGate]
| module = {{Infobox Korean name|child=yes
| hangul = 안인영
| hanja = 安仁英<ref>{{cite news|url=http://news.naver.com/main/read.nhn?mode=LSD&mid=shm&sid1=104&oid=001&aid=0007173748|script-title=ja:南極科学基地の越冬隊長に初の女性任命|agency=Yonhap News Agency|date=2014-10-08|accessdate=2016-06-07}}</ref>
| mr = An Inyŏng
| rr = An Inyeong}}
}}
{{Eastern name order|Ahn In-Young}}

'''In-Young Ahn''' is a South Korean scientist. She is known for being the first South Korean woman to visit [[Antarctica]] and the first Asian woman to become an [[Research stations in Antarctica|Antarctic station]] leader ([[King Sejong Station]]).<ref>{{Cite web|url=http://www.abc.net.au/news/2015-05-05/women-scientists-in-antarctica-report-sexual-harassment/6443570|title=Stories of sexual harassment against women in Antarctica highlight issue in science industry|date=2015-05-04|website=ABC News|language=en-AU|access-date=2016-06-03}}</ref><ref name=":0">{{Cite web|url=https://womeninpolarsciencedotorg.files.wordpress.com/2015/12/womeninpolarscience-issueii.pdf|title=Newsletter Issue II|last=|first=|date=December 2015|website=womeninpolarscience.org|publisher=Women in Polar Science|access-date=2016}}</ref><ref>{{Cite web|url=http://www.abc.net.au/foreign/content/2015/s4230123.htm|title=Southern Exposure|date=2015-05-05|website=ABC.com.au|access-date=2016-06-03}}</ref><ref>{{Cite web|url=http://www.abc.net.au/res/sites/news-projects/interactive-king-george-island/1.1.0/|title=King George Island's research stations|website=www.abc.net.au|publisher=ABC News|access-date=2016-06-30}}</ref> She is a [[Benthic zone|benthic]] ecologist and is currently working as a principal research scientist for Korea Polar Research Institute.<ref name=":2">{{Cite web|url=http://www.abc.net.au/foreign/content/2015/s4230123.htm|title=Antarctica – Southern Exposure|website=abc.net.au|publisher=ABC|access-date=2016-06-03}}</ref><ref name=":1">{{Cite web|url=http://eng.kopri.re.kr/www/about/kopri_brochure/annual_report_2015_eng.pdf|title=Annual Report|last=|first=|date=2015|website=eng.kopri.re.kr|publisher=Korea Polar Research Institute|access-date=2016}}</ref>

==Early life and education==
Ahn graduated from [[Seoul National University]] in 1982 (majoring in Biological [[Oceanography]]) and then received her PhD in Coastal Oceanography from the [[State University of New York]] at [[Stony Brook University|Stony Brook]] in 1990. Ahn started her research at the Korean Polar Research Institute (KOPRI) on July 1, 1991.<ref name=":0" />

==Career and impact==
Ahn was in charge of [[environmental monitoring]] program at the King Sejong Station from 1996 to 2011, and conducted field surveys to obtain scientific data necessary for designation of the [[Antarctic Specially Protected Area]] (ASPA #171) near the Korean station. Ahn has also served as a representative and a National Contact Point of the Committee for Environmental Protection (CEP) at the [[Antarctic Treaty System|Antarctic Treaty Consultative Meetings]] from 1997 through 2014, until she was designated as the overwintering officer-in-charge.<ref>{{Cite web|url=http://eng.kopri.re.kr/home/contents/images/contents/e_5310000/Final_CEE_Jang_Bogo_ROK.pdf|title=Final Comprehensive Environmental Evaluation|last=|first=|date=April 2012|website=eng.kopri.re.kr|publisher=Korea Ppolar Research Institute|access-date=2016}}</ref> Ahn served as the vice president of the Korea Polar Research Institute (KOPRI) from May 2010 to June 2012. She also served as Vice President of the Korean Society of Oceanography in 2010–2011,<ref>{{Cite web|url=http://www.ksocean.or.kr/sub01_04.asp|title=연혁/역대회장|last=|first=|date=|website=ksocean.or.kr|publisher=Korean Society of Oceanography|language=ko|trans-title=History|access-date=}}</ref> and Korea Federation of Women's Science & Technology Associations in 2014 and 2015.<ref>{{Cite web|url=http://www.kofwst.org/|title=한국여성과학기술단체총연합회|website=www.kofwst.org|publisher=|language=ko|trans-title=Korea Federation of Women's Science & Technology Associations|access-date=2016-06-03}}</ref> She was expedition leader of the 28th overwintering team (2015) of the South Korean King Sejong Antarctic station, where she served as the station chief for about an year.<ref name=":2" /><ref name=":1" />

Ahn's research interests include Antarctic marine [[benthic zone|benthic ecology]] with special interests on benthic invertebrates and monitoring on Antarctic coastal marine ecosystems.<ref name=":0" /> She has studied the Antarctic clam ''[[Laternula elliptica]]'', a dominant marine [[Bivalvia|bivalve]] around Antarctic Continent. Ahn's current research includes studies on the impacts of [[Glacial motion|glacier retreat]] on nearshore marine benthic communities around the King Sejong Station.

She is currently an Adjunct Professor at the [[Korea University of Science and Technology|University of Science & Technology]] (UST), and a Principal Research Scientist at the Korean Polar Institute (KOPRI), which forms part of Korea Institute of Ocean Science & Technology (KIOST).

==Awards and honors==
The South Korean government awarded Ahn a Medal of Science & Technology Merit for outstanding accomplishment in Antarctic Research in April 2001.<ref>{{Cite web|url=http://news.naver.com/main/read.nhn?mode=LSD&mid=sec&sid1=101&oid=001&aid=0000067080|title=과학기술진흥유공자 포상자 명단|date=2001|website=news.naver.com|publisher=yunhap news|trans-title=Science and Technology Merit list|access-date=2016-06-20}}</ref> She was further awarded a Commendation from the [[Ministry of Environment (South Korea)|South Korean Ministry of Environment]] in June 2008 for her contribution in the designation of Antarctic Specially Protected Area (ASPA #171) near the King Sejong station.<ref>{{Cite web|url=http://eng.kopri.re.kr/www/about/kopri_brochure/annual_report_2008.pdf|title=Annual Report|last=|first=|date=2008|website=kopri.re.kr|publisher=Korean Polar Research Institute|access-date=}}</ref> In 2016, she additionally received South Korea's [[Orders, decorations, and medals of South Korea|Prime Minister's award]] in recognition of her achievement as the station leader at King Sejong station.<ref>{{Cite web|url=http://www.ksg.co.kr/news/main_newsView.jsp?bbsID=news&bbsCategory=KSG&categoryCode=all&backUrl=main_news&pNum=108601|title=코리아쉬핑가제트|date=2016|work=Korea Shipping Gazette|trans-title=Golden Tower and Silver Tower Medals|access-date=2016-06-20}}</ref>

==Selected works==
*Ahn, In-Young, et al. "First record of massive blooming of benthic diatoms and their association with megabenthic filter feeders on the shallow seafloor of an Antarctic fjord: Does glacier melting fuel the bloom?" ''Ocean Science Journal'' 51. 2 (2016): 273–279.
*HW Moon, WMRW Hussin, HC Kim,  In-Young Ahn*. "The Impacts of climate change on Antarctic nearshore mega-epifaunal benthic assemblages in a glacial fjord on King George Island: Responses and implications." ''Ecological Indicators'' 57 (2015): 280–292.
*Ahn, In-Young, et al. "Influence of glacial runoff on baseline metal accumulation in the Antarctic limpet Nacella concinna from King George Island." ''Marine Pollution Bulletin'' 49 (2004): 119-141.
*Ahn, In-Young, et al. "Growth and seasonal energetics of the Antarctic bivalve Laternula elliptica from King George Island, Antarctica." ''Marine Ecology Progress Series'' 257(2003): 99–110.
*Ahn, In-Young, et al. "Baseline heavy metal concentrations in the Antarctic clam, Laternula elliptica in Maxwell Bay, King George Island, Antarctica." ''Marine Pollution Bulletin'' 32.8 (1996): 592–598.
*Ahn, In-Young. "Enhanced particle flux through the biodeposition by the Antarctic suspension-feeding bivalve Laternula elliptica in Marian Cove, King George Island." ''Journal of Experimental Marine Biology and Ecology'' 171.1 (1993): 75–90.

==References==
{{Reflist|35em}}

==External links==
* [https://scholar.google.co.uk/citations?user=HawzMpkAAAAJ&hl=en&oi=sra In-Young Ahn] on [[Google Scholar]]
* [https://www.researchgate.net/profile/In-Young_Ahn In-Young Ahn] on [[ResearchGate]]<!-- Categories for this template, but not the final article -->

{{Authority control}}

{{DEFAULTSORT:Ahn, In-Young}}

[[Category:South Korean women scientists]]
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:Antarctic scientists]]
[[Category:South Korean scientists]]
[[Category:20th-century births]]
[[Category:Women earth scientists]]
[[Category:Women Antarctic scientists]]