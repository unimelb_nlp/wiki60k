{{Infobox NFL player
|name=Jared Green
|image=
|caption=
|number=9
|position=[[Wide receiver]]
|birth_date={{Birth date and age|1989|04|01}}
|birth_place=[[Ashburn, Virginia]]
|death_date=
|death_place=
|height_ft = 6 
|height_in = 1
|weight_lbs = 186
|high_school = [[Oakton High School|Vienna (VA) Oakton]]
|college=[[Southern Jaguars football|Southern]]
|undraftedyear=2012
|pastteams=
* [[Carolina Panthers]] ({{NFL Year|2012}})*
* [[Dallas Cowboys]] ({{NFL Year|2013}})*
* [[Oakland Raiders]] ({{NFL Year|2013}}–{{NFL Year|2014}})*
|pastteamsnote = yes
|highlights=
|statseason=2012
|statlabel1=Receptions
|statvalue1=--
|statlabel2=Receiving yards
|statvalue2=--
|statlabel3=[[Touchdown|Receiving TDs]]
|statvalue3=--
|nflnew=jaredgreen/2534860
}}

'''Jared Green''' (born April 1, 1989) is a former [[American football]] [[wide receiver]] in the [[National Football League]] (NFL). He is an [[Associate Pastor]] at [[Grace Covenant]] Church in [[Chantilly, Virginia]]. Green played [[college football]] for [[Southern University]] and for the [[University of Virginia]]<ref>{{cite web | title = WR Green has ball, will travel | work = theadvocate.com | url = http://theadvocate.com/sports/523249-32/story.html |accessdate=}}</ref> He signed with the [[Carolina Panthers]] as an [[undrafted free agent]] in 2012. He had brief stints with the Panthers, [[Dallas Cowboys]] and [[Oakland Raiders]] before retiring from professional football in 2014 to pursue the ministry full-time.<ref name="former">{{cite web | url=http://nfldraftdiamonds.com/former-raiders-wide-gives-football-god/ | title=Former Raiders Wide Out Gives Up Football For God | publisher=NFLDraftDiamonds.com | accessdate=January 8, 2015}}</ref>

==Early years==
Green attended [[Oakton High School]] in [[Vienna, Virginia]].<ref>{{cite web | title = Virginia Cavaliers Profile
| work = virginiasports.com | url = http://www.virginiasports.com/sports/m-footbl/mtt/green_jared00.html | accessdate =2012 }}</ref>

==College career==
Green played [[college football]] at the [[Virginia Cavaliers football|University of Virginia]] from 2008 to 2010. He transferred from Virginia to [[Southern Jaguars|Southern]] for his senior year and the 2011 football season. He finished his college career with 52 receptions, 670 receiving yards and four receiving touchdowns.<ref>{{cite web | title = Jared Green Yahoo Profile | work = rivals.yahoo.com | url = http://rivals.yahoo.com/ncaa/football/players/155784 |accessdate=}}</ref><ref>{{cite web | title = Jared Green Washingtonpost Profile | work = washingtonpost.com | url = http://stats.washingtonpost.com/cfb/players.asp?id=155784 |accessdate=}}</ref>

In his freshman year at Virginia, Green played in 12 games had 12 receptions, 144 receiving yards and one receiving touchdown.<ref>{{cite web | title = Jared Green 2008 Virginia Cavaliers stats | work = cfbstats.com | url = http://www.cfbstats.com/2010/player/746/1011883/receiving/situational.html | accessdate =  21 August 2012 }}</ref> On November 1, 2008, he had three receptions for 25 yards and a touchdown against [[Miami Hurricanes|Miami]] in which Virginia lost the game in Overtime 24-17.<ref>{{cite web | title = Miami (FL) 24, Virginia 17 Box score | work = sports-reference.com | url = http://www.sports-reference.com/cfb/boxscores/2008-11-01-virginia.html | accessdate =2012 }}</ref>
 
In his sophomore year, he played in 11 games in which he recorded 15 receptions, 104 receiving yards and no touchdowns.<ref>{{cite web | title = Jared Green 2009 Virginia Cavaliers stats | work = cfbstats.com | url = http://www.cfbstats.com/2010/player/746/1011883/receiving/situational.html | accessdate =  21 August 2012 }}</ref> On September 5, 2009, he had three receptions for 28 yards in a loss against [[William & Mary]].<ref>{{cite web | title = William & Mary 26, Virginia 14 Box score | work = sports-reference.com | url = http://www.sports-reference.com/cfb/boxscores/2009-09-05-virginia.html | accessdate =2012 }}</ref> On September 19, 2009, he had 4 receptions for 18 yards in a loss against [[Southern Mississippi]].<ref>{{cite web | title = Virginia 34, Southern Mississippi 37 Box score | work = sports-reference.com | url = http://www.sports-reference.com/cfb/boxscores/2009-09-19-southern-mississippi.html | accessdate =2012 }}</ref>
 
In his junior year, he played in 11 games and recorded 8 receptions, 95 receiving yards and one receiving touchdown.<ref>{{cite web | title = Jared Green 2010 Virginia Cavaliers stats | work = cfbstats.com | url = http://www.cfbstats.com/2010/player/746/1011883/receiving/situational.html | accessdate =  21 August 2012 }}</ref> On September 25, 2010, he recorded four receptions for 63 yards and a touchdown against [[Virginia Military Institute]] in which Virginia wins the game 48-7.<ref>{{cite web | title = Virginia Military Institute 7, Virginia 48 Box score | work = sports-reference.com | url = http://www.sports-reference.com/cfb/boxscores/2010-09-25-virginia.html | accessdate =2012 }}</ref>

In his senior year at Southern, he played in 11 games and started in three of them. He finished the season with 17 receptions, 307 receiving yards and two receiving touchdowns. On October 15, 2011, he had a career high 7 receptions, career high 141 receiving yards and one touchdown against [[Arkansas-Pine Bluff]] but Southern lost 22-21.<ref>{{cite web | title = Southern University 21 Arkansas-Pine Bluff 22 Box Score| work = washingtonpost.com | url = http://stats.washingtonpost.com/cfb/boxscore.asp?gamecode=201110150589&home=589&vis=193&final=true | accessdate =  23 August 2012 }}</ref>

==Professional career==

===Carolina Panthers===
Green went undrafted in the [[2012 NFL Draft]]. On May 11, 2012, he signed with [[Carolina Panthers]] as an undrafted free agent.<ref>{{cite web | title =Panthers sign WR Green as undrafted free agent | work = cbssports.com | url =http://www.cbssports.com/nfl/blog/nfl-rapidreports/18897127 | accessdate = April 29, 2012}}</ref><ref>{{cite web | title = Carolina Panthers sign 11 free agents, including 4 wide receivers | work = charlotteobserver.com | url = http://www.charlotteobserver.com/2012/04/29/3207760/four-wideouts-among-11-free-agents.html | accessdate =  11 May 2012 }}</ref><ref>{{cite web | title = Panthers announce a dozen undrafted free agents | work = http://profootballtalk.nbcsports.com | url = http://profootballtalk.nbcsports.com/2012/04/30/panthers-announce-a-dozen-undrafted-free-agents/ | accessdate =  11 May 2012 }}</ref><ref>{{cite web | title = Former Southern WR Jared Green to join Carolina Panthers | work = nbc33tv.com | url = http://www.nbc33tv.com/sports/southern/former-southern-wr-jared-green-to-join-carolina-panthers | accessdate =  11 May 2012 }}</ref> On August 31, 2012, he was released.<ref>{{cite web | title = Panthers cut WR Jared Green, 9 others | work = cbssports.com | url = http://www.cbssports.com/nfl/story/19997630/panthers-cut-wr-jared-green-9-others | accessdate =  August 31, 2012 }}</ref><ref>{{cite web | title = Panthers set 53-man roster | work = panthers.com | url = http://www.panthers.com/news/article-2/Panthers-set-53-man-roster/6a4e0cc4-a64c-456d-b867-6b5e3a26060e | accessdate =  31 August 2012 }}</ref><ref>{{cite web | title = Panthers finally release full roster cuts | work = profootballtalk.nbcsports.com | url = http://profootballtalk.nbcsports.com/2012/08/31/panthers-finally-release-full-roster-cuts/ | accessdate =  31 August 2012 }}</ref> On September 1, 2012, he was re-signed to the practice squad.<ref>{{cite web | title = Panthers sign 7 to practice squad | work = panthers.com | url = http://www.panthers.com/news/article-2/Panthers-sign-7-to-practice-squad-/ed116f48-dbc2-499d-94d1-631d7b0c4a34 | accessdate =  1 September 2012 }}</ref>

===Dallas Cowboys===
On January 7, 2013, Green signed with the [[Dallas Cowboys]] to a future/reserve contract.<ref>{{cite web | title= Cowboys sign nine players | work= espn.go.com | url=http://espn.go.com/blog/dallas/cowboys/post/_/id/4704404/cowboys-sign-nine-players |accessdate=January 7, 2013}}</ref><ref>{{cite web | title = Cowboys Signees Include Speedy Son of Legend Darrell Green | work = cbslocal.com | url = http://dfw.cbslocal.com/2013/01/07/cowboys-signees-include-speedy-son-of-legend-darrell-green/ | accessdate = January 7, 2013}}</ref>

===Oakland Raiders===
Green was signed to the [[practice squad]] of the [[Oakland Raiders]] on December 12, 2013.<ref>{{cite web|work=ESPN.com|url=http://espn.go.com/blog/oakland-raiders/tag/_/name/jared-green|title=Son of HOF Green signed to practice squad|date=December 12, 2013|first=Paul|last=Gutierrez|accessdate=2014-01-12}}</ref> On December 30, the Raiders signed him to a [[futures contract]].<ref>{{cite web|work=Raiders.com|url=http://www.raiders.com/news/article-1/Raiders-Announce-ReserveFuture-Signings/4f8cff2b-f9e9-4440-9cca-5e94a447b9cd|title=Raiders Announce Reserve/Future Signings|date=December 30, 2013|accessdate=2014-01-12}}</ref> On June 5, 2014, Green was waived by the team.<ref>{{cite web|work=Raiders.com|url=http://www.raiders.com/news/article-1/Raiders-Sign-Three-Free-Agents/54486625-9b7a-453b-a22c-ad1288664dae|title=Raiders Sign Three Free Agents|date=June 5, 2014|accessdate=2014-06-26}}</ref>

==Personal life==
He is the son of [[Pro Football Hall of Fame|Hall of Fame]] [[Washington Redskins]]  [[cornerback]] [[Darrell Green]]. He has an older sister, Jerrell, and a younger sister, Joi. He presented his father for induction to the Hall of Fame in Canton in 2008. Jared is married to Joanna Green and they have one daughter named Alana Green.<ref>{{cite web | title = ‘Green speed’ legacy tries out for Panthers
| work = charlotteobserver.com | url = http://www.charlotteobserver.com/2012/05/03/3215692/green-speed-legacy-tries-out-for.html | accessdate =  3 May 2012 }}</ref> Green is an active minister at Grace Covenant Church in Chantilly, Virginia.<ref name="former"/>

== References ==
{{Reflist|30em}}

==External links==
* [http://www.virginiasports.com/sports/m-footbl/mtt/green_jared00.html Virginia Cavaliers bio]
* [http://www.gojagsports.com/ViewArticle.dbml?DB_OEM_ID=28100&ATCLID=205354735 Southern Jaguars bio] 
* [http://www.panthers.com/team/roster/Jared-Green/fb95d570-c342-464a-9b05-79dc01a6bcde Carolina Panthers bio]
* [http://www.dallascowboys.com/team/roster/Jared-Green/02d50817-3f5b-47f1-bd55-8db6cac7a6b9 Dallas Cowboys bio]

{{DEFAULTSORT:Green, Jared}}
[[Category:1989 births]]
[[Category:Living people]]
[[Category:African-American Christian clergy]]
[[Category:Carolina Panthers players]]
[[Category:Dallas Cowboys players]]
[[Category:Oakland Raiders players]]
[[Category:Undrafted National Football League players]]
[[Category:Virginia Cavaliers football players]]