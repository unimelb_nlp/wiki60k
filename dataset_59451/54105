{{Use dmy dates|date=September 2014}}
{{Use Australian English|date=September 2014}}
{{Infobox Australian road
|type= street
|urban= yes
|road_name= Brougham Place
|city = [[North Adelaide]]
|state= sa
|image= Brougham Gardens, North Adelaide, Australia, Eastern End in 1910.jpg
|caption= Eastern end in 1910
|maintained= [[City of Adelaide]]
|ring= [[Brougham Gardens]]
}}
'''Brougham Place''' is a street lined with large mansions set in landscaped grounds in the [[Adelaide]] suburb of [[North Adelaide]], [[South Australia]]. It surrounds [[Brougham Gardens]], ([[Park 29]] of the [[Adelaide Park Lands]]), that joins the three grids that comprise North Adelaide. It was named after [[Henry Brougham, 1st Baron Brougham and Vaux]]. He was a staunch supporter of the [[1832 Reform Act]] and the passing of this Act led to the third and successful attempt to found a colony in SA in 1834.<ref>{{cite web
  | last = 
  | first = 
  | authorlink = 
  | coauthors = 
  | title = History of Adelaide Through Street Names
  | work = 
  | publisher = 
  | date = 
  | url = http://www.historysouthaustralia.net/STsquare.htm#brou
  | format = 
  | doi = 
  | accessdate = }}</ref>

Brougham Place starts and finishes at its intersection with LeFevre Terrace and Stanley Street and runs anti-clockwise around Brougham Gardens.<ref>{{cite book|title=2003 Adelaide Street Directory, 41st Edition |publisher=UBD (A Division of Universal Press Pty Ltd) |year=2003 |isbn=0-7319-1441-4}}</ref> Like other streets in the City of Adelaide with properties only along one side, numbering is sequential from 1 to 228. There is also a short stretch of Brougham Place south of Melbourne Street opposite Roberts Gardens.

Institutions and heritage listed buildings along Brougham Place include <ref>{{cite book
  | last = 
  | first = 
  | authorlink = 
  | coauthors = 
  | title = Adelaide (City) Development Plan
  | publisher = 
  | date = 
  | location = 
  | pages = 
  | url = http://www.planning.sa.gov.au/edp/pdf/AD.PDF
  | doi = 
  | id = 
  | isbn = }}
</ref>

{| class="wikitable"
|-
! Number
! Name
! Heritage listing
|-
| 5-7 
| Private dwelling
| State
|-
| 9 
| Taylor House (private dwelling)
| State
|-
| 24 
| St. Margarets
| National
|-
| 32 
| Former Baker family dwelling, now East Building of [[Lincoln College (University of Adelaide)|Lincoln College]]
| State
|-
| 35-37 
| Former dwelling now part of Lincoln College
| State
|-
| 39 
| Former Rymill family dwelling now part of Lincoln College
| State
|-
| 45 
| Former Milne family dwelling now part of Lincoln College
| State
|-
| 49 
| Brougham House (private dwelling)
| State
|-
| colspan="3" align="center" | [[King William Street, Adelaide|King William Road]] / O'Connell Street
|-
| 62
| Site of the former Hotel Australia
| 
|-
| 71-74 
| Belmont, former Masonic Hall
| National
|-
| 75-78
| Kingsmead House (private dwelling)
| National
|-
| 80 
| South Australian office of the [[Australian Medical Association]]
|
|-
| colspan="3" align="center" | [[King William Street, Adelaide|King William Road]]
|-
| 137-160
| [[Women's and Children's Hospital, Adelaide|Women's and Children's Hospital]]
|
|-
| colspan="3" align="center" | Sir Edwin Smith Avenue / [[Melbourne Street, North Adelaide|Melbourne Street]] 
|-
| 187-191
| [[St. Ann's College]]
| Local
|-
| 193
| [[Brougham Place Uniting Church]], formerly North Adelaide Congregational Church
| National
|-
| 210
| Former hall of North Adelaide Congregational Church (private dwelling)
| State
|-
| 222
| Private dwelling
| National
|-
| 225
| Private dwelling
| State
|}

==Brougham Court==
Brougham Court (formerly Bower Street) is located off Brougham Place between 95 and 96. It contains the national heritage Ebenezer Baptist Chapel, built in 1843 at 21-29, now a private dwelling <ref>{{cite web
  | last = 
  | first = 
  | authorlink = 
  | coauthors = 
  | title = North Adelaide Baptist Church
  | work = 
  | publisher = 
  | date = 
  | url = http://www.nabc.org.au/history.php
  | format = 
  | doi = 
  | accessdate = }}</ref>

==See also==
{{portal-inline|Australian Roads}}

==References==
{{reflist}}

{{Coord|-34.90986|138.59910|type:landmark:AU-SA|display=title}}

[[Category:Streets in Adelaide]]