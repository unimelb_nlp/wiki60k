'''Edgar W. Denison''' (August 31, 1904 – August 14, 1993) was a conservationist, amateur botanist and naturalist who was an early proponent of the value of the use of native plants in cultivated landscapes and in preserving and restoring biodiversity in natural and disturbed habitats. He provided text as well as many photographs and illustrations for the handbook, ''Missouri Wildflowers'', published by [[Missouri Department of Conservation]] in 1972, and now in its 6th edition.

== Biography ==

Edgar W. Denison was born in [[Stuttgart]], Germany on August 31, 1904.<ref name=Missouriensis>{{cite journal|last=Turner|first=Joanna|title=Edgar Denison 1904-1993|journal=Missouriensis|year=1993|volume=14|issue=2|pages=50–57|accessdate=2014-01-28}}</ref> His interest in nature was formed as a boy, when he would make frequent trips with his father to Switzerland, and became “fascinated with the roses and edelweiss he saw while hiking.”<ref name="Denison obit">{{cite news|title=Edgar W. Denison; Missouri Wildflower Expert|accessdate=2014-01-28|newspaper=St. Louis Post-Dispatch|date=1993-08-17}}</ref>  “I am a mountain goat by nature,” he once told an interviewer.<ref name="Oral History">{{cite web|title=Oral History T-0059|url=http://shs.umsystem.edu/stlouis/manuscripts/transcripts/s0829/t0059.pdf|work=Oral History Collection (S0829)|publisher=The State Historical Society of Missouri|accessdate=2014-01-28}}</ref> He attended preparatory school in Germany; then, because of the economic depression at the time, immigrated to [[St. Louis]], Missouri, in 1927, settling in Kirkwood, Missouri, in 1932, where he lived the rest of his life. He was married in 1931 to Ruth Israel.

== Informal Education ==

Denison’s personal interests were diverse. He appreciated the arts in many forms, from music to visual arts, including painting and photography. He played the piano—with a particular fondness for the music of [[Scott Joplin]]—and was a self-taught artist and nature photographer. Just as widespread was his enthusiasm for [[natural history]] in many forms, especially botany, zoology and geology.

As a youth, Denison learned the names of plants from his father’s library of botanical books.<ref name=Missouriensis />  In St. Louis, he collected books and studied native plants of Missouri. 
As a transplanted European, he was astounded by the vastness and beauty of open landscapes surrounding the city when he arrived in St. Louis. “I was tremendously impressed by the flowers, by the beautiful country, and by the limitless expanse of our country.”<ref name="Oral History" />  He took frequent long walks through the undeveloped land that was then abundant near his Kirkwood home. And his job with Union Electric gave him frequent opportunities to exploit those interests further afield. Required to make frequent visits to rural Missouri during the construction of the [[Taum Sauk Hydroelectric Power Station]], Denison collected rocks and minerals, and photographed the wildflowers he saw. He convinced Union Electric to include a nature museum (The Taum Sauk Nature Museum) as part of the new facility, and stocked the museum with many pieces and photographs from his own collections. When asked how he got into botany, he answered, “I simply worked myself into the field.”<ref name=Missouriensis />

== Influence ==

Though Denison knew very little English when he first arrived in the United States, a fact that may have contributed to his initial preference to explore the countryside over socializing, and carried his German accent throughout his life, he was generous in sharing his knowledge and his plants. His own Kirkwood garden was a showcase, with over 1,000 different varieties of plants, and he shared specimens and information freely with neighbors and visitors and even the [[Missouri Botanical Garden]].<ref name="Denison obit" />

He was very active with the Missouri Botanical Garden, and wrote many articles for the Garden’s Bulletin through the years. He also trained volunteers for the Garden’s “Answer Man” horticulture answer service, and led wildflower walks at the Garden’s Shaw Arboretum (in 2000 renamed [[Shaw Nature Reserve]]) near Gray Summit, Missouri.<ref name=Missouriensis />

Denison was co-founder of the Missouri Native Plant Society, and was an active member and leader throughout his life and contributed many articles to its publication ''Missouriensis''. He was also a long-time member of the Webster Groves Nature Study Society and contributed articles to its publication, ''Nature Notes''.<ref name=Missouriensis />

Denison was also an active conservationist and animal lover, and served on the executive committee of the St. Louis chapter of the [[Sierra Club]] for many years, and as a board member of the Missouri chapter of  [[The Humane Society of the United States]] for over 20 years.<ref name=Missouriensis />  He and Ruth Denison were also members of the Missouri Prairie Foundation, and the Denison Prairie, a tract of 440 acres in southwestern Missouri’s Barton County, is named after him in recognition of his contributions to Missouri Department of Conservation.

One of his proudest accomplishments was being hired by [[Washington University in St. Louis]] to assist with botany classes, that despite having no formal advanced education in botany.<ref name=Missouriensis />

But by far his most influential contribution was to author and provide many illustrations and photos for the ''Missouri Wildflowers'' handbook. Denison wanted to encourage people to learn Missouri’s native plants, and so arranged the book in a unique, yet easy to follow system, first by color of bloom, then by season of bloom, to make identification as easy as possible.<ref name="Globe Democrat article">{{cite news|last=LaMont|first=Anita Buie|title=He's wild about wildflowers|accessdate=2014-01-28|newspaper=St. Louis Globe-Democrat|date=3/17-18/79}}</ref>  The book has sold more than 100,000 copies to date, with all proceeds going to the Missouri Department of Conservation.

== Awards ==

* Erna R. Eisendrath Memorial Education Award (Missouri Native Plant Society) (1985)
* Julian A. Steyermark Award (MNPS) ( 1993) 
* Missouri Department of Conservation Hall of Fame (1994)

== Published Works ==

* ''Missouri Wildflowers'', Missouri Department of Conservation, 1972 
* “What Oak is this? Part I: The White Oaks,” by Edgar Denison, ''The Missouri Conservationist'' 54 (12): 12-17 (December 1993).
* “What Oak is this? Part II: The Red Oaks,” by Edgar Denison, ''The Missouri Conservationist'', (January 1994)
* "Common Plant Families of Missouri, II. Monocots", by Edgar Denison, ''Missouriensis'', Vol. 14, No. 2, 1003

== See also ==

*“He’s wild about wildflowers,” by Anita Buie LaMont, St. Louis ''Globe-Democrat'', 3/17-18/79
*“Kirkwood Garden Evokes Guardian’s Spirit,” by Marianna Riley, St. Louis ''Post-Dispatch'', 8/26/93
*“Edgar W. Denison” (obituary), St. Louis ''Post-Dispatch'', 8/17/93
*“For the chronicler of Edgar Denison’s garden, fall wildflowers are magic,” By Becky Homan, St. Louis ''Post-Dispatch'', 10/20/96

== External links ==
* [http://www.moprairie.org/ Missouri Prairie Foundation]
* [http://www.moprairie.org/placemarks/denison-prairie-barton-county/ Denison Prairie]
* [http://www.monativeplants.org/ Missouri Native Plant Society]
* [http://www.wgnss.org/ Webster Groves Nature Study Society]

== References ==

{{reflist}}

{{DEFAULTSORT:Denison, Edgar W.}}
[[Category:1904 births]]
[[Category:1993 deaths]]
[[Category:Place of death missing]]
[[Category:Missouri Botanical Garden people]]
[[Category:American naturalists]]
[[Category:German naturalists]]