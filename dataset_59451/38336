{{Infobox Simpsons episode
| episode_name      = The Bob Next Door
| image             = [[File:The Bob Next Door Promotional Image.jpg|200px]]
| image_caption     = Promotional image for the episode featuring Bart, Sideshow Bob and Walt, the Simpsons' new neighbor whom Bart at first thinks ''is'' Sideshow Bob.
| episode_no        = 463
| prod_code         = MABF11
| airdate           = May 16, 2010
| show runner       = [[Al Jean]]
| writer            = [[John Frink]]
| director          = [[Nancy Kruse]]
| blackboard        = "[[Batman]] is "not 'nothing'" without his utility belt".
| couch_gag         = Harold from ''[[Harold and the Purple Crayon]]'' draws the Simpsons' living room before the Simpsons arrive. [[Homer Simpson|Homer]] asks him to “Draw me some beer!”, and is obliged.
| guest_star        = [[Kelsey Grammer]] as [[Sideshow Bob]]
| commentary        =
| season            = 21
}}

"'''The Bob Next Door'''" is the twenty-second episode of ''[[The Simpsons]]''{{'}} [[The Simpsons (season 21)|twenty-first season]]. It originally aired on the [[Fox Broadcasting Company|Fox network]] in the United States on May 16, 2010. In the episode, [[Bart Simpson]] becomes convinced that their new neighbor is [[Sideshow Bob]] in disguise, but after a trip to the Springfield Penitentiary they find a distressed Bob still incarcerated. Eventually Bart discovers that Bob has surgically swapped faces with his cellmate and still plans to kill him, although he is ultimately defeated.

The episode was written by [[John Frink]] and directed by [[Nancy Kruse]]. The episode guest stars [[Kelsey Grammer]] as [[Sideshow Bob|Sideshow Bob Terwilliger]]. The episode's plot is based on the film ''[[Face/Off]]''. "The Bob Next Door" received positive reviews from critics; most agreed that it was a funny return for Sideshow Bob and an improvement over "[[Funeral for a Fiend]]" and "[[The Italian Bob]]".

==Plot==
A [[financial crisis]] in Springfield causes the sale of many houses and the release of all low-level criminals from Springfield Penitentiary, including a man named Walt Warren. Walt purchases a house next door to the Simpson family (where [[The Winfields]] and [[New Kid on the Block|Ruth and Laura Powers]] used to live), and he immediately charms the neighborhood. However, [[Bart Simpson|Bart]] is convinced that Walt is [[Sideshow Bob]] in disguise, because they have the [[Kelsey Grammer|same voice]]. He tries several times to find proof, but fails. [[Marge Simpson|Marge]] convinces him otherwise by taking him to visit the penitentiary, where they see Bob locked in a [[padded cell]], wearing a [[straitjacket]] and writing "Bart Simpson Will Die!" on the walls. A reassured Bart decides to go to a [[Springfield Isotopes|baseball game]] with Walt. However, Bart's initial instincts prove right when "Walt" removes his small shoes to show his long feet folded inside, revealing himself to be Sideshow Bob. Bob restrains Bart in the car and gags him with duct tape, planning to take him to [[Springfield (The Simpsons)#Five Corners|Five Corners]], a location where five states meet, to kill him.

Meanwhile, the real Walt Warren escapes prison while bearing Bob's hair and face and comes to the Simpsons' home. At first, everyone thinks Bob has escaped prison, but Walt's short feet (which was also used as a clue in "[[Krusty Gets Busted]]" to clear Krusty's name for robbing the Kwik-E-Mart) reveal his true identity. Walt explains that he and Bob were cellmates and, prior to Walt's release, Bob drugged him and performed a transplant to [[face transplant|switch their faces]]. The transplant left Walt unable to talk properly, and his garbled speech led the guards to put him in the padded cell. He wrote his message on the wall as a warning, but it was misinterpreted as a threat. Walt and the Simpsons immediately go after Bob, knowing that Bart's life is in danger. Meanwhile, a waitress at a roadside diner becomes infatuated with Bob-as-Walt until Bob's new face peels off. Amidst a distraction outside the diner, Homer, Marge, and [[Lisa Simpson|Lisa]] travel to Mexico in search of Bart while Walt gets away and continues to the Five Corners to save Bart.

At Five Corners, Bob intends to kill Bart in such a way that the crime takes place in [[extraterritorial jurisdiction|five separate states]], thus making it impossible to prosecute. Bart stalls by repeatedly jumping into the same state as Bob until Walt arrives. Walt and Bob struggle over the gun, but just before Bob can fire on either Walt or Bart, [[Chief Wiggum]] and the Springfield Police Department arrive to arrest Bob, having been tipped off by Bart. Bob jumps into the other states in order to escape their jurisdiction, only to be promptly confronted by police from each state, and he is taken into custody by officers with a strong [[New Jersey]] accent. His house is bought by [[Ned Flanders]]' cousin Ted, and Homer groans at the realization that he now lives next door to ''two'' Flanders families. The Sideshow Bob [[leitmotif]] first introduced in the episode "[[Cape Feare]]" plays over the credits.

==Production==
The episode was written by [[John Frink]], his second writing of the season after "[[Stealing First Base]]". It is also his third [[Sideshow Bob]] writing credit after "[[The Great Louse Detective]]" and "[[The Italian Bob]]". The episode was directed by [[Nancy Kruse]], her second director's credit for the season after "[[The Devil Wears Nada]]". It features the return of recurring guest voice [[Kelsey Grammer]] to voice recurring character [[Sideshow Bob]], making it his 12th vocal appearance. The episode was originally slated to air on January 14, 2010, along with "[[Once Upon a Time in Springfield]]" and ''[[The Simpsons 20th Anniversary Special – In 3-D! On Ice!]]''.<ref name="IGN2">{{cite web |last=Goldman |first=Eric |date=September 25, 2009 |url=http://www.ign.com/articles/2009/09/25/the-simpsons-say-hello-to-season-21 |publisher=[[IGN]] |accessdate=February 5, 2016 |title=The Simpsons Say Hello to Season 21}}</ref>

==Cultural references==
The plot of "The Bob Next Door" is based on the movie ''[[Face/Off]]''.<ref name="tv squad"/> The opening couch gag also features Harold from  the 1955 children's book ''[[Harold and the Purple Crayon]]''.<ref name="tv squad"/> Bart tries to tempt Walt to sing "Three Little Maids from School Are We" from ''[[The Mikado]]''; the same song was also used in an earlier episode, "[[Cape Feare]]", which also featured Sideshow Bob. Later in the show, when Sideshow Bob reveals his true identity, he exclaims he is now "able to sing all the Gilbert & Sullivan I damn well please", followed by him pulling a Japanese fan out of the glove box and singing the opening notes of "Behold The Lord High Executioner", another number from ''The Mikado'', to Bart's horror.<ref name="tv squad"/> Marge and Homer tell Bart that a lot of people have voices like Sideshow Bob's, such as "Frasier on ''[[Cheers]]'', Frasier on ''[[Frasier]]'' or Lt. Cmdr. Tom Dodge in ''[[Down Periscope]]''."  Both of these characters were played by Sideshow Bob's voice, Kelsey Grammer.<ref name="ign"/> When [[Sideshow Bob]] steps on the rake it is a call back to the famous scene from "[[Cape Feare]]" in which he steps on multiple rakes.<ref name="tv squad"/>

==Reception==
In its original American broadcast on the Fox network on May 16, 2010, "The Bob Next Door" was viewed by an estimated 6.258 million households and got a 2.9 rating/9% share in the 18–49 demographic, rising 9% from "[[Moe Letter Blues]]". It came second in its time slot after the [[season finale]] of ''[[Survivor: Heroes v. Villains]]'' and became the second highest-rated show in Fox's "[[Animation Domination]]" programming block after a new episode of ''[[Family Guy]]'', according to the [[Nielsen Media Research]].<ref>{{Cite web|url=http://tvbythenumbers.com/2010/05/17/tv-ratings-survivior-finale-tops-abcs-finale-sunday/51572|title=TV Ratings: Survivor Finale Tops ABC's Finale Sunday, Celebrity Apprentice Ties Series Low|first=Bill|last=Gorman|publisher=[[TV by the Numbers]]|accessdate=May 17, 2011}}</ref>

The episode received positive reviews. Robert Canning of [[IGN]] gave it an 8.5 rating, stating that it was "Great" and "Overall, this was a great return to form for an appearance from Sideshow Bob. The vengeful character has been let down by recent episodes, but 'The Bob Next Door' has reminded us what makes Bob so much fun."<ref name="ign">{{cite web |last=Canning |first=Robert |date=May 17, 2010 |url=http://www.ign.com/articles/2010/05/17/the-simpsons-the-bob-next-door-review |publisher=IGN |accessdate=February 5, 2016 |title=The Simpsons: "The Bob Next Door" Review}}</ref> Canning later named "The Bob Next Door" the best episode of the season tied with "[[The Squirt and the Whale]]".<ref>{{cite web |last=Canning |first=Robert |date=June 1, 2010 |url=http://www.ign.com/articles/2010/06/01/the-simpsons-season-21-review |publisher=IGN |accessdate=February 5, 2016 |title=The Simpsons: Season 21 Review}}</ref> TV Fanatic gave the episode four out of five and stated that they enjoyed the plot twists but thought the jokes were unfunny, remarking that "the episode just didn't have the same humor as say, '[[Cape Feare]]'."<ref>{{Cite web|url=http://www.tvfanatic.com/2010/05/the-simpsons-review-the-bob-next-door|title=The Simpsons Review: "The Bob Next Door"|first=Eric|last=Hochberger|publisher=TV Fanatic}}</ref>

John Teti of ''[[The A.V. Club]]'' gave the episode a B+, which tied it for the best grade of "Animation Domination" after ''[[American Dad!]]''. He stated that "The showdown at Five Corners played out just like 'Cape Feare,' complete with rake gag, which is not a bad thing. If ''The Simpsons'' intends to self-plagiarize (and it obviously does), that's a good episode to copy."<ref>{{cite web |last=Teti |first=John |date=May 17, 2010 |url=http://www.avclub.com/tvclub/the-bob-next-doorclevelands-angelsthe-splendid-sou-41204 |publisher=''[[The A.V. Club]]'' |accessdate=February 5, 2016 |title="The Bob Next Door"/"Cleveland's Angels"/"The Splendid Source"/"Great Space Roaster"}}</ref> Sharon Knolle of [[AOL TV]] said, "I'd say overall this ep ranks with some of the better Sideshow Bob eps, if not the very best. Certainly, it beats the heck out of '[[The Italian Bob]]' and '[[Funeral for a Fiend]].'"<ref name="tv squad">{{cite web |last=Knolle |first=Sharon |date=May 17, 2010 |url=http://www.aoltv.com/2010/05/17/the-simpsons-the-bob-next-door-recap/ |publisher=[[AOL TV]] |archiveurl=https://web.archive.org/web/20120603123648/http://www.aoltv.com/2010/05/17/the-simpsons-the-bob-next-door-recap/ |archivedate=June 3, 2012 |deadurl=yes |accessdate=February 5, 2016 |title='The Simpsons' - 'The Bob Next Door' Recap}}</ref>

==References==
{{Reflist}}

==External links==
<!-- {{wikiquote|The Simpsons#The Bob Next Door|The Bob Next Door}} -->
{{Portal|The Simpsons}}
*[http://www.thesimpsons.com/recaps/season21/index.htm "The Bob Next Door"] at TheSimpsons.com
*[http://www.snpp.com/episodeguide/season21.html "The Bob Next Door"] at [[The Simpsons Archive]]
*{{tv.com episode|the-simpsons/the-bob-next-door-1304155}}
*{{imdb episode|1645678}}

{{Simpsons Sideshow Bob}}
{{The Simpsons episodes|21}}

{{good article}}

{{DEFAULTSORT:Bob Next Door, The}}
[[Category:The Simpsons (season 21) episodes]]
[[Category:2010 American television episodes]]
[[Category:Black comedy]]