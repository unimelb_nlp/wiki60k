'''Anand Thakore''' (born 17 February 1971) is a [[poet]] and [[Hindustani classical music|Hindustani classical vocalist]]. ''Elephant Bathing'' (Poetrywala, 2012),  ''Mughal Sequence'' (Poetrywala, 2012) and ''Waking in December'' (Harbour Line, 2001) are his three collections of verse.<ref name=":0" /><ref name=":1" /> He received training in Hindustani vocal music for many years from [[Satyasheel Deshpande]] and Pandit Baban Haldankar of the [[Agra Gharana]]. He is the founder of ''Harbour Line'', a publishing collective, and ''Kshitij'', an interactive forum for musicians.<ref>{{cite web|author=tata_admin |url=http://www.litlive.in/?p=481 |title=event2 « Tata Litlive! 2015 |publisher=Litlive.in |date=2014-12-29 |accessdate=2016-01-11}}</ref>

== Early life and background ==

Anand Thakore was born in [[Mumbai]] in 1971. His father Sandeep Thakore was a sitar enthusiast and a disciple of the late Ustad Mohammed Khan Beenkaar. As a child he was fascinated by [[Hindu Mythology]] and [[Indian classical music]] and [[Indian classical dance|dance]]. His Grandmother, Kapila Thakore was an award-winning Gujarati children's writer and translator.

He spent a part of his childhood in the UK and has lived in India since then. He was educated at [[Solihull School]], in the West Midlands, and at [[Cathedral and John Connon School]], Mumbai. While in the UK, he received instruction in Western choral singing and violin. On returning to Mumbai he studied Hindustani vocal music under Ustad Aslam Khan and Pandit Satyasheel Deshpande, senior disciple of [[Kumar Gandharva|Pandit Kumar Gandharva]].

He gave his first public performance at the Vitaan Festival of Music and Dance, 1989, hosted by the Sheriff of Mumbai. He has been giving public concerts and '[[mehfil]]s' regularly since. While learning music he simultaneously developed a passion for [[English literature]] and various Indian languages: [[Hindi]], [[Urdu]], [[Sanskrit]] and [[Braj Bhasha]]. In addition to writing poetry in English, he began, in his late teens, to compose his own Hindustani compositions with Braj and Hindi lyrics.

He earned a BA in English Literature from the [[University of Mumbai]] where he also studied courses in [[Sanskrit literature]]. He then earned an MA in English Literature from the [[University of Pune]].

== Literary career and style ==

Anand Thakore has been writing verse in English since his teens. He is the author of three books of verse ''Elephant Bathing'', ''Mughal Sequence'' and ''Waking in December''. He has also authored a number of critical essays on music and poetry and a pamphlet of 'Khayal' lyrics in Hindi.

Thakore's poems first appeared in Literature alive, a British Council Journal.<ref>[https://books.google.com/books?id=NN48WUBx_54C&pg=PA21&lpg=PA21&dq=literature+alive+british+council&source=bl&ots=PVnxFEgp5u&sig=xRKjkWfdi0tC5vgBsqalqBWAAd4&hl=en&sa=X&ei=kuPdUIGxGsTqrAe3j4GwBg&ved=0CF8Q6AEwBg Anand Thakore's poem in Literature alive]</ref> His work has appeared in various national and international journals<ref>[http://www.tandfonline.com/doi/pdf/10.1080/17449850701430671 Journal of Postcolonial Writing Volume 43, Issue 2, 2007]</ref> and anthologies since then. He has read work at major literary festivals<ref>[http://www.litlive.in/?p=481 Anand Thakore at Tata literature live!, The Mumbai Litfest]</ref>

Post-Colonial critic Bruce King points out in [[World Literature Today]] ( Vol. 75, No. 3/4, Summer - Autumn, 2001, p.&nbsp;136) : "That Thakore is a classical singer shows in his imagery and complex patterns of sound, and in the texture of his verse. There is a song-like quality about his verse".<ref>[http://www.jstor.org/stable/40156810 World Literature Today Vol. 75, No. 3/4 (Summer - Autumn, 2001) on jstor.org]</ref>

[[Jeet Thayil]] writes about Anand Thakore in the book ''The Bloodaxe Book of Contemporary Indian Poets (Newcastle: Bloodaxe, 2008)'' : "Both cultures feed and animate his work. His poems have a line and weight reminiscent of mid-twentieth century British verse; his music reaches out to antiquity".<ref>[http://www.bloodaxebooks.com/titlepage.asp?isbn=1852248017 The Bloodaxe Book of Contemporary Indian Poets (Newcastle: Bloodaxe, 2008)]</ref>

"From a child who grew up partly in England writing prose, Thakore evolved into a teenager who explored Braj lyrics, which he had to sing as a student of music. His love for poetry took him to literary greats who lived in Mumbai such as Adil Jussawalla, [[Dom Moraes]] and [[Gieve Patel]] who guided him through the jungle of words. But he considers [[Hart Crane]] as one of his earliest poetic influences&nbsp;... The metre and discipline of traditional verse forms helped Thakore find a 'deeper integration of ideas and meanings.' "Free verse," he says, "was driving me crazy. There was too much linguistic and cultural chaos in my head'." - Anupama Raju, [[The Hindu]].<ref name=":0">[http://www.thehindu.com/todays-paper/tp-features/tp-literaryreview/article662166.ece Music of the spoken word, Anupama Raju, The Hindu]</ref>

In 2001 Anand Thakore co-founded ''Harbour Line'' - a publishing collective devoted to English verse on the Indian subcontinent. ''Harbour Line'' was founded in collaboration with poets Jane Bhandari, ( 'Aquarius',2002, ISBN 81-902981-1-9), Deepankar Khiwani ('Entr'acte', 2006, ISBN 81-902981-2-7) and Vivek Narayanan (Universal Beach, 2006, ISBN 81-902981-3-5).

The verse of ''Waking in December'', Thakore's first book shows a reverence for definite form and an interest in classical structures like the [[sonnet]] and the [[villanelle]],<ref>[http://openspaceindia.org/item/vacillations-of-a-recondite-nudist.html Vacillations Of A Recondite Nudist on http://openspaceindia.org]</ref> exemplified in poems like ''Chandri Villa''<ref>[http://openspaceindia.org/item/chandri-villa.html Chandri Villa on openspaceindia.org]</ref> or ''What I can Get away with''.<ref>[http://www.hindu.com/lr/2008/10/05/stories/2008100550170500.htm Literary review of Sixty Indian Poets in The Hindu. October 05, 2008.]</ref> This interest in classical forms is linked to his training as a Hindustani classical vocalist and composer.

In 2006 he received a Charles Wallace India Trust<ref>[http://www.britishcouncil.in/study-uk/charles-wallace-india-trust Charles Wallace India Trust]</ref> grant for an experimental music-poetry collaboration in the UK with composer and guitarist [[Pete M Wyer|Pete Wyer]]. He has a number of recorded readings with music - both Hindustani and western- some now available with his books of verse.

His poems and critical essays on music and poetry have appeared in leading national and international journals and anthologies. His poetry in included in ''Anthology of Contemporary Indian Poetry''<ref>{{cite web|title=Anthology of Contemporary Indian Poetry|url=http://bigbridge.org/BB17/poetry/indianpoetryanthology/Anand_Thakore.html|publisher=BigBridge.Org|accessdate=9 June 2016}}</ref> ( United States ).

Anand Thakore was judge and a co-editor for the first [[Montreal International Poetry Prize]] (2011) with [[Valerie Bloom]], [[Fred D'Aguiar]], [[John Kinsella (poet)|John Kinsella]] and [[Stephanie Bolster]], amongst others.<ref>[http://montrealprize.com/about-us/2011-competition/editorial-board Editorial board of Montreal International Poetry Prize 2011]</ref>

== Publications ==

=== Books ===
* ''Mughal Sequence'' (Poetrywala, 2012). ISBN 978-81-922254-3-2<ref>[http://punemirror.in/article/101/20120524201205240815465811b1e73f9/Some-Mughal-Musings.html Some Mughal Musings by Eunice de Souza in Pune Mirror.in]</ref><ref>[http://www.caravanmagazine.in/bookshelf/mughal-sequence Mughal Sequence published at The Caravan - A Journal of Politics and Culture ]</ref><ref>[http://www.wherevent.com/detail/Anand-Thakore-BOOK-LAUNCH-Anand-Thakore-MUGHAL-SEQUENCE-and-ELEPHANT-BATHING Book launch of Mughal Sequence and Elephant Bathing]</ref>
* ''Elephant Bathing'' (Poetrywala, 2012). ISBN 978-81-922254-4-9<ref name=":1">[http://www.thehindu.com/arts/books/idioms-and-images/article3968307.ece Newspaper article about ''Elephant Bathing'' by Gopikrishnan Kottoor in ][[The Hindu]] October 6, 2010</ref><ref>[http://tehelka.com/dalrymple-rushdie-top-authors-reading-list-in-2012 writer-academic Amit Chaudhuri's comments about Anand Thakore's ''Elephant Bathing'' ]</ref><ref>[http://www.hindustantimes.com/StoryPage/Print/981453.aspx ]</ref><ref>[http://www.hindustantimes.com/Books/LiteraryBuzz/Rushdie-Dalrymple-top-authors-reading-list-in-2012/Article1-981453.aspx Rushdie, Dalrymple top authors' reading list in 2012, Hidustan Times, PTI New Delhi, December 27, 2012]</ref>
* ''Waking in December'' (Harbour Line, 2001). ISBN 81-902981-0-0<ref>[http://www.hindu.com/2001/09/02/stories/1302017u.htm Article on Waking in December, The Hindu, September 2, 2001]</ref>

=== Essays ===
* ''On the Music of 'A Missing Person': Adil Jussawalla and the Craft of Despair'' (New Quest, Pune, Ed. [[Dilip Chitre]])<ref>[http://www.poetryinternationalweb.net/pi/site/cou_article/item/2690 On the Music of 'A Missing Person': Adil Jussawalla and the Craft of Despair on poetryinternationalweb.net. November 01, 2004]</ref>
* ''Myth and Monologue'' (New Quest, Pune, Issue 167, 2007, ed. Dilip Chitre)
* ''If Music be the food of speech'' (New Quest, issue 169, 2007, ed. Dilip Chitre)<ref>[http://www.kshitijmusic.org/library_text_anand_thakore_If_Music_Be_The_Food_Of_Speech.html If Music be the food of speech on kshitijmusic.org]</ref>

=== Editorial work ===
*  {{cite book  |title=Global Poetry Anthology: 2011 |publisher=Vehicle Press |location=Montreal |year=2011 |isbn=978-1-55065-318-2|editor-first1=Eric |editor-last1=Ormsby | editor-first2=Arand |editor-last2=Thakore}} 
* {{cite book |title=Sarpa Satra |publisher=Pras Prakashan |year=2004 |isbn=81-902110-1-3}}

=== Anthologised Poems ===

Thakore's poetry has appeared in the following anthologies amongst others:

* ''The HarperCollins Book of English Poetry'' (Harper Collins, 2012 ed. [[Sudeep Sen]])<ref>[http://harpercollins.co.in/BookDetail.asp?Book_Code=3375 The HarperCollins Book of English Poetry on http://harpercollins.co.in]</ref><ref>[http://www.hindustantimes.com/Books/booksreviews/Waxing-poetic/Article1-930189.aspx Review : The Harper Collins Book of English Poetry by Manjula Narayan, Hindustan Times. September 15, 2012 ]</ref>
* ''Sixty Indian Poets'' (Penguin India 2008, ed. Jeet Thayil)<ref>[http://www.hindu.com/lr/2008/10/05/stories/2008100550170500.htm Literary Review, The Hindu, October 5, 2008]</ref>
* ''Reasons For Belonging'' (Viking, 2002, ed. [[Ranjit Hoskote]]).  ISBN 0-670-89094-4
* ''Journal of Postcolonial Writing'' (Oxford, UK, 2007 ed. [[Janet Wilson]])<ref>[http://www.tandfonline.com/toc/rjpw20/43/2 Journal of Postcolonial Writing on tandfonline.com]</ref>
* ''Poetry Wales'' (Summer Issue 2002, Bridgend, Wales ed. [[Robert Minhinnick]])<ref>[http://www.hindu.com/thehindu/lr/2002/08/04/stories/2002080400130201.htm Literary review of Poetry Wales in The Hindu. August 04, 2002]</ref>
* ''Fulcrum Three'' (Cambridge, MA, USA, 2004, ed. Philip Nikolayev)<ref>[http://fulcrumpoetry.org/issues/3/ Fulcrum Three on fulcrumpoetry.org]</ref>
* ''The Bloodaxe book of Contemporary Indian Poets'' (Bloodaxe, UK, 2008 ed. Jeet Thayil)<ref>[http://www.gbv.de/dms/goettingen/571649416.pdf pdf file containing index of The Bloodaxe book of Contemporary Indian Poets]</ref>
* ''Both Sides Of The Sky'' (National Book trust, India, 2008, ed. [[Eunice de Souza]])<ref>[http://openlibrary.org/books/OL22975831M/Both_sides_of_the_sky Both Sides Of The Sky on http://openlibrary.org]</ref>
* ''Poetry with Prakriti'' (Prakriti Foundation, 2009)<ref>[http://www.prakritifoundation.com/events09/poetrywp/poetrywp.html Poetry with Prakriti on prakritifoundation.com]</ref>

== Musical career and style ==

As a child, Anand Thakore learnt Hindustani vocal music, [[sitar]] and [[Tabla]]. When the family moved to England for a period, he studied rudimentary western musical theory, and violin and sang soprano in the Solihull Chapel Choir. He has always maintained a strong interest in [[Classical music|Western Classical music]] and jazz though fusion as such, is not his choice of form. On returning to Mumbai he trained for a while with Ustad Aslam Khan of the [[Jaipur-Atrauli gharana|Atrauli Gharana]]; though most of his training has been under Pandit Satyasheel Deshpande, senior disciple of Pandit Kumar Gandharva.

Inspired by his Guru, Pandit Satyasheel Deshpande and Pandit Kumar Gandharva (whose music he had the good fortune to listen to at close quarters), Anand Thakore began to compose his own compositions at the age of sixteen, working with Hindi Lyrics in the traditional form known as the [[Bandish]]. In addition to being taught a host of traditional compositions from different gharana-s and sources, he was also initiated into various aspects of gayaki ( i.e. modes of improvisation and raga-development) and he was encouraged to seek out his own mode of presentation and expression. He played a supportive role in the musicological research and documentational activities at the Samvaad Foundation<ref>[http://www.satyasheel.com/samvaad-foundation Samvaad foundation on satyasheel.com]</ref> run by his Guru, where he had the good fortune to interact with a wide spectrum of artists of the older generation; to vocally accompany and comparatively analyse the music of such stalwarts as Ustad Salamat Ali, [[Ramashreya Jha|Pandit Ramashreya Jha]], [[K. G. Ginde|Pandit K G Ginde]], and Pandit Sharadchandra Arolkar, amongst others.

In 1994 he received a scholarship to study music full-time from the [[Ministry of Human Resource Development (India)]]. He then studied music for several years under Pandit Baban Haldankar of the Agra Gharana. This ''Taleem'' (training) familiarized him with a large number of Agra-Gharana compositions and various aspects of the Agra tradition: ''Bol-alaap'', ''Nom-tom'' ''alaap'' and ''Layakari''.

Thakore's first professional concert was at the Vitaan Festival of Music and Dance 1989, hosted by the Sheriff of Mumbai. He has accompanied his Guru-s and performed across the country as a soloist at mehfil-s and music festivals since then.<ref>[http://underscorerecords.com/events/detail/437 Dr. Pt. Chandrashekhar Rele Smriti Sangeet Utsav]</ref>

In 2001 he co-founded Kshitij, an interactive forum for musicians, devoted to the spirit of the live Hindustani ''mehfil''. Kshitij was founded in collaboration with vocalists [[Sanjeev Chimmalgi]], Kedar Bodas, Krishna Bhat and Tabla accompanist Rupak Kharvandikar. The group has organized concerts of senior and younger artists in semi-private soirees as well as public auditoriums. It also functions as a musicians' salon, repeatedly creating space for interaction between musicians, connoisseurs,critics and musicologists.

In 2006 he received a grant from the Charles Wallace India Trust to work on a collaborative experimental project with British composer and jazz-guitarist [[Pete M Wyer|Pete Wyer]]. A participant in the ''[[Time Structured Mapping]]'' project, he collaborated with soprano Evelyne Beech, microtonal vocalist Toby Twining, Pianist Burkhard Finke and the [[Orchestra of the Swan]] on a piece called ''Four Bridges''; a score or ''Time Structured Map'', with scope for improvisation, simultaneously recorded in various parts of the world.

In 2009 Anand Thakore presented ''Sabadpiya Ki Khoj'', a concert and presentation at the [[National Centre for the Performing Arts (India)|National Centre for the Performing Arts]], Mumbai,<ref>[http://www.ncpamumbai.com/event/sabadpiya-ki-khoj Sabadpiya ki khoj on ncpamumbai.com]</ref>  focusing on his work as a composer of ''bandish-es'' composed under the ''Takhallus'' or pseuodonym ''Sabadpiya'' (''sabadpiya'' implying ''lover of the word'' and reminiscent of Agra gharana pen-names like [[Khadim Hussain Khan|Ustad Khadim Hussain]] ''Sajan piya'' etc.)  The event brought together his work both as a composer in Hindustani [[Raga]] and [[Tala (music)|Tala]] and as a lyric-writer in Hindi. A pamphlet of the words of these compositions brought out by Kshitij was also published at the event.

Guldasta-e-Khayal (2011) is his latest Hindustani classical CD. The CD features a live 'mehfil' in Mumbai with ragas Multani, Tilak Kamod, Darbari and Malkauns and is distributed by Underscore Records Pvt. ltd.

== Discography ==

* Guldasta-e-Khayal ( Underscore Records Pvt. Ltd. 2011)<ref>[http://underscorerecords.com/catalog/detail/524/Guldasta-e-Khayal Guldasta-e-Khayal on underscorerecords.com]</ref>
* Live Mehfil ( Musicians' Guild, 2009)

== Awards and recognition ==

* He is the recipient of a National scholarship for music from the Ministry of Human Resource Development, a grant from the Charles Wallace India Trust for experimental work in the UK.
* He has received "Sur-mani" award for excellence in classical music, conferred by the Sur Singar Samsad.

== References ==
{{Reflist|30em}}

== External links ==
* [http://www.kshitijmusic.org/core_anand_thakore1.html Anand Thakore on Khitij Group]
* [http://www.bangaloremirror.com/index.aspx?page=article&sectid=36&contentid=2012052320120523224848190d1a41b36 Article in [[Bangalore Mirror]] ]
* [http://www.dnaindia.com/mobile/report.php?n=1120772 An afternoon of Renaissance men. An article by Malavika Sangghvi in [[Daily News and Analysis|DNA]], Mumbai. September 11, 2012]
* [http://www.bangaloremirror.com/printarticle.aspx?page=comments&action=translate&sectid=36&contentid=2012110420121104203218163c604a8d&subsite= Update from Poetry Live by Anindita Sengupta, November 04, 2012]

{{DEFAULTSORT:Thakore, Anand}}
[[Category:Living people]]
[[Category:1971 births]]
[[Category:Hindustani singers]]
[[Category:Indian male classical singers]]
[[Category:English-language poets from India]]
[[Category:Indian male poets]]
[[Category:21st-century Indian poets]]
[[Category:Poets from Maharashtra]]