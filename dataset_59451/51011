{{Infobox journal
| title = Contemporary Sociology
| cover = [[Image:Contemporary Sociology cover.gif]]
| editor = Alan Sica
| discipline = [[Sociology]]
| former_names = 
| abbreviation = Contemp. Sociol.
| publisher = [[SAGE Publications]]
| country =
| frequency = Bimonthly
| history = 1972–present
| openaccess = 
| license = 
| impact =0.739
| impact-year = 2015
| website = http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal201970
| link1 = http://csx.sagepub.com/content/current
| link1-name = Online access
| link2 = http://csx.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 00943061
| OCLC = 958948
| LCCN = 72621888 
| CODEN = C0S0AG
| ISSN = 0094-3061 
| eISSN = 1939-8638 
}}
'''''Contemporary Sociology''''' is a bimonthly [[Peer review|peer-reviewed]] [[academic journal]] of [[sociology]] published by [[SAGE Publications]] in association with the [[American Sociological Association]] since 1972. Each issue of the journal publishes a large number of both in-depth and brief reviews of recent publications in sociology and related disciplines, as well as a list of publications received that have not been reviewed. In 2010 the journal published just under 400 [[book review]]s.<ref name=WoS>{{cite web |url=http://isiwebofknowledge.com |title=Web of Science |year=2011 |accessdate=2011-07-21}}</ref> In addition, the journal also publishes a small number of review essays and discursive articles in each issue. The [[editor-in-chief]] is Alan Sica ([[Pennsylvania State University]]).

== Abstracting and indexing ==
''Contemporary Sociology'' is abstracted and indexed in [[Scopus]], [[CSA Sociological Abstracts]], [[Current Contents|Current Contents/Physical, Chemical and Earth Sciences]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2015 [[impact factor]] is 0.739, ranking it 84th out of 142 journals in the "Sociology" category.<ref name="WoS"/>

== References ==
{{Reflist}}

==External links==
* {{Official website|http://csx.sagepub.com/}}
* [http://www.asanet.org/journals/cs/index.cfm Journal page] at homepage of the American Sociological Association

[[Category:SAGE Publications academic journals]]
[[Category:Publications established in 1972]]
[[Category:Bimonthly journals]]
[[Category:Sociology journals]]
[[Category:English-language journals]]


{{socialscience-journal-stub}}