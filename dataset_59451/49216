{{for|the economist|Don Lavoie}}
'''Donald Lavoie''' (born 21 May 1942) is a self-proclaimed former [[Contract killing|hit man]] for the [[Dubois Brothers|Dubois Gang]], situated in [[Montreal]], Quebec, Canada.<ref name="google.ca"/><ref name="Motreal Gazette"/> During his career as a hit man, Lavoie assassinated at least 15 people, to which he later confessed.<ref name="google.ca"/><ref name="Motreal Gazette"/> Lavoie's [[testimony]] was used by the Montreal Police Force to convict members of the Dubois Gang. Until his testimony, the Dubois Gang had been nearly impossible to infiltrate.

==Early childhood==
Donald Lavoie was born in [[Chicoutimi]], Quebec, Canada, and was one of three children. At a young age, his parents surrendered him and his siblings to a local orphanage.<ref name="auto">True Face of Crime in Quebec (Director). (2006). The Hitman (Le Tueur) [Motion picture]. Canada.</ref> Why the children were handed into care is unclear. Lavoie claims he was a smart young man and did well in school. As a teenager he would commit breaking and entering and would rob convenience stores.<ref name="auto"/>

==Hit man==
Lavoie moved to Montreal and soon after came in contact with the Dubois brothers, also known as the Dubois Gang. After his infiltration into the gang, he became an important member and was known as the Gang's top hit man.<ref name="Motreal Gazette">{{cite news|url=https://news.google.com/newspapers?nid=1946&dat=19821115&id=XIkxAAAAIBAJ&sjid=NqUFAAAAIBAJ&pg=3137,2061952&hl=en |title=I don't fear vengeance, police informant says |work=The Gazette |location=Montreal |date= November 15, 1982|accessdate=December 28, 2015}}</ref> During Lavoie's career as a hit man, he admitted to killing at least fifteen people with a gun (the exact number is unknown).<ref name="Motreal Gazette"/> Lavoie testified to 27 murders committed by the Dubois Gang.<ref name="google.ca">{{cite book|last=Auger|first=Michel|title=The Biker Who Shot Me: Recollections of a Crime Reporter|url=https://books.google.com/books?id=XJygZdld0O4C&pg=PT89|date=8 February 2012|publisher=McClelland & Stewart|isbn=978-1-55199-695-0|pages=89–}}</ref><ref name="Motreal Gazette"/> Lavoie dedicated ten to twelve years of his life to the Dubois Gang.<ref name="Schneider2009">{{cite book|author=Stephen Schneider|title=Iced: The Story of Organized Crime in Canada|url=https://books.google.com/books?id=ZO8jKSn25DAC&pg=PA273|date=9 December 2009|publisher=John Wiley & Sons|isbn=978-0-470-83500-5|page=273}}</ref>

==Informant==
Lavoie's career came to an end when he overheard a conversation between Claude Dubois and another gang member planning to shoot him.<ref name="google.ca"/> He was able to hide safely from the other gang members, by sliding down a laundry chute and waiting patiently.<ref name="google.ca"/> Lavoie now needed police protection and decided to testify against the Dubois Gang.<ref>{{cite web|url=http://xxxorganizedcrime.com/crime-encyclopedia-j/|title=Crime Encyclopedia: J|publisher=|accessdate=13 January 2016}}</ref>

When Lavoie agreed to testify against Claude Dubois in 1982,<ref name="TCP-1"/> it resulted in Claude Dubois serving a life sentence for murder charges.<ref name="theglobeandmail.com">{{cite web|url=http://www.theglobeandmail.com/news/national/family-says-farewell-to-a-scion-of-montreals-first-family-of-crime/article22294417/|title=Family says farewell to a scion of Montreal's brotherhood of crime|work=The Globe and Mail|accessdate=13 January 2016}}</ref> Lavoie also testified, as the key witness, against Adrien Dubois.<ref name="TCP-1"/> In the testimony, Lavoie claimed he drove Adrien Dubois to a location to murder someone. Lavoie was not convicted of any murder charges he admitted to (but did serve some time in a penitentiary),<ref name="google.ca"/> and was placed in the [[Witness protection|Witness Protection Program]].<ref>{{cite web|url=https://books.google.ca/books?id=ZO8jKSn25DAC&pg=PA273&lpg=PA273&dq=donald+lavoie|title=Iced|publisher=}}{{full}}</ref> The testimony of Donald Lavoie was crucial to bringing the  Dubois Gang down, and the gang was dissolved after that. Along with the murders Lavoie committed himself, he was also able to give insight on many other murders and other criminal acts.<ref name="google.ca"/><ref name="theglobeandmail.com"/><ref>[http://go.galegroup.com.roxy.nipissingu.ca/ps/retrieve.do?sort=DA-SORT&docType=Article&tabID=T004&prodId=STND&searchId=R2&resultListType=RESULT_LIST&searchType=BasicSearchForm&contentSegment=&currentPosition=5&searchResultsType=SingleTab&inPS=true&userGroupName=ko_acd_can&docId=GALE%7CA164083153&contentSet=GALE%7CA164083153&authCount=1&u=ko_acd_can Go.galegroup.com]. {{paywall}}{{full}}</ref><ref name="Motreal Gazette"/>

== Life after crime ==
After serving his time, Lavoie now lives in Laurentian, Quebec, Canada.<ref name="google.ca"/>

== References ==
{{Reflist|30em|refs=
<ref name="TCP-1">[http://pgnewspapers.pgpl.ca/fedora/repository/pgc:1983-05-30-05/OCR/Full%20Text%20OCR "Testimony by Lavoie brings up questions".] ''[[The Canadian Press]]''. circa 1983. Retrieved 12 January 2016.</ref>
}}

==Further reading==
* {{cite web | title=Hitman – Episodes – the fifth estate | website=[[CBC.ca]] | date=April 27, 1983 | url=http://www.cbc.ca/fifth/episodes/40-years-of-the-fifth-estate/hitman | ref={{sfnref | CBC.ca | 1983}} | accessdate=January 13, 2016}}
* Laurent, Rene (September 22, 1982). [https://news.google.com/newspapers?nid=1946&dat=19820922&id=Pn8xAAAAIBAJ&sjid=C6UFAAAAIBAJ&pg=870,288601&hl=en "Reporter 'sat in' on gang parlays"]. ''[[The Montreal Gazette]]''.
{{authority control}}
{{DEFAULTSORT:Lavoie, Donald}}
[[Category:Articles created via the Article Wizard]]
[[Category:1942 births]]
[[Category:Living people]]
[[Category:Contract killers]]
[[Category:Canadian assassins]]
[[Category:People from Saguenay, Quebec]]