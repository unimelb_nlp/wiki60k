{{Infobox journal
| title = AAPS PharmSciTech
| cover = 
| former_name = 
| abbreviation = AAPS PharmSciTech
| discipline = [[Pharmaceutical sciences]]
| editor = Robert O. Williams III
| publisher = [[Springer Science+Business Media]] on behalf of the [[American Association of Pharmaceutical Scientists]]
| country =
| history = 2000-present
| frequency = Bimonthly
| openaccess = 
| license = 
| impact = 1.954
| impact-year = 2015
| ISSN = 
| eISSN = 1530-9932
| CODEN = AAPHFZ
| JSTOR = 
| LCCN = 00215572
| OCLC = 44575164
| website = http://www.springer.com/biomed/pharmacology+%26+toxicology/journal/12249
| link1 = http://link.springer.com/journal/volumesAndIssues/12249
| link1-name = Online archive
| link2 = http://www.aaps.org/PharmSciTech/
| link2-name = Journal page at society website
}}
'''''AAPS PharmSciTech''''' is a bimonthly [[peer-reviewed]] [[scientific journal]] covering all aspects of the [[pharmaceutical sciences]]. The [[editor-in-chief]] is Robert O. Williams III ([[University of Texas at Austin]]). The journal was established in 2000 and is published by [[Springer Science+Business Media]] on behalf of the [[American Association of Pharmaceutical Scientists]].

==Abstracting and indexing==
The journal is abstracted and indexed in:
*[[Chemical Abstracts Service]]<ref name=CASSI>{{cite web |url=http://cassi.cas.org/search.jsp |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2016-04-28}}</ref>
*[[CSA (database company)|CSA databases]]
*[[Embase]]<ref name=Embase>{{cite web |url=http://www.elsevier.com/solutions/embase/coverage |title=Embase Coverage |publisher=[[Elsevier]] |work=Embase |accessdate=2016-04-28}}</ref>
*[[Index Medicus]]/[[MEDLINE]]/[[PubMed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/100960111 |title=AAPS PharmSciTech |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=2016-04-28}}</ref>
*[[ProQuest|ProQuest databases]]
*[[Science Citation Index Expanded]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2016-04-28}}</ref>
*[[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2016-04-28}}</ref>
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 1.954.<ref name=WoS>{{cite book |year=2016 |chapter=AAPS PharmSciTech |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==See also==
* [[List of pharmaceutical sciences journals]]

==References==
{{Reflist}}

==External links==
*{{Official website|http://www.springer.com/biomed/pharmacology+%26+toxicology/journal/12249}}

[[Category:English-language journals]]
[[Category:Pharmacology journals]]
[[Category:Bimonthly journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Publications established in 2000]]


{{pharmacology-journal-stub}}