{{For|the former Las Vegas, Nevada airport also known as Rockwell Field|Anderson Field (Nevada)}}
{{Infobox military structure
| name= Rockwell Field
| partof= 
| location=  North Island, [[San Diego, California]]
| image= Rockwell Field California World War I.jpg
| image_size = 300px
| caption=Rockwell Field, California in 1924, looking north.  Rockwell Field is in the foreground.  On the other side of the island is the Naval Air Station San Diego.
| pushpin_map= California
| pushpin_label=Rockwell Field
| pushpin_mark=Airplane_silhouette.svg
| pushpin_mapsize=300
| coordinates= {{Coord|32|41|51|N|117|11|51|W|name=Rockwell Field|display=inline,title}}
| type= Pilot training airfield (1912-1921)<br>Rockwell Air Depot (1922-1939)
| code= 
| ownership= 
| controlledby= [[File:USAAC Roundel 1919-1941.svg|25px]]&nbsp;[[Air Service, United States Army]]<br>[[United States Army Air Service]]<br>[[United States Army Air Corps]]
| condition= Incorporated into [[Naval Air Station, North Island]] (1939)
| built=1911
| builder= 
| used=1912–1939
| materials= 
| demolished= 
| battles= [[File:World War I War Service Streamer without inscription.png|150px]]<br>[[World War I]]
| events= 
| past_commanders= 
| garrison= 
| occupants= 
}}
'''Rockwell Field''' is a former [[United States Army Air Corps]] military airfield, located {{convert|1.1|mi|km}} northwest of the city of [[Coronado, California]] on the northern part of the Coronado Peninsula across the bay from [[San Diego]], California.

This airfield played a fundamental role in the development of United States military aviation in the period before and during [[World War I]]. Originally it was '''The Curtiss School of Aviation''', founded by [[Glenn Curtiss]].  In November 1912, the Army established a permanent flying school on the island.  It served as a major flying school during World War I, and remained active as an Army Air Corps facility after the war. The facility was transferred to the [[United States Navy]] on 31 January 1939.<ref name="Rockwell">[http://www.sandiegohistory.org/journal/v52-3/pdf/2006-3_air.pdf Forgotten Air Pioneers: The Army’s Rockwell Field at North Island]</ref>

Today, Rockwell Field forms the southeastern quadrant of what is today the [[Naval Air Station, North Island]] (NAS North Island).  The facility was added to the [[National Register of Historic Places]] in 1991.

==History==
[[File:San Diego Bay, Plan Showing Anchorages and Moorings - NARA - 295436.jpg|thumb|right|Map of San Diego Bay, featuring Rockwell Field, [[Coronado, California|Coronado]], [[National City, California|National City]], and the surrounding area.]]
The field was originally called the [[Signal Corps (United States Army)|Signal Corps]] Aviation School. It was the first [[U.S. Army]] school to provide flying training for [[military]] pilots, and North Island was the school's first permanent location. The Aviation School was officially established on North Island in 1912.

In 1910, climatic conditions, flat terrain, good beaches and protected stretches of water attracted [[Glenn H. Curtiss]], aviation pioneer and [[Wright Brothers]]' competitor, to North Island, where he founded an aviation school. At that time North Island really was an island, separated from South Coronado on the [[Silver Strand (San Diego)|Silver Strand]] peninsula by a narrow bight of water. Both North Island and South Coronado were privately owned, but North Island had not been developed. In January 1911, Curtiss signed a contract with the owner of North Island to use the land for three years for a flying school, which was established in February 1911. Curtiss invited the Army and the Navy to send officers to his new school for flying training. The Army sent three airmen to the Curtiss school in early 1911, but they were ordered to [[Texas]] before completion of their training. During the winter of 1911 to 1912, the Navy sent three pilots to the Curtiss school for flying training.

The Army's Signal Corps Aviation School relocated the [[Glenn Curtiss|Curtiss]] airplane group from its original location at [[College Park, Maryland]], to North Island during November to December 1912 instead of to [[Augusta, Georgia]], as it had the previous winter. The [[Wright Company|Wright]] group, organized as the [[1st Reconnaissance Squadron|1st Provisional Aero Squadron]], came to North Island after mobilizing in Texas in March. The Army flyers established a tent camp at the north end of North Island, and for about a year, the Signal Corps Aviation School rented airplanes and [[hangar]]s constructed for the Curtiss school. None of the buildings from this early period, constructed on the north end of the island, still exist. Existing historic and architecturally significant buildings reflect the use and development of Rockwell Field from 1918 to 1935.

On July 20, 1917, the Signal Corps Aviation School was named Rockwell Field in honor of 2nd Lt. [[Lewis C. Rockwell]], killed in the crash of [[Wright Model B]], Signal Corps ''4'', at College Park on September 28, 1912. Also in July, the [[United States Congress]] authorized the [[President of the United States|President]] to proceed with the taking of North Island for Army and Navy aviation schools. There was a needed for trained military pilots as the United States had entered World War I earlier in the year. President [[Woodrow Wilson]] signed an [[Executive order (United States)|Executive Order]] in August 1917 for condemnation of the land, which was still privately owned. The Army turned over the north end of the island to the Navy and relocated to the south end of North Island, the location of the Rockwell Field Historic District. The Navy's first occupancy of North Island occurred on September 8, 1917, but Congress did not authorize the purchase of North Island, for $6,098,333, until July 1919. The Army selected well-known [[Detroit]] industrial architect, [[Albert Kahn (architect)|Albert Kahn]], to develop a site and building designs. Permanent construction of Kahn's design began in mid-1918. During World War I, Rockwell Field provided training for many of the pilots and crews sent to [[France]]. It also was the source of men and aircraft for the [[6th Aero Squadron]], and the [[7th Aero Squadron]]s, which established the first military aviation presence in [[Hawaii]] and the [[Panama Canal Zone]], respectively.

After World War I, construction came to a complete standstill. Rockwell Field was demoted from one of the major [[United States Army Air Service]] training fields on the West Coast to an Aviation General Supply and Repair Depot in 1920 and redesignated again as Rockwell Air Intermediate Depot in 1922. By 1922 there were only 10 officers, two warrant officers, 42 enlisted men, and 190 civilians employed at the airfield.

However, the base figured in numerous historic achievements in aviation during the 1920s. Lt. [[Jimmy Doolittle]] (who had accomplished his initial flight training at Rockwell years before) landed there in September 1922 after establishing a new record for the first transcontinental flight within a single day. The first nonstop transcontinental flight, originating at [[Roosevelt Field, New York]], was accomplished by Army pilots and ended at Rockwell Field in May 1923. On June 27 of that year, pilots from Rockwell Field (Capt. [[Lowell H. Smith]] and 1st Lts. John P. Richter, Virgil Hine, and Frank W. Seifert) conducted the first complete [[aerial refueling]] between two airplanes. In the first week of 1929 the field was an operating location for another air refueling operation, in which a [[Douglas C-1]] transport performed 27 sorties refueling the modified [[Fokker C-2|Atlantic-Fokker C-2]] nicknamed the ''[[Question Mark (airplane)|Question Mark]]''. [[Charles A. Lindbergh]]'s flight from [[New York City|New York]] to [[Paris]] in May 1927 originated at Rockwell Field on North Island on May 10, 1927, when Lindbergh began the first leg of his journey.<ref>[http://www.charleslindbergh.com/history/log.asp Log of the Spirit of St. Louis]</ref>

As the Navy's emphasis began shifting from seaplanes to the land planes used on [[aircraft carrier]]s, its requirement for land increased. Eventually, agreement was reached within the War Department to grant the Navy complete control of North Island. After visiting the air station and the Army airfield on an inspection tour in October 1935, President [[Franklin Roosevelt]] issued an Executive Order transferring Rockwell Field and all of its buildings to the Navy. The Army moved most of their aircraft to [[March Field]] in [[Riverside, California]], but it took another three years to completely phase-out Army activities at North Island.

The historic and architecturally significant buildings of Rockwell Field form the southeastern quadrant of what is today the Naval Air Station, North Island ([[NAS North Island]]). The buildings were designed in the [[Mission Revival]] and [[Spanish Colonial Revival Style architecture|Spanish Colonial Revival Style]]s. The Kahn-designed Mission Revival Field Officers Quarters (later married officers quarters) are reinforced concrete-framed, in-filled with hollow [[terra cotta]] tile and finished in buff color [[stucco]]. Kahn's Mission Revival [[hangar]]s (Buildings 501, 502 and 503 from 1918) are similar in materials with red clay tile, [[gable]]d roofs. They were built to the same plan: a [[rectangle]], 135 feet by 70 feet, with 30 feet clear to the ceiling. A low, flat-roofed, lean-to on the east side of each contained offices. Located on the bluff edge at the North Island end of the Coronado-North Island causeway, the Army-Navy Gate House/Meter Room (Building 505, 1918; later Meter House) functioned as the [[gatehouse]] for both Rockwell Field and NAS San Diego. This group of buildings reflects the War Department's plan to create buildings that would be appropriate for Southern California, and illustrates Kahn's "Spanish military" design implemented at Rockwell Field.

==See also==
{{Portal|United States Air Force|Military of the United States|World War I}}
* [[National Register of Historic Places]]
* [[Naval Air Station North Island]]
* [[United States Army World War I Flight Training]]

==References==
{{Air Force Historical Research Agency}}
{{Reflist}}
{{refbegin}}
{{refend}}

==External links==
{{Commons category|Rockwell Field}}
*[[National Park Service]]'s [[National Register of Historic Places]] and Regional Offices, in partnership with [[Dayton Aviation Heritage National Historical Park]], [[United States Air Force]], [[U.S. Centennial of Flight Commission]] and the [[National Conference of State Historic Preservation Officers]].
*[http://www.nps.gov/history/nr/travel/aviation/ Aviation: From Sand Dunes to Sonic Booms, a National Park Service ''Discover Our Shared Heritage'' Travel Itinerary]

{{National Register of Historic Places}}

[[Category:Government buildings completed in 1917]]
[[Category:History of San Diego County, California]]
[[Category:National Register of Historic Places in San Diego County, California]]
[[Category:Military facilities on the National Register of Historic Places in California]]
[[Category:Closed facilities of the United States Navy]]
[[Category:Airfields of the United States Army Air Service]]
[[Category:Spanish Colonial Revival architecture in California]]
[[Category:World War I airfields]]
[[Category:World War I sites in the United States]]