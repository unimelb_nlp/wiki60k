{{about||the Spanish military officer|Félix Verdeja Bardales|the Spanish wine grape|Verdeja (grape)}}

{{Infobox Weapon
|is_vehicle=yes
|type=[[Light tank]]
|origin={{flagcountry|Spain|1938}}
| name = Verdeja 1
| image = [[Image:Verdeja front.jpg|300px]]
| caption = Verdeja 75&nbsp;mm self-propelled howitzer, based on the '''Verdeja&nbsp;1''' prototype chassis
| length = 4.498&nbsp;m (14&nbsp;ft 7&nbsp;in)
| width = 2.152&nbsp;m (7&nbsp;ft 1&nbsp;in)
| height = 1.572&nbsp;m (5&nbsp;ft 8&nbsp;in)
| weight = 6.5&nbsp;[[tonne]]s (14,300&nbsp;[[Pound (mass)|lb]])
| suspension = leaf spring
| speed = 44 km/h (27&nbsp;mph)
| vehicle_range = 220&nbsp;km (137&nbsp;mi)
| primary_armament = [[45 mm anti-tank gun M1937 (53-K)45|&nbsp;mm L/44 Mark I ''modelo 1939'']]
| secondary_armament = 2x Dreyse 7.92&nbsp;mm [[MG-13]] [[machine gun]]s
| ammunition = 72 45&nbsp;mm shells & 2,500 7.92&nbsp;mm projectiles
| armour = 7–25&nbsp;mm (0.3–1.0&nbsp;in)
| engine = [[Ford flathead V8 engine|Ford]] V-8 model 48
| crew = 3
| engine_power = 85&nbsp;[[Horsepower|hp]] (114&nbsp;[[Kilowatt|kW]])
| pw_ratio = 13.08&nbsp;hp/tonne
| total production in 1938-1954 = 4 including the original prototype, Verdeja&nbsp;1 prototype, Verdeja&nbsp;2 prototype and Verdeja 75 mm self-propelled howitzer prototype
}}
{{Spanish tanks}}

'''Verdeja''' was the name of a series of [[light tank]]s developed in [[Spain]] between 1938 and 1954 in an attempt to replace German [[Panzer I]] and Soviet [[T-26]] tanks in [[Spanish Army|Spanish service]].

The program was headed by [[major]] [[Félix Verdeja Bardales]] and led to the development of four prototype vehicles, including a [[self-propelled howitzer]] armed with a 75&nbsp;millimeter (3&nbsp;in) gun. It was designed as an advanced light tank and was one of the first development programs which took into account survivability of the crew as opposed to the protection of the tank itself.<ref>de Mazararrasa, pp. 11–12</ref> The tank was influenced by several of the light tanks which it was intended to replace, including the Panzer I and T-26, both of which were originally used during the [[Spanish Civil War]]. The Verdeja was considered a superior tank to the T-26 after a lengthy testing period, yet was never put into mass production.<ref>Armas, p. 28</ref>

Three light tank prototypes were manufactured between 1938 and 1942, including the Verdeja&nbsp;1 and the Verdeja&nbsp;2.  Interest in the vehicle's development waned after the end of the [[Second World War]].  Despite attempts to fit a new engine in the Verdeja&nbsp;2 and convert the Verdeja&nbsp;1 into a [[self-propelled artillery]] piece, ultimately the program was unofficially canceled in favor of adopting the [[United States|U.S.]] [[M47 Patton|M47 Patton Tank]] in 1954.<ref>Manrique & Molina, p. 31</ref>  A prototype of the 75&nbsp;millimetre self-propelled howitzer<ref name = "EdT">Ministerio de Defensa, ''Materiales'', retrieved on 2008-05-27</ref> and of the Verdeja&nbsp;2 were put on display in the early 1990s.<ref>Núñez, p. 85</ref>

==Development==
Spain received its first tank in mid-1919, a French [[Renault FT]], for testing purposes, and later received ten more tanks on 18 December 1921.<ref>García 2004, pp. 5–9</ref>  The use of these tanks during the [[Rif War (1920)|Rif War]], including the first amphibious landing with tanks,<ref>García 2000, pp. 49–54</ref> offered valuable experience for Spain's first indigenous armor program, the [[Trubia A4]]. The Trubia tank program, based on the FT, led to the development of four prototypes, but ultimately the program failed due to lack of interest from the national government.<ref>García 2008, pp. 54–56</ref> These prototypes influenced a subsequent indigenous attempt to produce a tank, named the Trubia-Naval.<ref>García 2008, p.64</ref> This design also failed to get past the prototype type stage.<ref>Manrique & Molina, p.9</ref> Due to the failure of Spanish efforts to produce a tank, and the ineffective attempts to procure foreign designs such as the Italian [[Fiat 3000]],<ref>de Mazararrasa 1998, pp. 74–76</ref> by the start of the Spanish Civil War there were only ten working FT light tanks available in the country.<ref>García 2004, pp. 6–7. On 24 August 1925, the Spanish government successfully petitioned to procure six more FTs, to replace those operating in Morocco since 1922=; García 2004, pp.19–21. At the start of the war there were 15 FTs, but only 10 in working condition.</ref>

The lack of armor prompted the [[Soviet Union]] to supply the [[Popular Front (Spain)|Popular Front]] and [[Germany|Nazi Germany]] and [[Italy]] to supply the [[Nationalist Spain|Nationalist Front]] with light tanks. Between 1936 and 1939, the Germans provided the Nationalists with 122 Panzer Is<ref>Manrique & Molina 2006, p.311</ref> and the Italians provided 155 [[L3/35|L-3-35s]].<ref>Manrique & Molina 2006, p.314</ref> Meanwhile, the Soviets issued Republican Spain 281 [[T-26]]s and 50 [[BT-5]]s.<ref>Manrique & Molina 2006, pp.320–321</ref> The Nationalists quickly found out  the light machine guns on their tanks could not penetrate the T-26's armor at over {{convert|150|m|yd}}, and Republican tankers could routinely knock out Panzer Is and L-3-35s at ranges of up to {{convert|1000|m|yd}}.<ref>Candil, p.36</ref> In order to re-equip Nationalist armored forces with the T-26, German Major [[Ritter von Thoma]] offered Spanish troops 500&nbsp;[[Spanish peseta|''peseta'']]s for each tank captured.<ref>Perrett, p.35</ref> There were also attempts to up-gun the Panzer I with an Italian [[Breda]] 20&nbsp;millimetre Model 1935 anti-aircraft gun, due to its high [[Muzzle velocity]] and low recoil. Despite four successfully converted vehicles, designated Panzer I ''Breda'', there was no widespread program to retrofit the gun into the Panzer I.<ref>Molina 2005, pp. 47–50</ref> Instead, the Nationalists began to press captured T-26s into service against their previous owners, with the first Nationalist T-26 unit formed in June 1937.<ref>Molina 2007, p. 16</ref>

On 6 September 1937, Captain Félix Verdeja, commanding the maintenance company of the Nationalist ''Batallón de Carros de Combate'' ("Tank Battalion"), began to privately develop a new light tank. His position, with direct access to Panzer Is and T-26s, gave Verdeja direct evidence of the shortcomings of current tank models in terms of combat ability and maintenance issues. Verdeja established a future tank requiring the 45&nbsp;millimetre (1.77&nbsp;in) gun fitted in the T-26; two coaxial light machine guns; a low profile, all-around armor greater than 15&nbsp;millimetres (0.6&nbsp;in), with a turret [[mantlet]] plate of at least 30&nbsp;millimetres (1.2&nbsp;in); road speed of {{convert|70|km/h|mph}}, combat range of {{convert|200|km|mi}}, and a capable suspension and new track system. This latter requirement was based on experiences with existing light tanks, which frequently lost their tracks in combat. These requirements and solutions were presented in October 1938 to Colonel Díaz de la Lastra, commanding officer of the ''Agrupación de Carros de Combate''. Although the project was approved, the program had to use scrap to build the first prototype due to a lack of resources and money.<ref>de Mazararrasa, pp.11–14</ref> Despite early obstacles, including criticism from von Thoma, the program continued and Verdeja was awarded a warehouse in [[Zaragoza]] to continue with the construction of the prototype.&nbsp;<ref>de Mazararrasa, pp. 14–16</ref>

[[Image:T26 right side.jpg|right|thumb|The Verdeja was heavily influenced by the Soviet [[T-26]]]]

The prototype was manufactured from spare parts and equipment scavenged from other light tanks, and featured a rectangular turret with 16&nbsp;millimetre (0.6&nbsp;in) basic armor. The chassis was divided into four quarters, with the forward right half occupied by the engine, gear box, clutch and final drive, beside the driver. The rear half of the vehicle was taken up mostly by the turret basket and forty-six 45&nbsp;millimetre rounds, as well as two 60 liter (13 Imp gal) fuel tanks.<ref>de Mazararrasa, p.20</ref> Turret space was used by the tank commander-gunner and the loader, as well as the 45&nbsp;millimetre model 1932 anti-tank gun and two [[MG-13]] machine guns. The main gun was originally commissioned as the Soviet 45&nbsp;millimetre 19K anti-tank gun in March 1932, and featured heavier ammunition and a faster rate of fire than older anti-tank guns. Starting in 1934, a newer model began to be fitted into newly assembled T-26s.<ref>Baryatinskiy, p.28</ref> Apart from the gun, the tank commander's model 1932 panoramic periscope was also scavenged from a T-26. The vehicle was powered by a [[Ford]] [[Ford Flathead engine#221|Model 48]] engine taken from a civilian automobile, displacing 3,622&nbsp;cc (221 in<sup>3</sup>) and producing 85&nbsp;hp (63&nbsp;kW) at 2,000&nbsp;rpm. The engine was paired with a brand-new radiator and exhaust system. The Verdeja prototype used the Panzer I's Aphon PG-31 gearbox, although this worked at excessive revolutions for the engine, offering less torque which made slopes greater than 40° difficult. Possibly the most unusual features of the Verdeja were the suspension and tracks.  To prevent the tank's tracks slipping off the roadwheels, two track pieces were fitted together to create a central groove for the roadwheel to travel in.<ref>de Mazarrasa, pp. 20–25</ref> With a weight of under {{convert|5|t|ST}} the Verdeja had a maximum speed of {{convert|70|km/h|mph}} and a combat radius of {{convert|120|km|mi}}.<ref>de Mazarrasa, p. 44.</ref> Following the prototype's success in testing between 10 January and 20 January 1939, Captain Verdeja was ordered to begin construction of the definitive model of the light tank.

===Verdeja 1===
The appearance of the resulting Verdeja 1 prototype was close to that originally envisioned in Captain Verdeja's first designs.  The vehicle's hull was elongated and the rear plate sloped, while the fuel capacity—and thus combat range—was increased, as was the ammunition capacity and the thickness of the armor.  The vehicle was fabricated in [[Bilbao]], the only city in Spain with a heavy vehicle assembly line.  Due to the end of the Spanish Civil War and a shortage of funds, construction was postponed until May&nbsp;1940.  The prototype was completed three months later and delivered to the proving grounds in Carabanchel, Madrid, powered with basically the same [[Ford flathead V8 engine]] also used as the motive power of the three-tonne weight British [[Universal Carrier]].<ref>de Mazarrasa 1994, pp. 33–36</ref> A major external difference between the previous model and this prototype was the new, low-profile turret which allowed the 45 millimetre gun to depress and elevate from 8° to 70°.  The original 45&nbsp;millimetre model 1932 gun was exchanged with a new 45&nbsp;millimetre Mark I tank gun fabricated by S.A. Placencia de las Armas, in Spain.  However, the new prototype adopted the suspension and tracks from the original prototype.  In essence, the main advantages of the new prototype were its low-profile, high elevation of the main gun and the increased sloping of the armor from 12° to 45°.<ref>de Mazarrasa, pp. 49—54</ref>  It should be noted that the Verdeja 1 retained the original configuration by placing the engine in the front, to increase crew survivability.<ref>Jedsite, ''de Mazarrasa, pp. 11–12</ref>

On arrival at Carabanchel, the vehicle was tested against the T-26 in mobility over different terrain types and in firepower.  The vehicles were graded based upon a five-point scale for each test, which would be multiplied by a coefficient of importance for each test.<ref>Armas, p. 33</ref>  During the testing the Verdeja traveled for some 500&nbsp;kilometres (300&nbsp;mi) without any maintenance problems, the only issue being the large consumption of water by the gasoline engine, due to the lack of an efficient radiator, and the loss of a rubber liner of one of the roadwheels.  It was found that the maximum velocity of the Verdeja was either on par with similar vehicles in foreign service or superior, while the Verdeja proved itself capable of going over trenches almost 2&nbsp;m wide and climbing slopes of 40°.  In terms of armament, it was proved that the vehicle could withstand the recoil of the 45&nbsp;millimetre high-velocity tank gun.  One of the vehicle's disadvantages was that the tank commander's aiming device was designed for a 37&nbsp;millimetre anti-tank cannon, adapted into the Verdeja due to the lack of time to manufacture one for the 45&nbsp;millimetre Mark I.  Testing concluded with the Verdeja receiving a total of 243 points, compared to the 205 points awarded to the T-26B.<ref>de Mazarrasa, pp. 43–46</ref>  Testing completed, the prototype was returned and several problems were fixed, including engine deficiencies, the elevation of the sprocket and an increase to 10&nbsp;millimetre of armor on all areas that had less.  These changes made, the Verdeja returned to testing, this time scoring 261.98 points.<ref>de Mazarrasa, pp. 45–48</ref>

[[Image:Ausf A Front gray.jpg|right|thumb|The [[Panzer I]] heavily influenced the Verdeja's turret design]]

Plans to produce one thousand Verdeja tanks were approved on 2 December 1940, divided into ten batches of one hundred tanks each. The Verdeja production prototype was to adopt the 120&nbsp;horsepower (89&nbsp;kW) [[Lincoln-Zephyr]] gasoline [[Lincoln-Zephyr V12 engine|V12]] engine, requiring a contract between the Spanish government and Ford Motor Ibérica, Ford's Spanish subsidiary. Simultaneously, in case of failure of talks between Ford and Spain, the government also began to contact a number of German companies, including [[Maybach]]. In order to begin production, the Tank Workshop in Zaragoza was to be expanded to allow final assembly of at least five tanks per month. Despite funding and two years of construction allotted, the factory construction and expansion was never completed. Other problems arose, including the failure to reach an agreement with Ford or Maybach. These factors, the poor economic situation in Spain, the lack of clients other than the [[Spanish Army]] and the lack of incentives for Spanish companies to partake in the construction program, led to the abandonment of the attempt to fabricate the Verdeja&nbsp;1. Another attempt was undertaken at contracting the ADESA (''Armamento de Aviación, S.A.'') company, to manufacture two Verdeja light tanks for experimental purposes. Despite the failure to procure an engine, ADESA offered to construct 300 units, but these attempts failed and the program was abandoned by 1941.<ref>For information on the production of the Verdeja&nbsp;1, ''see:'' de Mazarrasa, pp. 57—64</ref>

===Verdeja 2===
As the Verdeja&nbsp;1 program dissolved, Captain Verdeja began to design a successor taking into consideration lessons learned during the opening campaigns of the Second World War.  The new design featured a redesigned engine bay at the rear of the chassis, which meant moving the drive sprocket to the rear as well.  The movement of the engine's location allowed for better cooling of the vehicle's motor and the fighting compartment, as well as allowing the turret to be moved forward.  The vehicle's armor was also increased substantially by between five and ten millimetres.<ref>de Mazarrasa 1994, p. 39</ref>  This new tank was not approved for production or further development due to continued postponement of the production of the Verdeja&nbsp;1 for reasons which included offers by the German government to supply the Panzer IV's engine for the Verdeja&nbsp;1.  Although production of the new vehicle finally began in 1942, it was not until August 1944 that the Verdeja&nbsp;2 prototype was delivered.<ref>de Mazarrasa, p. 67</ref>  The program was delayed by the incorporation of twenty [[Panzer IV#Ausf. G to Ausf. J|Panzer IV Ausf. H's]] and ten [[Sturmgeschütz III]]s into the Spanish Army in late 1943,<ref>Doyle & Jentz, p. 41</ref> as well as failed attempts to procure one hundred more Panzer IVs and even [[Panther tank|Panthers]] and [[Tiger I|Tigers]] during 1944.<ref>Caballero & Molina, pp. 75–82</ref>  With these new vehicles integrated into the army and the fiscal problems which plagued the Verdeja&nbsp;1, the Verdeja&nbsp;2 remained unimproved until 1950, when there was an attempt to fit a [[Pegaso]] Z-202 engine.<ref>de Mazarrasa 1994, p. 40</ref>  Despite this, the Verdeja remained on factory grounds until 1973, when it was transferred to the Infantry Academy of [[Toledo, Spain|Toledo]].<ref>de Mazarrasa, pp. 69–70</ref>

===Comparative data===
{| class="wikitable" border=0 cellspacing=0 cellpadding=3 style="border-top:3px; border-collapse:collapse; text-align:left;;" summary="Characteristics of the Verdejas"
|+ '''The Verdeja series compared to the T-26 and Panzer I'''
|- style="vertical-align:bottom; border-bottom:1px solid #999;"
!
! style="text-align:left;" | Verdeja 1
! style="text-align:left;" | Verdeja 2
! style="text-align:left;" | Verdeja 75&nbsp;mm
! style="text-align:left;" | T-26B<ref>Baryatinskiy, p.96</ref>
! style="text-align:left;" | Panzer I Ausf. B<ref>Franco, p.613</ref>
|-
! style="text-align:right;" | Weight
| 6.5 [[Tonne|t]] (7.1&nbsp;[[short ton]]s)
| 10.9&nbsp;t (12.0&nbsp;tons)
| 6.5&nbsp;t (7.2&nbsp;tons)
| 9.4&nbsp;t (10.4&nbsp;tons)
| 5.4&nbsp;t (6.0&nbsp;tons)
|-
! style="text-align:right;" | Gun
| 45&nbsp;mm cannon (1.77&nbsp;inches)
| 45&nbsp;mm cannon
| 75&nbsp;mm howitzer (2.95&nbsp;in)
| 45&nbsp;mm cannon
| 7.92&nbsp;mm machine gun (0.312&nbsp;in)
|-
! style="text-align:right;" | Ammunition
| 72 rounds
| 146 rounds
| 32 rounds
| 122 rounds
| 2,250 rounds
|-
! style="text-align:right;" | Road&nbsp;range
| 220&nbsp;km (136&nbsp;mi)
| 220&nbsp;km (136&nbsp;mi)
| 220&nbsp;km (136&nbsp;mi)
| 175&nbsp;km (109&nbsp;mi)
| 200&nbsp;km (124&nbsp;mi)
|-
! style="text-align:right;" | Maximum speed
| 44&nbsp;km/h (28&nbsp;mph)
| 46&nbsp;km/h (29&nbsp;mph)
| 44&nbsp;km/h (27&nbsp;mph)
| 31.1&nbsp;km/h (19.3&nbsp;mph)
| 50&nbsp;km/h (31&nbsp;mph)
|-
! style="text-align:right;" | Armor
| 7–25&nbsp;mm (0.3–1.0&nbsp;in)
| 10–40&nbsp;mm (0.4–1.6&nbsp;in)
| 7–25&nbsp;mm (0.3–1.0&nbsp;in)
| 7–16&nbsp;mm (0.3–0.6&nbsp;in)
| 7–13&nbsp;mm (0.2–0.5&nbsp;in)
|}

==Self-propelled howitzer==
Between the late 1940s and early 1950s there were a number of programs in Spain to develop a self-propelled [[howitzer]] based on an existing chassis.  For example, during the early 1950s, Spanish engineers attempted to retrofit a R-43 105-millimeter (4.1&nbsp;in) [[caliber#Caliber as measurement of length|L/26]] howitzer into a [[Sturmgeschütz III|StuG III]]. This required reconstruction of the turret's casemate, in a fashion similar to the Verdeja 75-millimeter self-propelled howitzer. Although one vehicle began conversion, the program was never finalized. There were similar programs to fit an 88-millimeter L/56 and a 122-millimeter L/46 howitzer thereafter, but these did not advance beyond the planning stage, either.<ref>de Mazarrasa 1994, pp. 131–132</ref> One of the most successful programs was the attempt to produce a 75-millimeter self-propelled howitzer based on the chassis of the Verdeja&nbsp;1 prototype. Beginning in 1945,&nbsp;<ref>Ministerio de Defensa, ''Exposición de Unidades Acorazadas'', retrieved on 2008-05-27</ref> now-Major Verdeja was ordered to begin designing this piece using a rapid-firing 75-millimeter L/40 howitzer designed by [[Navantia|''Sociedad Española de Construcción Naval'']].<ref>de Mazarrasa 1994, p.59</ref> The availability of the required parts and the lack of complicated changes meant that the vehicle was quickly prepared and tested extensively. The fate of the self-propelled piece was much the same as that of the Verdeja&nbsp;2, and the vehicle was left untouched at the proving grounds in Carabanchel until 1973, when it was moved to the Spanish base ''Alfonso XIII'', housing the then Mechanized Infantry Regiment ''Wad Rass&nbsp;nº&nbsp;55''. It was soon moved to another base, and finally delivered to the base of ''El Goloso'', outside of Madrid, as a part of an armored vehicles museum.<ref>de Mazarrasa, pp. 78–79</ref>

[[Image:75mm Verdeja rear gun.jpg|right|thumb|The Verdeja 75&nbsp;mm Self-Propelled Howitzer, with the gun system visible]]

Major changes to the original Verdeja 1 included removing the turret and replacing it with a gun shield with 10&nbsp;millimetre thick steel armor. This meant that much of the chassis' roof and rear wall was eliminated. The howitzer was designed as a monoblock steel tube, using a double-baffle muzzle brake, with twelve twists completing a full turn every forty [[caliber]]s. As mounted, the howitzer could fire between 0.5° and 25°, and move 4.5° either left or right. The crew could stow eight rounds of ammunition in a ready-round stowage area near the walls of the gun shield on each side of the breech, allowing easy access to projectiles. Otherwise, the vehicle could store another 24 rounds in an auxiliary carriage. The carriage was based on the axles and wheels of a [[3.7 cm PaK 36|PaK 36]] [[Anti-tank warfare#Anti-tank guns|anti-tank gun]]. A unique feature of this prototype was a mechanical [[brake]] built into the [[idler-wheel]] to the rear of the chassis, guaranteeing the vehicle's stability when firing and avoiding damage to the transmission.<ref>de Mazarrasa, pp. 79–82</ref>

==Conclusions==
Ultimately the Verdeja program's end came with the arrival of military equipment from the United States, beginning in 1953. From 1954, the Spanish Army received 389 M47 Patton Tanks, replacing the T-26s, Panzer Is and Panzer IVs then in service.<ref>Zaloga 1999, pp. 36–37</ref> The Verdeja had become completely obsolete when compared to larger, more potent tanks such as the German [[Panther tank|Panther]], the Soviet [[T-54]] and the US M47. The T-54 had 200&nbsp;millimetre of steel armor on the turret mantlet, far greater than the Verdeja 2's maximum armor thickness of 40&nbsp;milimetres.<ref>Zaloga 2004</ref> The Soviet 45&nbsp;millimetre model 1932 gun was replaced by the [[T-34|T-34's]] [[F-34 tank gun|76.2&nbsp;millimetre gun]], while the Germans adopted the [[7.5 cm KwK 42|75&nbsp;millimetre L/70 tank gun]] on the Panther. By 1950, Soviet tanks such as the T-54 were armed with the [[D-10 tank gun|D-10T 100&nbsp;millimetre tank gun]], and American tanks adopted the [[90 mm gun|90&nbsp;millimetre main gun]]. Although the Verdeja was Spain's most successful indigenous design,<ref>de Mazarrasa, p. 10</ref> it was outclassed as foreign countries produced superior products. Furthermore, the need for self-propelled artillery was soon eliminated as the United States offered Spain [[M24 Chaffee#Variants|M37]] and M44 self-propelled howitzers.<ref>Manrique & Molina, p. 36</ref> As a result, interest in the Verdeja dried up after 1954. Spain would not attempt another indigenous tank until the advent of the [[Lince (tank)|Lince]] main battle tank in the late 1980s.

==Notes==
{{Reflist|30em}}

==References==
{{Portal|Tank}}
{{Refbegin|30em}}
{{cite web
 | title = Exposicíon de Materiales Acorazadas
 | publisher = Ministerio de Defensa (Spain)
 | url = http://www.ejercito.mde.es/materiales/acorazadas/index.html
 | accessdate = 2008-05-27
 | language = Spanish
 | archiveurl = https://web.archive.org/web/20080411161016/http://www.ejercito.mde.es/materiales/acorazadas/index.html <!--Added by H3llBot-->
 | archivedate = 2008-04-11 |ref= EdT}}
* {{cite journal
 | title = Verdeja vs. Vickers T-26B
 | journal = Armas
 | issue = 281
 | pages = 28–34
 | publisher = MC Ediciones
 | location = Barcelona, Spain
 |date=November 2005
 | language = Spanish
}}
* {{cite book
 | last = Baryatinskiy
 | first = Mikhail
 | title = Light Tanks: T-27, T-38, BT, T-26, T-40, T-50, T-60, T-70
 | publisher = Ian Allan
 | location = London, United Kingdom
 | pages = 96
 | isbn = 978-0-7110-3163-0
 | year = 2006}}
* {{cite book
 | last = Caballero
 | first = Carlos
 |author2=Lucas Molina
  | title = Panzer IV: El puño de la Wehrmacht
 | publisher = AF Editores
 |date=October 2006
 | location = Valladolid, Spain
 | pages = 96
 | isbn = 84-96016-81-1
 | language = Spanish}}
* {{cite journal
 | last = Candil
 | first = Antonio J.
 | title = Aid Mission to the Republicans Tested Doctrine and Equipment
 | journal = ARMOR
 | pages = 49–54
 | publisher = Army Armor Center
 | location = Fort Knox, Kentucky
 | date = March 1, 1999}}
* {{cite book
 | last = de Mazarrasa
 | first = Javier
 | title = Blindados en España 2ª Parte: La Dificil Postguerra 1939–1960
 | publisher = Quirón Ediciones
 |date=May 1994
 | location = Valladolid, Spain
 | pages = 184
 | isbn = 84-87314-10-4
 | language = Spanish}}
* {{cite book
 | last = de Mazarrasa
 | first = Javier
 | title = Carro de Combate Verdeja
 | publisher = L. Carbonell
 | location = Barcelona, Spain
 | pages = 83
 | isbn = 84-86749-02-6
 | language = Spanish}}
* {{cite book
 | last = de Mazarrasa
 | first = Javier
 | title = Los Carros de Combate en la Guerra de España 1936–1939 (Vol. 1º)
 | publisher = Quirón Ediciones
 |date=June 1998
 | location = Valladolid, Spain
 | pages = 160
 | isbn = 84-87314-37-6
 | language = Spanish}}
* {{cite book
 | last = Doyle
 | first = Hilary
 |author2=Tom Jentz
  | title = Panzerkampfwagen IV Ausf. G, H and J 1942–45
 | publisher = Osprey
 | year = 2001
 | location = London, United Kingdom
 | pages = 48
 | isbn = 1-84176-183-4}}
*{{cite book |last= Franco |first= Lucas Molina |title= Panzer I: El inicio de una saga |year= 2005 |publisher= AF Editores |location= Madrid, Spain |language= Spanish |isbn = 84-96016-52-8}}
* {{cite journal
  | last = García
  | first = Dionisío
  | title = Alhucemas, el desembarco: Una mirada retrospectiva en el 75 aniversario
  | journal = Serga
  | issue = 7
  | pages = 49–54
  | publisher = Almena
  | location = Madrid, Spain
  |date=September 2000
  | language = Spanish}}
* {{cite journal
  | last = García
  | first = Dionisío
  | title = Trubia: El Primer Carro de Combate Español
  | journal = Serga
  | issue = 52
  | pages = 54–64
  | publisher = Almena
  | location = Madrid, Spain
  |date=March 2008
  | language = Spanish}}
* {{cite journal
  | last = García
  | first = Dionisío
  | title = Renault FT 17 en España (1): La Guerra de Marruecos
  | journal = Serga
  | issue = 30
  | pages = 2–24
  | publisher = Almena
  | location = Madrid, Spain
  |date=July 2004
  | language = Spanish}}
* {{cite journal
  | last = García
  | first = Dionisío
  | title = Renault FT 17 en España (2): La Guerra Civil
  | journal = Serga
  | issue = 31
  | pages = 2–15
  | publisher = Almena
  | location = Madrid, Spain
  |date=September 2004
  | language = Spanish}}
* <cite id = Manriquemolina>{{cite book
  | last = Manrique
  | first = José María
  |author2=Lucas Molina
   | title = La Brunete: 1ª Parte
  | publisher = Quirón Ediciones
  | location = Valladolid, Spain
  | pages = 80
  | isbn = 84-96016-27-7
  | language = Spanish}} </cite>
* {{cite book
|last= Manrique
|first= José María
|author2=Lucas Molina Franco
 |title= Las Armas de la Guerra Civil Española
|year= 2006 |publisher= La Esfera de los Libros
|location= 28002 Madrid
|pages= 613
|isbn = 84-9734-475-8
  | language = Spanish}}
* {{cite book
  | last = Molina
  | first = Lucas
  | authorlink = 
  | title = Blindados Soviéticos en el Ejército de Franco
  | publisher = Galland Books
  |date=December 2007
  | location = Spain
  | pages = 48
  | isbn = 978-84-612-1221-7 
  | language = Spanish}}
* {{cite book
  | last = Molina
  | first = Lucas
  | title = Panzer I: El incidio de una saga
  | publisher = AF Editores
  |date=May 2005
  | location = Valladolid, Spain
  | pages = 64
  | isbn = 84-96016-51-X
  | language = Spanish}}
* {{cite journal
  | last = Núñez
  | first = Jesús
  | title = Museo de la Academia de Infantería
  | journal = Armas
  | issue = 310
  | pages = 80–86
  | publisher = MC Ediciones
  | location = Barcelona, Spain
  |date=April 2008
  | language = Spanish}}
* {{cite book
  | last = Zaloga
  | first = Steven
  | title = The M47 and M48 Patton Tanks
  | publisher = Osprey
  | year = 1999
  | location = London, United Kingdom
  | pages = 48
  | isbn = 1-85532-825-9}}
* {{cite book
  | last = Zaloga
  | first = Steven
  | title = T-54 and T-55 Main Battle Tanks 1944–2004
  | publisher = Osprey
  | year = 2004
  | location = London, United Kingdom
  | pages = 48
  | isbn = 1-84176-792-1}}
{{refend}}

{{featured article}}

[[Category:World War II light tanks]]
[[Category:Light tanks of Spain]]
[[Category:Light tanks of the Cold War]]
[[Category:Light tanks of the interwar period]]