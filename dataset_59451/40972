{{infobox weapon
| name               = HDT Global Storm SRTV
| image              = 
| caption            = 
| origin             = [[United States]]
| type               = Special Operations Vehicle
<!-- Type selection -->
| is_vehicle         = yes
<!-- Service history -->
| service            = 
| used_by            = United States, [[United States Air Force Pararescue]] <!-- Do not use flag icons here, per MOS:INFOBOXFLAG -->
| wars               = 
<!-- Production history -->
| designer           = 
| design_date        = 
| manufacturer       = HDT Global Inc. formerly '''H'''unter '''D'''efence '''T'''echnologies Inc.
| unit_cost          = 
| production_date    = August 2013 (test vehicle) {{ubl |[[Geneva, Ohio]] ([[HDT Global]])}}
| number             = 
| variants           =
<!-- General specifications -->
| spec_label         = 
| weight             = {{convert|4320|lb|kg|0|abbr=on|disp=flip}}, {{convert|8050|lb|kg|0|abbr=on|disp=flip}} ([[Gross vehicle weight rating|GVWR]])
| length             = {{convert|13|in|mm|0|abbr=on|disp=flip}}
| part_length        = 
| width              = {{convert|80|in|mm|0|abbr=on|disp=flip}}
| height             = {{convert|66|in|mm|0|abbr=on|disp=flip}}
| crew               = 6 ([[United States Air Force Combat Rescue Officer|Combat Rescue Officer]] (CRO), [[United States Air Force Pararescue|Pararescue]] (PJ), [[Survival, Evasion, Resistance and Escape|SERE]] careerfields)
| passengers         = 3 patients/survivors
<!-- Vehicle/missile specifications -->
| armour             = 
| primary_armament     = 
| secondary_armament   = 
| engine             = {{ubl | unknown ([[gasoline]], [[octane rating|87 Octane or higher]], [[Diesel fuel|diesel]], [[JP-8]]) }}
| engine_power       = 430 hp
| pw_ratio           = 
| transmission       = 
| payload_capacity   = {{convert|3730|lb|kg|0|abbr=on|disp=flip}}
| suspension         = [[long travel suspension]]
| clearance          = {{convert|18|in|mm|0|abbr=on|disp=flip}}
| fuel_capacity      = 
| vehicle_range      = {{convert|350|mi|km|0|abbr=on|disp=flip}}
| speed              = {{convert|100|mph|km/h|0|abbr=on|disp=flip}}
| guidance           = 
| steering           = 
}}

The '''Storm Search and Rescue Tactical Vehicle (SRTV)''' is an all-terrain light military vehicle developed by the [[United States]]. It was the winner of the '''Guardian Angel Air-Deployable Rescue Vehicle (GAARV)''' competition awarded by the [[Air Force Life Cycle Management Center]] (AFLCMC). The competition was named for the pararescuemen and combat rescue officers known as the "Guardian Angel Weapon System." The Storm SRTV is to be used by the [[United States Air Force Pararescue]].

== History ==

In 2004, BC Customs (BCC) designed the SRTV-3 for USAF Guardian Angel Forces and fielded it in 2007. The SRTV was included in the [[Air National Guard]]'s (ANG) FY09 Weapons Systems Modernization Requirements documents specifically for Guardian Angel use. BCC worked with the ANG on Airdrop and multiple MDS (aircraft type) ITV certifications while awaiting the [[Air Combat Command]] (ACC) GAARV Solicitation process.<ref name=BCC>[http://soldiersystems.net/2011/08/01/srtv-from-bc-customs/ "SRTV from BC Customs"] Soldier Systems, August 1st, 2011.</ref>
In February 2012, the [[Aeronautical Systems Center]] (ASC) solicited proposals for a Guardian Angel Air Rescue Vehicle (GAARV). The purpose was to acquire an asset for air-deployable, surface rescue platform capable of maneuvering over adverse terrain in order to search for and recover Isolated Personnel (IP) and/or equipment, while also providing the capability of transporting the rescue team and the IP from an area of high threat to a defendable location for recovery by aircraft or self recovery to the final destination.<ref>[https://www.fbo.gov/index?s=opportunity&mode=form&id=a3972d9ba28b8fad46a06b4955e920b1 "Guardian Angel Air Rescue Vehicle"] Federal Business Opportunities, 08 February 2012.</ref>

On September 25, 2012, HDT showed the Storm SRTV for the first time at the Modern Day Marine show. It addressed the requirements of the [[Air Force National Guard]] (ANG) Combat Search and Rescue (CSAR) units, and [[Air Force Special Operations Command|Special Operations Command]] (AFSOC). The SRTV can carry a tactical special team of six, plus three passengers on litters<ref name=DefUpdt20120925>[http://defense-update.com/20120925_storm_srtv.html "HDT Presents the Storm SRTV for Special Ops"] Defense Update, September 25, 2012.</ref> or four crew members and four patients.<ref name=AFT20131002>[http://www.airforcetimes.com/article/20131002/NEWS04/310020008/Combat-rescue-vehicle-medical-evacuations-unveiled "Combat rescue vehicle for medical evacuations unveiled"] Air Force Times, October 2nd, 2013.</ref> A team is composed of [[United States Air Force Combat Rescue Officer|Combat Rescue Officer]] (CRO), [[United States Air Force Pararescue|Pararescue]] (PJ), and [[Survival, Evasion, Resistance and Escape]] (SERE) careerfields.<ref name=SoldierSystem20130119>[http://soldiersystems.net/2013/01/19/usaf-selects-storm-srtv-gaarv/ "USAF Selects Storm SRTV as GAARV"] Soldier Systems, January 19, 2013.</ref> On January 17, 2013, the USAF announced the selection of the HDT Storm SRTV as the GAARV.<ref name=SoldierSystem20130119/> The Air Force Life Cycle Management Center (AFLCMC) awarded the company's Expeditionary Systems Group the Guardian Angel Air-Deployable Rescue Vehicle (GAARV) contract to produce the HDT Storm Search and Rescue Tactical Vehicles (SRTV).<ref>[http://www.reuters.com/article/2013/02/01/oh-hdtglobal-contract-idUSnPnFL52380+160+PRN20130201 "HDT Global Awarded Guardian Angel Air-Deployable Rescue Vehicle (GAARV) Contract For the HDT Storm(TM) SRTV"] Reuters, February 1st, 2013.</ref><ref>[http://www.terradaily.com/reports/HDT_Global_Awarded_Guardian_Angel_Air_Deployable_Rescue_Vehicle_Contract_999.html "HDT Global Awarded Guardian Angel Air-Deployable Rescue Vehicle Contract"] Terra Daily, February 6th, 2013.</ref>

On August 28, 2013, the first production vehicle was formally unveiled in Geneva, Ohio. The first two vehicles had started safety certification testing.<ref name=AFT20131002/> On November 18, 2013, the USAF's 88th Test and Evaluations Squadron (TES) in [[Nellis Air Force Base]], Nevada took delivery of the initial set of the GAARV for operational testing. Tests were scheduled to commence in March 2014 and will validate the vehicle's suitability and effectiveness for the Guardian Angel (GA) weapon system. The 88th TES is focused on maximising [[HH-60 Pave Hawk]], [[HC-130 Hercules]], and the GA weapon systems capability.<ref>[http://www.airforce-technology.com/news/newsusaf-receives-initial-guardian-angel-air-droppable-rescue-vehicles "USAF receives initial Guardian Angel air-droppable rescue vehicles"] Air Force Technology, November 18th, 2013.</ref>

== Mission ==

Guardian Angel is an USAF non-aircraft weapon system program designed to execute USAF [[Combat Search and Rescue]] (CSAR) and Personnel Recovery (PR) across the full spectrum of military operations.<ref name=DefUpdt20120925/> Currently, Guardian Angels can reach casualties by parachute or helicopter. The new vehicle enables the team to be dropped off well outside the range of the enemy’s anti-aircraft weapons to infiltrate, exfiltrate, and recover downed aircrew and personnel. It will expand the radius of operation of air-assets.<ref name=DefUpdt20120925/>

The vehicle would make the Guardian Angels susceptible to a different style of [[Search and Rescue]] (SAR) traps, where an enemy would try to purposely draw in rescue personnel. They may set up a different kind of trap with [[improvised explosive device]]s (IEDs) or vehicle-borne IEDs. Operating from a vehicle offers more avenues to get to a victim and get out, as well as transport all the gear necessary.<ref name=AFT20131002/>

== Design ==

The Storm SRTV is based on an original design by BC Customs (BCC) of Utah, originally submitted for the [[Ground Mobility Vehicle (Humvee variant)|Ground Mobility Vehicle System (GMV 1.1)]] competition hosted by [[United States Special Operations Command|SOCOM]]. It is in the tactical family of SXOR Mobility Vehicles of SXOR Motorsports.<ref name=BCC/><ref name=DefUpdt20120925/> It is a highly mobile 2-door [[off-roading]] [[pickup truck]] design with [[four-wheel drive|all-wheel-drive]]. The light-weight tactical vehicle has a high performance [[Mid-engine design|mid-engine]] for maximum stability<ref>[http://www.hdtglobal.com/products/specialty-vehicles/hdt-storm/ "HDT Storm™ SRTV"] HDT Global, 2013</ref> with 430 horsepower and 425&nbsp;lb-ft of torque. The SRTV can exceed double its curb weight in payload while performing missions.<ref name=DefUpdt20120925/> The engine will run using standard gasoline, 87 Octane or higher, diesel, or JP-8.<ref name=SoldierSystem20130119/> The SRTV is constructed of MIL-STD aircraft tubing, minimizing weight, maximizing strength, and providing a skeletal base for multiple types of [[commercial off-the-shelf]] (COTS) ballistic and blast armoring. The vehicle utilizes identical components with multiple body configurations, hence, it provides multi-role configurations.<ref name=BCC/>

The SRTV is measured to be carried internally by the [[Lockheed HC-130|M/HC-130P/N/J]], [[Lockheed C-130 Hercules|C-130]]/[[Lockheed Martin C-130J Super Hercules|C-130J]], [[Lockheed Martin KC-130|KC-130J]] and [[Boeing C-17 Globemaster III|C-17]] fixed-wing aircraft as well as the [[Boeing CH-47 Chinook|CH-47]] and [[Sikorsky CH-53 Sea Stallion|CH-53]] helicopters.<ref name=HDTBrochure>[http://www.hdtglobal.com/site_media/uploads/files/products/data_sheets/type/HDT_Storm_Vehicle_11.pdf "HDT Storm™ SRTV Light Tactical All-Terrain Vehicle"] HDT Global Brochure version 1.1, 2014</ref> It is too wide for the [[Bell Boeing V-22 Osprey|V-22]]. The V-22 compatible version can carry a team of three with two patients on litters in the roll protected cage. The vehicle can be delivered through various methods including [[Airdrop|Low Velocity Aerial Delivery]] (LAPS) or via [[Joint Precision Airdrop System]] (JPADS), guided parachutes, methods.<ref name=DefUpdt20120925/>

The SRTV has a range of up to 350 miles, and can operate in difficult environmental conditions, and including at 10,000 ft (3,048 m) above mean sea level. The vehicle can travel at a speed over 100&nbsp;mph and climb vertical grades over 70 degrees, move on side slopes angles of over 60 degrees and climb vertical steps over three feet. It can turn on a 20' (6.1 m) diameter curb to curb. Maneuverability is enhanced with cutting brakes allowing U-turns on steep hills within the length and width of the vehicle, also allowing high speed J-turns. The vehicle provides rollover safety for all occupants without limiting mission operational performance. Storm can carry up to three liters in side the rollover protection frame, without modifications, while still maintaining 360° of weapon field of fire.<ref name=HDTBrochure/>

== Weapons ==

There are weapons mount options: Fore and aft crew-served weapon mounts, 360 degree turret with roll protection, and standard swing arm mounts. BCC has been working with [[Garwood Industries]] (GI) to integrate their [[Minigun|M134 Minigun]], [[FN Herstal]] to integrate their light Weapon Stations, and [[Military Systems Group]] (MSG) for multiple weapon stations.<ref name=BCC/>

== Variants ==

The BCC SRTV-5, nicknamed the “Warthog”, has an internal crew of three with options for up to two additional crew and two internal (roll protected) patients. BCC had fielded an SRTV-5 variant to the US Border Patrol special operations group BORSTAR.<ref name=BCC/>

The BCC SRTV-22 (Osprey) variant has an internal crew of three with options for two additional crew or two (roll protected) patients, but loses either one crew member or patient as required. The variant is designed to quickly load and unload from the V-22 with no major adjustments other than collapsing the roll protection. Additionally, it is less than 900&nbsp;lbs per wheel, making it ideal for use as an Internally Transportable Vehicle (ITV) and can be ordered with the V-22 trailer.<ref name=BCC/>

== See also ==

* [[General Dynamics Flyer]]
* [[Growler (vehicle)]]
* [[Ultra Light Combat Vehicle]]
* [[SPECTRE light vehicle]]

== References ==

{{Reflist|30em}}

== External links ==
* [http://www.hdtglobal.com/products/specialty-vehicles/hdt-storm/ HDT Storm™ SRTV]

[[Category:Military vehicles of the United States]]
[[Category:Military light utility vehicles]]
[[Category:Off-road vehicles]]
[[Category:All-wheel-drive vehicles]]
[[Category:2010s automobiles]]