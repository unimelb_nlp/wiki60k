{{good article}}
{{Infobox University Boat Race
| name= 72nd Boat Race
| image= Boat Race poster 1920.jpg
| image_size = 160px
| caption = London Underground poster advertising the 1920 Boat Race
| winner = Cambridge
| margin = 4 lengths
| winning_time= 21 minutes 11 seconds
| date= {{Start date|1920|03|27|df=y}}
| umpire = [[Frederick I. Pitman]]<br>(Cambridge)
| prevseason= [[The Boat Race 1914|1914]]
| nextseason= [[The Boat Race 1921|1921]]
| overall =32&ndash;39
}}
The '''72nd Boat Race''' took place on 27 March 1920.  Generally held annually, the Boat Race is a [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]].  As a result of the [[First World War]], this was the first race for six years: Oxford went into the race as reigning champions, having won the [[The Boat Race 1914|previous race held in 1914]]. Both universities had participated in various Peace Regattas in 1919.  In this year's race, umpired by former rower [[Frederick I. Pitman]], Cambridge won by four lengths in a time of 21 minutes 11 seconds.  The victory took the overall record to 39&ndash;32 in Oxford's favour.

==Background==
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 20 August 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  The race was first held in 1829, and since 1845 has taken place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 20 June 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref><ref>{{Cite web | url = http://theboatraces.org/the-course | title = The Course| accessdate = 24 July 2014 | publisher = The Boat Race Company Limited}}</ref>  The rivalry is a major point of honour between the two universities and followed throughout the United Kingdom and worldwide.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford&ndash;Cambridge Boat Race crew|date=6 April 2014|publisher=[[CBC News]] |url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=20 August 2014}}</ref>  Oxford went into the race as reigning champions, having won the [[The Boat Race 1914|1914 race]] by four and a half lengths,<ref name=results>{{Cite web | url = http://theboatraces.org/results| publisher = The Boat Race Company Limited| title = Boat Race – Results| accessdate = 20 August 2014}}</ref> and led overall with 39 victories to Cambridge's 31 (excluding the [[The Boat Race 1877|"dead heat" of 1877]]).<ref name=results/><ref>{{Cite web | url= http://theboatraces.org/classic-moments-the-1877-dead-heat | publisher = The Boat Race Company Limited | title = Classic moments – the 1877 dead heat | accessdate =20 August 2014}}</ref>  

The First World War caused a six-year hiatus in the event: during the conflict, at least 42 [[Oxbridge]] [[Blue (university sport)|Blues]] were killed,<ref>Drinkwater, pp. 133&ndash;134</ref> including four of the previous race's Cambridge crew and one from the Oxford boat.<ref>{{Cite web | title = The tragic fate of the 1914 boat race crews revealed| url = http://www.independent.co.uk/news/world/europe/the-tragic-fate-of-the-1914-boat-race-crews-revealed-9241149.html | first = Nick | last = Clark | accessdate = 29 January 2015 | date = 6 April 2014 | work = [[The Independent]]}}</ref>  No race was arranged for 1919, but the crews participated in the Peace Regatta at the [[Henley Royal Regatta]] that year.  Taking part in the King's Cup, Cambridge were defeated by the Australian Army crew in the semi-final, the latter going on to defeat Oxford in the final.  The Light Blues also took part in the Inter Allied Peace Regatta in Paris the same year, victorious in the final against New Zealand and Australia crews.<ref>Burnell, p. 14</ref>

Oxford were coached by R. W. Arbuthnot (who had rowed for Cambridge four times between 1909 and 1912), [[Harcourt Gilbey Gold]] (Dark Blue president for the [[The Boat Race 1900|1900 race]] and four-time Blue) and E. D. Horsfall (who had rowed in the three races prior to the war).  Cambridge's coaches were [[Steve Fairbairn]] (who had rowed in the [[The Boat Race 1882|1882]], [[The Boat Race 1883|1883]], [[The Boat Race 1886|1886]] and [[The Boat Race 1887|1887 races]]) and [[Sidney Swann]] (who had rowed in the previous four races).<ref>Burnell, pp. 110&ndash;111</ref> For the twelfth year the umpire was [[Eton College|Old Etonian]] [[Frederick I. Pitman]] who rowed for Cambridge in the [[The Boat Race 1884|1884]], [[The Boat Race 1885|1885]] and 1886 races.<ref>Burnell, pp. 49, 108</ref>  He was accompanied on his [[Launch (boat)|launch]] by [[Albert, Prince Consort|Prince Albert]] and [[Prince Henry, Duke of Gloucester]].<ref name=drink137/>

==Crews==
The Cambridge crew weighed an average of 12&nbsp;[[Stone (unit)|st]] 9&nbsp;[[Pound (mass)|lb]] (80.1&nbsp;kg), {{convert|1.25|lb|kg|1}} per rower more than their opponents.  As a result of the six-year hiatus, none of the participants had rowed in the Boat Race prior to this year.<ref name=burn71/>  Three of the participants in the race were registered as non-British: Oxford's [[Hugh Cairns (surgeon)|Hugh Cairns]] and Neil Harcourt MacNeil,<ref>{{Cite web | url = http://adb.anu.edu.au/biography/macneil-neil-harcourt-7433 | title = MacNeil, Neil Harcourt (1893–1946) |first = Bruce | last = Mansfield | work = [[Australian Dictionary of Biography]]| accessdate = 28 January 2015}}</ref> and Cambridge's [[John Campbell (rower)|John Alan Campbell]] were all Australian.<ref>Burnell, p. 39</ref>
[[File:Hugh William Bell Cairns.jpg|right|thumb|upright|[[Hugh Cairns (surgeon)|Hugh Cairns]] rowed at number seven for Oxford.]]
{| class=wikitable
|-
! rowspan="2"| Seat
! colspan="3"| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
! colspan="3"| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]] || [[Sebastian Earl|S. Earl]] || [[Magdalen College, Oxford|Magdalen]]  || 12 st 6.5 lb || H. O. C. Boret || [[Trinity College, Cambridge|3rd Trinity]] || 12 st 1 lb
|-
| 2 || N. H. MacNeil || [[Balliol College, Oxford|Balliol]] ||12 st 0 lb || J. H. Simpson || [[Pembroke College, Cambridge|Pembroke]]  || 13 st 0 lb
|-
| 3 || A. T. M Durand || [[Magdalen College, Oxford|Magdalen]] || 13 st 0 lb || A. F. W Dixon || [[Christ's College, Cambridge|Christ's]] || 12 st 11 lb
|-
| 4 || A. C. Hill || [[St John's College, Oxford|St John's]] || 13 st 8.5 lb || R. L. L. McEwan || [[Trinity College, Cambridge|3rd Trinity]] || 13 st 8 lb
|-
| 5 || D. T. Raikes || [[Merton College, Oxford|Merton]] ||13 st 7 lb || [[Humphrey Playford|H. B. Playford]] || [[Jesus College, Cambridge|Jesus]] || 13 st 5 lb
|-
| 6 || [[Walter James, 4th Baron Northbourne|W. E. C. James]] (P) || [[Magdalen College, Oxford|Magdalen]] || 13 st 8.5 lb || [[John Campbell (rower)|J. A. Cambell]] || [[Jesus College, Cambridge|Jesus]] || 13 st 5 lb
|-
| 7 || [[Hugh Cairns (surgeon)|H. W. B. Cairns]] || [[Balliol College, Oxford|Balliol]] || 12 st 0 lb || A. Swann || [[Trinity Hall, Cambridge|Trinity Hall]] || 12 st 0.5 lb
|-
| [[Stroke (rowing)|Stroke]] || M. H. Ellis || [[Keble College, Oxford|Keble]] || 10 st 4.5 lb || P. H. G. H. -S. Hartley (P) || [[Lady Margaret Boat Club]] || 10 st 10.25 lb
|-
| [[Coxswain (rowing)|Cox]] || W. H. Porritt || [[Magdalen College, Oxford|Magdalen]] || 8 st 9.5 lb || [[Robin Johnstone|E. T. Johnstone]] || [[Christ's College, Cambridge|Christ's]] || 8 st 11 lb
|-
!colspan="7"|Source:<ref name=burn71>Burnell, p. 71</ref><br>(P) &ndash; boat club president<ref>Burnell, pp. 50&ndash;51</ref>
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]] along which the Boat Race is contested]]
Cambridge won the [[coin flipping|toss]] and elected to start from the Surrey station, handing the Middlesex side of the river to Oxford.<ref name="times">{{Cite newspaper The Times |articlename=Battle of The Blues |author= |section=Sport |day_of_week=Monday |date=29 March 1920 |page_number=7 |page_numbers= |issue=42371}}</ref> The race started at 5:40&nbsp;p.m. with a fair tide in front of "immense crowds"; ''The Times'' reported that the attendance was the largest in the race's history despite a rainstorm an hour before the race start.<ref name=drink137/><ref name="times" />  Both crews rowed 38 strokes and were level for the first minute until Cambridge, slightly reducing their [[stroke rate|rating]] began to pull ahead, holding a [[Canvas (rowing)|canvas-length]] lead by the Mile Post.<ref name=drink137/>  

As the crews passed below [[Hammersmith Bridge]], the Light Blues held a three-quarter length lead and with the bend in the river in their favour, pulled further ahead to be a quarter of a length clear by The Doves pub.  Facing a strong headwind, Oxford pulled in behind Cambridge and attempted a "bold bid for the inside of the Barnes corner".<ref name=drink137/>  Cambridge spurted in response and passed below [[Barnes Railway Bridge|Barnes Bridge]] three lengths ahead, and extended their lead to four lengths by the time they passed the finishing post in a time of 22 minutes 11 seconds.  According to author and former Oxford rower George Drinkwater, Cambridge "rowed and in nice easy fashion" while Oxford "were very uneven at times and the crew seemed to labour badly in the rough water."<ref name=drink137>Drinkwater, p. 137</ref>  It was Cambridge's second consecutive victory, albeit over a six-year span, and took the overall record in the event to 39&ndash;32 in Oxford's favour.<ref name=results/>

==References==
'''Notes'''
{{reflist|30em}}

'''Bibliography'''
*{{Cite book | title = One Hundred and Fifty Years of the Oxford and Cambridge Boat Race | first = Richard | last = Burnell | authorlink = Dickie Burnell | year=1979| isbn= 978-0-95-006387-4 | publisher = Precision Press}}
*{{Cite book | title = The University Boat Race &ndash; Official Centenary History | first = G. C. |last = Drinkwater | first2= T. R. B. |last2=Sanders |publisher = Cassell & Company, Ltd.| year = 1929}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1920}}
[[Category:1920 in English sport]]
[[Category:The Boat Race]]
[[Category:March 1920 sports events]]
[[Category:1920 in rowing]]