{{Infobox journal
| title         = The Baum Bugle: A Journal of Oz
| cover         = 
| editor        = 
| discipline    = [[Literature]]
| peer-reviewed = 
| language      = English
| former_names  = 
| abbreviation  = 
| publisher     = [[The International Wizard of Oz Club]]
| country       = United States
| frequency     = Triannual
| history       = 1957 to present
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       = http://ozclub.org/Winkie_Country_-_the_Baum_Bugle_1.html 
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 
| LCCN          = 
| CODEN         = 
| ISSN          = 0005-6677
| eISSN         = 
| boxwidth      = 
}}
'''''The Baum Bugle: A Journal of Oz''''' is the official journal of [[The International Wizard of Oz Club]]. The journal was founded in 1957, with its first issue released in June of that year (to a subscribers' list of sixteen). It publishes three times per year, with issues dated Spring, Autumn, and Winter; Issue No. 1 of Volume 50 appeared in the Spring of 2006. The journal publishes both scholarly and popular articles on [[L. Frank Baum]], [[the Oz books]] written by Baum and other writers, and related subjects, plus reviews of Oz-related films and theater productions, rare photographs and illustrations, and similar materials.

Among the range of articles and fiction published in ''The Baum Bugle'':
* Baum's "[[A Kidnapped Santa Claus]]," Winter 1968
*"The Tiger's Eye: A Jungle Fairy Tale," a rare Baum short story, Spring 1979
*"Dorothy Gage and Dorothy Gale," by Sally Roesch Wagner, discussing the familial connection between the Baums and the name Dorothy, Autumn 1984
*"Bibliographia Baumiana: ''The Sea Fairies,''" by Patrick M. Maund, Spring 1997
*"Bibliographia Baumiana: ''The Enchanted Island of Yew,''" by Patrick M. Maund and Peter E. Hanff, Spring 1998
*"Triumph and Tragedy on the Yellow Brick Road: Censorship of ''The Wizard of Oz'' in America," by Hana S. Field, Spring 2000.
*"Adventures in Oz" Vacationing in the Land of Oz Over the Rainbow to Beech Mountain, North Carolina by Gregory Hugh Leng, Spring 2011.

==References==
* Bracken, James K. ''Reference Works in British and American Literature.'' Second edition; Englewood, CO, Libraries Unlimited, 1998.
* Clark, Beverly Lyon. ''Kiddie Lit: The Cultural Construction of Children's Literature in America.'' Baltimore, Johns Hopkins University Press, 2003.
*Multiple Authors. ''The Best of the Baum Bugle.'' The International Wizard of Oz Club; published biennially.
*Otto, Frederick E. ''Index to the Baum Bugle.'' The International Wizard of Oz Club, 1990. Revised and expanded by Richard R. Rutter, 2002.
* Rogers, Katharine M. ''L. Frank Baum: Creator of Oz.'' New York, St. Martin's Press, 2002.

==External links==
* [http://ozclub.org/Winkie_Country_-_the_Baum_Bugle_1.html On ''The Baum Bugle'']

{{Portal |Children's literature}}
{{Oz}}
{{L. Frank Baum}} 

{{DEFAULTSORT:Baum Bugle, The}}
[[Category:Fanzines]]
[[Category:Publications established in 1957]]
[[Category:Oz studies]]


{{Oz-stub}}