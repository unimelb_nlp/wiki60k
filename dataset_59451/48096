Genesis Archive are two archives with rarities of the British band [[Genesis (band)|Genesis]]:

==Genesis Archive 1967-75==
{{Infobox album <!-- See Wikipedia:WikiProject_Albums. -->
| Name       = Genesis Archive 1967–75
| Type       = box
| Artist     = [[Genesis (band)|Genesis]]
<!-- Commented out: | Cover      = Genesisarchive67-75.jpg -->
| Released   = 22 June 1998
| Recorded   = 1967–1975, 1995
| Genre      = [[Progressive rock]]
| Length     = 250:43
| Label      = [[Virgin Records|Virgin]] (UK) CD BOX 6 <br />[[Atlantic Records|Atlantic]] (US)
| Producer   = [[Genesis (band)|Genesis]], [[Nick Davis (music producer)|Nick Davis]], Geoff Callingham
| Last album = ''[[Calling All Stations]]''<br>(1997)
| This album = '''''Genesis Archive 1967–75'''''<br>(1998)
| Next album = ''[[Turn It On Again: The Hits]]''<br>(1999)
}}
'''''Genesis Archive 1967–75''''' is a 1998 boxed set by [[progressive rock]] band [[Genesis (band)|Genesis]].

This set features live recordings and rarities from when [[Peter Gabriel]] was the lead singer. The first two of the four discs consist of a previously unreleased (except for "The Waiting Room", issued as a 7" b-side in 1974) live recording of ''[[The Lamb Lies Down on Broadway]]'' from 1975. The remaining discs contain performances from an unreleased live concert from London's Rainbow Theatre in 1973, demos, rare single tracks and 1970 [[BBC]] Sessions. All 52 tracks (besides the last 3 on disc 3 and track 2 from disc 2) had been previously unreleased. The entire set is arranged in reverse chronological order.

Some of Peter Gabriel's vocals on the live recordings were re-recorded by the singer in 1998 due to technical problems with the recording as well as elaborate costumes that often muffled his voice.<ref name=review>{{Allmusic|class=album|id=r365174/review|pure_url=yes}}</ref> Guitarist [[Steve Hackett]] re-recorded some guitar parts as well.<ref name=review/>

''Genesis Archive 1967–75'' reached No. 35 in the UK.

==Track listing==

===Disc one===
All songs written by [[Tony Banks (musician)|Tony Banks]]/[[Phil Collins]]/[[Peter Gabriel]]/[[Steve Hackett]]/[[Mike Rutherford]].

#"[[The Lamb Lies Down on Broadway (song)|The Lamb Lies Down on Broadway]]" - 6:29  
#"[[Fly on a Windshield]]" - 2:54
#"Broadway Melody of 1974" - 2:19<ref>As with several CD editions of ''The Lamb Lies Down on Broadway'', the CD has tracks two and three mis-indexed, having them as 4:38 and 0:34, respectively, due to an error by the record label. "Broadway Melody of 1974" is intended to begin with the line "Echoes of the Broadway Everglades", but instead the instrumental break at the end of the song is the only thing included in the CD index for that track.</ref>
#"Cuckoo Cocoon" - 2:17  
#"[[In the Cage (song)|In the Cage]]" - 7:56
#"The Grand Parade of Lifeless Packaging" - 4:25
#"Back in N.Y.C." - 6:19   
#"Hairless Heart" - 2:22     
#"Counting Out Time" - 4:00    
#"[[The Carpet Crawlers]]" - 5:45    
#"The Chamber of 32 Doors" - 5:52

===Disc two===
All songs written by Banks/Collins/Gabriel/Hackett/Rutherford.

#"Lilywhite Lilith" - 3:04
#"[[The Waiting Room (song)|The Waiting Room]]" - 6:15
#"Anyway" - 3:28
#"Here Comes the Supernatural Anaesthetist" - 3:57
#"The Lamia" - 7:12
#"Silent Sorrow in Empty Boats" - 3:15
#"The Colony of Slippermen (Arrival - A Visit to the Doktor - Raven)" - 8:47
#"Ravine" - 1:39
#"The Light Dies Down on Broadway" - 3:37     
#"Riding the Scree" - 4:30   
#"In the Rapids" - 2:25
#"It" - 4:20
#* ''All of the above recordings were captured live at the [[Shrine Auditorium]], [[Los Angeles, California]] on 24 January 1975''
#* ''"It" is a reworked studio version with new vocals prepared for this box set''
#* ''There are vocal and guitar overdubs recorded specifically for this box set by Peter Gabriel and Steve Hackett''

===Disc three===
All songs written by Banks/Collins/Gabriel/Hackett/Rutherford, except where noted.

#"[[Dancing with the Moonlit Knight]]" - 7:05
#"[[Firth of Fifth]]" - 8:29
#"[[More Fool Me (Genesis song)|More Fool Me]]" - 4:01
#"[[Supper's Ready]]" - 26:31
#"[[I Know What I Like (In Your Wardrobe)]]" - 5:36
#* ''Tracks 1–5 recorded live at the Rainbow Theatre in London, England on 20 October 1973''
#"Stagnation" <small>(Banks/Gabriel/[[Anthony Phillips]]/Rutherford)</small> - 8:52
#* ''A 10 May 1971 recording for the [[BBC]] "Sounds of the 70s" show''     
#"Twilight Alehouse" <small>(Banks/Gabriel/Phillips/Rutherford)</small> - 7:45
#* ''A 3 August 1973 Non- LP [[B-side]] to the "I Know What I Like (In Your Wardrobe)" single''   
#"Happy the Man" - 2:53
#* ''A 10 May 1972 [[A-side]] non-LP single release''
#"[[Watcher of the Skies]]" - 3:42
#* ''A 1972 re-recording for an A-side single release, shortened but containing an ending segment not heard on the album version''

===Disc four===
All songs written by Banks/Gabriel/Phillips/Rutherford.

#"In the Wilderness" - 3:00
#* ''August 1968 rough mix without strings''
#"Shepherd" - 4:00 
#"Pacidy" - 5:42  
#"Let Us Now Make Love" - 6:14
#* ''Tracks 2-4 recorded 22 February 1970 for the BBC "Night Ride" show''  
#"Going Out to Get You" - 4:54
#"Dusk" - 6:14
#* ''Tracks 5-6 are 20 August 1969 Regent Sound demos''
#"Build Me a Mountain" - 4:13    
#"Image Blown Out" - 2:12    
#"One Day" - 3:08
#* ''Tracks 7-9 are August 1968 rough mixes''
#"Where the Sour Turns to Sweet" - 3:14    
#"In the Beginning" - 3:31    
#"The Magic of Time" - 2:01
#* ''Tracks 10-12 are early 1968 demos'' 
#"Hey!" - 2:28
#* ''13 March 1968 demo''
#"Hidden in the World of Dawn" - 3:10
#"Sea Bee" - 3:05    
#"The Mystery of the Flannan Isle Lighthouse" - 2:36    
#"Hair on the Arms and Legs" - 2:42
#* ''Tracks 14-17 are Autumn 1967 demos''
#"She Is Beautiful" - 3:47 (became "The Serpent" with different lyrics)
#"Try a Little Sadness" - 3:21
#* ''Tracks 18-19 are Summer 1967 demos''
#"Patricia" - 3:05 (became "In Hiding" with lyrics added)
#* ''Easter 1967 demo''

==Personnel==
*[[Peter Gabriel]]: [[lead vocals]], [[Backing vocalist|backing vocals]], [[flute]], [[percussion]], [[Drum kit|drums]] ("Patricia")
*[[Tony Banks (musician)|Tony Banks]]: [[piano]], [[organ (music)|organ]], [[electric piano]], [[Mellotron]], [[synthesizer]], 12-string [[guitar]], backing vocals, 2nd lead vocal ("Shepherd")
*[[Mike Rutherford]]: [[bass guitar]], 12-string guitar, [[bass pedals]], backing vocals
*[[Steve Hackett]]: lead guitar (Discs 1-3)
*[[Phil Collins]]: [[Drum kit|drums]], [[Percussion instrument|percussion]], vocals (Discs 1-3), lead vocals ("More Fool Me")
*[[Anthony Phillips]] : guitars, backing vocals (Disc 4), 2nd lead vocal ("Let Us Now Make Love")
*[[John Mayhew (musician)|John Mayhew]]: drums (Disc 4, tracks 3-6)
*[[John Silver (musician)|John Silver]]: drums (Disc 4, tracks 1, 7-8), biscuit tin (Disc 4, track 12)
Note: Not all credits for percussion/drums are listed in the booklet for Disc 4. On "Patricia" they are played by Peter Gabriel. Original drummer [[Chris Stewart (author)|Chris Stewart]] is not listed.

==Reception==
*[[Allmusic]] {{Rating|4|5}}<ref name=review/>
*''[[Entertainment Weekly]]'' B<ref>{{cite journal |title=Genesis Archives 1967-75 Review |publisher=''[[Entertainment Weekly]]'' |date=19 June 1998 |page=75}}</ref>
*''[[The Rolling Stone Album Guide]]'' {{Rating|3|5}}<ref name="The new Rolling Stone album guide">{{cite book | author1= Nathan Brackett | author2= Christian David Hoard| title = The new Rolling Stone album guide | publisher = Simon & Schuster | pages = 327–328 | location = New York | year = 2004 | isbn = 978-0-7432-0169-8 | url = https://books.google.com/books?id=t9eocwUfoSoC&pg=PA328&lpg=PA328&dq=rolling+stone+genesis+album+guide&source=bl&ots=BiKpmp3MS7&sig=zfX0_JMOyscHEEf3Pzaddk7thes&hl=en&sa=X&ei=RPjQT-DCK6fl0gGns-XWDQ&ved=0CF0Q6AEwBA#v=onepage&q&f=false}}</ref>

==Genesis Archive 2: 1976-1992==
{{Infobox album <!-- See Wikipedia:WikiProject_Albums -->
| Name        = Genesis Archive #2: 1976–1992
| Type        = box
| Artist      = [[Genesis (band)|Genesis]]
<!-- Commented out: | Cover       = Genesisarchive76-92.jpg -->
| Released    = 6 November 2000
| Recorded    = 1976–1992
| Genre       = [[Pop rock]], [[art rock]]
| Length      = 206:18
| Label       = [[Virgin Records|Virgin]] (UK) CD BOX 7<br>[[Atlantic Records|Atlantic]] (US)
| Producer    = [[Genesis (band)|Genesis]], [[Hugh Padgham]], [[David Hentschel]], [[Nick Davis (music producer)|Nick Davis]]
| Reviews     = 
| Last album  = ''[[Turn It on Again: The Hits]]''<br>(1999)
| This album  = '''''Genesis Archive 2: 1976–1992'''''<br>(2000)
| Next album  = ''[[Platinum Collection (Genesis album)|Platinum Collection]]''<br>(2004)
}}

{{Album ratings
| rev1      = [[Allmusic]]
| rev1Score = {{Rating|2.5|5}}<ref>{{Allmusic|class=album|id=r505115}}</ref>
| rev2      = ''[[Entertainment Weekly]]''
| rev2Score = C<ref>{{cite web|url=http://www.ew.com/ew/article/0,,20273920,00.html|title=Packaged Deals (Genesis: ''Genesis Archive 2: 1976-1992'')|last=Morgan|first=Laura|publisher=''[[Entertainment Weekly]]''|date=2000-12-01|accessdate=2012-06-09}}</ref>
| rev3      = ''[[The Rolling Stone Album Guide]]''
| rev3Score = {{Rating|2|5}}<ref name="The new Rolling Stone album guide"/>
}}

'''''Genesis Archive #2: 1976–1992''''' is a 2000 boxed set by veteran [[progressive rock]]/[[pop music|pop]] band [[Genesis (band)|Genesis]]. This retrospective covers the band's history while [[Phil Collins]] was lead singer.

The three discs consist of live recordings (mostly previously unreleased, with a few that had been B-sides), 12" remixes, a "work-in-progress" jam, non-album [[B-sides]], and tracks from the ''[[Spot the Pigeon]]'' and ''[[3X3]]'' EPs. There was a minor backlash from hardcore fans as "Match of the Day" and "Me and Virgil" were left off the set (although both had been released on CD before) due to [[Tony Banks (musician)|Tony Banks]] and [[Mike Rutherford]] disliking the former track while Phil Collins disliked the latter one. (Less talked about was the fact that "Submarine" and "It's Yourself", neither of which had been released on CD otherwise, were both edited versions—the former due to master damage, the latter due to the band's preference. "Submarine" was later released in its unedited form on ''[[Genesis 1976–1982]]''.)

''Genesis Archive 2: 1976–1992'' reached No. 103 in the UK.

==Track listing==
All songs written by [[Tony Banks (musician)|Tony Banks]], [[Phil Collins]], and [[Mike Rutherford]], except where noted.

===Disc one===
#"On The Shoreline" - 4:47
#* ''[[B-Side]] of "[[I Can't Dance]]", December 1991''
#"Hearts On Fire" - 5:14
#* ''B-Side of "[[Jesus He Knows Me]]", July 1992''
#"You Might Recall" - 5:30
#"[[Paperlate]]" - 3:20
#* ''above two tracks recorded in 1981 during the [[Abacab]] sessions and released in 1982 in the UK on the [[3X3]] EP and on the original [[North America]]n release of [[Three Sides Live]]''
#"Evidence Of Autumn" <small>(Banks)</small> - 4:58
#* ''B-Side of "[[Misunderstanding (Genesis song)|Misunderstanding]]", August 1980 in the UK; released on the original [[North America]]n release of Three Sides Live''
#"Do The Neurotic" - 7:07
#* ''B-Side to "[[In Too Deep (Genesis song)|In Too Deep]]" in the UK, August 1986''
#"I'd Rather Be You" - 3:57
#* ''B-Side to "[[Throwing It All Away]]" in the UK; July 1987''
#"Naminanu" - 3:52
#* ''B-side to "[[Keep It Dark]]", October 1981''
#"Inside And Out" <small>(Banks, Collins, [[Steve Hackett]], Rutherford)</small> - 6:43
#* ''From the [[Spot the Pigeon]] EP, May 1977; B-side to "[[Follow You, Follow Me]]", 1978''
#"Feeding The Fire" - 5:49
#* ''B-Side to "[[Land Of Confusion]]" in the UK, November 1986''
#"[[I Can't Dance]]" - 7:00
#* ''12" remix''
#"Submarine" - 5:13
#* ''Modified version of the B-side to "[[Man on the Corner]]", March 1982; original ending is omitted and another part is repeated, resulting in an edited version that is longer than the original.''

===Disc two===
#"[[Illegal Alien (song)|Illegal Alien]]" - 5:31
#* ''Recorded live at the [[The Forum (Inglewood, California)|L.A. Forum]], [[Los Angeles]] on 14 January 1984''
#"Dreaming While You Sleep" - 7:48
#* ''Recorded live at [[Earls Court Exhibition Centre]], [[London]] in November 1992''
#* ''The B-Side of [[Tell Me Why (Genesis song)|Tell Me Why]] CD Single''
#"It's Gonna Get Better" - 7:31
#* ''Recorded live at the L.A. Forum, Los Angeles on 14 January 1984''
#"[[Deep in the Motherlode]]" <small>(Rutherford)</small> - 5:54
#* ''Recorded live at [[Theatre Royal, Drury Lane]], London on 5 May 1980''
#"Ripples" <small>(Banks, Rutherford)</small> - 9:54
#* ''Recorded live at the [[Lyceum Theatre (London)|Lyceum Theatre]], London on 6 May 1980''
#"[[The Brazilian]]" - 5:18
#* ''Recorded live at [[Wembley Stadium (1923)|Wembley Stadium]], London on 4 July 1987''
#* ''The B-Side of Invisible Touch (live) CD Single''
#"Your Own Special Way" <small>(Rutherford)</small> - 6:51
#* ''Recorded live at the [[Sydney Entertainment Centre]] in December 1986''
#* ''The B-Side of [[Hold on My Heart]] CD Single''
#"Burning Rope" <small>(Banks)</small> - 7:29
#* ''Recorded live at the [[Hofheinz Pavilion]], [[Houston]] on 22 October 1978''
#"[[Entangled (song)|Entangled]]" <small>(Banks, Hackett)</small> - 6:57
#* ''Recorded live at Bingley Hall, [[Staffordshire]] on 10 July 1976''
#"Duke's Travels" - 9:32
#* ''Includes "Duke's End." Recorded live at the Lyceum Theatre, London on 7 May 1980''

===Disc three===
#"[[Invisible Touch (song)|Invisible Touch]]" - 5:58
#* ''12" remix''
#"Land Of Confusion" - 6:59
#* ''12" remix''
#"[[Tonight, Tonight, Tonight]]" - 11:46
#* ''12" remix''
#"[[No Reply At All]]" - 4:56
#"Man On The Corner" <small>(Collins)</small> - 4:04
#* ''The above two tracks recorded live at the Savoy Theatre, [[New York City]] on 28 November 1981''
#"[[The Lady Lies (song)|The Lady Lies]]" <small>(Banks)</small> - 6:07
#* ''Recorded live at the Lyceum Theatre, London on 6 May 1980''
#* ''The nearly identical version published on a green translucide flexi-disc in ''[[Flexipop]]'' #21 in 1982 in the UK was recorded live at the Lyceum Theatre, London on 7 May 1980''
#"Open Door" (Rutherford) - 4:08
#* ''The B-Side of "[[Duchess (Genesis song)|Duchess]]" in May 1980 in the UK, and released on the original [[North America]]n release of [[Three Sides Live]]''
#"The Day The Light Went Out" <small>(Banks)</small> - 3:12
#"Vancouver" <small>(Collins, Rutherford)</small> - 3:01
#* ''The above two tracks constituted a double B-Side for "[[Many Too Many]]" in June 1978''
#"Pigeons" - 3:12
#* ''From the Spot the Pigeon EP in May 1977''
#"It's Yourself" <small>(Banks, Collins, Hackett, Rutherford)</small> - 5:26
#* ''Recorded during [[A Trick of the Tail]]'s sessions (and excerpted to open "Los Endos"), this track would be released as the B-Side to "Your Own Special Way" in February 1977.  The original version was about 5:45;  this is faded out early.''
#"[[Mama (Genesis song)|Mama (Work in Progress)]]" - 10:43
#* ''A rehearsal take recorded in 1983 during the [[Genesis (Genesis album)|Genesis]]'' sessions''

==Personnel==
*[[Tony Banks (musician)|Tony Banks]] - [[keyboard instrument|keyboards]], [[Singing|vocals]], background vocals
*[[Phil Collins]] - [[Drum kit|drums]], [[percussion instrument|percussion]], vocals
*[[Mike Rutherford]] - [[guitar]]s, [[bass guitar|bass]], background vocals
*[[Steve Hackett]] - [[guitar]]s on Disc 1, Track 9; Disc 2, Track 9; and Disc 3; Tracks 10 and 11

;Additional personnel

*[[Daryl Stuermer]] - bass, guitar on Disc 2 (except "Entangled") and on Disc 3, Tracks 4, 5 and 6
*Chester Thompson - percussion, drums on Disc 2 (except "Entangled") and on Disc 3, Tracks 4, 5 and 6
*[[Bill Bruford]] - percussion on Disc 2, Track 9

==References==
{{Reflist}}

{{Genesis}}

{{Use dmy dates|date=December 2010}}

{{DEFAULTSORT:Genesis Archive 2: 1976-1992}}
[[Category:Albums produced by Hugh Padgham]]
[[Category:Albums produced by David Hentschel]]
[[Category:Genesis (band) compilation albums]]
[[Category:Atlantic Records compilation albums]]
[[Category:Albums produced by Nick Davis (record producer)]]
[[Category:B-side compilation albums]]
[[Category:2000 compilation albums]]
[[Category:Virgin Records compilation albums]]