{{Infobox person
| name = John Irvin Beggs
| image = JohnIBeggs.jpg
| caption = 
| birth_date ={{Birth date|1847|9|17|mf=y}}
| birth_place =[[Philadelphia, Pennsylvania]], [[United States]] 
| death_date ={{death date and age|1925|10|17|1847|9|17|mf=y}} 
| death_place =[[Milwaukee, Wisconsin]]
| occupation = [[Financier]], [[Entrepreneur]], [[Industrialist]]
| salary = $8,000 annually in 1888
| networth = $20 Million in 1925
| spouse = Sue Elizabeth Charles
| website = 
}}
'''John Irvin Beggs''' (September 17, 1847 &ndash; October 17, 1925) was an [[United States|American]] businessman. He was associated closely with the electric utility boom under [[Thomas Edison]]. He was also associated with [[Milwaukee, Wisconsin|Milwaukee]], [[St. Louis, Missouri]] and other regional rail and [[interurban]] [[tram|trolley]] systems. Beggs is also known for developing modern [[depreciation]] techniques for business accounting and for being one of the early directors of what became [[General Electric]].

== Youth ==
John Irvin Beggs was born in [[Philadelphia]] on September 17, 1847, the son of James and Mary Irvin Beggs.  Both of his parents were of [[Scottish descent]] but had emigrated to the United States from [[Northern Ireland]].

His early life was spent around Philadelphia. After his father died when he was seven years old, Beggs worked to support of his mother in a brickyard, as a cattleman, and butcher.

== Education ==
As a young man Beggs taught accounting and handwriting in the [[Bryant & Stratton College|Bryant & Stratton Business College]] in Philadelphia. He went to [[Harrisburg, Pennsylvania]] at the age of 21 to work for Mitchell & Haggerty Coal Company as an accountant.  He then worked selling real estate and fire insurance in Harrisburg.  Beggs joined the [[Masonic]] fraternities at Harrisburg and maintained his membership until his death.

== Electric light industry ==
When the electric light industry was in its infancy, Beggs assisted organization of the [[Harrisburg Electric Light Co.]] He built and managed its plant  which was "the first commercially successful electric light plant in the United States".  Beggs’ interest in electric lighting arose because he was head of the building committee of [[Grace Methodist Episcopal Church, Harrisburg, PA.|Grace Methodist Episcopal Church]] and wanted to electrify the church to save on the cost and cleanup of candles.  This church became the first in the world to be wired and to use light bulbs instead of candles.

He was married in Harrisburg to Sue Elizabeth Charles, who died  March 14, 1902.  They had one child, Mary Grace Beggs.

On account of his success in Harrisburg as an electric plant manager, he was called by [[J.P. Morgan]] to [[New York City]] in 1886 as manager of the [[Edison Illuminating Company]] of that city. He remained in New York for about five years during which time he built two electric stations.<ref>[https://books.google.com/books?id=Gp1xBgAAQBAJ&pg=PA15&lpg=PA15&dq=John+I.+Beggs+edison&source=bl&ots=HZMG3g8fHW&sig=ATltS8VnJn7IP0RUfrYhXA7hOqc&hl=en&sa=X&ved=0CDIQ6AEwBGoVChMItIiX8ofMxwIVCHo-Ch3KRQKq#v=onepage&q=John%20I.%20Beggs%20edison&f=false Ken Riedl, William F. Jannke III, Watertown History Annual 2: Hometown Series of Publications, pages 13-19]</ref>  Pearl Street provided electricity for the first time to [[Wall Street]]'s stockbrokers.

He worked closely with [[Thomas A. Edison]] and consequently became one of that small group known as [[Edison Pioneers]].  Beggs was one of the Illuminating Company Directors.  He was also a Director at the Detroit Edison Board meeting when [[Henry Ford]] first met Edison and first pitched his idea for the automobile startup to those venture capitalists present.

== Career ==
From New York he went to [[Chicago]] as Western Manager of [[General Electric#History|Edison Company]] where he remained until the Edison Company was merged with the [[Thomson-Houston Electric Company]] to form what is now the [[General Electric|General Electric Company]].

The [[North American Company]], which had just been organized, had acquired an electric lighting interest in [[Cincinnati, Ohio]] and Beggs went to Cincinnati in charge of these interests.  The North American Company shortly afterward acquired the electric railway and lighting companies in [[Milwaukee, Wisconsin]], and for several years, Beggs divided his time between these cities.  In 1897, the Cincinnati interests were sold and Beggs moved to Milwaukee to devote his time to the utilities there.

In 1903, The North American Company began to acquire electric lighting interests in [[St. Louis, Missouri]].  Beggs first visited St. Louis as an advisor, and then began to divide his time between the two cities.  At one time, Beggs was president of the St. Louis electric lighting company, the gas company, and the street railway company, as well as president and general manager of [[The Milwaukee Electric Railway and Light Company]].

While Beggs was President of the Milwaukee Companies he built the Public Service Building in Milwaukee. His funeral services were conducted in its auditorium by the [[Employees’ Mutual Benefit Association]].  He also constructed the systems of [[interurban railways]] radiating from Milwaukee.

By 1911 Beggs had acquired a controlling interest in the [[St. Louis Car Company]]. He resigned from the Milwaukee companies and moved to St. Louis.  He still maintained many business connections in Milwaukee and spent time there, although his residence was in St. Louis.

==Beggs Isle==
In the spring of 1911, Beggs purchased and named Beggs Isle in [[Lac La Belle, Wisconsin|Lac La Belle]], at {{coord|43.125|-88.509|type:isle_region:US-WI|display=inline|name=Beggs Isle}},<ref>{{GNIS |1561506 |Beggs Island }} Retrieved January 13, 2011.</ref> near [[Oconomowoc, Wisconsin]].  He developed it into a summer residence for himself and his daughter’s family.  Beggs turned this island into a botanical garden bringing in exotic plants. Egyptian [[papyrus]] plants were trained to last through the long Wisconsin winters.  Beggs would purchase large commercial grade fireworks for their [[Fourth of July]] celebrations.<ref name=Beggs>{{cite journal
  | author = John I. Beggs  
  | title = Personal Correspondence
  | version = Original
  | date = 1910–1912
  | format = bound carbon copies }}</ref>

In 1915, he invested in water power in northern Wisconsin and began to spend more time in that state, although still residing in St. Louis.  In 1920 he was again elected president of The Milwaukee Electric Railway and Light Co., which position he still held at the time of his death.

Beggs was a member of the Executive Committee of the North American Company.  He also devoted much time to the First Wisconsin National Bank in which he invested.  During his last decade he directed the construction of the second largest paper mill in the country; engineered the reorganization of the J. I. Case Plow Company, arranged to finance a hotel in [[Atlantic City, New Jersey]]. and conducted a large Florida real estate transaction.<ref name=Linden>{{cite book
  | last = Linden
  | first = [[Mary Sue McCulloch]]
  | title = Suzie's Story:The Autobiography of Socialite, Philanthropist & World Traveler 
  | publisher = Rainbow Books
  | year = 1992
  | pages = 7–10
  | isbn = 0-935834-87-7}}</ref>

== Director and Officer ==
At the time of his death, Beggs was an active director or officer of 53 companies, including:

# [[General Electric|North American Edison Company]], Director (Now General Electric)
# [[North American Company|The North American Company]], Director, Member of Executive Committee
# [[The Milwaukee Electric Railway and Light Company]], Director, President, Member of Executive Committees
# [[Wisconsin Energy Corporation|Wisconsin Gas & Electric Company]], Director, Vice-President
# [[Briggs and Stratton|Briggs & Stratton Corporation]], Director, Chairman Executive Committee
# [[St. Louis Car Company]], Director, Chairman of Board
# [[Case Corporation|J. I. Case Plow Works Company, Inc.]], Director
# [[Southern Improvement Company]], Director, President
# [[Firstar Corporation|First Wisconsin National Bank]], Milwaukee, Director, Member of Executive and Finance Committees
# [[First Wisconsin Company]], Milwaukee, Director
# [[Grand & Sixth National Bank]], Milwaukee, Director, Member of Executive and Finance Committees
# [[First National Bank in St. Louis]], Director
# [[Milwaukee Northern Railway Company]], Director, President
# [[Wisconsin Traction, Light, Heat & Power Company]], Director, President
# [[Peninsular Power Company]], Director
# [[North American Utilities Investment Corporation]], Director
# [[West Kentucky Coal Company]], Director
# [[United Railways Company of St. Louis]], Director
# [[Wisconsin Securities Company]], Director, Member of Executive Committee
# [[Wisconsin Public Service Corporation]], Director
# [[Menominee & Marinette Light & Traction Company]], Director
# [[Wisconsin Railway, Light & Power Company]], Director
# [[Land & Sea Investment Company]], Baltimore, Director
# [[Oil Transport Company, Baltimore]], Director
# [[Wisconsin Power, Light & Heat Company]], Director
# [[Wisconsin River Power Company]] (Hydro-Electric), Director
# [[Southern Wisconsin Power Company]] (Hydro-Electric), Director
# [[Northwestern Casualty & Surety Company]], Director, Member of Executive and Finance Committees
# [[Newport Company|The Newport Company]], Director
# [[The Milwaukee Coke & Gas Company|Milwaukee Coke & Gas Company]], Director
# [[Globe Electric Company]], Director, President
# [[Globe Real Estate Company]], Director, President
# [[Wisconsin Real Estate Development Corporation]], Director, President
# [[Grand & Sixth Building, Inc.]], Director, President
# [[Central Utilities Securities Corporation]], Director, President, Treasurer
# [[Midland Oil, Gas & Refining Company]], Director
# [[Lane Oil Producing Company]], Director
# [[Johnson & King Coal Company]], Director, President
# [[Raven Mining Company]], Director
# [[American Granite Company]], Director, President
# [[Shotwell Manufacturing Company]], Director
# [[East Coast Development Company]] (Florida), Director, President
# [[Louisiana Pulp & Paper Company]] (Bastrop, LA.), Director, President
# [[Frankenberg Refrigerating Company]], Director
# [[Prescott & Northwestern Ry. Company]], (Prescott, Ark.), Vice-President
# [[Montana Railroad Company]] (Clarksville, Ark.), President
# [[President Apartment Hotel Company]] (Atlantic City), President

== Legacy ==
He died in [[Milwaukee]] on October 17, 1925 at the age of 78.  He was buried in [[Harrisburg, Pennsylvania]].

Edison and Beggs remained friends throughout their lifetimes.  On Beggs' 75th Birthday Celebration on Beggs Isle, Edison presented Beggs with a large grandfather clock and a signed photograph addressed "To my hustler friend,  (signed) Thomas A. Edison".

At the time of his death, Beggs was reported to be the wealthiest man in Wisconsin, with an estimated net worth of over $20M.  He passed this fortune to his grandchildren:

* Grandson [[Robert P. McCulloch|Robert Paxton McCulloch]] (1911–1977), was responsible for [[McCulloch (company)|McCulloch Chainsaws]], the [[Paxton Automotive|Paxton Supercharger]], founding [[Lake Havasu City]], Arizona and moving the [[London Bridge (Lake Havasu City)|London Bridge]] to [[Arizona]].<ref name="Linden"/>  He married Barbra Ann Briggs, whose father was [[Stephen Foster Briggs]] of [[Briggs and Stratton]].
* Granddaughter  [[Mary Sue McCulloch]], "Suzie Linden" (1913–1996), author of ''Suzie's Story'', was inducted into the [[Croquet Hall of Fame]] for founding in 1957, and operating for forty years, the [[Green Gables Croquet Club]] in [[Spring Lake, New Jersey]], the oldest continuously running club in the USA.<ref>{{cite book
  | author = Karen Kaplan
  | title = The New York Croquet Club History
  | work =
  | publisher =
  | date = September 2004
  | url = http://www.newyorkcroquetclub.com/images/NYCCLUB_History.pdf
  | format = PDF
  | page= 8
  | accessdate = January 13, 2011 }}</ref>  Also, a founding member of the [[United States Croquet Association|USCA]].  In 1931, She first married [[Whip Jones]] who went on to found [[Aspen Highlands]] in [[Aspen]], [[Colorado]] and was inducted into the Aspen Hall of Fame [http://www.aspenhistory.org/hofnom.html] and the [http://www.coloradoskihalloffame.com/ Colorado Ski Hall of Fame][http://www.coloradoskihalloffame.com/images_bio_htm_files/Whip_Jones.htm].  Subsequent husbands were New York investment banker and attorney [[James Lowell Oakes]] (father of judge [[James L. Oakes]]), [[World War II]] veteran [[Bruce Lister Knight]], and Portland banker [[Carvel Cabel Linden]].

* Grandson [[John I.B. McCulloch|John Irvin Beggs McCulloch]] (1908–1983), became fluent in over 10 foreign languages.  After graduating from [[Yale]], in 1933 he married Whip's sister Elizabeth Ten Broeck Jones.  He became a foreign political analyst and a prolific writer of books and articles in many languages.  Later in his career he supported the [[English Speaking Union]] pushing for the wider use of English.

These three had another notable grandfather, [[Capt. Robt. McCulloch|Robert McCulloch]] (1841–1914) who was the only confederate officer to survive the [[High-water mark of the Confederacy|High Tide]] of [[Pickett's Charge]] at the [[Battle of Gettysburg]].<ref>{{cite book
  | author = Bruce McCulloch Jones, Editor
  | title = The High Tide at Gettysburg
  | work = Memorial packet and first hand account of Gettysburg (Battle 1863) and (Reconciliation 1913)
  | publisher = Amazon
  | date = July 2013
  | url = http://www.amazon.com/High-Tide-at-Gettysburg-ebook/dp/B00DNKSW6I
  | format = PDF
  | page= 29
  | access-date = July 3, 2013 }}</ref>   Both grandfathers were active in [[Freemasonry]].

== Filmography ==
* {{IMDb title|0224296|The Trolley at East Troy (1986)}} - 1986 documentary, directed by [http://www.imdb.com/name/nm0749376/ Louis Rugani].  Through the use of archival footage John I. Beggs 'stars' in this look at the history and survival efforts of this small anachronistic Wisconsin trolley line since 1907, and an overview of its relationship to the surrounding area, the now-dissolved parent company which built it, and the vanished traction empire of which it was a small part.

== See also ==
*[[East Troy Electric Railroad]]
*[[Edison Pioneers]]

== References ==
{{reflist}}

==Further reading==
* {{cite book |title= Let there be light: The electric utility industry in Wisconsin, 1881-1955 |author= [[Forrest McDonald]] |year= 1957 |publisher= American History Research Center |url= https://books.google.com/books?ei=gp4vTbGWLZC4sAPLrPixBQ }}
* {{cite book |title= The High Tide at Gettysburg |author= Bruce McCulloch Jones |year= 2013 |publisher= Amazon |url= http://www.amazon.com/The-High-Tide-at-Gettysburg-ebook/dp/B00DNKSW6I }}
* In Memoriam, John Irvin Beggs (1926), 49 pages, with Photo
* [https://books.google.com/books?id=Gp1xBgAAQBAJ&pg=PA15&lpg=PA15&dq=John+I.+Beggs+edison&source=bl&ots=HZMG3g8fHW&sig=ATltS8VnJn7IP0RUfrYhXA7hOqc&hl=en&sa=X&ved=0CDIQ6AEwBGoVChMItIiX8ofMxwIVCHo-Ch3KRQKq#v=onepage&q=John%20I.%20Beggs%20edison&f=false Watertown History Annual 2: Hometown Series of Publications, By Ken Riedl, William F. Jannke III, pages 13-19]

==External links==
* [http://www.wisconsinhistory.org/dictionary/index.asp?action=view&term_id=1073&term_type_id=1&term_type_text=People&letter=B Biographical sketch] from [[Wisconsin Historical Society]]
* {{cite web |title= Beggs Isle, Lac La Belle, Oconomowoc, Wis (1927) |date= June 12, 2006 |publisher= Oconomowoc Historical Society |url= http://www.oconomowochistoricalsociety.com/Oconomowoc/pres0103.html |accessdate= January 13, 2011 }}

{{Briggs & Stratton}}

{{Authority control}}

{{DEFAULTSORT:Beggs, John Irvin}}
[[Category:1847 births]]
[[Category:1925 deaths]]
[[Category:American technology chief executives]]
[[Category:American energy industry executives]]
[[Category:American financiers]]
[[Category:American people of Scottish descent]]
[[Category:American railroad executives of the 20th century]]
[[Category:American real estate businesspeople]]
[[Category:Businesspeople from Wisconsin]]
[[Category:Thomas Edison]]
[[Category:Edison Pioneers]]
[[Category:People from Harrisburg, Pennsylvania]]
[[Category:People from Milwaukee]]
[[Category:People from Oconomowoc, Wisconsin]]
[[Category:Businesspeople from Philadelphia]]