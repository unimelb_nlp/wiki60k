{{featured article}}

{{Infobox Christian leader
| type             = bishop
| honorific-prefix = [[The Right Reverend]]
| name             = Michael Francis Egan
| honorific-suffix = O.F.M.
| title            = Bishop of Philadelphia
| image            = Michael Egan.jpg
| image_size       = 
| alt              = 
| caption          = 
| church           = 
| archdiocese      = 
| province         = [[Roman Catholic Archdiocese of Baltimore|Baltimore]]
| metropolis       = 
| diocese          = [[Roman Catholic Archdiocese of Philadelphia|Philadelphia]]
| see              = 
| elected          = 
| appointed        = April 8, 1808
| term             = 
| term_start       = October 28, 1810
| quashed          = 
| term_end         = July 22, 1814
| predecessor      = ''none''
| opposed          = 
| successor        = [[Henry Conwell]]
| other_post       = 
<!---------- Orders ---------->
| ordination       = 1785 or 1786
| ordinated_by     = 
| consecration     = October 28, 1810
| consecrated_by   = [[John Carroll (bishop)|John Carroll]]
| rank             = 
<!---------- Personal details ---------->
| birth_name       = 
| birth_date       = {{birth date|1761|9|29|mf=y}}
| birth_place      = [[Ireland]]
| death_date       = {{death date and age|1814|7|22|1761|9|29|mf=y}}
| death_place      = [[Philadelphia]], [[Pennsylvania]]
| buried           = 
| nationality      = 
| religion         = [[Roman Catholic]]
}}

'''Michael Francis Egan,''' [[Franciscan|O.F.M.]] (September 29, 1761 – July 22, 1814) was an Irish, later American [[prelate]] of the [[Roman Catholic Church]]. Born in Ireland in 1761, he joined the [[Franciscan|Franciscan Order]] at a young age. He served as a priest in Rome, Ireland, and Pennsylvania and became known as a gifted preacher. In 1808, Egan was appointed the first [[Roman Catholic Archdiocese of Philadelphia|Bishop of Philadelphia]], holding that position until his death in 1814.{{sfn|Friend|2010}} Egan's tenure as bishop saw the construction of new churches and the expansion of the Catholic Church membership in his diocese, but much of his time was consumed by disputes with the lay trustees of his [[pro-cathedral]], [[St. Mary's Roman Catholic Church (Philadelphia)|St. Mary's Church]] in [[Philadelphia]]. He died in Philadelphia, probably of [[tuberculosis]], in 1814.

==Early life and priesthood==
Michael Francis Egan was born in [[Ireland]] on September 29, 1761.{{sfn|Bransom|1990|p=12}} The exact location of his birth is uncertain. Early biographers believed Egan was possibly born in [[Galway]],{{sfn|Griffin|1893|pp=3–4}} though more recent scholarship suggests it was actually [[Limerick]].{{sfn|Friend|2010}}{{sfn|Ennis|1976|pp=63–64}} He joined the [[Order of Friars Minor]] (commonly known as the Franciscans) and studied at the [[Old University of Leuven]] and [[Charles University in Prague]].{{sfn|Friend|2010}} Egan received [[minor orders]], [[Subdeacon|subdiaconate]], and [[Deacon|diaconate]] at [[Mechelen]], in modern-day Belgium.{{sfn|Ennis|1976|p=64}} He was [[Holy orders|ordained]] a [[Priesthood (Catholic Church)|priest]], probably in Prague, in 1785 or 1786.{{sfn|Friend|2010}}{{sfn|Bransom|1990|p=12}} While studying on the continent, Egan became fluent in German.{{sfn|Ennis|1976|p=64}}

Egan advanced rapidly to positions of responsibility in the Franciscan order.{{sfn|Loughlin|1909}} He was appointed ''[[Custos (Franciscans)|custos]]'' ("guardian") of the province of [[Munster]] in Ireland in March 1787.{{sfn|Griffin|1893|p=4}} Later that year, he was also appointed ''custos'' of the [[Pontifical Irish College|Pontifical College]] at [[Sant’Isidoro a Capo le Case]], the home of Irish Franciscans in [[Rome]].{{sfn|Griffin|1893|p=4}} Egan remained there until 1790, when he returned to his native Ireland and was appointed ''custos'' of [[Ennis]]. He remained in Ireland until 1787 or 1788, when he may have made a visit to the United States.{{sfn|Griffin|1893|p=4}} After several more years as a missionary in Ireland, Egan came (or returned) to the United States in 1802.{{sfn|Loughlin|1909}}

==Priest in Pennsylvania==
Accepting an invitation from the Catholics near [[Lancaster, Pennsylvania]], Egan arrived in the [[United States]] in January 1802 to serve as assistant [[pastor]] to [[Louis de Barth]] at [[Basilica of the Sacred Heart of Jesus, Conewago|Conewago Chapel]] in Adams County.{{sfn|Griffin|1893|p=5}} When the state legislature sat in Lancaster that year, word of Egan's preaching abilities traveled back to [[Philadelphia]], and soon the parishioners of that city's [[St. Mary's Roman Catholic Church (Philadelphia)|St. Mary's Church]] petitioned Bishop [[John Carroll (bishop)|John Carroll]] of Baltimore to send Egan to them. (At that time, the Bishop of Baltimore had jurisdiction over the entire Catholic Church in the United States.){{sfn|Griffin|1893|pp=6–8}}

The following year, 1803, Egan became one of the pastors of St. Mary's Church at Philadelphia.{{sfn|Griffin|1893|pp=6–8}} The move coincided with a [[yellow fever]] outbreak in Philadelphia. Though less virulent than Philadelphia's famous [[Yellow fever epidemic of 1793|1793 outbreak]] of the disease, there were nonetheless many deaths, and Egan presided over many funerals that year—St Mary's had 77 interments between June and November 1803.{{sfn|Griffin|1893|p=9}} In 1804, Egan received permission to establish a province of Franciscans in the United States for the first time, independent of the Irish Franciscans who were then supervising the American mission.{{sfn|Griffin|1893|p=11}}{{sfn|Ennis|1976|p=66}} Two years later, a parishioner willed Egan some land along the [[Yellow Creek (Two Lick Creek)|Yellow Creek]] in [[Indiana County, Pennsylvania|Indiana County]], for the establishment of a Franciscan church.{{sfn|Ennis|1976|p=66}} Because of the Order's vows of poverty, Egan asked Carroll to hold the land in his name.{{sfn|Griffin|1893|pp=12–13}} Egan's dream was never realized, as he was unable to attract Franciscans from Europe to establish the planned church.{{sfn|Ennis|1976|p=67}}

Egan and the trustees of St. Mary's established a singing school in 1804, with the goal of improving the quality of the choir there.{{sfn|Griffin|1893|pp=17–19}} The following year was consumed by another yellow fever outbreak, and Egan joined John Rossiter, the pastor of another of Philadelphia's four Catholic churches, [[Old St. Joseph's Church|St. Joseph's]], in ministering to the sick.{{sfn|Griffin|1893|pp=20–22}} In 1806, they worked with the parishioners of a third church, [[Holy Trinity Catholic Church (Philadelphia)|Holy Trinity]], to found an [[orphanage]], as the problem of orphaned children had been made worse by the yellow fever deaths.{{sfn|Griffin|1893|pp=22–23}}

==Bishop of Philadelphia==
===Ordination===
[[File:Old St. Mary's Church.jpg|thumb|[[St. Mary's Roman Catholic Church (Philadelphia)|St. Mary's Church]] in Philadelphia served as Egan's [[pro-cathedral]].]]
The Catholic population in the United States was growing, and Bishop Carroll had for some time wished for his vast diocese to be divided into more manageable territories.{{sfn|Shea|1888|pp=617–622}} On April 8, 1808, [[Pope Pius VII]] granted Carroll's request, erecting four new [[Episcopal see|sees]] in the United States and elevating Baltimore to an archdiocese. Among the new sees was the [[Roman Catholic Archdiocese of Philadelphia|Diocese of Philadelphia]], which included the states of [[Pennsylvania]] and [[Delaware]] as well as the western and southern parts of [[New Jersey]].{{sfn|Shea|1888|pp=617–622}} Even before the diocese was created, Carroll had determined to recommend Egan for the post, writing to Rome that Egan "was truly pious, learned, religious, remarkable for his great humility, but deficient, perhaps, in firmness and without great experience in the direction of affairs".{{sfn|Griffin|1893|pp=23–24}}

Because of disruptions caused by the [[Napoleonic Wars]], the [[papal bull]] nominating Egan did not reach the United States until 1810.{{sfn|Loughlin|1909}} When it arrived, Egan traveled to [[St. Peter the Apostle Church|St. Peter's Pro-cathedral]] in Baltimore, where he was ordained bishop by Archbishop Carroll, assisted by [[Benedict Joseph Flaget]] and [[Jean-Louis de Cheverus]], who had been appointed to bishoprics but had not yet been consecrated.{{efn|Although three bishops are typically required for ordination, the Pope may issue a dispensation when co-consecrators are unavailable. See [http://www.vatican.va/archive/ENG1104/__P3O.HTM Canon 1014].}}{{sfn|Bransom|1990|p=12}} Egan chose St. Mary's to serve as his [[pro-cathedral]] in Philadelphia.{{sfn|Friend|2010}} Even before Egan's installation, Philadelphia Catholics began to raise funds to expand the church in accordance with its new prominence in the diocese.{{sfn|Kurjack|1953|p=207}} After their ordinations, the new bishops planned a council of the American church leadership for the near future; in fact, they did not meet until 1829, long after Egan's death.{{sfn|Ennis|1976|p=67}}

===Trusteeism dispute===
Egan's elevation to the episcopate worsened an existing conflict in the American church: the dispute over [[trusteeism]]. In Europe, the Church owned property and directly controlled its parishes through the clergy. In the United States, however, early Catholic churches were typically founded by [[Laity|laymen]] who purchased the property, erected the church buildings. The laypeople accordingly demanded some control over the administration of the parish, even after the arrival of clergy from Europe who, like Egan, held the traditional view of parish organization.{{sfn|Carey|1978|pp=357–358}} The dispute also had nationalist elements to it, as the heavily German parish of Holy Trinity resented the imposition of an Irish bishop instead of one of their own.{{sfn|Carey|1978|p=361}} When Holy Trinity's pastor left for a new assignment in [[Maryland]] in 1811, the trustees there were perturbed at Egan's temporary appointment of an Irish priest, Patrick Kenny, to lead the parish, until a German priest could be found (a German priest, Francis Roloff, was assigned the following year).{{sfn|Griffin|1893|p=58}}{{sfn|Ennis|1976|p=70}}

Egan's own research into the issue showed that the trustees had conveyed St. Mary's Church to the previous pastor, Father Robert Harding, and then to his heirs, but the trustees did not consider that property transfer to have extinguished their role in the church's leadership.{{sfn|Griffin|1893|pp=54–56}} By 1811, Egan's worsening health caused him to accept the assistance of two priests at St. Mary's, James Harold and his nephew, William Vincent Harold.{{sfn|Griffin|1893|pp=54–56}} Egan and the trustees became further embroiled in a dispute about clerical salaries, a situation possibly made worse by the decline in shipping income in the port city caused by the outbreak of the [[War of 1812]].{{sfn|Ennis|1976|p=68}} Egan also came to believe the Harolds were making the situation worse by taking pro-clergy positions that were more extreme than Egan's own and by the younger Harold's scheming to be named Egan's [[coadjutor bishop]].{{sfn|Griffin|1893|pp=68–70, 79}} He appealed to the trustees for compromise, and offered to bring his cousin (also a priest) over from Ireland to replace the elder Harold.{{sfn|Griffin|1893|pp=68–70, 79}} By 1813, Egan and the trustees had reconciled and together resolved to remove the Harolds, who agreed to resign later that year and relocate to England.{{sfn|Griffin|1893|pp=74–82}}

===Death and burial===
Although the main complaints between bishop and trustees were resolved, some salary disputes lingered into 1813.{{sfn|Griffin|1893|pp=87–96}} The conditions at St. Mary's worsened in 1814 with the election of new trustees who were more in conflict with Egan than the previous ones.{{sfn|Griffin|1893|pp=103–107}} Elsewhere in the diocese, Egan was more successful. In about 1811, he made his most extensive visitation of his diocese, travelling as far west as Pittsburgh after stopping in Lancaster and Conewago.{{sfn|Ennis|1976|p=69}} He continued to raise funds for the Catholic orphanage and opened a new parish, [[Sacred Heart Church (Trenton, New Jersey)|Sacred Heart]], in [[Trenton, New Jersey]], in 1813, bringing the total number of churches in the diocese to sixteen.{{sfn|Ennis|1976|p=70}}{{sfn|Griffin|1893|pp=97–99}}

Egan's health continued to decline, and he died on July 22, 1814.{{sfn|Shea|1888|p=661}} While 19th-century chroniclers suggest that it "may be said in all truth that Bishop Egan died of a broken heart",{{sfn|Shea|1888|p=661}} modern biographers believe his health troubles more closely resembled [[tuberculosis]].{{sfn|Friend|2010}}{{sfn|Ennis|1976|p=66}} Egan was buried in St. Mary's churchyard.{{sfn|Griffin|1893|p=112}} In 1869, after the construction of the [[Cathedral Basilica of Saints Peter and Paul (Philadelphia)|Cathedral Basilica of Saints Peter and Paul]] on [[Logan Circle (Philadelphia)|Logan Square]], his remains were removed there and reburied in a crypt along with those of his successor, Bishop [[Henry Conwell]].{{sfn|Griffin|1893|pp=126–127}} [[Conwell-Egan Catholic High School]] in [[Fairless Hills]], Pennsylvania, is named in honor of Egan and his successor.

==Notes==
{{notes}}

==References==
{{reflist | colwidth = 26em | refs = }}

==Sources==
{{refbegin}}
'''Books'''
* {{cite book
  | last = Bransom
  | first = Charles N.
  | title = Ordinations of U.S. Catholic Bishops, 1790–1989
  | year = 1990
  | publisher = United States Catholic Conference
  | location = Washington, D.C.
  | isbn = 1-55586-323-X
  | ref = harv
  }}
* {{cite book
  | last = Ennis
  | first = Arthur J.
  | editor-last = Connelly
  | editor-first = James F.
  | title = The History of the Archdiocese of Philadelphia
  | publisher = Unigraphics Incorporated
  | location = Wynnewood, Pennsylvania
  | date = 1976
  | pages = 63–112
  | chapter = Chapter Two: The New Diocese
  | oclc = 4192313
  | ref = harv
  }}
* {{cite book
  | last = Griffin
  | first = Martin I.J.
  | year = 1893
  | title = History of Rt. Rev. Michael Egan, D. D., First Bishop of Philadelphia
  | publisher = 
  | location = Philadelphia, Pennsylvania
  | url = https://archive.org/details/historyofrtrevmi01grif
  | oclc = 7637383
  | ref = harv
  }}
* {{cite encyclopedia
  | last = Loughlin
  | first = James
  | title = Michael Egan
  | encyclopedia = [[Catholic Encyclopedia]]
  | publisher = Robert Appleton Company
  | location = New York
  | url = http://www.newadvent.org/cathen/05324c.htm
  | year = 1909
  | ref = harv
  }}
* {{cite book
  | last = Shea
  | first = John Gilmary
  | year = 1888
  | title = History of the Catholic Church in the United States
  | volume = 2
  | publisher = D.H. McBride & Co.
  | location = Akron, Ohio
  | url = https://archive.org/details/historyofcatholi22shea
  | oclc = 3211384
  | ref = harv
  }}
'''Articles'''
* {{cite journal
  | last = Carey
  | first = Patrick
  | title = The Laity's Understanding of the Trustee System, 1785–1855
  | journal = The Catholic Historical Review
  | date = July 1978
  | volume= 64
  | issue = 3
  | pages = 357–376
  | jstor = 25020365
  | ref = harv
  }}
* {{cite journal
  | last = Friend
  | first = Christine
  | title = Philadelphia's First Bishop
  | journal = Philadelphia Archdiocesan Historical Research Center
  | date = February 2010
  | url = http://www.pahrc.net/philadelphias-first-bishop/
  | ref = harv
  }}
* {{cite journal
  | last = Kurjack
  | first = Dennis C.
  | title = St. Joseph's and St. Mary's Churches
  | journal = Transactions of the American Philosophical Society
  | date = 1953
  | volume= 43
  | issue = 1
  | pages = 199–209
  | jstor = 1005672
  | ref = harv
  }}
{{refend}}

{{s-start}}
{{s-rel|ca}}
{{s-new|diocese}}
{{s-ttl|title=[[Roman Catholic Archdiocese of Philadelphia|Bishop of Philadelphia]]|years=1808–1814}}
{{s-aft|after=[[Henry Conwell]]}}
{{s-end}}

{{Roman Catholic Archdiocese of Philadelphia|state=collapsed}}
{{authority control}}
{{DEFAULTSORT:Egan, Michael}}
[[Category:1761 births]]
[[Category:1814 deaths]]
[[Category:19th-century Roman Catholic bishops]]
[[Category:American Roman Catholic bishops]]
[[Category:American people of Irish descent]]
[[Category:Irish emigrants to the United States (before 1923)]]
[[Category:Clergy from Philadelphia]]
[[Category:Charles University in Prague alumni]]
[[Category:Burials at the Cathedral Basilica of Saints Peter and Paul, Philadelphia]]