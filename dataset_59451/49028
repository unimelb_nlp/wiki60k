{{Infobox medical person
| name = Barry Kerzin
| image = Barry Kerzin, American Professor of Medicine and Buddhist monk.gif
| image_size = 
| alt = 
| caption =
| birth_name = Barry Michael Kerzin
| birth_date = November 1, 1947
| birth_place = Hollywood, USA
| nationality = American
| citizenship = [[United States of America]]
| education = University of California at Berkeley (BA Philosophy) and University of Southern California (MD)
| occupation = Teacher, physician, and Buddhist monk
| known_for = Medical doctor to the Dalai Lama and teacher at the interface of Buddhism and medicine
| relations =
| signature = 
| profession = 
| field =
| work_institutions = Private practice (Ventura County, California)<br>[[University of Washington School of Medicine]] (Assistant Professor)<br>Private practice (charitable, [[Dharamsala]] India)<br>Human Values Institute (Japan)<br>Altruism in Medicine Institute (USA)<br>University of Hong Kong (Honorary)
| specialism = 
| research_field = 
| prizes =
| child = 
| module2 = 
}}

'''Barry Michael Kerzin''', M.D., (born November 1, 1947) is an American physician and Buddhist monk.  He has lived in [[Dharamsala]] since 1988 and serves as a personal physician to the [[14th Dalai Lama]], along with treating people in the local community, free of charge. Following his ordination as a monk by the Dalai Lama in the mid-2000s, he has travelled widely, teaching and offering workshops in which he blends Buddhist teaching and his medical training, emphasizing the spiritual and health benefits of meditation and compassion.  He has served as a research subject in neuroscience research into [[Brain activity and meditation|the effects of meditation on the brain]].

==Early life and education ==
Kerzin was born in Hollywood, California on November 1, 1947.<ref name=Ventura/><ref>Altruism in Medicine. [http://altruismmedicine.org/dr-barry-kerzin-a-personal-journey/ Dr Barry Kerzin: A Personal Journey]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }} Video: Month and day of birth and location at 3:29-3:35.</ref>  When he was young he read a book by [[D.T. Suzuki]]'s World of Zen, which sparked an interest in Buddhism.<ref name=caduc />   Starting at around six, he was plagued by questions of who he was and why he was here; they led him to join a philosophy club in high school and to switch to studying philosophy in college; he had started as a pre-med student.<ref>Altruism in Medicine. [http://altruismmedicine.org/dr-barry-kerzin-a-personal-journey/ Dr Barry Kerzin: A Personal Journey]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }} Video: 4:30-8:20.</ref>  He had wanted to become a doctor and did choose to continue on to medical school, because at the age of eleven he had a [[brain abscess]] that caused him to have seizures and fall into comas; it was eventually treated by a neurosurgeon with four brain surgeries over several years; the experience inspired him to become a doctor so that he could help other people.<ref name=caduc>{{cite web|author1=Yaya Huang |title=Mind the Gap —An Interview with Prof. Barry Kerzin |url=http://www.hkucaduceus.net/Volume%2045/Issue2/int1.html |publisher=HKU Medical Faculty 'Caduceus' magazine 2014 |accessdate=31 March 2015 |location=Hong Kong |deadurl=yes |archiveurl=https://web.archive.org/web/20150402163838/http://www.hkucaduceus.net/Volume%2045/Issue2/int1.html |archivedate=2 April 2015 |df= }}</ref>

Kerzin received BA in Philosophy from the [[University of California at Berkeley]] and in 1976 he received an MD degree from the [[University of Southern California]].<ref name=USCCN /><ref name=AMIbio/>

==Career==

Kerzin did his residency at [[Ventura County Medical Center]] and practiced family medicine in Ojai, California for seven years.<ref name=Ventura>{{cite news|last=Kim Lamb Gregory|title=Doctor/Tibetan monk returns to Ventura County to speak on mind, body|url=http://www.vcstar.com/news/a-physician-who-is-a-tibetan-monk-returns-to-to|newspaper=Ventura County Star|location=Ventura County, California|date=March 6, 2014|quote=Article says he is 66 years old. His birth year must be 1947 as he was born in November}}</ref>  His mother had died when he was 27, and just after he started working in Ojai, his wife was diagnosed with [[ovarian cancer]]. She died in 1983 and they had no children.<ref name=Ventura/>

He travelled in India, Sri Lanka, and Nepal for nearly a year, visiting various lamas.<ref>Altruism in Medicine. [http://altruismmedicine.org/dr-barry-kerzin-a-personal-journey/ Dr Barry Kerzin: A Personal Journey]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }} Video: 8:40 - 9:35</ref>

He then obtained an appointment as an Assistant Professor of Medicine at the [[University of Washington School of Medicine]] from late 1985 to early 1989.<ref name=Ventura/><ref>[https://in.linkedin.com/pub/barry-kerzin/b6/937/832?trk=seokp_posts_secondary_cluster_res_author_name Barry Kerzin's LinkedIn Profile]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }} Page accessed August 9, 2015.  Source used for dates.</ref>

In the mid-1980s, [[B. Alan Wallace]] and the [[Dharma Friendship Foundation]] coaxed a lama from Dharamsala, [[Gen Lamrimpa]], to come to Seattle for two years, and Kerzin served as his driver.<ref>Altruism in Medicine. [http://altruismmedicine.org/dr-barry-kerzin-a-personal-journey/ Dr Barry Kerzin: A Personal Journey]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }} Video: 9:30 - 10:15</ref>  In 1988 Gen Lamrimpa returned to India and Kerzin accompanied him, intending to take a six-month leave of absence from the University of Washington.<ref>Altruism in Medicine. [http://altruismmedicine.org/dr-barry-kerzin-a-personal-journey/ Dr Barry Kerzin: A Personal Journey]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }} Video: 15:25 - 17:00</ref>  He stayed in Dharamsala when his leave ended, and began practicing medicine there for the local people, the Dalai Lama and other Tibetan lamas.<ref name=Ventura/><ref name=USCCN>{{cite web|url=http://tfm.usc.edu/autumn-2013/class-notes-autumn-2013#1970s |publisher=USCTrojan Family |title=Barry Kerzin MD ’76 (MED) |accessdate=March 30, 2015 |deadurl=yes |archiveurl=https://web.archive.org/web/20150402152211/http://tfm.usc.edu/autumn-2013/class-notes-autumn-2013 |archivedate=April 2, 2015 |df= }}</ref><ref name=AMIbio/>  He also began studying Buddhism and meditation intensively, and 19 years after he moved there (in the mid-2000s), he was ordained a Buddhist monk by the Dalai Lama.<ref name=Ventura/><ref name=AMIbio/>  Throughout his career Kerzin has maintained his board certification with the [[American Board of Family Medicine]].<ref name=AMIbio>Altruism in Medicine Institute [http://altruismmedicine.org/about/founder-dr-barry-kerzin/ About Dr. Barry Kerzin] {{webarchive |url=https://web.archive.org/web/20150810131944/http://altruismmedicine.org/about/founder-dr-barry-kerzin/ |date=August 10, 2015 }} Page accessed August 9, 2015</ref>

After his ordination, he began traveling and teaching about the interface between [[medical science|modern medical science]] and [[Buddhism and psychology#Buddhism.27s phenomenological psychology|Buddhist psychology]] and [[Buddhist philosophy#Mahayana|philosophy]], with particular reference to [[altruism]] in medicine and [[secular ethics]] as well as [[Compassion#Buddhism|compassion]], [[Wisdom#Buddhism|wisdom]], [[Meditation#Buddhism|meditation]], death and dying and emotional hygiene; he has also participated in neuroscience research on the effect of meditation on the brain.<ref name=caduc /><ref name=AMIbio/>

[[File:Barry Kerzin meditating with EEG for neuroscience research.jpg|thumbnail|left|Kerzin meditating with EEG for neuroscience research]]
In the mid-2000s he served as a research subject in neuroscience research into [[Brain activity and meditation|the effects of meditation on the brain]] led by Richard J Davidson at the [[University of Wisconsin]].<ref>CNN. [http://edition.cnn.com/TRANSCRIPTS/0612/16/hcsg.01.html HOUSE CALL WITH DR. SANJAY GUPTA House Call with Dr. Sanjay Gupta. Encore Presentation: Happiness Cure] Aired December 16, 2006</ref>

Kerzin founded the Human Values Institute in Japan in 2010 after visiting there regularly starting in 2007; he serves as chairman of the organization.<ref name=HVI>Human Values Institute. [http://humanvaluesinstitute.org/purpose-methods/ HVI Purpose & Methods] {{webarchive |url=https://web.archive.org/web/20150816015612/http://humanvaluesinstitute.org/purpose-methods/ |date=August 16, 2015 }} Page accessed August 9, 2015</ref> The institute publishes books and instructional movies, gives lectures, leads workshops and meditation retreats, holds an annual symposium in Tokyo, and leads pilgrimages on the island of [[Shikoku#Traditions|Shikoku]]; the education focuses on healthy physical and emotional living and handling death compassionately.<ref name=HVI/>  He taught about the [[Heart Sutra]] at the [[Gokoku-ji|Gokokuji Temple]] in Tokyo shortly after the [[2011 Tōhoku earthquake and tsunami]].<ref>[http://issuu.com/dhardho/docs/thetibetpostinternational Tibet Post International March 32, 2011]. Tibetans and Japanese Hold Prayers for Victims of Tsunami. Page 1 and 6</ref>

He participated in a 2011 weeklong workshop organized by scientists at the [[Max Planck Institute for Human Cognitive and Brain Sciences]] in Leipzig, exploring the role compassion training has in changing human behavior and emotions.<ref>[http://www.cbs.mpg.de/depts/singer/WorkshopConf Workshop "How to Train Compassion" 20.-23. July 2011, Studio Olafur, Berlin]</ref>  The workshop led to a documentary film and a multimedia book to which Kerzin contributed two chapters.<ref>[http://altruismmedicine.org/wp-content/uploads/2015/03/Compassion_Web_2_Chapters.pdf Chapters 4 and 6] {{webarchive |url=https://web.archive.org/web/20150402101215/http://altruismmedicine.org/wp-content/uploads/2015/03/Compassion_Web_2_Chapters.pdf |date=April 2, 2015 }}</ref>

In 2013, Kerzin founded the Altruism in Medicine Institute in the US.<ref>[https://opencorporates.com/companies/us_ca/C3624572 Altruism in Medicine Institute in the Open Corporates Database] Page accessed August 9, 2015]</ref>

He had a visiting professorship at the Central University of Tibetan Studies, Varanasi, India in 2006.<ref name=HKUCV>HKU Centre of Buddhist Studies [http://www.buddhism.hku.hk/teaching_staff.htm Staff listing]. Page accessed August 9, 2015. [http://www.buddhism.hku.hk/staff_info/DrBarryKerzinCV2015.pdf Kerzin CV at HKU Last updated March 2015]. Page accessed August 9, 2015</ref> At the [[University of Hong Kong]] he was appointed 'Visiting Professor of Medicine' for 2014 and 2015 and was made an Honorary Professor at the university's Centre of Buddhist Studies in March 2015.<ref name=HKUCV/>  Kerzin is a fellow of the [[Mind & Life Institute]],<ref>Mind & Life Institute. [https://www.mindandlife.org/fellows-programs/ List of Fellows] Page accessed August 9, 2015</ref> which was initiated in 1985 to foster a dialogue between Buddhist scholars and Western scientists.<ref>Gay Watson, Beyond happiness: deepening the dialogue between Buddhism, psychotherapy and the mind sciences, Karnac Books, 2008, ISBN 1-85575-404-5, ISBN 978-1-85575-404-1, [https://books.google.com/books?id=80JNDfdTA2MC&lpg=PP1&pg=PA169#v=onepage&q&f=false p. 169]</ref><ref>Barinaga M. Buddhism and neuroscience. Studying the well-trained mind. Science. 2003 Oct 3;302(5642):44-6. PMID 14526055</ref>

==Works ==
* '''Publications''': Kerzin is the author of The Tibetan Buddhist Prescription for Happiness<ref>{{cite web|title=Tibetan Buddhist Prescription for Happiness|url=http://books.barrykerzin.org/|publisher=Human Values Institute, Tokyo|accessdate=31 March 2015}}</ref> published in Japanese and being translated into English and Chinese, and various book chapters.
* '''TEDx Talks''': He delivered a [[TED (conference)#TEDx|TEDx talk]], on Happiness in 2010<ref>{{cite web|author1=Barry Kerzin|title=Happiness|url=http://tedxtalks.ted.com/video/TEDxPhoenixville-Barry-Kerzin-H;Lifestyle|publisher=TEDx Phoenixville Salon|date=July 2010|format=Video}}</ref> and one in 2014 on Compassion and Anger Management<ref name=TEDx>{{cite web|title=Barry Kerzin at TEDx Taipei 2014|url=http://tedxtaipei.com/talks/2014-barry-kerzin/|publisher=TEDx Taipei|accessdate=31 March 2015|location=Taipei, Taiwan|format=Video}}</ref>
* '''Documentaries''': Prof Kerzin was featured on the 2006 U.S. [[PBS|Public Broadcasting Service]] documentary entitled The New Medicine.<ref>{{cite web|title=The New Medicine|url=http://www.pbs.org/thenewmedicine/|publisher=PBS|format=Radio programme|date=March 29, 2006}}</ref> This TV documentary received a largely negative review in the Wall Street Journal,<ref>{{cite news|author1=Dorothy Rabinowitz|title=The New Medicine|url=https://www.wsj.com/articles/SB114315337050506797|publisher=Wall Street Journal|date=March 24, 2006}}</ref> but a more positive one appeared in the New York Times.<ref>{{cite news|author1=Charles McGrath|title='The New Medicine' on PBS: Doctors Turn to the Mind for Healing|url=https://www.nytimes.com/2006/03/29/arts/television/29mcgr.html?_r=2&|publisher=New York Times|date=March 29, 2006}}</ref>
* Kerzin also featured in October 2015 in a [[PBS Newshour]] documentary "[http://www.pbs.org/newshour/bb/dalai-lamas-american-doctor-wants-compassion-medicine/ Dalai Lama’s American doctor wants more compassion in medicine]"

== External links ==
{{Commons category|Barry Kerzin}}
* [https://altruismmedicine.org/| Altruism in Medicine]

== References ==
{{reflist|25em}}

{{DEFAULTSORT:Kerzin, Barry}}
[[Category:1947 births]]
[[Category:Living people]]
[[Category:American Buddhist monks]]
[[Category:Buddhist monks]]
[[Category:Tibetan Buddhists from the United States]]
[[Category:Buddhist teachers]]
[[Category:Religion and science]]
[[Category:Meditation]]
[[Category:Consciousness researchers and theorists]]
[[Category:Health promotion]]
[[Category:Keck School of Medicine of USC alumni]]
[[Category:University of California, Berkeley alumni]]