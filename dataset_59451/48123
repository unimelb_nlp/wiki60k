{{EngvarB|date=April 2015}}
{{Use dmy dates|date=April 2015}}
{{Infobox musical artist 
| name                = Max George
| background          = solo_singer
| image               = File:Max George 2012.jpg
| caption             = George in 2012
| birth_name          = Max Albert George
| birth_date          = {{birth date and age|df=yes|1988|09|06}}
| birth_place         = [[Manchester]], United Kingdom
| instrument          = {{hlist|Bass|guitar|vocals}}
| genre               = {{hlist|[[Pop music|Pop]]|[[synthpop]]}} 
| occupation          = {{hlist|Singer|songwriter|actor}}
| years_active        = 2006–present
| label               = {{hlist|[[Geffen Records|Geffen]]|[[Island Records|Island]]|[[Mercury Records|Mercury]]|[[Virgin EMI Records|Virgin EMI]]}}
| associated_acts     = {{hlist|[[Avenue (band)|Avenue]]|[[The Wanted]]}}
}}

'''Max Albert George''' (born 6 September 1988) is a British singer, songwriter and actor, known for being a former member of [[boy band]], [[The Wanted]]. In 2013, George starred in the [[E!]] channel reality series ''[[The Wanted Life]]''.<ref>{{cite web|last=Garibaldi|first=Christina|url=http://www.mtv.com/news/1708314/the-wanted-life-show-max-george/|title='The Wanted Life' Is 'Not for My Mom's Eyes,' Max George Shares|publisher=[[MTV|www.mtv.com]]|date=31 May 2013}}</ref>

George started his career as a footballer, playing for [[Preston North End F.C.|Preston North End]]. After an injury ended his football career, he decided to pursue a music career. George made his singing debut with the band [[Avenue (band)|Avenue]]. After the group disbanded in 2009, George joined the newly formed band, The Wanted. In 2014, he landed his first acting role in the [[Glee (season 6)|sixth season]] of ''[[Glee (TV series)|Glee]]'' in the role of Clint (a bully), the leader of "Vocal Adrenaline".<ref>{{cite news|last=Evans|first=Denise|url=http://www.manchestereveningnews.co.uk/whats-on/film-news/watch-max-george-stars-glee-8578093|title=Watch: Max George stars in Glee|work=[[Manchester Evening News]]|agency=[[Trinity Mirror]]|publisher=manchestereveningnews.co.uk|date=4 February 2015|accessdate=4 February 2015|location=United Kingdom}}</ref>

== Early life ==
George was born on 6 September 1988 in [[Manchester]], England to Barbara George. He has an older brother, Jack, a visual effect coordinator. George studied at the [[Bolton School]].<ref>{{cite news|last=|first=|url=http://www.theboltonnews.co.uk/news/10640992.The_Wanted_closer_to_breaking_America/| publisher=theboltonnews.co.uk|title=The Wanted closer to breaking America|date=29 August 2013|accessdate=16 September 2014}}</ref>

== Music career ==

=== Avenue and ''The X Factor'' ===
{{main|Avenue (band)}}
George joined the four-piece boy band [[Avenue (band)|Avenue]] in 2005. After applying to ''[[The X Factor]]'' in 2006, the band auditioned for [[The X Factor (UK series 3)|the third series of the competition]] singing an [[a cappella]] version of the [[Will Young]] song, "[[Leave Right Now]]".  They were mentored by [[Louis Walsh]] after completing the bootcamp and judges' house portion.  Making it to the final 12, the band then signed a management contract with the series. After it was discovered they were already under contract with another management firm, they were disqualified from the show.<ref>{{cite web|last=|first=|url=http://www.realitytvworld.com/news/avenue-boy-band-kicked-off-simon-cowell-british-the-x-factor-show-1009953.php| publisher=realitytvworld.com|title=Avenue boy band kicked off Simon Cowell's British "The X Factor" show|accessdate=26 September 2006|date=16 September 2014}}</ref> Following the disqualification, they were signed to the Crown Music Management recording label and were awarded an album deal with Island Records.

In 2008, Avenue released their debut single, "Last Goodbye" and George appeared naked on the magazine cover of [[AXM]]. The single peaked when charting at number 50 on the [[UK Singles Chart]] and their plans to go on tour were cancelled. The group disbanded in 2009.<ref>{{cite web|last=Balls|first=David|url=http://m.digitalspy.co.uk/music/s103/the-x-factor/news/a151704/boyband-avenue-announce-split.html#~oPUXL5kSRgpOUA| publisher=digitalspy.co.uk|title=Boyband Avenue announce split|accessdate=3 April 2009|date=15 September 2014}}</ref>

=== The Wanted ===
{{main|The Wanted}}
In 2009, a mass audition was held by Jayne Collins to form a boy band, after successfully launching [[Parade (band)|Parade]] and [[The Saturdays]]. George auditioned and was selected as one of five members, along with Nathan Sykes, Siva Kaneswaran, Tom Parker and Jay McGuiness out of the thousands of others who auditioned. The band was formed and together they worked on their debut album before finding a perfect name for their band, [[The Wanted]]. Their debut album "[[All Time Low (song)|All Time Low]]" was released on 25 July 2010, debuted at number one on the [[UK Singles Chart]]. Their second single, "[[Heart Vacancy]]" was released on 17 October 2010. The song hit number two on the [[UK Singles Chart]], and at number 18 in Ireland. Their third single, "[[Lose My Mind (The Wanted song)|Lose My Mind]]" was released on 26 December 2010. The song peaked number 19 on the [[UK Singles Chart]], and peaked at number 30 in Ireland. In 2011, the single was released in Germany and in 2012, in the United States.<ref>{{cite web|last=Rovi|first=Andrew Leahey|url=http://www.billboard.com/artist/419401/wanted/biography?mobile_redirection=false| publisher=billboard.com|title=The Wanted|accessdate=|date=23 September 2014}}</ref> In 2013, in support of a crossover appeal to the American music market, the group starred in their own reality series on [[E!]].<ref>{{cite web|last=Garibaldi|first=Christina|url=http://www.mtv.com/news/1708314/the-wanted-life-show-max-george/|title='The Wanted Life' Is 'Not for My Mom's Eyes,' Max George Shares|publisher=[[MTV|www.mtv.com]]|date=6 February 2015}}</ref> The series, ''[[The Wanted Life]]'' only aired for one season.

The band announced their decision to break in January 2014.<ref>{{cite web|last=Wyatt|first=Daisy|url=http://www.independent.co.uk/arts-entertainment/music/news/the-wanted-split-up-boy-band-announce-break-9078311.html| publisher=independent.co.uk|title=The Wanted split up: Boy band announce break|accessdate=22 January 2014|date=25 September 2014}}</ref>

=== Solo artist ===
In January 2014, George revealed in an interview that he was signed as a solo artist by [[Scooter Braun]].<ref>{{cite web|last=Citizen|first=The|url=http://www.gloucestercitizen.co.uk/Nathan-Sykes-bandmate-Max-George-sparked-Wanted-s/story-20501879-detail/story.html| publisher=gloucestercitizen.co.uk|title=Nathan Sykes and bandmate Max George sparked The Wanted's split for solo careers|accessdate=26 January 2014|date=21 September 2014}}</ref>

== Other ventures ==

=== Modeling ===
In June 2013, George was named as the new face and spokesmodel for Buffalo denim's fall line. He starred in the campaign opposite Sports illustrated swimsuit model, [[Hannah Davis (model)|Hannah Davis]]. In November 2013, he did an underwear campaign for Buffalo again.<ref>{{cite web|last=|first=|url=http://www.proudprisoners.com/2013/11/max-george-models-underwear-for-buffalo.html?m=1| publisher=proudprisoners.com|title=Max George Models Underwear for Buffalo David Bitton @MaxTheWanted|accessdate=15 November 2013|date=29 September 2014}}</ref>

=== Acting career ===
George is signed with [[Creative Artists Agency]] since 2013.<ref>{{cite web|last=Shenton|first=Zoe|url=http://www.mirror.co.uk/3am/celebrity-news/max-george-wanted-split-singer-3098198| publisher=mirror.co.uk|title= Max George on The Wanted split: ''I wasn't dishonest, everything I said the boys already knew''|accessdate=31 January 2014|date=21 September 2014}}</ref> In June 2014, George revealed his decision to enroll in [[elocution]] classes, to help improve his pursuit of an acting career.<ref>{{cite web|last=|first=|url=http://www.express.co.uk/news/showbiz/481993/Max-George-in-elocution-classes-to-improve-chances-of-acting-jobs| publisher=express.co.uk|title=Max George in elocution classes to improve chances of acting jobs|accessdate=12 June 2014|date=16 September 2014}}</ref> In September 2014, it was confirmed that he had joined the cast of ''[[Glee (TV series)|Glee]]'' in the role of Clint, after [[Mark Salling]] shared a photo of him on Twitter hanging out with Max on the set of the final season of the US comedy drama.<ref>{{cite web|last=Grubbs|first=Jefferson|url=http://www.bustle.com/articles/58874-glee-newbie-clint-is-played-by-max-george-from-the-wanted-so-hes-basically-perfect-to|title='Glee' Newbie Clint Is Played By Max George From The Wanted, So He's Basically Perfect To Lead Vocal|publisher=www.bustle.com|date=16 January 2015}}</ref><ref>{{cite web|last=H|first=Emily|url=http://primetime.unrealitytv.co.uk/glee-star-confirms-wanteds-max-george-will-final-season/| publisher=unrealitytv.co.uk|title=Glee Spoilers: The Wanted's Max George CONFIRMED for final season!|accessdate=20 September 2014|date=21 September 2014}}</ref><ref>{{cite web|last=Heath|first=Olivia|url=http://m.reveal.co.uk/showbiz-celeb-gossip/news/a597772/the-wanteds-max-george-hangs-out-with-mark-salling-on-glee-set.html| publisher=reveal.co.uk|title=The Wanted's Max George hangs out with Mark Salling on Glee set|accessdate=14 September 2014|date=19 September 2014}}</ref> He made his first on screen appearance on 9 January 2015.

On 2 February 2015, it was announced that George had joined the cast of [[Bear Grylls]]' new TV survival series titled ''[[Bear Grylls: Mission Survive]]''. The show is about eight celebrities pushed to the limit by Grylls. The team will be making their dangerous journey through the jungle, where they will be competing for 12-day survival mission.<ref>{{cite news|last=Campbell|first=Tina|url=http://metro.co.uk/2015/02/02/from-max-george-to-emilia-fox-bear-grylls-mission-survive-celebrity-lineup-confirmed-5045507/|title=From Max George to Emilia Fox: Bear Grylls Mission Survive celebrity lineup confirmed|work=[[Metro (British newspaper)|Metro]]aagency=[[DMG Media]]|date=2 February 2015|accessdate=2 February 2015}}</ref>

== Personal life ==
George was engaged to British soap opera actress [[Michelle Keegan]] after meeting her at one of his concerts in December 2010. They split up in 2012.<ref>{{cite web|last=|first=|url=http://www.mtv.co.uk/the-wanted/news/michelle-keegan-and-max-george-have-split-up| publisher=mtv.co.uk|title=Michelle Keegan And Max George Have Split Up|accessdate=21 July 2012|date=24 September 2014}}</ref> In 2013, he began dating Sports Illustrated model [[Nina Agdal]]. They broke up in February 2014. In October 2014, he confirmed being in a relationship with [[Miss Oklahoma]] contestant Carrie Baker.<ref>{{cite web|last=Eames|first=Tom|url=http://m.digitalspy.co.uk/showbiz/news/a603951/the-wanteds-max-george-dating-miss-oklahoma-contestant.html#~oU5cnNMEFqbtOs|title=The Wanted's Max George dating Miss Oklahoma contestant| publisher=digitalspy.co.uk|accessdate=16 October 2014|date=29 October 2014}}</ref><ref>{{cite web|last=Hind|first=Katie|url=http://www.mirror.co.uk/3am/celebrity-news/max-george-moves-hollywood-good-4584912| publisher=mirror.co.uk|title=Max George moves to Hollywood for GOOD to be with new girlfriend Carrie Baker|accessdate=7 November 2014|date=7 November 2014}}</ref>

== Filmography ==
{| class="wikitable sortable"
|+Television
! Year
! Title
! Role
! class="unsortable" | Notes
|-
| rowspan="2" | 2013
| ''[[Chasing the Saturdays]]''
| Himself
| Guest role; #DeepFriedSats
|-
| ''[[The Wanted Life]]''
| Himself
| 7 episodes
|-
| rowspan="2" | 2015
| ''[[Glee (TV series)|Glee]]''
| Clint (antagonist)
| 6 episodes
|-
| ''[[Bear Grylls: Mission Survive]]''
| Himself
| 2 episodes
|}

== References ==
<!--- See http://en.wikipedia.org/wiki/Wikipedia:Footnotes on how to create references using<ref></ref> tags, these references will then appear here automatically -->
{{Reflist|30em}}

== External links ==
* {{IMDb name|4013729}}
* {{Facebook|therealmaxgeorge}}

{{The Wanted}}

{{DEFAULTSORT:George, Max}}
[[Category:21st-century English male actors]]
[[Category:English male singers]]
[[Category:English pop singers]]
[[Category:English songwriters]]
[[Category:English male television actors]]
[[Category:Living people]]
[[Category:Musicians from Manchester]]
[[Category:People educated at Bolton School]]
[[Category:1988 births]]
[[Category:English footballers]]