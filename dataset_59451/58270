{{Use dmy dates|date=January 2013}}
{{Infobox journal
| title = Journal of Management
| cover = 
| editor = Patrick M. Wright <ref>http://www.sagepub.com/journals/Journal201724/boards</ref>
| discipline = [[Management]]
| former_names = 
| abbreviation = J. Manag.
| publisher = [[Sage Publications]]
| country = 
| frequency = Bimonthly
| history = 1975-present
| openaccess = 
| license = 
| impact = 6.071 <ref>http://jom.sagepub.com/</ref>
| impact-year = 2014
| website = http://www.sagepub.com/journals/Journal201724/title
| link1 = http://jom.sagepub.com/content/current
| link1-name = Online access
| link2 = http://jom.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 311545277
| LCCN = 79644536
| CODEN = JOMADO
| ISSN = 0149-2063
| eISSN = 1557-1211
}}
The '''''Journal of Management''''' is a bimonthly [[peer-reviewed]] [[academic journal]] published by [[SAGE Publications]] and covering research on all aspects of [[management]] as well as the related field of [[industrial and organizational psychology]].<ref>Zickar, M. J., & Highhouse, S. (2001, April). Measuring the prestige of journals in industrial-organizational psychology. The Industrial-Organizational Psychologist, 38(4). Available online: http://www.siop.org/tip/backissues/TipApr01/03Zicker.aspx</ref>{{cn|date=May 2014}}{{OR|date=May 2014}} Special issues containing [[review article]]s only are published biannually in January and July. It is an official journal of the [[Southern Management Association]]. The journal was established in 1975 and the [[editor-in-chief]] is Patrick M. Wright ([[University of South Carolina]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Current Contents]]/Social & Behavioral Sciences
* [[EBSCO Information Services|EBSCO Business Source]]
* [[International Bibliography of the Social Sciences]]
* [[LexisNexis]]
* [[PAIS International]]
* [[PsycINFO]]
* [[Scopus]]
* [[Social Sciences Citation Index]]
* [[VINITI Database RAS]]
}}
According to the ''[[Journal Citation Reports]]'' the journal has a 2014 [[impact factor]] of 6.071.<ref name=WoS>{{cite book |year=2015 |chapter=Journal of Management |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
*{{Official website|http://www.sagepub.com/journals/Journal201724/title}}

[[Category:Publications established in 1975]]
[[Category:Business and management journals]]
[[Category:SAGE Publications academic journals]]
[[Category:Bimonthly journals]]
[[Category:English-language journals]]