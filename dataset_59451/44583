{{Use mdy dates|date=July 2014}}
{{Infobox person
|name        = Mary Lynn Rajskub
|image       = Mary Lynn Rajskub at 24 finale 2009 crop.jpg
|caption     = Rajskub at a screening of the ''[[24 (TV series)|24]]'' season 7 finale in 2009
|birth_date  = {{birth date and age|1971|6|22}}
|birth_place = [[Detroit, Michigan]], U.S.
|yearsactive = 1995–present
|occupation  = Actress, comedian
|spouse      = Matthew Rolph (m. 2009–present)
|children = 1, Sam Ecker
}}

'''Mary Lynn Rajskub''' ({{IPAc-en|ˈ|r|aɪ|s|k|ə|b}}; born June 22, 1971) is an American actress and comedian, best known for portraying [[Chloe O'Brian]] in the [[Fox Broadcasting Company|Fox]] action thriller series ''[[24 (TV series)|24]]''.

==Early life==
Rajskub was born in [[Detroit]], [[Michigan]] and raised in nearby [[Trenton, Michigan|Trenton]], the daughter of Betty and Tony Rajskub. She is of Irish, Polish & Czechoslovak descent.<ref>{{cite web|url=http://www.trentontrib.com/trenton-was-first-stage-for-rajskubs-talents.html|title=Trenton was first stage for Rajskub’s talents|work=Trenton Trib}}</ref> Her father is a [[pipefitter]] of [[Czechs|Czech]] descent,{{citation needed|date=July 2011}} and her mother worked as a [[pharmacist]]'s assistant.<ref>{{cite web|url=http://www.filmreference.com/film/0/Mary-Lynn-Rajskub.html |title=Mary Lynn Rajskub Biography (1971-) |publisher=Filmreference.com |date= |accessdate=July 31, 2010}}</ref> Rajskub played the clarinet in a school band and portrayed Frenchie in the musical ''[[Grease (musical)|Grease]]''. One of her childhood inspirations was the television series ''[[Moonlighting (TV series)|Moonlighting]]''. She later moved to [[Los Angeles]], [[California]], where she worked as a waitress in a [[Hard Rock Cafe]] and a ticket-taker at the Beverly Center movie theater.
[[File:The cast of 24 2009.jpg|thumb]]

==Career==
Rajskub's first part was as an [[Oompa-Loompa]] in a community theater production of ''[[Willy Wonka & the Chocolate Factory]]'', and her first starring role was [[Raggedy Ann]].<ref>{{cite web|url=http://www.usmagazine.com/moviestvmusic/news/mary-lynn-rajskub-25-things-you-dont-know-about-me-2010211 |title=25 Things You Don't Know About Me: Mary Lynn Rajskub |publisher=UsMagazine.com |date=January 21, 2010 |accessdate=July 31, 2010}}</ref> In 1996 she appeared in a music video for the song [[The Good Life (Weezer song)#Music video|The Good Life]] by [[Weezer]].

Her most notable role is [[Counter Terrorist Unit|CTU]] [[systems analyst]] [[Chloe O'Brian]] on ''[[24 (TV series)|24]]'', which she joined in 2003 at the start of the show's third season.  Her character was a hit with viewers and critics and was one of the few cast members to return in the show's fourth season.  After being a regular guest star for two seasons, Rajskub became a main cast member in the show's fifth season. By the end of the series, she was the lead female, with top billing second only to [[Kiefer Sutherland]]. Her character also had the honor of saying the final words of the series in the season 8 series finale. Rajskub and Sutherland appeared briefly as their ''24'' characters in a 2007 episode, "[[24 Minutes]]", of the Fox animated series ''[[The Simpsons]]''. In August 2013, it was announced that she would reprise her Chloe O'Brian role in the 2014 limited series ''[[24: Live Another Day]]''.<ref>{{cite web |url=http://feedproxy.google.com/~r/Tvbythenumbers/~3/VDMNV5_yXFI/ |title=Mary Lynn Rajskub Clocks in For '24: Live Another Day' |work=[[Zap2It]] |publisher=FOX press release |author=Kondolojy, Amanda |date=August 1, 2013 |accessdate=August 1, 2013}}</ref>

Rajskub was one of the original cast members of ''[[Mr. Show]]''.<ref name="Sanders">{{cite video |people=Featurette: Interview with Mary Lynn Rajskub|year=2007|title=Not Just the Best of The Larry Sanders Show (Disc 3)|url=http://www.amazon.com/Just-Best-Larry-Sanders-Show/dp/B000MTFDB0|format=Box set, Closed-captioned, Color, Dolby, DVD, Full Screen, Subtitled, NTSC|medium=DVD|publisher=[[Sony Pictures Home Entertainment]]|accessdate=February 11, 2012|asin=B000MTFDB0}}</ref> She appeared in [[Kelsey Grammer]]'s ''[[The Sketch Show]]'' on [[Fox Broadcasting Company|Fox Television]], ''[[The King of Queens]]'' as a character named "Priscilla". ("The Secret of Management"), and in numerous films including  ''[[Mysterious Skin]]'', ''[[Legally Blonde 2]]'', ''[[Sweet Home Alabama (film)|Sweet Home Alabama]]'', ''[[Dude, Where's My Car?]]'', ''[[Man on the Moon (film)|Man on the Moon]]'', ''[[Punch-Drunk Love]]'', ''[[The Anniversary Party]]'', ''[[Firewall (film)|Firewall]]'', ''[[Little Miss Sunshine]]'', music videos for [[Beck]], [[Weezer]] and [[Sheryl Crow]], as well as portraying a blind girl in the film ''[[Road Trip (film)|Road Trip]]''.

[[Image:Mary Lynn Rajskub at GBK 2011.jpg|thumb|220|right|Rajskub (right) in June 2011]]

Rajskub was part of a comic duo (with [[Karen Kilgariff]]) called Girls Guitar Club. In 2006, she made a cameo appearance in "Partings", the [[Gilmore Girls Season 6|6th season]] finale of ''[[Gilmore Girls]]'', where she played a [[troubadour]] looking for her big break. (Rajskub had previously appeared on ''Gilmore Girls'' as the female lead in ''A Film by Kirk'', a [[short film]] made by the character [[Kirk Gleason]].) She has volunteered as an actress with the [[Young Storytellers Program]]. She has an educational background as a painter, having attended the [[San Francisco Art Institute]].

Rajskub has been nominated twice for a [[Screen Actors Guild Award]]; once in 2005, and again in 2007 for Outstanding Performance by an Ensemble in a Drama Series. She guest starred on ''[[Flight of the Conchords (TV series)|Flight of the Conchords]]'' episode "[[Prime Minister (Flight of the Conchords)|Prime Minister]]" as Karen, an Art Garfunkel fanatic. She guest starred as "Gail the Snail" in an episode of ''[[It's Always Sunny in Philadelphia]]'' titled "The Gang Gives Frank an Intervention", and reprised the role in the ninth season finale, "The Gang Squashes Their Beefs". In 2009, she also appeared in the film ''[[Julie & Julia]]'' as Sarah, one of [[Julie Powell]]'s close friends. In 2010, Rajskub performed stand-up on ''[[John Oliver's New York Stand Up Show]]''. In June 2010, she appeared in the "Lovesick" episode during the second season of the [[USA Network|USA]] series ''[[Royal Pains (season 2)|Royal Pains]]''.

From July through October 2010, she performed in her solo show, ''Mary Lynn Spreads Her Legs'', at the [[Steve Allen Theater]] in Los Angeles. Reviewer F. Kathleen Foley of the ''[[Los Angeles Times]]'' wrote "that cheerfully vulgar title sums up the overall tone, which is often breezily obscene".<ref>{{cite web|author=F. Kathleen Foley |url=http://latimesblogs.latimes.com/culturemonster/2010/07/mary-lynn-spreads-her-legs-at-the-steve-allen.html |title=Theater review: 'Mary Lynn Spreads Her Legs' at the Steve Allen Theater |publisher=Latimesblogs.latimes.com |date=July 2, 2010 |accessdate=September 14, 2010}}</ref>  The show, written by Rajskub with help from director/developer Amit Itelman, was inspired by Rajskub’s experiences with pregnancy, childbirth and early motherhood.

In January 2011, Rajskub guest starred in the episode "[[Our Children, Ourselves]]" on the [[Modern Family (season 2)|second season]] of [[American Broadcasting Company|ABC]]'s ''[[Modern Family]]''. In the fall of 2011, Rajskub appeared in the short-lived sitcom ''[[How to Be a Gentleman]].'' Also in 2011, Rajskub's webseries, ''Dicki'', began airing on My Damn Channel.<ref>{{cite web|url=http://www.mydamnchannel.com/Dicki/Dicki_Episodes/Episode1MakingFriends_7789.aspx |title=DICKI - The Official Website |publisher=Mydamnchannel.com |date=November 3, 2011 |accessdate=December 5, 2013}}</ref> ''Dicki'' is based on a number of people that Rajskub grew up with in and around Michigan. The title character is a 40-year-old woman who lives at home with her parents, makes crafts, and takes her art seriously. ''Dicki'' has been one of My Damn Channel's most successful web series to date. The first season concluded in November 2011, but a second season is currently in development. Rajskub performed in the June 2012 edition of ''[[Don't Tell My Mother! (Live Storytelling)]]'', a monthly showcase in which celebrities share true stories they would never want their mothers to know.<ref>{{cite web |url=http://www.laweekly.com/2012-06-21/calendar/don-r-tell-my-mother-post-father-s-day-special/ |title=Don't Tell My Mother: Post Father's Day Special |deadurl=no |accessdate=July 9, 2012}}</ref> She currently hosts a podcast on the Nerdist Network called ''[[Kickin' it Mary Lynn Style]]''.{{when|date=October 2013}}

In 2013, Rajskub appeared in the Netflix semi-original series ''[[Arrested Development (TV series)|Arrested Development]]'' in a silent yet well-received<ref>{{cite web|last=Lovelace|first=Steve|title=Five Best New Arrested Development Characters|url=http://steve-lovelace.com/10-best-new-arrested-development-characters/}}</ref> role as Heartfire, a character Rajskub has said "speak[s] from the heart, but do[es]n't use any words".<ref>{{cite web|last=Eggerton|first=CHris|title=Arrested Development's Mary Lynn Rajskub on playing George Sr.'s New Assistant|url=http://www.hitfix.com/news/arrested-developments-mary-lynn-rajskub-on-playing-george-sr-s-new-assistant-watch#CL5zEaJv18AVLeLV.99}}</ref>
In the same year Rajskub also appeared in the web series ''[[All Growz Up with Melinda Hill]]''.<ref>{{cite web|last=Carrie|first=Stephanie|title=Steph’s LA Weekly Feature Series – Melinda Hill’s Romantic Encounters|url=http://tangledwebwewatch.com/2013/07/17/stephs-la-weekly-feature-series-melinda-hills-romantic-encounters/}}</ref>

Rajskub appeared on [[Ken Reid (comedian)|Ken Reid]]'s ''TV Guidance Counselor'' podcast on March 27, 2015.

In August 2016, Rajskub appeared at the [[Edinburgh Festival Fringe]]' in 24 Hours With Mary Lynn Rajskub, well-received among fans and critics <ref>{{cite web |url=http://www.theatresmart.com/edinburgh-fringe-reviews-2016.html |title=24 Hours With Mary Lynn Rajskub |publisher=TheatreSmart | date=August 6, 2016 |accessdate=August 8, 2016}}</ref>

==Personal life==
Rajskub dated [[David Cross]], who introduced her to ''[[Mr. Show]]'',<ref name="Sanders" /> and left the show when they broke up after the end of the second season. Afterwards, she dated music producer [[Jon Brion]] for five years until they broke up in the fall of 2002. Rajskub later dated comedian [[Duncan Trussell]].<ref>{{cite web|last=Rogan|first=Joe|title=Joe Rogan Experience #202|url=http://blog.joerogan.net/archives/4559|accessdate=April 5, 2012}}</ref>

Rajskub met personal trainer Matthew Rolph when he approached her after one of her comedy sets. They began dating, and she became pregnant three months later.<ref>Friday May 2, 2014 [https://www.youtube.com/watch?v=ZnE1RO2Zsr0 ''The Wendy Williams Show'']</ref> Their son, Valentine Anthony, was born in 2008.<ref>{{cite web|author= |url=http://celebrity-babies.com/2009/02/01/real-life-mirrors-stage-life-for-mary-lynn-rajskub/ |title=Real Life Mirrors Stage Life for Mary Lynn Rajskub |publisher=Celebrity-babies.com |date=February 1, 2009 |accessdate=July 31, 2010}}</ref> Rajskub married Rolph on August 1, 2009 in an impromptu wedding in [[Las Vegas]].<ref>{{cite web|url=http://www.tvshark.com/read/?art=arc3872 |title='24's Mary Lynn Rajskub Married To Trainer Matthew Rolph |publisher=TV Shark |date=August 4, 2009 |accessdate=July 31, 2010}}</ref>

Rajskub is bisexual and discussed her past relationships with women in an article for AfterEllen.com.<ref>{{cite web|url=http://www.afterellen.com/i-still-think-megan-fox-is-hot-mary-lynn-rajskub-talks-about-being-bisexual/11/2012/|title="I still think Megan Fox is hot": Mary Lynn Rajskub talks about being bisexual - AfterEllen|work=AfterEllen}}</ref>

== Filmography==

===Film===
[[Image:Mary Lynn Rajskub4.jpg|thumb|right|upright|Rajskub at [[The Heart Truth]] Fashion Show, February 1, 2008]]
{| class="wikitable sortable"
|-
! Year
! Title
! Role
! Notes
|-
|1997
| ''[[Who's the Caboose?]]''
| Cheeseball
|
|-
| 1998
| ''[[The Thin Pink Line]]''
| Suzy Smokestack
|
|-
| 1999
| ''[[Magnolia (film)|Magnolia]]''
| Janet
| Voice 
|-
| 1999
| ''[[Man on the Moon (film)|Man on the Moon]]''
| Friday's Mary
|
|-
| 2000
| ''[[Road Trip (film)|Road Trip]]''
| Blind Brenda
|
|-
| 2000
| ''[[Dude, Where's My Car?]]''
| Zelmina
|
|-
| 2000
| ''[[Sunset Strip (film)|Sunset Strip]]''
| Eileen
|
|-
| 2001
| ''[[Storytelling (film)|Storytelling]]''
| Melinda
|
|-
| 2001
| ''[[The Anniversary Party]]''
| Mary-Lynn
|
|-
| 2002
| ''[[Punch-Drunk Love]]''
| Elizabeth
|
|-
| 2002
| ''[[Sweet Home Alabama (film)|Sweet Home Alabama]]''
| Dorothea
|
|-
| 2002
| ''[[Run Ronnie Run]]''
| Herself
|
|-
| 2003
| ''[[Legally Blonde 2: Red, White & Blonde]]''
| Reena Giuliani
|
|-
| 2003
| ''[[Claustrophobia (2003 film)|Claustrophobia]]''
| Grace
|
|-
| 2004
| ''[[Mysterious Skin]]''
| Avalyn Friesen
|
|-
| 2004
| ''[[Helter Skelter (2004 film)|Helter Skelter]]
| [[Lynette Fromme|Lynette "Squeaky" Fromme]]
|
|-
| 2006
| ''[[Firewall (film)|Firewall]]''
| Janet Stone
|
|-
| 2006
| ''[[Little Miss Sunshine]]''
| Pageant Assistant Pam
|
|-
| 2006
| ''[[Grilled (film)|Grilled]]''
| Renee
|
|-
| 2007
| ''Humble Pie''
| Peggy Orbison
|
|-
| 2008
| ''[[Sunshine Cleaning]]''
| Lynn
|
|-
| 2009
| ''[[Julie & Julia]]''
| Sarah
|
|-
| 2012
| ''[[Safety Not Guaranteed]]''
| Bridget
|
|-
|2013
| ''[[The Kings of Summer]]''
| Captain Davis
|
|-
|2015
| ''[[Sex, Death and Bowling]]''
| Kim Wells
|
|-
| 2017
| ''[[Wilson (2017 film)|Wilson]]''
| Jodie
|
|-
|}

===Television===
{| class="wikitable sortable"
|-
! Year
! Title
! Role
! Notes
|-
| 1995
| ''[[Mr. Show]]''
| Various Characters
| 10 episodes
|-
| 1996–1998
| ''[[The Larry Sanders Show]]''
| Mary Lou Collins
| 18 episodes
|-
| 1998
| ''[[NewsRadio]]''
| Waitress
| 1 episode
|-
| 1999
| ''[[Shasta McNasty]]''
| Diana
| 1 episode
|-
| 1999–2000
| ''[[Veronica's Closet]]''
| Chloe
| 15 episodes
|-
| 2001
| ''[[Just Shoot Me]]''
| Penny
| 1 episode
|-
| 2002
| ''[[Gilmore Girls]]''
|Girlfriend in Kirk's film
| 1 episode
|-
| 2002
| ''[[The King of Queens]]''
| Priscilla Stasna
| 1 episode
|-
| 2003–2010
| ''[[24 (TV series)|24]]''
| [[Chloe O'Brian]]
| 125 episodes
|-
| 2004
| ''[[JAG (TV series)|JAG]]''
| CI Administrative Assistant
| 1 episode
|-
| 2005
| ''[[Kelsey Grammer Presents The Sketch Show|The Sketch Show]]''
| Various Characters
|
|-
| 2006
| ''[[Gilmore Girls]]''
| Town Troubadour 
| 1 episode
|-
| 2007
| ''[[Human Giant]]''
| Mindy
| 2 episodes
|-
| 2007
| ''[[The Simpsons]]''
| [[Chloe O'Brian]]
| Episode: "[[24 Minutes]]"
|-
| 2008
| ''[[The Middleman]]''
| Dr. Gibbs
| 1 episode
|-
| 2009
| ''[[Flight of the Conchords (TV series)|Flight of the Conchords]]''
| Karen
| Episode: "[[Prime Minister (Flight of the Conchords)|Prime Minister]]"
|-
| 2009–2013
| ''[[It's Always Sunny in Philadelphia]]''
| Gail the Snail
| 2 episodes
|-
| 2010
| ''[[Royal Pains]]''
| Blake
| 1 episode
|-
| 2010
| ''[[The Benson Interruption]]''
| Herself
| 1 episode
|-
| 2010
| ''[[Stargate Universe]]''
| Extra
| 1 episode
|-
| 2011
| ''[[Modern Family]]''
| Tracy
| Episode: "[[Our Children, Ourselves]]"
|-
| 2011
| ''[[Raising Hope]]''
| Tanya
| 1 episode
|-
| 2011
| ''[[How to Be a Gentleman]]''
| Janet
| 9 episodes
|-
| 2012
| ''[[The L.A. Complex]]''
| Herself
| 1 episode
|-
| 2012
| ''[[Dirty Work (TV series)|Dirty Work]]''
| Roxy
| 3 episodes
|-
| 2012
| ''[[Chelsea Lately]]''
| Herself
|
|-
| 2012
| ''[[The Burn with Jeff Ross]]''
| Herself
|
|-
| 2012
| ''[[Mash Up (TV series)|Mash Up]]''
| Herself
|
|-
| 2012
|''[[Grey's Anatomy]]''
| Marion Steiner
| 1 episode
|-
| 2013
| ''[[The Mentalist]]''
| Susie Hamplin
| 1 episode
|-
| 2013
| ''[[New Girl (TV series)|New Girl]]''
| Peg
| 1 episode
|-
| 2013
| ''[[Arrested Development (TV series)|Arrested Development]]''
| Heartfire
| Season 4
|-
| 2013–2014
| ''[[2 Broke Girls]]''
| Bebe
| 5 episodes
|-
| 2014
| ''[[Californication (TV series)|Californication]]''
| Goldie
| 4 episodes
|-
| 2014
| ''[[24: Live Another Day]]''
| [[Chloe O'Brian]]
| All 12 episodes (limited series)
|-
| 2014–2016
| ''[[@midnight]]''
| Herself
| 3 episodes
|-
| 2014 
| ''[[Talking Dead]]''
| Herself
| 1 episode — S04E03
|-
| 2015 
| ''[[Maron (TV series)|Maron]]''
| Herself
| 1 episode — S03E03
|-
| 2015–2016
| ''[[Brooklyn Nine-Nine]]''
| Genevieve Mirren-Carter
| 4 episodes
|-
| 2015
| ''Highston''
| Jean Liggetts
| 1 episode
|-
| 2015
| ''[[W/ Bob & David]]''
| Chef Krissie
| 1 episode
|-
| 2016
| ''[[The Girlfriend Experience (TV series)|The Girlfriend Experience]]''
| Erin Roberts
| 10 episodes
|-
| 2016 
| ''[[Take My Wife (2016 TV series)|Take My Wife]]''
| Mary Lynn
| 1 episode
|-
| 2016
| ''[[Those Who Can't]]''
| Summer
| 3 episodes
|-
| 2016
| ''[[Dream Corp LLC]]''
| Patient #046
| 1 episode
|-
| 2016
| ''[[Drunk History]]''
| [[Cherry Sisters|Effie Cherry]]
| 1 episode
|}

==References==
{{reflist|2}}

== External links ==
{{commons category|Mary Lynn Rajskub}}
* {{IMDb name|707476|Mary Lynn Rajskub}}
* [http://www.afewmomentswith.com/ Mary Lynn Rajskub Interview on Fox News Radio]
* [http://effinfunny.com/mary-lynn-rajskub Stand-up comedy video]
* [http://mydamnchannel.com/Dicki/ ''Dicki''], a series of wepisodes written by and starring Mary Lynn Rajskub

{{Authority control}}

{{DEFAULTSORT:Rajskub, Mary Lynn}}
[[Category:1971 births]]
[[Category:Actresses from Michigan]]
[[Category:American film actresses]]
[[Category:American stand-up comedians]]
[[Category:American television actresses]]
[[Category:American television writers]]
[[Category:Living people]]
[[Category:People from the Greater Los Angeles Area]]
[[Category:American women comedians]]
[[Category:American people of Czech descent]]
[[Category:American people of Irish descent]]
[[Category:American people of Polish descent]]
[[Category:People from Dearborn, Michigan]]
[[Category:20th-century American actresses]]
[[Category:21st-century American actresses]]
[[Category:Bisexual actresses]]