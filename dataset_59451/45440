{{Infobox military person
|name=Josef Kraft
|birth_date=8 February 1921
|death_date={{death date and age|1994|10|16|1921|2|8|df=y}}
|image=File:Josef Kraft.jpg
|birth_place=[[Vienna]]
|death_place=[[Fürstenfeldbruck]]
|allegiance={{flag|Nazi Germany}}
|branch={{Luftwaffe}}
|serviceyears=1940–45
|rank=[[Hauptmann]]
|unit=[[Nachtjagdgeschwader 4|NJG 4]]<br/>[[Nachtjagdgeschwader 5|NJG 5]]<br/>[[Nachtjagdgeschwader 6|NJG 6]]<br/>[[Nachtjagdgeschwader 1|NJG 1]]
|commands=12./Nachtjagdgeschwader 1
|battles=[[World War II]]
*[[Defense of the Reich]]
|awards=[[Knight's Cross of the Iron Cross with Oak Leaves]]
|laterwork=}}

'''Josef Kraft''' (8 February 1921 – 16 October 1994) was a [[Luftwaffe]] [[night fighter]] [[fighter ace|ace]] and recipient of the [[Knight's Cross of the Iron Cross| Knight's Cross of the Iron Cross with Oak Leaves]] during [[World War II]].  The Knight's Cross of the Iron Cross and its higher grade Oak Leaves was awarded to recognise extreme battlefield bravery or successful military leadership.  Kraft claimed 56 aerial victories.<ref group="Note">For a list of Luftwaffe night fighter aces see ''[[List of German World War II night fighter aces]]''.</ref>

==Aerial victory claims==
Kraft was credited with 56 nocturnal aerial victories, four of which on the [[Eastern Front (World War II)|Eastern Front]], claimed in 129 combat missions.{{sfn|Obermaier|1989|p=76}}

<center>
{| class="wikitable plainrowheaders" style="text-align:right;"
|-
! colspan="6" | Chronicle of aerial victories
|-
!scope="col"| Victory
!scope="col" style="width:150px"| Date
!scope="col"| Time
!scope="col"| Type
!scope="col"| Location
!scope="col"| Serial No./Squadron No.
|-
! colspan="6" | – 6./''Nachtjagdgeschwader'' 5 –
|-
| 1
| 28 August 1943
| 01:20
| [[Avro Lancaster|Lancaster]]{{sfn|Foreman|Parry|Matthews|2004|p=107}}
| {{Convert|22|km|mi|abbr=on}} west [[Nuremberg]]
|
|-
| 2
| 1 September 1943
| 00:47
| Lancaster{{sfn|Foreman|Parry|Matthews|2004|p=109}}
| {{Convert|25|km|mi|abbr=on}} south-southwest [[Berlin]]
|
|-
| 3
| 1 September 1943
| 00:55
| [[Short Stirling|Stirling]]{{sfn|Foreman|Parry|Matthews|2004|p=109}}
| east Berlin
|
|}
</center>

==Awards==
* [[Honour Goblet of the Luftwaffe|Honour Goblet of the ''Luftwaffe'']] (''Ehrenpokal der Luftwaffe'') on 1 May 1944 as ''[[Leutnant]]'' and pilot{{sfn|Patzwall|2008|p=124}}{{refn|According to Obermaier on 31 March 1944.|group="Note"}}
* [[Front Flying Clasp of the Luftwaffe|Front Flying Clasp of the ''Luftwaffe'']] in Gold (18 February 1945)
* [[Iron Cross]] (1939)
** 2nd Class (1 September 1943){{sfn|Thomas|1997|p=400}}
** 1st Class (5 November 1943){{sfn|Thomas|1997|p=400}}
* [[German Cross]] in Gold on 23 July 1944 as ''[[Oberleutnant]]'' in the 8./''Nachtjagdgeschwader'' 6{{sfn|Patzwall|Scherzer|2001|p=249}}
* [[Knight's Cross of the Iron Cross with Oak Leaves]]
** Knight's Cross on 30 September 1944 as ''Oberleutnant'' and pilot in the II./''Nachtjagdgeschwader'' 6{{sfn|Fellgiebel|2000|p=271}}{{refn|According to Scherzer as pilot in the 7./''Nachtjagdgeschwader'' 6.{{sfn|Scherzer|2007|p=469}}|group="Note"}}
** 838th Oak Leaves on 17 April 1945 as ''[[Hauptmann]]'' and ''[[Staffelkapitän]]'' of the 12./''Nachtjagdgeschwader'' 1{{sfn|Scherzer|2007|p=469}}{{sfn|Fellgiebel|2000|p=102}}

==Notes==
{{reflist|group="Note"}}

==References==

===Citations===
{{Reflist|25em}}

===Bibliography===
{{Refbegin}}
* {{Cite book
  |last=Fellgiebel
  |first=Walther-Peer
  |authorlink=Walther-Peer Fellgiebel
  |year=2000
  |origyear=1986
  |title=Die Träger des Ritterkreuzes des Eisernen Kreuzes 1939–1945 — Die Inhaber der höchsten Auszeichnung des Zweiten Weltkrieges aller Wehrmachtteile
  |trans_title=The Bearers of the Knight's Cross of the Iron Cross 1939–1945 — The Owners of the Highest Award of the Second World War of all Wehrmacht Branches
  |language=German
  |location=Friedberg, Germany
  |publisher=Podzun-Pallas
  |isbn=978-3-7909-0284-6
  |ref=harv
}}
* {{Cite book
  |last1=Foreman
  |first1=John
  |last2=Parry
  |first2=Simon
  |last3=Matthews
  |first3=Johannes
  |year=2004
  |title=Luftwaffe Night Fighter Claims 1939–1945
  |trans_title=
  |language=English
  |location=Walton on Thames
  |publisher=Red Kite
  |isbn=978-0-9538061-4-0
  |ref=harv
}}
* {{Cite book
  |last=Obermaier
  |first=Ernst
  |year=1989
  |title=Die Ritterkreuzträger der Luftwaffe Jagdflieger 1939 – 1945
  |trans_title=The Knight's Cross Bearers of the Luftwaffe Fighter Force 1939 – 1945
  |language=German
  |location=Mainz, Germany
  |publisher=Verlag Dieter Hoffmann
  |isbn=978-3-87341-065-7
  |ref=harv
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D.
  |last2=Scherzer
  |first2=Veit
  |year=2001
  |title=Das Deutsche Kreuz 1941 – 1945 Geschichte und Inhaber Band II
  |trans_title=The German Cross 1941 – 1945 History and Recipients Volume 2
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-45-8
  |ref=harv
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D.
  |year=2008
  |title=Der Ehrenpokal für besondere Leistung im Luftkrieg
  |trans_title=The Honor Goblet for Outstanding Achievement in the Air War
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-08-3
  |ref=harv
}}
* {{Cite book
  |last=Scherzer
  |first=Veit
  |year=2007
  |title=Die Ritterkreuzträger 1939–1945 Die Inhaber des Ritterkreuzes des Eisernen Kreuzes 1939 von Heer, Luftwaffe, Kriegsmarine, Waffen-SS, Volkssturm sowie mit Deutschland verbündeter Streitkräfte nach den Unterlagen des Bundesarchives
  |trans_title=The Knight's Cross Bearers 1939–1945 The Holders of the Knight's Cross of the Iron Cross 1939 by Army, Air Force, Navy, Waffen-SS, Volkssturm and Allied Forces with Germany According to the Documents of the Federal Archives
  |language=German
  |location=Jena, Germany
  |publisher=Scherzers Militaer-Verlag
  |isbn=978-3-938845-17-2
  |ref=harv
}}
* {{Cite book
  |last=Thomas
  |first=Franz
  |year=1997
  |title=Die Eichenlaubträger 1939–1945 Band 1: A–K
  |trans_title=The Oak Leaves Bearers 1939–1945 Volume 1: A–K
  |language=German
  |location=Osnabrück, Germany
  |publisher=Biblio-Verlag
  |isbn=978-3-7648-2299-6
  |ref=harv
}}
{{Refend}}

==External links==
*[http://www.lexikon-der-wehrmacht.de/Personenregister/K/KraftJ.htm Lexikon der Wehrmacht]

{{Knight's Cross recipients of NJG 6}}
{{Knight's Cross recipients of NJG 1}}
{{Top German World War II night fighter aces}}
{{Subject bar
| portal1=Aviation
| portal2=Biography
| portal3=Military of Germany
| portal4=World War II
}}

{{DEFAULTSORT:Kraft, Josef}}
[[Category:1921 births]]
[[Category:1994 deaths]]
[[Category:Luftwaffe pilots]]
[[Category:Austrian military personnel of World War II]]
[[Category:German World War II flying aces]]
[[Category:Recipients of the Gold German Cross]]
[[Category:Recipients of the Knight's Cross of the Iron Cross with Oak Leaves]]
[[Category:People from Vienna]]