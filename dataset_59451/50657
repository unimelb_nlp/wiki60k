{{Infobox journal
| title = Australasian Physical & Engineering Sciences in Medicine
| cover =
| editor = Martin Caon
| former_name = Australasian Physical Sciences in Medicine
| discipline = [[Medical physics]]
| abbreviation = Australas. Phys. Eng. Sci. Med.
| publisher = [[Springer Science+Business Media]]
| country =
| frequency = Quarterly
| history = 1977–present
| openaccess = [[Hybrid open access journal|Hybrid]]
| impact = 0.882
| impact-year = 2014
| website = https://www.springer.com/biomed/journal/13246
| link2 = http://link.springer.com/journal/volumesAndIssues/13246
| link2-name = Online archive
| JSTOR =
| OCLC = 60622736
| LCCN =
| CODEN = AUPMDI
| ISSN = 0158-9938
| eISSN = 1879-5447
}}
'''''Australasian Physical & Engineering Sciences in Medicine''''' is a quarterly [[peer-reviewed]] [[medical journal]] covering research on [[medical physics]] and [[biomedical engineering]].<ref name="APESM">{{Cite web
 | title = Australasian Physical and Engineering Sciences in Medicine
 | publisher = [[Springer Science+Business Media|Springer]]
 | url = https://www.springer.com/biomed/journal/13246
 | accessdate = 2015-11-18}}
</ref> It is the official journal of the [[Australasian College of Physical Scientists and Engineers in Medicine]].

The journal was first published in 1977 as ''Australasian Physical Sciences in Medicine'', obtaining its current name in 1980. It has been published by [[Springer Science+Business Media]] since 2010. The journal replaced the earlier ''Australasian Bulletin of Medical Physics'' and ''Australasian Newsletter of Medical Physics'', which was established in December 1959.

The Kenneth Clarke Journal Prize is awarded annually for the best paper published in the journal in the previous year.

==Abstracting and indexing==
The journal is abstracted and indexed in:
* [[Chemical Abstracts Service]]<ref name=CASSI>{{cite web|url=http://cassi.cas.org/search.jsp |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2015-11-18 }}{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>
* [[Index Medicus]]/[[MEDLINE]]/[[Pubmed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/8208130 |title=Australasian Physical & Engineering Sciences in Medicine |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=2015-11-18}}</ref>
* [[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2015-11-25}}</ref>

According to the ''[[Journal Citation Reports]]'' it has a 2014 [[impact factor]] of 0.882.<ref name=WoS>{{cite book |year=2015 |chapter=Australasian Physical & Engineering Sciences in Medicine |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{reflist}}

==External links==
*{{Official website|https://www.springer.com/biomed/journal/13246}}

{{DEFAULTSORT:Australasian Physical and Engineering Sciences in Medicine}}
[[Category:Medical physics journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1977]]