<!-- FAIR USE of The Aspern Papers.JPG: see image description page at http://en.wikipedia.org/wiki/Image:The Aspern Papers.JPG for rationale -->
{{Infobox book  | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name           = The Aspern Papers
| image          = The Aspern Papers 1st ed.jpg
| border         = yes
| caption        = First edition title page
| author         = [[Henry James]]
| country        = United Kingdom, United States
| language       = English
| genre          = [[Novella]]
| publisher      = Macmillan and Co., [[London]], [[New York City]]
| release_date   = London: 29 September 1888<br>New York City: 10 November 1888
| media_type     = Print
| pages          = London: volume one, 239; volume two, 258<br>New York City: 290<br>Both editions also included the stories ''Louisa Pallant'' and ''The Modern Warning''
| isbn           = 
|dewey           = 813.4
}}
'''''The Aspern Papers''''' is a [[novella]] written by [[Henry James]], originally published in ''[[The Atlantic|The Atlantic Monthly]]'' in 1888, with its first book publication later in the same year. One of James's best-known and most acclaimed longer tales, ''The Aspern Papers'' is based on the letters [[Percy Bysshe Shelley]] wrote to [[Mary Shelley]]'s stepsister, [[Claire Clairmont]], who saved them until she died. Set in [[Venice]], ''The Aspern Papers'' demonstrates James's ability to generate suspense while never neglecting the development of his [[fictional character|characters]].

== Plot summary ==
A nameless narrator goes to [[Venice]] to find Juliana Bordereau, an old lover of Jeffrey Aspern, a famous and now dead American [[poetry|poet]]. The narrator presents himself to the old woman as a prospective lodger and is prepared to court her niece Miss Tita (renamed ''Miss Tina'' in later editions), a plain, somewhat naive spinster, in hopes of getting a look at some of Aspern's letters and other papers kept by Juliana. Miss Tita had denied the existence of any such papers in a letter to the narrator and his publishing partner, but he believes she was dissembling on instructions from Juliana. The narrator eventually discloses his intentions to Miss Tita, who promises to help him.

Later, Juliana offers to sell a [[portrait miniature]] of Aspern to the narrator for an exorbitant price. She doesn't mention Jeffrey Aspern's name, but the narrator still believes she possesses some of his letters. When the old woman falls ill, the narrator ventures into her room and gets caught by Juliana as he is about to rifle her desk for the letters. Juliana calls the narrator a "[[publishing]] scoundrel" and collapses. The narrator flees, and when he returns some days later, he discovers that Juliana has died. Miss Tita hints that he can have the Aspern letters if he [[marriage|marries]] her.

Again, the narrator flees. At first he feels he can never accept the proposal, but gradually he begins to change his mind. When he returns to see Miss Tita, she bids him farewell and tells him that she has burned all the letters one by one. The narrator never sees the precious papers, but he does send Miss Tita some money for the miniature portrait of Aspern that she gives him.

== Major themes ==

James (a very private man) examines the conflicts involved when a [[biography|biographer]] seeks to pry into the intimate life of his subject. James paints the nameless narrator of ''The Aspern Papers'' as, in Juliana's words, a "publishing scoundrel", but also generates sympathy for the narrator as he tries to work the papers loose from Juliana, who is presented as greedy, domineering and unappealing.

The story unwinds into the double climax of Juliana's discovery of the narrator about to break into her desk, and Miss Tita's revelation that she has destroyed the papers. Miss Tita is ashamed of her marriage proposal to the narrator, but James implies that she does exactly the right thing by depriving him of the papers. In a way, she develops into the true [[hero]]ine of the story.

== Critical evaluation ==
{{POV section|date=November 2014}} 
James thought so highly of this story that he put it first in volume 12 of The ''New York Edition'', ahead of even ''[[The Turn of the Screw]]''. [[Critic]]s have almost unanimously agreed with him about the tale's superb quality. Leon Edel wrote, "The story moves with the rhythmic pace and tension of a mystery story; and the double climax ... gives this tale ... high drama".<ref>Edel, Leon (1960). [https://books.google.com/books?id=QUJ1bNk7u2gC&pg=PA27 ''Henry James''], p. 27. University of Minnesota Press.</ref>

The central characters are all fully realized, and James describes Venice so lovingly that the city almost becomes a character in its own right, a crumbling, beautiful, mysterious place where the incredible becomes real and the strange is almost commonplace.{{original research inline|date=November 2013}} Critics have disagreed about the narrator's guilt and Miss Tita's complex motives, but few deny that James has presented the pair with masterful completeness.

The theme of an editor or literary biographer's search for hitherto secret information about an author was used later by, amongst others, [[Somerset Maugham]] in "Cakes and Ale" (loosely based on the life of the once popular British novelist [[Hugh Walpole]]), [[Penelope Lively]] in "According to Mark", [[A.S. Byatt]] in ''[[Possession (Byatt novel)|Possession]]'' and [[Alan Hollinghurst]] in ''[[The Stranger's Child]]''. Most significantly, James' close friend, [[Edith Wharton]], used this theme as the subject of her first novel, ''[[The Touchstone]]''.

== Text versions ==
''The Aspern Papers'' was first published in three parts in March–May 1888 editions of ''[[The Atlantic Monthly]]'', and published in book form in London and New York later in the same year. It was subsequently revised, with the addition of a Preface and changes including "Miss Tita" being renamed to "Miss Tina", for the 1908 ''[[New York Edition]]''.<ref>Cornwell, Neil. 17 January 2006. [http://www.litencyc.com/php/sworks.php?rec=true&UID=1559 "The Aspern Papers"]. ''The Literary Encyclopedia''. Accessed 17 August 2008</ref>

== Film, play and opera versions ==
* The 1947 film ''[[The Lost Moment]]'' was based on ''The Aspern Papers''. It starred [[Susan Hayward]] as Miss Tina and [[Robert Cummings]] as the narrator.
* In 1959, ''The Aspern Papers'' was adapted for the stage by [[Michael Redgrave]] and successfully produced at the [[Queen's Theatre]] in London's West End, with Redgrave and [[Flora Robson]] in the lead roles. A [[Broadway (Manhattan)|Broadway]] production followed in 1962 starring [[Maurice Evans (actor)|Maurice Evans]] and [[Wendy Hiller]]. The play has been revived a number of times since the original production.
* In 1974, London Weekend Television (ITV) made ''Affairs of the Heart'', a collection of seven dramatisations of stories by Henry James. The third episode, retitled "Miss Tita", was a fairly close adaptation of ''The Aspern Papers''. It starred Margaret Tyzack as Miss Tita, Beatrix Lehmann as Juliana Bordereau, and John Carson as Charles Faversham, the narrator and would-be editor.
* In 1984 the play was produced at the Theatre Royal, Haymarket, London. The cast included [[Vanessa Redgrave]] playing Miss Tina, the lead, and Wendy Hiller playing Juliana. [[Christopher Reeve]] played the male lead, Henry Jarvis. Vanessa Redgrave was given the Olivier Award for Best Actress in a revival.
* The 1991 film ''Els Papers d'Aspern'', directed by [[Jordi Cadena]], starring [[Silvia Munt]] 
* The 1985 film ''[[Aspern (film 1985)|Aspern]]'', directed by [[Eduardo de Gregorio]], starring [[Jean Sorel]], [[Bulle Ogier]] and [[Alida Valli]]
* In 1988 the [[Dallas Opera]] presented the world premiere of [[Dominick Argento|Dominick Argento's]] opera ''[[The Aspern Papers (opera)|The Aspern Papers]]''.
* In 2002, Les Papiers d'Aspern, scenic adaptation by Jean Pavans, staged by Jacques Lassalle, produced by the Comédie-Française.  
* On 26 April 2010, [[BBC Radio 4]] began broadcasting an abridged audio version of ''The Aspern Papers'', in their ''[[Book at Bedtime]]'' slot. It was read by [[Samuel West]].
* The 2010 film ''The Aspern Papers'', directed by [[Mariana Hellmund]], starring Judith Roberts and [[Brooke Smith (actress)|Brooke Smith]], Felix d'alviella,Joan Juliet Buck, Lourdes Brito and, Marvin Huise.

== References ==
{{Reflist}}<!--added under references heading by script-assisted edit-->

==Sources==
* ''Tales of Henry James: The Texts of the Tales, the Author on His Craft, Criticism'' edited by Christof Wegelin and Henry Wonham (New York: W.W. Norton & Company, 2003) ISBN 0-393-97710-2
* ''The Tales of Henry James'' by [[Edward Wagenknecht]] (New York: Frederick Ungar Publishing Co., 1984) ISBN 0-8044-2957-X

==External links==
*{{Wikisource-inline}}
*{{Commonscat-inline}}
* [http://cdl.library.cornell.edu/cgi-bin/moa/pageviewer?coll=moa&root=/moa/atla/atla0061/&tif=00304.TIF&view=50&frames=1 Original magazine text of ''The Aspern Papers'' (1888)]
* [http://www.gutenberg.org/etext/211 First book text of ''The Aspern Papers'' (1888)]
* [http://www.henryjames.org.uk/prefaces/text12.htm Author's preface to the ''New York Edition'' text of ''The Aspern Papers'' (1908)]
* {{librivox book | title=The Aspern Papers | author=Henry JAMES}}

{{Henry James}}

{{DEFAULTSORT:Aspern Papers, The}}
[[Category:1888 novels]]
[[Category:Short stories by Henry James]]
[[Category:American novellas]]
[[Category:British novellas]]
[[Category:Novels first published in serial form]]
[[Category:Works originally published in The Atlantic (magazine)]]
[[Category:Novels set in Venice]]
[[Category:19th-century American novels]]
[[Category:British novels adapted into films]]
[[Category:American novels adapted into films]]
[[Category:19th-century British novels]]