{{good article}}
{{Infobox University Boat Race
| name= 83rd Boat Race
| winner =Cambridge
| margin =  2 and 1/2 lengths
| winning_time=  19 minutes 26 seconds
| date= 21 March 1931
| umpire = John Houghton Gibbon<br>(Cambridge)
| prevseason= [[The Boat Race 1930|1930]]
| nextseason= [[The Boat Race 1932|1932]]
| overall =42&ndash;40
| reserve_winner= 
| women_winner = 
}}

The '''83rd Boat Race''' took place on 21 March 1931. Held annually, the Boat Race is a [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]].  In a race umpired by the former rower John Houghton Gibbon, Cambridge won by two and a half lengths in a time of 19 minutes 26 seconds.  The victory took the overall record to 42&ndash;40 in their favour.  It was the first race for which [[John Snagge]] provided a radio commentary for the [[BBC]].

==Background==
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 20 August 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  The race was first held in 1829, and since 1845 has taken place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 20 June 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref><ref>{{Cite web | url = http://theboatraces.org/the-course | title = The Course| accessdate = 24 July 2014 | publisher = The Boat Race Company Limited}}</ref>  The rivalry is a major point of honour between the two universities; it is followed throughout the United Kingdom and, as of 2014, broadcast worldwide.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford&ndash;Cambridge Boat Race crew|date=6 April 2014|publisher=[[CBC News]] |url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=20 August 2014}}</ref>  Cambridge went into the race as reigning champions, having won the [[The Boat Race 1930|1930 race]] by two lengths, and led overall with 41 victories to Oxford's 40 (excluding the [[The Boat Race 1877|"dead heat" of 1877]]).<ref name=results>{{Cite web | url = http://theboatraces.org/results| publisher = The Boat Race Company Limited | title = Boat Race – Results| accessdate = 25 August 2014}}</ref><ref>{{Cite web | url= http://theboatraces.org/classic-moments-the-1877-dead-heat | publisher = The Boat Race Company Limited | title = Classic moments – the 1877 dead heat | accessdate =20 August 2014}}</ref>

Cambridge were coached by R. W. M. Arbuthnot (who had rowed for Cambridge four times between 1909 and 1912), J. A. MacNabb (who had rowed in the [[The Boat Race 1924|1924 race]]) and P. H. Thomas (a four-time Blue between 1902 and 1905).  Oxford's coaches were A. V. Campbell (who took part in the [[The Boat Race 1922|1922]] and [[The Boat Race 1925|1925 races]]), [[Stanley Garton]] (who had rowed three times between 1909 and 1911) and P. C. Mallam (a Dark Blue from 1921 to 1924 inclusive).<ref>Burnell, pp. 110&ndash;111</ref>  The race was umpired by former Cambridge rower John Houghton Gibbon who had participated in the [[The Boat Race 1899|1899]] and [[The Boat Race 1900|1900 races]].<ref>Burnell, p. 49, 105</ref>

The rowing correspondent for ''The Times'' stated that the crews were "well-matched",<ref>{{Cite news | title = Well-matched crews | work = [[The Times]] | date = 21 March 1931 | page =13 | issue =45777}}</ref> while ''The Manchester Guardian''{{'}}s correspondent predicted a "gruelling struggle".<ref>{{Cite news | title = Gruelling struggle likely | work = [[The Manchester Guardian]] | date = 21 March 1931 | page = 11}}</ref>  It was the first time [[John Snagge]] provided a radio commentary for the [[BBC]].<ref>{{Cite web | url = http://www.independent.co.uk/news/people/obituary-john-snagge-1344505.html | date = 28 March 1996 | title = Obituary: John Snagge | work=[[The Independent]] | first = Leonard | last = Miall |author-link=Leonard Miall }}</ref>  He would go on to commentate for the corporation every year up to and including the [[The Boat Race 1980|1980 race]].<ref>{{Cite web | url = https://www.theguardian.com/media/2004/nov/26/bbc.radio |first= John | last = Plunkett | date = 26 November 2004 | work = [[The Guardian]] | accessdate = 22 February 2015 | title = BBC loses Boat Race radio rights}}</ref>

==Crews==
The Cambridge crew weighed an average of 12&nbsp;[[Stone (unit)|st]] 4.625&nbsp;[[Pound (mass)|lb]] (78.1&nbsp;kg), {{convert|1.125|lb|kg|1}} per rower more than their opponents.  Oxford saw three rowers return to their crew with Boat Race experience, including D. E. Tinne who was making his third consecutive appearance.  Cambridge's crew included five rowers who had participated in the event prior to this year, including their [[stroke (rowing)|stroke]] T. A. Brocklebank who was also rowing for the third year in a row.<ref name=burn73>Burnell, p. 73</ref>  All of the participants in the race were registered as British.<ref>Burnell, p. 39</ref>
{| class="wikitable"
|-
! rowspan="2" scope="col| Seat
! colspan="3" scope="col| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
! colspan="3" scope="col| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]] ||W. L. Garstang ||  [[Trinity College, Oxford|Trinity]] || 11 st 2 lb || [[David Haig-Thomas|D. Haig-Thomas]] || [[Lady Margaret Boat Club]] || 11 st 4.5 lb
|-
| 2 || G. M. L. Smith ||  [[Brasenose College, Oxford|Brasenose]] || 11 st 11 lb || [[Walter Prideaux (rower)|W. A. Prideaux]] || [[Trinity College, Cambridge|3rd Trinity]] || 12 st 6 lb
|-
| 3 ||  D. E. Tinne (P) || [[University College, Oxford|University]] || 12 st 4 lb || R. H. H. Symonds || [[Lady Margaret Boat Club]] || 11 st 12.5 lb
|-
| 4 || C. M. Johnston  || [[Brasenose College, Oxford|Brasenose]] || 12 st 9 lb || G. Gray || [[Queens' College, Cambridge|Queens']] || 13 st 5 lb
|-
| 5 || R. A. J. Poole ||  [[Brasenose College, Oxford|Brasenose]] || 13 st 2 lb || [[Farn Carpmael|P. N. Carpmael]] || [[Jesus College, Cambridge|Jesus]] || 13 st 0 lb
|-
| 6 || [[Lewis Clive|L. Clive]] ||[[Christ Church, Oxford|Christ Church]] || 13 st 2.5 lb || [[Harold Rickett|H. R. N. Rickett]] || [[Trinity College, Cambridge|3rd Trinity]] || 12 st 10 lb
|-
| 7 || W. D. C. Erskine-Crum || [[Christ Church, Oxford|Christ Church]] || 12 st 1.5 lb || [[Charles Sergel|C. J. S. Sergel]] || [[Clare College, Cambridge|Clare]] || 12 st 7 lb
|-
| [[Stroke (rowing)|Stroke]] || R. W. G. Holdsworth  ||[[Brasenose College, Oxford|Brasenose]] || 11 st 10.5 lb || T. A. Brocklebank (P) || [[Trinity College, Cambridge|3rd Trinity]] || 11 st 6 lb
|-
| [[Coxswain (rowing)|Cox]] || E. R. Edmett || [[Worcester College, Oxford|Worcester]] || 8 st 7 lb || [[John Ranking|J. M. Ranking]]  ||[[Pembroke College, Cambridge|Pembroke]] || 6 st 13 lb
|-
!colspan="7"|Source:<ref name=burn73>Burnell, p. 73</ref><br>(P) &ndash; boat club president<ref>Burnell, pp. 50, 52</ref>
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]] along which the Boat Race is contested]]
Oxford won the toss and elected to start from the Middlesex station, handing the Surrey side of the river to Camridge.  In wind gusting from the south-west, the umpire Gibbons started the race<!-- AT X&nbsp;P.M. -->.  Cambridge took an early lead, although they were [[stroke rate|out-rated]] by the Dark Blues.  The boats were close, with oars nearly touching, for the first two minutes of the race, before the Light Blue cox steered to avoid a collision.  By the end of Fulham Wall, Cambridge held a half-length lead although Oxford kept in touch, and even reduced the deficit to a quarter of a length by the time the crews passed the Mile Post.<ref name=again>{{Cite news | title = Cambridge's Boat-Race again | first = E. P. |last= Evans | work = [[The Manchester Guardian]] | page = 11 | date = 23 March 1931}}</ref>

By the Crab Tree pub, Cambridge's [[stroke (rowing)|stroke]] Brocklebank made his first [[Glossary of rowing terms#The commands|push for ten strokes]] to which Oxford responded in kind.  A second push from the Light Blues however saw them pull away and lead by one and a quarter lengths as they passed below [[Hammersmith Bridge]].  As both crews enountered a headwind, they dropped to 29 strokes per minute, but their styles were markedly different: Cambridge were lively while Oxford laboured. By Chiswick Steps the lead was two and a half lengths and three by [[Barnes Railway Bridge|Barnes Bridge]].  The Dark Blues pushed hard in the final stretches and slowly reduced Cambridge's lead until Brocklebank put one final spurt in at [[Mortlake#Stag Brewery or Mortlake Brewery|Mortlake Brewery]] to secure the victory.<ref name=again/>

Cambridge won by two and a half lengths in a time of 19 minutes 26 seconds, their eighth consecutive win in the event and the narrowest margin of victory since the [[The Boat Race 1923|1923 race]].  The win took the overall record in the event to 42&ndash;40 in their favour.<ref name=results/>

==References==
'''Notes'''
{{reflist|30em}}

'''Bibliography'''
*{{Cite book | title = One Hundred and Fifty Years of the Oxford and Cambridge Boat Race | first = Richard | last = Burnell | authorlink = Dickie Burnell | year=1979| isbn= 0950063878 | publisher = Precision Press}}
*{{Cite book | title = The Oxford & Cambridge Boat Race | first = Christopher| last = Dodd | isbn= 0091513405 | publisher =[[Stanley Paul (publishers)|Stanley Paul]] |year= 1983}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1931}}
[[Category:1931 in English sport]]
[[Category:The Boat Race]]
[[Category:March 1931 sports events]]