{{Orphan|date=December 2016}}

{{Use dmy dates|date=September 2013}}
{{Location map many | Mexico
| width    = 200
| caption  = The location of shelters.
| lat1_deg = 19.43
| lon1_deg = -99.13
| label1   = Mexico City
| lat2_deg = 21.16
| lon2_deg = -86.85
| label2   = Cancún
| lat3_deg = 25.67
| lon3_deg = -100.3
| label3   = Monterrey
}}

The '''Casitas del Sur case''' refers to the disappearance of several minors from children's refuges operated by the religious organization Iglesia Cristiana Restaurada (Restored Christian Church).<ref>{{cite conference |url=http://www2.ohchr.org/english/bodies/hrcouncil/docs/14session/A.HRC.14.32.Add1.pdf |title=Mexico |last1=Ezeilo |first1=Joy Ngozi |date= |publisher=[[Office of the High Commissioner for Human Rights]] |booktitle=Report submitted by the Special Rapporteur on trafficking in persons |pages=11–13 |conference= |id= }}</ref><ref>{{cite news |title=Lawyer Arrested for Child Trafficking in Mexico |url=http://www.laht.com/article.asp?CategoryId=14091&ArticleId=418756 |newspaper=Latin American Herald Tribune |date=22 August 2011 |accessdate=8 September 2013}}</ref>

The case is a reference to the private [[Mexico City]] shelter 'Casitas del Sur' from which 126 children were rescued in January 2009.<ref>{{cite news |title=Woman Arrested in Mexico on Child-Trafficking Charges |author=[[EFE]] |url=http://www.laht.com/article.asp?CategoryId=14091&ArticleId=476767 |newspaper=Latin American Herald Tribune |date=9 March 2012 |accessdate=27 July 2013}}</ref> In addition to the 'Casitas del Sur' shelter, minors were reported missing from Centro de Adaptación e Integración Familiar A.C. (CAIFAC) in [[Monterrey, Nuevo León]] and from "La Casita" in [[Cancún, Quintana Roo]].<ref>{{cite web |url=http://embamex.sre.gob.mx/canada_eng/index.php?option=com_content&view=article&id=3611&Itemid=48 |title=Help Locate Missing or Abducted Children |date=4 August 2011 |website=Embassy of Mexico in Canada |publisher=[[Secretariat of Foreign Affairs (Mexico)]] |accessdate=27 July 2013}}</ref> As of March 2012, fourteen children remained missing.<ref>{{cite news|title=Una cuidadora del albergue Casitas del Sur es arrestada |url=http://mexico.cnn.com/nacional/2012/03/08/una-cuidadora-del-albergue-casitas-del-sur-es-arrestada |newspaper=CNNMéxico |date=8 March 2012 |accessdate=27 July 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20130726203836/http://mexico.cnn.com/nacional/2012/03/08/una-cuidadora-del-albergue-casitas-del-sur-es-arrestada |archivedate=26 July 2013 |df=dmy }}</ref>

The missing children are allegedly abducted for religious [[indoctrination]], [[Adoption fraud|illegal adoptions]], [[organ trafficking]], and [[trafficking of children]].<ref>{{cite web |url=http://www.crin.org/resources/infodetail.asp?ID=19916 |title=MEXICO: Disappearance of children in institutions |date=23 March 2009 |website=Child Rights International Network |author=Red por los Derechos de la Infancia en México |accessdate=27 July 2013}}</ref><ref>{{cite news |title=Nationwide Child Pornography Ring Busted by Authorities |author=News Wires |url=http://www.banderasnews.com/0904/nr-pornring.htm |newspaper=Banderas News |date=April 2009 |accessdate=8 September 2013}}</ref> The [[Attorney General of Mexico]] offers a $1.2 million reward for information leading to the location and recovery of the missing children.<ref>{{cite news |title=Mexico offers reward in missing children case |author=CNN Wire Staff |url=http://www.cnn.com/2010/WORLD/americas/10/09/mexico.missing.children/index.html |newspaper=CNN |date=9 October 2010 |accessdate=8 September 2013}}</ref>

The case is the subject of the book ''Se venden niños'' by investigative journalist [[Sanjuana Martínez]].<ref>{{cite journal|last1= |first1= |last2= |first2= |year= |title=Jorge Erdely: El jefe |journal=Emeequis |date=22 June 2009 |volume= |issue= |pages=30–36 |publisher= |url=http://www.m-x.com.mx/xml/pdf/177/30.pdf |accessdate=8 September 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20140103232234/http://www.m-x.com.mx/xml/pdf/177/30.pdf |archivedate=3 January 2014 |df=dmy }}</ref> ''[[La Jornada]]'' journalist Carlos Martínez García was granted protective measures after receiving threats for his report on the case.<ref>{{cite news |title="La Jornada" journalist granted protective measures after being threatened |first= |last= |url=http://ifex.org/mexico/2009/05/21/martinez_threatened/ |newspaper=CENCOS/IFEX |date=21 May 2009 |accessdate=29 September 2013}}</ref><ref>{{cite news |title=Casitas sin hogar |first=Carlos |last=Martínez García |url=http://www.jornada.unam.mx/2009/03/11/politica/023a1pol |newspaper=La Jornada |date=11 March 2009 |accessdate=29 September 2013}}</ref>

== Ilse Michell ==
[[File:Casitas del Sur Missing Children March.jpg|thumb|300px|A rally for the missing children.]]
The case began to surface in 2005, when irregularities in La Casita shelter in Cancún were reported.<ref>{{cite news |title=Los responsables están avalados por el gobierno |first=Gastón |last=Pardo |url=http://www.voltairenet.org/article126095.html |newspaper=[[Voltaire Network]] |date=13 August 2005 |accessdate=8 September 2013}}</ref> The case became more publicized in late 2008 when ten-year-old Ilse Michel Curiel Martínez was not found in the Casitas del Sur shelter.<ref>{{cite news |title=Investiga PGJDF desaparición de niña enviada a casa hogar |url= |newspaper=[[Notimex]] |date=10 November 2008}}</ref> In 2005 Martinez was taken to a temporary shelter of the Mexico City Attorney General’s Office (PGJDF) due to an alleged case of domestic violence.<ref name="Busca2008">{{cite news |title=Busca PGJDF en Morelos a niña desaparecida de casa hogar |url= |newspaper=NOTIMEX |date=24 December 2008}}</ref>

She was sent to the Casitas del Sur shelter while the case was settled in the court. When the charges were dropped, the judge requested the release of the minor to her grandmother but shelter staff rejected the request. On 31 October 2008, government officials went to the property to request the release of the minor, but their entry was denied.<ref name="Busca2008" /> After obtaining a search warrant, police entered the facility to look for the minor but were unable to find her. 

Police suspect the minor had been transferred to the state of Morelos.<ref name="Busca2008" /> Local [[National Action Party (Mexico)|PAN]] congressman Agustín Castilla Marroquín filed a criminal complaint against the director of the temporary shelter, Lorena González, that transferred Ilse Michell to Casitas del Sur and demanded that she be relieved of her duties for her possible involvement in the case.<ref>{{cite news |title=Denuncian a directora de albergue de PGJDF por desaparición de niña |first= |last= |url= |newspaper=NOTIMEX |date=3 December 2008}}</ref> González denied having transferred the child to Casitas del Sur saying it was the responsibility of the district attorney's office.<ref name="Sierra2009">{{cite news |title='Pierden' a otra niña en Casitas del Sur |first=Arturo |last=Sierra |url= |newspaper=Reforma |date=27 January 2009}}</ref>

When a second girl was reported missing on 27 January 2009, local PAN congressman Agustín Castilla charged public officials with negligence and complicity saying personnel from the District Attorney's office warned the shelter of the searches so that the minors could be removed from the facilities.<ref name="Sierra2009" />

== Rescued children ==

On 29 January 2009 when at least four parents complained of being forbidden to see their children,<ref name="Cattan2009">{{cite news |title=Children's groups claim city authorities culpable |first=Nacha |last=Cattan |url= |newspaper=McClatchy – Tribune Business News |date=19 February 2009}}</ref> over 30 unarmed police officers, social workers, and staff from the Human Rights Commission entered two Casitas del Sur facilities disguised as nurses to confuse shelter staff and to prevent the children from panicking.<ref name="Sierra2009b">{{cite news |title=Rescata PGJ a niños de albergue |first1=Arturo |first2=Antonio |last1=Sierra |last2=Nieto |url= |newspaper=Reforma |date=30 January 2009}}</ref><ref name="Mendoza2009">{{cite news |title=Operación rescate de niños en DF |first=Gardenia |last=Mendoza Aguilar |url= |newspaper=La Opinión |date=31 January 2009}}</ref> Seven shelter employees objected with shouts to the release of the minors, causing the children to go on a frenzy, screaming, crying, and chanting apocalyptic phrases and accusing their rescuers of being corrupt.<ref name="Cattan2009" /><ref name="Mendoza2009" /><ref>{{cite news |title=Despliega PGJDF operativo en Casa Hogar del sur del DF |first= |last= |url= |newspaper=NOTIMEX |date=29 January 2009}}</ref>

The children repeated phrases such as "Here only the almighty God exists, and here it all ends", leading officials to believe that the minors were encouraged to participate in a mass suicide.<ref name="Sierra2009" /> Police believed the children were subjected to severe religious indoctrination and transferred the 126 minors to the [[National System for Integral Family Development]] to undergo medical and psychological studies.<ref name="Mendoza2009" /><ref>{{cite news |title=Piden a PGJDF investigar operación de albergues "Casitas del Sur" |first= |last= |url= |newspaper=NOTIMEX |date=29 January 2009}}</ref>

The children were reluctant to provide information to psychologists.<ref name="Sierra2009" /> Officials discovered that children received no outside schooling and were forced to participate in long hours of prayer.<ref name="Sierra2009" /> A rescued girl initially identified herself as Martinez, but later identified as another minor.<ref>{{cite news |title=Motiva cateo desaparición de ocho niños |first=Henia |last=Prado |url= |newspaper=Reforma |date=1 February 2009}}</ref> DNA tests performed on the minor gave a negative result.<ref>{{cite news |title=Desconoce PGJDF paradero de niña retenida por Casitas del Sur |first= |last= |url= |newspaper=NOTIMEX |date=5 February 2009}}</ref> The seven shelter employees with arrested and charged with [[obstruction of justice]].<ref>{{cite news |title=Asegura PGJDF a 116 niños de casas-hogar en investigación |first= |last= |url= |newspaper=NOTIMEX |date=29 January 2009}}</ref>

On June 6, 2014 Ilse Michel arrived alone in a taxi to the offices of the [[National System for Integral Family Development]] in Ecatepec, Mexico. She reunited with her grandmother after spending five years with a couple who told her she had been abandoned. As of March 2014, eight children remain missing. [http://www.eluniversal.com.mx/ciudad-metropoli/2014/impreso/regresa-a-casa-ilse-michel-122087.html]

== References ==
{{reflist|2}}

[[Category:Human trafficking in Mexico]]
[[Category:Human rights abuses]]
[[Category:Organized crime activity]]
[[Category:Kidnapped Mexican children]]
[[Category:International child abduction]]
[[Category:Human rights in Mexico]]
[[Category:Crime in Mexico]]
[[Category:Child abuse]]
[[Category:Crimes against children]]