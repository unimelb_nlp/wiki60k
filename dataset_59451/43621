<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name= CH-124 Sea King
 |image= File:CH-124 Sea King.jpg
 |caption= A Canadian Forces CH-124 Sea King
}}{{Infobox Aircraft Type
 |type= [[Anti-submarine warfare|ASW]] / [[utility helicopter]]
 |national origin= [[United States]] / [[Canada]]
 |manufacturer= [[Sikorsky Aircraft]]
 |builder= [[Pratt & Whitney Canada|United Aircraft of Canada]]
 |first flight=
 |introduction= 1963<ref>[http://www.cbc.ca/news/background/cdnmilitary/seaking.html Requiem for the Sea King.] Retrieved on November 17, 2008.</ref>
 |retired=
 |status= Active service
 |primary user= [[Canadian Forces]]
 |more users= [[Royal Canadian Navy]]<br>[[Royal Canadian Air Force]]
 |produced=
 |number built=41
 |unit cost=
 |developed from= [[SH-3 Sea King]]
 |variants with their own articles=
}}
|}

The '''Sikorsky CH-124 Sea King''' is a twin-engined [[anti-submarine warfare]] (ASW) [[helicopter]] designed for shipboard use by Canadian Naval forces, based on the [[US Navy]]'s [[Sikorsky SH-3 Sea King|SH-3 (or S-61)]] and has been continuously in service with the [[Royal Canadian Navy]] (RCN) and [[Canadian Forces]] since 1963.

==Design and development==
The advent of nuclear-powered attack submarines in the late 1950s prompted RCN leaders to assess the new threat they posed. Although these craft were noisier than older submarines and could therefore be detected at longer ranges, they were also capable of {{convert|30|kn|km/h|0}} while submerged, which was faster than the top speed of the RCN's new {{sclass-|St. Laurent|destroyer|0}} [[destroyer escort]]s at {{convert|28.5|kn|km/h|1}}. Some RCN leaders harbored serious doubts that the destroyers could effectively pursue and destroy such fast vessels, even when operating in pairs. During a 25 February 1959 meeting of the Naval Board, it was decided that the Navy would counter the new threat by outfitting destroyers for helicopter operation.<ref name="soward169">Soward 1995, pp.169-171.</ref>

The RCN had examined the feasibility of operating ASW helicopters from small escorts when it modified the {{sclass-|Prestonian|frigate}} {{HMCS|Buckingham|FFE 314|6}} in mid-1956 with a temporary helicopter landing platform. Successful trials were held in October 1956 using a [[Sikorsky H-19|Sikorsky HO4S-3]]<ref name="soward63">Soward 1995, pp.63-65.</ref><ref name="beartrap">[http://www.readyayeready.com/timeline/1960s/beartrap/index.htm ''Crowsnest Magazine - Vol 17, Nos 3 and 4 March-April 1965'']</ref> and a larger temporary landing platform was soon installed on the new destroyer escort {{HMCS|Ottawa|DDE 229|6}}. Operational trials were conducted using an [[Royal Canadian Air Force|RCAF]] [[Sikorsky H-34|Sikorsky S-58]], a substantially larger and heavier aircraft than the HO4S, and the success of these tests led to approval of the concept.<ref name="beartrap"/><ref name="soward92">Soward 1995, pp.92-93.</ref>

The RCN's then current HO4S-3 utility helicopter could not operate safely in inclement weather with a heavy weapons and sensor load, which would be imperative for the ASW role; hence, a more capable aircraft was needed. Initial 1959 studies identified two helicopters that seemed suitable - the Sikorsky S-61 (HSS-2) and the [[Kaman SH-2 Seasprite|Kaman K-20 (HU2K)]]- but neither aircraft had flown at the time, so no choice was made. After further studies concluded that the smaller Kaman would better satisfy RCN requirements, the [[Treasury Board]] approved an initial procurement of 12 HU2K helicopters for $14.5 million in December 1960.<ref name="soward169"/>

Despite this apparent setback for Sikorsky, several factors would derail the Kaman proposal. When the Naval Board held a follow-up meeting on 27 January 1961 to discuss the program, it was revealed that the asking price for the initial 12 units had nearly doubled to $23 million, a mere 6 weeks after the Treasury Board had approved the purchase. The Naval Board continued to endorse the HU2K, but some RCN leaders had serious misgivings due to the drastic price increase and staff reports that Kaman's performance projections might be overly optimistic. The Naval Board decided to await upcoming USN sea trials of the HU2K before rendering a final decision.<ref>Soward 1995, pp.244-246.</ref> The USN trials confirmed the calculations of RCN staff members; the HU2K was substantially heavier than promised, hampering its flight performance and rendering it incapable of meeting RCN requirements, even if Kaman were to install a proposed upgraded engine. The Sea King was ultimately chosen for production on 20 December 1961.<ref>Soward 1995, pp.261-262.</ref>

The first of 41 helicopters would be delivered in 1963 carrying the designation '''CHSS-2 Sea King'''. The airframe components were made by [[Sikorsky Aircraft|Sikorsky]] in Connecticut but most CHSS-2s were assembled in [[Longueuil, Quebec]] by United Aircraft of Canada (now [[Pratt & Whitney Canada]]), a subsidiary of Sikorsky's [[United Technologies Corporation|parent company]]. On 27 November 1963, the new landing platform aboard {{HMCS|Assiniboine|DDH 234|6}} was used for the first operational destroyer landing of a production CHSS-2.<ref>Soward 1995, pg. 326.</ref> Upon the unification of Canada’s military in 1968, the CHSS-2 was re-designated '''CH-124'''.<ref name="CSAR variants">{{cite web |url=http://www.sfu.ca/casr/bg-helo-ch124-variants.htm |title=CH-124 Sea King Variants |accessdate= 2007-06-19|author= |authorlink= |coauthors= |date= |year= |month= |work= |publisher=Canadian American Strategic Review |pages= |language= |archiveurl = http://web.archive.org/web/20070911183438/http://www.sfu.ca/casr/bg-helo-ch124-variants.htm <!-- Bot retrieved archive --> |archivedate = 2007-09-11 |quote= }}</ref>

In the 1960s,<ref name=lisa>Gordon, Lisa, [http://www.verticalmag.com/features/features_article/The-king-at-sea#.UqiykNLuIqY The King at sea]" ''Vertical Magazine'', 9 December 2013. Accessed: 11 December 2013.</ref> the RCN developed a technique for landing the huge helicopters on small ship decks, using a 'hauldown' winch (called a '[[Beartrap (helicopter device)|Beartrap]]'),<ref>{{cite web|url=http://www.hazegray.org/navhist/canada/current/seaking/|title=Haze Gray & Underway - The Canadian Navy of Yesterday & Today - Sea King}}</ref> earning aircrews the nickname of 'Crazy Canucks'.<ref name=autogenerated4>{{cite news|url=http://www.cbc.ca/news/background/cdnmilitary/seaking.html|title=CBC News In Depth: Canada's Military | date=1 February 2006}}</ref>  The 'Beartrap' allows recovery of the Sea King in virtually any [[sea state]].<ref>[http://web.archive.org/web/20091027010245/http://geocities.com/CapeCanaveral/9411/fraser/class/  ST. LAURENT Class History.]</ref> In 1968, the RCN, [[Royal Canadian Air Force]] (RCAF) and [[Canadian Army]] unified to form the Canadian Forces; air units were dispersed throughout the new force structure until [[Canadian Forces Air Command|Air Command]] (AIRCOM) was created in 1975. In August 2011, the Canadian Forces reverted to the former structure of the [[Royal Canadian Navy]], [[Canadian Army]] and [[Royal Canadian Air Force]].

==Operational service==
[[File:Sikorsky CH-124.JPG|thumb|Sikorsky CH-124A Sea King]]

The Sea King is assigned to {{sclass-|Iroquois|destroyer|0}} destroyers (2 per ship with total of 6), {{sclass-|Halifax|frigate|1}}s (1 per ship with total of 12), and {{sclass-|Protecteur|replenishment oiler|0}} [[replenishment ship]]s (3 per ship with total 6) as a means of extending the surveillance capabilities beyond the horizon. When deployed, the Sea King is accompanied by a number of crews - each with 2 pilots, a Tactical Coordinator ([[TACCO]]), and an Airborne Electronic Sensor Operator (AESOp).<ref>{{cite web|url=http://www.airforce.forces.gc.ca/site/equip/ch124/specs_e.asp|title=Canada's Air Force - Aircraft - CH-124 Sea King - Technical Specifications}}</ref>

In order to find submarines, the Sea King's sonar uses a transducer ball at the end of a 450-foot cable. It can also be fitted with FLIR ([[Forward looking infrared|Forward-Looking Infra-Red]]) to find surface vessels at night.

The CH-124 has undergone numerous refits and upgrades, especially with regard to the electronics, main gearboxes and engines, surface-search radar, secure cargo and passenger carrying capabilities.

In 2013 the CH-124 fleet averaged 9–14,000 flying hours, while Sea Kings of other fleets go as high as 40,000 hours. Although the CH-124 had frequent technical issues, none are serious, and they could maintain a 87 percent serviceability rate.<ref name=lisa/>

===Replacement===
{{Main|Canadian Sea King replacement}}
From 1983 onward attempts were made to [[Canadian Sea King replacement|replace the aging Sea King]] helicopters. Due to a series of financial and political issues, the process was hampered by repeated delays. In the end the [[CH-148 Cyclone]], a new version of the [[Sikorsky S-92|Sikorsky H-92 Superhawk]] was selected.

==Variants==
[[File:SikorskyCH124SeaKing07A.JPG|thumb|right|Sikorsky CH-124A Sea King with blades folded for storage.]]
[[File:US Navy 080726-N-7883G-108 A CH-124 Sea King assigned to the Canadian 436 lands aboard the aircraft carrier USS Kitty Hawk (CV 63) after the conclusion of Rim of the Pacific 2008.jpg|thumb|CH-124A Sea King aboard the aircraft carrier {{USS|Kitty Hawk|CV-63|6}}]]

;CH-124 : [[Anti-submarine warfare]] helicopter for the [[Royal Canadian Navy]] (41 assembled by United Aircraft of Canada).<ref name="CSAR variants"/>
;CH-124A : The Sea King Improvement Program (SKIP) added modernized avionics as well as improved safety features.<ref name="CSAR variants"/>
;CH-124B : Alternate version of the CH-124A without a dipping sonar but formerly with a MAD sensor and additional storage for deployable stores.  In 2006, the 5 aircraft of this variant were converted to support the Standing Contingency Task Force (SCTF), and were modified with additional troop seats, and frequency agile radios.  Plans to add fast-rope capability, EAPSNIPS (Engine Air Particle Separator / Snow & Ice Particle Separator) did not come to fruition.<ref name="CSAR variants"/>
;CH-124B2 : 6 CH-124B's were upgraded to the CH-124B2 standard in 1991-1992. The revised CH-124B2 retained the sonobuoy processing gear to passively detect submarines but, the aircraft was now also fitted with a towed-array sonar to supplement the ship's sonar. Since anti-submarine warfare is no longer a major priority within the Canadian Forces, the CH-124B2 were refitted again to become improvised troop carriers for the newly formed Standing Contingency Task Force.<ref name="CSAR variants"/>
;CH-124C : One CH-124 operated by the Helicopter Operational Test and Evaluation Facility located at [[CFB Shearwater]]. Used for testing new gear, and when not testing new gear, it is deployable to any Canadian Forces ship requiring a helicopter.<ref name="CSAR variants"/>
;CH-124U : Unofficial designation for 4 CH-124's that were modified for passenger/freight transport. One crashed in 1973, and the survivors were later refitted to become CH-124A's.<ref name="CSAR variants"/>

==Operators==
;{{CAN}}
* [[Royal Canadian Navy]]<ref name="RCAF.com">[http://rcaf.com/Aircraft/aircraftDetail.php?SEA-KING-179 Sikorsky CH-124 Sea King]</ref>
* [[Royal Canadian Air Force]]<ref name="RCAF.com" />
** [[443 Maritime Helicopter Squadron]]<ref>{{cite web|url= http://www.canadianwings.com/Squadrons/squadronDetail.php?No.-443-Squadron-99|title= No. 443 Squadron |publisher= canadianwings.com |date=|accessdate=23 March 2013}}</ref> 
** [[406 Maritime Operational Training Squadron]]<ref>{{cite web|url= http://www.canadianwings.com/Squadrons/squadronDetail.php?No.-406-Squadron-65|title= No. 406 Squadron |publisher= canadianwings.com |date=|accessdate=23 March 2013}}</ref>
** [[423 Maritime Helicopter Squadron]]<ref>{{cite web|url= http://www.rcaf-arc.forces.gc.ca/12w-12e/sqns-escs/page-eng.asp?id=465|title= 423 Maritime Helicopter Squadron |publisher= rcaf-arc.forces.gc.ca |date=|accessdate=23 March 2013}}</ref>

==Specifications (CH-124 Sea King)==
[[File:SH-3 Sea King drawing.svg|right|450px|Orthographically projected diagram of the SH-3 Sea King.]]
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=copter
|jet or prop?=prop
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For additional lines, end your alt units with)</li> and start a new, fully-formatted line with <li> -->
|crew=4  (2 pilots, 1 navigator, 1 airborne electronic sensor operator)
|capacity=3 passengers
|length main=54 ft 9 in
|length alt=16.7 m
|height main=16 ft 10 in
|height alt=5.13 m
|span main=62 ft
|span alt=19 m
|area main= ft²
|area alt= m²
|empty weight main=11,865 lb
|empty weight alt=5,382 kg
|loaded weight main=18,626 lb
|loaded weight alt=8,449 kg
|max takeoff weight main=22,050 lb
|max takeoff weight alt=10,000 kg
|engine (prop)=[[General Electric T58]]-GE-8F/-100
|type of prop=[[turboshaft]]s
|number of props=2
|power main=1,500 shp
|power alt= 1118 kW
|max speed main=166 mph
|max speed alt=267 km/h
|range main=621 mi
|range alt=1,000 km
|ceiling main=14,700 ft
|ceiling alt=4,481 m
|climb rate main=1,310-2,220 ft/min
|climb rate alt=400-670 m/min
|loading main=
|loading alt=
|power/mass main=
|power/mass alt=
|armament=
* 2&times; [[Mark 46 torpedo|Mk 46]] Mod V anti-submarine [[torpedo]]es
* Various [[sonobuoy]]s and pyrotechnic devices
* door guns (some variants)
}}

==See also==
{{Portal|Aviation|Canadian Armed Forces}}
* [[Water landing#"Water bird" emergency helicopter landing technique|Water bird emergency landing]]
{{aircontent|
|related=
* [[SH-3 Sea King]]
* [[Sikorsky S-61R]]
* [[Sikorsky S-62]]
* [[Westland Sea King]]
|similar=
* [[Mil Mi-14|Mi-14 Haze]]
|lists=
* [[List of active Canadian military aircraft]]
}}

==References==

===Notes===
{{Reflist|30em}}

===Bibliography===
* {{cite book|last=Soward |first=Stuart E. |title=Hands to Flying Stations, a Recollective History of Canadian Naval Aviation, Volume II. |location=Victoria, British Columbia |publisher=Neptune Developments |date=1995 |isbn=0-9697229-1-5}}

==External links==
{{Commons|H-3 Sea King}}
* [http://www.rcaf-arc.forces.gc.ca/en/aircraft-current/ch-124.page Canadian Forces official CH-124 Sea King website]
* [http://www.naval-technology.com/projects/seaking/ UK Defence Industries Site]
* [http://www.cbc.ca/news/canada/story/2013/07/30/f-sea-king-timeline.html The Sea King Timeline], CBC News, July 31, 2013

{{Sikorsky Aircraft}}
{{Canadian Forces aircraft}}

[[Category:United States military helicopters]]
[[Category:United States helicopters 1960–1969]]
[[Category:Sikorsky aircraft|H-124]]
[[Category:Twin-turbine helicopters]]