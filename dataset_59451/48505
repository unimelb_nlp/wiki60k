{{Use mdy dates|date=October 2015}}
{{notability|Biographies|date=May 2015}}
{{Infobox artist
| honorific_prefix = 
| name             = Ann Hirsch <!-- include middle initial, if not specified in birth_name -->
| honorific_suffix = 
| image            = <!-- just the pagename, without the File:/Image: prefix or [[brackets]] -->
| image_size       = 
| alt              = 
| caption          = 
| native_name      = 
| native_name_lang = 
| birth_name       = <!-- only use if different than name -->
| birth_date       = 1985 <!-- {{Birth date and age|YYYY|MM|DD}} for living artists, {{Birth date|YYYY|MM|DD}} for dead. For living people supply only the year unless the exact date is already WIDELY published, as per [[WP:DOB]]. Treat such cases as if only the year is known, so use {{birth year and age|YYYY}} or a similar option. -->
| birth_place      = 
| death_date       = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} --> 
| death_place      =
| resting_place    = 
| resting_place_coordinates = <!-- {{Coord|LAT|LONG|type:landmark|display=inline}} -->
| nationality      = 
| education        = [[Washington University in St. Louis]], <br> [[Syracuse University]]
| alma_mater       = 
| known_for        = Video and performance art
| notable_works    = 
| style            = 
| movement         = 
| spouse           = 
| awards           = <!-- {{awd|award|year|title|role|name}} (optional) -->
| elected          =
| patrons          = 
| memorials        = 
| website          = <!-- {{URL|Example.com}} -->
| module           =
}}
'''Ann Hirsch''' (born 1985<ref name=VerbatimAnnHirsch>{{cite news | title = Verbatim: Ann Hirsch | first = Matthew Shen | last = Goodman | journal =  [[Art in America]] | date = June 25, 2014 | url = http://www.artinamericamagazine.com/news-features/interviews/verbatim-ann-hirsch/}}</ref>) is a contemporary American video and performance artist. Her work addresses women's sexual self-expression and identity online and in popular culture.

== Work ==

=== Early life ===
As a teen in the late 1990s, Hirsch developed a significant chatroom presence.<ref>{{Cite web|title = Ann Hirsch discusses her latest work about ’90s cybersex|url = http://artforum.com/words/id=43319|website = ArtForum Magazine|accessdate = 2015-12-28}}</ref> Her experiences on chatrooms and online would inform much of her later work, including 2013's "Playground."<ref name=":0">{{Cite web|title = Ann Hirsch: Playground|url = http://www.newmuseum.org/calendar/view/ann-hirsch-playground|website = New Museum|accessdate = 2015-12-28}}</ref>

Hirsch received a BFA in sculpture from [[Washington University in St. Louis]], Missouri, in 2007, and an MFA in Video Art from [[Syracuse University]] in 2010.<ref>{{Cite web|url = http://www.americanmedium.net/uploads/3/0/5/8/30583131/ann_hirsch_cv.pdf|title = Ann Hirsch CV|date = |accessdate = 2015-12-28|website = American Medium|publisher = |last = |first = }}</ref>

=== Video and performance art ===
In 2008, Hirsch initiated the "Scandalishious" project, a series of videos posted to her YouTube account, "Caroline's fun fun channel."  Using her computer to record herself, Hirsch performed as Caroline, a [[SUNY]] freshman.<ref>{{cite news | title = [Portrait of the internet as a young girl] | first = Moira | last = Weigel | journal =  [[Rhizome]] | date = September 30, 2013 | url = http://rhizome.org/editorial/2013/sep/30/portrait-internet-young-girl/}}</ref> Many of the clips show Caroline dancing to music ranging from [[MGMT]] to [[Katy Perry]] to [[Meat Loaf]]. In other videos, Caroline reads poetry or confides to her viewers about her personal life. By using her computer to record and convey videos of herself in private to a wider audience while seeming to directly address that audience, Hirsch makes an apparently intimate experience into a very public one.  The channel has reached over one million views.<ref name="VerbatimAnnHirsch" /> Of her motivation in creating "Scandalishious," Hirsch has said, <blockquote>While I was growing up and becoming a woman, I hated myself. I knew I was smart but other than that I thought I was just a disgusting girl that no one could be sexually interested in. I started performing as "Scandalishious" because I was tired of feeling that way. Or at least, I was tired of appearing as though I felt that way. So I started pretending I thought I was sexy and I quickly learned that if I pretended to be confident, people would believe it. And then I actually became more confident as a result.<ref name="rhizome">{{cite news |title = Ann Hirsch: Artist Profile|first = Karen|last = Archey|journal = Rhizome|date = March 7, 2012|url = http://rhizome.org/editorial/2012/mar/7/artist-profile/}}</ref> </blockquote>

This personal motivation coincided with a desire to present a multifaceted female YouTube figure to a community already filled with images of women which, as Hirsch saw it, were either speaking to the camera or dancing for the camera, with very little overlap between the two roles. Hirsch combined these [[camgirl]] tropes to create a character who was, to use Hirsch's words, both sexual and human.<ref name="rhizome" /> By merging these distinct and established roles available to women online, the "Scandalishious" project explores questions of femininity, extreme publicity, online communities, and the appropriation and dissection (in the harassment or approval by YouTube commenters, for example) of bodies and personalities on the internet. Hirsch also went on to display and discuss segments of the "Scandalishious" project in galleries.<ref>{{cite news | title = Women on the Verge | first = Johanna | last = Fateman | journal =  [[Artforum]] | date = April 2015 | url = https://artforum.com/inprint/issue=201504&id=50736}}</ref>

In 2010, Hirsch was a participant on the [[VH1]] reality TV show [[Frank the Entertainer in a Basement Affair]].<ref>{{Cite web|url=https://www.bostonglobe.com/arts/theater-art/2016/01/13/hijacking-chatrooms-youtube-and-reality/1YYfNW7ZWGdFJoTKv2Xp6I/story.html|title=Hijacking chatrooms, YouTube, and reality TV - The Boston Globe|website=BostonGlobe.com|access-date=2016-03-05}}</ref> Hirsch went by "Annie," one of fifteen women trying to win over [[Frank Maresca]], himself a former reality TV contestant on [[I Love New York 2]] and [[I Love Money]].<ref name="nydailynews">{{cite news | title = Frank Moresco is looking for love from his parent's basement in VH1's 'Frank the Entertainer'| first = Cristina | last = Kinon | page = Television section, page 61 | nopp = y | newspaper =  [[New York Daily News]] | date = December 24, 2009 | url = http://nydailynews.com/entertainment/tv/2009/12/24/2009-12-24_starting_at_the_bottom_singles_head_to_basement.html}}</ref> Hirsch's incentive in joining the show was to create a performance piece and to investigate the derogatory stereotypes surrounding women who vie for publicity. As was the case with "Scandalishious," Hirsch was also interested in the transformative possibilities of playing this particular role, and of the effects such a role could have on her personally.<ref>{{cite news | title = Shaming Famewhores Part I: On Becoming a Famewhore| first = Ann | last = Hirsch | magazine =  [[Bust (magazine)|Bust]] | date = January 25, 2010 | url = http://bust.com/shaming-famewhores-part-i-on-becoming-a-famewhore.html}}</ref> Hirsch's participation on "Frank the Entertainer" highlighted the nebulous line between performance and reality. She found herself cast as "the nice girl," and realized "the non-sexualization of my character, both through my own doing and through careful editing...rendered me an inadequate partner for Frank."<ref>{{cite news | title = Shaming Famewhores Part II: On Being a Failed Famewhore| first = Ann | last = Hirsch | magazine =  [[Bust (magazine)|Bust]] | date = February 23, 2010 | url = http://bust.com/shaming-famewhores-part-ii-on-being-a-failed-famewhore.html}}</ref> To avoid this carefully packaged characterization, Hirsch performed an expletive-filled rap song and was promptly sent home. Her demonstration of the sexual, outspoken side of the "nice girl" character altered the narrative the producers had envisioned. Historically, video art has borrowed from and been shown on TV, but Hirsch's role on "Frank the Entertainer" was itself the completed work, rather than any later documentation of the performance. In an interview with Hirsch, [[Karen Archey]] suggested that reality TV itself was the medium for the performance.<ref name="artforum">{{cite news | title = Ann Hirsch | journal =  [[Artforum]] | date = October 1, 2013 | url = http://artforum.com/words/id=43319/}}</ref> Even so, a compilation of clips of her appearances on the show along with her application video, entitled "Here For You (Or my Brief Love Affair With Frank Maresca," can be found on Hirsch's website. Hirsch also participated on the TV series [[Oddities (TV series)|Oddities]].

After Hirsch's work with Frank the Entertainer ended, in 2011 she collaborated with performance artist [[Genevieve Belleveau]] (also known as gorgeousTaps) to create ''The Reality Show with gorgeousTaps.''<ref>{{Cite web|url=http://www.blouinartinfo.com/blog/image-conscious/studio-visit-the-real-world-of-ann-hirsch|title=Studio Visit: The Real World of Ann Hirsch|date=2011-02-01|website=Blouin Artinfo|publisher=Louise Blouin Media|access-date=2016-03-07}}</ref>

Hirsch's "Playground," a play about a coming of age, teenage girl engaging in a relationship with an adult man through an internet [[chatroom]], debuted in 2013.<ref name=":0" /> The set of "Playground" consists of two desks where the actors sit at computers, appearing to chat with each other, while the textual component of their conversation is projected onto the wall behind them.<ref name="artforum" /><ref>{{Cite web|title = Ann Hirsch: Playground|url = http://morganquaintance.com/2014/12/22/ann-hirsch-playground/|website = Morgan Quaintance writings|accessdate = 2015-12-28|first = Morgan|last = Quaintance}}</ref> The play was inspired by Hirsch's own teenage experience of meeting and engaging in a multi-year online relationship with an older man. Hirsch also collaborated with designer [[James LaMarre]] to produce "Twelve," an [[Application software|app]] that recreated the now-defunct [[AOL]] chatrooms of Hirsch's adolescence. Though the app recreated the appearance of the interactive chatroom space, an app-user would be met with a pre-conceived narrative of a young girl chatting with an older man. Hirsch explained, "My point with telling this story is just to be honest and convey both the benefits I got from this relationship (intimacy, sexual knowledge) but also show the manipulation and exploitation that was involved in a relationship like this as well." The app was eventually banned by the iTunes Store.<ref>{{cite news | title = Artist's Notebook: Ann Hirsch |first = Marina | last = Galperina | journal =  [[Animal New York]] | date = April 28, 2014| url =http://animalnewyork.com/2014/artists-notebook-ann-hirsch/}}</ref>

Hirsch participated in "Body Anxiety," a 2015 online exhibition featuring work by video and performance artists based on questions of the female body through the lens of the internet.<ref>{{cite news | title = "Body Anxiety:" Sabotaging Big Daddy Mainframe, via online exhibition |first = Josephine | last = Bosma | journal =  [[Rhizome]] | date = January 26, 2015| url=http://rhizome.org/editorial/2015/jan/26/body-anxiety/}}</ref>

==Influences and contemporaries==
Hirsch has cited [[Cindy Sherman]], [[Andrea Fraser]], [[Jill Magid]], [[Alex Bag]], and [[Laurel Nakadate]] as key influences.<ref name="rhizome" /> In the wake of work by these and other artists, Hirsch’s multi-media oeuvre operates within a history of [[feminist]] artwork concerning the representation and objectification of women and other crucial feminist concerns. Along with [[Petra Cortright]] and other contemporary artists who employ [[Photobooth]] and YouTube in their work, Hirsch reimagines long-standing feminist questions, particularly in the context of the internet, reality TV, and other modern instances of mass communication and publicity. Hirsch has expressed interest in the videos of and reception to internet [[camgirl]]s like [[Boxxy]].<ref name="rhizome" />

== References ==
<!--- See http://en.wikipedia.org/wiki/Wikipedia:Footnotes on how to create references using<ref></ref> tags, these references will then appear here automatically -->
{{reflist|30em}}

== External links ==
*[http://therealannhirsch.com/ Ann Hirsch] official website
*Interview: ''[http://brooklynquarterly.org/keeping-track-of-the-real-ann-hirsch/ Keeping Track of the Real Ann Hirsch]'' with Rachel Wetzler of Brooklyn Quarterly
*Interview: ''[http://arcade-gallery.tumblr.com/post/28567488079/featured-artist-ann-hirsch-i-am-watched I am watched therefore I am]'' with The Arcade Gallery in 2012

{{DEFAULTSORT:Hirsch, Ann}}
[[Category:1985 births]]
[[Category:Living people]]
[[Category:American women video artists]]
[[Category:American video artists]]
[[Category:Articles created via the Article Wizard]]
[[Category:Feminist artists]]