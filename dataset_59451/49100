'''Nigel Konstam''' (born 1932) is a British sculptor and art historian who has researched the history of art and lectured internationally on art historical subjects. He specialised in exploring the development of Man's ability to understand what he sees he challenged orthodoxy on important turning points in art history.[[File:Nigel Konstam in front of Maquettes.JPG|thumb|Nigel Konstam in front of his Maquettes, Verrocchio Art Centre]]

==Early life and education==

Born in London in 1932, Nigel Konstam studied sculpture under Dr Karel Vogel from 1956 to 1958 at Camberwell School of Art,<ref>{{Citation
| last      = Hassell
| first     = Geoff
| title     = Camberwell School of Arts & Crafts
| publisher = ACC Art Books
| place     = USA
| year      = 1999
 |   page               =   112
| type      = hardcover
| isbn      = 9781851491803}}</ref> later known as the [[Camberwell College of Arts]], and briefly at the [[Royal College of Art]] in 1958. He moved to [[Italy]] in 1983 living near [[Siena]].

==Career==

Konstam became an active sculptor and bronze-caster, in parallel forging a career to explore the history of art, specializing in tracing the development of Man's ability to understand what he sees. He lectured on art historical subjects at the major art colleges in [[Kingdom of Great Britain]] also at [[Harvard]] in the US and PINC<ref>{{cite web
 | title = PINC.12 Speaker: Nigel Konstam
 | url = http://www.pinc.nl/pinc_speaker_prev.php?speaker=&cid=211
 | website = pinc.nl
 | date = 2002
 | access-date = December 21, 2015}}
</ref>
in the [[Netherlands]].

Konstam was commissioned to make portraits of musicians including [[Otto Klemperer]] in 1982,<ref>{{cite web
 | title = Artist Biography K Artists In Britain Since 1945 - Chapter K
 | url = http://issuu.com/powershift/docs/dictionary_k/83
 | website = issuu.com
 | date = 2015
 | access-date = December 21, 2015}}
</ref> [[Hans Hotter]], [[Manoug Parikian]] and [[John Ireland (composer)]]. Showing at The Cadogan Contemporary Gallery<ref>{{cite web
 | title = Cadogan Contemporary The Gallery 
 | url = https://www.cadogancontemporary.com/the-gallery/
 | date = 2015
 | access-date = December 21, 2015}}
</ref> in [[London]] his carvings in alabaster are ordinarily on show or can been seen on request. He has drawings in the collection of [[The British Museum]].<ref>{{cite web
 | title = The British Museum – Collection online
 | page = 1972,0722.4
 | url = http://www.britishmuseum.org/research/collection_online/collection_object_details.aspx?objectId=732946&partId=1
 | website = britishmuseum.org
 | date = 1966
 | access-date = October 4, 2016}}</ref><ref>British Museum Catalogue 1971,0212.    PRN: PDB22204 Location: British Roy PIX</ref><ref>British Museum Catalogue 1972,0722.2  PRN: PDB22203 Location: British Roy PIX</ref><ref>British Museum Catalogue 1972,0722.1  PRN: PDB22200 Location: British Roy PIX</ref><ref>British Museum Catalogue 1972,0722.3  PRN: PDB22199 Location: British Roy PIX</ref><ref>British Museum Catalogue 1972,0722.4  PRN: PDB22198 Location: British Roy PIX</ref><ref>British Museum Catalogue 1972,0722.5  PRN: PDB22197 Location: British Roy PIX</ref> He chaired the Contemporary Portrait Society in [[London]] between 1975 and 1980.<ref>{{cite web
 | title = Artist Biographies Contemporary Portrait Society 
 | url = http://www.artbiogs.co.uk/2/societies/contemporary-portrait-society
 | website = artbiog.co.uk 
 | date = 2015
 | access-date = December 21, 2015}}
</ref> In 2001 he exhibited in [[Pisa]], in [[Florence]] in 2002 and [[Todi]] in 2011.  In [[Spain]] he sculpted Portrait busts of [[Juan Carlos I of Spain]] and [[Queen Sofía of Spain]].<ref>ABC Newspaper, 15.6.1980.</ref><ref>Iberian Daily Sun 22.6.80.</ref>

In Italy he set up and ran The Verrocchio Arts Centre<ref>{{cite web
 | title = Welcome to the Verrocchio Art Centre
 | url = http://www.verrocchio.co.uk/cms/index.php/component/content/frontpage
 | last = Konstam
 | first = Nigel
 | website = Verrocchio.co.uk
 | date = 2015
 | access-date = December 21, 2015}}</ref> in [[Casole d'Elsa]]. Within the Centre is the Museo Konstam<ref>{{cite web
 | title = MUSEO KONSTAM sculpture and drawings
 | url = http://tuscany.angloinfo.com/classifieds/index/1000/category/general-for-sale/viewclassified/106326/museo-konstam-sculpture-and-drawings
 | website = artbiog.co.uk 
 | date = 2015
 | access-date = December 21, 2015}}
</ref> a number of Konstam’s sculptures and drawings are displayed. Many of his larger works can be seen near La chiesa di San Niccolò and in the village of [[Casole d'Elsa]]. Also in the Museo Konstam resides The Research Centre for the True History of Art.<ref>{{cite web
 | title = Spring Newsletter 2013 News and updates from Cadogan Contemporary
 | url = http://issuu.com/cadogancontemporary/docs/spring_2013_newsletter/1
 | website = issuu.com
 | date = April 2013
 | access-date = December 21, 2015}}</ref><ref>{{cite web
 | title = The Museum of the True History of Art
 | url = http://nigelkonstam.com/cms/index.php/the-museum-of-the-true-history-of-art
 | place  =  Casole d'Elsa
 | website =  Nigelkonstam.com
 | date = 1976
 | access-date = December 21, 2015}}
</ref>

The main exhibits in the museum are demonstrations of artists' use of mirrors (for example [[Rembrandt]], [[Velasquez]], [[Vermeer]] and [[Brunelleschi]]). Also featured is the Roman tradition in European Art, Greek Life-casting and Bronze Casting technique, and includes medieval Sienes sculpture and drawing artifacts.
Konstam was an artist whose work was displayed in mixed exhibitions, and some solo ones in and around London though, notably, as far back as 1980, presented a major respective in [[Madrid]] with over 100 pieces on show followed by shows in [[Barcelona]] and [[Salamanca]].<ref>{{cite web
 | title = Nigel Konstam Sculptures
 | url = http://www.worldcat.org/title/nigel-konstam-sculptures/oclc/501278319&referer=brief_results
 | place  =  Galería Kreisler (Madrid)
 | last    =  Antolin
 | first   =  Mario
 | website = issuu.com
 | date = 1980
 | access-date = December 21, 2015}}</ref><ref>{{cite web
 | title = ABC De Las Arts
 | url = http://hemeroteca.abc.es/nav/Navigate.exe/hemeroteca/madrid/abc/1980/07/06/099.html
 | place  =  Madrid
 | website =  hemeroteca.abc.es
 | date = 1980
 | access-date = December 21, 2015}}
</ref> He has revealed evidence that some aspects of the history of art are mistaken and challenged orthodoxy on important turning points in art history; namely the classical phase in [[Greece]], the start of the Renaissance and around 1630 which he claims is the start of the modern era.<ref>New Humanism by Nigel Konstam 2015 published by Industria Grafica Pistolesi Editrice Il Leccio s.r.l. - Via della Resistenza 117, Località Badesse, Monteriggioni (Siena)</ref>

==Artistic analysis==

A prime focus of Konstam was the exploration of the history of Man's ability to understand what he sees. Many of his relatively revolutionary ideas have been catalogued.<ref>{{cite web
 | title = The Story of a Discovery
 | url = http://www.saveRembrandt.org.uk
 | last      = Konstam
 | first     = Nigel
 | editor-last = Grossman-Telfer
 | editor-first = Nancy
 | website = saverembrant.org.uk
 | date = 2015
 | access-date = December 21, 2015}}
</ref><ref>{{cite web
 | title = Nigel Konstam, Artist, Sculptor
 | url = http://www.nigelkonstam.com 
 | last = Konstam
 | first = Nigel
 | website = nigelkonstam.com
 | date = 2015
 | access-date = December 21, 2015}}</ref>
<ref>{{cite web
 | title = Welcome to the Verrocchio Art Centre
 | url = http://www.verrocchio.co.uk/cms/index.php/component/content/frontpage
 | last = Konstam
 | first = Nigel
 | website = Verrocchio.co.uk
 | date = 2015
 | access-date = December 21, 2015}}
</ref> 
Konstam claimed to have established two important contributions to the understanding of western art. The first was outlined in an article in The Apollo Magazine (Aug.1972)<ref>{{cite web
 | title = Roman Sculpture, a Technical Analysis
 | url = http://www.apollo-magazine.com
 | magazine   = Apollo The International Art Magazine
 | series  = 126
 | volume  = XCV1  
 | date = August 1972
 | access-date = December 21, 2015}}

</ref> describing the geometric method by which a bust of Hadrian was copied into marble, arguing that this three-dimensional geometry was used by many artist,<ref>{{cite web
 | url = http://www.math.wichita.edu/history/topics/geometry.html#geo-art
 | title = Topics in Geometry Art and Geometry
 | last = Eastman
 | first = Susan
 | date = 30 June 2010
 | website = math.wichita.edn
 | access-date = December 21, 2015}}
</ref> but in his research Konstam did not find evidence of any art historians referring to this technique as the basis for exploring solid and space.  Thus Konstam produced evidence of a revised way to think about drawing; accomplished by recognising the two traditions of form making (Greek and Roman) and distinguishing between them.<ref>{{Citation
| last      = Konstam
| first     = Nigel
| title     = Sculpture the Art and the Practice
| chapter   = 2
| edition   = second
| series    =  third revise
| publisher = Verrocchio Arts, Central Books
| place     = 99 Wallis Road LE9 5LN  via San MIchele 16, Casole d'Elsa, SI 53031
| year      = 2015
| page      = 26 
| isbn      = 0-9523568-1-3
}}
</ref> He has stated that a line drawn by Holbein <ref name=Holbein>Sculpture the Art and the Practice'', Second Edition, LE9 5LN, 2015, p. 146</ref> or Rembrandt is not conceived in the same way as one by Raphael <ref name=Raphael>Sculpture the Art and the Practice'', Second Edition, LE9 5LN, 2015, p.40</ref>  and his followers. Konstam’s understanding of Rembrandt was based on this different approach: that every mark Rembrandt makes relates to a position in space. Konstam noted this is the secret of Rembrandt’s success in capturing the spirit of the individual in his drawings <ref>{{Citation
| last      = Konstam
| first     = Nigel
| editor-last = Havell
| editor-first = Jane
| title      = Sculpture, the Art and the Practice: With the Riance Bronze Supplement
| publisher = Verrocchio Arts
| place     = Chester, United Kingdom
| year      = 2002
| type      = paperback
| edition   = 3rd Revised
| isbn      = 095 23568 13
}}

</ref> and has stated that Rembrandt is the most transparent artist of whom he has researched.

Konstam also noted his concern<ref>{{cite web
 | title = The Greek & Roman Dept. The British Museum
 | url = http://www.verrocchio.co.uk/nkonstam/blog/2014/11/
 | website = britishmuseum.org
 | date = 2014
 | access-date = October 4, 2016}}</ref> that The British Museum had altered a particular bust stating that "he was shocked to see that the ear lobes of Hadrian (room 70) had been ‘restored’. … thus they have repaired the chips in both of Hadrian’s ears which constitutes the clearest demonstration of my thesis: that three dimensional geometry was used and loved by the Romans and many great artists since (Rembrandt owned 30 Roman portraits and filled two books with drawings of them)” <ref>{{cite web
 | title = Inventory of Renbrandt’s Possessions
 | url = http://www.saverembrandt.org.uk/cms/content/view/81/98/
 | website = saverembrandt.org.uk
 | date = 2015
 | access-date = December 21, 2015}}</ref>

Konstam’s second contention was his discovery that ancient Greek sculpture was based on Life-casting, from approx 500BC onwards. This he has noted “could be regarded as shocking because it alters the foundations of art history as taught today e.g. Heinrich Wölfflin, Principles of Art History. The assumption has previously been that the Greeks arrived at their "Classical" phase by a leap of the imagination. Konstam has said “this version has less romantic appeal but is claimed to be the truth. He has documented that Lifecasting is why Greek stone sculptures show traces of being measured, but no explanation of what they were measured from. Life-casting explains why no sculptor since the Greeks has ever equaled their quality in terms of modeling veins or other small details of anatomy”.

==Rembrandt controversy==
[[File:Konstam Maquette of Musicians.JPG|thumb|Konstam Maquette mirror-image comparison to Rembrandt's Four Musicians with Instruments]]
Konstam's article, "[[Rembrandt]]'s Use of Models and Mirrors" was published in [[The Burlington Magazine]] February 1977<ref>
{{cite web
 | title = Rembrandt's Use of Models and Mirrors
 | url = http://burlington.org.uk/archive/back-issues/197702
 | magazine   = The Burlington Magazine Publications
 | series  = No 887
 | volume  = 119  
 | date = February 1977
 | access-date = December 21, 2015}}

</ref>  with the backing of Prof. Sir [[Ernst Gombrich]]. Benedict Nicolson, editor The Burlington, wrote in response "I find the evidence you have accumulated of the greatest possible interest, and so I am sure will Rembrandt scholars, who must now get down to revising the corpus of drawings!".<ref>{{cite web
 | title = The Burlington Magazine Publications
 | url = http://www.saverembrandt.org.uk/cms/content/view/78/96/
 | website = saverembrandt.org.uk
 | date = April 21, 1976
 | access-date = December 21, 2015}}
</ref> Lawrence Gowing, from The Slade School of Fine Art at the time also wrote in support,<ref>{{cite web
 | title = The Slade School of Fine Art - Lawrence Gowing
 | url = http://www.saverembrandt.org.uk/cms/content/view/80/97/
 | website = saverembrandt.org.uk
 | date = July 15, 1977
 | access-date = December 21, 2015}}
</ref> noting “Your view of the division between objective and imaginative seems to me, artistically and psychologically, much more comprehensible and satisfactory than anything before.” A similar article appeared in Rembrandthuiskroniek 1978.1[28] [[Max Wykes-Joyce]], in The International Herald Tribune, wrote, on 27 January 1976, of Konstam’s exhibition running at the Consort Gallery [[Imperial College]] to 13 February 1976 "...  which, by implication, contradicts much of the 20th-century criticism and scholarship. As a working artist, the case he makes for the redating and reconsideration of many of [[Rembrandt]]'s drawings is strong ... But certainly the exhibition is a seminal one which should not be lightly dismissed".<ref>{{cite web
 | title = London Max Wykes-Joyce
 | url = http://www.saverembrandt.org.uk/cms/content/view/79/93/
 | website = saverembrandt.org.uk
 | date = January 27, 1976
 | access-date = December 21, 2015}}
</ref>

Prof. Bryan Coles, Professor of Liberal Studies at [[Imperial College]] wrote  “these reconstructions (many of which compel assent) ... it would be a pity for scholarship not to profit from his (Konstam’s) imaginative researches”.<ref>{{cite web
 | title = Reviews A Sculptor’s Perspective
 | url = http://www.saverembrandt.org.uk/cms/content/view/76/94/
 | website = saverembrandt.org.uk
 | date = 1976
 | access-date = December 21, 2015}}
</ref> [[Rembrandt]] was the first artist Konstam discovered to be using mirrors and similar usage by [[Diego Velázquez]], [[Johannes Vermeer]] and [[Filippo Brunelleschi]] soon followed. The Konstam Maquette of Musicians comparative image is posited as solid evidence that Rembrandt used models and a mirror in the construction of his drawing of Four Musicians with Wind Instruments. The nearest flautist holds the flute the wrong way round and the oboist's hands are shown too close to the mouthpiece. This would not have occurred if the subject was drawn from true musicians. The second flautist has his instrument the right way round, proving the mirror reversal. Konstam has discovered 80 instances of this method of doubling the subject matter in Rembrandt's drawings. The Adoration of the Shepherds is the one instance where Konstam has found Rembrandt making two paintings, one from a mirror image and the other from life direct but both observed from the same position.{{citation needed|date=July 2016}} Thus geometry is part of the proof.

[[File:Rembrandt Harmensz. van Rijn 004.jpg|thumb|right|Rembrandt Harmensz. van Rijn 004 The Old Man Sitting in a Chair]]
By 1974 Konstam was contending he had proved the case that [[Rembrandt]] observed reality and reflections etc. to help create his masterpieces rather than using his imagination, the scholars' view, as noted in his eBook.<ref>{{cite web
 | title = Introduction to Rembrandt
 | url = http://www.nigelkonstam.com/cms/index.php/rembrandt-book
 | website = nigelkonstam.com
 | date = August 21, 2013
 | access-date = December 21, 2015}}
</ref> Konstam wrote to the [[Rembrandt Research Project]] on two occasions vigorously requesting they reattributed “The Old Man Sitting in a Chair” to [[Rembrandt]].<ref>{{cite web
 | title = Two Letters to the RRP
 | url = http://www.verrocchio.co.uk/nkonstam/blog/2014/07/02/two-letters-to-the-rrp/
 | website = verrocchio.co.uk
 | date = July 2, 2014
 | access-date = December 21, 2015}}
</ref> His view was dismissed at the time by the [[Rembrandt Research Project]].<ref>
{{cite web
 | url = http://www.rembrandtresearchproject.org/index.php?7
 | title = Rembrandt Research Project
 | website = rembrandtresearch.org
 | access-date = December 21, 2015}}
</ref> However, the project was terminated by the [[Rembrandt Research Project]] Board in 2011 and in 2014 Professor [[Ernst van de Wetering]], once chairman of the [[Rembrandt Research Project]] an accepted 'authority', or in terms of connoisseurship,<ref>{{cite web
 | url = http://www.nationalgallery.org.uk/paintings/glossary/connoisseur
 | title = Glossary Connoisseur
 | website = nationalgallery.org.uk
 | publisher = National Gallery
 | access-date = December 21, 2015}}
</ref> on [[Rembrandt]], gave his opinion in an article in [[The Guardian]]<ref>{{Citation
 | last1 = Brown | first1 = Mark
 | title = Rembrandt expert urges National Gallery to rethink demoted painting
 | newspaper = The Guardian
 | date = 23 May 2014
 | url = https://www.theguardian.com/artanddesign/2014/may/23/rembrandt-expert-national-gallery-painting-old-man-armchair
 | access-date = December 21, 2015}}
</ref> that the demotion of the 1652 painting of the Old Man Sitting in a Chair "was a vast mistake...it is a most important painting. The painting needs to be seen in terms of [[Rembrandt]]’s experimentation”. Not only is Van de Wetering convinced that Old Man in an Armchair is a genuine Rembrandt, but also that it is a pivotal work. “It is of wonderful quality and is revolutionary in a sense.”
<ref>{{cite web
| title = Rembrandt Expert Challenges British National Gallery
 | url = https://news.artnet.com/art-world/rembrandt-expert-challenges-british-national-gallery-26992
 | date = May 26, 2014
 | access-date = September 12, 2016}}
</ref>
In 2014 the [[National Gallery]] noted “This striking painting is evidence of Rembrandt’s profound influence on his contemporaries. But who painted it? … it has now been reattributed to an unknown contemporary follower of Rembrandt, probably working in the 1650s.”
<ref>{{cite web
 | url = http://www.nationalgallery.org.uk/paintings/follower-of-rembrandt-an-old-man-in-an-armchair
 | title = An Old Man in an Armchair 1650s Follower of Rembrandt
 | website = nationalgallery.org.uk
 | publisher = National Gallery
 | access-date = December 21, 2015}}</ref> Some commentators might not find this a very convincing argument either, though additional study is noted running counter to the view of Professor [[Ernst van de Wetering]].<ref>{{cite web
 | url = http://www.nationalgallery.org.uk/paintings/research/an-old-man-in-an-armchair
 | title = An Old Man in an Armchair
 | last = Wieseman
 | first = Marjorie E.
 | date = 30 June 2010
 | website = nationalgallery.org.uk
 | publisher = National Gallery
 | access-date = December 21, 2015
 | quote = Close Examination Fakes, Mistakes and Discoveries}}
</ref> Thus this particular debate continues.

In 2009 Konstam provided a demonstration of the London version of The Adoration of the Shepherds (also deattributed by the [[Rembrandt Research Project]]), noting that “it has to originate from a complex three dimensional group seen through a mirror”, the very group accepted as a [[Rembrandt]] in [[Munich]]. The [[National Gallery (London)]] subsequently reviewed their previous judgment and replaced the version they held, The Adoration of the Shepherds, among their [[Rembrandt]]'s, (see register 15 October. 2014–18 January 2015 <ref>{{cite web
 | last  = Wieseman
 | first = Marjorie E.
 | last2 = Bikker | first2 = J.
 | last3 = Hinterding | first3 = E.
 | last4 = Schapelhouman | first4 = M.
 | last5 = Godycki | first5 = A.
 | last6 = Packer  | first6 = L.
 | title = Rembrandt The Late Works, supplement with provenance, selected literature and bibliography 
 | website = nationalgallery.org
 | publisher = National Gallery
 | date = 2014
 | url = http://www.nationalgallery.org.uk/rembrandt-the-late-works-supplement
 | access-date = December 21, 2015 }}</ref>).  It had previously been languishing in the basement since the [[Rembrandt Research Project]] had insisted it was a 17th-century variant of a version in [[Munich]].

==Alternative approaches==

Other contributors to the debate of techniques used by artists included on 8 March 2012 [[Tim's Vermeer]], documentary film "Jan Vermeer and the Camera Obscura" which made the claim that a newly invented instrument, probably unknown to [[Vermeer]]1632 – 1675 in the Music Lesson 1664, thus disproving conclusively the previously thought idea that he used the [[camera obscura]] technique in this and other work.  On 14 March 2014, [[David Hockney]] presented a BBC Documentary, Secret Knowledge - Part One featuring Phillip Steadman’s book Vermeer’s Camera.<ref>{{Citation
| last      = Steadman
| first     = Philip
| title      = Vermeer's Camera : Uncovering the Truth Behind the Masterpieces
| publisher = Oxford University Press
| place     = City/Country Oxford, United Kingdom
| year      = 2002
| type      = paperback
| isbn      = 019 280302 6}}
</ref>

Konstam disputed this in his third film on Vermeer, pointing out that [[Johan Vermeer]] died in debt had a large family and would have needed a very large studio (over 6m in length) whereas Konstam's explanation needs only a small studio of just over 3&nbsp;m. Furthermore, the image from a 17th-century camera is impractically small and weak. Konstam also noted the strange way of viewing [[Filippo Brunelleschi]]'s first experiment in perspective, which he claimed was easily explained following Manetti's description precisely.

==Later developments==

Konstam has offered a summary of his contention in the [[Journal of Information Ethics]] published by McFarland Books in 2015.<ref>{{Citation
 | title = Journal of Information Ethics - Academic Barriers to Discussion
 | volume  = Vol 24 
 | issue  = 2 
 | pages = 96–104
 | url = http://www.mcfarlandbooks.com/customers/journals/journal-of-information-ethics/
 | place  =  Jefferson NC 28640
 | website =  Macfarlandbooks.com
 | date = 2015
 | access-date = February 8, 2016}}.</ref>  His assertion is that 'Rembrandt specialists' have subconsciously determined to convert the artist to 20/21st Century mores or contemporary beliefs; the preference for imagination over observation whereas Rembrandt's contemporaries all insisted "he observed, anything else was worthless in his eyes". Konstam noted that Rembrandt was very much a part of his time, "born into the middle of the revolution in science which started with Copernicus and Galileo; a revolution based on the rejection of the hypothetical philosophy inherited from the Greeks, Aristotle in particular, and relied instead on careful observation, measurement and logical deduction from the data."<ref>http://www.nigelkonstam.com/cms/index.php/rembrandt-book</ref> He has argued that Rembrandt embodies the same spirit in art, insisting that direct observation was the mainstay of Rembrandt’s achievement and that contemporary understanding of the nature of the visual imagination is misguided. Konstam has espoused his theory in a [[YouTube]] film.<ref>{{YouTube | id= XmuPjn0QLWU| title= ''Rembrandt Resurrected''}}</ref>

==References==
{{Reflist|30em}}

==External links==
*Nigel Konstam on Rembrandt's Adoration Paintings 2009 https://www.youtube.com/watch?v=io767gJDc2k
*Hockney BBC Secret Knowledge - Part One https://www.youtube.com/watch?v=ynrnfBnhWSo
*Konstam - Reality and Reflection in One Drawing http://www.saverembrandt.org.uk/cms/content/view/17/33/
*Konstam - Brunelleschi's First Experiments in Perspective demonstrates 28 Jun 2011 https://www.youtube.com/watch?v=frB-XiXeNiQ
*BBC film 1976: Nigel Konstam demonstrates Rembrandt's use of live models and mirrors https://www.youtube.com/watch?v=ZfQ0fcZBhak
*Konstam - Vermeer's Methods by Nigel Konstam Part 1 https://www.youtube.com/watch?v=Z9ur5UtP59w
*Konstam 2014 - Sculptor, Interview https://www.youtube.com/watch?v=9Cglw0X2wHQ
*Konstam - Summary of YouTube Art Videos http://www.nigelkonstam.com/cms/index.php/youtube-videos-by-nigel-konstam

{{DEFAULTSORT:Konstam, Nigel}}
[[Category:1932 births]]
[[Category:Living people]]
[[Category:Artists from London]]
[[Category:British art historians]]
[[Category:British sculptors]]
[[Category:British male sculptors]]
[[Category:Departments of University College London]]
[[Category:University of the Arts London]]
[[Category:Rembrandt]]