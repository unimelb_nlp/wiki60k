{{Infobox military conflict
|conflict= Battle of Maychew
|partof= the [[Second Italo-Abyssinian War]]
|image=
|caption=
|date= 31 March 1936
|place= Near [[Maychew]], [[Tigray Region|Tigray]], [[Ethiopia]]
|result=Decisive Italian victory
*Destruction of Haile Selassie's last army in the north
|combatant1={{flag|Kingdom of Italy}}
*[[File:Eritrea COA.svg|13px]] [[Italian Eritrea]]
|combatant2={{flag|Ethiopian Empire}}
|commander1={{flagicon|Kingdom of Italy}} [[Pietro Badoglio]]
|commander2={{flagicon|Ethiopian Empire}} [[Haile Selassie I of Ethiopia|Haile Selassie]]
|strength1=40,000 (with another 40,000 in reserve)<ref name="Barker97">[[#Barker|Barker 1971]], 97.</ref>
|strength2=31,000 (including six battalions of the [[Kebur Zabangna|Imperial Guard]])<ref name=Barker97/>
|casualties1=400 Italians killed and wounded, 873 Eritreans killed and wounded<ref name=Barker97/>
|casualties2=Between 1,000 and 8,000 killed,<ref name=Barker97/> roughly 11,000 total casualties<ref name=Barker105>[[#Barker|Barker 1971]], 105.</ref>
}}
{{Campaignbox Second Italo-Abyssinian War}}

The '''Battle of Maychew''' (also known as the '''Battle of Mai Ceu''') was the last major battle fought on the northern front during the [[Second Italo-Abyssinian War]]. The battle consisted of a failed [[counterattack]] by the Ethiopian forces under [[Emperor of Ethiopia|Emperor]] [[Haile Selassie I of Ethiopia|Haile Selassie]] making [[frontal assault]]s against prepared [[Italian Colonial Empire|Italian]] defensive positions under the command of [[Marshal]] [[Pietro Badoglio]]. The battle was fought near [[Maychew]] (Mai Ceu), [[Ethiopia]], in the modern region of [[Tigray Region|Tigray]].

==Background==

On 3 October 1935, [[General]] [[Emilio De Bono]] advanced into Ethiopia from [[Eritrea]] without a [[declaration of war]], leading a force of approximately 100,000 Italian and 25,000 Eritrean soldiers towards the Ethiopian capital of [[Addis Ababa]]. In December, after a brief period of inactivity and minor setbacks for the Italians, De Bono was replaced by Badoglio.{{citation needed|date=May 2014}}

Under Badoglio, the advance on Addis Ababa was renewed. Badoglio overwhelmed the armies of ill-armed and uncoordinated Ethiopian warriors with [[mustard gas]], tanks, and heavy artillery.<ref>[[#Laffin|Laffin]], 28.</ref> He defeated the Ethiopian armies at the [[Battle of Amba Aradam]], the [[Second Battle of Tembien]], and the [[Battle of Shire]].

===Quorom and Maychew===
On 1 March 1936, Emperor [[Haile Selassie I|Haile Selassie]] arrived by foot at his new headquarters in [[Korem|Quorom]].  He arrived forty years to the day from the decisive Ethiopian victory at [[Battle of Adwa|Adwa]] during the [[First Italo-Ethiopian War]]. On 19 March, both [[Ethiopian aristocratic and court titles|''Ras'']]{{#tag:ref|Roughly equivalent to [[Duke]].|group=nb}} [[Kassa Haile Darge]] and ''Ras'' [[Seyum Mangasha]] made their way to Quorom to join the Emperor.  In addition, ''Ras'' [[Getachew Abate]] arrived with a fresh army from [[Kaffa Province, Ethiopia|Kaffa Province]].<ref>Mockler. p. 113</ref>  The Emperor divided his army into four groups.  He arranged that one group would be commanded directly by himself and that the other three groups would be commanded by ''Ras'' Kassa, ''Ras'' Seyum, and ''Ras'' Getachew.<ref>Haile Selassie I, Volume I, p. 277</ref>

Compared to other Ethiopian forces, Haile Selasie's army was extremely well armed. He had an [[artillery]] regiment of twenty 75&nbsp;mm field guns, some Oerlikon 37&nbsp;mm guns, and even a few 81&nbsp;mm Brandt mortars. However, compared to the resources available to Badoglio, Haile Selasie's army was hopelessly outmatched.<ref name=Barker97/>  To even things up, the Emperor handed out between ten and fifteen dollars and distributed other gifts to the [[Oromo people|Azebu Galla]].  In exchange, they swore their allegiance to him and agreed to attack the Italian flanks.<ref>Mockler, p. 116</ref>

Badoglio had the four divisions of the Italian I Army Corps and the three divisions of the Eritrean Corps at Maychew.<ref name=Barker97/>  Before the battle, the Marshal explained: "The Emperor has three choices. To attack, and be defeated; to wait for our attack, and we will win anyway; or to retreat, which is disastrous for an army that lacks means of transport and proper organization for food and munitions."<ref>[[Time Magazine]], 13 April 1936</ref>  Badoglio also enjoyed the intelligence edge of being able to intercept most of the Ethiopian radio communications.

On 21 March, Haile Selassie sent a radio message to  his wife, Empress [[Menen Asfaw]]:{{citation needed|date=May 2014}}
:"Since our trust in our Creator and in the hope of His help and as '''we have decided to advance''' and enter the fortifications and since God is our only help, confide this decision in secret to the [[Abuna]], to the ministers and to the dignitaries and offer unto God our fervant prayers."
As soon as Badoglio intercepted this message indicating that Haile Selassie had decided to advance, he cancelled orders for his own proposed offensive.  Instead, Badoglio would prepare defensive positions for an Ethiopian attack.<ref>Mockler, p. 114</ref>

On 23 March, looking across a lush green valley towards the Italian positions at [[Maychew]], the Emperor contemplated his decision to strike first. His army was the last intact Ethiopian army between Badoglio and Addis Ababa. He decided he would direct the attack personally in accordance with tradition and the expectation of his followers. Six battalions of the [[Kebur Zabangna|Imperial Guard]] (''[[Kebur Zabangna]]'') would be part of his force of approximately 31,000 fighters. Haile Selassie chose to attack against the advice of his foreign experts and against his own better judgement.<ref name=Barker97/><ref name=Marcus>[[#Marcus|Marcus]], 145-6.</ref>

Had Haile Selassie attacked on 24 March as he originally planned, things may have gone differently; many of the Italians had only recently arrived at Maychew after the fall of [[Amba Aradam]]. But, during a week frittered away by the Ethiopians in war councils, banquets, and prayers, the Italians had time to strengthen their defenses and time to bring up reserves.<ref name=Barker97/>

==Battle==
At dawn on 31 March 1936, the [[Frontal assault|attack was launched]]. It was [[St. George's Day]]. The attack began at 0545 hours and continued for thirteen hours with little or no let up.<ref name=Barker97/>

The Italians had been "standing to" in the [[front line]] positions all night, alerted to the attack by an Ethiopian deserter. The [[Mountain warfare|mountain troops]] (''[[Alpini]]'') of the [[5 Alpine Division Pusteria|5th "Pusteria" Mountain Division]] were dug in on the slopes of Amba Bokora for the Italian I Corps. The rest of the I Corps was in reserve, the [[Italian 26 Infantry Division Assiette|26th "Assietta" Infantry Division]], the [[Italian 30 Infantry Division Sabauda|30th "Sabauda" Infantry Division]], and the [[Italian 4 Blackshirt Division 3 Gennaio|4th "3rd January" Blackshirt Division]]. The two Eritrean divisions of the Eritrean Corps held Mekan Pass, the [[Italian 1st Eritrean Division|1st Eritrean Division]] and the [[Italian 2nd Eritrean Division|2nd Eritrean Division]].<ref name=Barker97/>  The [[Italian 1 Blackshirt Division 23 Marzo|1st "23rd March" Blackshirt Division]] was in reserve for the Eritrean Corps.

The Ethiopians advanced in three columns of 3,000 men each. In the first attacks, the Ethiopians hurled themselves at the Italian positions in waves. The fury of the attack and surprisingly accurate mortar fire carried the Ethiopians well into the defensive lines of the "Pusteria" Division. But the mountain troops struck back and soon the front lines were stabilized.<ref name=Barker97/>

===Switch to the left flank===
The Ethiopians switched the focus of their attack and fifteen thousand men under ''Ras'' Kassa<ref name="Mockler, p. 117">Mockler, p. 117</ref> advanced against the Eritreans holding Mekan Pass on the Italian left flank.  Haile Selassie hoped to face less resistance from the Eritreans.{{#tag:ref|Barker states this.<ref>Barker, p. 97</ref>|group=nb}}  From 0700 to 0800 hours, the Ethiopians kept up a steady onslaught and, despite taking heavy casualties, were beginning to make gains. But at 0800 Badoglio unleashed the bombers of the Italian Royal Air Force (''[[Regia Aeronautica]]'') and the Ethiopians could hear the ominous engine roar as they closed in with poison gas.<ref name=Barker97/>

===Imperial Guard sent in===
Haile Selassie now played his trump card. The Imperial Guard, under the command of ''Ras'' Getachew Abate, was sent in against the Eritreans.<ref name="Mockler, p. 117"/> The training and discipline of this elite force was apparent in the methodical mode of their advance over the open ground. For three hours they struggled to roll up the Italian flank. The X Battalion of the [[Italian 2nd Eritrean Division|2nd Eritrean Division]] was virtually annihilated. In the end, the Italian commander of the unit called down concentrated artillery fire onto his own overrun positions and saved the day.<ref name=Barker97/>

===Last attack===
By 1600 hours, it was apparent that the Imperial Guard was not going to be able to capture their objectives and Haile Selassie played his last card. He ordered an attack along the entire front. This last desperate action was again made by three columns, it was made under a heavily overcast sky, and it was made with little chance of success. The Ethiopians attacked everywhere and were driven back.<ref name=Barker97/>  It was at this point that the [[Oromo people|Azebu Galla]], who had been on the sidelines, made their allegiance clear and attacked the withdrawing Ethiopians.<ref>Mockler, p. 118</ref>

Haile Selassie's order to retreat was to be late in coming. He placed ''Ras'' Getachew Abate as [[Ethiopian aristocratic and court titles|''Asmach'']].{{#tag:ref|Roughly equivalent to Commander of the Rear Guard.|group=nb}} But the Ethiopians had lost many front line commanders, the soldiers had not eaten since before dawn, and discipline had understandably broken down. To make matters even worse, as the Ethiopians fled from the battlefield, they were mercilessly bombed from above by the Italian Royal Air Force and harassed on the ground by the Azebu Galla.<ref name=Barker98>[[#Barker|Barker 1971]], 98.</ref>

==After the battle==
On the evening of 31 March, Haile Selassie sent another message to his wife:  
:"From five in the morning until seven in the evening our troops attacked the enemy's strong positions, fighting without pause. We also took part in the action and by the grace of God remain unharmed. Our chief and trusted soldiers are dead or wounded. Although our losses are heavy, the enemy too has been injured. The [[Kebur Zabangna|Guard]] fought magnificently and deserve every praise. The [[Amhara people|Amhara]] troops also did their best. Our troops, even though they are not adapted for fighting of the European type, were able to bear comparison throughout the day with the Italian troops."<ref name=Barker98 />

Many of the Ethiopian commanders now prepared to make for their own lands.  [[Ethiopian aristocratic and court titles|''Dejazmach'']]{{#tag:ref|Roughly equivalent to Commander of the Gate.|group=nb}} [[Wondosson Kassa]], one of ''Ras'' Kassa's sons, was to go to [[Lasta]] south of [[Wag]], his grandfather's country.  In Lasta the inhabitants were both warlike and loyal to the "Shoan Emperor," Haile Selassie.  ''Dejazmach'' [[Aberra Kassa]], one of ''Ras'' Kassa's other sons was to go to the Kassa [[fief]] of Salale in northern Shoa.  ''Ras'' Seyum was ordered to return to [[Tigray (province)|Tigray]] and wage [[Guerrilla warfare|guerrilla war]].  ''Ras'' Kassa and ''Asmach'' Getachew, with the remnants of their own forces and with the remnants of the Guard, accompanied Haile Selassie as he made his way into the friendly highlands of Wag and Lasta and away from the snarling Azebu Galla.<ref>Mockler, pp. 121-122</ref>

==Retreat==
On the night of 2 April, the Emperor finally ordered a retreat.  The retreating columns set off before dawn the next day towards [[Lake Ashangi]] and the highlands of Quorom.  Haile Selassie, wearing a [[pith helmet]], rode a white horse and the retreat was initially not chaotic.  In the early morning, circumstances changed as two latent threats materialized.  The Azebu Galla started attacking the flanks and Italian aircraft arrived.  The Imperial Guard, as part of the rear guard commanded by ''Asmach'' Getachew, lost more men over the next two days than were lost during the battle.<ref name="Mockler, p. 121">Mockler, p. 121</ref>

Late on 3 April, the Ethiopian columns reached Quorom and the relative safety of the highlands.  It was now decided that the columns would be dispersed.  As a result, all semblance of order and organization were lost.<ref name="Mockler, p. 121"/>  In the early morning of 4 April, the battle weary and thirsty survivors of the Emperor's army struggled towards Lake Ashangi. Roughly 20,000 Ethiopians crossed the open plain towards Lake Ashangi and, due to brutal attacks from the Azebu Galla and due to near continuous attacks from the air, thousands would be lost before they got close to the lake's shore. Worse, the water of Lake Ashangi had been sprayed with deadly chemicals by the Italian Royal Air Force and it was poison by the time the Emperor's army arrived.  Late on 4 April, Haile Selassie looked with despair upon the horrific sight of the dead bodies of much of his army ringing the poisoned lake.<ref name=Barker105 />

===Pilgrimage to Lalibella and plan to stand at Dessie===
It was believed among the Ethiopians that it might be possible to make a stand at Dessie. The [[Crown Prince]] [[Asfa Wossen]] had been sent there to raise a new army.  With the Crown Prince in Dessie were [[Ethiopian aristocratic and court titles|''Shum'']]{{#tag:ref|Roughly equivalent to [[Governor]].|group=nb}} [[Wadajo Ali]] and [[Ethiopian aristocratic and court titles|''Fitawrari'']]{{#tag:ref|Roughly equivalent to Commander of the Advanced Guard.|group=nb}} [[Fikremariam]].  Wadajo Ali was the "real" Governor of [[Wollo]] and Fikremariam commanded the Guard and the [[Shewa]]n garrison at Dessie.  Ammunition and supplies were accumulated in anticipation of protracted operations in the north.<ref name="Mockler, p. 122">Mockler, p. 122</ref>

Before getting to Dessie, the Emperor decided to make a pilgrimage to the holy city of [[Lalibella]].  Included in the Emperor's retinue was [[Coptic Orthodox Church of Alexandria|Coptic]] ''[[Abuna]]''{{#tag:ref|Roughly equivalent to [[Metropolitan bishop|Metropolitan Bishop]].|group=nb}} Petros, [[Ethiopian ecclesiastical titles|Etchage]]{{#tag:ref|Roughly equivalent to [[Abbot]].|group=nb}} [[Gabre Giyorgis]], ''Ras'' Kassa, ''Dejazmach'' [[Adafersaw Yenadu]],  ''Dejazmach'' Wondosson Kassa, and ''Dejazmach'' Aberra Kassa.  From 12 April<ref name="Barker, p. 106">Barker, p. 106</ref> the Emperor spent three days there in prayer.<ref>Mockler, p. 123</ref>  On 15 April, Haile Selassie left Lalibella and rejoined his army as it continued its plodding march towards Dessie.  At [[Magdala]], the Emperor was to learn that the Crown Prince had abandoned Dessie on 14 April without firing a shot.  He also learned that the city was already occupied by the Eritreans.  The Emperor's column turned towards [[Were Ilu|Worra Ilu]], but runners then brought the news that Worra Ilu had fallen too.  By forced march, the Emperor and his party made their way to [[Fiche|Fikke]] in Salale.<ref name="Barker, p. 106"/>

On 20 April, Marshal Badoglio flew to Dessie.  He noted great strips of cloth stretched across the decorated streets.  On the cloth, the local population had written:  "The Hawk has flown."<ref name="Mockler, p. 122"/>

On 26 April, when Badoglio launched his "[[March of the Iron Will]]" towards Addis Ababa, he faced no meaningful Ethiopian resistance.<ref name=Barker109>[[#Barker|Barker 1971]], 109.</ref>

===Addis Ababa===
From Fikke, Haile Selassie made his way towards Addis Ababa. By 1 May, he had arrived in the capital.  This was one month after the Battle of Maychew. With him were ''Ras'' Kassa and ''Asmach'' Getachew. When the Emperor arrived, he found a city in a state of near panic.<ref name=Mockler133>[[#Mockler|Mockler]], 133.</ref>

==See also==
* [[Ethiopian Order of Battle Second Italo-Abyssinian War]]
* [[Army of the Ethiopian Empire]]
* [[Italian Order of Battle Second Italo-Abyssinian War]]
* [[Royal Italian Army]]

== Notes ==
;Footnotes
{{reflist|2|group=nb}}

;Citations
{{reflist|2}}

== References ==
* {{cite book| last=Barker| first=A.J.| title=The Rape of Ethiopia, 1936| publisher=Ballantine Books| year=1971|location=New York |isbn=978-0-345-02462-6|pages= |ref=Barker 1971}}
* {{cite book| first=A.J.| last=Barker| title=The Civilizing Mission: A History of the Italo-Ethiopian War of 1935-1936| publisher=Dial Press| year=1968|location=New York |isbn= |pages= |ref=Barker 1968}}
* {{cite book| first=Translated and Annotated by Edward Ullendorff| last=Haile Selassie I| title=My Life and Ethiopia's Progress:  The Autobiography of Emperor Haile Selassie I, King of Kings and Lord of Lords, Volume I: 1892-1937 | publisher=Research Associates School Times Publications | year=1999 |location=Chicago |isbn= 0-948390-40-9 |page=338 }}
* {{cite book| first=Edited by Harold Marcus with others and Translated by Ezekiel Gebions with others| last=Haile Selassie I| title=My Life and Ethiopia's Progress:  The Autobiography of Emperor Haile Selassie I, King of Kings and Lord of Lords, Volume II | publisher=Research Associates School Times Publications | year=1999 |location=Chicago |isbn= 0-948390-40-9 |page=190 }}
* {{cite book| last=Laffin| first=John| title=Brassey's Dictionary of Battles| publisher=Barnes & Noble Books| year=1995|location=New York |isbn=0-7607-0767-7|pages= |ref=Laffin}}
* {{cite book| last=Marcus| first=Harold G.| title=A History of Ethiopia| publisher=University of California Press| date=February 2002| isbn=978-0-520-22479-7| pages=142–6| ref=Marcus| url=https://books.google.com/books?id=hCpttQcKW7YC&pg=PA145&vq=Maychew}}
*{{cite book| last=Mockler| first=Anthony| title=Haile Sellassie's War| publisher=Olive Branch Press| year=2003| location=New York| isbn=978-1-56656-473-1 |ref=Mockler}}
* {{cite book| last=Nicolle| first=David| title=The Italian Invasion of Abyssinia 1935-1936 | publisher=Osprey| year=1997|location=Westminster, MD |isbn=978-1-85532-692-7|pages= |ref=Nicole}}

==External links==
*{{cite news|url=http://www.time.com/time/magazine/article/0,9171,755962,00.html|title=Hit & Run.|work=Time Magazine|date=April 13, 1936|accessdate=August 3, 2009}}

{{coord missing|Ethiopia}}

{{DEFAULTSORT:Battle Of Maychew}}
[[Category:1936 in Ethiopia|Maychew]]
[[Category:Conflicts in 1936|Maychew]]
[[Category:Battles of the Second Italo-Ethiopian War|Maychew 1936]]
[[Category:Battles involving Ethiopia|Maychew 1936]]
[[Category:Battles involving Italy|Maychew 1936]]
[[Category:Tigray Region]]
[[Category:March 1936 events]]