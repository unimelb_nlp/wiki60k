{{Infobox journal
| title = Family Business Review
| cover = 
| editor = Pramodita Sharma 
| discipline = [[Business]]
| former_names = 
| abbreviation = Fam. Bus. Rev.
| publisher = [[SAGE Publications]]
| country = United Kingdom
| frequency = Quarterly
| history = 1988–present
| openaccess = 
| license = 
| impact = 2.426 
| impact-year = 2010
| website = http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal201921
| link1 = http://fbr.sagepub.com/content/current
| link1-name = Online access
| link2 = http://fbr.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 16115306 
| LCCN = sf93092906 
| CODEN = 
| ISSN = 0894-4865 
| eISSN = 1741-6248 
}}

'''''Family Business Review''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that publishes papers in the field of [[Business]]. The journal's [[Editor-in-Chief|editor]] is Pramodita Sharma ([[University of Vermont]]) ([[Babson University]]). It has been in publication since 1988<ref>{{cite web|title=Entrepreneurship Education Chronology|url=http://www.slu.edu/eweb/connect/for-faculty/infrastructure/entrepreneurship-education-chronology|work=Saint Louis University|accessdate=19 October 2015}}</ref> and is currently published by [[SAGE Publications]] in association with [[Family Firm Institute]].

== Scope ==
''Family Business Review'' seeks to explore the dynamics of family-controlled enterprise, including firms ranging in size from the very large to the relatively small. The scholarly journal publishes interdisciplinary research on families of wealth and the family office covering such areas as succession planning, the impact of family dynamics on managerial behaviours and estate and tax planning.

== Abstracting and indexing ==
''Family Business Review'' is abstracted and indexed in, among other databases:  [[SCOPUS]], and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2013 [[impact factor]] is 4.243, ranking it #4 out of 110 journals in the category ‘Business’.<ref name=WoS>{{cite book|year=2011|chapter=Journals Ranked by Impact: Business|title=2010 Journal Citation Reports|publisher=[[Thomson Reuters]]|edition=Social Sciences |accessdate=30 September 2011|work=Web of Science |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|1=http://fbr.sagepub.com/}}
* {{Official website|http://www.ffi.org/|FFI official website}}

[[Category:English-language journals]]
[[Category:Publications established in 1988]]
[[Category:SAGE Publications academic journals]]