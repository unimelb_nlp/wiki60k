{{Infobox musical artist 
| name                = K²
| image               =
| landscape           = yes
| caption             = L-R: Ryo Okumoto, Brent George, Doug Sanborn, Steve Poloni and Ken Jaquess - Live in the Netherlands, 2012
| background          = group_or_band
| origin              = [[United States|America]]
| genre               = [[Progressive rock]], [[jazz fusion]]
| years_active        = 2001-present
| label               = [[Magna Carta Records]], [[ProgRock Records]]
| associated_acts     = [[Spocks Beard]], [[GPS (band)|GPS]], [[The Untouchables (band)|The Untouchables]], Atlantis, Code Red
| website             = http://www.reverbnation.com/k2band
| current_members     = Ken Jaquess<br>Doug Sanborn <br>[[Ryo Okumoto]]<br>Steve Poloni<br>Brent George
| past_members        = [[Allan Holdsworth]]<br>[[John Miner]]<br>[[Yvette Devereaux]]<br>Josh Gleason<br>Shaun Gurein<br>Karl Johnson
}}

'''K²''' is a Progressive Rock Band from Los Angeles, California. The band was composed of bassist/keyboardist Ken Jaquess (Atlantis, New Cross), keyboardist [[Ryo Okumoto]] ([[Spocks Beard]], [[Natalie Cole]]), drummer Doug Sanborn ([[The Untouchables (band)|The Untouchables]]), electric violinist [[Yvette Devereaux]], singer Shaun Guerin, guitarist [[Allan Holdsworth]] ([[Tempest (UK band)|Tempest]], [[Soft Machine]], [[Gong (band)|Gong]] and [[U.K. (band)|U.K.]]) and guitarist [[John Miner]] ([[Art Rock Circus]]).

== History ==

K² was formed from the ashes of "Atlantis" (another Prog band from L.A.) and was founded by Ken Jaquess. When Ken's previous group "Atlantis" disbanded in 2000, he was left with a full albums worth of new material that was originally to become "Atlantis'" third album. The untitled project needed a new vehicle and Ken decided to take the advice of an old friend and finally produce a solo album. Picking the moniker "K-Squared" or "K²", Ken embarked on an journey that would last 4 years and eventually end with the release of K²'s first album, "Book of the Dead".<ref>[http://www.seaoftranquility.org/reviews.php?op=showcontent&id=2219 Book of the Dead<!-- Bot generated title -->]. Review, ''Sea of Tranquility''</ref>

== Formation and first album ==

K² began quite by accident in May 2001 when bassist Ken Jaquess asked drummer Doug Sanborn to play on some new music he was working on. Having no idea if anything would come of these sessions, they set out to record a bold and daring project that would soon take on a life all its own. After only 4 rehearsals and 5 hours of recording, Ken and Doug had laid the foundation for an epic album of high musicianship, complex compositions and haunting themes.

Over the next four years, fellow musicians Allan Holdsworth, Shaun Guerin, Ryo Okumoto, Yvette Devereaux & John Miner added layer upon layer of unique, signature sounds that resulted in K²'s first album [[Book of the Dead]]. Sadly, just after recording his vocals, original vocalist Shaun Guerin<ref>[http://mi2n.com/press.php3?press_nb=54813 Shaun Guerin<!-- Bot generated title -->]</ref> died. Critically acclaimed around the world, K² went on to perform BOTD live with a new line-up consisting of Ken and Doug, Ryo Okumoto (keyboards), Karl Johnson (guitar), and Josh Gleason (vocals). In 2006, K² filmed their first gig, which was released on their 2008 concert DVD, K² Live in Hollywood.

== Black Garden ==
   [[File:BGTI.jpg|thumb|Tour 2012]]
In 2007, this lineup went back to the drawing board to create what would become their long overdue 2nd release, Black Garden.<ref>[http://www.seaoftranquility.org/reviews.php?op=showcontent&id=9624 Black Garden<!-- Bot generated title -->]</ref> Released September 21, 2010 on [[Magna Carta Records]], this outing showcases this modern rock quintet in an entirely new setting. With a central theme of Oceania, K² explores the hidden depths of this mysterious region and take the listener on a dark, sonic journey to places rarely visited, and offers a glimpse of a people unlike any other on earth. K² then followed this release with a short east coast tour that saw the band gaining new fans and appreciation, with an amazing gig in Boston, that was the high point of the tour.

== Third album ==

After the tour, writing and recording of new material commenced with a couple of epics and some shorter songs working their way thru the band. During this time, there was a line-up change with vocalist Brent George and guitarist Steve Poloni coming on board. With this change it was decided to head back out on the road, this time to Europe. K²'s 2012 European Tour was an amazing experience for the whole band, making new friends and adding more fans with each stop. Jumping over the "Pond" proved to be a resounding success for K², playing all over England and ending in the Netherlands. A new DVD of their concert filmed in Rotterdam @ the iO Pages Festival during the tour has just finished production. Originally meant to be a bonus disc for their 3rd album, it came out better than anyone expected! It will now be released as a separate for ″purchase″ DVD, September 15, 2015. Also, K² are finishing the recording of their untitled 3rd album. They hope for a Fall release and another tour to follow.

== Musical style ==

K²'s music is characterized by skilled musicianship, odd-numbered time signatures, mixed meters and unusually varied keyboard, bass and drum interplay, with an emphasis on darker, symphonic compositions.

== Present Members ==

*   Ken Jaquess:   Bass Guitar, Keyboards (2001–present)
* [[Ryo Okumoto]]: Keyboards (2002–present)
*   Steve Poloni:  Guitar (2012–present)
*   Doug Sanborn:  Drums, Percussion (2001–present)

== Former ==
* Brent George: Vocals (2012-2014)
* Josh Gleason: Vocals (2005–2011)
* Shaun Guerin: Vocals (2004)
* [[Allan Holdsworth]]: Guitar (2004)
* Karl Johnson: Guitar (2005–2011)
* [[John Miner]]: Guitar (2004)

== Guest Artist's ==

* [[Allan Holdsworth]]: Guitar (2004)
* [[Yvette Devereaux]]: Electric Violin (2004)
* [[John Miner]]: Guitar (2004)

=== Discography ===

== Studio albums ==

* ''Book of the Dead'' (2005)
* ''Black Garden'' (2010)
   
== DVD's ==

* ''K²-Live In Hollywood'' (2008)
* ''K²-Live @ iO Pages Festival'' (2015)

== Reviews ==

* [http://www.seaoftranquility.org/reviews.php?op=showcontent&id=2219 Sea of Tranquility]
* [http://www.backgroundmagazine.nl/QLinks/K2BookOfDead.html Background Magazine]
* [http://www.rockeyez.com/reviews/cd/book_of_the_dead/rev-book_of_the_dead-K2.html RockEyez]
* [http://www.cduniverse.com/search/xx/music/pid/6854193/a/book+of+the+dead.htm CD Universe]
* [http://www.musicstreetjournal.com/index_cdreviews_display.cfm?id=102734 Music Street Journal]
* [http://www.technologytell.com/blog/4800/k2-peter-gabriel-genesis-ryo-okumoto-spocks-beard-progressive-rock-cd/ Technology Tell]
* [http://www.progarchives.com/album.asp?id=30633 Prog Archives]
* [http://www.amarokprog.net/critiques_35003_201151.html Amarok Prog]
* [http://www.stormbringer.at/reviews.php?id=5740 Stormbringer]
* [http://www.home-of-rock.de/CD-Reviews/K2/Black_Garden.html Home of Rock]
* [http://www.musikexpress.de/das-archiv/article275736/k2.html Musik Express]
* [http://www.progrock.co.uk/acatalog/K2.html Prog Rock UK]
* [http://www.progrock.co.uk/acatalog/K2.html True Metal]
   
== References ==

{{Reflist}}

== External links ==
* [http://www.reverbnation.com/k2band K² EPK website]
* [http://www.facebook.com/pages/K2/54333771423 K² Facebook website]

{{DEFAULTSORT:K2 (band)}}
[[Category:Musical groups from Los Angeles]]
[[Category:Progressive rock musical groups from California]]