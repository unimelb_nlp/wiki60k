'''Elliot Christopher Cowdin I''' (August 8, 1819 - April 12, 1880) was an American businessman and politician. He served one term in the New York State Assembly in 1876, representing the 11th district.<ref name="NYT obituary"/>

== Life ==
Elliot C. Cowdin was the son of Angier and Abiah (Carter) Cowdin. He born in on August 8, 1819, in [[Jamaica, Vermont]], but grew up mainly in [[Boston, Massachusetts]], where the family moved to after his father's death.

Cowdin became involved in the silk trade in the 1840s and, in 1852, he moved to New York City to establish his own business, Elliot C. Cowdin & Company, an importing firm that specialized in "ribbons, silks, flowers and other Paris novelties."<ref name="NYT obituary" />  The business had a branch office in Paris, France and Cowdin maintained a second home there. He was also active in politics; as a speaker and lecturer; and in several civic organizations, including the [[New York Chamber of Commerce]], the [[New England Society of New York]], the Bedford Farmers' Club, and the [[Union League]], which he had helped to found.<ref name="NYT obituary" />

Cowdin married Sarah Katharine Waldron (1827-1903), the daughter of Samuel W. and Martha (Melcher) Waldron, on September 13, 1853. They had six children: Katharine W., [[John Elliot Cowdin|John Elliot]], Martha G., Winthrop, Alice, and Elliot.

Elliot Cowdin died at his home in New York on April 12, 1880.

== Political career ==
Cowdin held some appointed posts in government, including that of U.S. Commissioner to the Paris Expedition in 1867.  He was also a member of the [[100th New York State Legislature]] in 1876, elected to the New York State Assembly as a Republican representative of the 11th District (New York County). His interests as an Assemblyman, as indicated by the bills he introduced, included government reform, taxation and finance, and trade and commerce.<ref name="Cowdin finding aid">{{cite web|title=Elliot Christopher Cowdin (finding aid)|url=http://www.nysl.nysed.gov/msscfa/sc22723.htm|work=New York State Library|accessdate=24 March 2014}}</ref>  Cowdin served just one term, declining afterward to run for reelection.<ref name="NYT obituary">{{cite news|title=Elliot C. Cowdin Dead  |url=https://query.nytimes.com/mem/archive-free/pdf?res=F10914FB34551B7A93C1A8178FD85F448884F9|accessdate=24 March 2014|newspaper=New York Times|date=13 April 1880}}</ref>

== References ==
{{Reflist}}

== External links ==
*[http://politicalgraveyard.com/bio/cowart-cowpland.html#910.13.68 Elliot C. Cowdin] at [[The Political Graveyard]]
*{{findagrave|19147066}}

{{DEFAULTSORT:Cowdin, Elliot C}}
[[Category:1819 births]]
[[Category:1880 deaths]]
[[Category:People from Jamaica, Vermont]]
[[Category:New York Republicans]]
[[Category:Members of the New York State Assembly]]
[[Category:19th-century American politicians]]