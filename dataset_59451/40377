{{italic title}}
The '''''Einsiedeln Eclogues''''' are two [[Latin]] [[Eclogue|pastoral poems]], written in [[hexameter]]s. They were discovered in a tenth century manuscript from [[Einsiedeln Abbey]] (codex Einsidlensis 266(E) pp 206–7) and first published in 1869, by H. Hagen.<ref>Reeve, M.D.  (1983), Carmina Einsidlensia  in Reynolds, L.D., Texts and Transmission, p.37; Duff, J.W. and Duff , A.M. (1934) Minor Latin Poets (Vol 1), p. 319; Hornblower, S. and Spawforth, A. (eds) (1996), the Oxford Classical Dictionary, 3rd ed. p 513</ref>

The poems are generally considered to be incomplete fragments - although the reason for their incompleteness is disputed. As Hubbard explains,  "some have explained it as a result of mechanical accident in the transmission, while others have thought the poems abbreviated by the poet himself  to avoid giving offense to an ever more suspicious Nero".<ref>Hubbard, T.K. (1998) The Pipes of Pan p 149, citing Korzeniewski (1971) ''Hirtengedichte aus neronischer Zeit'', p 116</ref>

== Authorship, date and place in the pastoral tradition  ==
It is generally agreed that the poems post-date the [[Eclogues]] of [[Virgil]]. Since their publication, the poems have usually been dated to Neronian times (AD 54- 68)<ref>Duff, J.W. and Duff , A.M. (1934) Minor Latin Poets (Vol 1), p 319, Hubbard, T.K. The Pipes of Pan (1998) p 140.</ref> (more specifically, the first fragment is sometimes dated to AD 64 or 65<ref>Hornblower, S. and Spawforth, A. (eds) (1996), the Oxford Classical Dictionary, 3rd ed. p 513</ref>) in which case, they clearly pre-date the [[Eclogues of Nemesianus|Eclogues]] of [[Nemesianus]] and may, or may not, pre-date the [[Eclogues of Calpurnius Siculus|Eclogues]] of [[Titus Calpurnius Siculus|Calpurnius Siculus]].<ref>Hubbard, T.K. The Pipes of Pan (1998), p. 140, fn 3.</ref> However, Stover argues that the poems were written during the (probably later) 4th century AD.<ref>Stover, J. (2015) Olybrius and the Einsiedeln Eclogues, Journal of Roman Studies Vol 105 p 285 - 321.</ref>

The authorship of the poems is unknown. It is even disputed whether the two fragments were written by the same poet.<ref>Hornblower, S. and Spawforth, A. (eds) (1996), the Oxford Classical Dictionary, 3rd ed. p 513</ref> Some scholars have proposed Calpurnius Siculus, [[Lucan]] or Calpurnius Piso as possible authors,<ref>Karakasis, E (2011) Song Exchange in Roman Pastoral p 44, fn 229</ref> however such attributions have not gained wide acceptance. More recently, Stover has expressed the view that author is likely to be Ancius Hermogenianus Olybrius (identifying the poems with the "''Bucolicon Olibrii''" referenced  in a 15th-century copy of a 9th-century catalogue from [[Murbach Abbey|Murbach]]).<ref>Stover, J. (2015) Olybrius and the Einsiedeln Eclogues, Journal of Roman Studies Vol 105 p 285 - 321.</ref>

== Synopses ==

=== Fragment I ===
The poem features three characters: Thamyras, Ladas and Midas.

Thamyras asks Midas to judge a song contest between him and Ladas: Midas agrees.

Ladas and Thamyras discuss whether the prize for the song contest should be a goat or a pipe, given by Faunus. Ladas says that he will win the prize, as he is minded to sing the praises of Caesar.

Ladas sings first. He invokes and sings about [[Apollo]], alluding to the [[Pythia|Oracle of Delphi]], [[cosmology]], the slaughter of the monster [[Python (mythology)|Python]] and Apollo’s musical skills.

Next Thamyras sings. He invokes the [[Muse]]s, and sings that the riches of [[Mount Helicon|Helicon]] and an Apollo are here. He also invokes [[Troy]], and sings that its [[Trojan War|fall]] was all worth it. The meaning is unclear, but it is usually interpreted to mean that a new poem has been written about Troy (possibly by Caesar himself). Thamyras sings of how [[Homer]] crowns Caesar with his own crown and [[Mantua]] tears up its writings.

=== Fragment II ===
The poem features two characters: Glyceranus and Mystes.

Glyceranus asks Mystes why he is being quiet. Mystes explains that it is because he is worried. Mystes explains that the source of his worries is over-abundance (''satias''). Glyceranus invites Mystes to tell him more, under the shade of an elm tree.

The remainder of the fragment consists of a monologue. Mystes describes a [[Golden Age|golden age]] - featuring a prosperous village, with worship, music, dancing and plentiful agricultural produce, without the threat of war and political crisis. He proceeds to tell of how crops grow from uncultivated land, the seas are not bothered by ships, tigers eat their young and lions submit to the yoke. He invokes the goddess [[Lucina (goddess)|Lucina]].

== Commentary ==

=== General ===
Watson refers to the "''incompetence and obscurity of the writing''" of the poems.<ref>Hornblower, S. and Spawforth, A. (eds) (1996), the Oxford Classical Dictionary, 3rd ed. p 513</ref> However, Hubbard writes that "''whilst no one would contend that the Einsiedeln poet was a great master of Latin verse, the poems do exhibit a wide range of learning, as well as a certain imaginative energy and an independence that merit serious consideration in any account of the pastoral tradition''"<ref>Hubbard, T.K. (1998) The Pipes of Pan p 141</ref>

Hubbard notes that "''both poems ratchet up the terms of the encomium to a virtual breaking point at which credibility ceases''"<ref>Hubbard, T.K. (1998) The Pipes of Pan p 149</ref> and that, in each Fragment, such hyperbole is reached through references/allusions to Virgil and his poetry such that "''By problematizing Vergil as hyperbolic and not fully believable, the texts problematize the praise of Nero and thus ultimately their own authenticity, bracketed within frames of ironic self-distancing''".<ref>Hubbard, T.K. (1998) The Pipes of Pan p 150</ref>

=== Fragment I ===
Several scholars consider that Thamyras' song refers to a poem about [[Troy]]  that was written by the emperor [[Nero]] (which he supposedly recited, whilst Rome itself burned) and that the fragment is therefore purporting to praise Nero's poetry over and above that of [[Homer]] and [[Virgil]] (who was born in [[Mantua]]).<ref>Duff, J.W. and Duff , A.M. (1934) Minor Latin Poets (Vol 1), p 329 fn(d), Hubbard, T.K. The Pipes of Pan (1998) p 141ff.</ref>

Watson notes that "two competing shepherds praise the emperor...in terms so extravagant that critics are undecided whether to regard the poem as botched panegyric, or as ironic and derisive".<ref>Hornblower, S. and Spawforth, A. (eds) (1996), the Oxford Classical Dictionary, 3rd ed. p 513</ref> In this regard, Duff and Duff consider that the reference in Thamyras' song to [[Mantua]] tearing its writings to shreds demonstrates gross sycophancy to [[Nero]],<ref>Duff, J.W. and Duff , A.M. (1934) Minor Latin Poets (Vol 1), p 331 fn(a)</ref> whereas some scholars consider that both Ladas' and Thamyras' songs allude to [[Lucan|Lucan's]] encomium to [[Nero]] (from the prologue of [[Pharsalia|Bellum Civile]]) and that, like Lucan's encomium, the extravagant praise of Nero may be construed ironically.<ref>Hubbard, T.K. (1998) The Pipes of Pan p 142f, citing Korzeniewski (1966) ''Die panegyrische Tendenz in den Carmina Einsidlensia ,'' Hermes 94, p 346-53 and Korzeniewski (1971) ''Hirtengedichte aus neronischer Zeit'', p 110-11 and also citing (with regards to ironic readings of Lucan's encomium of Nero) Ahl (1976) Lucan: An Introduction, p 17-61, Sullivan (1985) Literature and politics in the age of Nero, p 144-52, Johnson (1987) Momentary Monsters: Lucan and his Heroes, p 121-22 and Hinds (1987) Generalising about Ovid, Ramus 16, p 26-29</ref>

Exploring the allusions to Lucan further, Hubbard argues that the real object of the poet's awe is Lucan's literary technique - and that therefore (read alongside the fragment's challenges to the primacy of Homer and Virgil) Lucan is  elevated to the status of the poet's primary model.<ref>Hubbard, T.K. (1998) The Pipes of Pan, p 143</ref>

=== Fragment II ===
With its description of a [[Golden Age]], Fragment II is clearly indebted to [[Virgil|Virgil's]] [[Eclogue 4|Fourth]] (and Fifth) [[Eclogues]]. However, Hubbard notes that "t''he Einsiedeln poet avoids an overly close dependency and at times even goes out of his way to make clear his familiarity with Vergil's own sources''".<ref>Hubbard, T.K. (1998) The Pipes of Pan p 146
</ref> In particular, Hubbard explores Fragment II's allusions to [[Theocritus]]' encomium of Hieron (Theocritus Idyll 16) and to [[Aratus]].<ref>Hubbard, T.K. (1998) The Pipes of Pan p 146ff.</ref>

Hubbard notes that the later lines of the Fragment recall Virgilian sources more closely, but in fantastical, hyperbolic terms (e.g. the description of lions submitting to the yoke - possibly an allusion to Daphnis' yoking of tigers in Virgil's Fifth Eclogue): concluding that "''this rhetorical excess must in some sense be what Mystes meant in worrying about satias''".<ref>Hubbard, T.K. (1998) The Pipes of Pan p 149</ref>

== Editions and Translations ==
* Duff, J.W. and Duff, A.M. (1934) Minor Latin Poets (Vol 1) - Latin text with English translation (excerpted version available [http://penelope.uchicago.edu/Thayer/E/Roman/Texts/Einsiedeln_Eclogues/home.html online]).
* Amat, J. (1997) ''Consolation à Livie, Élégies à Mécène, Bucoliques d'Einsiedeln'' - Latin text with French translation and commentary.

==References==
{{reflist}}

[[Category:Latin poems]]
[[Category:10th-century manuscripts]]
[[Category:1st-century poems]]
[[Category:1st century Latin texts]]