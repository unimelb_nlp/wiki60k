{|{{Infobox Aircraft Begin
 |name= BR1 
 |image = Bentley AR1.jpg
 |caption = Preserved Bentley BR1
}}
{{Infobox Aircraft Engine
 |type= Rotary engine
 |national origin = 
 |manufacturer= [[Humber Limited]] 
 |first run= 
 |major applications= [[Sopwith Camel]] 
 |number built = 
 |program cost = 
 |unit cost = 
 |developed from =[[Clerget 9B]] 
 |developed into = 
 |variants with their own articles = 
}}
|}

The '''Bentley BR.1''' was a [[United Kingdom|British]] [[rotary engine|rotary]] [[aircraft engine]] of the [[World War I|First World War]].  Designed by the [[automobile|motor car]] engine designer [[W. O. Bentley]], the BR.1 was built in large numbers, being one of the main powerplants of the [[Sopwith Camel]].

==Design and development==
The {{convert|130|hp|kW}} [[Clerget 9B]] was an important engine for the British [[Royal Naval Air Service]] and [[Royal Flying Corps]], being license-produced in Britain and powering a number of important British aircraft, including the [[Sopwith Camel]]. However, at £907 a copy it was expensive, and prone to overheating, so the [[Admiralty]] asked Lieutenant [[W. O. Bentley]], an established pre-war engine designer,<ref name="barstow bentley">{{cite book |last= Barstow|first= Donald|title= W.O. Bentley - Engineer|year= 1978|publisher= Haynes publications|location= Sparkville, Somerset, UK|isbn= 0-85429-215-2 }}</ref> to produce a modified version to solve these problems.<ref name="gunston piston">{{cite book |last= Gunston|first=Bill |authorlink=Bill_Gunston  |title= The Development of Piston Engines|year= 1999|publisher= Patrick Stephens |location= Sparkville, Somerset, UK|edition= Second |isbn=1-85260-599-5 }}</ref>

Bentley came up with his idea of an engine - fitted with aluminium cylinders with cast iron liners, and aluminium pistons.  Dual ignition was introduced to improve reliability, and the stroke increased to {{convert|6.7|in|cm}} which allowed power to be increased to {{convert|150|hp|kW}}.<ref name="gunston engine">{{cite book |last= Gunston|first=Bill |authorlink=Bill_Gunston  |title=World Encyclopedia of Aero Engines |year=1986 |publisher= Guild Publishing|location=London |isbn= }}</ref>  The cost of the engine was also reduced, falling to £605 per engine.<ref name="gunston piston"/>

The resulting engine, initially known as the '''A.R.1''' for "Admiralty Rotary", but later called the '''BR.1''' ("Bentley Rotary") was manufactured in quantity, although initially against Admiralty orders. It was standardised for the Camel in RNAS squadrons, but unfortunately there were never enough to entirely replace the inferior and more expensive Clerget engine in British service, and most RFC Camel squadrons continued to use Clerget engines; in fact licensed production of the Clerget continued.<ref name="gunston engine"/>

The BR.1 was developed as the [[Bentley BR2|BR.2]], a heavier, more powerful engine, which powered, among other types or aircraft, the Camel's eventual replacement, the [[Sopwith Snipe]].

==Applications==
* [[Sopwith Camel]]
* [[Avro 536]]
* [[Westland N.1B]]
* [[Port Victoria P.V.9]]

==Specifications==
{{pistonspecs
|<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- Please include units where appropriate (main comes first, alt in parentheses). If data are missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with )</li> and start a new, fully-formatted line with <li> -->
|ref=''Jane's Fighting Aircraft of World War I'' <ref name="JanesairWW1">{{cite book  |title= Jane's Fighting Aircraft of World War I |year= 2001 |publisher=Random House |location= London|isbn= 1-85170-347-0|page= 274}}</ref>
|type=9-Cylinder air-cooled [[rotary engine]]
|bore=4.72 in, (120 mm)
|stroke=6.69 in (170 mm)
|displacement=1,055.9 cu in (17.3 L)
|length=
|diameter=
|width=
|height=
|weight=397 lb (180 kg)
|valvetrain=
|supercharger=
|turbocharger=
|fuelsystem=
|fueltype=
|oilsystem=
|coolingsystem=Air-cooled
|power=150 bhp (110 kW) at 1,250 rpm 
|specpower=
|compression=
|fuelcon= 11 gallon/hr (50 L/hr)
|specfuelcon=
|oilcon= 12 pints/hr (6.8 L/hr)
|power/weight=0.38 bhp/lb 
}}

==See also==
{{aircontent
<!-- other related articles that have not already linked: -->
|see also=

<!-- designs which were developed into or from this aircraft: -->
|related=
*[[Bentley BR2]]
<!-- For aircraft engine articles.  Engines that are of similar to this design: -->
|similar engines=
* [[Clerget 9B]]
* [[Clerget aircraft engines|Clerget 9Z]]
* [[Gnome Monosoupape 9 Type N]]
* [[Le Rhone 9J]]
<!-- relevant lists that this aircraft appears in: -->
|lists=
*[[List of aircraft engines]]
<!-- See [[WP:Air/PC]] for more explanation of these fields. -->
}}

==References==
{{reflist}}

==External links==
{{commons category|Bentley AR1}}
{{Bentley}}

[[Category:Rotary aircraft piston engines]]
[[Category:Air-cooled aircraft piston engines]]
[[Category:Aircraft piston engines 1910–1919]]