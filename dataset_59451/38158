{{for|those of a similar name|Jonathan Blum (disambiguation)}}
{{Infobox ice hockey player
| image = Jon Blum Predators 04-2011.jpg
| caption =
| image_size = 230px
| position = [[Defenceman|Defense]]
| shoots = Right
| height_ft = 6
| height_in = 2
| weight_lb = 194
| team = [[Admiral Vladivostok]]
| league = [[Kontinental Hockey League|KHL]]
| prospect_team = 
| prospect_league = 
| former_teams = [[Nashville Predators]]<br>[[Minnesota Wild]]
| birth_date = {{birth date and age|1989|1|30}}
| birth_place = [[Long Beach, California]], [[United States|USA]]
| ntl_team = <!--Senior caps only-->
| draft = 23rd overall
| draft_year = 2007
| draft_team = [[Nashville Predators]]
| career_start = [[2008–09 AHL season|2009]]
}}
'''Jonathon Gregory Blum''' (born January 30, 1989) is an [[United States|American]] professional [[ice hockey]] [[defenseman]] currently playing with [[Admiral Vladivostok]] of the [[Kontinental Hockey League]] (KHL).

Selected by the Predators 23rd overall in the [[2007 NHL Entry Draft]], Blum was born in [[Long Beach, California]] and grew up in [[Rancho Santa Margarita, California]]. During his [[junior ice hockey|major junior]] career with the [[Vancouver Giants]] of the [[Western Hockey League]] (WHL), Blum helped the club to a [[Ed Chynoweth Cup|President's Cup]] as WHL champions in [[2005–06 WHL season|2006]] and a [[Memorial Cup]] as [[Canadian Hockey League]] (CHL) champions in [[2007 Memorial Cup|2007]]. He received the [[Bill Hunter Memorial Trophy]] as the WHL's top defenseman and the [[CHL Defenceman of the Year]] Award in [[2008–09 WHL season|2009]]. In his final season with the Giants, he established himself as the franchise's all-time assists leader. Turning professional in 2009, he spent parts of three seasons with the Predators' minor league affiliate, the [[Milwaukee Admirals]] of the [[American Hockey League]] (AHL), before joining the Predators. Internationally, Blum has represented [[USA Hockey|Team USA]] on three occasions. He won silver at the [[2006 Ivan Hlinka Memorial Tournament]] and has competed in the [[2008 World Junior Ice Hockey Championships|2008]] and [[2009 World Junior Ice Hockey Championships|2009 World Junior Championships]], finishing without a medal both times.

==Early life==
Blum was born in [[Long Beach, California|Long Beach]], [[California]], <!--there are conflicting reports about birthplace: NHL.com says Long Beach, Vancouver Giants and WHL say Rancho Santa Margarita--> to parents John and Dana. An avid [[surfing|surfer]] growing up in [[Rancho Santa Margarita, California|Rancho Santa Margarita]], approximately 40 miles southeast of Long Beach, Blum was initially playing [[roller hockey]] recreationally at the age of four before discovering [[ice hockey]] a couple of years later.<ref name=gamut>{{cite web|title=Gamut of emotions|url=http://www.ocregister.com/ocregister/sports/columns/article_1700915.php|accessdate=2009-02-08|date=2007-05-20|publisher=''OC Register''|archivedate=2011-03-07|archiveurl=http://www.webcitation.org/5x1YyQD41}}</ref> He played [[minor hockey#United States|minor hockey]] with the California Wave, a team led by [[Jeff Turcotte]], younger brother of former [[National Hockey League|NHL]] player [[Alfie Turcotte]].<ref name=early/> Due to the lack of elite minor hockey competition in California, the team travelled to [[Canada]] and the [[Northeast United States]] to play in top-level tournaments. Blum and the Wave recorded second-place finishes in national Midget AAA and Bantam AAA championships culminating in an international Bantam tournament championship in [[Kamloops]], [[British Columbia]], in 2004.<ref name=gamut/> Discovered by scouts at one such tournament, Blum was drafted by the [[Vancouver Giants]] of the [[Western Hockey League|WHL]] in the seventh round of the junior draft.<ref name=early>{{cite web|title=Blum's hockey dream helps heal past personal wounds|url=http://sports.espn.go.com/nhl/draft2007/columns/story?columnist=burnside_scott&id=2913922|accessdate=2009-02-08|date=2007-06-23|publisher=ESPN|archivedate=2011-03-07|archiveurl=http://www.webcitation.org/5x1Z7SlCL}}</ref>

In the midst of Blum's pending [[Junior hockey#Major junior|major junior]] career, his family was beset with tragedy. A few months prior to his first training camp in Vancouver and a freshman at [[Trabuco Hills High School]],<ref name=gamut/> his house caught fire from a gas leak on April 2, 2004, killing his twin sister, Ashley.<ref name=early/> Blum has two remaining half-siblings, an older brother and sister. The following year, during his rookie season in the WHL, his mother was diagnosed with a form of juvenile cancer. She underwent [[heart surgery]] and [[chemotherapy]] to overcome the cancer by May 2006.<ref name=early/>

==Playing career==

===Vancouver Giants (2005–09)===
Drafted 134th overall in the [[2004 WHL Bantam Draft]],<ref>{{cite web|title=2004 WHL Bantam Draft List|url=http://www.whl.ca/draft/index.php?round=7&draft_id=1|accessdate=2009-03-15|publisher=[[Western Hockey League]]|archiveurl = https://web.archive.org/web/20060516013724/http://www.whl.ca/draft/index.php?round=7&draft_id=1 |archivedate = May 16, 2006}}</ref> Blum played his first season with the [[Vancouver Giants]] of the [[Western Hockey League|WHL]] in [[2005–06 WHL season|2005–06]], recording 24 points as a rookie. He added 8 points in the post-season, helping the Giants to a [[Ed Chynoweth Cup|President's Cup]] as WHL champions<ref name=pres>{{cite web|title=Ed Chynoweth Cup|url=http://whl.ca/page/the-ed-chynoweth-cup|accessdate=2011-03-07|publisher=[[Western Hockey League]]}}</ref> and a berth in the [[2006 Memorial Cup]] in [[Moncton]], where they finished third.<ref>{{cite web|title=The 2006 Memorial Cup History|url=http://mastercardmemorialcup.com/cuphistory.php?y=2006|accessdate=2010-01-20|publisher=[[Canadian Hockey League]]|archiveurl=http://web.archive.org/web/20100116025734/http://mastercardmemorialcup.com/cuphistory.php?y=2006 <!--Added by H3llBot-->|archivedate=2010-01-16}}</ref> The following season, in [[2006–07 WHL season|2006–07]], Blum improved to 51 points as the Giants prepared to defend their WHL title as [[2007 Memorial Cup]] hosts.<ref name=mem07>{{Cite web|title=The 2007 Memorial Cup History|url=http://mastercardmemorialcup.com/cuphistory.php?y=2007|accessdate=2010-01-20|publisher=[[Canadian Hockey League]]|archiveurl=http://web.archive.org/web/20100115081540/http://mastercardmemorialcup.com/cuphistory.php?y=2007 <!--Added by H3llBot-->|archivedate=2010-01-15}}</ref> With a league-high +37 [[Plus-minus (ice hockey)|plus-minus]], he earned the [[WHL Plus-Minus Award]].<ref name=plusminus>{{cite web|title=2006-07 Season - Plus/Minus Leaders|url=http://whl.ca/stats/show/type/records/ls_season/227/subtype/10|accessdate=2011-03-07|publisher=[[Western Hockey League]]}}</ref> Blum also participated in the [[CHL Top Prospects Game]] along with teammates [[Tyson Sexsmith]], [[Michal Řepík|Michal Repik]] and [[Spencer Machacek]].<ref>{{cite web|title=Four Giants headed to Top Prospects|url=http://www.oursportscentral.com/services/releases/?id=3411840|accessdate=2009-02-09|date=2007-01-04|publisher=OurSports Central}}</ref> The Giants met the [[Medicine Hat Tigers]] in the WHL finals, where they were defeated in seven games. Then, facing the Tigers in the Memorial Cup final Blum and the Giants captured the franchise's first [[Canadian Hockey League]] (CHL) title by a 3–1 score.<ref name=mem07/> That off-season, going into the [[2007 NHL Entry Draft]], Blum was ranked 17th among North American skaters by [[NHL Central Scouting Bureau|NHL Central Scouting]].<ref name=gamut/> He would be selected 23rd overall by the [[Nashville Predators]], becoming the first California-born-and-raised player to be drafted in the first round.<ref>{{cite web|last=Wood|first=Dan|title=Blum puts California on hockey map|url=http://www.ocregister.com/ocregister/sports/homepage/article_1742119.php|year=2007|publisher=OC Register|accessdate=2008-03-28}}</ref>

[[Image:Jon blum 2.jpg|thumb|left|Blum with the [[Vancouver Giants]] in [[2007–08 WHL season|2007–08]]]]

After being sent back to junior from the Predators' training camp in September 2007,<ref>{{cite web|title=Reinforcements headed back to Giants from NHL|url=http://www.canada.com/theprovince/news/sports/story.html?id=8dfea63b-8c0b-420e-abb3-9d617e02a156|accessdate=2009-02-09|date=2007-09-19|publisher=''[[The Province]]''}}</ref> Blum was signed to an entry-level contract with Nashville on December 17, 2007.<ref name=contract>{{Cite web|title=Predators sign 1st-round pick Jonathon Blum|url=http://www.cbc.ca/sports/hockey/story/2007/12/17/jonathon-blum.html?ref=rss|accessdate=2009-02-08|date=2007-12-17|publisher=[[Canadian Broadcasting Corporation|CBC]]}}</ref> Earlier that month, on December 2, he set a Giants' franchise record with four assists in a 6–1 win over [[Portland Winter Hawks]].<ref>{{Cite web|title=WHL:Record night for Giants' Jonathon Blum|url=http://www.tsn.ca/chl/story/?id=224237|accessdate=2009-02-09|date=2007-12-02|publisher=[[The Sports Network|TSN]]}}</ref> One of the Giants' [[alternate captain (hockey)|alternate captain]]s along with forward [[Garet Hunt]],<ref>{{cite web|title=Giants appoint team captains|url=http://www.oursportscentral.com/services/releases/?id=3545801|accessdate=2009-02-09|date=2007-10-04|publisher=OurSports Central}}</ref> he finished the season with career-highs of 18 goals and 63 points, finishing second among league defensemen (behind [[Ty Wishart]]'s 67 points in 72 games).<ref>{{cite web|title=Top Scorers - 2007-08 Regular Season - Defencemen|url=http://whl.ca/stats/show/type/top_scorers/ls_season/229/subtype/1|accessdate=2011-03-07|publisher=[[Western Hockey League]]}}</ref> His offensive production also set a single-season franchise-record among Giants defensemen for goals (surpassed by [[Kevin Connauton]] in [[2009–10 WHL season|2009–10]])<ref name=goalsrec>{{cite web|title=Vancouver Giants defeat Kelowna Rockets 6-3|url=http://www.vancouversun.com/sports/Vancouver+Giants+defeat+Kelowna+Rockets/2402394/story.html|accessdate=2010-01-20|date=2010-01-03|publisher=''[[Vancouver Sun]]''}}</ref> and points (surpassed by [[Brent Regner]] in [[2008–09 WHL season|2008–09]]).<ref name=pointsrec>{{cite web|title=Giants weekly recap & schedule|url=http://www.oursportscentral.com/services/releases/?id=3606295|accessdate=2011-03-07|date=2008-03-17|publisher=OurSports Central}}</ref><ref>{{cite news|title=Connauton: Old Oiler fan turned Canuck prospect|url=http://communities.canada.com/theprovince/blogs/dubhub/archive/2010/04/28/connauton-old-oiler-fan-turned-canuck-prospect.aspx|accessdate=2011-03-07|date=2010-04-28|publisher=''[[The Province]]''}}</ref> At the end of the season, he was named to the WHL West Second All-Star Team, along with teammate [[Tyson Sexsmith]].<ref name=second>{{cite web|title=Vancouver Giant nominated for WHL rookie of the year|url=http://www.canada.com/vancouversun/news/story.html?id=fc7b3ae2-16af-4d64-85f6-004b37d04b47&k=4015|accessdate=2008-12-31|publisher=''[[Vancouver Sun]]''}}</ref>

With the departure of team captain [[Spencer Machacek]] to the professional ranks the following season, Blum was chosen in his place for the 2008–09 season.<ref>{{Cite web|title=Hay's words catch on with captain|url=http://www.canada.com/theprovince/news/sports/story.html?id=f3d17f3c-32f3-41ce-a8dc-9c902769947f|accessdate=2008-09-30|publisher=[[The Province]]}}</ref> He began the season with 14 points in his first 10 games and was named WHL Player of the Month for September/October.<ref name=pom>{{Cite web|title=Giants' Blum named WHL player of the month|url=http://www.canada.com/vancouversun/story.html?id=7eb489b3-8fdc-443f-b49b-2c0615d56c88|accessdate=2008-11-03|date=2008-11-03|publisher=''[[Vancouver Sun]]''}}</ref> On November 19, 2008, he recorded his first career WHL [[hat trick]] in a 4–1 win against the [[Chilliwack Bruins]].<ref>{{cite web|title=Blum paces Giants past Chilliwack|url=http://www.canada.com/vancouversun/news/sports/story.html?id=10c6faf2-39cb-4596-bae0-4991b3b9a651|accessdate=2008-11-20|date=2008-11-20|publisher=''[[Vancouver Sun]]''}}</ref> Later that season, on February 7, 2009, during a 4–2 victory over the [[Portland Winter Hawks]], Blum became the Giants' all-time assists leader with his 148th assist to pass [[Adam Courchaine (ice hockey b. 1984)|Adam Courchaine]]'s 147-assists mark.<ref name=resume>{{Cite web|title=Blum adds to glowing resumé|url=http://www2.canada.com/theprovince/news/sports/story.html?id=51f2c288-a73b-4120-a943-545fba320dcb|accessdate=2009-02-10|date=2009-02-10|publisher=''[[The Province]]''}}</ref> Nearly a week later, however, he was sidelined with a shoulder injury after receiving a couple of hits from [[Kelowna Rockets]] forward [[Jamie Benn]] in a 3–2 overtime loss.<ref>{{Cite web|title=Win-win with Bruins, Blum|url=http://www2.canada.com/theprovince/news/sports/story.html?id=51af215e-aa21-4106-a713-0a109ba5a260|accessdate=2009-02-16|date=2009-02-15|publisher=''[[The Province]]''}}</ref>

Blum returned to complete the season with 66 points in just 51 games to finish third in league scoring among defensemen behind teammate Brent Regner and [[Paul Postma]] of the [[Calgary Hitmen]]. Despite missing 21 games, Blum was named to the WHL West First All-Star Team, along with teammates [[Casey Pierro-Zabotel]] and [[Evander Kane]],<ref name=whl09>{{Cite web|title=WHL Announces 2008–09 Western Conference All-Star Teams and Award Finalists|url=http://media.whl.ca/whl-announces-2008-09-conference-all-star-teams-and-award-finalists-p127905|accessdate=2011-03-07|date=2009-03-18|publisher=[[Western Hockey League]]}}</ref> and won the [[Bill Hunter Memorial Trophy]] as the league's top defenseman.<ref name=hunter>{{Cite web|title=WHL Announces 2008–09 Award Winners|url=http://media.whl.ca/whl-announces-2008-09-awards-winners-p128253|accessdate=2011-03-07|date=2009-04-30|publisher=[[Western Hockey League]]}}</ref> He was later chosen as the [[CHL Defenceman of the Year]] over [[Dmitri Kulikov (ice hockey)|Dmitri Kulikov]] of the [[Quebec Major Junior Hockey League]] (QMJHL) and [[Ryan Ellis]] of the [[Ontario Hockey League]] (OHL), who were both chosen as defensemen of the year in their respective leagues.<ref name=chldefence>{{Cite web|title=Tavares, Hodgson garner end of year awards|url=http://www.tsn.ca/chl/story/?id=279701|accessdate=2009-05-23|publisher=[[The Sports Network|TSN]]}}</ref> In addition to his league-wide honours, he was named co-team MVP with Casey Pierro-Zabotel, as well as the Giants' Top defenseman and Most Sportsmanlike Player.<ref name=giantsawards>{{cite web|title=Giants Pound Cougars 6–0|url=http://www.oursportscentral.com/services/releases/?id=3789861|accessdate=2009-03-15|date=2009-03-15|publisher=OurSports Central}}</ref> After eliminating the [[Prince George Cougars]] in the first round, Blum scored 2 goals and 1 assist in the first 2 games of the second round against the Spokane Chiefs, earning WHL Player of the Week honors on April 6.<ref name=pow>{{Cite web|title=Vancouver Giants Jon Blum Named Boston Pizza WHL Player of the Week|url=http://news.bostonherald.com/sports/hockey/bruins/view/2009_04_05_Tim_Thomas_deal_puts_cap_pressure_on_Chiarelli/srvc=home&position=recent|accessdate=2009-04-06|date=2009-04-06|publisher=[[Western Hockey League]]}}</ref> The Giants' post-season ended in the semi-finals against the Kelowna Rockets. Blum led the team in playoff scoring with 18 points in 17 games.

===Milwaukee Admirals===
Upon the Giants' elimination, Blum was assigned by the Predators to their [[American Hockey League]] (AHL) affiliate, the [[Milwaukee Admirals]], for the remainder of their [[2009 Calder Cup Playoffs|2009 playoff]] run.<ref name=tsn>{{cite web|title=Jonathon Blum|url=http://tsn.ca/nhl/teams/players/bio/?id=6215|accessdate=2011-03-07|publisher=[[The Sports Network]]}}</ref> He joined the Admirals, making his professional debut, in the second round against the [[Houston Aeros (1994–2013)|Houston Aeros]]. Blum appeared in 5 games, recording no points, as the Admirals were eliminated in seven games.

He remained with Milwaukee in the AHL the [[2009–10 AHL season|following season]] and scored his first professional goal on October 14, 2009, against [[Hannu Toivonen]] in a 5–2 win against the [[Peoria Rivermen (AHL)|Peoria Rivermen]]. Blum finished the campaign with 11 goals and 30 assists for 41 points, third among all rookie defensemen in the league.<ref>{{cite web|title=2009-10 Regular Season - Defencemen|url=http://theahl.com/stats/statdisplay.php?type=top_scorers&subType=1&season_id=30&leagueId=4&lastActive=&singleSeason=&confId=0|accessdate=2010-04-20|publisher=[[American Hockey League]]}}</ref> He then added eight points in seven playoff games as the Admirals were eliminated by the [[Chicago Wolves]] in the opening round.

Blum began the [[2010–11 AHL season|2010–11 season]] with Milwaukee for a second consecutive year. After recording 7 goals and 34 points in 54 games, he was called up by the Predators on February 22, 2011.<ref name=tsn/>

===Nashville Predators===
Blum appeared in his first NHL game on the night of his first call-up, registering a -1 plus-minus rating in 16 minutes of ice time; the Predators lost 4–0 to the [[Columbus Blue Jackets]].<ref>{{cite web|title=Predators vs. Blue Jackets 02/22/2011|url=http://predators.nhl.com/club/boxscore.htm?id=2010020898|accessdate=2011-04-15|date=2011-02-22|publisher=[[Nashville Predators]]}}</ref> In a rematch with Columbus five days later, he scored his first NHL goal against [[Mathieu Garon]] in a 3–2 win.<ref>{{cite news|title=Predators score 3 goals in 3rd period to slip past Jackets|url=http://www.tsn.ca/nhl/story/?id=355757|accessdate=2011-04-15|date=2011-02-27|publisher=[[The Sports Network]]|agency=Associated Press}}</ref> Remaining with the Predators for the remainder of the campaign, Blum recorded 3 goals and 8 points over 23 NHL games while averaging 17 minutes and 45 seconds of ice time per contest.<ref>{{cite web|title=2010-2011 Regular Season Nashville Predators Time on Ice Per Game|url=http://www.nhl.com/ice/playerstats.htm?fetchKey=20112NSHDADAll&sort=avgTOIPerGame&viewName=summary|accessdate=2011-04-15|publisher=[[National Hockey League]]}}</ref> Making his [[Stanley Cup playoffs]] debut in Game 1 of the opening round against the [[Anaheim Ducks]], Blum helped the Predators to a 4–1 win with one assist.<ref>{{cite web|title=Predators vs. Ducks 04/13/2011|url=http://predators.nhl.com/club/boxscore.htm?id=2010030181|accessdate=2011-04-15|date=2011-04-13|publisher=[[Nashville Predators]]}}</ref> The following game, Ducks forward [[Bobby Ryan]] stomped on Blum's foot with his skate while the two players were tied up behind the Predators' net. Blum was not injured on the play, while Ryan was given a two-game suspension for the play.<ref>{{cite news|title=Ducks' Ryan suspended two games for stomping incident|url=http://tsn.ca/nhl/story/?id=362452|accessdate=2011-04-17|date=2011-04-16|publisher=[[The Sports Network]]|agency=Associated Press}}</ref> The Predators went on to eliminate the Ducks in six games, advancing to the second round, where they were defeated by the [[Vancouver Canucks]].<ref>{{cite web|title=2010-11 NHL Playoff Results|url=http://www.hockeydb.com/ihdb/stats/playoffdisplay.php?league=nhl1927&season=2011&leaguenm=NHL|accessdate=2011-07-22|publisher=Hockeydb.com}}</ref> Blum finished the [[2011 Stanley Cup playoffs|2011 playoffs]] with two assists over 12 games. Following the elimination, Blum was reassigned to the Admirals for their [[2011 Calder Cup Playoffs|2011 playoff season]].<ref name=tsn/> He dressed for one game, an elimination match against the [[Houston Aeros (1994–2013)|Houston Aeros]] in the second round, which Milwaukee lost.

Blum made the Predators' roster out of training camp for the first time in [[2011–12 NHL season|2011–12]]. After being made a healthy scratch in two consecutive games, however, he was returned to Milwaukee on December 12, 2011.<ref>{{cite news|title=Team reaction on Blum, Geoffrion's status|url=http://blogs.tennessean.com/predators/2011/12/13/team-reaction-on-blum-geoffrions-status/|accessdate=2011-12-17|date=2011-12-13|work=[[The Tennessean]]|author=Cooper, Josh}}</ref>

===Minnesota Wild===
The Nashville Predators failed to tender Blum following the [[2012–13 NHL season|2012-2013 season]]. On July 12, 2013, Blum signed a two-way contract as a free agent with the [[Minnesota Wild]].<ref>{{cite web |url=http://www.startribune.com/sports/blogs/215237111.html |title=Wild, Jonathon Blum agree to terms; Full prospects scrimmage Saturday |author=Michael Russo |date=July 12, 2013 |publisher=Limelight Networks |work=www.startribune.com |accessdate=July 12, 2013}}</ref>

=== Admiral Vladivostok ===
On August 8, 2015, as a restricted free agent with the Wild, Blum confirmed he had signed a one-year contract with Russian club, Admiral Vladivostok of the KHL.<ref>{{Cite web|url = http://www.sportsnet.ca/hockey/nhl/wild-defenceman-jonathon-blum-heading-to-khl/|title = Wild defenceman Jonathon Blum heading to KHL|date = August 8, 2015|accessdate = August 8, 2015|website = Sportsnet|publisher = |last = Johnston|first = Mike}}</ref>

==International play==
{{MedalTableTop|name = no}}
{{MedalCountry|{{flagicon|USA}} [[USA Hockey|United States]]}}
{{MedalSport|[[Ice hockey]]}}
{{MedalCompetition|[[Ivan Hlinka Memorial Tournament]]}}
{{MedalSilver|[[2006 Ivan Hlinka Memorial Tournament|2006 Czech Republic/Slovakia]]|}}
{{MedalBottom}}

Blum debuted internationally for [[USA Hockey|Team USA]] at the [[2006 Ivan Hlinka Memorial Tournament]] in the [[Czech Republic]] and [[Slovakia]].<ref>{{Cite web|title=Jonathan Blum|url=http://theahl.com/stats/player.php?id=3305|accessdate=2009-12-15|publisher=[[American Hockey League]]}}</ref> He captured a silver medal, as the United States lost to [[Hockey Canada|Canada]] in the final. Two years later, he competed in the [[2008 World Junior Ice Hockey Championships|2008 World Junior Championships]] in the Czech Republic, finishing fourth with Team USA. Set to appear in the [[2009 World Junior Ice Hockey Championships|2009 World Junior Championships]] in [[Ottawa]], Blum was named team captain of the United States.<ref>{{cite web|title=Captain Blum Q&A|url=http://www.usahockey.com/world_junior_championships_2009/default.aspx?NAV=AF_03&id=249578&DetailedNews=yes|accessdate=2008-12-22|date=2008-12-22|publisher=[[USA Hockey]]}}</ref> He contributed two goals and two assists in six games, but once again failed to win a medal, finishing in fifth place.

==Career statistics==

===Regular season and playoffs===
{| border="0" cellpadding="1" cellspacing="0" style="text-align:center; width:60em"
|- bgcolor="#e0e0e0"
! colspan="3" bgcolor="#ffffff" | &nbsp;
! rowspan="99" bgcolor="#ffffff" | &nbsp;
! colspan="5" | [[Regular season|Regular&nbsp;season]]
! rowspan="99" bgcolor="#ffffff" | &nbsp;
! colspan="5" | [[Playoffs]]
|- bgcolor="#e0e0e0"
! [[Season (sports)|Season]]
! Team
! League
! GP
! [[Goal (ice hockey)|G]]
! [[Assist (ice hockey)|A]]
! [[Point (ice hockey)|Pts]]
! [[Penalty (ice hockey)|PIM]]
! GP
! G
! A
! Pts
! PIM
|-
| 2004–05
| California Wave
| [[Minor hockey|Minor]]
| 55
| 15
| 50
| 65
| 65
| —
| —
| —
| —
| —
|- bgcolor="f0f0f0"
| [[2005–06 WHL season|2005–06]]
| [[Vancouver Giants]]
| [[Western Hockey League|WHL]]
| 61
| 7
| 17
| 24
| 25
| 18
| 1
| 7
| 8
| 16
|-
| [[2006–07 WHL season|2006–07]]
| Vancouver Giants
| WHL
| 72
| 8
| 43
| 51
| 48
| 22
| 3
| 6
| 9
| 8
|- bgcolor="f0f0f0"
| [[2007–08 WHL season|2007–08]]
| Vancouver Giants
| WHL
| 64
| 18
| 45
| 63
| 44
| 10
| 3
| 4
| 7
| 10
|-
| [[2008–09 WHL season|2008–09]]
| Vancouver Giants
| WHL
| 51
| 16
| 50
| 66
| 30
| 17
| 7
| 11
| 18
| 6
|- bgcolor="f0f0f0"
| [[2008–09 AHL season|2008–09]]
| [[Milwaukee Admirals]]
| [[American Hockey League|AHL]]
| —
| —
| —
| —
| —
| 5
| 0
| 0
| 0
| 0
|-
| [[2009–10 AHL season|2009–10]]
| Milwaukee Admirals
| AHL
| 80
| 11
| 30
| 41
| 32
| 7
| 1
| 7
| 8
| 0
|- bgcolor="f0f0f0"
| [[2010–11 AHL season|2010–11]]
| Milwaukee Admirals
| AHL
| 54 || 7 || 27 || 34 || 20
| 1 || 0 || 0 || 0 || 0
|-
| [[2010–11 NHL season|2010–11]]
| [[Nashville Predators]]
| [[National Hockey League|NHL]]
| 23 || 3 || 5 || 8 || 8
| 12|| 0|| 2||2 ||0
|- bgcolor="f0f0f0"
| [[2011-12 NHL season|2011–12]]
| Nashville Predators
| NHL
| 33
| 3
| 4
| 7
| 6
| —
| —
| —
| —
| —
|-
| [[2011-12 AHL season|2011–12]]
| Milwaukee Admirals
| AHL
| 48
| 4
| 22
| 26
| 36
| 3
| 0
| 1
| 1
| 4
|- bgcolor="f0f0f0"
| [[2012-13 AHL season|2012–13]]
| Milwaukee Admirals
| AHL
| 34
| 1
| 11
| 12
| 16
| —
| —
| —
| —
| —
|-
| [[2012-13 NHL season|2012–13]]
| Nashville Predators
| NHL
| 35
| 1
| 6
| 7
| 6
| —
| —
| —
| —
| —
|- bgcolor="f0f0f0"
| [[2013-14 AHL season|2013–14]]
| [[Iowa Wild]]
| AHL
| 54
| 7
| 22
| 29
| 23
| —
| —
| —
| —
| —
|- 
| [[2013-14 NHL season|2013–14]]
| [[Minnesota Wild]]
| NHL
| 15
| 0
| 1
| 1
| 0
| —
| —
| —
| —
| —
|- bgcolor="f0f0f0"
| [[2014-15 AHL season|2014–15]]
| Iowa Wild
| AHL
| 66
| 12
| 25
| 37
| 18
| —
| —
| —
| —
| —
|- 
| [[2014-15 NHL season|2014–15]]
| Minnesota Wild
| NHL
| 4
| 0
| 1
| 1
| 2
| —
| —
| —
| —
| —
|- bgcolor="f0f0f0"
| [[2015-16 KHL season|2015–16]]
| [[Admiral Vladivostok]]
| [[Kontinental Hockey League|KHL]]
| 55
| 8
| 22
| 30
| 45
| 5
| 0
| 1
| 1
| 4
|- 
| [[2016-17 KHL season|2016–17]]
| Admiral Vladivostok
| KHL
| 36
| 2
| 19
| 21
| 20
| 4
| 0
| 1
| 1
| 6
|- ALIGN="center" bgcolor="#e0e0e0"
! colspan="3" | NHL totals
! 110 !! 7 !! 17 !! 24 !! 22
! 12!! 0!! 2!! 2!!0
|}

===International===
{| border="0" cellpadding="1" cellspacing="0" style="text-align:center; width:40em"
|- ALIGN="center" bgcolor="#e0e0e0"
! Year
! Team
! Event
! Result
! rowspan="99" bgcolor="#ffffff" | &nbsp;
! GP
! G
! A
! Pts
! PIM
|- ALIGN="center"
| ALIGN="center" | [[2008 World Junior Ice Hockey Championships|2008]]
| ALIGN="center" | [[United States men's national junior ice hockey team|United States]]
| ALIGN="center" | [[World Junior Ice Hockey Championships|WJC]]
| 4th
| ALIGN="center" | 6
| ALIGN="center" | 0
| ALIGN="center" | 1
| ALIGN="center" | 1
| ALIGN="center" | 0
|- ALIGN="center" bgcolor="f0f0f0"
| ALIGN="center" | [[2009 World Junior Ice Hockey Championships|2009]]
| ALIGN="center" | United States
| ALIGN="center" | WJC
| 5th
| ALIGN="center" | 6
| ALIGN="center" | 2
| ALIGN="center" | 2
| ALIGN="center" | 4
| ALIGN="center" | 0
|- ALIGN="center" bgcolor="#e0e0e0"
! colspan="4" | Junior totals
! ALIGN="center" | 12
! ALIGN="center" | 2
! ALIGN="center" | 3
! ALIGN="center" | 5
! ALIGN="center" | 0
|}

*All stats taken from {{nhlprofile|8474164}}

==Awards==

===CHL===
{| class="wikitable"
!Award!!Year(s)
|-
| [[Memorial Cup]] ([[Vancouver Giants]]) || [[2007 Memorial Cup|2007]]<ref name=mem07/>
|-
| [[CHL Defenceman of the Year|Defenceman of the Year]] || 2009<ref name=chldefence/>
|}

===WHL===
{| class="wikitable"
!Award!!Year(s)
|-
| [[Ed Chynoweth Cup|President's Cup]] ([[Vancouver Giants]]) || [[2005–06 WHL season|2006]]<ref name=pres/>
|-
| [[Memorial Cup]] (Vancouver Giants) || [[2007 Memorial Cup|2007]]
|-
| [[WHL Plus-Minus Award]] || [[2006–07 WHL season|2007]]<ref name=plusminus/>
|-
| West Second All-Star Team || [[2007–08 WHL season|2008]]<ref name=second/>
|-
| Player of the Month || September/October [[2008–09 WHL season|2008]]<ref name=pom/>
|-
| West First All-Star Team || 2009<ref name=whl09/>
|-
| [[Bill Hunter Memorial Trophy]] <small>(top defenseman)</small> || 2009<ref name=hunter/>
|-
| Player of the Week || April 6, 2009<ref name=pow/>
|}

===Vancouver Giants===
{| class="wikitable"
!Award!!Year(s)
|-
| Team MVP || 2009 <small>(shared with [[Casey Pierro-Zabotel]])</small><ref name=giantsawards/>
|-
| Top Defenseman || 2009<ref name=giantsawards/>
|-
| Most Sportsmanlike Player || 2009<ref name=giantsawards/>
|}

==Records==
*Vancouver Giants' franchise record; all-time assists – 155 <small>(surpassed [[Adam Courchaine (ice hockey b. 1984)|Adam Courchaine]], 147, on February 7, 2009)</small><ref name=resume/>

==Transactions==
*June 22, 2007 – Drafted by the [[Nashville Predators]] 23rd overall in the [[2007 NHL Entry Draft]].
*December 17, 2007 – Signed to an entry-level contract by the Nashville Predators.<ref name=contract/>

==References==
{{Reflist|25em}}

==External links==
*{{Eliteprospects}}
*{{hockeydb|90120}}
*{{nhlprofile|8474164}}
*[http://theahl.com/stats/player.php?id=3190 Jonathon Blum's AHL Profile]

{{Commons category|Jonathon Blum}}

{{s-start}}
{{s-ach}}
{{succession box | before = [[Paul Albers]] | title = Winner of the [[WHL Plus-Minus Award]] | years = [[2006–07 WHL season|2007]] | after = [[Greg Scott (ice hockey)|Greg Scott]]}}
{{succession box | before = [[Karl Alzner]] | title = [[Bill Hunter Memorial Trophy]] | years = [[2008–09 WHL season|2009]] | after = [[Tyson Barrie]]}}
{{succession box | before = [[Karl Alzner]] | title = Winner of the [[CHL Defenceman of the Year]] | years = [[2008–09 WHL season|2009]] | after = [[David Savard]]}}
{{S-sports}}
{{succession box | before = [[Ryan Parent]] | title = [[List of Nashville Predators draft picks|Nashville Predators first round draft pick]] | years = [[2007 NHL Entry Draft|2007]] | after = [[Colin Wilson (ice hockey)|Colin Wilson]]}}
{{succession box | before = [[Spencer Machacek]] | title = [[Vancouver Giants]] captain | years = [[2008–09 WHL season|2008–09]] | after = [[Lance Bouma]]}}
{{s-end}}

{{good article}}

{{DEFAULTSORT:Blum, Jonathon}}
[[Category:1989 births]]
[[Category:Living people]]
[[Category:Admiral Vladivostok players]]
[[Category:American ice hockey defensemen]]
[[Category:Ice hockey people from California]]
[[Category:Iowa Wild players]]
[[Category:Memorial Cup winners]]
[[Category:Milwaukee Admirals players]]
[[Category:Minnesota Wild players]]
[[Category:Nashville Predators draft picks]]
[[Category:Nashville Predators players]]
[[Category:National Hockey League first round draft picks]]
[[Category:Sportspeople from Orange County, California]]
[[Category:Sportspeople from Long Beach, California]]
[[Category:Twin people from the United States]]
[[Category:Vancouver Giants players]]
[[Category:American expatriates in Russia]]
[[Category:People from Rancho Santa Margarita, California]]