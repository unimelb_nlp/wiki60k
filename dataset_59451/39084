{{good article}}
{{Infobox military conflict
|image=
|caption=
|conflict=Byzantine–Bulgarian war of 894–896
|partof=the [[Byzantine–Bulgarian wars]]
|date=894–896
|place=Eastern [[Balkan Peninsula|Balkans]]: [[Thrace]], [[Dobrudzha]]; Ukrainian steppes
|territory=Bulgarian territorial gains in Thrace<br />Magyars forced to migrate westwards and settle in [[Pannonia]]
|result=Bulgarian victory<br />Bulgaria's status as [[most favoured nation]] restored
|combatant1=[[First Bulgarian Empire|Bulgarian Empire]]<br />[[Pechenegs]]
|combatant2=[[Byzantine Empire]]<br />[[Magyars]]
|commander1=[[Simeon I of Bulgaria|Simeon I the Great]]
|commander2=[[Leo VI the Wise]]<br />Prokopios Krenites<br />Leo Katakalon<br />[[Liüntika]]
|strength1=Unknown
|strength2=Unknown
|casualties1=Unknown
|casualties2=Heavy
}}
{{Byzantine-Bulgarian Wars}}

The '''Byzantine–Bulgarian war of 894–896''' ({{lang-bg|Българо–византийска война от 894–896}}), also called the '''Trade war''' ({{lang-bg|Търговската война}}), was fought between the [[First Bulgarian Empire|Bulgarian Empire]] and the [[Byzantine Empire]] as a result of the decision of the Byzantine emperor [[Leo VI the Wise|Leo VI]] to move the Bulgarian market from [[Constantinople]] to [[Thessaloniki]] which would greatly increase the expenses of the Bulgarian merchants.

Following the defeat of the Byzantine army in the initial stages of the war in 894 Leo VI sought aid from the [[Magyars]] who at the time inhabited the steppes to the north-east of Bulgaria. Aided by the [[Byzantine navy]], in 895  the Magyars invaded [[Dobrudzha]] and defeated the Bulgarian troops. [[Simeon I of Bulgaria|Simeon I]] called for truce and deliberately protracted the negotiations with the Byzantines until securing the assistance of the [[Pechenegs]]. Cornered between the Bulgarians and the Pechanegs, the Magyars suffered a [[Battle of Southern Buh|crushing defeat]] at the hands of the Bulgarian army and had to migrate westwards, settling in [[Pannonia]].

With the Magyar threat eliminated, Simeon led his hosts south and routed the Byzantine army in the [[battle of Boulgarophygon]] in the summer of 896, which forced Byzantium to agree to the Bulgarian terms. The war ended with a peace treaty which restored the Bulgarian market in Constantinople and confirmed Bulgarian domination in the Balkans. The Byzantine Empire was obliged to pay Bulgaria an annual tribute in exchange for the return of captured Byzantine soldiers and civilians. Under the treaty, the Byzantines also ceded an area between the [[Black Sea]] and the [[Strandzha]] mountains to Bulgaria. Despite several violations, the treaty formally lasted until Leo VI's death in 912.

== Background ==
[[File:Bulgaria under rule of Boris I.png|left|thumb|200px|alt=A map of the Bulgarian Empire in the late 9th century|A map of Bulgaria in the second half of the 9th century.]]
During the reign of [[Boris I of Bulgaria|Boris I]] (r. 852–889) Bulgaria underwent major changes&nbsp;— the [[Christianization of Bulgaria|Christianization of the country]] and the admission of the disciples of [[Saints Cyril and Methodius]], which marked the beginning of the creation and consolidation of the medieval Bulgarian literature and alphabet. Following intense negotiations with the [[Catholic Church|Papacy in Rome]] and the [[Ecumenical Patriarchate of Constantinople]], Bulgaria converted to [[Eastern Orthodox Church|Eastern Orthodox Christianity]], which caused discontent among part of the nobility who directly associated the new religion with the Byzantine Empire and feared that the country would fall under Byzantine influence.<ref>{{harvnb|Andreev|Lalkov|1996|pp=73, 75}}</ref>

During the [[Council of Preslav]] in 893, assembled after the unsuccessful attempt of Boris I's eldest son [[Vladimir of Bulgaria|Vladimir-Rasate]] (r. 889–893) to restore the traditional Bulgar religion, [[Tengriism]], it was decided that [[Old Church Slavonic|Old Bulgarian]] would replace Greek as the language of the church and the Byzantine clergy would be banished and replaced with Bulgarians.<ref>{{harvnb|Zlatarski|1972|pp=261–262}}</ref><ref>{{harvnb|Zlatarski|1972|pp=272–273}}</ref> The Council sealed Boris I's ambitions to secure the cultural and religious independence from the Byzantine Empire<ref>{{harvnb|Andreev|Lalkov|1996|p=87}}</ref> and calmed down the concerns among the nobility. It was also decided that his third son [[Simeon I of Bulgaria|Simeon]], born after the Christianization and called the "child of peace",<ref>{{harvnb|Andreev|Lalkov|1996|p=91}}</ref> would become the next Prince of Bulgaria.<ref name="andreev92">{{harvnb|Andreev|Lalkov|1996|p=92}}</ref><ref>{{harvnb|Runciman|1930|p=137}}</ref> These events brought an end to the Byzantine hopes to exert influence over the newly Christianized country.<ref name="andreev92" /><ref name="bakalov251">{{harvnb|Bakalov et al|2003|p=251}}</ref>

== Prelude ==
[[File:BulgarianDelegationLeonVI.jpg|right|thumb|300px|alt=A page from a medieval manuscript|A Bulgarian delegation and Leo VI, ''[[Madrid Skylitzes]]''.]]
In 894 [[Stylianos Zaoutzes]], ''[[basileopator]]'' and leading minister of [[Leo VI the Wise]] (r. 886–912), convinced the emperor to move the Bulgarian market from [[Constantinople]] to [[Thessaloniki]].<ref name="mladjov">{{cite web|url=http://sitemaker.umich.edu/mladjov/selections_on_byzantium&|title=Selections on Byzantium. Selections from the Chronicle of Ioannes Skylitzes, translated and adapted from B. Flusin and J.-C. Cheynet (2003)|last=Mladjov|first=Ian|accessdate=8 November 2014}}</ref> That move affected not only private interests but also the international commercial importance of Bulgaria and the principle of Byzantine–Bulgarian trade, regulated with the [[Byzantine–Bulgarian Treaty of 716|Treaty of 716]] and later agreements on the [[most favoured nation]] basis.<ref name="fine137">{{harvnb|Fine|1991|p=137}}</ref><ref>{{harvnb|Runciman|1930|p=144}}</ref><ref>{{harvnb|Zlatarski|1972|p=286}}</ref> The Bulgarian merchants were allowed to live in Constantinople, resided in their own colony and paid favourable taxes.<ref name="fine137" /> The city was a major destination of trade routes from all over [[Europe]] and [[Asia]] and the transfer of the Bulgarian market to Thessaloniki cut short the direct access to goods from the east, which under the new circumstances the Bulgarians would have to buy through middlemen, who were close associates of Stylianos Zaoutzes. In Thessaloniki the Bulgarians were also forced to pay higher tariffs to sell their goods, enriching Zaoutzes' cronies.<ref name="fine137" />

The Byzantine chronicler [[Theophanes Continuatus]] described the reasons for the conflict as follows:
{{cquote|The cause of the war was the following&nbsp;— the ''basileopator'' Stylianos Zaoutzes had an [[eunuch]] slave named Mousikos. He befriended Staurakios and Cosmas, who originated from [[Hellas (theme)|Hellas]], merchants greedy for profit and covetous. In their desire to enrich themselves and through the mediation of Mousikos, they moved the market of the Bulgarians from the capital [Constantinople] to Thessaloniki and taxed the Bulgarians with heavier duties. When the Bulgarians acquainted Simeon with the issue, he informed emperor Leo. Infatuated in his predilection to Zaoutzes, he considered all this a trifle. Simeon was infuriated and raised arms against the Romans.
—''Chronographia'' by Theophanes Continuatus<ref name="bakalov251" /><ref>"Chronographia by Theophanes Continuatus" in GIBI, vol. V, Bulgarian Academy of Sciences, Sofia, [http://www.promacedonia.org/gibi/5/gal/5_121.html pp. 121]–[http://www.promacedonia.org/gibi/5/gal/5_122.html 122]</ref>}}
The ousting of the merchants from Constantinople was a heavy blow for Bulgarian economic interests.<ref>{{harvnb|Obolensky|1971|p=105}}</ref> The merchants complained to Simeon I, who in turn raised the issue to Leo VI, but the appeal was left unanswered.<ref name="andreev92" /> Simeon, who according to the Byzantine chroniclers was seeking a pretext to declare war and to implement his plans to seize the Byzantine throne,<ref name="zlatarski288–289">{{harvnb|Zlatarski|1972|pp=288–289}}</ref> attacked,<ref>{{harvnb|Andreev|Lalkov|1996|pp=92–93}}</ref> provoking what has sometimes been called (inappropriately) the first commercial war in Europe.<ref name="bakalov251" /><ref name="mladjov" /> However, many historians including [[Vasil Zlatarski]] and [[John Van Antwerp Fine, Jr.|John Fine]] consider those claims unlikely, arguing that in the beginning of his reign Simeon needed to consolidate his power and imperial ambitions had not yet been crystallised, therefore his military intervention was a defensive act to protect the Bulgarian commercial interests.<ref name="fine137" /><ref name="zlatarski288–289" />

== Initial campaigns and Magyar intervention ==
{{multiple image
 | direction = vertical
 | width = 300
 | footer =
 | image1 = Bulgarians defeat Byzantines under Krenites and Kourtikios.jpg
 | alt1 = A page from a medieval manuscript
 | caption1 = The Bulgarians defeat the Byzantines under Krenites and Kourtikios in Thrace, ''Madrid Skylitzes''.
 | image2 = HungariansPursueBulgarians.jpg
 | alt2 = A page from a medieval manuscript
 | caption2 = The Magyars pursue Simeon I to [[Silistra|Drastar]], ''Madrid Skylitzes''.
 }}
In the autumn of 894 Simeon I launched an invasion of Byzantine Thrace, taking advantage of Byzantium's engagements with the [[Arabs]] to the east, which had left the Balkan provinces vulnerable. Leo VI hastily assembled an army under the generals Prokopios Krenites and Kourtikios and many [[archon]]s, which included the Imperial Guard that consisted of [[Khazars|Khazar]] mercenaries.<ref name="mladjov" /> In the ensuing battle in the [[Macedonia (theme)|Theme of Macedonia]] (modern [[Eastern Thrace]]), probably around [[Edirne|Adrianople]],<ref>{{harvnb|Zlatarski|1972|pp=289–290}}</ref> the Byzantines were defeated and their commanders perished. Most of the Khazars were captured and Simeon had their noses cut and "sent them in the capital [Constantinople] for shame of the Romans [i.e. the Byzantines]".<ref name="andreev93">{{harvnb|Andreev|Lalkov|1996|p=93}}</ref><ref>{{harvnb|Whittow|1996|pp=286–287}}</ref> The Bulgarians looted the region and retired to the north taking many captives.<ref>{{harvnb|Zlatarski|1972|p=290}}</ref>

This failure urged the Byzantines to seek aid from the [[Magyars]], who at the time inhabited the steppes between the [[Dnieper]] and the [[Danube]]. Leo VI sent his envoy Nicetas Scleros to the Magyar leaders [[Árpád]] and [[Kurszán]] in 894 or 895 "to give presents" and incite them against the Bulgarians.<ref name="fine138">{{harvnb|Fine|1991|p=138}}</ref><ref name="spinei52">{{harvnb|Spinei|2003|p=52}}</ref> At the same time, in the autumn of 894, Leo VI sent one Anastasios in [[Regensburg]] to [[Arnulf of Carinthia]], king of [[East Francia]]. While no records have survived of that mission's purpose, it was most likely a pre-emptive move to discourage a German–Bulgarian alliance which existed between Arnulf and Simeon I's predecessor, Vladimir-Rasate.<ref>{{harvnb|Zlatarski|1972|pp=294–295}}</ref>

In the beginning of 895 the talented general [[Nikephoros Phokas the Elder]] was summoned to Constantinople and sent against the Bulgarians at the head of a large army.<ref>{{harvnb|Zlatarski|1972|p=295}}</ref> While Simeon concentrated his forces along the southern border to confront Phokas, the [[Byzantine navy]] under admiral [[Eustathios Argyros (admiral under Leo VI)|Eustathios Argyros]] sailed to the [[Danube Delta]] to assist the Magyars.<ref>{{harvnb|Runciman|1930|pp=145–146}}</ref> Believing that Simeon I would back off Leo VI sent an envoy, Constantinacios, to propose peace. Simeon I, who had studied in the [[University of Constantinople]] and was familiar with the [[Byzantine diplomacy]], was suspicious of the Byzantine rapprochement, charged Constantinacios with espionage and put him into custody.<ref name="andreev93" /><ref name="runciman146">{{harvnb|Runciman|1930|p=146}}</ref> The Danube was barred with an iron chain to impede the movement of the Byzantine navy and the bulk of the army was dislocated northwards. The Byzantines, however, managed to break the chain and transported the Magyar hordes south of the river.<ref name="andreev93" /> The Magyars, led by Árpád's son [[Liüntika]],<ref name="spinei52" /> ravaged Dobrudzha and dealt a heavy defeat on the Bulgarian army, led personally by Simeon I.<ref name="fine138" /><ref>{{harvnb|Zlatarski|1972|pp=297–299}}</ref> Simeon sought refuge in the strong fortress of [[Silistra|Drastar]] while the Magyars pillaged and looted unopposed, reaching the outskirts of the capital [[Veliki Preslav|Preslav]].<ref name="runciman146" /> Before retreating north, the Magyars sold thousands of captives to the Byzantines.<ref name="runciman146" /><ref name="bg248">{{harvnb|Bozhilov|Gyuzelev|1999|p=248}}</ref>

== Truce negotiations ==
[[File:Seal of Simeon I of Bulgaria.jpg|left|thumb|200px|alt=A medieval seal|A seal of Simeon I.]]
Facing a difficult situation with war on two fronts, Simeon sent a peace proposal through admiral Eustathios to buy time to deal with the Magyar menace, promising to return the Byzantine captives. Leo VI gladly complied, ordered Eustathios and Nikephoros Phokas to retreat and sent the diplomat [[Leo Choirosphaktes]] to Bulgaria to negotiate the terms.<ref name="andreev93" /><ref name="runciman146" /> That was exactly what Simeon I had aimed. Leo Choirosphaktes was detained in one fortress and was repeatedly refused an audience. Instead, Simeon I exchanged letters with him, protracting the negotiations, showing suspicions over the wording of the Byzantine proposals, constantly seeking clarifications and adding new demands.<ref name="fine138" /> The main issue was the exchange of the captives&nbsp;— the Byzantine priority was to free the prisoners captured during the Bulgarian campaign of 894.<ref>{{harvnb|Zlatarski|1972|pp=301–305}}</ref> In one of his letters to Choirosphaktes Simeon I demonstrated his diplomatic skills deriding the emperor:
{{cquote|The eclipse of the sun, and its date, not only to the month, week or day, but to the hour and second, your emperor prophesied to us the year before last in the most marvellous fashion. And he also explained how long the eclipse of the moon will last. And they say he knows many other things about the movements of the heavenly bodies. If this is true, he must also know about the prisoners; and if he knows, he will have told you whether I am going to release them or keep them. So prophesy one thing or the other, and if you know my intentions, you shall get the prisoners as reward for your prophecy and your embassy, by God! Greetings!.
—letter of Simeon I to Leo Choirosphaktes<ref>{{harvnb|Fine|1991|pp=138–139}}</ref>}}

Choirosphaktes replied with an ambiguous answer, which was used by Simeon to claim that Leo could not prophesy the future and to refuse the return of the captives, further prolonging the negotiations.<ref>{{harvnb|Zlatarski|1972|pp=306–307}}</ref>

== Defeat of the Magyars and battle of Boulgarophygon ==
While exchanging correspondence with Leo Choirosphaktes, Simeon sent envoys to forge an alliance with the [[Pechenegs]], the eastern neighbours of the Magyars, and in the beginning of 896 the Bulgarians and Pechenegs attacked the Magyar homeland on two fronts.<ref name="andreev94">{{harvnb|Andreev|Lalkov|1996|p=94}}</ref> In the decisive battle the Bulgarian army dealt a [[Battle of Southern Buh|crushing defeat on the Magyars]] in the steppes along the [[Southern Bug]] river. The struggle was so bloody that it is said that the victorious Bulgarians lost 20,000 riders.<ref name="runciman147">{{harvnb|Runciman|1930|p=147}}</ref> At the same time, the Pechenegs advanced westwards and prevented the Magyars from returning to their homeland.<ref name="fine139">{{harvnb|Fine|1991|p=139}}</ref> The blow on the Magyars was so heavy that they were forced to migrate further west in search of new pastures, eventually settling in the [[Pannonian Basin]], where they established the powerful [[Kingdom of Hungary (medieval)|Kingdom of Hungary]].<ref name="bg248" /><ref name="fine139" />
[[File:Boulgarofygon.jpg|thumb|right|300px|alt=A page from a medieval manuscript|The Bulgarians rout the Byzantine army at Boulgarophygon, ''Madrid Skylitzes''.]]
With the Magyar threat eliminated, Simeon returned to Preslav "proud of the victory"<ref name="andreev94" /> and demanded the return of all Bulgarian captives as a precondition for further peace negotiations. Leo VI, who was in a difficult situation, facing the Arabs in the east and deprived of the services of the capable general Nikephoros Phokas, who was either disgraced as a result of the intrigues of Stylianos Zaoutzes or died in early 896, had to comply.<ref name="runciman147" /><ref>{{harvnb|Zlatarski|1972|p=315}}</ref> Leo Choirosphaktes and a Bulgarian envoy called [[Theodore Sigritsa|Theodore]], a trusted man of Simeon's, were sent to Constantinople to arrange the transfer which was successfully implemented.<ref>{{harvnb|Zlatarski|1972|pp=312–313}}</ref> Interpreting that as a sign of weakness, Simeon claimed that not all Bulgarians had been released and in the summer of 896 invaded Thrace.<ref name="fine139" /> The Byzantines secured an uneasy truce with the Arabs and transferred to Europe "all ''[[Theme (Byzantine district)|themes]]'' and ''[[Tagma (military)|tagmata]]''",<ref name="andreev94" /> i. e. all forces of the empire. The troops were commanded by the [[Domestic of the Schools]] Leo Katakalon, who lacked the ability of Phokas.<ref name="runciman147" /> The two armies clashed in the [[battle of Boulgarophygon]] and the Byzantines were thoroughly routed&nbsp;— most of the soldiers perished, including the second-in-command, the ''[[protovestiarios]]'' Theodosius. Katakalon managed to escape with a few survivors.<ref name="mladjov" /><ref name="andreev94" /><ref name="fine139" /><ref>{{harvnb|Kazhdan|1991|p=317}}</ref> The defeat was so grave that one Byzantine soldier retired from society and became an ascetic under the name of [[Luke the Stylite]].<ref name="runciman147" />

The Byzantine sources have not recorded the aftermath of the battle but, according to the accounts of the contemporary Arab historian [[Muhammad ibn Jarir al-Tabari]], the Bulgarians marched towards Constantinople. Leo VI was in such a panic that he considered arming Arab prisoners of war and sending them against the Bulgarians in return for their freedom, but eventually abandoned the idea.<ref name="fine139" /><ref>{{harvnb|Zlatarski|1972|p=317}}</ref> Further negotiations followed until the Byzantines agreed to the Bulgarian demands.<ref name="fine139" />

== Aftermath ==
[[File:SimeonHungariansRadzivillChronicleFol14r.jpg|thumb|right|300px|alt=A page from a medieval manuscript|A depiction of a battle during the Byzantine–Bulgarian war of 894–896, ''[[Radziwiłł Chronicle]]'']]
The war ended with a peace treaty which confirmed the Bulgarian domination on the [[Balkan Peninsula|Balkans]],<ref name="whittow287">{{harvnb|Whittow|1996|p=287}}</ref> restored the status of Bulgaria as a [[most favoured nation]], abolished the commercial restrictions and obliged the Byzantine Empire to pay annual tribute. Under the treaty, the Byzantines also ceded an area between the [[Black Sea]] and [[Strandzha]]{{cref|a}} to Bulgaria. In exchange, the Bulgarians released the captured Byzantine soldiers and civilians, who were allegedly 120,000.<ref>{{harvnb|Fine|1991|pp=139–140}}</ref><ref>{{harvnb|Zlatarski|1972|pp=318–321}}</ref> The peace treaty remained in force until Leo VI's death in 912 although Simeon I did violate it following the [[Sack of Thessalonica (904)|Sack of Thessaloniki]] in 904, extracting further territorial concessions in [[Macedonia (region)|Macedonia]].<ref>{{harvnb|Fine|1991|p=140}}</ref>

The Bulgarian monarch was satisfied with the results and considered that he had superiority over the Byzantine Empire to achieve his political ambitions&nbsp;— to assume the throne in Constantinople.<ref name="andreev94" /> Despite the success, however, Simeon I realized that there was still a lot to do in order to prevail over the Empire for good. He needed his own political and ideological base and launched an ambitious construction programme in Preslav so that it could rival Constantinople.<ref>{{harvnb|Andreev|Lalkov|1996|pp=94–95}}</ref> In addition, Simeon I took precautions to reduce the Byzantine influence over the Western Balkans by imposing his authority over the [[Principality of Serbia (medieval)|Principality of Serbia]] in return for recognizing [[Petar Gojniković]] as its ruler.<ref>{{harvnb|Fine|1991|p=141}}</ref>

The devastation in Dobrudzha at the hands of the Magyars indicated how vulnerable Bulgaria was to attacks from the north under the influence of the Byzantine diplomacy.<ref name="whittow287" /> That experience paid off well in 917, when Simeon managed to counter the Byzantine efforts to ally with the [[Serbs]] or the [[Pechenegs]], and forced them to fight alone in the [[Battle of Achelous (917)|battle of Achelous]], where the Byzantines were soundly defeated in one of the biggest disasters in Byzantine history.<ref>{{harvnb|Andreev|Lalkov|1996|pp=99–100}}</ref>

== See also ==
{{Portal bar|Bulgarian Empire|Bulgaria|Byzantine Empire|Middle Ages|War}}
{|width=80%
|valign=top|
*[[Byzantine–Bulgarian wars]]
*[[Bulgarian–Hungarian Wars]]
|valign="top"|
*[[Medieval Bulgarian army]]
*[[Byzantine army]]
|}

== Footnotes ==
=== Notes ===
{{Cnote|a|The borders established after the treaty are unknown but according to contemporary chronicles in 907 the town of [[Kıyıköy|Medea]] lied on the Byzantine–Bulgarian border.<ref>{{harvnb|Zlatarski|1972|p=320}}</ref>}}

=== Citations ===
{{Reflist|25em}}

== Sources ==
{{Refbegin|35em}}
* {{cite book | ref={{harvid|Andreev|Lalkov|1996}}
 | title = Българските ханове и царе (The Bulgarian Khans and Tsars)
 | last = Андреев (Andreev)
 | first = Йордан (Jordan)
 | first2= Милчо (Milcho) |last2= Лалков (Lalkov)
 | chapter =
 | year = 1996
 | language = Bulgarian
 | publisher = Абагар (Abagar)
 | location = Велико Търново ([[Veliko Tarnovo]])
 | isbn = 954-427-216-X
 }}
* {{cite book | ref={{harvid|Bakalov et al|2003}}
 | title = История на българите от древността до края на XVI век 
 | trans-title = History of the Bulgarians from Antiquity to the end of the XVI century
 | last = Бакалов (Bakalov)
 | first = Георги (Georgi)
 | first2 = Петър (Petar)
 | last2 = Ангелов (Angelov)
 | first3 = Пламен (Plamen)
 | last3 = Павлов (Pavlov)
 | display-authors = etal 
 | chapter =
 | year = 2003
 | language = Bulgarian
 | publisher = Знание (Znanie)
 | location = София ([[Sofia]])
 | isbn = 954-621-186-9
 }}
* {{cite book | ref={{harvid|Bozhilov|Gyuzelev|1999}}
 | title = История на средновековна България VII–XIV век (History of Medieval Bulgaria VII–XIV centuries)
 | last = Божилов (Bozhilov)
 | first = Иван (Ivan)
 | first2 = Васил (Vasil)
 | last2 = Гюзелев (Gyuzelev)
 | author-link2 = Vasil Gyuzelev
 | chapter =
 | year = 1999
 | language = Bulgarian
 | publisher = Анубис (Anubis)
 | location = София (Sofia)
 | isbn = 954-426-204-0
 }}
* {{cite book  
 | title = Гръцки извори за българската история (ГИБИ), том V (Greek Sources for Bulgarian History (GIBI), volume V)
 | url=http://www.promacedonia.org/gibi/5/index.html
 | last = Колектив (Collective)
 | first =
 | chapter = 11. Продължителят на Теофан (11. Theophanis Continuati)
 | year = 1964
 | language = Bulgarian, Greek
 | publisher = Издателство на БАН ([[Bulgarian Academy of Sciences]] Press)
 | location = София (Sofia)
 | isbn =
 }}
* {{cite book | ref=harv
 | title = The Early Medieval Balkans, A Critical Survey from the Sixth to the Late Twelfth Century
 | last = Fine
 | first = J.
 | authorlink = John Van Antwerp Fine, Jr.
 | chapter =
 | year = 1991
 | publisher = [[University of Michigan Press]]
 | location =
 | isbn = 0-472-08149-7
 }}
* {{cite book | ref={{harvid|Kazhdan|1991}}
 | title = The Oxford Dictionary of Byzantium
 | last = Kazhdan
 | first = A.
 | authorlink = Alexander Kazhdan
 |author2=collective
 | chapter =
 | year = 1991
 | publisher = [[Oxford University Press]]
 | location = [[New York City|New York]], [[Oxford]]
 | isbn = 0-19-504652-8
 }}
* {{cite book | ref=harv
 | title = The Byzantine Commonwealth: Eastern Europe, 500–1453
 | last = Obolensky
 | first = D.
 | chapter =
 | year = 1971
 | publisher = [[Greenwood Publishing Group|Praeger Publishers]]
 | location = [[New York City|New York]], [[Washington, D.C.|Washington]]
 | isbn = 0-19-504652-8
 }}
* {{cite book | ref=harv
 |last=Runciman
 |first=Steven
 |authorlink=Steven Runciman
 |title=A History of the First Bulgarian Empire
 |url=http://www.promacedonia.org/en/sr/index.html
 |chapterurl=http://www.promacedonia.org/en/sr/sr_3_1.htm
 |chapter= The Two Eagles
 |publisher=[[George Bell & Sons]]
 |location=[[London]]
 |oclc= 832687
 |year=1930
}}
* {{cite book | ref=harv
 |last=Spinei
 |first=Victor
 |year=2003
 |title=The Great Migrations in the East and South East of Europe from the Ninth to the Thirteenth Century
 |publisher= Romanian Cultural Institute (Center for Transylvanian Studies) and Museum of Brăila Istros Publishing House
 |isbn= 973-85894-5-2
}}
* {{cite book | ref=harv
 | title = The Making of Byzantium (600–1025)
 | last = Whittow
 | first = Mark
 | chapter =
 | year = 1996
 | publisher = [[University of California Press]]
 | location = [[Los Angeles]]
 | isbn = 0-520-20497-2
 }}
* {{cite book | ref={{harvid|Zlatarski|1972}}
 |last=Златарски (Zlatarski)
 |first=Васил (Vasil)
 |authorlink=Vasil Zlatarski
 |title=История на българската държава през средните векове. Том I. История на Първото българско царство. (History of the Bulgarian state in the Middle Ages. Volume I. History of the First Bulgarian Empire.)
 |url=http://www.promacedonia.org/vz1b/index.html
 |edition=2
 |publisher=Наука и изкуство (Nauka i izkustvo)
 |location=София (Sofia)
 |year=1972
 |origyear=1927
 |language=Bulgarian
 |oclc=67080314
 }}
{{Refend}}

== External links ==
* {{cite web |url= http://sitemaker.umich.edu/mladjov/selections_on_byzantium& |title= Selections from the Chronicle of Ioannes Skylitzes, translated and adapted from B. Flusin and J.-C. Cheynet (2003) |last=Mladjov |first=Ian |work= |publisher= Hosted on the Department of History, University of Michigan |accessdate=8 November 2014}}

{{DEFAULTSORT:Byzantine-Bulgarian war of 894-896}}
[[Category:Byzantine–Bulgarian Wars]]
[[Category:Wars involving Hungary]]
[[Category:9th-century conflicts]]
[[Category:9th century in Bulgaria]]
[[Category:9th century in the Byzantine Empire]]
[[Category:Wars involving the First Bulgarian Empire]]