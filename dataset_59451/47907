{{Use British English|date=May 2012}}
{{Use dmy dates|date=May 2012}}
{{Infobox legislation
| short_title      = The Fiscal Responsibility and Budget Management Act, 2003
| image           = 
| imagesize       = 150
| imagelink       = 
| imagealt        = 
| caption         = 
| long_title       = An Act to provide for the responsibility of the Central Government to ensure inter – generational equity in fiscal management and long-term macro-economic stability by achieving sufficient revenue surplus and removing fiscal impediments in the effective conduct of monetary policy and prudential debt management consistent with fiscal sustainability through limits on the Central Government borrowings, debt and deficits, greater transparency in fiscal operations of the Central Government and conducting fiscal policy in a medium-term framework and for matters connected therewith or incidental thereto.
| citation        = [http://finmin.nic.in/law/frbmact2003.pdf Act No. 39 of 2003]
| enacted_by       = [[Parliament of India]]
| date_enacted     = 26 August 2003
| date_assented    = 26 August 2003
| date_signed      = 
| date_commenced   = 5 June 2004
| bill            = 
| bill_citation    = 
| bill_date        = 
| introduced_by    = Mr.[[Yashwant Sinha]]
| 1st_reading      = 
| 2nd_reading      = 
| 3rd_reading      = 
| white_paper      = 
| committee_report = 
| amendments      = 
| repeals         = 
| related         = 
| summary         = 
| keywords        = 
| status = in force
}}
The '''Fiscal Responsibility and Budget Management Act''', 2003 ([[FRBMA]]) is an [[Act of Parliament|Act]] of the [[Parliament of India]] to institutionalize financial discipline, reduce India's fiscal deficit, improve macroeconomic management and the overall management of the public funds by moving towards a [[balanced budget]] and strengthen fiscal prudence. The main purpose was to eliminate revenue deficit<ref group="Note">''Revenue Deficit'' is defined in the ''act'' as the difference between the revenue expenditure and revenue receipts which indicates increase in liabilities of the Central Government without corresponding increase in assets</ref> of the country (building revenue surplus thereafter) and bring down the [[fiscal deficit]] to a manageable 3% of the GDP by March 2008. However, due to the [[Financial crisis (2007-present)|2007 international financial crisis]], the deadlines for the implementation of the targets in the act was initially postponed and subsequently suspended in 2009. In 2011, given the process of ongoing recovery, [[Economic Advisory Council]] publicly advised the [[Government of India]] to reconsider reinstating the provisions of the FRBMA. [[N. K. Singh]] is currently the Chairman of the review committee for Fiscal Responsibility and Budget Management Act, 2003, under the [[Ministry of Finance (India)]], [[Government of India]].

==Enactment==
The Fiscal Responsibility and Budget Management ''Bill'' (FRBM Bill) was introduced in India by the then [[Finance Minister]] of India, Mr.[[Yashwant Sinha]]<ref name="Bill">{{cite news
 | title = Fiscal Responsibility Bill tabled 
 | url = http://www.hinduonnet.com/businessline/2000/12/21/stories/1421542w.htm
 | work = Business Line, The Hindu
 | date = 21 December 2000
 | accessdate =22 February 2011
}}</ref> in December, 2000. Firstly, the bill highlighted the terrible state of government finances in India both at the Union and the state levels under the statement of objects and reasons.<ref name="IE1">{{cite news
 | title = New Bill aims to cut fiscal deficit to 2%
 | url = http://www.expressindia.com/ie/daily/20001221/ibu21020.html
 | work = Express India, ENS Economic Bureau
 | date = 21 December 2000
 | accessdate =22 February 2011
}}</ref> Secondly, it sought to introduce the fundamentals of fiscal discipline at the various levels of the government.The FRBM bill was introduced with the broad objectives of eliminating revenue deficit by 31 Mar 2006, prohibiting government borrowings from the [[Reserve Bank of India]] three years after enactment of the bill, and reducing the fiscal deficit to 2% of GDP (also by 31 Mar 2006).<ref name="IE1"/> Further, the bill proposed for the government to reduce liabilities to 50% of the estimated GDP by year 2011. There were mixed reviews among economists about the provisions of the bill, with some criticising it as ''too drastic''.<ref>{{cite news
 | title = Fiscal Responsibility and Budget Management Bill – Some provisions `too drastic' 
 | url = http://www.hinduonnet.com/businessline/2000/12/29/stories/042920gs.htm
 | work = Business Line, The Hindu
 | date = 29 December 2000
 | accessdate =22 February 2011
}}</ref> Political debate ensued in the country. Several revisions later, it resulted in a much relaxed and watered-down version of the bill<ref name="Rediff1">{{cite news
 | title = Fiscal responsibility & Budget management
 | url = http://www.rediff.com/money/2003/may/15guest.htm
 | work = Rediff.com Business, A Seshan
 | date = 15 May 2003
 | accessdate =22 February 2011
}}</ref> (including postponing the date for elimination of revenue deficit to 31 March 2008) with some experts, like Dr Saumitra Chaudhuri of ICRA Ltd.<ref group="Note">Formerly Investment Information and Credit Rating Agency of India Limited</ref><ref>[http://www.icra.in/ ICRA Ltd]</ref>(and now a member of [[Economic Advisory Council|Prime Ministers' Economic Advisory Council]]) commenting, ‘‘all teeth of the Fiscal Responsibility Bill have been pulled out and in the current form it will not be able to deliver the anticipated results.’’<ref name="IE2">{{cite news 
 | title = Tenth Plan for adoption of Fiscal Responsibility Bill by the states
 | url = http://www.indianexpress.com/oldStory/12441/
 | work = Indian Express, ENS ECONOMIC BUREAU
 | date = 4 Nov 2002
 | accessdate =22 February 2011
}}</ref> This bill was approved by the Cabinet of Ministers of the Union Government of India in February, 2003<ref>{{cite news
 | title = Diluted Fiscal Responsibility Bill gets nod
 | url = http://www.indianexpress.com/oldStory/17828/
 | work = Indian Express, ENS ECONOMIC BUREAU
 | date = 5 Feb 2003
 | accessdate =22 February 2011
}}</ref> and following the due [[enactment of a bill|enactment]] process  of Parliament, it received the assent of the [[President of India]] on 26 August 2003.<ref name="Act">{{cite news
 | title = The Gazette of India
 | url = http://finmin.nic.in/law/frbmact2003.pdf
 | work = Controller of Publications, Government of India Press
 | date = 26 August 2003
 | accessdate =22 February 2011
}}</ref> Subsequently, it became effective on 5 July 2004.<ref name="BL2">{{cite news
 | title = Fiscal responsibility versus democratic accountability 
 | url = http://www.thehindubusinessline.in/2004/07/27/stories/2004072700081100.htm
 | work = Business Line, C. P. Chandrasekhar & Jayati Ghosh
 | date = 27 July 2004
 | accessdate =22 February 2011
}}</ref> This would serve as the ''day of commencement of this Act''.

==Objectives==
The main objectives of the act were:<ref name="CBGA">{{cite news
 | title = FRBM – A Review
 | url = http://www.cbgaindia.org/files/primers_manuals/FRBM%20-%20A%20Review.pdf
 | work = Centre for Budget and Governance Accountability
 | year = 2007
 | accessdate =22 February 2011
}}</ref>
# to introduce transparent fiscal management systems in the country
# to introduce a more equitable and manageable distribution of the country's debts over the years
# to aim for fiscal stability for India in the long run
Additionally, the act was expected to give necessary flexibility to [[Reserve Bank of India]](RBI) for managing inflation in India.<ref name=TFE1>{{cite news|title=Fiscal Responsibility Bill to help maintain stable inflation rate|url=http://www.financialexpress.com/printer/news/121453/|accessdate=25 February 2011|newspaper=The Financial Express|date=24 December 2004}}</ref>

==Content of the Act==
Since the act was primarily for the management of the governments' behaviour, it provided for certain documents to be tabled in the [[Parliament of India|Parliament]] annually with regards to the country's fiscal policy.<ref name="Act"/> This included the following along with the [[Union budget of India|Annual Financial Statement and ''demands for grants'']]:
# a document titled ''Medium-term Fiscal Policy Statement'' – This report was to present a three-year rolling target for the ''fiscal indicators'' <ref group="Note">The ''fiscal indicators'' was defined in this law as numerical ceilings and proportions to Gross Domestic Product, as may be prescribed for evaluation of the fiscal position of the Central Government.</ref> with any assumptions, if applicable. This statement was to further include an assessment of sustainability with regards to ''revenue deficit'' and the use of capital receipts of the Government (including market borrowings) for generating productive assets.
# a document titled ''Fiscal Policy Strategy Statement'' – This was a tactical report enumerating strategies and policies for the upcoming [[Financial Year]] including strategic fiscal priorities, taxation policies, key fiscal measures and an evaluation of how the proposed policies of the Central Government conform to the 'Fiscal Management Principles' of this act.
# a document titled  ''Macro-economic Framework Statement'' – This report was to contain forecasts enumerating the growth prospects of the country. GDP growth, revenue balance, gross fiscal balance and external account balance of the balance of payments were some of the key indicators to be included in this report.
The Act further required the government to develop measures to promote ''fiscal transparency and reduce secrecy'' in the preparation of the Government financial documents including the Union Budget.

===Fiscal management principles===
The Central Government, by rules made by it, was to specify the following:<ref name="Act"/>
#a plan to eliminate revenue deficit by 31 Mar 2008 by setting annual targets for reduction starting from day of commencement of the act.
#reduction of annual fiscal deficit of the country
#annual targets for assuming contingent liabilities in the form of guarantees and the total liabilities as a percentage of the GDP

===Borrowings from Reserve Bank of India===
The Act provided that the Central Government shall not borrow from the [[Reserve Bank of India]](RBI) except under exceptional circumstances where there is temporary shortage of cash in particular financial year. It also laid down rules to prevent RBI from trading in the primary market for Government securities. It restricted them to the trading of Government securities in the secondary market after an April, 2005, barring situations highlighted in ''exceptions'' paragraph.

====Exceptions====
National security, natural calamity or other ''exceptional grounds that the Central Government may specify'' were cited as reasons for not implementing the targets for ''fiscal management principles, prohibition on borrowings from RBI and fiscal indicators'' highlighted above, provided they were approved by both the Houses of the Parliament as soon as possible, once these targets had been exceeded.<ref name="Act"/>

==Measures to enforce compliance==
This was a particularly weak area of the act. It required the Finance Minister of India to only conduct quarterly reviews of the receipts and expenditures of the Government and place these reports before the Parliament. Deviations to targets set by the Central government for fiscal policy had to be approved by the Parliament.<ref name="Act"/> No other measures for failure of compliance have been specified.

==Implementation==

===Targets and fiscal indicators===
Subsequent to the enactment of the FRBMA, the following targets and fiscal indicators were agreed by the Central government:<ref name="BL2"/><ref name="FRBMR">[http://finmin.nic.in/law/frbmrules2004.pdf Fiscal Responsibility and Budget Management Rules 2004]</ref>
*Revenue deficit
** Date of elimination – 31 March 2009 (postponed from 31 March 2008)
** Minimum Annual reduction – 0.5% of GDP
*Fiscal Deficit
** Ceiling – 3% of the GDP by 31 Mar 2008
** Minimum Annual reduction – 0.3% of GDP
*Total Debt – 9% of the GDP (a target increased from the original 6% requirement in 2004–05)
** Annual Reduction – 1% of GDP
*RBI purchase of Government bonds – to cease from 1 April 2006
Four fiscal indicators to be projected in the medium term fiscal policy statement were proposed. These are, revenue deficit as a percentage of GDP, fiscal deficit as a percentage of GDP, tax revenue as percentage of GDP and total outstanding liabilities as percentage of GDP.<ref name="FRBMR"/>

===Jurisdiction===
The residuary powers to make rules with respect to this act were with the Central Government<ref name="Act"/> with subsequent presentation before the Parliament for ratification. Civil courts of the country had no jurisdiction for enforcement of this act or decisions made therein. The power to remove difficulties was also entrusted to the Central Government.

==Criticism==
Some quarters, including the subsequent Finance Minister Mr. [[P. Chidambaram]], criticised the act and its rules as ''adverse'' since it might require the government to cut back on social expenditure necessary to create ''productive assets'' and general upliftment of rural poor of India.<ref name="BL2"/> The vagaries of [[monsoon]] in India, the social dependence on agriculture and over-optimistic projections of the task force in-charge of developing the ''targets'' were highlighted as some of the potential failure points of the Act. However, other viewpoints insisted that the act would benefit the country by maintaining stable inflation rates which in turn would promote social progress.<ref name="TFE1">{{cite news
 | title = Fiscal Responsibility Bill to help maintain stable inflation rate
 | url = http://www.financialexpress.com/printer/news/121453/
 |work=The Financial Express 
 | date = 24 December 2004
 | accessdate =22 February 2011
}}</ref>

Some others have drawn parallel to this act's international counterparts like the [[Gramm-Rudman-Hollings Balanced Budget Act|Gramm-Rudman-Hollings Act]] (US) and the [[Growth and Stability Pact]] (EU) to point out the futility of enacting laws whose relevance and implementation over time is bound to decrease.<ref name="BL2"/>  They described the law as ''wishful thinking'' and a ''triumph of hope over experience''. Parallels were drawn to the US experience of [[enactment of a bill|enacting]] ''debt-ceilings'' and how lawmakers have traditionally been able to amend such laws to their own political advantage.<ref name = "Rediff1"/> Similar fate was predicted for the Indian version which indeed was suspended in 2009 when the economy hit rough patches.<ref name="LM"/>

==Suspension and reinstatement==
Implementing the Act, the government had managed to cut the fiscal deficit to 2.7% of GDP and revenue deficit to 1.1% of GDP in 2007–08.<ref name=TET27022010>{{cite news|title=Budget 2010: How FM tamed the deficit|url=http://economictimes.indiatimes.com/news/union-budget/Budget-2010-How-FM-tamed-the-deficit/articleshow/5622365.cms|accessdate=25 February 2011|newspaper=The Economic Times|date=27 February 2010}}</ref> However, given the [[Financial crisis (2007-present)|international financial crisis of 2007]], the deadlines for the implementation of the targets in the act were suspended.<ref name="LM">{{cite news
 | title = Caution in troubled times
 | url = http://www.livemint.com/2009/07/06223033/Caution-in-troubled-times.html
 | work = Ashok K. Lahiri, livemint.com – Economics & Politics
 | date = 7 July 2009
 | accessdate =22 February 2011
}}</ref><ref name=TET27022010/> The fiscal deficit rose to 6.2%<ref name=DH>{{cite news|title=Fiscal deficit falls 17 per cent to Rs 1.5 lakh during April–August|url=http://www.deccanherald.com/content/101001/fiscal-deficit-falls-17-per.html|accessdate=25 February 2011|newspaper=Deccan Herald|date=30 September 2010}}</ref> of GDP in 2008–09 against the target of 3% set by the Act for 2008–09.<ref name=TET31012011>{{cite news|title=Fiscal deficit down 44.75pc to Rs 1.71 lakh cr during Apr–Dec|url=http://economictimes.indiatimes.com/news/economy/indicators/fiscal-deficit-down-4475pc-to-rs-171-lakh-cr-during-apr-dec/articleshow/7398122.cms|accessdate=25 February 2011|newspaper=The Economic Times|date=31 Jan 2011}}</ref> However, IMF estimated fiscal deficit to be 8% after accounting for oil bonds and other off budget expenses.<ref name=ZN>{{cite web|title=India's fiscal responsibility legislation should be reformed: IMF|url=http://www.zeenews.com/news555740.html|work=Bureau Report|publisher=zeenews.com|accessdate=25 February 2011}}</ref>

In August 2009, IMF had opined that India should implement fiscal reform at the soonest possible, enacting a successor to the current act.<ref name=ZN/> This IMF paper was authored by two senior IMF economists Alejandro Sergio Simone and Petia Topalova<ref>{{cite news|title=India’s fiscal responsibility performance is mixed: IMF Paper|url=http://www.thehindubusinessline.in/2009/08/16/stories/2009081650870500.htm|accessdate=25 February 2011|newspaper=Business Line|date=16 August 2009}}</ref> and highlighted the shortcomings of the current law along with proposed improvements for a new version.

It was reported that the Thirteenth [[Finance Commission of India]] was working on a new plan for reinstating fiscal management in India.<ref name=rediff2>{{cite news|last=John Samuel Raja D|first=Sapna Singh|title=Spread fiscal deficit target over 5–6 yrs: Govt|url=http://business.rediff.com/report/2009/jun/01/bcrisis-spread-fiscal-deficit-target-over-5-6-yrs-govt.htm|accessdate=25 February 2011|newspaper=Rediff.com|date=1 June 2009}}</ref> The initial expectation for revival of ''fiscal prudence'' was in 2010–11<ref name="LM">{{cite news
 | title = Union Budget 2009–'10: Mission to rein in deficit in ’10–11 when revival is expected
 | url = http://economictimes.indiatimes.com/News/Economy/Policy/Union-Budget-2009-10-Mission-to-rein-in-deficit-in-10-11-when-revival-is-expected/articleshow/4746709.cms
 | work = Deepshikha Sikarwar,Economic Times Bureau 
 | date = 7 July 2009
 | accessdate =22 February 2011
}}</ref> but was further delayed. Finally, the government did announce a path of fiscal consolidation starting from fiscal deficit of 6.6% of GDP in 2009–10 to a target of 3.0% by 2014–15<ref name=Skoch>{{cite web|last=Taraport|first=S.S.|title=Budget 2011–12: Need For an Equitable Fiscal Consolidation|url=http://www.inclusion.in/index.php?option=com_content&view=article&id=582|work=Inclusion|publisher=Skoch Media Pvt. Ltd.|accessdate=26 February 2011}}</ref> However, eminent economist and ex-RBI Deputy Governor, [[S.S. Tarapore]] is quick to highlight the use of [[creative accounting]] to misrepresent numbers in the past. Furthermore, he added that fiscal consolidation is indeed vital for India, as long as the needs of the poor citizens are not marginalised. This need for financial inclusion of the poor while maintaining the fiscal discipline was highlighted by him as the most critical requirement for the 2011–12 [[Union budget of India|Budget of India]].

More recently, in February 2011, the [[PMEAC]] recommended the need for reinstatement of fiscal discipline of the [[Government of India]], starting 2011–12 financial year.<ref>{{cite news
 | title = PMEAC asks govt to withdraw few tax exemptions
 | url = http://www.business-standard.com/india/news/pmeac-asks-govt-to-withdrawfew-tax-exemptions-in-budget/426102/
 | work = BS reported
 | date = 22 February 2011
 | accessdate =22 February 2011
}}</ref> In FY 2011–12, it is almost certain that government will cross budgetary fiscal deficit target of 4.6% and it will be around 5%.

==States level fiscal responsibility legislations in India==
The tenth plan of the [[Planning Commission of India]] highlighted the need for fiscal discipline at even the level of the states.<ref name="IE2"/> This was to reduce the [[debt-to-GDP ratio]] of India. [[Reserve Bank of India]](RBI),in its role as the ultimate financial authority in India, was also a keen supporter of the concept and publicly highlighted the need for state level fiscal responsibility legislations in India.<ref name="TFE1"/> By 2007, the states of [[Karnataka]],
[[Kerala]], [[Punjab, India|Punjab]], Nadu, [[Maharashtra]] and [[Uttar Pradesh]] are among those which have already legislated the required fiscal discipline laws at the state level.<ref name="CBGA"/><ref name=WB>{{cite web|title=Fiscal Responsibility Legislation at the State Level in India: is it working?|url=http://info.worldbank.org/etools/docs/library/239556/Howes.pdf|work=Stephen Howes|publisher=World Bank|accessdate=25 February 2011}}</ref>

==Notes==
<references group="Note"/>
{{Economy of India topics}}
{{India topics}}

==References==
{{reflist|2}}

==External links==
{{Sisterlinks|Fiscal Responsibility and Budget Management Act}}
{{Portal|India|Economics|Government of India}}
*[http://lawyersclubindia.com/bare_acts/Fiscal-Responsibility-and-Budget-Management-Act-394.asp Fiscal Responsibility and Budget Management Act,2003 Act No : 39 of 2003 Online version]

{{Indian legislations}}

[[Category:Union budgets of India]]
[[Category:Vajpayee administration initiatives]]
[[Category:Indian business law]]
[[Category:Acts of the Parliament of India 2003]]