{{Infobox journal
| title = Michigan Law Review
| editor = John He
| discipline = [[Law]]
| abbreviation = Mich. Law Rev.
| publisher = [[University of Michigan Law School]]
| country = United States
| frequency = Monthly
| history = 1902-present
| openaccess =
| impact = 2.122
| impact-year = 2009
| website = http://michiganlawreview.org/
| link1 = http://michiganlawreview.org/category/current-issue/
| link1-name = Online access
| link2 = http://michiganlawreview.org/category/articles-essays/
| link2-name = Online archive
| cover = [[File:Michigan Law Review cover.jpg|150px|A typical ''Michigan Law Review'' cover.]]
| ISSN = 0026-2234
| eISSN =
| JSTOR = 00262234
| OCLC = 1757366
}}
The '''''Michigan Law Review''''' (''[[Bluebook]]'' abbreviation: ''Mich. L. Rev.'') is an American [[law review]] established in 1902, after [[Gustavus Ohlinger]], a student in the [[University of Michigan Law School|Law Department]] (now the Law School) of the [[University of Michigan]], approached the dean with a proposal for a law journal. The ''Michigan Law Review'' was originally intended as a forum in which the faculty of the Law Department could publish its legal scholarship. The faculty resolution creating the ''Michigan Law Review'' required every faculty member to submit two articles per year to the new journal.

== History ==
From its inception until 1940, the ''Michigan Law Review'''s student members worked under the direction of faculty members who served as [[editor-in-chief]]. The first of these was [[Floyd Mechem]], the last [[Paul Kauper]]. In 1940, the first student editor-in-chief was selected. During the years that followed, student editors were given increasing responsibility and autonomy; today, the ''Michigan Law Review'' is run with no faculty supervision. The current editor-in-chief is Erin Chapman. Seven of each volume's eight issues ordinarily are composed of two major parts: "Articles" by legal scholars and practitioners and "Notes" written by the student editors. One issue in each volume is devoted to book reviews. Occasionally special issues are devoted to symposia or colloquia.

The ''Michigan Raw Review'', a parody of the ''Michigan Law Review'', was published annually by the Barristers Society, a self-styled honorary at the University of Michigan Law School. The ''Raw Review'' used the same cover, layout, and typeface, but contained content totally dissimilar, leaning to the "insulting and semi-pornographic".<ref>[http://hdl.handle.net/2027.42/55586 Swift, Theodore W., "There's a unicorn in the garden", ''Law Quadrangle Notes'' (Fall, 1981)] Reprinted in Frazier, Richard, ''Let the Record Show'', Michigan State University Press, ISBN 0-87013-425-6 (1997)p 284</ref>

== Significant articles ==
{{Or|date=March 2011}}
{{laundry|section|date=May 2015}}
*{{cite journal |last=Gregory |first=Charles Noble |authorlink= |coauthors= |year=1904 |month= |title=Jurisdiction over Foreign Ships in Territorial Waters |journal=Michigan Law Review |volume=2 |issue=5 |pages=333–357 |doi=10.2307/1273556 |jstor= 1273556|publisher=Michigan Law Review, Vol. 2, No. 5 }}
*{{cite journal |last=Fairlie |first=John A. |authorlink= |coauthors= |year=1920 |month= |title=Administrative Legislation |journal=Michigan Law Review |volume=18 |issue=3 |pages=181–200 |doi=10.2307/1277269 |jstor= 1277269|publisher=Michigan Law Review, Vol. 18, No. 3 }}
*{{cite journal |last=Prosser |first=William L. |authorlink= |coauthors= |year=1939 |month= |title=Intentional Infliction of Mental Suffering: A New Tort |journal=Michigan Law Review |volume=37 |issue=6 |pages=874–892 |doi=10.2307/1282744 |jstor= 1282744|publisher=Michigan Law Review, Vol. 37, No. 6 }}
*{{cite journal |last=Dawson |first=John P. |authorlink= |coauthors= |year=1947 |month= |title=Economic Duress—An Essay in Perspective |journal=Michigan Law Review |volume=45 |issue=3 |pages=253–290 |doi=10.2307/1283644 |jstor= 1283644|publisher=Michigan Law Review, Vol. 45, No. 3 }}
*{{cite journal |last=Estep |first=Samuel D. |authorlink= |coauthors= |year=1960 |month= |title=Radiation Injuries and Statistics: The Need for a New Approach to Injury Litigation |journal=Michigan Law Review |volume=59 |issue=2 |pages=259–304 |doi=10.2307/1286328 |jstor= 1286328|publisher=Michigan Law Review, Vol. 59, No. 2 }}
*{{cite journal |last=Sax |first=Joseph L. |authorlink= |coauthors= |year=1970 |month= |title=The Public Trust Doctrine in Natural Resource Law: Effective Judicial Intervention |journal=Michigan Law Review |volume=68 |issue=3 |pages=471–566 |doi=10.2307/1287556 |jstor= 1287556|publisher=Michigan Law Review, Vol. 68, No. 3 }}
*{{cite journal |last=Lempert |first=Richard O. |authorlink= |coauthors= |year=1977 |month= |title=Modeling Relevance |journal=Michigan Law Review |volume=75 |issue=5/6 |pages=1021–1057 |doi=10.2307/1288024 |jstor= 1288024|publisher=Michigan Law Review, Vol. 75, No. 5/6 }}
*{{cite journal |last=Braithwaite |first=John |authorlink= |coauthors= |year=1982 |month= |title=Enforced Self-Regulation: A New Strategy for Corporate Crime Control |journal=Michigan Law Review |volume=80 |issue=7 |pages=1466–1507 |doi=10.2307/1288556 |jstor= 1288556|publisher=Michigan Law Review, Vol. 80, No. 7 }}
*{{cite journal |last=Ulen |first=Thomas S. |authorlink= |coauthors= |year=1984 |month= |title=The Efficiency of Specific Performance: Toward a Unified Theory of Contract Remedies |journal=Michigan Law Review |volume=83 |issue=2 |pages=341–403 |doi=10.2307/1288569 |jstor= 1288569|publisher=Michigan Law Review, Vol. 83, No. 2 }}
*{{cite journal |last=Delgado |first=Richard |authorlink= |coauthors= |year=1989 |month= |title=Storytelling for Oppositionists and Others: A Plea for Narrative |journal=Michigan Law Review |volume=87 |issue=8 |pages=2411–2441 |doi=10.2307/1289308 |jstor= 1289308|publisher=Michigan Law Review, Vol. 87, No. 8 }}
*{{cite journal |last=Esty |first=Daniel C. |authorlink= |coauthors= |year=1996 |month= |title=Revitalizing Environmental Federalism |journal=Michigan Law Review |volume=95 |issue=3 |pages=570–653 |doi=10.2307/1290162 |jstor= 1290162|publisher=Michigan Law Review, Vol. 95, No. 3 }}

== Notable alumni ==
{{Div col||40em}}
* [[Steven G. Bradbury]], former head of the [[Office of Legal Counsel]]
* [[David W. Belin]], assistant counsel to the [[Warren Commission]]
* [[Charles Blakey Blackmar]], former Chief Justice, [[Supreme Court of Missouri]]
* [[Ann Coulter]], conservative commentator, author, and syndicated columnist
* [[George E. Cryer]], former mayor of [[Los Angeles]]
* [[David M. Ebel]], judge, [[United States Court of Appeals for the Tenth Circuit]]
* [[Harry T. Edwards]], former Chief Judge, [[United States Court of Appeals for the District of Columbia Circuit]]
* [[Jeffrey L. Fisher]], [[United States Supreme Court|Supreme Court]] litigator
* [[Robert Jerry]], dean of the [[University of Florida Levin College of Law]]
* [[Alex Joel]], first Civil Liberties Protection Officer for the [[Office of the Director of National Intelligence]]
* [[Sally Katzen]], former administrator of [[Office of Information and Regulatory Affairs|OIRA]]
* [[W. Wallace Kent]], former judge, [[United States Court of Appeals for the Sixth Circuit]]
* Bob Kimball, former President and CEO of [[RealNetworks]]
* [[Dave Kopel]], conservative pundit and blogger
* [[Jeffrey P. Minear]], Counselor to [[United States Supreme Court|Supreme Court]] [[Chief Justice of the United States|Chief Justice]] [[John G. Roberts, Jr.]].
* Benjamin Mizer, [[Solicitor General of Ohio]]
* [[Ronald Olson|Ronald L. Olson]], "name partner" in the Los Angeles office of the law firm of [[Munger, Tolles & Olson]] LLP
* [[Doug Pappas]], baseball writer and researcher who was considered the foremost expert on the business of baseball
* [[John Porter (Illinois politician)|John Porter]], former [[United States Representative]] from [[Illinois]]
* [[Daniel Tarullo]], member of the Board of Governors of the [[Federal Reserve System]]
* [[David Westin]], President of [[ABC News]]
* [[James D. Zirin]], lawyer, writer and cable TV talk show host
{{div col end}}

== References ==
{{Reflist}}

== Further reading ==
*{{cite journal |last=Stason |first=E. Blythe |authorlink= |coauthors= |year=1952 |month= |title=The Law Review—Its First Fifty Years |journal=Michigan Law Review |volume=50 |issue=8 |pages=1134–1138 |doi=10.2307/1284411 |jstor= 1284411|publisher=Michigan Law Review, Vol. 50, No. 8 }}

== External links ==
* {{Official website|http://www.michiganlawreview.org/}}

[[Category:American law journals]]
[[Category:General law journals]]
[[Category:Publications established in 1902]]
[[Category:University of Michigan]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:1902 establishments in Michigan]]
[[Category:Law journals edited by students]]