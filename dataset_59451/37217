{{infobox barium}}
'''Barium''' is a [[chemical element]] with symbol&nbsp;'''Ba''' and [[atomic number]]&nbsp;56. It is the fifth element in Group 2, a soft silvery [[metal]]lic [[alkaline earth metal]]. Because of its high chemical [[Reactivity (chemistry)|reactivity]], barium is never found in nature as a free element. Its hydroxide, known in pre-modern history as [[Barium hydroxide|baryta]], does not occur as a [[mineral]], but can be prepared by heating barium carbonate.

The most common naturally occurring minerals of barium are [[Baryte|barite]] ([[barium sulfate]], BaSO<sub>4</sub>) and [[witherite]] ([[barium carbonate]], BaCO<sub>3</sub>), both insoluble in water. The barium name originates from the alchemical derivative "baryta", from [[Greek language|Greek]] βαρύς (''barys''), meaning "heavy."  '''Baric''' is the adjective form of barium. Barium was identified as a new element in 1774, but not reduced to a metal until 1808 with the advent of [[electrolysis]].

Barium has few industrial applications.  Historically, it was used as a [[getter]] for [[vacuum tube]]s. It is a component of [[Yttrium barium copper oxide|YBCO]] ([[High-temperature superconductivity|high-temperature superconductors]]) and electroceramics, and is added to steel and cast iron to reduce the size of carbon grains within the microstructure. Barium compounds are added to fireworks to impart a green color. [[Barium sulfate]] is used as an insoluble additive to [[oil well]] [[drilling fluid]], as well as in a purer form, as X-ray [[radiocontrast agent]]s for imaging the human gastrointestinal tract. The soluble barium [[ion]] and soluble compounds are poisonous, and have been used as [[rodenticide]]s.

==Characteristics==

===Physical properties===
[[File:Barium 1.jpg|thumb|left|Oxidized barium]]
Barium is a soft, silvery-white metal, with a slight golden shade when ultrapure.<ref name="Ullman2005">{{cite book |author=Kresse, Robert |author2=Baudis, Ulrich |author3=Jäger, Paul |author4=Riechers, H. Hermann |author5=Wagner, Heinz |author6=Winkler, Jocher |author7=Wolf, Hans Uwe |chapter=Barium and Barium Compounds |editor=Ullman, Franz |title=Ullmann's Encyclopedia of Industrial Chemistry |date=2007 |publisher=Wiley-VCH |doi=10.1002/14356007.a03_325.pub2}}</ref>{{rp|2}} The silvery-white color of barium metal rapidly vanishes upon [[redox|oxidation]] in air yielding a dark gray [[Barium oxide|oxide]] layer. Barium has a medium [[specific weight]] and good electrical conductivity. Ultrapure barium is very difficult to prepare, and therefore many properties of barium have not been accurately measured yet.<ref name="Ullman2005" />{{rp|2}}

At room temperature and pressure, barium has a [[body-centered cubic]] structure, with a barium–barium distance of 503 [[picometer]]s, expanding with heating at a rate of approximately 1.8{{e|-5}}/°C.<ref name="Ullman2005" />{{rp|2}} It is a very soft metal with a [[Mohs hardness]] of 1.25.<ref name="Ullman2005" />{{rp|2}} Its melting temperature of {{convert|1000|K|C F}}<ref name="Lide2004">{{cite book|last = Lide |first= D. R. |title = CRC Handbook of Chemistry and Physics |edition = 84th |location = Boca Raton (FL) |publisher = CRC Press |date = 2004 |isbn = 978-0-8493-0484-2}}</ref>{{rp|4–43}} is intermediate between those of the lighter strontium ({{convert|1050|K|C F|disp=or}})<ref name="Lide2004" />{{rp|4–86}} and heavier radium ({{convert|973|K|C F|disp=or}});<ref name="Lide2004" />{{rp|4–78}} however, its boiling point of {{convert|2170|K|C F}} exceeds that of strontium ({{convert|1655|K|C F|disp=or}}).<ref name="Lide2004" />{{rp|4–86}} The density (3.62&nbsp;g·cm<sup>−3</sup>)<ref name="Lide2004" />{{rp|4–43}} is again intermediate between those of strontium (2.36&nbsp;g·cm<sup>−3</sup>)<ref name="Lide2004" />{{rp|4–86}} and radium (~5&nbsp;g·cm<sup>−3</sup>).<ref name="Lide2004" />{{rp|4–78}}

===Chemical reactivity===
Barium is chemically similar to magnesium, calcium, and strontium, but even more reactive. It always exhibits the oxidation state of +2, except in a few rare and unstable molecular species that are only characterised in the gas phase such as BaF.<ref name="Ullman2005" />{{rp|2}} Reactions with [[chalcogen]]s are highly [[exothermic reaction|exothermic]] (release energy); the reaction with oxygen or air occurs at room temperature, and therefore barium is stored under oil or in an inert atmosphere.<ref name="Ullman2005" />{{rp|2}} Reactions with other [[nonmetal]]s, such as carbon, nitrogen, phosphorus, silicon, and hydrogen, are generally exothermic and proceed upon heating.<ref name="Ullman2005" />{{rp|2–3}} Reactions with water and alcohols are very exothermic and release hydrogen gas:<ref name="Ullman2005" />{{rp|3}}
: Ba + 2 ROH → Ba(OR)<sub>2</sub> + H<sub>2</sub>↑ (R is an alkyl or a hydrogen atom)

Barium reacts with [[ammonia]] to form complexes such as Ba(NH<sub>3</sub>)<sub>6</sub>.<ref name="Ullman2005" />{{rp|3}}

The metal is readily attacked by most acids. [[Sulfuric acid]] is a notable exception because [[Passivation (chemistry)|passivation]] stops the reaction by forming the insoluble [[barium sulfate]] on the surface.<ref>{{cite book|author=Müller, Hermann|chapter=Sulfuric Acid and Sulfur Trioxide|editor=Ullman, Franz|title=Ullmann's Encyclopedia of Industrial Chemistry|date=2007|publisher=Wiley-VCH|doi=10.1002/14356007.a03_325.pub2}}</ref> Barium combines with several metals, including [[aluminium]], [[zinc]], [[lead]], and [[tin]], forming [[intermetallics|intermetallic phases]] and alloys.<ref>{{cite book|author= Ferro, Riccardo|author2= Saccone, Adriana|last-author-amp= yes|page=355|title=Intermetallic Chemistry|publisher=Elsevier|date=2008|isbn=978-0-08-044099-6}}</ref>

===Compounds===
{| class="wikitable" style="float:left; margin-top:0; margin-right:1em; text-align:center; font-size:10pt; line-height:11pt; width:25%;"
|+ style="margin-bottom: 5px;"|Selected alkaline earth and zinc salts densities, g·cm<sup>−3</sup>
|-
!
! [[oxide|{{chem|O|2-}}]]
! [[sulfide|{{chem|S|2-}}]]
! [[fluoride|{{chem|F|-}}]]
! [[chloride|{{chem|Cl|-}}]]
! [[sulfate|{{chem|SO|4|2-}}]]
! [[carbonate|{{chem|CO|3|2-}}]]
! [[peroxide|{{chem|O|2|2-}}]]
! [[hydride|{{chem|H|-}}]]
|-
! scope="row"|[[calcium|{{chem|Ca|2+}}]]<ref name="Lide2004" />{{rp|4–48–50}}
|3.34
|2.59
|3.18
|2.15
|2.96
|2.83
|2.9
|1.7
|-
! scope="row"|[[strontium|{{chem|Sr|2+}}]]<ref name="Lide2004" />{{rp|4–86–88}}
|5.1
|3.7
|4.24
|3.05
|3.96
|3.5
|4.78
|3.26
|-
! scope="row" style="background:#ff9;"| '''''{{chem|Ba|2+}}'''''<ref name="Lide2004" />{{rp|4–43–45}}
| style="background:#ff9;"| ''5.72''
| style="background:#ff9;"| ''4.3''
| style="background:#ff9;"| ''4.89''
| style="background:#ff9;"| ''3.89''
| style="background:#ff9;"| ''4.49''
| style="background:#ff9;"| ''4.29''
| style="background:#ff9;"| ''4.96''
| style="background:#ff9;"| ''4.16''
|-
! scope="row"|[[zinc|{{chem|Zn|2+}}]]<ref name="Lide2004" />{{rp|4–95–96}}
|5.6
|4.09
|4.95
|2.09
|3.54
|4.4
|1.57
|—
|}

Barium salts are typically white when solid and colorless when dissolved, and barium ions provide no specific coloring.<ref>{{cite book|page=87|title=Qualitative analysis and the properties of ions in aqueous solution|author=Slowinski, Emil J.|author2=Masterton, William L.|publisher=Saunders|date=1990|edition=2nd|isbn=978-0-03-031234-2}}</ref> They are denser than the [[strontium]] or [[calcium]] analogs, except for the [[halide]]s (see table; [[zinc]] is given for comparison).

[[Barium hydroxide]] ("baryta") was known to alchemists, who produced it by heating barium carbonate. Unlike calcium hydroxide, it absorbs very little CO<sub>2</sub> in aqueous solutions and is therefore insensitive to atmospheric fluctuations. This property is used in calibrating pH equipment.

Volatile barium compounds burn with a green to pale green [[flame test|flame]], which is an efficient test to detect a barium compound. The color results from [[spectral line]]s at 455.4, 493.4, 553.6, and 611.1&nbsp;nm.<ref name="Ullman2005" />{{rp|3}} <!---BaO forms a peroxide when heated in air.<ref name=O2/>--->

[[Group 2 organometallic chemistry#Organobarium|Organobarium compounds]] are a growing field of knowledge: recently discovered are dialkylbariums and alkylhalobariums.<ref name="Ullman2005" />{{rp|3}}

===Isotopes===
{{main|Isotopes of barium}}

Barium found in the Earth's crust is a mixture of seven primordial nuclides, barium-130, 132, and 134 through 138.<ref name="iso"/> Barium-130 undergoes very slow [[radioactive decay]] to [[xenon]]-130 by double [[Beta decay|beta plus decay]], and barium-132 theoretically decays like xenon-132, with half-lives a thousand times greater than the [[age of the Universe]].<ref name="NUBASE">{{cite journal| first = Audi| last = Georges|title = The NUBASE Evaluation of Nuclear and Decay Properties| journal = Nuclear Physics A| volume = 729| pages = 3–128| publisher = Atomic Mass Data Center| date = 2003| doi=10.1016/j.nuclphysa.2003.11.001| bibcode=2003NuPhA.729....3A| last2 = Bersillon| first2 = O.| last3 = Blachot| first3 = J.| last4 = Wapstra| first4 = A. H.| url = http://hal.in2p3.fr/in2p3-00014184}}</ref> The abundance is ~0.1% that of natural barium.<ref name="iso">{{cite journal|date=2003 |title=Atomic weights of the elements. Review 2000 (IUPAC Technical Report)|journal=[[Pure and Applied Chemistry]] |volume=75 |issue=6 |pages=683–800 |doi=10.1351/pac200375060683|last1=De Laeter|first1=J. R.|last2=Böhlke|first2=J. K.|last3=De Bièvre|first3=P.|last4=Hidaka|first4=H.|last5=Peiser|first5=H. S.|last6=Rosman|first6=K. J. R.|last7=Taylor|first7=P. D. P.}}</ref> The radioactivity of these isotopes is so weak that they pose no danger to life. 

Of the stable isotopes, barium-138 composes 71.7% of all barium, and the lighter the isotope, the less abundant.<ref name="iso" /> 

In total, barium has about 50 known isotopes, ranging in mass between 114 and 153. The most stable metastable isotope is barium-133 with a half-life of approximately 10.51&nbsp;years.  Five other isotopes have half-lives longer than a day.<ref name="NUBASE" /> Barium also has 10 [[meta state]]s, out of which barium-133m1 is the most stable with a half-life of about 39 hours.<ref name="NUBASE" /><!---<sup>133</sup>Ba is a standard calibrant for [[gamma-ray]] detectors in nuclear physics studies.--->

==History==
[[File:Humphry Davy Engraving 1830.jpg|thumb|left|upright|Sir Humphry Davy, who first isolated barium metal]]
Alchemists in the early Middle Ages knew about some barium minerals. Smooth pebble-like stones of mineral barite found in [[Bologna]], [[Italy]], were known as "Bologna stones." Alchemists were attracted to them because after exposure to light they would glow for years.<ref name=history/> The phosphorescent properties of barite heated with organics were described by V. Casciorolus in 1602.<ref name="Ullman2005" />{{rp|5}}

[[Carl Scheele]] determined that barite contained a new element in 1774, but could not isolate barium, only [[barium oxide]]. [[Johan Gottlieb Gahn]] also isolated [[barium oxide]] two years later in similar studies. Oxidized barium was at first called "barote" by [[Guyton de Morveau]], a name that was changed by [[Antoine Lavoisier]] to ''baryta.'' Also in the 18th century, English mineralogist [[William Withering]] noted a heavy mineral in the lead mines of [[Cumberland]], now known to be [[witherite]]. Barium was first isolated by electrolysis of molten barium salts in 1808 by Sir [[Humphry Davy]] in [[England]].<ref>Davy, H. (1808) "[https://books.google.com/books?id=gpwEAAAAYAAJ&pg=102 Electro-chemical researches on the decomposition of the earths; with observations on the metals obtained from the alkaline earths, and on the amalgam procured from ammonia]," ''Philosophical Transactions of the Royal Society of London'', vol. 98, pp. 333–370.</ref> Davy, by analogy with [[calcium]], named "barium" after baryta, with the "-ium" ending signifying a metallic element.<ref name=history>{{cite book| page = 80| url = https://books.google.com/?id=yb9xTj72vNAC| title = The history and use of our earth's chemical elements: a reference guide| author = Krebs, Robert E. | publisher = Greenwood Publishing Group| date = 2006| isbn = 0-313-33438-2}}</ref> [[Robert Bunsen]] and [[Augustus Matthiessen]] obtained pure barium by electrolysis of a molten mixture of [[barium chloride]] and [[ammonium chloride]].<ref>{{cite journal|doi = 10.1002/jlac.18550930301|title = Masthead|date = 1855|journal = Annalen der Chemie und Pharmacie|volume = 93|issue = 3|pages = fmi–fmi}}</ref><ref>{{cite journal|doi =10.1002/prac.18560670194|title =Notizen|date =1856|last1 =Wagner|first1 =Rud|last2 =Neubauer|first2 =C.|last3 =Deville|first3 =H. Sainte-Claire|last4 =Sorel|last5 =Wagenmann|first5 =L.|last6 =Techniker|last7 =Girard|first7 =Aimé|journal =Journal für Praktische Chemie|volume =67|pages =490–508}}</ref>

The production of pure oxygen in the [[Brin process]] was a large-scale application of barium peroxide in the 1880s, before it was replaced by electrolysis and [[fractional distillation]] of liquefied air in the early 1900s. In this process barium oxide reacts at {{convert|500|-|600|C|F}} with air to form barium peroxide, which decomposes above {{convert|700|C}} by releasing oxygen:<ref name=O2>{{cite journal|last1 = Jensen|first1 = William B.|title = The Origin of the Brin Process for the Manufacture of Oxygen|journal = Journal of Chemical Education|volume = 86|pages = 1266|date = 2009|doi = 10.1021/ed086p1266|issue = 11 |bibcode = 2009JChEd..86.1266J}}</ref><ref>{{cite book|url = https://books.google.com/books?id=34KwmkU4LG0C&pg=PA681|page = 681|title = The development of modern chemistry|isbn = 978-0-486-64235-2|author1 = Ihde, Aaron John|date = 1984-04-01}}</ref>
:2 BaO + O<sub>2</sub> ⇌ 2 BaO<sub>2</sub>

Barium sulfate was first applied as a [[radiocontrast]] agent in [[medical imaging|X-ray imaging]] of the digestive system in 1908.<ref>{{cite journal|pmc = 1081520|title = Some Observations on the History of the Use of Barium Salts in Medicine|date = 1974|volume = 18|issue = 1|author=Schott, G. D.|journal=Med. Hist.|pages=9–21|doi = 10.1017/S0025727300019190|pmid = 4618587}}</ref>

==Occurrence and production==
The abundance of barium is 0.0425% in the Earth's crust and 13&nbsp;µg/L in sea water. The primary commercial source of barium is barite (also called barytes or heavy spar), a barium sulfate mineral.<ref name="Ullman2005" />{{rp|5}} with deposits in many parts of the world. Another commercial source, far less important than barite, is witherite, a barium carbonate mineral. The main deposits are located in England, Romania, and the former USSR.<ref name="Ullman2005" />{{rp|5}}

{{multiple image
| footer = Barite, left to right: appearance, graph showing trends in production over time, and the map showing shares of the most important producer countries in 2010.
| align = center
| width1 = 180
| width2 = 246
| width3 = 376
| image1 = Barite.jpg
| alt1 = alt1
| image2 = BariteWorldProductionUSGS.PNG
| alt2 = alt2
| image3 = World Baryte Production 2010.svg
| alt3 = alt3
}}
The barite reserves are estimated between 0.7 and 2 billion [[tonne]]s. The maximum production, 8.3 million tonnes, was produced in 1981, but only 7–8% was used for barium metal or compounds.<ref name="Ullman2005" />{{rp|5}} Barite production has risen since the second half of the 1990s from 5.6 million tonnes in 1996 to 7.6 in 2005 and 7.8 in 2011. China accounts for more than 50% of this output, followed by India (14% in 2011), Morocco (8.3%), US (8.2%), Turkey (2.5%), Iran and Kazakhstan (2.6% each).<ref>Miller, M. M. [http://minerals.usgs.gov/minerals/pubs/commodity/barite/mcs-2012-barit.pdf Barite]. USGS.gov</ref>

The mined ore is washed, crushed, classified, and separated from quartz. If the quartz penetrates too deeply into the ore, or the iron, zinc, or lead content is abnormally high, then [[froth flotation]]<!--https://books.google.com/books?id=zNicdkuulE4C&pg=PA223--> is used. The product is a 98% pure barite (by mass); the purity should be no less than 95%, with a minimal content of iron and [[silicon dioxide]].<ref name="Ullman2005" />{{rp|7}} It is then reduced by carbon to barium sulfide:<ref name="Ullman2005" />{{rp|6}}
:BaSO<sub>4</sub> + 2 C → BaS + 2 CO<sub>2</sub>↑

The water-soluble barium sulfide is the starting point for other compounds: reacting BaS with oxygen produces the sulfate, with nitric acid the nitrate, with carbon dioxide the carbonate, and so on.<ref name="Ullman2005" />{{rp|6}} The nitrate can be thermally decomposed to yield the oxide.<ref name="Ullman2005" />{{rp|6}} Barium metal is produced by reduction with [[aluminium]] at {{convert|1100|C}}. The [[intermetallic compound]] BaAl<sub>4</sub> is produced first:<ref name="Ullman2005" />{{rp|3}}
:3 BaO + 14 Al → 3 BaAl<sub>4</sub> + Al<sub>2</sub>O<sub>3</sub>

BaAl<sub>4</sub> is an intermediate reacted with barium oxide to produce the metal. Note that not all barium is reduced.<ref name="Ullman2005" />{{rp|3}}
:8 BaO + BaAl<sub>4</sub> → Ba↑ + 7 BaAl<sub>2</sub>O<sub>4</sub>

The remaining barium oxide reacts with the formed aluminium oxide:<ref name="Ullman2005" />{{rp|3}}
:BaO + Al<sub>2</sub>O<sub>3</sub> → BaAl<sub>2</sub>O<sub>4</sub>

and the overall reaction is<ref name="Ullman2005" />{{rp|3}}
:4 BaO + 2 Al → 3 Ba↑ + BaAl<sub>2</sub>O<sub>4</sub>

Barium vapor is condensed and packed into molds in an atmosphere of argon.<ref name="Ullman2005" />{{rp|3}} This method is used commercially, yielding ultrapure barium.<ref name="Ullman2005" />{{rp|3}} Commonly sold barium is about 99% pure, with main impurities being strontium and calcium (up to 0.8% and 0.25%) and other contaminants contributing less than 0.1%.<ref name="Ullman2005" />{{rp|4}}

A similar reaction with silicon at {{convert|1200|C}} yields barium and [[barium metasilicate]].<ref name="Ullman2005" />{{rp|3}} Electrolysis is not used because barium readily dissolves in molten halides and the product is rather impure.<ref name="Ullman2005" />{{rp|3}}
[[File:Benitoite HD.jpg|thumb|Benitoite crystals on natrolite. The mineral is named for the [[San Benito River]] in [[San Benito County]] where it was first found.]]

===Gemstone===
The barium mineral, [[benitoite]] (barium titanium silicate), occurs as a very rare blue fluorescent gemstone, and is the official state gem of [[California]].

==Applications==

===Metal and alloys===
Barium, as a metal or when alloyed with aluminium, is used to remove unwanted gases ([[getter]]ing) from vacuum tubes, such as TV picture tubes.<ref name="Ullman2005" />{{rp|4}} Barium is suitable for this purpose because of its low [[vapor pressure]] and reactivity towards oxygen, nitrogen, carbon dioxide, and water; it can even partly remove noble gases by dissolving them in the crystal lattice. This application is gradually disappearing due to the rising popularity of the tubeless LCD and plasma sets.<ref name="Ullman2005" />{{rp|4}}

Other uses of elemental barium are minor and include an additive to [[silumin]] (aluminium–silicon alloys) that refines their structure, as well as<ref name="Ullman2005" />{{rp|4}}
* [[bearing alloy]]s;
* lead–tin [[solder]]ing alloys – to increase the creep resistance;
* alloy with nickel for [[spark plug]]s;
* additive to steel and cast iron as an inoculant;
* alloys with calcium, manganese, silicon, and aluminium as high-grade steel deoxidizers.
<!--
Frary metal lead barium bearing alloy. https://books.google.com/books?id=g-aUf3nM6AEC&pg=PT645-->

===Barium sulfate and barite===
[[File:BariumXray.jpg|thumb|left|[[Amoebiasis]] as seen in radiograph of barium-filled colon]]
[[Barium sulfate]] (the mineral barite, BaSO<sub>4</sub>) is important to the petroleum industry as a [[drilling fluid]] in [[oil well|oil and gas wells]].<ref name="Lide2004" />{{rp|4–5}} The precipitate of the compound (called "blanc fixe", from the French for "permanent white") is used in paints and varnishes; as a filler in ringing ink, plastics, and rubbers; as a paper coating pigment; and in [[nanoparticles]], to improve physical properties of some polymers, such as epoxies.<ref name="Ullman2005" />{{rp|9}}

Barium sulfate has a low toxicity and relatively high density of ca. 4.5 g·cm<sup>−3</sup> (and thus opacity to X-rays). For this reason it is used as a [[radiocontrast]] agent in [[medical imaging|X-ray imaging]] of the digestive system ("[[barium meal]]s" and "[[Lower gastrointestinal series|barium enemas]]").<ref name="Lide2004" />{{rp|4–5}} [[Lithopone]], a [[pigment]] that contains barium sulfate and [[zinc sulfide]], is a permanent white with good covering power that does not darken when exposed to sulfides.<ref>{{cite book| page = 102| url = https://books.google.com/?id=uEJHsZWyO-EC| title= Medicinal applications of coordination chemistry| author = Jones, Chris J.| author2 = Thornback, John| last-author-amp = yes | publisher =Royal Society of Chemistry| date = 2007| isbn =0-85404-596-1}}</ref>

===Other barium compounds===
[[File:2006 Fireworks 1.JPG|thumb|Green barium fireworks]]
Other compounds of barium find only niche applications, limited by the toxicity of Ba<sup>2+</sup> ions (barium carbonate is a [[rat poison]]), which is not a problem for the insoluble BaSO<sub>4</sub>.
* [[Barium oxide]] coating on the [[electrode]]s of [[fluorescent lamp]]s facilitates the release of [[electron]]s.
* By its great atomic density, b[[Barium carbonate|arium carbonate]] increases the [[refractive index]] and luster of glass<ref name="Lide2004" />{{rp|4–5}} and reduces leaks of X-rays from [[cathode ray tubes]] (CRT) TV sets.<ref name="Ullman2005" />{{rp|12–13}}
* Barium, typically as [[barium nitrate]] imparts a yellow or "apple" green color to fireworks;<ref>{{cite book| page =110| url = https://books.google.com/?id=yxRyOf8jFeQC| title = Chemistry of Fireworks| author = Russell, Michael S.| author2 = Svrcula, Kurt| last-author-amp = yes| publisher= Royal Society of Chemistry| date = 2008| isbn = 0-85404-127-3}}</ref> for brilliant green barium monochloride is used.
* [[Barium peroxide]] is a catalyst in the [[aluminothermic reaction]] ([[thermite]]) for welding rail tracks. It is also a green flare in [[tracer ammunition]] and a bleaching agent.<ref>{{cite journal| doi =10.1002/prep.19950200604| title =Surfactant coatings for the stabilization of barium peroxide and lead dioxide in pyrotechnic compositions| date =1995| author =Brent, G. F.| journal =[[Propellants, Explosives, Pyrotechnics]]| volume =20| pages =300| last2 =Harding| first2 =M. D.| issue =6}}</ref>
* [[Barium titanate]] is a promising [[electroceramic]].<ref>{{cite book |title=Introduction to ferroic materials |last=Wadhawan |first=Vinod K. |date=2000 |publisher=CRC Press |isbn=978-90-5699-286-6 |page=740}}</ref>
* [[Barium fluoride]] is used for optics in infrared applications because of its wide transparency range of 0.15–12&nbsp;micrometers.<ref>{{cite web|url=http://www.crystran.co.uk/barium-fluoride-baf2.htm |title=Crystran Ltd. Optical Component Materials|work=crystran.co.uk |accessdate=2010-12-29}}</ref>
* [[Yttrium barium copper oxide|YBCO]] was the first [[High-temperature superconductivity|high-temperature superconductor]] cooled by liquid nitrogen, with a transition temperature of {{convert|93|K|C F}} that exceeded the boiling point of nitrogen ({{convert|77|K|C F|disp=or}}).<ref>{{cite journal|title = Superconductivity at 93 K in a New Mixed-Phase Y-Ba-Cu-O Compound System at Ambient Pressure|journal = Physical Review Letters|date = 1987|volume = 58|pages = 908–910|doi = 10.1103/PhysRevLett.58.908|pmid = 10035069|issue = 9|bibcode=1987PhRvL..58..908W|last1 = Wu|first1 = M.|last2 = Ashburn|first2 = J.|last3 = Torng|first3 = C.|last4 = Hor|first4 = P.|last5 = Meng|first5 = R.|last6 = Gao|first6 = L.|last7 = Huang|first7 = Z.|last8 = Wang|first8 = Y.|last9 = Chu|first9 = C.}}</ref>
* [[Ferrite (magnet)|Ferrite]], a type of [[sintering|sintered]] ceramic composed of Iron Oxide (Fe<sub>2</sub>O<sub>3</sub>) and barium oxide (BaO), is both [[electrical conductivity|electrically nonconductive]] and [[ferrimagnetic]], and can be temporarily or permanently magnetized.
<!-- *The ratio of barium (biogenic barium) to aluminium within marine cores is used as a proxy for surface ocean export production in the past.<ref>{{cite journal|doi = 10.1016/S0025-3227(04)00004-0|title = Biogenic barium and the detrital Ba/Al ratio: a comparison of their direct and indirect determination|year = 2004|last1 = Reitz|first1 = A.|journal = Marine Geology|volume = 204|issue = 3–4|pages = 289–300|last2 = Pfeifer|first2 = K.|last3 = De Lange|first3 = G. J.|last4 = Klump|first4 = J.}}</ref>-->

==Toxicity==
Because of the high reactivity of the metal, toxicological data are available only for compounds.<ref name=bariumtoxic/> Water-soluble barium compounds are poisonous. In low doses, barium ions act as a muscle stimulant, and higher doses affect the [[nervous system]], causing cardiac irregularities, tremors, weakness, [[anxiety (mood)|anxiety]], [[dyspnea|shortness of breath]], and [[paralysis]]. This toxicity may be caused by Ba<sup>2+</sup> blocking [[potassium ion channels]], which are critical to the proper function of the nervous system.<ref>{{cite book |pages = 77–78| isbn = 0-07-049439-8|url = https://books.google.com/?id=Xqj-TTzkvTEC&pg=PA243 |title = Handbook of inorganic chemicals |author = Patnaik, Pradyot |date = 2003}}</ref> Other organs damaged by water-soluble barium compounds (i.e., barium ions) are the eyes, immune system, heart, respiratory system, and skin<ref name=bariumtoxic>{{cite book|title=Barium|url=http://www.espimetals.com/index.php/msds/46-barium|accessdate=2012-06-11|publisher=ESPI Metals}}</ref> causing, for example, blindness and sensitization.<ref name=bariumtoxic/>

Barium is not carcinogenic<ref name=bariumtoxic/> and it does not [[bioaccumulation|bioaccumulate]].<ref>{{cite web |url=http://www.epa.gov/region5/superfund/ecology/html/toxprofiles.htm#ba|archiveurl = https://web.archive.org/web/20100110125521/http://www.epa.gov/region5/superfund/ecology/html/toxprofiles.htm#ba |archivedate = 2010-01-10|accessdate = 2012-06-16|title = Toxicity Profiles, Ecological Risk Assessment|publisher = US EPA}}</ref><ref>{{cite book| author = Moore, J. W.|date =1991| title = Inorganic Contaminants of Surface Waters, Research and Monitoring Priorities| publisher = Springer-Verlag| location= New York}}</ref> Inhaled dust containing insoluble barium compounds can accumulate in the lungs, causing a [[benign]] condition called [[baritosis]].<ref name="pmid1257935">{{cite journal |author=Doig, A.T. |title=Baritosis: a benign pneumoconiosis |journal=Thorax |volume=31 |issue=1 |pages=30–9 |date=1976 |pmid=1257935 |pmc=470358 |doi= 10.1136/thx.31.1.30}}</ref> The insoluble sulfate is nontoxic and is not classified as a dangerous goods in transport regulations.<ref name="Ullman2005" />{{rp|9}}

To avoid a potentially vigorous chemical reaction, barium metal is kept in an argon atmosphere or under mineral oils. Contact with air is dangerous and may cause ignition. Moisture, friction, heat, sparks, flames, shocks, static electricity, and exposure to oxidizers and acids should be avoided. Anything that may contact with barium should be electrically grounded. Anyone who works with the metal should wear pre-cleaned non-sparking shoes, flame-resistant rubber clothes, rubber gloves, apron, goggles, and a gas mask. Smoking in the working area is forbidden. Thorough washing is required after handling barium.<ref name=bariumtoxic/>

==See also==
* [[Han purple and Han blue]] – synthetic barium [[copper]] [[silicate]] pigments developed and used in ancient and imperial [[China]]
{{Subject bar
|portal1=Chemistry
|portal2=Medicine
|book1=Barium
|book2=Period 6 elements
|book3=Alkaline earth metals
|book4=Chemical elements (sorted&nbsp;alphabetically)
|book5=Chemical elements (sorted by number)
|commons=y
|wikt=y
|wikt-search=barium
|v=y
|v-search=Barium atom
|s=y
|s-search=1911 Encyclopædia Britannica/Barium
}}

==References==
{{reflist|30em}}

==External links==
* [http://www.periodicvideos.com/videos/056.htm Barium] at ''[[The Periodic Table of Videos]]'' (University of Nottingham)
* [http://elements.vanderkrogt.net/element.php?sym=Ba Elementymology & Elements Multidict]
* [http://oai.dtic.mil/oai/oai?verb=getRecord&metadataPrefix=html&identifier=ADA338490 3-D Holographic Display Using Strontium Barium Niobate]

{{Barium compounds}}
{{Compact periodic table}}
{{Good article}}

{{Authority control}}

[[Category:Barium| ]]
[[Category:Chemical elements]]
[[Category:Alkaline earth metals]]
[[Category:Toxicology]]
[[Category:Reducing agents]]