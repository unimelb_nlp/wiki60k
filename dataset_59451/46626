'''BookNet Canada''' (BNC)  is an industry-led, non-profit organization that develops technology, standards, and education to serve the Canadian book industry. It is partially funded by the [[Department of Canadian Heritage]] and is accountable to the [[Government of Canada]] for servicing and reporting on the Canadian book industry.<ref>{{cite web|url=http://publishing.sfu.ca/2013/08/the-canadian-book-industry-supply-chain-initiative-the-inception-and-implementation-of-a-new-funding-initiative-for-the-department-of-canadian-heritage|last=Maclean|first=Heather|title=The Canadian Book Industry Supply Chain Initiative: The Inception and Implementation of a New Funding Initiative for the Department of Canadian Heritage|publisher=Publishing @ SFU}}</ref>

== History ==

BookNet Canada was founded in 2002 in response to government and industry initiatives to support book publishing in Canada, with an initial mandate of “implementing technologically focused, universally adopted supply chain standards and solutions (including a centralized sales reporting database).”<ref>{{cite web|url=http://publishing.sfu.ca/2013/08/first-do-no-harm-five-years-of-book-industry-data-sharing-with-booknet-canada-salesdata/#1.1.1|last=Theriault|first=Chelsea|title=“First, Do No Harm”: Five Years of Book-Industry Data Sharing With BookNet Canada SalesData|publisher=Publishing @ SFU|accessdate=3 August 2016}}</ref>

The first impetus to create the organization goes back to the late 1990s with the Standing Committee on Canadian Heritage. The committee met several times in 1999 and 2000 to assess the government’s support of the book industry and the issues facing Canadian book publishing as a whole.<ref>{{cite web|url=http://www.parl.gc.ca/Committees/en/HERI/StudyActivity?studyActivityId=625260|title= Canadian Book Publishing Industry, Standing Committee on Canadian Heritage|publisher=Parliament of Canada|accessdate=3 August 2016}}</ref> Their research culminated in the report, The Challenge of Change: A Consideration of the Canadian Book Industry, which presented several recommendations mainly focused on the need for technological standards and reliable market data.<ref>{{cite web|url=http://www.parl.gc.ca/HousePublications/Publication.aspx?Language=e&Mode=1&Parl=36&Ses=2&DocId=1031737|title=The Challenge of Change: A Consideration of the Canadian Book Industry|publisher=Standing Committee on Canadian Heritage|accessdate=3 August 2016}}</ref>

BookNet Canada was formed out of the vestiges of the Canadian Telebook Agency (CTA), an existing organization that was already managing the closest thing to a national book database at the time, with a Board of Directors formed from representatives from the Canadian Booksellers Association, Association of Canadian Publishers, Canadian Publishers Council, and the Association of Canadian Book Wholesalers.<ref>{{cite web|url=http://publishing.sfu.ca/2013/08/first-do-no-harm-five-years-of-book-industry-data-sharing-with-booknet-canada-salesdata/#1.1.1|last=Theriault|first=Chelsea|title=“First, Do No Harm”: Five Years of Book-Industry Data Sharing With BookNet Canada SalesData|publisher=Publishing @ SFU|accessdate=3 August 2016}}</ref>

With ongoing support from the [[Department of Canadian Heritage]] through the Canada Book Fund and direction from industry representatives, BookNet Canada began working on improvements to the book supply chain, starting with the creation of an electronic communications platform for the exchange of documents between supply chain partners and a national book sales tracking and aggregation service, which was launched in 2005 under the name SalesData.<ref>{{cite web|url=http://publishing.sfu.ca/2013/08/first-do-no-harm-five-years-of-book-industry-data-sharing-with-booknet-canada-salesdata/#1.1.1|last=Theriault|first=Chelsea|title=“First, Do No Harm”: Five Years of Book-Industry Data Sharing With BookNet Canada SalesData|publisher=Publishing @ SFU|accessdate=3 August 2016}}</ref>

== Current services ==

===Print book sales tracking and aggregation===

All print book sales in the Canadian trade market are tracked and aggregated in BNC SalesData, which is accessible to publishers and booksellers. The system collects POS transaction data as well as on-hand and on-order levels from retailers, both physical and online, with approximately 85% coverage of the print trade market in Canada.<ref>{{cite web|last=Renouf|first=Susan|last2=McCraney |first2=Tricia|url=http://www.creativebc.com/database/files/library/Final_Book_Publishing_Sector_Profile___September_2015.pdf|title=Reading the Tea Leaves: A Book Sector Profile for British Columbia|publisher=Association of Book Publishers of British Columbia|accessdate=3 August 2016}}</ref>

The data collected in SalesData is used to report on the size and scope of the trade book market in Canada, most notably in the annual ''The Canadian Book Market'' report. In 2015, the size of the Canadian print book market was reported at 52.6 million units sold for a total value of $983.4 million CAD (US${{To USD | 983.4 | CAN |2016|round=yes }} million), according to BNC sales numbers.<ref>{{cite web|last=Godfrey|first=Laura|url=http://www.publishersweekly.com/pw/by-topic/international/international-book-news/article/69896-canadian-print-book-sales-up-slightly-in-2015.html/|title= Canadian Print Book Sales Up Slightly in 2015|publisher=Publishers Weekly|accessdate=3 August 2016}}</ref>

The aggregated data is also used to create national bestseller lists, including those published by [[The Globe and Mail]]<ref>{{cite web|url=http://www.theglobeandmail.com/arts/books-and-media/bestsellers/|title=Bestseller Lists|publisher=The Globe and Mail|accessdate=3 August 2016}}</ref> and [[Toronto Star]].<ref>{{cite web|url=https://www.thestar.com/entertainment/books.html|title=Bestseller Lists|publisher=Toronto Star|accessdate=3 August 2016}}</ref>

===Bibliographic standards and certification===

The Canadian book industry trades bibliographic data for books according to a set of national and international standards, which are established, contributed to, and upheld by BNC. To accomplish this, the organization sits on several national and international committees on standards for book metadata, identifiers, classification schemes, and electronic data interchange. It works to educate the supply chain on these standards, and oversees Canadian Bibliographic Certification to support adherence in the industry. BNC also monitors and participates in the development of other industry standards, including those for accessibility, EPUB, GDSN, SAN, and GLN.<ref>{{cite web|url=https://books.google.ca/books?id=I7GaCwAAQBAJ&lpg=PA97&ots=Hebz-SBA6c&dq=booknet%20canada%20standards&pg=PR1#v=onepage&q&f=false|last=Register|first=Renée|title=The Metadata Handbook: Second Edition|publisher=Datacurate}}</ref>

===Supply chain services===

BNC operates several tools used by the Canadian book supply chain: CataList, a digital catalogue service used by publishers, booksellers, media, and librarians to host and browse forthcoming books; Pubnet EDI (Electronic Data Interchange), an e-commerce service used by the book industry to send and receive orders, shipment notices, and returns; and BiblioShare, a national database for collecting and disseminating bibliographic data on Canadian books.<ref>{{cite web|url=http://www.booknetcanada.ca/products/|title=Products|publisher=BookNet Canada|accessdate=3 August 2016}}</ref>

=== Research ===

In addition to sales numbers, BookNet conducts industry and consumer research for the book industry, including reports on the prevalence of ebooks vs. print among book buyers.<ref>{{cite web|url=http://www.cbc.ca/news/business/ebooks-print-bookstores-1.3378902|title=E-book sales plateau while print books hold place in readers' hearts|publisher=CBC News|accessdate=3 August 2016}}</ref>

==References==
{{reflist}}

==External links==
*[http://www.booknetcanada.ca/ BookNet Canada website]
{{authority control}}
[[Category:Non-profit organizations based in Canada]]
[[Category:Organizations established in 2002]]
[[Category:2002 establishments in Canada]]