{{EngvarB|date=February 2017}}
{{Use dmy dates|date=February 2017}}
{{Infobox Weapon
|is_missile=yes
|name=A-Darter
|image= [[File:A-darter.jpg|300px]]
|caption= A-Darter missile
|origin= South Africa & Brazil
|type=[[Air-to-air missile]]
|used_by=
|manufacturer=[[Denel Dynamics]]<br />[[Mectron]]
|unit_cost=
|production_date = since 2015<ref name=janedart>{{cite news|title=Denel launches A-Darter SRAAM production|url=http://www.janes.com/article/56021/denel-launches-a-darter-sraam-production|publisher=IHS Jane's 360|date=16 November 2015}}</ref><ref name= "defenceweb1">{{cite news| url = http://www.defenceweb.co.za/index.php?option=com_content&view=article&id=38741:a-darter-contract-moves-missile-programme-forward&catid=35:Aerospace&Itemid=107 |title = A-Darter contract moves missile programme forward |agency=Reuters | publisher =Defence Web |date= 13 April 2015 |accessdate = 16 April 2015}}</ref>
|service=
|engine=Solid fuel rocket
|weight={{convert |89|kg|lb}}
|length={{convert |2.98|m|in}}
|diameter={{convert |0.166|m|in}}
|wingspan={{convert |0.488|m|in}}
|speed=
|vehicle_range= {{convert |22|km|mi}}<ref>{{cite book |editor-last = Jackson |editor-first = Paul | title = All the World's Aircraft 2004–2005 | year = 2004 | page = 810 |publisher=Jane's |location = Surrey, UK | isbn = 0-7106-2614-2}}</ref>
|ceiling=
|altitude=
|filling=
|guidance=dual-colour [[Infrared homing|Infra-red homing]] 
|detonation=laser proximity fuse
|launch_platform=[[Combat aircraft]]
}}

The V3E '''A-Darter''' (Agile Darter) is a modern short-range [[infrared homing]] ("heat seeking") [[air-to-air missile]], featuring countermeasures resistance with a 180-degree<ref name = FAB /> look angle and 120-degrees per second track rate,<ref name = saair>{{cite web | url= http://www.saairforce.co.za/news-and-events/1005/denel-dynamics-completes-a-darter-integration-on-gripen | title = Denel Dynamics completes A-Darter integration on Gripen | publisher = SA air force | date = 7 June 2011 |last = Heitman | first =Helmoed-Römer |accessdate = 22 August 2012}}</ref> developed by South Africa's [[Denel Dynamics]] (formerly Kentron) and Brazil's [[Mectron]], [[Avibras]] and [[Opto Eletrônica]].<ref name = FAB /> It will equip [[South African Air Force]]'s [[Saab JAS 39 Gripen#Second generation|Saab JAS 39 Gripen C/D]] and [[BAE Systems Hawk#Hawk 120/LIFT|BAe Hawk 120]]; [[Brazilian Air Force]]'s [[AMX International AMX| A-1M AMX]], [[Northrop F-5#Brazil| Northrop F-5BR]] and [[Saab JAS 39 Gripen#Third generation|Gripen E/F]].<ref>{{cite web | place = ZA | url = http://www.af.mil.za/news/2005/caf_interview.htm | title = CAF interview | publisher = Air Force |date= 30 October 2007 | accessdate = 22 August 2012}}</ref> It is expected to be in production before the end of 2015.<ref>{{cite news|url= http://www.defenceweb.co.za/index.php?option=com_content&view=article&id=37865:a-darter-heading-up-strong-denel-exhibit-at-idex-2015&catid=7:Industry |title=A-Darter heading up strong Denel exhibit at IDEX 2015 |publisher= Defence Web | date =6 February 2015 |accessdate= 20 August 2015}}</ref>

==Development==
Development of the A-Darter started in 1995,<ref name= engi /> however it suffered from inadequate funding and changes to the SAAF's requirements.<ref>{{cite journal |last=Hewson |first=Rob |title= Briefing: South Africa and partners |journal=Jane's Defence Weekly |issue= 32 |volume=42 |pages=26–27 |publication-date=10 August 2005 |publisher= Jane's Information Group |location=Surrey, UK|issn= 0265-3818}}</ref> Mectron, Avibras and [[Atech]] joined the program in 2006 after a three-year negotiation process<ref name=engi>{{cite web| last= Campbell |first=Keith |url=http://www.engineeringnews.co.za/article/target-date-for-missile-service-entry-revealed-2006-05-19|publisher=Engineering News |title=Target date for missile service entry revealed |date=19 May 2006 |accessdate=18 September 2012}}</ref> with US$52&nbsp;million invested by the Brazilian Government in the project, estimated to be worth US$130&nbsp;million.<ref>{{cite journal|last1= Dorschner |first1= Jim |last2= Resende |first2= Pedro |title=Briefing: South American Air Forces |journal= Jane's Defence Weekly|issue=11 |volume=43 |page=25 |publication-date= 15 March 2006 |publisher=Jane's Information Group |location= Surrey, UK| issn= 0265-3818}}</ref> In that same year, Denel announced that it would use the latest solid-state [[inertial measurement unit]], the SiIMU02 from [[BAE Systems]], for mid course range guidance.<ref>{{cite web| url = http://www.na.baesystems.com/newsreleases/266-inertial_measurement_unit_selected_for_new_air-to-air_missile.pdf| title= BAE Systems Inertial Measurement Unit Selected for New Air-to-Air Missile|accessdate=6 March 2007|date=25 September 2006| publisher= BAE Systems}}</ref> The Brazilian company Opto Eletrônica has partnered with Denel Dynamics in the development of the missile imaging infrared seeker for thermal guidance.<ref name=FAB />

Ground seeker tests were concluded in January 2010,<ref name= flightg>{{cite web | title=South Africa, Brazil ready for A-Darter missile test | publisher=Flight Global | year=2010|url=http://www.flightglobal.com/articles/2010/07/06/344104/south-africa-brazil-ready-for-a-darter-missile-test.html| accessdate= 18 July 2010| archiveurl= https://web.archive.org/web/20100710192631/http://www.flightglobal.com/articles/2010/07/06/344104/south-africa-brazil-ready-for-a-darter-missile-test.html|archivedate= 10 July 2010 | deadurl= no}}</ref> while trajectory guidance and agility flight tests were performed in February 2010.<ref name=defenceweb>{{cite web|url= http://www.defenceweb.co.za/index.php?option=com_content&view=article&id=9005:a-darter-successfully-launched-off-saaf-gripen-&catid=35:Aerospace&Itemid=107 |title=A-Darter successfully launched off SAAF Gripen |publisher=DefenceWeb |date= 21 July 2010 |accessdate= 22 August 2012}}</ref> Prototypes were sent to [[Saab AB]] to begin the integration of the missile to the Saab JAS 39 Gripen. Captive flight trials were concluded in March 2010.<ref name= flightg/><ref name=defenceweb/> The first successful in-flight launch from a Gripen fighter took place on 17 June 2010.<ref name= defeweb />

In March 2012, Denel Dynamics disclosed that the missile, which was to be ready for production by end of 2013, entered the qualification phase. Several testing firings at Denel's [[Overberg Test Range]] were carried out from a Gripen in January 2012. Final testing included the use of high-speed target drones to simulate an aircraft by towing infrared targets at high speed.<ref name= defeweb>{{cite web| url= http://www.defenceweb.co.za/index.php?option=com_content&id=24166 |title=A-Darter January flight tests successful|publisher=DefenceWeb |date=7 March 2012 |accessdate=22 August 2012}}</ref>

In December 2012, the Brazilian air force commissioned Denel to build a factory in [[São José dos Campos]], close to Mectron, Avibras and Opto Eletrônica.<ref name=commi />

In February 2015, Denel Dynamics signed an agreement with Marotta Controls for supply of the latter's MPACT pure air compression technology to cool the A-Darter's infrared seeker.<ref>{{cite news|url=http://www.defenceweb.co.za/index.php?option=com_content&view=article&id=37885:us-company-supplying-cooling-component-on-a-darter&catid=7:Industry&Itemid=116 |title=US company supplying cooling component on A-Darter |publisher=DefenceWeb.co.za|date=9 February 2015}}</ref>

==Production==
In March 2015 SAAF ordered an undisclosed number of missiles from Denel Dynamics.<ref name="defenceweb1" />

==Design==
The missile seeker can be slaved to the Helmet Mounted Display ([[Helmet Mounted Display|HMD]]),<ref>{{cite web|url=  http://www.defenceweb.co.za/index.php?option=com_content&view=article&id=19328:cobra-helmets-for-saaf-gripen-&catid=35:Aerospace&Itemid=107 |title=Cobra helmets for SAAF Gripen|publisher=DefenceWeb |date=22 September 2011 |accessdate=25 September 2012}}</ref> allowing the pilot to track a target beyond the aircraft's radar scan envelope using the missile's high off-boresight capability, achieved by the pilot turning his head towards the target to lock-on,<ref>{{cite book|last=Melzer |first=James |editor-last=Spitzer |editor-first=Cary |title=The Avionics Handbook |chapter=Chapter 5: Head-Mounted Displays |chapterurl=http://www.davi.ws/avionics/TheAvionicsHandbook_Cap_5.pdf |publisher=CRC Press |location=Boca Raton, USA|publication-date=2011 |isbn=0-8493-8348-X}}</ref> better known as "look and shoot". The missile can then be launched and can immediately pull extreme [[g-force]] to reverse its course to engage a target behind the aircraft, sometimes called an "over-the-shoulder".<ref>{{cite web|url=http://www.segurancaedefesa.com/A-Darter.html |title=A-Darter|publisher=Segurancaedefesa.com|date= |accessdate=22 August 2012}}</ref> Engage modes include Lock-On After Launch (LOAL) capability to engage targets outside its seeker's acquisition range, and Lock-On Before Launch (LOBL) capability where the target is identified and designated before launch.<ref name=FAB>{{cite web|author=Cecomsaer |url=http://www.fab.mil.br/portal/capa/index.php?mostra=10161|title=TECNOLOGIA – Diretor-Geral do DCTA acompanha o Projeto A-Darter (míssil) na África do Sul |publisher=FAB |date=|accessdate=22 August 2012}}</ref> The two colour<ref name=denelpdf>{{cite web|url=http://www.deneldynamics.co.za/brochures/Broc0264_A-Darter%20External.pdf |title=A-DARTER: Fifth-generation Air-to-air Missile System |publisher=Denel Dynamics |date= |accessdate=22 August 2012}}</ref> thermal imaging technology and a laser proximity fuse fitted on the missile provide multiple [[Electronic counter-countermeasures]] (ECCM) techniques with targeting algorithms including advanced spatial filtering techniques and velocity profiling.<ref>{{cite web|url=http://www.saairforce.co.za/the-airforce/weapons/31/v3e-a-darter |title=V3E A-Darter |publisher=saairforce.co.za |date=|accessdate=22 August 2012}}</ref>

The A-Darter has four major sections:  guidance section, warhead, control, and rocket motor.<ref name=denelpdf/> It uses a streamlined design with low aerodynamic drag in a wingless airframe, ensuring ranges beyond those of traditional short-range missiles.<ref>{{cite web|url=http://www.defenseindustrydaily.com/south-africa-brazil-to-develop-adarter-sraam-03286/ |title=South Africa, Brazil to Develop A-Darter SRAAM |publisher=Defense Industry Daily |date=21 September 2010 |accessdate=22 August 2012}}</ref> It is fitted with a [[thrust vectoring]] control (TVC) system<ref name=mectron>{{cite web|url=http://www.mectron.com.br/armamentos-inteligentes.asp|title=Armamentos Inteligentes |publisher=Mectron |date= |accessdate=22 August 2012}}</ref> to provide agility up to 100 times the force of gravity (100G).<ref name=commi>{{cite web|url=http://www.flightglobal.com/news/articles/brazilian-air-force-commissions-factory-for-a-darter-missile-confirms-specs-380258/ |title=Brazilian air force commissions factory for A-Darter missile, confirms specs |first=Stephen |last=Trimble |publisher=Flight International |date=14 December 2012 |accessdate=31 December 2012}}</ref>

The absence of aluminium powder from the motor propellent inhibits production of a smoke trail, which means no visual warning for enemy aircraft.<ref name=saair /> According to SAAF fighter pilots involved within the project, the A-Darter it is better than the [[IRIS-T]] in some respects.<ref name=saair />

==See also==
;Similar missiles
* [[AIM-9#AIM-9X|AIM-9X]]
* [[ASRAAM]]
* [[IRIS-T]]
* [[Python 5]]
* [[Vympel R-73]]
* [[AAM-5 (Japanese missile)]]

==References==
{{Reflist|40em}}

== External links ==
* {{Official website|http://www.deneldynamics.co.za/|Denel Dynamics – official website}}
* [http://www.deneldynamics.co.za/products/missiles/air-to-air-missiles/a-darter Manufacturer's official brochure, A-Darter]
* [http://www.mectron.com.br/armamentos-inteligentes.asp Mectron] (Brazilian partner's website) {{pt icon}}

{{Denel}}

{{DEFAULTSORT:A-Darter (Missile)}}
[[Category:Post–Cold War weapons of South Africa]]
[[Category:Air-to-air missiles of Brazil]]
[[Category:Air-to-air missiles of South Africa]]
[[Category:Denel]]