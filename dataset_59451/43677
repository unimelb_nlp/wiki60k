{{Use dmy dates|date=December 2016}}
{{Use British English|date=December 2016}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
|name           =Sopwith Three-seater
|image          =
|caption        =
}}{{Infobox Aircraft Type
|type           =General purpose aircraft
|national origin=United Kingdom
|manufacturer   =[[Sopwith Aviation Company]]
|designer       =[[Thomas Sopwith]]
|first flight   =4 July 1912
|introduced     =1912
|retired        =1915
|primary user   =[[Royal Flying Corps]]
|more users     =[[Royal Naval Air Service]]
|number built   =13
}}
|}
The '''Sopwith Three-seater''' was a British aircraft designed and built prior to the start of the [[World War I|First World War]]. One of the first aircraft built by the  [[Sopwith Aviation Company]], it was operated by both the [[Royal Naval Air Service]] (RNAS) and the [[Royal Flying Corps]] (RFC), being used briefly over Belgium by the RNAS following the start of the War.

==Development and design==
In 1912, [[Thomas Sopwith]], who had learned to fly during 1910 and had set up a flying school at [[Brooklands]], built a [[tractor configuration]] [[biplane]] using the wings from a [[Wright Model B|Wright Biplane]] and the fuselage and tail of a [[COW Biplane|Coventry Ordnance Works biplane]].<ref name="Mason p78">Mason 1982, p. 78</ref> and powered by a 70&nbsp;hp (52&nbsp;kW) [[Gnome Gamma]] [[rotary engine]].<ref name="Bruce RFC p491,628">Bruce 1982, pp. 491, 628</ref> The resulting aircraft, known as the '''Hybrid''', first flew on 4 July 1912.<ref name="Robertson p31">Robertson 1970, p. 31</ref>
The Hybrid was rebuilt in October 1912 and sold to the British [[Admiralty]], being delivered in November 1912.<ref name="Mason p78"/><ref name="Robertson p32">Robertson 1970, p. 32</ref> When the Admiralty tendered further orders for an improved tractor biplane based on the design,Sopwith created the [[Sopwith Aviation Company]], with a factory in a disused [[roller rink]] at [[Kingston upon Thames]]. The resulting aircraft, known variously as the '''Three-Seat Tractor Biplane''',<ref name="Robertson p210">Robertson 1970, p. 210</ref> the '''Sopwith 80&nbsp;hp Biplane''',<ref name="Bruce RFC p491"/> the '''Sopwith D1''',<ref name="Davis p12-3">Davis 1999, pp. 12–13</ref> or the '''Sopwith Tractor Biplane''',<ref>''Roots In The Sky - A History of British Aerospace Aircraft'', Oliver Tapper (1980), ISBN 061700323 8; p. 18. "In June 1913 [Sopwith] reached 12,000 feet with one passenger and later, in July, he flew the same machine, this time with three passengers, to the world-record height of 8,400 feet."</ref> was flown on 7 February 1913 before being displayed at the International Aero Show at [[Olympia, London]] opening on 14 February.<ref name="Mason p78"/> It had [[Interplane strut|two-bay]] wings, with lateral control by [[wing warping]], and was powered by an 80&nbsp;hp (60&nbsp;kW) [[Gnome Lambda]] rotary engine. It had two cockpits, the pilot sitting aft one and passengers sitting side by side in the forward one. Three transparent [[celluloid]] windows were placed in each side of the fuselage to give a good downwards view.<ref name="Bruce RFC p491">Bruce 1982, p. 491</ref><ref name="Bruce British p517">Bruce 1957, p. 517</ref> 
A second aircraft was retained by Sopwith as a demonstrator, being used to set a number of British altitude records between June and July 1913. A further two tractor biplanes were built for the RNAS, being delivered in August and September 1913, with the original hybrid being rebuilt to a similar standard. Following tests of a Tractor Biplane fitted with [[aileron]]s instead of wing warping for lateral control,<ref name="Bruce RFC p491"/><ref name="robertson p35">Robertson 1970, p.35</ref> a further nine aircraft were ordered for the [[Royal Flying Corps]] (RFC) in September 1913.<ref name="Mason p78-9">Mason 1982, pp. 78—79</ref>

==Operational history==
The RFC received its Three-seaters between November 1913 and March 1914, with the first example being tested to destruction in the [[Royal Aircraft Factory]] at [[Farnborough, Hampshire|Farnborough]], where it was determined that the structural strength was inadequate. Despite this, the remaining eight aircraft were issued to [[No. 5 Squadron RAF|No. 5 Squadron]] as two-seaters. Two were destroyed in a fatal mid-air collision on 12 May 1914, while several more were wrecked in accidents before the outbreak of the First World War. 5 Squadron left its remaining Tractor Biplanes in England when it deployed to France in August 1914, these being briefly used as trainers at the [[Central Flying School]].<ref name="Mason p79">Mason 1982, p. 79</ref><ref name="Bruce RFC p492-3">Bruce 1982, pp.  492—493</ref>
The RNAS aircraft were issued to seaplane stations to allow flying to continue when sea conditions were unsuitable for seaplane operation. On the outbreak of war, the RNAS also acquired Sopwith's demonstrator aircraft. Three Sopwith Tractor biplanes went with the Eastchurch wing of the RNAS when it deployed to Belgium under the command of [[Wing Commander (rank)|Wing Commander]] [[Charles Rumney Samson]]. These were used for reconnaissance and bombing missions, attempting to bomb [[Zeppelin]] sheds at [[Düsseldorf]] on 23 September and railway lines on 24 September, being withdrawn in October.<ref name="Robertson p48-51">Robertson 1970, pp. 48—51</ref> The RNAS also used Sopwith Tractor Biplanes for patrol duties from [[Great Yarmouth]],<ref name="Thetford p424">Thetford 1978, p. 424</ref> one remaining in use until November 1915.<ref name="Mason p79"/>

==Operators==
;{{UK}}
*[[Royal Flying Corps]]<ref name="Bruce RFC p493">Bruce 1982, p. 493</ref>
**[[No. 5 Squadron RAF|No. 5 Squadron RFC]]
**[[Central Flying School]]
*[[Royal Naval Air Service]]

==Specifications (80 hp Gnome)==
{{aerospecs
|ref=Sopwith-The Man and his Aircraft <ref name="Robertson p234-41">Robertson 1970, pp. 234—235, 238—239</ref>
|met or eng?=eng
|crew=1
|capacity=3
|length m=8.99
|length ft=29
|length in=6
|span m=12.19
|span ft=40
|span in=0
|height m=
|height ft=
|height in=
|wing area sqm=36.9
|wing area sqft=397
|wing profile=
|empty weight kg=482
|empty weight lb=1,060
|gross weight kg=823
|gross weight lb=1,810
|eng1 number= 1
|eng1 type=[[Gnome et Rhône|Gnome]] [[rotary engine]]
|eng1 kw=60
|eng1 hp=80
|max speed kmh=118
|max speed mph=73.6
|range km=
|range miles=
|endurance h=2½<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=12,900<ref name="Bruce brit p518">Bruce 1957, p. 518</ref>
|climb rate ms=5.1
|climb rate ftmin=400<ref>Climb to 1000 ft (305 m): 2½ min</ref>
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=*[[List of aircraft of the Royal Naval Air Service]]
*[[List of aircraft of the Royal Flying Corps]]<!-- related lists -->
}}

==Notes==
{{reflist|2}}

==References==
{{refbegin}}
* Bruce, J.M. ''British Aeroplanes 1914-18''. London:Putnam, 1957
* Bruce, J.M. ''The Aeroplanes of the Royal Flying Corps (Military Wing)''. London:Putnam, 1982. ISBN 0-370-30084-X
* Davis, Mick. ''Sopwith Aircraft''. Ramsbury, UK: The Crowood Press, 1999. ISBN 1-86126-217-5
* Mason, Tim. "Tom Sopwith...and his Aeroplanes 1912-14". ''[[Air Enthusiast]]'', Number Twenty, December 1982-March 1983. Bromley, UK:Pilot Press. {{ISSN|0143-5450}}. pp.&nbsp;74–80
* Robertson, Bruce. ''Sopwith-The Man and his Aircraft''. Letchworth, UK:Air Review, 1970. ISBN 0-900435-15-1
* Thetford, Owen. ''British Naval Aircraft since 1912''. London:Putnam, Fourth edition 1978. ISBN 0-370-30021-1
{{refend}}

==External links==
{{commons category|Sopwith Aviation Company}}
* [http://www.flightglobal.com/pdfarchive/view/1913/1913%20-%200185.html "The 80-h.p. Sopwith Tractor Biplane"]   [[Flight International|''Flight'']] 15 February 1913

{{Sopwith Aviation Company aircraft}}
{{wwi-air}}

[[Category:British military reconnaissance aircraft 1910–1919]]
[[Category:Sopwith aircraft|Tractor Biplane]]