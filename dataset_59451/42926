{{Use dmy dates|date=June 2015}}
{{Use British English|date=June 2015}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=H.P.54 Harrow
 |image=HP Harrow ExCC.jpg
 |caption=Harrow of [[No. 115 Squadron RAF]] 
}}{{Infobox Aircraft Type
 |type=Heavy bomber
 |manufacturer=[[Handley Page]]
 |designer=[[Gustav Lachmann|G.V. Lachmann]]
 |first flight=10 October 1936
 |introduced=1937
 |retired=1945
 |status=
 |primary user=[[RAF]]
 |more users=[[Fleet Air Arm]]
 |produced=1936–1937
 |number built=100
 |unit cost=
 |variants with their own articles=
}}
|}

The '''Handley Page H.P.54 Harrow''' was a [[United Kingdom|British]] [[heavy bomber]] of the 1930s built by [[Handley Page]] and used by the [[Royal Air Force]], being used for most of the [[World War II|Second World War]] as a transport. It was a twin-engine, high-wing [[monoplane]] with a fixed [[Landing gear|undercarriage]].

==Development==
The H.P. 54 Harrow was the production version of the earlier [[Handley Page H.P.51]] design, itself a monoplane conversion of the three-engined [[Handley Page H.P.43]] biplane.<ref name="Barnes1">Barnes 1987, pp. 347–351.</ref> The two monoplanes were both designed by Dr. [[Gustav Lachmann|G.V. Lachmann]]. Initially Handley Page intended to offer the H.P.51 to [[List of Air Ministry Specifications#1920-1929|Air Ministry specification C.26/31]] for a bomber-transport, then saw the H.P.54 as a more likely winner. In the end neither type was a candidate for C.26/31, since in June 1935 the [[Air Ministry]], anxious to expand and modernise the [[RAF]] wrote specification B.29/35 around the Harrow, emphasising its bomber role though retaining its transport capability. On 14 August, months before the first Harrow flew, the Ministry put in an order for 100 aircraft.<ref name="Barnes2">Barnes 1987, p. 372.</ref> Powered by [[Bristol Pegasus]] X engines of 830&nbsp;hp (620&nbsp;kW), the first Harrow flew on 10 October 1936 from [[Radlett]].<ref name="lewis bomber"/> The Harrow was designed to have powered nose and tail turrets, with a manually operated dorsal turret. The nose and dorsal turrets were armed with a single [[Lewis gun]], while the tail turret carried two Lewis guns. (These guns were later replaced by [[Vickers K machine gun]]s). A bombload of up to {{convert|3000|lb|kg}} could be carried under the cabin floor, with the aircraft being able to carry a single {{convert|2000|lb|kg}} bomb.<ref name="am186 p4-7">Lumsden and Heffernan ''Aeroplane Monthly'' January 1986, pp. 4–7.</ref>

==Operational history==
The first Harrow was delivered to [[No. 214 Squadron RAF]] on 13 January 1937, with all 100 delivered by the end of the year, with five bomber squadrons of the RAF being equipped with the Harrow.<ref name="lewis bomber"/> The [[Fleet Air Arm]] ordered 100 Harrows but Handley Page lacked the production capacity to supply them.<ref name="lewis bomber"/> Despite being fitted with cabin heating by steam boilers using exhaust heat, the Harrow gained a reputation of being a cold and draughty aircraft, owing to the turret design.<ref name ="mason bomber">Mason 1994, pp. 301–302.</ref> As the delivery of more modern bombers increased, the Harrow was phased out as a frontline bomber by the end of 1939 but continued to be used as a transport. [[No. 271 Squadron RAF|271 Squadron]] was formed on 1 May 1940 with a mixture of Harrows, [[Bristol Bombay]]s and impressed civil aircraft.<ref name= "thetford raf">Thetford 1957 pp.248–9</ref> While the other aircraft equipping 271 Squadron were replaced by [[C-47 Skytrain|Douglas Dakotas]], it retained a flight of Harrows (sometimes nicknamed "Sparrows" due to their new nose fairings to give a more streamlined fuselage) as transports and ambulance aircraft until the end of the Second World War in Europe.<ref name="mason bomber"/><ref name="thetford raf2"/>

Harrows were used occasionally to operate risky flights between England and [[Gibraltar]], two being lost on this route.<ref name="mason bomber"/> Harrows also operated in support of Allied forces, in their advance into north-west Europe, evacuating wounded from the [[Operation Market Garden|Arnhem operation]] in September 1944.<ref name="thetford raf"/><ref name="mondey">Mondey 1994, pp. 125–126.</ref> Seven Harrows were destroyed by a low level attack by ''[[Luftwaffe]]'' fighters of [[JG 26]] and [[JG 54]] on [[Haren Airport|Evere]] airfield as part of [[Unternehmen Bodenplatte|Operation ''Bodenplatte'']], the German attack on Allied airfields in northwest Europe on 1 January 1945, leaving only five Harrows, which were eventually retired on 25 May 1945.<ref name="mason bomber"/><ref name="mason bomber"/><ref name="mondey"/>

The Harrow also served in a novel operational role at the height of [[The Blitz|the German night Blitz]] against Britain in the winter of 1940–1941. Six Harrows equipped [[No. 420 Flight RAF]] (later [[No. 93 Squadron RAF]]) which used lone Harrows to tow Long Aerial Mines (LAM) into the path of enemy bombers.<ref name="mondey"/> The LAM had an explosive charge on the end of a long cable and the unorthodox tactic was credited with the destruction of six German Bombers or 4–5, depending on the source.<ref name="mason bomber"/><ref name="mondey"/> The experiment was judged of poor value and the planned deployment of [[Douglas Havoc]]s in the LAM role was cancelled. Nine Harrows were also used by [[782 Naval Air Squadron]], Fleet Air Arm as transports.<ref name="faa archive">{{cite web |url= http://www.fleetairarmarchive.net/Aircraft/Harrow.html|title= Fleet Air Arm Archive, Handley Page Harrow|accessdate=2007-03-29 |work= }}</ref> After flight refuelling trials, three Harrows were operated by [[Flight Refuelling Ltd|Flight Refuelling Limited]] and refuelled [[Short Empire]] [[Flying boat|Flying Boat]]s on transatlantic services, two from [[Gander, Newfoundland and Labrador|Gander, Newfoundland]] and one based in [[Foynes|Foynes, Ireland]]. In 1940, the two aircraft based at Gander were impressed into service with the [[Royal Canadian Air Force]].

==Variants==
;Harrow Mk.I
:Powered by two 830&nbsp;hp (620&nbsp;kW) [[Bristol Pegasus]] X engines, 19 built.
;Harrow Mk.II
:Powered by two 925&nbsp;hp (690&nbsp;kW) Pegasus XX engines, 81 built.

==Operators==
[[File:Handley Page Harrow - Feltwell - The Royal Air Force in the 1930s HU67364.jpg|thumb|right|Harrows of 214 Squadron at RAF Feltwell in 1938.]]
;{{flag|Canada|1921}}
*[[Royal Canadian Air Force]]
;{{UK}}
*[[Royal Air Force]]
**[[No. 37 Squadron RAF]] – 1937–1939 at RAF Feltwell<ref name="jefford37"/>
**[[No. 75 Squadron RAF]] – 1937–1939 at RAF RAF Driffield and later RAF Honnington<ref>Jefford 1988, p.48</ref>
**[[No. 93 Squadron RAF]] – 1940–1941 at RAF Middle Wallop (aerial mine role)<ref name="jefford37">Jefford 1988, p.37</ref>
**[[No. 115 Squadron RAF]] – 1937–1939 at RAF Marham<ref>Jefford 1988, p.52</ref>
**[[No. 214 Squadron RAF]] – 1937–1939 at RAF Scampton later RAF Feltwell<ref name="jefford71">Jefford 1988, p.71</ref>
**[[No. 215 Squadron RAF]] – 1937–1939 at RAF Driffield and later RAF Honnington<ref name="jefford71" />
**[[No. 271 Squadron RAF]] – 1940–1945 at RAF Doncaster later RAF Ampney Down (transport role)<ref name="jefford82">Jefford 1988, p.82</ref>
**[[No. 420 Flight RAF]] – became 93 Squadron<ref name="jefford37" />
**[[No. 1680 Flight RAF]] – became 271 Squadron<ref name="jefford82" />
*[[Fleet Air Arm]]
**[[782 Naval Air Squadron]]
*[[Flight Refuelling Ltd|Flight Refuelling Limited]]

==Specifications (Harrow II)==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=<!-- options: plane/copter -->plane
|jet or prop?=<!-- options: jet/prop/both/neither -->prop
|ref=P Lewis, The British Bomber since 1914<ref name="lewis bomber">Lewis 1980, pp. 270–271.</ref>
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For instructions on using |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specifications]]. To add a new line, end the old one with a right parenthesis ")", and start a new fully formatted line beginning with * -->
|crew=five
|capacity= 20 fully equipped soldiers<ref name="thetford raf2">Thetford 1957, p.499</ref> or 12 stretcher cases (used as transport) 
|length main= 82 ft 2 in
|length alt= 25.05 m
|span main= 88 ft 5 in
|span alt= 26.96 m
|height main= 19 ft 5 in
|height alt= 5.92 m
|area main= 1,090 ft²
|area alt= 101.3 m²
|airfoil=
|empty weight main= 13,600 lb
|empty weight alt= 6,180 kg
|loaded weight main= 23,000 lb
|loaded weight alt= 10,500 kg
|useful load main=  
|useful load alt=  
|max takeoff weight main=  
|max takeoff weight alt=  
|more general=
|engine (prop)=[[Bristol Pegasus]] XX
|type of prop=nine-cylinder [[radial engine]]
|number of props=2
|power main= 925 hp
|power alt= 690 kW
|power original=
|max speed main= 174 kn
|max speed alt= 200 mph, 322 km/h
|cruise speed main= 142 kn <ref name="thetford raf"/>
|cruise speed alt= 163 mph, 262 km/h
|never exceed speed main=  
|never exceed speed alt=  
|stall speed main=  
|stall speed alt=      
|range main= 1,096 nmi
|range alt= 1,260 mi, 2,029 km
|ceiling main= 22,800 ft
|ceiling alt= 6,950 m
|climb rate main= 710 ft/min
|climb rate alt= 3.6 m/s
|loading main= 21.1 lb/ft²
|loading alt= 103 kg/m²
|thrust/weight=<!-- a unitless ratio -->
|power/mass main= 0.0804 hp/lb
|power/mass alt= 0.132 kW/kg
|more performance=
|guns= 4 × 0.303 in (7.7 mm) [[Lewis Gun]]s
|bombs= Up to 3,000 lb (1,400 kg) of bombs internally.
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
*[[Armstrong Whitworth Whitley]]
*[[Bristol Bombay]]
*[[Dornier Do 23]]
*[[Junkers Ju 52]]
|lists=<!-- related lists -->
* [[List of aircraft of World War II]]
*[[List of aircraft of the RAF]]
|see also=<!-- other relevant information -->
*[[Douglas DB-7#Havoc Mk I|Havoc Mk I (Pandora) intruder]] – also used for Long Aerial Mine (LAM) operations
}}

==References==

===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
* Barnes, C.H. ''Handley Page Aircraft since 1907''. London: Putnam Publishing, 1987. ISBN 0-85177-803-8.
* Clayton, Donald C. ''Handley Page, an Aircraft Album''. Shepperton, Surrey, UK: Ian Allan Ltd., 1969. ISBN 0-7110-0094-8.
* Jefford, C G. ''RAF Squadrons'', first edition 1988, Airlife Publishing, UK, ISBN 1 85310 053 6.
* Lewis, Peter. ''The British Bomber since 1914''. London: Putnam Aeronautical Books, 1980. ISBN 0-370-30265-6.
* Lumsden, Alec and Terry Heffernan. "Probe Probare No. 20: Handley Page Harrow". ''[[Aeroplane Monthly]]'', Vol. 14, No. 1, January 1986. pp. 4–7. {{ISSN|0143-7240}}.
* Mason, Francis K. ''The British Bomber since 1914''. London: Putnam Aeronautical Books, 1994. ISBN 0-85177-861-5.
* Mondey, David. ''The Hamlyn Concise Guide to British Aircraft of World War II''. London: Aerospace Publishing, 1994. ISBN 1-85152-668-4.
* Thetford, Owen. ''Aircraft of the Royal Air Force, 1918–57''. London: Putnam Aeronautical Books, 1957. OCLC 3875235
{{refend}}

==External links==
{{Commons category|Handley Page H.P.54 Harrow}}
* [http://www.raf.mod.uk/bombercommand/bc_aircraft2.html Bomber Command Aircraft]

{{Handley Page aircraft}}

[[Category:British bomber aircraft 1930–1939]]
[[Category:Handley Page aircraft|H.P.054 Harrow]]
[[Category:Twin-engined tractor aircraft]]