{{Infobox journal
| title = Journal of Biological Chemistry
| cover = [[File:Jbc cover.gif]]
| editor = [[Lila Gierasch]], [[F. Peter Guengerich]], [[Herbert Tabor]]
| discipline = [[Biochemistry]], [[Molecular biology|Molecular Biology]]
| abbreviation = J. Biol. Chem.
| publisher = [[American Society for Biochemistry and Molecular Biology]]
| country = United States
| frequency = Weekly
| history = 1905–present
| openaccess = After 12 months
| license =
| impact = 4.573
| impact-year = 2014
| website = http://www.jbc.org/
| link1 = http://www.jbc.org/content/current
| link1-name = Online access
| link2 = http://www.jbc.org/content/by/year
| link2-name = Online archive
| JSTOR =
| OCLC = 54114375
| LCCN = 06046735
| CODEN = JBCHA3
| ISSN = 0021-9258
| eISSN = 1083-351X
}}
The '''''Journal of Biological Chemistry''''' is a weekly [[Peer review|peer-reviewed]] [[scientific journal]] that was established in 1905. Since 1925, it is published by the [[American Society for Biochemistry and Molecular Biology]]. It covers research in areas of [[biochemistry]] and [[molecular biology]]. The [[editors-in-chief|editor-in-chief]] is Lila Gierasch.<ref>{{Cite web|url=http://www.jbc.org/site/misc/eic.xhtml|title=The Journal of Biological Chemistry names a new editor in chief|website=www.jbc.org|access-date=2016-08-04}}</ref> All its articles are available free after one year of publication. In press articles are available free on its website immediately after acceptance.

== History ==
The journal was established in 1905 by [[John Jacob Abel]] and [[Christian Archibald Herter (physician)|Christian Archibald Herter]], who also served as the first editors; the first issue appeared in October 1905.<ref>{{cite journal | title = The First Years of the Journal of Biological Chemistry | last = Fruton | first = Joseph S. | journal = [[The Journal of Biological Chemistry]] | volume = 277 | issue = 23 |date=June 7, 2002 | pages = 20113–20116 | doi = 10.1074/jbc.R200004200 | url = http://www.jbc.org/content/277/23/20113.full | pmid=11960998}}</ref> The location of the journal's editorial offices has included [[Cornell Medical College]] (until 1937), [[Yale University]] (1937–1958), [[Harvard University]] (1958–1967), and [[New York City]] (from 1967).<ref>{{cite journal | title = The Journal of Biological Chemistry After Seventy-Five Years | last = Edsall | first = John T. | journal = [[The Journal of Biological Chemistry]] | volume = 255 | issue = 19 | pages = 8939–8951 |date=October 10, 1980 | url = http://www.jbc.org/content/255/19/8939.full}}</ref> The journal is currently published by the [[American Society for Biochemistry and Molecular Biology]].

== Editors ==
The following individuals have served as editors-in-chief:
*1906–1909: [[John Jacob Abel]] and [[Christian Archibald Herter (physician)|Christian Archibald Herter]]
*1909–1910: [[Christian Archibald Herter (physician)|Christian Archibald Herter]]
*1910–1914: [[Alfred Newton Richards]]
*1914–1925: [[Donald Van Slyke|Donald D. Van Slyke]]
*1925–1936: [[Stanley Rossiter Benedict|Stanley R. Benedict]]. After Benedict died, [[John Tileston Edsall|John T. Edsall]] served as temporary editor until the next editor was appointed.
*1937–1958: [[Rudolph J. Anderson]]
*1958–1967: [[John Tileston Edsall|John T. Edsall]]
*1968–1971: [[William Howard Stein]]
*1971–2011: [[Herbert Tabor]]
*2011–2015: [[Martha Fedor]]
*2016–present: Lila Gierasch

== Ranking and criticism of impact factor ==
The editors of the ''Journal of Biological Chemistry'' have criticized the modern reliance upon the [[impact factor]] for ranking journals, noting that [[review article]]s, commentaries, and [[retraction]]s are included in the calculation. Further, the denominator of total articles published encourages journals to be overly selective in what they publish, and preferentially publish articles which will receive more attention and citations.<ref>{{cite web | title = JBC on Journal Ranking | last = Hascall | first = Vincent C. | last2 = Hanson | first2 = Richard W. |date=August 20, 2007 | url = http://www.jbc.org/site/misc/journalranking.xhtml | quote = Notably, The Annual Review of Immunology had the highest Impact Factor score in 2005 with The Annual Review of Biochemistry rating second. This raises the question of whether citations in reviews should, in fact, be included in the data base used to calculate Impact Factors.... High Impact Factor journals, such as Science and Nature, publish letters, commentaries, and even retractions, all of which have citations that are included in the numerator without inclusion of their number in the denominator of the Impact Factor.}}</ref>

Due to these factors, the journal's practice of publishing  a broad cross-section of biochemistry articles has led it to suffer in impact factor, in 2006 ranking 260 of 6,164, while remaining a highly cited journal.<ref>{{cite web | title = JBC on Journal Ranking | last = Hascall | first = Vincent C. | last2 = Hanson | first2 = Richard W. |date=August 20, 2007 | url = http://www.jbc.org/site/misc/journalranking.xhtml | quote = As a result of this policy, the Journal has grown over the past 20 years in parallel with the growth of research in the biological sciences, to the point that today it is the world's largest and most cited journal. This is not, however, necessarily a good thing for the presumed status of the Journal; it may be highly cited, but in 2006 it ranked only 260 among the 6,164 scientific journals evaluated by Impact Factor metrics.}}</ref> When science journals were evaluated with a [[PageRank]]-based algorithm, however, the ''Journal of Biological Chemistry'' ranked first.<ref>{{cite journal | title = Impact Factor Page Rankled |date=July 27, 2007 | journal = [[ASBMB Today]] | pages = 16–19 | last = Hascall | first = Vincent C. | last2= Bollen | first2 = Johan | last3 = Hanson | first3 = Richard W. | url = http://www.jbc.org/site/misc/Journal-Ranking-ASBMBToday.pdf}}</ref>

Using the [[Eigenfactor]] metric, the ''Journal of Biological Chemistry'' ranked 5th among all ISI-indexed journals in 2010.<ref>{{cite web | title = Eigenfactor journal rankings for 2010 | date=August 26, 2012 | url = http://eigenfactor.org/rankings.php?bsearch=2010&searchby=year&orderby=eigenfactor}}</ref>

The 2014 impact factor of the journal is 4.573.<ref name=WoS>{{cite book |year=2014 |chapter=Journal of Biological Chemistry |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate= |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{reflist|30em}}

==External links==
* {{Official website|http://www.jbc.org/}}

{{Portal bar|Chemistry|Molecular and cellular biology}}

[[Category:Delayed open access journals]]
[[Category:Publications established in 1905]]
[[Category:Biochemistry journals]]
[[Category:Weekly journals]]
[[Category:English-language journals]]
[[Category:Academic journals published by learned and professional societies]]