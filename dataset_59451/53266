{{Infobox NCAA Basketball Conference Tournament |
| Year=2014
| Conference     = ACC
| Division=
| Gender         = Women's
| Image          = 2014 ACC Women's Basketball Tournament logo.png
| ImageSize      = 200px
| Caption        = 2014 ACC Women's Basketball Tournament in Greensboro, NC
| Teams=15
| Arena          = [[Greensboro Coliseum Complex|Greensboro Coliseum]]
| City           = [[Greensboro, North Carolina]]
| 1stRoundArena=
| 1stRoundCity=
| QuarterArena=
| QuarterCity=
| SemiArena=
| SemiCity=
| FinalArena=
| FinalCity=
| Champions=[[2013–14 Notre Dame Fighting Irish women's basketball team|Notre Dame]]
| TitleCount=1st
| Coach=[[Muffet McGraw]]
| CoachCount=1st
| MVP=[[Jewell Loyd]]
| MVPTeam=Notre Dame
| Attendance=
| OneTopScorer=
| TwoTopScorers=
| TopScorer=
| TopScorerTeam=
| TopScorer2=
| TopScorer2Team=
| Points=
| Television      = [[College Basketball on ESPN|ESPN]], [[ESPNU College Basketball|ESPNU]], [[Regional sports network|RSN]] 
}}
{{2013–14 ACC women's basketball standings}}
The '''2014 Atlantic Coast Conference Women's Basketball Tournament''' was the postseason women's basketball tournament for the [[Atlantic Coast Conference]], held from March 5–9, 2014, in [[Greensboro, North Carolina]], at the [[Greensboro Coliseum Complex|Greensboro Coliseum]]. This was the first ACC Tournament to include 15 teams, a result of the conference [[2010–13 Big East Conference realignment|adding]] Syracuse, Pitt, and Notre Dame, and also the last to involve Maryland, which [[2010–13 Big Ten Conference realignment#Maryland|will leave the ACC]] in July 2014 to join the [[Big Ten Conference]].

==Seeds==
{| class="wikitable" style="white-space:nowrap; font-size:90%;"
|-
| colspan="10" style="text-align:center; background:#DDDDDD; font:#000000" | '''2014 ACC Women's Basketball Tournament seeds and results'''
|- bgcolor="#efefef"
! Seed
! School
! Conf.
! Over.
! Tiebreaker
|-
|1 
|‡-[[2013–14 Notre Dame Fighting Irish women's basketball team|Notre Dame]]
| 16-0
| 29-0
|
|-
|2
|†-[[2013–14 Duke Blue Devils women's basketball team|Duke]]
|12-4
|25-5
|1-0 vs. Maryland
|-
|3
|†-[[2013–14 Maryland Terrapins women's basketball team|Maryland]]
|12-4
|24-5
|0-1 vs. Duke
|-
|4
|†-[[2013–14 NC State Wolfpack women's basketball team|NC State]]
|11-5
|24-6
|
|-
|5
|#-[[2013–14 Syracuse Orange women's basketball team|Syracuse]]
|10-6
|21-8
|1-0 v. North Carolina
|-
|6
|#-[[2013–14 North Carolina Tar Heels women's basketball team|North Carolina]]
|10-6
|22-8
|0-1 vs. Syracuse
|-
|7
|#-[[2013–14 Georgia Tech Yellow Jackets women's basketball team|Georgia Tech]]
|9-7
|19-10
|
|-
|8
|#-[[2013–14 Miami Hurricanes women's basketball team|Miami (FL)]]
|8-8
|16-13
|
|-
|9
|#-[[2013–14 Florida State Seminoles women's basketball team|Florida State]]
|7-9
|19-10
|
|-
|10
|[[2013–14 Virginia Cavaliers women's basketball team|Virginia]]
|6-10
|13-16
|
|-
|11
|[[2013–14 Wake Forest Demon Deacons women's basketball team|Wake Forest]]
|5-11
|14-15
|
|-
|12
|[[2013–14 Virginia Tech Hokies women's basketball team|Virginia Tech]]
|4-12
|14-15
|1-0 vs. Clemson
|-
|13
|[[2013–14 Clemson Tigers women's basketball team|Clemson]]
|4-12
|12-18
|0-1 vs. Virginia Tech
|-
|14
|[[2013–14 Pittsburgh Panthers women's basketball team|Pittsburgh]]
|3-13
|11-19
|1-0 vs. Boston College
|-
|15
|[[2013–14 Boston College Eagles women's basketball team|Boston College]]
|3-13
|12-18
|0-1 vs. Pitt
|-
| colspan="10" style="text-align:left;|<small>‡ – ACC regular season champions, and tournament No. 1 seed.<br>† – Received a double-bye in the conference tournament.<br># – Received a single-bye in the conference tournament.<br>Overall records include all games played in the ACC Tournament.</small>
|-
|}

==Schedule==
{| class="wikitable" style="font-size: 95%"
|- align="center"
!Session
!Game
!Time*
!Matchup<sup>#</sup>
!Television
!Attendance
|-
!colspan=6| First Round – Wednesday, March 5
|-
|rowspan=3|<center>Opening<br>day
|<center>1
|1 pm
|<center>#12 Virginia Tech vs #13 Clemson   
|rowspan=3|<center>[[Regional sports network|RSN]]
|rowspan=2|<center>4,440
|-
|<center>2
|3:30 pm
|<center>#10 Virginia vs #15 Boston College
|-
|<center>3
|6:30 pm
|<center>#11 Wake Forest vs #14 Pittsburgh
|<center>4,950
|-
!colspan=6| Second Round – Thursday, March 6
|-
|rowspan=2|<center>1
|<center>4
|11 am
|<center>#5 Syracuse vs #13 Clemson
|rowspan=4|<center>RSN
|rowspan=2|<center>4,026
|-
|<center>5
|2 pm
|<center>#8 Miami vs #9 Florida State
|-
|rowspan=2|<center>2
|<center>6
|6 pm
|<center>#7 Georgia Tech vs #10 Virginia
|rowspan=2|<center>5,941
|-
|<center>7
|8 pm
|<center>#6 North Carolina vs #11 Wake Forest
|-
!colspan=6| Quarterfinals – Friday, March 7
|- 
|rowspan=2|<center>3
|<center>8
|noon
|<center>#4 NC State vs #5 Syracuse
|rowspan=4|<center>RSN
|rowspan=2|<center>4,506
|-
|<center>9
|2 pm
|<center>#1 Notre Dame vs #9 Florida State
|-
|rowspan=2|<center>4
|<center>10
|6 pm
|<center>#2 Duke 82 vs #7 Georgia Tech
|rowspan=2|<center> 6,949
|-
|<center>11
|8 pm
|<center>#3 Maryland vs #6 North Carolina
|-
!colspan=6| Semifinals – Saturday, March 8
|-
|rowspan=2|<center>5
|<center>12
|5 pm
|<center>#1 Notre Dame vs #4 NC State
|rowspan=2|<center>[[ESPNU]]
|rowspan=2|<center>8,169
|-
|<center>13
|7:30 pm
|<center>Game #2 Duke vs #6 North Carolina
|-
!colspan=6| Championship Game – Sunday, March 9
|-
|rowspan=1|<center>6
|<center>14
|7 pm
|<center>#1Notre Dame vs #2 Duke
|<center>[[ESPN]]
|<center>8,190
|-  
|colspan=6| <small>*Game Times in [[Eastern Time Zone|ET]]. #-Rankings denote tournament seed</small> 
|}

==Bracket==
{{15TeamBracket-ACCBasketball
| RD1='''First Round'''<br/>Wednesday, March 5<br/>FSN
| RD2='''Second Round'''<br/>Thursday, March 6<br/>FSN
| RD3='''Quarterfinals'''<br/>Friday, March 7<br/>FSN
| RD4='''Semifinals'''<br/>Saturday, March 8<br/>ESPNU
| RD5='''Championship Game'''<br/>Sunday, March 9<br/>ESPN

| RD1-seed01=12
| RD1-team01=Virginia Tech
| RD1-score01=56
| RD1-seed02=13
| RD1-team02='''Clemson
| RD1-score02='''69

| RD1-seed03=10
| RD1-team03='''Virginia
| RD1-score03='''74
| RD1-seed04=15
| RD1-team04=Boston College
| RD1-score04=59

| RD1-seed05=11
| RD1-team05='''Wake Forest
| RD1-score05='''72
| RD1-seed06=14
| RD1-team06=Pittsburgh
| RD1-score06=58

| RD2-seed01=8
| RD2-team01=Miami
| RD2-score01=67
| RD2-seed02=9
| RD2-team02='''Florida State
| RD2-score02='''72'''<sup>OT</sup>

| RD2-seed03=5
| RD2-team03='''Syracuse
| RD2-score03='''63
| RD2-seed04=13
| RD2-team04=Clemson
| RD2-score04=53

| RD2-seed05=7
| RD2-team05='''Georgia Tech
| RD2-score05='''77
| RD2-seed06=10
| RD2-team06=Virginia
| RD2-score06=76

| RD2-seed07=6
| RD2-team07='''North Carolina
| RD2-score07='''69
| RD2-seed08=11
| RD2-team08=Wake Forest
| RD2-score08=65

| RD3-seed01=1
| RD3-team01='''Notre Dame
| RD3-score01='''83
| RD3-seed02=9
| RD3-team02=Florida State
| RD3-score02=57

| RD3-seed03=4
| RD3-team03='''NC State
| RD3-score03='''79
| RD3-seed04=5
| RD3-team04=Syracuse
| RD3-score04=63

| RD3-seed05=2
| RD3-team05='''Duke
| RD3-score05='''82
| RD3-seed06=7
| RD3-team06=Georgia Tech
| RD3-score06=52

| RD3-seed07=3
| RD3-team07=Maryland
| RD3-score07=70
| RD3-seed08=6
| RD3-team08='''North Carolina
| RD3-score08='''73

| RD4-seed01=1
| RD4-team01='''Notre Dame
| RD4-score01='''83
| RD4-seed02=4
| RD4-team02=NC State
| RD4-score02=48

| RD4-seed03=2
| RD4-team03='''Duke
| RD4-score03='''66
| RD4-seed04=6
| RD4-team04=North Carolina
| RD4-score04=61

| RD5-seed01=1
| RD5-team01='''Notre Dame
| RD5-score01='''69
| RD5-seed02=2
| RD5-team02=Duke
| RD5-score02=53
}}

<sup>OT</sup> denotes overtime game

==Awards and honors==
'''Tournament [[Most Valuable Player|MVP]]:''' Jewell Loyd, Notre Dame

'''All-Tournament Teams:'''
{{Col-begin}}
{{Col-1-of-2}}
:'''First Team'''
:* Jewell Loyd, Notre Dame
:* Kayla McBride, Notre Dame
:* Elizabeth Williams, Duke
:* Tricia Liston, Duke
:* Diamond DeShields, North Carolina

{{Col-1-of-2}}
'''Second Team'''
* Natalie Achonwa, Notre Dame
* Haley Peters, Duke
* Dearica Hamby, Wake Forest
* Natasha Howard, Florida State
* Alyssa Thomas, Maryland

{{Col-end}}

==See also==
* [[2014 ACC Men's Basketball Tournament]]

==References==
{{reflist}}

==External links==
* [http://www.theacc.com/#!/page/championship_w-baskbl Atlantic Coast Conference Women's Basketball Tournament Official Website]

{{ACC Women's Basketball Tournament navbox}}
{{2014 NCAA Division I women's basketball tournament navbox}}

{{DEFAULTSORT:2014 ACC Women's Basketball Tournament}}
[[Category:2013–14 Atlantic Coast Conference women's basketball season]]
[[Category:ACC Women's Basketball Tournament]]