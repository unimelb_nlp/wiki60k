{{Infobox journal
| title = Management and Organization Review
| editor =  Arie Y. Lewin
| discipline = [[Management]]
| abbreviation = Manage. Organ. Rev.
| publisher = [[Cambridge University Press]] on behalf of the [[International Association for Chinese Management Research]]
| frequency = Triannually
| history = 2005-present
| openaccess = 
| license = 
| impact = 3.277
| impact-year = 2013
| website = http://journals.cambridge.org/action/displayJournal?jid=MOR
| link1 = http://journals.cambridge.org/action/displayIssue?jid=MOR&tab=currentissue
| link1-name = Online access
| link2 = http://journals.cambridge.org/action/displayBackIssues?jid=MOR
| link2-name = Online archive
| JSTOR = 
| OCLC = 56392067
| LCCN = 2005211366
| CODEN = 
| ISSN = 1740-8776
| eISSN = 1740-8784
}}
'''''Management and Organization Review''''' is a triannual [[peer-reviewed]] [[academic journal]] published by [[Cambridge University Press]] on behalf of the [[International Association for Chinese Management Research]]. It covers research on international, comparative, and cross-cultural management in a [[China|Chinese]] context. The [[editor-in-chief]] is Arie Y. Lewin ([[Duke University]]).

== Abstracting and indexing ==
''Management and Organization Review'' is abstracted and indexed in the [[Social Sciences Citation Index]], [[Scopus]], [[ProQuest]], and [[EBSCO Publishing|EBSCO databases]]. According to the ''[[ISI Journal Citation Reports ]]'', the journal has a 2013 [[impact factor]] of 3.277, ranking it 14th out of 172 journals in the category "Management".<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Management |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]] |postscript=.}}</ref>

== Young Scholar Award ==
In 2008 ''Management and Organization Review'' established the Young Scholar Award, which is awarded once every two years to one author who has recently published in the journal, and received their degree no more than five years prior to their article's publication. Winners receive a 12-month membership to the International Association for Chinese Management Research and $2,000 prize money.<ref>{{Cite web |url=http://www.iacmr.org/Awards/MORAwards/Awards.htm |title= Young Scholar Award |accessdate=2012-12-10}}</ref> 

== References ==
{{Reflist}}

== External links ==
* {{Official website|1=http://journals.cambridge.org/action/displayJournal?jid=MOR}}

[[Category:Cambridge University Press academic journals]]
[[Category:Business and management journals]]
[[Category:English-language journals]]
[[Category:Publications established in 2005]]
[[Category:Triannual journals]]
[[Category:Academic journals associated with learned and professional societies]]