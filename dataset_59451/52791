The '''San Francisco Declaration on Research Assessment''' (DORA) intends to halt the practice of correlating the [[journal impact factor]] to the merits of a specific scientist's contributions. Also according to this statement, this practice creates biases and inaccuracies when appraising [[scientific research]]. It also states that the impact factor is not to be used as a substitute "measure of the quality of individual research articles, or in hiring, promotion, or funding decisions".<ref name=Alberts/>

The declaration originated from the December 2012 [[academic conference|meeting]] of the [[American Society for Cell Biology]].
On May 13, 2013, more than 150 [[scientist]]s and 75 [[professional society|scientific organizations]] had signed  the declaration.<ref name=Alberts>
{{cite journal 
|last=Alberts 
|first=Bruce 
|authorlink=Bruce Alberts
|title=Impact Factor Distortions 
|journal=[[Science (journal)|Science]] 
|volume=340 
|issue=6134 
|pages=787 
|publisher=[[American Association for the Advancement of Science]] 
|date=May 17, 2013 
|url=http://www.sciencemag.org/content/340/6134/787.full
|doi=10.1126/science.1240319}}
</ref><ref name=Noorden>
{{cite journal 
|last=Van Noorden 
|first=Richard 
|title=Scientists join journal editors to fight impact-factor abuse 
|journal=Nature News Blog 
|volume=|issue=|pages=
|publisher=[[Nature Publishing Group]] 
|date=May 16, 2013
|url=http://blogs.nature.com/news/2013/05/scientists-join-journal-editors-to-fight-impact-factor-abuse.html 
|doi=}}</ref> 
The American Society for Cell Biology states that, {{as of|2013|05|10|lc=yes}}, there are more than 6,000 individual signatories to the declaration and that the number of scientific organizations "signing on has gone from 78 to 231" within two weeks.<ref name=afcb>
{{cite web 
|last=Fleischman 
|first=John
|title=Impact Factor Insurrection Catches Fire with Over 6,000 Signatures and Counting 
|publisher=The American Society for Cell Biology 
|date=May 30, 2013 
|url=http://am.ascb.org/ascbpost/index.php/us-news/item/198-impact-factor-insurrection-catches-fire-with-over-6000-signatures-and-counting |doi=}}</ref> As of 4 February 2017, the number of individual signatories has risen to over 12,000 and the number of scientific organizations to 840.<ref>{{Cite news|url=http://www.ascb.org/dora/|title=Dora - ASCB|newspaper=ASCB|language=en-US|access-date=2017-02-04}}</ref>

== Motivation ==
On 16 December 2012, a group of editors and publishers of [[academic journal]]s gathered at the Annual Meeting of The American Society for Cell Biology in San Francisco to discuss current issues related to how the quality of research output is evaluated and how the primary [[scientific literature]] is cited.<ref name=cagan/>

The motivation behind the meeting was the consensus that impact factors for many [[cell biology|cell biology journals]] do not accurately reflect the value to the cell biology community of the work published in these journals. The group therefore wanted to discuss how to better align measures of journal and article impact with journal quality.

All the above considerations also extend to other fields and the organizers consider DORA ''a worldwide initiative covering all scholarly disciplines''. In fact, the declaration has been signed by scientific associations with general scope, such as the [[American Association for the Advancement of Science]] or the [[Academy of Sciences of the Czech Republic]], by more specialized associations working in fields quite removed from biology, such as the [[European Mathematical Society]], the [[Geological Society of London]] or the [[Linguistic Society of America]], by some universities and other general institutions such as the [[Higher Education Funding Council for England]].

There is also an alarming trend for the [[citation]] of [[review article]]s over [[primary literature]], driven in part by space limitations that are imposed by some journals. Because this citation bias contributes to lower [[citation index|citation indices]] for journals that focus mainly on primary literature, the group discussed ways to combat this trend as well.

The outcome of the meeting and further discussions was a set of recommendations that is referred to as the San Francisco Declaration on Research Assessment, published in May 2013.<ref name=cagan>
{{cite journal
| doi=10.1242/dmm.012955
| url = http://dmm.biologists.org/content/early/2013/05/16/dmm.012955.full.pdf
| title= San Francisco Declaration on Research Assessment
| year=2013
| last1=Cagan
| first1=R.
| journal=[[Disease Models & Mechanisms]]}}</ref>

== See also ==
* ''[[Journal Citation Reports]]''
* [[Scientific journal]]

== References ==
{{reflist|30em}}

== Further reading ==
* {{cite journal |doi=10.1126/science.1165316 |title=The Misused Impact Factor |year=2008 |last1=Simons |first1=K. |journal=Science |volume=322 |issue=5899 |pages=165 |pmid=18845714}}
* {{cite journal |last=Archambault |first=Éric |authorlink= |author2=Vincent Larivière  |title=History of the journal impact factor: Contingencies and consequences |journal=[[Scientometrics (journal)|Scientometrics]] |volume=79 |issue=3 |pages=635–649 |publisher=|year=2009 |url=http://www.ost.uqam.ca/Portals/0/docs/articles/2009/11-arch2036.pdf |doi=10.1007/s11192-007-2036-x}}
* Andrew Plume (3 June 2013).  [https://www.elsevier.com/connect/san-francisco-declaration-on-research-assessment-dora-elseviers-view San Francisco Declaration on Research Assessment (DORA) – Elsevier’s view].  A commentary on DORA by the Associate Director – Scientometrics & Market Analysis of major scientific journal publisher [[Elsevier]].

== External links ==
* {{Official website|http://ascb.org/dora/}}

[[Category:Academic publishing]]
[[Category:American Society for Cell Biology]]
[[Category:Science and technology in the San Francisco Bay Area]]
[[Category:2012 in California]]
[[Category:2012 in science]]