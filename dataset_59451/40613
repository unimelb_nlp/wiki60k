'''Elias Ladopoulos'''<ref>{{Cite book|title = CyberwarsL Espionage on the Internet|last = Guisnel|first = Jean|publisher = Basic Books|year = 1997|isbn = 0-7382-0260-6|location = |pages = 118}}</ref> is a technologist and investor from [[New York City]].<ref>{{Cite web|url=http://www.raptorgroup.com/news/277|title=Raptor Bites Into VC Market With $32M Toward Fund I {{!}} Raptor Group|website=www.raptorgroup.com|access-date=2016-04-11}}</ref> Under the pseudonym '''Acid''' '''Phreak''',<ref>{{Cite web|url=https://w2.eff.org/Net_culture/Hackers/sentencing_hacker.article|title=Reflections On Hacker Sentencing 07/29/93|last=McMullen|first=John|date=|website=https://w2.eff.org|publisher=|access-date=}}</ref> he was a founder of the ''[[Masters of Deception]]'' (MOD) hacker group<ref>{{Cite news|title = Computer Savvy, With an Attitude; Young Working-Class Hackers Accused of High-Tech Crime|url = https://www.nytimes.com/1992/07/23/nyregion/computer-savvy-with-attitude-young-working-class-hackers-accused-high-tech-crime.html|newspaper = The New York Times|date = 1992-07-23|access-date = 2015-12-29|issn = 0362-4331|first = Mary B. W. Tabor With Anthony|last = Ramirez}}</ref> along with Phiber Optik ([[Mark Abene]]) and Scorpion ([[Paul Stira]]). Referred to as The Gang That Ruled Cyberspace<ref>{{cite book|last1=Slatalla|first1=Michelle|last2=Quittner|first2=Joshua|title=Masters of Deception: The Gang That Ruled Cyberspace|date=10 January 1996|publisher=Harper Collins|isbn=978-0-06-092694-6}}</ref> in a 1995 non-fiction book, MOD was at the forefront of exploiting telephone systems to hack into the private networks of major corporations.<ref>{{Cite web|url = http://www.junauza.com/2011/07/7-most-notorious-computer-hacker-groups.html|title = 7 Most Notorious Computer Hacker Groups of All Time|date = |accessdate = |website = |publisher = |last = Auza|first = Jun}}</ref> In his later career, Ladopoulos developed new techniques for electronic trading and computerized projections of stocks and shares performance, as well as working as a security consultant for the defense department. He is currently CEO of Supermassive Corp, which is a hacker-based incubation studio for technology start-ups].<ref>http://www.usatoday.com/story/tech/personal/2015/02/04/ozy-hacker-proof-helpers/22829861/</ref>

==Founding of MOD==

When Ladopoulos and Stira were engaged in exploring an unusual telephone system computer, Ladopoulos suggested seeking advice from Phiber Optik (Mark Abene), a well-known phreak who was also a member of the prestigious [[Legion of Doom (hacking)|Legion of Doom]] (LOD) group. A productive phone hacking partnership developed, with the group later branding themselves Masters of Deception (MOD).<ref>{{Cite web|title = Gang War in Cyberspace|url = https://www.wired.com/1994/12/hacker-4/|website = WIRED|accessdate = 2015-12-28|language = en-US}}</ref>
 
MOD’s hacking exploits included taking control of every major phone system and global packet-switching network in the United States. Ladopoulos claims that he and another hacker were able to place a call to Queen [[Elizabeth II]]. Their pranks included taking over the printers of the Public Broadcasting Service (PBS), an incident that escalated when another hacker used the access they had established to wipe the PBS systems. The group is also known for retrieving phone and credit information for celebrities such as Julia Roberts and John Gotti.<ref>{{Cite news|url = https://news.google.com/newspapers?nid=1755&dat=19931105&id=xDEcAAAAIBAJ&sjid=YHsEAAAAIBAJ&pg=4952,5919414&hl=en|title = Hacker Sentenced for Computer Crimes|last = |first = |date = November 5, 1993|work = |access-date = Dec 29, 2015|via = }}</ref>

===Conflict with former Legion of Doom members===

Abene’s involvement in both LOD and MOD showed a natural alignment between the two groups in MOD’s early years. As LOD’s original membership broke up however, conflicts arose between Abene and Eric Bloodaxe (Chris Goggans), another LOD member. Goggans declaring that Abene had been expelled from LOD, resulted in a permanent split between the two groups. Ladopoulos is credited with writing "The History of MOD" for "other hackers to envy."<ref>{{Cite web|title = NYTimes|url = https://www.nytimes.com/books/99/01/03/specials/slatalla-masters.html|website = www.nytimes.com|accessdate = 2015-12-29}}</ref> Further disagreements and pranks, including the hacking of Goggans’s security consultancy ComSec,<ref>{{Cite web|title = Computer Savvy, With an Attitude: Young Working-Class Hackers Accused of High-Tech Crime|url = https://partners.nytimes.com/library/cyber/hackstock/920723with-an-attitude.html|website = partners.nytimes.com|accessdate = 2015-12-29}}</ref> have been characterized as the [[Great Hacker War]].<ref>{{Cite web|title = Gang War in Cyberspace|url = https://www.wired.com/1994/12/hacker-4/|website = WIRED|publisher = https://plus.google.com/+WIRED|accessdate = 2015-12-28|language = en-US}}</ref>

===Prosecution===

On January 15, 1990 ([[Martin Luther King Day]]), the AT&T telephone network crashed.<ref>{{Cite book|title = Hacking for Beginners: a beginners guide to learn ethical hacking|last = Desai|first = Manthan|publisher = hackingtech.co.tv|year = |isbn = |location = |pages = 272, 274}}</ref> Later investigations revealed the cause to be a software bug, however an FBI task force that had been investigating MOD was convinced the group was implicated. On January 24 the FBI raided the homes of five MOD members, including Ladopoulos, Abene and Stira.<ref name="sterling">{{cite book|last1=Sterling|first1=Bruce|title=The Hacker Crackdown|date=1992|publisher=Bantam|location=New York|isbn=9780553080582|page=233}}</ref> Despite being released without charge due to lack of evidence, the MOD members were later re-arrested on a conspiracy charge following wire-tapping of future MOD members. After Abene rejected a plea bargain, Ladopoulos refused to testify against his fellow hacker, pleaded guilty and was sentenced to 6 months in a supervised camp facility, followed by 6 months house arrest.  According to U.S. Attorney [[Otto G. Obermaier|Otto Obermaier]] it was the "first investigative use of court-authorized wiretaps to obtain conversations and data transmissions of computer hackers" in the United States.{{citation needed|date=April 2016}}

==Career==

After completing his sentence, Ladopoulos was hired as a security engineer by the Reuters-owned electronic trading business, [[Instinet]].  Hiring other former hackers, Ladopoulos built a department responsible for securing Instinet’s global trading operations and developing security systems that were later acquired by NASDAQ. Later, as a consultant for Instinet, Ladopoulos also worked as VP Operations for the government security contractor NetSec (later Verizon Government).
 
In 2008, he founded Kinetic Global Markets with Roger Ehrenberg. As CEO and CIO, he led a team pioneering new approaches to [[systematic trading]] based on the computational analysis of terms used in SEC filings. Ladopoulos consulted on Ehrenberg’s launch of IA Venture Capital.
 
In 2013, Ladopoulos founded Supermassive Corp.,<ref>http://www.usatoday.com/story/tech/personal/2015/02/04/ozy-hacker-proof-helpers/22829861/</ref> which describes itself as the original hacker incubation studio, “bringing together teams of extremely unique talents to rapidly prototype ideas that have a big impact.”

==References==
{{reflist}}
*

==External links==
* The History of MOD
** [http://www.textfiles.com/hacking/modbook1.txt modbook1.txt] — ''"The History of MOD: Book One: The Originals"''
** [http://www.textfiles.com/hacking/modbook2.txt modbook2.txt] — ''"The History of MOD: Book Two: Creative Mindz"''
** [http://www.textfiles.com/hacking/modbook3.txt modbook3.txt] — ''"The Book of MOD: Part Three: A Kick in the Groin"''
** [http://www.textfiles.com/hacking/modbook4.txt modbook4.txt] — ''"The Book of MOD: Part Four: End of '90-'1991"''
** [http://www.textfiles.com/hacking/modbook5.txt modbook5.txt] — ''"The Book of MOD: Part 5: Who are They And Where Did They Come From? (Summer 1991)"''
*Small Scale Sin, Act Three http://www.thisamericanlife.org/radio-archives/episode/2/small-scale-sin?act=3#play

<!-- Categories -->

[[Category:People associated with computer security]]

[[Category:Living people]]
[[Category:Masters of Deception]]

[[Category:1972 births]]
[[Category:Legion of Doom (hacking)]]
[[Category:People from New York City]]
[[Category:Phreaking]]

{{DEFAULTSORT:Ladopoulos, Elias}}