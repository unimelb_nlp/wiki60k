{{Infobox military structure
|name=Brookley Air Force Base
|ensign=[[File:USAF - Logistics Command.png|50px]]&nbsp;[[File:Mats-blue.jpg|60px]]<br>{{small|Brookley Army Airbase}}
|partof= [[Air Force Logistics Command]] (AFLC)
|location= Located near: [[Mobile, Alabama]]
|image= [[File:Brookleyfield-al-7apr1952.jpg|300px]]
|caption=Brookley Air Force Base - 7 April 1952
|pushpin_map= Alabama
|pushpin_label=Brookley AFB
|pushpin_mapsize=300
|pushpin_mark=Airplane_silhouette.svg
|coordinates = {{coord|30|37|36|N|088|04|05|W|type:landmark|display=inline}}
|built=1940
|builder=
|materials=
|height=
|used=1940-1969
|demolished=
|condition=
|ownership=
|controlledby={{air force|USA}}
|garrison=
|commanders=
|occupants=
|battles=
|events=
}}
: ''For the civil use of Brookley AFB after 1969, see: [[Mobile Downtown Airport]]''
'''Brookley Air Force Base''' is a former [[United States Air Force]] base located in [[Mobile, Alabama]]. After it closed in 1969, it became what is now known as the [[Mobile Aeroplex at Brookley]].

== History ==
Brookley Air Force Base had its [[Aeronautics|aeronautical]] beginnings with Mobile's first municipal airport, the original '''Bates Field'''. However, the site itself had been occupied from the time of Mobile's founding, starting with the home of Mobile's founding father, [[Jean-Baptiste Le Moyne, Sieur de Bienville]], in the early 18th century.<ref name=storymobile1>Delaney, Caldwell. ''The Story of Mobile'', page 32. Mobile, Alabama: Gill Press, 1953. ISBN 0-940882-14-0</ref>

In 1938 the [[United States Army Air Corps|Army Air Corps]] took over the then {{convert|1000|acre|0|sing=on|abbr=on}} Bates Field site and established the '''Brookley Army Air Field'''.<ref name="newhistory1">Thomason, Michael. ''Mobile : the new history of Alabama's first city'', page 213. Tuscaloosa : University of Alabama Press, 2001. ISBN 0-8173-1065-7</ref>  The military was attracted to the site because of the area's generally good flying weather and the bay-front location, but Alabama [[U.S. Congressman|Congressman]] [[Frank Boykin]]'s influence in [[Washington D.C.|Washington]] was important in convincing the [[United States Army|Army]] to locate the new military field in Mobile instead of [[Tampa, Florida]].<ref name="newhistory2">Thomason, Michael. ''Mobile : the new history of Alabama's first city'', page 210. Tuscaloosa : University of Alabama Press, 2001. ISBN 0-8173-1065-7</ref>  However, later that year, Tampa was also chosen for a military flying installation of its own, which would be named [[MacDill Field]], home of present-day [[MacDill Air Force Base]].

===World War II===
[[File:Brookley Field Mobile Alabama.jpg|thumb|left|World War II scene at Brookley Army Air Field.]]
During [[World War II]], Brookley Army Air Field became the major [[United States Army Air Forces|Army Air Forces]] supply base for the [[Air Material Command]] in the southeastern United States and the Caribbean.<ref name="newhistory1"/><ref name="adahbrookley">{{cite web |title=Alabama and World War II |work=Alabama Department of Archives and History |url=http://www.alabamamoments.state.al.us/sec50det.html |accessdate=2007-12-06}}</ref>

Many air depot personnel, logisticians, mechanics, and other support personnel were trained at Brookley during the war.  Both Air Materiel and Technical Services Command organized mobile Depot Groups at Brookley, then once trained were deployed around the world as Air Depot Groups, Depot Repair Squadrons, Quartermaster Squadrons, Ordnance Maintenance, Military Police, and many other units whose mission was to support the front-line combat units with depot-level maintenance for aircraft and logistical support to maintain their operations.  [[Air Transport Command (United States Air Force)|Air Transport Command]] operated large numbers of cargo and passenger aircraft from the base as part of its Domestic Wing.

During the war, Brookley became Mobile’s largest employer, with about 17,000 skilled civilians capable of performing delicate work with fragile instruments and machinery.  In 1944, the Army decided to take advantage of Brookley’s 
large, skilled workforce for its top-secret “Ivory Soap” project to hasten victory in the Pacific. The project required 24 large vessels to be re-modeled into Aircraft Repair and Maintenance Units that had to be able to accommodate B-29 bombers, P-51 Mustangs, R-4B Sikorsky helicopters, and amphibious vehicles.<ref name="newhistory1"/><ref name="adahbrookley"/>

The Air Force delivered all 24 vessels to Mobile, Alabama, in spring 1944 to start remodeling. Some 5,000 men 
underwent a complex training process that prepared them to rebuild the vessels and operate them once on the water. By the end of the year, the vessels departed Mobile.

One of the keys to Allied victory in Europe was the Norden Bomb Sight, which enabled bomber squadrons to target Germany’s war-making industry and infrastructure much more accurately.  The military repaired and calibrated the bombsights at Brookley in a secret facility, still standing and in use today.

In 1944 with the closure of the Army contract flying school at nearby [[Bates Army Airfield]],  Air Transport Command operations were shifted to Bates to alleviate runway traffic at Brookley.    Late in 1945 Bates Field was returned to civil control and ATC operations returned to Brookley.

===Postwar use===
Following World War II and the creation of an independent [[United States Air Force]], the installation became '''Brookley Air Force Base'''.   In 1947 with the closure of [[Morrison Field]], Florida,  the [[C-74 Globemaster]] project was moved to Brookley.   The C-74 was, at the time, the largest military transport aircraft in the world.  It was developed by [[Douglas Aircraft]] after the Japanese [[attack on Pearl Harbor]]. The long distances across the Atlantic, and especially the Pacific Ocean to the combat areas indicated a need for a transoceanic heavy-lift military transport aircraft.

[[File:1701st Air Transport Wing Douglas C-74 Globemaster at Brookley AFB.jpg|thumb|left|1701st Air Transport Wing Douglas C-74 Globemaster at Brookley AFB in the early 1950s]]
The "C-74 squadron" (later 521st Air Transport Group, [[1701st Air Transport Wing]]), [[Air Transport Command (United States Air Force)|Air Transport Command]] operated two squadrons of C-74 Globemasters from Brookley from 1947 until their retirement in 1955.  The eleven aircraft were used extensively for worldwide transport of personnel and equipment, supporting United States military missions.  They saw extensive service supporting the [[Berlin Airlift]] and the [[Korean War]] being used on scheduled [[Military Air Transport Service|MATS]] overseas routes though the late 1940s and mid-1950s.  Additionally, logistic support flights for [[Strategic Air Command]] (SAC), and [[Tactical Air Command]] (TAC) saw the Globemaster in North Africa, the Middle East, Europe, the Caribbean, and within the United States. Two C-74s were used to support the first TAC [[Republic F-84 Thunderjet]] flight across the Pacific Ocean to Japan. SAC also continued to use the Globemasters to rotate [[Boeing B-47 Stratojet]] Medium Bombardment Groups on temporary duty in England and Morocco as part of their REFLEX operation.  The C-74s were retired in 1955 due to lack of logistical support. The 1701st ATW flew strategic airlift missions on a worldwide scale with its [[C-124 Globemaster II]] fleet after the retirement of the C-74 until 1957 when Military Air Transport Service moved out of Brookley AFB and the base came under the full jurisdiction of Air Material Command.

In 1962, the Air Material Command was renamed as the [[Air Force Logistics Command]] ([[Air Force Logistics Command|AFLC]]) and Brookley AFB became an AFLC installation and the host base of the modification and repair center's successor organization, the '''Mobile Air Materiel Area''' (MOAMA).

After an immediate end to many of the wartime jobs of World War II, the base's civilian workforce again expanded to around 16,000 people by 1962, a result of both the [[Cold War]] and other [[USAF]] base closings in other areas of the country.<ref name="newhistory3">Thomason, Michael. ''Mobile : the new history of Alabama's first city'', page 286. Tuscaloosa : University of Alabama Press, 2001. ISBN 0-8173-1065-7</ref>  During this time, [[Air Force Logistics Command|AFLC]]'s Mobile Air Materiel Area (MOAMA) provided depot-level maintenance for various [[USAF]] aircraft of the period, to include the [[C-119 Flying Boxcar]], [[C-131 Samaritan]], [[F-84 Thunderstreak]], [[RF-84 Thunderflash]],the [[F-104 Starfighter]] and the Republic F-105 Thunderchief.

In 1964, the [[Air Force Reserve]] [[908th Tactical Airlift Group]] moved to Brookley from  Bates Field.  It operated [[C-119 Flying Boxcar]] transports.

===Closure===
On 19 November 1964, the [[United States Department of Defense|Department of Defense]] announced a progressive reduction in employment and the eventual closure of Brookley Air Force Base.<ref name="newhistory4">Thomason, Michael. ''Mobile : the new history of Alabama's first city'', pages 289-297. Tuscaloosa : University of Alabama Press, 2001. ISBN 0-8173-1065-7</ref>  The costs of the escalation of the [[Vietnam War]] was cited as the primary reason for the closure.  [[Robert McNamara]], the Secretary of Defense, was unpopular both with Congress and with the public.   Military bases were sources of employment and federal dollars for states and local communities, which allowed them to handle the cost of them and sales to military people stationed at the base.

Moreover, McNamara worked for President [[Lyndon B. Johnson]], who had a reputation for rewarding friends and punishing opponents.   When McNamara began the base closure announcements, suspicion began that Johnson was picking bases to close as retribution for the recent [[United States presidential election, 1964|1964 Presidential Election]].   The Republican candidate, Senator [[Barry Goldwater]], had carried Alabama in the election and it was believed that Johnson was penalizing Alabama for defecting from its traditional [[Democratic Party (United States)|Democratic Party]] ties.  McNamara, however, had another agenda, as he wanted to curb the Air Force's reliance on large aircraft in favor of long-range missiles and closing maintenance facilities such as Brookley was a way to do that.  McNamara, however, denied that politics played any part in the decision to close several Air Force bases including Brookley.<ref>[https://news.google.com/newspapers?id=TexNAAAAIBAJ&sjid=jYoDAAAAIBAJ&pg=7489,4507977&dq=brookley+air+force+base+closure&hl=en McNamara denies political motives]</ref>

The reserve 904th TAG was moved to [[Maxwell AFB]], Alabama in April. The incoming Nixon Administration in 1969 confirmed the closure of Brookley as a way to save money because of the Vietnam War, and when it finally closed in June 1969,<ref>[http://www.brookleyaeroplex.com/aeroplex_history.php History]</ref> Brookley AFB represented the largest base closure in U.S. history up to that time, eliminating 10% of local jobs for the Mobile workforce, which provided an annual payroll of $95 million to the local economy.<ref name="newhistory4"/>

===Major USAF units assigned===
* [[1701st Air Transport Wing]]
* [[1703d Air Transport Group]]
* [[908th Tactical Airlift Group]]
* [[26th Weather Squadron]]

===Post-military use===
After closure, the base was returned to the City of Mobile.  Later, the city transferred it to the Mobile Airport Authority and it became known as the Mobile Downtown Airport.  The city had created the Mobile Airport Authority in 1982 to oversee the operation of the [[Mobile Regional Airport]] and what would become the [[Mobile Aeroplex at Brookley]].<ref name="mmaw1">{{cite web|title=Mobile Airport Authority FAQs |work=Mobile Airport Authority website |url=http://www.mobairport.com/info/FAQ.php |accessdate=2007-12-06 |archiveurl=http://archive.is/20070901003316/http://www.mobairport.com/info/FAQ.php |archivedate=2007-09-01 |deadurl=yes |df= }}</ref> The Mobile Airport Authority is autonomous and is not a part of the city or Mobile County.<ref name="mmaw1"/> The Authority’s five board members are appointed by Mobile’s Mayor, approved by the Mobile City Council, and serve 6 year terms.<ref name="mmaw1"/>

[[Airbus]] recently began construction on an aircraft production plant at Brookley, producing the [[Airbus A320]] airliners.<ref>http://www.airbus.com/presscentre/pressreleases/press-release-detail/detail/brookley-aeroplexs-infrastructure-will-facilitate-the-production-of-a320-family-aircraft-in-the-u/ | Brookley Aeroplex’s infrastructure will facilitate the production of A320 Family aircraft in the U.S.</ref> Airbus had previously attempted to enter the market at Brookley Field when its military division [[EADS]] partnered with [[Northrop Grumman]] to produce the [[KC-45]], billed as the next generation of air refueling & cargo aircraft for the US Air Force as a replacement to the aging fleet of [[KC-135]]s. EADS/Northrop Grumman originally won the contract bid to produce the aircraft, but the plans were put in limbo after rival [[Boeing]] filed a protest over the bidding process;<ref>https://www.wsj.com/articles/SB120526137121727825 | Boeing Files Formal Protest Of Tanker Award</ref> and in 2011 Boeing was declared the winner of the rebidding.<ref>https://www.nytimes.com/2011/03/05/business/global/05tanker.html?_r=0 | EADS Won’t Protest Loss of Tanker Contract to Boeing</ref>

==In popular culture==
In the 1977 film, [[Close Encounters of the Third Kind]], the entire landing strip complex behind Devils Tower was actually constructed and [http://www.imdb.com/title/tt0075860/locations?ref_=tt_dt_dt filmed] in an abandoned aircraft hangar at former Brookley AFB.

== See also ==
{{Portal|United States Air Force|Military of the United States|World War II}}
* [[Alabama World War II Army Airfields]]

==References==
{{Air Force Historical Research Agency}}
<references />

==External links==
* [http://www.brookleycomplex.com/ Brookley Complex], official website
* [http://msrmaps.com/map.aspx?t=4&s=12&lat=30.6273&lon=-88.0693&w=700&h=1000&lp=---+None+--- Aerial image as of 4 March 2002] from [[USGS]] ''[[The National Map]]''

{{AL Airport}}
{{Mobile, Alabama}}

[[Category:1940 establishments in Alabama]]
[[Category:Airfields of the United States Army Air Corps]]
[[Category:Airfields of the United States Army Air Forces I Troop Carrier Command]]
[[Category:Airfields of the United States Army Air Forces Technical Service Command]]
[[Category:Airfields of the United States Army Air Forces in Alabama]]
[[Category:Airports in Alabama]]
[[Category:Closed facilities of the United States Air Force]]
[[Category:Initial United States Air Force installations]]
[[Category:Transportation in Mobile, Alabama]]
[[Category:Airfields of the United States Army Air Forces Air Transport Command in North America]]
[[Category:1969 disestablishments in Alabama]]
[[Category:Military installations in Alabama]]
[[Category:Buildings and structures in Mobile, Alabama]]