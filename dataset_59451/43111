<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=V 59 an V 61
 | image=LFG V 61.png
 | caption=V 61
}}{{Infobox Aircraft Type
 | type=Passenger transport
 | national origin=[[Germany]]
 | manufacturer=LFG ([[Luft-Fahrzeug-Gesellschaft]])
 | designer=
 | first flight=1926
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=3 (2×V 59 and  1×V 61)
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''LFG V 59''' and the '''LFG V 61''' were single engine, twin float passenger [[seaplane]]s designed and built in [[Germany]] in 1926 by the [[Luft-Fahrzeug-Gesellschaft]]. They differed only in their engines.

==Design and development==
The V&nbsp;59 and V&nbsp;61 were both twin float seaplanes, essentially identical apart from their engines and designed to carry four or five passengers. The V&nbsp;59 was powered by a {{convert|240|hp|kW|abbr=on|0}} [[BMW IV]] 6-cylinder water cooled inline and the V&nbsp;61 by a much more powerful, {{convert|400|hp|kW|abbr=on|0}} [[Bristol Jupiter]] 9-cylinder radial.<ref name=Flight1/> They were metal aircraft both in frame and covering, [[Mid-wing|low wing monoplanes]] of the semi-[[cantilever]] kind with external bracing between the upper [[fuselage]] and wing and further support from below via the flat topped floats. The wings were straight tapered with rounded [[wing tip|tips]].  The fuselage was flat sided and bottomed, with windows down the side and with a braced [[tailplane]] mounted on top of it.  The rudder extended well below the keel.<ref name=Flight1/>
[[File:LFG V 60 and V 61.png|thumb|The  LFG V 61 (foreground) and V 60 (right) at the 1926 German Seaplane Competition]]

==Operational history==
Both the V&nbsp;59 and the V&nbsp;61 were entered into the German Seaplane Competition, held between 12–23 July 1926 with flights along the [[Baltic Sea|Baltic]] and [[North Sea]] coasts from [[Warnemünde]].<ref name=Flight1/> The V&nbsp;59 did not score in the technical tests but the V&nbsp;61 came sixth in them whilst not completing the whole course.<ref name=Flight2/>

==Variants==
;V 59: {{convert|240|hp|kW|abbr=on|0}} [[BMW IV]] 6-cylinder water cooled inline engine. Four passengers.
;V 61: {{convert|400|hp|kW|abbr=on|0}} [[Bristol Jupiter]] 9-cylinder [[radial engine]].  Estimated maximum speed {{convert|185|km/h|mph|abbr=on|0}}. Five passengers.

==Specifications (V 59)==
{{Aircraft specs
|ref=Flight 22 July 1926 pp.448-451<ref name=Flight1/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=2
|capacity=4/5 passengers
|length m=
|length note=
|span m=
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=52
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=1430
|empty weight note=
|gross weight kg=2200
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[BMW IV]]
|eng1 type=6-cylinder inline water cooled 
|eng1 hp=240
|eng1 note=
|power original=
|more power=

|prop blade number=
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=151
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|power/mass=

|more performance=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}
<!--==Notes==-->

==References==
{{commons category|LFG Roland}}
{{reflist|refs=

<ref name=Flight1>{{cite magazine|date=22 July 1926 |title=The German seaplane Championship|magazine=[[Flight International|Flight]]|volume=XVIII|issue=29|pages=448–451|url=http://www.flightglobal.com/pdfarchive/view/1926/1926%20-%200510.html}}</ref>

<ref name=Flight2>{{cite magazine|date=5 August 1926 |title=The German seaplane Championship|magazine=[[Flight International|Flight]]|volume=XVIII|issue=31|page=479|url=http://www.flightglobal.com/pdfarchive/view/1926/1926%20-%200549.html}}</ref>
}}
<!-- ==Further reading== -->
<!-- ==External links== -->
<!-- Navboxes go here -->
{{LFG aircraft}}

[[Category:Seaplanes and flying boats]]
[[Category:German civil aircraft 1920–1929]]
[[Category:LFG aircraft|LFG V 59]]