{{Use dmy dates|date=June 2013}}
{{Use Australian English|date=September 2011}}
{{Infobox person
|name          = Bob Francis
|image         =
|image_size    = 
|caption       = 
|birth_name    = Robert Neville Francis
|birth_date    = {{Birth date|1939|3|11|df=y}}
|birth_place   = [[Cairo]], Egypt
|death_date    = {{death date and age|2016|11|12|1939|3|11|df=y}}
|death_place   = [[North Adelaide]], South Australia
|death_cause   = 
|resting_place = 
|resting_place_coordinates = 
|residence     = 
|nationality   = Australian
|other_names   = 
|known_for     =
|education     = Prince Alfred College
|employer      = [[5AA]]
|occupation    = Talkback radio presenter
|home_town     = Adelaide
|title         = 
|salary        = 
|spouse        = Anna Von Berg (2006–2016; his death)
|children      = 2
|parents       = 
|relations     = 
|signature     = 
|website       = 
|footnotes     = 
}}
'''Robert Neville '''"'''Bob'''"''' Francis''' {{post-nominals|country=AUS|OAM}} (11 March 1939 – 12 November 2016) was an Egyptian-born Australian talk back radio presenter at [[radio station]] [[5AA]] in [[Adelaide]]. His program aired between 8&nbsp;pm and 12 midnight on weekdays and was rated as Adelaide's most popular talk back program.<ref>{{cite news|url=http://www.nielsenmedia.com.au/en/pdf/mri/11/AdelaideSurvey7-2007.pdf |title=Adelaide Radio Summary Report – Survey No. 7, 2007 }}{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>

In July 2013, it was announced that Francis was to retire at the end of that year.<ref name=":0">http://www.adelaidenow.com.au/news/south-australia/veteran-fiveaa-latenight-talkback-radio-host-bob-francis-to-retire-after-57-years/story-fni6uo1m-1226675222975</ref> The following month he announced that he would retire from broadcasting on 8 August 2013 after 57 years. A variety of radio, television, media and music celebrities expressed their appreciation towards his career through taped segments being aired during his last week of broadcast.

==Life and career==

Francis was born in Cairo, Egypt, in 1939.<ref name="ABCD">{{cite news|url=http://www.news.com.au/adelaidenow/story/0,22606,22654962-5006301,00.html|title=I'm a true drink-drive idiot: Big Bob|work=AdelaideNow|date=27 October 2007|accessdate=2007-10-31 | first=Kim | last=Wheatley}}</ref> 

Francis hosted his own radio show on [[5AA]] from 1985 to 2013 and was the station's longest serving employee. He completed 50 years in radio.<ref name="ABCD" /> 

In 1964, at just 25 years of age, while working as a radio DJ, Francis put together a petition of 80,000 signatures which ended up persuading [[the Beatles]] to change their itinerary and visit Adelaide in their 1964 tour. He also introduced the Beatles to Adelaide from the Town Hall balcony in June that year.<ref>{{cite news|url=http://www.abc.net.au/adelaide/stories/s1134250.htm|title=Beatles breakfast: Bringing the Beatles to Adelaide|publisher=891 ABC Adelaide|date=10 June 2004|accessdate=2007-11-02 |archiveurl = https://web.archive.org/web/20070622074035/http://www.abc.net.au/adelaide/stories/s1134250.htm |archivedate = 22 June 2007}}</ref>

Older listeners remember his long-standing on-air partnership with [[Andy Thorpe]] in the late sixties to mid-seventies on radio [[5ADD]].

Until August 2013, Francis hosted his radio program from 8&nbsp;pm to midnight every weekday,commanding between 24 and 28 per cent of the evening radio audience.<ref name="Famous">{{cite news|url=http://www.news.com.au/adelaidenow/story/0,22606,16932044-2682,00.html| title=Famous Francis|work=AdelaideNow|date=15 October 2005|accessdate=2007-10-31}} {{Dead link|date=August 2010|bot=RjwilmsiBot}}</ref>

Francis was made famous to the rest of Australia through the [[ABC Television|ABC TV]] show ''[[The Chaser's War on Everything]]'' when one member of the Chaser team rang him to question 5AA reporters as they were not at the "Subway Fresh Bread" press conference which was actually just a commercial.<ref>{{YouTube|Z5h6TD10axY|Chaser Ad Road Test – Subway<!-- Bot generated title -->}}</ref> Because of his temper, he was a constant target for prank calls, such as one featuring on [[John Safran]]'s show. These clips have appeared on the video sharing site YouTube.<ref>{{YouTube|XbXNuWzcleY|John Safran and Bob Francis<!-- Bot generated title -->}} {{Dead link|date=May 2011}}</ref>

In 2005, Francis was inducted into the Australian Radio Hall of fame at the Australian Commercial Radio Awards held at the Sydney Convention Centre. The award recognises outstanding lifetime achievement and contribution to the radio broadcasting industry. After receiving the award, he said: "This is one of the greatest honours I can imagine, to me like winning an [[Academy Award]]."<ref name=Famous /> He was also presented with a Golden Microphone from 5AA for 20 years service.<ref>{{cite news|url=http://www.commercialradio.com.au/index.cfm?page_id=1265#100242 |title=Rod Muir and Bob Francis inducted to Hall of Fame |publisher=Commercial Radio Australia |date=15 October 2005 |accessdate=2007-10-31 |deadurl=yes |archiveurl=https://web.archive.org/web/20071107200151/http://www.commercialradio.com.au/index.cfm?page_id=1265 |archivedate=7 November 2007 |df=dmy }}</ref>

In July 2013, Francis announced that he would be retiring from radio at the end of the year after talks with 5AA management. He told the ''Sunday Mail'', "I just decided I wasn't really enjoying coming into work each day anymore."<ref name=":0" /> He made his last broadcast, which included many messages from well-wishers and ended with Francis in tears, on 8 August 2013.

Francis died on 12 November 2016 at the age of 77.<ref name="deathref">{{cite news|title='Larger than life' Adelaide radio legend Bob Francis dies aged 77|url=http://www.abc.net.au/news/2016-11-12/adelaide-radio-legend-bob-francis-remembered-after-death-at-77/8020566|accessdate=12 November 2016|work=ABC News|date=12 November 2016|language=en-AU}}</ref>

==Controversies==

One of his most controversial acts, also believed to have been one of the most controversial acts by any Australian talk show radio host, occurred on 27 September 2005 whilst talking to an elderly female listener. Francis spoke arrogantly to her, referring to her as a ''"stupid old lady"'' and repeatedly calling her a ''"dickbrain"''. The way he spoke and treated the elderly listener made headlines, and was even featured on ''[[Media Watch (TV program)|Media Watch]]'' two weeks later.<ref>{{cite news|url=http://www.abc.net.au/mediawatch/transcripts/s1478834.htm|title=MediaWatch Transcript|publisher=MediaWatch|date=10 October 2005|accessdate=2007-10-31}}</ref> Francis refused to say sorry over the incident, saying:
''"I loved it. If it was taken in context, she had a go at me. She was being nasty"'' and that ''"If she rang again, I'd do the same thing"''.
In February 2005, 5AA were found guilty by the country's peak broadcasting regulator, regarding comments made in regard to [[Indigenous Australians|aboriginals]] on Francis' program. It is unknown who made the comments or what the comments were, just that they were likely to have incited or perpetuated hatred against Aboriginal people on the basis of their race. The station issued an apology over the incident.<ref>{{cite news|url=http://www.abc.net.au/news/stories/2005/06/15/1392167.htm|title=Radio station guilty of vilification|work=ABC News|date=16 May 2006|accessdate=2007-11-01}}</ref>
In October 2005, Francis was involved in a legal issue after making inflammatory comments about a senior magistrate, Gary Gumpl. live on-air while commenting on a case involving Robert John Walker who had been charged with one count of possessing child pornography. Francis described Gumpl's decision to hear a bail application as "irresponsible" and made comments such as "Am I here as a normal bloody human being, or do judges live in another world?" and "Oh, smash the judge's face in." He later issued a   public apology.<ref>{{cite news|url=http://www.news.com.au/adelaidenow/story/0,22606,17099878-2682,00.html|title=Bob Francis may be charged with contempt of court|work=AdelaideNow|date=31 October 2005|accessdate=2007-10-31 |archiveurl = https://web.archive.org/web/20060824162311/http://www.news.com.au/adelaidenow/story/0,22606,17099878-2682,00.html |archivedate = 24 August 2006}} </ref>

Despite the apology, Francis had to appear in court over the comments and pleaded guilty to one count of bringing a judicial officer into contempt or lowering his authority.<ref>{{cite news|url=http://www.abc.net.au/news/stories/2006/07/18/1690057.htm|title=Bob Francis guilty over magistrate remarks|work=ABC News|date=18 July 2006|accessdate=2007-10-31}}</ref> In August 2006, Francis was found guilty for the comments he made, being sentenced to jail for nine weeks and fined $20,000 on a suspended sentence on the condition that he enter an 18-month good behaviour bond.<ref>{{cite news|url=http://www.abc.net.au/news/stories/2006/08/25/1723874.htm|title=Broadcaster Francis given suspended sentence|work=ABC News|date=25 August 2006|accessdate=2007-10-31}}</ref>

After being found guilty, Francis and the radio station faced a [[defamation]] lawsuit from Gumpl, the magistrate who Francis' comments were directed at. He accepted an offer of "around $60,000" which was paid by 5AA. Gumpl said that incident had caused him considerable stress and hardship and he was pleased it had been finalised. After the incident, Francis was banned from drinking alcohol on air, as he occasionally took a bottle of [[wine|red wine]] into the studio to drink while taking calls, and could have been partly responsible for his outbursts.<ref>{{cite news|url=http://www.news.com.au/adelaidenow/story/0,22606,17969110-2682,00.html|title=Booze ban for Francis|work=AdelaideNow|date=28 January 2006|accessdate=2007-11-02 |archiveurl = https://web.archive.org/web/20060824162553/http://www.news.com.au/adelaidenow/story/0,22606,17969110-2682,00.html |archivedate = 24 August 2006}} </ref>

In 2007, Francis was involved in an accident in which he broke his leg after falling off his scooter trying to avoid a dog. The accident resulted in surgery and two rods in his legs and a pin in his knee. It was later revealed that he was drink-driving and well over the legal limit at the time of the accident. He later called himself a "bloody idiot" and said that the only reason he was drinking was because his boss offered him an ongoing contract.<ref name="ABCD" />

On 7 June 2012, Francis was suspended for a month by 5AA following comments made about asylum seekers four days earlier. he said "Bugger the boat people, as far as I'm concerned, I hope they bloody drown on their way out here. In my opinion, they are not welcome here." Francis later verbally attacked a journalist who had reported the incident saying " Can you believe that bloody bitch in ''The Australian'' ... Some smart-arse dickhead woman ... wrote me up in the paper this morning." When asked by the paper why he had made the comments he remarked "Because I felt like it" and told them to "Get fucked". He was condemned by fellow 5AA presenters [[Keith Conlon]] and [[Amanda Blair]] over the incident.<ref>{{cite web|url=http://www.theaustralian.com.au/national-affairs/immigration/radio-king-slammed-over-boatpeople-tirade/story-fn9hm1gu-1226388100400/articleID/19481/Default.aspx|title=Radio "king" slammed over boatpeople tirade"|date=8 June 2012|work=The Australian|accessdate=2012-10-06}}</ref><ref>{{cite web|url=http://www.adelaidenow.com.au/ipad/anderson-francis-takes-the-crown/story-fn6br25t-1226390069761/Default.aspx|title=Anderson: Francis takes the crown|date=9 June 2012|work=Adelaide Now|accessdate=2012-10-06}}</ref><ref>{{cite web|url=http://www.mediaspy.org/report/2012/06/09/fiveaas-francis-off-air-after-boat-people-tirade//Default.aspx |title=FIVEaa's Francis off air after "boat people" tirade |date=9 June 2012 |work=Media Spy |accessdate=2012-10-06 }}{{dead link|date=January 2017 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>

==References==
{{reflist}}

{{DEFAULTSORT:Francis, Bob}}
[[Category:1939 births]]
[[Category:2016 deaths]]
[[Category:People from Adelaide]]
[[Category:Australian radio personalities]]
[[Category:Australian talk radio hosts]]
[[Category:Egyptian emigrants to Australia]]
[[Category:Recipients of the Medal of the Order of Australia]]
[[Category:People from Cairo]]