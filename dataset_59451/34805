{{good article}}
{{Infobox NCAA team season
| sport=football
| Year            = 1886
| Team            = Navy Midshipmen
| Conference      = Independent
| ShortConference = Independent
| Record          = 3–3
| ConfRecord      =
| HeadCoach       = None
| HCYear          = 
| Captain         = [[Clarance Stone]]
| StadiumArena    = Unknown
}}
{{1886 college football records}}
The '''1886 Navy Midshipmen football team''' represented the [[United States Naval Academy]] in the [[1886 college football season]]. The team marked the second time that the school played a multiple-game season. The squad was captained by [[halfback (American football)|halfback]] Clarence Stone. The year began with consecutive wins over rivals [[St. John's College (Annapolis/Santa Fe)|St. John's College]] and [[Johns Hopkins Blue Jays|Johns Hopkins]], but then reversed with a loss to the former and a close victory over the latter. The year concluded with [[shutout]] losses to the [[Princeton Tigers football|Princeton]] reserve squad and [[Gallaudet University|Gallaudet]]. The season was the program's longest until [[1890 Navy Midshipmen football team|1890]], when that year's team played seven games.

==Prelude==
According to [[Ellsworth P. Bertholf]]'s biographer C. Douglas Kroll, the first evidence of a form of football at the [[United States Naval Academy]] came in 1857, but the school's cadets lost interest in the game shortly afterward.<ref name="Bertholf">[[#Kroll|Kroll (2002)]], p.&nbsp;14</ref> The Naval Academy fielded [[1879 Navy Midshipmen football team|its first official football program]] in 1879, headed by [[William John Maxwell]]. The team played one match, a 0–0 tie, after which the program went on hiatus for two years. It returned with the [[1882 Navy Midshipmen football team|1882 season]] under the guidance of player-coach [[Vaulx Carter]]. That season and the following two years of football were single-game seasons played against [[Navy–Johns Hopkins football rivalry|rival]] Johns Hopkins; Navy won two of the three years. The [[1885 Navy Midshipmen football team|1885 season]] was the first the school participated in a multi-game year, playing three games, including the first against nearby [[St. John's College (Annapolis/Santa Fe)|St. Johns College]]. The season was a disappointment, ending in a {{Win-loss record|w=1|l=2}} record.<ref name="Bealle 7">[[#Bealle|Bealle (1951)]], pp.&nbsp;7–11</ref><ref name="Clary 10">[[#Clary|Clary (1997)]], pp.&nbsp;10–11</ref>

==Schedule==
{{CFB Schedule Start|time=no|rank=no|ranklink=|rankyear=|tv=no|attend=no}}
{{CFB Schedule Entry
| date         = November ??
| time         = no
| w/l          = w
| nonconf      = 
| rank         = no
| opponent     = {{cfb link|year=1886|school=St. John's College (Annapolis/Santa Fe)|title=St. John's College}}
| site_stadium = Unknown
| site_cityst  = [[Annapolis, Maryland|Annapolis, MD]]
| tv           = no
| score        = 12–0
| gamename     = [[Navy–St. John's College rivalry|Rivalry]]
| attend       = 
}}
{{CFB Schedule Entry
| date         = November 13
| time         = no
| w/l          = w
| nonconf      = 
| rank         = no
| opponent     = {{cfb link|year=1886|team=Johns Hopkins Blue Jays|title=Johns Hopkins}}
| site_stadium = Unknown
| site_cityst  = Annapolis, MD
| tv           = no
| score        = 6–0
| gamename     = [[Navy–Johns Hopkins football rivalry|Rivalry]]
| attend       = 
}}
{{CFB Schedule Entry
| date         = November ??
| time         = no
| w/l          = l
| nonconf      = 
| rank         = no
| opponent     = St. John's College
| site_stadium = Unknown
| site_cityst  = Annapolis, MD
| tv           = no
| score        = 4–0
| gamename     = 
| attend       = 
}}
{{CFB Schedule Entry
| date         = November 25
| time         = no
| w/l          = w
| nonconf      = 
| rank         = no
| opponent     = Johns Hopkins
| site_stadium = Unknown
| site_cityst  = Annapolis, MD
| tv           = no
| score        = 15–14
| gamename     = 
| attend       = 
}}
{{CFB Schedule Entry
| date         = November 27
| time         = no
| w/l          = l
| nonconf      = 
| rank         = no
| opponent     = [[1886 Princeton Tigers football team|Princeton reserve team]]
| site_stadium = Unknown
| site_cityst  = Annapolis, MD
| tv           = no
| score        = 30–0
| attend       = 
}}
{{CFB Schedule Entry
| date         = December ??
| time         = no
| w/l          = l
| nonconf      = 
| rank         = no
| opponent     = {{cfb link|year=1886|team=Gallaudet Bison|title=Gallaudet}}
| site_stadium = Unknown
| site_cityst  = Annapolis, MD
| tv           = no
| score        = 16–0
| attend       = 
}}
{{CFB Schedule End|rank=no|poll=|timezone=|ncg=no|hc=no}}

==Season==
The season began with a game against St. Johns, one of the first contests in what would become a heated rivalry.<ref name="Bealle 30">[[#Bealle|Bealle (1951)]], p.&nbsp;30</ref> Navy won the game with relative ease, 12–0. The following game was against Johns Hopkins, played on November 13. The contest was an irregularity in the schools' rivalry; all previous and most following games were played on [[Thanksgiving Day]], as a part of the Naval Academy's Thanksgiving athletic carnival. Although the score was close, a 6–0 win for the Naval Academy, Hopkins was never a threat to the cadets. After this point, the Academy's luck shifted. The squad was upset 4–0 in a rematch with St. Johns, and barely defeated Johns Hopkins in a 15–14 contest, played as a part of the athletic carnival. The ''[[Baltimore News-American|Baltimore American]]'' covered the Johns Hopkins game in detail:<ref name="Bealle 12">[[#Bealle|Bealle (1951)]], p.&nbsp;12</ref>

: Early in the first half, by much rushing, forcing, snapbacks and vigorous bullyragging Riggs, the huge Hopkins quarterback, crashed over the goal line for 4 points. [[Paul Dashiell]] converted. Riggs repeated his performance but Dashiell missed conversion. Navy then adopted the Hopkins rushing tactics and Stone went over for the first score.

: With Hopkins backed up against her own goal line, Dashiell broke through the entire Navy team for a touchdown. Goal was missed and the score was 14 to 6 against Navy. With the game fast coming to a close The Tars formed a closely knit ball with the halfback in center.

: Navy hit paydirt but the referee found something illegal and called the ball back, much to the consternation of the Cadet rooters. But on the next play George Hayward kicked a field goal, making the score 14 to 11. Just before the game ended a [[Lateral pass|double pass]], Bill Cloke to captain Clarence Stone, carried to ball over the Hopkins goal for the 4 points that won the game.<ref name="Bealle 12"/>

Just two days after the second Hopkins game, on November 27, the Naval Academy challenged the [[1886 Princeton Tigers football team|Princeton Tigers]] reserve squad and was easily [[shutout]], 30–0. The Academy never came close to scoring on the reserves. The Naval Academy hosted [[Gallaudet University#Football|Gallaudet]] in its final game of the season sometime in December, a contest that the visitors won in a shutout, 16–0.<ref name="Bealle 12"/>

==Players==
The 1886 Navy football team was made up of twelve players at five unique positions. The squad consisted of five rushers, one snapback, two fullbacks, two halfbacks, and two quarterbacks. Three of the players (both halfbacks and a rusher) had played the previous season.<ref name="Bealle 12"/> The age of the players ranged several years due to the academy's admission policy; the school allowed for men between the ages of 14 and 18 to be admitted, which future player John B. Patton remarked made it "just a [[Boarding school|boys' school]]".<ref name="Schoor 5">[[#Schoor|Schoor (1967)]], p.&nbsp;5</ref>

{{Col-begin}}
{{Col-3}}
'''[[Lineman (American football)|Rushers]]'''
* Carlo Brittain
* George Hayward
* John Patton 
* Bill Rowan 
* Elliott Snow
{{Col-3}}

'''[[Center (gridiron football)|Snapback]]'''
* Gerge Fermier
* James Alexander

'''[[Halfback (American football)|Halfbacks]]'''
* Bill Cloke 
* [[Clarence Stone]] (capt.) 
{{Col-3}}

'''[[Fullback (American and Canadian football)|Full-backs]]'''
* Henry Allen 
* Lou Anderson

'''[[Quarterback]]s'''
* Creighton Churchill
* William Williams
{{Col-3}}
{{Col-end}}

[[File:James N Alexander Navy Cadet in football team.jpg|thumbnail|1886 Navy Midshipmen Football Team]]

==Postseason and aftermath==
The first postseason college football game would not be played until 1902, with the [[Pasadena Tournament of Roses]]' establishment of the [[1902 Rose Bowl|Tournament East-West football game]], later known as the [[Rose Bowl Game|Rose Bowl]].<ref name="Rose Bowl">{{cite web |author=Staff |publisher=[[Pasadena Tournament of Roses]] |url=http://www.tournamentofroses.com/history/ |title=Tournament of Roses History |work=Pasadena Tournament of Roses |year=2005 |accessdate=February 10, 2015 |archiveurl=https://web.archive.org/web/20070102063138/http://www.tournamentofroses.com/history/ |archivedate=January 2, 2007}}</ref> The Midshipmen would not participate in their first Rose Bowl until the [[1924 Rose Bowl|1923 season]], when they went {{Win-loss record|w=5|l=1|d=2}} and tied with the [[Washington Huskies football|Washington Huskies]] 14–14 in the match.<ref name="1924 Game Results">{{cite news |last=Eckersall |first=Walter |newspaper=[[The Detroit Free Press]] |date=January 2, 1924 |title=Annual East–West Football Battle Ends In 14–14 Tie |page=16 |ISSN=1055-2758}}</ref> As a result of the lack of competition, there were no postseason games played after the 1886 season. According to statistics compiled by Billingsly, Houlgate, the National Championship Foundation, [[Parke H. Davis]], and the [[Helms Athletic Foundation]], Princeton and Yale were declared the 1886 season national co-champions.<ref name="Page 78">{{cite book|year=2009 |url=http://fs.ncaa.org/Docs/stats/football_records/DI/2009/2009FBS.pdf |chapter=National Poll Champions |title=Football Bowl Subdivision Records |publisher=[[National Collegiate Athletic Association]] |series=2009 NCAA Division I Football Records |accessdate=February 16, 2015 |format=PDF |page=78 |type=Record book |ref=All-Time}}</ref>

Paul Dashiell, the Johns Hopkins player who nearly single-handedly beat the Naval Academy, later served as the team's head coach, leading the program to a 25–5–4 record between 1904 and 1906.<ref name="Patterson 4">[[#Patterson|Patterson (2000)]], p.&nbsp;4</ref> The 1886 season kept Navy's overall win–loss record at an even 6–6–1. It also brought the Academy's record against Johns Hopkins to a 4–2 lead, from which Hopkins never recovered. The season marked the first time a team for the Naval Academy would play a multiple-game season. The 1886 schedule was the longest for the Naval Academy until 1891, when that year's squad played seven matches. Navy finished the 1880s with four winning seasons, and an overall record of 14–12–2. The school outscored their opponents 292–231, and would go on to finish the 19th century with an overall record of 54–19–3.<ref name="Bealle 12"/><ref name="Page 154">{{cite web |author=Naval Academy Athletic Association |publisher=United States Naval Academy Athletics |url=http://graphics.fansonly.com/photos/schools/navy/sports/m-footbl/auto_pdf/1935_05-FB-MG---9---History-15.pdf |title=Navy: Football History |format=PDF |work=2005 Navy Midshipmen Football Media Guide |year=2005 |page=154 |accessdate=February 10, 2014}}</ref>

==References==
'''Footnotes'''
{{reflist|30em}}
'''Bibliography''' 
{{refbegin}}
* {{cite book |ref=Bealle |last=Bealle |first=Morris Allson |year=1951 |title=Gangway for Navy: The Story of Football At United States Naval Academy, 1879–1950 |chapter=1886 |location=[[Washington, D.C.]] |publisher=Columbia Publishing Company |OCLC=1667386}}
* {{cite book |ref=Clary |last=Clary |first=Jack |year=1997 |chapter=The Tradition Begins: 1879–1899 |title=Navy Football: Gridiron Legends and Fighting Heroes |location=[[Annapolis, MD]] |publisher=[[Naval Institute Press]] |isbn=1-55750-106-8 |OCLC=36713133}}
* {{cite book |ref=Kroll |last=Kroll |first=C. Douglas |year=2002 |title=Commodore Ellsworth P. Bertholf: First Commandant of the Coast Guard |location=[[Annapolis, MD]] |publisher=[[Naval Institute Press]] |chapter=The Cadet Years |isbn=1-55750-474-1}}
* {{cite book |ref=Patterson |last=Patterson |first=Ted |year=2000 |title=Football in Baltimore: History and Memorabilia |location=[[Baltimore, Maryland|Baltimore, MD]] |publisher=[[Johns Hopkins University Press|The Johns Hopkins University Press]] |isbn=0-8018-6424-0}}
* {{cite book |ref=Schoor |last=Schoor |first=Gene |chapter=Football Comes to Annapolis and West Point |editor=Schoor, Gene |year=1967 |title=The Army-Navy Game: A Treasury of the Football Classic |location=New York City |publisher=[[Dodd, Mead and Company]] |pages=3–9 |OCLC=371756}}
{{refend}}

{{Navy Midshipmen football navbox}}

[[Category:1886 college football season|Navy]]
[[Category:Navy Midshipmen football seasons]]
[[Category:1886 in Maryland|Navy Midshipmen]]