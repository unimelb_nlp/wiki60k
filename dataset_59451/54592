{{no footnotes|date=June 2014}}
{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
[[File:Mazeppa 1st edition 1819.jpg|thumb|"Fragment of a Novel" first appeared in the ''Mazeppa'' collection, John Murray, London, 1819.]]"'''Fragment of a Novel'''" is an unfinished 1819 vampire horror story written by [[Lord Byron]]. The story, also known as "A Fragment" and "The Burial: A Fragment", was one of the first in English to feature a vampire theme. The main character was Augustus Darvell. [[John William Polidori]] based his novella ''[[The Vampyre]]'' (1819) on the Byron fragment. The vampire in the Polidori story, Lord Ruthven, was modelled on Byron himself. The story was the result of the meeting that Byron had in 1816 with [[Percy Bysshe Shelley]] where a "ghost writing" contest was proposed. This contest was also what led to the creation of [[Mary Shelley]]'s ''[[Frankenstein]]''. The story is important in the development and evolution of the vampire story in English literature. The short story first appeared under the title "A Fragment" in the 1819 collection ''[[Mazeppa (Byron)|Mazeppa]]: A Poem'', published by John Murray in London.

==Plot==
The story is written in an epistolary form with the narrator recounting the events that had occurred in a letter. The narrator embarks on a journey or "Grand Tour" to the East with an elderly man, Augustus Darvell. During the journey, Darvell becomes physically weaker, "daily more enfeebled". They both arrive at a Turkish cemetery between Smyrna and Ephesus near the columns of Diana. Near death, Darvell reaches a pact with the narrator not to reveal his impending death to anyone. A stork appears in the cemetery with a snake in its mouth. After Darvell dies, the narrator is shocked to see that his face turns black and his body rapidly decomposes:

"I was shocked with the sudden certainty which could not be mistaken — his countenance in a few minutes became nearly black. I should have attributed so rapid a change to poison, had I not been aware that he had no opportunity of receiving it unperceived."

Darvell is buried in the Turkish cemetery by the narrator. The narrator's reaction was stoical: "I was tearless." According to John Polidori, Byron intended to have Darvell reappear, alive again, as a vampire, but did not finish the story. Polidori's account of Byron's story indicates it "depended for interest upon the circumstances of two friends leaving England, and one dying in Greece, the other finding him alive upon his return, and making love to his sister."

==Major characters==

* The Narrator, recounts the story in a letter
* Augustus Darvell, a wealthy, elderly man
* Suleiman, a Turkish [[janizary]]

==Publication history==

Byron's unfinished short story, also known as "A Fragment" and "The Burial: A Fragment", was appended to ''Mazeppa'' by the publisher John Murray in June 1819 in London without the approval of Byron. On 20 March 1820, Byron wrote to Murray: "I shall not allow you to play the tricks you did last year with the prose you post-scribed to 'Mazeppa,' which I sent to you not to be published, if not in a periodical paper, – and there you tacked it, without a word of explanation, and be damned to you.".

==Byron on Vampirism==

Byron wrote about his views on vampires and vampirism, a theme that also appeared in ''[[The Giaour]]: A Fragment of a Turkish Tale'':

<blockquote>The Vampire superstition is still general in the Levant. Honest Tournefort tells a long story about these '[[Vrykolakas|Vroucolachas]]', as he calls them. The Romaic term is 'Vardoulacha'. I recollect a whole family being terrified by the scream of a child, which they imagined must proceed from such a visitation. The Greeks never mention the word without horror. I find that 'Broucolokas' is an old legitimate Hellenic appellation—the moderns, however, use the word I mention. The stories told in Hungary and Greece of these foul feeders are singular, and some of them most incredibly attested. ... I have a personal dislike to Vampires, and the little acquaintance I have with them would by no means induce me to reveal their secrets.</blockquote>

==References==
* Christopher Frayling, editor, with introduction. ''Vampyres: Lord Byron to Count Dracula''. Faber and Faber, 1992. ISBN 0-571-16792-6
* John Clute and John Grant, editors. ''The Encyclopedia of Fantasy''. Orbit, 1997, p.&nbsp;154.
* Emmanuel Carrère. ''Gothic Romance'' (Original French title: ''Bravoure''). (1984). English version published by Scribner, 1990. ISBN 0-684-19199-7
* Ken Gelder. ''Reading the Vampire''. NY: Routledge, 1994.
* Seed, David. '"The Platitude of Prose": Byron's Vampire Fragment in the Context of his Verse Narratives', in Bernard Beatty and Vincent Newey, eds., ''Byron and the Limits of Fiction'', Liverpool: Liverpool U.P., 1988, pp.&nbsp;126–147.
* [http://www.praxxis.co.uk/credebyron/vampyre.htm The Byronic Vampyre.]
* [https://web.archive.org/web/20110726075918/http://www.litgothic.com/Texts/giaour_frag.html A vampiric extract from ''The Giaour: A Fragment of a Turkish Tale'' (1813).]
* Barber, Paul. ''Vampires, Burial, and Death: Folklore and Reality.'' Yale University Press, 1988.
* Tournefort, Joseph Pitton de. ''A Voyage Into the Levant'', 1718. English edition, London: printed for D. Midwinter. Volume I, 1741.
* Bainbridge, S. "Lord Ruthven's Power: Polidori's 'The Vampyre', Doubles and the Byronic Imagination." ''The Byron Journal'', 2006.
* Skarda, Patricia. “Vampirism and Plagiarism: Byron’s Influence and Polidori’s Practice.” ''Studies in Romanticism'', 28 (Summer 1989): 249–69.
* McGinley, Kathryn. "Development of the Byronic Vampire: Byron, Stoker, Rice." ''The Gothic World of Anne Rice'', edited by Ray Broadus Browne and Gary Hoppenstand. Bowling Green State University Popular Press, 1996.
* Lovecraft, H. P. "Supernatural Horror in Literature." ''The Recluse'', No. 1 (1927), pp.&nbsp;23–59.

==External links==
* [http://www.sff.net/people/DoyleMacdonald/l_frag.htm Online edition of "Fragment of a Novel".]
* [https://archive.org/details/short_ghost_horror_12_1102 Audiorecording by Amy Gramour on the LibriVox website, selection 1, "The Burial: A Fragment" by George Gordon Lord Byron.]
* [https://archive.org/details/mazeppaapoem02byrogoog Online version of 1819 collection ''Mazeppa'' with short story "A Fragment".]
* [http://strangehorizons.com/2003/20031201/byron.shtml "Le Vampyre, the Gothic Novel, and George Gordon, Lord Byron", 2003.]
* [http://news.bbc.co.uk/local/nottingham/hi/people_and_places/history/newsid_8517000/8517132.stm BBC: "Lord Byron's image inspired modern take on vampires", 2010.]

{{Byron}}

{{DEFAULTSORT:Fragment Of A Novel}}
[[Category:Works by Lord Byron]]
[[Category:Horror short stories]]
[[Category:1819 short stories]]
[[Category:1800s short stories]]
[[Category:Horror novels]]
[[Category:Vampire novels]]
[[Category:Horror genres in literature]]
[[Category:Vampires in written fiction|Novels]]
[[Category:Unfinished novels]]