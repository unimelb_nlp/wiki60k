{{Infobox journal
| title = Leukemia
| cover = [[File:Leukemia cover.gif]]
| discipline = [[Hematology]], [[oncology]]
| abbreviation = Leukemia
| publisher = [[Nature Publishing Group]]
| country =
| frequency = Monthly
| history = 1987-present
| openaccess =
| license =
| impact = 12.104
| impact-year = 2015
| website = http://www.nature.com/leu/index.html
| link1 =
| link1-name =
| link2 = http://www.nature.com/leu/archive/index.html
| link2-name = Online archive
| editors = Andreas Hochhaus and Robert Peter Gale
| formernames =
| JSTOR =
| OCLC = 485729480
| LCCN =
| CODEN =
| ISSN = 0887-6924
| eISSN = 1476-5551
}}
'''''Leukemia''''' is a [[Peer review|peer-reviewed]] [[medical journal]] published by the [[Nature Publishing Group]]. It was established in 1987 by [[Nicole Muller-Bérat Killman]] and [[Sven-Aage Killman]], and is currently edited by Professors Andreas Hochhaus and Robert Peter Gale. The journal covers research on all aspects of [[leukemia]] and has a 2015 impact factor of 12.104.

==Abstracting and indexing==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
*[[Elsevier BIOBASE]]/[[Current Awareness in Biological Sciences]]
*[[BIOSIS Previews]]
*[[CSA (database company)|Cambridge Scientific Abstracts]]
*[[Current Contents]]/Life Sciences
*[[EMBASE|EMBASE/Excerpta Medica]]
*[[MEDLINE]]/[[Index Medicus]]
*[[Science Citation Index]]
}}
According to the ''[[Journal Citation Reports]]'', the journal had a 2015 [[impact factor]] of 12.104.<ref name=WoS>{{cite book |year=2015 |chapter=JOURNALNAME |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref> In the past, the journal has been accused of practicing [[coercive citation]].<ref name = smith>{{cite journal |doi=10.1136/bmj.314.7079.461d |title=Journal accused of manipulating impact factor |year=1997 |last1=Smith |first1=R. |journal=[[BMJ]] |volume=314 |issue=7079 |page=461 |pmid=9056791 |pmc=2125988}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.nature.com/leu/index.html}}

[[Category:Oncology journals]]
[[Category:Nature Publishing Group academic journals]]
[[Category:Publications established in 1987]]
[[Category:English-language journals]]
[[Category:Monthly journals]]
[[Category:Hematology journals]]