{{Infobox criminal
| name   = Nicole Abusharif
| birth_date  = {{birth date and age|mf=y|1980|09|14}}
| conviction     = [[Murder]]
| conviction_penalty        = 50 years
| conviction_status         = [[Incarceration|Incarcerated]] at [[Dwight Correctional Center]]
| spouse = Rebecca Klein (2001-2007; deceased)
}}
'''Nicole Abusharif''' (born September 14, 1980) is an [[United States|American]] woman, convicted of the 2007 [[Villa Park, Illinois]] murder of her [[domestic partner]], Rebecca "Becky" Klein. After being found [[guilt (law)|guilty]] of [[first-degree murder]] in May 2009, Abusharif was sentenced to 50 years in prison at the [[Dwight Correctional Center]] in [[Nevada Township, Illinois]]. The case made national news because of the intrigue of a "lesbian [[love triangle]]" murder.

==Murder and investigation==
On March 17, 2007, the body of Klein was discovered in the trunk of her 1966 [[Ford Mustang]], after Klein's [[domestic partner]] of seven years, Abusharif, reported her missing. Klein was found [[bound up|bound]] with [[duct tape]], gagged with a [[bandana]], [[blindfold]]ed, and [[suffocation|suffocated]] with a [[plastic bag]] over her head.<ref name=name1>{{cite web|url=http://www.dupageco.org/States_Attorney/States_Attorney_News/2009/30230 |title=DuPage County IL Official Website - Nicole Abusharif Sentenced to Fifty Years for Murder of Rebecca Klein |publisher=Dupageco.org |date= |accessdate=2012-08-18}}</ref> Four days later, Abusharif was charged with [[first-degree murder]] and concealing of a homicide.<ref name=name2>{{cite web|url=http://www.state.il.us/court/R23_Orders/AppellateCourt/2011/2ndDistrict/March/2090799_R23.pdf|title=Appeal Court Order|publisher=State.il.us|format=PDF|accessdate=11 October 2014}}</ref>

[[Villa Park, Illinois|Villa Park]] police officers believe that Abusharif killed Klein on March 15, 2007, two days before her body was found. After Abusharif allegedly suffocated Klein to death, she went out with another woman, the 19-year-old Rose Sodaro, she met on the social networking site [[MySpace]].<ref name=name3>{{cite web|author=Materville Studios - Host of Windy City Times |url=http://www.windycitymediagroup.com/gay/lesbian/news/ARTICLE.php?AID=21143 |title=Murder in Villa Park: Love and lies - 5041 - Gay Lesbian Bi Trans News - Windy City Times |publisher=Windycitymediagroup.com |date= |accessdate=2012-08-18}}</ref> That night, Abusharif and Sodaro went [[bowling]] in [[Tinley Park, Illinois|Tinley Park]] and returned to Abusharif's home, where they engaged in sex. Sodaro believed that Klein was Abusharif's [[roommate]], not her [[life partner]].<ref name=name3 />

In addition to her relationship with Sodaro, police also believe that Abusharif was motivated to kill Klein by a $400,000 insurance payout. [[Forensic scientist]]s found [[fingerprint]]s on the [[duct tape]] and [[plastic bag]] belonging to Abusharif and her [[DNA]] on the [[bandana]]. Prosecutors also uncovered a slew of lies that Abusharif allegedly told, including stating to Sodaro that she had been a [[New York City]] [[firefighter]] during the [[September 11, 2001 attacks]].<ref name=name4>{{cite web|url=http://prev.dailyherald.com/story/?id=290786 |title=Daily Herald &#124; Woman denies killing lover in Villa Park murder |publisher=Prev.dailyherald.com |date= |accessdate=2012-08-18}}</ref> Abusharif, who was also an [[alcoholic]], claimed to have [[liver cancer]] and told Sodaro that her [[alcoholism]] would lead to her death. Abusharif also brought Sodaro to a [[funeral home]], where she dramatically selected her own [[casket]].<ref>{{cite web|author=Materville Studios - Host of Windy City Times |url=http://www.windycitymediagroup.com/gay/lesbian/news/ARTICLE.php?AID=21143 |title=Murder in Villa Park: Love and lies - 5042 - Gay Lesbian Bi Trans News - Windy City Times |publisher=Windycitymediagroup.com |date= |accessdate=2012-08-18}}</ref>

During the police's investigation into the murder of Klein, Abusharif's coworker at a [[Des Plaines, Illinois|Des Plaines]] security company, Robert L. Edwards, was charged with five counts of [[obstructing justice]] for allegedly lying about his whereabouts when Klein was believed to have been murdered.<ref>{{cite web|url=http://articles.chicagotribune.com/2007-09-06/news/0709050932_1_villa-park-obstruction-new-indictments |title=Man faces new obstruction charges in Villa Park woman's death - Chicago Tribune |publisher=Articles.chicagotribune.com |date=2007-09-06 |accessdate=2012-08-18}}</ref> Police focused on Edwards during their initial investigation because he was at Abusharif and Klein's residence on March 16, 2007, during the search for Klein. He later admitted to police that he and Abusharif were "drug buddies who shared wild sex fantasies."<ref name=name6>{{cite web|url=https://prev.dailyherald.com/story/?id=248677 |title=Daily Herald &#124; Jurors see police video |publisher=Prev.dailyherald.com |date= |accessdate=2012-08-18}}</ref> Initially Edwards was on a $1 million [[bail]], which was later reduced to $500,000.<ref name=name7>{{cite web|url=http://articles.chicagotribune.com/2007-04-05/news/0704040714_1_villa-park-charges-computer |title=Bail halved for man tied to Villa Park case - Chicago Tribune |publisher=Articles.chicagotribune.com |date=2007-04-05 |accessdate=2012-08-18}}</ref>

After being [[indictment|indicted]] on [[first-degree murder]] charges, Abusharif was held at the [[DuPage County, Illinois|DuPage County]] Jail on $3 million bond, later lowered to $1 million. After bonding out of jail, Abusharif was put on [[home confinement]] in her [[Oak Lawn, Illinois|Oak Lawn]] apartment. However, on April 25, 2008, Abusharif violated her bail by leaving her apartment to visit a family member's home next door.<ref name=name8>{{cite web|author=|url=http://articles.chicagotribune.com/2008-05-06/news/0805050482_1_villa-park-roommate-ordered |title=Former Villa Park woman accused of roommate's murder ordered held in DuPage jail - Chicago Tribune |publisher=Articles.chicagotribune.com |date=2008-05-06 |accessdate=2012-08-18}}</ref>

Although Assistant State's Attorney Joseph Ruggiero applied to have Abusharif's bail revoked, her bail was only increased by $100,000.<ref name=name9>{{cite web|author=|url=http://articles.chicagotribune.com/2008-05-08/news/0805070983_1_villa-park-slaying-home-confinement |title=Woman's bail increased in Villa Park slaying - Chicago Tribune |publisher=Articles.chicagotribune.com |date=2008-05-08 |accessdate=2012-08-18}}</ref> She returned to county jail but was back out on bail before her trial began.

==Trials==
In November 2008, Edwards went to trial on charges of [[obstructing justice]]. [[Villa Park, Illinois|Villa Park]] police still believed he was not involved in Becky Klein's murder,<ref name=name6 /> but he was convicted of the obstruction charges and was sentenced to 75 days at the [[DuPage County, Illinois]] [[work camp]].{{citation needed|date=August 2012}}

Abusharif's [[jury trial]] commenced on April 20, 2009. Edwards did not testify for the [[prosecution]], but Sodaro did so and was joined by many members of Klein's family.<ref name=name2 /> Abusharif also testified in her own defense. When confronted with the evidence against her, she admitted lying during the police investigation. The defense hinged on whether Abusharif would have been physically able to kill Klein. Abusharif's [[defense attorney]]s, Bob Parchem and Dennis Sopata, maintained that Abusharif had a bad back and would not have been able to subdue Klein, who weighed 40 pounds more than she did.<ref name=name3 /> While Abusharif's attorneys were skeptical of gaining an [[acquittal]], they were able to prove that Klein's murder was not "cold, calculated and premeditated," as the prosecution requested. That eliminated the possibility for a sentence of [[life in prison without parole]].

==Verdict and aftermath==
On May 5, 2009, Abusharif was convicted of [[first-degree murder]] in the death of Klein after 13 hours of [[jury deliberation]]. She faced up to 60 years in prison, but Judge John Kinsella sentenced Abusharif to 50 years of incarceration. She will have to serve all of her sentence before being eligible for parole,<ref name=name1 /> at the age of 76.

Abusharif's conviction was [[affirmed in law|affirmed]] by the [[Illinois Appellate Court#Second District|Second District of the Illinois Appellate Court]] on March 4, 2011.<ref name=name2 />

== References ==
{{Reflist}}

{{DEFAULTSORT:Abusharif, Nicole}}
[[Category:1980 births]]
[[Category:American female murderers]]
[[Category:American people convicted of murder]]
[[Category:Lesbians]]
[[Category:Violence against women]]
[[Category:Living people]]
[[Category:People convicted of murder by Illinois]]
[[Category:Place of birth missing (living people)]]
[[Category:People from Villa Park, Illinois]]