{{Use dmy dates|date=January 2017}}
{{Use Australian English|date=January 2017}}
{{Good article}}
{{Infobox military unit
|unit_name=14th/32nd Battalion
|image=[[File:14 32nd Battalion (Australia) wounded New Britain March 1945.jpg|250px|alt=A man with a bandage around his head, smoking a cigarette beside another man with his right arm in a sling]]
|caption=14th/32nd Battalion wounded await evacuation from the battle zone, March 1945
|dates= 1942–45
|country= Australia
|allegiance=
|branch=[[Australian Army]]
|type=[[Infantry]]
|role=
|size=~800–900 men{{#tag:ref|At the start of the war, the normal size of an Australian infantry battalion was 910 men all ranks, however, following the reorganisation of the 5th Division along the [[Jungle division|jungle establishment]], the size dropped to 803 men all ranks.<ref>{{harvnb|Palazzo|2004|p= 94}}.</ref>|group=Note}}
|command_structure=[[6th Brigade (Australia)|6th Brigade]], [[5th Division (Australia)|5th Division]]
|garrison=
|garrison_label=
|nickname=Prahran/Footscray Regiment
|motto=
|colors=Yellow over Blue
|colors_label=Colours
|march=
|mascot=
|equipment=
|equipment_label=
|battles=[[Second World War]]
* [[New Guinea campaign]]
* [[New Britain campaign]]
|anniversaries=
|decorations=
|battle_honours=
|disbanded=
<!-- Commanders -->
|commander1=
|commander1_label=
|commander2=
|commander2_label=
|commander3=
|commander3_label=
|notable_commanders=
<!-- Insignia -->
|identification_symbol=[[File:14th Battalion AIF Unit Colour Patch.PNG|70px|alt=A two toned rectangle]]
|identification_symbol_label=[[Unit Colour Patch]]
}}
The '''14th/32nd Battalion''' was an [[infantry]] [[battalion]] of the [[Australian Army]] which served during the [[Second World War]]. It was formed in September 1942 by the amalgamation of the [[14th Battalion (Australia)|14th]] and [[32nd Battalion (Australia)|32nd Battalions]] and was assigned to the [[6th Brigade (Australia)|6th Brigade]], [[4th Division (Australia)|4th Division]] in [[Geraldton]], [[Western Australia]]. The battalion served firstly in Australia and then later [[New Guinea]], being employed mainly on garrison duties, before being transferred to the [[5th Division (Australia)|5th Division]] and deployed to [[New Britain]] late in the war where it took part briefly in the [[New Britain campaign|Australian containment campaign]] on that island. In April 1945 it returned to Australia  for rest and re-organisation and was disbanded later that year without seeing further combat.
{{TOC limit|2}}

==History==

===Formation===
By mid-1942, an over [[Military history of Australia during World War II|mobilisation]] of Australia's military forces resulted in a manpower shortage in the Australian economy. In order to rectify this situation, the Australian government decided to amalgamate and disband a number of units from the [[Australian Army Reserve|Militia]] in order to free up their personnel and return them to the civilian workforce.<ref>{{harvnb|Grey|2008|p=184}}.</ref> As a result, on 12 October 1942, the [[14th Battalion (Australia)|14th]] and [[32nd Battalion (Australia)|32nd Battalions]], which had been undertaking garrison duties around [[Geraldton, Western Australia|Geraldton]], [[Western Australia]], were amalgamated to form the 14th/32nd Battalion.<ref name=Bilney42>{{harvnb|Bilney| 1994| p=42}}.</ref><ref>{{harvnb|Johnston|2007|p=8}}.</ref> Upon formation the battalion adopted the territorial title of the "Prahran/Footscray Regiment" and was assigned to the [[6th Brigade (Australia)|6th Brigade]], [[4th Division (Australia)|4th Division]].<ref name=AWM>{{cite web|url=http://www.awm.gov.au/units/unit_11307.asp|title=14th/32nd Battalion |work=Second World War, 1939&ndash;1945 units |publisher=Australian War Memorial|accessdate=15 January 2010}}</ref>

The battalion's personnel were drawn predominately from the 14th Battalion, although 164 officers and men from the 32nd who had volunteered to serve under the same conditions of the [[Second Australian Imperial Force]], were transferred to the 14th/32nd at this time.<ref name=Bilney42/> As more than 65 per cent of the battalion's 996 personnel volunteeredfor overseas service,<ref name=Bilney42/>  the battalion was designated as an Australian Imperial Force battalion, meaning that it could be sent outside the territorial limits imposed by the ''Defence Act (1903)''.<ref>{{harvnb|Johnston|2007|p=9}}.</ref>

===New Guinea and New Britain===
After this, the 14th/32nd Battalion was moved to the [[Atherton Tablelands]] in [[Queensland]], to undertake amphibious landing and jungle warfare training in preparation for deployment overseas.<ref name=AWM/> In July 1943, they were sent to [[Port Moresby]], [[New Guinea]], where they once again filled the role of garrison troops for almost a year, operating between [[Buna, Papua New Guinea|Buna]], [[Gona]] and [[Nassau Bay, Papua New Guinea|Nassau Bay]].<ref name=AWM/> In 1944, under the command of Lieutenant Colonel William Caldwell, who had previously commanded at [[Company (military unit)|company]]-level in [[Battle of Greece|Greece]] and the [[Syria-Lebanon Campaign|Middle East]],<ref>{{harvnb|Long|1963|p=248}}.</ref> the battalion was attached to the [[5th Division (Australia)|5th Division]] and on 4 November the 14th/32nd Battalion, forming the vanguard of the 6th Brigade, carried out an amphibious [[landing at Jacquinot Bay]] on [[New Britain]].<ref name=AWM/>
[[File:Australian soldiers disembarking from a US Army landing craft at Jacquinot Bay on 4 November 1944.JPG|thumb|left|Soldiers from the 14th/32nd Battalion disembarking from a US Army landing craft during the landing at Jacquinot Bay on 4 November 1944]]

Although opposition to the landing was expected, the 14th/32nd Battalion arrived on the beach and found that the Japanese defenders had withdrawn further down the coast.<ref>{{harvnb|Bilney|1994|p=119}}.</ref><ref>{{harvnb|Long|1963|p=250}}.</ref> This allowed the battalion to concentrate on the task of establishing a beachhead and unloading stores. This task was made more difficult though by a torrential downpour on 6 November, nevertheless they were eventually able to establish themselves around Pal Mal Mal Plantation and patrols were sent out to locate the Japanese.<ref>{{harvnb|Bilney| 1994|p=120}}.</ref> By 10 November, patrols reached the Esis River and after contacting locals it was found that the Japanese were withdrawing towards the [[Gazelle Peninsula]], where it was estimated that about 93,000 Japanese were stationed.<ref name=Bilney124>{{harvnb|Bilney|1994|p=124}}.</ref><ref>{{harvnb|Long|1963|p=241}}.</ref> Due to the size of the Japanese garrison, as the rest of the 5th Division began to arrive on the island, it was decided that the Australians would undertake a campaign of containment rather than engage in offensive operations. As a part of this campaign, the 14th/32nd Battalion was assigned the task of advancing {{convert|75|mi|km}} along the coast from Jacquinot Bay to Henry Reid Bay. With no roads and only narrow tracks, the going was difficult and in order to improve mobility the battalion used barges to carry out a series of landings as they advanced through Sampun and Lampun, passing around Wide Bay, before reaching Kalai Plantation in mid-February 1945.<ref>{{harvnb|Bilney|1994|p=132}}.</ref>

[[File:Infantry wide bay (AWM 078376).jpg|thumb|right|Troops from the 14th/32nd Battalion at Wide Bay, on New Britain, January 1945 |alt=Soldiers wearing slouch hats and carrying rifles march past huts in a jungle clearing]]

At Kalai the battalion moved into the Kamandran Mission where they received intelligence reports of a force of about 200 Japanese occupying a position at Gogbulo creek. Fighting patrols were sent out, but no contact was made, although evidence of occupation was found indicating that the Japanese were moving back to a defensive position to the north of Mavelo river.<ref>{{harvnb|Bilney|1994|p=148}}.</ref> Before they could launch an assault, however, the battalion was relieved by the [[19th Battalion (Australia)|19th Battalion]] and on 28 February the 19th crossed the Mavelo. Following this the Japanese withdrew back towards the mountains around the Waitavalo and Tol Plantations,<ref>{{harvnb|Bilney|1994|pp=148–149}}.</ref> and in early March an attempt was made to force a crossing on the Wulwut river. After initially being turned back by intense Japanese mortar and machine-gun fire, the 19th made a successful second attempt,<ref>{{harvnb|Bilney|1994|pp=150–151}}.</ref> forcing the Japanese back further towards Mount Sugi and [[Battle of Bacon Hill|Bacon Hill]].<ref name=Bilney153>{{harvnb|Bilney|1994|p=153}}.</ref>

As orders were passed for an attack to be made on this position, the 14th/32nd was brought up to relieve the 19th.<ref name=Bilney153/><ref>{{harvnb|Long|1963|p=256}}.</ref> The Japanese position sat atop a {{convert|600|ft|m|adj=on}} hill which rose steeply, up to 45 degrees in some places, from the jungle below.<ref>{{harvnb|Bilney|1994|p=169}}.</ref> In preparation, the 14th/32nd occupied a number of positions around Bacon Hill and at 9:55&nbsp;am on 16 March 1945 the attack began with two companies, 'B' and 'D', attacking under the cover of an intense artillery barrage provided by elements of the 2/14th Field Regiment, with direct fire support being provided by 'A' and 'C' Companies from the high ground surrounding the hill. The Japanese defences were well-prepared though, and machine-gun and mortar fire from well-sited positions soon pinned down two [[platoon]]s from 'B' Company.<ref name=Bilney155>{{harvnb|Bilney|1994|p=155}}.</ref> In order to regain momentum, a flanking move to the left was undertaken by 'D' Company, but after two hours they too came under intense machine-gun fire as they attempted to round Mount Sugi and attack the Japanese from the rear, forcing them to dig-in to the north of Bacon Hill.<ref name=Long259>{{harvnb|Long|1963|p=259}}.</ref> At this point the situation became quite desperate for the attackers, and a number of Australian platoons found themselves embroiled in an intense battle. Finally, late in the day, after having lost 10 men killed and another 13 wounded the attack was called off.<ref>{{harvnb|Bilney|1994|pp=156–158}}.</ref> The attack was resumed early the following day with two companies attacking from the north and digging-in on the eastern approaches to the hill while two platoons clambered up the steep western slope amidst heavy Japanese mortar and grenade attacks. By nightfall the Australians, who had lost a further six men killed and 17 wounded, were in control of most of the position, although two Japanese positions were still holding out.<ref name=Long259/> On 18 March the attack was resumed and finally, by 3:00&nbsp;pm, the last Japanese had been cleared from the hill.<ref>{{harvnb|Bilney|1994|p=163}}.</ref><ref name=Long260>{{harvnb|Long|1963|p= 260}}.</ref>

Following the battle, the battalion continued patrolling operations beyond the Waitavalo–Tol area but no further contact was made. On 21 March when they were relieved once more by the 19th Battalion.<ref name=Long260/> A week later, on 28 March, the [[13th Brigade (Australia)|13th Brigade]] began to relieve the units of the 6th Brigade, and over the following fortnight the transition took place.<ref>{{harvnb|Bilney|1994|p=176}}.</ref> On 12 April the battalion returned to Kalai where they received the news that they were being returned to Australian for rest and re-organisation in preparation for further operations. On 7 May 1945 they embarked upon the troopship [[MV Duntroon|''Duntroon'']], arriving in [[Brisbane, Queensland|Brisbane]] a week later.<ref>{{harvnb|Bilney|1994|p=178}}.</ref>

===Disbandment===
With the war seen to be coming to a close, the Army decided to disband the 6th Brigade and its component units, and as a result, the 14th/32nd was disbanded on 21 July 1945,<ref>{{harvnb|Festberg|1972|pp=75 & 92}}</ref> while at [[Loganlea, Queensland|Loganlea]].<ref name=AWM/><ref>{{harvnb|Bilney|1994|p=181}}.</ref> During the war the battalion lost 31 men killed or died on active service and a further 46 wounded.<ref name=AWM/>{{#tag:ref|Bilney provides the following figures: 20 killed in action, 14 died on active service and 44 wounded.<ref>{{harvnb|Bilney|1994|p=191}}.</ref>|group=Note}} Members of the battalion received the following decorations: one [[Distinguished Service Order]], one [[Officer of the Most Excellent Order of the British Empire|Order of the British Empire]], one [[British Empire Medal]], two [[Military Cross]]es, one [[Distinguished Conduct Medal]], one [[Military Medal]], and seven [[Mention in Dispatches]].<ref name=AWM/>

==Battle honours==
For their service during the Second World War, the 14th/32nd Battalion received the following [[battle honour]]s:
* [[South West Pacific theatre of World War II|South-West Pacific 1942–45]] and [[New Britain campaign|Waitavolo]].<ref name=AWM/>

==Notes==
;Footnotes
{{reflist|group=Note}}

;Citations
{{reflist|3}}

==References==
{{Refbegin}}
* {{cite book|last=Bilney|first=Keith|title=14/32 Australian Infantry Battalion A.I.F. 1940–1945, Australia, New Guinea and New Britain|year=1994|publisher=14/32nd Australian Infantry Battalion Association|location=Melbourne, Victoria |isbn= |oclc=38358147 |ref=harv}}
* {{cite book|last=Festberg |first=Alfred |title=The Lineage of the Australian Army |year=1972 |publisher=Allara Publishing |location= Melbourne, Victoria |isbn= 978-0-85887-024-6 | ref=harv}}
* {{cite book | last=Grey | first=Jeffrey | year=2008 | title=A Military History of Australia | edition=3rd | publisher=Cambridge University Press | location=Melbourne, Victoria | isbn=978-0-521-69791-0 |ref=harv}}
* {{cite book|last=Johnston|first=Mark|title=The Australian Army in World War II|year=2007|series=Elite # 153|publisher=Osprey Publishing|location=Oxford|isbn=978-1-84603-123-6 |ref=harv}}
* {{cite book|last=Long|first=Gavin|title=The Final Campaigns|series=Australia in the War of 1939–1945, Series 1—Army. Volume VII|year=1963|edition=1st|publisher=Australian War Memorial|location=Canberra, Australian Capital Territory |url=https://www.awm.gov.au/collection/RCDIG1070206/|oclc=1297619 |ref=harv}}
* {{cite book|last=Palazzo|first=Albert|chapter=Organising for Jungle Warfare|title=The Foundations of Victory: The Pacific War 1943–1944|year=2004|editor=Dennis, Peter |editor2=Grey, Jeffrey|publisher=Army History Unit|location=Canberra, Australian Capital Territory |url=http://www.army.gov.au/Our-history/Army-History-Unit/Chief-of-Army-History-Conference/Previous-Conference-Proceedings/~/media/Files/Our%20history/AAHU/Conference%20Papers%20and%20Images/2003/2003-The_Pacific_War_1943-1944_Part_1.ashx|isbn=978-0-646-43590-9 |ref=harv}}
{{Refend}}

{{DEFAULTSORT:14th 32nd Battalion (Australia)}}
[[Category:Australian World War II battalions]]
[[Category:Military units and formations established in 1942]]
[[Category:Military units and formations disestablished in 1945]]
[[Category:1942 establishments in Australia]]