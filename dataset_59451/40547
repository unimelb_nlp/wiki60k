{{for|Pink Shirt Day|Anti-Bullying Day}}
{{Infobox Organization
|name         = International Day of Pink
|image        = 
|image_border =
|size         =
|caption      =
|map          =
|msize        =
|mcaption     =
|abbreviation =
|motto        =
|formation    = 2007
|extinction   =
|type         =
|status       = active
|purpose      = Anti-Bullying
|headquarters = [[Ottawa]], [[Ontario]]
|location     = [[Canada]]
|region_served =
|membership   =
|language     =
|leader_title = 
|leader_name  = 
|main_organ   =
|parent_organization =
|affiliations =
|num_staff    =
|num_volunteers =
|budget       =
|website      = [http://www.pinkshirtday.ca]
|remarks      =
}}

'''The International Day of Pink''' is a Canadian anti-bullying event held annually on February 28th [[www.pinkshirtday.ca]]. The event started when students David Shepherd and Travis Price saw another student, who was wearing a pink shirt, being bullied in their [[Central Kings Rural High School]] in [[Nova Scotia]], and deciding to show support for the student by getting everyone at their school to wear pink the following day.<ref>{{cite news|title=Bullied student tickled pink by schoolmates' T-shirt campaign|url=http://www.cbc.ca/news/canada/bullied-student-tickled-pink-by-schoolmates-t-shirt-campaign-1.682221|accessdate=26 June 2012|newspaper=CBC|date=19 September 2007}}</ref>

The initiative inspired youth at [[Jer's Vision]] who founded The International Day of Pink, an effort to support their peers internationally with resources and ways to make their schools safer.<ref>{{cite web|title=Constructing Change: Celebrating the Day of Pink|url=http://rabble.ca/podcasts/shows/constructing-change-activist-toolkit/2012/04/constructing-change-celebrating-day-pink|publisher=Rabble.ca|accessdate=26 June 2012}}</ref><ref>{{cite news|title=Residents Wear Pink to Stand up to Bullies|url=http://www.ottawasun.com/2012/04/10/residents-wear-pink-to-stand-up-to-bullies|accessdate=26 June 2012|newspaper=Ottawa Sun|date=10 April 2012}}</ref><ref>{{cite news|title=Jer's Vision Celebrates Diversity, the Day of Pink and Five Years |url=http://www.xtra.ca/public/Ottawa/Jers_Vision_celebrates_diversiy_the_day_of_pink_and_five_years-8511.aspx |accessdate=26 June 2012 |newspaper=Xtra |date=15 April 2010 |deadurl=yes |archiveurl=https://web.archive.org/web/20120817193002/http://www.xtra.ca/public/Ottawa/Jers_Vision_celebrates_diversity_the_day_of_pink_and_five_years-8511.aspx |archivedate=17 August 2012 |df= }}</ref><ref name="xtra">{{cite news|title=Jer's Vision and the Day of Pink Celebrating Six Years of Fighting Discrimination in Schools |url=http://www.xtra.ca/public/Ottawa/Jers_Vision_and_the_Day_of_Pink-10009.aspx |accessdate=26 June 2012 |newspaper=Xtra |date=13 April 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20120817120959/http://www.xtra.ca/public/Ottawa/Jers_Vision_and_the_Day_of_Pink-10009.aspx |archivedate=17 August 2012 |df= }}</ref>

In 2012 over 8 million people participated.<ref>{{cite news|last=Pearson|first=Matthew|title=Day of Pink campaign comes to Ottawa|url=http://www.ottawacitizen.com/Life/All/6431823/story.html|accessdate=26 June 2012|newspaper=Ottawa Citizen|date=11 April 2012}}{{dl|date=February 2017}}</ref><ref>{{cite web|title=Pink Day anti-bullying campaign draws millions of participants worldwide|url=http://ca.news.yahoo.com/blogs/dailybrew/day-pink-campaign-draws-million-participants-worldwide-165009164.html|publisher=Yahoo! News The Daily Brew}}</ref>

==Overview==
The International Day of Pink is run by youth volunteers from [[Jer's Vision]], in Ottawa, Ontario, Canada. The initiative seeks to support the work of students, educators, community and business in their efforts to stop bullying, discrimination, homophobia and transphobia.<ref name="xtra"/>

==Events==
In 2012, students participating the Day of Pink organized numerous flash mobs, including one on Parliament Hill.<ref>{{cite web|last=Mack|first=Chantal|title=Pink shirts bring anti-bullying message to Parliament Hill|url=http://www.canada.com/life/Pink+shirts+bring+anti+bullying+message+Parliament+Hill/6443371/story.html|publisher=Canada.com PostMedia Web|accessdate=26 June 2012}}{{dl|date=February 2017}}</ref>    The International Day of Pink also shares a Gala with JersVision.org and the event has featured comedian [[Rick Mercer]],<ref>{{cite news|title=Hundreds Celebrate Day of Pink|url=http://m.ottawasun.com/2012/04/12/hundreds-celebrate-day-of-pink|accessdate=26 June 2012|newspaper=Ottawa Sun}}{{dl|date=February 2017}}</ref> [[Brian Burke (ice hockey)|Brian Burke]] of the [[Toronto Maple Leafs]],<ref>{{cite news|title=Brian Burke Scores in Ottawa|url=http://www.torontosun.com/sports/hockey/2011/04/13/17985956.html|accessdate=26 June 2012|newspaper=Toronto Sun|date=13 April 2011}}</ref> and former Canadian Ambassador [[Stephen Lewis]].<ref name="xtra" />

==See also==
*[[Jer's Vision]]
*[[School Bullying]]
*[[International STAND UP to Bullying Day]]

==References==
{{Reflist}}

==External links==
*[http://www.dayofpink.org International Day of Pink]
{{Bullying}}
{{LGBT topics in Canada}}
{{LGBT}}

[[Category:Public holidays in Canada]]
[[Category:Movable April observances]]
[[Category:Anti-bullying campaigns]]
[[Category:Anti-bullying charities]]