{{Multiple issues|
{{advert|date=February 2017}}
{{overly detailed|date=February 2017}}
{{refimprove|date=February 2017}}
{{Primary sources|date=February 2017}}
}}

{{Infobox organization
| name          = ISBT (International Society of Blood Transfusion)
| size          = <!-- default 200px -->
| alt           = <!-- alt text; see [[WP:ALT]] -->
| caption       = ISBT logo
| map           = 
| msize         = <!-- map size, optional, default 250px -->
| malt          = <!-- map alt text -->
| formation     = 1935
| founder       = <!-- or | founders = -->
| extinction    = <!-- {{End date and age|YYYY|MM|DD}} -->
| merger        = 
| merged        = 
| type          = <!-- [[Governmental organization|GO]], [[Non-governmental organization|NGO]], [[Intergovernmental organization|IGO]], [[International nongovernmental organization|INGO]], etc -->
| status        = <!-- ad hoc, treaty, foundation, etc -->
| purpose       = <!-- focus as e.g. humanitarian, peacekeeping, etc -->
| headquarters  = 
| location      = Worldwide
| coords        = <!-- Coordinates of location using {{Coord}} -->
| region_served = 
| membership    = >1,600
| language      = 
| general       = <!-- Secretary General -->
| leader_title  = President
| leader_name   = Vengetassen (Ravi) Reddy (2016-2018) <ref>http://www.affairscloud.com/indian-origin-south-african-ravi-reddy-head-international-society-blood-transfusion-isbt/</ref>
| leader_title2 = President Elect
| leader_name2  = Martin L. Olsson (assumes office in 2018)
| leader_title3 = Past President
| leader_name3  = Celso Bianco (2014-2016)
| leader_title4 = Executive Director
| leader_name4  = Judith F. Chapman
| main_organ    = <!-- gral. assembly, board of directors, etc -->
| parent_organization = <!-- if one -->
| affiliations  = 
| budget        = 
| num_staff     = 
| num_volunteers =  
| website       = http://www.isbtweb.org/
| remarks       = 
}}

The '''International Society of Blood Transfusion''' ('''ISBT''') is a scientific society, founded in 1935, which aims to promote the study of [[blood transfusion]], and to spread the know-how about the manner in which blood transfusion medicine and science best can serve the patient's interests.  The society's central office is in [[Amsterdam]], and there are around 1700 members in 97 countries.<ref>http://www.isbtweb.org/fileadmin/user_upload/Annual_report_2015_2016_s.pdf</ref>  Currently, the President is Mr. Ravi Reddy.

The Society organizes an international congress every other even year and two regional congresses in odd years, one in Europe and one in Asia. ISBT advocates standardisation and harmonisation in the field of blood transfusion. The other major impact on the transfusion community is the classification of various [[Human blood group systems]] under a common nomenclature.<ref>http://www.isbtweb.org/working-parties/red-cell-immunogenetics-and-blood-group-terminology/</ref>  ISBT's coordination also extends to obtaining donors with rare antigens, a process that often involves international searches, and a common terminology is critical to that process.

==History==
The history of ISBT was described by Dr. Hans Erik Heier in 2015.<ref>http://onlinelibrary.wiley.com/doi/10.1111/voxs.12171/abstract</ref> He distinguished four phases in the formation of the society as we know it today.

===Phase 1: Formation of ISBT (1935-1985)===
The formation of the International Society of Blood Transfusion, or Societé International de Transfusion Sanguine, as it was called at the time, was initiated in [[Rome]] at a meeting between representatives from 20 nations, the [[International Red Cross]] and the Bogdanov institute in [[Moscow]]. Blood transfusion was a rather new therapeutic option, and therefore it was decided that transfusion-specific congresses should be organised, to highlight the potential importance of transfusion. To organize these congresses, a society was needed.

After it was decided that a society dedicated to organizing transfusion-related congresses should be created, it did not take long until ISBT was founded. In 1937 in [[Paris]] a Central Office (CO) was set up, led by newly appointed Secretary General [[Arnault Tzanck]]. Only two years later the CO activities had to be suspended in 1939 because of the [[Second World War]] (WW2).

In the period surrounding WW2 [[immunohaematology]] and [[transfusion technology]] had developed almost explosively. [[Blood banks]] were created, voluntary [[blood donations]] came in great numbers in the allied nations to support the fight for a free society, plasma-transfusion became a standard anti-shock treatment, Rh and [[Kell antigen system|Kell]] systems were discovered, and industrial [[blood plasma fractionation]] was developed to produce albumin, which can be used as a substitute for plasma. In 1947 the first post-war congress was organised in Turin, Italy. Here some specific future goals were laid out to complement the main activity of the Society, the organization of congresses.
-	Non-commercialisation of blood and –derivatives
-	Oversee and initiate standardization of equipment, reagents and nomenclature.
-	Stimulate the set-up of central transfusion organisations for every country, under flag of the National [[Red Cross Society]], unless otherwise organised.

These goals show the strong ambitions of the young, post-war society to not just organise congresses, but be a stimulating factor in the development of modern blood transfusion science and practice, taking into account socio-political and socio-economic aspects. After the congress in Turin and the goals that were set there the society was able to continue its work for 40 years, until 1985, the year of crisis.

===Phase 2: Years of crisis (1985-1999)===
In 1985 the [[HIV/AIDS]] epidemic struck transfusion medicine. During that time the ISBT CO was still located in Paris as a part of the Centre National de Transfusion Sanguine (CNTS) as their head, Michel Garretta, was also ISBT Secretary General at the time. In June 1991 he had to step down as head of CNTS, as the HIV/AIDS crisis had become a catastrophe for the transfusion system in France and eventually lead to a reorganisation of CNTS in 1991. Subsequently, at the  ISBT Congress in Hong Kong it was decided that ISBT could no longer be linked to CNTS, ruling out Garretta’s succession of a French colleague. Harold Gunson, who was President of ISBT in 1991, agreed to take on a second role as acting Secretary General. Together with CNTS, and ISBT Secretary Claudine Hossenlopp he supervised the move of the CO from Paris to Manchester, UK. In 1994 he resigned from his post as blood centre director in Manchester and moved the ISBT CO to Lancaster, into his own home. He upheld the CO together with his wife until 1999. The end of Gunson’s term meant having to find a new location for the CO, and a new Secretary General.

===Phase 3: Reformation (1999-2010)===
In 1999 the new location for the ISBT CO was [[Amsterdam]], where it became a part of professional congress organiser (PCO) Eurocongress. Paul Strengers, a doctor at [[Sanquin]] Blood Supply took up the role of Secretary General. A new vision for the 2002-2006 period of ISBT was created by the executive committee, focusing  on developing ISBT into an umbrella organization, improving communication with the membership, educational and scientific activities, and professionalizing the central office. In the coming ten years the society would work had to achieve these goals, with Strengers to remain Secretary General for that period. Eurocongress organised ISBT congresses together with the CO and local organizing committees. The help of a PCO took away economic risks attached to congresses as they were able to provide professional assistance and detailed advice. As the CO had moved to a different country, the ISBT statutes and by-laws were also updated and adapted to Dutch law.

===Phase 4: A professional organisation (2010-now)===
The reformations made in the previous years had led to an increase in workload for the ISBT CO. In order to continue the fulfilment of the strategic plans of the ISBT, a full-time, paid Chief Executive Officer (CEO) was hired in 2010. In 2012 the CO moved to a separate location in Amsterdam as the shared space with Eurocongress did not meet the needs of the expanded CO staff. Currently, 5 paid persons are employed full-time at the CO, managed by CEO Judith Chapman (2010 – today). Congresses are organised by MCI, of which Eurocongress became a part in 2010. In that same year Martin Olsson was appointed as Scientific Secretary (non-remunerated) to overlook the scientific programming of ISBT congresses and to guarantee the high scientific quality. The second scientific secretary, Ellen van der Schoot, is currently in office until 2018.

==Congresses==
{{unreferenced section|date=February 2017}}
The organisation of congresses was the main reason for creating ISBT in 1935. So far, 34 International Congresses and 26 Regional Congresses have been organised. ISBT congresses discuss a wide range of state of the art topics in the field of [[transfusion medicine]], with invited speakers for plenary and parallel sessions. Congresses typically start with a local or regional day on Saturday, followed by a day organised by the ISBT Academy on Sunday. Monday, Tuesday, and Wednesday are dedicated to parallel sessions, plenary sessions, workshops, and specialised group sessions. International congresses are typically one day longer and include Wednesday afternoon and Thursday morning in the scientific programming. All ISBT Congresses are complemented by a scientific exhibition with companies relevant to transfusion medicine.

Congresses provide networking and learning opportunities to delegates, as well as the prospect to connect with industry. The society is committed to organizing congresses around the world. The international congresses take place once every even year. In the odd years two regional congresses are organised, of which one is in Europe and the other in a different part of the world. All congresses are organised in cooperation with a local organizing committee and a local scientific committee, and in some instances are organised in conjunction with a congress by another scientific society.

==ISBT Academy==
{{unreferenced section|date=February 2017}}
In 2011 the ISBT Academy was founded with the aim to provide support educational and research activities around the world. In 2013 it was decided that the Academy would focus solely on education, as it was not able to provide the resources to sustain a fruitful research programme. All education and knowledge activities from ISBT are facilitated through the Academy nowadays. The Academy has three specific goals: Overseeing the ISBT Academy ePortal,<ref>http://academy.isbtweb.org/isbt/#!*menu=17*browseby=1*sortby=1</ref> supporting workshops or educational activities financially or by the use of the ISBT logo, or host workshops or educational activities, for example at ISBT Congresses.

The Academy is governed by the ISBT Academy Standing Committee, currently chaired by Erica Wood.<ref>http://isbtweb.org/knowledge-education/standing-committee-members/</ref> The members of the committee represent all regions across the globe and work together with the ISBT Scientific Officer. The Academy receives financial support from the ISBT Foundation, simultaneously acting as the advisory committee to the Foundation. The Standing Committee reviews all incoming applications for Academy support and decides whether requests are honoured.

===ISBT Academy ePortal===
The ePortal is an online learning environment accessible exclusively to members of ISBT. The platform contains a range of educational, scientific content, including eBooks, papers, webcasts, interviews, learning quizzes, and guidelines. Special content from ISBT Congresses are also posted on the ePortal, such as ePosters, abstracts, and recordings of scientific sessions, which are typically complemented with a learning quiz before posted on the ePortal.

==Awards and Fellowships==
{{unreferenced section|date=February 2017}}

===ISBT Award for Developing Countries===
Every two years at the ISBT International Congress this award is presented to a Blood Service or an institutional department of Transfusion Medicine from a Developing Country as a recognition of their significant contribution to improving blood transfusion practise in that country. The award is reserved to candidates from Low or Medium UNDP Human Development Index countries.

===Jean Julliard Prize===
This award, named after the Society’s first Secretary General, was established in 1962. It is awarded to scientists under 40 years of age in recognition of their recent scientific work on blood transfusion related subjects. Individual members of ISBT can apply themselves for the prize, which is awarded once every two years at the ISBT International Congress. The winner of the Jean Julliard Prize is expected to give a prize lecture at the plenary session. <ref>https://www.medischcontact.nl/nieuws/laatste-nieuws/artikel/prestigieuze-internationale-prijs-voor-de-haas.htm</ref>

===ISBT Presidential Award===
This award is granted to a senior person who has made significant contributions to transfusion medicine. It is awarded by the Foundation Transfusion Medicine to those that advanced transfusion medicine or a related field through the practice of transfusion therapy, basic or applied original research, or through educational or service contributions to the field. The winner is chosen by the Nomination Committee, consisting of the ISBT President, President Elect, the ISBT Scientific Officer, and the chairman and a member of the Board of the Foundation Transfusion Medicine. The award is presented once every two years at the ISBT International congress.

===ISBT Award===
This award is presented to those who have contributed significantly to transfusion medicine and science, especially in relation to education. The award is presented during the opening ceremony of ISBT congresses. Typically two ISBT Awards are presented each congress. Members of the Executive committee decide who are to receive the award.

===Harold Gunson Fellowship===
The Harold Gunson Fellowships were introduced in 2007 in recognition of the work Harold Gunson carried out as Secretary General and President of ISBT. The fellowship is essentially a travel grant, allowing applicants under 40 years of age to attend ISBT Congresses. The fellowship covers the congress registration fee, travel expenses, and hotel cost. Early 2016 regulations were changed to not only allow people from low- and medium HDI countries to apply, but allow people from all countries to apply for the grant. In order to be eligible for this grant, one must be the first, submitting, and presenting author of an abstract that has been accepted to the scientific programme of that congress. After the congress, Fellowship winners are expected to write a report on their experience.<ref>https://vitalrecord.tamhsc.edu/appiah-isbt-fellowship-grant/</ref><ref>http://wissenschaftsstadt-essen.de/dr-andre-goergens-erhaelt-harold-gunson-fellowship/</ref>

===Vox Sanguinis Best Paper Prize===
This scientific award is presented to the author(s) of the best original paper that was published in the ISBT journal [[Vox Sanguinis]] in the past calendar year. The selection of eligible articles is made by the editors of Vox Sanguinis. The prize is granted by the Editorial Board and the Standing Committee on Vox Sanguinis. The value of the prize is €5000 .

==See also==
{{Portal|Biology}}
*[[World Health Organization]]
*[[AABB]]
*[[ISBT 128]]

==References==
{{Reflist}}

==External links==
* {{Official website|http://www.isbtweb.org}}
* [http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1423-0410 Vox Sanguinis]
* [http://www.iccbba.com ISBT 128 Standardized bar coding]

{{transfusion medicine}}
{{Authority control}}

{{DEFAULTSORT:International Society Of Blood Transfusion}}
[[Category:Blood banks]]
[[Category:Transfusion medicine]]
[[Category:Organizations established in 1935]]
[[Category:Medical and health organisations based in the Netherlands]]