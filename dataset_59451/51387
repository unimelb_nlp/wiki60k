{{Infobox journal
| title = Genesis
| cover = [[File:Genesis-cover.gif]]
| editor = [[Sally A. Moody]]
| discipline = [[Genetics]], [[developmental biology]]
| abbreviation = Genesis
| former_names = Developmental Genetics
| publisher = [[Wiley-Liss]]
| country = 
| frequency = Monthly
| history = 1979–present
| openaccess = 
| license =
| impact = 2.165
| impact-year = 2015
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1526-968X
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1526-968X/currentissue
| link1-name = Online access
| link2 =  http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1526-968X/issues
| link2-name = Online archive
| JSTOR = 
| OCLC = 42463257
| LCCN = sn99009370
| CODEN = GNESFY
| ISSN = 1526-954X
| ISSN2 = 0192-253X
| ISSN2label = ''Developmental Genetics'':
| eISSN = 1526-968X
}}
'''''Genesis: The Journal of Genetics and Development''''' (often styled ''genesis'') is a [[peer review|peer-reviewed]] [[scientific journal]] of [[genetics]] and [[developmental biology]]. It was established as ''Developmental Genetics'' in 1979 and obtained its current title in 2000. In addition to [[original research]] articles, the journal also publishes [[letter to the editor|letters to the editor]] and technology reports relevant to the understanding of the functions of [[gene]]s. The [[editor-in-chief]] is [[Sally A. Moody]] ([[George Washington University]]).

==Abstracting and indexing==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Abstracts in Anthropology]]
* [[Elsevier BIOBASE]]
* [[Biological Abstracts]]
* [[BIOSIS Previews]]
* [[Biotechnology Citation Index]]
* [[CAB Direct (database)|CAB Abstracts]]
* [[CAB Direct (database)|CAB HEALTH]]
* [[CAB Direct (database)|CABDirect]]
* [[CSA (database company)|Cambridge Scientific Abstracts]]
* [[Chemical Abstracts Service]]
* [[CSA (database company)|CSA Biological Sciences Database]]
* [[Current Contents]]/Life Sciences
* [[Current Opinion in Cell Biology]]
* [[Current Opinion in Obstetrics & Gynecology]]
* [[Current Opinion in Pediatrics]]
* [[EMBASE]]
* [[Embiology]]
* [[International Bibliographic Information on Dietary Supplements]]
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]
* [[MEDLINE]]
* [[Neurosciences Abstracts]]
* [[Science Citation Index]]
* [[Scopus]]
* [[Soils and Fertilizers]]
* [[Veterinary Bulletin]]
* [[VINITI Database RAS]]
* [[Zoological Abstracts]]
* [[The Zoological Record]]
}}
According to the ''[[Journal Citation Reports]]'', its 2010 [[impact factor]] is 2.584.<ref name=WoS>{{cite book |year=2013 |chapter=Genesis |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |work=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1526-968X}}

{{DEFAULTSORT:Genesis}}
[[Category:Publications established in 1979]]
[[Category:Genetics journals]]
[[Category:Wiley-Liss academic journals]]
[[Category:English-language journals]]
[[Category:Monthly journals]]
[[Category:Developmental biology journals]]