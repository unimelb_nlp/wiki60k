{{Use dmy dates|date=June 2013}}
{{Use British English|date=June 2013}}
{{Infobox professional wrestler
|name=Mark Rocco
|image=Replace this image male.svg
|caption=
|names=Mark Rocco<br />[[Black Tiger (professional wrestling)#Black Tiger I|Black Tiger]]
<!-- Please don't change the height or weight. These are the measures as officially stated and they should not be changed. -->
|height={{height|ft=5|in=8}}
|weight={{convert|195|lb|kg|abbr=on}}
|birth_date={{birth date and age|df=y|1951|5|11}}
|death_date=
|birth_place=[[Manchester]], England
|death_place=
|resides=[[Tenerife]], [[Canary Islands]]
|billed=
|trainer=Colin Joynson
|debut=1972
|retired=1991
|}}
'''Mark Hussey''' (born 11 May 1951) is a retired English [[Professional wrestling|professional wrestler]] who competed for [[All-Star Wrestling]] as '''Mark "Rollerball" Rocco''' and as the original masked '''Black Tiger''' in [[New Japan Pro Wrestling]] during the 1970s and 80s. A fourth-generation wrestler, he is the son of British wrestler Jim Hussey and the father of boxer Johnathan "Rocco" Hussey.

Regularly appearing on [[ITV Network|ITV]]'s ''[[World of Sport (UK TV series)|World of Sport]]'', he feuded with many of the top light heavyweight wrestlers of the era including [[Marty Jones]], the [[Dynamite Kid]], [[Clive Myers|"Iron Fist" Clive Myers]] in England and masked Japanese wrestler [[Satoru Sayama|Tiger Mask]] in [[Japan]].

==Early career==
Born in Manchester, Rocco grew up in his father's gym where other local wrestlers trained. Although his father was opposed to his being a professional wrestler, going so far as to have his son banned from his gym, Rocco would receive lessons from some of the veterans while his father was out on tour.<ref>[[Kendo Nagasaki|Nagasaki, Kendo]]. ''The Grapple Manual: Heroes & Villains from the Golden Age of World Wrestling''. New York: Sterling Publishing, 2005. ISBN 0-297-84419-9</ref><ref name="AOW">{{cite AV media | people=[[Colt Cabana|Cabana, Colt]] (Host) | date=19 June 2013 | title=AOW 152: Rollerball Rocco | url = http://tsmradio.com/coltcabana/2013/06/19/aow-152-rollerball-rocco/ | medium=[[Podcast]] | location=United States | publisher=TSMradio.com}}</ref>

Rocco started amateur wrestling at age 16, competing as far away as southern France and Pakistan,<ref name="AOW"/> was definitely wrestling professionally by late 1972, being then a regular at Northern venues such as Liverpool Stadium and Blackpool Tower.{{Citation needed|date=May 2010}}
Making his debut in Dale Martin's London territory under the [[Joint Promotions]] banner, he became a rising star in the organization, defeating [[Bert Royal]] for the [[British Heavy Middleweight Championship]] on 11 June 1977 and was involved in televised high-profile matches with Marty Jones before losing the title to him on 13 September 1978.

After Jones vacated the title, Rocco regained the title after defeating then-rookie [[Chris Adams (wrestler)|Chris Adams]] in a tournament final on 6 December 1978.  Rocco lost the championship to Adams a few months later, and regained it towards the middle of 1979.

Touring North America the following year, he teamed with [[Greg Gagne (wrestler)|Greg Gagne]] and briefly competed in the [[World Wide Wrestling Federation]],<ref name="AOW"/> one of his opponents being [[Hulk Hogan|Terry Bollea]].

In 1981, Rocco had his first feud with [[Satoru Sayama]], then wrestling in Britain as Sammy Lee.  Rocco was scheduled to wrestle Lee for the World Heavy-Middleweight title (recognised as vacant by Joint Promotions) at Wembley Arena that year on the undercard of the famous [[Shirley Crabtree|Big Daddy]] versus [[Martin Ruane|Giant Haystacks]] grudge match, but this was cancelled after Lee returned to Japan due a family bereavement.  Rocco was awarded Joint Promotions recognition as champion by default that night; later that year he defeated Joel de Fremery at a TV taping in [[Southport]] for the main European version of the World Heavy Middlweight title.<ref name="worldhmwt">{{cite web|url=http://www.wrestling-titles.com/world/world-hm.html |title=World Heavy-Middleweight Title (U.K.) |publisher=Wrestling-titles.com |date= |accessdate=2014-03-26}}</ref>  Vacating his British title to concentrate on the World title, Rocco feuded intensely with a returning [[Dynamite Kid]], culminating in a World title match in [[Lewisham]], [[South London]] that ended in a double knockout.<ref>{{cite web|url=http://www.johnlisterwriting.com/itvwrestling/82.html |title=itvwrestling.co.uk - 1982 |publisher=Johnlisterwriting.com |date= |accessdate=2014-03-26}}</ref>

==New Japan Pro Wrestling==
{{Main|Black Tiger (professional wrestling)}}

After his series of highly regarded matches, Rocco was contacted by [[New Japan Pro Wrestling]] to wrestle a series of matches against Lee/Sayama in Japan. Wrestling under the name '''Black Tiger''', against Sayama's '''[[Satoru Sayama|Tiger Mask]]''' character, Rocco and Tiger Mask's matches were some of the highest rated in Japanese television history.<ref name="AOW"/> The success of this series of matches between the original Black Tiger and original Tiger Mask would be followed with later incarnations of wrestlers to have competed under both the Black Tiger and Tiger Mask names in later years.

The rivalry between the two Tigers would continue throughout 1982, as the two feuded over the [[WWF Junior Heavyweight Championship]] after Rocco defeated [[Gran Hamada]] in a tournament final for the title in [[Fukuoka, Fukuoka|Fukuoka, Japan]] on 6 May before losing it back to Tiger Mask less than a month later in [[Tokyo, Japan]] on 26 May 1982.

Rocco would make further return visits to Japan in the late 1980s where he and [[Jushin Thunder Liger|Keiichi Yamada]] would recreate their UK feud. In 1989, as Black Tiger, Rocco fought with Yamada's own superhero [[alter ego]], Jushin Liger. He was also involved in Liger's early training.<ref name="AOW"/>

==All Star Wrestling==
Back home in Britain, Rocco was lured away from the TV/Joint Promotions spotlight by independent promoter [[Orig Williams]].  Crucial to the defection was that Rocco brought his World Heavy Middleweight championship with him.<ref>El Bandito: Orig Williams The Autobiography - Orig Williams, Y Llolfa 2010</ref>  Rocco agreed and made the jump, also working for promoter Brian Dixon, whose Wrestling Enterprises promotion evolved into [[All Star Wrestling]].<ref name="AOW"/>  When not on tour in Japan or elsewhere overseas, Rocco would continue to work for Dixon for the remainder of his career. Dixon would later comment that Rocco was his best employee, both as a worker and as a loyal friend.<ref>The Wrestling, Simon Garfield, Faber & Faber 1995</ref>

In 1983, Rocco appeared during All Star Wrestling's national tour of Great Britain and issued an open challenge for a non-title match to any wrestler in the promotion. Accepted by [[Frank Cullen|Frank "Chic" Cullen]], he was defeated by Rocco although they shook hands following the match.

During the second week of the tour, after defeating Mike Jordan in a singles match, Rocco challenged the [[Dynamite Kid]] who had also recently returned from NJPW to a match later that night. Agreeing to a tag team match, he and [[Fit Finlay]] would later lose to Dynamite Kid and Marty Jones at the end of the night after Dynamite Kid pinned Finlay. The following week he again challenged the Dynamite Kid challenging him to a 30-minute "iron man" match which resulted in a time limit draw with one pinfall each. This led to a brutal feud between the two, which would lead to many aggressive, bloody encounters, culminating in the Dynamite Kid challenging Rocco to a [[ladder match]] for his World Heavy Middleweight title. Rocco successfully defended the title after he had tied the Dynamite Kid's arms to the cord of the area curtains.  He would later defend the title in a rematch against Cullen, [[Robbie Brookside]] and his former tag team partner [[George Takano|The Cobra]] during the last weeks of the tour.

In late 1985, Rocco lost his title to Cullen but regained it a few days later.<ref name="worldhmwt" />  The following year, he faced the challenge of Yamada, now billed by All Star as "Flying" Fuji Yamada.  During the second half of 1986, Rocco lost his title to Yamada, regained it and then lost it again.  During this feud, All Star finally gained a share of ITV's wrestling coverage and so when Rocco finally won the belt back in Lewisham in March 1987, it was televised nationally.<ref name="worldhmwt" /><ref name="itv87">{{cite web|url=http://www.johnlisterwriting.com/itvwrestling/87.html |title=itvwrestling.co.uk - 1988 |publisher=Johnlisterwriting.com |date= |accessdate=2014-03-26}}</ref>

==Tag team and feud with Kendo Nagasaki==
Another televised confrontation between Rocco and Yamada would come in Spring 1987, on opposite sides of a tag match.  Yamada and his tag partner in Britain, "Ironfist" Clive Myers had challenged legendary masked wrestler [[Peter Thornley|Kendo Nagasaki]] to a tag team match and, having a shared rival in Yamada, Rocco volunteered.  Nagasaki and Rocco defeated Yamada and Myers in the main event of a TV taping at the [[Fairfield Hall]] [[Croydon]].<ref name="itv87" />

Following this match, Nagasaki and Rocco would continue to team until a year later at another televised Croydon tag match, where the team collapsed in spectacular fashion while facing Myers and [[Dave Taylor (wrestler, born 1957)|Dave Taylor]].  Taylor was attempting, mid-match, to unmask Nagasaki and had nearly succeeded when Rocco intervened.  Rocco attempted to pull the mask back down, but Taylor forearm-smashed Rocco, causing the mask to come off in his hands.  As Taylor and Myers celebrated, Kendo fled to the dressing room and returned with another mask.  Kendo's manager George Gillette blamed Rocco for the unmasking, igniting a major feud that would run on into the early 1990s.<ref>{{cite web|url=http://www.kendonagasaki.org/comeback.htm |title=-: the comeback :- |deadurl=yes |archiveurl=https://web.archive.org/web/20071114184647/http://www.kendonagasaki.org/comeback.htm |archivedate=14 November 2007 }}</ref>

==Tours of mainland Europe==
In June 1988, he would also team with [[Dave Finlay]] losing to Mile Zrno & Tony St. Clair in a match to crown the first CWA World Tag Team Champions in [[Linz, Austria]].<ref>{{cite web |url=http://www.wrestling-titles.com/europe/austria/cwa/eu-cwa-t.html |title=C.W.A. World Tag Team Title |accessdate= |author= |authorlink= |coauthors= |date= |year=2003 |month= |work= |publisher=Puroresu Dojo |pages= |language= |archiveurl= |archivedate= |quote= }}</ref>

Rocco also wrestled in France for Roger Delaporte's [[:fr:Fédération Française de Catch Professionnel|Fédération Française de Catch Professionnel]].  One of his last World Heavy Middleweight title defences, against [[Danny Boy Collins]] in [[Paris]], [[France]] in 1991, was aired on [[Eurosport]]'s ''New Catch'' programme, with Williams providing English commentary.

==Retirement==
In 1991, Rocco collapsed in the dressing room following a match against Fit Finlay in [[Worthing]]. Rocco, who had been suffering from pain in his back and kidneys since a match against Dave Taylor the previous night,<ref>{{cite web |url=http://www.thesun.co.uk/article/0,,2003560001-2006250068,00.html |title=Wrestling: Roccing all over the world |accessdate= |author=The Lilsboys|date= May 2006|work=[[The Sun (newspaper)|The Sun]]}}</ref> was taken to a nearby hospital where doctors found his heart was working at only 30% and diagnosed him with a heart condition which forced him to retire from professional wrestling.<ref>Symkus, Ed and Vinnie Carolan. ''Wrestle Radio U.S.A.: Grapplers Speak''. Toronto: ECW Press, 2004. ISBN 1-55022-646-0</ref>

In 1990's Mark emigrated to Tenerife where he ran a Car Hire firm in Los Cristianos.

==Recent years==
In February 2003, he was scheduled to attend a "Legends' Reunion" event held by 'All Star Wrestling' in Croydon, England which included former alumni such as [[Mick McManus (wrestler)|Mick McManus]], [[Johnny Kincaid]], [[Dave Bond]], [[John Elijah]] and [[Wayne Bridges]]; however, he was unable to attend.

In August 2006, he and his father received a lifetime achievement award at the 15th Southern Wrestlers’ Reunion at [[South Darenth|South Darenth, Kent]]. During the event, Rocco also presented a lifetime achievement award to promoter Brian Dixon as well.<ref>{{cite web |url=http://www.bigtimewrestlinguk.com/index.php?id=pages/news/news21 |title=Rollerball Rocco Steps Forward As Wrestling Honours Leading Personalities Past and Present |accessdate= |author=Plummer, Russell|date=2006-09-02 |year= |month= |publisher=BigTimeWrestlingUK.com}}</ref>
Rollerball Rocco was the subject of the 2011 song "Inside the Restless Mind of Rollerball Rocco" by English musician Luke Haines. It is featured on Haine's psychedelic rock concept album about British wrestlers entitled "9 1/2 Psychedelic Meditations On British Wrestling Of The 1970's And Early 1980's".<ref>{{cite AV media notes |title=9 1/2 Psychedelic Meditations On British Wrestling Of The 1970's And Early 1980's |others=Haines, Luke |year=2011 |chapter=Inside The Restless Mind Of Rollerball Rocco |url=http://www.amazon.com/dp/B00C1A8C5M |type=MP3 |publisher=Fantastic Plastic |id= |location=United Kingdom}}</ref>

In September 2012, Rocco was named as one of the mentors on the [[Challenge (TV channel)|Challenge]] [[reality television]] [[television program|programme]] [[TNA Wrestling: British Boot Camp]].<ref name="Challenge">{{cite web|url=http://www.challenge.co.uk/wrestling/tna-british-boot-camp-on-challenge-tv.html|title=TNA British Boot Camp on Challenge TV|publisher=[[Challenge (TV channel)|Challenge.co.uk]]|accessdate=2012-10-07}}</ref> In June 2013, he was interviewed on ''[[Colt Cabana|The Art of Wrestling with Colt Cabana]]''.<ref name="AOW"/>

==In wrestling==
*'''As Mark Rocco'''
**'''Finishing moves'''
***[[Professional wrestling aerial techniques#Diving knee drop|Diving knee drop]]
**'''Signature moves'''
***[[Professional wrestling throws#Back body drop|Back body drop]]
***[[Professional wrestling holds#Three-quarter facelock|European headlock]]
***[[Professional wrestling attacks#European uppercut|European uppercut]]
***[[Powerslam#Scoop powerslam|Scoop powerslam]]
*'''As Black Tiger'''
**'''Finishing moves'''
***''Black Tiger Bomb'' ([[Powerbomb#Sitout crucifix powerbomb|Sitout crucifix powerbomb]])

==Championships and accomplishments==
*'''[[All Star Promotions]]'''
**World Heavy Middleweight Championship (3 times)<ref>{{cite web |url=http://www.wrestling-titles.com/world/world-hm.html |title=World Heavy-Middleweight Title (U.K.) |year=2003 |publisher=Puroresu Dojo}}</ref>
**British Heavy Middleweight Championship (2 times)<ref>{{cite web |url=http://www.wrestling-titles.com/europe/uk/bri-hm.html |title=British Heavy Middleweight Title |year=2003|publisher=Puroresu Dojo}}</ref>
*'''British Commonwealth'''
**British Light Heavyweight Championship (1 time)<ref>{{cite web |url=http://www.wrestling-titles.com/europe/uk/bri-lh.html |title=British Light Heavyweight Title|year=2003|publisher=Puroresu Dojo}}</ref>
*'''[[Pro Wrestling Illustrated]]'''
**PWI ranked him #'''298''' of the 500 best singles wrestlers of the [[PWI 500]] in 1991
*'''[[World Wrestling Entertainment|World Wrestling Federation]]'''
**[[WWF Junior Heavyweight Championship]] ([[WWF Junior Heavyweight Championship#Title history|1 time]])<ref>{{cite web |url=http://www.wrestling-titles.com/japan/newjapan/wwf-j.html |title=W.W.F. Junior Heavyweight Title |year=2003 |publisher=Puroresu Dojo}}</ref>

==References==
{{Reflist}}

==External links==
{{Portal|Professional wrestling}}
*[http://www.onlineworldofwrestling.com/columns/2007/jonathancruickshank01.html The Forgotten Legend of Rollerball Rocco] by Jonathan Cruickshank
*[http://www.smartwrestlingfan.com/TikiWiki/tiki-view_forum_thread.php?comments_parentId=5444&topics_threshold=0&topics_offset=25&topics_sort_mode=title_desc&topics_find=&forumId=11 SWF Wiki: Mark Rocco interview]
*[http://wrestlingclassics.com/cgi-bin/.ubbcgi/ultimatebb.cgi?ubb=get_topic;f=7;t=000288 Mark "Rollerball" Rocco Gordy List] by Kenny McBride
*{{Professional wrestling profiles}}

{{DEFAULTSORT:Rocco, Marc}}
[[Category:1951 births]]
[[Category:Living people]]
[[Category:English male professional wrestlers]]
[[Category:Sportspeople from Manchester]]
[[Category:Masked wrestlers]]