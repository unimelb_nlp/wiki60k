{{Infobox airport
| name         = Flesland Air Station
| nativename   = 
| image        = Flesland flyplass Rullebane.jpg
| image-width  = 300
| IATA         = BGO
| ICAO         = ENBR
| type         = Joint (military and civilian)
| owner        = [[Royal Norwegian Air Force]]
| operator     =
| city-served  = 
| location     = [[Flesland]], [[Bergen]], [[Norway]]
| metric-elev  = yes
| elevation-f  = 166
| elevation-m  = 50.6
| website      = 
| coordinates  = {{coord|60|17|37|N|005|13|05|E|region:NO|display=inline,title}}
| pushpin_map            = Norway
| pushpin_relief         = yes
| pushpin_label          = '''BGO'''
| pushpin_mapsize        = 300
| pushpin_map_caption    = 
| metric-rwy   = yes
| r1-number    = 17/35
| r1-length-m  = 2,990
| r1-length-f  = 9,810
| r1-surface   = Asphalt/concrete
}}

'''Flesland Air Station''' ({{lang-no|Flesland flystasjon}}) is a [[military air base]] situated at [[Flesland]] in [[Bergen]], [[Norway]]. Part of the [[Royal Norwegian Air Force]] (RNoAF), it shares a {{convert|2990|m|sp=us|adj=on}} [[runway]] with [[Bergen Airport, Flesland]]. The air station has since 1999 had a mobilization status and is only manned with six employees. Its main structure is an subterranean hangar with space for 25 fighter jets.

Construction began in 1952 and the following year the [[North Atlantic Treaty Organization]] (NATO) issued grants for a long runway. The facility cost 70 million [[Norwegian krone]] and opened on 14 September 1954. Flesland was never the permanent base of any squadrons. Its role was intended as a [[forward operating base]] for the [[United States Air Force]] (USAF) in case the [[Cold War]] turned hot. From about 1966 Flesland became a forward storage facility for [[nuclear warhead]]s, which could be moved to the air station in case of war. From 1985 the became operational as a collocated operating base, with supplies and facilities for USAF aircraft. Flesland Air Station was gradually mothballed during the 1990s.

==History==
===Prelude===
The first military air base to serve Bergen was [[Bergen Naval Air Station]], situated on the island of [[Flatøy]] in what is today [[Meland]]. Construction was started in 1917 and was commissioned in August 1919.<ref>Hafsten & Arheim: 40</ref> It served as one of the four main air stations for the [[Royal Norwegian Navy Air Service]] until 1940.<ref>Hafsten & Arheim: 42</ref> During the 1930s there were launched plans to build a land airport in Bergen. Flesland, which was at the time located in the then independent municipality of [[Fana (municipality)|Fana]], was one alternative. The other main alternative was [[Herdla]], an island northern-most in [[Askøy]]. The main disadvantage with Flesland was the topography and that it would not be possible to build longer runways than {{convert|800|and|850|m|sp=us}}, respectively.<ref name=o40>Østerbø: 40</ref>

After the [[Operation Weserübung|German invasion of Norway]] in April 1940, the [[Wehrmacht]] started looking for a location for an airstrip. They were partially in need for a counteraction against British raids and in part in need to protect German ship traffic. Four locations were considered: [[Nesttun]], Haukåsmyrene in [[Åsane]], Flesland and Herdla. The choice of Herdla was taken after a German bomber emergency landed in a field and was later able to take off from there. [[Herdla Airport]] received two runways, the longest {{convert|1000|m|sp=us}}.<ref name=o40 />

[[File:NoRAF F-16 and Braathens 737-200 at Flesland.jpg|thumb|[[General Dynamics F-16 Fighting Falcon]] and a [[Braathens SAFE]] [[Boeing 737|Boeing 737-200]] at Flesland in 1981]]
The [[Avinor|Civil Aviation Administration]] (CAA) started working on plans for an airport for Bergen in 1947. Herdla was by them seen as the prime candidate, predominantly due to the rugged topography around Bergen.<ref>Østerbø: 43</ref> The CAA recommended that Herdla be chosen and that the two runways be expanded. However, its director, [[Einar Bøe]], was skeptical to the Herdla plans, citing the lack of possibilities to extend the runway past the initially proposed {{convert|1500|m|sp=us}} and the long travel distance from Herdla to the city. At the time there was no bridge neither from Herdla to Askøy nor from Askøy to the mainland, nor was there a road across the island. Transport would therefore have to be carried out from Bergen using a ferry. The [[Minister of Transport and Communications (Norway)|Minister of Transport and Communications]] [[Nils Langhelle]] from Bergen supported Bøe's concerns and recommended that [[Parliament of Norway|Parliament]] place construction of an airport serving Bergen on hold.<ref>Østerbø: 44</ref>

Engineering reports were made of both Herdla and Flesland in 1950 and 1951. Previous investigations of Flesland had concluded that the length of the runway was limited to a small valley which is located at about the middle of the current runway. The new report proposed connecting the proposed area to a larger area to the south of the valley, which was at the right elevation. This would allow for a {{convert|3000|m|sp=us|adj=on}} runway.<ref>Østerbø: 46</ref> A political concern was the high cost of constructing a new airport.<ref>Østerbø: 63</ref>

===Construction===
[[File:US F-16 at Flesland 2.jpg|thumb|[[General Dynamics F-16 Fighting Falcon]] of the [[United States Air Force]]'s [[4th Tactical Fighter Squadron]] in January 1981—the first ever overseas deployment of the F-16]]
At the same time the Royal Norwegian Air Force stated looking at Flesland as a suitable air station. Military engineers surveyed the area and concluded that it was well-suited for military purposes. NATO granted funding for seven air stations in Norway in 1952, but these did not include Flesland.<ref>Østerbø: 64</ref> Financing was instead secured through a national military communications project, of which NOK&nbsp;16 million was set aside over a period of three years, which would secure construction of a {{convert|1460|m|sp=us|adj=on}} runway. A further NOK&nbsp;4 million was presumed financed by the municipalities of Bergen and Fana to build a terminal, [[eminent domain|expropriation]] and a road. The plans were passed by Parliament on 25 April 1952. The military funding was arranged by Langhelle, who by then had been appointed [[Minister of Defence (Norway)|Minister of Defence]]. Flesland received additional NATO grants in 1953, allocating NOK&nbsp;50 million. The [[Ministry of Defence (Norway)|Ministry of Defence]] paid the remaining NOK&nbsp;5 million. The increased funding allowed the runway to be extended to {{convert|2440|m|sp=us}}.<ref>Østerbø: 66</ref>

Construction started on 14 August 1952 with construction of a road from Blomsterdalen.<ref>Østerbø: 53</ref> Work on the airport lot itself started in early 1953. Between 200 and 300 people partook in the construction, some of which lived in sheds at Nordheim. Work was carried out in two shifts. Thirty farms were partially expropriated. Half a million cubic meters (18 million cu ft) of rock were blasted and a similar amount of earthwork moved in the construction process. The work consisted of a {{convert|2440|by|45|m|sp=us|adj=on}} runway and an equivalently long taxiway, although it was only half the width. The terminal building cost NOK&nbsp;200,000 and was located next to a parking lot with place for seventy cars. A {{convert|12|km|sp=us}} long [[barbed wire]] fence circumferenced the airport.<ref name=o56>Østerbø: 56</ref> The airport had [[instrument landing system]] from the start.<ref name=o78>Østerbø: 78</ref> Two people were killed in the construction, which had a total cost of NOK&nbsp;70 million.<ref name=o56 />

===Operational history===
[[File:NoRAF Westland Sea King at Flesland.jpg|thumb|[[Westland Sea King]] of the [[No. 330 Squadron RNoAF|330 Squadron]]]]
The first aircraft to land at the airport was a [[de Havilland Canada DHC-3 Otter]] of the air force on 18 June 1954. At the time {{convert|800|m|sp=us}} of the runway was completed. Later that year the paved section was extended to {{convert|1500|m|sp=us}} and several [[Douglas DC-3]] aircraft landed.<ref name=o72>Østerbø: 72</ref> The military officially took the airport into use on 14 September 1954.<ref name=o72 /> The official civilian opening took place on 2 October 1955. By then the airport had a temporary terminal and control tower, the airport road was not paved, aviation fuel tanks were not installed and there was no snow-removal equipment.<ref>Østerbø: 69</ref> The [[No. 338 Squadron RNoAF|338 Squadron]] was temporarily moved to Flesland for the summer of 1958, while the runway at [[Ørland Main Air Station]] was upgraded.<ref>Arheim: 150</ref>

A contentious issue arose in the late 1950s concerning the storage of American [[nuclear warhead]]s in Norway. For Western Norway [[Sola Air Station]] was designated as such a site.<ref>Skogrand & Tamnes: 254</ref> By 1959 the plans were changed and instead Flesland was selected as a Norwegian site along with [[Bodø Main Air Station]] for Class C warheads, to be used with [[attack aircraft]].<ref>Skogrand & Tamnes: 257</ref> Work commenced in 1964 and by 1966 the special ammunition storage (SAS) was taken into use. Flesland would in this capacity be placed under command of [[Allied Forces Northern Europe]] and serve the [[Third Air Force]] of the USAF.<ref>Skogrand & Tamnes: 261</ref> Nuclear weapon components are presumed to have been stored at Flesland, although the actually warheads presumably never have.<ref>Skogrand & Tamnes: 263</ref>

With the establishment of SAS, Flesland became an important NATO training station. The logistics surrounding the use by a strike force, Operation Dovetail, was carried out every four to eight weeks.<ref>Skogrand & Tamnes: 212</ref> Every other year there were larger exercises with about a dozen aircraft and lasting for several weeks.<ref>Østerbø: 92</ref> It is believed that in case of a nuclear strike on the Soviet Union, the nuclear warheads would be transferred to Flesland from other sites in Europe, ready for the arrival of the attack aircraft.<ref>Skogrand & Tamnes: 214</ref> The confidentiality meant that most of the RNoAF was unknown to Flesland's strategic role and in the late 1960s the airport was proposed closed in the late 1960s by parts of the RNoAF unaware of its function. This caused the American strike force to also receive a conventional role, which could be made public and be used as a public excuse to retain the operational level at Flesland.<ref>Skogrand & Tamnes: 213</ref>

[[File:Sgt. Jim Hendricks steadies an- Mark 82 bomb being carried by a bomb loader to an F-16 Fighting Falcon aircraft for loading DF-ST-82-07192.jpg|thumb|A [[Mark 82 bomb]] on a bomb loader during an American Cold War exercise at Flesland]]
The [[Norwegian High Command]] and the United States Air Force agreed on 29 May 1974 that there would be established nine [[collocated operating base]]s (COB) in Norway, including Flesland. This involved establishing nine air stations which in case of crisis and war could act as [[forward operating base]]s for about six hundred US aircraft. In addition to ground support for the aircraft and crew, it involved the stationing of spare parts, ammunition and fuel. The COBs were completed in 1985. By 1993 the US was no longer interested in them and Flesland lost this status from 1994.<ref>Arheim: 62</ref>

In early 1981 the [[United States Air Force]]'s [[4th Tactical Fighter Squadron]] visited Flesland. This was the first time operational [[General Dynamics F-16 Fighting Falcon]]s were deployed outside the United States.<ref>Arheim: 224</ref> A new control tower was taken into use in 1991.<ref>Avinor: 29</ref>

With the ending of the [[Cold War]] following the [[dissolution of the Soviet Union]] in 1991, the military activity at Flesland diminished.<ref name=o91 /> Planning of the downgrading of the air station had started in 1988, and from 1995 only personnel necessary for maintaining infrastructure was left, reducing the crew to 33. The final demise of the air station came in 1999, when all stationary assets were sold, including 30 vehicles. The daily operation of the air station was transferred to the [[Royal Norwegian Navy]], who have six employees at the base. Flesland Air Station has since only held mobilization status and will only be used by the air force in case of war and larger emergencies.<ref name=o96>Østerbø: 96</ref>

==Facilities==
[[File:US F-16 at Flesland.jpg|thumb|[[General Dynamics F-16 Fighting Falcon]]s of the [[United States Air Force]]'s [[4th Tactical Fighter Squadron]] in January 1981—the first ever overseas deployment of the F-16]]
Flesland is a joint military and civilian airport, situated at [[Flesland]], westernmost in the [[Ytrebygda]] borough of Bergen, Norway. The air station takes up the northeastern section of the aerodrome area, and shares the runway and taxiway system with the civilian section. Flesland Air Station has mobilization status and will under current plans only taken into use during war. In this state its limited facilities are maintained by the Royal Norwegian Navy, which have six employees working at the base.<ref name=o96 /> This consists of some activity in the COB hangar, the technical workshop and some storage ares. The RNoAF retains ownership of most of the aerodrome, including the squadron area, a buffer zone around it, as well as the runway and the parallel taxiway.<ref name=a8>Avinor: 8</ref>

The runway is designated 17/35, roughly north–south, and measures {{convert|2990|by|45|m|sp=us}}. This includes a {{convert|270|m|sp=us|adj=on}} overrun at each end which is only used under military rules. The overruns and {{convert|105|m|sp=us}} on the inside of each threshold are concrete—the rest being asphalt.<ref>Avinor: 20</ref> There is a parallel taxiway for the length of the runway.<ref>Avinor: 21</ref> Both directions have full category I instrument landing system, including [[precision approach path indicator]]. Both primary and secondary radar are installed.<ref>Avinor: 22</ref>

The squadron area features a series of [[hardened aircraft shelter]]s which are connected by means of a taxiway.<ref>Avinor: 44</ref> The centerpiece is a subterranean hall, the COB hangar, which has capacity for twenty-five fighter aircraft. These also include barracks and commando facilities.<ref name=o91>Østerbø: 91</ref>

==Future==
The [[Norwegian Defence Estates Agency]], the owner of the facility, stated in 2011 that they may in the future sell the property. This is part of a national coordination project where Avinor and the military have agreed to allow the primary user of each joint aerodrome to become the owner of the facility.<ref>Avinor: 9</ref>

Avinor estimates that the amount of traffic at Flesland will exceed the capacity of a single runway by 2025. The agency has therefore proposed that a second runway be built. Due to requirements of it being at least {{convert|1035|m|sp=us}} from the existing runway, the only suitable site is at the squadron area of the air station. It is proposed to have a length of {{convert|2360|m|sp=us}}.<ref>Avinor: 41</ref>

==References==
{{reflist|39em}}

==Bibliography==
{{commons category|Flesland Air Station}}
* {{cite book |last=Arheim |first=Tom |last2=Hafsten |first2=Bjørn |last3=Olsen |first3=Bjørn |last4=Thuve |first4=Sverre |title=Fra Spitfire til F-16: Luftforsvaret 50 år 1944–1994 |url=http://www.nb.no/nbsok/nb/2bb06c47357b69c0081d12c7bb647284 |location=Oslo |publisher=Sem & Stenersen |year=1994 |language=Norwegian |isbn=82-7046-068-0}}
* {{cite web|author=[[Avinor]] |url=http://www.avinor.no/tridionimages/Flesland%20Masterplan%202012%20-%20111006_tcm181-137285.pdf |title=Bergen lufthavn Flesland Masterplan 2012 |year=2011 |language=Norwegian |archivedate=2 October 2012 |accessdate=2 October 2012 |archiveurl=http://www.webcitation.org/6B7Rfdd3x?url=http://www.avinor.no/tridionimages/Flesland%20Masterplan%202012%20-%20111006_tcm181-137285.pdf |deadurl=no |df= }}
* {{cite book |last=Hafsten |first=Bjørn |last2=Arheim |first2=Tom |title=Marinens flygevåpen 1912–1944 |publisher=TankeStreken |year=2003 |isbn=82-993535-1-3 |language=Norwegian}}
* {{cite book |last=Skogrand |first=Kjetil |last2=Tamnes |first2=Rolf |title=Fryktens likevekt |publisher=[[Tiden Norsk Forlag]] |location=Oslo |year=2001 |isbn=82-10-04623-3 |language=Norwegian}}
* {{cite book |last=Østerbø |first=Kjell |title=Da Bergen tok av |year=2005 |publisher=Avinor |language=Norwegian |isbn=82-303-0495-5}}

{{Airports in Norway}}
{{Royal Norwegian Air Force}}
{{Portal bar|Aviation|Cold War|War|Norway}}

[[Category:Airports in Hordaland]]
[[Category:Royal Norwegian Air Force stations]]
[[Category:Military installations in Bergen]]
[[Category:Airports established in 1954]]
[[Category:1954 establishments in Norway]]