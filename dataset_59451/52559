{{Infobox journal
| title         = Physical Review E: Statistical, Nonlinear, Biological, and Soft Matter Physics 
| cover         = 
| editor        = [[Eli Ben-Naim]] 
| discipline    = [[Many-body theory|Many-body phenomena]]
| peer-reviewed = 
| abbreviation  = Phys Rev E Stat Nonlin Soft Matter Phys
| publisher     = [[American Physical Society]]  
| country       = United States
| frequency     = Monthly
| history       = 1993 to present
| openaccess    = Partial (see text)
| license       = 
| impact        = 2.288 
| impact-year   = 2014
| website       = http://journals.aps.org/pre/ 
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| formernames   = Physical Review,  Physical Review A: General Physics
| JSTOR         = 
| OCLC          = 45808357 
| LCCN          = 2001227060 
| CODEN         = PRESCM 
| ISSN          = 1539-3755 
| eISSN         = 1550-2376
}}

'''''Physical Review E: Statistical, Nonlinear, Biological, and Soft Matter Physics''''' is a [[peer review|peer-reviewed]], [[scientific journal]], published monthly by the [[American Physical Society]]. The main field of interest is [[Many-body theory|many-body phenomena]].  It is currently edited by [[Eli Ben-Naim]] of the [[Los Alamos National Laboratory]].<ref name=About/><ref name=Pubmed/> While original research content requires subscription, editorials, news, and other non-research content is [[open access (publishing)|openly accessible]].

==Scope==
Although the focus of this journal is [[Many-body theory|many-body phenomena]], the broad scope of the journal includes quantum chaos, soft matter physics, classical chaos, biological physics and granular materials.

Also emphasized are [[statistical physics]], equilibrium and transport properties of fluids, [[liquid crystal]]s, complex fluids, [[polymer]]s, chaos, [[fluid dynamics]], [[plasma (physics)|plasma physics]], [[classical physics]], and [[computational physics]].<ref name=About>[http://pre.aps.org/about  About Physical Review E]. APS. July 2010</ref><ref name=Pubmed>[http://www.ncbi.nlm.nih.gov/sites/entrez?db=journals&term=%22Phys%20Rev%20E%20Stat%20Nonlin%20Soft%20Matter%20Phys%22%5BTitle%20Abbreviation%5D  Pubmed Journals section]. NCBI, NLM, NIH. Department of Health & Human Services. July 2010.</ref>

==Former names==
This journal began as "''[[Physical Review]]''" in 1893. In 1913 the [[American Physical Society]] took over "''Physical Review''". In 1970 "''Physical Review''" was subdivided into Physical Review A, B, C, and D. From 1990 until 1993 a process was underway which split the journal then entitled  "'' [[Physical Review A: Atomic, Molecular and Optical Physics|Physical Review A: General Physics]]''" into two journals. Hence, from  1993 until 2000, one of the split off journals became '''''Physical Review E: Statistical Physics, Plasmas, Fluids, and Related Interdisciplinary Topics'''''. In 2001 the journal was changed, in name, to its present title. As an aside,  in January  2007, the section which published works on [[optics|classical optics]] was transferred ''from'' "''Physical Review E''" ''to'' "''Physical Review A''". This action unified the classical and quantum parts of optics into a single journal.<ref name=editorial-Drake07-2010>{{Cite web
  | last =Drake, Gordon W. F.
  | title =''Editorial: 40th Anniversary of Physical Review A'' 
  | publisher =American Physical Society 
  | date =July 1, 2010
  | url =http://publish.aps.org/edannounce/PhysRevA.82.010001
  | accessdate =2010-07-13}}</ref>

==Rapid Communications==
'''''Physical Review E Rapid Communications''''' was announced on June 7, 2010. This section (or feature) gives priority to results which are deemed significant, and merits a prominent display on the ''Physical Review E'' website. The specific article is displayed for several weeks, and is part of a rotation with other articles, also deemed significant.<ref name=rapid-comm>{{Cite web
  | last =Physical Review E
  | title =Highlighting PRE Rapid Communications  
  | publisher =American Physical Society 
  | date =June 7, 2010 
  | url =http://pre.aps.org/edannounce/pre-rapid-highlight 
  | accessdate =2010-07-05}}</ref>

==See also==
* ''[[Advanced Studies in Theoretical Physics]]''
* ''[[Annales Henri Poincaré]]''
* ''[[Applied Physics Express]]''
* ''[[American Journal of Physics]]''
* ''[[CRC Handbook of Chemistry and Physics]]''
* ''[[Physics Today]]''
* ''[[Journal of Physics A]]''
* ''[[Journal of Physical and Chemical Reference Data]]''
* ''[[European Physical Journal E]]''
* ''[[Physics Letters|Physics Letters A]]''

==Abstracting and indexing==
''Physical Review E'' is indexed in the following bibliographic databases:<ref name=About/><ref name=Pubmed/><ref>[http://science.thomsonreuters.com/cgi-bin/jrnlst/jlresults.cgi?PC=MASTER&ISSN=1539-3755 Master Journal List]. Thomson Reuters. 2011.</ref><ref name=cassi>[http://cassi.cas.org/publication.jsp?P=LglBQf5Q2NQyz133K_ll3zLPXfcr-WXfS-_PW8l9n4aUwvRl-5hXyTLPXfcr-WXfimSBIkq8XcUjhmk0WtYxmzLPXfcr-WXfJqUYTRc57wPYGZT3GiBOGA CAS Source Index (CASSI)]. American Chemical Society. January 2011.</ref>

*[[Science Citation Index Expanded]]
*[[Current Contents]] / Physical, Chemical & Earth Sciences
*[[Chemical Abstracts Service]] - [[CASSI]]
*[http://www.museumstuff.com/learn/topics/Current_Physics_Index Current Physics Index]
*[[Inspec]]
*[[MEDLINE]]
*[[Index Medicus]]
*[[Pubmed]]
*[[NLM catalog]]
*[http://www.absolute-inc.com/_/inventory/view.html?title=PHYSICS+ABSTRACTS+-+SERIES++A%3A+SCIENCE+ABSTRACTS&sng=true&cpt=true Physics Abstracts]
*[[SPIN bibliographic database|SPIN]]

==References==
{{Reflist}}

==External links==
*Editorial: [http://publish.aps.org/edannounce/PhysRevA.82.010001 40th Anniversary of ''Physical Review A'']. American Physical Society. July 1, 2010.

[[Category:Publications established in 2001]]
[[Category:Physics journals]]
[[Category:Fluid dynamics journals]]
[[Category:English-language journals]]
[[Category:Monthly journals]]
[[Category:American Physical Society academic journals]]