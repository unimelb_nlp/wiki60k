{{notability|Products|date=February 2014}}
{{primary sources|date=February 2014}}
{{Infobox programming language
| name                   = Axum
| logo                   = [[File:Axum Banner.png|150px]]
| screenshot             = 
| caption                = 
| developer              = [[Microsoft]]
| released               = May 8, 2009
| latest_release_version = [[Community Technology Preview]]
| latest_release_date    = {{Release date|2009|05|08}}
| latest_preview_version = 
| latest_preview_date    = 
| operating_system       = [[Windows XP]]<br />[[Windows Server 2003]]<br />[[Windows Vista]]<br />[[Windows Server 2008]]<br />[[Windows 7]]
| platform               = [[x86]], [[x86-64]] and [[Itanium]]
| license           = [[Closed source]]
| website                = [http://msdn.microsoft.com/en-us/devlabs/dd795202.aspx Axum at DevLabs]
}}
'''Axum''' (previously codenamed '''Maestro''') is a [[domain-specific language|domain specific]] [[concurrent programming|concurrent]] [[programming language]], based on the [[Actor model]], that was under active development by [[Microsoft]]<ref name="c9">{{cite web
|url=http://channel9.msdn.com/shows/Going+Deep/Maestro-A-Managed-Domain-Specific-Language-For-Concurrent-Programming/
|title=Maestro: A Managed Domain Specific Language For Concurrent Programming
|publisher=[[Channel 9 (discussion forum)|Channel 9]]
|accessdate=2009-04-16
| archiveurl= https://web.archive.org/web/20090407042640/http://channel9.msdn.com/shows/Going+Deep/Maestro-A-Managed-Domain-Specific-Language-For-Concurrent-Programming/| archivedate= 7 April 2009 <!--DASHBot-->| deadurl= no}}</ref> between 2009 and 2011.<ref name="shutdown">{{cite web
|url=http://blogs.msdn.com/b/maestroteam/archive/2011/02/28/the-state-of-axum.aspx
|title=The State of Axum
|publisher=Niklas Gustafsson
|accessdate=2011-02-28
}}</ref> It is an [[object-oriented programming|object-oriented language]] based on the [[.NET Framework|.NET]] [[Common Language Runtime]] using a [[C (programming language)|C]]-like syntax which, being a domain-specific language, is intended for development of portions of a software application that is well-suited to concurrency. But it contains enough general-purpose constructs that one need not switch to a general-purpose programming language (like [[C Sharp (programming language)|C#]]) for the sequential parts of the concurrent components.<ref name="c9"/>

The main idiom of programming in Axum is an ''Agent'' (or an ''Actor''), which is an isolated entity that executes in parallel with other Agents.<ref name="isolation">{{cite web
|url=http://blogs.msdn.com/maestroteam/archive/2009/02/27/we-haven-t-forgotten-about-other-models-honest.aspx
|title=We haven’t forgotten about other models – honest!
|publisher=[[MSDN]] blogs
|author=Philips Joshua
|accessdate=2009-04-16
| archiveurl= https://web.archive.org/web/20090508055636/http://blogs.msdn.com/maestroteam/archive/2009/02/27/we-haven-t-forgotten-about-other-models-honest.aspx| archivedate= 8 May 2009 <!--DASHBot-->| deadurl= no}}</ref> In Axum parlance, this is referred to as the agents executing in separate ''isolation domains'';<ref name="isolation"/> objects instantiated within a domain cannot be directly accessed from another.<ref>{{cite web
|url=http://blogs.msdn.com/maestroteam/archive/2009/02/27/isolation-in-maestro.aspx
|title=Isolation in Maestro
|publisher=[[MSDN]] blogs
|author=Niklas Gustafsson
|accessdate=2009-04-16
| archiveurl= https://web.archive.org/web/20090508000455/http://blogs.msdn.com/maestroteam/archive/2009/02/27/isolation-in-maestro.aspx| archivedate= 8 May 2009 <!--DASHBot-->| deadurl= no}}</ref> Agents are loosely coupled (i.e., the number of dependencies between agents is minimal) and do not share resources like memory (unlike the [[shared memory (interprocess communication)|shared memory]] model of [[C Sharp (programming language)|C#]] and similar languages); instead a [[message passing]] model is used.<ref name="isolation"/> To co-ordinate agents or having an agent request the resources of another, an explicit message must be sent to the agent. Axum provides ''Channels'' to facilitate this.

''Channels'' can be regarded as a directional pathway to communicate between agent instances. The member functions of a ''Channel'' object, after it has been bound to an agent instance, can be used to communicate with it. A ''Channel'' contains input and output ''ports'', which are [[queue (data structure)|queue]]s which are used to send data to an agent or receive data from one. To co-ordinate the communication between agents, Axum allows each channel to have a user-defined ''protocol'' for communication. The protocol is defined as a [[state machine]].<ref>{{cite web
|url=http://blogs.msdn.com/maestroteam/archive/2009/03/02/channels.aspx
|title=Channels
|publisher=[[MSDN]] blogs
|author=Niklas Gustafsson
|accessdate=2009-04-16
| archiveurl= https://web.archive.org/web/20090426212830/http://blogs.msdn.com/maestroteam/archive/2009/03/02/channels.aspx| archivedate= 26 April 2009 <!--DASHBot-->| deadurl= no}}</ref> The data sent over a channel can be optionally restricted to conform to a certain pre-defined ''schema''. The compiler and runtime will enforce the conformance with the schema.<ref>{{cite web
|url=http://www.langnetsymposium.com/2009/talks/32-JoshuaPhillips-Axum.html
|title=Axum
|author=Joshua Philips
|publisher=Lang.NET Presentation
|accessdate=2009-04-20
| archiveurl= https://web.archive.org/web/20090421173403/http://www.langnetsymposium.com/2009/talks/32-JoshuaPhillips-Axum.html| archivedate= 21 April 2009 <!--DASHBot-->| deadurl= no}}</ref> Under the hood, a ''schema'' is translated into a [[serialization|serializable]] .NET [[class (computing)|class]] that contains only properties and side effect-free methods.<ref name="objects">{{cite web
|url=http://blogs.msdn.com/maestroteam/archive/2009/04/20/objects-in-axum.aspx
|title=Objects in Axum
|publisher=[[MSDN]] Blogs
|accessdate=2009-04-20
|author=Niklas Gustafsson
| archiveurl= https://web.archive.org/web/20090426214328/http://blogs.msdn.com/maestroteam/archive/2009/04/20/objects-in-axum.aspx| archivedate= 26 April 2009 <!--DASHBot-->| deadurl= no}}</ref>

The Axum project reached the state of a prototype with working [[Microsoft Visual Studio]] integration.<ref>{{cite web
|url=http://blogs.zdnet.com/microsoft/?p=2522
|title=Axum: Another piece of Microsoft's parallel programming puzzle
|publisher=[[CNet]] blogs
|accessdate=2009-04-16
| archiveurl= https://web.archive.org/web/20090417203031/http://blogs.zdnet.com/microsoft/?p=2522| archivedate= 17 April 2009 <!--DASHBot-->| deadurl= no}}</ref> Microsoft had made a [[Community Technology Preview|CTP]] of Axum available to the public,<ref name="dnld">{{cite web
|url=http://msdn.microsoft.com/en-us/devlabs/dd795202.aspx
|title=Axum
|publisher = MSDN DevLabs
|accessdate=2009-05-09
| archiveurl= https://web.archive.org/web/20090511155406/http://msdn.microsoft.com/en-us/devlabs/dd795202.aspx| archivedate= 11 May 2009 <!--DASHBot-->| deadurl= yes}}</ref> but this has since been removed.
Although Microsoft decided not to turn Axum into a project,<ref name="shutdown"/> some of the ideas behind Axum are used in [[TPL Dataflow]] in .Net 4.5.

==References==
{{Reflist}}

==External links==
*[http://download.microsoft.com/download/B/D/5/BD51FFB2-C777-43B0-AC24-BDE3C88E231F/Axum%20Programmers%20Guide.pdf Programmers' Guide]
*Team blog - [http://blogs.msdn.com/maestroteam/default.aspx Axum: Isolation, Agents, and Message-passing in .NET]

{{Common Language Infrastructure}}

[[Category:.NET programming languages]]
[[Category:Object-oriented programming languages]]
[[Category:Concurrent programming languages]]