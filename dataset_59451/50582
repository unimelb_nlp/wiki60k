{{Infobox journal
| title = Archives of Biochemistry and Biophysics
| cover = [[File:ABB Front cover.gif]]
| editor = [[Paul Fitzpatrick (biochemist)|Paul Fitzpatrick]], [[Helmut Sies]], [[Jian-Ping Jin]], Henry Forman
| discipline = [[Biochemistry]], [[biophysics]]
| abbreviation = Arch. Biochem. Biophys.
| formernames = Archives of Biochemistry
| publisher = [[Elsevier]]
| country = [[United States]]
| frequency = Biweekly
| history = 1942–present
| openaccess = 
| license = 
| impact = 3.017
| impact-year = 2014
| website = http://www.journals.elsevier.com/archives-of-biochemistry-and-biophysics
| link1 = http://www.sciencedirect.com/science/journal/00039861
| link1-name = Online access
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 637968940
| LCCN = 44005905
| CODEN = ABBIA4
| ISSN = 0003-9861
| eISSN = 1096-0384
| ISSN2 = 1522-4724
| ISSN2label = ''Molecular Cell Biology Research Communications'':
}}
'''''Archives of Biochemistry and Biophysics''''' is a biweekly [[peer-reviewed]] [[scientific journal]] that covers research on all aspects of [[biochemistry]] and [[biophysics]]. It is published by [[Elsevier]] and {{As of|2012|lc=yes}}, the [[editors-in-chief]] are Paul Fitzpatrick ([[University of Texas Health Science Center at San Antonio]]), Helmut Sies ([[University of Düsseldorf]]), Jian-Ping Jin ([[Wayne State University School of Medicine]]), and Henry Forman ([[University of Southern California]]).<ref>{{cite web | url=http://www.journals.elsevier.com/archives-of-biochemistry-and-biophysics/editorial-board/ | publisher=Elsevier | title=Archives of Biochemistry and Biophysics Editorial Board | accessdate=April 16, 2014 | deadurl=no | archivedate=October 20, 2012 | archiveurl=https://web.archive.org/web/20121020165354/http://www.journals.elsevier.com/archives-of-biochemistry-and-biophysics/editorial-board/ }}</ref>

== History ==
The journal was established in 1942 by [[Academic Press]] as the '''''Archives of Biochemistry''''',<ref name=nlmcatAB>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/90832 |title=Archives of Biochemistry |accessdate=April 16, 2014 |website=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |deadurl=no<!--found in Internet Archive-->}}</ref> obtaining its current name in 1952.<ref name=nlmcatABB>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/0372430 |title=Archives of Biochemistry and Biophysics |accessdate=April 16, 2014 |website=NLM Catalog | publisher=National Center for Biotechnology Information | deadurl=no<!--found in Internet Archive--> }}</ref> It absorbed the journal ''Molecular Cell Biology Research Communications'' (formerly section B of ''[[Biochemical and Biophysical Research Communications]]''), which was published from 1999 to 2001.<ref name=nlmcatMCBRC>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/100889076 |title=Molecular Cell Biology Research Communications: MCBRC |accessdate=April 16, 2014 |website=NLM Catalog |publisher=National Center for Biotechnology Information |deadurl=no<!--found in Internet Archive-->}}</ref> An index to authors for the first 75 volumes, covering the period from 1943 to 1958, was published in October 1959.<ref>{{cite book |title=Books and Pamphlets: July to December 1959 |page=999 |url=https://books.google.com/books?id=aCIhAQAAIAAJ&dq=%22Archives%20of%20Biochemistry%20and%20Biophysics%22%20academic%20publication%20history&pg=PA999#v=onepage&q=%22Archives%20of%20Biochemistry%20and%20Biophysics%22%20academic%20publication%20history&f=false |publisher=Copyright Office, Library of Congress |year=1960 |volume=Volume 13, Part 1, Number 2 |series=Catalog of Copyright Entries: Third Series}}</ref>

== Abstracting and indexing ==
The journal is abstracted or indexed in [[Biological Abstracts]], [[Chemical Abstracts]], [[Current Contents]]/Life Sciences, [[EMBASE]], [[EMBiology]], [[Excerpta Medica]], [[Genetics Abstracts]], [[MEDLINE]], and the [[Science Citation Index]].<ref>{{cite web | url=http://www.elsevier.com/wps/find/journalabstracting.cws_home/622787/abstracting | title=Abstracting and Indexing | website=Archives of Biochemistry and Biophysics | publisher=Elsevier | accessdate=April 16, 2014 | deadurl=no<!--found in Internet Archive--> }}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 3.017.<ref name=WoS>{{cite book |year=2015 |chapter=Archives of Biochemistry and Biophysics|title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist|30em}}

== External links ==
*{{Official website|http://www.elsevier.com/locate/yabbi}}

{{DEFAULTSORT:Archives Of Biochemistry And Biophysics}}
[[Category:Elsevier academic journals]]
[[Category:Biophysics journals]]
[[Category:Publications established in 1942]]
[[Category:Biochemistry journals]]
[[Category:Biweekly journals]]