{{About|the roller coaster at [[Carowinds]]|the roller coaster previously called Afterburn|Flight Deck (California's Great America)}}
{{Good article}}
{{Use mdy dates|date=December 2013}}
{{DISPLAYTITLE:Afterburn (roller coaster)}}{{Infobox roller coaster
| name                 = Afterburn
| logo                 = <!--Use ONLY the filename, not a full [[File:]] link-->
| logodimensions       = <!--Must be expressed in pixels (px); default is 250px-->
| image                = Afterburn overview.jpg
| imagedimensions      = 250px
| caption              = An overview of Afterburn. Its lift hill, first drop and batwing can be seen in the foreground, with its vertical loop in the background.
| previousnames        = Top Gun: The Jet Coaster
| location             = Carowinds
| locationarticle      = <!--Must not be linked.-->
| section              = County Fair
| subsection           = <!--Should be linked.-->
| coordinates          = {{Coord|35.10025|-80.94092|format=dms|region:US-SC_type:landmark|display=title,inline}}
| status               = Operating
| opened               = {{Start date|1999|3|20}}
| soft_opened          = {{Start date|1999|3|16}}
| year                 = 1999
| closed               = 
| cost                 = [[US$]]10.5 million
| previousattraction   = The Wild Bull
| replacement          = 
| type                 = Steel
| type2                = Inverted
| type3                = <!--Must not be linked, will auto-categorize the coaster.-->
| manufacturer         = Bolliger & Mabillard
| designer             = [[Ing.-Büro Stengel GmbH]]
| model                = Inverted Coaster &ndash; Custom
| track                = 
| lift                 = [[Chain lift hill]]
| height_ft            = 113
| drop_ft              = <!--Must be expressed in feet and may contain only numeric characters.-->
| length_ft            = 2956
| speed_mph            = 62
| inversions           = 6
| duration             = 2:47
| angle                = <!--Do not include "degrees", it is added automatically.-->
| capacity             = 1380
| acceleration         = <!--Expression in full form e.g. "X to Y mph (χ to ψ km/h) in Z seconds". -->
| acceleration_from    = <!--Initial speed in mph or km/h defaults to zero, only numeric characters-->
| acceleration_mph     = <!--Final speed in mph may contain only numeric characters-->
| acceleration_km/h    = <!--Final speed in km/h may contain only numeric characters-->
| acceleration_in      = <!--Number of seconds may contain words -->
| gforce               = 
| restriction_in       = 54
| trains               = 2
| carspertrain         = 8
| rowspercar           = 1
| ridersperrow         = 4
| virtual_queue_name   = [[Fast Lane (Cedar Fair)|Fast Lane]]
| virtual_queue_image  = Cedar Fair Fast Lane availability icon.svg
| virtual_queue_status = available
| single_rider         = <!--Must be "available" if available.-->
| accessible           = <!--Must be "available" if available.-->
| transfer_accessible  = <!--Must be "available" if available.-->
| custom_label_1       = 
| custom_value_1       = 
| custom_label_2       = 
| custom_value_2       = 
| custom_label_3       = 
| custom_value_3       = 
| custom_label_4       = 
| custom_value_4       = 
| rcdb_number          = 527
}}

'''Afterburn''' is a steel [[inverted roller coaster]] located at [[Carowinds]] amusement park. After more than two years of planning and construction, the roller coaster opened on March 20, 1999. The ride previously operated as Top Gun: The Jet Coaster, before it was renamed Afterburn following [[Cedar Fair|Cedar Fair's]] purchase of the park in 2006.

Designed by [[Bolliger & Mabillard]], Afterburn stands {{convert|113|ft|m}} tall and reaches speeds of {{convert|62|mph|km/h}}. It features a {{convert|2956|ft|m|adj=mid|-long}} track and a nearly three-minute-long ride time. Afterburn has generally been well received, having been featured several times as a top 50 roller coaster in [[Amusement Today|Amusement Today's]] Golden Ticket Awards.

==History==
In early 1997, Paramount's Carowinds began consultations with roller coaster manufacturer [[Bolliger & Mabillard]] about adding a new ride to their park.<ref name=AB /> During 18 months of discussions, several designs and themes for the ride were developed, including theming it to the ''[[Godzilla (franchise)|Godzilla]]'' franchise.<ref name=AB /><ref name=RCDB /> In July 1998, the park announced the addition of Top Gun: The Jet Coaster for the 1999 season.<ref name=AB>{{cite journal|last=Muret|first=Don|title=Top Gun shoots down Wild Bull at Paramount's Carowinds Park|journal=Amusement Business|date=July 13, 1998|volume=110|issue=28|page=42}}</ref> At a cost of $10.5 million, it would be the single biggest investment in the park's history.<ref name=Rutherford /> The announcement followed [[Paramount Parks]] adding ''[[Top Gun (movie)|Top Gun]]''-themed inverted and [[suspended roller coaster]]s to several of their parks throughout the 1990s, including [[Flight Deck (California's Great America)|Paramount's Great America]], [[Flight Deck (Canada's Wonderland)|Paramount Canada's Wonderland]], and [[The Bat (Kings Island; opened 1993)|Paramount's Kings Island]].<ref name="CGA">{{cite RCDB|coaster_name=Flight Deck|location=California's Great America|rcdb_number=80|accessdate=November 2, 2013}}</ref><ref name=CW>{{cite RCDB|coaster_name=Flight Deck|location=Canada's Wonderland|rcdb_number=57|accessdate=November 2, 2013}}</ref><ref name="KI">{{cite RCDB|coaster_name=Bat|location=Kings Island|rcdb_number=73|accessdate=November 2, 2013}}</ref> To construct the attraction at Carowinds, the park's [[Bayern Kurve]] ride, Wild Bull, had to be removed. The park intended to keep the ride in storage for a potential relocation elsewhere within the park. The removal of Wild Bull shortened construction time because no trees or buildings had to be removed. Construction began in late July 1998, with the first track arriving in August. Testing was completed in January 1999.<ref name=AB />

Top Gun: The Jet Coaster [[soft opened]] to guests and media on March 16, 1999.<ref name="Flying high">{{cite news|title=Flying high|url=https://news.google.com/newspapers?id=Z5hIAAAAIBAJ&sjid=XAoNAAAAIBAJ&pg=2978%2C235801|accessdate=November 2, 2013|newspaper=The Post and Courier|date=March 17, 1999}}</ref> Riders of the first cycle included [[NASCAR]] drivers [[Stanton Barrett]], [[Tony Stewart]], [[Jerry Nadeau]], and [[Kenny Wallace]].<ref name=Rutherford>{{cite book|last=Rutherford|first=Scott|title=Carowinds|year=2013|publisher=Arcadia Publishing|isbn=9781467120036|pages=106–107}}</ref> The ride officially opened to the public on March 20, 1999.<ref name=RCDB>{{cite RCDB|coaster_name=Afterburn|location=Carowinds|rcdb_number=527|accessdate=November 2, 2013}}</ref> On July 1, 2006, the [[Cedar Fair|Cedar Fair Entertainment Company]] announced their acquisition of all of the Paramount Parks, including Carowinds.<ref name=Rutherford /> The aforementioned Top Gun roller coasters at other parks were all renamed to Flight Deck by the 2008 season, with Carowinds' installation being renamed to Afterburn.<ref name=RCDB /><ref name=CGA /><ref name=CW /><ref name=KI /><ref name="Carowinds slashing season ticket prices">{{cite news|last=O'Daniel|first=Adam|title=Carowinds slashing season ticket prices|url=http://www.heraldonline.com/2008/03/29/452709/carowinds-slashing-season-ticket.html|accessdate=November 2, 2013|newspaper=Herald Online|date=March 29, 2008}}</ref> For the 2014 season, the ride's 15th season, Afterburn received a new coat of paint.<ref>{{cite web|title=Timeline Photos|url=https://www.facebook.com/photo.php?fbid=10152053561438599&set=a.125492718598.106289.91085023598&type=1|publisher=Facebook|accessdate=December 13, 2013|author=Carowinds|date=December 11, 2013}}</ref>

==Characteristics==
{{Stack|[[File:Afterburn (Carowinds) 02.JPG|thumb|Afterburn's Immelmann]]}}
The {{Convert|2956|ft|m|adj=mid|-long}} Afterburn stands {{Convert|113|ft}} tall. The ride features a total of six [[Roller coaster inversion|inversions]] including a [[vertical loop]], an [[Immelmann loop]], a [[zero-g roll]], a [[Roller coaster elements#Batwing|batwing]], and a [[Corkscrew (roller coaster element)|corkscrew]]. Riders over {{Convert|54|in|cm}} achieve a top speed of {{Convert|62|mph}} on the two-minute forty-seven second ride. Afterburn operates with two [[Train (roller coaster)|trains]] featuring eight cars. Each car seats riders four across, for a total of 32 riders per train. The theoretical hourly capacity of the ride is 1,380 riders.<ref name=AB /><ref name=RCDB />

To match the original Top Gun theme, the ride's area was designed to feel like a military base. The main queue was housed in a [[hangar]] with a two-third scale [[F14 Tomcat]] fighter. Guests enter the ride area by passing through the batwing element.<ref name=AB />

==Ride experience==
As the train leaves the [[Station (roller coaster)|station]], a recorded message, "Clear for Takeoff," is played.<ref>{{cite news|title=Exhilarating Experience - 'Top Gun' Roller Coaster Fulfills Need For Speed - And Fear|newspaper=The State|date=March 20, 1999}}</ref> Afterburn then climbs its [[chain lift hill]], reaching a peak height of {{convert|113|ft|m}}. After a small [[pre-drop]], the roller coaster train then drops to the right before entering its first inversion, a vertical loop. Riders then travel through an underground trench and enter an Immelmann loop which sends the train in the opposite direction. The train then drops towards the ground, climbs back up, and spins through a [[zero-g roll]], an inverting element where riders experience the feeling of [[Air-time|weightlessness]]. Afterwards, the train dives back towards the ground and enters the two-inversion batwing element which crosses through a tunnel underneath the park's rear entrance.<ref name=RCDB /><ref name=POV>{{cite web|title=Afterburn Front Seat on-ride HD POV Carowinds|url=https://www.youtube.com/watch?v=WqdTMFC79bU|work=Coaster Force|publisher=YouTube|accessdate=November 5, 2013|date=January 26, 2011}}</ref>

As Afterburn exits the batwing, it climbs through a camelback hill over the station before entering its final inversion, a [[corkscrew (roller coaster element)|corkscrew]] to the right. The train then begins a 270-degree climbing helix to the left, after which it reaches the ride's [[roller coaster elements#Brake run|brake run]] and returns to the station.<ref name=RCDB /><ref name=POV />

==Reception==
[[File:Afterburn (Carowinds) 05.JPG|thumb|A train navigating Afterburn's zero-g roll]]
Afterburn has been well received. Arthur Levine of [[About.com]] stated the ride "is among the best inverted coasters" he's ridden, giving the ride a 4 out of 5 rating. Levine praises the ride for the lack of headbanging that results from over-the-shoulder-restraints (OTSR) on many roller coasters.<ref name=About.com>{{cite news|last=Levine|first=Arthur|title=Afterburn Roller Coaster at Carowinds Mini Review|url=http://themeparks.about.com/od/coasterridereviews/ss/Carowinds-Roller-Coasters.htm|accessdate=December 12, 2013|newspaper=About.com|publisher=The New York Times Company|year=2010}}</ref> The Coaster Critic describes the "pacing and order of inversions [as] near perfect", giving the ride an "excellent" rating of 9 out of 10.<ref>{{cite web|title=Afterburn @ Carowinds|url=http://www.thecoastercritic.com/2009/08/afterburn-carowinds-roller-coaster-reviews.html|publisher=The Coaster Critic|accessdate=December 12, 2013|date=August 14, 2009}}</ref>

Afterburn has appeared in ''[[Amusement Today]]''{{'}}s annual [[Golden Ticket Awards]] several times. It debuted at position 18 in 1999,<ref name="GTA1999" /> before disappearing from the poll for the three years to follow.<ref name="GTA2000" /><ref name="GTA2001" /><ref name="GTA2002" /> The ride returned to the poll, before hitting a low of 45 in 2012.<ref name="GTA2012" />

{{GTA table
| type       = steel
| accessdate = November 5, 2013
| df         = <!--Date format. Default is mdy, but it could be dmy.-->
| 1999       = 18
| 2000       = –
| 2001       = 34
| 2002       = 46
| 2003       = 39
| 2004       = 40
| 2005       = 29
| 2006       = 32
| 2007       = 41
| 2008       = 39
| 2009       = –
| 2010       = –
| 2011       = –
| 2012       = 45
| 2013       = 35
}}

In Mitch Hawker's worldwide Best Roller Coaster Poll, Afterburn entered at position 12 in 1999, before slowly declining to a low of 41 in 2012. The ride's ranking in subsequent polls is shown in the table below.<ref name=HawkerSteelPoll />
{{Mitch Hawker Poll table
| type       = steel
| accessdate = November 6, 2013
| 1999       = 12
| 2000       = &ndash;
| 2001       = 33
| 2002       = 33
| 2003       = 21
| 2004       = 20
| 2005       = 23
| 2006       = 29
| 2007       = 27
| 2008       = 34
| 2009       = 35
| 2010       = 31
| 2011       = &ndash;
| 2012       = 41
| 2013       = 41
}}

==References==
{{Reflist|30em}}

==External links==
{{Commons category|Afterburn (Carowinds)}}
*{{Official website|http://www.carowinds.com/rides/Thrill-Rides/Afterburn}}
*{{RCDB|527|Afterburn}}

{{Carowinds}}

[[Category:Carowinds]]
[[Category:Roller coasters in South Carolina]]
[[Category:Roller coasters introduced in 1999]]
[[Category:Roller coasters operated by Cedar Fair]]