{{pp-vandalism|small=yes}}
{{Infobox college coach
| name = Dabo Swinney
| image = Coach Dabo Swinney.jpg
| alt = 
| caption = Swinney coaching the Clemson Tigers in 2015
| sport = [[American Football|Football]]
| current_title = [[Head coach]]
| current_team = [[Clemson Tigers football|Clemson]]
| current_conference = [[Atlantic Coast Conference|ACC]]
| current_record = 89–28 <!-- As of games through 1/10/17 -->
| contract = $5.75 million 
| birth_date = {{Birth date and age|1969|11|20}}
| birth_place = [[Birmingham, Alabama]]
| death_date = 
| death_place =
| alma_mater = 
| player_years1 = 1990–1992
| player_team1 = [[Alabama Crimson Tide football|Alabama]]
| player_positions = [[Wide receiver]]
| coach_years1 = 1993–1995
| coach_team1 = [[Alabama Crimson Tide football|Alabama]] ([[Graduate assistant|GA]])
| coach_years2 = 1996
| coach_team2 = [[Alabama Crimson Tide football|Alabama]] (WR/TE)
| coach_years3 = 1997
| coach_team3 = [[Alabama Crimson Tide football|Alabama]] (TE)
| coach_years4 = 1998–2000
| coach_team4 = [[Alabama Crimson Tide football|Alabama]] (WR)
| coach_years5 = 2003–2006
| coach_team5 = [[Clemson Tigers football|Clemson]] (WR)
| coach_years6 = 2007–2008
| coach_team6 = [[Clemson Tigers football|Clemson]] (AHC/WR)
| coach_years7 = 2008
| coach_team7 = [[Clemson Tigers football|Clemson]] (interim HC / [[Offensive coordinator|OC]])
| coach_years8 = 2009–present
| coach_team8 = [[Clemson Tigers football|Clemson]]
| overall_record = 89–28 <!-- As of games through 1/10/17 -->
| bowl_record = 7–4
| tournament_record = 
| championships = 1 [[College football national championships in NCAA Division I FBS|National]] (2016)<br>3 [[Atlantic Coast Conference|ACC]] (2011, 2015, 2016)<br>5 [[Atlantic Coast Conference|ACC]] Atlantic Division (2009, 2011, 2012, 2015, 2016)
| awards = [[Bobby Dodd Coach of the Year Award|Bobby Dodd Coach of the Year]] (2011)<br />[[Atlantic Coast Conference football individual awards#Coach of the Year|ACC Coach of the Year]] (2015)<br />[[Home Depot Coach of the Year Award|Home Depot Coach of the Year]] (2015)<br />[[Walter Camp Coach of the Year]] (2015)<br/>[[Associated Press College Football Coach of the Year Award]] (2015)<br/>[[Maxwell Football Club Coach of the Year]] (2015)<br/>[[Paul "Bear" Bryant Award]] (2015)
| coaching_records =

}}
'''William Christopher "Dabo" Swinney''' (born November 20, 1969) is an American [[college football]] coach. He is the head football coach at [[Clemson University]].  Swinney took over as head coach for the [[Clemson Tigers football|Clemson Tigers]] midway through the 2008 season, following the resignation of [[Tommy Bowden]].  Swinney led the [[2016 Clemson Tigers football team]] to a victory in the [[2017 College Football Playoff National Championship]], capturing a [[College football national championships in NCAA Division I FBS|national championship]].

==Playing career==
Swinney was raised in [[Pelham, Alabama]], and attended the [[University of Alabama]], where he joined the [[Alabama Crimson Tide football|Crimson Tide football]] program as a [[walk-on (sports)|walk-on]] [[wide receiver]] in 1989. He earned a scholarship and lettered on three teams (1990–1992), including the Crimson Tide's [[1992 Alabama Crimson Tide football team|1992 National Championship team]]. During his time as an undergraduate at Alabama, Swinney was twice named an Academic All-[[Southeast Conference|SEC]] and SEC Scholar Athlete Honor Roll member.<ref>Clemson University Athletic Department, [http://www.clemsontigers.com/ViewArticle.dbml?DB_OEM_ID=28500&ATCLID=205529394 Dabo Swinney Biography]</ref> In three seasons at Alabama, he caught 7 passes for 81 yards.<ref>{{cite web|title=Dabo Sweeny Stats|url=http://www.sports-reference.com/cfb/players/dabo-swinney-1.html|website=Sports-Reference.com}}</ref> He received his degree in commerce & business administration in 1993 as well as a master's degree in business administration from Alabama in 1995.

==Assistant football coach==

===Alabama===
In December 1995, Swinney received his [[MBA]] from Alabama and became a full-time assistant coach for the Crimson Tide, in charge of wide receivers and tight ends.<ref>{{cite web|url=http://www.kevinturnerfoundation.org/template/advisory-board |title=Advisory Board |publisher=Kevin Turner Foundation |date= |accessdate=2014-07-15}}</ref> He was fired with all of head coach [[Mike DuBose]]'s staff in early 2001.<ref name=TBN082708>Jon Solomon, [http://www.al.com/sports/birminghamnews/index.ssf?/base/sports/121982524442980.xml&coll=2 Former Alabama player, assistant Dabo Swinney will face beloved Tide on Saturday], ''The Birmingham News'', August 27, 2008, Accessed October 13, 2008.</ref>

Swinney sat out the 2001 season while receiving his contractual payments from Alabama. His former strength coach at Alabama, [[Rich Wingo]], had become president of [[Birmingham, Alabama|Birmingham]]-based AIG Baker Real Estate and offered him a job. From April 2001 through February 2003, he did not coach and instead worked for AIG Baker Real Estate on development projects in Alabama.<ref name="TBN082708"/>

===Clemson===

In 2002, his former position coach at Alabama, [[Tommy Bowden]], made Swinney an offer to become an assistant coach for the wide receivers at [[Clemson University|Clemson]], and Swinney joined in 2003. He took over as [[Recruiting (college athletics)|Recruiting Coordinator]] from popular longtime coordinator [[Rick Stockstill]]. Swinney proved to be both an excellent wide receivers coach as well as recruiting coordinator, coaching [[Atlantic Coast Conference|ACC]]-leading receivers and being named one of the nation's top 25 recruiters in 2007 by [[Rivals.com]].<ref name="TBN082708"/>

==Head football coach==

=== 2008 ===
Swinney was named the interim head football coach on October 13, 2008, after previous head coach [[Tommy Bowden]] resigned six games into the [[2008 Clemson Tigers football team|2008 season]].<ref name=AP101308>[http://sportsillustrated.cnn.com/2008/football/ncaa/10/13/clemson.bowden.resigns/index.html Clemson's Bowden steps down], Associated Press, October 13, 2008, Accessed October 14, 2008.</ref> The Tigers had started the year ranked #9 in the preseason polls, but then went 3–3 (1–2 ACC) in their first six games.<ref name=ESPN101308a>Mark Schlabach, [http://sports.espn.go.com/ncf/news/story?id=3641028 Bowden ousted at Clemson; coach 'deserved' to be fired, QB says], ESPN.com, October 13, 2008, Accessed October 13, 2008.</ref>  At the time he was informed of his promotion, he was working with the wide receivers on their upcoming game.<ref name=ESPN101408a>Heather Dinich, [http://myespn.go.com/blogs/acc/0-2-420/Swinney-ready-to-move-forward-at--full-speed-.html Swinney ready to move forward at 'full speed'], ESPN.com, October 14, 2008, Accessed October 14, 2008.</ref>

With a reputation as a top-notch recruiter, Swinney was chosen over Clemson [[defensive coordinator]] [[Vic Koenning]] (former head coach of [[Wyoming Cowboys football|Wyoming]]), and associate head coach [[Brad Scott (American football)|Brad Scott]] (former head coach of [[South Carolina Gamecocks football|South Carolina]]).<ref name=ESPN101308b>Heather Dinich, [http://myespn.go.com/blogs/acc/0-2-414/Clemson-turns-to-Swinney-for-remainder-of-season.html Clemson turns to Swinney for remainder of season], ESPN.com, October 13, 2008, Accessed October 13, 2008.</ref>  Swinney's first actions as interim head coach were to fire offensive coordinator [[Rob Spence (American football)|Rob Spence]] and introduce a new tradition, the "Tiger Walk", where all players and coaches walk through the parking lot outside [[Memorial Stadium, Clemson|Memorial Stadium]] about two hours before a game as they head inside for final game preparations.<ref name=SI101608>[http://sportsillustrated.cnn.com/2008/football/ncaa/10/16/swinney.tigerwalk.ap/index.html Interim Clemson boss Swinney introduces 'Tiger Walk'], Associated Press, October 16, 2008, Accessed October 17, 2008.</ref>  On November 1, 2008, Swinney claimed his first victory as the Tigers' head coach by defeating Boston College, breaking Clemson's six-game losing streak against the Eagles. On November 29, 2008, Swinney coached Clemson to a 31–14 win over South Carolina in the [[Carolina–Clemson rivalry|annual rivalry game]], after which Clemson became bowl eligible. After a vote of confidence from athletic director [[Terry Don Phillips]], Swinney was formally named as Clemson's 27th head coach on December 1, 2008. In his first game as the full-time head coach, he lost the [[2009 Gator Bowl]] to the Nebraska Cornhuskers 26-21.

Swinney's recruiting reputation became evident when he produced five top-20 ESPN recruiting classes in a row, including top 10 classes in 2011 and 2012.<ref name="espn.go.com">{{cite web|last=Adelson |first=Andrea |url=http://espn.go.com/blog/acc/post/_/id/52357/clemson-now-a-top-15-recruiting-mainstay |title=Clemson now a Top 15 recruiting mainstay |publisher=ESPN |date=2013-02-14 |accessdate=2014-07-15}}</ref> As a result, Clemson was one of only 10 schools to be ranked in the top 20 of recruiting five years in a row (along with LSU, Alabama, Texas, USC, Florida, Georgia, Florida State, Ohio State and Oklahoma), and as of 2014 Swinney was one of only four active head coaches at the time accomplish the feat (along with [[Nick Saban]], [[Les Miles]], and [[Bob Stoops]]).<ref name="espn.go.com"/>

Despite his recruiting reputation, Swinney was an unpopular hire among most Clemson fans. Many fans and pundits criticized his lack of coordinator let alone head coaching experience, as well as the fact that he had been on the staff of fired coach Tommy Bowden.<ref name="postandcourier.com">http://www.postandcourier.com/sports/swinney-survived-rocky-start-as-head-coach-to-build-football/article_e64e0bda-d43c-11e6-8000-dfd1deec0887.html</ref><ref>http://www.12up.com/posts/4401081-espn-s-2008-rating-of-clemson-s-hire-of-dabo-swinney-is-hilariously-bad</ref>

=== 2009 ===
During the 2009 season, Swinney's first full season at the helm, Clemson was able to achieve several accomplishments. The 2009 team finished the season with a record of 9–5 (6–2 in ACC) to win the Atlantic Division title of the Atlantic Coast Conference. The 2009 season included three marquee wins: a win over #8 Miami (FL) in overtime on the road, a 16-point win over Florida State at home, and a win over [[Kentucky Wildcats football|Kentucky]] 21–13 in the 2009 Music City Bowl. Swinney coached the Clemson Tigers to a #24 AP Top 25 final season ranking for the 2009 football season.<ref>http://espn.go.com/college-football/rankings/_/year/2009</ref>

=== 2010 ===
In 2010, Swinney led Clemson to a 6–6 (4–4 in ACC) regular season. Of the 6 losses in the 2010 season, 5 were by less than 10 points and 4 were by 6 points or less. The season included close losses to Cam Newton and the eventual national champion Auburn Tigers (27–24 on the road in OT), and the eventual division champion Florida State Seminoles (16–13 on a 55-yard, time-expiring field goal on the road). After the conclusion of the regular season, many fans called for the firing of both Swinney and athletic director Terry Don Phillips.<ref name="postandcourier.com"/> Swinney would say years later he expected to be fired after the regular  season ended with a loss to South Carolina.<ref>http://www.goupstate.com/news/20170104/after-losing-season-in-2010-swinney-thought-run-was-over</ref> Don Phillips instead gave Swiney a vote of confidence and allowed him to return for the 2011 season.<ref>http://www.thestate.com/sports/college/acc/clemson-university/article124627299.html</ref> Discontent with Swinney grew even more after a bowl loss to South Florida made Clemson's final record 6-7, Clemson's first losing season since 1998. Swinney, who was 19-15, entered the 2011 season widely considered to be a coach on the hot seat.<ref>http://thebiglead.com/2011/01/19/five-prominent-college-football-coaches-on-the-hot-seat-for-2011/</ref> Despite a disappointing 6–7 record, the 2010 team featured one of the nation's top defenses and the Bronko Nagurski and Ted Hendricks award winner, Da'Quan Bowers.

=== 2011 ===
In 2011, Swinney led the Tigers to a 10–3 record that included an [[2011 ACC Championship Game|ACC Championship]], the Tigers' first since 1991. They earned a trip to the [[Orange Bowl]], their first major-bowl appearance since the 1981 national championship season. During a pre-game ESPN interview prior to the [[2012 Orange Bowl]], Swinney said, "Hopefully when this thing is over, people are going to be talking about the Clemson defense."<ref>[http://bleacherreport.com/articles/1459146-chick-fil-a-bowl-2012-les-miles-and-dabo-swinneys-5-best-presser-moments/page/3 Chick-fil-A Bowl 2012: Les Miles and Dabo Swinney's 5 Best Presser Moments]</ref> The comment proved to be prescient as #15 Clemson would go on to lose to the #23 West Virginia Mountaineers, 70–33, conceding an all-time record number of points scored in a quarter (35), half (49) and game (70) in the 109-year history of bowl games.<ref>{{cite news |title=West Virginia routs Clemson in record-setting Orange Bowl |url=http://sports.espn.go.com/ncf/recap?gameId=320040228 |agency=[[Associated Press]] |publisher=[[ESPN]] |date=2012-01-04 |accessdate=2012-01-21}}</ref> Defensive coordinator Kevin Steele was fired after the game.<ref>{{cite news |title=Kevin Steele out at Clemson |url=http://espn.go.com/college-football/story/_/id/7455008/kevin-steele-clemson-tigers-defensive-coordinator |date=2012-01-12 |first=Heather |last=Dinich |publisher=[[ESPN]] |accessdate=2012-01-21}}</ref>

Part of Swinney's success the past three years was the 2011 offensive coordinator hire of Chad Morris, who was originally seen as a risk as most of his coaching experience had been on the high school level. Morris brought in a fast-paced, up-tempo offense that shattered many Clemson offensive records.<ref>{{cite web|url=http://www.clemsontigers.com/ViewArticle.dbml?ATCLID=205529425 |title=Chad Morris Biography |publisher=Clemson Tigers Official Athletics Site |year=2014 |accessdate=2014-07-15}}</ref> Since then, Clemson's offense has averaged over 40 points a game and over 500 yards of total offense a game.<ref>{{cite web|first=Greg |last=Wallace |url=http://bleacherreport.com/articles/1898693-clemson-football-how-has-chad-morris-offense-evolved-in-the-last-3-years |title=Clemson Football: How Has Chad Morris' Offense Evolved in the Last 3 Years? |publisher=Bleacher Report |date=2013-12-23 |accessdate=2014-07-15}}</ref>

Swinney was the 2011 winner of the [[Bobby Dodd Coach of the Year Award]], which was established to honor the NCAA Division 1 football coach whose team excels on the field, in the classroom, and in the community. The award is named for [[Bobby Dodd]], longtime head football coach of the [[Georgia Tech Yellow Jackets football|Georgia Tech Yellow Jackets]]. The award was established in 1976 to honor the values that Dodd exemplified.<ref>{{cite news |title= Bobby Dodd Coach of the Year Foundation - Past Winners|url=http://www.bobbydoddfoundation.com/pastwinners2.html|publisher=Bobby Dodd Foundation |accessdate=2012-12-17}}</ref>

=== 2012 ===
In 2012, Swinney led Clemson to its first 11-win season since the 1981 national championship year, capping the year off with an upset victory over the #8 [[LSU Tigers]] in the [[2012 Chick-fil-A Bowl|Chick-fil-A Bowl]].<ref>{{cite web|url=http://scores.espn.go.com/ncf/recap?gameId=323660228 |title=LSU Tigers vs. Clemson Tigers - Recap - December 31, 2012 |publisher=ESPN |date=2012-12-31 |accessdate=2014-07-15}}</ref> The Tigers finished the year at 11-2, ranked 9th in the Coaches Poll and 11th in the AP poll. Swinney was a finalist for the third time in his career for the Liberty Mutual National Coach of the Year.<ref>{{cite web|url=http://www.clemsontigers.com/ViewArticle.dbml?ATCLID=205529394 |title=Dabo Swinney Biography |publisher=Clemson Tigers Official Athletics Site |date=2014-01-14 |accessdate=2014-07-15}}</ref>

=== 2013 ===
In 2013, Swinney guided the Tigers to their third 10-win season in a row, the first time since 1989.<ref>{{cite web|url=http://www.clemsontigers.com/ViewArticle.dbml?SPSID=657769&SPID=103701&DB_LANG=C&ATCLID=208830262&DB_OEM_ID=28500 |title=2013 Clemson Football Media Guide & Supplement |publisher=Clemson Tigers Official Athletics Site |date=2013-07-26 |accessdate=2014-07-15}}</ref> The highlight regular-season win came against #5 [[Georgia Bulldogs football|Georgia]] in the season opener. The Tigers won 38–35. Clemson's two regular season losses were to top 10 opponents, national champion [[Florida State Seminoles football|Florida State]] and [[South Carolina Gamecocks football|South Carolina]]. The 31-17 loss to the rival Gamecocks was a record fifth straight for the Tigers, the longest winning streak for South Carolina in the series.<ref>[http://www.usatoday.com/story/sports/ncaaf/2013/11/30/south-carolina-defeats-clemson-31-17/3793259/ South Carolina makes it five in a row against Clemson]</ref> The completion of the season marked 32 wins over three years for Swinney, the most ever in such a span in Clemson football history.<ref>{{cite web|url=http://athlonsports.com/college-football/ranking-all-35-college-football-bowls-2013-must-see-must-miss |first=Steven |last=Lassan |title=Ranking All 35 College Football Bowls for 2013: From Must-See to Must-Miss |publisher=AthlonSports.com |date=2013-12-09 |accessdate=2014-07-15}}</ref> The Tigers received their second BCS bowl bid under Swinney with an invitation to play seventh-ranked [[Ohio State Buckeyes football|Ohio State]] in the [[2014 Orange Bowl (January)|2014 Orange Bowl]].<ref>{{cite web|url=http://www.postandcourier.com/article/20131208/PC20/131209447/1032/clemson-ohio-state-to-lick-their-wounds-in-orange-bowl-showdown-per-espn-report |first=Aaron |last=Brenner |title=SOLID ORANGE: No. 12 Clemson or No. 7 Ohio State will redeem themselves in Orange Bowl clash |publisher=Post and Courier |date=2013-12-08 |accessdate=2014-07-15}}</ref> The Tigers defeated the Buckeyes 40–35 to give the Tigers' their third Orange Bowl win in their history and their first BCS bowl victory. The 2013 season marked the first time Clemson had back-to-back 11-win seasons. After the game, Swinney recalled the Tigers' lopsided loss two years before in the Orange Bowl and the team's journey since then. "Hey, listen: Two years ago we got our butts kicked on this field. And it has been a journey to get back. We're 22–4 since that night. And we are the first team from the state of South Carolina to ever win a BCS game," Swinney said.<ref>{{cite web|url=http://scores.espn.go.com/ncf/recap?gameId=340030194 |title=Clemson Tigers vs. Ohio State Buckeyes - Recap - January 03, 2014 |publisher=ESPN |date=2014-01-03 |accessdate=2014-07-15}}</ref> The win was Swinney's fourth victory over a top ten opponent as a head coach. The Tigers finished the season ranked in the top 10 in both polls (#8 in AP, #7 in Coaches), the first such achievement for Swinney as head coach.

Following the season, Swinney agreed to eight-year, $27.15 million contract and guaranteed if Swinney was fired in the next three years.<ref>[http://college-football.si.com/2014/01/18/dabo-swinney-clemson-contract/?sct=obnetwork "Clemson, coach Dabo Swinney agree on eight-year, $27.15 million contract."] SportsIllustrated.com. Retrieved 2014-Jan-18.</ref><ref>{{cite news|url=http://espn.go.com/college-football/story/_/id/10314032/dabo-swinney-clemson-tigers-gets-8-year-deal|title=Dabo Swinney gets 8-year deal|date=January 9, 2014|publisher=ESPN}}</ref>

=== 2014 ===
Under Swinney, Clemson had their fourth 10-win season in a row, making them one of only four schools to achieve the feat in the last four seasons.<ref name="tigernet.com">http://www.tigernet.com/update/player/Clemson-Oklahoma-Notes-18363</ref> The Tigers started the season ranked #16 but suffered early setbacks with losses to #13 [[Georgia Bulldogs football|Georgia]] and #1 [[Florida State Seminoles football|Florida State]].<ref>http://espn.go.com/college-football/team/_/id/228/clemson-tigers</ref> However, with the emergence of freshman quarterback [[Deshaun Watson]], the Tigers only lost one more game to ACC Coastal Division Champion [[Georgia Tech football|Georgia Tech]]. The regular season was highlighted with the finale against [[South Carolina Gamecocks football|South Carolina]] in which Clemson broke a 5-game losing streak to the Gamecocks to win 35–17 in Death Valley.<ref>http://espn.go.com/ncf/recap?id=400547750</ref> Clemson received an invitation to play [[Oklahoma Sooners football|Oklahoma]] in the Russell Athletic Bowl on Dec. 29, 2014. Led by Clemson's #1 ranked defense in the nation,<ref>http://www.greenvilleonline.com/story/sports/college/clemson/2014/11/23/clemson-football-team-will-enter-south-carolina-game-nations-top-ranked-defense/19459917/</ref> the Tigers routed the Sooners 40–6, holding Oklahoma to 275 total yards and forcing five turnovers.<ref>http://espn.go.com/ncf/boxscore?id=400610208</ref> Ironically, defensive coordinator [[Brent Venables]] had held the same position with the Sooners until coming to Clemson in 2012. The Tigers finished 10–3 for the season and ranked 15th in both the AP and the Coaches Poll.<ref name="ReferenceA">http://espn.go.com/college-football/rankings</ref>

Swinney's last three bowl wins have been over college programs that have all won national titles since 2000.<ref name="tigernet.com"/>

=== 2015 ===
Swinney completed his best season as Clemson's head coach in 2015, leading the Tigers to a 14-1 record with an ACC championship and an appearance in the national championship game. Clemson fell short to [[Alabama football|Alabama]] in the title game 45-40 after Nick Saban, the head coach of Alabama, surprised Clemson with a successful onside kick.<ref>http://espn.go.com/blog/ncfnation/post/_/id/124505/alabama-returns-to-top-of-college-football-world-with-45-40-national-championship-win-over-clemson</ref> The season marked Clemson's best run since the 1981 national championship season. The Tigers defeated #8 [[2015 North Carolina Tar Heels football team|North Carolina]] 45-37 to win their 15th ACC championship. Clemson also defeated #4 [[2015 Oklahoma Sooners football team|Oklahoma]] 37-17 in the Orange Bowl for its first college playoff appearance.<ref>http://collegefootball.ap.org/article/oklahomas-season-ends-semifinal-loss-clemson</ref> Swinney was named Associated Press Coach of Year, Walter Camp Coach of the Year, Home Depot Coach of the Year, and the Paul "Bear" Bryant Award.<ref>http://www.si.com/college-football/2015/12/21/clemson-tigers-dabo-swinney-ap-coach-year-award</ref> The 2015 Tigers set a record for single-season wins under Swinney with 14. Clemson ended the season ranked #2 in both the Associated Press and Coaches Polls.<ref name="ReferenceA"/>

=== 2016 ===

On April 12, 2016, Swinney signed a six-year contract extension with the Tigers.<ref>https://athleteshub.wordpress.com/2016/04/12/dabo-swinney-and-clemson-agree-on-extension/</ref> Swinney once again recorded a banner season as Clemson's coach, leading the Tigers to a 12-1 regular season record and another ACC Championship, the third in Swinney's career.<ref>http://www.sbnation.com/college-football/2016/12/3/13830730/clemson-virginia-tech-acc-championship-game-2016-final-score-results</ref> Clemson posted big wins during the 2016 season over #3 Louisville at home and #12 Florida State on the road. Swinney punctuated the regular season with a 56-7 home victory over arch-rival South Carolina, the largest margin of victory over the Gamecocks in Swinney's career and the largest in over 100 years in the history of the storied rivalry.<ref>http://www.espn.com/college-football/game?gameId=400869074</ref> Following Clemson's ACC title win over #19 Virginia Tech, the Tigers secured the #2 seed in the College Football Playoff. On Dec. 31, Swinney and the Tigers defeated #3 Ohio State 31-0 in Ohio State head coach Urban Meyer's first career shut-out to set up a rematch of the 2015 National Championship against #1 Alabama.<ref>https://www.nytimes.com/2016/12/31/sports/ncaafootball/clemson-ohio-state-fiesta-bowl.html</ref> On January 9, 2017 Swinney led the Tigers to a 35-31 comeback win over Alabama in the National Championship.

==Personal==
Swinney's [[nickname]] was given to him as an infant by his parents when his then-18-month-old brother would try to enunciate "dat boi" when referring to Swinney.<ref>Paul Strelow, [http://www.thestate.com/sports/story/495742.html Family Tides break upon Clemson coach], TheState.com, August 20, 2008, Accessed October 12, 2008.</ref> He married the former Kathleen Bassett in 1994 and has three sons.

==Head coaching record==
{{CFB Yearly Record Start |type=coach |conf=yes |bowl=yes |poll=both }}
{{CFB Yearly Record Subhead
 | name      = [[Clemson Tigers football|Clemson Tigers]]
 | conf      = [[Atlantic Coast Conference]]
 | startyear = 2008
 | endyear   =
}}
{{CFB Yearly Record Entry
 | year         = [[2008 NCAA Division I FBS football season|2008]]
 | name         = [[2008 Clemson Tigers football team|Clemson]]
 | overall      = 4–3<ref group="A">Took over midseason from [[Tommy Bowden]], who resigned on October 13, 2008</ref>
 | conference   = 3–2
 | confstanding = T–3rd <small>(Atlantic)</small>
 | bowlname     = [[2009 Gator Bowl|Gator]]
 | bowloutcome  = L
 | ranking      = 
 | ranking2     = 
}}
{{CFB Yearly Record Entry
 | championship = division
 | year         = [[2009 NCAA Division I FBS football season|2009]]
 | name         = [[2009 Clemson Tigers football team|Clemson]]
 | overall      = 9–5
 | conference   = 6–2
 | confstanding = 1st <small>(Atlantic)</small>
 | bowlname     = [[2009 Music City Bowl|Music City]]
 | bowloutcome  = W
 | ranking      = 
 | ranking2     = 24
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[2010 NCAA Division I FBS football season|2010]]
 | name         = [[2010 Clemson Tigers football team|Clemson]]
 | overall      = 6–7
 | conference   = 4–4
 | confstanding = T–4th <small>(Atlantic)</small>
 | bowlname     = [[2010 Meineke Car Care Bowl|Meineke Car Care]]
 | bowloutcome  = L
 | ranking      = 
 | ranking2     =
}}
{{CFB Yearly Record Entry
 | championship = conference
 | year         = [[2011 NCAA Division I FBS football season|2011]]
 | name         = [[2011 Clemson Tigers football team|Clemson]]
 | overall      = 10–4
 | conference   = 6–2
 | confstanding = 1st <small>(Atlantic)</small>
 | bowlname     = [[2012 Orange Bowl|Orange]]
 | bowloutcome  = L
 | bcsbowl      = yes
 | ranking      = 22
 | ranking2     = 22
}}
{{CFB Yearly Record Entry
 | championship = division
 | year         = [[2012 NCAA Division I FBS football season|2012]]
 | name         = [[2012 Clemson Tigers football team|Clemson]]
 | overall      = 11–2
 | conference   = 7–1
 | confstanding = T–1st <small> (Atlantic) </small>
 | bowlname     = [[2012 Chick-fil-A Bowl|Chick-fil-A]]
 | bowloutcome  = W
 | bcsbowl      = no
 | ranking      = 9
 | ranking2     = 11
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[2013 NCAA Division I FBS football season|2013]]
 | name         = [[2013 Clemson Tigers football team|Clemson]]
 | overall      = 11–2
 | conference   = 7–1
 | confstanding = 2nd <small> (Atlantic) </small>
 | bowlname     = [[2014 Orange Bowl (January)|Orange]]
 | bowloutcome  = W
 | bcsbowl      = yes
 | ranking      = 7
 | ranking2     = 8
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[2014 NCAA Division I FBS football season|2014]]
 | name         = [[2014 Clemson Tigers football team|Clemson]]
 | overall      = 10–3
 | conference   = 6–2
 | confstanding = 2nd <small> (Atlantic) </small>
 | bowlname     = [[2014 Russell Athletic Bowl|Russell Athletic]]
 | bowloutcome  = W
 | bcsbowl      = 
 | ranking      = 15
 | ranking2     = 15
}}
{{CFB Yearly Record Entry
 | championship = conference
 | year         = [[2015 NCAA Division I FBS football season|2015]]
 | name         = [[2015 Clemson Tigers football team|Clemson]]
 | overall      = 14–1
 | conference   = 8–0
 | confstanding = 1st <small>(Atlantic)</small>
 | bowlname     = [[2015 Orange Bowl|Orange]]<sup>†</sup>, '''L'''&nbsp;[[2016 College Football Playoff National Championship|CFP NCG]]
 | bowloutcome  = W
 | bcsbowl      = yes
 | ranking      = 2
 | ranking2     = 2
}}
{{CFB Yearly Record Entry
 | championship = national
 | year         = [[2016 NCAA Division I FBS football season|2016]]
 | name         = [[2016 Clemson Tigers football team|Clemson]]
 | overall      = 14–1 <!-- As of games through 1/10/17 -->
 | conference   = 7–1
 | confstanding = T–1st <small>(Atlantic)</small>
 | bowlname     = [[2016 Fiesta Bowl (December)|Fiesta]]<sup>†</sup>, '''W'''&nbsp;[[2017 College Football Playoff National Championship|CFP NCG]] 
 | bowloutcome  = W
 | bcsbowl      = yes
 | ranking      = 1
 | ranking2     = 1
}}
{{CFB Yearly Record Subtotal
 | name       = Clemson
 | overall    = 89–28 <!-- As of games through 1/10/17 -->
 | confrecord = 54–15
}}
{{CFB Yearly Record End
 | overall     = 89–28 <!-- As of games through 1/10/17 -->
 | bcs         = yes
 | poll        = two
 | polltype    = 
 | polltype2   = 
}}
{{reflist|group="A"}}

==References==
{{reflist|30em}}

==External links==
{{Commons category|Dabo Swinney}}
{{Portal|Biography|College football}}
* [http://www.clemsontigers.com/ViewArticle.dbml?DB_OEM_ID=28500&ATCLID=205529394 Clemson profile]
* {{CFBCR|5871|Dabo Swinney}}

{{Clemson Tigers football coach navbox}}
{{Atlantic Coast Conference football coach navbox}}
{{Navboxes
| title = Dabo Swinney—championships, awards, and honors
| list1 =
{{1992 Alabama Crimson Tide football navbox}}
{{2016 Clemson Tigers football navbox}}
{{Associated Press College Football Coach of the Year Award}}
{{Bobby Dodd Award}}
{{Home Depot Coach of the Year}}
{{Walter Camp Coach of the Year}}
{{George Munger Award}}
{{Sporting News College Football Coach of the Year}}
{{AFCA Coach of the Year}}
{{Bear Bryant Award}}
}}

{{DEFAULTSORT:Swinney, Dabo}}
[[Category:1969 births]]
[[Category:Living people]]
[[Category:American football wide receivers]]
[[Category:American real estate businesspeople]]
[[Category:Alabama Crimson Tide football coaches]]
[[Category:Alabama Crimson Tide football players]]
[[Category:Clemson Tigers football coaches]]
[[Category:People from Pelham, Alabama]]
[[Category:Sportspeople from Birmingham, Alabama]]
[[Category:Players of American football from Alabama]]