<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = Hanley <!--please avoid stating manufacturer in this field; it's stated two lines below -->
  |image = HPHanley.jpg
  |caption = <!--image caption; if it isn't descriptive it should be left blank-->
}}{{Infobox Aircraft Type
  |type = [[Torpedo bomber]]
  |manufacturer = [[Handley Page]]
|national origin =[[United Kingdom]]
  |designer = <!--only appropriate for individuals, not for project leaders or a company or corporation-->
  |first flight = 3 January 1922
  |introduced = <!--date the aircraft entered or will enter military or revenue service-->
  |introduction= <!--date the aircraft will enter military or revenue service, if the aircraft is still in development -->
  |retired = <!--date the aircraft left military or revenue service. If vague or multiples, it probably should be skipped-->
  |status = Prototype<!--in most cases, this field is redundant; use it sparingly-->
  |primary user = <!--please list only one, and don't use those tiny flags as they limit horizontal space-->
  |more users = <!--limited to three "more users" total. please separate with <br/>-->
  |produced = <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
  |number built = 3
  |unit cost = 
  |developed from = <!-- the aircraft which formed the basis for the topic type -->
  |variants with their own articles = [[Handley Page Hendon]]<!-- variants OF the topic type -->
}}
|}

The '''Handley Page Hanley''' was a [[United Kingdom|British]] [[torpedo bomber]] aircraft of the 1920s. A single-engine, single-seat [[biplane]] intended to operate from the [[Royal Navy]]'s [[aircraft carrier]]s, it was not successful, with only three aircraft being built.

==Design and development==
In late 1920, [[Handley Page]] started design of a new single-seat torpedo bomber to meet the requirements of [[List Of Air Ministry Specifications#1920-1929|Air Ministry Specification 3/20]] for a carrier-based aircraft to replace the [[Sopwith Cuckoo]], in competition with the [[Blackburn Dart]]. The resulting design, the Type T, (later known as the H.P.19) and named the Hanley was a single-engine biplane of wooden construction. It, like the Dart, was powered by a [[Napier Lion]] engine and had a crew of one. It had [[Folding wing|folding]] three-bay wings which were fitted with full-span [[leading edge slot]]s on both upper and lower wings in order to improve low-speed handling.<ref name="Mason Bomberp135-136">Mason 1994, p.135-136.</ref>

Three prototypes were ordered, the first of which ([[United Kingdom military aircraft serials|serialed]] ''N143'') flew on 3 January 1922.<ref name="Barnes p219">Barnes 1976, p.219.</ref> Initial testing revealed that performance was disappointing with low speed handling, and that the view from the [[cockpit]] was also poor.<ref name="Mason Bomber p136">Mason 1994, p.136.</ref> After being damaged in a crash landing, the first prototype was rebuilt with new wingtips, a revised two-bay wing and with the control cables for the [[Elevator (aircraft)|elevator]]s enclosed in the rear [[fuselage]] to reduce drag, flying in December 1922 as the Hanley Mark II.<ref name="Barnes p219"/> These changes improved performance, but handling was still poor. The third prototype was therefore fitted with revised slots, as well as the drag reduction changes tested on the Hanley Mark II, the revised aircraft being designated Hanley Mark III, demonstrating considerably improved handling.<ref name="Barnes p.221-222">Barnes 1976, p.221-222.</ref>

By the time that the Hanley Mark III was available for testing, the Dart, which was developed from Blackburn's earlier Swift, had already been ordered into service.<ref name="Mason Bomber p136"/>

==Specifications (Hanley III)==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane<!-- options: plane/copter -->
|jet or prop?=prop<!-- options: jet/prop/both/neither -->
|ref=''Handley Page Aircraft since 1907''.<ref name="Barnes p.229">Barnes 1976, p.229.</ref> 
|crew=one
|capacity=
|length main= 33 ft 4 in
|length alt= 10.17 m
|span main= 46 ft 0 in
|span alt= 14 m
|height main= 14 ft 2 in <ref>[http://www.handleypage.com/Aircraft_hp19.html Handley Page - Aircraft - H.P.19 Hanley] ''handleypage.com''. Retrieved 3 June 2008</ref>
|height alt= 4.32 m
|area main= 562 ft²
|area alt= 52.2 m²
|airfoil=
|empty weight main= 3,640 lb <ref>Hanley I</ref><ref name="Lewis Bomberp420-420">Lewis 1980, p.420-421</ref>
|empty weight alt= 1,655 kg
|loaded weight main= 6,444 lb
|loaded weight alt= 2,920 kg
|useful load main= <!-- lb -->
|useful load alt= <!-- kg -->
|max takeoff weight main= <!-- lb -->
|max takeoff weight alt= <!-- kg -->
|more general=
|engine (prop)=[[Napier Lion]] IIB
|type of prop= 12-cylinder water-cooled [[W engine]]
|number of props=1
|power main= 450 hp
|power alt= 336 kW
|power original=
|max speed main= 101 kn 
|max speed alt= 116 mph, 187 km/h
|cruise speed main= <!-- knots -->
|cruise speed alt= <!-- mph,  km/h -->
|never exceed speed main= <!-- knots -->
|never exceed speed alt= <!-- mph,  km/h -->
|stall speed main= 48 kn
|stall speed alt= 55 mph, 88 km/h
|range main= <!-- nm -->
|range alt= <!-- mi,  km -->
|ceiling main= 15,000 ft
|ceiling alt= 4,640 m
|climb rate main= <!-- ft/min -->
|climb rate alt=<!--  m/s -->
|loading main= 11.5 lb/ft²
|loading alt= 55.9 kg/m²
|thrust/weight=<!-- a unitless ratio -->
|power/mass main= 0.070 hp/lb
|power/mass alt= 0.12 kW/kg
|more performance=
|armament=1 × 18 in (457 mm) torpedo
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=*[[Handley Page Hendon]]<!-- related developments -->
|similar aircraft=*[[Blackburn Dart]]<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
|see also=<!-- other relevant information -->
}}

==References==

===Notes===
{{Reflist}}

===Bibliography===
{{refbegin}}
*Barnes, C.H. ''Handley Page Aircraft since 1907''. London:Putnam, 1976. ISBN 0-370-00030-7.
*Mason, Francis K. ''The British Bomber since 1914''. London:Putnam, 1994. ISBN 0-85177-861-5.
*Lewis, Peter. ''The British Bomber since 1914''. London:Putnam, Third edition, 1980. ISBN 0-370-30265-6.
*"[http://www.flightglobal.com/pdfarchive/view/1922/1922%20-%200697.html The Handley Page 'Hanley' Torpedo 'Plane]". ''[[Flight International|Flight]]''. 30 November 1922. Pages 697-702.
{{refend}}

==External links==
{{commons category|Handley Page Hanley}}
*[http://www.britishaircraft.co.uk/aircraftpage.php?ID=220 Handley Page Hanley]

{{Handley Page aircraft}}

[[Category:British bomber aircraft 1920–1929]]
[[Category:Handley Page aircraft|Hanley]]
[[Category:Biplanes]]
[[Category:Single-engined tractor aircraft]]
[[Category:Carrier-based aircraft]]