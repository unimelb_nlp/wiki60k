{{good article}}
{{Infobox hurricane season
| Basin=SWI
| Year=1992
| Track=1991-1992 South-West Indian Ocean cyclone season summary.jpg
| First storm formed=September 11, 1991
| Last storm dissipated=April 19, 1992
| Strongest storm name=Harriet-Heather
| Strongest storm pressure=930
| Strongest storm winds=90
| Average wind speed=10
| Total depressions=14
| Total storms=10, 1 unofficial
| Total hurricanes=3
| Total intense=1
| Fatalities=2
| Damages=
| five seasons=[[1989–90 South-West Indian Ocean cyclone season|1989–90]], [[1990–91 South-West Indian Ocean cyclone season|1990–91]], '''1991–92''', [[1992–93 South-West Indian Ocean cyclone season|1992–93]], [[1993–94 South-West Indian Ocean cyclone season|1993–94]]
| Australian season=1991–92 Australian region cyclone season
| South Pacific season=1991–92 South Pacific cyclone season
}}

The '''1991–92 South-West Indian Ocean cyclone season''' was an average cyclone season in which most storms remained over open waters. At the time, the season lasted from November 15, 1991, to April 30, 1992,<ref>{{cite report|author= Neal Dorst|author2=Anne-Claire Fontan|title=When is the hurricane season for each basin?|publisher=Météo France|accessdate=2014-01-01|url=http://www.meteo.fr/temps/domtom/La_Reunion/webcmrs9.0/anglais/faq/FAQ_Ang_G.html}}</ref> although this season began early when three tropical depressions formed before the official start. The second, designated Tropical Depression A2 by the [[Météo-France]] office (MFR) on [[Réunion]], passed north of Madagascar on October&nbsp;16 before weakening. The first named storm was Severe Tropical Storm Alexandra, which developed on December&nbsp;18 from the [[monsoon trough]]; many other storms during the year originated in this manner. Tropical Storm [[Bryna (given name)|Bryna]] was the only tropical storm of the season to make [[landfall (meteorology)|landfall]], having struck northeastern Madagascar on January&nbsp;2. The basin was most active in February, when five named storms developed, including Tropical Depression Elizabetha which struck western Madagascar. In early March, Cyclone Harriet entered the basin from the [[1991–92 Australian region cyclone season|Australian region]] and was renamed Heather. It intensified to peak winds of 165&nbsp;km/h (105&nbsp;mph), making Heather the strongest storm of the season. In April, another cyclone &ndash; Jane &ndash; crossed from the Australian region and was renamed Irna, which reentered the Australian region on April&nbsp;19 to end tropical activity within the basin.

==Season summary==
<center>
<timeline>
ImageSize = width:1000 height:210
PlotArea  = top:10 bottom:80 right:20 left:20
Legend    = columns:3 left:30 top:58 columnwidth:270

AlignBars  = early
DateFormat = dd/mm/yyyy
Period     = from:01/09/1991 till:01/06/1992
TimeAxis   = orientation:horizontal
ScaleMinor = grid:black unit:month increment:1 start:01/10/1991
Colors     =
  id:canvas value:gray(0.88)
  id:GP     value:red
  id:TD    value:rgb(0.38,0.73,1)   legend:Tropical_Depression
  id:TS    value:rgb(0,0.98,0.96)   legend:Moderate_Tropical_Storm
  id:ST    value:rgb(0.80,1,1)      legend:Severe_Tropical_Storm
  id:TC     value:rgb(1,1,0.80)      legend:Tropical_Cyclone
  id:IT    value:rgb(1,0.76,0.25)   legend:Intense_Tropical_Cyclone
  id:VI   value:rgb(1,0.38,0.38)   legend:Very_Intense_Tropical_Cyclone

Backgroundcolors = canvas:canvas

BarData =
  barset:Hurricane
  bar:Month

PlotData=
  barset:Hurricane width:11 align:left fontsize:S shift:(4,-4) anchor:till
  from:10/09/1991 till:13/09/1991 color:TS text:"01S"
  from:14/10/1991 till:22/10/1991 color:TD text:"A2 (TD)"
  from:16/10/1991 till:22/10/1991 color:TD text:"A3 (TD)"
  from:21/11/1991 till:29/11/1991 color:TS text:"A4 (MTS)"
  from:18/12/1991 till:29/12/1991 color:ST text:"Alexandra (STS)"
  from:25/12/1991 till:10/01/1992 color:TS text:"Bryna (MTS)"
  from:08/02/1992 till:14/02/1992 color:TS text:"Celesta (MTS)"
  barset:break
  from:16/02/1992 till:25/02/1992 color:TS text:"Davilia (MTS)"
  from:22/02/1992 till:26/02/1992 color:TS text:"Elizabetha (MTS)"
  from:23/02/1992 till:04/03/1992 color:TC text:"Farida (TC)"
  from:24/02/1992 till:04/03/1992 color:TS text:"Gerda (MTS)"
  from:26/02/1992 till:01/03/1992 color:TD text:"H1 (TD)"
  from:01/03/1992 till:07/03/1992 color:VI text:"Harriet-Heather (VITC)"
  from:14/04/1992 till:19/04/1992 color:TC text:"Jane-Irna (TC)"

  bar:Month width:5 align:center fontsize:S shift:(0,-20) anchor:middle color:canvas
  from:01/09/1991 till:01/10/1991 text:September
  from:01/10/1991 till:01/11/1991 text:October
  from:01/11/1991 till:01/12/1991 text:November
  from:01/12/1991 till:01/01/1992 text:December
  from:01/01/1992 till:01/02/1992 text:January
  from:01/02/1992 till:01/03/1992 text:February
  from:01/03/1992 till:01/04/1992 text:March
  from:01/04/1992 till:01/05/1992 text:April
  from:01/05/1992 till:01/06/1992 text:May

TextData =
   pos:(569,23)
   text:"(For further details, please see"
   pos:(713,23)
   text:"[[Tropical cyclone scales#Comparisons_across_basins|scales]])"
</timeline>
</center>

In general, [[sea surface temperature]]s were warmest near the [[equator]] in the northeast portion of the basin, and in the [[Mozambique Channel]] between Mozambique and Madagascar. During the season, the [[Météo-France]] office (MFR) on [[Réunion]] island issued warnings in tropical cyclones within the basin. The agency estimated intensity through the [[Dvorak technique]],<ref name="report">{{cite report|author=Guy Le Goff|year=1992|work=RSMC La Reunion|publisher=[[Météo-France]]|title=1991-1992 Cyclone Season|accessdate=2014-01-01|url=http://www.meteo.fr/temps/domtom/La_Reunion/webcmrs9.0/anglais/archives/publications/saisons_cycloniques/index19911992.html}}</ref> and warned on tropical cyclones in the region from the coast of Africa to 80°&nbsp;[[Longitude|E]], south of the equator.<ref>{{cite report|author=Philippe Caroff|date=June 2011|title=Operational procedures of TC satellite analysis at RSMC La Reunion|publisher=World Meteorological Organization|accessdate=2013-04-22|url=http://www.wmo.int/pages/prog/www/tcp/documents/RSMCLaReunionforIWSATC.pdf|format=PDF|display-authors=etal}}</ref> The [[Joint Typhoon Warning Center]] (JTWC), which is a joint [[United States Navy]]&nbsp;– [[United States Air Force]] task force, also issued tropical cyclone warnings for the southwestern Indian Ocean.<ref name="atcr">{{cite report|publisher=Joint Typhoon Warning Center|page=iii, 183–185|title=Annual Tropical Cyclone Report|accessdate=2014-01-31|url=http://www.usno.navy.mil/NOOC/nmfc-ph/RSS/jtwc/atcr/1992atcr.pdf|format=PDF}}</ref>

During the season, there was an [[El Niño]] event that caused [[tropical cyclogenesis]] in the southern hemisphere to shift more to the east. In addition, the [[monsoon trough]], which helped spawn most of the storms in the season, was weaker than normal in the Indian Ocean.<ref>{{cite journal|author=Jonathan Gill |title=The South Pacific and Southeast Indian Ocean Tropical Cyclone Season 1991–1992 |journal=Australian Meteorological Magazine |volume=43 |pages=181&nbsp;– 182 |publisher=Australian Bureau of Meteorology |url=http://www.bom.gov.au/amm/docs/1994/gill.pdf |format=PDF |accessdate=2012-07-11 |archiveurl=http://www.webcitation.org/69EEQ1ZrK?url=http%3A%2F%2Fwww.bom.gov.au%2Famm%2Fdocs%2F1994%2Fgill.pdf |archivedate=2012-07-17 |deadurl=no |df= }}</ref> The number of tropical depressions forming was above average, although there were fewer days than normal with tropical cyclone activity.<ref name="mwl"/>

==Systems==

===Severe Tropical Storm Alexandra===
{{Infobox hurricane small
|Basin=SWI
|Image=Alexandra Dec 20 1991 0918Z.png
|Track=Alexandra 1991 track.png
|Formed=December 18
|Dissipated=December 29
|10-min winds=56
|1-min winds=105
|Pressure=972
}}
On December&nbsp;17, the JTWC began monitoring a tropical depression about 700&nbsp;km (440&nbsp;mi) west of [[Diego Garcia]] in association with the [[intertropical convergence zone]] (ITCZ),<ref name="mwl"/><ref name="abt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1992 Alexandra (1991352S08066)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-01-01|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1991352S08066}}</ref> designating it as Tropical Cyclone 09S.<ref name="atcr"/> The next day, the MFR also began classifying the system. The nascent depression moved generally to the southeast, and later more to the south-southeast,<ref name="abt"/> ahead of an approaching [[trough (meteorology)|upper-level trough]].<ref name="report"/> On December&nbsp;20, the depression intensified into Tropical Storm Alexandra,<ref name="abt"/> and that day developed an ill-defined [[eye (cyclone)|eye]],<ref name="report"/> as well as good [[inflow (meteorology)|inflow]].<ref name="ddar">{{cite journal|page=2|author=Darwin Regional Specialised Meteorological Centre|publisher=Bureau of Meteorology|volume=10|number=12|date=December 1991|title=Darwin Tropical Diagnostic Statement|accessdate=2014-01-01|url=http://www.bom.gov.au/climate/ntregion/statements/tropical/dtds-199112.pdf|format=PDF}}</ref> The JTWC upgraded Alexandra to the equivalent of a minimal hurricane &ndash; with [[maximum sustained wind|1&#8209;minute sustained wind]]s of at least 120&nbsp;km/h (75&nbsp;mph) on December&nbsp;21, but MFR only estimated peak 10‑minute winds of 105&nbsp;km/h (65&nbsp;mph). Although the MFR estimated the storm subsequently weakened, the JTWC assessed that Alexandra continued to intensity to a peak 1‑minute intensity of 195&nbsp;km/h (120&nbsp;mph) on December&nbsp;22. Around that time, the cyclone had slowed and turned to the east.<ref name="abt"/> Increased shear caused gradual weakening, and the strengthening of the [[subtropical ridge]] turned Alexandra to the southwest.<ref name="ddar"/> The JTWC discontinued advisories on December&nbsp;26, and Alexandra dissipated three days later well to the east-northeast of [[Mauritius]], or about 1,600&nbsp;km (1,000&nbsp;mi) south of where it first formed.<ref name="abt"/>
{{Clear}}

===Moderate Tropical Storm Bryna===
{{Infobox hurricane small
|Basin=SWI
|Image=Bryna Jan 1 1992 0403Z.png
|Track=Bryna 1991 track.png
|Formed=December 25
|Dissipated=January 10
|10-min winds=39
|1-min winds=45
|Pressure=988
}}
The MFR began monitoring a tropical depression on December&nbsp;25 about 900&nbsp;km (550&nbsp;mi) east-northeast of the northern tip of Madagascar.<ref name="bbt"/> Initially moving eastward without any strengthening, the depression turned back to the west toward Madagascar on December&nbsp;28 due to a ridge.<ref name="mwl"/> Two days later, the JTWC also began tracking the system,<ref name="bbt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1992 Bryna (1991360S13058)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-01-01|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1991360S13058}}</ref> designating it as Tropical Cyclone 10S.<ref name="atcr"/> With warmer water temperatures but persistent wind shear,<ref name="mwl"/> the system strengthened into Tropical Storm Bryna on December&nbsp;31. At 1800&nbsp;[[Coordinated Universal Time|UTC]] that day, the MFR estimated peak 10‑minute winds of 70&nbsp;km/h (45&nbsp;mph). On January&nbsp;1, the JTWC estimated peak 1‑minute winds of 85&nbsp;km/h (50&nbsp;mph), before assessing that Bryna began weakening. The storm made [[landfall (meteorology)|landfall]] in the [[Sava Region]] of eastern Madagascar early on January&nbsp;2 and subsequently crossed the northern portion of the country. Although the JTWC discontinued advisories while Bryna was inland, the MFR continued tracking it, and the circulation emerged into the Mozambique Channel on January&nbsp;3,<ref name="bbt"/> moving around a ridge.<ref name="mwl"/> Bryna curved to the south, brushing [[Melaky]] before moving farther offshore. The MFR estimated a secondary peak intensity of 65&nbsp;km/h (40&nbsp;mph) on January&nbsp;7,<ref name="bbt"/> based on ship reports, although the structure was more [[subtropical cyclone|subtropical]] in nature.<ref name="mwl"/> Around that time, Br[[Bryna (given name)|Bryna]]yna was turning to the southeast, and the next day made a final landfall in southwestern Madagascar. After crossing the southern portion of the country, [[Bryna (given name)|Bryna]] moved over open waters, eventually turning back to the southwest before dissipating on January&nbsp;10.<ref name="bbt"/>

While moving over Madagascar, Bryna dropped heavy rainfall,<ref name="report"/> causing some damage and two deaths in [[Mahajanga]].<ref name="mwl"/>
{{Clear}}

===Moderate Tropical Storm Celesta===
{{Infobox hurricane small
|Basin=SWI
|Image=Celesta Feb 11 1992 1035Z.png
|Track=Celesta 1992 track.png
|Formed=February 8
|Dissipated=February 14
|10-min winds=43
|1-min winds=45
|Pressure=985
}}
After a period of inactivity lasting about a month, a tropical depression formed from the [[monsoon trough]] about 900&nbsp;km (560&nbsp;mi) northeast of Mauritius on February&nbsp;8.<ref name="cbt"/><ref name="fdar">{{cite journal|page=2|author=Darwin Regional Specialised Meteorological Centre|publisher=Bureau of Meteorology|volume=11|number=02|date=February 1992|title=Darwin Tropical Diagnostic Statement|accessdate=2014-01-01|url=http://www.bom.gov.au/climate/ntregion/statements/tropical/dtds-199202.pdf|format=PDF}}</ref> The system moved generally to the south and southeast, passing just east of [[Rodrigues]] on February&nbsp;10. Later that day, the depression intensified into Tropical Storm Celesta. On February&nbsp;11, both the JTWC and the MFR estimated peak winds of 80&nbsp;km/h (50&nbsp;mph). Celesta turned to the south and looped to the northwest, crossing over its track on February&nbsp;12. The storm dissipated on February&nbsp;14,<ref name="cbt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1992 Celesta (1992039S14063)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-01-01|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1992039S14063}}</ref> having succumbed to [[wind shear]].<ref name="fdar"/> Despite Celesta passing near Rodriduges, winds on the island did not exceed 19&nbsp;km/h (11&nbsp;mph).<ref name="mwl"/>
{{Clear}}

===Moderate Tropical Storm Davilia===
{{Infobox hurricane small
|Basin=SWI
|Image=Davilia Feb 22 1992 1007Z.png
|Track=Davilia 1992 track.png
|Formed=February 16
|Dissipated=February 25
|10-min winds=39
|1-min winds=35
|Pressure=988
}}
Two days after Celesta dissipated, another tropical depression formed in the same region on February&nbsp;16 from a broad [[low pressure area]] involving Celesta's remnants. It moved northeastward and later turned to the southeast due to a trough.<ref name="mwl"/><ref name="dbt"/> The new depression vacillated in intensity but remained weak. On February&nbsp;22, the JTWC also began issuing warnings on the depression,<ref name="dbt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1992 Davilia (1992048S18067)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-01-01|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1992048S18067}}</ref> designating it as Tropical Cyclone 19S.<ref name="atcr"/> On the next day, the MFR upgraded the depression to Tropical Storm Davilia, estimating peak winds of 70&nbsp;km/h (45&nbsp;mph). Continuing to the southeast, Davilia failed to intensify further due to persistent wind shear, and it dissipated on February&nbsp;25.<ref name="fdar"/><ref name="dbt"/>
{{Clear}}

===Moderate Tropical Storm Elizabetha===
{{Infobox hurricane small
|Basin=SWI
|Image=Elizabetha Feb 24 1992 1126Z.png
|Track=
|Formed=February 22
|Dissipated=February 26
|10-min winds=35
|1-min winds=45
|Pressure=992
}}
Late on February&nbsp;22, a tropical depression formed within a broad area of convection south of the [[Comoros]] in the Mozambique Channel. With warm water temperatures, it gradually intensified while moving east-southeastward, and MFR upgraded it to Tropical Storm Elizabetha on February&nbsp;24. After reaching winds of 65&nbsp;km/h (40&nbsp;mph), the storm weakened back to tropical depression status and turned sharply southward, making landfall just west of Mahajanga in western Madagascar. Elizabetha weakened further over land and dissipated on February&nbsp;26, producing wind gusts of 87&nbsp;km/h (54&nbsp;mph) at Mahajanga. The JTWC did not issue any advisories in the storm.<ref name="mwl"/><ref>{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1992 Elizabeta:Elizabetha (1992054S14044)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-01-01|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1992054S14044}}</ref>
{{Clear}}

===Tropical Cyclone Farida===
{{Infobox hurricane small
|Basin=SWI
|Image=Farida Feb 29 1992 1024Z.png
|Track=Farida 1992 track.png
|Formed=February 23
|Dissipated=March 4
|10-min winds=82
|1-min winds=120
|Pressure=941
}}
A tropical depression developed in the northeast portion of the basin on February&nbsp;23 within the monsoon trough,<ref name="fdar"/><ref name="fbt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1992 Farida (1992055S11086)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-01-01|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1992055S11086}}</ref> classified as Tropical Cyclone 22S by the JTWC.<ref name="atcr"/> For much of its duration, the storm moved southwestward due to weak steering currents.<ref name="fdar"/> It intensified into Tropical Storm Farida on February&nbsp;25. Two days later, the JTWC upgraded the storm to the equivalent of a minimal hurricane, and the next day the MFR upgraded Farida to tropical cyclone status. The cyclone intensified further, reaching peak 10‑minute winds of 150&nbsp;km/h (95&nbsp;mph) according to MFR, and peak 1‑minute winds of 220&nbsp;km/h (140&nbsp;mph) according to JTWC.<ref name="fbt"/> While near peak intensity, the cyclone had developed well-defined [[outflow (meteorology)|outflow]],<ref name="fdar"/> and around that time was [[Fujiwhara interaction|interacting]] with Tropical Storm Gerda to its northwest.<ref name="report"/> Due to increasing wind shear, Farida gradually weakened,<ref name="mdar">{{cite journal|page=2|author=Darwin Regional Specialised Meteorological Centre|publisher=Bureau of Meteorology|volume=11|number=03|date=March 1992|title=Darwin Tropical Diagnostic Statement|accessdate=2014-01-01|url=http://www.bom.gov.au/climate/ntregion/statements/tropical/dtds-199203.pdf|format=PDF}}</ref> first below tropical cyclone intensity on March&nbsp;1, and to tropical depression intensity the next day. Late in its duration, the system turned to the northwest before dissipating on March&nbsp;4.<ref name="fbt"/>
{{Clear}}

===Moderate Tropical Storm Gerda===
{{Infobox hurricane small
|Basin=SWI
|Image=Gerda Feb 29 1992 0322Z.png
|Track=Gerda 1992 track.png
|Formed=February 24
|Dissipated=March 4
|10-min winds=35
|1-min winds=35
|Pressure=992
}}
A tropical depression formed northwest of Reunion on February&nbsp;24, which moved generally eastward. The next day, the depression passed north of Reunion and Mauritius and south of [[St. Brandon]], before turning to the northeast.<ref name="gbt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1992 Gerda (1992055S17053)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-01-01|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1992055S17053}}</ref> The system was located near another tropical depression to the northeast,<ref name="report"/> and both vortexes were tracked for several days.<ref name="mwl"/> On February&nbsp;26, the JTWC began issuing advisories on Tropical Cyclone 24,<ref name="atcr"/> although the agency estimated the system was located farther to the north,<ref name="gbt"/> closer to where the other tropical depression was.<ref name="h1"/> The JTWC assessed tropical storm intensity on February&nbsp;27, noting the system was moving generally southeastward, although the MFR tracked the depression as executing a loop before turning to the southeast. On February&nbsp;29, the MFR estimated peak 10‑minute sustained winds of 65&nbsp;km/h (40&nbsp;mph), upgrading it to Tropical Storm Gerda. At the same time, the JTWC had downgraded Gerda to a tropical depression. The system turned to the southwest, executing another large loop before resuming its southwest trajectory on March&nbsp;1. That day, Gerda weakened to a tropical depression, before executing a third loop to the northeast and later to the southeast, passing between Mauritius and Rodrigues. On March&nbsp;4, Gerda dissipated,<ref name="gbt"/> having produced gusts to 100&nbsp;km/h (62&nbsp;mph) on Rodrigues.<ref name="mwl"/>
{{Clear}}

===Intense Tropical Cyclone Harriet-Heather===
{{Infobox Hurricane Small
|Basin=SWI
|Image=Harriet-Heather Mar 1 1992 0832Z.png
|Track=Harriet 1992 track.png
|Formed=March 1 <small>([[1991-92 Australian region cyclone season|Entered basin]])</small>
|Dissipated=March 7 <small>(Out of area)</small>
|10-min winds=90
|1-min winds=120
|Pressure=930
}}
The monsoon trough spawned a tropical low about {{convert|550|km|mi|abbr=on}} east of the [[Cocos Islands]] on February&nbsp;24. Located in the Australian basin, it quickly intensified to tropical storm status and was named "Harriet" by the BoM,<ref name="har"/> and Tropical Cyclone 20S by the JTWC.<ref name="atcr"/> After passing just south of [[North Keeling Island]], Harriet turned more to the southwest, and strengthened into a Category&nbsp;5 on the [[tropical cyclone scales#Australia and Fiji|Australian tropical cyclone intensity scale]] on March&nbsp;1.<ref name="har">{{cite report|publisher=Bureau of Meteorology|title=Severe Tropical Cyclone Harriet|accessdate=2014-01-01|url=http://www.bom.gov.au/cyclone/history/harriet92.shtml}}</ref> By that time, the MFR had begun issuing advisories, and later that day renamed Harriet as Heather after the cyclone crossed into the south-west Indian Ocean. When Heather reached the basin, the JTWC was estimating peak 1‑minute winds of 220&nbsp;km/h (140&nbsp;mph), and shortly thereafter the MFR estimated peak 10‑minute winds of 165&nbsp;km/h (105&nbsp;mph).<ref name="hbt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1992 Harriet:Harriet_Heat:Heather (1992056S11102)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-01-01|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1992056S11102}}</ref>

On March&nbsp;4, an approaching trough turned the cyclone to the south and southeast, producing stronger wind shear that induced weakening.<ref name="har"/> By that time, the JTWC estimated Heather had weakened to a 1‑minute intensity of 150&nbsp;km/h (90&nbsp;mph). The cyclone reintensified slightly while accelerating to the southeast, and MFR estimated Heather re-attained its peak of 165&nbsp;km/h (105&nbsp;mph) on March&nbsp;5. Soon after, the storm began weakening again, and on March&nbsp;7 Heather exited into the Australian region below tropical cyclone status.<ref name="hbt"/> The cyclone continued to the southeast, becoming extratropical on March&nbsp;8 and dissipating the next day in the [[Great Australian Bight]].<ref name="har"/>
{{Clear}}

===Tropical Cyclone Jane-Irna===
{{Infobox hurricane small
|Basin=SWI
|Image=Jane Apr 13 1992 2115Z.png
|Track=Jane-Irna 1992 track.png
|Formed=April 14 <small>(Entered basin)</small>
|Dissipated=April 19 <small>(Exited basin)</small>
|10-min winds=75
|1-min winds=120
|Pressure=950
}}
The final storm of the season formed in the Australian basin, and like Harriet-Heather also originated out of the monsoon trough. On April&nbsp;7, a tropical low developed northeast of the Cocos Islands, and gradually intensified while moving southward, becoming Tropical Cyclone Jane the next day.<ref name="jane"/> The JTWC classified it as Tropical Cyclone 29S.<ref name="atcr"/> A [[ridge (meteorology)|ridge]] to the south turned the storm to the west on April&nbsp;11. The cyclone intensified further, developing an eye, and crossed into the south-west Indian Ocean on April&nbsp;13.<ref name="jane">{{cite report|publisher=Bureau of Meteorology|title=Severe Tropical Cyclone Jane|accessdate=2014-01-01|url=http://www.bom.gov.au/cyclone/history/jane.shtml}}</ref> At that time, the MFR renamed Jane as Tropical Cyclone Irna. Late on April&nbsp;13, the agency estimated peak 10‑minute sustained winds of 140&nbsp;km/h (85&nbsp;mph). The next day, the JTWC estimated Irna reached peak 1‑minute winds of 220&nbsp;km/h (140&nbsp;mph).<ref name="ibt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1992 Irna:Jane:Jane_Irna (1992096S05101)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-01-01|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1992096S05101}}</ref> Subsequently, an approaching trough turned the cyclone to the south and increased wind shear, which caused weakening. The eye dissipated, and Irna soon weakened below tropical cyclone status.<ref name="jane"/> On February&nbsp;17, Irna weakened into a tropical depression. The next day, the JTWC discontinued advisories, and on April&nbsp;19, Irna crossed back into the Australian region, dissipating on April&nbsp;20.<ref name="ibt"/>
{{Clear}}

===Other systems===
[[File:02S Oct 19 1991 1121Z.png|right|thumb|Satellite image of Tropical Depression A2 passing north of Madagascar in late October]]
In addition to the named storms, there were several other storms during the season that were warned on.<ref name="report"/> On September&nbsp;10, 1991, the JTWC began monitoring a tropical depression in the northeastern portion of the basin. The system moved southeastward, and according to the agency intensified into a 85&nbsp;km/h (50&nbsp;mph <small>1‑minute sustained</small>) tropical storm on September&nbsp;12. By the next day, the storm dissipated after turning back to the north.<ref>{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1992 HSK0192 (1991253S09080)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-01-01|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1991253S09080}}</ref>

About a month after the previous storm dissipated, the JTWC classified another tropical depression on October&nbsp;11 about 960&nbsp;km (600&nbsp;mi) east-southeast of Seychelles. The system tracked west-southwestward, and the MFR also began issuing warnings on it as Tropical Depression A2 on October&nbsp;14. Two days later, the JTWC briefly upgraded the depression to a tropical storm, although MFR only estimated peak 10‑minute winds of 50&nbsp;km/h (30&nbsp;mph). By that time, the storm had turned to the west, passing north of the northern tip of Madagascar. After turning to the west-northwest, the depression dissipated on October&nbsp;22.<ref>{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1992 A29192:HSK0292 (1991285S08064)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-01-01|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1991285S08064}}</ref> While the depression was active, the MFR also briefly tracked another tropical depression to its northeast, which formed on October&nbsp;16 about 540&nbsp;km (345&nbsp;mi) south-southeast of Diego Garcia. Classified as Tropical Depression A3 by MFR, the depression tracked to the west-southwest, turned to the southeast, and resumed its westward trajectory for several days before dissipating on October&nbsp;22.<ref>{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1992 A39192 (1991290S10077)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-01-01|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1991290S10077}}</ref>

On November&nbsp;20, the JTWC began tracking Tropical Cyclone 04S a short distance southeast of Diego Garcia. The next day, MFR also initiated advisories on it, classifying the system as Tropical Depression A4. After moving southwestward initially, the storm curved south-southeastward and intensified. The JTWC estimated peak 1‑minute winds of 85&nbsp;mph (50&nbsp;mph), and the MFR estimated 10‑minute winds of 65&nbsp;km/h (40&nbsp;mph),<ref name="a4">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1992 A49192:HSK0491 (1991324S08074)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-01-01|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1991324S08074}}</ref> indicating that it could have been named.<ref name="mwl">{{cite journal|hdl=2027/uiug.30112104094039|title=Saison Cyclonique 1991-92|author=Service Meteorlogique de la Reunion|journal=Mariners Weather Log|volume=37|number=2|date=Spring 1993|pages=60–63}}</ref> While near peak intensity, the storm was affected by wind shear that displaced the circulation center along the north edge of the convection.<ref name="report"/> The storm turned to the west, passing just south of St. Brandon before dissipating on November&nbsp;29.<ref name="a4"/>

While storms Farida and Gerda were both active, Tropical Depression H1 formed east of Madagascar on February&nbsp;26, and throughout its duration moved in a circular direction around Gerda.<ref name="report"/> The depression moved to the east but slowly executed a clockwise loop around St. Brandon. It never intensified beyond winds of 55&nbsp;km/h (35&nbsp;mph), and dissipated on March&nbsp;1.<ref name="h1">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1992 H19192 (1992057S17052)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-01-01|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1992057S17052}}</ref> On March&nbsp;7, the MFR identified a [[subtropical cyclone]] in the Mozambique Channel, but did not issue warnings on it.<ref name="report"/>

==Season effects==
This table lists all the cyclones that developed in the Indian Ocean, during the 1993–94 South-West Indian Ocean cyclone season. It includes their intensity, duration, name, landfalls, deaths, and damages.

{{Pacific areas affected (Top)}}
|-
| {{Sort|01|'''Alexandra'''}} || December 18 – 29  || bgcolor=#{{storm colour|STS}}|{{sort|2|Severe Tropical Storm}} || bgcolor=#{{storm colour|STS}}|{{sort|0105|105&nbsp;km/h (65&nbsp;mph)}} || bgcolor=#{{storm colour|STS}}|972&nbsp;hPa (28.7&nbsp;inHg)  || None || None || None ||
|-
| {{Sort|02|'''Bryna'''}} || December 25 – January 10  ||  bgcolor=#{{storm colour|TS}}|{{Sort|1|Moderate Tropical Storm}} || bgcolor=#{{storm colour|TS}}|{{sort|070|70&nbsp;km/h (45&nbsp;mph)}} || bgcolor=#{{storm colour|TS}}|988 hPa (29.17 inHg) || Tromelin, Madagascar || || || 
|-
| {{Sort|03|'''Celesta'''}} || February 8 – 14  ||  bgcolor=#{{storm colour|TS}}|{{Sort|1|Moderate Tropical Storm}} || bgcolor=#{{storm colour|TS}}|{{sort|080|80&nbsp;km/h (50&nbsp;mph)}} || bgcolor=#{{storm colour|TS}}|985 hPa (29.08 inHg) || Rodrigues || || || 
|-
| {{Sort|04|'''Davilia'''}} || February 16 – 25 ||  bgcolor=#{{storm colour|TS}}|{{Sort|1|Moderate Tropical Storm}} || bgcolor=#{{storm colour|TS}}|{{sort|070|70&nbsp;km/h (45&nbsp;mph)}} || bgcolor=#{{storm colour|TS}}|988 hPa (29.17 inHg) || || || || 
|-
| {{Sort|05|'''Elizabetha'''}} || February 22 – 26 ||  bgcolor=#{{storm colour|TS}}|{{Sort|1|Moderate Tropical Storm}} || bgcolor=#{{storm colour|TS}}|{{sort|065|65&nbsp;km/h (40&nbsp;mph)}} || bgcolor=#{{storm colour|TS}}|992 hPa (29.29 inHg) || Madagascar || || || 
|-
| {{Sort|06|'''Farida'''}} || February 23 – March 4 || bgcolor=#{{storm colour|TC}}|{{Sort|3|Tropical Cyclone}} || bgcolor=#{{storm colour|TC}}|{{sort|150|150&nbsp;km/h (95&nbsp;mph)}} || bgcolor=#{{storm colour|TC}}|941 hPa (27.78 inHg) ||  ||  ||  ||
|-
| {{Sort|07|'''Gerda'''}} || February 24 – March 4 ||  bgcolor=#{{storm colour|TS}}|{{Sort|1|Moderate Tropical Storm}} || bgcolor=#{{storm colour|TS}}|{{sort|065|65&nbsp;km/h (40&nbsp;mph)}} || bgcolor=#{{storm colour|TS}}|992 hPa (29.29 inHg) || Mauritius, Rodrigues || || || 
|-
| {{Sort|08|'''Harriet-Heather'''}} || March 1 (Entered basin) – 7 (Exited basin) || bgcolor=#{{storm colour|VITC}}|{{Sort|4|Very Intense Tropical Cyclone}} || bgcolor=#{{storm colour|VITC}}|{{Sort|165|165&nbsp;km/h (105&nbsp;mph)}} || bgcolor=#{{storm colour|VITC}}|930 hPa (27.46 inHg) ||  || || ||
|-
| {{Sort|09|'''Jane-Irna'''}} || April 14 (Entered basin) – 19 (Exited basin) || bgcolor=#{{storm colour|TC}}|{{Sort|3|Tropical Cyclone}} || bgcolor=#{{storm colour|TC}}|{{sort|140|140&nbsp;km/h (85&nbsp;mph)}} || bgcolor=#{{storm colour|TC}}|950 hPa (28.05 inHg) ||  ||  ||  || 
|-
{{TC Areas affected (Bottom)|TC's=9&nbsp;systems|dates=December 181&nbsp;– April 19|winds=165&nbsp;km/h (105&nbsp;mph)|pres=930&nbsp;hPa (27.46&nbsp;inHg)|damage=none|deaths=none|Refs=}}

==See also==
{{Portal|Tropical cyclones}}
*[[List of Southern Hemisphere tropical cyclone seasons]]
<!--*[[List of South Pacific cyclone seasons]]-->
*Atlantic hurricane seasons: [[1991 Atlantic hurricane season|1991]], [[1992 Atlantic hurricane season|1992]]
*Pacific hurricane seasons: [[1991 Pacific hurricane season|1991]], [[1992 Pacific hurricane season|1992]]
*Pacific typhoon seasons: [[1991 Pacific typhoon season|1991]], [[1992 Pacific typhoon season|1992]]
*North Indian Ocean cyclone seasons: [[1991 North Indian Ocean cyclone season|1991]], [[1992 North Indian Ocean cyclone season|1992]]

==References==
{{reflist|2}}

{{TC Decades|Year=1990|basin=South-West Indian Ocean|type=cyclone|shem=yes}}

{{DEFAULTSORT:1991-92 South-West Indian Ocean cyclone season}}
[[Category:South-West Indian Ocean cyclone seasons]]
[[Category:1991–92 South-West Indian Ocean cyclone season]]
[[Category:Articles which contain graphical timelines]]