{{Infobox Journal|
title          = Bibliotheca Sacra|
cover          = [[File:Bib Sac.jpg|200px]]|
editor         = Larry J. Waters|
frequency      = Quarterly| 
history        = 1844–present|
discipline     = [[Theology]], [[Old Testament]], [[New Testament]]|
publisher      = [[Dallas Theological Seminary]]|
firstdate      = 1844|
country        = [[United States|U.S.]]|
language       = [[English language|English]] |
website        = http://www.dts.edu/media/publications/bibliothecasacra/| 
ISSN           = 0006-1921
}}
'''''Bibliotheca Sacra''''' is a [[List of theological journals|theological journal]] published by [[Dallas Theological Seminary]], first published in 1844 and the oldest theological journal in the [[United States]]. It was founded at [[Union Theological Seminary in the City of New York|Union Theological Seminary]] in 1843, and moved to Andover Theological Seminary (now [[Andover Newton Theological School]]) in 1844 after publishing three issues, to [[Oberlin College]] in 1884, and to [[Pittsburgh Theological Seminary|Xenia Seminary]] in 1922. Dallas Theological Seminary (then the Evangelical Theological Seminary) took over publication in 1934.

== Editors ==
The founding editor of ''Bibliotheca Sacra'' was [[Edward Robinson (scholar)|Edward Robinson]], who handed it over to [[Bela Bates Edwards]] in 1844, who merged it with the ''Biblical Repository'' in 1851. Upon his death in 1852, it was taken over by [[Edwards Amasa Park]], who pledged to "cherish a catholic spirit among the conflicting schools of evangelical [[divine (noun)|divines]]." He held the editorship until 1884, when he transferred control of the journal to [[George Frederick Wright]] at Oberlin College in an effort to keep it safe from growing liberal sentiment at Andover. Wright edited ''Bibliotheca Sacra'' until 1921, when he was succeeded by Melvin G. Kyle. Kyle's successors as editor were John H. Webster (1930–1933), Rollin T. Chafer (1934–1940), [[Lewis Sperry Chafer]] (1940–1952), [[John F. Walvoord]] (1952–1985), Roy B. Zuck (1986–2013), and Larry J. Waters (2013–present).<ref>Morison, William J. "Bibliotheca Sacra" in ''The Conservative Press in Twentieth-Century America'' ed. Ronald Lora, (Greenwood Press, 1999), p. 91–101.</ref>

==References==
{{reflist}}

== External links ==
[[Image:dtsbibsac.jpg|thumb|left|Early edition of ''Bibliotheca Sacra'']]
* [https://books.google.com/books?id=GE8EAAAAQAAJ Volume 1] 
* [https://books.google.com/books?id=bT0bAAAAYAAJ Volume 2]
* [https://books.google.com/books?id=dlwQAAAAYAAJ Volume 3]
* [https://books.google.com/books?id=ZPgRAAAAYAAJ Volume 4] 
* [https://books.google.com/books?id=-1woAAAAYAAJ Volume 5] 
* [https://books.google.com/books?id=Z2NPAAAAcAAJ Volume 6] 
* [https://books.google.com/books?id=lOMWAQAAIAA Volume 7] 
* [https://books.google.com/books?id=ilYXAAAAYAAJ Volume 8]
* [https://books.google.com/books?id=olsgAAAAYAAJ Volume 9] 
* [https://books.google.com/books?id=dWIQAAAAYAAJ Volume 10 ]
* [https://books.google.com/books?id=zmAQAAAAYAAJ Volume 11]
* [https://books.google.com/books?id=P08XAAAAYAAJ Volume 12] 
* [https://books.google.com/books?id=d9sWAQAAIAAJ Volume 13]
* [https://books.google.com/books?id=-9sWAQAAIAAJ Volume 14 ]
* [https://books.google.com/books?id=VQoXAAAAYAAJ Volume 15] 
* [https://books.google.com/books?id=5voRAAAAYAAJ Volume 16] 
* [https://books.google.com/books?id=JN0WAQAAIAAJ Volume 17] 
* [https://books.google.com/books?id=3PURAAAAYAA Volume 18] 
* [https://books.google.com/books?id=RPYRAAAAYAAJ Volume 19]
* [https://books.google.com/books?id=ymJPAAAAcAAJ Volume 20] 
* [https://books.google.com/books?id=G2RPAAAAcAAJ Volume 21] 
* [https://books.google.com/books?id=SWRPAAAAcAAJ Volume 22] 
* [https://books.google.com/books?id=eWRPAAAAcAAJ Volume 23] 
* [https://books.google.com/books?id=FaDNAAAAMAAJ Volume 24] 
* [https://books.google.com/books?id=WNQWAQAAIAAJ Volume 25] 
* [https://books.google.com/books?id=dF0QAAAAYAAJ Volume 26] 
* [https://books.google.com/books?id=1n4XAAAAYAAJ Volume 27] 
* [https://books.google.com/books?id=C9YWAQAAIAAJ Volume 28] 
* [https://books.google.com/books?id=OpJPAAAAcAAJ Volume 29] 
* [https://books.google.com/books?id=gF4QAAAAYAAJ Volume 30] 
* [https://books.google.com/books?id=MTIXAAAAYAAJ Volume 31] 
* [https://books.google.com/books?id=tfkRAAAAYAAJ Volume 32]
* [https://books.google.com/books?id=sdkWAQAAIAAJ Volume 33]
* [https://books.google.com/books?id=CVAXAAAAYAAJ Volume 34]
* [https://books.google.com/books?id=ndoWAQAAIAAJ Volume 35] 
* [https://books.google.com/books?id=rQ4XAAAAYAAJ Volume 36] 
* [https://books.google.com/books?id=RE0XAAAAYAAJ Volume 37] 
* [https://books.google.com/books?id=IwEFAAAAQAAJ Volume 38] 
* [https://books.google.com/books?id=SwEFAAAAQAAJ Volume 39]
* [https://books.google.com/books?id=EfsRAAAAYAAJ Volume 40] 
* [https://books.google.com/books?id=ZQEFAAAAQAAJ Volume 41]
* [https://books.google.com/books?id=gOEWAQAAIAAJ Volume 42] 
* [https://books.google.com/books?id=yeEWAQAAIAAJ Volume 43] 
* [https://books.google.com/books?id=s1kQAAAAYAAJ Volume 44]
* [https://books.google.com/books?id=HvYRAAAAYAAJ Volume 45] 
* [https://books.google.com/books?id=lOQWAQAAIAAJ Volume 46] 
* [https://books.google.com/books?id=XE4XAAAAYAAJ Volume 47]
* [https://books.google.com/books?id=ouUWAQAAIAAJ Volume 48]
* [https://books.google.com/books?id=FOYWAQAAIAAJ Volume 49]
* [https://books.google.com/books?id=iuYWAQAAIAAJ Volume 50] 
* [https://books.google.com/books?id=F-cWAQAAIAAJ Volume 51] 
* [https://books.google.com/books?id=sfYRAAAAYAAJ Volume 52] 
* [https://books.google.com/books?id=UZ_NAAAAMAAJ Volume 53] 
* [https://books.google.com/books?id=xp_NAAAAMAAJ Volume 54] 
* [https://books.google.com/books?id=PVEXAAAAYAAJ Volume 55] 
* [https://books.google.com/books?id=zOgZAAAAYAAJ Volume 56] 
* [https://books.google.com/books?id=IUoXAAAAYAAJ Volume 57] 
* [https://books.google.com/books?id=sfYRAAAAYAAJ Volume 58]
* [https://books.google.com/books?id=kfgRAAAAYAAJ Volume 59] 
* [https://books.google.com/books?id=YFcXAAAAYAAJ Volume 60] 
* [https://books.google.com/books?id=1VcXAAAAYAAJ Volume 61] 
* [https://books.google.com/books?id=31EXAAAAYAAJ Volume 62]
* [https://books.google.com/books?id=WqTNAAAAMAAJ Volume 63]
* [https://books.google.com/books?id=EVoQAAAAYAA Volume 64] 
* [https://books.google.com/books?id=Ja_NAAAAMAAJ Volume 65]
* [https://books.google.com/books?id=P-4ZAAAAYAAJ Volume 66]
* [https://books.google.com/books?id=kM8aAAAAYAA Volume 67]
* [https://books.google.com/books?id=wJ_NAAAAMAAJ Volume 68]
* [https://books.google.com/books?id=mc4aAAAAYAAJ Volume 69]
* [https://books.google.com/books?id=Rs4aAAAAYAAJ Volume 70] 
* [https://books.google.com/books?id=9KDNAAAAMAAJ Volume 71]
* [https://books.google.com/books?id=2PsRAAAAYAAJ Volume 72] 
* [https://books.google.com/books?id=RggXAAAAYAAJ Volume 73] 
* [https://books.google.com/books?id=Rs4aAAAAYAAJ Volume 74]
* [https://books.google.com/books?id=NqPNAAAAMAAJ Volume 75]
* [https://books.google.com/books?id=ywsXAAAAYAAJ Volume 76] 
* [https://books.google.com/books?id=hwsXAAAAYAAJ Volume 77]
* [https://books.google.com/books?id=n8kaAAAAYAAJ Volume 78]
* [https://books.google.com/books?id=vKTNAAAAMAAJ Volume 79]
{{Dallas Theological Seminary|state=expanded}}

[[Category:Protestant studies journals]]
[[Category:Publications established in 1844]]
[[Category:Quarterly journals]]