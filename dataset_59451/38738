{{good article}}
{{Infobox Governor
|name = George Nixon Briggs
|image = George Nixon Briggs-Southworth and Hawes.jpg
|caption = Portrait by [[Southworth & Hawes]], c. 1848
|order = 19th
|office = Governor of Massachusetts
|term_start = January 9, 1844
|term_end = January 11, 1851
|lieutenant = [[John Reed, Jr.]]
|predecessor = [[Marcus Morton]]
|successor = [[George S. Boutwell]]
|state2 = [[Massachusetts]]
|district2 = {{ushr|MA|9|9th}}
|term_start2 = March 4, 1831
|term_end2 = March 3, 1833
|predecessor2 = [[Henry W. Dwight]]
|successor2 = [[William Jackson (Massachusetts)|William Jackson]]
|state3 = [[Massachusetts]]
|district3 = {{ushr|MA|7|7th}}
|term_start3 = March 4, 1833
|term_end3 = March 3, 1843
|predecessor3 = [[George Grennell, Jr.]]
|successor3 = [[Julius Rockwell]]
|office4 = Member of the [[Massachusetts House of Representatives]]
|birth_date = {{birth date|1796|4|12|mf=y}}
|birth_place = [[Adams, Massachusetts]]
|death_date = {{death date and age|1861|9|12|1796|4|12|mf=y}}
|death_place = [[Pittsfield, Massachusetts]]
|party = [[Whig Party (United States)|Whig]]
|spouse = Harriet Briggs
|children=Harriet Briggs<br>George Briggs<br>[[Henry Shaw Briggs]]
|profession = [[Lawyer]]
|religion = [[Baptist]]
|signature=George N. Briggs signature.png
}}

'''George Nixon Briggs''' (April 12, 1796 – September 12, 1861) was an American lawyer and politician from [[Massachusetts]].  A [[United States Whig Party|Whig]], Briggs served for twelve years in the [[United States House of Representatives]], and served seven one-year terms as the [[List of Governors of Massachusetts|19th]] [[Governor of Massachusetts]], from 1844 to 1851.

Raised in rural [[Upstate New York]], Briggs studied law in western Massachusetts, where his civic involvement and successful legal practice preceded statewide political activity.  He was elected to Congress in 1830, where he supported the conservative Whig agenda, serving on the [[United States House Committee on the Post Office and Post Roads|Committee on the Post Office and Post Roads]].  He was also a regular advocate of [[The Temperance movement in the United States|temperance]], abstaining from all alcohol consumption.

He was nominated by the Whigs in 1843 to run against [[Democratic Party (United States)|Democratic]] Governor [[Marcus Morton]] as part of a Whig bid for more rural votes, and easily won election until 1849.  Although he sought to avoid the contentious issue of [[Slavery in the United States|slavery]], he protested [[South Carolina]] policy allowing the imprisonment of free [[African Americans]].  He supported [[capital punishment]], notably refusing to commute the death sentence of [[John White Webster]] for the murder of [[George Parkman]].  Briggs died of an accidental gunshot wound at his home in [[Pittsfield, Massachusetts]].

==Early life and education==
[[File:George N. Briggs birthplace.png|thumb|left|upright|Briggs' birthplace]]
George Nixon Briggs was born in [[Adams, Massachusetts]] on April 12, 1796.  He was the eleventh of twelve children of Allen Briggs, a blacksmith originally from [[Cranston, Rhode Island]], and Nancy (Brown) Briggs, of [[Huguenot]] descent.<ref>Richards, p. 15</ref>  His parents moved the family to [[Manchester, Vermont]] when he was seven, and, two years later, to [[White Creek, New York]].<ref>Richards, pp. 22–23</ref>  The household was religious: his father was a [[Baptist]] and his mother was a [[Quaker]], and they gave their children religious instruction from the Bible.<ref>Richards, pp. 20, 26</ref>

At the age of 14, during the [[Second Great Awakening]], which was especially strong in [[Upstate New York]], Briggs experienced a [[Born again Christianity|conversion experience]] and joined the Baptist faith.  He spoke at [[revival meetings]] of his experience, drawing appreciative applause from the crowds, according to [[Hiland Hall]], who came to know Briggs at that time and who became a lifelong friend and political associate.<ref>Richards, pp. 27–28</ref>  His faith informed his personal behavior: he remained committed to religious ideals, for instance [[Sunday Sabbatarianism|objecting]] to Congressional sessions that stretched into Sunday and [[Teetotalism|abstaining from alcohol consumption]].<ref>Richards, p. 146</ref><ref name=Burns412/>

Briggs sporadically attended the public schools in White Creek, and was apprenticed for three years to a Quaker [[hatter]].<ref>Richards, pp. 33–34</ref>  With support from his older brothers he embarked on the study of law in [[Pittsfield, Massachusetts|Pittsfield]] and [[Lanesboro, Massachusetts|Lanesboro]] in 1813, and was [[Admission to the bar in the United States|admitted to the Massachusetts bar]] in 1818.<ref>Richards, pp. 39–63</ref>  He first opened a practice in Adams, moved it to Lanesboro in 1823, and Pittsfield in 1842.  His trial work was characterized by a contemporary as clear, brief, and methodical, even though he was fond of telling stories in less formal settings.<ref>''History of Berkshire County'', Volume 1, p. 346</ref>

In 1817 Briggs helped to establish a Baptist church in Lanesboro; in this congregation he met Harriet Hall, whom he married in 1818; their children were Harriet, George, and [[Henry Shaw Briggs|Henry]].<ref>Richards, pp. 51, 63, 159, 200</ref>  Briggs was also called upon to raise the four orphaned children of his brother Rufus, one of the brothers who supported him in his law studies.  Rufus died in 1816, followed by his wife not long afterward.<ref>Richards, p. 40</ref>

Briggs' involvement in civic life began at the local level.  From 1824 to 1831 Briggs was the [[registry of deeds|register of deeds]] for the Northern district of [[Berkshire County, Massachusetts]].<ref name="HistofBshCtyVolIp303">''History of Berkshire County'', Volume 1, p. 303</ref>  He was elected [[town clerk]] in 1824, was appointed chairman of the board of commissioners of highways in 1826.<ref name=ANB539>Larson, p. 539</ref>  His interest in politics was sparked by his acquaintance with [[Henry Shaw (Massachusetts)|Henry Shaw]], who served in the [[United States House of Representatives]] from 1817 to 1821.<ref>Whipple, p. 167</ref>

A criminal case tried in 1826 brought Briggs wider notice.  An [[Oneida people|Oneida]] Indian living in [[Stockbridge, Massachusetts|Stockbridge]] was accused of murder.  Briggs was appointed by the court to defend him; convinced by the evidence that the man was innocent, Briggs made what was described by a contemporary as a plea that was "a model of jury eloquence".  The jury, unfortunately, disagreed with Briggs, and convicted the man, who was hanged.  In 1830 the true murderer confessed to commission of the crime.<ref>Whipple, p. 171</ref>

==U.S. House of Representatives==
Despite his rise in prominence, Briggs was at first ineligible for state offices because he did not own property.  In 1830 he decided to run for Congress, for which there was no such requirement.  He was elected to the [[Twenty-second United States Congress|twenty-second]] through the [[Twenty-fourth United States Congress|twenty-fourth]] Congresses as an [[Anti-Jacksonian]], and as a [[Whig Party (United States)|Whig]] to the [[Twenty-fifth United States Congress|twenty-fifth]] through [[twenty-seventh Congress]]es, serving from March 4, 1831 to March 3, 1843.  He decided not to run for reelection in 1842.<ref name=ANB540>Larson, p. 540</ref>

[[Image:Hiland Hall.jpg|thumb|left|upright|[[Hiland Hall]] of [[Vermont]] was a longtime friend and Congressional colleague.]]
Briggs was what became known in later years as a "[[Cotton Whig]]".  He was in favor of [[protectionist tariffs]], and opposed the expansion of slavery into western territories, but did not seek to threaten the unity of the nation with a strong stance against slavery.  He served on the [[United States House Committee on Public Expenditures|Committee on Public Expenditures]] and the [[United States House Committee on the Post Office and Post Roads|Committee on the Post Office and Post Roads]], serving for a time as the chairman of each.<ref name=ANB540/>  The Post Office committee was a regular recipient of complaints from southern states concerning the transmission of [[Abolitionism in the United States|abolitionist]] mailings, which were seen there as incendiary; the matter was of some controversy because southern legislators sought to have these types of mailings banned.  Briggs' friend [[Hiland Hall]], who also sat on the committee, drafted a report in 1836 rebutting the rationales used in such legislative proposals, but the committee as a whole, and then the House, refused to accept the report.<ref>John (1997), p. 94</ref>  Although the authorship of the report appears to be entirely Hall's, Briggs may have contributed to it, and was a signatory to Hall's publication of the report in the ''[[National Intelligencer]]'', a major political journal.<ref>John (1997), pp. 94–96</ref>  The document was influential in driving later Congressional debate on legislative proposals concerning abolitionist mailings, none of which were ever adopted.<ref>John (1997), pp. 104–105</ref>  Briggs and Hall were both instrumental in drafting and gaining passage of the [[Post Office Act of 1836]], which included substantive accounting reforms in the wake of financial mismanagement by [[United States Postmaster General|Postmaster General]] [[William Taylor Barry]].<ref>John (2009), pp. 244–248</ref>

During his time in Congress, Briggs was a vocal advocate for [[The Temperance movement in the United States|temperance]].  He formed the Congressional Temperance Society in 1833, sitting on its executive committee; at an 1836 temperance convention at [[Saratoga Springs, New York]] he advocated the taking of total abstinence pledges as a way to bring more people away from the evils of alcohol,<ref name=Burns412>Burns, p. 412</ref>  and notably prepared such a pledge for [[Kentucky]] Representative [[Thomas F. Marshall]] on the floor of the House of Representatives.  His moves to organize the temperance movement in Congress died out when he left the body, but it was a cause he would continue to espouse for the rest of his life.<ref>Burns, p. 413</ref>  In 1860 he was chosen president of the [[American Temperance Union]].<ref>Burns, p. 414</ref>

During the winter of 1834-1835, while walking along the Washington Canal, he heard a crowd exclaim that a young black boy had fallen in and was drowning.  Upon hearing this, he dove into the water without removing any of his clothes and saved the boy.<ref name=beehive>{{cite web | url = http://www.masshist.org/blog/1364 | title = “Just Mede of Praise”: George N. Briggs’ Heroic Act | first = Susan | last = Martin | publisher = Massachusetts Historical Society | journal = The Beehive | accessdate = August 11, 2016 | date = August 11, 2016 }}</ref>

==Governor of Massachusetts==
[[Image:Marcus Morton.jpg|thumb|right|upright|[[Marcus Morton]], the incumbent governor, lost to Briggs in 1843.]]
Briggs was nominated to run for the governorship on the Whig ticket against the incumbent Democrat [[Marcus Morton]] in 1843.<ref name=Hart93/>  Former Governor [[John Davis (Massachusetts governor)|John Davis]] had been nominated first, but refused the nomination, possibly because [[Daniel Webster]] promised him party support for a future [[Vice President of the United States|vice presidential bid]].  Briggs was apparently recommended as a compromise candidate acceptable to different factions within the party (one controlled by Webster, the other by [[Abbott Lawrence]]).<ref>Dalzell, pp. 77–78</ref>  He was also probably chosen to appeal more directly to the state's rural voters, a constituency that normally supported Morton.  The abolitionist [[Liberty Party (United States, 1840)|Liberty Party]] also fielded a candidate, with the result that none of the candidates won the needed majority.  The legislature decided the election in those cases; with a Whig majority there, Briggs' election was assured.<ref name=Hart93>Hart, p. 4:93</ref>  Briggs was reelected annually until 1850 against a succession of Democratic opponents.  He won popular majorities until the 1849 election, even though third parties (including the Liberty Party and its successor, the [[Free Soil Party]]) were often involved.<ref>Hart, pp. 4:94–99</ref>  Although Whigs had a reputation for aristocratic bearing, Briggs was much more a man of the people than the preceding Whig governors, John Davis and [[Edward Everett]].<ref>Formisano, p. 301</ref>

In 1844 Briggs, alarmed at a recently enacted policy by [[South Carolina]] authorizing the imprisonment of free blacks arriving there from Massachusetts and other northern states, sent representatives to protest the policy.  [[Samuel Hoar]] and his daughter Elizabeth were unsuccessful in changing South Carolina policy, and after protests against what was perceived as Yankee interference in Southern affairs, were advised to leave the state for their own safety.<ref>Petrulionis, pp. 385–418</ref>

[[Capital punishment]] was a major issue that was debated in the state during Brigg's tenure, with social reformers calling for its abolition.  Briggs personally favored capital punishment, but for political reasons called for moderation in its use, seeking, for example, to limit its application in murder cases to those involving [[Murder (United States law)|first degree murder]].<ref>Rogers, p. 84</ref>  After an acquittal in an 1846 murder case where anti-death penalty sentiment was thought to have a role, Briggs, seeking to undercut the anti-death penalty lobby, proposed eliminating the penalty for all crimes except murder, but expressed concern that more such acquittals by sympathetic juries would undermine the connection between crime and punishment.<ref>Rogers, pp. 88–90</ref>

{{quote box|align=left|width=30%
|quote=[Briggs is] an excellent middle man; he looks well when speaking, and seems always ready to say something good, but never said anything; he is an orateur [[wikt:manqué|manqué]].
|source=&mdash; [[Ralph Waldo Emerson]]<ref>Formisano, p. 300</ref>
}}
Briggs' argument was used in the 1849 trial of [[Washington Goode]], a black mariner accused of killing a rival for the affections of a lady.  The case against Goode was essentially circumstantial, but the jury heeded the district attorney's call for assertive punishment of "crimes of violence" and convicted him.<ref>Rogers, pp. 90–91</ref>  There were calls for Briggs to commute Goode's capital sentence, but he refused, writing "A pardon here would tend toward the utter subversion of the law."<ref>Rogers, p. 93</ref>

Not long after the Goode case came [[Parkman–Webster murder case|the sensational trial]] of Professor [[John White Webster]] in the murder of [[George Parkman]], a crime that took place at the [[Harvard Medical School]] in November 1849.  The trial received nationwide coverage, and the prosecution case was based on evidence that was either circumstantial (complicated by the fact that a complete corpse was not found), or founded on new types of evidence ([[forensic dentistry]] was used for the first time in this trial).<ref>Bowers, p. 22</ref><ref>Rogers, pp. 95–97</ref>  Furthermore, [[Massachusetts Supreme Judicial Court]] Chief Justice [[Lemuel Shaw]] was widely criticized for bias in the instructions he gave to the jury.<ref>Rogers, pp. 99–103</ref>  Briggs was petitioned to commute Webster's sentence by death penalty opponents, and even threatened with physical harm if he did not.<ref>Richards, p. 239</ref>   He refused however, stating that the evidence in the case was clear (especially after Webster gave a confession), and that there was no reason to doubt that the court had acted with due and proper diligence.<ref>Richards, pp. 244–249</ref>

During Briggs' time as governor, abolitionist activists continued to make inroads against both the Whigs and Democrats, primarily making common cause with the Democrats against the dominant Whigs.<ref>Holt, pp. 452–453, 579</ref>  Briggs' stance as a Cotton Whig put him in opposition to these forces.  He opposed the [[Mexican-American War]], but acceded to federal demands that the states assist in raising troops for the war, earning the wrath of activist [[Wendell Phillips]].  He did promote other types of reform, supporting [[Horace Mann]] in his activities to improve education in the state.<ref name=ANB540/>

[[Image:George Sewall Boutwell by Southworth & Hawes, c1851 restored.png|thumb|right|upright|[[George S. Boutwell]], c. 1851 portrait by [[Southworth & Hawes]]]]
In 1849, Briggs failed to secure a majority in the popular vote because of the rise in power of the Free Soil Party, but the Whig legislature returned him to office.<ref>Holt, p. 452</ref>  In the 1850 election, anger over the [[Compromise of 1850]] (a series of federal acts designed to preserve the unity of the nation which included the [[Fugitive Slave Act of 1850|Fugitive Slave Act]]) prompted the Democrats and Free Soilers to form a coalition to gain control over the Massachusetts legislature, and divided the Whigs along pro- and antiabolition lines.  With the gubernatorial election again sent to the legislature, Democrat [[George S. Boutwell]] was chosen over Briggs.<ref>Holt, pp. 580–583</ref>

==Later years==
Briggs resumed the practice of law in Pittsfield. He was a member of the [[Massachusetts Constitutional Convention of 1853|state constitutional convention in 1853]], and sat as a judge of the Court of Common Pleas from 1853 to 1858.<ref name="HistofBshCtyVolIp329">''History of Berkshire County'', Volume 1, p. 329</ref>  In 1859 he was nominated for governor by the fading [[Know-Nothing]] movement, but trailed far behind other candidates.<ref>Mitchell, p. 128</ref>

In 1861 Briggs was appointed by President [[Abraham Lincoln]] to a diplomatic mission to the South American [[Republic of New Granada]] (roughly present-day [[Colombia]] and [[Panama]]).  However, he died before he could take up the position.<ref name=ANB540/>  On September 4, 1861<ref name="Richardsp397y1867">Richards, p. 397</ref> Briggs was getting an overcoat out of his closet at his home in Pittsfield, when a gun fell.  As Briggs was picking it up, the gun discharged and Briggs was shot.<ref name="Richardsp398y1867">Richards, p. 398</ref>  Briggs died early in the morning of September 12, 1861, and was buried in the [[Pittsfield Cemetery]].<ref>Smith, p. 324</ref>

==Notes==
{{reflist|2}}

==References==
{{Portal|Biography}}
*{{cite book|last=Bowers|first=C. Michael|title=Forensic Dentistry Evidence: An Investigator's Handbook|publisher=Elsevier|year=2011|location=Amsterdam|isbn=9780123820013|oclc=688636942}}
*{{cite book|last=Burns|first=James Dawson|title=The Temperance Dictionary|url=https://books.google.com/books?id=6pIBAAAAQAAJ&pg=PA411#v=onepage|publisher=Job Caudwell|location=London|year=1861|oclc=752754623}}
*{{cite book|last=Dalzell, Jr|first=Robert|title=Daniel Webster and the Trial of American Nationalism, 1843–1852|publisher=Houghton Mifflin|year=1973|location=Boston|isbn=0395139988}}
*{{cite book|last=Formisano|first=Ronald|title=The Transformation of Political Culture: Massachusetts Parties, 1790s–1840s|publisher=Oxford University Press|year=1983|location=New York|isbn=9780195035094|oclc=18429354}}
*{{cite book|last=Hart|first=Albert Bushnell (ed)|title=Commonwealth History of Massachusetts|publisher=The States History Company|location=New York|year=1927|oclc=1543273}} (five volume history of Massachusetts until the early 20th century)
*{{cite book|last=Holt|first=Michael|title=The Rise and Fall of the American Whig Party: Jacksonian Politics and the Onset of the Civil War|publisher=Oxford University Press|location=New York|year=1999|isbn=9780195055443|oclc=231788473}}
*{{cite book | title = History of Berkshire County, Massachusetts, Volume 1 | publisher = J. H. Beers & Co | location = New York | year = 1885| url=https://books.google.com/books?id=_qbAR2WAAVsC&lpg=PA304&pg=PA346#v=onepage}}
*{{cite journal|last=John|first=Richard|title=Hiland Hall's "Report on Incendiary Publications": A Forgotten Nineteenth Century Defense of the Constitutional Guarantee of the Freedom of the Press|journal=The American Journal of Legal History|volume=41|issue=1|date=January 1997|jstor=845472|pages=94–125}}
*{{cite book|last=John|first=Richard|title=Spreading the News: The American Postal System from Franklin to Morse|publisher=Harvard University Press|year=2009|location=Cambridge, MA|isbn=9780674039148|origyear=1995|oclc=434587908}}
*{{cite encyclopedia |last=Larson|first=Sylvia |title=Briggs, George Nixon |encyclopedia=Dictionary of American National Biography|publisher=Oxford University Press |volume=3 |location=New York |pages=539–541 |year=1999 |isbn=9780195206357|oclc=39182280}}
*{{cite book|last=Mitchell|first=Thomas|title=Anti-Slavery Politics in Antebellum and Civil War America|publisher=Praeger|year=2007|location=Westport, CT|isbn=9780275991685|oclc=263714377}}
*{{cite journal|last=Petrulionis|first=Sandra Harbert|title="Swelling That Great Tide of Humanity": The Concord, Massachusetts, Female Anti-Slavery Society|journal=The New England Quarterly|volume=74|issue=3|date=September 2001|jstor=3185425|pages=385–418 }}
*{{cite book | last=Richards|first= William Carey  | title = Great in Goodness: A Memoir of George N. Briggs, Governor of The Commonwealth Of Massachusetts, From 1844 to 1851 | publisher = Gould and Lincoln | location = Boston, MA | year = 1867 |  url=https://books.google.com/books?id=-GhmVdorY7EC&pg=RA1-PR5#v=onepage |oclc=4045699}}
*{{cite book|last=Rogers|first=Alan|title=Murder and the Death Penalty in Massachusetts|publisher=University of Massachusetts Press|location=Amherst, MA|year=2008|isbn=9781558496323|oclc=137325169}}
*{{cite journal|last=Smith|first=Joseph | title=Hon. George N. Briggs, LL. D | journal=Memorial Biographies, 1845–1871: 1860–1862 |publisher=New England Historic Genealogical Society|url=https://books.google.com/books?id=Cb1PAQAAIAAJ&pg=PA297#v=onepage|year=1885|location=Boston|oclc=13166463}}
*{{cite journal|last=Whipple|first=A. B|title=George N. Briggs|journal=Collections of the Berkshire Historical and Scientific Society|issue=Volume 2|year=1896|url=https://books.google.com/books?id=DuMNAQAAMAAJ&pg=PA153#v=onepage|oclc=1772059}}

==Further reading==
*{{cite book|last=Giddings|first=Edward Jonathan|title=American Christian Rulers|url=https://books.google.com/books?id=HZ0-AAAAYAAJ&pg=PA61#v=onepage|pages=61–69|location=New York|publisher=Bromfield & Company|year=1890|oclc=5929456}}

==External links==
*[http://bioguide.congress.gov/scripts/biodisplay.pl?index=B000830  Congressional Biography of George Nixon Briggs]

{{s-start}}
{{s-par|us-hs}}
{{USRepSuccessionBox
|state=Massachusetts
|district=9
|before=[[Henry W. Dwight]]
|after= [[William Jackson (Massachusetts)|William Jackson]]
|years= 1831–1833 }}
{{USRepSuccessionBox
|state=Massachusetts
|district=7
|district_ord=7th
|before=[[George Grennell, Jr.]]
|after= [[Julius Rockwell]]
|years=March 4, 1833 – March 3, 1843 }}
{{s-off}}
{{succession box
|title=[[Governor of Massachusetts]]
|before=[[Marcus Morton]]
|after=[[George S. Boutwell]]
|years=January 9, 1844 – January 11, 1851}}
{{s-end}}

{{Governors of Massachusetts}}
{{US House Post Office and Civil Service chairs}}
{{USRepMA}}
{{Authority control}}

{{DEFAULTSORT:Briggs, George N.}}
[[Category:Governors of Massachusetts]]
[[Category:People from Adams, Massachusetts]]
[[Category:Massachusetts lawyers]]
[[Category:Massachusetts Whigs]]
[[Category:Members of the Massachusetts House of Representatives]]
[[Category:Baptists from the United States]]
[[Category:Members of the United States House of Representatives from Massachusetts]]
[[Category:1796 births]]
[[Category:1861 deaths]]
[[Category:Accidental deaths in Massachusetts]]
[[Category:Deaths by firearm in Massachusetts]]
[[Category:Firearm accident victims in the United States]]
[[Category:Burials in Massachusetts]]
[[Category:Massachusetts National Republicans]]
[[Category:National Republican Party members of the United States House of Representatives]]
[[Category:Whig Party members of the United States House of Representatives]]
[[Category:19th-century American politicians]]
[[Category:Whig Party state governors of the United States]]
[[Category:People from White Creek, New York]]