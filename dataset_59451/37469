{{Infobox military conflict
|conflict=Battle of the Rice Boats
|image=[[Image:SavannahAugusta1796.jpg|200px]]
|caption=''A 1796 map showing the Savannah area, Hutchinson Island is in the Savannah River near the town.''
|partof=the [[American Revolutionary War]]
|date=March 2–3, 1776
|place= near [[Savannah, Georgia|Savannah]], [[Province of Georgia]], [[Savannah River]]
|coordinates={{coord|32|4|52.56|N|81|5|8.87|W|type:city_scale:100000_region:US|display=inline,title}}
|result=British tactical victory; Colonial strategic victory
|combatant1={{flagicon|United States|1776}} [[Province of Georgia|Georgia]]<br>{{flagicon|South Carolina|1775}} [[Province of South Carolina|South Carolina]]
|combatant2={{flagcountry|Kingdom of Great Britain}}
|commander1=[[Lachlan McIntosh]]
|commander2=Andrew Barclay<br>James Grant
|strength1='''Land:'''<br/>800 [[militia]],<br/>3 [[cannon|artillery pieces]],<br/>1 [[shore battery]]<br/>'''Sea:'''<br/>1 [[fire ship]],<br/>unknown [[auxiliary ship|supply ship]]s
|strength2='''Land:'''<br/>250–300 [[infantry]]<br/>'''Sea:'''<br/>4 [[men-of-war]],<br>2 [[troop transport|transport]]s<ref name=R74>Russell, p. 74</ref>
|casualties1=1 wounded,<br/>1 fire ship sunk,<br/>~3 supply ships captured
|casualties2=unknown human casualties,<br/>~2 men-of-war damaged,<br/>3 supply ships sunk
}}
{{Campaignbox American Revolutionary War: Southern 1775-1779}}

The '''Battle of the Rice Boats''', also called the '''Battle of Yamacraw Bluff''', was a land and naval battle of the [[American Revolutionary War]] that took place in and around the [[Savannah River]] on the border between the [[Province of Georgia]] and the [[Province of South Carolina]] on March 2 and 3, 1776. The battle pitted the [[Patriot (American Revolution)|Patriot]] [[militia]] from Georgia and South Carolina against a small fleet of the [[Royal Navy]].

In December 1775, the [[British Army during the American War of Independence|British Army]] was [[Siege of Boston|besieged in Boston]]. In need of provisions, a [[Royal Navy]] fleet was sent to Georgia to purchase [[rice]] and other supplies.  The arrival of this fleet prompted the colonial rebels who controlled the Georgia government to arrest the British Royal Governor, [[James Wright (governor)|James Wright]], and to resist the British  seizure and removal of supply ships anchored at Savannah.  Some of the supply ships were burned to prevent their seizure, some were recaptured, but most were successfully taken by the British.

Governor Wright escaped from his confinement and safely reached one of the fleet's ships. His departure marked the end of British control over Georgia, although it was briefly restored when  [[Savannah, Georgia|Savannah]] was [[Capture of Savannah|retaken]] by the British in 1778. Wright again ruled from 1779 to 1782, when British troops were finally withdrawn during the closing days of the war.

==Background==
In April 1775, tensions over British colonial policies in the [[Thirteen Colonies]] boiled over into war with the [[Battles of Lexington and Concord]].  Following those events, Patriot colonists surrounded the city of [[Boston, Massachusetts|Boston]], placing it under [[Siege of Boston|siege]], although the encirclement was incomplete: the city could be resupplied by sea.  News of this action and the June [[Battle of Bunker Hill]] fanned the flames of independence throughout the colonies.  Although the [[Province of Georgia]] had managed to remain relatively neutral before these events, radicals in the Georgia provincial congress came into power during the summer of 1775 and progressively stripped Georgia's Royal Governor, [[James Wright (governor)|James Wright]], of his powers.<ref>Jones, pp. 194–210</ref>  While Wright had requested a naval presence near Savannah, Patriots in [[Charleston, South Carolina]] had intercepted his request and substituted for it a dispatch indicating he did not need such support.<ref>Jones, p. 180</ref>

The dispute in Georgia reached a crisis point when British [[men-of-war]] began arriving at [[Tybee Island, Georgia|Tybee Island]] in January 1776.  On January 12, three ships were seen at anchor off Tybee Island; by January 18 the fleet consisted of the [[HMS Cherokee|HMS ''Cherokee'']], {{HMS|Siren|1773|6}}, {{HMS|Raven|1771|6}}, {{HMS|Tamar|1758|6}}, and a number of smaller vessels.  Wright's opinion, expressed to [[Joseph Clay (Georgia)|Joseph Clay]] and others, was that the fleet had been sent to punish the local rebels.<ref name=Johnson129>Johnson, p. 129</ref>  In fact, these ships were the beginnings of a fleet assembled to acquire provisions in Savannah for the beleaguered British troops in Boston.  In December 1775 General [[William Howe, 5th Viscount Howe|William Howe]] had ordered an expedition to purchase [[rice]] and other provisions in Georgia.<ref name=Johnson128>Johnson, p. 128</ref>  By early February the entire fleet had assembled off Tybee Island.  It was under the overall command of Captain Andrew Barclay (or ''Barkley'')  on the {{HMS|Scarborough|1756|6}}, and included {{HMS|Hinchinbrook||6}} and two transports, HMS ''Whitby'' and HMS ''Symmetry'', carrying about 200 British army regulars from the [[40th Foot]] under the command of [[Major]] James Grant.<ref name=Johnson128/>

The arrival of the first ships in January prompted the Georgia Committee of Safety to order the arrest of Wright and other provincial representatives of the Crown on January 18.  [[Joseph Habersham]], a major in the Georgia militia, placed Governor Wright under house arrest, and extracted a promise from the governor that he would not attempt to communicate with the British ships.<ref>Russell, p. 73</ref>  Wright, who continued to be harassed in spite of his confinement, feared for his life, and escaped the mansion on the night of February 11.  He made his way to the plantation of a [[Loyalist (American Revolution)|Loyalist]] supporter and was taken from there to the ''Scarborough''.<ref>Jones, p. 212</ref>  In the meantime, Georgia's provincial assembly had met, elected representatives to the [[Second Continental Congress]], and begun the process of raising regiments for the [[Continental Army]].<ref>Jones, pp. 215–217</ref>

[[Image:JamesWrightBySoldi.jpg|thumb|upright|right|Georgia's last Royal Governor, [[James Wright (governor)|James Wright]]]]
After Governor Wright arrived aboard the ''Scarborough'' he wrote a letter to the remaining members of his council, in which he expressed frustration over getting assurances of safety and access to the desired supplies from the Patriot authorities.<ref name=R74>Russell, p. 74</ref>  Georgia had, along with the other twelve colonies, in 1774 adopted the terms of the [[Continental Association]] created by the [[First Continental Congress]] banning trade with [[Great Britain]].<ref>Jones, p. 188</ref>  With negotiations effectively failed, Barclay ordered his fleet into action on February 29.<ref name=R74/>  His objective was a number of merchant vessels docked at Savannah, whose owners were desirous of moving their goods, something that became possible on March 1 when the previous constraints expired.<ref name=R75>Russell, p. 75</ref>

==Battle==
On March 1, ''Scarborough'', ''Tamar'', ''Cherokee'', and ''Hinchinbrook'' sailed up the [[Savannah River]] to Five-Fathom Hole, accompanying transports carrying two to three hundred men under Grant's command. ''Hinchinbrook'' and one of the transports then sailed up the [[Back River (Georgia)|Back River]].  The transport anchored opposite the port area, while ''Hinchinbrook'', in an attempt to take a position above the town, grounded on a sandbank in the river.  Gunfire from Joseph Habersham's militia cleared ''Hinchinbrook''{{'s}} decks, but without suitable boats, Habersham was unable to attempt the taking of the vessel, which floated free on the next high tide.<ref>Jones, pp. 225–226</ref>  Late on the evening of March 2, Grant's men were landed on [[Hutchinson Island (Georgia)|Hutchinson Island]].  They made their way across the island, and, at 4:00 am on March 3, took over a number of the rice boats anchored near the island.  Due to their success at remaining quiet, and possibly with the collusion of the ship captains, the alarm was not raised in Savannah until 9:00 am.<ref name=R75/>  The arrival of the ships on March 1 prompted the Committee of Safety to issue calls for the defense of the town and the ships, which were forward along with a request for assistance to South Carolina's Committee of Safety the next day.<ref>Jones, pp. 221–223</ref>

When the alarm was raised, Colonel McIntosh took 300 militiamen and set up three 4-pound cannons on Yamacraw Bluff.  He then sent Lieutenant Daniel Roberts and Major Raymond Demeré II under a parley flag to one of the occupied ships; they were promptly arrested.  When a second, larger, parley arrived to discuss the release of the two captives and the ships, the situation turned nasty when Captain Rogers, leader of the party, was insulted.  After he fired at someone on the occupied ship, the British responded in kind, wounding one and very nearly sinking the parley group's boat.<ref name=R75/>  Following that boat's retreat, McIntosh opened fire with the cannons on the bluff, beginning a gunbattle that lasted for four hours.<ref name=J227>Jones, p. 227</ref>

[[Image:Lachlan McIntosh.jpg|thumb|left|upright|Colonel [[Lachlan McIntosh]]|alt=A black-and-white print of man.  The portrait is half-height.  The man is older, with white hair, and is wearing an 18th-century military uniform.  His body is turned three-quarters left, but is looking at the viewer.]]
The Committee of Safety, when it met to discuss the situation, decided that the supply ships should be burned, and a company of militia was assembled to accomplish this task.  One supply ship, the ''Inverness'', was torched and set adrift toward the occupied vessels, causing a scramble as the British troops hurried to abandon them in the face of the arriving [[fire ship]].  During the confusion, the Patriot militia and battery were active, raking the scurrying British crews with musket fire and [[grape shot]].  Two of the occupied vessels managed to get away downstream, and two more escaped the flames by going upstream, but were forced to dock, and their crews were taken prisoner.  Three ships succumbed to the flames, which burned well into the night.  The action was assisted by the timely arrival of 500 South Carolina militia sent in response to the earlier appeal.<ref name=J227/>

==Aftermath==
Colonel McIntosh sent a parley to Captain Barclay the next day, offering a prisoner exchange.  When Barclay refused the exchange, the Committee of Safety ordered the arrest of the remaining members of Wright's council.  This move proved successful; the British-held prisoners were released in exchange for promises of protection of those councillors.<ref>Jones, p. 228</ref>

In spite of the action, the British successfully sailed most of the merchant ships down the Back River, although some of the ships needed to dump a portion of their cargo in order to make it down the shallow channel.<ref>Clark, vol 4, pp. 279,494</ref>  Once they reached Tybee Island, the desired provisions, amounting to 1,600 barrels of rice, were loaded onto the two British transport ships.<ref>Clark, vol 4, p. 444</ref>

The fleet remained anchored off Tybee Island while negotiations went on over the exchange of prisoners.  During this time the fleet detained several arriving vessels, which were later disposed of as [[prize (law)|prize]]s.<ref>Clark, vol 4, p. 327</ref> On March 25, a band of militia from Savannah burned all the houses on the island to deny their use to Wright and the ships' officers.<ref>Jones, p. 229</ref>  Barclay weighed anchor on March 30 and sailed north, leading the convoy of merchant ships and transports.<ref>Clark, vol 4, p. 602</ref>  As the British had [[Evacuation Day (Massachusetts)|abandoned Boston earlier in March]], he first put into [[Newport, Rhode Island]], where the local Patriots denied him any assistance and fired at his ships using field artillery.<ref>Clark, vol 4, pp. 815,1114</ref>  He eventually rejoined the British forces at [[City of Halifax|Halifax, Nova Scotia]] in May.<ref>Clark, vol 5, p. 118</ref>

The battle and Wright's departure marked the end of British control over Georgia until [[Capture of Savannah|Savannah was recaptured]] by British forces in December 1778. Governor Wright returned, and Savannah then remained in British hands until 1782.<ref>Cashin</ref>

==Notes==
{{reflist|colwidth=24em}}

==References==
*{{cite web|url=http://www.georgiaencyclopedia.org/nge/Article.jsp?id=h-2709|publisher=New Georgia Encyclopedia|title=Revolutionary War in Georgia|accessdate=2010-04-14|first=Edward J|last=Cashin|date=2005-03-26}}
*{{cite book|last=Johnson|first=James M|url=https://books.google.com/books?id=O9tNxxBdkJUC&lpg=PA111&dq=wright%20dartmouth%201776&lr=&pg=PA128#v=onepage&q=wright%20dartmouth%201776&f=false|title=Militiamen, Rangers And Redcoats: The Military In Georgia, 1754–1776|publisher=Mercer University Press|year=2003|isbn=978-0-86554-910-4|oclc=227969253|location=Macon, GA}}
*{{cite book|last=Jones|first=Charles Colcock|url=https://books.google.com/books?id=6HEFAAAAQAAJ&dq=barclay%20tybee%201776&lr=&pg=PA211#v=onepage&q=&f=false|title=The history of Georgia, Volume 1|year=1883|publisher=Houghton, Mifflin|location=Boston|oclc=1816720}}
*{{cite book|last=Russell|first=David Lee|url=https://books.google.com/books?id=iojIM25o6wkC&lpg=PT85&dq=Maitland%20rice%20Georgia&lr=&pg=PT84#v=onepage&q=&f=false|title=Oglethorpe and colonial Georgia: a history, 1733–1783|year=2006|publisher=McFarland|isbn=978-0-7864-2233-3|oclc=260062906|location=Jefferson, NC}}
*{{cite book|oclc=469688874|title=Naval Documents of the American Revolution|volume=4,5|year=1970|location=Washington|publisher=US Department of the Navy|editor-first=William Bell |editor-last=Clark}}

==External links==
*[http://ourgeorgiahistory.com/wars/Revolution/revolution07.html Our Georgia History page describing the battle]
{{good article}}
[[Category:1776 in the United States]]
[[Category:Conflicts in 1776|the Rice Boats]]
[[Category:battles of the American Revolutionary War|Rice Boats]]
[[Category:Battles involving Great Britain|Rice Boats]]
[[Category:Georgia (U.S. state) in the American Revolution]]
[[Category:History of Savannah, Georgia]]
[[Category:South Carolina in the American Revolution]]
[[Category:1776 in South Carolina]]
[[Category:1776 in Georgia (U.S. state)]]
[[Category:1776 in the British Empire]]
[[Category:March 1776 events]]