<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Breguet 670
 | image=Breguet 670.png
 | caption=
}}{{Infobox Aircraft Type
 | type=18 seat [[airliner]]
 | national origin=[[France]]
 | manufacturer=[[Breguet Aviation|Société des Avions Louis Breguet]]
 | designer=
 | first flight=1 or 16 March 1935
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=1
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Breguet 670''', '''Breguet 670T''' or '''Breguet-Wibault 670''' was a [[France|French]] twin engine, all metal. eighteen seat [[airliner]] with a retractable [[undercarriage (aeronautics)|undercarriage]] flown in 1935.  Only one was built.

==Design==

In 1934 Breguet acquired [[Wibault|Chantiers Aéronautiques Wibault-Penhoët]] and produced some of their unbuilt designs.<ref name=GunstonMan/> The Breguet 670 was one of these, an all-metal, [[low wing]], twin engine airliner accommodating eighteen passengers.<ref name=Flight/> Engine layout apart, it was similar to though larger than the successful [[trimotor]] [[Wibault 282|Wibault-Penhoët 282]], used by six French airlines including [[Air France]].<ref name=Wib/> In the mid-1930s companies worldwide were designing and producing twin engine aircraft of the same configuration, most notably the earlier [[Douglas DC-2]], which was less powerful and carried only fourteen passengers.<ref name=GenCiv/>

The Breguet 670's wing had a constant thickness centre section, with [[wing root]]s [[aircraft fairing|faired]] into the [[fuselage]] on its [[trailing edge]]s, and two outer panels, tapering in both thickness and plan to semi-elliptical tips. It was a two [[spar (aeronautics)|spar]] structure, with sheet [[duralumin]], I-section<ref name=Lailes/> spars which had extruded [[spar (aeronautics)#Materials and construction|webs]], and was duralumin skinned. Narrow [[chord (aeronautics)|chord]] [[ailerons|slotted ailerons]] occupied the outer two-thirds of the span and the rest fitted with similar [[flap (aeronautics)#Types|flaps]].<ref name=Flight/>

It was powered by two wing-mounted {{convert|825|hp|kW|abbr=on|order=flip}} [[Gnome-Rhône 14K]]rs Mistral Major fourteen cylinder [[radial engine]]s driving three blade [[propeller (aeronautics)#Variable pitch|variable pitch propellers]].<ref name=LAvion/> The engine mountings were steel tube structures supported by the longerons;<ref name=LAvion/> the engine [[aircraft fairing#Types|cowlings]] were most prominent above the wings.<ref name=Flight/> The main legs of the {{convert|5.56|m|ftin|abbr=on}} track<ref name=Lailes/> [[landing gear]], with fairings mounted on the front of, them retracted rearwards into the cowlings.<ref name=Flight/><ref name=LaerN/> The undercarriage was completed with a [[oleo strut|oleo]] mounted, steerable tailwheel. There were fuel tanks in the central section of the wings between both the engines and the longerons.<ref name=Lailes/>

The fuselage was duralumin throughout and was flat sided and bottomed, though its top was slightly rounded and the nose was rounded in both plan and elevation.<ref name=Flight/>  The pilots' cabin had two seats side-by-side, fitted with dual control and radio equipment by the righthand seat.<ref name=Lailes/> Behind them there was a separate cabin with a {{convert|0.75 × 1.8|m|ftin|abbr=on}} floor, which could be fitted as a navigator's post or a bar and gave access to an underfloor baggage hold;<ref name=Lailes/> a port-side external door accessed this space and allowed the pilots to reach their positions via an internal door. A second internal door opened into the passenger cabin, {{convert|9.1 × 1.8|m|ftin|abbr=on}} in plan and {{convert|5|ft|9|in|m|abbr=on|order=flip}} high, which had nine rows of seats, one on each side under its own window. There was a toilet at the rear and behind it a final space containing a library<ref name=Lailes/> and the main passenger door port-side.<ref name=Flight/><ref name=LAvion/>

The Breguet 670 was designed so that it could be adapted to carry merchandise or mail instead of passengers. With a {{convert|2030|kg|lb|abbr=on}} payload, its range was {{convert|750|km|mi|abbr=on}} but reducing this to {{convert|1320|kg|lb|abbr=on}} increased the range to {{convert|1500|km|mi|abbr=on}}.<ref name=LAvion/>

The empennage was conventional, with a tapered, round tipped horizontal tail mounted on top of the fuselage. The [[fin]] and [[rudder]] were straight edged, meeting in a rounded top.  Neither the rudder, which reached down to the keel and worked in a small cut-out between the [[elevator (aeronautics)|elevators]], nor the elevators were [[balanced rudder|balanced]].<ref name=Flight/>

==Operational history==

Two slightly different dates for the first flight appear in the contemporary literature, 1 March 1935<ref name=Wib/> and 16 March 1935.<ref name=LaerN/> This was followed by about six months testing and refining at the hands of pilots Détroyat and Ribière before going for its air ministry tests at [[Vélizy – Villacoublay Air Base|Villacoubly]].<ref name=Lailes/>

The Breguet 670 did not go into production and only the prototype was built. In June 1936 structural problems appeared when the passenger door detached from the fuselage, fortunately whilst the aircraft was on the ground; it turned out that the adhesive attaching soundproofing material to the cabin walls was attacking the duralumin.  After a major rebuild ownership passed in March 1938 to La Société Francaise des Transportes Aérien, a company formed to supply aircraft clandestinely to the Spanish Republican government forces during the [[Spanish Civil War]]. It may have been used for spares or re-registered in Spain but destroyed by bombing in [[Catalonia]].<ref name=Howson/>

==Specifications==
{{Aircraft specs
|ref=L'année aéronautique 1934-5<ref name=LaAn/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=Two
|capacity=18 passengers
|length m=18.74
|length note=
|span m=24.86
|span note=
|height m=7.2
|height note=
|wing area sqm=78.6
|wing area note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=5059
|empty weight note=
|gross weight kg=9009
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity={{convert|1100|kg|lb|abbr=on}} fuel + oil
|more general=
<!--
        Powerplant
-->
|eng1 number=2
|eng1 name=[[Gnome-Rhône 14Krsd Mistral Major]]
|eng1 type=14-cylinder, [[supercharged]] twin row [[radial engine|radials]], geared down 3/2
|eng1 hp=825
|eng1 note=
|power original=
|more power=

|prop blade number=3
|prop name=
|prop dia m=3.10
|prop dia note=[[propeller (aeronautics)#Variable pitch|variable pitch]]<ref name=LAvion/>
<!--
        Performance
-->
|perfhide=

|max speed kmh=340
|max speed note=at {{convert|2000|m|ft|abbr=on}}
|cruise speed kmh=300
|cruise speed note=at 60% power
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=2000
|range note= with a {{convert|1465|kg|lb|abbr=on}} payload<ref name=Lailes/>
|endurance=<!-- if range unknown -->
|ceiling m=6000
|ceiling note=<ref name=avfr/>
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|more performance=
*'''Landing speed:''' {{convert|105|km/h|mph kn|abbr=on}}<ref name=GenCiv/>
*'''Landing distance:''' {{convert|550|m|ft|abbr=on}}<ref name=LAvion/>
}}

==References==
{{reflist|refs=

<ref name=GunstonMan>{{cite book |last=Gunston |first=Bill |title=World Encyclopaedia of Aircraft Manufacturers: from the pioneers to the present day |year=1993 |publisher=Patrick Stephens Limited |location=Sparkford, Somerset |isbn=9 781852 602055 |pages=327–8}}</ref>

<ref name=Howson>{{cite book |title=Aircraft of the Spanish Civil War |last=Howson |first=Gerald |year=1990|volume= |publisher=Putnam Aeronautical Books |location=London |isbn=0 85177 842 9 |pages=67–8}}</ref>

<ref name=LaAn>{{cite journal |last=Hirshauer |first=L. |first2=Ch. |last2=Dolfus |title=Avion Breguet, Type 670 |journal=L'année aéronautique |volume=1934-5 |issue=16 |year=1935 |pages=30 |publisher=Dunod |location=Paris |url=http://gallica.bnf.fr/ark:/12148/bpt6k6556993d/f40}}</ref>

<ref name=Wib>{{cite journal |last=Wibault |first=Michel |title=Genèse d'un avion commercial: le Breguet-Wibault 670 |journal=L'Aéronautique |date=March 1935 |issue=192  |pages=122 |url=http://gallica.bnf.fr/ark:/12148/bpt6k6553613d/f8}}</ref>

<ref name=Flight>{{cite journal |date=25 April 1935 |title=Variation on a theme |journal=[[Flight International|Flight]] |volume=XXVIII |issue=1374 |pages=443 |url=https://www.flightglobal.com/pdfarchive/view/1935/1935%20-%200921.html}}</ref>

<ref name=Lailes>{{cite journal |title=Les Avion commercial Breguet-Wibault "670" |journal=Les Ailes |date=17 October 1935 |issue=748|pages=3 |url=http://gallica.bnf.fr/ark:/12148/bpt6k65539032/f3}}</ref>

<ref name=GenCiv>{{cite journal |last=Précoul |first=Michel |title=Les Avion de transport modernes |journal=Le Genie Civil |date=21 November 1936 |volume=CIX |issue=21|pages=452–6 |url=http://gallica.bnf.fr/ark:/12148/bpt6k64923202/f9}}</ref>

<ref name=LaerN>{{cite journal |last= |first= |title=Société Anonymes de Ateliers d'Aviation Loius Breguet |journal=L'Aéronautique |date=December 1935 |issue=199  |pages=334–5 |url=http://gallica.bnf.fr/ark:/12148/bpt6k6553620j/f9}}</ref>

<ref name=LAvion>{{cite journal |last= |first= |title=L'avion de transport Breguet 670 |journal=Les Avion |date=January 1936 |issue=133 |pages=12–13 |url=http://gallica.bnf.fr/ark:/12148/bpt6k5410789d/f14}}</ref>

<ref name=avfr>{{cite web |url=http://www.aviafrance.com/breguet-wibault-670-aviation-france-4744.htm |title=Breguet Wibault 670 |author=Bruno Parmentier |date=24 June 1998 |accessdate=21 September 2015}}</ref>

}}
<!-- ==Further reading== -->
<!-- ==External links== -->

{{Breguet aircraft|670}}

[[Category:Breguet aircraft]]
[[Category:French airliners 1930–1939]]