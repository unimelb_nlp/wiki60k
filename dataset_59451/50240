{{Use dmy dates|date=November 2015}}
{{F1 race
| Name                  = Abu Dhabi Grand Prix
| Flag                  = Flag of United Arab Emirates.svg
| Circuit               = [[Yas Marina Circuit]]
| Circuit_image         = Circuit Yas-Island.svg
| Image_size            = 300px
| Laps                  = 55
| Circuit_length_km     = 5.554
| Circuit_length_mi     = 3.451
| Race_length_km        = 305.470
| Race_length_mi        = 189.810
| First_held            = 2009
| Last_held             = 
| Times_held            = 8
| Most_wins_driver      = {{flagicon|GBR}} [[Lewis Hamilton]] (3)<br>{{flagicon|GER}} [[Sebastian Vettel]] (3)
| Most_wins_constructor = {{flagicon|DEU}} [[Mercedes-Benz in Formula One|Mercedes]] (3)<br>{{flagicon|Austria}} [[Red Bull Racing|Red Bull]] (3)
| Current_year          = 2016
| Pole_driver           = {{flagicon|GBR}} [[Lewis Hamilton]]
| Pole_team             = [[Mercedes-Benz in Formula One|Mercedes]]
| Pole_time             = 1:38.755
| Winner                = {{flagicon|GBR}} [[Lewis Hamilton|L. Hamilton]]
| Winning_team          = [[Mercedes-Benz in Formula One|Mercedes]]
| Winning_time          = 1:38:04.013
| Second                = {{flagicon|DEU}} [[Nico Rosberg|N. Rosberg]]
| Second_team           = [[Mercedes-Benz in Formula One|Mercedes]]
| Second_time           = +0.439
| Third                 = {{flagicon|DEU}} [[Sebastian Vettel|S. Vettel]]
| Third_team            = [[Scuderia Ferrari|Ferrari]]
| Third_time            = +0.843
| Fastest_lap_driver    = {{flagicon|DEU}} [[Sebastian Vettel]]
| Fastest_lap_team      = [[Scuderia Ferrari|Ferrari]]
| Fastest_lap           = 1:43.729
}}

The '''Abu Dhabi Grand Prix''' ({{lang-ar|سباق جائزة أبوظبي الكبرى}}) is a [[Formula One]] [[motor race]]. It was announced in early 2007 at the Abu Dhabi F1 Festival in the [[United Arab Emirates]]. The first race took place on 1 November 2009, held at the [[Hermann Tilke]] designed [[Yas Marina Circuit]].<ref name="YasMarina2009">{{cite news |title=Yas Marina Circuit construction progressing | url=http://www.gulfnews.com/Sport/Motor_Racing/10244440.html | publisher=gulfnews.com | date=29 October 2009 | accessdate=30 October 2009}}</ref>

On 25 June 2008 the [[FIA]] announced the provisional {{F1|2009}} Formula One calendar including the Abu Dhabi Grand Prix as the 19th and final race of the season on 15 November. On 5 November 2008, however, it was announced that the race would be held as the season finale on 1 November, two weeks before the initially planned date, as the 17th and final race.<ref name="WorldMotor2009">{{cite news |title= World Motor Sport Council - Decisions  | url=http://www.fia.com/en-GB/mediacentre/pressreleases/wmsc/wmsc08/Pages/wmsc_051108.aspx | publisher=fia.com | date=29 October 2009 | accessdate=30 October 2009}}</ref>

The inaugural race was [[Formula One]]'s first ever day-night race, starting at 17:00 [[Time zone|local time]].  Floodlights used to illuminate the circuit were switched on from the start of the event to ensure a seamless transition from daylight to darkness.<ref name="AbuDhabi2009">{{cite news |title=Abu Dhabi confirms 5pm race start | url=http://www.autosport.com/news/report.php/id/78061 | publisher=autosport.com | date=29 October 2009 | accessdate=30 October 2009}}</ref> Subsequent Abu Dhabi Grands Prix have also been day-night races.

==History==

===Origin===
Formula 1 first came to Abu Dhabi in 2007 in the guise of the first ever Formula One Festival.<ref name="Countdownunderway2007">{{cite web|url=http://www.formula1.com/news/headlines/2007/2/5591.html|title=Countdown underway in Abu Dhabi|date=1 February 2007|publisher=[[Formula One]]|accessdate=28 August 2009| archiveurl= https://web.archive.org/web/20091012023508/http://www.formula1.com/news/headlines/2007/2/5591.html| archivedate= 12 October 2009 | deadurl= no}}</ref> Announced in January 2007,<ref name="Festivalcoming2007">{{cite web|url=http://www.formula1.com/news/headlines/2007/1/5530.html|title=F1 Festival coming to Abu Dhabi|date=22 January 2007|publisher=[[Formula One]]|accessdate=28 August 2009}}</ref> the event which took place on 3 February 2007 was free, and the largest gathering of current Formula One cars and drivers outside of a Grand Prix.<ref name="AbuDhabi2007">{{cite web|url=http://www.formula1.com/news/headlines/2007/1/5576.html|title=Abu Dhabi line-up taking shape|date=30 January 2007|publisher=[[Formula One]]|accessdate=28 August 2009}}</ref> At the festival it was announced that Abu Dhabi had won the rights to host a Grand Prix from 2009 until 2016.<ref name="AbuDhabi2007a">{{cite web|url=http://www.formula1.com/news/headlines/2007/2/5604.html |title=Abu Dhabi gets Grand Prix for 2009 |date=3 February 2007 |publisher=[[Formula One]] |accessdate=28 August 2009 |deadurl=yes |archiveurl=https://web.archive.org/web/20071015040241/http://www.formula1.com:80/news/headlines/2007/2/5604.html |archivedate=15 October 2007 |df=dmy }}</ref><ref name="AbuDhabi2007b">{{cite web|url=http://www.ameinfo.com/126899.html|title=Abu Dhabi 'on track' with Grand Prix Circuit construction|date=16 July 2007|publisher=[[AME Info]]|accessdate=28 August 2009| archiveurl= https://web.archive.org/web/20090915182751/http://www.ameinfo.com/126899.html| archivedate= 15 September 2009 | deadurl= no}}</ref> Later that year, [[Etihad Airways]] negotiated a three-year deal for them to become sponsors of the Grand Prix.<ref name="EtihadAirways2007">{{cite web|url=http://www.formula1.com/news/headlines/2007/12/7191.html|title=Etihad Airways to sponsor Abu Dhabi Grand Prix |date=18 December 2007|publisher=[[Formula One]]|accessdate=28 August 2009}}</ref>

===Inaugural Grand Prix===
{{Main|2009 Abu Dhabi Grand Prix}}

For the [[2009 Formula One season]], the Abu Dhabi Grand Prix was added to the schedule. It was provisionally announced as being held on 15 November 2009, as the 19th and final Grand Prix of the season.<ref name="AbuDhabi2008">{{cite web|url=http://news.bbc.co.uk/sport1/hi/motorsport/formula_one/7473748.stm|title= Abu Dhabi to stage 2009 F1 finale|date=25 June 2008|work=[[BBC Sport]]|publisher=[[BBC]]|accessdate=28 August 2009}}</ref> Both the Canadian Grand Prix and French Grand Prix were later removed from the provisional schedule,<ref name="WantsFrench2008">{{cite web|url=http://news.bbc.co.uk/sport1/hi/motorsport/formula_one/7672931.stm|title= FIA wants French GP clarification|date=16 October 2008|work=[[BBC Sport]]|publisher=[[BBC]]|accessdate=28 August 2009}}</ref><ref name="Canadadropped2008">{{cite web|url=http://news.bbc.co.uk/sport1/hi/motorsport/formula_one/7657014.stm|title= Canada dropped from F1 calendar|date=8 October 2008|work=[[BBC Sport]]|publisher=[[BBC]]|accessdate=28 August 2009}}</ref> and as a result the Abu Dhabi Grand Prix was moved to 1 November 2009 where it would become the last of 17 meetings.<ref name="Chinamoves2008">{{cite web|url=http://www.formula1.com/news/headlines/2008/11/8650.html|title=China moves to April as FIA issues revised 2009 calendar|date=5 November 2008|publisher=[[Formula One]]|accessdate=28 August 2009}}</ref> In August 2009, it was announced that the start time would be 1700 local time (1300 UTC), and that the race would be floodlit.<ref name="Holt2009">{{cite web|url=http://news.bbc.co.uk/sport1/hi/motorsport/formula_one/8226520.stm|title=F1 fight to have day-night climax |last=Holt|first=Sarah|date=28 August 2009|work=[[BBC Sport]]|publisher=[[BBC]]|accessdate=28 August 2009}}</ref> The inaugural race was won by [[Sebastian Vettel]] for [[Red Bull Racing]].<ref name="Vettelwins2009">{{cite web|url=http://news.bbc.co.uk/sport2/hi/motorsport/formula_one/8336637.stm|title=Vettel wins as Hamilton drops out  |date=1 November 2009|work=BBC Sport|accessdate=4 November 2009| archiveurl= https://web.archive.org/web/20091104072449/http://news.bbc.co.uk/sport2/hi/motorsport/formula_one/8336637.stm| archivedate= 4 November 2009 | deadurl= no}}</ref>

===2010 Grand Prix===
{{main|2010 Abu Dhabi Grand Prix}}

For the [[2010 Formula One season]], the Abu Dhabi Grand Prix was held on the Yas Marina Circuit, for the weekend of 12, 13, 14 November 2010. The first race in Abu Dhabi in 2009 was a [[dead rubber]] as the both championships had been sealed in Brazil. This time around however, the drivers championship was decided in Abu Dhabi for the first time. With championship leader [[Fernando Alonso]] losing out and [[Sebastian Vettel]] completing his second victory on this track, the young German driver subsequently sealed the world championship.

===2011 Grand Prix===

{{main|2011 Abu Dhabi Grand Prix}}

The 2011 Grand Prix was the 18th and penultimate race of the [[2011 Formula One season]], and took place on 13 November.

The race was won by [[Lewis Hamilton]] in a [[McLaren]]-[[Mercedes-Benz HighPerformanceEngines|Mercedes]]. Second was [[Fernando Alonso]] in a [[Scuderia Ferrari|Ferrari]], with [[Jenson Button]] coming third in a [[McLaren]]-[[Mercedes-Benz HighPerformanceEngines|Mercedes]]. [[Sebastian Vettel]], in a [[Red Bull Racing|Red Bull]]-[[Renault in Formula One|Renault]], had been in pole position, but retired after a puncture in the first lap whilst going round the second corner.

===2012 Grand Prix===

{{main|2012 Abu Dhabi Grand Prix}}

Championship leader [[Sebastian Vettel]] finished the race in 3rd position after starting from the pitlane, due to his disqualification from qualifying due to not having enough fuel to return to [[parc fermé]]. His main championship rival [[Fernando Alonso]] finished 2nd behind the Finnish driver [[Kimi Räikkönen]], who won for the first time after his return to Formula One earlier in 2012.

===2013 Grand Prix===

{{main|2013 Abu Dhabi Grand Prix}}
[[Red Bull Racing]] celebrated their double fourth (Drivers' Champion and Constructors' Champion) in [[Abu Dhabi]]. 
[[David Coulthard]] performed some more doughnuts on the helipad of the [[Burj Al Arab]] luxury hotel in Dubai, 210 metres above ground level.

Red Bull clinched their fourth consecutive constructors’ championship in India and [[Sebastian Vettel]] won the drivers’ title for the fourth year in a row.<ref>{{cite news|title=Red Bull celebrate titles with doughnuts in Dubai|url=http://www.f1fanatic.co.uk/2013/10/31/red-bull-celebrate-titles-with-more-doughnuts-in-dubai/|accessdate=2 November 2013|date=31 October 2013}}</ref>

===2014 Grand Prix===
{{main|2014 Abu Dhabi Grand Prix}}
The 2014 Abu Dhabi Grand Prix took place on 23 November<ref>{{cite news|title=FIA confirms revised calendar for 2014|url=http://www.formula1.com/news/headlines/2013/12/15328.html|accessdate=4 December 2013|date=4 December 2013}}</ref> and was the concluding race of the [[2014 Formula One season]].

Double points were awarded for the race, which was won by [[Lewis Hamilton]], securing his second driver's championship.

===2015 Grand Prix===
{{main|2015 Abu Dhabi Grand Prix}}
The 2015 Abu Dhabi Grand Prix was held on 29 November 2015. The race was won by [[Nico Rosberg]] making it three wins in a row with [[Lewis Hamilton]] and [[Kimi Räikkönen]] completing the podium

===2016 Grand Prix===
{{main|2016 Abu Dhabi Grand Prix}}
The 2016 Abu Dhabi Grand Prix was held on 27 November 2016. The race was won by [[Lewis Hamilton]] making it four wins in a row with [[Nico Rosberg]] and [[Sebastian Vettel]] completing the podium with [[Nico Rosberg]] securing his first drivers' championship.

==Circuit==

{{Main|Yas Marina Circuit}}

The Yas Marina Circuit was designed by [[Hermann Tilke]] and is located on [[Yas Island]] &mdash; a {{Convert|2550|ha|km2}} island on the east coast of [[Abu Dhabi]] &mdash; and the [[2009 Abu Dhabi Grand Prix]] was the first major event to take place on the circuit.<ref name="AbuDhabi">{{cite web|url=http://www.yasmarinacircuit.com/en/content/41/abu-dhabi-grand-prix.html|title=Abu Dhabi Grand Prix|publisher=[[Yas Marina Circuit]]|accessdate=28 August 2009}}</ref>

== Sponsors ==
[[Etihad Airways]] has been the title sponsor of the Abu Dhabi Grand Prix since its inception.
2009-2016

== Winners ==
===Repeat winners (drivers)===
''Embolded drivers have competed in the Formula One championship in the current season.''
{| class="wikitable" style="font-size: 95%;"
|-
! Number of wins
! Driver
! Years
|-
!rowspan="2"| 3
| {{flagicon|DEU}} '''[[Sebastian Vettel]]'''
| [[2009 Abu Dhabi Grand Prix|2009]], [[2010 Abu Dhabi Grand Prix|2010]], [[2013 Abu Dhabi Grand Prix|2013]]
|-
| {{flagicon|GBR}} '''[[Lewis Hamilton]]'''
| [[2011 Abu Dhabi Grand Prix|2011]], [[2014 Abu Dhabi Grand Prix|2014]], [[2016 Abu Dhabi Grand Prix|2016]]
|}

===Repeat winners (constructors)===
''Embolded teams have competed in the Formula One championship in the current season.''
{| class="wikitable" style="font-size: 95%;"
|-
! Number of wins
! Constructor
! Years
|-
!rowspan="2"| 3
| {{flagicon|AUT}} '''[[Red Bull Racing|Red Bull]]'''
| [[2009 Abu Dhabi Grand Prix|2009]], [[2010 Abu Dhabi Grand Prix|2010]], [[2013 Abu Dhabi Grand Prix|2013]]
|-
| {{flagicon|DEU}} '''[[Mercedes-Benz in Formula One|Mercedes]]'''
| [[2014 Abu Dhabi Grand Prix|2014]], [[2015 Abu Dhabi Grand Prix|2015]], [[2016 Abu Dhabi Grand Prix|2016]]
|}

===By year===
{| class="wikitable" style="font-size: 95%;"
|-
! Year
! Driver 
! Constructor
! Report
|-
! [[2016 Formula One season|2016]]
| {{flagicon|GBR}} [[Lewis Hamilton]]
| [[Mercedes-Benz in Formula One|Mercedes]]
| [[2016 Abu Dhabi Grand Prix|Report]]
|-
! [[2015 Formula One season|2015]]
| {{flagicon|DEU}} [[Nico Rosberg]]
| [[Mercedes-Benz in Formula One|Mercedes]]
| [[2015 Abu Dhabi Grand Prix|Report]]
|-
! [[2014 Formula One season|2014]]
| {{flagicon|GBR}} [[Lewis Hamilton]]
| [[Mercedes-Benz in Formula One|Mercedes]]
| [[2014 Abu Dhabi Grand Prix|Report]]
|-
! [[2013 Formula One season|2013]]
| {{flagicon|DEU}} [[Sebastian Vettel]]
| [[Red Bull Racing|Red Bull]]-[[Renault in Formula One|Renault]]
| [[2013 Abu Dhabi Grand Prix|Report]]
|-
! [[2012 Formula One season|2012]]
| {{flagicon|FIN}} [[Kimi Räikkönen]]
| [[Lotus F1|Lotus]]-[[Renault in Formula One|Renault]]
| [[2012 Abu Dhabi Grand Prix|Report]]
|-
! [[2011 Formula One season|2011]]
| {{flagicon|GBR}} [[Lewis Hamilton]]
| [[McLaren]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| [[2011 Abu Dhabi Grand Prix|Report]]
|-
! [[2010 Formula One season|2010]]
| {{flagicon|DEU}} [[Sebastian Vettel]]
| [[Red Bull Racing|Red Bull]]-[[Renault in Formula One|Renault]]
| [[2010 Abu Dhabi Grand Prix|Report]]
|-
! [[2009 Formula One season|2009]]
| {{flagicon|GER}} [[Sebastian Vettel]]
| [[Red Bull Racing|Red Bull]]-[[Renault in Formula One|Renault]]
| [[2009 Abu Dhabi Grand Prix|Report]]
|}

==References==
{{reflist|2}}

==External links==
{{Commons category|Abu Dhabi Grand Prix}}
* [http://www.etihad.com/en/about-us/our-sponsorships/abu-dhabi-grand-prix/ Formula 1 Etihad Airways Abu Dhabi Grand Prix]
* [http://www.cyberoceanz.com/2012/11/Formula-one-F1-grand-prix-Abu-Dhabi-pole-position-to-Lewis-Hamilton-moto-sport-gp-Indian-grand-prix-Mark-Webber-Sebastian-Vettel-Red-bull-Renault-McLaren-Jenson-Button-Kimi-Raikkonen-Ferrari.html Abu Dhabi Grand Prix pole position to Lewis Hamilton]
* https://web.archive.org/web/20070304102303/http://www.ameinfo.com:80/109577.html
* http://www.huliq.com/9352/abu-dhabi-to-host-formula-1-grand-prix-in-2009
* http://www.formula1.com/news/5604.html
* http://news.bbc.co.uk/sport1/hi/motorsport/formula_one/7473748.stm

{{Formula_One_races}}

[[Category:Abu Dhabi Grand Prix| ]]
[[Category:Formula One Grands Prix]]
[[Category:National Grands Prix]]
[[Category:Recurring sporting events established in 2009]]
[[Category:2009 establishments in the United Arab Emirates]]