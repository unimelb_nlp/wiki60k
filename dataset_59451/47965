{{Use mdy dates|date=September 2015}}
'''Carson Fox''' (born Oxford, Mississippi) is an American artist who lives and works in [[Brooklyn]], New York. Her work relies heavily on the imprint that individual experience has on the artist, and centers on the production of sculpture, installation, and prints.

==Biography==
American artist Carson Fox works primarily in sculpture.  Her study of art began at the [[Pennsylvania Academy of the Fine Arts]], where she received a four-year studio certificate.  At PAFA, she was awarded the Cresson Memorial Traveling Scholarship, funding three months of European study and an additional fellowship year at the institution.  Fox received her BFA from the University of Pennsylvania in [[Philadelphia]], and earned her MFA from [[Rutgers University]].<ref name="carsonfox1">{{cite web|last1=Fox|first1=Carson|title=Official Website|url=http://www.carsonfox.com/resume.html|website=Carson Fox Resume|accessdate=April 26, 2015}}</ref>

Fox has been included in solo and group shows in the United States and abroad. She is represented by Linda Warren Projects, Chicago.<ref>{{cite web|title=Linda Warren Projects|url=http://lindawarrenprojects.com/artists|website=Linda Warren Projects: Artists|publisher=Linda Warren Projects|accessdate=April 26, 2015}}</ref>

==Exhibitions==
In 2000, Fox received her first major solo exhibition, "Beauty Queens," at [[Rider University]] Gallery in Lawrenceville, New Jersey.<ref>{{cite web|last1=Allen|first1=Lynne|title=Carson Fox|url=http://carsonfox.com/press/26_RiderCatalog.pdf|website=Carson Fox: Press|accessdate=April 27, 2015}}</ref> Since then, her work has been exhibited in solo exhibitions at museums and galleries across the United States, including at the Boulder Museum of Contemporary Art, the [[Jersey City Museum]], and the [[New Britain Museum of American Art]].<ref name="wordpress1">{{cite web|title=Carson Fox: "Bi-Polar" Looks Beyond Fire, Flames, and Ice|url=https://nbmaa.wordpress.com/2011/11/03/carson-fox-bi-polar-looks-beyond-fire-flames-and-ice/|website=New Britain Museum of American Art Blog|accessdate=April 27, 2015}}</ref>
Carson Fox has been featured in over fifty significant national and international group exhibitions, including shows at the [[Museum of Arts and Design]],<ref>{{cite book|last1=McFadden|first1=David Revere|title=Radical Lace & Subversive Knitting|date=2008|publisher=Museum of Arts & Design|location=New York, NY|isbn=978-1851495689}}<!--|accessdate=April 27, 2015--></ref> [[Scottsdale Museum of Contemporary Art]], the [[Jersey City Museum]],<ref>{{cite news|last1=Bischoff|first1=Dan|title=Many Faces, Many Forms|url=http://carsonfox.com/press/19_2007_FeminineMReview_StarLedger.jpg|accessdate=April 27, 2015|agency=The Star-Ledger of New Jersey|issue=Sunday, September 30, 2007|publisher=The Star-Ledger of New Jersey|date=September 30, 2007}}</ref> [[Nassau County Museum of Art]],<ref>{{cite news|last1=Jacobson|first1=Aileen|title=Flowers, Yes, but Figures, Too, at 'Garden Party'|url=https://www.nytimes.com/2014/03/23/nyregion/flowers-yes-but-figures-too-at-garden-party.html?ref=topics&_r=0|accessdate=April 27, 2015|agency=New York Times|issue=March 21, 2014, page LI10|publisher=New York Times|date=March 21, 2014}}</ref> the Novosibirsk State Art Museum, [[Indiana State Museum]], [[University Museums at the University of Delaware]], [[Tweed Museum of Art]].

In 2009, Fox was commissioned by the Metropolitan Transit Authority (NYC),<ref>{{cite book|last1=Bloodworth|first1=Sandra|title=New York's Underground Art Museum: MTA Arts and Design|date=November 11, 2014|publisher=The Monacelli Press|isbn=978-1580934039}}<!--|accessdate=April 27, 2015--></ref> to complete a large scale, permanent public work at the Long Island Railroad Station in Seaford, New York<ref>{{cite news|last1=Castillo|first1=Alfonso A.|title=Creating Beauty for Commuters|url=http://www.newsday.com/long-island/nassau/creating-beauty-for-commuters-1.3263905|accessdate=April 27, 2015|agency=Newsday|issue=October 21, 2011|publisher=Newsday Long Island|date=October 21, 2011}}</ref>

==Collections==
Fox's work is included in a number of prominent museum collections around the world, including the Museum of Arts and Design (New York), the Jersey City Museum, the Pennsylvania Academy of Fine Arts Museum, Novosibirsk State Art Museum, Russia, and the Royal Museum of Belgium, among others.
Corporate collections include Eaton Corporation (*9),Catamaran Corporation, and Kirkland and Ellis, LLP.
<ref name="carsonfox1"/>

==Work==

===Sculpture===
Carson Fox's current practice centers on the creation of resin sculptural works that examine the tensions between [[nature]] and artiface, the awe of visual spectacle, and humanity's desire to bridle nature and time.<ref name="wordpress1"/>
Recent work can be categorized as flower, tree, sea, and rock forms. With nature as her subject, Fox contemplates humanity's relationship to the physical world in a profound, symbolic way that speaks to both personal and societal associations. The sculptures are intensely labored, composed of multiple parts of pigmented and cast resin the artist assembles and carves. David McFadden, former chief curator of the Museum of Arts and Design in New York, wrote of her work, "Fox invites the viewer into a world that teeters precariously between the real and the unreal, the beautiful and the unsettling."<ref name="carsonfox2">{{cite book|last1=McFadden|first1=David Revere|title=Ice Storm: Carson Fox|publisher=Redux Contemporary Art Center|location=Charleston, South Carolina, USA|url=http://carsonfox.com/press/08_2011_IceStorm_Redux.jpg|accessdate=April 27, 2015}}</ref>

In 2011, Carson Fox presented a solo show at The New Britain Museum of American Art, titled "Bi-Polar."<ref>{{cite web|title=Recently Off the Wall|url=http://www.nbmaa.org/index.php?option=com_content&task=view&id=34&Itemid=58|website=New Britain Museum of American Art: Recently Off the Wall|publisher=New Britain Museum of American Art|accessdate=April 27, 2015}}</ref>

NBMAA wrote about the show:

Bi-Polar is a visually stunning work, but also one that holds inherent potential to evoke emotion. This potential is realized and ignited once the   poignant symbolism behind the visual vocabulary is understood, enveloping the viewer in the artist’s personal history. At the same time, Bi-Polar invites the viewer into a wider world of questions regarding the universal human desire to resist against the forces of natural order, push the boundaries of what is within or beyond our control, and ultimately arrest time.<ref name="wordpress1"/>

Ice Storm, at Redux Contemporary Art Center in Charleston, SC, shared similar themes.  David McFadden, the former chief curator of the Museum of Art and Design, NYC, wrote in the exhibition catalog:

The [installations] establish an ambiance of threat and danger that undermines the sheer physical beauty of the [forms]… Fox's fantasies… hold time in abeyance- what should melt away is made permanent. Change (and by implication, death) is effectively checkmated.<ref name="carsonfox2"/>

At her 2014 solo exhibition at Linda Warren Projects titled Mimesis, sculptures of coral, crystals, and other natural forms dominated the gallery. 
Art critic, B. David Zarley wrote:

Across Fox’s lavish installation creeps the delicious subversion that comes from recreating-perhaps even improving upon – shock! Blasphemy! – natural beauty with toxic media. It becomes difficult to tell, through the pulchritude and plastic, whether fox is sanctifying nature or supplanting it, genuflecting before creation or substituting her own; she pulls forth these beautiful, terrible ideas, as she does the chthonian forms comprising the bulk of “Mimesis.”<ref>{{cite news|last1=Zarley|first1=B David|title=Review: Carson Fox/Linda Warren Projects|url=http://art.newcity.com/2014/11/22/review-carson-foxlinda-warren-projects/|accessdate=April 27, 2015|agency=New City Art|issue=November 22, 2014|publisher=New City Art|date=November 22, 2014}}</ref>

The artist says of her work: “My goal is for (the work) to seem preposterous and wondrous, to underscore that nothing is more perplexing, complex, and extraordinary than nature.”<ref>{{cite web|last1=Fox|first1=Carson|title=Carson Fox: Statement: Recent Work|url=http://carsonfox.com/recentstatement.html|website=Carson Fox: Statement: Recent Work|publisher=Carson Fox|accessdate=April 27, 2015}}</ref>

===Prints===
Fox's prints embody the same spirit as her sculptural works, "unabashedly and unapologetically beautiful," but "hinting at a more complicated and darker core." (*14) Fox commands a variety of print media, including etchings, digital prints, lithographs and relief prints.  
Having grown up in the American South, "Fox was exposed to a Southern gothic aesthetic and folk art tradition." (*14) Her works are repetitive and labor-intensive, employing visual elements from Victorian paper works.<ref>{{cite book|last1=Markowitz|first1=Joan|title=Fight or Flight: Carson Fox|date=2006|publisher=1708 Gallery|location=Richmond, Virginia|url=http://www.carsonfox.com/press/24_2006_1708Gallery.jpg|accessdate=April 27, 2015}}</ref>

Fox writes of her work: 

My natural inclination is to be interested in objects and themes that have been left out of the history of art, feeling a particular kinship with marginalized “craft” materials, and the popular illustrations and folk art of the Victorian era.  Like the Victorians, the fragility and brevity of life terrifies me, and one way I cope with it is to make things; thereby proving my existence through the evidence of my labor.

Many recent prints contain physical evidence of this labor.  Fox pokes thousands of holes into the printed image, “suggesting invisible routes made visible, a tangible history of [her] own industry, while transforming the paper into a lacy map."<ref>{{cite web|last1=Fox|first1=Carson|title=Carson Fox: Statement: Prints|url=http://carsonfox.com/printstatement.html|website=Carson Fox: Statement: Prints|publisher=Carson Fox|accessdate=April 27, 2015}}</ref>

==Work as an Educator, Lecturer, Curator==
In addition to her professional artistic practice, Fox maintains an academic career as a collegiate level educator, lecturer, and curator. 
Her teaching experience includes [[Harvard University]], [[New York University]], [[Rutgers University]], and the [[Pennsylvania Academy of the Fine Arts]], before joining the faculty at [[Adelphi University]] in Garden City, New York. Fox has lectured widely on [[printmaking]] and sculpture across the United States and abroad, including at [[Boston University]], [[Maryland Institute College of Art]], [[University of the Arts (Philadelphia)|University of the Arts]], and [[Rutgers University]]. 
<ref>{{cite web|title=Adelphi University: Faculty Profile: Carson Fox|url=http://www.adelphi.edu/faculty/profiles/profile.php?PID=0370|website=Adelphi University: Faculty Profile|publisher=Adelphi University|accessdate=April 27, 2015}}</ref>

Fox is the [[curator]] of an annual exhibition series at Adelphi University, "Ephemeral." The sequence "examines the human relationship with the transitory- investigating the role of human experience, memory, and mortality in our lives."<ref>{{cite news|last1=Donohue|first1=Erin|title=Ephemeral Exhibition at Adelphi University Engages the Public|url=http://patch.com/new-york/gardencity/ephemeral-exhibition-at-adelphi-university-engages-the-public|accessdate=April 27, 2015|agency=Garden City Patch|issue=September 6, 2013|publisher=Garden City Patch|date=September 6, 2013}}</ref> She has also curated exhibitions abroad, including the 6th Graphics Biennial (USA) at the Novosibirsk State Art Museum, Novosibirsk, Russia.

==References==

<references />

{{DEFAULTSORT:Fox, Carson}}
[[Category:Artists from Brooklyn]]
[[Category:Pennsylvania Academy of the Fine Arts alumni]]
[[Category:1970 births]]
[[Category:Living people]]