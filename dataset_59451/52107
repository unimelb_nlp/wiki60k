{{Infobox journal
| title = Journal of the Operational Research Society
| cover = [[File:Jorscover2011.jpg|Jorscover2011.jpg]]
| discipline = [[Operations research]], [[management]]
| former_names = Operational Research Quarterly
| abbreviation = J. Oper. Res. Soc.
| formernames = Operational Research Quarterly
| editor = Tom Archibald, Jonathan Crook
| publisher = [[Palgrave Macmillan]]
| country =
| frequency = Monthly
| history = 1950-present
| openaccess =
| license =
| impact =  0.971 
| impact-year = 2011
| website = http://www.palgrave-journals.com/jors/index.html
| link1 = http://www.palgrave-journals.com/jors/journal/vaop/ncurrent/
| link1-name = Online access
| link2 = http://www.palgrave-journals.com/jors/archive/index.html
| link2-name = Online archive
| JSTOR =
| OCLC = 03685489
| LCCN =
| CODEN = JORSDZ
| ISSN = 0160-5682
| eISSN = 1476-9360
}}
The '''''Journal of the Operational Research Society''''' is a [[Peer review|peer-reviewed]] [[academic journal]] covering [[operations research]]. It is an official journal of [[The Operational Research Society]] and has been in existence since 1950. It publishes full length case-oriented papers, full length theoretical papers, technical notes, discussions (viewpoints) and book reviews.

==History==
The journal began as ''Operational Research Quarterly'' in 1950. At that time it was published by the Operational Research Club (Great Britain).<ref>[http://catalogue.bl.uk/F/?func=file&file_name=login-bl-list British Library Integrated Catalogue]</ref> It was published four times a year until 1978 (from 1953–1969 under the title ''OR'') when it became a monthly publication and the name was changed to ''Journal of the Operational Research Society''.

== Abstracting and indexing ==
The journal is abstracted and indexed by ABI/INFORM, [[Compendex]], [[Current Contents]]/Engineering, Computing & Technology, Current Contents/Social & Behavioural Sciences, [[Inspec]], [[International Abstracts in Operations Research]], [[Science Citation Index]], [[Social Sciences Citation Index]], [[Scopus]], and [[Zentralblatt MATH]].

== Scope ==
Some idea of the scope and contents of the journal can be found in a survey of papers published during the period 2000-2009.<ref>{{Cite journal | last1 = Katsaliaki | first1 = K. | last2 = Mustafee | first2 = N. | last3 = Dwivedi | first3 = Y. K. | last4 = Williams | first4 = T. | last5 = Wilson | first5 = J. M. | title = A profile of OR research and practice published in the Journal of the Operational Research Society | doi = 10.1057/jors.2009.137 | journal = Journal of the Operational Research Society | volume = 61 | pages = 82 | year = 2010 | pmid =  | pmc = }}</ref>

* 51.7% of papers were theoretical while 31.3% were case-orientated.
== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.palgrave-journals.com/jors/index.html}}

{{Holtzbrinck}}

[[Category:Publications established in 1950]]
[[Category:Monthly journals]]
[[Category:Business and management journals]]
[[Category:English-language journals]]
[[Category:1950 establishments in the United Kingdom]]
[[Category:Palgrave Macmillan academic journals]]
[[Category:Operational Research Society academic journals]]