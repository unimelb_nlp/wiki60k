[[Image:Tttecnology.gif|thumb|350px|Schematic view of a flow sensor.]]

An '''ultrasonic flow meter''' is a type of [[flow meter]] that measures the velocity of a  [[fluid]] with [[ultrasound]] to calculate volume flow. Using ultrasonic [[transducers]], the flow meter can measure the average velocity along the path of an emitted beam of ultrasound, by averaging the difference in measured transit time between the pulses of ultrasound propagating into and against the direction of the flow or by measuring the frequency shift from the [[Doppler effect]]. Ultrasonic flow meters are affected by the acoustic properties of the fluid and can be impacted by temperature, density, viscosity and suspended particulates depending on the exact flow meter. They vary greatly in purchase price but are often inexpensive to use and maintain because they do not use [[moving parts]], unlike mechanical flow meters.

==Means of operation==

There are three different types of ultrasonic flow meters. Transmission (or contrapropagating transit-time) flow meters can be distinguished into in-line (intrusive, wetted) and clamp-on (non-intrusive) varieties. Ultrasonic flow meters that use the Doppler shift are called Reflection or Doppler flow meters. The third type is the Open-Channel flow meter.<ref>{{cite web|title=How Does an Ultrasonic Flow Meter Work|url=http://www.openchannelflow.com/blog/article/how-does-an-ultrasonic-flow-meter-work}}</ref>

==Principle==
===Time transit flow meter===
Ultrasonic flow meters measure the difference of the transit time of ultrasonic pulses propagating in and against flow direction. This time difference is a measure for the average velocity of the fluid along the path of the ultrasonic beam. By using the absolute transit times both the averaged fluid velocity and the speed of sound can be calculated. Using the two transit times <math>t_{up}</math> and <math>t_{down}</math> and the distance between receiving and transmitting transducers <math>L</math> and the inclination angle <math>\alpha</math>  one can write the equations:

<math>v = \frac{L}{{2\;\cos \left( \alpha  \right)}}\;\frac{{t_{up}  - t_{down} }}{{t_{up} \;t_{down} }}</math> and
<math>c = \frac{L}{2}\;\frac{{t_{up}  + t_{down} }}{{t_{up} \;t_{down} }}</math>

where <math>v</math> is the average velocity of the fluid along the sound path and <math>c</math> is the speed of sound.

===Doppler shift flow meters===

Another method in ultrasonic flow metering is the use of the [[Doppler shift]] that results from the reflection of an ultrasonic beam off sonically [[reflective]] materials, such as solid particles or [[Entrainment (hydrodynamics)|entrained]] [[air bubble]]s in a flowing fluid, or the [[turbulence]] of the fluid itself, if the liquid is clean.

Doppler flowmeters are used for [[slurries]], liquids with bubbles, gases with sound-reflecting particles.

This type of flow meter can also be used to measure the rate of [[blood]] flow, by passing an ultrasonic beam through the tissues, bouncing it off a reflective plate, then reversing the direction of the beam and repeating the measurement, the volume of [[blood]] flow can be estimated. The frequency of the transmitted beam is affected by the movement of blood in the vessel and by comparing the frequency of the upstream beam versus downstream the flow of blood through the vessel can be measured.  The difference between the two frequencies is a measure of true volume flow. A wide-beam sensor can also be used to measure flow independent of the cross-sectional area of the blood vessel.

===Open Channel flow meters===

In this case, the ultrasonic element is actually measuring the height of the water in the open channel; based on the geometry of the channel, the flow can be determined from the height.  The ultrasonic sensor usually also has a temperature sensor with it because the speed of sound in air is affected by the temperature.

==See also==
* [[Flow measurement]]
* [[Magnetic flow meter]]
* [[Turbine flow meter]]

==References==
{{Reflist}}
*Lipták, Béla G.: Process Measurement and Analysis, Volume 1. CRC Press (2003), ISBN 0-8493-1083-0 (v. 1)
*[http://www.cs.brown.edu/people/tld/courses/cs148/02/sonar.html Ultrasonic Acoustic Sensing] Brown University
*Lynnworth, L.C.: Ultrasonic Measurements for Process Control. Academic Press, Inc. San Diego. ISBN 0-12-460585-0

==External links==
{{Commons}}
* [http://www.mathpages.com/rr/s2-04/2-04.htm Doppler Shift for Sound and Light] at MathPages
* [http://www.kettering.edu/~drussell/Demos/doppler/doppler.html The Doppler Effect and Sonic Booms (D.A. Russell, Kettering University)]

{{DEFAULTSORT:Ultrasonic Flow Meter}}
[[Category:Ultrasound]]
[[Category:Flow meters]]