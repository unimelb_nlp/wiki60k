{{Infobox Academic Conference
 | history = 1980–present
 | discipline = [[Information systems]]
 | abbreviation = ICIS
 | publisher =
 | country= International
 | frequency = annual
}}
'''International Conference on Information Systems''' (ICIS) is an annual international conference for academics and research-oriented practitioners in the area of [[information systems]]. Previously known as the Conference on Information Systems (CIS), ICIS is the flagship conference of the [[Association for Information Systems]], an international professional organization serving academics in information systems.<ref>{{cite web | url=http://core.edu.au/cms/images/downloads/conference/Astar.pdf | title=2008 Australian Ranking of ICT Conferences | year=2008 | accessdate=2012-08-13}} (Tier A+, highest.)</ref><ref>{{cite web | url=http://www.cs.ualberta.ca/~zaiane/htmldocs/ConfRanking.html | title=Computer Science Conference Rankings | first=Osmar R. | last=Zaïane | date=August 2009 | accessdate=2009-08-25}} (Rank 1, highest.)</ref><ref>{{cite web | url=http://www.cais.ntu.edu.sg/content/research/conference_list.jsp | title=Conference Listing | work=CAIS/NTU | accessdate=2009-08-25}} (Rank A, second-highest.)</ref><ref>{{cite web|url=http://www-personal.umich.edu/~sankum/idle/2007/04/research-conferences-in-information.html |title=Research Conferences in Information Systems |first=Sanjeev |last=Kumar |date=April 2007 |accessdate=2009-08-25 |deadurl=yes |archiveurl=https://web.archive.org/web/20090226181007/http://www-personal.umich.edu/~sankum/idle/2007/04/research-conferences-in-information.html |archivedate=February 26, 2009 }} ("…the most prominent research conference in IS research area…")</ref>

ICIS was founded in 1980 at the [[University of California, Los Angeles]], and the first conference was held in [[Philadelphia]] the same year. Starting in 1990, when it had its first international venue in [[Copenhagen]], ICIS has been traditionally held outside of North America regularly. ICIS currently rotates between the Americas (AIS Region 1), Europe/Middle East/Africa (AIS Region 2), and Asia/Australia (AIS Region 3). ICIS is usually scheduled in early December and lasts for four days.

The conference is preceded by the ICIS Doctoral Consortium,<ref>[http://home.aisnet.org/displaycommon.cfm?an=1&subarticlenbr=84 ICIS Site Selection Criteria]</ref> held off site, usually in a nearby resort. The consortium brings together senior faculty from Information Systems (many of whom were participants in past consortia) and ~40 doctoral students, selected from ~120 nominations (each university or institution is allowed to nominate only one participant).

==Past and Future Conferences==
{| class="wikitable sortable plainrowheaders" style="text-align: center;"
|+ List of ICIS Conferences<ref>http://aisnet.org/?ICISPage</ref>
!scope="col" width=1%|Year
!scope="col" width=1%|Location
!scope="col" width=1%|AIS Region
|-
|2020
|''TBA''
|Region 3 (Asia/Australia/Pacific)
|-
|2019
|Munich, Germany <ref>[http://icis2019.aisnet.org/ ICIS 2019 Official Website]</ref>
|Region 2 (Europe/MiddleEast/Africa)
|-
|2018
|San Francisco, Caifornia, USA <ref>[http://icis2018.aisnet.org/ ICIS 2018 Official Website]</ref>
|Region 1 (Americas)
|-
|2017
|Seoul, South Korea <ref>[http://icis2017.aisnet.org/ ICIS 2017 Official Website]</ref>
|Region 3 (Asia/Australia/Pacific)
|-
|2016
|Dublin, Ireland <ref>[http://icis2016.aisnet.org/ ICIS 2016 Official Website]</ref>
|Region 2 (Europe/MiddleEast/Africa)
|-
|2015
|Fort Worth, Texas, USA <ref>[http://icis2015.aisnet.org/ ICIS 2015 Official Website]</ref>
|Region 1 (Americas)
|-
|2014
|Auckland, New Zealand <ref>[http://icis2014.aisnet.org/ ICIS 2014 Official Website]</ref>
|Region 3 (Asia/Australia/Pacific)
|-
|2013
|Milan, Italy <ref>[http://icis2013.aisnet.org/ ICIS 2013 Official Website]</ref>
|Region 2 (Europe/MiddleEast/Africa)
|-
|2012
|Orlando, Florida, USA <ref>[http://icis2012.aisnet.org/ ICIS 2012 Official Website]</ref>
|Region 1 (Americas)
|-
|2011
|Shanghai, China <ref>[http://icis2011.aisnet.org/ ICIS 2011 Official Website]</ref>
|Region 3 (Asia/Australia/Pacific)
|-
|2010
|St. Louis, Missouri, USA
|Region 1 (Americas)
|-
|2009
|Phoenix, Arizona, USA
|Region 1 (Americas)
|-
|2008
|Paris, France
|Region 2 (Europe/MiddleEast/Africa)
|-
|2007
|Montreal, Quebec, Canada
|Region 1 (Americas)
|-
|2006
|Milwaukee, Wisconsin, USA
|Region 1 (Americas)
|-
|2005
|Las Vegas, Nevada, USA
|Region 1 (Americas)
|-
|2004
|Washington, DC, USA
|Region 1 (Americas)
|-
|2003
|Seattle, Washington, USA
|Region 1 (Americas)
|-
|2002
|Barcelona, Spain
|Region 2 (Europe/MiddleEast/Africa)
|-
|2001
|New Orleans, Louisiana, USA
|Region 1 (Americas)
|-
|2000
|Brisbane, Australia
|Region 3 (Asia/Australia/Pacific)
|-
|1999
|Charlotte, North Carolina, USA
|Region 1 (Americas)
|-
|1998
|Helsinki, Finland
|Region 2 (Europe/MiddleEast/Africa)
|-
|1997
|Atlanta, Georgia, USA
|Region 1 (Americas)
|-
|1996
|Cleveland, Ohio, USA
|Region 1 (Americas)
|-
|1995
|Amsterdam, Netherlands
|Region 2 (Europe/MiddleEast/Africa)
|-
|1994
|Vancouver, British Columbia, Canada
|Region 1 (Americas)
|-
|1993
|Orlando, Florida, USA
|Region 1 (Americas)
|-
|1992
|Dallas, Texas, USA
|Region 1 (Americas)
|-
|1991
|New York City, New York, USA
|Region 1 (Americas)
|-
|1990
|Copenhagen, Denmark
|Region 2 (Europe/MiddleEast/Africa)
|-
|1989
|Boston, Massachusetts, USA
|Region 1 (Americas)
|-
|1988
|Minneapolis, Minnesota, USA
|Region 1 (Americas)
|-
|1987
|Pittsburgh, Pennsylvania, USA
|Region 1 (Americas)
|-
|1986
|San Diego, California, USA
|Region 1 (Americas)
|-
|1985
|Indianapolis, Indiana, USA
|Region 1 (Americas)
|-
|1984
|Tucson, Arizona, USA
|Region 1 (Americas)
|-
|1983
|Houston, Texas, USA
|Region 1 (Americas)
|-
|1982
|Ann Arbor, Michigan, USA
|Region 1 (Americas)
|-
|1981
|Cambridge, Massachusetts, USA
|Region 1 (Americas)
|-
|1980
|Philadelphia, Pennsylvania, USA
|Region 1 (Americas)
|}

== References ==
{{Reflist}}

== External links ==
* {{Official website}}

[[Category:Information systems conferences]]
[[Category:Academic conferences]]