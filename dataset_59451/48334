{{eastern name order|Hagyó Miklós}}
{{Infobox Officeholder
|honorific-prefix   = 
|name               = '''Miklós Hagyó'''
|native_name        = 
|native_name_lang   = [[Hungarian language|Hungarian]]
|honorific-suffix   = 
|image              =
|imagesize          = 
|smallimage         = <!--If this is specified, "image" should not be.-->
|alt                = 
|caption            =
|order              = 
|office             = [[Deputy Mayor]] of [[Budapest]]
|term_start         = 2006
|term_end           = 2009
|alongside          = [[Mayor]] [[Gábor Demszky]]
|vicepresident      = 
|viceprimeminister  = 
|mayor              = 
|lieutenant         = 
|monarch            = 
|president          = 
|primeminister      = 
|taoiseach          = 
|chancellor         = 
|governor           = 
|governor-general   = 
|governor_general   = 
|constituency       = 
|majority           = 
|succeeding         = <!--For President-elect or equivalent-->
|predecessor        = 
|successor          = 
|prior_term         =
|order2             =  <!--Can be repeated up to eight times by changing the number-->
|office2            =  [[Member of Parliament]] from [[Elections in Hungary|Budapest List]]
|term_start2        =  May 16, 2006
|term_end2          =  May 13, 2010
|alongside2         =  <!--Can be repeated up to eight times by changing the number-->
|vicepresident2     =  <!--Can be repeated up to eight times by changing the number-->
|viceprimeminister2 =  <!--Can be repeated up to eight times by changing the number-->
|deputy2            =  <!--Can be repeated up to eight times by changing the number-->
|lieutenant2        =  <!--Can be repeated up to eight times by changing the number-->
|monarch2           =  <!--Can be repeated up to eight times by changing the number-->
|president2         =  <!--Can be repeated up to eight times by changing the number-->
|primeminister2     =  [[Ferenc Gyurcsány]]: June 9, 2006 - resigned April 14, 2009; [[Gordon Bajnai]]: April 14, 2009 – May 29, 2010
|governor2          =  <!--Can be repeated up to eight times by changing the number-->
|succeeding2        =  <!--Can be repeated up to eight times by changing the number-->
|predecessor2       =  <!--Can be repeated up to eight times by changing the number-->
|successor2         =  <!--Can be repeated up to eight times by changing the number-->
|constituency2      =  <!--Can be repeated up to eight times by changing the number-->
|majority2          =  <!--Can be repeated up to eight times by changing the number-->
|office3            =  [[Member of Parliament]] from [[Elections in Hungary|National List]]
|term_start3       =  May 15, 2002
|term_end3         =  May 15, 2006
|primeminister3     =  [[Péter Medgyessy]]: May 27, 2002 – resigned September 29, 2004; [[Ferenc Gyurcsány]]: September 29, 2004 – June 9, 2006
|birth_date         = {{Birth date and age|1967|07|20}}
|birth_place        = 
|death_date         = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} or {{Death-date and age|Month DD, YYYY|Month DD, YYYY}} (death date then birth date) -->
|death_place        = 
|restingplace       = 
|restingplacecoordinates = 
|birthname          = 
|citizenship        = [[Hungary|Hungarian]]
|nationality        = [[Hungary|Hungarian]]
|party              = [[Hungarian Socialist Party]]
|otherparty         =  <!--For additional political affiliations-->
|spouse             = 
|partner            =  <!--For those with a domestic partner and not married-->
|relations          = 
|children           = 
|residence          = 
|alma_mater         = 
|occupation         = [[Entrepreneur]], [[Politician]]
|website            = http://www.hagyomiklos.com
}}
'''Miklós Hagyó''' ([[Hungarian language|Hungarian]] pronunciation: [[Help:IPA for Hungarian|hɒɟoː mikloːʃ]]; born July 20, 1967) is a Hungarian [[entrepreneur]] and [[politician]], a former [[deputy mayor]] of Budapest and a former member of the [[Hungarian parliament]].  From 1998 until 2010, Hagyó was active in the [[Hungarian Socialist Party]].<ref name=Nagy>{{cite news|last=Nagy|first=György B.|title=Hagyó Miklós tündöklése és bukása|url=http://www.hetek.hu/belfold/201002/hagyo_miklos_tundoklese_es_bukasa|accessdate=22 April 2013|newspaper=Hetek|date=5 February 2010}}</ref> Immediately following the [[Hungarian parliamentary election, 2010|April 2010 elections]], when Hagyó lost his seat, he was arrested on suspicion of [[extortion]] and [[breach of fiduciary responsibility]].<ref name=Balogh>{{cite web|last=Balogh|first=Eva S.|title=A Botched-Up Show Trial|url=http://hungarianspectrum.wordpress.com/2012/09/25/a-botched-up-show-trial-in-hungary/|work=Hungarian Spectrum|accessdate=22 April 2013}}</ref> The [[trial]] against Hagyó and 14 other suspects is currently underway in [[Kecskemét]].

== Education ==

In 1989, Hagyó earned a [[bachelor's degree]] in [[Product Engineering]] from the former Budapest University of Horticulture and Food Industry in [[Szeged]], [[Hungary]]. After the undergraduate studies, he relocated to the university's Budapest campus in pursuit of an [[advanced degree]] in [[Food Engineering]] from the Department of Economics.<ref name="Szilagyi; Borsodi">{{cite news|last=Szilagyi, Richard;|first=Borsodi, Attila|title=Hagyó felemelkedése és bukása|url=http://mno.hu/migr_1834/hagyo_felemelkedese_es_bukasa-251769|accessdate=22 April 2013|newspaper=MNO|date=12 February 2010}}</ref> Hagyó claimed that he subsequently pursued doctoral work in the subject matter, but he was forced to abandon the academic project due to "political and personal life difficulties".<ref name="Hagyo Testimony">{{cite web|last=Hagyo|first=Miklos|title=Testimony of Miklos Hagyo I. accused at Kecskemet Tribunal No.1.B.73/2012|url=http://hagyomiklos.com/files/Testimony_of_Miklos_Hagyo_REVISED.pdf|work=hagyomiklos.com|accessdate=22 April 2013}}</ref> Hagyó did however co-author a published scientific investigation into the "conditions of the food retail trade units in Hungary".<ref>{{cite journal|last=Lakner|first=Z.|author2=Hagyo, M. |author3=Hajdu, I. |title=Óriások között. Közelkép a magyar élelmiszer-kiskereskedőkről.|journal=Gazdálkodás|year=2000|volume=44|issue=1|pages=59–67|url=http://www.cabdirect.org/abstracts/20001812376.html;jsessionid=F3321677AB144648EFC8658858BC9F09;jsessionid=8B844BC397D5708C6543885DE99BFE88?gitCommit=4.13.29&gitCommit=4.13.29|accessdate=22 April 2013}}</ref>

== Career ==

=== Private Sector ===

Resulting from the fall of the [[Iron Curtain]] in 1989, and the subsequent transition to a [[market economy]], [[Hungary]] saw a significant increase in [[foreign direct investment|foreign investment]] and trade after 1991.<ref name=Factbook>{{cite web|title=Hungary|url=https://www.cia.gov/library/publications/the-world-factbook/geos/hu.html|work=The World Factbook|publisher=CIA|accessdate=23 April 2013}}</ref>  Taking advantage of that, Hagyó began working in 1992 for [[Wrigley Company|Wrigley]]'s Hungarian operation, Hungaria. After working his way into an exclusive regional distribution position, Hagyó became involved in many [[Start-up company|start-up]] food distribution companies. According to him, he enjoyed financial growth and professional success "thanks to the [post-Soviet] economic boom." <ref name="Szilagyi; Borsodi" />

=== Politics ===

In 1998, Hagyó became a member of the [[Hungarian Socialist Party]], citing [[Gyula Horn]]'s "personality and role" as his attraction toward [[politics]].<ref name="Hagyo Testimony" />

Four years later, Hagyó was one of 178 [[socialists]] elected to the [[National Assembly]], thus granting him the title [[Member of Parliament]]. Thereafter he served as the party's president of district XII in [[Budapest]], which placed him on the party's national board.<ref name="Hagyo Testimony" /> At various moments between July 2002 until May 2006, Hagyó was active in various [[parliamentary committee]]s: Joint EU Institutions Committee, Regional Development Committee, Environmental Committee, and Foreign Affairs Committee.<ref name="Orszag Haza">{{cite web|title=Hagyo Miklos|url=http://www.parlament.hu/internet/plsql/ogy_kpv.kepv_adat?p_azon=h023|work=Az Orszag Haza|publisher=parlament.hu|accessdate=23 April 2013}}</ref> Also during that time, he delivered three speeches in [[Hungarian Parliament|Parliament]], and he submitted a total of 55 independent and non autonomous [[motion (parliamentary procedure)|motions]].<ref name="Orszag Haza" />

In 2005, acting as the party's chairman for nationwide charitable activities, he and Dr. Lajos Oláh organized Hope for [[Transylvania]]. With the help of the [[Hungarian Red Cross]], they collected and delivered food, medicine, tents, blankets, and other emergency supplies to residents in the severely flooded region.<ref name="Debreceni InfoPortál">{{cite news|title=Sikerrel zárult az MSZP Reménysugár Erdélyért akciója|url=http://www.dip.hu/index.php?action=cikk&id=13499|accessdate=23 April 2013|newspaper=Debreceni InfoPortál|date=22 December 205}}</ref> Moreover, he helped secure substantial funds from the Hungarian Red Cross in an effort to transport many of the victims to [[Hungary]] while the damaged communities were restored.<ref name="MTV - Archive">{{cite web|title=Az MSZP-s önkormányzatok székelyföldi gyerekeket látnak vendégül|url=http://www.tvarchivum.hu/?id=57658|publisher=MTV - Archive|accessdate=23 April 2013}}</ref>

Due to the social unrest of a [[2006 protests in Hungary|2006 scandal]] involving former [[Socialist]] [[Prime Minister]] [[Ferenc Gyurcsány]], many thought that opposing [[Fidesz]] representatives would oust the socialists from their [[Parliamentary seat|seats]] in [[Budapest]].  Despite that, the [[Alliance of Free Democrats]]-[[Hungarian Socialist Party|Socialist]] coalition incumbent [[Gábor Demszky]] was re-elected.  In addition to holding his parliamentary position, Hagyó was appointed as [[Deputy Mayor]] of [[Budapest]] on December 14, 2006.<ref name="Hagyo Testimony" /> As Deputy Mayor, Hagyó wield political oversight over the city's urban and asset management responsibilities.

During his second parliamentary term, Hagyó was active in the following committees between May 2006 and June 2007: Economic and Information Technology Commission, the Commission's Energy Subcommittee and the Enterprise Research and Development Subcommittee.<ref name="Orszag Haza" /> Also during that allotted time, he made three speeches to Parliament, and he submitted a total of 15 independent and joint motions.<ref name="Orszag Haza" />

As a member of the [[Hungarian Olympic Committee]], Hagyó was elected President of the Hungarian [[Synchronized Swimming]] Association (MSZSZ) in 2007 after the resignation of the previous president, Csaba Haranghy.<ref name=MOB>{{cite web|title=FŐPOLGÁRMESTER–HELYETTES A SZINKRONÚSZÓ - ALSZÖVETSÉG ELNÖKE|url=http://mob.hu/content/index/id/74728|work=mob.hu|publisher=Magyar Olimpiai Bizottság|accessdate=23 April 2013}}</ref> In a 2010 interview, MSZSZ Secretary General Laszló Szimandl stated in an interview:

{{quote|Mr. Miklós Hagyó greatly helped over the last two years...He provided the financial backing, thanks to which we were able to provide a coach from abroad for training sessions, and were able to organize professional training camps for our national team. We finally had the background support for our professional sport achievements.'''<ref name=stop.hu>{{cite news|title=Hagyó Miklós az elmúlt két évben nagyon sokat segített|url=http://www.stop.hu/sport/hagyo-miklos-az-elmult-ket-evben-nagyon-sokat-segitett/609913/|accessdate=23 April 2013|newspaper=stop.hu|date=11 February 2010}}</ref>}}

At that time, Hagyó was forced to resign from the [[Hungarian Socialist Party|socialist party]] and its associated position.  He therefore was unable to remain the president of the [[synchronized swimming]] association.

According to [[Corporate video|promotional videos]],<ref name="Budapest, I love you">{{cite web|title=Budapest, I love you like this!|url=https://www.youtube.com/watch?v=Apti8vL85Cc|publisher=Dosszie Hagyo|accessdate=23 April 2013}}</ref> his pride of his [[Jászberény]] [[Cultural heritage|heritage]]<ref name="Farm to Fame">{{cite web|last=Hawking|first=Ben|title=From the Farm to Fame - Who is Mr. Miklos Hagyo?|url=http://thehagyocase.wordpress.com/2012/06/22/from-the-farm-to-fame-who-is-mr-hagyo-miklos/|work=The Hagyo Case|accessdate=23 April 2013}}</ref>  - an ethnically mixed community due to historical wars - and his recognition of [[Romani people|Roma]] soldiers,<ref name="Múltikor">{{cite web|title=Az 1848-1849-es szabadságharc zsidó és roma hőseire emlékeztek|url=http://www.mult-kor.hu/cikk.php?id=20079|work=Múlti-kor történelmi portál|accessdate=23 April 2013}}</ref> Hagyó is a supporter of [[ethnic diversity]].  During a March 2008 speech recognizing the efforts of [[Jews]] and [[Romani people|Roma]] in the [[Hungarian Revolution of 1848|1848 revolution]], Hagyó reminded his Hungarian audience that the history of the [[Serbs]], the [[Croatians]], and the [[Poles]] are regrettably more known than that of the [[Romany soldiers|Romany]] soldiers.<ref name="Múltikor" />

Responding to the [[2008 Tibetan unrest]], Hagyó signed a denunciatory petition to promote Hungarian political support for the [[Tibetans]].  While doing so, he urged all politician to come forth with support despite [[political affiliation]].  Moreover, Hagyó stated:

{{quote|We do not consider it desirable to [[boycott]] the [[Olympic Games]] and vehemently reject any initiative of any country to boycott them, and we object to any country’s decision to forbid its athletes to attend the Games. However, the week’s ongoing events should make every responsibly thinking person consider protesting [[China]]’s behavior with regards to the [[Tibetans]]. Even though the Tibetan issue of [[Dictatorship|dictatorial governance]] overshadows the Olympic Games, I sincerely hope that the scheduled summer Games will in no way be compromised, and that the Olympic Games, which bring a message of bravery and friendship, will bring peace and equality to all.'''<ref name=HirExtra>{{cite news|title=Hagyó Miklós bojkottál|url=http://www.hirextra.hu/2008/04/01/hagyo-miklos-bojkottal/|accessdate=23 April 2013|newspaper=HírExtra|date=1 April 2008}}</ref>}}

=== Budapest Transit Authority (BKV) ===

In 2009, BKV was investigated by city's police and the State Audit Office of Hungary because of an employee severance payment controversy.<ref name="Turmoil on the Transportation Front">{{cite web|title=Turmoil on the Transportation Front|url=http://hagyofiles.blogspot.hu/2012/12/turmoil-on-transportation-front.html|work=Hagyo Files|accessdate=24 April 2013}}</ref>  Many of BKV's upper management were interrogated concerning possible involvement.

Former BKV chief executive office Zsolt Balogh gave an interview to the daily newspaper ''[[Magyar Nemzet]]'' (Hungarian National) on March 6, 2010<ref name="Balogh Magyar Nemzet">{{cite web|last=Csermely|first=Peter|title=I Handed Over the Money in a Nokia Box to Hagyo|url=http://hagyomiklos.com/files/Magyar_Nemzet_Eng_REVISED.pdf|work=Magyar Nemzet|accessdate=24 April 2013}}</ref> and a separate interview the following day to [[HírTV]].<ref name="Balogh HirTV Interview">{{cite web|title=HírTv Interview with Zsolt Balogh|url=http://hagyomiklos.com/files/HirTv_ENG_REVISED.pdf|work=HírTv|accessdate=24 April 2013}}</ref> During the interviews, Balogh publicly accused Hagyó of [[extortion]] and [[Breach of fiduciary duties|breach of fiduciary duty]].  According to the accuser, he entered Hagyó's office "in late September, early October 2007"<ref name="Balogh Magyar Nemzet" />  as the freshly promoted [[chief technology officer]]. Then and there, Balogh claimed, Hagyó dictated that Balogh must "be hard, because things must be done here, and who doesn't do it" will receive no mercy, and the throat "must be cut."<ref name="Balogh Magyar Nemzet" />

In the same two interviews, Balogh alleged that during a later meeting in [[Gödöllő]], Hagyó demanded from Balogh an annual "membership fee" of 15 million [[forints]] which would need to be paid directly to Hagyó.<ref name="Balogh HirTV Interview" /> Balogh admitted that the following day he obliged to the demand. In the March 6th interview, Balogh stated that he met Hagyó in the office of Ottó Lelovics, former [[public relations]] and communication consultant to BKV. Then and there he passed 15 million forints to Hagyó.  Balogh stated that he knew "15 million fits in a [[Nokia]] box," so he hid the [[money]] inside a Nokia box designed for mobile phone packaging.<ref name="Balogh Magyar Nemzet" /> As a result, this [[scandal]] has become known as the "Nokia Box Case" or the "Magic Box Case."

On March 7, 2010 the online Hungarian news site ''[[Index.hu]]'' reported that Hagyó resigned from his local and national positions in the [[Hungarian Socialist Party]].<ref name="Index - Hagyo Leaves Party">{{cite news|title=Hagyó Miklós felfüggesztette párttagságát|url=http://index.hu/belfold/2010/03/07/hagyo_miklos_felfuggesztette_parttagsagat/|accessdate=25 April 2013|newspaper=Index|date=7 March 2010}}</ref> In the same article, the party's [[spokesman]], István Nyakó, stated that the accusations had "not been about the truth."<ref name="Index - Hagyo Leaves Party" />  Nyakó also explained that other [[socialist]] [[politicians]] did not want the scandal to hinder the party in the upcoming [[Hungarian parliamentary election, 2010|2010 elections]].<ref name="Index - Hagyo Leaves Party" />

Immediately following the change of [[government]] on May 14, 2010, Hagyó was detained by police.<ref name="Police hold Hagyo">{{cite news|title=Police hold ex-deputy mayor Hagyo|url=http://www.politics.hu/20100517/police-hold-exdeputy-mayor-hagyo/|accessdate=25 April 2013|newspaper=Politics.hu|date=17 May 2010}}</ref> In addition to the [[bribery]] accusations from Balogh, police suspected him of instructing former BKV managing director Attila Antal of concluding an unnecessary [[contract]] with [http://www.aam.hu/hu/fooldal/ AAM] and misappropriating funds related to the suburban railway line [[HÉV]] passenger information supply system.<ref name=Indictment>{{cite web|title=Vádirat (Indictment)|url=http://hagyomiklos.com/files/CENTRAL_INVESTIGATING_CHIEF_PROSECUTOR_revised1.pdf|work=hagyomiklos.com|publisher=Central Investigating Chief Prosecutor's Office|accessdate=25 April 2013}}</ref>

== Pretrial Detention ==

On May 26, 2010 Hagyó was incarcerated at the Budapest Penitentiary in spite of numerous appeals from Hagyó's lawyer, András Kádár, who argued that his client suffered from numerous illnesses and no evidence supported the stringent measure.<ref name="Pretrial detention order">{{cite web|last=Szivos|first=Maria|title=The Metropolitan Court as second instance court 30.AJC.1355/2010/02.|url=http://hagyomiklos.com/files/7_ENG_REVISED.pdf|work=hagyomiklos.com|accessdate=25 April 2013}}</ref> The signatory judge, Mária Szívós, cited the necessity of the coercive measure as prevention from suspect collusion<ref name="Pretrial detention order" />  and prevention from Hagyó fleeing the country.<ref name="Police hold Hagyo" />  Prior to this, Hagyó communicated via his lawyer, András Kádár, willingness to cooperate in the ongoing investigation.<ref name="Cooperation Letter">{{cite web|last=Kadar|first=Andras|title=Letter From Kadar to Colonel Mihály Bezsenyi|url=http://hagyomiklos.com/files/elso_oldal_ENG.pdf|work=hagyomiklos.com|accessdate=25 April 2013}}</ref>

Hagyó's initial release date was June 17, 2010.<ref name="Pretrial detention order" /> The prosecution repeatedly proposed sentence extensions, which were approved by the Pest Central District Court and confirmed by the Municipal Court.<ref name="Jail Extension">{{cite web|last=Keresztes|first=Imre|title=Inv. 477/2010. (Proposal for Extension of Pretrial Detention From Chief Prosecutor Imre Keresztes to Pest Central District Court)|url=http://hagyomiklos.com/files/22_ENG_REVISED.pdf|work=hagyomiklos.com|accessdate=25 April 2013}}</ref> Hagyó was released from [[prison]] into house arrest on February 13, 2011, and he remained on house arrest until June 8, 2011.<ref name=Indictment />

On September 6, 2010, 11 days before his [[pretrial detention]] circumstance would again be extended via order 28.AJC.No.1850/2010/2.,<ref name="Jail Extension" /> Hagyó and Kádár filed application no.52624/10 with the [[European Court of Human Rights]].  In the application, they complained against validity of the pretrial detention and numerous prison conditions under [[Article 5 of the European Convention on Human Rights|Article 5]] § 1 (c), [[Article 5 of the European Convention on Human Rights|Article 5]] § 4 in conjunction with [[European Convention on Human Rights|Article 13]], [[Article 3 of the European Convention on Human Rights|Article 3]], and [[Article 8 of the European Convention on Human Rights|Article 8]] in conjunction with [[European Convention on Human Rights|Article 13]] of the [[European Convention on Human Rights]].  On April 23, 2013 the Court announced its decision, and in doing so they found the Hungarian judicial system to be in violation of the articles associated with the complaint.

While supporters of Hagyó's detainment agreed that the alleged [[mafia boss]] needed to be detained before administering further damage,<ref name="XpatLoop - Mafia Boss">{{cite web|title=Hagyo Indicted As Budapest Transport Company "Mafia Boss"|url=http://www.xpatloop.com/news/70615|work=XpatLoop.com|accessdate=26 April 2013}}</ref> the [[judicial process]] through which the decisions were made to imprison Hagyó received much criticism.  A Hungarian [[law]] student used the detainment and the subsequent trial as [[case studies]], focusing on the issue of basic rights established by the [[European Convention on Human Rights|Convention]].<ref name="Eva Kadar - Basic Rights">{{cite web|last=Kádár|first=Éva|title=BKV-ügy – a törvényes bírósághoz való jog hatálya alól is kiemelve?|url=http://www.jogiforum.hu/files/publikaciok/kadar_eva__bkv_ugy_a_tovenyes_birosaghoz_valo_jog_hataly_alol_is_kiemelve%5Bjogi_forum%5D.pdf|work=http://www.jogiforum.hu/|publisher=Jogi Fórum Publikáció|accessdate=16 May 2013}}</ref> The main points of contention were the prosecution's and court's unwillingness to allow neither Hagyó nor his lawyer access to the evidence<ref name="Eva Kadar - Basic Rights" /> which, according to the former bodies, necessitated an extended pretrial detention, especially when Hagyó suffered from deteriorating health.<ref name="Prison Hospital Letter">{{cite web|last=Toma|first=Dr. Albert|title=59-63-52/2010. (Letter from Central Penitentiary Hospital to Chief Prosecutor Dr. Katalin Kutron)|url=http://hagyomiklos.com/files/4_7_ENG_REVISED.pdf|work=hagyomiklos.com|accessdate=26 April 2013}}</ref>

As Hagyó was a young and popular socialist politician, some speculate that the detention and the trial have political, rather than criminal, motivations.  This speculation stems from bloggers,<ref name=Balogh /><ref name="Ben Hawking - Show Trial in Hungary">{{cite web|last=Hawking|first=Ben|title=A Show Trial n Hungary|url=http://thehagyocase.wordpress.com/2013/02/15/a-show-trial-in-hungary/|work=thehagyocase.wordpress.com|accessdate=26 April 2013}}</ref><ref name="Hagyo Files - Indictment Manipulation">{{cite web|title=Justice or No Justice: The Prosecution Manipulates the Indictment, Again|url=http://hagyofiles.blogspot.hu/2013/03/justice-or-no-justice-prosecution.html|work=Hagyo Files|accessdate=26 April 2013}}</ref> academics,<ref name="Eva Kadar - Basic Rights" /><ref name="Scheppele - Let's Pick Judges">{{cite news|url=https://krugman.blogs.nytimes.com/2012/03/10/first-lets-pick-all-the-judges/|title=First, Let's Pick All The Judges|last=Scheppele|first=Kim Lane|date=10 March 2012|work=|newspaper=New York Times|author-link=Kim Lane Scheppele|via=|accessdate=26 April 2013}}</ref> and journalists.<ref name="Nepszava - Csatat Nyert Hagyo">{{cite news|title=Csatát nyert Hagyó|url=http://www.nepszava.hu/articles/article.php?id=640105|accessdate=16 May 2013|newspaper=Népszava Online|date=24 April 2013}}</ref>

Critics of Hagyó consider him a [[relic]] of [[Soviet]]-imposed [[communism]], a symbol of corruption.<ref name=Hirszerzo>{{cite news|title=Lendvai: ha Hagyó bűnös, nincs helye a pártban|url=http://hirszerzo.hu/hirek/2010/3/6/142537_lendvai_ha_hagyo_bunos_nincs_helye_a_partba|accessdate=16 May 2013|newspaper=Hirszerző|date=6 March 2010}}</ref><ref name="Neszabadsag Hagyo Leaves Party">{{cite news|title=Hagyó hátrahúzásáról egyeztettek|url=http://nol.hu/archivum/mesterhazy_a_hagyo-ugyrol?ref=sso|accessdate=16 May 2013|newspaper=Népszabadság Online|date=1 February 2010}}</ref>

=== Forgery Accusations ===

While serving his pretrial detention at the Budapest Penitentiary, Miklós Hagyó, with legal oversight from Dr. Viktor Géza Szűcs, empowered his common-law wife to act on his behalf with regards to Hagyó's company, WIRTASS Trade and Service, LLC. on August 6, 2010.<ref name="Procuration Doc">{{cite web|last=Hagyo, Miklos;|first=Szucs, Viktor|title=Procuration|url=http://www.hagyomiklos.com/files/2_1ENG.pdf|work=hagyomiklos.com|accessdate=23 June 2013}}</ref> After receiving the authorization of the trial's prosecutor, the document was sent along with a letter from Hagyó's common-law wife to Brigader-General Csaba Boglyasovszky, the principal administrator of the Venyige Street Penal Enforcement Institution.<ref name="Common-Law Wife Visitation Request">{{cite web|title=Request in the Matter of Extraordinary Visitation|url=http://www.hagyomiklos.com/files/4_1ENG.pdf|work=hagyomiklos.com|accessdate=23 June 2013}}</ref> In the letter, Hagyó's partner states that the new situation would require extraordinary visitation rights.  She was previously allowed to visit Hagyó one time per month.  In her request dated August 17, 2010 she states that "I would also like to record, that the actual mutual confidence alone is not sufficient to provide [for WIRTASS LLC.] adequate, objective and rational basis for decision making."<ref name="Common-Law Wife Visitation Request" />   She therefore requested rights for two separate visitations per month to be "at pre-announced times in accordance with the house rules of the penal enforcement institution."<ref name="Common-Law Wife Visitation Request" /> Principal Administrator Csaba Boglyasovszky positively replied to the request on August 19, 2010 and granted her a visitation on the 26th of August, 2010.<ref name="XO Visitation Rights Granted">{{cite web|title=Coverage: #6486 D.001.|url=http://www.hagyomiklos.com/files/4_2ENG.pdf|work=hagyomiklos.com|accessdate=23 June 2013}}</ref> In the letter, Boglyasovszky stated, "Referring to the letter dated on the 17th of August 2010. I inform you that for visiting Miklós Hagyó detainee, to you as the legal representative is allowed."<ref name="XO Visitation Rights Granted" />

Prior to that, the television show Célpont aired by HírTV on March 21, 2010 allegedly showed material which were damaging to Miklós Hagyó and his partner.<ref name="Slander Suit">{{cite web|title=Forgery Accusation: What Exactly Happened?|url=http://www.hagyomiklos.com/?q=en/node/31|work=hagyomiklos.com|accessdate=23 June 2013}}</ref> As a result, Hagyó filed a lawsuit against HírTV within the Budapest Regional Court.<ref name="Slander Suit" /> On August 6, 2010 Hagyó and Dr. Viktor Szűcs signed a document which granted Hagyó's common-law wife to act as his legal representative in the slander lawsuit.<ref name="Slander Suit Law Reps">{{cite web|title=Procuration|url=http://www.hagyomiklos.com/files/2_2ENG.pdf|work=hagyomiklos.com|accessdate=23 June 2013}}</ref> Hagyó presented this authorization to the prison via his counselor on September 2, 2010.<ref name="Slander Suit" /> 
The prosecution observed this and the fact that Hagyó's common-law wife lacked the necessary legal qualifications.  They therefore filed misdemeanor foregery charges against Hagyó, his partner, and Dr. Szűcs.<ref name="Slander Suit" />

All three persons were cleared of the charges by the Pest District Central Court on January 16, 2012.<ref name="Forgery Verdict">{{cite web|last=Német|first=Dr. Leona Judit|title=Verdict: No. 6.B.35649/2011/11.|url=http://hagyomiklos.com/files/10_a_ENG_REVISED.pdf|work=hagyomiklos.com|accessdate=23 June 2013}}</ref>

== The trial ==

The trial of Miklós Hagyó and 14 other defendants is currently underway. It began in June 2012 at the [[Kecskemét]] Tribunal in the city of Kecskemét.<ref name="XpatLoop - Mafia Boss" />

Trial 1.B.73/2012<ref name="Resolution of Court Appointment">{{cite web|title=21/2012. (II. 16.) OBHE számú határozat bíróság kijelöléséről [OBHE No. 21/2012. (II. 16.), Resolution of Court Appointment]|url=http://www.birosag.hu/sites/default/files/allomanyok/obh/eljaro_bir/szabalyzat_hatarozat/21_2012_obh_hatarozat11.pdf|work=www.birosag.hu|publisher=Országos Bírósági Hivatal Elnöke (Chairman of the National Judicial Office)|accessdate=2 July 2013}}</ref> ''Hagyó Miklós és társai ellen bűnszervezetben elkövetett hűtlen kezelés bűntette és más bűncselekmények
''miatt ''(Against Miklós Hagyó and Others Who Perpetrated Organized Crimes of Misappropriation of Funds and Other Crimes), like the pretrial detention circumstances, has been shrouded in controversy.  Apart from the speculative political nature of the case, red flags were raised when the trial was transferred from the Budapest Metropolitan Tribunal to the Kecskemét Tribunal.

The trial transfer was permitted under a temporary provision relating to the Act XIX. of 1998. (Criminal Proceedings) and Act LXXXIX. of 2011. (accelerating criminal justice).<ref name="4th Amendment: Analysis of Consequences">{{cite web|title=The Fourth Amendment of the Basic Law: analysis of the consequences|url=http://www.riskandforecast.com/post/flash-report/the-fourth-amendment-of-the-basic-law-analysis-of-the-consequences_795.html|work=www.riskandforecast.com|publisher=Political Capital|accessdate=2 July 2013}}</ref>



Miklós Hagyó’s case has already been used by the politic in Hungary three times: the former politician was the symbol of corruption both in the national and the local elections in 2010. The only basis was a confession of a witness which was withdrawn since then. CÖF, the “civil” organization attached to the administration “advertised” the left-wing before the national elections of 2014 with posters in which Hagyó as a criminal was illustrated in the company of former prime ministers and party chairmen. There is no doubt that Hagyó’s case shall be used during the local elections in autumn. Furthermore, the restarted trial before the Kecskemét Tribunal will not give a quick end for sure since it is a very complex case indeed. Supposedly the right-wing will use Hagyó’s case even in the national elections of 2018 to blacken the political counter-party.

We do not know how the court considers but it is a fact that Zsolt Balogh eventually withdrew his confession in October, 2012. “I am not a hero. I did not want anything else but to go home” – he confessed the reason why he gave an interview to Magyar Nemzet daily and HÍR TV, the home media of FIDESZ. As he told the court he was forced by the police to tell stories about the NOKIA box in order to remain at large.

== Personal life ==

Hagyó completed the pilgrimage on [[Way of St. James|El Camino de Santiago]].<ref name="El Camino Video">{{cite web|title=A Video Clip from RTL: Hagyó and the Trip on El Camino|url=http://thehagyocase.wordpress.com/2013/05/06/a-video-clip-from-rtl-hagyo-and-the-trip-on-el-camino/|work=hagyocase.wordpress.com|publisher=RTL|accessdate=16 May 2013}}</ref>

== Studies ==

- A (from the scope of basic constitutional rights) special case? 

- BKV case- are they represented fairly according to the statutory right of the court? 

- The legend of the Nokia-box and the pure truth

== External links ==
* http://www.hagyomiklos.com (Hungarian and English)
* https://www.facebook.com/thehagyocase (English)
* https://www.facebook.com/HagyoFiles (Hungarian and English)
* https://www.facebook.com/hagyodosszie (Hungarian)

== References ==

{{reflist|2}}

{{DEFAULTSORT:Hagyo, Miklos}}
[[Category:1967 births]]
[[Category:Living people]]
[[Category:Hungarian businesspeople]]
[[Category:Hungarian Socialist Party politicians]]
[[Category:Members of the National Assembly of Hungary (2002–06)]]
[[Category:21st-century Hungarian politicians]]
[[Category:Members of the National Assembly of Hungary (2006–10)]]
[[Category:People from Jászberény]]