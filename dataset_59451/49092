{{Use dmy dates|date=February 2017}}
{{BLP sources|date=March 2015}}
'''Ernst Frideryk Konrad Koerner''' (born 5 February 1939), usually cited as '''E. F. K. Koerner''', is an author, researcher, professor of linguistics, and [[History of linguistics|historian of linguistics]].

==Early life and education==
Koerner was born in [[Mlewiec]] near [[Toruń]], Poland (formerly called Hofleben near Thorn, [[Prussia]]), on the family manor. He is the second son of the economist Johann Jakob Friedrich Koerner (LL.D., University of Heidelberg, 1922) and his wife Annelise, née Koerner (from a distant Berlin branch of the family). He has two well-known great grandfathers; one was the Lord Mayor of Thorn 1842–1871, Theodor Eduard Koerner (1810–1891; LLD, University of Berlin, 1835), the other the Berlin orientalist painter Ernst Carl Eugen Koerner (1846–1927).

Koerner was educated at the Gymnasium of Krefeld, graduating in March 1960. He performed obligatory military service the following two years, beginning his studies in German and English philology, the history of art, pedagogy, and philosophy at the [[University of Göttingen]] in the summer of 1962, with the idea of becoming a high-school teacher. Three semesters later he moved to the [[Free University of Berlin]], but also studied English literature and applied linguistics for two terms at the [[University of Edinburgh]] (1964–1965), before returning to Berlin in the summer of 1965 for his ''Philosophicum.''

Koerner accepted an appointment as Professeur d’allemand et d’anglais at the Collège Notre-Dame in Valenciennes, France, for the school year 1965–1966.

He returned to Germany and spent the next four semesters at the [[University of Gießen]], completing the exams for both the State Diploma and the M.A. in April and May 1968. By 1967, he had changed the focus of his studies to the analysis of language, writing his M.A. thesis on the development and use of the subjunctive in German.

Although he had made use of some Saussurean notions in his Master’s thesis, it was not until the spring of 1969, during his second semester as a graduate student in general linguistics at Simon Fraser University in Vancouver, B.C., Canada that he began to familiarize himself with the essentials of the posthumously assembled ''[[Cours de linguistique générale]]'' of [[Ferdinand de Saussure]]. He developed the grand scheme of a thesis proposal on the history and evolution of Saussure’s linguistic theory, persuading G. L. Bursill-Hall to act as supervisor. Koerner started, in typical German fashion, with the compilation of a bibliography "on the background, development and actual relevance of Ferdinand de Saussure’s general theory of language." It was accepted for publication a year later, by which time he was busily writing up the dissertation, which was defended in 1971, with [[Dell Hymes]] as external examiner. (It was published 15 months later in Germany and has since been translated into Spanish, Japanese, an Hungarian.)

==University career and major publishing projects==
Winfred P. Lehmann of the University of Texas arranged for Koerner to come to Austin as a Social Scientist Research Associate immediately after his thesis defence. The following academic year (1972–1973), he spent as Visiting Research Associate in [[Thomas A. Sebeok]]’s Research Institute at Indiana University in Bloomington. By this time, he had already begun developing plans of launching a journal devoted to the history of linguistics and several associated monograph series. He had his first meeting with John L. Benjamins, then exclusively an antiquarian and periodical trader, in Amsterdam in August 1972, and in subsequent exchanges they agreed to launch ''Historiographia Linguistica'', the first journal devoted to the history of linguistics, and several associated book series, "Amsterdam Classics in Linguistics"  and "Classics in Psycholinguistics", both making 19th and early 20th century texts available again with introductions by modern specialists, and notably "Studies in the History of the Language Sciences" (SiHoLS), in which so far 116 volumes have appeared.

In September 1973, Koerner relocated to Germany, as a ''habilitandus'' and research fellow associated with the Chair of General Linguistics at the University of Regensburg, which permitted him to continue his research and to build both the journal and the monograph series, not only those pertaining to the History of Linguistics but also the "Current Issues in Linguistic Theory" (CILT) series, in which 319 volumes have been published as of September 2011.

In 1976 Koerner became Associate Professor and Director of the Linguistics Documentation Centre at the University of Ottawa. While he also taught undergraduate courses on 20th-century linguistics from [[Edward Sapir|Sapir]] to [[Noam Chomsky|Chomsky]] and a graduate seminar on the History of Linguistics, his main subjects of instrction were phonetics, semantics, and historical linguistics. In 1980 was invited to teach the History of Linguistics  at the LSA Linguistic Institute held in Albuquerque, New Mexico.

In 1978 he organized in Ottawa the first International Conference on the History of the Language Sciences (ICHoLS), thus providing scholars in the field with a forum for the exchange of ideas and research results and projects. Since then there have been eleven more such conferences, in Lille (1981), Princeton (1984), Trier (1987), Galway (1990), Washington, D.C. (1993), Oxford (1996), Paris (1999), São Paulo/Campinas (2002), Urbana-Champaign (2005), Potsdam (2008), and St Petersburg (2011).

Koerner was a prime mover in establishing the [http://www.henrysweet.org/ Henry Sweet Society for the History of Linguistic Ideas] (HSS) in Oxford in the Spring of 1984, and the North American Association for the History of the Language Sciences (NAAHoLS) during the annual meeting of the Linguistic Society of America held in San Francisco, California, in December 1987.

While at Ottawa he co-organized, with William Cowan of Carleton University and Michael K. Foster of the National Museum of Man (now the Canadian Museum of Civilization), the international Edward Sapir Centenary Conference, which took place in the Victoria Memorial Museum, Ottawa, in October 1984. It was also in 1984 that Koerner established ''Diachronica: International Journal for Historical Linguistics'', first published by Georg Olms and afterwards by John Benjamins. ''Diachronica'' provides a forum for the presentation and discussion of all aspects of language change, in any and all languages of the globe. Until December 2001, Koerner was General Editor, and since then has continued to act as Consulting Editor.

He served as the Subject Editor for History of Linguistics of the 10-volume ''Encyclopedia of Language & Linguistics'', ed. by R. E. Asher (Oxford & New York: Pergamon Press, 1994). In 2000, he served as a consultant on History of Linguistics for the ''Encyclopædia Britannica'' (Chicago). He was chief editor of the three-volume ''History of the Language Sciences'' (Berlin & New York: Walter de Gruyter, 2000–2006). In 2007, his 3-volume edition of ''Edward Sapir: Critical assessments of leading linguists'' (London & New York: Routledge) appeared. With the creation of ''Historiographia Linguistica'', the editing of various series entirely devoted specifically to the subject, and the organization of the first congress in the History of Linguistics, Koerner can be said to have more or less single-handedly professionalized the field, making it an internationally recognized subject of serious scholarly endeavour within linguistics and adjacent disciplines.

===Current activity and honours===
Koerner was named Professor of General Linguistics at the University of Ottawa in 1988, and retired as Professor Emeritus in 2001. Since then has lived in Berlin, where he continues to carry out his very active programme of editorial activities and research. In October 1994, he received the honorary degree of ‘Doctor of the Philological Sciences’ from the University of Sofia, Bulgaria; in February 1995, Nicolaus Copernicus University of Toruń, Poland, awarded him its Medal of Merit. In May 1997, he was elected Fellow of the Royal Society of Canada; in February 1998, he was made a Fellow of the Royal Society of Arts (London). In 2001–2002 Koerner was Fellow-in-Residence at the Netherlands Institute for Advanced Study in the Humanities and Social Sciences in Wassenaar near Leiden. In 2002–2003 Koerner was, as an awardee of the Konrad Adenauer Research Prize of the Alexander von Humboldt Foundation, a visiting scholar, first at the University of Cologne and then at the Zentrum für Allgemeine Sprachwis-senschaft in Berlin, with which he has been associated ever since. In April 2004, the University of St. Petersburg awarded him the degree of "Doctor honoris causa".
His 50th and 60th birthdays in 1999 were marked with the publication of ''Festschriften'' and bibliographies of his work, and another volume of studies honouring his 70th birthday appeared in 2010 (listed at end of Bibliography). In September 2016, the University of Nicolaus Copernicus in Poland awarded him the degree of "Doctor honoris causa UMK"

==Bibliography==
===Monographs by Koerner===

* ''Bibliographia Saussureana 1870–1970: An annotated, classified bibliography on the background, development, and actual relevance of Ferdinand de Saussure’s general theory of language'' (Metuchen, New Jersey: Scarecrow Press, 1972).
* ''Contribution au débat post-saussurien sur le signe linguistique: Introduction générale et bibliographie annotée'' (The Hague & Paris: Mouton, 1972).
* ''Ferdinand de Saussure: Origin and Development of His Linguistic Thought in Western Studies of Language. A contribution to the history and theory of linguistics'' (Braunschweig: Friedrich Vieweg & Sohn [Oxford & Elmsford, N.Y.: Pergamon Press], 1973). — This book has been translated into Hungarian (Budapest: Tankönyvkiadó, 1982); Japanese (Tokyo: Taishukan, 1982), and Spanish (Madrid: Gredos, 1982).
* ''The Importance of Techmer’s “Internationale Zeitschrift für Allgemeine Sprachwissenschaft” in the Development of General Linguistics'' (Amsterdam: John Benjamins, 1973).
* ''Western Histories of Linguistics, 1822–1976: An annotated, chronological bibliography'' (Amsterdam: John Benjamins, 1978).
* ''Toward a Historiography of Linguistics: Selected essays.'' Foreword by R. H. Robins (Amsterdam: John Benjamins, 1978).
* ''Noam Chomsky: A personal bibliography, 1951–1986'' (Together with Matsuji Tajima as associate compiler) (Amsterdam & Philadelphia: John Benjamins, 1986).
* ''Saussurean Studies / Études saussuriennes''. Avant-propos de Rudolf Engler (Genève: Éditions Slatkine, 1988).
* ''Practicing Linguistic Historiography: Selected essays'' (Amsterdam & Philadelphia: John Benjamins, 1989).
* ''Professing Linguistic Historiography'' (Amsterdam & Philadelphia: John Benjamins, 1995).
* ''Linguistic Historiography: Projects & prospects'' (Amsterdam & Philadelphia: John Benjamins, 1999).
* ''Toward a History of American Linguistics'' (London & New York: Routledge, 2002).
* ''Essays in the History of Linguistics'' (Amsterdam & Philadelphia: John Benjamins, 2004).
* ''Jezikoslovna historigrafija: Metodologija i praksa. Transl. by Milica Lukšić'', ed. by Zvonko Pandžić (Zagreb: Tusculanae Editiones, 2007).
* ''Universal Index of Biographical Names in the Language Sciences'' (Amsterdam & Philadelphia: John Benjamins, 2008).

===Festschriften and similar books in honour of Koerner===
* ''E. F. Konrad Koerner Bibliography'', ed. by William Cowan & Michael K. Foster (Bloomington, Ind.: Eurolingua, 1989).
* ''Professing Koernerian Linguistics'', by Paul Sidwell & Neile A. Kirk (Melbourne: Association for the History of Language, 1998).
* ''E.F.K. Koerner: A biobibliography'', ed. by Pierre Swiggers (Leuven: Peeters for Centre International de Dialectologie Générale, 1999).
* ''The Emergence of the Modern Language Sciences: Studies on the transition from historical-comparative to structural linguistics, in honour of E. F. K. Koerner''. Ed. by Sheila Embleton, John E. Joseph and Hans-Josef Niederehe. Vol. I: Historiographical  Perspectives; vol. II: Methodological Perspectives and Applications (Amsterdam & Philadelphia: John Benjamins, 1999).
* ''Chomskyan (R)evolutions'', ed. by Douglas A. Kibbee (Amsterdam & Philadelphia: John Benjamins, 2010).

{{Authority control}}

{{DEFAULTSORT:Koerner, E. F. K.}}
[[Category:1939 births]]
[[Category:Living people]]
[[Category:Linguists from Poland]]
[[Category:University of Göttingen alumni]]
[[Category:Free University of Berlin alumni]]
[[Category:Alumni of the University of Edinburgh]]
[[Category:University of Giessen alumni]]
[[Category:University of Ottawa faculty]]
[[Category:Fellows of the Royal Society of Canada]]