<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Wib 12 Sirocco
 | image=VickersW127.jpg
 | caption=Wib 122 (Vickers 127)
}}{{Infobox Aircraft Type
 | type=Two seat [[fighter aircraft]]
 | national origin=[[France]]
 | manufacturer=Avions Michel Wibault
 | designer=
 | first flight=May 1926
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=3
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from=[[Wibault 8 Simoun]] 
 | variants with their own articles=
}}
|}

The '''Wibault 12 Sirocco''' or '''Wib 12 Sirocco''' was a two seat, [[parasol wing]] [[fighter aircraft]] designed and built in [[France]] in the 1920s.  Three fighter prototypes were completed, one for the [[RAF]], together two Army co-operation variants. There was no series production.

==Design==
The Wib 12 Sirocco, a single engine, two seat, parasol wing fighter was a structurally much revised development of the similarly configured [[Wibault 8 Simoun|Wib 8 Simoun]], following it through the Avions Marcel Wibault works with less than a month between their maiden flights. They were externally very similar but the more refined structure of the Wib 12 made it both stronger and 12% lighter than its predecessor.  One major change was the replacement of the Wib 8's box [[spar (aviation)|spars]] with ones of I-section.<ref name=G&S/>

The general design followed that of Wibault's family of single- and two-seat fighters, the [[Wibault 3|Wib 3]], [[Wibault 7|Wib 7]], Wib 8 and [[Wibault 9|Wib 9]] but in external detail was closest to the Wib 8. It was an all-metal aircraft, the structure mostly [[Duralumin]] and covered with narrow aluminium strips applied longitudinally.   The parasol wing was straight edged and of constant [[chord (aircraft)|chord]], braced to the lower [[fuselage]] with a pair of parallel [[strut]]s on each side which met the wing at about mid-span. The Wib 12 had a new pair of [[jury strut]]s from the main wing struts to the wing underside for strengthening. There were [[cabane strut]]s over the fuselage and a [[trailing edge]] cut-out in the wing over the pilot's [[cockpit]] to enhance his visibility. A pair of [[synchronization gear|synchronised]] {{Convert|7.7|mm|in|3|abbr=on}} [[Vickers machine gun]]s fixed to the fuselage fired forwards through the [[propeller (aircraft)|propeller]] arc; in addition the rear cockpit was fitted with a pair of [[Lewis gun]]s of the same calibre on a [[Scarff ring]].  A braced [[tailplane]] was mounted towards the top of the fuselage, together with an angular [[fin]] and [[rudder]].<ref name=G&S/>

The Wib 12's engine, the same {{convert|500|hp|kW|abbr=on|0}} water-cooled [[V-12 engine|V-12]] [[Hispano-Suiza 12H]]b type as used by the Wib 8, was totally enclosed and drove a two blade propeller.  It was cooled by a retractable, half-cylindrical [[radiator (engine cooling)|radiator]] on the fuselage underside at the back of the engine. Behind the engine the fuselage was flat sided. The fighter had a fixed [[conventional undercarriage]] with mainwheels on a split axle attached to the fuselage underside, supported by a pair of V-struts; there was a small tailskid.<ref name=G&S/>

The Wib 12 and the '''Wib 121''' (the first and second prototypes) were both flown for the first time in May 1926.  The latter was about 8% faster in a climb to 4,000 m (13,125&nbsp;ft) than the heavier Wib 8.<ref name=G&S/> The third aircraft, the '''Wib 122''' was built for [[Vickers]] aircraft, with whom Wibault shared patents and collaborative designs.  They fitted it with a {{convert|550|hp|kW|abbr=on|0|disp=flip}} W-12 [[Napier Lion|Napier Lion XI]] engine and knew it as the '''Vickers Type 127'''. To avoid the central bank of cylinders the Vickers guns were moved from the fuselage top to the sides. Its test programme was interrupted by continual engine overheating problems.<ref name=G&S/><ref name=A&M/>

When the Service Technique de l'Aéronautique (S.T.Aé, Technical Department of Aeronautics) cancelled the two seat fight specification in 1926, further development by Wibault also ended; though the Wib 121 went on a sales demonstration in [[Turkey]] in 1928, no orders were gained.<ref name=G&S/>  Instead, the company tried to develop the design into a two seat Army cooperation reconnaissance aircraft.  The '''Wib 124''' had its armament modified, having no wing guns and only one fuselage mounted [[synchronization gear|synchronised]] [[Vickers machine gun]], but with an added ventral Lewis gun.<ref name=AF_124/> The  '''Wib 125''' had the same armament but was powered by a [[Renault 12 J]]c {{convert|500|hp|kW|abbr=on|0}} water-cooled [[V12 engine|V-12]] engine. Again, no orders resulted.<ref name=AF_125/>

==Variants==
C=Chasseur (fighter); A=Army; 2=two-seater. One of each only.
;Wib 12 Sirocco C.2: First prototype, fuselage and rear seat guns only.<ref name=G&S/>
;Wib 121 Sirocco C.2: Second prototype, wing guns added.
;Wib 122 Sirocco C.2: One aircraft built as the '''Vickers Type 127'''.
;Wib 124 A.2: Army co-operation version. No wing guns and only one synchronised Vickers gun in upper fuselage but a ventral Lewis gun added.<ref name=AF_124/>  
;Wib 125 A.2: Army co-operation version. [[Renault 12 J]]c {{convert|500|hp|kW|abbr=on|0}} water-cooled [[V12 engine|V-12]] engine, same armament as Wib 124.<ref name=AF_125/>
;Vickers Type 127
:\the single Wib 122 Sirocco C.2 built for [[Vickers]], who replaced the Hispano engine with a {{convert|550|hp|kW|abbr=on|0|disp=flip}} W-12 [[Napier Lion XI]]. Vickers referred to it as the '''Vickers Type 127'''; its [[RAF]] serial was  ''J9029''.<ref name=G&S/><ref name=A&M/>

==Specifications (Wib 121 C.2)==
{{Aircraft specs
|ref=Green & Swanborough p.596-7<ref name=G&S/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=One
|capacity=
|length m=9.44
|length note=
|span m=12.66
|span note=
|height m=3.15
|height note=
|wing area sqm=29.63
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=1212
|empty weight note=
|gross weight kg=2050
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Hispano-Suiza 12H]]b
|eng1 type=water-cooled upright V-12
|eng1 hp=500
|eng1 note=
|power original=
|more power=

|prop blade number=2
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=242
|max speed note=at 3,000 m (9,840 ft)
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=6200
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=14.15 min 33 s to 4,000 m (13,125 ft)
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|more performance=
<!--
        Armament
-->
|armament=
*'''Wing guns:''' 2×{{Convert|7.7|mm|in|3|abbr=on}} unsynchronised [[Darne machine gun]]s firing outside the propeller arc.
*'''Fuselage guns''' 2×{{Convert|7.7|mm|in|3|abbr=on}} [[synchronization gear|synchronised]] [[Vickers machine gun]]s firing through the propeller arc.
*'''Rear cockpit:''' 2×{{Convert|7.7|mm|in|3|abbr=on}} [[Lewis machine gun]]s on a [[Scarff ring]].
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}
<!--==Notes==-->

==References==
{{commons category|Wibault aircraft}}
{{reflist|refs=

<ref name=G&S>{{cite book |last=Green |first=William |first2= Gordon|last2= Swanborough |title=The Complete Book of Fighters |year=1994|publisher=Salamander Books|location=Godalming, UK|isbn=1-85833-777-1|pages=596–7}}</ref>

<ref name=A&M>{{cite book |title= Vickers Aircraft since 1908 |last= Andrews |first= CF |last2=Morgan|first2=E.B.  |edition= 2nd |year= 1988|publisher= Putnam|location= London|isbn= 0 85177 815 1|pages=209, 515}}</ref>

<ref name=AF_124>{{cite web |url=http://www.aviafrance.com/wibault-124-aviation-france-9391.htm
|title=Wibault 124|accessdate=1 May 2013}}</ref>

<ref name=AF_125>{{cite web |url=http://www.aviafrance.com/wibault-125-aviation-france-2339.htm
|title=Wibault 125|accessdate=1 May 2013}}</ref>

}}
<!-- ==Further reading== -->
<!-- ==External links== -->
{{Wibault aircraft}}

[[Category:Single-engine aircraft]]
[[Category:Parasol-wing aircraft]]
[[Category:French fighter aircraft 1920–1929]]
[[Category:Wibault aircraft|Wib 12]]