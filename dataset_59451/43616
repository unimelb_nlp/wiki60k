__NOTOC__
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=R.III
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Bomber]]
 | national origin=Germany
 | manufacturer=[[Siemens-Schuckert]]
 | designer=[[Bruno and Franz Steffen]]<ref name="gt572">Grey & Thetford 1962, p.572</ref>
 | first flight=late 1915<ref name="hg187">Haddow & Grosz 1963, p.187</ref>
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=1<ref name="hg187"/>
 | developed from= [[Siemens-Schuckert R.I]]<ref name="hg180">Haddow & Grosz 1963, p.180</ref>
 | variants with their own articles=
}}
|}

The '''Siemens-Schuckert R.III''' was a prototype bomber aircraft built in Germany during World War I.<ref name="jea">Taylor 1989, p.808</ref><ref name="iea">''The Illustrated Encyclopedia of Aircraft'', p.2920</ref> It was one of six aircraft based on the [[Siemens-Schuckert R.I]] that were originally intended to be identical, but which each developed in a different direction and were designated as different aircraft types by the German Inspectorate of Flying Troops (the [[Idflieg]]).<ref name="hg184">Haddow & Grosz 1963, p.184</ref> The aircraft's development was impeded by the unreliability of its [[Maybach HS]] engines,<ref name="hg187"/> and when it was eventually accepted for military service, it was only in a training role.<ref name="hg187"/><ref name="jea"/><ref name="gt573">Grey & Thetford 1962, p.573</ref>

As designed, the R.III was a large three-bay biplane with unstaggered wings of unequal span<ref name="hg181">Haddow & Grosz 1963, p.181</ref> and a fully enclosed cabin. Power was to be supplied by three 180-kW (240-hp) [[Maybach HS]] engines mounted internally in the fuselage, which transmitted their power via driveshafts to two propellers mounted tractor-fashion on the interplane struts nearest the fuselage.<ref name="hg174">Haddow & Grosz 1963, p.174</ref> The main undercarriage consisted of divided units, each of which carried dual wheels, and the tail was supported by a pair of tailwheels.<ref name="hg187"/> The fueslage was forked into an upper and lower section, which allowed a clear field of fire to the rear of the aircraft.<ref name="gt572"/>

The R.III was delivered to [[Döberitz]] for military service on 30 December 1915.<ref name="hg187"/> Problems with the engines began almost immediately, and numerous modifications to the engines and their cooling systems were carried out in the field.<ref name="hg187"/> Despite the modifications, engine problems were probably responsible when the R.III crashed in early 1916.<ref name="hg187"/> New wings were fitted to the aircraft to replace those damaged in the crash, but further attempts to fly the R.III were again hampered by its engines, and the aircraft was returned to Siemens-Schuckert.<ref name="hg187"/> In June, the firm requested permission from the ''Idflieg'' to replace the troublesome engines with [[Benz Bz.IV]] engines, but was told to place the R.III in storage instead.<ref name="hg187"/>

Work on the R.III commenced again in October 1916, when a number of improvements based on field experience with the [[Siemens-Schuckert R.V|R.V]] and [[Siemens-Schuckert R.VI|R.VI]] were incorporated into the aircraft. These included: reinforcement of the tail, engine and gearbox thermometers, lighting in the engine room, and covers for the radiators that could be extended in flight.<ref name="hg187"/> The wing was extended both in span and in chord.<ref name="hg187"/> By this time, the ''Idflieg'' had relaxed its specifications for the R-types ordered from Siemens-Schuckert,<ref name="hg180">Haddow & Grosz 1963, p.192</ref> and with the Benz engines fitted, the R.III was able to meet the lowered standard.<ref name="hg187"/> Siemens-Schuckert delivered the refurbished aircraft to ''[[Riesenflugzeugersatzabteilung]]'' (Rea — "giant aircraft support unit") on 12 December 1916 and it was accepted into service on 21 December.<ref name="hg187"/> In its modified form, the aircraft was of no use on the front line, but was used for training instead,<ref name="hg187"/><ref name="jea"/><ref name="gt573"/> a role in which it was still serving in February 1918.<ref name="hg187"/>

<!-- ==Development== -->
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications==
{{aerospecs
|ref=<!-- reference -->Kroschel & Stützer 1994, p.141
|met or eng?=<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->met

|crew=4
|capacity=
|length m=17.7
|length ft=58
|length in=1
|span m=34.33
|span ft=112
|span in=8
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=4.6
|height ft=15
|height in=8
|wing area sqm=177
|wing area sqft=1,910
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=5,400
|empty weight lb=11,900
|gross weight kg=6,820
|gross weight lb=15,000
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->

|eng1 number=3
|eng1 type=[[Benz Bz.IV]]
|eng1 kw=<!-- prop engines -->150
|eng1 hp=<!-- prop engines -->200
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|max speed kmh=132
|max speed mph=83
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=480
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=3,000
|ceiling ft=9,800
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->

|armament1=3 × 7.9-mm machine guns
|armament2=500&nbsp;kg of bombs
|armament3=
|armament4=
|armament5=
|armament6=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{commons category|Siemens-Schuckert aircraft}}
* {{cite book| last=Gray |first=Peter |author2=Owen Thetford  |title=German Aircraft of the First World War |publisher=Putnam |location=London |year=1962}}
* {{cite book| last=Haddow |first=G.W. |author2=Peter M. Grosz  |title=The German Giants: The Story of the R-planes 1914–1919 |publisher=Putnam |location=London |year=1962}}
* {{cite book |title=The Illustrated Encyclopedia of Aircraft |publisher=Aerospace Publishing|location=London |pages= }}
* {{cite book| last=Kroschel |first=Günter |author2=Helmut Stützer  |title=Die Deutschen Militärflugzeuge 1910–1918 |publisher=Mittler |location=Herford |year=1994}}
* {{cite book| last=Taylor |first=Michael J.H. |title=Jane's Encyclopedia of Aviation |publisher=Studio Editions |location=London |year=1989}}
<!-- ==External links== -->
{{Siemens-Schuckert aircraft}}
{{Idflieg R-class designations}}

[[Category:German bomber aircraft 1910–1919]]
[[Category:Siemens-Schuckert aircraft|R.III]]
[[Category:Three-engined twin-prop tractor aircraft]]
[[Category:Biplanes]]