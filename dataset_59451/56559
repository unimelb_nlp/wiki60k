The '''Development Studies Association''' ('''DSA''') is a scholarly society which works to "connect and promote the development research community in UK and Ireland". It was formally established at the National Development Research Glasgow Conference in 1978. [[David Hulme (academic) | David Hulme]] is the current president of the DSA.

==The Role of the DSA==

The DSA's stated role is as follows:
* improving links and information exchange between DSA members
* representing DSA members in consultations
* bringing the work of DSA members to a wider audience (prospective students, partners and donors).
More broadly they have the aims of:
* promoting the advancement of knowledge on international development
* disseminating information on development research and training
* encouraging interdisclipinary exchange and cooperation.<ref>[http://www.devstud.org.uk/about/ Development Studies Association: About the Association]</ref><ref name="tribe">[http://www.devstud.org.uk/downloads/4b28d5cf9a404_DSA_History_January-2009-draft_ver9-web.pdf Tribe, Michael (2009) A Short History of the Development Studies Association]</ref>

The ''[[Journal of International Development]]'' is the official journal of the Development Studies Association.

The Development Studies Association's case for a development studies unit of assessment a sub-panel in [[HEFCE]]'s Research Assessment Exercise is discussed in Mohan and Wilson (2005)<ref>Mohan, Giles  and Gordon Wilson (2005)The antagonistic relevance of development studies,  Progress in Development Studies 5, 4 pp. 261–278</ref> where they report the DSA claim that: ''Development Studies is driven by real-world issues and problems, rather than by disciplinary perspectives. Success in Development Studies requires that different disciplines find ways to communicate and pool resources. It follows, therefore, that it is characteristic of Development Studies research, that disciplinary priorities are secondary to the engagement with the reality of the lives of men, women and children living in poor countries and also of the processes and policies which affect them. (DSA, 2004)''<ref>Development Studies Association 2004: The case for a development studies unit of assessment (sub-panel).In Research Assessment Exercise 2008.</ref>

==A History of the Development Studies Association==

Tribe, Mike (2009) A Short History of the Development Studies Association,  Journal of International Development, Volume 21, Issue 6  (p 732-741)

==Affiliations==

The DSA is affiliated to the [[European Association of Development Research and Training Institutes| European Association of Development Research and Training Institutes (EADI)]].<ref name="tribe"/>

==Other sources==

Bown L and Veitch M (eds.). 1986. The Relevance of Development Studies to the Study of Change in
Contemporary Britain; ESRC: London.

CDSC (Conference of Directors of Special Courses). Development Studies in Britain: A Select Course Guide. Published in various locations but mainly at the [[Institute of Development Studies]]

Faber M. (1987) Development Studies in Britain: A Look at Ourselves. World Development. 15(4)
April: 533–536.

Grindle MS, Hilderbrand ME. (1999). The Development Studies Sector in the United Kingdom:
Challenges for the New Millennium. Cambridge (Mass): [[Harvard Institute for International Development]] and London: Department for International Development.

==External links==
*{{Official|http://www.devstud.org.uk/}}

==Related associations==

[[Royal_Geographical_Society#Research_groups|Developing Areas Research Group (DARG—within the Royal Geographical Society)]]

[[British International Studies Association]] (BISA—within the [[Political Studies Association]])

==References==
{{Reflist}}

[[Category:Social sciences organizations]]
[[Category:Development studies]]