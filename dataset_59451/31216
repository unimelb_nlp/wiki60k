{{featured article}}
{{italic title}}
[[File:Ghost Stories March 1930.jpg|thumb|Cover of the March 1930 issue]]
'''''Ghost Stories''''' was a U.S. [[pulp magazine]] that published 64 issues between 1926 and 1932.  It was one of the earliest competitors to ''[[Weird Tales]]'', the first magazine to specialize in the fantasy and occult fiction genre.  It was a companion magazine to ''[[True Story (magazine)|True Story]]'' and ''[[True Detective Stories]]'', and focused almost entirely on stories about ghosts, many of which were written by staff writers but presented under pseudonyms as true confessions.  These were often accompanied by faked photographs to make the stories appear more believable.  ''Ghost Stories'' also ran original and reprinted contributions, including works by [[Robert E. Howard]], [[Carl Richard Jacobi|Carl Jacobi]], and [[Frank Belknap Long]].  Among the reprints were [[Agatha Christie]]'s "The Last Seance" (under the title "The Woman Who Stole a Ghost"), several stories by [[H.G. Wells]], and [[Charles Dickens]]' "[[The Signal-Man]]".  The magazine was initially successful, but began to lose readers, and in 1930 was sold to [[Harold Hersey]].  Hersey was unable to reverse the magazine's decline, and ''Ghost Stories'' ceased publication at the start of 1932.

==Publishing history and contents==
[[File:Ghost Stories November 1929.jpg|left|thumb|Cover of the November 1929 issue]]
Fantasy and occult fiction had often appeared in popular magazines prior to the twentieth century, but the first magazine to specialize in the genre, ''[[Weird Tales]]'', did not appear until 1923.<ref name=":1">Weinberg (1985), pp. 626–628.</ref>  ''Ghost Stories'', which was launched by [[Bernarr Macfadden]] in July 1926, was one of ''Weird Tales''' earliest competitors.<ref name=":1" />  Macfadden also published true confession magazines such as ''[[True Story (magazine)|True Story]]''; ''Ghost Stories'' followed this format, with the contents mostly produced by the publisher's staff writers, and attributed in print to a first-person narrator.  The magazine was initially printed on [[Slick (magazine format)|slick]] paper, which was sufficiently good quality to allow photographs to be used, and many of the stories had accompanying photographs purporting to be of their protagonists.  These were replaced by line drawings when the magazine switched to pulp paper in July 1928.  ''Ghost Stories'' occasionally printed contributions from outside writers, including  "The Apparition in the Prize Ring", by [[Robert E. Howard]], under the pseudonym "John Taverel".  Popular writers such as [[Frank Belknap Long]], [[Hugh B. Cave]], [[Victor Rousseau]], [[Stuart Palmer]], and [[Robert W. Sneddon]] all sold stories to ''Ghost Stories'', though the quality suffered because of the limited scope the magazine's formula gave them.  [[Carl Richard Jacobi|Carl Jacobi]]'s first published story, "The Haunted Ring", appeared in the final issue.<ref name=":0">Ashley (1997), p. 406.</ref><ref name="SFFWFM_GS">Ashley (1985a), pp.&nbsp;315–317.</ref>{{#tag:ref|This was not Jacobi's first sale—"Mive", which he had sold to ''Weird Tales'', did not appear in print until the following month.<ref name="SFFWFM_GS" />|name = "Jacobi"|group = note}}

In addition to original material, ''Ghost Stories'' included a substantial number of reprints, including well-known Victorian ghost stories such as "[[The Signal-Man|The Signalman]]" by [[Charles Dickens]], and "The Open Door" by [[Mrs. Oliphant]].  [[Agatha Christie]]'s "The Last Seance" appeared in the November 1926 issue, under the title "The Woman Who Stole a Ghost", and six stories by [[H.G. Wells]] were reprinted, including ghost stories such as "[[The Red Room (Wells)|The Red Room]]" and stories with less obvious appeal to ''Ghost Stories''<nowiki/>' readership, such as "Pollock and the Porroh Man".  [[Arthur Conan Doyle]]'s "The Captain of the Polestar" appeared in the April 1931 issue, and he also contributed a non-fiction piece, "Houdini's Last Escape", which appeared in March 1930.<ref name="SFFWFM_GS" />

Macfadden set up an arrangement with Walter Hutchinson, a U.K. publisher, to exchange suitable material with ''The Sovereign Magazine'' and ''Mystery-Story Magazine'', two of Hutchinson's U.K. genre pulps, and many stories appeared on both sides of the Atlantic as a result.<ref name=":0" />

The magazine was initially fairly successful, but sales soon began to fall.  In March 1930 [[Harold Hersey]] bought the magazine from Macfadden and took over as editor, but he was unable to revive the magazine's fortunes.<ref name="SFFWFM_GS" /><ref>Hersey (1937), p. 190.</ref>  In 1931 the schedule slipped to bimonthly, and three issues later the magazine ceased publication, probably because readers grew bored: the limited scope meant that the contents of the magazine eventually became predictable.  The final issue was dated December 1931/January 1932.<ref name=":0" />

==Bibliographic details==
{| class="wikitable" style="font-size: 10pt; line-height: 11pt; margin-left: 2em; text-align: center; float: right"
! !!Jan !! Feb !!Mar !!Apr !!May !!Jun !!Jul !!Aug !!Sep !!Oct !!Nov !!Dec
|-
!1926
|| || || || || || ||bgcolor=#ffff99|1/1 ||bgcolor=#ffff99|1/2 ||bgcolor=#ffff99|1/3 ||bgcolor=#ffff99|1/4 ||bgcolor=#ffff99|1/5 ||bgcolor=#ffff99|1/6
|-
!1927
|bgcolor=#ffff99|2/1  ||bgcolor=#ffff99|2/2  ||bgcolor=#ffff99|2/3  ||bgcolor=#ffff99|2/4  ||bgcolor=#ffff99|2/5  ||bgcolor=#ffff99|2/6  ||bgcolor=#ffff99|3/1 ||bgcolor=#ffff99|3/2 ||bgcolor=#ffff99|3/3 ||bgcolor=#ffff99|3/4 ||bgcolor=#ffff99|3/5 ||bgcolor=#ffff99|3/6
|-
!1928
|bgcolor=#ffff99|4/1  ||bgcolor=#ffff99|4/2  ||bgcolor=#ffff99|4/3  ||bgcolor=#ffff99|4/4  ||bgcolor=#ffff99|4/5  ||bgcolor=#ffff99|4/6  ||bgcolor=#ffff99|5/1 ||bgcolor=#ffff99|5/2 ||bgcolor=#ffff99|5/3 ||bgcolor=#ffff99|5/4 ||bgcolor=#ffff99|5/5 ||bgcolor=#ffff99|5/6
|-
!1929
|bgcolor=#ffff99|6/1  ||bgcolor=#ffff99|6/2  ||bgcolor=#ffff99|6/3  ||bgcolor=#ffff99|6/4  ||bgcolor=#ffff99|6/5  ||bgcolor=#ffff99|6/6  ||bgcolor=#ffff99|7/1 ||bgcolor=#ffff99|7/2 ||bgcolor=#ffff99|7/3 ||bgcolor=#ffff99|7/4 ||bgcolor=#ffff99|7/5 ||bgcolor=#ffff99|7/6
|-
!1930
|bgcolor=#ffff99|8/1  ||bgcolor=#ffff99|8/2  ||bgcolor=#ffff99|8/3  ||bgcolor=#ffff99|8/4  ||bgcolor=#ffff99|8/5  ||bgcolor=#ffff99|8/6  ||bgcolor=#ffff99|9/1 ||bgcolor=#ffff99|9/2 ||bgcolor=#ffff99|9/3 ||bgcolor=#ffff99|9/4 ||bgcolor=#ffff99|9/5 ||bgcolor=#ffff99|9/6
|-
!1931
|bgcolor=#ffff99|10/1  ||bgcolor=#ffff99|10/2  ||bgcolor=#ffff99|10/3  ||bgcolor=#ffff99|10/4  ||bgcolor=#ffff99|10/5  ||bgcolor=#ffff99|10/6  ||bgcolor=#ffff99|11/1  || colspan="2" bgcolor=#ffff99|11/2 ||colspan="2" bgcolor=#ffff99|11/3 ||bgcolor=#ffff99|11/4
|-
!1932
|bgcolor=#ffff99|11/4  || || || || || || || || || || ||
|-
| colspan="13" style="font-size: 8pt; text-align:left" |Issues of ''Ghost Stories'', showing volume/issue number.  The sequence of<br/>editors is not known with enough certainty to be indicated in the table.  Note that<br/>the last issue (volume 11 number 4) was dated December 1931/January 1932<br/>and spans two rows in the table.<ref name="SFFWFM_GS" />
|}
''Ghost Stories'' was published by Bernarr Macfadden, under the imprint Constructive Publishing Co., of Dunellin, New Jersey, until the March 1930 issue, after which it was taken over by Good Story Magazine Co. of New York, which was run by Harold Hersey, who had earlier edited ''[[The Thrill Book]]''.  The editorial director of Constructive Publishing during MacFadden's ownership was [[Fulton Oursler]]; his assistants, Harry A. Keller, W. Adolphe Roberts, George Bond, Daniel Wheeler, and Arthur B. Howland, each (in that order) spent close to a year editing, though the dates of transition between them are not known.  When Hersey took over, his assistant was Stuart Palmer.<ref name=":0">Ashley (1997), p. 406.</ref>

The magazine began as a slick, in [[bedsheet]] format and switched to pulp layout with the July 1928 issue; it remained as a pulp until the end of its run with the exception of eight issues in large pulp format from April to December 1929.  There were 64 issues, with six issues per volume, except for the last volume which included only four issues.<ref name=":0" />  The price was 25 cents throughout; it had 128 pages when pulp-sized, and 96 pages when a bedsheet and when it was a large pulp.<ref name="SFFWFM_GS" />

No anthologies have selected their contents solely from ''Ghost Stories'', but two magazines have done so: ''True Twilight Tales'' and ''Prize Ghost Stories'', both published by League Publications, a subsidiary of the company that owned the rights to the original stories, [[Macfadden Communications Group#Macfadden/Bartell|MacFadden-Bartell]].  ''Prize Ghost Stories'' published one issue, dated 1963, and ''True Twilight Tales'' published two, dated Fall 1963 and Spring 1964: both magazines were in large pulp format, with 96 pages, priced at 50 cents.  The first issue of ''True Twilight Tales'' was edited by Helen Gardiner, who probably also was the editor of ''Prize Ghost Stories''; the second issue of ''True Twilight Tales'' was edited by John M. Williams.  There may have been other issues of both titles, as neither was numbered.<ref>Ashley (1985b), pp. 482–483.</ref><ref>Ashley (1985c), pp. 678–679.</ref>

==Notes==
{{reflist|group=note}}

==Footnotes==
{{reflist|30em}}

==References==
*{{cite book | title=Science Fiction, Fantasy and Weird Fiction Magazines| last=Ashley | first=Mike |publisher=Greenwood Press | year=1985a | isbn= 0-313-21221-X|editor-last = Tymn|editor-first = Marshall B.| location=Westport, Connecticut|pages = 315–317|chapter = Ghost Stories|quote=|author-link=Mike Ashley (writer)|via=|editor2-last = Ashley|editor2-first = Mike}}
*{{cite book | first=Mike | last=Ashley | title=Science Fiction, Fantasy and Weird Fiction Magazines|publisher=Greenwood Press | location=Westport, Connecticut| year=1985b | isbn= 0-313-21221-X|chapter = Prize Ghost Stories|editor-last = Tymn|editor-first = Marshall B.|editor2-last = Ashley|editor2-first = Mike|pages = 482–483}}
*{{cite book | first=Mike | last=Ashley | title=Science Fiction, Fantasy and Weird Fiction Magazines|publisher=Greenwood Press | location=Westport, Connecticut| year=1985c | isbn= 0-313-21221-X|chapter = True Twilight Tales|editor-last = Tymn|editor-first = Marshall B.|editor2-last = Ashley|editor2-first = Mike|pages = 678–679}}
*{{cite book|last=Ashley|first= Mike| title= The Encyclopedia of Fantasy| year=1997| publisher= St. Martin's Press, Inc.| location= New York| isbn= 0-312-15897-1|editor-last = Clute|editor-first = John|editor2-last = Grant|editor2-first = John|chapter = Ghost Stories|page = 406}}
*{{Cite book|title = Pulpwood Editor|last = Hersey|first = Harold Brainerd|publisher = Frederick A. Stokes Company|year = 1937|isbn=|location = New York|pages=|oclc=2770489|quote=|author-link=Harold Hersey|via=}}
* {{Cite book|title = Science Fiction, Fantasy, and Weird Fiction Magazines|last = Weinberg|first = Robert|publisher = Greenwood Press|year = 1985|isbn = 0-313-21221-X|editor-last = Tymn|editor-first = Marshall B.|location = Westport, Connecticut |pages = 626–628|chapter = Strange Tales of Mystery and Terror|quote=|author-link=Robert Weinberg (author)|via=|editor2-last = Ashley|editor2-first = Mike}}

{{commons category|Ghost Stories (magazine)}}
{{ScienceFictionPulpMagazines}}


[[Category:Fantasy fiction magazines]]
[[Category:Pulp magazines]]
[[Category:Magazines established in 1926]]
[[Category:Magazines disestablished in 1932]]
[[Category:Magazines published in New Jersey]]
[[Category:Magazines published in New York]]
[[Category:Horror fiction magazines]]
[[Category:Defunct magazines of the United States]]