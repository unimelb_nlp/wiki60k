{{about||the driving horse|Old Bob|other uses}}
{{unreferenced|date=September 2014}}
{{Use British English|date=September 2014}}
{{infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = Owd Bob
| title_orig    = 
| translator    = 
| image         = Owd Bob title page 1898.jpg
| caption = Title page of the first UK edition (1898)
| author        = [[Alfred Ollivant (writer)|Alfred Ollivant]]
| illustrator   = 
| cover_artist  = 
| country       = United Kingdom
| language      = English
| series        = 
| genre         = 
| publisher     = 
| release_date  = 1898
| english_release_date =
| media_type    = 
| pages         = 
| isbn          = 
| preceded_by   = 
| followed_by   = 
}}
'''''Owd Bob: The Grey Dog of Kenmuir''''', also titled '''''Bob, Son of Battle''''' for US editions, is a children's book by English author [[Alfred Ollivant (writer)|Alfred Ollivant]]. It was published in 1898 and became popular in the United Kingdom and the United States, though most of the dialogue in the book was written in the [[Cumbrian]] dialect. The name "Owd Bob" is a rendering of the phrase "Old Bob" in a dialect style.

==Plot==
The story emphasizes the rivalry between two [[sheepdog]]s and their masters, and chronicles the maturing of a boy, David, who is caught between them.  His mother dies, and he is left to the care of his father, Adam M'Adam, a sarcastic, angry [[alcoholic]] with few redeeming qualities.   M'Adam is the owner of Red Wull, a huge, violent dog who herds his sheep by brute force.  The other dog is Bob, son of Battle.  He herds sheep by finesse and persuasion.  His master is James Moore, Master of Kenmuir, who acts as surrogate father to David.  David and Moore's daughter Maggie become romantically intrigued by each other.  The dogs compete for the Shepherd's Trophy, the prize in an annual sheep-herding contest which is the highlight of the year in the North Country.  A dog who wins three competitions in a row wins the Shepherd's Cup outright, which has never yet happened.  Complications arise—a rogue dog is killing sheep, and both Bob and Red Wull are suspected of being the culprit. The story chronicles David's boyhood and early manhood, his struggle to live with his father, his frequent escapes to Kenmuir, and his intermittent friendship with Maggie Moore.

==In other media==

===Films===
* ''[[Owd Bob (1924 film)|Owd Bob]]'' (1924), UK
* ''[[Owd Bob (1938 film)|Owd Bob]]'' (1938), UK
* ''[[Thunder in the Valley (film)|Thunder in the Valley]]'' (1947), US
* ''[[Owd Bob (1998 film)|Owd Bob]]'' (1998), UK–Canada

===Comics===
* ''Bob, Son of Battle'' [[Dell Comics]] one-shot, issue #729 in Dell's catch-all nigh-weekly comic book, [[Four Color Comics]]. Adaptation written by [[Gaylord Du Bois]]. Art by [[Mike Sekowsky]].

== External links ==
Public domain copies at [[Internet Archive]]
* [https://archive.org/details/owdbobgreydogofk00olli ''Owd Bob''] (UK edition)
* [https://archive.org/details/bobsonbattle03olligoog ''Bob, Son of Battle''] (US edition)
{{Portal |Children's literature}}

[[Category:1898 novels]]
[[Category:British children's novels]]
[[Category:19th-century British children's literature]]
[[Category:Novels set in Cumbria]]
[[Category:Dogs in literature]]
[[Category:Children's novels about animals]]
[[Category:19th-century British novels]]
[[Category:British novels adapted into films]]

{{1890s-child-novel-stub}}