{{Infobox motorsport venue
|Name = Auto Club Speedway
|Time = [[Pacific Time Zone|UTC−8]] / −7 ([[Pacific Daylight Time|DST]])
|Location = 9300 Cherry Avenue<br />[[Fontana, California]] 92335
|Image = [[File:Auto Club Speedway logo.jpg|250px]]<br>[[Image:ACSinfield.jpg|250px]]
|Image_caption =  The infield at the speedway.
|Capacity = 122,000 (total)<ref>http://www.worldofstadiums.com/north-america/united-states/california/auto-club-speedway/</ref><ref>{{cite web|url=http://www.jayski.com/news/tracks/story/_/page/auto-club-speedway|title=Auto Club Speedway Track News, Records & Links|website=jayski.com|publisher=jayski.com|access-date=March 5, 2016}}</ref>
|Owner = [[International Speedway Corporation]]
|Operator = International Speedway Corporation
|Broke_ground = 1995
|Opened = 1997
|Construction_cost = [[United States dollar|US$]]100&nbsp;million
|Architect = Paxton Waters Architecture<br>Penske Motorsports, Inc.
|Former_names = California Speedway (1997–2007)
|Events =
*'''[[Monster Energy NASCAR Cup Series]]'''
**[[Auto Club 400]]
*'''NASCAR [[Xfinity Series]]'''
**[[Service King 300]]
*'''[[AMA Superbike]]'''
**Suzuki Superbike Challenge
|Miles_first = True
|Layout1 = D-shaped oval
|Surface = [[Asphalt]]
|Length_km = 3.22
|Length_mi = 2.0
|Turns = 4
|Banking = Turns: 14°<br>Frontstretch: 11°<br>Backstretch: 3°
|Record_time = 241.428 miles per hour
|Record_driver = [[Gil de Ferran]]
|Record_team = [[Penske Racing]]
|Record_year = October 28, 2000
|Record_class = [[Champ Car|CART]]
|Layout2 = Interior Test Circuit
|Surface2 = Asphalt
|Length_km2 = 2.3
|Length_mi2 = 1.45
|Turns2 = 13
|Banking2 =
|Record_time2 =
|Record_driver2 =
|Record_team2 =
|Record_year2 =
|Record_class2 =
|Layout3 = Sports Car Course
|Surface3 = Asphalt
|Length_km3 = 4.5
|Length_mi3 = 2.8
|Turns3 = 21
|Banking3 =
|Record_time3 =
|Record_driver3 =
|Record_team3 =
|Record_year3 =
|Record_class3 =
|Layout4 = Motorcycle Course
|Surface4 = Asphalt
|Length_km4 = 3.79
|Length_mi4 = 2.36
|Turns4 = 21
|Banking4 =
|Record_time4 =
|Record_driver4 =
|Record_team4 =
|Record_year4 =
|Record_class4 =
|Layout5 = Drag strip
|Surface5 = Asphalt
|Length_km5 = 0.40
|Length_mi5 = 1/4
|Turns5 =
|Banking5 =
|Record_time5 =
|Record_driver5 =
|Record_team5 =
|Record_year5 =
|Record_class5 =
}}

'''Auto Club Speedway''', formerly '''California Speedway''',<ref name="namechange">{{cite web|title=California Speedway to change name UPDATE|url=http://www.jayski.com/pages/tracks/past/california-past.htm|publisher=jayski.com|accessdate=September 13, 2010}}</ref> is a two-mile (3&nbsp;km), low-banked, D-shaped oval [[superspeedway]] in [[Fontana, California]] which has hosted [[NASCAR]] racing annually since 1997. It is also used for [[open wheel racing]] events. The racetrack is located near the former locations of [[Ontario Motor Speedway]] and [[Riverside International Raceway]]. The track is owned and operated by [[International Speedway Corporation]] and is the only track owned by ISC to have naming rights sold. The speedway is served by the nearby [[Interstate 10]] and [[Interstate 15]] freeways as well as a [[Metrolink (Southern California)|Metrolink]] station located behind the backstretch.

Construction of the track, on the site of the former Kaiser Steel Mill, began in 1995 and was completed in late 1996. The speedway has a grandstand capacity of 68,000 and 28 skyboxes and a total capacity of 122,000. In 2006, a fanzone was added behind the main grandstand. Lights were added to the speedway in 2004 with the addition of a second annual NASCAR weekend. Since 2011, the track has hosted only one NASCAR weekend.

[[IndyCar]] returned to the track in 2012 with its season finale race (a 500-mile night race); the series previously ran a 400-mile race from 2002 to 2005.

== Track history ==

=== Early history and construction ===
[[Image:ACSpits.jpg|250px|thumb|left|Main Grandstand From Pit Road at Auto Club Speedway]]
On April 20, 1994, [[Roger Penske]] and [[Kaiser Steel|Kaiser]] announced the construction of a racetrack on the site of the abandoned [[Kaiser Steel|Kaiser Steel mill]] in [[Fontana, CA]]. A day after the announcement [[Champ Car|CART]] announced it would hold an annual race at the speedway. Three months later [[NASCAR]] President [[Bill France, Jr.]] agreed to sanction [[Winston Cup Series]] races at the speedway upon completion, marking the first time NASCAR has made a commitment to run a race at a track that had yet to be built.<ref name="Dates">{{Cite news|last=Eisenberg |first=Jeff |title=Looking Back: Key dates in the history of California Speedway |newspaper=The Press Enterprise |year=2007 |url=http://www.pe.com/sports/projects/2007/10years/# |accessdate=September 13, 2010 |deadurl=yes |archiveurl=https://web.archive.org/web/20090212120327/http://www.pe.com/sports/projects/2007/10years/ |archivedate=February 12, 2009 }}</ref> Community meetings were held to discuss issues related to the construction of the track and the local effects of events held. The local community largely supported construction of the speedway citing potentially increased land values and rejuvenation of the community. In April 1995, after having toured the sister track Michigan International Speedway, the [[San Bernardino County]] Board of Supervisors unanimously approved the project.<ref name="latimes">{{Cite news | last = Glick| first = Shav| title = New Track Is a Steel California Speedway Will Be Built on Site of Old Fontana Mill | newspaper = [[Los Angeles Times]]| url = http://articles.latimes.com/1995-11-27/sports/sp-7600_1_california-speedway| accessdate=November 13, 2010| date=November 27, 1995}}</ref> The [[California Environmental Protection Agency]] gave Penske permission to begin construction after Kaiser agreed to pay $6 million to remove hazardous waste from the site. Construction on the site began on November 22, 1995 with the demolition of the Kaiser Steel Mill.<ref name="Dates" /> The 100-foot water tower, a landmark of the Kaiser property, was preserved in the center of the track to be used as a scoreboard. {{convert|3000|cuyd}} of contaminated dirt was removed and transported to a toxic waste landfill. To prevent remaining impurities from rising to the surface, a cap of non-porous [[polyethylene]] was put down and covered with {{convert|2|ft}} of clean soil.<ref name="latimes" /> Construction of the track was completed in late 1996.<ref name="Dates" />

On January 10, 1997 Marlboro Team Penske’s driver [[Paul Tracy]] became the first driver to test on the new speedway. NASCAR held its first open test session on at the track from May 5–7. The official opening and ribbon cutting ceremony was held on June 20, 1997 with the first race, a [[NASCAR West Series]] race, being held the next day.<ref name="Dates" />

== California Speedway ==
The track was named the California Speedway from the time it was built through February 21, 2008 when the Southern California Automobile Club (Auto Club) purchased the naming rights in a 10-year deal. Thus creating Auto Club Speedway.

=== Expansion and additions ===
With early success following the opening of the track, the speedway began to expand reserved grandstand seating along the front stretch with an additional 15,777 seats. In May 1999, an additional 28 skyboxes were added to the top of the main grandstand. In 2001 the Auto Club Dragway, a 1/4 mile dragstrip, was built outside of the backstretch of the main speedway. That same year, the infield of the speedway was reconfigured to hold a multipurpose road course. On April 24, 2003 The San Bernardino County Planning Commission approved the changing of the speedway’s conditional use permit to allow the installation of lights around the track. Later that year NASCAR announced a second annual NASCAR Cup Series race at the track for the 2004 season, with the second race being run "under the lights".<ref>{{cite web|author=Jensen, Tom |date=August 7, 2010 |url=http://www.racingconnection.com/NascarEventPackages/california_speedwayseating.htm |title=California Speedway |publisher=Racingconnection.com |accessdate=September 14, 2010}}</ref> NASCAR ran two weekends of racing annually until the [[2011 in NASCAR|2011 season]], when the track returned to a single annual race weekend.<ref>{{cite web|url=http://nascar.speedtv.com/article/cup-auto-club-loses-chase-date/ |title=NASCAR&nbsp;— CUP: Auto Club Loses Chase Date&nbsp;— SPEED.com |publisher=Nascar.speedtv.com |date=August 7, 2010 |accessdate=September 14, 2010| archiveurl= https://web.archive.org/web/20100818074629/http://nascar.speedtv.com/article/cup-auto-club-loses-chase-date/| archivedate= August 18, 2010 <!--DASHBot-->| deadurl= no}}</ref>

In 2006, the speedway's midway, located behind the main grandstand, was overhauled. The new midway, called ''Discover IE FanZone'', includes the addition of Apex (a [[Wolfgang Puck]] restaurant), additional shade and lounge areas, a new retail store and an entertainment stage.<ref>{{cite web|title=2006 Racing Season Concludes, 2007 Just Around the Corner |url=http://www.autoclubspeedway.com/Articles/2006/10/2006-Racing-Season-Concludes-2007-Just-Around-The-Corner.aspx|publisher=Autoclubspeedway.com|date=October 5, 2006|accessdate=September 13, 2010}}</ref>

In March 2014, [[Las Vegas]] based company [[Exotics Racing]] expanded to California by opening a new 1.2 mile road course at the Auto Club Speedway.

=== Configurations ===
<gallery>
Image:Auto Club Speedway (formerly California Speedway) - Speedway.svg|Speedway Oval
Image:Auto Club Speedway (formerly California Speedway) - Sports Car.svg|Sports Car course
Image:Auto Club Speedway (formerly California Speedway) - Motorcycles.svg|Motorcycle course
Image:Auto Club Speedway (formerly California Speedway) - Interior Circuit.svg|Interior Test Circuit
Image:Auto_club_speedway_LA_january_2014.jpg|Overview of track looking north
</gallery>

=== Attendance problems ===
Upon the addition of a second NASCAR weekend at the track in 2004, attendance at the races dropped off dramatically, by as much as 20,000. With such a large attendance swing, drivers and media began to doubt if the track deserved two dates, even if the track was near Los Angeles, the nation's second-largest media market.<ref>{{cite web|author=Gluck, Jeff |url=http://www.scenedaily.com/news/articles/sprintcupseries/Jeff_Gluck_fontana.html |title=Lack of attendance remains No. 1 concern at Auto Club Speedway |publisher=SceneDaily.com |date=February 21, 2009 |accessdate=September 14, 2010}}</ref> Weather also became a concern with either extremely hot days or with rain threatening the races. All of this factored into NASCAR's decision to remove a second race from the track with the realignment of the 2011 NASCAR schedule. Former track owner [[Roger Penske]] said the track may be located in a one-race market. Track president Gillian Zucker cited bad weather windows and fans having other entertainment options as reasons for the attendance decline.<ref>{{cite web|author=Gluck, Jeff|url=http://www.scenedaily.com/news/articles/sprintcupseries/Weather_scheduling_blamed_for_attendance_woes_loss_of_Cup_races_at_Atlanta_California.html|title=Weather, scheduling blamed for attendance woes, loss of Cup races at Atlanta, California|publisher=SceneDaily.com|date=August 8, 2010|accessdate=September 14, 2010}}</ref>

Effective in the 2014 racing season, the grandstand capacity was reduced from 92,000 to 68,000. This was accomplished by removing approximately 12,000 seats near Turn 1 and installing a hospitality area and a digital display showing speeds along the straightaway.<ref>{{cite news|last=Peltz|first=Jim|title=Auto Club Speedway slashes grandstand seating by 26% to 68,000|url=http://www.latimes.com/sports/sportsnow/la-sp-sn-nascar-fontana-seating-20140321-story.html|accessdate=March 21, 2014|work=[[Los Angeles Times]]|publisher=[[Tribune Publishing]]|date=March 21, 2014}}</ref> In addition, seats were further reduced as a result of modifying average seat width from 18 inches to 23 inches. The capacity quoted does not include luxury boxes and infield seating, which when added up reaches a capacity of approximately 100,000.<ref>[http://www.dailynews.com/sports/20140323/auto-club-speedway-wins-its-race-long-before-kyle-busch-did-in-auto-club-400 Auto Club Speedway wins its race long before Kyle Busch did in Auto Club 400<!-- Bot generated title -->]</ref>

=== Name change ===
On February 21, 2008, the [[Automobile Club of Southern California]] (ACSC) became the title sponsor of the raceway, making Auto Club Speedway the track's official name. The [[naming rights]] deal will last for ten years and is worth an estimated $50 to $75&nbsp;million. In addition to naming rights, the ACSC will have use of the facility for road tests for ''[[Westways]]'' Magazine and other consumer tests. The money will be used for capital improvements to the track.<ref name="namechange" />

=== In pop culture ===
The facility is often used for filming television shows, commercials and films. In 2000, portions of ''[[Charlie's Angels (film)|Charlie's Angels]]'' were filmed at the speedway,<ref>{{cite web|url=http://www.seeing-stars.com/locations/ca1/CharliesAngels3.shtml |title=Charlie's Angels Filming Locations&nbsp;— part 3 |publisher=Seeing-stars.com |accessdate=September 13, 2010}}</ref> and in 2005, portions of ''[[Herbie: Fully Loaded]]'' were filmed there.<ref>{{cite web|title=Trivia for Herbie Fully Loaded |url=http://www.imdb.com/title/tt0400497/trivia|publisher=imdb.com|accessdate=September 13, 2010| archiveurl= https://web.archive.org/web/20100912170322/http://www.imdb.com/title/tt0400497/trivia| archivedate= September 12, 2010 <!--DASHBot-->| deadurl= no}}</ref> In 2007, ''[[The Bucket List]]'' saw [[Jack Nicholson]] and [[Morgan Freeman]] drive a vintage Shelby Mustang and Dodge Challenger around the {{convert|2|mi|km|adj=on}} speedway.<ref>{{cite web|url=http://www.imdb.com/title/tt0825232/locations |title=Filming Locations For The Bucket List |publisher=IMDB.com |accessdate=November 13, 2010}}</ref>

A parody of the track was used in the 2006 [[Pixar]] film ''[[Cars (film)|Cars]]''. It is the venue for the Piston Cup tiebreaker race between the movie's main character [[Lightning McQueen]] (voiced by [[Owen Wilson]]), retiring veteran [[Strip "The King" Weathers]] (voiced by [[Richard Petty]]) and perennial runner-up [[Chick Hicks]] (voiced by [[Michael Keaton]]). The race is held at the Los Angeles International Speedway, which is a conglomeration of the [[Los Angeles Memorial Coliseum]], the [[Arroyo Seco (Los Angeles County)|Arroyo Seco]] in [[Pasadena, California|Pasadena]] where the [[Rose Bowl (stadium)|Rose Bowl]] is located, as well as the Auto Club Speedway.

=== Fatalities ===
During the [[1999 Marlboro 500]] CART race, Canadian driver [[Greg Moore (race car driver)|Greg Moore]] died in a crash along the backstretch of the track. It was determined that after sliding along the infield grass, Moore's car hit the edge of oncoming pavement, which caused the car to flip into a concrete retaining wall. The incident prompted the track owners, ISC, to pave the backstretch of both Auto Club Speedway and its sister track [[Michigan International Speedway]] in an attempt to prevent a similar accident. Shortly after the crash, CART mandated the use of a head-and-neck restraint system on all ovals. The rule eventually became mandatory on all tracks.<ref>{{cite news|title=Moore, 24, killed in horrifying CART crash|url=http://espn.go.com/auto/cart/news/1999/1031/144448.html|accessdate=September 13, 2010|work=ESPN News Services|agency=[[Associated Press]]|publisher=ESPN Internet Ventures|date=November 3, 1999|location=[[Fontana, California]]}}</ref><ref>{{cite web|title=Greg Moore |url=http://www.danspitstopracing.com/greg_moore |accessdate=November 13, 2010 |deadurl=yes |archiveurl=https://web.archive.org/web/20100531214158/http://www.danspitstopracing.com/greg_moore |archivedate=May 31, 2010 }}</ref>

On April 5, 2002, Ricky Lundgren was killed in a qualifying session for a motorcycle race.<ref>{{cite news|last=Henderson|first=Martin|title=Motorcyclist Dies at California Speedway|url=http://articles.latimes.com/2002/apr/06/sports/sp-death06|accessdate=January 19, 2016|work=[[Los Angeles Times]]|publisher=[[Tribune Publishing]]|date=April 6, 2002}}</ref>

On August 7, 2004, a police officer from San Diego, John Barr, died during an open track event after coming off his motorcycle.<ref>[http://www.roadracingworld.com/news/san-diego-policeman-dies-in-crash-during-track-ride-day-at-california-speedway/ San Diego Policeman Dies In Crash During Track Ride Day At California Speedway<!-- Bot generated title -->]</ref>

On June 2, 2005, two men died while participating in an event sponsored by the San Diego Chapter of the Ferrari Owners' Club.<ref>[http://www.utsandiego.com/sports/20050603-1041-ca-speedwaydeaths.html SignOnSanDiego.com > Sports - Two San Diego County men killed in Fontana speedway crash<!-- Bot generated title -->]</ref>

On October 15, 2010, a 24-year-old woman died while participating in a driving school at the track. The woman was driving a replica [[Indycar]] as part of the Mario Andretti Racing Experience when she lost control and hit the inside wall of the track.<ref>{{cite web|title=LA woman killed in crash during racing class at Auto Club Speedway in Fontana |date=October 17, 2010 |url=http://www.swrnn.com/southwest-riverside/2010-10-17/news/la-woman-killed-in-crash-during-racing-class-at-auto-club-speedway-in-fontana|accessdate=November 13, 2010| archiveurl= https://web.archive.org/web/20101023085346/http://www.swrnn.com/southwest-riverside/2010-10-17/news/la-woman-killed-in-crash-during-racing-class-at-auto-club-speedway-in-fontana| archivedate= October 23, 2010 <!--DASHBot-->| deadurl= no}}</ref>

== Racing events ==

=== Current races ===
* [[Monster Energy NASCAR Cup Series]]:
**[[Auto Club 400]]
* [[NASCAR]] [[Xfinity Series]]:
**[[Service King 300]]
* Shell [[Eco-marathon]] Americas

=== Former races ===
* [[Monster Energy NASCAR Cup Series]]
**[[Pepsi Max 400]] (2004–2010)
* [[NASCAR]] [[Xfinity Series]]
**[[CampingWorld.com 300]] (2004–2010)
* [[NASCAR]] [[Camping World Truck Series]]
**[[San Bernardino County 200]] (1997–2009)
* [[NASCAR West Series]] (1997–2006)
* [[Champ Car|CART]]
**Marlboro 500 (1997–2002)
* [[IndyCar Series]]
**[[MAVTV 500]] (2002-2005, 2012-2015)
* [[Rolex Sports Car Series]]
**Grand American 400 (Sports Car Course) (2002–2005)
* [[IROC]] (1997, 1998, 2002)
* [[Super GT]] (2004)

== Other events ==
* Red, White & Cruise&nbsp; — A [[Independence Day (United States)|July 4]] festival consisting of a car show, various family-friendly entertainment and a fireworks show.
* [[Epicenter (music festival)|Epicenter 2010]] at the speedway's midway
* Cardenas Festival —The annual Cardenas Festival is held in the parking lot. This is a festival where all the company's that sell food at the grocery store Cardenas give out free samples of new or upcoming food. There are also performances from many artists.

== Track length of paved oval ==
The track length is disputed by CART and NASCAR that run at Auto Club Speedway. The NASCAR timing and scoring use a length of exactly {{convert|2.000|mi|km}}.<ref>[http://www.nascar.com/en_us/nascar-tracks/auto-club-speedway.html Auto Club Speedway at NASCAR.com]</ref> The IRL timing and scoring use also a length of exactly 2 miles since their first race in 2002.<ref>[http://www.imscdn.com/INDYCAR/Documents/3315/2015-06-27/indycar-race-results.pdf 2015 IndyCar race result at Indycar homepage].</ref> CART measured the track as {{convert|2.029|mi|km}} and used this length for timing and scoring between 1997 and 2002.<ref>[http://www.champcarstats.com/races/200218.htm The 500 Presented by Toyota result on champcarstats.com]</ref>

== Track records ==
The closed-course practice and qualifying one-lap records [[Arie Luyendyk]] had set in the run-up to the [[1996 Indianapolis 500|1996 Indy 500]] at {{convert|239.260|mi/h|km/h|3|abbr=on}} and {{convert|237.498|mi/h|km/h|3|abbr=on}} respectively were improved by [[Maurício Gugelmin]] on September 27, 1997. He was clocked at {{convert|238.869|mi/h|km/h|3|abbr=on}} and {{convert|237.498|mi/h|km/h|3|abbr=on}} respectively.<ref>{{cite news|last=Glick|first=Shav|title=At Marlboro 500, Change Is Almost as Quick as the Cars|url=http://articles.latimes.com/1997/sep/28/sports/sp-37232|newspaper=Los Angeles Times|date=September 28, 1997|archiveurl=https://web.archive.org/web/20131110021404/http://articles.latimes.com/1997/sep/28/sports/sp-37232|archivedate=November 10, 2013}}</ref>

After [[Juan Pablo Montoya]] had missed Gugelmin's unofficial record in practice,<ref>{{cite news|title=Montoya threatens all-time record in practice|url=http://www.autosport.com/news/report.php/id/11903|publisher=[[Autosport#Autosport.com|Autosport.com]]|date=October 28, 2000|archiveurl=https://web.archive.org/web/20131110000536/http://www.autosport.com/news/report.php/id/11903|archivedate=November 10, 2013}}</ref> [[Gil de Ferran]] set a new official one-lap record at {{convert|237.977|mi/h|km/h|3|abbr=on}} during CART qualifying on October 28, 2000.<ref>{{cite news|title=De Ferran wins pole, sets record|url=http://www.lasvegassun.com/news/2000/oct/28/de-ferran-wins-pole-sets-record|newspaper=[[Las Vegas Sun]]|date=October 28, 2000|archiveurl=https://web.archive.org/web/20131110003755/http://www.lasvegassun.com/news/2000/oct/28/de-ferran-wins-pole-sets-record|archivedate=November 10, 2013}}</ref> There is a discrepancy in average speed recognised because CART did use a slightly larger track length. Under CART measurements, the speed was listed as {{convert|241.428|mi/h|km/h}}. As of March 2012, this is the fastest lap speed ever recorded at an official race meeting and the fastest ever lap on a closed racing circuit.<ref>{{cite web|last=Webster|first=George|url=http://www.prnmag.com/columns/44-columns/66-who-holds-the-worlds-closed-course-record-aj-foyt.html|title=PRN&nbsp;— Performance Racing News&nbsp;— Who holds the world’s closed course record? A.J. Foyt &#124; PRN&nbsp;— Performance Racing News|publisher=Prnmag.com|accessdate=February 22, 2012}}</ref> The 2003 Indycar race was the fastest circuit race ever in motorsport history, with an average speed of {{convert|207.151|mph|km/h|3|abbr=on}} over {{convert|400|mi|km}}, topping the previous record average of {{convert|195.165|mph|km/h|3|abbr=on}} over {{convert|500|mi|km}}, which was set by the final CART race held in Fontana the preceding year (again, the time was adjusted to reflect the discrepancy between the CART's measured distance and the recognised distance).<ref>{{cite news|title=Fastest race goes to Hornish|url=http://articles.chicagotribune.com/2003-09-22/sports/0309220140_1_hornish-toyota-indy-irl|newspaper=[[Chicago Tribune]]|date=September 22, 2003|archiveurl=https://web.archive.org/web/20131109235840/http://articles.chicagotribune.com/2003-09-22/sports/0309220140_1_hornish-toyota-indy-irl|archivedate=November 9, 2013}}</ref>

{| class="wikitable"
|-
!'''Record'''!!'''Year'''!!'''Date'''!!'''Driver'''!!'''Time'''!!'''Speed/Avg. Speed'''
|-
!colspan=7|'''Monster Energy NASCAR Cup Series'''
|-
| Qualifying (one lap) ||2016||March 18||[[Denny Hamlin]] ||align=right|38.194||{{convert|188.511|mi/h|km/h|3|abbr=on}}
|-
| Race (500 miles) ||1997||June 27|| [[Jeff Gordon]] ||align=right|3:13:32||{{convert|155.012|mi/h|km/h|3|abbr=on}}
|-
| Race (400 miles) ||2012||March 25|| [[Tony Stewart]] ||align=right|2:39:06||{{convert|160.166|mi/h|km/h|3|abbr=on}}
|-
!colspan=7|'''NASCAR Xfinity Series'''
|-
| Qualifying (one lap) ||2005|| September 3 ||[[Tony Stewart]] ||align=right|38.722||{{convert|185.941|mi/h|km/h|3|abbr=on}}
|-
| Race (300 miles) ||2001||April 28||[[Hank Parker, Jr.]] ||align=right|1:55:25||{{convert|155.957|mi/h|km/h|3|abbr=on}}
|-
!colspan=7|'''NASCAR Camping World Truck Series'''
|-
| Qualifying (one lap) ||2006|| February 24 ||[[David Reutimann]] ||align=right|40.228||{{convert|178.980|mi/h|km/h|3|abbr=on}}
|-
| Race (200 miles) ||2003|| September 20 ||[[Ted Musgrave]] ||align=right|1:22:14||{{convert|145.926|mi/h|km/h|3|abbr=on}}
|-
!colspan=7|'''NASCAR West Series'''
|-
| Qualifying (one lap) ||2001|| April 28 ||[[Mark Reed (driver)|Mark Reed]] ||align=right|39.649||{{convert|181.593|mi/h|km/h|3|abbr=on}}
|-
| Race (200 miles) ||2001|| April 28 ||[[Brendan Gaughan]] ||align=right|1:28:47||{{convert|152.316|mi/h|km/h|3|abbr=on}}
|-
!colspan=7|'''CART'''
|-
| Qualifying (one lap - 2.000&nbsp;mi)||2000|| October 28 ||[[Gil de Ferran]] ||align=right| 30.255|| |{{convert|241.428|mi/h|km/h|3|abbr=on}}
|-
| Race (500 miles) ||2002|| November 3 ||[[Jimmy Vasser]] ||align=right| 2:33:42 ||{{convert|195.165|mi/h|km/h|3|abbr=on}}
|-
!colspan=7|'''INDYCAR'''
|-
| Qualifying (one lap) ||2003|| September 20 ||[[Helio Castroneves]] ||align=right| 31.752 ||{{convert|226.757|mi/h|km/h|3|abbr=on}}
|-
| Race (400 miles) ||2003|| September 21 ||[[Sam Hornish, Jr.]] ||align=right| 1:55:51 ||{{convert|207.151|mi/h|km/h|3|abbr=on}}
|-
| Race (500 miles) ||2014|| August 30 ||[[Tony Kanaan]] ||align=right| 2:32:58.4659 ||{{convert|196.111|mi/h|km/h|3|abbr=on}}
|-
|colspan=7|<center><small>''Source:<ref>{{cite web|title=Race Results at Auto Club Speedway|url=http://www.racing-reference.info/tracks/Auto_Club_Speedway|publisher=racing-reference.info|accessdate=September 13, 2010}}</ref></small></center>
|}

== Monster Energy NASCAR Cup Series stats ==
<small>(As of 3/20/16)</small>
{| class="wikitable" style="font-size:95%;"
|-
| Most Wins || 6 || [[Jimmie Johnson]]</tr>
| Most Top 5s || 13 || [[Jimmie Johnson]]</tr>
| Most Top 10s || 15 || [[Matt Kenseth]]</tr>
| Starts || 25 || [[Jeff Gordon]]</tr>
| Poles || 4 || [[Kurt Busch]]</tr>
| Most Laps Completed || 5892 || [[Jeff Gordon]]</tr>
| Most Laps Led || 980 || [[Jimmie Johnson]]</tr>
| Avg. Start* || 9.2 || [[Jimmie Johnson]]</tr>
| Avg. Finish* || 6.6 || [[Jimmie Johnson]]</tr>
|}
<nowiki>*</nowiki> <small>from minimum 5 starts.</small>

=== Monster Energy NASCAR Cup Series race winners ===
{| class="wikitable" style="font-size:85%;"
|-
! Season
! Date
! Official Race Name
! Winning Driver
! Car #
! Make
! Distance
! Avg Speed
! Margin of Victory
|-
| 1997
| June 22
| '''California 500'''
| [[Jeff Gordon]]
| 24
| [[Chevrolet Monte Carlo]]
| {{convert|500|mi|km|abbr=on}}
| {{convert|155.012|mi/h|km/h|3|abbr=on}}
| 1.074 sec
|-
| 1998
| May 3
| '''California 500 presented by NAPA'''
| [[Mark Martin (racecar driver)|Mark Martin]]
| 6
| [[Ford Taurus]]
| {{convert|500|mi|km|abbr=on}}
| {{convert|140.22|mi/h|km/h|3|abbr=on}}
| 1.287 sec
|-
| 1999
| May 2
| '''California 500 presented by NAPA'''
| [[Jeff Gordon]]
| 24
| [[Chevrolet Monte Carlo]]
| {{convert|500|mi|km|abbr=on}}
| {{convert|150.276|mi/h|km/h|3|abbr=on}}
| 4.492 sec
|-
| 2000
| April 30
| '''NAPA Auto Parts 500'''
| [[Jeremy Mayfield]]
| 12
| [[Ford Taurus]]
| {{convert|500|mi|km|abbr=on}}
| {{convert|149.378|mi/h|km/h|3|abbr=on}}
| 0.300 sec
|-
| 2001
| April 29
| '''NAPA Auto Parts 500'''
| [[Rusty Wallace]]
| 2
| [[Ford Taurus]]
| {{convert|500|mi|km|abbr=on}}
| {{convert|143.118|mi/h|km/h|3|abbr=on}}
| 0.27 sec
|-
| 2002
| April 28
| '''NAPA Auto Parts 500'''
| [[Jimmie Johnson]]
| 48
| [[Chevrolet Monte Carlo]]
| {{convert|500|mi|km|abbr=on}}
| {{convert|150.088|mi/h|km/h|3|abbr=on}}
| 0.620 sec
|-
| 2003
| April 27
| '''Auto Club 500'''
| [[Kurt Busch]]
| 97
| [[Ford Taurus]]
| {{convert|500|mi|km|abbr=on}}
| {{convert|140.111|mi/h|km/h|3|abbr=on}}
| 2.294 sec
|-
| rowspan="2"|2004
| May 2
| '''Auto Club 500'''
| [[Jeff Gordon]]
| 24
| [[Chevrolet Monte Carlo]]
| {{convert|500|mi|km|abbr=on}}
| {{convert|137.268|mi/h|km/h|3|abbr=on}}
| 12.871 sec
|-
| September 5
| '''Pop Secret 500'''
| [[Elliott Sadler]]
| 38
| [[Ford Taurus]]
| {{convert|500|mi|km|abbr=on}}
| {{convert|128.324|mi/h|km/h|3|abbr=on}}
| 0.263 sec
|-
| rowspan="2"|2005
| February 27
| '''Auto Club 500'''
| [[Greg Biffle]]
| 16
| [[Ford Taurus]]
| {{convert|500|mi|km|abbr=on}}
| {{convert|139.697|mi/h|km/h|3|abbr=on}}
| 0.231 sec
|-
| September 4
| '''Sony HD 500'''
| [[Kyle Busch]]
| 5
| [[Chevrolet Monte Carlo]]
| {{convert|508|mi|km|abbr=on}} *
| {{convert|136.356|mi/h|km/h|3|abbr=on}}
| 0.554 sec
|-
| rowspan="2"|2006
| February 26
| '''Auto Club 500'''
| [[Matt Kenseth]]
| 17
| [[Ford Fusion (Americas)|Ford Fusion]]
| {{convert|502|mi|km|abbr=on}} *
| {{convert|147.852|mi/h|km/h|3|abbr=on}}
| 0.338 sec
|-
| September 3
| '''Sony HD 500'''
| [[Kasey Kahne]]
| 9
| [[Dodge Charger]]
| {{convert|500|mi|km|abbr=on}}
| {{convert|144.462|mi/h|km/h|3|abbr=on}}
| 3.427 sec
|-
| rowspan="2"|2007
| February 25
| '''Auto Club 500'''
| [[Matt Kenseth]]
| 17
| [[Ford Fusion (Americas)|Ford Fusion]]
| {{convert|500|mi|km|abbr=on}}
| {{convert|138.451|mi/h|km/h|3|abbr=on}}
| 0.679 sec
|-
| September 2
| '''Sharp AQUOS 500'''
| [[Jimmie Johnson]]
| 48
| [[Chevrolet Monte Carlo SS]]
| {{convert|500|mi|km|abbr=on}}
| {{convert|131.502|mi/h|km/h|3|abbr=on}}
| 1.868 sec
|-
| rowspan="2"|2008
| February 25
| '''Auto Club 500'''
| [[Carl Edwards]]
| 99
| [[Ford Fusion (Americas)|Ford Fusion]]
| {{convert|500|mi|km|abbr=on}}
| {{convert|132.704|mi/h|km/h|3|abbr=on}}
| UC
|-
| August 31
| '''Pepsi 500'''
| [[Jimmie Johnson]]
| 48
| [[Chevrolet Impala]]
| {{convert|500|mi|km|abbr=on}}
| {{convert|138.857|mi/h|km/h|3|abbr=on}}
| 2.076 sec
|-
| rowspan="2"|2009
| February 22
| '''Auto Club 500'''
| [[Matt Kenseth]]
| 17
| [[Ford Fusion (Americas)|Ford Fusion]]
| {{convert|500|mi|km|abbr=on}}
| {{convert|135.839|mi/h|km/h|3|abbr=on}}
| 1.463 sec
|-
| October 11
| '''Pepsi 500'''
| [[Jimmie Johnson]]
| 48
| [[Chevrolet Impala]]
| {{convert|500|mi|km|abbr=on}}
| {{convert|143.908|mi/h|km/h|3|abbr=on}}
| 1.603 sec
|-
| rowspan="2"|2010
| February 21
| [[2010 Auto Club 500|'''Auto Club 500''']]
| [[Jimmie Johnson]]
| 48
| [[Chevrolet Impala]]
| {{convert|500|mi|km|abbr=on}}
| {{convert|141.911|mi/h|km/h|3|abbr=on}}
| 1.523
|-
| October 10
| [[2010 Pepsi Max 400|'''Pepsi Max 400''']]
| [[Tony Stewart]]
| 14
| [[Chevrolet Impala]]
| {{convert|400|mi|km|abbr=on}}
| {{convert|131.953|mi/h|km/h|3|abbr=on}}
| 0.466 sec
|-
| 2011
| March 27
| [[2011 Auto Club 400|'''Auto Club 400''']]
| [[Kevin Harvick]]
| 29
| [[Chevrolet Impala]]
| {{convert|400|mi|km|abbr=on}}
| {{convert|150.849|mi/h|km/h|3|abbr=on}}
| 0.144 sec
|-
| 2012
| March 25
| [[2012 Auto Club 400|'''Auto Club 400''']]
| [[Tony Stewart]]
| 14
| [[Chevrolet Impala]]
| {{convert|258|mi|km|abbr=on}} **
| {{convert|160.166|mi/h|km/h|3|abbr=on}}
| UC
|-
| 2013
| March 24
| [[2013 Auto Club 400|'''Auto Club 400''']]
| [[Kyle Busch]]
| 18
| [[Toyota Camry]]
| {{convert|400|mi|km|abbr=on}}
| {{convert|135.351|mi/h|km/h|3|abbr=on}}
| UC
|-
| 2014
| March 23
| [[2014 Auto Club 400|'''Auto Club 400''']]
| [[Kyle Busch]]
| 18
| [[Toyota Camry]]
| {{convert|412|mi|km|abbr=on}} *
| {{convert|132.987|mi/h|km/h|3|abbr=on}}
| 0.214 sec
|-
| 2015
| March 22
| [[2015 Auto Club 400|'''Auto Club 400''']]
| [[Brad Keselowski]]
| 2
| [[Ford Fusion (Americas)|Ford Fusion]]
| {{convert|418|mi|km|abbr=on}} *
| {{convert|140.662|mi/h|km/h|3|abbr=on}}
| 0.710 sec
|-
| 2016
| March 20
| [[2016 Auto Club 400|'''Auto Club 400''']]
| [[Jimmie Johnson]]
| 48
| [[Holden Commodore (VF)#Chevrolet SS|Chevrolet SS]]
| {{convert|410|mi|km|abbr=on}} *
| {{convert|137.213|mi/h|km/h|3|abbr=on}}
| 0.772 sec
|}
<nowiki>*</nowiki>&nbsp;– Race extended due to [[green-white-checker finish]].
<nowiki>**</nowiki>&nbsp;– Race shortened due to rain.

== Open wheel race winners ==
{| class="wikitable" style="font-size: 95%;"
|-
! Season
! Date
! Race name
! Winning driver
! Winning team
|-
!colspan=7|CART
|-
| 1997
| September 28
| Marlboro 500
| {{Flagicon|UK}} [[Mark Blundell]]
| [[PacWest Racing|PacWest]]
|-
| 1998
| November 1
| Marlboro 500 Presented by Toyota
| {{Flagicon|USA}} [[Jimmy Vasser]]
| [[Chip Ganassi Racing]]
|-
| 1999
| October 31
| Marlboro 500 Presented by Toyota
| {{Flagicon|Mexico}} [[Adrián Fernández]]
| [[Patrick Racing]]
|-
| 2000
| October 30
| Marlboro 500
| {{Flagicon|Brazil}} [[Christian Fittipaldi]]
| [[Newman-Haas Racing]]
|-
| 2001
| November 14
| The 500 by Toyota
| {{Flagicon|Brazil}} [[Cristiano da Matta]]
| [[Newman-Haas Racing]]
|-
| 2002
| November 3
| The 500
| {{Flagicon|USA}} [[Jimmy Vasser]]
| [[Team Rahal]]
|-
| 2003
| November 9
| King Taco 500
| colspan=2| Canceled due to [[Old Fire (2003)|wildfires in the San Bernardino mountains]]<ref>{{cite news|last=Humason|first=John|title=Champ Car season ends early due to California fires|url=http://www.motorsport.com/news/article.asp?ID=141310&FS=INDYCAR|accessdate=June 20, 2010|newspaper=Motorsport.com|date=October 29, 2003}}</ref>
|-
!colspan=7|IndyCar Series
|-
| 2002
| March 24
| Yamaha Indy 400
| {{Flagicon|USA}} [[Sam Hornish, Jr.]]
| [[Panther Racing]]
|-
| 2003
| September 21
| Toyota Indy 400
| {{Flagicon|USA}} [[Sam Hornish, Jr.]]
| [[Panther Racing]]
|-
| 2004
| October 3
| Toyota Indy 400
| {{Flagicon|Mexico}} [[Adrian Fernández]]
| [[Aguri-Fernández Racing]]
|-
| 2005
| October 16
| Toyota Indy 400
| {{Flagicon|UK}} [[Dario Franchitti]]
| [[Andretti Green Racing]]
|-
| 2012
| September 15
| MAVTV 500
| {{Flagicon|USA}} [[Ed Carpenter (racing driver)|Ed Carpenter]]
| [[Ed Carpenter Racing]]
|-
| 2013
| October 19
| MAVTV 500
| {{Flagicon|AUS}} [[Will Power]]
| [[Team Penske]]
|-
| 2014
| August 30
| MAVTV 500
| {{Flagicon|Brazil}} [[Tony Kanaan]]
| [[Chip Ganassi Racing]]
|-
| 2015
| June 27
| MAVTV 500
| {{Flagicon|USA}} [[Graham Rahal]]
| [[Rahal Letterman Lanigan Racing]]
|}

== References ==
{{Reflist|colwidth=30em}}

== External links ==
{{commons category|Auto Club Speedway|<br/>Auto Club Speedway}}
* [http://www.autoclubspeedway.com/  Official  '''Auto Club Speedway''' website]
* {{Racing-Reference track|Auto_Club_Speedway}}
* [http://www.racingcircuits.info/north-america/usa/auto-club-speedway,-fontana.html RacingCircuits.info: Map and circuit history]
* [http://www.nascar.com/races/tracks/cal/index.html Auto Club Speedway Page] on [http://www.nascar.com/ NASCAR.com]
* [http://jayski.com/pages/tracks/california.htm Jayski's Auto Club Speedway Page] — ''current and past California Speedway News''.
* [https://web.archive.org/web/20060816083642/http://www.trackpedia.com:80/wiki/California_Speedway Trackpedia guide to driving this track]
* [http://racing.ballparks.com/California/index.htm Auto Club Speedway Page] on [http://www.ballparks.com/ Ballparks by Munsey & Suppes]
{{Coord|34.08858|-117.50000|region:US_type:landmark|display=title}}
{{Navboxes
|list =
{{NASCAR Sprint Cup Series racetracks}}
{{NASCAR Nationwide Series racetracks}}
{{NASCAR Truck Series racetracks}}
{{International Race of Champions tracks}}
{{Grand-Am circuits}}
{{Champ Car tracks}}
{{Indy Racing League}}
{{Indy Lights racetracks}}
{{D1GP circuits}}
}}
{{Use mdy dates|date=March 2012}}
{{good article}}

[[Category:Auto Club Speedway| ]]
[[Category:Motorsport venues in California]]
[[Category:Fontana, California]]
[[Category:Sports venues in San Bernardino County, California]]
[[Category:Sports venues in San Bernardino-Riverside Metro]]
[[Category:Champ Car circuits]]
[[Category:IndyCar Series tracks]]
[[Category:International Race of Champions tracks]]
[[Category:NASCAR tracks]]
[[Category:NHRA Division 7 drag racing venues]]