'''John Dew''' (born 1944) is a [[British people|British]] [[opera director]]. He was the artistic director of the [[Staatstheater Darmstadt]].<ref>{{cite web|title=Staatstheater Darmstadt - John Dew|url=http://www.staatstheater-darmstadt.de/leitungsteam/2253-dew-john|publisher=staatstheater-darmstadt.de|accessdate=March 30, 2013}}</ref>

== Biography ==
Dew was born in 1944 in [[Santiago de Cuba]], but later moved to [[England]] at age three.<ref name=Gomez>{{cite web|last=Gomez|first=Boris|title=The Bad Boy of Bielefeld Puts Fun Into Opera|url=http://www.praguepost.com/archivescontent/11231-the-bad-boy-of-bielefeld-puts-fun-into-opera.html|publisher=The Prague Post|accessdate=March 30, 2013|date=October 20, 1992}}</ref>  He studied at the [[Pratt Institute]] in [[New York City]] where he gained a [[Bachelor of Arts]] degree, after which he was apprenticed to [[Walter Felsenstein]] and [[Wieland Wagner]]. In 1969 to 1976 he worked as assistant producer in [[Osnabrück]] and [[Ulm]], his first production being De Grandes's ''Eduward and Kenegunde'' in Ulm.

His freelance work from 1977 to 1982 took him to [[Kiel, Germany|Kiel]], [[Mannheim, Germany|Mannheim]], [[Hanover, Germany|Hanover]] and [[Basel, Switzerland|Basel]] where he mounted several productions, as well as a Ring cycle and various [[Wolfgang Amadeus Mozart|Mozart]] operas in [[Krefeld, Germany|Krefeld]].

In 1982, he was appointed director of productions and artistic director of the [[Bielefeld Opera|Bielefeld Municipal Opera]] where he remained until 1995. His work there included a cycle of 40 so-called [[Degenerate music|Entartete]] works - rediscovered works which had been banned by the [[Nazi Party|Nazis]]. 

After 1986, he directed productions at the [[Deutsche Oper Berlin]], the [[Staatsoper Hamburg]], the [[Royal Opera House, Covent Garden|Royal Opera House Covent Garden]], the [[Houston Grand Opera]],<ref>{{cite web|last=Rothstein|first=Edward|title=Review/Opera; Beauty and Beast in Age of Motels|url=https://www.nytimes.com/1992/02/22/arts/review-opera-beauty-and-beast-in-age-of-motels.html|publisher=nytimes.com|accessdate=March 30, 2013|date=February 22, 1992}}</ref><ref>{{cite web|last=Swed|first=Mark|title=OPERA REVIEW : A Pair of 'Beauties,' Wagner in Houston|url=http://articles.latimes.com/1992-02-25/entertainment/ca-2594_1_houston-grand-opera|publisher=latimes.com|accessdate=March 30, 2013|date=February 25, 1992}}</ref>  the [[Wiener Staatsoper]], the Badische Staatsoper,<ref>{{cite web|last=Sohre|first=James|title=Badisches Staastheater’s production of Tosca starts off with a bang.|url=http://www.operatoday.com/content/2010/12/karlsruhe_tosca.php|publisher=operatoday.com|date=December 15, 2010}}</ref>  [[Oper Leipzig]], the [[Opera Comique]], the [[Zurich Opera]], [[Teatro Real Madrid]], [[Gothenburg Opera]] and the [[State Opera Prague]].

He was artistic director of the municipal theatres of the city of [[Dortmund, Germany|Dortmund]] from 1995 to 2001.<ref>{{cite web|last=Von Umbach|first=Klaus|title=Ich fordere mehr Demut|url=http://www.spiegel.de/spiegel/print/d-8002914.html|publisher=Der Spiegel|accessdate=March 30, 2013|language=German|date=September 21, 1998}}</ref>  His work there included a cycle of [[French opera]]s including [[Gustave Charpentier]]'s ''[[Louise (opera)|Louise]]'' and ''[[Julien (opera)|Julien]]'', [[Giacomo Meyerbeer|Meyerbeer]]'s ''[[Dinorah]]'', [[Ernest Bloch|Bloch]]'s ''[[Macbeth (Bloch)|Macbeth]]'', [[Hector Berlioz|Berlioz]]' ''[[Les Troyens]]'', [[Albert Roussel|Roussel]]'s ''[[Padmâvatî]]'', and [[Fromental Halévy|Halévy]]'s ''[[La Juive]]''. 

== Awards and honors ==
In appreciation for his services to the French nation, he has been honored with the title ‘[[Ordre des Arts et des Lettres|Officier dans l´ordre des Arts et des Lettres]]’.{{cn|date=March 2013}} In 2012 he was awarded the Carl Orff prize for his dedication to producing the works of [[Carl Orff]], in particular the opera ''Gisei'', which was a world premiere, staged at the Staatstheater Darmstadt in 2010.<ref>{{cite web|last=Britsch|first=Eckhard|title=John Dew, Intendant des Staatstheaters Darmstadt, erhält den Carl-Orff-Preis 2012|url=http://www.opernnetz.de/seiten/news/dar_dew_bri_120303.htm|publisher=Opernnetz.de|accessdate=March 30, 2013|language=German|date=March 3, 2012}}</ref>

==References==
{{Reflist|2}}
BOOK: Entartet Verdrängt Vergessen - Bielefelds Oper erhebt Einspruch 1980-1993
PUBLISHED BY: Westfalen Verlag
ISBN 3-88918-076-0

DVD: John Dew, Opera Producer - A Portrait. John Dew, Opera Producer - A Portrait ... Run time: 00:44:00. Director: Hubert Ortkemper / John Dew. Producers: Unitel
http://www.cmajor-entertainment.com/catalogue/show/id/711

== External links ==
* {{Official website|http://www.john-dew.com/index.html}}

{{Authority control}}
{{DEFAULTSORT:Dew, John}}
[[Category:British opera directors]]
[[Category:1944 births]]
[[Category:Living people]]