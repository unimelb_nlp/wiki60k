{{Use dmy dates|date=May 2014}}
{{Infobox airport
| name         = Tirana International Airport <br>Mother Teresa
| nativename   = {{small|''Aeroporti Ndërkombëtar i Tiranës <br>Nënë Tereza''}}
| image        = Tirana International Airport Logo.svg
| image-width  = 250
| image2       = Terminal jashte.jpg
| image2-width = 250
| IATA         = TIA
| ICAO         = LATI
| type         = Public
| owner        = Tirana International Airport SHPK<ref name="The_World's_Top 500_Airports">{{cite book | first1=David | last1=Wragg | title=The World's Top 500 Airports | publisher=Haynes Holdings | date=November 2009 | edition=2nd | location=Sparkford, Yeovil, Somerset BA22 7JJ, UK | page=13 | isbn=978 184425 632 7}}</ref>
| operator     = Tirana International Airport SHPK <small>(Part of [[AviAlliance]])</small><ref name="The_World's_Top 500_Airports"/>
| city-served  = [[Tirana]], [[Albania]]<ref name="The_World's_Top 500_Airports"/>
| location     = [[Rinas]]<ref name="The_World's_Top 500_Airports"/>
| hub          = *[[Albawings]]
| focus_city   = *[[Adria Airways]] 
*[[Alitalia]]
*[[Blu-express]]
*[[Mistral Air]]
| elevation-f  = 108
| elevation-m  = 33
| website      = [http://www.tirana-airport.com/ Official website]
| coordinates  = {{coord|41|24|53|N|019|43|14|E|region:AL|display=inline,title}}
| pushpin_map            = Albania
| pushpin_mapsize        = 200
| pushpin_label          = '''TIA'''
| pushpin_label_position = right
| pushpin_map_caption    = Location of Airport in Albania
| metric-rwy   = y
| r1-number    = 18/36
| r1-length-f  = 9,022<ref name="tirana-airport.com">{{cite web|url=http://www.tirana-airport.com/l/7/53/company-information/facts-figures/|title=Facts and Figures about Tirana International Airport Nënë Tereza - Tirana International Airport|publisher=}}</ref>
| r1-length-m  = 2,750<ref name="tirana-airport.com"/>
| r1-surface   = [[Asphalt]]
| stat-year = <small>2016</small>
| stat1-header = Passengers
| stat1-data   = 2,195,100
| stat2-header = Passenger change 15–16
| stat2-data   = {{increase}}11.3%
| stat3-header = Aircraft movements
| stat3-data   = 22,352<ref>http://www.tirana-airport.com/l/6/108/business-to-business/tia-market-statistics/</ref>
| stat4-header = Movements change 15–16
| stat4-data   = {{increase}}7.1%
| footnotes    = <small>Source: Albanian [[Aeronautical Information Publication|AIP]] at [[European Organisation for the Safety of Air Navigation|EUROCONTROL]]<ref name="AIP"/> Landings.com<ref name="Landings">[http://aerobaticsweb.org/cgi-bin/search_apt?LATI Airport record for Nënë Tereza/Rinas International Airport] at Landings.com. Retrieved 1 August 2013</ref></small>
}}

'''Tirana International Airport Nënë Tereza''' ({{lang-sq|Aeroporti Ndërkombëtar i Tiranës Nënë Tereza}}, {{Airport codes|TIA|LATI|p=n}}) is currently [[Albania]]'s only [[international airport]].  It is commonly known as '''Rinas International Airport''', as it is located in the village of [[Rinas]] {{convert|6|NM|lk=in|abbr=off}} northwest<ref name="AIP">{{cite web|url=http://www.ead.eurocontrol.int/publicuser/protect/pu/main.jsp|title=EAD Basic - Error Page|publisher=|accessdate=7 June 2015}}</ref> of [[Tirana]]. The airport was named in 2001, after the [[Albanians in the Republic of Macedonia|Albanian]] [[Roman Catholic]] nun and missionary [[Mother Teresa]]. 

The airport is one of the busiest airport in the [[Balkans]], handling nearly 2.2 million passengers in 2016.<ref name="auto">{{cite web|title=Transporti ajror, Shkupi, Prishtina dhe Podgorica lanë pas Rinasin në 2016|url=http://www.monitor.al/transporti-ajror-shkupi-prishtina-dhe-podgorica-lane-pas-rinasin-ne-2016/|accessdate=6 February 2017}}</ref> It offers international connections primarily within Europe. It serves as a hub for local carrier [[Albawings]] and as a focus city for [[Alitalia]], [[Adria Airways]], [[Blue Panorama Airlines]] and [[Mistral Air]].<ref>{{cite web|url=http://www.tirana-airport.com/d/7/46/135/local-based-carrier-albawings-continues-to-grow-successfully-in-the-market/|title=Local-based carrier Albawings continues to grow successfully in the market - Tirana International Airport|publisher=|accessdate=21 February 2017}}</ref>

==History==

===Early years===
Tirana International Airport Nënë Tereza was constructed during a period of two years, from 1955 to 1957.  However, Tirana had commercial airline services before.  As early as 1938, Yugoslav carrier [[Aeroput]] introduced regular commercial flights linking Tirana with [[Belgrade]] with a landing in [[Dubrovnik]].<ref>[http://www.europeanairlines.no/drustvo-za-vazdusni-saobracaj-a-d-aeroput-1927-1948/ Drustvo za Vazdusni Saobracaj A D – Aeroput (1927–1948)] at europeanairlines.no</ref> With the [[fall of communism in Albania]] in 1991, and subsequent liberalization of travel restrictions abroad for [[Albanians]], the number of airlines operating at the airport increased.

===Development since the 2000s===
The air traffic equipment and facilities of the airport have been heavily modernised following investments by Tirana International Airport SHPK, a consortium led by [[Hochtief AirPort]]. Hochtief assumed management of the airport on 23 April 2005, for a 20-year concession period.<ref>{{cite web|url=http://www.tirana-airport.com/?RoseToken=169180229114147161160144183148154169229193083154156152160214184090168180231crc339|title=Tirana International Airport|publisher=|accessdate=7 June 2015}}</ref>

The concession included the construction of a completely new passenger terminal and various infrastructure improvements, among them the construction of a new access road, new parking lots, and a bridge over the old airport access road.<ref name = tirana-airport>{{cite web|url=http://www.tirana-airport.com/?RoseToken=169180229114147161160144183148154169229193083156156151160214184090168180231crc340|title=Tirana International Airport|publisher=|accessdate=7 June 2015}}</ref> The expansion resulted in an increased number of passengers per annum, estimated at 1.5M passengers for 2009.<ref>{{cite web|url=http://www.tirana-airport.com/?RoseToken=160172155187150151177202197084168180238114147166153090150154149152154155089155156154160214184090168180231crc471|title=Tirana International Airport|publisher=|accessdate=7 June 2015}}</ref><ref>[http://www.hochtief-concessions.com/concessions_en/350.jhtml Tirana International Airport, Albania] – HOCHTIEF.</ref> The number of passengers effectively increased to more than 1.5M in 2010.<ref name="Facts and Figures">{{cite web|url=http://www.tirana-airport.com/?RoseToken=169180229114147161160144183148154169229193083162156154160214184090168180231crc340|title=Facts and Figures about Tirana International Airport Nënë Tereza|publisher=}}</ref>
The Terminal building and its second expansion, the Cargo building, its landscaping and carpark canopies was designed by Malaysian Architect Hin Tan of Hintan Associates.

In 2016, the Government of Albania reached an agreement with Tirana International Airport on ending its monopoly on flights, opening the way for an [[Kukës International Airport Shaikh Zayed|airport]] at [[Kukës]] in the north to open.<ref>{{cite web|last1=Mejdini|first1=Fatjona|title=Albanians Get to Fly from Second Airport|url=http://www.balkaninsight.com/en/article/albania-to-open-flights-from-kukesi-airport-04-01-2016|website=Balkan Insight|date=1 April 2016}}</ref>

In December 2016 the Airport announced that it has served 2 million passengers during 2016, reaching its second milestone.<ref>https://www.ata.gov.al/en/tirana-international-airport-reaches-two-millionth-passenger-milestone/</ref>

==Ownership==
The following companies were the owners of the Tirana International Airport Nënë Tereza until 2016:<ref name="tirana-airport"/>
* 47% [[AviAlliance]]
* 31.7% [[German Investment Corporation]]
* 21.3% [[Albanian-American Enterprise Fund]]

After the Government of Albania reached an agreement with the Airport on ending its monopoly on flights, the airport was sold to [[China Everbright Limited]],<ref>{{cite web|url=http://shqiptarja.com/Ekonomi/2733/--8203-aeroporti-i-rinasit-kalon-100-tek-kompania-kineze-378503.html|title=Shqiptarja.com - Aeroporti i Rinasit kalon 100% tek kompania kineze|publisher=|accessdate=21 February 2017}}</ref> a company specialized in [[asset management]], [[direct investment]], [[brokerage]] and [[investment banking]].<ref>{{cite web|url=http://www.irasia.com/listco/hk/everbright/profile.htm|title=irasia.com - China Everbright Limited|publisher=|accessdate=21 February 2017}}</ref>

==Airlines and destinations==
The following airlines operate scheduled, seasonal and charter flights out of [[Tirana]]

<!-- PLEASE DO NOT ADD ROUTES AND AIRLINES (INCLUDING CHARTERS) WITHOUT GIVING A RELIABLE SOURCE!-->
{{Airport destination list
<!-- -->
| [[Adria Airways]] | [[Frankfurt Airport|Frankfurt]], [[Ljubljana Jože Pučnik Airport|Ljubljana]], [[Munich Airport|Munich]] <br>'''Seasonal charter:''' [[Poprad-Tatry Airport|Poprad-Tatry]]     
<!-- -->
| [[Aegean Airlines]] | [[Athens International Airport|Athens]]
<!-- -->
| [[Aegean Airlines]]<br>{{nowrap|operated by [[Olympic Air]]}} | [[Athens International Airport|Athens]]
<!-- -->
| [[Air Serbia]] | [[Belgrade Nikola Tesla Airport|Belgrade]]
<!-- -->
| [[Alitalia]] | [[Bari Karol Wojtyła Airport|Bari]], [[Leonardo da Vinci–Fiumicino Airport|Rome–Fiumicino]]
<!-- -->
| [[Albawings]] | [[Ancona Airport|Ancona]], [[Bari Karol Wojtyła Airport|Bari]], [[Bologna Airport|Bologna]], [[Florence Airport|Florence]], [[Genoa Cristoforo Colombo Airport|Genoa]], [[Malpensa Airport|Milan–Malpensa]], [[Perugia Airport|Perugia]], [[Rimini Airport|Rimini]], [[Treviso Airport|Treviso]], [[Verona Airport|Verona]]<ref>{{cite web|url=http://albawings.com/web/|title=Albawings -|publisher=|accessdate=21 February 2017}}</ref>
<!-- -->
| [[Anda Air]] |'''Seasonal charter:''' [[Kyiv International Airport (Zhuliany)|Kiev-Zhuliany]] (begins {{date|2017-06-19}})
<!-- -->
| [[Austrian Airlines]] | [[Vienna International Airport|Vienna]]
<!-- -->
| [[Blu-express]]<br>{{nowrap|operated by [[Blue Panorama Airlines]]}}<!--Blu express is a virtual airline, and is the lowcost brand of Blue Panorama --> | [[Il Caravaggio International Airport|Bergamo]]<!--Airport is always referred to as Bergamo ONLY in every article as per WP:AIRPORTS-->, [[Bologna Guglielmo Marconi Airport|Bologna]], [[Genoa Cristoforo Colombo Airport|Genoa]], [[Malpensa Airport|Milan–Malpensa]], [[Pisa International Airport|Pisa]], [[Leonardo da Vinci–Fiumicino Airport|Rome–Fiumicino]], [[Turin Airport|Turin]], [[Venice Marco Polo Airport|Venice]], [[Verona Airport|Verona]]
<!-- -->
| [[Bravo Airways]] | '''Seasonal charter:''' [[Kyiv International Airport (Zhuliany)|Kiev-Zhuliany]]
<!-- -->
| [[British Airways]] | [[Gatwick Airport|London–Gatwick]]
<!-- -->
| [[Corendon Airlines]] | '''Seasonal charter:''' [[Antalya Airport|Antalya]]
<!-- -->
| [[DART Ukrainian Airlines]] | '''Seasonal charter:''' [[Kyiv International Airport (Zhuliany)|Kiev-Zhuliany]] 
<!-- -->
| [[Eurowings]] <br>{{nowrap|operated by [[Germanwings]]}} | '''Seasonal:''' [[Cologne Bonn Airport|Cologne/Bonn]], [[Stuttgart Airport|Stuttgart]]
<!-- --> 
| [[Mistral Air]] | [[Marche Airport|Ancona]], [[Bari Karol Wojtyła Airport|Bari]], [[Il Caravaggio International Airport|Bergamo]], [[Bologna Guglielmo Marconi Airport|Bologna]], [[Cuneo International Airport|Cuneo]], [[Florence Airport, Peretola|Florence]], [[Genoa Airport|Genoa]], [[Malpensa Airport|Milan–Malpensa]], [[Perugia Airport|Perugia]], [[Abruzzo Airport|Pescara]], [[Pisa Airport|Pisa]], [[Federico Fellini International Airport|Rimini]], [[Trieste Airport|Trieste]], [[Verona Airport|Verona]]
<!-- -->
| [[Lufthansa Regional]] <br>{{nowrap|operated by [[Lufthansa Cityline]]}} | [[Frankfurt Airport|Frankfurt]] 
<!-- -->
| [[Onur Air]] | '''Seasonal charter:''' [[Antalya Airport|Antalya]]
<!-- -->
| [[Pegasus Airlines]] | [[Sabiha Gökçen International Airport|Istanbul–Sabiha Gökçen]] <br>'''Seasonal charter:''' [[Antalya Airport|Antalya]] 
<!-- -->
| [[Small Planet Airlines]] | '''Seasonal charter:''' [[Katowice Airport|Katowice]], [[Gatwick Airport|London-Gatwick]], [[Stansted Airport|London-Stansted]], [[Verona Airport|Verona]], [[Chopin Airport|Warsaw-Chopin]]
<!-- -->
| [[Smartwings]] <br>{{nowrap|operated by [[Travel Service (airline)|Travel Service]]}} | '''Seasonal:''' [[Brno Airport|Brno]], [[Prague Airport|Prague]]
<!-- -->
| [[Transavia]] | [[Amsterdam Airport Schiphol|Amsterdam]] (begins 10 May 2017)<ref>{{cite web|url=http://www.routesonline.com/news/38/airlineroute/269126/transavia-expands-netherlands-operation-in-s17/|title=Transavia expands Netherlands operation in S17|first=UBM (UK) Ltd.|last=2016|publisher=}}</ref>
<!-- --> 
| [[Transavia France]] | '''Seasonal:''' [[Orly Airport|Paris–Orly]]
<!-- -->
| [[Travel Service Polska]] | '''Seasonal charter:''' [[Katowice Airport|Katowice]], [[Chopin Airport|Warsaw]]
<!-- -->
| [[Travel Service Slovakia]] | '''Seasonal charter:''' [[Bratislava Airport|Bratislava]], [[Kosice Airport|Kosice]]
<!-- -->
| [[TUI fly Belgium]] | [[Brussels Airport|Brussels]]<ref>{{cite web|url=http://www.jetairfly.com/en/flightplan|title=Jetairfly Flight Plan|publisher=Jetairfly}}</ref>
<!-- -->
| [[Turkish Airlines]] | [[Istanbul Atatürk Airport|Istanbul–Atatürk]]
<!-- -->
| [[Volotea]] | '''Seasonal:''' [[Verona Airport|Verona]]
<!-- -->
| [[Wizz Air]] | [[Budapest Airport|Budapest]]<ref>{{cite web|url=http://www.tirana-airport.com/d/7/46/136/low-cost-airline-wizz-air-announces-the-first-route-tirana-budapest-on-5-april-2017/|title=Low-Cost Airline Wizz Air announces the first route Tirana-Budapest on 5 April 2017}}</ref>
}}

==Statistics==
[[File:Tirana Airport 1978.jpg|thumb|Old picture of the Airport in 1978]]
[[File:Interior of airport terminal 2014-08-17 22-04.jpg|thumb|Terminal interior]]
[[File:AlitaliaA320TiranaEI-EIA.jpg|thumb|[[Alitalia]] [[Airbus A320]] in Tirana]]
[[File:Adria at TIA.JPG|thumb|[[Adria Airways]] [[Airbus A319]] in Tirana]]

===Traffic===

{| class="toccolours sortable" style="text-align:right;"
|+ '''Passenger, aircraft operations and cargo statistics at Nënë Tereza Airport'''
|-
! Year
! Passenger 
! Change
! Aircraft Operations
! Change
! Cargo <br><small>(metric tons)</small>
! Change 
|- style="background:#eee;"
|2005||785,000 || {{increase}}20.77% || 15,400 || N.A. || N.A. || N.A.
|- style="background:#eee;"
|2006||906,103 || {{increase}}15.43% || 15,856 || {{increase}} 2.96% || 2,435 || N.A.
|- style="background:#eee;"
|2007||1,105,770 || {{nowrap|{{increase}}22.04%}} || 18,258 || {{nowrap|{{increase}}15.15%}} || 3,832 || {{nowrap|{{increase}}57.37%}}
|- style="background:#eee;"
|2008||1,267,041 || {{increase}}14.58% || 19,194 || {{increase}} 5.13% || 2,497 || {{decrease}}34.84%
|- style="background:#eee;"
|2009||1,394,688 || {{increase}}10.07% || 20,064 || {{increase}} 4.53% || 2,265 || {{decrease}} 9.29%
|- style="background:#eee;"
|2010||1,536,822 || {{increase}}10.19% || 20,768 || {{increase}} 3.51% || 2,355 || {{increase}} 3.97%
|- style="background:#eee;"
|2011||1,817,073 || {{increase}}18.24% || 22,988 || {{increase}}10.69% || 2,656 || {{increase}}12.78%
|- style="background:#eee;"
|2012||1,665,331 || {{decrease}} 8.35% || 20,528 || {{decrease}}10.70% || 1,875 || {{decrease}}29.41%
|- style="background:#eee;"
|2013||1,757,342 || {{increase}} 5.53% || 19,942 || {{decrease}} 2.85% || 2,164 || {{increase}}15.41%
|- style="background:#eee;"
|2014||1,810,305 || {{increase}} 3.02% || 17,928 || {{decrease}} 3.02% || 2,324 || {{increase}}13.53%
|- style="background:#eee;"
|2015||1,997,044 || {{increase}} 10.3% || 20,876 || {{increase}} 16.4% || 2,229 || {{decrease}}4.1%<ref name="tirana-airport.com"/>
|- style="background:#eee;"
|2016||2,195,100<ref name="auto"/> || {{increase}} 11.3% || 22,352 || {{increase}} 7.1% || 2200 || {{decrease}}1%
|}

===Busiest Routes===

{| class="wikitable" style="font-size: 85%" width= align=
|+ Most frequent routes at Tirana Airport
|-
! Rank
! Destination
! Airport(s)
! Number of Passengers 2016<ref>{{cite web|title=Tirana Airport passes two million passenger mark; Wizz Air to become newest carrier with Budapest connection; S17 capacity up 22%|url=http://www.anna.aero/2017/03/08/tirana-passes-two-million-passenger-mark/|date=8 March 2017}}</ref>
! Top carriers
|-
| 1
| {{flagicon|Italy}} [[Milan]]
| [[Milan–Malpensa Airport|MXP]], [[Orio al Serio International Airport|BGY]]
| 321.521
| [[Albawings]], [[Blue Panorama Airlines]], [[Mistral Air]]
|-
| 2
| {{flagicon|Italy}} [[Rome]]
| [[Rome-Fiumicino|FCO]]
| 300.601
| [[Alitalia]], [[Blue Panorama Airlines]]
|-
| 3
| {{flagicon|Turkey}} [[Istanbul]]
| [[Ataturk Airport|IST]], [[Sabiha Gökçen Airport|SAW]]
| 203.147
| [[Pegasus Airlines]], [[Turkish Airlines]]
|-
| 4
| {{flagicon|Austria}} [[Vienna]]
| [[Vienna International Airport|VIE]]
| 159.109
| [[Austrian Airlines]]
|-
| 5
| {{flagicon|Italy}} [[Pisa]]
| [[Pisa Airport|PSA]]
| 126.333
| [[Blue Panorama Airlines]], [[Mistral Air]]
|-
| 6
| {{flagicon|Italy}} [[Bologna]]
| [[Bologna Airport|BLQ]]
| 111.460
| [[Albawings]], [[Blue Panorama Airlines]], [[Mistral Air]]
|-
| 7
| {{flagicon|United Kingdom}} [[London]]
| [[London-Gatwick|LGW]], [[London-Stansted|STN]]
| 86.984
| [[British Airways]], [[Small Planet Airlines]]
|-
| 8
| {{flagicon|Greece}} [[Athens]]
| [[Athens International Airport|ATH]]
| 78.349
| [[Aegean Airlines]]
|-
| 9
| {{flagicon|Germany}} [[Frankfurt]]
| [[Frankfurt International Airport|FRA]]
| 75.584
| [[Adria Airways]], [[Lufthansa]]
|-
|}

==Ground transportation==
[[File:Taxis_in_Albania_International_Airport.jpg|thumb|Taxi rank at the airport]]
===Car===
The airport is linked with motorway SH60 (10&nbsp;km away) to SH2 [[Durres]] -[[Tirana]] access road. Taxis and car rental facilities are available at the airport. Taxis are available at the airport. The journey from Tirana to the airport takes 20–25 minutes, depending on traffic.

===Bus===
Rinas Express airport bus, located outside Arrivals terminal, leaves on the hour every hour (8am to 7pm), to the city centre, 
and the trip takes around 25 to 30 minutes.
Rinas Express operates 12 hours (6 am to 6 pm) as an hourly bus service between the Airport and the National Museum in the centre of Tirana. The single fare is 250 Albanian Lek.
From Durres the rate for a single fare is 480 Albanian Lek.

==Incidents and accidents==
{{main article| Turkish Airlines Flight 1476 }}
*October 3, 2006: [[Turkish Airlines Flight 1476]], flying from [[Tirana]] to [[Istanbul]], was hijacked by Hakan Ekinci in Greek airspace. The aircraft, with 107 passengers and six crew on board, transmitted two coded hijack signals which were picked up by the Greek air force; the flight was intercepted by military aircraft and landed safely at [[Brindisi]], Italy.
*June 30, 2016: Three armed and masked people entered the cargo terminal, where they robbed a huge amount of money that were to be transported abroad on airplanes. The amount of cash could be up to 3 million euros. The incident caused national security concerns.<ref>{{cite web|url=http://www.tiranatimes.com/?p=128285|title=Spectacular airport robbery raises national security concerns|first=Tirana|last=Times|date=30 June 2016|publisher=|accessdate=21 February 2017}}</ref><ref>{{cite web|url=http://english.albeu.com/news/news/huge-amount-of-money-stolen-at-the-tirana-airport/250380/|title=Huge amount of money stolen at the Tirana Airport|publisher=|accessdate=21 February 2017}}</ref>

==See also==
{{Portal|Albania|Tirana|Aviation}}
*[[Transport in Albania]] 
*[[Pristina International Airport]]
{{clear}}

==References==
{{reflist|30em}}

==External links==
{{Commons category inline|Tirana International Airport Nënë Tereza}}
* [http://www.tirana-airport.com/ Tirana International Airport Nënë Tereza]
* [http://www.balkans.com/open-news.php?uniquenumber=95946 Year numbers of airport]
* [http://www.airliners.net/open.file/0340056/L/ Aerial photo of Rinas Mother Teresa Airport]
* [http://aerobaticsweb.org/cgi-bin/search_apt?LATI Airport record for Nënë Tereza/Rinas International Airport] at Landings.com.
* {{ASN|TIA}}
* {{SkyVector|LATI}}
* {{NWS-current|LATI}}

{{Durrës}}
{{Tirana}}
{{Airfields in Albania}}

{{DEFAULTSORT:Tirana International Airport Nene Tereza}}
[[Category:Airports in Albania]]
[[Category:Buildings and structures in Krujë]]
[[Category:Airfields of the United States Army Air Forces Air Transport Command in the European Theater]]
[[Category:Airports established in 1957]]
[[Category:China Everbright Group]]