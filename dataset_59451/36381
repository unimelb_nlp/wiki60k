{{other ships|HMS Andromeda}}
{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=[[File:HMS Andromeda (1897).jpg|300px|]]
|Ship caption=''Andromeda'' at anchor at [[Weihai|Weihaiwei]], [[China]], 1904.
}}
{{Infobox ship career
|Hide header=
|Ship country=United Kingdom
|Ship flag={{shipboxflag|UK|naval}}
|Ship name=''Andromeda''
|Ship ordered=
|Ship namesake=[[Andromeda (mythology)|Andromeda]]
|Ship builder=[[Pembroke Dockyard]] 
|Ship laid down=2 December 1895
|Ship launched=30 April 1897
|Ship christened=
|Ship completed=5 September 1899
|Ship recommissioned=
|Ship decommissioned=
|Ship in service=
|Ship out of service=
|Ship renamed=*''Powerful II'', 23 September 1913
*''Impregnable II'' November 1919
*''Defiance'' 20 January 1931
|Ship reclassified=As a [[training ship]], 23 September 1913 
|Ship refit=
|Ship struck=
|Ship reinstated=
|Ship fate=Sold for [[ship breaking|scrap]], 1956
|Ship motto=
|Ship nickname=
|Ship honours=
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship class={{sclass-|Diadem|cruiser|0}} [[protected cruiser]]
|Ship displacement={{convert|11000|LT|t|0}}
|Ship length={{convert|435|ft|m|abbr=on|1}} ([[Length between perpendiculars|p/p]])
|Ship beam={{convert|69|ft|m|abbr=on|1}} 
|Ship draught={{convert|25|ft|6|in|m|abbr=on|1}} 
|Ship power=*{{convert|16500|ihp|lk=in|abbr=on}}
*30 × [[Belleville boiler]]s 
|Ship propulsion=*2 shafts
*2 × [[Marine steam engine#Triple or multiple expansion|Triple-expansion steam engines]]
|Ship speed={{convert|20.5|kn|lk=in|abbr=on}}
|Ship range=
|Ship complement=677
|Ship armament=*16 × single [[QF 6 inch Mk I - III naval gun|QF {{convert|6|in|mm|sing=on|sigfig=3}} guns]]
*12 × single [[QF 12-pounder 12 cwt naval gun|QF 12-pdr {{convert|3|in|abbr=on}} 12-cwt guns]]<ref group=Note>"Cwt" is the abbreviation for [[hundredweight]], 20 cwt referring to the weight of the gun.</ref>
*3 × single [[QF 3-pounder Hotchkiss|QF 3-pdr ({{convert|47|mm|in|abbr=on|1|disp=flip}})]] [[Hotchkiss gun]]s
*2 × [[British 18 inch torpedo|18-inch (450 mm) torpedo]] tubes
|Ship armour=*{{convert|6|in|mm|abbr=on|0}} [[casemate]]s
*{{convert|4.5|-|2|in|mm|abbr=on}} [[Deck (ship)|decks]]
|Ship notes=
}}
|}
'''HMS ''Andromeda''''' was one of eight {{sclass-|Diadem|cruiser|0}} [[protected cruiser]]s built for the [[Royal Navy]] in the 1890s. Upon completion in 1899, the ship was assigned to the [[Mediterranean Fleet]] where she helped to escort a [[royal yacht]] during its cruise through the [[Mediterranean Sea]]. After a refit, she was assigned to the [[China Station]] in 1904 and returned home three years later to be reduced to [[Reserve fleet|reserve]]. ''Andromeda'' was converted into a [[training ship]] in 1913 and remained in that role under various names until 1956. That year she was sold for [[ship breaking|scrap]] and broken up in [[Belgium]], the last [[Pembroke Dock|Pembroke]]-built ship still afloat.{{sfn|Phillips|2014|page=259}}

==Design and description==
The ''Diadem'' class was designed to protect British merchant shipping from fast [[cruiser]]s like the Russian {{ship|Russian cruiser|Rurik|1892|2}}{{sfn|McBride|1987|page=211}} and were smaller versions of the {{sclass-|Powerful|cruiser|4}}. The ships had a [[length between perpendiculars]] of {{convert|435|ft|m|1}}, a [[beam (nautical)|beam]] of {{convert|69|ft|m|1}} and a [[draft (ship)|draught]] of {{convert|25|ft|6|in|m|1}}. They [[Displacement (ship)|displaced]] {{convert|11000|LT|t}}. The first batch of ''Diadem''s were powered by a pair of four-cylinder [[triple-expansion steam engine]]s, each driving one shaft, which were designed to produce a total of {{convert|16500|ihp|lk=in}} and a maximum speed of {{convert|20.5|kn|lk=in}} using steam provided by 30 [[Belleville boiler]]s. They carried a maximum of {{convert|1900|LT|t|0}} of coal{{sfn|Chesneau|Kolesnik|1979|page=68}} and their hulls were sheathed with copper to reduce [[biofouling]]. Their complement numbered 677 officers and [[naval rating|ratings]].{{sfn|Phillips|2014|pages=214–15}}

The main armament of the ''Diadem''-class ships consisted of 16 [[quick-firing gun|quick-firing (QF)]] [[QF 6 inch /40 naval gun|QF {{convert|6|in|mm|sing=on|sigfig=3}} guns]]. Four of these were on the [[forecastle]] and in the stern, all protected by [[gun shield]]s. The remaining dozen guns were in armoured [[casemate]]s on each [[broadside]].{{sfn|Chesneau|Kolesnik|1979|page=5}} The ships carried 200 [[Cartridge (firearms)|rounds]] per gun.{{sfn|Phillips|2014|page=215}} Protection against [[torpedo boat]]s was provided by a dozen [[QF 12-pounder 12 cwt naval gun|QF 12-pounder {{convert|3|in|abbr=on}}, 12-cwt guns]],{{sfn|Chesneau|Kolesnik|1979|page=5}} for which 300 rounds per gun was provided,{{sfn|Phillips|2014|page=15}} and 3 [[QF 3-pounder Hotchkiss|QF 3-pounder ({{convert|47|mm|in|abbr=on|1|disp=flip}})]] [[Hotchkiss gun]]s.{{sfn|Chesneau|Kolesnik|1979|page=5}} In addition, the ships carried a pair of [[Ordnance QF 12-pounder 8 cwt|Ordnance QF 12-pounder 8-cwt]] landing guns for use ashore. The ships were also armed with a pair of submerged 18-inch (450&nbsp;mm) [[torpedo tube]]s.{{sfn|Phillips|2014|pages=213–14}}

The sloped armoured [[deck (ship)|deck]] ranged in thickness from {{convert|2.5|to|4|in|0}} on the flat and slopes, respectively.{{sfn|Chesneau|Kolesnik|1979|page=5}} The casemates were protected by 6 inches of [[Harvey armour]]{{sfn|Phillips|2014|page=15}} while the gun shields had {{convert|2|to|4.5|in|0}} of armour.{{sfn|Chesneau|Kolesnik|1979|page=5}} The [[conning tower]]s were protected by {{convert|12|in|0|adj=on}} walls and their roofs were 2 inches thick.{{sfn|Phillips|2014|page=15}} The tubes protecting the ammunition hoists were also 2 inches thick.{{sfn|Chesneau|Kolesnik|1979|page=5}}

==Construction and career==
''Andromeda'' was the fifth ship of her name to serve in the Royal Navy{{sfn|Colledge|Warlow|2006|page=15}} and was [[Keel|laid down]] on 2 December 1895 by [[Pembroke Dockyard]].{{sfn|Chesneau|Kolesnik|1979|page=5}} The ship was [[Ship naming and launching|launched]] on 30 April 1897 by Lady Scourfield, wife of Sir [[Scourfield baronets|Owen Scourfield]] [[Baronet|Bt]]. She was [[fitted out]] at Pembroke Dock until 5 September 1898 and sailed later that month to [[HMNB Portsmouth|Portsmouth Dockyard]] for completion.{{sfn|Phillips|2014|pages=258–59}}

Upon completion on 5 September 1899, she was assigned to the Mediterranean Fleet under the command of [[Captain (Royal Navy)|Captain]] John Burr, who was relieved on 12 March 1901 by Captain [[Francis Foley (Royal Navy officer)|Francis Foley]].<ref name="dn">{{cite web|title=H.M.S. Andromeda (1897)|url=http://www.dreadnoughtproject.org/tfs/index.php/H.M.S._Andromeda_%281897%29|website=www.dreadnoughtproject.org|publisher=The Dreadnought Project|accessdate=26 April 2016}}</ref> That month the ship was one of two cruisers tasked to escort the [[ocean liner]] {{HMS|Ophir}}, commissioned as a royal yacht for the world tour of the Duke and Duchess of Cornwall and York (later [[George V|King George]] and [[Mary of Teck|Queen Mary]]), from [[Gibraltar]] to [[Malta]], and then to [[Port Said]].<ref>{{Cite news |newspaper=The Times |title=The Duke of Cornwall´s visit to the colonies |date=13 March 1901 |page=5 |issue=36401}}</ref> Captain [[Christopher Cradock]] was appointed in command on 24 March 1902,<ref>{{Cite news |newspaper=The Times |title=Naval & Military intelligence |date=25 March 1902 |page=9 |issue=36724}}</ref> and from 11 June that year ''Andromeda'' served as flagship to [[Rear-Admiral (Royal Navy)|Rear-Admiral]] Sir [[Baldwin Wake Walker, 2nd Baronet|Baldwin Wake Walker]], commander of the Cruiser Division of the Mediterranean Fleet.<ref>{{Cite news |newspaper=[[The Times]] |title=Naval & Military intelligence |date=12 June 1902 |page=13 |issue=36792}}</ref> In May 1902 she visited [[Palermo]] to attend festivities in connection with the opening of an Agricultural Exhibition by King [[Victor Emmanuel III of Italy|Victor Emmanuel]],<ref>{{Cite news |newspaper=The Times |title=Naval & Military intelligence |date=27 May 1902 |page=10 |issue=36778}}</ref> and the following month the ship was in Gibraltar for a coronation fête.<ref>{{Cite news |newspaper=The Times |title=The Coronation - celebrations in the colonies |date=23 June 1902 |page=10 |issue=36801}}</ref>

''Andromeda'' returned home later that year and began a lengthy refit. She was assigned to the China Station in 1904 and returned home three years later. The ship was reduced to reserve at [[Chatham Dockyard]] upon her return, but transferred to [[HMNB Devonport|Devonport Dockyard]] shortly afterwards. ''Andromeda'' was assigned to the [[9th Cruiser Squadron]] of the new reserve [[Third Fleet (United Kingdom)|Third Fleet]] in 1912. The following year the ship was converted to a [[Boy seaman|boys']] training ship{{sfn|Friedman|2012|page=233}} and renamed ''Powerful II'' on 23 September 1913.{{sfn|Colledge|2006|page=15}} She was later renamed ''Impregnable II'' in November 1919 and finally, HMS ''Defiance'' on 20 January 1931,{{sfn|Colledge|2006|page=15}} when she became part of the [[HMS Defiance (shore establishment 1884)|torpedo school]].{{sfn|Friedman|2012|page=233}} The ship was sold for scrap in 1956 and arrived at [[Burgt]], Belgium, on 14 August to begin demolition.{{sfn|Phillips|2014|page=9}}

==Notes==
{{reflist|group=Note}}

==Citations==
{{reflist|30em}}

==References==
*{{cite book|title=Conway's All the World's Fighting Ships 1860-1905|editor1-last=Chesneau|editor1-first=Roger|editor2-last=Kolesnik|editor2-first=Eugene M.|publisher=[[Conway Maritime Press]]|location=Greenwich, UK|year=1979|isbn=0-8317-0302-4 |ref=harv}}
*{{cite book |last1=Colledge |first1=J. J. |authorlink1=J.J. Colledge |last2=Warlow |first2=Ben |year=2006 |origyear=1969 |title=Ships of the Royal Navy: The Complete Record of all Fighting Ships of the Royal Navy |edition=Revised |location=London |publisher=[[Lionel Leventhal|Chatham Publishing]] |isbn=978-1-86176-281-8 |oclc=67375475 |ref=harv}}
*{{cite book|last=Friedman|first=Norman|title=British Cruisers of the Victorian Era|date=15 November 2012|publisher=[[Seaforth Publishing|Seaforth]]|location=Barnsley, South Yorkshire, UK|isbn=978-1-59114-068-9 |ref=harv}}
*{{cite journal|last1=McBride|first1=Keith|title=The Diadem Class Cruisers of 1893|journal=[[Warship (magazine)|Warship]]|date=October 1987|issue=44|pages=210–16|issn=0142-6222 |ref=harv}}
*{{cite book|last=Phillips|first=Lawrie; Lieutenant Commander|title=Pembroke Dockyard and the Old Navy: A Bicentennial History|date=2014|publisher=[[The History Press]]|location=Stroud, Gloucestershire, UK|isbn=978-0-7509-5214-9|ref=harv}}

==External links==
* [http://www.worldwar1.co.uk/armoured-cruiser/hms-diadem.html Diadem class at worldwar1.co.uk]
{{Good article}}
{{Diadem class cruiser}}

{{DEFAULTSORT:Andromeda (1897)}}
[[Category:Diadem-class cruisers]]
[[Category:Pembroke-built ships]]
[[Category:1897 ships]]
[[Category:World War I cruisers of the United Kingdom]]