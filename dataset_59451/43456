__NOTOC__
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=PZL-126 Mrówka 
 | image=PZL 126 Mrowka.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=Agricultural aircraft
 | national origin=Poland
 | manufacturer=[[WSK-PZL]]
 | designer=
 | first flight=20 April 1990
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=
 | developed from= 
 | variants with their own articles=
}}
|}

The '''PZL-126 Mrówka''' (''ant'') is a [[Poland|Polish]] agricultural aircraft first flown in 1990. It is a diminutive low-wing monoplane of conventional if stubby appearance with a T-tail, an enclosed cabin and fixed, tricycle undercarriage. Significant parts of the design were undertaken by students at the training college attached to the PZL plant at [[Okęcie]] under the direction of Andrzej Słocinski,<ref name="JAWA87 204">''Jane's All the World's Aircraft 1987–88'', p. 204.</ref> with the whole project treated initially as a platform for design innovation.<ref name="Postlethwaite">Postlethwaite 1989, p. 14.</ref> Initial design work was completed in 1982 and detail work the following year.<ref name="JAWA87 205">''Jane's All the World's Aircraft 1987–88'', p. 205.</ref> Plans to fly a prototype by 1985 were delayed by revisions to the aircraft's equipment, although a mockup was displayed at an agricultural aviation exhibition at [[Olsztyn]] that August.<ref name="JAWA87 205" /> Legislation to outlaw certain agricultural chemicals was under consideration in Poland in the late 1980s, spurring interest in biological agents as an alternative and making a tiny aircraft like the Mrówka feasible as a useful piece of agricultural equipment.<ref name="Postlethwaite"/>

The prototype was eventually flown on 20 April 1990.<ref name="Simpson">Simpson 1995, p. 322.</ref><ref name="JAWA 05">''Jane's All the World's Aircraft 2005''</ref> After initial testing, the design was revised and the prototype rebuilt as the '''PZL-126P''', flying in this configuration on 20 October 2000.<ref name="JAWA 05" /> Flight testing commenced in 2003 and was still underway in 2005.<ref name="JAWA 05" />

<!-- ==Development== -->
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (as designed)==
{{aerospecs
|ref=<!-- reference -->''Jane's All the World's Aircraft 1987–88''<ref>Taylor 1988, p. 205.</ref>
|met or eng?=<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->met
|crew=1
|capacity=25 L (6.6 US gal) spray pod on each wingtip, plus retractable dispenser reel for 3 kg (6 lb) of biological agents under fuselage.
|length m=4.75
|length ft=15
|length in=7
|span m=5.00
|span ft=16
|span in=5
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=2.53
|height ft=8
|height in=7
|wing area sqm=
|wing area sqft=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=230
|empty weight lb=507
|gross weight kg=375
|gross weight lb=827
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|eng1 number=1
|eng1 type=[[PZL-F 2A-120-C1]]
|eng1 kw=<!-- prop engines -->45
|eng1 hp=<!-- prop engines -->60
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|max speed kmh=
|max speed mph=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->200
|cruise speed mph=<!-- if max speed unknown -->124
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->6
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=3.1
|climb rate ftmin=610
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{commons category|PZL-126 Mrówka}}
;Notes
{{reflist}}
;Bibliography
{{refbegin}}
* [http://www.janes.com/articles/Janes-All-the-Worlds-Aircraft/Agrolot-PZL-126P-Mrowka-2001-Poland.html."Agrolot PZL-126P Mrowka 2001".] ''Jane's All the World's Aircraft 2005 (extract)''. London: Jane's Information Group.  
* Postlethwaite, Alan. [http://www.flightglobal.com/pdfarchive/view/1989/1989%20-%201412.html "Poles back mini-ag aircraft".] ''Flight International'', 20 May 1989, p.&nbsp;14. 
* Simpson, R. W. ''Airlife's General Aviation''. Shrewsbury, UK: Airlife Publishing, 1995. 
* Taylor, John W. ''Jane's All the World's Aircraft 1987–88.'' London: Jane's Publishing, 1988. ISBN 0-7106-0867-5.
* Taylor, Michael J. H. ''Jane's Encyclopedia of Aviation''. London: Studio Editions, 1989. ISBN 0-517-69186-8.
{{refend}}
<!-- ==External links== -->

{{PZL aircraft}}

{{DEFAULTSORT:PZL-126 Mrowka}}
[[Category:Polish agricultural aircraft 1990–1999]]
[[Category:PZL aircraft]]