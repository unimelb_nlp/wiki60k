{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=[[Image:SS West Conob.jpg|300px|SS ''West Conob'' shortly after completion in 1919. She was renamed ''Mauna Loa'' in 1934.]]
|Ship caption= SS ''West Conob'' shortly after completion in 1919. She was renamed ''Mauna Loa'' in 1934.
}}
{{Infobox ship career
|Ship name=*''West Conob''
*1928: ''Golden Eagle''
*1934: ''Mauna Loa''
|Ship namesake=[[Mauna Loa]]
|Ship builder=*[[Los Angeles Shipbuilding & Dry Dock Company|Los Angeles Shipbuilding & Dry Dock Co.]]
*[[San Pedro, California]]
|Ship yard number=14<ref name=Colton>{{Cite web| last = Colton | first = Tim | title = Todd Pacific Shipyards, San Pedro CA | url = http://www.shipbuildinghistory.com/history/shipyards/2large/inactive/toddsanpedro.htm | work = Shipbuildinghistory.com | publisher = The Colton Company | accessdate = 23 September 2008 }} Colton refers to the ship as ''West Cohob''. ([[Todd Pacific Shipyards]] bought the Los Angeles Shipbuilding & Dry Dock Company in 1945.)</ref>
|Ship laid down=
|Ship launched=
|Ship sponsor=
|Ship completed=May 1919<ref name=Colton />
|Ship owner=*1919: {{USSB|first=short}}
*1928: [[Oceanic and Oriental Navigation Company|Oceanic and Oriental Navigation Co.]]<ref name=LAT-case-oil>{{Cite news| last = Drake | first = Waldo | title = Case-oil rush to Australia underway | work = Los Angeles Times | date = 15 March 1930 | page = 6 }}</ref>
*1934: [[Matson Navigation Company]]
|Ship operator=*1921: [[Pacific Mail Steamship Company|Pacific Mail Steamship Co.]]<ref name=LAT-tribute>{{Cite news| title = Tribute to ship built at harbor | work = Los Angeles Times | date = 17 April 1921 | page = I-7 }}</ref>
*1925: [[Swayne & Hoyt Lines]]<ref name=LAT-sn19251215>{{Cite news| title = Shipping and Los Angeles Harbor news | work = Los Angeles Times | date = 15 December 1925 | page = 19 }}</ref>
*1928: Oceanic and Oriental Navigation Co.<ref name=LAT-case-oil />
*1934: Matson Navigation Company
*1941: [[United States Department of War|War Department]]
|Ship identification=US Official number: 218048<ref name=Miramar>{{Cite web| url = http://www.miramarshipindex.org.nz/ship/list?search_op=OR&IDNo=2218048 | title = West Conob | work = Miramar Ship Index | publisher = R.B.Haworth | accessdate = 23 September 2008 }}</ref>
|Ship fate=bombed and sunk 19 February 1942 in the [[Bombing of Darwin (February 1942)|Bombing of Darwin]]<ref name=DANFS />
}}
{{Infobox ship characteristics
|Ship type=[[Design 1013 ship]]
|Ship tonnage=*1919: {{GRT|5,899}}<ref name=Miramar />
*1939: {{GRT|5,436}}<ref name=Jordan>Jordan, p. 404.</ref>
*{{DWT|8,600}}<ref name=Jordan />
|Ship displacement=
|Ship length=*{{convert|410|ft|1|in|m|abbr=on}} ([[length between perpendiculars|LPP]])<ref name=Miramar />
*{{convert|423|ft|2|in|m|abbr=on}} ([[length overall|overall]])<ref name=Jordan />
|Ship beam={{convert|54|ft|6|in|m|abbr=on}}<ref name=Miramar />
|Ship depth=
|Ship hold depth=
|Ship draft={{convert|24|ft|m|1|abbr=on}}<ref name=Jordan />
|Ship propulsion=*1 × [[triple-expansion steam engine]]<ref name=Miramar />
*1 × [[screw propeller]]<ref name=Miramar />
|Ship speed={{convert|10.5|knots|km/h}}<ref name=Miramar />
|Ship capacity=
|Ship crew=
|Ship armament=
|Ship notes=
}}
|}

'''SS ''Mauna Loa''''' was a [[steam engine|steam]]-powered [[cargo ship]] of [[Matson Navigation Company]] that was sunk in the [[Bombing of Darwin (February 1942)|bombing of Darwin]] in February 1942. She was christened '''SS ''West Conob''''' in 1919 and renamed '''SS ''Golden Eagle''''' in 1928. At the time of her completion in 1919, the ship was inspected by the [[United States Navy]] for possible use as '''USS ''West Conob'' (ID-4033)''' but was neither taken into the Navy nor ever [[ship commissioning|commissioned]].

''West Conob'' was built in 1919 for the {{USSB|first=long}}, part of the ''West'' series of ships—steel-[[hull (watercraft)|hull]]ed cargo ships built on the [[West Coast of the United States]] for the [[World War I]] war effort—and was the 14th ship built at [[Los Angeles Shipbuilding & Dry Dock Company]] in [[San Pedro, California]]. She initially sailed for the [[Pacific Mail Steamship Company]] and circumnavigated the globe twice by 1921. She began sailing to South America for [[Swayne & Hoyt Lines]] in 1925, and then, to Australia and New Zealand. When Swayne & Hoyt's operation was taken over by the [[Oceanic and Oriental Navigation Company]] a few years later, she sailed under the name ''Golden Eagle'' until 1934, when she was taken over by the [[Matson Navigation Company]] for service between Hawaii and the U.S. mainland and renamed ''[[Mauna Loa]]'', after the large [[shield volcano]] on the [[Island of Hawaii]].

Shortly before the United States' entry into [[World War II]], ''Mauna Loa'' was [[ship chartering|chartered]] by the [[United States Department of War]] to carry supplies to the [[Philippines]]. The ship was part of an aborted attempt to reinforce [[Allies of World War II|Allied]] forces under attack by the [[Empire of Japan|Japanese]] on [[Timor]] in mid-February 1942. After the return of her convoy to [[Darwin, Northern Territory]], ''Mauna Loa'' was one of eight ships sunk in Darwin Harbour in the first Japanese bombing attack on the Australian mainland on 19 February. The remains of her wreck and her cargo are a [[wreck diving|dive site]] in the harbor.

==Design and construction==
The ''West'' ships were [[cargo ship]]s of similar size and design built by several shipyards on the [[West Coast of the United States]] for the {{USSB|first=long}} for emergency use during [[World War I]]. Some 40 ''West'' ships were built by [[Los Angeles Shipbuilding & Dry Dock Company]] of Los Angeles,<ref name=Colton /> all given names that began with the word ''West''.<ref name=CW-358>Crowell and Wilson, pp. 358–59.</ref> ''West Conob'' (Los Angeles Shipbuilding yard number 14)<ref name=Colton /> was completed in May 1919.<ref name=Colton />

''West Conob'' was {{GRT|5,899|disp=long}}, and was {{convert|410|ft|1|in|m}} long ([[length between perpendiculars|between perpendiculars]]) and {{convert|54|ft|6|in|m}} [[beam (nautical)|abeam]].<ref name=Miramar /> She had a steel [[hull (watercraft)|hull]] and a [[deadweight tonnage]] of {{DWT|8,600}}.<!-- DWT --><ref name=Colton /><ref name=DANFS>{{cite DANFS | author = Naval Historical Center | title = West Conob | url = http://www.history.navy.mil/danfs/w5/west_conob.htm | short = on }}</ref><!-- steel hull --> Sources do not give ''West Conob''{{'}}s other hull characteristics, but {{SS|West Grama||2}}, a [[sister ship]] also built at Los Angeles Shipbuilding had a [[displacement (ship)|displacement]] of 12,225 t with a mean [[draft (hull)|draft]] of {{convert|24|ft|2|in|m}}, and a [[hold (ship)|hold]] {{convert|29|ft|9|in|m}} deep.<ref>{{cite DANFS | author = Naval Historical Center | title = West Grama | url = http://www.history.navy.mil/danfs/w5/west_grama.htm | short = on }}</ref>

''West Conob''{{'}}s power plant consisted of a single [[triple-expansion steam engine|triple-expansion]] [[steam engine|reciprocating steam engine]] with cylinders of 28½, 47, and 78&nbsp;inches (72, 120, and 200&nbsp;cm) with a {{convert|48|in|cm|adj=on}} stroke. She was outfitted with three Foster [[water-tube boiler]]s, each with a heating area of {{convert|4150|sqft}} and containing 52 {{convert|4|in|cm|adj=on}} and 827 {{convert|2|in|cm|adj=on}} tubes.<ref name=Andros-164>Andros, p. 164.</ref> Her boilers were heated by mechanical [[oil burner (engine)|oil burners]] fed by two pumps, each 6 by 4 by 6&nbsp;inches (15 × 10 × 15&nbsp;cm) with a capacity of {{convert|30|U.S.gal|L}} per minute.<ref>Andros, pp. 164–65.</ref> Fully loaded, the ship could hold {{convert|6359|oilbbl}} of [[fuel oil]]. ''West Conob''{{'}}s single [[screw propeller]] was {{convert|17|ft|1|in|m}} in diameter with a {{convert|15|ft|3|in|m|adj=on}} pitch and a developed area of {{convert|102|sqft}}.<ref name=Andros-164 /><ref group=Note>The ''developed area'' of a propeller is the surface area of all blades combined. See: Eliasson and Larsson, pp. 174–75, 179.</ref> The ship was designed to travel at {{convert|11|knots|km/h}},<ref name=Andros-164 /> and averaged {{convert|11.1|knots|km/h}} during her first voyage in June 1919.<ref>Andros, p. 162.</ref>

==Career==
After completion, ''West Conob'' was inspected by the [[12th Naval District]] of the [[United States Navy]] for possible naval service and was assigned the identification number of 4033. Had she been [[ship commissioning|commissioned]], she would have been known as USS ''West Conob'' (ID-4033), but the Navy neither took over the ship nor commissioned her.<ref name=DANFS />

Little information on the first years of ''West Conob''{{'}}s career is found in sources. But it is known that she was operated by the [[Pacific Mail Steamship Company]] on [[Pacific Ocean|Pacific]] routes.<ref name=LAT-tribute /> The ship departed Los Angeles on her maiden voyage to Hong Kong, making her way to San Francisco. ''West Conob'' departed from there on 13 June 1919 for [[Honolulu]], where she arrived eight days later. After refueling at Honolulu, she headed to Hong Kong, and from there, retraced her route to return to San Francisco.<ref>Andros, pp. 162, 164.</ref> Details of later voyages are not available, but by mid-April 1921, ''West Conob'' had completed two [[circumnavigation]]s without needing to stop for repairs. At that time, the {{USSB}} allocated ''West Conob'' for service to [[Genoa]].<ref name=LAT-sn19251215 /><ref group=Note>The [[Genoa]] service to which ''West Conob'' was allocated was reported as being from unspecified "northern ports".</ref>

In December 1925, ''West Conob'' was allocated to [[Swayne & Hoyt Lines]] for service to the east coast of South America.<ref name=LAT-sn19251215 /> By mid-1926, ''West Conob'' was sailing for Swayne & Hoyt's American-Australian-Orient Line when she was reported in the ''[[Los Angeles Times]]'' as sailing to New Zealand with {{convert|350000|sqft}} of [[wallboard]].<!-- name of line --><ref name=LAT-large>{{Cite news| title = Large shipping deal in making | work = Los Angeles Times | date = 18 October 1927 | page = 11 }}</ref><ref>{{Cite news| title = Large foreign shipment made by local firm | work = Los Angeles Times | date = 13 June 1926 | page = E12 }}</ref><!-- details of cargo, destination -->

In October 1927, the ''Los Angeles Times'' reported on the impending sale of ''West Conob'' and 18 other Swayne & Holt ships to a San Francisco financier.<ref name=LAT-large /> The ship later became a part of the fleet of the [[Oceanic and Oriental Navigation Company]], a joint venture between Oceanic-Matson, a subsidiary of [[Matson Navigation Company]], and the [[American-Hawaiian Steamship Company]], established to take over operation of transpacific routes that had been managed for the {{USSB}} by Swayne & Holt Lines.<ref>{{Cite news| title = New shipping concern | work = The New York Times | date = 23 February 1928 | page = 43 }}</ref><ref group=Note>Oceanic-Matson operated the California – Australia – New Zealand routes, while the [[American-Hawaiian Steamship Company]] operated the routes to China.</ref> Some time after March 1928,<ref>{{Cite news| title = Vessels in port | work = Los Angeles Times | date = 3 March 1928 | page = 11 }} The ship was reported docked in Los Angeles on 3 March 1928, still under the name ''West Conob''.</ref> the ship was renamed ''Golden Eagle'', the name under which she operated for the next six years.<ref name=Miramar /> ''Golden Eagle'' was sailing for Oceanic and Oriental from Los Angeles to Australia in March 1930, when the ''Los Angeles Times'' reported that she had sailed with {{convert|6700|LT|t}} of case oil and {{convert|200|LT|t}} of general merchandise.<ref name=LAT-case-oil />

In March 1934, Matson began a new "sugar, molasses and pineapple service" from Hawaii to San Francisco, Los Angeles, and either [[Philadelphia]] or New York, featuring ''Golden Eagle'' and three other cargo ships.<ref>{{Cite news| last = Drake | first = Waldo | title = New service opens today | work = Los Angeles Times | date = 19 March 1934 | page = A6 }}</ref><ref group=Note>The other three ships named were ''Mauna Ala'', ''General M.H. Sherman'', and ''Makiki''.</ref> In May, after returning from New York on her first voyage in the new service, ''Golden Eagle'' entered drydock at Los Angeles for general repairs and repainting. She emerged in Matson [[livery]] and with the new name of ''Mauna Loa''.<ref>{{Cite news| last = Drake | first = Waldo | title = Shipping news and activities at Los Angeles Harbor  | work = Los Angeles Times | date = 18 May 1934 | page = 19 }} The newspaper mistakenly reports that she would be renamed ''Mauna Ala'', a name already in use by another Matson ship. For another article listing the correct new name, see {{Cite news| last = Drake | first = Waldo | title = Shipping news and activities at Los Angeles Harbor | work = Los Angeles Times | date = 4 April 1934 | page = A12 }}</ref> She sailed on her maiden voyage under her new name to Honolulu with {{convert|4500|LT|t}} of general cargo in late May.<ref>{{Cite news| last = Cave | first = Wayne B. | title = Shipping news and activities at Los Angeles Harbor | work = Los Angeles Times | date = 25 May 1934 | page = 17 }}</ref> ''Mauna Loa'' continued on the Hawaii–California–Philadelphia/New York service, occasionally making extra voyages from Los Angeles to Honolulu when dictated by cargo bookings. One such extra voyage occurred in February 1936 when she carried almost a full load of building materials for family dwellings in Hawaii.<ref>{{Cite news| last = Drake | first = Waldo | title = Shipping news and activities at Los Angeles Harbor | work = Los Angeles Times | date = 25 February 1936 | page = A12 }}</ref>

In August 1936, ''Mauna Loa'' diverted to respond to a [[distress call]] issued by the [[windjammer]] ''Pacific Queen'' some {{convert|700|nmi|km}} southwest of Los Angeles. ''Pacific Queen'' had sailed from [[San Diego]] in July with a crew of 32—most of whom were [[Sea Scouting (Boy Scouts of America)|Sea Scouts]]—and had been missing for two weeks. ''Mauna Loa''{{'}}s crew provided required supplies for the sailing vessel and her radioed messages prompted the [[United States Coast Guard]] to recall all of its vessels actively searching for ''Pacific Queen''.<ref>{{Cite news| title = Aid given missing ship and sea hunt called off | work = Los Angeles Times | date = 24 August 1936 | page = A1 }}</ref>

On 18 November 1941, the [[United States Department of War|War Department]] [[ship chartering|chartered]] ''Mauna Loa'' and seven other ships to carry supplies to the [[Philippines]].<!-- date, sailing destination --><ref name=DANFS /><ref name=LAT-new>{{Cite news| last = Cave | first = Wayne B. | title = New group of freighters drafted for war service | work = Los Angeles Times | date = 20 November 1941 | page = 33 }}</ref><!-- number of ships --> Even though details of the charters were deemed confidential, the names of all eight ships were published in the ''Los Angeles Times'' two days later.<ref name=LAT-new /><ref group=Note>The other seven ships were {{SS|Iowan||2}}, {{SS|Portmar|1919|2}}, {{SS|West Camargo|1920|2}}, ''{{SS|Steel Voyager||2}}'', {{SS|Jane Christenson|1918|2}}, {{SS|F. J. Luckenbach|1917|2}}, and {{SS|Malama|1919|2}}.</ref>

==World War II==
Less than three weeks after ''Mauna Loa''{{'}}s charter, the [[Attack on Pearl Harbor|Japanese Attack on Pearl Harbor]] propelled the United States into [[World War II]]. ''Mauna Loa''{{'}}s movements over the next three months are unknown, but by mid-February 1942, she had made her way to [[Darwin, Northern Territory]], Australia.<ref name=Cressman-75>Cressman, p. 75.</ref>

[[Image:SS Mauna Loa at Darwin Harbour.jpg|thumb|left|''Mauna Loa'' ''(middle)'' is seen in Darwin Harbour in February 1942 with [[sloop-of-war|sloops]] {{HMAS|Swan|U74|6}} ''(left)'' and {{HMAS|Warrego|U73|6}}.]]
Japanese forces—advancing down the [[Malay Barrier]], the notional [[Allies of World War II|Allied]] line of defense that ran down the [[Malayan Peninsula]] through Singapore and the southernmost islands of the [[Dutch East Indies]]—had reached the island of [[Timor]] by mid February. In order to prevent the fall of that island to the Japanese, which would give them a base within {{convert|400|mi}} of Darwin, the Allies assembled a joint American-Australian force to reinforce the Australian [[Sparrow Force]] and [[Royal Dutch East Indies Army]] forces defending Timor.<ref name=Feuer-6>Feuer, p. 6.</ref>

The American cruiser {{USS|Houston|CA-30|2}} and destroyer {{USS|Peary|DD-226|2}}, and the Australian [[sloop-of-war|sloops]] {{HMAS|Swan|U74|2}} and {{HMAS|Warrego|U73|2}}, led ''Mauna Loa'' and three other civilian ships out of Darwin Harbour at about 03:00 on 15 February heading for [[Koepang]] with relief intended for Timor.<ref name=Tolley>Tolley, p. 315.</ref>  ''Mauna Loa'', loaded with 500 men,<ref name=Cressman-75 /> and [[United States Army]] transport ship {{USAT|Meigs||2}} carried an Australian [[infantry battalion]] and an [[antitank]] unit between them.<ref name=Feuer-6 /><ref group=Note>{{USAT|Meigs}}, formerly named ''West Lewark'', was—like ''Mauna Loa''—built by the [[Los Angeles Shipbuilding & Dry Dock Company]]. See: {{Cite web| last = Colton | first = Tim | title = Todd Pacific Shipyards, San Pedro CA | url = http://www.shipbuildinghistory.com/history/shipyards/2large/inactive/toddsanpedro.htm | work = Shipbuildinghistory.com | publisher = The Colton Company | accessdate = 23 September 2008 }}</ref> The British refrigerated cargo ship {{MV|Tulagi||2}} and the American cargo ship {{SS|Portmar|1919|2}} carried the [[148th Field Artillery Regiment]] of the [[Idaho National Guard]] between them.<ref name=Feuer-6 /><ref>{{Cite web| url = http://www.miramarshipindex.org.nz/ship/show/241097 | title = Tulagi | work = Miramar Ship Index | publisher = R.B.Haworth | accessdate = 23 September 2008 }}</ref><ref group=Note>{{SS|Portmar|1919|2}} is sometimes referred to as ''Port Mar'' in sources describing this convoy and the subsequent attack on Darwin.</ref>

The ships were spotted by a Japanese [[Kawanishi H6K|Kawanishi H6K "Mavis"]] four-engined flying boat that tailed the convoy at {{convert|10000|ft|m}}.<ref name=Feuer-6 /> When Captain [[Albert H. Rooks]] of ''Houston'' requested [[air cover]] for the convoy,<ref name=Morison>Morison, p. 314.</ref> a lone [[Curtiss P-40]] responded and engaged the Mavis, with each plane managing to shoot down the other.<ref name=Feuer-7>Feuer, p. 7.</ref> At around 09:00 the next day, another Mavis began trailing the convoy and at 11:00, 36 land-based [[Mitsubishi Ki-21|Mitsubishi Ki-21 "Sally"]] twin-engine bombers and ten [[seaplane]]s attacked in two waves.<ref name=ONIJSC>{{cite book |last1=Office of Naval Intelligence – United States Navy |year=1943 |title=The Java Sea Campaign |series=Combat Narratives |location=Washington, DC |publisher= United States Navy |isbn= |lccn=2009397493 |pages=36–39 |url=http://www.history.navy.mil/library/online/javasea_campaign.htm |accessdate=26 August 2013 |ref=harv}}</ref> ''Houston'', the primary target of the bombers, unleashed all of her available antiaircraft fire with neither bombs nor ''Houston''{{'}}s fire being effective.<ref name=ONIJSC /> In the second wave, from the southwest and after the ships had scattered, ''Houston'' shot down seven of forty-four planes and repelled the attacking aircraft.<ref name=Cressman-75 /><ref name=ONIJSC /> ''Houston''{{'}}s 900 rounds fired in the 45-minute attack resembled a "sheet of flame", according to witnesses.<ref name=Cressman-75 /><ref name=Feuer-6 /> The only casualties during the attack were from one near miss on ''Mauna Loa''; 1 crewman and 1 passenger were killed and 18 men were wounded in the attack.<ref name=Cressman-75 /> The convoy was ordered back to Darwin when word that Koepang had fallen to the Japanese was received; she arrived back in Darwin on 18 February.<ref name=Tolley />

===Sinking===
{{Main|Bombing of Darwin (February 1942)}}
[[Image:SS Mauna Loa on fire before sinking.jpg|thumb|right|''Mauna Loa'' burns before sinking on 19 February 1942 in Darwin Harbour.]]
On 19 February 1942, the Japanese carrier striking force, consisting of aircraft carriers [[Japanese aircraft carrier Akagi|''Akagi'']], [[Japanese aircraft carrier Kaga|''Kaga'']], [[Japanese aircraft carrier Hiryu|''Hiryu'']], and [[Japanese aircraft carrier Soryu|''Soryu'']] under the command of Admiral [[Chuichi Nagumo]], launched 189 planes to attack Darwin.<ref name=Cressman-76>Cressman, p. 76</ref> The carrier planes rendezvoused with 54 land-based bombers from [[Kendari]] and [[Ambon Island|Ambon]].<ref>Morison, p. 316.</ref>

At the time of the raid the ''Mauna Loa'' and ''Meigs'' had unloaded troops and moved to anchorages with the force's equipment and ammunition aboard with {{MV|Neptuna||2}} and {{SS|Zealandia|1910|2}} unloading ammunition at the docks that were the first target of high altitude bombers. Both ships at the dock were hit with ''Neptuna'' exploding. After a second wave of bombers, concentrating on the airport, came waves of dive bombers that for two hours concentrated on ships in the harbor.<ref name=ONIJSC />

During the attack, ''Mauna Loa'' quickly sank after she was hit by two bombs that landed in an open [[hatch (nautical)|hatch]].<ref>Morison, p. 319.</ref> None of her 37-man crew or 7 passengers was injured.<ref name=Cressman-76 /> Along with  ''Mauna Loa'', two other American ships, destroyer ''Peary'' and Army transport ''Meigs'', were sunk. In addition to the many other ships that were damaged, five [[Commonwealth of Nations|Commonwealth]] ships were sunk, including two Australian passenger ships in use as troopships, ''Neptuna'' and ''Zealandia''. The total death toll for the attack was around 250; of the total, 157 died on ships.<ref>Swain, pp. 136–37.</ref>

<!-- After the war, a Japanese salvage firm was awarded the contract for salvaging the remains of ''Mauna Loa'' and the other wrecks in the harbor, but were prohibited from removing any of the American-owned cargo still remaining.<ref name=DDC /> -->What remains of ''Mauna Loa'' lies in Darwin Harbour at position {{Coord|12|29.86|S|130|49.16|E|display=inline,title|type:landmark_region:AU-NT}} at a depth of {{convert|60|ft|m}},<!-- position --><ref>{{Cite web| title= World War II Shipwrecks | publisher= Northern Territory Government, Australia | url=http://www.ntlis.nt.gov.au/heritageregister/f?p=103:302:2181777911322216::NO::P302_SITE_ID:770 | accessdate = 20 February 2015}}</ref><ref name=DDC>{{Cite web| title = WWII Wrecks | url = http://www.darwindivecentre.com.au/wwii%20wrecks.html | publisher = Darwin Dive Centre | accessdate = 23 September 2008 }}</ref><!-- depth --> and is a [[wreck diving|dive site]].<ref name=CM>Coleman and Marsh, p. 72.</ref> Military trucks, [[Universal Carrier|Bren Gun Carriers]], a [[Harley-Davidson WLA|Harley-Davidson motorcycle]], and many rounds of [[.303 British|.303-]] and [[.50 BMG|.50-caliber]] ammunition are among the pieces of ''Mauna Loa''{{'}}s cargo that still lie strewn about the wreck.<ref name=DDC/><ref name=CM />

==Notes==
{{Reflist|group=Note}}

==References==
{{Reflist|30em}}

==Bibliography==
{{Refbegin}}
* {{Cite book| last = Andros | first = Stephen Osgood  | title = Fuel oil in industry | location = Chicago | publisher = The Shaw Publishing Company | year = 1920 | oclc = 4013194 }}
* {{Cite book| last = Coleman | first = Neville |author2=Nigel Marsh | title = Diving Australia: A Guide to the Best Diving Down Under | location = Singapore | publisher = Periplus Editions | year = 2003 | edition = New | isbn = 978-962-593-311-5 | oclc = 61175221 }}
* {{Cite book| last = Cressman | first = Robert J. | url = http://www.ibiblio.org/hyperwar/USN/USN-Chron/| title = The Official Chronology of the U.S. Navy in World War II | chapter = Chapter IV: 1942 | chapterurl = http://www.ibiblio.org/hyperwar/USN/USN-Chron/USN-Chron-1942.html | location = [[Annapolis, Maryland]] | publisher = [[Naval Institute Press]] | year = 2000 | isbn = 978-1-55750-149-3 | oclc = 41977179 | accessdate = 23 September 2008 }}
* {{Cite book| last = Crowell  | first = Benedict  | authorlink = Benedict Crowell |author2=Robert Forrest Wilson | title = The Road to France: The Transportation of Troops and Military Supplies, 1917–1918 | series = How America Went to War: An Account From Official Sources of the Nation's War Activities, 1917–1920 | location = [[New Haven, Connecticut|New Haven]] | publisher = [[Yale University Press]] | year = 1921 | oclc = 18696066 }}
* {{Cite book| last = Eliasson | first = Rolf E. |author2=Lars Hannes Larsson | title = Principles of Yacht Design | location = [[Camden, Maine]] | publisher = International Marine | year = 2000 | edition = 2nd | isbn = 978-0-07-135393-9 | oclc = 44884292 }}
* Enright, Francis James, To Leave This Port, Orick, California: Enright Publishing Company, 1990.
* {{Cite book| last = Feuer | first = A. B. | title = Australian Commandos: Their Secret War Against the Japanese in World War II | location = [[Mechanicsburg, Pennsylvania]] | publisher = Stackpole | year = 2006 | origyear = 1996 | edition = 1st | isbn = 978-0-8117-3294-9 | oclc = 221269808 }}
* {{Cite book| last = Jordan | first = Roger W. | title = The World's Merchant Fleets, 1939: The Particulars And Wartime Fates of 6,000 Ships | location = Annapolis, Maryland | publisher = Naval Institute Press | origyear = 1999 | year = 2006 | isbn = 978-1-59114-959-0 | oclc = 150361480 }}
* {{Cite book|  last = Morison | first = Samuel | authorlink = Samuel Eliot Morison | title = [[History of United States Naval Operations in World War II]], Volume 3: The rising sun in the Pacific, 1931 – April 1942 | location = [[Urbana, Illinois|Urbana]] | publisher = [[University of Illinois Press]] | year = 2001 | origyear = 1948 | isbn = 978-0-252-06973-4 | oclc = 45243342 }}
* {{Cite DANFS | author = [[Naval Historical Center]] | title = West Conob | url = http://www.history.navy.mil/danfs/w5/west_conob.htm | accessdate = 23 September 2008 }}
* {{Cite DANFS | author = Naval Historical Center | title = West Grama | url = http://www.history.navy.mil/danfs/w5/west_grama.htm | accessdate = 23 September 2008 | link = off }}
* Sharp, Gregory. No Greater Sacrifice: Matson Lines' Unsung WWII Casualties.  Sea Classics. Vol. 38. Iss. 3. March 2005. Page Number: 26+ –
* Stindt, Fred A. Matson's Century of Ships, Modesto, California,: Fred A Stindt, 1982
* {{Cite book| last = Swain | first = Bruce T. | title = A Chronology of Australian Armed Forces at War 1939–45 | location = [[Crows Nest, New South Wales]] | publisher = [[Allen & Unwin]] | year = 2001 | isbn = 978-1-86508-352-0 | oclc = 47043750 }}
* {{Cite book| last = Tolley | first = Kemp | title = Cruise of the Lanikai: Incitement to War | location = Annapolis, Maryland | publisher = Naval Institute Press | year = 2000 | origyear = 1973 | isbn = 978-1-55750-406-7 | oclc = 49698840 }}
{{Refend}}

==External links==
{{Commons category|SS Mauna Loa}}
* {{navsource|12/174033|West Conob (ID 4033)}}

{{Featured article}}

 <!-- non-breaking space to keep AWB drones from altering the space before the navbox-->

{{Design 1013 ships}}
{{Los Angeles SB&DDC and Todd, Los Angeles ships}}
{{February 1942 shipwrecks}}
{{Use dmy dates|date=August 2014}}

{{DEFAULTSORT:Mauna Loa}}
[[Category:Design 1013 ships]]
[[Category:Ships built in Los Angeles]]
[[Category:1919 ships]]
[[Category:World War II merchant ships of the United States]]
[[Category:World War II auxiliary ships of the United States]]
[[Category:Ships sunk in the bombing of Darwin, 1942]]
[[Category:Maritime incidents in February 1942]]
[[Category:Wreck diving sites]]