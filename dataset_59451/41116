{{About|the American educator|the British film director|Arthur B. Woods|the New Zealand rugby union player|Arthur Woods (rugby union)}}
{{Infobox officeholder
|name          = Arthur Woods
|image         = Arthur Woods 01.jpg
|caption       = Woods circa 1920-1930
|birth_name    = Arthur Hale Woods
|birth_date    = {{birth date|1870|1|29}}
|birth_place   = [[Boston, Massachusetts]], [[United States]]
|death_date    = {{death date and age|1942|5|12|1870|1|29}} 
|death_place   = [[Washington, D.C.]] 
|death_cause   = [[Cerebral hemorrhage]]
|resting_place = [[family cemetery]] in [[Ipswich, Massachusetts]] 
|resting_place_coordinates = 
|residence     = 
|nationality   = [[United States|American]]
|other_names   = 
|education     =  
|alma_mater    = [[Harvard University]]<br/>[[University of Berlin]]
|employer      = 
|occupation    = Educator, journalist, military and law enforcement officer
|home_town     = [[New York City]], [[New York]]
|title         = 
|salary        = 
|networth      = 
|height        = 
|weight        = 
|term          = 
|party         = [[Republican Party (United States)|Republican]]
|boards        = 
|religion      = [[Episcopal Church (United States)|Episcopalian]]
|spouse        = [[Helen Morgan Hamilton]]
|partner       = 
|children      = John Pierpont Woods<br/>Leonard Hamilton Woods<br/>Alexander Hamilton Woods<br/>Caroline Frances Woods
|parents       = Joseph Wheeler and Caroline Frances Woods
|relations     = J. H. Woods, brother 
|signature     = 
|website       = 
|order= |office=[[New York City Police Commissioner]]|term_start=1914|term_end=1918|appointed=[[John Purroy Mitchel]]|predecessor=[[Douglas Imrie McKay]]|successor=[[Frederick Hamilton Bugher]]}}
[[File:Mayor Mitchell and General Leonard Wood review 30th Infantry.jpg|thumb|Arthur Woods and [[John Purroy Mitchel]] and [[Leonard Wood]] in 1915]]
Colonel '''Arthur Hale Woods''' (January 29, 1870 – May 12, 1942) was an American educator, journalist, military and law enforcement officer. One of the most prominent police reformers during the early 20th century, he served as deputy [[New York City Police Commissioner]] from 1907 to 1909 and later became New York City Police Commissioner in 1914. During his time with the [[New York City Police Department]], he was largely responsible for initiating the application of [[criminology]] and [[sociology]] in modern policing.<ref>[http://tierwriting.com/2010/03/arthur-woods/ Arthur Woods Biography]</ref>

In his later years, Woods worked with the [[Division of Military Aeronautics]] and was involved in government committees on unemployment under the administrations of Presidents [[Warren G. Harding]] and [[Herbert Hoover]]. Woods was also an important [[public servant]] as [[trustee]] for the [[Board of Education]] and presided as president and chairman of the board of [[Rockefeller Center]].


==Biography==
Arthur Woods was born in [[Boston, Massachusetts]] on January 29, 1870, to Joseph Wheeler and Caroline Frances Woods. He attended [[Harvard University]], graduating in 1892, and did his post-graduate work at the [[University of Berlin]] before becoming a schoolmaster at [[Groton School]] in 1895.<ref name="Article"/><ref name="Article2"/> One of his students, [[Franklin D. Roosevelt]], later became elected to the [[presidency of the United States]]. In 1905, he accompanied [[William Howard Taft]], [[Nicholas Longworth]], and [[Alice Roosevelt Longworth|Alice Roosevelt]] to the [[Philippines]] and then continued alone traveling the world for another year before returning to the United States.<ref name="Article"/>

Leaving Groton after a decade of service, his interest in [[sociology]] led him to become a reporter for the ''[[New York Evening Sun]]'' for $15 a week. He became interested in police work while working as a reporter and soon gained the attention of the Police Commissioner [[Theodore A. Bingham]], who liked his ideas for reforming the police system, and led to his appointment as deputy police commissioner of the [[New York City Police Department]] in 1907.<ref name="Article"/><ref name="Article2"/>

Woods was responsible for instituting better police training by introducing an official [[police academy]] modeled after [[Scotland Yard]],<ref name="Article2"/> which taught courses on law and sociology as well as [[physical training]].<ref name="Article"/> A staunch advocate of [[community policing]], Woods believed that the common rank-and-file policeman should be in a position of social importance and [[public value]]. He also felt that the public would benefit if they were more informed of issues affecting the community and the local police precinct. As part of that system, he published the first safety booklet available to the general public.<ref>Moreno, Barry. ''Manhattan Street Scenes''. Charleston, South Carolina: Arcadia Publishing, 2006. (pg. 8, 68) ISBN 0-7385-4506-6</ref>

During his time as deputy commissioner, Woods became well educated on gang related violence and was a supporter of Inspector [[Joseph Petrosino]] and the "Italian Squad", a special detectives unit which combated [[organized crime]] in [[Italian-American]] neighborhoods. He was partially responsible for its revival following Petrosino's murder in 1908, although the squad remained more low-profile then its previous incarnation. It was not officially reinstated for another decade.<ref name="Reppetto">Reppetto, Thomas A. ''American Mafia: A History of Its Rise to Power''. New York: Henry Holt & Co., 2004. (pg. 75-76, 82) ISBN 0-8050-7798-7</ref> A year later, he left the NYPD and went into the lumber business in [[Mexico]] and the cotton converting business in Boston before returning to the police force five years later.<ref name="Article"/>

As part of newly elected Mayor [[John Purroy Mitchel]]'s reform campaign, Woods succeeded [[Douglas I. McKay]] as [[New York City Police Commissioner]] in April 1914. Continuing his predecessor's efforts against the many street gangs operating in the city at that time, breaking up gangs in a systematic sweep from the Battery to Spuyten Duyvil which concluded with the [[Hudson Dusters]] two years later, he also became involved in labor racketeering during the "[[Labor Slugger War]]". Working with District Attorney [[Charles A. Perkins]], he was responsible for the arrests of over 200 known criminals during his first year in office.<ref>[[Herbert Asbury|Asbury, Herbert]]. ''The Gangs of New York: An Informal History of the New York Underworld''. New York: Alfred A. Knopf, 1928. (pg. 343-344) ISBN 1-56025-275-8</ref> On June 10, 1916, Woods married [[Helen Morgan Hamilton]], granddaughter of industrialist [[J.P. Morgan]], and retired from policing two years later.<ref name="Article"/><ref name="Article2"/> His removal as police commissioner was a direct result of Mitchel's election loss to [[Tammany Hall]]-backed [[John Hylan]], a Brooklyn judge, in 1917.<ref name="Reppetto"/>

That same year, he became an assistant director on the [[Committee of Public Information]] on [[propaganda|foreign propaganda]]. In early 1918, he was promoted to [[Lieutenant colonel (United States)|lieutenant colonel]] and assigned to the [[Division of Military Aeronautics]] for the [[United States War Department]].<ref name="Article2"/> He became a full [[colonel]] in August and, after a 3-month tour with the [[American Expeditionary Force]] overseas, he was appointed assistant director of military aeronautics.<ref name="Article"/> In 1920, he was awarded medals from three different countries: the [[Distinguished Service Medal (U.S. Army)|Distinguished Service Medal]] (U.S.), [[Order of St Michael and St George|Order of St. Michael and St. George]] (U.K.) and the [[Chevalier of the Legion of Honor]] (France).<ref name="Article3">"Harvard Man May Land Chief Prohibition Post". <u>The Harvard Crimson.</u> 17 Jan 1925</ref>

Following the end of [[World War I]], he became an assistant to the [[United States Secretary of War]] [[Newton D. Baker]] in charge of efforts to reestablish returning American servicemen in civil life. From 1921 to 1922, he presided as chairman on Committee on Civic and Emergency Measures as part of President [[Warren G. Harding]]'s [[1921 recession#Conference on unemployment|Conference on Unemployment]]. In 1925, he was considered to succeed Roy A. Haynes as national prohibition commissioner.<ref name="Article3"/> Woods also served as [[trustee]] for the [[Board of Education]] and presided as president and chairman of the board of [[Rockefeller Center]]. In addition, he assisted [[John D. Rockefeller]] in the restoration of historic [[Williamsburg, Virginia]], a near four-year project lasting from 1927 until 1931. In the early years of the [[Great Depression]], he was the chairman of the President [[Herbert Hoover]]'s Committee on Employment.<ref name="Article"/><ref name="Article2"/>

In 1937, he retired from public life due to ill health and settled in [[Washington, D.C.]] where he lived with his wife for several years. Woods died from a [[cerebral hemorrhage]] at his North Street home on May 12, 1942.<ref name="Article">{{cite news |author= |agency= |title=Arthur Woods, 72, Is Dead In Capital. Police Commissioner Here in 1914 to '18 Introduced New Methods of Enforcement. Air Colonel With The A.E.F.; Sociologist, Former Reporter, Taught Roosevelt at Groton. Wed Late J.P. Morgan Kin |url=https://query.nytimes.com/gst/abstract.html?res=9B05E3D8153CE33BBC4B52DFB3668389659EDE |quote= |newspaper=[[New York Times]] |date= |accessdate=2016-01-09 }}</ref><ref name="Article2">"Col. Arthur Woods Succumbs at 72". <u>Evening Independent.</u> 13 May 1942</ref> His funeral was held at [[St. John's Episcopal Church, Lafayette Square (Washington, D.C.)|St. John's Episcopal Church]] in [[Georgetown, Washington, D.C.|Georgetown]] and was later buried in the [[family cemetery|Woods family cemetery]] in [[Ipswich, Massachusetts]].<ref name="Article"/> Arthur Hale Woods was buried at Arlington National Cemetery.<ref>[http://www.arlingtoncemetery.net/adwoods.htm Arlington National Cemetery]</ref>

==Bibliography==
*''Crime Prevention'' (1918)
*''Policeman and Public'' (1919)
*''Dangerous Drugs'' (1930)

==See also==
{{Portal|Biography|United States Army}}

==References==
{{Reflist}}

==Further reading==
*Palmiotto, Michael. ''Community Policing: A Policing Strategy for the 21st Century''. Gaithersburg, Maryland: Aspen Publishers, 2000. ISBN 0-8342-1087-8
*Ronnie, Art. ''Counterfeit Hero: Fritz Duquesne, Adventurer and Spy''. Annapolis: Naval Institute Press, 1995. ISBN 1-55750-733-3

==External links==
* {{Internet Archive author |sname=Arthur Hale Woods}}
*[http://www.arlingtoncemetery.net/adwoods.htm Arlington National Cemetery Website: Arthur Hale Woods]
*[http://findingaids.loc.gov/db/search/xq/searchMfer02.xq?_id=loc.mss.eadmss.ms007032&_faSection=overview&_faSubsection=did&_dmdid= Arthur Woods Papers (Library of Congress)]

{{S-start}}
{{s-civ|pol}}
{{Succession box |
  title=[[New York City Police Commissioner]]|
  before=[[Douglas I. McKay]] |
  after=[[Frederick H. Bugher]] |
  years=1914-1918}}
{{S-end}}

{{Authority control}}

{{DEFAULTSORT:Woods, Arthur}}
[[Category:1870 births]]
[[Category:1942 deaths]]
[[Category:American educators]]
[[Category:American male journalists]]
[[Category:Commissioners of the New York City Police Department]]
[[Category:Writers from Boston]]
[[Category:Journalists from Washington, D.C.]]
[[Category:Harvard University alumni]]
[[Category:United States Army officers]]
[[Category:Chevaliers of the Légion d'honneur]]
[[Category:Burials at Arlington National Cemetery]]
[[Category:American military personnel of World War I]]
[[Category:Massachusetts Republicans]]
[[Category:New York Republicans]]
[[Category:Washington, D.C. Republicans]]