{{Infobox Journal
| title        = Biomedical Microdevices
| cover        = [[File:BiomedMicrodev cover.jpg]]
| editor       = Mauro Ferrari
| discipline   = [[Engineering]], [[Physics]]
| language     = [[English language|English]]
| abbreviation = Biomed Microdevices 
| publisher    = [[Springer Science+Business Media|Springer]]
| country      = [[Germany]]
| frequency    = 6/year
| history      = 1998–present
| openaccess   =
| impact       = 2.924 (2008)
| website      = http://www.springer.com/journal/10544/
| link1        = http://www.springerlink.com/content/1387-2176
| link1-name   = online access
| link2        =
| link2-name   = 
| RSS          = http://www.springerlink.com/content/1387-2176?sortorder=asc&export=rss
| atom         =
| JSTOR        =
| OCLC         =
| LCCN         =
| CODEN        =
| ISSN         = 1387-2176
| eISSN        = 1572-8781
}}

'''''Biomedical Microdevices''''' is a bimonthly scientific journal publishing articles on the applications of [[BioMEMS]] (Microelectromechanical systems) and biomedical [[nanotechnology]].

Its editor-in-chief is Mauro Ferrari of the [[University of Texas]], Health Science Center, Houston.

''Biomedical Microdevices'' has an [[impact factor]] of 2.924 (2008).<ref>{{Citation | title = Web of Science | year = 2009 | url = http://isiwebofknowledge.com | accessdate = 2009-10-09}}</ref>

== Indexing ==

Among others, the journal is abstracted/indexed in: [[Chemical Abstracts Service]] (CAS), [[EMBASE]], [[PubMed]] and [[Scopus]].<ref>{{cite web | url= http://www.springer.com/engineering/biomedical+eng/journal/10544?detailsPage=description | title=Biomedical Microdevices(Description) |accessdate=2009-10-09}}</ref>
.

== References ==

{{reflist}}

[[Category:English-language journals]]
[[Category:Physics journals]]
[[Category:Engineering journals]]
[[Category:Publications established in 1998]]
[[Category:Medical journals]]


{{engineering-journal-stub}}
{{med-journal-stub}}