{{Infobox civilian attack
| title      =2008 Khurcha incident
| image      = 
| caption    = 
| location   =Khurcha, Zugdidi district, [[Georgia (country)|Georgia]], Near the border with the internationally unrecognised [[Republic of Abkhazia]]
| coordinates = 
| date       =May 21, 2008
| time       = 
| timezone   = 
| type       =Shooting
| fatalities =None
| injuries   =3 people
| perps      = 
| perp       = 
| susperps   =Georgian authorities
| susperp    = 
| weapons    =Small arms, Rocket-propelled grenades
}}

The '''2008 Khurcha incident''' refers to the May 21, 2008 attack on two minibuses in the [[Georgia (country)|Georgian]] village of Khurcha, near the ceasefire line with the internationally unrecognised [[Republic of Abkhazia]]. The buses were carrying Georgians on the way to vote at the parliamentary election which was underway that day. 3 people were injured. During the high profile TV coverage later that day, and while voting booths were open, Georgian officials claimed that the attack was carried out by Abkhaz forces. Several investigations later showed that the incident was most likely staged by unknown Georgian servicemen.<ref name=hridc0524/><ref name=geotimes0804/>

==The attack==

The attack took place on May 21, 2008, the day of the [[Georgian legislative election, 2008|Georgian Parliamentary elections]] on a football field in the village of Khurcha. Two minibuses carrying voters from Abkhazia's [[Gali District, Abkhazia|Gali district]] arrived at the football field, where they came under attack by small arms fire and grenades. 3 people had to be hospitalised in [[Zugdidi]], one of whom was seriously injured.

==Reactions==

Georgia accused the Abkhazian side to have carried out the attack with Russian support. The Abkhazian side denied this and pointed out that Khurcha, the site of the incident, was well within Georgian-controlled territory. 

==Investigations==

On May 22, the Norwegian Helsinki Committee and the Human Rights Centre of Georgia carried out an investigation on the attack. They found out, that the buses had directly come to the football field, rather than to the polling station. Furthermore, media were already present at the scene, before the incident took place. Georgian security troops were immediately present at the scene of the incident, even though Khurcha lies within the demilitarised zone, the nearest base being 15 minutes away. The investigators further found out that the attack originated from a position only 100 meters from the Georgian side, that is, the side opposite to the ceasefire line. Local eyewitnesses all stated, that they believed that the attack had been staged by the Georgian side.<ref name=hridc0524>{{cite web
 |url=http://www.humanrights.ge/index.php?a=article&id=2754&lang=en
 |title=Questions Raised by the Khurcha  Incident
 |publisher=Human Rights Information and Documentation Center
 |date=2008-05-24
 |accessdate = 2008-08-21
}}</ref>

Another investigation was carried out by the Reporter studio in [[Tbilisi]], which made a documentary about the incident. It stated directly, that the attack was staged by the Georgian side. Before the busses were hit by grenades, the camera, that would record this, had been already positioned on a tripod and was already recording the buses. Local residents reported, that they had been asked to come to the football field for a video shoot, without voting being mentioned.<ref name=geotimes0804>{{cite web
 |url=http://www.geotimes.ge/index.php?m=home&newsid=11796
 |title=UN Blames Georgians For Khurcha Incident
 |publisher=The Georgian Times
 |date=2008-08-04
 |accessdate = 2008-08-21
| archiveurl= https://web.archive.org/web/20080820040014/http://www.geotimes.ge/index.php?m=home&newsid=11796| archivedate= 20 August 2008 <!--DASHBot-->| deadurl= no}}</ref>

The [[United Nations Observer Mission in Georgia]]'s investigation into the incident found that the attackers were located on the Georgian side of the ceasefire line, about 100m from the buses, and that although hard evidence of the attackers' identities was lacking, inconsistencies merited further investigation, particularly the suggestion that the filming of the attack seemed anticipatory.<ref name=unsc0723>{{cite web
 |url=http://daccess-dds-ny.un.org/doc/UNDOC/GEN/N08/427/22/PDF/N0842722.pdf?OpenElement
 |title=Report of the Secretary-General on the situation in Abkhazia, Georgia
 |publisher=[[United Nations Security Council]]
 |date=2008-07-23
 |accessdate = 2008-08-21
}}</ref>

In October 2013, under the new government of Georgia, two former Georgian security officers, Roman Shamatava and Malkhaz Murgulia, were arrested on charges of terrorism and exceeding of official powers; prosecutors alleged the Khurcha attack was masterminded by the Georgian security agencies with the purpose of "terrorizing peaceful population on the election day". Murgulia was soon released on bail and Shamatava was acquitted of the charges of terrorism, but ruled that he "exceeded his official powers with use of arms and violence" in July 2014.<ref>{{cite news|title=Verdict Delivered in Trial over 2008 Polling Day Blast in Khurcha|url=http://civil.ge/eng/article.php?id=27533|accessdate=31 July 2014|agency=Civil Georgia|date=30 July 2014}}</ref>

==References==
{{Reflist|1}}

{{Georgian-Abkhazian conflict}}

{{DEFAULTSORT:Khurcha Incident, 2008}}
[[Category:Georgian–Abkhazian conflict]]
[[Category:2008 in Georgia (country)|Khurcha Incident, 2008]]
[[Category:False flag operations]]