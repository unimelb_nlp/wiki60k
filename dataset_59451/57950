{{Infobox Journal
| cover	=	[[Image:JBScover.gif]]
| discipline	=	[[History of the British Isles]]
| editor = Brian Cowan<br>Elizabeth Elbourne
| abbreviation	=	J. Br. Stud.
| publisher	=	[[Cambridge University Press]]
| country	=	United States
| frequency	=	Quarterly
| history	=	1961-present
| website	=	http://journals.cambridge.org/action/displayJournal?jid=JBR
| impact = 0.521
| impact-year = 2009
| ISSN	=	0021-9371
| JSTOR = 00219371
| OCLC = 1783237
| LCCN = 67001623
}}

The publication of the [http://www.nacbs.org/ North American Conference on British Studies], '''''The Journal of British Studies''''' is an [[academic journal]] aimed at scholars of British culture from the Middle Ages through the present. The journal was co-founded in 1961 by [[George B. Cooper (historian)|George Cooper]]. JBS presents scholarly articles and book reviews from renowned international authors who share their ideas on [[British society]], [[politics]], [[law]], [[economics]], and the [[arts]].  Until 2005 it covered subjects from the medieval period to the present, while ''Albion'' (another journal published by the NACBS) covered all periods of British history.  ''Albion'' was merged into the JBS as of vol. 44 in 2005.
Until October 2012 the journal was published by [[University of Chicago Press]]. From volume 52 onwards it will be published by [[Cambridge University Press]].

== See also ==
* [[George B. Cooper (historian)]]
* [[Historiography of the United Kingdom]]

== References ==
* [http://www.jstor.org/journals/00219371.html JSTOR Reference]
{{reflist}}

== External links ==
* [http://journals.cambridge.org/action/displayJournal?jid=JBR ''Journal of British Studies'' homepage]

[[Category:European studies journals]]
[[Category:University of Chicago Press academic journals]]
[[Category:Publications established in 1961]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:European history journals]]


{{area-journal-stub}}