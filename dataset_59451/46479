{{Use dmy dates|date=May 2013}}
{{Use British English|date=May 2013}}
{{Infobox writer <!-- for more information see [[:Template:Infobox writer/doc]] -->
|name = Mackenzie Bell
|image = Mackenzie Bell 001.jpg
|caption = Mackenzie Bell 1890
|birth_name = Henry Thomas Mackenzie Bell
|pseudonym = Mackenzie Bell
|birth_date = {{Birth date|df=y|1856|3|2}}
|birth_place = [[Liverpool]], [[England]], U.K.
|death_date = {{Death date and age|df=y|1930|12|13|1856|3|2}}
|death_place = [[London]], England
|occupation = Writer, poet, critic, journalist, [[lecture]]r
|nationality = British
|genre = [[Fiction]], [[poetry]], [[non-fiction]], [[biographies]], [[essay]], [[literary criticism]]
|influences =
|influenced =
|signature =
|notableworks = ''A Forgotten Genius: Charles Whitehead'', ''Christina Rossetti: A Biographical and Critical Study''
|spouse=
|children=
}}
'''Henry Thomas Mackenzie Bell''' (2 March 1856 – 13 December 1930), commonly known by his pen name '''Mackenzie Bell''', was an English writer, poet and literary critic. He was a writer for many [[Victorian era]] publications, most especially the ''[[London Academy]]'', and published several volumes of poetry between 1879 and 1893.

A noted world traveller, he was acquainted with many literary figures in Victorian Britain and abroad. He was a personal friend of [[Christina Rossetti]] and authored her biography, as well as those of fellow English poets [[Algernon Charles Swinburne|Algernon Swinburne]] and [[Charles Whitehead]], and published critical studies of their literary work. He also contributed biographies to the [[Dictionary of National Biography]].

A staunch [[The Relugas Compact#Liberal Imperialists and the Liberal League|Liberal Imperialist]], Bell was a charter member of [[W.E. Forster]]'s Imperial Federation Committee, lectured for the Social and Political Education League and on four occasions contested St George Hanover Square on behalf of the [[Liberal Party (UK)|Liberal Party]]. He was also a member of the [[Athenaeum Club, London|Athenaeum]] for many years.

==Biography==
Henry Thomas Mackenzie Bell was born at 8 Falconer Square, [[Liverpool, England]] on 2 March 1856, the youngest child of merchant Thomas Bell and Margaret Mackenzie. His uncle was the Scottish judge and Solicitor-General for Scotland Lord Thomas Mackenzie.<ref name="Peattie">Peattie, Roger W., ed. ''Selected Letters of William Michael Rossetti and Christina Rossetti''. University Park: Pennsylvania State University Press, 1990. (pg. 578) ISBN 0-271-00678-1</ref> Bell suffered from poor health as a child, a fall resulting from a careless nurse having caused a minor [[stroke|paralytic stroke]], and he was educated privately. Though he was trained in preparation for a career in law at [[Cambridge University]], Bell instead chose to study abroad and lived in [[Portugal]], [[Spain]], [[Italy]], [[France]] and [[Madeira]].<ref name="Kunitz">Kunitz, Stanley and Howard Haycraft, eds. ''Twentieth Century Authors: A Biographical Dictionary of Modern Literature''. Vol. 2. New York: H. W. Wilson, 1973. (pg. 107) ISBN 0-8242-0049-7</ref> During his years as a world traveller, he became close friends with [[Christina Rossetti]] and later wrote her biography after her death.<ref name="Plarr">Plarr, Victor G. ''Men and Women of the Time: A Dictionary of Contemporaries''. 15th ed. London: George Routledge & Sons, 1899. (pg. 79-80)</ref><ref name="Article">"Obituary: Mr. Mackenzie Bell." ''The Times''. 15 December 1930: 8.</ref><ref name="Reilly">Reilly, Catherine W. ''Mid-Victorian Poetry, 1860-1879: An Annotated Biobibliography''. London and New York: Mansell, 2000. (pg. 36) ISBN 0-7201-2318-6</ref> While still a young man, he published his first poetry books ''The Keeping of the Vow and Other Verses'' (1879), ''Verses of Varied Life'' (1882) and ''Old Year Leaves'' (1883).

In 1884, Bell returned to Great Britain and settled in [[Ealing, London]] as a professional writer. That same year, he published a well-received biography on [[Charles Whitehead]] entitled ''A Forgotten Genius'' (1884).<ref name="Kunitz"/><ref name="Chambers">Chambers, Robert and David Patrick, eds. ''Chambers's Cyclopaedia of English Literature''. Vol. III. Philadelphia: J. B. Lippincott co., 1910. (pg. 717)</ref> He gained a staff position on the ''[[London Academy]]'' and eventually became its leading literary critic.<ref name="Reilly"/> Bell went on to become a contributor of articles, poems and letters to various Victorian era publications including ''[[The Fortnightly Review]]'', ''[[The Pall Mall Magazine]]'', ''[[The Atlantic Monthly]]'', ''[[Athenaeum (British magazine)|The Athenaeum]]'', ''The Speaker'', ''[[The Literary World (magazine)|The Literary World]]'', ''Temple Bar'', ''[[The Lady's Realm]]'', ''[[Black and White]]'' and ''The Academy''. He also wrote articles for the ''[[Dictionary of National Biography]]'',<ref name="Article"/> ''The Poets and the Poetry of the Century'' and the ''Savage Club Papers''.<ref name="Plarr"/>

During the 1890s, he published a second series of poetry collections ''Spring's Immortality and Other Poems'' (1893), ''Pictures of Travel and Other Poems'' (1898) and ''Collected Poems'' (1901). Four years after the death of Rossetti, he published her biography ''Christina Rossetti: A Biographical and Critical Study'' (1898).<ref name="Peattie"/><ref name="Kunitz"/><ref name="Plarr"/><ref name="Article"/><ref name="Chambers"/>

Bell was also active politically during this time as a [[The Relugas Compact#Liberal Imperialists and the Liberal League|Liberal Imperialist]]. He was a charter member of [[W.E. Forster]]'s Imperial Federation Committee,<ref name="Kunitz"/> lectured for the Social and Political Education League and on four occasions contested St George Hanover Square (or the London County Council)<ref name="Reilly"/> on behalf of the [[Liberal Party (UK)|Liberal Party]]. For several years, he was also a member of the [[Athenaeum Club, London|Athenaeum]]. Bell died at his [[Orme Square]] home in [[Bayswater, London]], on 13 December 1930.<ref name="Article"/>

==Bibliography==
*''The Keeping of the Vow and Other Verses'' (1879)
*''Verses of Varied Life'' (1882)
*''Old Year Leaves'' (1883)
*''A Forgotten Genius: Charles Whitehead'' (1884)
*''Spring's Immortality and Other Poems'' (1893)
*''Christina Rossetti: A Biographical and Critical Study'' (1898)
*''Pictures of Travel and Other Poems'' (1898)
*''Collected Poems'' (1901)
*''Poems: With a Dedicatory Essay to Theodore Watts-Dunton'' (1909)
*''Poetical Pictures of the Great War'' (1917)

==References==
{{Wikisource}}
:{{A Short Biographical Dictionary of English Literature}}
{{Reflist}}

==External links==
* {{Internet Archive author |sname=Mackenzie Bell |sopt=t}}
*[http://www.nationalarchives.gov.uk/nra/searches/subjectView.asp?ID=P35055 Bell, Henry Thomas Mackenzie (1856-1930) Writer] at the [[National Register of Archives]]

{{DEFAULTSORT:Bell, Henry Thomas Mackenzie}}
[[Category:1856 births]]
[[Category:1930 deaths]]
[[Category:Lecturers]]
[[Category:English biographers]]
[[Category:English literary critics]]
[[Category:English poets]]
[[Category:People of the Victorian era]]
[[Category:Liberal Party (UK) politicians]]
[[Category:English male poets]]