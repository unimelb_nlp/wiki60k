{{Use dmy dates|date=October 2012}}
{{Infobox Australian place
| type                = suburb
| name                = Blakeview
| city                = Adelaide
| state               = sa
| image               = Grandview Drive Blakeview.jpg
| caption             = Looking into Blakeview from Craigmore Road at twilight
| pop                 = 5,093
| pop_year = {{CensusAU|2011}}
| pop_footnotes       = <ref name=Census2011>{{Census 2011 AUS | id = SSC40053 | name = Blakeview (State Suburb)|quick = on | accessdate=16 February 2015}}</ref>
| pop2                = 4,294
| pop2_year = {{CensusAU|2006}}
| pop2_footnotes      = <ref name=ABS2006>{{Census 2006 AUS | id =SSC41151 |name = Blakeview (State Suburb) |accessdate=20 April 2011 | quick=on}}</ref>
| est                 = 1990
| postcode            = 5114<ref name=Postcodes>{{cite web |url=http://www.postcodes-australia.com/areas/sa/adelaide/blakeview
|title=Blakeview, South Australia (Adelaide) |author= |date= |work=Postcodes-Australia |publisher=Postcodes-Australia.com |accessdate=20 April 2011}}</ref>
| area                = 
| dist1               = 29
| dir1                = N
| location1           = [[Adelaide city centre]]<ref name=Postcodes/>
| lga                 = City of Playford
| stategov            = [[Electoral district of Napier|Napier]] <small>''(2011)''</small><ref>{{cite web|url=http://www.ecsa.sa.gov.au/apps/news/?year=2010 |title=Electoral Districts - Electoral District for the 2010 Election |author= |date= |work= |publisher=Electoral Commission SA |accessdate=20 April 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110822191822/http://www.ecsa.sa.gov.au/apps/news/?year=2010 |archivedate=22 August 2011 |df=dmy }}</ref>
| fedgov              = [[Division of Wakefield|Wakefield]] <small>''(2011)''</small><ref>{{cite web |url=http://apps.aec.gov.au/esearch |title=Find my electorate |author= |date=15 April 2011 |work= |publisher=Australian Electoral Commission |accessdate=20 April 2011}}</ref>
| near-n              = [[Kudla, South Australia|Kudla]]
| near-ne             = [[Evanston South, South Australia|Evanston South]]
| near-e              = [[Uleybury, South Australia|Uleybury]]
| near-se             = [[Craigmore, South Australia|Craigmore]]
| near-s              = [[Elizabeth Downs, South Australia|Elizabeth Downs]]
| near-sw             = [[Smithfield, South Australia|Smithfield]]
| near-w              = [[Munno Para, South Australia|Munno Para]]
| near-nw             = [[Munno Para, South Australia|Munno Para]]
}}

'''Blakeview''' is a northern [[Suburbs and localities (Australia)|suburb]] of [[Adelaide]], [[South Australia]]. It is located in the [[City of Playford]]. Blakeview is predominantly a residential suburb, but also has two commercial areas and two education areas.

==Geography==
Blakeview is located to the northeast of [[Elizabeth, South Australia|Elizabeth]] and lies on the east side of [[Main North Road]] opposite [[Smithfield, South Australia|Smithfield]] and [[Munno Para, South Australia|Munno Para]].<ref name=UBD>{{cite book |title=Adelaide and surrounds street directory|last= |first= |authorlink= |coauthors= |edition= 47th|year=2009 |publisher=UBD |location= |isbn=978-0-7319-2336-6 |page= |pages= |url= |accessdate=}}</ref> There is a commercial area facing Main North Road in the oldest part of the suburb which includes a [[Coles Express]] service station and medical services. The Blakes Crossing subdivision north of Craigmore Road also has a commercial area, which includes [[Woolworths Supermarkets|Woolworths]] and [[Aldi]] supermarkets.

Blakeview was gazetted as a suburb in 1990, taking territory from [[Smithfield, South Australia|Smithfield]] and [[Munno Para, South Australia|Munno Para]].<ref name="PLB">{{cite web |url=http://maps.sa.gov.au/plb/#|title=Placename Details: Blakeview |work=Property Location Browser |access-date=20 November 2016 |publisher=Government of South Australia |date=11 March 2009 |id=SA0007230}}</ref>

==Demographics==
The 2006 Census by the [[Australian Bureau of Statistics]] counted 4,294 persons in Blakeview on census night. Of these, 49.6% were male and 50.4% were female.<ref name=ABS2006/>

The majority of residents (76.2%) are of Australian birth, with other common census responses being [[England]] (12.4%) and [[Scotland]] (2.2%).<ref name=ABS2006/>

The age distribution of Blakeview residents is skewed towards a slightly younger population than the greater Australian population. 58% of residents were over 25 years in 2006, compared to the Australian average of 66.5%; and 42% were younger than 25 years, compared to the Australian average of 33.5%.<ref name=ABS2006/>

==Community==
[[File:Blakeview Christian School 2016.jpg|thumb|Blakeview Christian School buildings from Main Terrace, 2016]]

===Schools===
The southern education precinct in Blakeview includes Blakeview Primary School on Omega Drive, [[Trinity College, Gawler|Trinity College]] Blakeview adjacent to it on Park Lake Boulevard, with [[Craigmore High School]] next along, and a preschool centre across the road.<ref>{{cite web |url=http://www.australianschoolsdirectory.com.au |title=Australian Schools Directory |author= |date= |work= |publisher=Australian Schools Directory |accessdate=20 April 2011}}</ref>

Blakes Crossing Christian College operated by Christian Community Ministries opened for reception to year 5 in 2014 and intends to gradually extend to year 12 and a total of 700 students.<ref>{{cite web |url=http://www.blakescrossing.sa.edu.au/about-us |title=Blakes Crossing Christian College - About Us |date=2013 |accessdate=1 October 2014}}</ref> At the beginning of 2014 it had only six students enrolled, using the [[Lend Lease Group|Lend Lease]] land sales offices for classrooms until the school's own buildings could be constructed.<ref>{{cite news |url=http://www.theaustralian.com.au/news/blakes-crossing-christian-college-opens-in-blakeview-with-only-six-students/story-e6frg6n6-1226811919092 |title=Blakes Crossing Christian College opens in Blakeview with only six students |publisher=News Ltd |work=[[The Australian]] |author=Kurtis Eichler |accessdate=1 October 2014}}</ref> It is now sited in its own buildings, not far from the Blakes Crossing shopping precinct and parks.

==Facilities and attractions==
===Parks===
There are several parks and reserves throughout the suburb, especially along Smith Creek and Main Terrace. Many of the parks are provided with playground equipment.<ref name=UBD/>

==Transportation==
===Roads===
Blakeview is serviced by [[Main North Road]], connecting the suburb to [[Adelaide city centre]].<ref name=UBD/>

===Public transport===
Blakeview is serviced by public transport run by the [[Adelaide Metro]].<ref name=Metro>{{cite web|url=http://timetables.adelaidemetro.com.au/ttsearch.php |title=Public Transport in Adelaide |author= |date=12 January 2011 |work=Adelaide Metro official website |publisher=Dept. for Transport, Energy and Infrastructure, Public Transport Division |accessdate=20 April 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110426182058/http://timetables.adelaidemetro.com.au/ttsearch.php |archivedate=26 April 2011 |df=dmy }}</ref>

==See also==
{{commonscat|Blakeview, South Australia}}
*[[List of Adelaide suburbs]]

==References==
{{reflist}}

==External links==
*{{cite web |url=http://www.playford.sa.gov.au |title=City of Playford |author= |date= |work=Official website |publisher=City of Playford |accessdate=20 April 2011}}

{{Coord|-34.682|138.702|format=dms|type:city_region:AU-SA|display=title}}
{{City of Playford suburbs}}

[[Category:Suburbs of Adelaide]]