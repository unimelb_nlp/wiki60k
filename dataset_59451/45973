{{Infobox film
| name           = A Mother Should be Loved
| image          = 
| image size     = 
| border         = 
| caption        = 
| director       = [[Yasujirō Ozu]]
| producer       = [[Kōgo Noda]]
| writer         = 
| screenplay     = Tadao Ikeda<br>Masao Arata
| story          = Shutaro Komiya<br>[[Kōgo Noda]]
| narrator       = 
| starring       = Den Ohinata<br>[[Koji Mitsui|Hideo Mitsui]]<br>[[Mitsuko Yoshikawa]]
| music          = 
| cinematography = Isamu Aoki
| art director    = 
| editing        = 
| studio         = [[Shochiku]]/Kamata
| distributor    = 
| released       = {{Film date|1934|05|11|Japan}}
| runtime        = 93 mins
| country        = Japan
| language       = Japanese
| budget         = 
| gross          =
}}

{{nihongo|'''''A Mother Should be Loved'''''|母を恋はずや|Haha o kowazuya}} is a 1934 Japanese film directed by [[Yasujirō Ozu]], the first and last reels of which have been lost. Ozu had wanted to name the film ''Tokyo Twilight'', but studio executives preferred a title that referenced motherhood, a popular theme in Japanese cinema at the time of release.<ref name=Richie>{{cite book|last=Richie|first=Donald|title=Ozu|year=1974|publisher=University of California Press|location=Berkeley|isbn=0-520-02445-1|authorlink=Donald Richie}}</ref>

The film tells of the strained relationship between a mother and her two sons after the death of the family patriarch. Ozu once said that he remembered making this early film "not because it was any good, but because my father died while I was making it".<ref name=Richie/>

==Plot==
Mr. Kajiwara ([[Yukichi Iwata]]) promises his two sons, Sadao and Kosaku, a weekend trip to the beach. Later that day his wife, Chieko ([[Mitsuko Yoshikawa]]), receives a phone call informing her that her husband has collapsed at work.

After Kajiwara's funeral, his friend Okazuki visits Chieko, asking her to continue to raise the eldest boy, Sadao, as if he were her own. It is revealed that Chieko was Kajiwara's second wife and that Sadao is Kajiwara's son from his first marriage.

Several years later Okazuki visits Chieko again. Chieko tells him that she has just argued with Sadao because, when preparing materials for his university application, Sadao consulted his birth certificate and discovered the secret of his parentage. Okazuki explains to Sadao that the deception was necessary because it allowed Chieko to treat Sadao as her own son. At first, Sadao complains that he feels tricked, but then tearfully apologizes to his mother.

Sadao persuades a friend on the university rowing team, Hattori ([[Chishū Ryū]]), to leave the whorehouse in which he has been living. He asks Chieko for a loan to pay Hattori's debts but then discovers that Chieko has asked Kosaku to cancel an expensive trip he had been planning. Sadao tries to return the loan, but Chieko refuses it, so he instead gives the money to Kosaku to pay for his trip. At first, Chieko resists, but when the sons point out that she always treats Sadao more favorably than Kosaku, she relents.

While Kosaku is away, Sadao again argues with Chieko that she is treating the sons differently. Later, Kosaku asks Sadao why he made their mother upset. Kosako becomes angry and repeatedly strikes Sadao, who storms away. To explain Sadao's behavior, Chieko reveals the truth of his parentage to Kosaku.

The next day Chieko finds Sadao at the whorehouse and begs him to come home, but he refuses. After Chieko leaves, a cleaner asks Sadao to reconsider. Moved by her words, he returns home, and the family is reconciled.

==Cast==
* [[Yukichi Iwata]] as  Kajiwara (performance missing from the surviving reels)
* [[Mitsuko Yoshikawa]] as Kajiwara's second wife
* [[Den Obinata]] as Sadao, Kajiwara's son by his first wife
* [[Seiichi Kato]] as young Sadao
* [[Koji Mitsui]] as Kosaku, Kajiwara's son by his second wife (credited as Hideo Mitsui)
* [[Akio Nomura]] as young Kasaku
* [[Shinyo Nara]] as Okazaki
* [[Kyoko Mitsukawa]] as Kazuko
* [[Chishū Ryū]] as Hattori
* [[Yumeko Aizome]] as Mitsuko, a prostitute
* [[Junko Mastui]] as Ranko
* [[Choko Iida]] as a maid

==Home media==
In 2011, the BFI released a Region 2 DVD of the film as a bonus feature on its Dual Format Edition (Blu-ray + DVD) of ''[[Late Autumn (1960 film)|Late Autumn]]''.<ref>http://filmstore.bfi.org.uk/acatalog/info_19520.html</ref>

==References==
{{reflist}}
{{Yasujirō Ozu}}

{{DEFAULTSORT:Mother Should be Loved}}
[[Category:Japanese films]]
[[Category:Films directed by Yasujirō Ozu]]
[[Category:1934 films]]
[[Category:Japanese black-and-white films]]