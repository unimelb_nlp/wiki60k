{{for|the 2011 winter storm|January 31 – February 2, 2011 North American winter storm}}
{{Good article}}
{{Infobox Hurricane
| Name=Groundhog Day Tropical Storm
| Type=Tropical storm
| Year=1952
| Basin=Atl
| Image location=Groundhog Day TS Feb 3 1952.png
| Image name=Surface weather analysis of the system as a non-frontal low on February&nbsp;3 while crossing the Florida Peninsula
| Formed=February 3, 1952
| Dissipated=February 5, 1952
| Extratropical=February 4, 1952
| 1-min winds=60
| Pressure=990
| Damages=Minimal
| Fatalities=None reported
| Areas=[[Yucatan Peninsula]], [[Cuba]], [[Florida]], [[The Bahamas]], [[East Coast of the United States]]
| Hurricane season=[[1952 Atlantic hurricane season]]
}}
The extraordinary '''1952 Groundhog Day Storm''' was the only Atlantic [[tropical cyclone]] on record in the month of February. First observed in the western [[Caribbean Sea]] on February&nbsp;2 as a non-frontal low, it moved rapidly throughout its duration and struck southwestern [[Florida]] early the next day as a gale-force storm. In the state, the winds damaged some crops and power lines, but no serious damage was reported. The system became a tropical storm after emerging over the Atlantic Ocean before quickly transitioning into an [[extratropical cyclone]] on February&nbsp;4. Strong winds and waves washed a freighter ashore, but no injuries were related to the event. Subsequently, the storm brushed eastern New England, causing minor power outages, before it moved inland near Maine. There were no reported fatalities related to the storm.

==Meteorological history==
[[File:1952 Groundhog Day tropical storm track.png|thumb|left|Storm path]]
On February&nbsp;2, [[Groundhog Day]], a disturbance was first observed in the western [[Caribbean Sea]]. Winds were estimated at around 35&nbsp;mph (55&nbsp;km/h), and it tracked rapidly northward, initially to the north-northwest. After passing near [[Cancún, Mexico|Cancún]] along the [[Yucatan Peninsula]], it turned northeastward and brushed the northwest coast of Cuba. Early on February&nbsp;3 the storm approached [[Key West, Florida|Key West]], and shortly thereafter moved ashore near [[Cape Sable, Florida]]. It quickly crossed the state, passing near [[Miami, Florida|Miami]] before emerging into the western Atlantic Ocean.{{Atlantic hurricane best track}} The Miami [[National Weather Service]] office recorded a wind gust of 68&nbsp;mph (110&nbsp;km/h), as well as sustained tropical storm force winds for about four hours; the station also recorded a [[atmospheric pressure|barometric pressure]] of 1004&nbsp;[[bar (unit)|mbar]] (29.66&nbsp;[[inch of mercury|inHg]]).<ref name="usatoday">{{cite news|author=USAToday.com|year=2008|title=Only February tropical storm hit Florida in 1952|accessdate=2009-02-27|url=http://www.usatoday.com/weather/hurricane/history/groundhog-day-storm.htm | work=USA Today}}</ref>

After leaving Florida, the storm continued rapidly northeastward and transitioned into a tropical cyclone. Late on February&nbsp;3 it reached its peak strength with maximum sustained winds of 70&nbsp;mph (110&nbsp;km/h). On February&nbsp;4 it completed the transition into an [[extratropical cyclone]] off the coast of [[North Carolina]].<ref name="HURDAT"/> Around that time, gale force winds extended 100&nbsp;miles (160&nbsp;km) to the east of the center.<ref name="up24"/> Later that day, it passed over [[Cape Cod]], and early on February&nbsp;5 it moved into eastern [[Maine]]. The Hurricane Research Division assessed the storm as losing its identity shortly thereafter, over [[New Brunswick]].<ref name="HURDAT"/> However, a map produced by the [[National Hurricane Center|U.S. Weather Bureau]] indicated that the storm continued northward into the [[Gulf of Saint Lawrence]] and later crossed eastern [[Quebec]] and [[Labrador]]. By February&nbsp;6, it reached the ocean again, deepening to a minimum pressure of 988&nbsp;mbar (29.18&nbsp;inHg). At that point, the Weather Bureau track ended, and as such the ultimate fate of the storm is unknown.<ref>{{cite web|author=Jay S. Winston|publisher=Washington, D.C. Weather Bureau Office|year=1952|title=Weather and Circulation of February 1952|accessdate=2009-02-27|url=http://docs.lib.noaa.gov/rescue/mwr/080/mwr-080-02-0026.pdf|format=PDF}}</ref>

==Impact and records==
Residents and tourists in southern Florida were unprepared for the unusual off-season storm. Winds of up to 65&nbsp;mph (105&nbsp;km/h) spread across the area,<ref name="upi25">{{cite web|author=Staff Writer|publisher=United Press|date=1952-02-05|title=Johnny Come Lately Winds High|accessdate=2009-02-27|url=http://www.thehurricanearchive.com/Viewer.aspx?img=46212363_clean&firstvisit=true&src=search&currentResult=1&currentPage=0}} {{Dead link|date=April 2012|bot=H3llBot}}</ref> causing damage to windows and power lines.<ref>{{cite web|author=Staff Writer|publisher=United Press|date=1952-02-04|title=Cyclone Whirls Near Carolinas|accessdate=2009-02-27|url=http://www.thehurricanearchive.com/Viewer.aspx?img=76872037_clean&firstvisit=true&src=search&currentResult=5&currentPage=0}} {{Dead link|date=April 2012|bot=H3llBot}}</ref> The storm dropped 2–4&nbsp;inches (50–100&nbsp;mm) of precipitation along its path; the combination of unseasonable rainfall and winds resulted in crop damage in [[Miami-Dade County, Florida|Miami-Dade County]].<ref name="usatoday"/>
[[File:One 1952-02-04 weather map.png|thumb|right|Pressure map showing the Groundhog Day tropical storm as an extratropical storm off the Eastern seaboard.]]
After the storm moved into the western Atlantic, the Miami [[National Hurricane Center|U.S. Weather Bureau]] issued [[Gale warning|storm warnings]] for the [[North Carolina]] coastline from [[Wilmington, North Carolina|Wilmington]] to [[Cape Hatteras]]; the region was warned to prepare for strong winds. The agency also issued a [[small craft advisory]] southward through [[Charleston, South Carolina]].<ref name="upi25"/> Offshore, the storm produced winds of up to 85&nbsp;mph (140&nbsp;km/h),<ref name="upi25"/> as well as waves up to 35&nbsp;feet (10&nbsp;m) in height. The combination of the winds and rough waves drove a freighter ashore along [[Portsmouth, North Carolina|Portsmouth Island]] in the Outer Banks,<ref name="up24">{{cite web|author=Staff Writer|publisher=United Press|date=1952-02-04|title=Ship With 26 Is Wrecked Off Carolina As Gale From Atlantic Sweeps Up Coast|accessdate=2009-02-27|url=http://www.atlantichurricaneguide.com/February.html |archiveurl=https://web.archive.org/web/20010813095540/http://www.atlantichurricaneguide.com/February.html |archivedate=2001-08-13}}</ref> after the engine was damaged when water entered the fuel line.<ref name="upi25"/> The 26&nbsp;person crew initially planned to evacuate, but they later decided to stay on the freighter as the [[United States Coast Guard|U.S. Coast Guard]] were deployed to assist.<ref name="up24"/> The seas damaged a portion of the ship, but the entire crew was rescued without any injuries.<ref>{{cite web|author=Staff Writer|publisher=Associated Press|date=1952-02-05|title=Hulk Wallows|accessdate=2009-02-27|url=http://www.thehurricanearchive.com/Viewer.aspx?img=103665463_clean&firstvisit=true&src=search&currentResult=8&currentPage=0}} {{Dead link|date=April 2012|bot=H3llBot}}</ref> The storm later brushed New England, bringing rain, fog, warmer temperatures, and gusty winds. The combination resulted in downed power poles and tree limbs, leaving 10,000&nbsp;houses without electricity.<ref name="up24"/>

The storm was described as a "freak", forming about three months after the end of the hurricane season. The chief forecaster at Miami U.S. Weather Bureau, Grady Norton, remarked that he was unsure how the cyclone developed.<ref name="upi25"/> It is the only tropical or [[subtropical cyclone|subtropical]] storm on record during the month of February, and was the earliest tropical cyclone to strike the United States.<ref name="HURDAT"/> Its structure initially was uncertain, and the storm was not included in the [[1952 Atlantic hurricane season]] summary published by the Miami Weather Bureau office.<ref>{{cite web|author=Grady Norton|date=1953-01-12|publisher=U.S. Weather Bureau Office in Miami|title=Hurricanes of 1952|accessdate=2009-02-27|url=http://docs.lib.noaa.gov/rescue/mwr/081/mwr-081-01-0012.pdf|format=PDF}}</ref> Ultimately it was included in the tropical cyclone database. Had it been operationally treated as a tropical cyclone, it would have been named Tropical Storm Able.<ref name="HURDAT"/>

==See also==
{{Portal|Tropical cyclones}}
* [[List of off-season Atlantic hurricanes]]
* [[List of Florida hurricanes (1950–1974)]]
{{clear}}

==References==
{{Reflist}}
{{Off-season Atlantic hurricanes}}
{{1952 Atlantic hurricane season buttons}}

{{DEFAULTSORT:1952 Groundhog Day tropical storm}}
[[Category:1952 Atlantic hurricane season]]
[[Category:Atlantic tropical storms]]
[[Category:Off-season Atlantic tropical cyclones]]
[[Category:Atlantic hurricanes in Mexico]]
[[Category:Hurricanes in Florida]]
[[Category:1952 natural disasters in the United States]]