{{Other people|William Powell}}
{{Infobox person 
| name        = William J. Powell
| image       = William J. Powell 1917.jpg
| caption     = Lt. William J. Powell in France, 1917
| imagesize   = 200px
| birth_date  = {{birth date |1897|7|27|mf=yes}}
| birth_place = Henderson, Kentucky
| death_date  = {{Death date and age|1942|7|12|1897|7|27|mf=yes}}
| death_place = Los Angeles, California
| known_for   = Aviator, engineer, author
}}

'''William Jenifer Powell''' (July 27, 1897 – July 12, 1942) was an [[United States|American]] engineer, soldier, civil aviator and author who is credited with promoting [[aviation]] among the [[African American]] community. Along with [[Bessie Coleman]] and [[James Banning]], he is recognized as a pioneer aviator and a civil rights activist. Powell was optimistic about the prospects of African-Americans in aviation, and believed that their involvement in the industry would help end racial prejudice at a time of widespread [[Racial segregation in the United States|segregation]] under the [[Jim Crow]] laws.<ref name="Brady" /><ref name="AARegistry" />

==Early life==
Powell was born in [[Henderson, Kentucky]] and moved with his family to [[Chicago]], where he was accepted to the [[University of Illinois]] [[electrical engineering]] program. His studies were cut short when he volunteered to the [[370th Infantry Regiment (United States)|370th Illinois Infantry Regiment]] and was shipped off to fight in [[World War I]]. He was wounded in a gas attack, and subsequently returned to the United States to finish his college degree.<ref name="BlackPast.org" />

==Career==
He was fascinated by flight and applied to the [[United States Army Air Corps|Army Air Corps]] and several other flight schools without success, until he was accepted at the [[Los Angeles School of Flight]] in 1928.<ref name="BlackPast.org" /> Powell then founded the ''Bessie Coleman Aero Club'', in honor of the first female black aviator who had died three years before.<ref name="BlackPast.org" /> In 1929, Chicago Congressman [[Oscar De Priest]], then the nation's only black representative, visited [[Los Angeles]]. Powell took De Priest on a flight over the city, and subsequently asked Susan Hancock, [[Booker T. Washington]]'s mother-in-law, to christen the plane with the congressman's name.<ref name="BlackPast.org" /> Other personalities that visited Powell's club included [[Duke Ellington]] and [[Joe Louis]].<ref name="Lienhard" />

To complement his work at the club, Powell also established a school to train mechanics and pilots, and published the ''Craftsmen Aero News'', which he claimed to be the first African American [[trade journal]].<ref name="Brady" /> On [[Labor Day]] 1931, the club sponsored the first all-black [[air show]] in the United States, which was attended by 15,000 spectators.<ref name="Smithsonian" /> By 1932, Powell was one of only 14 Black pilots in the United States, as well as being a licensed navigator and aeronautical engineer.<ref name="Lienhard" />

''Bessie Coleman Aero'' was closed due to financial hardships caused by the [[Great Depression]].<ref name="BlackPast.org" /> In 1934 Powell published ''Black Wings'', a fictionalized account of his own life, through which he aimed to inspire young African Americans to enter aviation not only as pilots, but as designers, engineers and mechanics. He called for them to "fill the air with black wings".<ref name="HillAFB" />

Powell died in 1942, probably due to the effects of his exposure to poison gas in the war.<ref name="Lienhard" /> He is buried in the [[Los Angeles National Cemetery]].<ref>[http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GSln=Powell&GSfn=William&GSby=1897&GSbyrel=in&GSdy=1942&GSdyrel=in&GSob=n&GRid=8553438&df=all& William J Powell at Find A grave]</ref>

==References==
{{Reflist|2|refs=
    <ref name="AARegistry">
        {{cite web
            |date=
            |title=William J. Powell, a pioneer in aviation &#124; African American Registry
            |url=http://www.aaregistry.org/historic_events/view/william-j-powell-pioneer-aviation
            |publisher=African American Registry
            |accessdate=22 March 2013}}
</ref>
    <ref name="BlackPast.org">
        {{cite web
            |date=
            |title=Powell, William J., and the Bessie Coleman Aero Club (1897-1942)
            |url=http://www.blackpast.org/?q=aah/william-j-powell-and-bessie-coleman-aero-1897-1942
            |publisher=BlackPast.org
            |language=
            |last=Winter
            |first=Elizabeth
            |accessdate=22 March 2013}}
</ref>
    <ref name="HillAFB">
        {{cite web
            |date=May 15, 2007
            |title=Fact Sheets: William Powell
            |url=http://www.hill.af.mil/library/factsheets/factsheet.asp?id=5935
            |publisher=[[Hill Air Force Base]]
            |language=
            |accessdate=22 March 2013}}
</ref>
    <ref name="Brady">
        {{cite book
            |last=Brady
            |first=Tim
            |title=The American Aviation Experience: A History
            |url=https://books.google.com/books?id=7ccymjJZxLcC
            |year=2000
            |publisher=SIU Press
            |isbn=9780809323715
            |page=344}}
</ref>
    <ref name="Smithsonian">
        {{cite web
            |date=
            |title=Biographical Passage about William J. Powell
            |url=http://airandspace.si.edu/blackwings/hstudent/bio_powell.cfm
            |publisher=National Air and Space Museum
            |language=
            |accessdate=22 March 2013}}
</ref>
    <ref name="Lienhard">
        {{cite web
            |date=
            |title=BLACK AVIATOR
            |url=http://www.uh.edu/engines/epi987.htm
            |publisher=[[University of Houston]]
            |language=
            |last=Lienhard
            |first=John H. 
            |accessdate=22 March 2013}}
</ref>
}}

==Further reading==
<div style="font-size: 90%">
* {{cite book
    |last=Powell
    |first=William J.
    |title=Black wings
    |url=https://books.google.com/books?id=NcmEAAAAIAAJ
    |year=1934
    |publisher=I. Deach, Jr
    |OCLC =3261929
    |page=}}

* {{cite book
    |last1=Powell
    |first1=William J.
    |editor=Von Hardesty
    |title=Black aviator: the story of William J. Powell: a new edition of William J. Powell's Black wings
    |url=https://books.google.com/books/about/Black_aviator.html?id=RHZTAAAAMAAJ
    |year=1934
    |series=Smithsonian history of aviation series
    |publisher=Smithsonian Institution Press
    |isbn=9781560983415
    |page=}}

* {{cite book
    |last=Flamming
    |first=Douglas
    |title=Bound for Freedom: Black Los Angeles in Jim Crow America
    |url=https://books.google.com/books?id=DK5Bd50XA4gC
    |year=2006
    |series=George Gund Foundation Imprint in African American Studies
    |publisher=University of California Press
    |isbn=9780520940284
    |page=}}

* {{cite book
    |last=Hardesty
    |first=Von
    |title=Black Wings: Courageous Stories of African Americans in Aviation and Space History
    |url=https://books.google.com/books?id=s1BDNA02mrcC
    |year=2008
    |publisher=HarperCollins
    |isbn =9780061261381
    |page=}}
</div>

==External links==
* {{url|http://airandspace.si.edu/exhibitions/gal208/pioneers/blackwing.cfm|Barron Hilton's Pioneers of Flight Gallery}} at the [[National Air and Space Museum]]

{{Authority control}}

{{DEFAULTSORT:Powell, William J}}
[[Category:1897 births]]
[[Category:1942 deaths]]
[[Category:American aviators]]
[[Category:Aviators from Kentucky]]
[[Category:Writers from Chicago]]
[[Category:People from Henderson, Kentucky]]
[[Category:University of Illinois alumni]]
[[Category:African-American people]]
[[Category:United States Army soldiers]]