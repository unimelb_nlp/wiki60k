<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Mitchell U-2 Superwing
 | image=Mitchell U-2 Superwing b.jpg
 | caption=A Mitchell U-2 superwing at the [[Steven F. Udvar-Hazy Center]]
}}{{Infobox Aircraft Type
 | type=[[Ultralight aircraft]]
 | national origin=[[United States]]
 | manufacturer=[[Mitchell Wing Company]]
 | designer=[[Don Mitchell (aircraft designer)|Don Mitchell]]
 | first flight=1980
 | introduced=
 | retired=
 | status=Plans available
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= $5500 US average materials cost + instruments & engine
 | developed from= 
 | variants with their own articles=[[AmeriPlanes Mitchell Wing A-10]]
}}
|}

The '''Mitchell U-2 Superwing''' is an [[United States|American]] [[Tailless aircraft|tailless]] [[ultralight aircraft]] that was designed by [[Don Mitchell (aircraft designer)|Don Mitchell]] for [[Homebuilt aircraft|amateur construction]].<ref name="Cliche">Cliche, Andre: ''Ultralight Aircraft Shopper's Guide'' 8th Edition, page E-259. Cybair Limited Publishing, 2001. ISBN 0-9680628-1-4</ref>

==Design and development==
Although the aircraft was designed before the US [[FAR 103 Ultralight Vehicles]] rules came into force, the U-2 Superwing complies with them anyway (including the category's maximum empty weight of {{convert|254|lb|kg|0|abbr=on}}). The aircraft has a standard empty weight of {{convert|240|lb|kg|0|abbr=on}}. It features a cantilever [[mid-wing]], a single-seat enclosed cockpit, [[tricycle landing gear]] and a single engine in [[pusher configuration]]. The U-2 is a development of the [[high-wing]] [[Mitchell Wing B-10|B-10]].<ref name="Cliche" />

The aircraft [[fuselage]] is made from welded steel tube, while the wing is of wood and foam, with [[Aircraft dope|doped]] [[aircraft fabric covering]]. Its {{convert|34|ft|m|1|abbr=on}} span wing employs a modified Wortmann FX05-191 [[airfoil]]. The flight controls are unconventional; pitch and roll are controlled by [[elevon]]s and yaw is controlled by the [[wing tip]] [[rudder]]s. The main landing gear has suspension and the nose wheel is steerable and equipped with a brake.<ref name="Cliche" /><ref name="Incomplete">{{Cite web|url = http://www.ae.illinois.edu/m-selig/ads/aircraft.html|title = The Incomplete Guide to Airfoil Usage |accessdate = 1 September 2011|last = Lednicer |first = David |authorlink = |year = 2010}}</ref>

The U-2 can accept a variety of engines ranging from {{convert|25|to|40|hp|kW|0|abbr=on}} mounted in [[pusher configuration]].<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 137. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref>

==Specifications (U-2)==
{{Aircraft specs
|ref=Cliche<ref name="Cliche" />
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=
|crew=one
|capacity=
|length m=
|length ft=
|length in=
|length note=
|span m=
|span ft=34
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=136
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=
|empty weight lb=240
|empty weight note=
|gross weight kg=
|gross weight lb=550
|gross weight note=
|fuel capacity={{convert|3|u.s.gal}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Zenoah G25]]
|eng1 type=
|eng1 kw=<!-- prop engines -->
|eng1 hp=20<!-- prop engines -->
|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=
|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=26
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=180
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=<!-- aerobatic -->
|glide ratio=20:1 at {{convert|45|mph|km/h|0|abbr=on}}
|climb rate ms=
|climb rate ftmin=400
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=4.04
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==See also==
{{aircontent|
|related=
|similar aircraft=
* [[Aériane Swift]]
|sequence=
|lists=
|see also=
* [[Ultralight aviation]]
* [[Ultralight aircraft (United States)]]
}}

==References==
{{Reflist}}

== External links ==
{{Commons category|Mitchell Wing}}
* [http://home.earthlink.net/~mitchellwing/ US Pacific; Current supplier of Mitchell Wing plans]
* [http://web.archive.org/web/*/http://www.ameriplanes.com/ AmeriPlanes former manufacturer of Mitchell Wing kits] - website archives on [[Archive.org]]

{{Don Mitchell aircraft}}

[[Category:United States ultralight aircraft 1970–1979]]
[[Category:Don Mitchell aircraft]]