{{refimprove|date=May 2012}}
[[Image:Leski Kazimierz.jpg|thumb|right|137px|Kazmierz Leski]]
{{Polish Underground State sidebar}}
'''Kazimierz Leski''', ''[[pseudonym#Nom de guerre|nom de guerre]]'' '''Bradl''' (21 June 1912 — 27 May 2000), was a [[Poland|Polish]] [[engineering|engineer]], co-designer of the Polish submarines ''[[ORP Sęp]]'' and ''[[ORP Orzeł (1938)|ORP Orzeł]]'', a [[fighter aircraft|fighter pilot]], and an officer in [[World War II]] [[Armia Krajowa|Home Army]]'s intelligence and counter-intelligence.

He is credited, during World War II, with at least 25 journeys across [[Nazi Germany|German]]-held Europe, usually in the uniform of a  [[Wehrmacht]] [[Major General]].

After the war, he was imprisoned by [[People's Republic of Poland|Poland's]] communist authorities.  He spent seven years on death row before being [[rehabilitation (Soviet)|rehabilitated]] in 1956. He then resumed work as an engineer.

==Early life==
Kazimierz Leski was born in [[Warsaw]] on June 21, 1912. His father, Major [[Juliusz Leski]], had been an engineer and a pioneer of [[Second Polish Republic|Poland's]] arms industry.<ref name="Juliusz Leski">Among other things, he founded the [[Pocisk]] munitions works in Warsaw, the explosives factories at [[Rembertów]] and [[Boryszew]], and the predecessor of the [[PZL]] works at [[Okęcie]].</ref> following the [[Polish-Bolshevik War]]. However, he fell out of grace after the [[May Coup (Poland)|May 1926 Coup d'État]], when he remained loyal to the government. Because of that, Kazimierz had to work as a railway worker to pay for his studies at the [[Wawelberg and Rotwand College]] in Warsaw. He also got a simple job in the foundry of the Pocisk munitions works. To study professional books, he learned [[English language|English]], [[Russian language|Russian]] and [[German language|German]]—abilities that proved invaluable later. Early on, he also learned [[French language|French]].

==Engineer==
Immediately upon graduating in 1936, he was offered a job at the [[Nederlandse Verenigde Scheepsbouw Bureaus B.V.]] design bureau (NVSB) in [[The Hague]]. The company was the leading design bureau in the Netherlands, working for all the major naval shipyards in the country. Initially working as a [[technical drawing|draughtsman]], Leski learned the [[Dutch language]], which allowed him to rise quickly through the ranks of the design bureau. His career in the Dutch shipbuilding industry was significantly sped up by the fact that the Netherlands won a contract for construction of two modern [[Orzeł class submarine]]s for the [[Polish Navy]]. He started additional studies at the Maritime Faculty of the [[Delft University of Technology]] and became one of the heads of the Submarine Division of the NVSB, responsible for the comparison of the projects with the supplied machinery. After he patented a new mounting for the [[ballast tank]] funnels, he was promoted and became an independent specialist. Soon afterwards Leski became the head designer for the [[Orzeł class submarine]]s: the future [[ORP Orzeł (1938)|ORP Orzeł]] and [[ORP Sęp]], as well as the deputy to the lead constructor Niemeier.

After the works on the ship series were complete, Leski decided to return to Poland, where he joined the [[Polish Army]] and graduated from his third school<!--rank?-->: the NCO Aviation School in [[Dęblin]].

==War begins==
Mobilized prior to the 1939 [[Invasion of Poland (1939)|German invasion of Poland]], he joined the Polish Air Force. On September 17, 1939, his [[Lublin R-XIII]] F plane was shot down by the [[Soviet Union|Soviets]], and Leski was badly injured. Soon after, he was taken [[prisoner of war]] by Soviet soldiers but managed to escape and reach [[Lwów]]. From there he crossed the new Soviet-German "border of peace" and in October 1939 moved to Warsaw, where he joined an underground organization, ''Muszkieterowie'' (the "Musketeers").

The organization, which was later integrated into the [[Armia Krajowa|Home Army]] (''Armia Krajowa''), was an ''[[en cadre]]'' military organization primarily focused on intelligence. Thus Leski– still suffering from wounds received in September 1939 and unfit for front-line service in the [[Leśni|Forest Units]]– became a leading intelligence officer with the Musketeers and later with the Home Army.

His most important achievements included a complete list of German military units, their [[insignia]], numbers and dispositions. He and his cell also prepared detailed reports on the logistics and transport of German units bound for the [[Eastern Front (World War II)|Eastern Front]], and on the state of bridges, railways and roads in German-held Europe. Leski's unit also began developing a communications network spanning German-occupied Europe from Poland to [[Portugal]], [[France]] and finally the [[Polish Government in Exile]] in the [[United Kingdom]].

==Disguises==
In 1941 Leski made his first trip as a courier to France. In his first trip, he posed as a Lieutenant of the [[Wehrmacht]]. However, he decided to ''promote himself'' to the rank of ''[[Generalmajor]]'' for all other trips in order to be able to travel first class, as his wounds made it impossible for him to travel in crowded, third-class railway cars.<ref name="Stawiński">{{cite journal |author=[[Jerzy Stefan Stawiński]] |date=July 2006 |title=Stawiński nie do zobaczenia |journal=[[Gazeta Wyborcza]] |issue=2006–07–01 |url=http://szukaj.gazeta.pl/archiwum/1,0,4661164.html?kdl=20060701GW&wyr=Kazimierz%2BLeski%255C%255C%2B |accessdate=2006-10-20 |language=pl}}</ref> As General [[Julius von Halmann]] he managed to cross Europe several times in a row without his true identity being revealed. The disguise, his fluent knowledge of several languages and his excellently [[forgery|forged]] documents also allowed him to witness several events he did not plan. Among them was his 1942 visit to the [[Atlantic Wall]] construction site, which was made possible because he convinced one of the passengers in his car that his superiors might want to build a similar line of fortifications in the [[Ukraine]].<ref name="ELEM">{{cite journal |author=ELEM |date=October 2000 |title=Kazimierz Leski |journal=[[Gazeta Wyborcza]] |issue=255 |pages= |url=http://szukaj.gazeta.pl/archiwum/1,0,1257893.html?kdl=20001031GW&wyr=%2522Kazimierz%2BLeski%2522%2B%2B%2B |accessdate=2006-10-20 |language=pl}}</ref> On another occasion he visited the field staff of Field Marshal [[Gerd von Rundstedt]].<ref name="ELEM"/> Apart from his service in intelligence and counter-intelligence, he also took over a cell focused on smuggling information and people in and out of German prisons in occupied Poland, notably the infamous [[Pawiak]].<ref name="Domańska">{{cite book |author=Regina Domańska |coauthors= |title=Pawiak - kaźń i heroizm (Pawiak: slaughter and heroism) |year=1988 |editor=  |chapter= | chapterurl = |publisher=Książka i Wiedza |location=Warsaw |isbn=83-05-11813-4 |url=https://books.google.com/books?id=oXsNAAAAIAAJ&q=Kazimierz+Leski&dq=Kazimierz+Leski |format= |accessdate=|page=138|language=pl}}</ref>

==Warsaw Uprising==
At the outbreak of the [[Warsaw Uprising]] in August 1944, Kazimierz Leski was not commissioned. However, with a group of volunteers he formed an infantry battalion, ''Miłosz'', and became  commander of its first company, ''Bradl''.<ref name="Bradl">Named after Leski's nom-de-guerre. Throughout the war, he used several dozen false names and pseudonyms</ref> The unit fought with distinction in the area of [[Triple Cross Square]] in the [[Warszawa-Śródmieście|Warsaw City Center]]. For his gallantry, Leski was promoted to captain and awarded  several decorations, including the Silver [[Virtuti Militari]], the Gold and Silver [[Krzyż Zasługi z Mieczami|Crosses of Merit with Swords]], and three [[Cross of Valor|Crosses of Valor]].

After the Uprising's capitulation, Leski managed to escape from a column of prisoners and, pretending to be a civilian, returned to the underground.<ref name="Warszawa">{{cite web |author= |title=Kazimierz Leski: honorowy obywatel Warszawy |publisher=City of Warsaw |year=2005 |work=Official website of the city of Warsaw |url=http://www.um.warszawa.pl/v_syrenka/miasto/obywatele-19.htm |accessdate=2006-10-19 |language=pl}}</ref> He became commander of the [[Home Army]] Western Area and later the chief of the [[Armed Forces Delegation for Poland]].

==Communist prison==
After [[History of Poland (1945-1989)|the communist takeover of Poland]] he gradually dismantled his underground net and moved to [[Gdańsk]]. A member of the [[Wolność i Niezawisłość]] anti-communist resistance, under the false name ''Leon Juchniewicz'' he became the first managing director of the demolished [[Gdańsk Shipyard]]. His tasks included reconstructing the shipyard, which had been devastated by Allied air raids and by the withdrawing Germans. In August 1945 he received the communist regime's highest civilian award<!--name?-->, but later the same day was arrested by [[Ministry of Public Security of Poland|the secret police]], who had discovered his true identity.
[[Image:Powazki Lescy.JPG|thumb|right|200px|Leski family tomb, [[Powązki Cemetery]], [[Warsaw]]]]
Charged with attempting to overthrow the regime, he was sentenced to 12 years in prison. The sentence was later commuted to six years. However, in 1951 he was not released. Instead, he was charged with having collaborated with the German occupation forces and held in solitary confinement and brutally tortured.

==Rehabilitation==
After the deaths of Soviet leader [[Joseph Stalin]] (1953) and Polish leader [[Bolesław Bierut]] (1956), Leski was freed and soon [[rehabilitation (Soviet)|rehabilitated]]. Still, he could not find a job, as Poland's communist authorities continued to view former Home Army soldiers with suspicion. He had to give up work in the shipbuilding industry and worked as a clerk at the [[Państwowe Wydawnictwa Techniczne|PWT]] publishing house.  Eventually he became a member of the [[Polish Academy of Sciences]]. Awarded a [[doctorate]], for political reasons he could not receive the rank of [[professor]] for his work on computer analysis of [[natural language]] codes. Nevertheless, he continued his scientific work, publishing seven books and over 150 other publications. He also [[patent]]ed inventions. Righteous Among the Nations honored by Yad Vashem in 1995.
 
Largely unknown to the public, in 1989—after [[history of Solidarity|Solidarity's victory]] and the fall of the communist regime—he published his [[memoir]]s, which became a best-seller.  He received the Polish [[PEN Club]] Prize and the [[Polish Writers' Society]] in Exile Award.

He died on 27 May 2000 and was interred with [[military honors]] at Warsaw's [[Powązki Cemetery]].

==See also==
*[[Jan Karski]]
*[[Jan Nowak-Jeziorański]]
*[[Krystyna Skarbek#Accusations|Krystyna Skarbek]] (was in contact with the "Musketeers" organization)
*[[List of Poles#Intelligence|List of Poles]]

==Notes==
<!--This article uses the Cite.php citation mechanism. If you would like more information on how to add references to this article, please see http://meta.wikimedia.org/wiki/Cite/Cite.php -->
{{reflist}}
{{refbegin}}

==References==
* {{cite book |author= Kazimierz Leski.|coauthors= |title=Życie niewłaściwie urozmaicone: wspomnienia oficera wywiadu i kontrwywiadu AK ''(A Checkered Life:  Memoirs of a Home Army Intelligence and Counterintelligence Officer)'' |year=1989 |editor=  |publisher=[[Wydawnictwo Naukowe PWN]] |location=Warsaw |isbn=83-01-08351-4|language=pl}} 600 pp.
* {{cite web |author=Cpt. Stanisław Wielebski |title=Kazimierz Leski |publisher= |year=2003 |work=ORP Orzeł |url=http://www.mdk2.lublin.pl/ORPOrzel/KLeski.html |accessdate=2006-10-19 |language=pl}}
* {{cite journal |author= |date=July 2001 |title=Kazimierz Leski |journal=[[Gazeta Wyborcza]] |issue=4/07/2001 |pages=11 |url=http://szukaj.gazeta.pl/archiwum/1,0,1470572.html?kdl=20010704GDG&wyr=%2522Kazimierz%2BLeski%2522%2B%2B%2B |accessdate=2006-10-20 |language=pl}}
{{refend}}

{{Authority control}}


{{DEFAULTSORT:Leski, Kazimierz}}
[[Category:Polish military personnel of World War II]]
[[Category:Polish Army officers]]
[[Category:Polish engineers]]
[[Category:Polish scientists]]
[[Category:Polish pilots]]
[[Category:Polish spies]]
[[Category:World War II spies]]
[[Category:1912 births]]
[[Category:2000 deaths]]
[[Category:Burials at Powązki Cemetery]]
[[Category:Delft University of Technology alumni]]
[[Category:Polish People's Republic rehabilitations]]
[[Category:Polish people of World War II]]
[[Category:Polish prisoners and detainees]]
[[Category:World War II prisoners of war held by the Soviet Union]]
[[Category:Polish resistance fighters]]
[[Category:Armia Krajowa members]]
[[Category:People detained by the Polish Ministry of Public Security]]