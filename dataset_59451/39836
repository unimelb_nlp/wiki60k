{{Infobox military person 
|name=Gordon Edmund Williams
|image= Major General Gordon E. Williams.jpg
|caption= Major General Gordon Edmund Williams 
|birth_date= 1935
|death_date=  
|placeofbirth= [[Nashua, New Hampshire]] 
|placeofdeath=  
|placeofburial= 
|placeofburial_label= 
|nickname=  
|allegiance= {{flag|United States|23px}}
|branch= {{air force|United States|23px}}
|serviceyears= 1957-1988  
|rank= Major general
|unit= 
|commands=  
|battles=  [[File:Vietnam Service Medal ribbon.svg|50px]]<br>Vietnam War
|awards=   
|relations=
|laterwork= 
}}

'''Gordon E. Williams''' (born 1935) is a retired [[United States Air Force]] officer. A command pilot with more than 4,000 flying hours, he flew numerous Air Force and Navy aircraft. Upon his retirement on 1 August 1988, he was the director for plans and policy, J-5, Headquarters [[United States European Command]], [[Stuttgart-Vaihingen]], West Germany.

He was the commander of the [[81st Tactical Fighter Wing]], being stationed at [[RAF Bentwaters]], England, in 1980 at the time of the [[Rendlesham Forest incident]].

==Biography==
General Williams was born in 1935, in [[Nashua, New Hampshire]], and graduated from [[Alvirne High School]], [[Hudson, New Hampshire]], in 1953. He earned a bachelor of science degree in general engineering from the [[U.S. Military Academy]] in 1957 and a master of science degree in systems management from the [[University of Southern California]] in 1971. He completed [[Air Command and Staff College]] in 1969, [[National War College]] in 1975 and [[Harvard University]]'s executive program on national and international security in 1983.<ref name="USAFB">[http://www.af.mil/AboutUs/Biographies/Display/tabid/225/Article/105262/major-general-gordon-e-williams.aspx USAF Biography, Major General  Gordon E. Williams]</ref>

===Service===
Williams entered pilot training in August 1957 and received his wings at [[Laredo Air Force Base]], Texas, in September 1958. After gunnery training at [[Luke Air Force Base]], Arizona, and [[Nellis Air Force Base]], Nevada, he was assigned to the [[510th Tactical Fighter Squadron]], [[Clark Air Base]], Philippines, in September 1959, flying [[F-100 Super Sabre]]s. He subsequently was assigned to the [[612th Tactical Fighter Squadron]], [[401st Tactical Fighter Wing]], [[England Air Force Base]], Louisiana. He represented the wing at William Tell 1962, the worldwide tactical gunnery meet. During this assignment, Williams also attended the [[Air Force Fighter Weapons School]] at Nellis Air Force Base and the Army Airborne School at [[Fort Benning]], Georgia.<ref name="USAFB"/>

Selected for exchange duty with the [[United States Navy]] in 1964, General Williams flew an [[F-4 Phantom II]] combat tour of duty in [[Southeast Asia]] from the carrier {{USS|Ranger|CV-61}}. He then was assigned to the initial Air Force contingent in combat evaluation of the [[A-7 Corsair II]] with the Navy, again from the USS Ranger. In May 1968 he transferred to [[Edwards Air Force Base]], California, as [[Tactical Air Command]] project officer for [[A-7D]] testing.<ref name="USAFB"/>

In February 1971 he was assigned to the Tactical Fighter Division, Directorate of Operational Requirements, Headquarters United States Air Force, Washington, D.C. He culminated this tour of duty as chief, Advanced Systems Branch, with requirements responsibilities for a broad range of new tactical fighters, including [[F-15 Eagle]]s, [[F-16 Fighting Falcon]]s and [[A-10 Thunderbolt II]]s.<ref name="USAFB"/>

Williams graduated from the National War College in 1975 and then was assigned as commander, The United States Logistics Group, Detachment 118, [[Izmir]], Turkey. He served as the deputy commander for operations, [[406th Tactical Fighter Training Wing]], [[Zaragoza Air Base]], Spain, from July 1976 to September 1977. He then transferred to the 81st Tactical Fighter Wing, [[Royal Air Force Station Bentwaters]], England, as vice commander. He became commander of the wing in August 1979. In May 1981 he moved to [[Ramstein Air Base]], West Germany, as inspector general, [[United States Air Forces in Europe]].<ref name="USAFB"/>

Upon returning to the United States in September 1982, General Williams was assigned as director of aerospace safety, Air Force Inspection and Safety Center, [[Norton Air Force Base]], California. In July 1984 he became center commander.<ref name="USAFB"/> He later chaired the inquiry into the fatal accident of Lieutenant General [[Robert M. Bond]], who died when he lost control of a MiG-23 that the USAF was secretly operating.<ref name="Red Eagles">{{cite book|last=Davies|first=Steve |title=Red Eagles: America's Secret MiGs|date=1 January 2012|publisher=Osprey Publishing|isbn=9781849088404|pages=224–226}}</ref>

In June 1985 he became commander of the [[13th Air Force]], [[Pacific Air Forces]], Clark Air Base. In March 1987 he was assigned as assistant deputy chief of staff for programs and resources at Air Force headquarters. He was later assigned as director for plans and policy, J-5, Headquarters [[United States European Command]], Stuttgart-Vaihingen, West Germany.<ref name="USAFB"/>

He was promoted to Major General on September 1, 1984, with date of rank March 1, 1981. He has more than 4,000 flying hours and has flown numerous Air Force and Navy aircraft. He retired on 1 August 1988.<ref name="USAFB"/>

===Awards and decorations===
General Williams' military decorations and awards include the [[Air Force Distinguished Service Medal|Distinguished Service Medal]], [[Legion of Merit]] with oak leaf cluster, [[Distinguished Flying Cross (United States)|Distinguished Flying Cross]] [[Meritorious Service Medal (United States)|Meritorious Service Medal]] with oak leaf cluster, [[Air Medal]] with 15 oak leaf clusters, and [[Navy Commendation Medal]] with "V" device and three service stars.<ref name="USAFB"/>

==Rendlesham Forest incident==
In September 1977, then-Colonel Williams was assigned as the Vice Wing Commander of the 81st TFW at RAF Bentwaters, England, and was promoted to Wing Commander in August 1979. He was the commander of the 81st TFW at the time of the [[Rendlesham Forest incident]] in December 1980. Williams has never gone on record about this event, but it has been alleged by other Air Force members that he was involved in the incident and allegedly communicated with alien entities (EBEs).<ref>[http://www.amazon.com/You-Cant-Tell-People-Definitive/dp/033039021X Bruni and Pope (2001), You Can't Tell the People: The Definitive Account of the Rendlesham Forest UFO Mystery, Pan Macmillan; 2nd edition] ISBN 033039021X</ref>

{{Portal|Military of the United States|United States Air Force}}

==References==
{{Air Force Historical Research Agency}}
{{Reflist}}
{{refbegin}}
{{refend}}

==External links==
* {{IMDb name|5077741}}

{{DEFAULTSORT:Williams, Gordon E.}}
[[Category:1935 births]]
[[Category:1980 in England]]
[[Category:Living people]]
[[Category:United States Air Force generals]]
[[Category:People from Nashua, New Hampshire]]
[[Category:American military personnel of the Vietnam War]]
[[Category:John F. Kennedy School of Government alumni]]