<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Kellner-Béchereau 28VD
 | image=Kellner-Béchereau 28VD 1933.jpg
 | caption=The Kellner-Béchereau 28VD circa 1933
}}{{Infobox Aircraft Type
 | type=[[Racing aircraft]]
 | national origin=[[France]]
 | manufacturer=[[Kellner-Béchereau]]
 | designer=
 | first flight=c. May 1933
 | status=destroyed 14 May 1933
 
}}
|}

The '''Kellner-Béchereau 28VD''' was a [[France|French]] racing aircraft built to compete in the 1933 [[Coupe Deutsch de la Meurthe]]. Engine failure and damage sustained in the consequent emergency landing prevented the 28VD from participation in the race.

==Design and development==

The Kellner-Béchereau 28VD was designed to compete in the 1933 Coupe Deutsch, a race around a {{convert|200|km|mi|abbr=on}} circuit from [[Etampes]] in two {{convert|1000|km|mi|abbr=on}} flights separated by a refuelling stop.<ref name=Lailes1/> Engines of less than {{convert|8|l|cuin|abbr=on}} were stipulated; the {{convert|7.95|l|cuin|abbr=on}} [[Delage 12C.E.D.irs]] engine of the 28VD<ref name=NACA/> just met this limit.

The 28VD was a [[low wing]], [[cantilever]] [[monoplane]]. Its [[wing span]] was small ({{convert|6.65|m|ftin|abbr=on}}; a wing area large enough to keep the [[wing loading]] to values comparable to those of [[World War II]] [[fighter aircraft]] designed a few years later produced an [[aspect ratio (aeronautics)|aspect ratio]] of only 4.2. In plan the wing was carefully [[aircraft fairing|faired]] into the [[fuselage]] and had a swept, straight [[leading edge]], rounded tips and a curved [[trailing edge]] entirely occupied by [[ailerons]]. There were two [[spar (aeronautics)|spar]]s, one perpendicular to the fuselage at mid-[[chord (aeronautics)|chord]] and one at an angle bearing the ailerons.<ref name=LaeroN/>

Its fuselage was slender, with a rounded cross-section.  The {{convert|370|hp|kW|abbr=on|disp=flip}}  water-cooled, double-[[supercharger|supercharged]], inverted V-12 Delage engine drove a large diameter, two blade propeller. Its [[radiator (engine cooling)|radiator]]s were in the wing. There were two on each side, one inboard and one outboard, both occupying the full chord and forming the wing skin. An open [[cockpit]] was placed a little in front of the trailing edge of the wing, within a narrow dorsal fairing which stretched over half the fuselage, starting close to the nose. The fuselage tapered rearwards to a conventional tail, with a round-tipped triangular [[tailplane]], mounted at mid-fuselage, which had an adjustable [[angle of incidence (aerodynamics)|angle of incidence]]. Its [[vertical tail]] was tall and slightly rounded and the [[balanced rudder|unbalanced rudder]] extended down to the keel.<ref name=LaeroN/>

The [[landing gear]] of the 28VD had retractable mainwheels, mounted on [[shock absorber|shock absorbing]] forward-raked, parallel legs from the wing. These were hinged to the lower fuselage on opened U-shaped frames. Retraction was vacuum-powered; to raise the wheels the tops of the legs were translated outward, swinging frames and wheels upwards. The tailskid was fixed to a small ventral extension of the fuselage under the fin.<ref name=LaeroN/>

The 1933 Coupe Deutsch de la Meurthe was scheduled to start on 28 May and each aircraft had to make a flight at more than {{convert|200|km/h|mph kn|abbr=on}} over a {{convert|100|km|mi|abbr=on}} course in the period 8–14 May to participate.<ref name=Lailes3/> The 28VD arrived at Etampes on 3 May to be flown by Vernhol;<ref name=Lailes2/> it is not known how much flying it had done before that, though it was only completed a fortnight before the competition.<ref name=LaeroN/> The early days of the test period were occupied with the refinement of the 28VD but on the 12–13 May it was reported to have engine problems.<ref name=Lailes3/> During the early tests it was decided to set a finer [[blade pitch|propeller pitch]] as the initial setting was producing to much drag on the engine. When the aircraft attempted its qualifying flight on the last allowed day, this change of setting allowed the engine to overspeed. As a result, a rubber joint in the cooling system failed, releasing a cloud of vapour which temporarily blinded the pilot and forced an emergency landing in which the undercarriage collapsed and the aircraft overturned. Vernhol was thrown clear and was not seriously hurt but the 28VD was too damaged to take part in the contest for which it was built.<ref name=LaeroN/><ref name=Lailes3/>

==Specifications==
{{Aircraft specs
|ref=L'Aéronautique August 1933<ref name=LaeroN/> Engine details from NACA Technical Memo. No.724<ref name=NACA/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=One
|capacity=
|length m=7.16
|length note=
|span m=6.65
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=10.6
|wing area note=
|aspect ratio=4.2
|airfoil=
|empty weight kg=1002
|empty weight note=
|gross weight kg=1600
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Delage 12C.E.D.irs]]
|eng1 type=Water-cooled, supercharged, inverted V12 engine|V-12]]
|eng1 hp=370
|eng1 note= at 3,900 rpm. Double reduction gear produced 1,900 rpm at the propeller
|more power=

|prop blade number=2
|prop name=Ratier
|prop dia m=2.5
|prop dia note=approximately, adjustable pitch

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|wing loading kg/m2=150
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=

|more performance=

}}

==References==
{{reflist|refs=

<ref name=LaeroN>{{cite journal |date=August 1933| first=Pierre |last=Léglise |title=Les avions de la Coupe Deutsch de la Muerthe 1933 - Kellner-Béchereau 28 V.D.| journal=L'Aéronautique|volume=171 |pages=179–184|url=http://gallica.bnf.fr/ark:/12148/bpt6k6582642n/f9 }}</ref>

<ref name=Lailes1>{{cite journal |last=Frachet |first=André |date=16 February 1939|title=La Coupe Deutsch |journal=Les Ailes|issue=609 |pages=13|url=http://gallica.bnf.fr/ark:/12148/bpt6k65568361/f13 }}</ref>

<ref name=Lailes2>{{cite journal |last=Houard |first=George  |date=11 May 1939|title=Les préparatifs - Chez Kellner-Béchereau|journal=Les Ailes|issue=621 |pages=9|url=http://gallica.bnf.fr/ark:/12148/bpt6k65568487/f9 }}</ref>

<ref name=Lailes3>{{cite journal |date=18 May 1939|title=Huit concurrents se sont qualifiés pour la Coupe Deutsch de la Muerthe |journal=Les Ailes|issue=622 |pages=4|url=http://gallica.bnf.fr/ark:/12148/bpt6k6556849n/f5 }}</ref>

<ref name=NACA>{{cite web |url=http://digital.library.unt.edu/ark:/67531/metadc63516/m1/32/|title=N.A.C.A. Technical Memorandum No.724 pp.31-3 |accessdate=31 July 2016}}</ref>

}}

{{Kellner-Béchereau aircraft}}

{{DEFAULTSORT:Kellner-Bechereau 28VD}}
[[Category:Kellner-Béchereau aircraft]]
[[Category:French sport aircraft 1930–1939]]