<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = Sport Trainer<!--please avoid stating manufacturer in this field; it's stated two lines below -->
  |image = Great lakes.jpg
  |caption = Great Lakes 2T-1A-1<!--image caption; if it isn't descriptive it should be left blank-->
}}{{Infobox Aircraft Type
  |type = Trainer/Tourer
  |manufacturer = [[Great Lakes Aircraft Company]],<br />[[WACO Classic Aircraft]]
  |designer = <!--only appropriate for individuals, not for project leaders or a company or corporation-->
  |first flight = 1929<!--if it hasn't happened, leave it out!-->
  |introduced = <!--date the aircraft entered or will enter military or revenue service-->
  |introduction= <!--date the aircraft will enter military or revenue service, if the aircraft is still in development -->
  |retired = <!--date the aircraft left military or revenue service. If vague or multiples, it probably should be skipped-->
  |status = <!--in most cases, this field is redundant; use it sparingly-->
  |primary user = <!--please list only one, and don't use those tiny flags as they limit horizontal space-->
  |more users =  <!--limited to three "more users" total. please separate with <br/>-->
  |produced = 1929-1933, 1973-1982, 2011-<!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
  |number built = 
  |unit cost = [[US$]]245,000 (2013)<ref name="Grady11Jun13" />
  |developed from = <!-- the aircraft which formed the basis for the topic type -->
  |variants with their own articles = <!-- variants OF the topic type -->
}}
|}
The '''Great Lakes Sport Trainer''' is an [[United States of America|American]] [[biplane]] trainer and aerobatic aircraft.  Originally produced in large numbers before the company building it went bankrupt in the [[Great Depression]] in 1933. Owing to its continuing popularity, however, it was eventually placed back into production in the 1970s and again in 2011 by [[WACO Classic Aircraft]].

==Development and design==
The [[Great Lakes Aircraft Company|Great Lakes Aircraft Corporation]] of [[Cleveland, Ohio]] produced a design for a small two-seat sports/trainer in early 1929, with the first prototype flying in March 1929.<ref name="donald worldp467">Donald 1997, p.467.</ref>  The resulting aircraft, designated '''2-T-1''' was a single bay [[biplane]] of mixed, [[Aircraft fabric covering|fabric-covered]] construction and with a tailskid undercarriage.  Power was by a single {{convert|85|hp|abbr=on}} [[Cirrus Engine|Cirrus]] III inline engine (as the [[Detroit Aircraft Corporation]], the holding company for Great Lakes, also held the American rights to the Cirrus, so all Sports Trainers were originally sold with Cirrus engines).<ref name="glakes">[http://www.geocities.com/greatlakesbipe/history.htm  History of the Great Lakes Sport Trainer]{{dead link|date=October 2010|bot=AnomieBOT}} Retrieved 2 April 2008.</ref> Initial testing showed that the aircraft was tail heavy, so after the first four aircraft were built, the upper wing was swept back.

In January 2011, WACO Classic Aircraft announced that it will put the Great Lakes Model 2T-1A-1/2 model biplane back into production. The aircraft had not been available since 1980. The aircraft will incorporate several changes including metal wing [[spar (aviation)|spar]]s. It will be offered in two models, a touring model, with a [[Lycoming O-360|Lycoming IO-360-B1F6]] engine and a higher-performance sport model, with a [[Lycoming O-360|Lycoming AEIO-360-B1G6]] engine. Work on the new production model was completed in June 2013 and the base price announced as [[US$]]245,000.<ref name="Grady11Jun13">{{Cite news|url = http://www.avweb.com/avwebflash/news/WacoGreatLakesProductionUnderway_208834-1.html |title = Waco Great Lakes Production Underway|accessdate = 13 June 2013|last = Grady|first = Mary|authorlink = |date = 11 June 2013| work = AvWeb}}</ref><ref name="AvWeb18Jan11">{{Cite news|url = http://www.avweb.com/avwebflash/news/WacoRevivesGreatLakesBiplane_203974-1.html|title = Waco Revives Great Lakes Biplane|accessdate = 20 January 2011|last = Grady|first = Mary|authorlink = |date=January 2011| work = AvWeb}}</ref>

==Operational history==
The aircraft proved very successful, with about 250 built before construction ended. It was  highly maneuverable even on the relatively modest power of a Cirrus engine.  A 1929 Great Lakes Model 2T1a held for many years the world record for consecutive [[Aerobatic maneuver|outside loops]], a total of  131, set by Jim Moss flying the Hunt Special which by the early 1930s had been highly modified and re-powered with a Warner Scarab 165 7 cylinder radial engine.<ref name="Davisson GL">Davisson, Budd. [http://www.airbum.com/pireps/PirepGreatLakes.html "The Great, Great Lakes"]. Air Progress, July 1975</ref>

The Great Lakes continued to be popular well after production ended, many aircraft being re-engined with more powerful engines, particularly [[Jacobs Aircraft Engine Company|Jacobs]] [[radial engine]]s.<ref name="Davisson GL"/>  Eventually, in the 1960s, the rights for the Sport Trainer were acquired by [[Harvey Swack]], who offered plans of the aircraft for sale to homebuilders.  In 1972, Swack sold the rights on to Doug Champlin, who set up a reconstituted Great Lakes Aircraft Company to produce a revised version meeting the current airworthiness requirements, powered by modern [[Lycoming Engines|Lycoming]] engines and revised materials of construction (including the use of [[Douglas Fir]] instead of [[Spruce]]).<ref name="glakes"/>  The first of the new production aircraft, the Model 2T-1A-1, powered by a 140&nbsp;hp engine was certified in May 1973, with production starting in October that year.  A more powerful version, the 2T-1A-2 followed in July 1974.<ref name="JAWA76-77p.290"/> In 1978 production was divided between Witchita Kansas and Enid, Oklahoma with the intent on relocation to Florida.<ref>{{cite journal|magazine=Air Progress|date=November 1978|page=14}}</ref>

Rights to the Sport Trainer continued to switch hands, resulting in production moving several times.  By 1980, 137 Sport Trainers had been built.<ref name="Janes 80-81">Taylor, J.W.R. 1980, p.347-348</ref> Production finally finished in 1985.<ref>[http://www.greatlakes2t.co.uk/aircraft/2nd_gen/index.html The Second Generation Great Lakes 1973 - 1984.] Retrieved 2 April 2008</ref>

==Variants==
[[File:Example. Melclose.jpg|right|thumb|Great Lakes 2T-1A]]
;Model 2T-1
:Original production. 85&nbsp;hp (63&nbsp;kW) American Cirrus III engine. Small tail.  Approximately 40 built.<ref name="donald worldp467"/> 
;Model 2T-1A
:Revised version with enlarged tail surfaces and 90&nbsp;hp (67&nbsp;kW) American Cirrus Ace engine.  About 200 built.<ref name="donald worldp467"/>
;Model 2T-1A-1
:New production (1973 on) version. Powered by 140&nbsp;hp (104&nbsp;kW) [[Lycoming O-320]] engine.
;Model 2T-1A-2
:More powerful version - powered by 180&nbsp;hp (134&nbsp;kW) [[Lycoming IO-360]] engine.
;Model 2T-1A-E
:Homebuilt Experimental Plans-built <ref>{{cite journal|magazine=Air Trails|date=Winter 1971}}</ref>
;Model 2T-1E
:95&nbsp;hp (71&nbsp;kW) American Cirrus Ensign engine.  About twelve built.<ref name="donald worldp467"/>
;X
:A 2T-1E used for aero-engine testing.
;Model 2T-1MS
:[[Menasco Pirate]] powered two-seat version, one example (registration of ''NC 304Y'') airworthy at [[Old Rhinebeck Aerodrome]] is fitted with [[Tundra tires#History|Goodyear "airwheel"]] low-pressure main gear tires, and was a favorite aircraft of [[Cole Palen]], the museum's founder.<ref>[http://oldrhinebeck.org/ORA/great-lakes-2t-1ms/ Old Rhinebeck Aerodrome NC304Y]</ref>
;Model 2T-1R
:Homebuilt variant with a {{convert|200|hp|abbr=on}} Ranger 6-440-5.<ref>{{cite journal|magazine=Air Trails|date=Winter 1971|page=9}}</ref>
;Model 2T-2 Speedster
:Racing version of 2T-1-A. Powered by {{convert|95|hp|abbr=on}}  Cirrus Hi-Drive and straight top wing.  Later rebuilt as Model 2T-1E.<ref name="aerofiles great lakes">[http://www.aerofiles.com/_grlakes.html Great Lakes]. ''Aerofiles.''  Retrieved 29 March 2008</ref>
[[File:BabyGreatLakes-2T-1A-2.jpg|thumb|2013 Great Lakes 2T-1A-2]]
;WACO Classic 2T-1A-1/2
:New production model starting in 2011. There will be two models, a touring model, with a [[Lycoming O-360|Lycoming IO-360-B1G6]] engine and a higher-performance sport model, with a [[Lycoming O-360|Lycoming AEIO-360-B1G6]] engine<ref name="AvWeb18Jan11" />

==Specifications (Model 2T-1A-2)==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane<!-- options: plane/copter -->
|jet or prop?=prop<!-- options: jet/prop/both/neither -->
|ref=Jane's All The World's Aircraft 1976-77 <ref name="JAWA76-77p.290">Taylor 1976, p.290</ref>
|crew=2
|capacity=
|length main= 20 ft 4 in
|length alt= 6.20 m
|span main= 26 ft 8 in
|span alt= 8.13 m
|height main= 7 ft 4 in
|height alt= 2.24 m
|area main= 187.6 ft²
|area alt= 17.43 m²
|airfoil=M-12
|empty weight main= 1,230 lb
|empty weight alt= 558 kg
|loaded weight main=  
|loaded weight alt=  
|useful load main=  
|useful load alt=  
|max takeoff weight main= 1,800 lb
|max takeoff weight alt= 816 kg
|more general=
|engine (prop)= [[Lycoming IO-360]]-B1F6
|type of prop=4-cylinder horizontally opposed air-cooled [[piston engine]]
|number of props=1
|power main= 180 hp
|power alt= 134 kW
|power original=
   
|max speed main= 115 knots
|max speed alt= 132 mph, 212 km/h
|max speed more= at sea level
|cruise speed main= 102 knots
|cruise speed alt= 118 mph, 190 km/h
|cruise speed more= (max cruise)
|never exceed speed main=  
|never exceed speed alt=  
|stall speed main= 50 knots
|stall speed alt= 57 mph, 92 km/h
|range main= 260 nm
|range alt= 300 mi, 482 km
|ceiling main= 17,000 ft
|ceiling alt= 5,180 m
|climb rate main= 1,400 ft/min
|climb rate alt= 7.2 m/s
|loading main= 9.59 lb/ft²
|loading alt= 46.8 kg/m²
|thrust/weight=<!-- a unitless ratio -->
|power/mass main= 0.1 hp/lb
|power/mass alt= 160 W/kg
|more performance=
|armament=
|avionics=

}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
*[[Bücker Bü 133]] ''Jungmeister''
*[[Bücker Bü 131|Bucker Jungmann]]
*[[Oldfield Baby Great Lakes]] – a scaled down homebuilt based on the Sport Trainer
*[[Pitts S-2]]
|lists=<!-- related lists -->
|see also=<!-- other relevant information -->

}}

==References==

===Notes===
{{Reflist}}

===Bibliography===
{{refbegin}}
* Donald, David (editor). ''The Encyclopedia of World Aircraft''. Aerospace Publishing. 1997. ISBN 1-85605-375-X.
* Taylor, J.W.R. ''Jane's All The World's Aircraft 1976-77''. London: Jane's, 1976. ISBN 0-354-00538-3.
* Taylor, J.W.R. ''Jane's All The World's Aircraft 1980-81''. London: Jane's, 1980. ISBN 0-531-03953-6.

{{refend}}

==External links==
{{commons category|Great Lakes 2T-1A Sportster}}
*{{Official website|http://www.wacoaircraft.com/great-lakes/}}
*[http://www.airweb.faa.gov/Regulatory_and_Guidance_library/rgMakeModel.nsf/0/9ba5854f54f1541f86256f0200421595/$FILE/A18EA.pdf Type Certificate]
{{Aerobatics}}

[[Category:United States civil trainer aircraft 1920–1929]]
[[Category:Single-engine aircraft]]
[[Category:Great Lakes aircraft|Sport Trainer]]
[[Category:Aerobatic aircraft]]