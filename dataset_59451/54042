{{redirect|Angle Park|the song by [[Big Country]]|The Crossing (Big Country album)}}
{{refimprove|date=January 2008}}
{{Use dmy dates|date=October 2012}}
{{Infobox Australian place| type = suburb
| name     = Angle Park
| city     = Adelaide
| state    = sa
| image    = 
| caption  = 
| lga      = City of Port Adelaide Enfield
| postcode = 5010
| est      = 1950s
| pop      = 1,467 | pop_year = {{CensusAU|2011}}
| pop_footnotes = <ref name=Census2011>{{Census 2011 AUS | id=SSC40017 | name=Angle Park (State Suburb) | quick=on |accessdate=14 February 2015}}</ref>
| pop2      = 583 | pop2_year = {{CensusAU|2006}}
| pop2_footnotes = <ref name=Census2006>{{Census 2006 AUS | id=SSC41056 | name=Angle Park (State Suburb) | quick=on |accessdate=17 July 2010}}</ref>
| area     = 
| stategov = [[Electoral district of Enfield|Enfield]]
| fedgov   = [[Division of Port Adelaide|Port Adelaide]]
| near-nw  = 
| near-n   = [[Wingfield, South Australia|Wingfield]]
| near-ne  = 
| near-w   = [[Mansfield Park, South Australia|Mansfield Park]]
| near-e   = [[Regency Park, South Australia|Regency Park]]
| near-sw  = [[Woodville Gardens, South Australia|Woodville Gardens]]
| near-s   = [[Ferryden Park, South Australia|Ferryden Park]]
| near-se  = 
| dist1    = 10
| location1= Adelaide
|latd=34.86
|longd=138.56
|use_lga_map=yes
}}

'''Angle Park''' is a north-western [[suburb]] of [[Adelaide]] 10&nbsp;km from the [[Central Business District|CBD]], in the state of [[South Australia]], [[Australia]] and falls under the [[City of Port Adelaide Enfield]]. It is adjacent to [[Wingfield, South Australia|Wingfield]], [[Mansfield Park, South Australia|Mansfield Park]], [[Ferryden Park, South Australia|Ferryden Park]], and [[Regency Park, South Australia|Regency Park]]. The post code for Angle Park is 5010. It is bounded to the north by [[Grand Junction Road, Adelaide|Grand Junction Road]], west by Trafford Street, and to the east by Days Road and the [[North-South Motorway]].

== History ==

Prior to the Second World War, Angle Park mainly consisted of pastures and open land. Its development as a residential area coincided with the post-war boom in the 1950s. It is home to a large number of houses owned by the [[South Australian Housing Trust]]. However, in the eastern side of the suburb, there is a variety of light industry and warehouses.

In 1960, the Angle Park Boys Technical High School was constructed on what is now the [[Parks Community Centre]], followed by the Angle Park Girls Technical High School the following year. They operated separately until 1977 when they combined to become the Angle Park High School. The complex was expanded in 1979 to become the [[Parks Community Centre]] in 1979, which included a council library, with the high school becoming known as the [[Parks High School]].

In the 1990s, plans were made for the Westwood Urban Renewal project, of which [[Ferryden Park, South Australia|Ferryden Park]], [[Mansfield Park, South Australia|Mansfield Park]], [[Woodville Gardens, South Australia|Woodville Gardens]], and [[Athol Park, South Australia|Athol Park]] are also a part. This involved the demolition of many of the Housing Trust homes, to be replaced by either private housing, or a lesser number of newer, townhouse-style housing trust homes. The building began in 2001, and as of 2004 and 2005, began to affect Angle Park.

== Facilities ==
[[Image:Parks statue.jpg|Sculpture at the Parks Community Centre|thumb]]
There were three primary schools in the area, being the public [[Mansfield Park Primary School]], established in 1969,closed in 2010, and the [[Catholic]] [[St Patrick's School (Mansfield Park, South Australia)|St Patrick's School]], both of which are in the adjacent [[Mansfield Park, South Australia|Mansfield Park]], and the [[Ferryden Park Primary School]] which also closed in 2010 and was replaced with a new R-7 superschool which opened at Woodville Gardens at the start of 2011.<ref>http://www.wgs.sa.edu.au/</ref> There is a small shopping complex at the corner of Trafford and Wilson Streets, and the [[Parks Community Centre]], provides a library, health centre, gym and fitness centre, swimming complex, council office, and a few other community services. The [[Greyhound Park]] racing facility exists on the eastern side of the suburb, and it is also the site of a fire brigade unit and an [[ETSA Utilities]] manufacturing plant where [[Stobie pole]]s are assembled. It was also the site of the [[Parks High School]], which was closed at the end of 1996, due to declining enrolments. Currently, the local public zone high school is [[Woodville High school]], in [[Woodville, South Australia]]. However, a large number of the families in the area choose to send their children to other private schools, mainly in the city centre, which was one of the causes of the closure of the Parks High School in 1996 and the nearby Croydon High school in 2006.{{citation needed|date=March 2016}}

== Transport ==

Trafford Street is served by the 251(311) and 252(312) bus services, while the 239 & 250(316) serves Days and Grand Junction Road. The 361 serves Grand Junction Road.

== Demographics ==
[[Image:Nursing home angle park.JPG|Salvation Army nursing home at Angle Park.|thumb]]
Angle Park is home to a large immigrant population. The initial settlement of the suburb coincided with a large wave of immigrants from Eastern Europe arriving in Australia, following the Second World War. Although most of their children have moved on, a large number of immigrant retirees still live in the area. Another wave of immigration occurred in the 1980s, when Vietnamese immigrants arrived after the Vietnam War. Currently, more than 50% of the population is from a non-English speaking background.

== Politics ==
Angle Park is located within the federal seat of [[Division of Port Adelaide|Port Adelaide]]. It is the most pro-[[Australian Labor Party]] area in South Australia, recording the highest two-party-preferred vote in the state for the ALP at the [[Mansfield Park Primary School]] booth (the closest booth to Angle Park) at the 1998, 2001 and 2004 federal elections, garnering more than 80% on each occasion.

==References==
{{reflist}}
Lewis, H.J., ''Enfield and the northern villages'', Corporation of  the City of Enfield, 1985

{{Commons category|Angle Park, South Australia}}
{{City of Port Adelaide Enfield suburbs}}

[[Category:Suburbs of Adelaide]]