{{Use dmy dates|date=April 2014}}
{{COI|date=August 2009}}
{{Infobox accounting body
|abbreviation          = CICA
|name_in_local_language=
|image                 = CICA_logo.gif
|image_border          =
|image_caption         = 
|motto                 = 
|predecessor           = Dominion Association of Chartered Accountants
|formation             = {{Start date and years ago|df=yes|1902|05|15}}
|legal_status          = Incorporated by Private Act (SC 1902, c. 58)
|objective             = Our mission is to foster public confidence in the CA profession by acting in the public interest and helping our members excel.
|headquarters          = [[Toronto|Toronto, Ontario]], Canada
|coords                = 
|region_served         = Canada, Bermuda
|membership_num        = 78,000 
|students_num          = 
|members_designations  = CA, FCA
|language              = English, French
|highest_officer_title = Chair
|highest_officer_name  = [[Bill MacKinnon]] 
|second_officer_title  = Vice Chair
|second_officer_name   = [[Shelley Brown]]
|third_officer_title   = President and CEO
|third_officer_name    = [[Kevin Dancey]]
|fourth_officer_title  = 
|fourth_officer_name   = 
|administering_organ   = board of directors
|IFAC_membership_date  = 1977
|additional_data_name1 = 
|additional_data1      = 
|additional_data_name2 = 
|additional_data2      = 
|additional_data_name3 =
|additional_data3      =
|num_staff             =
|website               = {{url|http://www.cica.ca}}
|remarks               =
}}

The '''Canadian Institute of Chartered Accountants''' was incorporated by an Act of the Parliament of Canada in 1902.<ref>{{Cite web| author = Stephen Bernhut |title = Setting the standard  | url = http://www.camagazine.com/archives/print-edition/2002/may/features/camagazine23639.aspx | publisher = CA Magazine | date = May 2002 | accessdate = 6 November 2011}}</ref> This Act, now known as the ''Canadian Institute of Chartered Accountants Act'', was last amended in 1990 to reflect the CICA's current mandate and powers.<ref>SC 1902, c. 58, as amended by SC 1951, c. 89 and SC 1990, c. 52</ref>

Working in collaboration with its provincial member organizations, the CICA supports the setting of accounting, auditing and assurance standards for business, not-for-profit organizations and government, and develops and delivers education programs. It also provides a range of member services and professional literature; undertakes research and development of intellectual property; issues guidance on risk management and governance; and fosters relationships with key stakeholders nationally and internationally.

==About the CICA==

Chartered Accountants (CAs) constitute the oldest of Canada's three national accounting bodies, and the CICA represents Canada's CAs both nationally and internationally.  The CICA is a founding member of the [[International Federation of Accountants]] and the [[Global Accounting Alliance]].

==History==

* 1902 – The Dominion Association of Chartered Accountants (DACA) is incorporated by Private Act of the Parliament of Canada. (SC 1902, c. 58)
* 1934 – The Canada Companies Act is amended to provide for the involvement of the DACA in setting standards for accounting policies.
* 1938 – All provinces agree that membership should be determined by a [[Uniform Evaluation]].<ref>{{Cite web|title = The UFE |url = http://www.cica.ca/becoming-a-ca/the-ufe/ |accessdate = 18 March 2014}}</ref>
* 1951 – DACA changes its name to the Canadian Institute of Chartered Accountants. (SC 1951, c. 89)
* 1954 – Students in all provinces in Canada are writing the same examination.
* 1968 – The CICA releases the ''CICA Handbook'', which codifies [[Generally Accepted Accounting Principles (Canada)|Canadian GAAP]].
* 1972 – The [[Canadian Securities Administrators]] rule that provincial securities commissions must consider the ''CICA Handbook'' as the basis for Canadian GAAP.<ref>{{Cite web|title = National Instrument 52–107 ''Acceptable Accounting Principles and Auditing Standards'' and its Companion Policy |url = http://www.osc.gov.on.ca/en/SecuritiesLaw_rule_20101210_52-107-cp_consolidated.htm |accessdate = 7 November 2011}}</ref>
* 1975 – The ''CICA Handbook'' is incorporated by reference into the [[Canada Business Corporations Act]] for specifying the basis of Canadian GAAP.<ref>{{Cite web|title = Canada Business Corporations Regulations, s. 70 |url = http://laws-lois.justice.gc.ca/eng/regulations/SOR-2001-512/page-13.html#h-31 |accessdate = 7 November 2011}}</ref>
* 1990 – Act of incorporation is updated to reflect the current mandate and powers. (SC 1990, c. 52)
* 2008 – The CICA announces that Canadian GAAP will converge with [[International Financial Reporting Standards]] for publicly accountable enterprises, effective with reporting periods beginning on or after 1 January 2011.<ref>{{Cite web|title = International Financial Reporting Standards |url = http://www.acsbcanada.org/international-activities/ifrs/item48766.aspx |accessdate = 7 November 2011}}</ref>

A merger was proposed between the CICA and [[Certified Management Accountants of Canada|CMA Canada]] in 2004, but it did not go beyond exploratory talks. Discussions began again in 2011, this time including [[Certified General Accountants Association of Canada|CGA Canada]],<ref>{{Cite web|title = Your source for CA-CMA-CGA Merger Information |url = http://cpacanada.ca/ |accessdate = 6 November 2011}}</ref> to create a new national accounting body which would issue the new [[professional certification|professional designation]] of [[Chartered Professional Accountant]].

==Qualification as a member==
CAs are admitted to the profession through their Provincial Institutes/Ordre. These bodies are responsible for establishing and administering the qualification process, admission criteria and performance standards within their jurisdictions.

Pre-qualification education is delivered regionally through one of four systems across Canada:

{| border="1" cellpadding="5" cellspacing="0"
|- 
! scope="col" | Region
! scope="col" | Education provider
|- valign="top"
! scope="row" align="left"| Atlantic provinces
| [http://www.asca.ns.ca/  Atlantic School of Chartered Accountancy]
|- valign="top"
! scope="row" align="left"| Quebec
| [http://ocaq.qc.ca/become-ca/becoming-a-ca/education-in-quebec-canada/undergraduate-diploma-recognized-in-quebec.html Professional Education Program of the Ordre des comptables agréés du Québec]
|- valign="top"
! scope="row" align="left"| Ontario
| [http://www.cpaontario.ca/Students/SchoolofAccountancy(SOA)/1014page1143.aspx CPA Ontario School of Accountancy]
|- valign="top"
! scope="row" align="left"| British Columbia, Alberta, Saskatchewan, Manitoba and the territories
| [http://www.casb.com/ CA School of Business]
|}

The CICA's role – in concert with all provinces, territories – is to develop and maintain consistent, uniform standards for the profession's qualification process. These standards ensure the portability of the CA designation across Canada and internationally through various mutual recognition agreements.

Admission to the CA profession requires:

* A university degree.
* Specified university courses or the equivalent.
* Completion of a professional pre-qualification program as noted above.
* Prescribed practical experience with approved training offices, which are set in place in accredited CA firms, offices of provincial or national Auditors General, and selected corporations and government organizations.<ref>{{Cite web|title = CA Training Offices | url = http://www.catrainingoffice.ca/}}</ref>
* A passing grade in the [[Uniform Evaluation]].

==Foreign trained accountants==
The CICA itself does not admit members and students; its membership is derived from membership in one of the 10 provincial and two territorial institutes of chartered accountants and the [[Institute of Chartered Accountants of Bermuda]].   The institutes are empowered by local laws to regulate and govern the chartered accountancy profession within their jurisdictions. Individuals holding foreign accountancy designations who want to become a Canadian Chartered Accountant must apply to the institute in the jurisdiction where they live or intend to live.

The CICA works in partnership with and on behalf of the institutes to:

* Assess the qualification processes of foreign accounting bodies to determine the extent to which they are equivalent to the Canadian process
* Negotiate Mutual Recognition Agreements with accounting bodies whose qualification processes are substantially equivalent;
* Determine the additional education, evaluation and experience requirements for members of reviewed accounting bodies not deemed substantially equivalent.

Credentials of foreign-trained accountants are assessed according to the following categories:<ref>{{Cite web| title = International Qualifications Appraisal Board  | url = http://www.cica.ca/become-a-ca/internationally-trained-accountants/international-qualifications-appraisal-board/index.aspx | accessdate = 7 November 2011}}</ref>

{| border="1" cellpadding="5" cellspacing="0"
|- 
! scope="col" | Category
! scope="col" | Description
|- valign="top"
! scope="row" align="left"| '''Designated Accounting Bodies'''
| These designated accounting bodies are considered by the Canadian CA profession to be substantially equivalent.  Members of these accounting bodies generally qualify for membership in the Canadian CA profession with minimal additional requirements.
|- valign="top"
! scope="row" align="left"| '''Accounting Bodies with Reciprocal Membership Agreements'''
| 
|- valign="top"
! scope="row" align="left"| '''Non-Equivalent Accounting Bodies'''
| These accounting bodies have been reviewed by IQAB and their qualification processes have been determined not to be equivalent to the Canadian qualification process.
|- valign="top"
! scope="row" align="left"| '''Accounting Bodies Currently Under Review'''
| 
|- valign="top"
! scope="row" align="left"| '''Members of Non-Assessed Accounting Bodies'''
| Individuals who are members of professional accounting bodies which have not been reviewed by IQAB, or who are not members in any professional accounting body can request individual assessment by the appropriate PICA/Ordre.  This review will identify any additional education, examination and/or experience requirements the individual must complete to qualify for membership in the Canadian CA profession.
|}

Assessments of foreign bodies, and negotiations of Mutual Recognition agreements are conducted by the profession's International Qualifications Appraisal Board (IQAB), the current status of which is as follows:

{| border="1" cellpadding="5" cellspacing="0"
|- 
! scope="col" width="120" |  Country
! scope="col" width="170"|  Designated accounting body
! scope="col" width="170"|  Accounting body with Reciprocal Membership Agreement
! scope="col" width="170"| Non-equivalent accounting body
! scope="col" width="170"| Currently under review
|- valign="top"
! scope="row" align="left"| Australia
| [[Institute of Chartered Accountants in Australia]]
| 
| [[CPA Australia]]
| 
|- valign="top"
! scope="row" align="left"| Belgium
| [http://www.ibr-ire.be/fra Institut des Réviseurs d'Enterprises de Belgique]
| 
| 
| 
|- valign="top"
! scope="row" align="left"| France
| [[Ordre des Experts Comptables]]
| 
| 
| 
|- valign="top"
! scope="row" align="left"| Hong Kong
| [[Hong Kong Institute of Certified Public Accountants]]
| 
| 
| 
|- valign="top"
! scope="row" align="left"| India
| 
| [[The Institute of Chartered Accountants of India]]
| 
| 
|- valign="top"
! scope="row" align="left"| Ireland
| [[Institute of Chartered Accountants in Ireland]]
| 
| 
| 
|- valign="top"
! scope="row" align="left"| Japan
| [[Japanese Institute of Certified Public Accountants]]
| 
| 
| 
|- valign="top"
! scope="row" align="left"| Luxembourg
| 
| 
| 
| [http://www.ire.lu Institut des réviseurs d’entreprises]
|- valign="top"
! scope="row" align="left"| Mexico
| [http://www.imcp.org.mx Instituto Mexicano de Contadores Publicos]
| 
| 
| 
|- valign="top"
! scope="row" align="left"| Netherlands
| [http://www.nivra.nl Nederland Instituut van Register Accountants]
| 
| 
| 
|- valign="top"
! scope="row" align="left"| New Zealand
| [[New Zealand Institute of Chartered Accountants]]
| 
| 
| 
|- valign="top"
! scope="row" align="left"| Nigeria
| 
| 
| 
| [[Institute of Chartered Accountants of Nigeria]]
|- valign="top"
! scope="row" align="left"| Pakistan
| 
| 
| [[Institute of Chartered Accountants of Pakistan]] ''currently under review''
|
|- valign="top"
! scope="row" align="left"| Philippines
| 
| 
| [[Philippine Institute of Certified Public Accountants]]
|
|- valign="top"
! scope="row" align="left"| South Africa
| [[South African Institute of Chartered Accountants]]
| 
| 
| 
|- valign="top"
! scope="row" align="left"| Sri Lanka
| 
| 
| [[Institute of Chartered Accountants of Sri Lanka]]
|
|- valign="top"
! scope="row" align="left"| Switzerland
| 
| 
| 
| [http://www.treuhand-kammer.ch Swiss Institute of Certified Accountants and Tax Consultants]
|- valign="top"
! scope="row" align="left"| United Kingdom
| [[Institute of Chartered Accountants in England and Wales]], [[Institute of Chartered Accountants of Scotland]]
| 
| [[Association of Chartered Certified Accountants]]
| 
|- valign="top"
! scope="row" align="left"| United States
| [[American Institute of Certified Public Accountants]], [[National Association of State Boards of Accountancy]]
| 
| 
| 
|- valign="top"
! scope="row" align="left"| Zimbabwe
| [[Institute of Chartered Accountants of Zimbabwe]]
| 
| 
| 
|}

==See also==
* [[International Qualification Examination]]
* [[Uniform Evaluation]]
*[[Accounting Standards Board (Canada)]]

==Member institutes==

{| border="1" cellpadding="5" cellspacing="0"
|- 
! scope="col" width="200" |  Province
! scope="col" width="300"|  Institute/Ordre
|- valign="top"
! scope="row" align="left"| British Columbia
| [http://www.ica.bc.ca The Institute of Chartered Accountants of British Columbia]
|- valign="top"
! scope="row" align="left"| Alberta
| [http://www.icaa.ab.ca The Institute of Chartered Accountants of Alberta]
|- valign="top"
! scope="row" align="left"| Saskatchewan
| [http://www.icas.sk.ca The Institute of Chartered Accountants of Saskatchewan]
|- valign="top"
! scope="row" align="left"| Manitoba
| [http://www.icam.mb.ca Institute of Chartered Accountants of Manitoba]
|- valign="top"
! scope="row" align="left"| Ontario
| [http://cpaontario.ca Chartered Professional Accountants Ontario]
|- valign="top"
! scope="row" align="left"| Quebec
| [http://www.ocaq.qc.ca Ordre des comptables professionnels agréés du Québec]
|- valign="top"
! scope="row" align="left"| New Brunswick
| [http://www.nbica.org The New Brunswick Institute of Chartered Accountants]
|- valign="top"
! scope="row" align="left"| Nova Scotia
| [http://www.icans.ns.ca The Institute of Chartered Accountants of Nova Scotia]
|- valign="top"
! scope="row" align="left"| Prince Edward Island
| [http://www.icapei.com Institute of Chartered Accountants of Prince Edward Island]
|- valign="top"
! scope="row" align="left"| Newfoundland and Labrador
| [http://www.ican.nfld.net The Institute of Chartered Accountants of Newfoundland and Labrador]
|- valign="top"
! scope="row" align="left"| Yukon
| [http://www.icayk.ca Institute of Chartered Accountants of the Yukon Territory]
|- valign="top"
! scope="row" align="left"| Northwest TerritoriesNunavut
| [http://www.icanwt.nt.ca Institute of Chartered Accountants of the Northwest Territories and Nunavut]
|- valign="top"
! scope="row" align="left"| Bermuda
| [http://www.icab.bm Institute of Chartered Accountants of Bermuda]
|}

==References==
{{Reflist}}

{{IFAC Members}}

{{DEFAULTSORT:Canadian Institute Of Chartered Accountants}}
[[Category:Accounting in Canada]]
[[Category:Auditing organizations]]
[[Category:Canadian accounting associations]]