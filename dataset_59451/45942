{{use dmy dates|date=February 2013}}
{{Infobox cricket tournament
| name = 2011–12 Caribbean Twenty20
| image =
| administrator = [[West Indies Cricket Board|WICB]]
| cricket format = [[Twenty20]]
| tournament format = [[Round-robin tournament|Round-robin]] and [[Single-elimination tournament|knockout]]
| host = {{cr|WIN}}
| champions = {{cr|TTO}}
| count = 2
| participants = 10
| matches = 24
| attendance =
| player of the series = [[Kieron Pollard]]
| most runs = [[Johnson Charles]] (207)
| most wickets = [[Krishmar Santokie]] (14)
| website = [http://ct20.windiescricket.com/ ct20.windiescricket.com]
| previous_year = 2010–11
| previous_tournament = 2010–11 Caribbean Twenty20
| next_year = 2012–13
| next_tournament = 2012–13 Caribbean Twenty20
}}
The '''2011–12 Caribbean Twenty20''' was the third season of the [[Caribbean Twenty20]], a domestic T20 tournament administered by the [[West Indies Cricket Board]]. The opening match was held on 9 January 2012, and the final was played at [[Kensington Oval]], [[Barbados]] on 22 January 2012. The national teams of Canada and Netherlands in addition to Sussex participated as the overseas teams. There were ten teams as it was in the 2010–11 tournament.

[[Trinidad and Tobago national cricket team|Trinidad and Tobago]] won the tournament and, as the best performing domestic team, qualified for the [[2012 Champions League Twenty20]] (qualifying stage).

== Venues ==
[[Queen's Park Oval]] in [[Trinidad and Tobago]] was initially a venue for some of the preliminary matches, but eventually was cancelled due to nonavailability of the ground on 4 January.<ref>{{cite news |url=http://www.antiguaobserver.com/?p=68331 |title=Caribbean T20 preliminary matches shifted to SVRCG |first=Vanroy |last=Burnes |date=7 December 2011 |publisher=Antigua Observer |accessdate=16 August 2012 }}</ref> All matches were played at the following two grounds:

<center>
{| class="wikitable" style="text-align:center;"
|-
! width="50%" | North Sound, [[Antigua]]
! width="50%" | [[Bridgetown]], [[Barbados]]
|-
| [[Sir Vivian Richards Stadium]]
| [[Kensington Oval]]
|-
| Capacity: 10,000
| Capacity: 15,000
|-
| [[Image:Sir Vivian Richards Stadium aerial view Oct 2006.jpg|150px]]
| [[Image:Kensington Oval, Barbados During 2007 World Cup Cricket Final.jpg|150px]]
|-
| colspan=2 |<center>{{location map+ |Caribbean|float=none |width=375 |caption= |places=
{{location map~ |Caribbean|lat=17.0833 |long=-61.7333 |label=North Sound |position=left}}
{{location map~ |Caribbean|lat=13.0947 |long=-59.6175 |label=[[Bridgetown]] |position=left}} }}</center>
|}
</center>

==Format==
The format was the same as it was in 2010–11. The tournament consisted of 24 matches, divided into a group stage and a knockout stage. The group stage had the teams divided into two equal groups, with each playing a [[round-robin tournament]]. The top two teams of each group advanced to the knockout stage. The knockout stage consisted of two semi-finals, a third-place playoff and the grand final. The semi-finals had the top team of one group facing the second from the other. The winners of the semi-finals played the grand final to determine the winner of the competition, while the losers of the semi-finals played the third-place playoff.

Points in the group stage were awarded as follows:

{| class="wikitable"
|+Points
|-
! colspan="1"|Results
! colspan="1"|Points
|-
|Win||4 points
|-
|No result||1 point
|-
|Loss||0 points
|}

== Teams ==
The following ten teams participated in the tournament:

{{col-begin}}
{{col-2}}
'''[[#Group A|Group A]]'''
* {{cr|CAN}}<sup>†</sup><ref>{{cite news |url=http://www.newsday.co.tt/sport/0,149248.html |title=Netherlands, Sussex set for regional T20 |date=20 October 2011 |publisher=Trinidad and Tobago's Newsday |accessdate=16 August 2012 }}</ref>
* {{cr|GUY}}
* {{flagicon|WIN}} [[Leeward Islands cricket team|Leeward Islands]]
* {{cr|TTO}}
* {{flagicon|WIN}} [[Windward Islands cricket team|Windward Islands]]

{{col-2}}
'''[[#Group B|Group B]]'''
* {{cr|BAR}}
* {{flagicon|WIN}} [[Combined Campuses and Colleges cricket team|Combined Campuses and Colleges]]
* {{cr|JAM}}
* {{cr|NED}}<sup>†</sup><ref name=ned>{{cite news |url=http://www.espncricinfo.com/westindies/content/story/537209.html |title=Netherlands, Sussex to play Caribbean T20 |date=20 October 2011 |publisher=ESPN |work=CricInfo |accessdate=6 January 2012 }}</ref>
* {{flagicon|ENG}} [[Sussex County Cricket Club|Sussex]]<sup>†</sup><ref name=ned/>
{{col-end}}
<sup>†</sup>Invited overseas team

==Results==

===Group stage===

====Group A====
{| class="wikitable" style="text-align:center"
!width=240|Team
!width=20|{{Tooltip | Pld | Played}}
!width=20|{{Tooltip | W | Won}}
!width=20|{{Tooltip | L | Lost}}
!width=20|{{Tooltip | NR | No result}}
!width=50|{{Tooltip | [[Net Run Rate|NRR]] | Net run rate}}
!width=20|{{Tooltip | Pts | Points}}
|- style="background:#cfc;"
|align="left"|{{flagicon|WIN}} [[Windward Islands cricket team|Windward Islands]] || 4 || 4 || 0 || 0 || +0.817 || '''16'''
|- style="background:#cfc;"
|align="left"|{{cr|TTO}} || 4 || 3 || 1 || 0 || +2.578 || '''12'''
|-
|align="left"|{{cr|GUY}} || 4 || 2 || 2 || 0 || +0.106 || '''8'''
|-
|align="left"|{{flagicon|WIN}} [[Leeward Islands cricket team|Leeward Islands]] || 4 || 1 || 3 || 0 || –2.024 || '''4'''
|-
|align="left"|{{cr|CAN}} || 4 || 0 || 4 || 0 || –1.553 || '''0'''
|}

{{Cricket match summary |bg=#eee
| date = 9 January
| daynight = yes
| team1 = [[Windward Islands cricket team|Windward Islands]] {{flagicon|WIN}}
| score1 = 129/4 (20 overs)
| score2 = 118/9 (20 overs)
| team2 = {{cr|TTO}}
| result = [[#match1|Windward Islands won by 11 runs]]
| scorecard = [http://cricketarchive.com/Archive/Scorecards/392/392036.html Scorecard]
| venue = [[Sir Vivian Richards Stadium]], North Sound, [[Antigua]]
}}
{{Cricket match summary
| date = 9 January
| daynight = yes
| team1 = [[Leeward Islands cricket team|Leeward Islands]] {{flagicon|WIN}}
| score1 = 134/6 (20 overs)
| score2 = 139/5 (19.2 overs)
| team2 = {{cr|GUY}}
| result = [[#match2|Guyana won by 5 wickets]]
| scorecard = [http://cricketarchive.com/Archive/Scorecards/392/392037.html Scorecard]
| venue = [[Sir Vivian Richards Stadium]], North Sound, [[Antigua]]
}}
{{Cricket match summary |bg=#eee
| date = 11 January
| daynight = yes
| team1 = {{cr-rt|CAN}}
| score1 = 111/7 (20 overs)
| score2 = 114/1 (16.3 overs)
| team2 = {{flagicon|WIN}} [[Windward Islands cricket team|Windward Islands]]
| result = [[#match5|Windwards won by 9 wickets]]
| scorecard = [http://cricketarchive.com/Archive/Scorecards/392/392040.html Scorecard]
| venue = [[Sir Vivian Richards Stadium]], North Sound, [[Antigua]]
}}
{{Cricket match summary
| date = 11 January
| daynight = yes
| team1 = {{cr-rt|TTO}}
| score1 = 211/3 (20 overs)
| score2 = 44 (12.5 overs)
| team2 = {{flagicon|WIN}} [[Leeward Islands cricket team|Leeward Islands]]
| result = [[#match6|Trinidad and Tobago won by 167 runs]]
| scorecard = [http://cricketarchive.com/Archive/Scorecards/392/392041.html Scorecard]
| venue = [[Sir Vivian Richards Stadium]], North Sound, [[Antigua]]
}}
{{Cricket match summary |bg=#eee
| date = 13 January
| daynight = yes
| team1 = {{cr-rt|CAN}}
| score1 = 108 (19.3 overs)
| score2 = 109/2 (13.4 overs)
| team2 = {{flagicon|WIN}} [[Leeward Islands cricket team|Leeward Islands]]
| result = [[#match9|Leeward Islands won by 8 wickets]]
| scorecard = [http://cricketarchive.com/Archive/Scorecards/392/392044.html Scorecard]
| venue = [[Sir Vivian Richards Stadium]], North Sound, [[Antigua]]
}}
{{Cricket match summary
| date = 13 January
| daynight = yes
| team1 = {{cr-rt|GUY}}
| score1 = 101/9 (20 overs)
| score2 = 104/3 (17.1 overs)
| team2 = {{cr|TTO}}
| result = [[#match10|Trinidad and Tobago won by 7 wickets]]
| scorecard = [http://cricketarchive.com/Archive/Scorecards/392/392045.html Scorecard]
| venue = [[Sir Vivian Richards Stadium]], North Sound, [[Antigua]]
}}
{{Cricket match summary |bg=#eee
| date = 17 January
| daynight = yes
| team1 = {{cr-rt|CAN}}
| score1 = 143/7 (20 overs)
| score2 = 144/2 (17 overs)
| team2 = {{cr|GUY}}
| result = [[#match13|Guyana won by 8 wickets]]
| scorecard = [http://www.espncricinfo.com/caribbean-t20-12/engine/match/538744.html Scorecard]
| venue = [[Kensington Oval]], [[Bridgetown]], [[Barbados]]
| rain =
}}
{{Cricket match summary
| date = 17 January
| daynight = yes
| team1 = [[Windward Islands cricket team|Windward Islands]] {{flagicon|WIN}}
| score1 = 157 (19.5 overs)
| score2 = 135 (19.2 overs)
| team2 = {{flagicon|WIN}} [[Leeward Islands cricket team|Leeward Islands]]
| result = [[#match14|Windward Islands won by 22 runs]]
| scorecard = [http://cricketarchive.com/Archive/Scorecards/392/392049.html Scorecard]
| venue = [[Kensington Oval]], [[Bridgetown]], [[Barbados]]
}}
{{Cricket match summary |bg=#eee
| date = 19 January
| daynight = yes
| team1 = {{cr-rt|CAN}}
| score1 = 99/9 (20 overs)
| score2 = 91/2 (15.1 overs)
| team2 = {{cr|TTO}}
| result = [[#match17|Trinidad and Tobago won by 8 wickets (D/L)]]
| scorecard = [http://cricketarchive.com/Archive/Scorecards/392/392052.html Scorecard]
| venue = [[Kensington Oval]], [[Bridgetown]], [[Barbados]]
| rain = Rain interrupted Trinidad & Tobago's innings, reducing it to 18 overs and setting a revised target of 91 runs under the [[Duckworth–Lewis method]].
}}
{{Cricket match summary
| date = 19 January
| daynight = yes
| team1 = {{cr-rt|GUY}}
| score1 = 140/8 (20 overs)
| score2 = 119/5 (16 overs)
| team2 = {{flagicon|WIN}} [[Windward Islands cricket team|Windward Islands]]
| result = [[#match18|Windward Islands won by 5 wickets (D/L)]]
| scorecard = [http://cricketarchive.com/Archive/Scorecards/392/392053.html Scorecard]
| venue = [[Kensington Oval]], [[Bridgetown]], [[Barbados]]
| rain = Rain interrupted Windward Islands' innings, reducing it to 16 overs and setting a revised target of 119 runs under the [[Duckworth–Lewis method]].
}}

====Group B====
{| class="wikitable" style="text-align:center"
!width=240|Team
!width=20|{{Tooltip | Pld | Played}}
!width=20|{{Tooltip | W | Won}}
!width=20|{{Tooltip | L | Lost}}
!width=20|{{Tooltip | NR | No result}}
!width=50|{{Tooltip | [[Net Run Rate|NRR]] | Net run rate}}
!width=20|{{Tooltip | Pts | Points}}
|- style="background:#cfc;"
|align="left"|{{cr|BAR}} || 4 || 4 || 0 || 0 || +3.582 || '''16'''
|- style="background:#cfc;"
|align="left"|{{cr|JAM}} || 4 || 3 || 1 || 0 || +0.157 || '''12'''
|-
|align="left"|{{flagicon|WIN}} [[Combined Campuses and Colleges cricket team|Combined Campuses and Colleges]] || 4 || 1 || 3 || 0 || –0.947 || '''4'''
|-
|align="left"|{{flagicon|ENG}} [[Sussex County Cricket Club|Sussex]] || 4 || 1 || 3 || 0 || –0.988 || '''4'''
|-
|align="left"|{{cr|NED}} || 4 || 1 || 3 || 0 || –1.487 || '''4'''
|}

{{Cricket match summary |bg=#eee
| date = 10 January
| daynight = yes
| team1 = [[Sussex County Cricket Club|Sussex]] {{flagicon|ENG}}
| score1 = 125/9 (20 overs)
| score2 = 106/8 (20 overs)
| team2 = {{cr|NED}}
| result = [[#match3|Sussex won by 34 runs]]
| scorecard = [http://cricketarchive.com/Archive/Scorecards/392/392038.html Scorecard]
| venue = [[Sir Vivian Richards Stadium]], North Sound, [[Antigua]]
}}
{{Cricket match summary
| date = 10 January
| daynight = yes
| team1 = [[Combined Campuses and Colleges cricket team|Combined Campuses and Colleges]] {{flagicon|WIN}}
| score1 = 131/9 (20 overs)
| score2 = 133/8 (19.3 overs)
| team2 = {{cr|JAM}}
| result = [[#match4|Jamaica won by 2 wickets]]
| scorecard = [http://cricketarchive.com/Archive/Scorecards/392/392039.html Scorecard]
| venue = [[Sir Vivian Richards Stadium]], North Sound, [[Antigua]]
}}
{{Cricket match summary |bg=#eee
| date = 12 January
| daynight = yes
| team1 = {{cr-rt|JAM}}
| score1 = 152/5 (20 overs)
| score2 = 102 (19.2 overs)
| team2 = {{flagicon|ENG}} [[Sussex County Cricket Club|Sussex]]
| result = [[#match7|Jamaica won by 50 runs]]
| scorecard = [http://cricketarchive.com/Archive/Scorecards/392/392042.html Scorecard]
| venue = [[Sir Vivian Richards Stadium]], North Sound, [[Antigua]]
}}
{{Cricket match summary
| date = 12 January
| daynight = yes
| team1 = {{cr-rt|NED}}
| score1 = 73 (20 overs)
| score2 = 76/0 (7.3 overs)
| team2 = {{cr|BAR}}
| result = [[#match8|Barbados won by 10 wickets]]
| scorecard = [http://cricketarchive.com/Archive/Scorecards/392/392043.html Scorecard]
| venue = [[Sir Vivian Richards Stadium]], North Sound, [[Antigua]]
}}
{{Cricket match summary |bg=#eee
| date = 14 January
| daynight = yes
| team1 = [[Combined Campuses and Colleges cricket team|Combined Campuses and Colleges]] {{flagicon|WIN}}
| score1 = 130/5 (20 overs)
| score2 = 101/9 (20 overs)
| team2 = {{flagicon|ENG}} [[Sussex County Cricket Club|Sussex]]
| result = [[#match11|Combined Campuses and Colleges won by 29 runs]]
| scorecard = [http://www.espncricinfo.com/caribbean-t20-12/engine/match/538742.html Scorecard]
| venue = [[Sir Vivian Richards Stadium]], North Sound, [[Antigua]]
}}
{{Cricket match summary
| date = 14 January
| daynight = yes
| team1 = {{cr-rt|BAR}}
| score1 = 157/7 (20 overs)
| score2 = 95 (16.5 overs)
| team2 = {{cr|JAM}}
| result = [[#match12|Barbados won by 62 runs]]
| scorecard = [http://www.espncricinfo.com/caribbean-t20-12/engine/match/538743.html Scorecard]
| venue = [[Sir Vivian Richards Stadium]], North Sound, [[Antigua]]
}}
{{Cricket match summary |bg=#eee
| date = 18 January
| daynight = yes
| team1 = {{cr-rt|NED}}
| score1 = 160/3 (20 overs)
| score2 = 134/7 (20 overs)
| team2 = {{flagicon|WIN}} [[Combined Campuses and Colleges cricket team|Combined Campuses and Colleges]]
| result = [[#match15|Netherlands won by 26 runs]]
| scorecard = [http://cricketarchive.com/Archive/Scorecards/392/392050.html Scorecard]
| venue = [[Kensington Oval]], [[Bridgetown]], [[Barbados]]
}}
{{Cricket match summary
| date = 18 January
| daynight = yes
| team1 = [[Sussex County Cricket Club|Sussex]] {{flagicon|ENG}}
| score1 = 89 (18 overs)
| score2 = 90/2 (14.4 overs)
| team2 = {{cr|BAR}}
| result = [[#match16|Barbados won by 8 wickets]]
| scorecard = [http://cricketarchive.com/Archive/Scorecards/392/392051.html Scorecard]
| venue = [[Kensington Oval]], [[Bridgetown]], [[Barbados]]
| rain =
}}
{{Cricket match summary |bg=#eee
| date = 20 January
| daynight = yes
| team1 = {{cr-rt|NED}}
| score1 = 116 (19.4 overs)
| score2 = 118/2 (17.2 overs)
| team2 = {{cr|JAM}}
| result = [[#match19|Jamaica won by 8 wickets]]
| scorecard = [http://www.espncricinfo.com/caribbean-t20-12/engine/match/538750.html Scorecard]
| venue = [[Kensington Oval]], [[Bridgetown]], [[Barbados]]
}}
{{Cricket match summary
| date = 20 January
| daynight = yes
| team1 = [[Combined Campuses and Colleges cricket team|Combined Campuses and Colleges]] {{flagicon|WIN}}
| score1 = 76 (20 overs)
| score2 = 81/1 (10 overs)
| team2 = {{cr|BAR}}
| result = [[#match20|Barbados won by 9 wickets]]
| scorecard = [http://www.espncricinfo.com/caribbean-t20-12/engine/match/538751.html Scorecard]
| venue = [[Kensington Oval]], [[Bridgetown]], [[Barbados]]
}}

===Knockout stage===
{{4TeamBracket-with 3rd
|RD1-seed1=A1
|RD1-team1={{flagicon|WIN}} [[Windward Islands cricket team|Windward Islands]]
|RD1-score1=[[#match-sf1|98/8 (20 ov)]]
|RD1-seed2=B2
|RD1-team2='''{{cr|JAM}}'''
|RD1-score2='''[[#match-sf1|99/5 (18 ov)]]'''
|RD1-seed3=B1
|RD1-team3={{cr|BAR}}
|RD1-score3=[[#match-sf2|90 (19.5 ov)]]
|RD1-seed4=A2
|RD1-team4='''{{nobr|{{cr|TTO}}}}'''
|RD1-score4='''{{nobr|[[#match-sf2|93/5 (14 ov)]]}}'''
|RD2-seed1=B2
|RD2-team1={{cr|JAM}}
|RD2-score1=[[#match-f|105/5 (20 ov)]]
|RD2-seed2=A2
|RD2-team2='''{{nobr|{{cr|TTO}}}}'''
|RD2-score2='''[[#match-f|168/6 (20 ov)]]'''
|RD3-seed1=A1
|RD3-team1='''{{flagicon|WIN}} [[Windward Islands cricket team|Windward Islands]]'''
|RD3-score1='''{{nobr|[[#match-3rd|105/3 (17.5 ov)]]}}'''
|RD3-seed2=B1
|RD3-team2={{cr|BAR}}
|RD3-score2=[[#match-3rd|101 (20 ov)]]
}}

==Fixtures==
:''All times shown are in [[Organisation of Eastern Caribbean States|Eastern Caribbean]] Time ([[UTC−04]]).''

===Group stage===

====Group A ====
{{Limited overs matches
| date = {{anchor|match1}} 9 January
| daynight = yes
| time = 16:00
| team1 = [[Windward Islands cricket team|Windward Islands]] {{flagicon|WIN}}
| score1 = 129/4 (20 overs)
| score2 = 118/9 (20 overs)
| team2 = {{cr|TTO}}
| runs1 = [[Darren Sammy]] 41* (31)
| wickets1 = [[Samuel Badree]] 2/17 (4 overs)
| runs2 = [[Darren Bravo]] 51 (42)
| wickets2 = [[Shane Shillingford]] 3/21 (4 overs)
| result = Windward Islands won by 11 runs
| report = [http://cricketarchive.com/Archive/Scorecards/392/392036.html Scorecard]
| venue = [[Sir Vivian Richards Stadium]], North Sound, [[Antigua]]
| umpires = [[Gregory Brathwaite]] and [[Clancy Mack]]
| motm = [[Darren Sammy]] (WI)
| toss = Windward Islands won the toss and elected to bat.
}}
----
{{Limited overs matches
| date = {{anchor|match2}} 9 January
| daynight = yes
| time = 20:00
| team1 = [[Leeward Islands cricket team|Leeward Islands]] {{flagicon|WIN}}
| score1 = 134/6 (20 overs)
| score2 = 139/5 (19.2 overs)
| team2 = {{cr|GUY}}
| runs1 = [[Justin Athanaze]] 30[[Not out|*]] (15)
| wickets1 = [[Steven Jacobs (cricketer)|Steven Jacobs]] 2/18 (4 overs)
| runs2 = [[Leon Johnson (cricketer)|Leon Johnson]] 37[[Not out|*]] (32)
| wickets2 = [[Gavin Tonge]] 1/11 (4 overs)
| result = Guyana won by 5 wickets
| report = [http://cricketarchive.com/Archive/Scorecards/392/392037.html Scorecard]
| venue = [[Sir Vivian Richards Stadium]], North Sound, [[Antigua]]
| umpires = [[Vincent Bullen]] and [[Peter Nero (umpire)|Peter Nero]]
| motm = [[Leon Johnson (cricketer)|Leon Johnson]] (Guy)
| toss = Leewards won the toss and elected to bat.
}}
----
{{Limited overs matches
| date = {{anchor|match5}} 11 January
| daynight = yes
| time = 16:00
| team1 = {{cr-rt|CAN}}
| score1 = 111/7 (20 overs)
| score2 = 114/1 (16.3 overs)
| team2 = {{flagicon|WIN}} [[Windward Islands cricket team|Windward Islands]]
| runs1 = [[Ruvindu Gunasekera]] 38 (31)
| wickets1 = [[Liam Sebastien]] 1/17 (4 overs)
| runs2 = [[Johnson Charles]] 61[[Not out|*]] (58)
| wickets2 = [[Henry Osinde]] 1/13 (3 overs)
| result = Windwards won by 9 wickets
| report = [http://cricketarchive.com/Archive/Scorecards/392/392040.html Scorecard]
| venue = [[Sir Vivian Richards Stadium]], North Sound, [[Antigua]]
| umpires = [[Vincent Bullen]] and [[Peter Nero (umpire)|Peter Nero]]
| motm = [[Johnson Charles]] (WI)
| toss = Canada won the toss and elected to bat.
}}
----
{{Limited overs matches
| date = {{anchor|match6}} 11 January
| daynight = yes
| time = 20:00
| team1 = {{cr-rt|TTO}}
| score1 = 211/3 (20 overs)
| score2 = 44 (12.5 overs)
| team2 = {{flagicon|WIN}} [[Leeward Islands cricket team|Leeward Islands]]
| runs1 = [[Kieron Pollard]] 56[[not out|*]] (15)
| wickets1 = [[Chesney Hughes]] 1/20 (2 overs)
| runs2 = [[Chesney Hughes]] 16 (25)
| wickets2 = [[Sunil Narine]] 4/8 (2 overs)
| result = Trinidad and Tobago won by 167 runs
| report = [http://cricketarchive.com/Archive/Scorecards/392/392041.html Scorecard]
| venue = [[Sir Vivian Richards Stadium]], North Sound, [[Antigua]]
| umpires = [[Gregory Brathwaite]] and [[Nigel Duguid]]
| motm = [[Kieron Pollard]] (T&T)
| toss = Trinidad and Tobago won the toss and elected to bat.
}}
----
{{Limited overs matches
| date = {{anchor|match9}} 13 January
| daynight = yes
| time = 16:00
| team1 = {{cr-rt|CAN}}
| score1 = 108 (19.3 overs)
| score2 = 109/2 (13.4 overs)
| team2 = {{flagicon|WIN}} [[Leeward Islands cricket team|Leeward Islands]]
| runs1 = [[Raza-ur-Rehman]] 30 (31)
| wickets1 = [[Gavin Tonge]] 3/23 (4 overs)
| runs2 = [[Kieran Powell]] 47[[Not out|*]] (42)
| wickets2 = [[Rizwan Cheema]] 1/15 (3 overs)
| result = Leeward Islands won by 8 wickets
| report = [http://cricketarchive.com/Archive/Scorecards/392/392044.html Scorecard]
| venue = [[Sir Vivian Richards Stadium]], North Sound, [[Antigua]]
| umpires = [[Nigel Duguid]] and [[Joel Wilson (umpire)|Joel Wilson]]
| motm = [[Gavin Tonge]] (LI)
| toss = Canada won the toss and elected to bat
}}
----
{{Limited overs matches
| date = {{anchor|match10}} 13 January
| daynight = yes
| time = 20:00
| team1 = {{cr-rt|GUY}}
| score1 = 101/9 (20 overs)
| score2 = 104/3 (17.1 overs)
| team2 = {{cr|TTO}}
| runs1 = [[Trevon Griffith]] 31 (28)
| wickets1 = [[Kevon Cooper]] 3/26 (4 overs)
| runs2 = [[Lendl Simmons]] 30 (17)
| wickets2 = [[Royston Crandon]] 1/15 (4 overs)
| result = Trinidad and Tobago won by 7 wickets
| report = [http://cricketarchive.com/Archive/Scorecards/392/392045.html Scorecard]
| venue = [[Sir Vivian Richards Stadium]], North Sound, [[Antigua]]
| umpires = [[Gregory Brathwaite]] and [[Vincent Bullen]]
| motm = [[Kevon Cooper]] (T&T)
| toss = Trinidad and Tobago won the toss and elected to field
}}
----
{{Limited overs matches
| date = {{anchor|match13}} 17 January
| daynight = yes
| time = 16:00
| team1 = {{cr-rt|CAN}}
| score1 = 143/7 (20 overs)
| score2 = 144/2 (17 overs)
| team2 = {{cr|GUY}}
| runs1 = [[Jimmy Hansra]] 40 (36)
| wickets1 = [[Devendra Bishoo]] 2/21 (4 overs)
| runs2 = [[Narsingh Deonarine]] 52[[Not out|*]] (32)
| wickets2 = [[Rizwan Cheema]] 1/21 (3 overs)
| result = Guyana won by 8 wickets
| report = [http://www.espncricinfo.com/caribbean-t20-12/engine/match/538744.html Scorecard]
| venue = [[Kensington Oval]], [[Bridgetown]], [[Barbados]]
| umpires = [[Gregory Brathwaite]] and [[Joel Wilson (umpire)|Joel Wilson]]
| motm = [[Narsingh Deonarine]] (Guy)
| toss = Canada won the toss and elected to bat
| rain =
}}
----
{{Limited overs matches
| date = {{anchor|match14}} 17 January
| daynight = yes
| time = 20:00
| team1 = [[Windward Islands cricket team|Windward Islands]] {{flagicon|WIN}}
| score1 = 157 (19.5 overs)
| score2 = 135 (19.2 overs)
| team2 = {{flagicon|WIN}} [[Leeward Islands cricket team|Leeward Islands]]
| runs1 = [[Darren Sammy]] 25 (15)
| wickets1 = [[Justin Athanaze]] 3/12 (4 overs)
| runs2 = [[Kieran Powell]] 30 (18)
| wickets2 = [[Shane Shillingford]] 4/22 (4 overs)
| result = Windward Islands won by 22 runs
| report = [http://cricketarchive.com/Archive/Scorecards/392/392049.html Scorecard]
| venue = [[Kensington Oval]], [[Bridgetown]], [[Barbados]]
| umpires = [[Nigel Duguid]] and [[Peter Nero (umpire)|Peter Nero]]
| motm = [[Shane Shillingford]] (WI)
| toss = Windward Islands won the toss and elected to bat
}}
----
{{Limited overs matches
| date = {{anchor|match17}} 19 January
| daynight = yes
| time = 16:00
| team1 = {{cr-rt|CAN}}
| score1 = 99/9 (20 overs)
| score2 = 91/2 (15.1 overs)
| team2 = {{cr|TTO}}
| runs1 = [[Hiral Patel]] 32 (36)
| wickets1 = [[Ravi Rampaul]] 4/26 (4 overs)
| runs2 = [[Darren Bravo]] 42[[Not out|*]] (41)
| wickets2 = [[Henry Osinde]] 1/15 (4 overs)
| result = Trinidad and Tobago won by 8 wickets (D/L)
| report = [http://cricketarchive.com/Archive/Scorecards/392/392052.html Scorecard]
| venue = [[Kensington Oval]], [[Bridgetown]], [[Barbados]]
| umpires = [[Gregory Brathwaite]] and [[Clancy Mack]]
| motm = [[Darren Bravo]] (T&T)
| toss = Canada won the toss and elected to bat
| rain = Rain interrupted Trinidad & Tobago's innings, reducing it to 18 overs and setting a revised target of 91 runs under the [[Duckworth–Lewis method]].
}}
----
{{Limited overs matches
| date = {{anchor|match18}} 19 January
| daynight = yes
| time = 20:00
| team1 = {{cr-rt|GUY}}
| score1 = 140/8 (20 overs)
| score2 = 119/5 (16 overs)
| team2 = {{flagicon|WIN}} [[Windward Islands cricket team|Windward Islands]]
| runs1 = [[Sewnarine Chattergoon]] 27 (22)
| wickets1 = [[Darren Sammy]] 3/11 (4 overs)
| runs2 = [[Johnson Charles]] 65 (43)
| wickets2 = [[Devendra Bishoo]] 3/15 (4 overs)
| result = Windward Islands won by 5 wickets (D/L)
| report = [http://cricketarchive.com/Archive/Scorecards/392/392053.html Scorecard]
| venue = [[Kensington Oval]], [[Bridgetown]], [[Barbados]]
| umpires = [[Vincent Bullen]] and [[Joel Wilson (umpire)|Joel Wilson]]
| motm = [[Darren Sammy]] (WI)
| toss = Windward Islands won the toss and elected to field
| rain = Rain interrupted Windward Islands' innings, reducing it to 16 overs and setting a revised target of 119 runs under the [[Duckworth–Lewis method]].
}}

====Group B ====
{{Limited overs matches
| date = {{anchor|match3}} 10 January
| daynight = yes
| time = 16:00
| team1 = [[Sussex County Cricket Club|Sussex]] {{flagicon|ENG}}
| score1 = 125/9 (20 overs)
| score2 = 106/8 (20 overs)
| team2 = {{cr|NED}}
| runs1 = [[Ben Brown (cricketer)|Ben Brown]] 42 (42)
| wickets1 = [[Timm van der Gugten]] 5/21 (4 overs)
| runs2 = [[Peter Borren]] 45 (40)
| wickets2 = [[Naved Arif]] 3/12 (3.3 overs)
| result = Sussex won by 34 runs
| report = [http://cricketarchive.com/Archive/Scorecards/392/392038.html Scorecard]
| venue = [[Sir Vivian Richards Stadium]], North Sound, [[Antigua]]
| umpires = [[Nigel Duguid]] and [[Joel Wilson (umpire)|Joel Wilson]]
| motm = [[Timm van der Gugten]] (Ned)
| toss = Sussex won the toss an elected to bat.
}}
----
{{Limited overs matches
| date = {{anchor|match4}} 10 January
| daynight = yes
| time = 20:00
| team1 = [[Combined Campuses and Colleges cricket team|Combined Campuses and Colleges]] {{flagicon|WIN}}
| score1 = 131/9 (20 overs)
| score2 = 133/8 (19.3 overs)
| team2 = {{cr|JAM}}
| runs1 = [[Chadwick Walton]] 66 (59)
| wickets1 = [[Krishmar Santokie]] 4/17 (4 overs)
| runs2 = [[Marlon Samuels]] 66[[Not out|*]] (52)
| wickets2 = [[Yannick Ottley]] 3/20 (4 overs)
| result = Jamaica won by 2 wickets
| report = [http://cricketarchive.com/Archive/Scorecards/392/392039.html Scorecard]
| venue = [[Sir Vivian Richards Stadium]], North Sound, [[Antigua]]
| umpires = [[Clancy Mack]] and [[Peter Nero (umpire)|Peter Nero]]
| motm = [[Marlon Samuels]] (Jam)
| toss = Combined Campuses and Colleges won the toss and elected bat.
}}
----
{{Limited overs matches
| date = {{anchor|match7}} 12 January
| daynight = yes
| time = 16:00
| team1 = {{cr-rt|JAM}}
| score1 = 152/5 (20 overs)
| score2 = 102 (19.2 overs)
| team2 = {{flagicon|ENG}} [[Sussex County Cricket Club|Sussex]]
| runs1 = [[Nkruma Bonner]] 66[[Not out|*]] (57)
| wickets1 = [[Joe Gatting]] 1/12 (1 overs)
| runs2 = [[Chris Nash|Christopher Nash]] 26 (28)
| wickets2 = [[Odean Brown]] 3/17 (4 overs)
| result = Jamaica won by 50 runs
| report = [http://cricketarchive.com/Archive/Scorecards/392/392042.html Scorecard]
| venue = [[Sir Vivian Richards Stadium]], North Sound, [[Antigua]]
| umpires = [[Vincent Bullen]] and [[Joel Wilson (umpire)|Joel Wilson]]
| motm = [[Nkruma Bonner]] (Jam)
| toss = Jamaica won the toss and elected to bat.
}}
----
{{Limited overs matches
| date = {{anchor|match8}} 12 January
| daynight = yes
| time = 20:00
| team1 = {{cr-rt|NED}}
| score1 = 73 (20 overs)
| score2 = 76/0 (7.3 overs)
| team2 = {{cr|BAR}}
| runs1 = [[Michael Swart]] 16 (16)
| wickets1 = [[Ashley Nurse]] 3/7 (4 overs)
| runs2 = [[Dwayne Smith]] 49[[Not out|*]] (27)
| wickets2 =
| result = Barbados won by 10 wickets
| report = [http://cricketarchive.com/Archive/Scorecards/392/392043.html Scorecard]
| venue = [[Sir Vivian Richards Stadium]], North Sound, [[Antigua]]
| umpires = [[Nigel Duguid]] and [[Clancy Mack (umpire)|Clancy Mack]]
| motm = [[Ashley Nurse]] (Bar)
| toss = Netherlands won the toss and elected to bat.
}}
----
{{Limited overs matches
| date = {{anchor|match11}} 14 January
| daynight = yes
| time = 16:00
| team1 = [[Combined Campuses and Colleges cricket team|Combined Campuses and Colleges]] {{flagicon|WIN}}
| score1 = 130/5 (20 overs)
| score2 = 101/9 (20 overs)
| team2 = {{flagicon|ENG}} [[Sussex County Cricket Club|Sussex]]
| runs1 = [[Romel Currency]] 48 (48)
| wickets1 = [[Chris Liddle]] 2/17 (4 overs)
| runs2 = [[Joe Gatting]] 37 (32)
| wickets2 = [[Romel Currency]] 4/8 (4 overs)
| result = Combined Campuses and Colleges won by 29 runs
| report = [http://www.espncricinfo.com/caribbean-t20-12/engine/match/538742.html Scorecard]
| venue = [[Sir Vivian Richards Stadium]], North Sound, [[Antigua]]
| umpires = [[Gregory Brathwaite]] and [[Clancy Mack]]
| motm = [[Romel Currency]] (CCC)
| toss = Combined Campuses and Colleges won the toss and elected to bat
}}
----
{{Limited overs matches
| date = {{anchor|match12}} 14 January
| daynight = yes
| time = 20:00
| team1 = {{cr-rt|BAR}}
| score1 = 157/7 (20 overs)
| score2 = 95 (16.5 overs)
| team2 = {{cr|JAM}}
| runs1 = [[Dwayne Smith]] 86 (57)
| wickets1 = [[Krishmar Santokie]] 2/27 (4 overs)
| runs2 = [[Nkruma Bonner]] 27 (31)
| wickets2 = [[Tino Best]] 2/17 (4 overs)
| result = Barbados won by 62 runs
| report = [http://www.espncricinfo.com/caribbean-t20-12/engine/match/538743.html Scorecard]
| venue = [[Sir Vivian Richards Stadium]], North Sound, [[Antigua]]
| umpires = [[Peter Nero (umpire)|Peter Nero]] and [[Joel Wilson (umpire)|Joel Wilson]]
| motm = [[Dwayne Smith]] (Bar)
| toss = Barbados won the toss and elected to bat
}}
----
{{Limited overs matches
| date = {{anchor|match15}} 18 January
| daynight = yes
| time = 16:00
| team1 = {{cr-rt|NED}}
| score1 = 160/3 (20 overs)
| score2 = 134/7 (20 overs)
| team2 = {{flagicon|WIN}} [[Combined Campuses and Colleges cricket team|Combined Campuses and Colleges]]
| runs1 = [[Michael Swart]] 57 (43)
| wickets1 = [[Kyle Mayers]] 1/22 (4 overs)
| runs2 = [[Kyle Mayers]] 23 (28)
| wickets2 = [[Pieter Seelaar]] 2/27 (4 overs)
| result = Netherlands won by 26 runs
| report = [http://cricketarchive.com/Archive/Scorecards/392/392050.html Scorecard]
| venue = [[Kensington Oval]], [[Bridgetown]], [[Barbados]]
| umpires = [[Vincent Bullen]] and [[Clancy Mack]]
| motm = [[Michael Swart]] (Ned)
| toss = Netherlands won the toss and elected to bat
}}
----
{{Limited overs matches
| date = {{anchor|match16}} 18 January
| daynight = yes
| time = 20:00
| team1 = [[Sussex County Cricket Club|Sussex]] {{flagicon|ENG}}
| score1 = 89 (18 overs)
| score2 = 90/2 (14.4 overs)
| team2 = {{cr|BAR}}
| runs1 = [[Michael Yardy]] 33 (34)
| wickets1 = [[Tino Best]] 3/11 (3 overs)
| runs2 = [[Kevin Stoute]] 37[[Not out|*]] (48)
| wickets2 = [[Will Beer]] 1/10 (4 overs)
| result = Barbados won by 8 wickets
| report = [http://cricketarchive.com/Archive/Scorecards/392/392051.html Scorecard]
| venue = [[Kensington Oval]], [[Bridgetown]], [[Barbados]]
| umpires = [[Nigel Duguid]] and [[Peter Nero (umpire)|Peter Nero]]
| motm = [[Tino Best]] (Bar)
| toss = Sussex won the toss and elected to bat
| rain =
}}
----
{{Limited overs matches
| date = {{anchor|match19}} 20 January
| daynight = yes
| time = 16:00
| team1 = {{cr-rt|NED}}
| score1 = 116 (19.4 overs)
| score2 = 118/2 (17.2 overs)
| team2 = {{cr|JAM}}
| runs1 = [[Eric Szwarczynski]] 44 (45)
| wickets1 = [[Krishmar Santokie]] 4/13 (3.4 overs)
| runs2 = [[Nkrumah Bonner]] 35 (34)
| wickets2 = [[Pieter Seelaar]] 1/14 (4 overs)
| result = Jamaica won by 8 wickets
| report = [http://www.espncricinfo.com/caribbean-t20-12/engine/match/538750.html Scorecard]
| venue = [[Kensington Oval]], [[Bridgetown]], [[Barbados]]
| umpires = [[Gregory Brathwaite]] and [[Peter Nero (umpire)|Peter Nero]]
| motm = [[Krishmar Santokie]] (Jam)
| toss = The Netherlands won the toss and elected to bat
}}
----
{{Limited overs matches
| date = {{anchor|match20}} 20 January
| daynight = yes
| time = 20:00
| team1 = [[Combined Campuses and Colleges cricket team|Combined Campuses and Colleges]] {{flagicon|WIN}}
| score1 = 76 (20 overs)
| score2 = 81/1 (10 overs)
| team2 = {{cr|BAR}}
| runs1 = [[Floyd Reifer]] 20 (22)
| wickets1 = [[Fidel Edwards]] 2/19 (4 overs)
| runs2 = [[Kevin Stoute]] 36[[Not out|*]] (29)
| wickets2 = [[Jason Dawes]] 1/28 (2 overs)
| result = Barbados won by 9 wickets
| report = [http://www.espncricinfo.com/caribbean-t20-12/engine/match/538751.html Scorecard]
| venue = [[Kensington Oval]], [[Bridgetown]], [[Barbados]]
| umpires = [[Nigel Duguid]] and [[Joel Wilson (umpire)|Joel Wilson]]
| motm = [[Fidel Edwards]] (Bar)
| toss = Combined Campuses and Colleges won the toss and elected to bat
}}

===Knockout stage===
;Semi-finals
{{Limited overs matches
| date = {{anchor|match-sf1}} 21 January
| daynight = yes
| time = 16:00
| team1 = [[Windward Islands cricket team|Windward Islands]] {{flagicon|WIN}}
| score1 = 98/8 (20 overs)
| score2 = 99/5 (18 overs)
| team2 = {{cr|JAM}}
| runs1 = [[Lindon James]] 37* (41)
| wickets1 = [[Odean Brown]] 2/15 (4 overs)
| runs2 = [[David Bernard Jr.]] 22 (20)
| wickets2 = [[Shane Shillingford]] 1/15 (4 overs)
| result = Jamaica won by 5 wickets
| report = [http://www.espncricinfo.com/caribbean-t20-12/engine/match/538752.html Scorecard]
| venue = [[Kensington Oval]], [[Bridgetown]], [[Barbados]]
| umpires = [[Gregory Brathwaite]] and [[Joel Wilson (umpire)|Joel Wilson]]
| motm = [[Odean Brown]] (Jam)
| toss = Windward Islands won the toss and elected to bat
}}
----
{{Limited overs matches
| date = {{anchor|match-sf2}} 21 January
| daynight = yes
| time = 20:00
| team1 = {{cr-rt|BAR}}
| score1 = 90 (19.5 overs)
| score2 = 93/5 (14 overs)
| team2 = {{cr|TTO}}
| runs1 = [[Shane Dowrich]] 22 (31)
| wickets1 = [[Dwayne Bravo]] 3/23 (4 overs)
| runs2 = [[Kieron Pollard]] 31* (23)
| wickets2 = [[Ryan Hinds]] 2/11 (2 overs)
| result = Trinidad & Tobago won by 5 wickets
| report = [http://www.espncricinfo.com/caribbean-t20-12/engine/match/538753.html Scorecard]
| venue = [[Kensington Oval]], [[Bridgetown]], [[Barbados]]
| umpires = [[Nigel Duguid]] and [[Clancy Mack]]
| motm = [[Sunil Narine]] (T&T)
| toss = Barbados won the toss and elected to bat
}}

;Third-place playoff
{{Limited overs matches
| date = {{anchor|match-3rd}} 22 January
| daynight = yes
| time = 16:00
| team1 = {{cr-rt|BAR}}
| score1 = 101 (20 overs)
| score2 = 105/3 (17.5 overs)
| team2 = {{flagicon|WIN}} [[Windward Islands cricket team|Windward Islands]]
| runs1 = [[Jonathan Carter (cricketer)|Jonathan Carter]] 42 (53)
| wickets1 = [[Delorn Johnson]] 5/5 (4 overs)
| runs2 = [[Andre Fletcher]] 42 (39)
| wickets2 = [[Fidel Edwards]] 1/15 (4 overs)
| result = Windward Islands won by 7 wickets
| report = [http://www.espncricinfo.com/caribbean-t20-12/engine/match/538754.html Scorecard]
| venue = [[Kensington Oval]], [[Bridgetown]], [[Barbados]]
| umpires = [[Peter Nero (umpire)|Peter Nero]] and [[Joel Wilson (umpire)|Joel Wilson]]
| motm = [[Delorn Johnson]] (WI)
| toss = Barbados won the toss and elected to bat
}}

;Final
{{Limited overs matches
| date = {{anchor|match-f}} 22 January
| daynight = yes
| time = 20:00
| team1 = {{cr-rt|TTO}}
| score1 = 168/6 (20 overs)
| score2 = 105/5 (20 overs)
| team2 = {{cr|JAM}}
| runs1 = [[Dwayne Bravo]] 49 (43)
| wickets1 = [[Odean Brown]] 3/22 (4 overs)
| runs2 = [[Carlton Baugh]] 39* (23)
| wickets2 = [[Samuel Badree]] 1/14 (4 overs)
| result = Trinidad & Tobago won by 63 runs
| report = [http://www.espncricinfo.com/caribbean-t20-12/engine/match/538755.html Scorecard]
| venue = [[Kensington Oval]], [[Bridgetown]], [[Barbados]]
| umpires = [[Gregory Brathwaite]] and [[Nigel Duguid]]
| motm = [[Dwayne Bravo]] (T&T)
| toss = Trinidad & Tobago won the toss and elected to bat
}}

==References==
{{Reflist}}

==External links==
*[http://ct20.windiescricket.com/ Official Caribbean T20 website]
*[http://espncricinfo.com/caribbean-t20-12/content/series/537231.html CricInfo Caribbean T20 2011/12 website]

{{Caribbean Twenty20}}

{{DEFAULTSORT:2011-12 Caribbean Twenty20}}
[[Category:2012 in cricket|C]]
[[Category:Caribbean Twenty20]]
[[Category:2011–12 West Indian cricket season]]
[[Category:Domestic cricket competitions in 2011–12|Caribbean Twenty20]]