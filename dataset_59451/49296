{{Infobox artist
| name          = LigoranoReese
| birth_date    = 1956, Ligorano; 1955, Reese
| birth_place   = Gettysburg, PA, Ligorano; Washington, D.C., Reese
| nationality   = American
| field         = [[Conceptual Art]], [[Ice Sculpture]], [[Installation art]], [[New media art]]
| training      = [[Maryland Institute College of Art]], Ligorano; [[Pomona College]], Reese
| patrons       = 
| influenced by =
| influenced    = 
| awards        =  
}}

'''LigoranoReese''' is the collaborative name of '''Nora Ligorano''' and '''Marshall Reese''', artists who’ve worked together since the mid-eighties. Their artwork falls within the fields of new technology, [[ice sculpture]], [[installation art]], [[video art]], [[artists books]] and limited edition multiples.

== Background ==

Nora Ligorano met Marshall Reese in 1977 while studying painting at the [[Maryland Institute College of Art]] in Baltimore. Reese moved to Baltimore in 1977 after a year abroad as a student at [[Pomona College]]. From 1978 to 1980, he co-published with Kirby Malone the small press magazine ''Epod''<ref>{{cite web|title=Epod magazine #3|url=http://epc.buffalo.edu/library/e-pod-3_1978.pdf|accessdate=31 August 2014}}</ref> that featured early [[language poetry]] and performance scores and began experimenting with visual and [[sound poetry]] as a member of CoAccident a collective of poets and musicians. Ligorano and Reese began working collaboratively in that context.<ref name="meaning">{{cite web|title=M/E/A/N/I/N/G Online#2|url=http://writing.upenn.edu/epc/meaning/02/ligorano-reese.html|accessdate=1 September 2014}}</ref>
In 1982 Ligorano was awarded a [[Fulbright]] scholarship in design arts in Spain. Reese moved with Ligorano to Barcelona, where they made their first video art piece, relocating to Madrid the following year. In 1984, Reese moved to New York City to pursue interests in video art. Ligorano joined him one year later in Williamsburg Brooklyn. In New York, both of them continued working in [[performance art]] and video expanding into other media including installations, artists books and limited edition multiples.<ref name="meaning" />

== Works ==

=== New Technology ===

In 2001, [[The Kitchen]] and [[MIT MediaLab]] commissioned LigoranoReese to make an interactive installation for the exhibition ID/entity.<ref>{{cite journal|last1=Iverson|first1=Hana|title=Mirror Mirror|journal=Afterimage|date=Spring 2002|volume=29|issue=5|accessdate=31 August 2014}}</ref> The artists’ ''Van Eyck’s Mirror'' is an installation based on the [[Arnolfini Portrait]] using video, sensors and computer controls. This was among LigoranoReese’s first artworks that incorporated interactive technology.

In 2003 they made ''In Memory of Truth'' an installation using a microprojection system designed by the artists and the optical engineer Tony Cappo. The centerpiece of the installation is a magnifying glass on a pedestal with a primary lens that miniaturizes Hollywood war films to fit on the head of a pin. Holland Cotter described the piece “a tour-de-force.”<ref>{{cite news|last1=Cotter|first1=Holland|title=With Politics in the Air, a Freedom Free-for-All Comes to Town|publisher=New York Times|url=https://www.nytimes.com/2008/09/23/arts/design/23demo.html?pagewanted=all|accessdate=31 August 2014|date=September 22, 2008}}</ref>
''History’s Garden'' 2006 was the next installation using a similar microprojection system. Mounting a prism and lens on a mechanical arm, they replaced the moving counterweight of a metronome with a small projection screen on which images of refugees from Central Europe and the Middle East are projected.<ref>{{cite web|title=New Media When, Neuberger Museum, SUNY Purchase|url=https://www.neuberger.org/exhibitions.php?view=126|website=https://www.neuberger.org/|accessdate=1 September 2014}}</ref>

With funding from [[NYSCA]] and the Jerome Foundation in 2003 they started research and development on illuminating woven fiber optic thread. From 2003 – 2007 they worked with Eric Singer on developing the hardware. In 2009 while residents at [[Eyebeam Art and Technology Center|Eyebeam]],<ref>{{Cite web|title = Ligorano/Reese {{!}} eyebeam.org|url = http://eyebeam.org/people/ligoranoreese|website = eyebeam.org|access-date = 2016-01-28}}</ref> LigoranoReese met Luke Loeffler and devised programming with him for the fabric to respond to live information from the internet. Calling this body of work “fiber optic data tapestries” the artists completed ''50 Different Minds''<ref>{{cite journal|last1=Avila|first1=Susan Taber|title=Textile Tweets|journal=Fiber Arts|date=Spring 2011|accessdate=31 August 2014}}</ref> in 2010, which uses Twitter feeds and air flight data to make patterns and colors on its surface.

In 2013 they debuted a new tapestry called ''I•AM•I'' at Miami Art Project with [[Catharine Clark Gallery]]. ''I•AM•I'' is a woven personal data portrait using [[Fitbit]] activity and responses to a self-reporting emotional survey to create an abstract portrait of the artists’ subjects. Two of these pieces are in the collections of the University of Wyoming and 21C Museum in Louisville.

[[Megan Prelinger]] in her book ''Inside the Machine: Art and Invention in the Electronic Age''<ref>{{cite book|last1=Prelinger|first1=Megan|title=Inside the Machine: Art and Invention in the Electronic Age|date=2015|publisher=W. W. Norton & Company, Inc.|location=New York, N.Y.|isbn=978-0-393-08359-0|page=121|edition=First}}</ref> notes:
<blockquote>... these characteristics allowed microcircuits to retain a legacy association with textile crafts, an association that has been encouraged in the intervening decades by the networked nature of the electronic systems that now structure everyday life. The twenty-first century textile artists LigoranoReese writing about their electronic tapestries woven of fiber-optic thread, explain their approach to integrating electronics with weaving: 'Weaving is a social activity. It is about threading narratives and mythology, even language and accounting, with (the) [[quipu]]. Weaving is a shared tradition common to cultures throughout the world in the same way that computers and networks have flattened the world, making communication and exchange more common.'</blockquote>

=== Ice Sculptures ===

In 2006, on the third anniversary of the Iraq War, the artists installed the word ''Democracy'' sculpted from 2000 pounds of ice in the garden of Jim Kempner Fine Art in New York City. They called it ''The State of Things'' and photographed and filmed it while it disappeared. This project began a series of public art events LigoranoReese call “temporary monuments.”

In 2008, [[Provisions Library]] invited the artists to participate in BrushFire, public art interventions by a number of artists during the campaign season in the Midwest.<ref>{{cite web|title=The UnConvention|url=http://theunconvention.com/projects/brushfire/|accessdate=31 August 2014}}</ref> The artists reprised ''The State of Things'' at the conventions in Denver and St. Paul, installing ice sculptures of ''Democracy'' in front of the [[Museum of Contemporary Art Denver]] on the first day of the Democratic Convention and on the state capital grounds in St. Paul on the first day of the Republican Convention.<ref>{{cite news|last1=Bloom|first1=Julie|title=Your (Nonpartisan) Message Here|publisher=New York Times|url=https://www.nytimes.com/2008/08/17/arts/design/17bloo.html?pagewanted=all|accessdate=31 August 2014|date=August 15, 2008}}</ref>

In October that same year, the artists installed an ice sculpture of the word ''Economy'' on the 79th Anniversary of the [[Great Depression]] in front of the New York State Supreme Court Building at Foley Square.<ref>{{cite web|last1=Chung|first1=Jen|title=A Literal Economic Meltdown|url=http://gothamist.com/2008/10/29/a_literal_economic_meltdown.php|website=Gothamist|accessdate=31 August 2014|date=October 29, 2008}}</ref>
In 2010 LigoranoReese made an ice sculpture of the words ''Middle Class'' and filmed it disappearing in Kempner’s garden called ''Morning In America''. [[Senator Bernie Sanders]] featured the time lapse video on his U.S. Senate webpage.<ref>{{cite web|last1=Sanders|first1=Bernie|title=The Melting Middle Class|url=http://www.sanders.senate.gov/newsroom/recent-business/the-melting-middle-class|accessdate=31 August 2014}}</ref> In 2012 at the conventions in Tampa and Charlotte, the artists remade the installation in public parks there.<ref>{{cite journal|last1=Kazakina|first1=Katya|title=Vanishing ‘Middle Class’ Ice Sculpture Hits Conventions|journal=Bloomberg Businessweek|date=August 29, 2012|url=http://www.businessweek.com/news/2012-08-29/vanishing-middle-class-ice-sculpture-hits-conventions|accessdate=31 August 2014}}</ref> The artists installed the ice sculpture ''Dawn of the Anthropocene'' in New York City in front of the Flatiron Building as part of the [[People's Climate March]] on September 21, 2014. It was a 21-foot long sculpture of the words "The Future"<ref>{{cite news|last1=Revkin|first1=Andrew|title=Humanity’s Long Climate and Energy March|url=http://dotearth.blogs.nytimes.com/2014/09/22/humanitys-long-climate-and-energy-march/?_php=true&_type=blogs&_r=0|accessdate=9 October 2014|publisher=New York Times|date=September 22, 2014}}</ref> disappeared in 13 hours.

=== Installation Art ===

LigoranoReese began making video installations in 1992 often as sculptural objects that incorporate video monitors placed inside other media: newspapers, books<ref name="weiss">{{cite web|last1=Weiss|first1=Jason|title=The Book Talks Back:  the video books of Ligorano/Reese|url=http://www.itinerariesofahummingbird.com/ligoranoreese.html|accessdate=31 August 2014}}</ref> and clocks.  These sculptures concerned how electronic networks were changing physical media and transforming the act of reading and sharing information.

[[Johanna Drucker]] commented on their installation ''The Corona Palimpsest'':<ref>{{cite news|last1=Cotter|first1=Holland|title=When Words' Meaning Is Their Look|url=https://www.nytimes.com/1998/10/16/arts/art-review-when-words-meaning-is-in-their-look.html|accessdate=2 September 2014|publisher=New York Times|date=October 16, 1998}}</ref> <blockquote>The current tension of the book reflects the present tense of electronic media continuing to come into being. This is not a contrast between the space of the real and the space of the virtual, but between two modes of imaginative life of thought, language, and the eye, each competing to determine the relations of history, language and idea. As the page was once written so the monitor redraws itself.<ref>{{cite book|last1=Drucker|first1=Johanna|title=Figuring the Word|date=1998|publisher=Granary Books|isbn=1-887123-23-7|page=170|url=http://raley.english.ucsb.edu/wp-content2/uploads/234/Drucker.pdf|accessdate=31 August 2014}}</ref></blockquote>
In 1994 they installed their first public art installation in the windows of the [[Donnell Library Center]]. ''Acid Migration of Culture'' was a 48&nbsp;ft x 8&nbsp;ft photo mural of an open dictionary of cultural terms. Four video monitors displayed statements by artists, politicians and civic leaders on the role of arts in society.<ref name="weiss" /><ref>{{cite web|title=Acid Migration of Culture|url=http://www.leonardo.info/isast/wow/ligorano-wow274.html|publisher=Leonardo On-Line|accessdate=31 August 2014}}</ref>
''Free Speech Zone'' (2004) at the [[Brooklyn Public Library]] Grand Army Plaza Main Branch featured backlit duratrans photos of blind-folded library users and electronic zipper signs of titles and phrases of banned and challenged books. In 2005, they reinstalled it in the windows of the Donnell Library Center.<ref>{{cite news|last1=Strausbaugh|first1=John|title=Artistic Commentary at the Library on the Zeal to Ban Books|publisher=New York Times|url=https://www.nytimes.com/2005/09/26/books/26book.html|accessdate=31 August 2014|date=September 26, 2005}}</ref>

=== Video Art ===

Single channel video art and videos as elements of sculptures, installations and websites is an ongoing activity of their collaboration. They exhibit these in galleries and festivals.

=== Limited Edition Multiples ===

LigoranoReese began making artists books and limited edition multiples in 1992 with the ''Bible Belt'' as an element of a room-size installation with the same name. The edition consisted of a New Testament Bible mounted on a 33-inch belt with a gold-plated Jesus belt buckle.<ref>{{cite web|title=The Allan Chasanoff Book Art Collection, Yale University Art Gallery|url=http://artgallery.yale.edu/collections/objects/bible-belt|accessdate=2 September 2014}}</ref> ''The Bible Belt'' became the first edition piece of the series the Pure Products of America.<ref>{{cite web|last1=Valdez|first1=Sarah|title=Pure Satire|journal=Art on Paper|date=April 2007|url=http://www.pureproductsusa.com/wp-content/uploads/ARTONPAPERlow.pdf|accessdate=31 August 2014}}</ref> In 2001 the edition series became the website pureproductsusa.<ref>{{cite web|title=pureproductsusa|url=http://pureproductsusa.com|accessdate=1 September 2014}}</ref>
''The Bible Belt'' was followed with ''[[Contract with America]] underwear'' in 1995, an edition of cotton underwear briefs with the name of the Republican congressional campaign silkscreened on the waistband, an image of the House Speaker Newt Gingrich on the crotch and the platform of the Contract on the seat. After the artists mailed pairs of the underwear to political leaders in Washington as gifts, they were sued by the Republican National Committee to cease and desist citing trademark infringement.<ref>{{cite news|last1=Morrison|first1=Jim|title=Researching Legal Briefs (the Silk-Screened Kind)|accessdate=31 August 2014|publisher=New York Times|date=June 11, 1995}}</ref>
In 2001 LigoranoReese published the ''W Collection'' on the anniversary of the Supreme Court decision halting the counting of ballots in Florida. The Collection consisted of a ''Bush vs Gore'' dish towel, ''Money/Honey'', and the ''John Ashcroft'' snow globe.<ref>{{cite journal|last1=Tapper|first1=Jake|title=Snow Blind|accessdate=31 August 2014|journal=The New Yorker|date=October 28, 2002|url=http://www.newyorker.com/magazine/2002/10/28/snow-blind}}</ref>
In 2004 the artists published the postcard book ''Line Up'' and sold it in Union Square during the Republican convention in NYC. ''Line Up'' depicted the Bush administration cabinet in the form of mug shots. Madness of Art editions published a limited edition set of digital prints acquired by the [[New York Public Library]].<ref>{{cite news|last1=Johnson|first1=Ken|title=Politically Charged Prints Cause Talking in the Library|url=https://www.nytimes.com/2007/12/04/arts/design/04nypl.html|accessdate=31 August 2014|publisher=New York Times|date=December 3, 2007}}</ref>
LigoranoReese's limited edition art includes mirrors, lenticulars and snow globes.<ref>{{cite news|last1=Murphy|first1=Kate|title=The World Through a Flurry of Snow|url=https://www.nytimes.com/2012/12/20/fashion/the-world-through-a-flurry-of-snow.html|accessdate=31 August 2014|publisher=New York Times|date=December 12, 2012}}</ref>

== Representation ==

LigoranoReese is represented by [[Catharine Clark Gallery]] in San Francisco.

== References ==

{{reflist}}

== External links ==
* [http://ligoranoreese.net LigoranoReese Website]
* [http://meltedaway.com Melted Away Ice Sculpture Website]
* [http://pureproductsusa.com PureProductsUSA Website]

[[Category:New media artists]]
[[Category:Installation artists]]
[[Category:Conceptual artists]]
[[Category:Art duos]]