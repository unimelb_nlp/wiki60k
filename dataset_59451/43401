<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = J-3 Cub
  |image = PiperJ-3Cub02.jpg
  |caption = '''Piper J-3 Cub'''
}}{{Infobox Aircraft Type
  |type = Trainer
  |manufacturer = [[Piper Aircraft]]
  |designer = [[C. G. Taylor]] <br/> [[Walter Jamouneau]]
  |first flight = 1938
  |introduced = 
  |status = 
  |more users = 
  |produced = 1938–1947
  |number built = 19,888 (US built)<ref name="Peperell" /><br>150 (Canadian-built)<ref name="Peperell" /><br>253 TG-8 gliders<ref name="Peperell" />
  |unit cost = $995-$2,461 when new
  |variants with their own articles = [[Piper PA-11|PA-11 Cub Special]] <br/> [[Piper PA-15 Vagabond|PA-15 Vagabond]] <br/> [[Piper PA-16 Clipper|PA-16 Clipper]] <br/> [[Piper PA-18 Super Cub|PA-18 Super Cub]]
}}
|}

The '''Piper J-3 Cub''' is an [[United States|American]] light aircraft that was built between 1937 and 1947 by [[Piper Aircraft]]. The aircraft has a simple, lightweight design which gives it good low speed handling properties and short field performance. The Cub is one of the best known light aircraft of all time. The Cub's simplicity, affordability and popularity — as well as its large production numbers, with nearly 20,000 built in the United States — invokes comparisons to the [[Ford Model T]] [[automobile]].

The Cub was originally intended as a trainer, and saw great popularity in this role and as a general aviation aircraft. Due to its performance, it was well suited a variety of military uses such as reconnaissance, liaison and ground control, and was produced in large numbers during the [[World War II|Second World War]] as the L-4 Grasshopper. Large numbers of Cubs are still flying today. Notably Cubs are highly prized as [[bush aircraft]].

The Cub is a [[high-wing]] [[strut-braced]] [[monoplane]] with a large area rectangular wing. It is powered by an air-cooled piston engine driving a fixed pitch propeller. Its [[fuselage]] is a welded steel frame covered in [[aircraft fabric covering|fabric]], seating two people in [[tandem]].

The aircraft's standard [[chrome yellow]] paint has come to be known as "Cub Yellow" or "Lock Haven Yellow".<ref name="MagnusLord">{{cite web|url = http://www.pipercubforum.com/yellow.htm|title = The story of Cub Yellow|accessdate = 2008-09-15|last = Lord|first = Magnus|authorlink = |date=April 2008}}</ref>

==Pre-war==
The [[Taylor E-2|Taylor E-2 Cub]] first appeared in 1930, built by [[Taylorcraft Aircraft|Taylor Aircraft]] in [[Bradford, Pennsylvania]]. Sponsored by [[William T. Piper]], a Bradford industrialist and investor, the affordable E-2 was meant to encourage greater interest in aviation. Later in 1930, the company went bankrupt, with Piper buying the assets but keeping founder C. Gilbert Taylor on as president. In 1936, an earlier Cub was altered by employee Walter Jamouneau to become the [[Taylor J-2|J-2]] while Taylor was on sick leave. (The coincidence led some to believe that the "J" stood for Jamouneau, while aviation historian Peter Bowers concluded that the letter simply followed the E, F, G, and H models, with the I omitted because it could be mistaken for the numeral one.).<ref name=smith>{{cite web | url=http://www.nasm.si.edu/research/aero/aircraft/piperj3.htm | title=Piper J-3 | work=Aircraft of the Smithsonian | accessdate=2006-04-02 }}</ref><ref>Peter M. Bowers, ''Piper Cubs'' (Tab Books 1993)</ref> When he saw the redesign, Taylor was so incensed that he fired Jamouneau. Piper, however, had encouraged Jamouneau's changes, and hired him back. Piper then bought Taylor's share in the company, paying him [[United States dollar|US$]]250 per month for three years.
<ref name=spence>{{cite web | url=http://www.historynet.com/theyre-not-all-piper-cubs-november-97-aviation-history-feature.htm | title=They're not all Piper Cubs | work=Aviation History | author=Spence, Charles | date=23 September 1997 | accessdate=2011-09-06 }}</ref>

Although sales were initially slow, about 1,200 J-2s were produced before a fire in the Piper factory ended its production in 1938. After Piper moved his company from Bradford to [[Lock Haven, Pennsylvania|Lock Haven]], the J-3, which featured further changes by Jamouneau, replaced the J-2.  The changes mostly amounted to integrating the vertical fin of the tail into the rear fuselage structure and covering it simultaneously with each of the fuselage's sides, changing the rearmost side window's shape to a smoothly curved half-oval outline, and placing a true steerable tailwheel at the rear end of the J-2's leaf spring-style tailskid, linked for its steering function to the lower end of the rudder with springs and lightweight chains to either end of a double-ended rudder control horn. Powered by a 40&nbsp;hp (30&nbsp;kW) engine, in 1938, it sold for just over $1,000.<ref>Piper J-3 Cub Film Series (TM Technologies, footage from 1937-1948 shows step-by-step construction. 110 minutes.)</ref>

A number of different air-cooled engines, most of [[flat-four engine|flat-four]] configuration, were used to power J-3 Cubs, resulting in differing model designations for each type: the '''J3C''' models used the [[Continental O-170|Continental A series]],<ref name="A691" /> the '''J3F''' used the [[Franklin 4AC]],<ref name="A692" /> and the '''J3L''' used the [[Lycoming O-145]].<ref name="A698" /> A very few examples, designated '''J3P''', were equipped with Lenape ''Papoose'' 3-cylinder [[radial engine]]s.<ref name="ATC695" />

The outbreak of hostilities in Europe in 1939, along with the growing realization that the United States might soon be drawn into [[World War II]], resulted in the formation of the [[Civilian Pilot Training Program]] (CPTP). The Piper J-3 Cub became the primary trainer aircraft of the CPTP and played an integral role in its success, achieving legendary status. 75 percent of all new pilots in the CPTP (from a total of 435,165 graduates) were trained in Cubs. By war's end, 80 percent of all United States military pilots had received their initial flight training in Piper Cubs.<ref name=guill>{{cite web | url=http://www.centennialofflight.gov/essay/GENERAL_AVIATION/piper/GA6.htm | title=The Piper Cub | author=Guillemette, Roger | work=US Centennial of Flight Commission | accessdate=2006-04-02 }}</ref>

The need for new pilots created an insatiable appetite for the Cub. In 1940, the year before the United States' entry into the war, 3,016 Cubs had been built; wartime demands soon increased that production rate to one Cub being built every 20 minutes.<ref name=guill />

==Flitfire==
{{Main article|Flitfire}}
[[File:Piper J-3 Flitfire.JPG|thumb|Flitfire, used in RAF Benevolent Fund and war bond efforts]]	
Prior to the United States entering World War II, J-3s were part of a fund-raising program to support the United Kingdom. Billed as a ''Flitfire'', a Piper Cub J3 bearing Royal Air Force insignia was donated by W. T. Piper and Franklin Motors to the [[RAF Benevolent Fund]] to be raffled off. Piper distributors nationwide were encouraged to do the same.  On April 29, 1941 all 48 Flitfire aircraft, one for each of the 48 states that made up the country at that time, flew into [[La Guardia Field]] for a dedication and fundraising event which included Royal Navy officers from the battleship [[HMS Malaya|HMS ''Malaya'']], in New York for repairs, as honored guests.<ref>{{cite journal|title=Shindig at N.Y. Airport Opens Fund Drive for R.A.F.|journal=Life|date=12 May 1941|url=https://books.google.com/books?id=sUwEAAAAMBAJ&pg=PA36&dq=flitfire&hl=en&ei=JmmCTIrlMML38Aa11YX0AQ&sa=X&oi=book_result&ct=result&resnum=5&ved=0CDUQ6AEwBA#v=onepage&q=flitfire&f=false}}</ref><ref>{{cite web|title= Alamo Liaison Squadron|url=http://www.als-cannonfield.com/Flitfire.htm}}</ref> At least three of the original Flitfires have been restored to their original silver-doped finish; one, powered with a Franklin 65 engine, is on display at the [[North Carolina Aviation Museum]] in Asheboro, North Carolina. This Flitfire was flown by [[Orville Wright]] in a war bond promotion campaign.<ref>{{cite web|title=Museum Guide|publisher=North Carolina Aviation Museum}}</ref>

==World War II service==
[[File:USAFM ey22.jpg|thumb|L-4A painted and marked to represent an aircraft that flew in support of the Allied invasion of North Africa in November 1942]]
[[File:DoD USMC 86249.jpg|thumb|A Piper Cub of the 1st Marine Division’s improvised air force snags a message from a patrol on New Britain's north coast.]]
The Piper Cub quickly became a familiar sight. First Lady [[Eleanor Roosevelt]] took a flight in a J-3 Cub, posing for a series of publicity photos to help promote the CPTP. [[Newsreel]]s and newspapers of the era often featured images of wartime leaders, such as Generals [[Dwight Eisenhower]], [[George Patton]] and [[George Marshall]], flying around European battlefields in Piper Cubs. Civilian-owned Cubs joined the war effort as part of the newly formed [[Civil Air Patrol]] (CAP), patrolling the [[East Coast of the United States|Eastern Seaboard]] and [[Gulf Coast of the United States|Gulf Coast]] in a constant search for German [[U-boat]]s and survivors of U-boat attacks.<ref>Campbell, Douglas E., "Volume III: U.S. Navy, U.S. Marine Corps and U.S. Coast Guard Aircraft Lost During World War II Listed by Aircraft Type", Lulu.com,  ISBN 978-1-257-90689-5 (2011), p. 374</ref>{{vs|date=June 2016}}

Piper developed a military variant ("All we had to do," Bill Jr. is quoted as saying, "was paint the Cub olive drab to produce a military airplane"),<ref name=spence/>
variously designated as the '''O-59''' (1941), '''L-4''' (after April 1942), and '''NE''' (U.S. Navy).  The L-4 Grasshopper was mechanically identical to the J-3 civilian Cub, but was distinguishable by the use of a [[Plexiglas]] greenhouse skylight and rear windows for improved visibility, much like the [[Taylorcraft L-2]] and [[Aeronca L-3]] also in use with the US armed forces.  Carrying a single pilot and no passenger, the L-4 had a top speed of {{convert|85|mph|km/h|0|abbr=on}}, a cruise speed of {{convert|75|mph|km/h|0|abbr=on}}, a service ceiling of {{convert|12000|ft|m|0|abbr=on}}, a stall speed of {{convert|38|mph|km/h|0|abbr=on}}, an endurance of three hours,<ref name="FOU">Fountain, Paul, ''The Maytag Messerschmitts'', Flying Magazine, March 1945, p. 90: With one pilot aboard, the L-4 had a maximum endurance of three hours' flight time (no reserve) at a reduced cruising speed of 65 mph.</ref> and a range of {{convert|225|mi|km|0|abbr=on}}.<ref>Gunston, Bill, and Bridgman, Leonard, ''Jane's Fighting Aircraft of World War II'', Studio Editions, ISBN 978-1-85170-199-5 (1989), p. 253</ref>  5,413 L-4s were produced for U.S. forces, including 250 built for the U.S. Navy under contract as the NE-1 and NE-2.<ref>Frédriksen, John C., ''Warbirds: An Illustrated guide to U.S. Military Aircraft, 1915-2000'', ABC-CLIO,  ISBN 978-1-57607-131-1 (1999), p. 270</ref><ref>Bishop, Chris, ''The Encyclopedia of Weapons of World War II'', Sterling Publishing Company, Inc., ISBN 978-1-58663-762-0 (2002), p. 431</ref>

All L-4 models, as well as similar, tandem-cockpit accommodation aircraft from [[Aeronca L-3|Aeronca]] and [[L-2 Grasshopper|Taylorcraft]], were collectively nicknamed "[[Grasshopper (disambiguation)|Grasshoppers]]", though the L-4 was almost universally referred to by its civilian designation of Cub.  The L-4 was used extensively in World War II for reconnaissance, transporting supplies, artillery spotting duties, and medical evacuation of wounded soldiers.<ref name=guill/>  During the [[Operation Overlord|Allied invasion of France]] in June 1944, the L-4's slow cruising speed and low-level maneuverability — alongside examples of the [[Taylorcraft Auster|Auster AOP]] aircraft occasionally used by the [[British Army]] and other Commonwealth forces for the same purposes — made it an ideal observation platform for spotting hidden German tanks, anti-tank gun emplacements, and ''[[Sturmgeschütz]]'' and ''[[Jagdpanzer]]'' [[tank destroyer]]s waiting in ambush in the hedgerowed [[bocage]] country south of the invasion beaches.  For these operations the pilot generally carried both an observer/radio operator and a 25-pound communications radio, a load that often exceeded the plane's specified weight capacity.<ref name="FOU"/>  After the Allied breakout in France, L-4s were also sometimes equipped with improvised racks, usually in pairs or quartets, of infantry [[bazooka]]s for ground attack against German armored units.  The most famous of these L-4 ground attack planes was ''Rosie the Rocketer'', piloted by Maj. [[Charles Carpenter (Lt. Col.)|Charles "Bazooka Charlie" Carpenter]], whose six bazooka rocket launchers were credited with eliminating six enemy tanks and several armored cars during its wartime service,<ref>''What's New in Aviation: Piper Cub Tank Buster'', Popular Science, Vol. 146 No. 2 (February 1945) p. 84</ref><ref>Kerns, Raymond C., Above the Thunder: Reminiscences of a Field Artillery Pilot in World War II, Kent State University Press, ISBN 978-0-87338-980-8 (2009), pp. 23–24, 293-294</ref> especially during the [[Battle of Arracourt]].

After the war, many L-4s were sold as surplus, but a considerable number were retained in service.<ref name="EDW">Edwards, Paul M., ''Korean War Almanac'', Infobase Publishing, ISBN 978-0-8160-6037-5 (2006), p. 502</ref>  L-4s sold as surplus in the U.S. were redesignated as J-3s, but often retained their wartime glazing and paint.<ref>''Nicholas Aircraft Sales, Flying Magazine, April 1946, Vol. 38, No. 4, ISSN 0015-4806, p. 106</ref>

==Postwar==
[[File:piperj3c.jpg|thumb|A taxiing Piper J-3 Cub, shown in "Cub Yellow" hue]]

An icon of the era, and of American [[general aviation]] in general, the J-3 Cub has long been loved by pilots and non-pilots alike, with thousands still in use today. Piper sold 19,073 J-3s between 1938 and 1947, the majority of them L-4s and other military variants. Postwar, thousands of Grasshoppers were civilian-registered under the designation J-3. Hundreds of Cubs were assembled from parts in Canada (by [[Cub Aircraft]] as the '''Cub Prospector'''), Denmark and Argentina, and by a licensee in Oklahoma.

In the late 1940s, the J-3 was replaced by the [[Piper PA-11]] ''Cub Special'' (1,500 produced), the first Piper Cub version to have a fully enclosed cowling for its powerplant, and then the [[Piper PA-18 Super Cub]], which Piper produced until 1981 when it sold the rights to WTA Inc. In all, Piper produced 2,650 Super Cubs. The Super Cub had a 150&nbsp;hp (110&nbsp;kW) engine which increased its top speed to 130&nbsp;mph (210&nbsp;km/h); its range was {{convert|460|mi|km|0}}.

==Korean War Service==
The L-4 was used extensively by both U.S. and South Korean Air Forces in the early 1950s.<ref name="EDW"/>  During the Korean War, the L-4 saw service in many of the same roles it had performed during World War II, such as artillery spotting, forward air control, and reconnaissance.<ref name="EDW"/> Some L-4s were fitted with a high-back canopy in order to carry a single stretcher for medical evacuation of wounded soldiers.<ref name="EDW"/>

==Modern production==
[[File:PiperCub-75th.jpg|thumb|right|Cubs gather for their 75th anniversary at [[AirVenture]] 2012]]
Modernized and up-engined versions are produced today by [[Cub Crafters]] of [[Washington (U.S. state)|Washington]] and by [[American Legend Aircraft]] in [[Texas]], as the Cub continues to be sought after by [[bush plane|bush]] pilots for its [[STOL]] ('''S'''hort '''T'''ake'''o'''ff and '''L'''anding) capabilities, as well as by recreational pilots for its nostalgia appeal. The new aircraft are actually modeled on the PA-11, though the Legend company does sell an open-cowl version with the cylinder heads exposed, like the J-3 Cub. An electrical system is standard from both manufacturers.{{Citation needed|date=January 2014}}

[[File:Piper J-3C-65 Cubimg 0505.jpg|thumb|left|A Piper J-3C-65 nose-on with "eyebrow" air scoops on its engine cylinders]]
The J-3 is distinguished from its successors by having a cowl that exposes its engine's cylinder heads — the exposed cylinders of any J-3's engine were usually fitted with sheet metal "eyebrow" air scoops to direct air over the cylinder's fins for more effective engine cooling in flight. There are very few other examples of "flat" aircraft engine installations (as opposed to radial engines) in which the cylinder heads are exposed. From the PA-11 on through the present Super Cub models, the cowling surrounds the cylinder heads.<ref name="Supercub">Clark, Anders. (21 November 2014) "[https://disciplesofflight.com/worlds-iconic-airplane-piper-j-3-cub/ Piper J-3 Cub: The World’s Most Iconic Airplane]". Disciples of Flight. Retrieved 21 August 2014.</ref>

A curiosity of the J-3 is that when it is flown solo, the lone pilot normally occupies the rear seat for proper balance, to balance the fuel tank located at the firewall. Starting with the PA-11, and some L-4s, fuel was carried in wing tanks, allowing the pilot to fly solo from the front seat.<ref name="Supercub"/>

[[File:o'brien's flyingcircus aerobaticsteam cotswoldairshow 2010 arp.jpg|thumb|A "clipped-wing" Piper J3C-65 of O’Briens Flying Circus Aerobatic Stunt Team lands on a moving trailer at [[Cotswold Airport]].]]

== Variants ==

===Civil variants===
;J-3
:Equipped with a [[Continental A-40]], A-40-2 or A-40-3 engine of {{convert|37|hp|kW|0|abbr=on}}, or A-40-4 engine of {{convert|40|hp|kW|0|abbr=on}}<ref name="ATC660">{{cite web|url = http://www.airweb.faa.gov/Regulatory_and_Guidance_Library/rgMakeModel.nsf/0/9ae985fe2a4d8847862572c9006eda41/$FILE/ATTQ7W2D/ATC660.pdf|title = Approved Type Certificate 660|accessdate = 2010-02-15|last = [[Federal Aviation Administration]]|authorlink = |date=October 1939}}</ref>
;J3C-40
:Certified 14 July 1938 and equipped with a [[Continental A-40|Continental A-40-4]] or A-40-5 of {{convert|40|hp|kW|0|abbr=on}}<ref name="A691">{{cite web|url = http://www.airweb.faa.gov/Regulatory_and_Guidance_Library/rgMakeModel.nsf/0/f323caa28762f4c486257209007258d4/$FILE/A-691.pdf|title = AIRCRAFT SPECIFICATION NO. A-691|accessdate = 2010-02-15|last = [[Federal Aviation Administration]]|authorlink = |date=August 2006}}</ref>
;J3C-50
:Certified 14 July 1938 and equipped with a [[Continental O-170|Continental A-50-1]] or A-50-2 to -9 (inclusive) of {{convert|50|hp|kW|0|abbr=on}}<ref name="A691" />
;J3C-50S
:Certified 14 July 1938 and equipped with a [[Continental O-170|Continental A-50-1]] or A-50-2 to -9 (inclusive) of {{convert|50|hp|kW|0|abbr=on}}. Equipped with optional float kit.<ref name="A691" />
;J3C-65
:Certified 6 July 1939 and equipped with a [[Continental O-170|Continental A-65-1]] or A-65-3, 6, 7, 8, 8F, 9 or 14 of {{convert|65|hp|kW|0|abbr=on}} or an A-65-14, [[Continental O-170|Continental A-75-8]], A-75-8-9 or A-75-12 of {{convert|75|hp|kW|0|abbr=on}} or [[Continental O-190|Continental A-85-8]] or C-85-12 of {{convert|85|hp|kW|0|abbr=on}} or [[Continental O-200|Continental A-90-8F]] of {{convert|90|hp|kW|0|abbr=on}}<ref name="A691" />
;J3C-65S
:Certified 27 May 1940 and equipped with a [[Continental O-170|Continental A-65-1]] or A-65-3, 6, 7, 8, 8F, 9 or 14 of {{convert|65|hp|kW|0|abbr=on}} or an A-65-14, [[Continental O-170|Continental A-75-8]], A-75-8-9 or A-75-12 of {{convert|75|hp|kW|0|abbr=on}} or [[Continental O-190|Continental A-85-8]] or C-85-12 of {{convert|85|hp|kW|0|abbr=on}} or [[Continental O-200|Continental A-90-8F]] of {{convert|90|hp|kW|0|abbr=on}}. Equipped with optional float kit.<ref name="A691" />
;J3F-50
:Certified 14 July 1938 and equipped with a [[Franklin 4AC|Franklin 4AC-150 Series 50]] of {{convert|50|hp|kW|0|abbr=on}}<ref name="A692">{{cite web|url = http://www.airweb.faa.gov/Regulatory_and_Guidance_Library/rgMakeModel.nsf/0/b517fb092ee72c198625720a0055b58a/$FILE/A-692.pdf|title = AIRCRAFT SPECIFICATION NO. A-692|accessdate = 2010-02-15|last = [[Federal Aviation Administration]]|authorlink = |date=August 2006}}</ref>
;J3F-50S
:Certified 14 July 1938 and equipped with a [[Franklin 4AC|Franklin 4AC-150 Series 50]] of {{convert|50|hp|kW|0|abbr=on}}. Equipped with optional float kit.<ref name="A692" />
;J3F-60
:Certified 13 April 1940 and equipped with a [[Franklin 4AC|Franklin 4AC-150 Series A]] of {{convert|65|hp|kW|0|abbr=on}} or a [[Franklin 4AC|Franklin 4AC-171]] of {{convert|60|hp|kW|0|abbr=on}}.<ref name="A692" />
;J3F-60S
:Certified 31 May 1940 and equipped with a [[Franklin 4AC|Franklin 4AC-150 Series A]] of {{convert|65|hp|kW|0|abbr=on}} or a [[Franklin 4AC|Franklin 4AC-171]] of {{convert|60|hp|kW|0|abbr=on}}. Equipped with optional float kit.<ref name="A692" />
;J3F-65
:Certified 7 August 1940 and equipped with a [[Franklin 4AC|Franklin 4AC-176-B2]] or a [[Franklin 4AC|Franklin 4AC-176-BA2]] of {{convert|65|hp|kW|0|abbr=on}}.<ref name="A692" />
;J3F-65S
:Certified 4 January 1943 and equipped with a [[Franklin 4AC|Franklin 4AC-176-B2]] or a [[Franklin 4AC|Franklin 4AC-176-BA2]] of {{convert|65|hp|kW|0|abbr=on}}. Equipped with optional float kit.<ref name="A692" />
;J3L
:Certified 17 September 1938 and equipped with a [[Lycoming O-145|Lycoming O-145-A1]] of {{convert|50|hp|kW|0|abbr=on}} or a [[Lycoming O-145|Lycoming O-145-A2]] or A3 of {{convert|55|hp|kW|0|abbr=on}}<ref name="A698">{{cite web|url = http://www.airweb.faa.gov/Regulatory_and_Guidance_Library/rgMakeModel.nsf/0/654cec2e0f108d0a8625720900728d14/$FILE/a-698.pdf|title = AIRCRAFT SPECIFICATION A-698|accessdate = 2010-02-15|last = [[Federal Aviation Administration]]|authorlink = |date=August 2006}}</ref>
;J3L-S
:Certified 2 May 1939 and equipped with a [[Lycoming O-145|Lycoming O-145-A1]] of {{convert|50|hp|kW|0|abbr=on}} or a [[Lycoming O-145|Lycoming O-145-A2]] or A3 of {{convert|55|hp|kW|0|abbr=on}}. Equipped with optional float kit.<ref name="A698" />
;J3L-65
:Certified 27 May 1940 and equipped with a [[Lycoming O-145|Lycoming O-145-B1]], B2 or B3 of {{convert|65|hp|kW|0|abbr=on}}<ref name="A698" />
;J3L-65S
:Certified 27 May 1940 and equipped with a [[Lycoming O-145|Lycoming O-145-B1]], B2 or B3 of {{convert|65|hp|kW|0|abbr=on}}. Equipped with optional float kit.<ref name="A698" />
[[File:PiperJ3P.jpg|thumb|right|Piper J-3P]]
;J3P
:Variant powered by a {{convert|50|hp|kW|0|abbr=on}} [[Lenape LM-3-50]] or [[Lenape AR-3-160]] three-cylinder radial  engine.<ref name="Peperell" /><ref name="ATC695">{{cite web|url = http://www.airweb.faa.gov/Regulatory_and_Guidance_Library/rgMakeModel.nsf/0/ae0aecbca42009eb862572c9006edb1b/$FILE/ATTYNRM6/TC695.pdf|title = Approved Type Certificate 695|accessdate = 2010-02-18|last = [[Federal Aviation Administration]]|authorlink = |date=October 1942}}</ref>
;J-3R
:Variant with slotted flaps powered by a {{convert|65|hp|kW|0|abbr=on}} [[Lenape LM-3-65]] engine.<ref name="Peperell" />
;J-3X
:1944 variant with cantilever wing powered by a {{convert|65|hp|kW|0|abbr=on}} [[Continental A-65|Continental A-65-8]] engine.<ref name="Peperell" />
;Cammandre 1
:A French conversion of J-3 Cub/L-4 aircraft.<ref name="Cammandre">{{cite web|url =http://www.abpic.co.uk/search.php?q=Camandre%201%20(Cub%20conversion)&u=type|title = Cammandre 1|accessdate = 2011-02-07}}</ref>
;Poullin J.5A:Five L-4 Cubs converted by Jean Poullin for specialist tasks.<ref name=Gaillard4464>{{cite book|last=Gaillard|first=Pierre|title=Les Avions Francais de 1944 a 1964|year=1990|publisher=Editions EPA|language=French|location=Paris|isbn=2 85120 350 9}}</ref>
;Poullin J.5B:A single L-4 Cub converted by Jean Poullin for specialist tasks.<ref name=Gaillard4464/>
;Wagner Twin Cub:A twin fuselage conversion of the J-3.{{Citation needed|date=October 2015}}

===Military designations and variants===
;YO-59
: Four US Army Air Corps test and evaluation J3C-65.<ref name="Andrade140" />
;O-59
: Production version for the USAAC; 140 built later re-designated L-4.<ref name="Andrade140" />
;O-59A
: Improved version, powered by a 65-hp (48-kW) [[Continental O-170]]-3 piston engine; 948 built, later re-designated L-4A.<ref name="Andrade140" />
;L-4
: Redesignated YO-59 and O-59.<ref name="Andrade129" />
;L-4A
: Redesignated O-59A.<ref name="Andrade129" />
;L-4B
: As per L-4A, but without radio equipment; 980 built.<ref name="Andrade129" />
;L-4C
: Eight impressed J3L-65s, first two originally designated UC-83A.<ref name="Andrade129" />
;L-4D
: Five impressed J3F-65s.<ref name="Andrade129" />
;L-4H
: As per L-4B but with improved equipment and fixed-pitch propeller, 1801 built.<ref name="Andrade129" />
;L-4J
: L-4H with controllable-pitch propeller, 1680 built.<ref name="Andrade129" />
;UC-83A
: Two impressed J3L-65s, later re-designated L-4C.<ref name="Andrade81" />
;TG-8
: Three-seat training glider variant, 250 built.<ref name="Andrade170" />
;LNP
: United States Navy designation for three TG-8s received.<ref name="Andrade170" />
[[File:PiperNE-1.jpg|thumb|right|Piper NE-1]]
;NE-1
: United States Navy designation for dual control version of J3C-65, 230 built.<ref name="Andrade201" />
;NE-2
: As per NE-1 with minor equipment changes, 20 built.<ref name="Andrade201" />

== Operators ==
[[File:Piper J3 Cub.svg|right|300px]]

=== Civil operators ===
The aircraft has been popular with flying schools — especially from the pre-World War II existence of the [[Civilian Pilot Training Program]] using them in the United States — and remains so with private individuals, into the 21st century.

=== Military operators ===
;{{BRA}}
*[[Brazilian Air Force]] {{citation needed|date=October 2013}}
;{{ROK}}
*[[Republic of Korea Air Force]]<ref name="Triggs1319">Triggs, James M.: ''The Piper Cub Story'', pages 13-19. The Sports Car Press, 1963. SBN 87112-006-2</ref>
;{{PAR}}
*[[Military of Paraguay]] - L-4<ref name="Krivinyi">Krivinyi, Nikolaus: ''World Military Aviation'', page 181. Arco Publishing Company, 1977. ISBN 0-668-04348-2</ref>
;{{UK}}
*[[Royal Air Force]]<ref name="Andrade239" />
;{{THA}}
*[[Royal Thai Air Force]]{{Citation needed|date=November 2012}}
;{{USA}}
*[[United States Air Force]]<ref name="Peperell" />
*[[United States Army]]<ref name="Triggs1319" />
*[[United States Army Air Forces]]<ref name="Peperell" />
*[[United States Navy]]<ref name="Peperell" /><ref name="Triggs1319" />

==Specifications (J3C-65 Cub)==
[[File:Piper Cub cockpit.jpg|thumb|right|300px|Inside the cockpit of a Piper Cub.  The aircraft has far fewer [[Flight instruments|instruments]] than most modern aircraft.]]

{{aircraft specifications|
|switch order of units?=no
|include 'capacity' field?=yes
|plane or copter?=plane
|jet or prop?=prop
|ref=The Piper Cub Story<ref name="Triggs">Triggs, James M.: ''The Piper Cub Story'', page 31. The Sports Car Press, 1963. SBN 87112-006-2</ref>
|crew=one pilot
|capacity=one passenger
|length main=22 ft 5 in
|length alt=6.83 m
|span main=35 ft 3 in
|span alt=10.74 m
|height main=6 ft 8 in
|height alt=2.03 m
|area main=178.5 ft²
|area alt=16.58 m²
|empty weight main=765 [[pound (mass)|lb]]
|empty weight alt=345 kg
|loaded weight main=
|loaded weight alt=
|useful load main=455 lb
|useful load alt=205 kg
|max takeoff weight main=1,220 lb
|max takeoff weight alt=550 kg
|engine (prop)=[[Continental A-65|Continental A-65-8]]
|type of prop=air-cooled [[horizontally opposed]] four cylinder
|number of props=1
|power main=65 [[horsepower|hp]]
|power alt=48 [[kilowatt|kW]]
|power more =at 2,350 [[Revolutions per minute|rpm]]
|max speed main=76 [[knot (unit)|kn]]
|max speed alt=87 mph, 140 km/h
|cruise speed main=65 kn
|cruise speed alt=75 mph, 121 km/h
|range main=191 NM
|range alt=220 [[statute mile|mi]], 354 km
|ceiling main=11,500 ft
|ceiling alt=3,500 m
|climb rate main=450 ft/min
|climb rate alt=2.3 m/s
|loading main=6.84 lb/ft²
|loading alt=33.4 kg/m²
|power/mass main=18.75 lb/hp
|power/mass alt=11.35 kg/kW
}}

==See also==
{{Portal|Military of the United States|United States Air Force|Aviation}}
{{aircontent|
|related=
*[[American Legend AL3C-100]]
*[[CubCrafters CC11-100 Sport Cub S2]]
*[[LIPNUR Belalang]]
*[[Marawing 1-L Malamut]]
*[[Taylor J-2|Piper J-2]]
*[[Piper PA-15 Vagabond]]
*[[Piper PA-16 Clipper]]
*[[Piper PA-18 Super Cub]]
*[[Piper PA-20 Pacer]]
*[[Wag-Aero CUBy]]
|similar aircraft=
*[[Aeronca Champion]]
*[[Aeronca L-3]]
*[[American Eagle Eaglet 31]]
*[[Fieseler Fi 156]] ''Storch''
*[[Denney Kitfox|Kitfox Model 5]]
*[[Taylorcraft B|Taylorcraft BC-65]]
*[[Taylorcraft L-2]]
|lists=
* [[List of aircraft of World War II]]
|see also=
}}

==References==
{{reflist|30em|refs=
<ref name="Andrade81">Andrade 1979, p. 81</ref>
<ref name="Andrade129">Andrade 1979, p. 129</ref>
<ref name="Andrade140">Andrade 1979, p. 140</ref>
<ref name="Andrade170">Andrade 1979, p. 170</ref>
<ref name="Andrade201">Andrade 1979, p. 201</ref>
<ref name="Andrade239">Andrade 1979, p. 239</ref>
<ref name="Peperell">Peperell 1987, pp. 22–34</ref>
}}
{{refbegin}}
* {{cite book |last= Andrade |first= John |title= U.S.Military Aircraft Designations and Serials since 1909|year=1979 |publisher=Midland Counties Publications|isbn= 0-904597-22-9}}
* {{cite book|author=Bowers, Peter M.|title=Piper Cubs|publisher=McGraw Hill|year=1993|isbn=0-8306-2170-9}}
*{{cite book |last=Peperell |first=Roger W |author2=Smith, Colin M  | title= Piper Aircraft and their Forerunners | year=1987 |publisher=[[Air-Britain]] | location=Tonbridge, Kent, England | isbn=0-85130-149-5}}
*{{cite book|last=Gaillard|first=Pierre|title=Les Avions Francais de 1944 a 1964|year=1990|publisher=Editions EPA|language=French|location=Paris|isbn=2 85120 350 9}}
{{refend}}

==External links==
<!--===========================({{NoMoreLinks}})===============================-->
<!--| DO NOT ADD MORE LINKS TO THIS ARTICLE. WIKIPEDIA IS NOT A COLLECTION OF |-->
<!--| LINKS. If you think that your link might be useful, do not add it here, |-->
<!--| but put it on this article's discussion page first or submit your link  |-->
<!--| to the appropriate category at the Open Directory Project (www.dmoz.org)|-->
<!--| and link back to that category using the {{dmoz}} template.             |-->
<!--|                                                                         |-->
<!--|           Links that have not been verified WILL BE DELETED.            |-->
<!--|  See [[Wikipedia:External links]] and [[Wikipedia:Spam]] for details    |-->
<!--===========================({{NoMoreLinks}})===============================-->
{{commons|Piper Cub}}
*{{dmoz|Recreation/Aviation/Aircraft/Fixed_Wing/Piper/|Fixed Wing Piper}}
*[http://www.fiddlersgreen.net/AC/aircraft/Piper-Cub/j3cub_info/j3_info.htm Fiddler's Green] history of the J-3
*[http://www.piper.com/history/ Piper Aircraft, Inc. - History] - Brief timeline of the history of Piper Aircraft, starting with the Piper Cub
*[http://www.sentimentaljourneyfly-in.com/ Sentimental Journey] - Annual [[Fly-In|fly-in]] of Piper Cubs held in Lock Haven, Pennsylvania
*[http://www.scribd.com/doc/55896180 T.O. No. 01-140DA-1 Pilot's Flight Instructions L-4A and L-4B Airplanes (1943)]
*[http://www.scribd.com/doc/55895618AN 01-140DA-2 Erection and Maintenance Instructions for Airplanes Army Models L-4A, L-4B, L-4H, and L-4J British Model Piper Cub (1945)]

<!-- Banners -->
{{Piper Cub}}
{{Piper}}
{{USN trainer aircraft}}
{{USAF liaison aircraft}}
{{USAAF observation aircraft}}
{{USAF transports}}
{{USAAF glider aircraft}}

<!-- Languages -->

<!-- Categories -->
[[Category:High-wing aircraft]]
[[Category:Single-engined tractor aircraft]]
[[Category:Piper aircraft|Cub, J-3]]
[[Category:United States civil utility aircraft 1930–1939]]
[[Category:United States military utility aircraft 1940–1949]]
[[Category:1938 introductions]]