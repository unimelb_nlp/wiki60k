{{Use British English|date=September 2013}}
{{Infobox musical artist  <!-- See Wikipedia:WikiProject Musicians -->
| name            = SHY & D.R.S 
| image           = 
| caption         = SHY & D.R.S performing at Rockness in 2010
| background      = solo_singer
| birth_name      = Mark Scott and Darren Scott
| birth_place     = [[Aberdeen]], Scotland
| years_active    = 1999–present
| instrument      = Vocals 
| genre           = [[British hip hop|Hip hop]], [[rap rock]], [[R&B]]
| label           = Awe Records (2008 - 09)<br/>Guardian Angels Records/[[Universal Records|Universal]] (2011–2013) Unparallel Music Group/Unsigned [[The Orchard (company)|The Orchard]]/[[Sony]] (2014-present)
| associated_acts = {{flat list|
*[[Sandi Thom]]
*[[Nazareth]]
*[[Nina Nesbitt]]
*[[Keenan Cahill]]
*[[D12]]
*[[Daniel de Bourg]]
}}
| current_members = Mark Scott (SHY)<br /> Darren Scott (D.R.S)
| past_members    = 
| website = www.facebook.com/SHYandDRS
}}
'''Mark James Scott''' and '''Darren Scott''' ; better known by their [[stage name]] '''SHY & D.R.S''' are Scottish rappers and songwriters who have achieved four Official Top 40 Chart hits independently making Scottish music history. 

==Biography==
===Beginnings===
Born from humble beginnings in Torry, Aberdeen the twin brothers Mark (SHY) and Darren (D.R.S) were discovered by [[Midge Ure]] and [[Virgin Records]] ex head of A&R Ronnie Gurr in 2009 and are widely recognized as the pioneers of Scottish Hip Hop. <ref>{{cite web|url=http://www.beattiegroup.com/prclients/pr-press-releases/2010/march/music-legend-midge-ure-named-director-new-venture-to-protect.aspx |title=Music Legend Midge Ure Named As Director Of New Venture To Protect Artists' Rights &#124; News |publisher=Beattiegroup.com |date=2010-03-15 |accessdate=2015-02-27}}</ref>  

They gained global recognition when internet viral star Keenan Cahill chose to lip-synch their track "Relapse" in 2011 with the video going viral.<ref>{{cite web|author=Danny Law |url=http://news.stv.tv/north/24202-aberdeen-rappers-latest-hit-covered-by-youtube-sensation/ |title=Aberdeen rappers’ latest hit covered by YouTube sensation &#124; Aberdeen & North |publisher=News |date=2011-09-13 |accessdate=2015-02-27}}</ref>

=== 2011–2012: Signing and ''The Love Is Gone'' ===
SHY & D.R.S were signed to Record label Guardian Angels Records, a record label distributed by [[Fontana Records|Fontana]] [[Universal Records|Universal]]<ref>{{cite web|url=http://music-fanclub.net/2012/06/guardian-angels-records-joins-umg-as-ingrooves-fontanas-newest-label/ |title=Guardian Angels Records Joins UMG as InGrooves Fontana’s Newest Label |publisher=Music-Fanclub.net |date= |accessdate=2015-02-27}}</ref><ref>{{cite web|url=http://www.musicweek.com/news/read/singer-sandi-thom-launches-own-uk-and-us-label/048818 |title=Singer Sandi Thom launches own UK and US label |publisher=Music Week |date=2012-05-08 |accessdate=2015-02-27}}</ref> and Sony ATV, a record label run by [[Sandi Thom]] and Nick Yeatman until 2013, when the label terminated. The debut single featuring [[Sandi Thom]] charted in the Official Chart at number 32.<ref name="officialcharts.com">{{cite web|url=http://www.officialcharts.com/archive-chart/_/22/2012-10-20/ |title=Official Charts Company |publisher=Officialcharts.com |date= |accessdate=2015-02-27}}</ref>  In an interview in October 2012, SHY & DRS revealed it was actually filmed in 2009 and the accompanying music video was based on the movie, ''[[Sin City (film)|Sin City]]''.<ref>{{cite web|url=http://www.streetnortheast.com/music/interview-with-scottish-hip-hop-duo-shy-drs/ |title=Street North East &#124; Interview with Scottish hip hop duo Shy & Drs |publisher=Streetnortheast.com |date=2012-08-02 |accessdate=2015-02-27}}</ref>
The music video was premiered [[VEVO]] on September 12, 2012.<ref>{{cite web|author= |url=https://www.youtube.com/watch?v=Xm8vUcUEY-Y&list=UUs2MnNEpCaYXH19ua8bIoRQ |title=SHY & DRS - THE LOVE IS GONE ft. Sandi Thom |publisher=YouTube |date=2012-09-13 |accessdate=2015-02-27}}</ref>
It premiered on Music Television on 21 September 2012<ref>[http://shyanddrs.com/the-love-is-gone-video-tv-debut/ ]{{dead link|date=February 2015}}</ref> The single made it onto Scottish Radio DJ [[Jim Gellatly]]'s "Scotland's Greatest Album" in December 2012<ref>{{cite web|url=http://amazingradio.com/home/scotlands-greatest-album-2012-2 |title=Scotland’s Greatest Album: 2012 « Amazing Radio |publisher=Amazingradio.com |date=2013-01-02 |accessdate=2015-02-27}}</ref>

=== 2012-2013: ''Relapse'', ''I've Got (Enough Love)'' and ''Before Too Long'' ===
In July 2013, SHY & D.R.S released their debut studio album, ''Before Too Long'',<ref>{{cite web|url=http://www.stereoboard.com/content/view/172659/9 |title=SHY & FISHY - Before Too Long (Album Review) |publisher=Stereoboard |date=2012-05-17 |accessdate=2015-02-27}}</ref> which features guest vocals from multi-platinum Singer songwriter [[Sandi Thom]], Rock legends [[Nazareth]],<ref>{{cite web|url=http://www.stereoboard.com/content/view/179425/9 |title=SHY & DRS Feat. Nazareth - I've Got (Enough Love) (Single Review) |publisher=Stereoboard |date=2013-06-10 |accessdate=2015-02-27}}</ref> [[Eminem]]'s band [[D12]] and [[Daniel de Bourg]]. The album entered into the Top 20 [[iTunes]] [[Hip hop]] Chart and remained there for six consecutive weeks.

In 2013, they released single "I've Got (Enough Love)" accompanied with a music video to [[VEVO]] which featured rock legends [[Nazareth (band)|Nazareth]]. The single was covered by ''[[Classic Rock (magazine)|Classic Rock]]'' magazine.<ref name="classicrockmagazine.com">{{cite web|url=http://www.classicrockmagazine.com/news/nazareth-deny-split-rumour/ |accessdate=February 4, 2016 |deadurl=yes |archiveurl=https://web.archive.org/web/20140407055904/http://www.classicrockmagazine.com/news/nazareth-deny-split-rumour/ |archivedate=April 7, 2014 }}</ref> They were also featured on [[Keenan Cahill]]'s first single "Closer" in November 2014 which was shared by the likes of Ryan Seacrest, Family Guy star Seth Green & Austin Mahone and has amassed over half a million views combined worldwide.

SHY & DRS' second single featuring ''[[BBC Introducing]]'' artist [[Luke Bingham]] was a re-release of their viral hit "Relapse" in April 2013, with the video performed and sung by internet sensation [[Keenan Cahill]] and the release of the SHY & DRS brand Energy Drink by [[Nae Danger]]. The song entered the [[Scottish Singles Chart]] at number 40.<ref>{{cite web|author= |url=http://www.dunfermlinepress.com/news/roundup/articles/2013/06/05/459969-video-nazareth-push-for-chart-success/ |title=VIDEO: Nazareth push for chart success &#124; News |publisher=Dunfermline Press |date=2013-06-05 |accessdate=2015-02-27}}</ref>
''[[Classic Rock (magazine)|Classic Rock]]'' magazine covered the collaboration<ref name="classicrockmagazine.com"/>

In August 2013, they released an animated music video onto [[VEVO]] which features rapper [[Bizarre (rapper)|Bizarre]] from [[Eminem]]'s multi-platinum selling band [[D-12]]<ref>{{cite web|url=http://clpnation.com/animated-visuals-shy-drs-bizarre-d-12-out-my-face/ |title=Animated Visuals: Shy & DRS &#124; Bizarre (D-12) – Out My Face &#124; CLP Nation &#124; We Are The Nation |publisher=CLP Nation |date= |accessdate=2015-02-27}}</ref>
In July 2013 they released a charity track recorded with [[Northsound 1]] [[Bauer Radio|Bauer]] radio presenters Greigsy & Aylissa which went straight into the Top 10 UK iTunes hip hop chart<ref>{{cite web|author= |url=http://www.dailyrecord.co.uk/entertainment/music/music-news/greigsy-aylissa-rap-twins-shy-1863478 |title=Greigsy and Aylissa on rap twins Shy and Drs and Aberdeen's Big Beach Ball - Greigsy and Aylissa |publisher=Daily Record |date=2013-05-02 |accessdate=2015-02-27}}</ref><ref>{{cite web|author= |url=http://www.dailyrecord.co.uk/entertainment/music/music-news/northsound-1s-greigsy-aylissa-write-1836875 |title=Northsound 1's Greigsy and Aylissa write for The Aberdonian - Greigsy and Aylissa |publisher=Daily Record |date=2013-04-17 |accessdate=2015-02-27}}</ref>

===2014 - present: ''Born Again''===
SHY & D.R.S featured on internet star [[Keenan Cahill]]'s single "Closer" in 2014 which was premiered on [[Ryan Seacrest]]'s Radio show and website.<ref name="ryanseacrest.com">{{cite web|url=http://www.ryanseacrest.com/2013/12/05/keenan-cahill-closer-video/ |title=Keenan Cahill Creates Light Show in Electrifying ‘Closer’ Video |publisher=Ryan Seacrest |date=2013-12-05 |accessdate=2015-02-27}}</ref> The song was performed live on German TV and American [[WGN-TV]] Channel<ref name="video.bostonherald.com">{{cite web|author=STUDIO |url=http://video.bostonherald.com/YouTube-star-Keenan-Cahill-talks-dropping-lipsyncing-for-new-career-move-25430622?playlistId=10245#.UxnriOdJaGo |title=YouTube star Keenan Cahill talks dropping lip-syncing for new career move |publisher=Video.bostonherald.com |date= |accessdate=2015-02-27}}</ref><ref name="brittanyhagerty.buzznet.com">{{cite web|author= |url=http://brittanyhagerty.buzznet.com/user/journal/17350835/exclusive-interview-youtube-celebrity-keenan/ |title=Exclusive Interview With Youtube Celebrity Keenan Cahill on Brittany Lee's Blog - Buzznet |publisher=Brittanyhagerty.buzznet.com |date=2013-09-05 |accessdate=2015-02-27}}</ref>

In February 2014, it was announced that they are leaving for the US with a new label deal with [[Sony]] and under new management<ref name="aberdeen.stv.tv"/><ref>{{cite web|url=http://www.eveningexpress.co.uk/news/local/video-aberdeen-rappers-shy-drs-eye-move-to-us-after-single-success-1.205533 |title=Video: Aberdeen rappers Shy & DRS eye move to US after single success - Local / News / Evening Express |publisher=Eveningexpress.co.uk |date= |accessdate=2015-02-27}}</ref>

SHY & D.R.S formed their own independent label Unparallel Music Group<ref>{{cite web|url=http://www.thescottishsun.co.uk/scotsol/homepage/scotlandfeatures/5793315/Birth-of-hip-hop.html?teaser=true |title=The Sun |publisher=Thescottishsun.co.uk |date= |accessdate=2015-02-27}}</ref>  and released "[[Born Again (SHY & DRS song)|Born Again]]" featuring British trance singer [[Christina Novelli]] in August 2014. "Born Again" charted independently at number 26 in the Official Scottish Chart and at number 14 on the ''[[Music Week]]'' Indie Chart in its first week.<ref>{{cite web|url=http://www.officialcharts.com/archive-chart/_/22/2014-08-16/ |title=Official Charts Company |publisher=Officialcharts.com |date= |accessdate=2015-02-27}}</ref> The video was removed from [[YouTube]] for its controversial subject and scenes but was kept on [[VEVO]]. The video was later reissued on the YouTube Channel.<ref>{{cite web|last=Pease |first=Victoria |url=http://aberdeen.stv.tv/articles/285874-rap-duo-shy-and-drs-hit-scottish-top-40-with-single-born-again/ |title=Rap duo SHY and DRS hit Scottish top 40 with single Born Again &#124; STV |publisher=Aberdeen |date=2014-08-11 |accessdate=2015-02-27}}</ref> The video received major TV airplay from ''[[4Music]]'', ''[[Pinoy Extreme|VIVA]]'' and [[Capital TV]].<ref>{{cite web|url=http://www.thescottishsun.co.uk/scotsol/homepage/5819942/Hip-hop-hooray-Rappers-make-official-top-40.html |title=Hip hop hooray: Rappers make official top 40 |publisher=''The Scottish Sun'' |date= |accessdate=2015-02-27}}</ref> 

They performed "Born Again" and their first official Top 40 Single "The Love is Gone" live on [[BBC Radio Scotland]]'s Culture Show and interviewed by Janice Forsyth who dubbed them "Scottish HipHop's Most Exciting Export" revealing they were touring with [[Eminem]]'s Band D12 in 2015.<ref>{{cite web|author= |url=http://www.bbc.co.uk/programmes/b04g14lx |title=BBC Radio Scotland - The Culture Studio with Janice Forsyth, 04/09/2014 |publisher=Bbc.co.uk |date=2014-09-04 |accessdate=2015-02-27}}</ref>

On 13 November 2015 they released the single "Beautiful to Me' featuring X-Factor's Janet Devlin. The song was the official single for the UK's National Anti-Bullying Week and charted at Number 25 on the official Scottish chart<ref>{{Cite web|title = Official Scottish Singles Chart Top 100 {{!}} Official Charts Company|url = http://www.officialcharts.com/charts/scottish-singles-chart/20151120/41/|website = www.officialcharts.com|accessdate = 2015-12-07}}</ref> The song raised money for Childline UK and UK anti-bullying charity Ditch the Label. 

Their 2015 video "What's Wrong With Us?" went viral achieving over 260,000 views in less than two weeks.

==Business==
The pair are sponsored by [[Nae Danger]] energy drink based in [[Glasgow]] who are part owned by Scottish business mogul Duncan Bannatyne. In July 2013, Nae Danger released a "SHY & DRS" brand energy drink that features their image on the cans; giving a free download of their track "Relapse"<ref>{{cite web|url=https://www.facebook.com/photo.php?fbid=516484938390495&set=pb.405979152774408.-2207520000.1394206177.&type=3&theater |title=Nae Danger Energy Drink's Photos - Nae Danger Energy Drink |publisher=Facebook |date= |accessdate=2015-02-27}}</ref><ref>{{cite web|url=http://www.nae-danger.co.uk/index.php/the-drinks/item/166-original-energy |title=Original Energy |publisher=Nae-danger.co.uk |date=2013-10-21 |accessdate=2015-02-27}}</ref><ref>{{cite web|url=http://shyanddrs.com/naedanger/ |accessdate=February 4, 2016 |deadurl=yes |archiveurl=https://web.archive.org/web/20140107064537/http://shyanddrs.com/naedanger/ |archivedate=January 7, 2014 }}</ref>

They revealed in a 2014 Interview that they are now sponsored by German clothing label Goldside Blackforest and in early 2015 revealed they are also sponsored by Scottish clothing label Marble Boy.<ref>{{cite web|url=http://www.guttermagazine.com/2012/07/14/introducing-shy-the-face-of-scottish-hip-pop/ |title=Introducing SHY & DRS; The Face Of Scottish Hip Pop &#124; Fashion and Art Underground |publisher=Gutter Magazine |date= |accessdate=2015-02-27}}</ref><ref>{{cite web|url=http://www.gside.de/gsbf-friends/ |title=GSBF GOLDSIDE BLACKFOREST 1982 - GSBF - FRIENDS |publisher=Gside.de |date= |accessdate=2015-02-27}}</ref>

In early 2016 SHY & D.R.S announced a new sponsorship deal with UK clothing brand Fathersons Clothing and modelled  for the label.

== Personal life ==
Also known as Mark and Darren Scott, they grew up in Torry [[Aberdeen]] and attended [[Aberdeen University]]. Darren graduated in 2008 with a First Class [[Honours Degree]] in [[Sociology]] and Mark graduated with a [[Masters Degree]] in [[Psychology]]. They began rapping in American accents but later changed to using a classic Scottish accent.<ref name="aberdeen.stv.tv">{{cite web|last=Smith |first=Laura |url=http://aberdeen.stv.tv/articles/263977-aberdeen-hip-hop-stars-shy-and-drs-hope-to-crack-america-after-youtube-hit/ |title=hip hop stars SHY and DRS hope to crack America after YouTube hit &#124; STV |publisher=Aberdeen |date=2014-02-16 |accessdate=2015-02-27}}</ref>

==Discography==

===Albums===
* 2006: ''The Unexpected''
* 2008: ''Behind Closed Doors'' 
* 2013: ''Before Too Long''

===EPs===
* 2013: ''[[Read All About It (EP)|Read All About It]] EP''

===Singles===
{| class="wikitable plainrowheaders" style="text-align:center;"
|+ List of singles, with selected chart positions, showing year released and album name
|-
! rowspan=2| Year
! rowspan=2| Title
! colspan=3| Peak chart positions
! rowspan=2| Album
|-
! style="width:3em;font-size:85%;"| [[UK Singles Chart|UK]]
! style="width:3em;font-size:85%;"| [[Irish Singles Chart|IRE]]
! style="width:3em;font-size:85%;"| [[Scottish Singles and Albums Charts|SCO]]<br/><ref name="SCOSingles">Peak positions for the singles in Scotland:
* For "The Love Is Gone ft. [[Sandi Thom]]": {{cite web|url=http://www.officialcharts.com/archive-chart/_/22/2012-10-20/|title=Chart Archive > April 20, 2013|publisher=[[Official Charts Company]]|date=2013-04-20}}
</ref>
|-
| 2012
! scope="row"| "The Love Is Gone" <br><small>(featuring [[Sandi Thom]])</small>
| 139 || — || 32
| ''Before Too Long''
|-
| rowspan=2| 2013
! scope="row"| "Relapse" <br><small>(featuring [[Luke Bingham]])</small>
| 188 || — || 40
| ''Before Too Long'' and ''Behind Closed Doors''
|-
! scope="row"| "I've Got (Enough Love)" <br><small>(featuring [[Nazareth (band)|Nazareth]])</small>
| — || — || 57
| ''Before Too Long''
|-
| 2014
! scope="row"| "[[Born Again (SHY & DRS song)|Born Again]]" <br><small>(featuring [[Christina Novelli]])</small> 
| 109 || — || 26
| rowspan="2" {{N/A|Non-album singles}}
|-
| 2015
! scope="row"| "Beautiful to Me" <br><small>(featuring [[Janet Devlin]])</small> 
| 85 || 25 || 25
|-
| colspan=18 style="font-size:85%"| "—" denotes a single that did not chart or was not released.
|}

;Featured in
*2013: "Closer" with [[Keenan Cahill]] featuring SHY & DRS (also ''Closer (The Remixes)'' EP released in 2014)

==References==
{{Reflist|colwidth=30em}}

==External links==
{{commons category|SHY & DRS}}
* [http://vevo.com/artist/shy-drs Vevo]
*[https://www.facebook.com/SHYandDRS Facebook]
*[https://www.youtube.com/channel/UCqNb_LGS00qPPpavp6gQuWg YouTube channel]

{{Authority control}}

{{DEFAULTSORT:SHY and DRS}}
[[Category:1984 births]]
[[Category:Scottish male singers]]
[[Category:Scottish pop singers]]
[[Category:Scottish male rappers]]
[[Category:Living people]]
[[Category:People from Hackney Central]]
[[Category:NME Awards winners]]