{{Infobox Australian road
| type                     = road
| urban                    = yes
| uc_former                = 
| road_name                = McIntyre Road
| road_name2               = Kings Road
| city = Adelaide
| state                    = sa
| image                    = 
| imagesize                = 
| caption                  = 
| image_alt                = 
| alternative_location_map = Australia Greater Adelaide
| loc_caption              = 
| location_alt             = 
| coordinates              = 
| length                   = 
| length_rnd               = 
| length_ref               = 
| est                      = 
| closed                   = 
| gazetted                 = 
| gazetted_ref             = 
| maintained               = 
| history                  = 
| route                    = {{AUshield|SA|A17}} A17
| former                   = 
| tourist                  = 
| direction_a              = northwest
| end_a                    = [[Port Wakefield Road]], [[Paralowie, South Australia|Paralowie]]
| latd_a = 34
| latm_a=45
| lats_a=48
| longd_a=138
| longm_a=35
| longs_a=35
| exits                    = [[Main North Road]]
| direction_b              = southeasteast
| end_b                    = [[North East Road]], [[Modbury, South Australia|Modbury]]
| latd_b                   = 34
| latm_b                   = 50
| lats_b                   = 8
| longd_b                  = 138
| longm_b                  = 40
| longs_b                  = 50
| region                   = 
| lga                      = {{plainlist |
* [[City of Salisbury]]
* [[City of Tea Tree Gully]]
}}
| through                  = {{plainlist |
* [[Salisbury Downs, South Australia|Salisbury Downs]]
* [[Parafield Gardens, South Australia|Parafield Gardens]]
* [[Salisbury South, South Australia|Salisbury South]]
* [[Parafield, South Australia|Parafield]]
* [[Salisbury East, South Australia|Salisbury East]]
* [[Para Hills West, South Australia|Para Hills West]]
* [[Gulfview Heights, South Australia|Gulfview Heights]]
* [[Para Hills, South Australia|Para Hills]]
* [[Wynn Vale, South Australia|Wynn Vale]]
* [[Modbury Heights, South Australia|Modbury Heights]]
* [[Modbury North, South Australia|Modbury North]]
}}
| restrictions             = 
| show_links               = 
}}
'''McIntyre Road''' and '''Kings Road''' are arterial roads crossing the northern and northeastern suburbs of [[Adelaide]] in [[South Australia]]. They are together denoted as route A18. 

==Route description==
Kings Road crosses the northern suburbs connecting [[Port Wakefield Road]] and [[Main North Road]] immediately north of [[Parafield Airport]] across the southern edge of [[Salisbury, South Australia|Salisbury]]. The western part of Kings Road passes through residential areas and crosses the [[Little Para River]]. East of the railway lines, the character changes to industrial manufacturing, and the [[Parafield Airport]].

McIntyre Road continues east of Main North Road with a residential character again. It rises up the face of the [[Adelaide Hills]] through a corridor of native bushland and sweeps past the [[Golden Grove, South Australia|Golden Grove]] development of the 1980s to [[North East Road]] at [[Modbury, South Australia|Modbury]]. Much of the McIntyre Road corridor was originally reserved as part of the 1960s [[Metropolitan Adelaide Transport Study]] (MATS Plan).

==History==
McIntyre Road was opened in 1989, funded primarily by the [[Australian Government]].<ref>{{Citation | author1=Brown, Bob | title=New $14 million road opened in North East Adelaide | publication-date=1989-06-22 | url=http://trove.nla.gov.au/work/195509024 | accessdate=28 June 2016 }}</ref>

In 2016, the western end of Kings Road was realigned so that instead of it ending at a tee junction with Bolivar Road just short of Port Wakefield Road, the two roads meet at a large roundabout enabling easier traffic flow to and from Kings Road. The Port Wakefield Road terminus is also going to become a crossroad with a new extension to an interchange on the [[Northern Connector]] to be completed by 2019.<ref>{{cite web |url=http://www.infrastructure.sa.gov.au/nsc/northern_connector/kingsbolivar_intersection_realignment |title=Kings/Bolivar Intersection Realignment |publisher=[[Department of Planning, Transport and Infrastructure]], [[Government of South Australia]] |accessdate=29 June 2016}}</ref> Prior to this alteration, route A18 included 300m of Bolivar Road to connect the end of Kings Road to Port Wakefield Road.

==Major intersections==
{{AUSinttop |location_ref=<ref>{{cite web |url=http://maps.sa.gov.au/plb/ |title=Property Location Browser |accessdate=30 June 2016 |publisher=Government of South Australia}}</ref> |length_ref=<ref>{{google maps |url=https://www.google.com.au/maps/dir/-34.763024,138.5930763/-34.8354902,138.6806252/@-34.7974781,138.5669823,12z/am=t/data=!3m1!4b1!4m2!4m1!3e0?hl=en |accessdate=30 June 2016}}</ref>}}
{{SAint
|LGAC=Salisbury
|LGAspan=7
|location=Paralowie
|lspan=2
|type=
|km=0
|road={{AUshield|N|A1}} [[Port Wakefield Road]] {{small|national route A1}}  – {{SAcity|Virginia|Port Wakefield}}, [[Adelaide city centre]]
|notes=Western end of route A17 is Bolivar Road, proposed to be extended 200m to the [[Northern Connector]] by 2019
}}
{{SAint
|type=trans
|km=0.4
|road=Bolivar Road
|notes=Intersection realignment in 2016 created a roundabout south of the original junction. A17 is Kings Road from here.
}}
{{SAint
|location_special={{SAcity|Paralowie|Salisbury Downs|Parafield Gardens}}
|km=1.7
|bridge=[[Little Para River]]
|notes=
}}
{{SAint
|location_special={{SAcity|Salisbury Downs|Parafield Gardens}}
|type=
|km=3.9
|road={{AUshield|SA|A13}} [[Salisbury Highway]] {{small|A13}} – {{SAcity|Salisbury|Port Adelaide}}
|notes=
}}{{SAint
|location_special={{SAcity |Salisbury Downs|Salisbury South|Parafield Gardens|Parafield}}
|type=
|km=4.4
|place=[[Adelaide-Port Augusta railway line]] and [[Gawler Central railway line]]
|notes=level crossing
}}
{{SAint
|location_special={{SAcity|Salisbury South|Salisbury East|Parafield|Para Hills West}}
|lspan=
|type=trans
|km=6.2
|road={{AUshield|SA|A52}} [[Main North Road]] – [[Gawler, South Australia|Gawler]], [[Adelaide city centre]]
|notes=Kings Road to the northwest, McIntyre Road to the southeast
}}
{{SAint
|location_special={{SAcity|Salisbury East|Gulfview Heights|Para Hills|Para Hills West}}
|lspan=
|type=
|km=7.4
|road=Bridge Road
|notes=
}}
{{SAint
|LGAC=Tea Tree Gully
|LGAspan=3
|location_special={{SAcity|Wynn Vale|Modbury Heights}}
|lspan=
|type=
|km=9.7
|road=The Golden Way – [[Golden Grove, South Australia|Golden Grove]]
|notes=
}}
{{SAint
|location_special={{SAcity|Modbury North|Modbury}}
|lspan=
|type=
|km=11.6
|road=[[Montague Road, Adelaide|Montague Road]]
|notes=
}}
{{SAint
|location=Modbury
|lspan=
|type=
|km=12.4
|road={{AUshield|SA|A10}} [[North East Road]] {{small|A10}} – [[Adelaide city centre]]
|notes=McIntyre Road
}}
{{Jctbtm |conv=no |keys=trans}}

==References==
{{reflist}}

{{Road infrastructure in Adelaide}}

[[Category:Roads in Adelaide]]


{{Australia-road-stub}}