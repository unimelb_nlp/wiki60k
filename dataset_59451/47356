{{Infobox artist
| name             = Greta Dale
| image            = <!-- just the pagename, without the File:/Image: prefix or [[brackets]] -->
| image_size       =
| alt              =
| caption          =
| birth_name       = Margreta Lundberg
| birth_date       = 1929
| birth_place      = [[Kelowna]], [[British Columbia]], Canada
| death_date       = 1978
| death_place      = [[Toronto]], Ontario, Canada
| nationality      = Canadian
| education        = Studied with [[José Chávez Morado]]
| alma_mater       = [[Ontario College of Art and Design|Ontario College of Art]]
| known_for        = Murals
| notable_works    = Mural, [[Manitoba Centennial Centre|Centennial Concert Hall]], [[Winnipeg]], [[Manitoba]]
| style            =
| movement         =
| spouse           =
| awards           = <!-- {{awd|award|year|title|role|name}} (optional) -->
| elected          =
| patrons          =
}}

'''Greta Dale''' (1929–1978) was a [[Canadians|Canadian]] mural sculptor who executed numerous public and private commissions in Canada and the United States, including the mural in the lobby of the [[Manitoba Centennial Centre|Centennial Concert Hall]] in Winnipeg, Manitoba.

==Biography==
Greta Dale, born Margreta Lundberg in [[Kelowna]] B.C., studied at the [[Ontario College of Art and Design|Ontario College of Art]], c1949-1953 alongside photographer and architect, Jack Dale and painter Jack Akroyd.<ref name=Busby>{{cite book|last1=Busby|first1=Peter|title=The Life and Art of Jack Akroyd|date=2015|publisher=Mother Tongue Publishing|location=Vancouver|isbn=978-1896949437|page=29}}</ref>  In 1953 Greta moved with Jack Dale to [[Vancouver]] where they soon married, and by 1956 the couple had two children. Like many Canadian artists Greta Dale pursued postgraduate studies outside Canada. Around 1959, accompanied by her young family, she used grant money to study for a year in Mexico with the renowned muralist [[José Chávez Morado]].  There she was also introduced to the forms and textures of [[Maya architecture|Mayan architecture]], which subsequently influenced the sculptural style of her ceramic reliefs.<ref name=Busby /><ref name=petite>{{cite news|title=Massive Mural Being Unveiled, Petite Artist Likes Lots of Space|work=The Globe and Mail|date=6 March 1965}}</ref>

==Artistic career==
Upon returning to Vancouver, Dale completed two public mural commissions: one a figurative sgraffitto at 2033 Comox, Vancouver that has recently been restored (2014), while another, representing B.C. industries in encaustic for [[Johnston Heights Secondary School]] in Surrey B.C., points to her encounters with Mexican muralism.<ref name=Imredy>{{cite book|last1=Imredy|first1=Peggy|title=A Guide to Sculpture in Vancouver|date=1980|publisher=D.M. Imredy|location=Vancouver|pages=22–23}}</ref>  Dale and her new partner, the architect W.R. (Wilfrid) Ussner, then left B.C. together, and aside from a short time in Montreal, 1962–63, with intermittent travels to Europe and Mexico, were located in Toronto throughout the 1960s. Dale and Ussner collaborated professionally during this time, with Ussner often affording Dale opportunities for relief murals through his architectural projects, and Dale providing the professional expertise to his clients who wished to integrate art and architecture.<ref name=Williams>{{cite news|last1=Williams|first1=Toni|title=Mini-muralist totes clay for five tons of maxi-mural|work=Toronto Telegram|date=22 September 1967}}</ref>  Their close working relationship is evident in a joint brief Montreal business venture, “Techniques des Arts,” mounted in November 1962, that designated Dale as director and Ussner as architectural advisor. Its opening coincided with Dale’s exhibition of paintings and ceramics at the nearby small Art-tech Gallery, where her continued interest in the abstract sculptural surfaces of Mayan architectural forms is evident in the works represented in the gallery invitation.<ref>“Greta Dale” artist file, Montreal Museum of Fine Arts Library</ref><ref>{{cite journal|title=Products|journal=Canadian Architect|date=August 1965|volume=10|issue=8|page=76}}</ref>  By the mid-1960s Dale had completed fourteen murals in central Canada and Spain, including works in clay, stained glass, sand casting, concrete and encaustic.<ref name=Aarons2 >{{cite book|last1=Aarons|first1=Anita|title=Allied Arts Catalogue/Catalogue des Arts Connexes|date=1966|publisher=University of Toronto and RAIC|location=Toronto|page=11}}</ref>

===Ceramic mural commissions===
[[File:Untitled Greta Dale 1967 Manitoba Centennial Concert Hall.JPG|thumb|Greta Dale: Untitled Mural, Manitoba Centennial Concert Hall, 1967]]
Dale’s first traceable ceramic commission was for the Briarwood Presbyterian Church, Beaconsfield, QC., c.1963. It is probable that this was facilitated through contacts she made during her Montreal sojourn. However, it is known she made the abstract clay reliefs for the baptismal font, the lectern and front doors from a basement studio in her Toronto home, using rough [[Credit River]] clay from [[Mississauga]].<ref>{{cite journal|last1=Bell|first1=Virginia|title=About the Front Doors|journal=Briarwood Presbyterian Church Newsletter|date=November 2013|pages=4–5}}</ref>  Dale also completed at least two other commissions in Montreal churches, including ceramic panels for louvered windows in Saint Paul’s Chapel, a sculpted altar, (architect W.R. Ussner), and a twelve-foot [[Stations of the Cross]].<ref name=Williams /><ref name=Aarons2 /><ref>Ryerson University Special Collections, Catholic Information Centre. 2009.002. QC Quebec series ref. code 2009.002.068</ref>

Dale’s first major secular ceramic commission was executed in 1964-65 for Sarco Canada’s new facility in Toronto, a building designed by Ussner. With its intricate surfaces of cut bricks within which were integrated abstracted figures, representing “sympathy for man,” it was clearly influenced by recently excavated Mayan architecture. Made of unglazed and glazed brick and sculptured stoneware, in colours ranging from Venetian red through terra cotta, orange, grey, purple, blue and turquoise, the five hundred square foot mural covered a wall in the entrance to Sarco’s Toronto offices.<ref name=petite /><ref name=Aarons2 /><ref name=Aarons1>{{cite journal|last1=Aarons|first1=Anita|title=Miscellaneous Happenings: Art and Architecture|journal=RAIC Journal|date=August 1965|page=14}}</ref>  A year later, champion and critic of the Allied Arts, Anita Aarons, prominently featured this project in an exhibition catalogue page dedicated to Dale’s ceramic murals published in conjunction with the [[University of Toronto]] and the [[Royal Architecture Institute of Canada]]. Aarons also subsequently included her in a 1967 exhibition of arts and architecture and participated with her in a radio interview on the importance of the allied arts.<ref name=Aarons2 /><ref name=Aarons1 /><ref>{{cite news|last1=Morrison|first1=Suzanne|title=These handicrafts are designed to ‘clothe’ architecture|work=The Toronto Star|date=10 March 1967}}</ref>

Dale’s last major ceramic work, and the largest, weighing five tons and measuring twenty-five hundred square feet (25' x 10'), was the untitled mural, or screen as it was called at the time, created for the Centennial Concert Hall in Winnipeg in 1967. This was one of four artworks commissioned for the building’s interior, and Dale was the only artist chosen who did not have close ties to Manitoba. Dale’s previous experience in commissions allowed her to traverse the multiple stages of submissions and negotiations, from the initial 1966 call for submissions from artists specifically chosen by the arts committee until the final work was installed in January 1968.<ref name="ReferenceA">Manitoba Archives, MCC0006 Chairman’s Files ACCG139 Box 16</ref>   The mural was divided into four main sections: three circular ones with figures representing the performing arts of dance, music and drama, and one horizontal that included all those who worked backstage as well as the audience.   In designing and fabricating this mural in sections for easy transportation and handling, Dale turned once again to bricks as her base unit form and material, still inspired by Mayan textures and sectional building techniques. She cut the clay bricks at a variety of heights and cut and reformed wet clay to construct the architectural shapes around her sculptured figures, themselves formed by the expressionist gestures of cutting, gouging and pushing.

===Later career===
In 1969 Dale received a [[Canada Council]] grant to pursue her studies in Mexico where she began to experiment with [[fibreglass]], a medium she consequently favoured, finding it more flexible than clay. These experiments culminated first in a mural commission for the Winnipeg Planetarium as part of the Manitoba’s 1970 provincial centennial celebrations, financed by the Bronfman Family through CEMP Investments Ltd.<ref name="ReferenceA"/>  This black triptych representing the universe has since been removed and placed in storage at the planetarium. Dale's next commission, a more colourful organic abstract fibreglass mural, measured 6 x 27 feet. Designed for the lobby of the Royal St. Andrew apartment building in [[Sarasota, Florida]], its colours ranged from blue to purple in a technique that used wax, a return to the [[encaustic painting|encaustic]] technique Dale had used a decade earlier.<ref name="Kritzwiser 1969">{{cite news|last1=Kritzwiser|first1=Kay|title=White walls, orange rug and WHAM!|work=The Globe and Mail|date=17 February 1969}}</ref>

The fifth-floor studio from which Dale worked in a century-old building on Toronto’s Market St. was severely damaged by a fire in March 1970 that destroyed the building. Dale lost her kiln and art supplies.<ref>{{cite news|title=Injured Women Rescued: $200,000 fire razes century old building|work=The Globe and Mail|date=10 March 1970}}</ref><ref>{{cite news|title=Four Hurt as 1882 Building Burns|work=The Toronto Star|date=10 March 1970}}</ref>  She was forced to relocate to [[Jarvis Street]] in the former [[The Salvation Army|Salvation Army]] offices where she pursued her fibreglass work. Her last known relief was a fibreglass sculpture for the Greenblade Junior High School Mississauga Ontario (architect W.R. Ussner). This thirty-inch wide mural, executed in three vertical sections, reached to a second-floor balcony was coloured in shades of blue. It was designed with apertures to allow children to physically interact with it. After the completion of this mural, Dale she indicated she wished to focus on painting.<ref name="Kritzwiser 1970">{{cite news|last1=Kritzwiser|first1=Kay|title=A Sculpture Goes to School|work=The Globe and Mail|date=31 August 1970}}</ref>

==Legacy==
Dale was acutely aware of the logistical and aesthetic challenges of integrating art and architecture. In interviews she emphasized the range of knowledge required by an artist implicated in the allied arts, such as taking into account the light, colour, forms, textures, and even humidity of architectural spaces, being aware of a variety of installation materials and techniques, and acquiring the skills to work with a client’s aesthetics and philosophy. She believed that her works needed to be integrated with the architectural space, rather than imposed upon it, and should invite visual and tactile engagement.<ref name=petite /><ref name="Kritzwiser 1969" /><ref name="Kritzwiser 1970" />  Dale’s gender regularly played a role in the publicity she received in the popular press, with remarks about her small physical size and fashion sense appearing repeatedly, and even interest in her leisure time.<ref name=petite /><ref name=Williams /><ref name="Kritzwiser 1969" /><ref name="Kritzwiser 1970" />

Her January 1978 death notice makes no mention of her career as an artist.<ref>{{cite news|title=Deaths: Dale, Margreta (Greta)|work=The Globe and Mail|date=30 January 1978}}</ref>

== References ==
{{reflist}}

== External links ==
* [http://centennialconcerthall.com/about-us/history Centennial Concert Hall, Winnipeg]

{{DEFAULTSORT:Dale, Greta}}
[[Category:Canadian women artists]]
[[Category:Canadian women sculptors]]
[[Category:Articles created via the Article Wizard]]
[[Category:1929 births]]
[[Category:1978 deaths]]
[[Category:People from Kelowna]]
[[Category:OCAD University alumni]]
[[Category:20th-century women artists]]
[[Category:20th-century Canadian artists]]
[[Category:20th-century Canadian sculptors]]