{{Infobox journal
| cover = [[File:Acta Biotheoretica.jpg]]
| editor = D.J. Kornet
| discipline = [[Theoretical biology]]
| formernames = 
| abbreviation = Acta Biotheor.
| publisher = [[Springer Science+Business Media]] 
| frequency = Quarterly 
| history = 1935–present 
| impact = 0.950
| impact-year = 2012
| website = http://www.springer.com/philosophy/epistemology+and+philosophy+of+science/journal/10441 
| link1 = http://www.springerlink.com/content/x63k85738714/ 
| link1-name = Online access 
| link2 = 
| link2-name =
| JSTOR = 
| OCLC =41558734
| LCCN =38011617
| CODEN =ABIOAN
| ISSN =0001-5342
| eISSN =1572-8358
}}
'''''Acta Biotheoretica:''' Mathematical and philosophical foundations of biological and biomedical science'' is a quarterly [[Peer review|peer-reviewed]] [[scientific journal]] published by [[Springer Science+Business Media]]. It is the official journal of the [[Jan van der Hoeven|Jan van der Hoeven Society for Theoretical Biology]]. The [[editor-in-chief]] is [[D.J. Kornet]] ([[Leiden University]]).

==Aims and scope==
The journal's focus is [[theoretical biology]] which includes mathematical representation, treatment, and modeling for simulations and [[descriptive statistics|quantitative description]]s. The journal's focus also includes the [[philosophy of biology]] which emphasizes looking at the methods developed to form [[biological theory]]. Topical coverage also includes [[biomathematics]], [[computational biology]], [[genetics]], [[ecology]], and [[morphology (biology)|morphology]].

==Abstracting and indexing==
This journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Biological Abstracts]]
* [[BIOSIS Previews]]
* [[Science Citation Index]]
* [[The Zoological Record]]
* [[Current Contents]]/Agriculture, Biology & Environmental Sciences
* [[Elsevier BIOBASE]]
* [[EMBASE]]
* [[EMBiology]]
* [[GEOBASE (database)|GEOBASE]]
* [[Chemical Abstracts Service]]
* [[CAB Abstracts]]
* [[Global Health]]
* [[Academic OneFile]]
* [[Astrophysics Data System]]
* [[Current Index to Statistics]]
* [[International Bibliographic Information on Dietary Supplements]]
* [[International Nuclear Information System|INIS]] - Atomindex
* [[MEDLINE]]
* [[Scopus]]
* [[The Philosopher's Index]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 0.950.<ref name=WoS>{{cite book |year=2013 |chapter=Acta Biotheoretica |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

==References==
{{reflist}}

== Further reading ==
*{{cite book
 | last1 = Reydon
 | first1 = Thomas A. C.
 | last2 = Dullemeijer
 | first2 = Piet
 | last3 = Hemerik
 | first3 = Lia
 | editor1-last = Reydon
 | editor1-first = Thomas A.C.
 | editor2-last = Hemerik
 | editor2-first = Lia
 | title = Current Themes in Theoretical Biology: A Dutch Perspective
 | publisher = [[Springer Netherlands]]
 | chapter = The History of Acta Biotheoretica and the Nature of Theoretical Biology
 | chapterurl = http://edepot.wur.nl/172678
 | format =
 | edition =
 | date = 2005
 | oclc = 209840350
 | pages = 1–8
 | language =
 | url = https://books.google.com/books?id=ez_JduJm2voC&printsec=frontcover&dq=The+History+of+Acta+Biotheoretica+and+the+Nature+of&hl=en&sa=X&ei=cwQXUqrgAoKo9gTyqIH4BQ&ved=0CDoQuwUwAA#v=onepage&q=The%20History%20of%20Acta%20Biotheoretica%20and%20the%20Nature%20of&f=false
 | doi = 10.1007/1-4020-2904-7_1
 | id =
 | isbn =978-1-4020-2901-1 }}

== External links ==
*{{Official website|http://www.springer.com/philosophy/epistemology+and+philosophy+of+science/journal/10441}}

[[Category:Biology journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Mathematical and theoretical biology]]
[[Category:Publications established in 1935]]
[[Category:English-language journals]]