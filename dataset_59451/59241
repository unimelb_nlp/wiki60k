{{Infobox journal
| title = Open Life Sciences
| formernames = Central European Journal of Biology
| discipline = [[Biology]]
| abbreviation = Open Life Sci.
| publisher = [[Walter de Gruyter]]
| frequency = Monthly
| history = 2006-present
| impact = 0.710
| impact-year = 2014
| openaccess= Yes
| license = [[Creative Commons-BY-NC-ND]]
| website = http://www.degruyter.com/view/j/biol
| link1 = http://link.springer.com/journal/11535
| link1-name = Journal homepage at Springer
| ISSN = 2391-5412
| ISSN2label = '' Central European Journal of Biology'':
| ISSN2 = 1895-104X
| eISSN2 = 1644-3632
| OCLC = 907772243
| LCCN = 2015247750
| CODEN = OLSPBT
}}
'''''Open Life Sciences''''' is a  [[peer-reviewed]]  [[open access]] [[scientific journal]] covering all areas of the [[life sciences]]. It was established in 2006 as the ''Central European Journal of Biology'' and co-published by [[Versita]] and [[Springer Science+Business Media]]. It obtained its current title in 2014 when it was moved completely to the [[De Gruyter Open]] [[imprint (trade name)|imprint]], obtaining its current title and switching to full [[open access]]. The [[editor-in-chief]] is Mariusz Ratajczak ([[University of Louisville]]).

== Abstracting and indexing ==
The journals is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[AGRICOLA]]
* [[Biological Abstracts]]
* [[BIOSIS Previews]]
* [[CAB Direct (database)|CAB Direct]]
* [[Chemical Abstracts Service]]
* [[Current Contents]]/Agriculture, Biology, and Environmental Sciences
* [[Elsevier Biobase]]
* [[EMBiology]]
* [[GeoRef]]
* [[Global Health]]
* [[ProQuest]]
* [[Science Citation Index Expanded]]
* [[Scopus]]
* [[The Zoological Record]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 0.710.<ref name=WoS>{{cite book |year=2015 |chapter=Central European Journal of Biology |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.degruyter.com/view/j/biol}}

[[Category:Biology journals]]
[[Category:Publications established in 2006]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Creative Commons Attribution-licensed journals]]
[[Category:Walter de Gruyter academic journals]]