{{Italic title}}
The '''''Journal of Applied Ichthyology''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] on [[ichthyology]], [[marine biology]], and [[oceanography]] published by [[Wiley-Blackwell]]. It is the official journal of the [[World Sturgeon Conservation Society]] and of the [[Deutsche Wissenschaftliche Kommission für Meeresforschung]] ("German Scientific Commission for the Exploration of the Sea"). The [[editors-in-chief]] are [[Harald Rosenthal]] and Dietrich Schnack.

The ''Journal of Applied Ichthyology'' was established as a separate journal in 1985, but merged with the ''Archive of Fishery and Marine Research'' in 2005. The latter journal had been established as ''Berichte der Deutschen Wissenschaftlichen Kommission für Meeresforschung'', published from before [[World War I]] (with a hiatus for the war)<ref>{{citation |first=W. |last=Schnakenbeck |title=Berichte der Deutschen wissenschaftlichen Kommission für Meeresforschung |journal=Naturwissenschaften |volume=13 |issue=23 |year=1925 |pages=512–515 |doi=10.1007/BF01500694|language=de}}</ref> until volume 24 in 1975–1976. It was then renamed to ''Meeresforschung: Reports on Marine Research'' ({{ISSN|0341-6836}}, {{OCLC|4048873}}) and was published by Paul Parey Verlag in Hamburg from 1976 until 1991, when the last volume (nr. 33) appeared in print. From 1994 (nr. 42) until 2005 it was published as the ''Archive of Fishery and Marine Research'' (''Archiv für Fischerei- und Meeresforschung''; {{ISSN|0944-1921}}).

== References ==
{{reflist}}

==External links==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1439-0426}}
* Print: {{ISSN|0175-8659}}
* Online {{ISSN|1439-0426}}

[[Category:Ichthyology journals]]
[[Category:Oceanography journals]]
[[Category:Biological oceanography]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Publications established in 1985]]