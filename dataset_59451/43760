[[File:Ki-43-I captured in flight over Brisbane 1943.jpg|right|thumb|300px|A [[Nakajima Ki-43]] ''Hayabusa'' ("Oscar") (often confused with the [[Mitsubishi A6M|Mitsubishi A6M "Zero"]]) flying over Brisbane, Australia in 1943. The Ki-43 was reconstructed from several captured aircraft, by the Technical Air Intelligence Unit (TAIU), at Hangar 7 at Eagle Farm, Brisbane.]]

'''Technical Air Intelligence Units''' (TAIU) were joint [[Allies of World War II|Allied]] military intelligence units formed during [[World War II]] to recover [[Empire of Japan|Japanese]] aircraft to obtain data regarding their technical and tactical capabilities.

The first such unit, known later as Technical Air Intelligence Unit&ndash;[[South West Pacific]] (TAIU&ndash;SWPA), was formed in November 1942 by the [[United States Navy]] (USN), [[United States Army Air Forces]] (USAAF) and [[Royal Australian Air Force]] (RAAF) at [[Eagle Farm Airport|Eagle Farm Airbase]], [[Brisbane]], Australia, in November 1942.<ref name="Trojan">{{cite web |url= http://www.j-aircraft.com/research/David_Trojan/Technical%20Air%20Intelligence%20Wreck%20chasing%20in%20the%20Pacific%20during%20the%20war.pdf |title=Technical Air Intelligence: Wreck Chasing in the Pacific during the War |first=David |last=Trojan |work=j-aircraft.com | accessdate=16 February 2011 }}</ref>

During 1943&ndash;44, three other TAIUs were formed in the other Allied theatres of the [[Pacific War]].<ref name="j-aircraft">{{cite web |url= http://www.j-aircraft.com/research/jas_jottings/end_of_the_jaaf_and_jnaf.htm |title= The End of the JAAF and JNAF |first=Peter  |last=Starkings |work=j-aircraft.com |year=2011 |accessdate=16 February 2011}}</ref><ref name="Skaarup">Harold Skaarup, 2006, ''RCAF War Prize Flights, German and Japanese Warbird Survivors'', Bloomington Indiana, iUniverse, pp33&ndash;4.</ref>

* [[South East Asian Theatre|South East Asia]]: ATAIU&ndash;SEA; British [[Royal Air Force]] (RAF)/USAAF
* [[Pacific Ocean Areas]]: TAIU&ndash;POA; USN
* [[Second Sino-Japanese War|China]]: [[Republic of China Air Force]]

A proposed joint U.S. Army-U.S. Navy research unit in the continental United States was never established, as neither service was prepared to work with the other.<ref name="Trojan"/> Some Japanese aircraft were tested in the US, at various bases, by pilots from the [[Naval Air Test Center]], the USAAF [[U.S. Air Force Test Pilot School|Test Training Unit]] (which was established with the assistance of RAF technical intelligence units in Europe) and the [[National Advisory Committee for Aeronautics]].

Crashed and captured aircraft were located, identified, and evaluated (often in or near the front lines), before being recovered for further tests. Aircraft that were not too badly damaged were rebuilt for test flights that revealed vulnerabilities that could be exploited. Examination of the materials used in the construction of aircraft allowed the Allies to analyse Japanese war production. The unit also absorbed a small team who developed the [[World War II Allied names for Japanese aircraft|code name system]] for Japanese aircraft, and produced [[aircraft recognition]] charts and photographs.<ref name="Trojan"/>

==Early technical air intelligence operations==
The [[attack on Pearl Harbor]] demonstrated the key role of aircraft in the [[Pacific War]], but the United States possessed virtually no information about the capabilities of Japanese aircraft. Several shot down aircraft were recovered from Hawaii and examined by the Naval Air Test Center and the USAAF Test Training Unit, who completed their own separate studies.

A [[A6M Zero|Mitsubishi A6M "Zero"]] of the [[Imperial Japanese Navy Air Service]] made a forced landing in June 1942 on [[Akutan Island]], off [[Alaska]]. The aircraft (known later as the "[[Akutan Zero]]") was recovered by the USN and shipped to [[NAS North Island]], California, where it was repaired and made a number of test flights to determine its performance and capabilities.<ref name="Trojan"/>

In late 1942, the [[Australian Army]] captured lightly damaged or incomplete examples of the [[Nakajima Ki-43]] ''Hayabusa'' ("Oscar") &ndash; the main fighter used by the [[Imperial Japanese Army Air Force]] during the war &ndash; in [[Papua New Guinea]]. The Ki-43s were discovered nearly intact at [[Buna Airfield]], after the [[Battle of Buna–Gona]], and were shipped to Australia for examination.

==TAIU operations==
[[File:J2M over Malaya.jpg|right|thumb|Japanese Mitsubishi J2Ms with ATAIU-SEA markings in flight over [[British Malaya]] in December 1945, during evaluation by [[Royal Air Force]] officers from [[Seletar Airport|RAF Seletar]].]]
[[File:A6M Zeros over Malaya.jpg|thumb|A6M2 (left) and A6M5 Zero in Malaya being tested and evaluated by Japanese pilots under the supervision of RAF officers. The A6M5's cockpit survives today at the [[Imperial War Museum Duxford]].<ref name="Mitsubishi Zero A6M5 cockpit">{{citation|title = Mitsubishi Zero A6M5 cockpit|url = http://www.iwm.org.uk/collections/item/object/70000203|publisher = Imperial War Museum|accessdate = 22 Nov 2013}}</ref>]]
It was in order to consolidate and co-ordinate these different operations that the Technical Air Intelligence Unit was formed, based in Hangar 7 at the RAAF/USAAF [[Eagle Farm Airport|Eagle Farm Airbase]], in [[Brisbane]], Australia, during November 1942.<ref name="Trojan"/> By early 1943 an "Oscar" had been built using parts from five different aircraft.<ref name="Trojan"/> Test flights included a mock [[dogfight]] against a [[Supermarine Spitfire|Spitfire V]]. It was concluded that the "Oscar" was superior to the Spitfire below 20,000 feet. In late 1943 the aircraft was shipped to the United States aboard the [[escort carrier]] {{USS|Copahee|CVE-12|2}}, and sent to Wright Field where it was flown and evaluated.<ref name="airforcehistory">{{cite web |url= http://www.airforcehistory.hq.af.mil/PopTopics/airtechintel.htm |title=Air Technical Intelligence |work=Air Force Historical Studies Office |year=2008 |accessdate=16 February 2011}}</ref>

[[File:Mitsubishi A62M Zero USAF.jpg|200px|thumb|left|Mitsubishi A6M2 "Zero" at the [[National Museum of the United States Air Force]] at [[Dayton, Ohio|Dayton]], Ohio. This aircraft was found near [[Kavieng]] on [[New Ireland (island)|New Ireland]], Papua New Guinea, and probably operated by the 6th ''Kokutai'' (Squadron) and later by the 253rd ''Kokutai''. It is painted to represent a section leader's aircraft from the aircraft carrier {{Ship|Japanese aircraft carrier|Zuihō||2}} during the [[Battle of the Bismarck Sea]] in March 1943.<ref>{{cite web |url= http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?id=471 |title=Factsheets : Mitsubishi A6M2 Zero |work=[[National Museum of the United States Air Force]] |year=2011 |accessdate=16 February 2011}}</ref>]]

Further success came in late December 1943, when [[US Marine Corps|US Marines]] captured the airfield at [[Cape Gloucester]] on the north coast of [[New Britain]], finding many wrecks and several nearly intact aircraft. TAIU officers logged the serial numbers of the aircraft, and engine configurations, serial numbers and dates of manufacture. They inspected cockpits for layout and control locations, and armour plate. They recovered armaments and noted their locations and mounts. Some reports provided considerable detail, including oil and fuel tank capacities, and special electronics installed. One of these was a [[Kawasaki Ki-45]] "Nick" fighter, about which little was known. Another fighter, a [[Kawasaki Ki-61]] "Tony", was also examined.<ref name="Trojan"/>

One of the biggest problems faced by the TAIU teams was Allied troops, who commonly stripped enemy aircraft for "souvenirs". Efforts were made to minimize indiscriminate souvenir hunting and troops were encouraged to turn in all captured items and report enemy aircraft wrecks. Most of these efforts were in vain, and it remained a constant problem throughout the war. Another obstacle was that most Japanese aircraft fell into the ocean, and those that did not often crashed in isolated areas that were difficult to reach. As an example, a newly developed [[Yokosuka D4Y]] "Judy" dive bomber with an [[Inline engine (aviation)|in-line engine]] crashed six miles inland, on a {{Convert|1500|ft|m|sing=on}} hill, on [[Santa Isabel Island]]. The TAIU had to recruit local men to cut a trail to the crash site with machetes, and then carry out the engine on a cradle woven from tree bark.<ref name="Trojan"/>

Crashed Enemy Aircraft Reports (CEARs) were systematically compiled from April 1943. In February 1944, it was agreed that production data on enemy equipment was essential, and more extensive reports detailed the age and condition of captured equipment to give an indication of the general state of the Japanese war economy, paying particular attention to the name plates and markings which gave information on the manufacturers. Eventually a special unit known as "JAPLATE" was created to conduct this task, and 6,336 intact name plates or their details were collected.<ref name="Trojan"/>

In mid-1944, U.S. Navy personnel were withdrawn from the TAIU and reassigned to NAS Anacosta to form the Technical Air Intelligence Centre (TAIC) to centralise and co-ordinate the work of test centres in the United States with the work of TAIUs in the field. The unit was then renamed '''TAIU for the South West Pacific Area''' (TAIU-SWPA).<ref name="j-aircraft"/>

Technical Air Intelligence operations were fully developed by the time of the [[Philippines Campaign (1944–45)|invasion of the Philippines]]. Considerable instruction was given to the troops on the equipment likely to be found and the importance of its preservation. The TAIU-SWPA moved from Australia to the Philippines in early 1945 and gained an appreciation of the state of enemy technological and economic development essential to the build-up for the planned [[Operation Downfall|invasion of Japan]].<ref name="Trojan"/>  Aircraft acquired there included examples of the [[Mitsubishi A6M Zero]], [[Mitsubishi J2M]] "Jack", [[Kawasaki Ki-45]] "Nick", [[Kawasaki Ki-61]] "Tony", [[Kawanishi N1K]] "George", [[Nakajima Ki-44]] "Tojo", and [[Nakajima Ki-84]] "Frank" fighters; the [[Nakajima B5N]] "Kate", [[Nakajima B6N]] "Jill", [[Yokosuka D4Y]] "Judy", and [[Mitsubishi G4M]] "Betty" bombers; the [[Showa L2D]] "Tabby" transport, and the [[Mitsubishi Ki-46]] "Dinah" reconnaissance aircraft.<ref name="airforcehistory"/>

==Other Technical Air Intelligence Units==
A joint [[Royal Air Force|RAF]]/USAAF unit, known as the TAIU for [[South-East Asian theatre of World War II|South East Asia]]" (ATAIU-SEA) was formed in [[Calcutta]] in late 1943, and disbanded at [[Singapore]] in 1946. Two other units were also created; "TAIU for the Pacific Ocean Area" (TAIU-POA), a U.S. Navy unit which operated in the Pacific Islands, and "TAIU for China" (TAIU-CHINA) under the control of [[Chiang Kai-shek]]'s Nationalists.<ref name="j-aircraft"/>

==Post-war operations==
Technical Air Intelligence Units operated in Japan after the end of the war, shifting from tactical intelligence to post-hostilities investigations. General [[Henry H. Arnold|Hap Arnold]] ordered the preservation of four of every type of aircraft<ref name="Trojan"/> one of each for the USAAF, USN, RAF and museum collections.<ref name="j-aircraft"/> By the end of 1945 these were collected together at [[Yokohama]] Naval Base. One hundred and fifteen aircraft were shipped to the United States, with 73 going to the Army and 42 to the Navy. However, lack of funds, storage space and interest meant that only six aircraft were restored, flown and evaluated by the Army and only two by the Navy. Eventually, 46 complete aircraft were sent to various museums while the rest were scrapped.<ref name="Trojan"/> By early 1946 ATAIU-SEA in Singapore had collected 64 Japanese Army and Navy aircraft, most in flyable condition, for shipment to the UK. However, lack of shipping space prevented this operation and only four eventually arrived in England to be put in display in museums.<ref name="j-aircraft"/>

==See also==
* [[Air Fighting Development Unit]] (RAF)
* [[Operation Lusty]]
* [[Naval Air Fighting Development Unit]] (Royal Navy)
* [[No. 1426 Flight RAF]]
* [[Zirkus Rosarius]]

==References==
{{Reflist}}

==External links==
* [http://www.hangar7.org.au/ataiu.htm Eagle Farm Airfield during WW2]
* [http://www.risingdecals.com/DyingSun/DyingSun_A.htm Japanese Aircraft tested by Yanks and British]
* [http://home.watvc.com/tware/atnum.htm Allied Technical Intelligence Unit Aircraft Numbers]

[[Category:Technical intelligence during World War II]]
[[Category:Royal Air Force units]]
[[Category:Military units and formations of the Royal Australian Air Force]]
[[Category:Military units and formations of the United States Army Air Forces]]
[[Category:Military units and formations of the United States Navy]]
[[Category:Military units and formations of the Republic of China]]