{{Infobox military person
|name= Walter Bradel
|birth_date={{Birth date|1911|7|31|df=y}}
|death_date={{death date and age|1943|5|5|1911|7|31|df=y}}
|image=
|birth_place=[[Breslau]], now [[Poland]]
|death_place=
|placeofburial=[[Ysselsteyn German war cemetery]], [[Netherlands]]<br />(Block CX, Row 4, Grave 85)
|nickname=
|branch=[[German Army (Wehrmacht)|Army]]<br/>[[Luftwaffe]]
|allegiance={{flag|Nazi Germany}}
|serviceyears=1934–43
|rank= ''[[Oberstleutnant]]''
|commands=II./KG 2
|unit= ''[[Condor Legion]]''<br/>''[[Kampfgeschwader 3]]''<br/> ''[[Kampfgeschwader 2]]''
|battles=[[Spanish Civil War]]
----
[[World War II]]
*[[Polish Campaign]]
*[[Battle of France]]
*[[Balkans Campaign (World War II)|Balkans Campaign]]
*[[Eastern Front (World War II)|Eastern Front]]
|awards=[[Spanish Cross]] in Silver with Swords
[[Iron Cross]] First and Second Class<br/> [[Front Flying Clasp of the Luftwaffe]] in Gold<br/>[[Ehrenpokal der Luftwaffe|Honorary Cup of the Luftwaffe]] <br/> [[Knight's Cross of the Iron Cross]]
|laterwork=}}

'''Walter Bradel''' (31 July 1911 – 5 May 1943) was a ''Luftwaffe'' [[Bomber]] pilot and ''[[Geschwaderkommodore]]'' of KG 2 during [[World War II]]. He was also a recipient of the [[Knight's Cross of the Iron Cross]] ({{lang-de|Ritterkreuz des Eisernen Kreuzes}}). The Knight's Cross of the Iron Cross was awarded to recognise extreme battlefield bravery or successful military leadership.

==Luftwaffe career==
Bradel joined the ''[[Reichswehr|Heer]]'' in 1933, before joining the ''Luftwaffe''.
Bradel flew the [[Dornier Do 17]] until 1942, and then the [[Dornier Do 217]]. Bradel was awarded the Knight's Cross on 17 July 1941 for his units particular effectiveness over the [[Grodno]] area, in which the Soviet 6th Cavalry Corps was routed, and lost 105 tanks.<ref>Bergström 2007 (Barbarossa title), p. 23.</ref>

In 1943 Bradel's unit started flying bombing missions over England once more. During the night of 4 May 1943 to 5 May [[Angriffsführer England|''Angriffsführer'' England]] ordered a consolidated attack on [[Norwich]]. Involved were 43 aircraft from KG 2 under the command of Bradel, which took off from the airport of [[Soesterberg]]. The attack force was augmented by aircraft from II./KG 40 and 36 [[Ju 88]] from [[KG 6]]. Bradel's [[Do 217|Do 217K]] was attacked by a British nightfighter, and suffered engine damage. Pilot ''[[Leutnant]]'' [[Ernst Andres]] attempted an emergency landing near [[Landsmeer]], [[Amsterdam]]. The aircraft was 80% damaged and Bradel was killed.<ref>de zeng ''et al'' 2007 Vol 1, p. 24.</ref> Walter Bradel is buried at the German Military Cemetery at [[Ysselsteyn]], [[Netherlands]] (Block CX, Row 4, Grave 85)<ref>MacLean 2007, p. 224.</ref>

Bradel's victor was [[Flying Officer]] Brian "Scruffy" Williams and his radar operator [[Pilot Officer]] Doug Moore of [[No. 605 Squadron RAF]]. They flew an intruder sortie to Eindhoven that night and spotted two Dornier Do 217s circling the field. He fired at the lead Dornier but did not claim a hit. Attacking the second, he scored hits but did not claim it destroyed, but as damaged. Wing Commander C. D. Tomalin contacted Williams the following night to confirm the enemy bomber had crashed. The loss was reported in German newspapers and was given a lot of coverage. Williams learned the name of the pilot was ''Major'' Bradel. The first Dornier, piloted by ''[[Leutnant]]'' Alfons Schlander of 2./''KG'' 2, also crashed. But whether this was a result of Williams' attack is unknown.<ref>bowman 2010, p. 66.</ref>

==Awards==
* [[Spanish Cross]] in Silver with Swords 6 June 1939
* [[Cruz de Guerra]] June 1939
* [[Iron Cross]] 2nd and 1st Class
* [[Front Flying Clasp of the Luftwaffe|Front Flying Clasp of the ''Luftwaffe'']] in Gold
* ''[[Ehrenpokal der Luftwaffe]]'' (30 July 1941)
* [[Knight's Cross of the Iron Cross]] on 17 September 1941 as ''[[Hauptmann]]'' and ''[[Staffelkapitän]]'' of the 9./KG 2<ref>Scherzer 2007, p. 237.</ref>

==References==

===Citations===
{{Reflist|25em}}

===Bibliography===
{{Refbegin}}
* Bergström, Christer (2007). ''Barbarossa - The Air Battle: July–December 1941''. London: Chervron/Ian Allan. ISBN 978-1-85780-270-2.
* Bowman, Martin (2010). ''Mosquito Mayhem: de Havilland's Wooden Wonder in Action in WWII''. Pen & Sword. ISBN 184884323-2
* {{Cite book
  |last=Fellgiebel
  |first=Walther-Peer
  |authorlink=Walther-Peer Fellgiebel
  |year=2000
  |origyear=1986
  |title=Die Träger des Ritterkreuzes des Eisernen Kreuzes 1939–1945 — Die Inhaber der höchsten Auszeichnung des Zweiten Weltkrieges aller Wehrmachtteile
  |trans_title=The Bearers of the Knight's Cross of the Iron Cross 1939–1945 — The Owners of the Highest Award of the Second World War of all Wehrmacht Branches
  |language=German
  |location=Friedberg, Germany
  |publisher=Podzun-Pallas
  |isbn=978-3-7909-0284-6
}}
* {{Cite book
  |last=MacLean
  |first=French L
  |year=2007
  |title=Luftwaffe Efficiency & Promotion Reports: For the Knight's Cross Winners
  |location=Atglen, Pennsylvania
  |publisher=Schiffer Military History
  |isbn=978-0-7643-2657-8
}}
* {{Cite book
  |last=Scherzer
  |first=Veit
  |year=2007
  |title=Die Ritterkreuzträger 1939–1945 Die Inhaber des Ritterkreuzes des Eisernen Kreuzes 1939 von Heer, Luftwaffe, Kriegsmarine, Waffen-SS, Volkssturm sowie mit Deutschland verbündeter Streitkräfte nach den Unterlagen des Bundesarchives
  |trans_title=The Knight's Cross Bearers 1939–1945 The Holders of the Knight's Cross of the Iron Cross 1939 by Army, Air Force, Navy, Waffen-SS, Volkssturm and Allied Forces with Germany According to the Documents of the Federal Archives
  |language=German
  |location=Jena, Germany
  |publisher=Scherzers Militaer-Verlag
  |isbn=978-3-938845-17-2
}}
* de Zeng, H.L; Stanket, D.G; Creek, E.J. (2007). ''Bomber Units of the Luftwaffe 1933-1945; A Reference Source, Volume 1''. Ian Allan Publishing. ISBN 978-1-85780-279-5.
{{Refend}}

{{s-start}}
{{s-mil}}
{{succession box|
before=''Oberstleutnant'' [[Hans von Koppelow]]|
after=''Oberstleutnant'' [[Karl Kessel]]|
title= Commander of [[Kampfgeschwader 2|''Kampfgeschwader'' 2]]|
years=23 January 1943 – 5 May 1943
}}
{{s-end}}

{{Knight's Cross recipients of KG 2}}
{{Subject bar
| portal1=Aviation
| portal2=Biography
| portal3=Military of Germany
| portal4=World War II
}}

{{DEFAULTSORT:Bradel, Walter}}
[[Category:1911 births]]
[[Category:1943 deaths]]
[[Category:People from Wrocław]]
[[Category:People from the Province of Silesia]]
[[Category:Condor Legion personnel]]
[[Category:German World War II pilots]]
[[Category:German military personnel killed in World War II]]
[[Category:Recipients of the Spanish Cross]]
[[Category:Recipients of the Knight's Cross of the Iron Cross]]
[[Category:Burials at Ysselsteyn German war cemetery]]
[[Category:Aviators killed in aviation accidents or incidents]]