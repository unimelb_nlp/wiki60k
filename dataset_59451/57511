{{Refimprove|date=January 2013}}
'''ICHEP''' or '''International Conference on High Energy Physics''' is one of the most prestigious international scientific [[Academic conference|conferences]] in the field of [[particle physics]], bringing together leading theorists and experimentalists of the world. It was first held in 1950, and is biennial since 1960. Since the first conferences of the series took place in [[Rochester, New York]], the event is also commonly referred to as the '''Rochester conference'''.<ref name="google">{{cite book|title=Bulletin of the Atomic Scientists|publisher=Educational Foundation for Nuclear Science, Inc.|issn=0096-3402|url=https://books.google.com/books?id=KAcAAAAAMBAJ|page=96|accessdate=2015-01-03}}</ref>

==Past Conferences==
{{Cleanup-list|section|date=April 2010}}
{{colbegin||22em}}
*I Rochester (1950)
*II Rochester (1952)
*III Rochester (1952)
*IV Rochester (1954)
*V Rochester (1955)
*VI Rochester (1956)
*VII Rochester (1957)
*VIII Geneva (1958)
*IX Kiev (1959)
*X Rochester (1960)
*XI Geneva (1962)
*XII Dubna (1964)
*XIII Berkeley (1966)
*XIV Vienna (1968)
*XV Kiev (1970)
*XVI Chicago (1972)
*XVII London (1974)
*XVIII Tbilisi (1976)
*XIX Tokyo (1978)
*XX Madison (1980)
*XXI Paris (1982)
*XXII Leipzig (1984)
*XXIII Berkeley (1986)
*XXIV Munich (1988)
*XXV Singapore (1990)
*XXVI Dallas (1992)
*XXVII Glasgow (1994)
*XXVIII Warsaw (1996)
*XXIX Vancouver (1998)
*XXX Osaka (2000)
*XXXI Amsterdam (2002)
*XXXII Beijing (2004)
*XXXIII Moscow (2006)
*XXXIV Philadelphia (2008)
*XXXV Paris (2010)
*XXXVI Melbourne (2012)
*XXXVII Valencia (2014)
*[http://ichep2016.org/ XXXVIII Chicago (2016)]
{{colend}}

==Future conferences==
*XXXIX to be held in Seoul (2018)

== References ==
{{Reflist}}

==External links==
* https://twitter.com/pressichep

[[Category:1950 establishments in New York]]
[[Category:Physics conferences]]
[[Category:Recurring events established in 1950]]