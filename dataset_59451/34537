{{good article}}
{{Use American English|date=December 2016}}
{{Use dmy dates|date=May 2011}}
{{Infobox military unit
|unit_name= 3rd Sustainment Brigade
|image=[[File:3rd Sustainment Brigade.jpg|160px]]
|caption=3rd Sustainment Brigade shoulder sleeve insignia
|dates=21 April 2006 – present
|country=United States
|allegiance=[[United States Army]]
|branch=
|type=[[Sustainment Brigade]]
|role=[[Sustainment Brigade|Sustainment]]
|size=[[Brigade]]
|command_structure=[[United States Army Forces Command|FORSCOM]]
|garrison=[[Fort Stewart]], [[Georgia (U.S. state)|Georgia]]
|garrison_label=
|nickname=
|patron=
|motto="Heart of the Rock"
|colors=
|colors_label=
|march=
|mascot=
|equipment=
|equipment_label=
|battles=
|anniversaries=
|decorations=Presidential Unit Citation
|battle_honours=
<!-- Commanders -->
|commander1=
|commander1_label=
|commander2=
|commander2_label=
|commander3=
|commander3_label=
|notable_commanders=
<!-- Insignia -->
|identification_symbol=[[File:3SustainBdeDUI.jpg|150px]]
|identification_symbol_label=Distinctive Unit Insignia
|identification_symbol_2=
|identification_symbol_2_label=
}}

The '''3rd Infantry Division Sustainment Brigade''' is a [[Sustainment Brigade|sustainment]] [[brigade]] of the [[United States Army]] headquartered at [[Fort Stewart]], [[Georgia (U.S. state)|Georgia]]. The 3rd Sustainment Brigade is responsible for providing logistical support to the [[3rd Infantry Division (United States)|3rd Infantry Division]], however the modular nature of the brigade means that it takes on other roles while deployed. Personnel serving under this brigade wear the [[Shoulder Sleeve Insignia]] exclusive to this brigade, rather than the insignia designated for the [[3rd Infantry Division (United States)|3rd Infantry Division]].

Though its lineage dates back to 1957, the unit was not designated as a separate unit until 2005. The 3rd Sustainment Brigade has served three tours in [[Iraq]] in support of [[Operation Iraqi Freedom]], its first tour being the initial invasion and securing of the country, followed by a second tour of duty as logistics support for units around Baghdad, and finally as the multinational division supply lines in the northern division of the country. 

The Brigade left for a planned nine-month deployment to Afghanistan in late November 2012.

==Organization==
The 3rd Sustainment Brigade has a permanent organization of two attached battalions, however this number can be changed when the unit is deployed in a theater of operations. These permanent attachments include the [[87th Combat Sustainment Support Battalion]],<ref name="3SB"/> and a [[Special Troops Battalion]].<ref name="3SB"/> The units are headquartered at [[Fort Stewart]]. [[Georgia (U.S. state)|Georgia]], along with the brigade's [[Headquarters and Headquarters Company]]. Under non-deployment circumstances, the HHC of the Brigade would, in turn, be directly subordinate to the 3rd Infantry Division.

==History==
[[File:Iraq-War-Map.png|thumb|left|Battle plan for the [[Iraq War]], with the 3rd SB supporting the [[3rd Infantry Division (United States)|3rd Infantry Division]]'s attack in the southern region of the country.]]

The unit was constituted 1 July 1957 in the regular army as Headquarters and Headquarters Detachment, 3rd Infantry Division Trains, and activated at [[Fort Benning]], [[Georgia (U.S. state)|Georgia]]. On 20 March 1963, the unit was consolidated with the [[3rd Infantry Division Band]], which has previously been organized in 1943 as the ''band, 3rd Infantry Division''. On 15 March 1968, the unit was re-organized and re-designed as [[Headquarters and Headquarters Company]] and Band, 3rd Infantry Division Support Command.<ref name="3SB"/> On 21 May 1972, the unit was re-organized and re-designed as Headquarters and Headquarters Company, 3rd Infantry Division Support Command.<ref name="3SB"/> It was deployed to as part of the [[Cold War]] buildup, should hostilities arise in the region with the [[Soviet Union]]. Though the unit was placed on alert constantly, it never saw action during its time in the region, which lasted from its activation until 1991.<ref name="3ID"/>

In the fall of 2002, the Division Support Command deployed in support of [[Operation Iraqi Freedom]] I, earning the [[Presidential Unit Citation (United States)|Presidential Unit Citation]] along with the rest of the 3rd Infantry Division.<ref name="3SB">[http://www.stewart.army.mil/3DIDWeb/3rd%20Sustainment/3rdSustainmentBrigadehome.htm 3rd Infantry Division Homepage: 3rd Sustainment Brigade], 3rd Infantry Division Staff. Retrieved 21 February 2008</ref> The Brigade and its division had spearheaded the invasion into the nation of [[Iraq]], supporting the four brigades as they pushed through southern Iraq and into the capital of Baghdad. After the initial invasion and capture of Baghdad, the brigade remained in the city, supporting the 3rd Infantry Division as the unit conducted counterinsurgency and infrastructure activities in the area. It returned home to [[Fort Stewart]] in August 2003.<ref name="3ID"/>

The Division Support Command was renamed the Division Support Brigade and deployed to [[Iraq]] in the fall of 2004 in support of Operation Iraqi Freedom III, leading coalition troops in control of the [[Baghdad]] area. Under Multinational Division, Baghdad. It returned home again in January 2006.<ref name="3ID">[http://www.stewart.army.mil/3DIDWeb/History/Historyhome.htm 3rd Infantry Division: History] 3rd Infantry Division Staff. Retrieved 21 February 2008</ref> The Division Support Brigade was reorganized as the 3rd Support Brigade on 15 June 2005, and re-designated as the 3rd Sustainment Brigade on 21 April 2006.<ref>[http://www.tioh.hqda.pentagon.mil/Sustain/3SustainmentBrigade.htm 3rd Sustainment Brigade], ''The Institute of Heraldry'', United States Army. Retrieved 21 February 2008</ref>

In 2007, the Brigade saw its third deployment to Iraq during the Iraq War, relieving the [[45th Sustainment Brigade]] of its areas of responsibility of Multinational Division, North, comprising over a dozen [[Forward Operating Base]]s.<ref>[http://www.defendamerica.mil/articles/jul2007/a070607ej3.html Sustainment Brigade Hands off Q-West Mission], Staff Sgt. Carlos Lazo, ''DefendAmerica.mil''. Accessed 21 February 2008</ref> The Brigade deployed to Iraq in fall of 2007 following the [[Brigade Combat Team]]s and aviation brigade of the 3rd Infantry Division, however, for this deployment, the unit did not support the Division directly, as its mission in Multinational Division, North covered facilities management, not unit logistics. Brigade projects focused on building infrastructure throughout northern Iraq.<ref>[http://www.coastalcourier.com/news/archive/603/ 3rd ID preps for third deployment], Daisy Jones, coastalcourier.com Staff Writer. Retrieved 03-15-2008.</ref> The brigade served a total of 15 months in the country, headquartered at [[Forward operating base|Contingency Operating Base Q-West]] in Northern Iraq under the command of the [[316th Expeditionary Support Command]]. It was relieved during a change of command ceremony on 9 August 2008, at which time it returned to Fort Stewart. It was replaced by the [[16th Sustainment Brigade]].<ref>[http://www.16sustainment.army.mil/news/releases/2008-08-04_royer.pdf 16th Sustainment Brigade Press Release], 16th Sustainment Brigade Public Affairs Office. Retrieved 30 November 2008.</ref> Since its return, the brigade participated in an intramural soccer tournament at Fort Benning, something many of its members enjoyed doing, even while deployed.<ref>Lowers, Gaelen. [http://www.stewart.army.mil/frontlineonline/archivedpages/frontline11-13-08sports.pdf 3rd Sustainment out kicks 1/9 FA], 3rd Sustainment Brigade Public Affairs. Retrieved 30 November 2008.</ref>

In late November 2012 the Brigade deployed to Afghanistan for its first tour in support of [[Operation Enduring Freedom]] in Afghanistan.<ref>{{cite news|last=Staff|title=The Army as of Nov 21|newspaper=Army Time|date=3 December 2012}}</ref>

==Honors==

===Unit Decorations===
{| class="wikitable" border="1" cellpadding="4" cellspacing="0"
|- bgcolor="#efefef"
! Ribbon
! Award
! Year
! Notes
|-
||[[File:United States Army and U.S. Air Force Presidential Unit Citation ribbon.svg|50px]] 
||[[Presidential Unit Citation (United States)|Presidential Unit Citation]]
||2003
||for service in [[Operation Iraqi Freedom]]
|}

===Campaign Streamers===
{| border="1" cellpadding="4" cellspacing="0" 
|- bgcolor="#efefef"
! Conflict
! Streamer
! Year(s)
|-
|| [[Iraq War]]
|| [[Operation Iraqi Freedom|Operation Iraqi Freedom I]] 
|| 2002–2003
|-
|| Iraq War
|| [[Operation Iraqi Freedom|Operation Iraqi Freedom III]]
|| 2004–2006
|-
|| Iraq War
|| [[Operation Iraqi Freedom|Operation Iraqi Freedom V]]
|| 2007–2008
|}

==References==
{{reflist}}

==External links==
* [http://www.stewart.army.mil/units/home.asp?u=3SB 3rd Infantry Division Homepage: 3rd Sustainment Brigade]
* [http://www.tioh.hqda.pentagon.mil/Sustain/3SustainmentBrigade.htm The Institute of Heraldry: 3rd Sustainment Brigade]

[[Category:Sustainment Brigades of the United States Army|003]]