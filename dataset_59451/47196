{{Infobox NRHP
  | name = Conrad Rice Mill
  | nrhp_type =
  | image = Conrad Rice Mill - circa 1913.jpg
  | caption = Rice mill circa 1913
  | location= 307 Ann Street<br />[[New Iberia, Louisiana]]
  | coordinates = {{coord|29|59|52.57|N|91|48|38.65|W|display=inline,title}}
| locmapin = Louisiana#USA
  | built = 1912
  | architect =
  | architecture =
  | added = November 10, 1982
  | area = two acres
  | governing_body = Private
  | refnum = 82000437<ref name="nris">{{NRISref|2009a}}</ref>
}}

The '''Conrad Rice Mill''' is an independently owned and operated [[rice]] mill located in [[New Iberia, Louisiana]], and produces the '''Konriko''' brand of rice varieties. Established in 1912, it is the oldest independently owned rice mill in the United States still in operation, and listed in the [[National Register of Historic Places listings in Iberia Parish, Louisiana|National Register of Historic Places]].<ref>[http://www.nationalregisterofhistoricplaces.com/la/Iberia/state.html National Register of Historic Places - Louisiana, Iberia Parish]</ref>

== Origins ==
Philip Amelius (PA) Conrad (b. 1882, [[Pointe Coupee Parish, Louisiana]]) began rice farming in [[Avoyelles Parish, Louisiana]] around the turn of the 20th century with his uncle Charles Conrad. Wanting to operate independently, he moved to [[New Iberia, Louisiana|New Iberia]] on the banks of the [[Bayou Teche]], buying land on the north side of the Bayou near present-day [[Louisiana Highway 87|LA Highway 87]],<ref>{{Coord|29|59|53|N|91|47|42|W|display=inline}} (approximate location of first rice fields)</ref> just east of North Lewis Street, to grow rice. The [[harvest]]ed rice was shipped approximately 125 miles by steamboat to [[New Orleans, Louisiana|New Orleans]] for milling, as no local rice mills existed. After several crops, in 1910, he built a small rice mill on the property to avoid the need to ship to New Orleans.

In 1912 he relocated, rebuilding the mill at its current location at 307 Ann Street in New Iberia and establishing the '''Conrad Rice Milling and Planting Company'''. He purchased additional land and moved his rice fields to the south bank of the bayou (now the location of the former Julian Conrad house (Beau Revé) at 1312 East Main St. in New Iberia. The location of the rice fields on the Bayou Teche was crucial, since water was required for irrigation of the crops. In subsequent years the company continued to acquire land for farming further to the south, to just beyond the present location of [[U.S. Route 90 in Louisiana|US Highway 90]], accumulating a total of 575 acres.

== Early farming ==
Rice farming in the early history of the mill was without any mechanical equipment. The land was [[Tillage|tilled]] and [[Harrow (tool)|harrow]]ed using [[mule]]-drawn implements. After laying [[seed]] by hand in the fields, small [[levee]]s were built with shovels of sufficient height to hold water at a depth of 1 to 1 ½ inches. Water was brought from the [[Bayou Teche]] (up to three miles away) using an irrigation system built specifically for that purpose. Water [[pump]]s pulled water from the [[bayou]] to an elevated trough twelve feet above the bayou that angled down over 700 feet to a street-level [[flume]], using gravity to drive the water to the fields. The flume traversed over a section of the city of [[New Iberia, Louisiana|New Iberia]], being routed under streets using salvaged steam boilers. Two pumps were positioned at the bayou, one operational and one backup, driven by [[Fairbanks-Morse]] cold-start crude oil engines, each with two 9” pistons, adapted from ice house pumps that drove the refrigeration equipment, and capable of pumping 1100 to 1300 gallons per minute. Initial flooding of the fields took about a week, but water had to be continually pumped to keep the fields under water until the sprouts grew above the ground. At this point, pumping was stopped, and the levees were opened with shovels to drain the water to avoid injury to the young stalks. When the stalks became firm, the fields were again flooded until the rice was almost mature. Rice was planted in the spring (around [[Easter]]) and harvested in August. Growing time was about 150 days initially, but newer varieties of rice reduced this time to about 120 days. Irrigation and harvesting by hand required from 20 to 40 workers. Rice on its stalks was cut and assembled by hand into shocks, and allowed to dry in the field for 2 to 3 days. The harvested rice was threshed in the field by a tractor-powered [[Threshing machine|thresher]], then transported by wagon to the mill, a distance of up to several miles. At the mill the rice underwent the completion of the drying process by two to three passes in a forced air elevated chimney dryer, to reduce its moisture content from about 25% to 18% to facilitate the milling process.

== Mechanical farming ==
[[Image:International 1920 tractor.JPG|thumb|right|Early tractor of the type used for tilling and plowing]]
Mechanical equipment was introduced beginning in the 1930s. [[Tractor]]s first replaced mules for tilling and plowing. The first tractors used were [[International Harvester|McCormick-Deering/International Harvester]] models with steel wheels and requiring hand cranking for starting. Mechanical harvesting was not introduced until just prior to World War II, when a [[Reaper|McCormick Reaper]] was adapted for use with rice. Rice on its stalks was cut and bundled by the reaper, assembled by hand into shocks for the initial drying in the field. A mechanical thresher replaced the tractor-driven thresher, and the introduction of rice dryers allowed the rice to be dried at the field, reducing the risk of loss to birds and rain. In 1946, mechanical [[Combine harvester|combines]] were introduced that were capable of both reaping and threshing, removing the rice from the stalk in the field and allowing all of the drying process to take place with rice off the stalk in dryers at the mill.

== Milling ==
The mill built on St. Ann Street in 1912 was three stories high in order to take advantage of gravity in the flow of rice throughout its processing stages. Each of the stages involved processing steps on the rice as it was delivered from the third down to the second and first floors, then up again for the next stage. The first stage began when rice was brought by elevator into a storage bin on the third floor. The bin feed into the “stone”, two 5-foot-diameter stones separated enough to dehull the rice, then into the [[Rice huller|huller]] to remove the [[bran]], then the “brush” for polishing. This stage produced a highly polished rice that was popular during these years, but now has been reduced to only the first step, leaving the bran intact. From the “brush” the rice was fed into the “trumble”, applying a coating of sucrose (purchased in 55 gallon drums) and talcum powder (hence the instructions “wash before cooking”). After the “trumble”, the rice was moved to scales for weighing and packaging, originally in 100-pound sacks, and in later years bags of 10, 25 and 50 pounds.

[[Image:Engelberg-huller 1922.jpg|thumb|left|Engelberg Huller Company – Huller and Separator]]

The hullers were [[Engelberg Huller Company|Engelberg hullers]] manufactured in Germany. Before the use of [[electric motor]]s, the hullers were driven by [[Corliss steam engine|Corliss]] 150&nbsp;hp steam engines powered from [[Babcock & Wilcox]] boilers. An extensive network of belts and pulleys was used to transfer the power from the engines to the hullers. Electric motors were introduced in 1951, replacing the steam engines.

== Owners and Employees ==
PA Conrad's eldest son Philip Odell (PO) Conrad joined his father in the business following high school in 1920, and continued until his death in 1945. PA's second son Allen Conrad started working at the mill in 1925 following completion of boarding school at [[St. Paul's School (Covington, Louisiana)]], initially as a farm hand. PA's third son Julian worked as a farm hand during his school years, and joined the business following his graduation from the [http://old-new-orleans.com/NO_Soule.html Soule Business College] in New Orleans, assuming the role of [[Bookkeeping|bookkeeper]].  PA Conrad retired in 1940 to open an International Harvester dealership, turning the business over to his three sons, but continued as an informal consultant until his death in 1961. During WWII the business lost all of its full-time labor force. Since there was little mechanization at that time, family members and day laborers were used to keep the operation going.

The employee with the longest tenure was Leander “Gutchie” Viltz (son of Adam Viltz). Leander grew up as a child with Allen and Julian, began working at the mill at the age of six, and worked continuously for 72 years.

== Recent history ==
Farming rice was discontinued in 1968 when a portion of the farm land was sold to develop Caroline subdivision in [[New Iberia, Louisiana|New Iberia]] and other housing developments were established near the irrigation system. Milling, however, continued and does so until this day. During its management by the Conrad family, the mill produced only white rice which was sold to local grocery stores.

The mill was bought from the Conrad family in 1975 by Michael Davis following the retirement of Allen and Julian Conrad. Davis expanded the varieties of rice produced, including [[Brown rice|brown]] and flavored rice. The business also expanded into other food product lines including locally manufactured seasonings, spices, sauces, marinades and snacks, added the brands HOL GRAIN and QUIGGS, and expanded marketing throughout the US and Canada.

== Konriko Brand ==
An attempt to [[trademark]] the brand as “Conrico” ('''Con'''rad '''Ri'''ce '''Co'''mpany) was made in the 1950s, but was rejected by the [[United States Patent and Trademark Office|US Patent and Trademark Office]] due to similarity of a trademark by the California Rice Cooperative. A change to the name '''Konriko''' was accepted, and remains the brand by which it is known today.

== References ==
{{reflist}}

== External links ==
* [http://www.conradricemill.com/ Conrad Rice Mill] - official site
* [http://www.wafb.com/story/17656480/rice-mill WAFB News story] on the 100th anniversary

{{National Register of Historic Places}}

<!-- Just press the "Save page" button below without changing anything! Doing so will submit your article submission for review. Once you have saved this page you will find a new yellow 'Review waiting' box at the bottom of your submission page. If you have submitted your page previously, either the old pink 'Submission declined' template or the old grey 'Draft' template will still appear at the top of your submission page, but you should ignore it. Again, please don't change anything in this text box. Just press the "Save page" button below. -->

[[Category:New Iberia, Louisiana]]
[[Category:Buildings and structures in Iberia Parish, Louisiana]]
[[Category:Industrial buildings and structures on the National Register of Historic Places in Louisiana]]
[[Category:Companies based in Louisiana]]
[[Category:National Register of Historic Places in Iberia Parish, Louisiana]]