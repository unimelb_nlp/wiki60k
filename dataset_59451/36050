'''Tropical Storm Aletta''' was the first tropical cyclone of the [[2006 Pacific hurricane season]]. Aletta developed from an area of disturbed weather located south-southwest of the [[Mexico|Mexican]] port of [[Acapulco, Guerrero]]. It gradually gained organized convection and was classified as a tropical depression early on May&nbsp;27, and became a tropical storm later that morning, the first of 2006 in the [[Western Hemisphere]]. Aletta strengthened to a tropical storm with 45&nbsp;mph (75&nbsp;km/h) sustained winds, while moving towards the [[Guerrero]] coast in southwestern Mexico. The storm became stationary, though it later turned to the west and weakened on May&nbsp;29. Aletta continued to weaken until it dissipated on May&nbsp;31. The storm dropped moderate rainfall along the Mexican coast, and generated winds that downed trees and caused minor damage.

{{Infobox Hurricane
 | Name= Tropical Storm Aletta
 | Type= Tropical storm
 | Year= 2006
 | Basin=EPac
 | Formed=May 27, 2006
 | Dissipated=May 30, 2006
 | 1-min winds=40
 | Pressure=1002
 | Image location=Tropical Storm Aletta (2006).jpg
 | Image name=Tropical Storm Aletta
 | Damages=minimal
 | Fatalities=None
 | Areas= [[Mexico]]
 | Hurricane season= [[2006 Pacific hurricane season]]
}}

==Meteorological history==
{{storm path|Aletta 2006 track.png}}
On May&nbsp;21, 2006, a [[tropical wave]] crossed [[Central America]] and entered the [[Pacific Ocean|East Pacific]]. The wave drifted westward, and after several days began to interact with a low-level [[Trough (meteorology)|trough]] near the [[Gulf of Tehuantepec]], and as a result deep convection increased. By May&nbsp;25, a large [[Low pressure area|area of low pressure]] formed several hundred miles south of [[Acapulco, Mexico]]. [[Wind shear]] inhibited development initially, though conditions became slightly more favorable after a number of days, and at 0600&nbsp;[[Coordinated Universal Time|UTC]] on May&nbsp;27, a tropical depression formed about 190&nbsp;mi (305&nbsp;km) south of Acapulco.<ref name="tcr">{{Cite web|author=Richard J. Pasch|title=Tropical Storm Aletta Tropical Cyclone Report|year=2006|publisher=National Hurricane Center|accessdate=2008-09-02|url={{NHC TCR url|id=EP012006_Aletta}}}}</ref>

Moving very little, the depression was disorganized due to continued shear, leaving the center of circulation west of the convective activity.<ref>{{cite web|author=Avila|title=Tropical Depression One Discussion Number 1|year=2006|publisher=National Hurricane Center|accessdate=2008-09-02|url=http://www.nhc.noaa.gov/archive/2006/ep01/ep012006.discus.001.shtml?}}</ref> By later in the morning hours of May&nbsp;27, the system began to show overall signs of organization, primarily related to a burst of convection in the eastern semicircle. At the same time, the center of circulation either reformed or began to move farther north, possibly north-northeast. Embedded within the steering currents of a [[High pressure area|ridge]], most forecasts anticipated the storm would drift northward for several days, though some [[tropical cyclone forecasting|computer models]] predicted the storm would eventually move inland near Acapulco.<ref>{{cite web|author=Stewart|title=Tropical Depression One Discussion Number 2|year=2006|publisher=National Hurricane Center|accessdate=2008-09-02|url=http://www.nhc.noaa.gov/archive/2006/ep01/ep012006.discus.002.shtml?}}</ref> At 1800&nbsp;UTC on May&nbsp;27, the depression organized into a tropical storm, and as such was named ''Aletta'' by the [[National Hurricane Center]].<ref name="tcr"/>

With continued forecasting difficulty,<ref>{{cite web|author=Pasch|title=Tropical Storm Aletta Discussion Number 4|year=2006|publisher=National Hurricane Center|accessdate=2008-09-02|url=http://www.nhc.noaa.gov/archive/2006/ep01/ep012006.discus.004.shtml?}}</ref> a burst of new convection formed east of the center early on May&nbsp;28, as the storm began meandering southward.<ref>{{cite web|author=Stewart|title=Tropical Storm Aletta Discussion Number 5|year=2006|publisher=National Hurricane Center|accessdate=2008-09-02|url=http://www.nhc.noaa.gov/archive/2006/ep01/ep012006.discus.005.shtml?}}</ref> At 0600&nbsp;UTC, Aletta attained peak winds of {{convert|45|mph|km/h|abbr=on}} while nearly stationary in forward movement.<ref name="tcr"/> Later in the afternoon, the storm's center was obscured, thus subjecting the exact location to speculation. In addition, the circulation fluctuated in organization with the alternating intensity of the wind shear, and at the time maintained elongated cloud patterns, rather than the typical circulatory shape.<ref>{{cite web|author=Franklin|title=Tropical Storm Aletta Discussion Number 7|year=2006|publisher=National Hurricane Center|accessdate=2008-09-02|url=http://www.nhc.noaa.gov/archive/2006/ep01/ep012006.discus.007.shtml?}}</ref> As the storm drifted to the west and executed a small cyclonic loop by early on May&nbsp;29, increased shear and dry air became entrained in the system, causing it to weaken to a tropical depression at 1800&nbsp;UTC.<ref name="tcr"/> Little convection remained afterward,<ref>{{cite web|author=Pasch|title=Tropical Depression Aletta Discussion Number 12|year=2006|publisher=National Hurricane Center|accessdate=2008-09-02|url=http://www.nhc.noaa.gov/archive/2006/ep01/ep012006.discus.012.shtml?}}</ref> and the storm struggled for several days, becoming a remnant low by May&nbsp;31. The low dissipated shortly thereafter.<ref name="tcr"/>

==Preparations and impact==
When the storm began to move towards the coast, the Mexican government issued [[tropical storm watch]]es between [[Punta Maldonado]] and [[Zihuatanejo]].<ref name="SMN Aletta">{{cite web | author = Alberto Hernández Unzón | author2 = M. G. Cirilo Bravo Lujano | publisher = Servicio Meteorologico Nacional, Comisión Nacional del Agua | title = Resúmen de la Tormenta Tropical Aletta del Océano Pacífico | url = http://smn.cna.gob.mx/ciclones/tempo2006/pacifico/aletta/aletta.pdf | accessdate = 2006-09-09 | format = PDF | language=es|archiveurl = https://web.archive.org/web/20060921210808/http://smn.cna.gob.mx/ciclones/tempo2006/pacifico/aletta/aletta.pdf |archivedate = September 21, 2006}}</ref> By 1800&nbsp;UTC on May&nbsp;29, the advisory was discontinued after a series of modifications and extensions.<ref name="tcr"/>

Aletta produced moderate rainfall across Mexico, including a 24-hour rainfall total of 100.2&nbsp;mm (3.94&nbsp;inches) in Jacatepec, Oaxaca on May&nbsp;30, and 96.0&nbsp;mm (3.78&nbsp;inches) in La Calera, Guerrero, the next day.<ref name="SMN Aletta"/> High winds knocked down trees and caused minor structural damage. In [[Zihuatanejo]], a ship with nine people was rescued after being reported as lost, which may have been a result of high seas generated by Aletta.<ref>{{cite web|author=Staff Writer|title=Afecta tormenta Aletta a Guerrero|year=2006|publisher=El Siglo De Torreon|accessdate=2008-09-02|url=http://www.google.com/search?hl=en&sa=X&oi=spell&resnum=0&ct=result&cd=1&q=Tropical+Tormenta+Aletta&spell=1|language=Spanish}}</ref> However, there were no reports of fatalities associated with Aletta.<ref name="tcr"/>

==See also==
{{Portal|Tropical cyclones}}
*[[List of Pacific hurricanes]]
*[[Hurricane Aletta|Other tropical cyclones named Aletta]]
* [[Timeline of the 2006 Pacific hurricane season]]
* [[List of storms in the 2006 Pacific hurricane season]]

==References==
{{Reflist|2}}

{{2006 Pacific hurricane season buttons}}

{{Good article}}

{{DEFAULTSORT:Aletta (2006)}}
[[Category:2006 Pacific hurricane season|Aletta 2006]]
[[Category:Eastern Pacific tropical storms|Aletta 2006]]
[[Category:2006 in Mexico|Tropical Storm Aletta]]