{{about|the inlet in South Australia||Barker (disambiguation)}}
{{Use dmy dates|date=May 2014}}
{{Use Australian English|date=May 2014}}
{{Infobox body of water
|name = Barker Inlet
|image = barker Inlet location.png
|caption = The inlet in relation to [[Adelaide]]'s [[Central business district]]
|image_map = 
|image_bathymetry =
|caption_bathymetry =
|location = [[South Australia]]
|coords ={{coord|-34.745|138.50|type:city|format=dms|display=title}}
|rivers = Dry Creek
|oceans = 
|countries = {{AUS}}
|length =  
|width =  
|area =  
|depth =  
|max-depth =
|volume =
|shore =
|salinity =
|frozen =
|islands = 
|cities = [[Adelaide]]
|reference =
}}
The '''Barker Inlet''' is a tidal [[inlet]] of the [[Gulf St Vincent]] in [[Adelaide]], [[South Australia]], named after Captain [[Collet Barker]] who first sighted it in 1831. It contains one of the southernmost [[mangrove]] forests in the world, a [[bottlenose dolphin|dolphin]] sanctuary, [[seagrass]] meadows and is an important fish and [[shellfish]] breeding ground. The inlet separates [[Torrens Island]] and [[Garden Island (South Australia)|Garden Island]] from the mainland to the East and is characterized by a network of tidal creeks, artificially deepened channels, and wide [[mudflat]]s. The extensive belt of mangroves are bordered by [[Tecticornia|samphire]] saltmarsh flats and low-lying sand dunes, there are two boardwalks (at Garden Island and [[St Kilda, South Australia|St Kilda]]), and [[ships graveyard]]s in Broad Creek, Angas Inlet and the North Arm.

The inlet has been adversely impacted since the settlement of South Australia with [[stormwater]] and raw [[sewage]] discharge, fishing, [[landfill]] rubbish dumping, power generation and other activities adversely affecting its [[flora]] and [[fauna]]. Much of this has changed with the landfill dump on adjacent Garden Island closed in 2000 and remediation work begun.<ref>{{cite web|url=http://www.clw.csiro.au/staff/FitzpatrickR/barker_inlet_reports/Stage1ReportAppB.pdf |title=Literature Review of Acid Sulfate Soils and the environment in the Barker Inlet/ Gillman area |publisher=[[CSIRO]] Land and Water |last=Thomas |first=Brett |author2=Fitzpatrick R |author3=Merry R. |accessdate=2006-12-26 |date=July 2001 |deadurl=yes |archiveurl=https://web.archive.org/web/20060920171717/http://www.clw.csiro.au/staff/FitzpatrickR/barker_inlet_reports/Stage1ReportAppB.pdf |archivedate=20 September 2006 |df=dmy }}</ref> Some stormwater is now being filtered through [[wetland]]s before discharge and  the inlet has been declared a reserve for the preservation of dolphins, fish, crabs and aquatic plants. The mangroves and waterways are still affected by the adjacent [[Sea salt|salt crystallization pans]], hot [[wastewater]] discharge from [[Torrens Island Power Station, South Australia|Torrens Island power station]], heavy metal contamination from stormwater and treated sewage, and disturbances from boat traffic.

== Physical structure ==
Barker Inlet is a shallow tidal inlet which, with the adjacent [[Port River]] Estuary, formed during the [[Holocene]] by the [[Spit (landform)|progressive extension]] of the [[Lefevre Peninsula]] by northward [[Longshore drift|littoral drift]] of sand carried by wave action along the eastern shore of [[Gulf St Vincent]].<ref>Bowman, G. & Harvey, N. (1986): [http://www.jstor.org/pss/4297197 Geomorphic Evolution of a Holocene Beach-Ridge Complex, LeFevre Peninsula, South Australia.] ''Journal of Coastal Research'' 2(3):345-362</ref>

It has a narrow central channel used for [[boating]]. Spring [[tide]]s are over 2½ metres and at low tide much of the inlet is mudflats that are above water level. Most of the [[Creek (tidal)|creek]]s through the mangroves drain surrounding land and are not [[Navigability|navigable]] except at high tide by very small boats. There is an artificial channel, running along the side of a [[Breakwater (structure)|breakwater]], from a boat ramp at St Kilda near the inlet's north end. The coast side of the mangroves are bounded by extensive salt evaporation ponds leased for industrial usage by the [[South Australian Government]].

Most of the creeks on the eastern side are tidal although ''Swan Alley creek'' is the outlet for the [[Dry Creek (South Australia)|Dry Creek]] and the [[Little Para River]], and the ''North Arm Creek'' for the Barker Inlet Wetlands. The wetlands were created in 1994 as part of a stormwater treatment system with both tidal and freshwater sections. There is 1.72&nbsp;km<sup>2</sup> of constructed wetlands holding 1.2 [[litre|Gigalitres]] of stormwater before discharging via the creek.<ref>{{cite web|url=http://www.deh.gov.au/minister/env/2001/mr21sep01.html |title=A CLEANER FUTURE FOR THE DOLPHINS OF BARKER INLET (media release) |publisher=Department of the Environment and Heritage |accessdate=2006-12-26 |date=21 September 2001 |archiveurl=https://web.archive.org/web/20060910074337/http://www.deh.gov.au/minister/env/2001/mr21sep01.html |archivedate=10 September 2006 |deadurl=yes |df=dmy }}</ref>

== Flora and fauna ==

===Flora===
The grey mangroves are uniformly of the type ''[[Avicennia marina]]'' var. ''resinifera'' and cover most of the pre-settlement area, but the surrounding samphire salt flats have been greatly reduced in size by changes in the landform with ''[[Tecticornia]] flabelliformis'' now listed as threatened in the area.<ref>{{cite web|url=http://www.bipec.on.net/ecology.html |title=Ecology |publisher=Barker Inlet Port Estuary Committee |last=Edyvane |first=K |year=2000 |accessdate=2006-12-26 |deadurl=yes |archiveurl=https://web.archive.org/web/20060825112510/http://www.bipec.on.net/ecology.html |archivedate=25 August 2006 }}</ref> The inlet's deeper sections are dominated by strap or tape weed (''[[Posidonia]] spp.''). Eelgrass (''[[Zostera]] muelleri'') and garweed (''[[Heterozostera]] tasmanica'') dominate the shallows, often being exposed on mudflats at low tide.<ref name="Portbook">{{cite book | title=The Port River | publisher = City of Port Adelaide Enfield | isbn=0-646-40920-4 | year=2001 | pages = 4, 44}}</ref>

===Fauna===
Over 70 species of fish have been recorded, along with over 110 of crustaceans and almost 50 of molluscs including species such as [[Melicertus|western king prawns]], [[King George whiting|King George]] and [[yellowfin whiting]] and [[Portunus pelagicus|blue swimmer crabs]].<ref name="Portbook" />  
Many bird species use the inlet including [[cormorant]]s, [[tern]]s, [[duck]]s, [[swan]]s, [[pelican]]s, [[egret]]s and [[heron]]s, as well as [[silver gull]]s and [[white-bellied sea eagle]]s. Including migratory birds, over 250 species have been recorded in the inlet, surrounding wetlands and lagoons.<ref name=bli>{{cite web|title=Important Bird Areas factsheet: Gulf St Vincent|url=http://www.birdlife.org/datazone/sitefactsheet.php?id=25116|publisher=BirdLife International|accessdate=21 October 2014|date=2014}}</ref>
[[File:Barker inlet with pelicans.JPG|thumb|centre|600px|[[Pelican]]s on [[mudflat]]s, Barker Inlet]]
{{clear}}

== Former uses ==
From 1906 until 1972, the inlet's ''Broad Creek'' was used as a landing point for [[Explosive material|explosives]] that were then transported by a {{convert|2.4|km|miles|abbr=on}} [[tram]]way to the [[Dry Creek explosives depot]]. There are abandoned ships in ''Broad Creek'', Angas Inlet and the North Arm of the [[Port River]]. The remains of over 30 [[iron]] and wooden ships abandoned up until 1945 are now bird roosts and a canoeing attraction.<ref>{{cite web | url = http://www.environment.sa.gov.au/heritage/ships_graveyards/pt_adelaide.html|title =Port Adelaide Ships' Graveyards |  accessdate=2006-12-26| publisher=South Australian Department of Environment and Heritage}}</ref>

==Protected areas and other designations==

===Reserves declared by the South Australian government===
The Barker Inlet is associated with the following [[protected area]]s - the [[Protected areas of South Australia#Adelaide Dolphin Sanctuary|Adelaide Dolphin Sanctuary]], the [[Barker Inlet-St Kilda Aquatic Reserve]], the southern part of the [[St Kilda – Chapman Creek Aquatic Reserve]] and the [[Torrens Island Conservation Park]].<ref name=LMV>{{cite web|title=Search result for "Barker Inlet-St Kilda Aquatic Reserve" with the following databases selected - "Suburbs and Localities", "Aquatic Reserves", “Dolphin Sanctuary” and “NPW and Conservation Reserve Boundaries”, “Coastline MHWM” and "Metropolitan Adelaide Boundary (Development Act 1993)"|url=http://location.sa.gov.au/viewer/|work=Location SA Map Viewer |publisher=Government of South Australia |accessdate=15 October 2016}}</ref>
===Non-statutory arrangements===
The Baker Inlet is located both within a [[A Directory of Important Wetlands in Australia|nationally recognised wetland system]] known as 'Barker Inlet & St Kilda' and at the southern extent of an [[Important Bird Area]] (IBA) known as the [[Gulf St Vincent Important Bird Area]].<ref name=DIWA>{{cite web|title=Search result for "Barker Inlet & St Kilda - SA005"  |url=http://www.environment.gov.au/cgi-bin/wetlands/report.pl |work=Directory of Important Wetlands |publisher=Department of Environment and Energy, Government of Australia |accessdate=15 October 2016}}</ref><ref>{{cite book|last1=Dutson|first1=Guy|author2=Garnett, Stephen|author3=Gole, Cheryl|title=Australia’s Important Bird Areas, Key sites for bird conservation|date=October 2009|publisher=Birds Australia|pages=33|url=http://www.birdlife.org.au/documents/OTHPUB-IBA-supp.pdf|accessdate=5 October 2014}}</ref>

==See also==
* [[Adelaide International Bird Sanctuary]]

== References ==
{{Reflist}}

== External links ==
* [https://web.archive.org/web/20061206113254/http://www.bipec.on.net/ BARKER INLET PORT ESTUARY COMMITTEE]

{{Bays of South Australia|state=autocollapse}}

{{DEFAULTSORT:Barker Inlet}}
[[Category:Inlets of Australia]]
[[Category:Rivers of Adelaide]]
[[Category:Gulf St Vincent]]