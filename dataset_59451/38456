{{good article}}
{{Infobox baseball biography
|name=Joe Borden
|image=Joe Borden.jpg
|position=[[Pitcher]]
|bats=Right
|throws=Right
|birth_date={{Birth date|1854|5|9}}
|birth_place=[[Jacobstown, New Jersey|Jacobstown]] in [[North Hanover Township, New Jersey]]
|death_date={{death date and age|1929|10|14|1854|5|9}}
|death_place=[[Yeadon, Pennsylvania]]
|debutleague = MLB
|debutdate=July 24
|debutyear=1875
|debutteam=Philadelphia White Stockings
|finalleague = MLB
|finaldate=July 15
|finalyear=1876
|finalteam=Boston Red Caps
|statleague = MLB
|stat1label=[[Win–loss record (pitching)|Win–loss record]]
|stat1value=13–16
|stat2label=[[Earned run average]]
|stat2value=2.60
|stat3label=[[Strikeout]]s
|stat3value=41
|teams=
*[[Philadelphia White Stockings]] ({{baseball year|1875}})
*[[Boston Red Caps]] ({{baseball year|1876}})
|highlights=
*July 28, 1875: First pitcher to throw a no-hitter in a professional game.
*April 22, 1876: Winning pitcher in the first [[National League]] game.
}}
'''Joseph Emley Borden''', aka ''Joe Josephs'', (May&nbsp;9, 1854 – October&nbsp;14, 1929), nicknamed "Josephus the Phenomenal", was a [[starting pitcher]] in professional baseball for two seasons.  Born in the [[Jacobstown, New Jersey|Jacobstown]] section of [[North Hanover Township, New Jersey]], he was playing for a Philadelphia amateur team when he was discovered by the [[Philadelphia White Stockings]] of the [[National Association of Professional Base Ball Players|National Association]]&nbsp;(NA) in {{baseball year|1875}}.  The White Stockings needed a replacement for a recently released pitcher, and were awaiting the arrival of a replacement.  During his short, seven-game stint with the team, he posted a 2–4&nbsp;[[win–loss record (pitching)|win–loss record]], both victories recorded as [[shutout]]s.  On July&nbsp;28 of that season, he threw the first [[no-hitter]] in professional baseball history.

When the NA folded after the 1875&nbsp;season, Borden signed a three-year contract with the [[Boston Red Caps]].  On April&nbsp;22, 1876, Borden and the Red Caps were victorious in the first [[National League]]&nbsp;(NL) game ever played.  Later that season, on May&nbsp;23, he pitched a shutout, which some historians claim was the first no-hitter in [[Major League Baseball]].  Known for having an eccentric personality, he played under different surnames, such as Josephs and Nedrob, so as to disguise his involvement in baseball; his prominent family would have disapproved had they known.  After he was released from the Red Caps as a player during the first season of his contract, he worked for a short period of time as their groundskeeper until he and the owner agreed to a buyout of the remainder of his contract. It was mistakenly claimed that he died in1889, in the [[Johnstown Flood]]. His official death date is recognized as occurring in 1929 when he was 75&nbsp;years of age.

==Early life==
Joseph Emley Borden was born on May&nbsp;9, 1854 in [[Jacobstown, New Jersey|Jacobstown]] in [[North Hanover Township, New Jersey]] into a wealthy family.<ref name="retroborden">{{cite web|title=Joe Borden|url=http://www.retrosheet.org/boxesetc/B/Pbordj101.htm|work=retrosheet.org|publisher=Retrosheet, Inc|accessdate=June 23, 2010}}</ref><ref name="cook42">Cook, p. 42</ref> The fourth of John H. and Sarah Ann (Emley) Borden's six children, his father was a shoe manufacturer. Borden moved to Philadelphia in 1870. He joined the J.B. Doerr club by 1875, an amateur baseball club that played several teams around Philadelphia.<ref name="WCU">{{cite web|title=Joe Borden (Early Baseball Player)|url=http://digitalcommons.wcupa.edu/cgi/viewcontent.cgi?article=1050&context=hist_wchest|work=[[West Chester University]]|last=Weatherby|first=Charlie|year=2015|accessdate=August 12, 2016}}</ref> It is claimed that his family would have been embarrassed that their son was playing baseball for money, and would have disapproved.<ref name="cook42"/><ref name="nemeczeman7">Nemec, Zeman, p. 7</ref>  To hide his playing career, he assumed several various last names, such as Josephs and Nedrob, which is Borden spelled backwards.<ref name="cook42"/>

==Career==

===1875 season===
The [[Philadelphia White Stockings]], of the NA, had recently dismissed pitcher [[Cherokee Fisher]] in July 1875 due to his clashing with team [[manager (baseball)|captain]], [[Mike McGeary]], and were in need of a replacement.  The White Stockings signed [[George Zettlein]], but there was a delay in his arrival.<ref name="nemeczeman7"/>  The team then signed Borden, who was a local Philadelphia amateur player, as a replacement until Zettlein arrived.<ref name="nemeczeman7"/>  His first appearance in a game for the White Stockings was on July&nbsp;24, an 11–4&nbsp;loss to the [[Dick McBride]] and the [[Philadelphia Athletics (1860–76)|Philadelphia Athletics]].<ref name="nemeczeman7"/>

In his next start, on July&nbsp;28 against the [[Chicago White Stockings (1870–89)|Chicago White Stockings]], he pitched the first recorded [[no-hitter]] in professional baseball history.<ref name="nemeczeman7"/><ref name="no-hitterchrono">{{cite web|title=No Hitters Chronologically|url=http://www.retrosheet.org/nohit_chrono.htm|work=retrosheet.org|publisher=Retrosheet, Inc|accessdate=June 23, 2010}}</ref>  Borden lost the next three games he started before defeating the [[St. Louis Brown Stockings]], and future [[National Baseball Hall of Fame and Museum|Hall of Fame]] pitcher [[Pud Galvin]], 16–0 on August&nbsp;9.<ref name="nemeczeman7"/>  By this time, Zettlein had settled in as their regular pitcher, and Borden was no longer needed.<ref name="nemeczeman7"/>  During his time with the White Stockings, he pitched in seven [[games pitched|games]], all of which he [[games started|started]] and [[complete game|completed]].<ref name="retroborden"/>  He finished the season with a 2–4&nbsp;[[win–loss record (pitching)|win–loss record]], two [[Shutouts in baseball|shutouts]], a 1.50&nbsp;[[earned run average]] in 66&nbsp;[[innings pitched]].<ref name="retroborden"/>

===1876 season===
After the 1875&nbsp;season concluded, the league, and subsequently the Philadelphia White Stockings, folded, allowing the NL to form, becoming the first "Major" league.<ref name="baseballlibrary1876">{{cite web|title=1876|work=baseballlibrary.com|publisher=The Idea Logical Company, Inc|url=http://www.baseballlibrary.com/chronology/byyear.php?year=1876|accessdate=June 23, 2010}}</ref>  Before the season, Borden signed a three-year contract worth $2000&nbsp;a season (${{formatnum:{{Inflation|US|2000|1876|r=-1}}}} current dollar adjustment) with the [[Boston Red Caps]] of the NL.<ref name="macdonald95">Macdonald, p. 95</ref>  The club had high expectations that he could adequately replace [[Albert Spalding]] who had recently departed for the Chicago White Stockings.<ref name="macdonald95"/>  Sportswriters were in agreement with the club and dubbed him "Josephus the Phenomenal".  Hall of Fame baseball writer [[Henry Chadwick (writer)|Henry Chadwick]] described Borden's pitching style as having speed, but with little strategy.<ref name="macdonald74">Macdonald, p. 74</ref>  In addition to his swiftly moving [[fastball]], he also delivered a [[curveball]] that moved down and away from right-handed batters; both pitches he delivered from a low arm angle.<ref name="macdonald95"/>  His Red Caps played the Philadelphia Athletics in the first game in NL history on April&nbsp;22; the only game of the day due to rain cancelling the rest of the league's schedule.  The Red Caps defeated the Athletics with two [[run (baseball)|runs]] in the ninth inning, and a final score of 6–5 at [[Jefferson Street Grounds|Athletic Park]], with Borden pitching the complete game for the victory.<ref name="baseballlibrary1876"/>

On May&nbsp;23, Borden pitched a two-hit 8–0&nbsp;shutout victory against the [[Cincinnati Reds (1876–1880)|Cincinnati Reds]]. Various historians claim that this performance was, instead, the first no-hitter thrown in the NL, thus the first in major league history.<ref name="cook50">Cook, p. 50</ref>  According to [[Lee Allen (baseball)|Lee Allen]], baseball historian and writer, the scorecard shows that Borden gave up two [[base on balls|bases on balls]], which were counted as [[hit (baseball)|hits]] in the final score.<ref name="cook50"/>  The official scorekeeper for the game was [[O. P. Caylor]], a writer for ''[[The Cincinnati Enquirer]]'', who believed, unlike his contemporaries, that bases on balls should be scored as hits.<ref name="cook50"/>  Bases on balls were only officially counted as hits for the {{baseball year|1887}} season.<ref name="cook50"/>  ''The Boston Daily Globe'' reported on the game the following day, and noted that Reds had had two clean one-base hits during the game.<ref name="cook52">Cook, p. 52</ref> Furthermore, on June&nbsp;6, the ''[[New York Clipper]]'' did not report an occurrence of a no-hitter in their summary of the game, though the [[box score]] did have differing stats in other categories than what the ''Daily Globe'' had reported.<ref name="cook52"/>  Consequently, the first official no-hitter in Major League history was pitched by [[George Bradley]], of the [[St. Louis Brown Stockings]] on July&nbsp;15, 1876.<ref name="no-hitterchrono"/>

Two days after his near no-hitter, on May&nbsp;25, Borden and the Red Caps faced the Reds again; this time against Cherokee Fisher, resulting in a scoreless game through nine innings.<ref name="cook52"/>  The Red Caps scored four runs in tenth inning for a 4–0 victory.<ref name="cook52"/>  Unfortunately for Borden, his pitching effectiveness declined rapidly after this, and at one point he reportedly lost his temper during a game in response to his own ineffectiveness, admonishing his teammates, even the well-liked and good-natured future Hall of famer [[George Wright (sportsman)|George Wright]].<ref name="macdonald120">Macdonald, p. 120</ref>  Due to his erratic pitching and behavior, he was released from the team by August.<ref name="cook52"/><ref name="macdonald120"/>  In 29&nbsp;games pitched during the 1876&nbsp;season, he started 24, completed 16, and recorded two shutouts.  Additionally, he had an 11–12&nbsp;[[win–loss record (pitching)|win–loss record]], a 2.89&nbsp;[[earned run average]] in 218⅓&nbsp;[[innings pitched]], and led the league in allowing 51&nbsp;bases on balls.<ref name="retroborden"/>

==Post-baseball career==
Though the Red Caps had released him as a player, he was still under contract, so they had him work in various other capacities, such as a ticket-taker and [[groundskeeper]].<ref name="cook52"/>  Eventually, team owner [[Nicholas Apollonio]] agreed to pay Borden approximately three-quarters of the two remaining years of his contract, and released him from the team.<ref name="connor7">Connor, p. 7</ref><ref name="purdy38">Purdy, p. 38</ref>  Borden reportedly found work stitching baseballs in [[Philadelphia]].<ref name="cook52"/> He later moved to [[West Chester, Pennsylvania]] and found work making shoes and boots. During the summer of 1883, he joined the semiprofessional Brandywine Base Ball Club, where he pitched and hit 1-for-11 in two victories.<ref name="WCU"/>

It was mistakenly claimed that he died in May 1889 during the [[Johnstown Flood]], the same disaster that had stranded the entire [[Louisville Colonels]] team.<ref name="cook166">Cook, p. 166</ref> The [[Sporting Life (American newspaper)|Sporting Life]] magazine corrected the error in its  June 19 issue, stating he was alive and living in Philadelphia. On February 7, 1891, Borden married Henrietta S. Evans, the daughter of publisher [[Henry S. Evans]]. The couple had two children, Richard, who died as a baby, and Lavinia. Borden worked as a banker with the Guarantee Trust and Safe Deposit Company and the Philadelphia representative for the U.S. Shipping Board. He was an amateur boxer and trained at Philadelphia Boxing Academy. He also kept hunting dogs and won exhibitions.<ref name="WCU"/>  Borden died in [[Yeadon, Pennsylvania]] at the age of 75, and is interred at [[Oaklands Cemetery]] in [[West Chester, Pennsylvania]].<ref name="retroborden"/><ref name=oaklandscemetery>{{cite web|title=Joseph Emley Borden (9 May 1854 – 14 Oct 1929)|url=http://www.oaklandscemetery.org/stop17.htm|work=oaklandscemetery.org|publisher=The Oaklands Cemetery|accessdate=January 6, 2012}}</ref>

==References==
;General
*{{cite book|last=Connor|first=Floyd|title=Baseball's Most Wanted: The Top 10 Book of the National Pastime's Outrageous Offenders, Lucky Bounces, and Other Oddities|year=2006|publisher=Sterling Publishing Company, Inc|isbn=1-57866-157-9|url=https://books.google.com/books?id=-CBQRtOgxacC&pg=PA5&lpg=PA5&dq=%22Joe+Borden%22+baseball&source=bl&ots=IztIctg_kh&sig=RwjX8nmn9Qibm2fsXazVJ-VYLOU&hl=en&ei=350jTK2rB4P_8AbTm7STBQ&sa=X&oi=book_result&ct=result&resnum=2&ved=0CBgQ6AEwAThG#v=onepage&q=%22Joe%20Borden%22%20baseball&f=false}}
*{{cite book|last=Cook|first=William A.|title=The Louisville Grays scandal of 1877: the taint of gambling at the dawn of the National League|year=2005|publisher=McFarland|isbn=0-7864-2179-7|url=https://books.google.com/books?id=AIJ1rvPxOEcC&pg=PA42&lpg=PA42&dq=%22Joe+Borden%22+no-hitter&source=bl&ots=oQe5_SqmGS&sig=XF_krAQd0oTgutlvUSkkvZ0ZKUQ&hl=en&ei=qD0iTPyfEML-8Ab4hcydDg&sa=X&oi=book_result&ct=result&resnum=2&ved=0CBQQ6AEwATgK#v=onepage&q=%22Joe%20Borden%22%20no-hitter&f=false}}
*{{cite book|last=Macdonald|first=Neil W.|title=The league that lasted: 1876 and the founding of the National League of Professional Base Ball Clubs|year=2004|publisher=McFarland|isbn=0-7864-1755-2|url=https://books.google.com/books?id=kUTWjHzAiwkC&pg=PA111&lpg=PA111&dq=%22Joe+Borden%22+no-hitter&source=bl&ots=RjNUtsO697&sig=qlRYQ2v4o5klpxzdZnMKdFGjztI&hl=en&ei=D1QiTJjbBIH78Ab01_ycDg&sa=X&oi=book_result&ct=result&resnum=7&ved=0CCEQ6AEwBjgK#v=onepage&q=Borden&f=false}}
*{{cite book|author1=Nemec, David |author2=Dave Zeman |title=The baseball rookies encyclopedia|year=2004|publisher=Brassey's|isbn=1-57488-670-3|url=https://books.google.com/books?id=5gm9KgshyAAC&pg=PA7&lpg=PA7&dq=%22Joe+Borden%22+baseball&source=bl&ots=w5uqCEUpqQ&sig=Zie07GBrDbnGuFnuA8MNp53cALc&hl=en&ei=05AjTK3YL4K78gbQ_-nDBQ&sa=X&oi=book_result&ct=result&resnum=2&ved=0CBkQ6AEwATge#v=onepage&q=%22Joe%20Borden%22%20baseball&f=false}}
*{{cite book|last=Purdy|first=Dennis|title=Kiss 'em Goodbye: An ESPN Treasury of Failed, Forgotten, and Departed Teams|year=2010|publisher=Random House, Inc|isbn=0-345-52012-2|url=https://books.google.com/books?id=2GZ_IBNP3oMC&pg=PA38&lpg=PA38&dq=%22Nathaniel+Apollonio%22&source=bl&ots=rBedKtYzBj&sig=aL-vypLHB9r1IcFqrIeiGa88XwM&hl=en&ei=gqMjTNnQJsP78AaAwYS0BQ&sa=X&oi=book_result&ct=result&resnum=1&ved=0CBIQ6AEwAA#v=onepage&q=%22Nathaniel%20Apollonio%22&f=false}}
;Specific
{{Reflist|30em}}

==External links==
{{Baseballstats|br=b/bordejo01|fangraphs=1001198|brm=borden001joe|mlb=111231|espn=19377}}
* {{findagrave|10711108}}

{{s-start}}
{{S-ach|ach}}
{{succession box |title=[[List of Major League Baseball no-hitters|No-hitter pitcher]] | before=''None'' |years=July 28, 1875 |after=[[George Bradley]]}}
{{s-end}}

{{DEFAULTSORT:Borden, Joe}}
[[Category:1854 births]]
[[Category:1929 deaths]]
[[Category:Philadelphia White Stockings players]]
[[Category:Boston Red Caps players]]
[[Category:19th-century baseball players]]
[[Category:Major League Baseball pitchers]]
[[Category:Baseball players from New Jersey]]
[[Category:Sportspeople from Burlington County, New Jersey]]