<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=R.35
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=Airliner
 | national origin=Belgium
 | manufacturer=[[Constructions Aéronautiques G. Renard|Renard]]
 | designer=Alfred Renard
 | first flight=1 April 1938
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=1
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Renard R.35''' was a prototype [[Cabin pressurization|pressurised]] [[airliner]] of the 1930s built by the [[Belgium|Belgian]] aircraft manufacturer ''[[Constructions Aéronautiques G. Renard]]''.  A [[Trimotor|three-engined]] low-winged [[monoplane]] with retractable undercarriage, the R.35 was destroyed in a crash on its first flight.

==Development and design==

In 1935, Alfred Renard, chief designer and co-founder of the Renard company, started design of a pressurised airliner for use by [[SABENA]] on its routes to the [[Belgian Congo]].  Renard designed a low-winged monoplane of all-metal construction, powered by three engines as required by SABENA, and received an order for a single prototype on 3 April 1936.<ref name="De Wulf p147-8">De Wulf 1978, pp. 147-148.</ref>  The R.35 had a circular section fuselage, housing a pressurised cabin which accommodated twenty passengers and a flight crew of three. It was intended to be powered by a range of radial engines with the 950&nbsp;hp (709&nbsp;kW) [[Gnome-Rhône Mistral Major|Gnome-Rhône 14K]] preferred by Renard, but the prototype was fitted with 750&nbsp;hp (560&nbsp;kW) [[Gnome-Rhône 9K]] engines.<ref name="De Wulf p148">De Wulf 1978, p.148.</ref><ref name="aerostories p2"/>

The R.35 was completed early in 1938.<ref name="De Wulf p148"/> On 1 April 1938, it was planned to carry out high-speed taxiing trials at [[Evere]] airfield in front of an audience of visiting dignitaries and journalists. After carrying out a single taxi-run, however, the R.35 took off during a second run, and while attempting a [[Airfield traffic pattern|circuit]] to return to the runway, the R.35 dived into the ground and was destroyed, killing the pilot Georges Van Damme. The cause of the crash was unknown.<ref name="De Wulf p147">De Wulf 1978, p.147.</ref><ref name="aerostories p1">Hauet, André. "[http://aerostories.free.fr/appareils/renard/index.html Renard R.35 Un avion stratosphérique belge en 1938. (histoire)]". ''Aérostories''. (In French). Retrieved 26 August 2009.</ref><ref>"[http://aviation-safety.net/wikibase/wiki.php?id=17301 ASN Aircraft accident 01-APR-1938 Renard R.35 OO-ARM]". ''Aviation Safety Network''. 15 April 2009. Retrieved 26 August 2009.</ref> Following this crash, SABENA abandoned its interest in the R.35, and Renard abandoned development.<ref name="De Wulf p148"/>

==Variants==
;R.35B
: Proposed bomber version, capable of carrying 2,800 kg (6,200 lb) of bombs. Unbuilt.<ref name="De Wulf p148"/>

==Specifications (performance estimated) ==

{{aircraft specifications

|plane or copter?=plane<!-- options: plane/copter -->
|jet or prop?=prop<!-- options: jet/prop/both/neither -->

|ref=A Belgian Rare Avis <ref name="De Wulf p149">De Wulf 1978, p.149.</ref>

|crew=3 (2 pilots and radio operator
|capacity= 20 passengers<!-- the number of passengers carried in the case of a commercial aircraft-->
|payload main=2,000 kg
|payload alt=4,400 lb
|payload more=
|length main= 17.50 m
|length alt=57 ft 5 in
|span main=25.50 m
|span alt=83 ft 8 in
|height main=5.50 m
|height alt=18 ft 1 in
|area main= 87 m² <ref name="aerostories p2">Hauet, André. "[http://aerostories.free.fr/appareils/renard/page2.html Renard R.35 Un avion stratosphérique belge en 1938. (technique)]". ''Aérostories''. (In French). Retrieved 26 August 2009.</ref>
|area alt= 936 sq ft
|airfoil=
|empty weight main= 6,100 kg
|empty weight alt=13,400 lb 
|loaded weight main= 
|loaded weight alt= 
|useful load main= 
|useful load alt= 
|max takeoff weight main= 10,500 kg
|max takeoff weight alt= 23,100 lb
|max takeoff weight more=
|more general=

|engine (prop)=[[Gnome-Rhône 9K]]
|type of prop= 9-cylinder air-cooled [[radial engine]] 
|number of props=3<!--  ditto number of engines-->
|power main= 560 kW
|power alt=750 hp
|power original=
|power more=

|max speed main= 435 km/h
|max speed alt= 235 knots, 270 mph
|max speed more= at 5,000 m (16,400 ft)
|cruise speed main= 350 km/h
|cruise speed alt= 190 knots, 218 mph
|cruise speed more= 
|stall speed main= 
|stall speed alt= 
|stall speed more=
|never exceed speed main= 
|never exceed speed alt= 
|range main= 1,800 km
|range alt=974 [[nautical mile|nmi]], 1,120 mi
|ferry range main=
|ferry range alt=
|ferry range more=
|ceiling main= 9,000 m
|ceiling alt= 29,500 ft
|climb rate main= 
|climb rate alt= 
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=
|power/mass alt=
|more performance=

|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==

===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
*De Wulf, Herman. "A Belgian Rare Avis". ''[[Air International]]'', September 1978, Vol 15 No. 3. Bromley, UK: Fine Scroll. pp.&nbsp;147–149.
{{refend}}

==External links==
*[http://www.baha.be/Webpages/Navigator/Photos/CivilPics/civil_pics_ooaaa_ooczz/RenardR35.htm photo]
*[http://www.fnar.be/html/R35/R-35.htm RENARD R-35]

{{Renard aircraft}}

[[Category:Belgian airliners 1930–1939]]
[[Category:Renard aircraft|R.35]]
[[Category:Trimotors]]
[[Category:Low-wing aircraft]]