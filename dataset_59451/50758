{{Infobox journal
| title = Brain 
| cover = 
| editor = [[Dimitri Kullmann]]
| discipline = [[Neurology]], [[neuroscience]]
| abbreviation = Brain
| publisher = [[Oxford University Press]]
| country =
| frequency = Monthly
| history = 1878–present
| openaccess = 
| license = 
| impact = 10.103
| impact-year = 2015
| website = http://brain.oxfordjournals.org/
| link1 = http://brain.oxfordjournals.org/content/current
| link1-name = Online access
| link2 = http://brain.oxfordjournals.org/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 1536984
| LCCN = 66084758
| CODEN = BRAIAK
| ISSN = 0006-8950
| eISSN = 1460-2156
}}
'''''Brain''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] of [[neurology]], founded in 1878 by [[John Charles Bucknill]], [[David Ferrier]], [[James Crichton-Browne]] and [[John Hughlings Jackson]]. It is currently published by [[Oxford University Press]]. Its full name is "Brain: a Journal of Neurology".

It was edited by [[John Newsom-Davis]] from 1997 to 2004. Under his editorship it became one of the first scientific journals to go online.<ref>{{cite journal
 | last = Compston
 | first = Alastair 
 | date = August 2004
 | title = Editorial
 | journal = Brain
 | volume = 127
 | issue = 8
 | pages = 1689–1690,
 | doi = 10.1093/brain/awh240 
 | url = http://brain.oxfordjournals.org/cgi/content/full/127/8/1689
 | accessdate = 2007-09-19
}}
</ref> From 2004 to 2013 the journal was edited by [[Alastair Compston]] ([[Cambridge University]]). The current editor is [[Dimitri Kullmann]] ([[University College London]]).

According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 10.103.<ref name=WoS>{{cite book |year=2015 |chapter=Brain |title=2015 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://brain.oxfordjournals.org/}}
* https://www.twitter.com/Brain1878

{{DEFAULTSORT:Brain (Journal)}}
[[Category:Publications established in 1878]]
[[Category:Oxford University Press academic journals]]
[[Category:Neurology journals]]
[[Category:Neuroscience journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]