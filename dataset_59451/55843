{{for|the later journal with a similar title|Bell Labs Technical Journal}}
{{Infobox journal
| title         = Bell System Technical Journal
| cover         = 
| editor        = 
| discipline    = 
| peer-reviewed = 
| language      = 
| former_names  = 
| abbreviation  = Bell Syst. Tech. J.
| publisher     = American Telephone and Telegraph Company
| country       = 
| frequency     = Quarterly (1922-1951), Monthly (1952-1995)
| history       = 1922-1995
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       = http://www.alcatel-lucent.com/bstj/
| link1 = http://ieeexplore.ieee.org/xpl/aboutJournal.jsp?punumber=6731005
| link1-name = Online archive
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 6313803
| LCCN          = 29029519 
| CODEN         = BSTJAN
| ISSN          = 0005-8580
| eISSN         = 
| boxwidth      = 
}}

The '''Bell System Technical Journal''' was a periodical publication by the [[American Telephone and Telegraph Company]] (AT&T) in New York devoted to the scientific and engineering aspects of electrical communication.<ref name=issue1>''Bell System Technical Journal'', Vol. 1, No. 1, p.1.</ref> It was published under this name from 1922 until 1983, when the [[breakup of the Bell System]] placed various parts of the system into separate companies. However, publication continued under different names until 1995.

==History==
The Bell System Technical Journal was published by AT&T in New York City through its Information Department, in behalf of [[Western Electric Company]] and the Associated Companies of the Bell System.<ref name=issue1 /> The first issue was released in July 1922, under the editorship of R.W. King and an eight-member editorial board. From 1922 to 1951, the publication schedule was quarterly. It was bimonthly until 1964, and finally produced ten monthly issues per year until the end of 1983, combining the four summer months into two issues in May and July.

Publication of the journal under the name ''Bell System Technical Journal'' ended with Volume 62 by the end of 1983, because of the divestiture of AT&T. Under new organization, publication continued as ''AT&T Bell Laboratories Technical Journal'' in 1984 with Volume 63, maintaining the volume sequence numbers established since 1922. In 1985, ''Bell Laboratories'' was removed from the title to ''AT&T Technical Journal'' until 1995 (Volume 74).

In 1996, the title was changed to ''[[Bell Labs Technical Journal]]'', and publication management was transferred to Wiley Periodicals, Inc., establishing a new volume sequence (Volume 1).

==Notable papers==
* In 1928, [[Clinton Joseph Davisson]] published a paper on electron diffraction by nickel crystal, thus unambiguously establishing the wave nature of electron.<ref>{{cite journal|last=Davisson|first=Clinton J.|title=The Diffraction of Electrons by a Crystal of Nickel|journal=Bell System Technical Journal|date=January 1928|pages=90–105, |url=http://www.alcatel-lucent.com/bstj/vol07-1928/articles/bstj7-1-90.pdf|accessdate=2012-03-31}}</ref> This discovery led to a widespread acceptance of particle-wave duality of matter and won him the 1937 Nobel Prize in Physics.
* [[Claude Shannon]]'s paper "[[A Mathematical Theory of Communication]]", which founded the field of [[information theory]], was published as two-part article in July and October issue of 1948.<ref>{{cite journal|last=Shannon|first=Claude E.|title=A mathematical theory of communication|journal=Bell System Technical Journal|date=July 1948|pages=27:379–423|url=http://cm.bell-labs.com/cm/ms/what/shannonday/shannon1948.pdf|accessdate=2010-10-21}}</ref><ref>{{cite journal|last=Shannon|first=Claude E.|title=A mathematical theory of communication|journal=Bell System Technical Journal|date=October 1948|pages=623–656, |url=http://cm.bell-labs.com/cm/ms/what/shannonday/shannon1948.pdf|accessdate=2010-10-21}}</ref>
* Many landmark papers from the developers of [[Unix]] appeared in a themed issue in 1978.<ref>{{cite journal|last=Ritchie|first=D.M.|author2=K. Thompson|title=The UNIX Time-Sharing System|journal=Bell System Technical Journal|date=July–August 1978|volume=57|issue=6|url=http://cm.bell-labs.com/cm/cs/who/dmr/cacm.html|accessdate=2010-10-22}}</ref>
* The journal is also notorious for a November 1954 article entitled "In-Band Single-Frequency Signaling"<ref>{{cite journal|last=Weaver|first=A.|author2=N. A. Newell|title=In-Band Single-Frequency Signaling|year=1954|volume=33|issue=6|url=http://www.alcatel-lucent.com/bstj/vol33-1954/articles/bstj33-6-1309.pdf|accessdate=2011-03-03}}</ref> by Weaver and Newell that revealed the internal operation of the long-distance switching system in use at that time. This article enabled [[phone phreak]]s to develop the [[blue box]] apparatus that manipulated the switching system to allow them to make free long-distance calls.
* The 2009 Nobel Prize physicists [[Willard Boyle]] and [[George E. Smith]] described their new [[charge-coupled device]] in the journal in a 1970 paper.<ref>[http://www.alcatel-lucent.com/bstj/vol49-1970/articles/bstj49-4-587.pdf "Charge coupled semiconductor devices."] ''Bell System Technical Journal'', 49(4): 587-93, April 1970.</ref>

==Editors==
Below is a list of some of the former editors of this journal.

*1922 (July) R.W. King <ref name=king>
{{cite journal
 | last =
 | first =
 | authorlink =
 | title = Forward
 | journal = Bell System Technical Journal
 | volume = 01
 | issue = 01
 | pages = 1–3
 | publisher = American Telephone and Telegraph Company
 | location = New York City
 | date = July 1922
 | language =
 | url = http://www.alcatel-lucent.com/bstj/vol01-1922/bstj-vol01-issue01.html
 | jstor =
 | issn =
 | doi =
 | accessdate = 2013-01-20}}</ref>

*1954 J.D. Tebo <ref>[http://www.historyofphonephreaking.org/docs/blr-1954-toc.pdf The History of Phone Phreaking]. 1954.</ref>
*1957 (May) W.D. Bulloch <ref name=new>
{{cite web
 | title = Announcement of New Editor
 | work =
 | publisher = AT & T
 | url = http://www.alcatel-lucent.com/bstj/bstj_results.html?keyword=editor&searchScope=any&title=&authors=&dateFrom=&dateTo=&sortBy=relevance&x=13&y=13
 | format =
 | doi =
 | accessdate =2013-01-20}}</ref>

*1959 (January) H.S. Renne <ref name=new/>
*1961 (March)   G.E. Schindler, Jr.<ref name=new/>

==Former indexing services==
* [[Chemical Abstracts Service]]

==See also==
*''[[Bell Labs Record]]''
*[[Scientific journal]]

==References==
{{reflist}}

==External links==
*[http://www.alcatel-lucent.com/bstj Bell System Technical Journal Archives, 1922-1983]
*[http://onlinebooks.library.upenn.edu/webbin/serial?id=belltechj The Bell System Technical Journal, Volumes 1 through 36 archived at The Internet Archive]
*[http://www.alcatel-lucent.com/bstj/vol07-1928/articles/bstj7-1-90.pdf	The Diffraction of Electrons by a Crystal of Nickel]
*[http://cassi.cas.org/publication.jsp?P=LglBQf5Q2NQyz133K_ll3zLPXfcr-WXf-RXC78SWL01tjXovRF6YRjLPXfcr-WXfimSBIkq8XcUjhmk0WtYxmzLPXfcr-WXfA6vmvS4KlzS-EiWdN2-uyg CAS Source Index (CASSI)] for ''Bell System Technical Journal.''

[[Category:Defunct journals]]
[[Category:Publications established in 1922]]
[[Category:Engineering journals]]
[[Category:Publications disestablished in 1983]]
[[Category:Bell Labs]]
[[Category:English-language journals]]
[[Category:House organs]]