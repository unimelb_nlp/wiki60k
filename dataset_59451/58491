{{Infobox journal
| title = Journal of Service Research
| cover =
| editor = Mary Jo Bitner
| discipline = [[Business studies]]
| former_names =
| abbreviation = J. Serv. Res.
| publisher = [[Sage Publications]]
| country =
| frequency = Quarterly
| history = 1998-present
| openaccess =
| license =
| impact = 2.143
| impact-year = 2013
| website = http://www.sagepub.com/journals/Journal200746/title
| link1 = http://jsr.sagepub.com/content/current
| link1-name = Online access
| link2 = http://jsr.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR =
| OCLC = 37279531
| LCCN = 98660141 
| CODEN =
| ISSN = 1094-6705 
| eISSN = 1552-7379 
}}
The '''''Journal of Service Research''''' is a quarterly [[Peer review|peer-reviewed]] [[academic journal]] that covers the field of [[business studies]]. The [[editor-in-chief]] is Mary Jo Bitner ([[Arizona State University]]). The prior editors-in-chief were Katherine Lemon ([[Boston College]]), A. Parasuraman ([[University of Miami]]), and Roland Rust ([[University of Maryland, College Park|University of Maryland]]). The journal was established by Rust in 1998 and is published by [[Sage Publications]].  

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2011 [[impact factor]] is 2.143, ranking it 26th out of 111 journals in the category "Business".<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Business |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=Web of Science}}</ref> 

==Best Paper Award==
Each year the [[Editorial board|editorial review board]] votes on the best research paper that was published during the prior publication year.

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.sagepub.com/journals/Journal200746/title}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Business and management journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1998]]