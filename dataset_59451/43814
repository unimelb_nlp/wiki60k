<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Hummingbird
 | image=VAT Hummingbird 260l (8420613276).jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Helicopter]]
 | national origin=[[United States]]
 | manufacturer=[[Vertical Aviation Technologies]]
 | designer=
 | first flight=
 | introduced=1991
 | retired=
 | status=In production (2013)
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= [[US$]]138,600 (2011)
 | developed from= [[Sikorsky S-52]]
 | variants with their own articles=
}}
|}
The '''Vertical Hummingbird''' is an [[United States|American]] [[helicopter]], produced by [[Vertical Aviation Technologies]] of [[Sanford, Florida]] that was introduced in 1991. The aircraft is supplied as a kit for [[Homebuilt aircraft|amateur construction]].<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 194. WDLA UK, Lancaster UK, 2011. {{ISSN|1368-485X}}</ref><ref name="VertHome">{{cite web|url = http://vertical-aviation.com/|title = Home of the four passenger Hummingbird|accessdate = 14 February 2013|last = Vertical Aviation Technologies|year = 2013}}</ref>

==Design and development==
The Hummingbird is a development of the [[type certificate|certified]] [[Sikorsky S-52]] that first flew in 1947, adapted to kit form. The aircraft features a single main rotor, a four seat enclosed cabin, quadracycle [[landing gear]] and a choice of two powerplants.<ref name="WDLA11" /> The prototype was converted by Vertical Aviation Technologies from a [[Sikorsky S-52-3]].<ref name=JAWA88-89/>

The Hummingbird [[fuselage]] is made from riveted [[aluminum]] sheet. The nose section is adapted from the [[Bell 206]]. Its {{convert|33|ft|m|1|abbr=on}} diameter fully articulated three-bladed main rotor employs a [[NACA airfoil|NACA 0015]] [[airfoil]]. The two-bladed tail rotor has a diameter of {{convert|5.75|ft|m|1|abbr=on}}. The aircraft has an empty weight of {{convert|1850|lb|kg|0|abbr=on}} and a gross weight of {{convert|2800|lb|kg|0|abbr=on}}, giving a useful load of {{convert|950|lb|kg|0|abbr=on}}. With full fuel of {{convert|57|u.s.gal}} the payload is {{convert|608|lb|kg|0|abbr=on}}.<ref name="WDLA11" /><ref name="Incomplete">{{Cite web|url = http://www.ae.illinois.edu/m-selig/ads/aircraft.html|title = The Incomplete Guide to Airfoil Usage |accessdate = 14 February 2012|last = Lednicer |first = David |year = 2010}}</ref><ref name="260L">{{cite web|url = http://vertical-aviation.com/hummingbird-kit-helicopter/260l/|title = Hummingbird 260L |accessdate = 14 February 2013|last = Vertical Aviation Technologies|year = 2013}}</ref>
<!-- ==Operational history== -->

==Variants==
;VAT S-52-3:Prototype of the Hummingbird family, converted from an original [[Sikorsky S-52|Sikorsky S-52-3]].<ref name=JAWA88-89>{{cite book |title=Jane's All the World's Aircraft 1988-89 |year=1988 |publisher=Jane's Information Group |location=London |isbn=0-7106-0867-5 |editor=John W.R. Taylor|page=495}}</ref>
;Hummingbird 260L.
:Version powered by a six cylinder, air-cooled, [[four-stroke]], [[dual-ignition]] {{convert|265|hp|kW|0|abbr=on}} [[Lycoming O-435|Lycoming IVO-435]] engine<ref name="260L" />
;Hummingbird 300LS
:Version powered by an eight cylinder, liquid-cooled, four-stroke, single-ignition {{convert|325|hp|kW|0|abbr=on}} [[General Motors LS7]] V-8 automotive conversion engine, derated to {{convert|280|hp|kW|0|abbr=on}}<ref name="300LS">{{cite web|url = http://vertical-aviation.com/hummingbird-kit-helicopter/300ls/|title = Hummingbird 300LS |accessdate = 14 February 2013|last = Vertical Aviation Technologies|year = 2013}}</ref>
<!-- ==Aircraft on display== -->

==Specifications (Hummingbird 260L) ==
{{Aircraft specs
|ref=Bayerl and Vertical Aviation<ref name="WDLA11" /><ref name="260L" />
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, 
met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=three passengers
|length m=
|length ft=
|length in=
|length note=
|width m=
|width ft=5
|width in=0
|width note=
|height m=
|height ft=8
|height in=7
|height note=
|airfoil=[[NACA airfoil|NACA 0015]]
|empty weight kg=
|empty weight lb=1850
|empty weight note=
|gross weight kg=
|gross weight lb=2800
|gross weight note=
|fuel capacity={{convert|57|u.s.gal}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Lycoming O-435|Lycoming IVO-435]]
|eng1 type=six cylinder, air-cooled, [[four stroke]] [[aircraft engine]]
|eng1 kw=
|eng1 hp=265

|rot number=1
|rot dia m=
|rot dia ft=33
|rot dia in=0
|rot area sqm=
|rot area sqft=
|rot area note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=100
|cruise speed kts=
|cruise speed note=
|never exceed speed kmh=
|never exceed speed mph=120
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=375
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=14000
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=950
|climb rate note=at {{convert|2800|lb|kg|0|abbr=on}}
|time to altitude=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}
<!-- ==See also== -->

==References==
{{reflist}}

==External links==
{{commons category}}
*{{Official website|http://vertical-aviation.com/}}

[[Category:United States sport aircraft 1990–1999]]
[[Category:United States helicopters 1990–1999]]
[[Category:Homebuilt aircraft]]
[[Category:Single-engine aircraft]]
[[Category:Vertical Aviation aircraft|Hummingbird]]
[[Category:Single-engined piston helicopters]]