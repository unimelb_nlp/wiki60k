{{Infobox journal
| title = Mayo Clinic Proceedings
| cover = January 2016 Mayo Clinic Proceedings Journal Cover.jpg
| caption=January 2016 Mayo Clinic Proceedings
| editor = Lisa K. Muenkel
| discipline = [[Internal medicine]]
| abbreviation = Mayo Clin. Proc.
| publisher = [[Elsevier]] on behalf of the [[Mayo Clinic]]
| country = United States
| frequency = Monthly
| history = 1926–present
| openaccess =
| impact = 6.262
| impact-year = 2014
| website = http://www.mayoclinicproceedings.org
| link1 = http://www.mayoclinicproceedings.org/current
| link1-name = Online acces
| link2 = http://www.mayoclinicproceedings.org/issues
| link2-name = Online archive
| formernames = Proceedings of the Staff Meetings of the Mayo Clinic
| JSTOR =
| OCLC = 00822709
| LCCN = sc78001722
| CODEN = MACPAJ
| ISSN = 0025-6196
| eISSN = 1942-5546
}}
'''''Mayo Clinic Proceedings''''' is a monthly [[peer-reviewed]] [[medical journal]] published by [[Elsevier]] and sponsored by the [[Mayo Clinic]]. It covers the field of general [[internal medicine]]. The journal was established in 1926 as the ''Proceedings of the Staff Meetings of the Mayo Clinic'' and obtained its current name in 1964. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 6.262, ranking it 11th out of 153 journals in the category "Medicine, General & Internal".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Medicine, General & Internal |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref> The journal started online publishing in 1999.<ref>{{cite journal |doi=10.1016/S0025-6196(11)64567-9 |title=Leading Mayo Clinic Proceedings Into the Future |journal=Mayo Clinic Proceedings |year=1999 |volume=74 |page=104 |last1=Lanier |first1=W.L.}}</ref> Initially, its website consisted of simple lists of tables of content. In 2012, the current website was established. In addition to the journal content, it contains extra features such as Medical Images, Residents Clinics, Art at Mayo, and Stamp Vignettes on Medical Science, as well as author interviews and monthly issue summaries. Readers can also obtain CME credit.<ref>{{cite web |url=http://www.mayoclinicproceedings.org/content/cmeInstrux |title=Journal CME Instructions |publisher=Elsevier |work=Mayo Clinic Proceedings |date= |accessdate=2013-03-12}}</ref>

== Types of articles published ==
''Mayo Clinic Proceeding''s publishes the following types of articles:

* Original Article
* Review Article
* Solicited Review
* Concise Review for Clinicians (continuing medical education – CME – credit is offered with the Concise Review for Clinicians section)
* My Treatment Approach
* Diagnosis and Treatment Guidelines
* Special Article
* Commentary
* Brief Report
* Editorial
* Letter to the Editor
* Medical Images
* Stamp / Historical Vignettes
* Art at Mayo
* Meeting Reports
* Symposium  (continuing medical education – CME – credit is offered with the Symposium collections)
* Case Report
* Residents' Clinic
* Path-to-Patient image quiz

== Editors ==
The following persons have been [[editor-in-chief|editors-in-chief]] of the journal:
* E. D. Bayrd (1964–1970)
* A. B. Hayles (1971–1976)
* J. L. Juergens (1977–1981)
* R. G. Siekert (1982–1986)
* P. J. Palumbo (1987–1993)
* Udaya B. S. Prakash (1994–1998)
* William L. Lanier (1999–present)

== References ==
{{Reflist}}

==External links==
*{{Official website|http://www.mayoclinicproceedings.org}}

[[Category:Publications established in 1926]]
[[Category:General medical journals]]
[[Category:Mayo Clinic]]
[[Category:English-language journals]]
[[Category:Monthly journals]]
[[Category:Elsevier academic journals]]
[[Category:Academic journals associated with non-profit organizations of the United States]]