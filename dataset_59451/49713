{{Use dmy dates|date=June 2011}}
{{Use British English|date=June 2011}}
{{Infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = Lady Anna
| title_orig    = 
| translator    = 
| image         = Lady Anna 1st.jpg
| caption = First edition title page
| author        = [[Anthony Trollope]]
| illustrator   = 
| cover_artist  = 
| country       = United Kingdom
| language      = English
| series        = 
| genre         = 
| publisher     = [[Chapman & Hall]]
| pub_date      = April 1873 to April 1874; 2 volumes, 1874
| english_pub_date =
| media_type    = Serialized; Print
| pages         = 
| isbn = 0-19-283718-4
| isbn_note = (Oxford University Press paperback edition, 1998)
| preceded_by   =
| followed_by   =
}}

'''''Lady Anna''''' is a novel by [[Anthony Trollope]], written in 1871 and first published in book form in 1874.  The protagonist is a young woman of noble birth who, through an extraordinary set of circumstances, has fallen in love with and become engaged to a tailor.  The novel describes her attempts to resolve the conflict between her duty to her social class and her duty to the man she loves.

==Plot summary==

''Lady Anna'' is set during the 1830s, at about the time of the [[Reform Act 1832|First Reform Act of 1832]].<ref>Orgel, Stephen (1990).  Introduction to Anthony Trollope, ''Lady Anna''.  Oxford University Press.  p. xii.</ref>

The title character is the daughter of the late Earl Lovel.  Her mother married him out of ambition rather than love, and despite his evil reputation.  Soon after their marriage, he told her that he had a living wife, which made their union invalid and their unborn daughter [[Legitimacy (law)|illegitimate]].  He then sailed to Italy without her and did not return to England for twenty years.

During those two decades, Lady Lovel struggled to prove the validity of her marriage, and consequently her right to her title and her daughter's legitimacy.  She enjoyed neither the sympathy of the public nor the support of her family during this time; her only friend and supporter was Thomas Thwaite, a [[Radicalism (historical)|Radical]] tailor of [[Keswick, Cumbria|Keswick]], who gave her and her daughter shelter and financed her legal battles.

Early in the novel, Lord Lovel returns to England and dies [[Intestacy|intestate]].  His [[earl]]dom, and a small estate in [[Cumberland]], pass to a distant cousin, young Frederick Lovel.  However, the bulk of his large fortune is [[personal property]], and thus not attached to the title.  If his marriage to Lady Lovel was valid, it will go to her and to their daughter; otherwise, it will go to the young earl.

The new earl's lawyers, headed by the [[Solicitor General for England and Wales|Solicitor General]], come to believe that their case against Lady Lovel is weak and their claim probably false.  They accordingly propose a compromise: that the earl marry Lady Anna, thus reuniting the title and the assets held by her father.  The plan is enthusiastically supported by Lady Lovel, as fulfilling all of her ambitions for herself and her daughter.  The young earl is favorably impressed by Lady Anna's appearance and character.  However, in her twenty years as an outcast, Lady Anna has come to love Thomas Thwaite's son Daniel, and the two have become secretly engaged.

When the engagement is known, Lady Lovel and others strive to break it.  Lady Anna will not yield to persuasion or to mistreatment; Daniel Thwaite rejects arguments and bribes to end the relationship.  Lady Anna is approaching her twenty-first birthday, after which she will be free to marry without her mother's consent.  In desperation, Lady Lovel secures a pistol and attempts to murder Thwaite.  She wounds but does not kill him; Thwaite refuses to name her to the police; and the attempt puts an effective end to her attempts to keep the two apart.

With Thwaite's consent, Lady Anna makes half of her fortune over to the young earl.  She marries Thwaite with the public approval of the Lovel family, though Lady Lovel refuses to attend the ceremony.  The two then emigrate to Australia, where they expect that his low birth and her title will no longer be a burden to them.

==Development and publication history==

In 1871, Anthony and Rose Trollope sailed from England to Australia to visit their son Fred, who had settled at a sheep station near [[Grenfell, New South Wales]].<ref>Joyce, R. B.  [http://adbonline.anu.edu.au/biogs/A060324b.htm "Trollope, Anthony (1815 - 1882)".]  In [http://adbonline.anu.edu.au/adbonline.htm Australian Dictionary of Biography Online.]  Retrieved 2010-07-10.</ref>
''Lady Anna'' was written entirely at sea,<ref name=autobio19>Trollope, Anthony (1883).  [http://ebooks.adelaide.edu.au/t/trollope/anthony/autobiography/chapter19.html ''An Autobiography'', chapter 19.]  Retrieved 2010-07-10.</ref> 
between 25 May and 19 July.<ref name=moody>Moody, Ellen.  [http://www.jimandellen.org/trollope/trollope.writing.chron.html "A Chronology of Anthony Trollope's Writing Life".]  [http://www.jimandellen.org/ellen/emhome.htm Ellen Moody's Website: Mostly on English and Continental and Womens' Literature].  Retrieved 2010-07-10.</ref>

The novel was serialized in the ''[[Fortnightly Review]]'' between April 1873 and April 1874.<ref name=moody />
It was published in two volumes by [[Chapman & Hall]] in 1874.<ref>[http://www.booksellerworld.com/anthony-trollope.htm "Anthony Trollope Bibliography."]  [http://www.booksellerworld.com/ Bookseller World.]  Retrieved 2010-07-10.</ref>

Trollope received ₤1200 for ''Lady Anna''; his other 1874 novels, ''[[Phineas Redux]]'' and ''[[Harry Heathcote of Gangoil]]'', earned him ₤2500 and ₤450 respectively.<ref>Trollope (1883), [http://ebooks.adelaide.edu.au/t/trollope/anthony/autobiography/chapter20.html chapter 20].  Retrieved 2010-05-18.</ref>

Beside the ''Fortnightly'', the novel was serialized in 1873&ndash;74 in the ''Australasian'', and in Russian translation in ''[[Vestnik Evropy]]''.  In 1873, it was also published in book form by [[Harper (publisher)|Harper]] of [[New York City|New York]], by Hunter Rose of [[Toronto]], by [[Tauchnitz]] of [[Leipzig]], and in Russian as ''Lady Anna''.<ref name=tingay>Tingay, Lance O (1985).  ''The Trollope Collector''.  London: The Silverbridge Press.  p. 36.</ref>

More recent editions have included one by Arno Press in 1981; editions by [[Dover Publications|Dover]] and by [[Oxford University Press]], the latter with an introduction and notes by [[Stephen Orgel]], in 1984; and one by the Trollope Society in 1990.<ref>Moody, Ellen.  [http://www.jimandellen.org/trollope/general.biblio.html "Trollope's Singletons".]  [http://www.jimandellen.org/ellen/emhome.htm Ellen Moody's Website: Mostly on English and Continental and Womens' Literature].  Retrieved 2010-07-10.</ref>

==Adaptation==

A play by Craig Baxter, ''Lady Anna: All At Sea'', combining the plot of the novel and the story of Trollope's writing it while voyaging to Australia, was commissioned by the Trollope Society as part of the 2015 Trollope Bicentennial Celebrations.  It was presented at London's [[Park Theatre (London)|Park Theatre]] in 2015.<ref>[https://web.archive.org/web/20160402150604/https://www.parktheatre.co.uk/whats-on/lady-anna-all-at-sea "Lady Anna: All At Sea".]  [https://www.parktheatre.co.uk/ Park Theatre.]  Retrieved 2016-04-02.  Archived from [https://www.parktheatre.co.uk/whats-on/lady-anna-all-at-sea original] 2016-04-02.</ref><ref>Norman, Neil.  [http://www.express.co.uk/entertainment/theatre/601159/review-Lady-Anna-All-At-Sea-Park-Theatre "Review: Lady Anna: All At Sea at the Park Theatre".]  [http://www.express.co.uk/ ''Express''.]  2015-08-28.  Retrieved 2016-04-02.</ref>

==References==
{{reflist}}

==External links==
*[http://ebooks.adelaide.edu.au/t/trollope/anthony/anna/ ''Lady Anna'']&mdash;easy-to-read HTML version at [http://ebooks.adelaide.edu.au/ University of Adelaide Library]
*[http://www.gutenberg.org/etext/31274 ''Lady Anna''] at [http://www.gutenberg.org/wiki/Main_Page Project Gutenberg]
* {{librivox book | title=Lady Anna | author=Anthony Trollope}}

{{Anthony Trollope}}

[[Category:1874 novels]]
[[Category:Novels by Anthony Trollope]]
[[Category:Works originally published in Fortnightly Review]]
[[Category:Novels first published in serial form]]
[[Category:Novels set in the 1830s]]
[[Category:Novels set in Cumbria]]
[[Category:Chapman & Hall books]]