The '''''PubMed Dietary Supplement Subset''''' (PMDSS) is a joint project between the [[National Institutes of Health]] (NIH) [[National Library of Medicine]] (NLM) and the NIH ''Office of Dietary Supplements'' (ODS). PMDSS is designed to help people search for [[academic journal|academic journal articles]] related to [[dietary supplement]] literature. The subset was created using a search strategy that includes terms provided by the Office of Dietary Supplements , and selected journals indexed for [[PubMed]] that include significant dietary supplement related content. It succeeds the ''International Bibliographic Information on Dietary Supplements'' (IBIDS) database, 1999-2010, which was a collaboration between the Office of Dietary Supplements and the [[U.S. Department of Agriculture]]'s [[National Agricultural Library]].<ref>{{Cite web
|title=New PubMed® subset - "Dietary Supplements 
|format=U.S.Government content and in the Public Domain 
|url=http://ods.od.nih.gov/Health_Information/IBIDS.aspx}}</ref><ref name=herbal>
{{cite book
 | last = Bonakdar
 | first = Robert Alan 
 | authorlink =
 | title = The H.E.R.B.A.L. Guide: Dietary Supplement Resources for the Clinician
 | publisher = Lippincott Williams & Wilkins 
 | series =
 | volume =
 | edition =
 | date = 2010
 | location =
 | pages = 195
 | language =
 | url = https://books.google.com/books?id=xkfxvp-X024C&pg=PA300&dq=International+Bibliographic+Information+on+Dietary+Supplements&hl=en&sa=X&ei=dpP7UPm9A4GC8ATf84CwCw&ved=0CEEQ6AEwAQ#v=onepage&q=International%20Bibliographic%20Information%20on%20Dietary%20Supplements&f=false
 | doi =
 | id =
 | isbn = 9780781782685
 | mr =
 | zbl =
 | jfm = }}</ref>

==The Subset==
ODS and NLM partnered to create this Dietary Supplement Subset of NLM's PubMed database. PubMed provides access to citations from the [[MEDLINE]] database and additional [[life science]] academic journals. It also includes links to many full-text articles at journal Web sites and other related Web resources.

The subset is designed to limit search results to [[citation index|citations]] from a broad spectrum of dietary supplement literature including vitamin, mineral, phytochemical, ergogenic, botanical, and herbal supplements in human nutrition and animal models. The subset will retrieve dietary supplement-related citations on topics including, but not limited to:

*chemical composition; 
*biochemical role and function — both in vitro and in vivo; 
*clinical trials; 
*health and adverse effects; 
*fortification; 
*traditional Chinese medicine and other folk/ethnic supplement practices; 
*cultivation of botanical products used as dietary supplements; as well as, 
*surveys of dietary supplement use.<ref>
{{Cite web
|title=About the PubMed Dietary Supplement Subset 
|format= U.S.Government content and in the Public Domain
|url=http://ods.od.nih.gov/Research/PubMed_Dietary_Supplement_Subset.aspx}}</ref>

The PMDSS is a free service and can be accessed either directly through the ODS Website or in PubMed using the Dietary Supplement filter (formerly referred to as a Limit).

==History==
Dietary supplements were first regulated in by the Federal Food Drug and Cosmetic Act of 1938. In 1941 the United States Food and Drug Administration proferred definitions for dietary supplementary foods which included minerals, vitamins and other specialized supplements. In the early 1970s the FDA tried to regulate the constituents that make up vitamins and minerals, specifically as dietary supplements. However, as the 1970s progressed, a 1974 court decision and legislation that passed in 1976 would not allow such action. In 1994 the Dietary Supplement Health Education Act(DSHEA) was passed which answered the need for promoting health and giving consumers accurate and up-to-date information on vitamins and minerals. Dietary Supplements also became a regulated category with the passage of the DSHEA. This act defines dietary supplements, makes safety a matter of regulation, states the requirements for approved third party literature, and regulates labeling standards.<ref name=hpg/>

The Dietary Supplement Health and Education Act of 1994 mandated the establishment of the Office of Dietary Supplements (ODS) at the NIH. The ODS was created in 1995 as a third tier office within the National Institutes of Health. The mission of ODS is to strengthen knowledge and understanding of dietary supplements by evaluating scientific information, stimulating and supporting research, disseminating research results, and educating the public to foster an enhanced quality of life and health for the U.S. population. In support of this mission the ODS created two databases one of which was named the ''International Bibliographic Information on Dietary Supplements (IBIDS) database''. The other database, ''Computer Access to Research on Dietary Supplements'' (CARDS), is a database of federally funded research projects pertaining to dietary supplements. The IBIDS database was retired in 2010 and the PMDSS was launched to continue the ODS mission to disseminate dietary supplement-related research results.<ref name=hpg>
{{cite book
 | last =Allison Sarubin Fragakis, Cynthia Thomson, American Dietetic Association staff
 | first =
 | authorlink =
 | title = The Health Professional's Guide to Popular Dietary Supplements
 | publisher = [[American Dietetic Association]]
 | series =
 | volume =
 | edition = 3rd
 | date = 2007
 | location =
 | pages = 639–645
 | language =
 | url = https://books.google.com/books?hl=en&lr=&id=KA8vU6XKfIAC&oi=fnd&pg=PA639&dq=International+Bibliographic+Information+on+Dietary+Supplements+&ots=9Vh3Ng21bc&sig=CXhXB0r93cqbufJRQ3xkiHlRLC0#v=onepage&q=International%20Bibliographic%20Information%20on%20Dietary%20Supplements&f=false
 | doi =
 | id =
 | isbn =978-0-88091-363-8
 | mr =
 | zbl =
 | jfm = }}</ref><ref>
{{Cite web
 | last =ODS
 |title=Mission, Origin and Mandate 
 |format= U.S.Government content and in the Public Domain
 |url=http://ods.od.nih.gov/About/MissionOriginMandate.aspx}}</ref>

==See also==
* [[Natural Standard]]
* [[Examine.com]]

==References==
{{reflist}}

==External links==
{{Commons category|Dietary Supplements (database)}}
* [http://ods.od.nih.gov/Research/PubMed_Dietary_Supplement_Subset.aspx  PMDSS via ODS]
* [http://www.ncbi.nlm.nih.gov/pubmed/  PubMed] Activate the PMDSS via Dietary Supplement filter

[[Category:Bibliographic databases and indexes]]
[[Category:National Institutes of Health]]
[[Category:Databases in the United States]]
[[Category:Scientific databases]]
[[Category:Online databases]]