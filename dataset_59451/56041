{{Infobox journal
| title = Bulletin of the History of Archaeology
| editor = Gabriel Moshenska
| discipline = [[Archaeology]]
| publisher = [[Ubiquity Press]]
| openaccess= Yes
| history = 1991–present
| frequency = Biannually
| website = http://www.archaeologybulletin.org/
| link1 = http://www.archaeologybulletin.org/issue/current
| link1-name = Online access
| link2 = http://www.archaeologybulletin.org/issue/archive
| link2-name = Online archive
| ISSN = 1062-4740
| OCLC = 24683018
}}
The '''''Bulletin of the History of Archaeology''''' is an [[open access]], [[peer-reviewed]] [[academic journal]] publishing research, reviews, and short communications on the [[history of archaeology]].<ref>{{cite web|url=http://www.uq.edu.au/research/rid/files/info/biblio_info/specifications/RegisterofRefereedJournals.pdf | title=Register of Refereed Journals, Australian Government, Department of Education, Science and Training | accessdate=2011-06-17}}</ref><ref>{{cite web|url=http://www.hssonline.org/publications/CBs/2008%20Isis%20CB%20Vol%2099.pdf | title=2008 Isis Current Bibliography of the History of Science and Its Cultural Influences | accessdate=2011-06-15}}</ref> It was established in May 1991 by Douglas Givens.<ref>{{cite journal|url=http://www.springerlink.com/content/7x75382721038h22/ | title=The coming of age of the history of archaeology|journal= Journal of Archaeological Research| volume= 2| number= 1|pages= 113–136| DOI= 10.1007/BF02230098 | accessdate=2011-06-17}}</ref> It was edited for many years by [[Tim Murray (archaeologist)|Tim Murray]], and since 2015 it has been [[Editor-in-chief|edited]] by Gabriel Moshenska.<ref>{{Cite web|title = Bulletin of the History of Archaeology|url = http://www.archaeologybulletin.org/about/editorialteam/|website = www.archaeologybulletin.org|access-date = 2016-01-26}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.archaeologybulletin.org/}}

[[Category:Archaeology journals]]
[[Category:Publications established in 1991]]
[[Category:English-language journals]]
[[Category:Biannual journals]]
[[Category:Ubiquity Press academic journals]]


{{archaeology-journal-stub}}