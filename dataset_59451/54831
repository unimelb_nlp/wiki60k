{{Infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = The Snow-Image, and Other Twice-Told Tales
| title_orig    = 
| translator    = 
| image         = The Snow-Image, and Other Twice-Told Tales.jpg
| caption = 
| author        = [[Nathaniel Hawthorne]]
| illustrator   = 
| cover_artist  = 
| country       = [[United States]]
| language      = [[English language|English]]
| series        = 
| genre         = [[Short stories]]
| publisher     = [[Ticknor & Fields|Ticknor, Reed & Fields]]
| release_date  = 1851
| english_release_date = 
| media_type    = Print ([[Hardcover|Hardback]])
| pages         = 
| preceded_by   = 
| followed_by   = 
}}
'''''The Snow-Image, and Other Twice-Told Tales''''' is a collection of short stories by American author [[Nathaniel Hawthorne]]. Released in late 1851 with a copyright of 1852, it is the final collection of tales by Hawthorne published in his lifetime.

==Background==
After publishing his collection ''[[Mosses from an Old Manse]]'' in 1846, Hawthorne mostly turned away from the short tales that had marked the majority of his career to that point. In the interim period leading up to the collection ''The Snow-Image, and Other Twice-Told Tales'', he wrote only four new stories: "Main-street", "Feathertop", "The Snow-Image", and "[[The Great Stone Face (Hawthorne)|The Great Stone Face]]".<ref>Quirk, Tom. ''Nothing Abstract: Investigations in the American Literary Imagination''. Columbia, MO: University of Missouri Press, 2001: 64. ISBN 0-8262-1364-2</ref> All but "Feathertop" would be included in the new collection along with several other previously-published works.

In his preface to the collection, Hawthorne playfully noted that his confessional tone in writing about himself should not be trusted: "[T]hese things hide the man instead of displaying him", he wrote, and suggested that readers seeking "essential traits" of the author "must make quite another kind of inquest", specifically that "you must look through the whole range of his fictitious characters, good and evil".<ref>Porte, Joel. ''n Respect to Egotism: Studies in American Romantic Writing''. New York: Cambridge University Press, 1991: 124. ISBN 978-0-521-36273-3</ref>

==Publication history==
[[File:Ticknor Reed and Fields advertisement September 1852.jpg|thumb|Advertisement from [[Ticknor and Fields|Ticknor, Reed and Fields]] advertising several of Hawthorne's works in 1852]]
[[File:Front Cover - First Edition Printed in 1852.jpg|thumb|Front Cover - First Edition Printed in 1851]]
Hawthorne was ending his brief stay in [[Lenox, Massachusetts]], as ''The Snow-Image, and Other Twice Told Tales'' was being prepared. During his time there, Hawthorne had befriended [[Herman Melville]], who had just published ''[[Moby-Dick]]'' with a dedication to Hawthorne as Hawthorne was preparing the preface for his new book.<ref>Miller, Edwin Haviland. ''Salem Is My Dwelling Place: A Life of Nathaniel Hawthorne''. Iowa City: University of Iowa Press, 1991: 353. ISBN 0-87745-332-2.</ref> Publisher [[James Thomas Fields]] compiled the collection of 15 tales and sketches and published it in book form in December 1851. Commercially, it was Hawthorne's least popular book.<ref name=Wineapple245>Wineapple, Brenda. ''Hawthorne: A Life''. New York: Random House, 2004: 245. ISBN 0-8129-7291-0</ref>

Hawthorne dedicated the collection to his friend [[Horatio Bridge]], who had subsidized his first collection years earlier. In his dedicatory letter, Hawthorne directly addressed Bridge: "If anybody is responsible for my being at this day an author, it is yourself."<ref name=Wineapple245/> Hawthorne also fondly recalled the time he shared with Bridge as students at [[Bowdoin College]]:
{{bquote|we were lads together at a country college,—gathering blueberries, in study-hours, under those tall academic pines; or watching the great logs, as they tumbled along the current of the [[Androscoggin River|Androscoggin]]; or shooting pigeons and grey squirrels in the woods; or bat-fowling in the summer twilight; or catching trouts in that shadowy little stream which, I suppose, is still wandering riverward through the forest.<ref name=Miller69>Miller, Edwin Haviland. ''Salem Is My Dwelling Place: A Life of Nathaniel Hawthorne''. Iowa City: University of Iowa Press, 1991: 69. ISBN 0-87745-332-2.</ref>}}

Bridge believed that Hawthorne "overvalued" his friendship but after Hawthorne's death his widow [[Sophia Hawthorne]] assured him that the admiration was deserved: "He had the utmost reliance upon you, and reposed upon it with infinite satisfaction. It seems to me not a small merit to have inspired in him such respect, love and trust as he invariably expressed for you... he had no more thorough friend than you in the world, and I know he thought so."<ref name=Miller69/>

==Contents==
* Preface (1852)
* "[[The Snow-Image]]" (1850)
* "[[The Great Stone Face (Hawthorne)|The Great Stone Face]]" (1850)
* "[[Main-street]]" (1849)
* "[[Ethan Brand]]" (1850)
* "[[A Bell's Biography]]" (1837)
* "[[Sylph Etherege]]" (1838)
* "The Canterbury Pilgrims" (1833)
* "Old News" (1835)
* "[[The Man of Adamant]]" (1837)
* "[[The Devil in Manuscript]]" (1835)
* "[[John Inglefield's Thanksgiving]]"(1840)
*[[File:First Edition printed in 1851.jpg|thumb|First Edition printed in 1851]]"[[Old Ticonderoga]]" (1836)
* "[[The Wives of the Dead]]" (1832)
* "[[Little Daffydowndilly]]" (1843)
* "[[My Kinsman, Major Molineux]]" (1832)
[[File:Preface and Chapter One pages of First Edition printed 1851.jpg|thumb|Preface and Chapter One pages of First Edition printed 1851]]

==References==
*{{cite book | last=Bleiler | first=Everett | authorlink=Everett F. Bleiler | title=The Checklist of Fantastic Literature | location=Chicago | publisher=Shasta Publishers | date=1948 | page=145}}
{{reflist}}

{{Nathaniel Hawthorne}}

{{DEFAULTSORT:Snow-Image, and Other Twice-Told Tales, The}}
[[Category:1852 short story collections]]
[[Category:Short story collections by Nathaniel Hawthorne]]


{{story-collection-stub}}