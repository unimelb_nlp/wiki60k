{{good article}}
{{Infobox hurricane season 
| Basin=Atl
| Year=1854
| Track=1854 Atlantic hurricane season summary map.png
| First storm formed=June 25, 1854
| Last storm dissipated=October 22, 1854
| Strongest storm name=[[#Hurricane Three|Three]]
| Strongest storm winds=115
| Strongest storm pressure=938
| Average wind speed=1
| Total storms=5
| Total hurricanes=3
| Total intense=1
| Damages=0.02
| Inflated=0
| Fatalities=30+ direct
| five seasons=[[1852 Atlantic hurricane season|1852]], [[1853 Atlantic hurricane season|1853]], '''1854''', [[1855 Atlantic hurricane season|1855]], [[1856 Atlantic hurricane season|1856]]
}} 
The '''1854 Atlantic hurricane season''' featured five known [[tropical cyclone]]s, three of which made [[Landfall (meteorology)|landfall]] in the [[United States]]. At one time, another was believed to have existed near [[Galveston, Texas]] in September,<ref name="Partagas">{{cite report|url=http://www.aoml.noaa.gov/hrd/Landsea/Partagas/1851-1857/1854.pdf|title=Year 1854|author =Jose F. Partagas|date=1996|work=[[Atlantic Oceanographic and Meteorological Laboratory]]|publisher=[[National Oceanic and Atmospheric Administration]]|pages=21–26|accessdate=May 2, 2013|location=Miami, Florida|format=PDF}}</ref> but [[HURDAT]] &ndash; the official Atlantic hurricane database &ndash; now excludes this system.{{Atlantic hurricane best track}} The first system, Hurricane One, was initially observed on June&nbsp;25. The final storm, Hurricane Five, was last observed on October&nbsp;22. These dates fall within the period with the most tropical cyclone activity in the Atlantic. No tropical cyclones during this season existed simultaneously. One tropical cyclone has a single known point in its track due to a sparsity of data.

Of the season's five tropical cyclones, three reached hurricane status. Furthermore, one of those strengthened into a [[Saffir-Simpson Hurricane Scale#Categories|major hurricane]], which is Category&nbsp;3 or higher on the modern-day [[Saffir–Simpson hurricane wind scale]]. The strongest cyclone of the season, the third hurricane, peaked at Category&nbsp;3 strength with 125&nbsp;mph (205&nbsp;km/h) winds. After making landfall near the [[Georgia (U.S. state)|Georgia]]-[[South Carolina]] border, the storm caused 26&nbsp;fatalities and extensive damage in the area. Hurricane Four caused four deaths and approximately $20,000 (1854&nbsp;[[USD]]) in damage after striking the coast of [[Texas]]. Hurricane One also caused moderate damage in Texas.

__TOC__
{{clear}}

==Systems==

===Hurricane One===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1854 Atlantic hurricane 1 track.png
|Formed=June 25
|Dissipated=June 27
|1-min winds=70
|Pressure=982
}}
A tropical storm was first observed in the Gulf of Mexico on June&nbsp;25, while located about {{convert|240|mi|km}} south-southwest of [[Marsh Island (Louisiana)|Marsh Island]], [[Louisiana]]. It headed westward and strengthened into a hurricane about 12&nbsp;hours later. Peaking with [[maximum sustained wind]]s 80&nbsp;mph (130&nbsp;km/h) and a minimum [[barometric pressure]] of {{convert|982|mbar|inHg|abbr=on|lk=on}},{{Atlantic hurricane best track}}<ref name="u.s. hurricanes">{{cite report|url=http://www.aoml.noaa.gov/hrd/hurdat/All_U.S._Hurricanes.html|title=Chronological List of All Hurricanes: 1851 &ndash; 2012|date=2013|work=Atlantic Oceanographic and Meteorological Laboratory|publisher=National Oceanic and Atmospheric Administration|accessdate=July 24, 2013}}</ref> the storm maintained this intensity until making landfall in [[South Padre Island, Texas]] at 1200&nbsp;UTC on June&nbsp;26. It quickly weakened inland and fell to tropical storm strength about six hours later. The system continued in a west-northwestward direction over northern [[Mexico]], until dissipating in a rural area of [[Coahuila]] on June&nbsp;27.{{Atlantic hurricane best track}}

This system brought near tropical storm-force winds to Texas as far north as [[Galveston]]. [[Brazos Island]] experienced the brunt of this storm, where winds blew a "perfect hurricane". Many buildings in the area lost their roofs or were moved by the winds. Additionally, a cistern at the Quartermaster’s Department was destroyed. The coast of Texas was also impacted by storm surge, with several bath houses washed away at [[Port Lavaca, Texas|Lavaca]]. Precipitation in the region was generally light, peaking at {{convert|6.63|in|mm}} at Fort Ringgold, which is near modern-day [[Rio Grande City, Texas|Rio Grande City]].<ref name="TXhurricane">{{cite report|url=http://www.wpc.ncep.noaa.gov/research/txhur.pdf|title=Texas Hurricane History|author =David M. Roth|date=January 17, 2010|work=[[Weather Prediction Center]]|publisher=National Oceanic and Atmospheric Administration|page=17|accessdate=July 1, 2013|format=PDF}}</ref>
{{clear}}

===Tropical Storm Two===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1854 Atlantic tropical storm 2 track.png
|Formed=August 23
|Dissipated=August 23
|1-min winds=60
}}
The ships ''Highflyer'' and ''Osceola'' encountered a "very violent" gale on August&nbsp;23, while located at 33.0°N,  55.0°W, which is about 565&nbsp;miles (910&nbsp;km) east-northeast of [[Bermuda]].<ref name="Partagas" /> A sustained wind speed of 70&nbsp;mph (110&nbsp;km/h) was recorded, indicative of a strong tropical storm.<ref>{{cite report|url=http://www.aoml.noaa.gov/hrd/hurdat/excelfiles_centerfix/1854/1854_2.XLS|title=1854 Storm 2|work=Atlantic Oceanographic and Meteorological Laboratory|publisher=National Oceanic and Atmospheric Administration|accessdate=May 23, 2013|format=XLS}}</ref> No further information is available of this storm.{{Atlantic hurricane best track}} However, the [[barque]] ''Pilgrim'' experienced a severe gale on August&nbsp;29, which may have been the [[Extratropical cyclone|extratropical]] remnants of this system.<ref name="Partagas"/>

{{clear}}

===Hurricane Three===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1854 Atlantic hurricane 3 track.png
|Formed=September 7
|Dissipated=September 12
|1-min winds=110
|Pressure=938
}}
The brig ''Reindeer'' sighted a hurricane about {{convert|25|mi|km}} east of [[Hope Town]] in [[The Bahamas]] on September&nbsp;7.<ref name="Partagas"/> With winds of 125&nbsp;mph (205&nbsp;km/h) and a minimum [[barometric pressure]] of {{convert|938|mbar|inHg|abbr=on}}, this was the strongest tropical cyclone of the season. It moved northwestward and weakened slightly on September&nbsp;8. Later that day at 2000&nbsp;UTC, the hurricane made landfall near [[St. Catherines Island]], Georgia with winds of 115&nbsp;mph (185&nbsp;km/h). Early on September&nbsp;9, it weakened to a Category&nbsp;1 hurricane, then a tropical storm several hours later. Thereafter, the storm accelerated northeastward and re-emerged into the Atlantic Ocean near [[Virginia Beach, Virginia]] on September&nbsp;10. The system re-strengthened, becoming a hurricane again on September&nbsp;11. It eventually began to weaken again while moving rapidly eastward and was last noted about 515&nbsp;miles (830&nbsp;km) southeast of [[Cape Race]], [[Newfoundland (island)|Newfoundland]].{{Atlantic hurricane best track}}

Gales were reported in Florida, including as far south as [[St. Augustine, Florida|St. Augustine]]. In Georgia, the entire coast suffered significant impacts, with damage more severe from [[St. Simons, Georgia|St. Simons]] northward. About {{convert|110|acres|ha|abbr=on}} of rice crops were destroyed, equivalent to a loss of approximately 6,000&nbsp;bushels. Between [[Savannah, Georgia]] and [[Charleston, South Carolina]], "extraordinary tides" were reported.<ref>{{cite report|url=http://www.aoml.noaa.gov/hrd/Landsea/history/index.html|title=Chronological Listing of Tropical Cyclones affecting North Florida and Coastal Georgia 1565-1899|author1=Al Sandrik |author2=Christopher W. Landsea |date=May 2003|work=Atlantic Oceanographic and Meteorological Laboratory|publisher=National Oceanic and Atmospheric Administration|accessdate=July 1, 2013}}</ref> At [[Hutchinson Island (Georgia)|Hutchinson Island, Georgia]], the island was completely submerged, while there was significant inundation in eastern Savannah. Storm surge also brought coastal flooding to much of South Carolina, from [[Beaufort, South Carolina|Beaufort]] to [[Georgetown, South Carolina|Georgetown]]. Wind damage in that area was mainly confined to downed trees. However, in Charleston, South Carolina, a two story wooden building was destroyed and there was slight to moderate damage to other structures, limited to roofs and the destruction of fences.<ref>{{cite report|url=http://www.aoml.noaa.gov/hrd/Landsea/A%20Reanalysis%20of%20Five%2019th%20Century%20South%20Carolina%20Major%20Hurricanes.pdf|title=A Reanalysis Of Five 19th Century South Carolina Major Hurricanes Using Local Data Sources|author =Douglas Owen Mayes|date=1999|work=[[University of Northern Colorado]]|publisher=[[Atlantic Oceanographic and Meteorological Laboratory]]|pages=40–47|accessdate=July 23, 2013|format=PDF}}</ref> Throughout the United States, this storm resulted in at least 26&nbsp;fatalities.<ref>{{cite report|url=http://www.nhc.noaa.gov/pastdeadlya1.html|title=The Deadliest Atlantic Tropical Cyclones, 1492 &ndash; Present|author1=Jack Beven |author2=Jose F. Partagas |author3=Edward N. Rappaport |date=April 22, 1997|work=National Hurricane Center|publisher=National Oceanic and Atmospheric Administration|accessdate=July 1, 2013}}</ref>
{{clear}}

===Hurricane Four===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1854 Atlantic hurricane 4 track.png
|Formed=September 18
|Dissipated=September 20
|1-min winds=90
|Pressure=965
}}
Reports first indicated a hurricane in the Gulf of Mexico on September&nbsp;18, while centered about {{convert|110|mi|km}} south-southwest of [[Cameron, Louisiana]]. The storm drifted west-northwestward with winds of 105&nbsp;mph (165&nbsp;km/h), equivalent to a Category&nbsp;2 hurricane.{{Atlantic hurricane best track}} The lowest barometric pressure estimate was {{convert|965|mbar|inHg|abbr=on}}.<ref name="u.s. hurricanes"/> At 2100&nbsp;UTC on September&nbsp;18, the storm made landfall near [[Freeport, Texas]] at the same intensity. It weakened to a Category&nbsp;1 hurricane early on the following day. The system further weakened to a tropical storm at 1200&nbsp;UTC on September&nbsp;19. Re-curving northeastward, the storm persisted until dissipating over eastern Texas on September&nbsp;20.{{Atlantic hurricane best track}}

The steamship ''Louisiana'' reported that a gale struck [[Matagorda, Texas]] with "unparalleled fury", with nearly all buildings and vessels in the area destroyed.<ref name="Partagas"/> Several vessels also capsized near Galveston, including the ''Nick Hill'' and ''Kate Ward''. Within the city of Galveston, merchants, businesses, and houses suffered significant water damage due to an {{convert|8|ft|m}} storm surge. Cotton and sugar cane crops throughout the area were ruined. The storm caused at least four deaths, with several more occurring later due to a [[yellow fever]] outbreak. Damage in the region totaled approximately $20,000.<ref name="TXhurricane"/>
{{clear}}

===Tropical Storm Five===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1854 Atlantic tropical storm 5 track.png
|Formed=October 20
|Dissipated=October 22
|1-min winds=60
}}
The barque ''Southerney'' observed a tropical storm on October&nbsp;20,<ref name="Partagas"/> while located about {{convert|460|mi|km}} north-northwest of [[San Juan, Puerto Rico]]. The storm strengthened slowly while heading northward, until peaking with winds of 70&nbsp;mph (110&nbsp;km/h) on October&nbsp;21. The storm then began to re-curve northeastward. Early on October&nbsp;22, it passed near Bermuda,{{Atlantic hurricane best track}} though no impact was reported on the island.<ref name="Partagas"/> Several hours later, this system was last noted about 365&nbsp;miles (585&nbsp;km) east-northeast of Bermuda.{{Atlantic hurricane best track}}
{{clear}}

==References==
{{Reflist|2}}

{{TC Decades|Year=1850|basin=Atlantic|type=hurricane}}

{{DEFAULTSORT:1854 Atlantic Hurricane Season}}
[[Category:Atlantic hurricane seasons]]
[[Category:1850–1859 Atlantic hurricane seasons|*]]