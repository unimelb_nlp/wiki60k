{{Infobox album<!-- See Wikipedia:WikiProject_Albums -->
| Name       = 24: The Soundtrack
| Type       = [[Album]]
| Artist     = [[Sean Callery]]
| Cover      = 24 Soundtrack.jpg
| Released   = December 7, 2004
| Recorded   = 2001-2004
| Genre      = [[Electronic music]]
| Length     = 00:51:18
| Label      = [[Varèse Sarabande|Varése Sarabande]]
| Producer   = [[Sean Callery]]
| Reviews    = 
| Last album = ''[[La Femme Nikita (TV series)]]''<br />(2003)
| This album = ''24: The Soundtrack''<br />(2004)
| Next album = ''[[James Bond 007: Everything or Nothing]]'' <br />(2005)
}}

'''24: The Soundtrack''', released on December 7, 2004 in the USA, is based on the Fox television [[drama]] series [[24 (television)|''24'']],  contains nineteen tracks of music composed exclusively for the first three seasons by producer [[Sean Callery]], including the show's full theme song which has never been aired. The music contained in the soundtrack is somewhat of a hybrid mix of electronic pulses and rich orchestral textures that is meant to give each episode its own sound, yet at the same time have a sound that is consistent with the rest of the series in a way that compliments the show's "real-time" format. The insert of the album contains various photographs from the three seasons and also includes Callery's comments about how he went about producing each track.  The liner notes also list which specific episode each track comes from. The album was released by [[Varèse Sarabande]] in the USA, [[Virgin TV]] in the UK, and EMI in Japan.<ref>{{cite web|url=http://www.soundtrackcollector.com/title/68904/24 |title=24 - Soundtrack Collector listing}}</ref>

This album has been released with the [[Copy Control]] protection system in some regions.{{citation needed|date=April 2013}}

Callery had won three [[Primetime Emmy Award]]s for his score to ''24'' in 2003, 2006 and 2010.

==Track listing==
# "'24' Theme" – 4:41
# "Up and Down Stairs" – 2:44
# "L.A. at 9:00 a.m." – 1:57
# "[[Jack Bauer|Jack]] on the Move" – 2:19
# "Jack's Revenge at the Docks" – 4:02
# "[[Kim Bauer|Kim]] and [[Teri Bauer|Teri]]'s Escape from the Safe House" – 2:03
# "Jack in the Limo" – 2:41
# "In Pursuit of Kyle" – 2:39
# "[[Recurring and minor characters in 24#24: Season 3 2|Salazar]]'s Theme" – 1:54
# "'Copter Chase Over L.A." – 2:32
# "Jack Tells Kim He's Not Coming Back" – 2:12
# "The Bomb Detonates" – 2:38
# "Palmer's Theme" – 1:50
# "[[Alexis Drazen|Alexis]]" – 2:04
# "Coliseum Finale" – 1:56
# "Amnesia" – 2:14
# "Jack and Kim Trying to Reconnect" – 3:05
# "Season One Finale / Teri's Death" – 5:33
# "Season Three Finale / Jack's Humanity" – 2:14
# "Gunman For Breakfast" – 2:24 ''(Japanese edition only)''
# "Christiansands" (Performed by Tricky) – 3:53 ''(European and Japanese editions only)''
# "'24' Theme - The Longest Day (Armin Van Buuren Remix - Album Edit)" – 7:54 ''(European and Japanese editions only)''
# "CTU Intercom" – 0:04 ''(European and Japanese editions only)''
# "CTU Ring 1" – 0:04 ''(European and Japanese editions only)''
# "CTU Ring 2" – 0:04 ''(European and Japanese editions only)''

(Note that on Track 6 and Track 18, "Teri" is misspelled on the CD as "Terry".)

The Japanese and European editions include exclusive tracks.<ref>{{cite web|url=http://www.soundtrackcollector.com/title/68904/24 |title=24 - Soundtrack Collector listing}}</ref>

==Other soundtracks==
{{Infobox album<!-- See Wikipedia:WikiProject_Albums -->
| Name     = 24: Seasons Four and Five Soundtrack
| Type     = [[Album]]
| Artist   = [[Sean Callery]]
| Cover    = 24Soundtrack2.JPG
| Released = November 14, 2006
| Recorded = 2004-2006
| Genre    = [[Electronic music]]
| Length   = 01:10:00
| Label    = [[Varèse Sarabande|Varése Sarabande]]
| Producer = [[Sean Callery]]
| Reviews  = 
}}

===24: Seasons Four and Five Soundtrack===

'''24: Seasons Four and Five Soundtrack''' contains 21 songs from [[24 (season 4)|Season 4]], [[24 (season 5)|Season 5]], and [[24: The Game]]. It contains approximately 1 Hour and 10 Minutes of music. The Soundtrack was released on November 14, 2006. The orchestral selections from [[24: The Game]] were "recorded at Abbey Road Studios in London in the Summer of 2005." <!--quote from the packet in the CD quote by Sean Calery!-->

# "'24' Main Title" – 4:48
# "Collette's Arrest" – 2:51
# "Closing in on Marwan" – 4:27
# "Death in the Open Desert" – 1:42
# "Logan's Downfall" – 6:38
# "Mandy's Plan" – 2:55
# "Henderson" – 2:40
# "Jack's Women" – 3:36
# "C.T.U." – 3:55
# "The Name's O'Brian— Chloe O'Brian" – 2:31
# "Mission Briefing" – 2:12
# "Bierko Entering the Gas Company" – 2:36
# "Logan's Near Suicide" – 2:24
# "Lynn McGill's Sacrifice" – 3:50
# "Base Mission" – 2:36
# "Airport Russians" – 2:45
# "Infiltrating the Sub" – 5:59
# "Loft Mission" – 2:66
# "Reviving Jack" – 3:16
# "Jack Storms the Gas Plant" – 8:10
# "Tony's Farewell" – 1:36

(Note that the third track title, "Closing In On Marwan", is misspelled on the release as "Closing in on Marwon".)

=== 24: Redemption ===

'''24: Redemption''' is a soundtrack released by Varése Sarabande for the TV movie, [[24: Redemption]], which bridged the gap between [[24 (season 6)|Season 6]] and [[24 (season 7)|Season 7]]. It is the first time the music of Sean Callery has been presented in the form of a single self-contained work.

# "Prologue — Sangala" (04:07)
# "Across the Plains" (01:00)
# "Willie" (02:56)
# "Dubaku on the Hunt" (03:17)
# "Jack and Benton" (03:33)
# "Soccer Game Interrupted" (03:55)
# "Vultures" (02:51)
# "Paranoid Friend" (01:32)
# "Don’t Let Them Take My Kids" (04:44)
# "Tortured Jack "(03:56)
# "Evacuating the School" (03:09)
# "Anything at All" (01:44)
# "One Man Against Juma’s Army" (02:36)
# "Benton’s Sacrifice" (04:27)
# "Street Battle" (03:37)
# "Open the Gate" (04:22)
# "New President in a Troubled World" (02:52)

=== 24: The Game Original Game Score ===

'''24 Original Game Score'''  is a soundtrack of new songs that Sean Callery made exclusively for [[24: The Game]], which takes place between [[24 (season 2)|Season 2]] and [[24 (season 3)|Season 3]] although the game uses songs from the series. It was "live orchestred performing nearly 30 minutes of original music".<ref>'''Callery''': I completed the "24" video game, which included a live orchestra performing nearly 30 minutes of original music.[http://www.soundtrack.net/features/article/?id=182]</ref> However, unlike the original soundtrack, it was only offered as [[music download|digital download]].<ref>[http://www.soundtrack.net/news/article/?id=732 SoundtrackNet : News : Sean Callery's score to 24: The Game to be released as a digital-download<!-- Bot generated title -->]</ref>

# "'24' Symphonic Suite" – 4:47
# "Storming The Cargo Ship" – 3:03
# "Sean Is Shot" – 2:33
# "Jack At The Base Part 2" – 2:53
# "Kim Surrenders" – 2:14 
# "The Ship Is Taken" – 2:54 
# "CTU Takeover" – 2:55 
# "CTU Shootout" – 1:03

=== 24: The Longest Day/Remix Bundle ===

'''24 The Longest Day''' or officially as '''24: Remix Bundle''' is a custom soundtrack of the 24 theme song by international producer [[Armin Van Buuren]]. It contains three versions of the theme.  This CD was never released in the US.  However, there is the music video of this song included in the 24 Season 4 DVD set, and the song itself is available for download on [[iTunes]].

# "'24' Theme - The Longest Day (Armin Van Buuren Remix)" – 10:02
# "'24' Theme - The Longest Day (Armin Van Buuren Remix – Radio Edit)" – 3:26
# "'24' Theme - The Longest Day (Armin Van Buuren Remix – Album Edit)" – 8:11

===iTunes Exclusive Remixes===
Apple's iTunes offers three exclusive remixes on the 24 theme song. These remixes are exclusively for 24 Season Six season pass holders (purchasing all the episodes of the season in bulk). The theme song is remixed by [[The Crystal Method]], [[Benny Benassi]], and [[Towa Tei]].

==References==
{{Reflist}}

==External links==
*[http://www.seancallery.com Sean Callery's Official Website]

{{24 (TV series)}}

[[Category:24 (TV series)]]
[[Category:Industrial soundtracks]]