{{Use mdy dates|date=June 2013}}
{{Infobox short story | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = The Black Cat
| title_orig    =
| translator    =
| author        = [[Edgar Allan Poe]]
| country       = United States
| language      =English
| series        =
| genre         = [[Horror fiction]]<br />[[Short story]], [[Gothic Literature]]
| published_in  =
| publisher     = ''United States Saturday Post''<ref>{{cite book |first=Nina |last=Baym |title=The Norton Anthology of American Literature, 8th Edition, Volume B: 1820-1865 |publisher=[[W. W. Norton & Company|Norton]] |location=New York City |year=2012 |page=695}}</ref>''
| media_type    = Print ([[periodical]])

| preceded_by   =
| followed_by   =
}}

"'''The Black Cat'''" is a [[short story]] by [[Edgar Allan Poe]]. It was first published in the August 19, 1843, edition of ''[[The Saturday Evening Post]]''. It is a study of the psychology of guilt,  often paired in analysis with Poe's "[[The Tell-Tale Heart]]".<ref>{{cite book |first=Jeffrey |last=Meyers |title=Edgar Allan Poe: his life and legacy |publisher=[[Charles Scribner's Sons]] |location=New York City |year=1992 |page=137 |isbn=0-8154-1038-7 |oclc=44413785}}</ref> In both, a murderer carefully conceals his crime and believes himself unassailable, but eventually breaks down and reveals himself, impelled by a nagging reminder of his guilt.

== Plot ==
[[File:Aubrey Beardsley - Edgar Poe 2.jpg|right|thumb|Illustration for "The Black Cat" by [[Aubrey Beardsley]] (1894–1895)]]
The story is presented as a [[first-person narrative]] using an [[unreliable narrator]]. He is a condemned man at the outset of the story.<ref>Hart, James D. "[http://www.oxfordreference.com/views/ENTRY.html?subview=Main&entry=t53.e262 The Black Cat]". ''The Concise Oxford Companion to American Literature''. Oxford UP, 1986. ''Oxford Reference Online''. Accessed October 22, 2011.</ref> The narrator tells us that from an early age he has loved animals. He and his wife have many pets, including a large, beautiful black cat (as described by the narrator) named Pluto. This cat is especially fond of the narrator and vice versa. Their mutual friendship lasts for several years, until the narrator becomes an [[alcoholism|alcoholic]]. One night, after coming home completely intoxicated, he believes the cat to be avoiding him. When he tries to seize it, the panicked cat bites the narrator, and in a fit of rage, he seizes the animal, pulls a [[penknife|pen-knife]] from his pocket, and deliberately gouges out the cat's eye.

From that moment onward, the cat flees in terror at his master's approach. At first, the narrator is remorseful and regrets his cruelty. "But this feeling soon gave place to irritation. And then came, as if to my final and irrevocable overthrow, the spirit of perverseness."  He takes the cat out in the garden one morning and ties a noose around its neck, hanging it from a tree where it dies. That very night, his house mysteriously catches fire, forcing the narrator, his wife and their servant to flee the premises.

The next day, the narrator returns to the ruins of his home to find, imprinted on the single wall that survived the fire, the apparition of a gigantic cat, with a rope around the animal's neck.

At first, this image deeply disturbs the narrator, but gradually he determines a logical explanation for it, that someone outside had cut the cat from the tree and thrown the dead creature into the bedroom to wake him during the fire. The narrator begins to miss Pluto, feeling guilty. Some time later, he finds a similar cat in a tavern. It is the same size and color as the original and is even missing an eye. The only difference is a large white patch on the animal's chest. The narrator takes it home, but soon begins to loathe, even fear the creature. After a time, the white patch of fur begins to take shape and, to the narrator, forms the shape of the [[gallows]]. This terrifies and angers him more, and he avoids the cat whenever possible.
Then, one day when the narrator and his wife are visiting the cellar in their new home, the cat gets under its master's feet and nearly trips him down the stairs. Enraged, the man grabs an axe and tries to kill the cat but is stopped by his wife- whom, out of fury, he kills instead. To conceal her body he removes bricks from a protrusion in the wall, places her body there, and repairs the hole. A few days later, when the police show up at the house to investigate the wife's disappearance, they find nothing and the narrator goes free. The cat, which he intended to kill as well, has also gone missing. This grants him the freedom to sleep, even with the burden of murder.

On the last day of the investigation, the narrator accompanies the police into the cellar. They still find nothing significant. Then, completely confident in his own safety, the narrator comments on the sturdiness of the building and raps upon the wall he had built around his wife's body. A loud, inhuman wailing sound fills the room. The alarmed police tear down the wall and find the wife's corpse, and on its rotting head, to the utter horror of the narrator, is the screeching black cat.  As he words it: "I had walled the monster up within the tomb!"

== Publication history ==
"The Black Cat" was first published in the August 19, 1843, issue of ''[[The Saturday Evening Post]]''. At the time, the publication was using the temporary title ''United States Saturday Post''.<ref>{{cite book |first=Arthur Hobson |last=Quinn |title=Edgar Allan Poe: a critical biography |publisher=[[Johns Hopkins University Press]] |location=[[Baltimore]] |year=1998 |page=394 |isbn=0-8018-5730-9 |oclc=37300554}}</ref> Readers immediately responded favorably to the story, spawning parodies including [[Thomas Dunn English]]'s "The Ghost of the Grey Tadpole".<ref name=Sova28>{{cite book |first=Dawn B. |last=Sova |title=Edgar Allan Poe, A to Z: the essential reference to his life and work |publisher=[[Facts on File]] |location=New York City |year=2001 |page=28 |isbn=0-8160-4161-X |oclc=44885229}}</ref>

== Analysis ==
Like the narrator in Poe's "[[The Tell-Tale Heart]]", the narrator of "The Black Cat" has questionable sanity. Near the beginning of the tale, the narrator says he would be "mad indeed" if he should expect a reader to believe the story, implying that he has already been accused of madness.<ref>{{cite book |first=John |last=Cleman |chapter=Irresistible Impulses: Edgar Allan Poe and the Insanity Defense |editor=[[Harold Bloom]] |title=Edgar Allan Poe |publisher=Chelsea House Publishers |location=New York City |year=2002 |page=73 |isbn=0-7910-6173-6 |oclc=48176842}}</ref>

The extent to which the narrator claims to have loved his animals suggests mental instability in the form of having “too much of a good thing”. His partiality for animals substitutes “the paltry friendship and gossamer fidelity of mere Man”. Since the narrator’s wife shares his love of animals, he likely thinks of her as another pet, seeing as he distrusts and dislikes humans. Additionally, his failure to understand his excessive love of animals foreshadows his inability to explain his [[motivation|motives]] for his actions.<ref name=Gargano>Gargano, James W. "The Black Cat": Perverseness Reconsidered". ''Texas Studies in Literature and Language'' 2.2 (1960): 172-78.</ref>

One of Poe's darkest tales, "The Black Cat" includes his strongest denunciation of alcohol. The narrator's perverse actions are brought on by his [[alcoholism]], a "disease" and "fiend" which also destroys his personality.<ref>{{cite journal |first=L. Moffitt |last=Cecil |url=http://www.eapoe.org/pstudies/ps1970/p1972204.htm |title=Poe's Wine List |journal=Poe Studies |volume=V |issue=2 |date=December 1972 |page=42}}</ref> The use of the [[black cat]] evokes various [[superstition]]s, including the idea voiced by the narrator's wife that they are all [[witch]]es in disguise. Poe owned a black cat. In his "[[Instinct vs Reason -- A Black Cat]]" he stated: ''The writer of this article is the owner of one of the most remarkable black cats in the world - and this is saying much; for it will be remembered that black cats are all of them witches.''<ref>{{cite book|last=Barger|first=Andrew|title=Edgar Allan Poe Annotated and Illustrated Entire Stories and Poems|year=2008|publisher=Bottletree Books LLC|location=U.S.A.|isbn=978-1-933747-10-1|page=58}}</ref>  In Scottish and Irish mythology, the [[Cat Sìth]] is described as being a black cat with a white spot on its chest, not unlike the cat the narrator finds in the tavern. The titular cat is named Pluto after the [[Pluto (mythology)|Roman god]] of the [[Underworld]].

* The [[Doppelgänger]] – see also "[[William Wilson (short story)|William Wilson]]"
* Guilt – see also "[[The Tell-Tale Heart]]"

Although Pluto is a neutral character at the beginning of the story, he becomes [[antagonist]]ic in the narrator’s eyes once the narrator becomes an alcoholic. The alcohol pushes the narrator into fits of intemperance and violence, to the point at which everything angers him – Pluto in particular, who is always by his side, becomes the malevolent witch who haunts him even while avoiding his presence. When the narrator cuts Pluto’s eye from its socket, this can be seen as symbolic of self-inflicted partial blindness to his own vision of [[good and evil|moral goodness]].<ref name=Gargano/>

The fire that destroys the narrator’s house symbolizes the narrator’s "almost complete moral disintegration".<ref name=Gargano/> The only remainder is the impression of Pluto upon the wall, which represents his unforgivable and incorrigible sin.<ref name=Gargano/>

From a rhetorician's standpoint, an effective scheme of omission that Poe employs is [[zeugma|diazeugma]], or using many verbs for one subject; it omits pronouns. Diazeugma emphasizes actions and makes the narrative swift and brief.<ref>Zimmerman, Brett. ''Edgar Allan Poe: Rhetoric and Style''. Montreal: McGill-Queen's UP, 2005.</ref>

== Adaptations ==
*In 1910–11 Futurist artist [[Gino Severini]] painted "The Black Cat" in direct reference to Poe's short story. [[File:The Black Cat (Severini).jpg|thumb|The Black Cat, 1910–1911. A painting by [[Gino Severini]]]]
*Universal Pictures made two films titled ''The Black Cat'', one in [[The Black Cat (1934 film)|1934]], starring [[Bela Lugosi]] and [[Boris Karloff]], and another in [[The Black Cat (1941 film)|1941]], starring Lugosi and [[Basil Rathbone]]. Both films claimed to have been "suggested by" Poe's story, but neither bears any resemblance to the tale aside from the presence of a black cat.<ref name=Sova28/> Elements of Poe's story were, however, used in the 1934 film ''[[Maniac (1934 film)|Maniac]]''.<ref>{{cite web|author=J. Stuart Blackton |url=http://www.allmovie.com/work/maniac-31313 |title=Maniac - Cast, Reviews, Summary, and Awards - AllRovi |publisher=Allmovie.com |date= |accessdate=2014-04-28}}</ref>
*"The Black Cat" was adapted into a 7-page comic strip in Yellowjack Comics #1 (1944).
*Sept. 18, 1947, Mystery in the Air Radio Program with Peter Lorre as the Protagonist in The Black Cat. Note: eye is not gouged out. Instead the cat's ear is torn.
*The middle segment of director [[Roger Corman]]'s 1962 anthology film ''[[Tales of Terror]]'' combines the story of "The Black Cat" with that of another Poe tale, "[[The Cask of Amontillado]]."<ref name=Sova28/> This version stars [[Peter Lorre]] as the main character (given the name Montresor Herringbone) and [[Vincent Price]] as Fortunato Luchresi.
*In 1970, [[Czechs|Czech]] writer [[Ludvík Vaculík]] made many references to "[[A Descent into the Maelström]]" as well as "The Black Cat" in his novel ''[[The Guinea Pigs]]''.
*Writer/director [[Lucio Fulci]]'s 1981 film ''[[The Black Cat (1981 film)|The Black Cat]]'' is loosely based on Poe's tale.
*The 1990 film ''[[Two Evil Eyes]]'' presents two Poe tales, "[[The Facts in the Case of M. Valdemar]]" and "The Black Cat." The former was written and directed by [[George A. Romero]] while the latter was written and directed by [[Dario Argento]]. This version stars [[Harvey Keitel]] in the lead role.
*In 1997, a compilation of Poe's work was released on a double CD entitled ''[[Closed on Account of Rabies]]'', with various celebrities lending their voices to the tales. The Black Cat was read by [[avant-garde]] performer [[Diamanda Galás]].
*"The Black Cat" was adapted and performed with "The Cask of Amontillado" as ''Poe, Times Two: Twin tales of mystery, murder...and mortar''—a double-bill of short, one-man plays written and performed by Greg Oliver Bodine. First produced in NYC at Manhattan Theatre Source in 2007, and again at WorkShop Theater Company in 2011.  Part of the 2012 season at [[Cape May Stage]] in Cape May, NJ.
*"[[The Black Cat (Masters of Horror episode)|The Black Cat]]" is the eleventh episode of the second season (2007) of the television series ''[[Masters of Horror]]''. The plot essentially retells the short story in a semi-autobiographical manner, with Poe himself undergoing a series of events involving a black cat which he used to inspire the story of the same name.
*In 2011, Hyper Aware Theater Company produced "The Black Cat", one of several Poe stage adaptations written by [[Lance Tait]], as part of its “Gutterdrunk: The Poe Revisions” in New York City.<ref>http://www.timeout.com/newyork/theater/gutterdrunk-the-poe-revisions</ref>

== References ==
{{Reflist|2}}

== External links ==
{{Wikisource|The Black Cat}}
{{commons category|The Black Cat (Poe)}}
* [http://www.gutenberg.net/etext/2148 Project Gutenberg: The Works of Edgar Allan Poe, Volume 2]
* [http://www.eapoe.org/works/tales/blcatd.htm Complete Text at E A Poe Society of Baltimore]
* [http://poestories.com/text.php?file=blackcat Full text on PoeStories.com] with hyperlinked vocabulary words.
* [http://www.poedecoder.com/essays/blackcat The Poe Decoder: The Black Cat]
* {{librivox book | title=The Black Cat | author=Edgar Allan Poe}}
* [http://cybermuse.gallery.ca/cybermuse/search/artwork_e.jsp?mkey=3924 Illustration and description of Severini's painting].
* [http://www.love-poems-queen.com/scary-poems.html The Black Cat reading by Gerry Hay]

{{Edgar Allan Poe}}
{{The Black Cat}}

{{DEFAULTSORT:Black Cat}}
[[Category:1843 short stories]]
[[Category:Short stories by Edgar Allan Poe]]
[[Category:Horror short stories]]
[[Category:Fiction with unreliable narrators]]
[[Category:Fictional cats]]
[[Category:Works originally published in The Saturday Evening Post]]
[[Category:Short stories adapted into films]]