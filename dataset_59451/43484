<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = XF-103
  |image = Republic XF-103 in flight.jpg
  |caption =Artist's impression of the XF-103
}}{{Infobox Aircraft Type
  |type = Interceptor
  |manufacturer = [[Republic Aviation]]
  |designer =[[Alexander Kartveli]] and William O'Donnell
  |first flight =
  |introduced =
  |retired =
  |number built =
  |status = Canceled at mock-up stage
  |unit cost =
  |program cost=[[United States dollar|US$]]104 million for the program<ref name="knaack">Knaack, Marcelle Size. ''Encyclopedia of US Air Force Aircraft and Missile Systems: Volume 1 Post-World War II Fighters 1945-1973''. Washington, DC: Office of Air Force History, 1978. ISBN 0-912799-59-5.</ref>
  |variants with their own articles =
  |primary user =
  |more users = 
}}
|}

The '''Republic XF-103''' was an [[United States|American]] project to develop a powerful [[air-to-air missile|missile]]-armed [[interceptor aircraft]] capable of destroying [[Soviet Union|Soviet]] [[bomber]]s while flying at speeds as high as [[Mach number|Mach]] 3 (2,300&nbsp;mph; 3,700&nbsp;km/h). Despite a prolonged development, it never progressed past the [[mockup]] stage.

==Development==

In 1949, the [[USAF]] issued a request for an advanced [[supersonic aircraft|supersonic]] interceptor to equip the [[Air Defense Command]]. Known formally as Weapon System WS-201A, but better known informally as the [[1954 interceptor]], it called for a supersonic aircraft with all-weather capability, intercept [[radar]] and [[air-to-air missile]] armament. [[Republic Aviation Company|Republic]] was one of six companies to submit proposals. On 2 July 1951, three of the designs were selected for further development, Convair's scaled-up [[Convair XF-92|XF-92]] that evolved into the [[Convair F-102 Delta Dagger|F-102]], a Lockheed design that led to the [[Lockheed F-104 Starfighter|F-104]], and Republic's AP-57.  AP-57 was an advanced concept to be built almost entirely of [[titanium]] and capable of [[Mach number|Mach]]&nbsp;3 at altitudes of at least {{convert|60000|feet|km}}.

A full-scale mock-up of the AP-57 was built and inspected in March 1953. A contract for three prototypes followed in June 1954.<ref name=joe /> Work on the prototypes was delayed by continued problems with the titanium construction, and more by continuing problems with the proposed [[Wright J67]] engine. The contract was later reduced to a single prototype.<ref name=joe /> In the end, the J67 never entered production and the aircraft it had been chosen for were forced to turn to other engine designs, or were cancelled outright. Republic suggested replacing the J67 with the [[Wright J65]], a much less powerful engine. The project was eventually cancelled on 21 August 1957 with no flying prototypes ever being completed.<ref name=joe />

The design was given a brief reprieve as part of the Long-Range Interceptor - Experimental (LRI-X) project that ultimately led to the [[North American XF-108 Rapier]]. Part of this project was the development of the advanced [[Hughes AN/ASG-18]] [[pulse-doppler radar]] and the [[AIM-47 Falcon|GAR-9]] missile. Republic proposed adapting the F-103 as a testbed for these systems, although it wouldn't be able to come close to meeting the range requirements of LRI-X. Some work was carried out adapting the mockup to house the 40&nbsp;inch antenna, which required the nose section to be scaled up considerably. Nothing ever came of the proposal,<ref name=val/> and testing of the ASG-18/GAR-9 was carried out on a modified [[Convair B-58 Hustler]] instead.<ref>Crickmore 2004, p. 87.</ref>

==Design==
[[File:Republic XF-103 mock-up.jpg|thumb|A mock-up of the F-103 was built at the Republic factory. In this image, the pilot's capsule is shown in its lowered position.]]

===Propulsion===
Mach&nbsp;3 performance in the 1950s was difficult to achieve. Jet engines compress incoming air, then mix it with fuel and ignite the mixture. The resulting expansion of gases produces thrust. The compressors generally can ingest air only at subsonic speeds. To operate supersonically, aircraft use advanced [[intake]]s to slow the supersonic air to a usable speed. The energy lost in this process heats the air, which means the engine has to operate at ever-higher temperatures to provide net thrust. The limiting factor in this process is the temperature of the materials in the engines, in particular, the turbine blades just behind the combustion chambers. Using materials available at the time, speeds much beyond Mach&nbsp;2.5 were difficult to achieve.

The solution to this problem is the removal of the turbine. The [[ramjet]] engine consists mostly of a large tube, and is relatively easy to air-cool by forcing extra air around the engine. Experimental ramjet aircraft of the era, like the [[Lockheed X-7]], were reaching speeds as high as Mach&nbsp;4. There are numerous problems with the ramjet engine, however. Fuel economy, or [[thrust specific fuel consumption]] in aircraft terms, is extremely poor. This makes general operations like flying from one airbase to another expensive propositions. More problematic is the fact that ramjets rely on forward speed to compress the incoming air, and become efficient above Mach&nbsp;1.

[[Alexander Kartveli]], Republic's Chief Designer, devised a solution to these problems. He proposed using a [[Wright J67]] [[turbojet]] (a license-built derivative of the [[Bristol Olympus]]) supplemented by an RJ55-W-1 [[ramjet]] behind it. Connecting the two were a series of movable ducts that could route air between the engines. At low speeds the aircraft would be powered by the J67, with the RJ55 acting as a traditional afterburner, producing a total of about 40,000&nbsp;lbf (180&nbsp;kN) thrust. At high speeds, starting above [[Mach number|Mach 2.2]], the jet engine would be shut down and the airflow from the intake would be routed around the jet engine and directly into the RJ55. Although the net thrust was reduced by shutting down the jet, operating on the ramjet alone allowed the aircraft to reach much higher speeds.

Both engines were located behind a single very large ventral single-sided Ferri intake, which featured a prominent, swept-forward lip, a design feature employed on the [[Republic F-84F Thunderstreak|Republic RF-84F Thunderstreak]] and later [[Republic F-105 Thunderchief|F-105 Thunderchief]]. The J67 was installed just behind the intake, angled with its intake below the centerline of the aircraft. The XJ55 was installed inline with the fuselage in the extreme rear, as if it were the exhaust of a conventional engine installation. There was a significant empty space above the J67 for ducting.

===Wings and control surfaces===
All of the control surfaces were pure [[delta wing]]s. The main wing was swept at 55&nbsp;degrees, and could be rotated around the spar to provide variable incidence. For takeoff and landing, the wing was tilted upwards to increase the [[angle of attack]] while keeping the fuselage nearly horizontal. The length of the fuselage made it difficult to achieve the same end by tilting the entire aircraft upwards, which would have required a very long extension on the [[landing gear]]. The system also allowed the fuselage to fly flat to the airflow at various speeds, setting the [[angle of incidence (aerodynamics)|trim angle]] independent of the aircraft as a whole. This decreased trim drag, thus improving range.

The wing was split at about two-thirds of the span. The portion outside of this line able to rotate independently of the rest of the wing. These movable portions acted as large [[aileron]]s, or as Republic called them, ''tiperons''. To keep the surface area in front and behind the pivot point somewhat similar, the split line was closer to the fuselage in front of the pivot. Large conventional flaps ran from the fuselage to the tiperons. Hard points for [[drop tank]]s were available at about {{frac|3}} of the way out from the wing root.

The horizontal stabilizers were seemingly undersized, and mounted below the line of the wing. The larger vertical fin was supplemented by a ventral fin for high-speed stability. This fin folded to the right, as seen from behind, during takeoff and landing to avoid hitting the ground. Two petal-style air brakes were mounted directly behind the horizontal surfaces, opening out and up at about a 45° angle into the gap between the horizontal and vertical surfaces. A provision for a braking parachute is not evident on the mock-up or the various artwork, although this was common for aircraft of the era.

===Fuselage===
The fuselage was completely smooth, with a high [[fineness ratio]] for low drag at supersonic speeds. The design was developed prior to the discovery of the [[area rule]], and does not display any of the wasp waisting common to aircraft primarily developed after 1952. The fuselage contours were mainly cylindrical, but blended into the intake starting around the wing root, giving it a rounded, rectangular profile through the middle, before reverting to a pure cylinder shape again at the engine nozzle.

===Cockpit===
The cockpit design originally featured a canopy, but low drag requirements for high speed suggested that it be removed. The idea of using a periscope arrangement for forward viewing on high speed aircraft was then in vogue, the [[Avro 730]] selecting a very similar system. The Air Force demanded that it be used on the F-103. Kartveli was opposed to this layout, and continued to press for the use of a "real" canopy. Design documents throughout the program continued to include this as an optional feature, along with performance estimates that suggested the difference would be minimal.<ref name=val>Jenkins and Landis 2004</ref>

The system shown on mockups used two large oval windows on the cockpit sides, and a periscope system projecting an image onto a [[fresnel lens]] arrangement directly in front of the pilot. In 1955, the periscope concept was tested on a modified [[Republic F-84 Thunderjet|F-84G]], which was flown on a long, cross-country flight with the pilot's forward vision blocked.<ref name=joe>Baugher, Joe. [http://www.joebaugher.com/usaf_fighters/f103.html "Republic XF-103."] ''Joe Baugher's Encyclopedia of American Military Aircraft'', 4 December 1999. Retrieved: 16 February 2011.</ref>{{#tag:ref|The F-84G testbed (s/n 51-843) accumulated nearly 50 hours of flight testing with pilots reporting the periscope system worked extremely well.<ref>Jenkins 2004, p. 23.</ref>|group=N}}

A unique supersonic escape capsule was designed for the XF-103. The pilot's seat was located in a shell with a large movable shield in front that was normally slid down into the area in front of the pilot's legs. In the case of depressurization, the shield would slide up in front of the pilot, sealing the seat into a pressurized pod. Basic flight instruments inside the capsule allowed the aircraft to be flown back to base, and a window in the front of the shield allowed the periscope system to be used. In an emergency, the entire capsule would be ejected downward, along with a small portion of the aircraft fuselage that provided a stable aerodynamic shape. To enter and exit the aircraft, the ejection module was lowered on rails out of the bottom of the aircraft, allowing the pilot to simply walk into the seat, sit down, and raise the module into the aircraft. The capsule was fully pressurized, allowing the pilot to continue operating the aircraft without a pressure suit when the capsule was locked up.<ref>Pace 1991, p. 128.</ref>

===Avionics and armament===
The entire nose of the aircraft was taken up by the large Hughes radar set, which (at the time) offered long detection ranges. Guidance and fire control were to be provided by the same MX-1179 package being developed for all of the WS-201 designs. Hughes had won this contract with their Hughes MA-1 fire control system, which was under development. Weapons were carried in bays located on the sides of the fuselage behind the cockpit, which opened by flipping upward, thereby rotating the missiles out of their bays. It was to be armed with six [[AIM-4 Falcon|GAR-1/GAR-3 Falcon]] (then known as MX-904), with a likely arrangement of three or four each GAR-1s and GAR-3s, fired in pairs (one each radar and infrared guided) to improve the odds of a hit. The XF-103 also was to feature 36 2.75-inch "Mighty Mouse" [[Mk 4/Mk 40 Folding-Fin Aerial Rocket|FFAR]]s.

==Specifications (XF-103, as designed)==
{{aircraft specifications
|plane or copter?=plane
|jet or prop?=both
|ref=
|crew= one pilot
|capacity=
|payload main=
|payload alt=
|length main= 77 ft 
|length alt= 23.5 m
|span main= 34 ft 5 in 
|span alt= 10.5 m
|height main= 16 ft 7 in 
|height alt= 5.1 m
|area main= 401 ft² 
|area alt= 37.2 m²
|airfoil=
|empty weight main= 24,949 lb 
|empty weight alt= 11,317 kg
|loaded weight main= 38,505 lb 
|loaded weight alt= 17,466 kg
|useful load main= 
|useful load alt= 
|max takeoff weight main= 42,864 lb 
|max takeoff weight alt= 19,443 kg
|more general=
|engine (jet)= [[Wright J67|Wright XJ67]]-W-3
|type of jet= [[turbojet]]
|number of jets=1
|thrust main= 15,000 lbf
|thrust alt= 66.7 kN
|thrust original=
|afterburning thrust main=
|afterburning thrust alt= 
|engine (prop)= Wright XRJ55-W-1
|type of prop=[[ramjet]]
|number of props=1
|power main= 18,800 lbf
|power alt= 83.6 kN
|power original=
|max speed main=Mach 3 (as a turbojet) / Mach 5 (ramjet-only)
|max speed alt= 
|cruise speed main= 
|cruise speed alt=
|stall speed main= 
|stall speed alt= 
|never exceed speed main= 
|never exceed speed alt= 
|range main= 
|range alt=
|ceiling main= 80,000 ft +
|ceiling alt= 24,390 m +
|climb rate main= 19,000 ft/min 
|climb rate alt= 5,800 m/min
|loading main=96 lb/ft² 
|loading alt=470 kg/m²
|thrust/weight=0.57:1 (afterburner only); 0.95:1 (afterburner and ramjet)
|power/mass main=
|power/mass alt=
|more performance=
* '''Combat radius:''' 245 mi (394 km)
* '''Ferry range:''' 1,545 mi (2,486 km)
|armament=
* 36 2.75 in (70 mm) [[Mk 4/Mk 40 Folding-Fin Aerial Rocket|FFAR]] rockets
'''and'''
* 6 GAR-1/GAR-3 [[AIM-4 Falcon]]
'''or''' 
* 4 GAR-1/GAR-3 [[AIM-4 Falcon]] missiles
* 2 Nuclear-Tipped [[air-to-air missile]]s
|avionics=
}}

==See also==
{{Portal|Military of the United States|Aviation}}
{{aircontent|
|related=
|similar aircraft=
* [[North American XF-108 Rapier]]
|lists=
|see also=
* [[Century Series]]
}}

==References==
;Notes
{{reflist|group=N}}
;Citations
{{reflist}}
;Bibliography
{{refbegin}}
* Crickmore, Paul, [http://books.google.ca/books?id=xwPFC3GtcL8C&pg=PA87 ''Lockheed Blackbird: Beyond the Secret Missions.''] Oxford, UK: Osprey, 2004. ISBN 978-1-84176-694-2.
* Jenkins, Dennis R. "Titanium Titan: The Story of the XF-103." ''Airpower'', January 2004.
* Jenkins, Dennis R. and Tony R. Landis. ''Experimental & Prototype U.S. Air Force Jet Fighters.'' Minnesota: Specialty Press, 2008. ISBN 978-1-58007-111-6.
* Jenkins, Dennis R. and Tony R. Landis. ''Valkyrie: North American's Mach 3 Superbomber''. North Branch, Minnesota: Specialty Press Publishers & Wholesalers, 2004. ISBN 1-58007-072-8.
* Pace, Steve. ''X-Fighters: USAF Experimental and Prototype Fighters, XP-59 to YF-23''. St. Paul, Minnesota: Motorbooks International, 1991. ISBN 0-87938-540-5.
{{refend}}

==External links==
{{commons category|Republic XF-103}}
* [http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?id=2377 USAF Museum: XF-103]

{{Republic aircraft}}
{{USAF fighters}}

[[Category:Ramjet-powered aircraft]]
[[Category:Cancelled military aircraft projects of the United States]]
[[Category:Republic aircraft|F-103]]
[[Category:Mixed-power aircraft]]
[[Category:Monoplanes]]