{{italic title}}
{{Taxobox 
| image              = Canegrass dragon 1.jpg
| image_caption      = Canegrass dragon
| regnum = [[Animal]]ia
| phylum = [[Chordate|Chordata]]
| subphylum = [[Vertebrate|Vertebrata]]
| classis = [[Reptile|Reptilia]]
| ordo = [[Squamata]]
| subordo = [[Iguania]]
| familia = [[Agamidae]]
| subfamilia = [[Amphibolurinae]]
| genus = ''[[Diporiphora]]''
| species = '''''D. winneckei''''' 
| binomial = '''''Diporiphora winneckei'''''
| binomial_authority = [[Arthur Henry Shakespeare Lucas|Lucas]] & Frost, 1896
}}

'''''Diporiphora winneckei''''' (the '''canegrass dragon''', '''blue-lined dragon''' or '''Winnecke's two-pored dragon''') is a small, terrestrial, [[Diurnality|diurnal]] (active during the day)<ref name=carearticle/> lizard [[endemic]] to Australia.<ref name=RDB>{{NRDB species|genus=Diporiphora|species=winneckei|accessdate=5 October 2014}}</ref> It is found in throughout arid zones of Australia and is also a common house pet.<ref name=carearticle>Van Der Reijden , J. (2014, August 18). The captive maintenance and breeding of ''Diporiphora winneckei'' (Canegrass dragon) at the Alice Springs Desert Park. Retrieved from Australasian society of zoo keeping inc: http://www.aszk.org.au/docs/cane_grass_dragon.pdf</ref>

==Description==
''Diporiphora winneckei'' is a slim lizard with a long tail and limbs, approximately {{convert|15|-|24|cm|abbr=on}} in length.<ref name=snakes>Swan, G. (1995). Canegrass Two-line Dragon. In G. Swan, A photographic Guide to Snakes & other reptiles of Australia (p. 107). Frenchs Forest: NewHolland Publishers.</ref> It is a grey or pale brown to reddish-brown colour, with a thick grey ventral band that runs from the nape to the tail.<ref name=snakes/> It also has creamy-yellow (female) or silver grey (male) stripes that run down each side of its back, converging at the pelvis. Dark angular blotches lie between the stripe and the ventral band of female, while males have fewer or no blotches visible.<ref name=reptiles>Wilson, S., & Swan, G. (2013). Canegrass Dragon; Blue-lined Dragon. In S. Wilson, & G. Swan, A complete guide to reptiles of Australia Fourth Edition (p. 406). Chatswood: New Holland Publishers.</ref> The dragon also has a creamy mid lateral stripe that runs from eye to ear on each side. The dragons have homogenous scales as well as weak gular and postauricular folds. They lack the spines and crest that many other dragons display.<ref name=old>Cogger, H. G. (1994). ''Diporiphora winneckei''. In H. G. Cogger, Reptiles and Amphibians of Australia (p. 336). Chatswood: Imago Productions.</ref>

==Taxonomy==
Canegrass dragons were first officially described in 1896 by [[Arthur Henry Shakespeare Lucas|Lucas]] and Frost.<ref name=carearticle/> Their common name originates from the fact that they are often found in [[canegrass]]. Their scientific name, ''Diporiphora winneckei'', suggests they have two visible pores, however the pores are not visible on this species.<ref name=old/> The closest species to the canegrass dragon is Amelia’s dragon (''[[Diporiphora ameliae]]'') which was split from the canegrass dragon as a new species in 2012. Amelia’s dragon has four longitudinal ventral stripes when compared to the canegrass dragons two ventral stripes, as well as having postauricular fold and spines, which the canegrass dragon lacks.<ref name=carearticle/>

[[File:Distribution of Canegrass dragon.png|thumb|Distribution of Canegrass dragon]]

==Distribution and habitat==
The canegrass dragon is an [[endemic]] species found in the arid zones of Australia, with a rainfall of less than 500mm.<ref name=snakes/> It’s predominantly found in the region surrounding the junction of the Northern Territory, Queensland, New South Wales and South Australian borders. However it also occupies habitat across Western Australia as far as the northern West Australian coastline.  The dragon is usually found on desert sand ridges or other sandy soils in amongst ground litter, cane grass, spinifex or hummock grasses.<ref name=reptiles/> The dragon is rarely sighted, despite not being rare, due to its excellent camouflage and secretive behaviour.<ref name=carearticle/>

==Reproduction==
Canegrass dragons reach sexual maturity at around one year old. During the breeding season, the male will attract a female by repeated head bobs, which displays his willingness to mate. If the female is not ready she will run away, however if she is, she will lower her head several times to show she is interested. The breeding season for the dragons is from October to April and in that time the female can lay between four and six clutches. After fertilization, the eggs will develop in approximately two to three weeks. The dragons are oviparous, and will lay between one and three eggs per clutch in a small nest in moist sandy soil.<ref name=carearticle/>

==Diet==
Canegrass dragons feed during the days and are insectivorous. However they have been found on some occasions to consume leafy greens and flowers.<ref name=carearticle/>

==Threats==
Although the canegrass dragon is not currently listed as a conservation concern, it is still subject to those threats that threaten all Australian reptiles. These threats include introduced diseases, death on roads, habitat modification, degradation and loss and feral predators such as foxes, dogs, cats and even [[cane toad]]s.<ref>Macdonald, S. (2014, August 18). Canegrass dragon. Retrieved from Australian Reptile Online Database: http://www.arod.com.au/arod/reptilia/Squamata/Agamidae/Diporiphora/winneckei</ref>

==References==
{{reflist}}

[[Category:Agamidae]]
[[Category:Reptiles of Australia]]