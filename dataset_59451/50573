{{Infobox journal
| italic title = force
| title = Arbeit - Bewegung - Geschichte: Zeitschrift für historische Studien
| cover = 
| editor = [[Christa Hübner]], [[Ralf Hoffrogge]], Bärbel Kontny, [[Diemtar Lange]], [[Elke Scherstjanoi]], [[Axel Weipert]]
| discipline = [[labor history (discipline)|labour history]]
| language = German
| formernames = 
| abbreviation = 
| publisher = Metropol Verlag
| country = Germany
| frequency = Triannually
| history = 2002-present
| openaccess = 
| license = 
| impact = 
| impact-year =
| website = http://www.arbeiterbewegung-jahrbuch.de/jahrbuch.html
| link1 = http://www.arbeiterbewegung-jahrbuch.de/hefte.html
| link1-name = Online tables of content
| link2 = 
| link2-name =
| JSTOR = 
| OCLC = 49930935
| LCCN = 
| CODEN = 
| ISSN = 2366-2387
| eISSN = 
}}
'''''Arbeit - Bewegung - Geschichte''''' ("''Labour - Movement - History''") is a [[academic journal]] covering the history of [[Labour movement|labour]] and other social movements. It was established in 2002 as '''''Jahrbuch für Forschungen zur Geschichte der Arbeiterbewegung''''' ("''Yearbook on Labour History''") and renamed in 2016.

Each issue has a main section of historical [[essay]]s dealing with a variety of subjects such as the history of women's liberation, social movements in general or the [[antifascist]] resistance movements in Germany and Europe.<ref>{{cite web|url=http://hsozkult.geschichte.hu-berlin.de/zeitschriften/id=124 |title=Jahrbuch für Forschungen zur Geschichte der Arbeiterbewegung - H-Soz-u-Kult / Zeitschriften |publisher=Hsozkult.geschichte.hu-berlin.de |date=2011-12-02 |accessdate=2012-10-04}}</ref> Its main focus nevertheless is the history of the international labour and [[union movement]]s, including organizations such as the [[Comintern]] and its member parties as well as social-democratic parties.

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.arbeiterbewegung-jahrbuch.de/}}
* [http://www.dielinke-bremen.de/nc/politik/buecherkultur/detail/zurueck/buecherkultur/artikel/gesellschaftliche-entwicklungen-arbeiterbewegung-und-sozialismus-zu-beginn-des-21-jahrhunderts-ko/ Review] by Andreas Diers
* [http://www.neues-deutschland.de/artikel/167972.verpasste-chancen.html Review] in the German daily newspaper ''[[Neues Deutschland]]''

{{DEFAULTSORT:Arbeit - Bewegung - Geschichte}}
[[Category:Labour journals]]
[[Category:Triannual journals]]
[[Category:German-language journals]]
[[Category:Publications established in 2002]]
[[Category:2002 establishments in Germany]]