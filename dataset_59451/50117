{{Use dmy dates|date=May 2014}}
{{Neopaganism-sidebar}}
'''Abkhaz Neopaganism''', or the '''Abkhaz native religion''', is the [[Neopaganism|contemporary re-emergence]] of the [[ethnic religion]] of the [[Abkhaz people]] in [[Abkhazia]], a revitalisation which started in the 1980s.<ref name = "Schnirelmann202">Schnirelmann, p. 202.</ref> The most important holy sites of the religion are the [[Seven Shrines of Abkhazia]], each one having its own priestly clan, where rituals and prayers began to be solemnly restored from the 1990s onwards.

According to the 2003 census, 8% of the population of Abkhazia adheres to Abkhaz Paganism.<ref>{{Citation | url = http://www.portal-credo.ru/site/print.php?act=fresh&id=188 | first = Александр [Alexande] | last = Крылов [Krylov] | script-title=ru:Единая Вера Абхазских "Христиан" и "Мусульман". Особенности религиозного сознания в современной Абхазии | trans_title = Of United Vera Abhazskyh "Christians" & "Muslims." Features of religious consciousness in Modern Abkhazia | publisher = Portal-credo | place = [[Russia|RU]] | date = 17 March 2004 | accessdate = 30 May 2011| language = Russian }}.</ref> On 3 August 2012 the [[Council of Priests of Abkhazia]] was formally constituted in [[Sukhumi]].<ref>{{Citation | url = http://apsnypress.info/news/6898.html | title = В Абхазии создана религиозная организация "Совет жрецов Абхазии" | trans_title = In Abkhazia creatures Religious Organization "Tip zhretsov Abkhazia" | publisher = [[Apsnypress]]}}.</ref> The possibility of making the Abkhaz native religion one of the state religions was discussed in the following months.<ref>{{Citation | url = http://www.newsland.ru/news/detail/id/1014509 | title = Язычество в Абхазии не станет государственной религией | trans_title = Paganism in Abkhazia will be a state religion | place = RU | publisher = Newsland | date = 12 August 2012 | accessdate = 24 September 2012}}.</ref>

==History==
The traditional Abkhaz religion was actually never completely wiped out; circles of priests, whose activity was kept secret,<ref>Krylov, 1999</ref> passed on traditional knowledge and rites in the times when Christianity and [[Islam]] became dominant in the region, and later in Soviet times of anti-religion.<ref name = "Schnirelmann202" />

Since the 1980s, and later in the 1990s after the collapse of the Soviet Union, the Abkhaz native religion was resurrected by the joint efforts of priests who began to resurface, rural people reactivating local rituals, and urban intellectuals supporting Paganism as an integral part for a reawakening of the Abkhaz [[ethnic]] and cultural identity.<ref name="Schnirelmann202"/><ref>Filatov & Shipkov, 1996</ref>

A turning point for the revival of the Abkhaz native religion came with the [[Georgian–Abkhazian conflict]].<ref name = "Schnirelmann205">Schnirelmann, p. 205.</ref> With tensions growing, more and more Abkhazians began associating [[Orthodox Christianity]] with the [[Georgians]], and chose to reject it, turning to the native gods.<ref name = "Schnirelmann205" /> The eventual victory of [[Abkhazia]] in the [[war in Abkhazia (1992–93)|1992–93 war with Georgia]] catalysed the Neopagan revival. Many Abkhaz believe that their [[national god]] [[Dydrypsh]] awarded them the victory.<ref name="Schnirelmann206">Schnirelmann, p. 206.</ref>

Since then the Abkhaz native religion has been protected by Abkhaz authorities.  Government officials took part in a bull sacrifice in October 1993 celebrated to thank the Lord Dydrypsh for the victory over the Georgians, and since then they regularly take part in worship rituals.<ref name="Schnirelmann206"/><ref>Krylov, 1998a: 24–26; 1998b</ref>

==See also==
* [[Council of Priests of Abkhazia]]
* [[Seven Shrines of Abkhazia]]

;[[Caucasian Neopaganism|Caucasian religions]]
* [[Adyghe Habzism]]

;Indo-European religions
* [[Etseg Din]]
* [[Rodnovery]]

;Turkic religions
* [[Tengrism]]
* [[Vattisen Yaly]]

;[[Uralic Neopaganism|Uralic religions]]
* [[Estonian Neopaganism]]
* [[Finnish Neopaganism]]
* [[Mari native religion]]
* [[Mordvin native religion]]
* [[Udmurt Vos]]

==References==
{{reflist|30em}}

==Bibliography==
* {{Citation | last1 = Filatov | first1 = S | last2 = Shchipkov | first2 = A | language = Russian | contribution = Поволжские народы в поисках национальной веры (Povolzhskie narody v poiskakh natsional'noi very) [Volga region peoples in search of national faith] | editor-last = Filatov | editor-first = SB | title = Религия и права человека: На пути к свободе совести (Religiia i prava cheloveka: Na puti k svobode sovesti) | trans_title = Religion and Human Rights: Towards Freedom of Conscience | place = Moscow | publisher = Nauka | year = 1996 | pages = 256–84}}.
* {{Citation | last = Krylov | first = AB | title = Абхазское святилище Дыдрыпш: прошлое, настоящее и устная традиция (Abkhazskoe sviatilishche Dydrypsh: proshloe, nastoiashchee i ustnaia traditsiia) | language = Russian | trans_title = Abkhaz sanctuary Dydrypsh: Past, Present and oral tradition | journal = Этнограрусское обозрение (Etnogra cheskoe obozrenie) [Ethnography review] | volume = 6 | year = 1998a | pages = 16–28}}.
* {{Citation | last = Krylov | first = AB | author-mask = 3 | title = Дыдрыпш-ныха: святилище Абхазов (Dydrypsh-nykha: sviatilishche abkhazov) | language = Russian | trans_title = Dydrypsh-nykha: sanctuary Abkhazes | journal = Азия и Африка сегодня (Aziia i Afrika segodnia) [Asia & Africa today] | volume = 6 | year = 1998b | pages = 55–58}} & 7, 1998 b: 54–56.
* {{Citation | last = Krylov | first = AB | author-mask = 3 | title = Абхазия: возрождение святилища (Abkhazia: vozrozhdenie sviatilishcha) | language = Russian | trans_title = Abkhazia: the revival of the Sanctuary | journal = Азия и Африка сегодня (Aziia i Afrika segodnia) [Asia & Africa today] | volume = 4 | year = 1999 | pages = 70–72}}.
* {{Citation | last = Schnirelmann | first = Victor | url = http://www.wlu.ca/documents/6483/Christians_Go_home.pdf | title = "Christians! Go home": A Revival of Neo-Paganism between the Baltic Sea and Transcaucasia | journal = Journal of Contemporary Religion | volume = 17 | number = 2 | year = 2002 | place = [[Canada|CA]] | publisher = WLU}}.

==External links==
* {{Citation | url = http://www.russiaotherpointsofview.com/2010/03/abkhazia.html | title = Abkhazia versus Georgia: Implications for US Policy toward Russia | publisher = Russia: other points of view |date=Mar 2010 }}.
* {{Citation | url = http://www.eurasianet.org/node/64180 | title = Abkhazia: Breakaway Church Confronts Its Own Breakaway Bid | publisher = Eurasia net}}

{{Neopaganism}}

==Further reading==
* {{Citation | author = George Hewitt |title = The Abkhazians | publisher = Routledge |date = 2013}}.

[[Category:Caucasian Neopaganism]]
[[Category:Neopaganism in Europe]]
[[Category:Religion in Abkhazia]]
[[Category:Abkhazian people]]
[[Category:Religious nationalism]]