{{featured article}}
{{Infobox pro wrestling championship
| championshipname=CMLL World Middleweight Championship
| image =LuchaLibreObrera153.JPG
| alt=A Mexican professional wrestler during an outdoor wrestling event
| image_size =
| caption =[[Negro Casas]], the 12th and 15th champion<ref name=2004YIR/><ref name=CasasMiddle>{{cite web|url=http://superluchas.com/2010/02/15/negro-casas-nuevo-campeon-mundial-medio-del-cmll/|title=Negro Casas nuevo campeón Mundial Medio del CMLL|author=Ruiz Glez, Alex|date=February 15, 2010|accessdate=February 16, 2010|work=SuperLuchas|language=Spanish}}</ref>
| currentholder=[[Ángel de Oro]]
| won=March 25, 2017<ref name=AngelDeOroMiddle/>
| promotion=[[Consejo Mundial de Lucha Libre]]
| created=December 18, 1991<ref name=CMLLMiddleBook/>
| mostreigns=[[El Dandy]] ([[List of CMLL World Middleweight Champions#Title history|3 times]])<ref name=CMLLMiddleBook/>
| firstchamp=[[Blue Panther]]<ref name=CMLLMiddleBook/>
| longestreign=[[Dragón Rojo Jr.]] ({{age in years and days|November 18, 2011|March 25, 2017}} days)<ref name=DragonRojoJr/><ref name=WVDragonRojoJr>{{cite web | url=http://www.wrestleview.com/featured-articles/42323-feature-viva-la-raza-lucha-weekly-for-6-29-13/ | title=Feature: Viva la Raza!: Lucha Weekly for 6/29/2012 | date=June 29, 2013 | accessdate=August 24, 2016 | first=Josh | last=Boutwell | publisher= Wrestleview.com | quote=CMLL World Middleweight Champion: Dragon Rojo Jr. (since November 18, 2011)}}</ref><ref name=AngelDeOroMiddle/>
| shortestreign=El Dandy ({{age in years and days|July 3, 1992|September 4, 1992}})
| lightest=[[Apolo Dantés]] {{nobr|({{convert|92|kg|lb|abbr=on}})}}<!--In Mexico they measure weight in kilograms so that should be first -->
| heaviest=[[Averno (wrestler)|Averno]] {{nobr|({{convert|105|kg|lb|abbr=on}})}}<!--In Mexico they measure weight in kilograms so that should be first -->
| youngest=[[El Hijo del Fantasma]] ({{age in years and days|April 30, 1984|July 21, 2009}})
| oldest=[[Ringo Mendoza]] ({{age in years and days|September 19, 1949|March 7, 1999}})
}}
The '''CMLL World Middleweight Championship''' ([[Spanish language|Spanish]]: ''Campeonato Mundial de Peso Medio del CMLL'') is a [[professional wrestling]] [[World Heavyweight Championship (professional wrestling)#Nomenclature|world championship]] promoted by the Mexican wrestling promotion [[Consejo Mundial de Lucha Libre]] (CMLL). While lighter [[professional wrestling weight classes|weight classes]] are regularly ignored in wrestling promotions in the [[United States]], with most emphasis placed on "heavyweights", more emphasis is placed on the lighter classes in [[Mexico|Mexican]] companies. The official definition of the [[Professional wrestling weight classes#Mexico|middleweight division]] in Mexico is a person between {{convert|82|kg|lb|abbr=on}} and {{convert|87|kg|lb|abbr=on}}, but the weight limits are not strictly adhered to. As it is a professional wrestling championship, it is not won via legitimate competition; it is instead won via a scripted ending to a match or on occasion awarded to a wrestler because of a storyline.<ref>{{cite book|last1=Mazer|first1=Sharon|title=Professional Wrestling: Sport and Spectacle|date=February 1, 1998|publisher=University Press of Mississippi|isbn=1-57806-021-4|pages=18–19|quote=page 18: http://www.webcitation.org/6iNSeiNUp / page 19: http://www.webcitation.org/6iNS9iCe0|url=https://books.google.com/books?id=elWutuNtfUMC|accessdate=June 19, 2016}}</ref>

[[Ángel de Oro]] is the current CMLL World Middleweight Champion, having defeated [[Dragón Rojo Jr.]] for the championship on March 25, 2017. Dragón Rojo Jr. is the longest reigning champion in the history of the championship. Since its creation in 1991, there have been 17 individual championship reigns shared between 12 wrestlers. [[El Dandy]] is the only three-time champion; [[Apolo Dantés]] had the shortest reign of any champion, 77 days.

==History==
[[Image:Averno closeup.jpg|thumb|left|alt=Closeup of a masked wrestler yelling; his mask has skull and horns markings on it|[[Averno (wrestler)|Averno]], the 13th CMLL World Middleweight Champion<ref name="SL180">{{cite news|title=Averno el Campeon!|author=Manuel Rivera|work=SuperLuchas|id=180|pages=10–11|date=January 23, 2006|language=Spanish}}</ref>]]
The middleweight division was one of the first weight divisions in Mexican ''[[lucha libre]]'' to have a specific championship as the [[Mexican National Middleweight Championship]] was created in 1933.<ref name=MexMiddleBook>{{cite book|last1=Duncan|first1=Royal|last2=Will|first2=Gary|title=Wrestling Title Histories|publisher=Archeus Communications|chapter=Mexico: National Middleweight Championship|page=392|year=2000|isbn=0-9698161-5-4|location=Pennsylvania, USA|url=https://books.google.com/?id=_SnYAAAAMAAJ}}</ref><ref name=MexTitleMag>{{cite news|title=Los Reyes de Mexico: La Historia de Los Campeonatos Nacionales|work=Lucha 2000|id=Especial 21|date=December 20, 2004|language=Spanish|location=Mexico City, Mexico}}</ref> When the Mexican [[professional wrestling promotion]] [[Empresa Mexicana de Lucha Libre]] ("Mexican Wrestling Enterprise"; EMLL) was founded in September 1933, they became one of several Mexican promotions to promote the championship.<ref name=MexMiddleBook/> EMLL later created the World Middleweight Championship to represent the highest level prize of the middleweight division, higher than the Mexican National Middleweight Championship.<ref name=MexMiddleBook/><ref name=NWAMiddleBook>{{cite book|last1=Duncan|first1=Royal|last2=Will|first2=Gary|title=Wrestling Title Histories|publisher=Archeus Communications|year=2000|edition=4th|chapter=Mexico: EMLL NWA World Middleweight Title|pages=389–390|isbn=0-9698161-5-4|location=Pennsylvania, USA|url=https://books.google.com/?id=_SnYAAAAMAAJ}}</ref> In 1952, EMLL joined the [[National Wrestling Alliance]] (NWA) and changed the title to the [[NWA World Middleweight Championship]].<ref name=NWAMiddleBook/>

In the late 1980s, EMLL left the NWA over internal politics, and by 1991 they had changed their name to Consejo Mundial de Lucha Libre ("World Wrestling Council"; CMLL) to distance themselves from the NWA.<ref name="Mondo">{{cite book | author= Madigan, Dan | title= Mondo Lucha a Go Go: the bizarre & honorable world of wild Mexican wrestling | publisher= HarperColins Publisher | year= 2007 | chapter = "Okay... what is Lucha Libre?" and "El Médico Asasino" | pages = 29–40 and 114–118 | isbn=978-0-06-085583-3|location=New York, New York|url=https://books.google.com/?id=jtUt0rKjsZkC}}</ref> At first, they continued to use the name "NWA World Middleweight Championship" as the name had originated with EMLL, but they soon created a series of CMLL-branded world championships, including the CMLL World Middleweight Championship, the third middleweight championship in the promotion.<ref name=CMLLMiddleBook>{{cite book |last1=Duncan|first1=Royal|last2=Will|first2=Gary|title=Wrestling Title Histories |publisher=Archeus Communications |chapter = MEXICO: EMLL CMLL Consejo Mundial de Lucha Libre Middleweight Title |location=Pennsylvania, USA|page = 395 |year= 2000 |isbn=0-9698161-5-4|url=https://books.google.com/?id=_SnYAAAAMAAJ}}</ref><ref name=MexMiddleBook/><ref name=NWAMiddleBook/> CMLL held a one-night, eight-man tournament to determine the first middleweight champion on December 18, 1991. The tournament final saw [[Blue Panther]] defeat [[El Satánico]] to become the first new titleholder.<ref name=CMLLMiddleBook/>

In June 1992, many wrestlers left CMLL to join the newly formed [[Asistencia Asesoría y Administración]] ("Assistance, Assessment, and Administration"; AAA), which significantly affected CMLL's middleweight championships. The Mexico City Boxing and Wrestling Commission allowed AAA to assume control of the Mexican National Middleweight Championship as the reigning champion [[Octagón]] had joined AAA. Meanwhile, the CMLL World Middleweight Championship was vacated after the departure of the champion, Blue Panther. CMLL held a 16-man [[Battle royal (professional wrestling)|battle royal]] match to reduce the field to two finalists. [[El Dandy]] and [[Negro Casas]] survived the match, and a week later El Dandy defeated Casas to become the second CMLL World Middleweight Champion.<ref name=DragonRojoJr>{{cite web|url=http://www.record.com.mx/tmf/2011-11-19/dragon-rojo-jr-nuevo-campeon-peso-medio-del-cmll|title=Dragón Rojo Jr., nuevo Campeón peso Medio del CMLL|last=González | first=Fernando|date=November 19, 2011|accessdate=November 20, 2011|work=[[Récord]]|language=Spanish|archiveurl=https://web.archive.org/web/20131019133442/http://www.record.com.mx/tmf/2011-11-19/dragon-rojo-jr-nuevo-campeon-peso-medio-del-cmll|archivedate=October 19, 2013}}</ref><ref name=CMLLMiddleBook/> The championship has not been vacated since then.<ref name=Cagematch>{{cite web | url=http://www.cagematch.net/?id=5&nr=93 | title=CMLL World Middleweight Championship | accessdate=August 20, 2016 | publisher=Cagematch.net}}</ref>

The exodus from CMLL to AAA also meant that CMLL lost control of the Mexican National Middleweight Championship as then-reigning champion Octagón was among the wrestlers that left the promotion. The Mexico City Boxing and Wrestling Commission allowed AAA to take control of the Mexican National Middleweight Championship at that point in time.<ref name=MexMiddleBook/> On August 12, 2010, CMLL returned the NWA World Middleweight Championship to the NWA, but immediately replaced it with the [[NWA World Historic Middleweight Championship]] to keep two "world" level championships in the middleweight division.<ref name=2010YIR>{{cite news|title=Número Especial - Lo mejr de la lucha ilbre mexicana durante el 2010|work=SuperLuchas|id=399|date=January 12, 2011|language=Spanish}}</ref>

On May 3, 2010, [[Jushin Thunder Liger]] defeated Negro Casas to win the CMLL World Middleweight Championship. The match took place in [[Fukuoka|Fukuoka, Fukuaka prefecture]], [[Japan]], which was the first time the championship changed hands outside of Mexico and also marked the first time a non-Mexican wrestler held the championship.<ref name=Liger>{{cite web|url=http://superluchas.com/2010/05/03/jushin-liger-nuevo-campeon-mundial-medio-del-cmll-nakamura-pierde-el-titulo-iwgp/|title=Jushin Liger, nuevo campeón mundial medio del CMLL – Nakamura pierde el título IWGP|author=Flores, Manuel|date=May 3, 2010|accessdate=May 3, 2010|work=SuperLuchas|language=Spanish}}</ref>

==Reigns==
[[File:Jushin Thunder Liger.JPG|thumb|right|200px|[[Jushin Thunder Liger]], the only Japanese wrestler to hold the championship|alt=Picture of a masked Japanese wrestler wearing an elaborate red mask with horns.]]
{{main|List of CMLL World Middleweight Champions}}

[[Ángel de Oro]] is the current champion, having won the title on March 25, 2017, from [[Dragón Rojo Jr.]]<ref name=AngelDeOroMiddle>{{cite web | url=http://cmll.com/?p=91832 | title=Resultados Sabado Aarena Coliseo: ¡Ángel de Oro Acabo con un historico reinado! | publisher=[[Consejo Mundial de Lucha Libre]] | language=Spanish | date=March 26, 2017 | accessdate=March 28, 2017}}</ref> This is Ángel de Oro's first reign as middleweight champion; he is the 18th overall champion. Dragón Rojo Jr. is the wrestler who has held the championship the longest, a total of {{age in years and days|November 18, 2011|March 25, 2017}}<ref name=DragonRojoJr/><ref name=Cagematch/> El Dandy holds the record for most CMLL World Middleweight Championship reigns with three and is one of only three wrestlers to hold the title more than once, the others being Negro Casas and [[Emilio Charles Jr.]]<ref name=Cagematch/> [[Apolo Dantés]] held the title the shortest amount of time, a total of 77 days.<ref name=CMLLMiddleBook/><ref name=Cagematch/>

==Rules==
{{See also|Professional wrestling championship}}

The official definition of the [[Professional wrestling weight classes#Mexico|middleweight division]] in Mexico is from {{convert|82|kg|lb|abbr=on}} to {{convert|87|kg|lb|abbr=on}}.<ref name="WeightDivision">{{cite web| url= http://www.ordenjuridico.gob.mx/Estatal/ESTADO%20DE%20MEXICO/Reglamentos/MEXREG004.pdf | format=PDF | title= Reglamento de Box y Lucha Libre Professional del Estado de Mexico | last1= Rojas | first1= Arturo Montiel | publisher= Comisión de Box y Lucha Libre Mexico D.F. | date= August 30, 2001 | accessdate= April 3, 2009 | quote= Articulo 242: "Super Welter 82 kilos / Medio 87 kilos"|archiveurl=https://web.archive.org/web/20061130181418/http://www.ordenjuridico.gob.mx/Estatal/ESTADO%20DE%20MEXICO/Reglamentos/MEXREG004.pdf|archivedate=November 30, 2006|language=Spanish}}</ref> In the 20th century, CMLL was generally consistent and strict about enforcing the actual weight limits,<ref name=CMLLTitles>{{cite book |last1=Duncan|first1=Royal|last2=Will|first2=Gary|title=Wrestling Title Histories | publisher=Archeus Communications |chapter = MEXICO: EMLL CMLL| pages = 395–410 |year= 2000 |isbn=0-9698161-5-4|location=Pennsylvania, USA|url=https://books.google.com/?id=_SnYAAAAMAAJ}}</ref> but in the 21st century the official definitions have at times been overlooked for certain champions. One example of this was when [[Mephisto (wrestler)|Mephisto]], officially listed as {{convert|90|kg|lb|abbr=on}}, won the [[CMLL World Welterweight Championship]], a weight class with a {{convert|82|kg|lb|abbr=on}} upper limit.<ref name=2004YIR>{{cite news|title=Número Especial - Lo mejr de la lucha ilbre mexicana durante el 2004|work=SuperLuchas|id=399|date=January 12, 2011|language=Spanish}}</ref><ref name="WeightDivision"/>

With [[Consejo Mundial de Lucha Libre#Championships|twelve CMLL-promoted championships labelled as "World" titles]], the promotional focus shifts from championship to championship over time with no single championship consistently promoted as the "main" championship; instead CMLL's various major shows feature different weight divisions and are most often headlined by a ''[[Lucha de Apuestas]]'' ("Bet match") instead of a championship match.<ref name="Mondo"/> From 2013 until June 2016, only two major CMLL shows have featured championship matches, ''[[Sin Salida (2013)|Sin Salida]]'' in 2013 and the 2014 ''[[Juicio Final (2014)|Juicio Final]]'' show featuring the [[NWA World Historic Welterweight Championship]].<ref name=Medio1234>{{cite web | url=http://www.mediotiempo.com/mas-deportes/lucha-libre/noticias/2014/08/02/cayo-la-cabellera-de-princesa-blanca-y-la-mascara-de-seductora | title=Cayó la cabellera de Princesa Blanca y la máscara de Seductora | first=Apolo | last=Valdés | work=MedioTiempo | date=August 2, 2014 | accessdate=August 2, 2014 | language=Spanish}}</ref><ref name=SLNWA>{{cite web | url=http://superluchas.com/2013/06/02/mascara-dorada-nuevo-campeon-mundial-historico-nwa-peso-welter/ | title=Mascara Dorada nuevo campeon mundial historico NWA peso welter | date=June 2, 2013 | accessdate=June 3, 2013 | work=SuperLuchas Magazine | publisher=SuperLuchas Magazine | language=Spanish}}</ref> Championship matches usually take place under [[Professional wrestling match types#Series variations|best two-out-of-three falls rules]].<ref name="Mondo"/> On occasion, single fall title matches have taken place, especially when promoting CMLL title matches in Japan, conforming to the traditions of the local promotion, illustrated by Jushin Thunder Liger winning the championship during [[New Japan Pro Wrestling]]'s ''[[Wrestling Dontaku 2010]]'' in a single-fall match.<ref name=Liger/>

==Tournaments==

===1991===
In 1991, CMLL held an eight-man, one-night tournament to crown the first ever CMLL World Middleweight Champion. In the end, Blue Panther won the championship by defeating El Satánico.<ref name=CMLLMiddleBook/>

{{8TeamBracket-Compact-NoSeeds | RD2=First round| RD3=Semifinals| RD4=Final

| RD1-team01='''[[Blue Panther]]'''
| RD1-team02=[[Ringo Mendoza]]
| RD1-score01='''W'''
| RD1-score02=&nbsp;

| RD1-team03=Espectro Jr.
| RD1-team04='''[[El Dandy]]'''
| RD1-score03=&nbsp;
| RD1-score04='''W'''

| RD1-team05='''[[Ángel Azteca]]'''
| RD1-team06=[[Emilio Charles Jr.]]
| RD1-score05='''W'''
| RD1-score06=&nbsp;

| RD1-team07='''[[El Satánico]]'''
| RD1-team08= [[Arturo Beristain|El Hijo de Gladiador]]
| RD1-score07='''W'''
| RD1-score08=&nbsp;

| RD2-team01='''Blue Panther'''
| RD2-team02=El Dandy
| RD2-score01='''W'''
| RD2-score02=&nbsp;

| RD2-team03=Ángel Azteca
| RD2-team04='''El Satánico'''
| RD2-score03=&nbsp;
| RD2-score04='''W'''

| RD3-team01='''Blue Panther'''
| RD3-team02=El Satánico
| RD3-score01='''W'''
| RD3-score02=&nbsp;
}}

===1992===
Due to a large number of wrestlers leaving the company in the summer of 1992, the middleweight championship was vacated, forcing CMLL to hold a tournament. They opted to start out with a 16-man battle royal elimination match as a means to qualify for the final match the following week. Negro Casas and El Dandy outlasted a field of wrestlers that consisted of [[Black Terry|Guerrero Maya]], [[Águila Solitaria]], Ponzona, Guerrero del Futuro, [[Los Metálicos|Plata]], Espectro de Ultratumba, Espectro Jr., [[Oro (wrestler)|Oro]], Javier Cruz, [[Kung Fu (wrestler)|Kung Fu]], [[Kato Kung Lee]], [[Ringo Mendoza]], [[Bestia Salvaje]] and [[Último Dragón]].<ref name=CMLLMiddleBook/> The following week El Dandy defeated Casas to start his first of three championship reigns.<ref name=CMLLMiddleBook/>

==References==
{{reflist|30em}}

==External links==
*[http://www.cmll.com Official CMLL Website]

{{Consejo Mundial de Lucha Libre championships}}
{{CMLL World Middleweight Championship}}

[[Category:Consejo Mundial de Lucha Libre championships]]
[[Category:Middleweight wrestling championships]]