{{Infobox journal
| title = Living Reviews in Solar Physics
| cover = [[File:logo lrsp.gif]]
| editor = [[Sami Solanki]]
| discipline = [[Astronomy]], [[astrophysics]], [[solar physics]]
| abbreviation = Living Rev. Sol. Phys.
| publisher = [[Springer Science+Business Media]]
| frequency = 
| history = 2004-present
| openaccess = Yes
| impact = 17.636 
| impact-year = 2014
| license = [[Creative Commons]]
| website = http://solarphysics.livingreviews.org/
| link1 = 
| link1-name = 
| JSTOR = 
| OCLC = 163295955
| LCCN = 
| CODEN = 
| ISSN = 1614-4961
| eISSN = 
}}
'''''Living Reviews in Solar Physics''''' is a [[Peer review|peer-reviewed]] [[open-access]] [[scientific journal]] publishing [[Literature review|reviews]] on all areas of [[Solar physics|solar]] and [[Heliosphere|heliospheric]] [[physics]]. It was founded and published at the [[Max Planck Institute for Solar System Research]] from 2004-2015. After it was sold by the [[Max Planck Society]] in June 2015, it is now published by the academic publisher [[Springer Science+Business Media]].

The articles in ''[[Living Reviews]]'' provide critical reviews of the current state of research in the fields they cover. Articles also offer annotated insights into the key literature and describe other available resources. ''Living Reviews'' is unique in maintaining a suite of high-quality reviews, which are kept up-to-date by the authors through regularly adding the latest developments and research findings. This is the meaning of the word "living" in the journal's title.

It is central to the concept of '''''Living Reviews in Solar Physics''''' that it be a fully electronic journal, this allows to update its articles regularly and with minimum effort. The journal's reference database provides indexes of useful resources. Thus, in addition to a review journal, ''[[Living Reviews]]'' can be used by its readers as a database, an encyclopedia, or a resource letter. '''''Living Reviews in Solar Physics''''' has become a valuable tool for the scientific community and one of the first places a researcher looks for information about current work in solar and heliospheric physics.<ref>{{cite news | url = http://solarphysics.livingreviews.org/About/concept.html | title =Living Reviews in Solar Physics: Journal Concept }}</ref>

== See also ==
* [[List of scientific journals in astronomy]]

== References ==
<references/>

== External links ==
* {{Official website|http://solarphysics.livingreviews.org/}}

{{Max Planck Society}}

[[Category:Astrophysics journals]]
[[Category:Open access journals]]
[[Category:Physics review journals]]
[[Category:Publications established in 2004]]
[[Category:English-language journals]]
[[Category:Max Planck Society]]
[[Category:Springer Science+Business Media academic journals]]