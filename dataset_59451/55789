{{Infobox journal
| cover =
| editor = Lisa Adkins and Maryanne Dever
| discipline = [[Feminist studies]]
| former_names =
| abbreviation = Austr. Fem. Stud.
| publisher = [[Routledge]]
| country =
| frequency = Quarterly
| history = 1985-present
| openaccess = [[Hybrid open access journal|Hybrid]]
| license =
| impact = 0.500
| impact-year = 2015
| website = http://www.tandfonline.com/action/journalInformation?journalCode=cafs20
| link1 = http://www.tandfonline.com/toc/cafs20/current
| link1-name = Online access
| link2 = http://www.tandfonline.com/loi/cafs20
| link2-name = Online archive
| JSTOR =
| OCLC = 41979718
| LCCN = 89646678
| CODEN = AFSTFJ
| ISSN = 0816-4649
| eISSN = 1465-3303
}}
'''''Australian Feminist Studies''''' is a quarterly [[peer-reviewed]] [[academic journal]] covering [[feminist studies]]. It was established in 1985<ref name="bril">{{cite web|title=Women and gender studies: Periodicals|url=http://www.bl.uk/reshelp/findhelprestype/refworks/women/womenper/womenper.html|publisher=British Library|accessdate=29 January 2017}}</ref> and is published by [[Routledge]]. The founding [[editor-in-chief]] was [[Susan Magarey]] ([[University of Adelaide]]). She was succeeded as editor by Mary Spongberg ([[University of Technology Sydney]]). The current editors are Lisa Adkins ([[University of Newcastle (Australia)|University of Newcastle]]) and Maryanne Dever ([[University of Technology Sydney]]). The journal was formerly published twice a year.<ref name="bril"/>

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|2|
* [[America: History and Life]]
* [[Australian Education Index]]
* [[British Humanities Index]]
* [[EBSCOhost]]
* [[Current Contents]]/Social and Behavioural Sciences
* [[Social Sciences Citation Index]]
* [[Scopus]]
* [[International Bibliography of the Social Sciences]]
* [[Sociological Abstracts]]
* [[Studies in Women and Gender Abstracts]]
}}

According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 0.500, ranking it 29th out of 40 journals in the category "Women's Studies".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Women's Studies |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== See also ==
* [[Feminist theory]]
* [[Gender studies]]
* [[List of women's studies journals]]
* [[Women's liberation movement]]
* [[Women's studies]]

== References ==
{{Reflist}}

== External links ==
* {{Official website|1=http://www.tandfonline.com/action/journalInformation?journalCode=cafs20}}

{{DEFAULTSORT:Australian Feminist Studies}}
[[Category:English-language journals]]
[[Category:Feminist journals]]
[[Category:Publications established in 1985]]
[[Category:Quarterly journals]]
[[Category:Routledge academic journals]]
[[Category:Women's studies journals]]
[[Category:Biannual journals]]
[[Category:1985 establishments in Australia]]


{{Womens-journal-stub}}