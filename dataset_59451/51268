{{Infobox journal
| title = European Journal of Criminology
| cover = [[File:European Journal of Criminology.jpg]]
| editor = Paul Knepper
| discipline = [[Criminology & Criminal Justice]]
| former_names = 
| abbreviation = Eur. J. Criminol.
| publisher = [[Sage Publications]] on behalf of the [[European Society of Criminology]]
| country = 
| frequency = Bimonthly 
| history = 2004-present
| openaccess = 
| license = 
| impact = 1.141
| impact-year = 2013
| website = http://www.uk.sagepub.com/journals/Journal201644/title
| link1 = http://euc.sagepub.com/content/current
| link1-name = Online access
| link2 = http://euc.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 1477-3708
| eISSN = 1741-2609
| OCLC = 56561756
| LCCN = 2005211118
}}
The '''''European Journal of Criminology''''' is a [[Peer review|peer-reviewed]] [[academic journal]] covering the field of [[criminology]]. The [[editor-in-chief]] is Paul Knepper ([[Sheffield University]]). It was established in 2004 and is published by [[Sage Publications]] on behalf of the [[European Society of Criminology]].

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2013 [[impact factor]] is 1.141, ranking it 24 out of 52 journals in the category "Criminology & Penology".<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Criminology & Penology |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.uk.sagepub.com/journals/Journal201644/title}}
* [http://www.esc-eurocrim.org/ European Society of Criminology]

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Criminology journals]]
[[Category:Publications established in 2004]]
[[Category:Bimonthly journals]]
[[Category:Academic journals associated with international learned and professional societies of Europe]]