{{Refimprove|date=June 2009}}
{{Infobox state
| name                    = Akwa Ibom State
| official_name           = 
| type                    = [[States of Nigeria|State]]
| image_skyline           = 
| image_alt               = 
| image_caption           = 
| image_flag              = 
| flag_alt                = Flag of Akwa Ibom
| image_seal              = 
| seal_alt                = Seal of Akwa Ibom
| nickname                = [[List of Nigerian state nicknames|Land of Promise]]
| image_map               = Nigeria - Akwa Ibom.svg
| map_alt                 = 
| map_caption             = Location of Akwa Ibom in Nigeria
| coordinates             = {{coord|05|00|N|07|50|E|region:NG_type:adm1st|display=inline,title}}
| coor_pinpoint           = neymar
| coordinates_footnotes   = 
| subdivision_type        = [[List of sovereign states|Country]]
| subdivision_name        = {{flag|Nigeria}}
| established_title       = [[List of Nigerian states by date of statehood|Date created]]
| established_date        = 23 September 1987
| seat_type               = [[List of Nigerian state capitals|Capital]]
| seat                    = [[Uyo]]
| government_footnotes    = 
| leader_party            = [[People's Democratic Party (Nigeria)|PDP]]
| leader_title            = [[List of Nigerian state governors|Governor]]''' <br /> ([[List of Governors of Akwa Ibom State|List]])
| leader_name             = [[Udom Gabriel Emmanuel]]
| leader_title1           = [[Senate of Nigeria|Senators]]
| leader_name1            = {{unbulleted list|||}}
| leader_title2           = [[Nigerian House of Representatives|Representatives]]
| leader_name2            = 
| unit_pref               = Metric<!-- or US or UK -->
| area_footnotes          = 
| area_total_km2          = 7081
| area_rank               = [[List of Nigerian states by area|30 of 36]]
| area_note               = 
| elevation_footnotes     = 
| elevation_m             = 
| population_footnotes    = <ref>[https://akwaibomstate.gov.ng/page-page-akwa-ibom-state-history.html Akwa Ibom State History]</ref>
| population_total        = 5,450,758
| population_as_of        = 2016
| population_est          = 
| pop_est_as_of           = 
| population_rank         = [[List of Nigerian states by population|15 of 36]]
| population_density_km2  = auto
| population_note         = 
| population_demonym      = [[List of demonyms for states in Nigeria|Akwa Ibomite]]
| demographics_type1      = [[List of Nigerian states by GDP|GDP (PPP)]]
| demographics1_footnotes = 
| demographics1_title1    = Year
| demographics1_info1     = 2007
| demographics1_title2    = Total
| demographics1_info2     = $11.18&nbsp;billion<ref name="C-GIDD GDP">{{cite web|url=http://www.cgidd.com |publisher=Canback Dangel |title=C-GIDD (Canback Global Income Distribution Database) |accessdate=2008-08-20 |archiveurl=http://www.webcitation.org/665kvq69j?url=http%3A%2F%2Fwww.cgidd.com |archivedate=March 11, 2012 |deadurl=no |df= }}</ref>
| demographics1_title3    = Per capita
| demographics1_info3     = $2,779<ref name="C-GIDD GDP" />
| timezone1               = [[West Africa Time|WAT]]
| utc_offset1             = +01
| postal_code             = 520211, 520212, 520221, 520222, 520231, 520232, 520241, 520251, 520261, 520271
| area_code_type          = 
| area_code               = 
| iso_code                = [[ISO 3166-2:NG|NG-AK]]
| website                 = {{URL|http://akwaibomstate.gov.ng/}}
| footnotes               = 
}}
'''Akwa Ibom''' is a [[States of Nigeria|state]] in Nigeria. It is located in the coastal southern part of the country, lying between latitudes 4°32′N and 5°33′N, and longitudes 7°25′E and 8°25′E. The state is bordered on the east by [[Cross River State]], on the west by [[Rivers State]] and [[Abia State]], and on the south by the [[Atlantic Ocean]] and the southernmost tip of Cross River State.

Akwa Ibom is one of Nigeria's 36 states, with a population of over five million people and more than 10 million people in diaspora. The state was created in 1987 from the former [[Cross River State]] and is currently the highest oil- and gas-producing state in the country. The state's capital is [[Uyo]], with over 500,000 inhabitants. Akwa Ibom has an [[Akwa Ibom Airport|airport]] and two major seaports on the [[Atlantic Ocean]] with a proposed construction of a world-class seaport [[Ibaka Seaport]] at [[Oron, Akwa Ibom|Oron]]. The state also boasts of a 30,000-seat ultramodern [[Akwa Ibom Stadium|sports complex]]. Akwa Ibom state is also home to the Ibom E-Library, a world-class information centre.<ref>http://www.ibomelibrary.org/</ref> In addition to [[English language|English]], the main spoken languages are [[Ibibio language|Ibibio]], [[Annang language|Annang]], [[Eket language|Eket]] and [[Oron language|Oron]].<ref>{{Cite web
|url=http://www.premiumherald.com/2016/03/29/akwa-ibom-state-government-official-website-gets-new-look/
|title=Akwa Ibom State Government official Website gets a new look – The Premium Herald
|website=The Premium Herald
|language=en-US
|access-date=2016-04-07
}}</ref>

== Major cities ==

[[Essien Udim]],[[Uyo]], [[Eket]], [[Ikot Ekpene]], [[Oron, Nigeria|Oron]], [[Abak]], [[Ikot Abasi]], [[Ikono]],<ref name="ikono">Ikono – the cradle of Ibibio nation</ref> [[Etinan]], [[Esit Eket]], [[Uruan]], and [[Ibeno]].

== Ministries in Akwa Ibom ==
Here are the list of ministries in Akwa Ibom<ref>https://akwaibomstate.gov.ng/page-page-akwa-ibom-state-executive-council-.html</ref>
*[[Akwa Ibom State Ministry of Justice]]
*[[Akwa Ibom State Ministry of Agricultural Resources|Akwa Ibom State Ministry of Agriculture and Food Sufficiency]]
*[[Akwa Ibom State Ministry of Finance]]
*[[Akwa Ibom State Ministry of Works]]
*[[Akwa Ibom State Ministry of Education]]
*Akwa Ibom State Ministry of Environment
*[[Akwa Ibom State Ministry of Transport and Petroleum|Akwa Ibom State Ministry of Transport & Petroleum Resources]]
*Akwa Ibom State Ministry of Local Government and Chieftaincy Affairs
*Akwa Ibom State Ministry of Lands & Town Planning
*[[Akwa Ibom State Ministry of Information and Strategy|Akwa Ibom State Ministry of Information & Strategy]]
*[[Akwa Ibom State Ministry of Health]]
*[[Akwa Ibom State Ministry of Science & Technology]]
*Akwa Ibom State Ministry of Women Affairs and Social Welfare
*Akwa Ibom State Ministry of Youth & Sports
*Akwa Ibom State Ministry of Housing & Special Duties
*Akwa Ibom State Ministry of Economic Development Labour and Manpower Planning
*Akwa Ibom State Ministry of Investment, Commerce and Industries
*[[Akwa Ibom State Ministry of Culture and Tourism]]
*Akwa Ibom State Bureau of Political/Legislative Affairs and Water Resources
*Akwa Ibom State Bureau of Rural Development & Cooperatives

== Local government areas ==
Akwa Ibom State consists of thirty-one (31) [[Local government areas of Nigeria|local government areas]]. They are:

{{colbegin||10em}}
* [[Abak]]
* [[Eastern Obolo]]
* [[Eket]]
* [[Esit-Eket]]
* [[Essien Udim]]
* [[Etim-Ekpo]]
* [[Etinan]]
* [[Ibeno]]
* [[Ibesikpo-Asutan]]
* [[Ibiono-Ibom]]
* [[Ika, Nigeria|Ika]]
* [[Ikono]]
* [[Ikot Abasi]]
* [[Ikot Ekpene]]
* [[Ini, Nigeria|Ini]]
* [[Itu, Nigeria|Itu]]
* [[Mbo, Nigeria|Mbo]]
* [[Mkpat-Enin]]
* [[Nsit-Atai]]
* [[Nsit-Ibom]]
* [[Nsit-Ubium]]
* [[Obot-Akara]]
* [[Okobo, Nigeria|Okobo]]
* [[Onna]]
* [[Oron, Nigeria|Oron]]
* [[Oruk Anam]]
* [[Ukanafun]]
* [[Udung-Uko]]
* [[Uruan]]
* [[Urue-Offong/Oruko]]
* [[Uyo]]
{{colend}}

== History ==
The region of the state was created out of [[Cross River State]] on September 23, 1987.

== Demography ==
The people are predominantly [[Christian]]. The main ethnic groups of the state are:

*[[Ibibio people|Ibibio]]
*[[Annang]]
*[[Oron people|Oron]]
*[[Eket]]
*[[Obolo people|Obolo]]
The Ibibio, Annang, and Eket, who speak a dialect of the Ibibio language, Oron and Obolo, comprising Ibono (Ibeno) and Eastern Obolo people, are the largest ethnic groups. The Oro [Oron] is an ethnic group similar to the [[Efik people|Efik]], who also speak a dialect of Ibibio language and predominate in neighbouring [[Cross River State]], and are found in five of the state's Local Government Areas. Located at the Atlantic Ocean coast are the Eket, Ibeno and Eastern Obolo people. The Ibono have similarities with the Oro and Obolos. Some people of the North western border of Akwa by virtue of their interactions with the Igbos tend to be bilingual,  likewise the Igbos at the border who are fluent in Annang.  However Igbos are not indigenous to Akwa ibom 

The Ibibio language belongs to the [[Benue–Congo]] language family, which forms part of the [[Niger–Congo]] group of languages.

Despite the homogeneity, no central government existed among the people of what is now Akwa Ibom State prior to the [[British people|British]] invasion in 1904. Instead, the [[Annang]], [[Oron people|Oron]], [[Efik people|Efik]], [[Ibonos]] and [[Ibibio people|Ibibio]] were all autonomous groups.

Although several [[Scottish missionaries]] arrived in [[Calabar]] in 1848, and [[Ibono]] in 1887, the British did not firmly establish control of the area until 1904. In that year, the [[Enyong Division]] was created encompassing the area of the current state of Akwa Ibom, with headquarters at [[Ikot Ekpene]], an Annang city described by the noted [[African studies|Africanist]] [[Kaanan Nair]], as the cultural and political capital of Annang and Ibibio.{{citation needed|date=October 2015}}

The creation of [[Enyong Division]] for the first time allowed the numerous ethnic groups to come together. This further provided a venue to create of the [[Ibibio Welfare Union]], later renamed [[Ibibio State Union]]. This social organization was first organized as a local development and improvement forum for educated persons and groups who were shut out from the colonial administration in 1929. Nonetheless, some historians have wrongly pointed to the union to buttress their argument about the homogeneity of groups in the area.{{Citation needed|date=May 2008}} The Obolo Union, comprising Ibono and [[Andoni]] stock, was another strong socioeconomic and cultural organisation that thrived in the region. The Ibono people have fought wars to maintain their unique identity and territory in the region more than any other group.

When [[Akwa Ibom state]] was created in 1987, [[Uyo]] was chosen as the state capital to spread development to all regions of the state.

== Education ==
The [[Akwa Ibom State Ministry of Education]] is tasked with monitoring the education sector of the state. The current region of Akwa Ibom State in old [[Akwa Akpa|Calabar Kingdom]] was the first to encounter Western education in Nigeria with the establishment of [[Hope Waddell Training Institute]], at Calabar in 1895, and the Methodist Boys High School at Oron in 1905 as well as other top schools such as the Holy Family College at [[Abak]] and Regina Coeli College in [[Essene]].

Some educational institutes in the state are:
*[[University of Uyo]]
*[[Maritime Academy of Nigeria]], [[Oron, Nigeria|Oron]]
*[[Akwa Ibom State University]] [Oruk Anam LGA And Mkpat Enin LGA]
*[[Obong University]], [[Obong Ntak Inyang]]
*[[Akwa Ibom State Polytechnic]]
*[[Uyo City Polytechnic]]
*[[Apex Polytechnic]]
*[[Heritage Polytechnic]], [[Eket]]
*[[School of Nursing]]; [[Uyo]], [[Eket]], [[Oron, Nigeria|Oron]], [[Ikot Ekpene]], [[Etinan]]
*[[Akwa Ibom State College of Education]], [[Afaha Nsit]]
*[[School Of Basic Studies]]
*[[College of Arts & Sciences]], [[Nnung Ukim]]
*[[Ritman University]]

== Notable People from Akwa Ibom ==
* [[Ita Enang|Senator Ita Enang]] Senior Special Assistant to President  [[Muhammadu Buhari]]
* [[Godswill Akpabio|Godswill Obot Akpabio]] Former governor of Akwa Ibom State. Present Senate Minority Leader
* [[Obong Victor Attah]] Former governor of Akwa Ibom State
* [[Idongesit Nkanga]] Former military governor of Akwa Ibom State
* [[Effiong Bob]]
* [[Umana Okon Umana]]
* [[Moses Ukpong]] Former State House of Assembly member who brought [[People's Democratic Party (Nigeria)|People's Democratic Party]] (Nigeria) to the limelight representing [[Eket]] LGA.
* [[Hon. Victor Antai]] Commissioner For Culture and Tourism.
* [[Nsikak Eduok]]
* [[Chris Ekpeyong]] Former deputy governor of Akwa Ibom State in the [[Victor Attah]] administration.
* [[Onofiok Luke]], the 11th Speaker of the Akwa Ibom State House of Assembly and the Pioneer Speaker of the Nigeria Youth Parliament.
* [[Bassey Albert]]
* [[Late Egbert Udo Udoma]] A jurist, administrator, elder statesman. Former chief and acting Governor-General of Uganda. Retired justice of the Supreme Court of Nigeria. National president, [[Ibibio State Union]]. Leader COR State Movement. Chairman, Constitution Drafting Committee.
* [[Late Chief Nyong Essien(CMG,aCON)]] first representative of Old Province in the Legislative Council in Lagos. First president of Eastern Regional House of Chiefs, first installed president of Ibibio Union. First officially recognised Paramount Ruler of [[Uyo]]
* [[Akpan Isemin]]
* [[Dominic Ekandem|Late Dominic Cardinal Ekanem (CFR)]] first cardinal in English-speaking West Africa. First Nigerian Cardinal to qualify as a candidate to the papacy.
* [[Clement Isong|Late Chief (Dr) Clement Isong (CFR)]] first governor, Central Bank of Nigeria. First civilian governor of the former [[Cross River State]]
* [[Late Brigadier-General U. J. Esuene ]] military governor of the South Eastern State (1967)
* [[Samuel Peter|Samuel Okon Peter (OON), (DSP)]] World heavyweight boxing champion.
* [[Vincent Enyeama]] Professional footballer and former Super Eagle captain.
* [[Donald Etiebet|Chief Don Etiebet]] (Former minister of Petroleum)

== Politics ==
Politics in Akwa Ibom state are dominated by the three main ethnic groups, the [[Ibibio people|Ibibio]], [[Annang]] and [[Oron people|Oron]]. Of these three, the [[Ibibio people|Ibibio]] remain the majority and have held sway in the state since its creation. For the past eight years, the Annang people held sway, since the governor for those eight years was from Ikot Ekpene senatorial district.

== References ==
{{reflist}}

== Related Pages ==
* [[Akwa Ibom State Ministry of Education]]
* [[List Of Government Ministries Of Akwa Ibom State|Ministries Of Akwa Ibom State And Commissioners]]

== External links ==
* [http://www.akwaibomstate.gov.ng/ akwaibomstate.gov.ng: Official website of the Akwa Ibom State Government]

{{AkwaIbomStateGovernors}}
{{Nigeria states}}

[[Category:States of Nigeria]]
[[Category:Akwa Ibom State| ]]
[[Category:States and territories established in 1987]]
[[Category:1987 establishments in Nigeria]]