{{Infobox military person
| name =  Paul D. Stroop
| image = Vice Admiral Paul Stroop in Japan.jpg
| caption = Vice Admiral Paul D. Stroop on Okinawa in 1957
| birth_date = {{Birth date|1904|10|30|df=y}}
| death_date = {{Death date and age|df=y|1995|5|17|1904|10|30}}
| placeofburial_label = Place of burial
| placeofburial =  
| birth_place = [[Zanesville, Ohio]]
| death_place = [[Coronado, California]]
| placeofburial_coordinates = <!-- {{Coord|LAT|LONG|display=inline,title}} -->
| nickname =
| allegiance = {{USA}}
| branch = [[File:United States Department of the Navy Seal.svg|25px]] [[United States Navy]]
| serviceyears = 1926&ndash;1965
| rank =[[File:US-O9 insignia.svg|35px]] [[Vice admiral (United States)|Vice Admiral]]
| unit =
| commands = {{USS|Mackinac|AVP-13}}<br/>{{USS|Croatan|CVE-25}}<br/>{{USS|Princeton|CV-37}}<br/>{{USS|Essex|CV-9}}<br/>[[Naval Air Weapons Station China Lake|Naval Ordnance Test Station, China Lake]]<br/>Taiwan Patrol Force<br/>Bureau of Naval Weapons<br/>Naval Air Force, Pacific Fleet
| battles =[[World War II]]<br/>[[Korean War]] 
| awards = [[Legion of Merit]] (2)
| relations =
| laterwork =
}}
Vice Admiral '''Paul David Stroop''' (30 October 1904 – 17 May 1995) was an officer of the [[United States Navy]] and a [[Naval Aviator]]. He held numerous high-ranking staff positions in aviation from the 1930s onward, including [[World War II]] service on the staff of the Chief of Naval Operations.  During the late 1940s and early 1950s, he held various sea commands.  From 1959-1962, he oversaw the development of the Navy's aerial weapons, including early [[guided missiles]], as Chief of the [[Bureau of Naval Weapons]].  During the later 1960s, he commanded Naval air forces in the Pacific.

==Biography==

===Early life and career===
Stroop  was born in [[Zanesville, Ohio]],<ref>[http://books.google.ca/books?id=d5j_JljLWYAC&pg=PT18&dq=%22Paul+D.+Stroop%22+1904&hl=en&sa=X&ei=FOAtU5yUEM2bqAG8sIAo&ved=0CCwQ6AEwAA#v=onepage&q=%22Paul%20D.%20Stroop%22%201904&f=false]</ref> but grew up in [[Mobile, Alabama]].<ref name="nytimes">{{Cite web
 |url= https://www.nytimes.com/1995/05/21/obituaries/paul-stroop-commander-of-navy-s-pacific-air-force-dead-at-90.html
 |title=Paul Stroop, Commander of Navy's Pacific Air Force, Dead at 90
 |work=[[The New York Times]]
 |date= May 21, 1995
 |author= J. Michael Elliott
 |accessdate=11 November 2010
}}</ref> He graduated from the [[United States Naval Academy]] in 1926, then spent the next two years on board the battleship {{USS|Arkansas|BB-33|3}}. In 1928, he served as a member of U.S. gymnastic team at the [[1928 Summer Olympics|Olympic Games]] in [[Amsterdam]].

===Naval aviation assignments===
From 1928-1929, Stroop received flight training at [[Naval Air Station Pensacola]], Florida, and in 1929 received his wings as a [[United States Naval Aviator|Naval Aviator]].  His first aviation assignment was with Torpedo Squadron 9, based at [[NAS Norfolk]], Virginia. In 1932 he was transferred to Patrol Squadron 10, also based at Norfolk.

From 1932-1934, he undertook postgraduate work at the Naval Academy. After completing his studies, he returned to Fleet assignments.  He served from 1934-1936 with Bombing Squadron 5, aboard the carrier {{USS|Ranger|CV-4|3}}.  From 1936-1937, he was Senior Aviator aboard the cruiser {{USS|Portland|CA-33|3}}. In 1937, Stroop gained his first experience in the Naval Aviation material establishment when he was assigned to the Navy's [[Bureau of Aeronautics]] (BuAer).  He left BuAer in 1940 to join the staff of Admiral [[Aubrey Fitch]], commander of Patrol Wing 2, based at [[Pearl Harbor]], Hawaii. In 1940, Stroop became Flag Officer and Tactical Officer of Carrier Division 1 at San Diego.

===World War II===
After the United States entry into World War II, Stroop was transferred to Pearl Harbor.  In 1942, he joined the staff of the Carrier Task Force, aboard {{USS|Lexington|CV-2|3}} at Pearl Harbor.  From 1942-1943, he served as Planning Officer to the Senior Naval Commander, Air Force, South Pacific.

He next gained his own command, serving from 1943-1944 as Commanding Officer of the [[seaplane tender]] {{USS|Mackinac|AVP-13}}. Stroop spent the last months of the war in Washington, D.C., serving from 1944-1945 in the Navy Department as Aviation Plans Officer on the Staff of Fleet Admiral [[Ernest J. King]], the [[Chief of Naval Operations]] and Commander in Chief, [[United States Fleet|U.S. Fleet]]. In this capacity, Stroop attended the [[Yalta Conference|Yalta]], [[Second Quebec Conference|Quebec]], and [[Potsdam Conference|Potsdam]] Conferences, later making a trip around the world to inform commands of outcome of the Yalta Conference.

===Post-war activities===
In 1945, Stroop left the Navy Department to become Commanding Officer of the [[escort carrier]] {{USS|Croatan|CVE-25|3}}. He served as Fleet Aviation Officer (later Chief of Staff, Operations), in the [[United States Fifth Fleet|Fifth Fleet]], based at Yokosuka, Japan, from 1945–1946, and then as Aviation Officer (later Assistant Chief of Staff) Operations to the Commander in Chief of the [[United States Pacific Fleet|Pacific Fleet]] ([[CINCPACFLT]]), at Pearl Harbor, in 1946-1948.

From 1948-1950, Stroop served as Executive Officer at the [[Naval Postgraduate School|Navy's General Line School]] in [[Monterey, California]], then again took up his own studies as a student at the [[National War College]] at Washington, D.C., in 1950-1951.

In 1951, Stroop became Commanding Officer of the carrier {{USS|Princeton|CV-37|3}} in the Sea of Japan during the [[Korean War]].<ref name="nytimes"/> Then, in 1952, he assumed command of the {{USS|Essex|CV-9|3}}, and was promoted to rear admiral.<ref name="nytimes"/> In 1953, he left the ''Essex'' to become Commanding Officer of the [[Naval Air Weapons Station China Lake|Naval Ordnance Test Station, China Lake]], [[California]].

From 1953-1955, he was Senior Member of the Weapons Systems Evaluation Group, Joint Chiefs of Staff, Navy Department, Washington.  From 1955-1957, he served as Deputy Chief at the [[Bureau of Ordnance]] (BuOrd).

From 1957-1958, he was Commanding Officer of the [[United States Taiwan Defense Command|Taiwan Patrol Force]] based at Okinawa, Japan.  From 1959-1962 he was Chief of the [[Bureau of Naval Weapons]].

Stroop served from 1962-1965 as Commanding Officer of the Naval Air Force, Pacific Fleet ([[COMNAVAIRPAC]]), and as Commanding Officer, [[United States First Fleet|First Fleet]], Air PAC, with the rank of vice admiral.<ref name="nytimes"/>

He retired in 1965. After his retirement to the [[San Diego]] area, he was a consultant to [[Ryan Aeronautical]] and Teledyne Ryan of San Diego until 1992.<ref name="nytimes"/>

Stroop died on at the Coronado Hospital in [[Coronado, California]], on 17 May 1995, aged 90.<ref name="nytimes"/>

==Personal life==
Stroop was married to Esther Holscher Stroop from 1926 until her death in 1982. He was survived by his second wife, Kay Roeder Stroop; his two sons, two daughters, three stepdaughters, a stepson, 13 grandchildren, and 11 great-grandchildren.<ref name="nytimes"/>

==References==
{{reflist}}
* Grossnick, Roy et al.  "Part 8: The New Navy 1954-1959."  ''United States Naval Aviation 1910-1995."  4th edition.  Washington, D.C.: Naval Historical Center, 1997.  Online.  Naval Historical Center.  Viewed 24 February 2006. [http://www.history.navy.mil/avh-1910/PART08.PDF http://www.history.navy.mil/avh-1910/PART08.PDF]
* "Stroop, Paul D., VADM, USN, 1904-1995".  in ''A GUIDE TO ARCHIVES, MANUSCRIPTS AND ORAL HISTORIES IN THE NAVAL HISTORICAL COLLECTION''. Naval War College, Newport, R.I.  2001.  Compiled by Evelyn M. Cherpak, Ph.D.  Online.  2001.  Naval War College.  Viewed 24 February 2006. [http://www.nwc.navy.mil/Library/3Publications/NWCLibraryPublications/NavHistCollPubs/NHC%20Guide.doc http://www.nwc.navy.mil/Library/3Publications/NWCLibraryPublications/NavHistCollPubs/NHC%20Guide.doc] [Source of biographical data]

{{NHC}} ''It also contains public-domain information collected from the [[Naval War College]], an institution of the United States government.''

==External links==
*[http://www.nawcwpns.navy.mil/clmf/COs.html China Lake Military Leadership] - from the Naval Air Warfare Center Weapons Division, China Lake

{{Authority control}}

{{DEFAULTSORT:Stroop}}
[[Category:1904 births]]
[[Category:1995 deaths]]
[[Category:People from Muskingum County, Ohio]]
[[Category:American people of Dutch descent]]
[[Category:Gymnasts at the 1928 Summer Olympics]]
[[Category:Olympic gymnasts of the United States]]
[[Category:United States Navy admirals]]
[[Category:United States Naval Academy alumni]]
[[Category:United States Naval Aviators]]
[[Category:Recipients of the Legion of Merit]]