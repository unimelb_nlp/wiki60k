{{Infobox person
| name = Greg Wilson
| image  = 
| image_size = 
| caption = 
| birth_name = Gregory D. Wilson
| birth_date = 
| birth_place = {{nowrap|[[Dallas, Texas]], U.S.}}
| nationality   = [[United States|American]]
| other_names   = Greg Wilson, Greg D. Wilson
| occupation = Comedian, actor, voice actor
| years_active = 1996-present
| notable_works = ''[[The Hottie and the Nottie]]'', ''[[World's Dumbest...]]''
<!--This page is about a real person NOT an internet meme.  Do not add a religion/religious view without citing a source-->
| residence = [[Los Angeles, California]]
| website = http://thegregwilson.com/
}}
'''Gregory D "Greg" Wilson''', better known by the stage name '''Greg Wilson''', is an American [[comedian]], [[actor]], and [[voice actor]] based in [[Los Angeles]]. He is best known for his role as Arno Blount in the 2008 movie ''[[The Hottie and the Nottie]]''.

==Career==

Greg Wilson got his start in comedy as a member of the Dallas improv theater group ''Ad-Libs''.<ref name="bio">[http://thegregwilson.com/bio/ "Bio"], thegregwilson.com. Retrieved: 14 March 2015</ref><ref>Martin, Amy. [http://www.theaterjones.com/reviews/20120116111046/2012-01-16/Ad-Libs-Comedy-Improv-Troupe/Ad-Libs-25th-Anniversary-All-Star-Reunion-Show "Review: Ad-Libs 25th Anniversary All-Star Reunion Show | Ad-Libs Comedy Improv Troupe | Mouth"], Theaterjones.com, 16 January 2012. Retrieved: 21 March 2015.</ref> He went on to develop his own shows: ''The Comedy Madhouse'', in [[Las Vegas]], and later on, ''THE GREG WILSON'S Stand-Up|SMACKDOWN!'', in [[New York City]].<ref name="bio" /><ref name="boyz">[http://www.prnewswire.com/news-releases/get-laughs-or-die-trying-white-boyz-in-the-hood-57050337.html "Get Laughs or Die Trying: White Boyz In The Hood"], ''[[PR Newswire]]'', 19 September 2006. Retrieved: 14 March 2015.</ref><ref>Heldman, Breanne L. [http://www.nydailynews.com/archives/nydn-features/thursday-new-york-article-1.552365 "THURSDAY IN NEW YORK"], ''[[Daily News (New York)|Daily News]]'',  3 February 2005. Retrieved: 21 March 2015.</ref>

Currently based in Los Angeles, Wilson has performed in venues such as [[The Laugh Factory]] and [[Stand Up NY]].<ref name="bio" /><ref name="boyz" /><ref>[http://www.laughfactory.com/TheGregWilson "The The Greg Wilson Channel"], ''Laugh Factory''. Retrieved: 14 March 2015.</ref> On television, he has made appearances in several [[stand-up comedy]] shows and specials, including [[Showtime (TV network)|Showtime]]'s ''White Boyz in the Hood'',  ''[[Comics Unleashed]]'', and ''[[Comedy.tv]]'',<ref name="bio" /><ref name="comic">McCarthy, Sean L. [http://thecomicscomic.com/2013/04/26/comedian-auditioning-for-americas-got-talent-accused-of-stealing-bit-from-the-tv-shows-warm-up-comic/ "Comedian auditioning for “America’s Got Talent” accused of stealing bit from the TV show’s warm-up comic"], ''The Comic's Comic'', 26 April 2013. Retrieved on 13 March 2015.</ref> and was featured in an episode of the reality show ''[[Who Wants to Date a Comedian?]]''.<ref>[http://www.tvtango.com/series/who_wants_to_date_a_comedian "Who Wants to Date a Comedian? Overview"], ''TV Tango''. Retrieved: 14 March 2015.</ref> From 2012 to 2013, he was a cast member on ''[[World's Dumbest...]]''.<ref name="bio" /><ref name="comic" /> He also co-hosts the podcast ''Hot N' Heavy'', along with model [[Angie Everhart]].<ref>[http://toadhopnetwork.com/f/Hot "Hot N' Heavy"], ''Toad Hop Network''. Retrieved: 21 March 2015.</ref>

Wilson runs a stand-up comedy academy, The Comedy Institute.<ref>[http://www.thecomedyinstitute.com/ The Comedy Institute.] Retrieved: 14 March 2015.</ref><ref>[http://video.foxnews.com/v/4374442/the-greg-wilson-on-red-eye/?#sp=show-clips "The Greg Wilson on 'Red-Eye'"], ''[[Fox News]]'', 15 October 2010. Retrieved: 14 March 2015</ref> In it, he offers courses on storytelling, handing hecklers, and other skills necessary for a stand-up comedian, as well as a master class on "Mastering Stand-Up".

As an actor, Wilson has appeared in television series such as ''[[Bones (TV series)|Bones]]'', ''[[Ugly Betty]]'', and ''[[Modern Family]]'', usually in small, episodic roles, as well as commercials for [[Heineken]]<ref>[http://www.iftn.ie/?act1=record&aid=73&rid=869&sr=1&only=1&hl=ttitude&tpl=archnews "Heineken Present Classic Films On TG4"], ''[[Irish Film and Television Network]]'', 15 November 2004. Retrieved on 14 March 2015.</ref> and [[Toyota]], among other brands. In 2008, Wilson landed a starring role in the [[romantic comedy]] film ''The Hottie and the Nottie'', opposite [[Paris Hilton]] and [[Joel David Moore]]. He portrayed Arno Blount, the male lead's quirky childhood friend.<ref name="bio" /><ref>[http://www.tvguide.com/movies/hottie-nottie-291941/cast/ The Hottie and the Nottie Cast.] ''[[TV Guide]]''. Retrieved: 13 March 2015.</ref> The film was a [[box office bomb]] and received universally negative reviews, and has been cited as [[List of films considered the worst|one of the worst films ever made]].

Notable voice acting roles include the character JD O'Toole in the video game ''[[Grand Theft Auto: Liberty City Stories]]'' and the eponymous Mr. Two in the animated [[Nickelodeon]] pilot ''[[Nickelodeon Animated Shorts Program|Charlie and Mr. Two]]''.

==Controversy==

On April 26, 2013, ''[[Slashfilm]]'' writer Peter Sciretta claimed Wilson had been publicly accused of [[joke theft]] during a recording of ''[[America's Got Talent]]''.<ref name="comic" /><ref>Sciretta, Peter. [http://www.slashfilm.com/howie-mandel-catches-comedian-stealing-another-comics-material-during-americas-got-talent-taping/ "Howie Mandel “Catches” Comedian Stealing Another Comic’s Material During ‘America’s Got Talent’ Taping?"], ''[[Slashfilm]]'', 26 April 2013. Retrieved on 13 March 2015.</ref> Allegedly, Wilson auditioned for the talent show with a comedy act involving a mimed reenactment of an argument between a married couple. During the subsequent evaluation by the show's jury, judge [[Howie Mandel]]
mentioned having previously witnessed a strikingly similar act being performed by comedian [[Frank Nicotero]], who had been hired as a [[warm-up comedian]] for the programme and was present during the taping. Wilson denied having stolen the material. Although the judges decided to advance Wilson to the next round nonetheless, he was later disqualified and the footage was never aired.

Both Nicotero and Wilson later confirmed Sciretta's account of the events.<ref name="comic" />

==Filmography==

===Film===
{| class="wikitable sortable"
|-
! Year
! Title
! Role
! class="unsortable" | Notes
|-
| 1996
| ''Cyberstalker''
| Slick
| 
|-
| 2001
| ''The Process of Creative Deception ''
| Sheldon Smith
| Leading role
|-
| 2008
| ''[[The Hottie and the Nottie]]''
| Arno Blount
| Leading role
|-
| 2009
| ''[[Endless Bummer (film)|Endless Bummer]]''
| Animal
| Actor and writer
|-
| 2010
| ''Back Nine''
| Youth Group Leader
| Television movie
|-
| 2016
| ''Hidden in the Heart of Texas: The Official Hide and Go Seek Documentary''
| The Bellhop
| Post-production
|-
| 2017
| ''Larry Leap Year''
| [[Easter Bunny]]
| 
|}

===Television===
{| class="wikitable sortable"
|-
! Year
! Title
! Role
! class="unsortable" | Notes
|-
| 2006
| ''White Boyz in the Hood''
| Himself
| 1 episode
|-
| 2006
| ''[[Studio 60 on the Sunset Strip]]''
| Improv MC
| Episode: "The Wrap Party"
|-
| 2006, 2015
| ''[[Comics Unleashed]]''
| Himself
| 3 episodes
|-
| 2007
| ''[[Trading Spaces]]''
| Himself
| 5 episodes
|-
| 2007
| ''[[Life (NBC TV series)|Life]]''
| Lee
| Episode: "Let Her Go"
|-
| 2007
| ''[[Ugly Betty]]''
| Pizza Delivery Guy
| Episode: "Giving up the Ghost"
|-
| 2009
| ''[[Bones (TV series)|Bones]]''
| Tumbles
| Episode: "Double Trouble in the Panhandle"
|-
| 2009
| ''[[Comedy.tv]]''
| Himself
| 1 episode
|-
| 2010
| ''[[Modern Family]]''
| Head Elf
| Episode: "Undeck the Halls"
|-
| 2011
| ''[[Who Wants to Date a Comedian?]]''
| Himself
| 1 episode
|-
| 2012
| ''[[Undercovers (TV series)|Undercovers]]''
| Serge
| Episode: "Dark Cover"
|-
| 2012
| ''[[Mr. Box Office]]''
| Director
| Episode: "Somebody's Watching Me"
|-
| 2012-2013
| ''[[The Smoking Gun Presents: World's Dumbest...]]''
| Himself
| 7 episodes
|-
| 2013
| ''[[Kickin' It]]''
| Bert
| Episode: "Jack Stands Alone"
|-
| 2013
| ''[[Nickelodeon Animated Shorts Program|Charlie and Mr. Two]]''
| Mr. Two
| Voice. TV pilot.
|-
| 2014
| ''[[The Mindy Project]]''
| Vic Defruzzio
| Episode: "Caramel Princess Time"
|-
| 2014
| ''[[Hawaii Five-0 (2010 TV series)|Hawaii Five-0]]''
| Tree Lot Owner
| Episode: "Ke Koho Mamao Aku"
|-
| 2015
| ''[[Marry Me (U.S. TV series)|Marry Me]]''
| Lester
| Episode: "Change Me"
|-
| 2015
| ''[[The Odd Couple (2015 TV series)|The Odd Couple]]''
| Manager
| Episode: "The Ghostwriter"
|-
| 2015
| ''[[Fresh Off the Boat]]''
| Janitor
| Episode: "Very Superstitious"
|-
| 2018
| ''Electives''
| Shannon
| Post-production
|}

===Video games===
{| class="wikitable sortable"
|-
! Year
! Title
! class="unsortable" | Role
|-
| 2002
| ''[[Grand Theft Auto: Vice City]]''
| Imager
|-
| 2005
| ''[[The Warriors (video game)|The Warriors]]''
| Vargas
|-
| 2005
| ''[[Grand Theft Auto: Liberty City Stories]]''
| Joseph Daniel 'J.D' O'Toole
|}

==Discography==

* ''Pottymouth'' (2007)<ref>[https://itunes.apple.com/es/album/pottymouth-live/id260313953 "Pottymouth (Live)"], ''[[iTunes]]''. Retrieved: 14 March 2015.</ref>
* ''Hollywood Legend & Sex Symbol'' (2010)<ref>[https://itunes.apple.com/es/album/hollywood-legend-sex-symbol/id413469743 "Hollywood Legend & Sex Symbol"], ''[[iTunes]]''. Retrieved: 14 March 2015.</ref>

==Bibliography==

* Wilson, Greg. ''The Complete Guide to Stand-up: Everything you need to know, from open-mics to going pro! '', ISBN 9780692341315, Gregory D. Wilson / The Comedy Institute 2014.

==References==
{{reflist}}

*
*
*
*

==External links==
*{{IMDb name|1034313}}
*{{Official website|thegregwilson.com}}
*{{Official website|thecomedyinstitute.com}} of The Comedy Institute.

{{DEFAULTSORT:Wilson, Greg}}
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:American stand-up comedians]]
[[Category:American male actors]]
[[Category:American male voice actors]]
[[Category:People from Dallas]]