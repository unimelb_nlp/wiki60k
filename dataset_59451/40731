{{Orphan|date=September 2014}}

 <!-- Do not remove this line! -->
[[File:Saad Nadim.PNG|thumb|Saad Nadim the famous documentary film director]]

'''Saad Nadim''' ([[Arabic]]: سعد نديم) (February 17, 1920 - March 11, 1980) is an Egyptian Documentary Film Director. Considered by many the pioneer of documentary cinema in Egypt and the Arab world.

== Early Years ==

Born on February 19, 1920 in Bolaq an old suburb in ''[[Cairo]]'', ''[[Egypt]]'', Saad was born in the times following the [[Egyptian Revolution of 1919]] which lead to his father naming him after the famous leader of that revolution [[Saad Zaghloul]] who was an inspirational figure in Egyptian political scene at the time.

== Education ==

Saad joined "Al Farouq" School which was one of the revolting spots from which the demonstrations against colonialism, oppression and foreign privileges took place. Saad was dismissed from the school several times for incitement to demonstrate and eventually finished high school and joined the Faculty of Law in the Cairo university in 1939.

Not really fond of the academic nature of his studies in the faculty of law, young Saad decided to drop out and discontinue his law studies. He was deeply affected by [[Paul Rotha]]'s book "The Documentary Film" which later had a big impact on his career in the years to come, a book he borrowed from his cousin and future business partner, the famous film director [[Salah Abu Seif]].

Mesmerized by the world of documentary cinema, Nadim joined the Cinema Institution in 1944 where his passion for Documentary films evolved leading to a long and prosperous career in the documentary film industry.
[[File:Portrait of Saad Nadim holding his camera.png|thumb|left|Portrait of Saad Nadim by Croatian artist Hans Lesjak]]

== Work ==
In the summer of 1944 Saad went to Studio Masr to start his first job as an Editor Assistant in the montage section, which was headed by his cousin Salah Abu Saif. He quickly fell in love with the job as it fits his artistic tendency as well as his personal choice. He learned that arranging shots is one of the most important stages of the film making and that even if the editor has a weak piece of work he must transfer it by cutting, deleting and adjusting the tempo to a coherent and attractive piece of art.

Nadim worked as an assistant for the head of the montage section, Salah Abu Seif in many feature films and really benefited from the experience of the film "Saif Al-Galad" (the Whipper's sword). Cutting and arranging one shot after the other, this movie was growing more and more consistent and the result was that most of the critics praised the montage of the film when it was screened at Cannes Film Festival in 1946.

After less than a year of his work in the montage section, he had the opportunity to work on his first solo editing work in the film “Dunia” directed by Mohammed Karim as recognition of his abilities.
For over a quarter of a century he will be involved in the documentary filming as a director, screenwriter, production manager, tutor and the head manager of the National Center for documentary films. He worked on nearly 70 films in almost every category of documentaries: cultural, scientific, artistic, national, news and advertising.

== Culture and Leisure Group ==
[[File:Saad Nadim Lecture.PNG|thumb|Saad Nadim during a lecture in the Cinema institute]]
Saad Nadim was not just a literal director, Despite his serious interest in all the elements of documentary films, his desire for change was the force which drove him towards the cultural activities. Saad Nadim, Salah Abu Seif, Mohamed Odeh and Asaad Halim established a group and named it the "Culture & Leisure Group". They held seminars where they discussed serious issues concerning ideas, art and reality.

His public activity increased after he was elected as the Secretary of The Cinema Syndicate. As the Syndicate had an educational role for the first time in 1947 when Saad Nadim organized classical shows for German mime cinema.

== His Journey to London ==

The Egyptian Government has decided to send four young men to England to study various branches including Saad Nadim for directing. For Saad, England was the mother country of the documentary films which consists of various trends, cultural waves and psychological backgrounds. he was in need for this scholarship after the death of his wife -Bothaina al Nahry- the artist who he used to share his aspirations with which he had his first son Anaan.

In England Saad Nadim’s expectations were fulfilled as he found in London a great revival in the film making. In England the universities used to organize uncertified film courses where one of the industry leaders provide students with his theoretical and practical experience and Saad Nadim joined one of these courses at London University.

In the lectures he met “Ahmed Al-Hadary” a young Egyptian engineering student and a big cinema fan. -The dean of the Cinema Institute and the Director of the National Center for Film Culture in the future-. Al-Hadary was a prominent member of the “Egyptian Club" which used to sponsor student affairs in England and provide students with discounted tickets to attend theatre performances and musicals, allowing Saad Nadim, Salah Ezz El-Din with the company of Ahmed Al-Hadary off course to see the world classical theatre. And when he met the father of documentary films, [[John Grierson]] and talked to him about his passion for studying documentary film making, he prepared for him a comprehensive course for one and a half year in the motion picture unit attached to "The transportation Authority".

His social connections had grown with the documentary filmmakers in London. And when the English filmmaker [[Sir Arthur Elton, 10th Baronet]] who was working in “Shell” came to Egypt in 1953 he visited the company in Cairo and talked to the workers in the cinema section about that Egyptian man who will build a big legacy in the documentary industry and whom he met in London then he asked the public relations Department of the company to benefit from his energy when he return back from England. In England Saad met his second wife, Sheila, with whom he had two children, Raamy and Magda.

== Complete Filmography ==

1.	Did you know, 1946

2.	Fishing City

3.	Arabian Horses, 1947

4.	Modern Egypt, 1947

5.	Sugar Industry in Egypt

6.	Kafr El-Dawwar Factories

7.	Alexandria, 1950

8.	Cairo, 1955

9.	Aswan,1959

10.	To Helwan, 1959

11.	The Red Sea, 1959

12.	Kom Ombo

13.	Edfu

14.	A day in the countryside, 1949

15.	 The Industry

16.	The Update

17.	Colorful Wings, 1949

18.	 Archaeological Discoveries

19.	 The Military Police, 1954

20.	 In Suez Canal Area, 1954

21.	Signing the Evacuation Agreement

22.	The Victory Parade, 1955

23.	 The 6th of October, 1974

24.	Towards a Better Nutrition, 1954

25.	The Mission of the Syrian Press, 1955

26.	Tanseek al Quahira

27.	The Story of a Book, 1959

28.	Building the Future, 1955

29.	 Let the World Witness

30.	Evacuation Celebrations, 1956

31.	Petroleum in Egypt

32.	 Museum of Civilization Series

•	Amada & Ibrim

•	Kalabsha & Beit Al Wadi

•	Abu Simbel

•	Saving Philae Temple

33.	Railroad Museum

34.	The Art of Our Country Series

35.	 The Nubians

36.	A Story from the Nuba, 1963

37.	A trip to the Nuba Series, 1961

•	Philae

•	Kalbsha and Beit Al Wali

•	From Dandar to El-Subo3

•	From Amada to Ibrim

•	Abu Simbel

•	Heritage of the Humanity

•	Life of the Residents,

38.	The Artist Ragheb Ayad, 1965

39.	Contemporary Egyptian Art, 1969

40.	The Egyptian Sculptor Anwar Abdel Mawla, 1970

41.	Full-time in photography and sculpture

42.	The Egyptian In 50 years – 1975

43.	10 October and Culture – 1975

44.	Bhzaan in The Garden, 1976

45.	Shame on America

46.	We Are Not Alone

47.	Eye Witness

48.	Aggression towards The Arab Nation, 1968

49.	Peace Not Surrender, 1957

50.	The Road to Peace, 1966

51.	Defending Peace, 1972

52.	My Beautiful Country, 1971

53.	The Suez Canal, 1979

54.	Decoding Philae Temple, 1975

55.	Between Philae Island and Aiglka, 1977

56.	From Philae to Aiglka, 1979

57.	Diseases Spreaders, 1948

58.	Al-Mowasah Hospital, 1949

59.	Fayoum Water, 1972

60.	Fayoum Excavations

61.	Suez Canal Convention, 1955

62.	Culture in the Way to Evolution, 1960

63.	El-Mawled, 1965

64.	Youth Festival, 1974

== Awards ==

- Film Review Award, 1960

- Scenario and directing awards from the Ministry of Culture, 1964

- Certificate of appreciation from the Leipzig Festival for "A story from Nuba"

== Sources ==

- "Saad Nadim: The Documentary Films Pioneer" – Kamal Ramzy

- "The Biography of the Documentary Films Pioneer Saad Nadim" – Mahmoud Sami Atallah

- Salah Ezz eldine, [http://unesdoc.unesco.org/images/0018/001849/184951eb.pdf "Arab Documentary Film Production and the use of music in Arab films"], '' United Nations Educational, Scientific and Cultural Organization Report'', November 29, 1962

== References ==

{{reflist}}

[[Category:1920 births]]
[[Category:1980 deaths]]
[[Category:Documentary film producers]]
[[Category:Egyptian film directors]]
[[Category:People from Cairo]]
[[Category:Egyptian documentary filmmakers]]