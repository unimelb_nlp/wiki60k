{{italic title}}
''The '''Medical Record''': A Weekly Journal of Medicine and Surgery'' was founded in 1866 by [[George Frederick Shrady, Sr.]] who was its first editor-in-chief.<ref>{{cite book
  | last = Shrady
  | first = George Frederick Shrady
  | title = General Grant's Last Days; with a Short Biographical Sketch of Dr. Shrady
  | location = New York
  | year = 1908
  | url = http://twister.lib.siu.edu/projects/usgrant/hist/shrady.html
  | oclc = 14139666}}.</ref>  [[Thomas Lathrop Stedman]] became assistant editor in 1890 and editor-in-chief in 1897.<ref>{{Cite news
  | title = Thomas Lathrop Stedman
  | newspaper = [[HighLights: A Quarterly Publication for Health Science Booksellers]]
  | publisher = [[Lippincott Williams & Wilkins]]
  | date = Winter 2005
  
  | url = http://www.lww.com/static/docs/Highlights_Winter_2005.pdf
  | postscript = <!--None-->}}</ref>

It was published in [[New York City]].  It was later published by the [[Washington Institute of Medicine]].

Many issues of ''Medical Record'' are now in the public domain and available through the [[Google Books]] project.

{{cquote|Started in 1866, the ''Medical Record'' has for forty-six years held the first place among medical weeklies in America. Impartial, judicial, and scientific, its single aim has been to furnish to the Medical Profession an independent, enterprising, and progressive medical newspaper conserving the best interests of the profession.

The ''Medical Record'' believes that the proper scope of a medical newspaper is all that concerns the Science and Practice of Medicine and Surgery, and all that concerns the Physician and Surgeon. It is conducted on the broadest lines, sparing no expense in the employment of its Editorial Staff, in collecting news, in maintaining correspondents in various parts of the world, and in securing exclusive reports of meetings by cable and telegraph.

The ''Medical Record'' is independent of the control of any group of individuals or of any personal policy.  It is controlled by the best judgment that long experience of the needs of the better class of American physicians can give. Such experience teaches that the enlightened sentiment of the Profession is the only safe guide in this respect.  |40px|40px|&#160;|1912 Advertisement for ''Medical Record''<ref>{{cite book
  | title = State Board Examination Questions & Answers of the United States and Canada: A Practical Work Giving Authentic Questions and Authoritative Answers in Full That Will Prove Helpful in Passing State Board Examinations: Reprinted from the Medical Record
  | publisher = [[William Wood & Company]]
  | location = New York
  | date = September 1912
  | page = 824
  | edition = 4th
  | url = https://books.google.com/books?id=w09GGHdCLuwC&printsec=titlepage
  | oclc = 14788513
  | author1 = Record, Medical}}.</ref>}}</div>

== References ==
{{reflist}}

[[Category:Publications established in 1866]]
[[Category:General medical journals]]
[[Category:1866 establishments in New York]]


{{med-journal-stub}}