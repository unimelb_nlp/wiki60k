{{Orphan|date=June 2016}}
{{italic title}}
{{Taxobox 
| name = ''Ninox novaeseelandiae ocellata''
| image = 
| image_alt = 
| image_caption = 
| status = LC
| status_system = iucn3.1
|status_ref =<ref>BirdLife International (2014). "Ninox boobook". IUCN Red List of Threatened Species. Version 2014.2. International Union for Conservation of Nature. Retrieved 2014-08-24</ref>
| regnum = [[Animal]]ia
| phylum = [[Chordate|Chordata]]
| classis = [[bird|Aves]]
| ordo = [[Strigiformes]]
| familia = [[Strigidae]]
| genus = ''[[Ninox]]''
| species = ''[[Ninox novaeseelandiae|N. novaeseelandiae]]''
| subspecies = '''''N. n. ocellata'''''
| trinomial = ''Ninox novaeseelandiae ocellata''
| trinomial_authority = 
}}

'''''Ninox novaeseelandiae ocellata''''' is a subspecies of [[southern boobook]]. The southern boobook is the most common and smallest owl on the Australian mainland.<ref name="Olsen">{{cite book|last1=Olsen|first1=Jerry|title=Australian High Country Owls|date=2011|publisher=CSIRO|location=Collingwood, Victoria|isbn=9780643104112|pages=15–17|chapter=What is a Southern Boobook}}</ref> The sub species is characterised by its lighter colour than other sub species, and is generally found in a wide range of habitats (adapting very well to human activities). Ocellata is the smallest of the boobook subspecies as it is found in the warmer western parts of Australia (north-western New South Wales/western Queensland across to Western Australia).<ref name="Simpson">{{cite book|last1=Simpson|first1=Ken|last2=Day|first2=Nicolas|title=Field Guide to the Birds of Australia|date=2010|publisher=Penguin Group|location=Melburne, Victoria|isbn=9780670072316|pages=156|edition=8th}}</ref> although it is not found in arid areas.<ref name="Olsen" /> 
The Boobook can be commonly known as the ‘mopoke’ due to its call (double hoot ‘boo-book’). This owl is often confused with the barking owl due to their similar appearance, however the boobook is a fair amount smaller. The boobook feeds on insects and small vertebrates (usually around the size of a mouse).<ref name="Konig">{{cite book|last1=König|first1=Claus|last2=Weick|first2=Friedhelm|last3=Becking|first3=Jan-Hendrik|title=Owls of the World|date=2009|publisher=A&C Black|isbn=9781408108840|pages=457–59}}</ref>

== Physical description ==
The ''ocellata'' subspecies of southern boobook differs from other subspecies in its plumage colouring, which is lighter than the mainland birds.<ref name="Konig" />  southern boobooks have brown plumage on their backs and cream to rufous brown stomachs, often heavily streaked or spotted with white, they have markings on the scapulars and spots on the wings.<ref name="Konig" />

All boobooks have a ‘facial mask’ with dark chocolate brown circles around the eyes, these circles are outlined by lighter triangle shaped areas on the throat and forehead. The eyes are large and grey-green or yellow, and the bill grey.<ref name="Konig" />
Being the smallest species of owl on the Australian mainland the boobook weighs in at 200-300g, smaller birds being found in warmer areas (ocellata tends to be smaller), and measuring 27–37&nbsp;cm in length.<ref name="Konig" /> Boobooks are often confused with the barking owl, however the easiest way to differentiate is by size, the boobook is a fair bit smaller.

Mayr found that colour in southern boobooks may directly correlate with rainfall, with the paler species being found in areas of lower rainfall, indicating the semi arid regions of Australia (the range of the ocellata sub species).<ref>Mayr, Ernst (1943). "Notes on Australian Birds (II)". Emu 43 (1): 3–17. doi:10.1071/MU943003</ref>

== Call ==
The boobook has a very distinctive double hoot ‘boo-book’ with the second note being lower.<ref name="Simpson" /> This call has earned it the common name of ‘mopoke’.<ref name="BIB">{{cite web|title=Southern Boobook|url=http://www.birdsinbackyards.net/species/Ninox-novaeseelandiae|website=Birds in Backyards|accessdate=5 June 2016}}</ref>

== Distribution and Habitat ==
Subspecies ocellata tends to inhabit the warmer areas of Australia, from north-western New South Wales/western Queensland spanning across to Western Australia,<ref name="Simpson" /> although it is rarely found in the more arid regions.<ref name="Konig" />
The boobook is found in a wide variety of habitats, from semi-arid areas, to scrubland, open woodland and forests.<ref name="Konig" /> This species has adapted to human influence very well, being found in any area where there are scattered trees (such as suburban areas or farmland).<ref name="Konig" />

== Diet ==
The boobook is predominately a nocturnal hunter, although morning and afternoon hunts can occasionally occur (on dull or overcast days).<ref name="Konig" /> The Boobook feeds on small vertebrates and insects (generally nocturnal species of insect and small vertebrates approximately the size of a mouse) with prey being located by observation from a high perch, the boobook can catch prey both aerially and on land.<ref name="Konig" />

== Breeding ==
The southern boobook breeds from September until February, with peak breeding happening in October.<ref name="BIB" /> They nest in tree hollows high above the ground, and line nests with leaves and small twigs. Clutch sizes range from 3-5 eggs<ref name="BIB" /> and are incubated for a period of approximately 31 days, and have a nestling period of 42 days,<ref name="NINOX2" /> females incubate the eggs alone yet both sexes assist with feeding.<ref name="BIB" /> Little is known about the life span of these birds, however it is known that they reach breeding age at 2–3 years of age. The last recorded life span of a pure sub species of Southern Boobook was at least 12 years of age, with hybrid individuals reaching a recorded 18 years of age <ref name="NINOX2">{{cite web|author1=Department of the Environment|title=Ninox novaeseelandiae undulata — Norfolk Island Boobook, Southern Boobook (Norfolk Island)|url=http://www.environment.gov.au/cgi-bin/sprat/public/publicspecies.pl?taxon_id=26188|website=Species Profile and Threats Database|accessdate=5 June 2016|date=2016}}</ref>

==References==
{{reflist}}

[[Category:Ninox]]