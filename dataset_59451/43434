<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=661
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Airliner|Passenger transport]]
 | national origin= [[France]]
 | manufacturer=[[S.N.C.A.N.]]
 | designer=
 | first flight=18 July 1937
 | introduced=
 | retired=
 | status=
 | primary user=Air Afrique
 | number built=2
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Potez 661''' was a four-engined metal low-wing [[monoplane]] [[airliner]] developed in [[France]] just before [[World War II]]. The single example flew with [[Air Afrique pre war|Air Afrique]] on French colonial routes.

==Design and development==

In 1936 the well-established [[Potez]] company became part of the Société Nationale de Constructions Aéronautique du Nord ([[S.N.C.A.N.]]), under the Law for the Nationalisation of Military Industries.<ref>{{harvnb|Grey|1972|pages=108c}}</ref>  In 1937 they produced their first four-engined aircraft, the Type 661.  This was<ref name="JAWA38">{{harvnb|Grey|1972|pages=110c}}</ref> a commercial machine with seating for up to twelve passengers.  It was a low wing cantilever, almost all-metal monoplane.  The wing tapered with a nearly straight trailing edge<ref name ="Flight2"/> that carried outboard balanced ailerons and split trailing edge flaps over the whole of the centre section.  The four 220&nbsp;hp (164&nbsp;kW)  [[Renault 6Q]] inverted inline engines were conventionally mounted of the front wing spar and drove variable-pitch twin-bladed propellers.

The fuselage<ref name="JAWA38"/> was a metal monocoque, with a port side passenger door aft of six windows<ref name ="Flight2"/>  on each side, one per seat. Though the standard seat arrangement was for twelve, two seats could be removed to allow the installation of chaises-longues for longer flights. The pilots' cabin was enclosed, with side by side dual control seating.  The tail unit carried twin vertical [[endplate]] fins, slightly oval on a tailplane that had strong dihedral.  The balanced rudders and elevators were metal structures with the only fabric covering used on the aircraft.  The elevators carried trim tabs.  There was a small tailwheel, the main undercarriage retracting into the inner engine nacelles.

==Operational history==
The first flight was on 18 July 1937.<ref>[http://www.aviafrance.com/876.htm AviaFrance: Potez 661]</ref>  By August 1938 the 661 had completed its Air Ministry tests<ref>[http://www.flightglobal.com/pdfarchive/view/1938/1938%20-%202284.html ''Flight'' 11 August 1938 p.128]</ref> and by April 1939 its 100 hr "endurance" tests.<ref name="Flight"/>  In mid-April it was handed over to Air Afrique<ref>{{harvnb|Grey|1972|pages=75–6a}}</ref> and had made its first flight<ref name ="Flight">[http://www.flightglobal.com/pdfarchive/view/1939/1939%20-%201275.html ''Flight'' 24 April 1939 p.436]</ref> on the trans-Africa route from [[Dakar]] to [[Pointe Noire]].  Though only one was built before the war, it gained the reputation of being an economical transport.<ref name ="Flight2">[http://www.flightglobal.com/pdfarchive/view/1943/1943%20-%202359.html ''Flight'' 30  September 1943 p.436]</ref>  There were suggestions, in war-time England at least that the Potez 661 or its more powerful development the [[Potez 662]] might be built in occupied France for German use,<ref name ="Flight2"/> but there is no evidence that any more were produced.

==Variants==
;Potez 661: The initial prototype powered by 4x {{convert|220|hp|kW|abbr=on|disp=flip}} [[Renault 6Q]]-02 air-cooled inverted in-line engines
;[[Potez 662]]: A second prototype powered by 4x {{convert|660|hp|kW|abbr=on|disp=flip}} [[Gnome & Rhône 14M-5]] radial engines, with equal tapered outer wings to allow for changes in the centre of gravity with the heavier radial engines.
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (Potez 661) ==
{{aerospecs
|ref={{harvnb|Grey|1972|pages=110c}}
|met or eng?=met<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->

|crew=2
|capacity=12
|length m=16.6
|length ft=54
|length in=5
|span m=22.5
|span ft=73
|span in=10
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=4.4
|height ft=14
|height in=5
|wing area sqm=64
|wing area sqft=689
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=3,986
|empty weight lb=8,788
|gross weight kg=6,326
|gross weight lb=13,946
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->

|eng1 number=4
|eng1 type=[[Renault 6Q]] air cooled 6-cylinder inverted in-line 
|eng1 kw=164<!-- prop engines -->
|eng1 hp=220<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|max speed kmh=326
|max speed mph=203
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=300<!-- if max speed unknown -->
|cruise speed mph=186<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=1000
|range miles=620
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->

|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{commons category|Potez}}
;Citations
{{reflist}}
;Bibliography
{{refbegin}}
*{{cite book |title= Jane's All the World's Aircraft 1938|last= Grey |first= C.G. |coauthors= |edition= |year=1972|publisher= David & Charles|location= London|isbn=0-7153-5734-4|ref=harv }}
{{refend}}

<!-- ==External links== -->
{{Potez aircraft}}

[[Category:French airliners 1930–1939]]
[[Category:Potez aircraft|066]]