{{good article}}
{{Infobox song	
| Name     = Army of Me
| Artist   = [[Christina Aguilera]]
| Album    = [[Lotus (Christina Aguilera album)|Lotus]]	
| track_no = 2
| Recorded = 2012
| Format   = [[Music download|Digital download]]
| Genre    = {{flat list |
*[[Dance-pop]] 
*[[eurodance]]}} <!-- DO NOT REMOVE any of these genres. Each one is reliably sourced from a reputable reviewer. Unexplained genre changes constitute vandalism.  -->
| Length   = 3:26
| Label    = [[RCA Records|RCA]]
| Writer   = [[Christina Aguilera]], Jamie Hartman, David Glass, Phil Bentley
| Producer = [[Tracklacers]], Jamie Hartman <small>(co-producer)</small>, Christina Aguilera <small>(vocals)</small>
}}

"'''Army of Me'''" is a song recorded by American singer-songwriter [[Christina Aguilera]] for her seventh studio album, ''[[Lotus (Christina Aguilera album)|Lotus]]'' (2012). It was co-written by Aguilera with Jamie Hartman, David Glass and Phil Bentley, with production done by [[Tracklacers]] and Jamie Hartman. Described by Aguilera as part two to her 2002 single "[[Fighter (song)|Fighter]]", she decided to record the song so that her newer, younger fans would have an empowering song to listen in case they were unfamiliar with her previous work. The song combines [[dance-pop]] and [[Eurodance|euro-dance]] genres; its [[instrumentation]] incorporates drum beats and rock guitars. "Army of Me" garnered mixed reviews from [[Music journalism|music critics]]. Many praised Aguilera's strong delivery of the song's message and branded it a potential single, while others criticized it for being too similar to "Fighter". Upon the release of ''Lotus'', the song debuted on the South Korea [[Gaon Singles Chart|international singles chart]] at number 103 with [[Music download|digital download]] sales of 2,689. Aguilera has performed the song at the [[American Music Awards of 2012|40th American Music Awards]] in the United States.

==Background and recording==
Following the release of her sixth studio album, ''[[Bionic (Christina Aguilera album)|Bionic]]'' (2010),<ref name="Idolator1">{{cite news|last=Bain|first=Becky|url=http://idolator.com/6836852/christina-aguilera-your-body-single-listen|title=Christina Aguilera’s Demo Of New Single 'Your Body' Surfaces: Listen|work=''[[Idolator (website)|Idolator]]''|publisher=[[Buzz Media]]|date=August 23, 2012|accessdate=September 14, 2012}}</ref> Aguilera filed for divorce from her husband [[Jordan Bratman]], starred in her first feature film, ''[[Burlesque (2010 American film)|Burlesque]]'' and recorded [[Burlesque: Original Motion Picture Soundtrack|the accompanying soundtrack]].<ref name="Billboard Cover Story">{{cite news|last=Hampp|first=Andrew|url=http://www.billboard.com/articles/news/474983/christina-aguilera-billboard-cover-story|title=Christina Aguilera: Billboard Cover Story|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|date=September 21, 2012|accessdate=September 23, 2012}}</ref> She then became a coach on [[NBC]]'s singing competition show ''[[The Voice (U.S. TV series)|The Voice]]''<ref name="Billboard Cover Story"/> and appeared as a featured artist on [[Maroon 5]]'s single "[[Moves like Jagger]]" (2011), which spent four weeks atop the US ''[[Billboard (magazine)|Billboard]]'' [[Billboard Hot 100|Hot 100]] chart.<ref name="Billboard1">{{cite news|last=Schneider|first=Marc|url=http://www.billboard.com/articles/news/496285/christina-aguilera-cee-lo-hit-the-studio|title=Christina Aguilera, Cee Lo Hit the Studio|work=Billboard|publisher=Prometheus Global Media|date=April 11, 2012|accessdate=September 14, 2012}}</ref> Following these events, Aguilera announced that had plans to begin production of her seventh album, stating that she wanted high quality and "personal" songs for the record.<ref name="Billboard1"/> Regarding the creative direction, she revealed that the album would be a "culmination of everything I've experienced up until this point&nbsp;... I've been through a lot since the release of my last album, being on ('The Voice'), having had a divorce&nbsp;... This is all sort of a free rebirth for me."<ref name="Yahoo1">{{cite news|last=Elber|first=Lynn|url=http://music.yahoo.com/news/christina-aguilera-album-rebirth-121427390.html|title=Christina Aguilera: New album is a 'rebirth'|work=[[Yahoo! Music]]|publisher=[[Yahoo!]]|accessdate=September 14, 2012|date=August 28, 2012}}</ref> She further said "I'm embracing many different things, but it's all feel-good, super-expressive [and] super-vulnerable."<ref name="Yahoo1"/> Aguilera continued to say that the album would be about "self&ndash;expression and freedom" because of the personal struggles she had overcome during the last couple of years.<ref name="LATimes1">{{cite news|last=Kennedy|first=Gerrick D.|url=http://www.latimes.com/entertainment/music/posts/la-et-ms-christina-aguilera-readies-new-album-lotus-20120913,0,6597387.story|title=Christina Aguilera readies new album 'Lotus'|work=[[Los Angeles Times]]|publisher=[[Tribune Company]]|date=September 13, 2012|accessdate= October 5, 2012}}</ref> Speaking about her new material during an interview on ''[[The Tonight Show with Jay Leno]]'' in 2012, Aguilera said that the recording process for ''Lotus'' was taking a while because "I don't like to just get songs from producers. I like them to come from a personal place&nbsp;... I'm very excited. It's fun, exciting, introspective, it's going to be great".<ref>{{cite news|url=http://www.digitalspy.co.uk/music/news/a373459/christina-aguilera-new-album-is-quality-over-quantity.html|title=Christina Aguilera: 'New album is quality over quantity'|work=[[Digital Spy]]|publisher=[[Hearst Corporation]]|date=May 27, 2012|accessdate= October 5, 2012}}</ref>

Recorded at [[Henson Recording Studios]], Hollywood, California, and Radley Studios, Los Angeles, California, by Justin Stanley, "Army of Me" was co-written by Aguilera with Jamie Hartman, David Glass and Phil Bentley.<ref name="Notes"/> It was produced by [[Tracklacers]] and it was co-produced by Hartman.<ref name="Notes"/> Aguilera's vocals were recorded by Oscar Ramirez at The Red Lips Room in Beverly Hills in California.<ref name="Notes"/> Programming was carried out by Steve Daly and John Keep, while strings were composed by Hartman.<ref name="Notes"/> In an interview with Andrew Hampp for ''[[Billboard (magazine)|Billboard]]'', Aguilera explained how her role on ''The Voice'' has allowed her to reach a new generational audience who may not be familiar with her past work, including songs such as her 2002 single, "[[Fighter (Christina Aguilera song)|Fighter]]".<ref name="Billboard Cover Story"/> When asked if some of the songs on ''Lotus'' feature themes which are similar to that of her 2002 album, ''[[Stripped (Christina Aguilera album)|Stripped]]'', Aguilera responded by saying that "Army of Me" is what she describes as "Fighter 2.0".<ref name="Billboard Cover Story"/><ref name="MTVReview">{{cite news|last=Garibaldi|first=Christina|url=http://www.mtv.com/news/articles/1696891/christina-aguilera-lotus-preview.jhtml|title=Christina Aguilera Previews Five Songs From Lotus, Her 'Labor Of Love'|work=[[MTV News]]|publisher=[[Viacom]]|date=November 6, 2012|accessdate=December 9, 2012}}</ref>

<blockquote>Absolutely. There's a song called 'Army of Me,' which is sort of a 'Fighter 2.0.' There is a new generation of fans from a younger demographic that might not have been with me all the way but that watch me on the show now. I feel like every generation should be able to enjoy and have their piece of 'Fighter' within. This time, the way it musically came together it just felt right for this time and this generation. There's always going to be a fighter in me getting through some obstacle and some hurdle. All these 6-year-olds who know me from pushing my button and turning around in a big red chair who weren't around for the actual 'Fighter,' this is my chance to recharge it, rejuvenate it and do something modernized for them.<ref name="Billboard Cover Story"/><ref name="MTVReview"/></blockquote>

==Composition and lyrics==

"Army of Me" is an up-tempo [[dance-pop]]  and [[Eurodance|euro-dance]] song which lasts for a duration of {{Duration|m=3|s=26}} (three minutes and 26 seconds).<ref name="MTVReview"/><ref name="VirginMediaReview">{{cite news|last=Horton|first=Matthew|url=http://www.virginmedia.com/music/reviews/christina-aguilera-lotus.php|title=Christina Aguilera: Lotus Album Review&nbsp;— Virgin Media|publisher=[[Virgin Media]]|date=November 12, 2012|accessdate=December 10, 2012}}</ref><ref name="BillboardLotusReview">{{cite web|last=Hampp|first=Andrew|url=http://www.billboard.com/articles/review/1066766/christina-aguilera-lotus-track-by-track-review|title=Christina Aguilera, 'Lotus': Track-By-Track Review|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|date=November 12, 2012|accessdate=November 27, 2012}}</ref><ref name="LotusGBiTunes">{{cite web|url=https://itunes.apple.com/gb/album/lotus-deluxe-version/id571725689|title=iTunes&nbsp;— Music&nbsp;— Lotus by Christina Aguilera|work=[[iTunes Store]] (GB)|publisher=[[Apple Inc.|Apple]]|date=November 9, 2012|accessdate=November 24, 2012}}</ref> Instrumentation consists of a "pounding" drum beat and "[[Rock music|rocky]]" guitars.<ref name="Lotus4MusicReview"/> Aguilera presents herself as a fighter and tells her ex-boyfriend that she is stronger than he is in the lyrics "So how does it feel to know that I beat you?/ That I can defeat you?"<ref name="BillboardLotusReview"/><ref name="Lotus4MusicReview">{{cite news|last=Younie|first=Chris|title=News: Review: Christina Aguilera &ndash; Lotus|url=http://www.4music.com/news/news/6712/Review-Christina-Aguilera-Lotus|work=[[4Music]]|publisher=[[Channel 4]]|date=November 2, 2012|accessdate=November 2, 2012}}</ref> Although Aguilera is heartbroken by the decision to split up with her ex-boyfriend, she is not a broken person.<ref name="Lotus4MusicReview"/> The lyrics "One of me is wiser/ One of me is stronger/ One of me's a fighter/ And there's a thousand faces of me/ We're gonna rise up for every time you broke me/ You're gonna face an army of me" are similar to those performed by Aguilera on "Fighter", although of "Army of Me" does not [[sampling (music)|sample]] any of the song.<ref name="MTVReview"/><ref name="MikeWass"/> Aguilera sings "Now that I'm wiser/ Now that I'm stronger/ Now that I'm a fighter/ There's a thousand faces on me" over a "thumping" beat, and belts the line "We're gonna rise up for every time you broke me" on the chorus.<ref name="LotusDSReview">{{cite web|last=Copsey|first=Robert|url=http://www.digitalspy.co.uk/music/thesound/a435214/christina-aguileras-new-album-lotus-first-listen.html|title=Christina Aguilera's new album 'Lotus': First listen|work=Digital Spy|publisher=Hearst Corporation|date=November 2, 2012|accessdate=November 27, 2012}}</ref>
The song was covered by recording artist [[Anastacia]] for her compilation album [[Ultimate Collection (Anastacia album)|Ultimate Collection]].

==Critical reception==
"Army of Me" garnered mixed reviews from music critics. Andrew Hampp for ''Billboard'' and Chris Younie for [[4Music]] both though that "Army of Me" could have been a potential single, the latter of whom writing "This track must be a future single."<ref name="BillboardLotusReview"/><ref name="Lotus4MusicReview"/> Younie continued to write that the song "captivates and excites right from the very start", has a "euphoric" energy and is the type of "angry" pop song that [[Kelly Clarkson]] "would give her right arm for".<ref name="Lotus4MusicReview"/> Sarah Rodman of ''[[The Boston Globe]]'' described it as a "[[Gloria Gaynor]]-meets-[[Depeche Mode]] dance of anger."<ref name="LotusBostonGlobeReview">{{cite news|last=Rodman|first=Sarah|url=http://bostonglobe.com/arts/music/2012/11/13/album-review-christina-aguilera-lotus/sxrXvsFk8uy356lbt5YOBL/story.html|title=Christina Aguilera refocuses on her own voice in 'Lotus'|work=[[The Boston Globe]]|publisher=[[The New York Times Company]]|location=Boston|date=November 13, 2012|accessdate=November 17, 2012}}</ref> Mike Wass for [[Idolator (website)|Idolator]] wrote that although "Army of Me" is a "quality" song, it is an album filler.<ref name="MikeWass">{{cite news|last=Wass|first=Mike|url=http://idolator.com/7001572/christina-aguilera-lotus-album-review|title=Christina Aguilera's 'Lotus': Album Review'|work=''[[Idolator (website)|Idolator]]''|publisher=[[Buzz Media]]|date=November 13, 2012|accessdate=December 9, 2012}}</ref> Writing that it sounds as though it would have been a good song to include on her previous studio album, ''[[Bionic (Christina Aguilera album)|Bionic]]'', Wass thought that Aguilera was "not exaggerating" when she refers to it as "Fighter 2.0".<ref name="MikeWass"/> He concluded his review by saying that although her vocals are sparse, it is a "quirky" addition to ''Lotus''.<ref name="MikeWass"/> ''[[The A.V. Club]]''{{'}}s Annie Zaleski described Aguilera as a "playful" and "sassy techno diva" on "Army of Me".<ref name="LotusAVClubReview">{{cite news|last=Zaleski|first=Annie|url=http://www.avclub.com/articles/christina-aguilera-lotus,88558/|title=Christina Aguilera: Lotus|work=[[The A.V. Club]]|publisher=[[The Onion]], Inc|location=Chicago|date=November 13, 2012|accessdate=November 17, 2012}}</ref>

Kitty Empire of ''[[The Observer]]'' also thought that it shares similarities with "Fighter". She cited [[Army of Me (Björk song)|the Björk song of the same name]] as another influence, due to its "emotional territory"<ref name="ObserverReview">{{cite news|last=Empire|first=Kitty|authorlink=Kitty Empire|date=November 10, 2012|url=https://www.theguardian.com/music/2012/nov/11/christina-aguilera-lotus-review|title=Christina Aguilera: Lotus&nbsp;– review|work=[[The Observer]]|publisher=[[Guardian Media Group]]|accessdate=November 17, 2012}}</ref> Robert Copsey for [[Digital Spy]] described the song as "nothing we haven't heard from her before, but there's an urgency to it that suggests Christina needed to get it out of her system",<ref name="LotusDSReview"/> while Matthew Horton of [[Virgin Media]] wrote that Aguilera sounds as though she is declaring war.<ref name="VirginMediaReview"/> Michael Gallucci for PopCrush was critical of the song, writing that it sounds like a [[Cher]] disco song which features Aguilera "overworking" her vocal cords.<ref name="LotusPopCrushReview">{{cite web|last=Gallucci|first=Michael|url=http://popcrush.com/christina-aguilera-lotus-album-review/|title=Christina Aguilera, 'Lotus' - Album Review|work=PopCrush|publisher=[[Townsquare Media]]|date=November 2012|accessdate=November 29, 2012}}</ref> Melissa Maerz of ''[[Entertainment Weekly]]'' found the song's message to be confusing and did not understand who exactly Aguilera is supposed to be rising up against on the song, writing "She hollers 'And we're gonna rise up.../ For every time you wronged me/ Well, you're gonna face an army, army of me.' Which begs the question: Rise up against whom? Is the whole world really out to get her, or is this just an excuse to wear camouflage hot pants?"<ref name="LotusEWReview">{{cite journal|last=Maerz|first=Melissa|url=http://www.ew.com/ew/article/0,,20643719,00.html|title=Lotus - Review - Christina Aguilera Review|issue=1232-1233|journal=[[Entertainment Weekly]]|location=New York|date=November 9, 2012|accessdate=November 17, 2012}}</ref>

==Live performance==

Aguilera performed "Let There Be Love" for the first time at the [[American Music Awards of 2012|40th American Music Awards]] on November 18, 2012, held at the [[Nokia Theatre L.A. Live|Nokia Theatre]] in [[Los Angeles, California]].<ref name="MTV1AMA">{{cite web|last=Warner|first=Kara|url=http://www.mtv.com/news/articles/1695198/christina-aguilera-american-music-awards-performance-preview.jhtml|title=Christina Aguilera Teases AMA Performance Inspired By Lotus Cover|work=MTV News|publisher=Viacom|date=October 9, 2012|accessdate=November 12, 2012}}</ref> As one of the first singers to be announced as a performer at the award ceremony on October 19, 2012, Aguilera sang "Army of Me" as part of a medley with two other tracks from ''Lotus'': "[[Lotus Intro]]" and "[[Let There Be Love (Christina Aguilera song)|Let There Be Love]]".<ref name="DailyMail1AMA">{{cite web|last=Simpson|first=Leah|url=http://www.dailymail.co.uk/tvshowbiz/article-2235064/American-Music-Awards-2012-Christina-Aguilera-spills-star-spangled-bodysuit-performs-AMAs.html|title=Christina Aguilera is unapologetic about her curvy figure as she spills out of two costume changes at the AMAs|work=[[Daily Mail]]|publisher=[[Daily Mail and General Trust]]|date=November 19, 2012|accessdate=November 27, 2012}}</ref> During an interview with [[MTV News]], Aguilera revealed what the performance would be like and the creative direction behind it:

<blockquote>It's very exciting. It's definitely going to be a reflection of what Lotus means to me. If you take that album cover 
and give it a little performance twist, I'll bring that album cover to life, so it's going to be really fun. I can't give too much away about the songs, but it's definitely going to represent the album because the album is very multilayered. It doesn't represent 'Your Body' as a single tone. It has its ballads; and everything comes from a very sincere, deep&ndash;rooted place whether it's having fun or being vulnerable.<ref name="MTV1AMA"/></blockquote>

Wearing a "fuller&ndash;figure" "cinched in corset" designed by The Blonds, who also design outfits for [[Lady Gaga]],<ref name="DailyMail1AMA"/> Leah Simpson for the ''[[Daily Mail]]'' wrote that Aguilera put a "sexy twist on patriotism with a star&ndash;spangled bodysuit and managed to get a few pulses racing in the over&ndash;the&ndash;top ensemble."<ref name="DailyMail1AMA"/> The performance featured dance routines and dancers wearing "torture bags labelled the words 'Freak' and 'Queen' over their heads."<ref name="DailyMail1AMA"/> Bruna Nessif for [[E! Online]] described the performance as "interesting," and noted that the theme "to celebrate everyone for who they are" was similar to the moral content presented on Gaga's album ''[[Born This Way (album)|Born This Way]]'' (2011).<ref name="EAMA">{{cite web|last=Nessif|first=Bruna|url=http://uk.eonline.com/news/364242/2012-american-music-awards-best-worst-from-the-show-plus-full-winner-s-list|title=2012 American Music Awards: Best & Worst From the Show, Plus Full Winner's List|work=[[E! Online]]|publisher=[[NBCUniversal]]|date=November 18, 2012|accessdate=November 27, 2012}}</ref> As Aguilera finished her set, she was joined on stage by [[Pitbull (rapper)|Pitbull]] to perform his song "[[Feel This Moment]]", on which she is a featured artist.<ref name="DailyMail1AMA"/>

==Credits and personnel==
;Recording
*Recorded at Henson Recording Studios, Hollywood, California; Radley Studios, Los Angeles, California.
*String, Bass and Piano recorded at Henson Recording Studios, Hollywood, California.
*Acoustic Guitars, Synth Piano and Synth Strings recorded at Radley Studios, Los Angeles, California.
*Vocals recorded at The Red Lips Room, Beverly Hills, California.

;Personnel
{{col-begin}}
{{col-2}}
*Songwriting&nbsp;– Christina Aguilera, Jamie Hartman, David Glass, Phil Bentley
*Production&nbsp;– Tracklacers, Jamie Hartman <small>(co-producer)</small>
*Vocal production&nbsp;– Christina Aguilera
*Vocal recording&nbsp;– Oscar Ramirez
*String arrangement&nbsp;– Jamie Hartman
*String, Bass and Piano recording&nbsp;– Justin Stanley
*Acoustic Guitars, Synth Piano and Synth Strings recording&nbsp;– Jamie Hartman
*Assistant&nbsp;– Zivi Krieger
{{col-2}}
*Drum programming, Keyboards and Synths&nbsp;– Steve Daly, Jon Keep
*Synth Strings&nbsp;– Jamie Hartman, The Professor
*Live Strings&nbsp;– Songa Lee, Rodney Wirtz, Alisha Bauer, Marisa Kuney
*Live Piano&nbsp;– Jeff Babko, Jamie Hartman
*Synth Piano&nbsp;– Jamie Hartman, The Professor
*Live Bass&nbsp;– Tyler Chester, Steve Daly
*Acoustic Guitars&nbsp;– Jamie Hartman
{{col-end}}

Credits adapted from the liner notes of ''Lotus'', [[RCA Records]].<ref name="Notes">{{cite AV media notes|others=Christina Aguilera|title=[[Lotus (Christina Aguilera album)|Lotus]]|year=2012|type= inlay cover|publisher= [[RCA Records]]|page=iTunes Digital Booklet}}</ref>

==Charts==
Upon the release of ''Lotus'', "Army of Me" debuted on the [[Gaon Singles Chart|South Korean singles chart]] at number 103 during the week of November 11 to 17, 2012, due to digital download sales of 2,689.<ref name="SouthKoreaDebut">{{cite web|url=http://gaonchart.co.kr/main/section/online/download/list.gaon|title=South Korea Gaon International Chart (Week: November 11, 2012 to November 17, 2012)|publisher=[[Gaon Chart]]|date=November 17, 2012|accessdate=November 24, 2012|format=''The URL will open up the South Korea national singles chart. To view international, click the right hand box out of three above the orange 'number one' position. Then above the chart table on the right, select 2012 instead of 2013 and then select November 11 to 17, then at the bottom of the page, click 101-200 chart position link''}}</ref>

{| class="wikitable plainrowheaders" style="text-align:center;"
|-
!scope="col"| Chart (2012)
!scope="col"| Peak<br />position
|-
! scope="row"| South Korea ([[Gaon Chart|Gaon]])<ref name="SouthKoreaDebut"/>
| 103
|}

==Anastacia version==
{{Infobox single |
|  Name           = Army of Me
|  Cover          = Anastacia - Army of Me.jpg
|  Border         = yes
|  Artist         = [[Anastacia]]
|  from Album     = [[Ultimate Collection (Anastacia album)|Ultimate Collection]]
|  Released       = 23 October 2015
|  Format         = [[Music download|Digital download]]
|  Recorded       = 2015
|  Genre          = [[Pop music|Pop]]
|  Length         = 3:25
|  Label          = [[Sony Music Entertainment|Sony Music]]
|  Writer        = [[Christina Aguilera]], Jamie Hartman, David Glass, Phil Bentley
|  Producer       = 
|  Chronology     = Anastacia singles
|  Last single    = "Take This Chance"<br>(2015)
|  This single    = "'''Army of Me'''"<br>(2015)
|  Next single    = 
| Misc = 
}}

American recording artist [[Anastacia]] covered the song for her second greatest hits album ''[[Ultimate Collection (Anastacia album)|Ultimate Collection]]'' (2015). The song was released as album's second single on 23 October 2015 by [[Sony Music Entertainment]].<ref name="Ultimate Collection by Anastacia">{{cite web|title=Ultimate Collection by Anastacia|url=https://itunes.apple.com/gb/album/army-of-me/id1047913008?i=1047913510|work=iTunes|accessdate=10 December 2015}}</ref>

{| class="wikitable plainrowheaders"
|-
! scope="col"| Region
! scope="col"| Date
! scope="col"| Format(s)
! scope="col"| Label
! scope="col"| Ref.
|-
! scope="row"| Worldwide
| October 23, 2015
| [[Music download|Digital download]]
| [[Sony Music Entertainment]]
| <ref name="Ultimate Collection by Anastacia"/>
|}

==References==
{{Reflist|30em}}

==External links==
{{Wikipedia books|Lotus (album)|''Lotus'' (album)}}
* {{MetroLyrics song|christina-aguilera|army-of-me}}<!-- Licensed lyrics provider -->

{{Christina Aguilera songs}}
{{Anastacia}}

[[Category:2012 songs]]
[[Category:Christina Aguilera songs]]
[[Category:Anastacia songs]]
[[Category:Songs written by Christina Aguilera]]
[[Category:RCA Records singles]]
[[Category:Sony Music Entertainment singles]]
[[Category:Songs written by Jamie Hartman]]
[[Category:Eurodance songs]]
[[Category:Dance-pop songs]]