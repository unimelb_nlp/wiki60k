{{Infobox person
|name          = Nick Corea
|image         =  
|caption       = 
|birth_name    = Nicholas J. Corea
|birth_date    = {{birth date|1943|4|7}}
|birth_place   = [[St. Louis, Missouri]], [[United States|U.S.]]
|death_date    = {{death date and age|1999|1|17|1943|4|7}}
|death_place   = [[Burbank, California]]
|death_cause   = Cancer
|resting_place_coordinates = 
|residence     = 
|nationality   = [[United States|American]]
|other_names   = 
|known_for     = ''[[The Incredible Hulk (1978 TV series)|The Incredible Hulk]]'', ''[[The Incredible Hulk Returns]]'', ''[[Outlaws (1986 TV series)|Outlaws]]'', ''[[Renegade (TV series)|Renegade]]'', ''[[Walker, Texas Ranger]]''
|education     = 
|alma_mater    = 
|employer      = 
|occupation    = [[Television producer]], [[television director|director]] and [[television writer|writer]]
|home_town     = 
|title         = 
|salary        = 
|networth      = 
|height        = 
|weight        = 
|religion      = 
|spouse        = Pheny (1992-1999)
|partner       = 
|children      = 
|parents       = 
|relatives     = 
|signature     = 
|website       = 
|footnotes     = 
}}
'''Nicholas "Nick" J. Corea''' (April 7, 1943 – January 17, 1999) was an American author, television writer, director, producer and painter. Through best known for his work on ''[[The Incredible Hulk (1978 TV series)|The Incredible Hulk]]'' and the sequel 1988 telefilm ''[[The Incredible Hulk Returns]]'', he was involved with many television series during the late 1970s and 1980s including ''[[The Oregon Trail (TV series)|The Oregon Trail]]'', ''[[Airwolf]]'', ''[[Street Hawk]]'', ''[[Hard Time on Planet Earth]]'' and ''[[Booker (TV series)|Booker]]''. He was also the creator of the 1986 science fiction series ''[[Outlaws (1986 TV series)|Outlaws]]''.

Prior to his death in 1999, Corea wrote episodes for ''[[Renegade (TV series)|Renegade]]'', ''[[M.A.N.T.I.S.]]'', ''[[Kung Fu: The Legend Continues]]'' and [[Star Trek]]-series ''[[Star Trek: Deep Space Nine]]'' and ''[[Star Trek: Voyager]]''. He was also a one-time writer and [[creative consultant]] for ''[[Walker, Texas Ranger]]''.

==Biography==
Nicholas Corea was born and raised in [[St. Louis, Missouri]]. He enlisted in the [[United States Marines]] during the [[Vietnam War]], was awarded the [[Purple Heart]] and reached the rank of [[Sergeant]]<ref>Carrison, Dan and Rod Walsh. ''Semper Fi: Business Leadership the Marine Corps Way''. New York: AMACOM, 2004. (pg. xi) ISBN 0-8144-7272-9</ref> before being honorably discharged. Shortly thereafter, he returned to his hometown to join the [[University City, Missouri]] Police Department. While in the military, he was an active contributor to ''[[Stars and Stripes (newspaper)|Stars and Stripes]]'' and later wrote the police novel ''A Cleaner Breed'' in 1974.<ref name="Modesto">"Nicholas Corea: TV writer, producer". ''[[The Modesto Bee]]''. 26 Jan 1999</ref>

Following the success of his first book, Corea entered the television industry as a writer and producer. His first scripts were for [[police drama]]s ''[[Police Woman (TV series)|Police Woman]]'' (1974), ''[[Baa Baa Black Sheep]]'' (1976) and ''[[Kingston: Confidential]]'' (1977). His first regular writing job was for western series ''[[The Oregon Trail (TV series)|The Oregon Trail]]''. However, he found his first major success as writer, director and producer of ''[[The Incredible Hulk (1978 TV series)|The Incredible Hulk]]'' from 1978 to 1981.<ref name="Modesto"/><ref>Terrace, Vincent. ''Encyclopedia of Television Series, Pilots and Specials: 1974-1984''. Vol. II. New York: Zoetrope, 1985. (pg. 205) ISBN 0-918432-61-8</ref><ref>Lewis, Jon E. and Penny Stempel. ''Cult TV: The Essential Critical Guide''. London: Pavilion Books, 1996. (pg. 20) ISBN 1-85793-926-3</ref><ref>Phillips, Mark and Frank Garcia. ''Science Fiction Television Series: Episode Guides, Histories, and Casts and Credits for 62 Prime Time Shows, 1959 Through 1989''. Jefferson, North Carolina: McFarland, 1996. (pg. 134, 139, 143) ISBN 0-7864-0041-2</ref><ref name="Stanley">Stanley, John. ''Creature Features: The Science Fiction, Fantasy, and Horror Movie Guide''. New York: Berkley Boulevard Books, 2000. (pg. 262, 285) ISBN 0-425-17517-0</ref><ref>Muir, John K. ''The Encyclopedia of Superheroes on Film and Television''. Jefferson, North Carolina: McFarland, 2004. ISBN 0-7864-1723-4</ref>

After the series' end, he wrote and directed the 1981 television movie ''[[The Archer: Fugitive from the Empire]]''.<ref>Scheuer, Steven H. ''Movies on TV and Video Cassette, 1989-1990''. Toronto: Bantam Books, 1989. ISBN 0-553-27707-3</ref><ref>Maltin, Leonard. ''Leonard Maltin's TV Movies and Video Guide''. New York: Penguin, 1991. (pg. 46) ISBN 0-451-16748-1</ref><ref>Weiner, David J. ''Videohound's Golden Movie Retriever, 1992''. Detroit: [[Visible Ink Press]], 1991. (pg. 55) ISBN 0-8103-9404-9</ref><ref>Weldon, Michael J. ''The Psychotronic Video Guide''. New York: St. Martin's Press, 1996. (pg. 26) ISBN 0-312-13149-6</ref><ref>Martin, Mick and Marsha Porter. ''Video Movie Guide 1998''. New York: Ballantine Books, 1997. ISBN 0-345-40793-8</ref> The movie was intended to be a pilot for a regular television series and, although broadcast on [[NBC]], it was better received outside the United States where it "received some theatrical exposure" under the title ''The Archer and the Sorceress''.<ref>Worley, Alec. ''Empires of the Imagination: A Critical Survey of Fantasy Cinema from Georges Méliès to The Lord of the Rings''. Jefferson, North Carolina: McFarland, 2005. (pg. 195, 222) ISBN 0-7864-2324-2</ref>

He was also the supervising producer for ''[[Gavilian]]''<ref>Contemporary Authors: A Bio-Biographical Guide To Current Writhers In Fiction, General Nonfiction, Poetry, Journalism, Drama, Motion Pictures, Television, And Other Fields.</ref><ref>Sawyer, Thomas B. ''Fiction Writing Demystified: Techniques That Will Make You a More Successful Writer''. Malibu: Ashleywilde, Inc., 2003. (pg. 71) ISBN 0-9627476-1-0</ref> and a writer for ''[[The Renegades]]'' (1983), ''[[Airwolf]]'' (1984)<ref name="Naylor">Naylor, Lynne, ed. ''Television Writers Guide, Fourth Edition''. Vol. 50. Los Angeles: Lone Eagle Publishing Co., 1995. (pg. 82, 417, 476, 481) ISBN 0-943728-75-4</ref> and ''[[Street Hawk]]'' (1985).<ref>Gianakos, Larry J. ''Television Drama Series Programming: A Comprehensive Chronicle, 1984-1986''. Metuchen, New Jersey: Scarecrow Press, 1992. (pg. 389) ISBN 0-8108-2601-1</ref> He was also the writer and executive producer for ''[[J.O.E. and the Colonel]]'', another television movie, in 1985.<ref name="Stanley"/><ref>Sherman, Fraser A. ''Cyborgs, Santa Claus, and Satan: Science Fiction, Fantasy, and Horror Films Made for Television''. Jefferson, North Carolina: McFarland, 2000. (pg. 103) ISBN 0-7864-0793-X</ref> A year later, he wrote, produced and directed a short-lived western-themed science fiction series, ''[[Outlaws (1986 TV series)|Outlaws]]'', in 1986.<ref name="Naylor"/><ref>Buscombe, Edward. ''The BFI Companion to the Western''. London: Andre Deutsch/British Film Institute, 1988. (pg. 415) ISBN 0-233-98332-5</ref><ref>Rainey, Buck. ''The Shoot-Em-Ups Ride Again: A Supplement to Shoot-Em-Ups''. Metuchen, New Jersey: Scarecrow Press, 1990. (pg. 279) ISBN 0-8108-2132-X</ref> The pilot proved popular with viewers, being one of the most watched shows the week it aired, however ratings quickly dwindled as poor promotion and its placement in the [[graveyard slot|Saturday night "graveyard" slot]] led to its eventual cancellation by [[CBS]] at the end of its first season. Two of its stars, [[Rod Taylor]] and [[Charles Napier (actor)|Charles Napier]], had been cast members of ''The Oregon Trail'' and the final episode featured clips from that show as part of a "flashback" episode of how the characters first met.

In 1988, Corea wrote, directed and produced the television movie ''[[The Incredible Hulk Returns]]'' as a follow-up the original Incredible Hulk series.<ref>Cameron-Wilson, James and F. Maurice Speed. ''Film Review, 1994''. New York: St. Martin's Press, 1993. (pg. 129) ISBN 0-312-10653-X</ref><ref>Pringle, David. ''Imaginary People: A Who's Who of Fictional Characters from the Eighteenth Century to the Present Day''. Aldershot, Hampshire: Scolar Press, 1996. (pg. 119) ISBN 1-85928-162-1</ref><ref>Kohn, Martin F. ''Videohound's Family Video Guide''. Detroit: Visible Ink Press, 1996. ISBN 0-7876-0984-6</ref><ref>[[Blockbuster Entertainment]]. ''Blockbuster Entertainment Guide to Movies and Videos, 1998''. Island Books, 1997. (pg. 588) ISBN 0-440-22419-5</ref> He then wrote episodes for ''[[Hard Time on Planet Earth]]''<ref>Cotter, Bill. ''The Wonderful World of Disney Television: A Complete History''. New York: Hyperion, 1997. (pg, 614) ISBN 0-7868-6359-5</ref> and ''[[Booker (TV series)|Booker]]'' during 1989 and, in 1992, he wrote the television movie ''[[Mario and the Mob]]''.<ref name="Naylor"/>

From 1992 to 1993, Corea was a writer and executive producer for ''[[Renegade (TV series)|Renegade]]'' and later wrote episodes for ''[[M.A.N.T.I.S.]]'' in 1994,<ref>Lentz, Harris M. ''Science Fiction, Horror & Fantasy Film and Television Credits: Television shows''. Jefferson, North Carolina: McFarland, 2001. (pg. 1941) ISBN 0-7864-0952-5</ref> and ''[[Kung Fu: The Legend Continues]]'' and ''[[High Sierra Search and Rescue]]'' during 1995. He also wrote two episodes of ''[[Star Trek: Deep Space Nine]]'' and one of ''[[Star Trek: Voyager]]''.<ref>Schuster, Hal. ''The Trekker's Guide to Voyager: Complete, Unauthorized, and Uncensored''. Rocklin, California: Prima Publishing, 1996. ISBN 0-7615-0572-5</ref> One of his last television projects was as a one-time writer and creative consultant for ''[[Walker, Texas Ranger]]''. He died of cancer in [[Burbank, California]] on January 17, 1999.<ref name="Modesto"/>

Shortly before his death, Corea wrote an episode for ''Walker, Texas Ranger'' entitled "Brothers in Arms" which was to feature longtime friend and actor [[Grand L. Bush]] as a guest star. Bush portrayed Simon Trivette, an estranged brother of [[James Trivette]] ([[Clarence Gilyard]]), and is considered one of the most memorable episodes in the series. Bush and his wife, journalist [[Sharon Crews]], later watched the episode privately with Corea's widow when it was finally broadcast.

Nick Corea was also an avid painter whose works include ''Blue Baby''.

==Bibliography==
*''A Cleaner Breed'' (1974)

==References==
{{reflist}}

==External links==
{{Portal|Biography}}
*{{findagrave|43660191}}
*{{IMDb name|0179709}}
{{Memoryalpha|Nicholas Corea|Nicholas J. Corea}}

{{Authority control}}

{{DEFAULTSORT:Corea, Nicholas J.}}
[[Category:1943 births]]
[[Category:1999 deaths]]
[[Category:American military personnel of the Vietnam War]]
[[Category:American television directors]]
[[Category:American television producers]]
[[Category:American television writers]]
[[Category:Male television writers]]
[[Category:Deaths from cancer]]
[[Category:Writers from St. Louis]]
[[Category:United States Marines]]
[[Category:20th-century American businesspeople]]