The '''1974 [[Atlantic Coast Conference Men's Basketball Tournament]]''' was held in [[Greensboro, North Carolina]], at the [[Greensboro Coliseum]] from March 7–9. [[NC State Wolfpack men's basketball|North Carolina State]] defeated [[Maryland Terrapins men's basketball|Maryland]] in overtime 103–100 to win the championship. [[Tommy Burleson]] of NC State was named the tournament MVP. 

The final pitted arguably the two best teams in the country and has long been regarded by many to be the greatest ACC game in history, and one of the greatest college games ever.  The game was instrumental in forcing the expansion of the [[NCAA Men's Division I Basketball Championship]] to 32 teams, thus allowing more than one bid from a conference. That Maryland team, with six future NBA draft picks, is considered{{who?|date=March 2015}} the greatest team that did not participate in the NCAA tournament. <ref name="free">Bill Free - [http://articles.baltimoresun.com/1999-02-09/sports/9902090157_1_nba-draft-picks-david-thompson-burleson This Overtime Lasts 25 Years] Baltimore Sun </ref> 

Many considered it the all-time greatest college basketball game until [[Duke Blue Devils men's basketball|Duke]] pulled off a last-second overtime finish in the [[1992 NCAA Men's Division I Basketball Tournament|1992 NCAA East Regional final]] for a 104–103 win over [[Kentucky Wildcats men's basketball|Kentucky]]. "I know they call the Duke–Kentucky game the greatest now," said Burleson in 1999 at a 25-year commemoration of the 1974 game, "but we're still the greatest ACC game ever." <ref name="free" />

NC State went on to win the [[1974 NCAA Men's Division I Basketball Tournament|1974 NCAA Tournament]], dethroning [[Bill Walton]] and 7-time defending national champion [[UCLA Bruins men's basketball|UCLA]] in the semifinals in another classic, before defeating [[Marquette Golden Eagles men's basketball|Marquette]] in the final.

==Bracket==
{{3RoundBracket-Byes 
| RD1='''Quarterfinals'''<br>March 7, 1974
| RD2='''Semifinals'''<br>March 8, 1974
| RD3='''Championship Game'''<br>March 9, 1974
| RD1-seed1=1
| RD1-team1=
| RD1-score1=
| RD1-seed2=
| RD1-team2=
| RD1-score2=
| RD1-seed3=4
| RD1-team3='''Virginia'''
| RD1-score3='''68'''
| RD1-seed4=5
| RD1-team4=Clemson
| RD1-score4=63
| RD1-seed5=2
| RD1-team5='''Maryland'''
| RD1-score5='''85'''
| RD1-seed6=7
| RD1-team6=Duke
| RD1-score6=66
| RD1-seed7=3
| RD1-team7='''North Carolina'''
| RD1-score7='''76'''
| RD1-seed8=6
| RD1-team8=Wake Forest
| RD1-score8=62
| RD2-seed1=1
| RD2-team1='''NC State'''
| RD2-score1='''87'''
| RD2-seed2=4
| RD2-team2=Virginia
| RD2-score2=66
| RD2-seed3=2
| RD2-team3='''Maryland'''
| RD2-score3='''105'''
| RD2-seed4=3
| RD2-team4=North Carolina
| RD2-score4=85
| RD3-seed1=1
| RD3-team1='''[[1973–74 NC State Wolfpack men's basketball team|NC State]]'''
| RD3-score1='''103'''*
| RD3-seed2=2
| RD3-team2=[[Maryland Terrapins men's basketball (1970–1979)|Maryland]]
| RD3-score2=100
}}

<nowiki>* Denotes Overtime Game</nowiki>

== See also ==
{{Portal|ACC}}
*[[List of Atlantic Coast Conference men's basketball tournament champions]]

==References==
{{reflist}}

==External links==
* [http://www.theacc.com/sports/m-baskbl/archive/110299aaf.html ACC Tournament Archive] 

{{ACC Men's Basketball Tournament navbox}}

[[Category:ACC Men's Basketball Tournament]]
[[Category:1973–74 Atlantic Coast Conference men's basketball season]]