{{EngvarB|date=November 2013}}
{{Use dmy dates|date=November 2013}}
{{Infobox airport
| name         = Sligo Airport
| nativename   = Aerfort Shligigh
| nativename-a =
| nativename-r =
| ensign =
| image        = Sligo-airport.jpg
| image-width  =
| caption      = Sligo Airport
| IATA         = SXL
| ICAO         = EISG
| pushpin_map            = Ireland
| pushpin_map_caption    = Location of airport in Ireland
| pushpin_label          = SXL
| pushpin_label_position = right
| type         = Public
| owner        =
| operator     = Sligo Northwest Airport Co Ltd
| city-served  = [[Sligo]] , [[Republic of Ireland|Ireland]]
| location     = [[Strandhill]], [[County Sligo]]
| elevation-f  = 11
| elevation-m  = 3
| coordinates  = {{Coord|54|16|49|N|008|35|57|W|type:airport_region:IE-SO|display=inline,title|name=Sligo Airport }}
| website      = [http://www.sligoairport.com www.sligoairport.com]
| metric-rwy   = Y
| r1-number    = 11/29
| r1-length-f  = 3,933
| r1-length-m  = 1,199
| r1-surface   = [[Asphalt]]
| stat-year    =
| stat1-header =
| stat1-data   =
| stat2-header =
| stat2-data   =
| footnotes    = Source: Irish [[Aeronautical Information Service|AIS]]<ref name="AIP">{{AIP IE|EISG|name=SLIGO}}</ref>
}}

'''Sligo Airport''' ({{lang-ga|'''Aerfort Shligigh'''}}) is located in [[Strandhill]], [[County Sligo]], {{convert|5|NM|abbr=on|lk=in}}<ref name="AIP"/> west of [[Sligo]], at the end of the [[R277 road (Ireland)|R277]] road, in Ireland. The airport is a small regional airport and has no scheduled routes.

==Introduction and history==
Sligo Airport is the home of the Sligo Aero Club (a Registered Training Facility) and the northwest base for the Irish Coastguard. Private flight training, skydiving and charity jumps are all operated from the airport.

Like airports such as [[Gibraltar International Airport|Gibraltar]] and [[Madeira Airport|Funchal]], Sligo has a lack of safety margin for undershoots and overshoots because the peninsula upon which the airport is situated is less than {{convert|2|km|abbr=on}} long. In 2002 a [[Euroceltic Airways]] [[Fokker F27]] aircraft carrying the band [[Aslan (rock band)|Aslan]] overshot the runway and the nose dipped into the sea. The accident caused no casualties.

Euroceltic was operating the [[Government of Ireland]] [[public service obligation]] subsidy scheme for the route to [[Dublin]] at the time. The airline collapsed shortly afterwards and [[Aer Arann]] operated the route for the remainder of the contract. The 2005 contract tender was offered to [[Loganair]] who declined it. Aer Arann subsequently negotiated the operation of the contract with the Government.

On 21 February 2007 the Irish Government announced that it would be giving €8.5 million to the airport in capital grant money, to upgrade the runway and add approach lighting and safety enhancements. However, the proposed runway extension would have required infill and the erection of gantries across part of the adjacent protected beach. The plan drew much local criticism and almost 400 objections from residents of the local area, fisheries groups, the Department of Environment, [[An Taisce]] and Birdwatch Ireland. The planning permission was quashed on the third attempt by a high court judge on justification grounds.<ref>http://www.dorrins.com/latest-news/2010/06/high-court-quashes-airport-planning-decision/</ref>

Until the end of 2008 there was a connection with [[Manchester Airport]] which was operated by Aer Arann.
In 2011 the airport lost its only scheduled route, operated by [[Aer Arann]], to [[Dublin Airport]] twice daily. The final flight was on 21 July 2011.

The Irish Government-commissioned Value for Money Review of Exchequer Funding on the Regional Airports Programme recommended the ending of operational subvention to the airport and the ending of the PSO designation, citing poor performance, growing operational costs and development of alternative transport connections to the region.<ref>[http://www.transport.ie/viewitem.asp?id=12921&lang=ENG&loc=432]</ref>

Since the ending of passenger flights in 2011 the airport has continued to be a base for the [[Irish Coast Guard]] Rescue Helicopter (Rescue 118). Sligo Aero Club continues to operate from the airport. The airport is always busy with general aviation aircraft including Cessnas, Pipers, and Beechcraft. The airport receives occasional visits from jets which are suited to land on the shorter runway.

==CHC Ireland Search and Rescue Base==
Sligo Airport is the home of Rescue 118, the [[Irish Coast Guard]] Helicopter which has served the north-west of Ireland since 2004. The base operates a [[Sikorsky_S-92|Sikorsky S-92A]] helicopter 24 hours, 365 days a year (which replaced the previously used [[Sikorsky_S-61|S-61N]] on 1 July 2013). It deals with many types of incidents, such as cliff rescues, hospital transfers, and dealing with sick patients off coastal islands.

==Accidents and incidents==
*On 2 November 2002, a [[Euroceltic Airways|Euroceltic]] [[Fokker F-27]] (registered G-ECAT) was coming into land on Runway 11, after a routine flight from Dublin, when it overran the runway. After skidding about halfway down the runway, the aircraft eventually came to rest with the nose of the aircraft in the sea, and the main landing gear on an embankment. Passengers were evacuated and there were no reported casualties. However the plane was declared a write-off by the company two weeks later, due to the saltwater damage on the fuselage. The cause of the accident was later declared to be strong tailwinds and a wet runway.

==References==
{{reflist|colwidth=30em}}

==External links==
{{Portal|Ireland|Aviation}}
{{Commonscat-inline}}
* [http://www.sligoairport.com/ Official website]

{{Airports of Ireland}}

[[Category:Transport in County Sligo]]
[[Category:Airports in the Republic of Ireland]]