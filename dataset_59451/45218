{{eastern name order|Besenyei Péter}}
{{Infobox aviator
|name=Péter Besenyei
|image=Besenyei Péter vezet.jpg
|image_size=200px
|alt=
|caption=
|full_name=
|birth_date= {{Birth date and age|1956|6|8|mf=y}}
|birth_place=
|death_date= <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} -->
|death_place=
|death_cause=
|resting_place=
|resting_place_coordinates= <!-- {{Coord|LATITUDE|LONGITUDE|type:landmark}} -->
|monuments=
|nationality={{flagicon|Hungary}} [[Hungary]]
|spouse=
|relatives=
|first_race = 2003
|racing_best = 1st ([[2003 Red Bull Air Race World Series season|2003]])
|racing_aircraft = [[Corvus Racer 540]]
|website=[http://www.besenyeipeter.hu/ besenyeipeter.hu]
}}
[[Image:Extra 300S - Peter Besenyei.jpg|thumb|180px|right|Péter Besenyei piloting [[Extra 300S]]]]

'''Péter Besenyei''' (born 1956) is a [[Hungarian people|Hungarian]] [[aerobatics]] [[aviator|pilot]] and world champion air racer. He was born on June 8, 1956 in [[Körmend]], [[Hungary]]. He lived near the airport of [[Budapest]] and became interested in flying when he was a child. From watching 1962 [[World Aerobatic Championships]] he decided to become a pilot. At 15 years of age he flew a [[Glider (sailplane)|glider]] for the first time. In 1976 Péter entered his first flying competition<ref name="guide">{{cite web|url=http://www.budpocketguide.com/TouristInfo/famous/Famous_Hungarians13.asp|title=''Peter Besenyei Aerobatics Pilot''|publisher=budpocketguide.com|accessdate=2010-01-25}}</ref> by piloting a glider and showed his talent, finishing in second place.

Péter Besenyei became an aerobatics pilot and won several titles in national and international championships. He won his first gold medal in 1982 at the [[Austrian National Championship]]s.<ref>{{cite web|url=http://www.rtl.hu/besenyei/personal/about|title=''Besenyei Peter''|publisher=rtl.hu|language=Hungarian|accessdate=2010-01-25}}</ref> His specialty is free-style aerobatics. He invented a number of original snap rolls and, in 1984, the "knife-edge spin". In 1995 Péter Besenyei won 2 gold and 2 silver medals and he was named the most successful aerobatics pilot of his time.<ref name="guide"/> In 2001 Besenyei flew upside down under the [[Széchenyi Chain Bridge]],<ref name="red">{{cite web|url=http://www.redbullairrace.com/cs/Satellite?c=RB_Profile&childpagename=RedBullAirRace%2FLayout&cid=1238611551830&pagename=RedBullAirRaceWrapper|title=''Peter Besenyei''|publisher=redbullairrace.com|accessdate=2010-01-25}}</ref> that spans the river [[Danube]] in Budapest, a maneuver that became a standard in air races today.

Besenyei is sometimes referred to as the godfather of the [[Red Bull Air Race World Championship]] because of his work helping develop it.<ref name="red"/> He was asked, in 2001 by [[Austria]]n energy-drink company [[Red Bull]], to help develop the concept of an [[air racing]] competition. With enthusiasm he helped set up the rules and regulations and carefully selected the most daring pilots, with skills and courage, to handle the extreme physical and mental challenges of the air race. The first race was held in 2003 in [[Zeltweg]], Austria. After two years the competition became a worldwide organization of [[Red Bull Air Race World Series]].

He is currently a [[test pilot]]<ref name="red"/> for the Hungarian Aviation Office and a [[flying instructor]] for aerobatic pilots on [[Zivko Edge 540]]. Péter enjoys car racing, skiing, sky diving, fishing, and photography.<ref name="red"/> Besenyei retired from the Red Bull Air Race at the completion of the [[2015 Red Bull Air Race World Championship season|2015 season]].

==Achievements==
;1982:
* Austrian National Championships – overall winner
;1990:
* World Aerobatics Championships – 2nd
;1993:
* [[Breitling Aerobatics World Cup]] - 3rd
;1994:	
* World Champion of the Compulsory Program
;1995:	
* European Champion Freestyle		
* European Champion of the Compulsory Program
;1998:	
* [[FAI World Grand Prix Series]] 1st		
;2000:	
* World Champion Freestyle		
;2001:	
* FAI World Grand Prix Series 1st
;2003:	
* [[Tokyo]], [[Japan]] - 1st
;2005:	
* FAI World Series Grand Prix, [[Lausanne]], [[Switzerland]] - 1st

==Honors==

* 1996 - "Gold Medal of the President of the Republic of Hungary" by President [[Árpád Göncz]]<ref name="red"/>

{|class="wikitable"
|- 
! colspan=16 align=center|{{flagicon|Hungary}} Péter Besenyei<br>at the [[Red Bull Air Race World Series]]
|-
!width="2%"|Year
!width="2%"| 1
!width="2%"| 2
!width="2%"| 3
!width="2%"| 4
!width="2%"| 5
!width="2%"| 6
!width="2%"| 7
!width="2%"| 8
!width="2%"| 9
!width="2%"| 10
!width="2%"| 11
!width="2%"| 12
!width="2%"|Points
!width="2%"|Wins
!width="2%"|Rank
|- align=center
! [[2003 Red Bull Air Race World Series season|2003]]
| style="background:#ffffbf;"|{{flagicon|Austria}}<br>1st
| style="background:#ffffbf;"|{{flagicon|Hungary}}<br>1st
|
|
|
|
|
|
|
|
|
|
! 6
! 2
| style="background:#ffffbf;"|'''1st'''
|- align=center
! [[2004 Red Bull Air Race World Series season|2004]]
| style="background:#ffdf9f;"|{{flagicon|United Kingdom}}<br>3rd
| style="background:#ffdf9f;"|{{flagicon|Hungary}}<br>3rd
| style="background:#ffdf9f;"|{{flagicon|United States}}<br>3rd
|
|
|
|
|
|
|
|
|
! 12
! 0
| style="background:#dfdfdf;"|'''2nd'''
|- align=center
! [[2005 Red Bull Air Race World Series season|2005]]
| style="background:#ffffbf;"|{{flagicon|United Arab Emirates}}<br>1st
| style="background:#dfdfdf;"|{{flagicon|Netherlands}}<br>2nd
| style="background:#dfdfdf;"|{{flagicon|Austria}}<br>2nd
| style="background:#ffffbf;"|{{flagicon|Ireland}}<br>1st
| style="background:#ffdf9f;"|{{flagicon|United Kingdom}}<br>3rd
| style="background:#dfffdf;"|{{flagicon|Hungary}}<br>4th
| style="background:#dfffdf;"|{{flagicon|United States}}<br>4th
|
|
|
|
|
! 32
! 2
| style="background:#dfdfdf;"|'''2nd'''
|- align=center
! [[2006 Red Bull Air Race World Series season|2006]]
| style="background:#ffdf9f;"|{{flagicon|United Arab Emirates}}<br>3rd
| style="background:#ffffbf;"|{{flagicon|Spain}}<br>1st
| style="background:#dfdfdf;"|{{flagicon|Germany}}<br>2nd
| {{flagicon|Russia}}<br>CAN
| style="background:#ffdf9f;"|{{flagicon|Turkey}}<br>3rd  
| {{flagicon|Hungary}}<br>DQ  
| style="background:#dfdfdf;"|{{flagicon|United Kingdom}}<br>2nd
| style="background:#dfdfdf;"|{{flagicon|United States}}<br>2nd
| style="background:#ffffbf;"|{{flagicon|Australia}}<br>1st
|
|
|
! 35
! 2
| style="background:#dfdfdf;"|'''2nd'''
|- align=center
! [[2007 Red Bull Air Race World Series season|2007]]
| style="background:#ffffbf;"|{{flagicon|United Arab Emirates}}<br>1st
| style="background:#dfffdf;"|{{flagicon|Brazil}}<br>5th 
| style="background:#ffffbf;"|{{flagicon|United States}}<br>1st 
| style="background:#dfffdf;"|{{flagicon|Turkey}}<br>5th 
| {{flagicon|Spain}}<br>CAN
| style="background:#ffdf9f;"|{{flagicon|Switzerland}}<br>3rd
| style="background:#ffdf9f;"|{{flagicon|United Kingdom}}<br>3rd
| style="background:#dfffdf;"|{{flagicon|Hungary}}<br>4th
| style="background:#dfffdf;"|{{flagicon|Portugal}}<br>4th 
| style="background:#dfffdf;"|{{flagicon|United States}}<br>6th
| {{flagicon|Mexico}}<br>CAN
| style="background:#cfcfff;"|{{flagicon|Australia}}<br>7th
! 31
! 2
| style="background:#ffdf9f;"|'''3rd'''
|- align=center
! [[2008 Red Bull Air Race World Series season|2008]]
| style="background:#dfffdf;"|{{flagicon|United Arab Emirates}}<br>4th
| style="background:#dfffdf;"|{{flagicon|United States}}<br>8th
| style="background:#dfffdf;"|{{flagicon|United States}}<br>5th
| {{flagicon|Sweden}}<br>CAN
| style="background:#dfffdf;"|{{flagicon|Netherlands}}<br>6th
| style="background:#dfffdf;"|{{flagicon|United Kingdom}}<br>4th
| style="background:#dfffdf;"|{{flagicon|Hungary}}<br>5th
| style="background:#dfffdf;"|{{flagicon|Portugal}}<br>7th
| {{flagicon|Spain}}<br>CAN
| style="background:#dfffdf;"|{{flagicon|Australia}}<br>7th
| 
| 
! 34
! 0
| style="background:#dfffdf;"|'''5th'''
|- align=center
! [[2009 Red Bull Air Race World Championship season|2009]]
| style="background:#cfcfff;"|{{Flagicon|United Arab Emirates}}<br>10th
| style="background:#dfffdf;"|{{Flagicon|United States}}<br>4th
|{{Flagicon|Canada}}<br>DNS
| style="background:#cfcfff;"|{{Flagicon|Hungary}}<br>10th
| style="background:#dfffdf;"|{{Flagicon|Portugal}}<br>4th
| style="background:#dfffdf;"|{{Flagicon|Spain}}<br>8th
| 
| 
| 
|
| 
|
! 24
! 0
| style="background:#dfffdf;"|'''8th'''
|- align=center
! [[2010 Red Bull Air Race World Championship season|2010]]
| style="background:#ffdf9f;"|{{flagicon|United Arab Emirates}}<br>3rd
| style="background:#cfcfff;"|{{flagicon|Australia}}<br>10th
| style="background:#cfcfff;"|{{flagicon|Brazil}}<br>11th
| style="background:#cfcfff;"|{{flagicon|Canada}}<br>10th
| style="background:#dfffdf;"|{{flagicon|United States}}<br>8th
| style="background:#cfcfff;"|{{flagicon|Germany}}<br>9th
|{{flagicon|Hungary}}<br>CAN
|{{flagicon|Portugal}}<br>CAN
| 
| 
| 
|
!21
!0
|style="background:#cfcfff;"|'''10th'''
|- align=center
! [[2014 Red Bull Air Race World Championship season|2014]]
| style="background:#CFCFFF;"|{{flagicon|United Arab Emirates}}<br>10th
| style="background:#CFCFFF;"|{{flagicon|Croatia}}<br>11th
| style="background:#dfffdf;"|{{flagicon|Malaysia}}<br>7th
| style="background:#dfffdf;"|{{flagicon|Poland}}<br>7th
| style="background:#dfffdf;"|{{flagicon|United Kingdom}}<br>7th
| style="background:#CFCFFF;"|{{flagicon|United States}}<br>12th
| style="background:#CFCFFF;"|{{flagicon|United States}}<br>12th
| style="background:#CFCFFF;"|{{flagicon|Austria}}<br>12th
| 
| 
| 
|
!6
!0
|style="background:#cfcfff;"|'''11th'''
|- align=center
! [[2015 Red Bull Air Race World Championship season|2015]]
| style="background:#dfffdf;"|{{flagicon|United Arab Emirates}}<br>7th
| style="background:#CFCFFF;"|{{flagicon|Japan}}<br>13th
| style="background:#CFCFFF;"|{{flagicon|Croatia}}<br>12th
| style="background:#dfffdf;"|{{flagicon|Hungary}}<br>6th
| style="background:#dfffdf;"|{{flagicon|United Kingdom}}<br>6th
| style="background:#CFCFFF;"|{{flagicon|Austria}}<br>8th
| style="background:#CFCFFF;"|{{flagicon|United States}}<br>12th
| style="background:#CFCFFF;"|{{flagicon|United States}}<br>13th
| 
| 
| 
|
!9
!0
|style="background:#cfcfff;"|'''12th'''
|}
<ref name="red"/>

Legend:
* CAN: Cancelled
* DNP: Did not participate
* DNS: Did not show
* DQ: Disqualified
* NC: Not classified

==See also==
* [[Jurgis Kairys]]
* [[Competition aerobatics]]

==References==
<references/>

==External links==
{{commons category|Péter Besenyei}}
*[http://www.besenyeipeter.com/ Péter Besenyei Official Site]
*[https://www.facebook.com/pages/Peter-Besenyei-Official-Fan-Page/170827806418405 Péter Besenyei (Official Fan Page) ]
*[http://www.budpocketguide.com/TouristInfo/famous/Famous_Hungarians13.asp Biography at Budapest Pocket Guide website]
*[http://www.redbullairrace.com/ Red Bull Air Race World Series official website]
*[http://www.fulldeflection.com Aerobatics website with Péter Besenyei videos]
*[https://www.youtube.com/watch?v=vZebAtxq5a0 The Story of Péter Besenyei on Youtube]
*[http://www.sandeshaya.org/red-bull-air-show-unawatuna-beach-sri-lanka-by-peter-besenyei/ The Story of Air Show Unawatuna Beach, Sri Lanka]

{{s-start}}
{{s-sports}}
{{succession box
  |title=[[Red Bull Air Race World Series|Red Bull Air Race World Series Champion]]
  |before=none
  |after=[[Kirby Chambliss]]
  |years=2003}}
{{s-end}}

{{Red Bull Air Race World Series Champions}}
{{Red Bull Air Race World Series Pilots}}
{{Aerobatics}}

{{DEFAULTSORT:Besenyei, Peter}}
[[Category:1956 births]]
[[Category:Living people]]
[[Category:Hungarian aviators]]
[[Category:Hungarian air racers]]
[[Category:Red Bull Air Race World Championship pilots]]
[[Category:Red Bull Air Race World Championship champions]]
[[Category:Aerobatic pilots]]