{{Use dmy dates|date=October 2013}}
{{Infobox Grand Prix race report
| Type = F1
| Country = UAE
| Grand Prix = Abu Dhabi
| Details ref = <ref name="formal">{{cite web|title=2013 Formula 1 Etihad Airways Abu Dhabi Grand Prix |url=http://www.formula1.com/races/in_detail/abu_dhabi_910/circuit_diagram.html |work=Formula1.com |publisher=[[Formula One Group|Formula One Management]] |deadurl=yes |archiveurl=https://web.archive.org/web/20141111011244/http://www.formula1.com/races/in_detail/abu_dhabi_910/circuit_diagram.html |archivedate=11 November 2014 }}</ref>
| Fulldate = {{Start date|3 November 2013|df=y}}
| Date = 3 November
| Year = 2013
| Race_No = 17
| Season_No = 19
| Official name = 2013 Formula 1 [[Etihad Airways]] [[Abu Dhabi Grand Prix]]
| Image = Circuit Yas-Island.svg
| image-size = 250px
| Location       = [[Yas Marina Circuit]]<br />[[Yas Island]], [[Abu Dhabi]], [[United Arab Emirates]]
| Course         = Permanent racing facility
| Course_km      = 5.554
| Course_mi      = 3.451
| Distance_laps  = 55
| Distance_mi    = 189.739
| Distance_km    = 305.355
| Weather =
| Pole_Driver = [[Mark Webber]]
| Pole_Team = [[Red Bull Racing|Red Bull]]-[[Renault Sport F1|Renault]]
| Pole_Time = 1:39.957
| Pole_Country = AUS
| Fast_Driver = [[Fernando Alonso]]
| Fast_Team = [[Scuderia Ferrari|Ferrari]]
| Fast_Time = 1:43.434
| Fast_Lap = 55
| Fast_Country = ESP
| First_Driver = [[Sebastian Vettel]]
| First_Team = [[Red Bull Racing|Red Bull]]-[[Renault in Formula One|Renault]]
| First_Country = GER
| Second_Driver = [[Mark Webber]]
| Second_Team = [[Red Bull Racing|Red Bull]]-[[Renault in Formula One|Renault]]
| Second_Country = AUS
| Third_Driver = [[Nico Rosberg]]
| Third_Team = [[Mercedes-Benz in Formula One|Mercedes]]
| Third_Country = GER
| Lapchart = {{F1Laps2013|ABU}}
}}
The '''2013 Abu Dhabi Grand Prix''' (formally known as the '''2013 Formula 1 Etihad Airways Abu Dhabi Grand Prix''')<ref name="formal"/> was a [[Formula One]] [[Auto racing|motor race]] that was held at the [[Yas Marina Circuit]] on 3 November 2013. The race was the seventeenth round of the 2013 season, and marked the 5th running of the Abu Dhabi Grand Prix.

The race, contested over 55 laps, was won by [[Sebastian Vettel]], driving a [[Red Bull Racing|Red Bull]]. [[Mark Webber]] finished second, and by [[Nico Rosberg]] driving a [[Mercedes-Benz in Formula One|Mercedes]] was third. Vettel won the race 30.8s ahead of his Red Bull Team mate Mark Webber.
Vettel's victory equalled [[Alberto Ascari]] and [[Michael Schumacher]]'s record in [[1953 Formula One season|1953]] and [[2004 Formula One season|2004]]  record for most consecutive wins in a season. This was  Red Bull's 100th podium. [[Adrian Sutil]] scored his last ever World Championship points at this race, [[Paul di Resta]] also scored his final points.

==Report==

===Background===

====Partial solar eclipse====
{{main|Solar eclipse of November 3, 2013}}
During the race it was possible to observe a partial [[solar eclipse]] from 17:22 local time to 17:39 local time when the [[Sun]] set, and this was shown briefly during the race live broadcast.

====Tyres====
Like the [[2012 Abu Dhabi Grand Prix]], tyre supplier Pirelli brought its white-banded medium compound tyre as the harder "prime" tyre and the yellow-banded soft compound tyre as the softer "option" tyre.

===Free Practice===
[[Romain Grosjean]] and [[Lewis Hamilton]] were top in the first practice session.<ref>{{cite news|title=Grosjean fastest in Abu Dhabi first practice|url=http://uk.reuters.com/article/2013/11/01/uk-motor-racing-prix-practice-idUKBRE9A00DV20131101|publisher=Reuters|accessdate=2 November 2013}}</ref>

[[Red Bull Racing|Red Bull]] drivers were fastest in the second practice session with Vettel leading Webber by a tenth of a second.<ref>{{cite news|title=Normal service continues as Red Bull and Sebastian Vettel dominate Abu Dhabi second practice|url=http://www.mirror.co.uk/sport/formula-1/abu-dhabi-grand-prix-red-2665184|publisher=Daily Mirror|accessdate=2 November 2013}}</ref>

During third practice Mercedes and Red Bull were close at the top and the session ended with Vettel two tenths of a second ahead of Webber who in turn was less than a hundredth of a second faster than Hamilton’s Mercedes<ref>{{cite news|title=Red Bull ahead of Mercedes as practice ends|url=http://www.nst.com.my/latest/vettel-again-fastest-in-3rd-practice-at-abu-dhabi-1.390441/|publisher=New Straits Times|accessdate=2 November 2013}}{{dead link|date=January 2016}}</ref>

===Qualifying===
Webber qualified in pole position ahead of his Red Bull team mate Vettel who qualified in second. The Mercedes drivers of Hamilton and Rosberg qualified on the second row of the grid. [[Fernando Alonso]] was the highest placed driver in the World Championship standings who failed to qualify for the final round of qualifying.

[[Jules Bianchi]] was penalised before the start of qualifying five places on the grid for an unscheduled gearbox change.<ref>{{cite web|title=Qualifying&nbsp;— superb Webber beats Vettel to pole in Abu Dhabi |url=http://www.formula1.com/news/headlines/2013/11/15195.html |accessdate=2 November 2013 |publisher=Formula1.com |deadurl=yes |archiveurl=https://web.archive.org/web/20131103191741/http://www.formula1.com/news/headlines/2013/11/15195.html |archivedate=3 November 2013 }}</ref>

====Post-qualifying====
[[Kimi Räikkönen]] was excluded from qualifying and was relegated to twenty-second place on the grid having originally qualified fifth place on the grid after his car failed a floor deflection test.<ref name="Kimi"/><ref>{{cite web|title=Kimi Raikkonen excluded from Abu Dhabi GP qualifying after Lotus car fails floor test |url=http://www1.skysports.com/f1/news/12433/9005358/kimi-raikkonen-excluded-from-abu-dhabi-gp-qualifying-after-lotus-car-fails-floor-test |accessdate=2 November 2013|publisher=Sky Sports F1}}</ref>

===Race===
The race began with Vettel in second on the grid overtaking Webber into the first corner and then leading until the chequered flag . Rosberg moved up to second position, while [[Kimi Räikkönen]] starting from the back of the grid broke his right-front suspension after contact with [[Giedo van der Garde]], causing him to retire from the race. German [[Nico Hülkenberg]], during his first pit-stop was deemed to have been unsafely released and was given a drive through penalty.<ref>{{citeweb|url = http://www.telegraph.co.uk/sport/motorsport/formulaone/10423551/Abu-Dhabi-Grand-Prix-2013-Sebastian-Vettel-triumphs-ahead-of-Mark-Webber-to-claim-seventh-consecutive-win.html|title=Sebastian Vettel triumphs ahead of Mark Webber to claim seventh consecutive win|date=3 November 2013|work=Daily Telegraph|accessdate=5 November 2013}}</ref>
Grosjean, advanced from sixth on the grid to fourth after the start. During the first half of the race, [[Felipe Massa]] led his team-mate Alonso, but was overtaken by Alonso in the latter stages of the race. After Alonso's second pit-stop, he came out of the pitlane and moved around backmarker [[Jean-Éric Vergne]], going off the racetrack with all four wheels in a move that consequently caused Massa to run wide into turn five. <ref>{{citeweb|url = http://www.dailymail.co.uk/sport/formulaone/article-2486213/Sebastian-Vettel-wins-Abu-Dhabi-Grand-Prix.html?ico=sport%5Eheadlines|title=No foot off the gas for Vettel with champion driver in seventh heaven at Abu Dhabi GP|date=3 November 2013|work=Daily Mail|accessdate=5 November 2013}}</ref>
Vettel won the race by 30.8s over Webber.<ref>{{citeweb|url = http://www.bbc.co.uk/sport/0/formula1/24795236|title=Sebastian Vettel takes crushing Abu Dhabi GP win for Red Bull|date=3 November 2013|work=BBC Sport|accessdate=5 November 2013}}</ref>

===Post race===
During the cool down lap Vettel and Webber performed [[Doughnut (driving)|doughnuts]]. As Vettel had done one after the [[2013 Indian Grand Prix|previous race in India]], this time he incurred no penalty from the stewards.<ref>{{cite news|title=Abu Dhabi GP: Sebastian Vettel unpunished after donut celebrations|url=http://www.autosport.com/news/report.php/id/111115|accessdate=4 November 2013|newspaper=Autosport}}</ref><ref>{{cite web|title=Vettel wie im Rausch, aber auch geschockt |url=http://www.kicker.de/news/formel1/startseite/594485/artikel_vettel-wie-im-rausch-aber-auch-geschockt.html |accessdate=3 November 2013|publisher=Kicker.de|language=German}}</ref>

Alonso was sent for precautionary medical checks after a kerb impact measured at [[g-force|25g]].<ref>{{cite web|title=Formula 1 - Alonso had 'big hit' in Vergne moment
 |url=http://uk.eurosport.yahoo.com/news/formula-1-alonso-big-hit-vergne-moment-182607846--f1.html |accessdate=4 November 2013|publisher=Eurosport|date=3 November 2013}}</ref>

==Classification==

===Qualifying===
{| class=wikitable style="font-size:95%"
! {{Tooltip|Pos.|Qualifying position}}
! {{Tooltip|No.|Number}}
! Driver
! Constructor
! Q1
! Q2
! Q3
! {{Tooltip|Grid|Final grid position}}
|-
! 1
| align="center" | 2
| {{flagicon|AUS}} [[Mark Webber]]
| [[Red Bull Racing|Red Bull]]-[[Renault in Formula One|Renault]]
| 1:41.568
| 1:40.575
| '''1:39.957'''
| 1
|-
! 2
| align="center" | 1
| {{flagicon|GER}} [[Sebastian Vettel]]
| [[Red Bull Racing|Red Bull]]-[[Renault in Formula One|Renault]]
| 1:41.683
| 1:40.781
| 1:40.075
| 2
|-
! 3
| align="center" | 9
| {{flagicon|GER}} [[Nico Rosberg]]
| [[Mercedes-Benz in Formula One|Mercedes]]
| 1:41.420
| '''1:40.473'''
| 1:40.419
| 3
|-
! 4
| align="center" | 10
| {{flagicon|GBR}} [[Lewis Hamilton]]
| [[Mercedes-Benz in Formula One|Mercedes]]
| '''1:40.693'''
| 1:40.477
| 1:40.501
| 4
|-
! DSQ
| align="center" | 7
| {{flagicon|FIN}} [[Kimi Räikkönen]]
| [[Lotus F1|Lotus]]-[[Renault in Formula One|Renault]]
| 1:41.276
| 1:40.971
| 1:40.542
| 22{{ref|1|1}}
|-
! 6
| align="center" | 11
| {{flagicon|GER}} [[Nico Hülkenberg]]
| [[Sauber]]-[[Scuderia Ferrari|Ferrari]]
| 1:41.631
| 1:40.931
| 1:40.576
| 5
|-
! 7
| align="center" | 8
| {{flagicon|FRA}} [[Romain Grosjean]]
| [[Lotus F1|Lotus]]-[[Renault in Formula One|Renault]]
| 1:41.447
| 1:40.948
| 1:40.997
| 6
|-
! 8
| align="center" | 4
| {{flagicon|BRA}} [[Felipe Massa]]
| [[Scuderia Ferrari|Ferrari]]
| 1:41.254
| 1:40.989
| 1:41.015
| 7
|-
! 9
| align="center" | 6
| {{flagicon|MEX}} [[Sergio Pérez]]
| [[McLaren]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| 1:41.687
| 1:40.812
| 1:41.068
| 8
|-
! 10
| align="center" | 19
| {{flagicon|AUS}} [[Daniel Ricciardo]]
| [[Scuderia Toro Rosso|Toro Rosso]]-[[Scuderia Ferrari|Ferrari]]
| 1:41.884
| 1:40.852
| 1:41.111
| 9
|-
! 11
| align="center" | 3
| {{flagicon|ESP}} [[Fernando Alonso]]
| [[Scuderia Ferrari|Ferrari]]
| 1:41.397
| 1:41.093
|
| 10
|-
! 12
| align="center" | 14
| {{flagicon|GBR}} [[Paul di Resta]]
| [[Force India]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| 1:41.676
| 1:41.133
|
| 11
|-
! 13
| align="center" | 5
| {{flagicon|GBR}} [[Jenson Button]]
| [[McLaren]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| 1:41.817
| 1:41.200
|
| 12
|-
! 14
| align="center" | 18
| {{flagicon|FRA}} [[Jean-Éric Vergne]]
| [[Scuderia Toro Rosso|Toro Rosso]]-[[Scuderia Ferrari|Ferrari]]
| 1:41.692
| 1:41.279
|
| 13
|-
! 15
| align="center" | 16
| {{flagicon|VEN}} [[Pastor Maldonado]]
| [[Williams F1|Williams]]-[[Renault in Formula One|Renault]]
| 1:41.365
| 1:41.395
|
| 14
|-
! 16
| align="center" | 17
| {{flagicon|FIN}} [[Valtteri Bottas]]
| [[Williams F1|Williams]]-[[Renault in Formula One|Renault]]
| 1:41.862
| 1:41.447
|
| 15
|-
! 17
| align="center" | 12
| {{flagicon|MEX}} [[Esteban Gutiérrez]]
| [[Sauber]]-[[Scuderia Ferrari|Ferrari]]
| 1:41.999
|
|
| 16
|-
! 18
| align="center" | 15
| {{flagicon|GER}} [[Adrian Sutil]]
| [[Force India]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| 1:42.051
|
|
| 17
|-
! 19
| align="center" | 21
| {{flagicon|NED}} [[Giedo van der Garde]]
| [[Caterham F1|Caterham]]-[[Renault in Formula One#2011–present|Renault]]
| 1:43.252
|
|
| 18
|-
! 20
| align="center" | 22
| {{flagicon|FRA}} [[Jules Bianchi]]
| [[Marussia F1|Marussia]]-[[Cosworth]]
| 1:43.398
|
|
| 21{{ref|2|2}}
|-
! 21
| align="center" | 20
| {{flagicon|FRA}} [[Charles Pic]]
| [[Caterham F1|Caterham]]-[[Renault in Formula One#2011–present|Renault]]
| 1:43.528
|
|
| 19
|-
! 22
| align="center" | 23
| {{flagicon|GBR}} [[Max Chilton]]
| [[Marussia F1|Marussia]]-[[Cosworth]]
| 1:44.198
|
|
| 20
|-
! colspan=8 | 107% time: 1:47.741
|-
! colspan=8 | Source:<ref name="Qualifying results">{{cite news|title=2013 Formula 1 Etihad Airways Abu Dhabi Grand Prix Qualifiying Results |url=http://www.formula1.com/results/season/2013/910/7280/ |accessdate=2 November 2013 |work=Formula1.com |publisher=[[Formula One Group|Formula One Administration]] |date=2 November 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20131103185620/http://www.formula1.com/results/season/2013/910/7280/ |archivedate=3 November 2013 }}</ref>
|-
|}
'''Notes:'''<br />
{{note|1|1}}&nbsp;- [[Kimi Räikkönen]] qualified fifth, but was disqualified from qualifying when his car failed a floor deflection test.<ref name="Qualifying results" /><br>
{{note|2|2}}&nbsp;- [[Jules Bianchi]] qualified twentieth, but penalised five grid places for an unscheduled gearbox change<ref name="Qualifying results" /> and started from twenty-first after Räikkönen was disqualified from qualifying.<ref name="Kimi">{{cite news|title=Raikkonen excluded from qualifying after failed floor test |url=http://www.formula1.com/news/headlines/2013/11/15198.html |accessdate=3 November 2013 |newspaper=Formula1.com |deadurl=yes |archiveurl=https://web.archive.org/web/20131103201144/http://www.formula1.com/news/headlines/2013/11/15198.html |archivedate=3 November 2013 }}</ref>

===Race===
{| class=wikitable style="font-size:95%"
! {{Tooltip|Pos.|Qualifying position}}
! {{Tooltip|No.|Car Number}}
! Driver
! Constructor
! Laps
! Time/Retired
! {{Tooltip|Grid|Final grid position}}
! Points
|-
! 1
| align="center" | 1
| {{flagicon|DEU}} '''[[Sebastian Vettel]]'''
| '''[[Red Bull Racing|Red Bull]]-[[Renault in Formula One|Renault]]'''
| 55
| 1:38:06.106
| 2
| '''25'''
|-
! 2
| align="center" | 2
| {{flagicon|AUS}} '''[[Mark Webber]]'''
| '''[[Red Bull Racing|Red Bull]]-[[Renault in Formula One|Renault]]'''
| 55
| +30.829
| 1
| '''18'''
|-
! 3
| align="center" | 9
| {{flagicon|GER}} '''[[Nico Rosberg]]'''
| '''[[Mercedes-Benz in Formula One|Mercedes]]'''
| 55
| +33.650
| 3
| '''15'''
|-
! 4
| align="center" | 8
| {{flagicon|FRA}} '''[[Romain Grosjean]]'''
| '''[[Lotus F1|Lotus]]-[[Renault in Formula One|Renault]]'''
| 55
| +34.802
| 6
| '''12'''
|-
! 5
| align="center" | 3
| {{flagicon|ESP}} '''[[Fernando Alonso]]'''
| '''[[Scuderia Ferrari|Ferrari]]'''
| 55
| +1:07.181
| 10
| '''10'''
|-
! 6
| align="center" | 14
| {{flagicon|GBR}} '''[[Paul di Resta]]'''
| '''[[Force India]]-[[Mercedes-Benz in Formula One|Mercedes]]'''
| 55
| +1:18.174
| 11
| '''8'''
|-
! 7
| align="center" | 10
| {{flagicon|GBR}} '''[[Lewis Hamilton]]'''
| '''[[Mercedes-Benz in Formula One|Mercedes]]'''
| 55
| +1:19.267
| 4
| '''6'''
|-
! 8
| align="center" | 4
| {{flagicon|BRA}} '''[[Felipe Massa]]'''
| '''[[Scuderia Ferrari|Ferrari]]'''
| 55
| +1:22.886
| 7
| '''4'''
|-
! 9
| align="center" | 6
| {{flagicon|MEX}} '''[[Sergio Pérez]]'''
| '''[[McLaren]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]'''
| 55
| +1:31.198
| 8
| '''2'''
|-
! 10
| align="center" | 15
| {{flagicon|GER}} '''[[Adrian Sutil]]'''
| '''[[Force India]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]'''
| 55
| +1:33.257
| 17
| '''1'''
|-
! 11
| align="center" | 16
| {{flagicon|VEN}} [[Pastor Maldonado]]
| [[Williams F1|Williams]]-[[Renault in Formula One|Renault]]
| 55
| +1:35.989
| 14
|
|-
! 12
| align="center" | 5
| {{flagicon|GBR}} [[Jenson Button]]
| [[McLaren]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| 55
| +1:43.767
| 12
|
|-
! 13
| align="center" | 12
| {{flagicon|MEX}} [[Esteban Gutiérrez]]
| [[Sauber]]-[[Scuderia Ferrari|Ferrari]]
| 55
| +1:44.295
| 16
|
|-
! 14
| align="center" | 11
| {{flagicon|GER}} [[Nico Hülkenberg]]
| [[Sauber]]-[[Scuderia Ferrari|Ferrari]]
| 54
| +1 Lap
| 5
|
|-
! 15
| align="center" | 17
| {{flagicon|FIN}} [[Valtteri Bottas]]
| [[Williams F1|Williams]]-[[Renault in Formula One|Renault]]
| 54
| +1 Lap
| 15
|
|-
! 16
| align="center" | 19
| {{flagicon|AUS}} [[Daniel Ricciardo]]
| [[Scuderia Toro Rosso|Toro Rosso]]-[[Scuderia Ferrari|Ferrari]]
| 54
| +1 Lap
| 9
|
|-
! 17
| align="center" | 18
| {{flagicon|FRA}} [[Jean-Éric Vergne]]
| [[Scuderia Toro Rosso|Toro Rosso]]-[[Scuderia Ferrari|Ferrari]]
| 54
| +1 Lap
| 13
|
|-
! 18
| align="center" | 21
| {{flagicon|NED}} [[Giedo van der Garde]]
| [[Caterham F1|Caterham]]-[[Renault in Formula One|Renault]]
| 54
| +1 Lap
| 18
|
|-
! 19
| align="center" | 20
| {{flagicon|FRA}} [[Charles Pic]]
| [[Caterham F1|Caterham]]-[[Renault in Formula One|Renault]]
| 54
| +1 Lap
| 19
|
|-
! 20
| align="center" | 22
| {{flagicon|FRA}} [[Jules Bianchi]]
| [[Marussia F1|Marussia]]-[[Cosworth]]
| 53
| +2 Laps
| 21
|
|-
! 21
| align="center" | 23
| {{flagicon|GBR}} [[Max Chilton]]
| [[Marussia F1|Marussia]]-[[Cosworth]]
| 53
| +2 Laps
| 20
|
|-
! Ret
| align="center" | 7
| {{flagicon|FIN}} [[Kimi Räikkönen]]
| [[Lotus F1|Lotus]]-[[Renault in Formula One|Renault]]
| 0
| Collision\ Suspension
| 22
|
|-
! colspan=8 | Source:<ref name="Race Results">{{cite news|title=2013 Abu Dhabi Grand Prix Race Results |url=http://www.formula1.com/results/season/2013/910/7281/ |accessdate=3 November 2013 |newspaper=Formula1.com |deadurl=yes |archiveurl=https://web.archive.org/web/20131103201226/http://www.formula1.com/results/season/2013/910/7281/ |archivedate=3 November 2013 }}</ref>
|-
|}

==Notes==
* '''Last pole position:''' [[Mark Webber]]. 
* '''60th podium:''' [[Sebastian Vettel]].
* Last points for [[Paul di Resta]] and [[Adrian Sutil]].

==Championship standings after the race==
{{col-start}}
{{col-2}}
;Drivers' Championship standings
{| class="wikitable" style="font-size: 95%;"
|-
!
! {{Tooltip|Pos.|Position}}
! Driver
! Points
|-
| align="left" | [[File:1rightarrow_blue.svg|10px]]
| align="center" | 1
| {{flagicon|GER}} '''[[Sebastian Vettel]]'''
| align="left" | 347
|-
| align="left" | [[File:1rightarrow_blue.svg|10px]]
| align="center" | 2
| {{flagicon|ESP}} [[Fernando Alonso]]
| align="left" | 217
|-
| align="left" | [[File:1rightarrow_blue.svg|10px]]
| align="center" | 3
| {{flagicon|FIN}} [[Kimi Räikkönen]]
| align="left" | 183
|-
| align="left" | [[File:1rightarrow_blue.svg|10px]]
| align="center" | 4
| {{flagicon|GBR}} [[Lewis Hamilton]]
| align="left" | 175
|-
| align="left" | [[File:1rightarrow_blue.svg|10px]]
| align="center" | 5
| {{flagicon|AUS}} [[Mark Webber]]
| align="left" | 166
|-
|}
{{col-2}}
;Constructors' Championship standings
{|class="wikitable" style="font-size: 95%;"
|-
!
! {{Tooltip|Pos.|Position}}
! Constructor
! Points
|-
| align="left" | [[File:1rightarrow_blue.svg|10px]]
| align="center" | 1
| {{flagicon|AUT}} '''[[Red Bull Racing|Red Bull]]-[[Renault in Formula One|Renault]]'''
| align="left" | 513
|-
| align="left" | [[File:1rightarrow_blue.svg|10px]]
| align="center" | 2
| {{flagicon|GER}} [[Mercedes-Benz in Formula One|Mercedes]]
| align="left" | 334
|-
| align="left" | [[File:1rightarrow_blue.svg|10px]]
| align="center" | 3
| {{flagicon|ITA}} [[Scuderia Ferrari|Ferrari]]
| align="left" | 323
|-
| align="left" | [[File:1rightarrow_blue.svg|10px]]
| align="center" | 4
| {{flagicon|GBR}} [[Lotus F1|Lotus]]-[[Renault in Formula One|Renault]]
| align="left" | 297
|-
| align="left" | [[File:1rightarrow_blue.svg|10px]]
| align="center" | 5
| {{flagicon|GBR}} [[McLaren]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| align="left" | 95
|-
|}
{{col-end}}

* <small>'''Note''': Only the top five positions are included for both sets of standings.</small>

==References==
{{reflist}}

==External links==
{{Commons category|2013 Abu Dhabi Grand Prix}}

{{F1 race report
| Name_of_race = [[Abu Dhabi Grand Prix]]
| Year_of_race = 2013
| Previous_race_in_season = [[2013 Indian Grand Prix]]
| Next_race_in_season = [[2013 United States Grand Prix]]
| Previous_year's_race = [[2012 Abu Dhabi Grand Prix]]
| Next_year's_race = [[2014 Abu Dhabi Grand Prix]]
}}

{{F1GP 10-19}}

[[Category:2013 Formula One races|Abu Dhabi]]
[[Category:2013 in Emirati sport]]
[[Category:Abu Dhabi Grand Prix]]