{{Infobox journal
| title = St. Mary's Law Journal
| cover = [[File:StMarysLawJournalFirstIssue.jpg|200 px]]
| editor =
| discipline = [[Law]]
| abbreviation = St. Marys Law J.
| publisher = [[St. Mary's University, Texas|St. Mary's University]]
| country = United States
| frequency = Quarterly
| history = 1969-present
| website = http://www.stmaryslawjournal.org/
| link1 = http://stmaryslawjournal.org/recent_issues.aspx
| link1-name = Online archive
| OCLC = 02643086
| LCCN = 70012297
| ISSN = 0581-3441}}

'''''St. Mary's Law Journal''''' ([[Bluebook]] abbreviation: ''St. Mary's L.J.'') is a [[law review]] published by an independent student group at [[St. Mary's University School of Law]].

==Ranking==
The journal is consistently ranked among the most frequently cited law journals by state and federal courts. In the last five years, the journal has ranked as high as #3 and as low as #31 for citations by courts out of 1,659 US law reviews.<ref name=rankings>[http://lawlib.wlu.edu/lj/ W&L Law Journal Rankings]</ref> In 2010, the journal was the tenth most frequently cited law review in state and federal court decisions.<ref name=rankings/>

==History==
The journal was established in 1969 as a "practitioner's journal".<ref>[http://stmaryslawjournal.org/history.aspx History - St. Mary's Law Journal website]</ref> The inaugural [[editorial board]] received substantial support from the staff of the ''[[Harvard Law Review]]'' and the ''Houston Law Review''.<ref name=Beirne>[http://stmaryslawjournal.org/pdfs/History-Beirne.pdf Martin D. Beirne, My How You've Grown: St. Mary's Law Journal Turns Forty]</ref> The first article was authored by Justice Joe Greenhill of the [[Texas Supreme Court]], who later became Chief Justice of the court. Greenhill requested that the first [[editor-in-chief]], Martin Beirne {{who?|date=September 2013}}, become a named co-author because of his substantial work in preparing the article.<ref name=Beirne/>

The journal publishes a quadrennial revision of ''Hall's Standards of Review in Texas.'' This article is a comprehensive guide to standards of review in Texas state courts and is widely cited by them,<ref name=standards>[http://www.texasappellatelawblog.com/tags/wendell-hall/ Hall to Update Standards of Review in Texas]</ref> contributing substantially to the success of ''St. Mary's Law Journal'' in the Washington & Lee Law Journal Rankings.<ref name=rankings/>

The journal is headquartered in the Sarita Kenedy East Law Library at St. Mary's campus. Each year, 40 editors are selected total. Twelve are selected solely based on their first-year grades. Twelve are selected based solely on their competition scores. The remaining sixteen editors are selected based on a combination of their first-year grades and their competition scores. {{cn|date=September 2013}}

==''St. Mary's Journal on Legal Malpractice & Ethics''==
In 2011, the ''St. Mary's Law Journal'' began producing the ''St. Mary's Journal on Legal Malpractice & Ethics'', which is produced annually by the members of the journal. It is the only printed law review in the United States to specifically focus on topics in the field of legal malpractice. This topic-specific journal addresses legal malpractice and ethics issues that impact the daily work of legal practitioners. To this effort, ''St. Mary's Law Journal'' also hosts an annual symposium on legal malpractice and ethics issues including modern topics like "Professional Responsibility in Social Media".<ref>{{cite web|title=Tenth Annual Symposium on Legal Malpractice and Professional Responsibility|url=http://sutliffstout.com/tenth-annual-symposium-on-legal-malpractice-and-professional-responsibility}}</ref>

==Notable authors==
Authors who have been published in journal include [[Chief Justice of the United States|Chief Justice of the United States  Supreme Court]] [[William Rehnquist]], [[Carla Anderson Hills|Carla Hills]] (then a member of the President [[Gerald Ford]]'s cabinet), and [[Broadus A. Spivey]], president of the [[State Bar of Texas]].<ref>[http://stmaryslawjournal.org/aboutus.aspx About St. Mary's Law Journal]</ref>

==Notable ''St. Mary's Law Journal'' include== 
Notable alumni of the ''St. Mary's Law Journal'' include:
* [[John Love (judge)|John Love]], 1995, [[United States magistrate judge]], [[United States District Court for the Eastern District of Texas]]
* [[David Ezra]], 1972, District Judge, [[United States District Court for the District of Hawaii]]
* [[Karen Angelini]], 1979, Justice, [[Fourth Court of Appeals of Texas]]
* [[Paul W. Green]], 1977, Justice, [[Texas Supreme Court]]
* [[Phylis J. Speedlin]], Justice, Fourth Court of Appeals of Texas
* [[Sam Griffith]], Justice, [[Twelfth Court of Appeals of Texas]]
* [[Gregory Perkes]], 1987, Justice, [[Thirteenth Court of Appeals of Texas]]

==Other sources== 
* {{cite journal|last=Wendell|first=Hall |authorlink=Wendell Hall|year=2010|title=Hall's Standards of Review in Texas|journal=St. Mary's Law Journal|volume=42|issue=1}}
* {{cite journal|last=Flint|first=Richard E.|authorlink=James Bradley Thayer|year=1893|title=The Evolving Standards for Granting Mandamus Relief in the Texas Supreme Court: One More "Mile Marker Down the Road of No Return"|journal=St. Mary's Law Journal|volume=39|issue=1}}

==References==
{{Reflist}}

==External links==
*{{Official website|http://www.stmaryslawjournal.org/}}

[[Category:American law journals]]
[[Category:General law journals]]
[[Category:Publications established in 1887]]
[[Category:English-language journals]]
[[Category:Law journals edited by students]]
[[Category:Quarterly journals]]