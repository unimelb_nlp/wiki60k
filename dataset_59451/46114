{{DISPLAYTITLE: Aliyah (''NCIS'')}}
{{Infobox television episode
| title        = Aliyah
| series       = [[NCIS (TV series)|NCIS]]
| image        = NCIS - Aliyah.png
| image_size   = 250px
| caption      = Tony and Ziva's confrontation in Israel
| season       = 6
| episode      = 25
| director     = [[Dennis Smith (director)|Dennis Smith]]
| writer       = [[David North (screenwriter)|David North]]
| airdate      = {{Start date|2009|5|19}}
| guests       = * [[Brian Dietzen]] as [[Jimmy Palmer]]
* [[Michael Nouri]] as Mossad Director [[List of NCIS characters#Eli David|Eli David]]
* Merik Tadros as Mossad Officer [[List of NCIS characters#Michael Rivkin|Michael Rivkin]]
* [[Arnold Vosloo]] as Mossad Officer Amit Hadar
* [[Omid Abtahi]] as Saleem Ulman
* Inger Tudor as Dr. Sandra Holdren
| prev         = [[NCIS (season 6)#ep137|Semper Fidelis]]
| next         = [[Truth or Consequences (NCIS)|Truth or Consequences]]
| episode_list = [[NCIS (season 6)|''NCIS'' (season 6)]]<br />[[List of NCIS episodes|List of ''NCIS'' episodes]]
}}

"'''Aliyah'''" is the twenty-fifth and final episode of the [[NCIS (season 6)|sixth season]] of the American [[police procedural]] drama ''[[NCIS (TV series)|NCIS]]'', and the 138th episode overall. It originally aired on [[CBS]] in the United States on May 19, 2009. The episode is written by David North and directed by Dennis Smith, and was seen live by 16.51 million viewers.<ref name="TV by the Numbers"/>

"Aliyah" picks up following a cliff-hanger in the previous episode, "Semper Fidelis", in which [[Tony DiNozzo]] fatally shoots [[Ziva David]]'s Mossad boyfriend, Michael Rivkin, in self-defense. It also follows the "Legend" storyline, culminating in Ziva's departure from NCIS and eventual captivity in Somalia.

== Etymology ==

"[[Aliyah]]" is a Hebrew word which literally means "going up" or "ascent". It is most commonly associated with Jewish immigration to Israel and return to the [[Holy Land]].<ref>[http://www.jewfaq.org/defs/aliyah.htm Definition: Aliyah]</ref> In this case, it is a reference to Ziva's return to Israel and her blood family.

== Plot ==
Despite Ziva's efforts to help him, [[Mossad]] Officer Michael Rivkin (Merik Tadros) dies in the [[hospital]], having suffered injuries from his fight against Tony in the last few seconds of the previous episode ("Semper Fidelis"), while Tony survives albeit with a broken left arm. While Tony claims to have acted solely in self-defense, Ziva, infuriated, does not believe him and Director Vance is doubtful that he could have fairly survived the brawl with a Mossad assassin who was also a member of the [[Kidon]] Unit.<ref name="Aliyah">{{Cite episode|title=Aliyah|series=NCIS|serieslink=NCIS (TV series)|network=[[CBS]]|date=May 19, 2009|season=6|number=25}}</ref>

Matters are further complicated after Ziva's apartment is destroyed in an explosion, severely damaging the evidence and the crime scene. Gibbs approaches Ziva, knowing that the gas lines were cut and the explosion was intentional, and asks her if she knows any possible suspects. She deflects, and when Gibbs notes her stoical response, reminding her that it is her home in question, she replies, "No it's not."<ref name="Aliyah"/>

Ducky confirms that Rivkin and Tony's injuries line up with the latter's version of the story and provides a reason for his ability to win the fight: Rivkin was intoxicated by alcohol.<ref name="Aliyah"/>

As tension mounts, Vance announces that he's decided to make a request a reality and that he, Gibbs, Tony and Ziva are on the next flight to Tel Aviv, Israel, having been summoned there at the request of Eli David ([[Michael Nouri]]), the enigmatic and powerful head of Mossad and also Ziva's father while McGee and Abby are to stay in Washington D.C. to work on the case. In Israel, Eli interrogates Tony, accusing him of killing Rivkin solely out of jealously; DiNozzo in turn accuses Eli of sending rogue agents to Washington D.C. and ordering Rivkin to become involved romantically with Ziva to keep an eye on her.<ref name="Aliyah"/>

Tony later approaches Ziva, who viewed the interrogation, and tells her that Rivkin was "playing" her under orders from her father. She refuses to believe him, and he maintains that he had no choice in killing Rivkin. After he urges her "to take a punch" and "get it out of [her] system", Ziva snaps, knocking him to the ground while furiously contending that he could have chosen to wound, rather than kill, during the brawl. When he quietly asks her if she had loved Rivkin, she answers, "Guess I'll never know."<ref name="Aliyah"/>

Afterwards, she confronts Eli in his office, demanding to know if her relationship with Rivkin was real. He admits that he does not know, confirming Tony's assertions. He further states that he is no longer certain of her loyalty, insisting that she return to Mossad as a full-time operative and complete Rivkin's assignment.<ref name="Aliyah"/>

Meanwhile, McGee and Abby are in [[Washington, D.C.|Washington]] working on the laptop found in the wreckage of Ziva's home. They manage to recover various emails and pieces of information that allow them to assemble basic conclusions about the objective of Rivkin's mission and they also discover that Rivkin was in contact with Ziva.<ref name="Aliyah"/>

Gibbs, having gotten the information ultimately decides to leave Ziva in [[Tel Aviv]], saying that he would give her time to "remember who she can trust" and believing that she will return to NCIS soon enough. The team is shocked by the turn of events, and Ducky privately expresses his surprise that Gibbs would come to such a decision, saying, "You took to Ziva more quickly than to any other agent before her. Timothy, Caitlin, even DiNozzo. I've always sensed there's a strong bond between the two of you. Something shared perhaps." However, Vance confronts him shortly afterwards and questions the integrity of Ziva's position at NCIS as well as her motives for killing her half-brother, Ari Haswari, four years earlier to save Gibbs. Despite knowing that Gibbs doesn't want to hear this, Vance believes that Ziva will serve them well at Mossad. Gibbs, on the other hand disagrees, stating if Vance knows Eli, then they'll probably never see Ziva again.<ref name="Aliyah"/>

Ziva is later shown having once again been recruited into Mossad and embarking on a mission to stop a terrorist cell. The episode ends in a cliffhanger when in the Cape of Africa, terrorist leader Saleem Ulman ([[Omid Abtahi]]) enters a room and asks a female captive for information on NCIS before revealing the captive is none other than Ziva herself who has been tortured.<ref name="Aliyah"/>

== Production ==

=== Writing ===

The episode is written by David North and directed by Dennis Smith. Before the finale aired, then executive producer [[Shane Brennan]] stated that it would be "jaw-dropping" and "stunning" and that the impact of the episode's events would "send shockwaves" through NCIS. He further elaborated, "We filmed a secret ending, and those pages were only in one copy of the script. People will see the finale and say, 'I know what's going to happen next in the first episode back.' Wrong. You don't know what's going to happen. People will go, 'Am I really seeing that?  What just happened?' It really is exciting. I promised that with [the Season 5 finale] 'Judgment Day,' and I think we delivered. It is very much in that vein."<ref>{{cite web|last=Diaz|first=Glenn|title=NCIS: Preview of Finale "Aliyah"|url=http://www.buddytv.com/articles/ncis/ncis-preview-of-finale-aliyah-28800.aspx|work=[[BuddyTV]]|accessdate=23 March 2013}}</ref>

Fans had previously discussed the possibility that Ziva's relationship with Michael Rivkin had been "arranged" by her father, as was confirmed in "Aliyah".<ref name=tvguide/> However, Merik Tadros, who portrayed Rivkin, voiced his opinion on the subject: "They could easily have been paired and had to be in love and then had to learn to love each other.... The way I've always approached it is that Rivkin loves Ziva. They've always wanted to add some suspense and mystery to all of it, but I would always say to the directors, 'How could I not love her?'"<ref name=tvguide>{{cite web|last=Mitovich|first=Matt|title=''NCIS'' Preview: "You Are Witnessing the Calm Before the Storm"|url=http://www.tvguide.com/News/NCIS-Preview-Tadros-1005967.aspx|work=TV Guide|accessdate=March 23, 2013}}</ref>

When asked how Tony and Ziva's relationship would be affected, Cote de Pablo replied, "We could not be farther away from kissing. I’m telling you, it’s not 'I hate you' [''kissing sound''] anger; it's 'I want to kill you' anger. Like, 'I want to shoot you in the head, but instead I'm gonna put my gun on your knee' anger...He does something to her that is almost unforgivable. However, the reason he does it is because he truly believes it’s going to help her. Little does he know that he almost kills her...Physically and emotionally."<ref name=ew1>{{cite web|last=Bierly|first=Mandi|title='NCIS's Cote de Pablo talks Tiva tension and takes the EW Pop Culture Personality Test|url=http://popwatch.ew.com/2009/05/05/cote-de-pablo-n/|work=Entertainment Weekly|accessdate=March 23, 2013}}</ref> Tadros was asked a similar question and responded, "I think that love can hurt and heal. In this case, we'll have opportunity to see both of those things take place. I don't want to say too much, but... I think it will only make their connection stronger, as they learn a lot more about each other."<ref name=tvguide/>
[[File:NCIS Score Cover.jpg|thumb|left|200px|The cover art for the album NCIS: The Official TV Soundtrack - Vol. 1]]
Tadros further stated, "It gets pretty intense, without question. And it was very exciting, because I felt like it shed light on DiNozzo and David in a way that has never before done in the show. It was nice to be a part of that."<ref name=tvguide/>

The episode marked the first appearance of Mossad Officer Amit Hadar and the second of Mossad Director Eli David, who first appeared in the season's premier.

=== Music ===

''NCIS'' composer [[Brian Kirk]] created some of the sound tracks used for the episode, including the pieces entitled "Ziva Betrayed" and "Aliyah", which lasted 2:42 and 3:01 minutes respectively.<ref name="sound track">''[[NCIS: The Official TV Soundtrack]]''. Volume 1. ''NCIS''. DVD. Prod. by [[CBS]]. (2009 DVD release).</ref><ref>{{cite web|title=NCIS Official TV Score|url=http://www.cbsrecords.com/Store/NCIS_TVScore.html|work=CBS|accessdate=May 11, 2013}}</ref> They were released on disk with the ''NCIS: The Official TV Soundtrack'' later in 2009.<ref name="sound track"/>

Pauley Perrette's song, "Fear", was played during the scenes with characters Tony, Gibbs, Vance, and Ziva first arriving in Israel.<ref name="sound track"/>

== Reception ==

"Aliyah" was watched by 16.51 million viewers live following its broadcast on May 19, 2009.<ref name="TV by the Numbers">{{cite web |url=http://tvbythenumbers.com/2009/05/27/top-cbs-primetime-shows-may-18-24-2009/19466 |title=Top CBS Primetime Shows, May 18–24 |first=Robert |last=Seidman |date=May 27, 2009 |accessdate=March 15, 2010 |publisher=TV by the Numbers}}</ref> ''NCIS'' was, as a result, one of the most watched television programs in America during the week of May 18–24, second to ''[[The Mentalist]]'', which was seen by 16.82 million viewers.<ref name="TV by the Numbers"/> Compared to the previous episode, "Semper Fidelis", "Aliyah" was up in viewers.

[[BuddyTV]] listed the sixth-season finale among its list of "The 10 Best Ziva Episodes"<ref>{{cite web|last=Calbert|first=Michelle|title='NCIS' Season 10 Celebration: The 10 Best Ziva Episodes|url=http://www.buddytv.com/slideshows/ncis/ncis-season-10-celebration-the-10-best-ziva-episodes-93372.aspx|work=[[BuddyTV]]|date=May 9, 2013|accessdate=May 9, 2013}}</ref> and later among "The 10 Best 'Tiva' Episodes".<ref>{{cite web|title='NCIS' Season 10 Celebration: The 10 Best 'Tiva' Episodes|url=http://www.buddytv.com/slideshows/ncis/ncis-season-10-celebration-10-best-tiva-episodes-50658.aspx|work=BuddyTV|accessdate=May 16, 2013|date=May 14, 2013}}</ref> Sarah Lafferty from Starpulse.com noted that the interrogation scene between Eli and DiNozzo was "a fantastic way to construct the playing field for a good season-ending conflict" but that the plot was slow at certain points. She concluded, "Leaving [Ziva] tortured until next season however, is a very interesting move that I highly approve of. Great move on the part of the creators. Not only did they manage to tie the entire series together (bringing up Ari's storyline in seasons two and three) but they left both new and old fans dying for more."<ref>{{cite web|last=Lafferty|first=Sarah|title='NCIS' Season Finale: 'Aliyah'|url=http://www.starpulse.com/news/index.php/2009/05/20/ncis_season_finale_aliyah_|work=Starpulse.com|accessdate=23 March 2013}}</ref>

Brandon Millman of [[Zap2it]] remarked, "Season six of "NCIS" began with Leroy Jethro Gibbs successfully getting the gang back together... now the love triangle of death threatens to once again tear it apart. Can anyone or anything save Team Gibbs from another bout of separation anxiety?"<ref>{{cite web|last=Millman|first=Brandon|title=FinaleWatch: Ziva leaves 'NCIS', but for how long?|url=http://blog.zap2it.com/frominsidethebox/2009/05/finalewatch-ziva-leaves-ncis-but-for-how-long.html|work=[[Zap2it]]|accessdate=May 21, 2013|date=May 19, 2009}}</ref>

Some Jewish commentators discussed the portrayal of Israel and Israelis. Rachel Leibold from ''[[Jweekly]]'' commented, "While I'm highly doubtful that any of the show was actually filmed in ''ha'aretz'', I did get a major kick out of all the Hebrew signage...There were also a couple of cute Hebrew touches - like Nouri's character saying "rak shni'ah" ("just a second") when Ziva walked into his office while he was doing paperwork. And in a sort of weird moment, goth forensic chick Abby, who didn't travel with the team, freaked out to Mark Harmon's character over the phone about how badly she wants to visit Israel - noting that it was 'No. 3' on her list of dream vacations, after the Galapagos Islands and Dollywood."<ref>{{cite web|last=Leibold|first=Rachel|title=CBS’s "NCIS" makes "Aliyah"|url=http://www.jweekly.com/blog/full/38095/cbss-ncis-makes-aliyah/|work=[[Jweekly]]|accessdate=23 March 2013}}</ref> Elliot B. Gertel of ''[[Jewish World Review]]'' wrote that the episode "focused on Ziva and her drive for integrity in all her loyalties" and that it highlighted her "sense of homelessness". He further referred to "the obvious bid to point to the dangers faced by the Jewish State and by the United States".<ref name="Jewish World Review">{{cite journal |first=Elliot B. |last=Gertel |url=http://www.jewishworldreview.com/elliot/gertel_ncis.php3?printer_friendly |title=One tough Jewess |work=[[Jewish World Review]] |date=July 16, 2009 |accessdate=March 23, 2013}}</ref> Anthony Frosh from ''Galus Australis'' said of the storyline, "Depending on your perspective, this is either the classic dilemma of the diasporite Jew, or the classic canard ''about'' the diasporite Jew."<ref name="galusaustralis">{{cite web|url=http://galusaustralis.com/2009/07/410/jews-in-pop-culture-a-critical-examination-part-ii/|title=Jews in Pop-culture: a Critical Examination Part II|work=Galus Australis|date=July 9, 2009|accessdate=March 23, 2013}}</ref>

Reviewers also speculated about the identity of Ziva's captor, which was not revealed until the premier of the seventh season. Mandi Bierly from ''[[Entertainment Weekly]]'' wrote, "So who do you think that is, torturing a nearly unrecognizable Ziva (well done, makeup department) and asking her to 'Tell me everything you know...about NCIS'? Is it a member of the terrorist cell in Africa she’s trying to bring down now that she’s back working with the Mossad? Is it a member of the Mossad turning on her? Or is it a member of some other group that I’m not smart enough to identify?"<ref name=ew>{{cite web|last=Bierly|first=Mandi|title='NCIS' season finale: Confused, but amused|url=http://popwatch.ew.com/2009/05/20/ncis-season-fin/|work=Entertainment Weekly|accessdate=March 23, 2013}}</ref>

== See also ==
* [[Ziva David captivity storyline]]

== References ==
{{Reflist|30em}}

{{NCIS television}}

[[Category:2009 television episodes]]
[[Category:NCIS (TV series) episodes]]