{{Refimprove|date=September 2007}}
{{Use dmy dates|date=October 2012}}
:''Not to be confused with [[Glenalta, South Australia]].''

{{Infobox Australian place | type = suburb
| name     = Glenunga
| city     = Adelaide
| state    = sa
| image    = 
| caption  = 
| lga      = City of Burnside
| postcode = 5064
| pop      = 1,869| pop_year = {{CensusAU|2006}}
| pop_footnotes = <ref>{{Census 2006 AUS|id=SSC41646|name=Glenunga (State Suburb)|quick=on|accessdate=7 June 2008}}</ref>
| est      = 1860
| area     = 
| stategov = [[electoral District of Unley|Unley]]
| fedgov   = [[Division of Sturt|Sturt]]
| near-nw  = [[Glenside, South Australia|Glenside]]
| near-n   = [[Glenside, South Australia|Glenside]]
| near-ne  = [[Linden Park, South Australia|Linden Park]]
| near-w   = [[Frewville, South Australia|Frewville]]
| near-e   = [[St. Georges, South Australia|St. Georges]]
| near-sw  = [[Fullarton, South Australia|Fullarton]]
| near-s   = [[Myrtle Bank, South Australia|Myrtle Bank]]
| near-se  = [[Glen Osmond, South Australia|Glen Osmond]]
| dist1    = 5
| dir1     = southeast
| location1= Adelaide
<!--| coordinates = {{Coord|-34.951|138.639|format=dms|type:city_region:AU-SA|display=inline,title}}-->
| latd=34.951 |longd=138.639
| alternative_location_map = Australia Greater Adelaide
}}
'''Glenunga''' is a small [[List of Adelaide suburbs|southern suburb]] of 2,539 people in the [[South Australia]]n city of [[Adelaide]]. It is located five kilometres southeast of the [[Adelaide city centre]]. The name Glenunga is taken from an [[Australian Aborigines|Aboriginal]] language "unga" meaning near and "glen" because of its proximity to Glen Osmand (see Manning's places of South Australia by Geoffrey H. Manning published in 1990). Bounded on the north by Windsor Road, the east by [[Portrush Road, Adelaide|Portrush Road]], the south-west by [[Glen Osmond Road, Adelaide|Glen Osmond Road]] and the west by Conyngham Street, the leafy suburb forms a rough triangular layout. It is close by to other Burnside council suburbs of [[Toorak Gardens, South Australia|Toorak Gardens]] and [[Glenside, South Australia|Glenside]].

==History==
Glenunga, along with its neighbouring suburb of [[Glenside, South Australia|Glenside]] were once known by the name of 'Knoxville'.  The first European settlers of the area (in the 1840s) took up farming, and wheat grown in the area was awarded first prize in the [[Royal Adelaide Show]]. The area now taken up by [[Glenunga International High School]] and Webb Oval, were previously home to [[slaughterhouse]]s established in the 19th century. At one point, the slaughterhouses were exporting overseas and at the same time providing half of Adelaide's lamb requirements.

A number of coach companies, notably those of [[William Rounsevell]], [[Cobb & Co]] and [[John Hill (businessman)|John Hill]] were set up in the 1870s and 1880s. Up to 1,000 horses grazed the land. At this point, most of the streets were beginning to be named. Most were named by the inhabitants at the time, usually in reference to their original homes in [[England]], [[Ireland]], [[Scotland]], [[Wales]] and the [[United States]]. However, one street was named after an [[Australian Aborigines|Aboriginal]] word - "Allinga", meaning sun.

In the early 20th century, a number of businesses started locating themselves in Glenunga. The South Australian [[icon]], the [[Hills Hoist]] - was invented by the Hill family in Glenunga. Other notable businesses were the Symons & Symons glass merchants on Windsor Road, and one involved in "Bland Radios".

A church was established in 1926, and a larger church was later built in 1956 and dedicated to [[St Stephen]]. The suburb's transition from a largely rural area to a residential suburb began after [[World War II]], with migration to the area from the [[United Kingdom]] and other countries. St. Stephen's Church was demolished in 1999, with the church community moving to the growing [[Jesus as Saviour|St. Saviour]]'s Church in [[Glen Osmond]]. A plaque was left as a reminder where it had once stood.

==Geography==
The suburb is very leafy with many trees which make up for the lack of parks and reserves. There are more parks in nearby [[Glen Osmond, South Australia|Glen Osmond]], such as the popular Ridge Park. Glenunga [[Uniting Church]] is located on the corner of L'Estrange Street and Bevington Road.

==Transport==
Public transport for the suburb is easy; buses run down [[Glen Osmond Road, Adelaide|Glen Osmond Road]] every 15 minutes until 6pm and there are numerous other bus routes into the city, and the cross-city circle line. The buses are provided by the [[Adelaide Metro]]. However, the majority of commuters still use motor cars and Glenunga is well placed for this, bordering [[Portrush Road]] and [[Glen Osmond Road, Adelaide|Glen Osmond Road]]. Glenunga residents can reach the Adelaide CBD by various routes, and the [[Adelaide Hills]] by way of Glen Osmond Road and the [[South Eastern Freeway]].

==Residents==
The suburb is home to a number of families and retirees of predominantly [[Anglo-Celtic Australian|Anglo-Celtic]] background.

==Attractions==
Due to the small size of the suburb itself, there is only one main park - Glenunga Reserve which also contains Webb Oval. More than anything else in the suburb, the [[Glenunga International High School]] brings it prominence both in Australia and overseas. The school campus is an easily identifiable part of Glenunga, being situated roughly in the middle of the suburb itself. The school's expansion and development have aided in the once-quiet L'Estrange Street being clogged with both traffic and students at peak hour and other times.{{Citation needed|date=October 2016}}

A number of businesses and shops are located along Glen Osmond Road and Conyngham Street. A [[McDonald's]], [[Chinese cuisine|Chinese]] and [[Mexican cuisine|Mexican]] restaurants are found within Glenunga on Glen Osmond Road, along with a number of small businesses and motels. Conyngham Street contains the headquarters of [[Department of Education and Children's Services|DECS]] and empty commercial blocks are waiting to undergo development.{{When|date=October 2016}}{{Citation needed|date=October 2016}}

==Politics==
Glenunga is in the [[South Australian House of Assembly]] [[Electoral District of Unley]] and the [[Australian House of Representatives|Federal]] [[Division of Sturt]], and is a solid Liberal-voting area.

==References==
{{Reflist}}

{{City of Burnside suburbs}}

[[Category:Suburbs of Adelaide]]