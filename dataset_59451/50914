{{distinguish-redirect|Citation metric|Citation index}}
'''Citation impact''' quantifies the [[citation]] usage of scholarly works.<ref>
{{cite journal
 |last=Garfield |first=E.
 |year=1955
 |url=http://www.garfield.library.upenn.edu/papers/science_v122v3159p108y1955.html
 |title=Citation Indexes for Science: A New Dimension in Documentation through Association of Ideas
 |journal=[[Science (journal)|Science]]
 |volume=122 |issue=3159 |pages=108
 |bibcode=1955Sci...122..108G
 |doi=10.1126/science.122.3159.108
}}</ref><ref>
{{cite journal
 |last=Garfield |first=E.
 |year=1973
 |url=http://www.garfield.library.upenn.edu/essays/V1p406y1962-73.pdf
 |title=Citation Frequency as a Measure of Research Activity and Performance
 |journal=Essays of an Information Scientist
 |volume=1 |pages=406–408
}}</ref><ref>
{{cite journal
 |last=Garfield |first=E.
 |year=1988
 |url=http://www.garfield.library.upenn.edu/essays/v11p354y1988.pdf
 |title=Can Researchers Bank on Citation Analysis?
 |journal=Essays of an Information Scientist
 |volume=11 |pages=354
}}</ref><ref>
{{cite web
 |last=Garfield |first=E.
 |year=1998
 |url=http://www.garfield.library.upenn.edu/papers/eval_of_science_CBE(Utah).html
 |title=The use of journal impact factors and citation analysis in the evaluation of science
 |work=41st Annual Meeting of the Council of Biology Editors
}}</ref><ref>
{{cite book
 |last1=Moed |first1=Henk F.
 |year=2005
 |title=Citation Analysis in Research Evaluation
 |publisher=[[Springer (publisher)|Springer]]
 |isbn=978-1-4020-3713-9
}}</ref> It is a result of [[citation analysis]] or [[bibliometrics]]. Among the measures that have emerged from citation analysis are the citation counts for an individual article, an author, and an academic journal.

==Article-level==
{{main article|Article-level metrics}}
One of the most basic citation metrics is how often an article was cited in other articles, books, or other sources (such as theses). Citation rates are heavily dependent on the discipline and the number of people working in that area. For instance, many more scientists work in neuroscience than in mathematics, and neuroscientists publish more papers than mathematicians, hence neuroscience papers are much more often cited than papers in mathematics.<ref>
{{cite book
 |last=de Solla Price |first=D. J.
 |year=1963
 |title=Little Science, Big Science
 |publisher=[[Columbia University Press]]
}}</ref><ref>
{{cite journal
 |last1=Larsen |first1=P. O.
 |last2=von Ins |first2=M.
 |year=2010
 |title=The rate of growth in scientific publication and the decline in coverage provided by Science Citation Index
 |journal=[[Scientometrics (journal)|Scientometrics]]
 |volume=84 |issue=3 |pages=575–603
 |doi=10.1007/s11192-010-0202-z
}}</ref> Similarly, [[review paper]]s are more often cited than regular research papers because they summarize results from many papers. This may also be the reason why papers with shorter titles get more citations, given that they are usually covering a broader area.<ref>
{{cite journal
 |last1=Deng |first1=B.
 |title=Papers with shorter titles get more citations 
 |url=http://www.nature.com/news/papers-with-shorter-titles-get-more-citations-1.18246
 |journal=[[Nature News]]
 |date=26 August 2015
 |doi=10.1038/nature.2015.18246
}}</ref>

=== Most-cited papers ===
The most-cited paper of all time is the classic paper by [[Oliver H. Lowry|Oliver Lowry]] describing an assay to measure the concentration of proteins.<ref>
{{Cite journal
 |last=Lowry |first=O. H.
 |last2=Rosebrough |first2=N. J.
 |last3=Farr |first3=A. L.
 |last4=Randall |first4=R. J.
 |year=1951
 |title=Protein measurement with the Folin phenol reagent
 |journal=[[The Journal of Biological Chemistry]]
 |volume=193 |issue=1 |pages=265–275
 |pmid=14907713
}}</ref> By 2014 it had accumulated more than 305,000 citations. The 10 most cited papers all had more than 40,000 citations.<ref name=":0">
{{Cite journal
 |last=van Noorden |first=R.
 |last2=Maher |first2=B.
 |last3=Nuzzo |first3=R.
 |date=2014
 |title=The top 100 papers
 |journal=[[Nature (journal)|Nature]]
 |volume=514 |issue=7524 |pages=550–553
 |bibcode=2014Natur.514..550V
 |doi=10.1038/514550a
 |pmid=25355343
}}</ref> To reach the top-100 papers required 12,119 citations by 2014.<ref name=":0" /> Of [[Thomson Reuters|Thomson Reuter’s]] [[Web of Science]] database with more than 58 million items only 14,499 papers (~0.026%) had more than 1,000 citations in 2014.<ref name=":0" />

==Journal-level==
{{main article|Journal-level metrics}}
[[File:Journal impact factor Nature Plos One.png|thumb|440x440px|Journal impact factors are influenced heavily by a small number of highly cited papers. In general, most papers published in 2013–14 received many fewer citations than indicated by the impact factor. Two journals (Nature [blue], Plos One [orange]) are shown to represent a highly cited and less cited journal, respectively. Note that the high citation impact of Nature is derived from relatively few highly cited papers. Modified after Callaway 2016.<ref name = "Callaway2016">
{{Cite journal
 |last=Callaway |first=E.
 |date=2016
 |title=Beat it, impact factor! Publishing elite turns against controversial metric
 |url=http://www.nature.com/doifinder/10.1038/nature.2016.20224
 |journal=[[Nature (journal)|Nature]]
 |volume=535 |issue=7611 |pages=210–211
 |bibcode=2016Natur.535..210C
 |doi=10.1038/nature.2016.20224
 |pmid=27411614
}}</ref>]]
Journal impact factors (JIFs) are a measure of the average number of citations that articles published by a journal in the previous two years have received in the current year. However, journals with very high impact factors are often based on a small number of very highly cited papers. For instance, most papers in [[Nature (journal)|Nature]] (impact factor 38.1, 2016) were "only" cited 10 or 20 times during the reference year (see figure). Journals with a "low" impact (e.g. [[PLOS ONE]], impact factor 3.1) publish many papers that are cited 0 to 5 times but few highly cited articles.<ref name = "Callaway2016"/>

JIFs are often mis-interpreted as a measure for journal quality or even article quality. The JIF is a journal-level metric, not an article-level metric, hence its use to determine the impact of a single article is statistically invalid. Citation distribution is skewed for journals because a very small number of articles is driving the vast majority of citations (see figure). Therefore, some journals have stopped publicizing their impact factor, e.g. the journals of the [[American Society for Microbiology]].<ref>
{{Cite journal
 |last=Casadevall |first=A.
 |last2=Bertuzzi |first2=S.
 |last3=Buchmeier |first3=M. J.
 |last4=Davis |first4=R. J.
 |last5=Drake |first5=H.
 |last6=Fang |first6=F. C.
 |last7=Gilbert |first7=J.
 |last8=Goldman |first8=B. M.
 |last9=Imperiale |first9=M. J.
 |date=2016
 |title=ASM Journals Eliminate Impact Factor Information from Journal Websites
 |url=http://msphere.asm.org/content/1/4/e00184-16
 |journal=[[mSphere]]
 |volume=1 |issue=4 |pages=e00184–16
 |doi=10.1128/mSphere.00184-16
 |pmc=4941020
 |pmid=27408939
}}</ref>

=== Impact factor and manuscript rejection rates ===
It is commonly believed that manuscripts are more often rejected at high impact journals. However, in a random selection of 570 journals there was no such correlation.<ref>Pascal Rocha da Silva (2015) [http://blog.frontiersin.org/2015/12/21/4782/ Selecting for impact: new data debunks old beliefs], Frontiers Blog, 21 Dec 2015</ref> However, specific disciplines may have weak correlations, e.g. in the physical sciences there is even a negative correlation.<ref>Pascal Rocha da Silva (2016) [http://blog.frontiersin.org/2016/03/04/initial-findings-confirmed-no-significant-link-between-rejection-rate-and-journal-impact/ New Data Debunks Old Beliefs: Part 2], Frontiers Blog, 4 March 2016</ref>

==Author-level==
{{main article|Author-level metrics}}
Total citations, or average citation count per article, can be reported for an individual author or researcher. Many other measures have been proposed, beyond simple citation counts, to better quantify an individual scholar's citation impact.<ref>
{{cite journal 
 |last1=Belikov |first1=A. V.
 |last2=Belikov |first2=V. V.
 |year=2015
 |title=A citation-based, author- and age-normalized, logarithmic index for evaluation of individual researchers independently of publication counts
 |journal=[[F1000Research]]
 |volume=4 |pages=884
 |doi=10.12688/f1000research.7070.1
}}</ref> The best-known measures include the [[h-index]]<ref name=Hirsch2005>
{{cite journal
 |last=Hirsch |first=J. E.
 |date=2005
 |title=An index to quantify an individual's scientific research output
 |journal=[[Proceedings of the National Academy of Sciences|PNAS]]
 |volume=102 |issue=46 |pages=16569&ndash;16572
 |arxiv=physics/0508025
 |bibcode=2005PNAS..10216569H
 |doi=10.1073/pnas.0507655102
 |pmc=1283832
 |pmid=16275915
}}</ref> and the [[g-index]].<ref name=Egghe2006>
{{cite journal
 |last=Egghe |first=L.
 |year=2006
 |title=Theory and practise of the g-index
 |journal=[[Scientometrics (journal)|Scientometrics]]
 |volume=69 |issue=1 |pages=131–152
 |doi=10.1007/s11192-006-0144-7
}}</ref> Each measure has advantages and disadvantages,<ref name="10.1007/s11192-017-2330-1">{{cite journal |author=Gálvez RH |title=Assessing author self-citation as a mechanism of relevant knowledge diffusion |journal=Scientometrics |date=March 2017 |doi=10.1007/s11192-017-2330-1}}</ref> spanning from bias to discipline-dependence and limitations of the citation data source.<ref>
{{Cite journal
 |last=Couto |first=F. M.
 |last2=Pesquita |first2=C.
 |last3=Grego |first3=T.
 |last4=Veríssimo |first4=P.
 |year=2009
 |title=Handling self-citations using Google Scholar 
 |url=http://www.cindoc.csic.es/cybermetrics/articles/v13i1p2.html
 |journal=[[Cybermetrics]]
 |volume=13 |issue=1 |page=2
}}</ref>

==Alternatives==
{{main article|Altmetrics}}
An [[altmetrics|alternative approach]] to measure a scholar's impact relies on usage data, such as number of downloads from publishers and analyzing citation performance, often at [[article-level metrics|article level]].<ref name=Bollen2005>
{{cite journal
 |last=Bollen |first=J.
 |last2=Van de Sompel |first2=H.
 |last3=Smith |first3=J.
 |last4=Luce |first4=R.
 |year=2005
 |title=Toward alternative metrics of journal impact: A comparison of download and citation data
 |journal=[[Information Processing and Management]]
 |volume=41 |issue=6 |pages=1419–1440
 |arxiv=cs.DL/0503007
 |doi=10.1016/j.ipm.2005.03.024
}}</ref><ref>
{{cite journal
 |last=Brody |first=T.
 |last2=Harnad |first2=S.
 |last3=Carr |first3=L.
 |year=2005
 |title=Earlier Web Usage Statistics as Predictors of Later Citation Impact
 |journal=[[Journal of the Association for Information Science and Technology]]
 |volume= 57|issue= 8|pages=1060
 |arxiv=cs/0503020
 |bibcode=2005cs........3020B
 |doi=10.1002/asi.20373
}}</ref><ref>
{{cite journal
 |last1=Kurtz |first1=M. J.
 |last2=Eichhorn |first2=G.
 |last3=Accomazzi |first3=A.
 |last4=Grant |first4=C.
 |last5=Demleitner |first5=M.
 |last6=Murray |first6=S. S.
 |year=2004
 |title=The Effect of Use and Access on Citations
 |journal=[[Information Processing and Management]]
 |volume=41 |issue=6 |pages=1395–1402
 |arxiv=cs/0503029
 |bibcode=2005IPM....41.1395K
 |doi=10.1016/j.ipm.2005.03.010
}}</ref><ref>
{{cite journal
 |last1=Moed |first1=H. F.
 |year=2005b
 |title=Statistical Relationships Between Downloads and Citations at the Level of Individual Documents Within a Single Journal
 |journal=[[Journal of the American Society for Information Science and Technology]]
 |volume=56 |issue=10 |pages=1088–1097
 |doi=10.1002/asi.20200
}}</ref>

As early as 2004, the ''[[BMJ]]'' published the number of views for its articles, which was found to be somewhat correlated to citations.<ref>
{{cite journal
 |last1=Perneger |first1=T. V.
 |year=2004
 |title=Relation between online "hit counts" and subsequent citations: Prospective study of research papers in the BMJ
 |journal=[[BMJ]]
 |volume=329 |issue=7465 |pages=546–7
 |doi=10.1136/bmj.329.7465.546
 |pmid=15345629
 |pmc=516105
}}</ref> In 2008 the ''[[Journal of Medical Internet Research]]'' began publishing views and [[Twitter|Tweet]]s. These "tweetations" proved to be a good indicator of highly cited articles, leading the author to propose a "Twimpact factor", which is the number of Tweets it receives in the first seven days of publication, as well as a Twindex, which is the rank percentile of an article's Twimpact factor.<ref>
{{cite journal
 |last1=Eysenbach |first1=G.
 |year=2011
 |title=Can Tweets Predict Citations? Metrics of Social Impact Based on Twitter and Correlation with Traditional Metrics of Scientific Impact
 |journal=[[Journal of Medical Internet Research]]
 |volume=13 |issue=4 |pages=e123
 |doi=10.2196/jmir.2012
 |pmid=22173204
 |pmc=3278109
}}</ref>

==Open Access publications==
{{main|Open Access}}
Open access publications are available without cost to readers, hence they should be cited more frequently.<ref>[http://opcit.eprints.org/oacitation-biblio.html Bibliography of Findings on the Open Access Impact Advantage]</ref><ref>{{cite journal
 |last1=Brody |first1=T.
 |last2=Harnad |first2=S.
 |year=2004
 |title=Comparing the Impact of Open Access (OA) vs. Non-OA Articles in the Same Journals
 |url=http://eprints.ecs.soton.ac.uk/10207/
 |journal=[[D-Lib Magazine]]
 |volume=10 |issue= |page=6
}}</ref><ref>{{cite journal
 |last1=Eysenbach |first1=G.
 |last2=Tenopir |first2=C.
 |year=2006
 |title=Citation Advantage of Open Access Articles
 |journal=[[PLoS Biology]]
 |volume=4 |issue=5 |pages=e157
 |doi=10.1371/journal.pbio.0040157
}}</ref><ref>{{cite journal
 |last1=Eysenbach |first1=G.
 |date=2006
 |title=The Open Access Advantage
 |journal=[[Journal of Medical Internet Research]]
 |volume=8 |issue=2 |pages=e8
 |doi=10.2196/jmir.8.2.e8
}}</ref><ref>{{cite journal
 |last=Hajjem |first=C.
 |last2=Harnad |first2=S.
 |last3=Gingras |first3=Y.
 |year=2005
 |title=Ten-Year Cross-Disciplinary Comparison of the Growth of Open Access and How It Increases Research Citation Impact
 |url=http://sites.computer.org/debull/A05dec/A05DEC-CD.pdf
 |journal=[[IEEE Data Engineering Bulletin]]
 |volume=28 |issue=4 |pages=39–47
 |arxiv=cs/0606079
 |bibcode=2006cs........6079H
}}</ref><ref>{{cite journal
 |last1=Lawrence |first1=S.
 |year=2001
 |title=Free online availability substantially increases a paper's impact
 |journal=[[Nature (journal)|Nature]]
 |volume=411 |issue=6837 |pages=521–521
 |doi=10.1038/35079151
}}</ref><ref>{{cite journal
 |last1=MacCallum |first1=C. J.
 |last2=Parthasarathy |first2=H.
 |year=2006
 |title=Open Access Increases Citation Rate
 |journal=[[PLoS Biology]]
 |volume=4 |issue=5 |pages=e176
 |doi=10.1371/journal.pbio.0040176
}}</ref><ref>{{cite journal
 |last1=Gargouri |first1=Y.
 |last2=Hajjem |first2=C.
 |last3=Lariviere |first3=V.
 |last4=Gingras |first4=Y.
 |last5=Brody |first5=T.
 |last6=Carr |first6=L.
 |last7=Harnad |first7=S.
 |year=2010
 |title=Self-Selected or Mandated, Open Access Increases Citation Impact for Higher Quality Research
 |url=http://www.plosone.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0013636
 |journal=[[Plos One]]
 |volume=5 |issue=10 |page=e13636
 |bibcode=2010PLoSO...513636G
 |doi=10.1371/journal.pone.0013636
}}</ref> While this has been contradicted by some experimental and observational studies<ref>{{cite journal
 |last1=Davis |first1=P. M.
 |last2=Lewenstein |first2=B. V.
 |last3=Simon |first3=D. H.
 |last4=Booth |first4=J. G.
 |last5=Connolly |first5=M. J. L.
 |year=2008
 |title=Open access publishing, article downloads, and citations: randomised controlled trial
 |journal=[[BMJ]]
 |volume=337 |issue= |pages=a568–a568
 |doi=10.1136/bmj.a568
}}</ref><ref>{{cite journal
 |last1=Davis |first1=P. M.
 |year=2011
 |title=Open access, readership, citations: a randomized controlled trial of scientific journal publishing
 |journal=[[The FASEB Journal]]
 |volume=25 |issue=7 |pages=2129–2134
 |doi=10.1096/fj.11-183988
}}</ref> recent evidence suggests that &nbsp;OA journals were indeed found to have significantly more citations overall compared to non-OA journals (median 15.5 vs 12). Thus, it is better to publish in an OA journal for more citations.<ref>{{Cite journal|last=Chua|first=SK|last2=Qureshi|first2=Ahmad M|last3=Krishnan|first3=Vijay|last4=Pai|first4=Dinker R|last5=Kamal|first5=Laila B|last6=Gunasegaran|first6=Sharmilla|last7=Afzal|first7=MZ|last8=Ambawatta|first8=Lahiru|last9=Gan|first9=JY|date=2017-03-02|title=The impact factor of an open access journal does not contribute to an article’s citations|url=https://f1000research.com/articles/6-208/v1|journal=F1000Research|volume=6|doi=10.12688/f1000research.10892.1}}</ref>

==Recent developments==
{{further information|Citation analysis}}

An important recent development in research on citation impact is the discovery of ''universality'', or citation impact patterns that hold across different disciplines in the sciences, social sciences, and humanities. For example, it has been shown that the number of citations received by a publication, once properly rescaled by its average across articles published in the same discipline and in the same year, follows a universal [[log-normal distribution]] that is the same in every discipline.<ref name=Radicchi2008>
{{cite journal
 |last=Radicchi |first=F.
 |last2=Fortunato |first2=S.
 |last3=Castellano |first3=C.
 |date=2008
 |title=Universality of citation distributions: Toward an objective measure of scientific impact
 |journal=[[Proceedings of the National Academy of Sciences|PNAS]]
 |volume=105 |issue=45 |pages=17268–17272
 |arxiv=0806.0974
 |bibcode=2008PNAS..10517268R
 |doi=10.1073/pnas.0806977105
 |pmc=2582263
 |pmid=18978030
}}</ref> This finding has suggested a ''universal citation impact measure'' that extends the h-index by properly rescaling citation counts and resorting publications, however the computation of such a universal measure requires the collection of extensive citation data and statistics for every discipline and year. Social [[crowdsourcing]] tools such as Scholarometer have been proposed to address this need.<ref name=Hoang2010>
{{cite conference
 |last=Hoang |first=D.
 |last2=Kaur |first2=J.
 |last3=Menczer |first3=F.
 |year=2010
 |title=Crowdsourcing Scholarly Data
 |booktitle=Proceedings of the WebSci10: Extending the Frontiers of Society On-Line
 |url=http://journal.webscience.org/321/2/websci10_submission_107.pdf
}}</ref>

Research suggests the impact of an article can be, partly, explained by superficial factors and not only by the scientific merits of an article.<ref>
{{cite journal
 |last1=Bornmann |first1=L.
 |last2=Daniel |first2=H. D.
 |year=2008
 |title=What do citation counts measure? A review of studies on citing behavior
 |journal=[[Journal of Documentation]]
 |volume=64 |issue=1 |pages=45–80
 |doi=10.1108/00220410810844150
}}</ref> Field-dependent factors are usually listed as an issue to be tackled not only when comparison across disciplines are made, but also when different fields of research of one discipline are being compared.<ref>
{{cite journal
 |last=Anauati |first=M. V.
 |last2=Galiani |first2=S.
 |last3=Gálvez |first3=R. H.
 |year=2014
 |title=Quantifying the Life Cycle of Scholarly Articles Across Fields of Economic Research
 |ssrn=2523078
}}</ref> For instance in Medicine among other factors the number of authors, the number of references, the article length, and the presence of a colon in the title influence the impact. Whilst in Sociology the number of references, the article length, and title length are among the factors.<ref>{{cite journal
 |last=van Wesel |first=M.
 |last2=Wyatt |first2=S.
 |last3=ten Haaf |first3=J.
 |year=2014
 |title=What a difference a colon makes: how superficial factors influence subsequent citation
 |journal=[[Scientometrics]]
 |volume=98 |issue=3 |pages=1601–1615
 |doi=10.1007/s11192-013-1154-x
}}</ref> Also it is suggested scholars engage in ethical questionable behavior in order to inflate the number of citations articles receive.<ref>
{{cite journal
 |last=van Wesel |first=M.
 |year=2016
 |title=Evaluation by Citation: Trends in Publication Behavior, Evaluation Criteria, and the Strive for High Impact Publications
 |journal=[[Science and Engineering Ethics]]
 |volume=22 |issue=1 |pages=199–225
 |doi=10.1007/s11948-015-9638-0
}}</ref>

Automated [[citation index]]ing<ref>
{{cite conference
 |last=Giles |first=C. L.
 |last2=Bollacker |first2=K.
 |last3=Lawrence |first3=S.
 |year=1998
 |title=CiteSeer: An Automatic Citation Indexing System
 |booktitle=DL'98 Digital Libraries, 3rd ACM Conference on Digital Libraries
 |pages=89–98
 |doi=10.1145/276675.276685
}}</ref> has changed the nature of citation analysis research, allowing millions of citations to be analyzed for large scale patterns and knowledge discovery. The first example of automated citation indexing was [[CiteSeer]], later to be followed by [[Google Scholar]]. More recently, advanced models for a dynamic analysis of citation aging have been proposed.<ref>
{{cite journal
 |last=Yu |first=G.
 |last2=Li |first2=Y.-J.
 |year=2010
 |title=Identification of referencing and citation processes of scientific journals based on the citation distribution model
 |journal=[[Scientometrics]]
 |volume=82 |issue=2 |pages=249–261
 |doi=10.1007/s11192-009-0085-z
}}</ref><ref>
{{cite journal
 |last=Bouabid |first=H.
 |year=2011
 |title=Revisiting citation aging: A model for citation distribution and life-cycle prediction
 |journal=[[Scientometrics]]
 |volume=88 |issue=1 |pages=199–211
 |doi=10.1007/s11192-011-0370-5
}}</ref> The latter model is even used as a predictive tool for determining the citations that might be obtained at any time of the lifetime of a corpus of publications.

According to Mario Biagioli: "All metrics of scientific evaluation are bound to be abused. [[Goodhart's law]] [...] states that when a feature of the economy is picked as an indicator of the economy, then it inexorably ceases to function as that indicator because people start to game it."<ref>{{cite journal
 |last1=Biagioli |first1=M.
 |title=Watch out for cheats in citation game
 |journal=[[Nature (journal)|Nature]]
 |year=2016
 |volume=535 |issue=7611 |pages=201–201
 |bibcode=2016Natur.535..201B
 |doi=10.1038/535201a
}}</ref>

== See also ==
*[[H-index]], also applied to journals
*[[Impact factor]], the average citation count for a journal
*[[Eigenfactor]]
*[[Altmetrics]]
*[[SCImago Journal Rank]]

== References ==
{{reflist|30em}}

== Further reading ==
*{{cite journal | last1 = Chanson | first1 = Hubert | authorlink = Hubert Chanson | year = 2007 | title = Research Quality, Publications and Impact in Civil Engineering into the 21st Century. Publish or Perish, Commercial versus Open Access, Internet versus Libraries ? | url = http://espace.library.uq.edu.au/view/UQ:159099 | journal = Canadian Journal of Civil Engineering | volume = 34 | issue = 8| pages = 946–951 | doi = 10.1169/L07-027| doi-broken-date = 2017-02-20 }}
* {{Cite journal |author1=Panaretos, J. |author2=Malesios, C. |title=Assessing Scientific Research Performance and Impact with Single Indices |journal=Scientometrics |volume=81 |issue=3 |pages=635–670 |year=2009 |doi=10.1007/s11192-008-2174-9}}

[[Category:Research and development]]
[[Category:Academic publishing]]
[[Category:Citation metrics| ]]