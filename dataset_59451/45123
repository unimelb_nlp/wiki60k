{{about|the Beatles song|the memoir by Mishna Wolff|I'm Down (book)}}
{{Use British English|date=July 2011}}
{{Use dmy dates|date=July 2011}}
{{Infobox single
| Name      = I'm Down
| Cover     = Beatles_help2.jpg
| Caption   = US picture sleeve
| Artist     = [[The Beatles]]
| A-side     = "[[Help! (song)|Help!]]"
| Released    = 19 July 1965 (US)<br>23 July 1965 (UK)
| Format     = [[gramophone record|7"]]
| Recorded    = 14 June 1965,<br />[[Abbey Road Studios|EMI Studios]], London
| Genre     = [[Rock and roll]]
| Length     = 2:33
| Label     = [[Capitol Records]]
| Writer     = [[Lennon–McCartney]]
| Producer    = [[George Martin]]
| Chronology    = [[The Beatles]] UK singles
| Last single   = "[[Ticket to Ride]]"<br>(1965)
| This single   = "[[Help! (song)|Help!]]" /<br>"'''I'm Down'''"<br>(1965)
| Next single   = "[[We Can Work It Out]]" /<br/>"[[Day Tripper]]"<br>(1965)
| Misc          = {{Extra chronology
  | Artist         = [[The Beatles]] US singles
  | Type           = single
  | Last single   = "[[Ticket to Ride]]"<br>(1965)
  | This single   = "[[Help! (song)|Help!]]" /<br>"'''I'm Down'''"<br>(1965)
  | Next single    = "[[Yesterday (Beatles song)|Yesterday]]"<br>(1965)
}}
}}
"'''I'm Down'''" is a song by [[the Beatles]] written by [[Paul McCartney]] (credited to [[Lennon–McCartney]]) and first released as the [[A-side and B-side|B-side]] to the single "[[Help! (song)|Help!]]" in 1965.

==Composition==
According to critic [[Richie Unterberger]] of [[Allmusic]], "I'm Down" is "one of the most frantic rockers in the entire Beatles' catalog."{{sfn|Unterberger|2007}} McCartney told writer [[Barry Miles]] that the song and his vocal style on it were influenced by [[Little Richard]], "I used to sing his stuff but there came a point when I wanted one of my own, so I wrote 'I'm Down.'" <ref>Barry Miles, Paul McCartney: Many Years From Now, 1998</ref>

==Recording==
The Beatles recorded "I'm Down" on 14 June 1965 in the same session as "[[Yesterday (Beatles song)|Yesterday]]" and "[[I've Just Seen a Face]]".{{sfn|Lewisohn|1988|p=59}}

The Beatles recorded the backing track in seven takes. The first of these takes can be heard on ''[[Anthology 2]]'', with a quiet organ track and no backing vocals. At the beginning of the Anthology version, McCartney says, "Let's hope this one turns out pretty darn good" in a faux American accent. During the session, particularly between takes one and two, McCartney can be heard repeating the phrase "[[Plastic soul]], man, plastic soul". He later revealed that the phrase, which the Beatles later adapted for the title of their album ''[[Rubber Soul]]'', was used by black musicians to describe [[Mick Jagger]].{{sfn|The Beatles Bible|2008}}

==Release==
The official release date for the "Help"/"I'm Down" single was 19 July 1965 on [[Capitol Records]] in the United States and 23 July on [[Parlophone]] in the United Kingdom. "I'm Down" was never released on an official Beatle studio album, and was only available in the US in [[monaural|mono]] as the B-side of the "Help!" single until the summer of 1976. That year, it appeared in stereo on ''[[Rock 'n' Roll Music (album)|Rock 'n' Roll Music]]'', a compilation [[LP album|LP]] released in the US by Capitol featuring up tempo Beatles' tracks. The first [[compact disc|CD]] release was in 1988 on the compilation ''[[Past Masters|Past Masters, Volume One]]'', where it appeared in true stereo.{{sfn|Lewisohn|1988|pp=59–60, 62, 200–201}}

There is also an alternate version of the song ([[take]] 1) on ''Anthology 2''. The tempo is slower and there are no backing vocals.{{sfn|Apple Records|1996}}

==Live performances==
The song was performed at their fourth appearance on ''[[The Ed Sullivan Show]]''.

The Beatles used "I'm Down" to close concerts in their final year as a live act, replacing "[[Long Tall Sally]]" for most of those shows.{{sfn|Cross|2005|p=378}}

During their performance at [[Shea Stadium]] in August 1965 (the largest audience the Beatles ever drew during their career as a live touring band), the band played a memorably frenzied version of the song, with [[John Lennon]] playing a [[Vox Continental]] [[combo organ]] with his elbows at times. Lennon's antics caused both Lennon and [[George Harrison]] to laugh during the performance as they sing backing vocals from the same microphone.  Footage of this performance may be seen on ''[[The Beatles Anthology]]'' video.  [[Paul McCartney]] won praise for his soulful singing when they performed it at the [[Hollywood Bowl]].<ref name=pc28>{{Gilliland |url=http://digital.library.unt.edu/ark:/67531/metadc19783/m1/ |title=Show 28 - The British Are Coming! The British Are Coming!: The U.S.A. is invaded by a wave of long-haired English rockers. [Part 2] |show=28}} Track 2.</ref>

The band also played this song during their 12 September 1965 appearance on ''[[The Ed Sullivan Show]]'', which was recorded 14 August 1965, the day before the Shea concert.  Lennon played the keyboard with his elbow for this performance as well.  However, Lennon played guitar, rather than organ, for a version recorded in [[Tokyo]] on their 1966 tour, even though a Vox organ was set up on stage.

The Beatles also played "I'm Down" to close their brief live concert at the Circus Krone-Bau in Munich, West-Germany, on June 24, 1966.  A recording of the show was aired on German television some time later and can be accessed through several sites.  In this show, just prior to playing "I'm Down", an interlude occurred providing a glimpse of the relationships between the band members: 

<blockquote> 
After the Beatles finished the next-to-last song on the playlist ("[[Nowhere Man (song)|Nowhere Man]]") Paul McCartney announces in halting German that they will now play their final song and he thanks the audience.  But rather than starting to play, McCartney and George Harrison exchange a few words, and it appears McCartney is saying that he forgot the first line ("You telling lies thinking I can't see") of the song.  Then, John Lennon turns to McCartney and laughingly recites a line in the same rhythm.  Some commentators believe that Lennon was helping McCartney to remember, but in fact, he was playing a little joke on his bandmate, scanning "You feel down and you're not gonna sing".  McCartney then starts the song with the second verse ("Man buys ring woman throws it away").  After the chorus, he tries his hand one more time on the first verse but never gets it right.  The whole thing seems to take place in a spirit of camaraderie and fun. 
</blockquote>

McCartney played the song to open his set at [[The Concert for New York City]] following the [[11 September 2001 terrorist attacks]].  The concert was held at [[Madison Square Garden]] in support of firefighters, policemen, and other public workers who suffered from the aftermath of the attacks.  He also reintroduced "I'm Down" into his set list for his three concerts at [[Citi Field]] (which replaced Shea Stadium) in July 2009.

==Personnel==
* [[Paul McCartney]] – [[Singing|lead vocal]], [[bass guitar|bass]]
* [[John Lennon]] – [[Backing vocalist|backing vocal]], [[rhythm guitar]], [[Vox Continental| Vox Continental organ]]
* [[George Harrison]] – backing vocal, [[lead guitar]]
* [[Ringo Starr]] – [[Drum kit|drums]], [[Bongo drum|bongos]]
:Personnel per [[Ian MacDonald]].{{sfn|MacDonald|2005|p=156}}

==Cover versions==
* [[Heart (band)|Heart]] covered this song as a medley with "[[Long Tall Sally]]" on their 1980 album ''[[Greatest Hits Live (Heart album)|Greatest Hits Live]]''. It was on the live side of the album.
* In 1983, [[Adrian Belew]] released his second solo record, ''[[Twang Bar King]]'', which began with a version of this song.
* In 1982, [[Jay Ferguson (American musician)|Jay Ferguson]] recorded it for his album ''White Noise''.
* [[Aerosmith]] recorded a cover version of this song for the band's 1987 album ''[[Permanent Vacation (album)|Permanent Vacation]]''.
* [[Deacon Blue]] released a live cover version of this song as a B-side on their 1991 single, "[[Twist and Shout (Deacon Blue song)|Twist and Shout]]" (which is a different song than the Beatles' recording of the same name).
* [[The Kentucky Headhunters]] included a cover version on their 2006 album ''[[Big Boss Man (album)|Big Boss Man]]''.
* A live cover of this song recorded in 1976 appears on the ''[[Yesyears|YesYears]]'' box set by [[Yes (band)|Yes]]. The band also did the song live during [[List of Yes concert tours (1980s–90s)#9012Live Tour|the 9012Live tour]], with a performance in Dortmund, Germany featuring [[Jimmy Page]] playing with them on the song.
* [[New Grass Revival]] included a cover of this song on their 1989 album ''[[Friday Night in America]]''.
* [[The Punkles]] recorded a punk version of this song for their second album, ''Punk!''.
* The [[Beastie Boys]] recorded a version of this song to be featured on their debut album, ''[[Licensed to Ill]]'',  The lyrics, tempo and melody were substantially different but on the chorus portion "How can you laugh" used a sample of the original Beatles recording without securing permission to do so .The song was deleted from the final track list at the last minute due to licensing restraints but can be found on some Beastie Boys bootlegs.{{Citation needed|date=February 2010}}
* [[The Mummies]] recorded a cover on their ''(You Must Fight To Live) On The Planet Of The Apes'' EP.
* [[The Reatards]] released a version on their cassette-only album ''[[Teenage Hate|Fuck Elvis Here's The Reatards Cassette]]''.
* The Bawdies recorded a cover version.
* [[The Warriors (British band)|The Warriors]] used to perform the song live along with "[[She's a Woman]]".  Both songs can be found on ''Bolton Club 65'' released in 2003.

==Notes==
{{reflist}}

==References==
{{Refbegin|2}}
* {{cite book
| last=Cross
| first=Craig
| year=2005
| title=The Beatles: Day-by-Day, Song-by-Song, Record-by-Record
| publisher=iUniverse, Inc.
| location=Lincoln, NE
| isbn=978-0-595-34663-9
| ref=harv
}}
* {{cite web
| work=The Beatles Bible
| year=2008
| title=I'm Down
| url=http://www.beatlesbible.com/songs/im-down/
| accessdate=25 November 2008
| ref={{SfnRef|The Beatles Bible|2008}}
}}
* {{cite book
| last=Lewisohn
| first=Mark
| year=1988
| authorlink=Mark Lewisohn
| title=The Beatles Recording Sessions
| publisher=Harmony Books
| location=New York
| isbn=978-0-517-57066-1
| ref=harv
}}
* {{cite AV media notes
| last=Lewisohn
| first=Mark
| year=1996
| title=Anthology 2
| location=London
| titlelink=Anthology 2
| others=[[The Beatles]]
| type=booklet
| publisher=[[Apple Records]]
| id=34448
| ref=harv
}}
* {{cite book
| last=MacDonald
| first=Ian
| year=2005
| authorlink=Ian MacDonald
| title=Revolution in the Head: The Beatles' Records and the Sixties
| publisher=Pimlico (Rand)
| location=London
| edition=Second Revised
| isbn=978-1-84413-828-9
| ref=harv
}}
* {{cite web
| work=[[Amazon.com]]
| year=2007
| title=Past Masters, Vol. 1
| url=http://www.amazon.com/Past-Masters-Vol-1-Beatles/dp/B000002USY/ref=pd_bbs_sr_1/103-3232693-6612636?qid=1173040139&sr=8-1
| accessdate=4 March 2007
| ref={{SfnRef|Amazon.com|2007}}
}}
* {{cite web
| last=Unterberger
| first=Richie
| year=2007
| authorlink=Richie Unterberger
| title=Review of "I'm Down"
| work=[[Allmusic]]
| url={{Allmusic|class=song|id=t994020|pure_url=yes}}
| accessdate=4 March 2007
| ref=harv
}}
{{Refend}}

== External links ==
* {{Notes on|http://www.icce.rug.nl/~soundscapes/DATABASES/AWP/id.shtml}}

{{The Beatles singles}}

[[Category:1965 songs]]
[[Category:Aerosmith songs]]
[[Category:Yes (band) songs]]
[[Category:Songs written by Lennon–McCartney]]
[[Category:Song recordings produced by George Martin]]
[[Category:Capitol Records singles]]
[[Category:Songs published by Northern Songs]]
[[Category:The Beatles songs]]
[[Category:Parlophone singles]]