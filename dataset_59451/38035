{{good article}}
{{EngvarB|date=July 2014}}
{{Use dmy dates|date=July 2014}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name= B-48 Firecrest
 |image= File:Blackburn YA1 1st.png
 |caption=
}}{{Infobox Aircraft Type
 |type= [[Strike fighter]]
 |manufacturer= [[Blackburn Aircraft]]
 |designer= 
 |first flight= 1 April 1947
 |introduced=
 |retired=
 |status=
 |primary user=[[Fleet Air Arm]]
 |produced=1947
 |number built=3
 |unit cost=
 |developed from= [[Blackburn Firebrand]]
 |variants with their own articles=
}}
|}

The '''Blackburn B-48 Firecrest''', given the [[Society of British Aerospace Companies|SBAC]] designation YA.1, was a single-engine naval [[strike fighter]] built by [[Blackburn Aircraft]] for service with the British [[Fleet Air Arm]] during World War II. It was a development of the troubled [[Blackburn Firebrand|Firebrand]], designed to [[List of Air Ministry Specifications|Air Ministry Specification]] S.28/43, for an improved aircraft more suited to [[aircraft carrier|carrier operations]]. Three prototypes were ordered with the company designation of B-48 and the informal name of "Firecrest", but only two of them actually flew. The development of the aircraft was prolonged by significant design changes and slow deliveries of components, but the determination by the [[Ministry of Supply]] in 1946 that the [[airframe]] did not meet the requirements for a strike fighter doomed the aircraft. Construction of two of the prototypes was continued to gain flight-test data and the third was allocated to strength testing. The two flying aircraft were sold back to Blackburn in 1950 for disposal and the other aircraft survived until 1952.

==Design==
The Firebrand required significant effort by Blackburn to produce a useful aircraft and the first discussions on a redesign of the aircraft with a [[laminar flow|laminar-flow]] wing took place in September 1943. The new wing was estimated to reduce the weight of the wing by 700&nbsp;lb (318&nbsp;kg) and increase the aircraft speed by 13&nbsp;mph (21&nbsp;km/h). The extent of redesign increased and this led to a new fuselage and other improvements.<ref>Buttler 2004, pp. 181–182.</ref> In October 1943, Blackburn's design staff, led by G.E. Petty, started work on this development of the Firebrand which led to [[List of Air Ministry specifications#1940-1949|Specification S.28/43]] being issued by the [[Air Ministry]] on 26 February 1944 covering the new aircraft.<ref name="Jackson p452">Jackson 1968, p. 452.</ref><ref name="Mason Fighter p330-1">Mason 1992, pp. 330–331.</ref> The specification was designed around a [[Bristol Centaurus]] 77 [[radial engine]] with [[contra-rotating propellers]] that allowed the size of the rudder to be reduced.<ref>Buttler 1999, p. 55</ref>

The new design, given the company designation B-48, was known unofficially by Blackburn as the "Firecrest" but was always known by its specification number by the Air Ministry and Navy. It was a low-winged, single-seat, all-metal [[monoplane]]. Aft of the [[cockpit]] the fuselage was an oval-shaped stressed-skin [[semi-monocoque]], but forward it had a circular-section, tubular-steel frame. The cockpit of the Firecrest was moved forward and raised the pilot's position so that he now looked over the wing leading edge, and down the nose.<ref name=b81/> The [[Aircraft canopy|canopy]] was adapted from the [[Hawker Tempest]] fighter.<ref name=b81/> In the rear fuselage was a single {{convert|52|impgal|l USgal|adj=on|lk=in}} fuel tank with two {{convert|92|impgal|l USgal|adj=on}} fuel tanks in the centre wing section. The aircraft had a redesigned, thinner, inverted [[gull wing]] of laminar flow [[Airfoil|aerofoil section]]. The wing consisted of a two-[[spar (aviation)|spar]] centre section with just over 6.5 degrees of [[Dihedral (aeronautics)|anhedral]] and outer panels with 9 degrees of dihedral.<ref name=b57>Buttler 1999, pp. 55–57.</ref> It could be hydraulically folded in two places to allow more compact storage in the hangar decks of [[aircraft carrier]]s. Four [[Flap (aircraft)#Types|Fowler flap]]s were fitted to give good low-speed handling for landing and the wing had retractable [[dive brake]]s on both surfaces.<ref name="Jackson p452"/> In the course of the redesign the structure was simplified which reduced weight by 1,400&nbsp;lb (635&nbsp;kg) and even after the fuel capacity was increased by {{convert|70|impgal|l USgal}} the gross weight was still {{convert|900|lb|kg}} less than that of the Firebrand.<ref name=b81>Buttler 2004, p. 181.</ref>

Work on two prototypes was authorised in November 1943, but proposals for alternative engines delayed progress. In 1945, it was decided that as well as adding another Centaurus-engined prototype, there should be three prototypes with the Napier E.122 (a development of the [[Napier Sabre|Sabre]]) as Specification S.10/45. The Ministry believed that this would enable Blackburn to develop their knowledge of aerodynamic and structural design and support the engine development at Napier. However, it was found that the S.10/45 aircraft could only be balanced if the E.122 powerplant was placed behind the pilot. The necessary redesign and {{convert|1000|lb|adj=on|0}} weight increase, coupled with the limited funds available to the Royal Navy, meant that it could no longer be justified and the S.10/45 was cancelled on 8 October. While in final design, the Centaurus 77 engine with contra-rotating propellers was cancelled in January 1946 and a conventional {{convert|2825|hp|adj=on}} Centaurus 57 was substituted. This engine was found to require flexible mounts and was modified into the Centaurus 59. The vertical stabiliser and rudder had to be enlarged from {{convert|33|to|41|sqft}} to counteract the new engine's torque. In September 1946 a strength analysis conducted by the Ministry of Supply revealed that the aircraft would require strengthening to serve as a strike fighter and that a costly redesign would be required to bring it up to requirements. This would have made it comparable in weight and performance to the [[Westland Wyvern]] which had already flown so no contract was placed for production aircraft.<ref>Buttler 1999, pp. 56–58.</ref>

Delayed by the late delivery of its propeller, the first prototype was rolled out at [[Brough Aerodrome|Brough]] in February 1947 and then taken by road to [[RAF Leconfield]] where it made its [[maiden flight]] on 1 April that year. All three prototypes were completed by the end of September and the third prototype had been modified to reduce the outer-wing dihedral to 3 degrees. Both the second and third prototypes remained unflown when the Ministry of Supply ordered that flying be ceased and work on the aircraft be stopped. Later in the month, however, the third prototype was allocated to tests of powered [[aileron]] controls, as testing of the first prototype had shown that while adequate at cruise speed, the ailerons were heavy both at low and high speed. The second prototype was allocated to structural testing.<ref>Buttler 1999, pp. 57–58.</ref>

The third prototype made its maiden flight in early 1948, but the pace of the flight testing was leisurely with only 7 hours and 40 minutes completed by 30 November, over half of which were connected with [[air show]] performances. Testing concluded in March 1949 when the officer in charge concluded that there was no further purpose to the tests.<ref name="Useful p58-9">Buttler 1999, pp. 58–59.</ref> While the Firecrest was faster than the Firebrand, and gave its pilot a much better view from the cockpit, it was otherwise disappointing, with [[test pilot]] and naval aviator [[Captain (Royal Navy)|Captain]] [[Eric Brown (pilot)|Eric Brown]] claiming that the Firecrest was even less manoeuvrable than the sluggish Firebrand, while the powered ailerons gave lumpy controls, leading to instability in turbulent air.<ref name="Firebrand p47">Brown 1978, p. 47.</ref>

==Operational history==
[[File:Blackburn Firecrest.png|thumb|Firecrest third prototype VF172]]
Operational experience had found Blackburn's Firebrand strike fighter to be far from suited to carrier operations. In particular, the pilot sat near the wing's trailing edge, looking over a very long and wide nose which gave a particularly poor view for landing.<ref name="Firebrand p46">Brown 1978, p. 46.</ref> The Firecrest had also been rendered obsolete by the arrival of [[gas turbine]] engines, and while Blackburn did draw up proposals for [[turboprop]]-powered derivatives of the Firecrest, (as the B-62 (Y.A.6) with the [[Armstrong Siddeley Python]] engine), these went unbuilt, with orders instead going to Westland for the Wyvern.<ref name="Mason Fighter p330-1"/> The two flying prototypes remained in use until 1949, being sold back to Blackburn in 1950, and were later scrapped.<ref name="Useful p59">Buttler 1999, p. 59.</ref>

==Aircraft==
;RT651
:One of two prototypes ordered on 1 January 1944 to Specification S.28/43. The airframe was sold by the Controller of Supplies (Air) to Blackburn on 17 April 1950.<ref name="fixedwing41">Sturtivant 2004, p. 41.</ref>
;RT656
:The second of two prototypes ordered on 1 January 1944, it was used for structural testing before being disposed of in 1952.<ref name="Useful p59"/>
;VF172
:A third aircraft was ordered on 18 April 1945 and it was used for research into power-boosted ailerons during February 1948. The airframe was sold to Blackburn on 17 October 1949.<ref name="fixedwing42">Sturtivant 2004, p. 42.</ref>

Three further prototypes were ordered on 14 March 1945 against Specification S.10/45 and powered by Napier E.122 engine, but the order was cancelled and the aircraft were not built.<ref name="fixedwing42" />

==Operators==
;{{UK}}
*[[Fleet Air Arm]] (never entered service)

==Specifications==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
|plane or copter?=plane
|jet or prop?=prop
|ref=Blackburn Aircraft since 1909<ref name="Jackson p455-6">Jackson 1968, pp. 455–456.</ref>
|crew=one pilot
|span main=44 ft 11½ in
|span alt=13.71 m
|length main=39 ft 3½ in
|length alt=11.98 m
|height main=14 ft 6 in
|height alt=4.42 m
|area main=361.5 ft²
|area alt=33.60 m²
|airfoil=
|empty weight main=10,513 lb
|empty weight alt=4,779 kg
|loaded weight main=15,280 lb
|loaded weight alt=6,645 kg
|max takeoff weight main=
|max takeoff weight alt=
|more general=
|engine (prop)=[[Bristol Centaurus]] 59
|type of prop=18-cylinder [[radial engine]]
|number of props=1
|power main=2,475 hp
|power alt=1,846 kW
|max speed main=380 mph
|max speed alt=330 knots, 612 km/h
|max speed more=at 19,000 ft (5,790 m)
|cruise speed main=213 mph
|cruise speed alt=185 knots, 343 km/h
|cruise speed more=at 15,000 ft (4,600 m)<ref name="Useful p57">Buttler 1999, p. 57.</ref>
|stall speed main= 
|stall speed alt= 
|range main=900 mi
|range alt=783 [[nautical mile|nmi]], 1,450 km
|range more=
|ceiling main=31,600 ft
|ceiling alt=9,630 m
|climb rate main=2,500 ft/min
|climb rate alt=12.7 m/s
|loading main=
|loading alt=
|power/mass main=
|power/mass alt=
|guns=Provision for 2 × .50 in (12.7 mm) [[M2 Browning machine gun]]s under or in wing (not fitted to prototypes)
|bombs=<br/>
** 1 × 2,097 lb (951 kg) [[torpedo]], ''or''
** 2 × 250 lb (110 kg) bombs, one under each wing, in lieu of torpedo
|rockets=8 × [[RP-3]] rocket projectiles on underwing rails
}}

==See also==
{{aircontent
|similar aircraft=
* [[Westland Wyvern]]
* [[Douglas AD Skyraider]]
* [[Martin AM Mauler]]
|related=
* [[Blackburn Firebrand]]
|see also=
|lists=
* [[List of aircraft of the Fleet Air Arm]]
}}

==References==

===Notes===
{{reflist|2}}

===Bibliography===
* Brown, Eric. "The Firebrand...Blackburn's Baby 'Battleship'". ''[[Air International]]'', July 1978, Vol. 15:1. Bromley, UK: Fine Scroll. pp.&nbsp;25–31, 46–47. {{ISSN|0306-5634}}.
* Buttler, Tony. ''British Secret Projects: Fighters & Bombers 1935–1950''. Hinckley, UK: Midland Publishing, 2004. ISBN 1-85780-179-2.
* Buttler, Tony. "Something Useful! Blackburn's 'Firecrest', Son of Firebrand". ''[[Air Enthusiast]]''. No. 82, July/August 1999. Stamford, UK: Key Publishing. pp.&nbsp;55–59. {{ISSN|0143-5450}}.
* Jackson, A. J. ''Blackburn Aircraft Since 1909''. London: Putnam, 1968. ISBN 0-370-00053-6.
* Mason, Francis K. ''The British Fighter Since 1912''. Annapolis, Maryland: Naval Institute Press, 1992. ISBN 1-55750-082-7.
* [[Ray Sturtivant|Sturtivant, Ray]]. ''Fleet Air Arm Fixed-Wing Aircraft Since 1946''.  Tonbridge, Kent, UK: [[Air-Britain]], 2004. ISBN 0-85130-283-1.
* Williams, Ray. ''Fly Navy; Aircraft of the Fleet Air Arm Since 1945''. Shrewsbury, UK: Airlife, 1989. ISBN 1-85310-057-9.

==External links==
{{commons category|Blackburn Firecrest}}
*[http://www.flightglobal.com/pdfarchive/view/1947/1947%20-%200839.html "Designed to Strike"] a 1947 ''Flight'' article on the Firecrest
*[http://www.flightglobal.com/pdfarchive/view/1948/1948%20-%201430.html Brief mention in 'Flight' of an appearance at an airshow]
*[http://www.flightglobal.com/pdfarchive/view/1949/1949%20-%200393.html "Y.A.1 In the Air"] a 1949 ''Flight'' article on flying the Firecrest

{{Blackburn aircraft}}

[[Category:Cancelled military aircraft projects of the United Kingdom]]
[[Category:Blackburn aircraft|Firecrest]]
[[Category:British fighter aircraft 1940–1949]]
[[Category:Carrier-based aircraft]]
[[Category:Inverted gull-wing aircraft]]
[[Category:Single-engined tractor aircraft]]
[[Category:Low-wing aircraft]]