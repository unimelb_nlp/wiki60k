{{Infobox football club
  | nickname = Lady Reds, United, Reds
  | ground   = [[Elite Systems Football Centre]], [[Adelaide]]
  | capacity = 5,000
  | season   = [[2015–16 W-League|2015–16]]
  | pattern_la1=
| pattern_la2=
| pattern_b2 = _au14a
|pattern_ra1=
  |pattern_sh2=
  | leftarm1=FFFFFF
| leftarm2=000000
| body1 = FF0000
| body2 = FFFFFF
| rightarm1 = FFFFFF
| rightarm2 = 000000
| shorts1 = FF0000
| shorts2 = FFFFFF
|socks1=FF0000
  |socks2=FFFFFF
| clubname = 
  | image    = [[File:Adelaide_United_FC_W-League_logo.png|150px|logo]]
  | fullname = Adelaide United Football Club
  | founded  = 2008
  | chairman = [[Greg Griffin]]
  | manager  = [[Huss Skenderovic]]
  | league   = [[W-League (Australia)|W-League]]
  | position = 5th (league)
<!-- DATA TO HIDE: no link to 2015-16 season until article created | current   = 2014 Adelaide United W-League season -->
  | pattern_b1 = _au14h
}}

'''Adelaide United FC''', also known as the '''Lady Reds''', is an Australian [[association football|soccer]] team based in [[Adelaide]], [[South Australia]]. Founded in 2008, it is the affiliated [[Women's association football|women's team]] of [[Adelaide United FC|Adelaide United]]. The team competes in the country's premier women's soccer competition, the [[W-League (Australia)|W-League]], under [[Professional sports league organization#Systems around the world|licence]] from [[Football Federation Australia]].

==History==
=== Establishment ===
{{See also|W-League (Australia)#Establishment|l1=W-League: Establishment}}
Adelaide United Women's team was formed in 2008 with the inception of the W-League, becoming one of the founding eight teams.<ref>{{cite news|url=http://www.a-league.com.au/default.aspx?s=wleague_newsdisplay&id=24298|title=Westfield W-League officially launched|publisher=''A-League''|date=20 October 2008|accessdate=14 January 2009}}</ref> The inaugural set up saw [[North Eastern MetroStars]] coach [[Michael Barnett (footballer)|Michael Barnett]] take charge<ref>{{cite news|url=http://www.metrostars.com.au/Coaches/coaches.htm |title=MetroStars Coaches |publisher=''MetroStars'' |date=14 January 2009 |accessdate=14 January 2009 |archiveurl=https://web.archive.org/web/20080719213749/http://www.metrostars.com.au/Coaches/coaches.htm |archivedate=19 July 2008 |deadurl=no |df= }}</ref> with ex-Adelaide United player [[Richie Alagich]] take up the assistant coach role and [[Australia women's national soccer team|Matildas]] stalwart [[Dianne Alagich]] named as captain.<ref>{{cite news|url=http://www.adelaideunited.com.au/default.aspx?s=wleague_newsdisplay&id=23551|title=Former Matilda named as Reds' W-League Captain|publisher=''Adelaide United''|date=10 September 2008|accessdate=14 January 2009}}</ref>

=== Colours and badge ===
Since its inception Adelaide United has played in a predominantly all-red home kit. For the inaugural season the away kit consisted of a white top and socks and red shorts; during the 2009 season the away kit changed to a black top with red shorts and socks. The badge is heavily based on the Adelaide United men's team, with the logo being encased in a W-League shield; as is the case with every other W-League club.

===Inaugural season ===
{{main|2008–09 Adelaide United W-League season}}
Adelaide's first game was on 25 October 2008 against [[Brisbane Roar FC W-League|Queensland Roar]] at the [[Queensland Sport and Athletics Centre]], which ended in a 4–1 loss with [[Sharon Black]] getting the consolation goal.<ref>{{cite news|url=http://www.a-league.com.au/default.aspx?s=hal_newsdisplay&id=24439&pageid=2775 |title=Roar fires early |publisher=''A-League'' |date=25 October 2008 |accessdate=14 January 2009 |first=Rob |last=Forsaith |deadurl=yes |archiveurl=https://web.archive.org/web/20070706044557/http://a-league.com.au/default.aspx?s=hal_newsdisplay |archivedate=6 July 2007 |df= }}</ref> The first win came in Round 2 against the [[Newcastle Jets FC W-League|Newcastle Jets]] at [[Hindmarsh Stadium]], a hard fought 3–2 win thanks to a hat-trick from [[Sandra Scalzi]].<ref>{{cite news|url=http://www.a-league.com.au/article/scalzi-fires-united-home/11wazje2qcpgm1a5t2part6agw|title=Scalzi fires United home|publisher=''A-League''|date=1 November 2008|accessdate=3 March 2016}}</ref> The Reds followed this up with another 3–2 win over [[Sydney FC W-League|Sydney FC]] before failing to win any of their next seven league games, finished last in the eight-team competition in their [[W-League 2008-09|inaugural season]].

=== 2009 season ===
{{see also|2009 Adelaide United W-League season}}
After a disappointing inaugural season drastic changes were made to the playing staff including the retirement of experienced campaigners [[Sharon Black]] and [[Dianne Alagich]] to create a youthful team for the [[W-League 2009|2009 season]].<ref>{{cite news|url=http://www.adelaideunited.com.au/default.aspx?s=wleague_newsdisplay&id=29639 |title=Lady Reds to take off against Jets in Westfield W-League |publisher=''Adelaide United'' |date=30 September 2009 |accessdate=6 October 2009 |deadurl=yes |archiveurl=https://web.archive.org/web/20110312071353/http://www.adelaideunited.com.au/default.aspx?s=wleague_newsdisplay&id=29639 |archivedate=12 March 2011 |df= }}</ref><ref>{{cite news|url=http://www.adelaideunited.com.au/default.aspx?s=wleague_newsdisplay&id=29638|title=
Di Alagich ends amazing career in football|publisher=''Adelaide United''|date=30 September 2009|accessdate=6 October 2009}}</ref> Despite the new look squad Adelaide continued to struggle in the league failing to win any of the first 5 games which included a record equaling defeat against [[Sydney FC W-League|Sydney FC]] on 1 November 2009.<ref>{{cite news|url=http://www.a-league.com.au/default.aspx?s=wleague_newsdisplay&id=30403|title=
Sydney destroys Adelaide|publisher=''A-League''|date=1 November 2009|accessdate=1 November 2009|first=Aiden|last=Ormond }}</ref> The first goal of the 2009 season was scored by [[Tenneille Boaler]] against [[Newcastle Jets FC W-League|Newcastle Jets]] in the round 6 clash at the [[Wanderers Oval]] the game ended in a 3&nbsp;– 3 draw handing Adelaide its second point of the year.<ref>{{cite news|url=http://www.a-league.com.au/default.aspx?s=wleague_newsdisplay&id=30548|title=Goals galore for Jets, Reds|publisher=''A-League''|date=7 November 2009|accessdate=7 November 2009|first=Mark|last=Hughes}}</ref> The season didn't get any better for Adelaide as they failed to win a single game in the second season but thanks to an unlikely 2 all draw with power house team [[Brisbane Roar FC W-League|Brisbane Roar]] they finished the season in 7th place their best ever finish to date.<ref>{{cite news
 |url=http://www.adelaideunited.com.au/default.aspx?s=wleague_newsdisplay&id=31226
 |title=Adelaide United Women go down to Canberra United
 |publisher=''Adelaide United''
 |date=5 December 2009
 |accessdate=6 December 2009
 |first=Nick
 |last=Guoth
 |deadurl=yes
 |archiveurl=https://web.archive.org/web/20110220020043/http://www.adelaideunited.com.au/default.aspx?s=wleague_newsdisplay&id=31226
 |archivedate=20 February 2011
 |df= 
}}</ref>
Most Valuable player for 2009 season [[Racheal Quigley]].

=== 2010–11 season===
{{see also|2010–11 Adelaide United W-League season}}
The [[2010–11 W-League|2010–11 season]] was even worse for the Lady Reds as they lost all ten of their W-League matches.  They only scored four goals and finished with a disappointing −32 goal differential; they tied their worst defeat with a −1 loss to Newcastle in round nine.  Coach Michael Barnett was let go at the end of the disappointing season, and was replaced by David Edmondson.

=== 2011–12 season===
{{see also|2011–12 Adelaide United W-League season}}
Adelaide continued to struggle through most of the [[2011–12 W-League|2011–12 season]] as they opened the campaign with six more losses, scoring four goals during that time while letting twenty-one in.  This was better pace than the previous season, though, and the Lady Reds showed significant improvement in on-field play versus 2010–2011, cited as being "unlucky" to not come away with at least a point on multiple occasions<!--refs here-->.  They finally snapped their winless and losing streaks, at 34 and 18 games respectively, with a 1–0 defeat of the Perth Glory in round eight, taking them off the bottom of the table for the first time since November 2009.{{citation needed|date=October 2016}}

Following [[Adelaide United FC|Adelaide United]] taking control of the women's team, their first move was signing [[Mark Jones (footballer, born 1966)|Mark Jones]] as the head coach.<ref>{{cite web|url=http://thewomensgame.com/2016/09/mark-jones-to-lead-adelaide-united-in-201617/|title=Mark Jones to lead Adelaide United in 2016/17|publisher=The Women's Game|date=7 September 2016}}</ref>

==Current squad==
<small>squad for the [[2016–17 W-League|2016–17]] season. (current at 3 November 2016)<ref>{{cite web|url=http://www.adelaideunited.com.au/article/adelaide-united-announces-westfield-w-league-201617-squad/1my8lbxnklv341qvkximm5oez7|title=Adelaide United announces Westfield W-League 2016/17 squad|publisher={{ALeague AU}}|date=3 November 2016}}</ref></small>

{{fs start}}
{{fs player| no=1  | nat=AUS | pos=GK | name=[[Eliza Campbell]]}}
{{fs player| no=2  | nat=AUS | pos=MF | name=[[Cheyenne Hammond]]}}
{{fs player| no=4  | nat=AUS | pos=   | name=[[Emily Hodgson]]}}
{{fs player| no=5  | nat=BRA | pos=DF | name=[[Monica Hickmann Alves|Mônica]]|other=on loan from [[Orlando Pride]]}}
{{fs player| no=6  | nat=AUS | pos=   | name=[[Ally Ladas]]}}
{{fs player| no=7  | nat=AUS | pos=   | name=[[Stella Rigon]]|other=[[Captain (association football)|Captain]]}}
{{fs player| no=8  | nat=AUS | pos=FW | name=[[Emily Condon]]}}
{{fs player| no=9  | nat=AUS | pos=FW | name=[[Marijana Rajcic]]}}
{{fs player| no=10 | nat=AUS | pos=FW | name=[[Racheal Quigley]]}}
{{fs player| no=11 | nat=MEX | pos=FW | name=[[Sofia Huerta]]|other=on loan from [[Chicago Red Stars]]}}
{{fs player| no=12 | nat=AUS | pos=FW | name=[[Adriana Jones]]}}
{{fs mid}}
{{fs player| no=13 | nat=AUS | pos=   | name=[[Lucy Adamopoulos]]}}
{{fs player| no=14 | nat=AUS | pos=DF | name=[[Grace Abbey]]}}
{{fs player| no=15 | nat=AUS | pos=MF | name=[[Georgia Campagnale]]}}
{{fs player| no=16 | nat=AUS | pos=   | name=[[Kelsey Zafiridis]]}}
{{fs player| no=17 | nat=AUS | pos=   | name=[[Dragana Kljajic]]}}
{{fs player| no=18 | nat=AUS | pos=   | name=[[Isabella Scalzi]]}}
{{fs player| no=20 | nat=AUS | pos=GK | name=[[Sarah Willacy]]}}
{{fs player| no=21 | nat=AUS | pos=GK | name=[[Claudia Jenkins]]}}
{{fs player| no=24 | nat=USA | pos=MF | name=[[Danielle Colaprico]]|other=on loan from [[Chicago Red Stars]]}}
{{fs player| no=25 | nat=USA | pos=DF | name=[[Katie Naughton]]|other=on loan from [[Chicago Red Stars]]}}
{{fs player| no=30 | nat=AUS | pos=MF | name=[[Alex Chidiac]]}}
{{fs end}}

==Technical staff==
{| class="wikitable"
|-
! Position !! Name
|-
|Head Coach || {{flagicon|AUS}} [[Hussein Skenderovic]]
|-
|Assistant Coach || {{flagicon|AUS}} Simon Catanzaro
|-
|Goalkeeper Coach || {{flagicon|AUS}} Neil Tate
|-
|Conditioning Coach || {{flagicon|AUS}} Nik Hagicostas
|-
|Doctor || {{flagicon|AUS}} James Ilic
|-
|Physiotherapist || {{flagicon|AUS}} Marieke Cornielissen
|-
|Physiotherapist || {{flagicon|AUS}} Harry Roesch
|-
|Sports Trainer || {{flagicon|AUS}} Carol Goddard
|-
|}

==Managers==
''Last updated 20 November 2010''
{| class="wikitable sortable"
|- bgcolor="#efefef"
! Name
! From
! To
! Played
! Won&nbsp;
! Drawn
! Lost
|-
|{{flagicon|AUS}} [[Michael Barnett (footballer)|Michael Barnett]]
|Sep 2008
|Feb 2011
|30
|2
|4
|24
|-
|{{flagicon|ENG}} [[David Edmondson (coach)|David Edmondson]]
|Sep 2011
|Aug 2013
|22
|3
|0
|19
|-
|{{flagicon|AUS}} [[Ross Aloisi]]
|Aug 2013
|Jul 2015
|24
|6
|5
|13
|-
|{{flagicon|AUS}} [[Jamie Harnwell]]
|Jul 2015
|Sep 2016
|
|
|
|
|-
|{{flagicon|AUS}} [[Mark Jones (footballer, born 1966)|Mark Jones]]
|Sep 2016
|Sep 2016
|
|
|
|
|-
|{{flagicon|AUS}} [[Hussein Skenderovic]]
|Oct 2016
|
|
|
|
|
|}

==Stadium==
{{Main|Hindmarsh Stadium}}
Adelaide United WFC play their home games at Hindmarsh Stadium where they sometimes play a curtain-raiser to A-League games.<ref>{{cite news|url=http://theworldgame.sbs.com.au/newcastle-united-jets/w-league-reds-come-home-148134/|title=W-League: Reds come home|publisher=''[[Special Broadcasting Service|SBS]]''|date=30 October 2008|accessdate=17 January 2009}}</ref>

==Broadcasting==
{{see also|W-League (Australia)#Broadcasting}}
One league match per week is broadcast in Australia via [[ABC (Australian TV channel)|ABC]].<ref>{{cite web|title=Football: W-League|url=http://www.abc.net.au/tv/programs/football-wleague/|publisher=ABC|accessdate=26 December 2016}}</ref> Beginning in 2016, the weekly game is also broadcast on [[ESPN 3]] in the United States.<ref>{{cite web|last1=Initil|first1=Daniela|title=W-League broadcasting breakthrough indicative of progress for women's sport|url=http://www.abc.net.au/news/2016-11-02/w-league-broadcasting-indicative-of-progress-women's-sport/7986126|publisher=ABC|accessdate=26 December 2016|date=2 November 2016}}</ref>

==Records==
''Last updated 18 December 2011''
* '''Record Victory''': 10&nbsp;– 2 vs [[Western Sydney Wanderers FC W-League|Western Sydney Wanderers]], 14 January 2017, 3&nbsp;– 2 vs [[Newcastle Jets FC W-League|Newcastle Jets]], 31 October 2008 and [[Sydney FC W-League|Sydney FC]], 9 November 2008, 1&nbsp;– 0 vs [[Perth Glory FC W-League|Perth Glory]], 17 December 2011 and 4&nbsp;– 3 vs [[Western Sydney Wanderers FC W-League|Western Sydney Wanderers]], 20 October 2012
* '''Record Defeat''': 0&nbsp;– 6 vs [[Central Coast Mariners FC W-League|Central Coast Mariners]], 6 December 2008 and 14 November 2009, [[Sydney FC W-League|Sydney FC]], 1 November 2009, 1&nbsp;– 7 vs [[Newcastle Jets FC W-League|Newcastle Jets]], 8 January 2011
* '''Undefeated Streak''': 2, 31 October 2008&nbsp;– 9 November 2008
* '''Without Winning''':  34, 15 November 2008&nbsp;– 10 December 2011
* '''All-time Leading Goal Scorer''': [[Racheal Quigley]], 7 goals.
* '''All-time Leading Appearances''': [[Donna Cockayne]], 27 Appearances.

==See also==
* [[List of top-division football clubs in AFC countries]]
* [[Women's soccer in Australia]]
* [[W-League (Australia) all-time records]]
* [[Australia women's national soccer team]]
{{Portal bar|Women's association football|Women's sport|Association football|Australia}}

==References==
{{reflist}}

==External links==
* [http://www.adelaideunited.com.au/default.aspx?s=wleague_news Official club website]

{{AUS fb W-League AU}}
{{AUS fb W-League}}
{{Association football in South Australia}}
{{Adelaide Sports Teams}}

[[Category:Adelaide United FC (W-League)]]
[[Category:W-League (Australia) teams]]
[[Category:Adelaide United FC]]
[[Category:Association football clubs established in 2008]]
[[Category:Women's soccer clubs in Australia]]
[[Category:2008 establishments in Australia]]

[[pl:Adelaide United#Sekcja kobiet|Adelaide United]]