{{Infobox Aircraft accident|
|image         = Boeing 737-3Y0, Avia Traffic AN2213187.jpg
|caption       = EX-37005, the aircraft involved
|image_size    = 250
|summary       = Severe damage due to hard landing
|name          = Avia Traffic Company Flight 768
|date          = 22 November 2015
|site          = [[Osh Airport]]
|aircraft_type = [[Boeing 737-3YO]]
|operator      = [[Avia Traffic Company]]
|tail_number   = EX-37005
|origin        = [[Bishkek Airport]], [[Kyrgyzstan]]
|destination   = [[Osh Airport]], Kyrgyzstan 
|passengers    = 153
|crew          = 6
|injuries      = 14 (4 serious)
|fatalities    = 0
|survivors     = 159 (all)
|}}
{{Location map many
| Kyrgyzstan
| width       = 
| float       = 
| border      = 
| caption     = A map showing Manas International Airport (FRU) and Osh Airport (OSS), the location of the accident.
| alt         = A map showing the crash site
| relief      = 1
| AlternativeMap = 
| <!--first label/marker-->
| label1      =  FRU
| label1_size =  <!-- or: label_size -->
| position1   =  <!-- or: position, pos1, pos -->
| background1 =  <!-- or: background, bg1, bg -->
| mark1       =  Airplane_silhouette.svg
| mark1size   =  10
| link1       =  <!-- or: link -->
| lat1_deg    =  43
| lat1_min    =  03
| lat1_sec    =  40.7
| lat1_dir    =  N
| lon1_deg    =  74
| lon1_min    =  28
| lon1_sec    =  39.2
| lon1_dir    =  E
| <!--second label/marker-->
| label2      = OSS
| label2_size = 
| position2   =  <!-- or: pos2 -->
| background2 = 
| mark2       = Airplane_silhouette.svg
| mark2size   = 10
| link2       = 
| lat2_deg    = 40
| lat2_min    = 36
| lat2_sec    = 32
| lat2_dir    = N
| lon2_deg    = 72
| lon2_min    = 47
| lon2_sec    = 35
| lon2_dir    = E
| <!--third label/marker-->
| label3      = Crash site
| label3_size = 
| position3   =  left
| background3 = 
| mark3       = Yellow pog.svg
| mark3size   = 10
| link3       = 
| lat3_deg    = 40
| lat3_min    = 36
| lat3_sec    = 32
| lat3_dir    = N
| lon3_deg    = 72
| lon3_min    = 47
| lon3_sec    = 35
| lon3_dir    = E}}

'''Avia Traffic Company Flight 768''' was a scheduled passenger flight from [[Bishkek Airport]], to [[Osh Airport]] operated by the airline's fleet of four [[Boeing 737-3YO]] aircraft. On 22 November 2015 EX-37005, operating the flight, was on final approach to [[Osh Airport]] when it touched down hard enough to shear off the left and right main landing gear. The aircraft skidded off the runway with the left engine being torn from its mount.<ref>{{cite web|url=http://www.jacdec.de/2015/11/22/2015-11-22-avia-traffic-boeing-737-300-seriously-damaged-on-landing-at-osh/|title=2015-11-22 Avia Traffic Boeing 737-300 seriously damaged on landing at Osh|publisher=}}</ref>

==Accident==

===Aircraft===
The aircraft involved was a Boeing 737-3YO, registration EX-37005, originally delivered to [[Philippine Airlines]] in 1990; it was later sold to [[Garuda Indonesia]], [[Citilink]] and [[Sama Airlines]] before being sold to [[Avia Traffic Company]] in 2011. The aircraft was 25 years old at the time of the accident.<ref>{{cite web|url=https://www.planespotters.net/airframe/Boeing/737/24681/EX-37005-Avia-Traffic-Company|title=EX-37005 Avia Traffic Company Boeing 737-3Y0 - cn 24681 / 1929|publisher=}}</ref> The aircraft was written off after the accident.{{Citation needed|date=March 2016}}

===Landing===
The aircraft had originally departed [[Krasnoyarsk Airport]] [[Russia]] for Osh but diverted to Bishkek due to fog in Osh. After the weather improved, the crew departed for Osh. Ground observers reported that the visibility deteriorated to about {{convert|150|m|abbr=on}}.{{Citation needed|date=March 2016}}

The aircraft was performing an [[Instrument Landing System|ILS]] approach to Osh's runway 12 at about 07:56L (01:56Z) but touched down hard causing the landing gear to collapse and separate from the aircraft. The aircraft went off the runway and ran over rough terrain, the left-hand [[CFM International CFM56]] engine separated and the right-hand engine received substantial damage before the aircraft came to a stop about {{convert|1500|m|abbr=on}} from touchdown. Four occupants received serious injuries and ten occupants received minor injuries.<ref name="avherald.com">{{cite web|url=http://avherald.com/h?article=48fbb5cb&opt=0|title=The Aviation Herald|publisher=}}</ref>

==Investigation==
Russia's Civil Aviation Authority opened an investigation into the accident. Preliminary reports suggested the aircraft was performing an ILS approach to Osh's runway 12, with {{convert|50|m|abbr=on}} visibility in fog. The crew conducted a go-around following a hard touch down and joined the traffic pattern, but during the traffic pattern the crew decided to divert to their alternate and return to Bishkek, but soon after they received warnings of two hydraulic system failures, as well as failure of the No.2 engine, which was caused by the collapsed right landing gear. The crew shut the No.2 engine down and decided to perform an emergency landing at Osh, despite weather being below safe minima. The aircraft touched down very hard and skidded off the runway.<ref name="avherald.com"/><ref>{{cite web|url=http://www.dailymail.co.uk/travel/travel_news/article-3331763/Passengers-hospitalised-plane-hit-ground-hard-landing-gear-COLLAPSED-left-wing-fell-off.html|title=Aviatraffic Company passengers hospitalised after plane hit ground so hard its wing came off|date=24 November 2015|work=Mail Online}}</ref>

==See also==
*[[Garuda Indonesia Flight 200]]
*[[Turkish Airlines Flight 1878]]

==References==
{{reflist}}

{{Aviation accidents and incidents in 2015}}

[[Category:Accidents and incidents involving the Boeing 737]]
[[Category:Airliner accidents and incidents caused by pilot error]]
[[Category:Aviation accidents and incidents in 2015]]
[[Category:Aviation accidents and incidents in Kyrgyzstan]]
[[Category:2015 in Kyrgyzstan]]
[[Category:November 2015 events]]
[[Category:Osh]]