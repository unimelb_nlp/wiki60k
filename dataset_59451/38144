{{Infobox play
| name       = The Blue Flame
| image      = Theda Bara in The Blue Flame 1920.jpg
| image_alt  = Black and white photo of a white woman wearing a feathered headdress and a dark gown with lace sleeves
| caption    = Theda Bara in ''The Blue Flame''
| writer     = George V. Hobart, [[John Willard (playwright)|John Willard]], Leta Vance Nicholson
| characters = 
| setting    = New York City
| premiere   = {{Start date|1920|03|20}}
| place      = [[Shubert Theatre (New York City)|Shubert Theatre]]
| orig_lang  = English
| genre      = [[Science fiction]], [[Thriller (genre)|thriller]]
}}

'''''The Blue Flame''''' is a four-act [[Play (theatre)|play]] written by George V. Hobart and [[John Willard (playwright)|John Willard]], who revised an earlier version by Leta Vance Nicholson. In 1920, producer [[Albert H. Woods]] staged it on [[Broadway theatre|Broadway]] and on tour across the United States. The main character is a religious young woman who dies and is revived by her scientist fiancé as a soulless ''[[femme fatale]]''. She seduces several men and involves them in crimes, including drug use and murder. In the final act, her death and resurrection are revealed to be a dream. The production starred [[Theda Bara]], a popular [[silent film]] actress who was known for playing similar roles in movies.

The play received strongly negative reviews, with critics ridiculing the plot, dialog, and Bara's acting. It has been called "one of the worst plays ever written".<ref name="Morehouse175">{{harvnb|Morehouse|1949|p=175}}</ref> However, Bara's movie fame drew large crowds to theaters, making the play a commercial success, with the production breaking attendance records at some of its venues. ''The Blue Flame'' was Bara's only Broadway role and one of her last professional acting projects.

==Plot==
In the first of the play's four acts, irreligious scientist John Varnum has developed a device to bring the recently dead back to life. His sweet, religious fiancée, Ruth Gordon, does not approve of his experiments and hopes to reform him. However, when she is struck by lightning and killed, she becomes the first person to be revived by his machine. Before she is reanimated, the audience sees her soul visibly leave her body as the "blue flame" of the title.<ref name="Sun">{{cite news |title=Miss Bara Fails; ''The Blue Flame'' Lacks in Thrills |work=[[The Sun (New York)|The Sun and The New York Herald]] |date=March 16, 1920 |volume=87 |issue=198 |page=9 |url=https://www.newspapers.com/clip/1854090/the_sun_and_the_new_york_herald/ |via=[[Newspapers.com]]}}{{open access}}</ref> With no soul, the revived Ruth has an entirely different personality. Upon waking, she asks John for a kiss, then suggests they marry immediately so they can begin having sex.

In the second and third acts, Ruth seduces a young man named Larry Winston and steals him away from his own fiancée. She takes Larry to New York's [[Chinatown, Manhattan|Chinatown]], where she gets him hooked on [[cocaine]] and steals an emerald from an idol. She seduces Ned Maddox and kills him for insurance money, framing another man for the murder. In the final act, Ruth's death and revival is revealed to be a dream John Varnum was having. Upon waking he now understands the importance of the soul; he embraces religion and destroys his life restoration device.

==Cast and characters==
[[File:Alan Dinehart in Big Town Girl.jpg|thumb|upright|right|alt=Sepia tone portrait photo of a white man wearing a jacket and tie|Alan Dinehart played scientist John Varnum.]]
[[File:DeWitt Jennings - 1919.jpg|thumb|upright|right|alt=Black and white portrait photo of a white man wearing a jacket and tie|DeWitt Jennings played police inspector Ryan.]]
The characters and cast from the Broadway production are given below: 
{| class="wikitable sortable plainrowheaders"
|+ Cast of the Broadway production
! scope="col" | Character
! scope="col" | Broadway cast
|-
! scope="row" |John Varnum
|[[Alan Dinehart]]
|-
! scope="row" |Ah Foo
|Jack Gibson
|-
! scope="row" |Ruth Gordon
|[[Theda Bara]]
|-
! scope="row" |Barnes
|Joseph Buckley
|-
! scope="row" |Cicely Varnum
|Helen Curry
|-
! scope="row" |Larry Winston
|[[Donald Gallaher]]
|-
! scope="row" |Quong Toy
|[[Henry Herbert (actor)|Henry Herbert]]
|-
! scope="row" |Ned Maddox
|Kenneth Hill
|-
! scope="row" |The Stranger
|Earl House
|-
! scope="row" |Patterson
|Frank Hughes
|-
! scope="row" |Inspector Ryan
|[[DeWitt C. Jennings]]
|-
! scope="row" |Nora Macree
|Tessie Lawrence
|-
! scope="row" |Clarissa Archibald
|Thais Lawton
|-
! scope="row" |Wung Ming
|Robert Lee
|-
! scope="row" |Grogan
|Martin Malloy
|-
! scope="row" |Tom Dorgan
|Harry Minturn
|-
! scope="row" |Miller
|Tom O'Hara
|-
! scope="row" |Ling Foo
|Royal Stout
|}

==History==

===Background and development===
The first version of ''The Blue Flame'' was written by Leta Vance Nicholson, a movie [[scenario writer]]. She sold it to theatrical agent Walter C. Jordan, who had it rewritten by George V. Hobart and John Willard. Jordan paid the three writers $10,000 for their work (about ${{inflation|US-NGDPPC|10000.00|1920|r=-3|fmt=c}} in {{inflation-year|US-NGDPPC}} dollars),{{inflation-fn|US-NGDPPC|group=notes}} then resold the play to producer Albert H. Woods for $35,000.<ref name="Claims">{{cite news |title=Davis Claims Right to ''The Blue Flame'' |work=The New York Times |date=March 13, 1920 |volume=69 |issue=22,694 |page=12 |url=https://query.nytimes.com/gst/abstract.html?res=9507E5D8153EE13ABC4B52DFB566838B639EDE}}</ref>

Actress Theda Bara was one of the most popular stars of [[silent film]]s.<ref>{{harvnb|Golden|1996|p=6}}</ref> From her first leading role as "the Vampire" in the 1915 movie ''[[A Fool There Was (1915 film)|A Fool There Was]]'', Bara had been [[Typecasting (acting)|typecast]] as a "vamp", playing ''[[femme fatale]]'' roles, with her characters seducing and ruining innocent men.{{refn |group=notes |These roles were not [[undead]] [[vampires]] as would be seen in later [[horror film]]s. The term "vampire" for a seductive woman was derived from "The Vampire", an 1897 poem by [[Rudyard Kipling]].<ref>{{harvnb|Golden|1996|p=30}}</ref>}} Although she sometimes performed in films playing other types of roles, these were not as successful commercially as her "vamp" films. She played dozens of similar roles while contracted with [[Fox Film]] from 1915 to 1919.<ref>{{harvnb|Genini|2012|pp=79–80}}</ref>

After Bara's contract with Fox ended, Woods approached her about appearing in a play. She had performed on stage early in her career, working with touring companies and in [[summer stock]], but had not performed on [[Broadway theatre|Broadway]].<ref>{{harvnb|Golden|1996|pp=21–23}}</ref> Bara told a reporter she was offered a few scripts to consider, and chose ''The Blue Flame'' (at that time titled ''The Lost Soul'') because it allowed her to play two versions of the character, one good and the other bad. She also hoped moving to the stage would bring her new career opportunities.<ref>{{harvnb|Golden|1996|pp=204–205}}</ref> Woods gave Bara a lucrative contract. Each week she received a salary of $1500. This was considerably less than the $4000 per week she had earned in her last year with Fox,<ref>{{harvnb|Genini|2012|p=81}}</ref> but Bara was also promised half the production's net profits. Additionally, Woods provided a finely appointed [[private railroad car]] to take her from city to city when the show toured.<ref>{{harvnb|Genini|2012|p=83}}</ref><ref name="Golden 1996 209">{{harvnb|Golden|1996|p=209}}</ref>

===Productions and legacy===
[[File:Blue Flame theme song cover.jpg|thumb|upright|right|alt=Sheet music cover showing a white woman with long, dark hair, dressed in a floor-length patterned gown and matching headpiece; her image is surrounded by text giving credits for the play and song|A theme song for the play was published in April 1920.]]
Woods brought in two directors, [[J. C. Huffman]] and W. H. Gilmore, to assist with the production.<ref name="Golden205">{{harvnb|Golden|1996|p=205}}</ref> The production began with a series of [[Preview (theatre)|preview]] performances in February 1920, appearing in [[Pittsburgh, Pennsylvania]]; [[Washington, D.C.]]; [[Stamford, Connecticut]]; and [[Chicago, Illinois]]. The final performances before the Broadway premier were in [[Boston]] in early March.<ref>{{harvnb|Golden|1996|pp=205–206}}</ref>

While the show was still in previews, writer [[Owen Davis]] claimed the story had been lifted from his earlier play ''Lola'', which had appeared briefly on Broadway in March 1911, then was adapted as a movie in 1914.<ref name="Claims"/> He filed a lawsuit, but by the end of May it was settled with a cash payment to Davis.<ref>{{cite news |title=Settle Davis Suit |work=Variety |date=May 28, 1920 |volume=59 |issue=1 |page=17 |url=http://www.varietyultimate.com/archive/issue/WV-05-28-1920-17}}</ref>

The show opened on Broadway at the [[Shubert Theatre (New York City)|Shubert Theatre]] on March 15, 1920. It had a run of 48 performances and closed in late April.<ref>{{harvnb|Golden|1996|pp=206, 209}}</ref><ref>{{cite web |url=http://ibdb.com/production.php?id=6785 |title=The Blue Flame |publisher=[[Internet Broadway Database]] |accessdate=February 25, 2015}}</ref> The show then continued to other cities on tour. The play was further promoted with the release of sheet music for a theme song, with Bara's image featured prominently on the cover.<ref>{{harvnb|Wlaschin|2009|p=73}}</ref> The song had music by William Frederic Peters and lyrics by [[Ballard MacDonald]], and was published in April by [[Shapiro, Bernstein & Co.]]<ref>{{cite book |title=[[Catalog of Copyright Entries]]: Part 3: Musical Compositions |publisher=[[United States Government Printing Office]] |location=Washington, DC |year=1920 |volume=15 |page=[https://archive.org/stream/catalogofcopyri151libr#page/438/mode/2up 6900]}}</ref>

The tour closed on January 1, 1921.<ref name="Golden 1996 209"/> ''The Blue Flame'' was Bara's last Broadway performance and her last acting tour. She did a season of [[vaudeville]] touring, but did not act in it; instead she talked with audiences and told stories about her career.<ref>{{harvnb|Golden|1996|p=210}}</ref> Her only subsequent stage acting was in a [[Little Theatre Movement|Little Theatre]] production of ''Bella Donna'' in 1934.<ref>{{harvnb|Golden|1996|p=225}}</ref> Bara's film career was also waning. She acted in only one feature film after ''The Blue Flame'' ended, the 1925 drama ''[[The Unchastened Woman]]''.<ref>{{harvnb|Golden|1996|pp=213–218}}</ref>

==Reception==

===Critical reception===
The play received overwhelmingly negative reviews. Biographer [[Eve Golden]] described the reviews of Bara's acting as "nothing less than vicious",<ref>{{harvnb|Golden|1996|p=206}}</ref> but the commentary about the play as a whole was even more negative.<ref name="Golden207">{{harvnb|Golden|1996|p=207}}</ref> ''[[Variety (magazine)|Variety]]'' said opinions in the daily press were united about how bad the play was.<ref>{{cite news |title=Broadway Has No New Shows Coming for Next Fortnight |work=Variety |date=March 19, 1920 |volume=58 |issue=4 |page=13 |url=http://www.varietyultimate.com/archive/issue/WV-03-19-1920-13}}</ref> The theater critic for ''[[Munsey's Magazine]]'' quoted several negative reviews and compared Bara's acting unfavorably to that of drama school students.<ref>{{harvnb|White|1920|p=144}}</ref>

In ''[[The New York Times]]'', [[Alexander Woollcott]] mocked the dialog, which included lines such as, "Have you brought the cocaine?" and "You make my heart laugh and I feel like a woman of the streets." Delivered seriously, this dialog drew laughter from the audience. Woollcott highlighted one particular line: "I'm going to be so bad, I'll be remembered always." He said Bara was bad, but not bad enough to be memorable. He credited her for speaking clearly and for not losing her composure when the audience laughed at her.<ref>{{harvnb|Woollcott|1920|p=18}}</ref> Other reviewers gave Bara similarly faint praise: she had "average competence" or "was not so bad". Some complimented her looks or her glamorous wardrobe.<ref name="Golden207"/>

Other reviewers were even more negative. The ''[[Brooklyn Daily Eagle]]'' said she fulfilled the promise to be unforgettably bad, calling her acting "freakish".<ref>{{cite news |title=Theda Bara Star of Queer Drama |work=Brooklyn Daily Eagle |date=March 16, 1920 |volume=80 |issue=75 |page=1.6 |url=https://www.newspapers.com/clip/1854009/the_brooklyn_daily_eagle/ |via=[[Newspapers.com]]}}{{open access}}</ref> ''The Sun and New York Herald'' said Bara's acting was disappointing and the play was "abysmal in intelligence and all that touches the art of the theatre".<ref name="Sun" /> In ''[[Ainslee's Magazine]]'', [[Dorothy Parker]] said the play's authors had taken the line about being remembered for badness "as their working motto", and suggested the crowds at the performances were there to attack the playwrights.<ref>{{harvnb|Parker|2014|p=136}}</ref> In the ''[[New-York Tribune]]'', [[Heywood Broun]] suggested that the entire production company should fear the wrath of God for such a terrible play.<ref name="Genini85">{{harvnb|Genini|2012|p=85}}</ref>

Historians and critics looking back on the play have affirmed the negative assessment. Biographer Ronald Genini called the play "painfully bad"<ref>{{harvnb|Genini|2012|p=82}}</ref> and described the reviews as "a panning orgy".<ref name="Genini85"/> Theater historian [[Ward Morehouse]] described it as "one of the worst plays ever written".<ref name="Morehouse175"/> Literary critic [[Edward Wagenknecht]] said Bara's participation helped end her acting career.<ref>{{harvnb|Wagenknecht|2014|p=130}}</ref> Golden wondered why Bara took the role, speculating that only "desperation and incredibly poor judgement" could justify her participation.<ref>{{harvnb|Golden|1996|p=204}}</ref>

===Box office===
Financially the play was a tremendous success, with the previews breaking attendance records.<ref name="Golden205" /> In Boston, the show was sold out, even after adding extra matinees. Despite the strongly negative reviews, the first week on Broadway took in nearly $20,000, close to the maximum the Shubert Theatre could generate at normal prices.<ref>{{cite news |title=Theda Bara Does $20,000 |work=[[The New York Clipper]] |date=March 24, 1920 |volume=68 |issue=7 |page=4 |url=http://idnc.library.illinois.edu/cgi-bin/illinois?a=d&d=NYC19200324.2.16&dliv=none&e=-------en-20--1--txt-txIN--------}}</ref> The show continued to pack the theater for two months, averaging $15,000 per week, before heading back to the road.<ref>{{harvnb|Golden|1996|p=208}}</ref>

==Notes==
{{Reflist|group=notes}}

==References==
{{Reflist|30em}}

===Works cited===
* {{Cite book |ref=harv |last=Genini |first=Ronald |title=Theda Bara: A Biography of the Silent Screen Vamp, with a Filmography |location=Jefferson, North Carolina |publisher=McFarland |year=2012 |origyear=1996 |isbn=978-0-7864-6918-5 |oclc=781129383}}
* {{Cite book |ref=harv |last=Golden |first=Eve |authorlink=Eve Golden |title=Vamp: The Rise and Fall of Theda Bara |location=Vestal, New York |publisher=Emprise |year=1996 |isbn=1-887322-00-0 |oclc=34575681}}
* {{Cite book |ref=harv |last=Morehouse |first=Ward |authorlink=Ward Morehouse |title=Matinee Tomorrow: Fifty Years of Our Theater |location=New York |publisher=Whittlesey House |year=1949 |page=175}}
* {{Cite book |ref=harv |last=Parker |first=Dorothy |authorlink=Dorothy Parker |editor-last=Fitzpatrick |editor-first=Kevin C. |title=Dorothy Parker: Complete Broadway, 1918-1923 |location=Bloomington, Indiana |publisher=iUniverse |year=2014 |isbn=978-1-4917-2265-7 |oclc=953166959}} Reprinted from {{cite news |last=Parker |first=Dorothy |date=May 1920 |title=In Broadway Playhouses: Springtime on the Rialto |work=Ainslee's Magazine |volume=45 |issue=4 |pages=154–158}}
* {{Cite book |ref=harv |first=Edward |last=Wagenknecht |authorlink=Edward Wagenknecht |title=The Movies in the Age of Innocence |edition=3rd |publisher=McFarland |location=Jefferson, North Carolina |year=2014 |isbn=978-0-7864-9462-0 |oclc=881446531}}
* {{Cite news |ref=harv |first=Matthew, Jr. |last=White |title=The Stage |work=Munsey's Magazine |date=June 1920 |volume=70 |issue=1 |pages=141–154}}
* {{Cite book |ref=harv |title=The Silent Cinema in Song, 1896-1929: An Illustrated History and Catalog of Songs Inspired by the Movies and Stars, with a List of Recordings |first=Ken |last=Wlaschin |publisher=McFarland & Company |location=Jefferson, North Carolina |year=2009 |isbn=978-0-7864-3804-4 |oclc=320443261}}
* {{Cite news |ref=harv |last=Woollcott |first=Alexander |authorlink=Alexander Woollcott |title=The Play: ''The Blue Flame'' |work=The New York Times |date=March 16, 1920 |volume=69 |issue=22,697 |page=18 |url=https://query.nytimes.com/gst/abstract.html?res=9802E4D7163AEE32A25755C1A9659C946195D6CF}}

==External links==
{{Commons category|The Blue Flame}}
* {{IBDB show|2107|The Blue Flame}}

{{Good article}}

{{DEFAULTSORT:Blue Flame}}
[[Category:1920 plays]]
[[Category:Broadway plays]]
[[Category:Dreams in theatre]]
[[Category:Melodramas]]
[[Category:Science fiction theatre]]