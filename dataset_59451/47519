[[File:Light intensity distribution.jpg|thumb|7x7 matrix using green laser and diff. beam splitter (Courtesy of Holo-Or)]]
The '''diffractive beam splitter''' 
<ref name="www.amazon.com">[http://www.amazon.com/Diffraction-Gratings-Applications-Optical-Engineering/dp/0824799232#reader_0824799232 Diffraction Gratings and Applications, Loewen, Erwin C. and Popov, Evgeny. Marcel Dekker, Inc. 1997.]</ref>
<ref name="www.amazon.com1">[http://www.amazon.com/Digital-Diffractive-Optics-Introduction-Technology/dp/0471984477#reader_0471984477 Digital diffractive optics: an introduction to planar diffractive optics and related technology, Bernard C. Kress, Patrick Meyrueis , 2005.]</ref>
(also known as '''multispot beam generator''' or '''array beam generator''') is a single [[Lens (optics)|optical element]] that divides an input [[Laser beam|beam]] into N output [[laser beam|beams]].<ref name="books.google.com">[https://books.google.com/books?id=NFrG-zFrIDYC&lpg=PP3&dq=diffractive%20optics&pg=PA83#v=twopage&q&f=false Diffractive Optics – Design, Fabrication and Test, O'Shea, Suleski, Kathman and Prather, 2004. p.83]</ref> Each output [[laser beam|beam]] retains the same optical characteristics as the input beam, such as size, [[polarization (waves)|polarization]] and [[phase (waves)|phase]]. A diffractive [[beam splitter]] can generate either a 1-dimensional beam array (1xN) or a 2-dimensional beam [[wikt:matrix|matrix]] (MxN), depending on the [[Diffraction#Patterns|diffractive pattern]] on the [[Lens (optics)|element]]. The diffractive beam splitter is used with [[Monochromatic electromagnetic plane wave|monochromatic light]] such as a [[laser beam]], and is designed for a specific [[wavelength]] and [[Angular distance|angle of separation]] between output beams.

==Applications==

Normally, a diffractive beam splitter is used in [[tandem]] with a [[Lens (optics)#Types of simple lenses|focusing lens]] so that the output beam array becomes an array of [[Focus (optics)|focused]] spots on a plane at a given distance from the lens, called the "working distance."  The [[focal length]] of the lens, together with the [[Angular distance|separation angle]] between the beams, determines the separation distance between the [[Focus (optics)|focused]] spots. This simple optical set-up is used in a variety of high-power [[laser]] research and industrial applications that typically include:

:* [[Laser cutting|Laser scribing]] ([[solar cell]]s)
:* [[Laser cutting|Glass dicing]] ([[Liquid crystal display|LCD displays]])
:* [[Perforation]] ([[cigarette filter]]s)
:* Beam sampling <ref name="LC">[http://www.lasercomponents.com/fileadmin/user_upload/home/Datasheets/holoor/beam_sampler.pdf Beam sampler]</ref>(Power monitoring & control)
:* 3-D [[Motion detection|motion sensing]]  <ref name="primesense">[http://www.google.com/patents/about?id=6WFWAQAAEBAJ OPTICAL DESIGNS FOR ZERO ORDER REDUCTION(patent)]</ref> ([[Kinect|Example]])
:* Medical/aesthetic applications (skin treatment) <ref name="medical1">[http://www.photonics.com/Article.aspx?AID=42337 Fractional Laser Skin Treatment Using Diffractive Optics]</ref>

[[File:Phase image.jpg|thumb|Periodic etch patterns on a)19x19 diff. beam splitter and b)1x31 splitter (courtesy of Holo-Or)]]

==Design principle==
The theory of operation is based on the [[wave]] nature of [[light]] and [[Huygens–Fresnel principle|Huygens' Principle]] (See also [[Diffraction]]). Designing the [[Diffraction#Patterns|diffractive pattern]] for a beam splitter follows the same principle as a [[diffraction grating]], with a repetitive pattern etched on the surface of a substrate. The depth of the [[etching]] pattern is roughly on the order of the wavelength of light in the application, with an adjustment factor related to the substrate's index of refraction. The [[etching]] pattern is composed of "periods" –identical sub-pattern units that repeat cyclically. The width d of the [[period (physics)|period]] is related to the separation angle θ between output beams according to the [[Diffraction_grating#Theory of operation|grating equation]]: 

:<math> d \sin \theta_m = m  \lambda </math>

'''''m''''' represents the order of the [[Diffraction|diffracted beam]], with the [[Diffraction grating|zero order]] output simply being the undiffracted continuation of the input beam.

While the [[Diffraction_grating#Theory of operation|grating equation]] determines the direction of the output beams, it does not determine the distribution of [[intensity (physics)|light intensity]] among those beams. The [[Power (physics)|power]] distribution is defined by the etching profile within the unit period, which can involve many (not less than two) etching transitions of varying duty cycles.

In a 1-dimensional diffractive beam splitter, the diffractive pattern is linear, while a 2-dimensional element will have a complex pattern.

For information on manufacturing process, see [[lithography]].

==References==
{{reflist}}

== External links ==
* [http://holoor.co.il/Diffractive_optics_Applications/Laser-Beam-Splitting.htm HOLOOR Diffractive beam-splitter]
* [https://www.youtube.com/watch?v=LSJXXIpc6aA Video presenting diffractive beam splitter developing. IFTA]
* [https://www.youtube.com/watch?v=QHrzIcZ7Pw8 Video.Light propagation simulation through diffractive splitter]

[[Category:Optical components]]
[[Category:Diffraction]]