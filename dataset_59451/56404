'''''Constructivist Foundations''''' is a triannual [[Peer review|peer-reviewed]] [[academic journal]] that focusses on [[constructivist epistemology|constructivist]] approaches to [[science]] and [[philosophy]], including [[radical constructivism]], [[enactivism|enactive cognitive science]], [[second order cybernetics]], biology of [[cognition]] and the theory of [[autopoietic]] systems, and non-dualizing philosophy. It was established in 2005 and the [[editor-in-chief]] is Alexander Riegler ([[Free University of Brussels]]).

== Content ==
The journal publishes scholarly articles with empirical, formal or conceptual content, survey articles providing an extensive overview, target articles which are openly discussed in commentaries, opinions, and book reviews. In addition to regular issues, the journal occasionally publishes special issues.

== Abstracting and indexing ==
The journal is abstracted and indexed in the [[Arts & Humanities Citation Index]], [[Current Contents]]/Arts & Humanities, [[Philosopher's Index]], [[PhilPapers]], [[Scopus]], and [[EBSCO Publishing|Education Research Complete]].

==External links==
* {{Official website|http://www.constructivistfoundations.info/}}
* {{ISSN|1782-348X}}

[[Category:Philosophy journals]]
[[Category:Contemporary philosophy]]
[[Category:Constructivism]]
[[Category:Triannual journals]]
[[Category:English-language journals]]
[[Category:Publications established in 2005]]


{{philo-journal-stub}}