{{infobox book| <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name          = Two Years' Vacation
| title_orig    = Deux ans de vacances
| translator    = 
| image         = 'Two Years' Vacation' by Léon Benett 01.jpg
| author        = [[Jules Verne]]
| illustrator   = [[Léon Benett]]
| cover_artist  = 
| country       = France
| language      = French
| series        = [[Voyages Extraordinaires|The Extraordinary Voyages]] #32
| genre         = [[Adventure novel]]
| publisher     = [[Pierre-Jules Hetzel]]
| release_date  = 1888
| english_pub_date = 1889
| media_type    = Print ([[Hardcover|Hardback]])
| pages         = 
| preceded_by   = [[The Flight to France]]
| followed_by   = [[Family Without a Name]]
}}

'''''Two Years' Vacation''''' ({{lang-fr|Deux ans de vacances}}) is an adventure novel by [[Jules Verne]], published in [[1888 in literature|1888]]. The story tells of the fortunes of a group of schoolboys stranded on a deserted island in the [[Oceania|South Pacific]], and of their struggles to overcome adversity. In his preface to the book, Verne explains that his goals were to create a [[Robinson Crusoe]]-like environment for children, and to show the world what the intelligence and bravery of a child was capable of when put to the test.

== Publication ==
As with most of Verne's works, it was serialised (in twenty-four parts between January and December 1888) in the "[[Voyages Extraordinaires|Extraordinary Journeys]]" section of the French ''Magasin d’Éducation et de Récréation'' by Parisian publisher [[Pierre-Jules Hetzel|Hetzel]]. It was also published in book form in two volumes in June and early November of that year. An illustrated double volume with a colour map and a preface by Verne was released in late November.

=== Translations and adaptations===
* An English translation of the book was serialised in 36 installments in the ''[[Boy's Own Paper]]'' between 1888 and 1889.
* In 1889 a two-volume [[English language|English-language]] book titled ''A Two Year's Vacation'' was published by Munro in the United States. Later the same year, a single-volume abridged edition in the United Kingdom was released by Sampson Low under the title of ''Adrift in the Pacific''.
* In 1890, from February 22 through March 14, the ''Boston Daily Globe'' newspaper serialized ''Adrift in the Pacific; the Strange Adventures of a Schoolboy Crew''.
* In 1965 the I. O. Evens version of the Sampson Low translation was published in England (ARCO) and the U.S. (Associated Publishers) in two volumes: ''Adrift in the Pacific'' and ''Second Year Ashore''.
* In 1967 a new modified and abridged translation by Olga Marx with illustrations by [[Victor Ambrus]] titled ''A Long Vacation'' was published by Oxford University Press in the United Kingdom and Holt, Rinehart & Winston in the United States.
* In 1967 Czech filmmaker [[Karel Zeman]] made a live-action/animated film adaptation under the title ''Ukradená vzducholod'' ("The Stolen Airship", released world-wide<ref>http://www.imdb.com/title/tt0062412/releaseinfo?ref_=tt_dt_dt#akas</ref> as ''[[Two Years' Vacation (film)|Two Years' Vacation]]''), loosely based on Jules Verne's novels ''Two Years' Vacation'' and ''[[The Mysterious Island]]''.
* In 1969 an Australian film produced, directed and written by [[Mende Brown]] entitled ''[[Strange Holiday (1970 film)|Strange Holiday]]'' credited Jules Verne for the story.<ref>http://www.imdb.com/title/tt0065040/</ref>
* The 1974 four-part T.V. series ''Deux ans de vances'' was produced in a cooperation of French, Belgian, Swiss, West-German and Romanian television.<ref>http://www.imdb.com/title/tt0071076/releaseinfo?ref_=tt_dt_dt#akas</ref>
* In 1982 a Japanese studio [[Toei Animation]] made an anime adaptation under the title of ''Adrift in the Pacific'' ({{lang-ja|十五少年漂流記}}).
* In 1987 a made-for-TV animation was produced by the Japanese studio [[Nippon Animation]] under the title of ''The Story of Fifteen Boys'' ({{lang-ja|十五少年漂流記}}).
* The book became  the story for different anime series like [[Ginga Hyōryū Vifam]], [[Jura Tripper|Kyōryū Bōkenki Jura Tripper]], [[Infinite Ryvius]], and [[Mujin Wakusei Survive]].

==Plot summary==
[[Image:'Two Years' Vacation' by Léon Benett 39.jpg|thumb|Map of "Chairman Island"]]
The story takes place in March, 1860 and opens with a group of schoolboys aged between eight and fourteen on board a 100-ton [[schooner]] called the ''Sleuth'' moored at [[Auckland]], New Zealand, and preparing to set off on a six-week vacation. With the exception of the oldest boy Gordon, an American, and Briant and Jack, two French brothers, all the boys are British.

While the schooner's crew are ashore, the moorings are cast off under unknown circumstances and the ship drifts to sea, where it is caught by a storm. Twenty-two days later, the boys find themselves cast upon the shore of an uncharted island, which they name "Chairman Island." They go on many adventures and even catch wild animals while trying to survive. They remain there for the next two years until a passing ship sinks in the close vicinity of the island. The ship had been taken over by [[mutiny|mutineer]]s, intent on trafficking slaves. With the aid of two of the surviving members of the original crew, the boys are able to defeat the mutineers and make their escape from the island, which they find out is close to the [[Chile]]an coast ([[Hanover Island|Hanover-Island]] located at 50°56’ S, 74°47’ W).

The struggles for survival and dominance amongst the boys were to be echoed in [[William Golding]]'s ''[[Lord of the Flies]]'', written some 66 years later.

== In popular culture ==
''Deux ans de vacances'' is the first book Shiori Shiomiya reads from the shelves of the school library during a flashback to her childhood in the [[anime]] ''[[The World God Only Knows]]''.

==Notes==
{{reflist}}

== References ==
*{{cite web |vauthors=Dehs V, Margot JM, Har'El Z | title=The Complete Jules Verne Bibliography| url=http://jv.gilead.org.il/biblio/voyages.html#DV | accessdate=18 March 2006}}

==External links==
{{wikisource|Two Years' Vacation}}
{{commonscat-inline|Two Years' Vacation}}
*{{Internet Archive|id=adriftinpacific01verngoog|name=Adrift in the Pacific (1889; abridged version)}}

{{Verne}}
{{Authority control}}

[[Category:1888 novels]]
[[Category:Novels by Jules Verne]]
[[Category:Novels set in New Zealand]]
[[Category:Novels set in the Pacific Ocean]]
[[Category:Novels set on islands]]
[[Category:Castaways in fiction]]
[[Category:1860 in fiction]]
[[Category:1861 in fiction]]
[[Category:1862 in fiction]]
[[Category:French novels adapted into films]]
[[Category:Novels adapted into television programs]]