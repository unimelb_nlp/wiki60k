{{Infobox military person
|name=Peter Strasser
|birth_date= 1 April 1876
|death_date= {{death date and age|1918|8|5|1876|4|1|df=y}}
|image=Peterstrasserwwi.png
|caption=Peter Strasser during WWI
|nickname=
|birth_place= [[Hanover]], [[Kingdom of Prussia]]
|death_place=  near [[Wells-next-the-Sea]], [[Norfolk]], [[UK]]
|allegiance= {{flag|German Empire}}
|branch= {{navy|German Empire}}
|serviceyears= 1891–1918
|rank=[[Commander|Fregattenkapitän]]
|unit=Führer der Luftschiffe (F.d.L.)
|commands=''Marine-Luftschiff-Abteilung''
|battles=[[World War I]] 
|awards=[[Pour le Mérite]]<br>[[Iron Cross]] First class
|relations=
|laterwork=
}}

'''Peter Strasser''' (1 April 1876 &ndash; 5 August 1918) was chief commander of [[German Imperial Navy]] [[Zeppelin]]s during [[World War I]], the main force operating bombing campaigns from 1915 to 1917. He was killed when flying the war's last airship raid over Great Britain.

==Early career==
Strasser was born in [[Hanover]], [[Germany]], on 1 April 1876. At the age of 15, he joined the German Imperial Navy (''[[Kaiserliche Marine]]''). After serving on board  SMS ''Stein'' and SMS ''Moltke'', he entered the Naval academy in Kiel. He quickly rose through the ranks and was promoted to [[Lieutenant]] in 1895. He served  on board SMS ''Mars'', SMS ''Blücher'', SMS ''Panther'', SMS ''Mecklenburg'' and SMS ''Westfalen'' from 1897 to 1902. He was an excellent gunnery officer and was placed in the [[German Imperial Naval Office]] (''[[Reichsmarine-Amt]]'') in charge of German shipboard and coastal artillery. In September 1913,<ref name=Castle/> he took command of the Naval Airship Division (''[[Marine-Luftschiff-Abteilung]]''{{r|Urban}}). Airships were as yet an unproven technology and ''Korvettenkapitän'' Strasser became the new naval airship chief after his predecessor, ''Korvettenkapitän'' [[Friedrich Metzing]],<ref name=Castle/> drowned in the crash of the very first naval airship, the [[List of Zeppelins#LZ14|L 1]]. Also the single remaining naval airship [[List of Zeppelins#LZ 18|L 2]] was soon lost in another fatal accident.<ref name=Lehmann5/> Strasser completed theoretical studies on airships and gained practical experience piloting the civilian airship [[List of Zeppelins#LZ 17|LZ 17 ''Sachsen'']]. Another airship, [[Zeppelin LZ13|LZ 13 ''Hansa'']] was chartered to train naval crews while new ships were being built. At the start of the war Navy had only one airship operational, the [[List of Zeppelins#LZ 24|LZ 24 (Navy designation L 3)]]. L 3, under Strasser's personal command, was the only one to participate in the Imperial Navy manoeuvres just before the war.

==First World War==
Following the outbreak of [[World War I]] in August 1914, Navy airships were initially confined to anti-submarine, anti-mine and scouting missions. They served in the [[Battle of Heligoland (1914)|Battle of Heligoland Bight]].

However, on 19&ndash;20 January 1915, L3 and L4 participated in the first bombing raids over [[England]], attacking [[Great Yarmouth]], [[Sheringham]] and [[King's Lynn]]. Over the next 3 years, bombing campaigns would be launched primarily against Britain, but also on [[Paris]] and other cities and ports. Strasser would participate in the England raids at least once a month.<ref name=Lehmann5/> He decided to test the newly developed [[spy basket]] himself, and almost fell out when it became entangled with the [[Zeppelin]]'s aerial.<ref name=Lehmann5/> Initially, bombing was limited to military targets but with great lobbying support of ''Konteradmiral'' [[Paul Behncke]],<ref name=Castle/> the Kaiser approved attacks against civilian targets. Official British estimates list 498 civilians and 58 soldiers killed by air attack in Britain between 1915 and 1918. 1,913 injuries are recorded. The Imperial Navy dropped 360,000&nbsp;kg of bombs, the majority on the British Isles. 307,315&nbsp;kg were directed at enemy vessels, ports and towns; 58,000&nbsp;kg were dropped over Italy, the Baltic and the Mediterranean. German army airships carried 160,000&nbsp;kg of bombs to their designated targets: 44,000&nbsp;kg hit Belgium and France, 36,000&nbsp;kg England, and 80,000&nbsp;kg Russia and south eastern Europe. However, questions remain over whether airships (and more importantly, their irreplaceable crews) would have been better used as a purely naval weapon.

''Vizeadmiral'' [[Reinhard Scheer]] became Strasser's superior in January 1916, and tried unsuccessfully to tame Strasser's aggressive pursuit of independence.<ref name=Castle/> On 28 November,{{citation needed|date=May 2011}} 1916, Strasser was appointed by imperial decree as "Leader of Airships" (''Führer der Luftschiffe''; ''F.d.L.'').{{clarify|date=May 2011|reason=Was it a rank or a post? Most sources claim he was still Fregattenkapitan.}}

==Death in the last raid over Great Britain==
Strasser did not live to see the end of the war. On 5 August 1918,<ref name=Lehmann5/> during a  night raid against [[Boston, Lincolnshire|Boston]], [[Norwich]], and the Humber Estuary, Strasser's [[List of Zeppelins#LZ 112|L 70]] met a British reconnaissance [[D.H.4]].<ref name=l>{{cite book|last=Lawson|first=Eric|title=The first air campaign, August 1914-November 1918|year=1996|publisher=Da Capo Press|location=Cambridge, MA|isbn=0-306-81213-4|pages=79–80|url=https://books.google.com/books?id=9PGHckhHiX0C&lpg=PA213&dq=Zeppelin%20reconnaissance%20raids%20missions&pg=PA79#v=onepage&q&f=false|author2=Lawson, Jane}}</ref> Pilot Major [[Egbert Cadbury]] and Gunner Major [[Robert Leckie (aviator)|Robert Leckie]] shot down the L 70 just north of [[Wells-next-the-Sea]] on the [[Norfolk]] coast. None of the 23 men aboard survived. It proved to be the last airship raid over Great Britain.<ref name=l/>

==Legacy==
Strasser's impact on both the war and history was important for the future [[air warfare]]. He was instrumental in the development of long range bombing and the development of the rigid airship as an efficient, high altitude, all-weather aircraft. He was a major proponent of the doctrine of bombing attacks on civilian as well as military targets to serve both as propaganda and as a means of diverting resources from the front line.

{{quote|We who strike the enemy where his heart beats have been slandered as 'baby killers' ... Nowadays, there is no such animal as a noncombatant. Modern warfare is [[total warfare]].|Peter Strasser<ref name=l/>}}

One possible but unconfirmed name for [[Flugzeugträger B]], the unfinished [[sister ship]] of the World War II [[German aircraft carrier Graf Zeppelin|German aircraft carrier ''Graf Zeppelin'']], was ''Peter Strasser.''

==Awards==
* [[Iron Cross]] of 1914, 1st and 2nd class
* [[Pour le Mérite]] (20 August 1917)
* [[Hanseatic Cross]] of Hamburg
* Knight's Cross of the Royal [[House Order of Hohenzollern]]
* [[Order of the Red Eagle]], 4th class
* Lifesaving Medal at the band

==See also==
*[[Kaiserliche Marine]]
*[[Schütte-Lanz]]
*[[Total war]]
*[[Zeppelin]]
*[[List of Zeppelins]]

==References==
{{Reflist|

<ref name=Castle>{{cite book|last=Castle|first=Ian|title=London 1914-17 : the Zeppelin menace|year=2008|publisher=Osprey|location=Oxford|isbn=1-84603-245-8|pages=11–12|url=https://books.google.com/books?id=JySSFSaXOcQC&lpg=PA12&dq=luftschiff%20Metzing&pg=PA12#v=onepage&q&f=false|edition=1. publ. in Great Britain}}</ref>

<ref name=Lehmann5>[[Ernst A. Lehmann|Lehmann, Ernst A.]]; Mingos, Howard. 1927. {{lang|en|''The Zeppelins. The Development of the Airship, with the Story of the Zepplins Air Raids in the World War.''}} [http://www.hydrogencommerce.com/zepplins/zeppelin5.htm Chapter V (online)] Published by I. H. SEARS & COMPANY, Inc. New York</cite> ([http://www.hydrogencommerce.com/zepplins/zepplins.htm#The%20Zeppelins All online chapters - I to VII])</ref>

<ref name=Urban>{{cite book|last=Urban|first=Heinz|title=Zeppeline der kaiserlichen Marine, 1914-1918|year=2008|publisher=Masuren|location=Meersburg|isbn=3-00-022731-8|edition=Dt. Orig.-Ausg., 1. Aufl.|page=560|language=German}}</ref>

}}

{{Authority control}}

{{DEFAULTSORT:Strasser, Peter}}
[[Category:1876 births]]
[[Category:1918 deaths]]
[[Category:People from Hanover]]
[[Category:People from the Province of Hanover]]
[[Category:Military aviation leaders of World War I]]
[[Category:German military personnel killed in World War I]]
[[Category:Aviators killed by being shot down]]
[[Category:Recipients of the Pour le Mérite (military class)]]
[[Category:Recipients of the Hanseatic Cross (Hamburg)]]
[[Category:Knights of the House Order of Hohenzollern]]
[[Category:Recipients of the Order of the Red Eagle, 4th class]]
[[Category:Imperial German Navy personnel]]
[[Category:German airship aviators]]