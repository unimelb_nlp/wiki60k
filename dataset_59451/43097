<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Leduc RL.21 
 | image=Leduc RL.21 1967.png
 | caption=RL.21 with (from left) Prosper (engineer), R. Leduc (designer) and R. Davy (pilot)
}}{{Infobox Aircraft Type
 | type=Class speed record setter
 | national origin=[[France]]
 | manufacturer=
 | designer=[[René Leduc]]
 | first flight=August 1960
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=1
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Leduc RL.21''' was a single engine, single seat light aircraft built in the late 1950s in [[France]].  Designed to achieve high speeds from modest engine power, it set seven class records in the early 1960s.

==Design and development==
Between 1939 and 1975 the amateur aircraft designer and builder [[René Leduc]] (not the [[ramjet]] designer of the same name) completed and flew five different lightplanes, two of which set records in their class.<ref name=af/><ref name=Flight60/><ref name=Flight67/>  The RL.21 was a [[low wing]] [[monoplane]] with its wings braced to the upper [[fuselage]] by a pair of inverted-V [[strut]]s. It was powered by a {{convert|135|hp|kW|abbr=on|disp=flip|0}} [[SNECMA Régnier 4L|SNECMA-Régnier 4L-00]] four cylinder inverted air-cooled inline engine driving a two blade [[propeller (aeronautics)|propeller]].  A low, smoothly streamlined [[aircraft canopy|canopy]] covered the single seat [[cockpit]] and merged aft into a raised rear fuselage.  The [[empennage]] was conventional.  The RL.21 had a [[landing gear|tailwheel undercarriage]] with [[aircraft fairing|spatted]] main wheels on [[cantilever]] legs, mounted on the wings approximately below the ends of the wing struts.<ref name=GaillardI/>

Leduc built the RL.21 over a period of six years with assistance from [[Sud Aviation]] and from the [[Nantes]] Technical School. It flew for the first time in August 1960.<ref name=GaillardI/> Between late 1960 and early 1967 it set seven records in its C1a and C1b categories,<ref name=Flight67/> respectively for aircraft with maximum take-off weights below {{convert|500|kg|lbs|abbr=on|0}} and between {{convert|500-1000|kg|lbs|abbr=on|0}}.<ref name=Flight60/>  The earliest were for speeds of {{convert|313.5|km/h|mph|abbr=on|1|}} and {{convert|316.2|km/h|mph|abbr=on|1|}} for the two classes around a {{convert|100|km|mi|abbr=on|0}} closed circuit <ref name=Flight60/> and the last at {{convert|217|mph|km/h|abbr=on|1|disp=flip}} and {{convert|208|mph|km/h|abbr=on|1|disp=flip}} over a {{convert|500|km|mi|abbr=on|0}} closed circuit.<ref name=Flight67/> Throughout these flights the pilot was Raymond Davy.<ref name=GaillardI/>

==Specifications==
{{Aircraft specs
|ref=Gaillard (1990) p.209<ref name=GaillardI/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|capacity=One
|length m=6.90
|length note=
|span m=5.70
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=6.5
|wing area note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=390
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=500
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=SNECMA-[[Régnier 4L]]-00
|eng1 type=4 cylinder inverted air-cooled inline
|eng1 hp=135
|eng1 note=
|power original=
|more power=

|prop blade number=2
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=360
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=8000
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=

|more performance=

}}

==References==
{{reflist|refs=

<ref name=GaillardI>{{cite book |title=Les Avions Francais de 1944 à 1964|last=Gaillard|first=Pierre|year=1990|publisher=Éditions EPA|location=Paris|isbn=2 85120 350 9|page=209}}</ref>

<ref name=Flight60>{{cite magazine |last= |first= |authorlink= |coauthors= |date=9 December 1960 |title= Sport and Business|magazine= [[Flight International|Flight]]|volume=78 |issue=2700 |page=700  |url= http://www.flightglobal.com/pdfarchive/view/1960/1960%20-%202894.html}}</ref>

<ref name=Flight67>{{cite magazine |last= |first= |authorlink= |coauthors= |date=12 January 1967|title=Sport and Business|magazine= [[Flight International|Flight]]|volume=91 |issue=3018 |page=52 |url=http://www.flightglobal.com/pdfarchive/view/1967/1967%20-%200054.html}}</ref>

<ref name=af>{{cite web |url=http://www.aviafrance.com/aviafrance1.php?ID=9884&ID_CONSTRUCTEUR=1475&ANNEE=0&ID_MISSION=0&MOTCLEF=|title=Leduc R-21 |author= |date= |work= |publisher= |accessdate=20 January 2015}}</ref>

}}
<!-- ==Further reading== -->
<!-- ==External links== -->



[[Category:French sport aircraft 1960–1969]]
[[Category:Leduc aircraft|RL.21]]