{{Use dmy dates|date=October 2010}}
{{moresources|date=April 2016}}

{{Infobox Union
|name = Royal College of Nursing
|country = [[United Kingdom]]
|affiliation = [[International Council of Nurses]]
|members = 432,000+ (2015)
|full_name = Royal College of Nursing
|native_name =
|image = [[File:Rcn-logo.png|200px]]
|founded = 1916
|current =
|head =
|office = London, United Kingdom
|people = [[Cecilia Anim]], President<br>[[Janet Davies (nurse)|Janet Davies]], General Secretary<br>[[Michael Brown (nurse)|Michael Brown]], Chair of Council
|website = {{URL|http://www.rcn.org.uk}}
}}

The '''Royal College of Nursing''' ('''RCN''') is a membership organisation and [[trade union]] with over 432,000 members in the [[United Kingdom]].  It was founded in 1916, receiving its [[royal charter]] in 1928. [[Majesty|Her Majesty]] [[Queen Elizabeth II]] is the patron. The majority of members are [[registered nurse]]s; however student nurses and [[healthcare assistant]]s are also members.

The RCN describes its mission as representing nurses and [[nursing]], promoting excellence in practice and shaping [[health]] policies. It has a network of stewards, [[safety]] representatives and union learning representatives as well as advice services for members. Services include a main [[library]] in [[London]] and regional libraries around the country. The RCN Institute also provides courses for nurses.

==History==
In 1916 the College of Nursing Ltd was founded with 34 members as a professional organisation for trained nurses.<ref>{{cite web |url=http://www.rcn.org.uk/aboutus/our_history |title=About us: our history |publisher=RCN |accessdate=27 July 2014}}</ref> A Royal Charter was granted in 1928 and the organisation became the College of Nursing.<ref>{{cite web|url=http://www.rcn.org.uk/aboutus/our_constitutional_documents|title=About us: our constitutional documents|publisher=RCN|accessdate=27 July 2014}}</ref> In 1939 the college's name was changed to the Royal College of Nursing. Since 1977 the RCN has also been registered as a trade union.<ref>{{cite web|url=http://www.rcn.org.uk/aboutus/our_structure|title=About us: our structure|publisher=RCN|accessdate=27 July 2014}}</ref>

In 2016 the college is celebrating 100 years since its foundation. It is undertaking a number of activities to celebrate the centenary, including a photography competition and special events.<ref>{{Cite web|title=#RCN100|url=https://www.rcn.org.uk/centenary|website=The Royal College of Nursing|accessdate=8 February 2016}}</ref>

==Offices==
[[File:Rcnhq.JPG|thumb|RCN HQ, Cavendish Square London]]

===Headquarters===
The headquarters are at 20 Cavendish Square, London, a [[Grade II listed]] building<ref>{{cite web|title=20 Cavendish Square|work=Images of England|url=http://www.imagesofengland.org.uk/details/default.aspx?pid=2&id=209054|accessdate=30 June 2006}}</ref> which was built as a substantial town house in 1729 and became the residence of British Prime Minister [[H. H. Asquith]]. The building was refronted and incorporated by architect [[Edwin Cooper (architect)|Edwin Cooper]] in 1930 into his redevelopment of the corner site with Henrietta Place.{{cn|date=April 2016}}

===Regional Offices===
The RCN has offices throughout the UK. In England regional offices are located in [[Birmingham]], [[Bolton]], [[Bury St Edmunds]], [[Croydon]], [[Exeter]], [[Newbury, Berkshire|Newbury]], [[Nottingham]], [[City of Leeds|Leeds]], and [[City of Sunderland|Sunderland]]. The [[Northern Ireland]] office is in [[Belfast]]. The [[Scotland|Scottish]] offices are located in [[Aberdeen]], [[Edinburgh]] and [[Glasgow]]; and the [[Wales|Welsh]] offices are located in [[Cardiff]] and [[Conwy]].{{Citation needed|date=August 2012}}

==Council==
{{unsourced|section|date=April 2016}}
The RCN is governed by its Council. Council members are guardians/trustees of the organisation's mission and values on behalf of the members. They are also charity trustees and carry legal duties and responsibilities laid down by charity law. The Council is responsible for the overall governance of the RCN, and has ultimate responsibility for the sustainability and the finances of the organisation.{{cn|date=April 2016}}

The Council is made up of 31 Council members: two elected by each of the 12 geographical sections (Scotland, Wales, Northern Ireland and 9 English), two elected by student members (RCN Students), two elected by HCA members (RCN HCA), the RCN President and Deputy President, elected by all members, and the Chair of RCN Congress (non-voting), who is elected by Congress voting entities.{{cn|date=April 2016}} The RCN's General Secretary is appointed by Council. Council members are not paid to serve on Council but voluntarily give up their time to serve the RCN and its members, in their governance role. The current{{when|date=July 2016}} Council Chair is Michael Brown, the Vice Chair is Lors Alford.{{cn|date=April 2016}}

==Presidents==
[[File:Andrea Spyropoulos at RCN AGM.jpg|thumb|upright=0.56|Previous RCN President, [[Andrea Spyropoulos]], at the 2010 AGM]]
{{columns-list|colwidth=30em|
* 1922–1925 Dame [[Sidney Browne]] {{postnominals|country=GBR|GBE|RRC}}
* 1925–1927 Dame [[Sarah Swift]] {{postnominals|country=GBR|GBE|RRC}}
* 1927–1929 [[Annie Warren Gill]] {{postnominals|country=GBR|RRC}}
* 1929–1930 [[Rachael Cox-Davies]] {{postnominals|country=GBR|CBE|RRC}}
* 1930–1933 [[Mary Sparshott|Mary E. Sparshott]] {{postnominals|country=GBR|CBE|RRC}}
* 1933–1934 [[Edith MacGregor Rome]] {{postnominals|country=GBR|RRC}}
* 1934–1935 [[Rachael Cox-Davies]] {{postnominals|country=GBR|CBE|RRC}}
* 1935–1937 [[Dorothy Coode|Dorothy S. Coode]] {{postnominals|country=GBR|OBE}}
* 1937–1938 [[Edith MacGregor Rome]] {{postnominals|country=GBR|RRC}}
* 1938–1940 [[Beatrice Marsh Monk]] {{postnominals|country=GBR|CBE|RRC}}
* 1940–1942 [[Mary Jones (nurse)|Mary Jones]] {{postnominals|country=GBR|OBE|ARRC}}
* 1942–1944 [[Emily MacManus|Emily E. P. MacManus]] {{postnominals|country=GBR|OBE}}
* 1944–1946 [[Mildred Hughes (nurse)|Mildred F. Hughes]] 
* 1946–1948 [[Gladys Hillyers|Gladys V. L. Hillyers]] {{postnominals|country=GBR|OBE}}
* 1948–1950 Dame [[Louisa Wilkinson]] {{postnominals|country=GBR|DBE|RRC}}
* 1950–1952 [[Lucy Duff-Grant]] {{postnominals|country=GBR|RRC}}
* 1952–1954 [[Lucy Ottley|Lucy J. Ottley]] 
* 1954–1956 [[Sybil Bovill|Sybil C. Bovill]] 
* 1956–1958 [[Gertrude Godden (nurse)|Gertrude M.Godden]] {{postnominals|country=GBR|OBE}}
* 1958–1960 [[Marjorie Marriott|Marjorie J. Marriott]] {{postnominals|country=GBR|OBE}}
* 1960–1962 [[Margaret Smith (nurse)|Margaret J. Smith]] {{postnominals|country=GBR|CBE}}
* 1962–1963 [[Marjorie Marriott|Marjorie J. Marriott]] {{postnominals|country=GBR|OBE}}
* 1963–1964 [[Mabel Gordon Lawson]] {{postnominals|country=GBR|OBE}}
* 1964–1966 [[Florence Udell]] {{postnominals|country=GBR|CBE}}
* 1966–1968 [[Theodora Turner]] {{postnominals|country=GBR|OBE|ARRC}}
* 1968–1972 [[Mary Blakeley]] {{postnominals|country=GBR|OBE}}
* 1972–1976 Dame [[Winifred Prentice]] {{postnominals|country=GBR|DBE}}
* 1976–1980 [[Eirlys Rees|Eirlys M Rees]] {{postnominals|country=GBR|CBE}}
* 1981–1982 [[Marian Morgan|Marian K. Morgan]] 
* 1982–1987 Dame [[Sheila Quinn]] {{postnominals|country=GBR|DBE|FRCN}}
* 1988–1990 [[Maude Storey]] {{postnominals|country=GBR|CBE|CstJ|FRCN}}
* 1990–1994 Professor Dame [[June Clark (nurse)|June Clark]] {{postnominals|country=GBR|DBE|FRCN}}
* 1994–1998 Professor Dame [[Betty Kershaw]] {{postnominals|country=GBR|DBE|FRCN}}
* 1999–2000 [[Christine Watson]]
* 2000–2002 [[Roswyn Hakesley-Brown]]
* 2002–2006 [[Sylvia Denton]] {{postnominals|country=GBR|OBE|FRCN}}
* 2006–2010 [[Maura Buchanan]]
* 2010–2014 [[Andrea Spyropoulos]]
* 2015–<sup>'''present'''</sup>   [[Cecilia Anim]]
}}

==Secretaries==
{{columns-list|colwidth=30em|
* 1916-1935 [[Mary Snell Rundle]] {{postnominals|country=GBR|RRC}}
* 1935-1957 [[Frances G. Goodall]] {{postnominals|country=GBR|OBE}}
* 1957-1982 Dame [[Catherine M. Hall]] {{postnominals|country=GBR|DBE|FRCN}}
* 1982-1989 [[Trevor Clay]] {{postnominals|country=GBR|CBE|FRCN}}
* 1989-2001 [[Christine Hancock]] 
* 2001-2007 [[Beverly Malone]] 
* 2007-2015 [[Peter Carter (nurse)|Peter Carter]] {{postnominals|country=GBR|OBE|FRCGP}}
* 2015-<sup>'''present'''</sup> [[Janet Davies (nurse)|Janet Davies]]
}}

==Members==
The RCN is  a membership organisation and a trade union with over 435,000 members. Nursing students may join at reduced fees. Following the announcement of the removal of NHS Student bursaries in November 2015 the RCN initiated its support{{clarifyme|date=July 2016}} through the campaign "Nursing counts".<ref>{{Cite web|title=Nursing Counts|url=https://www.rcn.org.uk/nursingcounts|website=The Royal College of Nursing|accessdate=9 February 2016}}</ref>

==Events==
RCN holds events nationwide throughout the year, including branch events, educational events and the annual Congress and AGM. The annual congress aims to help members to meet to learn, develop and share nursing practice; members help to inform the RCN agenda and influence nursing and health policy through debate. In 2016 Congress will be in Glasgow.<ref>{{Cite web|title=About the RCN|url=https://www2.rcn.org.uk/newsevents/congress/2016/about|website=rcn.org.uk|date=31 July 2015|accessdate=8 February 2016}}</ref>

==RCN libraries==
The RCN Library claims to be Europe's largest nursing-specific collection.<ref name=":0">{{Cite web|url=https://www.rcn.org.uk/library|title=RCN Library|accessdate=7 April 2016|website=rcn.org.uk|publisher=RCN}}</ref> The RCN has four libraries throughout the [[United Kingdom]]: one in [[Northern Ireland]] (a region) and one in each constituent country of the United Kingdom. The libraries are located in [[Belfast]], [[Cardiff]], [[Edinburgh]] and [[London]].

The London Library, which is now known as the UK Library, was founded in 1921, and its contents include 60,000 volumes, 500 videos and 400 current periodicals on nursing and related subjects. The catalogue, with information on over 600m records, is now online.<ref>{{cite web|title=About the catalogue|url=http://archives.rcn.org.uk/CalmView/Aboutcatalogue.aspx|website=archives.rcn.org.uk|publisher=Royal College of Nursing|accessdate=26 August 2016}}</ref> Due to its historical holdings, the Library is a member of [[The London Museums of Health & Medicine]] group.<ref>{{cite web|title=Medical Museums|url=http://medicalmuseums.org/|website=medicalmuseums.org|accessdate=26 August 2016}}</ref> Special collections include the Historical Collection and the RCN Steinberg Collection of Nursing Research, the latter of which comprises over 1,000 nursing theses and dissertations. Set up in 1974, the RCN Steinberg Collection of Nursing Research contains a selection of influential nursing theses and dissertations from the early 1950s to the present day.<ref>{{cite web|title=Permanent Collections|url=https://www.rcn.org.uk/library/collections/permanent-collections|website=Royal College of Nursing|publisher=Royal College of Nursing|accessdate=26 August 2016}}</ref>

==Fellowships==
The RCN awards Fellowships for exceptional contributions to nursing. Honorary Fellowships can also be granted by RCN Council to those who are unable to become an RCN member, either because they are from overseas or because they work outside the nursing profession. Fellows and Honorary Fellows are entitled to the postnominal FRCN. <ref>{{cite web|title=RCN Fellowship and Honorary Fellowship Roll of Honour|url=https://www.rcn.org.uk/get-involved/rcn-awards/rcn-fellowship-and-honorary-fellowship-awards|publisher=Royal College of Nursing|date=2016|accessdate=3 November 2016}}</ref>

==RCN Publications==
RCN Publishing (branded as RCNi since March 2015) produces ''RCN Bulletin'', a monthly member publication, and ''[[Nursing Standard]]'', which is available through subscription and on news stands. It also publishes a range of journals for specialist nurses: ''[[Cancer Nursing Practice]]'', ''[[Emergency Nurse (magazine)|Emergency Nurse]]'', ''[[Learning Disability Practice]]'', ''[[Mental Health Practice]]'', ''Nursing Children and Young People'', ''Nursing Management'', ''[[Nursing Older People]]'', ''[[Nurse Researcher]]'', and ''[[Primary Health Care (journal)|Primary Health Care]]''.

===RCNi Nurse Awards===
Formerly the Nursing Standard Nurse Awards, the annual RCNi Nurse Awards celebrate nursing in different categories. The Awards take place in England, Scotland, Wales and Northern Ireland. There is a category for Nursing Students.<ref>{{Cite web|title=Nursing Student Award|url = https://www2.rcn.org.uk/aboutus/wales/wales_nurse_of_the_year/nurse-of-the-year-2015/nursing-student-award|website=rcn.org.uk|date=14 April 2015|accessdate=9 February 2016|first=John|last=Hoddinott}}</ref>

==Campaigns==
As of February 2016 the RCN is currently running the 'Nursing Counts' campaign.<ref>{{Cite web|title=Nursing Counts|url=https://www.rcn.org.uk/nursingcounts|website=Royal College of Nursing|accessdate=8 February 2016}}</ref> The Nursing Counts campaign is a broad campaign encompassing fair pay and conditions and the removal of the NHS student bursary which will be removed from 2017.

Previous campaigns have been – What if?<ref>{{Cite web|title=What If|url=http://whatif.rcn.org.uk|website=whatif.rcn.org.uk|accessdate=8 February 2016}}</ref> & 'Nursing the Future' 2004<ref>{{Cite web|title=Campaigns> Re-branding Nursing (RCNi)|url=http://journals.rcni.com/page/ns/campaigns/re-branding-nursing|website=journals.rcni.com|accessdate=8 February 2016}}</ref>

==RCN Foundation==
{{unsourced|section|date=April 2016}}
On 1 April 2010 the RCN announced the launch of the RCN Foundation – an independent charity to support nursing and improve the health and wellbeing of the public. The new foundation will undertake a number of activities including giving grants for improving nursing practice through activities that, for example, support the development of clinical practice and improve the quality and standard of patient care and experience.

==References==
{{Reflist|30em}}

==Further reading==
*{{cite book|author=Bowman, Gerald|year=1967|title=The Lamp and the Book: The Story of the RCN 1916–1966|location=London|publisher=Queen Anne Press}}

==External links==
{{Commons category}}
{{Nursing in the United Kingdom}}

{{DEFAULTSORT:Royal College Of Nursing}}
[[Category:Grade II listed buildings in the City of Westminster]]
[[Category:Health in the City of Westminster]]
[[Category:Medical associations based in the United Kingdom]]
[[Category:Medicine and healthcare trade unions]]
[[Category:Organisations based in the City of Westminster]]
[[Category:Trade unions established in 1916]]
[[Category:Nursing in the United Kingdom]]
[[Category:Nursing organizations]]
[[Category:Royal College of Nursing]]
[[Category:Royal Colleges]]

[[Category:1916 establishments in the United Kingdom]]
[[Category:Medical museums in London]]