{{for|the ship called SS Bowes Castle|SS Empire Advocate}}
{{good article}}

{{Infobox Military Structure
|name = Bowes Castle
|location = [[Bowes]], [[County Durham]], England
|coordinates = {{coord|54.5168|N|2.0139|W|type:landmark_region:GB|display=inline,title}}
|gridref = {{gbmapping|NY992135}}
|image = [[File:Bowes Castle - geograph.org.uk - 1060655.jpg|225px]]
|caption = Keep of Bowes Castle
|map_type = County Durham
|map_size = 200
|map_alt = 
|map_caption = Shown within [[County Durham]]
|type = 
|materials = Stone
|height = 
|condition = 
|ownership = [[English Heritage]]
|open_to_public = Yes
|battles = 
|events= [[Revolt of 1173–1174|Great Revolt of 1173-74]]
}}

'''Bowes Castle''' was a medieval [[castle]] in the village of [[Bowes]] in [[County Durham]], [[England]]. Built within the perimeter of the former [[Ancient Rome|Roman]] [[castra|fort]] of [[Lavatrae]], the early timber castle on the site was replaced by a more substantial stone structure between 1170 and 1174 on the orders of [[Henry II of England|Henry II]]. A planned village was built alongside the castle. Bowes Castle withstood Scottish attack during the [[Revolt of 1173–1174|Great Revolt of 1173-74]] but was successfully looted by rebels in 1322. The castle went into decline and was largely dismantled after the [[English Civil War]]. The ruins are now owned by [[English Heritage]] and run as a tourist attraction.

==History==
===12th century===
Bowes Castle was built within the ruins of the Roman fort of [[Lavatrae]].<ref>Creighton, p.40; Pounds, p.179; Butler, p.101.</ref> The route was one of the few upland passes to link England and Scotland and had remained strategically important during the medieval period.<ref>Pounds, p.179; Butler, p.101.</ref> The castle site lay within the [[Honour of Richmond]], a grouping of lands traditionally owned by the [[Count of Brittany|Counts of Brittany]] during the early medieval period, but the land itself was a [[demesne]] estate, owned by the Crown.<ref>Butler, p.101.</ref>

Around 1136, [[Alan de Bretagne, 1st Earl of Richmond|Alan de Bretagne]], the Count of Brittany, built a timber castle in the north-west corner of the old fort.<ref>Kenyon, p.36; ''[http://www.pastscape.org/hob.aspx?hob_id=981380 Bowes Castle]'', National Monuments Record, English Heritage, accessed 2 March 2012.</ref> The use made of the older Roman fortification at Bowes was similar to that at the nearby castles of [[Brough Castle|Brough]] and [[Malton Castle|Malton]].<ref>Creighton, p.40.</ref> This castle was inherited by his son, [[Conan IV, Duke of Brittany|Conan]], and when he in turn died in 1171, it was claimed by [[Henry II of England|Henry II]].<ref name="Kenyon, p.36">Kenyon, p.36.</ref> 

Royal concerns over security led to Henry II investing heavily in a new castle structure on the site between 1171 and 1174. It was unusual for a new royal castle to be built in this part of England during the 12th century, and Henry appears to have been driven by the military threat from Scotland before and during the [[Revolt of 1173–1174|Great Revolt]] of 1173 to 1174.<ref>Pettifer, p.288; Pounds, p.181.</ref> Henry II spent almost £600 on the castle between 1170 and 1187, most of it in the first few years, rebuilding the older structure under the supervision of the Count of Brittany's local tenants, Torfin, Osbert and Stephen of Barningham.<ref>Brown, p.119; Butler, p.102.</ref>{{#tag:ref|It is impossible to accurately compare 12th century and modern prices or incomes. For comparison, £600 represents around four times the typical average annual income for a typical 12th century baron.<ref>Pounds, p.147.</ref>|group="nb"}}

[[File:Bowes Keep.png|thumb|left|The plan of the [[keep|hall-keep]]]]
The rebuilt castle featured a [[keep|hall-keep]], an uncommon design in English castles; built of stone, this was a three-storied structure {{convert|82|ft}} long, {{convert|60|ft}} wide and {{convert|50|ft}} high.<ref>Pettifer, p.288, Mackenzie, p.211.</ref> Internally the keep was divided to form a long hall and a solar and was lit by large, rounded windows.<ref>Pettifer, p.288; Mackenzie, p.211.</ref> The keep had architectural similarities to various nearby castles in the region, but in particular to those at [[Middleham Castle|Middleham]] and [[Pendragon Castle|Outhgill]].<ref>Goodall, p.139; Brown, p.43.</ref> A ditch formed an inner defensive [[Bailey (fortification)|bailey]] around the keep, with the ramparts of the old fort forming a larger, outer bailey.<ref>Pettifer, pp.288-289; Creighton, p.40.</ref> A mill, then an essential part of any castle, was built by the [[River Greta, Durham|River Greta]] to supply flour for the garrison .<ref>Mackenzie,p.211.</ref> The village of Bowes was built after the castle and formed a planned site running up to the castle, complete with a church and a market place; this form of planned village is again unusual in England.<ref>Creighton, p.204.</ref>

In England, the Great Revolt against Henry's rule involved a coalition of rebel barons, bolstered by support from the King of Scotland and European allies. [[William the Lion]] pushed south from Scotland in 1173 and Bowes Castle was damaged in the raids; work was carried out in anticipation of further attacks the following year, including repairs to the chamber, gates and the construction of bulwarks around the keep.<ref name=VCH>''[http://www.british-history.ac.uk/report.aspx?compid=64715 Parishes: Bowes]'', A History of the County of York North Riding: Volume 1 (1914), pp. 42-49, accessed 29 January 2012.</ref> The next year William of Scotland directly besieged the castle, but was he forced to retreat after the arrival of a relief force led by Henry's illegitimate son [[Geoffrey (archbishop of York)|Geoffrey]], then the [[Bishop of Lincoln]].<ref name=EH>''[http://www.english-heritage.org.uk/daysout/properties/bowes-castle/history-and-research/ Bowes Castle]'', English Heritage, accessed 2 March 2012.</ref>

===13th - 14th centuries===
[[File:Bowes Castle 1785.jpg|thumb|Ruins of Bowes Castle depicted in 1785]]
Henry II was successful in quelling the Great Revolt, imprisoning William the Lion until a peace treaty was agreed, extending Henry's authority north into Scotland. In the subsequent years the security situation in the north of England improved significantly.<ref>Pettifer, p.288.</ref> King [[John of England|John]] gave control of Bowes Castle to [[Robert de Vieuxpont]], an important administrator in the north, in 1203 and he retained control of the fortification until 1228.<ref name=VCH/> John stayed there himself in 1206 and in 1212, and the castle was also used briefly to hold John's niece [[Eleanor, Fair Maid of Brittany|Eleanor of Brittany]], who had been placed under the custody of Vieuxpont.<ref name=VCH/> [[Henry III of England|Henry III]] granted it briefly to William de Blockley and Gilbert de Kirketon, until it was given to Duke [[Peter I, Duke of Brittany|Peter]] of Brittany in 1232, and then to [[William de Valence, 1st Earl of Pembroke|William de Valence]].<ref name=VCH/> In 1241 [[Peter II, Count of Savoy|Peter II]], the [[Count of Savoy]] was made the Earl of Richmond and was then given Bowes by the king.<ref name="Kenyon, p.36"/>

The castle remained in the hands of the Earls of Richmond until 1322, by when it was in a poor state of repair.<ref name=PettiferVCHEH>Pettifer, p.289; ''[http://www.british-history.ac.uk/report.aspx?compid=64715 Parishes: Bowes]'', A History of the County of York North Riding: Volume 1 (1914), pp. 42-49, accessed 29 January 2012; ''[http://www.english-heritage.org.uk/daysout/properties/bowes-castle/history-and-research/ Bowes Castle]'', English Heritage, accessed 2 March 2012.</ref> [[Edward II of England|Edward II]] then gave Bowes Castle to John de Scargill instead; the local tenants of the Earl of Richmond rebelled and attacked the castle.<ref name=PettiferVCHEH/> The lord of the castle was away at the time, and the attackers burnt part of a hall, drank four [[Tun (unit)|tun]]s of wine and stole armour, [[springald]]s and other goods.<ref>Pettifer, p.265.</ref> Conflict with Scotland led to further raids against the castle and the surrounding manor; the neighbouring fields were abandoned as a result and by 1340 the castle was in ruins and the manor worth nothing.<ref name=VCH/>

===Later history===
Still ruined, Bowes Castle was reclaimed by the Crown in 1361; between 1444 and 1471 it was controlled by the [[House of Neville|Neville family]], powerful regional landowners, before reverting to the Crown once again.<ref name="Kenyon, p.36"/> [[James I of England|James I]] sold the castle in the early 17th century and the remaining fortifications were dismantled in the mid-17th century after the [[English Civil War]].<ref>''[http://www.british-history.ac.uk/report.aspx?compid=64715 Parishes: Bowes]'', A History of the County of York North Riding: Volume 1 (1914), pp. 42-49, accessed 29 January 2012; ''[http://www.english-heritage.org.uk/daysout/properties/bowes-castle/history-and-research/ Bowes Castle]'', English Heritage, accessed 2 March 2012; Kenyon, p.36.</ref> By 1928, the castle was in a poor condition, with little interest being shown in it from locals or its owner, Lady [[Richard Curzon, 4th Earl Howe|Lorna Curzon-Howe]].<ref name=FryP15>Fry, p.15.</ref> Facing death duties on her estate, Curzon-Howe agreed to pass the castle into the care of the Office of Works in 1931.<ref name=FryP15/>

In the 21st century, the castle is controlled by [[English Heritage]] and operated as a tourist attraction.<ref name="Kenyon, p.36"/> The ruins of the keep survive, largely intact, and are protected as a Grade I [[listed building]] and as a [[scheduled monument]].<ref>''[http://www.gatehouse-gazetteer.info/English%20sites/983.html Bowes Castle]'', Gatehouse website, accessed 2 March 2012.</ref>

==See also==
*[[Castles in Great Britain and Ireland]]
*[[List of castles in England]]

==Notes==
{{reflist|group="nb"}}

==References==
{{reflist|3}}

==Bibliography==

* {{cite book
  | last = Brown
  | first = R. Allen
  | year = 1962
  | title = Castles and Landscapes: Power, Community and Fortification in Medieval England
  | edition = 
  | publisher = Batsford
  | location= London
  | oclc = 1392314
  | url = https://books.google.com/books?id=BXNnAAAAMAAJ
  }}
* {{cite book
  | last = Butler
  | first = Lawrence
  | editor-last=Liddiard
  | editor-first=Robert
  | coauthor =
  | year = 2003
  | chapter = The Origins of the Honour of Richmond and its Castles
  | title = Anglo-Norman Castles
  | edition = 
  | publisher = Boydell Press
  | location= Woodbridge, UK
  | isbn = 978-0-85115-904-1
  | url = https://books.google.co.uk/books?id=J5qe7uWeXYwC
  }}
* {{cite book
  | last = Creighton
  | first = Oliver Hamilton
  | year = 2005
  | title = Castles and Landscapes: Power, Community and Fortification in Medieval England
  | edition = 
  | publisher = Equinox
  | location= London
  | isbn = 978-1-904768-67-8
  | url = https://books.google.co.uk/books?id=rr-ixYkUVcoC
  }}
* {{cite book
  | last = Fry
  | first = Sebastion
  | year = 2002
  | title = A History of the National Heritage Collection: Volume 5, 1931-1945
  | publisher = English Heritage
  | location= Portsmouth, UK
  | issn = 2046-9799
  }}
* {{cite book
  | last = Goodall
  | first = John
  |authorlink=John Goodall (author) 
  | year = 2011
  | title = The English Castle
  | edition = 
  | publisher = Yale University Press
  | location= New Haven
  | isbn = 978-0-300-11058-6
  | url = https://books.google.com/books?id=n_6PHAAACAAJ
  }}
* {{cite book
  | last = Kenyon
  | first = Katy
  | year = 1999
  | title = Barnard Castle, Egglestone Abbey, Bowes Castle
  | edition = 
  | publisher = English Heritage
  | location= London
  | isbn = 1-85074-720-2
  | url = 
  }}
* {{cite book
  | last = Mackenzie
  | first = James D.
  | year = 1896
  | title = The Castles of England: Their Story and Structure, Vol II
  | edition = 
  | publisher = Macmillan
  | location= New York
  | oclc = 504892038
  | url = https://books.google.co.uk/books?id=aY9pQwAACAAJ&dq=%22mackenzie%22+%22story+and+structure%22&hl=en&ei=JaNBTaWLJ4mFhQeMyJjVAQ&sa=X&oi=book_result&ct=result&resnum=1&ved=0CCkQ6AEwAA
  }}
* {{cite book
  | last = Pettifer
  | first = Adrian
  | year = 2002
  | title = English Castles: A Guide by Counties
  | edition = 
  | publisher = Boydell Press
  | location= Woodbridge, UK
  | isbn = 978-0-85115-782-5
  | url = https://books.google.co.uk/books?id=47iheRUGKIEC
  }}
* {{cite book
  | last = Pounds
  | first = Norman John Greville
  | year = 1994
  | title = The Medieval Castle in England and Wales: A Social and Political History
  | edition = 
  | publisher = Cambridge University Press
  | location= Cambridge, UK
  | isbn = 978-0-521-45828-3
  | url = https://books.google.co.uk/books?id=d8babfRDfxwC
  }}

==External links==
{{Commons category|Bowes Castle}}
*[http://www.english-heritage.org.uk/daysout/properties/bowes-castle/ Bowes Castle], English Heritage

{{CastlesCDT&W}}
{{English Heritage properties in North East England}}


[[Category:Castles in County Durham]]
[[Category:English Heritage sites in County Durham]]
[[Category:Ruins in County Durham]]