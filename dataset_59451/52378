{{Infobox journal
| title = Nature Reviews Drug Discovery
| cover = [[File:Nature Review Drug Discovery.jpg|200 px]]
| discipline = [[Pharmacology]], [[biotechnology]]
| abbreviation = Nat. Rev. Drug Discov.
| publisher = [[Nature Publishing Group]]
| frequency = Monthly
| history = 2002-present
| website = http://www.nature.com/nrd/index.html
| link1 = http://www.nature.com/nrd/current_issue
| link1-name = Online access
| link2 = http://www.nature.com/nrd/archive/index.html
| link2-name = http://www.nature.com/nrd/archive/index.html
| ISSN = 1474-1776
| eISSN = 1474-1784
| impact = 47.120
| impact-year = 2015
| OCLC = 427380493
| CODEN = NRDDAG
| LCCN = 2002243044
}}
'''''Nature Reviews Drug Discovery''''' is a [[peer-reviewed]] [[review journal]] covering [[drug discovery]] and development. According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 47.120.<ref name=WoS>{{cite book |year=2016 |chapter=Nature Reviews Drug Discovery |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

Reviews are commissioned to specialists and supplemented with glossary explanations for non-specialist readers and illustrated with figures drawn by Nature's in-house art editors. Besides reviews, the journal publishes analysis articles based on existing datasets (e.g. [[metaanalysis]]), progress articles that focus on outstanding issues, and perspective articles&mdash;typically opinions or historical pieces.<ref>[http://www.nature.com/nrd/info/guide_reviews.html Guide : Information : Nature Reviews Drug Discovery]. Nature.com. Retrieved on 2011-06-29.</ref>

== See also ==
* ''[[Nature Biotechnology]]''
* ''[[Annual Review of Pharmacology and Toxicology]]''
* ''[[Pharmacological Reviews]]''

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.nature.com/nrd/index.html}}

{{Georg von Holtzbrinck Publishing Group|state=collapsed}}
[[Category:Pharmacology journals]]
[[Category:Nature Publishing Group academic journals]]
[[Category:English-language journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 2002]]

{{med-journal-stub}}