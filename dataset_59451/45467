{{Refimprove|date=November 2015|reason=there are some strong claims made in this article , eg first this etc, but without inline support}}
{{Infobox engineer
|image                =Bundesarchiv_Bild_102-13690,_Günter_Groenhoff.jpg
|image_size           = <!-- Default is frameless -->
|alt                  =
|caption              =Alexander Lippisch, with Günther Grönhoff in the cockpit of the Storch V.
|name                 =Alexander Lippisch
|native_name          =
|native_name_lang     =
|nationality          =German
|citizenship          =
|birth_name           =
|birth_date           ={{Birth date|1894|11|02}}
|birth_place          =[[Munich]], [[German Empire]]
|death_date           ={{Death date and age|1976|02|11|1894|11|02}}
|death_place          =[[Cedar Rapids, Iowa|Cedar Rapids]], [[Iowa]], US
|resting_place        =
|resting_place_coordinates = <!-- {{Coord|LAT|LONG|type:landmark|display=inline}} -->
|education            =
|spouse               =
|parents              =
|children             =
|discipline           =
|institutions         =
|practice_name        =
|employer             =
|significant_projects =[[Messerschmitt Me 163]]
|significant_design   =
|significant_advance  =first [[delta wing]] to fly<br>first mass-produced [[rocket fighter]]
|significant_awards   =
|signature            =
}}
'''Alexander Martin Lippisch''' (November 2, 1894 – February 11, 1976) was a German aeronautical engineer, a pioneer of [[aerodynamics]] who made important contributions to the understanding of [[flying wing]]s, [[delta wing]]s and the [[ground effect in aircraft|ground effect]], and also worked in the U.S.  His most famous designs are the [[Messerschmitt Me 163]] rocket-powered interceptor<ref name=Reitsch>Reitsch, H., 1955, The Sky My Kingdom, London: Biddles Limited, Guildford and King's Lynn, ISBN 1853672629</ref>{{rp|174}} and the strange-looking Aerodyne.<ref>[http://discaircraft.greyfalcon.us/LIPPISCHE%20AERODYNE%20RESEARCH.htm discaircraft.greyfalcon.us]</ref>

==Early life==
Lippisch was born in [[Munich]], [[Kingdom of Bavaria]].  He later recalled that his interest in aviation began with a demonstration conducted by [[Wright Brothers|Orville Wright]] over [[Tempelhof International Airport|Tempelhof Field]] in [[Berlin]] in September 1909.<ref>[http://www.centennialofflight.gov/essay/Wright_Bros/Military_Flyer/WR11G1.htm Wright Flyer over Templehoff]</ref>  Nonetheless, he planned to follow his father’s footsteps into art school, until the outbreak of [[World War I]] intervened.  During his service with the German Army in 1915–1918 Lippisch had the chance to fly as an aerial photographer and mapper.

==Early aircraft designs==
Following the war, Lippisch worked with the [[Luftschiffbau Zeppelin|Zeppelin Company]], and it was at this time that he first became interested in [[tailless aircraft]]. In 1921 his first design to be  built, by his friend [[Gottlob Espenlaub]], was the [[Espenlaub E-2]] glider. This was the beginning of a research programme that would result in some fifty designs throughout the 1920s and 1930s. Lippisch’s growing reputation saw him appointed in 1925 as the director of the [[Rhön-Rossitten Gesellschaft]] (RRG), a glider organisation including research groups and construction facilities.

Lippisch also designed conventional gliders at this time, including the [[Lippisch Wien|Wien]] of 1927 and its successor the [[Lippisch Fafnir|Fafnir]] of 1930. In 1928 Lippisch’s [[Canard (aeronautics)|tail-first]] [[Lippisch Ente|Ente]] (''Duck'') became the first aircraft to fly under [[rocket]] power.{{citation needed|date=November 2015}} From 1927 he resumed his tailless work, leading to a series of designs named '''Storch I''' – '''Storch IX''' (Stork I-IX), mostly gliders. These designs attracted little interest from the government and private industry.

==Delta wing designs==
Experience with the Storch series led Lippisch to concentrate increasingly on [[delta wing|delta-winged]] designs. The '''Delta I''' was the world's first<ref name=secret>{{cite book|last=Ford|first=Roger|title=Germany's secret weapons in World War II|date=2000|publisher=MBI Publ.|location=Osceola, WI|isbn=0-7603-0847-0|url=https://books.google.com/books?id=lU8xBhe9ntsC&lpg=PA36&dq=Lippisch&pg=PA36#v=onepage&q=Lippisch&f=false|edition=1. publ.|page=36}}</ref> tailless delta wing aircraft to fly (in 1931<ref name=Ducted>{{cite book|title=Ducted Fan Design |edition=Revised|volume=1|pages=129–130|url=https://books.google.com/books?id=YcAjcSSP4HMC&pg=PA129&dq=Lippisch+erste+delta#v=onepage&q&f=false|author=F. Marc de Piolenc|author2=George E. Wright Jr.|last-author-amp=yes|accessdate=13 February 2011}}</ref><ref name=pop1931>[https://books.google.com/books?id=ESgDAAAAMBAJ&pg=PA65&dq=Popular+Mechanics+1931+curtiss&hl=en&ei=inUETaDeIKeJnAf06JnlDQ&sa=X&oi=book_result&ct=result&resnum=6&ved=0CDUQ6AEwBTgU#v=onepage&q&f=true "New Triangle Plane Is Tailless", December 1931, Popular Science] article and photo of Delta I at bottom of page 65</ref>). This interest resulted in five aircraft, numbered Delta I – Delta V, which were built between 1931 and 1939.<ref name=pop1931 /> In 1933, RGG had been reorganised into the ''Deutsche Forschungsanstalt für Segelflug'' (German Institute for Sailplane Flight, ''DFS'') and the Delta IV and Delta V were designated as the [[DFS 39]] and [[DFS 40]] respectively.

==World War II projects==
In early 1939 the ''Reichsluftfahrtsministerium'' ([[Reich Air Ministry|RLM]], Reich Aviation Ministry) transferred Lippisch and his team to work at the [[Messerschmitt]] factory, in order to design a high-speed [[fighter aircraft]] around the rocket engines<ref name=Ducted />  then under development by [[Hellmuth Walter]]. The team quickly adapted their most recent design, the [[DFS 194]], to rocket power, the first example successfully flying in early 1940. This successfully demonstrated the technology for what would become the [[Messerschmitt Me 163]] Komet.<ref>Lippisch, A.; ''The Delta Wing: History and Development'', Iowa State University 1981, page 45: "Let me stress, however that the ''DFS 194'' should in no way be regarded as a predecessor of the ''Me 163''. The ''Me 163-Delta IVd'' was derived directly from the ''Delta-IVc-DFS 39''."</ref>

Although technically novel, the Komet did not prove to be a successful weapon and friction between Lippisch and Messerschmitt was frequent. In 1943, Lippisch transferred to Vienna’s Aeronautical Research Institute (''Luftfahrtforschungsanstalt Wien'', ''LFW''), to concentrate on the problems of high-speed flight.<ref name=Ducted /> That same year, he was awarded a doctoral degree in engineering by the [[University of Heidelberg]].

[[Wind tunnel]] research in 1939 had suggested that the delta wing was a good choice for [[supersonic]] flight, and Lippisch set to work designing a supersonic, [[ramjet]]-powered fighter, the [[Lippisch P.13a]]. By the time the war ended, however, the project had only advanced as far as a development glider, the [[Lippisch DM-1|DM-1]].

==Postwar work in the United States==
Like many German scientists, Lippisch was taken to the United States after the war under [[Operation Paperclip]] at the [[White Sands Missile Range]].

===Convair===
Advances in [[jet engine|jet engine design]] were making Lippisch's ideas more practical, and [[Convair]] became interested in a hybrid ([[mixed power]]) jet/rocket design which they proposed as the F-92. In order to gain experience with the delta wing handling at high speeds, they first built a test aircraft, the 7002 which, on June 9, 1948, became the first jet-powered delta-wing aircraft to fly.<ref>{{cite book|title=Aviation week|volume=51|date=1949|publisher=McGraw-Hill Pub. Co.|url=https://books.google.com/books?id=typQAAAAYAAJ&q=%22first+delta-wing%22+Lippisch&dq=%22first+delta-wing%22+Lippisch}}</ref> Although the U.S. Air Force lost interest in the F-92, the next test model 7003 was designated the [[XF-92A]]. This led Convair to proposing delta wing for most of their projects through the 1950s and into the 1960s, including the [[F-102 Delta Dagger]], [[F-106 Delta Dart]] and [[B-58 Hustler]].

==Ground effect aircraft==
From 1950–1964, Lippisch worked for the [[Collins Radio Company]] in [[Cedar Rapids, Iowa]], which had an aeronautical division.<ref name=Ducted /> It was during this time that his interest shifted toward [[Ground effect vehicle|ground effect craft]]. The results were an unconventional [[VTOL]] aircraft (eventually becoming the [[Dornier Aerodyne]]) and an aerofoil boat research seaplane [[Collins X-112|X-112]], flown in 1963. However, Lippisch contracted [[cancer]], and resigned from Collins.

When he recovered in 1966, he formed his own research company, [[Lippisch Research Corporation]], and attracted the interest of the West German government. Prototypes for both the aerodyne and the ground-effect craft [[RFB X-113]] (1970) then [[RFB X-114]] (1977) were built, but no further development was undertaken. The [[Mercury Marine|Kiekhaefer Mercury]] company was also interested in his ground-effect craft and successfully tested one of his designs as the Aeroskimmer, but also eventually lost interest.

Lippisch died in [[Cedar Rapids, Iowa|Cedar Rapids]] on February 11, 1976.<ref name=Ducted />

== Some Lippisch designs ==
* [[Lippisch SG-38 Zögling]], 1926
* [[Lippisch P.01-111]], designed during 'Projekt X', which would eventually culminate in the [[Messerschmitt]] [[Me-163]] Komet.<ref name=genesis>{{cite book|last=Masters|first=David|title=German Jet Genesis
|date=1982|publisher=Jane's Publishing Company Limited.|location=London, UK|isbn=0-86720-622-5|url=http://www.amazon.com/German-Jet-Genesis-David-Masters/dp/0867206225|edition=1. publ.|page=142}}</ref>
* [[Lippisch Li P.04]], a tailless airplane designed as a competitor to the [[Messerschmitt Me 329]]
* [[Lippisch Li P.10]], 1942 tailess bomber design
* [[Lippisch P.11]], designed to compete with the [[Horten brothers|Horten]] [[Horten Ho 229|Ho-IX]]; the latter went on to become the Horten (Gotha) Ho-(Go-)229.
* [[Lippisch P.13]], 1943 push-pull bomber design
* [[Lippisch P.13a]], a unique delta-winged, ramjet-powered interceptor.
* [[Lippisch P.13b]], a unique airplane powered by a rotating fuel-table of lignite, owing to the fuel shortages late in [[World War II|World War 2]] in Germany.
* [[Lippisch P.15]], a development of the Messerschmitt Me-163 Komet.
* [[Lippisch P.20]], a development of the P.15.
* [[Dornier Aerodyne]], a 1972 VTOL testbed

== See also ==
* [[German inventors and discoverers]]
* [[John Carver Meadows Frost]]

== References ==
{{Reflist}}

== External links ==
{{Commons category|Alexander Lippisch}}
*[http://www.luft46.com/lippisch/lippisch.html Luft '46]
*Summary of his 'Aerodyne' work [http://discaircraft.greyfalcon.us/LIPPISCHE%20AERODYNE%20RESEARCH.htm]
*Lecture on aerodynamics by Dr. Lippisch [https://www.youtube.com/watch?v=1oykDzWbmnM]
{{Lippisch aircraft}}

{{Authority control}}

{{DEFAULTSORT:Lippisch, Alexander}}
[[Category:1894 births]]
[[Category:1976 deaths]]
[[Category:Aerodynamicists]]
[[Category:German military personnel of World War I]]
[[Category:German people of World War II]]
[[Category:Bavarian emigrants to the United States]]
[[Category:German aerospace engineers]]
[[Category:People from Munich]]
[[Category:People from the Kingdom of Bavaria]]
[[Category:Operation Paperclip]]
[[Category:Recipients of the Knights Cross of the War Merit Cross]]