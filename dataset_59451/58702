{{Infobox journal
| title = Kriterion
| cover =
| language = English, German
| editor = Albert J. J. Anglberger, Christian J. Feldbacher, Alexander Gebharter, Laurenz Hudetz, Christine Schurz, Christian Wallmann
| discipline = [[Philosophy]]
| abbreviation = Kriterion
| publisher =
| country =
| frequency = Irregular
| history = 1991–present
| openaccess =
| impact =
| impact-year =
| website = http://www.kriterion-journal-of-philosophy.org
| link1 = http://www.kriterion-journal-of-philosophy.org/kriterion/read-online/
| link1-name = Online access
| eISSN = 1019-8288
| OCLC =
| LCCN =
}}
'''''Kriterion – Journal of Philosophy''''' is a [[peer-reviewed]] [[academic journal]] of [[philosophy]] that was established in 1991. The journal is dedicated to [[analytic philosophy]] and publishes articles in [[English language|English]] and [[German language|German]]. It is a sponsor of the annual [[Salzburg Conference for Young Analytic Philosophy]] (SOPhiA) since its inception in 2010.<ref>[https://www.sbg.ac.at/sophia/SOPhiA/2015/languages/en/history.php "History"] SOPhiA</ref>

The journal is listed as a key journal in ''[[The Philosopher's Index]]''<ref>[http://philinfo.wpengine.com/wp-content/uploads/2015/04/PIC_Alphabetical_Coverage.pdf "Key journals"], ''The Philosopher's Index''</ref> and included in the [[European Reference Index for the Humanities]].<ref>[https://dbh.nsd.uib.no/publiseringskanaler/erihplus/periodical/info?id=482742 "Kriterion – Journal of Philosophy"] ERIH</ref> It is abstracted and indexed in [[EBSCO Information Services|EBSCO databases]]<ref>[http://www.kriterion-journal-of-philosophy.org/ "Home"] Kriterion</ref> and [[Scopus]].<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2015-06-09}}</ref>

== References ==
{{Reflist}}

==External links==
* {{Official website|http://www.kriterion-journal-of-philosophy.org/}}

[[Category:Analytic philosophy literature]]
[[Category:Philosophy journals]]
[[Category:Contemporary philosophical literature]]
[[Category:Multilingual journals]]
[[Category:Publications established in 1991]]