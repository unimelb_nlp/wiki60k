{{featured article}}
{{Infobox film
| name           = Si Ronda
| image_size     = 275
| border         = 
| alt            = 
| director       = [[Lie Tek Swie]]
| producer       =Tan Khoen Yauw
| screenplay     =
| narrator       = 
| starring       ={{plain list|
*[[Bachtiar Effendi]]
*Momo
}}
| music          = 
| cinematography = A Loepias
| editing        = 
| studio         = [[Tan's Film]]
| distributor    = 
| released       = {{Film date|df=yes|1930|||Dutch East Indies}}
| runtime        = 
| country        = [[Dutch East Indies]]
| language       = 
| budget         = 
| gross          = 
}}
'''''Si Ronda''''' is a 1930 [[silent film]] from the [[Dutch East Indies]] which was directed by [[Lie Tek Swie]] and starred [[Bachtiar Effendi]]. Based on contemporary [[Betawi people|Betawi]] oral tradition, it follows the exploits of a bandit, skilled in ''[[silat]]'' (traditional Malay martial arts), known as Si Ronda. In the ''[[lenong]]'' stories from which the film was derived, Ronda was often depicted as a [[Robin Hood]] type of figure. The production, now thought [[lost film|lost]], was one of a series of martial arts films released between 1929 and 1931. ''Si Ronda'' received little coverage in the media upon its release. A second adaptation of the tale, ''Si Ronda Macan Betawi'', was made in 1978.

==Production==
''Si Ronda'' was adapted from a ''[[lenong]]'' (a [[Betawi people|Betawi]] oral tradition similar to a stage play) popular with [[Chinese Indonesians|ethnic Chinese]] and [[Native Indonesians|native]] audiences of the time. The Ronda stories follow the Betawi bandit of the same name, who is skilled at ''[[silat]]'' (traditional martial arts) and reputed to take from the rich to give to the poor.<ref>{{harvnb|Filmindonesia.or.id, Si Ronda}}; {{harvnb|Biran|2009|pp=104–05}}; {{harvnb|van Till|1996|p=470}}</ref> The Indonesian film scholar [[Misbach Yusa Biran]] suggests that Ronda was selected for adaptation because of its potential action sequences. In the domestic cinema, such sequences had generally been inspired by American works and been well received by audiences.<ref>{{harvnb|Biran|2009|p=108}}; {{harvnb|Sen|1995|p=15}}</ref>

Similar stories to Si Ronda's include those of Si Jampang and [[Si Pitung]];{{sfn|JCG, Ronda, Si}} these stage plays centred on extraordinary men (referred to as ''jago'') who, though living outside the law, generally fought for the common populace.{{sfn|Gouda|1999|p=169}} Adaptations of the genre, manifested as bandit films, became popular in domestic cinema following the release of ''[[Si Tjonat]]'' by Batavia Motion Picture in 1929. This release was followed by the [[Wong brothers]]' ''Rampok Preanger'' (also 1929), and an [[Si Pitoeng (1931 film)|adaptation]] of the Si Pitung stories in 1931.<ref>{{harvnb|Sen|1995|p=15}}; {{harvnb|Biran|2009|pp=105, 111, 113}}</ref> Not all films in this genre, which consisted of a quarter of all [[List of films of the Dutch East Indies|domestic releases for 1929–1931]], maintained the heroic qualities of the central character: the Wongs' adaptation of ''Si Pitung'', for instance, portrayed him as a simple bandit and not the [[Robin Hood]] figure of stage.<ref>{{harvnb|Biran|2009|pp=379–80}}; {{harvnb|van Till|1996|pp=461–68}}</ref>

''Si Ronda'' was directed by [[Lie Tek Swie]] and produced by Tan Khoen Yauw of [[Tan's Film]].{{sfn|Filmindonesia.or.id, Kredit Si Ronda}} The two had previously worked together on the company's highly profitable ''[[Njai Dasima (1929 film)|Njai Dasima]]'' in 1929.{{sfn|Biran|2009|pp=99–100}} [[Cinematography]] was handled by A. Loepias. Shot in black-and-white,{{sfn|Filmindonesia.or.id, Kredit Si Ronda}} this [[silent film]] starred [[Bachtiar Effendi]], a set decorator with Tan's, in his on-screen debut playing the title role.{{sfn|Said|1982|p=138}} It also featured Momo, an actor who had appeared in the film ''Njai Dasima''.{{sfn|Filmindonesia.or.id, Momo}}

==Release and legacy==
[[File:Bachtiar Effendi p104.JPG|thumb|upright|[[Bachtiar Effendi]], who played the title role in ''Si Ronda'']]
''Si Ronda'' was released in 1930;{{sfn|Filmindonesia.or.id, Si Ronda}} Effendi stated that it was released before Tan's ''[[Nancy Bikin Pembalesan]]'' (''Nancy Takes Revenge''), a sequel to ''Njai Dasima'', began screening in May 1930.{{sfn|Biran|2009|pp=104–05}} Dutch newspapers indicate that it had screened in [[Medan]], [[North Sumatra]], by 1932.{{sfn|De Sumatra Post 1932, Kunst en Vermakelijkheden}} Biran writes that the film received little coverage; he notes that [[Sinematek Indonesia]] has no news clippings related to ''Si Ronda''.{{sfn|Biran|2009|pp=104–05}}

After ''Si Ronda'', Lie and Tan collaborated on three further films.{{sfn|Said|1982|pp=142–43}} Lie left Tan's in 1932, reportedly as his approach no longer matched Tan's low-class target audience and caused the works to go over budget.{{sfn|Biran|2009|p=111}} Effendi continued to work with Tan's until 1932, when he left to head the cinema magazine ''Doenia Film''.{{sfn|Said|1982|p=138}} Momo continued acting until 1941, first with Tan's and later with Standard Film.{{sfn|Filmindonesia.or.id, Momo}} The film is likely [[lost film|lost]]. The American visual anthropologist [[Karl G. Heider]] writes that all Indonesian films from before 1950 are lost.{{sfn|Heider|1991|p=14}} However, JB Kristanto's ''Katalog Film Indonesia'' (''Indonesian Film Catalogue'') records several as having survived at Sinematek Indonesia's archives.{{sfn|Biran|2009|p=351}}

Another film based on the Ronda stories, titled ''Si Ronda Macan Betawi'' (''Ronda the Betawi Tiger''), was released in 1978.{{sfn|Filmindonesia.or.id, Si Ronda}} Directed by Fritz G. Schadt, it starred [[Dicky Zulkarnaen]] in the title role and Lenny Marlina as his lover. In this adaptation Ronda uses his ''silat'' skills to fight corrupt land owners and colonial government workers.{{sfn|Filmindonesia.or.id, Si Ronda Macan Betawi}}

==References==
{{reflist|30em}}

==Works cited==
{{refbegin|40em}}
* {{cite book
  | title = [[Sejarah Film 1900–1950: Bikin Film di Jawa]]
  | trans_title = History of Film 1900–1950: Making Films in Java
  | language = Indonesian
  | last = Biran
  | first = Misbach Yusa
  | authorlink = Misbach Yusa Biran
  | location = Jakarta
  | publisher = Komunitas Bamboo working with the Jakarta Art Council
  | year = 2009
  | isbn = 978-979-3731-58-2
  | ref = harv
  }}
*{{cite book
 |title=Gender, Sexuality and Colonial Modernities
 |chapter=Gender and 'Hyper-masculinity' as Post-colonial Modernity during Indonesia's Struggle for Independence, 1945 to 1949
 |last=Gouda
 |first=Frances
 |isbn=978-0-203-98449-9
 |editor1-last=Burton
 |editor1-first=Antoinette
 |publisher=Routledge
 |year=1999
 |location=London
 |pages=163–76
 |ref=harv
}}
*{{cite book
 |url=https://books.google.com/books?id=m4DVrBo91lEC
 |title=Indonesian Cinema: National Culture on Screen
 |isbn=978-0-8248-1367-3
 |author1=Heider
 |first1=Karl G.
 |year=1991
 |publisher=University of Hawaii Press
 |location=Honolulu
 |ref=harv
}}
* {{cite web
  | title = Kredit Si Ronda
  |trans_title=Credits for Si Ronda
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-s007-30-376864_si-ronda/credit
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 28 April 2013
  | archiveurl = http://www.webcitation.org/6GDP11xP1
  | archivedate = 28 April 2013
  | ref = {{sfnRef|Filmindonesia.or.id, Kredit Si Ronda}}
  }}
*{{cite news
 |title=Kunst en Vermakelijkheden
 |trans_title=Art and Entertainment
 |language=Dutch
 |url=http://kranten.kb.nl/view/article/id/ddd%3A011023834%3Ampeg21%3Ap005%3Aa0110
 |work=De Sumatra Post
 |location=Medan
 |page=5
 |date=7 January 1932
 |publisher=J. Hallermann
 |accessdate=28 April 2013
 |ref={{sfnRef|De Sumatra Post 1932, Kunst en Vermakelijkheden}}
}}
* {{cite web
  | title = Momo
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/name/nmp4b852da934003_momo/filmography
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 28 April 2013
  | archiveurl = http://www.webcitation.org/6GDPNk58N
  | archivedate = 28 April 2013
  | ref = {{sfnRef|Filmindonesia.or.id, Momo}}
  }}
*{{cite web
 |url=http://www.jakarta.go.id/web/encyclopedia/detail/2623/Ronda-Si
 |title=Ronda, Si
 |language=Indonesian
 |work=Encyclopedia of Jakarta
 |publisher=Jakarta City Government
 |accessdate=28 April 2013
 |archivedate=28 April 2013
 |archiveurl=http://www.webcitation.org/6GDRRIJpT
 |ref={{sfnRef|JCG, Ronda, Si}}
}}
*{{cite book
 |title=Profil Dunia Film Indonesia
 |trans_title=Profile of Indonesian Cinema
 |language=Indonesian
 |last=Said
 |first=Salim
 |publisher=Grafiti Pers
 |location=Jakarta
 |year=1982
 |oclc=9507803
 |ref=harv
}}
*{{cite book
 |title=Indonesian Cinema: Framing the New Order
 |publisher=Zed Books
 |last1=Sen
 |first1=Krishna
 |ref=harv
 |year=1995
 |isbn=978-1-85649-123-5
 |location=London
}}
* {{cite web
  | title = Si Ronda
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-s007-30-376864_si-ronda
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 22 July 2012
  | archiveurl = http://www.webcitation.org/69LYXyB55
  | archivedate = 22 July 2012
  | ref = {{sfnRef|Filmindonesia.or.id, Si Ronda}}
  }}
* {{cite web
  | title = Si Ronda Macan Betawi
  | language = Indonesian
  | url =http://filmindonesia.or.id/movie/title/lf-s018-78-423638_si-ronda-macan-betawi
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 28 April 2013
  | archiveurl = http://www.webcitation.org/6GDPZX4VY
  | archivedate = 28 April 2013
  | ref = {{sfnRef|Filmindonesia.or.id, Si Ronda Macan Betawi}}
  }}
*{{cite journal |title=In Search of Si Pitung: The History of an Indonesian Legend |first= Margreet |last=van Till |work=Bijdragen tot de taal-, land- en volkenkunde |jstor=27864777 |year=1996 |volume=152 |issue=3 |pages=461–482 |issn=0006-2294 |oclc=770588866 |ref=harv}} {{subscription required}}
{{refend}}

==External links==
{{Portal|Film}}
*{{IMDb title|1846736|Si Ronda}}

{{Lie Tek Swie}}

[[Category:Indonesian black-and-white films]]
[[Category:Dutch films]]
[[Category:Dutch silent films]]
[[Category:Films directed by Lie Tek Swie]]
[[Category:Lost films]]
[[Category:Tan's Film films]]