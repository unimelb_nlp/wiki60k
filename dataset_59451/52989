{{italictitle}}
{{Infobox journal
| cover        = [[Image:Jcl print logo.gif|200px|JCL cover]]
| discipline   = [[Law review]]
| abbreviation = J. Corp. L.
| publisher    = Joe Christensen, Inc.
| country      = [[United States]]
| frequency    = Quarterly
| history      = 1974-present
| website      = http://www.law.uiowa.edu/journals/jcl/
| ISSN         = 0360-795X
}}
'''''The Journal of Corporation Law''''' (J. Corp. L. or JCL), at the [[University of Iowa College of Law]], is the nation's oldest student-published periodical specializing in corporate law.<ref> http://blogs.law.uiowa.edu/jcl/about-jcl/ </ref> It published its first issue in 1975.<ref> http://blogs.law.uiowa.edu/jcl/wp-content/uploads/2012/01/Thomas.pdf </ref> Its current adviser is Robert T. Miller, who joined the College of Law faculty in August 2012. The journal is routinely cited by scholars, practitioners, and courts, including the United States Supreme Court.<ref> Most recently in Amgen Inc. v. Connecticut Retirement Plans and Trust Funds, _ S. Ct. _ (2013) </ref>

==Membership==
Students apply for membership after completing their first year of legal study. The application consists of several short assignments meant to test an applicant's writing and editing ability. Each summer, the editorial board reviews these applications and invites 25 to 30 applicants to become student writers for the following academic year.

As student writers, JCL members write a Note discussing a relevant topic in corporate law. Excellent Notes are selected for publication. In addition, student writers are expected to complete thirty-five secondary hours per semester. This includes time spent at authority checks or on other journal-related projects that the editorial board assigns. Student writers improve their writing and editing skills and contribute to maintaining JCL's status as a high quality publication. 

Each spring, members of the outgoing editorial board select the next year's board members from the pool of student writers.

== Rankings ==
JCL is widely regarded as one of the premier journals in its field. In a recent ranking of corporate law journals by the ExpressO Law Review Submission Guide, JCL was the highest ranked journal in the field.<ref>{{cite web | title = ExpressO Law Review Submission Guide | url = http://law.bepress.com/expresso/2005/subject_other.html}}</ref> According to the Washington and Lee Law Journal Rankings, JCL is the top-ranked corporate law journal in the country.<ref>{{cite web | title = Washington and Lee Rankings | url = http://lawlib.wlu.edu/LJ/index.aspx}}</ref>

==References==
{{reflist}}

{{DEFAULTSORT:Journal of Corporation Law}}
[[Category:American law journals]]
[[Category:Publications established in 1974]]
[[Category:University of Iowa College of Law]]
[[Category:1974 establishments in Iowa]]