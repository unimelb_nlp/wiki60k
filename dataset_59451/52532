{{Infobox journal
| title = PharmacoEconomics
| abbreviation = PharmacoEconomics
| discipline = [[Pharmacoeconomics]]
| editor = Christopher I. Carswell
| publisher = Adis International ([[Springer Science + Business Media]])
| country =
| history = 1992-present
| frequency = Monthly
| impact = 3.338
| impact-year = 2013
| openaccess = [[Hybrid open-access journal|Hybrid]]
| website = http://www.springer.com/adisonline/journal/40273
| link1 = http://rd.springer.com/journal/volumesAndIssues/40273
| link1-name = Online archive
| ISSN = 1170-7690
| eISSN = 1179-2027
| OCLC = 715234833
| LCCN = sn92033444
| CODEN = PARMEK
}}
'''''PharmacoEconomics''''' is a [[Peer review|peer-reviewed]] [[medical journal]] published by Adis International ([[Springer Science + Business Media]]) that covers the fields of [[health economics]], [[pharmacoeconomics]], and [[Quality of life|quality-of-life assessment]].

== Abstracting and indexing ==
'' PharmacoEconomics '' is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[MEDLINE]]
* [[EMBASE]]
* [[International Pharmaceutical Abstracts]]
* [[Current Contents]]/Clinical Medicine
* [[Science Citation Index]]
* [[Chemical Abstracts Service]]
* [[Sociedad Iberoamericana de Información Científica]]
* [[PsycINFO]]
* [[Research Papers in Economics]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 3.338.<ref name=WoS>{{cite book |year=2014 |chapter= PharmacoEconomics  |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate= |series=[[Web of Science]] |postscript=.}}</ref>

==References==
{{Reflist}}

==External links==
* {{Official website|http://www.springer.com/adisonline/journal/40273}}

[[Category:Pharmacology journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1992]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Monthly journals]]