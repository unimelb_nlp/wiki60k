{{Infobox rail accident
| title            = Alfarelos train crash
| boxwidth         = 
| image            = 
| image_alt        = 
| image_width      = 
| caption          = 
| date             = 21 January 2013
| time             = 21:15
| location         = [[Alfarelos]], [[Soure, Portugal|Soure]], [[Coimbra]]
| location-dist    = 
| location-dist-mi = 
| location-dir     = 
| location-city    = 
| coordinates      = 
| country          = [[Portugal]]
| line             = [[Linha do Norte]]
| operator         = [[Comboios de Portugal|Caminhos-de-Ferro Portugueses]]
| type             = Rear collision
| cause            = 
| trains           = 2
| pax              = 82<ref name=JNI/>
| deaths           = 0<ref name=PublicoII/>
| injuries         = 25<ref name=RTPI/>
| damage           = 
| map              = {{Infobox rdt|map=
{{BS2||CONTg||{{RoutemapRoute||Licon=u|''Linha do Norte'' to [[Porto Campanhã railway station|Porto Campanhã]]|enclosed=no}}}}
{{BS2||HST|201,211|Formoselha|}}
{{BS2||SPLa+vBHF|198,339|Alfarelos|}}
{{BS2||INCIDO|197,782|'''Crash site'''<ref name=report/>|}}
{{BS2|CONTgq|vSTRgr||'''''[[Ramal de Alfarelos|R. Alfarelos]]''''' → <small>[[Bifurcação de Lares|Bif. Lares]]</small>}}
{{BS2||HST|191,365|Vila Nova de Anços|}}
{{BS2||CONTf||{{RoutemapRoute||Licon=d|''Linha do Norte'' to [[Santa Apolónia railway station|Lisbon Santa Apolónia]]|}}}}
}}
| map_name         = Linha do Norte route map
| map_state        = collapsed
}}

The '''Alfarelos train crash''' occurred on 21 January 2013 at the
[[Alfarelos station]], in the Portuguese [[Linha do Norte|Northern Line]]. Two trains
crashed, after [[Signal passed at danger|passing the same signal at danger]], wounding 25 people
and halting the Northern Line for three days.<ref name=RTPI>{{cite web|url=http://www.rtp.pt/noticias/index.php?article=622446&tm=8&layout=122&visual=61|title=Relatório sobre desastre ferroviário de Alfarelos chega ao Governo|accessdate=21 August 2014|date=25 January 2013|publisher=Rádio e Televisão de Portugal|language=Portuguese}}</ref><ref name=PublicoI>{{cite web|url=http://www.publico.pt/sociedade/noticia/circulacao-na-linha-de-comboios-do-norte-reaberta-nesta-quintafeira-1581893#/0|title=Linha do norte reabre nesta quinta-feira|accessdate=22 August 2014|date=24 January 2013|publisher=Público|language=Portuguese}}</ref>

==Context==

On the day of the accident, services were operating as usual in the
Northern Line. 
At 19:30, an [[InterCity]] train left [[Santa Apolónia railway station|Lisbon Santa Apolónia]] towards [[Porto Campanhã railway station|Porto Campanhã]].

About half an hour later, at 19:55, a train left [[Entroncamento]], {{convert|106|km|mi}} away from Santa Apolónia, making the
regional service towards [[Coimbra]], where it was scheduled to arrive at 21:51.

Both trains were expected to meet at around 21:17 in Alfarelos, where the InterCity would overtake the regional.

==Crash==

The inquiry commission led by [[Comboios de Portugal|CP]] and [[Rede Ferroviária Nacional|REFER]] collected information about the events that led to the collision.<ref name=report>{{cite web|url=
http://www.refer.pt/LinkClick.aspx?fileticket=D6u56w2Qvvk%3d&tabid=102
|title=Inquérito DMS N.o 472859-DSC|accessdate=21 August 2014|date=24 January 2013|publisher=[[Rede Ferroviária Nacional|REFER]]|language=Portuguese}}</ref> According to their report,
shortly before arriving at Alfarelos, the regional train encountered a
caution signal (solid yellow), meaning that it would find a red signal
at Alfarelos. As the driver slowed down the train, so that it would stop before the red signal, it started slipping
on the rails.

[[File:CP 5613 com um IC PT LX (5088518469).jpg|thumb|right|CP 5613 pulling a train. This  [[CP Class 5600]] locomotive was pulling the InterCity train involved in the crash, and suffered heavy damage.]]

The driver was not able to stop the train on time. {{convert|230|m|mi}} before the signal, with the train
travelling at {{convert|36|km/h|mph}}, [[CONVEL]] triggered the emergency
brakes. This was not enough to stop the train which [[Signal passed at danger|passed the signal at danger]] at a speed of
{{convert|34.5|km/h|mph}} and stopped {{convert|269|m|mi}} after the
signal.

Following the rules for this type of incident, the driver
contacted the REFER Lisbon Operational Command Center (CCO Lisboa),
awaiting instructions on how to proceed. The train could either be
told to go back to the signal, or to continue ahead.

At the same time, the InterCity train passed through the same caution
signal at {{convert|130.5|km/h|mph}}, and faced similar
problems. After braking for {{convert|500|m|mi}}, the train speed was
still {{convert|90|km/h|mph}}, which prompted the driver to use the
[[Sandbox (locomotive)|sander]]. This train was also unable to stop on
time, and passed the signal at danger with a speed of
{{convert|58.5|km/h|mph}}, moment at which CONVEL triggered the
emergency brakes. Shortly after, at 21:15, this train collided with
the regional train, at {{convert|42|km/h|mph}}, destroying the last car and damaging other two.<ref name=PublicoII/>

==Aftermath==

Despite the collision and the destruction, which included graphic
photos of a rail car torn apart, with the InterCity going through it
"like it were a tunnel",<ref name=JNI>{{cite web|url=http://www.jn.pt/PaginaInicial/Sociedade/Interior.aspx?content_id=3006856&page=-1|title=Choque entre comboio "Intercidades" e "Regional" fez pelo menos 15 feridos|accessdate=22 August 2014|publisher=JN|language=Portuguese}}</ref> there were no fatalities: the three last cars, which suffered most of the damage,<ref name=PublicoI/> had no passengers. The driver of the InterCity locomotive survived by getting down on the floor.<ref name=PublicoII>{{cite web|url=http://www.publico.pt/sociedade/noticia/queda-de-grua-atrasa-remocao-de-comboios-que-colidiram-em-alfarelos-1581761|title=Comboios que colidiram em Alfarelos não conseguiram parar no sinal vermelho|accessdate=21 August 2014|date=23 January 2013|publisher=Público|language=Portuguese}}</ref>

The regional train would normally be one [[multiple unit]] with three
cars ([[CP Class 2240]]), but, on the night of the accident, this
train had two multiple units, 2257 and 2294, comprising six cars.

In the aftermath of the accident, the track was not used for several
days, as works were in progress to remove the debris and to repair the infrastructure, and authorities
carried on investigations.

A preliminary inquiry found no explanation for the accident,
but ruled out human error from the drivers, and malfunctions in the
signaling system.  According to the inquiry, on both cases, the brakes were
applied in a way that would have stopped the trains before the red
signal under normal operating conditions.

The inquiry commission concluded that there was lack of adhesion
between the wheels and the rails. The commission suggested a speed
limitation of {{convert|30|km/h|mph}} in the accident area.

==References==
{{Reflist}}

{{2013 railway accidents}}

[[Category:Train collisions in Portugal]]
[[Category:2013 in Portugal]]
[[Category:Soure, Portugal]]
[[Category:Railway accidents in 2013]]
[[Category:Railway accidents involving a disregarded signal]]
[[Category:January 2013 events]]

[[pt:Desastre ferroviário de Alfarelos]]