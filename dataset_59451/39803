{{Infobox person
 | name          = Samuel Franklin Cody
 | image         = File:Samuel Franklin Cody.jpg
 | image_size    = 
 | caption       = Samuel Franklin Cody in 1909
 | birth_date    = {{Birth-date|6 March 1867|6 March 1867}}
 | birth_place   = [[Davenport, Iowa]], USA
 | death_date    = {{death date and age|df=yes|1913|8|7|1867|3|6}}
 | death_place   = [[Farnborough, Hampshire]], [[United Kingdom]]
 | occupation    = Showman, aviator, aircraft designer
 | spouse        =
 | parents       =
 | children      =
}}

'''Samuel Franklin Cowdery''' (later known as '''Samuel Franklin Cody'''; 6 March 1867&nbsp;– 7 August 1913, born [[Davenport, Iowa|Davenport]], [[Iowa]], USA<ref name="Newnes">{{cite book
|title=Newnes Family  Encyclopedia
|volume=1
|publisher=George Newnes Ltd.
|location=London
|page=727}})</ref>) was a [[Wild West Shows|Wild West showman]] and early pioneer of manned flight. He is most famous for his work on the large kites known as ''Cody War-Kites'', that were used by the British in [[World War I]] as a smaller alternative to [[balloon]]s for artillery spotting. He was also the first man to fly an aeroplane in [[United Kingdom|Britain]], on 16 October 1908.<ref>Taylor 1983 p. 28</ref><ref>{{cite journal |last1=Gibbs-Smith|first1=Charles H.|date=23 May 1958|year=|title=S.F. Cody: An Historian’s Comments|journal=[[Flight International|Flight]]|volume=73|issue=2574|page=699|publisher=|url= http://www.flightglobal.com/pdfarchive/view/1958/1958%20-%200683.html|accessdate=30 August 2013}}</ref> A flamboyant showman, he was often confused with [[Buffalo Bill Cody]],<ref> For example, see Pawle, 1956, p36.</ref> whose surname he took when young.

==Early life==
Cody's early life is difficult to separate from his own stories told later in life, but he was born Samuel Franklin Cowdery in 1867, in [[Davenport, Iowa]],<ref>"First Flight was a Mistake!", article in ''Ancestors'', October 2008, pp&nbsp;18–19</ref> where he attended school until the age of 12. Not much is known about his life at this time, although he claimed that during his youth he had lived the typical life of a cowboy. He learned how to ride and train horses, shoot and use a [[lasso]]. He later claimed to have prospected for gold in an area which later became [[Dawson City, Yukon|Dawson City]], centre of the famous [[Klondike Gold Rush]].

==Showman==
In 1888, at 21 years of age, Cody started touring the US with [[Adam Forepaugh|Forepaugh's Circus]], which at the time had a large [[Wild West Shows|Wild West show]] component. He married Maud Maria Lee in [[Norristown, Pennsylvania]], and the name ''Samuel Franklin Cody'' appears on the April 1889 marriage certificate.

Cody, together with his wife Maud Maria Lee, toured England with a shooting act. Maud used the stage name Lillian Cody, which she kept for the rest of her performing career. In London they met Mrs Elizabeth Mary King (later known as Lela Marie Cody; née Elizabeth Mary Davis), wife of Edward John King, a [[licensed victualler]], and mother of four children. Mrs King had stage ambitions for two of her younger children, Leon and Vivian King (later known as Leon and Vivian Cody). In 1891, Maud Maria Lee taught the boys how to shoot, but then later returned to the USA alone. Evidence suggests that by the autumn of 1891, Maud was unable to perform with her husband because of injury, morphine addiction, the onset of [[schizophrenia]], or a combination of these ills.

After Maud Cody returned to America, and around the time of Edward John King's death, Cody took up with Mrs King. While in England, the two lived together as husband and wife, and Mrs King, who used the name Lela Marie Cody, was generally assumed to be his legal wife. However, the marriage of Cody and Maud Maria Lee was never legally dissolved.

While in England, Cody, Lela and her sons toured the [[music hall]]s, which were very popular at the time, giving demonstrations of his horse riding, shooting and lassoing skills. While touring Europe in the mid-1890s, Cody capitalized on the bicycle craze by staging a series of horse vs. bicycle races against famous cyclists. Cycling organizations quickly frowned on this practice, which drew accusations of fixed results. In 1898, Cody's stage show, ''The Klondyke Nugget'', became very successful; it included Lela's eldest son Edward who was known as Edward Le Roy,<ref name=wdytyr>''Who Do You Think You Are?'', ''BBC Documentary'', broadcast September 2013</ref> and her younger sons Leon and Vivian (King), who were known as Cody to save any embarrassment.<ref name=wdytyr />

One of Lela's great-grandsons (and the grandson of Lela's daughter Lizzy 'Liese' King with her husband Edward King) is the BBC World Affairs Editor [[John Simpson (journalist)|John Simpson]].<ref name=wdytyr />

==Kites==
[[File:Cody manlifter.jpg|right|thumb|'''Man-lifter War Kite''' designed by Samuel Franklin Cody]]
It is not clear why Cody became fascinated by kite flying. Cody liked to recount a tale that he first became inspired by a Chinese cook; who, apparently, taught him to fly kites, whilst travelling along the old cattle trail.<ref>[http://www.ctie.monash.edu.au/hargrave/cody.html Image collection of Cody's man-lifter kites]</ref> However, it is more likely that Cody's interest in kites was kindled by his friendship with Auguste Gaudron, a balloonist Cody met while performing at Alexandra Palace. Cody showed an early interest in the creation of kites capable of flying to high altitudes and of carrying a man. Leon also became interested, and the two of them competed to make the largest kites capable of flying at ever-increasing heights. Vivian too became involved after a great deal of experimentation.

Financed by his shows, Cody significantly developed [[Lawrence Hargrave]]'s double-cell [[box kite]] to increase its lifting power, especially by adding wings on either side. He also developed a sophisticated system of flying multiple kites up a single line, which was capable of ascending to many thousands of feet or of carrying several men in a gondola. He patented his design in 1901, and it became known as the Cody kite.

Balloons were then in use for meteorological and military observation, but could only be operated in light winds. Cody realised that kites, which can only be operated in stronger winds, would allow these activities to be carried out in a wider range of weather conditions. His kites were soon adopted for [[meteorology]], and he was made a Fellow of the [[Royal Meteorological Society]].<ref name="walkerI">Walker, P.; "Early Aviation at Farnborough, Volume I: Balloons, Kites and Airships", Macdonald (1971).</ref>

In December 1901, he offered his design to the [[War Office]] as an observation "War Kite" for use in the [[Second Boer War]], and made several demonstration flights of up to 2,000&nbsp;ft in various places around London. A large exhibition of the Cody kites took place at [[Alexandra Palace]] in 1903. Later, he succeeded in crossing the [[English Channel]] in a [[Berthon Boat|Berthon boat]] towed by one of his kites. His exploits came to the attention of the [[Admiralty]], who hired him to look into the military possibilities of using kites for observation posts. He demonstrated them later in 1903, and again on 2 September 1908, when he flew them off the deck of battleship [[HMS Revenge (1892)|HMS ''Revenge'']]. The Admiralty eventually purchased four of his War Kites.<ref name="walkerI" />

In 1905, using a radically different design looking more like a tailless biplane, he devised and flew a manned "glider-kite". The machine was launched on a tether like a kite, and the tether was then released to allow gliding flight. The design showed little similarity to his earlier kites, but had more the appearance of a tailless biplane. It was notable in being the first aircraft to use ailerons (in fact they were elevons) effectively to control roll.<ref name="walkerI" />

Cody eventually managed to interest the British Army in his kites. In 1906, he was appointed Chief Instructor of Kiting for the [[School of Ballooning|Balloon School]] in [[Aldershot]] and soon after joined the new Army Balloon Factory down the road at Farnborough, along with his purported son Vivian. The Factory would eventually become the [[Royal Aircraft Establishment]], and Vivian Cody would go on to a long and successful career as a technical specialist.<ref name="walkerI" /> In 1908, the War Office officially adopted Cody's kites for the Balloon Companies he had been training. This group would in due course evolve into the [[Air Battalion Royal Engineers|Air Battalion]] of the Royal Engineers, No. 1 Company of which later became No. 1 Squadron, [[Royal Flying Corps]] and eventually [[No. 1 Squadron RAF|No. 1 Squadron]] [[Royal Air Force]].

Finally, in 1907, he created an unmanned "power-kite". Somewhat similar to his standard kite but with bigger wings and a tailplane with twin fins in place of the rear cell, this was fitted with a 15&nbsp;hp [[Buchet]] engine. It was not allowed to fly free; Cody strung a long aerial wire down the length of the Farnborough Balloon Shed and flew it indoors.<ref name="walkerI" />

All that remained to him, was to bring together the manned free-flying glider and the power-kite's engine to create Britain's first aeroplane.

==''Nulli Secundus''==
Before Cody could turn his newfound skills to aeroplanes, he was required to help complete an airship then under construction in the [[Farnborough, Hampshire|Farnborough]] Airship Shed. In December 1906, he was despatched to France, where he purchased a 40&nbsp;hp [[Antoinette (manufacturer)|Antoinette]] engine. During 1907, he was given full authority as the designer of the airship's understructure and propulsion system. On 5 October 1907, Britain's first powered airship [[British Army Dirigible No 1]] ''Nulli Secundus'', flew from Farnborough to London in 3 hours 25 minutes, with Cody and his commanding officer Colonel [[John Capper|J E Capper]] on board. After circling [[St Paul's Cathedral]], they attempted to return to Farnborough, but 18&nbsp;mph headwinds forced them to land at [[Crystal Palace, London|Crystal Palace]]. There, the airship was damaged by the high winds.<ref name="walkerI" />

==Flight==
Later in 1907 the Army decided to back the development of his powered aeroplane, the [[British Army Aeroplane No 1]]. After just under a year of construction, he started testing the machine in September 1908, gradually lengthening his "hops" until they reached {{convert|1,390|ft|m|abbr=on}} on 16 October 1908.<ref>{{cite journal |last1=Gibbs-Smith |first1=Charles H. |last2= |first2= |date=3 Apr 1959|year= |title= Hops and Flights: A roll call of early powered take-offs |journal=[[Flight International|Flight]] |volume=75 |issue=2619 |page=470|publisher= |url= http://www.flightglobal.com/pdfarchive/view/1959/1959%20-%200939.html|accessdate=24 Aug 2013}}</ref>

His flight of 16 October 1908 is recognised as the first official flight of a piloted heavier-than-air machine in [[Great Britain]].<ref name="walkerII">Walker, Vol. II (1974).</ref> The machine was damaged at the end of the flight. After repairs and extensive modifications, Cody flew it again, early in 1909. The [[War Office]] then decided to stop backing development of heavier-than-air aircraft, and Cody's contract with the Army ended in April 1909. Cody was given the aircraft, and continued to work on the aircraft at [[Farnborough, Hampshire|Farnborough]], using Laffan's Plain for his test flights.

On 14 May 1909 he succeeded in flying the aircraft for over a mile, establishing the first official British distance and endurance records.<ref>[http://www.flightglobal.com/pdfarchive/view/1909/1909%20-%200296.html "Cody flies in front of Prince of Wales"] [[Flight International|''Flight'']] 22 May 1909</ref> By August 1909, Cody had completed the last of his long series of modifications to the aircraft. He carried passengers for the first time on 14 August 1909: first his old workmate Capper, and then Lela Cody (Mrs Elizabeth Mary King).

On 29 December 1909 Cody became the first man to fly from [[Liverpool]] in an unsuccessful attempt to fly non-stop between Liverpool and [[Manchester]]. He set off from [[Aintree Racecourse]] at 12.16 p.m., but only nineteen minutes later he was forced to land at Valencia Farm near to Eccleston Hill, [[St Helens, Merseyside|St Helens]], close to [[Prescot]] because of thick fog.<ref>{{cite web |url=
http://www.liverpooldailypost.co.uk/liverpool-news/regional-news/2009/12/29/colonel-samuel-cody-s-historical-flight-to-st-helens-recreated-92534-25484467/ |title= Colonel Samuel Cody's historical flight to St Helens recreated|accessdate=2009-12-29 |publisher= [[Liverpool Daily Post]]}}</ref>

On 7 June 1910 Cody received [[Royal Aero Club]] certificate number 9 using a [[Cody Michelin Cup Biplane|newly built aircraft]], and later in the year won the [[Michelin Cup]] for the longest flight made in England during 1910 with a flight of 4 hours 47 minutes on 31 December 1910. In 1911, a [[Cody Circuit of Britain biplane|third aircraft]] was the only British machine to complete the ''[[Daily Mail]]''<nowiki/>'s "[[Daily Mail Circuit of Britain Air Race|Circuit of Great Britain]]" air race, finishing fourth, for which achievement he was awarded the Silver Medal of the R.Ae.C. in 1912.<ref name=Medals>R.Ae.C. Silver Medals awarded in January 1912 [http://www.flightglobal.com/pdfarchive/view/1912/1912%20-%200081.html Award of Medals] [[Flight International|''Flight'']], 27 January 1912]</ref> The [[Cody V biplane|Cody V]] machine with a new 120&nbsp;hp (90&nbsp;kW) engine won first prize at the [[1912 British Military Aeroplane Competition]] Military Trials on [[Salisbury Plain]]. He first prepared a monoplane, the [[Cody IV monoplane|Cody IV]], for the trials, but it was badly damaged in a crash before the trials began.

== Death ==
[[File:Cody VI wreckage RAE-O590.jpg|thumb|left|The wreckage of Cody's fatal air crash]]
[[File:cody-funeral-1913.jpg|thumb|left|[[Gale and Polden]] postcard of Cody's funeral procession on 11 August 1913]]
[[File:Samuel Franklin Cody, grave, Aldershot.jpg|thumb|right|The grave of Samuel Franklin Cody in [[Aldershot Military Cemetery]]]]
Cody continued to work on aircraft using his own funds. On 7 August 1913, he was test flying his latest design, the [[Cody Floatplane]], when it broke up at {{convert|200|ft|m}} and he and his passenger, the cricketer [[William Evans (English cricketer)|William Evans]], were killed. The two men, not strapped in, were thrown out of the aircraft<ref name="Jarrett AE p17">Jarrett ''Air Enthusiast'' July/August 1999, p. 17.</ref> and the [[Royal Aero Club]] accident investigation concluded that the accident was due to "inherent structural weakness", and suggested that the two might have survived the crash if they had been strapped in.<ref name="Jarrett AE p17"/><ref>''Flight'' 20 September 1913, p. 1040.</ref> Cody was buried with full military honours in the [[Aldershot Military Cemetery]]; the funeral procession drew an estimated crowd of 100,000.{{cn|date=February 2017}}

Adjacent to Cody's own grave marker is a memorial to his only son, Samuel Franklin Leslie Cody, born [[Basel]], Switzerland 1895, who joined the [[Royal Flying Corps]] and was killed in Belgium on 23 January 1917 while serving with [[No. 41 Squadron RAF|41 Squadron]].<ref>{{cite website|url=http://www.cwgc.org/find-war-dead/casualty/103158/CODY,%20SAMUEL%20FRANKLYN%20LESLIE|publisher=[[Commonwealth War Graves Commission]]|title=Cody, Samuel Franklyn Leslie|accessdate=2 Feb 2017}}</ref>

==Legacy==
[[File:Cody-wyrdlight-804536D.jpg|thumb|175px|Cody's commemorative statue at the Farnborough Air Sciences Trust Museum]]A team of volunteer enthusiasts built a full-sized replica of British Army Aeroplane No 1 to commemorate the 100th anniversary of the first flight. It is on permanent display at the [[Farnborough Air Sciences Trust]] Museum in Farnborough.<ref>{{cite web|url=http://www.airsciences.org.uk/index.html|title=Farnborough Air Sciences Trust&nbsp;— about the Trust and how to visit the museum}}</ref><ref>{{cite web|url=http://www.codyflyerproject.com/|title=Farnborough Air Sciences Trust&nbsp;— Cody flyer project}}</ref>  The display is about three hundred metres from the take-off point of the historic flight.

Memorials to Cody include:
*When Cody was testing his first aeroplane, he tied it to a tree in order to assess the pulling power of its propeller. The tree became known as the Cody Tree and survived for many years. Later an aluminium replica was cast by apprentices of the Royal Aircraft Establishment, and this now marks the spot.<ref>{{cite web|url=http://www.aviationarchive.org.uk/Gpages/html/G3089.html|title=Brabazon Flying Over Cody Tree|website=aviationarchive.org.uk|accessdate=2 June 2014}}</ref>
*A commemorative statue of Cody, adjacent to the Farnborough Air Sciences Trust Museum, was unveiled by 94-year-old [[Eric Brown (pilot)|Captain Eric "Winkle" Brown]] in August 2013.<ref>http://www.codystatue.org.uk</ref><ref>[http://www.bbc.co.uk/news/uk-england-hampshire-23591409]</ref>
*Cody Technology Park, Farnborough was named in his honour.
*Cody Cricket Club, based at Farnborough, was named after him.
*Cody lived for the last few years of his life in [[Ash Vale]], [[Surrey]], where his former house is marked by a blue plaque, and is adjacent to a car dealership called Cody's which features an aeroplane on its sign.
The [[Aldershot Military Museum]] has artifacts relating to Cody.

In April 2013, two of Vivian Cody's (real name Vivian King) grandsons appeared on [[BBC One]]'s ''[[Antiques Roadshow]]'' with two Michelin Trophies, won by Cody, which were each valued at £25,000 – £30,000.<ref>{{cite web|url=http://www.bbc.co.uk/programmes/b01s7v1r|title=Farnborough 2: Series 35 Episode 21 of 25|date=28 April 2013|publisher=bbc.co.uk|accessdate=28 April 2013}}</ref>

===The Broomfield hoax===
G. A. Broomfield had been an assistant and friend to Cody after he left the Army and moved to Laffan's Plain. In 1948, he presented to the [[Science Museum, London|Science Museum]], Kensington, a model of the No.1 machine which was wrong in many details.<ref>Walker, Vol. II (1974), Pages 147–149</ref> He claimed that the first flight had been in May 1908. This was one month before a similar claimed first flight by [[Alliott Verdon Roe|A. V. Roe]], and Bloomfield wanted to establish primacy for Cody.<ref>Walker, Vol. II (1974), Page 146: "Broomfield's original objective seems to have been to credit Cody with a flight antedating one that A. V. Roe was alleged to have made in June 1908, and so place him indisputably as the first man to fly in Britain."</ref> Roe's claim was later disallowed, but by then Broomfield was too deep in his story to back out. 

The next year, Broomfield made the same claim to the [[Royal Aircraft Establishment]], and caused a new plaque with the date of 16 May 1908 to be made for the Cody Tree. The story first appeared in print in 1951, and again in 1952, in articles published by independent researchers. A fuller account of the fictitious day's flying appeared in Broomfield's biography of Cody, ''Pioneer of the Air'', 1953. It was endorsed by [[Geoffrey de Havilland]] who provided the Foreword and [[C. G. Grey]], editor of the journal ''Aeroplane'', who wrote the Introduction. The hoax was not exposed until 1958, the 50th anniversary of flight in Britain, when three investigators, G. W. B. Lacey from the Science Museum, A. T. E. Bray from the R.A.E. and the independent historian [[Charles Gibbs-Smith]], asked Broomfield for clarifications.<ref>Walker, Vol. II (1974), Chap. 6.</ref>

== List of aircraft ==
* Cody kite (1901)
* [[Cody glider-kite]] (1905)
* [[Cody power-kite]] (1907)
* [[British Army Aeroplane No 1]] (1908) or Cody No. 1 or Cody Cathedral
* [[Cody Michelin Cup Biplane]] (1910)
* [[Cody Circuit of Britain Biplane]] (1911)
* [[Cody monoplane]] (1912)
* [[Cody V]] (1912) (Cody Military Trials Biplane)
* [[Cody Floatplane]] (1913)

==Notes==
{{Reflist}}

==References==
* Kuntz, Jerry ''A Pair of Shootists: The Wild West Story of S. F. Cody and Maud Lee''. Norman. OK: University of Oklahoma Press, 2010. ISBN 978-0-8061-4149-7
* Lewis, P ''British Aircraft 1809–1914'' London: Putnam, 1962.
* Pawle, Gerald ''The Secret War 1939–45''. London, Harrap, 1956.
* Taylor, M.J.H. and Mondey, David ''Milestones of Flight''. London: Jane's, 1983. ISBN 0-7106-0201-4
* Walker, P.; "Early Aviation at Farnborough" (Two volumes), Macdonald (Volume I 1971, Volume II 1974).

==External links==
{{Commons category|Samuel Franklin Cody}}
*[http://www.sfcody.org.uk/ Samuel Franklin Cody] by Jean Roberts
*[http://texashistory.unt.edu/permalink/meta-pth-17180 Photograph of Samuel Cody], hosted by the [http://texashistory.unt.edu/ Portal to Texas History]
*[http://autry.iii.com/search/c=MIMSY%2096.159 Samuel Franklin Cody Papers at the Autry National Center]
*[http://www.flightglobal.com/pdfarchive/view/1913/1913%20-%200879.html S F Cody – The man and his work] – obituary in ''Flight''
*[http://www.flightglobal.com/pdfarchive/view/1958/1958%20-%200674.html S. F. CODY – A Personal Reminiscence] G A Broomhead.
*[http://www.flightglobal.com/pdfarchive/view/1958/1958%20-%200683.html S. F. Cody: an Historian's Comments] C Gibbs-Smith
*[http://www.iwm.org.uk/collections/item/object/205192333 S F Cody demonstrates one of his kite designs to the Royal Navy]
*[http://www.iwm.org.uk/collections/item/object/205021195 A 1908 aerial photograph of HMS ''Revenge'' taken from his kite by S F Cody]

{{Cody aircraft}}
{{Aviators killed in early aviation accidents}}

{{Authority control}}

{{DEFAULTSORT:Cody, Samuel}}
[[Category:1867 births]]
[[Category:1913 deaths]]
[[Category:People from Davenport, Iowa]]
[[Category:American aviators]]
[[Category:British aviators]]<!--naturalised-->
[[Category:American kite fliers]]
[[Category:Aviators from Texas]]
[[Category:Aviators killed in aviation accidents or incidents in the United Kingdom]]
[[Category:Burials at Aldershot Military Cemetery]]
[[Category:Wild West shows]]