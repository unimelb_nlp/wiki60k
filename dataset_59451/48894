{{Infobox scientist
| name = John M Kovac
| image = <!-- [[File:JMK and telescopes.jpg|thumb|Photo of John M. Kovac in Antartica]] deleted from commons-->
| caption = 
| birth_date = 
| birth_place = 
| death_date = 
| death_place = 
| residence = [[United States]]
| nationality = [[United States|American]]
| field = [[Experimental Physics]] and [[Cosmology]]
| work_institution =[[California Institute of Technology|Caltech]]<br>[[Harvard University|Harvard]]
| alma_mater = [[Princeton University]]<br>
[[University of Chicago]]
| thesis_title = [http://adsabs.harvard.edu/abs/2004PhDT.......204K Detection of Polarization in the Cosmic Microwave Background using DASI]
| doctoral_advisor = [[John Carlstrom]]
| doctoral_students =
| influences = 
| influenced =
| known_for  = [[BICEP2]], [[BICEP and Keck Array]]
| societies = 
| prizes = 
| spouse = 
| children = 
| website =  
| footnotes = 
}}
'''John M. Kovac''' (born 1970) is an American physicist and astronomer. His cosmology research, conducted at the [[Harvard-Smithsonian Center for Astrophysics]] in [[Cambridge, Massachusetts]], focuses on observations of the [[Cosmic Microwave Background]] (CMB) to reveal signatures of the physics that drove the birth of the universe, the creation of its structure, and its present-day expansion. Currently, Kovac is an Associate Professor of Astronomy and Physics at [[Harvard University]].<ref>{{cite web|title=Harvard Astronomy Department Website|url=http://astronomy.fas.harvard.edu/people/john-m-kovac}}</ref>

== Career ==

He is the principal investigator of the [[BICEP2]] telescope, which is part of the [[BICEP and Keck Array]] series of experiments.<ref>{{cite web|title=BICEP2 Website|url=http://www.cfa.harvard.edu/CMB/bicep2/}}</ref><ref>{{cite web|title=Award Abstract #0742818   Collaborative Research: BICEP2 and SPUD - A Search for Inflation with Degree-Scale Polarimetry from the South Pole|url=http://www.nsf.gov/awardsearch/showAward?AWD_ID=0742818&HistoricalAwards=false|publisher=NSF}}</ref><ref>{{cite web|title=Award Abstract #1044978   Collaborative Research: BICEP2 and SPUD - A Search for Inflation with Degree-Scale Polarimetry from the South Pole|url=http://www.nsf.gov/awardsearch/showAward?AWD_ID=1044978&HistoricalAwards=false|publisher=NSF}}</ref>  Measurements announced on 17 March 2014 from the BICEP2 telescope give support to the idea of cosmic [[inflation (cosmology)|inflation]], by reporting the first evidence for a primordial [[B-modes|B-Mode pattern]] in the polarization of the CMB.<ref name=bicep2-nytimes>{{cite web|last=Overbye|first=Dennis|authorlink=Dennis Overbye|title=Space Ripples Reveal Big Bang’s Smoking Gun|url=http://nyti.ms/1ebT9LS|publisher=New York Times|accessdate=19 March 2014}}</ref><ref name="NYT-20140324">{{cite news |last=Overbye |first=Dennis |authorlink=Dennis Overbye |title=Ripples From the Big Bang |url=https://www.nytimes.com/2014/03/25/science/space/ripples-from-the-big-bang.html |date=24 March 2014 |work=[[New York Times]] |accessdate=24 March 2014 }}</ref><ref name=bicep2-arxiv>{{Cite arXiv|last=BICEP2 Collaboration|title=BICEP2 I: Detection Of B-mode Polarization at Degree Angular Scales|eprint=1403.3985|first1=BICEP2|last2= R Ade|first2=P. A.|last3= Aikin|first3=R. W.|last4= Barkats|first4=D.|last5= Benton|first5=S. J.|last6= Bischoff|first6=C. A.|last7= Bock|first7=J. J.|last8= Brevik|first8=J. A.|last9= Buder|first9=I.|last10= Bullock|first10=E.|last11= Dowell|first11=C. D.|last12= Duband|first12=L.|last13= Filippini|first13=J. P.|last14= Fliescher|first14=S.|last15= Golwala|first15=S. R.|last16= Halpern|first16=M.|last17= Hasselfield|first17=M.|last18= Hildebrandt|first18=S. R.|last19= Hilton|first19=G. C.|last20= Hristov|first20=V. V.|last21= Irwin|first21=K. D.|last22= Karkare|first22=K. S.|last23= Kaufman|first23=J. P.|last24= Keating|first24=B. G.|last25= Kernasovskiy|first25=S. A.|last26= Kovac|first26=J. M.|last27= Kuo|first27=C. L.|last28= Leitch|first28=E. M.|last29= Lueker|first29=M.|last30= Mason|first30=P.|display-authors=29|class=astro-ph.CO|year=2014}}</ref> If confirmed, this measurement provides a direct image of primordial [[gravitational waves]] and the [[Quantum gravity|quantization of gravity]].

Prior to BICEP2, as a graduate student at the [[University of Chicago]], Kovac worked on the [[Degree Angular Scale Interferometer]] led by [[John Carlstrom]], which in 2002 announced the first detection of polarization in the CMB.<ref>{{cite journal
 | doi = 10.1038/nature01269
 | pmid = 12490941
 | author = Kovac, J. M.
 | display-authors = etal
 | title = Detection of polarization in the cosmic microwave background using DASI
 | journal = Nature
 |date=Dec 2002
 | arxiv = astro-ph/0209478
 | volume = 420
 | issue = 6917
 | pages = 772–787
 | bibcode = 2002Natur.420..772L}}</ref> In 2003, Kovac moved to Caltech as a Millikan Postdoctoral Fellow, beginning work under [[Andrew Lange]] on the [[QUaD]] telescope and on BICEP1, the predecessor of BICEP2.  After BICEP1's deployment to the South Pole in 2006, at Lange's invitation Kovac joined the research faculty of Caltech as a Kilroy Fellow and led the team that proposed BICEP2. In 2009 Kovac joined the faculty at Harvard University.<ref>{{cite news|last=Cowen|first=Ron|title=Cosmology: Polar star|url=http://www.nature.com/news/cosmology-polar-star-1.14954|accessdate=1 April 2014|newspaper=Nature|date=31 March 2014|doi=10.1038/508028a}}</ref>

== Education and early life ==

Kovac was born in Princeton, New Jersey. He attended [[Jesuit High School (Tampa)|Jesuit High School]] in Tampa, Florida.<ref name="TampaTribune-20140324">{{cite news |last=Stockfisch |first=Jerome |title=Tampa education inspired head of Big Bang team |url=http://tbo.com/news/education/tampa-education-inspired-head-of-big-bang-team-20140319/ |date=19 March 2014 |work=[[The Tampa Tribune]] |accessdate=25 March 2014 }}</ref> He received a bachelor's degree in Mathematics from [[Princeton University]]. He went on to the [[University of Chicago]] to receive a Masters and Doctorate in Physics in 2004. His thesis advisor was [[John Carlstrom]].

== Awards ==

In 2013 Kovac received the National Science Foundation Career Award.<ref>{{cite web|url=http://astronomy.fas.harvard.edu/news/john-kovac-assistant-professor-harvards-astronomy-and-physics-departments-has-been|title=John Kovac, Assistant Professor in Harvard's Astronomy and Physics departments, has been awarded the Faculty Early Career Development (CAREER) Award by the National Science Foundation |accessdate=2014-03-27}}</ref> In 2011 Kovac was selected as a [[Sloan Fellowship|Sloan Research Fellow]].<ref>{{cite web|url=http://www.sloan.org/fileadmin/media/files/goroff/2011_srf_nytimes_ad_vf.pdf|title=Alfred P. Sloan Research Fellowships 2011 |accessdate=2014-03-27}}</ref> He was awarded the 2002 - 2003 Sugarman Award by the [[Enrico Fermi Institute]].

== References ==

{{reflist}}

== External links ==
*[http://astronomy.fas.harvard.edu/ Department of Astronomy - Harvard University]
*[http://www.cfa.harvard.edu/CMB/bicep2/ BICEP2 and Research Group Page]

{{DEFAULTSORT:Kovac, John M.}}
[[Category:American people of Czech descent]]
[[Category:American astrophysicists]]
[[Category:1970 births]]
[[Category:Living people]]
[[Category:Princeton University alumni]]
[[Category:University of Chicago alumni]]
[[Category:Harvard University faculty]]
[[Category:Jesuit High School (Tampa) alumni]]