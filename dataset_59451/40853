{{Infobox scientist
| name              = Elisa Victoria Quintana
| image             =
| image_size        = 
| alt               = Elisa Quintana
| caption           = Quintana backed by an artists impression of [[Kepler 186f]]
| birth_date        = {{Birth year and age|1973}}
| birth_place       = 
| death_date        = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} (death date then birth date) -->
| death_place       = 
| resting_place             = 
| resting_place_coordinates = <!-- {{Coord|LAT|LONG|type:landmark|display=inline,title}} -->
| other_names       = 
| residence         = 
| citizenship       = 
| nationality       = 
| fields            = [[Astronomy]]
| workplaces        = Senior Fellow at [[NASA Ames Research Center]]
| alma_mater        = [[University of Michigan]], [[Grossmont College]], [[University of California at San Diego]] 
| thesis_title      = 
| thesis_url        = 
| thesis_year       = 
| doctoral_advisor  = [[Fred Adams]]
| academic_advisors = 
| doctoral_students = 
| notable_students  = 
| known_for         = [[Astronomy]]
| author_abbrev_bot = 
| author_abbrev_zoo = 
| influences        = 
| influenced        = 
| awards            = 
| signature         = <!--(filename only)-->
| signature_alt     = 
| website           = <!-- {{URL|www.example.com}} -->
| footnotes         = 
| spouse            = 
| children          = 
}}
'''Elisa Victoria Quintana''' is a [[scientist]] working in the field of [[astronomy]] and [[planetary science]] at [[NASA Ames Research Center]]. Her research focuses the detection and characterization of [[exoplanets]] in addition to studying how they form. She is known for the detection of [[Kepler 186f]],<ref name="NYT-20140417"/> the first Earth-sized planet found in the [[habitable zone]] of a [[star]] other than the [[Sun]].<ref name="AP-20140417"/><ref name="BBC-20140417"/>

==Early life and education==
Quintana was born in [[Silver City, New Mexico]].<ref name="early-life"/> Her father [[Leroy Quintana]], is a [[Chicano poetry|Chicano poet]] and her grandfather was a miner who appeared in [[Hollywood blacklist|blacklisted]] movie [[Salt of the Earth (1954 film)|Salt of the Earth]]. Aged 9 she moved to [[San Diego]]. She attended [[Grossmont College]] and transferred to the [[University of California at San Diego]] where she obtained a [[Bachelor of Science]] degree in Physics. During her time as an undergraduate Quintana worked on [[Orbital mechanics|astrodynamics]] the [[KidSat]] program (later renamed [[Sally Ride EarthKAM|EarthKAM]]) with first US woman [[astronaut]] [[Sally Ride]] who was a professor in San Deigo. She received [[Master's degree]]s in both [[Aerospace engineering|Aerospace Science]] and [[Physics]] from the [[University of Michigan]] and earned her [[Doctor of Philosophy|PhD]] in Physics from the [[University of Michigan]] in 2004. Her PhD thesis for on the topic of [[planet formation]] in [[binary star]] systems. Quintana was amongst the first people to study whether planets could form in the [[Alpha Centauri]] system.<ref name="alpha-cen"/>

==Academic career==
Based at [[NASA Ames Research Center]], Quintana has been a member of the NASA [[Kepler Mission]] Team since 2006. She worked as a [[software engineer|scientific programmer]] developing the Kepler pipeline for which she was awarded the NASA Software of the Year in 2010.<ref name="software-2010"/> She was part of the team that discovered the first rocky exoplanet [[Kepler-10b]], the first exoplanet to orbit the [[habitable zone]] of another star [[Kepler-22b]] and the first Earth-sized exoplanet [[Kepler-20e]]. In 2014 she led the team that discovered [[Kepler-186f]], an earth-sized exoplanet orbiting in the habitable zone a [[red dwarf]] star<ref name="k186f-2014"/> which was published in the journal [[Science]].<ref name="SCI-20140418"/> Quintana received the 2015 Scientist of the Year award from Great Minds in STEM for her discovery of Kepler-186f and contribution to science.<ref name="henaac-award"/><ref name="henaac-video"/> More recently Quintana has been studying the frequency of [[Giant-impact hypothesis|giant impacts]]  on exoplanets and comparing how their frequency compares with [[Earth]].<ref name="giant-impacts-paper"/><ref name="giant-impacts-nova"/>

Quintana is one of the few female [[hispanic]] scientist in [[astronomy]]<ref name="woc-science"/> and her contributions to the [[Latino]] community were recognized when the [[Los Angeles Theatre Center]] awarded her the 2014 [[Lupe Ontiveros]] Dream Award.<ref name="dream-award"/>

==Awards and honors==
*Hispanic Engineers National Achievement Awards Scientist of the Year (2015)
*Lupe Ontiveros Dream Award (2014)
*NASA Software of the Year (2010)

==References==
{{reflist |refs=
<ref name="early-life">{{cite news |last=Lartaud |first=Derek |title=Career Spotlight: The Planet-Searching Physicist |url=http://ww2.kqed.org/quest/2016/01/07/career-spotlight-the-planet-searching-physicist/ |date=7 January 2016 |work=[[KQED]] }}</ref>
<ref name="NYT-20140417">{{cite news |last=Chang |first=Kenneth |title=Scientists Find an 'Earth Twin', or Maybe a Cousin |url=https://www.nytimes.com/2014/04/18/science/space/scientists-find-an-earth-twin-or-maybe-a-cousin.html |date=17 April 2014 |work=[[New York Times]] }}</ref>

<ref name="AP-20140417">{{cite news |last=Chang |first=Alicia |title=Astronomers spot most Earth-like planet yet |url=http://apnews.excite.com/article/20140417/DAD832V81.html |date=17 April 2014 |agency=Associated Press |deadurl=no |archivedate=2014-04-18 |archiveurl=https://web.archive.org/web/20140418172517/http://apnews.excite.com/article/20140417/DAD832V81.html }}</ref>

<ref name="BBC-20140417">{{cite news |last=Morelle |first=Rebecca |title='Most Earth-like planet yet' spotted by Kepler |url=http://www.bbc.co.uk/news/science-environment-27054366 |date=17 April 2014 |work=[[BBC News]] |deadurl=no |archivedate=2014-04-18 |archiveurl=https://web.archive.org/web/20140418172858/http://www.bbc.co.uk/news/science-environment-27054366 }}</ref>

<ref name="SCI-20140418">{{Cite journal| doi = 10.1126/science.1249403| title = An Earth-Sized Planet in the Habitable Zone of a Cool Star| journal = [[Science (journal)|Science]]| volume = 344| issue = 6181| pages = 277–280| date = 2014-04-18| url=http://www.sciencemag.org/content/344/6181/277| arxiv=1404.5667| last1 = Quintana | first1 = E. V.| last2 = Barclay | first2 = T.| last3 = Raymond | first3 = S. N.| last4 = Rowe | first4 = J. F.| last5 = Bolmont | first5 = E.| last6 = Caldwell | first6 = D. A.| last7 = Howell | first7 = S. B.| last8 = Kane | first8 = S. R.| last9 = Huber | first9 = D.| last10 = Crepp | first10 = J. R.| last11 = Lissauer | first11 = J. J. | authorlink11=Jack J. Lissauer| last12 = Ciardi | first12 = D. R.| last13 = Coughlin | first13 = J. L.| last14 = Everett | first14 = M. E.| last15 = Henze | first15 = C. E.| last16 = Horch | first16 = E.| last17 = Isaacson | first17 = H.| last18 = Ford | first18 = E. B.| last19 = Adams | first19 = F. C.| last20 = Still | first20 = M.| last21 = Hunter | first21 = R. C.| last22 = Quarles | first22 = B.| last23 = Selsis | first23 = F.|bibcode = 2014Sci...344..277Q | pmid=24744370}}</ref>

<ref name="k186f-2014">{{cite web |title=Kepler-186f, the First Earth-size Planet in the Habitable Zone |publisher=NASA |url=http://www.nasa.gov/ames/kepler/kepler-186f-the-first-earth-size-planet-in-the-habitable-zone}}</ref>

<ref name="alpha-cen">{{cite journal
 |author1=Quintana, E. V. |author2=Lissauer, J. J. |author3=Chambers, J. E. |author4=Duncan, M. J. |author5= | title=Terrestrial Planet Formation in the Alpha Centauri System
 | journal=[[Astrophysical Journal]]
 | date=2002 | volume='''2''', part 1 | issue=2 | pages=982–996
 | doi=10.1086/341808 | bibcode=2002ApJ...576..982Q
}}</ref>

<ref name="software-2010">{{cite web |title=NASA's Kepler Mission Wins 2010 Software Of The Year Award |publisher=NASA |url=http://www.nasa.gov/home/hqnews/2010/oct/HQ_10-245_Kepler_Software_Award.html}}</ref>

<ref name="dream-award">{{cite web |title=Dr. Elisa Quintana receives the Lupe Ontiveros Dream Award |work=Space Science and Astrobiology at Ames |publisher=NASA Ames Research Center |url=http://spacescience.arc.nasa.gov/story/dr-elisa-quintana-receives-lupe-ontiveros-dream-award}}</ref>

<ref name="henaac-award">{{cite web|title=2015 HENAAC Award Winners |publisher= Great Minds In STEM |url=http://www.greatmindsinstem.org/professionals/award-winners-2015.html}}</ref>

<ref name="henaac-video">{{YouTube|id=Ja2jCj5YDN4|title="2015 GMiS HENAAC Awards Part 12 - Charles Bolden Message, Scientist of the Year Dr. Elisa Quintana"}}</ref>

<ref name="giant-impacts-paper">{{cite journal 
| last1 = Quintana | first1 = E. V.| last2 = Barclay | first2 = T.| last3 = Borucki | first3 = W. J.| last4 = Rowe | first4 = J. F.| last5 = Chambers | first5 = J. E.| title=The Frequency of Giant impacts on Earth-like Worlds
 | journal=[[Astrophysical Journal]]
 | date=2016 | volume='''821''' |issue=2 | pages=126
 | doi=10.3847/0004-637X/821/2/126 | bibcode=2016ApJ...821..126Q
}}</ref>

<ref name="giant-impacts-nova">{{cite news |last=Kohler |first=Susanna |title=Giant Impacts on Earth-Like Worlds |url=http://aasnova.org/2016/05/09/giant-impacts-on-earth-like-worlds/ |date=9 May 2016 |work=[[American Astronomical Society]]}}</ref>

<ref name="woc-science">{{cite web |title=Research Scientist Highlight: Dr. Elisa Quintana |publisher=Astronomy in Color |url=http://astronomyincolor.blogspot.com/2016/03/professional-highlight-dr-elisa-quintana.html}}</ref>
|30em}}

{{Authority control}}

{{DEFAULTSORT:Quintana, Elisa}}
[[Category:University of Michigan alumni]]
[[Category:Grossmont College alumni]]
[[Category:American astronomers]]
[[Category:Women astronomers]]
[[Category:University of California, San Diego alumni]]
[[Category:Living people]]
[[Category:1973 births]]
[[Category:Discoverers of exoplanets]]