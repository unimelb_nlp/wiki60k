{{Use dmy dates|date=December 2016}}
{{Use British English|date=December 2016}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]] and [[Wikipedia:WikiProject Biography/Military]]. -->
{{Infobox military person
| name          =Edwin Louis Benbow
| image         =
| caption       =
| birth_date    = {{Birth date|1895|12|10|df=y}}
| death_date    = {{Death date and age|1918|5|30|1895|12|10|df=y}}
| placeofburial_label =
| placeofburial = [[Duhallow ADS Commonwealth War Graves Commission Cemetery|Duhallow ADS Cemetery]], [[Ypres]], [[West Flanders]], [[Belgium]]
| birth_place   = [[Abbotsbury]], Dorset, England
| death_place   =
| placeofburial_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| nickname      =Lobo
| allegiance    =United Kingdom
| branch        =British Army<br/>Royal Air Force
| serviceyears  =1915–1918
| rank          =Captain
| unit          =[[Royal Field Artillery]]<br/>[[No. 40 Squadron RAF|No. 40 Squadron RFC]]<br/>[[No. 85 Squadron RAF]]
| commands      =
| battles       =
| awards        =[[Military Cross]]
| relations     =
| laterwork     =
}}
Captain '''Edwin Louis Benbow''' {{post-nominals|country=GBR|MC}} (10 December 1895 – 30 May 1918) was an English [[flying ace]] during the [[First World War]], credited with eight victories, comprising six destroyed and one shared destroyed, and one 'out of control'. He was the only pilot to gain 'ace' status flying the [[Royal Aircraft Factory F.E.8]] exclusively.<ref>Guttman & Dempsey (2009), pp.63–65.</ref>

==Wartime career==
Benbow joined the [[Royal Field Artillery]] in February 1915, and served for a year<ref name="P63">Guttman & Dempsey (2009), p.63.</ref> being commissioned a [[second lieutenant]] on 27 May 1915.<ref>{{London Gazette |issue=29184 |date=4 June 1915 |startpage=5480 |supp=yes |nolink=yes }}</ref> He was appointed to be a [[Flying Officer|flying officer (observer)]], effective 10 March 1916, before being seconded to the [[Royal Flying Corps]] on 15 April 1916.<ref>{{London Gazette |issue=29549 |date=14 April 1916 |startpage=3998 |supp=yes |nolink=yes }}</ref> He served his first eight months in aviation as an observer/gunner. He then trained as a pilot,<ref name="P63"/> was appointed a [[flying officer]] on 7 July 1916,<ref>{{London Gazette |issue=29693 |date=1 August 1916 |startpage=7655 |endpage=7656 |supp=yes |nolink=yes }}</ref>  and was assigned to [[No. 40 Squadron RAF|40 Squadron]] as an F.E.8 pilot. He achieved all his successes while with this squadron; on 20 October 1916, using F.E.8, Serial No. 7627, he destroyed an [[Albatros D.II]] for his first victory. Two days later he set a German two-seater afire over [[Vimy]].<ref name="P63"/>

On 16 November 1916 Benbow shot down an [[Albatros Flugzeugwerke|Albatros]] two-seater. On 4 December he downed another Albatros D.II. On the 20th, he felled another Albatros two-seater; Benbow thus became the only F.E.8 ace, and No. 7627 the only Royal Aircraft Factory F.E.8 to be involved in the shooting down of five or more enemy aircraft.

On the early afternoon of 23 January 1917, Benbow was in the midst of a dogfight when his gun jammed. While clearing his weapon, he evaded a head-on assault by the [[Red Baron]], who went on to down Benbow's squadron comrade Lt. J. Hay for ''[[Jasta 11]]''{{'}}s first victory.<ref name="P63"/> Benbow did not succeed in returning to the attack on that occasion, but on 14 February using F.E.8 Serial No. A4871, he destroyed another Albatros D.II. His seventh victory came the following day, with Benbow's only "out of control" claim.<ref>Guttman & Dempsey (2009), p.64.</ref>

On 6 March, Benbow and the Red Baron clashed again<ref name="P6465">Guttman & Dempsey (2009), p.64–65,</ref> when nine F.E.8s of 40 Squadron fought five ''Jasta 11'' aircraft led by Richthofen. While the Baron was attacking a [[Sopwith 1½ Strutter]], Benbow shot him down, forcing him to land near [[Hénin-Beaumont|Hénin-Liétard]] with a damaged engine, spraying fuel from holed tanks, and seemingly on fire, for Benbow's eighth victory (although Richthofen survived).<ref name="P6465"/> On 12 March, 40 Squadron began its re-equipment with [[Nieuport 17]] [[tractor configuration|"tractors"]]. A week later, Benbow was [[wounded in action]] by anti-aircraft shrapnel, ending his first combat assignment in France.<ref>Guttman & Dempsey (2009), p.66.</ref>

Benbow would serve as an instructor in his time away from the front. He was appointed a [[Flight Commander]] with the concomitant rank of Temporary Captain on 31 March 1917,<ref>{{London Gazette |issue=30085 |date=22 May 1917 |startpage=5009 |supp=yes |nolink=yes }}</ref> and posted to [[Billy Bishop]]'s No. 85 Squadron in May 1918. On the 30th while piloting [[Royal Aircraft Factory S.E.5]]a, Serial No.C1861, he was shot down and killed by [[Hans-Eberhardt Gandert]] of ''[[Jasta 51]]''.<ref name="P6667">Guttman & Dempsey (2009), p.66–67.</ref>

==Honours and awards==
;Military Cross
: 2nd Lt. (temp. Lt.) Edwin Louis Benbow, Royal Field Artillery and Royal Flying Corps.
: For conspicuous gallantry in action. He has on several occasions displayed great courage and skill, and has destroyed four enemy machines under difficult conditions.<ref>{{London Gazette |issue=29940 |date=13 February 1917 |startpage=1539 |supp=yes |nolink=yes }}</ref>

==References==
;Notes
{{reflist|30em}}
;Bibliography
* {{cite book |title=Pusher Aces of World War I |first1=Jon |last1=Guttman |first2=Harry |last2=Dempsey |publisher=Osprey Pub. Co. |year=2009 |isbn=978-1-84603-417-6}}

==External links==
* {{cite web |url= http://www.theaerodrome.com/aces/england/benbow.php |title=Edwin Louis Benbow |work=theaerodrome.com |year=2014 |accessdate=26 August 2014}}
* {{cite web |url= http://www.roll-of-honour.com/Dorset/Abbotsbury.html |title=Abbotsbury War Memorial, Dorset |first=Brian |last=Sapsed |work=Roll of Honour |year=2010 |accessdate=26 August 2014}}

{{DEFAULTSORT:Benbow, Edwin}}
[[Category:1895 births]]
[[Category:1918 deaths]]
[[Category:People from West Dorset (district)]]
[[Category:Royal Artillery officers]]
[[Category:Royal Flying Corps officers]]
[[Category:Royal Air Force personnel of World War I]]
[[Category:British World War I flying aces]]
[[Category:British military personnel killed in World War I]]
[[Category:Aviators killed by being shot down]]
[[Category:Recipients of the Military Cross]]