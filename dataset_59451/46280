{{Use dmy dates|date=February 2015}}
{{Infobox organization
| name         = Asian Institute
| image        = Asian Institute Logo.jpg
| location     =[[University of Toronto]]
| leader_name  =[[Ritu Birla]]
| leader_title =Director
| staff        = David Chu Professor and Director in Asia Pacific Studies: Takashi Fujitani
*Director, Centre for South Asian Studies: Ritu Birla 
*Director, Contemporary Asian Studies Program: Dylan Clark
*Director, Centre for Southeast Asian Studies: Nhung Tuyet Tran
*Centre for the Study of Korea: Ito Peng, Interim Director
| website      =http://www.munk.utoronto.ca/ai/Default.aspx}}

The '''Asian Institute''' is a research centre at the [[Munk School of Global Affairs]] at the [[University of Toronto]], and is located in the historical Devonshire House, a former residential hall of the university's [[Trinity College, Toronto|Trinity College]]. [[Ritu Birla]] is the Richard Charles Lee Director of the Asian Institute. 

==Research Focus==
The Asian Institute has over one hundred affiliated scholars whose research focuses on the geopolitical region of Asia. Research at the Asian Institute is interdisciplinary and ranges from the humanities to the social sciences.<ref>''The Asian Institute'' "Home Page" http://www.munk.utoronto.ca/ai/ (accessed 3 May 2013).</ref> Examples of this interdisciplinary approach are the Global Ideas Institute, the Contemporary Asian Studies undergraduate program,<ref>UofT News, " Students dig into the complexities of contemporary Asia." (Accessed 30 May 2013) http://www.news.utoronto.ca/students-dig-complexities-contemporary-asia</ref> and the recent student run conference for Sustainable Development that examined how economic and social developments operate in the "regional" context.<ref>''The Varsity'' "Redefining Sustainable Development," http://thevarsity.ca/2012/03/12/redefining-sustainable-development/ (accessed 30 April 2013).</ref>

==Research centres==

===Centre for South Asian Studies===
The Centre for South Asian Studies (CSAS) is a constitutive unit of the Asian Institute at the [[Munk School of Global Affairs#Research Centres|Munk School of Global Affairs]].<ref>''The Varsity'' "South Asian Studies Moves to Munk" http://thevarsity.ca/2009/03/30/south-asian-studies-moves-to-munk/ (accessed 19 April 2013)</ref> [[Ritu Birla]] is the former director of the centre. CSAS was established in 1981 and is a key research centre of the Faculty of Arts and Sciences at [[University of Toronto]] with core faculty across the University of Toronto's downtown St. George, [[University of Toronto Scarborough|UTSC]] and [[University of Toronto Mississauga|UTM]] campuses. CSAS hosts and organizes many public lectures and academic events throughout the school year.<ref>''The Canadian Journalism Project'' "The challenges of bringing diversity into a newsroom: SAJA panel examines how South Asian stories are covered in Canada " http://j-source.ca/article/challenges-bringing-diversity-newsroom-saja-panel-examines-how-south-asian-stories-are-cover (accessed 25 April 2013).</ref>

The CSAS examines "South Asia" and its regions as objects of knowledge, from mythic to governmental, to geopolitical, and [[Postcolonialism|"Postcolonial."]] CSAS programming addresses questions as wide-ranging as the workings of postcolonial democracy, law and activism; histories and contemporary configurations of the sacred and secular; political economy and cultures of capitalism; media, technology and the public sphere; the material and imaginative terrains of literary and visual cultures; and the present life of ancient civilizations.<ref name="CSAS Website">accessed 20 March 2013, {{cite web|url=http://www.utoronto.ca/csas/ |title=Archived copy |accessdate=2014-02-11 |deadurl=yes |archiveurl=https://web.archive.org/web/20140122194508/http://www.utoronto.ca:80/csas/ |archivedate=22 January 2014 |df=dmy }}</ref>
[[File:South Asia (ed)update.PNG|right|250px|thumb|Geopolitical region of South Asia.]]

==== Graduate and Undergraduate Study ====

CSAS does not grant undergraduate or graduate degrees, but has a collaborative program for students interested in pursuing a [[Master of Arts|MA]] or [[Doctor of Philosophy|PhD]] who have already been accepted to study at the University of Toronto. The Collaborative Master's and Doctoral Program in South Asian Studies focuses on basic methodological grounding for students working towards their research degrees. The program builds from an interdisciplinary and critical study of South Asia and as starting point to examine the development of global processes.<ref>"University of Toronto Graduate Program" Centre for South Asian Studies (Accessed 18 April 2013) {{cite web|url=http://www.gradschool.utoronto.ca/programs/collaborative/South_Asian_Studies.htm |title=Archived copy |accessdate=2014-02-11 |deadurl=yes |archiveurl=https://web.archive.org/web/20130331013229/http://www.gradschool.utoronto.ca:80/programs/collaborative/South_Asian_Studies.htm |archivedate=31 March 2013 |df=dmy }}</ref>

The CSAS offers a minor in South Asian Studies for undergraduate students that is a part of the Contemporary Asian Studies program at the Asian Institute.  The South Asian Studies minor begins with an introduction into the study of Bangladesh, India, Pakistan, Nepal, and Sri Lanka. Students can also take a variety of undergraduate courses from other departments and faculties that can be used toward a CSAS minor.<ref>Faculty of Arts and Sciences University of Toronto, "South Asian Studies" (Accessed 18 April 2013) http://www.artsci.utoronto.ca/futurestudents/academics/progs/southasian</ref>
[[File:Munk Centre for International Studies.JPG|left|250px|thumb|Munk School of Global Affairs.]]

===Other centres===
*Central and Inner Asia Studies
*Centre for Southeast Asian Studies
*Centre for the Study of Korea
*Dr. David Chu Program in Asia- Pacific Studies (soon to be the Contemporary Asian Studies program).

== Notable Lectures ==
[[File:Asia (orthographic projection).svg|right|250px|thumb|Geopolitical Region of Asia.]]


[[Partha Chatterjee (scholar)]], "Nationalism, Internationalism and Cosmopolitanism: Some Observations from Modern Indian History." 

[[Chen Kaige]], [[Xie Fei (director)|Xie Fei]], Bart Testa, Chen Biqiang, on the panel, "A Century of Chinese Cinema: Buried Treasures of Chinese Silent Cinema."

[[Elizabeth J. Perry|Elizabeth J. Perry's, ]] "The Culture of Chinese Communist Resilience: Mining the Anyuan Revolutionary Tradition."

[[Wang Hui (intellectual)|Wang Hui's, ]] "The Beginning of China’s Twentieth Century: Revolution and Negotiation in the Era of 'Awakening of Asia.'"

=== Fall 2011- Spring 2012 ===

[[Laurie L. Patton|Laurie L. Patton's, ]] "Is Every Sanskritist a Nationalist?"

[[Frank Dikötter|Frank Dikötter's]], "Reassessing the Politics of Man-Made Catastrophe: China's Great Leap Forward."

[[Mahesh Dattani|Mahesh Dattani's]], "My Life in Theatre and Cinema."

[[Robert Petit]], Kunthear Thorng, and Kate Robertson spoke at the symposium, "From Impunity to Accountability? The Khmer Rouge Tribunal."

=== Fall 2010- Spring 2011 ===

[[Mark Selden|Mark Selden's]], "Electronic Publication and the Critical Intellectual in the Post-Print Era: An Asia-Pacific Perspective."

[[Saeed Naqvi|Saeed Naqvi's, ]] "How Have 170 Million Indian Muslims Remained Moderate?"

[[Arjun Appadurai|Arjun Appadurai's, ]]"The New and the Now: Globalization and the Politics of the Déjà Vu."

[[Dai Qing|Dai Qing's, ]] "China's "Rise" and the Environment's Decline."
[[File:JohnW GrahamLibrary,TrinityCollege2.jpg|left|250px|thumb|Munk School of Global Affairs.]]

==Graduate and Undergraduate Study==
Not all of the research centres that are a part of the Asian Institute offer taught programs, however, in association with the Dr. David Chu Program in Asia-Pacific Studies and the Centre for South Asian Studies, the Asian Institute runs several undergraduate and graduate programs.

== Undergraduate ==

*Contemporary Asian Studies major and minor.
*South Asian Studies minor.

== Graduate ==

*Collaborative Master's Program in Asia-Pacific Studies.
*Collaborative Master's and Doctoral Program in South Asian Studies.

==Current Director==
In 2015 [[Ritu Birla]] was appointed the Richard Charles Lee Director of the Asian Institute. She received a B.A. from Columbia (Summa Cum Laude); a second BA and MA from Cambridge (on a [[Kellett Fellowship]]); and an M.Phil and Ph.D. from Columbia University. She is recognized for bringing the empirical study of Indian economy to current questions of social and political theory and her research has sought to build new conversations in the global study of capitalism and its forms of governing.<ref>http://munkschool.utoronto.ca/ai/feature/welcome-ritu-birla-richard-charles-lee-director-of-the-asian-institute/</ref>

== References ==

{{reflist}}

*
*
*
*

[[Category:University of Toronto]]