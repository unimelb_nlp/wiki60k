{{other people||Richard Basset (disambiguation){{!}}Richard Basset}}

{{Infobox judge
|name        = Richard Basset
|image       = 
|imagesize   = 
|alt         = 
|caption     = 
|office      =  Royal Justice
|term_start=[[circa|c.]] 1120
|term_end=c. 1135
|birth_date   = 
|birth_place  = 
|death_date   = between 1135 and 1144
|death_place  = 
|restingplace = 
|birthname    = 
|spouse       = Matilda
|relations    = 
|children     = Geoffrey Ridel<br/>Ralph Basset<br/>William Basset<br/>Sibil<br>Matilda
|residence    = 
|occupation   = Royal justice
|profession   = 
}}

'''Richard Basset''' (died between 1135 and 1144) was a royal judge and sheriff during the reign of King [[Henry I of England]]. His father was also a royal justice. In about 1122 Basset married the eventual heiress of another other justice; the marriage settlement has survived. In 1129-30 Basset was co-sheriff of eleven counties. Basset and his wife founded a monastic house in 1125 from their lands, which before the donation were equivalent to 15 [[knight's fee]]s.

==Early life==

Basset was the son of [[Ralph Basset]],<ref name=DD>Keats-Rohan ''Domesday Descendants'' p. 166</ref> who was a [[royal justice]] under Henry I. While it is not known whether Richard was Ralph's eldest son, Richard inherited Ralph's estates in [[Normandy]], which were near [[Montreuil-au-Houlme]]. He also inherited his father's English estates at Colston Basset, Kingston Winslow, and Peatling Parva.<ref name=Green231/> The bulk of Ralph's English lands did not go to Richard, however.<ref name=Chartersx>Reedy "Introduction" ''Basset Charters'' pp. x-xi</ref> Basset's brother Nicholas signed over his own inheritance to Richard.<ref name=Green231/> Ralph Basset was considered one of the "new men" of Henry I.<ref name=Newman73>Newman ''Anglo-Norman Nobility'' p. 73</ref><ref name=Newman102>Newman ''Anglo-Norman Nobility'' p. 102</ref> William Basset, the [[abbot]] of the [[Benedictine Order|Benedictine]] monastery of the abbey of [[St Benet's Abbey|St Benet of Hulme]], may have been a relative, as he granted lands to Richard Basset in return for a £10 annual rent.<ref name=Newman149>Newman ''Anglo-Norman Nobility'' pp. 149–150</ref> Another relative may have been the Robert Basset who witnessed nine charters of [[Ranulf de Gernon, 4th Earl of Chester|Ranulf de Gernon]], [[Earl of Chester]].<ref name=Aristo213>Green ''Aristocracy of Norman England'' p. 213</ref>

==Royal service==
In 1125, the king appointed Basset to oversee the lands of [[Peterborough Abbey]] after the death of the abbot. The revenues of a vacant abbey went to the king, and Basset's job was to secure Peterborough's income for King Henry.<ref name=Aristo267>Green ''Aristocracy of Norman England'' p. 267</ref>

In 1129-30, Basset served as [[High Sheriff of Bedfordshire and Buckinghamshire|sheriff of Bedfordshire and Buckinghamshire]], [[High Sheriff of Cambridgeshire and Huntingdonshire|Cambridgeshire and Huntingdonshire]], [[High Sheriff of Essex|Essex]], [[High Sheriff of Hertfordshire|Hertfordshire]], [[High Sheriff of Leicestershire|Leicestershire]], [[High Sheriff of Northamptonshire|Northamptonshire]], [[High Sheriff of Norfolk and Suffolk|Norfolk and Suffolk]], and [[High Sheriff of Surrey|Surrey]] together with [[Aubrey de Vere II]].<ref name=Green231>Green ''Government of England'' pp. 231–232</ref> The number of shrievalties was unusual, and is known from the [[Pipe Roll]] of 1130. According to the entries in the Pipe Roll, de Vere and Basset did not function as traditional sheriffs, farming the revenues, but were instead responsible for the entire royal revenue in those counties.<ref name=Henryi360>Hollister ''Henry I'' p. 360</ref>

As well as his service as a sheriff, Basset also served as a royal justice, hearing pleas in Leicestershire in 1129 and 1130.<ref name=Aristo249>Green ''Aristocracy of Norman England'' p. 249</ref> Between 1131 and 1133, Basset appears to have been a frequent attendee at the royal court, as he witnessed a number of documents. He was present at the councils held at Northampton in 1131 and at Westminster in 1132. Basset witnessed no royal documents after 1133, when King Henry left England for Normandy for the final time.<ref name=DNB>Green "Basset, Richard" ''Oxford Dictionary of National Biography''</ref>

After King Henry's death in 1135, Basset was not employed as a royal official, either as a justice or as a sheriff. He appears once as a witness to a charter of [[Stephen, King of England|King Stephen]]'s in 1136, but the authenticity of this document has been questioned. He had built a castle in Normandy at Montreuil-au-Houlme, but Basset did not have possession of it in 1136, when it was held against Stephen's opponents by [[William de Montpincon]].<ref name=DNB/>

==Lands==

Basset's lands did not form a compact estate, as they were spread over 11 counties.<ref name=Newman126>Newman ''Anglo-Norman Nobility'' p. 126</ref> In 1135, Basset's lands totalled 184.25 [[carucate]]s of land, and were later considered 15 knight's fees.<ref name=Bartlett222>Bartlett ''England Under the Norman and Angevin Kings'' p. 222</ref> In Leicestershire, Basset held most of the lands held by Robert de Buci at the time of the [[Domesday Book|Domesday Survey]] of 1086. The lands were held by Basset of the king by right of his wife, but how the lands had passed into her family is unclear. In addition, Basset held land in Leicestershire from both King [[David I of Scotland]] and from [[Robert de Beaumont, 2nd Earl of Leicester|Robert de Beaumont]], the [[Earl of Leicester]].<ref name=Survey92>Slade ''Leicestershire Survey'' pp. 92–94</ref>

In 1125, Basset and his wife founded an [[Augustinian Order]] priory at [[Launde]] in Leicestershire,<ref name=Aristo403>Green ''Aristocracy of Norman England'' pp. 403–404</ref> This priory, [[Launde Abbey|Launde Priory]], was endowed with the village of [[Loddington]] in Leicestershire and a number of churches in that county and others.<ref name=VCH>Hoskins "[http://british-history.ac.uk/report.aspx?compid=38161 Houses of Augustinian canons: The priory of Launde]" ''History of the County of Leicestershire''</ref>

==Family and death==

Basset married Matilda, the daughter and eventual heiress of [[Geoffrey Ridel (royal justice)|Geoffrey Ridel]] (d. 1120), some time between 1120 and 1123.<ref name=DD/> Matilda had a brother Robert, who was mentioned in her [[marriage settlement]].  By the terms of the settlement, Robert Ridel was placed under the guardianship of Richard Basset until he was knighted and married to Basset's niece.<ref name=Green231/> The marriage settlement describes Matilda's [[dowry]] as being worth four [[knight's fee]]s.<ref name=Newman82/> Basset also received the right to arrange marriages for Matilda's sisters. Robert's lands were to come to Basset if Robert had no children.<ref name=Newman82>Newman ''Anglo-Norman Nobility'' p. 82</ref> Not long after the settlement was written, Basset was in possession of the lands that should have been Robert's.<ref name=Green231/>

Basset witnessed a royal charter in 1135 but was dead by 1144, when his lands were granted by the [[Empress Matilda]] and her son [[Henry II of England|Henry]] to Richard's son [[Geoffrey II Ridel|Geoffrey Ridel]]. His other sons were Ralph Basset, who held lands near Drayton, and [[William Basset]], who held lands near Sapcote.<ref name=DD/> William became a royal justice and sheriff like his father.<ref name=DNB/> Richard also had two daughters: Sibil, who married Robert de Cauz, and Matilda, who married John de Stuteville.<ref name=DD/> Ralph inherited the ancestral lands in Normandy.<ref name=Green231/> The Norman chronicler [[Orderic Vitalis]] wrote that Basset built a tower on his ancestral lands of Montreuil in Normandy purely to demonstrate his status and wealth.<ref name=Newman73/>

Ralph inherited the ancestral lands in Normandy.<ref name=Green231/> The Norman chronicler [[Orderic Vitalis]] wrote that Basset built a tower on his ancestral lands of Montreuil in Normandy purely to demonstrate his status and wealth.<ref name=Newman73/>

==Citations==
{{reflist|40em}}

==References==
{{refbegin|60em}}
* {{cite book |author=Bartlett, Robert C. |authorlink=Robert Bartlett (historian)|title=England Under the Norman and Angevin Kings: 1075–1225 |publisher=Clarendon Press |location=Oxford, UK |year=2000 |isbn=0-19-822741-8 }}
* {{cite book |author=Green, Judith A.|authorlink= Judith Green (historian) |title=The Aristocracy of Norman England |year=1997 |publisher=Cambridge University Press |location=Cambridge, UK |isbn=0-521-52465-2 }}
* {{cite encyclopedia |author= Green, Judith A. |authorlink= Judith Green (historian) |title=Basset, Richard (d. in or before 1144) |encyclopedia= Oxford Dictionary of National Biography |publisher= Oxford University Press |year= 2004 |url=http://www.oxforddnb.com/view/article/1647 |accessdate= 15 April 2011 |doi= 10.1093/ref:odnb/1647 |format={{ODNBsub}}}}
* {{cite book |author=Green, Judith A.|authorlink= Judith Green (historian) |title= The Government of England Under Henry I |publisher=Cambridge University Press |location=Cambridge, UK |year=1986  |isbn=0-521-37586-X }}
* {{cite book |author1=Hollister, C. Warren |authorlink1=C. Warren Hollister |editor=Frost, Amanda Clark |title=Henry I |publisher=Yale University Press |location=New Haven, CT |year=2001  |isbn=0-300-08858-2 }}
* {{cite encyclopedia |author=Hoskins, W. G. (ed.) |editor=W. G. Hoskins and R. A. McKinley | title=Houses of Augustinian Canons: The Priory of Launde|encyclopedia=A History of the County of Leicestershire: Volume 2 |year=1954 |pages= 10–13 |url= http://british-history.ac.uk/report.aspx?compid=38161|publisher=Victoria County History |accessdate=5 January 2010}}
* {{cite book |author=Keats-Rohan, K. S. B. |authorlink= Katharine Keats-Rohan |title=Domesday Descendants: A Prosopography of Persons Occurring in English Documents, 1066–1166: Pipe Rolls to Cartae Baronum |publisher=Boydell Press |location=Ipswich, UK |year=1999 |isbn=0-85115-863-3 }}
* {{cite book |author=Newman, Charlotte A. |title=The Anglo-Norman Nobility in the Reign of Henry I: The Second Generation |publisher=University of Pennsylvania Press |location=Philadelphia, PA |year=1988 |isbn=0-8122-8138-1 }}
* {{cite book |author=Reedy, William T. |chapter=Introduction |title=Basset Charters c. 1120 to 1250 |editor=Reedy, William T. | pages=v-xxxix |publisher=Pipe Roll Society |isbn=0-901134-12-0 | location=London |year=1995}}
* {{cite book |author=Slade, C. F. |title = The Leicestershire Survey c. A. D. 1130 |publisher=University College of Leicester |year=1956 |location=Welwyn Garden City, UK |oclc= 4830086}}
{{refend}}
<!-- See ''Complete Peerage'', ii, 1.for Matilda, his wife -->

{{Good article}}
{{Authority control}}
{{Use British English|date=June 2013}}

{{DEFAULTSORT:Basset, Richard}}
[[Category:Anglo-Normans]]
[[Category:High Sheriffs of Norfolk]]
[[Category:High Sheriffs of Suffolk]]
[[Category:High Sheriffs of Bedfordshire]]
[[Category:High Sheriffs of Buckinghamshire]]
[[Category:High Sheriffs of Cambridgeshire and Huntingdonshire]]
[[Category:High Sheriffs of Essex]]
[[Category:High Sheriffs of Hertfordshire]]
[[Category:High Sheriffs of Leicestershire]]
[[Category:High Sheriffs of Northamptonshire]]
[[Category:High Sheriffs of Surrey]]
[[Category:12th-century deaths]]
[[Category:Year of birth unknown]]