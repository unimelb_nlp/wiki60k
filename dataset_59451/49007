{{Advert|date=February 2014}}
{{Infobox officeholder
|name           = Keith Loris
|image          = Keith Loris.png
|office         = CEO of Sales Renewal
|term_start     = 
|term_end       = 
|predecessor    = 
|succeeding     = 
|successor      = 
|monarch1       = 
|term_start1    = 
|term_end1      = 
|predecessor1   = 
|successor1     = 
|office2        = 
|term_start2    = 
|term_end2      = 
|predecessor2   = 
|successor2     = 
|term_start3    = 
|term_end3      = 
|predecessor3   = 
|successor3     = 
|office4        = 
|primeminister4 = 
|term_start4    = 
|term_end4      = 
|predecessor4   = 
|successor4     = 
|birth_date     = 
|birth_place    = [[Brooklyn]], [[New York (state)|New York]]
|death_date     = 
|death_place    = 
|party          = 
|spouse         = Donna Balmuth; 2 Children
|alma_mater     = [[Vassar College]], [[New York University]]<ref name=Patch>{{cite web|last=Ball|first=Patrick|title=About Keith Loris|url=http://concord.patch.com/users/keith-loris|work=ConcordPatch|accessdate=18 December 2012}}</ref>
|religion       =  
}}

'''Keith Loris''' is a business entrepreneur with a 28-year executive career.<ref name="Patch"/><ref name=IPO>{{cite news|last=Davids|first=Meryl|title=The IPO CEO's Reality|url=http://chiefexecutive.net/ipo-ceos-reality-check|accessdate=18 December 2012|newspaper=Chiefexecutive.net|date=January 2000}}</ref> Loris is currently president and CEO of his fourth startup, Sales Renewal. Loris was the CEO of Softlock Inc, the first company to digitally market and sell a mass-market novel ([[Riding the Bullet]] by [[Stephen King]]) when he was <ref name="IPO" /><ref name=PDF>{{cite web|last=Abrew|first=Karl|title=PDF eBooks are Here to Stay|url=http://www.planetpdf.com/enterprise/article.asp?ContentID=6358|work=Debenu|accessdate=19 December 2012}}</ref><ref name=PBS>{{cite news|last=Lehrer|first=Jim|title=The Business of E-Books|url=http://www.pbs.org/newshour/bb/media/jan-june00/e-books_sidebar.html|accessdate=19 December 2012|newspaper=NewsHour|agency=PBS}}</ref><ref name="WSJ">[https://online.wsj.com/article/SB962748747863241304.html], Wall St Journal, SoftLock.com Blazed a Trail for E-Books, Jul 5, 2000</ref> {{dubious|date=February 2014}}

== Early education and career ==

Keith Loris obtained a Bachelor of Science degree in [[Biology]] from [[Vassar College]] in 1979 and a Master degree in [[computer science]] from [[New York University]] in 1986.<ref name="Patch"/><ref name=Freelib>{{cite web|title=Softlock Names Keith Loris Chief Executive Officer and Chairman of the Board; Experienced Software Executive Joins Information E-Commerce Pioneer|url=http://www.thefreelibrary.com/Softlock+Names+Keith+Loris+Chief+Executive+Officer+and+Chairman+of...-a053054838|work=Gale Group|publisher=Business Wire|accessdate=19 December 2012}}</ref> While at NYU, Loris worked as a research scientist at the Computation Neuroscience Labs designing models that accurately express the visual processing of primates.<ref name=NYU>{{cite web|title=Computer Vision and Computational Neuroscience Laboratory|url=http://eslab.bu.edu/people/people.php|work=Department of Cognitive and Neural Systems|accessdate=18 December 2012}}</ref>
After graduating NYU, Keith Loris was hired by [[NYNEX]] Image Recognition Systems Corporation and within a few years became vice president of technology, acquiring 2 patents in [[neural networks]] and [[lexical analysis]] (Patent 4,876,731 and 5,239,593).<ref name="Freelib"/><ref name=NYTimes>{{cite web|last=Andrews|first=Edmund|title=Patents; Computer Teaches Itself To Read Handwriting|url=https://www.nytimes.com/1990/02/03/business/patents-computer-teaches-itself-to-read-handwriting.html|work=NY Times|accessdate=May 7, 2013|archivedate=February 3, 1990}}</ref> By 1991, Loris was hired by [[Xerox|Xerox Corporations]] as vice president of technology. While at Xerox, Loris received an additional patent (5,642,435) as he led a team of 150 people developing new imaging technologies in neural networks.<ref name="Freelib" /> Loris took this experience to ServiceSoft Corporation where, as the Vice President of Marketing & Business Development,<ref name="Freelib" /> he successfully helped turn around a struggling, 14 person software developer, which led to its purchase by [[Kana]].<ref name=dbnews>{{cite news|title=Broadbase Acquires Servicesoft|url=http://www.dmnews.com/broadbase-acquires-servicesoft/article/70206/|accessdate=2 February 2013|newspaper=Direct Marketing News|date=22 December 2000}}</ref>

== Softlock ==

In 1998, Keith Loris joined as Chief Executive Officer of a small start-up that was based out of [[Rochester, NY|Rochester, New York]].<ref name=Metro>{{cite news|last=Stape|first=Andrea|title=Locking INTO Growth|newspaper=Metrowest Daily News|date=January 27, 2000}}</ref>  The company had patented software that secured copyright protection allowing companies to securely sell their content electronically.<ref name="IPO" /><ref name="WSJ" /><ref name="Freelib" />  Prior to Softlock’s business model, competitors had tried two different strategies: distribute less valuable content in hopes of attracting enough customers to create ad revenue or use password protected websites that have a subscription fee. Softlock was unique because it had copyright protection, payment processing and product distribution all within the software.<ref name="Freelib" /> Furthermore, Softlock charged no Fees for its software or marketing services and relied strictly on commission.<ref name="WSJ" /><ref name="Metro" />
As CEO, Keith Loris partnered with [[Simon & Schuster]] to release [[Stephen King|Stephen King's]] 66 page horror story as an e-book [[Riding the Bullet]].<ref name="PDF" /><ref name="PBS" /> This was the first time a mass marketed novel was released strictly in electronic form.<ref name="PDF" /><ref name="PBS" /><ref name="WSJ" /> The short story sold for $2.50 per copy and within 24 hours there were 200,000 orders processed.<ref name="WSJ" />
Softlock’s model allowed its partners to link their website to Softlock’s to purchase the book without the buyer having to leave the partner's site.<ref name="PBS" /><ref name="WSJ" /> Independent companies earn 20 percent commission while Softlock earns 30 to 35 percent commission for every e-book sold.<ref name="PDF" /><ref name="Metro" /> Softlock’s software allowed users to send a copy of the "SoftLocked" content to others, however, only the first few pages of a book, or minutes of a song or movie, were unlocked. By having independent websites link to Softlock’s website and allowing users send copies to others, Softlock leveraged viral marketing to sell their products.<ref name="WSJ" /><ref name="Metro" /> Loris raised $15 million and turned a company with a share price of $0.89 into one with an average of $6.65 (at one point it sold for $21.875).<ref name="BBJ">[http://www.bizjournals.com/boston/stories/2000/02/28/newscolumn3.html?page=all] Boston Business Journal, Public firm SoftLock.com nears $15 million in funds, Feb 28, 2000</ref> In May 2000, Keith Loris left Softlock.<ref name="WSJ"/>

== References ==

{{reflist}}

{{sisterlinks|d=no|b=no|s=no|n=no|v=no|voy=no|species=no|wikt=no}}
{{DEFAULTSORT:Loris, Keith}}
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:Vassar College alumni]]