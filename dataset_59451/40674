'''Stephanie Martin''', sometimes credited as '''Stéphanie Martin''', is an American and Canadian singer-songwriter and actress having performed in notable musical productions in both French and English. She is best known for her role as Éponine in 3 productions of the musical ''Les Misérables'' and as the [[Québec French|Québécoise]] singing voice of Pocahontas in Disney's 1995 animation ''Pocahontas''.

== Life and career ==
Stephanie Martin was born in the US where she spent the first 6 years of her life. She is a dual Canadian-American citizen.<ref>{{cite news|last1=Donnelly|first1=Pat|title=Les Miz alumni take the world by storm; Eponine role in London latest part won by Montreal performer: [FINAL Edition]|url=http://search.proquest.com/docview/432311158/|accessdate=25 March 2016|agency=The Gazette, Montreal E7|publisher=Infomart, a division of Postmedia Network Inc.|date=4 July 1992|ISSN=0384-1294|archivedate=6 December 2014|location=Canadian Newsstand Major Dailies Data Base}}</ref> The family then moved to [[Beaconsfield, Quebec]] where, as a child, Stephanie sang with both parents in [[Montreal|Montreal’s]] Donovan Chorale. She participated in the Diocesan Folk Music Camp for youths held at Camp Kinkora located in [[Saint-Adolphe-d'Howard, Quebec|Saint-Adolphe-d’Howard, Quebec]].<ref>{{cite web|last1=Mac Master|first1=John|title=Stephanie Martin|url=https://www.facebook.com/mactenor/posts/10152312833827115|website=Facebook Post|accessdate=18 March 2016}}</ref> She began performing publicly in her mid-teens in Montreal.<ref>{{cite news|last1=News Desk|title=Stéphanie Martin|url=http://montrealgazette.com/life/urban-expressions/stephanie-martin/2|accessdate=18 March 2016|agency=Postmedia Network Inc|publisher=Montreal Gazette|date=1 November 2011}}</ref>

Stephanie Martin played the role of [[Éponine]] in ''[[Les Misérables (musical)|Les Misérables]]'' for 3 consecutive years starting with the bilingual Montreal production<ref name=":1" /> in 1991 that led to the Paris production of ''Les Misérables'' in 1991-1992<ref>{{cite news|last1=Riding|first1=Alan|title=Parisians Flocking To See 'Les Mis'|url=https://www.nytimes.com/1991/10/30/theater/parisians-flocking-to-see-les-mis.html|accessdate=17 March 2016|publisher=New York Times|date=30 October 1991}}</ref> followed by the London production in 1992-1993.<ref name=":2">{{cite web|last1=southerncalcosette|first1=Alex|title=30 YEARS OF LES MISERABLES LONDON CASTS!|url=http://southerncalcosette.tumblr.com/post/117704271289/30-years-of-les-miserables-london-casts|website=Tumblr|accessdate=17 March 2016}}</ref><ref>{{cite web|url=http://www.regardencoulisse.com/stephanie-martin-son-histoire/|title=Stéphanie Martin – Son histoire|website=Regard en coulisses|language=french|last1=Lapointe|first1=André|accessdate=17 March 2016}}</ref> This aforementioned production of ''Les Misérables'' in Paris won the 1992 [[Molière Award]] for Best Musical.

Stephanie Martin has performed with symphony orchestras across North America, Europe and Asia. She is a core member<ref>{{cite web|last1=Brennan|first1=Peter|title=Stephanie Martin - vocalist|url=http://www.jeansnclassics.com/artists/stephanie-martin.asp|website=Jeans 'n Classics|accessdate=17 March 2016}}</ref> of Jeans 'n Classics developed by Peter Brennan. Stephanie has toured North America with Jeans 'n Classics singing with symphony orchestras in programs of [[classic rock]].<ref>{{cite web|last1=Brennan|first1=Peter|title=About Jeans 'n Classics|url=http://www.jeansnclassics.com/|website=Jeans 'n Classics|accessdate=23 March 2016|location=London, Ontario}}</ref> Stephanie has participated in a United Nations Show Tour for Canadian peacekeeping troops in [[Lahr]], [[Zagreb]] and [[Sarajevo]]. There, Stephanie delivered a peace package from Quebec elementary school children.<ref>{{cite news|last1=Donnelly|first1=Pat|title=Miserables veterans bring joy to troops; Montrealers off to do shows in Bosnia, Germany and Croatia: [FINAL Edition]|url=http://search.proquest.com/docview/432443932|accessdate=25 March 2016|agency=The Gazette, Montreal D5|publisher=Infomart, a division of Postmedia Network Inc.|date=26 June 1993|archivedate=8 December 2014}}</ref><ref>{{cite web|url=https://www.google.ca/search?q=tycoon+%2B+%22stephanie+martin%22&espv=2&biw=1440&bih=704&source=lnt&tbs=cdr%3A1%2Ccd_min%3A1992%2Ccd_max%3A1995&tbm=#tbs=cdr:1%2Ccd_min:1992%2Ccd_max:1995&q=plamondon+%2B+%22stephanie+martin%22|title=Stéphanie Martin: Un tour de chant en plein coeur de l'ex-Yougoslavie et de la guerre|date=17 July 1993|website=La Presse Montreal|pages=6|language=french|format=PDF|last1=Sarfati|first1=Sonia|accessdate=25 March 2016}}</ref><ref>{{cite web|last1=Martin|first1=Stephanie|title=Fountainhead Actor Profile Download|url=http://fountainheadtalent.com/music/stephanie-martin/|website=Fountainhead Talent Inc|accessdate=23 March 2016|format=PDF}}</ref> In 1995 and 1996, she toured 11 cities in Japan with the ''Francis Lai Music Orchestra International Tour'' under the direction of Raphael Sanchez.<ref>{{cite web|url=http://uchebana5.ru/cont/2900984.html|title=tintin et le temple du soleil, bio for Raphael Sanchez|website=Uchebana5|language=french|last1=Sanchez|first1=Raphael|accessdate=23 March 2016}}</ref><ref>{{cite web|url=http://fr.viadeo.com/fr/profile/raphael.sanchez11|title=RAPHAEL SANCHEZ COMPOSITEUR ET CHEF D'ORCHESTRE, MAÎTRE DE CONFÉRENCES|website=VIADEO|last1=Sanchez|first1=Raphael|accessdate=8 May 2016}}</ref>

Stephanie has numerous acting and singing credits in both Television and Film. She was heard announcing as the French "Voice of God" during the opening and closing ceremonies of the [[2015 Pan American Games|2015 Pan American]] and [[2015 Parapan American Games|Para Pan American Games]] held in Toronto.<ref>{{cite web|title=Offertory Performers|url=http://www.mcctoronto.com/offertory-performers|website=Metropolitan Community Church of Toronto|accessdate=18 March 2016}}</ref>

Stephanie Martin has released 2 original music albums and 1 single. The albums ''shape line & harmony'' in 2007 and ''April Snow'' in 2016 were both co-written and produced by [[Juno Award]] winner Chad Irschick.<ref>{{cite web|last1=JUNO|title=Awards Chad Irschick|url=https://junoawards.ca/awards/?from-year=1970&to-year=2016&nomination-category=&wins-only=no&artist=chad+irschick|website=JUNO Canada's Music Awards|accessdate=18 March 2016}}</ref> The single ''SAILING ON'' was co-written with Diane Leah and produced by Dave Pickell.

Stephanie Martin has resided primarily in [[Toronto]], Ontario with husband [[Andrew Sabiston]] since they met in 1994<ref>{{cite web|last1=Ouzounian|first1=Richard|title=Singer now calling her own tune|url=http://www.thestar.com/entertainment/music/2008/11/07/singer_now_calling_her_own_tune.html|website=The Toronto Star|accessdate=22 March 2016|date=7 November 2008}}</ref> during the Toronto musical production of [[Napoleon (musical)|Napoleon]] at the [[Elgin and Winter Garden Theatres|Elgin Theatre]].

== Community involvement ==

Stephanie Martin has contributed to fundraising events for organizations bringing awareness to missing children. She performed at the 2008 Bring Christina Home Fundraising Gala in Richmond Hill, Ontario<ref>{{cite web|title=Bring Christina Home Fundraising Gala|url=http://www.findchristinacalayca.com/gala.htm|website=Find Christina Calayca|accessdate=23 March 2016|date=30 October 2008}}</ref> and can be heard on the lead track of the Missing Children's Network benefit CD entitled ''Help us Find the Children''.<ref>{{cite news|last1=Schnurmacher|first1=Thomas|title=Polish presence at chamber show ; Borowicz featured in concert at Erskine and American: [FINAL Edition]|url=http://search.proquest.com/docview/432545055|accessdate=25 March 2016|agency=The Gazette, Montreal|publisher=Infomart, a division of Postmedia Network Inc.|date=8 December 1993|archivedate=7 December 2014}}</ref>

Stephanie Martin was a committee member of Toronto's BIKESTOCK 2014, an organization promoting cycling safety in Toronto founded by [[Albert Koehl]].<ref>{{cite web|last1=Campbell|first1=Jenna|title=BIKESTOCK Toronto: Sept. 14 in photos|url=http://dandyhorsemagazine.com/blog/2014/09/15/bikestock-toronto-sept-14-in-photos/|website=Dandyhorse Magazine|accessdate=18 March 2016}}</ref>

Stephanie Martin has been a guest singer at the [[Metropolitan Community Church of Toronto]] (MCC Toronto). Stephanie has recorded a version of her song ''Walk in the Light'' with noted musical director Diane Leah<ref>{{cite news|last1=Coulbourn|first1=John|title=2011 Dora Award noms unveiled - BEST MUSICAL DIRECTION|url=http://www.torontosun.com/2011/06/07/2011-dora-award-noms-unveiled|accessdate=25 March 2016|agency=Toronto Sun|publisher=Postmedia|date=7 June 2011}}</ref> and the choir of MCC Toronto for their music album ''These Old Walls''.<ref name="Broadwayworld">{{cite news|last1=Kucherawy|first1=Dennis|title=BWW Review: Stephanie Martin in Concert|url=http://www.broadwayworld.com/toronto/article/BWW-Review-Stephanie-Martin-in-Concert-20141221#|accessdate=17 March 2016|publisher=Broadwayworld|date=21 December 2014}}</ref> ''Walk in the Light'' was the working title of her musical album, funded by [[Kickstarter]] backers, released March 30, 2016 as ''April Snow''.<ref name="Stephanie Martin - Walk In The Light - album presale">{{cite web|last1=Martin|first1=Stephanie|title=Walk in the Light|url=https://www.kickstarter.com/projects/288808127/stephanie-martin-walk-in-the-light-album-presale|website=Kickstarter|accessdate=17 March 2016}}</ref>

== Discography  ==

=== Original music studio albums ===
{| class="wikitable"
|-
!Year !! Title !! Artist !! Producer !! Label
|-
| 2007 || ''shape, line & harmony'' || Stephanie Martin || Chad Irschick || Sovereign Productions
|-
| 2016 || ''April Snow'' || Stephanie Martin || Chad Irschick || Sovereign Productions
|}

=== Original music studio single ===
{| class="wikitable"
|-
!Year !! Title !! Composers/Artists/Writers !! Producer !! Label
|-
| 2010 || ''SAILING ON'' || Diane Leah, Stephanie Martin, Dave Pickell || Dave Pickell || Sovereign Productions
|}

=== Musicals; cast recording credits ===
{| class="wikitable sortable"
|-
! Year !! Title !! Producer !! Role
|-
| 1991 || ''Les Misérables''<ref>{{cite web|url=http://www.fglmusic.com/produit.php?id=747|title=LES MISÉRABLES - L'Intégrale CD|website=FGL Music|language=french|last1=FLG|first1=Productions|accessdate=24 March 2016}}</ref> || Trema, original Paris cast recording || Éponine
|-
| 1995 || ''[[Robert Marien]]'': Broadway-Montreal'' || Analekta, cast recording || Lead tracks
|-
| 1996 || ''[[Napoleon (musical)|Napoleon]]'' || EMI Broadway Angel, Toronto cast recording || Clarice
|-
| 1997 || ''La vie en bleu: comédie musicale''<ref>{{cite web|last1=Cast|first1=Recording|title=La Vie en Bleu: Comedie Musicale CD Trema #710744|url=http://www.allmusic.com/album/release/vie-en-bleu-comedie-musicale-mr0000710532|website=ALLMUSIC|accessdate=24 March 2016}}</ref> || Trema, original Paris cast || Germaine 
|-
| 2011 || ''Schwartz's: The Musical''<ref>{{cite web|last1=Schwartz's|first1=Deli|title=Schwartz's the Musical CD|url=http://schwartzsdeli.com/ca/en/music-cd-dvd-film/cd-musical-music-musique-songs|website=Schwartz's Deli Website|accessdate=24 March 2016|location=Montreal, Quebec}}</ref> || [[Centaur Theatre|Centaur Theatre Company]], Montreal live cast recording || Amber
|}

=== Animation feature ===

{| class="wikitable"
|-
! Year !! Title !! Producer !! Role
|-
| 1995 || ''Pocahontas, une légende indienne'' || Walt Disney Records, French Québécois recording || Pocahontas singing voice
|}

== Theatre productions ==

{| class="wikitable sortable"
|-
! Year !! Title !! Theatre !! Director !! Role
|-
| 1991 || ''Les Misérables''<ref name=":1">{{cite web|last1=Donnelly|first1=Pat|title=From the Archive: Les Miz scores in two languages|url=http://montrealgazette.com/entertainment/arts/from-the-archive-les-miz-scores-in-two-languages|website=Montreal Gazette|accessdate=25 March 2016|archivedate=25 January 1991|date=25 January 1991}}</ref> || [[Théâtre Saint-Denis]], Montreal, Winnipeg tour || Richard J. Alexander || Éponine
|-
| 1991 - 
1992 
|| ''Les Misérables'' || [[Théâtre Mogador]], Paris || John Caird || Éponine
|-
| 1992 - 
1993 
|| Les Misérables<ref name=":2" /> || [[Palace Theatre, London]] || [[John Caird (director)|John Caird]]/[[Trevor Nunn]] || Éponine
|-
| 1993 || ''Tycoon'' (''[[Starmania (musical)|Starmania]]'' in French) || Sydmonton Festival, London || [[Tim Rice]], [[Luc Plamondon]] || Crystal<ref>Napoleon Playbill, Curtain Call Spring 1994, The Elgin Theatre, Toronto, Artist Biography p.28</ref>
|-
| 1994 || ''[[Napoleon (musical)|Napoleon]]'' || [[Elgin and Winter Garden Theatres|The Elgin Theatre]], Toronto || John Wood || Clarice
|-
| 1994 || ''[[Robert Marien]]'': Broadway-Montreal'', musical revue || chapiteau [[Saint-Sauveur, Quebec]] and tour || Robert Marien || various
|-
| 1994 || ''Love Notes''<ref>{{cite news|last1=Donnelly|first1=Pat|title=Lu's Love Notes: light, lively, sugary sweet: [FINAL Edition]|url=http://search.proquest.com/docview/432426753/|accessdate=25 March 2016|agency=The Gazette, Montreal D9|publisher=Infomart, a division of Postmedia Network Inc.|date=16 April 1993}}</ref> || [[Centaur Theatre|Centaur Theatre Company]], Montreal || Lu Hanessian<ref>{{cite news|last1=Brownstein|first1=Bill|title=Making the move from snow business to show business; Lu Hanessian brings `the right mix of chutzpah and fear' to her new career as a singer: [FINAL Edition]|url=http://search.proquest.com/docview/432428498/|accessdate=25 March 2016|agency=The Gazette, Montreal D5|publisher=Infomart, a division of Postmedia Network Inc.|date=10 April 1993|archivedate=8 December 2014}}</ref> || various
|-
| 1997 || ''La vie en bleu: comédie musicale''<ref>{{cite web|last1=Ly-Cuong|first1=Stephane|title=Former "Eponines" Star in Paris Musicals|url=http://www.playbill.com/article/former-eponines-star-in-paris-musicals-com-72573|website=PLAYBILL|accessdate=24 March 2016|location=France Correspondent|date=22 December 1997}}</ref> || [[Théâtre Mogador]], Paris and Monaco || Robert Hossein || Germaine
|-
| 2009 || ''[[Napoleon (musical)|Napoleon]]'' || Talk is Free Theatre, Barrie || Richard Ouzounian || Therese
|-
| 2011 || ''Schwartz's: The Musical''<ref>{{cite web|last1=Bowser|first1=George|last2=Blue|first2=Rick|title=Schwartz's the Musical|url=http://www.bowserandblue.com/centaur.html|website=Bowser and Blue Website|accessdate=24 March 2016|date=2011}}</ref><ref>{{cite web|last1=Canadian|first1=Encyclopedia|title=Centaur Theatre Company|url=http://www.thecanadianencyclopedia.ca/en/article/centaur-theatre-company/|website=The Canadian Encyclopedia|accessdate=25 March 2016}}</ref> || Centaur Theatre|[[Centaur Theatre|Centaur Theatre Company]], Montreal || Roy Surette || Amber
|}

== Animation features credits - singer ==

{| class="wikitable"
|-
! Year !! Title !! Distribution !! Role
|-
| 1995 || ''Le caillou et le pingouin'';<ref>{{cite web|title=The Pebble and the Penguin|url=http://www.lesgrandsclassiques.fr/youbipingouin.php|website=Les grands classiques|accessdate=24 March 2016}}</ref> Quebec version of ''The Pebble and the Penguin'' ||  Metro-Goldwyn-Mayer (direct to video) ||  Marina singing voice, Quebec dub
|-
| 1995 || ''Pocahontas, une légende indienne'' || Disney animation ||  Pocahontas singing, Quebec dub
|-
| 1998 || ''Pocahontas 2 : À la découverte d'un monde nouveau'' ||  Disney animation (direct to video)  || Pocahontas singing, Quebec dub
|}

== Television ==

=== Series and television film credits - actress ===
{| class="wikitable"

|-
! Year !! Title !! Role
|-
| 2003 || ''[[Interlude (visual novel)|Interlude]]'' || Izumi Marufuji (English version, voice)
|-
| 2006 || ''[[Time Warp Trio]]'' || Sophie 
|-
| 2007 || ''[[Booky & the Secret Santa]]'' || (television film) Caroler #1
|-
| 2010 || ''[[The Dating Guy]]'' || Shoji (voice)
|-
| 2006 - 
2012 
|| ''[[Postcards from Buster]]'' || Mora (voice) - 15 episodes
|}

=== Series credits - singer/back-up vocalist ===

{| class="wikitable"
|-
! Year !! Title !! Particulars
|-
| 2003 - 
2005 
|| ''[[JoJo's Circus]]'' || back-up vocalist - 8 episodes
|-
| 2006 - 
2012 
|| ''[[Postcards from Buster]]'' || featured songs singer - 15 episodes
|}

== See also ==
* [[List of Canadian musicians]]
{{Portal bar|Musical theatre|Music|Canada|United States}}

==References==
{{reflist|35em}}

== External links ==
* [http://www.stephaniemartin.ca/ stephaniemartin.ca] Official website
* {{IMDb name|3954847}}

{{Authority control}}

{{DEFAULTSORT:Martin, Stephanie}}
[[Category:20th-century American actresses]]
[[Category:21st-century Canadian actresses]]
[[Category:20th-century American singers]]
[[Category:20th-century Canadian singers]]
[[Category:American emigrants to Canada]]
[[Category:American female singer-songwriters]]
[[Category:American female singers]]
[[Category:American musical theatre actresses]]
[[Category:American voice actresses]]
[[Category:Actresses from Montreal]]
[[Category:Actresses from Toronto]]
[[Category:Canadian female singer-songwriters]]
[[Category:Canadian female singers]]
[[Category:Canadian musical theatre actresses]]
[[Category:Canadian voice actresses]]
[[Category:French-language singers of Canada]]
[[Category:French-language singers of the United States]]
[[Category:Living people]]
[[Category:Musicians from Toronto]]
[[Category:Year of birth missing (living people)]]
[[Category:People from Beaconsfield, Quebec]]
[[Category:Singers from Montreal]]