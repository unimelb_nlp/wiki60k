[[Image:Heiankyo palace location.png|thumb|300px|Schematic map of Heian-kyō showing the location of the palace as well as the Tsuchimikado temporary palace that developed into the current [[Kyoto Gosho|Kyoto Imperial Palace.]]]]

The {{Nihongo|'''Heian Palace'''|平安宮|Heian-kyū}} or {{Nihongo|'''Daidairi'''|大内裏}} was the original imperial palace of [[Heian-kyō]] (present-day [[Kyoto]]), the capital of [[Japan]], from 794 to 1227. The palace, which served as the imperial residence and the administrative centre of for most of the [[Heian period]] (from 794 to 1185), was located at the north-central location of the city in accordance with the [[China|Chinese]] models used for the design of the capital.

The palace consisted of a large rectangular [[#Greater Palace (Daidairi)|walled enclosure]], which contained several ceremonial and administrative buildings including the government ministries. Inside this enclosure was the separately walled  [[#Inner Palace (Dairi)|residential compound]] of the [[Emperor of Japan|emperor]] or the ''Inner Palace'' (''Dairi''). In addition to the emperor's living quarters, the Inner Palace contained the residences of the imperial consorts, as well as certain official and ceremonial buildings more closely linked to the person of the emperor.

The original role of the palace was to manifest the centralised government model adopted by Japan from China in the 7th century—the ''[[Daijō-kan]]'' and its subsidiary Eight Ministries.  The palace was designed to provide an appropriate setting for the emperor's residence, the conduct of great affairs of state, and the accompanying ceremonies. While the residential function of the palace continued until the 12th century, the facilities built for grand state ceremonies began to fall into disuse by the 9th century. This was due to both the abandonment of several statutory ceremonies and procedures and the transfer of several remaining ceremonies into the smaller-scale setting of the Inner Palace.

From the mid-Heian period, the palace suffered several fires and other disasters. During reconstructions, emperors and some of the office functions resided outside the palace. This, along with the general loss of political power of the court, acted to further diminish the importance of the palace as the administrative centre. Finally in 1227 the palace burned down and was never rebuilt. The site was built over so that almost no trace of it remains. Knowledge of the palace is thus based on contemporary literary sources, surviving diagrams and paintings, and limited excavations conducted mainly since the late 1970s.

== Location ==
The palace was located at the northern centre of the rectangular Heian-kyō, following the Chinese model (specifically that of the [[Tang dynasty]] capital of [[Chang'an]]) adopted already for the [[Heijō Palace]] in the earlier capital [[Heijō-kyō]] (in present-day [[Nara, Nara|Nara]]), and [[Nagaoka-kyō]].  The south-eastern corner of the Greater Palace was located in the middle of the present-day [[Nijo Castle|Nijō Castle]]. The main entrance to the palace was the gate [[Suzakumon]] ({{Coord|35|0|49|N|135|44|32|E|type:landmark_region:JP|display=inline,title}}), which formed the northern terminus of the great [[Suzaku Avenue]] that ran through the centre of the city from the gate [[Rashōmon]]. The palace thus faced south and presided over the symmetrical urban plan of Heian-kyō. In addition to the Suzakumon, the palace had 13 other gates located symmetrically along the side walls. A {{Nihongo|major avenue|大路|ōji}} led to each of the gates, except for the three along the northern side of the palace, which was coterminous with the northern boundary of the city itself.

== History ==
The palace was the first and most important structure to be erected at the new capital of Heian-kyō, where the court moved in 794 following [[Emperor Kanmu]]'s order. The palace was not completely ready by the time of the move, however—the Daigokuden was completed only in 795, and the government office in charge of its construction was disbanded only in 805.<ref>Hall (1974) p. 7</ref>

The grand Chinese-style compounds of Chōdō-in and Buraku-in started to fall into disuse quite early on, in parallel with the decline of the elaborate Chinese-inspired ''[[ritsuryō]]'' government processes and bureaucracy, which were gradually either abandoned or reduced to empty forms. The centre of gravity of the palace complex moved to the Inner Palace or Dairi, and the Shishinden and later even the Seiryōden overtook the Daigokuden as loci for the conduct of official government business. 

In parallel with the concentration of activity within the Dairi, the Greater Palace began to be regarded as increasingly unsafe, especially by night. One reason may be the prevalent superstition of the period: uninhabited buildings were avoided for fear of spirits and ghosts, and even the great Buraku-in compound was thought to be haunted. In addition, the level of actual security maintained at the palace went into decline, and by the early 11th century only one palace gate, the Yōmeimon in the east, appears to have been guarded. Hence burglary and even violent crime became a problem within the palace by the first half of 11th century.<ref>McCullough & McCullough (1980) pp. 849–850</ref>

Fires were a constant problem as the palace compound was constructed almost entirely of wood. The Daigokuden was reconstructed after fires in 876, 1068 and in 1156 despite its limited use. However, after the major fire of 1177 which destroyed much of the Greater Palace, the Daigokuden was never again rebuilt. The Burakuin was destroyed by a fire in 1063 and was never rebuilt.<ref name="McCullough pp. 174–175"/>

As of 960, the Dairi was also repeatedly destroyed by fires, but it was systematically rebuilt and used as the official imperial residence until the late 12th century.<ref name="McCullough pp. 174–175"/> During periods of rebuilding the Dairi following fires, the emperors frequently had to stay at their secondary {{Nihongo|''sato-dairi''|里内裏|}} palaces within the city. Often these secondary palaces were provided by the powerful [[Fujiwara clan|Fujiwara]] family, which especially in the latter part of the Heian period exercised ''de facto'' control of politics by providing consorts to successive emperors. Thus the residences of the emperors' maternal grandparents started to usurp the residential role of the palace even before the end of the Heian period. The institution of rule by retired emperors or the {{Nihongo|[[Cloistered rule|insei system]]|院政|insei}} from 1086 further added to the declining importance of the palace as retired emperors exercised power from their own residential palaces inside and outside the city.

After a fire in 1177, the original palace complex was abandoned and emperors resided in smaller palaces (the former ''sato-dairi'') within the city and villas outside it. In 1227 a fire finally destroyed what remained of the Dairi, and the old Greater Palace went into complete disuse. In 1334 [[Emperor Go-Daigo]] issued an edict to rebuild the Greater Palace, but no resources were available to support this and the project came to nothing.<ref>Hall (1974) p. 27</ref> The present [[Kyoto Gosho|Kyoto Imperial Palace]] is located immediately to the west of the site of the {{Nihongo|Tsuchimikado Mansion|土御門殿|tsuchimikadodono}}, the great Fujiwara residence in the north-eastern corner of the city.<ref>McCullough (1999) p. 175</ref> The [[Jingi-kan]], the final standing section of the palace, remained in use until 1585.

== Primary sources ==
[[Image:HeiankyoDaigokudenAto.jpg|thumb|Memorial stone at the site of the Daigokuden hall of the palace.]]

While the palace itself has been completely destroyed, a significant amount of information regarding it has been obtained from contemporary and almost contemporary sources. The Heian Palace figures as a background for action in many Heian period literary texts, both fiction and non-fiction. These provide important information on the palace itself, court ceremonies and functions held there as well as everyday routines of the courtiers living or working there. Notable examples include the [[Tale of Genji]] by [[Murasaki Shikibu]], the so-called [[Pillow Book]] by [[Sei Shōnagon]] and the chronicle [[Eiga Monogatari]]. In addition, paintings in certain [[emakimono]] picture scrolls depict (sometimes fictional) scenes that took place at the palace; the Genji Monogatari Emaki, dating from about 1130 is perhaps the best-known example. Finally, there are also partially damaged contemporary maps of the palace from the 10th and 12th centuries showing the layout and function of the buildings within Dairi.<ref>
Farris (1998) p. 188</ref>

In addition to literary evidence, archaeological excavation conducted mainly since the late 1970s have revealed further information about the palace. In particular, the existence and location of buildings such as the Buraku-in compound has been verified against the contemporary documentary sources.<ref name="McCullough p. 111"/>

== Greater Palace (Daidairi) ==
The {{Nihongo|'''Greater Palace'''|大内裏|daidairi}} was a walled rectangular area extending approximately {{convert|1.4|km|mi}} from north to south between the first and second major east-west avenues ({{nihongo|Ichijō ōji|一条大路|}} and {{Nihongo|Nijō ōji|二条大路|}} and {{convert|1.2|km|mi}} from west to east between the {{Nihongo|Nishi Ōmiya ōji|西大宮大路|}} and {{Nihongo|Ōmiya ōji|大宮大路|}} north-south avenues.<ref>Maps of the city and Daidairi McCullough and McCullough (1980) pp. 834–835; dimensions McCullough (1999) p. 103</ref>
The three main structures within the Greater Palace were the ''Official Compound'' {{Nihongo|'''Chōdō-in'''|朝堂院|}}, the ''Reception Compound'' {{Nihongo|'''Buraku-in'''|豊楽院|}} and the {{Nihongo|'''Inner Palace'''|内裏|dairi}}. 

=== Chōdō-in ===
[[Image:DaidairiPlan.png|thumb|350px|left|Schematic plan of the Greater Palace]]
Chōdō-in was a rectangular walled enclosure situated directly to the north of the Suzakumon gate in the centre of the southern wall of the Greater Palace. It was based on Chinese models and followed Chinese architectural styles, and archaeological evidence from earlier capitals shows that this building complex was present in earlier palaces and had a remarkably stable design from the 7th century onwards.<ref>Hall (1974) pp. 11–12</ref>

==== Daigokuden ====
The main building within the Chōdō-in was the {{Nihongo|Daigokuden|大極殿|}} or the Great Audience Hall, facing south at the northern end of the compound. This was a large (approximately 52&nbsp;m (170&nbsp;ft) east to west and 20&nbsp;m (65&nbsp;ft) north to south<ref name="McCullough p. 111">McCullough (1999) p. 111</ref>) Chinese-style building with white walls, vermilion pillars and  green tiled roofs, intended to host the most important state ceremonies and functions. The southern part of the Chōdō-in was occupied by the Twelve Halls where the bureaucracy was seated for ceremonies according to strict order of precedence.  The [[Heian Jingū]] shrine in Kyoto includes an apparently faithful reconstruction of the Daigokuden in somewhat reduced scale. 

It was in the Chōdō-in that Accession Audiences were held, the [[Emperor of Japan|emperor]] was supposed to preside over early morning deliberations on major state affairs by the bureaucracy, receive monthly reports from officials, hold New Year Congratulations and receive foreign ambassadors.<ref name="McCullough & McCullough pp. 836–837">McCullough and McCullough (1980) pp. 836–837</ref> However, the practice of the morning deliberations ceased to be followed by 810<ref name="McCullough p. 40">McCullough (1999) p. 40</ref> as did the monthly reports. Foreign ambassadors were no longer received for most of the Heian period, and the New Year celebrations were abbreviated and moved into the [[#Inner Palace (Dairi)|Dairi]] by the end of the 10th century, leaving the Accession Audiences and certain Buddhist ceremonials as the only ones held in the Chōdō-in.<ref name="McCullough & McCullough pp. 836–837"/>

=== Buraku-in ===
The Buraku-in was another large rectangular Chinese-style compound, situated to the west of the Chōdō-in. It was built for official celebrations and banquets and used also for other types of entertainment such as archery contests.<ref name="McCullough p. 111"/> Like the Chōdō-in, also the Buraku-in had a hall at the central northern end of the enclosure overseeing the court. This hall, the {{Nihongo|Burakuden|豊楽殿}}, was used by the emperor and courtiers presiding over activities in the Buraku-in. However, like the Chōdō-in, the Buraku-in also fell gradually into disuse as many functions were moved to the Dairi.<ref name="McCullough & McCullough pp. 836–837"/> Its site is one of the few within the palace area that has been excavated.<ref name="McCullough p. 111"/>

=== Other buildings ===
Apart from the Inner Palace, the remaining area of the Greater Palace was occupied by ministries, lesser offices, workshops, storage buildings and the large open space of the ''Banqueting Pine Grove'' or {{Nihongo|En no Matsubara|宴の松原|}} to the east of the Dairi. The buildings of the ''Council of State'' or {{Nihongo|Daijōkan|太政官|}} were situated in a walled enclosure immediately to the east of the Chōdō-in, laid out in the typical symmetrical plan of buildings opening to a courtyard in the south. The palace also housed the {{Nihongo|{{visible anchor|Shingon-in}}|真言院|}}, apart from [[Tō-ji]] and [[Sai-ji]], the only [[Buddhism|Buddhist]] establishment permitted within the capital.<ref>Hall (1974) p. 13</ref> Its placement right next to the Inner Palace shows the influence of the [[Shingon Buddhism|Shingon sect]] during the early Heian Period.

== Inner Palace (Dairi) ==
[[Image:DairiPlan.png|thumb|350px|right|Schematic plan of the Inner Palace]]

The Inner Palace or Dairi was located to the north-east of the Chōdō-in, somewhat to the east of the central north-south axis of the Greater Palace. Its central feature was the ''Throne Hall''.  The Dairi encompassed the emperor's quarters and the pavilions of the imperial consorts and ladies-in-waiting (collectively, the ''[[Kōkyū]]''). The Dairi was enclosed within two sets of walls. In addition to the Dairi itself, the outer walls enclosed some household offices, storage areas, and the {{Nihongo|Chūwain|中和院|}}, a walled area of [[Shinto]] buildings associated with the emperor's religious functions, situated to the west of the Dairi itself, at the geographic centre of the Greater Palace. The principal gate of the larger enclosure was the {{Nihongo|Kenreimon gate|建礼門|}}, located in the southern wall along the median north-south axis of the Dairi.<ref>Plan of the Inner Palace in McCullough and McCullough (1980) p. 840</ref>

The Dairi proper, the residential compound of the emperor, was enclosed within another set of walls to the east of Chūwain. It measured approximately 215&nbsp;m (710&nbsp;ft) north to south and 170&nbsp;m (560&nbsp;ft) east to west.<ref name="McCullough pp. 115–116">McCullough (1999) pp. 115–116</ref> The main gate was the {{Nihongo|Shōmeimon gate|承明門|}} at the centre of the southern wall of the Dairi enclosure, immediately to the north of the Kenreimon gate. In contrast to the solemn official Chinese-style architecture of the Chōdō-in and the Buraku-in, the Dairi was built in more intimate Japanese architectural style — if still on a grand scale. The Inner Palace represented a variant of the [[Shinden zukuri|shinden style]] architecture used in the aristocratic villas and houses of the period. The buildings, with unpainted surfaces and gabled and shingled cypress bark roofs, were raised on elevated wooden platforms and connected to each other with covered and uncovered slightly elevated passages. Between the buildings and passages were gravel yards and small gardens. 

=== Shishinden ===
The largest building of the Dairi was the ''Throne Hall'' or {{Nihongo|Shishinden|紫宸殿|}}, a building reserved for [[official functions]]. It was a rectangular hall measuring approximately 30&nbsp;m (98&nbsp;ft) east to west and 25&nbsp;m (82&nbsp;ft) north to south,<ref name="McCullough pp. 115–116"/> and situated along the median north-south axis of the Dairi, overseeing a rectangular courtyard and facing the Shōmeimon gate. A tachibana orange tree and a sakura cherry tree stood symmetrically on both sides of the front staircase of the building. The courtyard was flanked on both sides by smaller halls connected to the Shishinden, creating the same configuration of buildings (influenced by Chinese examples) that was found in the aristocratic shinden style villas of the period. 

[[Image:Imperial Palace in Kyoto - looking into south gate of main building 3.JPG|thumb|left|The Shishinden of the present-day [[Kyoto Imperial Palace]], built according to Heian period models]]
The Shishinden was used for official functions and ceremonies that were not held at the Daigokuden of the Chōdō-in complex. It took over much of the intended use of the larger and more formal building from an early date, as the daily business of government ceased to be conducted in the presence of the emperor in the Daigokuden already at the beginning of the ninth century.<ref name="McCullough p. 40"/> Connected to this diminishing reliance on the official government procedures described in the ''[[Ritsuryō|Ritsuryō code]]'' was the establishment of a personal secretariat to the emperor, the ''Chamberlain's Office'' or {{Nihongo|[[Kurōdodokoro]]|蔵人所|}}. This office, which increasingly took over the role of coordinating the work of government organs, was set up in the {{Nihongo|Kyōshōden|校書殿|}}, the hall to the south-west of the Shishinden.<ref>McCullough and McCullough (1980) pp. 817–818</ref>

=== Jijūden ===
To the north of the Shishinden stood the {{Nihongo|Jijūden|仁寿殿|}}, a similarly constructed hall of somewhat smaller size that was intended to function as the emperor's living quarters. However, beginning already in the ninth century, the emperors often chose to reside in other buildings of the Dairi. A third still smaller hall, the {{Nihongo|Shōkyōden|承香殿|}} was located next to the north along the main axis of the Dairi. After the Dairi was rebuilt following a fire in 960, the regular residence of the emperors moved to the smaller {{Nihongo|Seiryōden|清涼殿|}},<ref name="McCullough pp. 174–175">McCullough (1999) pp. 174–175</ref> an east-facing building located immediately to the north-west from Shishinden. Gradually the Seiryōden began to be used increasingly for meetings as well, with emperors spending much of their time in this part of the palace. The busiest part of the building was the {{Nihongo|Courtiers Hall|殿上間|Tenjōnoma}}, where high-ranking nobles came to meet in the presence of the emperor.

=== Other buildings ===
The empress, as well as the official and unofficial imperial consorts, was also housed in the Dairi, occupying buildings in the northern part of the enclosure. The most prestigious buildings, housing the empress and the official consorts, were the ones that had appropriate locations for such use according to the originally Chinese design principles (the {{Nihongo|Kokiden|弘徽殿|}}, the {{Nihongo|Reikeiden|麗景殿|}} and the {{Nihongo|Jōneiden|常寧殿|}}, as well as the ones closest to the imperial residence in Seiryōden (the {{Nihongo|Kōryōden|後涼殿|}} and the {{Nihongo|Fujitsubo|藤壷|}}).<ref>McCullough and McCullough (1980) pp. 845–847</ref>
The lesser consorts and ladies-in-waiting occupied other buildings in the northern half of the Dairi.

One of the [[Imperial Regalia of Japan]], the emperor's replica of the [[Yata no kagami|sacred mirror]], was also housed in the {{Nihongo|Unmeiden hall|温明殿|}} of the Dairi.<ref>McCullough and McCullough (1980) p. 848</ref>

The present-day [[Kyoto Gosho|Kyoto Imperial Palace]], located in what was the north-eastern corner of Heian-kyō, reproduces much of the Heian-period Dairi, in particular the Shishinden and the Seiryōden.

== See also ==
[[Image:Heian Jingu Daigokuden.jpg|thumb|The modern reconstruction of the Heian Palace Daigokuden in [[Heian Jingū]], [[Kyoto]]]]
* [[Architecture of Japan]]
* [[Heian period]]
* [[Kyoto Imperial Palace]]

== Notes ==
{{reflist|25em}}

== References ==
*{{citation|last=Farris|first=William Wayne|year=1998|title=Sacred Texts and Buried Treasures: Issues on the Historical Archaeology of Ancient Japan |place=Honolulu, HW|publisher=University of Hawai'i Press|ISBN=0-8248-2030-4}}
*{{citation|last=Hall|first=John W.|year=1974|contribution=Kyoto as Historical Background|editor1-last=Hall|editor1-first= John W.|editor2-last=Mass|editor2-first=Jeffrey|title=Medieval Japan &ndash; Essays in Institutional History|place=Stanford, CA|publisher=Stanford University Press|isbn=0-8047-1511-4}}
* {{citation|last=McCullough|first=William H.|year=1999|contribution=The Heian court 794–1070; The capital and its society|editor1-last=Shively|editor1-first= Donald H.|editor2-last=McCullough|editor2-first=William H.|title=The Cambridge History of Japan: Heian Japan|place=Cambridge, UK|publisher=Cambridge University Press|ISBN=0-521-22353-9|volume=2}}
*{{Citation
  | last=McCullough
  | first=William H.
  | last2=McCullough
  | first2=Helen Craig
  | author2-link = Helen Craig McCullough
  | contribution=Appendix B: The Greater Imperial Palace
  | title=A Tale of Flowering Fortunes
  | volume=2
  | year=1980
  | isbn=0-8047-1039-2
  | pages=833–854
  | publisher=Stanford University Press
  | place=Stanford, CA
  }}

==Further reading==
* {{Citation
  | last = Imaizumi Atsuo (今泉篤男)
  | first = 
  | author-link =
  | last2 = al.
  | first2 =
  | author2-link =
  | title = Kyōto no rekishi (京都の歴史)
  | place= Tōkyō
  | publisher = Gakugei Shorin (学芸書林)
  | year = 1970
  | location =
  | volume = 1
  | edition =
  | url =
  | doi =
  | id = 
  | isbn = }}. The main Japanese reference work on the Palace according to McCullough (1999). First volume of a ten-volume general history of Kyoto.
* {{Citation
  | last = Morris
  | first = Ivan
  | author-link =
  | last2 =
  | first2 =
  | author2-link =
  | title = The World of the Shining Prince: Court Life in Ancient Japan
  | place= New York, NY
  | publisher = Kodansha
  | year = 1994
  | location =
  | volume =
  | edition =
  | url =
  | doi =
  | id = 
  | isbn = 1-56836-029-0}}. Originally published in 1964.
* {{Citation
  | last = Ponsonby-Fane
  | first = Richard Arthur Brabazon
  | author-link =
  | last2 =
  | first2 =
  | author2-link =
  | title = The Capital and Palace of Heian 
  | journal = Transactions and Proceedings of the Japan Society, London 
  | volume = 22
  | issue =
  | pages = 107
  | year = 1925
  | url =
  | doi =
  | id =  }}
* {{Citation
  | last = Ponsonby-Fane
  | first = Richard Arthur Brabazon
  | author-link =
  | last2 =
  | first2 =
  | author2-link =
  | title = Kyoto: The Old Capital of Japan, 794–1869
  | place= Kyoto
  | publisher = The Ponsonby Memorial Society
  | year = 1956
  | location =
  | volume =
  | edition =
  | url =
  | doi =
  | id = 
  | isbn = }}. A reissue of the 1931 ed. published in Hong Kong, with some new illus. and minor changes, under title: ''Kyoto: its history and vicissitudes since its foundation in 792 to 1868''. First published in article form 1925–28.

== External links ==
{{commonscat-inline}}
<!-- Id like to propose a link freeze... discuss in talk page ~~~~ -->
* [http://tukineko.pekori.jp/heian/daidai/daidai.html Japanese page with interactive map of the Palace]
* [http://www.sengokudaimyo.com/shinden/Shinden.html Page with detailed description of shinden style buildings]

{{Imperial Palaces Japan}}
{{featured article}}

[[Category:794]]
[[Category:Buildings and structures completed in the 8th century]]
[[Category:Classical Japan]]
[[Category:Buildings and structures in Kyoto]]
[[Category:Imperial residences in Japan]]
[[Category:Former buildings and structures in Japan]]
[[Category:Former palaces]]
[[Category:Historic Sites of Japan]]