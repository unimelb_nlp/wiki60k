[[File:ONL (1887) 1.259 - St Paul's and the Neighbourhood in 1540.jpg|thumb|St Paul's Cathedral, London, view as in 1540]]
The '''Convocation of 1563''' was a significant gathering of English and Welsh clerics that consolidated the [[Elizabethan religious settlement]], and brought the ''[[Thirty-Nine Articles]]'' to close to their final form (which dates from 1571). It was, more accurately, the Convocation of 1562/3 of the [[province of Canterbury]], beginning in January 1562 ([[Old Style and New Style dates|Old Style]]).

==Summary==
[[Matthew Parker]] who was [[Archbishop of Canterbury]] had prepared documents outlining further reform in the [[Church of England]], as had other bishops. A more thorough-going reform agenda was supported by over 30 of the participants. A compromise version, the "six articles", was narrowly defeated on a vote. The result was that the momentum for reform of the Church by its constitutional procedures was halted. Parker steered the outcome towards the ''[[via media]]''.<ref>{{cite book|author=[[Patrick Collinson]]|title=Archbishop Grindal, 1519-1583: The Struggle for a Reformed Church|url=https://books.google.com/books?id=hREq8H5DDRUC&pg=PA162|date=1 January 1979|publisher=University of California Press|isbn=978-0-520-03831-8|page=162}}</ref> "Swiss-inspired reformists" were headed off.<ref>{{cite book|author=Paul D. L. Avis|title=Anglicanism and the Christian Church: Theological Resources in Historical Perspective|url=https://books.google.com/books?id=dpjtH9MxW_8C&pg=PA20|year=2002|publisher=A&C Black|isbn=978-0-567-08849-9|page=20}}</ref>

The Convocation restored the position of the ''Thirty-Nine Articles'' in the Church of England. More accurately said, the ''Forty-Two Articles'' of Edward VI were reduced to a draft at this point, which was widely supported, and eventually enforced after 1571. There were further proposals from reformers, in particular on [[canon law]] and [[liturgy]], some of which originated from a group among the bishops. These, however, proved contentious, and did not pass. Subsequent contestation of the same issues made some of them a matter of authority.<ref>{{cite book|author=Felicity Heal|title=Reformation in Britain and Ireland|url=https://books.google.com/books?id=5SRANUBTt3AC&pg=PA362|accessdate=21 November 2012|year=2005|publisher=Oxford University Press|isbn=978-0-19-928015-5|page=362}}</ref>

Collinson comments that

<blockquote>''Moves to improve the settlement in the convocation of 1563 were led by the bishops rather than by 'Puritans' in the lower house'' [...]<ref>{{cite ODNB|id=24649|authorlink=Patrick Collinson|first=Patrick|last=Collinson|title=Sandys, Edwin}}</ref></blockquote>

Dawley writes that probably the surprise of the Convocation

<blockquote>[...] ''was not the amount of support given to the Precisians but the unexpected extent of loyalty to the existing regulations'',</blockquote>

"Precisian" being the term used by Parker for his opponents on the issue of clerical dress.<ref>{{cite book|author=Powel Mills Dawley|title=John Whitgift and the Reformation|year=1955|pages=66 and 70|publisher=Adam & Charles Black}}</ref>

==Participants==

===Bishops===
Of 20 bishops of the time (the see of Oxford being vacant), there were 12 who had left the Kingdom of England under Mary Tudor: the "Marian exiles". Of those who had remained, some had done so covertly.
{| class="wikitable"
!Name
!Exile?
!See
!Comments
|-
|[[Gilbert Berkeley]]
|Frankfurt
|Bath and Wells
|
|-
|[[Richard Cheyney]]||No||Bristol, Gloucester||Did not subscribe the 39 articles (ODNB)
|-
|[[Matthew Parker]]||No||Canterbury|| 
|-
|[[William Barlow (bishop of Chichester)|William Barlow]]||Germany, Poland, [[Emden]]||Chichester||
|- 
|[[Thomas Bentham]]||Zurich, Basle||Coventry and Lichfield|| 
|-
|[[Richard Cox (bishop)|Richard Cox]]||Frankfurt ||Ely||Probably a reforming bishop
|-
|[[William Alley]]||No<ref>{{cite ODNB|id=397|first=Nicholas|last=Orme}}</ref>||Exeter|| drafts
|-
|[[John Scory]]||Emden||Hereford||
|-
|[[Nicholas Bullingham]]||Emden<ref>{{cite ODNB|id=3917|first=Julian|last=Lock|title=Bullingham, Nicholas}}</ref> ||Lincoln||
|-
|[[Edmund Grindal]]||Frankfurt ||London||Reformer, supported attempts to revise the Prayer Book rubrics.<ref>{{cite book|author=G. J. R. Parry|title=A Protestant Vision: William Harrison and the Reformation of Elizabethan England|url=https://books.google.com/books?id=u8BlfZeqCO0C&pg=PA145|date=22 August 2002|publisher=Cambridge University Press|isbn=978-0-521-52218-2|page=145}}</ref>
|-
|[[John Parkhurst]]||Zurich||Norwich|| 
|-
|[[Edmund Scambler]]||London||Peterborough|| 
|-
|[[Edmund Gheast]]||No<ref>{{cite ODNB|id=11712|first=Jane|last=Freeman|title=Guest, Edmund}}</ref> ||Rochester|| drafting of article XXIX
|-
|[[John Jewel]]||Strasbourg, Zürich, Padua||Salisbury|| 
|-
|[[Robert Horne (bishop)|Robert Horne]]||Zurich, Frankfurt, Strasburg||Winchester||
|-
|[[Edwin Sandys (bishop)|Edwin Sandys]]||Frankfurt, Augsburg, Strasbourg, Zürich||Worcester||Draft on the sign of the cross in baptism (ODNB)
|-
|[[Rowland Meyrick]]||No<ref>{{cite ODNB|id=18643|first=Mihail Dafydd|last=Evans|title=Meyrick, Rowland}}</ref> ||Bangor|| 
|-
|[[Hugh Jones (bishop)|Hugh Jones]], as proxy||No||Llandaff|| 
|-
|[[Thomas Davies (bishop)|Thomas Davies]]||No||St. Asaph||   
|-
|[[Richard Davies (bishop)|Richard Davies]]||Geneva||St. David's|| 
|}

Of these bishops, 19 attended at the start—not Jones, who was acting as proxy for the aged [[Anthony Kitchin]].<ref name="Hardwick1852">{{cite book|author=[[Charles Hardwick]]|title=A history of the Articles of religion: to which is added a series of documents, from A. D. 1536 to A. D. 1615; together with illustrations from contemporary sources|url=https://books.google.com/books?id=qXkXAAAAYAAJ&pg=PA129|accessdate=22 November 2012|year=1852|publisher=H. Hooker|pages=128–9}}</ref><ref name="RabbSeigel2015">{{cite book|author1=Theodore K. Rabb|author2=Jerrold E. Seigel|title=Action and Conviction in Early Modern Europe: Essays in Honor of E.H. Harbison|url=https://books.google.com/books?id=NhvWCgAAQBAJ&pg=PA139|date=8 December 2015|publisher=Princeton University Press|isbn=978-1-4008-7606-8|page=139 note 23}}</ref>

===Lower House===
There were 27 in the Lower House of Convocation who had been émigrés of Queen Mary's time.<ref>{{cite book|author=William P. Haugaard|title=Elizabeth and the English Reformation: The Struggle for a Stable Settlement of Religion|url=https://books.google.com/books?id=YRY7AAAAIAAJ&pg=PA27|accessdate=22 November 2012|year=1968|publisher=CUP Archive|page=27|id=GGKEY:LA9WJTAP5T9}}</ref> An estimate of over 50 who had conformed in Mary's reign has also been given.<ref>{{cite book|author=William P. Haugaard|title=Elizabeth and the English Reformation: The Struggle for a Stable Settlement of Religion|url=https://books.google.com/books?id=YRY7AAAAIAAJ&pg=PA389|accessdate=23 November 2012|year=1968|publisher=CUP Archive|pages=389–90|id=GGKEY:LA9WJTAP5T9}}</ref> Carlson argues for a definite group of 34 Puritan reformers in the Lower House.<ref>{{cite book|author1=Theodore K. Rabb|author2=Jerrold E. Seigel|title=Action and Conviction in Early Modern Europe: Essays in Honor of E. H. Harbison|url=https://books.google.com/books?id=NhvWCgAAQBAJ&pg=PA134|date=8 December 2015|publisher=Princeton University Press|isbn=978-1-4008-7606-8|page=134}}</ref>

====Deans====
{| class="wikitable"
!Name
!Exile?
!Standing in convocation
!Comments
|-
|[[John Bale]]
|Basel<ref name="Bale">{{cite DNB|wstitle=Bale, John}}</ref>
|Prebendary of Canterbury.<ref name="Bale"/>
|
|-
|[[William Bradbridge]]
|No<ref name="Bradbridge">{{ODNBweb|id=3165|title=Bradbridge, William|first=Kenneth|last=Carleton}}</ref>
|Chancellor of Chichester<ref name="Bradbridge"/>
|Voted for the six articles.<ref name="Bradbridge"/>
|-
|[[William Day (bishop)|William Day]] 
|
|[[Provost of Eton College]]
|Supporter of the six articles.
|-
|[[Gabriel Goodman]] 
|
|[[Dean of Westminster]] (ODNB)
|Against further reform.
|-
|[[Francis Mallett]] 
|
|[[Dean of Lincoln]]
|Voted by proxy.
|-
|[[Alexander Nowell]] 
|
|[[Dean of St Paul's]]
|Prolocutor on the nomination of [[Matthew Parker]],<ref>{{cite book|author=Patrick Collinson|title=Archbishop Grindal, 1519–1583: The Struggle for a Reformed Church|url=https://books.google.com/books?id=hREq8H5DDRUC&pg=PA162|date=1 January 1979|publisher=University of California Press|isbn=978-0-520-03831-8|page=162}}</ref> reformer, drafted catechism
|-
|[[Laurence Nowell (priest)|Laurence Nowell]]
|
|[[Dean of Lichfield]]
|Brother of Alexander; voted to change ceremonies (ODNB)
|-
|[[John Pedder (dean)|John Pedder]]<ref name="Pedder">{{cite DNB|wstitle=Pedder, John}}</ref>
|Frankfurt<ref name="Pedder"/>
|[[Dean of Worcester]]
|Supported 21 requests, voted for six articles, subscribed the 39 articles.<ref name="Pedder"/>
|-
|[[John Salisbury (bishop)|John Salisbury]]<ref>{{cite book|author=William P. Haugaard|title=Elizabeth and the English Reformation: The Struggle for a Stable Settlement of Religion|url=https://books.google.com/books?id=YRY7AAAAIAAJ&pg=PA7|accessdate=24 November 2012|year=1968|publisher=CUP Archive|page=7|id=GGKEY:LA9WJTAP5T9}}</ref>
|
|[[Dean of Norwich]]
|Subscribed to the 39 articles, signed the petition for discipline.<ref>{{cite DNB|wstitle=Salisbury, John (1500?-1573)}}</ref>
|-
|[[Thomas Sampson]]
|Frankfurt, Zürich, Geneva, Lausanne (ODNB)
|[[Dean of Christ Church, Oxford]]
|Radical, opponent of ceremonial
|}

[[Nicholas Wotton]], [[Dean of Canterbury]], did not attend.(ODNB)

====Archdeacons====
{| class="wikitable"
!Name
!Exile?
!Standing in convocation
!Comments
|-
|[[John Aylmer (bishop)|John Aylmer]]
|Switzerland
|[[Archdeacon of Lincoln]]
|Held back in debate,(ODNB) subscribed to the 39 articles.<ref>{{cite DNB|wstitle=Aylmer, John}}</ref>
|-
|[[Robert Beaumont (Master of Trinity College)|Robert Beaumont]]
|Geneva.<ref name="Beaumont">{{cite DNB|wstitle=Beaumont, Robert (d.1567)}}</ref>
|[[Archdeacon of Huntingdon]]<ref name="Beaumont"/>
|Among the 33 signatories.(ODNB) Supported the six articles.<ref name="Beaumont"/>
|-
|[[John Bridgewater]] 
|No
|[[Archdeacon of Rochester]]
|Voted against the six articles. (ODNB)
|-
|[[Thomas Cole (priest)|Thomas Cole]]
|Frankfurt<ref name="Cole">{{cite ODNB|id=5856|first=Brett|last=User|authorlink=Brett Usher|title=Cole, Thomas}}</ref>
|[[Archdeacon of Essex]]
|One of the 34 proposing the seven articles; abstained on the six articles.<ref name="Cole"/>
|-
|[[Robert Crowley (printer)|Robert Crowley]]
|
|[[Archdeacon of Hereford]]?. Canon of Hereford.(ODNB)
|
|-
|[[Thomas Lever]] 
|
|[[Archdeacon of Coventry]]
|reformer.
|-
|[[John Molyns|John Mullins]]
|
|[[Archdeacon of London]]
|Supported seven articles, abstained on six articles.
|-
|[[John Pullain]]<ref name="Pullain">{{cite DNB|wstitle=Pullain, John}}</ref>
|Geneva<ref name="Pullain"/>
|[[Archdeacon of Colchester]]
|Advocate of Calvinism.<ref name="Pullain"/>
|-
|[[Nicholas Robinson (bishop)|Nicholas Robinson]]
|
|[[Archdeacon of Merioneth]]
|
|-
|[[Richard Rogers (bishop)|Richard Rogers]] 
|Frankfurt<ref>{{cite ODNB|id=23994|title=Rogers, Richard|first=Stanford|last=Lehmberg}}</ref>
|[[Archdeacon of St Asaph]]<ref>{{cite DNB|wstitle=Rogers, Richard (1532?-1597)|volume=49}}</ref>
|Reformer.
|-
|Thomas Spencer
|Zürich<ref>{{cite book|author=Christina Hallowell Garrett|title=The Marian Exiles: A Study in the Origins of Elizabethan Puritanism|url=https://books.google.com/books?id=mNZxec42AhEC&pg=PA8|date=10 June 2010|publisher=Cambridge University Press|isbn=978-1-108-01126-6|page=8 note 5}}</ref>
|[[Archdeacon of Chichester]]<ref name="Bloxam1857">{{cite book|author=John Rouse Bloxam|title=A register of the presidents, fellows, demies, instructors in grammar and in music, chaplains, clerks, choristers, and other members of Saint Mary Magdalen College in the university of Oxford, from the foundation of the college to the present time|url=https://books.google.com/books?id=C2pLAAAAcAAJ&pg=PR70|year=1857|page=lxx}}</ref>
|-
|[[John Watson (bishop)|John Watson]]
|
|[[Archdeacon of Surrey]]
|Conservative.(ODNB)
|-
|[[Thomas Watts (priest)|Thomas Watts]]
|Frankfurt (ODNB)
|[[Archdeacon of Middlesex]]
|Reformer.
|-
|[[Robert Wisdom]]
|Frankfurt, Heidelberg (ODNB)
|[[Archdeacon of Ely]]
|Voted for six articles.
|}

====Proctors====
{| class="wikitable"
!Name
!Exile?
!Standing in convocation
!Comments
|-
|[[Thomas Bickley]]
|France<ref>{{cite DNB|wstitle=Bickley, Thomas}}</ref>
|Proctor for Coventry and Lichfield,
|Reformer.
|-
|Walter Bower
|
|Proctor for the clergy of Somerset<ref name="Bloxam1857"/>
|
|-
|[[James Calfhill]] 
|
|Three votes; proctor for the London clergy and Oxford chapter<ref>{{cite DNB|wstitle=Calfhill, James|volume=8}}</ref>
|One of the 34 signing the seven articles. (ODNB)
|-
|[[Thomas Godwin (bishop)|Thomas Godwin]] 
|
|Proctor for Lincoln chapter (ODNB)
|Voted for further reform.
|-
|[[Thomas Huet]]
|No
|??; precentor of St David's Cathedral (ODNB)
|Signed the 39 articles.
|-
|[[Thomas Lancaster]]
|
|??
|Voted for six articles. (58/59 ODNB)
|-
|[[Robert Lougher]]
|
|Proctor for the clergy of the diocese of Exeter (ODNB)
|Opposed six articles.
|-
|[[Stephen Nevinson]] 
|
|Proctor for the clergy of the diocese of Canterbury (ODNB)
|Reformer
|-
|[[Andrew Peerson]]
|
|Proctor for Llandaff (ODNB)
|Voted against six articles.
|-
|[[Michael Renniger]]
|
|Proctor for Winchester chapter (ODNB)
|Reformer.
|-
|[[Arthur Saul]]
|
|Proctor for Gloucester chapter (ODNB)
|Reformer.
|-
|[[John Walker (archdeacon)|John Walker]]
|Not known (ODNB)
|Proctor for the Suffolk clergy
|
|-
|[[Robert Weston]]
|
|Proctor for the Lichfield clergy<ref>{{cite book|author=James Murray|title=Enforcing the English Reformation in Ireland: Clerical Resistance and Political Conflict in the Diocese of Dublin, 1534-1590|url=https://books.google.com/books?id=kvFpuRoiL24C&pg=PA266|date=21 July 2011|publisher=Cambridge University Press|isbn=978-0-521-36994-7|page=266}}</ref>
|
|-
|[[Percival Wiburn]]
|
|Proctor for Rochester chapter (ODNB)
|
|}

==Procedure==
The Convocation was called simultaneously with a Parliament, and took place in London, in [[St Paul's Cathedral]].<ref>[[s:The Cambridge Modern History/Volume II/Chapter XVI]]</ref><ref>{{cite book|author=Bruce C. Daniels|title=New England Nation: The Country the Puritans Built|url=https://books.google.com/books?id=thTIAAAAQBAJ&pg=PA20|date=6 September 2012|publisher=Palgrave Macmillan|isbn=978-1-137-02563-0|pages=20–}}</ref> Its sessions took place from 11 January to 14 April 1563 ([[New Style|N.S.]]).<ref>{{cite book|author1=Theodore K. Rabb|author2=Jerrold E. Seigel|title=Action and Conviction in Early Modern Europe: Essays in Honor of E. H. Harbison|url=https://books.google.com/books?id=NhvWCgAAQBAJ&pg=PA133|date=8 December 2015|publisher=Princeton University Press|isbn=978-1-4008-7606-8|page=133 note 1}}</ref> [[Robert Weston]] opened the Convocation on 12 January, formally, with a prorogation to the following day.<ref>{{cite web|url=https://archive.org/stream/livesofarchbisho09hookuoft#page/340/mode/2up|title=Lives of the Archbishops of Canterbury|last=Hook|first=Walter Farquhar|year=1860|work=[[Internet Archive]]|publisher=Bentley|volume=9|pages=341–2|accessdate=27 January 2016|location=London}}</ref> The actual proceedings of Convocation opened on 13 January, when the [[Litany]] was sung, and a Latin sermon by [[William Day (bishop)|William Day]] preached.<ref>{{cite ODNB|id=7373|first=Brett|last=Usher|title=Day, William}}</ref><ref>{{cite book|author=[[Ralph Churton]]|title=The Life of Alexander Nowell Dean of St. Paul's|url=https://books.google.com/books?id=vBI6AAAAcAAJ&pg=PA8|year=1809|publisher=University Press|page=8}}</ref><ref>{{cite book|author=Church of England|title=The Book of Common Prayer and Administration of the Sacraments, and Other Rites and Ceremonies of the Church, According to the Use of the United Church of England and Ireland: Together with the Psalter Or Psalms of David ... and the Form Or Manner of Making, Ordaining, and Consecrating of Bishops, Priests, and Deacons. The Text Taken from the Sealed Book for the Chancery ... With Notes, Legal and Historical|url=https://books.google.com/books?id=7PnbWWGYyhkC&pg=PA540|year=1849|publisher=For the Ecclesiastical History Society|pages=540 note}}</ref>

==The 39 Articles, to 1571==
The subsequent passage of the 39 Articles into the orthodoxy of the Church of England was tortuous. There are various versions of the Articles: manuscript from the Convocation, printed in Latin ([[Reyner Wolfe]]) and English by [[John Cawood (printer)|John Cawood]] and [[Richard Jugge]] (1563); printed later.<ref name="Tayler1853">{{cite book|author=John James Tayler|title=A retrospect of the religious life of England: or, The church, Puritanism, and free inquiry|url=https://books.google.com/books?id=-hULAAAAYAAJ&pg=PA55|accessdate=23 November 2012|year=1853|publisher=J. Chapman|page=55}}</ref> A bill in the Parliament of 1566 to confirm the articles from the Convocation was halted in the House of Lords, by pressure from the Queen.<ref>{{cite book|author=Wallace T. MacCaffrey|title=Shaping of the Elizabethan Regime|url=https://books.google.com/books?id=VBjWCgAAQBAJ&pg=PA214|date=8 December 2015|publisher=Princeton University Press|isbn=978-1-4008-7586-3|page=214}}</ref>

<!-- Articles XX and XXIX?

Bishops subscribing in 1571.<ref name="RoseMaitland1841">{{cite book|author1=[[Hugh James Rose]]|author2=[[Samuel Roffey Maitland]]|title=The British Magazine and Monthly Register of Religious and Ecclesiastical Information, Parochial History, and Documents Respecting the State of the Poor, Progress of Education, Etc|url=https://books.google.com/books?id=SMhPAAAAMAAJ&pg=PA323|accessdate=22 November 2012|year=1841|publisher=J. Petheram|page=323}}</ref>
 -->

==References==
{{reflist}}

[[Category:History of the Church of England]]
[[Category:1560s in England]]
[[Category:1562 in Christianity]]
[[Category:1563 in Christianity]]
[[Category:1562 in England]]
[[Category:1563 in England]]