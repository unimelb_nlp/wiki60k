{{Italic title}}
{{Infobox journal
| title         = Mycologia
| formernames   =
| cover         = [[File:Mycologia cover.jpg|150px]]
| caption       = Cover of the first issue
| editor        = [[Jeffrey K. Stone]]
| discipline    = [[Mycology]]
| abbreviation  = Mycologia
| publisher     = [[Mycological Society of America]]
| country       = [[United States]]
| frequency     = 6/year
| history       = 1909–present<br>Merger of 
:''Journal of Mycology'' (1885–1908)<br>
:''Mycological Bulletin'' (1903-1908)
| openaccess    = 
| license       = 
| impact        = 1.587
| impact-year   = 2009
| website       = http://www.mycologia.org 
| link1         = http://www.mycologia.org/current.shtml
| link1-name    = Online access
| link2         = http://www.mycologia.org/contents-by-date.0.shtml
| link2-name    = Online archive
| RSS           = 
| atom          = 
| JSTOR         = 00275514
| OCLC          = 1640733
| LCCN          = 57051730
| CODEN         = MYCOAE
| ISSN          = 0027-5514
| eISSN         = 1557-2536
| boxwidth      = 
}}
'''''Mycologia''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] that publishes papers on all aspects of the [[fungi]], including [[lichens]]. It first appeared as a bimonthly journal in January 1909, published by the [[New York Botanical Garden]] under the editorship of [[William Murrill]]. It became the official journal of the [[Mycological Society of America]], which still publishes it today. It was formed as a merger of the ''Journal of Mycology'' (14 volumes; 1885–1908) and the ''Mycological Bulletin'' (7 volumes; 1903-1908). The ''Mycological Bulletin'' was known as the ''Ohio Mycological Bulletin'' in its first volume.

==Editors-in-chief==
The following persons have been editor-in-chief of the journal:
{{columns-list|colwidth=20em|
* 1909–1924 - [[William Murrill]]
* 1924–1932 - [[Fred J. Seaver]]
* 1933 - [[H.M. Fitzpatrick]]
* 1933–1934 - [[J.A.Stevenson]]
* 1934–1935 - [[F.A. Wolf]]
* 1935–1936 - [[G.R. Bisby]]
* 1945–1950 - [[Alexander H. Smith]]
* 1951–1957 - [[George Willard Martin|G.W. Martin]]
* 1958–1960 -  [[Donald P. Rogers]]
* 1960–1965 - [[Clark Thomas Rogerson|Clark T. Rogerson]]
* 1966–1970 - [[R.W. Lichtward]]
* 1967 - [[Joseph C. Gilman]]
* 1971–1975 - [[R.K. Benjamin]]
* 1976–1980 - [[Margaret Elizabeth Barr-Bigelow|M. Barr Bigelow]]
* 1981–1985 - [[T.W. Johnson, Jr.]]
* 1986–1990 - [[H. Petersen]]
* 1991–1995 - [[David McLaughlin]]
* 1996–2000 - [[David H. Griffin]]
* 2001–2004 - [[Joan W. Bennett]]
* 2004–2009 - [[Don Natvig]]
* 2009–2014 - [[Jeffrey K. Stone]]
}}

==Abstracting and indexing==
''Mycologia'' is abstracted and indexing in the following databases:
{{columns-list|colwidth=20em|
*[[Academic Search Premier]]
*[[AGRICOLA]]
*[[Biosis]]
*[[EMBASE]]
*[[Geobase]]
*[[MEDLINE]]
*[[Science Citation Index]]
*[[Scopus]]
}}

==References==
*{{cite journal | last1 = Dearness | first1 = J | year = 1938 | title = The background of mycology and of ''Mycologia'': both should be more widely known | url = | journal = Mycologia | volume = 30 | issue = 2| pages = 111–19 | doi=10.2307/3754550}}
*{{cite journal | last1 = Farr | first1 = ML | year = 1982 | title = Developmental studies on the MSA | url = | journal = Mycologia | volume = 74 | issue = 1| pages = 1–19 }}

==External links==
*{{Official website|http://www.mycologia.org}}

[[Category:Mycology journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1909]]
[[Category:Biweekly journals]]
[[Category:1909 establishments in the United States]]