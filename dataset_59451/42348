{{Use dmy dates|date=June 2015}}
{{Use British English|date=June 2015}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = B-3 (M.1/30)<!--please avoid stating manufacturer in this field; it's stated two lines below -->
  |image = BlM.130.jpg
  |caption = <!--image caption; if it isn't descriptive it should be left blank-->
}}{{Infobox Aircraft Type
  |type = Torpedo Bomber
  |manufacturer = [[Blackburn Aircraft]]
  |designer = <!--only appropriate for individuals, not for project leaders or a company or corporation-->
  |first flight = 8 March 1932
  |introduced = <!--date the aircraft entered or will enter military or revenue service-->
  |introduction= <!--date the aircraft will enter military or revenue service, if the aircraft is still in development -->
  |retired = <!--date the aircraft left military or revenue service. If vague or multiples, it probably should be skipped-->
  |status = Prototype
  |primary user = <!--please list only one, and don't use those tiny flags as they limit horizontal space-->
  |more users = <!--limited to three "more users" total. please separate with <br/>-->
  |produced = <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
  |number built = 2
  |unit cost = 
  |developed from = <!-- the aircraft which formed the basis for the topic type -->
  |variants with their own articles = <!-- variants OF the topic type -->
}}
|}

The '''Blackburn B-3''' was a prototype [[United Kingdom|British]] [[torpedo bomber]] designed and built by [[Blackburn Aircraft]] as a potential replacement for the [[Blackburn Ripon|Ripon]]. It was unsuccessful, with only the two prototypes being built.

==Design and development==
In 1930, the British [[Air Ministry]] issued [[List of Air Ministry Specifications|Specification M.1/30]] for a [[aircraft carrier|carrier-based]] torpedo bomber to replace the [[Blackburn Ripon|Ripon]], to be powered by the [[Rolls-Royce Buzzard]] or [[Armstrong Siddeley Leopard]] engines.  Prototypes were ordered from Blackburn, [[Handley Page]] and [[Vickers]]. The Blackburn design was a single-bay [[biplane]], with a fabric-covered steel tube [[fuselage]], powered by a Buzzard engine. The prototype was flown first on 8 March 1932 <ref name ="mason bomber">{{Cite book |author=Mason, Francis K |authorlink =
|title=The British Bomber since 1914 |publisher=Putnam Aeronautical Books |location= London |year=1994 |isbn= 0-85177-861-5}}</ref> and crashed in June 1933 following an engine failure.  Because it had been ordered by the Air Ministry, this machine carried an RAF serial (''S1640'') and was known throughout its life as the '''M.1/30''', after the Specification.<ref>{{cite book |title= Blackburn Aircraft since 1909|last= Jackson|first=A.J.|year=1968|page=344|publisher=Putnam Publishing |location=London |isbn=0-370-00053-6}}</ref>

Following relaxation of some of the specifications requirements, Blackburn constructed a second aircraft as a private venture, with a watertight metal [[monocoque]] fuselage replacing the previous steel tube fuselage,<ref name="lewis bomber">{{cite book |title=The British Bomber since 1914 |last=Lewis |first=Peter |edition= Third |date= |year=1980  |publisher=Putnam |location= London |isbn= 0-370-30265-6}}</ref> this first flying on 24 February 1933.<ref name ="mason bomber"/> Because it was a private venture it received and carried the Blackburn [[United Kingdom aircraft test serials|Class B civil test marking]] '''B-3''' and was referred to as such, though it was also known as the '''M.1/30A'''.<ref>{{cite book |title= Blackburn Aircraft since 1909|last= Jackson|first=A.J.|year=1968|page =345|publisher=Putnam Publishing |location=London |isbn=0-370-00053-6}}</ref> It performed poorly during testing, still being incapable of meeting the performance requirements of the specification even though they had been relaxed, and being too heavy for the carrier deck lifts. As none of the competitors for the specification could meet its requirements, the specification was cancelled, with no aircraft being ordered.

==Specifications (M.1/30A)==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop
|ref=Mason, The British Bomber since 1914 <ref name ="mason bomber"/>
|crew=two
|capacity=
|length main= 39 ft 10 in
|length alt=12.14  m
|span main= 49 ft 6 in
|span alt= 15.09 m
|height main= 14 ft 7 in
|height alt= 4.45 m
|area main= 651 ft²
|area alt= 30.5 m²
|airfoil=
|empty weight main= 6,138 lb
|empty weight alt= 2,790 kg
|loaded weight main= 10,393 lb
|loaded weight alt= 4,724 kg
|useful load main=  
|useful load alt=  
|max takeoff weight main=  
|max takeoff weight alt=  
|more general=
|engine (prop)=[[Rolls-Royce Buzzard]] IIIMS
|type of prop=V-12 engine
|number of props=1
|power main= 825 hp
|power alt= 615 kW
|power original=
|max speed main= 123 kn
|max speed alt= 142 mph, 229 km/h
|cruise speed main=  
|cruise speed alt=  
|never exceed speed main=  
|never exceed speed alt=  
|stall speed main= 
|stall speed alt=  
|range main=  
|range alt=  
|ceiling main= 9,150 ft
|ceiling alt= 2,790 m
|climb rate main=  
|climb rate alt=  
|loading main= 16.0 lb/ft²
|loading alt= 155 kg/m²
|thrust/weight=<!-- a unitless ratio -->
|power/mass main= 0.079 hp/lb
|power/mass alt= 130 W/kg
|more performance=*'''Climb to 6,500 ft (2,000 m):''' 20 min
|armament=
* 1 × fixed, forward-firing .303 in (7.7 mm) [[Vickers machine gun]] (not Mk II) and 1 × .303 in (7.7 mm) [[Lewis Gun]] in rear cockpit
*1 × 1,900 lb (860 kg) 18 in (457 mm) torpedo ''or'' 4 × 551 lb (250 kg) bombs
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=<!-- related developments -->
|similar aircraft=
*[[Vickers Type 207]]
*[[Handley Page H.P.46]]<!-- similar or comparable aircraft -->
|sequence=<!-- designation sequence, if appropriate -->
|lists=<!-- related lists -->
|see also=<!-- other relevant information -->
}}

==References==

{{Reflist}}

==External links==
{{commons category|Blackburn B-3}}
* [https://web.archive.org/web/20070927075456/http://www.historicaircraft.org/British-Aircraft/pages/Blackburn-M1-30A.html Historical Aircraft Photo]

{{Blackburn aircraft}}

[[Category:British bomber aircraft 1930–1939]]
[[Category:Blackburn aircraft|M.1 30]]
[[Category:Single-engined tractor aircraft]]
[[Category:Biplanes]]