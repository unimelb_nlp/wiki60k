{{for|the song by [[Styx (band)|Styx]]|The Grand Illusion}}
{{good article}}
{{Infobox song
| Name           = Castle Walls
| Artist         = [[T.I.]] featuring [[Christina Aguilera]]
| Album          = [[No Mercy (T.I. album)|No Mercy]]
| Recorded       = 2010
| Genre          = {{flat list|
*[[Hip hop music|Hip hop]]
*[[electro (music)|electro]]}}
| Length         = 5:29
| Label          = {{flat list|
*[[Grand Hustle Records|Grand Hustle]]
*[[Atlantic Records|Atlantic]]}}
| Writer         = {{flat list|
*[[T.I.|Clifford Harris]]
*[[Alex da Kid|Alexander Grant]]
* [[Skylar Grey|Holly Hafferman]]}}
| Producer       = Alex da Kid
| Tracks         = 
| prev           = "Lay Me Down"
| prev_no        = 13
| track_no       = 14
| next           = "[[I'm Back (song)|I'm Back]]"<br>{{small|(deluxe edition)}}
| next_no        = 15
}}
"'''Castle Walls'''" is a song by American singers [[T.I.]] and [[Christina Aguilera]], from T.I.'s seventh studio album ''[[No Mercy (T.I. album)|No Mercy]]'' (2010). [[Alex da Kid]] produced the song and co-wrote it along with [[Skylar Grey]] and T.I. The song was initially produced for [[Sean Combs|Diddy]]'s album ''[[Last Train to Paris]]'', but Diddy felt that "Castle Walls" would be better suited to T.I.; Aguilera was later chosen as the featured artist on the song. A [[hip hop music|hip hop]] and [[electro (music)|electro]] number, "Castle Walls" received mixed response from [[music journalism|music critics]], some of whom picked it as a highlight from ''No Mercy'', and some others criticized the song's lyrics. Despite not being released as a [[single (music)|single]], the track still managed to appear on [[record chart]]s of several nations, including on the US [[Bubbling Under Hot 100 Singles]], where it peaked at number five.

== Background ==

Originally, "Castle Walls" belonged to [[Sean Combs|Diddy]], who had commissioned the song for his fifth album ''[[Last Train to Paris]]'' with his group [[Diddy-Dirty Money|Dirty Money]]. But Diddy told [[T.I.]], "Yeah, this is my record, but you know what, I think this is a better fit for you. I think you should rock out on this one. I think this speaks volumes to where you are, what you going through, what you living and how you feel."<ref name="MTV">{{cite web|last=Rodriguez |first=Jayson |url=http://www.mtv.com/news/1653943/exclusive-ti-reveals-diddy-gave-him-castle-walls/ |title=Exclusive: T.I. Reveals Diddy Gave Him 'Castle Walls'|publisher=[[MTV News]] |date=9 December 2010 |accessdate=15 July 2014}}</ref> Consequently, [[Christina Aguilera]] was chosen as the featured guest on the song, which appeared on T.I.'s album ''[[No Mercy (T.I. album)|No Mercy]]''. [[Alex da Kid]], producer of the song, said about the collaboration, "I love it. I think it's amazing. It's my sound, just kind of an evolution of that. I think it'll cater to a lot of different people."<ref name="vh1">{{cite web|url=http://www.vh1.com/news/articles/1652869/20101122/index.jhtml|archiveurl=https://web.archive.org/web/20101208001536/http://www.vh1.com/news/articles/1652869/20101122/index.jhtml|title=T.I., Christina Aguilera Collabo Is 'Amazing,' Alex Da Kid Says|publisher=[[VH1]]|date=22 November 2010|archivedate=8 December 2010|first=James|last=Dinh}}</ref> T.I. told RapFix Live about the track's inspiration:

<blockquote>I live the life that most would die for, but there's a lot of things that come with this life. People don't take that into consideration. There are a lot of things in this life that I would trade in a minute just for a slice of normalcy [...] I just listed a few things that they probably never viewed from that particular perspective. Because I think people need to see things another way; they need to see it other than just celebrity.<ref name="MTV"/></blockquote>

In November 2010, Alex da Kid announced that "Castle Walls" would be released as a [[single (music)|single]] from ''No Mercy''.<ref name="vh1"/><ref name="RapUp"/> However, the song was not released. A 30-second snippet of the song featuring Aguilera's part was released onto [[YouTube]] later that month.<ref>{{cite web|url=http://www.rap-up.com/2010/11/24/new-music-ti-f-christina-aguilera-castle-walls-snippet/|title=New Music: T.I. f/ Christina Aguilera &ndash; 'Castle Walls'|work=Rap-Up|date=24 November 2010|accessdate=15 July 2014}}</ref>

==Composition==
{{Listen
|filename=T.I. - Castle Walls featuring Christina Aguilera (sample).ogg
|title="Castle Walls"
|description=A 30 second sample of "Castle Walls"
|format=[[Ogg]]
|pos=right}}

"Castle Walls" lasts for a duration of 5:29 (five minutes and twenty nine seconds)<ref>{{cite web|url=https://itunes.apple.com/us/album/no-mercy-deluxe-version/id404386339|title=No Mercy (Deluxe Edition)|publisher=[[iTunes Store]] (US)|accessdate=15 July 2014}}</ref> and is a [[hip hop music|hip hop]] and [[electro (music)|electro]] song.<ref name="RapUp">{{cite web|url=http://www.rap-up.com/2010/11/23/rap-up-tv-alex-da-kid-builds-castle-walls-for-ti-and-christina-aguilera/|title=Alex Da Kid Builds Castle Walls for T.I. and Christina Aguilera|work=[[Rap-Up]]|date=23 November 2010|accessdate=15 July 2014}}</ref> The song features a [[Europop]] keyboard in its arrangement.<ref name="Pitchfork">{{cite web|url=http://pitchfork.com/reviews/albums/14923-no-mercy/|title=T.I.: No Mercy|publisher=[[Pitchfork Media]]|first=Tom|last=Breihan|date=10 December 2010|accessdate=15 July 2014}}</ref> Nathan Rabin from ''[[The A.V. Club]]'' wrote that the lyrics of "Castle Walls" "offer similarly incisive and only occasionally self-pitying commentary on the tragedy and triumph of being young, black, rich, famous, and a repeat felon."<ref>{{cite web|url=http://www.avclub.com/review/ti-emno-mercyem-49006|title=T.I.: No Mercy {{*}} Music Review|work=[[The A.V. Club]]|first=Nathan|last=Rabin|date=14 December 2010|accessdate=17 July 2014}}</ref> The song begins with the [[chorus effect|chorus]], in which Aguilera sings, "Everyone thinks that I have it all / But it's so empty living behind these castle walls (These castle walls) / If I should tumble if I should fall/ Would anyone hear me screaming behind these castle walls? / There's no one here at all, behind these castle walls."<ref name="vh1"/><ref name="MuuMuse">{{cite web|url=http://www.muumuse.com/2010/11/t-i-builds-castle-walls-with-christina-aguilera.html/|title=T.I. Builds "Castle Walls" with Christina Aguilera|publisher=[[MuuMuse]]|first=Bradley|last=Stern|accessdate=15 July 2014}}</ref> After the chorus, T.I. raps the verse "See with the Phantoms and Ferraris in the driveway / But you see it came in exchange of a sane man's sanity / Your vision jaded by the Grammys on the mantlepiece / Just switch your camera lenses, you will see the agony" over a "warbling" electronic background and hip hop beats.<ref name="MuuMuse"/><ref name="AOLRadio">{{cite web|last=Cheung|first= Nadine|date=29 November 2012|url=http://www.aolradioblog.com/2010/11/29/t-i-castle-walls-christina-aguilera/|title=T.I., 'Castle Walls' feat. Christina Aguilera - New Song|publisher=[[AOL Radio]]|accessdate=15 July 2014}}</ref> Towards the song's conclusion, there is a "sad" [[string instruments|string]] arrangement and an [[electronica]]-influenced "triumphantly striding beat."<ref name="MuuMuse"/> An editor from [[HipHopDX]] compared "Castle Walls" to "[[Love the Way You Lie]]" by [[Eminem]] featuring [[Rihanna]], which was also produced by da Kid.<ref>{{cite web|url=http://www.hiphopdx.com/index/singles/id.12934/title.ti-f-christina-aguilera-castle-walls-prod-alex-da-kid|title=T.I. f. Christina Aguilera &ndash; Castle Walls [Prod. Alex da Kid]|publisher=[[HipHopDX]]|date=1 December 2010|accessdate=17 July 2014}}</ref>

== Reception ==

[[File:Christina Aguilera at the premiere of Burlesque (2010).jpg|thumb|left|170px|Christina Aguilera ''(pictured in 2010)'' was featured on "Castle Walls"]]

"Castle Walls" received mixed reviews from [[music journalism|music critics]]. Becky Bain from [[Idolator (website)|Idolator]] called "Castle Walls" a "bright spot" for both T.I. and Aguilera "after the not-so-great years both artists have had."<ref>{{cite web|url=http://www.idolator.com/5702242/christina-aguilera-soars-on-t-i-s-castle-walls|title=Christina Aguilera Soars On T.I.'s 'Castle Walls'|publisher=[[Idolator (website)|Idolator]]|first=Becky|last=Bain|date=29 November 2010|accessdate=15 July 2014}}</ref> Steve Jones, an editor from ''[[USA Today]]'', praised Aguilera's appearance on the track and picked it as one of the highlights on ''No Mercy''.<ref>{{cite web|url=http://usatoday30.usatoday.com/life/music/reviews/2010-12-07-listen07_st_N.htm|title=Listen Up: T.I., Daft Punk, Natasha Bedingfield, more|first1=Steve|last1= Jones|first2= Edna|last2= Gundersen|first3= Elysa|last3= Gardner|first4= Brian |last4=Mansfield|work=[[USA Today]]|date=7 December 2010|accessdate=15 July 2014}}</ref> Likewise, Slava Kupersein of HipHopDX and ''[[The Boston Globe]]'''s Ken Capobianco labelled "Castle Walls" a standout from ''No Mercy'' and applauded Aguilera's vocals.<ref>{{cite web|url=http://www.hiphopdx.com/index/album-reviews/id.1592/title.ti-no-mercy|title=T.I. &ndash; No Mercy|publisher=HipHopDX|first=Slava|last=Kuperstein|date=7 December 2010|accessdate=16 July 2014}}</ref><ref>{{cite web|url=http://www.boston.com/ae/music/cd_reviews/articles/2010/12/06/ti_no_mercy/|title=T.I. 'No Mercy' Review|work=[[The Boston Globe]]|date=6 December 2010|accessdate=16 July 2014|first=Ken|last=Capobianco}}</ref> Chase McMullen from [[One Thirty BPM]] thought that the song "epitomizes the album nearly perfectly", but opined that [[Justin Timberlake]] should be the guest vocalist rather than Aguilera.<ref>{{cite web|url=http://onethirtybpm.com/reviews/album-review-t-i-no-mercy/|title=Album Review: T.I. &ndash; No Mercy|publisher=[[One Thirty BPM]]|first=Chase|last=McMullen|date=14 December 2010|accessdate=17 July 2014}}</ref> Leah Greenblatt of ''[[Entertainment Weekly]]'' simply deemed the track "mournful, lonely" and described the chorus on the song as "melancholy".<ref>{{cite web|url=http://music-mix.ew.com/2010/11/30/t-i-castle-walls-christina-aguilera/|title=New T.I. track feat. Christina Aguilera, 'Castle Walls': Hear it here|first=Leah|last=Greenblatt|work=[[Entertainment Weekly]]|date=30 November 2010|accessdate=15 July 2014}}</ref>

David Amidon writing for [[PopMatters]] wrote a mixed review, calling "Castle Walls" "heartfelt [...] but it also feels like a hip-hop version of late-period [[Phil Collins]], a sort of end so inoffensive and far removed from the life force of hip-hop as to appear a sick joke."<ref>{{cite web|url=http://www.popmatters.com/review/135066-t.i.-no-mercy/|title=T.I.: No Mercy &ndash; Review|publisher=[[PopMatters]]|first=David|last=Amidon|date=3 January 2011|accessdate=15 July 2014}}</ref> Critic Jody Rosen from ''[[Rolling Stone]]'' was displeased with the lyrics of the song, deeming it "an icky bit of self-pity from a rich and famous man."<ref>{{cite web|url=http://www.rollingstone.com/music/albumreviews/no-mercy-20101207|title=No Mercy|work=[[Rolling Stone]]|first=Jody|last=Rosen|date=7 December 2010|accessdate=16 July 2014}}</ref> Sean Fennessey of ''[[The Washington Post]]'' named it an "unfortunately regal perspective",<ref>{{cite web|url=http://blog.washingtonpost.com/clicktrack/2010/12/album_review_ti_no_mercy.html|title=Album review: T.I., 'No Mercy'|work=[[The Washington Post]]|first=Sean|last=Fennessey|date=7 December 2010}}</ref> while [[Pitchfork Media]]'s Tom Breihan called it "downright insulting."<ref name="Pitchfork"/> Prefix Magazine's Dave Park described "Castle Walls" as "cloying" and opined that the track bore an "embarrassing resemblance" to [[Britney Spears]]' song "[[Lucky (Britney Spears song)|Lucky]]" (2001).<ref>{{cite web|url=http://www.prefixmag.com/reviews/ti/no-mercy/45294/|title=Album Review: T.I. &ndash; No Mercy|publisher=Prefix Magazine|first=Dave|last=Park|accessdate=16 July 2014}}</ref>

Despite not being released as an official single, "Castle Walls" still managed to enter the [[record chart]]s of several countries. The track was a success in South Korea, where it debuted at number six on the [[Gaon Chart|Gaon International Singles Chart]] during the week of 5 December 2010.<ref name="Gaon"/> In the United States, the song reached number five on the [[Bubbling Under Hot 100 Singles]] chart and number 84 on the [[Hot R&B/Hip-Hop Songs]].<ref name="Bubbling"/><ref name="R&B"/> On the [[Canadian Hot 100]] chart, it peaked at number 99.<ref name="Canada"/> "Castle Walls" also entered the charts of three European countries: number 27 in Czech Republic, number 31 in Slovakia, and number 51 in Sweden.<ref name="Czech"/><ref name="Slovakia"/><ref name="Sweden"/>

== Weekly charts ==

{| class="wikitable sortable plainrowheaders"
|-
! Chart (2010–11)
! Peak<br>position
|-
! scope="row" {{singlechart|Canada|99|artist=T.I.|artistid={{BillboardID|T.I.}}|song=Castle Walls|refname=Canada|accessdate=15 July 2014}}
|-
! scope="row" {{singlechart|Czech Republic|27|year=2011|week=20|accessdate=15 July 2014|refname=Czech}}
|-
! scope="row" | Poland ([[Polish Music Charts|Polish Airplay New]])<ref>{{cite web|url=http://bestsellery.zpav.pl/airplays/pozostale/archiwum.php?year=2011&typ=nowosci&idlisty=294#title|title=Listy bestsellerów, wyróżnienia :: Związek Producentów Audio-Video|publisher=[[Polish Music Charts|Polish Airplay New]]|accessdate=30 March 2017}}</ref>
| align="center" | 5
|-
! scope="row"| Russia ([[Russian Music Charts|Radio TopHit Airplay]])<ref name="Russia">{{cite web|url=http://tophit.ru/cgi-bin/trackinfo.cgi?id=30922|title=Наивысшая позиция трека в Top Hit Weekly General|publisher=[[Russian Music Charts]]|accessdate=14 November 2014}}</ref>
| style="text-align:center"|13
|-
!scope="row"| Slovakia ([[International Federation of the Phonographic Industry|IFPI]])<ref name="Slovakia">{{cite web|url=http://www.ifpicr.cz/hitparadask/index.php?a=titul&hitparada=18&titul=146384&sec=b149017418dcd65f3766b29cf2a61d25|title=SNS IFPI|publisher=[[IFPI]] Slovakia|accessdate=15 July 2014}}</ref>
|align="center"| 31
|-
! scope="row"| South Korea ([[Gaon Chart|Gaon International Singles Chart]])<ref name="Gaon">{{cite web |url=http://gaonchart.co.kr/chart/download.php?f_type=week&f_year=2010&f_month=&f_week=51&f_chart_kind_cd=E|title=South Korean International Singles Chart|publisher =[[Gaon Chart]]|language =Korean|accessdate=15 July 2014}}</ref>
|align="center"| 6
|-
! scope="row" {{singlechart|Sweden|51|artist=T.I.|song=Castle Walls|accessdate=15 July 2014|refname=Sweden}}
|-
! scope="row"|US [[Bubbling Under Hot 100 Singles]] (''[[Billboard (magazine)|Billboard]]'')<ref name="Bubbling">{{cite web|url=http://www.billboard.com/artist/277486/ti/chart?f=344 |title=Nelly chart history: Bubbling Under Hot 100|work=[[Billboard (magazine)|Billboard]]|accessdate=15 July 2014}}</ref>
| style="text-align:center"|5
|-
! scope="row" {{singlechart|Billboardrandbhiphop|84|artist=T.I.|artistid={{BillboardID|T.I.}}|song=Castle Walls|accessdate=15 July 2014|refname=R&B}}
|-
|}

==References==
{{Reflist|2}}

==External links==
* {{MetroLyrics song|ti|castle-walls}}<!-- Licensed lyrics provider -->

{{T.I. singles}}
{{Christina Aguilera songs}}

[[Category:Christina Aguilera songs]]
[[Category:T.I. songs]]
[[Category:Song recordings produced by Alex da Kid]]
[[Category:Songs written by T.I.]]
[[Category:2010 songs]]
[[Category:Songs written by Alex da Kid]]
[[Category:Songs written by Skylar Grey]]