{{for|the English rower|Frank Willan (rower)}}
{{Use dmy dates|date=April 2012}}
{{Infobox military person
|name= Frank Willan
|image= 
|image_size= 
|alt= 
|caption= 
|birth_date= {{birth date|1915|12|21|df=yes}}
|death_date= {{Death date and age|1981|11|12|1915|12|21|df=yes}}
|birth_place= 
|death_place= [[Teffont]], Wiltshire
|placeofburial= 
|nickname= 
|allegiance= United Kingdom
|branch= [[Royal Air Force]]
|serviceyears= 1937–60
|rank= [[Group Captain]]
|unit= 
|commands= [[RAF Feltwell]] (1958–60)<br/>[[Oxford University Air Squadron]] (1951–53)
|battles= [[Second World War]]
|awards= [[Commander of the Order of the British Empire]]<br/>[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]]
|relations= [[Robert Hugh Willan]] (father)<br/>[[Frank Willan (rower)|Frank Willan]] (grandfather)
|laterwork= [[List of chairmen of Wiltshire County Council|Chairman]] of [[Wiltshire County Council]] (1973–79)
}}
[[Group Captain]] '''Frank Andrew Willan''', {{postnominals|country=GBR|size=100%|sep=,|CBE|DFC|DL}} (21 December 1915 – 12 November 1981) was an English aviator, [[Royal Air Force]] officer and [[Conservative Party (UK)|Conservative]] politician. He was [[List of chairmen of Wiltshire County Council|Chairman]] of [[Wiltshire County Council]] from 1973 to 1979.

==Early life==
The son of Brigadier [[Robert Hugh Willan]], and the grandson of Colonel [[Frank Willan (rower)|Frank Willan]] of [[Thornhill, Southampton|Thornehill Park]], [[Bitterne]], [[Hampshire]], Willan was educated at [[West Downs School]], [[Eton College]] and [[Magdalen College, Oxford]].<ref name=who>'WILLAN, Group Captain Frank Andrew' in ''[[Who's Who (UK)|Who Was Who 1981–1990]]'' (A. & C. Black, London)</ref><ref>[http://www.westdowns.com/owd_year.htm Old West Downs] at westdowns.com</ref> His grandfather, after whom he was named, was Colonel of the [[Oxfordshire Light Infantry|3rd Oxfordshire Light Infantry]], an [[alderman]] for Hampshire, a [[Justice of the Peace]] and a [[Deputy Lieutenant]].<ref>[http://www.douglashistory.co.uk/famgen/getperson.php?personID=I9187&tree=main Colonel Frank Willan (1846–1875)] at douglashistory.co.uk</ref>

==Career==
Willan was commissioned into the Royal Air Force as a [[pilot officer]] with effect from 25 October 1937,<ref>''[[London Gazette]]'' Issue 34452, 9 November 1937, p. 6969</ref> and served the [[Second World War]] with [[RAF Bomber Command]], being promoted [[flight lieutenant]] with effect from 25 April 1940.<ref>''London Gazette'' Issue 34844, 7 May 1940, p. 2723</ref> After the war, he was Commanding Officer of the [[Oxford University Air Squadron]] (1951–53) and of [[RAF Feltwell]] (1958–60), before retiring the service in 1960.<ref name=who/>

Willan's brother Martin Stuart Willan was [[killed in action]] in northern France on 25 May 1940 at the age of 21, while serving with the [[King's Royal Rifle Corps]] as part of the [[British Expeditionary Force (World War II)|British Expeditionary Force]].<ref>[http://www.wakefieldfhs.org.uk/genealogyjunction/Eton/Eton%20Roll%20of%20Honour%208%201939-1945.html Eton College Rolls of Honour] at wakefieldfhs.org.uk</ref>

On 11 October 1945, in the chapel of [[New College, Oxford]], Willan married Joan, a daughter of Mr Leopold Wickham Legg, a fellow of the college, of [[Boar's Hill]], and a granddaughter of the ecclesiologist [[John Wickham Legg]].<ref>''The Aeroplane'', vol. 69 (1945), p. 494</ref>

Elected a member of [[Wiltshire County Council]] in 1961, he continued in that until his death in 1981. Willan was Chairman of the County Council's Education Committee from 1965 to 1968; Vice-Chairman of Council, 1968 to 1969; and [[List of chairmen of Wiltshire County Council|Chairman]], 1973 to 1979.<ref name=who/> Between 1974 and 1981 he was also Chairman of the [[Wiltshire Victoria County History]] and was succeeded in both positions by [[Nigel Anderson]].<ref>From 'Editorial note', in ''A History of the County of Wiltshire: Volume 12'' (1983), p. XV: "Group Captain F. A. Willan, C.B.E., D.F.C., D.L., died in November 1981 shortly after being succeeded as Chairman of the Wiltshire Victoria County History Committee by Mr. N. J. M. Anderson, M.C., D.L., who had earlier succeeded him as Chairman of the County Council."</ref>

Willan was appointed a [[Commander of the Order of the British Empire]] (Military Division) in the [[New Year Honours]] for 1961,<ref>''London Gazette'' (Supplement) 31 December 1960, [http://www.london-gazette.co.uk/issues/42231/supplements/8896/page.pdf p. 8896] online</ref> and a [[Deputy Lieutenant]] for Wiltshire in 1968.<ref>''London Gazette'' Issue 44520, 6 February 1968, [http://www.london-gazette.co.uk/issues/44520/pages/1489 p. 1489] online</ref> He was also a Member of the Wessex Regional Hospital Board 1970–1974, Chairman of the [[Salisbury Diocese|Salisbury Diocesan Board of Finance]], 1970–1981, and Chairman of the Joint Advisory Committee on Local Authorities’ Purchasing, 1970–77.<ref name=who/>

He died on 12 November 1981, when he was of Bridges, [[Teffont]], Wiltshire, and left an estate valued at £258,491.<ref>[https://probatesearch.service.gov.uk probatesearch.service.gov.uk]</ref>

==References==
{{reflist|30em}}

{{DEFAULTSORT:Willan, Frank Andrew}}
[[Category:1915 births]]
[[Category:1981 deaths]]
[[Category:Alumni of Magdalen College, Oxford]]
[[Category:Commanders of the Order of the British Empire]]
[[Category:Recipients of the Distinguished Flying Cross (United Kingdom)]]
[[Category:Royal Air Force officers]]
[[Category:Royal Air Force personnel of World War II]]
[[Category:Deputy Lieutenants of Wiltshire]]
[[Category:Members of Wiltshire County Council]]
[[Category:People educated at Eton College]]
[[Category:People educated at West Downs School]]
[[Category:Conservative Party (UK) politicians]]