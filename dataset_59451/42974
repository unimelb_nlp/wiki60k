<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Bi-mono
 | image= Hillson Bi-mono 3view.svg
 | caption=3-way drawing
}}{{Infobox Aircraft Type
 | type=[[Experimental aircraft]]
 | national origin=[[United Kingdom]]
 | manufacturer=[[F Hills & Sons]]
 | designer=
 | first flight=1941
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=1
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Hillson Bi-mono''' was a [[United Kingdom|British]] [[experimental aircraft]] of the 1940s. It was designed to test the idea of "slip-wings", where the aircraft could take off as a [[biplane]], jettison the upper, disposable wing, and continue flying as a [[monoplane]]. A single example was built, which successfully demonstrated jettisoning of the slip wing in flight.

==Design and development==

In the 1930s, as take-off weights of aircraft continued to increase, designers grew increasingly concerned about the effects that these weights would have on take-off runs, and several designers investigated the concept of a "slip-wing", which could be jettisoned after take-off. Amongst the proponents of the "slip-wing" was [[Noel Pemberton Billing]], the founder of [[Supermarine]], who wrote several articles in the aviation press promoting the idea, either with a manned, reusable auxiliary wing, or a disposable or "scrap-wing".<ref name="Jarrett p204">Jarrett ''Aeroplane Monthly'' April 1990, p. 204.</ref><ref name="PB p3-7">Jarrett ''Air Enthusiast'' August to October 1993, pp. 3–7.</ref> [[Blackburn Aircraft]] also investigated "slip-wings".<ref name="Ellis p49">Ellis ''Air Enthusiast'' September/October 2003, p. 49.</ref>

Following the outbreak of the [[Second World War]], [[F Hills and Son]], a light aircraft manufacturer based at [[Trafford Park]], [[Manchester]] offered a design for a light fighter aircraft to the British [[Air Ministry]]. The fighter would be cheap to build and could be operated from small fields or open roads. To give the required take-off performance, the design was to be fitted with a disposable "slip-wing". While the proposal was not accepted by the Air Ministry, Hills and Sons decided to continue with the project as a private venture, and so built a scale test-bed, to prove the slip-wing process.<ref name="Jarrett p204"/><ref name="Ellis p49"/>

The test bed, known as the Bi-mono, was a small [[tractor configuration|tractor]] [[monoplane]] with a fabric-covered steel-tube construction fuselage and a wooden wing. It had a fixed [[conventional landing gear|tailwheel undercarriage]] while an enclosed cockpit was provided for the pilot. The auxiliary wing was attached to the top of the cockpit canopy and to the lower wing by [[interplane struts]]. Two different upper wings were designed with different wingspans, the shorter wing having the same span as the lower wing. The aircraft was powered by a single [[de Havilland Gipsy Six]] engine.<ref name="Jarrett p205">Jarrett ''Aeroplane Monthly'' April 1990, p. 205.</ref><ref name="Ellis p50">Ellis ''Air Enthusiast'' September/October 2003, p. 50.</ref>

The Bi-mono was not the only slip-wing project built by Hills and Sons, as they were also contracted by Pemberton Billing to build his PB.37 design for a slip-wing dive-bomber, with a [[pusher configuration|pusher]] monoplane lower component powered by a 290&nbsp;hp (216&nbsp;kW) engine, while the manned slip-wing upper component was a tractor monoplane powered by a 40&nbsp;hp (30&nbsp;kW) engine. Construction work started on the PB.37 early in 1940, but work was abandoned in July 1940 when construction was almost finished but the aircraft was unflown.<ref name="PB p3-4">Jarrett ''Air Enthusiast'' August to October 1993, pp. 3–4.</ref>

==Operational history==

The Bi-mono, which carried no [[United Kingdom military aircraft serials|serial number]] or [[United Kingdom aircraft registration|civil registration]], made its maiden flight from [[Barton Aerodrome]] during 1941.<ref name="Ellis p51">Ellis ''Air Enthusiast'' September/October 2003, p. 51.</ref> Test flights were made both as a monoplane and as a biplane, with the shorter upper wing being chosen.<ref name="Jarrett p205"/>  In order to avoid the potential hazards to people on the ground of dropping the wing, wing jettisoning tests were carried out from [[Squires Gate Airport]], [[Blackpool]], with the upper wing being successfully dropped over the [[Irish Sea]] on 16 July 1941.<ref name="Ellis p50"/> The test proved successful, with no great change in [[trim (aircraft)|trim]] and a few hundred feet in altitude being lost when the upper wing was jettisoned.<ref name="Jarrett p206">Jarrett ''Aeroplane Monthly'' April 1990, p. 206.</ref>

The Bi-mono was subject to further testing by the [[Aeroplane and Armament Experimental Establishment]] at [[MoD Boscombe Down|Boscombe Down]] from October 1941. The A&AEE found that the maximum speed of the biplane configuration was slower than the stalling speed of the monoplane configuration. Its landing characteristics were likened to a [[kangaroo]].<ref>Mason ''The Secret Years: Flight testing at Boscombe Down'' p209</ref>

Hills and Sons went on to further develop the slip-wing concept, flying the Hillson FH.40, a [[Hawker Hurricane]] fitted with a slip-wing.<ref name="Ellis p50-1">Ellis ''Air Enthusiast'' September/October 2003, pp. 50–51.</ref>

==Specifications (short upper wing)==
{{Aircraft specs
|ref=Nothing Ventured...Part 1"<ref name="Jarrett p205-6">Jarrett ''Aeroplane Monthly'' April 1992, pp. 205–206.</ref>
|prime units?=imp
<!--
        General characteristics
-->
|genhide=

|crew=1
|capacity=
|length m=
|length ft=19
|length in=6
|length note=
|span m=
|span ft=20
|span in=0
|span note=
|height m=
|height ft=7
|height in=0
|height note= as biplane - 6 ft 4 in (1.93 m) as monoplane
|wing area sqm=
|wing area sqft=132
|wing area note= as biplane - 66 sq ft (6.13 m²) as monoplane
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=Lower wing RAF 34, Upper wing Clark Y
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=1940
|gross weight note=(biplane)
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[de Havilland Gipsy Six]]
|eng1 type=air-cooled six-cylinder [[inline engine (aviation)|inline engine]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=200<!-- prop engines -->
|eng1 note=

<!--
        Performance
-->
|perfhide=Y

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=

|power/mass=
|thrust/weight=

|more performance=
<!--
        Armament
-->
|armament=<!-- add (bulleted list) here or if you want to use the following specific parameters, remove this parameter-->
|guns= 
|bombs= 
|rockets= 
|missiles=

|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|other armament=

|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{Commons category|Hillson Bi-mono}}
{{refbegin}}
* Ellis, Ken. "Back to the Biplane: The 'Slip Wing' and the Hurricane". ''[[Air Enthusiast]]'', No 107, September/October 2003. {{ISSN|0143-5450}}. pp.&nbsp;47–51.
* Jarrett, Philip. "Nothing Ventured...: Part 1". ''[[Aeroplane Monthly]]'', April 1990, Vol 18 No 4. {{ISSN|0143-7240}}. pp.&nbsp;204–206.
* Jarrett, Philip. "PB's Dream Machines". ''Air Enthusiast'', Fifty-one, August to October 1993. {{ISSN|0143-5450}}. pp.&nbsp;1–7.

<!-- insert the reference sources here -->
{{refend}}
<!-- ==Further reading== -->
<!-- ==External links== -->

<!-- Navboxes go here -->

[[Category:British experimental aircraft 1940–1949]]