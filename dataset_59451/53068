'''''Tyr: Myth—Culture—Tradition''''' is the name of an American [[Traditionalist School|Radical Traditionalist]] ([[anti-modern]], [[neo-tribalist]]<ref>[http://tyrjournal.tripod.com/about_the_journal.htm "About the Journal"]: "['Radical Traditionalism'] means to reject the modern, materialist reign of 'quantity over quality,' the absence of any meaningful spiritual values, environmental devastation, the mechanization and over-specialization of urban life, and the imperialism of corporate mono-culture, with its vulgar 'values' of progress and efficiency.  It means to yearn for the small, homogeneous tribal societies that flourished before Christianity &mdash; societies in which every aspect of life was integrated into a holistic system."</ref>) journal, edited by [[Joshua Buckley]], [[Michael Moynihan (journalist)|Michael Moynihan]], and (in the first issue) [[Collin Cleary]].  

It is an annual publication named after [[Tyr]], the [[Germanic paganism|Germanic]] god. The magazine states that it "celebrates the traditional myths, culture, and social institutions of pre-Christian, pre-modern Europe."  The first issue was published in 2002 under the [[Ultra Publishing|ULTRA]] imprint in [[Atlanta, Georgia]]. The magazine largely focuses on topics relating to [[Germanic neopaganism]] and [[Germanic paganism]] with an amount of content regarding [[Celtic polytheism]] as well.

Three volumes have appeared so far; vol. 1 in 2002 and vol. 2 in 2004 and now vol. 3 2006 is available from the Tyr website or from Norway's Integral Publications. Contributors include [[Asatru Folk Assembly]] founder [[Stephen McNallen]], ''[[Nouvelle Droite]]'' leader [[Alain de Benoist]], an interview with noted French comparative [[philologist]] [[Georges Dumézil]], British musicologist and translator [[Joscelyn Godwin]], modern [[Germanic mysticist]] [[Nigel Pennick]] and scholar [[Stephen Flowers]], besides translations of texts by "[[Traditionalist School|Traditionalist]]" author and occultist [[Julius Evola]] and [[völkisch]] poet and musician [[Hermann Löns]]. Volume 2 also includes a CD of music related to the subject matter or authors contributing.

==Reviews==
[[Thomas Wiloch]] states that:
<blockquote>"Tyr serves as a meeting place for those who see intriguing commonalities between the environmental, pagan, alternative music, and occult communities, and between certain political ideas of both the left and the right,"
further stating that the publication is "on the extreme edge of things".<ref>Review of ''Tyr'' #1 by [[Thomas Wiloch]] for Flux Europa webzine: [http://www.fluxeuropa.com/tyr.htm]</ref></blockquote>

The reviewer for [[Northvegr]] identifies the philosophy behind ''Tyr'' as primarily "[[Odianism|Odian]]" ([[Stephen Flowers]]' school of occult "Runosophy"), expressing concern that the magazine:
<blockquote>"..wraps into a round of praise and admiration for the likes of [[Julius Evola]], [[Hermann Löns|Herman Lons]], and the dark master of chaos himself, [[Karl Maria Wiligut]]."</blockquote>
Northvegr then requests  "firm voices calling out from the side of right and order" to correct the impression that the occultist "Traditionalism" advocated by ''Tyr'' represents a mainstream position in [[Germanic neopaganism]].<ref>Review of ''Tyr'' issue #1 by Ári Óðinssen for [[Northvegr]]. Online: [http://www.northvegr.org/reviews/tyr.php]</ref>

The reviewer for ''[[Willamette Week]]'' identifies ''Tyr'' as a journal of "neo-pagan crypto-scholarship" (but does not elaborate further or give example) and an "artifact of modern Bohemia" aiming at the "creation of an alternative intellectual reality", and states further that:
<blockquote>"...a section of this issue's preface attempts to dismiss 'The Fascist Accusation' before the fact."<ref>Review of ''Tyr'' by Zach Dundas for ''[[Willamette Week]]'', May 12, 2004. Online: [http://www.wweek.com/story.php?story=5087]</ref></blockquote>

The "attempt" the Willamette Week refers to is the preface to volume 2. It reads:
<blockquote>"Watching the impressive spectacle of thousands of black-clad storm troopers marching in lock-step formation, one is reminded of nothing so much as the regimentation of modern, industrial society. The Nazi's overarching emphasis on biological materialism, and the idea that human perfectibility could be achieved through eugenics, is mirrored in the modern obsession (albeit purged of the focus on "racial purity") with cloning, genetic engineering, and mood-controlling pharmaceuticals. That any of these unnatural and frightening panaceas could genuinely reverse the soul-sickness of our age seems highly unlikely. They will only make things worse."</blockquote>

Michael Strmiska, faculty member (Global Studies) at SUNY Orange, wrote an overall review of all three issues of ''Tyr'' for the Pagan Studies journal [[The_Pomegranate_(journal) | ''The Pomegranate'']] in 2010. Strmiska's review begins as follows:<blockquote>''Tyr'' is a unique, largely but not exclusively Pagan-oriented publication that is in fact rather difficult to categorize. It has much to recommend it, from scholarly articles on ancient European Pagan religion and mythology, thoughtful reflections on Ásatrú and other Pagan revival movements of the last several decades and their roots in earlier cultural movements such as Romanticism, interviews with leading figures in contemporary Paganism, philosophers and musicians, and music and book reviews.<ref>Review of ''Tyr: Myth-Culture-Tradition'', by Michael Strmiska, [[The_Pomegranate_(journal) | ''The Pomegranate'']] vol. 12, n. 1, 2010, p. 118</ref></blockquote>Strmiska also addresses what he describes as "certain more controversial political aspects"<ref>Strmiska 2010:118</ref> of ''Tyr'' in his review, and, in particular, he states that "''Tyr'' is by no means a neo-Nazi or pro-Fascist publication ... The rich and varied contents of the journal are a still more convincing demonstration that ''Tyr'' is not a bullhorn for a right-wing, ethno-nationalist agenda, let alone a Nazi call to arms, but an investigation of Pagan European, primarily Norse-Germanic spiritual and cultural traditions."<ref>Strmiska 2010:120</ref>

==Publication data==
*TYR Myth, Culture, Tradition Vol. 1, Ultra (2002), ISBN 978-0-9720292-0-9.
*TYR Myth, Culture, Tradition Vol. 2, Ultra (2004), ISBN 978-0-9720292-1-6.
*TYR Myth, Culture, Tradition Vol. 3, Ultra (2006), ISBN 978-0-9720292-3-0.
*TYR Myth, Culture, Tradition Vol. 4, Ultra (2014), ISBN 978-0-9720292-4-7
==See also==
*[[Traditionalist School]]
*[[Development criticism]]

==References==
{{reflist|2}}

==External links==
* [http://www.radicaltraditionalist.com/ ''Tyr'' official site]

{{DEFAULTSORT:Tyr (Journal)}}
[[Category:Cultural magazines]]
[[Category:Traditionalist School]]
[[Category:Fascist newspapers and magazines]]
[[Category:English-language magazines]]
[[Category:Germanic mysticism]]
[[Category:Magazines established in 2002]]
[[Category:American political magazines]]
[[Category:Neopagan texts]]
[[Category:2002 establishments in the United States]]