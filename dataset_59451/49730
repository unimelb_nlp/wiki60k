{{italic title}}
'''''The Lustful Turk, or Lascivious Scenes from a Harem''''' is a pre-[[Victorian literature|Victorian]] British [[erotic novel|erotic]] [[epistolary novel]] first published anonymously in 1828 by John Benjamin Brookes and reprinted by [[William Dugdale (publisher)|William Dugdale]]. However, it was not widely known or circulated until the 1893 edition.

==Plot==

The novel consists largely of a series of letters written by its heroine, Emily Barlow, to her friend, Sylvia Carey. When Emily sails from England for India in June 1814 her ship is attacked by [[Moorish pirates]] and she is taken to the [[harem]] of Ali, [[dey]] of [[Algiers]]. Ali [[rape]]s her and subjects her to his will, awakening her sexual passions. Emily's debasement continues when Ali insists on [[anal sex]], arousing the horror of her correspondent Sylvia, who expresses her indignation at Ali's behaviour, in a letter that the latter intercepts. Annoyed at her attitude, Ali arranges for Sylvia to be abducted and brought to the slave market of Algiers. After an elaborate charade in which Ali pretends to be a sympathetic Frenchman, bidding to save her from sexual slavery, and engaging her in a fake marriage, he deflowers her and awakens her sexuality, as he had done with Emily. Revealing his true identity Ali enjoys both girls together. This sexual idyll is eventually terminated when a new addition to the harem objects to anal rape and [[penectomy|cuts off the Dey's penis]] with a knife, and then commits [[suicide]].<ref>Patrick J. Kearney, "A history of erotic literature", Parragon, 1982, ISBN 1-85813-198-7, p.107</ref> Seemingly unfazed by this, Ali has "his lost members preserved in spirits of wine in glass vases" which he presents to Emily and Sylvia, sending them back to England with these tokens of his affection.<ref>Marcus (2008) p.203</ref>

The novel also incorporates interpolated stories concerning the erotic misadventures of three other girls abducted into the harem and enlarges on the fate of Emily's maid Eliza who, presented by Ali to Muzra, [[bey]] of Tunis, is bound, flogged and raped in turn.<ref>Marcus (2008) p.201</ref>

The book was one of those condemned as obscene by [[John Campbell, 1st Baron Campbell|Lord Chief Justice Campbell]] when Dugdale was prosecuted in 1857.<ref name=sova150>Sova (2006) p.150</ref>

==Influences==

''The Lustful Turk'' uses the contemporary conventions of the [[novel of sensibility]] and [[Gothic romance]] and its exotic Oriental themes are influenced by the life, adventures and writings of [[Lord Byron]].<ref>Marcus (2008) pp.209-210</ref> It was influential on many other works of erotica, and the theme of the virgin who is forcibly introduced to sexual acts and later becomes insatiable in her appetite for the carnal is common in later erotica.<ref>[[Alan Norman Bold]], "The Sexual dimension in literature", Vision Press, 1983, ISBN 0-389-20314-9, pp.97-99</ref> Such works include ''[[The Way of a Man with a Maid]]'', a classic work of Victorian erotica concerning the forcible seduction of a girl called Alice by a Victorian gentleman; ''May's Account of Her Introduction to the Art of Love'', first published in the Victorian erotic periodical ''[[The Pearl (magazine)|The Pearl]]'' and ''[[The Sheik (novel)|The Sheik]]'' written by [[Edith Maude Hull]], published in 1921.<ref>{{cite book |last=Lutz |first=Deborah |title=The Dangerous Lover: Gothic Villains, Byronism, and the Nineteenth-Century Seduction Narrative |location=Columbus |publisher=Ohio State University Press |year=2006}}</ref>

==Adaptations==

A film adaptation of ''The Lustful Turk'' was directed in 1968 by [[Byron Mabe]] with the screenplay written by [[David F. Friedman|David Friedman]] and starring [[Abbe Rentz]], [[Linda Stiles]] and [[Gee Gentell]].<ref>[http://uk.rottentomatoes.com/m/lustful_turk/  Rotten Tomatoes: The Lustful Turk]</ref><ref>[http://www.imdb.com/title/tt0063252/ The Lustful Turk (1968) - IMBd]</ref>

In the mystery novel ''Die For Love'' by [[Barbara Mertz]] (under the pseudonym Elizabeth Peters), plagiarism of ''The Lustful Turk'' is a minor plot point.<ref>{{cite book |last=Peters |first=Elizabeth |title=Die For Love |location=New York |publisher=Tor Books |year=1987}}</ref>

==Oriental setting==

Whereas [[Steven Marcus]] employed ''The Lustful Turk'' in his construction of the placeless realm of [[pornotopia]] in Victorian erotica, later writers have stressed the importance of the [[Orientalism|orientalist]] setting in generating a further sexualised charge<ref>I. C. Schick, ''The Erotic Margin'' (1999) p. 52-3</ref>—the harem as a sort of erotic finishing-school.<ref>R. B. Yeazell, ''Hareems of the Mind'' (2000) p. 118</ref>

==See also==
{{portalbar|United Kingdom|Pornography|Novels}}
* ''[[A Night in a Moorish Harem]]''

==Footnotes==
{{reflist|30em}}

==References==
* Anon, ''The Lustful Turk'' (illustrated). Wordsworth Classic Erotica. 1997
* [[Gaétan Brulotte]], John Phillips, ''Encyclopedia of Erotic Literature'', CRC Press, 2006, ISBN 1-57958-441-1, p.&nbsp;841.
* Steven Marcus, ''The Other Victorians: a study of sexuality and pornography in mid-nineteenth-Century England'', Transaction Publishers, 2008, ISBN 1-4128-0819-7, pp.&nbsp;195–217.
* {{cite book | title=Literature Suppressed on Sexual Grounds | first=Dawn B. | last=Sova | publisher=Infobase Publishing | year=2006 | isbn=0-8160-6272-2 | pages=148–150 }}

{{DEFAULTSORT:Lustful Turk, The}}
[[Category:1828 novels]]
[[Category:British erotic novels]]
[[Category:Works published anonymously]]
[[Category:British novels adapted into films]]
[[Category:Rape in fiction]]
[[Category:Suicide in fiction]]
[[Category:Epistolary novels]]
[[Category:Novels set in Algeria]]
[[Category:BDSM literature]]
[[Category:19th-century British novels]]