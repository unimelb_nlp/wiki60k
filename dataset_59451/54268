{{Use dmy dates|date=November 2016}}
{{Use Australian English|date=November 2016}}
[[File:Lawrence Grayson.jpeg|thumb|right]]

'''Lawrence<ref>Often, and perhaps correctly, spelled "Laurence". Earliest newspaper references were so spelled, as were the few references to his eldest son Laurence William.</ref> Grayson''' (c. 1839 – 7 October 1916) was a mechanical engineer and politician in colonial [[South Australia]].

Grayson was born in [[Chorlton-upon-Medlock]], [[Manchester]], England, was educated at the Manchester Academy, and served his apprenticeship as an engineer with Sir [[William Fairbairn]]. In 1859 he emigrated to South Australia in the ''Lady Anne'', acting as schoolmaster during the voyage. He gained employment with the [[South Australian Railways]]' [[Islington Railway Workshops|Islington workshops]], where he remained for 21 years before joining the Union Engineering Company as manager, then managing director, a position he held until shortly before his death.<ref name=death/>

He was elected to the seat of [[Electoral district of West Adelaide|West Adelaide]] in the [[South Australian House of Assembly]] and served from March 1887 to April 1893. In 1892 he was appointed Commissioner of Public Works in the Downer ministry, during which time he recommended implementation of the Happy Valley water project. He was a strong advocate of the principle of private tendering for Government work, on the grounds of economy and promotion of private enterprise.<ref name=death>{{cite news |url=http://nla.gov.au/nla.news-article59916800 |title=Death of Mr. L. Grayson |newspaper=[[South Australian Register|The Register (Adelaide, SA : 1901 - 1929)]] |location=Adelaide, SA |date=9 October 1916 |accessdate=9 September 2015 |page=4 |publisher=National Library of Australia}}</ref>

He was a founding member of the SA Chamber of Manufactures, and from 1903 to 1905 its President.<ref name=TBSA>Cumming, D. A. and Moxham, G. ''They Built South Australia'' Published by the authors February 1986. ISBN 0 9589111 0 X This reference favors "Laurence".</ref> He was President and Secretary of the Amalgamated Society of Engineers. He was a pioneer of the eight-hour day principle, which was first implemented by the South Australian Railways construction department, and in founding the Eight Hours' Day celebrations in 1874.
He was a longtime member of the Board of Governors of the [[South Australian Museum|Public Library, Museum, and Art Gallery]], and their Vice-President for many years. He was an active member of the [[UniSA|School of Mines]] council and promoted the technical education of boys.<ref name=death/>

He died at his home "Droylsden", Commercial Road, [[Hyde Park, South Australia|Hyde Park]] after suffering an internal complaint for several years, and undergoing an operation at "Parkwynd" private hospital. Obituaries spoke of his uprightness of character, uncompromising honesty and modesty, despite his considerable achievements.<ref name=death/>

==Family==
He married Mary Ann Shakespeare (c. 1841 – November 1917), a sister of organist [[James Shakespeare]], on 3 December 1863. Their children included:
*Alice Martha Grayson (9 June 1864 – ) married Robert Hogarth on 20 September 1894
*Laurence William Grayson (5 April 1866 – 22 January 1930) married Helena May "Lena" Stockdale on 25 April 1894. A mining engineer, he was manager of the Crystal Salt Company, Port Augusta, and pioneered the production of salt in SA by solar evaporation.<ref name=TBSA/> He patented a rowing machine.
*Alfred James "Alf" Grayson PCIA (16 April 1868 – 6 May 1936) married Margaret Homan Hammill on 12 February 1909, an insurance inspector, he lived in Adelaide, a noted rower and footballer for [[Norwood Football Club|Norwood]].
*Walter Thomas Grayson (c. April 1871 – ) was engineer at the Railways workshops, Islington, served in WWI.<ref>{{cite web|url=https://rslvirtualwarmemorial.org.au/explore/memorials/1092|title=Virtual War Memorial|publisher=RSL|accessdate=10 September 2015}}</ref><!--deserted his wife?-->
*Charles Edwin Grayson ( – ) married Minnie G. McQuillan on 15 September 1903. He was an electrician with the Great Boulder Proprietary mine, Western Australia.<!--perhaps the son born 14 October 1874--><!--left WA 1912-->
*Maud Grayson (8 February 1876 – ) married Allen Crisp Edwards on 18 April 1901
*May Elsie Grayson (21 September 1980 – ) married Herbert James Cadd on 11 April 1916, lived in Adelaide

== References ==
{{Reflist}}

{{s-start}}
{{s-off}}
{{s-bef | before= [[Andrew Dods Handyside|Andrew Handyside]] }}
{{s-ttl | title=[[Commissioner of Public Works (South Australia)|Commissioner of Public Works]] | years= 1892&ndash;1893 }}
{{s-vac|next=[[Frederick Holder]]}}
{{s-end}}

{{DEFAULTSORT:Grayson, Laurence}}
[[Category:Members of the South Australian House of Assembly]]
[[Category:Australian mechanical engineers]]
[[Category:1839 births]]
[[Category:1916 deaths]]