<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name= CF-104 Starfighter
 |image= File:CF-104 Starfighters of 417 Sqn in flight near Cold Lake 1976.jpg
 |caption=CF-104s of 417 Squadron near Cold Lake in 1976
}}{{Infobox Aircraft Type
 |type=[[Interceptor aircraft]], [[Fighter-bomber]]
 |manufacturer=[[Canadair]]
 |designer=[[Lockheed Corporation]]
 |first flight= 26 May 1961
 |introduced=March 1962
 |retired= 1995 [[Turkish Air Force]]<ref>[http://www.worldairforces.com/Countries/turkey/tur.html "Historical Listings: Turkey, (TUR)]."] ''World Air Forces.'' Retrieved: 10 June 2011.</ref>
 |status=
 |primary user= [[Royal Canadian Air Force]]<!--please list only one-->
 |more users= [[Canadian Armed Forces]]<!--up to three more. please separate with <br/>.-->
 |produced=<!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
 |number built= 200
 |unit cost= 
 |developed from = [[Lockheed F-104 Starfighter]]
 |variants with their own articles=
}}
|}

The '''Canadair CF-104 Starfighter''' ('''CF-111''', '''CL-90''') was a modified version of the [[Lockheed F-104 Starfighter]] [[supersonic]] [[fighter aircraft]] built in [[Canada]] by [[Canadair]] under licence. It was primarily used as a ground attack aircraft, despite being designed as an interceptor. It served with the [[Royal Canadian Air Force]] (RCAF) and later the [[Canadian Armed Forces]] (CAF) until it was replaced by the [[McDonnell Douglas CF-18 Hornet]].

==Design and development==
In the late 1950s, Canada redefined its role in the [[North Atlantic Treaty Organization]] (NATO) with a commitment to a [[nuclear weapon|nuclear]] strike mission.<ref name="CdnWings">{{cite web|url = http://www.canadianwings.com/Aircraft/aircraftDetail.php?STARFIGHTER-13|title = Canadair CF-104 Starfighter|accessdate = 7 January 2014|last = Canadian Wings|year = 2012}}</ref><ref name="CWHM">{{cite web|url = http://www.warplane.com/vintage-aircraft-collection/aircraft-history.aspx?aircraftId=53|title = Lockheed CF-104D Starfighter|accessdate = 7 January 2014|last = [[Canadian Warplane Heritage Museum]]|year = 2014}}</ref> At the same time, the RCAF began to consider a replacement for the [[Canadair Sabre|Canadair F-86 Sabre]] series that had been utilized as a NATO [[day fighter]].<ref name="Bashow p. 8">Bashow 1990, p. 8.</ref> An international fighter competition involved current types in service as well as development, including the [[Blackburn Buccaneer]], [[Dassault Mirage III]]C, [[Fiat G.91]], [[Grumman F11F-1F Super Tiger|Grumman Super Tiger]], [[Lockheed F-104 Starfighter|Lockheed F-104G Starfighter]], [[Northrop F-5|Northrop N-156]] and the [[Republic F-105 Thunderchief]].<ref name="Bashow p. 8"/> Although the RCAF had preferred the F-105 Thunderchief equipped with an [[Avro Canada]] [[Orenda Iroquois]] engine, eventually the choice for a strike-reconnaissance aircraft revolved around cost as well as capability.<ref>McIntyre 1985, p. 6.</ref> {{#tag:ref|The McDonnell F-4 was never considered although many sources have listed it as a contender and the RCAF's preferred choice.|group=N}}

A Canadian government requirement for a license manufacture also favoured the Lockheed proposal due to a collaboration with Canadair based in [[Montreal]]. On 14 August 1959, Canadair was selected to manufacture 200 aircraft for the RCAF under license from Lockheed. In addition, Canadair was contracted to manufacture  wingsets, tail assemblies and rear fuselage sections for 66 Lockheed-built F-104Gs destined for the West [[German Air Force]].<ref>Baugher, Joe. [http://www.joebaugher.com/usaf_fighters/f104_27.html "Canadair CF-104 Starfighter."] ''American Military Aircraft,'' 6 October 2003. Retrieved: 1 May 2011.</ref>{{#tag:ref|Canadair eventually built a total of 600 wing, tail and fuselage sections.<ref name="Pickler p. 186"/>|group=N}}

Canadair's internal designation was CL-90 while the RCAF's version was initially designated CF-111, then changed to CF-104. Although basically similar to the F-104G, the CF-104 was optimized for the nuclear strike/reconnaissance role, fitted with R-24A NASARR equipment dedicated to the air-to-ground mode only as well as having provision for a ventral reconnaissance pod equipped with four Vinten cameras. Other differences included retaining the removable refuelling probe, initial deletion of the fuselage-mounted 20&nbsp;mm (.79&nbsp;in) M61A1 cannon (replaced by an additional fuel cell) and the main undercarriage members being fitted with longer-stroke liquid springs and larger tires. The first flight of a Canadian-built CF-104 (s/n 12701) occurred on 26 May 1961.<ref>Stachiw 2007, p. 30.</ref> The Canadair CF-104 production was 200 aircraft with an additional 140 F-104Gs produced for Lockheed.<ref name="Pickler p. 186">Pickler and Milberry 1990, p. 186.</ref>

==Operational history==
[[File:CF-104Starfighter01A.JPG|thumb|right|[[No. 417 Squadron RCAF|417 Sqn]] CF-104 at [[CFB Moose Jaw]] in 1982]]
The CF-104 entered Canadian service in March 1962. Originally designed as a supersonic [[interceptor aircraft]], it was used primarily for low-level strike and reconnaissance by the RCAF. Eight CF-104 squadrons were originally stationed in Europe as part of Canada's NATO commitment. This was reduced to six in 1967, with a further reduction to three squadrons in 1970.<ref>Greenhous and Halliday 1999, p. 152.</ref> Up to 1971, this included a nuclear strike role that would see Canadian aircraft [[Nuclear sharing|armed with US-supplied nuclear weapons]] in the event of a conflict with [[Warsaw Pact]] forces. During its service life the CF-104 carried the [[B28 nuclear bomb|B28]], [[B43 nuclear bomb|B43]] and [[B57 nuclear bomb|B57]] nuclear weapons.<ref name=canada-nw>{{cite book|url=https://books.google.com/?id=5-R7EJ0r680C|title=Canadian Nuclear Weapons: The Untold Story of Canada's Cold War Arsenal|pages=91-116|author=John Clearwater|year=1998|publisher=Dundurn Press|isbn=1-55002-299-7|accessdate=19 December 2016}}</ref>

When the CAF later discontinued the strike/reconnaissance role for conventional attack, the M61A1 was refitted, along with U.S. Snakeye "iron" bombs, British [[BL755]] cluster bombs and Canadian-designed [[CRV-7]] rocket pods. Although Canadian pilots practised air combat tactics, [[AIM-9 Sidewinder]] missiles were never carried operationally by Canadian Starfighters (however, examples provided to other air forces, such as Norway and Denmark, did carry Sidewinders on a twin-rail centreline station and the wingtip rails). The CF-104D two-seater did not normally carry any armament except for a centreline practice-bomb dispenser.

There were 110 class A accidents in the 25 years that Canada operated the CF-104 resulting in 37 pilot fatalities. Most of these were in the early part of the program centering on teething problems. Of the 110 class A accidents 21 were attributed to foreign object damage (14 of which were birds), 14 were in flight engine failures, 6 were faulty maintenance, 9 were mid air collisions. 32 struck the ground flying at low level in poor weather conditions. Of the 37 fatalities 4 were clearly attributable to systems failures, all of the others were attributable to some form of pilot inattention.<ref name="autogenerated92"/>

The accident rate of the 104 compares favourably to its predecessor, the F-86 Sabre. In only 12 years of operation the F-86 had 282 class A accidents with a loss of 112 pilots. The Sabre was also a simpler aircraft and was flown at altitude.<ref>Bashow, David, "Starfighter", 1990, Fortress Plubications, p. 96</ref>

The CF-104 was nicknamed the "Widowmaker" by the press but not by the pilots and crews of the aircraft. David Bashow states on page 92 of his book "I never heard a pilot call it the Widowmaker". Sam Firth is quoted on page 93 in Bashow's book "I have never heard a single person who flew, maintained, controlled, or guarded that aircraft of any force (and that includes the Luftwaffe) call it the Widowmaker". The pilots did refer to it, in jest, as the "Aluminium Death Tube", "The Lawn Dart" and "The Flying Phallus" but generally called it the 104 (one oh four) or the Starfighter.<ref name="autogenerated92">Bashow, David, "Starfighter", 1990, Fortress Plubications, p 92,93</ref>

Low level attack runs in the 104 were done visually at 100 feet AGL and at speeds up to 600 kn. Low level evasive maneuvers could increase speeds to supersonic.<ref>Bashow, David, "Starfighter", 1990, Fortress Plubications, p. 119</ref>

The 104 was very difficult to attack owing to its small size, speed, and low altitude capability. Dave Jurkowski, former CF-104 and CF-18 pilot is quoted "Because of our speed, size and lower level operations, no Canadian Zipper driver was ever 'shot down' by either air or ground threats in the three Red Flag Exercises in which we participated." <ref>Bashow, David, "Starfighter", 1990, Fortress Plubications, p. 138</ref>

The CF-104 was very successful in operational exercises held by NATO. The Canadians first took part in the AFCENT Tactical Weapons meet in 1964 and did so every year after that. This meet was a competition between squadrons from Belgium, France, Germany, USA, Britain, and the Netherlands. Scores were based on several factors. Bomb accuracy, time on target, navigation, mission planning and aircraft serviceability. Pilots were chosen at random from the various squadrons to accurately represent operational capabilities.<ref name="Bashow, David 1990, p. 47">Bashow, David, "Starfighter", 1990, Fortress Plubications, p. 47</ref>

=== AFCENT Tactical Weapons Meet (strike era) ===
* 1964: (first participation) Best team went to the 2 Canadians taking part.<ref>Bashow, David, "Starfighter", 1990, Fortress Plubications, p. 33</ref>
* 1965: Best Nation went to the Canadians, Top individual score went to F/L Frioult of 427.<ref>Bashow, David, "Starfighter", 1990, Fortress Plubications, p. 37</ref>
* 1966: RCAF was second best Nation, Top individual score went to F/L Morion of 421.<ref>Bashow, David, "Starfighter", 1990, Fortress Plubications, p. 38</ref>
* 1967: RCAF best team, McCallum and Rozdeba received awards<ref name="Bashow, David 1990, p. 47"/>
* 1968: Second Best Team (427)<ref name="Bashow, David 1990, p. 52">Bashow, David, "Starfighter", 1990, Fortress Plubications, p. 52</ref>
* 1970: Canadians were 1st in strike event.<ref>Bashow, David, "Starfighter", 1990, Fortress Plubications, p. 58</ref>

=== AFCENT Tactical Weapons Meet (attack era) ===
biennial schedule.
* 1974: (first participation) Top attack pilot Canadian Larry Crabb<ref>Bashow, David, "Starfighter", 1990, Fortress Plubications, p. 78</ref>
* 1976: 1CAG - Highest scoring nation<ref>Bashow, David, "Starfighter", 1990, Fortress Plubications, p. 79</ref>
* 1978: The meet was renamed the Tactical Air Meet the scoring was marred by squabbles and announced a tie.<ref name="Bashow, David 1990, p. 81">Bashow, David, "Starfighter", 1990, Fortress Plubications, p. 81</ref>
* 1980: The Canadians did "well"<ref name="Bashow, David 1990, p. 81"/>
* 1982 onward: the meet was changed to a non-competitive setup.<ref name="Bashow, David 1990, p. 81"/>

=== Royal Flush ===
A competition for Recce squadrons. The Canadians first took part in 1966 and managed the following awards:<ref>Bashow, David, "Starfighter", 1990, Fortress Plubications, p. 51</ref>

* 1968: First place.<ref name="Bashow, David 1990, p. 52"/>
* 1969: First and Second place (441, 439)<ref name="Bashow, David 1990, p. 52"/>
* 1970: 439 won the day competition. (Canada had no IR equipment)<ref name="Bashow, David 1990, p. 52"/>

=== Tiger Meet ===
A competition between NATO squadrons with cat mascots.<ref name="Nato Tiger Meets">{{cite web|url=http://www.natotigers.org/tigermeet/index.php |title=Nato Tiger Meets |publisher=www.NatoTigers.org |date= |accessdate=2014-06-17}}</ref>

* 1979: Silver Tiger Trophy <ref name="Nato Tiger Meets"/>
* 1981: Silver Tiger Trophy <ref name="Nato Tiger Meets"/>
* 1985: Silver Tiger Trophy <ref name="Nato Tiger Meets"/>

In the late 1970s, the [[New Fighter Aircraft program]] was launched to find a suitable replacement for the CF-104, as well as the [[McDonnell CF-101 Voodoo]] and the [[Canadair CF-5]].  The winner of the competition was the CF-18 Hornet, which began to replace the CF-104 in 1982.  All of the CF-104s were retired from service by the Canadian Forces by 1987, with most of the remaining aircraft given to [[Turkey]].

==Variants==
{{Main article|List of Lockheed F-104 Starfighter variants}}
;CF-104:Single-seat fighter-bomber version for the RCAF.
;CF-104D: Two-seat training version for the RCAF.

==Operators==
{{Main article|List of F-104 Starfighter operators}}
;{{CAN}}
* [[Royal Canadian Air Force]]
* [[Canadian Forces]]
;{{DEN}}
* [[Royal Danish Air Force]]
;{{NOR}}
* [[Royal Norwegian Air Force]]
;{{TUR}}
* [[Turkish Air Force]]

==Accidents and incidents==
*On 22 May 1983, during an airshow at the [[Rhein-Main Air Base]], a Canadian CF-104 Starfighter [[1983 Rhein-Main Starfighter crash|crashed]] onto a nearby road, hitting a car and killing all passengers, a vicar's family of five. The pilot was able to eject.<ref>Richadson, W. John and Tim West. [http://www.ejection-history.org.uk/Aircraft_by_Type/Canadian_CF-104.htm "The Canadair CF-104."] ''ejection-history.org,'' 2010. Retrieved: 21 March 2011.</ref>

==Aircraft on display==
[[File:CF104 starfighter borden 2.jpg|thumb|right|CF-104 displayed at CFB Borden]]
[[File:CF-104 (Trenton).jpg|thumb|CF-104D Starfighter 104646 at the [[National Air Force Museum of Canada]], CFB Trenton]]
;Canada
* F-104A, Royal Canadian Air Force ''12700'' used as a pattern aircraft for the CF-104 model - [[Canada Aviation and Space Museum]]<ref>[http://www.aviation.technomuses.ca/collections/artifacts/aircraft/LockheedF-104AStarfighter/ "Lockheed F-104A Starfighter."] ''Aviation.technomuses.ca (Canada Aviation and Space Museum)'', 4 March 1954. Retrieved: 6 January 2013.</ref>
* CF-104, Royal Canadian Air Force ''12703'' - [[Canadian Starfighter Museum]]<ref>{{cite web|url=http://www.canadianstarfightermuseum.ca/picture-gallery.php |title=Canadian Starfighter Museum |publisher=Canadianstarfightermuseum.ca |date= |accessdate=2014-04-19}}</ref>
[[File:CF104 - 12846.jpg|thumb|CF-104 on display at the [[Air Force Museum of Alberta]], located within the [[The Military Museums]], in Calgary, Alberta, Canada.]]
* CF-104, Royal Canadian Air Force ''12846'' - [[Air Force Museum of Alberta]], located within [[The Military Museums]], [[Calgary, Alberta]]{{Citation needed|date=January 2016}}
* CF-104, Royal Canadian Air Force ''104783'' - [[Atlantic Canada Aviation Museum]][http://atlanticcanadaaviationmuseum.com/museum-collections/cf-104-starfighter/ "CF-104 Starfighter."] 
* CF-104, Royal Canadian Air Force ''104784'' - CFB St-Jean-sur-le-Richelieu<ref>{{cite web|url=http://roadsideattractions.ca/popfiles/stjean.html|title=CF-104 "Starfighter" - Saint-Jean-Sur-Richelieu, Quebec|work=roadsideattractions.ca|accessdate=22 June 2016}}</ref> 
* CF-104, Royal Canadian Air Force ''104731'' - [[Comox Air Force Museum]]<ref>[http://www.comoxairforcemuseum.ca/AirPark/starfighter.html "Lockheed Starfighter."]  ''Comoxairforcemuseum.ca (Comox Air Force Museum). Retrieved: 6 January 2013.''</ref>
* CF-104D RCAF ''104758'' and ''104641'' - [[Canadian Warplane Heritage Museum]] in [[Hamilton, Ontario]]<ref>[http://www.warplane.com/pages/aircraft_cf104.html "Canadair CF-104 Starfighter."] ''Warplane.com (Canadian Warplane Heritage Museum).'' Retrieved: 6 January 2013.</ref>
* CF-104D RCAF ''104651'' [[Alberta Aviation Museum]] in [[Edmonton, Alberta]]<ref>{{cite web|url=https://www.albertaaviationmuseum.com/starfighter#!starfighter|title=Lockheed CF-104D Starfighter|work=albertaaviationmuseum.com|accessdate=5 July 2015}}</ref>
* CF-104D - [[Canadian Museum of Flight]], [[Langley Regional Airport]]<ref>[http://www.canadianflight.org/content/lockheed-cf-104-starfighter "Lockheed CF-104 Starfighter."] ''The Canadian Museum of Flight'' via ''Canadianflight.org'', 20 June 2008. Retrieved: 5 August 2013.</ref>
;Hungary
* CF-104 Turkish Air Force ''63-893'' on display at Szolnok Aviation Museum in Szolnok.<ref>[http://www.repulomuzeum.hu/Leltar/Leltarfotok/F-104TR.htm "F-104G (CF-104G), gift of the Turkish Air Force."] ''repulomuzeum.hu''. Retrieved: 17 February 2008.</ref>
;Norway
*CF-104D cn. 104 730. Displayed at the Sola Air Museum in [[Stavanger]].<ref>[http://www.flymuseum-sola.no/sider/hoved.html "CF-104." (Norwegian language).] ''Sola Museum.'' Retrieved: 22 October 2008.</ref>
*CF-104 cn. 104 755. Well kept at [[Kjeller]], [[Lillestrøm]].<ref name=Glenne>Oppdrag utført - Norges luftmilitære kulturarv (Glenne, Roar. 2012)</ref>
*CF-104D cn. 104 766. Displayed on pedestal in front of [[Kjeller Air Station]], [[Lillestrøm]].<ref name=Glenne />
*CF-104 cn. 104 801. Displayed at the Norwegian Air Museum in [[Bodø]].<ref name=Glenne />
*CF-104 cn. 104 836. Given to Bardufoss High School. Displayed outside Bardufoss Air Station.<ref name=Glenne />
*CF-104 cn. 104 882. Displayed as gatekeeper at [[Volvo Arero Norway]], [[Kongsberg]].<ref name=Glenne />
*CF-104 cn. 104 886. Privately owned at Rudshøgda, [[Hamar]].
*CF-104 cn. 104 889. At Torp Airport, [[Sandefjord]].<ref name=Glenne />

==Survivors==
* The [[Florida]] based civilian Starfighters Demo team currently operate one CF-104D and two CF-104 aircraft under the company ''RLB Aviation Inc''.<ref name="N104RB">[http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=104RB "N-Number Inquiry Results Registration N104RB Serial number 104632."] ''[[Federal Aviation Administration]]'', October 2009. Retrieved: 4 October 2009.</ref><ref name="N104RD&RN">[http://registry.faa.gov/aircraftinquiry/MMS_results.aspx?Mmstxt=5260419&Statetxt=FL&conVal=0&PageNo=1 "N-Number Inquiry Results Registration N104RD & N104RN Serial numbers 104850 & 104759."] ''[[Federal Aviation Administration]]'', October 2009. Retrieved: 4 October 2009.</ref><ref name="Airliners.netN104RB">Wang, Ben. [http://www.airliners.net/photo/Starfighters-Demo-Team/Lockheed-CF-104D-Starfighter/1116786/M/ "Picture of the Lockheed CF-104D Starfighter aircraft."] ''airliners.net'',  September 2006. Retrieved: 4 October 2009.</ref>
* Mark Sherman from Phoenix, Arizona owns and operates a single CF-104D under the company ''Fuel Fresh Inc''.<ref name="N104">[http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=104 "N-Number Inquiry Results Registration N104 Serial number 104633."] ''[[Federal Aviation Administration]]'', October 2009. Retrieved: 4 October 2009.</ref>
* CF-104D cn. 104 637 has been restored to flight condition by a group of volunteers called Friends of Starfighter, and is based at [[Bodø Main Air Station|Bodø]] in Norway.<ref>{{Cite web|url=https://www.nrk.no/nordland/her-flyr-eskil-_the-widowmaker_-for-forste-gang-pa-33-ar-1.13152617|title=Her flyr Eskil "The Widowmaker" for første gang på 33 år|last=NRK|access-date=2016-09-29}}</ref>

==Specifications (CF-104)==
[[File:Cf104 canadian warplane heritage museum 3.jpg|thumb|CF-104D in front of [[Canadian Warplane Heritage Museum]]]]
{{aircraft specifications
<!-- please use lowercase only for this section. Valid answers are jet, prop, plane, copter, yes, no -->
|plane or copter?=plane
|jet or prop?=jet
<!-- please include units. if something doesn't apply, leave it blank. -->
|crew=1
|capacity=
|length main=54 ft 6 in
|length alt=16.7 m
|span main=21 ft 9 in
|span alt=6.63 m
|height main=13 ft 5 in
|height alt=4.08 m
|area main=<!-- ft²-->
|area alt=<!-- m²-->
|empty weight main=14,000 lb
|empty weight alt=6,300 kg
|loaded weight main=<!-- lb-->
|loaded weight alt=<!-- kg-->
|useful load main=<!-- lb-->
|useful load alt=<!-- kg-->
|max takeoff weight main=29,038 lb
|max takeoff weight alt=13,171 kg
|engine (jet)=[[Orenda J79]]-OEL-7
|type of jet=afterburning [[turbojet]]
|number of jets=1
|thrust main=10,000 lbf
|thrust alt=44 kN
|afterburning thrust main=15,800 lbf
|afterburning thrust alt=66.7 kN
|max speed main=1,146 mph 
|max speed alt=996 kn, 1,844 km/h
|cruise speed main=<!-- mph-->
|cruise speed alt=<!-- knots,  km/h-->
|never exceed speed main=<!-- mph-->
|never exceed speed alt=<!-- knots,  km/h-->
|stall speed main=<!-- mph-->
|stall speed alt=<!-- knots,  km/h-->
|range main=1,630 mi
|range alt=1,420 nmi, 2,630 km
|ceiling main=50,000 ft
|ceiling alt=15,240 m
|climb rate main=<!-- ft/min-->
|climb rate alt=<!-- m/s-->
|loading main=<!-- lb/ft²-->
|loading alt=<!-- kg/m²-->
|thrust/weight=<!-- a unitless ratio -->
|guns=1× [[20 mm caliber|20 mm]] (0.79 in) [[M61 Vulcan|M61A1 Vulcan]] [[autocannon|cannon]]
* '''Other:''' External bombs, rockets and missiles
}}

==Badges==
<gallery>
File:CF104Crest.jpg|CF-104 Crest worn by aircrew and ground crew in the mid-1970s
</gallery>

==See also==
{{Portal|Aviation|Canadian Armed Forces}}
{{aircontent
|related=
* [[Aeritalia F-104S Starfighter]]
* [[Lockheed F-104 Starfighter]]
|similar aircraft=
* [[English Electric Lightning]]
* [[Sukhoi Su-15]]
|lists=
* [[List of aircraft of Canada's air forces]]
* [[List of fighter aircraft]]
* [[List of Lockheed F-104 Starfighter operators]]
}}

==References==

===Notes===
{{Reflist|group=N}}

===Citations===
{{Reflist|30em}}

===Bibliography===
{{Refbegin}}
* Bashow, David L. ''Starfighter: A Loving Retrospective of the CF-104 Era in Canadian Fighter Aviation, 1961-1986''. Stoney Creek, Ontario: Fortress Publications Inc., 1990. ISBN 0-919195-12-1.
* Francillon, R. J. ''Lockheed Aircraft Since 1913.'' London: Putnam, 1987. ISBN 0-370-30329-6.
* Greenhous, Brereton and Hugh A. Halliday. ''Canada's Air Forces, 1914–1999''. Montreal: Editions Art Global and the Department of National Defence, 1999. ISBN 978-2-92071-872-2.
* McIntyre, Robert. ''CF-104 Starfighter (Canadian Profile: Aircraft No. 1)''. Ottawa, Ontario: Sabre Model Supplies Ltd., 1985. ISBN 0-920375-00-6.
* Pickler, Ron and [[Larry Milberry]]. ''Canadair: The First 50 Years.'' Toronto: CANAV Books, 1995. ISBN 0-921022-07-7.
* Stachiw, Anthony L. ''CF-104 Starfighter (Aircraft in Canadian Service)''. St. Catharine's, Ontario: Vanwell Publishing Limited, 2007. ISBN 1-55125-114-0.
{{Refend}}

==External links==
{{Commons category|Canadair CF-104}}
*[http://www.i-f-s.nl/ The International F-104 Society]
*[http://www.airforce.forces.gc.ca/site/equip/historical/starfighterlst_e.asp Canadian Forces Historical Aircraft - CF-104]
*[http://www.rcaf.com/aircraft/fighters/starfighter/index.php?name=Starfighter CF-104 at RCAF.com]
*[http://www.starfighters.net/Aircraft_Home.html Starfighters F-104 Demo team]
*[http://www.starfighter.no/web/indeng.html Norwegian site with CF-104D restoration for flight]
*[http://www.joebaugher.com/usaf_fighters/f104_27.html Canadair CF-104 Starfighter]
*[http://www.joebaugher.com/usaf_fighters/f104_28.html Lockheed CF-104D Starfighter]
*[http://www.tailsthroughtime.com/2015/08/canadas-nuclear-strike-force-1st-air.html Canada's Nuclear Strike Force: 1st Air Division 1964-1972]
{{Canadair aircraft}}
{{CF aircraft}}

[[Category:Canadair aircraft|CF-104]]
[[Category:Canadian fighter aircraft 1960–1969]]
[[Category:Single-engined jet aircraft]]
[[Category:Monoplanes]]
[[Category:Lockheed F-104 Starfighter]]
[[Category:Nuclear weapons of Canada]]