{{Infobox company
| name             = Storm Aircraft
| logo             = 
| caption          = 
| type             = [[Privately held company]]
| traded_as        = 
| genre            = <!-- Only used with media and publishing companies -->
| fate             = 
| predecessor      = 
| successor        = 
| foundation       = 1981
| founder          = 
| defunct          = <!-- {{End date|YYYY|MM|DD}} -->
| location_city    = [[Sabaudia]]
| location_country = [[Italy]]
| location         = 
| locations        = 
| area_served      = 
| key_people       = 
| industry         = [[Aerospace]]
| products         = [[Kit aircraft]]<br />[[Microlight aircraft]]<br />[[Aircraft parts]]
| services         = 
| revenue          = 
| operating_income = 
| net_income       = 
| assets           = 
| equity           = 
| owner            = 
| num_employees    = 
| parent           = 
| divisions        = 
| subsid           = 
| homepage         = {{URL|www.stormaircraft.com}}
| footnotes        = 
| intl             = 
}}

'''Storm Aircraft''', also called the '''StormAircraft Group''', is an [[Italy|Italian]] aircraft manufacturer based in [[Sabaudia]]. The company specializes in the design and manufacture of [[kit aircraft]] and [[microlight aircraft]], predominantly for the European market, as well as sub-contract work for manufacturers of larger aircraft.<ref name="Aerocrafter">Purdy, Don: ''AeroCrafter - Homebuilt Aircraft Sourcebook, Fifth Edition'', pages 245-246. BAI Communications, 15 July 1998. ISBN 0-9636409-4-1</ref><ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', pages 77-78. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref>

Storm Aircraft was originally called SG Aviation srl, but changed its name for brand alignment. The company works predominantly with [[aluminum]] sheet and [[fibreglass]] construction methods for its kit aircraft.<ref name="Aerocrafter" /><ref name="WDLA11" />

==History==
Founded in 1981 by a group of French and Italian aeronautical engineers as SG Aviation, the company engaged in sub-contract work including the [[Aermacchi MB-339]] [[empennage]] and [[Martin-Baker]] [[ejection seat]] mechanical units. The company has designed and produced cold parts for engine nacelles, including inlets, fan cowls and EBU and systems-to-engine interfaces.<ref name="Hist">{{cite web|url = http://www.stormaircraft.com/new-page.htm|title = Company Story|accessdate = 24 February 2014|last = Storm Aircraft|date = n.d.}}</ref>

The company has also sold over 1200 kit and complete aircraft to customers in 20 countries.<ref name="Hist" />

== Aircraft ==
[[File:Flyer (SG Aviation) Storm 300B RG AN0878872.jpg|thumb|right|Storm RG]]
{| class="wikitable" align=center style="font-size:90%;"
|-
|+ align=center style="background:#BFD7FF"| '''Summary of aircraft built by SG Aviation and Storm Aircraft'''
|- style="background:#efefef;"
! Model name
! First flight
! Number built
! Type

|-
|align=left| '''[[Storm 280]]'''
|align=center| 
|align=center| 
|align=left| two seat low-wing [[microlight aircraft]]
|-
|align=left| '''[[Storm 300]]'''
|align=center| 
|align=center| 
|align=left| two seat low-wing [[homebuilt aircraft]]
|-
|align=left| '''[[Storm 320E]]'''
|align=center| 
|align=center| 
|align=left| two seat low-wing  microlight aircraft
|-
|align=left| '''[[Storm Century]]'''
|align=center| 
|align=center| 
|align=left| two seat low-wing microlight aircraft
|-
|align=left| '''[[Storm Rally]]'''
|align=center| 
|align=center| 
|align=left| two seat high-wing microlight aircraft
|-
|align=left| '''[[Storm RG Fury]]'''
|align=center| 
|align=center| 
|align=left| two seat retractable gear low-wing microlight aircraft
|-
|align=left| '''[[Storm Sea Storm]]'''
|align=center| 
|align=center| 
|align=left| two to four seat amphibious [[flying boat]] homebuilt aircraft
|-

|}

==References==
{{Reflist}}

==External links==
{{Commons category}}
*{{Official website|http://www.stormaircraft.com/}}
{{Storm Aircraft}}

[[Category:Aircraft manufacturers of Italy]]
[[Category:Homebuilt aircraft]]