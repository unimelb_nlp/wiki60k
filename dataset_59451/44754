{{italic title}}
{{Infobox Journal
| title = Le Naturaliste canadien
| cover = [[File:Naturaliste Canadien cover.jpg|Image:Naturaliste Canadien cover.jpg]]
| discipline = [[Natural science]]s and [[conservation biology]] in [[Quebec]]
| language = Mostly [[French language|French]]
| abbreviation = Nat. Can.
| publisher = [[Société Provancher d'Histoire Naturelle du Canada]]
| country = Canada
| frequency = Semiannual
| history = 1868-1891; 1894-present
| impact = 
| impact-year = 
| website = http://www.provancher.qc.ca/publications/naturaliste_can.html
| OCLC = 6312227
| LCCN = 05037702
| ISSN = 0028-0798
}}

'''''Le Naturaliste Canadien''''' is a [[Canada|Canadian]] [[French language|French-language]] [[Peer review|peer-reviewed]] [[scientific journal]] published semiannually by the [[Société Léon-Provancher d'Histoire Naturelle du Canada]]. The journal publishes articles on all topics of [[natural science]]s with a specific focus on [[ecology]] and [[conservation biology]] in [[Quebec]]. The journal also acts as the official publication of the society. The journal is the oldest scientific publication in French in North America and one of the oldest scientific journals still in publication in Canada.

==Foundation by Provancher==
The ''Naturaliste Canadien'' was launched in 1868 by [[Léon Abel Provancher]], an important figure in [[Quebec]]'s scientific history. It was aimed to be an organ for the diffusion of scientific findings and popularization:

{{Quotation|''Populariser les connaissances en histoire naturelle, provoquer les recherches, recueillir les observations, constater les découvertes et faire connaître les nouvelles applications que l'on peut faire des connaissances déjà acquises au profit des arts, de l'industrie et des besoins de la vie, tel sera le but de cette publication.''<br>
<br>
"Disperse knowledge of natural history, trigger research, gather new observations, find out about new findings and diffuse new applications for existing knowledge for the profit of the arts, industry, and all needs of life, such will be the goal of this publication"|Provancher||inaugural issue of the ''Naturaliste Canadien''<ref>Quoted in Desmeules, p. 13.</ref>}}

For all his life, Provancher would remain the owner and editor of the journal, as well as its main contributor, although a number of other prominent naturalists of the time also published in it: [[Dominique-Napoléon Saint-Cyr]], [[Charles-Eusèbe Dionne]] (who also for a time edited a concurrent journal), [[François-Xavier Bélanger]], [[Joseph-Alexandre Crevier]], [[James MacPherson Le Moine]] and [[Jean-Baptiste Meilleur]] amongst others. Their combined contribution amount to less than a hundred pages; Provancher providing the vast majority of the journal's content, over 6,000 pages' worth.<ref>Desmeules, p. 15.</ref> While many articles treated scientific topics, Provancher also wrote essays on related topics such as public education, the establishment of museums, and a [[botanical garden]] project in [[Quebec City]]. Soon after it was launched, having reached 500 subscription (a rather high rate when 50% of the population couldn't sign their name), it received a government grant to help with publication.

During this whole period, Provancher maintained an overall anti-[[evolution]]ist (particularly anti-Lamarckian) stance; for example, he published several virulent attack on evolution from [[François-Xavier Burque]]. However, the naturalist still got into disagreements with the more extreme elements of Quebec [[ultramontanism|ultramontane]]s over some theological points. A [[polemics|polemist]] at heart, he had a particularly violent dispute with journalist [[Jules-Paul Tardivel]], in which he was defended by Burque, and his University and [[Roman Catholic Archdiocese of Quebec|Archdiocese]].<ref>Chartrand ''et al.'', pp. 172–179.</ref> In another instance, he called Lamarckism a theory "serving the self-pride of [[materialism|materialist]] French politicians, [[Paul Bert|Bert]], Hugo [probably referring to both [[Victor Hugo]] and his sons], [[Jules Ferry|Ferry]], [[René Goblet|Goblet]], and [[Georges Clemenceau|Clémenceau]].{{sic}}"<ref>Quoted in Chartrand ''et al.'', p. 178.</ref> He also launched a crusade in the journal's pages against another journal, the ''Journal des Campagnes'', an agricultural magazine published by the teachers at [[Collège de Sainte-Anne-de-la-Pocatière]] over a perceived slight.<ref name=caron>{{cite journal|last=Caron|first=Omer|title=L'Oeuvre du ''Naturaliste Canadien''| journal=Le Naturaliste Canadien| volume=61| issue=1| pages=5–17}}</ref> This dispute degenerated to the point of involving [[Legislative Assembly of Quebec|MLA]]s, [[Minister (government)|minister]]s, and finally the [[Roman Catholic Archdiocese of Quebec|Archbishop of Quebec]], who chastised both journal editors.<ref name="perron">Perron, p. 7.</ref>

Despite a number of interruptions, the monthly publication was relatively steady until 1879, when a series of ministers successively canceled and reestablished the grant Provancher depended on to publish his journal. After Provancher refused to retract an article criticizing [[Premier of Quebec|Premier]] [[Honoré Mercier]] in 1890, the grant was definitively canceled and publication stopped with the May 1891 issue. At the time, Provancher was already sick, but kept hope of restarting publication, tasking his self-styled disciple [[Victor-Alphonse Huard]] with it.

==Huard's continuation==
Despite Provancher's hopes that [[Charles Boucher de Boucherville]]'s government would be more open, Huard could not secure any grant until 1919, and published the journal himself with very little interruption from 1894 until his death in 1929. Under Huard, it became a more accessible journal devoted to vulgarization, and frequently included reports on curiosities and travel notes. Although neither as virulent nor uncompromising as his predecessor, he was always willing to publish attacks on evolutionary ideas.<ref>Desmeules, pp. 15–16; Chartrand ''et al.'', p. 195.</ref>

While [[entomology]] had a noticeable priority, like it did under Provancher, the journal, under the impetus of its many contributors, published on a much wider variety of topics including [[ichthyology]], [[geology]], [[anatomy]] and [[microbiology]]. Also published were articles of regional interests (''e.g.'' "Salmon in Lac Saint-Jean", "Grape harvest in Chicoutimi", "The caterpillars of Saguenay"), reports on national and regional expositions, and various posthumous papers of Provancher that Huard discovered in the papers he recovered. Huard also published extracts of his books and his multi-part biography of Provancher. Before his death he bequeathed, as early as 1925, the journal to [[Laval University]] with the two conditions that the title not change, and it not be merged with another publication.<ref name=caron/>

==Laval and modern incarnations==
The University took over the journal and its publication (which was also for a time the official publication of the newly founded Linnean Society of Quebec) from 1930 onward, making it the official publications of the department of biology. The format and editorial policies—only original material—were revised, attracting criticism from both its original public and the scientific community as the editorial committee attempted to satisfy both. As early as 1931 it was one of the most circulated French-Canadian publications, its 600 copies going to individuals and institutions in 18 countries,<ref name=caron/> but changes in leadership and editorial problems left the journal wobbly until the 60s. With the subtitle "''revue d'écology et de systématique''" ("journal of ecology and systematics"), a clearer policy and a proper peer review system, the magazine acquired an unprecedented circulation, and was published in French with [[abstract (summary)|abstract]]s in both English and French. It remained a monthly until 1942, when it became a bimonthly, then became a quarterly publication in 1976.

By 1991 dwindling numbers of submissions, increasing difficulties with respecting deadlines—several of the later issues were published with a year of delay—, new governmental policies for grants and other factors such as the increasing specialization of publications made discontinuation the obvious outcome. Issue 1 of volume 118 was the last of this run.<ref>{{cite journal|year=1991|last=Payette|first=Serge|title=Note de la rédaction| journal=Le Naturaliste Canadien| volume=118| issue=1| pages=1}}</ref> In 1994 a new departmental journal, ''[[Écoscience]]'' was launched, edited by mostly the same team that had edited the previous journal. The University announced it would be willing to give out the ''Naturaliste'' if its original goals were to be perpetuated. The [[Société Léon-Provancher d'Histoire Naturelle du Canada]] and the university came to an agreement where the society would take over publication, the magazine replacing the Society's previous ''Euskarien''.<ref name="fil">{{cite news|last=Desmatis| first=André| date=November 17, 1994|title= La Société Provancher reprend Le Naturaliste canadien| work=Au Fil des Événements| url=http://www.scom.ulaval.ca/Au.fil.des.evenements/1994/08/013.html| accessdate=2007-10-25}}</ref>

Although the combined volumes have a single [[collation]], Provancher, Huard's and Laval's runs are often treated as first, second and third series respectively, with Huard's and Laval's series having collations of their own. This additional collation was abandoned at Laval in 1942. The journal now publishes papers and observations on [[ecology]] and [[conservation biology]] in Quebec, as well as the Society's own news, notably about [[Basque people|Basque]] history in the province—the Society owns and manages the [[Île aux Basques]] archeological site and [[List of Migratory Bird Sanctuaries of Canada|bird sanctuary]]. It is a semiannual publication and as of 2007 was indexed by Repères, [[CSA (database company)|Cambridge Scientific Abstracts]], and [[The Zoological Record]]. The journal has an unusual policy on reprinting: much like the [[Creative Commons]] attribution license, articles may be reprinted in all or part as long as the source is mentioned.

==Notes==
{{Reflist}}

== References ==
{{Refbegin}}
;General
*{{Cite book|last=Desmeules| first= Mélanie|year=2004| title=L'abbé Léon Provancher: Le naturaliste polyvalent| chapter=La voie du Naturaliste: ''Le Naturaliste canadien''| publisher=Lidec| location=Montreal| series=Célébrités : collection biographique '''101'''| pages=12–16|isbn=2-7608-7089-8|language=fr}}
*{{Cite journal|last=Desmeules| first= Mélanie|year=2002| title=Les Années chicoutimiennes du Naturaliste canadien|journal=Saguenayensia| volume=44|issue=3| pages=19–21|language=fr}}
*{{Cite book|last=Chartrand| first= Luc |author2=Raymond Duchesne |author3=Yves Gingras |year=1988 |title= Histoire des sciences au Québec| publisher= Boréal| location=Montreal| isbn= 2-89052-205-9|language=fr}}
*{{cite journal|last=Perron|year=2001|first=Jean-Marie|title=La Course à relais du Naturaliste Canadien| journal=Le Naturaliste Canadien| volume=125| issue=2| pages=6–10|language=fr}}
*[http://lccn.loc.gov/05037702 Bibliographic data]. Library of Congress Online Catalog]. Accessed 2008-07-04.
{{Refend}}

{{DEFAULTSORT:Naturaliste Canadien}}
[[Category:Ecology journals]]
[[Category:French-language journals]]
[[Category:Publications established in 1868]]