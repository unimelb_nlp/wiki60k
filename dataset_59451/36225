{{good article}}
{{Infobox television episode
| title = The Amazing Maleeni
| image = Maleeni x files.jpg
| image_size = 250px
| alt = A severed head lies on the floor, facing the viewer.
| caption = The severed head of The Amazing Maleeni.
| series = [[The X-Files]]
| season = [[The X-Files (season 7)|7]]
| episode = 8
| airdate = {{Nowrap|{{Start date|2000|01|16}}}}
| production = 7ABX08
| writer = [[Vince Gilligan]]<br />[[John Shiban]]<br />[[Frank Spotnitz]]
| director = [[Thomas J. Wright]]
| length = 44 minutes
| guests = *[[Ricky Jay]] as Herman/Albert Pinchbeck
*[[Jonathan Levit]] as Billy LaBonge
*[[Robert LaSardo]] as Cissy Alvarez
*Jim Maniaci as Bullethead
*Rick Marzan as Holding Cell Officer
*Mark Chaet as Bank Officer
*Dennis Keiffer as Bullethead
*Dan Rice as Uniform Cop
*Sherri Howard as Female Employee
*J. David as Young Boss
*Steven Barr as Courier Guard
*Adam Vernier as Driver<ref name="plot">Shapiro, pp. 95–104</ref>
| prev = [[Orison (The X-Files)|Orison]]
| next = [[Signs and Wonders (The X-Files)|Signs and Wonders]]
| episode_list = [[List of The X-Files episodes|List of ''The X-Files'' episodes]]
}}
"'''The Amazing Maleeni'''" is the eighth episode of the [[The X-Files (season 7)|seventh season]] of the [[science fiction]] [[television series]] ''[[The X-Files]]''. It premiered on the [[Fox Broadcasting Company|Fox network]] in the United States on January 16, 2000. It was written by [[Vince Gilligan]], [[John Shiban]], and [[Frank Spotnitz]] and directed by [[Thomas J. Wright]]. The episode is a [[List of Monster-of-the-Week characters in The X-Files|"Monster-of-the-Week"]] story, unconnected to the series' wider [[Mythology of The X-Files|mythology]]. "The Amazing Maleeni" earned a Nielsen household rating of 9.4, being watched by 16.18 million people in its initial broadcast. The episode received mixed reviews.

The show centers on [[Federal Bureau of Investigation|FBI]] special agents [[Fox Mulder]] ([[David Duchovny]]) and [[Dana Scully]] ([[Gillian Anderson]]) who work on cases linked to the paranormal, called [[X-File]]s. Mulder is a believer in the paranormal, while the skeptical Scully has been assigned to debunk his work. In this episode, The Amazing Maleeni, a small-time magician, performs an amazing feat to impress a heckler—he turns his head 360 degrees. So when he is later found without a head at all, Mulder and Scully arrive on the case and discover an angry ex-con, an unimpressed rival, and Maleeni’s twin brother. All seem to have something to do with a plan to rob a major bank.

Although written by Gilligan, Shiban, and Spotnitz, the story for "The Amazing Maleeni" was conceived largely by executive producer [[Frank Spotnitz]], who had wanted to do an episode dealing with "magic and illusion" since the show's [[The X-Files (season 2)|second season]]. Real-life magician [[Ricky Jay]], who also was Spotnitz's favorite, was brought in to play the part of the titular Maleeni.

==Plot==
At the [[Santa Monica Pier]], a magician, The Amazing Maleeni, twists his head completely around at a carnival, while Billy LaBonge, another magician, heckles Maleeni during the event. As he's leaving, his severed head falls completely off. Billy LaBonge is later questioned by [[Fox Mulder]] ([[David Duchovny]]) and [[Dana Scully]] ([[Gillian Anderson]]); he tells the agents that he thought Maleeni was a ripoff. During the autopsy, Scully finds that, although Maleeni's head was cleanly cut off, he died of a heart attack. She also finds that he was dead for at least a month and refrigerated, even though the carnival manager spoke to him mere moments leading up to his head falling off.

Meanwhile, LaBonge finds a man named Cissy Alvarez whom Maleeni owed money. LaBonge admits that he caused Maleeni's head to fall off, and says that he will give Alvarez the money he is owed if he helps him with his magic. Mulder and Scully learn that Maleeni has an identical twin brother, Albert. Albert, interestingly enough is even wearing a neckbrace, which he says he got in a car accident in Mexico. Mulder tells him he thinks he did the magic act, but the man shows that he has no legs, which he also lost in Mexico in the car accident. Back at work, Alvarez threatens Pinchbeck that he will kill him if he does not get his money. LaBonge then frames Alvarez for a robbery by attacking a security truck disguised as Alvarez. Mulder soon finds out that Pinchbeck is the real Maleeni and that he faked having no legs. After confronting Pinchbeck, he admits that he faked his own death in order to get out of Alvarez's debt. Pinchbeck admits that he found his brother dead of a heart attack at home and used his body as a double. Pinchbeck is promptly arrested, as is LaBonge, who brings a gun to a bar, in an attempt to purposely get arrested. In addition, Alvarez is arrested because of the attempted robbery LaBonge did earlier.

The vault at Pinchbeck's work is emptied and the money is found above Alvarez' bar. Later Mulder and Scully confront LaBonge and Maleeni as they are released on bail, where Mulder explains that he figured out their plan - LaBonge and Pinchbeck were not, in fact, enemies, and that they had been working together to put Alvarez in prison for making LaBonge's life miserable in prison 8 years prior, and that, as masters of sleight of hand and escape tricks, the two of them easily escaped, performed the robbery, and returned to their cells before being noticed.

After the two magicians make their exit, confident in the lack of evidence against them, Mulder reveals to Scully the true trick being performed—that everything involving Alvarez was purely misdirection. Earlier, when checking whether Pinchbeck had stolen funds from the bank, the manager had told Mulder they would need his badge number and thumb print to gain access to the [[Electronic Fund Transfer]] (EFT) system. Mulder shows Scully Maleeni's wallet, which he had collected from evidence before confronting him and LaBonge. When the agents first met LaBonge, he had surreptitiously pickpocketed their badges as an example of his skill with sleight of hand, which gave him Mulder's badge number. Upon their first meeting with Pinchbeck impersonating his brother, Pinchbeck did a card trick with Mulder, leaving Mulder's prints on the card, which as Mulder displays, is securely in Maleeni's wallet. Mulder explains that the pair purposely acted in a high-profile manner to draw the attention of the FBI, and that if they had collected the badge number and thumbprint, they would have been able to perform EFTs. As Mulder and Scully leave the jail, Scully shows that she, too, has learned a trick, and turns her hand around 360 degrees in a similar fashion as LaBonge had done with his hand before. Mulder asks Scully to explain how, and she brushes him off, saying simply "magic".<ref name="plot"/>

==Production==
[[File:Ricky Jay by David Shankbone.jpg|Noted magician [[Ricky Jay]] appeared in the episode.|200px|thumb|right]]

===Writing===
The idea behind "The Amazing Maleeni" started with executive producer [[Frank Spotnitz]]. When pitching ideas for the seventh-season episodes, Spotnitz wanted an episode to deal solely on "magic and illusion" and have nothing to do with the paranormal. Spotnitz had been petitioning the writers of ''The X-Files'' for a magic-based episode since [[The X-Files (season 2)|season two]], but nothing ever came to light. Eventually, during the seventh season, [[Vince Gilligan]] was assigned to write the episode, something he described as "agony".<ref name="s105"/> Gilligan explained, "The episode started with Frank, because he—for several years—had wanted to write an episode about magicians. Frank was a fan of the TV show ''[[The Magician (U.S. TV series)|The Magician]]'' with [[Bill Bixby]] so I believe that was part of it, but Frank was interested in the idea of magic and the idea of fooling people who wished to be fooled."<ref name="big book">Hurwitz and Knowles, p. 177</ref>

Spotnitz's favorite magician was [[Ricky Jay]], and so, for the episode, Jay was brought in to play the part of The Amazing Maleeni. Initially, however, the episode hit a snag. The production crew discovered that Jay's production agency had not been informed and that he would be unable to guest star in the episode. The staff's back-up magician, [[David Blaine]], was unavailable for shooting as well. Series creator [[Chris Carter (screenwriter)|Chris Carter]], however, later stated that the show would not take no for an answer: "We got on the phone with him. He agreed to come to our offices to talk about the script and ended up doing some card tricks for us that reduced Frank and I to being six-year-olds again."<ref name="s105"/> Jay eventually agreed to do the part, but requested that he only do the tricks he was accustomed to doing. Thus, many of Jay's trademark tricks were included in the episode's script.<ref name="s105"/> Gilligan, in retrospect, later noted, "There was no choice other than Ricky Jay as far as we were concerned. He was not looking forward to the idea of playing a magician because I think he felt that magicians were never portrayed very realistically in movies or television shows."<ref name="big book"/>

The cast and crew of the episode enjoyed the "amusement park" feel of the story. [[Gillian Anderson]] later noted, "Because of all the magic, I was constantly being entertained. The difficulty with something like this is you have tendency to forget that people are still having bad things happen to them."<ref name="s105"/> Anderson later stated that, because many of the lines were written in a tongue-in-cheek style, she and [[David Duchovny]] said them in a humorous style; the two had to keep reminding themselves that the story revolved around a murder.<ref name="s105"/>

===Special effects===

The episode used several special effects. However, in order to ensure that the episode felt "camera-real" to preserve the theme of magic and illusions, many of the more intense effects were replaced with more conventional effects. For instance, the scene wherein Bill LaBonge's hand erupted into flames was created through the use of stunt man, rather than through expensive and, ultimately, "less convincing" CGI effects. The scene featuring The Amazing Maleeni turning his head 360 degrees was created using a prosthetic head, courtesy of John Vulich's Optic Nerve Studios.<ref name="s105">Shapiro, p. 105</ref>

==Broadcast and reception==
"The Amazing Maleeni" first aired in the United States on January 16, 2000.<ref name="DVD">{{cite DVD notes |title=The X-Files: The Complete Seventh Season |titlelink=The X-Files (season 7) |origyear=1999–2000 |others=[[Kim Manners]], et al |type=booklet |publisher=[[Fox Broadcasting Corporation|Fox]]}}</ref> This episode earned a [[Nielsen rating]] of 9.4, with a 14 share, meaning that roughly 9.4 percent of all television-equipped households, and 14 percent of households watching television, were tuned in to the episode.<ref name="ratings"/> It was viewed by 16.18 million viewers.<ref name="ratings">Shapiro, p. 281</ref> The episode aired in the United Kingdom and Ireland on [[Sky1]] on May 7, 2000 and received 0.79 million viewers, making it the fourth most watched episode that week.<ref>{{cite web |url=http://www.barb.co.uk/viewing/weekly-top-10/? |title=BARB's multichannel top 10 programmes |publisher=barb.co.uk |accessdate=4 January 2011}} Note: Information is in the section titled "w/e May 1–7, 2000", listed under Sky 1</ref>

The episode received mixed reviews from critics. Kenneth Silber from [[Space.com]] was critical of the episode's intricacy, writing, "'The Amazing Maleeni' is a convoluted episode that ultimately lacks verve and excitement. Even if one can figure out what is going on, there remains the question of how much, or even whether, to care."<ref>{{cite web|last=Silber |first=Kenneth |title=The X-Files - 'The Amazing Maleeni' |url=http://space.com/sciencefiction/tv/xfiles_708_000117.html |work=''[[Space.com]]'' |publisher=TechMediaNetwork |date=9 June 2000 |accessdate=5 January 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20050207160006/http://space.com/sciencefiction/tv/xfiles_708_000117.html |archivedate=February 7, 2005 }}</ref> [[Robert Shearman]] and [[Lars Pearson]], in their book ''Wanting to Believe: A Critical Guide to The X-Files, Millennium & The Lone Gunmen'', rated it two stars out of five. The two noted that, had the episode used real magic tricks instead of "resort[ing] to [[Computer-generated imagery|CGI]], the episode would have been much more than a simple "cheat".<ref name="shear"/> Furthermore, Shearman and Pearson criticized the fact that the explanation behind Maleeni turning his head around 360 degrees was never satisfactorily explained.<ref name="shear">Shearman and Pearson, p. 213</ref> Paula Vitaris from ''[[Cinefantastique]]'' gave the episode a mixed review and awarded it two stars out of four.<ref name=cinepaula>{{cite journal|last=Vitaris|first=Paula|title=The X-Files Season Seven Episode Guide|journal=[[Cinefantastique]]|date=October 2000|volume=32|issue=3|pages=18–37}}</ref> Vitaris compared the episode's plot to the 1999 movie ''[[Arlington Road]]'', noting that while "the scheme is fun to watch while it unfolds, […] in the end, it's not credible; too much is left open to chance for it really to happen."<ref name=cinepaula/>

Not all reviews were so critical. Rich Rosell from Digitally Obsessed awarded the episode 4 out of 5 stars. Although he noted that the episode was "not Gilligan's best work" and that the writing was "a little spotty", he said "the vibe [of 'The Amazing Maleeni'] is very well done."<ref name="dig">{{cite web|last=Rosell|first=Rich|title=The X-Files: The Complete Seventh Season|url=http://www.digitallyobsessed.com/displaylegacy.php?ID=4807|publisher=DigitallyObsessed|date=27 July 2003|accessdate=14 January 2012}}</ref> Tom Kessenich, in his book ''Examinations'', gave the episode a moderately positive review, writing "My heart wasn't racing and I wasn't sitting on the edge of my seat as Mulder and Scully unmasked our two magicians […] But it was fun to put the clues together to see what it all added up to."<ref name="kess">Kessenich, p. 107</ref> Zack Handlen of ''[[The A.V. Club]]'' awarded the episode a "B+" and wrote that he "love[d] it".<ref name=avclub/> He praised the episode's "good-naturedness", which he felt was due to Mulder and Scully's interaction.<ref name=avclub/> Handlen also enjoyed the writing, noting that the "script also does a good job at doling out its secrets in a way that never makes either the magicians or our heroes come off as idiots".<ref name=avclub>{{cite web|last=Handlen|first=Zack|title='Orison'/'The Amazing Maleeni' {{!}} The X-Files/Millennium {{!}} TV Club|url=http://www.avclub.com/articles/orisonthe-amazing-maleeni,89725/|work=[[The A.V. Club]]|publisher=[[The Onion]]|accessdate=December 30, 2012|date=December 22, 2012}}</ref> Paul Spragg of Xposé wrote positively of the episode, describing it as a "rather fun tale".<ref>{{cite web|url=http://www.visimag.com/xpose/xs14_feature.htm |archiveurl=https://web.archive.org/web/20071127205149/http://www.visimag.com/xpose/xs14_feature.htm |title=Xposé Special magazine #14: The year in The X-Files |work=Xposé |publisher=[[Visual Imagination]] |author=Spragg, Paul |date=January 12, 2001 |archivedate=November 27, 2007}}</ref>

==References==
;Footnotes
{{Reflist|2}}

;Bibliography
*{{Cite book |title=The Complete X-Files |first1=Matt |last1=Hurwitz |first2=Chris |last2=Knowles |publisher=Insight Editions |year=2008 |isbn=1-933784-80-6 }}
* {{cite book | last= Kessenich |first= Tom | title = Examination: An Unauthorized Look at Seasons 6–9 of the X-Files  | publisher = [[Trafford Publishing]] | year = 2002 | isbn = 1-55369-812-6 }}
*{{cite book | year=2000 | author=Shapiro, Marc | title=All Things: The Official Guide to the X-Files Volume 6|publisher=Harper Prism|isbn=0-06-107611-2}}
*{{cite book | year=2009 | first1=Robert |last1=Shearman |first2=Lars |last2=Pearson | title=Wanting to Believe: A Critical Guide to The X-Files, Millennium & The Lone Gunmen|publisher=Mad Norwegian Press|isbn=0-9759446-9-X}}

== External links ==
{{wikiquote|The_X-Files|TXF Season 7}}
* {{imdb episode|0751219}}
* [http://www.tv.com/shows/the-xfiles/the-amazing-maleeni-637/ "The Amazing Maleeni"] at [[TV.com]]

{{TXF episodes|7}}

{{DEFAULTSORT:Amazing Maleeni, The}}
[[Category:2000 television episodes]]
[[Category:California in fiction]]
[[Category:Magic in television]]
[[Category:Screenplays by Vince Gilligan]]
[[Category:The X-Files (season 7) episodes]]