{{good article}}
{{Use dmy dates|date=July 2011}}
{{Infobox military conflict
| conflict    = Anglo-Egyptian Darfur Expedition
| partof      = [[World War I]]
| image       =[[File:Egypt sudan under british control.jpg|300px]]
| caption     = Darfur as a province of the Sudan in 1912
| date        =16 March – 6 November 1916
| place       = [[Darfur]], now part of [[Sudan]]
|coordinates = {{Coord|13|00|N|25|00|E|display=title}}
| result      =  {{plainlist|
*Anglo-Egyptian victory
*Darfur becomes a province of Sudan}}
| combatant1  = {{plainlist|
*{{flag|British Empire}}
*{{flagicon image|Flag of Egypt (1882-1922).svg}} [[Sultanate of Egypt]]}}
| combatant2  = {{flagicon image|Bandera Darfur.svg}} [[Sultanate of Darfur]]
| commander1  = {{plainlist|
*{{flagicon|British Empire}} [[Reginald Wingate]]
*{{flagicon|British Empire}} [[Philip James Vandeleur Kelly|Philip Kelly]]}}
| commander2  ={{plainlist|
*{{flagicon image|Bandera Darfur.svg}} [[Ali Dinar]]{{KIA}}
*{{flagicon image|Bandera Darfur.svg}} Ramadan Ali}}
| units1      =Elements of {{plainlist|
*Mounted Infantry
*Camel Corps
*13th Sudanese Infantry
*14th Sudanese Infantry
*14th Egyptian Infantry
*Arab Battalion}}
| units2   = Fur Army
| strength1   =2,000 all ranks
| strength2   ={{plainlist|
*4,000–6,000 riflemen
*unknown number of auxiliaries armed with spears}}
| casualties1 ={{plainlist|
*5 dead
*23 wounded}}
| casualties2 ={{plainlist|
*c. 231 dead
*c. 1,096 wounded.{{#tag:ref|These are the only casualties recorded; the true amount could be greater.|group=nb}}}}
| notes       =
| campaignbox = {{Campaignbox North African theatre (World War I)}}
}}
The '''Anglo-Egyptian Darfur Expedition''' of 1916 was a military operation by [[British Empire]] and the [[Sultanate of Egypt]], launched as a preemptive invasion of the [[Sultanate of Darfur]].

The [[sultan]] of [[Darfur]] [[Ali Dinar]] had been reïnstated by the British after their victory in the [[Mahdist War]] but during the [[First World War]] he grew restive, refusing his customary tribute to the [[Anglo-Egyptian Sudan|Sudanese government]] and showing partiality to the [[Ottoman Empire]] in 1915.

[[Sirdar]] [[Reginald Wingate]] then organized a force of around 2,000 men; under the command of {{nowrap|[[Lieutenant Colonel (United Kingdom)|Lieutenant-Colonel]]}} [[Philip James Vandeleur Kelly]], the force entered Darfur in March 1916 and decisively defeated the Fur Army at [[Beringia]] and occupied the capital [[El Fasher]] in May. Ali Dinar had already fled to the mountains and his attempts to negotiate a surrender were eventually broken off by the British. His location becoming known, a small force was sent after him and the sultan was killed in action in November 1916. Subsequently, Darfur was fully annexed to the British administration of the [[Anglo-Egyptian Sudan]] and remained part of [[Sudan]] upon its independence.

==Background==
{{further|History of Darfur|History of Sudan}}

In the late 19th and early 20th centuries [[Darfur]], which means "land of the Fur",<ref>{{cite web|accessdate=10 September 2013|title=Q&A: Sudan's Darfur conflict |publisher=BBC News|url=http://news.bbc.co.uk/1/hi/world/africa/3496731.stm}}</ref> was an independent country, located to the west of [[Sudan]] and east of what was then [[French Equatorial Africa]]. It is equal in size to France and can be divided into three regions: a semi [[arid]] region in the north, with very little rain, joining the [[Sahara desert]]; a central region divided in two by the [[Marrah Mountains|Jebal Marra]] [[volcano]], which rises {{convert|10,000|ft}} above [[sea level]] that is surrounded by sand and rock plains to the east and west; and a southern region which has a rich [[alluvial]] type soil and a heavy annual rainfall.<ref>{{cite web|accessdate=8 September 2013|publisher=Darfur Development|title=Darfur: Land, People, and Conflict|url=http://www.darfurdevelopment.org/node/2}}</ref>

The [[Sultanate of Darfur]] was one of the kingdoms that stretched across the centre of Africa. In 1874, it was invaded by its [[Islamic]] neighbours from the south, which resulted in the country being [[annexed]] by Egypt and joined with [[Anglo-Egyptian Sudan]]. This lasted until the [[Mahdist War]] (1881–1889), when Anglo-Egyptian suzerainty was temporarily curtailed by the forces of [[Muhammad Ahmad]], until Anglo-Egyptian control of the region was re-established following the [[battle of Omdurman]] on 2 September 1898. In 1899, [[Ali Dinar]] became the [[Sultan]] of Darfur with the approval of the then [[Sirdar]] [[Herbert Kitchener, 1st Earl Kitchener|Lord Kitchener]], on the condition that he paid an annual tribute to the British. Relations between Dinar and the Anglo-Egyptians were assisted by the Inspector-General [[Rudolf Carl von Slatin]] who had knowledge of the Darfur region and its people.<ref>{{London Gazette|accessdate=8 September 2013|date=24 October 1916|startpage=10366|issue=29800|sup=y|url=http://www.london-gazette.co.uk/issues/29800/supplements/10366}}</ref><ref name=falls>McMunn and Falls, pp.147–153</ref>

The status quo remained until disputes started over what was Darfur's exact western boundary and who had "overlordship" over its frontier districts. The British believed the delay in resolving these disputes, along with anti-government propaganda, led to a change in Dinar's attitude towards them. Their beliefs were not helped by Dinar's refusal to allow any Europeans to enter Darfur.<ref name=gazette10367/> Dinar's domestic policies caused internal unrest among the Arab portion of the population who were generally against him, or in the case of the [[Rizeigat tribe]] from the south-west Darfur, "openly hostile".<ref name=gazette10367/>

On hearing the news of war between the [[British Empire]] and [[Ottoman Empire|Turkey]], Dinar became more defiant and in April 1915 renounced his allegiance to the Anglo-Egyptian Sudan Government, declaring himself pro-Turkish and making contact with them via the [[Senussi]]. At the time, Darfur had a population of just under 1,000,000 controlled by what was described as a "slave army" of about 10,000 men.{{#tag:ref|In 1903, the Fur Army was estimated to be 1,700 cavalry and 6,000 infantry arranged into divisions, but a report prepared just prior to the expedition claimed the numbers had shrunk to 5,000 men.<ref name=d107>Daly, p.107</ref>|group=nb}} By December, affairs had deteriorated to such an extent that a small unit from the [[Egyptian Camel Transport Corps|Egyptian Camel Corps]] was dispatched to protect trade at [[Nahud]], and at the same time act as a warning against Dinar's proposed offensive against the Rizeigat tribe. Dinar instead countered the deployment of the Camel Corps detachment by moving his own troops – forty cavalry and ninety infantry – to reinforce [[Jebel el Hella]]. However, by then the British believed he was preparing for an invasion of Sudan.<ref name=gazette10367>{{London Gazette|accessdate=8 September 2013|date=24 October 1916|startpage=10367|issue=29800|sup=y|url=http://www.london-gazette.co.uk/issues/29800/supplements/10367}}</ref><ref>Daly, p.111</ref>

==Expedition==
[[File:ReginaldWingate.jpg|thumb|alt=Mustached man in officers uniform. With San-Browne belt, sword at waist, wearing gauntlets and holding his pith helmet at waist level in his right hand.|[[Sirdar]] [[Reginald Wingate]]]]

To counter the expected threat to Sudan, Sirdar [[Reginald Wingate]] gathered a force together at Nahud. The commander was British [[Lieutenant Colonel]] [[Philip James Vandeleur Kelly]], of the [[3rd The King's Own Hussars]], on secondment to the [[Egyptian Army]]. The force was composed of:
* Two companies of mounted infantry, commanded by Major Cobden, [[9th Lancers]];
* Five companies from the Camel Corps, commanded by Major Huddleston, [[Dorsetshire Regiment]];
* Six companies from the 13th and 14th Battalions, Sudanese Infantry, commanded by Major Bayly, [[Royal Welsh Fusiliers]] and Major Darwell, [[Royal Marine Light Infantry]];
* Two companies from the Arab Battalion, commanded by Major Cowan, [[Queen's Own Cameron Highlanders|Cameron Highlanders]];
* Two companies from the 14th Battalion, Egyptian Infantry;
* Two [[Ordnance BL 12-pounder 6 cwt|12-pounder]] artillery batteries, which also included two [[Maxim machine gun]]s, commanded by Major Spinks [[Royal Artillery]]; and
* One Maxim machine gun battery.<ref name=falls/><ref name=gazette10370/>

With medical and other non combat units, the force totalled around 2,000 men. Intelligence gathered supported the theory that Dinar was going to invade Sudan, so in March 1916 Wingate ordered Kelly to cross the border and occupy Jebel el Hella and [[Um Shanga]]. The two villages offered the only permanent water supplies that were on the road to [[El Fasher]], Dinar's capital.<ref name=falls/><ref name=gazette10367/><ref name=gazette10370>{{London Gazette|date=24 October 1916|accessdate=8 September 2013|issue=29800|sup=y|startpage=10370|url=http://www.london-gazette.co.uk/issues/29800/supplements/10370}}</ref>

On 16 March, five companies from the Camel Corps and mounted infantry scouts, supported by a 12-pounder artillery battery and a Maxim machine gun battery, crossed the Darfur frontier and four days later occupied Um Shanga. Their only opposition was from a small observation post which was forced to withdraw. Unexpectedly, upon arrival the Anglo-Egyptian force found the water supply at Um Shanga scarce. With the main body of his force expected to arrive that evening, having only two days supply of water with them, Kelly considered withdrawing back to Sudan. Instead, he divided his force, forming a fast moving column, consisting of thirty mounted infantry scouts, 240 men from the Camel Corps, two artillery pieces and eight Maxims, which left for Jebel el Hella at dawn on 22 March.<ref name=falls/><ref name=gazette10367/>

===Jebel el Hella===
Kelly's flying column faced only slight opposition from Fur scouts until they reached a position {{convert|4|mi}} from Jebel el Hella. There a force of 800 Fur horsemen tried to surround them, and were only prevented from doing so by Anglo-Egyptian machine gun fire. Advancing a further {{convert|2|mi}} the flying column located a large concentration of Fur troops in a wooded valley, which they engaged with artillery and machine guns. Having forced the Fur troops to disperse, the column reached Jebel el Hella at 14:15, and secured its wells without any further opposition. A small reconnaissance party was dispatched by Kelly to check on the wells at [[Lugud]] {{convert|2|mi}} away, which were occupied in strength, when the remainder of Kelly's force arrived on 26 March. During their advance, the Anglo-Egyptian casualties were described as "insignificant" while the Fur forces had lost twenty men dead and wounded. With the occupation of Jebel el Hella complete, the Anglo-Egyptians had secured one of the invasion routes into Sudan.<ref name=falls/><ref name=gazette10367/>

The main Fur Army was now located in their capital of El Fasher and was estimated to consist of between 4,000 and 6,000 riflemen with adequate supplies of ammunition.<ref name=gazette10368/> Their equipment ranged from older [[muzzleloader]] weapons, such as [[Martini-Henry]] and [[Remington rifles|Remington]] rifles and [[shotgun]]s, to spears, shields and [[chain mail]].<ref name=d107/> They were supported by an unknown number of auxiliary troops armed only with spears. At the same time Dinar was concentrating his troops from other regions in the capital. Those in contact with the Anglo-Egyptian forces and some small number of reinforcements, were instead grouped at [[Burush]] and [[Kedada]].<ref name=falls/><ref name=gazette10368>{{London Gazette|accessdate=8 September 2013|date=24 October 1916|issue=29800|sup=y|startpage=10368|url=http://www.london-gazette.co.uk/issues/29800/supplements/10368}}</ref>

[[File:Anglo-Egyptian Sudan camel soldier of the British army.jpg|thumb|alt=Sitting camel, with soldier behind. Holding a rifle at the slope and wearing a fez. Buildings in the background|An Anglo-Egyptian camel soldier.]]

===Supply problems===
Wingate believed that Dinar would avoid a large battle in the provinces, but would instead gather his troops at El Fasher, until the rainy period started, which would benefit their style of [[guerrilla]] fighting and raids on the Anglo-Egyptian column and their [[lines of communication]]. However, Kelly's immediate concern was providing water and other supplies to his troops. The Anglo-Egyptian expedition coincided with a period of no rainfall and once all the native food supplies had been used, their nearest supply point was the railhead at [[Al-Ubayyid|El Obeid]] {{convert|300|mi}} to the west. To overcome some of their supply problems Wingate started construction of a road suitable for trucks, which he had obtained to supplement his camel transport. The road would stretch from the rail line at [[Rahad]] to [[Taweisha]] then on to the capital of El Fasher, about {{convert|460|mi}}. Once completed, a journey on the road by vehicles, from the rail line to the capital, would take four days. General Sir [[Archibald Murray]], the commander of the [[Egyptian Expeditionary Force]] fighting in the [[Sinai and Palestine Campaign]], arranged for the [[Royal Flying Corps]] (RFC) to send a flight of four aeroplanes, [[Ordnance BLC 15-pounder|15-pounder guns]] and ammunition, wireless communication sets and light transport vehicles, to assist the expedition.<ref name=falls/><ref name=gazette10368/>

===April reconnaissance===
In early April, Anglo-Egyptian reconnaissance patrols located small numbers of Fur troops at [[Burush]], [[Um Eisheishat]] and Um Kedda. To their south the Fur troops deployed at Taweisha were withdrawn to Tulu. Kelly ordered a large reconnaissance force to head west to [[Abiad]]. They had three objectives: locate any water supplies, for their men and animals; disperse any Fur troops at Burush and Um Kedada; and deny the Fur forces access to the water wells at Abiad and at the same time secure the route between Nahud and El Fasher.<ref name=gazette10368/> On 3 April, the Anglo-Egyptian force, consisting of two mounted infantry companies, four artillery pieces, six Maxim machine guns and the 13th Sudanese Battalion companies, reached Burush by midday forcing out a Fur cavalry unit. The next day they continued their advance towards Um Kedada. This time they met a Fur force of 700 men, some [[trench|entrenched]] in front of the wells, but the Anglo-Egyptian guns opened fire on them forcing them to withdraw. Four days later, on 8 April, the Anglo-Egyptian reconnaissance continued, reaching Abiad early the next day only to find that the Fur troops had left the previous evening. Leaving four Sudanese infantry companies and four guns behind the remaining Anglo-Egyptian reconnaissance force continued deeper into Darfur. The garrison left
behind at Abiad was attacked on 14 and 15 April, but casualties are not recorded. By the end of the month the lines of communication road was secured, with large detachments of Anglo-Egyptian troops left at Abiad, Um Kedada, Burush, Lugud, Jebel el Hella and Um Shanga. At the same time a system of observation posts was established along the frontier from [[Gabr el Dar]] to [[Shebb]] manned by 260 friendly natives, who were issued with Remington rifles. Another 200 men belonging to the [[Kababish tribe]] occupied [[Jebel Meidob]], observing the road from Darfur to the Senussi lands in the north.<ref name=gazette10368/>

===May advance to contact===
In May, the Anglo-Egyptian forces started reinforcing their lines of communication and bringing forward  supplies to enable them to continue the advance.<ref name=gazette10368/> The Fur Army did not leave them alone and on 5 May a force of 500 men attacked the Anglo-Egyptian garrison at Abiad and four days later the [[telegraph]] post {{convert|3|mi}} east of Abiad was also attacked. On 12 May, an RFC reconnaissance aircraft flew over El Fasher dropping leaflets to the population.<ref name=falls/><ref name=gazette10369>{{London Gazette|date=24 October 1916|accessdate=8 September 2013|issue=29800|sup=y|startpage=10369|url=http://www.london-gazette.co.uk/issues/29800/supplements/10369}}</ref> The leaflets denounced Dinar and promised that once he was removed there would be religious freedom and justice for all, and that the tribal leaders would remain in position if they submitted, and there would be an end to repression.<ref>Daly, p.112</ref>

By 14 May Kelly had completed his preparations to advance to El Fasher and had gathered a force at Abiad consisting of sixty mounted infantry scouts, four companies from the Camel Corps with two Maxim machine guns of their own, eight companies from the 13th and 14th Sudanese Infantry and the Arab Battalion. They were supported by eight artillery pieces, fourteen Maxim machine guns and a [[field hospital]]. Still suffering from a shortage of water Kelly divided the force into two columns. "A" Column would be slow moving, while "B" Column would be more mobile. They would reunite at a rendezvous point {{convert|40|mi}} west of Abiad and {{convert|28|mi}} from the village of [[Meliet]], which had a well known supply of water. Reconnaissance patrols had located a small Fur garrison at Meliet and Kelly made that his first objective. Once Meliet had been captured the Anglo-Egyptians would be well positioned to attack the capital, El Fasher.<ref name=falls/><ref name=gazette10369/>

Just after 04:00 on 15 May a small unit of mounted infantry scouts, captured a Fur observation post {{convert|2|mi}} from Abiad, taking prisoner all bar two of the Fur soldiers, who managed to escape on foot. The slow moving "A" Column left Abiad on 15 May followed by the "B" Column the next day. Both columns reached the rendezvous on 17 May. The same morning an RFC reconnaissance aircraft bombed a force of around 500 Fur troops at Meliat. The next day both of Kelly's columns reached the village, which had been evacuated by the Fur troops, leaving some of their supplies behind. On 19 May, RFC reconnaissance aircraft reported there was no trace of any Fur troops within {{convert|15|mi}}. However, the Anglo-Egyptian force was exhausted from their cross country march and remained resting at Meliat.<ref name=falls/><ref name=gazette10369/>

===Battle of Beringia===
[[File:Maxim machine gun Megapixie.jpg|thumb|alt=Brass barrelled gun with black coloured rear parts. On a tripod on a wooden plinth with a wood wall behind.|British tripod mounted Maxim machine gun]]

At 05:30 on 22 May, the Anglo-Egyptian advance continued, but the nature of the country, which consisted mainly of rolling low level sand-hills, with plenty of concealed ground and scattered bushes, reduced visibility down to just a few hundred yards. From the start, large groups of Fur camaliers and cavalry were observed. So instead of sending out his scouts Kelly formed an advance guard of the mounted infantry, a camel company and four Maxim machine guns. At 10:30, the Fur troops were observed in a strong entrenched position around the village of [[Beringia, Sudan|Beringia]].<ref name=falls/><ref name=gazette10369/>

The Fur Army commander, Ramadan Ali, had established a {{convert|2,000|yd|adj=on}} crescent shaped trench, mostly concealed from the advancing Anglo-Egyptians by a [[wadi]]. Ali's plan was for them advance close enough for him to ambush them with his trench system, believing that his larger force would overrun their artillery and machine guns before they could cause any serious damage to his troops.<ref name=mac213>McGregor, p.213</ref>

The Anglo-Egyptian artillery opened fire on the Fur trenches, driving the Fur troops back. To the Anglo-Egyptian left Fur horsemen were seen gathering and they were also engaged by the artillery at a range of {{convert|1,600|yd}}. The Anglo-Egyptians [[Infantry square|formed a square]] and advanced {{convert|800|yd}}, then started digging trenches of their own. At the same time, the advance guard were ordered to man a higher position to the right front and south-west of the square. To counter a threat from Fur cavalry from his left, Kelly sent a Camel Corps company and a Maxim machine gun section to secure the higher ground there.<ref name=falls/><ref name=gazette10369/>

The Anglo-Egyptian forces were established {{convert|500|yd}} from Beringia. The main Fur Army position was {{convert|600|yd}} south of the village, extending for {{convert|1,000|yd}} to the east and west in a semi circle. Kelly's scouts could not get into a position to see all of the Fur Army positions so Kelly made the decision to launch an immediate attack. While he was organising the assault, Huddleston commanding the Camel Corps company escorting the artillery and machine guns on the right of the square, exceeded his orders and entered the village.<ref name=falls/><ref name=gazette10369/><ref name=mac213/> Exiting to the south they came under heavy fire from the Fur defenders and were forced to withdraw, pursued by some of the Fur troops. However, when they came within range of the Anglo-Egyptian square, their artillery and machine guns opened fire on their open flank. Seeing this, the remainder of the Fur Army left their trenches and attacked the southern side of the square in strength. The south of the square was manned from left to right by an artillery battery, three infantry companies, another artillery battery, one infantry company and a Maxim section. There was then a gap of around {{convert|150|yd}} before another infantry company and a Maxim section were positioned facing east. Kelly now reinforced the south of the square with two Maxim sections and a company from the Arab Battalion. The Fur attack lasted around forty minutes, but it eventually failed and the nearest they got to the square was around {{convert|10|yd}}.{{#tag:ref|MacGregor claims there attack had got within {{convert|6|yd}}.<ref name=mac213/>|group=nb}}<ref name=falls/><ref name=gazette10369/> Kelly ordered an infantry counter-attack, supported by his artillery, with the Maxim guns advancing alongside the infantry. The Fur Army broke and the survivors retreated, leaving 231 dead,{{#tag:ref|Daly gives the number of dead as 261.<ref name="Daly">Daly, p.113</ref>|group=nb}} ninety-six seriously wounded and another 1,000 less seriously wounded behind, from a force of over 3,600. Anglo-Egyptian casualties were four officers wounded, five [[Other ranks (UK)|other ranks]] dead and eighteen wounded. At 16:00, Kelly resumed his advance to El Fasher stopping for the night just short of the capital.<ref name=falls/><ref name="gazette10370" />

The Fur Army were not defeated and a force of 500 cavalry and 300 infantry attacked the Anglo-Egyptian camp at 03:00 on 23 May. Kelly's artillery opened fire with [[starshell]]s, lighting up the battleground. The attack was defeated, Fur casualties are not known, but the only Anglo Egyptian casualty was a wounded [[Gunner (rank)|gunner]]. Later that day, at 06:00 the Anglo-Egyptians were just about to break camp, when several hundred Fur troops appeared on their left flank. They were engaged and forced to withdraw by artillery, machine gun fire and aerial bombardment. At 10:00, Kelly and his mounted troops entered the capital, finding it deserted except for some women. Sultan Ali Dinar had left El Fasher accompanied by 2,000 troops after hearing about the defeat at Beringia. Captured in the city were four artillery pieces, 55,000 rounds of small arms ammunition and 4,000 rifles.<ref name=falls/><ref name="gazette10370"/>

===Dibbis and Kulme===
Sultan Ali Dinar, fled to the Jebel Marra mountains {{convert|50|mi}} to the south-west of El Fasher, with around 2,000 men,{{#tag:ref|Daly claims he fled accompanied only by a "small retinue".<ref name="Daly" />|group=nb}} Kelly's troops were unable to immediately pursue him, due to a lack of supplies and exhaustion. However, Dinar approached the Anglo Egyptians offering to discuss surrender terms. Discussions continued until 1 August, when Kelly broke off the talks, as it had become apparent that Dinar was just stalling for time. Dinar's followers had started to desert him and at that point he was only left with around 1,000 men. Kelly's troops had at that stage occupied [[Kebkebia]] {{convert|80|mi}} west of El Fasher. Huddleston, with his own Camel Corps troops and men from the 13th Sudanese Infantry, two artillery pieces and four Maxim machine guns – 200 men in total – were sent to occupy [[Dibbis]] {{convert|110|mi}} south-west of El Fasher. They reached Dibbis on 13 October, engaging a Fur force of 150 riflemen and 1,000 men armed with spears, defeating them after a short fight. Following this, Dinar once again approached the Anglo Egyptians to discuss terms. When once again it appeared Dinar was only stalling, Kelly dispatched 100 men from the 13th Sudanese Infantry to reinforce Huddleston.<ref name=falls/>

[[File:Ali Dinar.jpg|thumb|The body of [[Ali Dinar]], November 1916.]]

Huddleston had discovered Dinar was in hiding at [[Kulme]] {{convert|50|mi}} to the west. The remaining Fur troops were in a poor condition, hungry and diseased and little resistance was expected to Huddleston's troops. Without waiting for reinforcements Huddleston marched on Kulme. Occupying the village almost unopposed on 3 November, they captured several hundred prisoners and most of Dinar's remaining military stores. Some of his immediate family also surrendered at the same time.<ref name=falls/>

Dinar, avoiding battle, fled to [[Jebel Juba]] to the south-west of Kulme. Two days later, on 5 November, Huddleston with 150 men, an artillery piece and four Maxim machine guns, mounted on captured horses, set off in pursuit. Huddleston reached Dinar's camp on 6 November and opened fire at a range of  {{convert|500|yd}}. The Fur troops fled, followed by Huddleston's force, around {{convert|1|mi}} from the Fur camp. Huddleston's troops discovered the body of Dinar shot through the head.<ref name=falls/>

==Aftermath==
{{further|War in Darfur}}

After the expedition, the independent country of Darfur, and its inhabitants, became part of Sudan.<ref name=falls/> The £500,000 bill for the cost of the expedition was sent to the Egyptian Government in Cairo for payment by the Egyptian taxpayers.<ref>MacGregor, p.215</ref> The British commanders of the operation were also recognised. In 1917, Wingate became the [[List of diplomats of the United Kingdom to Egypt#High Commissioners|British High Commissioner for Egypt]].<ref>{{cite web|accessdate=10 September 2013|publisher=British Embassy Cairo|title=Previous Ambassadors|date=2 January 2013|url=http://ukinegypt.fco.gov.uk/en/about-us/our-embassy/our-ambassador/previous-ambassadors|archive-url=http://webarchive.nationalarchives.gov.uk/20130217073211/http://ukinegypt.fco.gov.uk/en/about-us/our-embassy/our-ambassador/previous-ambassadors|dead-url=yes|archive-date=17 February 2013}}</ref> The commander of the Anglo-Egyptian expedition, Kelly, became the first Governor of the Darfur province with his office located in the Sultan's palace throne room until May 1917.<ref>Daly, p.118</ref> He was then promoted to [[brigadier general]] and given command of the [[5th Mounted Brigade]], which was part of the [[Australian Mounted Division]] fighting in Palestine.<ref>{{cite web|accessdate=10 September 2013|publisher=National Archives|title=Papers of Brigadier General Philip Kelly|url=http://www.nationalarchives.gov.uk/nra/searches/subjectView.asp?ID=P43219}}</ref><ref>Preston, p.333</ref>

==See also==
{{portal|World War I}}
* [[Darfur Conflict]], the rebellion of Darfur against the Sudanese government (2003&ndash;present)

==References==
;Footnotes
{{reflist|group=nb}}

;Citations
{{reflist|2}}

;Sources
{{refbegin}}
* {{cite book|last=Daly|first=Martin William|title=Darfur's Sorrow: The Forgotten History of a Humanitarian Disaster|publisher=Cambridge University Press|location=Cambridge, England|year=2010|isbn=1139788493|url=https://books.google.com/books?id=LLGqKAZpUHkC&pg=PA113&redir_esc=y#v=onepage&q&f=false}}
* {{cite book|last=MacGregor|first=Andrew James|title=A Military History of Modern Egypt: From the Ottoman Conquest to the Ramadan War|series=Praeger Security International|publisher=Greenwood Publishing Group|location=Westport, Connecticut|year=2006|isbn=0275986012|url=https://books.google.com/books?id=WrbCziCWJPEC&dq=darfur+expedition+1916&source=gbs_navlinks_s}}
* {{cite book|last1=MacMunn|first1=Sir George Fletcher|last2=Falls|first2=Cyril|title=Military Operations, Egypt & Palestine: From the Outbreak of War with Germany to June 1917|series=Official History of the Great War Based on Official Documents by Direction of the Historical Section of the Committee of Imperial Defence|volume=Volume 1|publisher=[[HMSO|H.M. Stationery Office]]|year=1928|location=London|oclc= 817051831}}
* {{cite book|last=Preston|first=Richard Martin|title=The Desert Mounted Corps: An Account of the Cavalry Operations in Palestine and Syria, 1917–1918|publisher=Constable and Company|location=London|year=1921|isbn=9781146758833}}
{{refend}}

==Further reading==
{{refbegin}}
*{{cite journal|last=Slight|first=John|date=2010|title=British Perceptions and Responses to Sultan Ali Dinar of Darfur, 1915–16|journal=The Journal of Imperial and Commonwealth History|volume=38|issue=2|pages=237–260|issn=0308-6534}}
{{refend}}

==External links==
* [http://www.kaiserscross.com/188001/224322.html The Soldier's Burden: Darfur 1916]

{{World War I}}

[[Category:Conflicts in 1916]]
[[Category:1916 in Sudan]]
[[Category:Battles of World War I involving the United Kingdom]]
[[Category:Military history of Sudan]]
[[Category:British colonisation in Africa]]
[[Category:Egypt in World War I]]