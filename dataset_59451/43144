__NOTOC__
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Lohner B.VII, C.I
 | image=Kriegsalbum 106, Bild 32567 (BildID 15722240).jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Reconnaissance aircraft]]
 | national origin=[[Austria-Hungary]]
 | manufacturer=[[Jacob Lohner Werke und Sohn|Lohner]]
 | designer=
 | first flight=
 | introduced=
 | retired=
 | status=
 | primary user=[[KuKLFT]]
 | number built=
 | developed from= 
 | variants with their own articles=
}}
|}

The unarmed '''Lohner B.VII''' and its armed derivative the '''C.I''' were military [[reconnaissance]] aircraft produced in Austria-Hungary during [[World War I]].<ref name="Jane's">Taylor 1989, 610–11</ref> They were the ultimate developments in a family of aircraft that had begun with the [[Lohner B.I|B.I]] prior to the outbreak of war, and were the first members of that family that proved suitable for front-line service during the conflict.<ref name="Murphy 105">Murphy 2005, 105</ref> Like their predecessors, the B.VII and C.I were conventional biplanes with characteristic swept-back wings.

The '''B.VII''' appeared in August 1915 and finally provided a machine suitable for service use.<ref name="Murphy 105" /> These were used to conduct long-range reconnaissance missions over the [[Italian Campaign (World War I)|Italian Front]], as well as occasional bombing raids, carrying 80&nbsp;kg (180&nbsp;lb) of bombs internally. Many B.VIIs in operational service were equipped with machine guns on flexible mounts for the observer,<ref name="Murphy 105" /> and this led to the armed '''C.I''' version being produced at both the Lohner and Ufag factories. Aside from its factory-installed armament, the C.I also sported a streamlined cowling around the engine, whereas the B-types had their cylinders exposed to the airstream. Notable missions carried out by these aircraft included the raid on the [[Porta Volta]] power station in [[Milan]] on 14 February 1916 (a 378&nbsp;km/276&nbsp;mi round trip for 12 B.VIIs)<ref name="Murphy 106">Murphy 2005, 106</ref> and [[Julius Arigi]] sinking an Italian steamer at [[Vlorë|Valona]] in a B.VII in 1916.<ref name="Chant">Chant 2002, 56</ref>

Production of all versions ceased in 1917,<ref name="Jane's" /><ref name="Gunston 188">Gunston 1993, 188</ref> and all were withdrawn from service soon afterwards. 
<!-- ==Development== -->
<!-- ==Operational history== -->

==Variants==
* '''B.VII''' - unarmed version with 110&nbsp;kW (150&nbsp;hp) or 120&nbsp;kW (160&nbsp;hp) Austro Daimler engine (73 built)
* '''C.I''' - version with 120&nbsp;kW (160&nbsp;hp) Austro Daimler engine and armed with single machine gun on trainable mount for observer (40 built)
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (B.VII)==
{{aerospecs
|ref=<!-- reference -->Grosz 2002
|met or eng?=<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->met
|crew=Two, pilot and observer
|capacity=
|length m=9.50
|length ft=31
|length in=2
|span m=15.40
|span ft=50
|span in=6
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=3.75
|height ft=12
|height in=4
|wing area sqm=44.0
|wing area sqft=473
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=913
|empty weight lb=2,000
|gross weight kg=
|gross weight lb=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|eng1 number=1
|eng1 type=Austro-Daimler
|eng1 kw=<!-- prop engines -->110
|eng1 hp=<!-- prop engines -->150
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|max speed kmh=
|max speed mph=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->6
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=1.8
|climb rate ftmin=350
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
* {{cite book |last= Chant |first= Christopher |title=Austro-Hungarian Aces of World War 1 |year=2002 |publisher=Osprey |location= Oxford |pages= }}
* {{cite book |last= Grosz |first= Peter M. |title=Austro-Hungarian Army Aircraft of World War One |year=2002 |publisher=Flying Machine Press |location= Colorado |pages= }}
* {{cite book |last= Gunston |first= Bill |title=World Encyclopedia of Aircraft Manufacturers |year=1993 |publisher=Naval Institute Press |location= Annapolis |pages= }}
* {{cite book |last= Murphy |first= Justin D. |title=Military Aircraft: Origins to 1918 |year=2005 |publisher=ABC-Clio |location= Santa Barbara |pages= }}
* {{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London |pages= }}

==Further reading==
* [http://www.flightglobal.com/pdfarchive/view/1916/1916%20-%200144.html A contemporary account] of the raid carried out on 14 February 1916, published in ''[[Flight International|Flight]]''.
<!-- ==External links== -->

{{Lohner aircraft}}
{{KuKLFT B-class designations}}
{{KuKLFT C-class designations}}
{{Aviation lists}}

[[Category:Austro-Hungarian military reconnaissance aircraft 1910–1919]]
[[Category:Lohner aircraft|B.VII]]

[[fr:Lohner C.I]]