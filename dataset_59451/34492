{{Other uses|6th Battalion (Australia)|6th Battalion, Royal Australian Regiment}}
{{good article}}
{{Use dmy dates|date=January 2017}}
{{Use Australian English|date=January 2017}}
{{Infobox military unit
|unit_name=2/6th Battalion
|image=2-6 Bn training Qld 1944.jpg
|alt=Soldiers armed with rifels and sub-machine guns occupy a defensive position during training
|caption=Three infantrymen of the Australian 2/6th Battalion training in the Watsonville area of North Queensland, April 1944
|dates=25 October 1939&nbsp;– 18 February 1946
|country=Australia
|allegiance=
|branch=[[Australian Army]]
|type=[[Infantry]]
|role=
|size=~800–900 men{{#tag:ref|By the start of World War II, the authorised strength of an Australian infantry battalion was 910 men all ranks, however, later in the war it fell to 803.<ref name=Palazzo94>Palazzo 2004 p. 94.</ref>|group=Note}}
|command_structure=[[17th Brigade (Australia)|17th Brigade]], [[6th Division (Australia)|6th Division]]
|current_commander=
|garrison=
|ceremonial_chief=
|colonel_of_the_regiment=
|nickname=
|patron=
|motto="Nothing over us"
|colors=Purple over Red
|colors_label=Colours
|march=
|mascot=
|battles=[[Second World War]]
*[[Battle of Greece|Greek campaign]]
*[[North African campaign]]
*[[Syrian campaign]]
*[[New Guinea campaign]]
*[[Aitape-Wewak campaign|Aitape–Wewak campaign]]
|anniversaries=
|decorations=
|battle_honours=
|disbanded=18 February 1946
<!-- Commanders -->
|commander1=
|commander1_label=
|commander2=
|commander2_label=
|commander3=
|commander3_label=
|notable_commanders=
<!-- Insignia -->
|identification_symbol=[[File:2nd 6th Battalion AIF UCP.PNG|90px|alt=A two-toned rectangular organisational symbol]]
|identification_symbol_label=Unit Colour Patch
|identification_symbol_2=
|identification_symbol_2_label=
|identification_symbol_3=
|identification_symbol_3_label=
|identification_symbol_4=
|identification_symbol_4_label=
}}

The '''2/6th Battalion''' was an [[infantry]] [[battalion]] of the [[Australian Army]] that served during the [[Second World War]]. Raised in October 1939 as part of the all volunteer [[Second Australian Imperial Force]], the battalion formed part of the [[6th Division (Australia)|6th Division]] and was among the first troops raised by Australia during the war. Departing Australia in early 1940, the 2/6th were deployed to the Middle East where in January 1941, it took part in the first action of the war by Australian ground forces, the [[Battle of Bardia]], which was followed by further actions around [[Battle of Tobruk (1941)|Tobruk]]. Later, the 2/6th were dispatched to take part in the [[Battle of Greece]], although their involvement in the campaign was short before they were evacuated. Some members of the battalion also subsequently fought on [[Battle of Crete|Crete]] with a composite 17th Brigade battalion, and afterwards the battalion had to be re-formed in Palestine before being sent to Syria in 1941–42, where they formed part of the Allied occupation force that was established there in the aftermath of the [[Syria–Lebanon campaign]].

In mid-1942, the battalion was withdrawn from the Middle East to help face the threat posed by the Japanese in the Pacific. A period of garrison duty was undertaken in [[Ceylon]] between March and July 1942, before they arrived back in Australia in August 1942. Following this, the 2/6th deployed to New Guinea in January 1943, fighting around [[Battle of Wau|Wau]] and then advancing towards Salamaua during the [[Salamaua–Lae campaign]]. They were withdrawn to the [[Atherton Tablelands]] for rest in September 1943 and subsequently did not see action again until later in the war, when they were committed to the [[Aitape-Wewak campaign|Aitape–Wewak campaign]] in late 1944. The 2/6th remained in New Guinea until the end of the war, and was disbanded in February 1946, after returning to Puckapunyal the previous December.

==History==

===Formation and training===
[[File:AWM 004819 Australian 2-6 and 2-7 Bns Egypt October 1940.JPG|thumb|alt=Soldiers standing on two mechanised vehicles in the desert |Soldiers from the 2/6th on [[Universal Carrier|Bren Carriers]] in Egypt, October 1940.]]
The 2/6th Battalion{{#tag:ref|The numerical designation of 2nd AIF units was prefixed by "2/", which was used to set them apart from Militia units with corresponding numerical designations.<ref>Long 1952, p. 51.</ref> |group=Note}} was raised at the [[Royal Melbourne Showgrounds]] on 25 October 1939,<ref name=AWM/> as part of the all volunteer [[Second Australian Imperial Force]], which was raised for overseas service at the start of the war.<ref>Grey 2008, p. 146.</ref> The battalion's motto was "Nothing over us",<ref>Hay 1984, frontcover.</ref> which it adopted due to a popular [[Coles Supermarkets|Coles]] advertising slogan of the time which used the words "Nothing over [[Australian pound|2/6]]".<ref name=AWM/> The colours chosen for the battalion's [[Unit Colour Patch]] (UCP) were the same as those of the [[6th Battalion (Australia)|6th Battalion]], a unit which had served during World War I before being raised as a Militia formation in 1921. These colours were purple over red, in a horizontal rectangular shape, although a border of gray was added to the UCP to distinguish the battalion from its Militia counterpart.<ref>Long 1952, pp. 321–323.</ref>

In early November, after it had started concentration, the battalion&nbsp;– consisting at that stage of just a small [[Cadre (military)|cadre]] force of officers and non-commissioned officers drawn mainly from several Militia units including the [[14th Battalion (Australia)|14th]], the [[23rd/21st Battalion (Australia)|23rd/21st]], the [[29th Battalion (Australia)|29th]], and the [[46th Battalion (Australia)|46th Battalions]]&nbsp;– was moved to [[Puckapunyal]], [[Victoria, Australia|Victoria]]. While there, it received a number of drafts of recruits and was brought up to strength.<ref>Hay 1984, pp. 1–2.</ref> With an authorised strength of around 900 personnel,<ref name=Palazzo94/> like other Australian infantry battalions of the time, the battalion was formed around a nucleus of four rifle [[Company (military unit)|companies]]&nbsp;– designated 'A' through to 'D'&nbsp;– each consisting of three [[platoon]]s.<ref>Long 1952, p. 52.</ref>

A short period of rudimentary training followed under the tutelage of members of the [[Australian Instructional Corps]].<ref>Hay 1984, p. 15.</ref> This was completed by April 1940, and that month the battalion&nbsp;– under the command of Lieutenant Colonel Arthur Godfrey,<ref name=Godfrey>{{cite web |url=http://www.awm.gov.au/units/people_8459.asp |title=VX25 Brigadier Arthur Harry Langham Godfrey, DSO |work=People |publisher=Australian War Memorial |accessdate=27 September 2014}}</ref> a [[First World War]] veteran who had previously commanded the 23rd/21st Battalion<ref>Hay 1984, p. 2.</ref>&nbsp;– embarked for the Middle East on the transport ship ''Neuralia'', departing from Port Melbourne and sailing via [[Fremantle]], [[Colombo]], [[Aden]] and the [[Suez Canal]].<ref>Hay 1984, pp. 27, 32 & 35.</ref> At this time it was attached to the [[17th Brigade (Australia)|17th Brigade]], which was assigned to the [[6th Division (Australia)|6th Division]]. Recruited from Victoria&nbsp;– although at various times the battalion's composition was boosted by recruits from other states<ref>Hay 1984, pp. 234–235</ref>&nbsp;– the 17th also consisted of the [[2/5th Battalion (Australia)|2/5th]], [[2/7th Battalion (Australia)|2/7th]] and [[2/8th Battalion (Australia)|2/8th Battalions]].<ref name=AWM/><ref>Hay 1984, p. 5.</ref>

===Middle East, Greece and Crete===
Upon their arrival in mid-May, the battalion established itself around Beit Jirja, and completed its training at various locations in Palestine and Egypt.<ref name=AWM/><ref>Hay 1984, pp. 35–36, 51.</ref> In early January 1941, the 6th Division was committed to the fighting in [[Libya]], and the 2/6th took part in the first action of the war by Australian ground forces, the [[Battle of Bardia]], during which they fought against Italian forces.<ref name=AWM/><ref>Coulthard-Clark 1998, pp. 176–177.</ref> The battalion's involvement in the battle was meant to be limited to creating a diversion for the main attack, but in the end proved to be its most costly, resulting in 22 killed and 51 wounded.<ref>Hay 1984, p. 91.</ref> This was followed by further actions around [[Battle of Tobruk (1941)|Tobruk]] later in the month,<ref>Coulthard-Clark 1998, pp. 177–178.</ref> attacking across the Bardia–Tobruk road towards the harbour through Wadi ed Delia during the 6th Division's assault.<ref>Hay 1984, p. 107.</ref> Afterwards, the 2/6th was transported to El Gazala, {{convert|45|mi|km}} west of Torbuk, where they continued the advance to [[Derna, Libya|Derna]] and beyond in late January, advancing on a two-company front during which they clashed briefly with Italians from the 86th Regiment, capturing over 400.<ref>Hay 1984, pp. 119–120.</ref> In February, the 2/6th detached personnel to garrison the towns of [[Marj|Barce]] and [[Benghazi]] before moving to Mersa Matruh, where they received new equipment, in late March 1941.<ref>Hay 1984, pp. 127–134.</ref> Casualties during this period were 24 dead and 75 wounded.<ref name="Johnston 2008, p. 242">Johnston 2008, p. 242.</ref>

In early April 1941, the 6th Division was dispatched to [[Battle of Greece|Greece]], where they fought a very brief campaign following the German invasion of that country in the middle of the month. Overwhelmed, the Allied forces were forced back over the course of several weeks during which the 2/6th took part in several desperate rearguard actions and withdrawals during which the battalion lost 28 men killed and 43 wounded.<ref name=Hay193>Hay 1984, p. 193.</ref> Finally, they were evacuated by sea at the end of the month, but amidst the confusion a large of the battalion's personnel&nbsp;– 217 personnel from all ranks<ref name=Hay193/>&nbsp;– were captured, while others were landed on Crete, instead of Alexandria in Egypt, after the ship on which they were sailing, the ''Costa Rica'', was sunk.<ref>Hay 1984, p. 186.</ref> On Crete, 13 officers and 202 other ranks from the 2/6th were organised into a 17th Brigade composite battalion along with men from the brigade's other battalions less the 2/7th.<ref>Hay 1984, pp. 184 & 193.</ref> They subsequently fought unsuccessfully to repulse the German invasion that came in May, after which many more became prisoners of war.<ref name=AWM/>

[[File:Australian Owen gun exercise, April 1944, Queensland.jpg|thumb|left|alt=A soldier advances through the bush with a submachine-gun during training |A soldier from the 2/6th with an [[Owen gun]] during an exercise in Queensland, April 1944]]

The battalion's losses in Greece and Crete were heavy, totaling 30 dead, 54 wounded and 353 captured.<ref>Johnston 2008, p. 243.</ref> As a result, the 2/6th had to be re-formed in Palestine and brought back up to strength with reinforcements before it was dispatched [[Syria]] in December 1941, to join the Allied garrison that had been established there as occupation force at the conclusion of the [[Syria–Lebanon campaign]] against the [[Vichy France|Vichy French]]. In early 1942, the Australian government decided to bring the 6th Division back to Australia to help bolster its defences following Japan's entry into the war. Consequently, in March the battalion embarked from Suez on the transport HMT ''Otranto'', bound for Australia.<ref name=AWM/><ref>Hay 1984, p. 226.</ref>

===Ceylon and New Guinea===
On its way home, the battalion&nbsp;– along with the 16th Brigade and the rest of the 17th Brigade&nbsp;– was landed on [[Ceylon]] due to the perceived threat of a Japanese invasion there. The battalion remained there for five months, constructing defences and conducting jungle training at various locations including [[Koggala|Lake Koggala]], [[Weligama]], [[Matara, Sri Lanka|Matara]], [[Tangalle]] and [[Hambantota]].<ref>Hay 1983, p. 227.</ref> After the threat of invasion passed, the battalion eventually returned to [[Melbourne, Victoria|Melbourne]] on the transport HMT ''Athlone Castle'', arriving in early August 1942, at the height of the fighting along the [[Kokoda Track campaign|Kokoda Track]] in [[Territory of Papua|Papua]].<ref name=AWM/>

A period of reorganisation and training followed as the battalion was prepared for the rigours of jungle warfare. The battalion concentrated at the [[Nagambie|Nagambie Road Camp]] in central Victoria initially, but in late September moved to [[Greta Camp|Greta]] in New South Wales.<ref>Hay 1984, p. 236.</ref> In October, after a period of intense training, the 2/6th was moved to Brisbane from where, on 13 October 1942, they embarked on the Dutch merchant ship ''Bontekoe'', bound for [[Milne Bay]] for the first of their two campaigns there against the Japanese.<ref>Hay 1984, p. 245.</ref> After arriving at Milne Bay, where the 17th Brigade was held in reserve, on 19 October they remained there until January 1943, when the battalion embarked upon the MV ''Pulganbar'' and several smaller coastal vessels and moved to [[Port Moresby]]. From Moresby, they were airlifted to Wau on 14 January.<ref>Hay 1984, p. 253.</ref><ref>Bradley 2008, p. 114.</ref> During the battalion's time around Milne Bay, they had suffered heavily from malaria and over 300 men were in hospital at the time the battalion deployed to Wau; consequently, it was severely understrength by the time it went into battle.<ref>Bradley 2008, p. 116.</ref> Nevertheless, throughout late January 1943, the battalion was heavily involved in the [[Battle of Wau]], then afterwards took part in the [[Salamaua-Lae campaign|advance on Salamaua]], during which it fought several key actions, including the fighting around [[Battle of Lababia Ridge|Lababia Ridge]] in late June, before supporting the [[landing at Nassau Bay]] and the [[Battle of Mubo]] in July and then taking part in the fighting around [[Battle of Mount Tambu|Mount Tambu]] and Komiatum Ridge in August.<ref name=AWM/><ref>Bradley 2010, pp. 124&ndash;134; 165&ndash;181; 277&ndash;279.</ref> During the fighting the battalion sustained casualties of 59 dead and 133 wounded.<ref name=Johnston244>Johnston 2008, p. 244.</ref>

[[File:AWM 055632 Aust 2-6th Inf Bn mortars New Guinea 1943.JPG|thumb|alt=Soldiers loading a 3-inch mortar |A 3-inch mortar from the 2/6th's headquarters company, New Guinea, August 1943.]]

In late September 1943, the 2/6th were withdrawn to Australia for rest, sailing from Milne Bay on a Dutch transport, the ''Bosch Fontein'', landing in Cairns.<ref>Hay 1984, p. 367.</ref> They spent the next year training at [[Shire of Herberton|Wondecla]] on the [[Atherton Tablelands]] in Queensland prior to their final campaign of the war: the [[Aitape-Wewak campaign|Aitape–Wewak campaign]]. Arriving at Aitape in December 1944, the 2/6th spent the remainder of the war&nbsp;– a period of eight months&nbsp;– carrying out a "mopping up campaign" to clear the Japanese from the surrounding areas, conducting a series of patrols and advances through the [[Torricelli Mountains|Torricelli]] and [[Prince Alexander Mountains|Prince Alexander Ranges]],<ref name=AWM/><ref>Keogh 1965, pp. 400–408.</ref> advancing to [[Maprik District|Maprik]] in the early stages of the campaign,<ref>Hay 1984, p.
422.</ref> and then helping to capture the town of Yamil {{convert|6|mi|km}} to the west, clearing a series of jungle ridges in the process<ref>Hay 1984, p. 441.</ref> before continuing the drive inland towards Ulunkohoitu in an effort to pin Japanese forces down while the 2/7th Battalion conducted a wide sweep towards Kiarivu.<ref>Hay 1984, pp. 466–468.</ref> Losses during this campaign numbered 37 dead and 85 wounded.<ref name=Johnston244/>

The war came to an end in mid-August 1945 following Japan's surrender in the wake of the [[atomic bombings of Hiroshima and Nagasaki]]. At the conclusion of the fighting, the 2/6th remained in New Guinea, concentrating in the area around [[Wewak]]. The battalion's strength was slowly reduced as personnel were repatriated back to Australia individually for demobilisation based upon a formal points system.<ref name=Hay473>Hay 1984, p. 473.</ref> On 13 December 1945, the battalion's remaining personnel sailed for Australia, eventually returning to Puckapunyal. As the battalion's personnel were slowly demobilised or transferred out to other units its strength decreased rapidly until it was finally was disbanded on 18 February 1946.<ref name=AWM>{{cite web|url=http://www.awm.gov.au/units/unit_11257.asp|title=2/6th Battalion |work=Second World War, 1939–1945 units|publisher=Australian War Memorial|accessdate=24 April 2009}}</ref> Those personnel who were not discharged were transferred to other units for further service.<ref name=Hay473/>

During the war, a total of 2,965 men served with the battalion<ref name="Johnston 2008, p. 242"/> of whom 179 were killed and 335 wounded.<ref name=AWM/> Members of the battalion received the following decorations: four [[Distinguished Service Order]]s, 15 [[Military Cross]]es, five [[Distinguished Conduct Medal]]s, 35 [[Military Medal]]s, and 63 [[Mentioned in Despatches|Mentions in Despatches]].<ref name=AWM/>

==Battle honours==
The 2/6th received the following [[battle honour]]s:
* [[North African campaign|North Africa]], [[Battle of Bardia|Bardia 1941]], [[Battle of Tobruk (1941)|Capture of Tobruk]], [[Battle of Greece|Greece 1941]], [[Pacific War|South-West Pacific 1942–1945]], [[Battle of Wau|Wau]], [[Battle of Lababia Ridge|Lababia Ridge]], [[Battle of Bobdubi|Bobdubi II]], [[Battle of Mubo|Mubo II]], [[Salamaua&ndash;Lae campaign|Komiatum]], [[New Guinea campaign|Liberation of Australian New Guinea]], [[Aitape-Wewak campaign|Maprik]], [[Aitape-Wewak campaign|Yamil–Ulupu]], [[Aitape-Wewak campaign|Kaboibus–Kiarivu]].<ref name=AWM/>

These honours were subsequently entrusted to the [[6th Battalion (Australia)|6th Battalion]] in 1961,<ref>Festberg 1972, p. 64.</ref> and through this link are maintained by the [[Royal Victoria Regiment]].<ref>Festberg 1972, pp. 29–30.</ref>

==Commanding officers==
The following officers commanded the 2/6th during the war:<ref name=AWM/>
* Lieutenant Colonel Arthur Harry Langham Godfrey (1939–41);<ref name=Godfrey/>
* Lieutenant Colonel [[Hugh Wrigley]] (1941–42);<ref>{{cite web |url=http://www.awm.gov.au/units/people_8343.asp |title=VX171 Colonel Hugh Wrigley, OBE, CBE, MC |work=People |publisher=Australian War Memorial |accessdate=27 September 2014}}</ref>
* Lieutenant Colonel Frederick George Wood (1942–45);<ref>{{cite web |url=http://www.awm.gov.au/units/people_7709.asp |title=VX166 Brigadier Frederick George Wood, DSO |work=People |publisher=Australian War Memorial |accessdate=27 September 2014}}</ref>
* Lieutenant Colonel David Arion Collingwood Jackson (1945).<ref>{{cite web |url=http://www.awm.gov.au/units/people_1078883.asp |title=WX32 Lieutenant Colonel David Arion Collingwood Jackson, OBE, MC |work=People |publisher=Australian War Memorial |accessdate=27 September 2014}}</ref>

==See also==
*[[Military history of Australia during World War II]]

==Notes==
;Footnotes
{{Reflist|group=Note}}

;Citations
{{Reflist|3}}

==References==
* {{cite book |last=Bradley |first=Phillip |authorlink=Phillip Bradley |title=The Battle for Wau: New Guinea's Frontline 1942–1943 |year=2008 |publisher=Cambridge University Press |location=Port Melbourne, Victoria |isbn=978-0-521-89681-8}}
* {{cite book |last=Bradley |first=Phillip  |title=To Salamaua |year=2010 |publisher=Cambridge University Press |location=Port Melbourne, Victoria |isbn=978-0-521-76390-5 }}
* {{cite book|last=Coulthard-Clark |first=Chris |title=Where Australians Fought: The Encyclopaedia of Australia's Battles |year=1998 |edition=1st |publisher=Allen & Unwin |location=St Leonards, New South Wales |isbn=1-86448-611-2 }}
* {{cite book|last=Festberg |first=Alfred |title=The Lineage of the Australian Army |year=1972 |publisher=Allara Publishing |location= Melbourne, Victoria |isbn= 978-0-85887-024-6}}
* {{cite book |last=Grey |first=Jeffrey |authorlink=Jeffrey Grey  |title=A Military History of Australia |publisher=Cambridge University Press |location=Port Melbourne, Victoria |year=2008 |edition=3rd |isbn=978-0-521-69791-0}}
* {{cite book |last=Hay |first=David |year=1984 |title=Nothing Over Us: The Story of the 2/6th Australian Infantry Battalion |publisher=Australian War Memorial |location= Canberra, Australian Capital Territory |isbn=9780642994707}}
* {{Cite book|last=Johnston|first=Mark|title=The Proud 6th: An Illustrated History of the 6th Australian Division 1939–1945|year=2008|publisher=Cambridge University Press|location=Port Melbourne, Victoria|isbn=978-0-521-51411-8}}
* {{cite book|last=Keogh|first=Eustace|title=The South West Pacific 1941–45|year=1965|publisher=Grayflower Productions |location=Melbourne, Victoria|oclc=7185705}}
* {{cite book|last=Long|first=Gavin|title=To Benghazi|year=1952|series=Australia in the War of 1939–1945. Series 1&nbsp;– Army|volume=I|edition=1st|publisher=Australian War Memorial|location=Canberra, Australian Capital Territory|url=https://www.awm.gov.au/collection/RCDIG1070200/ |accessdate=22 December 2015|oclc=18400892}}
* {{cite book|last=Palazzo|first=Albert|chapter=Organising for Jungle Warfare|title=The Foundations of Victory: The Pacific War 1943–1944|year=2004|editor=Dennis, Peter |editor2=Grey, Jeffrey |editorlink=Jeffrey Grey |publisher=Army History Unit|location=Canberra, Australian Capital Territory|url=http://www.army.gov.au/Our-history/Army-History-Unit/Chief-of-Army-History-Conference/Previous-Conference-Proceedings/~/media/Files/Our%20history/AAHU/Conference%20Papers%20and%20Images/2003/2003-The_Pacific_War_1943-1944_Part_1.ashx|pages=86&ndash;101 |isbn=978-0-646-43590-9}}

==Further reading==
* {{cite book |last=Gullett |first=Henry |year=1976 |title=Not As a Duty Only: An Infantryman's War |publisher= Melbourne University Press |location=Carlton, Victoria |isbn=9780522841060}}

{{Infantry formations of the Second Australian Imperial Force |state=collapsed}}

{{DEFAULTSORT:2 6th Battalion (Australia)}}
[[Category:Australian World War II battalions|0]]
[[Category:Military units and formations established in 1939]]
[[Category:Military units and formations disestablished in 1946]]