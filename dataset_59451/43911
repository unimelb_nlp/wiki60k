<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Yakovlev AIR-3
 | image= File:Koshits D. A. (right) and mechanic Podlesniy B. N. the aircraft Yakovlev AIR-3.jpg
| caption= D.A Koshits (r) and his mechanic B.N. Podlensky in front of their AIR-3
}}{{Infobox Aircraft Type
 | type=Two-seat [[monoplane]]
 | national origin=[[Soviet Union]]
 | manufacturer=[[Yakovlev]]
 | designer=[[Aleksandr Sergeyevich Yakovlev]]
 | first flight= (AIR-3) 17 August 1929<ref name= "OKB Yak">{{cite book|last1= Gordon|first1= Yefim|last2= Komissarov| first2 = Dmitry|last3=Komissarov|first3=Sergey|title=OKB Yakovlev|publisher= Hinkley, Midland|year= 2005|isbn= 1-85780-203-9}}</ref> (AIR-4) September 1930, (AIR-8) 1934
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built= (AIR-3) 1, (AIR-4) 5, (AIR-8) 1
 | developed from= [[Yakovlev AIR-2]]
 | variants with their own articles=
}}
|}

The '''Yakovlev AIR-3''' was a 1920s [[Soviet Union|Soviet]] two-seat general aviation [[monoplane]] designed and built by [[Aleksandr Sergeyevich Yakovlev]].<ref name="orbis" />

==Development==
Following his design of the earlier [[Yakovlev AIR-1|AIR-1]] and AIR-2, Yakovlev was taken on as a student at the [[Soviet military academies#Nikolai Zhukovsky Air Force Engineering Academy|Nikolai Zhukovsky Air Force Engineering Academy]], where he designed the AIR-3, which was similar to the earlier AIR-2 biplane but with a strut-braced high [[parasol wing]].<ref name = "OKB Yak" /> Powered by a {{convert|60|hp|kW|0|abbr=on}} [[Walter NZ-60]] radial piston engine,<ref name= "orbis" /> the AIR-3 was also known as '''''Pionerskaya Pravda''''' after the [[Pionerskaya Pravda]], a young-communist newspaper, which had raised funds for the construction from its readership.<ref name="OKB Yak"/> On 6 September 1929 the aircraft was flown non-stop between [[Mineralnye Vody]] and [[Moscow]], a distance of 1835&nbsp;km, achieving two light aircraft world records.<ref name="orbis" /><ref name="nemecek" />

In 1930 the design was refined as the '''Yakovlev AIR-4''' with a new split-axle [[landing gear]], wider cockpits fitted with entrance doors and extra fuel.<ref name="nemecek" />

One AIR-4 was modified as the '''Yakovlev AIR-4MK''' in 1933 to test nearly full-span [[Flap (aircraft)|split flaps]]. Floating wingtips, which provided roll control, were added to release as much trailing edge as possible for the flaps.<ref name="nemecek" />

A military liaison variant of the AIR-4, the '''Yakovlev AIR-8''', was also produced in 1934, fitted with a {{convert|85|hp|kW|0|abbr=on}} [[Siemens]] engine and constant chord wings of greater area.<ref name="nemecek" />

== Kozlov PS ==
After a preliminary experiment using a [[Polikarpov U-2]], Professor [[:ru:Козлов, Сергей Григорьевич (авиаконструктор)|Sergei Grigorevich Kozlov]], of the [[Nikolai Zhukovsky Air Force Engineering Academy]], modified a Yakovlev AIR-4, in 1935, to produce the '''Kozlov PS (''Prozrachnyy Samolyot'' — transparent aircraft)'''.  Fabric covering on the fuselage and wings was replaced with a transparent plastic material, called <nowiki>''Cellon'' or ''Rhodoid''</nowiki>, and the opaque structure was painted with a white paint mixed with aluminium powder. Trials with ground and airborne observations confirmed Kozlov's theories, with the bonus of excellent visibility for the crew. After the initial success, the film was found to become opaque through dirt collection and the effects of the sun, diminishing the <nowiki>''invisibility effect''</nowiki>.<ref name="OKB Yak"/><ref name="Sov X">{{cite book|title=SovietX-Planes|last1= Gordon | first1= Yefim| last2 = Gunston|first2=Bill|publisher=Hinkley, Midland|year=2000|isbn=1-85780-099-0}}</ref>

Kozlov proposed an invisible single-seat reconnaissance aircraft using the transparent plastic material, but doubts about structural strength of the material precluded development. Further studies into transparent aircraft were ordered from the experimental institute headed by [[Pyotr I. Grokhovskii]] but no more transparent aircraft were built using Kozlov's methods.<ref name="OKB Yak"/><ref name="Sov X"/>

==Variants==

===AIR-3===
:Two-seat monoplane with a {{convert|60|hp|kW|0|abbr=on}} [[Walter NZ-60]] radial piston engine developed from the [[Yakovlev AIR-2]]. Only one AIR-3 was built.<ref name="OKB Yak"/>

===AIR-4===
:Improved variant fitted with increased fuel capacity and modified landing gear. At least five AIR-4 aircraft were built.<ref name="OKB Yak"/>

===AIR-4MK===
:(MK – ''Mekhanizeerovannoye Krylo'' — mechanised wings). For research into high lift systems for approach control, a single AIR-4 (regn. CCCP-E-31) was modified with full-span split flaps, with floating wingtips rotating around transverse axles for roll control.<ref name="OKB Yak"/>

===E-31===
:An alternative designation for the AIR-4MK taken from the registration.<ref name="OKB Yak"/>

===Kozlov PS===
:A single AIR-4 was modified by Sergei G. Kozlov to demonstrate his theory on invisible aircraft.  Covered with a transparent plastic sheeting and with the interior structure and opaque parts painted silver, the PS was found to be very difficult to see at first, but accumulation of dirt and the sheeting turning opaque diminished the effect fairly rapidly.<ref name="OKB Yak"/><ref name="Sov X"/>

===AIR-8===
:Military liaison variant with a {{convert|85|hp|kW|0|abbr=on}} Siemens engine, fitted with a constant chord wing with greater area. One AIR-8 was built.<ref name="OKB Yak"/>

==Specifications (AIR-3)==
{{aerospecs
|ref=<ref name="nemecek" /> <small>The History of Soviet Aircraft from 1918</small>
|met or eng?=met
|crew=two
|capacity=
|length m=11
|length ft=36
|length in=1
|span m=7.1
|span ft=23
|span in=3
|height m=
|height ft=
|height in=
|wing area sqm=16.5
|wing area sqft=178
|empty weight kg=392
|empty weight lb=864
|gross weight kg=587
|gross weight lb=1294

|eng1 number=1
|eng1 type=[[Walter NZ-60]] radial piston engine
|eng1 kw=45
|eng1 hp=60
|eng2 number=
|eng2 type=

|max speed kmh=146
|max speed mph=91
|range km=1835
|range miles=1144
|ceiling m=4200
|ceiling ft=13776
|climb rate ms=
|climb rate ftmin=
}}
<!-- ==See also== -->
{{aircontent|
|related=
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=
}}

==References==
{{reflist|refs=
<ref name="orbis">{{cite book |title= The [[Illustrated Encyclopedia of Aircraft]] | chapter = Part Work 1982–1985 |year= 1985 | publisher= Orbis Publishing|page=3098}}</ref>
<ref name="nemecek">{{cite book |last= Nemecek |first= Vaclav |title=The History of Soviet Aircraft from 1918 |year=1986 |publisher=Willow Books |location=London |page=245 |isbn=0-00-218033-2}}</ref>
}}

==External links==
{{commons category|Yakovlev aircraft}}
* [http://www.yak.ru/ENG/FIRM/HISTMOD/air-3.php Yakovlev AIR-3]

{{Yakovlev aircraft}}

[[Category:Soviet and Russian civil utility aircraft 1920-1929]]
[[Category:Soviet and Russian sport aircraft 1920–1929]]
[[Category:Yakovlev aircraft|AIR-3]]