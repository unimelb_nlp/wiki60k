{{Good Article}}
{{Infobox sheep breed
 | name     =Boreray
 | image    =Boreray Ram.jpg
 | image_size  =250px
 | image_alt  =
 | image_caption=A Boreray ram.
 | status    =Critical
 | altname    =
 | country   =[[Scotland]]
 | distribution =[[Scotland]]
 | standard   =
 | type     =
 | use     =Conservation Grazing, Meat, Wool
 | nickname   =
 | maleweight  =45kg
 | femaleweight =30kg
 | maleheight  =55cm
 | femaleheight =55cm
 | skincolor   =
 | woolcolor   =
 | facecolor   =
 | horns    =Horned
 | note     =
}}
[[File:Harris tweed.jpg|thumb|[[Harris Tweed|Tweed]], an end product manufactured from the Boreray's wool.]]
The '''Boreray''' is a [[List of sheep breeds|breed of sheep]] originating on the [[St Kilda, Scotland|St Kilda archipelago]] off the west coast of [[Scotland]] and surviving as a [[feral]] animal on one of the islands, [[Boreray, St Kilda|Boreray]].

The breed, also known as the '''Boreray Blackface''' or '''Hebridean Blackface''',<ref name="OKState">{{cite web |url = http://www.ansi.okstate.edu/breeds/sheep/boreray|title = Boreray|work = Breeds of Livestock|publisher = [[Oklahoma State University]] Dept. of Animal Science|accessdate = 2008-07-10|archiveurl = https://web.archive.org/web/20151122101119/http://www.ansi.okstate.edu/breeds/sheep/boreray|archivedate = 22 November 2015}}</ref> was once reared for [[Lamb and mutton|meat]] and [[wool]], but is now used mainly for conservation grazing. The Boreray is one of the [[Northern European short-tailed sheep]] group of breeds.

It is the [[Rare breed (agriculture)|rarest breed]] of sheep in the [[United Kingdom]]. It is the only breed classed as "Category 2: Endangered" by the [[Rare Breeds Survival Trust]], because fewer than 300-500 are known to exist.

==St Kilda sheep==
Several types of sheep have been associated with St Kilda.  In addition to the Boreray, these include the [[Soay sheep]], a feral type from [[Soay, St Kilda|Soay]] (one of the other islands in the St Kilda archipelago), and the [[Hebridean (sheep)|Hebridean sheep]], which was formerly called the "St Kilda sheep", although the sheep it was derived from were probably not in fact from St Kilda itself.<ref>{{Cite web|url=http://hebrideansheep.org.uk/history.html |title=Hebridean Sheep History |date=2011 |accessdate=21 November 2015 |publisher=The Hebridean Sheep Society |deadurl=yes |archiveurl=https://web.archive.org/web/20151120201433/http://www.hebrideansheep.org.uk/history.html |archivedate=20 November 2015 |df= }}</ref><ref>{{Cite web |url=http://soaysheep.org/boreray.html |title=Boreray sheep  |accessdate=21 November 2015 |publisher=Soay Sheep Society}}</ref>

==History==
Until the late eighteenth century, the [[domesticated sheep]] throughout the [[Scotland|Scottish]] [[Highlands and Islands]] belonged to a type called the [[Scottish Dunface]] or Old Scottish Shortwool, which was probably similar to the sheep kept in the whole of northern and western Europe up to the [[Iron Age]]. A local variety of Dunface was kept on the two main [[St Kilda, Scotland|St Kilda]] islands of Boreray and Hirta by the crofters of the islands, who lived on Hirta, the largest island of the archipelago.<ref name=":0" />  Modern breeds descended from the Dunface include the Boreray and also the [[North Ronaldsay sheep|North Ronaldsay]] and the [[Shetland sheep|Shetland]].<ref>{{Cite book |title=A History of British Livestock Husbandry, 1700-1900 |url=https://books.google.com/books?id=zrzt7S8YesIC |publisher=Taylor & Francis |date=3 November 2005 |isbn=978-0-415-38112-3 |first=Robert |last=Trow-Smith}}</ref>

In the mid-eighteenth century the crofters' sheep were described as being "of the smallest kind", with short, coarse wool, and all having [[horn (anatomy)|horn]]s&nbsp;– usually one pair, but often two pairs. At that time there were about a thousand of these sheep on Hirta and about four hundred on Boreray.<ref name=Macaulay>{{cite book |url=https://books.google.com/books?id=kdk9AAAAcAAJ&printsec=frontcover&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false | author=Macaulay, Kenneth | year=1764 | title=The History of St Kilda | publisher=T Becket and P A De Hondt | page=129}}</ref>

In the late nineteenth century the crofters' sheep were cross-bred with [[Scottish Blackface]] sheep,<ref name=WHS>{{cite web |url=http://www.kilda.org.uk/kildanomdoc/level3p13.htm |title=Land Mammals |year=2003 |publisher=[[National Trust for Scotland]] |accessdate=22 November 2015}}</ref> which by then had replaced the Dunface throughout mainland Scotland.<ref>{{Cite web |url=http://www.hebrideansheep.org.uk/history.html |title=Scottish Dunface history |date=2 March 2011 |accessdate=7 November 2015| publisher=Hebridean Sheep Society |archiveurl=https://web.archive.org/web/20151122101420/http://www.hebrideansheep.org.uk/history.html |archivedate=22 November 2015}}</ref>

Before the evacuation of the St Kildian inhabitants, these sheep were farmed.<ref>{{Cite book | title=Introduced Mammals of the World: Their History, Distribution and Influence | last=Long | first=John L. | publisher=Csiro Publishing | year=2003 | pages=527 | url=https://books.google.co.uk/books?id=7YC3cYhGMOcC&pg=PA527&dq=boreray+sheep&hl=en&sa=X&ved=0CCAQ6AEwAGoVChMIvOGZh4v-yAIVxGAPCh3FAgvX#v=onepage&q=boreray%20sheep&f=false}}</ref> However, when the St Kilda archipelago's human inhabitants were evacuated in 1930, the sheep of Hirta were also removed and in 1932 they were replaced by Soays, which still live there as well as on Soay itself. Meanwhile, the remaining sheep on Boreray were left to become [[feral]];<ref name=WHS/> these became the only survivors of the crofters' sheep, and one of the few surviving descendants of the Dunface. This means that they are the original, unmodified sheep that used to be farmed on the island.<ref name=":3">{{Cite web |url=http://www.smallholderseries.co.uk/index.php?option=com_content&view=article&id=119:sheep-breeds&catid=29&Itemid=141#b |title=Sheep Breeds |accessdate=21 November 2015 |website=Smallholder Series}}</ref> In the 1970s half a dozen of them were exported to form the basis of a breeding population on the mainland, but the majority of Borerays still remain on the island.<ref name=":1" />

==Characteristics==
[[File:Boreray Ewes.jpg|thumb|Two Boreray [[sheep|ewe]]s, one with the less typical [[Animal coloration|darker colouring]]]]
{{further|Northern European short-tailed sheep}}

Despite being partially derived from a long-tailed breed (the Scottish Blackface), Borerays display characteristics which group them with other [[northern European short-tailed sheep]]. They are amongst the smallest sheep, with mature [[sheep|ewe]]s weighing {{Convert|28|kg|lbs|sigfig=2|abbr=on}} and standing {{Convert|55|cm|in|sigfig=2|abbr=on}} at the withers.<ref name=":3" />

They have naturally short tails, which do not require [[Docking (animal)|docking]]. They also [[Moulting|moult]] their [[Wool|fleece]] naturally, rather than having to be [[Sheep shearing|shorn]] annually, though older individuals do not moult as easily and may require additional shearing.<ref name=":0">{{Cite web |url=http://www.rarebreedsessex.co.uk/info2.cfm?info_id=110944 |title=Essex Rare Breeds&nbsp;— Boreray Sheep |accessdate=7 November 2015 |publisher=Millfields Rare Breeds |archiveurl=https://web.archive.org/web/20151122101209/http://www.rarebreedsessex.co.uk/info2.cfm?info_id=110944 |archivedate=22 November 2015}}</ref> Fleeces are grey or creamy white on the body, though darker individuals occur whose colouring is similar to the [[Soay sheep]]. Rough in quality, the wool is mostly used in the creation of [[tweeds]] or [[carpet]] yarns. A tweed is a rough-surfaced coarse cloth, typically made in Scotland. Its colour is a mix of flecked colours.<ref>{{Cite web |url=http://www.oxforddictionaries.com/definition/english/tweed?q=Tweed |title=Tweed |accessdate=21 November 2015 |publisher=Oxford English Dictionary}}</ref> The face and legs are wool-free and black and white, with the proportions varying between individuals.<ref>{{Cite web | url=http://www.bcsba.org.uk/coloured-sheep/boreray-sheep.html | title=Wool of the Boreray Sheep | accessdate=7 November 2015 | publisher=British Coloured Sheep Breeders Association}}</ref>

Both sexes of the Boreray display [[horn (anatomy)|horn]]s, formerly sometimes more than one pair, but in the modern breed always only one pair. The horns on the ewes tend to be thinner than those on the males and while they curve they do not [[spiral]] beyond 360 degrees. Mature rams can grow especially large, spiral horns which may be used for crafts such as making [[Shepherd |shepherd's crooks]].<ref name="OKState" /><ref name=":1" /> The horns on the rams have been described as "striking and majestic" by one farmer of the breed.<ref>{{Cite web |url=http://www.gaerllwyd.co.uk/boreray.html |title=Boreray Sheep Description | website=Gaerllwyd Farms |date=2009 | access-date=21 November 2015 }}</ref>

===Population===
In 1999 the population was estimated to be at less than 84, with 74 ewes. In 2002, there were between 92 and 100 animals, with 92 ewes and the male population estimated to be less than 7. In 2012, 204 ewes were registered in [[Breed registry|herdbooks]].<ref>{{Cite web | url=http://dad.fao.org/cgi-bin/EfabisWeb.cgi?sid=fa7587595582f178d00cac9f967de977,reportsreport8a_50013115 | title=Breed Information&nbsp;— UK Government | date=<!--not stated--> | accessdate=7 November 2015 | publisher=DEFRA}}</ref> According to the [[Rare Breeds Survival Trust]], there are fewer than 300-500 sheep in the UK, so they are classed as "Category 2: Endangered". They are the only breed in this category, and therefore the rarest breed of sheep in the United Kingdom.<ref name=":2">{{Cite web | url=http://www.rbst.org.uk/Our-Work/Watchlist/Watchlist2 | title=RBST Watchlist | date=2015 | accessdate=7 November 2015 | publisher=Rare Breeds Survival Trust}}</ref>

===Use in farming===
The breed was primarily reared for meat and wool, but due to its rarity it is now reared for conservation purposes, if reared at all, as most of the population is thought to be feral.<ref>{{Cite web | url=http://www.britannicrarebreeds.co.uk/breedinfo/sheep_boreray.php | title=Boreray - Sheep breed |publisher=Britannic Rare Breeds | accessdate=7 November 2015}}</ref><ref name="DADIS">{{cite web | url=http://dad.fao.org/cgi-bin/EfabisWeb.cgi?sid=fa7587595582f178d00cac9f967de977,reportsreport8a_50013115 | title=Boreray/United Kingdom | work=Breed Data Sheet | publisher=Domestic Animal Diversity Information System | accessdate=28 September 2009}}</ref>

Due to the native conditions of where it developed, the Boreray is very well suited to [[conservation grazing]], which is grazing that uses livestock to improve biodiversity and achieve nature conservation in a given area.<ref>{{Cite web |url=http://www.openspacetrust.org/whatwesave/stewardship_conservation.html | title=What is Conservation Grazing? |accessdate=21 November 2015 |publisher=Open Space Trust}}</ref> However, the Rare Breeds Survival Trust believe that this could be further capitalised on if the breed is developed further.<ref name=":1">{{Cite web |url=http://www.rbst.org.uk/Rare-and-Native-Breeds/Sheep/Boreray |title=RBST Breed profile |date= |accessdate=6 November 2015 |publisher=[[Rare Breeds Survival Trust]] |archiveurl=https://web.archive.org/web/20151122101541/http://www.rbst.org.uk/Rare-and-Native-Breeds/Sheep/Boreray |archivedate=22 November 2015}}</ref>

==See also==
* [[Fauna of Scotland]]

==References==
{{reflist|colwidth=30em}}

==External links==
* [http://www.bcsba.org.uk/coloured-sheep/boreray-sheep.html Borerays], British Coloured Sheep Breeders Association
* {{cite web | url=http://www.soaysheep.org/boreray.html | title=Boreray sheep | publisher=Soay Sheep Society: Friends of the Soay and Boreray | accessdate=2008-07-10 }}
*[http://www.soayandboreraysheep.com The Sheep of St. Kilda]
*[http://www.rbst.org.uk The Rare Breeds Survival Trust]

{{Islands of Scotland}}
{{St Kilda}}
{{British livestock|state=collapsed}}
{{portalbar|Mammals|Animals|Biology|Scotland}}

[[Category:Animal breeds on the RBST Watchlist]]
[[Category:Fauna of St Kilda, Scotland]]
[[Category:Sheep breeds]]
[[Category:Sheep breeds originating in Scotland]]
[[Category:St Kilda, Scotland]]