{{good article}}
{{infobox country at games
| NOC = BAR
| NOCname = [[Barbados Olympic Association]]
| games = Summer Olympics
| year = 2008
| flagcaption = 
| oldcode = 
| website = {{url|www.olympic.org.bb }}
| location = [[Beijing]]
| competitors = '''8'''
| sports = '''3'''
| flagbearer = [[Bradley Ally]]
| rank = 
| gold = 0
| silver = 0
| bronze = 0
| officials = 
| appearances = auto
| app_begin_year = 
| app_end_year = 
| summerappearances = 
| winterappearances = 
| seealso = {{flagIOC|BWI|1960 Summer}} (1960)
}}

'''[[Barbados]]''' sent a delegation to compete at the '''[[2008 Summer Olympics]]''' in [[Beijing]], [[People's Republic of China|China]]. The island nation made its tenth appearance as an independent nation upon its arrival in Beijing. Eight athletes across three sports and ten events represented Barbados, marking the smallest delegation in its history up to the Beijing Games. Its runners and swimmers advanced past the first rounds in their events in four of their nine events, although none advanced to their events' final rounds or medaled. The nation's flagbearer during the Beijing Games was swimmer [[Bradley Ally]].

==Background==

[[Barbados]] is the easternmost of the islands located within the [[Caribbean Sea]]. Home to 280,000 residents, Barbados was first settled by the British in the 1620s. The nation remained a British colony until it declared total independence from the [[United Kingdom]] in 1966.<ref name=cia /> The first appearance of a uniquely Barbadian delegation at the Olympic games came two years after it declared independence. At its debut, nine male athletes arrived to participate at the [[1968 Summer Olympics]] in [[Mexico City]]. Previously, Barbados (as a British colony) constituted a major part of the [[West Indies Federation]] along with [[Jamaica]] and [[Trinidad and Tobago]], which [[British West Indies at the 1960 Summer Olympics|sent a delegation]] to participate at the [[1960 Summer Olympics]] in [[Rome]].<ref name=bbc />

Between its 1968 debut and its appearance at the Beijing Olympics, Barbadian delegations appeared at every Summer Olympics except for the [[1980 Summer Olympics]], which took place in [[Moscow]] in the [[Soviet Union]].<ref name="bar-sr" /> With the exceptions of the 1968 and the 2008 Barbadian teams, every appearance by Barbados at the Olympics (as of Beijing) included at least ten athletes; its smallest delegation arrived in 2008, carrying only eight competitors. All the delegations except the one that arrived in 1968 have included female athletes.<ref name="bar-sr" /> Prior to and including the 2008 Beijing Olympics, there had been one medalist from Barbados. [[Obadele Thompson]] won a bronze medal at the [[2000 Summer Olympics]] in [[Sydney]], [[Australia]], as part of Barbados' most successful showing at any Olympic games as of 2008.<ref name="bar-sr" />

In Beijing, eight athletes participated across three sports (swimming, track and field, and sailing) in ten distinct events. [[Greg Douglas (sailor)|Gregory Douglas]], the nation's only sailor in Beijing, was the youngest participant from Barbados at 18 years old; runner [[Jade Bailey]], the only female athlete in the delegation, was the oldest, at 25 years old. There were no medalists from Barbados at Beijing.<ref name="bar8-sr" /> Swimmer [[Bradley Ally]] bore the flag of Barbados at the ceremonies.<ref name="bar-sr" />

==Athletics==
{{main article|Athletics at the 2008 Summer Olympics}}

In track and field events, there were two ways to qualify for later rounds. The first way, "qualifying by right", involves ranking high enough in one's heat. Athletes could also "qualify by result"; if they did not rank high enough in their heats but were high enough in the event's overall standings, they could also advance.

===Men's competition===
[[Andrew Hinds]] participated for the Barbadian team at the Beijing Olympics, participating in the men's 100&nbsp;meters dash. Born in 1984, he is the son of former Olympian [[Hadley Hinds]], and has been affiliated in training with the MVP Track and Field Club in [[Kingston, Jamaica|Kingston]], [[Jamaica]]. Hinds participated in the Beijing Olympics at age 24, and had not previously participated in any Olympic games prior to that.<ref name="hinds-sr" /> During the qualification round of the event, which took place on August 14, Hinds participated in the third heat. He completed the race in 10.35&nbsp;seconds, placing fifth out of the heat's eight athletes. [[Indonesia]]'s [[Suryo Agung Wibowo]] ranked behind him in sixth place (10.46&nbsp;seconds), while [[Ghana]]'s [[Aziz Zakari]] ranked ahead of him in fourth place (10.34&nbsp;seconds). Trinidad and Tobago's [[Richard Thompson (athlete)|Richard Thompson]] (10.24&nbsp;seconds) and [[France]]'s [[Martial Mbandjock]] (10.26&nbsp;seconds) led the Barbadian sprinter's heat. of the 80 athletes participating in the event, Hinds tied [[Colombia]]'s [[Daniel Grueso]] and the [[Netherlands Antilles]]' [[Churandy Martina]] for 27th place. He advanced to the quarterfinal round.<ref name=256ESPN />
[[File:AndrewHinds.png|200px|right|thumb|Andrew Hinds, who participated in the men's 100&nbsp;meters in Beijing]]
During the quarterfinal round, which took place on August 15, Hinds was placed in the second heat. Eight athletes participated in the heat, and Hinds placed fifth with a time of 10.25&nbsp;seconds. Brazil's [[Jose Carlos Moreira]] ranked directly behind him (10.32&nbsp;seconds), while Nigeria's [[Olusoji Fasuba]] ranked directly ahead of him (10.21&nbsp;seconds). Hinds' heat was led by Richard Thompson again (9.99&nbsp;seconds) and the United States' [[Tyson Gay]] (10.09&nbsp;seconds). Of the 40 athletes who advanced to quarterfinals in the event, Hinds tied Russia's [[Andrey Epishin]] for 24th place. He did not advance to later rounds.<ref name=256ESPN />

[[Ryan Brathwaite]] is a hurdler who competed for Barbados at the Beijing Olympics. Born in 1988 in the Barbadian capital city of [[Bridgetown]], Brathwaite attended [[Barton County Community College]] in rural central [[Kansas]], competing in the school's athletic programs. He competed in Beijing at age 20 in a single track event, the men's 110&nbsp;meters hurdles. Brathwaite had not previously competed at any Olympic games.<ref name="brath-sr" /> During the qualification round of his event, which took place on August 17, Brathwaite participated in the third heat against seven other athletes. He finished the race in 13.38&nbsp;seconds, placing second in the event ahead of the [[Czech Republic]]'s [[Petr Svoboda (athlete)|Petr Svoboda]] (13.43&nbsp;seconds) and behind [[Colombia]]'s [[Paulo Villar]] (13.37&nbsp;seconds). 43 athletes competed in the qualification round of the event, and 40 finished their races; of those, Brathwaite ranked third. He advanced to the quarterfinal round.<ref name=102ESPN />

Brathwaite participated in the third heat of the quarterfinal round in the men's 110&nbsp;meters hurdles, which took place on August 19. His heat included eight athletes, although one was disqualified and did not rank. With a time of 13.44&nbsp;seconds, Brathwaite ranked second in the event behind Jamaica's [[Maurice Wignall]] (13.36&nbsp;seconds) and ahead of Villar, who led Brathwaite's qualification heat (13.46&nbsp;seconds). The quarterfinal round included 32 advancing athletes, and 30 of those competitors finished their races. Of those, Brathwaite ranked tenth. He again advanced, this time to the semifinal round.<ref name=102ESPN />

Semifinals in the hurdling event took place on August 20. Brathwaite competed in the eight-person second heat. Running the race in 13.59&nbsp;seconds, Brathwaite finished in seventh place, defeating France's [[Samuel Coco-Villoin]] (13.65&nbsp;seconds) but falling behind Dutch sprinter [[Marcel van der Westen]] (13.45&nbsp;seconds). The heat was led by the [[United States]]' [[David Oliver (athlete)|David Oliver]] (13.31&nbsp;seconds) and [[Poland]]'s [[Artur Noga]] (13.34&nbsp;seconds). Of the 16 semifinalists, Brathwaite ranked 12th. He did not advance to the final round.<ref name=102ESPN />

===Women's competition===

[[Jade Bailey|Jade Latoya Bailey]] participated at the Beijing Olympics in track and field events on Barbados' behalf. Born in 1983, Bailey was 25 at the time she entered the Beijing Olympics. She had not previously competed in any Olympic games or events.<ref name="bailey-sr" /> One of the events in which she participated was the women's 100&nbsp;meters dash. During the qualification round of the event, which occurred on August 15, Bailey was placed in the ninth heat against eight other athletes. She ranked second in the heat after finishing the race in 11.46&nbsp;seconds, displacing Jamaica's [[Sherone Simpson]] (11.48&nbsp;seconds) and falling behind [[Russia]]'s [[Evgeniya Polyakova]] (11.24&nbsp;seconds). Of the 85 athletes who competed in and finished this round of the event, Bailey tied [[Tahesia Harrigan]] of the [[British Virgin Islands]] for 25th place. She advanced to the quarterfinal round.<ref name=302ESPN />
[[File:2008 Summer Olympics - Womens 100m Round 2 - Heat 1.jpg|250px|thumb|Jade Bailey ''(far left)'' participating in the first heat of quarterfinals in her event at Beijing]]
The quarterfinal round of the women's 100&nbsp;meters took place on August 16. Bailey competed in the first heat, which included eight athletes. Of those, Bailey placed last in the heat after finishing the race in 11.67&nbsp;seconds. Nigeria's [[Franca Idoko]] ranked ahead of her (11.66&nbsp;seconds), while Cameroon's [[Myriam Leonie Mani]] ranked ahead of Idoko (11.65&nbsp;seconds). The heat was led by Jamaica's [[Shelly-Ann Fraser]] (11.06&nbsp;seconds) and Polyakova, who led Bailey's previous heat (11.13&nbsp;seconds). 40 athletes advanced to the quarterfinal round. Of those, the Barbadian sprinter tied Brazil's [[Lucimar Moura]] for 37th place. She did not advance to semifinals.<ref name=302ESPN />

Jade Bailey also competed in the women's 200&nbsp;meters race. During the qualification round of the event, which took place on August 18, Bailey was placed in the fourth heat, which included eight athletes. She finished in seventh place after finishing with a time of 23.62&nbsp;seconds, defeating [[Myanmar]]'s [[Lai Lai Win]] (24.37&nbsp;seconds) but falling behind Uzbekistan's [[Guzel Khubbieva]] (23.44&nbsp;seconds). Bailey's heat was led by [[Bahrain]]i sprinter [[Roqaya Al Ghasara]] (22.81&nbsp;seconds) and Great Britain's [[Emily Freeman]] (22.95&nbsp;seconds). 48 people competed in the qualification round, and 46 of those athletes finished the races. Bailey tied [[Slovenia]]'s [[Sabina Veit]] for 34th place overall. She did not advance to later rounds.<ref name=358ESPN />

<small>
;Key
*'''Note'''–Ranks given for track events are within the athlete's heat only
*'''Q''' = Qualified for the next round
*'''q''' = Qualified for the next round as a fastest loser ''or'', in field events, by position without achieving the qualifying target
*'''NR''' = National record
*N/A = Round not applicable for the event
*Bye = Athlete not required to compete in round
</small>

;Men
{|class=wikitable style="font-size:90%"
|-
!rowspan="2"|Athlete
!rowspan="2"|Event
!colspan="2"|Heat
!colspan="2"|Quarterfinal
!colspan="2"|Semifinal
!colspan="2"|Final
|-style="font-size:95%"
!Result
!Rank
!Result
!Rank
!Result
!Rank
!Result
!Rank
|-align=center
|align=left|[[Ryan Brathwaite]]
|align=left|[[Athletics at the 2008 Summer Olympics – Men's 110 metres hurdles|110 m hurdles]]
|13.38 '''[[Barbadian records in athletics|NR]]'''
|2 '''Q'''
|13.44
|2 '''Q'''
|13.59
|7
|colspan=2|Did not advance
|-align=center
|align=left|[[Andrew Hinds]]
|align=left|[[Athletics at the 2008 Summer Olympics – Men's 100 metres|100 m]]
|10.35
|5 '''q'''
|10.25
|5
|colspan=4|Did not advance
|}

;Women
{|class=wikitable style="font-size:90%"
|-
!rowspan="2"|Athlete
!rowspan="2"|Event
!colspan="2"|Heat
!colspan="2"|Quarterfinal
!colspan="2"|Semifinal
!colspan="2"|Final
|-style="font-size:95%"
!Result
!Rank
!Result
!Rank
!Result
!Rank
!Result
!Rank
|-align=center
|align=left rowspan=2|[[Jade Bailey]]
|align=left|[[Athletics at the 2008 Summer Olympics – Women's 100 metres|100 m]]
|11.46
|2 '''Q'''
|11.67
|8
|colspan=4|Did not advance
|-align=center
|align=left|[[Athletics at the 2008 Summer Olympics – Women's 200 metres|200 m]]
|23.62
|7
|colspan=6|Did not advance
|}

==Sailing==
{{main article|Sailing at the 2008 Summer Olympics}}

Then 18-year-old [[Canada]]-raised<ref name=biog /> [[Gregory Douglas (sailor)|Gregory Douglas]] participated in the Beijing Olympics on behalf of Barbados. He was the only Barbadian sailor to participate in the Beijing Olympics. Douglas competed in the one person dinghy event, where he was assessed using a score derived from his participation in nine distinct races.<ref name="doug-sr" /> In the first race, Douglas ranked 41st place out of 43; in the second, 43rd out of 43; in the third, 43rd out of 43; in the fourth, 34th out of 43; in the fifth, 33rd out of 43; in the sixth, 30th out of 43; in the seventh, 40th out of 43; in the eighth, 37th out of 43; and in the ninth, 41st out of 43.<ref name=734ESPN /> His total final score was 342, and his net final score was 299.<ref name="doug-sr" /> Overall, Douglas ranked 43rd out of 43, falling behind 42nd place finalist [[Adil Mohammad]] of the [[United Arab Emirates]] by 27 points, and behind gold medalist [[Paul Goodison]] of Great Britain by 236 points.<ref name=734ESPN />

;Men
{|class="wikitable" style="font-size:90%"
!rowspan="2"|Athlete
!rowspan="2"|Event
!colspan=11|Race
!rowspan=2|Net points
!rowspan=2|Final rank
|-style="font-size:95%"
!1
!2
!3
!4
!5
!6
!7
!8
!9
!10
!M*
|-align=center
|align=left|[[Gregory Douglas (sailor)|Gregory Douglas]]
|align=left|[[Sailing at the 2008 Summer Olympics – Men's Laser class|Laser]]
|41
|<s>43<s>
|43
|34
|33
|30
|40
|37
|41
|CAN 
|EL
|299
|43
|}
<small>'''M''' = Medal race; EL = Eliminated – did not advance into the medal race; CAN = Race cancelled;</small>

==Swimming==
{{main article|Swimming at the 2008 Summer Olympics}}

Qualifiers for the latter rounds of swimming events were decided on a time only basis, therefore positions shown are overall results versus competitors in all heats.

[[Bradley Ally]] was the only Barbadian swimmer at the Beijing Olympics who competed in multiple events. Born in Barbados in 1986, Ally was involved in the Pine Crest Swim Team in [[Fort Lauderdale, Florida|Fort Lauderdale]], a city in southern [[Florida]]. His first appearance at the Olympics was the [[2004 Summer Olympics]] in [[Athens]], [[Greece]], when he represented Barbados in the men's 100&nbsp;meters breaststroke; the men's 200&nbsp;meters breaststroke; the men's 200&nbsp;meters individual medley; and the men's 400&nbsp;meters individual medley. He was 17 years old at the time. Ally returned to the Olympics as a 21-year-old. One of the events to which he returned was the men's 200&nbsp;meters individual medley.<ref name="ally-sr" /> During the course of the men's 200&nbsp;meters IM, Ally was placed in the fourth heat of the August 13 preliminary round. He competed against seven other athletes in this heat. Completing the event in 1:58.57, Ally placed third ahead of British swimmer [[Liam Tancock]] (1:59.79) and behind [[Japan]]'s [[Ken Takakuwa]] (1:58.51). The heat itself was led by [[Hungary]]'s [[László Cseh]] (1:58.29). Of the 46 athletes who finished their races in this round, Ally ranked fifth. He advanced to the next round.<ref name=151ESPN />

The event's semifinal round took place later on August 13. Ally was placed in the second heat, which included seven other athletes again. During this race, he finished with a time of 1:59.53, placing fourth in the heat. Great Britain's [[James Goddard]] ranked ahead (1:59.63), while Japan's [[Takuro Fujii]] (1:59.59) ranked behind. The heat's headers included American swimmer [[Ryan Lochte]] (1:57.69) and Brazil's [[Thiago Pereira]] (1:58.06). 16 swimmers competed in the semifinal round; Ally ranked ninth, falling one position short of advancing to the final round.<ref name=151ESPN />

Bradley Ally also returned to compete in the men's 400&nbsp;meters individual medley, an event in which he had participated while at the Athens Olympics in 2004. He competed in the preliminary round on August 9, when he was placed in the second heat against seven other athletes. Ally finished the event in 4:14.01, placing third; [[Italy]]'s [[Allesio Boggiatto]] placed ahead (4:10.68), while Israel's [[Gal Nevo]] placed behind (4:14.03). László Cseh of Hungary led the heat (4:09.26). 29 competitors participated in this round of the event. Ally placed tenth, and did not advance to the final round, which occurred later that day.<ref name=257ESPN />

[[Andrei Cross]] participated for Barbados as a swimmer while the nation participated in the Beijing Olympics. Born on Barbados, Cross was affiliated with [[TeamBath]], the sports organization of [[England]]'s [[University of Bath]]. He was 24 years old when he participated at the Beijing Olympics in the men's 100&nbsp;meters breaststroke event. He had not previously competed at any Olympic games.<ref name="cross-sr" /> During the course of the August 9 preliminary round, Cross was placed in the third heat against seven other athletes. He finished the race in 1:04.57, ending up in seventh place; he defeated [[Uzbekistan]]'s [[Ivan Demyanenko]] (1:05.14), but fell behind [[Argentina]]'s [[Sergio Andres Ferreyra]] (1:03.65). The heat was led by [[Puerto Rico|Puerto Rican]] swimmer [[Daniel Velez]] (1:01.80) and [[India]]'s [[Sandeep Sejwal]] (1:02.19). 65 athletes participated in the preliminary round, and 63 finished their races. Cross ranked 55th. He did not advance to later rounds.<ref name=237ESPN />

[[Martyn Forde]] also swam on Barbados' behalf at the Beijing Olympics. Born in [[Canada]] in 1985, Forde was 23 years old when he made his appearance at the Beijing Olympics. He participated in the men's 50&nbsp;meters freestyle, which marked the first time he ever participated in an Olympic games.<ref name="forde-sr" /> The preliminary round of his event took place on August 14. Forde was placed in the seventh heat against seven other athletes. Finishing the race in 23.08&nbsp;seconds, the Barbadian swimmer placed sixth ahead of [[Josh Laban]] of the [[United States Virgin Islands]] (23.28&nbsp;seconds) but behind [[Kazakhstan]]'s [[Stanislav Kuzmin]] (22.91&nbsp;seconds). The heat was led by [[Chile]]'s [[Oliver Elliot]] (22.75&nbsp;seconds) and [[Romania]]'s [[Norbert Trandafir]] (22.80&nbsp;seconds). 97 athletes participated in the preliminary round of the event, with Forde ranking 51st. He did not advance to later rounds.<ref name=145ESPN />

[[Terrence Haynes]] represented Barbados in the men's 100&nbsp;meters freestyle while at the Beijing Olympics. He was born in Canada in 1984, and first participated on Barbados' behalf as a 19-year-old in the Athens Olympics of 2004. During the 2004 Olympics, Haynes raced in the men's 50&nbsp;meters freestyle and ranked 56th in the preliminary round. He returned to the Olympics as a 23-year-old.<ref name="haynes-sr" /> During the preliminary round of the men's 100&nbsp;meters freestyle, which took place on August 12, Haynes competed in the third heat against six other athletes. He finished the race in 50.50&nbsp;seconds, placing third. [[Alexandr Sklyar]] of Kazakhstan placed behind him (51.24&nbsp;seconds), while [[Latvia]]'s [[Romans Miloslavskis]] placed ahead (50.40&nbsp;seconds). India's [[Virdhawal Khade]] led the heat (50.07&nbsp;seconds). Of the 64 athletes who finished the preliminary round races, Haynes ranked 47th. He did not advance to later rounds.<ref name=320ESPN />

;Men
{|class=wikitable style="font-size:90%"
|-
!rowspan="2"|Athlete
!rowspan="2"|Event
!colspan="2"|Heat
!colspan="2"|Semifinal
!colspan="2"|Final
|-style="font-size:95%"
!Time
!Rank
!Time
!Rank
!Time
!Rank
|-align=center
|align=left rowspan=2|[[Bradley Ally]]
|align=left|[[Swimming at the 2008 Summer Olympics – Men's 200 metre individual medley|200 m individual medley]]
|1:58.57
|5 '''Q'''
|1:59.53
|9
|colspan=2|Did not advance
|-align=center
|align=left|[[Swimming at the 2008 Summer Olympics – Men's 400 metre individual medley|400 m individual medley]]
|4:14.01
|10
|colspan=2 {{n/a}}
|colspan=2|Did not advance
|-align=center
|align=left|[[Andrei Cross]]
|align=left|[[Swimming at the 2008 Summer Olympics – Men's 100 metre breaststroke|100 m breaststroke]]
|1:04.57
|55
|colspan=4|Did not advance
|-align=center
|align=left|[[Martyn Forde]]
|align=left|[[Swimming at the 2008 Summer Olympics – Men's 50 metre freestyle|50 m freestyle]]
|23.08
|51
|colspan=4|Did not advance
|-align=center
|align=left|[[Terrence Haynes]]
|align=left|[[Swimming at the 2008 Summer Olympics – Men's 100 metre freestyle|100 m freestyle]]
|50.50
|47
|colspan=4|Did not advance
|}

Legend: '''Q''' - Qualified

==See also==
* [[Barbados at the 2007 Pan American Games]]
* [[Barbados at the 2010 Central American and Caribbean Games]]

==References==
{{Reflist|refs=

<ref name=biog>{{cite web |url=http://www.sailing.org/biog.php?memberid=59533 |title=Gregory Douglas |author= |year=2012 |work= |publisher=[[International Sailing Federation]] |accessdate=20 May 2012}}</ref>

<ref name="doug-sr">{{cite web |url=http://www.sports-reference.com/olympics/athletes/do/gregory-douglas-1.html |title=Gregory Douglas |author= |year=2008 |work= |publisher=Sports-reference.com |accessdate=20 May 2012}}</ref>

<ref name=734ESPN>{{cite web |url=http://sports.espn.go.com/oly/summer08/results?eventId=734 |title=2008 Summer Olympics Results - Sailing - One person Dinghy Results |author= |year=2008 |work=Beijing 2008 |publisher=[[ESPN]] |accessdate=20 May 2012}}</ref>

<ref name=256ESPN>{{cite web |url=http://sports.espn.go.com/oly/summer08/results?eventId=256 |title=2008 Summer Olympics Results - Track and Field - Men's 100m Results |author= |year=2008 |work= |publisher=Sports-reference.com |accessdate=13 July 2012}}</ref>

<ref name="hinds-sr">{{cite web |url=http://www.sports-reference.com/olympics/athletes/hi/andrew-hinds-1.html |title=Andrew Hinds |author= |year=2008 |work= |publisher=Sports-reference.com |accessdate=13 July 2012}}</ref>

<ref name=358ESPN>{{cite web |url=http://sports.espn.go.com/oly/summer08/results?eventId=358 |title=2008 Summer Olympics Results - Track and Field - Women's 200m Results |author= |year=2008 |work= |publisher=ESPN |accessdate=13 July 2012}}</ref>

<ref name=302ESPN>{{cite web |url=http://sports.espn.go.com/oly/summer08/results?eventId=302 |title=2008 Summer Olympics Results - Track and Field - Women's 100m Results |author= |year=2008 |work= |publisher=ESPN |accessdate=13 July 2012}}</ref>

<ref name="bailey-sr">{{cite web |url=http://www.sports-reference.com/olympics/athletes/ba/jade-bailey-1.html |title=Jade Bailey |author= |year=2008 |work= |publisher=Sports-reference.com |accessdate=13 July 2012}}</ref>

<ref name=102ESPN>{{cite web |url=http://sports.espn.go.com/oly/summer08/results?eventId=102 |title=2008 Summer Olympics Results - Track and Field - Men's 110m Hurdles Results |author= |year=2008 |work= |publisher=ESPN |accessdate=13 July 2012}}</ref>

<ref name="brath-sr">{{cite web |url=http://www.sports-reference.com/olympics/athletes/br/ryan-brathwaite-1.html |title=Ryan Brathwaite |author= |year=2008 |work= |publisher=Sports-reference.com |accessdate=13 July 2012}}</ref>

<ref name=151ESPN>{{cite web |url=http://sports.espn.go.com/oly/summer08/results?eventId=151 |title=2008 Summer Olympics Results - Swimming - Men's 200m Individual Medley Results |author= |year=2008 |work= |publisher=ESPN |accessdate=13 July 2012}}</ref>

<ref name="ally-sr">{{cite web |url=http://www.sports-reference.com/olympics/athletes/al/bradley-ally-1.html |title=Bradley Ally |author= |year=2008 |work= |publisher=Sports-reference.com |accessdate=13 July 2012}}</ref>

<ref name=257ESPN>{{cite web |url=http://sports.espn.go.com/oly/summer08/results?eventId=257 |title=2008 Summer Olympics Results - Swimming - Men's 400m Individual Medley Results |author= |year=2008 |work= |publisher=ESPN |accessdate=13 July 2012}}</ref>

<ref name=237ESPN>{{cite web |url=http://sports.espn.go.com/oly/summer08/results?eventId=237 |title=2008 Summer Olympics Results - Swimming - Men's 100m Breaststroke Results |author= |year=2008 |work= |publisher=ESPN |accessdate=13 July 2012}}</ref>

<ref name="cross-sr">{{cite web |url=http://www.sports-reference.com/olympics/athletes/cr/andrei-cross-1.html |title=Andrei Cross |author= |year=2008 |work= |publisher=Sports-reference.com |accessdate=13 July 2012}}</ref>

<ref name=145ESPN>{{cite web |url=http://sports.espn.go.com/oly/summer08/results?eventId=145 |title=2008 Summer Olympics Results - Swimming - Men's 50m Freestyle Results |author= |year=2008 |work= |publisher=ESPN |accessdate=13 July 2012}}</ref>

<ref name="forde-sr">{{cite web |url=http://www.sports-reference.com/olympics/athletes/fo/martyn-forde-1.html |title=Martyn Forde |author= |year=2008 |work= |publisher=Sports-reference.com |accessdate=13 July 2012}}</ref>

<ref name=320ESPN>{{cite web |url=http://sports.espn.go.com/oly/summer08/results?eventId=320 |title=2008 Summer Olympics Results - Swimming - Men's 100m Freestyle Results |author= |year=2008 |work= |publisher=ESPN |accessdate=13 July 2012}}</ref>

<ref name="haynes-sr">{{cite web |url=http://www.sports-reference.com/olympics/athletes/ha/terrence-haynes-1.html |title=Terrence Haynes |author= |year=2008 |work= |publisher=Sports-reference.com |accessdate=13 July 2012}}</ref>

<ref name="bar8-sr">{{cite web |url=http://www.sports-reference.com/olympics/countries/BAR/summer/2008/ |title=Barbados at the 2008 Beijing Summer Games |author= |year=2008 |work= |publisher=Sports-reference.com |accessdate=13 July 2012}}</ref>

<ref name="bar-sr">{{cite web |url=http://www.sports-reference.com/olympics/countries/BAR/ |title=Barbados |author= |year=2008 |work= |publisher=Sports-reference.com |accessdate=13 July 2012}}</ref>

<ref name=cia>{{cite web |url=https://www.cia.gov/library/publications/the-world-factbook/geos/bb.html |title=Barbados |author= |date=20 June 2012 |work=[[World Factbook]] |publisher=[[Central Intelligence Agency]] |accessdate=13 July 2012}}</ref>

<ref name=bbc>{{cite web |url=http://www.bbc.co.uk/caribbean/news/story/2008/08/080807_olympics_factoid_westindies.shtml |title=Fact box - West Indies |author= |date=7 August 2008 |work= |publisher=[[British Broadcasting Corporation]] |accessdate=13 July 2012}}</ref>

}}

{{Nations at the 2008 Summer Olympics}}

[[Category:Nations at the 2008 Summer Olympics]]
[[Category:Barbados at the Summer Olympics by year|2008]]
[[Category:2008 in Barbadian sport|Olympics]]