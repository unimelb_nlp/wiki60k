{{Infobox journal
| cover = 
| title = Networks and Heterogeneous Media
| editor = Benedetto Piccoli
| discipline = [[Mathematics]]
| former_names =
| abbreviation = Netw. Heterog. Media
| publisher = American Institute of Mathematics
| country = United States
| frequency = Quarterly
| history = 2006-present
| openaccess = 
| license =
| impact = 0.952
| impact-year = 2013
| website = https://aimsciences.org/journals/home.jsp?journalID=9
| JSTOR =
| OCLC = 60455896
| LCCN = 2005214858
| CODEN =
| ISSN = 1556-1801
| eISSN = 1556-181X
}}
'''''Networks and Heterogeneous Media''''' is a [[peer-reviewed]] [[academic journal]] published quarterly by the [[American Institute of Mathematics]] and sponsored by the Istituto per le applicazioni del calcolo.<ref name=NHM Editorial Board>{{cite web |url=http://www.iac.rm.cnr.it/~piccoli/NHM_pubbl_BIG.pdf |title=NHM Editorial Board |publisher=[[American Institute of Mathematics]] |accessdate=2015-06-04}}</ref> The journal was established in 2006 and focuses on [[network theory|networks]], heterogeneous media, and related fields. The [[editor-in-chief]] is Benedetto Piccoli ([[Rutgers University]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in ''[[Zentralblatt MATH]]'',<ref name=MATH>{{cite web |url=http://www.zentralblatt-math.org/serials/ |title=Serials Database |publisher=[[Springer Science+Business Media]] |work=[[Zentralblatt MATH]] |accessdate=2015-06-04}}</ref> [[MathSciNet]], [[Scopus]],<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2015-06-04}}</ref> [[Current Contents]]/Physical, Chemical & Earth Sciences,<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-06-04}}</ref> and [[Science Citation Index Expanded]].<ref name=ISI/> According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 0.952.<ref name=WoS>{{cite book |year=2014 |chapter=Networks and Heterogeneous Media |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|1=https://aimsciences.org/journals/JournalAbstracted.jsp?journalID=9}}

[[Category:Mathematics journals]]
[[Category:Publications established in 2006]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Academic journals published by learned and professional societies]]