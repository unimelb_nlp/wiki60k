{{Infobox journal
| title = Herpetological Conservation and Biology
| cover = [[File:Herp Con Biol vol. 1 no. 1 cover.jpg]]
| editor = [[R. Bruce Bury]]
| discipline = [[Herpetology]]
| abbreviation = Herpetol. Conserv. Biol.
| publisher = Herpetological Conservation and Biology
| country = United States
| frequency = Triannually
| history = 2006–present
| openaccess = Yes
| license =
| impact = 0.595
| impact-year = 2014
| website = http://www.herpconbio.org
| link1 =
| link1-name =
| link2 = http://www.herpconbio.org/volumes.html
| link2-name = Online archive
| OCLC = 427887140
| LCCN = 
| CODEN =
| ISSN = 2151-0733
| eISSN =1931-7603
}}
'''''Herpetological Conservation and Biology''''' is a [[peer-reviewed]] [[open access]] [[scientific journal]] established in 2006 that covers the conservation, management, and [[natural history]] of [[reptiles]] and [[amphibian]]s. It publishes up to three regular issues per year as well as occasional [[monograph]]s.

== History and production ==
The journal was established in 2006, with the first issue appearing in September.<ref name=Newsletter2005-2007>[https://web.archive.org/web/20070630225346/http://www.herpconbio.org/newsletter2.htm Newsletters of the HCB journal committee 2005-2007]</ref> In 2012 it was included in the ''[[Journal Citation Reports]]''.<ref name>{{cite journal |last=McCallum |first=Malcolm L. |year=2012 |title=Thomson Reuters reports the first official 2-year and 5-year impact ratings for HCB |journal=Herpetological Conservation and Biology |volume=7|issue=2 |url=http://herpconbio.org/Volume_7/Issue_2/McCallum_2012.pdf}}</ref> In September 2014, the journal became an incorporated [[nonprofit corporation]].<ref name=HCBinc>{{cite journal |first1=Raymond A. |last1=Saumure |first2=Andrew D. |last2=Walde |title=Herpetological Conservation and Biology Incorporated |journal=Herpetological Conservation and Biology |volume=9 |issue=3 |year=2014 |url=http://www.herpconbio.org/Volume_9/Issue_3/Saumure_Walde_2014.pdf}}</ref>

The [[editor-in-chief]] is [[R. Bruce Bury]] ([[US Geological Survey]]). The journal is open access and does not charge [[article processing fee]]s or per-page costs to authors, without any limitations on article length. The journal has been cited as a successful model of low-cost [[academic publishing]] with production costs, paid by the editorial staff, of around US$100 per year.<ref name=Howard2011>{{cite news |url=http://chronicle.com/article/Hot-Type-Scholars-Create/126090/ |archiveurl=http://myweb.astate.edu/strauth/Scholars%20Create%20High-Impact%20Journal%20for%20About%20$100%20per%20Year%20-%20Publishing%20-%20The%20Chronicle%20of%20Higher%20Education.pdf |archivedate=2011 |author=Howard, Jennifer |date=January 30, 2011 |title= Scholars create influential journal for about $100 a year |newspaper=[[The Chronicle of Higher Education]]}}</ref><ref name=Fredette2012>{{cite web |last=Fredette |first= Michelle  |title=Rewriting the Journal |website=Campus Technology |date=August 28, 2012 |url=http://campustechnology.com/articles/2012/08/28/rewriting-the-journal.aspx}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Current Contents]]/Agriculture, Biology & Environmental Sciences<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-01-31}}</ref>
* [[Science Citation Index Expanded]]<ref name=ISI/>
* [[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2015-07-03}}</ref>
* [[The Zoological Record]]<ref name=ISI/>
* [[EMBiology]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 0.595.<ref name=WoS>{{cite book |year=2015 |chapter=Herpetological Conservation and Biology |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{reflist|30em}}

== External links ==
* {{Official website|http://www.herpconbio.org}}

[[Category:Herpetology journals]]
[[Category:Open access journals]]
[[Category:Publications established in 2006]]
[[Category:English-language journals]]