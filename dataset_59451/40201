{{Infobox artist
| honorific_prefix =
| name             = Gustave Blache III
| honorific_suffix =
| native_name      =
| native_name_lang =
| image            = Gustave-Blache-III.JPG
| image_size       =
| alt              =
| caption          =
| birth_name       =
| birth_date       = <!-- {{Birth date and age|YYYY|MM|DD}} -->1977
| birth_place      =
| death_date       = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} -->
| death_place      =
| resting_place    =
| resting_place_coordinates = <!-- {{Coord|LAT|LON|type:landmark|display=inline,title}} -->
| nationality      =
| spouse           =
| field            =
| training         =
| alma_mater       = BFA, School of Visual Arts (Savannah, GA); MFA, School of Visual Arts (New York)
| movement         =
| works            = Series Works include: Leah Chase Series (2010), Self-Portraits (2000-2008) Mop Makers Series (2006), Curtain Cleaner Series (2003), Still Life (2002-2003, Portraits (2000-2001)
| patrons          =
| awards           =
| memorials        =
| elected          =
| website          = {{URL|gustaveblache.com}}
| daao_record      = <!-- {{daao.org.au/bio/person-name/biography}} -->
| bgcolour         =
| module           =
}}

'''Gustave Blache III''' is an American figurative artist from [[New Orleans, Louisiana]], currently residing in [[Brooklyn, New York]]. He is best known for his works in series that highlight the process and unique labors of everyday society.

== Early life and education ==

Gustave Blache III was born in [[San Bernardino]], [[California]] in 1977.  In 1983 he moved to New Orleans, Louisiana. While in elementary school, Blache was invited to study twice a week at the [[New Orleans Museum of Art]] (NOMA).  Drawing pictures of plaster casts and painting copies of Old Masters provided an early understanding of the Old Masters who influence his work today.<br /><br />
Blache attended the [[New Orleans Center for Creative Arts]] (N.O.C.C.A.), a selective visual and performing arts high school with notable alumni such as jazz musicians [[Branford Marsalis|Branford]], [[Winton Marsalis|Winton]] and [[Jason Marsalis]] and actor [[Wendell Pierce]]. From 1994-1998, he attended the School of Visual Arts in Savannah, Georgia, a satellite campus of New York’s [http://www.sva.edu School of Visual Arts].  Upon receiving his Bachelor of Fine Arts from the School of Visual Arts (Savannah), Blache moved to New York to attend the School of Visual Arts main campus, where he earned an MFA in May 2000.

== Career ==

In Savannah, Blache gained recognition for his life-size figurative paintings.  This led to him receiving several commissions, reviews, and a book cover.  Savannah author [[Aberjhani]] featured Blache’s painting, The Art Spirit, as the original cover for his book, I Made My Boy Out of Poetry.<ref name="Aberjhani Book">{{cite book|last1=Aberjhani|title=I Made My Boy Out of Poetry|date=20 March 1998|publisher=iUniverse, Inc|isbn=9780595157655|url=http://redroom.com/member/aberjhani/books/i-made-my-boy-out-of-poetry#|accessdate=7 July 2014}}</ref>   The Art Spirit paid homage to one of Blache's influences, artist [[Robert Henri]]. The painting contains the book written by Robert Henri entitled The Art Spirit in the bottom right corner which is left untethered by a belt that is used to tie other books beneath into a bundle. <br /> <br />
While in graduate school in New York, Blache transitioned from the large life-size format on canvas and began working on small Masonite scraps that fellow students would discard originally as a cost cutting measure.  This change in format would then become smaller wood scraps that Blache would get from various lumber yards in Manhattan but he has since transitioned to painting on custom wood panels that he has specially prepared. The much smaller and intimate format suited him well.  Blache liked the idea of bringing the viewer closer to his smaller pocket-sized paintings versus the greater distance viewers once needed to view his large life-sized canvases.<ref name="MacCash Leah Chase NPG">{{cite web|last1=MacCash|first1=Doug|title=Leah Chase likeness enshrined in the National Portrait Gallery|url=http://www.nola.com/arts/index.ssf/2012/02/leah_chase_joins_other_famous.html|website=The Times-Picayune|publisher=The Times-Picayune|accessdate=7 July 2014}}</ref>   This new size was first displayed at his thesis show at the Tribeca Art Club in the spring of 2000.  This was a period of transition for Blache, not just in scale and surface, but in subject matter too.  He declined doing commissioned portraits (a practice that he still abides by today) and would only work with sitters of his choosing.  Blache's Brooklyn loft, in the Bushwick neighborhood became the background and setting for friends and former classmates to model for him.  This group of paintings would be used in Blache's first solo show in New York. <br /><br />
[[File:The Curtain Cleaners.jpg|thumb|right|Gustave Blache III, ''Yawning (Curtain Cleaner)'', 2003, oil on wood, 7 x 4 inches, Private Collection]]

19th Century Manhattan-based Art dealer Island Weiss, founder and owner of Island Weiss Gallery, first noticed Blache's small paintings fit well within his stable of artists and first included him in group exhibitions alongside Edgar Degas, Mary Cassatt, James McNeil Whistler and other heralded 19th Century artists.<ref name="Parker Smithsonian Article">{{cite web|last1=Parker|first1=Kelly|title=Native son's artistic journey takes him to the Smithsonian, again|url=http://www.louisianaweekly.com/native-sons-artistic-journey-takes-him-to-the-smithsonian-again/|website=The Louisiana Weekly|publisher=The Louisiana Weekly|accessdate=7 July 2014}}</ref><br />
At the age of 24, Island Weiss Gallery in New York hosted Blache’s first solo exhibition.<ref name="Island Weiss Gustave Blache">{{cite web|title=Gustave Blache|url=http://www.islandweiss.com/GBlache/Gustave.htm|website=Island Weiss Gallery|publisher=Island Weiss Gallery|accessdate=7 July 2014}}</ref>  The Woodward Gallery’s Paper 5 exhibition displayed Blache’s work alongside paintings by [[Robert Rauschenberg]], [[Andy Warhol]], [[Jasper Johns]], [[Jean Michel Basquiat]], and other notable Post-War artists.<ref name="Woodward Paper 5">{{cite web|title=Paper Invitational 5|url=http://www.woodwardgallery.net/exhibitions/ex-paper5.html|website=WoodwardGallery.net|accessdate=21 May 2014}}</ref><br />

In November 2003, Blache's solo exhibition [http://gustaveblache.com/curtain.html "The Curtain Cleaners"], opened at Cole Pratt Gallery in New Orleans, Louisiana. The Curtain Cleaners chronicled the process of laundresses hand washing curtains in Blache's Brooklyn loft. This exhibition was the first to display Blache’s use of visual journalism, a focus he acquired during his studies at the School of Visual Arts in New York.<br /><br />

Cole Pratt Gallery exhibited Blache’s next series, [http://gustaveblache.com/mopmaker.html "The Mop Makers"], in December 2006.  "The Mop Makers" depicted seeing impaired employees of the Lighthouse for the Blind making mops and brooms from hand along with other materials contracted by the government and other private corporations.  This show highlighted the craftsmanship needed to construct such items even though the employees were faced with the challenges of not having sight.  “The workers from the attended opening night, knowing they would not be able to see their likenesses on Blache’s panels. “But they wanted to be there,” Blache says. “And the gallery owner let them touch the panels.  They were able to feel the brush strokes and images.”<ref name="Miller NOA Article">{{cite news|last1=Miller|first1=Robin|title=Portrait artist's self-study joins Leah Chase image at Smithsonian|url=http://www.theneworleansadvocate.com/features/7574659-171/portrait-artists-self-study-joins-leah|accessdate=7 July 2014|publisher=The New Orleans Advocate|date=30 November 2013}}</ref> <br />
<br />
In 2007, Blache appeared in the documentary film [[Colored Frames]] that recounts the influences, inspirations and experiences of African-American artists in the art world over the past 50 years.<ref name="Colored Frames- A Visual Art Documentary">{{cite AV media|last1=Wilson|first1=Lerone|title=Colored Frames- A Visual Art Documentary|date=2007|publisher=Boondoggle Films|url=http://coloredframes.com/}}</ref>  Colored Frames also featured artists [[Benny Andrews]] and [[Ed Clark]] along with influential artdealers and scholars June Kelly, [[Danny Simmons]] and [[Mary Schmidt Campbell]]. Blache’s painting "Between the Head and the Hand" was selected for the DVD cover.<br />
<br />
[[File:Self Portraits.jpg|thumb|right|Gustave Blache III, ''Self Portrait with Checkered Scarf'', 2008, oil on wood, 9.5 x 7.75 inches, National Museum of African American History and Culture, Smithsonian Institution, Washington, DC.]] 

Blache's solo exhibition at Cole Pratt Gallery in 2008 entitled, In Shadow, featured ten self-portraits which isolated shadowed sections of his face in an effort to articulate the range of color that the shadow possesses.<ref name="Self Portrait Blache web">{{cite web|last1=Blache III|first1=Gustave|title=Self Portrait|url=http://gustaveblache.com/selfportrait.html|website=GustaveBlache.com|accessdate=21 May 2014}}</ref>  The [http://www.themckennamuseum.com George and Leah McKenna Museum] in New Orleans, Louisiana acquired two self-portraits from this exhibition, Self-Portrait in Shadow and Self-Portrait Standing at Easel.<br />
<br />

In 2013 the Smithsonian National Museum of African American History and Culture acquired Self Portrait with Checkered Scarf.<ref name="Miller NOA Article">{{cite news|last1=Miller|first1=Robin|title=Portrait artist's self-study joins Leah Chase image at Smithsonian|url=http://www.theneworleansadvocate.com/features/7574659-171/portrait-artists-self-study-joins-leah|accessdate=7 July 2014|publisher=The New Orleans Advocate|date=30 November 2013}}</ref> “The painting is part of a series created by Blache to study the effects of color, light, and shadow on the human face,” says NMAAHC Curator, Tuliza Fleming.  “The acquisition of Blache’s self-portrait helps fulfill our goal of owning and exhibition a stylistically, regionally, and culturally diverse array of contemporary art by African-American artists.”<ref name="Parker Smithsonian Article" />

        
      
        
          
=== Leah Chase ===

[[File:LeahChase.jpg|thumb|Gustave Blache III, ''Cutting Squash'', 2010, oil on wood, 8 x 10.25 inches, National Portrait Gallery, Smithsonian Institution, Washington, DC. ]]
From April 24, 2012 to September 16, 2012, the [[New Orleans Museum of Art]] exhibited Blache's most notable series up to date, Leah Chase: Paintings by Gustave Blache III.<ref name="Leah Chase NOMA">{{cite web|title=Leah Chase Paintings by Gustave Blache III|url=http://noma.org/exhibitions/detail/15/Leah-Chase-Paintings-by-Gustave-Blache-III|website=NOMA.org|accessdate=21 May 2014}}</ref> The exhibition documented national culinary star chef [[Leah Chase]] in the kitchen and the dining room in one of New Orleans’ most famous restaurants, [http://www.dookychaserestaurant.com/ Dooky Chase Restaurant]. <br /><br />

[[File:LeahChase1.jpg|thumb|left|Gustave Blache III, ''Leah Red Coat Stirring (Sketch)'', 2010, oil on wood, 8.25 x 3.5 inches, National Museum of African American History and Culture, Smithsonian Institution, Washington, DC]]
Blache was introduced to the idea of painting Leah Chase by his art representative at the time Eugene C. Daymude in the summer of 2009. The 20 small oil paintings that comprised the series detailed the day-to-day activities 92-year-old Chef Leah Chase encounters from early morning prep work to greeting guests in the dining room.  Blache was heralded for his depiction of the culinary icon for his accuracy and delicate handling of paint, color, and texture.  “Asked whether she thought the rendering was accurate, Chase, 89, said the young artist had gotten it right.  “I told him, ‘You could have made me look like Halle Berry or Lena Horne, but you made it look like me,’” she said.  The painting ''Cutting Squash'' was acquired for its permanent collection by the Smithsonian National Portrait Gallery in 2011.  “We are always looking for portraits of nationally prominent figures,” National Portrait Gallery chief curator Brandon Fortune said.  “It is a very interesting image of a woman at work, doing a very simple task, cutting squash,” . . . “But in some ways it transcends the everyday and becomes something of national significance.”  This series reinforces Blache's commitment to documenting everyday labor and showcasing these intimate processes to the public.  Another one of the Smithsonian Institution Museums, National Museum of African History and Culture, came calling for another painting of Chase from the Blache series in 2013 and acquired Leah Red Coat Stirring (Sketch) in 2013, making it the third painting from Blache to be included in the permanent collection of the Smithsonian Institution. <ref name="MacCash Leah Chase NPG">{{cite web|last1=MacCash|first1=Doug|title=Leah Chase likeness enshrined in the National Portrait Gallery|url=http://www.nola.com/arts/index.ssf/2012/02/leah_chase_joins_other_famous.html|website=The Times-Picayune|publisher=The Times-Picayune|accessdate=7 July 2014}}</ref> <br />
[[File:Chef Leah Chase and Artist Gustave Blache III.jpg|thumb|Chef Leah Chase (left) and Artist Gustave Blache III (right) standing in front of the painting Stirring Pot (Close Up) from the New Orleans Museum of Art exhibition Leah Chase: Paintings by Gustave Blache III.]]

=== Leah Chase: Exhibition Catalogue ===
The catalogue for the exhibition Leah Chase: Paintings by Gustave Blache III was published by Hudson Hills Press in the Fall of 2012.<ref name="Leah Chase Catalog">{{cite book|title=Leah Chase: Paintings by Gustave Blache III|date=Fall 2012|publisher=Hudson Hills Press|isbn=1-55595-378-6|pages=14, 15, 24, 29|url=http://www.hudsonhills.com/title_detail/349/Leah-Chase-Paintings-by-Gustave-Blache-III|accessdate=7 July 2014}}</ref>  The foreword to the catalogue was written by Miranda Lash, PhD, Curator of Modern and Contemporary Art of the New Orleans Museum of Art.  In her comments, Lash likens Blache’s work to the American realists of the early twentieth century, stating, “Although Blache in the past has been called a "contemporary impressionist," it might be more accurate to compare his work to that of these American realist painters from the early twentieth century. Henri and Sloan's interest in depicting the labor and beauty of modern life..." <br />
<br />
The introduction to the catalogue was provided by Michael Quick, PhD. Quick writes, "Blache's figure painting definitely is painting. I say that because of the freedom of his skillful brushwork, best seen in figures." <br />
<br />
[[File:Leahchase3343.jpg|thumb|left|Photograph of Banner, ''Leah Chase: Paintings by Gustave Blache III'', New Orleans Museum of Art]]
In addition to cataloging the complete Leah Chase series of paintings, the catalogue also includes several essays, written by A.P. Tureaud, Jr., E. John Bullard, Director Emeritus of the New Orleans Museum of Art, and Richard Anthony Lewis, PhD, Curator of Visual Arts, Louisiana State Museum. These essays offer critical discussion of the Leah Chase Paintings. <br />
<br />
In his essay, Richard Anthon Lewis, PhD writes, "The combination of Blache's artistic style with an established and honored subject in Leah Chase is a contemporary extension of both classical French and American Realism."  Lewis’s statement on style mirror the comments made by Miranda Lash, classifying Blache’s work in the American Realist style while also concurring with earlier critics regarding the similarities between Blache’s style and that of the Classical French. John Bullard drew stylistic connections between Blache and French artist Édouard Vuillard, stating “His [Blache’s] paintings, done in small, intimate size and reminiscent of the French artist Edouard Vuillard, are highly detailed and richly colored."

   
        
       
        
            
== Exhibitions ==

=== Solo exhibitions ===
* 2012 Leah Chase: Paintings by Gustave Blache III, One-man exhibition, New Orleans Museum of Art, New Orleans, LA<ref name="NOMA Leah Chase">{{cite web|title=LEAH CHASE PAINTINGS BY GUSTAVE BLACHE III|url=http://noma.org/exhibitions/detail/15/Leah-Chase-Paintings-by-Gustave-Blache-III|website=NOMA.com|publisher=New Orleans Museum of Art|accessdate=21 May 2014}}</ref>
* 2010 Dooky Chase Restaurant, Preview Exhibition, Le Musée de f.p.c., New Orleans, LA
* 2008 In Shadow, [http://gustaveblache.com/selfportrait.html Self Portraits], Cole Pratt Gallery, New Orleans, LA
* 2006 [http://gustaveblache.com/mopmaker.html The Mop Makers], Cole Pratt Gallery, New Orleans, LA
* 2004 [http://gustaveblache.com/stilllife.html Still Lifes], Cole Pratt Gallery, New Orleans, LA
* 2003 [http://gustaveblache.com/curtain.html The Curtain Cleaners], Cole Pratt Gallery, New Orleans, LA
* 2001 Recent Works, Island Weiss Gallery, New York, NY
* 2000 Recent Works, Kim Iocovozzi Fine Art, Savannah, GA

=== Group exhibitions ===
* 2005 Art 20, [http://www.armoryonpark.org Park Avenue Armory], New York, NY[14]
* 2004 Intimate Viewing of a Selection of 19th and 20th Century, European, and Contemporary Art,Island Weiss Gallery, New York, NY
* 2004 Selected Works, New Orleans African American Museum, New Orleans, LA
* 2004 The Art of Summer Living!, Island Weiss Gallery, New York, NY
* 2004 Online Exhibition, [http://www.franklinriehlman.com Franklin Riehlman Fine Art], New York, NY
* 2004 13th Annual American Fine Art Show, 33rd Street Armory, Philadelphia, PA
* 2003 Recent Works, Perry Nicole Fine Art, Memphis, TN
* 2002 Selected Works, Cole Pratt Gallery, New Orleans, LA
* 2002 Small Works, Perry Nicole Fine Art, Memphis, TN
* 2002 Paper Invitational 5, Woodward Gallery, New York, NY
* 2002 Selected Works, Free Biennial Show, New York, NY
* 2001 Selected Works, Arts Forum Gallery, New York, NY
* 2001 Selected Works, Island Weiss Gallery, New York, NY
* 2000 Alumni Exhibition, School of Visual Arts, New York, NY
* 2000 MFA Thesis Exhibition, Tribeca Art Club, New York, NY
* 2000 Selected Works, [http://www.societyillustrators.org Society of Illustrators], New York, NY

== References ==
{{reflist}}

{{DEFAULTSORT:Blanch, Gustave}}
[[Category:1977 births]]
[[Category:Living people]]
[[Category:School of Visual Arts alumni]]
[[Category:Artists from New Orleans]]
[[Category:American painters]]