{{Infobox character
| colour      = 
| colour text = 
| name        = 
| series      = [[David Copperfield (novel)|David Copperfield]]
| image       = Daniel Peggotty from David Copperfield.jpg
| caption     = Daniel Peggotty<br>"A hale grey-haired old man." (ch. LXIII)<br>Art by [[Frank Reynolds (artist)|Frank Reynolds]] (1910)
| first       = 
| last        = 
| cause       = 
| creator     = [[Charles Dickens]]
| portrayer   = 
| episode     = 
| nickname    = 
| alias       = 
| species     = 
| gender      = 
| occupation  = 
| title       = 
| family      = 
| significantother = 
| children    = 
| relatives   = 
| religion    = 
| nationality = British
}}
[[File:Peggotty from Charles Dickens David Copperfield art by Frank Reynolds.jpg|thumb|right|Peggotty and little David. Art by [[Frank Reynolds (artist)|Frank Reynolds]] (1910).]]

The '''Peggotty''' family are fictional characters in [[Charles Dickens]]'s 1850 [[English novel|novel]] ''[[David Copperfield (novel)|David Copperfield]]''.

==Daniel Peggotty==
[[Great Yarmouth|Yarmouth]] fisherman '''Daniel Peggotty''' is the brother of Clara. Referred to as "Mr. Peggotty", he is a fisherman and dealer in lobsters, crabs, and crawfish. He lives in a converted boat on the beach at Yarmouth with Emily, Ham, and Mrs. Gummidge. When Emily abandons them to elope with Steerforth, Daniel vows to find her. Steerforth later leaves Emily and she is re-united with Daniel. At the end of the novel Daniel, Emily, and Mrs. Gummidge resettle in Australia.

==Clara Peggotty==
'''Clara Peggotty''' is usually referred to as simply 'Peggotty' so as not to confuse her with David's mother, who is also called Clara. Peggotty is the [[Housekeeper (domestic worker)|housekeeper]] of the family home and plays a big part in David's upbringing. Peggotty is the sister of [[Great Yarmouth|Yarmouth]] fisherman Daniel Peggotty, and the aunt of Ham Peggotty and Little Em'ly.<ref name=dickens>Dickens, Charles 'David Copperfield' Published by Bradbury & Evans (1850)</ref> Early in the novel, David's aunt, [[Betsey Trotwood]], dismisses Peggotty's surname (had it been a given name) as being [[Paganism|pagan]]:

<blockquote>"Peggotty!" repeated Miss Betsey, with some indignation. "Do you mean to say, child, that any human being has gone into a Christian church, and got herself named Peggotty?"<ref name=dickens/></blockquote>

Mrs Copperfield then explains that Peggotty is known by her surname to avoid confusion with herself as they share the same first name.  As a "peggotty" or a "[[knitting nancy]]" (also known as a [[knitting loom]]) is a [[knitting]] device, the name may be a reference to Clara Peggotty's fondness for [[knitting]].

Peggotty is described as having cheeks like a red apple. Peggotty is gentle and caring, opening herself and her family to David whenever he is in need. She remains faithful to David Copperfield all her life, being like a second mother to him, never abandoning him, his mother, or his great-aunt Miss Betsey Trotwood. In her kind motherliness, Peggotty contrasts markedly with the harsh and unloving Miss Murdstone, the sister of David's cruel stepfather [[Edward Murdstone|Mr Murdstone]].<ref>[http://www.sparknotes.com/lit/copperfield/characters.html Peggotty on SparkNotes.com]</ref> 

She marries carrier Mr Barkis and is afterwards sometimes referred to as Mrs. Barkis, a name Aunt Betsey Trotwood regards as much more suitable. On her husband's death Peggotty inherits £3,000 — equivalent to about £{{formatprice|{{inflation|UK|3000|1850|r=-4}}}} in present-day terms.{{inflation-fn|UK}} After his death, she becomes Betsy Trotwood's servant and companion.

Peggotty has been played by [[Judy Cornwell]] (2000), [[Pauline Quirke]] (1999), [[Jenny McCracken]] (1986), Lila Kaye (1966), [[Elsa Vazzoler]] (1965), [[Barbara Ogilvie]] (1958), [[Jessie Ralph]] (1935), and [[Karen Caspersen]] (1922).<ref>[http://www.imdb.com/character/ch0047864/ Peggotty on the [[Internet Movie Database]]]</ref>

[[File:Mr. Peggotty and Ham from David Copperfield art by Frank Reynolds.jpg|thumb|left|Ham (right) and Daniel Peggotty]]
==Ham Peggotty==
A big and simple fisherman and boatbuilder, '''Ham Peggotty''' is the orphaned nephew of Clara and Daniel Peggotty and is the fiancé of Emily, to whom he became engaged on the visit of David Copperfield and [[James Steerforth|Steerforth]] to the boat house at Great Yarmouth. He drowns trying to rescue Steerforth during a storm at sea off Yarmouth.<ref>Michael Pointer,  ''Who's Who in Dickens'' Bison Books Ltd (1995) pg 110</ref> "He was a huge, strong fellow of six feet high, broad in proportion, and round-shouldered; but with a simpering boy's face and curly light hair that gave him quite a sheepish look. He was dressed in a canvas jacket, and a pair of such very stiff trousers that they would have stood quite as well alone, without any legs in them. And you couldn't so properly have said he wore a hat, as that he was covered in a-top, like an old building, with something pitchy."

==References==
{{Reflist}}

==External links==
{{Commons category|Peggotty}}
*[http://www.perryweb.com/Dickens/work_copper_who.shtml Who's Who in David Copperfield]
*[http://how-serendipitous.webs.com/copperfield Information, Illustrations, Analysis, Select Resources on the novel and characters]
{{David Copperfield}}

[[Category:Fictional characters introduced in 1850]]
[[Category:David Copperfield characters]]