{{Infobox food
| name = Avocado cake
| image = Mocha almond fudge avocado cake (4673005762).jpg
| caption = A mocha almond fudge [[avocado]] [[layer cake]]. Avocado is present within the layers of the cake.|alt=A mocha almond fudge avocado layer cake. Avocado is present within the layers of the cake.
| alternate_name = 
| country = 
| region = 
| national_cuisine = 
| creator = 
| year = 
| mintime = 
| maxtime = 
| type = [[Cake]]
| course = [[Dessert]]
| served = Cold or warmed
| main_ingredient = [[avocado]] fruit and cake [[batter (cooking)|batter]]
| minor_ingredient = 
| variations = 
| serving_size = 
| calories = 
| protein = 
| fat = 
| carbohydrate = 
| glycemic_index = 
| similar_dish = [[Fruitcake]]
| other = 
}}
'''Avocado cake''' is a [[cake]] prepared using [[avocado]] as a primary ingredient, together with other typical cake ingredients. The avocados can be mashed, and may be used as an ingredient in cake [[batter (cooking)|batter]], in cake toppings and alone atop a cake. Cake variations include raw avocado cake, avocado [[Chocolate brownie|brownie]]s and avocado [[cheesecake]]. Raw, uncooked versions of avocado cake can be high in [[vitamin E]] and [[essential fatty acid]]s, which are derived from avocado. Avocado-based cake toppings include avocado [[Fruit fool|fool]] and avocado crazy.

==Overview==
[[File:Sliced avocado.jpg|thumb|Avocado is a primary ingredient in avocado cake]]
[[Avocado]] is a main ingredient in avocado cake, along with other typical cake ingredients.<ref name="Darley 1993"/> Various varieties of avocados can be used. Avocado cake may have a subtle avocado flavor imbued in the dish.<ref name="Daza 2015"/> Mashed avocado can be used as an ingredient in the [[batter (cooking)|batter]] and in cake frostings and toppings.<ref name="Darley 1993"/><ref name="Castella 2010"/><ref name="Hornby 2015"/> Sliced avocado can be used to top or [[Garnish (food)|garnish]] it,<ref name="Castella 2010"/> as can other ingredients such as the [[Zest (ingredient)|zest]] of citrus fruits.<ref name="Independent.ie 2015"/>

Additional ingredients used can include yogurt, buttermilk, raisins, dates, walnuts, hazelnuts, [[allspice]], cinnamon and nutmeg<ref name="Darley 1993"/><ref name="HuffPost 2013"/> among others. Lemon juice may be used on the avocado to prevent [[Browning (food process)|browning]] from occurring.<ref name="Independent.ie 2015"/> Avocado cake can be prepared as a vegetarian and [[veganism|vegan]] dish.<ref name="Hornby 2015"/><ref name="Kumai"/> [[Chocolate cake]] and pancakes can be prepared with avocado as an ingredient in the batter.<ref name="Hornby 2015"/><ref name="HuffPost 2013"/>

==Variations==
===Raw avocado cake===
Avocado cake can be prepared as an uncooked cake using raw avocados and other raw ingredients, which are blended together into a smooth consistency and then chilled.<ref name="Independent.ie 2015"/> A food processor can be used to blend the ingredients.<ref name="Independent.ie 2015"/> Raw avocado cake prepared with a significant amount of avocado may contain substantial amounts of [[vitamin E]] and [[essential fatty acid]]s, which are derived from avocado.{{efn|"This cake is packed full of vitamin E and essential fatty acids, which help keep the skin looking young and feeling soft, because of the high avocado content."<ref name="Independent.ie 2015"/>}}

===Avocado brownies===
Avocado brownies are [[Chocolate brownie|brownie]]s prepared using avocado as a primary ingredient.<ref name="Shreeves 2015"/> The use of overripe avocados may contribute to the gooey, fudge-like consistency of the dish.<ref name="Shreeves 2015"/> Black beans can be used in the dish, and can be used in the place of flour.<ref name="Morelan 2016"/>

===Avocado cheesecake===
Avocado cheesecake is a style of [[cheesecake]] prepared using avocado as a main ingredient.<ref name="Daly 2015"/> Raw avocados may be used in its preparation,<ref name="Marshall 2015"/> and it may have a creamy texture and consistency.<ref name="Stachelski 2015"/> Avocado cheesecake was featured in an episode of the television show ''[[MasterChef]]'' in March 2015.<ref name="Daly 2015"/>

==Avocado-based cake toppings==
Avocado cakes can be topped with an avocado-based fool.<ref name="Darley 1993"/> A [[Fruit fool|fool]] is a pressed fruit mixture or fruit [[purée]] that is mixed with cream or custard.<ref name="Darley 1993"/> The term "fool" in this context dates to the 16th century, and was also a synonym for "a trifling thing of small consequence."<ref name="Darley 1993"/> 

Some milk [[sponge cake]]s can be topped with avocado crazy, a food in Sri Lankan cuisine.<ref name="Castella 2010"/><ref name="Donofrio 2009"/><ref>Donofrio 2009, [https://books.google.com/books?id=glydU3E7tbwC&pg=PR20 p. pr-20].</ref> Avocado crazy can be prepared with avocado, cream, sugar and lemon juice.<ref name="Castella 2010"/><ref name="Donofrio 2009"/> [[Rum]] may also be added.<ref name="Castella 2010"/> Avocado crazy may have a creamy texture and flavor.<ref name="Castella 2010"/>

==See also==
{{portal|Food}}
* [[Avocado toast]]
* [[List of avocado dishes]]
* [[List of cakes]]

==Notes==
{{notelist}}

==References==
{{reflist|30em|refs=
<ref name="Darley 1993">{{cite book | last=Darley | first=J.J. | title=Know and Enjoy Tropical Fruit: Tropical Fruit and Nuts: a Cornucopia | publisher=P & S Publishing | year=1993 | isbn=978-0-646-13539-7 | url=https://books.google.com/books?id=iyjrBQAAQBAJ&pg=PA9 | page=9}}</ref>
<ref name="Castella 2010">{{cite book | last=Castella | first=K. | title=A World of Cake | publisher=Storey Pub. | year=2010 | isbn=978-1-60342-576-6 | url=https://books.google.com/books?id=GLa2Gn1YRd4C&pg=PA255 | page=255}}</ref>
<ref name="Kumai">{{cite web | url=https://www.yahoo.com/food/vegan-dark-chocolate-avocado-cake-from-clean-123672938461.html | title=Vegan Dark Chocolate-Avocado Cake | publisher=[[Yahoo!]] Style | date=July 17, 2015 | accessdate=12 December 2015 | author=Kumai, Candice}}</ref>
<ref name="HuffPost 2013">{{cite web| title=10 Unexpected Ways To Eat Avocados | website=[[The Huffington Post]] | date=July 29, 2013 | url=http://www.huffingtonpost.com/2013/07/29/avocado-recipes_n_3654724.html | accessdate=December 12, 2015}}</ref>
<ref name="Daza 2015">{{cite web | last=Daza | first=Sandy | title=Take a bite–new cake discoveries in the city | website=[[Philippine Daily Inquirer]] | date=March 5, 2015 | url=http://lifestyle.inquirer.net/186280/take-a-bite-new-cake-discoveries-in-the-city | accessdate=December 12, 2015}}</ref>
<ref name="Independent.ie 2015">{{cite web | title=Raw avocado super-cake | website=[[The Independent]] | date=December 12, 2015 | url=http://www.independent.ie/life/food-drink/recipes/raw-avocado-supercake-30907046.html | accessdate=December 12, 2015}}</ref>
<ref name="Donofrio 2009">Donofrio 2009, [https://books.google.com/books?id=glydU3E7tbwC&pg=PA597 p. 597]. </ref>
<ref name="Daly 2015">{{cite web | last=Daly | first=Emma | title=MasterChef: Avocado cheesecake doesn't hit the right notes | website=[[Radio Times]] | date=March 11, 2015 | url=http://www.radiotimes.com/news/2015-03-11/masterchef-the-five-stages-of-eating-an-avocado-cheesecake | accessdate=March 8, 2016}}</ref>
<ref name="Shreeves 2015">{{cite web | last=Shreeves | first=Robin | title=Don't toss that overripe avocado! 7 ways to salvage it | website=[[Mother Nature Network]] | date=May 6, 2015 | url=http://www.mnn.com/food/healthy-eating/blogs/dont-toss-that-overripe-avocado-7-ways-to-salvage-it | accessdate=March 8, 2016}}</ref>
<ref name="Morelan 2016">{{cite web | last=Morelan | first=Jeanette | title=Unsuspecting Sweets: Most Shared Black Bean Dessert Recipes | website=[[Newton Kansan|The Kansan]] | date=February 1, 2016 | url=http://www.thekansan.com/article/ZZ/20160201/LIFESTYLE/302019903/2003/LIFESTYLE | accessdate=March 8, 2016}}</ref>
<ref name="Stachelski 2015">{{cite web | author=Stachelski, Deborah |title=Austin's Most Innovative Executive Chefs | website=The Huffington Post | date=October 23, 2015 | url=http://www.huffingtonpost.com/deborah-stachelski/austins-most-innovative-executive-chefs_b_8332410.html | accessdate=March 8, 2016}}</ref>
<ref name="Marshall 2015">{{cite web | author=Marshall, Kate | title=How to grow avocados | website=[[Stuff.co.nz]] | date=October 21, 2015 | url=http://www.stuff.co.nz/life-style/home-property/nz-gardener/72988880/how-to-grow-avocado | accessdate=March 8, 2016}}</ref>
<ref name="Hornby 2015">{{cite web | last=Hornby | first=Jane | title=Chocolate avocado cake | website=[[BBC]] Good Food | date=April 1, 2015 | url=http://www.bbcgoodfood.com/recipes/chocolate-avocado-cake | accessdate=March 8, 2016}}</ref>
}}

==Bibliography==
* {{cite book | last=Donofrio | first=B. | title=1000+ Indigenous Tasty Cusine of 23 Asian Countries | publisher=Xlibris US | year=2009 | isbn=978-1-4628-4205-6 | url=https://books.google.com/books?id=glydU3E7tbwC}}

==Further reading==
* {{cite web | last=Offaim | first=Hedai | title=The treat of the season: Go wild with avocados | website=[[Haaretz]] | date=January 24, 2015 | url=http://www.haaretz.com/jewish/food/recipes-appetizers-sides-salads/.premium-1.638453 | accessdate=December 12, 2015|quote=In cakes and salads, combined with a variety of ingredients, or just spread on fresh bread: Recipes for avocado lovers.}} 

==External links==
{{commons}}
* [http://www.epicurious.com/recipes/food/views/avocado-pound-cake-395113 Avocado pound cake]. [[Epicurious]].
* [http://www.epicurious.com/recipes/food/views/dark-chocolate-avocado-brownies-56389787 Dark chocolate avocado brownies]. Epicurious.
* [http://allrecipes.com/recipe/235847/avocado-cheesecake-with-walnut-crust/ Avocado Cheesecake with Walnut Crust]. [[Allrecipes.com]].
{{Cakes}}

[[Category:Cakes]]
[[Category:Edible Lauraceae]]

{{Good article}}