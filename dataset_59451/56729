{{Infobox journal
| title = Emergency Medicine Journal
| formernames = Archives of Emergency Medicine, Journal of Accident and Emergency Medicine
| cover = [[File:EMJ March 2015 cover.gif|200px]]
| discipline = [[Emergency medicine]]
| editor = Ellen Weber
| abbreviation = Emerg. Med. J.
| publisher = [[BMJ Group]] on behalf of the [[Royal College of Emergency Medicine]]
| country = United Kingdom
| frequency = Monthly
| history = 1983-present
| openaccess = 
| impact = 1.776
| impact-year = 2013
| website = http://emj.bmj.com/
| link1 = http://emj.bmj.com/content/current
| link1-name = Online access
| link2 = http://emj.bmj.com/content/
| link2-name = Online archive
| ISSN = 1472-0205
| eISSN = 1472-0213
| OCLC = 801060983
}}
The '''''Emergency Medicine Journal''''' is a monthly [[peer-reviewed]] [[medical journal]] that is jointly owned by the [[Royal College of Emergency Medicine]] (RCEM) and the [[BMJ Group]].<ref>http://emj.bmj.com/site/about/</ref> It is the official journal of the RCEM, as well as the [[British Association for Immediate Care]] and the [[Faculty of Pre-Hospital Care]] of the [[Royal College of Surgeons of Edinburgh]]. It covers developments in the field of [[emergency medicine|emergency]] and [[critical care medicine]] in both the hospital and pre-hospital environments.

The journal was established in March 1984 as the '''''Archives of Emergency Medicine''''' and was renamed '''''Journal of Accident and Emergency Medicine''''' in 1994, before receiving its current title in March 2000.

According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 1.776.<ref name=WoS>{{cite book |year=2014 |chapter=Emergency Medicine Journal |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{official website|http://emj.bmj.com/}}
*[https://fphc.rcsed.ac.uk Faculty of Pre-Hospital Care]

{{Emergency medicine}}

[[Category:Publications established in 1984]]
[[Category:Emergency medicine journals]]
[[Category:BMJ Group academic journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]