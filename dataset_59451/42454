<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Type II
 | image=File:Breguet II.JPG
 | caption=
}}{{Infobox Aircraft Type
 | type=Experimental aircraft
 | national origin=France
 | manufacturer=[[Louis Breguet]]
 | designer=Louis Breguet
 | first flight=5 January 1910
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=1
 | developed from=[[Breguet Type I]]
 
}}
|}
The '''Breguet Type II''' was the second fixed-wing aircraft design produced by [[Louis Breguet]]. Built during late 1909, it was soon discarded in favour of his next design, the [[Breguet Type III]]

==Design==
Like Breguet's previous design, the [[Breguet Type I]], the structure was principally of metal, although less highly-stressed parts such as the tail surfaces used wood.  It had a triangular section [[fuselage]] of wire-braced steel tube with the {{convert|55|hp|kW|abbr=on|disp=flip}} air-cooled Renault engine at the front: this drove a three-bladed propeller which was connected to the engine's [[camshaft]] and so revolved at half the speed of the engine.  The wings had pressed aluminium [[rib (aircraft)|rib]]s threaded onto a mainspar of {{convert|65|mm|in|abbr=on|2}} diameter steel tube.  These were connected by a single [[interplane strut]] on either side.  Tail surfaces consisted of a pair of horizontal surfaces, the lower carried on the rear of the fuselage and the upper by a pair of  booms running back from the centre section of the upper wing. The upper surface was  movable to achieve pitch control. A rectangular balanced rudder was mounted between the two horizontal surfaces.  In addition a pair of small horizontal stabilising surfaces were mounted at the front of the aircraft either side of the engine.  The pilot's seat was positioned halfway between the wings and the tail surfaces: a passenger seat was fitted behind the engine. The main undercarriage employed oleo-pneumatic suspension, and there was a single steerable tailwheel.<ref name=Aero58>{{cite journal|url=http://gallica.bnf.fr/ark:/12148/bpt6k65639070/f60.image  |journal=[[l'Aérophile]]|title=Le Aéroplane Louis Breguet|date=1 February 1910|pages=58–60|language=French}}</ref>

It was intended that lateral stability would be achieved by automatic differential movement of the two halves of the upper wing, this feature being the subject of a patent filed by Breguet.  In a turn, the greater speed of the outer wing would cause the [[angle of attack]] to be reduced,  so eliminating the increase in lift that the greater speed would otherwise have produced.  Lateral control was effected by a pair of mid-gap [[ailerons]] mounted on the interplane struts: these were evidently not effective and Breguet intended to use another method for lateral control in his next design.<ref name=Aero58/>

==Operational history==
Its  first recorded flight was made on 5 January 1910, when Louis Breguest made three circuits of the flying field at La Brayelle near [[Douai]].<ref>{{cite journal|journal=[[l'Aérophile]]|page=74|language=French|title=Au Jour le Jour  |url=http://gallica.bnf.fr/ark:/12148/bpt6k65639070/f76.image}}</ref> and on 16 January 1910 it made a flight of {{convert|1.5|km|mi|abbr=on}}  <ref>
{{cite magazine|magazine=[[Flight International|Flight]]|url=http://www.flightglobal.com/pdfarchive/view/1910/1910%20-%200067.html|title= M. Breguet at Douai|date= 22 January 1910}}</ref>  However, by April 1910 Breguet was flying his next design, the Type III.<ref>{{cite magazine|url=http://www.flightglobal.com/pdfarchive/view/1910/1910%20-%200311.html|title=Breguet flies Cross-Country|magazine=[[Flight International|Flight]]|date=23 April 1910 |page=309}}</ref>

==Specifications ==
{{Aircraft specs
|ref=[http://gallica.bnf.fr/ark:/12148/bpt6k65639070/f60.image ][[l'Aérophile]], 1 January 1910, pp. 58-60
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=1
|capacity=1
|length m=
|length ft=
|length in=
|length note=
|span m=
|span ft=
|span in=
|span note=
|upper span m=12.0
|upper span ft=
|upper span in=
|upper span note=

|lower span m=
|lower span ft=
|lower span in=
|lower span note=

|height m=
|height ft=
|height in=
|height note=
|wing area sqm=40
|wing area sqft=
|wing area note=

|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=580

|empty weight lb=
|empty weight note=
|gross weight kg=800
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=Renault
|eng1 type=V-8 air-cooled [[piston engine]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=55

|eng1 note=
|power original=
|thrust original=

|prop blade number=3
|prop name=
|prop dia m=2.50
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=70
|max speed mph=
|max speed kts=
|max speed note=

|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=

|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=

|power/mass=
|thrust/weight=

|more performance=

}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{reflist}}

{{Breguet aircraft}}

[[Category:Breguet aircraft|Type III]]
[[Category:French experimental aircraft 1910–1919]]
[[Category:Biplanes]]