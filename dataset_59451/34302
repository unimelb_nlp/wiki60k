{| {{Infobox ship begin}}
{{Infobox ship image
| Ship image   = [[File:SMS Westfalen LOC 25466u.jpg|300px|alt=A large warship steams at low speed; gray smoke drifts from the two smoke stacks]]
| Ship caption = SMS ''Westfalen''
}}
{{Infobox ship career
| Hide header = 
| Ship country = [[German Empire]]
| Ship flag = {{shipboxflag|German Empire|naval}}
| Ship name = 
| Ship namesake = [[Westphalia]]
| Ship owner = 
| Ship operator = 
| Ship registry = 
| Ship route = 
| Ship ordered = 
| Ship awarded = 
| Ship builder = [[AG Weser]], Bremen
| Ship original cost = 
| Ship yard number = 
| Ship way number = 
| Ship laid down = 12 August 1907
| Ship launched=1 July 1908
| Ship sponsor = 
| Ship christened = 
| Ship completed = 
| Ship acquired = 
| Ship commissioned = 16 November 1909
| Ship recommissioned = 
| Ship decommissioned = 
| Ship maiden voyage = 
| Ship in service = 
| Ship out of service = 
| Ship renamed = 
| Ship reclassified = 
| Ship refit = 
| Ship struck = 
| Ship reinstated = 
| Ship homeport = 
| Ship identification = 
| Ship motto = 
| Ship nickname = 
| Ship honours = 
| Ship honors = 
| Ship captured = 
| Ship fate = Scrapped 1924
| Ship status = 
| Ship notes = 
| Ship badge = 
}}
{{Infobox ship characteristics
| Hide header = 
| Header caption = 
| Ship class = {{sclass-|Nassau|battleship}}
| Ship tonnage = 
| Ship displacement =* Designed: {{convert|18873|t|LT|abbr=on}}
* Full load: {{convert|20535|t|LT|abbr=on|-1}}
  
| Ship tons burthen = 
| Ship length = {{convert|146.1|m|ftin|abbr=on}}
| Ship beam = {{convert|26.9|m|ftin|abbr=on}}
| Ship height = 
| Ship draft = {{convert|8.9|m|ftin|abbr=on}}
| Ship propulsion =* 3-shaft vertical triple expansion
* {{convert|16,181|kW|ihp|lk=in|abbr=on|order=flip}}
  
| Ship sail plan = 
| Ship speed =* Designed: {{convert|19|kn|lk=in}}
* Maximum: {{convert|20.2|kn}}
  
| Ship range = At {{convert|10|kn}}: {{convert|8,380|nmi|abbr=on|lk=in}}
| Ship boats = 10
| Ship complement =* Standard: 40&nbsp;officers, 968&nbsp;men
* Squadron flagship: 53&nbsp;officers, 1,034&nbsp;men
* 2nd command flagship: 42&nbsp;officers, 991&nbsp;men
  
| Ship crew = 
| Ship armament =* 12 × {{convert|28|cm|in|abbr=on}} SK L/45 guns
* 12 × {{convert|15|cm|in|abbr=on}} SK L/45 guns
* 16 × {{convert|8.8|cm|in|abbr=on}} SK L/45 guns
*  6 × {{convert|45|cm|in|abbr=on}} torpedo tubes
  
| Ship armor =* Belt: {{convert|300|mm|in|abbr=on}}
* Turrets: {{convert|280|mm|in|abbr=on}}
* Battery: {{convert|160|mm|in|abbr=on}}
* Conning Tower: {{convert|300|mm|in|abbr=on}}
* Torpedo bulkhead: {{convert|30|mm|in|abbr=on}}
  
| Ship notes =* Double bottom: 88%
* Watertight compartments: 16
  
}}
|}

'''SMS ''Westfalen'''''{{efn|name=SMS}} was one of the {{sclass-|Nassau|battleship}}s, the first four [[dreadnought]]s built for the [[German Imperial Navy]]. ''Westfalen'' was [[laid down]] at [[AG Weser]] in [[Bremen]] on 12 August 1907, launched nearly a year later on 1 July 1908, and commissioned into the [[High Seas Fleet]] on 16 November 1909. The ship was equipped with a main battery of twelve {{convert|28|cm|in|abbr=on}} guns in six twin [[gun turret|turrets]] in an unusual hexagonal arrangement.

The ship served with her [[sister ship]]s for the majority of [[World War I]], seeing extensive service in the [[North Sea]], where she took part in several fleet sorties. These culminated in the [[Battle of Jutland]] on 31 May&nbsp;– 1 June 1916, where ''Westfalen'' was heavily engaged in night-fighting against British light forces. ''Westfalen'' led the German line for much of the evening and into the following day, until the fleet reached [[Wilhelmshaven]]. On another fleet advance in August 1916, the ship was damaged by a torpedo from a British [[submarine]].

''Westfalen'' also conducted several deployments to the [[Baltic Sea]] against the [[Russian Navy]]. The first of these was during the [[Battle of the Gulf of Riga]], where ''Westfalen'' supported a German naval assault on the gulf. ''Westfalen'' was sent back to the Baltic in 1918 to support the [[White Guard (Finland)|White Finns]] in the [[Finnish Civil War]]. The ship remained in Germany while the majority of the fleet was interned in [[Scapa Flow]] after the end of the war. In 1919, following the [[scuttling of the German fleet in Scapa Flow]], ''Westfalen'' was ceded to the Allies as a replacement for the ships that had been sunk. She was then sent to [[ship breaking|ship-breakers]] in England, who broke the ship up for scrap by 1924.

== Description ==
{{main|Nassau-class battleship}}

[[File:Nassau class main weapon.svg|thumb|left|Line drawing of a ''Nassau''-class battleship showing the disposition of the main battery]]

''Westfalen'' was {{convert|146.1|m|ftin|abbr=on}} long, {{convert|26.9|m|ftin|abbr=on}} wide, and had a draft of {{convert|8.9|m|ftin|abbr=on}}. She displaced {{convert|18,873|t|LT|abbr=on}} with a standard load, and {{convert|20,535|t|LT|abbr=on|-1}} fully laden. The ship design retained 3-shaft [[Steam engine#Multiple expansion engines|triple expansion engines]] instead of the more advanced [[turbine]] engines. Steam was provided to the engines by twelve coal-fired boilers, with the addition in 1915 of supplementary oil firing.{{sfn|Gröner|p=23–24}} This machinery was chosen at the request of Admiral [[Alfred von Tirpitz]] and the Navy's construction department. The department stated in 1905 that the "use of turbines in heavy warships does not recommend itself."{{sfn|Herwig|pp=59–60}}

''Westfalen'' carried twelve [[28 cm SK L/45 gun|{{convert|28|cm|in|abbr=on}} SK L/45]]{{efn|name=gun nomenclature}} guns in an unusual hexagonal configuration.{{efn|name=wing turret}} Her secondary armament consisted of twelve [[15 cm SK L/45|{{convert|15|cm|in|abbr=on}} SK L/45]] guns and sixteen [[8.8 cm SK L/45 naval gun|{{convert|8.8|cm|in|abbr=on}} SK L/45]] guns, all of which were mounted in [[casemate]]s.{{sfn|Gröner|p=23–24}} The ship was also armed with six {{convert|45|cm|in|abbr=on}} submerged [[torpedo tube]]s. One tube was mounted in the bow, another in the stern, and two on each broadside, on either end of the [[torpedo bulkhead]].{{sfn|Gardiner & Gray|p=140}}

== Service history ==
[[File:SMS Westfalen NH 45196.jpg|thumb|SMS ''Westfalen'' shortly after completion, circa 1910]]

The German Imperial Navy (''Kaiserliche Marine'') ordered ''Westfalen'' under the provisional name ''Ersatz Sachsen'' as a replacement for {{SMS|Sachsen|1877|6}}, the [[lead ship]] of the elderly {{sclass-|Sachsen|ironclad|2}}s.{{sfn|Gröner|p=23–24}} The [[Reichstag (German Empire)|Reichstag]] secretly approved and provided funds for ''Nassau'' and ''Westfalen'' at the end of March 1906, but construction on ''Westfalen'' was delayed while arms and armor were procured.{{sfn|Staff|p=19}} She was laid down on 12 August 1907 at the AG Weser shipyard in Bremen.{{sfn|Staff|p=26}} As with her sister {{SMS|Nassau||2}}, construction proceeded swiftly and secretly; detachments of soldiers guarded both the shipyard and the major contractors who supplied building materials, such as [[Krupp]].{{sfn|Staff|p=19}}{{sfn|Hough|p=26}} The ship was launched on 1 July 1908, underwent an initial [[fitting-out]], and then in mid-September 1909 was transferred to [[Kiel]] by a crew composed of dockyard workers for a final fitting-out. However, the water level in the [[Weser River]] was low at this time of year, so six [[Float (nautical)|pontoons]] had to be attached to the ship to reduce her [[Draft (hull)|draft]]. Even so, it took two attempts before the ship cleared the river.{{sfn|Staff|p=26}}

On 16 October 1909, before ''Westfalen'' was commissioned into the fleet, the ship took part in a ceremony for the opening of the third set of locks in the [[Kaiser Wilhelm Canal]] in Kiel.{{sfn|Staff|pp=23–24}} Exactly one month later, ''Westfalen'' was commissioned for [[sea trial]]s, which were interrupted only by fleet training exercises in February 1910. At the completion of the trials on 3 May, ''Westfalen'' was added to the I Battle Squadron of the High Seas Fleet; two days later, she became the squadron flagship, replacing the [[pre-dreadnought]] {{SMS|Hannover}}. The navy had intended to transfer the ship to the II Battle Squadron, but this plan was discarded after the outbreak of World War I in July 1914.{{sfn|Staff|p=26}}

=== World War I ===
''Westfalen'' participated in most of the fleet advances into the North Sea throughout the war.{{sfn|Staff|p=26}} The first operation was conducted primarily by Rear Admiral [[Franz von Hipper]]'s [[battlecruiser]]s; the ships bombarded the English coastal towns of [[Raid on Scarborough, Hartlepool and Whitby|Scarborough, Hartlepool, and Whitby]] on 15–16 December 1914.{{sfn|Tarrant|p=31}} A German battlefleet of 12 dreadnoughts, including ''Westfalen'', her three sisters and eight pre-dreadnoughts sailed in support of the battlecruisers. On the evening of 15 December, they came to within {{convert|10|nmi|abbr=on}} of an isolated squadron of six British battleships. However, skirmishes between the rival [[destroyer]] screens in the darkness convinced the German fleet commander, Admiral [[Friedrich von Ingenohl]], that the entire [[Grand Fleet]] was deployed before him. Under orders from [[Kaiser Wilhelm II]], von Ingenohl broke off the engagement and turned the battlefleet back towards Germany.{{sfn|Tarrant|pp=31–33}} In late March 1915 the ship went into drydock for periodic maintenance.{{sfn|Staff|p=26}}

==== Battle of the Gulf of Riga ====
{{main|Battle of the Gulf of Riga}}

In August 1915, the German fleet attempted to clear the Russian-held [[Gulf of Riga]] in order to assist the German army, which was planning an assault on [[Riga]] itself. To do so, the German planners intended to drive off or destroy the Russian naval forces in the Gulf, which included the pre-dreadnought battleship {{ship|Russian battleship|Slava||2}} and a number of smaller [[gunboat]]s and destroyers. The German battle fleet was accompanied by several mine-warfare vessels, tasked first with clearing Russian minefields and then laying a series of their own minefields in the northern entrance to the Gulf to prevent Russian naval reinforcements from reaching the area. The assembled German fleet included ''Westfalen'' and her three sister ships, the four {{sclass-|Helgoland|battleship|2}}s, the battlecruisers {{SMS|Von der Tann||2}}, {{SMS|Moltke||2}}, and {{SMS|Seydlitz||2}}, and several pre-dreadnoughts. The force operated under the command of Franz von Hipper, who had by now been promoted to vice admiral. The eight battleships were to provide cover for the forces engaging the Russian flotilla. The first attempt on 8 August was broken off, as it took too long to clear the Russian minefields.{{sfn|Halpern|pp=196–197}}

On 16 August 1915, a second attempt was made to enter the Gulf: ''Nassau'' and {{SMS|Posen||2}}, four light cruisers, and 31&nbsp;torpedo boats managed to breach the Russian defenses.{{sfn|Halpern|p=197}} On the first day of the assault, two German light craft—the minesweeper {{SMS|T46||2}} and the destroyer {{SMS|V99||2}}—were sunk. The following day, ''Nassau'' and ''Posen'' battled ''Slava'', scoring three hits on the Russian ship that forced her to retreat. By 19 August, the Russian minefields had been cleared and the flotilla entered the Gulf. However, reports of Allied submarines in the area prompted the Germans to call off the operation the following day.{{sfn|Halpern|pp=197–198}} Admiral Hipper later remarked that "to keep valuable ships for a considerable time in a limited area in which enemy submarines were increasingly active, with the corresponding risk of damage and loss, was to indulge in a gamble out of all proportion to the advantage to be derived from the occupation of the Gulf ''before'' the capture of Riga from the land side." In fact, the battlecruiser ''Moltke'' had been torpedoed that morning.{{sfn|Halpern|p=198}}

==== Return to the North Sea ====
By the end of August ''Westfalen'' and the rest of the High Seas Fleet had returned to their anchorages in the North Sea. The next operation conducted was a sweep into the North Sea on 11–12 September, though it ended without any action. Another sortie followed on 23–24 October during which the German fleet did not encounter any British forces. Another uneventful advance into the North Sea took place on 21–22 April 1916. A bombardment mission followed two days later; ''Westfalen'' joined the battleship support for Hipper's battlecruisers while they [[Bombardment of Yarmouth and Lowestoft|attacked Yarmouth and Lowestoft]] on 24–25 April.{{sfn|Staff|p=31}} During this operation, the battlecruiser ''Seydlitz'' was damaged by a British mine and had to return to port prematurely. Due to poor visibility, the operation was soon called off, leaving the British fleet no time to intercept the raiders.{{sfn|Tarrant|pp=52–54}}

==== Battle of Jutland ====
{{main|Battle of Jutland}}

Admiral [[Reinhard Scheer]], who had succeeded Admirals von Ingenohl and [[Hugo von Pohl]] as the fleet commander, immediately planned another attack on the British coast. However, the damage to ''Seydlitz'' and condenser trouble on several of the III Battle Squadron dreadnoughts delayed the plan until the end of May 1916.{{sfn|Tarrant|pp=56–58}} The German battlefleet departed the [[Jadebusen|Jade]] at 03:30{{efn|name=CET times}} on 31 May.{{sfn|Tarrant|p=62}} ''Westfalen'' was assigned to the II Division of the I Battle Squadron, under the command of Rear Admiral W. Engelhardt. ''Westfalen'' was the last ship in the division, astern of her three sisters. The II Division was the last unit of dreadnoughts in the fleet; they were followed by only the elderly pre-dreadnoughts of the II Battle Squadron.{{sfn|Tarrant|p=286}}

Between 17:48 and 17:52, eleven German dreadnoughts, including ''Westfalen'', engaged and opened fire on the British 2nd Light Cruiser Squadron, though the range and poor visibility prevented effective fire, which was soon checked.{{sfn|Campbell|p=54}} At 18:05, ''Westfalen'' began firing again; her target was a British [[light cruiser]], most probably the {{HMS|Southampton|1912|2}}. Despite the short distance, around {{convert|18000|m|yd|-1}}, ''Westfalen'' scored no hits.{{sfn|Campbell|p=99}} Scheer had by this time called for maximum speed in order to pursue the British ships; ''Westfalen'' made {{convert|20|kn}}.{{sfn|Campbell|p=103}}  By 19:30 when Scheer signaled "Go west", the German fleet had faced the deployed Grand Fleet for a second time and was forced to turn away. In doing so, the order of the German line was reversed; this would have put the II Squadron in the lead, but Captain Redlich of ''Westfalen'' noted that II Squadron was out of position and began his turn immediately, assuming the lead position.{{sfn|Tarrant|pp=154, 172}}

Around 21:20, ''Westfalen'' and her sister ships began to be engaged by the battlecruisers of the [[3rd Battlecruiser Squadron (United Kingdom)|3rd Battlecruiser Squadron]]; several large shells straddled (fell to either side of) the ship and rained splinters on her deck. Shortly thereafter, two torpedo tracks were spotted that turned out to be imaginary. The ships were then forced to slow down in order to allow the battlecruisers of the I Scouting Group to pass ahead.{{sfn|Campbell|p=254}} Around 22:00, ''Westfalen'' and ''Rheinland'' observed unidentified light forces in the gathering darkness. After flashing a challenge via searchlight that was ignored, the two ships turned away to starboard in order to evade any torpedoes that might have been fired. The rest of I Battle Squadron followed them.{{sfn|Campbell|p=257}} During the brief encounter, ''Westfalen'' fired seven of her 28&nbsp;cm shells in the span of about two and a half minutes.{{sfn|Campbell|p=258}}  ''Westfalen'' again assumed a position guiding the fleet, this time because Scheer wanted lead ships with greater protection against torpedoes than the pre-dreadnoughts had.{{sfn|Tarrant|p=204}}

At about 00:30, the leading units of the German line encountered British destroyers and cruisers. A violent firefight at close range ensued; ''Westfalen'' opened fire on the destroyer {{HMS|Tipperary|1915|6}} with her 15&nbsp;cm and 8.8&nbsp;cm guns at a distance of about 1,800&nbsp;m (2,000&nbsp;yd). Her first salvo destroyed ''Tipperary''{{'}}s bridge and forward deck gun. In the span of five minutes, ''Westfalen'' fired ninety-two 15&nbsp;cm and forty-five 8.8&nbsp;cm rounds at ''Tipperary'' before turning 90&nbsp;degrees to starboard to evade any torpedoes that might have been fired.{{sfn|Tarrant|p=218}} ''Nassau'' and several cruisers and destroyers joined in the attack on ''Tipperary''; the ship was quickly turned into a burning wreck. The destroyer nevertheless continued to fire with her stern guns and launched her two starboard torpedoes.{{sfn|Campbell|p=286}} One of the British destroyers scored a hit on ''Westfalen''{{'}}s bridge with its {{convert|4|in|cm|sp=us|adj=on}} guns, killing two men and wounding eight;{{sfn|Tarrant|p=298}} Captain Redlich was slightly wounded.{{sfn|Campbell|p=287}}  At 00:50, ''Westfalen'' spotted {{HMS|Broke|1914|6}} and briefly engaged her with her secondary guns; in about 45&nbsp;seconds she fired thirteen 15&nbsp;cm and thirteen 8.8&nbsp;cm shells before turning away.{{sfn|Campbell|p=288}} ''Broke'' was engaged by other German warships, including the cruiser {{SMS|Rostock||2}}; she was hit at least seven times and suffered 42&nbsp;dead, six missing, and 34&nbsp;wounded crew members. An officer aboard the light cruiser {{HMS|Southampton|1912|2}} described ''Broke'' as "an absolute shambles."{{sfn|Bennett|pp=126–127}} Despite the serious damage inflicted, ''Broke'' managed to withdraw from the battle and reach port.{{sfn|Campbell|p=288}} Just after 01:00, ''Westfalen''{{'}}s searchlights fell on the destroyer ''Fortune'', which was wrecked and set ablaze in a matter of seconds by ''Westfalen'' and ''Rheinland''.{{sfn|Tarrant|p=222}} She also managed to sink the British destroyer, HMS Turbulent.<ref>{{Cite web|url=http://www.iwm.org.uk/history/battle-of-jutland-timeline|title=Battle Of Jutland Timeline|last=jtalarico|date=2016-05-13|access-date=2016-06-26}}</ref>

Despite the ferocity of the night fighting, the High Seas Fleet punched through the British destroyer forces and reached [[Horns Reef]] by 4:00 on 1&nbsp;June.{{sfn|Tarrant|pp=246–7}}  With ''Westfalen'' in the lead,{{sfn|Tarrant|p=240}} the German fleet reached Wilhelmshaven a few hours later, where the battleship and two of her sisters took up defensive positions in the outer [[roadstead]].{{sfn|Tarrant|p=263}} Over the course of the battle, the ship had fired fifty-one 28&nbsp;cm shells, one-hundred and seventy-six 15&nbsp;cm rounds, and one hundred and six 8.8&nbsp;cm shells.{{sfn|Tarrant|p=292}} Repair work followed immediately in Wilhelmshaven and was completed by 17 June.{{sfn|Campbell|p=336}}

==== Raid of 18–19 August ====
{{main|Action of 19 August 1916}}

Another fleet advance followed on 18–22&nbsp;August, during which the I&nbsp;Scouting Group battlecruisers were to bombard the coastal town of [[Sunderland, Tyne and Wear|Sunderland]] in an attempt to draw out and destroy Beatty's battlecruisers. As only two of the four German battlecruisers were still in fighting condition, three dreadnoughts were assigned to the Scouting Group for the operation: {{SMS|Markgraf||2}}, {{SMS|Grosser Kurfürst|1913|2}} (or ''Großer''{{efn|name=sharp S}} ''Kurfürst''), and the newly commissioned {{SMS|Bayern||2}}. The High Seas Fleet, including ''Westfalen'' at the rear of the line,{{sfn|Staff|p=26}} would trail behind and provide cover.{{sfn|Massie|p=682}} However, at 06:00 on 19 August, ''Westfalen'' was torpedoed by the British submarine {{HMS|E23}}, some {{convert|55|nmi}} north of [[Terschelling]]. The ship took in approximately {{convert|800|MT|sp=us}} of water, but the torpedo bulkhead held. Three torpedo-boats were detached from the fleet to escort the damaged ship back to port; ''Westfalen'' made {{convert|14|kn|abbr=on}} on the return trip.{{sfn|Staff|p=26}} The British were aware of the German plans and sortied the Grand Fleet to meet them. By 14:35, Admiral Scheer had been warned of the Grand Fleet's approach and, unwilling to engage the whole of the Grand Fleet just 11&nbsp;weeks after the close call at Jutland, turned his forces around and retreated to German ports.{{sfn|Massie|p=683}} Repairs to ''Westfalen'' lasted until 26 September.{{sfn|Staff|p=26}}

Following the repair work, ''Westfalen'' briefly went into the Baltic Sea for training, before returning to the North Sea on 4 October. The fleet then advanced as far as the [[Dogger Bank]] on 19–20 October.{{sfn|Staff|pp=26–27}} The ship remained in port for the majority of 1917. The ship did not actively take part in [[Operation Albion]] in the Baltic, though she was stationed off [[Apenrade]] to prevent a possible British incursion into the area.{{sfn|Staff|p=27}}

==== Expedition to Finland ====
[[File:Bundesarchiv DVM 10 Bild-23-61-23, Linienschiff "SMS Rheinland".jpg|thumb|SMS ''Rheinland''|alt=A large battleship lined with guns and equipped with two tall masts sits in harbor.]]

On 22 February 1918, ''Westfalen'' and ''Rheinland'' were tasked with a mission to Finland to support German army units to be deployed there. The Finns were engaged in a [[Finnish Civil War|civil war]]; the White Finns sought a conservative government free from influence from the newly created [[Soviet Union]], while the [[Red Guards (Finland)|Red Guards]] preferred Soviet-style communism. On 23 February, the two ships took on the 14th Jäger Battalion, and early on 24 February they departed for the [[Åland Islands]]. Åland was to be a forward operating base, from which the port of [[Hanko]] would be secured, following an assault on the capital of [[Helsinki]]. The task force reached the Åland Islands on 5 March, where they encountered the Swedish [[coastal defense ship]]s {{HSwMS|Sverige||2}}, {{HSwMS|Thor|1898|2}}, and {{HSwMS|Oscar II||2}}. Negotiations ensued, which resulted in the landing of the German troops on Åland on 7 March; ''Westfalen'' then returned to [[Danzig]].{{sfn|Staff|p=27}}

''Westfalen'' remained in Danzig until 31 March, when she departed for Finland with ''Posen''; the ships arrived at [[Russarö]], which was the outer defense for Hanko, by 3 April. The German army quickly took the port. The task force then proceeded to Helsingfors; on 9 April ''Westfalen'' stood off [[Reval]], organizing the invasion force. Two days later the ship passed into the harbor at Helsingfors and landed the soldiers; she supported their advance with her main guns. The Red Guards were defeated within three days. The ship remained in Helsingfors until 30 April, by which time the White government had been installed firmly in power.{{sfn|Staff|p=27}}

Following the operation, ''Westfalen'' returned to the North Sea where she rejoined the I Battle Squadron. On 11 August, ''Westfalen'', ''Posen'', {{SMS|Kaiser|1911|2}}, and {{SMS|Kaiserin||2}} steamed out towards Terschelling to support German patrols in the area. While en route, ''Westfalen'' suffered serious damage to her boilers that reduced her speed to {{convert|16|kn|abbr=on}}. After returning to port, she was decommissioned and employed as an artillery training ship.{{sfn|Staff|p=27}}

=== Fate ===
Following the German collapse in November 1918, a significant portion of the High Seas Fleet was interned in Scapa Flow under the terms of the [[Armistice with Germany|Armistice]]. ''Westfalen'' and her three sisters—the oldest dreadnoughts in the German navy-were not among the ships listed for internment, so they remained in German ports.{{sfn|Hore|p=67}} During the internment, a copy of ''[[The Times]]'' informed the German commander, Rear Admiral Ludwig von Reuter, that the Armistice was to expire at noon on 21 June 1919, the deadline by which Germany was to have signed the peace treaty. Von Reuter believed that the British intended to seize the German ships after the Armistice expired.{{efn|name=armistice expiry}} To prevent this, he decided to [[scuttling|scuttle]] his ships at the first opportunity. On the morning of 21 June, the British fleet left Scapa Flow to conduct training maneuvers; at 11:20 Reuter transmitted the order to his ships.{{sfn|Herwig|p=256}}

As a result of the scuttling at Scapa Flow, the Allies demanded replacements for the ships that had been sunk. ''Westfalen'' was struck from the German naval list on 5 November 1919 and subsequently handed over to the Allies under the contract name "D" on 5 August 1920.{{sfn|Gröner|p=24}} The ship was then sold to ship-breakers in [[Birkenhead]], where she was broken up for scrap by 1924.{{sfn|Staff|p=27}}

== Notes ==

'''Footnotes'''

{{notes
| notes =

{{efn
| name = SMS
| "SMS" stands for "''[[Seiner Majestät Schiff]]''", or "His Majesty's Ship".
}}

{{efn
| name = gun nomenclature
| In Imperial German Navy gun nomenclature, "SK" (Schnelladekanone) denotes that the gun is quick-firing, while "L/45" provides the length of the gun in terms of the diameter of the barrel. In this case, the L/45 gun is 45 [[Caliber (artillery)|caliber]], which means that the gun is 45&nbsp;times as long as its diameter.
}}

{{efn
| name = wing turret
| Four of the six gun turrets were arranged as [[wing turret]]s, two on either side of the [[superstructure]], a compact arrangement that reduced the length of the ship (see {{harvnb|Staff|p=21}}). No foreign dreadnought at that time used this arrangement. {{HMS|Dreadnought|1906|6}} carried two wing turrets and three more on the centerline, while {{USS|South Carolina|BB-26|6}} mounted all four turrets in [[superfire|superfiring pairs]] on the centerline. The first Russian ({{sclass-|Gangut|battleship|5}}) and Italian ({{ship|Italian battleship|Dante Alighieri||2}}) designs carried four gun turrets on the centerline. See {{harvnb|Gardiner & Gray|p=21}} for ''Dreadnought'', p. 112 for ''South Carolina'', p. 302 for ''Gangut'', and p. 259 for ''Dante Alighieri''.
}}

{{efn
| name = CET times
| The times used in this article are in [[Central European Time|CET]], which is one hour ahead of [[Coordinated Universal Time|UTC]], which is often used in British works.
}}

{{efn
| name = sharp S
| This is the German "sharp S"; see [[ß]].
}}

{{efn
| name = armistice expiry
| By this time, the Armistice had been extended to 23 June, though there is some contention as to whether von Reuter was aware of this. Admiral [[Sydney Fremantle]] stated that he informed von Reuter on the evening of the 20th, though von Reuter claims he was unaware of the development. For Fremantle's claim, see {{harvnb|Bennett|p=307}}. For von Reuter's statement, see {{harvnb|Herwig|p=256}}.
}}

}}

'''Citations'''

{{reflist|20em}}

== References ==
{{Commons category|SMS Westfalen (ship, 1908)}}
{{portal|Battleships}}

* {{cite book
  | last = Bennett
  | first = Geoffrey
  | year = 2006
  | title = The Battle of Jutland
  | publisher = Pen & Sword Military Classics
  | location = London
  | isbn = 978-1-84415-436-4
  | ref = {{sfnRef|Bennett}}
  }}
* {{cite book
  | last = Campbell
  | first = John
  | year = 1998
  | title = Jutland: An Analysis of the Fighting
  | publisher = Conway Maritime Press
  | location = London
  | isbn = 978-1-55821-759-1
  | ref = {{sfnRef|Campbell}}
  }}
* {{cite book
  | editor1-last = Gardiner
  | editor1-first = Robert
  | editor2-last = Gray
  | editor2-first = Randal
  | year = 1985
  | title = Conway's All the World's Fighting Ships: 1906–1921
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-0-87021-907-8
  | ref = {{sfnRef|Gardiner & Gray}}
  }}
* {{cite book
  | last = Gröner
  | first = Erich
  | year = 1990
  | title = German Warships: 1815–1945
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-0-87021-790-6
  | oclc = 22101769
  | ref = {{sfnRef|Gröner}}
  }}
* {{cite book
  | last = Halpern
  | first = Paul G.
  | year = 1995
  | title = A Naval History of World War I
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-1-55750-352-7
  | oclc = 57447525
  | ref = {{sfnRef|Halpern}}
  }}
* {{cite book
  | last = Herwig
  | first = Holger
  | year = 1998
  | origyear = 1980
  | title = "Luxury" Fleet: The Imperial German Navy 1888–1918
  | publisher = Humanity Books
  | location = [[Amherst, New York]]
  | isbn = 978-1-57392-286-9
  | oclc = 57239454
  | ref = {{sfnRef|Herwig}}
  }}
* {{cite book
  | last = Hore
  | first = Peter
  | year = 2006
  | title = Battleships of World War I
  | publisher = Southwater Books
  | location = London
  | isbn = 978-1-84476-377-1
  | oclc = 77797289
  | ref = {{sfnRef|Hore}}
  }}
* {{cite book
  | last = Hough
  | first = Richard
  | authorlink = Richard Hough
  | year = 2003
  | title = Dreadnought: A History of the Modern Battleship
  | publisher = Periscope Publishing
  | location = 
  | isbn = 978-1-904381-11-2
  | ref = {{sfnRef|Hough}}
  }}
* {{cite book
  | last = Massie
  | first = Robert K.
  | authorlink = Robert K. Massie
  | year = 2003
  | title = [[Castles of Steel]]
  | publisher = Ballantine Books
  | location = [[New York City]]
  | isbn = 978-0-345-40878-5
  | oclc = 57134223
  | ref = {{sfnRef|Massie}}
  }}
* {{cite book
  | last = Staff
  | first = Gary
  | year = 2010
  | title = German Battleships: 1914–1918
  | volume = 1
  | publisher = Osprey Books
  | location = Oxford
  | isbn = 978-1-84603-467-1
  | oclc = 705750106
  | ref = {{sfnRef|Staff}}
  }}
* {{cite book
  | last = Tarrant
  | first = V. E.
  | year = 2001
  | origyear = 1995
  | title = Jutland: The German Perspective
  | publisher = Cassell Military Paperbacks
  | location = London
  | isbn = 978-0-304-35848-9
  | oclc = 48131785
  | ref = {{sfnRef|Tarrant}}
  }}

{{Nassau class battleship}}
{{Use dmy dates|date=September 2010}}
{{Featured article}}

{{DEFAULTSORT:Westfalen}}
[[Category:Nassau-class battleships]]
[[Category:Ships built in Bremen (state)]]
[[Category:1908 ships]]
[[Category:World War I battleships of Germany]]