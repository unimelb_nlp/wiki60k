{{Use dmy dates|date=November 2013}}
{{Use British English|date=November 2013}}
[[File:George Stainforth Card.jpg|thumb|right|Flight Lieutenant G H Stainforth (cigarette card)]]

[[Wing Commander (rank)|Wing Commander]] '''George Hedley Stainforth''' [[Air Force Cross (United Kingdom)|AFC]] [[Royal Air Force|RAF]] (1899 - 27 September 1942) was a [[United Kingdom|British]] [[Royal Air Force]] pilot and the first man to exceed 400 miles per hour.

==Early life==
George Hedley Stainforth was the son of George Staunton Stainforth, a [[solicitor]].<ref name="Stainforth511">{{cite web
 |author=Peter Stainforth 
 |title=Not Found Wanting: A History of the Stainforths, an Anglo-Saxon family 
 |pages=511 
 |url=http://www.stainforth-history.co.uk/notfoundw.html 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20120112032151/http://www.stainforth-history.co.uk:80/notfoundw.html 
 |archivedate=12 January 2012 
 |df=dmy 
}}</ref> He attended [[Dulwich College]]<ref name="Hodges,1981" >{{cite book
  |author=Hodges, S.
  |year=1981
  |title=God's Gift: A Living History of Dulwich College
  |pages=87
  |publisher=Heinemann
  |location=London
}}</ref> and [[Weymouth College (Public School)|Weymouth College]].<ref name="WPBC" >{{cite web
  |url=http://www.weymouth.gov.uk/main.asp?svid=504/
  |title=Weymouth and Portland Borough Council
}}</ref> He joined the [[British Army|Army]] before joining the [[Royal Air Force]].<ref name="Marham" >{{cite web
  |url=http://www.rafmarham.co.uk/relations/stories/stainforth04.htm
  |title=RAF Marham
  |accessdate=21 April 2008 }}</ref>

==Career==
George Stainforth joined the [[Royal Air Force]] on 15 March 1923 and was posted to No 19 (F) Squadron on 10 April 1924. He was promoted after four years to [[Flight Lieutenant]] on 1 July 1928, and was posted to the [[Marine Aircraft Experimental Establishment]] ([[Marine Aircraft Experimental Establishment|MAEE]]) for duties with the [[High Speed Flight RAF|High Speed Flight]], also known as ''The Flight''.

===1929 Schneider Trophy===
[[File:British team for Schneider Trophy race 1929.jpg|thumb|right|1929 Schneider Trophy team <br>Stainforth is second from right]]
Stainforth was serving with ''The High Speed Flight'' in 1929, as pilot of the [[Gloster VI]] entrant. The aircraft was withdrawn for technical reasons shortly before the competition, which was then won by his team-mate [[Flight Lieutenant|Flt. Lt.]] [[Henry Waghorn|H. Waghorn]] in a [[Supermarine S.6]].

On the following day, 10 September 1929, Stainforth took the [[Gloster VI]] up for an attempt at the record over a measured mile course. He achieved a top speed of 351.3&nbsp;mph and a ratified world absolute speed record, averaged over four runs of 336.3&nbsp;mph.<ref name="napier powered" >{{cite book
  |title=Napier Powered
  |author=Alan Vessey
  |publisher= Tempus (''Images of England'' series)
  |location=Stroud
  |year=1997
  |isbn=0-7524-0766-X
}}</ref>

This record was held but briefly, as a later run by the racing seaplane [[Supermarine S.6]] managed to raise it to over 350&nbsp;mph.

===1931 Schneider Trophy and the 400 mph barrier===
[[File:British team for Schneider Trophy race 1931.JPG|thumb|right|1931 Schneider Trophy team <br>Stainforth is fourth from left]]
[[Flight Lieutenant]] Stainforth was also one of the team in 1931 when the Trophy was won for the third time in a row, and thus the competition won outright. Following the Trophy triumph on 16 September, he had the chance to once again break the [[airspeed record]]. His first attempt was made in [[Supermarine S.6B]] '''S1596''', in which he achieved 379&nbsp;mph. Following a minor taxiing accident during testing though, he caused S1596 to turn over and sink. Although she was recovered by divers the next day, he now transferred to '''S1595'''. This was another S.6B, which could also be fitted with the same specially prepared 2,600&nbsp;bhp [[Rolls-Royce R]] "sprint" engine, serial R27<ref>Now on display in the [[Science Museum (London)|Science Museum]], London</ref> and airscrew for the record attempt. The engine was using a specially prepared fuel mixture of [[petrol]], [[methanol]] and [[Ethyl group|ethyl]]. Starting the engine was uneasy and there was considerable danger of engine explosion.<ref name="Gunn" />

On 29 September 1931, the record attempt was made. Due to the aircraft having no flaps, Stainforth took off from the water after a very long run up. The record was established at a height of 400 m.<ref name="FleetAir" >{{cite web
  |url=http://www.fleetairarmarchive.net/daedalus/RollofHonour.html
  |title=Fleet Air Arm Roll of Honour
  |publisher=Fleet Air Arm Archive
}}</ref> He made a perfect record run over the four timed miles in opposing directions and achieved an average of {{convert|407.5|mi/h|km/h|abbr=on}}, being the first man in the world to exceed 400&nbsp;mph.<ref name="Gunn" >{{cite book
  |author=Gunn, J,
  |year=1985
  |title=The Defeat of Distance: Qantas 1919-1939
  |pages=138
}}</ref><ref>{{cite web
  |url=http://www.raf.mod.uk/history_old/schneider6.html
  |title=1931 - Squadron Leader Orlebar's Report
  |publisher=RAF
}}</ref> For this achievement he was awarded the [[Air Force Cross (United Kingdom)|Air Force Cross]] on 9 October 1931.<ref name="Stainforth385" >{{cite book
  |author=Peter Stainforth
  |title=Not Found Wanting: A History of the Stainforths, an Anglo-Saxon family
  |pages=385–6
  |url =http://www.stainforth-history.co.uk/notfoundw.html
}}</ref>

He later went on to break another record, this time by flying upside down for 12&nbsp;minutes.<ref name="WPBC" />

===Career after ''The Flight''===
Leaving the [[Marine Aircraft Experimental Establishment|MAEE]] in 1935, Stainforth spent a short period as [[Adjutant]] aboard [[HMS Glorious]]. He was promoted to [[Squadron Leader]] on 1 June 1936 and served with No 802 Squadron as the Officer Commanding No 30(B) Squadron in [[Iraq]]. In February 1939, he returned to [[Central Flying School|CFS Upavon]] (as Officer Commanding ''Examining and Handling Flight''). On 12 January 1940, he was promoted to [[Wing Commander (rank)|Wing Commander]] and commanded No 600(F) Squadron. In June 1940, Stainforth and [[Robert Stanford Tuck|Stanford Tuck]], the [[Battle of Britain]] [[Flying ace|ace]], were posted to [[Farnborough Airfield|Farnborough]] in south central [[England]]. His task was to take part in comparison trials of a captured [[Messerschmitt Bf 109|Me-109E]] and a [[Supermarine Spitfire|Spitfire]] Mark II. The tests began with Stainforth flying the Messerschmitt and Tuck flying the Spitfire in level flight, dives and turns, and at various speeds at different altitudes.<ref name=FleetAir/>

Stainforth was appointed as Officer Commanding [[No. 89 Squadron RAF|No 89 (Night Fighter) Squadron]] in October 1941. At the end of that year, the Squadron was posted to the Middle East, and on the night of 27 September 1942, Wing Commander Stainforth was killed in action whilst piloting the aircraft [[Beaufighter]] ''[[United Kingdom military aircraft serials|X7700]]'' at Gharib, near the [[Gulf of Suez]]. He was buried with full military honours, at the British Cemetery [[Ismailia]], [[Egypt]].

Following his death, a dossier was compiled by his friends detailing many of his achievements, recorded remarks and memories of him by distinguished officers and men who served with him during his lifetime. It also includes extracts from his own thoughts and writings. A copy of the dossier has been presented to the [[Royal Air Force Museum]] at [[Hendon]].<ref name="Marham" />

==The Stainforth Trophy==
The trophy was originally presented in 1974 to the Air Officer Commanding-in-Chief [[RAF Strike Command]], [[Air Chief Marshal]] Sir [[Denis Smallwood]], [[Order of the Bath|KCB]] [[Order of the British Empire|CBE]] [[Distinguished Service Order|DSO]] [[Distinguished Flying Cross (United Kingdom)|DFC]] [[Royal Air Force|RAF]] by members of the No 89 Squadron Reunion Club, to commemorate the long and distinguished career, both as an aviator and as a Royal Air Force Officer, of Wing Commander George Hedley Stainforth AFC RAF. The Trophy had been commissioned and paid for, anonymously, by a Mrs Stella Sketch who died in May 2000.<ref name="Marham" /> The Stainforth Trophy is awarded annually by the Air Officer Commanding-in-Chief [[RAF Strike Command]] to the operational station within the Command which has produced the best overall performance and is their most prestigious trophy. The Stainforth family are still represented at the presentation.<ref name=Stainforth511/>

===Design===
The Stainforth Trophy was designed by [[Robin Beresford]]. The trophy depicts in silver three supersonic aircraft and spiralling vapour trails soaring into the stratosphere.<ref name=Stainforth511/> The artist also tried to display an awareness of the achievements of the [[Schneider Trophy]] Team in the design, being aware that these achievements resulted in the improved design and development, by [[R. J. Mitchell]], of the [[Supermarine Spitfire|Spitfire]], which played such a major role in Britain's victory in [[World War II]]. In the design of the trophy, the artist has tried to show the Earth as seen by the astronauts, [[vapour trail]]s ending in arrows to depict high speed altitude flight, and with an outward sweep to infinity, which suggests that the sky is literally the limit of man's achievement in the air. Into his design, he has incorporated a star and its orbit to evoke the achievements of science and the [[Royal Air Force]] motto ''[[Per ardua ad astra]]''.<ref name="Marham" />

==Trivia==

===Cigarette cards===
The image of G H Stainforth appeared in a number of Cigarette Card Sets including:
* [[Lambert & Butler]]'s ''Famous British Airmen & Airwomen''
* [[Carreras Tobacco Company|Carreras]]: ''Famous Airmen and Airwomen'' (issue year 1936). 50 Cards. Named as [[Flight Lieutenant]] GH Stainforth.
* [[Carreras Tobacco Company|Carreras]]: ''Famous British Fliers'' (issue year 1956). 50 Cards. Named as GH Stainforth.
* Park Drive cigarettes ([[Gallaher Group|Gallaher]] Ltd): ''Champions 1st series'' (issue year 1934). Number 38 - G H Stainforth

===Stainforth Weather Vane, Greenhill Gardens, Weymouth===
[[File:Weymouth - Greenhill Gardens - geograph.org.uk - 953531.jpg|thumb|Weather vane in [[Greenhill, Dorset|Greenhill]] Gardens, [[Weymouth, Dorset|Weymouth]], commemorating Stainforth's world speed record]]
The [[weather vane]] was originally presented to [[Weymouth College]] in 1932 (at that point known as Weymouth Grammar School, later renamed Weymouth Secondary School) as a memorial to Stainforth, its former pupil, who had set the world air speed record the previous year. Made of [[hardwood]] and covered in a [[copper]] sheath, the vane was erected above Weymouth College chapel in 1932, but moved for safety at the start of World War II. It was later presented to the borough council and placed in [[Greenhill Gardens, Weymouth|Greenhill Gardens]] in May 1952. In 1996, the vane had to be taken down after the effects of years of sea spray and coastal winds had taken their toll, but it has since been restored and is now back in the gardens.<ref name="WPBC" />

==References==
{{Reflist}}

==Further reading==
* {{cite book
  |author=Birch J. Matthews
  |title=Race with the Wind: How Air Racing Advanced Aviation
  |year=2001
  |publisher=Zenith Imprint
}}

{{DEFAULTSORT:Stainforth, George}}
[[Category:People educated at Dulwich College]]
[[Category:English aviators]]
[[Category:Royal Air Force officers]]
[[Category:British air racers]]
[[Category:Schneider Trophy pilots]]
[[Category:British military personnel killed in World War II]]
[[Category:Royal Air Force personnel of World War II]]
[[Category:1899 births]]
[[Category:1942 deaths]]
[[Category:People from Kent]]
[[Category:People educated at Weymouth College (public school)]]
[[Category:Alumni of Weymouth College]]