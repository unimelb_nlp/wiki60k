{{infobox book
| name      = Sub-Coelum
| author    = [[Addison Peale Russell]]
| country  = United States
| language = English
|genre       = [[Speculative fiction]]<br>[[Utopian and dystopian fiction|Utopian fiction]]<br>[[Belles-lettres]]
| publisher = [[Houghton Mifflin]]
| pub_date = [[1893 in literature|1893]]
| media_type = Print ([[Hardcover]])
| pages       = 267 pp. 
}}

'''''Sub-Coelum: A Sky-Built Human World''''' is an [[1893 in literature|1893]] [[Utopian and dystopian fiction|utopian fiction]] written by [[Addison Peale Russell]].<ref>A. P. Russell, ''Sub-Coelum: A Sky-Built Human World'', Boston, Houghton Mifflin, 1893.</ref> The book is one volume in the large body of utopian, dystopian, and speculative literature that characterized the later nineteenth and early twentieth centuries.<ref>Kenneth Roemer, ''The Obsolete Necessity, 1888&ndash;1900'', Kent, OH, Kent State University Press, 1976.</ref><ref>Francine Cary, ''Shaping the Future in the Gilded Age: A Study of Utopian Thought, 1888&ndash;1900'', Madison, WI, University of Wisconsin Press, 1975.</ref>

==Genre==
Scholar of the genre Jean Pfaelzer has described ''Sub-Coelum'' as a "conservative utopia," a book written in reaction to the multiple radical implications of the utopian fiction of [[Edward Bellamy]] and similar writers. While some skeptics of utopianism responded with dystopian satires and parodies, others, like Russell, answered with speculative fictions of their own that defended more conservative values. (Pfaelzer places  John Macnie's ''[[The Diothas]]'' and [[John Jacob Astor IV]]'s ''[[A Journey in Other Worlds]]'' in the same category.)<ref>Jean Pfaelzer, ''The Utopian Novel in America, 1886&ndash;1896: The Politics of Form'', Pittsburgh, University of Pittsburgh Press, 1984; pp. 172-3; see also pp. 95-111.</ref> ''Sub-Coelum'' has been called "a protest against the materialistic and socialistic tendencies of the times."<ref>"D. O.," "In Memoriam," ''The Bookman'', Vol. 37 (1913), p. 545.</ref>

==Form==
''Sub-Coelum'' has been termed a novel, for want of a better classification &mdash; though it is that unusual type of novel that has no plot or characters. It might more accurately be called a [[fantasy]] or a meditation on society and human affairs. The book is divided into 146 short chapters; most are a page or two in length. The style is sometimes elaborate and eloquently descriptive:
 
:"Every flying and creeping thing had its enthusiasts and exponents. Ephemera, infusoria, animalculae, were classified and individualized, without limit. Microbes, bacilli, were pets of the imagination. Children, even, seemed familiar with the monsters of the microscope, and talked of them as glibly as of their playthings and the chemical elements."<ref>''Sub-Coelum'', p. 20.</ref>

It can also be pithy and aphoristic: "Sarcasm was not often indulged, and only then between close friends." At some points the prose rises to a pitch of ecstasy or delirium:

:"Light and heat were obtained almost entirely from water....Exalting tonics and enrapturing odors were diffused through the atmosphere at pleasure. Talent expended itself in producing essences and tinctures and stimulants of paradisaic delicacy to be so employed. On great occasions the light produced rivaled that of the sun. The whole atmosphere seemed to be aflame. The effect was magical. The smallest thing was made visible, and all things were beautified in appearance. Men appeared more manly and women more lovely."<ref>''Sub-Coelum'', pp. 95-6.</ref>

Some critics complained about the book; a [[Yale University|Yale]] reviewer noted its "vagueness and indefiniteness...."<ref>''The Yale Literary Magazine'', Vol. 59 (November 1893), p. 103.</ref> Russell's imagined land has been grouped with "[[A Traveler from Altruria|Altruria]], Equitania...or even [[Meccania]]"<ref>G. Stanley Hall, "The Message of the Zeitgeist," ''The Scientific Monthly'', Vol. 13 (1921), p. 106.</ref> (the fantasy countries of [[William Dean Howells]], Walter O. Henry, and Owen Gregory respectively).

==Moral matters==
Pfaelzer calls ''Sub-Coelum'' "an early behaviorist utopia...."<ref>Pfaelzer, p. 102.</ref> There is much "individuality" in Russell's projected social order, but little privacy; the people are close observers of each other. Artists who offend are jailed. Russell places a high value on sexual restraint. "Purity, of all things, was most jealously guarded. The incorrigibly impure were locked up forever. Men and women, as to that, were treated alike by the police and by the courts."<ref>''Sub-Coelum'', p. 61.</ref> To obtain a marriage license, a couple must answer a long series of questions, under oath.

On the positive side, material and mechanical progress continue; the workday is shortened, and extremes of wealth and poverty are leveled out. Even [[racism]] is  overcome. Surprisingly for the era of [[Jim Crow laws|Jim Crow]] and [[anti-miscegenation laws]], Russell appears to endorse inter-racial marriage: "Race prejudices gradually gave way, and bigotries. Fibres intermingled and blood interfused. Distinctions were obliterated by intermarriage."<ref>''Sub-Coelum'', p. 114.</ref>

As with race, so with gender: Russell's moralizing and idealizing tendency produces an unexpected result. "Men, many of them, changed places with women, and became essentially domestic. Household duties, in a great degree, had passed into their hands. They discovered a fondness for them, as to the other sex they became distasteful." "As far as possible woman was emancipated from menial duties." The country's doctors are women.<ref>''Sub-Coelum'', pp. 245, 248.</ref>

Russell consistently contrasts the past of Sub-Coelum, when things were less good, with the happier present.  In the past, the nation may have had inferior clergy, and corrupt lawyers, and vain and foolish social customs<ref>''Sub-Coelum'', pp. 43-8, 48-50, 68-70, 110-12.</ref> &mdash; but moral reformation has brought about improvement. In this way, Russell contrasts actual aspects of American culture in his age, with his vision of how things should be.

In Russell's imaginary country, "The vices, in a great measure, had been eliminated, or had died out." This includes alcohol abuse and tobacco, gambling and prize fighting. "Increase of common sense and practical wisdom was a marked result of the new life."<ref>''Sub-Coelum'', pp. 76, 79.</ref> Yet (with the vagueness cited by the Yale critic) Russell never explains how this renovation in human nature comes about.

==Eccentricities==
Russell also loads his fantasia with a fair share of oddities. The people of Sub-Coelum slaughter their chickens humanely, with guillotines. They keep "intelligent monkeys," along with monkey hospitals and monkey temples. They add trees and shrubs to the native forests, "to give greater variety." Squirrels are domesticated. Cemeteries are the most beautiful places in the sky-built land, and birds are lured into nesting on the graves. Snoring, whistling, and bell-ringing have been banished from society. The Sub-Coelumites have exceptionally good teeth; they train their pigs to eat in moderation.

The book is full of animals. In addition to the squirrels and monkeys, Russell includes passages on bees, butterflies, dogs, horses, orangutans, snakes, insects, and microscopic life. A ten-page chapter, the book's longest, extols the amazing qualities of rats.

==References==
{{reflist}}

==See also==
{{portal|Novels}}
* ''[[Arqtiq]]''
* ''[[The Milltillionaire]]''
* ''[[The Scarlet Empire]]''
* ''[[The World a Department Store]]''

[[Category:Utopian novels]]
[[Category:Speculative fiction novels]]
[[Category:1893 novels]]