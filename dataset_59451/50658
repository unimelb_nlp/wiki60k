<!-- Please leave this line alone! -->
{{Infobox journal
| title = Australasian Psychiatry
| cover = [[File:Australasian Psychiatry.jpg]]
| editor = Dr Vlasios Brakoulias 
| discipline = 
| former_names = 
| abbreviation = 
| publisher = [[SAGE Publications]]
| country = 
| frequency = Bi-monthly
| history = 
| openaccess = 
| license = 
| impact = 0.917
| impact-year = 2010
| website = http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal202097
| link1 = http://apy.sagepub.com/content/current
| link1-name = Online access
| link2 = http://apy.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 1039-8562
| eISSN = 1440-1665
| OCLC = 605130283
| LCCN = 96660764
}}

'''''Australasian Psychiatry''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that publishes papers in the field of [[Psychiatry]]. The journal's [[Editor-in-Chief|editor]] is Professor Garry Walter. It is currently published by [[SAGE Publications]] on behalf of [[The Royal Australian and New Zealand College of Psychiatrists]].

== Scope ==
''Australasian Psychiatry'' aims to promote psychiatry and the maintenance of excellent standards in practice. The journal publishes original research; reviews; descriptions of innovative services; comments on policy, history, politics, economics, training, ethics and the Arts as they relate to mental health and mental health services; statements of opinion; case reports, letters and commissioned book reviews. A section of ''Australasian Psychiatry'' provides information on RANZCP business and related matters.

== Abstracting and indexing ==
''Australasian Psychiatry'' is abstracted and indexed in:
:* [[Academic Search|Academic Search (EBSCO)]]
:* [[Australian Public Affairs and Information Service|Australian Public Affairs and Information Service (APAIS)]]
:* [[Applied Social Sciences Index and Abstracts|Applied Social Sciences Index and Abstracts (ASSIA)]]
:* [[Australasian Medical Index|Australasian Medical Index (AMI)]]
:* [[Embase]]
:* [[Health Source Nursing/Academic|Health Source Nursing/Academic (EBSCO)]]
:* [[MEDLINE]]
:* [[ProQuest]]
:* [[Psychology & Behavioural Science|Psychology & Behavioural Science (EBSCO)]]
:* [[PsycINFO]]
:* [[PubMed]]
:* [[Sciences Citation Index]]<ref name=WoS>{{cite book |year=2011 |chapter=Journals Ranked by Impact: Psychiatry |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition= Sciences |accessdate=2011-09-30 |work=Web of Science |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|1=http://apy.sagepub.com/}}
* [http://www.ranzcp.org/ RANZCP Official website]

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]