{{Use dmy dates|date=January 2015}}
{{Use Australian English|date=January 2015}}
{{Infobox station
| name                = Millswood
| image               = Millswood_Station_(October_2014).jpg
| caption             = 
| address             = 251 Goodwood Rd<br />Kings Park SA 5034
| coordinates         = {{coord|-34.958804|138.590602|format=dms|type:railwaystation_region:AU-SA|display=inline,title}}
| owned               = [[Department of Planning, Transport and Infrastructure|DPTI]]
| operator            = [[Adelaide Metro]]
| line                = [[Belair railway line|Belair Line]]
| distance            = 5.9&nbsp;km from [[Adelaide railway station|Adelaide]]
| platforms           = 1 Side
| tracks              = 2
| structure           = Ground
| parking             = No
| bicycle             = No
| disabled            = Yes
| bus_routes          = {{AdelaideMetroBus | bgcolour=#b57628 | ftcolour=#ffffff | rte=G10}} {{AdelaideMetroBus | bgcolour=#757575 | ftcolour=#ffffff | rte=G20}} {{AdelaideMetroBus | bgcolour=#00838f | ftcolour=#ffffff | rte=G21}} {{AdelaideMetroBus | bgcolour=#558b2f | ftcolour=#ffffff | rte=G30F}} {{AdelaideMetroBus | bgcolour=#f9a825 | ftcolour=#000000 | rte=N10}} {{AdelaideMetroBus | bgcolour=#00838f | ftcolour=#ffffff | rte=N21}}
| code                = 
| zone                = 
| website             = 
| opened              = 1910s
| closed              = 
| rebuilt             = 12 October 2014
| services            = 
{{s-rail|title=TransAdelaide}}
{{s-line|system=TransAdelaide|line=Belair|previous=Goodwood|next=Unley Park}}
}}
 
'''Millswood railway station''' is located on the [[Belair railway line|Belair line]] in [[Adelaide]].<ref>[http://www.adelaidemetro.com.au/var/metro/storage/original/application/6ede59c8022d206c992773d85ff753a4.pdf Belair timetable] Adelaide Metro 12 October 2014</ref> Situated in the [[Adelaide]] suburb of [[Millswood]], it is 5.9 kilometres from [[Adelaide railway station|Adelaide station]].

==History==
The station opened circa 1910.{{Citation needed|date=September 2008}} The platforms were constructed of earth-filled concrete each side of the dual tracks, which were both [[broad gauge]] until 1995. There were timber framed, iron clad open passenger shelters on each platform. The two outbound [[Unley Park railway station|Unley Park]] and [[Hawthorn railway station, Adelaide|Hawthorn]] stations had similar shelters. A ticket office was provided on the Up (western) platform until being demolished during in 1985, and the original shelters were in due course removed in March 1988 and replaced with the bus stop type shelters that are seen at some stations on the [[TransAdelaide]] network.

Millswood station closed on 28 April 1995, simultaneously with stations at [[Clapham railway station, Adelaide|Clapham]] and [[Hawthorn railway station, Adelaide|Hawthorn]] despite criticism from nearby residents, with the conversion of the [[Adelaide-Wolseley railway line|Adelaide-Wolseley line]] to [[standard gauge]] under the [[One Nation (infrastructure)|One Nation]] programme.<ref>"Adelaide's Millswood Station reopens after almost 20 years" ''[[Railway Digest]]'' December 2014 page 18</ref> A number of reasons were quoted as justification for the closures, including poor patronage,<ref>Former Minister of Transport [[Frank Blevins]] stated there had been consideration since 1966 to close the station because of poor patronage. In December 1994, Millswood had on average 123 passenger boardings per day. Source:Question Time, Legislative Council, 12 April 1995, p. 1933</ref> the excessive number of stations between Goodwood and Lynton and their closeness to each other, and the impractibility of single-line working with so many stations and so few crossing points. The western platform was demolished in 2007.

=== Reopening ===
In August 2013, moves were made towards the reopening of Millswood station, with the Transport Department launching an investigation into the proposal.<ref>
{{cite web|title=Transport Department to investigate the reopening of Millswood Station|work=[[Eastern Courier Messenger]]|author=John Stokes|date=29 August 2013|url=http://www.adelaidenow.com.au/messenger/east-hills/transport-department-to-investigate-the-reopening-of-millswood-station/story-fni9lkyu-1226706752938?nk=3013cb6cdf23b70e2d3cf854f0bd12fc|accessdate=2014-10-01}}</ref> In the leadup to the 2014 State Election, the [[Government of South Australia|State Government]] promised to reopen the station for a 12-month trial from 1 July 2014 if it was returned.<ref>{{cite web|title=Labor vows to reopen Millswood Station after almost 20 years if re-elected on March 15|work=Eastern Courier Messenger|author=Emmie Dowling|date= 20 February 2014|url=http://www.adelaidenow.com.au/messenger/east-hills/labor-vows-to-reopen-millswood-station-after-almost-20-years-if-reelected-on-march-15/story-fnii5yv7-1226832367146|accessdate=2014-10-01}}</ref> Upgrade works at the station commenced in July 2014 at a cost of $500,000,<ref>{{cite web|title=Work has began at Millswood station ahead of a planned trial re-opening in November|work=Eastern Courier Messenger|author=Emmie Dowling|date=7  July 2014|url=http://www.adelaidenow.com.au/messenger/east-hills/work-has-began-at-millswood-station-ahead-of-a-planned-trial-reopening-in-november/story-fnii5yv5-1226980487312|accessdate=2014-10-01  }}</ref> but the initial draft timetable for the station only included every second peak-hour train stopping at the reopened platform.<ref>
{{cite web|title=Peak hour trains to bypass Millswood station during 12-month trial re-opening under draft timetable|work=Eastern Courier Messenger|author=Emmie Dowling|date= 28 July 2014|url=http://www.adelaidenow.com.au/messenger/east-hills/peak-hour-trains-to-bypass-millswood-station-during-12month-trial-reopening-under-draft-timetable/story-fnii5yv7-1227004040358|accessdate=2014-10-01}}</ref>

The station reopened on 12 October 2014 for a 12-month trial.<ref>[http://www.abc.net.au/news/2014-10-13/millswood-station-reopens-after-almost-20-years/5808618 Adelaide's Millswood Station reopens after almost 20 years] ''[[ABC News (Australia)|ABC News]]'' 13 October 2014</ref> The trial was successful, and the station was reopened permanently.<ref>[http://www.adelaidenow.com.au/messenger/east-hills/millswood-train-station-is-set-to-remain-open-permanently-after-a-successful-year-long-trial/story-fni9lkyu-1227571788017?sv=e2ad48e8c777ce77b47c4e544171bb17 Millswood train station is set to remain open permanently, after a successful year-long trial] ''[[Adelaide Now]]'' October 16, 2015</ref>

==Transport links==
{{AdelaideMetroBusTable/Header
|stopname=7 ([[Goodwood Road, Adelaide|Goodwood Road]])}}
{{AdelaideMetroBusTable/Body
|busnumber=G10
|colour=#b57628
|textcolour=#ffffff
|details=[[Blair Athol, South Australia|Blair Athol]] – City – [[Flinders University]] – [[Westfield Marion]]<ref>[https://adelaidemetro.com.au/routes/G10 Route G10] Adelaide Metro</ref>}}
{{AdelaideMetroBusTable/Body
|busnumber=G20
|colour=#757575
|textcolour=#ffffff
|details=City – [[Flinders Medical Centre]] – [[Aberfoyle Park, South Australia|Aberfoyle Park]]<ref>[https://adelaidemetro.com.au/routes/G20 Route G20] Adelaide Metro</ref>}}
{{AdelaideMetroBusTable/Body
|busnumber=G21
|colour=#00838f
|textcolour=#ffffff
|details=City – [[Flinders Medical Centre]] – [[Aberfoyle Park, South Australia|Aberfoyle Park]]<ref>[https://adelaidemetro.com.au/routes/G21 Route G21] Adelaide Metro</ref>}}
{{AdelaideMetroBusTable/Body
|busnumber=G30F
|colour=#558b2f
|textcolour=#ffffff
|details=City – [[Bedford Park, South Australia|Bedford Park]] – [[Blackwood railway station|Blackwood]]<ref>[https://adelaidemetro.com.au/routes/G30F Route G30F] Adelaide Metro</ref>}}
{{AdelaideMetroBusTable/Body
|busnumber=N10
|colour=#f9a825
|textcolour=#000000
|details=[[Westfield Marion]] – [[Flinders Medical Centre]] – City<ref>[https://adelaidemetro.com.au/routes/N10 Route N10] Adelaide Metro</ref>}}
{{AdelaideMetroBusTable/Body
|busnumber=N21
|colour=#00838f
|textcolour=#ffffff
|details=City – [[Westfield Marion]] – [[Aberfoyle Park, South Australia|Aberfoyle Park]]<ref>[https://adelaidemetro.com.au/routes/N21 Route N21] Adelaide Metro</ref>}}
|}

==References==
{{Reflist|2}}

==External links==
*{{commons category-inline}}
*[https://www.flickr.com/photos/davo_1620/sets/72157615268154284/ Photos of Millswood station between 1984-1988] Flickr gallery

{{Belair railway line, Adelaide}}

[[Category:Railway stations in Adelaide]]
[[Category:Railway stations opened in 1910]]