{{Infobox academic
| honorific_prefix   = 
| name               = Gregory V. Jones
| honorific_suffix   = Ph.D.
| native_name        = 
| native_name_lang   = 
| image              = GregoryVJonesCROP.jpg
| image_size         = 
| alt                = 
| caption            = 
| birth_name         = <!-- Use only if different from full/othernames -->
| birth_date         = {{birth year and age|1959}}
| birth_place        = Murray, Kentucky
| death_date         = <!-- {{death date and age|YYYY|MM|DD|YYYY|MM|DD}} (death date then birth date) --> 
| death_place        = 
| death_cause        = 
| region             = 
| residence          = [[United States]]
| nationality        = American
| period             = 
| occupation         = Director and Professor
| boards             = Review Editor, Climate Research, 2004-- ; Editorial Advisory Board, Journal of Wine Economics, 2006-- ; Editorial Board, International Journal of Wine Research, 2008-- ; Editorial Advisory Board, International Journal of Biometeorology, 2012-- ; Erath Family Foundation, 2010-- <!-- Board or similar positions extraneous to main occupation - e.g. "Editoral board of The Journal of Sociology" -->
| known_for          = Climatology, Viticulture, Wine: Research on grape growing and wine production
| spouse             = 
| children           = 
| signature          = 
| signature_alt      = 
| signature_size     = 
| era                = 
| language           = 
| discipline         = <!-- Major academic discipline - e.g. Physicist, Sociologist, New Testament scholar, Ancient Near Eastern Linguist -->
| sub_discipline     = <!-- Academic discipline specialist area - e.g. Sub-atomic research, 20th Century Danish specialist, Pauline research, Arcadian and Ugaritic specialist -->
| movement           = <!-- Should match the idiologial movement or denomination (for religious), "school" of thought etc. (e.g. "Anglican", "Postmodernist", "Socialist" or "Green" etc. -->
| religion           = <!-- Religion should be supported with a citation from a reliable source -->
| denomination       = <!-- Religious denomination should be supported with a citation from a reliable source -->
| education          = University of Virginia
| alma_mater         = <!-- will often consist of the linked name of the last-attended higher education institution. -->
| thesis_title       = Synoptic Climatological Assessment of Viticultural Phenology
| thesis_url         = 
| thesis_year        = 1998
| doctoral_advisor   = 
| doctoral_students  = 
| notable_students   = 
| main_interests     = 
| workplaces         = Southern Oregon University
| notable_works      = 
| notable_ideas      = 
| influences         = 
| influenced         = 
| awards             = 
| website            = http://www.sou.edu/envirostudies/faculty/jones.html
| footnotes          =
}}
'''Gregory V. Jones''' is a research climatologist specializing in the climatology of viticulture, with a focus on how climate variation influences vine growth, wine production and the quality of wine produced. Jones serves as the Director of the Division of Business, Communication and the Environment at [[Southern Oregon University]] in [[Ashland, Oregon]] and is Professor in the University's Environmental Science and Policy Program.

==Background==
Gregory V. Jones (born 1959 in [[Murray, Kentucky]]) obtained a BA (1993) and Ph.D. (1998) in Environmental Sciences with a concentration in the Atmospheric Sciences from the [[University of Virginia]] Department of  Environmental Sciences.{{citation needed|date=December 2015}} His dissertation described research in [[Bordeaux]], France on the climatology of viticulture investigating the spatial differences in grapevine phenology, grape composition and yield, and how these are related to wine quality.<ref>{{cite journal|last1=Jones|first1=Gregory V.|title=Climate Influences on Grapevine Phenology, Grape Composition, and Wine Production and Quality for Bordeaux, France|journal=American Journal of Viticulture and Enology|date=2000|volume=51|issue=3|pages=249–261}}</ref>  Jones is best known for his research in climatology, meteorology, agriculture and hydrology; the phenology of plant systems; the interaction between biosphere and atmosphere; weather patterns and climate change; and quantitative methods in spatial and temporal analysis.<ref>{{cite web|last1=Eastman|first1=Janet|title=2009 Wine Person of the Year: Climatologist Greg Jones|url=http://oregonwinepress.com/article?articleTitle=person-of-the-year-climatologist-greg-jones--1262036157--76--|website=OregonWinePress.com|accessdate=30 October 2015}}</ref>

Jones has been invited to speak on climate and wine-related research at hundreds of regional, national and international conferences, and is a well-known applied research consultant to the grape and wine industry in Oregon. In 2014, Jones participated in a panel, ''A New World of Wine: How the Viticultural Map is Changing'' at the prestigious Institute of Masters of Wine's 8th annual conference in Florence, Italy.<ref>{{cite conference |url=https://vimeo.com/101389295|title=A New World of Wine: How the Viticultural Map is Changing (Masters of Wine Institute,8th, 2014: Session 3 |first=Christophe |last=Salin|first2=Dr. Jose|last2=Vouillamoz|first3=Professor Gregory V. |last3=Jones|first4=Frank|last4=Cornelissen |author= |author-link= |date= |year=2014 |conference= |conference-url= |editor= |others= |volume= |edition= |book-title= |publisher= |archive-url=|archive-date= |location=Florence, Italy|pages= |format= |id= |isbn= |bibcode= |oclc= |doi= |access-date= |quote= |ref= |separator= |postscript= |language= |page= |at= |trans-title= }}</ref> Jones is a regular presenter at the biennial International Terroir Congress, most recently at the Xth International Terroir Congress in Tokaj-Eger, Hungary. As organizer in 2016, Jones brings the XIth International Terroir Congress to the [[Willamette Valley]], Oregon.<ref>{{cite news|last1=Mitham|first1=Peter|title=Oregon to Host Terroir Congress International cadre of academics will share research and visit area vineyards|url=http://www.winesandvines.com/template.cfm?section=news&content=160017|accessdate=6 November 2015|work=Wines & Vines|date=October 28, 2015}}</ref>

==Research==
Jones' research focuses on the role of climate in the structure and characteristics of agricultural systems. As such he is interested in how climate influences whether a crop is suitable to a given region, how climate controls crop production and quality and ultimately drives economic sustainability. His main focus on climate's influence on agribusinesses is in viticulture and wine production where climate is arguably the most critical aspect in ripening fruit to optimum characteristics to produce a given wine style. Jones examines climate's role in growing wine grapes and wine production from a holistic perspective trying to understand 1) the weather and climate structure necessary for optimum quality and production characteristics, 2) the climate suitability to different wine grape cultivars, 3) the climate's variability in wine producing regions, and 4) the influence of climate change on the structure, suitability, and variability of climate.<ref>{{cite book|last1=Jones|first1=Gregory V.|title=Climate, Grapes, and Wine: Structure and Suitability in a Variable and Changing Climate in Percy H. Dougherty, The Geography of Wine|date=2011|publisher=Springer|location=Climate, Grapes, and Wine: Structure and Suitability in a Variable and Changing Climate|isbn=9400704631|pages=109–133}}</ref>

Jones' research has been described and his expertise noted in wine trade publications and websites including: ''[[Wine & Spirits]]'' magazine (May 2015),<ref>{{cite news|last1=Darlington|first1=David|title=Accounting for Taste: What happened to Russian River Valley Pinot Noir?|url=http://www.wineandspiritsmagazine.com/S=0/news/entry/accounting-for-taste|accessdate=4 November 2015|work=[[Wine & Spirits]]|date=May 18, 2015}}</ref> ''[[The Oregonian]]'' newspaper (May 2015),<ref>{{cite news|last1=Harbarger|first1=Molly|title=Pinot noir celebrates 50 years in Willamette Valley. Will there be 50 more?|url=http://www.oregonlive.com/business/index.ssf/2015/05/pinot_noir_celebrates_its_gold.html|accessdate=4 November 2015|work=[[The Oregonian]]|date=May 23, 2015}}</ref> ''Southern Oregon Wine Scene Magazine'' (Summer 2015),<ref>{{cite news|last1=Daspit|first1=MJ|title=One COOL Conversation with Greg Jones|url=http://jacksonvillereview.com/one-cool-conversation-with-greg-jones-by-mj-daspit/|accessdate=4 November 2015|work=Southern Oregon Wine Scene Magazine|publisher=''Jacksonville Review''|date=Summer 2015}}</ref> ''Great Northwest Wine'' (April 2015),<ref>{{cite news|last1=Degerman|first1=Eric|title=Oregon wine climatologist Greg Jones speaks to scientists in Columbia Valley|url=http://www.greatnorthwestwine.com/2015/04/03/oregon-wine-climatologist-greg-jones-speaks-to-scientists-in-columbia-valley/|accessdate=4 November 2015|publisher=Great Northwest Wine|date=April 3, 2015}}</ref> [[Slate.com]] (December 2014),<ref>{{cite news|last1=Miller|first1=Carrie|title=Pinot Noir Is Wine's Polar Bear: The opportunities and challenges that climate change presents to vintners|url=http://www.slate.com/articles/technology/future_tense/2014/12/wine_and_climate_change_pinot_noir_is_the_vintner_s_polar_bear.html|accessdate=4 November 2015|issue=December 23, 2014|website=[[Slate.com]]}}</ref> Vine2Wine (March 2014),<ref>{{cite news|last1=Staff|title=Climatologist Greg Jones Speaks at U of O on terroir|url=http://www.oregonvine2wine.com/2014/03/14/climatologist-greg-jones-speaks-at-u-of-o-on-terroir/|accessdate=4 November 2015|work=Vine2Wine|date=March 14, 2014}}</ref> Wine-Searcher (November 2013),<ref>{{cite news|last1=Cole|first1=Katherine|title=Greg Jones Turns Up The Heat on Wine & Climate Change|url=http://www.wine-searcher.com/m/2013/11/expert-turns-up-the-heat-on-wine-and-climate-change|accessdate=4 November 2015|website=Wine-Searcher|date=November 20, 2013}}</ref> ''Willamette Week'' newspaper (September 2013),<ref>{{cite news|last1=Staff|title=Hotseat: The Third Degree A climatologist describes how global warming will alter the wine industry|url=http://www.wweek.com/portland/article-21123-hotseat_the_third_degree.html|accessdate=4 November 2015|work=Willamette Week|date=September 10, 2013}}</ref> ''Wines and Vines'' magazine (November 2012),<ref>{{cite news|last1=Tourney|first1=Jon|title=Conference Explores Science of Wine Terroir Speakers at U.C. Davis say science supports link between climate and wine quality, but not soil-minerality link|url=http://www.winesandvines.com/template.cfm?section=news&content=107550|accessdate=4 November 2015|work=Wines & Vines|date=November 13, 2012}}</ref> and the ''[[Mail Tribune]]'' newspaper (June 2009).<ref>{{cite news|last1=Darling|first1=John|title=SOU professor Greg Jones makes wine 'Power List' -  Magazine puts him on its list for his studies into climate change's effect on the industry|url=http://www.mailtribune.com/article/20090604/News/906040328|accessdate=4 November 2015|work=Mail Tribune|date=June 4, 2009}}</ref>

==Professional affiliations and service==
* [http://www.int-res.com/journals/cr/editors/ Review Editor], [[Climate Research (journal)]], 2004– 
* Board, Erath Family Foundation, 2010–
* Editorial Advisory Board, [[International Journal of Biometeorology]], 2012– 
* [http://journals.cambridge.org/action/displayMoreInfo?jid=JWE&type=eb&sessionId=D8AE15BF2A325DAB9DA9624F30C52113.journals Editorial Advisory Board], Journal of Wine Economics, 2006– 
* [https://www.dovepress.com/journal-editor-international-journal-of-wine-research-eic52 Editorial Board], International Journal of Wine Research, 2008– 
* [[American Association of Wine Economists]], 2006– 
* [[American Geophysical Union]], 2004– 
* [[American Meteorological Society]], 1993– 
* [[American Society for Enology and Viticulture]], 1999– 
* [[Association of American Geographers]], 1993– 
* [[Association of Pacific Coast Geographers]], 1998– 
* [[International Society of Biometeorology]], 1995– 
* [[International Society of Horticultural Science]], 2005– 
* Oregon Horticultural Society, 2000– 
* Oregon Winegrape Growers Association, 1997– 
* Rogue Valley Winegrowers Association, 1997– 
* [[Sigma Xi]], 1993– (local chapter Vice President, 1999–00, President, 2000–01) 
* Umpqua Valley Winegrowers Association, 2000–

==Honors and awards==
In 2016, Gregory V. Jones was named Honorary Confrade with the Rank of Infanção (Nobleman) by the Confraria do Vinho do Porto for his work with the Portuguese wine industry.<ref>{{cite web|author1=Daspit, MJ |title=Wine Honors for SOU's Greg Jones Reach Worldwide|url=http://www.dailytidings.com/news/20161208/wine-honors-for-sous-greg-jones-reach-worldwide|accessdate=1 February 2016|work=Ashland Daily Tidings|date=December 8, 2016}}</ref> Also in 2016, Jones was included as one of Wine Business Monthly's Top 50 Wine Industry Leaders for 2016<ref>{{cite web|title=Top 50 Wine Industry Leaders|url=https://www.winebusiness.com/blog/?go=getBlogEntry&cms_preview=true&dataid=177343|website=Wine Business Monthly|publisher=Wine Communications Group|accessdate=1 February 2017}}</ref>.

In 1998 and 2004 Jones was  awarded a Prix Local by the Vineyard Data Quantification Society, an international organization of economists in service to vine and wine.<ref>{{cite web|author1=Ashenfelter, Orley  |author2=Greg Jones|title=The Demand for Expert Opinion: Bordeaux Wine|url=http://www.vdqs.net/fr/prix/index.asp|website=Vineyard Data Quantification Society Oenometrics Prize: Prix d'Ajaccio, 1998|accessdate=3 November 2015}}</ref>

Jones contributed to the 4th IPCC Assessment Report for the [[Intergovernmental Panel on Climate Change]], which shared a 2007 [[Nobel Peace Prize]] with [[Al Gore]].<ref>{{cite book|last1=Field, C.B., L.D. Mortsch,, M. Brklacich, D.L. Forbes, P. Kovacs, J.A. Patz, S.W. Running and M.J. Scott|title=North America. Climate Change 2007: Impacts, Adaptation and Vulnerability. Contribution of Working Group II to the Fourth Assessment Report of the Intergovernmental Panel on Climate Change|date=2007|publisher=Cambridge University Press|location=Cambridge, England|isbn=9780521705974|url=https://www.ipcc.ch/publications_and_data/ar4/wg2/en/ch14.html}}</ref>

Decanter Magazine named Jones to its 2009 Power List, counting him among the top fifty influencers in the wine world,<ref>{{cite web|last1=Woodward|first1=Guy|title=The Decanter Power List 2009 – Top 10, New Entries and Fallers|url=http://www.decanter.com/specials/the-decanter-power-list-2009-top-10-new-entries-and-fallers-70815/|website=Decanter|publisher=Time Inc UK|accessdate=30 October 2015}}</ref> and IntoWine.com included Jones in their top one hundred most influential people in the US wine industry in 2012 and 2013.<ref>{{cite web|last1=Cervin|first1=Michael|title=The IntoWine.com 2nd Annual "Top 100 Most Influential People in the U.S. Wine Industry" – 2013|url=http://www.intowine.com/intowinecom-2nd-annual-%E2%80%9Ctop-100-most-influential-people-us-wine-industry-%E2%80%93-2013|website=IntoWine.com|accessdate=1 October 2015}}</ref>. 

Jones was also named Oregon Wine Press's 2009 Wine Person of the Year,<ref>{{cite web|last1=Eastman|first1=Janet|title=2009 Wine Person of the Year: Climatologist Greg Jones|url=http://oregonwinepress.com/article?articleTitle=person-of-the-year-climatologist-greg-jones--1262036157--76--|website=OregonWinePress.com|accessdate=30 October 2015}}</ref> has been featured in the Linfield College History of Wine Archives,<ref>{{cite web|last1=Jones|first1=Gregory V.|title=Oral History from the Linfield College History of Wine Archives|url=https://www.youtube.com/watch?v=phKvT28hdO0&feature=youtu.be|website=YouTube.com|publisher=Linfield College|accessdate=30 October 2015}}</ref> and is included in the Southern Oregon University's Hannon Library's Wine of Southern Oregon Digital Archives.<ref>{{cite web|author1=Southern Oregon University Hannon Library|title=Gregory V. Jones|url=http://digital.hanlib.sou.edu/cdm/search/collection/p16085coll8/searchterm/gregory%20v.%20jones/order/winery|work=Wine of Southern Oregon Digital Archives|publisher=Southern Oregon University|accessdate=6 November 2015}}</ref>

==Selected publications==
Jones is the author of numerous book chapters, reports, and journal articles covering topics of climate, soil, phenology, economics, and sustainability as they pertain to viticulture and wine production. Book contributions include chapters in: 
* Jones, Gregory V.; White, Michael A; Diffenbaugh, Noah S. (2009). "Climate Variability,Climate Change, and Wine Production in the Western United States". In Wagner, Frederic H. Climate Warming in Western North America: Evidence and Environmental Effects. University of Utah Press. ISBN 0874809061. 
* Gregory V Jones (2011). "Climate, Grapes, and Wine: Structure and Suitability in a Variable and Changing Climate". In Dougherty, Percy H. The Geography of Wine. Dordrecht: Springer. pp.&nbsp;109–133. ISBN 9400704631. 
* Tomasi, Diego; Gaiotti, Federica; Jones, Gregory V (2013). The Power of the Terroir : the Case Study of Prosecco Wine. Basel: Springer. ISBN 3034806272. 
* Jones, Gregory V. (2013). "Winegrape Phenology". In Schwartz, Mark. Phenology : an Integrative Environmental Science (Second edition. ed.). Dordrecht: Springer. pp.&nbsp;563–584. ISBN 9400769245.

Jones' writing has been published in major scientific journals. Selected publications are cited below.

* Jones, G.V. and Davis, R.E. (2000). Climate Influences on Grapevine Phenology, Grape Composition, and Wine Production and Quality for Bordeaux, France, ''American Journal of Viticulture and Enology'', 51,No.3:249-261.
* Jones, G.V. and Storchmann, K-H. (2001). Wine market prices and investment under uncertainty: an econometric model for Bordeaux Crus Classés, ''Agricultural Economics'', 26:115-133.
* Nemani, R.R., White, M.A., Cayan, D.R., Jones, G.V., Running, S.W., and J.C. Coughlan, (2001). Asymmetric climatic warming improves California vintages. ''Climate Research'', Nov. 22, 19(1):25-34.
* Jones, G.V., White, M.A., Cooper, O.R., and Storchmann, K., (2005). Climate Change and Global Wine Quality. ''Climatic Change'', 73(3): 319-343.
* White, M.A., Diffenbaugh, N.S., Jones, G.V., Pal, J.S., and F. Giorgi (2006). Extreme heat reduces and shifts United States premium wine production in the 21st century. ''Proceedings of the National Academy of Sciences'', 103(30): 11217–11222.
* Jones, G.V. and Goodrich, G.B., (2008). Influence of Climate Variability on Wine Region in the Western USA and on Wine Quality in the Napa Valley. ''Climate Research'', 35: 241-254.
* Hall, A. and G.V. Jones (2009): Effect of potential atmospheric warming on temperature based indices describing Australian winegrape growing conditions. ''Australian Journal of Grape and Wine Research'', 15(2):97-119.
* White, M.A., Whalen, P, and G.V. Jones (2009). Land and Wine. ''Nature Geoscience'', 2:82-84.
* Jones, G.V., M. Moriondo, B. Bois, A. Hall, and A. Duff. (2009): Analysis of the spatial climate structure in viticulture regions worldwide. ''Le Bulletin de l'OIV'' 82(944,945,946):507-518.
* Jones, G.V, Duff, A.A., Hall, A., and J. Myers (2010). Spatial analysis of climate in winegrape growing regions in the western United States. ''American Journal of Enology and Viticulture'', 61:313-326.
* Hall, A. and G.V. Jones (2010). Spatial analysis of climate in winegrape growing regions in Australia. ''Australian Journal of Grape and Wine Research'', 16:389-404.
* Jones, G.V. and Webb, L.B. (2010) Climate Change, Viticulture, and Wine: Challenges and Opportunities. ''Journal of Wine Research'', 21: 2(103-106).
* Schultz, H.R. and Jones, G.V. (2010). Climate Induced Historic and Future Changes in Viticulture. ''Journal of Wine Research'', 21:2(137-145).
* Tomasi, D., Jones, G.V., Giust, M., Lovat, L. and F. Gaiotti (2011). Grapevine Phenology and Climate Change: Relationships and Trends in the Veneto Region of Italy for 1964-2009. ''American Journal of Enology and Viticulture'', 62:3(329-339).
* Borges, J., Real, A.C., Cabral, J.S., and G.V. Jones (2012). A new method to obtain a consensus ranking of a region's vintage quality. ''Journal of Wine Economics'', 7(1):88-107.
* Anderson, J.D., Jones, G.V., Tait, A., Hall, A. and M.T.C. Trought (2012). Analysis of viticulture region climate structure and suitability in New Zealand. ''International Journal of Vine and Wine Sciences'', 46(3):149-165.
* Malheiro, A.C., Santos, J.A., Pinot, J.G., and G.V. Jones (2012). European viticulture geography in a changing climate. ''Bulletin l'OIV'', 85(971-972-973): 15-22.
* Moriondo, M., Jones, G.V., Bois, B., Dibari, C., Ferrise, R., Trombi, G., and M. Bindi (2013). Projected shifts of wine regions in response to climate change. ''Climatic Change'', 119(3-4): 825-839.
* Koufos, G., Mavromatis, T., Koundouras, S., Fyllas, N.M, and G.V Jones (2013). Viticulture–climate relationships in Greece: the impacts of recent climate trends on harvest date variation. ''International Journal of Climatology'', 34(5):1445-1459, DOI: 10.1002/joc.3775
* Ashenfelter, O. and Jones, G.V. (2013). The Demand for Expert Opinion: Bordeaux Wine. ''Journal of Wine Economics'', 8(3): 285-293,  DOI:10.1017/jwe.2013.22
* Baciocco, K.A., David, R.E., and G.V. Jones (2014). Climate and Bordeaux wine quality: identifying the key factors that differentiate vintages based on consensus rankings. ''Journal of Wine Research'', 25(2):75-90, DOI: 10.1080/09571264.2014.888649

==References==
{{reflist|30em}}

{{DEFAULTSORT:Jones, Gregory V.}}
[[Category:1959 births]]
[[Category:American non-fiction environmental writers]]
[[Category:Living people]]
[[Category:Climate change and agriculture]]
[[Category:People from Murray, Kentucky]]
[[Category:Southern Oregon University faculty]]
[[Category:University of Virginia alumni]]
[[Category:Sigma Xi]]
[[Category:People associated with wine]]
[[Category:Viticulture]]