{{Infobox writer  
| name          = Paul Hervieu
| image         = Paul Hervieu 7.jpg
| alt           = 
| caption       = Hervieu in 1899
| pseudonym     = 
| birth_name    = Paul-Ernest Hervieu
| birth_date    = {{birth date|1857|9|2|df=yes}}
| birth_place   = [[Neuilly-sur-Seine]], [[France]]
| death_date    = {{death date and age|1915|10|25|1857|9|2|df=yes}}
| death_place   = [[Paris]], [[France]] 
| resting_place = [[Passy Cemetery]]<br />[[Paris]], [[France]]
| occupation    = [[Novelist]] and [[playwright]]
| language      = [[French language|French]]
| nationality   = 
| ethnicity     = 
| citizenship   = 
| alma_mater    = 
| period        = 1892&ndash;1905
| genre         = 
| subject       = 
| movement      = 
| notableworks  = 
| spouse        = 
| partner       = 
| children      = 
| relatives     = 
| influences    = 
| influenced    = 
| awards        = 
| signature     = 
| signature_alt = 
}}

'''Paul Hervieu''' (2 September 1857{{spaced ndash}}25 October 1915) was a [[French people|French]] [[novelist]] and [[playwright]].

==Early years==
He was born '''Paul-Ernest Hervieu'''  in [[Neuilly-sur-Seine]], France.
Hervieu was born into a wealthy upper-middle-class family. 
He studied law, but sought also had contact with writers like [[Leconte de Lisle]], [[Paul Verlaine]] and [[Alphonse Daudet]]. After graduating in 1877, he first practiced in a law firm, in 1879 qualified for the diplomatic service, and was posted in the French Embassy in Mexico. 
But he preferred to remain in France, where he attended fashionable literary salons, and the acquaintance of artists and writers such as [[Marcel Proust]], [[Paul Bourget]], [[Henri Meilhac]], [[Ludovic Halévy]], [[Guy de Maupassant]] and [[Edgar Degas]]. On the recommendation of his friend Octave Mirbeau, he tried his hand as a journalist.

==Career==
Hervieu was [[Call to the bar|called to the bar]] in 1877, and, after serving some time in the office of the president of the council, he qualified for the [[diplomatic service]], but resigned on his nomination in 1881 to a secretaryship in the French [[legation]] in [[Mexico]].<ref name=EB1911>{{EB1911|inline=1|wstitle=Hervieu, Paul|volume=13|page=405}} This in turn cites or notes:
*[[Alfred Binet]], in ''[[L'Année psychologique]]'', vol. x.
*Hervieu's ''Théâtre'' was published by [[Alphonse Lemerre|Lemerre]] (3 vols, 1900–1904).</ref>

He contributed novels, tales and essays to the chief [[Paris]]ian papers and reviews, and published a series of clever novels, including ''[[L'Inconnue]]'' (1887), ''[[Flirt (novel)|Flirt]]'' (1890), ''[[L'Exorcisée]]'' (1891), ''[[Peints par eux-mêmes]]'' (1893), an ironic study [[Epistolary novel|written in the form of letters]], and ''[[L'Armature]]'' (1895), dramatized in 1905 by [[Eugène Brieux]].<ref name=EB1911/>

Hervieu's plays are built upon a severely logical method, the mechanism of which is sometimes so evident as to destroy the necessary sense of illusion. The closing words of ''[[La Course du flambeau]]'' (1901) "Pour ma fille, j'ai tué ma mère" (For my daughter, I killed my mother), are an example of his selection of a plot representing an extreme theory. 
The riddle in ''[[L'Énigme]]'' (1901) (staged at [[Wyndham's Theatre]], [[London]], 1 March 1902, as ''Caesar's Wife'') is, however, worked out with great art, and ''[[Le Dédale]]'' (1903), dealing with the obstacles to the remarriage of a divorced woman, is reckoned among the masterpieces of the modern French stage.<ref name=EB1911/> He produced his last play, ''Le Destin est Maître'', in 1914.<ref>{{EB1922|inline=1| title=Hervieu, Paul| volume=31 |page=369 |url=https://archive.org/stream/encyclopaediabri31chisrich#page/369/mode/1up}}</ref>

===Honours===
He was elected to the [[Académie française]] in 1900.<ref name=EB1911/>

==Death==
Hervieu died at age 57 in [[Paris]], France, and was interred in its [[Passy Cemetery]].

==Bibliography==
{{inc-lit|date=January 2012}}
*''[[Les Paroles restent]]'' (Vaudeville, 17 November 1892)
*''[[Les Tenailes]]'' ([[Théâtre Français]], 28 September 1895)
*''[[La loi de l’homme]]'' ([[Théâtre Français]], 15 February 1897)
*''[[La Course du flambeau]]'' (Vaudeville, 17 April 1901)
*''[[Point de lendemain]]'' ([[Théâtre de l'Odéon]], 18 October 1901), a dramatic version of a story by [[Dominique Vivant|Vivant Denon]]
*''[[L'Énigme]]'' ([[Théâtre Français]], 5 November 1901)
*''[[Théroigne de Méricourt (play)|Théroigne de Méricourt]]'' ([[Théâtre Sarah Bernhardt]], 23 September 1902)
*''[[Le Dédale]]'' ([[Théâtre Français]], 19 December 1903)
*''Le Réveil'' ([[Théâtre Français]], 18 December 1905)

==See also==
{{portal|Biography|French and Francophone literature|Paris|Theatre}}
*[[List of French-language authors]]
*[[List of French novelists]]
*[[List of French playwrights]]
*[[List of members of the Académie française]]
{{clear}}

==References==
{{Reflist}}

==External links==
* {{Gutenberg author |id=Hervieu,+Paul | name=Paul Hervieu}}
* {{Internet Archive author |sname=Paul Hervieu}}

{{Académie française Seat 12}}

{{Authority control}}

{{DEFAULTSORT:Hervieu, Paul}}
[[Category:1857 births]]
[[Category:1915 deaths]]
[[Category:People from Neuilly-sur-Seine]]
[[Category:19th-century French novelists]]
[[Category:20th-century French writers]]
[[Category:Writers from Île-de-France]]
[[Category:19th-century French dramatists and playwrights]]
[[Category:20th-century French dramatists and playwrights]]
[[Category:Members of the Académie française]]
[[Category:Burials at Passy Cemetery]]
[[Category:French male novelists]]
[[Category:19th-century male writers]]