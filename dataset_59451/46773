{{EngvarB|date=August 2014}}
{{Use dmy dates|date=August 2014}}
{{Infobox person
| honorific_prefix          =
| name                      = Martyn Lawrence Bullard
| honorific_suffix          =
| native_name               =
| native_name_lang          =
| image                     =File:MartynLawrenceBullardPortrait.jpg
| image_size                =
| alt                       =
| caption                   =
| birth_name                = Martyn Lawrence Bullard
| birth_date                = {{birth date and age|df=yes|1967|03|30}}
| baptism_date              =
| birth_place               = London England
| disappeared_date          =
| disappeared_place         =
| disappeared_status        =
| death_date                =
| death_place               =
| death_cause               =
| body_discovered           =
| resting_place             =
| resting_place_coordinates =
| monuments                 =
| residence                 = [[Los Angeles, CA]]
| nationality               = British
| other_names               =
| ethnicity                 =
| citizenship               =
| education                 =
| alma_mater                =
| occupation                = Interior designer, author, television personality
| years_active              =
| employer                  =
| organization              = Martyn Lawrence Bullard Design
| agent                     =
| known_for                 =
| notable_works             = ''Live, Love & Decorate''
| style                     =
| home_town                 =
| salary                    =
| net_worth                 =
| height                    =
| weight                    =
| television                = ''[[Million Dollar Decorators]]'' ''Hollywood Me''
| boards                    =
| religion                  =
| denomination              =
| spouse                    =
| partner                   = 
| children                  =
| parents                   =
| relatives                 =
| awards                    = [[Andrew Martin International]] Interior Designer of the Year <br />''[[Architectural Digest]]'' AD100<br />''[[Elle Decor]]'' Top 25 Designers
| signature                 =
| signature_alt             =
| signature_size            =
| website                   = {{URL|martynlawrencebullard.com}}
| footnotes                 =
| box_width                 =
}}

'''Martyn Lawrence Bullard''' is a British interior designer in Los Angeles, author, and television personality.<ref name=Times>{{cite news|last=McKeough|first=Tim|title=A Celebrity Designer in the Service of Glamour|url=https://www.nytimes.com/2011/05/26/garden/qa-martyn-lawrence-bullard.html|work=May 25, 2011|publisher=New York Times|accessdate=16 September 2013|date=25 May 2011}}</ref> Named as one of the world's top interior designers by ''[[Architectural Digest]],''<ref name=Telegraph>{{cite news|last=Walden|first=Celia|title=Meet Mr Lawrence-Bullard, Hollywood's favourite interior designer|url=http://www.telegraph.co.uk/property/interiorsandshopping/10075876/Meet-Mr-Lawrence-Bullard-Hollywoods-favourite-interior-designer.html|work=May 29, 2013|publisher=The Telegraph|accessdate=16 September 2013|location=London|date=29 May 2013}}</ref> Bullard stars in both the [[Bravo (U.S. TV channel)|Bravo]] series ''[[Million Dollar Decorators]]'' and [[Channel 4]]'s ''Hollywood Me.''<ref name="Baltimore Sun">{{cite web|last=Reimer|first=Susan|title=Bravo's 'Million Dollar Decorator' dishes out inspiration, advice|url=http://articles.baltimoresun.com/2012-02-16/features/bs-hm-million-dollar-decorator-20120213_1_interior-design-movie-star-elle-decor|work=February 16, 2012|publisher=Baltimore Sun|accessdate=16 September 2013}}</ref><ref>{{cite web|title=Hollywood Me – Channel 4|url=http://www.channel4.com/programmes/hollywood-me|work=2013|publisher=Channel 4|accessdate=17 September 2013}}</ref>

== Early life ==
Born in London, Martyn Lawrence Bullard began his career in design while in his teens.  His father, an actor turned businessman, would accompany his son to the Greenwich Antiques Market in South London, where he rented a stall.  At 13, Bullard began to buy and sell what he called "oddments,"  and learned to identify decorator items that were valuable or well-designed.<ref name="Los Angeles Times" /> By the time he was 16, he had developed a clientele of prominent dealers and collecters, including the head buyer for [[Ralph Lauren]] Antiques, who shipped Bullard's pieces to the United States to dress the windows of Lauren's [[Madison Avenue]] and [[Rodeo Drive]] boutiques.<ref>{{cite news|last=Barnett|first=Meredith|title=Million Dollar Decorator: Martyn Lawrence Bullard|url=http://www.huffingtonpost.com/meredith-barnett/million-dollar-decorator-martyn_b_1077245.html|work=November 7, 2011|publisher=Huffington Post|accessdate=17 September 2013|date=7 November 2011}}</ref>

At 17, Bullard decided to pursue a career as an actor, and used his earnings to attend the [[Lee Strasberg]] Actors Studio in London's [[Covent Garden]].  While in school, Bullard continued to buy and sell antiques and raised enough money to move to Los Angeles in 1994.<ref name=Telegraph /><ref name="Evening Standard">{{cite web|last=Law|first=Katie|title=Martyn Lawrence Bullard: I've built my entire career on being a London boy in Hollywood|url=http://www.standard.co.uk/lifestyle/london-life/martyn-lawrence-bullard-ive-built-my-entire-career-on-being-a-london-boy-in-hollywood-8656723.html|work=June 13, 2013|publisher=London Evening Standard|accessdate=17 September 2013}}</ref><ref name=China>{{cite web|last=Tsang|first=Jacqueline|title=Seeing stars: Interior designer Martyn Lawrence Bullard on making it big|url=http://www.scmp.com/magazines/style/article/1159091/seeing-stars-interior-designer-martyn-lawrence-bullard-making-it-big|date=March 2013|publisher=South China Morning Post|accessdate=18 September 2013}}</ref>

== Career ==

=== Interior design ===
After moving to Los Angeles, Bullard was cast in several roles, including a part as [[Eartha Kitt]]'s [[Age disparity in sexual relationships|boy toy]] in [[Ed Wood]]'s final film,  ''[[I Woke Up Early The Day I Died]].''   One of the film's producers visited Bullard's home, and, impressed by its decor, invited Bullard to decorate his office.  From there, Bullard was hired to decorate the office and home of [[Liz Heller]], a Capitol Records executive, and was eventually asked to help create Heller's wedding to producer [[John Bard Manulis]]. As a guest at the event, he was seated next to [[Cheryl Tiegs]], who became his first celebrity client. Together, Tiegs and Bullard transformed Tiegs' [[Bel Air, Los Angeles|Bel Air]] home into a "[[Bali]]nese-inspired pavilion," which was featured in publications worldwide, and appeared on the cover of more than a dozen magazines.<ref name="Los Angeles Times">{{cite news|last=Beale|first=Lauren|title=Interior designer knows stars' homes inside out|url=http://articles.latimes.com/2013/may/10/business/la-fi-himi-bullard-20130512|work=May 10, 2013|publisher=Los Angeles Times|accessdate=17 September 2013|date=10 May 2013}}</ref>
After his success with Cheryl Tiegs' house, Bullard began to build a celebrity clientele. His early clients included [[Rebecca Romijn]] and [[John Stamos]], [[Christina Aguilera]], [[William H. Macy]] and [[Felicity Huffman]], [[Eva Mendes]], [[Patti LaBelle]], and [[Edward Norton]].<ref name=Times /> Later, he decorated homes for [[Cher]], [[Ellen Pompeo]], [[Sharon Osbourne|Sharon]] and [[Ozzy Osbourne]], [[Aaron Sorkin]], [[Kid Rock]], and [[Tommy Hilfiger]], among others.   While best known for high-end residential design, Bullard has additionally worked on commercial properties, including the [[Colony Palms Hotel]]<ref name="Elle Martyn Home">{{cite web|last=Keeps|first=David|title=STAR TURN: MARTYN LAWRENCE BULLARD'S HOLLYWOOD HOME|url=http://www.elledecor.com/celebrity-style/homes/million-dollar-decorators-martyn-lawrence-bullard-entry#slide-2|work=2011|publisher=Elle Decor|accessdate=19 September 2013}}</ref> and the [[Jimmy Choo]] boutiques in London and [[Osaka]], Japan.<ref name=HuffPo>{{cite news|last=Carreon|first=Blue|title=Martyn Lawrence-Bullard Interview on Fashion And Interior Design|url=http://www.huffingtonpost.com/2011/05/26/martyn-lawrence-bullard-i_n_867248.html|work=May 26, 2011|publisher=Huffington Post|accessdate=19 September 2013|date=26 May 2011}}</ref> Bullard is currently designing several luxury resorts and restaraurants, including the [[Château Gütsch|Château Gütsch Hotel]] in [[Lucerne|Lucerne, Switzerland]], and Red O Restaurant in [[Newport Beach, California]].<ref name="Elle Martyn Home" /><ref name="HuffPo" />

In 2010, Bullard won the "Oscar of the Interior Design World," the Andrew Martin International Designer of the Year Award.<ref>{{cite web|title=The Story|url=http://www.andrewmartin.co.uk/the-story/|work=2010|publisher=Andrew Martin International|accessdate=19 September 2013}}</ref> In 2011, Rizzolli published Bullard's book, ''Live, Love & Decorate,'' which reached its third reprint in its first 16 weeks of release, and topped the best-seller list in the interior design genre.<ref>{{cite web|last=Kuntz|first=Barbara|title=Million Dollar Decorator Martyn Lawrence Bullard spills his celeb design secrets & offers tips for your own home|url=http://houston.culturemap.com/news/home_design/03-21-12-million-dollar-decorator-martyn-lawrence-bullard-spills-his-celeb-design-secrets-tips-for-your-own-home/|work=March 21, 2012|publisher=Culture Map Houston|accessdate=19 September 2013}}</ref><ref name=About>{{cite web|last=Flanagan|first=Lauren|title=Books to Cherish: Live, Love & Decorate|url=http://interiordec.about.com/od/decoratingbook1/a/Books-To-Cherish-Live-Love-And-Decorate.htm|work=2010|publisher=About.com: Interior Decorating|accessdate=16 September 2013}}</ref> The book featured 16 of Bullard's projects, including Cher's "fantasy villa," and Tamara Mellon's $30 million Manhattan penthouse.<ref>{{cite web|title=Expert Touch|url=http://glo.msn.com/living/million-dollar-decorators-before-and-after-7111.gallery|work=2012|publisher=MSN|accessdate=19 September 2013}}</ref> The book also included Bullard's home in the [[Whitley Heights, Los Angeles|Whitley Heights]] neighbourhood of Los Angeles, which was previously owned by [[Rudolph Valentino]], [[Gloria Swanson]], and [[William Faulkner]].<ref>{{cite web|last=Keeps|first=David|title=STAR TURN: MARTYN LAWRENCE BULLARD'S HOLLYWOOD HOME|url=http://www.elledecor.com/celebrity-style/homes/million-dollar-decorators-martyn-lawrence-bullard#slide-1|work=March, 2012|publisher=Elle Decor|accessdate=10 October 2013}}</ref>
[[File:HollywoodatHomebyMartynLawrenceBullard1.jpg|thumb|left]][[File:MartynLawrenceBullardHollywoodatHome2.jpg|thumb|left|Ellen Pompeo's home, from Martyn Lawrence Bullard's ''Live, Love & Decorate''<br>Photo by Tim Street-Porter]]

==== Style ====

Bullard's style has been described as "Hollywood glamour meets ethnic exotica,"<ref>{{cite web|last=Keeps|first=David|title=STAR TURN: MARTYN LAWRENCE BULLARD'S HOLLYWOOD HOME|url=http://www.elledecor.com/celebrity-style/homes/million-dollar-decorators-martyn-lawrence-bullard#slide-1|work=2011|publisher=Elle Decor|accessdate=21 September 2013}}</ref> and "eclectic, luxurious, sophisticated, comfortable and fearless."<ref>{{cite web|title=Interview with Martyn Lawrence-Bullard|url=http://design-elements-blog.com/2011/02/01/interview-with-martyn-lawrence-bullard/|work=February 1, 2012|publisher=Design Elements|accessdate=21 September 2013}}</ref> He has been noted for his "adventurous use of texture, colour and extreme attention to detail,"<ref name=Palace>{{cite web|last=Norum|first=Roger|title=Martyn Lawrence Bullard – WALLS OF FAME|url=http://www.palacemagazine.asia/palace-story_201101_05-PalaceStyle_martyn-lawrence-bullard-walls-of-fame.html|work=2010|publisher=Palace Magazine|accessdate=21 September 2013}}</ref> and his "strong command of proportion and scale, a deft hand with color and pattern, and an exuberantly global sensibility."<ref name=AD100>{{cite web|title=2012 AD100: MARTYN LAWRENCE BULLARD DESIGN|url=http://www.architecturaldigest.com/AD100/2012/martyn-lawrence-bullard-design-ad100-profile|work=2012|publisher=Architectural Digest|accessdate=21 September 2013}}</ref>

=== Television ===
Bullard is a star of ''Million Dollar Decorators,'' a Bravo docu-series which airs in 62 countries.  Examining the world of high-end interior designers, ''Million Dollar Decorators'' premiered in 2011.<ref>{{cite web|title=Million Dollar Decorators Season 1|url=http://www.bravotv.com/million-dollar-decorators/season-1|work=2011|publisher=Bravo|accessdate=20 September 2013}}</ref> In a 2012 interview, Bullard, whose appearances on the show frequently feature his celebrity clients, said "Celebrities live their lives in the spotlight. As such, it's vital for their homes to be sanctuaries – places for complete relaxation and privacy. My one great joy with my celebrity clients over the years has been their passion to live out their fantasies, be it an Indian palace in Hollywood, a Tuscan villa in Malibu or an English country cottage in New York City. I'm here to be their enabler and make these fantasies into decorative reality."<ref name="Baltimore Sun" />

Bullard also stars in ''Hollywood Me,'' a series which brings unsuspecting Brits to Los Angeles for personal makeovers while Bullard redecorates their homes.  ''Hollywood Me, ''which won a National Reality Television Award in its inaugural season,<ref>{{cite web|title=National Reality TV Awards 2013: winners in pictures|url=http://tv.uk.msn.com/features/national-reality-tv-awards-2013-luisa-zissman-rylan-clark-and-towie-win?page=8|work=September 17, 2013|publisher=National Reality Television Awards|accessdate=3 October 2013}}</ref> launched in the UK in 2012 and will air in South Africa and Australia in 2013.<ref>{{cite web|title=Hollywood Me|url=http://www.channel4.com/programmes/hollywood-me|work=2013|publisher=Channel Four|accessdate=20 September 2013}}</ref> He appears frequently on ''[[The Talk (TV series)|The Talk]]'', and in ''Hollywood at Home'' design segments on''This Morning'' on [[ITV (TV network)|ITV]].  Bullard portrayed himself on ''[[The Young and the Restless]]'' (2008 and 2012), and on the television series ''[[Jane By Design]]'' (2012). He was also featured on [[TLC (TV network)|TLC's]] ''Material World'' in 2008.<ref>{{cite web|title=Martyn Lawrence Bullard at IMDb|url=http://www.imdb.com/name/nm3037745/|publisher=IMDb|accessdate=20 September 2013}}</ref><ref>{{cite web|last=Bentley|first=Jean|title='Jane By Design': Spoilers on the second half of Season 1|url=http://blog.zap2it.com/frominsidethebox/2012/05/jane-by-design-spoilers-on-the-second-half-of-season-1.html|work=May 30, 2012|publisher=Zap2It|accessdate=2 October 2013}}</ref>

=== Other enterprises ===
In 2010, Bullard introduced a fabric collection he designed in collaboration with Schumacher, a high-end interior design retailer; he won the ''UK Focus'' Best of the Best Fabric Designer Award in 2011 and 2012 for the collection, which features prints and wovens derived from antique textiles, motifs and patterns.  Bullard has licensing agreements for porcelain, crystal, rugs, tiles, flooring, wallpaper and fabrics with luxury design companies, and has launched his own lines of furniture, fabric, and wallpaper collections, as well as home fragrances, jewellery, and clothing.<ref>{{cite news|last=Douglas|first=Danielle|title=Martyn Lawrence-Bullard talks design|url=http://articles.washingtonpost.com/2012-02-24/news/35445015_1_interior-design-martyn-lawrence-bullard-powder-room|work=February 24, 2012|publisher=The Washington Post|date=25 February 2012}}</ref><ref>{{cite web|last=Rus|first=Mayer|title=Q+A: MARTYN LAWRENCE BULLARD ON HIS NEW WALL COVERINGS|url=http://www.architecturaldigest.com/blogs/daily/2013/06/martyn-lawrence-bullard-interview-schumacher-blog|work=June 17, 2013|publisher=Architectural Digest|accessdate=18 September 2013}}</ref>
<ref name="LA Confidential">{{cite web|last=Fender|first=Abby|title=Interior Designer Martyn Lawrence-Bullard|url=http://la-confidential-magazine.com/the-latest/people-and-parties/postings/interior-designer-martyn-lawrence-bullard|work=November 9, 2012|publisher=Los Angeles Confidential|accessdate=18 September 2013}}</ref><ref name=Houston />

Bullard is the face of Vi-Spring Beds, a British luxury brand, and a spokesperson for [[Jaguar Cars|Jaguar]] automobiles.<ref name=Houston>{{cite web|last=Glentzer|first=Molly|title=Celebrity style: Martyn Lawrence Bullard|url=http://www.chron.com/life/gloss/article/Celebrity-style-Martyn-Lawrence-Bullard-3435712.php|work=March 26, 2012|publisher=Houston Chronicle|accessdate=18 September 2013}}</ref>

== Philanthropy ==
Bullard is actively involved with [[amfAR, The Foundation for AIDS Research|amfAR]], the [[Sharon Osbourne]] Colon Cancer Program, P.S. Arts, and DIFFA (Design Industry Foundation Fighting AIDS).<ref name="Los Angeles Times" /><ref>{{cite web|title=Social Interests|url=http://www.martynlawrencebullard.com/social-interests|publisher=martynlawrencebullard.com|accessdate=16 September 2013}}</ref><ref>{{cite web|title=Scene in LA|url=http://diffa.org/content/press/press08_Angeleno_Magazine.pdf|work=December 2008|publisher=Angeleno Magazine|accessdate=17 September 2013}}</ref><ref>{{cite news|last=Bubbles|first=Monte|title=Sharon and Kelly Osbourne, Maria Menounos & Martyn Lawrence Bullard at Kreiss 75th Anniversary Celebration|url=http://ireport.cnn.com/docs/DOC-778713|work=April 20, 2012|publisher=CNN iReport|accessdate=17 September 2013}}</ref><ref>{{cite web|title=Chelsea Handler MCs amfAR event honoring Kevin Huvane|url=http://societynewsla.com/amfar-inspiration-gala-honoring-kevin-huvane/|work=October, 2012|publisher=Society News LA|accessdate=17 September 2013}}</ref> In 2013, Bullard was recruited by the Designer Dollhouse Showcase to design a dollhouse, which was auctioned to benefit the UCLA Children's Discovery and Innovation Institute at the Mattel Children's Hospital.<ref>{{cite web|last=Schellenbaum|first=Amy|title=Yes, This Dollhouse is Indeed by Martyn Lawrence Bullard|url=http://curbed.com/archives/2013/06/25/yes-this-dollhouse-is-indeed-by-martyn-lawrence-bullard.php#more|date=25 June 2013|publisher=Curbed|accessdate=20 September 2013}}</ref>

== Awards and recognition ==
* [[Andrew Martin International]] Interior Designer of the Year (2010)<ref>{{cite web|last=Murg|first=Stephanie|title=Alberto Alessi, Martyn Lawrence Bullard Receive International Design Honors|url=http://www.mediabistro.com/unbeige/alberto-alessi-martyn-lawrence-bullard-receive-international-design-honors_b8598|date=September 9, 2010|publisher=Media Bistro|accessdate=16 September 2013}}</ref>
* ''[[Architectural Digest]]'' AD100, (2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012,2013<ref>{{cite web|title=2012 AD100: MARTYN LAWRENCE BULLARD DESIGN|url=2012 AD100: MARTYN LAWRENCE BULLARD DESIGN|date=2012|publisher=Architectural Digest|accessdate=16 September 2013}}</ref>
* ''[[Elle Decor]]'' Top 25 Interior Designers (2010)<ref name=Times />
* ''[[Elle Decor]]'' A List (2006, 2007, 2008, 2009, 2010, 2011, 2012)<ref name="A List">{{cite web|last=Staff|first=Elle Decor|title=ELLE DECOR'S 2012 A-LIST|url=http://www.elledecor.com/design-decorate/talent/elle-decors-2012-a-list-a|date=2012|publisher=Elle Decor|accessdate=16 September 2013}}</ref>
* ''Luxe Interiors + Designs'' Gold List (2012)<ref>{{cite web|title=Editorial Calendar|url=http://www.luxesource.com/Content/editorial-calendar.pdf|date=Fall 2012|publisher=Luxe Interiors + Design|accessdate=16 September 2013}}</ref>
* ''[[Architectural Design]] France'' Top 100 Designers in the World (2011, 2012, 2013)
* ''[[Architectural Design]] Russia'' Top 25 Designers in the World (2012, 2013)
* [[Conde Nast]] Top American Tastemaker
* ''UK Focus'' Best of the Best: Fabric Designer, Focus Design, London (2011)
* ''[[Departures (magazine)|Departures]]'' World's Best in Travel and Design; Most Influential Designers Working Today (2013)<ref>{{cite web|last=McKeough|first=Tim|title=The World's Best in Travel And Design|url=http://www.departures.com/slideshows/the-worlds-best-in-travel-and-design/14|date=2012|publisher=Departures|accessdate=17 September 2013}}</ref>
* ''[[Hollywood Reporter]]'' 25 Most Influential Designers in L.A. (2012)<ref name="Hollywood Reporter">{{cite news|title=The 25 Most Influential Designers in LA|url=http://www.hollywoodreporter.com/lists/martyn-lawrence-bullard-382079|publisher=Hollywood Reporter|accessdate=19 September 2013|date=24 October 2012}}</ref>
* ''UK Focus: Best of the Best: Collection of the Year'' (Schumacher Collection), Focus Design, London (2012)

== Bibliography ==
Martyn Lawrence Bullard; photographed by Tim Street-Porter; ''Live, Love & Decorate;''4 October 2011; Rizzolli; ISBN 978-0-8478-3676-5

== External links ==
* {{Official website|http://www.martynlawrencebullard.com/}}
* [http://www.bravotv.com/million-dollar-decorators ''Million Dollar Decorators'' at Bravo]
* [http://www.channel4.com/programmes/hollywood-me ''Hollywood Me'']
* [http://psarts.org/ P.S. Arts]
* [http://diffa.org/ DIFFA]
* [http://www.amfar.org/ amfAR]

== References ==
{{reflist|3}}

{{DEFAULTSORT:Bullard, Martyn Lawrence}}
[[Category:1967 births]]
[[Category:Living people]]
[[Category:British interior designers]]
[[Category:British writers]]