{{for|the civil use of this facility|Fort Wayne International Airport}}
{{Infobox military structure
| name=Fort Wayne Air National Guard Station<br>{{smaller|Baer Army Air Base}}
| ensign=[[File:Air National Guard.png|60px]]
| partof= [[Indiana Air National Guard]] (IN ANG)
| location=  Located at: [[Fort Wayne International Airport]], Indiana
| image=  122d Fighter WIng General Dynamics F-16C Block 25D Fighting Falcon 84-1264.jpg
| image_size = 300px
| caption=  122d FW F-16C 84-1264 in heritage motif, painted to pay homage to the 122d FW's history.
| pushpin_map= Indiana
| pushpin_label=Fort Wayne ANGB
| pushpin_mapsize=150
| pushpin_map_caption=Location of Fort Wayne Air National Guard Station, Indiana
| coordinates={{Coord|40|58|42|N|85|11|42|W|name=Fort Wayne ANGS|display=inline,title}}
| type=Air National Guard Station
| code= 
| height= 
| ownership= 
| controlledby={{air force|USA}}
| condition= 
| built=1941
| builder= 
| used=1941-Present
| materials= 
| demolished= 
| battles= 
| events= 
| past_commanders= 
| garrison= [[File:122d Fighter Wing.png|60px]]&nbsp; [[122d Fighter Wing]]
| occupants= 
}}
'''Fort Wayne Air National Guard Station''' is a [[United States Air Force]] base, located at [[Fort Wayne International Airport]], Indiana. It is located {{convert|7.6|mi|km}} south-southwest of [[Fort Wayne, Indiana]].

Initially established in 1941 as a training airfield for the [[Army Air Forces]],  during [[World War II]], "Baer Field" became a major training and aircraft processing base for [[C-47 Skytrain]] and [[C-46 Commando]] transport aircraft, equipping many Troop Carrier squadrons with aircraft and training their aircrews in their operation.  Largely turned over to civil control after the war, the Air Force has maintained control over a small section of the civil airport for reserve training and [[Air National Guard]] use.   Since 1952 it has been the home station of the [[Indiana Air National Guard]] [[122d Fighter Wing]].

==Overview==
A unit of the [[United States Air Force]], in a national emergency, the 122d FW may be ordered to active duty by the President of the United States. The primary federal mission of the 122d FW is to achieve and maintain the level of operational readiness that will provide trained and equipped combat-ready tactical units, capable of global deployment, ready for immediate integration into the active Air Force to assure air offense, air defense, or joint action with ground forces.<ref name="122dfw">[http://www.122fw.ang.af.mil 122d Fighter Wing Website]</ref>

The 122d FW is available on orders from the Governor of Indiana to assist local authorities in the event of a disaster, disturbance or other emergency. The units of the 122d FW are capable of supporting rescue and relief operations, aiding in recovery from natural disaster, along with protecting the citizens of Indiana and their property.<ref name="122dfw"/>

==Units==
* 122d Operations Group (Tail code formerly "FW" (F-16), now "IN" (A-10)
: [[163d Fighter Squadron]], A-10C Thunderbolt II aircraft
: 122d Operations Support Squadron
* 122d Maintenance Group
: 122d Aircraft Maintenance Squadron
: 122d Maintenance Squadron
: 122d Maintenance Operations Flight
* 122d Mission Support Group
: 122d Civil Engineering Squadron
: 122d Security Forces Squadron
: 122d Logistics Readiness Squadron
: 122d Mission Support Flight
: 122d Communications Flight
: 122d Services Flight
: 122d Medical Group<ref name="122dfw"/>

==History==

===Paul Baer===
Originally named '''Baer Army Air Base''' (or more colloquially, "Baer Field"), the base was named after Paul Baer, a Fort Wayne native, born in 1894. A 16 victory [[Flying ace]], he flew with the [[Lafayette Escadrille]] and the [[103d Aero Squadron]] [[Army Air Service|American Expeditionary Force Air Service]] during [[World War I]] and was awarded the [[Distinguished Service Cross (United States)|Distinguished Service Cross]], the [[Legion of Honor]], and the [[Croix de guerre 1914–1918 (France)|French Croix de Guerre]]. He continued to fly after the war, opening air mail routes in South America, and participating in many aviation experiments. He was killed on 9 December 1930 while flying mail and passengers for Chinese Airway Federal, Inc.  His [[Loening C-2]]-H amphibian crashed on takeoff from [[Shanghai, China]].  He is buried in Fort Wayne.<ref name="baer">[http://www.museumofthesoldier.com/baer_field.htm Preparing C-47s for War (Baer Field)]</ref><ref>[http://www.firstworldwar.com/bio/baer.htm First World War.Com  Paul Baer]</ref>

=== Origins ===
[[File:Baer Army Air Base - IN - 24 November 1943.jpg|thumb|Baer Army Air Base, November 1943]]
Baer Field was unique in that its establishment at [[Fort Wayne]] was at the request of the city. The location for many airbases was often a decision by the [[United States Department of War|War Department]] with little input by the people affected. The citizens of Fort Wayne wanted the base, and the city took options to buy 700 acres for that purpose should the War Department decide to build a field there. The decision to build at Fort Wayne came quicker than expected. Early in January 1941 the War Department told the town it would locate a base there if possession of the land could be had by February 1. That was less than 30 days away. It was simply not possible to handle the real estate and financial matters that quickly. However the situation was saved by 30 businessmen, who signed notes totaling the $125,000 needed. Then four of the local banks advanced the city that amount to buy the land. Land owners were told to be ready to vacate in 15 days.  The federal government signed a $1 annual lease, and now the construction could begin.<ref name="baer"/><ref name="afhra">[http://airforcehistoryindex.org/search.php?q=BAER+FIELD&c=u&h=100&F=1940&L= AFHRA Search Baer Field]</ref>

The land was cleared and  the site prepared for construction.  There were eight homes, seven barns, and some other buildings to be razed.  The airfield consisted of three concrete runways with bituminous shoulders, 6230x148(NE/SW), 6300x148(NW/SE), 6000x175(N/S) including three large parking ramps, several hangars, a control tower and other auxiliary support aircraft buildings.  The ground station, located to the north of the airfield, consisted of more than one hundred buildings, all intended to be temporary. Station buildings and streets were also constructed, the buildings consisting primarily of wood, tar paper, and non-masonry siding. The use of concrete and steel was limited because of the critical need elsewhere. Most buildings were hot and dusty in the summer and very cold in the winter. Water, sewer and electrical services were also constructed.  Initially there was no paving to the streets,  compressed earth served immediate needs.<ref name="baer"/><ref name="Airfields">[http://www.airfieldsdatabase.com/WW2/WW2%20R27c%20ID-NH.htm WW2 Military Airfields including Auxiliaries and Support fields   Idaho - New Hampshire]</ref>

The major part of the original building project was finished by July 1941, about 150 days after the lease for the land was signed  by the War Department. and the [[United States Army Air Forces]] took possession of the field on 31 October.<ref name="baer"/><ref name="afhra"/>

===World War II===

====Operational training base====
[[File:Baer Field World War II Postcard.jpg|thumb|World War II postcard]]
The Army Air Forces assigned jurisdiction of Baer Field initially to [[I Fighter Command]], [[First Air Force]]. The major responsibility of the First Air Force was the organization and training of bomber, fighter and other units and crews for assignment overseas.<ref name="afhra"/>

The [[31st Pursuit Group]], the first to be equipped with the [[P-39 Airacobra]], was assigned to Baer Field on 6 December 1941.  It consisted of the [[39th Fighter Squadron|39th]], [[40th Fighter Squadron|40th]] and [[41st Pursuit Squadron]]s.  However Japan attacked Pearl Harbor the next day. Training was accelerated when Nazi Germany declared War on the United States on 11 December.  Part of the group left four days later and by February the within weeks the entire group was gone. By June they had deployed to England as part of [[VIII Fighter Command]] where they would be equipped with [[Supermarine Spitfires]]  They entered combat in August 1942.<ref name="Maurer 1983">Maurer, Maurer (1983). Air Force Combat Units Of World War II. Maxwell AFB, Alabama: Office of Air Force History. ISBN 0-89201-092-4.</ref><ref name="Maurer 1982">Maurer, Maurer (1982). Combat Squadrons of the Air Force, World War II, Office of Air Force history (1982). ISBN 0-8317-1501-4</ref>

The [[78th Fighter Group]] was activated at Baer Field on 9 February 1942. It consisted of the [[82d Fighter Squadron|82d]], [[83d Fighter Squadron|83d]] and [[84th Fighter Squadron]]s.   The 78th was equipped with the [[P-38E Lightning]], the first major production version of the aircraft and it's planned mission was bomber escort for [[B-17 Flying Fortress]] flying from England against targets in [[Occupied Europe]].   The group trained at Baer Field until April when it moved out to California to train in the desert air of the [[Mojave Desert]] at [[Muroc Army Airfield]].  The group deployed to England in December 1942 and later transferred to the [[Twelfth Air Force]] in [[North Africa]].<ref name="Maurer 1983"/><ref name="Maurer 1982"/>

The mission of First Air Force was changed in early 1942 to the defense of the [[Atlantic Ocean|Atlantic Seaboard]], and jurisdiction of Baer Field was transferred to [[III Bomber Command]], [[Third Air Force]] in March 1942. In May 1942 elements of the [[38th Bombardment Group]], equipped with  [[B-26 Marauder]]s stopped for servicing while on their way to the West Coast. This group would later participate in the [[Battle of Midway]].<ref name="afhra"/><ref name="Maurer 1983"/>

The airfield was closed to traffic beginning in June 1942 in order for additional construction to be completed.  Some of the changes made during this time were strengthening the runways, necessary to accommodate the increasing weight of the [[B-26 Marauder]] bombers, [[C-46 Commando]]es, and [[C-47 Skytrain]] transports.  Additional hangars were also constructed to accommodate the maintenance of large numbers of aircraft.  In addition, the ground station streets were paved.<ref name="baer"/>

====Aircraft/Crew processing center====
[[File:Troopcarriercommand-emblem.jpg|left|100px]]
[[File:B 26.jpg|thumb|Martin B-26 Marauder]]
[[File:313tcg-c46-1.jpg|thumb|Curtiss-Wright C-46 Commando]]
[[File:Douglas C-47 Skytrain 1985.jpg|thumb|Douglas C-47 Skytrain]]
By September 1942 the runways were again open, and III Bomber Command used Baer Field as a staging and processing facility for [[B-26 Marauder]] medium bombardment groups.   At Baer the newly manufactured aircraft were ferried from the Martin manufacturing plant near [[Baltimore, Maryland]] and assigned to the 1st Concentration Command (1st CC).  The 1st CC was responsible for completing the organization and equipment of tactical and combat bombardment groups prior to their deployment to the overseas combat theaters.  It made necessary modifications to the B-26s after their ferrying flights to the airfield to bring them up to current specifications and forming the ground support echelons of the units needed to support the air echelons of the bombardment groups.<ref name="baer"/><ref name="afhra"/>

In March 1943, jurisdiction of the base was again reassigned from Third Air Force to [[I Troop Carrier Command]] (I TCC), part of [[First Air Force]].  The mission of I TCC was to train [[C-46 Commando]] and [[C-47 Skytrain]] Troop Carrier (Transport) groups in preparation for overseas deployment to the various combat theaters.  Baer’s responsibility was the processing of Troop Carrier Command units for I TCC, by providing the groups with aircraft and providing the necessary training for their maintenance and support.  Large numbers of aircraft and crews were joined at Baer and from there were deployed overseas<ref name="baer"/><ref name="afhra"/>

The processing of the transport aircraft was performed in a similar manner as was done previously with the B-26 medium bombers. After the plane’s manufacture, primarily at the Douglas C-47 plant in [[Tulsa, Oklahoma]], or the Curtiss-Wright C-46 plants in [[St. Louis, Missouri]] or [[Louisville, Kentucky]], the aircraft was flown to an [[Air Technical Service Command]] modification center.<ref name="baer"/>

At the modification center, the plane was updated according to the latest modification orders and received the necessary equipment and changes to suit it for its final destination. From the various modification centers the aircraft were flown to Baer Field.  Aircraft were parked everywhere including two of the three runways.  The hardstands were also packed with new aircraft awaiting processing.<ref name="baer"/>

Baer’s responsibility was to inspect the aircraft and make any appropriate final changes; i.e., install long-range fuel tanks, remove unnecessary equipment, and give it a final flight safety test.  Bare's inspection of the aircraft was very detailed and involved considerable maintenance, repair and modification. There were two stages. The first was an assembly-line type of operation in the largest hangar where everything was checked. Examples of some of the problems found were leakage of hydraulic fittings, generators not working, loose electrical fittings, instruments inoperative, low fluid levels and missing parts, especially clocks. One C-46 arrived with a block of wood in a carburetor air filter.<ref name="baer"/><ref name="afhra"/>

The second stage was the last inspection. Here the plane was flight tested and then turned over to its crew.  The engines were run up, the plane taxied and then flown by military crews to check instruments, radios and single engine operation. The planes were flown north from Baer to [[Kendallville, Indiana]], and back to Baer. This was about 80 miles round trip. Around Kendallville one engine was shut down to see how the aircraft handled. The military flew the planes during the test flight, but often the pilot would be accompanied by a civilian test pilot. When the flight turned up additional problems they were corrected by the Flight Test section.  There were [[Pratt and Whitney]] technical representatives on the field for the engines, as well as other factory representatives from Douglas and Curtiss-Wright for the airframes.<ref name="baer"/>

Once completed, the aircraft was delivered to its squadron aircrew. Long range fuel tanks were installed in the fuselage for its overseas movement.  If the plane was going to [[England]] over the [[North Atlantic Route]] it received two 100 gallon tanks; to Africa over the [[South Atlantic Route]] required four 100 gallon tanks, and the very long-distances of the South Pacific Route, then eight 100 gallon tanks were installed due to the vast distances between island airfields.  Extra oil was also required and this was put into a 50-gallon drum with a hose to each engine through the wing. When the oil level got low more would be hand pumped from the drum to the engines oil tank.<ref name="baer"/>

While the planes were being serviced and made ready for overseas movement, personnel for these planes were also being processed.  Pilots, co-pilots and crew chiefs being assigned as a crew for each aircraft. In some cases a Navigator was assigned, depending upon the ships destination. The process typically took 2–3 weeks. Paperwork was handled, equipment was issued and some training accomplished. Baer would process between 10-40 crews per day.<ref name="baer"/>

Early in the war, a lot of training was very brief or non-existent. This was particularly true where men and machines were rushed into combat to meet an enemy that had been preparing and fighting for years.  This low level of training was also true for crew chiefs.  This would change. Training films were first used at Baer in June 1942 and by August 1943 there were 165 training films; later on this would grow to 485 films and numerous other training aids.<ref name="baer"/>

New groups which were processed through Baer Field prior to their deployments overseas were:<ref name="Maurer 1983"/>

{{col-begin}}
{{col-break|width=50%}}
* [[436th Troop Carrier Group]]
* [[375th Troop Carrier Group]]
* [[403d Troop Carrier Group]]
* [[433d Troop Carrier Group]]
* [[434th Troop Carrier Group]]
* [[435th Troop Carrier Group]]
{{col-break|width=50%}}
* [[437th Troop Carrier Group]]
* [[438th Troop Carrier Group]]
* [[439th Troop Carrier Group]]
* [[440th Troop Carrier Group]]
* [[441st Troop Carrier Group]]
* [[442d Troop Carrier Group]]
{{col-end}}

Beginning in 1944, Baer repaired and refurbished "war weary" aircraft which were returned from their combat assignments and were overhauled and inspected.    After April 1944, new aircrew training was suspended and replacement aircraft for overseas units were processed through Baer, with WASP and ATC Ferrying Command pilots moving checked out aircraft to their worldwide destinations.  In addition  [[Lend-Lease]] C-47s bound for [[Russia]] and [[Great Britain]] were processed through Baer Field, then turned over to [[Air Transport Command (United States Air Force)|Air Transport Command]] ferrying crews for subsequent ferrying overseas.<ref name="baer"/><ref name="afhra"/>

===Postwar use===
[[File:Continental Air Command.png|left|100px]]
Baer continued as a staging base for the I Troop Carrier Command until early May 1945, when its mission changed.   With the end of the European War, the base now became an assembly station for redeployment of personnel from  Europe to the [[Pacific Ocean theater of World War II|Pacific Theater]].  Aircraft staging activities were no longer conducted at the base, however it remained under the jurisdiction of I Troop Carrier Command.<ref name="baer"/><ref name="afhra"/>

Throughout the summer of 1945 many Troop Carrier Groups, which had been equipped and trained at Baer returned to the base from their overseas assignments where they were demobilized and the aircraft were ferried to storage depots in the southwest.<ref name="Maurer 1983"/>

This new assignment was short lived, and on 31 December 1945 Baer Field was placed on inactive status.  Baer’s last assignment was an Army Air Forces separation base.  The 333d Army Air Force Base Unit was assigned to the base with the mission to formulate procedures for the transfer of military equipment to other Air Force bases.<ref name="afhra"/>

On 10 March 1946 the facility was turned over to the City of Fort Wayne for use as a civil airport for one dollar, and the City renamed the facility "Fort Wayne Municipal Airport".  The military base was made up of more than 100 buildings, and over the years many of the military buildings were raised and the airfield re-engineered with longer runways to accommodate jet aircraft and as a civil airport.<ref name="baer"/><ref name="afhra"/>

On 1 February 1947 Baer Army Air Base was declared surplus and title to most facilities were transferred to the City of Fort Wayne except for specific areas to be retained for Air Force Reserve and Indiana Air National Guard activities.  The AAF  maintained a small cadre of personnel at the facility. On 15 December 1946 the 439th Army Air Forces Base Unit (Reserve Training) was activated, replacing the 333d AAFBU, by which consisted of three officers and no enlisted men.  The 439th AAFBU's activities consisted of screening, packing and shipping records to appropriate archive depots and furnishing information and assistance to auditors.<ref name="afhra"/>

On 1 July 1948, the 2467th Air Force Reserve Training Center opened at the base, placed under the jurisdiction of [[Continental Air Command]], [[Tenth Air Force]].  It was inactivated due to budget constraints on 27 June 1949.<ref name="afhra"/>

===Air National Guard use===
[[File:122d Fighter Wing.png|left|100px]]
The World War II [[Ninth Air Force]] [[358th Fighter Group]] was inactivated after the war on 7 November 1945.  The organization was turned over to Continental Air Forces, and was re-designated as the [[122d Fighter Group]] and allocated to the State of Indiana on 24 May 1946 for use in the newly constituted [[Air National Guard]].    The unit was extended federal recognition on 9 December 1946.  On 10 November 1947, federal recognition was granted to the [[163d Fighter Squadron]] at Baer Field<ref name="Maurer 1983"/><ref name="Maurer 1982"/>

The 122d Fighter Group was inactive until 1 February 1951 the unit was activated and federalized due to the [[Korean War]], and assigned to [[Air Defense Command]].   Two squadrons,  the 113th and 163d were assigned and the organization was re designated as the '''122d Fighter Interceptor Group''', being stationed at [[Stout Field]], Indiana.   The 122d FIG initially was equipped with [[F-51 Mustang]]s, later being equipped with [[F-84 Thunderjet]]s.   The unit was reassigned to Baer Field on 10 March 1951 and remained under Air Defense Command as an interceptor unit until being released to state control on 17 February 1952.  The ADC 4612th Air Base Squadron was activated as a provisional station administrative unit to process active-duty personnel from Baer Field to other units.   The last active-duty USAF personnel departed the field on 1 June 1952.<ref name="122dfw"/><ref name="afhra"/><ref name="Maurer 1982"/>

On 1 November 1952 the unit was reorganized and transferred to [[Tactical Air Command]] control, being re designated as the 122d Fighter-Bomber Group and equipped with the Lockheed [[F-80 Shooting Star]] in September 1954. The jet era continued with the conversion to the [[F-86 Sabre]] eighteen months later, and in January 1958, the Republic [[F-84 Thunderstreak]] gave the 122 TFW a new dimension for the next thirteen years.<ref name="122dfw"/>

Nicknamed the "Blacksnakes," the 122d Fighter Wing today is operationally gained by the [[Air Combat Command]] (ACC). Until 2010, the 122 FW flew the [[F-16 Fighting Falcon|F-16C/D Fighting Falcon]] in a multirole fighter mission.  As a result of [[BRAC 2005]], the unit converted to the [[A-10 Thunderbolt II]] aircraft and a close air support (CAS) mission, said conversion having been completed in 2010.<ref>http://www.saffm.hq.af.mil/shared/media/document/AFD-100126-028.pdf</ref>  The installation was subsequently redesignated as '''Fort Wayne Air National Guard Base'''.<ref>http://www.122fw.ang.af.mil/</ref>

The 122 FW operates from Fort Wayne Air National Guard Base, which is located on the east side of the airport in a secure area away from the publicly accessible facilities.<ref name="122dfw"/>

==See also==
{{Portal|World War II|United States Air Force|Military of the United States}}
* [[Indiana World War II Army Airfields]]
* [[I Troop Carrier Command]]

==References==
{{Air Force Historical Research Agency}}
{{Reflist}}

==External links==

{{Fort Wayne, Indiana}}

[[Category:1941 establishments in Indiana]]
[[Category:Airfields of the United States Army Air Forces in Indiana]]
[[Category:Military facilities in Indiana]]
[[Category:Buildings and structures in Allen County, Indiana]]
[[Category:Installations of the United States Air National Guard]]
[[Category:Airfields of the United States Army Air Forces I Troop Carrier Command]]