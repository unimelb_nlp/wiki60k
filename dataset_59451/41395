{{lead too short|date=August 2014}}
'''{{Infobox ethnic group
|group=Guyanese in the United Kingdom
|image=   
|caption= 
|population= 20,872 Guyanese-born (2001 Census)<br>21,417 Guyanese-born (2011 Census)
|popplace=[[London]], [[Birmingham]], [[Manchester]]
|langs=[[English language|English]] ([[British English]], [[Guyanese Creole]]), [[Akawaio language|Akawaio]], [[Hindi]], [[Macushi]], [[Wai-wai people|Wai-Wai]], [[Arawakan]], [[Cariban]]
|rels= [[Hinduism]], [[Pentecostalism]], [[Roman Catholic]], [[Islam]], [[Anglicanism]]
|related=[[Guyanese people]], [[British African-Caribbean community]], [[British Indo-Caribbean community]], [[Black British]], [[Black African]], [[Multiracial]], [[Indo-Caribbean]], [[Indo-Guyanese]], [[Amerindian]]
|footnotes=
<small id="*">* Please note that in 2001 only 40.4% of Afro-Caribbeans in the UK were actually born in the Caribbean, 59.6% were born elsewhere (of which 57.9% of the total ethnic groups population was born in the UK)<ref>[http://www.statistics.gov.uk/downloads/theme_compendia/foer2006/FoER_Main.pdf National Statistics 2006]</ref></small>
}}
'''Guyanese in the United Kingdom''' are citizens or residents of the [[United Kingdom]] whose origins lie in [[Guyana]].

==Demographics==

===Population===
At the time of the [[United Kingdom Census 2001|2001 UK Census]] there were 20,872 Guyanese-born people in the UK.<ref name=OECD>{{cite web|url=http://www.oecd.org/dataoecd/18/23/34792376.xls|title=Country-of-birth database|publisher=[[Organisation for Economic Co-operation and Development]]|accessdate=2009-10-04}}</ref> In 2001, Guyana was the [[Foreign-born population of the United Kingdom|sixth most common]] birthplace within the Americas for people in the UK and on a global scale ranked as the 51st most common birthplace of people resident in the UK.<ref name=OECD/> Estimates published by the [[Office for National Statistics]] suggest that the Guyanese-born population of the UK was 24,000 in 2009.<ref name="2009 estimates">{{cite web|url=http://www.statistics.gov.uk/downloads/theme_population/Population-by-country-of-birth-and-nationality-Oct08-Sep09.zip | title=Estimated population resident in the United Kingdom, by foreign country of birth (Table 1.3) | publisher=[[Office for National Statistics]] | date=September 2009 | accessdate=8 July 2010}}</ref>

==Culture and community==
{{see also|Culture of Guyana|Culture of the British African-Caribbean community}}

===Literature===
Guyanese immigrants have had an influence on recent [[literature]] in the UK, and significant numbers of writers and poets have made their footprint on current British culture and have become everyday household names.<ref name=Lit>{{cite web|url=http://www.caribvoice.org/A&E/guyanesewriters.html|title=Guyanese Writers in England|publisher=John Mair|accessdate=2009-07-29}}</ref> It is, however, claimed that this trend of success in the field has not continued through to the second- and third-generation Guyanese Britons.<ref name=Lit/> The late [[Beryl Gilroy]] was a significant figure within the Afro-Caribbean diaspora in the UK. This highly respected Guyanese-born novelist became the first black headteacher of any school in the country.<ref name=Lit/> Another important literary figure of the Guyanese British community in the UK as a whole is [[John Agard]], who is probably the most famous Black British poet and has been recognised with many awards. [[Pauline Melville]]'s output of work has led to such awards as [[Guardian Fiction Prize]], the Macmillan Silver Pen Award and the [[Commonwealth Writers' Prize]] for best first book.<ref name=Lit/> [[Wilson Harris]], who received the first ever Guyana Prize for Literature, has like many other Guyanese writers in the UK has been heavily influenced and inspired by the culture and history of his homeland.<ref name=Lit/> Indo-Guyanese writer [[David Dabydeen]], a UK resident, has interests that encompass the slave-trading history of Guyana as well as contemporary Caribbean culture in the UK.<ref name=DD>{{cite web|url=http://www.humboldt.edu/~me2/engl240b/student_projects/dabydeen/dabydeenbio.htm |title=David Dabydeen > Biography |publisher=Humboldt |accessdate=2009-07-29 |deadurl=yes |archiveurl=https://web.archive.org/web/20070716073721/http://www.humboldt.edu/~me2/engl240b/student_projects/dabydeen/dabydeenbio.htm |archivedate=July 16, 2007 }}</ref> Other writers, including [[Roy Heath]] and [[Michael Abbensetts]], have helped create a greater knowledge of Guyanese culture in the UK, and they are among the most successful literary diaspora communities as a whole in recent British history.<ref name=Lit/> Other UK-based writers of Guyanese origin include [[Fred D'Aguiar]], [[Mike Phillips (writer)|Mike Phillips]], and [[Jan Shinebourne]].

The pioneering black publishing company [[Bogle-L'Ouverture]] was founded in London in the late 1960s by Jessica and Eric Huntley from Guyana, their first publication being [[Walter Rodney]]'s ''The Groundings with My Brothers'' (1969).<ref>Petamber Persaud, [http://www.guyanachronicle.com/site/index.php?option=com_content&view=article&id=37490:bogle-louverture-a-story-in-black-publishing&catid=18:lead-stories&Itemid=17 "Bogle-L’Ouverture: A story in Black publishing"], ''Guyana Chronicle'', 7 January 2012.</ref>

===Music===
{{see also|Music of Guyana}}

The music of Guyana is a mix of Indian, African, European and native elements. It is similar to the music of various other Caribbean nations, where [[reggae]], [[soca music|soca]] and [[calypso music|calypso]] prove the most popular.<ref name=Music>{{cite web|url=http://georgetown-guyana.com/2009/03/08/guyanese-music/|title=Guyanese Music|publisher=Georgetown, Guyana|accessdate=2009-07-30}}</ref> These forms of music have worked their way into British life by the Guyanese community of the UK and even by several famous Guyanese musicians who have migrated to the UK. The influence of [[Caribbean music in the United Kingdom]] is evident in many walks of life; the work of many contemporary artists is based in the reggae and calypso styles. [[Eddy Grant]], a Guyanese-born immigrant to the UK, helped popularise such genres as reggae through his global hits such as "[[Electric Avenue (song)|Electric Avenue]]" and "[[I Don't Wanna Dance (Eddy Grant song)|I Don't Wanna Dance]]".<ref name=EG>{{cite web|url=http://caribbean.halloffame.tripod.com/Eddy_Grant.html
|title=Eddy Grant|publisher=Caribbean Hall of Fame|accessdate=2009-07-30}}</ref> Reggae has proven the most successful sub-category of Guyanese music (and Caribbean music in general) in the UK and Grant himself is noted as saying: "in my heart, I know that Soca and Ringbang have the same potential as reggae to achieve great popularity… but there has never been any proper commitment to marketing these artists and their music. We are not Sony, and the artists on board realise it will take time. It is an upliftment process."<ref name=EG/> Despite this, as the Guyanese community in the UK has advanced into its second and third generations, evidence of traditional Guyanese elements in the music has begun to decrease. British-born individuals of Guyanese origin have in particular become more mainstream and modernised. The most recent success story of a British singer of Guyanese origin is [[Leona Lewis]], the Londoner whose music is largely [[Pop music|pop]] and [[R&B]] won series three of the talent contest ''[[The X Factor (UK)|The X Factor]]''.<ref>[http://www.hackneygazette.co.uk/content/hackney/gazette/news/story.aspx?brand=HKYGOnline&category=news&tBrand=northlondon24&tCategory=newshkyg&itemid=WeED22%20Dec%202006%2010%3A42%3A29%3A740 "Winner Leona proud to be Hackney girl". ''Hackney Gazette'', 22 December 2006.]</ref> She has attained three number one hits in the UK and it the only solo British female in over two decades to have reached the top spot on the [[Billboard (magazine)|''Billboard'' Hot 100]]. Another example of a successful British-Guyanese artists is [[Wretch 32]], a rapper from [[Tottenham, London]] who has led on to release 4 UK top 10 singles, a number 1 single and an album which topped the UK R&B chart selling nearly 25,000 copies in its first week. Haring Traditional Guyanese acts and British acts influenced by such genres as reggae, soca and calypso can be found in festivals across the country, the most famous being the [[Notting Hill Carnival]] (the world's second largest street festival).<ref>[http://news.sky.com/skynews/Home/Sky-News-Archive/Article/200806413609012 Sky News.]</ref>

==Notable individuals==
{{main article|List of Guyanese Britons}}

==See also==
* [[Black British]]
* [[British Mixed]]
* [[British African-Caribbean community]]
* [[British Indo-Caribbean community]]
* [[Guyanese Canadians]]
* [[Demographics of Guyana]]

==References==
<references/>

==External links==
* [http://www.guyanauk.com/ Guyana UK]
* [http://profile.myspace.com/index.cfm?fuseaction=user.viewprofile&friendid=82087253 Myspace account aimed at the Guyanese British community]

{{Guyanese diaspora}}
{{AmericansinUK}}

[[Category:People of African descent]]
[[Category:Black British people|*Guyanese]]
[[Category:British people of Guyanese descent| ]]
[[Category:Caribbean British]]
[[Category:Guyanese diaspora|Unitedkingdom]]
[[Category:Immigration to the United Kingdom by country of origin]]