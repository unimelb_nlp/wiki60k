{{refimprove|date=March 2015}}
The '''American Society of Church History''' ('''ASCH''') was founded in 1888 with the disciplines of Christian denominational and ecclesiastical history as its focus. Today the society's interests include the broad range of the critical scholarly perspectives, as applied to the [[history of Christianity]] and its relationship to surrounding cultures in all periods, locations and contexts. The society was founded by the classic church historian [[Philip Schaff]]. 

The ASCH records are housed at the [[Presbyterian Historical Society]] in [[Philadelphia]]. 

== ''Church History'' ==

ASCH publishes the quarterly [[academic journal]] ''[[Church History: Studies in Christianity and Culture]]'', which was established in 1932.<ref>{{cite book|last1=Hein|first1=David|last2=Shattuck|first2=Gardiner H.|title=The Episcopalians|url=https://books.google.com/books?id=_iK5VggudLkC&pg=PA333|year=2004|publisher=Greenwood|isbn=9780313229589|pages=333–34}}</ref><ref>{{cite web |url=http://www.churchhistory.org/church-history-journal/ |title=Church History: Studies in Christianity and Culture |work=Homepage |publisher=American Society of Church History}}</ref> 

== References ==
{{Reflist}}

== External links==
* [http://www.churchhistory.org/ ASCH official website]
* [http://www.historians.org/affiliates/am_soc_church_his.htm ASCH description, officers, and staff] from the [[American Historical Association]] (AHA)
* [http://www.churchhistory.org/churchhistory.html Back issue access {{mdash}} American Society of Church History Journal]

[[Category:Non-profit organizations based in Connecticut]]
[[Category:Organizations established in 1888]]
[[Category:Historical societies of the United States]]
[[Category:Yale Divinity School]]
[[Category:Ancient Christianity studies]]