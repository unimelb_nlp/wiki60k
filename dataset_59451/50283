{{Redirect-distinguish|Sheikh Mansour|Sheikh Mansur}}
{{arabic name|Al Nahyan}}
{{Use dmy dates|date=April 2013}}
{{Infobox person
|name               = Mansour bin Zayed Al Nahyan
|image              = AM Spindelegger in Abu Dhabi (8469568165) (cropped).jpg
|birth_date         = {{birth date and age|1970|11|20}}
|birth_place        = [[Trucial States]] (now [[United Arab Emirates]])
|nationality        = {{flag|United Arab Emirates}}
|spouse             = {{marriage|Alia bint Mohammed bin Butti Al Hamed|1995}} <br> {{marriage|[[Manal bint Mohammed bin Rashid Al Maktoum|Manal bint Mohammed Al Maktoum]]|2 May 2005}}
|children           = {{plainlist|
*Zayed
*Fatima
*Mohammed
*Hamdan
*Latifa
*Rashid
}}
|father = [[Zayed bin Sultan Al Nahyan]]
|mother = [[Fatima bint Mubarak Al Ketbi]]
|networth = {{Increase}} [[US$]] 38 [[1000000000 (number)|billion]] (2014)
|module = 
{{Infobox deputy prime minister
|embed              = yes
|honorific-prefix   =
|honorific-suffix   =
|imagesize          = 
|smallimage         =
|alt                =
|caption            =
|order              =
|office             = [[List of Prime Ministers of the United Arab Emirates#List of Deputy Prime Ministers of the United Arab Emirates (1971-Present)|Deputy Prime Minister<br/>of the United Arab Emirates]]
|term_start         = 10 May 2009
|term_end           =
|alongside          = [[Saif bin Zayed Al Nahyan]]
|president          = [[Khalifa bin Zayed Al Nahyan]]
|primeminister      = [[Mohammed bin Rashid Al Maktoum]]
|predecessor        = [[Sultan bin Zayed bin Sultan Al Nahyan|Sultan bin Zayed Al Nahyan]]<br/>[[Hamdan bin Zayed bin Sultan Al Nahyan|Hamdan bin Zayed Al Nahyan]]
|successor          =
|office2            = [[Government of the United Arab Emirates|Minister of Presidential Affairs<br/>of the United Arab Emirates]]
|term_start2        = 1 November 2004
|term_end2          =
|president2         = [[Zayed bin Sultan Al Nahyan]]<br/>[[Khalifa bin Zayed Al Nahyan]]
|primeminister2     = [[Maktoum bin Rashid Al Maktoum]]<br/>[[Mohammed bin Rashid Al Maktoum]]
|occupation         = Politician 
|religion           =  [[Islam]]
|cabinet            = [[Cabinet of the United Arab Emirates]]
}}<!-- end of infobox deputy prime minister -->
|module2 =
{{Infobox royalty
|embed  = yes
|house = [[Al Nahyan]]
}}<!-- end of infobox royalty -->
}}<!-- end of infobox person -->
'''Mansour bin Zayed bin Sultan bin Zayed bin Khalifa Al Nahyan ''' (born 20 November 1970), commonly known as '''Sheikh Mansour''',<ref name=smguard1>{{cite web|title=Articles relating to Sheikh Mansour|url=https://www.theguardian.com/football/sheikh-mansour|publisher=[[The Guardian]]|accessdate=23 December 2013}}</ref><ref name=telegraphapril2013>{{cite news|last=Ogden|first=Mark|title=Manchester City owner Sheikh Mansour bin Zayed al Nahyan expected to secure MLS franchise in New York|url=http://www.telegraph.co.uk/sport/football/teams/manchester-city/10026499/Manchester-City-owner-Sheikh-Mansour-bin-Zayed-al-Nahyan-expected-to-secure-MLS-franchise-in-New-York.html|accessdate=23 December 2013|newspaper=The Daily Telegraph|date=29 April 2013}}</ref><ref name="indep"/> is the deputy prime minister of the [[United Arab Emirates]], minister of presidential affairs and member of the ruling family of Abu Dhabi. He is the half brother of the current President of UAE, [[Khalifa bin Zayed Al Nahyan]].<ref name="Cabinet Members">{{cite web|title=Cabinet Members|url=http://www.uaecabinet.ae/English/The%20Cabinet/Pages/CabinetMembers.aspx|publisher=UAE|accessdate=19 December 2012}}</ref>

He is also the chairman of the ministerial council for services, the Emirates Investment Authority and the Emirates Racing Authority. He sits on the [[Supreme Petroleum Council]] and the boards of numerous investment companies including the [[International Petroleum Investment Company]] and the [[Abu Dhabi Investment Council]].<ref name="Cabinet Members"/>

Mansour also owns stakes in a number of business ventures, including [[Virgin Galactic]] and [[Sky News Arabia]].<ref>{{cite news|last=Ruddick|first=Graham|title=Sheikh Mansour invests $280m in Virgin Galactic|url=http://www.telegraph.co.uk/finance/newsbysector/transport/5925697/Sheikh-Mansour-invests-280m-in-Virgin-Galactic.html|accessdate=19 December 2012|newspaper=The Telegraph|date=28 July 2008}}</ref> He is also the owner of the privately held [[Abu Dhabi United Group]] (ADUG), a specialist investment company that successfully acquired [[Manchester City F.C.|Manchester City Football Club]] in September 2008, and which has overseen a significant transformation at the Club since that time,<ref>{{cite news|title=Company Overview of Abu Dhabi United Group for Development and Investment|url=http://investing.businessweek.com/research/stocks/private/snapshot.asp?privcapId=47873031|newspaper=Bloomberg Businessweek|accessdate=27 June 2012}}</ref><ref>{{cite news|last=Engel|first=Matthew|title=Manchester-City-The-real-Premier-League-winner-Abu-Dhabi-United-Group-Development-Investment|url=http://www.dailymail.co.uk/debate/article-2144359/Manchester-City-The-real-Premier-League-winner-Abu-Dhabi-United-Group-Development-Investment.html|accessdate=19 December 2012|newspaper=Daily Mail|date=14 May 2012}}</ref> most notably, the club have won two top flight league titles for the first time since 1968, City's first [[Premier League]] titles. On May 21, 2013, [[Major League Soccer]] of the United States announced that its second [[New York City Metropolitan Area]] club, to be called [[New York City FC]], would begin play in the 2015 season, to be majority-owned by Mansour bin Zayed bin Sultan Al Nahyan, in association with brothers [[Hal Steinbrenner|Hal]] and [[Hank Steinbrenner]].<ref>{{cite web|url=http://msn.foxsports.com/foxsoccer/mls/story/manchester-city-join-forces-with-new-york-yankees-to-own-an-mls-club-052113|title=Man City, Yankees to own MLS club|author=Associated Press, Fox Sports Interactive Media|date=May 21, 2013|accessdate=May 21, 2013}}</ref>

==Early life and education==
Mansour was born in the [[Abu Dhabi (emirate)|Abu Dhabi emirate]] on 20 November 1970, the fifth son of the Emir of Abu Dhabi [[Zayed bin Sultan Al Nahyan|HH Sheikh Zayed bin Sultan Al Nahyan]].<ref name="indep">{{Cite news|last=Viner|first=Brian|title=Sheikh Mansour: The richest man in football |url=http://www.independent.co.uk/news/people/profiles/sheikh-mansour-the-richest-man-in-football-2052350.html|newspaper=[[The Independent]]|date=14 August 2010|accessdate=26 October 2010}}</ref> His mother is Sheikha [[Fatima bint Mubarak Al Ketbi]] and he has five full-brothers: [[Mohammed bin Zayed Al Nahyan|Crown Prince Mohammed]], [[Hamdan bin Zayed bin Sultan Al Nahyan|Hamdan]], [[Hazza bin Zayed Al Nahyan|Hazza]], Tahnoun, and [[Abdullah bin Zayed Al Nahyan|Abdullah]].<ref name=wleaks004>{{cite news|title=UAE Succession Update: The Post-Zayed Scenario|url=http://www.cablegatesearch.net/cable.php?id=04ABUDHABI3410|archive-url=https://web.archive.org/web/20130603051751/http://cablegatesearch.net/cable.php?id=04ABUDHABI3410|dead-url=yes|archive-date=3 June 2013|accessdate=16 April 2013|newspaper=Wikileaks|date=28 September 2004}}</ref> They are known as Bani Fatima or sons of Fatima.<ref name=ft5may>{{cite news|title=Abu Dhabi’s family business|url=http://www.ft.com/intl/cms/s/0/197e16f2-399b-11de-b82d-00144feabdc0.html#axzz2QYbDPdDK|accessdate=16 April 2013|newspaper=Financial Times|date=5 May 2009}}</ref>

Mansour attended [[Santa Barbara City College|Santa Barbara Community College]] as an English student in 1989.<ref name=wleaks22nov/> He is a graduate of [[United Arab Emirates University]] where he received a bachelor's degree in international affairs in 1993.<ref name=wleaks22nov/>

==Political career==
In 1997, Mansour bin Zayed was appointed chairman of the presidential office, which his father Zayed II is the first and by-then president of UAE. After the death of his father, he was appointed by his eldest half brother, [[Khalifa bin Zayed Al Nahyan|Khalifa II]], as first minister of presidential affairs of the United Arab Emirates, which is the merger of the presidential office and presidential court. He also served in a number of positions in Abu Dhabi to support his brother, Crown Prince [[Mohammed bin Zayed Al Nahyan]].

He was appointed chairman of the ministerial council for services, which is considered a ministerial entity attached to the Cabinet, comprising a number of ministers heading the services departments. Since 2000 he chaired [[National Center for Documentation and Research]]. In 2004 reshuffle, he became minister for presidential affairs.<ref name=wleaks22nov>{{cite news|title=UAE: Biographies of New Cabinet Members|url=http://www.cablegatesearch.net/cable.php?id=04ABUDHABI4188|accessdate=20 April 2013|newspaper=Wikileaks|date=22 November 2004}}</ref> In 2005, he became the deputy chairman of the Abu Dhabi education council (ADEC), chairman of the [[Emirates Foundation]], [[Abu Dhabi Food Control Authority]], and [[Abu Dhabi Fund for Development]]. In 2006, he was named the chairman of the [[Abu Dhabi Judicial Department]]. In 2007, he was appointed chairman of [[Khalifa bin Zayed Charity Foundation]].

Mansour served as the chairman of [[First Gulf Bank]] until 2006,<ref>{{Cite news|url=http://www.fgb.ae/en/aboutus/presskit/press5.asp|title= HH Sheikh Hazza bin Zayed elected first Gulf bank chairman|date=1 March 2006|accessdate=1 April 2010|work=First Gulf Bank}}</ref> and as a member of the board of trustees of the Zayed charitable and humanitarian foundation. Mansour has established scholarship programs for U.A.E students to study abroad. He is also chairman of the Emirates horse racing authority (EHRA).<ref name="indep"/> On 11 May 2009, he was appointed deputy prime minister, retaining his cabinet post of minister of presidential affairs.<ref name=gnews11may>{{cite news|title=Cabinet reshuffled; Saif and Mansour become Deputy Prime Ministers|url=http://gulfnews.com/news/gulf/uae/government/cabinet-reshuffled-saif-and-mansour-become-deputy-prime-ministers-1.68323|accessdate=19 April 2013|newspaper=Gulf News|date=11 May 2009}}</ref>

==Business portfolio==
{{Abu Dhabi Princely Family|right}}
Mansour heads the [[International Petroleum Investment Company]],<ref name="Bloomberg BusinessWeek">{{cite news|title=Executive Profile - Sheikh Mansour Bin Zayed Al Nahyan|url=http://investing.businessweek.com/research/stocks/private/person.asp?personId=33041425|newspaper=Bloomberg BusinessWeek|accessdate=27 July 2012}}</ref> which owns 71% of [[Aabar Investments PJSC|Aabar Investments]] and is used as an [[investment vehicle]].<ref name="thesun">{{Cite news|last=Custis|first=Neil|title=The richest man in football, Man City's Sheikh Mansour|url=http://www.thesun.co.uk/sol/homepage/sport/football/3110132/The-richest-man-in-football-Man-Citys-Sheikh-Mansour-is-uncovered-by-SunSport.html|newspaper=[[The Sun (United Kingdom)|The Sun]]|date=25 August 2010|accessdate=26 October 2010}}</ref>

In 2005, he was appointed as member of [[ADNOC|Supreme Petroleum Council]].<ref name="Bloomberg BusinessWeek"/> In the same year he chaired the board of directors for [[International Petroleum Investment Company]] and became the board member of [[Abu Dhabi Investment Authority]] (ADIA). In 2007, he was appointed chairman of the [[Emirates Investment Authority]], the [[sovereign wealth fund]] of UAE.<ref name="Bloomberg BusinessWeek"/>

Mansour has a 32% stake in [[Virgin Galactic]] after investing $280m in the project through [[Aabar Investments|Aabar]] in July 2009.<ref>{{Cite news|last=Ruddick|first=Graham|title=Sheikh Mansour invests $280m in Virgin Galactic |url=http://www.telegraph.co.uk/finance/newsbysector/transport/5925697/Sheikh-Mansour-invests-280m-in-Virgin-Galactic.html |publisher=[[Daily Telegraph]]|date=28 July 2009|accessdate=26 October 2010}}</ref><ref>{{Cite news|last=Wray|first=Richard|title=Abu Dhabi sheikh buys £170m stake in Virgin Galactic|url=https://www.theguardian.com/business/2009/jul/28/virginrichardbranson-spacetechnology|publisher=[[The Guardian]]|date=28 July 2009|accessdate=26 October 2010}}</ref> Aabar also has a 9.1% stake in [[Daimler AG|Daimler]] after purchasing the stake for $2.7 billion in March 2009<ref>{{Cite news|last=Reiter|first=Chris|title=Daimler Sells Aabar a 9.1% Stake for $4.7 Billion (Update3)|url=https://www.bloomberg.com/apps/news?pid=newsarchive&sid=aWOXzwD5Rw8s|newspaper=[[Bloomberg News]]|date=22 March 2009|accessdate=26 October 2010}}</ref> and it was reported that Aabar wishes to increase its stake to 15% in August 2010.<ref>{{Cite news|last=Christian|first=Andrew|title=Aabar wants to increase its 9.1% stake in Daimler to 15% |url=http://www.4wheelsnews.com/aabar-wants-to-increase-its-91-stake-in-daimler-to-15/|newspaper=4wheels News|date=30 August 2010 |accessdate=26 October 2010}}</ref> He owns the Abu Dhabi Media Investment Corporation (ADMIC) which partnered with British Sky Broadcasting to establish Sky News Arabia – a new Arabic-language news channel headquartered in the Emirate of Abu Dhabi.

===Sport===
Mansour is an accomplished horse rider and has won a number of endurance racing tournaments held in the [[Middle East]], and is chairman of the Emirates horse racing authority. He is a strong supporter of Arabian horse racing through the Sheik Mansour global Arabian flat racing festival with races on 5 continents. He is the patron of an annual road [[running]] competition in the Abu Dhabi, the [[Zayed International Half Marathon]].<ref>Abbasher, Yasir (8 January 2010). [http://gulfnews.com/sport/other-sports/full-zayed-marathon-next-year-1.565181 Full Zayed marathon next year]. ''Gulf News''. Retrieved 17 February 2010.</ref>

He is also the chairman of the Al Jazira sports company and was a leading figure in the Emirate of Abu Dhabi’s successful bid to host the FIFA Club World Cup in the UAE in both 2009 and 2010.<ref>{{cite web|url=http://jc.ae/web2/index.php?goto=board&lang=en|title=Club´s Board of Directors|publisher=Al Jazira Sports Club Official Site|accessdate=2 June 2012}}</ref> The company owns [[Al Jazira Club]], which plays football, [[volleyball]], [[team handball|handball]] and [[basketball]].<ref>{{cite web|title=Sport Activities|url=http://www.jc.ae/web2/index.php?goto=activity|publisher=Aljazira Sports Club Official Site|accessdate=23 September 2011}}</ref>  The football club achieved the President’s Cup in 2010-2011 and 2011-2012.<ref>{{cite web|title=Al Jazira|url=http://www.proleague.ae/en/club/al-jazira.html|accessdate=21 December 2012}}</ref>

Sheikh Mansour owns City Football Group, which consists of Manchester City F.C, Melbourne City F.C and New York City F.C. In 2009, HH Sheikh Mansour bought the City Football Group for 265 Million British Pounds. Approximately one year ago, HH Sheikh Mansour sold only 13% of City Football Group for the same price.{{citation needed|date=August 2016}}

==Personal life==
Mansour has two wives and five children. He married Sheikha Alia bint Mohammed bin Butti Al Hamed in the mid-1990s.<ref name=wleaks22nov/> They have one son, Zayed. Alia is the sister of [[Abdulla Bin Mohammed Bin Butti Al Hamed]]. Mansour married Sheikha [[Manal bint Mohammed bin Rashid Al Maktoum]], president of the Dubai women establishment in May 2005, and his second cousin once removed. They have five children, two daughters and three sons:
* Fatima (born 9 June 2006).
* Mohammed (born 4 December 2007).
* Hamdan (born 21 June 2011).
* Latifa (born 23 January 2014).
* Rashid (born 22 March 2017).<ref>{{cite web|title=Sheikh Mohammed becomes a proud grandfather - Khaleej Times|url=http://www.khaleejtimes.com/nation/dubai/sheikh-mohammed-becomes-proud-grandfather|website=www.khaleejtimes.com|accessdate=22 March 2017|language=English|date=22 March 2017}}</ref>

==Ancestry==
{{ahnentafel top|width=100%}}
<center>{{ahnentafel-compact5
|style=font-size: 90%; line-height: 110%;
|border=1
|boxstyle=padding-top: 0; padding-bottom: 0;
|boxstyle_1=background-color: #fcc;
|boxstyle_2=background-color: #fb9;
|boxstyle_3=background-color: #ffc;
|boxstyle_4=background-color: #bfc;
|boxstyle_5=background-color: #9fe;
|1= 1. '''Sheikh Mansour bin Zayed Al Nahyan'''
|2= 2. [[Zayed bin Sultan Al Nahyan|Sheikh Zayed bin Sultan Al Nahyan]]
|3= 3. [[Fatima bint Mubarak Al Ketbi|Sheikha Fatima bint Mubarak Al Ketbi]]
|4= 4. [[Sultan bin Zayed bin Khalifa Al Nahyan|Sheikh Sultan bin Zayed Al Nahyan]]
|5= 5. Sheikha Salma bint Butti Al Qubaisi
|6= 6. Sheikh Mubarak Al Ketbi 
|8= 8. [[Zayed bin Khalifa Al Nahyan|Sheikh Zayed bin Khalifa Al Nahyan]]
|10= 10. Sheikh Butti Al Qubaisi
|16= 16. Sheikh Khalifa bin Shakbut Al Nahyan
}}</center>
{{ahnentafel bottom}}

==References==
{{reflist|33em}}
{{Commons category}}

{{The City Football Group}}
{{Al Jazira Club}}
{{Manchester City F.C.}}
{{Melbourne Heart}}
{{New York City FC}}
{{Club Atlético Torque}}

{{DEFAULTSORT:Mansour Bin Zayed Al Nahyan}}
[[Category:1970 births]]
[[Category:Living people]]
[[Category:United Arab Emirates University alumni]]
[[Category:House of Al Nahyan]]
[[Category:Emirati bankers]]
[[Category:Emirati politicians]]
[[Category:Emirati Muslims]]
[[Category:Government ministers of the United Arab Emirates]]
[[Category:Children of Presidents of the United Arab Emirates]]
[[Category:Emirati football chairmen and investors]]
[[Category:Manchester City F.C. directors and chairmen]]