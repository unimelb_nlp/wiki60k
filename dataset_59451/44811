{{Infobox NCAA Basketball Conference Tournament
| Year           = 2007
| Conference     = ACC
| Gender         = Men's
| Image          = 2007acctournament.png
| ImageSize      = 150px 
| Caption        = 2007 ACC Tournament logo
| Teams          = 12
| Arena          = [[St. Pete Times Forum]]
| City           = [[Tampa, Florida]]
| Champions      = [[North Carolina Tar Heels men's basketball|North Carolina]]
| TitleCount     = 16th
| Coach          = [[Roy Williams (coach)|Roy Williams]]
| CoachCount     = 1st
| MVP            = [[Brandan Wright]]
| MVPTeam        = (North Carolina)
| Attendance     =
| OneTopScorer   =
| TwoTopScorers  =
| TopScorer      =
| TopScorerTeam  =
| TopScorer2     =
| TopScorer2Team =
| Points         =
}}

The '''2007 [[Atlantic Coast Conference Men's Basketball Tournament]]''' took place from March 8–11 in [[Tampa, Florida]], at the [[St. Pete Times Forum]], the first time the tournament was held in Florida. The quarterfinal games were televised nationwide on [[ESPN2]]. Semifinals and the championship game were televised on [[ESPN]]. The tournament was also televised by [[Raycom Sports]] in ACC markets. For the first time ever, Raycom broadcast the tournament in [[HDTV|high definition]].

The top four regular season teams ([[North Carolina Tar Heels|North Carolina]], [[Virginia Cavaliers|Virginia]], [[Virginia Tech Hokies|Virginia Tech]], and [[Boston College Eagles|Boston College]]) received first-round byes and played their first games in the quarterfinals. Both North Carolina and Virginia finished the [[2006/2007 ACC Men's Basketball Season|regular season]] tied for first place with an 11–5 record, but North Carolina received the #1 seed by virtue of the head-to-head tiebreaker.

All four opening-round games were won by the lower seeds. This tournament was the lowest [[Duke Blue Devils|Duke]] had been seeded (seventh) in the ACC Tournament since 1995, when they were the ninth seed.  Their on-court struggles were compounded by the suspension of freshman [[Gerald Henderson, Jr.|Gerald Henderson]].<ref>[http://dukechronicle.com/article/henderson-suspended-late-foul Henderson suspended for late foul]</ref>  By defeating Duke, [[NC State Wolfpack|NC State]] ended Duke's 8-year domination of the ACC Tournament.  Prior to this loss, Duke was 23–1 in ACC Tournament play over the previous 8 years, losing only the 2004 finals to Maryland. The 2007 final round was identical to the final round of the Women's ACC Tournament, with North Carolina beating NC State for the title.

The 2007 tournament marked the first time a 10th seed won a game in an ACC tournament, much less three in a row (although 12th seed Wake Forest won two games in the 2006 Tournament). It was the second time a team has played 4 games (NC State in 1997), and the first time a team seeded as low as 10 reached the championship game. Previously the lowest seed to reach the title game was #8 seed NC State in [[1997 ACC Men's Basketball Tournament|1997]], when the conference had nine members.

By winning the ACC championship, North Carolina received an automatic bid to the [[2007 NCAA Men's Division I Basketball Tournament|NCAA Tournament]]North Carolina's [[Brandan Wright]] won the tournament's Most Valuable Player award.

==Bracket==
{{4RoundBracket-Byes 
| RD1='''First Round''' <br> March 8, 2007
| RD2='''Quarterfinals''' <br> March 9, 2007
| RD3='''Semifinals''' <br> March 10, 2007
| RD4='''Championship Game''' <br> March 11, 2007
| group1=
| group2=
| group3=

| RD1-seed03=8
| RD1-team03=Clemson
| RD1-score03=66
| RD1-seed04=9
| RD1-team04='''Florida State'''<ref>[http://www.theacc.com/sports/m-baskbl/recaps/030807aab.html No. 9 Seed Florida State Eliminates No. 8 Seed Clemson from the ACC Tournament in the Last Second :: Florida State's Al Thornton makes a free-throw with 1.5 seconds remaining to lift the Seminoles over the Tigers, 67-66<!-- Bot generated title -->]</ref>
| RD1-score04='''67'''

| RD1-seed07=5
| RD1-team07=#17 Maryland
| RD1-score07=62
| RD1-seed08=12 
| RD1-team08='''Miami'''<ref>[http://www.theacc.com/sports/m-baskbl/recaps/030807aaj.html Miami Upsets No. 17 Maryland, 67-62, In Opening Round :: Jack McClinton scored 17 points for the Hurricanes<!-- Bot generated title -->]</ref>
| RD1-score08='''67'''

| RD1-seed11=7
| RD1-team11=#21 Duke
| RD1-score11=80
| RD1-seed12=10
| RD1-team12='''NC State'''<ref>[http://www.theacc.com/sports/m-baskbl/recaps/030807aao.html Wolfpack Knocks Out No. 21 Blue Devils In Opening Round Of ACC Tourney, 85-80 :: NC State (16-14) advances to Friday's quarterfinals against No. 2 seed Virginia at 7 p.m<!-- Bot generated title -->]</ref>
| RD1-score12='''85'''*

| RD1-seed15=6
| RD1-team15=Georgia Tech
| RD1-score15=112
| RD1-seed16=11
| RD1-team16='''Wake Forest'''<ref>[http://www.theacc.com/sports/m-baskbl/recaps/030907aaa.html Wake Forest Edges Georgia Tech In Double-Overtime Thriller, 114-112 :: Harvey Hale pours in 21 points in the extra periods to lead Demon Deacons over Yellow Jackets<!-- Bot generated title -->]</ref>
| RD1-score16='''114'''**

| RD2-seed01=1
| RD2-team01='''#8 North Carolina'''<ref>[http://www.theacc.com/sports/m-baskbl/recaps/030907aak.html No. 8 North Carolina Moves on to ACC Semifinals :: Tar Heels' Wayne Ellington scores 18 points to pace top-seeded North Carolina to a 73-58 quarterfinal victory over ninth-seeded Florida State<!-- Bot generated title -->]</ref>
| RD2-score01='''73'''
| RD2-seed02=9
| RD2-team02=Florida State
| RD2-score02=58

| RD2-seed03=4
| RD2-team03='''Boston College'''
| RD2-score03='''74'''*
| RD2-seed04=12
| RD2-team04=Miami<ref>[http://www.theacc.com/sports/m-baskbl/recaps/030907aap.html Boston College Rallies Past Miami to Advance to ACC Semifinals :: The Eagles' Tyrese Rice scores a career-high 32 points as fourth-seeded Boston College defeats twelfth-seeded Miami, 74-71 in overtime, in the ACC quarterfinals<!-- Bot generated title -->]</ref>
| RD2-score04=71

| RD2-seed05=2
| RD2-team05=Virginia
| RD2-score05=71
| RD2-seed06=10
| RD2-team06='''NC State'''<ref>[http://www.theacc.com/sports/m-baskbl/recaps/030907aat.html Wolfpack Upends Cavaliers In ACC Tournament Semis, 79-71 :: Sean Singletary scores 23 points to pace Virginia (20-10)<!-- Bot generated title -->]</ref>
| RD2-score06='''79'''

| RD2-seed07=3
| RD2-team07='''Virginia Tech'''<ref>[http://www.theacc.com/sports/m-baskbl/recaps/031007aab.html Virginia Tech Defeats Wake Forest, 71-52 :: The third-seeded Hokies used a fast-paced tempo to take advantage of Wake's short turnaround<!-- Bot generated title -->]</ref>
| RD2-score07='''71'''
| RD2-seed08=11
| RD2-team08=Wake Forest
| RD2-score08=52

| RD3-seed01=1
| RD3-team01='''#8 North Carolina'''<ref>[http://www.theacc.com/sports/m-baskbl/recaps/031007aak.html Wright Leads No. 8 Tar Heels Past Eagles In ACC Tournament Semis, 71-56 :: UNC (27-6) returns to conference tournament championship game for first time since 2001<!-- Bot generated title -->]</ref>
| RD3-score01='''71'''
| RD3-seed02=4
| RD3-team02=Boston College
| RD3-score02=56

| RD3-seed03=10
| RD3-team03='''NC State'''<ref>[http://www.theacc.com/sports/m-baskbl/recaps/031007aat.html NC State Wins its Third-Straight ACC Tournament Game as it Pushes Past Virginia Tech, 72-64 :: Tenth-seeded NC State knocks off third-seeded Virginia Tech in the ACC semis to advance to a showdown with North Carolina in the finals tomorrow<!-- Bot generated title -->]</ref>
| RD3-score03='''72'''
| RD3-seed04=3
| RD3-team04=Virginia Tech
| RD3-score04=64

| RD4-seed01=1
| RD4-team01='''#8 North Carolina'''
| RD4-score01='''89'''
| RD4-seed02=10
| RD4-team02=NC State
| RD4-score02=80
}}
<nowiki>* Denotes Overtime Game</nowiki>

<nowiki>** Denotes Double Overtime Game</nowiki>

<nowiki>AP Rankings at time of tournament</nowiki>

== See also ==
{{Portal|ACC}}
*[[List of Atlantic Coast Conference men's basketball tournament champions]]
*[[2006 ACC Men's Basketball Tournament]]

==References==
{{reflist}}

==External links==
*[http://www.theacc.com/sports/m-baskbl/acc-mtournament-central-2007.html Tournament Homepage]

{{ACC Men's Basketball Tournament navbox}}

[[Category:2006–07 Atlantic Coast Conference men's basketball season]]
[[Category:ACC Men's Basketball Tournament]]
[[Category:2007 in sports in Florida]]
[[Category:21st century in Tampa, Florida]]
[[Category:College sports in Florida]]
[[Category:Sports in Tampa, Florida]]
[[Category:Sports competitions in Florida]]