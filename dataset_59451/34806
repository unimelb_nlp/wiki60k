{{good article}}
{{Infobox Hurricane
| Name=Tropical Storm Sixteen
| Type=Tropical storm
| Year=1887
| Basin=Atl
| Image location=1887 Atlantic tropical storm 16 track.png
| Image name=Track map of the storm
| Formed=October&nbsp;29, 1887
| Dissipated=November&nbsp;6, 1887
| Extratropical=October 31
| 1-min winds=60
| Pressure=993
| Damages=0.007
| Damagespre=~
| Fatalities=2 total
| Areas=[[Gulf Coast of the United States|Gulf Coast]], [[Florida]], [[Georgia (U.S. state)|Georgia]], [[The Carolinas]], [[Virginia]], [[East Coast of the United States]], [[Atlantic Canada]]
| Hurricane season=[[1887 Atlantic hurricane season]]
}}
The '''1887 Halloween tropical storm''' was a late-season [[tropical cyclone]] that caused significant damage along the [[East Coast of the United States]] during [[Halloween]] of 1887. The sixteenth tropical storm of the [[1887 Atlantic hurricane season|annual hurricane season]], it formed from an area of disturbed weather over the [[Gulf of Mexico]] on October&nbsp;29. The storm later [[landfall (meteorology)|came ashore]] along the west coast of Florida. After crossing the state, it produced [[Thunderstorm#Severe thunderstorms|severe thunderstorm]]s along the North Carolina–Virginia coastline before becoming [[extratropical cyclone|extratropical]] on November&nbsp;1. The extratropical system intensified into the equivalent of a Category&nbsp;1 hurricane on the modern-day [[Saffir–Simpson Hurricane Scale]]. It eventually dissipated on November&nbsp;6, shortly after hitting northwest France.

The storm affected the town of [[Norfolk, Virginia|Norfolk]], where it became the most damaging storm since 1879. Despite the damage inland, the storm is best known for the unusually high amount of shipwrecks and maritime incidents it caused. One ship, a [[schooner]] called the ''Manantico'', capsized, killing the captain and one of its crew members. Three other ships were driven ashore on Virginia beaches from Dam Rock to [[Cape Henry]], and numerous others were put in danger.

==Meteorological history==
The storm originated from an area of disturbed weather that had persisted in the [[Gulf of Mexico]] during late October 1887, outside the area of coastal stations.<ref name="MWR">{{
cite web
| author=[[United States Army Signal Corps|United States Signal Service]]
| year=1887
| title=Monthly Weather Review: Areas of Low Pressure: X
| accessdate=2012-01-12
| url=http://docs.lib.noaa.gov/rescue/mwr/015/mwr-015-10-0265b.pdf
}}</ref> On October&nbsp;29, the disturbance completed [[tropical cyclogenesis]] and became the sixteenth tropical storm of the season.<ref name="Atl Best Track">{{
cite web
| author=[[National Hurricane Center]]
| year=2011
| title=Atlantic Best Track Data 1851–2010
| accessdate=2012-01-12
| url=http://www.nhc.noaa.gov/data/hurdat/tracks1851to2010_atl_2011rev.txt
}}</ref> After forming, the storm was located 200 miles (320&nbsp;km) northwest of Key West and began moving east-northeastward, making [[landfall (meteorology)|landfall]] on the Florida Peninsula. It crossed land and emerged over water within the next eight hours while weakening.<ref name="Partagas">{{
cite web
|author1=Partagas, Jose Fernandez  |author2=Dias, H. F.
| year=1996
| title=A Reconstruction of Historical Tropical Cyclone Frequency in the Atlantic from Documentary and other Historical Sources Part III: 1881–1890: 1887b
| publisher=NOAA
| accessdate=2012-01-12
| url=http://www.aoml.noaa.gov/hrd/Landsea/Partagas/1887/1887b.pdf
}}</ref> During its passage near [[Fort Meade, Florida|Fort Meade]], the storm had an estimated [[Atmospheric pressure|barometric pressure]] of {{convert|1007|mbar|inHg}}, supporting minimal tropical storm strength.<ref name="HURDAT">{{
cite web
| author=Landsea, Chris
| year=2005
| title=Documentation of Atlantic Tropical Cyclone Changes in HURDAT
| publisher=National Oceanic and Atmospheric Administration Hurricane Research Division
| accessdate=2012-01-12
| url=http://www.aoml.noaa.gov/hrd/hurdat/metadata_19151920_new.html
|display-authors=etal}}</ref> Upon crossing the state, it paralleled the [[East Coast of the United States|East Coast]] for two days while restrengthening. It passed closest to land near the [[North Carolina]] coastline on October&nbsp;31 at its peak intensity, with [[Maximum sustained winds|sustained winds]] of {{convert|70|mph|km/h|abbr=on}} and a pressure of {{convert|993|mbar|inHg}}. Shortly after moving away from land, it became [[Extratropical cyclone|extratropical]].<ref name="Atl Best Track"/><ref name="Partagas"/>

The extratropical cyclone moved away from the coast and strengthened to the equivalent of a Category&nbsp;1 hurricane on the modern-day [[Saffir-Simpson Hurricane Scale]] on November&nbsp;1. It began a weakening trend on November&nbsp;2 while taking a wavering path across the north [[Atlantic Ocean]].<ref name="Atl Best Track"/> On November&nbsp;4, the cyclone started a general east-southeast motion, passing near the southwest coasts of Ireland and Great Britain. It made landfall on the [[Cotentin Peninsula]] of France on November&nbsp;6 and dissipated, although one proposed track, which is a modified track that differs from the official record due to new evidence or theory, was given by Charles Mitchell that showed the cyclone executing a counter-clockwise loop over northwest France until dissipating the storm on November&nbsp;8.<ref name="Partagas"/>

==Impact==
In its formative stages, the storm was responsible for causing rain from the [[Rio Grande Valley]] along the Gulf Coast to Florida.<ref name="MWR"/> As the storm crossed Florida, Fort Meade recorded less than {{convert|1|in|cm|abbr=on}} of rainfall.<ref name="HURDAT"/> As it  strengthened off the East Coast, the storm caused damage in various towns. A maximum wind speed of {{convert|54|mph|km/h|abbr=on}} was measured at [[Hatteras, North Carolina]]. In [[Kitty Hawk, North Carolina|Kitty Hawk]], impact from the storm was more intense, generating maximum winds of {{convert|60|mph|km/h|abbr=on}} to as high as {{convert|70|mph|km/h|abbr=on}} from late on October&nbsp;30 to the following afternoon; despite the intensity, only minor damage was reported. In [[Lenoir, North Carolina|Lenoir]] and [[Raleigh, North Carolina|Raleigh]], heavy rain totals up to {{convert|4.18|in|cm|abbr=on}} were reported. Unusually, reports of [[hail]] and [[snow]] were also received from these locations. Telegraph poles were snapped on [[Bodie Island]] and south of Little Kinnakeet, affecting communication. The worst land-based impact from the storm was in Virginia. Cape Henry was hit with a combination of wind, rain, and blown sand on October&nbsp;31 and communications between Cape Henry and Norfolk were lost. In Norfolk, the storm was the longest-lasting and most damaging since the [[1879 Atlantic hurricane season#Hurricane Two|Great Beaufort Hurricane]] of 1879.<ref name="MWR"/> The storm conditions made beaches in the area so hazardous that they were watched day and night.<ref name="Pouliot">{{cite book
| last       = Pouliot
| first      = Richard A.
|author2= Julie J. Pouliot
| title         = Shipwrecks on the Virginia Coast and the Men of the United States Life-Saving Service
| year      = 1986
| publisher = Tidewater Publishers: Centreville MD
| isbn      = 978-0-87033-352-1
| pages     = 70–72
}}</ref> Effects from the storm reached as far north as [[Provincetown, Massachusetts]], where winds of {{convert|60|mph|km/h|abbr=on}} were recorded.<ref name="Partagas"/>

Despite causing damage along the East Coast, the storm is best known for causing a record number of maritime incidents.<ref name="Roth">{{
cite web
| author=Roth, David M.
| year=2001
| title=Late Nineteenth Century Virginia Hurricanes
| accessdate=2012-01-12
| url=http://www.wpc.ncep.noaa.gov/research/roth/valate19hur.htm
}}</ref> Numerous ships were caught in the storm from October&nbsp;30, when the [[steamship]] ''Claribel'' reported gale-force winds, to November&nbsp;6, when another steamship, the ''Australia'', reported stormy weather. One ship, the [[brig]] ''Osseo'', was caught in the storm on November&nbsp;1 and became flooded. Although the pumps were manned, the water level inside the ship soon reached {{convert|4|ft|m|abbr=on}}. After being carried away by a wave, the distressed ship was spotted by the ''Camalia'', which rescued the crew and brought them to port. Another vessel, the ''Wyonoka'', spotted a sunken schooner with its five crew grasping the mast and ropes: they were also rescued.<ref name="Partagas"/> In addition, four ships were deemed total losses after being beached in Virginia. The first was the ''Mary D. Cranmer'', which was ripped from its cables and stranded near Cape Henry. Shortly after the rescue of the crew of the Cranmer another ship, the ''Carrie Holmes'', was found beached. The ship had been driven so far up the beach that its crew were able to jump and wade to safety.<ref name="Pouliot"/>

A third ship, the ''Manantico'' also crashed into shore due to a combination of the storm and human error in which the captain confused Cape Henry with [[Cape Charles (headland)|Cape Charles]] after spotting another schooner. The ''Manantico'' was also where the two deaths associated with the storm occurred. The first was when a cook on the ship was crushed to death by the cargo of lumber being hauled by the ship. The ship was then pushed towards a sand bar. The captain, who had stayed high on the starboard side for safety, began climbing down to slip the ship cables, but the ship made a sudden stop. This flung the captain into the water, and he drowned. Both bodies were found after the storm and were very disfigured. The captain was sent to [[Middletown, Connecticut]] for interment while the body of the cook was buried on the beach.<ref name="USLSS">{{
cite book
| author=United States Life-Saving Service
| year=1889
| title=Annual Report of the Operations of the United States Life-Saving Service for the Fiscal Year Ending June 30, 1888
| accessdate=2012-01-12
| pages=25–27
| url=https://books.google.com/books?id=I3oDAAAAYAAJ&pg=PA47 
}}</ref> The final ship was the ''Harriet Thomas'', which was the schooner spotted by the ''Manantico''. After beaching, the crew managed to get a rope to shore where fishermen had tied the other end. The crew were able to climb ashore, although the captain had to be rescued via alternate means due to being too heavy for this method. The ship was written off as a $7000 (1887 USD) loss. Although all four ships were beached, due to the loss of communications, only one wreck – that of the ''Mary D. Cranmer'' – was reported in the ''Norfolk Virginian'' newspaper. As a result, news of the two deaths from the ''Manantico'' were initially unreported.<ref name="Pouliot"/>

==See also==
{{Portal|Tropical cyclones}}
*[[List of Atlantic hurricanes]]
*[[1887 Atlantic hurricane season]]

==References==
{{Reflist|colwidth=33em}}

[[Category:Atlantic tropical storms|1887 Halloween]]
[[Category:Hurricanes in Florida|1887 Halloween]]
[[Category:Hurricanes in North Carolina|1887 Halloween]]
[[Category:Hurricanes in Virginia|1887 Halloween]]
[[Category:1880–1889 Atlantic hurricane seasons]]
[[Category:1887 natural disasters in the United States]]
[[Category:1887 meteorology]]