{{Infobox journal
| title = IEEE Microwave and Wireless Components Letters
| cover = [[File:IEEE Microwave Theory and Wireless Component Letters.jpg]]
| editor = John Papapolymerou
| discipline = [[Microwave]] theory and techniques
| formernames = IEEE Microwave and Guided Wave Letters
| abbreviation = IEEE Microw. Wirel. Compon. Lett.
| publisher = [[IEEE Microwave Theory and Techniques Society]]
| country =
| frequency = Monthly
| history = 1991-present
| openaccess =
| license =
| impact = 2.236
| impact-year = 2013
| website = http://www.mtt.org/letters.html
| link1 = http://ieeexplore.ieee.org/xpl/RecentIssue.jsp?punumber=7260
| link1-name = Online access
| link2 = 
| link2-name =
| JSTOR =
| OCLC = 50187335
| LCCN = 00214018
| CODEN =
| ISSN = 1531-1309
| ISSNlabel = IEEE Microwave and Wireless Components Letters
| ISSN2 = 1051-8207
| ISSN2label = IEEE Microwave and Guided Wave Letters
| eISSN =
}}
'''''IEEE Microwave and Wireless Components Letters''''' is a monthly [[peer-reviewed]] [[scientific journal]] published by the [[IEEE Microwave Theory and Techniques Society]]. The [[editor-in-chief]] is John Papapolymerou ([[Georgia Institute of Technology]]). The journal covers research on [[electromagnetic radiation]] and the relevant, physical components to achieve such radiations. It focuses on devices, intermediate parts of systems, and completed systems of the interested [[wavelength]]s, but also includes papers which emphasize theory, experiment, and applications of the subjects covered.<ref name=first-link>{{cite web |url=http://ieeexplore.ieee.org/xpl/RecentIssue.jsp?punumber=7260 |title=Aims and Scope |publisher=[[IEEE Microwave Theory and Techniques Society]] |work=IEEE Microwave and Wireless Components Letters |accessdate=2011-02-04}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in ''[[Science Citation Index Expanded]]'' and ''[[Current Contents]]/Engineering, Computing & Technology''. According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 2.236.<ref name=WoS>{{cite book |year=2014 |chapter=IEEE Microwave and Wireless Components Letters |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== History ==
The journal was established in 1999 as ''IEEE Microwave and Guided Wave Letters'' and obtained its current title in December 2000.<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/101210890 |title=IEEE Microwave and Wireless Components Letters |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=2015-03-31}}</ref><ref>{{Cite web |title=IEEE Microwave and Guided Wave Letters |work=Library of Congress catalog |publisher=[[Library of Congress]] |url=http://lccn.loc.gov/91642577 |accessdate=2011-02-03}}</ref><ref>{{Cite web |title=IEEE Microwave and Guided Wave Letters |publisher=[[IEEE Microwave Theory and Techniques Society]] |url=http://ieeexplore.ieee.org/xpl/tocresult.jsp?isnumber=2650 |accessdate =2011-02-03}}</ref> This former title was abstracted and indexed in ''[[Inspec|Computer & Control Abstracts]]'', ''[[Inspec|Electrical & Electronics Abstracts]]'', and ''[[Inspec|Physics Abstracts]]''.<ref>{{Cite web |title=IEEE microwave and guided wave letters : a publication of the IEEE Microwave Theory and Techniques Society |work=Library catalog |publisher=[[National Library of Australia]] |date= |url=http://catalogue.nla.gov.au/Record/1963473?lookfor=isn:1051-8207&offset=1&max=1 |format= |accessdate =2011-02-03}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.ieee.org/index.html}}

[[Category:IEEE academic journals|Microwave and Wireless Components Letters]]
[[Category:Physics journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1991]]