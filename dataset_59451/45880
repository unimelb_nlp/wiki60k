{{redirect|Vasari|the Italian surname|Vasari (surname)}}

{{Use dmy dates|date=June 2016}}
{{Infobox artist
| bgcolour      =
| name          = Giorgio Vasari
| image         = Giorgio Vasari Selbstporträt.jpg
| caption       = Vasari's self-portrait
| birth_date     = {{birth date|df=yes|1511|7|30}}
| birth_place      = [[Arezzo]], [[Republic of Florence]]
| death_date     = {{Death date and age|df=yes|1574|6|27|1511|7|30}}
| death_place    = [[Florence]], [[Grand Duchy of Tuscany]]<ref name=gaunt>Gaunt, W. (ed.) (1962) ''Everyman's dictionary of pictorial art. Volume II.'' London: Dent, p. 328. ISBN 0-460-03006-X</ref>
| nationality   = Italian
| field         = Painting, architecture
| training      = [[Andrea del Sarto]]
| movement      = [[Renaissance]]
| works         = Biographies of Italian artists
| patrons       =
| influences    =
| influenced    = [[Karel van Mander]]
}}
'''Giorgio Vasari''' ({{IPA-it|ˈdʒordʒo vaˈzaːri|lang}}; 30 July 1511 – 27 June 1574) was an [[List of Italian painters|Italian painter]], [[Architecture of Italy|architect]], writer, and historian, most famous today for his ''[[Lives of the Most Excellent Painters, Sculptors, and Architects]]'', considered the ideological foundation of [[art history|art-historical]] writing.

==Early life==
Vasari was born in [[Arezzo]], Tuscany.<ref name=gaunt/> Recommended at an early age by his cousin [[Luca Signorelli]], he became a pupil of [[Guglielmo da Marsiglia]], a skillful painter of [[stained glass]]. Sent to [[Florence]] at the age of sixteen by Cardinal Silvio Passerini, he joined the circle of [[Andrea del Sarto]] and his pupils [[Rosso Fiorentino]] and [[Jacopo Pontormo]] where his humanist education was encouraged. He was befriended by [[Michelangelo]] whose painting style would influence his own.

==Painting==
[[File:Giorgio Vasari - Six Tuscan Poets - Google Art Project.jpg|thumbnail| Giorgio Vasari, ''Six Tuscan Poets'', c. 1544. From left to right: [[Marsilio Ficino]], [[Cristoforo Landino]], [[Petrarch|Francesco Petrarca]], [[Giovanni Boccaccio]], [[Dante Alighieri]] and [[Guido Cavalcanti]].<ref>{{cite web|url=http://collections.artsmia.org/art/1850/six-tuscan-poets-giorgio-vasari|title=Six Tuscan Poets, Giorgio Vasari|publisher=Minneapolis Institute of Art|accessdate=}}</ref>]]
[[File:Giorgio Vasari - The Garden of Gethsemane - Google Art Project.jpg|thumb|Giorgio Vasari, The Garden of Gethsemane]]  
In 1529, he visited Rome where he studied the works of [[Raphael]] and other artists of the Roman [[High Renaissance]]. Vasari's own [[Mannerist]] paintings were more admired in his lifetime than afterwards. In 1547 he completed the hall of the chancery in Palazzo della Cancelleria in Rome with frescoes that received the name [[Sala dei Cento Giorni]]. He was consistently employed by members of the [[Medici family]] in [[Florence]] and Rome, and worked in [[Naples]], Arezzo and other places. Many of his pictures still exist, the most important being the wall and ceiling paintings in the Sala di Cosimo I in the [[Palazzo Vecchio]] in Florence, where he and his assistants were at work from 1555, and the frescoes begun by him inside the vast [[cupola]] of the [[Santa Maria del Fiore|Duomo]] were completed by [[Federico Zuccari]] and with the help of [[Giovanni Balducci]]. He also helped to organize the decoration of the [[Studiolo of Francesco I (Palazzo Vecchio)|Studiolo]], now reassembled in the Palazzo Vecchio. Among his other pupils or followers are included [[Sebastiano Flori]], [[Bartolommeo Carducci]], [[Domenico Benci]],  Tommaso del Verrocchio, Federigo di Lamberto (Federigo del Padovano), Niccolo Betti, Vittor Casini, [[Mirabello Cavalori]] (Salincorno), Jacopo Coppi (Jacopo di Meglio), [[Piero di Ridolfo]], [[Stefano Veltroni]] of Monte Sansavino, Orazio Porta of Sansavino, [[Alessandro Fortori]] of Arezzo, Bastiano Flori of Arezzo, Fra Salvatore Foschi of Arezzo, and Andrea Aretino.<ref>[https://books.google.com/books?id=BaIZAAAAYAAJ The History of Painting in Italy: The Florentine, Sienese, and Roman schools], by Luigi Lanzi, page 201-202.</ref>

==Architecture==
Aside from his career as a painter, Vasari was also successful as an architect. His [[loggia]] of the [[Uffizi|Palazzo degli Uffizi]] by the [[Arno]] opens up the vista at the far end of its long narrow courtyard, a unique piece of urban planning that functions as a public piazza, and which, if considered as a short street, is unique as a Renaissance street with a unified architectural treatment. The view of the Loggia from the Arno reveals that, with the [[Vasari Corridor]], it is one of very few structures that line the river which are open to the river itself and appear to embrace the riverside environment.

[[File:Firenze - Uffizi.jpg|thumb|right|The Uffizi Loggia]]
In Florence, Vasari also built the long passage, now called Vasari Corridor, which connects the Uffizi with the [[Palazzo Pitti]] on the other side of the river. The enclosed corridor passes alongside the River Arno on an arcade, crosses the [[Ponte Vecchio]] and winds around the exterior of several buildings.

He also renovated the medieval churches of [[Santa Maria Novella]] and [[Basilica di Santa Croce, Florence|Santa Croce]]. At both he removed the original [[rood screen]] and loft, and remodelled the retro-[[choir]]s in the Mannerist taste of his time.  In Santa Croce, he was responsible for the painting of ''The Adoration of the Magi'' which was commissioned by [[Pope Pius V]] in 1566 and completed in February 1567.  It was recently restored, before being put on exhibition in 2011 in Rome and in Naples.  Eventually it is planned to return it to the church of Santa Croce in [[Bosco Marengo]] (Province of Alessandria, Piedmont).

In 1562 Vasari built the octagonal dome on the [[Basilica of Our Lady of Humility]] in [[Pistoia]], an important example of [[high Renaissance]] architecture.<ref>''The Christian Travelers Guide to Italy'' by David Bershad, Carolina Mangone, Irving Hexham 2001 ISBN 0-310-22573-6-page [https://books.google.com/books?id=KRkU9ocQr2oC&pg=PA139]</ref>

In Rome, Vasari worked with [[Giacomo Barozzi da Vignola]] and [[Bartolomeo Ammanati]] at [[Pope Julius III]]'s [[Villa Giulia]].

==''The Lives of the Most Excellent Painters, Sculptors, and Architects''==
{{main|Lives of the Most Excellent Painters, Sculptors, and Architects}}
[[File:Vite.jpg|thumb|A cover of the ''Lives'']]
Often called "the first art historian",<ref>[http://www.dictionaryofarthistorians.org/vasarig.htm Vasari, Giorgio.] Dictionary of Art Historians, 2013. Retrieved 26 May 2013. [http://www.webcitation.org/6GtHONdEV Archived here.]</ref> Vasari invented the genre of the encyclopedia of artistic biographies with his ''Le Vite de' più eccellenti pittori, scultori, ed architettori'' (''[[Lives of the Most Eminent Painters, Sculptors, and Architects]]''), dedicated to Grand Duke [[Cosimo I de' Medici]], which was first published in 1550. He was the first to use the term "[[Renaissance]]" (''rinascita'') in print, though an awareness of the ongoing "rebirth" in the arts had been in the air since the time of [[Leone Battista Alberti|Alberti]], and he was responsible for our use of the term [[Gothic Art]], though he only used the word Goth which he associated with the "barbaric" German style. The ''Lives'' also included a novel treatise on the technical methods employed in the arts.<ref>Vasari, Giorgio. (1907) ''[https://archive.org/details/vasariontechniqu1907vasa Vasari on technique: being the introduction to the three arts of design, architecture, sculpture and painting, prefixed to the Lives of the most excellent painters, sculptors and architects]''. [[Gerard Baldwin Brown|G. Baldwin Brown]] Ed. Louisa S. Maclehose Trans. London: Dent.</ref> The book was partly rewritten and enlarged in 1568, with the addition of woodcut portraits of artists (some conjectural).

The work has a consistent and notorious bias in favour of [[Florence, Italy|Florentines]], and tends to attribute to them all the developments in Renaissance art – for example, the invention of [[engraving]]. Venetian art in particular (along with arts from other parts of Europe), is systematically ignored in the first edition. Between the first and second editions, Vasari visited Venice and while the second edition gave more attention to Venetian art (finally including [[Titian]]), it did so without achieving a neutral point of view.

Vasari's biographies are interspersed with amusing gossip. Many of his anecdotes have the ring of truth, while others are inventions or generic fictions, such as the tale of young [[Giotto]] painting a fly on the surface of a painting by [[Cimabue]] that the older master repeatedly tried to brush away, a genre tale that echoes anecdotes told of the Greek painter Apelles. With a few exceptions, however, Vasari's aesthetic judgement was acute and unbiased.{{citation needed|date=April 2012}} He did not research archives for exact dates, as modern art historians do, and naturally his biographies are most dependable for the painters of his own generation and those of the immediate past. Modern criticism – with new materials opened up by research – has corrected many of his traditional dates and attributions.

Vasari includes a sketch of his own biography at the end of the ''Lives'', and adds further details about himself and his family in his lives of Lazzaro Vasari and [[Francesco de' Rossi (Il Salviati)|Francesco Salviati]].

According to the historian Richard Goldthwaite,<ref>Richard Goldthwaite, ''The Economy of Renaissance Florence'', 2009, pp. 390.</ref> Vasari was one of the earliest authors to use the term "competition" (or "concorrenza" in Italian) in its economic sense. He used it repeatedly, and stressed the concept in his introduction to the life of [[Pietro Perugino]], in explaining the reasons for Florentine artistic preeminence. In Vasari's view, Florentine artists excelled because they were hungry, and they were hungry because their fierce competition amongst themselves for commissions kept them so.  Competition, he said, is "one of the nourishments that maintain them."

==Social standing==

Vasari enjoyed high repute during his lifetime and amassed a considerable fortune. In 1547, he built himself a fine house in [[Arezzo]] (now a museum honouring him), and decorated its walls and vaults with paintings. He was elected to the municipal council or priori of his native town, and finally rose to the supreme office of [[gonfalonier]]e.

In 1563, he helped found the Florence [[Accademia delle Arti del Disegno|''Accademia e Compagnia delle Arti del Disegno'']], with the Grand Duke and Michelangelo as ''capi'' of the institution and 36 artists chosen as members.

==Public collections==
* [[Rijksmuseum Amsterdam]]<ref>[https://www.rijksmuseum.nl/en/search?p=1&ps=12&maker=Giorgio%20Vasari&ii=0 Collection Rijksmuseum]</ref>

==See also==
* Reading Vasari, eds. Anne B. Barriault, Andrew T. Ladis, Norman E. Land, and Jeryldene M. Wood (London: Philip Wilson, 2005)
* The Ashgate Research Companion to Giorgio Vasari, ed. David J. Cast (Surrey: Ashgate, 2014)

==Gallery==
<gallery mode=packed heights= 154x caption="Giorgio Vasari's works. Paintings">
File:Alessandro de Medici Ruestung.jpg|Alessandro de Medici resting

File:Douai chartreuse vasari pieta.jpg|Pieta
File:GIORGIO VASARI, JOANNES STRADANUS THE BIRD CATCHERS.jpg|Bird catchers
File:Vasari, Giorgiodel Sarto, Andrea - Holy Family - Google Art Project.jpg| Holy Family, with Andrea Sarto
File:Giorgio vasari, ultima cena, da ss. annunziata a figline, 1567-69, 04.JPG| Last Supper by Giorgio Vasari
File:Giorgio Vasari - Entombment - WGA24277.jpg|Entombment
 File:Giorgio Vasari - Temptations of St Jerome - WGA24282.jpg| Giorgio Vasari - ''Temptations of St Jerome'' 
File:Giorgio Vasari - St Luke Painting the Virgin - WGA24311.jpg|St Luke Painting the Virgin
File:Giorgio Vasari - Annunciation - WGA24286.jpg|Annunciation
File:Giorgio Vasari - Justice - WGA24280.jpg|Justice
File:Giorgio Vasari - The Prophet Elisha - WGA24289.jpg|The Prophet Elisha
</gallery>
<gallery mode=packed heights= 103x caption="Giorgio Vasari's frescos and decorations.  ">
File:Firenze-interno duomo.jpg|Interior of the dome of [[Florence Cathedral]] 
File:Giorgio Vasari - Cosimo studies the taking of Siena - Google Art Project.jpg|Cosimo studies the taking of Siena 
File:Giorgio Vasari - Apotheosis of Cosimo I - Google Art Project.jpg|Apotheosis of Cosimo I 
File:Giorgio Vasari - Defeat of the Venetians in Casentino - Google Art Project.jpg|Defeat of the Venetians in Casentino
 
</gallery>

<gallery mode=packed heights= 154x caption="Giorgio Vasari's works. Architecture">
File:Florenz Uffizien.jpg|The [[Uffizi]] colonnade and loggia
File:Loge de Vasali a Arezzo.JPG|The Loggia of Vasari in [[Arezzo]]
File:005San-Pietro-in-Montorio-Rome.jpg|Pietro in Montorio, Rome
File:9903 - Firenze - Santa Croce - Tomba di Michelangelo - Foto Giovanni Dall'Orto, 28-Oct-2007.jpg|Tomb of Michelangelo
File:Sala dei cento giorni - Giorgio Vasari - 1547 - Palazzo della Cancelleria 1.jpg|[[Sala dei Cento Giorni]] - Giorgio Vasari - 1547 - [[Palazzo della Cancelleria]]
 
 File:Villa Giulia - Court - Vasari - Vignola.jpg|[[Villa Giulia]] - Court - Vasari - Vignola 
</gallery>

==References and sources==
[[File:The Mutiliation of Uranus by Saturn.jpg|thumb|300px|''The [[Castration]] of [[Uranus]]'': fresco by Vasari & [[Cristofano Gherardi]] (c. 1560, Sala di Cosimo I, Palazzo Vecchio, Florence).]]
;References
{{Reflist}}
;Sources
*''The Lives of the Artists'' Oxford University Press, 1998. ISBN 0-19-283410-X
*''Lives of the Painters, Sculptors and Architects, Volumes I and II''. Everyman's Library, 1996. ISBN 0-679-45101-3
*''Vasari on Technique''. Dover Publications, 1980. ISBN 0-486-20717-X
*''Life of Michelangelo''. Alba House, 2003. ISBN 0-8189-0935-8
*{{EB1911|wstitle = Vasari, Giorgio|volume=27}}
*{{cite CE1913|wstitle = Giorgio Vasari|volume=15}}

==External links==
{{Wikiquote}}
{{Commons category|position=left}}
* {{Gutenberg author | id=Vasari,+Giorgio | name=Giorgio Vasari}}
* {{Internet Archive author |sname=Giorgio Vasari}}
* {{Librivox author |id=408}}
*[http://www.articlemyriad.com/36.htm  Biography of Vasari and analysis for four major works]
* {{Books and Writers |id=gvasari |name=Giorgio Vasari}}
*[http://artpaintingartist.org/giorgio-vasari-the-first-art-historian-1511-1574/ Giorgio Vasari] – The First Art-Historian
Copies of Vasari's ''Lives of the Artists'' online:
*[http://www.efn.org/~acd/vite/VasariLives.html “Giorgio Vasari's Lives of the Artists.”] Site created by Adrienne DeAngelis. Now largely completed in the posting of the Lives, intended to be re-translated to become the unabridged English version.
*[http://bepi1949.altervista.org/vasari/vasari00.htm “Le Vite."] 1550 Unabridged, original Italian.
*[https://archive.org/details/storiesoftheital007995mbp “Stories Of The Italian Artists From Vasari.”] Translated by E L Seeley, 1908. Abridged, in English.
* [http://biblio.cribecu.sns.it/vasari/consultazione/Vasari/indice.html Le Vite – Edizioni Giuntina e Torrentiniana]
* [http://www.storiarte.altervista.org/vasarielenco.htm Gli artisti principali citati dal Vasari nelle "Vite" (elenco)]
*[http://easyweb.easynet.co.uk/giorgio.vasari/vaspref.htm Excerpts from the ''Vite'' combined with photos of works mentioned by Vasari.]'''

{{Authority control}}
{{Medici|state=collapsed}}

{{DEFAULTSORT:Vasari, Giorgio}}
[[Category:Italian Renaissance painters]]
[[Category:Italian Renaissance architects]]
[[Category:Mannerist painters]]
[[Category:1511 births]]
[[Category:1574 deaths]]
[[Category:Artist authors]]
[[Category:Italian biographers]]
[[Category:Italian art historians]]
[[Category:Italian art critics]]
[[Category:Italian male writers]]
[[Category:Male biographers]]
[[Category:Tuscan painters]]
[[Category:People from Arezzo]]
[[Category:Art technological sources]]
[[Category:Uffizi]]
[[Category:16th-century Italian architects]]
[[Category:16th-century Italian painters]]
[[Category:Italian male painters]]
[[Category:16th-century Italian writers]]