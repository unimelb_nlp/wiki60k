{{Infobox journal
| title = The Journal of Imperial and Commonwealth History
| cover = 
| discipline = [[History]]
| abbreviation = J. Imp. Commw. Hist.
| editor = Stephen Howe, Philip Murphy
| publisher = [[Routledge]]
| country =
| frequency = 6/year
| history = 1972-present
| impact = 
| impact-year = 
| openaccess =
| license =
| website = http://www.tandfonline.com/action/journalInformation?journalCode=fich20
| link1 = http://www.tandfonline.com/toc/fich20/current
| link1-name = Online access
| link2 = http://www.tandfonline.com/loi/fich20
| link2-name = Online archive
| ISSN = 0308-6534
| eISSN = 1743-9329
| OCLC = 1089553
| LCCN = 81649122
}}
'''''The Journal of Imperial and Commonwealth History''''' is a [[peer-reviewed]] [[academic journal]] covering the history of the [[British Empire]] and [[British Commonwealth|Commonwealth]] and comparative [[European colonialism|European colonial]] experiences.<ref>{{cite web |url=http://www.history.ac.uk/history-online/journal/journal-imperial-and-commonwealth-history |title=The Journal of Imperial and Commonwealth History |publisher=The Institute of Historical Research |work=History On-line |accessdate=2014-05-25}}</ref> It was established in 1972 and is issued five times per year by [[Routledge]]. The [[editors-in-chief]] are Stephen Howe ([[University of Bristol]]) and Philip Murphy ([[Institute of Commonwealth Studies]], [[University of London]]).<ref>{{cite web |url=http://www.tandfonline.com/action/journalInformation?show=editorialBoard&journalCode=fich20 |title=Editorial board |publisher=Taylor & Francis |work=The Journal of Imperial and Commonwealth History |accessdate=2014-05-25}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[America: History and Life]]
* [[Arts & Humanities Citation Index]]
* [[British Humanities Index]]
* [[CSA Worldwide Political Science Abstracts]]
* [[Current Contents]]/Arts & Humanities
* [[Humanities International Index]]
* [[International Bibliography of the Social Sciences]]
* [[Scopus]]
* [[Sociological Abstracts]]
}}

== References ==
{{Reflist}}

== External links ==
* {{Official website|1=http://www.tandfonline.com/action/journalInformation?journalCode=fich20}}

{{DEFAULTSORT:Journal Of Imperial And Commonwealth History, The}}
[[Category:English-language journals]]
[[Category:History journals]]
[[Category:Publications established in 1972]]
[[Category:Taylor & Francis academic journals]]


{{history-journal-stub}}