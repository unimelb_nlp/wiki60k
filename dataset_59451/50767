{{Primary sources|date=September 2010}}
{{Infobox journal
| title = British Columbia Medical Journal
| cover = [[File:BCMJ 52Vol6 cover.homepage.gif]]
| editor = D.R. Richardson
| discipline = [[Medicine]]
| formernames = Vancouver Medical Association Bulletin, British Columbia Medical Journal
| abbreviation = B. C. Med. J.
| publisher = [[British Columbia Medical Association|BC Medical Association]]
| country = Canada
| frequency = 10/year
| history = 1924–present
| openaccess =
| license =
| impact =
| impact-year =
| website = http://www.bcmj.org
| link1 = http://www.bcmj.org/archive/2010
| link1-name = Online archive
| link2 =
| link2-name =
| JSTOR =
| OCLC = 01586013
| LCCN = cn 79310920
| CODEN = BCMJAC
| ISSN = 0007-0556
| eISSN =
}}
The '''''British Columbia Medical Journal''''' is a [[Peer review|peer-reviewed]] general [[medical journal]] covering scientific research, review articles, and updates on contemporary clinical practices written by [[British Columbia]]n physicians or focused on topics likely to be of interest to them, such as columns from the BC Centre for Disease Control and the [[Insurance Corporation of British Columbia]]. Although it is published by the [[British Columbia Medical Association]] (BCMA), it maintains distance from the BCMA in order to encourage open debate. It hosts an online database of all issues from 2000. The current [[editor-in-chief]] is David R. Richardson.

==Frequency of publication==
The journal publishes 10 issues per year, with combined issues in January/February and July/August.

==History==
The journal was originally known as the ''Vancouver Medical Association Bulletin''<ref>[http://internatlibs.mcgill.ca/bibliography/titles-q-z.htm McGill University Canadian Health Sciences Periodicals (Q-Z)]</ref> and was published by the Vancouver Medical Association (VMA) for 34 years, from October 1924 until January 1959.

In its early years, the journal relied heavily on advertiser supporting. In 1957, as this income had begun to decline, the VMA and the BCMA entered into an agreement to restructure the publication, including its retitling, with BCMA paying to sustain the financially troubled journal. The first issue under the ''British Columbia Medical Journal'' title was released in January 1959. In January 1963, in response to continued decreasing advertiser support, the BCMA took over the journal completely.

==Cover art==
Jerry Wong has been the cover artist for the ''British Columbia Medical Journal'' since 1982.<ref>[http://www.bcmj.org/archive/2000 BCMJ cover archive] {{webarchive |url=https://web.archive.org/web/20101026042743/http://www.bcmj.org/archive/2000 |date=October 26, 2010 }}</ref>

==Open access==
The ''British Columbia Medical Journal'' is an [[open access journal]]. All articles from 2000 onwards are archived on the journal's web site.<ref>[http://www.bcmj.org BCMJ home page]</ref>

==J.H. MacDermot Prize==
The ''British Columbia Medical Journal'' welcomes article submissions from student authors, and each year awards a prize of $1000 for the best article written by a medical student in the province of British Columbia. The prize honors [[John Henry MacDermot]] (1883–1969), who became the editor of the ''Vancouver Medical Bulletin'' at its formation in 1924, remaining at the helm until 1967, when he retired.

==References==
{{Reflist}}

== External links ==
* {{Official website|http://www.bcmj.org}}

[[Category:Open access journals]]
[[Category:Health in Canada]]
[[Category:Health in British Columbia]]
[[Category:English-language journals]]
[[Category:Publications established in 1924]]
[[Category:General medical journals]]