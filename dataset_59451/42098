<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name= XC
 | image=File:Wills Wing XC-185 hang glider.JPG
 | caption=Wills Wing XC at the [[Canada Aviation and Space Museum]]
}}{{Infobox Aircraft Type
 | type=[[Hang glider]]
 | national origin=[[United States]]
 | manufacturer=[[Wills Wing]]
 | designer=
 | first flight=
 | introduced=1977
 | retired=
 | status=Production completed
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= 1977-1980
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= <!-- [[US$]] [[Pound sterling|£]] [[Euro|€]] (date) -->
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Wills Wing XC''' (''Cross Country'') is an [[United States|American]] [[high-wing]], single-place, [[hang glider]] that was designed and produced by [[Wills Wing]] of [[Santa Ana, California]]. Now out of production, when it was available the aircraft was supplied complete and ready-to-fly.<ref name="Mus">{{cite web|url = http://casmuseum.techno-science.ca/en/collection-research/artifact-wills-wing-xc-185.php|title = Wills Wing XC-185|accessdate = 7 March 2016|author = [[Canada Aviation and Space Museum]]|year = 2016}}</ref>

The XC was Wills Wing's third hang glider model produced.<ref name="Mus"/>

==Design and development==
The aircraft is made from [[aluminum]] tubing, with the single-surface wing covered in [[Dacron]] sailcloth and  [[Flying wires|cable braced]] from a single [[kingpost]].<ref name="Mus"/>

The XC models are each named for their rough wing area in square feet.<ref name="Mus"/>
<!-- ==Operational history== -->

==Variants==
;XC-132
:Very small-sized model for lighter pilots. Pilot hook-in weight range is {{convert|120|to|220|lb|kg|0|abbr=on}}.<ref name="willswingsupport">{{cite web|url=https://www.willswing.com/hang-glider-placard-specifications/|title=Hang Glider Placard Specifications|author=Wills Wing Support|work=Wills Wing|accessdate=8 March 2016}}</ref>
;XC-142
:Small-sized model for lighter pilots. Pilot hook-in weight range is {{convert|140|to|240|lb|kg|0|abbr=on}}.<ref name="willswingsupport"/>
;XC-155
:Mid-sized model for medium-weight pilots. Pilot hook-in weight range is {{convert|170|to|280|lb|kg|0|abbr=on}}.<ref name="willswingsupport"/>
;XC-185
:Large-sized model for heavier pilots. The wing has a span of {{convert|10.9|m|ft|1|abbr=on}}. The glider empty weight is {{convert|25|kg|lb|0|abbr=on}}.<ref name="Mus"/>

==Aircraft on display==
*[[Canada Aviation and Space Museum]] - XC-185

==Specifications (XC-185) ==
{{Aircraft specs
|ref=[[Canada Aviation and Space Museum]]<ref name="Mus"/>
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=
|length m=3.4
|length ft=
|length in=
|length note=
|span m=10.9
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=25
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|more general=
<!--
        Performance
-->
|perfhide=Y

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|g limits=
|roll rate=
|glide ratio=<!--  at {{convert|XX|km/h|mph|0|abbr=on}} -->
|sink rate ms=
|sink rate ftmin=
|sink rate note=<!--  at {{convert|XX|km/h|mph|0|abbr=on}} -->
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|more performance=
}}

==References==
{{reflist}}

<!-- ==External links== -->
{{Wills Wing aircraft}}
[[Category:Wills Wing aircraft|XC]]
[[Category:Hang gliders]]