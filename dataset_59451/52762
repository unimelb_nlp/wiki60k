{{Infobox journal
| title = Revue des Études Arméniennes
| cover = [[File:REA vol 28 2007.jpg|140px]]
| editor = Aram Mardirossian, Nina Garsoian, [[Jean-Pierre Mahé]]
| discipline = [[Armenian studies]], [[Byzantine studies]], [[Oriental studies]], [[patristics]] 
| language = [[French language|French]] (English abstracts), [[English language|English]], [[German language|German]]
| abbreviation =
| publisher = [[Sorbonne University]], Section des Sciences Historiques et Philologiques
| country = France
| frequency = Annually
| history = 1920-present
| website = http://poj.peeters-leuven.be/content.php?url=journal.php&journal_code=REA
| OCLC  = 1605433
| ISSN  = 0080-2549
| eISSN = 1783-1741 
}}
'''''Revue des Études Arméniennes''''' is a [[peer-review]]ed [[academic journal]] that publishes articles relating to [[Classics|Classical]] and [[medieval]] [[Armenian history]], [[art history]], [[philology]], [[linguistics]], and [[literature]].<ref name="Peeters">{{fr icon}} [http://poj.peeters-leuven.be/content.php?url=journal.php&journal_code=REA Revue des Études Arméniennes]. Peeters Online Journals. Accessed September 12, 2014.</ref> The ''Revue'' was established in 1920 at the initiative of French scholars Frédéric Macler and [[Antoine Meillet]].<ref name="SAE">{{hy icon}} Mahé, Jean-Pierre. ''«Ռևյու դեզ էթյուդ Արմենիեն»'' [Revue des Études Arméniennes]. [[Armenian Soviet Encyclopedia]]. Yerevan: Armenian Academy of Sciences, 1983, vol. 9, p. 649.</ref> Meillet himself wrote many of the articles during the formative years of the journal (1920-1933), which typically covered Armenian history, [[grammar]], and [[Folklore|folk tale]]s. The ''Revue'' was not published from 1934 to 1963.<ref name="SAE"/>

In 1964, thanks to the efforts of the Paris-based Armenian scholar Haïg Berbérian (1887-1978), the journal was revived. Berbérian was able to secure the financial backing of the [[Calouste Gulbenkian Foundation]] for the journal's publication, and the first volume of the "Nouvelle série" appeared under his editorship in 1964.<ref>Kouymjian, Dickran. "Preface" in ''In Memoriam: Haïg Berbérian'', ed. Dickran Kouymjian. Lisbon: Calouste Gulbenkian Foundation, 1986, pp. xi-xii.</ref> Unlike during the first series, however, the publication of articles on the modern period of Armenian history was abandoned, and the journal has since limited its scope to the [[early modern period]] (that is, until roughly the eighteenth century).<ref name="Peeters"/>

Up until 1933, articles were published in French, but when publication resumed articles were also published in English and German.<ref name="SAE"/><ref>Manuscripts submitted in other languages, such as Spanish, are also now accepted.</ref> The journal uses the Hübschmann-Meillet-Benveniste system in the transcription of Armenian words into Latin characters. In addition to scholarly articles, it also publishes [[book review]]s.

The journal's former editors were Jacques Benveniste (nominally, 1964-1975, as Berbérian was responsible for much of the editing during this time), [[Georges Dumézil]] (1975-1980), and [[Sirarpie Der-Nersessian]] (1981-1989).<ref name="SAE"/> Its current editor is Aram Mardirossian. 

== See also ==
* [[Armenian studies]] 
* ''[[Bazmavep]]''
* ''[[Haigazian Armenological Review]]'' 
* ''[[Handes Amsorya]]'' 
* ''[[Patma-Banasirakan Handes]]''

== Notes ==
{{reflist|2}}

== External links ==
* {{Official website|1=http://poj.peeters-leuven.be/content.php?url=journal.php&journal_code=REA}}

{{DEFAULTSORT:Revue des Etudes Armeniennes}}
[[Category:Armenian studies journals]]
[[Category:History journals]]
[[Category:Publications established in 1920]]
[[Category:Multilingual journals]]