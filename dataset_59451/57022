{{Infobox journal
| title = Food and Chemical Toxicology
| cover =
| discipline = [[Toxicology]]
| abbreviation = Food Chem. Toxicol.
| formernames = Food and Cosmetics Toxicology
| editor = [[José L. Domingo]]
| publisher = [[Elsevier]]
| country =
| frequency = Monthly
| history = 1963–present
| openaccess =
| license = 
| impact = 2.895
| impact-year = 2014
| website = http://www.journals.elsevier.com/food-and-chemical-toxicology/
| link1 = http://www.sciencedirect.com/science/journal/02786915
| link1-name = Online archive
| link2 = 
| link2-name =
| JSTOR = 
| OCLC = 67270973
| LCCN = 82643214
| CODEN = FCTOD7
| ISSN = 0278-6915
| eISSN = 
}}
'''''Food and Chemical Toxicology''''' is a [[peer-reviewed]] [[scientific journal]] covering aspects of [[food safety]], chemical safety, and other aspects of consumer [[product safety]]. It is published by [[Elsevier]] and was established in 1963. The [[editor-in-chief]] is [[José L. Domingo]].<ref>{{cite web|title=Food and Chemical Toxicology (website) main|url=http://www.journals.elsevier.com/food-and-chemical-toxicology|accessdate=12 June 2016}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Analytical Abstracts]], [[BIOSIS Previews]], [[CAB International]], [[Chemical Abstracts Service]], [[Current Contents]]/Agriculture, Biology & Environmental Sciences, Current Contents/Life Sciences, [[Elsevier BIOBASE]], [[EMBASE]], [[MEDLINE]]/[[PubMed]], [[Science Citation Index]], and [[Scopus]]. According to the ''[[Journal Citation Reports]]'', it has a 2014 [[impact factor]] of 2.895, ranking it 30th out of 87 journals in the category "Toxicology"<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Toxicology |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition= Sciences |work=Web of Science}}</ref> and 14th out of 123 journals in the category "Food Science & Technology".<ref name=WoS1>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Food Science & Technology |title=2014 Journal Citation Reports |publisher=Thomson Reuters |edition= Sciences |accessdate= |work=Web of Science}}</ref>

== References ==
{{reflist|30em}}

== External links ==
* {{Official website|http://www.journals.elsevier.com/food-and-chemical-toxicology/}}

[[Category:Publications established in 1963]]
[[Category:Toxicology journals]]
[[Category:English-language journals]]
[[Category:Monthly journals]]
[[Category:Elsevier academic journals]]