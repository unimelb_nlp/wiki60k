{{Use dmy dates|date=November 2016}}
{{Use Australian English|date=November 2016}}
{{Userspace draft|source=ArticleWizard|date=September 2015}}

'''George Dutton Green''' (1 May 1850 – 27 April 1911) was a land agent, auctioneer and politician in the colony of South Australia. He was generally referred to as "Dutton Green" and adopted it as his family name, occasionally hyphenated.<ref>{{cite news |url=http://nla.gov.au/nla.news-article45752155 |title=Family Notices. |newspaper=[[The Advertiser (Adelaide)|Advertiser and Register (Adelaide, SA : 1931)]] |location=Adelaide, SA |date=4 March 1931 |accessdate=5 September 2015 |page=9 |publisher=National Library of Australia}}</ref>

He was born in North Adelaide, the eldest son of George Green who founded the real-estate firm of Green & Co. on King William Street in 1848. He was educated at [[St Peter's College, Adelaide|St. Peter's College]], and at [[Brighton|Brighton, England]]. He joined his father's firm in 1867, and took it over in 1871, with Colonel J. Chapman Lovely (c. 1837 – 11 November 1915) as partner, when his father retired. In 1879, supported by the [[Rymill brothers]] and several other businessmen, he established a new Stock Exchange (one of two competing schemes), on land known as "King's timber yard" in [[Pirie Street, Adelaide|Pirie Street]]; they hired [[Edmund W. Wright]] to design the new building.<ref>{{cite news |url=http://nla.gov.au/nla.news-article43091450 |title=New Exchange in Adelaide |newspaper=[[South Australian Register]] |location=Adelaide |date=24 July 1879 |accessdate=27 October 2015 |page=5 |publisher=National Library of Australia}}</ref>

Green was a director of many notable companies: the South Australian Gas Company, the South Australian Brewing Company, John Hill & Co., the Metropolitan Brick Company, Glenelg Railwvay Company. He was a vice-president of the Park Lands League, a trustee of the Wyatt Benevolent Fund, and patron of racing clubs and athletic bodies.<ref>{{cite news |url=http://nla.gov.au/nla.news-article5269951 |title=Personal |newspaper=[[The Advertiser (Adelaide)|The Advertiser (Adelaide, SA : 1889 - 1931)]] |location=Adelaide, SA |date=28 April 1911 |accessdate=5 September 2015 |page=6 |publisher=National Library of Australia}}</ref>

He represented the electorate of [[Electoral district of East Adelaide]] in the [[South Australian House of Assembly]], a colleague of [[John Cox Bray]], from April 1884 to May 1886, when he resigned.

==Family==
He married Constance Evelyn Charnock (3 January 1854 – 3 March 1931) on 9 February 1876, lived at "Holmfield", [[South Terrace, Adelaide]]; they had three daughters.
*Evelyn Dutton Green (15 December 1876 – ) married Captain Gordon Cavenagh-Mainwaring on 2 November 1904.
*[[Olive Dutton Green]] (19 February 1878 – July 1930) was a noted painter, established Olive Dutton Green landscape prize.
*Myrtle Dutton Green ( – ) married noted ornithologist Dr. Alexander Matheson Morgan MB (11 February 1867 – 18 October 1934), son of [[William Morgan (Australian politician)|Sir William Morgan]], on 11 October 1905. 
:*[[William Matheson Morgan]] (9 November 1906 – 2 February 1972) mining engineer<ref>E. D. J. Stewart, 'Morgan, William Matheson (1906–1972)', Australian Dictionary of Biography, National Centre of Biography, Australian National University, http://adb.anu.edu.au/biography/morgan-william-matheson-11166/text19893, published first in hardcopy 2000, accessed online 5 September 2015.</ref>

== References ==
{{Reflist}}

{{DEFAULTSORT:Green, George Dutton}}
[[Category:Members of the South Australian House of Assembly]]
[[Category:Australian real estate agents]]
[[Category:Australian auctioneers]]
[[Category:Australian businesspeople]]
[[Category:1850 births]]
[[Category:1900 deaths]]
[[Category:19th-century Australian politicians]]