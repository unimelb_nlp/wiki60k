{{Good article}}
{{Infobox military conflict
|conflict=Action of 10 April 1795
|partof=the [[French Revolutionary Wars]]
| image=[[File:HMS Astraea and Gloire.jpg|300px]]
| caption=''Capture of La Gloire April 10th 1795'', [[Thomas Whitcombe]], 1816, [[National Maritime Museum]]
|date=10&ndash;11 April 1795
|place=[[Western Approaches]], [[Atlantic Ocean]]
|result= British victory
|combatant1={{flagcountry|Kingdom of Great Britain}} 
|combatant2={{flag|France}} 
|commander1=Rear-Admiral [[John Colpoys]] 
|commander2=Captain Beens
|strength1=5 [[ships of the line]] and 2 [[frigates]]. Only frigate [[HMS Astraea (1781)|HMS ''Astraea'']] heavily engaged
|strength2=[[frigates]] [[French frigate Gloire (1778)|''Gloire'']], [[French frigate Gentille (1778)|''Gentille'']] and [[French frigate Aglaé (1788)|''Fraternité'']]
|casualties1=1 killed, 7 wounded 
|casualties2=40 killed and wounded. ''Gloire'' and ''Gentille'' captured.
}}
{{coord|49|30|N|10|46|W|display=title|type:event}}

The '''Action of 10 April 1795''' was a minor naval engagement during the [[French Revolutionary Wars]] in which a squadron of [[French Navy]] [[frigates]] was intercepted by a British battle squadron under Rear-Admiral [[John Colpoys]] which formed part of the [[blockade]] of the French naval base of [[Brest, France|Brest]] in [[Brittany]]. The French squadron split up in the face of superior British numbers, the three vessels seeking to divide and outrun the British pursuit. One frigate, [[French frigate Gloire (1778)|''Gloire'']] was followed by the British frigate [[HMS Astraea (1781)|HMS ''Astraea'']] and was ultimately brought to battle in a closely fought engagement. Although the ships were roughly equal in size, the British ship was easily able to defeat the French in an engagement lasting just under an hour.

The other French ships were pursued by British [[ships of the line]] and the chase lasted much longer, into the morning of 11 April when [[HMS Hannibal (1786)|HMS ''Hannibal'']] caught the frigate [[French frigate Gentille (1778)|''Gentille'']]. ''Hannibal'' was far larger than its opponent and the French captain surrendered immediately rather than fight a futile engagement. The third French frigate, [[French frigate Aglaé (1788)|''Fraternité'']] successfully escaped. After refitting in [[Portsmouth]], Colpoys' ships returned to their station off Brest, the blockade remaining in place for the remainder of the year.

==Background==
Great Britain and France had been at war for more than two years by April 1795, and British dominance at sea was well established, with the [[Royal Navy]] maintaining substantial [[blockade]] fleets off all of the principal French naval ports.<ref name="RG140"/> The biggest port on the French Atlantic coast was at [[Brest, France|Brest]] in [[Brittany]], from which French raiders could attack British shipping in the [[English Channel]] and Western Atlantic. The most efficient commerce raiders were frigates, light and fast warships that could strike rapidly and with devastating effect if left unopposed. One of the major roles of the British blockade squadrons was the detection and elimination of French frigates as they emerged from their bases.<ref name="RG140">Gardiner, p. 140</ref>

In April 1795, the inshore squadron of the British blockade at Brest was commanded by Rear-Admiral [[John Colpoys]], who had at his command five [[ships of the line]]: [[HMS London (1766)|HMS ''London'']], [[HMS Valiant (1759)|HMS ''Valiant'']], [[HMS Colossus (1787)|HMS ''Colossus'']], [[HMS Hannibal (1786)|HMS ''Hannibal'']] and [[HMS Robust (1764)|HMS ''Robust'']] and [[frigates]] [[HMS Astraea (1781)|HMS ''Astraea'']] and [[HMS Unicorn (1782)|HMS ''Thalia'']]. Colpoys' ships had formed an effective blockade: on 29 March they had taken the French [[corvette]] ''Jean Bart'' and the following day recaptured a lost British merchant ship. At 10:00 on 10 April, the British squadron was cruising off the approaches to Brest when three ships were spotted to the west.<ref name="LG1"/>

Colpoys immediately ordered his squadron to give chase and at 12:00 the strange ships were identified as a squadron of French frigates. The French ships were the 36-gun [[French frigate Gloire (1778)|''Gloire'']], [[French frigate Gentille (1778)|''Gentille'']] and [[French frigate Aglaé (1788)|''Fraternité'']], led by Captain Beens of ''Gloire'' and on a three-month raiding cruise from Brest in the [[Bay of Biscay]] that had so-far been uneventful: the only prize taken had been a small Spanish merchant [[brig]].<ref name="LG1"/> Beens quickly discovered the danger his squadron was in, and gave orders for them to sail westwards away from the British squadron. However, the wind favoured Colpoys and his vastly superior squadron rapidly gained on the French frigates. The first British ship to come within range was the 74-gun HMS ''Colossus'' under Captain [[John Monkton]], which managed to exchange distant gunfire with the rearmost French ship before the gap widened once more.<ref name="WJ284"/>

==Battle==
Seeing that his ships were in danger of being caught by the much larger British ships of the line, Captain Beens gave orders for the squadron to separate. ''Gentille'' and ''Fraternité'' splitting from ''Gloire'' to the west with the ships of the line HMS ''Hannibal'' and HMS ''Robust'' in close pursuit while ''Gloire'' swung northwest, eluding most of the British squadron except for the 32-gun frigate HMS ''Astraea'' under Captain [[Lord Henry Paulet]], which managed to stay in contact throughout the afternoon.<ref name="LG1">{{London Gazette|issue=13770|startpage=339|date=14 April 1795|accessdate=25 March 2012}}</ref>

At 18:00, with the rest of the pursuit far behind, Paulet succeeded in bringing ''Gloire'' within range of the cannon on his ship's [[quarterdeck]]. Opening fire with these guns brought a response from Beens' sternchaser guns, the frigates exchanging cannon shot for four and a half hours as ''Astraea'' slowly caught up with its elusive opponent.<ref name="WJ284">James, p. 284</ref> At 22:30, Paulet was finally close enough to lay ''Astraea'' alongside ''Gloire'' and the two frigates exchanged fire at close range for the next 58 minutes, Paulet concentrating his gunnery on the hull of the French ship while Beens' ordered his men to disable the British ship's rigging and masts. ''Gloire'' was a substantially larger ship than ''Astraea'', both in weight of shot and gross tonnage, and the battle was fiercely contested: Beens suffered a head injury and all three of ''Astraea'''s topmasts taking serious damage, so much so that the main topmast collapsed in the aftermath of the action.<ref name="WJ285">James, p. 285</ref> However at 23:28, with two British ships of the line visible in the distance, Beens surrendered his ship to Paulet.<ref name="WLC491">Clowes, p. 491</ref>

Both ships had suffered damage, with the injuries to ''Astraea'''s masts requiring urgent repairs while ''Gloire'' had also suffered damage to its rigging and sails. The French ship had also taken heavy casualties, with 40 men killed or wounded, including the captain. In contrast, ''Astraea'' had not lost a single man, although one of the eight wounded subsequently died. Paulet effected repairs to both ships and gave temporary command of ''Gloire'' to Lieutenant [[John Talbot (Royal Navy officer)|John Talbot]], who was subsequently promoted. He then brought both ships to the [[Portsmouth]], where Colpoys was reconstituting his scattered squadron.<ref name="WJ285"/>

==Aftermath==
It was while sailing off the [[Isle of Wight]] that Colpoys learned from Captain [[Edward Thornbrough]] of ''Robust'' that ''Hannibal'' had succeeded in catching the French frigate ''Gentille'' early on the morning of 11 April.<ref name="WJ285"/> The French captain surrendering without a fight before the overwhelming British force that he faced, shocked that his frigate had been caught by a ship of the line in open waters. In response, ''Hannibal''<nowiki>'</nowiki>s captain, [[John Markham (Royal Navy officer)|John Markham]], proudly claimed that "Hannibal sails like a witch". ''Hannibal'' subsequently joined ''Robust'' in the chase of ''Fraternité'', succeeding in firing several shot at the French ship before falling behind in a period of calm weather. After a chase of several days, ''Fraternité''<nowiki>'</nowiki>s captain lightened his ship by throwing guns and stores overboard and ultimately escaped pursuit, later rejoining the Brest fleet and participating in a number of subsequent campaigns.<ref name="CRM111">Markham, p. 111&ndash;112</ref> Both ''Gloire'' and ''Gentille'' were purchased for the Royal Navy, the entirety of Colpoy's squadron sharing in the [[prize money]] by prior arrangement.<ref name="LG2">{{London Gazette|issue=13851|startpage=974|date=19 September 1795|accessdate=25 March 2012}}</ref> Neither ship was in particularly good condition however and neither had long service in the British fleet. Colpoys returned to the inshore blockade of Brest with his squadron following a brief refit at Portsmouth, remaining off the port for the remainder of the year.<ref name="ODNB">[http://www.oxforddnb.com/view/article/5985 Colpoys, Sir John], ''[[Oxford Dictionary of National Biography]]'', [[John Knox Laughton|J. K. Laughton]], (subscription required), Retrieved 26 March 2012</ref>

== Notes ==
{{Reflist|2}}

== References ==
*{{cite book
 | last = Clowes
 | first = William Laird
 | authorlink = William Laird Clowes
 | year = 1997
 | origyear= 1900
 | chapter = 
 | title = The Royal Navy, A History from the Earliest Times to 1900, Volume IV
 | publisher = Chatham Publishing
 | location = London
 | isbn = 1-86176-013-2
}}
*{{cite book
 | author = Gardiner, Robert (editor)
 | authorlink = 
 | year = 2001
 | origyear= 1996
 | chapter = 
 | title = Fleet Battle and Blockade
 | publisher = Caxton Editions
 | location = 
 | isbn =9781840673630
}}
*{{cite book
 | last = James
 | first = William
 | authorlink = William James (naval historian)
 | year = 2002
 | origyear= 1827
 | chapter = 
 | title = The Naval History of Great Britain, Volume 1, 1793&ndash;1796
 | publisher = Conway Maritime Press
 | location = London
 | isbn = 0-85177-905-0
}}
*{{cite book
 | last = Markham
 | first = Sir Clements Robert
 | authorlink = Clements Robert Markham
 | year = 1883
 | origyear = 
 | chapter = 
 | title = A Naval Career during the Old War
 | url = https://books.google.com/books?id=MK9AAAAAYAAJ&pg=PA111&dq=%22Hannibal+Gentille%22&hl=en&sa=X&ei=pnmeT6HEO6ft0gHh4_yEDw&ved=0CDoQ6AEwAQ#v=onepage&q=%22Hannibal%20Gentille%22&f=false
 | publisher = Sampson Row, Marston, Searle & Rivington
 | location = London
}}

[[Category:Naval battles of the French Revolutionary Wars]]
[[Category:Conflicts in 1795]]
[[Category:Naval battles involving France]]
[[Category:Naval battles involving Great Britain]]
[[Category:April 1795 events]]