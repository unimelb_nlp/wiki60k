[[File:Troy Gas Light Half Elevation.png|thumb|This half elevation drawing of the [[Troy Gas Light Company|Troy Gasholder Building]] has been used as the SIA logo since 1971<ref>[http://www.siahq.org/Images/photos.html About the gasholder logo]</ref>]]
The '''Society for Industrial Archeology (SIA)''' is a [[North America]]n [[nonprofit organization]]<ref>{{cite web | title = Nonprofit Report for Society for Industrial Archeology | publisher = GuideStar | url = http://www.guidestar.org/organizations/23-7166954/society-industrial-archeology.aspx | accessdate = 2012-07-08}}</ref> dedicated to studying and preserving historic industrial sites, structures and equipment. It was founded in 1971 in [[Washington, D.C.]], and its members are primarily from the [[United States]] and [[Canada]], although there is some crossover with similar [[industrial archaeology]] organizations in the [[United Kingdom]]. SIA's headquarters is currently located in the Department of Social Sciences at [[Michigan Technological University]] in [[Houghton, Michigan]]. In addition to the national organization, there are thirteen regional chapters throughout the United States.<ref>{{cite web | title = About the Society for Industrial Archeology (SIA) | publisher = Society for Industrial Archeology | url = http://www.siahq.org/about/aboutthesia.html | accessdate = 2009-08-20}}</ref>

==Activities==
Since its founding in October 1971, SIA members have gathered at an annual conference, which also serves as the annual business meeting required by its bylaws.<ref>{{cite journal | last = Hyde | first = Charles K. | title = The Birth of the SIA and Reminiscences by Some of its Founders | journal = [[IA, The Journal of the Society for Industrial Archeology]] | volume = 17 | issue = 1 | pages = 3–16 | year = 1991 | jstor = 40968216}}</ref> The annual conference is typically held each spring. The Fall Tour, a second annual gathering usually held in September or October, began in 1972. Both annual events feature visits to local industrial sites, both active and historical.  The conference additionally includes a day of paper sessions.  Industrial heritage study tours to other countries began in 1990 and occur on an irregular schedule.

SIA produces two official publications, the formal, [[Peer review|peer-reviewed]] ''[[IA, The Journal of the Society for Industrial Archeology]]'' twice a year and the less formal quarterly ''Society for Industrial Archeology Newsletter (SIAN).''  The annual conference is typically accompanied by a custom guidebook profiling local industrial and cultural sites.  In addition, SIA produces occasional publications on special topics.

==Newsletter==
The ''Society for Industrial Archeology Newsletter (SIAN)'' ([[ISSN]] {{ISSN search link|0160-1067}}) is the Society's official [[newsletter]], currently edited by Patrick Harshbarger and published quarterly. ''SIAN'' publishes articles about recent events, summaries of the Society's annual conferences and fall tours, news about local chapters, brief abstracts of articles on industrial archeology-related subjects, and other notes and queries.

The first issue of ''SIAN'' was published in January 1972 and reported on the Society's founding. Volumes 1 through 9 consisted of six issues per calendar year, not including two special supplemental issues published in 1972. The current quarterly publication schedule with Winter, Spring, Summer, and Fall issues began with Volume 10 in 1981.<ref>{{cite web |title=SIA Newsletter |publisher=Society for Industrial Archeology |url=http://www.sia-web.org/publications/sia-newsletter/ |accessdate=2015-02-07}}</ref>

==See also==
*[[Industrial archaeology]]
*[[Historic American Engineering Record]]
*[[The International Committee for the Conservation of the Industrial Heritage]]

==References==
{{reflist}}

==External links==
* {{Official website|1=http://www.siahq.org/}}

[[Category:Archaeological organisations]]
[[Category:History organizations based in the United States]]
[[Category:Industrial archaeology]]
[[Category:Non-profit organizations based in Michigan]]