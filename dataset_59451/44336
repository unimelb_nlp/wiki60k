{{Infobox airport
| name                   = Sequoia Field Airport
| image                  = Sequoia Field Airport - California.jpg
| image-width            = 250
| caption                = 2006 USGS airphoto
| FAA                    = D86
| type                   = Public
| owner                  = [[Tulare County, California|County of Tulare]]
| operator               = 
| city-served            = [[Visalia, California]]
| location               = <!--if different than above-->
| elevation-m            = 95
| elevation-f            = 313
| coordinates            = {{coord|36|26|52|N|119|19|07|W|region:US-CA_type:airport_scale:10000|display=inline,title}}
| website                = 
| pushpin_map            = California
| pushpin_label_position = bottom
| pushpin_label          = '''D86'''
| pushpin_mapsize        = 250
| pushpin_map_caption    = Location of Sequoia Field Airport
| r1-number              = 13/31
| r1-length-m            = 918
| r1-length-f            = 3,012
| r1-surface             = [[Asphalt]]
| stat-year              = 2010
| stat1-header           = Aircraft operations
| stat1-data             = 12,000
| stat2-header           = Based aircraft
| stat2-data             = 15
| footnotes              = Source: [[Federal Aviation Administration]]<ref name="FAA">{{FAA-airport|ID=D86|use=PU|own=PU|site=02414.*A}}. Federal Aviation Administration. Effective 8 April 2010.</ref>
}}
[[File:Sequoia Field - Main Formation Area.jpg|thumb|View of Sequoia Field Headquarters and main formation area, 1943]]
[[File:Sequoia Field - Cadets Marching along Flight Line.jpg|thumb|Photo of Sequoia Field flight cadets marching in front of Fairchild PT-19 trainers in 1943]]
[[File:Camp Sequoia - CPS-1 Radar and training buildings.jpg|thumb|Camp Sequoia - CPS-1 Radar and training buildings, 1945]]
'''Sequoia Field Airport''' {{airport codes|||D86{{nobold|, formerly}} Q31}} is a county-owned, public-use [[airport]] located eight nautical miles (15&nbsp;km) north of the [[central business district]] of [[Visalia, California|Visalia]], a city in [[Tulare County, California|Tulare County]], [[California]], [[United States]].<ref name="FAA" />

== Facilities and aircraft ==
Sequoia Field covers an area of {{convert|150|acre|ha|lk=on}} at an [[elevation]] of 313 feet (95 m) above [[mean sea level]]. It has one [[runway]] designated 13/31 with an [[asphalt]] surface measuring 3,012 by 60 feet (918 x 18 m).<ref name="FAA" />

For the 12-month period ending January 6, 2010, the airport had 12,000 [[general aviation]] aircraft operations, an average of 32 per day. At that time there were 15 aircraft based at this airport: 40% single-[[aircraft engine|engine]], 27% multi-engine, 20% [[helicopter]] and 13% [[ultralight]].<ref name="FAA" />

==History==
In preparation for the eventual U.S. entry into World War II, the United States Army Air Corps sought to expand the nation's combat air forces by asking civilian flight schools to provide the primary phase of training for air cadets. Consequently, it contracted with nine civilian flying schools to provide primary flying training, with the graduates being moved on to basic and advanced training at regular military training airfields.<ref name="Cameron">Cameron, Rebecca Hancock, 1999, Training to Fly. Military Flight Training 1907-1945, Air Force Historical Research Agency, Maxwell AFB, Alabama</ref>

The original airfield had 2,300' turf runway.  Sequoia field had possibly had up to six axillary airfields in local area.  The only one located is Three Rivers Auxiliary Field {{Coord|36|27|30|N|118|54|00|W}}, which was an all-way open field and no remains of it exist.

===Pilot Training===
Activated on 4 October 1941, '''Sequoia Field''' was assigned to the [[United States Army Air Forces]] [[Western Flying Training Command]], [[35th Flying Training Wing (World War II)|35th Flying Training Wing]] as a primary (level 1) pilot training airfield.<ref>35th Flying Training Wing, lineage and history document Air Force Historical Agency, Maxwell AFB, Alabama</ref>  The 8th Flying Training Detachment, Army Primary Flying School was Commanded by a Major L.D. Dreisbach

The Visalia-Dinuba School of Aeronautics conducted basic flying training under contract until inactivated in October 1944.  The primary use of Sequoia Field was basic flying training of flight cadets. [[Ryan-PT22's]] were the primary trainer used. Also had several [[PT-17 Stearman]]s and a few [[P-40 Warhawk]]s assigned.  The nine weeks of training provided by civilian flying schools included 65 hours of flying instruction in addition to the standard ground school curriculum provided by the Air Corps.<ref name="Cameron"/>

The physical facilities of Sequoia Field included administrative buildings and quarters for officers and enlisted men, encircling a central location. A consolidated mess hall, which accommodated 1,000 enlisted men and a limited number of' officers, was located nearby. Adjacent to the mess hall was a Post Exchange, a Service Club and a dance floor. Recreational facilities at Camp Sequoia centered on the base swimming pool. Nearby were tennis and handball courts, and a softball diamond. Regular dances were conducted at the base Service Club and in Visalia.<ref name="CHP"/>

Sequoia Field was inactivated on 8 September 1944 with the draw down of AAFTC's pilot training program, and the final cadet class at Sequoia Field graduated on September 30, 1944.<ref name="CHP">[https://www.facebook.com/CAMilitaryHistory/posts/304046336449205 California Military History, Sequoia Field]</ref> With its closure, the facility was declared surplus and turned over to the Army Corps of Engineers on 5 May 1945 and placed on an inactive status.

===Radar Training===
On 23 July 1945, the post, now known as Camp Sequoia, was taken over by the Western Signal Aviation Training Center at Camp Pinedale as a sub-post. It was a replacement for nearby Camp Visalia. The garrison, which moved from Camp Visalia, consisted of Squadron F, 462nd Army Air Forces Base Unit (AAFBU) commanded by Maj. Hugh J. Roberts.<ref name="CHP"/>

Camp Sequoia was used for the purpose of providing on-the-job training for fighter control squadron personnel in the use of the AN/CPS-1 Radar. The unit also provided training on the AN/CPS-1 for maintenance and operating crews, assigned to the controllers at [[Hammer Field]].<ref name="CHP"/>
The Electronics School, situated at Camp Wawona until 14 July 1945, was assigned to Camp Sequoia on that date to provide a fundamental theoretical background for radar operations. Personnel assigned to the school designed and constructed laboratory equipment and prepared lesson plans.<ref name="CHP"/>

Camp Sequoia was evidently closed at the end of World War II, as it does not appear in any Station Lists or Airfield Directories after 1 December 1945.<ref name="CHP"/> The facility was turned over to civil control in February 1947 through the War Assets Administration (WAA). Several wartime buildings still remain in use.<ref name="CHP"/><ref>{{Air Force Historical Research Agency}}</ref>
<ref>Shaw, Frederick J. (2004), Locating Air Force Base Sites History’s Legacy, Air Force History and Museums Program, United States Air Force, Washington DC, 2004.</ref>
<ref>Manning, Thomas A. (2005), History of Air Education and Training Command, 1942–2002.  Office of History and Research, Headquarters, AETC, Randolph AFB, Texas ASIN: B000NYX3PC</ref>

===Current Use===
[[File:2009-0725-CA-SequoiaField-Dinuba.jpg|thumb|Sequoia Field- Hangar from Visalia-Dinuba School of Aeronautics, 2009]]
Today, Sequoia Field is a modern general aviation airport.  Much of the former containment area has been converted to the Tulare County Men's Correctional Facility, and many of the World War II buildings are still in use. Two World War II hangars from the military flight training field remain and are in use for general aviation aircraft.  The airfield has a modern runway, although aircraft parking is noted to use part of the former wartime parking area.

==See also==
{{Portal|United States Air Force|Military of the United States|World War II}}
* [[California World War II Army Airfields]]
* [[35th Flying Training Wing (World War II)]]

==References==
{{Air Force Historical Research Agency}}
{{Reflist}} 
* [http://www.airfieldsdatabase.com/WW2/WW2%20R26c1%20AL-CA.htm www.airfieldsdatabase.com]

==External links==
{{Commons category|Sequoia Field, California}}
* [http://msrmaps.com/map.aspx?t=1&s=11&lat=36.4479&lon=-119.3187&w=450&h=550&lp=---+None+--- Aerial image as of 1 June 1994] from [[USGS]] ''[[The National Map]]''
* {{US-airport-minor|D86}}

{{USAAF Training Bases World War II}}

[[Category:1941 establishments in California]]
[[Category:Airfields of the United States Army Air Forces in California]]
[[Category:USAAF Contract Flying School Airfields]]
[[Category:Airports in Tulare County, California]]
[[Category:Airports established in 1941]]
[[Category:USAAF Western Flying Training Command]]
[[Category:American Theater of World War II]]