{{about||the American politician|Mary Small (politician)|the New Zealand market gardener and farmer|Mary Elizabeth Small}}
{{Use mdy dates|date=November 2012}}
{{Infobox person
| name            = Mary Small
| image           =
| caption         = 
| birth_name      = Harriette Mary Small
| birth_date      = {{birth date|1922|5|14}}
| death_date      = {{death date and age|2007|2|27|1922|5|10}}
| birth_place     = [[Baltimore]], [[Maryland]]
| death_place     = [[Harlem]], [[New York City]]
| home_town       =
| residence       =
| notable_works   = 
| relatives       = 
| occupation      = Singer, actress, radio personality
| years_active    = 1928–2007
| module      = {{Infobox musical artist |embed=yes
| background      = solo_singer
| years_active    = 
| genre           = 
| label           = 
| associated_acts = [[Vic Mizzy]], [[Ray Bloch]], [[Milton Berle]], [[Vincent Lopez]]
| website         = {{URL|marysmall.net}}
}}}}
'''Mary Small''' (May 10, 1922 &ndash; February 27, 2007) was a prominent singing personality during the [[Golden Age of Radio]]<ref>{{cite news|title=From Child Star To Glamour Girl|accessdate=27 November 2012|newspaper=Tune In|date=December 1945|url=http://www.otrr.org/FILES/Magz_pdf/Tune%20In/Tune%20In%204512.pdf|page=41}}</ref> and hosted her own broadcasts for [[List of U.S. radio programs|14 consecutive years]] across all major networks.<ref>{{cite web|last=Husch|first=Larry|title=OTRRpedia|url=http://www.otrrpedia.net/getpersonF.php?PN=5740|work=Database Of Old Time Radio Programs And People|publisher=OTTRpedia.com|accessdate=27 November 2012|format=Database|year=1933–1947}}</ref> She headlined or opened at "presentation houses" from the 1930s through the 1950s including the Paramount Theater,<ref>{{cite web|last=Goldin|first=J. David|title=Radio Gold Index|url=http://radiogoldindex.com/cgi-local/p2.cgi?ProgramName=Riding+High|work=Riding High Chronology 1937|publisher=radiogoldindex.com|accessdate=27 November 2012|year=1937}}</ref> Madison Square Garden, the London Palladium,<ref>{{cite news|accessdate=27 November 2012|newspaper=Billboard Magazine|date=January 24, 1953|location=Music Section|page=44}}</ref> the Copacabana with [[Sammy Davis, Jr.]],<ref>{{cite news|newspaper=Milwaukee Sentinel|date=April 15, 1954}}</ref><ref>{{cite web|title=Richard Nixon, Sammy Davis Jr. And Mary - 1954|url=http://marysmall.tumblr.com/post/29345117357/richard-nixon-sammy-davis-jr-and-mary-1954|work=The Little Girl With The Big Voice|publisher=Rafael Moscatel|accessdate=27 November 2012}}</ref>   and the Palace Theater in Chicago.  In addition to being an established recording artist,<ref>{{cite web|last=Small|first=Mary|title=Best Female Voices Of The 1950's|url=https://itunes.apple.com/us/album/best-female-voices-50s/id382335920|work=Careless, The Brush Off|publisher=Masters Classics Records|accessdate=27 November 2012|year=2010}}</ref>  she was a published author<ref>{{cite journal|last=Small|first=Mary|title=Mistaken Hour|journal=Woman's Home Companion|date=July 1949|pages=22–23, 52–54|url=http://marysmall.tumblr.com/post/30941630165/mistaken-hour-a-rediscovered-short-story-by-mary|accessdate=27 November 2012}}</ref>  and performed on film, television<ref>{{cite web|last=Small|first=Mary|title=Credits|url=http://www.imdb.com/name/nm0806499/|publisher=IMDB|accessdate=27 November 2012}}</ref>  and Broadway<ref>{{cite web|last=Small|first=Mary|title=Credits|url=http://www.ibdb.com/person.php?id=79719|publisher=IBDB|accessdate=27 November 2012}}</ref>  during her career.<ref>{{cite book|last=Siegel|first=David|title=Remembering Radio: An Oral History Of Old-Time Radio|year=2010|publisher=BearManor Media|location=Albany, Georgia|isbn=1-59393-537-4|pages=246–266|url=http://www.barnesandnoble.com/w/remembering-radio-david-s-siegel/1022699953}}</ref> She was the first singer to be widely promoted as ''The Little Girl With The Big Voice'', a [[moniker]] likely adopted by her first manager Ed Wolfe that was marketed in the [[Fleischer Brothers]]' [[Screen Songs|''Love Thy Neighbor'']], distributed by [[Paramount Pictures]] in 1934.<ref>{{cite web|last=Small|first=Mary|title=Love Thy Neighbor|url=https://www.youtube.com/watch?v=_zhz-HLeWY4|work=Love Thy Neighbor|publisher=Fleischer Studios|accessdate=27 November 2012|format=audiovisual|year=1934}}</ref><ref>{{cite web|last=IMDB|title=Love Thy Neighbor|work=Screen Songs Love Thy Neighbor|publisher=Paramount Pictures|accessdate=27 November 2012|url=http://www.imdb.com/title/tt0025432/}}</ref> The moniker "Little Girl With The Big Voice" was subsequently used to promote female singing prodigies from [[Judy Garland]] to [[Jackie Evancho]]. She was married for a time to the composer [[Vic Mizzy]] with whom she had a widely publicized divorce.<ref>{{cite news|last=Neigher|first=Harry|title=Has Mizzy In A Tizzy|url=https://news.google.com/newspapers?nid=2229&dat=19610212&id=9LEyAAAAIBAJ&sjid=6AAGAAAAIBAJ&pg=4710,5542255|accessdate=27 November 2012|newspaper=Sunday Herald|date=February 12, 1961}}</ref> Her life is the subject of a documentary by Rafael Moscatel.<ref>{{cite web|title=The Little Girl With The Big Voice (2015)|url=http://www.imdb.com/title/tt3818140/|website=http://www.imdb.com/|publisher=IMDB|accessdate=7 September 2014}}</ref>

== Early years ==
Small was born in Baltimore, Maryland to Jack and Fannie Small.  Her father was a local vaudevillian and her mother a homemaker. She first performed on Baltimore radio station [[WBAL (AM)|WBAL]] at the age of six or seven and at nine won a radio contest hosted by [[Gus Edwards]]. She had a younger sister named Gloria. The story of how she was discovered was widely reported in newspapers, cartoon strips and interviews well into her later years <ref>{{cite news|last=Harmony|first=Henry|title= Henry Harmony Pictures The Rise Of Mary Small, Radio's Youngest Star|url=http://www.otrr.org/FILES/Magz_pdf/Microphone/Microphone_34_10_12.pdf|accessdate=27 November 2012|newspaper=The Microphone|date=October 19, 1934|page=9|format=Cartoon}}</ref><ref>{{cite news|last=Uricchio|first=Marylynn|title=Nostalgia pervades 'Follies' cast|url=https://news.google.com/newspapers?nid=1129&dat=19830711&id=YJ5RAAAAIBAJ&sjid=pG0DAAAAIBAJ&pg=4796,2025463|newspaper=Pittsburgh Post-Gazette|date=July 11, 1983}}</ref> She was interviewed by [[Joe Franklin]] in 1972.<ref>{{cite web|last=Franklin|first=Joe|title=Interviews|url=http://www.worldcat.org/title/lanny-ross-carol-bruce-mary-small-lanny-ross-carol-bruce-mary-small-bob-warren-dick-haymes/oclc/056044684|publisher=Frank Diggs Collection|accessdate=27 November 2012|year=1972}}</ref>

[[File:Henry Harmony Pictures The Rise Of Mary Small.png|thumbnail|right|Henry Harmony Pictures The Rise Of Mary Small]]

In 1933, at the age of eleven she was introduced to singing trio [[Three X Sisters|''the Three X Sisters']]' at the Hippodrome Theater on Eutaw Street in Baltimore.  The trio arranged for her an audition with their manager Ed Wolfe who then booked her on the [[Rudy Vallee|Rudy Vallee Hour]] on NBC affiliate [[WNBC (AM)|WEAF]] New York where she received her first big break singing ''Louisville Lady''.  Mary’s voice was unique for that of a child, almost freakish to some, and the audience disbelief as to her age captivated America.<ref>{{cite news|last=Small|first=Mary|title=Mary Small Vigorously Denies She Is Any Older Than Her 13 Years|url=https://news.google.com/newspapers?nid=1144&dat=19350814&id=5yEbAAAAIBAJ&sjid=hksEAAAAIBAJ&pg=1973,127431|accessdate=27 November 2012|newspaper=Pittsburgh Press|date=August 13, 1945|page=14}}</ref>   Within a month she had landed her own show on NBC which led into Frank Sinatra’s hour.  Along with a selected stable of stars, they were promoted across the country on matchbooks, bottle caps and subway cars. While a child in New York she attended the [[Professional Children's School]]. Her childhood friend was [[Baby Rose Marie]].<ref name="Siegel">{{cite book|last=Siegel|first=David|title=Remembering Radio: An Oral History of Old-Time Radio|year=2010|publisher=BearManor Media|isbn=978-1-59393-537-5|pages=246–266|chapter=9}}</ref>

== Golden Age Of Radio ==
[[File:Mary Small NBC Matchbook.jpg|framed|right|NBC Matchbook With Mary Small 1930s]]
Small was  successful on radio throughout the 1930s and 1940s and either hosted or was featured on a number of programs.  She worked with the biggest bands and orchestras of the day including [[Tommy Dorsey]], Ray Bloch, [[Glenn Miller]] and with stars like [[Roy Rogers]], [[Dean Martin]], [[Jerry Lewis]], [[Jackie Gleason]] and Frank Sinatra. She had a number of announcers for her programs over the years including [[Bud Collyer]] and [[Milton Cross]] who was best known as the voice of the Metropolitan Opera for 43 years.<ref name="Siegel"/>

She was interviewed by David Siegel on September 24, 1999, for his book ''Remembering Radio: An Oral History of Old Time Radio''<ref name="Siegel"/> and quoted as saying:

:''Then, I got my own radio show, which I just mentioned was fifteen minutes, five nights a week, which Frank Sinatra followed, and we knew each other pretty well.  Me with my little white socks, he with his long pants, but he had just left the Tommy Dorsey Band and I came in at 11:00 with Walter Gross' Orchestra, a seventeen-piece live band.  I rehearsed during the afternoon, and there was a commercial break of about sixty seconds, and Frank Sinatra came in at 11:15.  He was billed as the voice that is thrilling millions.''

Throughout her career was employed by [[NBC]], [[American Broadcasting Company|ABC]] and [[CBS]] and the [[Mutual Broadcasting Company]].

=== Partial List Of Radio Credits ===
* ''[[Ben Bernie]]'' (1933–1936)
* ''Little Miss Bab-O’s Surprise Party'' (1934–1935)
* ''The Maxwell House Showboat'' (1937)
* ''Riding High'' (1937)
* ''Keep It Dark'' (1941)
* ''Imperial Time'' (1941)
* ''Keep ‘Em Rolling'' (1942-01-25)
* ''The Chamber Music Society Of Lower Basin Street'' (1942)
* ''The Kemtone Hour'' (1944)
* ''The Mary Small Show'' (1944)
* ''Music For Millions'' (1945)
* ''By Popular Demand'' (1945)
* '[[Dorothy Kilgallen|'Dorothy Kilgallen’s Diary'']] (1945)
* ''Guest Star Program'' (1947)
* ''[[Three for the Money]]'' (1948)
* ''Behind The Mike'' (1940)
* ''[[Your Hit Parade]]'' (1940's)
* ''The Mary Small Junior Miss Show'' (1940's)

== WW2 & The USO ==
Throughout WW2 and beyond, radio stations' programming played a role in the war effort.  Mary’s ballads were swapped out for patriotic songs and she worked with the Treasury Department participating in US bond rallies where she shared the stage doing spots with actors like [[Jimmy Stewart]].<ref>{{cite web|last=Haendigis|first=Jerry|title=Episodic Log|url=http://www.otrsite.com/logsa/logg1018.htm|work=Series: Guest Star|publisher=ottrsite.com|accessdate=27 November 2012}}</ref><ref>{{cite web|title=Mary Small and Jimmy Stewart (1940's)|url=http://marysmall.tumblr.com/post/31822882640/in-this-clip-mary-does-some-back-and-forth-with|work=Guest Star|publisher=Rafael Moscatel|accessdate=27 November 2012|format=Audio Recording}}</ref>  Mary also joined Pearl Hamilton, one of ''The Three X Sisters'', to tour with the USO in 1943 or 1944 and sang the song ''Smile, America, Smile''. She also toured with B.A. Rolfe's ''Daughters Of Uncle Sam'' in 1942.<ref>{{cite news|title=Prov. Gross Low|url=https://books.google.com/books?id=LgwEAAAAMBAJ&pg=PA118&lpg=PA118&dq=%22daughters+of+uncle+sam%22+rolfe&source=bl&ots=nKBUNdXZbv&sig=FqZb1qGmXXwvhCdQsZRS_ajyYe0&hl=en&sa=X&ei=ZA-1UMTgI6XDigLH24G4BA&ved=0CE4Q6AEwBQ#v=onepage&q=%22daughters%20of%20uncle%20sam%22%20rolfe&f=false|accessdate=27 November 2012|newspaper=Billboard Magazine|date=June 13, 1942|page=18}}</ref>

=== Thank You, Mr. President ===
In 1942, at the [[March of Dimes]] event celebrating [[Franklin D. Roosevelt's paralytic illness|Franklin Roosevelt]]’s 60th birthday, Mary performed her own song, “Thank you, Mr. President,” backed by the Glenn Miller orchestra and broadcast live from the [[Waldorf Astoria]].<ref>{{cite web|last=Small|first=Mary|title=Thank You, Mr. President|url=http://marysmall.tumblr.com/post/35712805322/this-extremely-rare-recording-is-of-mary-small|accessdate=27 November 2012|year=1942}}</ref><ref>{{cite web|last=Goldin|first=J. David|title=January 30, 1942|url=http://radiogoldindex.com/cgi-local/p6.cgi?DateN=19420130;Date=January%20%20%2030,%201942|work=Radio Gold Index|publisher=J. David Goldin|accessdate=27 November 2012|date=January 30, 1942}}</ref>  This recording can be heard at [http://marysmall.tumblr.com/post/35712805322/this-extremely-rare-recording-is-of-mary-small The Little Girl With The Big Voice].

== Stage Performance & Recording Years ==
Small performed as a headliner and recorded consistently from 1934 through the 1950s.  Her image appears on dozens of sheet music titles.  After leaving showbiz to raise two daughters, she returned to Broadway in 1966 and toured with a new Follies cast.  She also expanded into dramatic theater playing the role of Lenny Bruce’s mother in a play about his life.<ref>{{cite web|title=ELLIOT NORTON REVIEWS: "LENNY" {MARY SMALL, JON YATES, MARTY BRILL} (TV)|url=http://www.paleycenter.org/collection/item/?q=the&p=569&item=T91:0241|publisher=Paley Center For Media|accessdate=27 November 2012|year=1974}}</ref>

A number of her recordings and television performances can be found at the website [http://marysmall.tumblr.com/ The Little Girl With The Big Voice].  A comprehensive CD of her recordings from the late 1940s and 50's was released in 2013 by Jasmine Records <ref name="jasmine-records.co.uk">http://www.jasmine-records.co.uk/acatalog/jascd-281.html</ref>

=== Incident With Richard Nixon and Sammy Davis, Jr. ===
In 1954, after a show at the [[Copacabana (nightclub)|Copacabana]] with Sammy Davis Jr., [[Richard Nixon]] and his wife Pat stopped by.  Afterwards the then Vice President made his way to the dressing rooms to thank them for the show.  The papers noted Mary as saying ''They applauded me as if I were a Republican!''

=== Partial List of Stage Credits ===
* ''Little Me'' (1982)
* ''[[Sextet (play)|Sextet]]'' (1974)
* ''[[Early to Bed (musical)|Early to Bed]]'' (1944)
* ''[[Lenny Bruce|Lenny]]'' (1974)
* ''[[Follies|Stephen Sondheim's Follies]]'' (1974)
* ''Goldilocks'' (1982)

== Film & Television ==
In 1930's Small began performing at the [[Paramount Theatre (Brooklyn, New York)|Paramount Theater]] between films and newsreels to draw in bigger crowds and then as a solo act.<ref>{{cite news|title=Movie Review: The Notorious Sophie Lang|accessdate=27 November 2012|newspaper=NY Times}}</ref><ref>{{cite news|title=Movie Review: The Notorious Sophie Lang|url=https://movies.nytimes.com/movie/review?res=9804EEDC133CE23ABC4951DFB166838F629EDE&pagewanted=print|accessdate=27 November 2012|newspaper=NY Times|date=July 21, 1934}}</ref> In 1934, [[Max Fleischer]] hired Mary to appear in one of his community-sing "[[Bouncing Ball]]" cartoons, ''Love Thy Neighbor'', filmed at his New York studio. She appeared on camera, singing the title song.

=== Partial List of Film & Television Credits ===
* ''American Minstrels'' (1949)
* ''Love Thy Neighbor'' (1934)
* ''[[Versatile Varieties]]'' (1949-1950)
* ''[[The Ed Sullivan Show]]'' (1952–1954)
* ''[[The Buick-Berle Show]]'' (1949)
* ''Rare Records''<ref name="jasmine-records.co.uk"/> (released 2013)

== Later Years ==
In her later years, Small worked as a sought after vocal coach and performed in nightclubs in Manhattan.<ref>{{cite news|title=Mary Small helps singers find their own voices|url=http://business.highbeam.com/5395/article-1G1-80344547/mary-small-helps-singers-find-their-own-voices|accessdate=27 November 2012|newspaper=Back Stage|date=October 26, 2001}}</ref>   At the time of her death she had outlived most of her contemporaries. Most of her life's work was not comprehensively cataloged until 2012.<ref>{{cite web|last=Moscatel|first=Rafael|title=Mary Small: The Little Girl With The Big Voice |url=http://www.marysmall.tumblr.com|work=The Little Girl With The Big Voice|accessdate=27 November 2012}}</ref>

==References==
{{reflist}}

{{DEFAULTSORT:Small, Mary}}
[[Category:American television actresses]]
[[Category:American film actresses]]
[[Category:American stage actresses]]
[[Category:American radio personalities]]
[[Category:American child singers]]
[[Category:American female singers]]
[[Category:1922 births]]
[[Category:2007 deaths]]
[[Category:20th-century American singers]]