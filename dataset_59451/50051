{{About|the season four episode|the season six episode|Oathbreaker (Game of Thrones)}}
{{Infobox television episode
 |title = Oathkeeper
 |image = Game_of_Thrones-S04-E04-The-Nights-King.jpg
 |image_size = 280
 |caption = In the episode's final scene, the [[Night King]] appears for the first time.
 |series = [[Game of Thrones]] 
 |season = 4
 |episode = 4
 |director = [[Michelle MacLaren]]
 |writer = [[Bryan Cogman]]
 |producer =
 |music = [[Ramin Djawadi]]
 |photographer = [[Robert McLachlan (cinematographer)|Robert McLachlan]]
 |editor = Crispin Green
 |production = 
 |airdate = {{Start date|2014|4|27}}
 |length = 55 minutes <!-- Exact running time: 55:02 -->
 |guests =
* [[Diana Rigg]] as Olenna Tyrell
* [[Michiel Huisman]] as Daario Naharis
* [[Ian McElhinney]] as Barristan Selmy
* [[Owen Teale]] as Alliser Thorne
* [[Burn Gorman]] as Karl Tanner
* [[Noah Taylor]] as Locke
* [[Dominic Carter (actor)|Dominic Carter]] as Janos Slynt
* [[Nathalie Emmanuel]] as Missandei
* [[Jacob Anderson]] as Grey Worm
* [[Thomas Sangster|Thomas Brodie-Sangster]] as Jojen Reed
* [[Ellie Kendrick]] as Meera Reed
* [[Kristian Nairn]] as Hodor
* [[Mark Stanley]] as Grenn
* [[Ben Crompton]] as Eddison Tollett
* Luke Barnes as Rast
* [[Dean-Charles Chapman]] as Tommen Baratheon
* [[Daniel Portman]] as Podrick Payne
* [[Josef Altin]] as Pypar
* [[Brenock O'Connor]] as Olly
* Emilio Doorgasingh as a Great Master
* [[Ross Mullan]] as a White Walker
* [[Richard Brake]] as the Night King
 |season_list =
 |prev = [[Breaker of Chains]]
 |next = [[First of His Name]]
 |episode_list = [[Game of Thrones (season 4)|''Game of Thrones'' (season 4)]]<br>[[List of Game of Thrones episodes|List of ''Game of Thrones'' episodes]]
}}

'''"Oathkeeper"''' is the fourth episode of the [[Game of Thrones (season 4)|fourth season]] of [[HBO]]'s [[fantasy]] television series ''[[Game of Thrones]]'', and the 34th overall. The episode was written by [[Bryan Cogman]],<ref name="Writers">{{cite web |url=http://winteriscoming.net/2014/02/here-is-your-season-4-writers-breakdown/ |title=Here is your season 4 writers breakdown |publisher=WinterIsComing.net |date=February 26, 2014 |accessdate=April 25, 2014}}</ref> and directed by [[Michelle MacLaren]].<ref name="4directors">{{Cite web|url=http://insidetv.ew.com/2013/07/16/game-of-thrones-season-4-director-list/|title='Game of Thrones' season 4 directors chosen|work=Entertainment Weekly|last=Hibberd|first=James|date=July 16, 2013|accessdate=April 25, 2014}}</ref> It aired on April 27, 2014.<ref name="Futon Critic">{{Cite web|url=http://www.thefutoncritic.com/listings/20140307hbo04/|title=(#34/404) "Oathkeeper"|work=The Futon Critic|accessdate=April 25, 2014}}</ref> The title refers to the new sword gifted to [[Brienne of Tarth]] by Jaime Lannister<ref name="title">{{Cite web|url=http://www.craveonline.com/tv/reviews/682109-game-of-thrones-4-04-oathkeeper |title=  Game of Thrones Season 4 Episode 4 |work=Crave Online|last= |first= |date= |accessdate=May 6, 2014}}</ref> and the themes of duty that propel the episode.<ref name="theme">{{Cite web|url=http://www.slantmagazine.com/house/2014/28/game-of-thrones-recap-season-4-episode-4-oathkeeper |title= Game of Thrones Recap: Season 4, Episode 4, "Oathkeeper" |work=Slant |last= Cole|first= Jack|date= April 28, 2014|accessdate=May 6, 2014}}</ref> The episode focuses on the aftermath of Joffrey's wedding, the Night's Watch's attempt to deal with the mutineers, and Daenerys' continued conquest of Meereen. "Oathkeeper" also featured the debut appearance of the Night King, a leader of the [[White Walker]]s, though he was not identified as such until [[Hardhome|the following year]].

==Plot==
===In King's Landing===
Jaime Lannister ([[Nikolaj Coster-Waldau]]) continues his sword training with Bronn ([[Jerome Flynn]]) and visits his imprisoned brother Tyrion ([[Peter Dinklage]]). Tyrion denies responsibility for Joffrey Baratheon's murder but says that Cersei Lannister will not rest until he is dead. Jaime tells Tyrion that Cersei also wants to recapture Sansa Stark, in whose innocence Tyrion is confident. 

As Lady Olenna Tyrell ([[Diana Rigg]]) prepares to depart for Highgarden, she encourages Margaery Tyrell ([[Natalie Dormer]]) to bond with Joffrey's brother and successor, Tommen ([[Dean-Charles Chapman]]), before Cersei can turn him against her. Olenna also implies that she had a hand in Joffrey's death to protect Margaery from his cruelty. In the evening, an angry Cersei ([[Lena Headey]]) questions Jaime's loyalties and is unwilling to hear of Tyrion's innocence. At night, Margaery sneaks into Tommen's room to talk with him about their marriage, and he appears smitten by her charms.

Later, Jaime tasks Brienne of Tarth ([[Gwendoline Christie]]) with finding and protecting Sansa. He gives her new armor, his [[Valyrian steel]] sword, which she names "Oathkeeper", and the service of Podrick Payne ([[Daniel Portman]]) as a squire.

===At sea===
On the way to the Eyrie, Lord Petyr "Littlefinger" Baelish ([[Aidan Gillen]]) tells Sansa ([[Sophie Turner (actress)|Sophie Turner]]), that he plans to marry her aunt Lysa Arryn there. He refers to his new but unspecified powerful allies, stating that Joffrey's death was a gift to them that will help their friendship "grow strong". He adds that, because he has no visible motive to kill Joffrey, he will not be suspected. Littlefinger informs Sansa that a missing stone in her necklace contained the poison used for the murder.

===At the Wall===
At Castle Black, Ser Alliser Thorne ([[Owen Teale]]) orders Jon Snow ([[Kit Harington]]) to stop training other men, reminding Jon that he is officially a steward. Janos Slynt ([[Dominic Carter (actor)|Dominic Carter]]) advises Thorne to send the now-popular Jon on an expedition to kill the mutineers at Craster's Keep in the hope that he will be killed before he can be elected the new Lord Commander. Jon volunteers for the mission, as do his friends Grenn ([[Mark Stanley]]), Edd ([[Ben Crompton]]), the new recruit Locke ([[Noah Taylor]]) and a few others.

===In Meereen===
As Daenerys Targaryen's ([[Emilia Clarke]]) army camps near Meereen, her aide Missandei ([[Nathalie Emmanuel]]) teaches Grey Worm ([[Jacob Anderson]]) the Common Tongue, the language of Westeros. At night, Grey Worm and other Unsullied infiltrate Meereen, arm the slaves, and incite a slave uprising that leaves Daenerys in control of the city the following morning. Although Ser Barristan Selmy ([[Ian McElhinney]]) advises Daenerys to show mercy, she orders 163 of the remaining masters crucified, as justice for the same number of slave children crucified along the road to Meereen.

===Beyond the Wall===
At Craster's Keep, the mutineers led by Karl Tanner ([[Burn Gorman]]) are raping, eating and drinking their fill. Karl orders his henchman Rast (Luke Barnes) to dispose of Craster's last child, a son, after Craster's wives ask that he be "given to the gods". Karl realizes that the child will be taken by the White Walkers, and that this is how Craster protected himself from them. He reasons that if they continue to uphold the bargain they too will be safe; Rast complies and abandons the infant in the woods. Nearby, Bran Stark ([[Isaac Hempstead-Wright]]) and his companions hear the infant's cries. They are captured by the mutineers, and Bran is forced to admit his identity.

Later, a White Walker retrieves Craster's son. The Walker travels to the Lands of Always Winter and lays the child on an altar made of ice.<!--The Walker in question was revealed to be the Night King in the season 5 episode "Hardhome". Do not change.--> The Night King approaches and places his hand on the boy, turning his eyes blue in the process.

==Production==
[[File:Bryan Cogman Fan Photograph (cropped).jpg|right|thumb|upright|Series veteran [[Bryan Cogman]] wrote this episode.]]
"Oathkeeper" was written by Bryan Cogman based on ''[[A Storm of Swords]]''. Reviewer Walt Hickey of [[FiveThirtyEight|FiveThirtyEight]] notes that the episode "contained the final scene of Jaime Lannister’s ninth ''“Storm of Swords”'' chapter. But lots of material from that chapter hasn’t been on the show yet, so I reasoned that he has completed only eight."<ref name=FiveThirtyEight>{{citeweb|url=http://fivethirtyeight.com/datalab/hbo-game-of-thrones-book-characters/|title=How Much Source Material Does HBO’s ‘Game of Thrones’ Have Left to Work With? |last=Hickey|first=Walt|work=FiveThirtyEight|date=May 4, 2014|accessdate=May 10, 2014}}</ref> In addition to chapter 72 (Jaime IX), some of the content from this episode is also found in ''A Storm of Swords'' chapters 61, 68, and 71 (Sansa V, Sansa VI, Daenerys VI).<ref name="Storm">{{cite book |last=Martin |first=George|author-link1=George R.R. Martin|date=2000 |title=''A Storm of Swords''|location=U.K. |publisher=Voyager Books |page= |isbn=0-00-224586-8 |accessdate= }}</ref><ref name=Westeros.org>{{citeweb|url=http://www.westeros.org/GoT/Episodes/Entry/Oathkeeper/Book_Spoilers/#Book_to_Screen/|title=EP404: Oathkeeper |last=Garcia|first=Elio|last2=Antonsson|first2=Linda|work=Westeros.org|date=May 3, 2014|accessdate=August 23, 2014}}</ref> 

Theresa DeLucci, a reviewer for ''[[Tor.com]]'', notes that the episode "didn’t even take liberties with the books; it completely made up whole new stories" that do not appear in ''A Storm of Swords'', including conversations between Missandei and Grey Worm, Bran's appearance at Craster's keep, and the final White Walker scene.<ref name=Tor>{{citeweb|url=http://www.tor.com/blogs/2014/04/game-of-thrones-episode-review-oathkeeper/|title=Game of Thrones Episode Review: “Oathkeeper” |last=Delucci|first=Theresa|work=IGN Conversations|date=Apr 28, 2014|accessdate=June 24, 2014}}</ref>  Reviewers from IGN applauded the new material, noting that the scenes at Craster's keep "give Bran something to do" and hint at the nature of the White Walkers.<ref name="Goldman"/> Erik Kain, of ''[[Forbes]]'' magazine, notes these departures from the books as well, stating that the episode departed as much from the books as any episode thus far in the HBO adaptation of Martin's book series. These deviations, notes Kain, "leave both readers and newcomers to the story of Westeros and its motley band of heroes and villains entirely uncertain as to what’s coming next."<ref name=Kain>{{cite web|last1=Kain|first1=Erik|title='Game Of Thrones' Season 4, Episode 4 Review: Oathkeeper|url=http://www.forbes.com/sites/erikkain/2014/04/28/game-of-thrones-season-4-episode-4-review/|website=Reviews|publisher=Forbes.com|accessdate=20 July 2014}}</ref>

==Reception==
===Ratings===
"Oathkeeper" established a new series high in ratings, with 6.95 million people watching the premiere.<ref>{{citeweb|url=http://tvbythenumbers.zap2it.com/2014/04/29/sunday-cable-ratings-game-of-thrones-wins-night-nba-playoffs-real-housewives-of-atlanta-mad-men-devious-maids-more/258458/|title=Sunday Cable Ratings: 'Game of Thrones' Wins Night, NBA Playoffs, 'Real Housewives of Atlanta', 'Mad Men', 'Devious Maids' & More|last=Bibel|first=Sara|work=[[TV by the Numbers]]|date=April 29, 2014|accessdate=April 29, 2014}}</ref><ref>{{citeweb|url=http://tvbythenumbers.zap2it.com/2014/04/28/game-of-thrones-hits-series-high-in-total-viewers/258511/|title='Game of Thrones' Hits Series High in Total Viewers; Solid Start for 'Last Week Tonight With John Oliver'|last=Bibel|first=Sara|work=[[TV by the Numbers]]|date=April 28, 2014|accessdate=April 28, 2014}}</ref> In the United Kingdom, the episode was viewed by 1.598 million viewers, making it the highest-rated broadcast that week. It also received 0.112 million timeshift viewers.<ref>{{cite web|url=http://www.barb.co.uk/whats-new/weekly-top-10?|title=Top 10 Ratings (28 April-4 May 2014)|work=[[Broadcasters' Audience Research Board|BARB]]|accessdate=January 19, 2017}}</ref>

===Critical reception===
Like the season's other episodes, "Oathkeeper" received acclaim from critics, with [[Rotten Tomatoes]] counting 97% positive reviews from among 36. The site's consensus is that "If it's a bit more subdued than its predecessors, 'Oathkeeper' is nonetheless a rock-solid installment of Game of Thrones – one that features assured direction, strong action scenes, and intriguing plot developments."<ref>{{cite web|title=Game of Thrones: Season 4: Episode 4|url=http://www.rottentomatoes.com/m/tv_editorial/news/1929961/game_of_thrones_season_4_episode_4/|publisher=Rotten Tomatoes|accessdate=April 29, 2014}}</ref>

Eric Goldman and Roth Cornet of [[IGN]] commented on the episode being a "game changer" because it diverges from the book series more than any other ''Game of Thrones'' episode; a few of the changes include Jon's and Bran's storylines, how Daenerys conquered Meereen, and new information with regard to how White Walkers multiply their army. Goldman and Cornet stated that much of the episode feels like a spoiler for readers of the series because of the changes, including the show creators, who know how the ongoing book series will end, possibly having incorporated aspects that happen later in the books. Though Goldman and Cornet indicated that significantly diverging from the books could be detrimental to the show, they credited "Oathkeeper" with adding an element of surprise and intrigue for all viewers.<ref name="Goldman">{{citeweb|title=Game of Thrones - The Biggest Change in Oathkeeper|authors=Eric Goldman and Roth Cornet|publisher=[[IGN]]|date=April 28, 2014|accessdate=May 1, 2014|url=http://www.ign.com/videos/2014/04/28/game-of-thrones-is-the-show-now-spoiling-the-books-ign-conversation}}</ref>

Writing for ''[[The A.V. Club]]'', Todd VanDerWerff (writing for viewers who have read the books) and Erik Adams (writing for viewers who have not) both gave the episode a B.<ref name="A.V. Club Experts">{{citeweb|url=http://www.avclub.com/tvclub/game-thrones-experts-oathkeeper-203883|title=Game Of Thrones (experts): “Oathkeeper”|last=VanDerWerff|first=Todd|work=[[The A.V. Club]]|date=April 27, 2014|accessdate=April 28, 2014}}</ref><ref name="A.V. Club Newbies">{{cite web |url=http://www.avclub.com/tvclub/game-thrones-newbies-oathkeeper-203881 |title=''Game Of Thrones'' (newbies): “Oathkeeper” |work=The A.V. Club |date=April 28, 2014 |first=Erik |last=Adams |accessdate=April 28, 2014}}</ref>  VanDerWerff commented that the scenes between Jamie and Cersei "seems to truly want us to think that what happened [[Breaker of Chains|last week]] wasn’t, in any way, rape" and wondered "whether the show is going to acknowledge it at all."<ref name="A.V. Club Experts" /> Adams notes how the episode serves as a "bridge" between episodes and plotlines well under way, but that there are "thematic riches" to be found; namely, the multiple searches for justice.<ref name="A.V. Club Newbies" />

===Accolades===
The episode was nominated for Outstanding Makeup for a Single-Camera Series (Non-Prosthetic) at the [[66th Primetime Creative Arts Emmy Awards]].<ref name="emmy2014">http://deadline.com/2014/08/creative-arts-emmy-awards-2014-winners-live-819681/</ref>

{| class="wikitable sortable plainrowheaders"
|-
! Year
! Award
! Category
! Nominee(s)
! Result
! class="unsortable" | {{abbreviation|Ref.|reference}}
|-
| 2014
|scope="row"| [[66th Primetime Emmy Awards]]
|scope="row"| Outstanding Makeup for a Single-Camera Series (Non-Prosthetic)
|scope="row"| Jane Walker and Ann McEwan
| {{nom}}
|<ref name="emmy2014"/>
|-
| 2015
|scope="row"| [[Canadian Society of Cinematographers]] 
|scope="row"| TV Series Cinematography
|scope="row"| [[Robert McLachlan (cinematographer)|Robert McLachlan]]
| {{nom}}
| <ref>{{cite web |url=http://www.csc.ca/CSCawards/2015/ |title=CSC Awards 2015 |publisher=[[Canadian Society of Cinematographers]] |accessdate=October 7, 2016}}</ref>
|-
|}

==References==
{{reflist|2}}

==External links==
{{wikiquotepar|Game_of_Thrones_(TV_series)#Oathkeeper_.5B4.04.5D|Oathkeeper}}
* {{URL|1=http://www.hbo.com/game-of-thrones/episodes/4/34-oathkeeper/index.html |2="Oathkeeper"}} at [[HBO.com]]
* {{IMDb episode|2972428|Oathkeeper}}
* {{tv.com episode|game-of-thrones/oathkeeper-3025466/|Oathkeeper}}

{{Game of Thrones Episodes}}
[[Category:2014 American television episodes]]
[[Category:Game of Thrones episodes]]