{{Infobox journal
| title = Cell
| cover = [[File:Cell cover (Nov 2008).gif]]
| editor = Emilie Marcus
| discipline = [[Biology]]
| abbreviation = Cell
| publisher = [[Cell Press]]
| country =
| frequency = Biweekly
| history = 1974–present
| openaccess = After 12 months
| license =
| impact = 28.710
| impact-year = 2015
| website = http://www.cell.com/
| link1 = http://www.cell.com/current
| link1-name = Online access
| link2 = http://www.cell.com/archive
| link2-name = Online archive
| link3 = http://www.elsevier.com/wps/find/journaldescription.cws_home/621181/description
| link3-name =Journal page on Elsevier website
| JSTOR =
| OCLC = 01792038
| LCCN = 74641498
| CODEN = CELLB5
| ISSN = 0092-8674
| eISSN = 1097-4172
}}
'''''Cell''''' is a [[peer review|peer-reviewed]] [[scientific journal]] publishing research papers across a broad range of disciplines within the [[life sciences]].<ref name=about>{{cite web| url= http://www.reed-elsevier.com/OURBUSINESS/ELSEVIER/Pages/Cell.aspx |title=About ''Cell''| publisher= Reed Elsevier | archiveurl= https://web.archive.org/web/20100323044022/http://www.reed-elsevier.com/OurBusiness/Elsevier/Pages/Cell.aspx| archivedate=2010-03-23}}</ref>  Areas covered include [[molecular biology]], [[cell biology]], [[systems biology]], [[stem cell]]s, [[developmental biology]], [[genetics]] and [[genomics]], [[proteomics]], cancer research, [[immunology]], [[neuroscience]], [[structural biology]], [[microbiology]], [[virology]], [[physiology]], [[biophysics]], and [[computational biology]]. The journal was established in 1974 by [[Benjamin Lewin]]<ref name=Elsevier_index>[http://www.elsevier.com/wps/find/journaldescription.cws_home/621181/description Elsevier: ''Cell'': Home] (accessed 12 December 2008)</ref> and is published twice monthly by [[Cell Press]], an imprint of [[Elsevier]].

==History==
[[Benjamin Lewin]] founded ''Cell'' in January 1974, under the aegis of [[MIT Press]]. He then bought the title and established an independent Cell Press in 1986.<ref name="Elsevier_index"/> In April 1999, Lewin sold Cell Press to [[Elsevier]].<ref>{{cite web|url=http://www.paperpub.com.cn/admin/upload/file/2009109142854546.pdf |title=Archived copy |accessdate=2011-07-10 |deadurl=yes |archiveurl=https://web.archive.org/web/20120128213257/http://www.paperpub.com.cn/admin/upload/file/2009109142854546.pdf |archivedate=2012-01-28 |df= }}</ref>

The "Article of the Future" feature was the recipient of a 2011 PROSE Award for Excellence in Biological & Life Sciences presented by the Professional and Scholarly Publishing Division of the [[Association of American Publishers]].<ref>{{cite web|url= http://www.cell.com/cellpress/PROSE_award2011 |date= February 8, 2011 |title=Cell Press Wins Prestigious PROSE Award for Article of the Future |publisher=Elsevier|work=Press release | location= Cambridge, MA| accessdate=2013-12-14}}</ref>

==Impact factor==
According to [[ScienceWatch]], the journal was ranked first overall in the category of highest-impact journals (all fields) over 1995–2005 with an average of 161.2 citations per paper.<ref>{{cite web|first=Nancy Imelda |last=Schafer |url=http://www.in-cites.com/research/2005/october_24_2005-2.html |title=Highest-Impact Journals (All Fields), 1995-2005 |publisher=In-cites.com |date= October 24, 2005|accessdate=2013-12-14}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 28.710, ranking it second out of 289 journals in "Biochemistry & Molecular Biology" after ''[[Nature Reviews Genetics]]''.<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: All Journals |title=2015 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |accessdate= |series=Web of Science}}</ref>

==Contents and features==
In addition to original research articles, ''Cell'''s "Leading Edge" section publishes previews, minireviews, reviews, analysis articles, commentaries, essays, correspondence, and Snapshot (a reference tool presenting up-to-date tables of nomenclature, glossaries, full signaling pathways and schematic diagrams of cellular processes).<ref name=about /> Features include "PaperClips" (short conversations between a ''Cell'' editor and an author exploring the rationale and implications of research findings)<ref name=home>{{cite web |url=http://www.cell.com/ |title=Cell website}}</ref> and "PaperFlicks" (video summaries of a ''Cell'' paper).<ref name=home/><ref>{{cite web|url=https://www.youtube.com/user/cellvideoabstracts |title=Cell PaperFlicks |publisher=Youtube.com |date= |accessdate=2013-12-14}}</ref>

==Availability==
Content over 12 months old is freely accessible, starting from the January 1995 issue.<ref name=Marcus_bio>{{cite web|url=http://www.elsevier.com/wps/find/newsroom.newsroom/bio_emiliemarcus |publisher=Elsevier |title=Emilie Marcus, Executive Editor |work=Cell Press |accessdate=2008-12-12 |deadurl=yes |archiveurl=https://web.archive.org/web/20090208234806/http://www.elsevier.com:80/wps/find/newsroom.newsroom/bio_emiliemarcus |archivedate=2009-02-08 |df= }}</ref>

==Editors==
*[[Emilie Marcus]], 2003–present
*[[Vivian Siegel]], 1999–2003
*[[Benjamin Lewin]], 1974–1999

== See also ==
* [[The Hallmarks of Cancer]]

==References==
{{Reflist|2}}


==External links==
* {{Official website|http://www.cell.com/}}


{{Portal bar|Biology}}

{{DEFAULTSORT:Cell (Journal)}}
[[Category:Biology journals]]
[[Category:English-language journals]]
[[Category:Delayed open access journals]]
[[Category:Cell Press academic journals]]
[[Category:Publications established in 1974]]
[[Category:Biweekly journals]]