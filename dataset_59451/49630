{{Infobox book
| name           = The Egoist
| image          = The Egoist 1879.jpg
| caption        = First edition title page
| author         = [[George Meredith]]
| country        = England
| language       = English
| genre          = [[tragicomedy|Tragicomic]]al novel
| published      = 1879
| wikisource     = The Egoist
}}
'''''The Egoist''''' is a [[tragicomedy|tragicomic]]al [[novel]] by [[George Meredith]] published in [[1879 in literature|1879]].<ref>{{cite book |last=Meredith|first= George |author-link=|year=1879 |editor-last= |editor-first= |contribution= |title= The Egoist, A Comedy in Narrative  |edition= 1 |publisher= Kegan Paul & Co. |publication-place=London  |volume= I |url=https://archive.org/stream/egoistcomedyinna00mereuoft#page/n1/mode/2up  |accessdate= 3 June 2016 |via= Internet Archive}}</ref><ref>{{cite book |last=Meredith|first= George |author-link=|year=1913 |editor-last= |editor-first= |contribution= |title= The Egoist, A Comedy in Narrative  |edition= Revised |publisher= Charles Scribner's Sons |publication-place=New York  |volume=  |url=https://archive.org/stream/egoistcomedyinna00mereuoft#page/n1/mode/2up  |accessdate= 3 June 2016 |via= Internet Archive}}</ref>

==Synopsis==
The novel recounts the story of self-absorbed Sir Willoughby Patterne and his attempts at [[marriage]]; jilted by his first bride-to-be, he vacillates between the sentimental Laetitia Dale and the strong-willed Clara Middleton.  More importantly, the novel follows Clara's attempts to escape from her engagement to Sir Willoughby, who desires women to serve as a mirror for him and consequently cannot understand why she would not want to marry him.  Thus, ''The Egoist'' dramatizes the difficulty contingent upon being a woman in [[Victorian era|Victorian]] society, when women's bodies and minds are trafficked between fathers and husbands to cement male bonds.

==Critical response==
In an afterword by [[Angus Wilson]], ''The Egoist'' was called "the turning point in George Meredith's career."  Wilson saw Meredith as "the first great art novelist"; his afterword interprets the book as an adaptation of a stage [[comedy]], an achievement he arrogates to few English authors, who, he suggests, present only "[[farce]] or [[satire]]."{{sfn|Wilson|1963|pp=501-03}} He compliments Meredith most when he is detached from his characters, as "it is then that our laughter is most thoughtful."{{sfn|Wilson|1963|p=503}} Wilson is most taken by "the absolute truth of much of the dialogue," such as how "the way Sir Willoughby continues to speak through the answers of other characters, returning to notice their replies only when his own vein of thought is exhausted" is a "wonderful observation of human speech."{{sfn|Wilson|1963|p=508}}

In his essay "Books Which Have Influenced Me," [[Robert Louis Stevenson]] reports the following story:
"A young friend of Mr. Meredith's (as I have the story) came to him in agony.  'This is too bad of you,' he cried. 'Willoughby is me!' 'No, my dear fellow,' said the author; 'he is all of us.'"{{sfn|Stevenson|1999|p=115}}

[[E. M. Forster]] discussed the book in his lecture series ''[[Aspects of the Novel]]'', using it as an example of a "highly organized" [[Plot (narrative)|plot]].{{sfn|Forster|1954|p=87}}  Much of his discussion, however, focuses on Meredith and his popularity as an author.

More materially, Forster compliments Meredith on not revealing Laetitia Dale's changed feelings for Willoughby until she rejects him in their midnight meeting; "[i]t would have spoiled his high comedy if we had been kept in touch throughout ... in fact it would be boorish. ... Meredith with his unerring good sense here lets the plot triumph" rather than explaining Dale's character more fully.{{sfn|Forster|1954|p=92}}

Forster further compares Meredith with [[Thomas Hardy]], complimenting Hardy on his pastoral sensibilities and Meredith on his powerful plots, "[knowing] what [his] novel[s] could stand."{{sfn|Forster|1954|p=94}}

== References ==
{{Reflist}}

==External links==
{{Wikisource|The Egoist}}
{{Gutenberg|no=1684|name=The Egoist}}
* [http://librivox.org/the-egoist-by-george-meredith/ Free MP3 audiobook of The Egoist] from [http://librivox.org LibriVox]

==Sources==
* {{cite book|ref=harv|first=E. M.|last=Forster|title=Aspects of the Novel|publisher=Harvest Books (Harcourt, Brace & World)|year=1954}}
* {{cite book|ref=harv|first=George|last=Meredith|author2=afterword by Angus Wilson|title=The Egoist|publisher=The New American Library of World Literature (Signet Books)|year=1963|pages=501–508}}
* {{cite book|ref=harv|first=Robert Louis|last=Stevenson|title=R.L. Stevenson on Fiction|publisher=Edinburgh University Press|year=1999}}
{{George Meredith}}

{{DEFAULTSORT:Egoist, The}}
[[Category:1879 novels]]
[[Category:English novels]]
[[Category:Victorian novels]]
[[Category:Novels by George Meredith]]