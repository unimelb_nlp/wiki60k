{{good article}}
{{Use British English|date=October 2015}}
{{Use dmy dates|date=October 2015}}
'''Caffo''' was a sixth-century Christian in [[Anglesey]], north [[Wales]], who is venerated as a saint and martyr.  The son of a king from northern Britain who took shelter in Anglesey, Caffo was a companion of [[Cybi|St Cybi]], and is mentioned as carrying a red-hot coal in his clothes to Cybi without his clothes getting burnt.  After leaving Cybi, Caffo was killed by shepherds in the south of Anglesey, possibly acting in retaliation for insults Caffo's brother had paid to the local ruler. The area where he died has a village, Llangaffo, named after him, as well as the parish church of [[St Caffo's Church, Llangaffo|St Caffo, Llangaffo]].

==Life and martyrdom==
[[File:St Caffo's Church, Llangaffo - geograph.org.uk - 1873345.jpg|thumb||alt=St Caffo's Church, a rubble masonry and limestone church in Llangaffo, is named after St Caffo |[[St Caffo's Church, Llangaffo|St Caffo's Church]] is located not far from his place of death near [[Llangaffo]].]]
Little is known for certain about Caffo; his dates of birth and death are not given in the sources. He is said to have been one of the sons of [[Caw (saint)|St Caw]], a king in northern Britain who lost his lands and sought safety with his family in [[Anglesey]]; the ruler [[Maelgwn Gwynedd]] gave him land in the north-east of the island, in the district known as [[Twrcelyn]].<ref name=Caw/> Other relatives of Caffo included his uncles [[Iestyn (saint)|St Iestyn]] and [[Cyngar of Llangefni|St Cyngar]] (brothers of Caw), his sister [[Cwyllog|St Cwyllog]] and various brothers including [[Gildas|St Gildas]] (although the number of his siblings varies from 10 to 21 in different manuscripts).<ref name=Caw>Baring-Gould, pp. 92–94.</ref><ref>Baring-Gould, p. 55.</ref><ref name=Caffo>Baring-Gould, pp. 49–51.</ref>

Caffo was a companion and cousin of [[Cybi|St Cybi]], a Christian from Cornwall who was active in the mid-6th century.  Cybi established himself in Anglesey within [[Caer Gybi (fort)|a disused Roman fort]] in what is now called [[Holyhead]]: the town's Welsh name is ''{{lang|cy|Caergybi}}'', or "Cybi's fort").<ref>{{cite web| url=http://yba.llgc.org.uk/en/s-CYBI-APS-0490.html|title=Cybi (fl. 550), saint|last=Lloyd|first=John Edward|authorlink=John Edward Lloyd|accessdate=27 January 2011|year=2009|work=[[Welsh Biography Online]]|publisher=[[National Library of Wales]]}}</ref> Caffo is mentioned in connection with Cybi in a manuscript written in about 1200, which contains two accounts of Cybi's life.<ref name=Caffo/><ref>{{cite book|title=The Book of Llandaf and the Norman church in Wales|url=https://books.google.com/books?id=so7R-Z1VB1wC&pg=PA124|page=133|last=Davies|first=John Reuben|year=2003|publisher=Boydell Press|isbn=978-1-84383-024-5}}</ref>  Caffo is not mentioned in the accounts of Cybi's life until an incident when he was sent to fetch fire from a blacksmith.  He returned to Cybi carrying a red-hot coal in his clothes, which were not burnt.<ref name=Caffo/><ref>{{cite book|title=Lives of the Cambro British saints|page=500|url=https://archive.org/stream/livescambrobrit00reesgoog#page/n532/mode/2up|year=1853|last=Rees|first=W. J.|authorlink=William Jenkins Rees|publisher=Longman}}</ref>

At some point, Cybi and Caffo parted company, possibly because of a disagreement between them, but possibly because his brother Gildas had insulted Maelgwn, who then forced Cybi to dismiss Caffo – both versions appear in the manuscript accounts. Thereafter, Caffo moved towards the south of Anglesey, where he was killed by shepherds from the area now called [[Newborough, Anglesey|Newborough]], perhaps avenging the insult on their king.<ref name=Caffo/>

==Commemoration==
The area of Caffo's death became known at some point as [[Llangaffo]], and a church was established there: the Welsh word "''{{lang|cy|[[llan (placename element)|llan]]}}''" originally meant "enclosure" and then "church", and "-gaffo" is a [[Colloquial Welsh morphology|modified form]] of the saint's name.<ref name=Caffo/><ref>{{cite web|url=http://www.bbc.co.uk/wales/whatsinaname/sites/themes/pages/religion.shtml|title=Religion and creed in place names|publisher=[[BBC Wales]]|accessdate=24 June 2010}}</ref> It is thought that there may have at one point been a monastery in this location, known as "Merthyr Caffo" (''Merthyr'' being the Welsh word for "martyr").<ref name=Anglesey>{{cite book|title=Anglesey Churches|pages=99–100|isbn=1-84527-089-4|publisher=Carreg Gwalch|last=Jones|first=Geraint I. L.|year=2006}}</ref> Caffo is venerated as a saint, although he was never canonized by a pope: as the historian Jane Cartwright notes, "In Wales sanctity was locally conferred and none of the medieval Welsh saints appears to have been canonized by the Roman Catholic Church".<ref>{{cite journal|journal=Medium Aevum |title=Dead virgins: feminine sanctity in medieval Wales|date=Spring 2002|url=http://findarticles.com/p/articles/mi_hb6408/is_1_71/ai_n28930393/?tag=content;col1|accessdate=26 August 2011|last=Cartwright|first=Jane|publisher=The Society for the Study of Medieval Languages and Literature}}</ref>

It is uncertain when the name "Llangaffo" was first used or when the first church was established here, but it was before 1254, when the church and community were recorded in the [[Norwich Taxation]] (a national survey of church names and property).<ref name=Caffo/> There is still [[St Caffo's Church, Llangaffo|a church dedicated to Caffo]] in the village, used for worship by the [[Church in Wales]].<ref>{{cite web|url=http://www.churchinwales.org.uk/parishholding/bangor/b620-en/churches-en/st-caffo-llangaffo_-en/church_view|title=St Caffo's Church, Llangaffo|publisher=[[Diocese of Bangor]]|accessdate=27 January 2011}}</ref>

Caffo is reported to have had a bubbling "holy well" in the area, called ''Crochan Caffo'' ("Caffo's cauldron") or ''Ffynnon Caffo'' ("Caffo's well").  Parents would offer fowls to be eaten by the attendant priest, in order to stop their children from peevishness. A nearby farm is still named after the well, although the well itself has been lost.<ref name=Caffo/><ref>The farm can be seen on maps at {{coord|53.199002|-4.332492|display=inline}}</ref>

==See also==
Other Anglesey saints commemorated in local churches include:
*[[Cwyllog|St Cwyllog]] at [[St Cwyllog's Church, Llangwyllog]]
*[[Elaeth|St Eleth]] at [[St Eleth's Church, Amlwch]]
*[[Iestyn (saint)|St Iestyn]] at [[St Iestyn's Church, Llaniestyn]]
*[[Peulan|St Peulan]] at [[St Peulan's Church, Llanbeulan]]
*[[Tyfrydog|St Tyfrydog]] at [[St Tyfrydog's Church, Llandyfrydog]]

==References==
;Notes
{{reflist|2}}
;Bibliography
*{{cite book|url=https://archive.org/stream/livesbritishsai04fishgoog#page/n58/mode/1up|title=The lives of the British Saints: the Saints of Wales and Cornwall and such Irish Saints as have dedications in Britain (volume 2)|authorlink=Sabine Baring-Gould|first=Sabine |last=Baring-Gould|year=1907|publisher=[[Honourable Society of Cymmrodorion]]}}

{{DEFAULTSORT:Caffo}}
[[Category:Medieval Welsh saints]]
[[Category:6th-century Christian martyrs]]
[[Category:Year of birth unknown]]
[[Category:6th-century Welsh people]]
[[Category:6th-century births]]
[[Category:British saint stubs]]
[[Category:Welsh Roman Catholic saints]]