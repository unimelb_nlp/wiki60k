{{ Infobox book
|
| image     = 
| caption = 
| name          = Frank Freeman's Barber Shop: A Tale
| author        = [[Baynard Rush Hall]]
| illustrator   = 
| cover_artist  = 
| country       = United States
| language      = English
| genre         = [[Anti-Tom literature|Plantation literature]]
| publisher     = Charles Scribner Publishers
| release_date  = [[1852 in literature|1852]]
| media_type    = Print ([[Hardcover]] & [[Paperback]]) & [[E-book]]
| pages         = c.300 pp (May change depending on the publisher and the size of the text)
}}

'''''Frank Freeman's Barber Shop''''' is an [[1852 in literature|1852]] [[Anti-Tom literature|plantation fiction]] novel written by [[Baynard Rush Hall]].

== Overview ==

''Frank Freeman's Barber Shop'' is an example of the numerous anti-Tom novels produced in the [[Southern United States]] in response to the publication of ''[[Uncle Tom's Cabin]]'' by [[Harriet Beecher Stowe]], which was criticised as inaccurately depicting plantation life as well as the relationship between slaveholders and their slaves.<ref>http://www.enotes.com/uncle-toms/</ref>

Hall's novel is among the earliest examples of the genre, and focusses on the criticisms of [[Abolitionism in the United States|Abolitionism]] and how it may easily be exploited - a concept later visited in ''[[The Planter's Northern Bride]]'' by [[Caroline Lee Hentz]] ([[1854 in literature|1854]]).<ref>http://www.iath.virginia.edu/utc/proslav/hentzhp.html</ref>

== Plot ==

The story focuses on a slave named '''Frank''' (later '''Frank Freeman'''), who is convinced to run away from his peaceful life on a southern plantation by "[[Philanthropy|philanthropists]]" (Hall's term for [[Abolitionism in the United States|abolitionists]]), having been promised that freedom would also bring a prestigious career.

When Frank comes to the end of his journey, however, he realises that he has been deceived: his prestigious career is nothing more than running a seedy [[Barber|barber shop]] frequented by his new abolitionist masters, and is paid meagre wages for his work.

However, Frank is soon discovered by members of the [[American Colonization Society]], who rescue Frank from his predicament, and pay for his passage back to [[Liberia]], his homeland, where he can finally live in peace.

== In other works ==

*Chapter VII of ''Freeman'' - entitled ''The Death of Dinah'' - is strongly echoed in a later anti-Tom novel: ''[[Uncle Robin, in His Cabin in Virginia, and Tom Without One in Boston]]'' by J.W. Page ([[1853 in literature|1853]]), in which another character also named Dinah passes away as a redeemed [[Christianity|Christian]], as does the character of Dinah in Hall's novel.<ref>http://www.iath.virginia.edu/utc/proslav/hallhp.html</ref> It should be noted, however, that the death of a Christianized slave was a frequent cliche used in anti-Tom novels, and another example of this is the death of Aunt Phillis in Eastman's ''[[Aunt Phillis's Cabin]]''.<ref>Chapter 26 of ''Aunt Phillis's Cabin'' - M.H. Eastman (1852)</ref>
*The 1853 novel ''[[Liberia; or, Mr. Peyton's Experiments]]'' bears some similarities to ''Freeman'', particularly as both feature ex-slaves who are sent to [[Liberia]] after leading miserable lives in the north.<ref>http://www.iath.virginia.edu/utc/proslav/halehp.html</ref>

== References ==

{{reflist}}

== External links ==
{{Portal|United States|Novels}}
*[http://www.iath.virginia.edu/utc/proslav/hallhp.html ''Frank Freeman's Barber Shop'' at the University of Virginia]

{{Uncle Tom's Cabin}}

[[Category:1852 novels]]
[[Category:Anti-Tom novels]]
[[Category:19th-century American novels]]