{{Infobox journal
| title  = Nezavisimiy Psikhiatricheskiy Zhurnal
| cover  = 
| editor = [[Yuri Savenko]]
| discipline = [[Psychiatry]]
| former_names  = 
| peer-reviewed = 
| language = Russian
| abbreviation = 
| publisher = [[Independent Psychiatric Association of Russia]]
| country = [[Russian Federation]]
| frequency = Quarterly
| history = 1991–present
| openaccess = 
| license = 
| impact = 
| impact-year = 
| website = http://www.npar.ru/journal/
| link1  = http://www.npar.ru/journal/contents.htm 
| link1-name = Online access
| link2  = http://www.npar.ru/journal/arch.htm
| link2-name = Online archives
| JSTOR  = 
| OCLC  = 224673640
| LCCN  = 
| CODEN  = 
| ISSN  = 1028-8554
| eISSN  = 
}}
'''''Nezavisimiy Psikhiatricheskiy Zhurnal''''' ({{lang-ru|Незави́симый психиатри́ческий журна́л,'' The Independent Psychiatric Journal''}}) is a Russian [[peer-reviewed journals|peer-reviewed]] [[scientific journal]] which covers clinical practice, issues of modern [[psychiatry]], and results of studies by Russian and foreign psychiatrists.<ref name="Elibrary.ru">{{cite web|title=Независимый психиатрический журнал|url=http://elibrary.ru/title_about.asp?id=8922|publisher=Научная электронная библиотека “Elibrary.ru”|accessdate=22 July 2011}}</ref> The journal is the official publication of the [[Independent Psychiatric Association of Russia]].<ref name="Elibrary.ru"/><ref name="Journal">{{cite web |author= |date= |url=http://www.npar.ru/journal/ |title=About ''Nezavisimiy Psikhiatricheskiy Zhurnal'' |publisher=The [[Independent Psychiatric Association of Russia]] |accessdate=22 July 2011}}</ref> 

The [[Higher Attestation Commission]] of the [[Ministry of Education and Science of the Russian Federation]] has included ''Nezavisimiy Psikhiatricheskiy Zhurnal'' in the list of leading journals and publications.<ref name="Journal"/>

The [[editor-in-chief]] is [[Yuri Savenko]].<ref name="Journal"/> Lyubov Vinogradova, a member of the journal’s [[editorial board]], works as editorial consultant of the Russian version of the journal ''[[World Psychiatry (journal)|World Psychiatry]]'' as well.

The journal is intended for doctors, psychologists, and lawyers, but also publishes materials interesting for lay persons and republishes works by Russian and Western doctors and philosophers which turned out to be inaccessible for a number of reasons.<ref name="НЗ">{{cite journal|title=Независимая психиатрическая ассоциация России|journal=Неприкосновенный запас|year=2001|volume=№ 5|issue=19|url=http://magazines.russ.ru/nz/2001/5/nez-pr.html|accessdate=22 July 2011}}</ref>

There is [[Open access (publishing)|open access]] to most issues of the journal.<ref name="Archive">{{cite web |author= |date= |url=http://www.npar.ru/journal/contents.htm |title=Archive of ''Nezavisimiy Psikhiatricheskiy Zhurnal'' |publisher=The [[Independent Psychiatric Association of Russia]] |accessdate=22 July 2011}}</ref>

In recent years, the journal publishes papers that force restrictions on patients’ rights.<ref name=Ustinov1>{{cite journal|author=Ustinov, A. [A. Устинов]|script-title=ru:К вопросу о законности правоприменительной практики в части порядка установления диспансерного наблюдения в отношении лиц, страдающих психическими расстройствами|trans_title=Towards the legality of the law enforcement practice as to the order for establishing dispensary supervision over persons with mental disorders|journal=Nezavisimiy Psikhiatricheskiy Zhurnal [The Independent Psychiatric Journal]|year=2012|issue=№ 4|pages=57–61|url=http://www.npar.ru/journal/2012/4/08-ustinov.htm|accessdate=18 February 2014|language=Russian}}</ref><ref name=Ustinov2>{{cite journal|author=Ustinov, A. [A. Устинов]|script-title=ru:Может ли быть ограничено право пациентов, находящихся в психиатрических стационарах, на ежедневную прогулку?|trans_title=Can the right of patients in psychiatric hospitals to a daily walk be restricted?|journal=Nezavisimiy Psikhiatricheskiy Zhurnal [The Independent Psychiatric Journal]|year=2012|issue=№ 3|pages=48–49|url=http://npar.ru/journal/2012/3/08-ustinov.htm|accessdate=18 February 2014|language=Russian}}</ref>  

==See also==
* [[List of psychiatry journals]]

== References ==
{{reflist}}

==External links==
* {{Official website|http://www.npar.ru/journal/}}
* [http://flogiston.ru/magazine/npj Russian psychologists recommend: ''Nezavisimiy Psikhiatricheskiy Zhurnal''] — information about the journal on the Flogiston website {{ru icon}}

[[Category:1991 establishments in the Soviet Union]]
[[Category:1991 establishments in Russia]]
[[Category:Publications established in 1991]]
[[Category:Psychiatry journals]]
[[Category:Quarterly journals]]
[[Category:Russian-language journals]]
[[Category:Academic journals associated with learned and professional societies]]
[[Category:Independent Psychiatric Association of Russia]]