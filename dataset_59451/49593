{{Infobox book
| name = The Confidence-Man
| image = Confidence Man 1857 First Edition Title Page.jpg
| caption = First edition title page
| alt = 
| author = [[Herman Melville]]
| illustrator = 
| cover_artist = 
| country = United States
| language = English
| subject = 
| genre = [[Satire]]
| published = 1857 {{small|(Dix, Edwards & Co.)}}
| media_type = Print
| awards = 
| isbn = 
| oclc = 
| dewey = 
| congress = 
| preceded_by = [[The Piazza Tales]]
| followed_by = Battle-Pieces and Aspects of the War
}}

'''''The Confidence-Man: His Masquerade''''' is the ninth book and final novel by American writer [[Herman Melville]], first published in New York in 1857. The book was published on April 1, the exact day of the novel's setting. ''The Confidence-Man'' portrays a ''[[Canterbury Tales]]''–style group of [[steamboat]] passengers whose interlocking stories are told as they travel down the [[Mississippi River]] toward [[New Orleans]]. Scholar Robert Milder notes: "Long mistaken for a flawed novel, the book is now admired as a masterpiece of irony and control, though it continues to resist interpretive consensus."<ref>Milder (1988), 440</ref> After the novel's publication, Melville turned from professional writing and became a professional lecturer, mainly addressing his worldwide travels, and later for nineteen years a federal government employee.

==Analysis==
[[File:Houghton bMS Am 188 (365) - Melville Confidence Man manuscript.jpg|thumb|left|Manuscript fragment from Chapter 14 of ''The Confidence-Man''.]]
The novel's title refers to its central character, an ambiguous figure who sneaks aboard a Mississippi steamboat on [[April Fool's Day]]. This stranger attempts to test the confidence of the passengers, whose varied reactions constitute the bulk of the text. Each person including the reader is forced to confront that in which he places his [[trust (emotion)|trust]].

''The Confidence-Man'' uses the Mississippi River as a metaphor for those broader aspects of American and human identity that unify the otherwise disparate characters.{{citation needed|date=January 2017}} Melville also employs the river's fluidity as a reflection and backdrop of the shifting identities of his "confidence man".{{citation needed|date=January 2017}}

The novel is written as cultural satire, [[allegory]], and metaphysical treatise, dealing with themes of [[sincerity]], [[identity (social science)|identity]], [[morality]], [[religiosity]], [[economic materialism]], [[irony]], and [[Cynicism (contemporary)|cynicism]]. Many critics have placed ''The Confidence-Man'' alongside Melville's ''[[Moby-Dick]]'' and "[[Bartleby, the Scrivener]]" as a precursor to 20th-century literary preoccupations with [[nihilism]], [[existentialism]], and [[absurdism]].

Melville's choice to set the novel on April Fool's Day underlines the work's satirical nature and reflects Melville's [[worldview]], once expressed in a letter to his friend Samuel Savage: "It is—or seems to be—a wise sort of thing, to realise that all that happens to a man in this life is only by way of joke, especially his misfortunes, if he have them. And it is also worth bearing in mind, that the joke is passed round pretty liberally & impartially, so that not very many are entitled to fancy that they in particular are getting the worst of it."<ref>Lynn Horth, ed. ''Correspondence.'' The Writings of Herman Melville: The Northwestern-Newberry Edition, Vol. 14, p. 203 (Letter of August 24, 1851). Evanston, IL and Chicago: Northwestern University Press and The Newberry Library, 1993. ISBN 0-8101-0995-6</ref>

The work includes presumed satires of 19th century literary figures: Mark Winsome is based on [[Ralph Waldo Emerson]] while his "practical disciple" Egbert is [[Henry David Thoreau]]; Charlie Noble is based on [[Nathaniel Hawthorne]]; [[Edgar Allan Poe]] inspired a beggar in the story.<ref>Delbanco, Andrew. ''Melville, His World and Work''. New York: Alfred A. Knopf, 2005: 248. ISBN 0-375-40314-0</ref>

==Adaptations==
The novel was turned into an opera by [[George Rochberg]]; it was premiered by the [[Santa Fe Opera]] in 1982, but was not held to be a success.<ref>[https://books.google.com/books?id=vecCAAAAMBAJ&pg=PA79&lpg=PA79&dq=%22The+Confidence-Man%22+opera&source=bl&ots=dZGD4zruL-&sig=4lFfcLrxU4QJ3uPwmMwivqUfefc&hl=en&ei=yLnJTNydLYGglAft7aWIAg&sa=X&oi=book_result&ct=result&resnum=4&ved=0CCEQ6AEwAw#v=onepage&q=%22The%20Confidence-Man%22%20opera&f=false "Lost in the Desert"], ''New York Magazine'', August 23, 1982</ref>

==References==
{{Reflist}}

== Sources ==
* Milder, Robert. (1988). "Herman Melville." ''Columbia Literary History of the United States''. Gen. Ed. Emory Elliott. New York: Columbia University Press. ISBN 0-231-05812-8

==External links==
{{Wikiquote}}
{{wikisource}}
* {{gutenberg|no=21816|name=The Confidence-Man}}
* {{librivox book | title=The Confidence-Man | author=Herman Melville}}
* [http://www.melville.org/hmconman.htm Critical reaction to and a publishing history of ''The Confidence-Man: His Masquerade''] from [http://www.melville.org/melville.htm The Life and Works of Herman Melville]
* [http://etext.lib.virginia.edu/toc/modeng/public/MelConf.html Online text of the novel] from the Electronic Text Center, University of Virginia Library

{{Herman Melville}}

{{DEFAULTSORT:Confidence-Man, The}}
[[Category:1857 novels]]
[[Category:Novels by Herman Melville]]
[[Category:19th-century American novels]]
[[Category:April Fools' Day]]
[[Category:Novels adapted into operas]]