{{Use dmy dates|date=June 2014}}
{{Infobox tunnel
|name         = Bramhope Tunnel
|image        = Bramhope Tunnel north portal 1a.jpg
|caption      = Bramhope Tunnel north portal
|line         = [[Harrogate Line]]
|location     = [[Horsforth]]–[[Weeton, North Yorkshire|Weeton]]
|coordinates  = 53°53'23"N. 1°36'45"W.
|system       = 
|status       = 
|start        = 
|end          = 
|stations     = 
|startwork    = 1845
|opened = 1849
|close        = 
|owner        = [[Network Rail]]
|operator     = [[Northern (train operating company)|Northern]]
|traffic      = 
|character    = 
|construction =
|length       = {{convert|2.138|mi|km}}<ref name="Steamindex1929">{{cite web|url=http://www.steamindex.com/locomag/lcwr35.htm|title=Locomotive Railway Carriage & Wagon Review|date=1929|work=Volume 35: The Bramhope Tunnel, L.N.E. Ry. 172. illus. (and Supplement)|accessdate=2 April 2010}}</ref>
|linelength   = 
|tracklength  = 
|notrack      = 
|gauge        = [[Standard gauge]]; [[double track]]
|el           = 
|speed        = {{convert|60|mph|km/h}}
|hielevation  = 
|lowelevation = 
|height       = {{convert|25|ft|m}}
|grade        = 1 in 94 (0.01%)
}}

'''Bramhope Tunnel''' is on the [[Harrogate Line]] between [[Horsforth railway station|Horsforth station]] and the [[Arthington Viaduct]] in [[West Yorkshire]], England. Services through the railway tunnel are operated mainly by [[Northern (train operating company)|Northern]]. The tunnel was constructed during 1845&ndash;1849 by the [[Leeds and Thirsk Railway]]. It is notable for its {{convert|2.138|mi|km|adj=on}} length and its Grade II [[listed building|listed]], [[crenellation|crenellated]] north portal. The deaths of 24 men who were killed during its construction are commemorated in [[Otley]] churchyard by a monument that is a replica of the tunnel's north portal.

[[Thomas Grainger]] was the engineer for the line and James Bray the contractor. Two sighting towers were erected and 20 [[shaft mining|shafts]] sunk along the tunnel's line. Men excavated rock from the shaft faces until the shafts were connected and the tunnel was completed in 1848. Thousands of [[navvy|navvies]] lived locally in temporary [[bothy|bothies]] with their families, and worked in dangerous and wet conditions to facilitate the grand opening in 1849.

== History ==
[[File:Bramhope Tunnel south portal.jpg|thumb|left|South portal in 2009]]
In the mid-1840s [[Railway Mania]]{{sfn|Baughn|1969|p=13|ps=}} was taking hold and railway companies competed with each other to bring forward schemes to access [[Wharfedale]]. Most of these schemes did not come to fruition but the [[Leeds and Thirsk Railway]] Company's proposal, a counter to [[George Hudson]]'s "megalomania",{{sfn|Seals|1964|p=49|ps=}} to build a line from [[Leeds]] to [[Thirsk]] received approval in an Act of Parliament in 1845.{{sfn|Baughn|1969|p=14|ps=}} The line would open up trade in Leeds to the [[North East England|North East]] and access lower [[Wharfedale]]. Among the several major obstacles on the route was the ridge between [[Airedale]] and Wharfedale requiring a long tunnel between [[Horsforth]] and [[Arthington]] under [[Bramhope]] village.<ref name="tunnel">{{cite web|url=http://www.bramhope.org/brtunnel.htm |title=Bramhope Tunnel |last=Sunderland |first= Philip |publisher=Bramhope Parish Council |accessdate=14 July 2009 |deadurl=yes |archiveurl=https://web.archive.org/web/20090427122730/http://www.bramhope.org/brtunnel.htm |archivedate=27 April 2009 }}</ref> The company appointed [[Thomas Grainger]] as engineer for the project{{sfn|Baughn|1969|p=14|ps=}}<ref name="listing" /> and James Bray was the contractor for the Bramhope contract. Bray, of Black Bull Street in Leeds, was originally a brass and iron [[foundry|founder]].{{sfn|Baughn|1969|p=48|ps=}}<ref name="tunnel"/>

Water was taken at first from the town well opposite St Giles Church, but the excessive demand diminished the supply and spoiled its quality. The tunnellers' water was then pumped from a site near the Dyneley Arms crossroads. At the same time the tunnel was draining away the local farmers' natural water supply and the source of Bramhope town well. Litigation on this subject continued for some years.<ref name="tunnel" /> To mitigate the situation, a public [[water supply|waterworks]] scheme with a [[reservoir]] and an [[aqueduct (watercourse)|aqueduct]] was proposed but not implemented.<ref name="bradfordarchive">{{cite web |url= http://archive.thisisbradford.co.uk/2003/9/4/108484.html |title=Tunnel vision was such a drain on cash and life: from the Telegraph & Argus, first published Thursday 4th Sep 2003.|publisher=Bradford & District archive|accessdate=2 April 2010}}</ref>

The tunnel was planned to be 3,344 yards in length but during construction it was extended to 3,743 yards.{{sfn|Seals|1964|p=49|ps=}} The cost of building the whole line was estimated to be £800,000 but the final total rose to £2,150,313 ({{Inflation|UK|2150313|1849|r=-4|fmt=eq|cursign=£}}){{Inflation-fn|UK}} by 1849 because of costs incurred for labour, unforeseen extra costs for tunnelling at Bramhope and work in Leeds and on the [[Arthington Viaduct]]. At a shareholders meeting in September 1848 it was reported that only 100 yards of new ground were left to be penetrated and Bray stated it would be possible to run a locomotive through the tunnel in the following May.{{sfn|Baughn|1969|p=56|ps=}} 

Work on the tunnel cost the lives of 24 men.<ref name="tunnel"/> The grand opening was 9 July 1849, a week later than intended, but the first train, full of Leeds and Thirsk railway officials, pulled by Bray's [[locomotive]] ''Stephenson'', went through a few weeks earlier on 31 May.<ref name="tunnel" /> The railway was opened to the public on 10 July.{{sfn|Baughn|1969|p=57|ps=}} When built it was the third longest rail tunnel in the country.{{sfn|Bairstow|1989|p=3|ps=}} The Leeds and Thirsk Company was renamed the [[Leeds Northern Railway]] shortly after the line was completed.

Since 2016 most services on the line through the tunnel are operated by the [[Northern (train operating company)|Northern franchise]] on behalf of [[Rail North]] which represents the Local Transport Authorities in West and North Yorkshire and other parts of the North of England.<ref>{{cite web|url=http://www.railnorth.org/franchises/introduction/|title= Introduction|publisher=Rail North|accessdate=22 February 2017}}</ref>

==Construction==
[[File:Bramhope 046.jpg|thumb|right|upright|Sighting tower with horses showing relative height]]
Two sighting towers were built at a cost of £140{{sfn|Seals|1964|p=49|ps=}} for the surveyors to keep the line true, then from 20 October 1845 twenty [[shaft mining|shafts]] were sunk to enable tunnelling.  Tunnelling started after the [[cornerstone|foundation stone]] was laid at the bottom of No. 1 airshaft in July 1846. The separate working faces were joined into a single tunnel on 27 November 1848, and work was completed in summer 1849.<ref name="tunnel" />

The four shafts retained for ventilation cost £35,000. The shaft north of Otley Road is 240 feet deep, the one behind Park House is 239 feet, Camp House Farm 204 feet and the one nearest to Horsforth station is 175 feet deep.{{sfn|Seals|1964|p=49|ps=}} The ventilation shafts measure {{convert|40|ft|m}} by {{convert|30|ft|m}} – wider than the tunnel.<ref Name=timelines/> The finished tunnel is 2&nbsp;miles, 243&nbsp;yd or {{convert|2.138|mi|km}} long; {{convert|25.5|ft|m}} wide by {{convert|25|ft|m}} high. It is a [[double track]] tunnel, with a [[slope|gradient]] of 1 in 94 (0.01%) from Horsforth towards [[Arthington]]. The line enters and leaves the tunnel on a curve.{{sfn|Seals|1964|p=49|ps=}}

===Working conditions===
Work was carried out by up to 2,300 [[navvy|navvies]] and 400 horses were brought in for the work. The workforce included 188 [[quarry]]men, 102 [[stonemasonry|stonemasons]], 732 tunnel men, 738 labourers and 18 [[carpentry|carpenters]]. Each day around 2150 wagon loads of rock and earth was removed from the workings to be tipped on the Wharfe embankment leading to the [[Arthington Viaduct]].{{sfn|Baughn|1969|p=48|ps=}}

Men were lowered by bucket down the airshafts to dig by [[lantern|candlelight]]. They were paid £1.50 per week to shovel 20 tons (20.32 tonnes) of rock and earth per 12–hour shift, seven days a week. Conditions were constantly wet, with foul air and [[gunpowder]] fumes and the danger of roof–collapse.<ref name="tunnel" /> The tunnel cuts through hard sandstone, shale and clay, and there are seven major faults in the rock near the centre point.<ref name="timelines">{{cite web|url=http://www.engineering-timelines.com/scripts/engineeringItem.asp?id=571|title=Bramhope Tunnel|year=2009|publisher=Engineering Timelines|accessdate=19 July 2009}}</ref> The work was dangerous because the rock at the Horsforth end was difficult to [[rock blasting|blast]], and there was frequent flooding and [[subsidence]]. About 1,563,480,000 gallons (7,107,580,080 litres) of water were pumped out of the workings between 1845 and 1849. Metal sheets had to be used to divert water inside the tunnel.

===Living conditions===
[[File:Bramhope 052.jpg|thumb|left|[[navvy|Navvies]] lived in [[bothy|bothies]] along the line of the tunnel, near the [[ventilation shaft]]s]]
For four years the workmen, some of whom brought their families, lived in 300 temporary wooden [[bothy|bothies]] either in a field alongside the offices and workshops, opposite the cemetery, or elsewhere along the line of the tunnel. Day– and night–shift workers lived up to 17 per hut [[hot racking|taking turns]] to use the beds in unsanitary conditions.{{sfn|Seals|1964|p=42|ps=}}

Workers' children overwhelmed the village school. It had been built by the [[Township (England)|township]] [[Copyhold|copyholders]] and [[Customary freehold|freeholders]] on Eastgate in 1790.{{sfn|Seals|1964|p=42|ps=}}<ref name="BramhopeSeals2000">{{cite web|url=http://www.bramhope.org/brhistry.htm|title=Bramhope.org (Bramhope & Carlton Parish Council)|last=Seals|first=W.F.|date=10 October 2000|publisher=A Brief History of Bramhope (source: "A History of the Township of Bramhope")|accessdate=2 April 2010}}</ref> There were originally 30 children but their number increased fourfold, and with a grant of £100 from the railway company the school building was enlarged to accommodate them.{{sfn|Baughn|1969|p=51|ps=}}{{sfn|Seals|1964|p=42|ps=}} The workers and their families used St Ronan's Methodist Chapel in Bramhope and the [[Methodism|Methodist]] Chapel at [[Pool-in-Wharfedale]]. The Leeds Mission spread bibles and tracts to families who lived in the bothies.<ref name="tunnel" />

Many navvies had been farm labourers from the [[Yorkshire Dales]], North East England and [[the Fens]], or had come for work from Scotland and Ireland.<ref name="tunnel" /> Drunkenness and fighting was such that Jos Midgeley, a railway police inspector, was hired for £1.25 per week to keep order. At one time he was attacked by a group of men, and at Wescoe Hill, two miles away on the opposite side of the River Wharfe, a riot occurred when the contractors tried to cut off the beer supply to keep the men sober enough to work.{{sfn|Baughn|1969|p=51|ps=}}

== What is visible today ==
[[File:Bramhope Tunnel north portal with train.jpg|thumb|right|North portal in 2007]]
Four of the twenty construction shafts were retained as [[ventilation shaft]]s.<ref Name=timelines/>  One of the two sighting towers, a tall, cylindrical sandstone structure, two metres in diameter with four vertical slits near the top and flat coping stones is still standing in the field opposite Bramhope cemetery.<ref name="tower">{{NHLE|num=1253379|desc= Sighting Tower  |accessdate= 8 July 2014}}</ref> The other one, now demolished, was behind Dyneley Hall. About {{convert|250000|cuyd|m3}} of sandstone and shale [[earthworks (engineering)|spoil]] was tipped close to the ventilation shafts along the line of the tunnel. One of the tips is in an area around the scout hut north of Otley Road through to the Knoll, another is south of Breary Lane, one is in a field opposite the cemetery and another near None-go-Byes Farm.{{sfn|Seals|1964|p=49|ps=}}

The sandstone sighting tower, the north and south portals and the retaining walls to the south portal are all [[Listed building|Grade II listed structures]]. The portals are on [[Network Rail]] land with no public access.{{efn|The north portal can be seen at a distance from the nearby [[Rights of way in England and Wales|public footpath]], and glimpsed from the Leeds–bound train when entering the tunnel. The south portal has no public access as it is in a cutting with no easy viewing point, and is difficult to glimpse from a train, which can now go through at {{convert|60|mph|km/h}}.}} The southern [[portal (architecture)|portal]] at the Horsforth end is a plain sandstone horseshoe-shaped arch with rusticated [[voussoir]]s below a [[cornice]] and a [[parapet]].<ref name="South">{{NHLE|num=1239959|desc=Portal to south entrance of Bramhope railway tunnel |accessdate=8 July 2014}}</ref> It is approached by a slightly curved 300-metre cutting faced with sandstone retaining walls. They have a concave [[Batter (walls)|batter]], slightly-projecting [[Pier (architecture)|piers]] at regular intervals and are topped with square [[Coping (architecture)|coping stones]].<ref name="Walls">{{NHLE|num=1261803|desc=Retaining walls of railway cutting extending southwards from south portal of Bramhope tunnel |accessdate=8 July 2014}}</ref>

The [[Gothic revival|Gothic]] north portal is castellated, and after it was finished, was lived in for a while by railway workers.<ref name="timelines"/> It is built of rock–faced sandstone and has three side towers with [[turret]]s.  The [[Keystone (architecture)|keystone]] on its horseshoe–shaped archway features a portrait of a bearded man. Its crenellated parapet has a carved [[cartouche]] in the centre featuring a wheatsheaf, [[wool|fleece]] and fish.<ref name="listing">{{NHLE|num=1253370|desc= Portal to north entrance of Bramhope railway tunnel at SE 255 438 |accessdate=9 February 2017}}</ref>>

==Incidents ==
A southbound passenger train and a pilot engine left [[Arthington railway station|Arthington station]] on 19 September 1854 heading for Leeds. A pilot engine had travelled northbound through the tunnel earlier the same day with no problems but this time the train ran into a pile of stone debris and was derailed when it was three-quarters of the way into the tunnel. The debris was from a roof fall that affected both tracks. The train engine collided with the pilot engine tender causing considerable damage.<ref>{{cite web|url=http://www.railwaysarchive.co.uk/docsummary.php?docID=1641|title=Accident Returns: Extract for the Accident at Bramhope Tunnel on 19th September 1854| publisher=Railway Archive|accessdate=8 July 2014 }}</ref>

Trains have been cancelled or delayed because of flooding in the tunnel. Water still runs fast into the tunnel, and in the 1960s a train was derailed by a 3-ton (3.3 tonne) icicle.<ref name="bradfordarchive" />

== Repairs ==
Major repair work was done in 2003 and 2006, when the [[Victorian era|Victorian]] drainage [[culvert]] was replaced and the track lowered to allow access for larger passenger and freight stock at a cost of £10&nbsp;million.<ref>{{cite web|url=http://news.bbc.co.uk/1/hi/england/north_yorkshire/3065563.stm|title=Rail tunnel to close for £10m revamp|date=14 July 2003|work=BBC News|accessdate=19 July 2009}}</ref><ref>{{cite web|url=http://www.pandrol.com/pdf/TR0304/p18-19.pdf|title=Renewal of the Bramhope Tunnel, Network Rail|last=Wilder|first=Peter|work=Pandrol Fastclip|format=PDF|accessdate=19 July 2009}}</ref> The 16 closed airshafts were deteriorating and had to be re-capped.<ref>{{cite web|url=http://www.etarmac.com/cmspozament/pr_Bramhope_090108.aspx|title=Bramhope Tunnel repairs on the right track|date=January 2007|work=Tarmac|accessdate=19 July 2009}}</ref> In 2003 the excavated material from the works was recycled to shore up the railway embankment near [[Castley]].<ref>{{cite web|url=http://www.aggregain.org.uk/case_studies/use_of_recycled.html|title=Use of recycled ballast as fill to embankment on a railway|date=5 April 2005|publisher=Wrap|accessdate=19 July 2009}}</ref>

== Human cost ==
[[File:Bramhope Tunnel memorial.jpg|thumb|200px|right|Bramhope Tunnel memorial, [[All Saints' Church, Otley|All Saints' Church]] churchyard, [[Otley]]]]
Records of death and injury were kept from 1847 to 1849, and grants were made to the [[Leeds General Infirmary|Leeds Infirmary]] and a special sprung handcart was provided to transport the injured to hospital.{{sfn|Seals|1964|p=49|ps=}}<ref name="tunnel" />

Five men died in 1846, twelve died in 1847 and seven more had died by 1849. The 24 men who died are commemorated in Otley churchyard by a Grade II listed monument in the shape of the north portal.<ref name="Memorial">{{NHLE|num=1135240|desc=Memorial to Victims of Bramhope Tunnel Disaster SE 2045 |accessdate=9 February 2017}}</ref> It was erected by the contractor.{{sfn|Seals|1964|p=49|ps=}} 
<blockquote>
The sadness of the harsh conditions of those days is captured by the simple epitaph on the gravestone of James Myers who is buried in the Methodist Cemetery at Yeadon behind the Town Hall. James was a married man just 22 years old who 'died by an accident in the Bramhope Tunnel on the 14th day of April, 1848'. Next to him lies the body of his 3 years old daughter who died two weeks later of some unspecified illness.<ref name="tunnel" />
</blockquote>

== References ==
'''Notes'''
{{notelist}}
'''Citations'''
{{reflist|2}}

'''Bibliography'''
{{refbegin}}
*{{citation |last=Bairstow |first=Martin |title= Railways around Harrogate |publisher=Allanwood Press|year=1989|isbn=1-871944-00-7}}
*{{citation |last=Baughn |first=Peter E. |title=The Railways of Wharfedale |publisher=[[David & Charles]]|year=1969|isbn=0-7153-4705-5}}
*{{citation |last=Seals |first= William Francis |title=A history of the township of Bramhope |publisher= |year=1964 |isbn=0950510300}}
{{refend}}

==External links==
[http://www.bbc.co.uk/programmes/p013f97b  Bramhope Tunnel Memorial video] 
{{commons category|Bramhope Tunnel}}

{{Coord|53|53|23|N|1|36|45|W|type:landmark_region:GB|display=title|name=Bramhope Tunnel}}

{{good article}}

[[Category:Railway tunnels in England]]
[[Category:Tunnels completed in 1849]]
[[Category:Rail transport in West Yorkshire]]
[[Category:North Eastern Railway (UK)]]
[[Category:1849 establishments in England]]