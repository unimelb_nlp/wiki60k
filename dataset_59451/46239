[[File:Georgia Aquarium - Giant Grouper.jpg|thumb|250px|right|[[Grouper]] capture their prey by sucking them into their mouth]]

'''Aquatic feeding mechanisms''' face a special difficulty as compared to feeding on land, because the density of water is about the same as that of the prey, so the prey tends to be pushed away when the mouth is closed. This problem was first identified by [[Robert McNeill Alexander]].<ref name="Alexander67">[[Robert McNeill Alexander|Alexander, R. McN]]. 1967. ''Functional design in fishes''. London, UK: Hutchinson University Library.</ref> As a result, underwater [[predator]]s, especially [[bony fish]], have evolved a number of specialized feeding mechanisms, such as [[filter feeding]], ram feeding, suction feeding, protrusion, and pivot feeding.

Most underwater predators combine more than one of these basic principles. For example, a typical generalized predator, such as the [[cod]], combines suction with some amount of protrusion and pivot feeding.

== Suction feeding ==
{{external media
|align=right
|width=230px
|video1=[https://www.youtube.com/watch?v=TbGILi8p5Y8&feature=rec-LGOUT-real_rn-HM Video of a red bay snook catching prey by suction feeding]
}}
'''Suction feeding''' is a method of ingesting a prey item in fluids by sucking the prey into the predator's mouth. This is typically accomplished by the predator expanding the volume of its oral cavity and/or throat, resulting in a pressure difference between the inside of the mouth and the outside environment.  When the mouth is opened, the pressure difference causes water to flow into the predator's mouth, carrying the prey item in with the fluid flow.<ref>{{Cite journal|last=Lauder|first=George|year=1980|title=The suction feeding mechanism in sunfish (Lepomis): an experimental analysis|url=http://www.people.fas.harvard.edu/~glauder/reprints_unzipped/Lauder1980.pdf|journal=Journal of Experimental Biology|volume=|pages=49–72|via=}}</ref> 

Though suction feeding can be seen across fish species, those with more derived characters show an increase in the suction force as a result of more complex [[Fish head|skull]] linkages that allow greater expansion of the buccal cavity and thereby creating a greater negative pressure. Most commonly this is achieved by increasing the lateral expansion of the skull. In addition, the derived character of upper jaw protrusion is acknowledged to increase the force exerted on the [[Predation|prey]] to be engulfed.<ref>{{Cite journal|last=Holzman, R., Day, S. W., Mehta, R. S., & Wainwright, P. C.|first=|year=2008|title=Jaw protrusion enhances	 forces exerted on prey by suction feeding fishes|url=|journal=Journal of the Royal Society Interface|volume=|pages=1445-1457|via=}}</ref> Perhaps the best examples of these derived characters that are explained belong to fishes in the [[teleost]]ei clade.<ref name=":1">{{Cite journal|last=Lauder|first=George|year=1982|title=Patterns of evolution in the feeding mechanism of actinopterygian fishes|url=|journal=American Zoologist|volume=|pages=275–285|via=}}</ref> However, a common misconception of these fishes is that suction feeding is the only or primary method employed.<ref name=":0">{{Cite journal|last=Gardiner, J. M., & Motta, P. J.|first=|year=2012|title=Largemouth bass (micropterus salmoides) switch feeding modalities in response to sensory deprivation|url=|journal=Zoology (Jena, Germany)|volume=|pages=78-83|via=}}</ref> In ''[[Largemouth bass|Micropterus salmoides]]'' ram feeding is the primary method for prey capture; however, they can modulate between the two methods or use both as with many teleosts.<ref>{{Cite journal|last=Norton, S. F., & Brainerd, E. L.|first=|year=1993|title=Convergence in the feeding mechanics of ecomorphologically similar species in the centrarchidae and cichlidae|url=|journal=Journal of Experimental Biology|volume=|pages=|via=}}</ref><ref name=":0" /> Also, it is commonly thought that fishes belonging to more [[Primitive (phylogenetics)|primitive]] [[Clade|clades]] exhibit suction feeding. Although suction may be created upon the mouth opening in such fishes, the criteria for suction feeding includes little or no bodily movement towards their prey.<ref name=":1" /> 

{{clear}}

== Ram feeding ==
[[File:Manta alfredi ram feeding.png|thumb|right|Foraging ''Manta alfredi'' [[ram feeding]], swimming against the tidal current with its mouth open and sieving zooplankton from the water <ref>{{cite journal | last1 = Jaine | first1 = FRA | last2 = Couturier | first2 = LIE | last3 = Weeks | first3 = SJ | last4 = Townsend | first4 = KA | last5 = Bennett | first5 = MB | last6 = Fiora | first6 = K | last7 = Richardson | first7 = AJ | year = 2012 | title = When Giants Turn Up: Sighting Trends, Environmental Influences and Habitat Use of the Manta Ray Manta alfredi at a Coral Reef | url = http://www.plosone.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0046170#pone-0046170-g002 | journal = PLOS ONE | volume = 7| issue = | page = e46170 | doi = 10.1371/journal.pone.0046170 | pmid=23056255 | pmc=3463571}}</ref>]]

'''Ram feeding''', also known as '''lunge feeding''', is a method of feeding underwater in which the predator moves forward with its mouth open, engulfing the prey along with the water surrounding it.  During ram feeding, the prey remains fixed in space, and the predator moves its jaws past the prey to capture it.  The motion of the head may induce a [[bow wave]] in the fluid which pushes the prey away from the jaws, but this can be avoided by allowing water to flow through the jaw.  This can be accomplished by means of an expandable throat, as in [[snapping turtle]]s and [[baleen whales]],<ref name=goldbogen>{{cite journal | last1 = Goldbogen | first1 = J. A. | last2 = Calambokidis | first2 = J. | last3 = Shadwick | first3 = R. E. | last4 = Oleson | first4 = E. M. | last5 = McDonald | first5 = M. A. | last6 = Hildebrand | first6 = J. A. | year = 2006 | title = Kinematics of foraging dives and lunge-feeding in fin whales | url = | journal = J. Exp. Biol. | volume = 209 | issue = | pages = 1231–1244 | doi=10.1242/jeb.02135}}</ref> or by allowing water to flow out through the gills, as in [[shark]]s and [[herring]]. A number of species have evolved narrow snouts, as in [[gar fish]] and [[Water snake (disambiguation)|water snake]]s 
.<ref name="Wassenbergh">Van Wassenbergh, S., Brecko, J., Aerts, P., Stouten, I., Vanheusden, G., Camp, A., Van Damme, R., and Herrel, A. (2010). Hydrodynamic constraints on prey-capture performance in forward-striking snakes. J. Roy. Soc. Interface, 7:773-785</ref>

[[Herring]]s often [[Forage fish#Hunting copepods|hunt copepods]]. If they encounter copepods [[Shoaling and schooling|schooling]] in high concentrations, the herrings switch to ram feeding. They swim with their mouth wide open and their [[Operculum (fish)|opercula]] fully expanded. Every several feet, they close and clean their [[gill raker]]s for a few milliseconds ([[filter feeding]]).  The fish all open their mouths and opercula wide at the same time (the red gills are visible in the photo below&mdash;click to enlarge).  The fish swim in a grid where the distance between them is the same as the jump length of the copepods.

<gallery widths=200px>
File:Herringramkils.jpg|Herring ram feeding on a school of [[copepod]]s
File:Group of fish near the beach of Sharm El Naga.jpg|[[Shoaling and schooling|School]] of adult [[Indian mackerel]] ram feeding on macroplankton
</gallery>

{{clear}}

== Pivot feeding ==
[[File:Black Sea fauna Seahorse.JPG|thumb|right|[[Seahorse]]s rely on stealth to ambush small prey such as [[copepod]]s. They use pivot feeding to catch the copepod, which involves rotating their snout at high speed and then sucking in the copepod.<ref name=Langley2013>{{cite web|author=Langley, Liz |date=26 November 2013 |url=http://newswatch.nationalgeographic.com/2013/11/26/why-does-the-seahorse-have-its-odd-head-mystery-solved/ |title=Why Does the Seahorse Have Its Odd Head? Mystery Solved – News Watch |publisher=Newswatch.nationalgeographic.com }}</ref><ref>{{Cite journal | doi = 10.1038/ncomms3840| title = Morphology of seahorse head hydrodynamically aids in capture of evasive prey| journal = Nature Communications| volume = 4| year = 2013| last1 = Gemmell | first1 = B. J. | last2 = Sheng | first2 = J. | last3 = Buskey | first3 = E. J. | pmid=24281430 | page=2840}}</ref>]]

'''Pivot feeding''' is a method to transport the mouth towards the prey by an upward turning of the head, which is pivoting on the neck joint. [[Pipefish]] such as [[sea horses]] and [[Leafy seadragon|sea dragons]] are specialized on this feeding mechanism.<ref name="lussanet">{{cite journal | last1 = de Lussanet | first1 = M. H. E. | last2 = Muller | first2 = M. | year = 2007 | title =  The smaller your mouth, the longer your snout: predicting the snout length of ''Syngnathus acus'', ''Centriscus scutatus'' and other pipette feeders| url = | journal = J. R. Soc. Interface | volume = 4 | issue = | pages = 561–573 | doi = 10.1098/rsif.2006.0201 }}</ref> With prey capture times of down to 5 ms ([[shrimpfish]] ''Centriscus scutatus'') this method is used by the fastest feeders in the animal kingdom.

The secret of the speed of pivot feeding is in a [[Linkage (mechanical)#Biological linkages|locking mechanism]], in which the [[hyoid arch]] is folded under the head and is aligned with the urohyal which connects to the [[shoulder girdle]]. A [[four-bar linkage]] at first locks the head in a ventrally bent position by the alignment of two bars. The release of the locking mechanism jets the head up and moves the mouth toward the prey within 5-10 ms. The trigger mechanism of unlocking is debated, but is probably in lateral [[adduction]].

{{clear}}

== Protrusion ==
{{external media
|align=right
|width=230px
|video1=[https://www.youtube.com/watch?v=pDU4CQWXaNY&feature=channel_page Video of a slingjaw wrasse catching prey by protruding its jaw]
}}
'''Protrusion''' is the extension of the mouth or [[premaxilla]] towards the prey, via [[Linkage (mechanical)#Biological linkages|mechanical linkages]]. Protrusion is only known in modern [[bony fishes]], which possess many forms of coupled linkages in their head.<ref>{{cite journal | last1 = Muller | first1 = M | year = 1996 | title = A novel classification of planar four-bar linkages and its application to the mechanical analysis of animal systems | url = | journal = Phil. Trans. R. Soc. Lond. B | volume = 351 | issue = | pages = 689–720 | doi=10.1098/rstb.1996.0065}}</ref> Remarkable examples are the [[slingjaw wrasse]] and the [[sand eel]] which can protrude their mouth by several centimeters. Another example of protrusion is seen in [[dragonfly]] larvae, or nymphs, which have hydraulic lower mandibles, protruding forward to catch prey and bring it to the top jaw.<ref>{{cite web|url=http://io9.com/how-is-this-dragonflys-prehensile-mouthpart-connected-t-1658069078|website=io9|publisher=io9}}</ref>

{{clear}}

== Filter and suspension feeding ==
[[File:Filterkrillkils2.gif|thumb|upright|Krill feeding under high [[phytoplankton]] concentration (slowed down by a factor of 12)]]

This is the selection of food particles from a water flow, for example by the [[gill rakers]] of fish, the baleens of [[baleen whales|whales]], or the ostia of [[sponges]].

=== Filter feeding ===
In '''[[filter feeding]]''', the water flow is primarily generated by the organism itself, for example by creating a pressure, by active swimming, or by [[cilium|ciliary]] movements.

=== Suspension feeding ===
In '''suspension feeding''', the water flow is primarily external or if the particles themselves move with respect to the ambient water, such as in [[sea lilies]].

== Lunge feeding ==
[[Baleen whale]]s feed on [[plankton]] by a technique called [[Lunge feed#Predator strategies|lunge feeding]]. Lunge feeding could be regarded as a kind of inverted suction feeding, during which a whale takes a huge gulp of water, which is then filtered through the [[baleen]]s.<ref>{{cite journal | last1 = Goldbogen | first1 = JA | last2 = Calambokidis | first2 = J | last3 = Shadwick | first3 = RE | last4 = Oleson | first4 = EM | last5 = McDonald | first5 = MA | last6 = Hildebrand | first6 = J A | year = 2006 | title = Kinematics of foraging dives and lunge-feeding in fin whales | url = http://cetus.ucsd.edu/Publications/Publications/GoldbogenJEB2006.pdf | format = PDF | journal = Journal of Experimental Biology | volume = 209 | issue = | pages = 1231–1244 | doi=10.1242/jeb.02135}}</ref> Biomechanically this is a unique and extreme feeding method, for which the animal at first must accelerate to gain enough [[momentum]] to fold its elastic throat ([[buccal cavity]]) around the volume of water to be swallowed.<ref>{{cite journal | last1 = Potvin | first1 = J | last2 = Goldbogen | first2 = JA | year = 2009 | title = ''Passive versus active engulfment: verdict from trajectory simulations of lunge-feeding fin whales ''Balaenoptera physalus | url = http://rsif.royalsocietypublishing.org/content/6/40/1005.full | journal = J. R. Soc. Interface | volume = 6 | issue = 40| pages = 1005–1025 | doi = 10.1098/rsif.2008.0492 | pmid=19158011 | pmc=2827442}}</ref> Subsequently, the water flows back through the baleens keeping back the food particles. The highly elastic and [[muscular]] buccal rills are a specialized adaptation to this feeding mode.

==See also==
* [[Cleaner fish]]
* [[Feeding behaviour of fish]]
* [[Lepidophagy]]
* [[List of feeding behaviours]]
* [[Paedophagy]]

== References ==
{{reflist|30em}}

== External links ==
* [[FishBase]] is a comprehensive database of bony fishes: [http://www.fishbase.org/search.php /fishbase.org]

{{feeding|state=expand}}
{{diversity of fish}}

[[Category:Predation]]
[[Category:Marine biology]]
[[Category:Biomechanics]]