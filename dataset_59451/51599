{{Infobox journal
| italic title = force
| title = International Journal for the Psychology of Religion
| cover = 
| discipline = [[Psychology of religion]]
| editor = [[Raymond Paloutzian|Raymond F. Paloutzian]]
| website = http://www.informaworld.com/smpp/title~db=all~content=t775653664~tab=summary
| link1 = http://www.informaworld.com/smpp/title~db=all~content=g931826172~tab=toc
| link1-name = Online access
| link2 = http://www.informaworld.com/smpp/title~db=all~content=t775653664~tab=issueslist
| link2-name = Online archive
| publisher = [[Routledge]]
| country = 
| abbreviation = Int. J. Psychol. Relig.
| history = 1991–present
| impact = 1.639
| impact-year = 2011
| frequency = Quarterly
| ISSN = 1050-8619
| eISSN = 1532-7582
| JSTOR = 
| OCLC = 45255134
| CODEN = IPRLEB
| LCCN = 91641651
}}
The '''''International Journal for the Psychology of Religion''''' is a [[Peer review| peer-reviewed]] [[academic journal]] devoted to research on the [[psychology of religion]]. Its scope includes the social psychology of religion, religious development, conversion, religious experience, religion and social attitudes and behavior, religion and mental health, and psychoanalytic and other theoretical interpretations of religion. The current [[editor-in-chief]] is [[Raymond F. Paloutzian]] ([[Westmont College]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in [[PsycINFO]], [[Applied Social Sciences Index and Abstracts]], [[Arts and Humanities Citation Index]], [[Current Contents]]/Arts & Humanities, Current Contents/Social and Behavioral Sciences, [[EBSCO Publishing| EBSCO databases]], [[Family Index Database]], [[Journal Citation Reports]]/Social Sciences [[Religion Index One]], [[Religious and Theological Abstracts]], [[Scopus]], and [[Social Sciences Citation Index]].<ref>{{cite web| url = http://www.informaworld.com/smpp/title~mode=abstracting_a_indexing~tab=summary?content=t775653664| title = Abstracting and indexing| publisher = Routledge| work = Homepage International Journal for the Psychology of Religion| accessdate = 2011-01-23}}</ref> Beginning with volume 18 (2008), the journal is included in the databases of the [[Institute for Scientific Information]].<ref name = paloutzisi>{{cite journal| author = [[Raymond F. Paloutzian]]| year = 2010| title = ISI and Impact Factor| journal = The International Journal for the Psychology of Religion| volume = 20| issue = 1| pages = 1| doi = 10.1080/10508610903481994}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2011 [[impact factor]] of 1.639, ranking it 37th out of 124 journals in the category "Psychology, Multidisciplinary".<ref name=WoS>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Psychology, Multidisciplinary |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2012-07-26 |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{reflist|30em}}

== External links ==
* {{Official|1=http://www.informaworld.com/smpp/title~db=all~content=t775653664~tab=summary}}

{{DEFAULTSORT:International Journal For The Psychology Of Religion}}
[[Category:Psychology of religion journals]]
[[Category:Religious studies journals]]
[[Category:Publications established in 1991]]
[[Category:Quarterly journals]]
[[Category:Taylor & Francis academic journals]]