{{Multiple issues|
{{Orphan|date=January 2016}}
{{refimprove|date=May 2015}}
}}

{{Infobox scientist
| name        = Lawrence Alexander Hardie
| image       = Lawrence A Hardie 2004.JPG
| caption     = Lawrence A. Hardie in 2004
| birth_name  = Lawrence Alexander Hardie
| birth_date  = {{Birth date|1933|1|13}}
| birth_place = [[Durban]], [[Natal, South Africa]]
| residence   = United States (1960-)
| nationality = American
| death_date  = {{Death date and age|2013|12|17|1933|1|13}}
| death_place = [[Seaside, California]], U.S.
| education  = [[Johns Hopkins University]]
| field       = [[Geology]], [[Sedimentology]], [[Geochemistry]]
| work_institutions = [[Johns Hopkins University]] [[Department of Earth and Planetary Sciences]]  professor 1965–2013, chair 1992–1995, 2004–2006
| alma_mater  = [[University of Natal]], RSA <br />  [[Johns Hopkins University]], US <br />(Ph.D.)
| doctoral_advisor = geochemist Hans Eugster, and sedimentologist Francis J. Pettijohn
| awards      = [[Francis J. Pettijohn]] Medal for Excellence by the Society for Sedimentary Geology in 2003.
| spouse      = Glenys Kathleen Hardie (1961-)
| children    = Deborah Buettner and Russell Hardie
}}

'''Lawrence Alexander Hardie''' (January 13, 1933 – December 17, 2013) was an American [[geologist]], [[sedimentologist]], and [[geochemist]]. For nearly 50 years, he mentored students and pursued research as a Professor at [[Johns Hopkins University]] (JHU) in Baltimore MD in the Department of Earth and Planetary Sciences.<ref name=sun>{{cite news|last1=Kelly|first1=Jacques|title=Lawrence A. Hardie, Hopkins geology professor|url=http://articles.baltimoresun.com/2014-01-17/news/bs-md-ob-lawrence-hardie-20140117_1_geology-ocean-salinity-students|work=[[Baltimore Sun]]|date=January 17, 2014}}</ref>  He authored or co-authored many scientific papers and several books.

Hardie's research topics included evaporites, dolomitization, cyclical deposition of carbonate sediments, and plate tectonic driven changes in seawater chemistry.<ref name=SEPM>2003 Annual Report of the Society for Sedimentary Geology (SEPM). http://www.sepm.org/CM_Files/SocietyRecords/2003.pdf</ref>  In the latter, he proposed that changes in the seafloor spreading rates at mid-ocean ridges have altered the composition of seawater throughout earth history, producing oscillations in the mineralogy of carbonate and evaporite precipitates.<ref name=JHUmagazine>Johns Hopkins Magazine, February 2002, Vol. 54, No. 1.  http://pages.jh.edu/~jhumag/0202web/wholly.html#sea</ref>  Specifically citing these scientific contributions, the [[Society for Sedimentary Geology]] (SEPM) awarded him the Francis J. Pettijohn Medal in 2003.<ref name=SEPM/>

== Early life and education ==

Hardie was born in [[Durban, South Africa|Durban, Natal, South Africa]], on January 13, 1933.<ref name=sun/>
He attended the [[University of Natal]], Durban, originally to pursue an undergraduate degree in chemistry.  He focused instead on geology after attending lectures by South African geologist [[Lester Charles King]].<ref name=EPS_News>Johns Hopkins University, Department of Earth & Planetary Sciences, Department News Archive, December 20, 2013. http://eps.jhu.edu/about/archive/</ref>  While a student, Hardie played soccer,<ref name=sun/> and was selected as a member of the South African Universities "All Star" team four times. He earned a B.Sc. degree in Geology and Chemistry in 1955, and a B.Sc. (Hons.)degree in Geology in 1956.

In 1957, he was hired by King as an Instructor and taught beginning classes in geology while working on his Master's thesis on the origin of the Table Mountain Sandstone.  He earned an M.Sc. in Geology in 1959 under the guidance of Drs. Peter Matthews and Joseph Frankel.

In 1960, he was awarded a Fellowship by the South African Council for Scientific and Industrial Research to spend an academic year in the U.S. He went to Johns Hopkins University and began working with sedimentologist [[Francis J. Pettijohn]] and geochemist Hans Eugster, in a newly built geochemistry laboratory.<ref name=EPS_News/><ref name=JHU_News>JHU University News, January 28, 2014.  http://hub.jhu.edu/2014/01/28/lawrence-hardie-eps</ref>
There he conducted experimental work on evaporite minerals.  He was offered a full-time graduate fellowship to earn a Ph.D. He briefly returned to South Africa to marry Glenys Kathleen Smith in Durban,<ref name=EPS_News/> and then graduated with a Ph.D. in 1965.

== Academic career ==
[[File:Lawrence A. Hardie driving Honda All Terrain Vehicle in Baja California, Mexico.JPG|thumb|Lawrence A. Hardie driving Honda All Terrain Vehicle in Baja California, Mexico during a geology field trip in the early 1980s.  Photo by R. C. Hardie.]]
Hardie joined the faculty at JHU as an assistant professor in the Department of Earth and Planetary Sciences in 1965.<ref name=sun/>  He later became a full professor, mentoring more than 30 graduate students. He also taught introductory geology to many undergraduate students, including his own children, introducing them to the intricacies of geological process through extensive field trips.<ref name=EPS_News/><ref name=JHU_News/>   He also served as the department chair from 1992 to 1995, and again in 2004–2006.<ref name=sun/>  He retired in 2007 to the status of professor emeritus.<ref name=JHU_News/>

Hardie ran the JHU field camp (Camp Singewald) in western Maryland for many summers in the 1960s and 1970s.  He led regular field trips to the Florida Keys and Assateague Island, and also took students to the Bahamas and Baja California, Mexico.  His work also took him frequently to the Dolomites in Italy,<ref name=sun/> and he partook in long hikes and climbs.  Some of his field work required travel by Zodiac inflatable boats, minibikes, and 3-wheeled all terrain vehicles.

== Scientific achievements ==
[[File:Lawrence A Hardie 1988.jpg|left|thumb|150px|Geologist Lawrence A Hardie, Professor, Johns Hopkins University Photo taken in 1988 by R. C. Hardie.]]

Early in his career, Hardie conducted research and experimentation with evaporites, showing that the signal from evaporites provide evidence for seawater chemistry change. He observed that secular changes in the mineralogy of potash evaporites and ooids and cements in marine limestones are synchronous with greenhouse/hothouse climates and global sea level. Hardie's study of evaporites led to the hypothesis, since verified through study of seawater trapped inside crystals of marine halite, that seawater has undergone long-term variations in its major ion composition.<ref name=JHUmagazine/> He demonstrated that these variations are linked to plate tectonic processes at mid-ocean ridges. These studies improved the understanding of calcifying marine organisms and their role in the global carbon cycle, and also had implications for geochemistry, mineralogy, tectonics, biological evolution (biomineralization), oil/gas resources, and climate change. In recognition, he was awarded the Francis J. Pettijohn Medal for Excellence by the [[Society for Sedimentary Geology]] in 2003.<ref name=SEPM/>

Hardie completed field research on the modern shallow marine [[Carbonate rock|carbonates]] of the Bahamas with Bob Ginsburg and others. In 1977 he wrote a book about comparative sedimentology.  He went on to study ancient carbonates of western Maryland (Cambro-Ordovician) and the Italian Dolomites (Triassic). Hardie's work on carbonates advanced the understanding of climate and sea level change, and the role of Milankovitch cycles in carbonate deposition.

Hardie studied the occurrence of dolomite in the geologic record, where, when dolomite replaces calcite minerals, its slightly smaller molar volume leaves voids in carbonate rock, causing oil migration. In the Dolomites, Hardie (with student Edith Wilson) demonstrated the hydrothermal origin of dolomite in the Triassic Latemar buildup.

Hardie and his students and colleagues studied cyclic sedimentation, confirming that platform carbonates of the Middle Triassic (Anisian-Ladinian) of the Latemar buildup consist of a vertical stack of over 500 thin (ave. thickness 0.6-0.85m) shallowing-upward depositional cycles that record high frequency eustatic sea level oscillations in tune to Milankovitch astronomical rhythms.  They described the details of the cycles and deposition and created appropriate computer simulations.

== Awards and honors ==

[[File:Francis J. Pettijohn Medal.jpg|thumb|Francis J. Pettijohn Medal for Sedimentology, from the Society for Sedimentary Geology, 2003.]]

{{refbegin}}
* Francis J. Pettijohn Medal for Sedimentology, from the Society for Sedimentary Geology, 2003.<ref name=SEPM/>   
* Selected by the Student Council of Johns Hopkins University as one of the top ten teachers in the School of Arts & Sciences in 1998.<ref>The Johns Hopkins News-Letter, Vol. CII, Issue 22</ref>
* Oualline Distinguished Scholar at University of Texas, Austin, 1995.
* The "Hardie Teaching Laboratory", in Olin Hall at JHU has been dedicated in 2014.  
* One of the annual Krieger School's Dean's Undergraduate Research Awards at JHU has been named after him, 2014.
* The Lawrence A. Hardie Memorial Fund in Earth and Planetary Sciences at JHU, 2014.
* Lawrence A. Hardie Commemorative Session, Geological Society of America meeting in Baltimore, MD, November 1–4, 2015.

==Personal life and death==
Hardie and his wife Glenys became U.S.<ref name=EPS_News/>  They had two children; his daughter Deborah obtained a degree in mathematics at JHU, and Russell studied engineering at Loyola University Maryland.  His son Russell is currently a professor in the department of Electrical and Computer Engineering at the University of Dayton.<ref name=sun/>

Haridie enjoyed jazz and attended concerts at the Famous Ballroom in Baltimore.  After from Baltimore to Pasadena, Maryland, in the 1970s, Hardie took to sailing on the Magothy River and the Chesapeake Bay.<ref name=sun/>  He taught his children and many of his students to sail.  He later took up snow skiing and golf.<ref name=sun/>

Hardie died at age 80 from complications of Alzheimer's disease on December 17, 2013, at the Community Hospital of the Monterey Peninsula in Monterey, California.<ref name=sun/>

== Selected publications ==

=== 1960s ===

*{{cite journal
 | title = The Fault-Pattern of Coastal Natal: an Experimental Reproduction
 | year = 1962
 | last1 = Hardie | first1 = Lawrence A
 | journal = Transactions of the Geological Society of South Africa
 | pages = 203–206
 | volume = 65
 }}

*{{cite journal
 | title = Morphological analysis of hodgkinsonite
 | year = 1964
 | journal = American Mineralogist
 | pages = 415–420
 | volume = 49
 | last4 = Donnay	 | first4 = J. D. H.
 | last3 = Donnay	 | first3 = Gabrielle
 | last2 = Munoz	 | first2 =  James L. 
 | last1 =  Hardie	 | first1 =  Lawrence A.	}}

*{{cite journal
 | title = Ferric tourmaline from Mexico
 | year = 1964
 | journal = Science
 | pages = 71–73
 | volume = 144
 | issue = 3614
 | last1 = Mason	 | first1 =  Brian
 | last2 = Donnay	 | first2 =  Gabrielle
 | last3 =  Hardie	 | first3 =  Lawrence A	 | doi=10.1126/science.144.3614.71 | pmid=17729799}}

*{{cite journal
 | title = The gypsum-anhydrite equilibrium at one atmosphere pressure
 | year = 1967
 | last = Hardie | first = Lawrence A
 | journal = The American Mineralogist
 | pages = 171–200
 | volume = 52
 }}

*{{cite journal
 | title = The origin of the recent non-marine evaporite deposit of Saline Valley, Inyo County, California
 | year = 1968
 | last = Hardie | first = Lawrence A
 | journal = Geochimica et Cosmochimica Acta
 | pages = 1279–1301
 | volume = 32
 | issue = 12
 | doi=10.1016/0016-7037(68)90029-x
 }}

=== 1970s ===
*{{cite journal |last=Hardie |first=Lawrence A|last2=Eugster |first2=Hans P |date= 1970|title= 
The evolution of closed-basin brines |journal= Mineral. Soc. Am. Spec. Pap | issue= 3 |pages=273–290}}

*{{cite journal |last=Hardie |first=Lawrence |last2=Eugster |first2=Hans P|date= 1971|title= The depositional environment of marine evaporites: a case for shallow, clastic accumulation |journal= Sedimentology | issue= 6 |pages= 187–200 |doi=10.1111/j.1365-3091.1971.tb00228.x |volume=16}}
*{{cite journal
 | title = Depositional theme of a marginal marine evaporite
 | year = 1973
 | journal = Sedimentology
 | pages = 5–27
 | volume = 20
 | issue = 1
 | last1 = Bosellini	 | first1 =  Alfonso
 | last2 =  Hardie	 | first2 =  Lawrence A	
 | doi=10.1111/j.1365-3091.1973.tb01604.x}}

*{{cite journal
 | title = Sedimentation in an ancient playa-lake complex: the Wilkins Peak Member of the Green River Formation of Wyoming
 | year = 1975
 | last1 = Eugster | first1 = Hans P
 | last2 = Hardie | first2 = Lawrence A
 | journal = Geological Society of America Bulletin
 | pages = 319–334
 | volume = 86
 | issue = 3
 | doi=10.1130/0016-7606(1975)86<319:siaapc>2.0.co;2
 }}

*{{cite book |last1= Ginsburg | first1 = Robert N | last2 = Hardie | first2 = Lawrence A | date=1975 | title= Tidal and storm deposits, northwestern Andros Island, Bahamas | pages = 201–208 | volume = 22 | publisher=Springer Berlin Heidelberg}}
*{{cite journal
| date = 1976 
| last1 = Reinhardt | first1 = Juergen 
| last2 = Hardie | first2 = Lawrence A 
| title = Selected examples of carbonate sedimentation, Lower Paleozoic of Maryland 
| journal = Maryland Geological Survey Guidebook 
| issue = 5
}}

*{{cite journal
 | title = The Geological Significance of the Freshwater Blue-Green Algal Calcareous Marsh
 | year = 1976
 | last1 = Monty | first1 = CLV
 | last2 = Hardie | first2 = Lawrence A
 | journal = Developments in sedimentology
 | pages = 447–477
 | volume = 20
 }}

*{{cite book |last= Hardie|first= Lawrence A| date=1977 | title=Sedimentation on the modern carbonate tidal flats of northwest Andros Island, Bahamas | volume = 22 | publisher=Johns Hopkins University Press}}
*{{cite journal | last = Eugster | first = Hans P | last2  = Hardie | first2  = Lawrence A
| date  = 1978 | title = Saline lakes | journal= Lakes | publisher = Springer-Verlag | pages = 237–293 }}

*{{cite journal | last = Hardie | first = Lawrence A | title = Saline lakes and their deposits: a sedimentological approach
 | year = 1978
 | journal = Modern and Ancient Lake Sediments. International Association of Sedimentologists, Special Publication No
 | pages = 7–42
 | volume = 2
 | last2 = Smoot	 | first2 =  Joseph P.
 | last3 =  Eugster	 | first3 =  Hans P.	}}

=== 1980s ===
*{{cite journal
 | title = Evaporation of seawater: calculated mineral sequences
 | year = 1980
 | last = Hardie | first = Lawrence A | last2 = Eugster | first2 = Hans P.
 | journal = Science
 | pages = 498–500
 | volume = 208
 | issue = 4443
 | doi=10.1126/science.208.4443.498
 | pmid=17744561
 }}
*{{cite journal
 | title = Patterns of platform and off-platform carbonate sedimentation in the upper Cambrian of the central Appalachians and their implications for sea level history
 | year = 1981
 | last = Demicco | first = Robert V
 | last2 = Hardie | first2 = Lawrence A 
 | journal = Short Papers for the 2nd International Symp. on the Cambrian System 1981, U.S.G.S. Open-File Report 81-743
 | pages = 67–70
 | issue = 81–743
 }}
*{{cite journal
 | title = Origin of CaCl2 brines by basalt-seawater interaction: Insights provided by some simple mass balance calculations
 | year = 1983
 | last = Hardie | first = Lawrence A
 | journal = Contributions to Mineralogy and Petrology
 | issue = 2–3
 | volume = 82
 | pages = 205–213
 | doi=10.1007/bf01166615
 }}
*{{cite journal
 | title = Evaporites; marine or non-marine?
 | year = 1984
 | last = Hardie | first = Lawrence A
 | journal = American Journal of Science
 | pages = 193–240
 | volume = 284
 | issue = 3
 | doi=10.2475/ajs.284.3.193
 }}
*{{cite journal
 | title = The problem of distinguishing between primary and secondary features in evaporites
 | year = 1985
 | last = Hardie | first = Lawrence A
 | last2 = Lowenstein | first2 = Timothy K
 | last3 = Spencer | first3 = Ronald J
 | journal = Sixth Int. Symposium on Salt 1983, Alexandria, Va., The Salt Institute
 | pages = 11–39
 | volume = 1
}}
*{{cite journal
 | title = Criteria for the recognition of salt‐pan evaporites
 | year = 1985
 | last = Lowenstein | first = Timothy K
 | last2 = Hardie | first2 = Lawrence A
 | journal = Sedimentology
 | pages = 627–644
 | volume = 32
 | issue = 5
 | doi=10.1111/j.1365-3091.1985.tb00478.x
 }}
*{{cite journal
 | title = Carbonate depositional environments: modern and ancient
 | year = 1986
 | last1 = Hardie | first1 = Lawrence A
 | last2 = Shinn | first2 =  E A
 | journal = Part 3: Tidal flats. Colorado School of Mines Quarterly
 | volume = 81
 | issue = 1
 | pages = 7–35 }}
*{{cite journal
 | title = Repeated subaerial exposure of subtidal carbonate platforms, Triassic, northern Italy: evidence for high frequency sea level oscillations on a 104 year scale
 | year = 1986
 | journal = Paleoceanography
 | pages = 447–457
 | volume = 1
 | issue = 4
 | last1 = Hardie | first1 = Lawrence A
 | last2 = Bosellini	 | first2 =  Alfonso
 | last3 =  Goldhammer	 | first3 =  Robert K	 | doi=10.1029/pa001i004p00447 | bibcode=1986PalOc...1..447H}}
*{{cite journal
 | title = Facies e cicli della Dolomia Principale delle Alpi Venete
 | year = 1986
 | journal = Mem. Soc. Geol. Ital.
 | volume = 30
 | last1 = Bosellini | first1 = A
 | last2 = Hardie	 | first2 =  Lawrence A 
  | pages = 245–266}}

*{{cite journal | last = Hardie | first = Lawrence A | date = 1987 | title = Dolomitization; a critical view of some current views | journal = Journal of sedimentary research | volume = 57 | issue = 1 | pages = 166–183 | doi=10.1306/212f8ad5-2b24-11d7-8648000102c1865d}}
*{{cite journal
 | title = High frequency glacio-eustatic sealevel oscillations with Milankovitch characteristics recorded in Middle Triassic platform carbonates in northern Italy
 | year = 1987
 | journal = American Journal of Science
 | pages = 853–892
 | volume = 287
 | issue = 9
 | last1 = Goldhammer | first1 = Robert K
 | last2 = Dunn	 | first2 =  Paul A
 | last3 =  Hardie	 | first3 =  Lawrence A | doi=10.2475/ajs.287.9.853}}

*{{cite journal
 | title = Cyclic platform carbonates in the Cambro-Ordovician of the central Appalachians: Field Trip Guidebook T161
 | year = 1989
 | journal = 28th International Geol. Congress
 | pages = 51–88
 | last1 =  Hardie | first1 =  Lawrence A }}

=== 1990s ===

*{{cite journal
 | title = Depositional cycles, composite sea level changes, cycle stacking patterns and the hierarchy of stratigraphic forcing: Examples form the platform carbonates of the Alpine Triassic
 | year = 1990
| last1 = Goldhammer | first1 = Robert K
| last2 = Dunn | first2 = Paul A
 | last3 = Hardie | first3 = Lawrence A
 | journal = Geol. Soc. Am. Bull.
 | pages = 535–562
 | volume = 102
 | doi=10.1130/0016-7606(1990)102<0535:dccslc>2.3.co;2
 }}

*{{cite journal
 | title = The roles of rifting and hydrothermal CaCl 2 brines in the origin of potash evaporites; an hypothesis
 | year = 1990
 | last = Hardie | first = Lawrence A.
 | journal = American Journal of Science
 | pages = 43–106
 | volume = 290
 | issue = 1
 | doi=10.2475/ajs.290.1.43
 }}

*{{cite journal
 | title = Dolomitization front geometry, fluid flow patterns, and the origin of massive dolomite: the Triassic Latemar buildup, northern Italy
 | year = 1990
 | journal = American Journal of Science
 | pages = 741–796
 | volume = 290
 | issue = 7
 | last1 = Wilson	 | first1 =  Edith Newton
 | last2 = Hardie | first2 = Lawrence A
 | last3 =  Phillips	 | first3 =  Owen M	 | doi=10.2475/ajs.290.7.741}}

*{{cite journal
 | title = Depositional cycles, composite sea-level changes, cycle stacking patterns, and the hierarchy of stratigraphic forcing: examples from Alpine Triassic platform carbonates
 | year = 1990
 | journal = Geological Society of America Bulletin
 | pages = 535–562
 | volume = 102
 | issue = 5
 | last1 = Goldhammer | first1 =  Robert K
 | last2 = Dunn	 | first2 =  Paul A
 | last3 =  Hardie  | first3 =  Lawrence  A | doi=10.1130/0016-7606(1990)102<0535:dccslc>2.3.co;2}}

*{{cite journal
 | title = Control of seawater composition by mixing of river waters and mid-ocean ridge hydrothermal brines
 | year = 1990
 | journal = In Fluid-Mineral Interactions: A Tribute to H.P. Eugster, Spec. Publ. No. 2, ed. R.J. Spencer and I-M Chou, San Antonio
 | publisher = Geochemical Society
 | pages = 409–419
 | last1 = Spencer | first1 =  Ronald J
 | last2 =  Hardie  | first2 =  Lawrence  A}}

*{{cite journal
 | title = On the significance of evaporites
 | year = 1991
 | last = Hardie | first = Lawrence A
 | journal = Annual Review of Earth and Planetary Sciences
 | pages = 131–168
 | volume = 19
 | doi=10.1146/annurev.earth.19.1.131
 }}

*{{cite journal
 | title = Massive burial dolomitization: The Jurassic Vajont Oolite of northeast Italy
 | year = 1991
 | last = Zempolich | first = William G
 | last2 = Hardie | first2 = Lawrence A
 | journal = Dolomieu Conference on Carbonate Platforms and Dolomitization, Ortisei, The Dolomites, Italy
 | pages = 297
 }}

*{{cite journal
 | title = On the origin and significance of high frequency depositional cycles in shallow carbonate platforms
 | year = 1991
 | last = Hardie | first = Lawrence A
 | journal = Dolomieu Conference on Carbonate Platforms and Dolomitization, Ortisei, The Dolomites, Italy
 | pages = 102–103
 }}

*{{cite journal
 | title = Cyclostratigraphy and dolomitization of the Middle Triassic Latemar buildup, the Dolomites, northern Italy
 | year = 1991
 | last = Hardie | first = Lawrence A
 | last2 = Wilson | first2 = E Newton
 | last3 = Goldhammer | first3 = Robert K
 | journal = Guidebook, Excursion F, Dolomieu Conference on Carbonate Platforms and Dolomitization, Ortisei, The Dolomites, Italy
 | pages = 56
 }}

*{{cite journal
 | title = Field and Modelling Studies of Cambrian Carbonate Cycles, Virginia Appalachians: DISCUSSION
 | year = 1991
 | journal = Journal of Sedimentary Research
 | pages = 61
 | last1 = Hardie | first1 = Lawrence A
 | last2 = Dunn	 | first2 =  Paul A
 | last3 =  Goldhammer	 | first3 =  Robert K	 | doi=10.1306/d42677a3-2b26-11d7-8648000102c1865d | volume= 61}}

*{{cite journal
 | title = Sequence stratigraphy and system tract development of the Latemar platform, Middle Triassic of the Dolomites (northern Italy): Outcrop calibration keyed to cycle stacking patterns
 | year = 1993
 | journal = In Loucks, R.G. and Sarg, J.F. (eds.) Carbonate Sequence Stratigraphy, Recent Developments and Applications, AAPG Memoir 
 |volume = 57
 | pages = 353–388
 | last4 = Hardie | first4 = Lawrence A
 | last3 = Dunn	 | first3 =  Paul A
 | last2 = Harris | first2 = Mark T
 | last1 =  Goldhammer	 | first1 =  Robert K	}}

*{{cite journal | last = Demicco | first = Robert V | last2 = Hardie | first2 = Lawrence A 
| date = 1994 | title = Sedimentary structures and early diagenetic features of shallow marine carbonate deposits | journal = SEPM (Society for Sedimentary Geology) Atlas Series Number 1}}

*{{cite journal | last = Hardie | first = Lawrence A | date = 1996 | title = Secular variation in seawater chemistry: An explanation for the coupled secular variation in the mineralogies of marine limestones and potash evaporites over the past 600 my | journal = Geology | volume = 24 | issue = 3 | pages = 279–283 | publisher = Geological Society of America | doi=10.1130/0091-7613(1996)024<0279:svisca>2.3.co;2}}
*{{cite journal
 | title = Ore-forming brines in active continental rifts
 | year = 1997
 | journal = In Barnes, H.L. (ed.) Geochemistry of Hydrothermal Ore Deposits. 3rd Edition. John Wiley & Sons, Inc., New York
 | pages = 877–935
 | last2 = Hardie | first2 = Lawrence A
| last1 = McKibben | first1 = M A  }}

*{{cite journal
 | title = Biostratigraphic and radiometric age data question the Milankovitch characteristics of the Latemar cycle (Southern Alps, Italy): Comment and Reply
 | year = 1997
 | journal = Geology
 | pages = 470–472
 | volume = 25
 | issue = 5
 | last1 = Hardie | first1 = Lawrence A
| last2 = Hinnov | first2 = Linda A
 | doi=10.1130/0091-7613(1997)025<0470:baradq>2.3.co;2}}

*{{cite journal
 | title = Geometry of dolomite bodies within deep-water resedimented oolite of the Middle Jurassic Vajont Limestone, Venetian Alps, Italy: Analogs for hydrocarbon reservoirs created through fault-related burial dolomitization
 | year = 1998
 | last1 = Zempolich | first1 = William G
 | last2 = Hardie | first2 = Lawrence A 
 | journal = In J. Kupez, J. Gluyas and S. Bloch (eds.), Reservoir Quality Prediction in Sandstones and Carbonates, AAPG Memoir 
 | volume = 69
 | pages = 127–162
 }}

*{{cite journal | last = Stanley | first = Steven M | last2 = Hardie | first2 = Lawrence A |
date = 1998 | title = Secular oscillations in the carbonate mineralogy of reef-building and sediment-producing organisms driven by tectonically forced shifts in seawater chemistry |
journal = Palaeogeography, Palaeoclimatology, Palaeoecology | publisher = Elsevier | volume = 144 | issue = 1 | pages = 3–19 | doi=10.1016/s0031-0182(98)00109-6}}

*{{cite journal
 | title = Hypercalcification: paleontology links plate tectonics and geochemistry to sedimentology
 | year = 1999
 | last = Stanley | first = Steven M | last2 = Hardie | first2 = Lawrence A
 | journal = GSA Today
 | pages = 1–7
 | volume = 9
 | issue = 2
 }}

=== 2000s ===
* {{cite journal
 | title = Oscillations in Phanerozoic seawater chemistry: Evidence from fluid inclusions
 | year = 2001
 | journal = Science
 | pages = 1086–1088
 | volume = 294
 | issue = 5544
 | last = Lowenstein | first = Tim K
 | last2 = Timofeeff | first2 = Michael N 
 | last3 = Brennan | first3 = Sean T
 | last4 = Hardie | first4 = Lawrence A
 | last5 = Demicco | first5 =  Robert V	 | doi=10.1126/science.1064280 | pmid=11691988}}

*{{cite journal
 | title = Middle Triassic orbital signature recorded in the shallow-marine Latemar carbonate buildup (Dolomites, Italy)
 | year = 2001
 | journal = Geology
 | pages = 1123–1126
 | volume = 29
 | issue = 12
 | last1 = Preto	 | first1 =  Nereo
 | last2 = Hinnov | first2 = Linda A
 | last3 = Hardie | first3 = Lawrence A
 | last4 =  De Zanche	 | first4 =  Vittorio | doi=10.1130/0091-7613(2001)029<1123:mtosri>2.0.co;2}}

*{{cite journal
 | title = Low-magnesium calcite produced by coralline algae in seawater of Late Cretaceous composition
 | year = 2002
 | journal = Proceedings of the National Academy of Sciences
 | pages = 15323–15326
 | volume = 99
 | issue = 24
 | last1 = Stanley | first1 = Steven M
 | last2 = Ries	 | first2 =  Justin B
 | last3 =  Hardie | first3 =  Lawrence A | doi=10.1073/pnas.232569499 | pmid=12399549 | pmc=137715}}

*{{cite journal
 | title = The `carbonate factory' revisited: A reexamination of sediment production functions used to model deposition on carbonate platforms
 | year = 2002
 | last1 = Demicco | first1 = Robert V
 | last2 = Hardie | first2 =  Lawrence A
 | journal = Journal of Sedimentary Research
 | pages = 849–857
 | volume = 72
 | issue = 6
 | doi=10.1306/041502720849
 }}

*{{cite journal
 | title = Secular variation in seawater chemistry and the origin of calcium chloride basinal brines
 | year = 2003
 | journal = Geology
 | pages = 857–860
 | volume = 31
 | issue = 10
 | last = Lowenstein | first = Tim K
 | last3 = Timofeeff | first3 = Michael N 
 | last2 = Hardie | first2 = Lawrence A
 | last4 = Demicco | first4 =  Robert V	 | doi=10.1130/g19728r.1}}

*{{cite journal
 | title = Atmospheric pCO2 since 60 Ma from records of seawater pH, calcium, and primary carbonate mineralogy
 | year = 2003
 | journal = Geology
 | pages = 793–796
 | volume = 31
 | issue = 9
 | last1 = Demicco | first1 = Robert V
 | last2 = Lowenstein	 | first2 =  Tim K.
 | last3 =  Hardie	 | first3 =  Lawrence A | doi=10.1130/g19727.1}}

*{{cite journal
 | title = Third-order depositional sequences controlled by synsedimentary extensional tectonics: evidence from Upper Triassic carbonates of the Carnian Prealps (N.E. Italy)
 | year = 2003
 | journal = Terra Nova
 | pages = 40–45
 | volume = 15
 | issue = 1
 | last1 = Cozzi | first1 = Andrea
 | last2 =  Hardie | first2 =  Lawrence A
 | doi=10.1046/j.1365-3121.2003.00461.x}}

*{{cite journal
 | title = Secular variations in Precambrian seawater chemistry and the timing of Precambrian aragonite seas and calcite seas
 | year = 2003
 | last = Hardie | first = Lawrence A.
 | journal = Geology
 | pages = 785–788
 | volume = 31
 | issue = 9
 | doi=10.1130/g19657.1
 }}

*{{cite journal
 | title = Anhydrite and Gypsum
 | year = 2003
 | last = Hardie | first = Lawrence A.
 | journal = in Middleton, G. (ed.), Encyclopedia of Sediments and Sedimentary Rocks, Kluwer Academic Publishers, Dordtrecht, Holland.
 | pages = 16–19
 }}

*{{cite journal
 | title = Evaporites
 | year = 2003
 | last = Hardie | first = Lawrence A.
 | journal = in Middleton, G. (ed.), Encyclopedia of Sediments and Sedimentary Rocks, Kluwer Academic Publishers, Dordtrecht, Holland.
 | pages = 257–263
 }}

*{{cite journal
 | title = Sabkha, Salt Flat, Salina
 | year = 2003
 | last = Hardie | first = Lawrence A.
 | journal = in Middleton, G. (ed.), Encyclopedia of Sediments and Sedimentary Rocks, Kluwer Academic Publishers, Dordtrecht, Holland.
 | pages = 584–585
 }}

*{{cite journal
 | title = Did the Mediterranean Sea dry out during the Miocene? A reassessment of the evaporite evidence from DSDP Legs 13 and 42A cores
 | year = 2004
 | last1 = Hardie | first1 = Lawrence A
 | last2 = Lowenstein | first2 =  Tim K
 | journal = Journal of Sedimentary Research
 | pages = 453–461
 | volume = 74
 | issue = 4
 | doi=10.1306/112003740453
 }}

*{{cite journal
 | title = Seawater chemistry, coccolithophore population growth, and the origin of Cretaceous chalk
 | year = 2005
 | journal = Geology
 | pages = 593–596
 | volume = 33
 | issue = 7
 | last1 = Stanley | first1 = Steven M
 | last2 = Ries	 | first2 =  Justin B
 | last3 =  Hardie	 | first3 =  Lawrence A | doi=10.1130/g21405.1}}

*{{cite journal
 | title = Model of seawater composition for the Phanerozoic
 | year = 2005
 | journal = Geology
 | pages = 877–880
 | volume = 33
 | issue = 11
 | last1 = Demicco | first1 = Robert V
 | last2 = Lowenstein | first2 = Tim K 
 | last3 = Hardie | first3 =  Lawrence A
 | last4 =  Spencer	 | first4 =  Ronald J.	 | doi=10.1130/g21945.1}}

*{{cite journal
 | title = Orbitally forced Lofer cycles in the Dachstein Limestone of the Julian Alps (northeastern Italy)
 | year = 2005
 | journal = Geology
 | pages = 789–792
 | volume = 33
 | issue = 10
 | last1 = Cozzi	 | first1 =  Andrea
 | last2 = Hinnov	 | first2 =  Linda A
 | last3 =  Hardie	 | first3 =  Lawrence A	 | doi=10.1130/g21578.1}}

*{{cite journal
 | title = Scleractinian corals produce calcite, and grow more slowly, in artificial Cretaceous seawater
 | year = 2006
 | journal = Geology
 | pages = 525–528
 | volume = 34
 | issue = 7
 | last1 = Ries | first1 = Justin B
 | last2 = Stanley	 | first2 =  Steven M
 | last3 =  Hardie	 | first3 =  Lawrence A | doi=10.1130/g22600a.1}}

*{{cite journal
 | title = Increased production of calcite and slower growth for the major sediment-producing alga Halimeda as the Mg/Ca ratio of seawater is lowered to a 'calcite sea' level.
 | year = 2010
 | journal = Journal of Sedimentary Research
 | pages = 16
 | volume = 1
 | issue = 6
 | last1 = Stanley | first1 = Steven M
 | last2 = Ries | first2 = Justin B
 | last3 = Hardie | first3 = Lawrence A
 | doi=10.2110/jsr.2010.011
}}

==References==
<references />

{{DEFAULTSORT:Hardie, Lawrence Alexander}}
[[Category:American geologists]]
[[Category:Johns Hopkins University faculty]]
[[Category:American geochemists]]
[[Category:Johns Hopkins University alumni]]
[[Category:1933 births]]
[[Category:2013 deaths]]