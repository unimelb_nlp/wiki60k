{{redirect|AM-34|the United States Navy minesweeper|USS Swan (AM-34)}}
{|{{Infobox Aircraft Begin
 |name = AM-34
 |image = AM-engine.jpg
 |caption = Mikulin M-34, 1932
}}{{Infobox Aircraft Engine
 |type=Liquid-cooled [[V12 engine]]
 |designer=[[Alexander Mikulin]]
 |national origin=[[Soviet Union]]
 |manufacturer=
 |first run=1931
 |major applications=[[Tupolev TB-3]]
 |number built =10,538   
 |produced = 1934–43
 |unit cost = 
 |developed from = 
 |developed into = [[Mikulin AM-35]]
 |variants with their own articles = 
}}
|}

The '''Mikulin AM-34 (M-34)''' was a [[Soviet Union|Soviet]] mass-produced, liquid-cooled, aircraft engine.<ref>Gunston 1989, p. 103.</ref> Before the war, the Russian aero engine industry was mainly engaged in producing engines of foreign design, notably Wright, Bristol, Hispano-Suiza, and Gnome-Rhône. Several engines of so-called original design have been developed, although these were probably largely based on foreign models. The M-34 was thought to have been originally designed in Italy by [[Fiat Aviazione|Fiat]] for the Russians; its design closely follows Italian in-line aero engine practice.<ref>SST Chobhan Lane Chertsey, Report on Russian C.I. Tank Engine, Report No. 0049989, May 1944</ref><ref>Armour in Profile, Brereton 1967 p. 3</ref> <ref group=Notes>Its origin design remaining in question, available evidence points to Italian origin, since the V2 adheres very closely to it but was recently discovered to be an original design by Mikulin.</ref> Its initial development was troubled, but it eventually became one of the most successful Soviet aircraft engines of the 1930s. It was utilized on the [[Beriev MBR-2]], [[Tupolev TB-3]], [[Tupolev TB-4]], [[Tupolev ANT-20]], [[Petlyakov Pe-8]], [[Kalinin K-7]], [[Polikarpov I-17]], and [[Bolkhovitinov DB-A]] aircraft as well as the [[G-5 class motor torpedo boat|G-5]] and various prototype [[motor torpedo boat]]s. A version of the maritime model was adapted for use in several prototype heavy tanks in 1939, although none were placed into production.

==Design and development==
The M-34 began development in 1928 as a replacement for the [[Mikulin M-17]], a license-built copy of the [[BMW VI]]. It had similar dimensions and attachment points, but was otherwise an entirely new design. It was a direct-drive, block-type engine with the cylinder block connected by long internal studs with centrally coupled [[Connecting rod#Compound rods|articulated connecting rods]]. The development of the engine process was prolonged, with the engineering drawings not completed until April 1931. The first engine was delivered to [[Baranov Central Institute of Aviation Motors development|TsIAM]] (Tsentralniy Institut Aviatsionnogo Motorostroeniya, Russian: Центральный Институт Авиационного Моторостроения) on 21 September 1931 for bench testing with imported [[carburetor]]s and [[ignition magneto|magneto]]s. It began state testing in November 1931, but failed. It was submitted again a year later with [[Soviet Union|Soviet]]-designed K-34 carburetors, but was again rejected. It was resubmitted in January 1933, but again failed. It was flight-tested in a [[Tupolev TB-3]] in October 1933.<ref name=k2>Kotelnikov, p. 102</ref>

Despite these failures it began production in 1932 at Factory No. 24 in [[Moscow]], and 64 engines had been delivered by the end of the year. 790 were built the following year, and it was exhibited in Paris as an achievement of the Soviet aviation industry. The M-34 was redesignated with Alexsander Mikulin's initials as the AM-34 on 9 August 1936 in honor of his achievement.<ref name=k2/>

The M-34 was used in an unusual system, first tried by Imperial Germany in 1918 with a [[Zeppelin-Staaken R.VI]], that used an external [[supercharger]] to supply pressurized air to the aircraft's M-34FRN engines. The first installation, designated ATsN-1 (''Agregat tsentral'novo nadduva'' – central boosting unit), used an auxiliary M-34 fitted inside the fuselage to drive a central supercharger with ducts leading to the engines in the wings. This was flight-tested in a Tupolev TB-3 in 1935. It was adapted for use in a Petlyakov Pe-8 bomber prototype with a smaller [[Hispano-Suiza 12Y#Foreign derivatives|Klimov M-100]] engine substituted for the M-34 as the ATsN-2. It was flight-tested during 1938–39, but was not approved for production.<ref>Kotelnikov, pp. 105–106</ref> The same idea was revived in 1943 by Nazi Germany with the [[Henschel Hs 130]]E bomber prototype series, with the ''Höhen-Zentrale Anlage'' unit.

Like the BMW VI and the Mikulin M-17, the AM-34FRN and subsequent models used [[Connecting rod#Compound rods|articulated connecting rods]] which caused a different stroke of {{convert|190|mm|abbr=on}} and {{convert|199|mm|abbr=on}} between right and left cylinder bank. The displacement was 46.9&nbsp;l (2,863.7 in<sup>3</sup>). Combined with a number of other changes power significantly increased in most models to {{convert|1200|hp}}.<ref>Kotelnikov, p. 106</ref>

Development of a version for [[motor torpedo boat]]s began in 1932 as the GM-34, but it did not pass its state tests until December 1934, although it was put into production that same year. It was given a reversing gear, a free-wheel sleeve, and its cooling and exhaust systems were modified. Production continued through 1943 with the GM-34s adapting features from the aviation models. With the exception of the GAM-34BP and the original GM-34, all maritime engines used a [[benzene]]-[[alcohol fuel]] mixture.<ref>Kotelnikov, pp. 106, 108</ref>

A version of the GM-34 was adapted for use in heavy tanks in 1939 as the GAM-34BT, although only small numbers were built. It was mounted in the prototypes of the [[T-100 tank|T-100]] and [[SMK tank|SMK]] heavy tanks and the [[SU-100Y Self-Propelled Gun|SU-100y]] [[self-propelled gun]], none of which were put into production. The cooling system was modified with an external fan, and it was given new gearing. An electric starter was used rather than the original pneumatic one. It was rated at {{convert|850|hp|abbr=on}}.<ref>Kotelnikov, p. 108</ref>

==Variants==
;M-34
:First production version. Direct drive. Early engines used imported [[Zenith Carburetters|Zenith]] 90R carburetors, although later ones used indigenous K-34 carburetors. In production until the end of 1939. Rated at {{convert|800|hp|abbr=on}} with a weight of {{convert|680|kg|abbr=on}}.
;Coupled M-34
:Two engines driving one propeller, project for the [[Kalinin K-7]], no production.
;M-34F
:small batch built in 1933 with a rating at {{convert|830|hp|abbr=on}}.
;M-34R
:R for ''Reduktor'' or reduction gear. Rated at {{convert|800|hp|abbr=on}} with a weight of {{convert|670|kg|abbr=on}}. Passed its state trials in May 1933 and in production from the end of that year to the end of 1939.
;M-34RD
:D for ''Dal'niy'' or long-range. Ten engines built with special attention to quality, smaller tolerances, and K-34RD carburetors to equip the [[Tupolev ANT-25]] record-breaking aircraft. RPMs were boosted to give a power of 830 hp. Later fifty more were built to power [[Tupolev TB-3]] bombers converted to VIP transports.
;M-34N
:N for ''Nagnetatel{{'}}'' or supercharged. Development began in 1931 of this direct-drive model, but the first two-stage supercharger design proved to be quite unreliable. A single-speed replacement was developed at TsIAM and tested in November 1933 and production began in September 1934. The [[centrifugal type supercharger]] was designed as a removable module and could be installed on other versions of the M-34. Rated at {{convert|820|hp|abbr=on}}. A PTK steam-powered supercharger was developed and tested from 1938 to 1940, but was not accepted for production.
;M-34RN
:This geared model used the same geared centrifugal supercharger (GCS) as the M-34N and had the same rating. It failed state testing in September 1934 when the pistons burned through.
;M-34NA
:A version of the M-34N with minor changes to some components to extend service life. It had the same power as the original model.
;M-34RA
:A version of the M-34R with the same changes and power as the M-34NA.
;M-34RNA
:A version of the M-34RN with all the changes introduced on the NA and RA models. Same power as before, but weighed {{convert|748|kg|abbr=on}}. Flight-tested on a TB-3 in May 1935 and production began at the end of the year.
;M-34NB
:The NA fitted with a strengthened [[crankcase]], a lightened [[crankshaft]] with a modified nose and a refined supercharger. The power remained the same, but the weight dropped to {{convert|638|kg|abbr=on}}. A TK-1 [[turbocharger]] was tested with the prototype on a [[Polikarpov R-Z]] in 1936.
;M-34RNB
:A geared equivalent of the NB model with a lightened reduction gear. The power remained the same, but the weight dropped to {{convert|725|kg|abbr=on}}. In production from October 1935 until the end of 1939.
;M-34P
:P for ''Pushechniy'' or cannon. A version of the M-34RN adapted to mount an [[autocannon]] in the V between the cylinder banks that fired through a hollow gear shaft. The specification was issued in August 1934, but no further information is known.
;M-34NV
:NV for ''Neposredstvenniy Vprysk'' or fuel-injected. It passed its bench tests in 1935 and was flight-tested in 1937, but was not accepted for production. Rated at {{convert|985|hp|abbr=on}}.
;M-34RNV
:Geared version similar to the NV.
;AM-34RNV-TK
:Prototype built in 1938, similar to the RNV with the addition of a TK-1 turbocharger. Rated at {{convert|850|hp|abbr=on}} and an estimated weight of {{convert|810|kg|abbr=on}}.
;AM-34RS
:Prototype built in 1938 with mixed cooling; air-cooled sleeves, and [[ethylene glycol]]-cooled [[cylinder head]]s. Rated at {{convert|1200|hp|abbr=on}}.
;AM-34NF
:Prototype with a geared centrifugal supercharger and a TK-1 supercharger. Flight-tested in a TB-3. Compression ratio of 6.6:1 and rated at {{convert|985|hp|abbr=on}}.
;M-34N2B
:A prototype with a supercharger, two turbochargers, and four K-4 carburetors. Rated at {{convert|1030|hp|abbr=on}}.
;AM-34FRN
:F for ''Forsirovanniy'' or boosted. Development began in 1934, but development was not completed until 1938. The crankshaft, crank case, gearing, and the side joints of the connecting rods were reinforced. The lubrication system was modified, the supercharger improved, and a new gas-distribution system was fitted. The carburetors were moved to behind the supercharger. Rated at {{convert|1200|hp|abbr=on}} and the weight dropped to {{convert|690|kg|abbr=on}}. It was exhibited in the 1937 [[Paris Air Show]].
;AM-34FRNA
:First main production version of the FRN model. Equipped with four carburetors.
;AM-34FRNB
:Next production model of the FRN. Equipped with six carburetors. A small batch was adapted for the ATsN-2 system with pressurized air provided by an external supercharger and flight-tested in a Pe-8 in 1938–39.
;AM-34FRNV
:The last main production variant of the AM-34. Built during 1938–39. The crankcase was modified and bronze bushings were used for the main supports. It had a longer crankshaft nose, four K-4 carburetors, and a modified lubrication system. The valve castings were made of magnesium alloy. The power remained the same, but the weight increased to {{convert|763.5|kg|abbr=on}}. Variants with fuel injection and two TK-1 turbochargers were tested, but not put into production.
;AM-34RB
:Superchargers were removed from AM-34RNB engines during 1938–39 to create this model.

===Maritime variants===
;GM-34
:Intended for motor torpedo boats. Rated at {{convert|800|hp|abbr=on}} with a weight of {{convert|864|kg|abbr=on}}.
;GAM-34F
:A boosted version of the GM-34 it had bronze main [[Bushing (bearing)|bushings]] and used the [[crankshaft]], block, heads, and some other components of the AM-34FRNV aircraft engine. Rated at {{convert|1000|hp|abbr=on}} with a weight of {{convert|1080|kg|abbr=on}} with a [[compression ratio]] of 7.3:1.
;GAM-34FN
:A version of the GAM-34F with a FN-25 geared centrifugal supercharger and one K-4 carburetor. It used the oil system of the GAM-34BS and was in production from August 1939.
;GAM-34BP
:Intended for armored river boats.
;GAM-34BS
:An improved version of the GAM-34BP. Rated at {{convert|850|hp|abbr=on}} with a weight of {{convert|1045|kg|abbr=on}}.

==Applications==

===Aviation===
*[[Beriev MBR-2]]
*[[Bolkhovitinov DB-A]]
*[[Kalinin K-7]]
*[[Petlyakov Pe-8]]
*[[Polikarpov I-17]]
*[[Tupolev ANT-20]]
*[[Tupolev TB-3]]
*[[Tupolev TB-4]]

===Maritime===
*[[G-5 class motor torpedo boat|G-5 motor torpedo boat]]
*[[G-6 motor torpedo boat]]
*[[G-8 motor torpedo boat]]
*[[SM-4 motor torpedo boat]]
*[[Project 1124 armored river boat]]
*[[Project 1125 armored river boat]]

===Tanks===
*[[T-100 tank|T-100]]
*[[SMK tank|SMK]]
*[[SU-100Y Self-Propelled Gun|SU-100y]]

==Specifications (AM-34R)==
{{pistonspecs|
|ref=''Kotelnikov''.<ref name=k2>Kotelnikov, p. 102</ref>
|type=12-cylinder 60° [[V engine|Vee]] aircraft engine
|bore=160 mm (6.30 in)
|stroke=190/199 mm (7.48/7.83 in) different between right and left cylinder bank due to [[Connecting rod#Compound rods|articulated connecting rods]].
|displacement=46.9 L (2,864 in³)
|length=
|diameter=
|width=
|height=
|weight={{convert|670|kg|abbr=on}}
|valvetrain=
|supercharger=
|turbocharger=
|fuelsystem=[[Carburetor]]
|fueltype=
|oilsystem=
|coolingsystem=water-cooled
|power={{convert|800|hp|kW|abbr=on}}
|specpower=13.42 kW/L (0.30 hp/in³)
|compression=6.0:1
|fuelcon=
|specfuelcon=
|oilcon=
|power/weight=0.92 kW/kg (0.56 hp/lb)
}}

==See also==
{{aircontent
<!-- first, the related articles that do not fit the specific entries: -->
|see also=

<!-- designs which were developed into or from this aircraft: -->
|related=
*[[Mikulin AM-35]]
<!-- aircraft that are of similar role, era, and capability as this design: -->
|similar aircraft=

<!-- relevant lists that this aircraft appears in: -->
|lists=
*[[List of aircraft engines]]
<!-- For aircraft engine articles.  Engines that are of similar to this design: -->
|similar engines=

<!-- See [[WP:Air/PC]] for more explanation of these fields. -->
}}

==References==
;Notes
{{reflist|group=Notes}}
;Citations
{{Reflist|1}}

===Bibliography===
{{refbegin}}
* {{cite book|last=Gunston|first=Bill|authorlink=Bill Gunston|title=World Encyclopedia of Aero Engines|publisher=Patrick Stephens Limited|location=Cambridge, England|year=1989|isbn=1-85260-163-9}}
* {{cite book|last=Kotelnikov|first=Vladimir|title=Russian Piston Aero Engines|publisher=Crowood Press|location=Marlborough, Wiltshire|year=2005|isbn=1-86126-702-9}}
{{refend}}

{{Mikulin aeroengines}}

[[Category:Mikulin aircraft engines]]
[[Category:Aircraft piston engines 1930–1939]]