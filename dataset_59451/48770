{{advert|date=January 2017}}
{{Use dmy dates|date=September 2015}}
{{Use Indian English|date=September 2015}}
{{Infobox company
| name                 = Intex Technologies (India) 
| type                   = [[Limited Company]]
| logo                   = Intex logo.png
| foundation         = {{start date and age|df=yes|1996}}<br>New Delhi India
| founder              = Narendra Bansal
| key_people          = [[Keshav Bansal]], Director
| no. of employees         = 13,000+
| area_served        = Worldwide
| location              = [[New Delhi]], [[Delhi]], [[India]]
| industry              = [[Telecommunications]], [[Consumer Electronics]]
| products             = [[Mobile phone]]s, [[Smartphone]]s, [[Washing machines]] , [[LED]] [[television]]s, Audio Equipment, UPS, consumer durables
webcams, security cameras, refrigerators

| revenue               = {{decrease}} {{₹}}6,200 Cr (USD$ 928 Million) 
<ref>{{cite news|title= Intex Technologies expects 50% of revenue from consumer durables by 2017-end|url= http://articles.economictimes.indiatimes.com/2016-06-14/news/73767889_1_intex-technologies-durables-mobile-phones}}</ref>

| homepage               = http://www.intex.in
}}
'''Intex Technologies''', founded in 1996, is an Indian [[smartphone]], consumer durables and IT Accessories manufacturer. Intex is headquartered in [[New Delhi]], [[India]].<ref>{{cite news|title=Intex Phones |url= http://gadgets.ndtv.com/mobiles/intex-phones |work=NDTV Convergence Ltd}}</ref><ref>{{cite news|title= Company Overview of Intex Technologies (India) Ltd|url= http://investing.businessweek.com/research/stocks/private/snapshot.asp?privcapId=39599696}}</ref>

Intex manufactures subpar quality [[Smartphones]], IT accessories and [[consumer electronics]] durables.<ref>{{cite news|title=Intex Phones |url= http://timesofindia.indiatimes.com/topic/Intex |work= Bennett, Coleman & Co. Ltd }}</ref>

Intex is an ISO 9001:2008 certified company. It has a portfolio of more than 16 product categories ranging from mobile handsets, wearable devices, home theatre solutions, multimedia speakers, and DVD players, LED / LCD TVs, induction cookers, washing machines and more.<ref>{{cite news|title= Intex Styles to strengthen presence in north India |url= http://www.business-standard.com/article/companies/intex-styles-to-strengthen-its-presence-in-north-india-114090200371_1.html }}</ref><ref>{{cite news|title= Intex Technologies aims at Rs 2,000 cr revenue in FY14 |url= http://articles.economictimes.indiatimes.com/2013-05-12/news/39203755_1_handset-market-crore-revenue-mobile-business-sanjay-kumar }}</ref><ref>{{cite news|title= Intex launches new 29-inch HD LED TV for Rs 18,990|url= http://www.moneycontrol.com/news/technology/intex-launches-new-29-inch-hd-led-tv-for-rs-18990_842216.html }}</ref><ref>{{cite news|title= Intex Technologies to expand consumer durables range; eyes Rs 2000 crore sales|url= http://economictimes.indiatimes.com/industry/cons-products/durables/intex-technologies-to-expand-consumer-durables-range-eyes-rs-2000-crore-sales/articleshow/25257099.cms}}</ref>

India's first [[Firefox OS]] powered mobile phone, [[Intex Cloud FX]], was launched by Intex.<ref>{{cite news|title=Intex Cloud FX & Firefox OS review: Half-baked alternative|url= http://timesofindia.indiatimes.com/tech/reviews/Intex-Cloud-FX-Firefox-OS-review-Half-baked-alternative/articleshow/42043736.cms}}</ref>
<ref>{{cite news|title=Now, Mozilla rolls out smartphones at Rs 1,999|url= http://www.business-standard.com/article/companies/mozilla-offers-smartphone-at-rs-1-999-in-bid-to-capture-indian-market-114082500891_1.html}}</ref>
India's first Linux-based [[Sailfish OS]] powered mobile phone, the [[Aqua Fish (mobile phone)|Aqua Fish]] was launched by Intex<ref name=IntexBrand>{{cite news|last1=Intex Technologies|title=Intex Technologies on Twitter|url=https://mobile.twitter.com/IntexBrand/status/702398248401637376|accessdate=24 February 2016}}</ref> after introducing it during [[World Mobile Congress|World Mobile Congress 2016]] at [[Barcelona]] in [[Spain]].

==History==

Intex Technologies (India) Ltd. was incepted in the year 1996. by Mr. (Narendra bansal), the founder and CMD of Intex Technologies. 
It started in 1990s with trading of Audio and Video cassettes. In 1993 International Impex was formed which dealt in floppy disks, ethernet cards and other accessories. In 1996, Intex Technologies was formed. The name came from International Impex, the first company of Mr. Narendra Bansal. By 2000, Intex introduced multimedia speakers and expanded its business outside Delhi. Overall company's turnover reached 32 Crores by 2003. By 2006 consumer electronics, with offerings like DVD players, LED/ LCD TVs, induction cookers, home theatre solutions were introduced to the business. Intex technologies started its mobile phone business in the year 2007.<ref>{{cite news|title= Narendra Bansal's Intex Technologies: How the company turned into Rs 1,070 cr electronics behemoth|url= http://articles.economictimes.indiatimes.com/2013-09-02/news/41689046_1_intex-technologies-delhi-university-wholesale-market/ |work= Bennett, Coleman & Co. Ltd }}</ref>

Intex Technologies achieved a turnover of Rs 4000 crore in FY14-15.<ref>{{cite news|title= Intex aims to double turnover to $1.5 bn|url= http://www.business-standard.com/article/news-ians/intex-aims-to-double-turnover-to-1-5-bn-115080300565_1.html}}</ref>

Intex has signed up with [[Mukesh Ambani]]-led [[Reliance Jio Infocomm]] to supply [[4G]] handsets. These handsets will be enabled with [[VoLTE|voice over LTE]] feature.
<ref>{{cite news|title= Intex ties up with Reliance Jio to supply 4G handsets |url= http://indianexpress.com/article/technology/tech-news-technology/intex-ties-up-with-reliance-jio-to-supply-4g-handsets/}}</ref>

==R&D and Manufacturing==
The company has Centre for Research and Development in [[India]] and [[China]]. Intex's manufacturing domain comprises factories in India and China manufacturing various products.
<ref>{{cite news|title= Intex Tech plans to invest ₹500 cr in smartphone unit
 |url=http://www.thehindubusinessline.com/features/smartbuy/tech-news/intex-tech-plans-to-invest-500-cr-in-smartphone-unit/article6350765.ece}}</ref>

==Distribution and Global Presence==
Intex has 29 branches - stock and sales offices, a distribution network comprising 1,100+ distributors and 80,000+ dealers spread across the country. Products are also available at more than 250 dedicated counters of reputed chains of hyper markets and specialty stores across the country, on TV shopping channels and e-commerce sites.
<ref>{{cite news|title= Intex Launches Aqua Y2 Pro in India at Rs. 4,333
 |url=http://www.communicationstoday.co.in/index.php?option=com_content&task=view&id=12644&Itemid=147}}</ref>
Intex has a Pan-India network of sales and service centers comprising 29 branches and over 900 service touch points.
<ref>{{cite news | title= ntex Mobile appoints Sanjeev Kumar as AGM, media planning|url=http://www.afaqs.com/news/story/43636_Intex-Mobile-appoints-Sanjeev-Kumar-as-AGM-media-planning}}</ref>
<ref>{{cite news | title= Mobile phone-maker Intex Tech turns to branding, marketing
|url=http://www.thehindubusinessline.com/features/smartbuy/mobile-phonemaker-intex-tech-turns-to-branding-marketing/article5719260.ece}}</ref>
Intex has a global presence with its products available in more than 90 countries.
<ref>{{cite news | title= Intex To Start Manufacturing Mobile Phones In India |url=http://www.cxotoday.com/story/intex-to-start-manufacturing-mobile-phones-in-india/}}</ref>
<ref>{{cite news | title= Intex To Set Up Its Mobile Manufacturing Unit In India |url=http://www.techtree.com/content/news/7275/intex-set-mobile-manufacturing-unit-india.html}}</ref>
<ref>{{cite news | title= Intex Mobile appoints Sanjeev Kumar as AGM, media planning|url=http://www.afaqs.com/news/story/43636_Intex-Mobile-appoints-Sanjeev-Kumar-as-AGM-media-planning}}</ref>

==Awards and Accreditations==
'''ICONIC IDC Insights Award 2016'''

Intex was honored with the esteemed ‘ICONIC IDC Insights Award 2016’ for Excellence in Operations attained through technology in Manufacturing & Logistics vertical.<ref>{{Cite news|url=http://www.free-press-release.com/news-intex-technologies-wins-iconic-idc-insights-award-2016-1482837027.html|title=Intex Technologies Wins ICONIC IDC Insights Award 2016|newspaper=Free Press Release|access-date=2016-12-29}}</ref>

'''Extraordinaire Awards by Times Now 2016'''

Intex was awarded the Extraordinaire Award at a special ceremony- Brand Vision Summit held in Mumbai organized by Nex Brands and Times Now.<ref>{{Cite web|url=http://ncnonline.net/news-in-brief/intex-honoured-with-extraordinaire-awards-by-times-now.html|title=Intex honoured with Extraordinaire Awards by Times Now {{!}} NCNONLINE|first=|date=|website=ncnonline.net|publisher=|access-date=2016-12-09}}</ref>

'''India’s Most Trusted Brands Award 2016'''

Intex was awarded 'India’s most trusted brands award 2016' under the mobile category by IBC InfoMedia.<ref>{{Cite web|url=http://www.newsvoir.com/release/intex-wins-indias-most-trusted-brands-award-2016-7794.html|title=Intex Wins India’s Most Trusted Brands Award 2016|website=www.newsvoir.com|access-date=2016-12-02}}</ref>

'''Online Business Awards – Brand Excellency Awards 2016'''<ref>{{cite news | title= Intex Wins Brand Excellency Awards 2016 by Shop CJ|website=UpRazor.com|publisher=|access-date=2016-09-20 |url=https://uprazor.com/news/technology/intex-wins-brand-excellency-awards-2016-by-shop-cj}}</ref><br>
Online Business Awards – Brand Excellency Awards 2016 by leading retail website, Shop CJ as part of its 6th anniversary celebrations. Intex was honored with two awards viz. ‘Top Performing Brand’ and ‘Brand of the Year’ at a glittering event.

Intex Technologies in November 2015 became India's No. 1 mobile handset brand.
<ref>{{cite news | title= Intex pips Micromax as India's No. 1 mobile handset brand |url=http://articles.economictimes.indiatimes.com/2015-11-23/news/68510096_1_micromax-intex-technologies-handset}}</ref>

'''The Fastest Growing Brand in India in Telecom Sector Award 2016''' <br>
Intex was awarded "The Fastest Growing Brand in Indian Telecom Sector Award" at World Brands Summit held in Dubai.
<ref>{{cite news | title= Intex Bags ‘The Fastest Growing Brand in India in Telecom Sector Award’ |url=http://digitalterminal.in/news/intex-bags-the-fastest-growing-brand-in-india-in-telecom-sector-award/6714.html}}</ref>

'''The Golden Globe Awards 2016''' <br>
Intex Smart World was awarded Golden Globe Awards 2016 for ‘Market Leadership’ and ‘Operational Excellence’.
<ref>{{cite news | title= Intex Smart World Wins Another Retail Industry Award |url=http://ncnonline.net/news-in-brief/intex-smart-world-wins-another-retail-industry-award.html}}</ref>

'''Retail Excellence Award 2016''' <br>
Intex Smart World bagged two awards for ‘Innovative Retail Concept of the Year’ and ‘Impactful Retail Design & Visual Merchandising’. 
<ref>{{cite news | title= Intex Smart World Wins Retail Excellence 2016 Award |url=http://www.techupdate3.com/2016/02/intex-smart-world-wins-retail.html}}</ref>

'''Mobility Excellence Award 2015''' - New Delhi <br>
- Intex claimed top three honors at the first edition of Mobility Excellence Awards 2015. <br>
- Intex CLOUD FX was selected as the Best Firefox Smartphone by Jury for being Asia’s first Firefox & India’s lowest priced smart phone
<ref>{{cite news | title= 1st Mobility Excellence Awards Ceremony held in New Delhi |url=http://www.newkerala.com/news/2015/fullnews-8261.html}}</ref>

'''Mobby's Award 2015''' - New Delhi <br>
(MOBBY’S Awards (Awards for Excellence in Mobile Entertainment & Digital Technology) - 2015
BEST USE OF SOCIAL MEDIA IN MARKETING 
<ref>{{cite news | title= 1st Mobility Excellence Awards Ceremony held in New Delhi |url=http://www.worldmarketingcongress.org/mobile&digitalmarketingsummit/awards_winner_2015.html}}</ref>

==Advertising Campaigns==

[[Suresh Raina]] has been currently roped in as Intex's new brand ambassador for speaker's segment.
<ref>{{cite news|title= Intex ropes in Suresh Raina as brand ambassador for speakers' segment
|url=http://wap.business-standard.com/article-amp/news-ians/intex-ropes-in-suresh-raina-as-brand-ambassador-for-speakers-segment-116092400419_1.html
}}</ref>

2016, Intex roped in [[Bollywood]] actor [[Madhuri Dixit]] for its consumer products for two years.
<ref>{{cite news|title= Intex appoints Madhuri Dixit as brand ambassador for consumer durable products 
|url=http://economictimes.indiatimes.com/industry/cons-products/durables/intex-appoints-madhuri-dixit-as-brand-ambassador-for-consumer-durable-products/articleshow/53418805.cms
}}</ref>

From 2013, [[Bollywood]] actor [[Farhan Akhtar]] was contracted as a brand ambassador of Intex.<ref>{{cite news|title= Intex ropes in Farhan Akhtar as ambassador for smartphones |url= http://articles.economictimes.indiatimes.com/2013-05-09/news/39143920_1_intex-brand-ambassador-mobile-business
}}</ref>

Intex, in the year 2014 also appointed leading superstar of Kannada film industry, Sudeep as its new brand ambassadors for better association with South Indian customers <ref>{{cite news|title= Mobile phone maker Intex ropes in Kannada actor Sudeep |url=http://www.thehindubusinessline.com/features/smartbuy/mobile-handsets/intex-ropes-in-kannada-actor-sudeep/article6308566.ece}}</ref>

In August 2015, Telugu Superstar [[Mahesh Babu]] was roped in as a brand ambassador with the launch of a new Intex smartphone Intex Aqua Trend.
<ref>{{cite news|title= Intex Technologies ropes in Mahesh Babu as brand ambassador|url= http://www.thehindubusinessline.com/features/smartbuy/tech-news/intex-technologies-ropes-in-mahesh-babu-as-brand-ambassador/article7495458.ece}}</ref>

[[Suriya]], Tamil superstar was roped in by Intex as a brand ambassador for Tamil Nadu, in August 2015.
<ref>{{cite news|title= Intex ropes in actor Suriya as brand ambassador for Tamil Nadu|url= http://articles.economictimes.indiatimes.com/2015-08-11/news/65452299_1_brand-ambassador-actor-suriya-intex-technologies}}</ref>

==Sponsorships==
Intex was principle broadcast sponsor for 2015 [[Indian Premier League]] (IPL) on Sony Network.
<ref>{{cite news|title= Intex becomes official broadcast sponsor of IPL 2015|url= http://www.dqchannels.com/intex-becomes-official-broadcast-sponsor-of-ipl-2015}}</ref>

==Products==
'''iRist Junior
'''
iRist Junior is the first IoT (Internet of Things) based wearable device from Indian brand.
<ref>{{cite news|title= Intex iRist Junior, iRist Pro Smartwatches Launched in India|url= http://gadgets.ndtv.com/wearables/news/intex-irist-junior-irist-pro-smartwatches-launched-in-india-836827}}</ref>

'''iRist
'''
July 2015,
With the launch of iRist, Intex became the first Indian smartphone player to launch an Android-based smartwatch in India.
Intex iRist, weighs 83 grams, comes with a 1.56-inch sapphire glass screen with a 240 X 240 resolution. It has 1.2Ghz dual-core processor and 512 MB RAM, the smartwatch supports 3G SIM and offers standalone calling feature. It runs on [[Android KitKat]] 4.4.2. It has in-built Playstore app and GPS. It sports a 5.0 MP camera. Intex iRist has 4GB internal storage that can be expanded up to 32 GB. It has standby time of up to 200 hours and a talk time of up to 4 hours.
<ref>{{cite news|title= iRist: Intex launches its first Android smartwatch in India at Rs 11,999|url= http://www.ibnlive.com/news/tech/irist-intex-launches-its-first-android-smartwatch-in-india-at-rs-11999-1020722.html}}</ref>
<ref>{{cite news|title= Intex iRist First Impressions|url= http://gadgets.ndtv.com/wearables/reviews/intex-irist-first-impressions-716036}}</ref>

'''Intex Aqua 4G+
'''
It is the first 4G smartphone by Intex. 
It runs on [[Android Lollipop]] 5.0, 5-inch HD display, 1.3&nbsp;GHz quad-core [[MediaTek]] MT6735 processor with 2GB of RAM, 16 GB of inbuilt storage.<ref>{{cite news|title= Intex Aqua 4G+: Take a first look at the first 4G smartphone from Intex |url= http://indianexpress.com/photos/technology-gallery/intex-aqua-4g-take-a-first-look-at-the-first-4g-smartphone-from-intex/6/}}</ref>
<ref>{{cite news|title= Intex Aqua 4G+ With Android 5.0 Lollipop Launched at Rs. 9,499 |url= http://gadgets.ndtv.com/mobiles/news/intex-aqua-4g-with-android-50-lollipop-launched-at-rs-9499-700178}}</ref>

'''Intex Aqua Q7N Pro'''

On 25 August 2016, Intex launched this smartphone.<ref>{{Cite web|url=https://www.mobigyaan.com/intex-aqua-q7n-pro-3g-emergency-rescue-feature|title=Intex Aqua Q7N Pro with emergency rescue feature launched for Rs. 4299|last=|first=|date=2016-08-25|website=MobiGyaan.com|publisher=|access-date=2016-08-26}}</ref> This smartphone comes with SOS feature which can send messages to up to three emergency contacts upon pressing the volume buttons for three seconds. Other specs include 1.3&nbsp;GHz quad-core processor, 1 GB RAM, Android 5.1 Lollipop, 4.5-inch FWVGA display, 5 MP rear camera, 0.3 MP front camera, 8 GB internal storage and 2000 mAh battery.

'''Intex Aqua S7'''

On 5 September 2016, Intex launched this smartphone which is powered by MediaTek's MT6735 quad-core processor and coupled with 3 GB RAM.<ref>{{Cite web|url=https://www.mobigyaan.com/intex-aqua-s7-5-inch-hd-display-fingerprint-scanner-launched-rs-9499|title=Intex Aqua S7 with 5 inch HD display and fingerprint scanner launched for Rs. 9499|last=|first=|date=2016-09-05|website=MobiGyaan.com|publisher=|access-date=2016-09-06}}</ref> It comes with a fingerprint scanner at the back and runs on Android 6.0. Marshmallow. Other specs include 5-inch HD display, 16 GB internal storage, 13 MP primary camera, 5 MP front camera and 3200 mAh battery.

'''Intex Aqua Supreme+'''

On 7 February 2017, Intex launched this smartphone which is powered by 1.3GHz quad-core MediaTek 6737 processor and coupled with 2 GB RAM.<ref>{{Citeweb|url=http://www.thegadgetbytes.com/intex-launches-three-smartphones-4g-volte-support-prices-starting-rs-4199/|title=Intex Launches Three Smartphones with 4G VoLTE support, prices starting from Rs 4,199}}</ref> Camera features include a 13-megapixel camera with LED flash and a 5-megapixel front camera. Other specs include 5-inch HD display, 16 GB internal storage, and 3000 mAh battery.

=== Fitness Band ===
On 11 February 2016, Intex launched its first fitness band, the FitRist, in India.<ref>{{Cite web|url=https://www.mobigyaan.com/intex-fitrist-activity-tracking-smartband-launched-in-india-for-rs-999|title=Intex FitRist activity tracking smartband launched in India for Rs. 999|last=|first=|date=2016-02-11|website=MobiGyaan.com|publisher=|access-date=2016-08-10}}</ref> The band sports a 0.86 OLED display and works with Android and iOS devices. The band is water-resistant and provides a battery life of up to 12 days. Features like step counter and sleep monitor are present as well.

On 10 August 2016, Intex launched fitRist Pulzz in India.<ref>{{Cite web|url=https://www.mobigyaan.com/intex-fitrist-pulzz-launched-in-india|title=Intex launches fitRist Pulzz fitness band in India for ₹1799|last=|first=|date=2016-08-10|website=MobiGyaan.com|publisher=|access-date=2016-08-10}}</ref> The fitRist Pulzz is successor to the FitRist and features a 0.66-inch OLED display having a resolution of 64 x 48 pixels. The band is water-resistant and also lets users control music and camera on their smartphone. Besides boasting features like step counter, hear rate monitor and sleep monitor, the band also shows notifications related to calls, SMS, Facebook Messenger and WhatsApp.

== References ==

{{Reflist|35em}}

[[Category:Companies of India]]
[[Category:Companies based in New Delhi]]
[[Category:Consumer electronics brands]]
[[Category:Electronics companies of India]]
[[Category:Indian brands]]
[[Category:MeeGo]]
[[Category:Mobile phone companies of India]]
[[Category:Mobile phone manufacturers]]
[[Category:Manufacturing companies established in 1996]]
[[Category:Indian Premier League franchise owners]]