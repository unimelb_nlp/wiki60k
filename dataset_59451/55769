{{Infobox journal
| title = Atmosphere
| discipline = [[Atmospheric sciences]]
| abbreviation = Atmosphere
| editor = Robert Talbot
| publisher = [[MDPI]]
| country =
| frequency = Monthly
| history = 2010-present
| impact = 1.221 
| impact-year = 2015
| openaccess = Yes
| website = http://www.mdpi.com/journal/atmosphere/
| eISSN = 2073-4433
| OCLC = 773027071
| CODEN = ATMOCZ
}}
'''''Atmosphere''''' is a monthly [[peer-reviewed]] [[open access]] [[scientific journal]] covering research related to the Earth`s [[atmosphere]]. The journal is published by [[MDPI]] and was established in 2010. The founding [[editor-in-chief]] was Daniela Jacob ([[Max-Planck-Institute for Meteorology]]) until 2014. The current editor-in-chief is Robert Talbot ([[University of Houston]]).

==Abstracting and indexing==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Astrophysics Data System]]
* [[AGORA]]
* [[CAB Abstracts]]
* [[Chemical Abstracts Service]]
* [[COMPENDEX|EI/COMPENDEX]]
* [[Current Contents]]/Physical, Chemical & Earth Sciences
* [[GEOBASE]]
* [[Inspec]]
* [[Science Citation Index Expanded]]
* [[Scopus]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 1.221.<ref name="WoS">{{cite book |year = 2015|chapter = Atmosphere|title = 2015 [[Journal Citation Reports]]|publisher = [[Thomson Reuters]]|edition = Science|series = Web of Science|postscript = .}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.mdpi.com/journal/atmosphere}}

[[Category:English-language journals]]
[[Category:Monthly journals]]
[[Category:Open access journals]]
[[Category:Publications established in 2010]]
[[Category:Multidisciplinary Digital Publishing Institute academic journals]]