{{ Infobox book
|
| image     = 
| caption = 
| name          = Tit for Tat
| author        = [[Anonymous work|Anonymous]] (credited as "A Lady of New Orleans")
| illustrator   = 
| cover_artist  = 
| country       = United States
| language      = English
| genre         = [[Anti-Tom literature|Plantation literature]]
| publisher     = Garret & Co.
| release_date  = [[1856 in literature|1856]]
| media_type    = Print ([[Hardcover]] & [[Paperback]]) & [[E-book]]
| pages         = c.300 pp (May change depending on the publisher and the size of the text)
}}

'''''Tit for Tat''''' is an [[1856 in literature|1856]] novel written anonymously by "A Lady of [[New Orleans]]".

== Overview ==

''Tit for Tat'' is one of several examples of [[Anti-Tom literature|plantation literature]] that emerged in the [[Southern United States]] in response to the [[1852 in literature|1852]] novel ''[[Uncle Tom's Cabin]]'' by [[Harriet Beecher Stowe]], which had been criticised in the south for inaccurately depicting the practices of [[slavery]] and the relationship(s) between master and slave.<ref>http://www.enotes.com/uncle-toms/</ref>

''Tit for Tat'', however, is an isolated example of the genre where, instead of defending slavery or attacking [[Abolitionism in the United States|Abolitionism]] like other works of the genre, attempts to attack the enthusiasm for ''Uncle Tom's Cabin'' upon its initial release in the United Kingdom.<ref>[http://www.pbs.org/wgbh/aia/part4/4p2958.html Slave Narratives and Uncle Tom's Cabin] Africans in America, PBS, accessed June 22, 2009</ref> Thus, ''Tit for Tat'' could be seen as attempting to promote [[anti-British sentiment]] in the United States rather than promoting slavery.

== Plot ==

The novel follows Totty, a young urchin living in poverty in Victorian-era [[London]]. Totty is stolen from his family whilst young, and forced to work as the apprentice of a sadistic [[chimney sweep]]. Totty's suffering is ignored by the [[Philanthropy|philanthropists]], who are so concerned with the welfare of black slaves in America that they fail to notice that they have simply replaced their own slavery with [[child labour]].

== Publication history ==

''Tit for Tat'' was first published in its entirety by Garret & Co. in 1856, but unlike other anti-Tom novels such as ''[[Aunt Phillis's Cabin]]'' or ''[[The Planter's Northern Bride]]'', the novel has not been republished in the modern day.

== References ==

{{reflist|2}}

== External links ==
*[http://www.iath.virginia.edu/utc/proslav/titfortathp.html ''Tit for Tat'' at the University of Virginia]

{{Uncle Tom's Cabin}}

{{DEFAULTSORT:Tit For Tat (1856 Novel)}}
[[Category:1856 novels]]
[[Category:Anti-Tom novels]]
[[Category:Works published anonymously]]
[[Category:19th-century American novels]]