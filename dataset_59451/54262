{{Use Australian English|date=September 2016}}
{{Use dmy dates|date=December 2014}}
{{Infobox Politician
| name = Pru Goward
|honorific-prefix  = [[The Honourable]]
| honorific-suffix = [[Member of the Legislative Assembly|MP]] 
| image            = Pru Goward (7010666561).jpg 
| caption          = Goward visits ''[[La traviata]]'' in Sydney (2012)
| constituency_MP  = [[Electoral district of Goulburn|Goulburn]]
| parliament       = New South Wales
| term_start       = [[New South Wales state election, 2007|24 March 2007]]
| term_end         = 
| majority         = 6.70 points (2015)
| predecessor      = ''new district''
| successor        =  
| office1          = [[Minister for Family and Community Services (New South Wales)|Minister for Family and Community Services]]
| term_start1      = 30 January 2017
| term_end1        = 
| premier1         = [[Gladys Berejiklian]]
| predecessor1     = [[Brad Hazzard]] 
| successor1       = 
| office2          = [[Minister for Family and Community Services (New South Wales)#Social Housing|Minister for Social Housing]]
| term_start2      = 30 January 2017
| term_end2        = 
| predecessor2     = [[Brad Hazzard]] 
| successor2       = 
| premier2         = Gladys Berejiklian
| office3          = [[Minister for Health (New South Wales)#Women|Minister for Prevention of Domestic Violence and Sexual Assault]]
| term_start3      = 2 April 2015
| term_end3        = 
| predecessor3     = ''new portfolio'' 
| successor3       = 
| premier3         = [[Mike Baird]]<br/>Gladys Berejiklian
| office4          = [[Minister for Health (New South Wales)#Mental Health|Minister for Mental Health]]
| term_start4      = 2 April 2015
| term_end4        = 30 January 2017
| premier4         = Mike Baird
| predecessor4     = [[Jai Rowell]] 
| successor4       = [[Tanya Davies]]
| office5          = [[Minister for Health (New South Wales)#Medical Research and Science|Minister for Medical Research]]
| term_start5      = 2 April 2015
| term_end5        = 30 January 2017
| premier5         = [[Mike Baird]]
| predecessor5     = [[Jillian Skinner]] 
| successor5       = Brad Hazzard
| office6          = [[Minister for Health (New South Wales)#Women|Minister for Women]]
| term_start6      = 3 April 2011
| term_end6        = 30 January 2017
| premier6         = [[Barry O'Farrell]]<br>Mike Baird
| predecessor6     = [[Linda Burney]] 
| successor6       = Tanya Davies
| office7          = [[Minister for Health (New South Wales)#Assistant Ministers|Assistant Minister for Health]]
| term_start7      = 2 April 2015
| term_end7        = 30 January 2017
| premier7         = Mike Baird
| predecessor7     = Jai Rowell 
| successor7       = ''portfolio abolished''
| office8          = [[Minister for Planning (New South Wales)|Minister for Planning]]
| term_start8      = 23 April 2014
| term_end8        = 2 April 2015
| premier8         = Mike Baird
| predecessor8     = [[Brad Hazzard]] 
| successor8       = [[Rob Stokes]]
| office9          = [[Minister for Family and Community Services (New South Wales)|Minister for Family and Community Services]]
| term_start9      = 3 April 2011
| term_end9        = 23 April 2014
| premier9         = Barry O'Farrell
| predecessor9     = [[Linda Burney]] 
| successor9       = [[Gabrielle Upton]]
| birth_name       = Prudence Jane Goward
| birth_date       = {{Birth date and age|df=yes|1952|11|2}}
| birth_place      = [[Adelaide]], South Australia
| death_date       = 
| death_place      = 
| party            = [[Liberal Party of Australia]]
| relations        = 
| spouse           = David Barnett (m. 1986)<br>Alastair Fischer (m. 1973; dis. 1983)
| children         = Three including [[Kate Fischer]]
| residence        = 
| occupation       = 
| religion         = 
| signature        = 
| alma_mater       = [[Adelaide University]] (1974)
| website          = [http://www.parliament.nsw.gov.au/Prod/Parlment/members.nsf/0/D05E2CA1D2AA4DE4CA2572A700161FF4?Open&refNavID=ME3_1 Parliamentary biography]
| footnotes        = 
}}
'''Prudence Jane Goward''' [[Member of the Legislative Assembly#Australia|MP]] (born 2 September 1952 in [[Adelaide]]<ref name="abc">{{cite news | url = http://www.abc.net.au/talkingheads/txt/s1784964.htm
 | title = Pru Goward | work = [[Talking Heads (Australian TV series)|Talking Heads]] | publisher = [[Australian Broadcasting Corporation]] | last = Thompson | first = Peter | date = 13 November 2006 | accessdate = 27 April 2011 }}</ref>), an Australian politician, is the [[New South Wales Government|New South Wales]] [[Minister for Family and Community Services (New South Wales)|Minister for Family and Community Services]] and [[Minister for Family and Community Services (New South Wales)#Social Housing|Minister for Social Housing]], since January 2017 in the [[Gladys Berejiklian|Berejiklian]] government,<ref name=gladys>{{Cite web|url=https://www.nsw.gov.au/your-government/the-cabinet/|title=The Cabinet|last=Cabinet|first=Department of Premier and|website=nsw.gov.au - your tagline here|language=en-AU|access-date=2017-02-01}}</ref> and the [[Minister for Health (New South Wales)#Women|Minister for Prevention of Domestic Violence and Sexual Assault]], since 2015. She has previously served as the [[Minister for Health (New South Wales)#Mental Health|Minister for Mental Health]], [[Minister for Health (New South Wales)#Medical Research and Science|Minister for Medical Research]], and [[Minister for Health (New South Wales)#Assistant Ministers|Assistant Minister for Health]] between April 2015 and January 2017, and the [[Minister for Health (New South Wales)#Women|Minister for Women]] between 2011 and January 2017, in the [[Second Baird ministry|second]] [[Mike Baird|Baird]] government<ref name="smh Baird 2">{{cite news|last=Hasham|first=Nicole|title=Premier Mike Baird's new NSW cabinet sworn in: Gladys Berejiklian and Gabrielle Upton first female Treasurer and Attorney-General|url=http://www.smh.com.au/nsw/premier-mike-bairds-new-nsw-cabinet-sworn-in-gladys-berejiklian-and-gabrielle-upton-first-female-treasurer-and-attorneygeneral-20150402-1mdjb7.html|accessdate=6 April 2015|newspaper=[[The Sydney Morning Herald]]|date=3 April 2015}}</ref> and the [[Minister for Planning (New South Wales)|Minister for Planning]] during 2014 and 2015. With the first [[Berejiklian ministry|Berejiklian government]] she returned to Community Services portfolio which she previously held between 2011 and 2014, in the [[O'Farrell ministry|O'Farrell]] and [[Baird ministry (2014–2015)|first Baird]] governments.<ref name="smh.com.au">{{cite news |url=http://www.smh.com.au/nsw/mike-bairds-cabinet-reshuffle-a-preparation-for-next-election-20140422-371g9.html |title=Mike Baird's cabinet reshuffle a preparation for next election |work=[[The Sydney Morning Herald]] |date=22 April 2014 |accessdate=24 April 2014 |author=Nicholls, Sean }}</ref><ref name=nswpl>{{cite web|url=http://www.parliament.nsw.gov.au/Prod/Parlment/members.nsf/0/D05E2CA1D2AA4DE4CA2572A700161FF4?Open&refNavID=ME3_1 |title=The Hon. Prudence Jane GOWARD, MP |work=Current members of parliament |publisher=[[Parliament of New South Wales]] |date=23 April 2014 |accessdate=24 April 2014 |deadurl=yes |archiveurl=https://web.archive.org/web/20140621170541/http://www.parliament.nsw.gov.au/Prod/Parlment/members.nsf/0/D05E2CA1D2AA4DE4CA2572A700161FF4?Open&refNavID=ME3_1 |archivedate=21 June 2014 |df=dmy }}</ref> Goward is a member of the [[New South Wales Legislative Assembly]] representing [[Electoral district of Goulburn|Goulburn]] for the [[Liberal Party of Australia]] since 2007.

Prior to entering politics, Goward served as the Australian Federal [[Sex Discrimination Commissioner]] and Commissioner Responsible for Age Discrimination with the Australian [[Human Rights and Equal Opportunity Commission]].

==Early life and personal background==
Goward was born to Gerald Goward and Zipporah Riggs, and was raised in Adelaide. She attended Morphett Vale Primary School, [[Willunga High School]] and gained entrance to [[St Peter's Woodlands Grammar|Woodlands Church of England Girls Grammar School]] on a half scholarship.<ref name="abc"/> She graduated with a Bachelor of Arts (Econ) (Hons) in 1974 from [[Adelaide University]].

She was married from 1973 to 1983 to university lecturer Alastair Fischer, whom she met while studying at Adelaide University.<ref name="abc"/>

Goward married journalist David Barnett in 1986. Goward and Barnett have maintained a close personal friendship with former prime minister [[John Howard]] for many years, and jointly authored a biography of Howard in 1997.<ref>{{Citation | author1=Barnett, David | author2=Goward, Pru | author3=Barnett, David, 1931 (27 Sept.)- | author4=Goward, Pru | title=John Howard : Prime Minister | publication-date=1997 | publisher=Viking | isbn=978-0-670-87389-0 }}</ref>

She is the mother of three daughters, former model and actor [[Kate Fischer]], Penny Fischer, and Alice Barnett. She has two granddaughters. {{citation needed|date=May 2014}}<ref>m.dailytelegraph.com.au/the-once-svelte-kate-fischer-stacks-on-the-pounds/story-e6freuy9-1226060334230</ref>

==Career==
Goward joined [[Australian Broadcasting Commission|ABC TV and Radio]] in 1980, firstly as a reporter with ''[[Nationwide (ABC TV series)|Nationwide]]'', then as a political correspondent on the ''[[7.30 Report]]'', and later as host of the ''Morning Show'' and ''Daybreak'' on [[Radio National]]. She has also worked as a high school teacher, a university lecturer in economics, a broadcast journalism lecturer at [[University of Canberra]], a media consultant and freelance writer. {{Citation needed|date=May 2014}}<ref>http://www.prugoward.com.au/About/AboutPru.aspx</ref>

She was Executive Director of the [[Office of the Status of Women]] in the [[Department of the Prime Minister and Cabinet (Australia)|Department of the Prime Minister and Cabinet]] from 1997 to 1999. In this position, Goward criticised the business community for the "primitive attitudes" that kept women out of senior executive ranks and boardrooms.<ref>{{cite news | last = Bagwell | first = S. | title = Get Rid of Primitive Attitude: Goward | work = Australian Financial Review | date = 4 September 1997 | accessdate =    }}</ref> At the time of her appointment, she was criticised by Anne Summers, a previous Executive Director, and [[Carmen Lawrence]], a prominent female politician, for her perceived inexperience and political connection to the Howard government.<ref>{{cite news | last = Humphries | first = D | url = http://www.smh.com.au/news/national/woman-most-likely/2006/09/22/1158431904498.html | title = Woman most likely | work = The Sydney Morning Herald | date = 23 September 2006 | accessdate =     }}</ref>

Goward was the Sex Discrimination Commissioner at [[Human Rights and Equal Opportunity Commission|HREOC]], a five-year tenure she began in July 2001. In this role, she called for the introduction of paid maternity leave, a position rejected by the Howard government. {{Citation needed|date=April 2014}} Howard extended her tenure for an additional three years in July 2006. {{Citation needed|date=September 2014}} However, she successfully ran for New South Wales state parliament in March 2007.

Pru Goward, while Minister for the [[Department of Family and Community Services (New South Wales)|Department of Family and Community Services]], initiated the Going Home Staying Home reforms which redistributed funding for youth refuges across the state.<ref name=factsheet>[http://gymeacommunityaid.org.au/wp-content/uploads/2013/12/FACS-Homelessness-Services-Fact-Sheet1.pdf "Specialist Homelessness Services Going Home Staying Home South Eastern Sydney District."] ''Fact Sheet October 2014''. New South Wales Department of Family and Community Services. October 2014.</ref><ref>[http://www.noffs.org.au/static/media/uploads/noffs_annual_report_2013-14.pdf "The Year in Highlight."] ''Annual Report 2014''. Ted Noffs Foundations. Accessed 27 May 2015.</ref>{{rp|14}}

==Parliamentary career==
[[File:Pru_Goward_-_2008.jpg|thumb|left|Goward at the 2008 NSW Country Liberals conference in [[Wagga Wagga]]]]
In 2006, Goward nominated for Liberal Party preselection for the [[New South Wales]] state parliament in seat of [[Electoral district of Epping|Epping]] in Sydney's north-west, but was defeated<ref>{{cite news | last = Mitchell | first = Alex | url = http://www.smh.com.au/news/national/beaten-but-pru-still-to-get-seat/2006/09/16/1158334735670.html | title = Goward sinks in Epping but resurfaces closer to home | work = The Sydney Morning Herald |date = 16 September 2006 | accessdate = }}</ref> by the former President of the [[Right to Life]] Association, [[Greg Smith (New South Wales politician)|Greg Smith]].<ref>{{cite news | last = Shanahan | first = D. | url = http://www.theaustralian.news.com.au/story/0,20867,19856240-17301,00.html | title = The trouble with talent in politics | work = [[The Australian]] | date = 21 July 2006 | accessdate = }}</ref> She was subsequently preselected unopposed for the seat of Goulburn, to replace retiring Liberal frontbencher [[Peta Seaton]]. Goward was expected to win the seat, however an unexpectedly strong swing to Labor in the Southern Highlands area of the seat put her victory in doubt on election night. Her main contender, the Independent Mayor of Goulburn, Paul Stephenson, conceded defeat on 29 March 2007. Goward was quoted as saying that she "didn't expect to win it. I knew I was behind the whole time, even four days before the election we were told I was five points behind, so I'm just so grateful."<ref>{{cite news | agency = [[Australian Associated Press|AAP]] | url = http://www.smh.com.au/news/national/goward-wins-goulburn-seat/2007/03/29/1174761661930.html | title = Goward wins Goulburn seat | work = The Sydney Morning Herald | date = 29 March 2006 | accessdate =     }}</ref>>

As the minister responsible for child protective services there have been reports of inadequate staffing and services to meet the need.<ref>{{cite news|url=http://www.dailytelegraph.com.au/news/nsw/pru-goward-under-pressure-after-allegations-of-misleading-parliament/story-fni0cx12-1226702455939|first=Andrew|last=Clennell|date=23 August 2013|title=Pru Goward under pressure after allegations of misleading parliament|work=The Daily Telegraph|location=Australia|accessdate=}}</ref><ref>{{cite news|url=http://www.smh.com.au/nsw/calls-for-pru-goward-to-be-sacked-over-abandoned-children-20140410-36g31.html|author1=Browne, Rachel|author2=Patty, Anna|title=Calls for Pru Goward to be sacked over abandoned children|date=11 April 2014|work=The Sydney Morning Herald|accessdate= }}</ref>

As the state's Community Services Minister, Goward announced in mid-March 2014 that around 300 harbourfront public housing properties will be sold under the management of Government Property NSW, with the proceeds reinvested into the public housing system. Considered historic structures, the harbourfront properties are located at Millers Point, The Rocks and on Gloucester Street, and include the Sirius complex, a high-rise, 79-unit apartment complex near the Harbour Bridge. The government expects to generate hundreds of millions of dollars from the sales and Goward explained, as a justification of the sale: "In the last two years alone, nearly [A]$7 million has been spent maintaining this small number of properties. That money could have been better spent on building more social housing, or investing in the maintenance of public housing properties across the state."<ref name="Hash">{{cite news|title=Sydney waterfront public housing properties to be sold off|url=http://www.smh.com.au/nsw/sydney-waterfront-public-housing-properties-to-be-sold-off-20140319-351fs.html|accessdate=21 March 2014|newspaper=The Sydney Morning Herald|date=19 March 2014|author=Hasham, Nicole}}</ref>

Due to the resignation of [[Barry O'Farrell]] as premier,<ref>{{cite news |url=http://www.theaustralian.com.au/national-affairs/barry-ofarrell-quits-as-nsw-premier-over-memory-fail/story-fn59niix-1226886224077 |title=Barry O'Farrell quits as NSW Premier over memory fail |date=16 April 2014 |work=[[The Australian]] |accessdate=23 April 2014 }}</ref> and the subsequent ministerial reshuffle by [[Mike Baird]], the new Liberal Leader,<ref name="smh.com.au"/> in April 2014 in addition to her existing responsibilities as a minister, Goward was appointed as the Minister for Planning; and lost the portfolio of Family and Community Services.<ref name="nswpl"/><ref>{{cite news |url=http://www.smh.com.au/nsw/mike-bairds-nsw-cabinet-20140422-371j4.html |title=Mike Baird's NSW cabinet |date=22 April 2014 |work=[[The Sydney Morning Herald]] |accessdate=23 April 2014 }}</ref> Following the [[New South Wales state election, 2015|2015 state election]], Goward was sworn in as the Minister for Mental Health, the Minister for Medical Research, the Minister for Women, the Minister for Prevention of Domestic Violence and Sexual Assault (a newly created portfolio), and the Assistant Minister for Health in the second Baird government.<ref name="smh Baird 2"/> Following the resignation of Baird as premier and the election of [[Gladys Berejiklian]] as [[Leader of the New South Wales Liberal Party|Liberal leader]], in January 2017 Goward was sworn in as the Minister for Family and Community Services, the Minister for Social Housing, and the Minister for Prevention of Domestic Violence and Sexual Assault.<ref name=gladys/>

===Controversy===
After being in Parliament a single day, Goward told a symposium of women in leadership in Sydney that "I have never worked in any profession as male-dominated or as ruthlessly sexist as this. I was quite shocked by it."<ref>{{cite news | url = http://www.smh.com.au/news/national/bearpit-sexist-says-new-mp/2007/05/10/1178390396196.html | last = | first =  | title = Bearpit sexist says new MP | work = The Sydney Morning Herald | date = 10 May 2007 | accessdate = }}</ref> These comments have not been supported by other female MPs from both sides of politics, who generally said Parliament had improved.<ref>{{cite news|url=http://www.news.com.au/mercury/story/0,22884,21710645-421,00.html |last= |first= |title=Story |work=Illawarra Mercury |date= |accessdate= }}{{dead link|date=June 2016|bot=medic}}</ref>

In May 2007, Goward was caught speeding in a school zone. This was her second driving offence for 2007. Goward said "It was extremely careless on my part and like thousands of other drivers I deeply regret it."<ref>{{cite news|author=|url=http://www.smh.com.au/news/national/goward-speeding-in-school-zone/2007/05/18/1178995380930.html|work=The Sydney Morning Herald|title=It was extremely careless: Goward|date= 18 May 2007|accessdate = }}</ref><ref>{{cite news|url=http://news.ninemsn.com.au/article.aspx?id=99033|title=Woman wants Goward's apology over advice|work=|publisher=NineMSN Pty Limited|date=|accessdate= }}</ref>

In February 2014, [[Katrina Hodgkinson]], the [[National Party of Australia|Nationals]] member for [[Electoral district of Burrinjuck|Burrinjuck]], a neighbouring electorate of Goulburn, announced that she would be contesting Goward's seat of Goulburn. Her decision followed a statewide [[Redistribution (election)|electoral redistribution]] by the [[New South Wales Electoral Commission|NSW Electoral Commission]] that resulted in a substantial revision of Hodgkinson's seat of Burrinjuck and the seat renamed as [[Electoral district of Cootamundra|Cootamundra]], with effect from the [[New South Wales state election, 2015|2015 state election]].<ref>{{cite news |url=http://www.smh.com.au/nsw/katrina-hodgkinson-to-take-on-pru-goward-for-goulburn-seat-20140219-32z50.html |title=Katrina Hodgkinson to take on Pru Goward for Goulburn seat |author=Nicholls, Sean |work=[[The Sydney Morning Herald]] |date=19 February 2014 |accessdate=21 February 2014 }}</ref><ref>{{cite news |url=http://www.abc.net.au/news/2014-02-19/hodgkinson-decides-to-run-in-goulburn/5268732 |title=Katrina Hodgkinson to challenge Pru Goward in preselection fight to win NSW seat of Goulburn |work=[[ABC News (Australia)|ABC News]] |location=Australia |date=19 February 2014 |accessdate=21 February 2014 }}</ref> Her announcement resulted in a dispute between the Nationals and Liberals;<ref>{{cite news |url=http://www.smh.com.au/nsw/pru-goward-katrina-hodgkinson-dispute-over-goulburn-unresolved-20140228-33r7h.html |title=Pru Goward, Katrina Hodgkinson dispute over Goulburn unresolved |author=Nicholls, Sean |work=[[The Sydney Morning Herald]] |date=28 February 2014 |accessdate=25 April 2014 }}</ref><ref>{{cite news |url=http://www.theaustralian.com.au/national-affairs/state-politics/national-katrina-hodgkinson-to-contest-liberal-pru-gowards-seat-of-goulburn/story-e6frgczx-1226831270423 |title=National Katrina Hodgkinson to contest Liberal Pru Goward's seat of Goulburn |work=[[The Australian]] |agency=[[Australian Associated Press|AAP]] |date=19 February 2014 |accessdate=25 April 2014 }}</ref> and on 28 February, Hodgkinson announced she would withdraw her nomination for Goulburn. Hodgkinson subsequently contested the newly constituted seat of Cootamundra,<ref>{{cite news |url=http://www.smh.com.au/nsw/katrina-hodgkinson-to-withdraw-from-goulburn-poll-fight-against-pru-goward-20140228-33ohq.html |title=Katrina Hodgkinson to withdraw from Goulburn poll fight against Pru Goward |author=Nicholls, Sean |work=[[The Sydney Morning Herald]] |date=28 February 2014 |accessdate=25 April 2014 }}</ref> and won the seat for the National Party.

==Awards==
Goward was awarded a [[Centenary Medal]] in 2001 for services to journalism and women's rights.<ref name="hreoc_bio">{{cite web|url=http://www.hreoc.gov.au/about_the_commission/president_commissioners/goward.html |title=Profile of Pru Goward |work=Former Commissioners |publisher=[[Human Rights and Equal Opportunity Commission]] |date=15 September 2006 |accessdate= |deadurl=yes |archiveurl=https://web.archive.org/web/20051107014258/http://www.hreoc.gov.au:80/about_the_commission/president_commissioners/goward.html |archivedate=7 November 2005 |df=dmy }}</ref>

==References==
{{reflist|2}}

==External links==
*[https://web.archive.org/web/20051107014258/http://www.hreoc.gov.au:80/about_the_commission/president_commissioners/goward.html Profile of Pru Goward at the Human Rights and Equal Opportunity Commission]
*[http://www.prugoward.com.au/ Official Website of Pru Goward]

==Publications==
*{{cite book |first1=David |last1=Barnett |first2=Pru |last2=Goward |title=John Howard, Prime Minister |publisher=Viking |year=1997 |isbn= 0-670-87389-6 }}
*{{cite book |first=Pru |last=Goward |title=A Business of Your Own: How Women Succeed in Business |publisher=Allen & Unwin |location=Australia |year=2001 |isbn= 1-86508-593-6 }}

{{s-start}}
{{s-par|au-nsw}}
{{s-new|district}}
{{s-ttl|title=Member for [[Electoral district of Goulburn|Goulburn]]|years=2007{{spaced endash}}present}}
{{s-inc}}
{{s-off}}
{{s-bef|rows=2|before=[[Brad Hazzard]] }}
{{s-ttl|title=[[Minister for Family and Community Services (New South Wales)|Minister for Family and Community Services]]|years=2017{{spaced endash}}present }}
{{s-inc|rows=3}}
{{s-ttl|title=[[Minister for Family and Community Services (New South Wales)#Housing|Minister for Social Housing]]|years=2017{{spaced endash}}present }}
{{s-new}}
{{s-ttl|title=[[Minister for Health (New South Wales)#Women|Minister for Prevention of Domestic Violence and Sexual Assault]]|years=2015{{spaced endash}}present}}
{{s-bef|before=[[Linda Burney]]}}
{{s-ttl|title=[[Minister for Health (New South Wales)#Women|Minister for Women]]|years=2011{{spaced endash}}2017}}
{{s-aft|rows=2|after=[[Tanya Davies]]}}
{{s-bef|rows=2|before=[[Jai Rowell]] }}
{{s-ttl|title=[[Minister for Health (New South Wales)#Mental Health|Minister for Mental Health]]|years=2015{{spaced endash}}2017}}
|-
{{s-ttl|title=[[Minister for Health (New South Wales)#Assistant Ministers|Assistant Minister for Health]]|years=2015{{spaced endash}}2017}}
{{s-aft|after=''portfolio abolished'' }}
{{s-bef|before=[[Jillian Skinner]] }}
{{s-ttl|title=[[Minister for Health (New South Wales)#Medical Research and Science|Minister for Medical Research]]|years=2015{{spaced endash}}2017}}
{{s-aft|after=[[Brad Hazzard]] }}
{{s-bef|before=[[Brad Hazzard]]}}
{{s-ttl|title=[[Minister for Planning (New South Wales)|Minister for Planning]]|years=2014{{spaced endash}}2015}}
{{s-aft|after=[[Rob Stokes]]}}
{{s-bef|before=[[Linda Burney]] }}
{{s-ttl|title=[[Minister for Family and Community Services (New South Wales)|Minister for Family and Community Services]]|years=2011{{spaced endash}}2014 }}
{{s-aft|after=[[Gabrielle Upton]] }}
{{s-end}}

{{Cabinet of New South Wales}}
{{NSWCurrentMLAs}}

{{Authority control}}

{{DEFAULTSORT:Goward, Pru}}
[[Category:Members of the New South Wales Legislative Assembly]]
[[Category:Liberal Party of Australia members of the Parliament of New South Wales]]
[[Category:Australian non-fiction writers]]
[[Category:University of Adelaide alumni]]
[[Category:Australian biographers]]
[[Category:Australian public servants]]
[[Category:1952 births]]
[[Category:Living people]]
[[Category:People from Adelaide]]
[[Category:Recipients of the Centenary Medal]]
[[Category:ABC journalists associated with the Liberal Party of Australia]]
[[Category:21st-century Australian politicians]]