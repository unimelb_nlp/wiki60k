{|{{Infobox Aircraft Begin
 | name=ES
 | image=Lancair-SuperES.jpg 
 | caption=Lancair Super ES
}}{{Infobox Aircraft Type
 | type=[[Homebuilt aircraft]]
 | national origin=[[United States]]
 | manufacturer=[[Lancair]]
 | designer=
 | first flight=
 | introduced=
 | retired=
 | status=Production completed in 2012
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=90 (2011)
 | program cost= <!--Total program cost-->
 | unit cost=[[US$]]100,000 (kit only, 2011)
 | developed from=[[Lancair IV]]
 | variants with their own articles=[[Cessna 400]]
}}
|}

The '''Lancair ES''' is an [[United States|American]] [[amateur-built aircraft]] that was designed and produced by [[Lancair]]. While it was in production the aircraft was supplied in the form of a kit for amateur construction.<ref name="KitplanesDec2011">Vandermeullen, Richard: ''2012 Kit Aircraft Buyer's Guide'', Kitplanes, Volume 28, Number 12, December 2011, page 59. Belvoir Publications. ISSN 0891-1851</ref><ref>{{cite journal|magazine=Popular Science|pp=93|date=March 1995}}</ref>
[[File:Lancair Super ES N132BB 01.JPG|thumb|right|Super ES at [[Sun 'n Fun]] 2006]]

Production of the aircraft kit was ended in 2012.<ref name="Production">{{cite web|url = http://lancair.com/index.php?option=com_content&view=article&id=145&Itemid=140|title = Our Aircraft|accessdate = 15 November 2012|last = [[Lancair]]|year = 2012}}</ref>

==Design and development==
The aircraft features a cantilever [[low wing]], a four-seat enclosed cabin that is {{convert|46|in|cm|0|abbr=on}} wide, fixed [[tricycle landing gear]] and a single engine in [[tractor configuration]].<ref name="KitplanesDec2011" />

The aircraft is made from composites. Its {{convert|35.5|ft|m|1|abbr=on}} span wing employs a McWilliams RXM5-217 [[airfoil]] at the [[wing root]], transitioning to a [[NACA airfoil|NACA 64-212]] at the [[wingtip]]. The wing has an area of {{convert|140|sqft|m2|abbr=on}} and mounts [[Flap (aircraft)|flaps]]. The aircraft's recommended engine power range is {{convert|210|to|310|hp|kW|0|abbr=on}} and standard engines used include the {{convert|310|hp|kW|0|abbr=on}} [[Continental IO-550]] (Super ES) [[four-stroke]] powerplant. Construction time from the supplied kit is estimated to be 2000 hours.<ref name="KitplanesDec2011" /><ref name="Incomplete">{{cite web|url = http://www.ae.illinois.edu/m-selig/ads/aircraft.html|title = The Incomplete Guide to Airfoil Usage |accessdate = 15 November 2012|last = Lednicer |first = David |authorlink = |year = 2010}}</ref>

In July 2016 the company announced it would be selling the older Lancair lines of aircraft to concentrate on the [[Lancair Evolution]] instead. Once the transition is complete the company will change its name to the Evolution Aircraft Company.<ref>{{cite web|url=http://www.avweb.com/avwebflash/news/Lancair-To-Sell-Legacy-Assets-In-Favor-Of-Evolution-Series-226710-1.html|title=Lancair To Sell Legacy Assets In Favor Of Evolution Series - AVweb flash Article|work=avweb.com|accessdate=30 July 2016}}</ref>

==Operational history==
By December 2011, 90 examples had been completed and flown.<ref name="KitplanesDec2011" /> At least two examples have utilized custom engine mounts to allow installation of [[Lycoming IO-540]] engines.<ref>{{cite web|url=http://www.lancair-builders.com/es/builders.html |title=List of Lancair ES Builders |publisher=Lancair-builders.com |date=2002-08-13 |accessdate=2013-09-14}}</ref>

==Specifications (Super ES)==
{{Aircraft specs
|ref=Kitplanes<ref name="KitplanesDec2011" />
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=three passengers
|length m=
|length ft=25
|length in=0
|length note=
|span m=
|span ft=35
|span in=6
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=140
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=
|empty weight lb=2200
|empty weight note=
|gross weight kg=
|gross weight lb=3550
|gross weight note=
|fuel capacity={{convert|105|u.s.gal}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Continental IO-550]]
|eng1 type=six cylinder, air-cooled, [[four stroke]] [[aircraft engine]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=310<!-- prop engines -->

|prop blade number=
|prop name=
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=215
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=70
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=1400
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=2000
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=25.4
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==References==
{{Commons category}}
{{Reflist}}
<!-- ==External links== -->
{{Lancair}}

[[Category:Single-engine aircraft]]
[[Category:United States sport aircraft]]
[[Category:Lancair aircraft|ES]]
[[Category:Homebuilt aircraft]]