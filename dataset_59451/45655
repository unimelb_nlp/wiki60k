{{Use dmy dates|date=September 2013}}
[[Image:Albert-tissandier.jpg|thumb|200px|Albert Tissandier.]]
[[Image:La Nature Cover page.gif|thumb|200px|right|Cover of the first issue of ''La Nature'', 1873. Illustration by Albert Tissandier.]]
'''Albert Tissandier''' (1839 &ndash; 5 September 1906) was a French [[architect]], [[aviator]], [[illustrator]], [[editing|editor]] and [[archaeologist]]. He was the brother of adventurer [[Gaston Tissandier]] with whom he collaborated in writing the magazine ''[[La Nature]]'', a French language [[scientific journal]] aimed at the [[popular science|popularization of science]]. He and his brother demonstrated the first electric powered flight.

==Early years and ''La Nature''==
Born in [[Paris]] in 1839, Albert Tissandier was a loyal and assiduous companion of his brother, [[Gaston Tissandier]]. As an architect, he was involved in a large number of projects.

Though his brother was nominally the sole founder of ''La Nature'', a [[scientific journal]] aimed at the popularization of science, Albert contributed extensively to the magazine, mostly as an [[illustrator]] and editor. He was heavily involved in it from the very first issue in 1873 until his retirement in 1905, less than a year before his death.

==Aviation career==
His devotion to [[aeronautics]] began on 8 November 1868, when he made his first [[hot air balloon]] ascent between [[Melun]] and Paris during a [[snowstorm]].

During the [[Siege of Paris (1870–1871)|siege of Paris]] in the [[Franco-Prussian war]], Albert Tissandier piloted one of the first mail balloons to outside the city. He escaped Paris on 14 October 1870, in the gondola of the balloon ''Jean-Bart'', taking two other travellers in his care. Additionally, he carried 400&nbsp;kg (1000&nbsp;lb) of mail and dispatches from 100 anxious families. He was awarded the [[Médaille militaire]] for his bravery.
[[Image:Lunar Halo Albert Tissandier.PNG|thumb|left|250px|Lunar Halo and Luminous Cross, drawing of nature. Albert Tissandier.]]

===The'' Zénith''===
A few years later, the two brothers ascended in another [[hot air balloon]], this one called the ''[[Zénith (balloon)|Zénith]]''. On 23 March and 24 March 1875 they flew from Paris to [[Arcachon]], on the other side of the country, near [[Bordeaux]], a total distance of 600&nbsp;km (400&nbsp;mi). During this ascent, he drew the countrysides below as naturally as possible. He also observed that when the moon passed above the clouds, the upper surface of the clouds shimmered like a lake, and recorded this in a drawing (left).

Only a few days after this ascent, on 15 April at 11:35 AM, the ''Zénith ''went up again, this time with only Gaston Tissandier, [[Joseph Croce-Spinelli]] and [[Théodore Sivel]] with the goal of reaching an extreme height in order to continue their observations. They were able to reach the unheard of altitude of 8,600 m (28,000&nbsp;ft). The latter became victims of their devotion to science dying from asphyxiation from the thin air. Gaston Tissandier himself became [[deaf]] and struggled with the problem for the rest of his life.

===First electric powered flight===
[[Image:Dirigible-electric.jpg|thumb|Drawing by E. A. Tilly of an airship powered by an electric motor developed by Albert and Gaston Tissandier, 1883]]
[[File:Md456.jpg|thumb|The Tissandier brothers' airship, France, 1883. The first aerostat with an electric engine.]]
In 1881, the brothers Tissandier demonstrated the world's first electric powered flight at an electricity exposition by attaching an [[electric motor]] to a [[dirigible]]. It was after their first experiences with flight that they made the large model they demonstrated, for which Albert drew the [[blueprints]]. The first flight of an electric dirigible [[aerostat]] took place on 8 October 1883. They made a second attempt on 26 September 1884 which gave them all the results they were looking for.

==Archaeology and travels==
Albert Tissandier was also a passionate writer and traveller. In 1886 he started a long trip around the world in [[Americas|America]], subsequently travelling to the [[East Indies]], [[Ceylon]] and many other [[Asia]]n locales, sending his magnificent illustrations back to Paris so they could appear in ''[[La Nature]]''.

In January 1890, he was sent by the Minister of Public Instruction and Fine Arts on an [[archaeology|archaeological]] mission to India, China and Japan. These travels occupied him for two years, and he gave a complete account of his travels, including remarkably exact and beautiful illustrations.

In 1893 and 1894, he visited [[Cambodia]] and [[Java]] including all the [[Khmer people|Khmer]] and Javanese ruins. Like his previous trip, he kept a record of precise and detailed illustrations and a large amount of remarkable [[Artifact (archaeology)|artifacts]].

==Retirement and death==
Following his travels, he at last returned to Paris and continued working extensively on ''[[La Nature]]''. Though his brother Gaston died on 30 August 1899, he continued to be a large contributor to and [[editing|editor]] of the magazine. He retired in 1905, dying on 5 September 1906 at [[Jurançon]]. Many believed that the magazine would not be able to continue after his death.

==References==
*Obituary by Jules Laffaruge from ''[[La Nature]]'' issue #1738 published 15 September 1906, now in the [[public domain]]

==External links==
* [http://www.gloubik.info/sciences/spip.php?rubrique4 Gaston et Albert Tissandier] {{fr icon}}
* {{gutenberg author| id=Albert_Tissandier | name=Albert Tissandier}}
* {{Internet Archive author |sname=Albert Tissandier}}
* [http://www.loc.gov/pictures/collection/tisc/ Tissandier Collection from the Library of Congress].  Drawings, prints, and photographs by and collected by Gaston and Albert Tissandier.
* Gaston and Albert Tissandier Collection: Publications relating to the history of aeronautics, (1,800 titles dispersed in the collection). From the [http://www.loc.gov/rr/rarebook/ Rare Book and Special Collections Division at the Library of Congress]


{{Authority control}}

{{DEFAULTSORT:Tissandier, Albert}}
[[Category:1839 births]]
[[Category:1906 deaths]]
[[Category:Architects from Paris]]
[[Category:Burials at Père Lachaise Cemetery]]
[[Category:19th-century French architects]]
[[Category:French archaeologists]]
[[Category:French balloonists]]
[[Category:French aerospace engineers]]
[[Category:French illustrators]]