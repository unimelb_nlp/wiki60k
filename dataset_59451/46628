'''Woodrow Wilson Borah''' (born 23 December 1912, Utica Mississippi, died 10 December 1999, Berkeley CA) was a U.S. historian of colonial Mexico, whose research contributions on demography, economics, and social structure made him a major Latin Americanist. With his 1999 death “disappears the last great figure in the generation that presided over the vast expansion of the Latin American scholarly field in the United States during the years following World War II.”<ref>http://content.cdlib.org/view?docId=hb1r29n709;NAAN=13030&doc.view=frames&chunk.id=div00004&toc.depth=1&toc.id=&brand=calisphere></ref>  With colleagues at [[University of California, Berkeley]] who came to be known as the “Berkeley School” of Latin American history, Borah pursued projects to gather data from archives on indigenous populations, colonial enterprises, and “land-life” relations that revolutionized the study of Latin American history.<ref>W. George Lovell, “Obituary: Woodrow Borah”, ''Colonial Latin American Review'' vol. 10 (1) 2001, p. 145.</ref>
==Life==
Borah was named for President [[Woodrow Wilson]]. As the first white child born in Utica, Mississippi following Wilson’s becoming president, Borah said in a wide-ranging 2001 interview that “My father would have been lynched if I hadn’t named me Woodrow Wilson.”<ref>James W. Wilkie and Rebecca Horn, “An Interview with Woodrow Borah”, ''Hispanic American Historical Review'' vol. 65(3) 1985, p. 403.</ref> He came from a family of Jewish businessmen.  He did not know how his parents and extended family came to be in Mississippi, but they moved to New York when Borah was young, then relocated to Los Angeles when he was an adolescent.  He attended high school in Los Angeles, which he remembered fondly for its rigorous teachers.  He enrolled at University of California, Los Angeles, graduating with a B.A. in history; he went on to earn a Master’s in history there. He began doctoral studies at UCLA, but his mentor urged him to go elsewhere since “You know the ropes here too well.You’re not really learning as much as you should”<ref>”Interview with Woodrow Borah,” p. 405.</ref>  He then transferred to UC Berkeley for his Ph.D., studying with [[Herbert E. Bolton]], [[Carl O. Sauer]], and Lesley Byrd Simpson, who had established Berkeley as a major center for Latin American studies.  In 1945, he married his wife Terry, with the union producing two children, Jonathan and Ruth.  Borah was raised a secular Jew, but he joined a congregation, had a religious wedding, and raised his children as Jews.  He spoke openly about the discrimination he encountered early in his career, advised by his mentor Bolton that he was unlikely to find an academic job because he was a Jew. “Berkeley itself had a good deal of anti-Semitism in the History Department until the early 1950s.”<ref>”Interview with Woodrow Borah”, p. 411.</ref> He spent the majority of his academic career at University of California, Berkeley, retiring in 1980.
  
==Academic career==
Borah was not interested in political history, and his Berkeley doctoral committee suggested that he pursue a dissertation on silk-raising in colonial Mexico, which he completed in 1940 and published in 1943.  In 1941, he unexpectedly got an offer to teach at Princeton for one year.  During World War II, he was recruited in 1942 to the [[Office of Strategic Services]], analyzing information on Latin America that might be useful for the war effort. His supervisor was Maurice Halperin, who was at the time a secret member of the Communist Party.<ref>Helen Delpar, ''Looking South: The Evolution of Latin Americanist Scholarship in the United States, 1850-1975''. Tuscaloosa: University of Alabama Press 2008, p. 124.</ref>  Borah remained with the OSS until 1947.  In 1948 he returned to UC Berkeley, hired by the Speech Department, where he remained for 14 years. In 1962, he moved to the History Department, becoming the Shepard Professor of History, where he remained until his retirement in 1980. Following the invasion of the Dominican Republic by the United States in 1965, Borah added his name to a letter signed by 103 Latin Americanists to President Lyndon Johnson, condemning the invasion.<ref>Delpar, ''Looking South'', p. 167.</ref>
  
Borah began collaborating with [[Sherburne F. Cook]] on the demography of indigenous populations in the Americas after the two had known each other for nearly 20 years.<ref>”Interview with Woodrow Borah”, pp. 419-20.</ref>  Their research interests carried them from Berkeley’s [[Bancroft Library]] to Mexican archives and travels on mule back.  The two published a series of monographs on the pre-Hispanic and colonial demography, ''The population of Central Mexico in 1548: an Analysis of the Suma de visitas de pueblos'' (1960); ''The Indian Population of Central Mexico on the Eve of the Spanish Conquest'' (1963); ''The Population of the Mixteca Alta, 1520-1960''(1968).and ''Essays in Population History: Mexico and the Caribbean'' (3 vols., 1971-79). They concluded that they preconquest Indian population living in central Mexico totaled over 25 million. Their unexpected high numbers implied that natives died by the  millions or tens of millions when the Spanish arrived, igniting a fierce debate among scholars. ''Essays in Population History'' applied a similar methodology to other areas in Latin America. In Hispaniola they estimated a population of 4 - 8 million. They attributed the  dramatic collapse in the Indian population to excessive hard labor imposed by the Spanish and the disruption of traditional society, as well as new European diseases.<ref>Carlos Perez, "Borah, Woodrow" in  Kelly Boyd, ed. ''Encyclopedia of Historians and Historical Writing, vol 1'' (1999) 1:107-9.</ref>

Borah also published important studies on economic history, including ''New Spain’s Century of Depression'', positing a downturn in the economy due to the decimation of the indigenous populations.  He examined the economic links in Spanish America in ''Early Colonial Trade and Navigation between Mexico and Peru'' (1954).  His last major monograph was ''Justice by Insurance: The General Indian Court of Colonial Mexico and the Legal Aides of the Half-Real'' (1983), which won the Conference of Latin American History’s Herbert E. Bolton Prize for the best book in English on Latin America.<ref> http://clah.h-net.org/?page_id=174</ref>

In 1979, the [[Conference on Latin American History]] conferred on Borah its highest honor, the Distinguished Service Award.<ref> http://clah.h-net.org/?page_id=188</ref>  He was asked by the National Autonomous University of Mexico (UNAM) to hold the Alfonso Caso Memorial Chair in 1981-82.<ref>Arnold J. Bauer, “Woodrow W. Borah (1912-1999)”. ''Hispanic American Historical Review'' 80(3)2000, p. 566.</ref>  As President of the Pacific Coast Branch of the [[American Historical Association]], Borah laid out his broad views in "Discontinuity and Continuity in Mexican History,"<ref> Woodrow Borah, "Discontinuity and Continuity in Mexican History,''Pacific Historical Review'', 48 (1980), 1-25.</ref>

Borah was well known for his biting wit and frank judgments. He “wielded a scathing and even eviscerating verbal style that might unnerve all but the most confident graduate student or thick-skinned colleague” but he also had a “fundamental , if often concealed, kindness.”<ref> Bauer, “Woodrow W.Borah”, pp. 566-57.</ref>

==Selected publications==
===Monographs===
*''Silk Raising in Colonial Mexico''. Berkeley: University of California Press, Ibero-Americana: 20, 1943.
*''New Spain's Century of Depression''. Berkeley: University of California Press, Ibero-Americana: 35, 1951 (published in Spanish ''El siglo de la depresión en Nueva España'' (Mexico City: Sep-Setentas, 1975)
*''Early Colonial Trade and Navigation between Mexico and Peru''. Berkeley: University of California Press, Ibero-Americana: 38, 1954 (published in Spanish: Comercio y navegación entre México y Perú en el siglo xvi (Mexico City: Instituto Mexicano de Comercio Exterior, 1975)
*''Price Trends of Some Basic Commodities in Central Mexico, 1551-1570''. Berkeley: University of California Press, Ibero-Americana: 40, 1958
*''The Population of Central Mexico in 1548. An Analysis of the Suma de visitas de pueblos''. Berkeley: University of California Press, Ibero-Americana: 43, 1960
*''The Indian Population of Central Mexico, 1531 - 1610''. Berkeley: University of California Press, Ibero-Americana: 44, 1960
*''The Aboriginal Population of Central Mexico on the Eve of the Spanish Conquest''. Berkeley: University of California Press, Ibero-Americana: 45, 1963
*''The Population of the Mixteca Alta, 1520-1960''. Berkeley: University of California Press, Ibero-Americana: 50, 1968
*''Essays in Population History: Mexico and the Caribbean'', 3 Vols.(Berkeley: University of California Press, 1971-79)
*''Justice by Insurance: The General Indian Court of Colonial Mexico and the Legal Aides of the Half-Real'' (Berkeley: University of California Press, 1983).

===Articles===
*"The Collection of Tithes in the Bishopric of Oaxaca during the Sixteenth Century," ''Hispanic American Historical Review'', 21 (Aug.1941), 386-409.
*"Archivo Municipal de Puebla. Guía para la consulta de sus materiales," ''Boletín del Archivo General de la Nación'', 13(1942), 207-239, 423-464.
*"Tithe Collection in the Bishopric of Oaxaca, 1601- 1867," ''Hispanic American Historical Review'', 29 (Nov.1949), 498-517.
*"Una interesante y enigmatica lista de extranjeros prisioneros en la Nueva España," ''Revista de Historia de América'', 31 (1951), 159-166.
*"Notes on Civil Archives in the City of Oaxaca," ''Hispanic American Historical Review'', 31 (Nov.1951), 723-749.
*"Race and Class in Mexico," ''Pacific Historical Review'', 23 (1954), 331-342.
*"The Rate of Population Change in Central Mexico, 1550- 1570," ''Hispanic American Historical Review'', 37 (Nov.1957), 463-470
*"America as Model: The Demographic Impact of European Expansion upon the Non-European World," Congreso Internacional de Americanistas, XXXV, Actas y Memorias, III (pub. 1962), 379-387
*"The Cortés Codex of Vienna and Emperor Ferdinand I," ''The Americas'', 19 (1962), 79-92.
*"La despoblación del México central en el siglo xvi," ''Historia Mexicana'', 12 (1962), 1-12
*"New Demographic Research on the Sixteenth Century in Mexico," in Howard F. Cline, ed., ''Latin American History: Essays on Its Study and Teaching'', 1898-1965, 2 vols. (Austin: University of Texas Press, 1967), II, 717- 722
*"Colonial Institutions and Contemporary Latin America: Political and Economic Life," ''Hispanic American Historical Review'', 43 (Aug.1963), 371-379.
*"Un gobierno provincial de frontera en San Luis Potosí (1612- 1620)," ''Historia Mexicana'', 13 (1964), 532-550.
*"Social Welfare and Social Obligation in New Spain: A Tentative Assessment," Congreso Internacional de Americanistas, XXXVI, Actas y Memorias, IV (pub. 1966), 45-57.
*"Marriage and Legitimacy in Mexican Culture: Mexico and California," ''California Law Review'', 54 (1966), 946- ioo8
*"La defensa fronteriza durante la gran rebelión tepehuana," ''Historia Mexicana'', 16 (1966), 15-29.
*"The California Mission," in Charles Wollenberg, ed., ''Ethnic Conflict in California History'' (Los Angeles: Tinnon-Brown Book Publishers, 1970), 3-22.
*"Juzgado General de Indios del Perú o Juzgado Particular de Indios de El Cercado de Lima," ''Revista Chilena de Historia del Derecho'' (Santiago), 6(1970), 128-142.
*"Latin America, 1610-1660," ''New Cambridge Modern History'' (Cambridge: Cambridge University Press, 1970), IV, 707-726.
*"The Urban Center as a Focus of Migration in the Colonial Period: New Spain," in Richard P. Schaedel, Jorge E. Hardoy, and Nora Scott Kinser, eds., ''Urbanization in the Americas from the Beginning to the Present'' (the Hague: Mouton, 1978), 383-397
*"Aging in Latin America during the Past Century," Congreso Internacional de Americanistas, XL, Atti, IV (pub. 1972), 257-276
*"The Historical Demography of Aboriginal and Colonial America: An Attempt at Perspective," in William N. Denevan, ed., The Native Population of the Americas in 1492 (Madison: University of Wisconsin Press, 1976), 13-34.
*"The Mixing of Populations," in Freddi Chiappelli, ed., ''First Images of America. The Impact of the New World on the Old'', 2 vols. (Berkeley: University of California Press, 1976), II, 707-722.
*"A Case History of the Transition from Precolonial to the Colonial Period in Mexico: Santiago Tejupan," in David J. Robinson, ed., ''Social Fabric and Spatial Structure in Colonial Latin America'' (Ann Arbor: University Microfilms, 1977), 409-432
*"Latin American Cities in the Eighteenth Century," in Woodrow Borah, Jorge E. Hardoy, and Gilbert A. Stelter, eds., ''Urbanization in the Americas: The Background in Historical Perspective'' (Ottawa: National Museums of Canada, 1980), 7-14.
*"Discontinuity and Continuity in Mexican History," ''Pacific Historical Review'', 48 (1980), 1-25.
*"The Spanish and Indian Law: New Spain," in George A. Collier, Renato I. Rosaldo, and John D. Wirth, eds., ''The Inca and Aztec States, 1400- 1800: Anthropology and History'' (New York: Academic Press, 1982), 265- 288.
*"Trends in Recent Studies of Colonial Latin American Cities," ''Hispanic American Historical Review'', 64 (Aug.1984), 535-554.


==References==
{{reflist|30em}}
{{authority control}}
{{DEFAULTSORT:Borah, Woodrow}}
[[Category:1912 births]]
[[Category:1999 deaths]]
[[Category:Historians of Mexico]]
[[Category:20th-century American historians]]
[[Category:People of the Office of Strategic Services]]
[[Category:University of California, Berkeley alumni]]
[[Category:University of California, Los Angeles alumni]]
[[Category:Jewish American historians]]
[[Category:Princeton University faculty]]
[[Category:National Autonomous University of Mexico faculty]]
[[Category:Writers from Mississippi]]
[[Category:People from Utica, Mississippi]]