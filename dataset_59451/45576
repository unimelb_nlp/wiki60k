{{Infobox military person
|name= James Stanley Scott
|image= 
|caption= 
|birth_date= {{birth date|1889|02|18}}
|death_date= {{death date and age|1975|07|19|1889|02|18}}
|birth_place= [[Roberval, Quebec]] 
|death_place= [[Halifax, Nova Scotia]]
|nickname=
|allegiance= {{flag|Canada|1921}}
|serviceyears= –1945
|rank= [[Air Commodore]]
|branch= {{flagicon|Canada|air force-1924}} [[Royal Canadian Air Force]]
|commands=
|unit=
|battles= [[World War I]]<br/>[[World War II]]
|awards= [[Military Cross]]<br/>[[Air Force Cross (United Kingdom)|Air Force Cross]]
|laterwork=
}}
[[Air Commodore]] '''James Stanley Scott''' {{postnominals|country=CAN|size=100%|sep=,|MC|AFC}} (February 18, 1889 – July 19, 1975) was a leading figure in the pre-World War II [[Royal Canadian Air Force]] and a [[Royal Flying Corps]] officer during [[World War I]].

==Career==
Scott was born in [[Roberval, Quebec]] in 1889 and graduated from [[Quebec High School]].<ref name=awards>[http://airforce.ca/awards.php?search=&keyword=&page=50&mem=&type=flyingww1 Air Force awards]</ref>

In March 1916 Scott was seconded from the [[Canadian Artillery]] to the Royal Flying Corps.<ref>{{London Gazette |issue=30416 |date=4 December 1917 |startpage=12795 |supp=x |accessdate=9 December 2012 }}</ref>  Only four months later in July 1916, while serving as a [[Lieutenant (British Army and Royal Marines)|lieutenant]], Scott was awarded the [[Military Cross]] for attacking a train well behind the German lines even though his aircraft was very badly damaged by enemy fire.<ref>{{London Gazette |issue=29684 |date=25 July 1916 |startpage=7429 |endpage=7434 |supp=x |accessdate=9 December 2012}}</ref>  He transferred to the [[Royal Air Force]] in 1918 and after the Armistice he was awarded the [[Air Force Cross (United Kingdom)|Air Force Cross]].<ref>{{London Gazette |issue=31098 |date=31 December 1918 |startpage=97 |endpage=98 |supp=x |accessdate=9 December 2012}}</ref>

After the War Scott returned to Canada, and after promotion to [[Wing commander (rank)|wing commander]], he served as the [[Commander of the Royal Canadian Air Force|Officer Commanding]] the [[Canadian Air Force (1920–1924)|Canadian Air Force]] from 1921 to 1922.<ref>{{cite book |last=Goodspeed |first=Donald James |title=The Armed Forces of Canada, 1867–1967: a century of achievement |year=1967 |publisher=Directorate of History, Canadian Forces Headquarters |page=273}}
</ref>  Two years later, Scott who was promoted to [[group captain]] again held the Air Force's senior post, this time as the Director of the [[Royal Canadian Air Force]].<ref>[http://www.flightglobal.com/pdfarchive/view/1927/1927%20-%200154.html Flight Global]</ref>  During this time he petitioned his superior Major-General [[James Howden MacBrien|J H MacBrien]] for permission for the Air Force to stop focussing on forestry and photography work in order to train as a fighting force. Scott's request was refused and he continued in post until 1928.<ref>[http://www.legionmagazine.com/en/index.php/2008/03/cameras-take-flight-air-force-part-26/ Cameras take flight]</ref>

On 1 April 1931, Scott was granted the honorary rank of [[air commodore]].<ref>[http://www.rcaf-arc.forces.gc.ca/CFAWC/eLibrary/Journal/Vol3-2010/Iss4-Fall/Sections/05-Honorary_Ranks_of_the_RCAF_e.pdf Honorary Ranks of the RCAF]</ref>

Scott left the RCAF and returned to duty in 1939 and served during [[World War II]] and retired in 1945.<ref name=awards/>

Scott died in [[Halifax, Nova Scotia]] in 1975.<ref name=awards/>

==Notes==
{{reflist}}

{{s-start}}
{{s-mil}}
|-
{{s-bef|before=[[Ronald Francis Redpath]]}}
{{s-ttl|title=[[Commander of the Royal Canadian Air Force|Officer Commanding the Canadian Air Force]]|years=1921–1922}}
{{s-aft|after=[[Lindsay Gordon]]<br><small>As Director</small>}}
|-
{{s-bef|before=[[William George Barker]]}}
{{s-ttl|title=[[Commander of the Royal Canadian Air Force|Director of the Royal Canadian Air Force]]|years=1924–1928}}
{{s-aft|after=[[Lloyd Samuel Breadner]]}}
|-
{{s-end}}

{{Commander of the Royal Canadian Air Force}}

{{DEFAULTSORT:Scott, James Stanley}}
[[Category:1889 births]]
[[Category:1975 deaths]]
[[Category:British military personnel of World War I]]
[[Category:Canadian recipients of the Military Cross]]
[[Category:Royal Air Force officers]]
[[Category:Royal Canadian Air Force officers]]
[[Category:Royal Canadian Air Force personnel of World War II]]
[[Category:Royal Flying Corps officers]]