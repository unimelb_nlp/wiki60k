{{Infobox journal
| title = The American Naturalist
| cover =
| discipline = [[Ecology]], [[evolution]], [[population biology]]
| abbreviation = Am. Nat.
| editor = Judith L. Bronstein
| publisher = [[University of Chicago Press]] on behalf of the [[American Society of Naturalists]]
| country = United States
| frequency = Monthly
| history = 1867–present
| impact = 4.725
| impact-year = 2011
| website = http://www.journals.uchicago.edu/an
| link1 = http://www.amnat.org/
| link1-name = Society website
| ISSN = 0003-0147
| JSTOR = 00030147
| OCLC = 45446849
| LCCN = 00-227441
}}
'''''The American Naturalist''''' is a monthly [[Peer review|peer-reviewed]] [[scientific journal]] that was established in 1867. It is published by the [[University of Chicago Press]] on behalf of the [[American Society of Naturalists]]. The journal covers research in [[ecology]], [[evolutionary biology]], population, and integrative [[biology]]. {{As of |2016}}, the [[editor-in-chief]] is Judith L. Bronstein. According to the ''[[Journal Citation Reports]]'', the journal had a 2011 [[impact factor]] of 4.736, ranking it 21st out of 131 journals in the category "Ecology"<ref name=WoS>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Ecology |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |work=Web of Science |postscript=.}}</ref> and 10th out of 45 journals in the category "Evolutionary Biology".<ref name=WoS2>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Evolutionary Biology |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |work=Web of Science |postscript=.}}</ref>

==History==

The journal was founded by [[Alpheus Hyatt]], [[Edward Sylvester Morse|Edward S. Morse]], [[Alpheus Spring Packard Jr.|Alpheus S. Packard Jr.]], and [[Frederick Ward Putnam|Frederick W. Putnam]] at the [[Essex Institute]] in [[Salem, Massachusetts]].<ref>{{cite journal|author=Dunn, L. C.|authorlink=L. C. Dunn|title=''The American Naturalist'' in American Biology|journal=The American Naturalist|date=Sep–Oct 1966|volume=100|issue=915|pages=481–492|jstor=2459204|doi=10.1086/282444}}</ref> The first issue appeared in print dated March 1867.<ref name=Periodicals>"American Naturalist," in International Magazine Co., ''Periodicals,'' vol. 1, no. 1 (October-December 1917), pg. 5.</ref>

In 1878 the journal was for sale and [[Edward Drinker Cope|Edward Cope]] bought half the rights. He moved the journal to Philadelphia and arranged to edit it jointly with Professor Alpheus S. Packard Jr. Cope became editor-in-chief in 1887 and continued in that capacity until his death in 1897.<ref>[http://www.nasonline.org/publications/biographical-memoirs/memoir-pdfs/cope-edward.pdf National Academy of Sciences Biographical Memoir of Edward Drinker Cope]</ref> 

In 1897, a group of professors from [[M.I.T.]], [[Harvard]], and [[Tufts]] bought the rights from the Cope estate and kept the journal in publication until 1907 when [[James McKeen Cattell|J. McKeen Cattell]] acquired control.<ref>{{cite journal|author=Conklin, Edwin G.|authorlink=Edwin Conklin|title=The Early History of ''The American Naturalist''|journal=The American Naturalist|year=1943|volume=78|issue=774|pages=29–37|doi=10.1086/281164}}</ref> Cattell's son Jacques became co-editor and publisher with his father in 1939.<ref name="history ASN">{{cite web|url=http://www.amnat.org/about/history/timeline.html|title=History of the ASN|accessdate=5 December 2013|year=2012|author=American Society of Naturalists}}</ref> 

Although the ASN became increasingly involved in editing ''The American Naturalist'' through changes in 1941 and 1951, the journal remained with the Cattell family until 1968, when the University of Chicago Press took it over after [[Jacques Cattell]]'s death.<ref name="history ASN"/>

==References==
{{reflist|30em}}

== External links ==
* {{Official website|http://www.journals.uchicago.edu/an}}
* [https://archive.org/details/recordsofamerica01amer Archive Records of the American Society of Naturalists (1884)] containing out of copyright issues of ''The American Naturalist'' and other records of the society

{{DEFAULTSORT:American Naturalist}}
[[Category:Biology journals]]
[[Category:Ecology journals]]
[[Category:University of Chicago Press academic journals]]
[[Category:Publications established in 1867]]
[[Category:English-language journals]]
[[Category:Monthly journals]]
[[Category:Academic journals associated with learned and professional societies of the United States]]