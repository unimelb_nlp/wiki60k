{{Orphan|date=March 2016}}

{{Infobox officeholder
| name        = Fadhel M. Ghannouchi
| image       =
| imagesize   = 200px
| caption     = P. Eng., Ph.D., FIEEE, FCAE, FRSC, FEIT, FEIC
| birth_date  = 1958
| birth_place = Gabes, Tunisia
| citizenship = Canadian & Tunisian 
| residence   = Calgary, AB, Canada
| known_for   = RF Microelectronics and Wireless Communications<br />Software-Defined Radio<br />Digital Predistortion
| occupation  = Professor of Electrical and Computer Engineering
| title       = Canada Research Chair (Tier 1) in Green Radio Systems<br />AITF Strategic Chair- in Intelligent RF Radio Technology<br />Founder and Director of iRadio Laboratory
}}

'''Fadhel M. Ghannouchi''' (born 1958 in [[Gabes]], [[Tunisia]]) is a Tunisian Canadian [[electrical engineer]], who conducts research  in  radio frequency (RF) technology and [[wireless communications]].

== Education ==
Ghannouchi obtained his  B.Sc.  in 1983, from [[Ecole Polytechnique de Montreal]] in  ([[Montreal,Canada]]). He completed his  [M.Sc.  (1984) and [[PhD]] (1987) at the [[University of Montreal]] and became a researcher at Ecole Polytechnique de Montreal. He was appointed Assistant Professor there in 1990,  and  Associate Professor  and the director of the Ampli Lab in 1994. In 1997, he becamea  Professor] in the department of Electrical Engineering and the Director of the M.Sc. in Micro-electronics program at Ecole Polytechnique de Montreal (Montreal, Canada) until 2005.   In 2005, Ghannouchi joined the University of Calgary as a Professor and Research Chair at the department of Electrical and Computer Engineering, where he founded the Intelligent RF Radio Laboratory (iRadio Lab), supported by   grants from the Government of [[Alberta]] ([[iCORE]]) and the Government of Canada (Canada Research Chair) and the  Canada Foundation of Innovation (CFI), as well as  industrial partners.<ref>{{cite web|url=http://www.labcanada.com/news/194m-creates-new-canada-research-chairs/1000035082/?&er=NA |title= $194M creates new Canada research chairs|publisher=[[LabCanada]] |accessdate={{Format date|2015|7|6}}}}</ref>

== Career ==
In 2005, he moved to the  [[University of Calgary]], as  Professor in the Department of Electrical and Computer Engineering, holding s Tier-1 [[Canada Research Chair]] in Green Radio Systems  and an Alberta Innovates Technology Futures  Tier-1 strategic Chair in Intelligent Radio-Frequency (RF)  Radio Technology.<ref>{{cite web|url=http://www.albertatechfutures.ca/AcademicProgramsiCORE/StrategicChairProgram/StrategicChairs/FadhelGhannouchi.aspx |title=AITF Strategic Chair Profile - Fadhel Ghannouchi |publisher=Alberta Innovates Technology Futures |accessdate={{Format date|2015|7|6}} |deadurl=yes |archiveurl=https://web.archive.org/web/20150926021752/http://www.albertatechfutures.ca/AcademicProgramsiCORE/StrategicChairProgram/StrategicChairs/FadhelGhannouchi.aspx |archivedate=2015-09-26 |df= }}</ref> 
He is the founder and director of Calgary's Intelligent RF Radio Laboratory (iRadio Lab)  He also holds an Honorary Distinguished Professorship at the [[Ningbo University]] in [[China]]<ref>{{cite web|url= http://news.nbu.edu.cn/xiangNews.aspx?id=85588 |title= Ningbo University Distinguished Guest Professorship Announcement |publisher=[[Ningbo University]] }}</ref>  Ghannouchi has supervised 50+ PhD  and 30+ postdoctoral researchers. He has  founded 3 spin-off companies with his collaborators and students: EMWorks (1995), Amplix (1998) acquired by Mitec Telecom in 2001, Green Radio Technologies (2011).<ref>{{cite web|url=http://www.cmc.ca/~/media/AboutCMC/SuccessStories/All/Making_Wireless_Communication_Gannouchi_Belostotski-EN-LO_2.pdf?la=en |title= Making wireless communications more energy efficient |publisher=[[CMC Microsystems]] |accessdate={{Format date|2012|3|30}}}}</ref>

==Research  ==
His research interests are   microwave and RF instrumentation and measurements, nonlinear modeling of microwave devices and communications systems, design of power and spectrum efficient microwave amplification systems and design of intelligent RF transceivers and [[Software-defined radio|SDR]] radio systems for wireless and satellite communications. He   has 650+ referred publications, 20 patents (10 pending)  and 4 books in the area.s.<ref>{{cite web|url=http://www.ieeeottawa.ca/ap_mtt/docs/FadhelGhannouchi_Mar61.pdf |title= Fadhel Ghannouchi Bio |publisher=[[Carleton University]] |accessdate={{Format date|2015|8|17}}}}</ref> His work on designing energy-efficient wireless transmitters has been featured in media outlets such as [[CBC Radio]].<ref>{{cite web|url=http://www.cbc.ca/news/canada/calgary/prof-tries-to-dial-down-greenhouse-emissions-from-cellphone-use-1.753123 |title= Prof tries to dial down greenhouse emissions from cellphone use |publisher=[[Canadian Broadcasting Corporation|CBC Calgary]] |date={{Format date|2008|12|08}}}}</ref>

[[File:Iradio2 20101109 UofC 02.jpg|thumb|Group shot of Prof. Ghannouchi with the iRadio Lab team (2011).]]

==Books==
*F. M. Ghannouchi, O. Hammi and M. Helaoui, Behavioral Modelling and Predistortion of Wideband Wireless Transmitters. Wiley, 2014.
*F. M. Ghannouchi and M. S. Hashmi, Load-Pull Techniques with Applications to Power Amplifier Design. Springer, 2012.
*A. Mohammadi and F. M. Ghannouchi, RF Transceiver Design for MIMO Wireless Communications. Springer, 2012.
*F. M. Ghannouchi and A. Mohammadi, The Six-Port Technique with Microwave and Wireless Applications. Norwood, MA: Artech House, 2009.
 
== Awards and accolades ==

*Fellow of the [[Institute of Electrical and Electronics Engineers]] (IEEE) in 2007<ref>{{cite web|url=http://services27.ieee.org/fellowsdirectory/getdetailprofile.html?custNum=XMN1%2BhxDg2bi4Q2sGvqOzA%3D%3D&bccaptions=Alphabetical%20Listing%20&bclocations=%2Ffellowsdirectory%2Fhome.html |title = IEEE Fellow Listing: Fadhel M. Ghannouchi |publisher=[[IEEE]]}}</ref>
*Fellow of The [[Institution of Engineering and Technology]] (IET) in 2008.
*Fellow of the [[Engineering Institute of Canada]] (EIC) in 2009<ref>{{cite web|url=http://eic-ici.ca/PDFs/honours_award/cit09/Ghannouchi.pdf |title = Engineering Institute of Canada Fellow Citation: Fadhel M. Ghannouchi |publisher=[[Engineering Institute of Canada|EIC]]}}</ref>
*Fellow of the [[Canadian Academy of Engineering]] (CAE) in 2009<ref>{{cite web|url=http://www.cae-acg.ca/links-of-interest/ |title = Canadian Academy of Engineering Fellow List: Fadhel M. Ghannouchi |publisher=[[Canadian Academy of Engineering|CAE]]}}</ref>
*Outstanding Leadership in Alberta Technology Award (2014) from the ASTech Foundation <ref>{{cite web|url=http://astech.ca/awardee/2014-technology-ghannouchi-dr-fadhel |title = ASTech Award Profile : Fadhel M. Ghannouchi |publisher=[[ASTech]]}}</ref>
*[[Fellow]] of The [[Royal Society of Canada]] (2010)<ref>{{cite web|url=https://rsc-src.ca/en/search-fellows?keywords_44=&first_name=&last_name=&display_name=&election_year_21=&academy_25=All&division_24=All&discipline_23=All&is_deceased=0&page=62&sort_by=last_name&sort_order=ASC |title = Royal Society of Canada Member List : Fadhel M. Ghannouchi |publisher=[[Royal Society of Canada]]}}</ref>
*Alberta Ingenuity Fund Research Excellence Award (2009) from the [[Association of Professional Engineers and Geoscientists of Alberta]] ([[APEGA]])<ref>{{cite web|url=http://www.apega.ca/members/Awards/SA2009/albertaingenuity.html |title=2009 SUMMIT AWARD® RECIPIENTS |publisher=[[APEGA]] }}{{dead link|date=December 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>

==References==
{{reflist|2}}

== External links ==
* [http://schulich.ucalgary.ca/profiles/fadhel-ghannouchi profile at Schulich School of Engineering Profile]
* [http://www.iradio.ucalgary.ca/ iRadio Lab website]
* [https://www.researchgate.net/profile/FM_Ghannouchi ResearchGate page]

{{Authority control}}

{{DEFAULTSORT:Ghannouchi, Fadhel}}
[[Category:Living people]]
[[Category:Canadian electrical engineers]]
[[Category:Fellows of the Canadian Academy of Engineering]]
[[Category:1958 births]]
[[Category:University of Calgary faculty]]
[[Category:Fellow Members of the IEEE]]
[[Category:Fellows of the Royal Society of Canada]]
[[Category:Canadian people of Tunisian descent]]
[[Category:Articles created via the Article Wizard]]