<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Type O
 | image=File:1A1182101XXXVII045 (15205000640).jpg
 | caption=The Caudron O at the 1914 Fete d'Aviation, held at the [[Longchamps racecourse]], [[Paris]]
}}{{Infobox Aircraft Type
 | type=Sports aircraft
 | national origin=[[France]]
 | manufacturer=[[Caudron]]
 | designer=Gaston Caudron
 | first flight=January–February 1914
 | number built=1
 }}
 |}
The '''Caudron Type O''' was a [[France|French]] single seat [[air racing]] [[biplane]] flown in 1914.

==Development==

The Type O was a [[biplane#Bays|single bay biplane]] with no [[stagger (aeronautics)|stagger]]. Both wings had two wooden [[spar (aviation)|spar]]s and were [[aircraft fabric covering|fabric covered]].  On each side there were two pairs of parallel [[interplane strut]]s joining the spars, one outboard and the other passing through the fuselage between the centre sections. These placed the upper wing well above the [[fuselage]] and the lower one a little below it.  The usual crossed diagonal pairs of [[flying wire]]s braced the bays.  The Type O used [[wing warping]] rather than [[aileron]]s for lateral control.<ref name=Laero/>

When it first flew in the early weeks of 1914, it was powered by a semi-cowled [[Anzani 6-cylinder|Anzani 6-cylinder radial]].<ref name=Hauet/>  There were two versions of this engine with different [[engine displacement|displacement]]s; l'Aérophile states a power output of {{convert|45|hp|kW|abbr=on|0}},<ref name=Laero/> corresponding to the smaller version, but Hauet quotes {{convert|50–60|hp|kW|abbr=on|0}},<ref name=Hauet/> that of the larger engine. By May 1914 it was flying with an uncowled {{convert|100|hp|kW|abbr=on|0}} [[Anzani 10-cylinder|Anzani 10-cylinder radial]].<ref name=Hauet/>

The fuselage was recycled from one of Caudron's earlier [[monoplanes]], the very similar [[Caudron Types M and N|Types M and N]], and was built around an [[Sorbus|ash]] lattice [[girder]] of square section which tapered to the rear. [[Stringer (aircraft)|Stringers]], stood off from the girder, gave the fabric covered fuselage a more rounded cross-section. An open, single seat [[cockpit]] was placed under the wing [[trailing edge]].<ref name=Laero/> When it first flew the Type O had an almost square, upright vertical tail with little or no [[fin]] and a large [[rudder]] reaching down to the keel.  The horizontal tail, narrow and with a straight, unswept [[leading edge]] was mounted on top of the fuselage so the rudder operated in an [[elevator (aircraft)|elevator]] cut-out. Later in the year, the aircraft, now with the 100&nbsp;hp Anzani and a modified upper forward fuselage, had a very different tail with a larger fin which had a long, curving leading edge, its contour continuing into that of a broad, deep rudder.  There may also have been wing modifications as well; l'Aéroplane describes the upper and lower wings as having the same span, whereas in Hauet's account the span of the upper wing was the greater.<ref name=Hauet/>

The Type O had an all-steel [[landing gear|tailskid undercarriage]], with a pair of spoked mainwheels on split axles hinged from the centre of a transverse rod mounted on four longitudinal V-[[strut]]s, arranged as an inverted W from the inner, under-fuselage interplane struts. Rubber springs damped the movement of the outer ends and wheels on landing.<ref name=Laero/>

==Operational history==

Early in its life the Type O acquired the nickname of "The soap box". In May 1914, re-engined and with its new tail, it was flown by Chanteloup in a race at the [[Bois de Boulogne]]; in June it flew in [[Vienna]].  In September, a month after the outbreak of [[World War I]], it was delivered to the military.<ref name=Hauet/>

==Specifications (100 hp Anzani)==
{{Aircraft specs
|ref=Hauet (2001)<ref name=Hauet/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=One
|length m=5.00
|length note=; l'Aérophile<ref name=Laero/> gives {{convert|7|m|ftin|abbr=on|0}}
|upper span m=7.40
|upper span note=
|lower span m=6.80
|lower span note=
|height m=2.50
|height note=
|wing area sqm=14.50
|wing area note=<ref name=Laero/>
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Anzani 10-cylinder|Anzani]]
|eng1 type=two row, 10-cylinder [[radial engine|radial]]
|eng1 hp=100
|eng1 note=
|more power=

|prop blade number=two
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=146
|max speed note=<ref name=Laero/>
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|more performance=

}}

==References==
{{reflist|refs=

<ref name=Hauet>{{cite book |title=Les Avions Caudrons |last=Hauet|first=André|year=2001|volume=1|publisher=Lela Presse|location=Outreau|isbn=2 914017-08-1|page=66}}</ref>

<ref name=Laero>{{cite magazine |last= |first= |authorlink= |coauthors= |date=1 August 1914 |title=Le biplane de Course "Caudron"|magazine=L'Aérophile|volume=22|issue=15 |pages=343–345|url=http://gallica.bnf.fr/ark:/12148/bpt6k65850910/f15 }}</ref>

}}
<!-- ==Further reading== -->
<!-- ==External links== -->

{{Caudron aircraft}}

[[Category:Caudron aircraft|TO]]
[[Category:French sport aircraft 1910–1919]]