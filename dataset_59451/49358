{{Infobox person
|name          = Richard Lonergan
|image         = 
|image_size    = 
|caption       = 
|birth_name    = 
|birth_date    = {{birth date|1900|01|16}}
|birth_place   = [[Brooklyn, New York]], [[United States]]
|death_date    = {{death date and age|1925|12|26|1900|01|16}}
|death_place   = [[South Brooklyn|South Brooklyn, New York]]
|death_cause   = [[Murdered]]
|resting_place = 
|resting_place_coordinates = 
|residence     = 
|nationality   = [[Irish-American]]
|other_names   = Peg Leg Lonergan<br>Pegleg Lonergan
|known_for     = Leader of the [[White Hand Gang]]
|education     =
|alma_mater    = 
|employer      = 
|occupation    = Mobster 
|home_town     = 
|title         = 
|salary        = 
|networth      = 
|height        = 
|weight        = 
|term          = 
|predecessor   = 
|successor     = 
|party         = 
|boards        = 
|religion      = [[Catholic]]
|spouse        = 
|partner       = 
|children      = 
|parents       = John Lonergan
|relations     = Anna Lonergan (sister)<br>[[Bill Lovett]] (brother-in-law)<br>Matty Martin (brother-in-law) 
|signature     = 
|website       = 
|footnotes     = 
}}

'''Richard Joseph "Peg Leg" Lonergan''' (January 16, 1900 - December 26, 1925) was an American underworld figure and labor racketeer. He was a high-ranking member and the final leader of the [[White Hand Gang]]. He succeeded [[Bill Lovett]] after his murder in 1923 and, under his leadership, led a two-year campaign against [[Frankie Yale]] over the New York waterfront until he and five of his lieutenants were killed in [[South Brooklyn]] during a [[Christmas Day]] celebration at the [[Adonis Social Club]] in 1925.<ref>[[Herbert Asbury|Asbury, Herbert]]. ''The Gangs of New York: An Informal History of the New York Underworld''. New York: Alfred A. Knopf, 1928. (pg. 78) ISBN 1-56025-275-8</ref>

==Biography==
Richard Lonergan was one of fifteen children, among them being Anna Lonergan known as "Queen of the Irishtown docks", born to local [[prize fighter]] and [[bare knuckle boxing|bare knuckle boxer]] John Lonergan. Raised in Irishtown, an [[Irish-American]] enclave between the [[Manhattan]] and [[Brooklyn]] [[Dock (maritime)|waterfront]], he later lost his right leg in a [[trolley car]] accident as a child from which his underworld nickname "Peg Leg" originated. A childhood friend and later brother-in-law of [[Bill Lovett]], Lonergan had earned a fearsome reputation in Irishtown and on the Brooklyn waterfront as a vicious street brawler after killing a Sicilian drug dealer in a Navy Street bike shop. Believed by authorities to have been involved in at least a dozen murders during his criminal career, he was reportedly well known for his hatred of [[Italian-Americans]] and would occasionally lead "[[ginzo|ginzo hunting]]" expeditions in [[Western saloon|saloons]] and [[dive bar]]s along the waterfront. He became the leader of the [[White Hand Gang]] shortly after the murder of its leader Bill Lovett in 1923. Lonergan spent the next two years battling [[Frankie Yale]] over control of the New York waterfront.<ref name="Bergreen">Bergreen, Laurence. ''Capone: The Man and the Era''. New York: Simon and Schuster, 1996. (pg. 156-158) ISBN 0-684-82447-7</ref><ref name="Johnson">Johnson, Curt and R. Craig Sautter. ''The Wicked City: Chicago from Kenna to Capone''. New York: Da Capo Press, 1998. (pg. 203-204) ISBN 0-306-80821-8</ref><ref name="English">[[T.J. English|English, T.J.]] ''Paddy Whacked: The Untold Story of the Irish American Gangster''. New York: HarperCollins, 2005. (pg. 158-159) ISBN 0-06-059002-5</ref><ref name="Parr">Parr, Amanda J. ''The True and Complete Story of 'Machine Gun' Jack McGurn: Chief Bodyguard and Hit Man to Chicago's Most Infamous Crime Czar Al Capone and Mastermind of the St. Valentine's Day Massacre''. Leicester: Troubador Publishing Ltd, 2005. (pg. 125-129) ISBN 1-905237-13-8</ref> 

On the night of December 25, 1925, Lonergan and five of his men entered the [[Adonis Social Club]] during a [[Christmas]] celebration. Lonergan and the other White handers, according to witnesses, were intoxicated and being unruly to other patrons. Lonergan himself loudly and openly called nearby customers "[[wop]]s", "[[List_of_ethnic_slurs#D|dago]]s" and other [[ethnic slurs]]. When three local Irish girls entered the club escorted by their Italian dates, Lonergan chased them out supposedly yelling at them to "Come back with white men, fer chrissake!" It was at that moment that the lights went out and gunfire was heard. Customers rushed for the exits in a panic as glass was shattered as well as tables and chairs being overturned. As police arrived, they found one of Lonergan's men, his best friend Aaron Harms, dead in the street and they followed a blood trail into the club where they found Lonergan and drug addict Cornelius "Needles" Ferry on the dance floor near a [[player piano]] shot execution style. A fourth member, James Hart, managed to escape, having been found a few blocks away crawling on the sidewalk after being shot in the thigh and leg. He was taken to the Cumberland Street Hospital where he eventually recovered but refused to cooperate with police. He denied being at the club claiming he had been shot by a stray bullet from a passing car. The two other members, Joseph "Ragtime Joe" Howard and Patrick "Happy" Maloney, were apparently unaccounted for leaving no witnesses willing to testify.<ref name="Johnson"/><ref name="Kobler">Kobler, John. ''Capone: The Life and World of Al Capone''. New York: Da Capo Press, 2003. (pg. 163-164) ISBN 0-306-81285-1</ref> Although seven men had been arrested in connection to the shooting, including a visiting [[Al Capone]], all the men were released on bail ranging from $5,000 to $10,000 and the case was eventually dismissed.<ref name="Bergreen"/><ref name="Johnson"/><ref name="English"/><ref name="Parr"/> 

Anna Lonergan publicly blamed the gangland shooting on "foreigners" commenting "You can bet it was no Irish American like ourselves who would stage a mean murder like this on Christmas Day".<ref name="English"/><ref name="Kobler"/> The killings are generally attributed to Capone, in partnership with Frankie Yale, although these often colorful accounts are sometimes vague and inconsistent but allege that the incident was prearranged. It is with the death of Lonergan however that the White Hand Gang disappeared from the Brooklyn waterfront allowing Frankie Yale and eventually the [[Five Families]] to take control.<ref name="Bergreen"/><ref name="Parr"/><ref name="Kobler"/><ref>Mannion, James. ''The Everything Mafia Book: True Life Accounts of Legendary Figures, Infamous Crime Families, and Chilling Events''. Avon, Massachusetts: Everything Books, 2003. (pg. 40) ISBN 1-58062-864-8</ref><ref>Toomey, Jeanne. ''Assignment Homicide: Behind the Headlines''. Santa Fe: Sunstone Press, 2006. (pg. 53) ISBN 0-86534-517-1</ref>

==References==
{{Reflist}}

==Further reading==
*Johnson, Malcolm Malone; Haynes Johnson and Budd Schulberg. ''On the Waterfront: The Pulitzer Prize-winning Articles that Inspired the Classic Movie and Transformed the New York Harbor''. New York: Chamberlain Bros., 2005. ISBN 1-59609-013-8
*Pasley, Fred D. ''Not Guilty - The Story of Samuel S. Leibowitz''. New York: G.P. Putnam's Sons, 1933.

{{DEFAULTSORT:Lonergan, Richard}}
[[Category:1925 deaths]]
[[Category:1900 births]]
[[Category:American amputees]]
[[Category:American mobsters of Irish descent]]
[[Category:Criminals from New York City]]
[[Category:Murdered American mobsters of Irish descent]]
[[Category:People from Brooklyn]]
[[Category:People murdered in New York]]
[[Category:Prohibition-era gangsters]]