'''Lloyd Louis Brown''' (April 3, 1913 – April 1, 2003) was an [[United States|American]] labor organizer, [[Communist Party USA|Communist Party]] activist, journalist, novelist, friend and editorial companion of [[Paul Robeson]]'s, and a Robeson biographer.

== Early life ==
Brown was born Lloyd Dight in [[St. Paul, Minnesota]], son of [[African-American]] Ralph Dight, a waiter originally from [[Alabama]], and Magdalena Paul Dight, from [[Stearns County, Minnesota]].

Brown and his three siblings were raised [[Roman Catholic]] and attended St. Peter Claver Church in St. Paul, an African-American parish. After Magdalena Dight died in 1917 Brown and his brother Ralph lived first in the Catholic Orphan Asylum in [[Minneapolis]], then for two years in the Crispus Attucks Home, an African-American orphanage and old folks home in St. Paul.

== Education ==
Brown attended the Cathedral School through eighth grade, then Cretin (now [[Cretin-Derham Hall]]) After receiving a reprimand in catechism class he quit school and educated himself for a year at the St. Paul Public Library. He also joined the [[Young Communist League USA|Young Communist League]] (then known as the Young Workers League.)

== Labor organizing ==
In 1929 Brown left St. Paul for [[Youngstown, Ohio]], to work in the steel mills there. Because of the stock market crash of that year no steel jobs could be had, so Brown found work of a different sort: at the age of 16 he became a Communist labor organizer. He then took the surname Brown in honor of the anti-slavery zealot [[John Brown (abolitionist)|John Brown]].<ref>Nelson, Paul D., "Orphans and Old Folks Revisited, With a Story by Lloyd L. Brown", ''Minnesota History'', Fall 2001, pp. 368–379.</ref>
Lloyd Brown spent the next decade as a labor organizer in Ohio, [[New Jersey]], [[Connecticut]], and [[Pennsylvania]], and also visited the [[Soviet Union]] as a journalist. His labor organizing in Western Pennsylvania landed him a stint in the [[Allegheny County Jail]] in [[Pittsburgh]]. After release he joined the [[US Army Air Corps]] and rose to the rank of sergeant.<ref> Brown, Lloyd L., ''Iron City.'' Masses & Mainstream, 1952 (3rd printing, paper), back cover.</ref>

== Literary career ==
After World War II Brown moved to [[New York City]] and began writing, first for the weekly ''[[The New Masses]]'', and then its successor, the monthly ''[[Masses & Mainstream]],'' both Marxist journals. Brown wrote on labor organizing, lynchings, baseball, among other topics, plus fiction and editorials. He served as managing editor of ''New Masses'' from 1946 to 1948 and associate editor of ''Masses & Mainstream'' from 1948 to 1952. In this literary environment Brown worked with celebrated leftist writers such as [[Dalton Trumbo]], [[Meridel LeSueur]], [[Herbert Aptheker]], and, of greatest consequence to Brown, [[Paul Robeson]].

In 1951 Brown published a novel, ''Iron City'', based on his experiences in Allegheny County Jail, the fictionalized tale of his and other inmates’ efforts to save Willie Jones, condemned to death for murder.

Brown began working with Paul Robeson in 1950, helped him write his column for the [[Harlem]] newspaper, ''Freedom'', and 1958 his autobiography, ''Here I Stand''.<ref> Duberman, Martin, ''Paul Robeson.'' Alfred A. Knopf, 1988, pp. 393, 448, 458; "Lloyd L. Brown," in Andrews, William L., et al, eds, ''Oxford Companion to African American Literature,'' Oxford University Press, 2001, pp. 219–220.</ref> In 1997 he published a partial biography, ''The Young Paul Robeson: On My Journey Now'' (Westview Press). In 2001 the [[Minnesota Historical Society]] republished, in its quarterly magazine ''Minnesota History'', Brown’s 1948 fictionalized memoir of his years at the Crispus Attucks Home, ''God’s Chosen People.''

== Later life ==
Brown lived his last decades in New York City. He and his wife, Lily Brown, of a New York Jewish family, had two daughters, Bonnie and Linda. Lily Brown died in 1996; a playground in Fort Washington Park is named for her. Lloyd Brown died in New York on April 1, 2003.<ref>{{cite web |url= https://www.nytimes.com/2003/04/14/us/lloyd-l-brown-89-journalist-and-paul-robeson-biographer.html |title= Lloyd L. Brown, 89, Journalist And Paul Robeson Biographer |author= Lavietes, Stuart |date= April 14, 2003 |work= [[The New York Times]]}}</ref>

== External links ==
[[iarchive:LloydBrownFBIFile|FBI files on Lloyd Brown]]

== References ==
{{Reflist}}

{{Authority control}}

{{DEFAULTSORT:Brown, Lloyd L}}
[[Category:1913 births]]
[[Category:2003 deaths]]
[[Category:American male writers]]
[[Category:American male journalists]]
[[Category:American communists]]
[[Category:American labor unionists]]
[[Category:Members of the Communist Party USA]]
[[Category:Communist writers]]
[[Category:African-American novelists]]