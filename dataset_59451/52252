{{Infobox journal
| title = Medical Decision Making
| cover = [[File:Medical Decision Making.jpg]]
| editor = Alan J. Schwartz
| discipline = [[Decision-making]], [[medical informatics]]
| former_names =
| abbreviation = Med. Decis. Making
| publisher = [[SAGE Publications]]  on behalf of the [[Society for Medical Decision Making]]
| country =
| frequency = Eight issues per year
| history = 1981-present
| openaccess =
| license =
| impact = 3.240
| impact-year = 2014
| website = http://mdm.sagepub.com
| link1 = http://mdm.sagepub.com/content/current
| link1-name = Online access
| link2 = http://mdm.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 0272-989X
| eISSN = 1552-681X
| OCLC = 300296676
| CODEN = MDMADE
}}
'''''Medical Decision Making''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that publishes papers in the fields of [[decision-making]] and [[medical informatics]]. Its [[editor-in-chief]] is Alan J. Schwartz (University of Illinois at Chicago). It was established in 1981 and is currently published by [[SAGE Publications]] on behalf of the [[Society for Medical Decision Making]]. A sister open access journal focusing on applications of medical decision making, ''Medical Decision Making Policy & Practice'', began publishing in 2016.

== Abstracting and indexing ==
''Medical Decision Making'' is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2014 [[impact factor]] is 3.240, ranking it 4th out of 24 journals in the category "Medical Informatics"<ref name="WoS">{{cite book |year=2015 |chapter=Journals Ranked by Impact: Medical Informatics |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition= Sciences |accessdate=2013-07-12 |series=Web of Science |postscript=.}}</ref> and 14th out of 89 journals in the category "Health Care Sciences & Services".<ref name="WoS1">{{cite book |year=2015 |chapter=Journals Ranked by Impact: Health Care Sciences & Services |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition= Sciences |accessdate=2013-07-12 |series=Web of Science |postscript=.}}</ref>

== Editors ==
The following persons have been editors-in-chief of the journal:
* Lee B. Lusted, 1981–1985
* Dennis G. Fryback, 1986–1988
* J. Robert Beck, 1989–1994
* Arthur S. Elstein, 1995–1999
* Frank A. Sonnenberg, 2000–2004
* Mark Helfand, 2005–2012
* Alan J. Schwartz, 2013-present

== References ==
{{reflist}}

== External links ==
* {{Official website|http://mdm.sagepub.com/}}
* [http://www.smdm.org/ Society for Medical Decision Making]

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Bimonthly journals]]
[[Category:Biomedical informatics journals]]
[[Category:Publications established in 1981]]