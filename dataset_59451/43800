<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=U 8 Limousine
 | image=Udet U 8.jpg
 | caption= U 8 with 9-cylinder Siemens-Halske engine
}}{{Infobox Aircraft Type
 | type=three passenger transport aircraft
 | national origin=[[Germany]]
 | manufacturer=[[Udet]]
 | designer=Hans Henry Herrmann
 | first flight=1924
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=5
 |developed from=Udet U 5

}}
|}

The [[parasol wing]], single engine '''Udet U 8''', sometimes referred to as the '''Limousine''', was a three seat commercial passenger transport designed and built in [[Germany]] in 1924. Five were produced and were used by German airlines until about 1928.

==Design and development==

The first Udet passenger transport was the two-passenger U 5, which appeared in 1923.<ref name=Flight1/><ref name=Lailes1/><ref name=histav1/><ref name=Note1 group=Notes/> This was powered by a  {{convert|70|hp|kW|abbr=on|disp=flip}}, seven-cylinder, [[Siemens-Halske Sh 5]] [[radial engine]].<ref name=Flight1/><ref name=Lailes1/> The following year Udet produced the first U 8, which had a {{convert|100|hp|kW|abbr=on|disp=flip}} nine-cylinder, [[Siemens-Halske Sh 6]] radial, making it rather heavier than the U 5 but leaving the design only slightly changed and the dimensions unaltered. The new engine allowed the U 8 to carry three passengers.<ref name=Flight2/><ref name=histav2/>

The [[cantilever]], one-piece parasol wing of the U.8 was trapezoidal in plan, with long, elliptical tips. It had a thick section which thinned outwards and was built around two [[spruce]] [[spar (aeronautics)|box spars]] and [[aircraft fabric covering|fabric covered]]. Its [[ailerons]] tapered in [[chord (aeronautics)|chord]] out to the wing tips; together, they occupied 45% of the span.<ref name=Flight2/><ref name=LaeroN/> The wing was mounted a little above the [[fuselage]] on four short [[strut]]s, two to each spar, an unusual arrangement used earlier on the U 5 and chosen by its designer, Hans Herrmanns, to improve both the [[aerodynamics]] at the wing-fuselage junction and cabin ventilation, a problem in small cabin aircraft of the time.<ref name=Flight1/> Under the wing, part of the cabin roof was open. The wing struts, uncovered on the U 5, were covered by longitudinal panels.<ref name=histav2/> 

At least three types of [[radial engine]]s powered the five U.8s known to have been built.  The first examples had the Siemens-Halske Sh 6 but later both nine-cylinder, {{convert|110|hp|kW|abbr=on|disp=flip}} [[Siemens-Halske Sh 12]]s and a 3-cylinder, {{convert|109|hp|kW|abbr=on|disp=flip}} [[Bristol Lucifer]].<ref name=Flight3/>  No images of the Bristol installation are known but the German radials were cleanly [[aircraft fairing#Types|cowled]] by the standards of their time, before either [[Townend ring]]s or [[NACA cowling]]s had been introduced, and with quite large [[spinner (aeronautics)|spinner]]s but with cylinder heads exposed for cooling.<ref name=Flight2/>

One advantage of the gap between fuselage and wing was that the pilot's open [[cockpit]] could be placed under the wing [[leading edge]] rather than set into it, spoiling its aerodynamics.  Entry into the [[plywood]] covered fuselage was through a port-side door under the wing; the three-seat cabin had pairs of windows on each side and a baggage space behind with its own door.<ref name=Flight2/>

At the rear the fuselage became quite slender, with a high mounted, long span, cantilever and almost rectangular plan [[tailplane]] with high [[aspect ratio (aeronautics)|aspect ratio]] [[elevator (aeronautics)|elevator]]s. The tailplane [[angle of incidence (aeronautics)|incidence]] could be adjusted in-flight. Its quadrant-shaped fin mounted a [[rudder]] which extended down to the keel.<ref name=LaeroN/> The U 8 had fixed, conventional [[landing gear]] of the single axle type which was more refined than that of the U5,<ref name=Lailes1/> with a longer [[oleo strut]] to mid-fuselage and rearward drag struts.  There was a short tailskid.<ref name=LaeroN/>

In about 1925 at least one Udet 8 was fitted with full-span [[leading-edge slats|Lippmann/Handley Page slats]] coupled to full-span ailerons which were lowered together as [[camber (aerodynamics)|camber increasing]] [[flap (aeronautics)|flap]]s when the slats were deployed. It was one one of the earliest German aircraft to have slats.<ref name=Lailes2/> The slatted Udet 8 was recorded as a '''Udet 8a'''<ref name=LaeroP/> or '''Udet 8B'''<ref name=Lailes2/> in contemporary journals, though a modern source states that the U 8a (''D-839'') had a new wing with an area increase of {{convert|7|m2|sqft|abbr=on}} and a Sh-12 engine.<ref name=histav2/> ''D-839'' is recorded as a U 8b in a reconstructed register.<ref name=GY/>

==Operational history==
In 1925 one U 8, ''D-670'', competed in the "Round Germany Flight", one of only two commercial types to do so. Powered during the contest by the Bristol Lucifer engine (it had a nine-cylinder Siemens at another time), it successfully completed all five circuits of the contest, a total of distance of {{convert|5242|km|mi nmi|abbr=on}} but, with a more powerful engine than most in its class (C}, was ranked only ninth. Another Udet entrant, the two seat [[Udet U 10]], won class B.<ref name=Flight3/> 

The five U 8s were initially used by [[Deutsche Aero Lloyd]], but two of them were transferred to [[Nordbayeriche Verkehrsflug]] when Aero Lloyd became [[Deutsche Lufthansa]] in 1926. One of these (''D-670'') crashed soon after. It was transferred to the [[Deutsche Verkehrsfliegerschule |DVS]] and was joined there by ''D-839'' later in the year.<ref name=histav2/><ref name=GY/>

== Specifications (Siemens-Halske Sh 6 engine) ==
{{Aircraft specs
|ref=Flight, May 1925<ref name=Flight2/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=One
|capacity=three passengers
|length m=7.12
|length note=
|span m=12.0
|span note=
|height m=2.67
|height note=<ref name=LAA/>
|wing area sqm=18
|wing area note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=490
|empty weight note=
|gross weight kg=860
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Siemens-Halske Sh 6]]
|eng1 type=9-cylinder [[radial engine|radial]]
|eng1 hp=100
|eng1 note=
|more power=

|prop blade number=2
|prop name=
|prop dia m=
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=170
|max speed note=
|cruise speed kmh=140
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=3000
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=1:14<ref name=Lailes1/>
|climb rate ms=
|climb rate note=
|time to altitude=13.5 min to {{convert|1000|m|ft|abbr=on}}
|sink rate ms=<!-- sailplanes -->
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|more performance=
}}

==Notes==
{{reflist|group=Notes|refs=
<ref name=Note1 group=Notes>The two 1923 references do not give the type number but Flight's image shows the registration was D-302. This is identified in the cited German register as the U 5.</ref>
}}

==References==
{{reflist|2|refs=

<ref name=LAA>{{cite book |title= L'Année Aéronautique|last=Hirschauer 1925-1926|first=L.|last2= Dollfus|first2=Ch. |year=1926 |publisher=Dunod|page=25|location=Paris |url=http://gallica.bnf.fr/ark:/12148/bpt6k6553408d/f38|}}</ref>

<ref name=Lailes1>{{cite journal |last=Serryer |first=J. |date=22 November 1923|title=La Limousine Udet|journal=Les Ailes|issue=127 |pages=2|url=http://gallica.bnf.fr/ark:/12148/bpt6k6556073j/f2 }}</ref>

<ref name=Lailes2>{{cite journal  |date=17 December 1925|title=Les Ailes en Allemagne|journal=Les Ailes|issue=235 |pages=4|url=http://gallica.bnf.fr/ark:/12148/bpt6k6568267d/f4 }}</ref>

<ref name=LaeroP>{{cite journal|date=1–15 June 1925 |title=Figure caption|journal=L'Aérophile|volume=33|issue=11-12 |pages=170|url=http://gallica.bnf.fr/ark:/12148/bpt6k6553816b/f176 }}</ref>

<ref name=LaeroN>{{cite journal|date=August 1924 |title=Le monoplan Udet-8|journal=L'Aéronautique|volume=8|issue=63 |pages=197|url=http://gallica.bnf.fr/ark:/12148/bpt6k65675926/f35}}</ref>

<ref name=Flight1>{{cite journal |date=22 November 1923 |title=The new Udet commercial monoplane|journal= [[Flight International|Flight]]|volume=XV |issue=47 |pages=709|url= https://www.flightglobal.com/pdfarchive/view/1923/1923%20-%200709.html }}</ref>

<ref name=Flight2>{{cite journal |date=28 May 1925 |title=Udet-Flugzeugbau G.M.B.H.|journal= [[Flight International|Flight]]|volume=XVII |issue=22 |pages=324-5|url= https://www.flightglobal.com/pdfarchive/view/1925/1925%20-%200324.html }}</ref>

<ref name=Flight3>{{cite journal |date=18 June 1925 |title=Round-Germany competition |journal=[[Flight International|Flight]]|volume=XVII |issue=25 |pages=371|url= https://www.flightglobal.com/pdfarchive/view/1925/1925%20-%200371.html }}</ref>

<ref name=GY>{{cite web |url=http://www.airhistory.org.uk/gy/reg_D-.html|title=Golden Years of Aviation |author= |date= |work= |publisher= |accessdate=14 January 2017}}</ref>

<ref name=histav1>{{cite web |url=http://www.histaviation.com/Udet_U-5.html|title= Udet U-5  |accessdate=17 January 2017}}</ref>

<ref name=histav2>{{cite web |url=http://www.histaviation.com/Udet_U-8.html|title= Udet U-8  |accessdate=15 January 2017}}</ref>

}}
<!-- ==Further reading== -->
<!-- ==External links== -->

{{Udet aircraft}}

[[Category:Parasol-wing aircraft]]
[[Category:Udet aircraft]]
[[Category:German civil aircraft 1920–1929]]