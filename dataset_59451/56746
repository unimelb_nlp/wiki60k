{{Infobox journal
| editor = Michael T. Klein
| discipline = [[Chemistry]], [[fossil fuel]]s
| abbreviation = Energy Fuels
| publisher = [[American Chemical Society]]
| country = United States
| frequency = Monthly
| history = 1987-present
| impact = 2.835
| impact-year = 2015
| website = http://pubs.acs.org/journal/enfuem
| link1 = http://pubs.acs.org/toc/enfuem/current
| link1-name = Online access
| link2 = http://pubs.acs.org/loi/enfuem
| link2-name = Online archive
| cover =[[Image:ef cover.jpg|200px]]
| CODEN = ENFUEM
| ISSN = 0887-0624
| eISSN = 1520-5029
| OCLC = 13076751
| LCCN = 87658131
}}
'''''Energy & Fuels''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] published by the [[American Chemical Society]]. It was established in 1987. Its publication frequency switched from bimonthly to monthly in 2009. The [[editor-in-chief]] is Michael T. Klein ([[University of Delaware]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Chemical Abstracts Service]]
* [[Scopus]]
* [[EBSCOHost]]
* [[Science Citation Index]]
* [[Current Contents]]/Engineering, Computing & Technology
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 2.790.<ref name=WoS>{{cite book |year=2015 |chapter=Energy & Fuels |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://pubs.acs.org/journal/enfuem}}

{{DEFAULTSORT:Energy and Fuels}}
[[Category:American Chemical Society academic journals]]
[[Category:Chemistry journals]]
[[Category:Energy and fuel journals]]
[[Category:Publications established in 1987]]
[[Category:Monthly journals]]
[[Category:English-language journals]]