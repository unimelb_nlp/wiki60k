{{infobox Protected Area		
| name = Holosiivskyi National Nature Park		
| alt_name = {{ lang-uk | Націона́льний приро́дний парк «Голосі́ївський» }} <br/>(Also: '''Goloseevs'kiy''')		
| iucn_category =II	
| photo = Госоївський парк.JPG	             
| photo_caption = Holosiivsky Park	
| photo_width=300		
| map = Ukraine
| relief = yes		
| map_caption = Location of Reserve		
| location = [[Ukraine]]		
| nearest_city = [[Kiev]]		
| coordinates = {{coord|50|17|50|N|30|33|37|E|format=dms|display=inline,title}}
| area = {{convert | 1879 | ha | sqmi | lk=on | 0 }}	
| website=	                
}}	
The '''Holosiivskyi National Nature Park''' {{ lang-uk | Націона́льний приро́дний парк «Голосі́ївський» }} is a protected remnant of forest surrounded by the urban area of the city of [[Kiev]], [[Ukraine]].  It is located on the Kiev hills, in the Dniester-Dnieper forest-steppe province, North-Dnieper lowland and steppe zone of Left-Bank Dnieper province, in [[Holosiivskyi District]].  Its total area is 4525.52  hectares, of which 1879.43 hectares are in permanent use.  It is managed by the [[Ministry of Ecology and Natural Resources (Ukraine)|Ministry of Ecology and Natural Resources of Ukraine]].<ref name="menru-1">{{cite web|title=National Natural Park "Goloseevsky" |url=http://cons.parus.ua/map/doc/098XP8A74F/Pro-vnesennya-zmin-do-Polozhennya-pro-natsionalnii-prirodnii-park-Golosiyivskii.html |website=MENRU - Order 04.08.2014 N 244 |publisher=Ministry of Ecology and Natural Resources |accessdate=29 June 2016 }}{{dead link|date=April 2017 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> 

A variety of landscapes, significant [[biodiversity]], lakes and wetlands, plus historical, cultural and ethnographic monuments define a high importance of the park’s area for scientific research, monitoring of ecosystems and conservation, as well as use by locals of ecological trails.

==Topography==
The park consists of several spatially disconnected forests located in the southern part of the city of [[Kiev]]  The park area belongs to the forest-steppe zone, with its main area covered by forest - 4232.8 hectares (more than 90% of the territory).  Wetlands make up 66.2 ha, and the remaining 45.9 ha is water.<ref name="parksite">{{cite web|title=Holosyiyiskyy NPP|url=http://pzf.menr.gov.ua/%D0%B3%D0%BE%D0%BB%D0%BE%D1%81%D1%97%D1%97%D0%B2%D1%81%D1%8C%D0%BA%D0%B8%D0%B9-%D0%BD%D0%BF%D0%BF.html|website=Official Website of Nature Park|publisher=HOLOSYIYIVSKYY NPP|accessdate=29 June 2016}}</ref>

==Flora and fauna==
The flora of the park includes 650 species of [[vascular plant]]s, 118 species of [[moss]], and more than 60 species of [[fungi]]. Some of these species have [[conservation status]] of international, national or regional level. Among the vascular plants that grow in the park, 5 species are listed in Annex I of the [[Berne Convention on the Conservation of European Wildlife and Natural Habitats]], 1 species in the European [[Regional Red List]], 24 species in the [[Red Book of Ukraine]], and 29 species are regionally rare.

The Fauna consists of 31 species of terrestrial [[mollusc]]s, 190 species of insects, and 181 species of [[vertebrate]]s (21 species of bony fish, 10 species of [[amphibian]]s, 6 species of [[reptile]]s, 100 species of birds, and 44 [[mammal]] species).
The park is represented by a number of vertebrate and [[invertebrate]] animals that are protected internationally, nationally, and regionally:
* 9 species are in the [[IUCN Red List]] (4 vertebrates and 5 invertebrates);
* 93 species are in Annex II of the Berne Convention (89 and 4, respectively);
* 11 species are listed in the [[CITES|Washington Convention]];
* 46 species are in the [[Convention on the Conservation of Migratory Species of Wild Animals|Bonn Convention]] (EUROBATS agreement - 10 species, AEWA - 11 species);
* 13 species are within the European Red List (4 and 9, respectively);
* 2 species belong to the European Red List of Butterflies;
* 35 species are in the Red Book of Ukraine (20 vertebrates and 15 invertebrates);
* 11 vertebrate species are in the list of species that are protected within Kiev.<ref name='parksite'/>

Among National Red Book species of vertebrates in the park, there are: [[European green lizard]], [[Coronella austriaca|smooth snake]], [[short-toed snake eagle]], [[greater spotted eagle]], [[stock dove]], [[white-backed woodpecker]], [[Miller's water shrew]], [[serotine bat]], [[Nathusius' pipistrelle]], [[Kuhl's pipistrelle]], [[common pipistrelle]], [[soprano pipistrelle]], [[common noctule]], [[Leisler's bat]], [[brown long-eared bat]], [[rearmouse]], [[Daubenton's bat]], [[stoat]], [[European polecat]] and [[otter]].
Among National Red Book species of invertebrates, there are: [[forest caterpillar hunter]], [[stag beetle]], [[musk beetle]], [[hermit beetle]], ''[[Click beetle|Neopristilophus depressus]]'' (lat.), [[swallowtail butterfly|swallowtail]], [[southern festoon]], [[clouded Apollo]], [[Hamearis lucina|Duke of Burgundy]], [[poplar admiral]], [[blue underwing]], ''[[Janus femoratus]]''(lat.), ''[[Megarhyssa superba]]''(lat.), [[carpenter bee]] and [[mammoth wasp]].

The park has  numerous age-old oaks (oak of Peter Mohyla, oaks of glory, etc.), primroses and colorful cascades of the ponds named Nutwoods, Kitaevsky and Didorivsky. Source of the holy bathing house located in Didorivsky watercourse near the "Mit'kin pound" in Holosiivskyi forest., a bath  called by religious people in the area healing or miraculous. Another bath  – Kitaevsky is near watercourse Kitaevsky.

==History==
A picturesque and cozy place that was called Goloseev or Holosievo was mentioned from the beginning of the 16th century. The origin of the name usually is connect with  planting trees on the treeless territory in the early 17th century by the order of the founder Goloseevskiy skit Peter Mohyla. However, there are more ancient references to forested  land and the farm Hulasiyivskyy. Thus, it is likely that local name is derived from the phrase “siyaty hul”("to sow hum , but not “golosiyaty” (“to plant treeless land”.)

== Attractions ==
[[File:Holosiiv National Nature Park.jpg|thumb|Park logo]]
The main historical and cultural attractions are located in the national park and close to it are:
- Holy Trinity Monastery (Kitaevskaya desert with ancient caves), 18th and 20th centuries;
- Holy Protection Monastery (Deserts Holosiivska), founded in the 17th century, rebuilt in the late 20th and early 21st centuries;
- Main Astronomical Observatory of [[National Academy of Sciences of Ukraine]], where part of the buildings were built in 1841-1845, respectively;
- Complex of buildings of Agricultural Academy (now the National University of Life and Environmental Sciences), built in 1925-1931.
- A fragment of the Kiev fortified area (KyUR), huge fortification with length of 85&nbsp;km, which was built during 1929-1935. Fortified area covered Kyiv semicircles leaning its flanks on the Dnipro River. In the Southpart of the bunkers was inscribed in the remains of the ancient "Serpent shaft." The depth of the defensive zone isup to 5&nbsp;km. Altogether 217 Pillboxes were built; 7 of them are situated in national parks.

==Gallery==
<center>
<gallery>
Image:Голосіївський ліс. Весна.JPG|
Image:Голосіївський ліс 7.jpg|
Image:Голосіївський ліс, один з Китаєвських ставків.jpg|
Image:Національний природний парк «Голосіївський», грабовий ліс восени.JPG|
Image:Голосіївський ліс!.JPG|
Image:Голосіївський ліс15.JPG|
Image:Голосіївський ліс20.JPG|
Image:Голосіївський ліс12.JPG|
</gallery>
</center>

== References ==
{{reflist}}

==External links==
* [http://www.openstreetmap.org/relation/1652168#map=13/50.3742/30.4965&layers=C Map of Holoseevski at OpenStreetmap.org]
* Litopys Pryrody (Літопис природи), 2011, National Nature Park "Holosiivskyi", official annual report submitted to the Ministry of Ecology and Natural Resources of Ukraine. 
* http://www.recreation.ecotour.com.ua/eco-route/189-2012-03-16-15-25-38
* http://www.reserves.in.ua/npp/golosijvskyj
* http://ukrainaincognita.com/shaparnya/natsionalnyi-pryrodnyi-park-golosiivskyi
{{commonscat}}
{{National Nature Reserves of Ukraine |state=expanded}}

[[Category:2007 establishments in Ukraine]]
[[Category:Holosiivskyi District]]
[[Category:National parks of Ukraine]]
[[Category:Parks in Kiev]]
[[Category:Protected areas established in 2007]]