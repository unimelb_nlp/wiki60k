{{Infobox journal
| title         = Language in Society
| cover         = [[File:Language in Society.jpg]]
| editor        = [[Jenny Cheshire]]
| discipline    = [[Sociolinguistics]]
| peer-reviewed = 
| language      = English
| former_names  = 
| abbreviation  = Lang. Soc.
| publisher     = [[Cambridge University Press]]
| country       = United Kingdom
| frequency     = 5 issues a year
| history       = 1972-present
| openaccess    = 
| license       = 
| impact        = 1.525
| impact-year   = 2015
| website       = http://journals.cambridge.org/action/displayJournal?jid=LSY
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 299393484
| LCCN          = 72623747
| CODEN         = 
| ISSN          = 0047-4045
| eISSN         = 1469-8013
| boxwidth      = 
}}

'''''Language in Society''''' is a [[Peer review|peer-reviewed]] [[academic journal]] of [[sociolinguistics]]. It was established in 1972 and is published five times a year by [[Cambridge University Press]]. The current [[editor in chief]] is [[Jenny Cheshire]] ([[Queen Mary University of London]]). It has a circulation of 1900.<ref name="adv">{{cite web | url=http://journals.cambridge.org/action/displayMoreInfo?jid=LSY&type=ai | title=Advertising Information | accessdate=2010-12-30 | date=2010 | publisher=Cambridge University Press}}</ref>

According to the [[Journal Citation Reports]], the journal's 2015 [[impact factor]] is 1.525, ranking it 22nd out of 179 journals in the category "Linguistics," and 33rd out of 142 journals in the category "Sociology."<ref name="impact">{{cite web | url=http://journals.cambridge.org/action/displayMoreInfo?jid=LSY&type=if | title=Impact Factor | accessdate=2016-08-17 | publisher=Cambridge University Press}}</ref>

==Aims and scope==
The journal treats language and communication in the context of social life. Apart from sociolinguistics, its scope encompasses also related fields, such as [[linguistic anthropology]]. Taking an international perspective, the journal aims at encouraging discussion among researchers and disciplines.<ref name="cambrd">{{cite web | url=http://journals.cambridge.org/action/displayJournal?jid=LSY | title=Cambridge Journals Online - Language in Society | accessdate=2010-12-30 | date=2010| publisher=Cambridge University Press}}</ref>

==References==
{{Reflist}}


[[Category:English-language journals]]
[[Category:Linguistics journals]]
[[Category:Cambridge University Press academic journals]]
[[Category:Publications established in 1972]]