{{notability|date=February 2017}}
{{refimprove|date=February 2017}}
{{Orphan|date=November 2016}}
{{Infobox journal
| title = Public History Weekly. The International BlogJournal
| cover =
| editor = Marko Demantowsky, Peter Gautschi, Thomas Hellmuth, Krzysztof Ruchniewicz 
| discipline = [[Public history]]
| language = English, German, and many others
| former_names  = 
| abbreviation  = Public Hist. Weekly
| publisher = [[Walter de Gruyter]]
| country =
| frequency = Weekly
| history = 2013-present
| openaccess = Yes
| license = 
| impact = 
| impact-year = 
| website = https://public-history-weekly.degruyter.com
| link1 = 
| link1-name = 
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 
| LCCN = 
| CODEN = 
| eISSN = 2197-6376
| ISSN = 
}}
'''Public History Weekly. The International BlogJournal''' is a [[peer-reviewed]] [[academic journal]] on all aspects of [[public history]] addressing a wider audience to popularize academic research and debates.<ref name="asu">[http://dhdhi.hypotheses.org/2358 Interview by Mareike Koenig (DHI Paris) with Marko Demantowsky]</ref><ref name="Zerwas">Zerwas M. (2014), ''Public History in der Schule. Unterricht in der Allgegenwart von Geschichte.'', in Geschichte lernen 2014, 159/160, pp. 92-93.</ref>

==Overview==
The journal was established in 2013<ref name="Announcement">[https://www.degruyter.com/page/746 Announcement of the Publisher]</ref> and is published by [[Walter de Gruyter]] on behalf of four universities from Austria ([[University of Vienna]]), Poland ([[University of Wrocław]]), and Switzerland (Universities of Education in Basel/Brugg and in Lucerne) as well as of the International Federation for Public History.<ref name="IFPH">[https://ifph.hypotheses.org Homepage of IFPH-FIPH]</ref><ref name="PHW">[https://public-history-weekly.degruyter.com Homepage of PHW]</ref> The journal publishes in English and in German and additionally in the mother tongue of each author. The journal has the explicit aim to link the discussions of [[Public history|Public historians]] and [[National Council for History Education|history educationalists]].<ref name="Bridging">[http://www.thenhier.ca/en/content/public-history-weekly THENHIER (Canada) on Public History Weekly]</ref> That aim of bridging an academic gap has been criticized recently.<ref name="Critique">[https://docupedia.de/zg/Zuendorf_public_history_v2_de_2016 Zuendorf, I. (2016) on Public History Weekly]</ref>
== References ==
{{reflist}}

==External links==
*{{Official website|https://public-history-weekly.degruyter.com}}


[[Category:Weekly journals]]
[[Category:History journals]]
[[Category:English-language journals]]
[[Category:Publications established in 2013]]
[[Category:Walter de Gruyter academic journals]]