<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Tucano Replica
 | image=Tucano-Replica.jpg
 | caption=Tucano Replica on display
}}{{Infobox Aircraft Type
 | type=[[Light-sport aircraft]]
 | national origin=[[Italy]]
 | manufacturer=[[Flying Legend]]
 | designer=
 | first flight=
 | introduced=2011
 | retired=
 | status=In production
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= 2011-present<!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= [[Euro|€]]130,000 (assembled, 2011)
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Flying Legend Tucano Replica''' ({{lang-en|[[Toucan]]}}) is an [[Italy|Italian]] [[light-sport aircraft]], designed and produced by [[Flying Legend]] of [[Caltagirone]] and introduced at the [[AERO Friedrichshafen]] show in 2011. The aircraft is supplied as a kit for [[Homebuilt aircraft|amateur construction]] or as a complete ready-to-fly-aircraft.<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 54. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref><ref name="WDLA15">Tacke, Willi; Marino Boric; et al: ''World Directory of Light Aviation 2015-16'', page 108. Flying Pages Europe SARL, 2015. {{ISSN|1368-485X}}</ref>

Flying Legend is a collaborative project between MGA and Barum.<ref name="WDLA11"/>

==Design and development==
The Tucano Replica is scale replica of the 1980s vintage [[Embraer EMB 312 Tucano]] [[turboprop]] [[Trainer (aircraft)|trainer]] and features a cantilever [[low-wing]], a two-seats-in-[[tandem]] enclosed cockpit under a [[bubble canopy]], retractable [[tricycle landing gear]] and a single engine in [[tractor configuration]]. A fixed gear model has been developed for the US [[light sport aircraft]] market.<ref name="WDLA11"/><ref name="WDLA15"/>

The aircraft is made from [[2024-T3 aluminum]] and [[6061-T6 aluminum]] sheet. Its {{convert|8.41|m|ft|1|abbr=on}} span wing has an area of {{convert|10.0|m2|sqft|abbr=on}} and is equipped with [[Flap (aircraft)|flaps]]. Standard engines available are the {{convert|100|hp|kW|0|abbr=on}} [[Rotax 912ULS]] [[four-stroke]] powerplant, with the {{convert|115|hp|kW|0|abbr=on}} [[Rotax 914]] and {{convert|125|hp|kW|0|abbr=on}} [[D-Motor LF39]] optional.<ref name="WDLA11"/><ref name="WDLA15"/><ref name="Kit">{{cite web|url = http://www.flyinglegend.it/2012/01/18/kit-tucano/?lang=en/|title = Tucano Replica Kit|accessdate = 3 July 2012|last = Flying Legend|year = 2011}}</ref>
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Aircraft on display== -->

==Specifications (Tucano Replica) ==
{{Aircraft specs
|ref=Bayerl and Flying Legend<ref name="WDLA11"/><ref name="Specs">{{cite web|url = http://www.flyinglegend.it/2012/01/17/scheda-tecnica-tucano/|title = Tucano Replica Specifications|accessdate = 3 July 2012|last = Flying Legend|year = 2011}}</ref>
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=one passenger
|length m=7.10
|length ft=
|length in=
|length note=
|span m=8.41
|span ft=
|span in=
|span note=
|height m=2.43
|height ft=
|height in=
|height note=
|wing area sqm=10.0
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=371
|empty weight lb=
|empty weight note=
|gross weight kg=595
|gross weight lb=
|gross weight note=
|fuel capacity={{convert|84|l}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Rotax 912ULS]]
|eng1 type=four cylinder, liquid and air-cooled, [[four stroke]] [[aircraft engine]]
|eng1 kw=75<!-- prop engines -->
|eng1 hp=<!-- prop engines -->

|prop blade number=3
|prop name= composite
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=240
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=75
|stall speed mph=
|stall speed kts=
|stall speed note=flaps up
|never exceed speed kmh=295
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=900
|range miles=
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=+4.4/-2.2
|roll rate=
|glide ratio=
|climb rate ms=5.5
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=59.5
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==References==
{{reflist}}

==External links==
*[http://www.flyinglegend.it/2011/12/15/tucano/?lang=en Official website]
{{Flying Legend aircraft}}

[[Category:Homebuilt aircraft]]
[[Category:Light-sport aircraft]]
[[Category:Single-engined tractor aircraft]]
[[Category:Low-wing aircraft]]
[[Category:Flying Legend aircraft|Tucano]]
[[Category:Replica aircraft]]