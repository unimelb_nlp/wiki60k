{{Infobox journal
| title = Old English Newsletter
| cover = 
| editor = Roy Liuzza
| discipline = [[Old English]] [[philology]], [[Anglo-Saxon England|Anglo-Saxon history and culture]]
| abbreviation = Old Engl. Newsl.
| publisher = [[University of Tennessee]]
| country =
| frequency =
| history = 1967–present 
| openaccess = 
| license = 
| impact = 
| impact-year = 
| website = http://www.oenewsletter.org/OEN/index.php 
| link1 = http://www.oenewsletter.org/OEN/pubs.php
| link1-name = Recent and forthcoming publications 
| link2 = http://www.oenewsletter.org/OEN/archive.php
| link2-name = Online archive
| JSTOR = 
| OCLC = 02428532
| LCCN = sf79010230
| CODEN = 
| ISSN = 0030-1973
| eISSN = 
}}
The '''''Old English Newsletter''''' is a [[Peer review|peer-reviewed]] [[academic journal]] established in 1967. It covers [[Anglo-Saxon England|Anglo-Saxon]] studies and is published by the [[University of Tennessee]]'s Department of English for the Old English Division of the [[Modern Language Association|Modern Language Association of America]]. Its current editor is [[Roy Liuzza]], of the [[University of Tennessee]].

The journal publishes an annual ''Bibliography'' and ''Year's Work'', which are widely relied upon.<ref>{{cite book |author=Baker, Peter S. |title=Introduction to Old English |publisher=[[Wiley-Blackwell|Blackwell Publishing]] |location=Malden, MA |year=2003 |page=325 |isbn=0-631-23453-5 |oclc=50919770 |doi= |accessdate=}}</ref> Many issues include [[obituary|obituaries]] of relevant scholars.

The online version contains an archive of several years of the journal's publications.<ref>{{cite web |url=http://www.oenewsletter.org/OEN/index.php/welcome/welcome |title=Old English Newsletter Online |publisher=Old English Division, Modern Language Association of America |work=Homepage |accessdate=2011-07-21}}</ref>

Originally, the ''Old English Newsletter'' was published at [[Binghamton, New York]], by the [[State University of New York]]'s Center for Medieval and Renaissance Studies.<ref>{{cite book |author=Marcuse, Michael J. |title=A Reference Guide for English Studies |publisher=[[University of California Press]] |location=Berkeley |year=1990 |page=312 |isbn=0-520-05161-0 |oclc=14241434 |doi= |accessdate=}}</ref> It is currently (2011) published at the Department of English, University of Tennessee.

== See also ==
*''[[Anglo-Saxon England (journal)|Anglo-Saxon England]]''

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.oenewsletter.org/OEN/index.php}}

[[Category:English-language journals]]
[[Category:European history journals]]
[[Category:Publications established in 1967]]
[[Category:University of Tennessee]]