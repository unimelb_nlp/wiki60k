{{Infobox person
|name = Max Nivelli
|image = Max Lewin-Nivelli in 1925.jpg
|image_size = 
|caption = Max Nivelli in 1925
|birth_name = Menachem Mendel Lewin
|birth_date = {{Birth date|1878|1|1}}
|birth_place = [[Kuźnica, Podlaskie Voivodeship|Kuźnica]], [[Russian Empire]]
|death_date = {{Death date and age|1926|2|27|1878|1|1}}
|death_place = [[Berlin]], [[Germany]]
|occupation = Film producer
|years_active = 1918-1926
}}

'''Max Nivelli''' (January 1, 1878 - February 27, 1926) was a film producer in Berlin during the [[Weimar Republic]] era. He was among the first to examine the issues of [[Antisemitism|anti-Semitism]] and [[prejudice]] in his films. Nivelli died at an early age (48) and worked in the film industry for less than 10 years, yet he produced 19 films, most of them full-length feature films. As most films of that era, his films were [[Silent film|silent]], [[Black and white|black-and-white]] and shot on [[celluloid]]. Only segments of three of his films exist today.

== Early life ==
Max Nivelli was born as Menachem (Mendel) Lewin in [[Kuźnica, Podlaskie Voivodeship|Kuźnica]],<ref name=":0">Berlin State Archive - "Landesarchive Berlin" - Rep. 805, Nr. 517 - Marriage Certificate of Mendel Lewin & Helena Kaufmann</ref> a town on the eastern border of [[Poland]], then part of the [[Russian Empire]]. His parents, Shmuel and Tsippa Lewin, were candy manufacturers.<ref name=":0" /> As a young man he emigrated to Berlin and between the years 1903-1911 he became the owner and partner in several companies which produced fruit preserves, candy and chocolates.<ref>Berlin Address Book - ''"Berliner Adreßbuch"'', 1904-1911
</ref>

In 1904 he married Helene Kaufmann from Rozdzień,<ref name=":0" /> today a suburb of [[Katowice]], Poland. They had two daughters - Dorothea and Regina.<ref>General Register Office, England - Marriage Certificate of Regina Lewin & Friedrich Wolfgang Schwarz, District of St. Martin, County of London - Volume 1a, page nr. 982</ref>

In 1912 Mendel Lewin assumed the name Max Nivelli ("Nivelli" being almost an anagram of the name "Lewin").<ref>Berlin Address Book - ''"Berliner Adreßbuch 1915"'', Part I, p. 1828</ref> He studied music and singing, appeared in opera productions <ref>German Stage Yearbook - ''"Deutsches Bühnen Jahrbuch", 1914 (Vol.25, p.851); 1915 (Vol.26, p.732); 1916 (Vol.27, p.754); 1917 (Vol.28, p.753); 1918 (Vol.29, p.759)''</ref> and also taught solo-singing (in German and Russian) in a music school in Berlin.<ref>Dr. Richard Stern, ''"Das Ochs-Eichelberg-Konservatorium - Was muss der Musikstudierende von Berlin wissen"'', Berlin, 1913–1914.</ref>

== Film production ==
In 1918 Nivelli established his first film production company - "Nivelli-Film Fabrikation", with partner Erwin Kampf.<ref>Berlin Address Book - ''"Berliner Adreßbuch 1919"'', Part I, p. 1983
</ref> That same year the company produced four full-length feature films. Nivelli himself wrote the script for his first film "Pathways of Life" and even played and sang the part of the opera singer.<ref>“Film” - Nr. 19, 1918</ref>

The following year he ended that partnership and formed "Nivo-Film & Co.", a new production company.<ref>Wid's Year Book 1921, p.370-371</ref> He then teamed up with Austrian director [[Joseph Delmont]]<nowiki/> to produce the films "Ritual Murder" and "Humanity Unleashed"'''.''' These films were of social and political nature and became the most successful and well known films of his career.

Nivelli tended to work with the same director on more than one project. Apart from his work with Delmont, he also had a productive collaboration with [[Carl Boese]] with whom he made four films, among them was “[[Nocturne of Love (1919 film)|Nocturne of Love]]” which was based on the life of the Polish composer [[Frédéric Chopin]]. Nivelli also produced two films under the direction of [[Arthur Ullman]] and went on to produce the "Albani Series" - three romantic films in a row, under the direction of [[Guido Schamberg Parisch]] and starring the Italian actress [[Marcella Albani]].<ref>"Reichsfilmblatt" - Nr. 20, 1923.</ref>

== Later years ==
During the years of the economic crisis and [[Hyperinflation in the Weimar Republic|hyperinflation]] in Germany (1921-1924), Nivelli lost most of his fortune. He turned to other occupations related to the film industry, which included the import, export, distribution and rental of films, with partner Arthur Gregor.<ref>"Lichtbild Bühne" (LBB) film magazine, nr. 16, April 21, 1923</ref> He also managed the cinema house "Lichtspiel Palmenhaus Kino" in Berlin<ref>{{Cite web|url=http://www.allekinos.com/BERLINFilmtheaterBerlin.htm|title=Filmtheater Berlin|last=|first=|date=|website=Berlin - Charlottenburg, Kurfürstendamm 193-194|publisher=|access-date=}}</ref> and served on the board of directors of "Paw Film", a Polish production and distribution company located in [[Warsaw]].<ref>{{Cite book|title=Industry and Commerce Directory of Poland - "Przewodnik Przemysłu i Handlu Polskiego"|last=|first=|publisher=|year=1926|isbn=|location=|pages=|quote=|via=}}</ref> Towards the end of that period, in 1924, Nivelli gradually resumed his role as film producer by making four short documentaries, depicting primarily state memorial ceremonies and celebrations.<ref>German Federal Film Archive - "Bundesarchiv-Filmachiv", Berlin - censorship card B.8810.</ref><ref>DIF - Deutsches Filminstitut, Frankfurt am Main, censorship decisions 11-08-1924 and 4-09-1924</ref>

In June 1925, he established a new production company - "Nivelli Film Max Nivelli & Co.", with partner Dr. Sander Kaisermann.<ref>{{Cite book|title=Kinematograph Year Book - Annual Index|last=|first=|publisher=Kinematograph Publications Ltd.|year=1927|isbn=|location=|pages=}}</ref><ref>Berlin Chamber of Commerce and Industry - "Industrie und Handelskammer Zu Berlin" – Certificate: I-Nr. St. 17510/25 – Company nr. 69145.</ref> He immediately embarked on his next project, which was another social awareness film, titled "Unity, Justice and Freedom". It was his third project with the director Joseph Delmont. Just days before the filming ended,<ref>{{Cite news|url=|title=Der Film|last=|first=|date=March 14, 1926|work=|access-date=|via=}}</ref> Max Nivelli died suddenly of a heart attack.<ref>{{Cite news|url=|title=Film Kurier|last=|first=|date=March 2, 1926|work=|access-date=|via=}}</ref><ref>{{Cite news|url=https://www.newspapers.com/newspage/8790128/|title=The Bridgeport Telegram from Bridgeport|last=|first=|date=March 2, 1926|work=|access-date=|via=}}</ref>

In the professional press he was described as brilliant, creative and dynamic and as a popular personality among the filmmakers in Berlin.<ref>{{Cite news|url=|title=Der Montag|last=|first=|date=March 1, 1926|work=|access-date=|via=}}</ref><ref>{{Cite news|url=|title=The Film - No. 10|last=|first=|date=March 7, 1926|work=|access-date=|via=}}</ref>

== Prominent Films ==

=== "Ritual Murder" (Der Ritualmord) / "The Outcasts" (Die Geächteten) ===
This 1919 film was made with the intention of educating the public on the dangers of [[Antisemitism|anti-Semitism]] in general and [[blood libel]] in particular and was marketed as an "[[Age of Enlightenment|enlightenment film]]" (Aufklärungsfilm). Following the end of [[World War I]], Jewish immigration into Western Europe increased and a surge in anti-Semitism and [[xenophobia]] was felt all over Germany. The term "[[Jewish question]]" (die Judenfrage) became a popular topic in German society and the issue of intimate relations between Germans and Jews, which until then was considered a [[taboo]], was also raised.<ref name=":1">{{Cite book|title=The Many Faces of Weimar Cinema (Edited by Christian Rogowski) - "Romeo with Sidelocks: Jewish-Gentile Romance...and Other Early Weimar Assimilation Films"|last=Walk|first=Cynthia|last3=|publisher=Camden House|year=2010|isbn=1-57113-532-4|location=Rochester New York|pages=84–101}}</ref>

The film portrays a violent attack against Jews ("[[Pogrom]]") in a village in [[Tsarist Russia]], after rumors about Jews performing ritual murder were spread. A young Russian student comes to the rescue of the leader of the community and prevents the mob from stoning him to death. As he gets to know the family, the student also falls in love with the leader's daughter. Following a dramatic confrontation, a disaster is prevented and a conspiracy to accuse the Jews of blood libel is exposed.<ref name=":1" />

The film's premiere was a glittering event attended by film critics from all major newspapers, as well as celebrities from the literary and artistic scene in Berlin.<ref>{{Cite news|url=|title=Kinematograph - Issue Nr. 665|last=|first=|date=1919|work=|access-date=|via=}}</ref> The screening was received with loud applause.<ref>{{Cite news|url=|title=B-Z am Mittag|last=|first=|date=1919|work=|access-date=|via=}}</ref> Newspaper reviews described the film as "a masterpiece",<ref>{{Cite news|url=|title=Berliner Börsenzeitung, Nationalzeitung|last=|first=|date=1919|work=|access-date=|via=}}</ref> "one of the best films to be produced so far"<ref>{{Cite news|url=|title="Neue Berliner 12-Uhr Zeitung"; "Der Film"; "Erste Internationale Filmzeitung".|last=|first=|date=1919|work=|access-date=|via=}}</ref> and as "a film where the hero is not a specific person but a whole nation".<ref>{{Cite news|url=|title=Berliner Mittagszeitung|last=|first=|date=1919|work=|access-date=|via=}}</ref> Max Nivelli was portrayed as the driving force of this project, by which "he succeeded in spreading his vision for enlightenment and the need to fight prejudice",<ref>{{Cite news|url=|title=Neue Berliner 12-Uhr Zeitung|last=|first=|date=1919|work=|access-date=|via=}}</ref> and in that "he can be considered as one of the monumental film producers of his time".<ref>{{Cite news|url=|title=Filmkunst|last=|first=|date=1919|work=|access-date=|via=}}</ref> Within 6 weeks of the premiere, which at the time was considered a record, the film was sold worldwide and orders were already pouring in for his next planned project with Delmont - the film "Humanity Unleashed".<ref>{{Cite news|url=|title=Kinematograph: Nr 679/80|last=|first=|date=1919|work=|access-date=|via=}}</ref>

=== “Humanity Unleashed” (Die Entfesselte Menschheit) ===
The film is an adaptation of a novel by the same name, written by [[Max Glass]] and published in 1919. In his novel, Glass described a dark world consumed by disease and war. The film makers decided to take the story to a more contemporary context and produced what was to become the first fictional account of the events of January 1919 in Berlin, the so-called “[[Spartacist Uprising]]”. This film is also considered one of the anti-[[Bolshevik]] films of that era.<ref name=":2">{{Cite book|title=The Many Faces of Weimar Cinema (Edited by Christian Rogowski) - "Humanity Unleashed: Anti-Bolshevism as Popular Culture in Early Weimar Cinema"|last=Stiasny|first=Philipp|publisher=Camden House|year=2010|isbn=1-57113-532-4|location=Rochester, New York|pages=48–66}}</ref>

In the film, a group of workers starts a violent rebellion in an attempt to destroy the existing order, actions which almost lead to civil war. The film reflected the growing fear among the German public of political [[radicalization]]. This fear was not unfounded – while the film was still in production, there was another [[Coup d'état|coup]] attempt (“[[Kapp Putsch]]”), this time by [[Nationalism|nationalist]] and [[Monarchism|monarchist]] factions. Reports in the press about the filming drew the attention of the government, sparking its concern about the effect the film might have on Germany's image abroad. The Foreign Office summoned Max Nivelli and asked him to allow their representative to view the film before its release. In June 1920 the film was approved by the censor but due to the sensitivity of the issue, "Nivo-Film" decided to wait.<ref name=":2" />

The premiere was held six months later and was attended by public figures and members of the government. Most film critics declared the film a success. The film was described as “an important historic document”,<ref>{{Cite news|url=|title=Börsen Zeitung|last=|first=|date=November 21, 1920|work=|access-date=|via=}}</ref> “one of the best films in recent years”,<ref>{{Cite news|url=|title=Acht-Uhr Abendblattl|last=|first=|date=November 20, 1920|work=|access-date=|via=}}</ref> “captivating and realistic”.<ref>{{Cite news|url=|title=Acht-Uhr Abendblattl|last=|first=|date=November 21, 1920|work=|access-date=|via=}}</ref> Some even praised the courage demonstrated by the makers of the film, who dared to examine such a sensitive issue while memories of recent events were still fresh in the public’s mind.<ref>{{Cite news|url=|title=Vossische Zeitung|last=|first=|date=November 23, 1920|work=|access-date=|via=}}</ref> On the other hand, newspapers which represented socialist views, claimed that the workers were negatively portrayed and that the film’s goal was to disseminate fear among the public.<ref>{{Cite news|url=|title=Vorwärts - Morgenausgabe|last=|first=|date=November 21, 1920|work=|access-date=|via=}}</ref> From an artistic viewpoint, it was considered a groundbreaking film – 17,000 people were involved in its production <ref>{{Cite news|url=|title=Der Tag, Abend-Ausgabe|last=|first=|date=November 20, 1920|work=|access-date=|via=}}</ref> and it was said that “clear thematic direction, strong construction and emotional imagery give this film character and pace”.<ref>{{Cite news|url=|title=B-Z Am Mittag|last=|first=|date=November 22, 1920|work=|access-date=|via=}}</ref>

== Filmograpy ==
{| class="wikitable"
|-
! Year !! Title (Ger.) !! Title (Eng.) !! Director !! Category
|-
| 1918 || Lebensbahnen – Ein Sängerleben || Pathways of Life - Life of a Singer || Ernst Sachs || Feature
|-
| 1918 || Der Glückssucher || The Luck Seeker || Arthur Ullmann || Feature
|-
| 1918 || [http://www.imdb.com/title/tt0361649/?ref_=fn_al_tt_1 Die Gestohlene Seele] || The Stolen Soul || Carl Boese || Feature
|-
| 1918 || [http://www.imdb.com/title/tt0361629/?ref_=fn_al_tt_1 Der Fluch des Nuri] 
Alternate Name: Das Lied des Nisami 
|| The Curse of Nuri 
The Song of Nisami 
|| Carl Boese || Feature
|-
|1918
|[http://www.imdb.com/title/tt5532112/?ref_=fn_al_tt_1 Das Alte Bild]
|The Old Image
|Arthur Ullmann
|Feature
|-
| 1919 || [http://www.imdb.com/title/tt0129962/?ref_=fn_al_tt_1 Der Ritualmord] 
Alternate Name: Die Geächteten 
|| Ritual Murder 
The Outcasts 
|| Joseph Delmont || Feature
|-
| 1919 || [http://www.imdb.com/title/tt0009433/?ref_=fn_al_tt_1 Nocturno der Liebe]  
Alternate Name: Chopin  
|| Nocturne of Love 
Chopin 
|| Carl Boese || Feature
|-
| 1919 || [http://www.imdb.com/title/tt0472426/?ref_=fn_al_tt_1 Die Tochter des Bajazzo]  || The Daughter of Bajazzo || Arthur Ullmann || Feature
|-
| 1920 || [http://www.imdb.com/title/tt0338005/?ref_=fn_al_tt_1 Die Entfesselte Menschheit]  || Humanity Unleashed || Joseph Delmont || Feature
|-
| 1922 || Dolores || Dolores || Carl Boese || Feature
|-
| 1922 || [http://www.imdb.com/title/tt0130705/?ref_=fn_al_tt_1 Frauenschicksal] || Women’s Fate || Guido Schamberg || Feature
|-
| 1923 || [http://www.imdb.com/title/tt0130278/?ref_=fn_al_tt_1 Das Spiel der Liebe]  || The Play of Love || Guido Schamberg || Feature
|-
| 1923 || Liebe und Ehe  || Love and Marriage || Unknown || Feature
|-
| 1923 || [http://www.imdb.com/title/tt0129155/?ref_=fn_al_tt_1 Im Rausche der Leidenschaft]  || In the Heat of Passion || Guido Schamberg || Feature
|-
| 1924 || Die Samland-Bäder
(Die ostpreußische Bernsteinküste)  
|| The Samland Baths 
(The East-Prussian Amber coast) 
|| Unknown || Short Documentary
|-
| 1924 || Die Tannenbergfeier in Königsberg  || The Tannenberg Ceremony in Königsberg || Unknown || Short Documentary
|-
| 1924 || Die Verfassungsfeier in Berlin  || The Constitution Ceremony in Berlin || Unknown || Short Documentary
|-
| 1924 || Die Ehrengedenkfeier für die Toten Helden || Memorial Ceremony for the Dead Heroes || Unknown || Short Documentary
|-
| 1926 || Einigkeit und Recht und Freiheit  || Unity, Justice and Freedom || Joseph Delmont || Feature
|}

== References ==
{{reflist}}

== External links ==
* [http://www.imdb.com/name/nm2574916/?ref_=fn_al_nm_1 Max Nivelli] at the [[IMDb|Internet Movie Database]]
* [http://www.filmportal.de/person/max-nivelli_c7e6caab4f424e06bdbb5b86c1082b74 Max Nivelli] at the [[Filmportal.de]] (German)
* [http://www.earlycinema.uni-koeln.de/films/index/Filter.FilterByCompany:1/Filter.Company.0:782/Filter.FilterByCompanyType:1/Filter.CompanyType.0:1/keep_frame2:1 Max Nivelli] at [http://www.earlycinema.uni-koeln.de/ The German Early Cinema Database]
* [http://digital.cjh.org/R/8PGGBJUQLVLFGN4CQ3C2Y755GJV4Q8E6RA9KKAIQNYSM3CKYFX-00545?func=dbin-jump-full&object_id=1711753&local_base=GEN01&pds_handle=GUEST Max Nivelli] at the [[Center for Jewish History]]

{{DEFAULTSORT:Nivelli, Max}}
[[Category:1878 births]]
[[Category:1926 deaths]]
[[Category:German film producers]]
[[Category:Cinema of Germany]]
[[Category:German male silent film actors]]
[[Category:20th-century German male actors]]
[[Category:Silent film producers]]
[[Category:German documentary filmmakers]]
[[Category:Jewish film people]]