The '''International Auditing and Assurance Standards Board''' ('''IAASB''') is an independent [[standards body]] which issues standards, like the [[International Standards on Auditing]], [[quality control]] guidelines and other services, to support the international [[financial audit|auditing]] of [[financial statement]]s. It is a body supported by the [[International Federation of Accountants]] (IFAC).<ref>{{cite web|url=http://www.ifac.org/auditing-assurance |title=IAASB |publisher=[[International Federation of Accountants]] |accessdate=2013-07-26}}</ref><ref>{{cite web|url=http://www.iasplus.com/en/resources/iaasb |title=International Auditing and Assurance Standards Board (IAASB) |publisher=[[Deloitte]] |accessdate=2013-07-26}}</ref><ref>{{cite web|url=http://www.icaew.com/en/library/subject-gateways/auditing/knowledge-guide-to-international-standards-on-auditing |title=Knowledge guide to International Standards on Auditing (ISA) |publisher=[[Institute of Chartered Accountants in England and Wales]] |accessdate=2013-07-26}}</ref>
The [[Public Interest Oversight Board]] provides oversight of the IAASB, ensuring that the standards are in the [[public interest]].<ref>{{cite web |url=http://www.ifac.org/about-ifac/structure-governance/piob |title=Public Interest Oversight Board |publisher=International Federation of Accountants |accessdate=2013-07-27}}</ref><ref>{{cite web|url=https://www.nytimes.com/2005/03/01/business/worldbusiness/01audit.html |title=Global Overseer of Auditing Rules Is Born |first=Floyd |last=Norris |authorlink=Floyd Norris |publisher=[[The New York Times]] |date=March 1, 2005 |accessdate=2013-07-27}}</ref>

To further ensure proposed standards are in the public interest the IAASB consults its Consultative Advisory Group, which is composed of standard setters, various international organizations from the private and public sectors, and regulators. Representatives include a balance of users and prepares of financial statements, and should to the extent practicable be balanced geographically. 

Founded in March 1978 as the International Auditing Practices Committee (IAPC), the IAASB's current strategic themes include:
* Supporting global financial stability
* Enhancing the role, relevancy and quality of assurance in an evolving world
* Facilitating adoption and implementation of the standards it sets <ref> [http://www.ifac.org/auditing-assurance/about-iaasb] About IAASB </ref>


==See also==
* [[International Organization of Supreme Audit Institutions]]
* [[International Public Sector Accounting Standards]]

==References==
{{reflist}}

==External links==
* {{Official website|http://www.ifac.org/auditing-assurance}}

[[Category:International accounting organizations]]
[[Category:Auditing standards]]
[[Category:International standards]]