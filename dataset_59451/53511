{{Infobox person
| name        = Irwin Federman
| image       = 
| caption     = 
| birth_date  = {{birth year and age|1936}}
| birth_place = 
| birth_name  = 
| death_date  = 
| death_place = 
| residence   = 
| nationality = United States
| education   = B.S. [[Brooklyn College]]
| occupation  = Businessman
| known_for   = President of [[Monolithic Memories]]<br> General Partner of [[U.S. Venture Partners]]
| religion    = 
| networth    = 
| spouse      = Shiela Federman (until death)<br> Concepcion Socarras
| parents     = 
| children    = four with Shiela Federman<br> three step-children with Socarras
| family      = 
| website     = 
}}

'''Irwin Federman''' (born 1936) is an American businessman, philanthropist and General Partner of [[U.S. Venture Partners]].

==Biography==
Federman was born in 1936<ref>[http://www.forbes.com/profile/irwin-federman/ Businessweek: "Irwin Federman"] retrieved May 26, 2014</ref> and graduated with a B.S. in Economics from [[Brooklyn College]].<ref name=SantaClara>[http://www.scu.edu/business/cie/about/abm/Irwin-Federman.cfm Santa Clara University Leavey School of Business: "Irwin Federman: General Partner, U.S. Venture Partners] retrieved May 26, 2014</ref> After college, he was awarded the Forbes Gold Medal for attaining the highest grade in California on his [[Certified Public Accountant|CPA]] exam<ref name=SantaClara /><ref name=ACG /> and then worked as an accountant in New York and California.<ref name=SantaClara /> He then went on to serve as CFO of three startup companies the last of which was the troubled semiconductor manufacturer [[Monolithic Memories]] (MMI).<ref name=SantaClara /> Irwin was appointed MMI's President, the first non-engineer CEO in the semiconductor industry, and he presided over its successful turn around.<ref name=SantaClara /> Instead of laying workers off, he required that all workers to take one unpaid day off every other week, effectively cutting the payroll by 10% but preserving everyone's job: the policy actually boosted morale as workers bonded over the shared sacrifice; managers came into work on their off days; and everyone was more careful with their expenses.<ref>[http://www.insidebayarea.com/columnists/ci_12625094 Inside Bay Area: "Cassidy: Attention, CEOs: Here's proof that layoffs aren't always the best solution" by Mike Cassidy] June 19, 2009</ref> In 1980, MMI went public. In 1987, in a $442 million stock swap, MMI was merged with [[Advanced Micro Devices]]<ref>[http://articles.latimes.com/1987-05-04/business/fi-4817_1_chip-industry Los Angeles Times: "Synergy of Contrasting Personalities Behind Chip Merger" by DONNA K. H. WALTERS] May 04, 1987</ref> to become the world's largest integrated circuit manufacturer.<ref name=LATimesMonlithic>[http://articles.latimes.com/1987-05-01/business/fi-1886_1_monolithic-memories Los Angeles Times: "Monolithic Will Combine With Advanced Micro: $422-Million Stock Swap Will Create World's Biggest Integrated Circuit Maker" by NANCY RIVERA BROOKS] May 01, 1987</ref> Federman was appointed Vice Chairman of AMD.<ref name=SantaClara /><ref name=LATimesMonlithic /> In 1988, he served as Managing Director of investment banking firm [[Dillon, Read & Co.]]<ref name=ACG /> In 1990, he joined early-stage [[venture capital]] firm, [[U.S. Venture Partners]],<ref name=ACG /> where he served as General Partner.<ref name=SantaClara /> He previously served on the Boards of Directors of [[SanDisk]], [[Checkpoint Software Technologies]] and [[Mellanox]], Inc.<ref name=SantaClara />

==Philanthropy and awards==
Federman served two terms as Chairman of the U.S. Semiconductor Industry Association;<ref>[http://articles.latimes.com/1987-03-05/business/fi-7625_1_sematech Los Angeles Times: "U.S. Semiconductor Firms Plan R&D Pool" by OSWALD JOHNSTON] March 05, 1987</ref> he served on the Board of Directors of the National Venture Capital Association, and served two terms on the Dean's Advisory Board of the Leavey School of Business at Santa Clara <ref name=SantaClara /> Federman received Torch of Liberty Award from the [[Anti-Defamation League]] and the Brotherhood Award from the Silicon Valley Conference for Community and Justice. In 2004, Federman was inducted into Junior Achievement's Silicon Valley Hall of Fame. In 2008, he received the International Business Forum's Special Achievement Award. He serves as a trustee of [[San Francisco Ballet]], [[Brooklyn College]], and the [[San Francisco Museum of Modern Art]].<ref name=ACG /> He was also awarded an Honorary Doctorate of Engineering Science from [[Santa Clara University]]<ref name=SantaClara /><ref name=ACG /> and the Exemplary Community Leadership Award from the Silicon Valley Conference of Christians and Jews (now FACES).<ref name=ACG />

==Personal life==
He first wife was Sheila Federman with whom he had four children; she died in 1987.<ref name=ACG>[http://www.acg.org/sv/events/event.aspx?EventId=4644 ACG Silicon Valley: "Irwin Federman, U.S. Venture Partners"] retrieved May 26, 2014</ref><ref>[https://www.nytimes.com/1991/10/27/style/jaime-federman-is-married-to-philip-greenberg.html New York Times: "Jaime Federman Is Married to Philip Greenberg"] October 27, 1991</ref> In 1992, he remarried to Concepcion Socarras,<ref name=ACG /> the former wife of [[Dean Woodman]] and mother of three children including [[Nick Woodman]], the founder of [[GoPro]].<ref>[http://blogs.wsj.com/venturecapital/2013/06/20/how-family-ties-helped-nick-woodman-make-gopro-click/ Wall Street Journal: "How Family Ties Helped Nick Woodman Make GoPro Click" By Lizette Chapman] June 20, 2013 | ''"The two-time entrepreneur had already raised money for one of his startups and...was well-connected to Silicon Valley players. With dad Dean Woodman, who was a founding partner at what became investment bank Robertson Stephens, and stepdad U.S. Venture Partners General Partner Irwin Federman"''</ref><ref>[http://www.newyorksocialdiary.com/node/3713/print New York Social Diary] retrieved May 26, 2014</ref><ref>[http://www.exploresanfrancisco.biz/blog/bay-area-billionaires-2014/ Explore San Francisco: "Bay Area Billionaires 2014"] retrieved May 26, 2014</ref>

==References==
{{reflist}}

{{DEFAULTSORT:Federman, Irwin}}
[[Category:1936 births]]
[[Category:Brooklyn College alumni]]
[[Category:American business executives]]
[[Category:American venture capitalists]]
[[Category:Living people]]