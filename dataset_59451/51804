{{Infobox journal
| title = Journal of Consciousness Studies
| cover =
| editor = Valerie Gray Hardcastle
| discipline = [[Cognitive science]], [[neurophysiology]], [[philosophy]]
| abbreviation = J. Conscious. Stud.
| publisher = Imprint Academic
| country =
| frequency = Monthly
| history = 1994-present
| openaccess =
| impact =.78
| impact-year = 2011
| website = http://www.imprint.co.uk/jcs.html
| link1 = http://www.imprint.co.uk/jcs_contents.html
| link1-name = Online archive
| link2 = http://www.ingentaconnect.com/journals/browse/imp/jcs
| link2-name = Online access at Ingentaconnect
| JSTOR =
| OCLC = 32043673
| LCCN =
| CODEN =
| ISSN = 1355-8250
| eISSN =
}}
The '''''Journal of Consciousness Studies''''' is an [[interdisciplinarity|interdisciplinary]] [[peer review|peer-reviewed]] [[academic journal]] dedicated entirely to the field of [[consciousness]] studies. It was previously [[Editor-in-chief|edited]] by [[Joseph Goguen]]. It is currently edited by Professor Valerie Gray Hardcastle of the [[University of Cincinnati]].
==Background==
The journal was established in part to provide visibility across disciplines to various researchers approaching the problem of consciousness from their respective fields. The articles are usually in non-specialized language (in contrast to a typical academic journal) in order to make them accessible to those in other disciplines. This also serves to help make them accessible to laypersons.

In contrast to other journals, it attempts to incorporate fields beyond the realm of the [[natural science]]s and the [[social science]]s such as the [[humanities]], [[philosophy]], [[critical theory]], [[comparative religion]], and [[mysticism]].

==Articles==
Some examples of articles published in the journal:

* "The Astonishing Hypothesis" (1994) - [[Francis Crick]] & J. Clark
* "Facing Up to the Problem of Consciousness" (1995) - [[David Chalmers]]
* "Facing Backwards on the Problem of Consciousness" (1996) - [[Daniel Dennett]]
* "In the Theatre of Consciousness" (1997) - [[Bernard Baars]]
* "The 'Shared Manifold' Hypothesis: From Mirror Neurons to Empathy" (2001) - [[Vittorio Gallese]]

==Single theme issues==
From time to time, the journal publishes collections of thematically or topically related academic papers. These often take the form of a double issue.

==Notable contributors==
{{Unreferenced|section|date=July 2011}}
{{div col|cols=3}}
*[[Igor Aleksander]]
*[[James H. Austin]]
*[[Susan Blackmore]]
*[[Patricia Churchland]]
*[[Owen Flanagan]]
*[[Walter J. Freeman (neuroscientist)|Walter Freeman]]
*[[Nicholas Humphrey]]
*[[Ivan Illich]]
*[[Stanley Krippner]]
*[[George Lakoff]]
*[[Jaron Lanier]]
*[[Benjamin Libet]]
*[[E.J. Lowe|Jonathan Lowe]]
*[[John McCarthy (computer scientist)|John McCarthy]]
*[[Colin McGinn]]
*[[Thomas Metzinger]]
*[[Rafael E. Núñez]]
*[[Roger Penrose]]
*[[Vilayanur S. Ramachandran]]
*[[Oliver Sacks]]
*[[John Searle]]
*[[David Skrbina]]
*[[Luc Steels]]
*[[Galen Strawson]]
*[[Ed Subitzky]]
*[[Charles Tart]]
*[[Francisco Varela]]
*[[Ken Wilber]]
{{div col end}}

==Reporting on Conferences==
The journal reports on conferences, notably the [[Toward a Science of Consciousness]] (TSC) conference, which is organized by the [[Center for Consciousness Studies]] based at the [[University of Arizona]] in Tucson. See, for example, TSC 2012.<ref>{{cite journal|title=A Thousand Flowers|journal=Journal of Consciousness Studies|volume=19|issue=7–8|pages=247–70|date=July–August 2012}}</ref>

Papers from the first [[Online Consciousness Conference]] were published in a special issue of JCS in April 2010.<ref>http://www.imprint.co.uk/jcs_17_3-4.html</ref>

==References==
{{reflist}}

==External links==
* {{Official website|http://www.imprint.co.uk/jcs.html}}

{{Use dmy dates|date=September 2010}}

{{DEFAULTSORT:Journal Of Consciousness Studies}}
[[Category:Consciousness studies]]
[[Category:Publications established in 1994]]
[[Category:1994 establishments in Ohio]]
[[Category:Cognitive science journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]