{{Infobox scientist
| name              = Janelle Knox-Hayes
| image             = Janelle_Knox-Hayes_small.jpg
| image_size        = 
| alt               = 
| caption           = 
| birth_place       = [[Cortez, Colorado|Cortez]], [[Colorado]], [[United States]]
| death_date        = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} (death date then birth date) -->
| death_place       = 
| nationality       = [[United States|American]]
| fields            = [[Economic geography]], [[Environmental finance]]
| workplaces        = [[Massachusetts Institute of Technology]]
| alma_mater        = [[University of Oxford]]<br/>[[University of Colorado Boulder]]
| doctoral_advisor  = [[Gordon L. Clark]]
| academic_advisors = 
| doctoral_students = 
| notable_students  =
| influences        = 
| influenced        =
| known_for         = [[Emissions trading|Carbon Markets]], [[Environmental finance]]
| awards            = Boettcher Scholarship, <br>Jack Kent Cooke Graduate Scholarship, Abe Fellowship, Fulbright Scholarship
}}

'''Janelle Knox-Hayes''' is the Lister Brothers [[Associate Professor]] of [[Economic geography|Economic Geography]] in the Department of Urban Studies and Planning<ref>{{cite web|url=http://dusp.mit.edu |title=MIT Urban Planning |publisher=dusp.mit.edu |date= |accessdate=2015-08-22}}</ref>  at the [[Massachusetts Institute of Technology]].<ref>{{cite web|url=http://dusp.mit.edu/faculty/janelle-knox-hayes |title=Janelle Knox-Hayes &#124; Department of Urban Studies and Planning at MIT |publisher=dusp.mit.edu |date= |accessdate=2015-08-22}}</ref>  Her research and teaching explore the institutional nature of social, economic and environmental systems, and the ways in which these are impacted by changing socio-economic spatial and temporal dynamics.

==Education==
A native of the [[Four Corners]] region of [[Colorado]], Knox-Hayes received her BA ([[Summa Cum Laude]]) in International Affairs, Ecology, and Japanese Language and Civilizations from the [[University of Colorado Boulder]] in 2004.  As an undergraduate, she worked with Professor Alex Cruz on the [[ethology]] and [[reproductive biology]] of [[African cichlid]] and the cuckoo catfish ([[Synodontis multipunctata]]).  Her research on the subject appeared in the 2004 version of the Encyclopedia of Animal Behavior.<ref>{{cite web|url=http://www.amazon.com/Encyclopedia-Animal-Behavior-Vol-Set/dp/0313327459 |title=Encyclopedia of Animal Behavior (3 Vol. Set): Marc Bekoff: 9780313327452: Amazon.com: Books |publisher=Amazon.com |date= |accessdate=2013-10-04}}</ref>  Knox-Hayes received her [[MSc]] in Nature, Society and Environmental Policy from the [[University of Oxford]].  There, she worked with Gordon Clark, then [[Halford Mackinder Professor of Geography]] and now Director of the [[Smith School of Enterprise and the Environment]] at Oxford University,<ref>{{cite web|url=http://www.smithschool.ox.ac.uk/new-director-of-the-smith-school-of-enterprise-and-the-environment-appointed/ |accessdate=May 12, 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20130307033129/http://www.smithschool.ox.ac.uk/new-director-of-the-smith-school-of-enterprise-and-the-environment-appointed/ |archivedate=March 7, 2013 }}</ref> on the geographic and socio-demographic characteristics of financial literacy and retirement planning.  Knox-Hayes stayed at Oxford for her [[DPhil]], continuing to work with Clark but shifting focus to climate change policy, specifically the role of carbon markets and environmental finance.

==Career==
Before starting graduate school, Knox-Hayes worked as an energy analyst for the United States [[Government Accountability Office]]. While in graduate school, she served as the president of Oxford Women in Politics, an organization intended to support and empower women in politics.<ref>{{cite web|url=http://www.oxwip.org/ |title=Oxford Women in Politics |publisher=OxWip |date= |accessdate=2013-10-04}}</ref> She also worked as an energy analyst for [http://about.bnef.com/ New Energy Finance]. In 2009, Knox-Hayes took up her current position as an assistant professor at the Georgia Institute of Technology.  There she has taught a range of courses focused on environmental sustainability, environmental finance, and political economy.

Knox-Hayes is the author of a number of peer-reviewed works falling broadly into three thematic clusters.

The first focuses on the institutional development of environmental markets and includes publications appearing in the ''Journal of Economic Geography'',<ref>{{cite web|author= |url=http://joeg.oxfordjournals.org/content/9/6/749.abstract |title=The developing carbon financial service industry: expertise, adaptation and complementarity in London and New York |publisher=Joeg.oxfordjournals.org |date=2009-02-17 |accessdate=2013-10-04}}</ref> the ''Annals of the American Association of Geographers'',<ref>{{cite journal|url=http://www.tandfonline.com/doi/full/10.1080/00045608.2010.500554#.UY9aqoLYDRw|title=Constructing Carbon Market Spacetime: Climate Change and the Onset of Neo-M|publisher=Tandfonline.com|accessdate=2013-10-04|doi=10.1080/00045608.2010.500554|volume=100|journal=Annals of the Association of American Geographers|pages=953–962}}</ref> ''Competition and Change'',<ref>{{cite web|url=http://www.ingentaconnect.com/content/maney/com/2010/00000014/F0020003/art00003 |title=ingentaconnect Creating the Carbon Market Institution: Analysis of the Organizat |publisher=Ingentaconnect.com |date=2010-12-01 |accessdate=2013-10-04}}</ref> ''Organization and Environment'',<ref>{{cite web|author= |url=http://oae.sagepub.com/content/26/1/61.abstract?patientinform-links=yes&legid=spoae;26/1/61 |title=The Time and Space of Materiality in Organizations and the Natural Environment |publisher=Oae.sagepub.com |date=2013-01-22 |accessdate=2013-10-04}}</ref> ''Regulation and Governance'',<ref>{{cite journal|url=http://onlinelibrary.wiley.com/doi/10.1111/j.1748-5991.2012.01138.x/abstract |title=Negotiating climate legislation: Policy path dependence and coalition stabilization - Knox-Hayes - 2012 - Regulation & Governance - Wiley Online Library |publisher=Onlinelibrary.wiley.com |date= |accessdate=2013-10-04 |doi=10.1111/j.1748-5991.2012.01138.x |volume=6 |journal=Regulation & Governance |pages=545–567}}</ref> and ''Global Environmental Change''.<ref>{{cite journal|url=http://www.sciencedirect.com/science/article/pii/S0959378013000228 |title=Understanding attitudes toward energy security: Results of a cross-national survey  |date= |accessdate=2013-10-04 |doi=10.1016/j.gloenvcha.2013.02.003 |volume=23 |journal=Global Environmental Change |pages=609–622}}</ref>  This work investigates the political and economic interfaces between financial markets and environmental systems, particularly the political and financial development of carbon emissions markets and other governance mechanisms like carbon disclosure.  Topics include the institutional settings and contexts under which markets are developed, the impacts of financial governance on environmental materiality, the nature of coalitions and security in climate policy formation and the changing nature of production systems as spearheaded by carbon markets.

The second includes her book and publications in ''Transactions of the Institute of British Geographers'',<ref>{{cite journal|url=http://onlinelibrary.wiley.com/doi/10.1111/j.1475-5661.2007.00277.x/abstract |title=Mapping UK pension benefits and the intended purchase of annuities in the aftermath of the 1990s stock market bubble - Clark - 2007 - Transactions of the Institute of British Geographers - Wiley Online Library |publisher=Onlinelibrary.wiley.com |date= |accessdate=2013-10-04 |doi=10.1111/j.1475-5661.2007.00277.x |volume=32 |journal=Transactions of the Institute of British Geographers |pages=539–555}}</ref> ''Environment and Planning A'',<ref>{{cite web|url=http://www.envplan.com/abstract.cgi?id=a41265 |title=Environment and Planning A abstract |publisher=Envplan.com |date= |accessdate=2013-10-04}}</ref> and ''Pensions''<ref>{{cite web|author= |url=http://www.palgrave-journals.com/pm/journal/v14/n1/abs/pm200838a.html |title=Pensions: An International Journal - Abstract of article: The /`new/' paternalism, consultation and consent: Expectations of UK participants in defined contribution and self-directed retirement savings schemes |publisher=Palgrave-journals.com |date=2009-02-01 |accessdate=2013-10-04}}</ref> and investigates how individuals plan for the future under conditions of economic uncertainty as well as the limits to which individual cognition can accommodate temporal scale.  These works explore the spatial nature and temporal scale of individual cognition in retirement planning, the effects of demographic factors on planning preparedness, and the impacts of economic uncertainty.

The third concerns the business and organizational implications of tacit knowledge.  Knox-Hayes's work on the subject appears in ''Strategic Organization'', and investigates the social and organizational impacts of information liberation and compression.  Specifically she is concerned with the nature of spatially and temporally embedded information (through tacit knowledge and culture) and how the liberation of information shapes governance and business operation.

==Awards==
Knox-Hayes has been the recipient of a number of prestigious awards.  In 2000, she was awarded the Boettcher Scholarship, the oldest and most prestigious undergraduate scholarship in the state of Colorado.<ref>{{cite web|author= |url=http://www.boettcherfoundation.org/home/scholarships/ |title=Scholarships |publisher=Boettcher Foundation |date= |accessdate=2013-10-04}}</ref>  In 2005 she was the recipient a 6-year graduate scholarship from the [[Jack Kent Cooke Foundation]].<ref>{{cite web|url=http://www.jkcf.org/our-scholars/alumni/567-Janelle-Knox-Hayes |title=Jack Kent Cooke Foundation - Janelle Knox-Hayes |publisher=Jkcf.org |date= |accessdate=2013-10-04}}</ref>  Subsequently she has received funding from the National Science Foundation and Georgia Department of Transportation. From 2012-2013 she held an Abe Fellowship from the [[Social Science Research Council]]<ref>{{cite web|author= |url=http://www.ssrc.org/fellowships/competitions/abe-fellowship/05282551-C595-E011-BD4E-001CC477EC84/ |title=Abe Fellowship 2011 — Fellowships & Grants — Social Science Research Council |publisher=Ssrc.org |date= |accessdate=2013-10-04}}</ref> and in 2014 she was awarded a [[Fulbright]] scholarship<ref>{{cite web|author= |url=http://spp.gatech.edu/news/janelle-knox-hayes-wins-fulbright-scholarship?destination=node/3604 |title=Janelle Knox-Hayes wins Fulbright scholarship |publisher=gatech.edu |date= |accessdate=2014-09-04}}</ref> to study and teach in Iceland

==Bibliography==
*''Saving for Retirement: Intention, Context, and Behavior'' with Gordon L. Clark and Kendra Strauss (Oxford University Press, 2012)
*''Cultures of Markets: The Political Economy of Climate Governance'' (Oxford University Press, 2016)

==References==
{{Reflist}}

==External links==
* [http://gatech.academia.edu/JanelleKnoxHayes Janelle Knox-Hayes's Academia.edu profile page]
* [http://www.geog.ox.ac.uk/staff/jknox-hayes.html Profile at the University of Oxford School of Geography and Environment]

{{DEFAULTSORT:Knox-Hayes, Janelle}}
[[Category:1983 births]]
[[Category:Living people]]
[[Category:Economic geographers]]
[[Category:Georgia Institute of Technology faculty]]
[[Category:University of Colorado Boulder alumni]]
[[Category:Alumni of Green Templeton College, Oxford]]
[[Category:MIT School of Architecture and Planning faculty]]