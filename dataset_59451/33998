{{Use Australian English|date=May 2011}}
{{Use dmy dates|date=January 2013}}
{{Featured article}}
{{Infobox cricketer
|          name = Ernie Toshack
|              female = 
<!-- Commented out because image was deleted: |               image = ErnieToshack.jpg -->
|             country = Australia
|            fullname = Ernest Raymond Herbert Toshack
|            nickname = The Black Prince
|          birth_date = {{Birth date|1914|12|8|df=yes}}
|        birth_place = [[Cobar, New South Wales]], Australia
|          death_date = {{Death date and age|2003|5|11|1914|12|8|df=yes}}
|        death_place = [[Bobbin Head, New South Wales]], Australia
|            heightft = 
|          heightinch = 
|             heightm = 
|             batting = Right-hand
|             bowling = Left-arm [[medium pace bowling|medium-pace]]
|                role = Specialist [[Bowler (cricket)|bowler]]
|       international = true
|       testdebutdate = 29 March
|       testdebutyear = 1946
|    testdebutagainst = New Zealand
|             testcap = 
|        lasttestdate = 22 July
|        lasttestyear = 1948
|     lasttestagainst = England
|               club1 = [[New South Wales cricket team|New South Wales]]
|               year1 = 1945–1949
|          deliveries = balls
|             columns = 2
|             column1 = [[Test cricket|Tests]]
|            matches1 = 12
|               runs1 = 73
|            bat avg1 = 14.59
|           100s/50s1 = 0/0
|          top score1 = 20*
|         deliveries1 = 3140
|            wickets1 = 47
|           bowl avg1 = 21.04
|            fivefor1 = 4
|             tenfor1 = 1
|       best bowling1 = 6/29
|  catches/stumpings1 = 4/0
|             column2 = [[First-class cricket|FC]]
|            matches2 = 48
|               runs2 = 185
|            bat avg2 = 5.78
|           100s/50s2 = 0/0
|          top score2 = 20*
|         deliveries2 = 11901
|            wickets2 = 195
|           bowl avg2 = 20.37
|            fivefor2 = 12
|             tenfor2 = 1
|       best bowling2 = 7/81
|  catches/stumpings2 = 10/0
|                date = 27 December
|                year = 2007
|              source = http://www.cricinfo.com/ci/content/player/7960.html Cricinfo
}}

'''Ernest Raymond Herbert Toshack''' (8 December 1914 – 11 May 2003) was an Australian [[cricketer]] who played in 12 [[Test cricket|Tests]] from 1946 to 1948. A left arm [[fast bowling|medium paced]] [[bowling (cricket)|bowler]] known for his accuracy and stamina in the application of [[leg theory]], Toshack was a member of [[Don Bradman]]'s  [[Australian cricket team in England in 1948|"Invincibles"]] that toured England in 1948 without being defeated. Toshack reinforced the Australian [[cricket ball#Condition of a cricket ball|new ball attack]] of [[Ray Lindwall]] and [[Keith Miller]].

Born in 1914, Toshack overcame many obstacles to reach international level cricket. He was orphaned as an infant, and his early cricket career was hindered because of financial difficulties caused by the [[Great Depression in Australia|Great Depression]]. The [[Second World War]] prevented Toshack from competing at [[first-class cricket|first-class level]] until he was into his thirties. In 1945–46, the first season of cricket after the end of the War, Toshack made his debut at first-class level and after only seven matches in the [[Sheffield Shield]] he was selected for Australia's tour of [[New Zealand]]. In [[Wellington]], he opened the bowling in a match that was retrospectively classed as an official Test match. Toshack became a regular member of the Australian team, playing in all of its Tests until the 1947–48 series against [[India national cricket team|India]]. He took his career-best match [[Bowling analysis|bowling figures]] of 11 [[Wicket#Dismissing a batsman|wickets]] for 31 [[Run (cricket)|runs]] (11/31) in the First Test but began to suffer recurring knee injuries, and a medical board had to approve his selection for the 1948 England tour. Toshack played in the first four Tests before being injured. After a long convalescence, he attempted a comeback during Australia's 1949–50 season, but further injury forced him to retire. He was a parsimonious bowler, who was popular with crowds for his sense of humour.

==Early years==
Born in the New South Wales bush town of [[Cobar, New South Wales|Cobar]] on 8 December 1914, Toshack was one of five children born to a stationmaster. Orphaned at the age of six,<ref name="p5">Pollard (1990), p. 5.</ref><ref name="tel"/> he was raised by relatives in [[Lyndhurst, New South Wales|Lyndhurst]] in the northwest of the state, and played his early cricket and [[rugby league]] for [[Cowra]].<ref name="az">{{cite book| last = Cashman |first=Richard | year = 1997 | title = The A-Z of Australian cricketers|pages =299–300|isbn=0-19-550604-9}}</ref> At this time, Toshack's ambition was to play rugby league for [[Australia national rugby league team|Australia]]. One of his childhood friends, [[Edgar Newham]], also played both sports and wanted to play Test cricket. However, the town's doctor, a local community leader, advised that they were targeting the wrong sport, and the two boys followed his recommendation. Newham later played rugby league for Australia.<ref name=a/>

In his youth he was also a [[Boxing|boxer]], and earned the nickname "Johnson" for his resemblance to American black heavyweight boxing champion [[Jack Johnson (boxer)|Jack Johnson]]. In the mid-1930s, he made brief appearances for the State Colts and [[11 (number)#In sports|Second XI]],<ref name="wisden">{{cite web| title= Players and Officials – Ernie Toshack | url=http://www.cricinfo.com/ci/content/player/7960.html | publisher=[[Wisden]]| accessdate=2007-05-23}}</ref> and played cricket against the likes of the Test cricketer [[Stan McCabe]].<ref name="p5"/> In December 1933, Toshack played in a colts match for [[New South Wales cricket team|New South Wales]] against [[Queensland cricket team|Queensland]]. He took 3/63 (three [[Wicket#Dismissing a batsman|wickets]] at a cost of 63 runs) and 3/36 but was unable to prevent a five-wicket defeat.<ref name=o/> He then took a total of 3/88 in a match for New South Wales Country against their city counterparts, and was promoted into the state's Second XI.<ref name=o/> Toshack took a total of 1/91 in a match against the [[Victoria cricket team|Victorian]] Second XI and did not play for his state again until 1945.<ref name=o/>

His cricket aspirations, already hindered due to economic difficulties caused by the [[Great Depression in Australia|Great Depression]], were further interrupted when he was wheelchair-bound for months after a [[appendicitis|ruptured appendix]] in 1938.<ref name="az"/> He was not allowed to enlist in the [[Australian Defence Force]] during [[World War II]] and worked at [[Lithgow, New South Wales|Lithgow]]'s Small Arms factory, in the [[Blue Mountains (Australia)|Blue Mountains]] west of Sydney. Only at the end of the war, aged 30, did he go to Sydney. At the time, he was a [[fast bowling#Categorisation of fast bowling|medium-fast]] left-arm bowler and approached [[Petersham, New South Wales|Petersham]]—as Toshack lived in their locality, they had the right to register him ahead of other clubs. They did not select Toshack, so he joined Marrickville in [[Sydney Grade Cricket]], starting in the [[Division (sport)|third grade team]] in 1944–45.<ref name="wisden"/> Within two matches, he rose to the first grade team.<ref name="az"/> By this time, Petersham regretted their decision to spurn Toshack and lodged a complaint with the cricket authorities, claiming that he was obliged to represent them and ineligible to play for Marrickville.<ref name=a/> Toshack later recalled that Petersham were "told where to go".<ref name=a/>

==First-class and Test debut==
<!-- Deleted image removed: [[File:ErnToshackBowling.jpg|right|thumb|Toshack in his delivery stride]] -->
Upon the resumption of first-class cricket in 1945–46, Toshack made his debut for [[New South Wales cricket team|New South Wales]] against [[Queensland cricket team|Queensland]] as an opening bowler aged almost 31, and was quickly among the [[Wicket#Dismissing a batsman|wickets]]. He took four 4/69 in his first innings as his team took a 128-run lead, but he managed only 0/87 from 20 overs in the second innings as New South Wales fell to a [[result (cricket)|four-wicket loss]], failing to defend a target of 270.<ref name=o/><ref>{{cite web|url=http://www.cricinfo.com/db/ARCHIVE/1940S/1945-46/AUS_LOCAL/QLD_NSW_23-27NOV1945.html|publisher=[[Cricinfo]]|accessdate=2007-11-28|title=Queensland vs New South Wales at Brisbane, November&nbsp;23–27, 1945}}</ref> His first wicket was that of [[Geoff Cook]].<ref name=ow>{{cite web| title=Player Oracle ERH Toshack|url=http://www.cricketarchive.com/cgi-bin/player_oracle_reveals_results2.cgi?playernumber=745&testing=0&opponentmatch=exact&playername=IK%20Pathan&resulttype=All&matchtype=All&teammatch=exact&startwicket=&homeawaytype=All&opponent=&endwicket=&wicketkeeper=&searchtype=WicketsTakenList&endscore=&playermatch=contains&branding=cricketarchive&captain=&endseason=&startscore=&team=&startseason= |accessdate=2009-05-14 |publisher=[[CricketArchive]]}}</ref> Toshack's most successful match of the season came in the following fixture, against [[South Australia cricket team|South Australia]]. Taking 4/30 and 4/78 as New South Wales won by an innings.<ref name=o>{{cite web| title=Player Oracle ERH Toshack|url=http://www.cricketarchive.com/cgi-bin/player_oracle_reveals_results2.cgi?playernumber=745&opponentmatch=exact&playername=Meckiff&resulttype=All&matchtype=All&teammatch=exact&startwicket=&homeawaytype=All&opponent=&endwicket=&wicketkeeper=&searchtype=InningsList&endscore=&playermatch=contains&branding=cricketarchive&captain=&endseason=&startscore=&team=&startseason= |accessdate=2009-05-14 |publisher=[[CricketArchive]]}}</ref> He then took 2/36 and 3/54 in an innings victory over the [[Australian Services cricket team|Australian Services]].<ref name=o/>

By the end of the season, in March 1946, Toshack had taken 35 wickets in seven first-class matches, at an [[bowling average|average]] of 18.82, making him the second highest wicket-taker behind [[George Tribe]]. He performed consistently and took at least four wickets in each match;<ref name=o/> his innings best was 4/30.<ref>{{cite web|url=http://www.cricinfo.com/db/ARCHIVE/1940S/1945-46/AUS_LOCAL/STATS/FC_1945-46_BOWL_MOST_WKTS.html|publisher=[[Cricinfo]]|accessdate=2007-11-28|title=Australian First-Class Season 1945/46: Bowling – Most Wickets}}</ref> Toshack was selected for a non-Test tour of [[New Zealand]].<ref name=az/> He played in three provincial tour matches against [[Auckland cricket team|Auckland]], [[Canterbury cricket team|Canterbury]] and [[Wellington cricket team|Wellington]], all of which were won by an innings.<ref name=o/> He took match figures of 7/91 against Auckland and 8/58 against Wellington.<ref name=o/>

In the final match of the tour, Toshack opened the bowling for Australia with fellow debutant [[Ray Lindwall]] in a match against [[New Zealand cricket team|New Zealand]] at [[Wellington]] that was retrospectively recognised as a Test two years later.<ref name="az"/> As it was eight years since Australia's last Test, a new post-war generation of international cricketers made their debut. Toshack was one of seven Australians playing their first Test.<ref name="ds"/> [[New Zealand cricket team|New Zealand]] were routed inside two days on a [[sticky wicket|damp pitch]], having been dismissed for 42 in their first innings after winning the toss and choosing to bat.<ref name="ds"/> Toshack's first Test wicket was that of opposing captain [[Walter Hadlee]], who was caught by [[Keith Miller]]. Toshack took three further wickets to end with innings figures of 4/12. He did not bat as Australia made 8/198. New Zealand were then bowled out in their second innings for 54; Toshack took 2/6 as Australia recorded an innings victory.<ref>Pollard (1988), p. 375.</ref>  He dismissed [[Eric Tindill]] and [[Ces Burke]] in both innings.<ref name="ds">{{cite web|url=http://www.cricinfo.com/ci/engine/match/62661.html|publisher=[[Cricinfo]]|accessdate=2007-11-28|title=Only Test:New Zealand vs Australia at Wellington, March&nbsp;29–30, 1946}}</ref> The performance ensured that Toshack would become an integral part of Australia's attack for the next three years.<ref name="wisden"/><ref name="obit"/> Toshack ended the tour with 23 wickets at 10.34 in four matches.<ref name=o/>

<!-- Deleted image removed: [[File:ToshackPortrait.jpg|left|125px|thumb|Ernie Toshack]] -->
Toshack started the 1946–47 season strongly, taking 5/46 and 4/70 as New South Wales opened the season with a five-wicket win over Queensland.<ref name=o/> He removed Australian wicket-keeper [[Don Tallon]] twice.<ref name=ow/> After going wicketless in a rain-curtailed match for his state against [[England cricket team|England]], Toshack was selected to make his [[the Ashes|Ashes]] debut in the [[1946-47 Ashes series#First Test – Brisbane|First Test at Brisbane]].<ref name="testlist"/> With the emergence of leading [[all rounder]] [[Keith Miller]], Toshack was relegated to first change bowler as Miller began his much celebrated partnership with Lindwall.<ref name="Ash1"/> Toshack was unbeaten on one in his first innings with the bat when Australia were bowled out for 645 on the third day.<ref name="Ash1"/><ref name="p381">Pollard (1988), p. 381.</ref>

On a [[sticky wicket]], Toshack initially struggled, bowling his characteristic leg stump line. England struggled to 117 runs for the loss of five wickets (5/117) at the end of the fourth day despite many interruptions caused by rain.<ref name="Ash1"/> [[Norman Yardley]] and captain [[Wally Hammond]] had defied the Australian bowlers since coming together at 5/66.<ref name="Ash1"/> On the fifth and final morning, captain [[Don Bradman]] advised him to pitch straighter and at a slower pace.<ref name="az"/><ref name="wisden"/><ref name="obit"/><ref name="p381"/> Before play began Bradman took him down the pitch and showed him exactly where he wanted him to bowl and even make him bowl a practice [[over (cricket)|over]] alongside to make sure he got it right.<ref>p119, [[Clif Cary]], ''Cricket Controversy, Test matches in Australia 1946–47'', T. Werner Laurie Ltd, 1948</ref><ref>pp61-62, [[E.W. Swanton]], ''Swanton in Australia with MCC 1946–1975'', Fontana/Collins, 1975</ref> Having started the day wicketless, Toshack [[dismissal (cricket)|dismissed]] Yardley and Hammond in the space of 13 runs to break the English resistance and finished with an economical 3/17 from 17 overs as England were bowled out for 141.<ref name="Ash1"/> Bradman enforced the follow-on, and with Lindwall indisposed,<ref name="p381"/> Toshack took the new ball with Miller. He continued where he finished in the first innings, taking four of the first six wickets ([[Bill Edrich]], [[Denis Compton]], Hammond and Yardley)<ref name=o/> as the English top order were reduced to 6/65. He ended the innings with 6/82 as England were bowled out twice in a day to lose by an innings and 332 runs.<ref name="wisden"/><ref name="obit"/><ref name="Ash1">{{cite web| url=http://www.cricinfo.com/ci/engine/match/62665.html| publisher=[[Cricinfo]]|accessdate=2007-11-28|title=1st Test:Australia vs England at Brisbane, Nov&nbsp;29- Dec&nbsp;4, 1946}}</ref> The remaining four Tests were less successful: only in one innings did he take more than one wicket. In the [[1946-47 Ashes series#Second Test – Sydney|Second Test at Sydney]] the pitch favoured [[spin bowling]] and Toshack only bowled 13 overs without taking a wicket as Australia claimed another innings victory. He took match figures of 2/127 on a flat pitch in the [[1946-47 Ashes series#Third Test – Melbourne|Third Test in Melbourne]],<ref>Piesse, p. 149.</ref> removing [[Len Hutton]] and Compton.<ref name=ow/> During the match, Toshack came in to bat in the second innings with Australia nine wickets down. He defended stubbornly and ended unbeaten on two as his partner Lindwall went from 81 to 100 to score the fastest Test century by an Australian, in 88 balls.<ref name=o/><ref>Piesse, p. 150.</ref> Toshack was more productive in the drawn [[1946-47 Ashes series#Fourth Test – Adelaide|Fourth Test in Adelaide]], where he took match figures of 5/135 from 66 eight ball overs in extreme heat,<ref name="wisden"/><ref name="obit">{{cite web| title= Obituary, 2004 – Ernie Toshack | url=http://www.cricinfo.com/ci/content/story/155391.html |year=2004 | publisher=[[Wisden]]| accessdate=2007-05-23}}</ref> including the wicket of Hammond twice, Edrich and [[Joe Hardstaff junior]].<ref name=ow/> Ahead of the final Test, Toshack removed Compton, Edrich and [[Godfrey Evans]] in a drawn match for [[MCC tour of Australia in 1946–47#Victoria vs MCC|Victoria against the tourists]].<ref name=o/><ref name=ow/> He took only one wicket in the [[1946-47 Ashes series#Fifth Test – Sydney|Fifth Test]] as Australia sealed the series 3–0 with a five-wicket win.<ref name=o/> Toshack finished the series with 17 wickets at a [[bowling average]] of 25.71.<ref name="testlist">{{cite web| title= Statsguru – ERH Toshack – Tests – Innings by innings list | url=http://stats.cricinfo.com/guru?sdb=player;playerid=745;class=testplayer;filter=basic;team=0;opposition=0;notopposition=0;season=0;homeaway=0;continent=0;country=0;notcountry=0;groundid=0;startdefault=1946-03-29;start=1946-03-29;enddefault=1948-07-27;end=1948-07-27;tourneyid=0;finals=0;daynight=0;toss=0;scheduledovers=0;scheduleddays=0;innings=0;result=0;followon=0;seriesresult=0;captain=0;keeper=0;dnp=0;recent=;viewtype=aro_list;runslow=;runshigh=;batposition=0;dismissal=0;bowposition=0;ballslow=;ballshigh=;bpof=0;overslow=;overshigh=;conclow=;conchigh=;wicketslow=;wicketshigh=;dismissalslow=;dismissalshigh=;caughtlow=;caughthigh=;caughttype=0;stumpedlow=;stumpedhigh=;csearch=;submit=1;.cgifields=viewtype | publisher=[[Cricinfo]]| accessdate=2007-11-28}}</ref><ref>Pollard (1988), p. 389.</ref> His first-class season was not as productive as in his debut year; he took 33 wickets at an average of 30.93 in eleven matches, making him the sixth highest wicket-taker for the season.<ref>{{cite web|url=http://www.cricinfo.com/db/ARCHIVE/1940S/1946-47/AUS_LOCAL/STATS/FC_1946-47_BOWL_MOST_WKTS.html|publisher=[[Cricinfo]]|accessdate=2007-11-28|title=Australian First-Class Season 1946/47: Bowling – Most Wickets}}</ref> Toshack had a particularly unsuccessful time in the two [[Sheffield Shield]] matches against arch-rivals [[Victorian Bushrangers|Victoria]], which were lost by heavy margins of an innings and 114 runs, and 288 runs respectively.<ref name=o/> In the first match he took 0/133 after Australian team-mate Miller hit three [[six (cricket)|sixes]] from his opening over.<ref name=o/><ref>Perry, pp. 193–194.</ref> In the second match he took a total of 3/144.<ref name=o/> Victoria went on to win the title.

The following 1947–48 season, Toshack warmed up for the Test campaign against the touring [[Indian cricket team|Indians]] by taking 2/64 and 4/65 for New South Wales in an innings win,<ref name=o/> dismissing [[Hemu Adhikari]] twice.<ref name=ow/> He retained his position in the national team, and in the First Test at [[Brisbane]] on a wet pitch,<ref name="p393"/> Toshack took ten wickets for the only time in his Test career.<ref name="testlist"/> In reply to Australia's 8/382 [[declaration and forfeiture|declared]], India had been reduced to 5/23 by Lindwall, Miller and [[Bill Johnston (cricketer)|Bill Johnston]] before [[Vijay Hazare]] and captain [[Lala Amarnath]] took the score to 53 without further loss, prompting Toshack's introduction into the attack. He dismissed both and removed the remaining lower-order batsmen to end with 5/2 in 19 balls as India were bowled out after adding only five further runs.<ref name="p393">Pollard (1988), p. 393.</ref><ref name="ind1"/> Bradman enforced the [[follow on]] and India reached 1/27 before a spell of 6/29 from Toshack reduced them to 8/89, including the wickets of Hazare, Amarnath and [[Khanderao Rangnekar]] for a second time.<ref name=o/><ref name=ow/><ref name=ind1/> India were bowled out for 98 as Australia won by an innings and 226 runs.<ref name=o/><ref name="obit"/><ref name="ind1">{{cite web| url=http://www.cricinfo.com/ci/engine/match/62676.html| publisher=[[Cricinfo]]|accessdate=2007-11-28|title=1st Test:Australia vs India at Brisbane, Nov&nbsp;29- Dec&nbsp;4, 1947}}</ref> Injury persistently curtailed Toshack during the season, and he missed a month of cricket, including the next two Tests.<ref name=o/><ref name=testlist/> He returned for the second match against arch-rivals Victoria, and took 6/38 and 2/71 to play a key role in a New South Wales victory by six wickets.<ref name=o/> His victims in the first innings included Test batsmen [[Lindsay Hassett]], [[Neil Harvey]] and [[Sam Loxton]] as New South Wales took a decisive 290-run lead.<ref name=o/><ref name=ow/> He dismissed Hassett and [[Ken Meuleman]] in the second innings to help set up victory.<ref name=o/><ref name=ow/> Toshack only played in one further Test during the season, the Fourth, where he was less successful with match figures of 2/139.<ref name="testlist"/> He dismissed centurion [[Dattu Phadkar]] as Australia went on to win the series 4–0.<ref name=ow/><ref name="auslist">{{cite web|url=http://stats.cricinfo.com/guru?sdb=team;team=AUS;class=testteam;filter=basic;opposition=0;notopposition=0;decade=0;homeaway=0;continent=0;country=0;notcountry=0;groundid=0;season=0;startdefault=1877-03-15;start=1877-03-15;enddefault=2007-11-20;end=2007-11-20;tourneyid=0;finals=0;daynight=0;toss=0;scheduledovers=0;scheduleddays=0;innings=0;followon=0;result=0;seriesresult=0;captainid=0;recent=;viewtype=resultlist;runslow=;runshigh=;wicketslow=;wicketshigh=;ballslow=;ballshigh=;overslow=;overshigh=;bpo=0;batevent=;conclow=;conchigh=;takenlow=;takenhigh=;ballsbowledlow=;ballsbowledhigh=;oversbowledlow=;oversbowledhigh=;bpobowled=0;bowlevent=;submit=1;.cgifields=viewtype |title=Statsguru – Australia – Tests – Results list |publisher=[[Cricinfo]] |accessdate=2007-12-21}}</ref> When fit, Toshack was a heavy wicket-taker; his 41 wickets at 20.26 placed him second only to [[Bill Johnston (cricketer)|Bill Johnston]]'s 42 among Australian bowlers for the season.<ref>{{cite web|url=http://www.cricinfo.com/db/ARCHIVE/1940S/1947-48/AUS_LOCAL/STATS/FC_1947-48_BOWL_MOST_WKTS.html|publisher=[[Cricinfo]]|accessdate=2007-11-28|title=Australian First-Class Season 1947/48: Bowling – Most Wickets}}</ref>

==''Invincibles'' tour==
{{main article|Ernie Toshack with the Australian cricket team in England in 1948}}
{{See also|Australian cricket team in England in 1948|1948 Ashes series}}
<!-- Deleted image removed: [[File:Toshack&Hamence.jpg|right|thumb|Toshack (right) pictured with Invincibles teammate [[Ron Hamence]]]] -->
By the end of the Indian series, knee injuries had begun to hamper Toshack, and he only made the trip to England for the 1948 tour on a 3–2 majority vote by a medical team,<ref name="obit"/> despite being one of the first selected by the board. Two Melbourne doctors ruled him unfit, but three specialists from his home state presented a more optimistic outlook that allowed him to tour.<ref name="p6"/> The tour was to guarantee him immortality as a member of Bradman's "[[Australian cricket team in England in 1948|Invincibles]]". He grew tired of signing autographs during the voyage, and entrusted a friend with the task. As a result, there are still sheets circulating with his name mis-spelt as ''Toshak''.<ref name="wisden"/> Between the new-ball attacks of Lindwall, [[Keith Miller]] and Johnston every 55 overs, Toshack played the role of stifling England's scoring. In one match against [[Sussex County Cricket Club|Sussex]], his 17 overs yielded only three scoring shots. He finished the match bowling 32 overs while conceding 29&nbsp;runs.<ref name="wisden"/><ref>Fingleton, p. 79.</ref> At [[Bramall Lane]], [[Sheffield]], he recorded the best innings analysis of his first-class career, taking 7/81 from 40 consecutive overs,<ref name="p11">Pollard (1990), p. 11.</ref><ref>Fingleton, p. 194.</ref> bemusing the [[Yorkshire CCC|Yorkshire]] spectators with the his accent and distinctive "Ow Wizz Ee" [[appeal (cricket)|appealing]]. Bradman considered his 6/51 against the [[Marylebone Cricket Club]] at [[Lord's]] as the best performance of all.<ref name="wisden"/><ref name="obit"/> He removed the leading English batsmen [[Len Hutton]] and [[Denis Compton]], as well as [[Martin Donnelly (cricketer)|Martin Donnelly]] and [[Ken Cranston]].<ref name=ow/> In particular, Toshack was involved in an extended battle with Compton before dismissing him; Bradman said that their duel was "worth going a long way to see".<ref name=a/> This performance helped Australia to take an innings victory over a team that was virtually a full-strength England outfit and allowed Australia to take a psychological victory in a dress rehearsal ahead of the Tests.<ref name=o/><ref>Perry, pp. 226–230.</ref><ref>Fingleton, p. 74.</ref>

Toshack's performance in the [[First Test, 1948 Ashes series|First Test]] at [[Trent Bridge]] was a quiet one, taking a wicket in each innings.<ref name="testlist"/> He was involved in an aggressive final wicket partnership of 32 with Johnston, scoring 19&nbsp;runs, his best at Test level to date in just 18&nbsp;minutes.<ref>Fingleton, p. 110.</ref> Toshack's best Test performance was his 5/40 in the second innings of the [[Second Test, 1948 Ashes series|Second Test]] at Lord's when Miller was unable to bowl after being injured,<ref name="obit"/> including the wickets of [[Cyril Washbrook]], [[Bill Edrich]], captain Yardley and [[Alec Coxon]].<ref name=ow/> During this performance, he employed two short legs and a silly mid-off.<ref name="p11"/> He had a moderately successful [[Third Test, 1948 Ashes series|Third Test]], taking figures of 3/101 in the only Test that Australia did not win. His knee injury flared again in the [[Fourth Test, 1948 Ashes series|Fourth Test]] after taking an ineffective 1/112 in the first innings, he was unable to bowl in the second innings of an Australian win.<ref name=o/> He made a recovery and it was hoped that he would be able to play in the Fifth Test, but he injured again himself in the lead-up match against [[Lancashire CCC|Lancashire]].<ref name=a/> He was taken to London for cartilage surgery, ending his tour and his Test career.<ref name="testlist"/> An inept batsman with an [[batting average|average]] of 5.78 in first-class fixtures, Toshack managed a Test average of 51 on the 1948 tour after being out only once, behind only [[Arthur Morris]], [[Sid Barnes]], Bradman and [[Neil Harvey]]. The unbeaten 20 he managed in the Lord's Test was his best first-class score, made in an uninhibited tenth-wicket stand with Johnston.<ref name="wisden"/><ref name="obit"/> Due to the fragility of his knee, Toshack was used sparingly in the tour games, playing in only 11 of the 29 non-Test matches on the tour.<ref name=o/><ref>Pollard (1990), p. 19.</ref> Toshack totalled 50 wickets at the average of 21.12 for the tour.<ref name=o/>

The knee injury prevented Toshack from playing during the 1948–1949 Australian domestic season.<ref name=az/><ref name=o/><ref>{{cite web|url=http://www.cricinfo.com/db/ARCHIVE/1940S/1948-49/AUS_LOCAL/STATS/FC_1948-49_BOWL_MOST_WKTS.html|publisher=[[Cricinfo]]|accessdate=2007-11-28|title=Australian First-Class Season 1948/49: Bowling – Most Wickets}}</ref> The Australian team to tour South Africa in 1949–50 was named at the end of the previous season, and Toshack was omitted after a season on the sidelines.<ref>Perry, p. 262.</ref> At the start of the 1949–50 season, when his Test teammates were sailing across the Indian Ocean to South Africa,<ref>Perry, pp. 265–267.</ref> Toshack made a strong start to his first-class comeback.<ref name=o/> He took 4/41 and 5/59 in a Shield match against Queensland in Brisbane,<ref name=o/> removing [[Ken Mackay]] and [[Wally Grout]] twice,<ref name=ow/> helping to seal a close 15-run win.<ref name=o/> In the second match, against [[Western Warriors|Western Australia]], Toshack took 4/68 in the first innings before his injury resurfaced. New South Wales won the match despite Toshack's inability to bowl in the second innings.<ref name=o/> The injury cost Toshack dearly; it forced him to retire from first-class cricket and cost him a Test recall.<ref name=az/> Toshack had been offered a position on the South African tour as a reinforcement for Johnston, who had been involved in a car crash. Instead, Miller took the position and played in all five Tests.<ref>Perry, pp. 266, 281.</ref>

==Style==
Bowling primarily from over the wicket, his accuracy, [[slower ball|changes of pace]], and movement in both directions, coupled with a [[leg theory|leg stump line]] to a packed leg-side field, made scoring off him difficult.<ref name="tel"/><ref name="az"/> He achieved his success in a manner not dissimilar to [[Derek Underwood]] a generation later. His accuracy and stamina allowed [[Ray Lindwall]] and [[Keith Miller]], one of Australia's finest fast bowling pairs of all time, to draw breath between short and incisive bursts of pace and [[swing bowling|swing]].<ref name="tel"/><ref name="az"/><ref name="wisden"/><ref name="obit"/> Standing {{convert|6|ft|2|in|cm|0}}, he was particularly effective on sticky wickets, reducing his speed to slow medium pace and using a repertoire of [[off cutter]]s, [[inswinger]]s, [[outswinger]]s and [[leg break]]s. Bowling a leg-stump line from over the wicket with a leg side cordon of two [[Fielding (cricket)#Fielding position names and locations|short legs]] and a [[Fielding (cricket)#Fielding position names and locations|silly mid-on]], he was described by Bradman as "unique in every way".<ref name="wisden"/><ref name="obit"/> Bradman further added "I cannot remember another of the same type...He worried and got out the best bats, was amazingly accurate and must have turned in fine figures had not his cartilage given way."<ref name=a/> He usually bowled with four men on the [[Fielding (cricket)#Off and leg side fields|off side]] including a [[Fielding (cricket)#Fielding position names and locations|slip]], and five on the leg.<ref name="p5"/> When the pitch was wet, he moved a further man to the [[leg side|on side]] to field at [[Fielding (cricket)#Fielding position names and locations|leg slip]].<ref name="p6">Pollard (1990), p. 6.</ref>

Nicknamed the "Black Prince" because of his tanned skin, Toshack's looks and sense of humour made him a crowd favourite, as did his theatrical [[appeal (cricket)|appeal]]ing, which was more reminiscent of later eras of cricketers.<ref name=a/> His vocal appealing prompted the journalist and former Australian Test batsman [[Jack Fingleton]] to dub him "The Voice", while teammate [[Sid Barnes]] called him "The film star" because of his looks.<ref name=a/> His sense of fun was often on show. While on the 1948 tour, he would often wear a bowler hat, grab a furled umbrella, and place a cigar in his mouth, parodying an Englishman.<ref name="tel"/><ref name="wisden"/><ref name="obit"/>

==After cricket==
Following his career, Toshack joined a firm of builders and spent 25 years as a foreman and supervisor on construction sites around Sydney.<ref name="az"/> He also wrote about cricket and enjoyed cultivating his vegetable garden in the northern Sydney suburb of [[Hornsby Heights, New South Wales|Hornsby Heights]]. Toshack died on 11 May 2003. He was survived by his wife Cathleen Hogan, whom he married in 1939, their only daughter, three granddaughters and two great-granddaughters.<ref name="tel">{{cite news| title=Ernie Toshack |url=http://www.telegraph.co.uk/news/main.jhtml?xml=/news/2003/05/13/db1302.xml|publisher=[[The Daily Telegraph]] | date=13 May 2003 |accessdate=2008-03-17}}</ref><ref name=a>Allen, pp. 89–90.</ref><ref name="wisden"/><ref name="obit"/><ref>{{cite news| title=Australia Test bowler Toshack passes away |date=12 May 2003 |accessdate=2008-03-18 |publisher =[[Rediff]] |url=http://in.rediff.com/cricket/2003/may/12roll.htm}}</ref>

==Test match performance==
{| class="wikitable" style="margin: 1em auto 1em auto" width="80%"
|-
!colspan=2| &nbsp;
!colspan=4| Batting<ref name="batsum">{{cite web
| url = http://stats.cricinfo.com/statsguru/engine/player/7960.html?class=1;template=results;type=batting
| title = Statsguru – ERH Toshack – Test matches – Batting analysis
| accessdate = 2008-03-18
| publisher = [[Cricinfo]]}}</ref>
!colspan=4| Bowling<ref name="bowlsum">{{cite web
| url = http://stats.cricinfo.com/statsguru/engine/player/7960.html?class=1;template=results;type=bowling
| accessdate = 2008-03-18
| title = Statsguru – ERH Toshack – Test matches – Bowling analysis
| publisher = [[Cricinfo]]}}</ref>
|-
! style="text-align:left;" | Opposition
!| Matches
!| Runs
!| Average
!| High Score
!| 100 / 50
!| Runs
!| Wickets
!| Average
!| Best (Inns)
|- style="text-align:right;"
| style="text-align:left;" | [[England cricket team|England]]
|| 9
|| 65
|| 16.25
|| 20[[not out (cricket)|*]]
|| 0/0
|| 801
|| 28
|| 28.60
|| 6/82
|- style="text-align:right;"
| style="text-align:left;" | [[India national cricket team|India]]
|| 2
|| 8
|| 8.00
|| 8
|| 0/0
|| 170
|| 13
|| 13.07
|| 6/29
|- style="text-align:right;"
| style="text-align:left;" | [[New Zealand national cricket team|New Zealand]]
|| 1
|| –
|| –
|| –
|| 0/0
|| 18
|| 6
|| 3.00
|| 4/12
|- style="text-align:right; border-top:solid 2px grey;"
| style="text-align:left;" | Overall
|| 12
|| 73
|| 14.60
|| 20*
|| 0/0
|| 989
|| 47
|| 21.04
|| 6/29
|}

==References==
{{reflist|30em}}

===Bibliography===
* {{cite book| last=Fingleton |first=Jack |authorlink=Jack Fingleton |title=Brightly fades the Don |year=1949 |publisher=Collins |oclc= 2943894 |location=London}}
* {{cite book|last=Perry|first=Roland| authorlink=Roland Perry| title=Miller's Luck: the life and loves of Keith Miller, Australia's greatest all-rounder|year=2005|publisher=Random House|location=Milsons Point, New South Wales|isbn=978-1-74166-222-1}}
* {{cite book|first=Jack |last=Pollard |authorlink=Jack Pollard|year=1988 |publisher=[[Harper Collins]] |title=The Bradman Years: Australian Cricket 1918–48 |isbn=0-207-15596-8 |location=North Ryde, New South Wales}}
* {{cite book|first=Jack |last=Pollard |authorlink=Jack Pollard|year=1990 |publisher=[[Harper Collins]] |title=From Bradman to Border: Australian Cricket 1948–89 |isbn=0-207-16124-0|location=North Ryde, New South Wales}}
* {{cite book |last=Piesse |first=Ken |authorlink=Ken Piesse |year=2003 |title=Cricket's Colosseum: 125 Years of Test Cricket at the MCG |location=South Yarra, Victoria| publisher=Hardie Grant Books |isbn=1-74066-064-1}}

==External links==
* {{Cricinfo|ref=ci/content/player/7960.html}}

{{The Invincibles squad}}

{{DEFAULTSORT:Toshack, Ernie}}
[[Category:1914 births]]
[[Category:2003 deaths]]
[[Category:Australia Test cricketers]]
[[Category:New South Wales cricketers]]
[[Category:The Invincibles (cricket)]]
[[Category:Cricketers from New South Wales]]