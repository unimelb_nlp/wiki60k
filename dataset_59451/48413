{{update|date=January 2015}}
{{Use mdy dates|date=November 2014}}
{{Infobox officeholder
|name        = Maura Healey
|image       = Maura Healey.jpg
|office      = 59th [[Massachusetts Attorney General|Attorney General of Massachusetts]]
|governor    = [[Charlie Baker]]
|term_start  = January 21, 2015
|term_end    = 
|predecessor = [[Martha Coakley]]
|successor   = 
|birth_date  = {{birth date and age|1971|2|8}}
|birth_place = [[Hampton Falls, New Hampshire|Hampton Falls]], [[New Hampshire]], [[United States|U.S.]]
|death_date  = 
|death_place = 
|party       = [[Democratic Party (United States)|Democratic]]
|partner     = [[Gabrielle Wolohojian]]
|education   = [[Harvard University]] {{small|([[Bachelor of Arts|BA]])}}<br>[[Northeastern University]] {{small|([[Juris Doctor|JD]])}}
|website     = {{url|maurahealey.com|Official website}}
}}
'''Maura T. Healey''' (born February 8, 1971) is an American attorney, a member of the [[Democratic Party (United States)|Democratic Party]] and the [[Massachusetts Attorney General|Attorney General]] of [[Massachusetts]].

Born in [[New Hampshire]], Healey graduated from [[Harvard College]] in 1992. She then spent two years playing professional basketball in Austria before returning to the United States and receiving a [[Juris Doctor]] degree from the [[Northeastern University School of Law]], in 1998. After clerking for federal judge [[A. David Mazzone]], she worked in private practice for seven years, also serving as a special [[assistant district attorney]] in [[Middlesex County, Massachusetts|Middlesex County]].

Hired by [[Massachusetts Attorney General]] [[Martha Coakley]] in 2007, Healey served as Chief of the Civil Rights Division, where she spearheaded the state's challenge to the federal [[Defense of Marriage Act]]. She was then appointed Chief of the Public Protection & Advocacy Bureau and then Chief of the Business and Labor Bureau before resigning in 2013 to run for Attorney General in the [[Massachusetts general election, 2014#Attorney General|2014 election]] as Coakley [[Massachusetts gubernatorial election, 2014|ran for Governor]]. She defeated former State Senator [[Warren Tolman]] in the Democratic primary and then defeated Republican attorney John Miller in the general election, thus becoming the first [[openly gay]] [[state attorney general]] elected in America.

==Early life and education==
Healey grew up as the oldest of five brothers and sisters. Her mother was a nurse at [[Lincoln Akerman School]] in [[Hampton Falls]], while her father was captain in the Navy and an engineer, and her stepfather, Edward Beattie, taught history and coached girl's sports at Winnacunnet high school. Her family roots are in [[Newburyport, Massachusetts|Newburyport]] and the North Shore area.

Healey attended [[Winnacunnet High School]] and majored in government at [[Harvard College]], graduating ''cum laude'' in 1992. She was co-captain of the [[Harvard Crimson|Harvard]] basketball team.<ref>{{cite web|url=http://www.thecrimson.com/article/1992/3/14/star-still-rising-for-w-cagers/?page=single |title=Star Still Rising for W. Cagers' Captain Maura Healey |publisher=''The Harvard Crimson'' |date=March 14, 1992 |first=Justin R.P. |last=Ingersoll |accessdate=November 5, 2014 }}</ref> After graduation, Healey spent two years playing as a starting point guard for a professional basketball team in Austria, UBBC Wustenrot Salzburg.<ref name="probasketball2014">{{cite web|url=http://www.boston.com/news/local/massachusetts/2014/08/26/pro-basketball-star-turned-attorney-general-hopeful-maura-healey-can-still-ball/Pa93onwxTj87DNc2jtOsoK/story.html |title=Pro Basketball Star-Turned-Attorney General Hopeful Maura Healey Can Still Ball |publisher=Boston.com |date=August 26, 2014 |first=Eric |last=Levenson |accessdate=November 5, 2014 }}</ref> Upon returning to the United States, Healey obtained her [[Juris Doctor|J.D.]] from [[Northeastern University School of Law]] in 1998.<ref name="mass"/>

==Career==
Healey began her legal career by clerking for Judge [[A. David Mazzone]] of the [[United States District Court for the District of Massachusetts]], where she prepared monthly compliance reports on the cleanup of the [[Boston Harbor]] and assisted the judge with trials, hearings and case conferences. Healey subsequently spent more than 7 years at the law firm [[Wilmer Cutler Pickering Hale and Dorr LLP]], where she worked as an associate and then junior partner. While at Wilmer Cutler Pickering Hale and Dorr, Healey’s focus was on commercial and securities litigation.<ref name=globe/>

She also served as a special assistant district attorney in [[Middlesex County, Massachusetts|Middlesex County]], where she tried drug, assault, domestic violence and motor vehicle cases in bench and jury sessions and argued bail hearings, motions to suppress, and probation violations and surrenders.<ref name=globe/>

Hired by Massachusetts Attorney General [[Martha Coakley]] in 2007, Healey served as Chief of the Civil Rights Division, where she spearheaded the state's challenge to the federal [[Defense of Marriage Act]]. She led the winning arguments for Massachusetts in America’s first lawsuit striking down the law.<ref>{{cite web|url=http://www.advocate.com/politics/politicians/2014/09/07/massachusetts-maura-healey-could-be-top-lgbt-attorney-country|title=Massachusetts: Maura Healey Could Be Top LGBT Attorney In The Country|work=Advocate.com|accessdate=November 5, 2014}}</ref>

In 2012, she was promoted to Chief of the Public Protection & Advocacy Bureau.<ref>{{cite web|url=http://www.mass.gov/ago/news-and-updates/press-releases/2012/2012-02-16-barry-smith-healey-kahn-appointments.html|title=AG Coakley Appoints New Leadership to Office |publisher=mass.gov |date=February 16, 2012 |accessdate=November 11, 2014}}</ref> She was then appointed Chief of the Business and Labor Bureau.<ref name="wbur">{{cite web|title=Coakley Aide Announces Run For Mass. Attorney General|url=http://www.wbur.org/2013/10/21/maura-healey-attorney-general-democrat|publisher=WBUR|accessdate=March 7, 2014|author=Associated Press}}</ref>

As a division chief and bureau head in the Attorney General's Office, Healey oversaw 250 lawyers and staff members and supervised the areas of consumer protection, fair labor, ratepayer advocacy, environmental protection, health care, insurance and financial services, civil rights, antitrust, Medicaid fraud, not-for-profit organizations and charities, and business, technology and economic development.<ref name=globe>{{cite web|title=Martha Coakley aide seeks her post|url=http://www.bostonglobe.com/metro/2013/10/20/top-aide-martha-coakley-run-for-attorney-general/etPQf9kriW7GjPrL2TxFTN/story.html|accessdate=April 8, 2014}}</ref><ref name="wbur"/>

==Massachusetts Attorney General==

===2014 election===
{{main|Massachusetts general election, 2014#Attorney General}}
In October 2013, Healey announced her intention to run for Attorney General. Coakley was retiring from the office to [[Massachusetts gubernatorial election, 2014|run for Governor]]. On September 9, 2014, Healey won the Democratic primary by 126,420 votes, defeating former State Senator [[Warren Tolman]] by 62.4% to 37.6%<ref>{{cite news|last1=Scharfenberg|first1=David|title=Healey defeats Tolman in Democratic AG primary|url=http://www.bostonglobe.com/metro/2014/09/09/maura-healey-warren-tolman-await-results-democratic-attorney-general-primary/MIr1BRhShV1sJP1eKt7bXK/story.html|accessdate=October 7, 2014|issue=September 9, 2014|publisher=Boston Globe}}</ref>

Healey's campaign was endorsed by State Senators Stan Rosenberg, Dan Wolf, Jamie Eldridge and America's largest resource for pro-choice women in politics, [[EMILY's List]].<ref>{{cite web|last=Rizzuto|first=Robert|title=Attorney general hopeful Maura Healey lands endorsements from Rosenberg, Dan Wolf, Jamie Eldridge |url=http://www.masslive.com/politics/index.ssf/2014/03/attorney_general_hopeful_maura.html|publisher=MassLive|accessdate=March 6, 2014}}</ref><ref>{{cite web|last=Bernstein|first=David|title=EMILY’s List Is Endorsing Maura Healey and Deb Goldberg|url=http://www.bostonmagazine.com/news/blog/2014/03/05/emilys-list-endorses-maura-healey-and-deb-goldberg/|publisher=Boston Daily|accessdate=March 7, 2014}}</ref> Her campaign has also been endorsed by Northeast District Attorney David Sullivan, [[Holyoke, Massachusetts|Holyoke]] Mayor Alex Morse, [[Fitchburg, Massachusetts|Fitchburg]] Mayor Lisa Wong and [[Northampton, Massachusetts|Northampton]] Mayor David Narkewicz.<ref>{{cite web|url=http://www.sentinelandenterprise.com/breakingnews/ci_25748117/fitchburg-mayor-endorses-maura-healey-attorney-general|title=Fitchburg mayor endorses Maura Healey for attorney general (video)|publisher=|accessdate=November 5, 2014}}</ref><ref>{{cite web|url=http://www.masslive.com/politics/index.ssf/2014/05/attorney_general_hopeful_maura_2.html|title=Attorney General hopeful Maura Healey lands endorsements from 2 Western Mass. mayors, discusses plan to tackle opiate abuse|work=masslive.com|accessdate=November 5, 2014}}</ref> Organizations that have endorsed the campaign include, Planned Parenthood Advocacy Fund of Massachusetts, [[MassEquality]] and the [[Gay & Lesbian Victory Fund|Victory Fund]].<ref>{{cite web|title=Northwestern District Attorney David Sullivan endorses Maura Healey for attorney general|url=http://www.gazettenet.com/news/townbytown/northampton/9722957-95/northwestern-district-attorney-david-sullivan-endorses-maura-healey-for-attorney-general|accessdate=February 7, 2014}}</ref><ref>{{cite web|url=http://www.masslive.com/politics/index.ssf/2014/05/candidates_for_attorney_genera.html|title=Warren Tolman and Maura Healey, Democratic candidates for attorney general, announce dueling endorsements to start week|work=masslive.com|accessdate=November 5, 2014}}</ref><ref>{{cite web|title=Bay Windows: Healey Wins Endorsement of The Victory Fund, MassEquality Political Action Committee|url=http://www.maurahealey.com/news/post/2014-01-bay-windows-healey-wins-endorsement-of-the-victory-f|accessdate=February 7, 2014}}</ref> Healey penned an OpEd in the ''Worcester Telegram and Gazette'' on upholding the Massachusetts buffer zone law, which she worked on while in the Attorney General’s Office.<ref name="mass">{{cite web|last=Schoenberg|first=Shira|title=Massachusetts Attorney General candidate Maura Healey says experience in AG's office prepared her for the top job|url=http://www.masslive.com/politics/index.ssf/2013/10/massachusetts_attorney_general_4.html|accessdate=March 7, 2014}}</ref> She also authored an OpEd in the Boston Globe outlining her plan to combat student loan predators.<ref>{{cite web|last=Healey|first=Maura|title=Stopping student loan predators|url=http://www.bostonglobe.com/opinion/2014/02/20/podium-forprofit/pASUrWlOZBKVKQQY50wYlL/story.html|publisher=Boston Globe|accessdate=March 7, 2014}}</ref><ref>{{cite web|title=Mass. AG hopeful Maura Healey calls for tougher oversight of for-profit colleges|url=http://www.therepublic.com/view/story/f118f848095d49f19d31de1478a3b38f/MA--Massachusetts-Attorney-General|publisher=Associated Press|accessdate=March 7, 2014}}</ref><ref>{{cite web|title=Mass. AG hopeful: Crack down on for-profit schools|url=http://www.washingtontimes.com/news/2014/feb/21/mass-ag-hopeful-crack-down-on-for-profit-schools/|publisher=The Washington Times|accessdate=March 7, 2014|author=Associated Press}}</ref>

She faced Republican nominee John Miller, an attorney, in the general election, and defeated him by 62.5% to 37.5% and thus became the first openly gay state attorney general elected in America.<ref>{{cite web|url=http://www.advocate.com/politics/election/2014/11/04/results-healey-elected-first-out-state-attorney-general|title=RESULTS: Healey Elected First Out State Attorney General|work=Advocate.com|accessdate=November 5, 2014}}</ref><ref>{{cite web|url=http://www.myfoxboston.com/story/27277432/democrat-maura-healey-tops-gops-miller-to-become-the-nations-1st-openly-gay-attorney-general|title=Democrat Maura Healey tops GOP's Miller to become the nation's 1st openly gay attorney general|work=My Fox Boston|date=November 5, 2014|accessdate=November 17, 2014}}</ref>

===Positions===
Healey’s plan to reduce gun violence seeks to address what she perceives as the root causes of violence. The plan includes enhancing the background check system to include information regarding recent restraining orders, pending indictments, any relations to domestic violence, parole and probation information. The plan also seeks to better track stolen and missing guns. Healey advocates for the incorporation of fingerprint trigger locks and firearm micro-stamping on all guns sold in Massachusetts.<ref name=MassLive>{{cite web|title=Attorney general candidate Maura Healey proposes stricter gun laws for Massachusetts in new plan|url=http://www.masslive.com/news/boston/index.ssf/2014/03/healey_ag_guns.html|accessdate=April 8, 2014}}</ref><ref name=Nasho>{{cite web|title=AG candidate outlines approach to gun violence|url=http://www.nashobapublishing.com/ci_25422542/ag-candidate-outlines-approach-gun-violence|accessdate=April 8, 2014}}</ref>

Healey's plan for criminal justice reform includes ending mandatory sentences for non-violent drug offenders and focusing on treatment rather than incarceration.<ref>{{cite web|title=Democrat Maura Healey says ending mandatory sentences for non-violent drug offenders, focusing on treatment over incarceration among priorities as attorney general|url=http://www.masslive.com/politics/index.ssf/2014/05/democrat_maura_healey_says_end.html|accessdate=May 29, 2014}}</ref>

Healey also plans to combat prescription drug abuse and the heroin epidemic in Massachusetts by implementing a “lock-in” program. The program will be carried out in pharmacies as a way to identify and track prescription drug abusers and/or distributors. Her plan also includes deployment of new resources to drug trafficking hotspots, improvement of treatment accessibility and expanding access to Narcan.<ref>{{cite web|title=Prescription Drug Abuse Reaches Epidemic Proportions|url=http://www.wggb.com/2014/05/06/prescription-drug-abuse-reaches-epidemic-proportions/|accessdate=May 29, 2014}}</ref>

====Abortion====
Healey’s women’s rights platform focuses on sex education, expanding access to abortion services in Massachusetts and ensuring that every woman in Massachusetts has access to abortion regardless of where she lives, her occupation or her income.<ref>{{cite web|title=Democratic attorney general hopeful Maura Healey says women's rights platform includes focusing on sex education, expanding access to abortion services in Massachusetts|url=http://www.masslive.com/politics/index.ssf/2014/05/democratic_attorney_general_ho.html|accessdate=May 29, 2014}}</ref>


====Gun control====
On July 20, 2016, Healey announced her intention to ban the sale or transfer of some semi-automatic rifles inside the Commonwealth of Massachusetts.<ref>{{cite web|url=http://www.mass.gov/ago/public-safety/awbe.html|title=Assault Weapons Ban Enforcement|date=July 19, 2016|publisher=}}</ref>

==Personal life==
She is openly gay, and lives in [[Charlestown, Massachusetts]] with her partner, [[Gabrielle Wolohojian]].<ref name="globe"/><ref>{{cite news|author= |url=http://www.huffingtonpost.com/charlotte-robinson/maura-healey_b_4947975.html |title=Maura Healey Talks Historic Campaign for Attorney General in Massachusetts |publisher=Huffingtonpost.com |date=March 13, 2014 |accessdate=June 29, 2014}}</ref>

== Electoral history ==
{| class="wikitable"
! colspan="4" |Massachusetts Attorney General Democratic Primary Election, 2014
|-
|'''Party'''
|'''Candidate'''
|'''Votes'''
|'''%'''
|-
|Democratic
|'''Maura Healey'''
|322,380
|62.1
|-
|Democratic
|Warren Tolman
|195,654
|37.7
|-
|Democratic
|Write-ins
|721
|0.1
|}
{| class="wikitable"
! colspan="4" |Massachusetts Attorney General Election, 2014
|-
|'''Party'''
|'''Candidates'''
|'''Votes'''
|'''%'''
|-
|Democratic
|'''Maura Healey'''
|1,280,513
|61.7
|-
|Republican
|John Miller
|793,821
|38.2
|-
|Write-ins
|Write-ins
|1,885
|0.1
|}

== References ==
{{reflist|30em}}

{{s-start}}
{{s-legal}}
{{s-bef|before=[[Martha Coakley]]}}
{{s-ttl|title=[[Massachusetts Attorney General|Attorney General of Massachusetts]]|years=2015–present}}
{{s-inc}}
{{s-end}}

{{Current Massachusetts statewide political officials}}
{{U.S. state attorneys general}}

{{DEFAULTSORT:Healey, Maura}}
[[Category:1971 births]]
[[Category:20th-century American lawyers]]
[[Category:21st-century American lawyers]]
[[Category:21st-century American politicians]]
[[Category:21st-century women politicians]]
[[Category:American expatriate basketball people in Austria]]
[[Category:American women lawyers]]
[[Category:American women's basketball players]]
[[Category:Basketball players from Massachusetts]]
[[Category:Harvard Crimson women's basketball players]]
[[Category:Harvard University alumni]]
[[Category:Lesbian politicians]]
[[Category:LGBT law enforcement workers]]
[[Category:LGBT lawyers]]
[[Category:LGBT people from Massachusetts]]
[[Category:LGBT people from New Hampshire]]
[[Category:LGBT politicians from the United States]]
[[Category:Living people]]
[[Category:Massachusetts Attorneys General]]
[[Category:Massachusetts Democrats]]
[[Category:Massachusetts lawyers]]
[[Category:Northeastern University School of Law alumni]]
[[Category:People from Charlestown, Boston]]
[[Category:People from Hampton Falls, New Hampshire]]
[[Category:Point guards]]
[[Category:Women in Massachusetts politics]]