{{Infobox journal
| cover = [[File:Jat cover new.gif]]
| discipline = [[Toxicology]]
| abbreviation = J. Appl. Toxicol.
| editor = Philip W. Harvey
| publisher = [[John Wiley & Sons]]
| country = 
| history = 1981-present
| frequency = monthly
| impact = 2.982
| impact-year = 2014
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1099-1263
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1099-1263/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1099-1263/issues
| link2-name = Online archive
| ISSN = 0260-437X
| eISSN = 1099-1263
| CODEN = JJATDK
| LCCN = 83644016
| OCLC = 07567999
}}
The '''''Journal of Applied Toxicology''''' is a monthly [[peer review|peer-reviewed]] [[scientific journal]] published since 1981 by [[John Wiley & Sons]]. It covers all aspects of [[toxicology]] and publishes reviews and research articles on mechanistic, fundamental, and applied research relating to the [[toxicity]] of drugs and chemicals at the molecular, cellular, tissue, target organ, and whole body level, both ''in vivo'' (by all routes of exposure) and ''in vitro/ex vivo''.

The current [[editor-in-chief]] is [[Philip W. Harvey]] (Covance Laboratories).<ref name="onlinelibrary">[http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1099-1263/homepage/EditorialBoard.html Editorial Board]</ref>

== Most cited papers ==
The journal's three most-cited papers (>130 citations) are:
# 'Review of oximes available for treatment of nerve agent poisoning', Volume 14, Issue 5, Sep-Oct 1994, Pages: 317-331, Dawson RM
# 'Chronic effects on the respiratory-tract of hamsters, mice and rats after long-term inhalation of high-concentrations of filtered and unfiltered diesel-engine emissions', Volume 6, Issue 6, Dec 1986, Pages: 383-395, Heinrich U, Muhle H and Takenaka S, et al.
# 'Development of a reconstituted water medium and preliminary validation of the Frog Embryo Teratogenesis Assay Xenopus (FETAX)', Volume 7, Issue 4, Aug 1987, Pages: 237-244, Dawson DA and Bantle JA

== Abstracting and indexing ==
The ''Journal of Applied Toxicology'' is abstracted and indexed in  [[Chemical Abstracts Service]], [[Scopus]], and the [[Science Citation Index Expanded]].<ref name="onlinelibrary"/> The 2014 [[impact factor]] is 2.982. It is ranked 27th out of 87 journals in the category "Toxicology".<ref>Institute for Scientific Information, ''Journal Citation Reports'', 2015</ref>

== References ==
<references />

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1099-1263}}

[[Category:Toxicology journals]]
[[Category:John Wiley & Sons academic journals]]
[[Category:Publications established in 1981]]
[[Category:English-language journals]]
[[Category:Monthly journals]]