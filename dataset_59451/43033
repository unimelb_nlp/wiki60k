{{other uses of|Joint Strike Fighter}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox aircraft begin
  |name = Joint Strike Fighter (JSF)
  |image = File:Boeing X-32B Patuxent.jpg
  |caption = X-32B at [[Naval Air Station Patuxent River|Patuxent River Naval Air Museum]]
}}{{Infobox aircraft program
  |aim = Strike Fighter 
  |requirement =
  |issuer = Multiple services
  |service =
  |value =
  |initiated =
  |proposals =
  |prototypes = [[Boeing X-32]] <br> [[Lockheed Martin X-35]]
  |concluded =
  |outcome = X-35 selected for production as [[Lockheed Martin F-35 Lightning II|F-35 Lightning II]]
  |predecessors = [[Common Affordable Lightweight Fighter]] (CALF) <br> Joint Advanced Strike Technology
  |successors = 
  |related =
}}
|}

'''Joint Strike Fighter''' ('''JSF''') is a development and acquisition program intended to replace a wide range of existing [[Fighter aircraft|fighter]], [[strike fighter|strike]], and [[ground attack aircraft]] for the United States, the United Kingdom, Turkey, Italy, Canada, Australia, the Netherlands and their allies. After a competition between the [[Boeing X-32]] and the [[Lockheed Martin X-35]], a final design was chosen based on the X-35. This is the [[F-35 Lightning II]], which will replace various tactical aircraft, including the US [[F-16 Fighting Falcon|F-16]], [[A-10 Thunderbolt II|A-10]], [[F/A-18 Hornet|F/A-18]], [[AV-8 Harrier II|AV-8B]] and British [[BAE Harrier II|Harrier GR7 & GR9]]s.  The projected average annual cost of this program is $12.5 billion with an estimated program life-cycle cost of $1.1 trillion.<ref name="GAO-12-437">{{cite web |url=http://gao.gov/assets/600/591608.pdf |title=JOINT STRIKE FIGHTER - DOD Actions Needed to Further Enhance Restructuring and Address Affordability Risks |publisher=United States Government Accountability Office |accessdate=1 August 2012}}</ref>

==Project formation==

The JSF program was the result of the merger of the [[Common Affordable Lightweight Fighter]] (CALF) and Joint Advanced Strike Technology (JAST) projects.<ref name="MartinBaker">[http://www.martin-baker.co.uk/getdoc/d25952ab-5881-4999-8593-6f7f196c8770/a_history_of_the_joint_strike_fighter_programme.aspx A history of the Joint Strike Fighter Program], Martin-Baker. Retrieved April 2011</ref><ref name="AFM">{{cite journal | first = Mark | last = Nicholls | title = JSF: The Ultimate Prize | work = [[Air Forces Monthly]] | publisher = Key Publishing | pages = 32–38 |date=August 2000 |accessdate = 2006-12-24}}</ref> The merged project continued under the JAST name until the engineering, manufacturing and development (EMD) phase, during which the project became the Joint Strike Fighter.<ref name="UKJAST">{{cite news | title = US, UK sign JAST agreement | work = Aerospace Daily | publisher = McGraw-Hill| page = 451| date = 1995-11-25| accessdate = 2006-12-24}}</ref>

The CALF was a [[DARPA]] program to develop a [[STOVL]] [[strike fighter]] (SSF) for the [[United States Marine Corps]] and replacement for the [[F-16 Fighting Falcon]]. The [[United States Air Force]] passed over the [[F-16 Agile Falcon]] in the late 1980s, essentially an enlarged F-16, and continued to mull other designs. In 1992, the Marine Corps and Air Force agreed to jointly develop the Common Affordable Lightweight Fighter, also known as Advanced Short Takeoff and Vertical Landing (ASTOVL). CALF project was chosen after [[Paul Bevilaqua]] persuaded<ref name="GovExec">Wilson, George C. "[http://www.govexec.com/dailyfed/0102/012202db.htm The engine that could]" ''GovExec'', January 22, 2002. Retrieved December 2009. [http://web.archive.org/web/20131019160442/http://www.govexec.com/defense/defense-beat/2002/01/the-engine-that-could/10890/ Archived] on 19 October 2013.</ref> the Air Force that his team's concept<ref name="LiftFanPatent1">[http://www.patentgenius.com/patent/5209428.html "Propulsion system for a vertical and short takeoff and landing aircraft"], United States Patent 5209428</ref> (if stripped of its [[Rolls-Royce LiftSystem|lift system]]) had potential as a complement to the [[Lockheed Martin F-22 Raptor|F-22 Raptor]].<ref name="GovExec"/> Thus, in a sense the [[F-35 Lightning II#F-35B|F-35B]] begat the [[F-35 Lightning II#F-35A|F-35A]], not the other way around.

The Joint Advanced Strike Technology (JAST) program was created in 1993, implementing one of the recommendations of a [[United States Department of Defense]] (DoD) "Bottom-Up Review to include the [[United States Navy]] in the Common Strike Fighter program."<ref>Bolkcom, Christopher. [http://www.dtic.mil/cgi-bin/GetTRDoc?Location=U2&doc=GetTRDoc.pdf&AD=ADA472773 JSF: Background, Status, and Issues] page CRS-2, ''dtic.mil'', 16 June 2003. Retrieved: 18 September 2010.</ref>  The review also led the [[The Pentagon|Pentagon]] to continue the F-22 Raptor and [[F/A-18E/F Super Hornet]] programs, cancel the Multi-Role Fighter (MRF) and the A/F-X programs, and curtail F-16 and [[F/A-18 Hornet#C/D|F/A-18C/D]] procurement.  The JAST program office was established on 27 January 1994 to develop [[aircraft]], [[weapon]]s, and [[radar|sensor technology]] with the aim of replacing several disparate US and UK aircraft with a single family of aircraft; the majority of those produced would replace F-16s. [[Merrill McPeak]], former Chief of Staff of the United States Air Force, has complained that [[Les Aspin]]'s decision to force all three services to use a single airframe greatly increased the costs and difficulty of the project.<ref>[http://www.washingtontimes.com/news/2013/mar/6/prices-soar-enthusiasm-dives-for-f-35-lightning/?page=3 "Prices soar, enthusiasm dives for F-35 Lightning."]</ref>

In November 1995, the [[United Kingdom]] signed a [[memorandum of understanding]] to become a formal partner, and agreed to pay $200 million, or 10% of the concept demonstration phase.<ref name="UKJAST"/>

In 1997, [[Canada]]'s Department of National Defence signed on to the Concept Demonstration phase with an investment of US$10 million. This investment allowed Canada to participate in the extensive and rigorous competitive process where [[Boeing]] and [[Lockheed Martin]] developed and competed their prototype aircraft.<ref>[http://news.gc.ca/web/article-eng.do?m=/index&nid=548059 ]. "Government of Canada"  16 July 2010 Retrieved: 26 July 2010</ref>

== JSF competition ==

Studies supporting JAST/JSF started in 1993 and led to STOVL submissions to the DOD by  [[McDonnell Douglas]],  [[Northrop Grumman]],  [[Lockheed Martin]] and  [[Boeing]]:<ref>https://www.flightglobal.com/pdfarchive/view/1995/1995%20-%200834.html</ref>

1)   McDonnell Douglas proposed an aircraft powered by a reheated turbofan, with a remote gas-driven fan to augment lift in the STOVL mode. Later, General Electric did a ground demonstration of this engine configuration.

2) The Northrop Grumman aircraft featured an auxiliary lift engine augmenting the dry thrust from a reheated turbofan fitted with a pair of thrust-vectoring nozzles.

3) The Lockheed Martin aircraft concept used a reheated turbofan with thrust augmentation from a remote shaft-driven lift fan. This engine configuration was to lead eventually to the [[F135]]-PW-600 which powers the  [[F-35B]] JSF production aircraft.

4) Boeing decided against thrust augmentation. They proposed an aircraft powered by a reheated turbofan that could be reconfigured (in the STOVL mode) into a direct lift engine with a pair of thrust-vectoring nozzles located near the aircraft centre-of-gravity. This led to the [[F119]]-PW-614S which powered the [[X-32]]B JSF demonstrator.
 
Two contracts to develop prototypes were awarded on November 16, 1996, one each to [[Lockheed Martin]] and [[Boeing]]. Each firm would produce two aircraft to demonstrate conventional takeoff and landing (CTOL), carrier takeoff and landing (CV version), and short takeoff and vertical landing ([[STOVL]]). [[McDonnell Douglas]]' [[McDonnell Douglas#1990.E2.80.931997|bid]] was rejected in part due to the complexity of its design.<ref>{{cite news| first = David | last = Fulghum|author2=Morrocco, John | title = Final JSF Competition Offers No Sure Bets | work = Aviation Week and Space Technology| publisher =McGraw-Hill | page = 20 | date = 1996-11-25| accessdate = 2006-12-23}}</ref> Lockheed Martin and Boeing were each given $750 million to develop their concept demonstrators and the definition of the Preferred Weapon System Concept (PWSC). The aim of this funding limit was to prevent one or both contractors from bankrupting themselves in an effort to win such an important contract.<ref name="AFM"/>

Also in 1996, the UK [[Ministry of Defence (United Kingdom)|Ministry of Defence]] launched the [[Joint Combat Aircraft|Future Carrier Borne Aircraft]] project. This program sought a replacement for the [[BAE Sea Harrier|Sea Harrier]] (and later the [[RAF Harrier II|Harrier GR7]]); the Joint Strike Fighter was selected in January 2001.

During concept definition, two Lockheed Martin airplanes were flight-tested: the X-35A (which was later converted into the X-35B), and the larger-winged X-35C.<ref>[http://www.jsf.mil/history/his_jsf.htm Joint Strike Fighter official site - History page]</ref>  Arguably the most persuasive demonstration of the X-35's capability was the final qualifying Joint Strike Fighter flight trials, in which the X-35B STOVL aircraft took off in less than 500 feet (150 m), went supersonic, and landed vertically&nbsp;– a feat that [[Boeing]]'s entry was unable to achieve.<ref name="Nova transcript X-planes">PBS: [http://www.pbs.org/wgbh/nova/transcripts/3004_xplanes.html Nova transcript "X-planes"]</ref> <!-- look for "Mission X" in the rather long Nova transcript -->

=== Outcome ===

The contract for System Development and Demonstration (SDD) was awarded on 26 October 2001 to Lockheed Martin,<ref>Bolkcom, Christopher. [http://www.dtic.mil/cgi-bin/GetTRDoc?Location=U2&doc=GetTRDoc.pdf&AD=ADA472773 JSF: Background, Status, and Issues] page CRS-4, ''dtic.mil'', 16 June 2003. Retrieved: 18 September 2010.</ref> whose X-35 beat the [[Boeing X-32]]. One of the main reasons for this choice appears to have been the method of achieving STOVL flight, with the Department of Defense judging that the higher performance lift fan system was worth the extra risk. When near to the ground, the Boeing X-32 suffered from the problem of hot air from the exhaust circulating back to the main engine, which caused the thrust to weaken and the engine to overheat.<ref name="Nova transcript X-planes"/>

The [[United States Department of Defense]] officials and [[William Bach]], the UK Minister of Defence Procurement, said the X-35 consistently outperformed the X-32, although both met or exceeded requirements.<ref name="Nova transcript X-planes"/> The development of the JSF will be jointly funded by the [[United States]], [[United Kingdom]], [[Italy]], the [[Netherlands]], [[Canada]], [[Turkey]], [[Australia]], [[Norway]] and [[Denmark]].

[[Lockheed Martin]]'s [[Lockheed Martin X-35|X-35]] would become the basis of the [[F-35 Lightning II]], currently in development.  On April 6, 2009 US Secretary of Defense [[Robert Gates]] announced that the US would buy a total of 2,443 JSFs.<ref>Gates, Dominic, "Aerospace Giant 'Hit Harder' Than Peers", ''[[Seattle Times]]'', April 7, 2009, p. 1.</ref>

==Program issues==

===Alleged Chinese espionage===
In April 2009, the [[Wall Street Journal]] reported that computer spies, allegedly Chinese but acknowledged to be from uncertain sources, had penetrated the database and acquired terabytes of secret information about the fighter, possibly compromising its future effectiveness.<ref>{{cite news |url=https://online.wsj.com/article/SB124027491029837401.html |author=Gorman S, Cole A, [[Yochi Dreazen|Dreazen Y]] |title=Computer Spies Breach Fighter-Jet Project Article |work=[[The Wall Street Journal]] |date=April 21, 2009}}</ref>

===Cost overruns===

On February 1, 2010, Secretary of Defense [[Robert M. Gates]] announced that, due to delays and other problems with the JSF development program, he was removing Major General David R. Heinz from command of the program and would withhold $614 million in bonuses from Lockheed Martin.<ref>Whitlock, Craig, "Gates To Major General: You're Fired", ''[[Washington Post]]'', February 2, 2010, p. 4.</ref>  On February 16, 2010, Deputy Defense Secretary Bill Lynn announced that the program will be delayed one year.<ref>Reed, John, "Pentagon Official Confirms 1-Year Delay For JSF", ''DefenseNews.com'', February 16, 2010.</ref> According to some estimates, overruns could increase the program's total costs to $388 billion, a 50% increase from the initial price tag.<ref name="Shachtman">Shachtman, Noah. [http://www.brookings.edu/opinions/2010/0715_airforce_shachtman.aspx "The Air Force Needs a Serious Upgrade"], [http://www.brookings.edu/ The Brookings Institution], 15 July 2010.</ref> Many of the program's financial and technical complications result from the Marine version of the JSF, capable of vertical take-offs and landings.<ref name="Shachtman" />

On 11 March 2010, [[United States Senate Committee on Armed Services]] investigated the progress of the JSF program in a meeting with [[The Pentagon|Pentagon]] officials, emphasizing cost due to the risk of a [[Nunn-McCurdy]] process.<ref name="auavi">[http://australianaviation.com.au/jsf-faces-us-senate-grilling/ JSF faces US Senate grilling], australianaviation.com.au 12 March 2010.</ref> According to the [[Government Accountability Office]], F-35A cost has risen from $50m in 2002, via $69m in 2007 to $74m in 2010, all measured in 2002 dollars.<ref name="auavi"/>

Canada reviewed their commitment to the project in December 2012, due to the cost overruns. The decision was made following a report by auditing firm [[KPMG]] that showed that Canada’s purchase would cost C$45bn over 42 years. [[Rona Ambrose]], Canada’s public works minister said: “We have hit the reset button and are taking the time to do a complete assessment of all available aircraft.” <ref>{{cite web|title=Canada puts JSF on ice |url=http://www.ft.com/cms/s/0/820a1384-453a-11e2-858f-00144feabdc0.html#axzz2cNivlX1u|work=|publisher=Financial Times|date=December 13, 2012 |accessdate= 2013-08-18}}</ref> Defence Minister [[Peter MacKay]] announced Canada’s plan to buy the F-35 in 2010 saying that the purchase price was $9 billion, but did not provide operating cost estimates. During an election campaign in 2011, the Conservatives declared that the total cost over 20 years would be $16 billion.<ref>{{cite web|title=Canada Reviews Plans to Buy F-35 Fighter Jets|url=https://www.nytimes.com/2012/12/13/business/high-cost-leads-canada-to-study-plans-to-buy-f-35s.html?_r=0|work=|publisher=New York Times|date=December 12, 2012 |accessdate= 2013-08-18}}</ref>

===Performance concerns===
Concerns about the F-35's performance have resulted partially from reports of simulations by [[RAND Corporation]] in which three regiments of Russian [[Sukhoi]] fighters defeat six F-22s by denying tanker refueling.<ref name="F-35 under attack">Trimble, Stephen. [http://www.flightglobal.com/news/articles/us-defence-policy-and-f-35-under-attack-317309/ "US defence policy – and F-35 – under attack."] ''Flight International'', Reed Business Information, 15 October 2008.</ref>

As a result of these media reports, then Australian defence minister [[Joel Fitzgibbon]] requested a formal briefing from the [[Department of Defence (Australia)|Australian Department of Defence]] on the simulation. This briefing stated that the reports of the simulation were inaccurate and that it did not compare the F-35's performance against that of other aircraft.<ref name="Fighter criticism">[http://www.abc.net.au/news/stories/2008/09/25/2373632.htm "Fighter criticism 'unfair' and 'misrepresented'."] ''ABC News'', 25 September 2008. Retrieved: 30 October 2008.</ref><ref>Wolf, Jim. [http://www.reuters.com/article/politicsNews/idUSN1925736420070920?sp=true "Air Force chief links F-35 fighter jet to China."] ''reuters.com'', 19 September 2007. Retrieved: 3 July 2010.</ref>

Andrew Hoehn, Director of RAND Project Air Force, made the following statement: "Recently, articles have appeared in the Australian press with assertions regarding a war game in which analysts from the RAND Corporation were involved. Those reports are not accurate. RAND did not present any analysis at the war game relating to the performance of the F-35 Joint Strike Fighter, nor did the game attempt detailed adjudication of air-to-air combat. Neither the game nor the assessments by RAND in support of the game undertook any comparison of the fighting qualities of particular fighter aircraft."<ref>Hoehn, Andrew. [https://www.rand.org/news/press/2008/09/25.html "Statement Regarding Media Coverage of F-35 Joint Strike Fighter."] ''RAND'', 12 October 2010. Retrieved 17 May 2012.</ref>

Furthermore, Maj. Richard Koch, chief of USAF Air Combat Command's advanced air dominance branch is reported to have said that "I wake up in a cold sweat at the thought of the F-35 going in with only two air-dominance weapons" with an ''Aviation Week'' article casting an extremely skeptical eye over the (USAF) source of claims that the F-35 would be "400% more effective" than projected opponents.<ref>Sweetman, Bill. [http://www.aviationweek.com/aw/blogs/defense/index.jsp?plckController=Blog&plckScript=blogScript&plckElementId=blogDest&plckBlogPage=BlogViewPost&plckPostId=Blog%3a27ec4a53-dcc8-42d0-bd3a-01329aef79a7Post%3ab1c3536a-8d96-481f-aef5-d6428ec6f9ca "JSF Leaders Back In The Fight."] ''aviationweek.com'', 22 September 2008. Retrieved: 3 July 2010.</ref>

The experience of the JSF program has led to a more conservative and open-ended [[Future Vertical Lift]] program.<ref name=syd>Sydney J. Freedberg Jr. "[http://breakingdefense.com/2014/06/future-vertical-lift-one-program-or-many/ Future Vertical Lift: One Program Or Many?]" ''Breaking Defense'', 5 June 2014. Accessed: 22 June 2014. [http://web.archive.org/web/20140606083642/http://breakingdefense.com/2014/06/future-vertical-lift-one-program-or-many/ Archived] on 6 June 2014.</ref>

==See also==
* [[Joint Combat Aircraft]]
* [[Advanced Tactical Fighter]]
* [[Joint Unmanned Combat Air Systems]]

==References==
{{Reflist|2}}

* Keijsper, Gerald. ''Lockheed F-35 Joint Strike Fighter''. Pen & Sword Aviation, 2007. ISBN 978-1-84415-631-3.
* Spick, Mike, ed. ''Great Book of Modern Warplanes''. MBI, 2000. ISBN 0-7603-0893-4.

==External links==
{{externalimage|align=left|images=[http://www.jsf.mil/gallery/gal_video.htm Official JSF program videos]}}
* [http://www.naval-technology.com/projects/f35-joint-strike-fighter/ F-35 Lightning II Joint Strike Fighter (JSF)]
* [http://www.pbs.org/wgbh/nova/xplanes/ "Battle of the X-Planes."] [[Emmy Award]]-winning ''[[Nova (TV series)|NOVA]]'' TV documentary, 4 February 2003.

{{Lockheed Martin F-35 Lightning II}}

[[Category:Lockheed Martin F-35 Lightning II]]
[[Category:Stealth aircraft]]
[[Category:Military aircraft procurement programs of the United States]]
[[Category:Collier Trophy recipients]]