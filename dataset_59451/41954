[[File:Pigeon fancier in Greenock.jpg|thumb|right|A pigeon fancier with his racing pigeon]]

'''Pigeon keeping''' is the art and [[Animal husbandry|science]] of breeding [[domestic pigeon]]s.  People have practised  pigeon keeping for about 10,000 years in almost every part of the world.  In that time, mankind has substantially altered the [[Morphology (biology)|morphology]] and the behaviour of the [[domestication|domesticated]] descendants of the [[rock dove]] to suit his needs for food, [[aesthetics|aesthetic satisfaction]] and entertainment.

People who [[breeder|breed]] pigeons are commonly referred to as pigeon [[Animal fancy|fanciers]].<ref name="Levi">Wendell (1977) 1</ref>  The hobby is gaining in popularity in the United States, after having waned within the last 50 years.<ref>{{cite web| last =Allmendinger | first =Lisa | title =Pigeons gain in popularity as pets
| publisher =The Ann Arbor News | url =http://blog.mlive.com/annarbornews/2008/06/pigeons_gain_in_popularity_as.html
| date=1 June 2008 | accessdate =2008-06-02 }}</ref> Both the hobby and commercial aspects of keeping pigeons are thriving in other parts of the world.

==Types of pigeons kept==
The [[rock dove]], which is generally believed to be the ancestor of domesticated pigeons,<ref>Wendell (1977) 37</ref> was probably domesticated around ten thousand years ago.<ref>{{cite book
  | last =Blechman | first =Andrew | title =Pigeons-The fascinating saga of the world's most revered and reviled bird. | publisher =University of Queensland Press | year =2007 | location =St Lucia, Queensland  | url =http://www.uqp.uq.edu.au/book_details.php?isbn=9780702236419
  | isbn =978-0-7022-3641-9 | page = 10 }}</ref> There are hundreds of [[breed]]s of domesticated pigeons arising from this common ancestor which are currently cultivated by pigeon fanciers.<ref>{{cite book |last=Levi |first=Wendell |title=Encyclopaedia of Pigeon Breeds|year= 1965|publisher= Levi Publishing Co, Inc|location= Sumter, S.C.|isbn=0-910876-02-9}}</ref>  Because of the large number of domesticated breeds, pigeon fanciers find it convenient for certain purposes to group sets of domesticated pigeons into larger groups.

In the United States and United Kingdom, there are three major recognized groups of breeds<ref>{{cite book | last = Hiatt | first = Shannon | first2 = Jon | last2 = Esposito, DVM | title = The Pigeon Guide | publisher = Silvio Mattacchione and Company | year =2000  |location =Port Perry, Ontario  | isbn = 1-895270-18-9 | page = 18}}</ref> of domesticated pigeons:
* Flying/Sporting
* Fancy
* Utility

It is worth noting that pigeon fanciers in other nations use different schemes in grouping domesticated pigeons; for example, a nationwide pigeon organization in Germany <ref>{{cite web |language = German |title = VDT Online |url= http://www.vdt-online.de/main/index.html|accessdate=2008-04-18}}</ref> uses a far different grouping scheme.<ref>{{cite web |language = German |title = VDT Online |url= http://www.vdt-online.de/seite3/index3.html|accessdate=2008-04-18}}</ref> The Australian grouping system is similar to the UK/European groupings. For example, see: [[Fancy pigeon#Major breed families of fancy pigeon|Major breed families of fancy pigeon]].

===Flying/Sporting pigeons===
{{main article|Flying/Sporting pigeons|Pigeon racing}}
These pigeons are kept and bred for their aerial performance and for reproduction. [[Racing homer]]s are a type of [[Homing pigeon]], trained to participate in the sport of [[pigeon racing]], and have been used to carry messages during times of war. Such races often have large cash prizes of up to $1 million as the Sun City Million Dollar Pigeon Race. Fanciers who fly racing pigeons sometimes win long-distance races and even break records.<ref>{{cite web| last =Crighton | first =Ryan | title =Lifelong dream comes true for pigeon fancier | publisher =The Press and Journal
| url =http://www.pressandjournal.co.uk/Article.aspx/738492?UserKey=0
| accessdate =2008-07-15 }}</ref>  Other flying/sporting pigeons are bred for unique flying characteristics other than homing, such as rolling, high-flying, and diving. These birds, which may be flown in competitions, include but are not limited to [[Roller (pigeon)|Rollers]], [[Tumbler (pigeon)|Tumblers]], and [[Tippler]]s.  It should also be noted that a few varieties, for example, the [[Parlor Roller]], are considered to be within the flying/sporting group even though they do not fly. This is because they compete on the basis of their performance and not their appearance.
Competitors in pigeon sporting competitions such as pigeon races can win large sums of prize money when their pigeons return home the fastest from a race.<ref>{{cite web| last =Judge | first =Chris | title =Rocky enjoys national win | publisher =Fenland Citizen | url =http://www.fenlandcitizen.co.uk/sport/Rocky-enjoys-national-win.4169335.jp | format =Web news
| accessdate =2008-06-13 }}</ref>
The use of pigeons to carry messages is commonly called [[pigeon post]]. Pigeons can also carry small light-weight packages, and have been used to smuggle drugs into a prison.<ref>{{cite web| last =Wingrove | first =Josh | title =Brazilian prisoners use pigeons to get high | publisher =GlobeLife (June 27, 2009)| url =http://www.theglobeandmail.com/servlet/story/RTGAM.20080627.wlpigeon27/BNStory/lifeMain/home?cid=al_gam_mostview
  | accessdate =2008-07-05 }}</ref>

===Fancy pigeons===
{{main article|Fancy pigeon}}
Fancy pigeons are pigeons which are specially bred to perpetuate particular features. Examples of fancy pigeons would include [[Jacobin (pigeon)|Jacobins]], [[Fantail (pigeon)|Fantails]] and [[Pigmy Pouter]]s.  Their owners compete them against each other at exhibitions or pigeon shows and judges decide who has the best by comparing them to each other and their respective [[breed standard]]. There are many breeds of fancy pigeons of all sizes, colors and types.<ref name="McClary">{{cite book |last=McClary |first=Douglas |title= Pigeons for Everyone|year= 1999|publisher= Winckley Press|location= Great Britain|isbn=0-907769-28-4 }}
</ref>

===Utility pigeons===
{{main article|Utility pigeon}}
[[File:King pigeons.jpg|thumb|A pair of White Kings]]
Utility pigeons are bred for their meat and as replacement breeding stock.  The meat of pigeons is customarily referred to as [[Squab (food)|squab]] and is considered a delicacy in many parts of the world. Examples of utility varieties include [[King (pigeon)|Kings]], several different varieties of [[French Mondain|Mondaines]] and [[Carneau]].
* All of the above [[List of pigeon breeds|pigeon breeds]] may also be exhibited in pigeon shows but true utility pigeons and flying/sporting pigeons are rarely exhibited.  This is because true utility pigeons are bred for meat and true flying/sporting pigeons are bred for their aerial performance so that in both cases appearance is usually a very minor consideration.

==Pigeon housing==
Houses for pigeons are generally called lofts.<ref>Wendell (1977) 507</ref> Pigeon houses are also sometimes referred to as "coops" although the word seems to have originally applied to the breeding pens inside the housing.<ref>Wendell (1977) 537</ref>  There are as many different kinds of enclosures used to house pigeons in as there are pigeon fanciers.  There are no real constraints on the design of housing for pigeons but there are some things that most fanciers find desirable.

These [[Dovecote|pigeon houses]] often contain specially constructed openings to allow the pigeon keeper to give his animals liberty for purposes of exercise while allowing them to re-enter the house without special assistance from the keeper.  At the same time these houses are constructed to keep the pigeons safe from predators and inclement weather and give them nesting places in which to raise their squabs.

=== Multiple pens ===
Many pigeon fanciers build their pigeon loft with at least two pens. This allows a few positive outcomes for the pigeon fancier:
# They are able to separate their males from females in order to control breeding.
# They are able to separate young, unmated pigeons from mated and settled pairs. This allows the mated and settled pairs to breed better.

Most fanciers have at least two pens for their pigeons and often fanciers have more than two pens or possibly multiple pigeon lofts. Extra pens allow for the keeping of spare, unmated females and males which can be useful to replace existing pigeons which might perish from disease or predation. Because it can be difficult to determine the sex of a young pigeon it is also handy to have extra pens for pigeons that have been weaned but which have not given external indications of their sex yet.

=== Trap/landing board ===
[[File:Sputnik Trap.jpg|thumb|right|A Sputnik trap.]]
For those pigeon fanciers that fly their pigeons (not all pigeon fanciers allow their pigeons to fly freely outside of their aviary) the pigeons need a means of ingress to the loft.  A trap or at least bobs and landing board allows the pigeon to get back into their home when they are ready to do so. There are different variations of trap and bobs used.<ref>Wendell (1977) 524</ref> Racing pigeons are commonly trapped home using a bob wired trapping arrangement that the birds push against the bob wires to gain access, but are restricted by the wires when trying to get back outside. Another form of trap typically called a ''Sputnik trap'' (pictured) uses openings set on an angle which are just wide enough for one bird to gain access by dropping through into the loft.

=== Flypen ===
Sometimes pigeon fanciers cannot allow their birds complete liberty due to complaints of neighbors.  However, pigeons have much better health and seem to be in much better spirits when they're given room to fly.  So most fanciers, including those that let their birds fly, will build a large enclosed area free of obstacles where the pigeons can fly as freely as they wish.  This is usually referred to as a flypen.<ref>Wendell (1977) 522</ref>

=== Nest boxes ===
Pigeon fanciers will often provide their mated pairs with nest boxes in which to build their nests.  Because pigeons are quite territorial about their nesting area<ref name="Pairing Behavior">{{cite journal|author1=Castoro, Paul L.  |author2=Guhl, A. M.  |lastauthoramp=yes | title = Pairing Behavior of Pigeons Related To Aggressiveness and Territory | journal =The Wilson Bulletin | url =http://sora.unm.edu/node/127648|volume=70|issue=1|pages=57–69}}</ref> pigeons co-exist much more harmoniously when each mated pair has two nest boxes of its own.

=== Perches ===
Again, pigeon fanciers will often provide their birds (both mated and unmated pigeons) with more perches than the birds need.  Because pigeons are also quite territorial about their perch<ref name="Pairing Behavior"/> it is best to ensure that every pigeon in the loft has lots of places to perch.

Pigeon fanciers often have their pigeon lofts in suitably modified garden sheds. In [[Glasgow]] and other areas of [[Scotland]] there has been a tradition of pigeon keepers building their own freestanding urban pigeon lofts, or [[dovecote|doocots]], standing about 4m high in areas of waste ground close to housing estates.  In New York City, pigeon fanciers often build pigeon lofts on the roof of the building.

<gallery>
Image:Doocot in Greenock garden.jpg|Pigeon loft in a garden.
Image:Glasgow doocot Firrhill 1.jpg|[[Glasgow]] doocot.
Image:Glasgow doocot Firrhill 2.jpg|Entrance door.
</gallery>

==Portrayal of pigeon keeping in the arts==
There have been several portrayals of pigeon keeping and pigeon fanciers in the arts.  One of the more famous portrayals of this hobby was in the film ''[[On The Waterfront]]'' where the main character, Terry Malloy, is a pigeon keeper.<ref>{{cite web
  | title =On The Waterfront (1954) Overview
  | publisher =Turner Classic Movies| url =http://www.tcm.com/tcmdb/title.jsp?stid=4749
  | format =Web article| accessdate =2008-05-02  }}</ref>
In the film ''[[Ghost Dog: The Way of the Samurai|Ghost Dog]]'', the title character is a [[hitman]] who communicates only using homing pigeons.

There have been portrayals of pigeon keeping in other art forms as well.  The artist [[Zina Saunders]] has painted portraits of New York pigeon keepers as part of her ''Overlooked New York'' project.<ref>{{cite web
  | last =Saunders| first = Zina| title =Rooftop Pigeon Coop Guys
  | publisher =Overlooked New York| url =http://www.overlookednewyork.com/pigeon/
  | format =Web article| accessdate =2008-01-05  }}</ref>
Photographer [[Zak Waters]] shot a black and white project documenting the lives of pigeon fanciers in the UK called ''Birdmen''. Sydney-based photographer [http://hohaitran.com Ho Hai Tran] recently debuted his work [http://www.onandon.com.au On Pigeons] which chronicled more than a year spent with the [http://www.pfsnsw.com Pigeon Fanciers Society of New South Wales] in Australia. The work was also exhibited in early 2013 in a show entitled ''Fancy'' which showcased portraits of some of the most notable fancy breeds including the Pouter, Jacobin, King and Dragoon.

The British visual and performance artist, [[Victoria Melody]], investigated pigeon fanciers to create a 2009 exhibition, ''Demographics of a Pigeon Fancier'', and a 2012 theatre show, ''Northern Soul''. The projects were the result of months spent living with British pigeon fanciers during the racing season.<ref name="Demographics">[http://www.victoriamelody.co.uk/projects/Demographic/installations/index.php Melody's description of the exhibition on her website]</ref>

On the U.S. television show [[NYPD Blue]], Detective [[Bobby Simone]] raised pigeons. Several episodes featured Simone tending to his pigeons on the roof of his home.

==Commercial pigeon keeping==
<!-- I'm working on this.  Yes, it still needs lots of work but I'm working on improving it.  The dummy headers are to help me break down the subject a bit more-->
<!--====Origins of commercial pigeon keeping====-->
Since their initial domestication by people, pigeons have been seen as a cheap source of good meat.  The Romans certainly kept pigeons for food as evidenced by the fact that they were familiar with the practice of force feeding squabs in order to fatten the young pigeons faster.<ref>Wendell (1977) 475</ref> Pigeons were especially prized because they would produce fresh meat during the winter months when larger animals were unavailable as a food source.  In the past wealthy landowners often had pigeon houses and kept pigeons.  Strict laws were enacted to protect the inhabitants of these structures.<ref>{{cite book |last=Tegetmeier |first=William |title= Pigeons: their structures, varieties,habits and management|year= 1868|publisher= George Rutledge and Sons|location= London|url=https://books.google.com/?id=v-gDAAAAQAAJ|page=38}}</ref>

===Improving yield===
Ten pairs of pigeons can produce eight squabs each month without being fed especially by the pigeon keepers. For a greater yield, commercially raised squab may be produced in a two-nest system, where the mother lays two new eggs in a second nest while the squabs are still growing in the first nest,<ref name=FAO>{{cite book|last=Schiere|first=Hans|author2=van der Hoek, Rein|title=Livestock keeping in urban areas: a review of traditional technologies based on literature and field experiences |publisher=[[Food and Agriculture Organization]]|year=2001|series=FAO animal production and health paper|volume=151|pages=29|isbn=978-92-5-104575-6|url=https://books.google.com/?id=mywom_Ourn8C&pg=PP12}}</ref> fed by their father.<ref>{{cite web|url=http://www.dpi.nsw.gov.au/__data/assets/pdf_file/0011/213221/Squab-raising.pdf|title=Squab raising |last=Bolla|first=Gerry|year=2007|publisher=[[New South Wales Department of Primary Industries]]|accessdate=2009-09-03}}</ref>  Establishing two breeding lines has also been suggested as a strategy, where one breeding line is selected for prolificacy and the other is selected for "parental performance".<ref>{{cite journal|last=Aggrey|first=S.E.|author2=Cheng, K.M. |year=1993|title=Genetic and Posthatch Parental Influences on Growth in Pigeon Squabs|journal=[[Journal of Heredity]] |volume=84|issue=3|pages=184–187|url=http://jhered.oxfordjournals.org/cgi/content/abstract/84/3/184|accessdate=2009-09-03}}</ref>

==Hazards of pigeon keeping==
Keeping pigeons has been found to lead to a condition called [[bird fancier's lung|pigeon fancier's lung]] in some fanciers.<ref name="PFL1">{{cite web
  | last =Bourke| first =Stephen|author2=Boyd,Gavin | title =Pigeon fancier's lung
  | publisher =BMJ| url =http://www.bmj.com/cgi/content/full/315/7100/70
  | format =Web Article| accessdate =2008-01-05  }}</ref> Pigeon fancier's lung is an extrinsic allergic reaction resembling asthma which occurs when a person has been exposed to certain proteins in the dust associated with a pigeon's feathers over long periods of time, usually several years.<ref name="PFL1"/>  When a fancier develops this condition, there are several treatments which may help him or her to keep his or her pigeons.<ref name="PFL1"/>  However, some cases are so severe that the fancier must avoid close contact with pigeons.<ref>{{cite web| title =Pigeon Lung| publisher =British Pigeon Fanciers Medical Research| url =http://www.pigeon-lung.co.uk/main.html#| format =Web Article| accessdate =2008-01-05}}</ref>

==Famous fanciers==
* [[Al-Muzaffar Hajji]] (died 1347), Mamluk sultan of Egypt, who was well known for engaging in pigeon raising and racing to the chagrin of his senior aides.<ref>{{cite book|last1=Holt|first1=Peter Malcolm|title=The Age of the Crusades: The Near East from the Eleventh Century to 151|date=1986|publisher=Addison Wesley Longman Limited|isbn=9781317871521|url=https://books.google.com/books?id=TqasAgAAQBAJ&pg=PA122|page=123}}</ref>
* [[Maximilien Robespierre]], French revolutionary, raised pigeons and sparrows in his youth.<ref>Robespierre, Charlotte (2001). ''Memoirs. ''Adamant Media Corporation. ISBN 0543909700</ref>
* [[Thomas S. Monson]] has enjoyed raising Birmingham Roller pigeons since he was a young boy.<ref>Heidi S. Swinton, To the Rescue: The Biography of Thomas S. Monson (Salt Lake City: Deseret Book Company, 2010)</ref>
*[[Mike Tyson]] former undisputed heavyweight boxing champion is a lifelong pigeon fancier.
* [[Duncan Ferguson]], former [[Everton F.C.|Everton]] player, enjoys the sport of [[pigeon racing]].<ref>{{cite web|title=Pigeon Crisis For Ferguson|url=http://www.uefa.com/news/newsid=69259.html|date = 16 May 2003 | publisher = Union of European Football Associations (UEFA.com) | accessdate=10 September 2015}}</ref>
* [[Queen Elizabeth II]] is a fourth generation member of the Royal Family who has enjoyed the sport of [[pigeon racing]].<ref>[http://www.royal.gov.uk/output/page4832.asp 80 facts about The Queen]</ref><ref>{{cite web|last=Cockcroft |first=Lucy |title=Queen gives racing pigeons to juvenile prison |publisher=Telegraph UK |url=http://www.telegraph.co.uk/news/newstopics/theroyalfamily/2464604/Queen-gives-racing-pigeons-to-juvenile-prison.html |accessdate=2008-08-05 |deadurl=unfit |archiveurl=http://www.webcitation.org/5ZqUaSYWA |archivedate=August 5, 2008 }}</ref>  The first racing pigeons at Sandringham were a gift to the future Edward VII by King Leopold of Belgium in 1886.
* [[Karel Meulemans]], a noted pigeon fancier in [[Arendonk]], [[Belgium]].
* [[Paddy Ambrose]] (1928–2002), Irish international footballer was said to prefer pigeon racing to the sport that he became famous for.
* [[Nikola Tesla]], famous inventor, fed the pigeons of New York City, nursing those who were sick or injured.  Of the birds, he said, "These are my sincere friends."
* [[Gerry Francis]], English footballer, coach and commentator is a pigeon racing fancier.
* [[Pablo Picasso]], famous artist who kept fantails named his daughter Paloma which means pigeon in Spanish.
* [[Charles Darwin]], English naturalist, kept fancy pigeon breeds for observation and anatomical study of his test breeding.
* [[Yul Brynner]], actor, ''[[The King and I (1956 film)|The King and I]]'', owned Roller pigeons, known to take private helicopter to view aerial performance of his pigeons.
* [[Manuel Noriega]], deposed Panamanian dictator, had many palaces to house fancy pigeons.
* [[Queen Victoria]], Queen of England who was fond of the Jacobin strain of fancy pigeons whose feathered "cowl" resembles the garments of Jacobin monks.
* [[Bill Lawry]], former Australian cricketer and Channel Nine cricket commentator. Bill's love of pigeons has been parodied in the successful [[The Twelfth Man]] comedy recordings.

==See also==
* [[List of pigeon breeds]]
* [[Pigeon racing]]
* [[Dovecote]]

==References==
{{reflist|30em}}

==Bibliography==
* [[Marius Kociejowski|Kociejowski, Marius]] ''The Pigeon Wars of Damascus'' [Biblioasis, 2010] contains six chapters on the history of pigeon fancying in the Arab world
* {{cite book |last=Levi |first=Wendell |title= The Pigeon|year= 1977|publisher= Levi Publishing Co, Inc|location= Sumter, S.C.|isbn=0-85390-013-2}}

==External links==
* http://www.faircountclub.com/faircount_facts_info.htm
* http://www.pigeonclubsusa.com
* http://www.uplandbirddog.com/training/loft.html
* [http://www.npausa.com/ US National Pigeon Association]
* [http://www.ifpigeon.com/ International Federation]
* [http://www.pigeon.org/ American Racing Pigeon Union]
* [http://www.zyworld.com/NPA/ UK National Pigeon Association]
* [http://www.hiddenglasgow.com/doocots/index.htm hiddenglasgow: doocots (dookits).]
* [http://www.thepigeoncoop.co/ Pigeon Racing Information Source]
* [http://darwinspigeons.com darwinspigeons.com]

{{Breed}}

[[Category:Domestic pigeons]]
[[Category:Livestock]]