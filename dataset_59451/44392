{{Infobox airport 
| name         = Vardø Airport, Svartnes
| nativename   = {{lang|no|Vardø Lufthavn, Svartnes}}
| nativename-a = 
| nativename-r = 
| image        = Vardø Airport.jpg
| image-width  = 300px
| caption      = Vardø Airport terminal
| IATA         = VAW
| ICAO         = ENSS
| type         = Public
| owner        = 
| operator     = [[Avinor]]
| city-served  = [[Vardø]], [[Norway]]
| location     = [[Svartnes]], Vardø
| elevation-f  = 42
| elevation-m  = 13
| coordinates  = {{coord|70|21|19|N|031|02|42|E|region:NO|display=inline,title}}
| pushpin_map            = Norway
| pushpin_relief         = 1
| pushpin_label_position =
| pushpin_label          = VAW
| pushpin_map_alt        =
| pushpin_mapsize        = 300
| pushpin_image          =
| pushpin_map_caption    = Location in Norway
| website      = [https://avinor.no/en/airport/vardo-airport/ Official website]
| metric-elev  = yes
| metric-rwy   = yes
| r1-number    = 15/33
| r1-length-f  = 3757
| r1-length-m  = 1145
| r1-surface   = Asphalt
| stat-year    = 2014
| stat1-header = Passengers
| stat1-data   = 13,889
| stat2-header = Aircraft movements
| stat2-data   = 2,518
| stat3-header = Cargo (tonnes)
| stat3-data   = 0.7
| footnotes    = Source:<ref name="aip_enss">
{{cite web
  |title=ENSS – Vardø/Svartnes
  |work=AIP Norge/Norway
  |publisher=Avinor
  |at=AD 2 ENSS
  |url=https://www.ippc.no/norway_aip/current/AIP/AD/ENSS/EN_AD_2_ENSS_en.pdf
  |date=8 March 2012
  |accessdate=19 August 2012
  |format=PDF
}}</ref><ref name=stats />
}}

'''Vardø Airport, Svartnes''' ({{lang-no|Vardø Lufthavn, Svartnes}}; {{Airport codes|VAW|ENSS|p=n}}) is a [[short take-off and landing]] airport located at [[Svartnes]] in [[Vardø Municipality]] in [[Finnmark]] county, [[Norway]]. Owned and operated by the state-owned [[Avinor]], it served 14,664 passengers in 2012. The airport has a {{convert|1145|by|30|m|sp=us|adj=on}} runway aligned 15–33. It is served by [[Widerøe]] who operate [[Bombardier Dash 8]] aircraft to [[Kirkenes]] and other communities in [[Finnmark]]. The airport is located {{convert|4|km|sp=us}} from [[Vardøya]] and the town center of [[Vardø (town)|Vardø]].

Svartnes was built by the German [[Luftwaffe]] 1943, where it served [[fighter aircraft]] to protect German convoys. The airport was abandoned in 1944 but reopened by the [[Norwegian Armed Forces]] for military passenger flights. Plans to start civilian operates were launched in the 1960s and from 1970 [[Norving]] started irregular flights to the airport. An upgrade to the terminal and runway were carried out between 1984 and 1990. Widerøe took over flights in 1991.

==History==
Svartnes was constructed by the Luftwaffe during the [[German occupation of Norway]] during [[World War II]].<ref name=h317>Hafsten: 317</ref> The background for the establishment was Soviet attacks on supply convoys operating to Kirkenes.<ref>Hafsten: 171</ref> During construction, on 12 July 1943, eight Soviet [[Ilyushin Il-2]]s attacked Svartnes, but all were shot down. While the war remained in the area Soviet air attacks continued on the air base.<ref>Hafsten: 173</ref> The airport was completed in the fall of 1943, it was exclusively used for fighter aircraft detachments of [[Jagdgeschwader 5]]. These were used as part of the defense of German ship traffic around [[Varangerhalvøya]]. The original wooden runway was {{convert|1000|by|90|m|sp=us}}.<ref name=h317 /> The [[Wehrmacht]] operated a [[prisoner of war camp]] at the military base.<ref>Olsen: 64</ref> The air base fell into disuse following the German evacuation in 1944.<ref name=gynnild>{{cite web |url=http://avinor.moses.no/index.php?seks_id=135&element=kapittel&k=2 |title=Flyplassenes og flytrafikkens historie |work=Kulturminner på norske lufthavner – Landsverneplan for Avinor |publisher=[[Avinor]] |last=Gynnild |first=Olav |year=2009  |accessdate=25 January 2012 |archivedate=25 January 2012 |archiveurl=http://www.webcitation.org/6DwQ1MXLE |deadurl=no}}</ref>

The wooden runway was pillaged by locals to accumulate building materials for reconstruction.<ref>Dancke: 367</ref> The Norwegian Armed Forces established itself in Vardø in the mid-1950s. The airport was renovated; a terminal was built consisting of two simple barracks, one used as a passenger terminal and the other as a tower, consisting of a glass addition on the roof. The {{convert|1100|by|80|m|sp=us|adj=on}} gravel runway received portable runway lights. The [[Royal Norwegian Air Force]] served the airport with [[de Havilland Canada Twin Otter]] and [[Shorts Skyvan]] aircraft to transport military personnel.<ref name=gynnild />

The first plans for a civilian airport at Svartnes was launched by Varangfly, later renamed Norving, in 1964. Vardø was mentioned as one of five villages in Finnmark which the airline hoped to open with simple airfields which could serve [[air taxi]] and [[air ambulance]] flights.<ref>Melling: 52</ref> Two years later several major airlines proposed a network of [[short take-off and landing]] (STOL) airports in Northern Norway, and Vardø was proposed as a possible location.<ref>Melling: 54</ref> A county committee was established in 1966 to look into the matter. It considered seven locations in Finnmark, including Vardø and recommended in its report that planning continue. Simultaneously the Ministry of Transport and Communications was working on a plan for larger [[short take-off and landing]] airports. It decided that such airports will first be built in [[Helgeland]], then [[Lofoten]] and [[Vesterålen]] and finally in [[Troms]] and Finnmark.<ref>Melling: 64</ref>

[[File:Widerøe DHC-8-100 at ENSS 20120312.jpg|thumb|left|[[Widerøe]] [[Dash 8-100]]]]
Norving started operating irregular air taxi flights to Vardø after they took delivery of an eight-seat [[Britten-Norman Islander]] in April 1970.<ref>Melling: 66</ref> In addition, the airline used the air field for air ambulance services.<ref name=gynnild /> Norving received permission to operate a scheduled taxi route from Vardø to [[Kirkenes Airport, Høybuktmoen]] and [[Båtsfjord Airport (1973–1999)|Båtsfjord Airport]] from the late 1970s.<ref>Melling: 161</ref>

Construction of a new terminal and upgrading the airport to regional standard started in 1984.<ref name=gynnild /> In February 1987 Vardø Municipality received operating permission from the government. The investments cost NOK&nbsp;11.4 million, of which NOK&nbsp;10 million was to be financed through a loan and the rest through subsidies from the government and the county. The latter was also responsible for covering the operating deficit.<ref>{{cite news |title=Kortbaneflyplass i Vardø |work=[[Aftenposten]] |date=25 July 1987 |page=32 |language=Norwegian}}</ref> The upgraded airport opened on 6 April 1987. The first two months Norving continued its taxi route service to the airport, but from 1 June a regular concession scheduled service was introduced. Widerøe took over the services in 1991. At first the airport was served using Twin Otters, but from the mid-1990s the Dash 8 was introduced.<ref name=gynnild /> From 1996, Svartnes and 25 other regional airports were taken over by the state and the Civil Aviation Administration (later renamed Avinor).<ref>{{cite news |title=Staten eier av flyplasser |work=[[Aftenposten]] |date=10 December 1994 |page=25 |language=Norwegian}}</ref> Widerøe lost the bid to operate the services between 2000 and 2003 to [[Arctic Air (Norwegian airline)|Arctic Air]], but resumed services in 2003.<ref name=gynnild /> [[Airport security]] was introduced on 1 January 2005.<ref>{{cite news |title=Tre usikre flyplasser i Midt-Norge |last=Solberg |first=Pål E. |work=[[Adresseavisen]] |date=30 September 2004 |page=4 |language=Norwegian}}</ref> There has several times been discuss whether to close down the airport, having in mind that Vadsø Airport is 67&nbsp;km away by road. In 2002 there was political support in [[Stortinget]] to close it if the road was upgraded,<ref>[https://www.nrk.no/finnmark/flertall-for-a-legge-ned-vardo-flyplass-1.105822 Flertall for å legge ned Vardø flyp]</ref> but neither happened. In 2015 Avinor stated that closing Vardø would give least passenger trouble related to the financial support in the country, but that no decision on it would be made before 2019.<ref>[http://www.bt.no/nyheter/lokalt/Vil-ikke-legge-ned-flyplasser-na-295274b.html Vil ikke legge ned flyplasser nå]</ref> Because of lack of available aircraft for purchase, before 2030 all short airports must be extended, closed or be flown with very small planes. Vardø has no room for extension.<ref>[http://www.ntp.dep.no/Nasjonale+transportplaner/2014-2023/Planforslag/_attachment/501391/binary/812582?_ts=13ff62eed70 Nasjonal transportplan 2014-2023 Framtidsrettet utvikling av lufthavnstrukturen]</ref>

==Facilities==
The airport has a single terminal building which has an integrated [[control tower]]. The passenger terminal has a capacity for thirty passengers per hour.<ref>{{cite web |url=http://www.regjeringen.no/upload/OED/pdf%20filer/Barentshavet_S/KI/14_Luftfart.pdf |title=Konsekvenser for luftfart |publisher=Avinor |date=October 2012 |language=Norwegian |pages=56–57 |accessdate= 25 January 2012 |archivedate=25 January 2012 |archiveurl=http://www.webcitation.org/6DvtAOwz2 |deadurl=no}}</ref> The airport is located {{convert|4|km|sp=us}} driving from the town center. Taxis are available at the airport.<ref>{{cite web |url=http://www.avinor.no/en/airport/vardo/tofromairport |title=To/From Airport |publisher=[[Avinor]] |language=Norwegian |date=24 September 2012 |accessdate=1 February 2012}}</ref> In 2012 the airport had 13,889 passengers, 2,518 aircraft movements and 0.7 tonnes of cargo handled.<ref name=stats>{{cite web |url=http://www.mynewsdesk.com/material/document/42080/download?resource_type=resource_document |title=Månedsrapport |publisher=[[Avinor]] |format=XLS |accessdate=13 January 2015 |year=2015}}</ref>

==Airlines and destinations==
The airport is served by Widerøe with 39-seat Dash 8-100 aircraft connecting the community to [[Tromsø]], Kirkenes and other communities in Finnmark. The routes are operated on [[public service obligation]] with the [[Ministry of Transport and Communications (Norway)|Ministry of Transport and Communications]].<ref>Draagen: 11</ref>

{{Airport-dest-list
| [[Widerøe]] | [[Båtsfjord Airport|Båtsfjord]], [[Berlevåg Airport|Berlevåg]], [[Kirkenes Airport, Høybuktmoen|Kirkenes]], [[Vadsø Airport|Vadsø]]
}}

==Accidents and incidents==
On 5 March 1978 the [[Partenavia P.68]] LN-MAD operated by Norving crashed at Falkefjell during approach to Vadsø Airport. The crew of two and a passenger all survived, but the aircraft was written off.<ref>Melling: 154</ref>

== References ==
{{reflist|30em}}

;Bibliography
* {{cite book |last=Dancke |first=Trond M. E. |title=Opp av ruinene |year=1986 |publisher=Gyldendal Norsk Forlag |location=Oslo |language=Norwegian |isbn=82-05-16986-1}}
* {{cite web |url=http://www.regjeringen.no/Upload/SD/Vedlegg/rapporter_og_planer/2012/rapp_ruter_2013.pdf |title=Alternative ruteopplegg for Finnmark og Nord Troms
 |last=Draagen |first=Lars |last2=Wilsberg |first2=Kjell |publisher=Gravity Consult |year=2011 |language=Norwegian |accessdate=1 October 2012 |archivedate=1 October 2012 |archiveurl=http://www.webcitation.org/6B5OfdMM6 |deadurl=no |format=PDF}}
* {{cite book |last=Hafsten |first=Bjørn |last2=Larsstuvold |first2=Ulf |last3=Olsen |first3=Bjørn |last4=Stenersen |first4=Sten |title=Flyalarm: Luftkrigen over Norge 1939–1945 |year=1991 |publisher=Sem & Stenersen |location=Oslo |language=Norwegian |isbn=82-7046-058-3}}
* {{cite book |last=Melling |first=Kjersti |title=Nordavind fra alle kanter |year=2009 |publisher=Pilotforlaget |location=Oslo |language=Norwegian}}
* {{cite book |last=Olsen |first=Margido |title=Det gamle Vardø herred og Vardø by |year=1999 |language=Norwegian}}

==External links==
[[File:Wind rose 2016 for ENSS Svartnes, Norway.jpg|thumb|left|[[Wind rose]] showing distribution of wind speed and direction for Vardø Airport, Svartnes for the year 2016.]]
{{commons category|Vardø Airport, Svartnes}}

{{Airports in Norway}}
{{Portal bar|Aviation|World War II|Norway|Arctic}}

{{DEFAULTSORT:Vardo Airport, Svartnes}}
[[Category:Airports in Finnmark]]
[[Category:Avinor airports]]
[[Category:Luftwaffe airports in Norway]]
[[Category:Royal Norwegian Air Force airfields]]
[[Category:Vardø]]
[[Category:1943 establishments in Norway]]
[[Category:Airports established in 1943]]
[[Category:Military installations in Finnmark]]