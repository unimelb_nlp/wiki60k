{{good article}}
{{Infobox college coach
| name          = Philip Henry Bridenbaugh
| image         = PHBridenbaugh.jpg
| caption       = Philip Henry Bridenbaugh in 1912
| birth_date    = May 1, 1890
| birth_place   = [[Martinsburg, Pennsylvania]]
| death_date    = {{death date and age|1990|6|14|1890|5|1}}
| death_place   = [[New Castle, Pennsylvania]]
| sport         = [[College football]]
| overall_record= 23–12–5 (0.638)
| awards        = 
| CFbDWID       = 
| player_years1 = 1908–1911
| player_team1  = [[Franklin & Marshall College|Franklin & Marshall]]
| player_positions = [[quarterback|QB]]
| coach_years1  = 1917–1921
| coach_team1   = [[Geneva College|Geneva]]
| CFBHOF_year   = 
}}
'''Philip Henry Bridenbaugh''' (May 1, 1890 – June 14, 1990) was an [[American football]] player, coach, and sports figure in the United States. A graduate of [[Franklin & Marshall College]], where he earned a degree in teaching and spent four years on several of its sports teams, Bridenbaugh coached football at several places in his home state of Pennsylvania prior to being selected as the head coach of the [[Geneva College]] [[Geneva Golden Tornadoes football|Golden Tornadoes]] in 1917. He left Geneva in 1922 with a 23–12–5 record and took a job with [[New Castle Junior/Senior High School]] as a mathematics teacher and head football, basketball, and track and field coach. He did not lose a football game in his first two years, marking the first of eleven undefeated seasons, and, over the course of 33 years, won seven league titles in the sport, leaving in 1955 with a 265–65–25 record. He continued to work as an assistant football coach at [[Grove City College]] until 1964 and was inducted into several regional halls of fame. He died in June 1990 at the age of 100.

==Early life==
Bridenbaugh was born on May 1, 1890 in [[Martinsburg, Pennsylvania]],<ref name="FAMHOF">{{cite web|url = http://www.godiplomats.com/trad/hall/bios/Philip_Bridenbaugh|title = Philip Bridenbaugh '12|publisher = [[Franklin & Marshall College]]|access-date = December 1, 2016}}</ref> one of 14 children<ref name="Children">{{cite news|last = Newton|first = Mrs A. R., Jr.|title = Emlenton Doctor Honored at Testimonial Dinner|url = https://www.newspapers.com/clip/8677476/the_oil_city_derrick/|newspaper = The Oil City Derrick|date = February 26, 1958|page = 5|via = [[Newspapers.com]]|accessdate = February 1, 2017}} {{free access}}</ref> of Professor Phillip Howard Bridenbaugh,<ref name="Marriage">{{cite news|title = Bridenbaugh-Crissman|url = https://www.newspapers.com/clip/8693860/altoona_tribune/|newspaper = Altoona Tribune|date = June 9, 1916|page = 4|via = [[Newspapers.com]]|accessdate = February 1, 2017}} {{free access}}</ref>  an educator and academic administrator,<ref name="Father">{{cite news|title = Short and Quick|url = https://www.newspapers.com/clip/8677015/the_evening_news/|newspaper = The Evening News|date = April 24, 1936|page = 12|via = [[Newspapers.com]]|accessdate = February 1, 2017}} {{free access}}</ref> and Catherine Oelling.<ref name="Mother">{{cite news|title = Carl G. Bridenbaugh|url = https://www.newspapers.com/clip/8694354/tyrone_daily_herald/|newspaper = Tyrone Daily Herald|date = June 24, 1939|page = 8|via = [[Newspapers.com]]|accessdate = February 1, 2017}} {{free access}}</ref> He attended [[Altoona Area High School]] in [[Altoona, Pennsylvania]] for one year, and then enrolled in a [[university-preparatory school]], prior to entering [[Franklin & Marshall College]] in [[Lancaster, Pennsylvania]] in 1908. At Franklin & Marshall he competed in track and field, football, basketball, and baseball<ref name="Obituary"/> while earning a teaching degree, despite the fact that "football was considered out of bounds in the Bridenbaugh family".<ref name="NoFootball">{{cite news|title = Father of Coach Bridenbaugh at Harrisburg Game|url = https://www.newspapers.com/clip/8676618/new_castle_news/|newspaper = New Castle News|date = October 13, 1924|page = 20|via = [[Newspapers.com]]|accessdate = February 1, 2017}} {{free access}}</ref> In football he played in the position of [[quarterback]] and was team captain during his senior year.<ref name="Legend">{{cite news|last = Dugo|first = Andy|title = Phil Bridenbaugh, Living Legend in New Castle|newspaper = [[Pittsburgh Post-Gazette]]|page = 21|publisher = [[Block Communications]]|date = February 4, 1970|url = https://news.google.com/newspapers?nid=1129&dat=19700204&id=kkoNAAAAIBAJ&sjid=CG0DAAAAIBAJ&pg=7322,415970|accessdate = December 1, 2016}}</ref> During his first season, the team went 4–6–1, but it improved to a winning record of 9–1 the following year, with its sole loss being against [[Carnegie Mellon University]]. In his final two years at the institution, the squad went 4–3–2 and 3–6.<ref name="FAMSeasons">{{cite web|url = http://www.godiplomats.com/sports/m-footbl/Career_Stats/Game-by-Game.pdf|title = Franklin & Marshall Football Game-by-Game|date = November 15, 2014|publisher = [[Franklin & Marshall College]]|access-date = December 1, 2016}}</ref> He graduated in 1912<ref name="FAMHOF"/> and married Belva Rebecca Crissman, a teacher in Martinsburg, on June 8, 1916.<ref name="Marriage"/> They had three children: Betty, J. Ross, and Audrey.<ref name="BelvaObit">{{cite news|title = Belva C. Bridenbaugh|newspaper = [[Altoona Mirror]]|publisher = [[Ogden Newspapers]]|date = March 20, 1982}}</ref>

==Coaching career==
Bridenbaugh's first assistant coaching job was with the [[Oberlin Yeomen football|Oberlin Yeomen]] of [[Oberlin College]] under head coach [[T. Nelson Metcalf]]. He then spent two seasons at Kiski Preparatory School in [[Saltsburg, Pennsylvania]] and another two at [[Beaver Falls High School]]. In 1917 he became the ninth head coach of [[Geneva College]]'s [[List of Geneva Golden Tornadoes head football coaches|Golden Tornadoes]] in [[Beaver Falls, Pennsylvania|Beaver Falls]].<ref name="Obituary"/> His record in this first season was 5–3–1, although the team allowed as many points as they scored (114). He went 4–2 in 1918, outscoring the opposition 67–36, and 4–2–2 in 1919, despite a negative point differential of 51–59 caused by a 2–33 loss to the [[University of Pittsburgh]]. In 1920 his record was 5–2–1, outscoring other institutions 125–61 despite a 0–47 loss to Pittsburgh. He was 5–3–1 in his final season, with a positive point differential of 106–49; half of their points scored during this year came in a 54–0 victory over [[Juniata College]]. Following his resignation,<ref name="Resign">{{cite news|title = Re-election of Coach Park May Set Geneva Athletic Machinery Rumbling Again|url = https://www.newspapers.com/clip/8676463/pittsburgh_postgazette/|newspaper = Pittsburgh Post-Gazette|date = December 25, 1922|page = 9|via = [[Newspapers.com]]|accessdate = February 1, 2017}} {{free access}}</ref> he was replaced by [[Robert Park (American football)|Robert Park]], who was head of the history department, for the 1922 season, and then [[Tom Davies (American football)|Tom Davies]] in 1923.<ref name="GenevaMG">{{cite web|url = http://www.geneva.edu/athletics/mens/football/football_media_guide|title = GT All-Time Scores|year = 2013|publisher = [[Geneva Golden Tornadoes football]]|access-date = December 1, 2016}}</ref>

During this time he also coached the school's basketball team. In his first season, 1917–1918, he went 13–2, outscoring his opponents 524–303 and losing against only [[Allegheny College]] and [[Westminster College (Pennsylvania)|Westminster College]]. He maintained a winning record for the next two seasons, going 10–7 for the 1918–1919 season and 10–9 in 1919–1920. In the latter case, the Golden Tornadoes managed a positive point margin of 599–581. Bridenbaugh went 7–14 for the 1920–1921 season, being outscored 584–631, and 8–10 in his final year, with a 546–589 point record.<ref name="Basketball">{{cite web|url = http://www.geneva.edu/athletics/mens/basketball/all_time_scores|title = Geneva men's basketball all-time scores|year = 2016|publisher = [[Geneva College]]|access-date = December 1, 2016}}</ref>

In 1922 Bridenbaugh was hired by [[New Castle Junior/Senior High School]] as a mathematics teacher and as their head football coach. He was undefeated in his first two seasons with New Castle, posting an 8–0–1 record in 1922–1923, with a point differential of 229–54, and finishing 10–0–1 in 1923–1924.<ref name="LawrenceCounty">{{cite web|url = http://www.lawrencechs.com/museum/exhibits/sports/football/|title = Football|year = 2016|publisher = [[Lawrence County, Pennsylvania|Lawrence County]] Historical Society|access-date = December 1, 2016}}</ref> He continued his undefeated streak for another 13 games in 1924–1925, before being bested 0–14 by a team from [[Ellwood City, Pennsylvania|Ellwood City]] that would itself go on to have a perfect season.<ref name="Ellwood1925">{{cite web|url = http://www.pawrsl.com/pfn/wilson_ellwoodcity25.htm|title = Ellwood City  1925|last = Wilson|first = Hal|year = 2006|publisher = PA Football News|access-date = December 1, 2016}}</ref> He coached at New Castle for 33 years, earning the nickname "The Fox",<ref name="Fox">{{cite news|title = Legend of The Fox|url = https://www.newspapers.com/clip/8677826/new_castle_news/|newspaper = New Castle News|date = June 23, 1971|page = 4|via = [[Newspapers.com]]|accessdate = February 1, 2017}} {{free access}}</ref> and left in 1955 with a record of 265–65–25, which included 11 undefeated seasons, seven one-loss seasons, and seven [[Western Pennsylvania Interscholastic Athletic League]] (WPIAL) championships (1924, 1932–34, 1942, 1948–49); at the time of his death, this was believed to be the best record in Pennsylvanian high school football history and the fourth-best in the United States.<ref name="Obituary"/> He also coached basketball at New Castle, leaving with a 319–159 record and two victories at the WPIAL championships in 1927 and 1936, and ran the track and field program. He ended his career by serving as an assistant coach at [[Grove City College]] in [[Grove City, Pennsylvania]] until 1964.<ref name="Obituary"/>

==Later life==
Bridenbaugh's wife died on March 19, 1982.<ref name="BelvaObit"/> A plaque listing his accomplishments was hung at the school in 1971<ref name="Plaque">{{cite news|title = Tribute to Bridenbaugh|url = https://www.newspapers.com/clip/8677876/new_castle_news/|newspaper = New Castle News|date = June 25, 1971|page = 18|via = [[Newspapers.com]]|accessdate = February 1, 2017}} {{free access}}</ref> and he was inducted into the Western Chapter of the [[Pennsylvania Sports Hall of Fame]] in 1972,<ref name="WCPSHOF">{{cite web|url = http://www.westernpasportshof.org/wp-content/uploads/2015/12/Western-Chapterbyname.pdf|title = Pennsylvania Sports Hall of Fame – Western Chapter – Inductees by Name|date = 23 April 2016|publisher = [[Pennsylvania Sports Hall of Fame]] – Western Chapter|access-date = 1 December 2016}}</ref> the Lawrence County Historical Society Sports Hall of Fame in 1984, the Pennsylvania High School Football Coaches Hall of Fame in 1989, and the Franklin & Marshall College Athletics Hall of Fame in 1994.<ref name="FAMHOF"/> He died on June 14, 1990 of heart disease, at the Indian Creek Nursing Center in [[New Castle, Pennsylvania]], at the [[centenarian|age of 100]].<ref name="Obituary">{{cite news|title = Philip Henry Bridenbaugh, football coach|newspaper = [[Pittsburgh Post-Gazette]]|page = 6|publisher = [[Block Communications]]|date = June 19, 1990|url = https://news.google.com/newspapers?id=pIMNAAAAIBAJ&sjid=yG4DAAAAIBAJ&pg=6721,4564485&dq=bridenbaugh+geneva-college|accessdate = December 1, 2016}}</ref> The field at New Castle is named jointly in honor of Bridenbaugh and Lindy Lauro,<ref name="Field">{{cite news|last = Emert|first = Rich|title = Naming Rights|newspaper = [[Pittsburgh Post-Gazette]]|page = EZ-8/EZ-10|publisher = [[Block Communications]]|date = August 18, 2005|url = https://news.google.com/newspapers?nid=1129&dat=20050818&id=ZIdIAAAAIBAJ&sjid=DXEDAAAAIBAJ&pg=6811,5269867|accessdate = December 1, 2016}}</ref> a player under Bridenbaugh who later coached New Castle to a 220–104–15 record, making it the only high school in the nation with two coaches who earned 200 or more victories.<ref name="LawrenceCounty"/>

==Head coaching record==
{{CFB Yearly Record Start | type = coach | team = | conf = | bowl = | poll = no}}
{{CFB Yearly Record Subhead
 | name      = [[Geneva Golden Tornadoes football|Geneva Golden Tornadoes]]
 | conf      = Independent
 | startyear = 1917
 | endyear   = 1921
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[1917 college football season|1917]]
 | name         = Geneva
 | overall      = 5–3–1
 | conference   = 
 | confstanding = 
 | bowlname     = 
 | bowloutcome  = 
 | bcsbowl      = 
 | ranking      = no
 | ranking2     = no
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[1918 college football season|1918]]
 | name         = Geneva
 | overall      = 4–2–0
 | conference   = 
 | confstanding = 
 | bowlname     = 
 | bowloutcome  = 
 | bcsbowl      = 
 | ranking      = no
 | ranking2     = no
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[1919 college football season|1919]]
 | name         = Geneva
 | overall      = 4–2–2
 | conference   = 
 | confstanding = 
 | bowlname     = 
 | bowloutcome  = 
 | bcsbowl      = 
 | ranking      = no
 | ranking2     = no
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[1920 college football season|1920]]
 | name         = Geneva
 | overall      = 5–2–1
 | conference   = 
 | confstanding = 
 | bowlname     = 
 | bowloutcome  = 
 | bcsbowl      = 
 | ranking      = no
 | ranking2     = no
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[1921 college football season|1921]]
 | name         = Geneva
 | overall      = 5–3–1
 | conference   = 
 | confstanding = 
 | bowlname     = 
 | bowloutcome  = 
 | bcsbowl      = 
 | ranking      = no
 | ranking2     = no
}}
{{CFB Yearly Record Subtotal
 | name       = Geneva
 | overall    = 23–12–5
 | confrecord = 
}}
{{CFB Yearly Record End
 | overall  = 23–12–5
 | bcs      = no
 | poll     = no
 | polltype = 
 | legend   = no
}}

{{CBB Yearly Record Start
|type=coach
|conference=
|postseason=
|poll=no
}}
{{CBB Yearly Record Subhead
|name=Geneva Golden Tornadoes
|startyear=1917
|conference= Independent
|endyear=1921
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 1917–1918
 | name         = Geneva
 | overall      = 13–2
 | conference   = 
 | confstanding = 
 | postseason = 
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 1918–1919
 | name         = Geneva
 | overall      = 10–7
 | conference   = 
 | confstanding = 
 | postseason = 
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 1919–1920
 | name         = Geneva
 | overall      = 10–9
 | conference   = 
 | confstanding = 
 | postseason = 
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 1920–1921
 | name         = Geneva
 | overall      = 7–14
 | conference   = 
 | confstanding = 
 | postseason = 
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 1921–1922
 | name         = Geneva
 | overall      = 8–10
 | conference   = 
 | confstanding = 
 | postseason = 
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Subtotal
 | name = Geneva
 | overall = 48–42
 | confrecord = 
}}
{{CBB Yearly Record End
|overall= 48–42
|poll=no
}}
== References ==
{{Reflist|30em}}

{{Geneva Golden Tornadoes football coach navbox}}

{{DEFAULTSORT:Bridenbaugh, Phillip Henry}}
[[Category:1890 births]]
[[Category:1990 deaths]]
[[Category:People from Blair County, Pennsylvania]]
[[Category:Geneva Golden Tornadoes football coaches]]
[[Category:Grove City Wolverines football coaches]]
[[Category:American centenarians]]
[[Category:Franklin & Marshall Diplomats football players]]
[[Category:Players of American football from Pennsylvania]]