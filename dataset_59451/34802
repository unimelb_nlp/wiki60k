{{good article}}
{{Infobox NCAA team season
| sport=football
| Year            = 1884
| Prev year       = 1883
| Team            = Navy Midshipmen
| Conference      = Independent
| ShortConference = Independent
| Record          = 1–0
| ConfRecord      =
| HeadCoach       = None
| HCYear          = 
| Captain         = Jim Kittrell
| StadiumArena    = Unknown
}}
{{1884 college football records}}
The '''1884 Navy Midshipmen football team''' represented the [[United States Naval Academy]] in the [[1884 college football season]]. The team was the fourth intercollegiate [[American football|football]] squad to represent the United States Naval Academy, and was the final time the school played a single-game season. The squad was captained by rusher Jim Kittrell. The team's single game was a 9 to 6 (9–6) defeat of rival-school [[Johns Hopkins University|Johns Hopkins]]. The season continued a seven-season, eight game rivalry between the Naval Academy and Johns Hopkins. It was the final season that a Naval Academy team would go unbeaten and untied.

==Background and prelude==
According to biographer C. Douglas Kroll, the first evidence of a form of football at the [[United States Naval Academy]] came in 1857, but the school's cadets lost interest in the game shortly afterward.<ref name="Bertholf">[[#Kroll|Kroll (2002)]], p.&nbsp;14</ref> However, it is widely believed by football researchers that the playing of [[intercollegiate football]] began in November 1869, when a player at [[Rutgers University]] challenged another player at the nearby [[Princeton University|College of New Jersey]] (now Princeton). The contest more closely resembled [[Association football|soccer]], with teams scoring by kicking the ball into the opponent's net, and lacked a uniform rules structure.<ref name="ESPN first game">{{cite web |last=Schlabach |first=Mark |year=2013 |url=http://sports.espn.go.com/ncf/face/team?teamId=164 |title=Rutgers Scarlet Knights&ndash;Nov. 6, 1869 |work=NCAA Football |publisher=[[ESPN.com]] |accessdate=August 4, 2014}}</ref><ref name="Beginnings, p2">[[#PFRA|PFRA Research]], "No Christian End!", p.&nbsp;2</ref> The game developed slowly; the first rules were drafted in October 1873, and only consisted of twelve guidelines.<ref name="Beginnings, p3">[[#PFRA|PFRA Research]], "No Christian End!", p.&nbsp;3</ref> Even though the number of teams participating in the sport increased, the game was still effectively controlled by the College of New Jersey, who claimed eight national championships in ten years. Only [[Yale University|Yale]] presented any form of challenge, claiming four national championships in the same time period.{{#tag:ref|Multiple teams were selected as national champion in several different seasons. Between 1869 and 1880, there were only four years when one team was selected as national champion by all selectors.|group="A"}}<ref name="Page 78">[[#All-Time|NCAA (2009)]], p.&nbsp;78</ref>

The Naval Academy's first ever football team was fielded in [[1879 Navy Midshipmen football team|1879]]. The squad was entirely student-operated, receiving no official support from Naval Academy officials. The team was entirely funded by its members and their fellow students.<ref name="1879 picture">{{Cite web |author=United States Naval Academy staff |publisher=[[United States Naval Academy]] |url=http://www.usna.edu/LibExhibits/archives/images/2780.jpg |title=Navy's First Football Squad |work=The Team of 1879 |year=1879 |accessdate=June 13, 2013}}</ref><ref name="Patterson 21">[[#Memorabilia|Patterson (2000)]], p.&nbsp;21</ref> The 1879 team participated in just one game, which resulted in a scoreless tie. It was played against the Baltimore Athletic Club, apparently on the Academy superintendent's cow pasture.<ref name="Patterson 21"/><ref name="Page 191">[[#Location|Kiland and Howren (2007)]], p.&nbsp;191</ref> Navy would not field a football team in 1880 or 1881, due to the lack of support from officials.<ref name="1879 picture"/><ref name="Patterson 21"/> When football returned to the academy in 1882, the squad was led by [[player-coach]] [[Vaulx Carter]], and won 8–0 in a match with Johns Hopkins, starting the [[Navy–Johns Hopkins football rivalry|seven-year rivalry]] between the schools.<ref name="CFDB">{{cite web |author=Staff |year=2013 |url=http://www.cfbdatawarehouse.com/data/div_ia/independents/navy/1880-1884_yearly_results.php |title=1880-1884 Yearly Results |work=Navy History{{ndash}}Yearly Results |publisher=[[College Football Data Warehouse]] |accessdate=June 13, 2013}}</ref> The 1883 season resulted in Navy's first ever loss, a 2{{ndash}}0 defeat by Johns Hopkins.<ref name="Bealle 10">[[#Bealle|Bealle (1951)]], p.&nbsp;10</ref>

==Schedule==
{{CFB Schedule Start|time=no|rank=no|ranklink=|rankyear=|tv=no|attend=yes}}
{{CFB Schedule Entry
| date         = November 27
| time         = no
| w/l          = w
| nonconf      = 
| rank         = no
| opponent     = {{cfb link|year=1884|team=Johns Hopkins Blue Jays|title=Johns Hopkins}}
| site_stadium = Unknown
| site_cityst  = [[Annapolis, Maryland|Annapolis, MD]]<ref name="CFDB"/>
| tv           = no
| score        = 9–6
| gamename     = [[Navy–Johns Hopkins football rivalry|Rivalry]]
| attend       = &nbsp;{{#tag:ref|No records exist to provide attendance figures for the game|group="A"}}
}}
{{CFB Schedule End|rank=no|poll=|timezone=|ncg=no|hc=no}}

==Season summary==

===Navy 9, Johns Hopkins 6===
The sole game of Navy's 1884 season was the annual competition against rival [[Johns Hopkins Blue Jays|Johns Hopkins]], the third consecutive playing of the series. In what was the final season where the rivalry was the only game of the year, Navy defeated Johns Hopkins 9{{ndash}}6.<ref name="Page 154">[[#All Time|Naval Academy Athletic Association (2005)]], p.&nbsp;154</ref> The game, played on November 27, was hosted by the Academy, likely on an unused drill or parade field.<ref name="Page 191"/> In an unusual agreement between the two schools, the contest was played entirely under [[rugby football|rugby]] rules. In the first half, Hopkins scored twice, on a [[touchdown]] from Mr. Bonsall and on a two-point [[Safety (gridiron football score)|safety]]. Navy scored twice in the second half of the game, when [[halfback (American football)|halfback]] Julius Dashiell, brother of Hopkins' [[Paul Dashiell]], kicked a [[Field goal (American and Canadian football)|five-point goal]] and rusher [[David W. Taylor]] scored a touchdown to secure a victory.<ref name="Bealle 11">[[#Bealle|Bealle (1951)]], p.&nbsp;11</ref>

The game was somewhat marred by one of the players suffering a broken collarbone and another spraining an ankle.<ref>{{cite news|title=Thanksgiving Observances|url=http://chroniclingamerica.loc.gov/lccn/sn84038114/1884-11-28/ed-1/seq-1/|accessdate=7 February 2017|work=Daily Republican|date=November 28, 1884|location=Wilmington, Delaware|page=1}}</ref>

==Players==
The 1884 Naval Academy team was made up of eleven players at four different positions. The squad consisted of seven rushers, one fullback, two halfbacks, and a quarterback:<ref name="Bealle 11"/>
{{Col-begin}}
{{Col-2}}
'''[[Offensive linemen|Rushers]]'''
* Cornelius Billings 
* Buell Franklin
* Jim Kittrell (capt.)
* Bill Miller
* Fred Moore
* Clarence Stone
* [[David W. Taylor|Dave Taylor]]
{{Col-2}}
[[File:Admiral David W. Taylor.jpg|thumb|150px|right|[[David W. Taylor|Dave Taylor]], who would later become a [[rear admiral (United States)|rear admiral]], was a rusher on the 1884 team|alt=A black-and-white image of a man in a naval uniform]]
'''[[Fullback (American football)|Fullbacks]]'''
* Joe Ricketts

'''[[Halfback (American football)|Halfbacks]]'''
* Julius Dashiell
* Pat McGuinness

'''[[Quarterback]]'''
* George Slocum
{{Col-2}}
{{Col-end}}

==Postseason and aftermath==
The first postseason college football game was not played until 1902, with the [[Pasadena Tournament of Roses]]' establishment of the east–west tournament game, later known as the [[Rose Bowl Game|Rose Bowl]].<ref name="Rose Bowl">{{cite web |author=Staff |publisher=[[Pasadena Tournament of Roses]] |url=http://www.tournamentofroses.com/history/ |title=Tournament of Roses History |work=Pasadena Tournament of Roses |year=2005 |accessdate=August 4, 2014 |archiveurl=https://web.archive.org/web/20070102063138/http://www.tournamentofroses.com/history/ |archivedate=January 2, 2007}}</ref> The Midshipmen did not participate in their first Rose Bowl until the 1923 season, when they went 5–1–2 and tied with the [[Washington Huskies football|Washington Huskies]] 14–14 in the match.<ref name="1924 Game Results">{{cite news |last=Eckersall |first=Walter |newspaper=[[The Detroit Free Press]] |date=January 2, 1924 |title=Annual East–West Football Battle Ends In 14–14 Tie |page=16 |ISSN=1055-2758}}</ref> As a result of the lack of a competition, there were no postseason games played after the 1882 season. According to statistics compiled by the National Championship Foundation, [[Parke Davis]], and the [[Helms Athletic Foundation]], Yale was declared the 1884 season champion. However, the Billingsley college football research center and Parke Davis also selected Princeton as the 1884 national champions.<ref name="Page 78"/>

The 1884 win over Johns Hopkins brought the Naval Academy's overall win-loss record to positive, as well as once again giving the Midshipmen a lead over Hopkins in their rivalry. The season marked the final time a team for the Naval Academy would play a single-game season. In 1885, their schedule was expanded to three games. It also marked the final time a Navy team finished a season unbeaten and untied; the closest a squad would come was in 1926, when they went 9{{ndash}}0{{ndash}}1. Navy finished the 1880s with four winning seasons, and an overall record of 14–12–2. The school outscored their opponents 292–231, and finished the 19th century with an overall record of 54–19–3.<ref name="Page 154"/><ref name="Bealle 11"/>

==References==
;Notes
{{Reflist|group=A}}
;Footnotes
{{reflist|2}}
;Bibliography 
{{refbegin}}
* {{cite book |ref=Bealle |last=Bealle |first=Morris Allson |year=1951 |title=Gangway for Navy: The Story of Football At United States Naval Academy, 1879-1950 |chapter=1884 |location=[[Washington, D.C.]] |publisher=Columbia Publishing Company |OCLC=1667386}}
* {{cite book |ref=Location |last=Kiland |first=Taylor Baldwin |last2=Howren |first2=Jamie |year=2007 |title=A Walk in the Yard: A Self-Guided Tour of the U.S. Naval Academy |publisher=[[United States Naval Institute]] |location=[[Annapolis, Maryland|Annapolis, MD]] |ISBN=1-59114-436-1 |OCLC=72799100}}
* {{cite book |ref=Kroll |last=Kroll |first=C. Douglas |year=2002 |title=Commodore Ellsworth P. Bertholf: First Commandant of the Coast Guard |location=[[Annapolis, MD]] |publisher=[[Naval Institute Press]] |chapter=The Cadet Years |ISBN=1-55750-474-1}}
* {{cite web |ref=All Time |author=Naval Academy Athletic Association |publisher=United States Naval Academy Athletics |url=http://graphics.fansonly.com/photos/schools/navy/sports/m-footbl/auto_pdf/1935_05-FB-MG---9---History-15.pdf |title=Navy: Football History |format=PDF |work=2005 Navy Midshipmen Football Media Guide |year=2005 |accessdate=August 4, 2014}}
* {{cite report |author=Staff |year=2009 |url=http://fs.ncaa.org/Docs/stats/football_records/DI/2009/2009FBS.pdf |at=National Poll Champions |title=Football Bowl Subdivision Records |publisher=[[National Collegiate Athletic Association]] |work=2009 NCAA Division I Football Records |accessdate=August 4, 2014 |format=PDF |type=Record book |ref=All-Time}}
* {{cite book |ref=Memorabilia |last=Patterson |first=Ted |year=2000 |title=Football in Baltimore: History and Memorabilia |location=[[Baltimore, Maryland|Baltimore, MD]] |publisher=[[Johns Hopkins University Press|The Johns Hopkins University Press]] |isbn=0-8018-6424-0}}
* {{cite web |ref=PFRA |author=PFRA Research |title=No Christian End! The Beginnings of Football in America |work=The Journey to Camp: The Origins of American Football to 1889 |publisher=[[Professional Football Researchers Association]] |url=http://www.profootballresearchers.org/Articles/No_Christian_End.pdf |format=PDF |accessdate=August 4, 2014}}
{{refend}}

{{Navy Midshipmen football navbox}}

[[Category:1884 college football season|Navy]]
[[Category:Navy Midshipmen football seasons]]
[[Category:College football undefeated seasons]]
[[Category:1884 in Maryland|Navy Midshipmen]]