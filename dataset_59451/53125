{{Infobox journal
| title = Willdenowia
| cover = [[File:Willdenowia_cover.jpg]]
| editor = Nicholas Turland
| discipline = [[Plant taxonomy]]
| abbreviation = Willdenowia
| formernames = Notizblatt des Königlichen Botanischen Gartens und Museums zu Berlin
| publisher = [[Botanical Garden in Berlin|Botanic Garden and Botanical Museum Berlin-Dahlem]], [[Free University of Berlin]]
| country = Germany
| frequency = Triannual
| history = 1895–present
| impact =0.721 
| impact-year =2014
| openaccess = Yes
| website = http://www.bgbm.org/de/bgbm-press/willdenowia
| link1 = http://www.bioone.org/loi/will
| link1-name = Online edition (1996-present)
| link2 =http://www.jstor.org/journals/05119618.html
| link2-name =Pre-1996 content on JSTOR
| JSTOR = 05119618
| OCLC = 224487211
| LCCN = sf81001129
| CODEN = 
| ISSN = 0511-9618
| eISSN = 1868-6397
}}
'''''Willdenowia''': Annals of the Botanic Garden and Botanical Museum Berlin-Dahlem'' is a triannual [[peer review|peer-reviewed]] [[scientific journal]] on plant, algal, and fungal [[Taxonomy (biology)|taxonomy]] published by [[Botanical Garden in Berlin|Botanic Garden and Botanical Museum Berlin-Dahlem]]. It was established in 1895 as ''Notizblatt des Königlichen Botanischen Gartens und Museums zu Berlin,'' and was renamed to the current title in 1954 to honor botanist [[Carl Ludwig Willdenow]] (1765-1812), director of the Botanical Garden.<ref>{{cite web|url = http://www.bgbm.org/de/bgbm-press/willdenowia|title = Willdenowia|publisher = Botanic Garden and Botanical Museum Berlin (BGBM)|accessdate = 14 September 2015}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|2|
* [[Biological Abstracts]]
* [[BIOSIS Previews|BIOSIS]]
* [[CAB Direct (database)|CAB Abstracts]]
* [[CAB International]]
* [[Current Contents]]/Agriculture, Biology & Environmental Sciences
* [[Kew Bibliographic Database]]
* [[Phytomed-Select]]
* [[Science Citation Index Expanded]]
* [[Scopus]]
}}

== References ==
{{reflist}}

== External links ==
*[http://www.bioone.org/loi/will Online edition (BioOne)]

[[Category:Botany journals]]
[[Category:Publications established in 1895]]
[[Category:English-language journals]]
[[Category:Biannual journals]]
[[Category:1895 establishments in Germany]]