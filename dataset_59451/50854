{{Infobox journal
| title = Cardiology
| abbreviation = Cardiology
| formernames = Cardiologia, Cardiology, Heart Drug
| cover = [[File:CoverIssueCardiology.jpg]]
| discipline = [[Cardiology]]
| editor = Jeffrey S. Borer
| publisher = [[Karger]]
| country = 
| frequency = Monthly
| history = 1937-present
| impact = 1.994
| impact-year = 2015
| website = http://www.karger.com/crd
| ISSN = 0008-6312
| eISSN = 1421-9751
| CODEN = CAGYAO
| OCLC = 02064881
}}
'''''Cardiology: International Journal of Cardiovascular Medicine, Surgery, Pathology and Pharmacology''''' is a monthly [[peer-reviewed]] [[medical journal]] published by [[Karger]]. The [[editor-in-chief]] is J. S. Borer. It was established in 1937 as ''Cardiologia'' by Bruno Kisch and Wilhelm Löffler. From 1971, the journal was published under the name ''Cardiology'' and in 2005 it incorporated the medical journal ''Heart Drug'' and obtained its current name.

==Scope==
The journal covers clinical, pre-clinical, and fundamental research as well as topical comprehensive reviews in selected areas of [[cardiovascular disease]]. Following the incorporation of the journal ''Heart Drug'', ''Cardiology'' has included coverage of issues relating to cardiovascular [[clinical pharmacology]] and drug trials.

==Abstracting and indexing==
The journal is abstracted and indexed in:
* [[Current Contents]]/Life Sciences
* [[PubMed]]/[[MEDLINE]]
* [[Biological Abstracts]]
* [[Excerpta Medica]]
* [[Science Citation Index]]
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 1.994.<ref name=WoS>{{cite book |year=2016 |chapter=Cardiology |title=2016 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

==External links==
* {{Official website|http://www.karger.com/crd}}

[[Category:Cardiology journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1937]]
[[Category:English-language journals]]
[[Category:Karger academic journals]]