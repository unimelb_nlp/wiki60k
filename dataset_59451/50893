{{Infobox journal
| title = Child Language Teaching and Therapy 
| cover = [[File:Child Language Teaching and Therapy.jpg]]
| editor =  Judy Clegg 
| discipline = [[computational linguistics/ Natural language processing]]
| former_names = 
| abbreviation = child. lang. teach. ther.
| publisher = [[SAGE Publications]]
| country = United Kingdom 
| frequency = Three Times a year    
| history = -1985-present
| openaccess = 
| license = 
| impact = 0.553
| impact-year = 2010
| website = http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal201804
| link1 = http://clt.sagepub.com/content/current
| link1-name = Online access
| link2 = http://.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC =42662298 
| LCCN = 85642008
| CODEN = 
| ISSN = 0265-6590
| eISSN = 1477-0865}}

'''''Child Language Teaching and Therapy''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that publishes papers in the fields of [[Linguistics]] and [[Education]]. The journal's [[Editor-in-Chief|editors]] are Judy Clegg ([[University of Sheffield]]) and Maggie Vance ([[University of Sheffield]]). It has been in publication since 1985 and is currently published by [[SAGE Publications]].

== Scope ==
''Child Language Teaching and Therapy'' focuses on children’s written and spoken language needs. The journal publishes research and review articles of relevance and which are of an inter-disciplinary nature. ''Child Language Teaching and Therapy'' publishes regular special issues on specific subject areas as well as keynote reviews of significant topics.

== Abstracting and indexing ==
''Child Language Teaching and Therapy'' is abstracted and indexed in, among other databases:  [[SCOPUS]], and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2010 [[impact factor]] is 0.553, ranking it 75 out of 141 journals in the category ‘Linguistics’.<ref name=WoS>{{cite book |year=2011 |chapter=Journals Ranked by Impact: Linguistics |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-09-30 |work=Web of Science |postscript=.}}</ref> and 27 out of 36 journals in the category ‘Education, Special’. <ref name=WoS1>{{cite book |year=2011 |chapter=Journals Ranked by Impact: Education, Special |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-09-30 |work=Web of Science |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|1=http://clt.sagepub.com/}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]