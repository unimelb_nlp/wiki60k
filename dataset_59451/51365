{{Distinguish|Frontiers (publisher)}}
{{Infobox journal
| cover  = Frontiers_in_Ecology_and_Environment_-_December_2013_Cover.jpg
| caption =December 2013 cover
| editor = 
| discipline = [[Ecology]], [[environmental science]]
| former_names = 
| abbreviation = Front. Ecol. Environ. 
| publisher = Ecological Society of America 
| country = United States
| frequency =10/year 
| history =2003-present 
| openaccess = Special Issues; Dispatches; Life Lines
| license = 
| impact =  8.504
| impact-year = 2015
| website = http://www.frontiersinecology.org/ 
| link1  =  http://www.esajournals.org/loi/fron
| link1-name = Online access
| link2  =
| link2-name = 
| JSTOR  = 
| OCLC  = 50198052 
| LCCN  = 2003252462 
| CODEN  = 
| ISSN  = 1540-9295 
| eISSN  = 1540-9309 
}}
'''''Frontiers in Ecology and the Environment''''' is a [[Peer review|peer-reviewed]] scientific journal published by the [[Ecological Society of America]] (ESA). The journal, issued 10 times per year, consists of [[Peer review|peer-reviewed]], synthetic review articles on all aspects of ecology, the environment, and related disciplines, as well as short, high-impact research communications of broad interdisciplinary appeal. Additional features include editorials, breaking news (domestic and international), a letters section, job ads, and special columns.<ref name=home>
{{Cite web
 | title = Home
 | work = Frontiers in Ecology and the Environment
 | publisher = [[Ecological Society of America]]
 | date = March 2014
 | url = http://www.frontiersinecology.org/front/
 | accessdate = 2014-03-04}}</ref>

== Aims and scope ==
''Frontiers in Ecology and the Environment'' is a benefit of membership of the [[Ecological Society of America|ESA]]. International in scope and [[Interdisciplinarity|interdisciplinary]] in approach, ''Frontiers'' focuses on current [[ecology|ecological]] issues and [[Natural environment|environmental]] challenges.

''Frontiers'' is aimed at professional ecologists and scientists working in related disciplines. With content that is timely, interesting, and accessible, even to those reading outside their own area of expertise, it has a broad, interdisciplinary appeal and is relevant to all users of ecological science, including policy makers, resource managers, and educators.

''Frontiers'' covers all aspects of ecology, the environment, and related subjects, focusing on global issues, broadly impacting [[research]], cross-disciplinary or multi-country endeavors, new techniques and technologies, new approaches to old problems, and practical applications of ecological science.

The journal is sent to all ESA members as part of their membership, and is also available by subscription to non-members and institutional libraries.<ref name=about/>

==Abstracting and indexing==
''Frontiers in Ecology and the Environment'' is covered by Current Contents
Agriculture, Biology, and Environmental Sciences, Science Citation Index,
ISI Alerting Services, Cambridge Scientific Abstracts, Biobase, Geobase, Scopus, CAB
Abstracts, and EBSCO Environmental Issues and Policy Index.<ref name=about>
{{Cite web
 | title = General Information
 | work = Frontiers in Ecology and the Environment
 | publisher = [[Ecological Society of America]]
 | date = March 2014
 | url = http://www.frontiersinecology.org/front/general-information/
 | accessdate = 2014-03-04}}</ref>
{{columns-list|colwidth=30em
* ''[[Current Contents]]/ Agriculture, Biology, and Environmental Sciences''
* ''[[Science Citation Index]]''
* ''[[CSA (database company)|Cambridge Scientific Abstracts]]''
* ''[[Elsevier BIOBASE]]''
* ''[[GEOBASE (database)|GEOBASE]]''
* ''[[Scopus]]''
* ''[[CAB Direct (database)|CAB Abstracts]]''
* ''[[EBSCO Industries|EBSCO]] - [[Environmental Issues and Policy Index]]''
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 7.615, ranking it 3nd{{Clarify|date=March 2014|reason=Should this be '2nd', '3rd' or something else?}} out of 210 journals in the category "Environmental Sciences"<ref name=WoS>{{cite book |year=2013 |chapter=Journals Ranked by Impact: Environmental Sciences |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2012-08-16 |series=[[Web of Science]] |postscript=.}}</ref> and 6th out of 136 journals in the category "Ecology".<ref name=WoS2>{{cite book |year=2013 |chapter=Journals Ranked by Impact: Ecology |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2012-08-16 |series=[[Web of Science]] |postscript=.}}</ref>

==References==
{{reflist}}

==External links==
* {{Official website|http://www.frontiersinecology.org/}}

[[Category:Ecology journals]]
[[Category:English-language journals]]
[[Category:Publications established in 2003]]
[[Category:Review journals]]