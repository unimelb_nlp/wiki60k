{{Infobox journal
| cover = [[File:Journal of Development Economics.gif]]
| discipline = [[Development economics]]
| abbreviation = J. Dev. Econ.
| publisher = [[Elsevier]]
| country = [[Netherlands]]
| frequency = Bimonthly
| history = 1974-present
| impact = 1.837
| impact-year = 2015
| website = http://www.journals.elsevier.com/journal-of-development-economics
| link1 = http://www.sciencedirect.com/science/journal/03043878
| link1-name = Online access
| ISSN = 0304-3878
| OCLC = 1292659
| LCCN = 76647333
| CODEN = JDECDF
}}
The '''''Journal of Development Economics''''' is a bimonthly [[peer-reviewed]] [[academic journal]] published by [[Elsevier]]. It was established in 1974 and is considered the top field journal in [[development economics]].<ref>Kalaitzidakis, Pantelis, Theofanis P. Mamuneas, and Thanasis Stengos. 2002. "Ranking of Academic Journals and Institutions in Economics", ''Journal of the European Economics Association'' 1(6): 1346-66.</ref><ref>{{Cite web |url=https://research.stlouisfed.org/publications/review/09/05/Engemann.pdf |title=A Journal Ranking for the Ambitious Economist |last= |first= |date= |website= |publisher= |access-date=}}</ref><ref>{{Cite web |url=http://www.rcfea.org/RePEc/pdf/wp15_10.pdf |title=Kalaitzidakis, Pantelis, Theofanis P. Mamuneas, and Thanasis Stengos (2010) "An Updated Ranking of Academic Journals in Economics" |last= |first= |date= |website= |publisher= |access-date=}}</ref>

Its [[editor-in-chief]] from 1985 to 2003 was [[Pranab Bardhan]], who has been the longest-serving JDE editor till date. He followed [[T.N. Srinivasan]], and [[Lance Taylor (economist)|Lance Taylor]] as Editors since the journal was established in 1974. He was succeeded by [[Mark Rosenzweig (economist)|Mark Rosenzweig]] (2003-2009) and [[Maitreesh Ghatak]] (2009-2015). The current editor-in-chief is Andrew Foster, who started in 2016.<ref>{{Cite web|url=http://www.journals.elsevier.com/journal-of-development-economics/editorial-board|title=Journal of Development Economics Editorial Board|last=|first=|date=|website=|publisher=|access-date=2016-10-29}}</ref>

==See also==
*[[The Developing Economies (journal)|''The Developing Economies'']]
*[[The World Economy (journal)|''The World Economy'']]

==References==
{{Reflist}}

==External links==
* {{Official website|http://www.elsevier.com/wps/find/journaldescription.cws_home/505546/description#description}}

{{DEFAULTSORT:Journal Of Development Economics}}
[[Category:Economics journals]]
[[Category:Bimonthly journals]]
[[Category:English-language journals]]
[[Category:Elsevier academic journals]]
[[Category:Publications established in 1974]]
[[Category:Development studies journals]]


{{econ-journal-stub}}