{{infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = Cousin Bazilio (aka Dragon's Teeth)
| title_orig    = O Primo Basílio
| translator    = Mary Jane Serrano, [[Roy Campbell (poet)|Roy Campbell]], [[Margaret Jull Costa]]
| image         = 
| image_caption = 
| author        = [[José Maria de Eça de Queiroz]]
| illustrator   = 
| cover_artist  = 
| country       = Portugal
| language      = Portuguese
| series        = 
| genre         = 
| publisher     = 
| release_date  = [[1878 in literature|1878]]
| english_release_date = [[1889 in literature|1889]], [[1953 in literature|1953]], [[2003 in literature|2003]]
| media_type    = 
| pages         = 
| isbn          = 0-8371-3089-1
| preceded_by   = 
| followed_by   = 
}}
'''''O Primo Basílio''''' ("Cousin Bazilio") is one of the most highly regarded [[Literary realism|realist]] novels of the Portuguese author [[José Maria de Eça de Queiroz]], also known under the modernized spelling '''Eça de Queirós'''. He worked in the Portuguese consular service, stationed at 53 Grey Street, Newcastle upon Tyne, from late 1874 until April 1879. The novel was written during this productive period in his career, appearing in [[1878 in literature|1878]].

A [[bowdlerized]] translation of this book by Mary Jane Serrano under the title ''Dragon's Teeth: A Novel'' was published in the United States in 1889,<ref>See Preface to 1889 translation.</ref> still available as a [[Print on demand|print-on-demand]] title.<ref>ISBN 978-1-143-93942-6</ref> More accurate translations have since been published, first in 1953 by the poet [[Roy Campbell (poet)|Roy Campbell]]<ref>ISBN 978-0-85635-967-5 (1992 reprint of 1953 trans.)</ref> and then in 2003 by award-winning translator [[Margaret Jull Costa]].<ref>ISBN 978-1-903517-08-6</ref>

==Plot==
Jorge, a successful engineer and employee of a ministry and Luísa, a romantic and dreamy girl, star as the typical bourgeois couple of the Lisbon society of the 19th century.

There is a group of friends who attend the home of Jorge and Luísa: D. Felicidade (''Dr. Happiness''), a woman suffering from gas crises and is in love with the Councillor; Sebastião (''Sebastian''), a close friend of Jorge; Councillor Acacio, good scholar; Ernestinho; and maids Joana - hussy and flirty - and Juliana - angry, envious, spiteful and bitter woman, responsible for the conflict of the novel.

At the same time cultivating a formal and happy marriage with Jorge, Louise still maintains friendship with a former colleague, Leopoldina - called "Bread and Cheese" for her continuous betrayal and adultery. Luísa's happiness and safety become endangered when Jorge need to travel to work at Alentejo.

After the departure of her husband, Luísa is bored with nothing to do, in the doldrums and melancholy caused by the absence of her husband, and exactly in this meantime Bazilio comes from abroad. A womanizer and a "bon vivant", he doesn't take long to win the love of Luisa (they had date before Luisa meet Jorge). Luísa was a person with a strong romantic view of life as she usually read only novels, and Bazilio was the man of her dreams: he was rich and lived in France. The friendly love became an ardent passion and this causes Luisa to practice adultery. Meanwhile, Juliana is just waiting for a chance to blackmail Luisa.

Luisa and Bazilio send love letters to one another, but one of these letters is intercepted by maid Juliana - who starts blackmailing Luisa. Turned from a spoiled lady into a slave, Luisa starts to become sick. The ill-treatment she suffers from Juliana quickly takes her liveliness, undermining her health.

Jorge comes back and is not suspicious, as Luisa satisfies every whim of the maid while looking for all possible solutions until she finds the disinterested help of Sebastian, who sets a trap for Juliana, trying to get her arrested and ends up causing her death. It's a new era for Luisa, surrounded by the affection of Jorge, Joan and the new maid, Mariana. However, it is too late: weakened by life that had endured under the tyranny of Juliana, Luisa is affected by a violent fever. Luisa does not notice that Bazilio hasn't replied to her letters for months, and when the postman delivers the letter at her home, it grabs Jorge's attention due to it being addressed to Luisa and being sent from France. He reads the letter and discovers the adultery of his wife. The evidence of her betrayal makes him go into despair but nevertheless, he forgives her because of the strong love he feels for Luisa and due to her fragile health. The affection and care of her husband, friends and the doctor are useless: Luisa dies.

After that, Jorge dismisses the maids and moves with Sebastian. Bazilio returns to Lisbon - where he had fled, leaving Luisa without support - and as he learns of Luisas's death, he cynically comments to a friend: "If I had brought Alphonsine before." This part closes the book revealing that Bazilio is a mean person. As they walk down the street, his friend Visconde Reinaldo admonished Bazilio for having an affair with a "bourgeois". He also said he found the relationship "absurd", after all and told that Bazilio had done what he made for "hygiene". Bazilio confirms his suspicions. Luisa had been used then. There was no feeling. Luisa died, so without ever being loved by him.

==TV and film adaptations==
The first film adaptation of a work of Queiroz was a 1922 adaptation of ''Cousin Bazilio'' by [[George Pallu]], and the work was adapted to film 5 more times (including a 1959 Portuguese film, a 1935 Mexican film, the 1977 Spanish film ''Dios bendiga cada rincón de esta casa'', a 1969 West German film and a 2007 Brazilian adaptation simply titled ''Primo Basílio''<ref>[http://www.imdb.com/name/nm0211055/?ref_=tt_ov_wr Eça de Queiroz] in the [[IMDB]]</ref> produced/directed by [[Daniel Filho]], with [[Fábio Assunção]] in the role of Basílio, and [[Débora Falabella]] as Luísa, [[Reynaldo Gianecchini]] as Jorge and [[Glória Pires]] as Juliana, with the action moved from 19th century [[Lisbon]] to [[São Paulo]] of the 1950s around the time of the construction of [[Brasília]]).

In 1988 the Brazilian company [[Rede Globo]] produced a television adaptation of ''O Primo Basílio'' in 35 episodes, starring the then rising star [[Giulia Gam]] and renowned actors [[Marcos Paulo (actor)|Marcos Paulo]] and [[Tony Ramos]]. Although never rerun, this production contains some of the best dramatic moments delivered by the actors involved, especially the villainous Juliana, played by [[Marília Pêra]].

== Notes ==
{{Reflist}}

== External links ==
{{Portal|Portugal|Novels}}
* [https://books.google.com/books?id=y5UDAAAAYAAJ Free ebook of 1889 English translation]
* [http://purl.pt/11 Full text in Portuguese]
* [http://www.imdb.com/title/tt0207917/ The 1988 TV series in the IMDb database]
* [http://www.imdb.com/title/tt0827778/ The 2007 movie in the IMDb database]

{{José Maria de Eça de Queirós}}

[[Category:Novels by José Maria de Eça de Queiroz]]
[[Category:1878 novels]]
[[Category:Portuguese novels adapted into films]]
[[Category:Novels set in Lisbon]]


{{1870s-novel-stub}}