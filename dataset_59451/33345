{{Redirect|RWC}}
{{About|a rugby union tournament|the rugby league tournament|Rugby League World Cup}}
{{pp-semi-protected|small=yes}}
{{Infobox rugby league football competition
| current_season =
| logo           = Rugby World Cup Trophy.JPG
| alt            = A gold cup with two handles inscribed with "The International Rugby Football Board" and "The Web Ellis Cup"
| pixels         = 200 px
| caption        = The Webb Ellis Cup is awarded to the winner of the Rugby World Cup
| sport          = Rugby union
| founded        = 1987
| teams          = 20
| countrytag     = Regions
| country        = Worldwide
| gov_body       = [[World Rugby]]
| championtag    = 
| champion       = {{ru|New Zealand}}
| season         = [[2015 Rugby World Cup|2015]]
| next host      = {{flagicon|Japan}} [[2019 Rugby World Cup|Japan]]
| most_champs    = {{ru|New Zealand}}
| count          = 3
| website        = {{url|www.rugbyworldcup.com}}
| current        = 
}}
{{Infobox
|image1= [[File:Rugby World Cup 2007Opening Ceremony1.jpg|350 px]]
|alt1=A rugby field with drummers on the perimeter, and a large multicoloured flag in the middle.
|caption1 = The opening ceremony of the 2007 tournament
|alt= 
|bodyclass = hlist nowraplinks
|headerstyle = border-top: 1px solid #aaa
|header1 = Tournaments
|data2 =
* [[1987 Rugby World Cup|1987]]
* [[1991 Rugby World Cup|1991]]
* [[1995 Rugby World Cup|1995]]
* [[1999 Rugby World Cup|1999]]
* [[2003 Rugby World Cup|2003]]
* [[2007 Rugby World Cup|2007]]
* [[2011 Rugby World Cup|2011]]
* [[2015 Rugby World Cup|2015]]
* ''[[2019 Rugby World Cup|2019]]''
* ''[[2023 Rugby World Cup|2023]]''
}}
The '''Rugby World Cup''' is a men's [[rugby union]] tournament contested every four years between the top international teams. The tournament was first held in 1987, when the tournament was co-hosted by New Zealand and Australia.
[[New Zealand national rugby union team|New Zealand]] are the current champions, having defeated [[Australia national rugby union team|Australia]] in the final of the [[2015 Rugby World Cup|2015 tournament]] in [[England]].

The winners are awarded the [[William Webb Ellis Cup]], named after [[William Webb Ellis]], the [[Rugby School]] pupil who — according to a popular legend — invented rugby by picking up the ball during a football game.<!-- Not association football, which came after this alleged event --> Four countries have won the trophy; New Zealand have won it three times, two teams have won twice, [[Australia national rugby union team|Australia]] and [[South Africa national rugby union team|South Africa]], while [[England national rugby union team|England]] have won it once.

The tournament is administered by [[World Rugby]], the sport's international governing body. Sixteen teams were invited to participate in the inaugural tournament in 1987, however since 1999 twenty teams have taken part. Japan will host the [[2019 Rugby World Cup|next event in 2019]].

== Format ==

=== Qualification ===
{{Main|Rugby World Cup qualification}}
Qualifying tournaments were introduced for the [[1991 Rugby World Cup|second tournament]], where eight of the sixteen places were contested in a twenty-four-nation tournament.<ref name="Peatey59">Peatey (2011) p. 59.</ref> The [[1987 Rugby World Cup|inaugural World Cup]] in 1987, did not involve any qualifying process; instead, the 16 places were automatically filled by seven eligible [[World Rugby|International Rugby Football Board]] (IRFB, now World Rugby) member nations, and the rest by invitation.<ref name=Peatey34>Peatey (2011) p. 34.</ref>

In 2003 and 2007, the qualifying format allowed for eight of the twenty available positions to be filled by automatic qualification, as the eight quarter finalists of the previous tournament enter its successor. The remaining twelve positions were filled by [[continent]]al qualifying tournaments.<ref name="RNV38I9PG26">{{cite news| title=Doin' it the Hard Way |magazine=Rugby News |volume=38 |issue=9 |year=2007 |page=26| postscript=<!--None-->}}</ref> Positions were filled by three teams from the Americas, one from Asia, one from Africa, three from Europe and two from Oceania.<ref name="RNV38I9PG26" /> Another two places were allocated for [[repechage]]. The first repechage place was determined by a match between the runners-up from the Africa and Europe qualifying tournaments, with that winner then playing the Americas runner-up to determine the place.<ref name="RNV38I9PG27">{{cite news| title=Doin' it the Hard Way |magazine=Rugby News |volume=38 |issue=9 |year=2007 |page=27| postscript=<!--None-->}}</ref> The second repechage position was determined between the runners-up from the Asia and Oceania qualifiers.<ref name="RNV38I9PG27" />

The current format allows for 12 of the 20 available positions to be filled by automatic qualification, as the teams who finish third or better in the group (pool) stages of the previous tournament enter its successor (where they will be [[Single-elimination tournament#Seeding|seeded]]).<ref>{{cite news| url=http://news.bbc.co.uk/sport1/hi/rugby_union/7258823.stm | work=BBC News | title=Rankings to determine RWC pools | date=22 February 2008 |accessdate=13 February 2013}}</ref><ref name=2008Seedings>{{cite news|title=AB boost as World Cup seedings confirmed|url=http://www.stuff.co.nz/sport/rugby/news/281939|accessdate=13 February 2013|newspaper=stuff.co.nz|date=22 February 2008|agency=NZPA}}</ref> The qualification system for the remaining eight places is region-based, with a total eight teams allocated for Europe, five for Oceania, three for the Americas, two for Africa, and one for Asia. The last place is determined by an intercontinental play-off.<ref name=Qualifying>{{cite news| url=http://www.rugbyworldcup.com/home/news/newsid=2022896.html#caribbean+kick+rwc+2011+qualifying |title=Caribbean kick off for RWC 2011 qualifying |publisher=irb.com |date=3 April 2008 |accessdate=13 February 2012}}</ref>

=== Tournament ===
The 2015 tournament involved twenty nations competing over six weeks.<ref name=2008Seedings /><ref name="2015Fixtures">{{cite web| url=http://www.rugbyworldcup.com/fixtures |title=Fixtures |accessdate=21 July 2015 |publisher=World Rugby}}</ref> There were two stages, a pool and a knockout. Nations were divided into four pools, A through to D, of five nations each.<ref name="2015Fixtures" /><ref name="TournamentRules">{{cite web | publisher=World Rugby | title=Tournament Rules|url=http://www.rugbyworldcup.com/tournament-rules| accessdate=21 July 2015}}</ref> The teams were [[seed (sports)|seeded]] before the start of the tournament, with the seedings taken from the [[World Rugby Rankings|World Rankings]] in December 2012. The four highest-ranked teams were drawn into pools A to D. The next four highest-ranked teams were then drawn into pools A to D, followed by the next four. The remaining positions in each pool were filled by the qualifiers.<ref name=2008Seedings /><ref name=2015Seedings>{{cite news | url = http://tvnz.co.nz/rugby-news/2015-world-cup-seedings-take-shape-5226586 | title = 2015 Rugby World Cup seedings take shape |publisher = tvnz.co.nz | accessdate = 13 April 2014 | date = 20 November 2012 | agency = AAP}}</ref>

Nations play four pool games, playing their respective pool members once each.<ref name="TournamentRules" /> A [[Rugby union bonus points system|bonus points system]] is used during pool play. If two or more teams are level on points, a system of criteria is used to determine the higher ranked; the sixth and final criterion decides the higher rank through the official World Rankings.<ref name="TournamentRules" />

The winner and runner-up of each pool enter the knockout stage. The knockout stage consists of quarter- and semi-finals, and then the final. The winner of each pool is placed against a runner-up of a different pool in a quarter-final. The winner of each quarter-final goes on to the semi-finals, and the respective winners proceed to the final. Losers of the semi-finals contest for third place, called the 'Bronze Final'. If a match in the knockout stages ends in a draw, the winner is determined through [[extra time]]. If that fails, the match goes into [[sudden death (sport)|sudden death]] and the next team to score any points is the winner. As a last resort, a kicking competition is used.<ref name="TournamentRules" />

== History ==
{{Main|History of the Rugby World Cup}}
[[File:2011 Rugby World Cup Wales vs Samoa (6168183024).jpg|right|300px|thumb|alt=A player holds a ball in front of two opposing groups of eight players. Each group is crouched and working together to push against the other team.|A scrum between [[Samoa national rugby union team|Samoa]] (in blue) and [[Wales national rugby union team|Wales]] (in red) during the 2011 World Cup]]
Prior to the Rugby World Cup, there was no truly global rugby union competition, but there were a number of other tournaments. One of the oldest is the annual [[Six Nations Championship]], which started in 1883 as the [[Home Nations]] Championship, a tournament between [[England national rugby union team|England]], [[Ireland national rugby union team|Ireland]], [[Scotland national rugby union team|Scotland]] and [[Wales national rugby union team|Wales]]. It expanded to the Five Nations in 1910, when [[France national rugby union team|France]] joined the tournament. France did not participate from 1931 to 1939, during which period it reverted to  a Home Nations championship. In 2000, [[Italy national rugby union team|Italy]] joined the competition, which became the Six Nations.<ref name="history">{{cite web | url=http://www.6-nations-rugby.com/six-nations-history | title=A brief history of the Six Nations rugby tournament | publisher=6 Nations Rugby | accessdate=31 October 2007|archiveurl = https://web.archive.org/web/20071108180856/http://www.6-nations-rugby.com/six-nations-history |archivedate = 8 November 2007}}</ref>

Rugby union was also played at the [[Summer Olympic Games]], first appearing at the [[1900 Summer Olympics|1900 Paris games]] and subsequently at [[1908 Summer Olympics|London in 1908]], [[1920 Summer Olympics|Antwerp in 1920]], and [[1924 Summer Olympics|Paris again in 1924]]. France won the first gold medal, then Australasia, with the last two being won by the United States. However rugby union ceased to be on Olympic program after 1924.<ref name="WorldRugbyOlympics">{{cite web | url=http://www.worldrugby.org/olympics/history | title=History of Rugby in the Olympics | publisher= World Rugby | date = 9 November 2014 | accessdate= 21 July 2015}}</ref><ref name="olympics2">{{cite web | url = http://www.espnscrum.com/scrum/rugby/story/167592.html | title = Rugby and the Olympics | last = Richards | first = Huw | date = 26 July 2012 | accessdate = 13 April 2012 | publisher = ESPN}}</ref>{{efn|However an exhibition tournament did take place at the [[1936 Summer Olympics|1936 Games]]. Rugby will be reintroduced to the Olympics in 2016, but as men's and women's seven-a-side rugby ([[Rugby Sevens]]).<ref name="WorldRugbyOlympics" />}}

The idea of a Rugby World Cup had been suggested on numerous occasions going back to the 1950s, but met with opposition from most unions in the IRFB.<ref name="wcweb" /> The idea resurfaced several times in the early 1980s, with the [[Australian Rugby Union]] (ARU) in 1983, and the [[New Zealand Rugby Union]] (NZRU) in 1984 independently proposing the establishment of a world cup.<ref name=Collins13>Collins (2008), p. 13.</ref> A proposal was again put to the IRFB in 1985 and this time successfully passed 10–6. The delegates from Australia, France, New Zealand and South Africa all voted for the proposal, and the delegates from Ireland and Scotland against; the English and Welsh delegates were split, with one from each country for and one against.<ref name="wcweb">{{cite web | publisher=worldcupweb.com | title=The History of RWC |url=http://www.worldcupweb.com/WCrugby/history.asp|accessdate=25 April 2006|archiveurl = https://web.archive.org/web/20060414193531/http://www.worldcupweb.com/WCrugby/history.asp |archivedate = 14 April 2006|deadurl=yes}}</ref><ref name=Collins13 />

The inaugural tournament, jointly hosted by Australia and New Zealand, was held in May and June 1987, with sixteen nations taking part.<ref name=Peatey31>Peatey (2011) p. 31.</ref> [[New Zealand national rugby union team|New Zealand]] became the first ever champions, defeating [[France national rugby union team|France]] 29–9 in the final.<ref name=Peatey42>Peatey (2011) p. 42.</ref> The subsequent [[1991 Rugby World Cup|1991 tournament]] was hosted by England, with matches played throughout Britain, Ireland and France. This tournament saw the introduction of a qualifying tournament; eight places were allocated to the quarter-finalists from 1987, and the remaining eight decided by a thirty-five nation qualifying tournament.<ref name="Peatey59" /> [[Australia national rugby union team|Australia]] won the second tournament, defeating [[England national rugby union team|England]] 12–6 in the final.<ref name=Peatey77>Peatey (2011) p. 77.</ref>

In 1992, eight years after their last official series,{{efn|Against England in 1984.<ref name=Harding137>Harding (2000), p. 137</ref>}} [[South Africa national rugby union team|South Africa]] hosted New Zealand in a one-off test match. The resumption of international rugby in South Africa came after the dismantling of the [[apartheid]] system, and was only done with permission of the [[African National Congress]].<ref name=Harding137 /><ref name=Peatey78>Peatey (2011) p. 78.</ref> With their return to test rugby, South Africa were selected to host the [[1995 Rugby World Cup]].<ref name=Peatey82>Peatey (2011) p. 82.</ref> After upsetting Australia in the opening match, South Africa continued to advance through the tournament until they met New Zealand in the final.<ref name=Peatey87>Peatey (2011) p. 87.</ref><ref name=Harding159>Harding (2000), pp. 159–160</ref> After a tense final that went into [[Overtime (sports)|extra time]], South Africa emerged 15–12 winners,<ref name=Peatey99>Peatey (2011) p. 99.</ref> with then President [[Nelson Mandela]], wearing a Springbok [[Rugby shirt|jersey]],<ref name=Harding159 /> presenting the trophy to South Africa's captain, [[Francois Pienaar]].<ref name=Harding168>Harding (2000), p. 168</ref>

The tournament in [[1999 Rugby World Cup|1999]] was hosted by Wales with matches also being held throughout the rest of the United Kingdom, Ireland and France. The tournament included a [[repechage]] system, alongside specific regional qualifying places, and an increase from sixteen to twenty participating nations. Australia claimed their second title, defeating France in the final.

The [[2003 Rugby World Cup|2003 event]] was hosted by Australia, although it was originally intended to be held jointly with New Zealand. England emerged as champions defeating Australia in extra time. England's win was unique in that it broke the [[SANZAAR|southern hemisphere]]'s dominance in the event. Such was the celebration of England's victory, that an estimated 750,000 people gathered in central [[London]] to greet the team, making the day the largest sporting celebration of its kind ever in the United Kingdom.<ref name="bbcrwc">{{cite news | publisher=bbc.co.uk | title=England honours World Cup stars |url=http://news.bbc.co.uk/sport1/hi/rugby_union/rugby_world_cup/3300061.stm|accessdate=2006-05-03 | date=2003-12-09}}</ref>

The [[2007 Rugby World Cup|2007 competition]] was hosted by France, with matches also being held in Wales and Scotland. South Africa claimed their second title by defeating defending champions England 15–6.  The [[2011 Rugby World Cup|2011 tournament]] was awarded to New Zealand in November 2005, ahead of bids from [[Japan]] and South Africa. The All Blacks reclaimed their place atop the rugby world with a narrow 8–7 win over France in the 2011 final.

In the [[2015 Rugby World Cup|2015 edition of tournament]], hosted by England, New Zealand once again won the final, this time against established rivals, Australia. In doing so, they became the first team in World Cup history to win three titles, as well as the first to successfully defend a title. It was also New Zealand's first title victory on foreign soil.

== Trophy ==
{{Main|Webb Ellis Cup}}
The Webb Ellis Cup is the prize presented to winners of the Rugby World Cup, named after [[William Webb Ellis]]. The trophy is also referred to simply as the ''Rugby World Cup''. The trophy was chosen in 1987 as an appropriate cup for use in the competition, and was created in 1906 by Garrard's Crown Jewellers.<ref name=RWCTrophy>{{cite news|title=Second World Cup exists, Snedden confirms|url=http://www.nzherald.co.nz/nz/news/article.cfm?c_id=1&objectid=10745840|accessdate=13 February 2013|newspaper=New Zealand Herald|date=18 August 2011}}</ref><ref name=QuinnPt3>{{cite news|last=Quinn|first=Keith|title=Keith Quinn: Back-history of RWC – part three|url=http://tvnz.co.nz/rugby-world-cup/keith-quinn-back-history-rwc-part-three-4374234|accessdate=13 February 2013|newspaper=TVNZ|date=30 August 2011}}</ref> The trophy is restored after each game by fellow Royal Warrant holder [[Thomas Lyte]].<ref name="BBC">{{cite web|url=http://www.bbc.co.uk/news/business-22213850|title=Friday Boss: Kevin Baker of silversmiths Thomas Lyte|work=BBC News}}</ref><ref name="RoyalWarrant">{{cite web|url=https://www.royalwarrant.org/company/thomas-lyte?back=%2Fdirectory%3Fquery%3D%26page%3D66|title=Thomas Lyte|work=royalwarrant.org}}</ref> The words 'The International Rugby Football Board' and 'The Webb Ellis Cup' are engraved on the face of the cup. It stands thirty-eight centimetres high and is silver gilded in gold, and supported by two cast scroll handles, one with the head of a [[satyr]], and the other a head of a [[nymph]].<ref name=WECup>{{cite web|title=The History of the Webb Ellis Cup|url=http://www.skysport.co.nz/rugby-webb-ellis-cup/|publisher=Sky Sport New Zealand|accessdate=13 February 2013}}</ref> In Australia the trophy is colloquially known as "Bill" — a reference to William Webb Ellis.

== Selection of hosts ==
{{Main|Rugby World Cup hosts}}
Tournaments are organised by Rugby World Cup Ltd (RWCL), which is itself owned by World Rugby. The selection of host is decided by a vote of World Rugby Council members.<ref>{{cite web | title=Official Website of the Rugby World Cup | url=http://www.rugbyworldcup.com/en/home/Disclaimer | work=rugbyworldcup.com | accessdate=14 April 2007| archiveurl = https://web.archive.org/web/20070202143044/http://www.rugbyworldcup.com/en/home/Disclaimer| archivedate = 2 February 2007}}</ref><ref name=JapEng2019>{{cite news|title=England awarded 2015 Rugby World Cup|url=http://www.abc.net.au/news/2009-07-29/england-awarded-2015-rugby-world-cup/1371384|accessdate=13 February 2013|newspaper=ABC News Australia|date=29 July 2009|agency=AFP}}</ref>  The voting procedure is managed by a team of independent auditors, and the voting kept secret. The allocation of a tournament to a host nation is now made five or six years prior to the commencement of the event, for example New Zealand were awarded the 2011 event in late 2005.

The tournament has been hosted by multiple nations. For example, the 1987 tournament was co-hosted by Australia and New Zealand. World Rugby requires that the hosts must have a venue with a capacity of at least 60,000 spectators for the final.<ref name=RWCStadium>{{cite news|title=New Zealand came close to losing Rugby World Cup 2011|url=http://www.rugbyweek.com/news/article.asp?id=17256|accessdate=13 February 2013|newspaper=Rugby Week|date=12 December 2008}}</ref> Host nations sometimes construct or upgrade stadia in preparation for the World Cup, such as [[Millennium Stadium]] – purpose built for the 1999 tournament – and [[Eden Park]], upgraded for 2011.<ref name=RWCStadium /><ref name="millen">{{cite web | title=Millennium Stadium, Cardiff  | work=Virtual Tourist | url=http://www.virtualtourist.com/travel/Europe/United_Kingdom/Wales/South_Glamorgan/Cardiff-315777/Things_To_Do-Cardiff-Millennium_Stadium-BR-1.html |accessdate=23 February 2007}}</ref> The first country outside of the traditional rugby nations of [[SANZAAR]] or the Six Nations to be awarded the hosting rights was Japan, who will host the [[2019 Rugby World Cup|2019 tournament]].

== Tournament growth ==

=== Media coverage ===
Organizers of the Rugby World Cup, as well as the Global Sports Impact, state that the Rugby World Cup is the third largest sporting event in the World, behind only the [[FIFA World Cup]] and the [[Olympics]],<ref>{{cite web |url=http://hospitality.rugbyworldcup.com/rugby_world_cup_2015_official_hospitality_in_numbers.aspx |title=Rugby World Cup 2015 Official Hospitality |author= |publisher=RWC Ltd |date= |accessdate=2014-12-04}}</ref><ref>http://www.bbc.com/sport/30326825</ref> although other sources question whether this is accurate.<ref>{{Cite news|url=http://www.nzherald.co.nz/sport/news/article.cfm?c_id=4&objectid=10761073|title=Rugby World Cup: Logic debunks outrageous numbers game|date=2011-10-23|work=New Zealand Herald|access-date=2017-04-03|language=en-NZ|issn=1170-0777}}</ref>

Reports emanating from World Rugby and its business partners have frequently touted the tournament's media growth, with cumulative worldwide television audiences of 300 million for the inaugural 1987 tournament, 1.75 billion in 1991, 2.67 billion in 1995, 3 billion in 1999,<ref name="seven">{{cite web | title=Rugby World Cup 2003 | work=sevencorporate.com.au | url=http://www.sevencorporate.com.au/page.asp?partid=225 |accessdate=2006-04-25|archiveurl = https://web.archive.org/web/20060415041733/http://www.sevencorporate.com.au/page.asp?partid=225 |archivedate = April 15, 2006|deadurl=yes}}</ref> 3.5 billion in 2003,<ref name="visa">{{cite web | title=Visa International Renews Rugby World Cup Partnership | work=corporate.visa.com | url=http://www.corporate.visa.com/md/nr/press269.jsp | archiveurl=https://web.archive.org/web/20060427071525/http://www.corporate.visa.com/md/nr/press269.jsp | archivedate=2006-04-27 |accessdate=2006-04-25}}</ref> and 4 billion in 2007.<ref name=Deloitte2008>{{cite web | url = http://www.deloitte.com/assets/dcom-unitedkingdom/Local%20assets/documents/uk_SBG_IrB2008.pdf |title = Potential Impact of the Rugby World Cup on a Host Nation | year = 2008 | accessdate = 12 April 2014 | publisher = Deloitte & Touche | page = 5}}</ref><br /> The 4 billion figure was widely dismissed as the global audience for television is estimated to be about 4.2 billion.<ref>{{cite web |url=http://www.vrworld.com/2011/01/26/digital-divide-global-household-penetration-rates-for-technology/  |title=Digital Divide: Global Household Penetration Rates for Technology  |author= |publisher=VRWorld |date= |accessdate=2015-09-01}}</ref>

However, independent reviews have called into question the methodology of those growth estimates, pointing to factual inconsistencies.<ref>{{cite web |url=http://www.nzherald.co.nz/business/news/article.cfm?c_id=3&objectid=10642274 |title=Filling the Cup – cost $500m and climbing |author=Nippert, Matt |work = New Zealand Herald |publisher=APN New Zealand |date=2010-05-02 |accessdate=2014-12-02}}</ref> The event's supposed drawing power outside of a handful of rugby strongholds was also downplayed significantly, with an estimated 97 percent of the 33 million average audience produced by the 2007 final coming from [[Australasia]], South Africa, the [[British Isles]] and France.<ref>{{cite web |url=http://www.nzherald.co.nz/sport/news/article.cfm?c_id=4&objectid=10761073 |title=Logic debunks outrageous numbers game |author=Burgess, Michael|work = New Zealand Herald |publisher=APN New Zealand |date=2011-10-23 |accessdate=2014-12-02}}</ref> Other sports have been accused of exaggerating their television reach over the years; such claims are not exclusive to the Rugby World Cup.

While the event's global popularity remains a matter of dispute, high interest in traditional rugby nations is well documented. The 2003 final, between Australia and England, became the most watched [[rugby union]] match in the history of Australian television.<ref name="tv">{{cite news | last=Derriman | first=Phillip | date = 2006-07-01 | title=Rivals must assess impact of Cup fever | work = Sydney Morning Herald | publisher = Fairfax | url=http://www.smh.com.au/news/sport/rivals-must-assess-impact-of-cup-fever/2006/06/30/1151174394285.html?page=2 |accessdate=2006-07-01}}</ref>

=== Attendance ===
{{See also|List of sports attendance figures}}
{| class="wikitable sortable" style="text-align: center;"
|+ Attendance figures<ref name=2012YearInReview>{{cite book| title = International Rugby Board Year in Review 2012| page = 62 | url = https://images.worldrugby.org/view-item?i=38261 | publisher = International Rugby Board | accessdate = 21 July 2015}}</ref>
|-
!scope="col"|Year
!scope="col"|Host(s)
!scope="col"|Total attendance
!scope="col"|Matches
!scope="col"|Avg attendance
!scope="col"|% change <br />in avg att.
!scope="col"|Stadium capacity
!scope="col"|Attendance as <br />% of capacity
|-
| [[1987 Rugby World Cup|1987]]
|[[Australia]], [[New Zealand]]
||604,500
||32
||20,156
|| —
|| 1,006,350
|| 60%
|-
|  [[1991 Rugby World Cup|1991]]
|[[England]], [[Wales]], [[France]],<br /> [[Ireland]], [[Scotland]]
||1,007,760
||32
||31,493
|| +56%
|| 1,212,800
|| 79%
|-
| [[1995 Rugby World Cup|1995]]
|[[South Africa]]
||1,100,000
||32
||34,375
|| +9%
|| 1,423,850
|| 77%
|-
|  [[1999 Rugby World Cup|1999]]
|[[Wales]]
||1,750,000
||41
||42,683
|| +24%
|| 2,104,500
|| 83%
|-
|  [[2003 Rugby World Cup|2003]]
|[[Australia]]
||1,837,547
||48
||38,282
|| –10%
|| 2,208,529
|| 83%
|-
|  [[2007 Rugby World Cup|2007]]
|[[France]]
||2,263,223
||48
||47,150
|| +23%
|| 2,470,660
|| 92%
|-
|  [[2011 Rugby World Cup|2011]]
|[[New Zealand]]
||1,477,294
||48
||30,777
|| –35%
|| 1,732,000
|| 85%
|-
|  [[2015 Rugby World Cup|2015]]
|[[England]]
||2,477,805
||48
||51,621
||+68%
||2,600,741
||95%
|}

=== Revenue ===
{| class="wikitable"  style="text-align: center;"
|+ Revenue for Rugby World Cup tournaments<ref name=2012YearInReview />
|-
!scope="col"|Source
!scope="col"|[[1987 Rugby World Cup|1987]]
!scope="col"|[[1991 Rugby World Cup|1991]]
!scope="col"|[[1995 Rugby World Cup|1995]]
!scope="col"|[[1999 Rugby World Cup|1999]]
!scope="col"|[[2003 Rugby World Cup|2003]]
!scope="col"|[[2007 Rugby World Cup|2007]]
!scope="col"|[[2011 Rugby World Cup|2011]]
![[2015 Rugby World Cup|2015]]
|-
! scope="row" |Gate receipts (M [[Pound sterling|£]])
|| --
|| --
||15
||55
||81
||147
||131
| -
|-
! scope="row" |Broadcasting (M £)
|| --
|| --
||19
||44
||60
||82
||93
| -
|-
! scope="row" |Sponsorship (M £)
|| --
|| --
||8
||18
||16
||28
||29
| -
|}

Notes:
* The host union keeps revenue from gate receipts. World Rugby, through RWCL, receive revenue from sources including broadcasting rights, sponsorship and tournament fees.<ref name=2012YearInReview />

== Results ==

=== Tournaments ===
{| class="wikitable" style="font-size:90%; width: 100%; text-align: center;"
|-
! rowspan="2" style="width:5%;"|Year
! rowspan="2" style="width:12%;"|Host(s)
| style="width:1%;" rowspan="11"|
!colspan=3|Final
| style="width:1%;" rowspan="11"|
!colspan=3|Bronze Final
!width=1% rowspan=11 style="background: #ffffff"|
!rowspan=2 width=4%|Number of teams
|-
!width=15%|Winner
!width=8%|Score
!width=15%|Runner-up
!width=15%|3rd place
!width=8%|Score
!width=15%|4th place
|-
| [[1987 Rugby World Cup|1987]]
|align=left|{{runionflag|AUS}} &<br />{{runionflag|NZL}}
|'''{{ru-big|New Zealand}}'''
|'''[[1987 Rugby World Cup Final|29–9]]'''
|{{ru-big|France}}
|{{ru-big|Wales}}
|'''22–21'''
|{{ru-big|Australia}}
|16
|- style="background: #D0E6FF;"
| [[1991 Rugby World Cup|1991]]
|align=left|{{runionflag|ENG}},<br />{{runionflag|FRA}},<br />{{runionflag|IRL}}<br />{{runionflag|SCO}} &<br /> {{runionflag|WAL}}
|'''{{ru-big|Australia}}'''
|'''[[1991 Rugby World Cup Final|12–6]]'''
|{{ru-big|England}}
|{{ru-big|New Zealand}}
|'''13–6'''
|{{ru-big|Scotland}}
|16
|-
| [[1995 Rugby World Cup|1995]]
|align=left|{{runionflag|RSA}}
|'''{{ru-big|South Africa}}'''
|'''[[1995 Rugby World Cup Final|15–12]]'''<br />([[extra time|aet]])
|{{ru-big|New Zealand}}
|{{ru-big|France}}
|'''19–9'''
|{{ru-big|England}}
|16
|- style="background: #D0E6FF;"
| [[1999 Rugby World Cup|1999]]
|align=left|{{runionflag|WAL}},<br />{{runionflag|ENG}},<br />{{runionflag|FRA}},<br />{{runionflag|IRL}}<br />{{runionflag|SCO}} <br />
|'''{{ru-big|Australia}}'''
|'''[[1999 Rugby World Cup Final|35–12]]'''
|{{ru-big|France}}
|{{ru-big|South Africa}}
|'''22–18'''
|{{ru-big|New Zealand}}
|20
|-
| [[2003 Rugby World Cup|2003]]
|align=left|{{runionflag|AUS}}
|'''{{ru-big|England}}'''
|'''[[2003 Rugby World Cup Final|20–17]]'''<br />([[extra time|aet]])
|{{ru-big|Australia}}
|{{ru-big|New Zealand}}
|'''40–13'''
|{{ru-big|France}}
|20
|- style="background: #D0E6FF;"
| [[2007 Rugby World Cup|2007]]
|align=left|{{runionflag|FRA}}
|'''{{ru-big|South Africa}}'''
|'''[[2007 Rugby World Cup Final|15–6]]'''
|{{ru-big|England}}
|{{ru-big|Argentina}}
|'''34–10'''
|{{ru-big|France}}
|20
|-
| [[2011 Rugby World Cup|2011]]
|align=left|{{runionflag|NZL}}
|'''{{ru-big|New Zealand}}'''
|'''[[2011 Rugby World Cup Final|8–7]]'''
|{{ru-big|France}}
| {{ru-big|Australia}}
|'''21–18'''
| {{ru-big|Wales}}
|20
|- style="background: #D0E6FF;"
| [[2015 Rugby World Cup|2015]]
|align=left|{{runionflag|ENG}}
|'''{{ru-big|New Zealand}}'''
|'''[[2015 Rugby World Cup Final|34–17]]'''
| {{ru-big|Australia}}
| {{ru-big|South Africa}}
|'''24–13'''
| {{ru-big|Argentina}}
|20
|-
| [[2019 Rugby World Cup|2019]]
|align=left|{{runionflag|JPN}}
|colspan=3|Future events
|colspan=3|Future events
|20
|-
|}

=== Performance of nations ===
[[File:Rugby world cup countries best results and hosts rev1.png|thumb|right|Map of nations' best results (excluding qualifying tournaments).]]
{{See also|National team appearances in the Rugby World Cup}}
Twenty-five nations have participated at the Rugby World Cup (excluding qualifying tournaments). Of the eight tournaments that have been held, all but one have been won by a national team from the [[southern hemisphere]].<ref name="RNV38I9PG32-33">{{cite news |magazine=Rugby News |title=Only the Strong Survive |volume=38 |issue=9 |pages=32–33 |year=2007 |postscript=<!--None-->}}</ref> The southern hemisphere's dominance has been broken only in 2003, when England beat Australia in the final.<ref name="RNV38I9PG32-33" />

Thus far the only nations to host and win a tournament are New Zealand (1987 and 2011) and South Africa (1995). The performance of other host nations includes England (1991 final hosts) and Australia (2003 hosts) finishing runners-up. France (2007 hosts) finished fourth, while Wales (1999 hosts) failed to reach the semi-finals. Wales became the first host nation to be eliminated at the pool stages in 1991, while, England became the first solo host nation to be eliminated at the pool stages in 2015. Of the twenty-five nations that have ever participated in at least one tournament, twelve of them have never missed a tournament.{{efn|Argentina, Australia, Canada, England, France, Ireland, Italy, Japan, New Zealand, Romania, Scotland and Wales are the nations that have never missed a tournament, playing in all seven thus far. South Africa has played in all five in the post-apartheid era.}}

=== Team records ===
{| class="wikitable sortable" style="width:100%;"
|-
!width=13%|Team
!width=13%|Champions
!width=15%|Runners-up
!width=13%|Third
!width=13%|Fourth
!width=24%|Quarter-finals
!width=8%|Top 8 <br> {{tooltip|Apps|Appearances}}
|-
|'''{{ru|NZL}}'''
|'''3''' ([[1987 Rugby World Cup|1987]], [[2011 Rugby World Cup|2011]], [[2015 Rugby World Cup|2015]])
|'''1''' ([[1995 Rugby World Cup|1995]])
|'''2''' ([[1991 Rugby World Cup|1991]], [[2003 Rugby World Cup|2003]])
|'''1''' ([[1999 Rugby World Cup|1999]])
|'''1''' ([[2007 Rugby World Cup|2007]])
|'''8'''
|-
|'''{{ru|AUS}}'''
|'''2''' ([[1991 Rugby World Cup|1991]], [[1999 Rugby World Cup|1999]])
|'''2''' ([[2003 Rugby World Cup|2003]], [[2015 Rugby World Cup|2015]])
|'''1''' ([[2011 Rugby World Cup|2011]])
|'''1''' ([[1987 Rugby World Cup|1987]])
|'''2''' ([[1995 Rugby World Cup|1995]], [[2007 Rugby World Cup|2007]])
|'''8'''
|-
|'''{{ru|RSA}}'''
|'''2''' ([[1995 Rugby World Cup|1995]], [[2007 Rugby World Cup|2007]])
| –
|'''2''' ([[1999 Rugby World Cup|1999]], [[2015 Rugby World Cup|2015]])
| –
|'''2''' ([[2003 Rugby World Cup|2003]], [[2011 Rugby World Cup|2011]])
|'''6'''
|-
|'''{{ru|ENG}}'''
|'''1''' ([[2003 Rugby World Cup|2003]])
|'''2''' ([[1991 Rugby World Cup|1991]], [[2007 Rugby World Cup|2007]])
| –
|'''1''' ([[1995 Rugby World Cup|1995]])
|'''3''' ([[1987 Rugby World Cup|1987]], [[1999 Rugby World Cup|1999]], [[2011 Rugby World Cup|2011]])
|'''7'''
|-
|{{ru|FRA}}
| –
|'''3''' ([[1987 Rugby World Cup|1987]], [[1999 Rugby World Cup|1999]], [[2011 Rugby World Cup|2011]])
|'''1''' ([[1995 Rugby World Cup|1995]])
|'''2''' ([[2003 Rugby World Cup|2003]], [[2007 Rugby World Cup|2007]])
|'''2''' ([[1991 Rugby World Cup|1991]], [[2015 Rugby World Cup|2015]])
|'''8'''
|-
|{{ru|WAL}}
| –
| –
|'''1''' ([[1987 Rugby World Cup|1987]])
|'''1''' ([[2011 Rugby World Cup|2011]])
|'''3''' ([[1999 Rugby World Cup|1999]], [[2003 Rugby World Cup|2003]], [[2015 Rugby World Cup|2015]])
|'''5'''
|-
|{{ru|ARG}}
| –
| –
|'''1''' ([[2007 Rugby World Cup|2007]])
|'''1''' ([[2015 Rugby World Cup|2015]])
|'''2''' ([[1999 Rugby World Cup|1999]], [[2011 Rugby World Cup|2011]])
|'''4'''
|-
|{{ru|SCO}}
| –
| –
| –
|'''1''' ([[1991 Rugby World Cup|1991]])
|'''6''' ([[1987 Rugby World Cup|1987]], [[1995 Rugby World Cup|1995]], [[1999 Rugby World Cup|1999]], [[2003 Rugby World Cup|2003]], [[2007 Rugby World Cup|2007]], [[2015 Rugby World Cup|2015]])
|'''7'''
|-
|{{ru|IRE}}
| –
| –
| –
| –
|'''6''' ([[1987 Rugby World Cup|1987]], [[1991 Rugby World Cup|1991]], [[1995 Rugby World Cup|1995]], [[2003 Rugby World Cup|2003]], [[2011 Rugby World Cup|2011]], [[2015 Rugby World Cup|2015]])
|'''6'''
|-
|{{ru|FIJ}}
| –
| –
| –
| –
|'''2''' ([[1987 Rugby World Cup|1987]], [[2007 Rugby World Cup|2007]])
|'''2'''
|-
|{{ru|SAM}}
| –
| –
| –
| –
|'''2''' ([[1991 Rugby World Cup|1991]], [[1995 Rugby World Cup|1995]])
|'''2'''
|-
|{{ru|CAN}}
| –
| –
| –
| –
|'''1''' ([[1991 Rugby World Cup|1991]])
|'''1'''
|}

== Records and statistics ==
[[File:Gavin Hastings.jpg|thumb|right|alt=A middle-aged man wearing a suit and tie holding the Scottish flag.|[[Gavin Hastings]] is one of four players to have kicked a record eight penalties in a single World Cup match.]]{{Main|Records and statistics of the Rugby World Cup|Rugby World Cup try scorers|List of Rugby World Cup hat-tricks}}
The record for most points overall is held by English player [[Jonny Wilkinson]], who scored 277 over his World Cup career.<ref name="Peatey243">Peatey (2011) p. 243.</ref> [[Grant Fox]] of New Zealand holds the record for most points in one competition, with 126 in 1987;<ref name=Peatey243 /> [[Jason Leonard]] of England holds the record for most World Cup matches: 22 between 1991 and 2003.<ref name="Peatey243" /> [[Simon Culhane]] holds the record for most points in a match by one player, 45, as well as the record for most conversions in a match, 20.<ref name=RWCRecords>{{cite web | url = http://www.rugbyworldcup.com/statistics/ | title = All Time RWC Statistics | publisher = International Rugby Board | accessdate = 12 April 2014}}</ref> [[Marc Ellis (rugby footballer)|Marc Ellis]] holds the record for most tries in a match, six, which he scored against Japan in 1995.<ref name=Peatey244>Peatey (2011) p. 244.</ref>

All Black [[Jonah Lomu]] is the youngest player to appear in a final – aged 20 years and 43 days at the 1995 Final.<ref name=Peatey245>Peatey (2011) p. 245.</ref> Lomu shares 2 records with South African [[Bryan Habana]]. Most tries in a tournament (8): Lomu in 1999 and Habana in 2007 and total world cup tournament tries, both scored 15.<ref name=Peatey244 /> The record for most penalties in a match is 8, held by [[Matt Burke]], [[Gonzalo Quesada]], [[Gavin Hastings]] and [[Thierry Lacroix]],<ref name=RWCRecords /> and the record for most penalties in a tournament, 31, is held by Gonzalo Quesada. South Africa's [[Jannie de Beer]] kicked five drop-goals against England in 1999 – an individual record for a single World Cup match.<ref name=Peatey245 />

The most points scored in a game is 145 — by the All Blacks against [[Japan national rugby union team|Japan]] in 1995, while the widest winning margin is 142, held by Australia in a match against [[Namibia national rugby union team|Namibia]] in 2003.<ref name=Peatey242>Peatey (2011) p. 242.</ref>

A total of 16 players have been [[List of Rugby World Cup red cards|sent off]] (red carded) in the tournament. Welsh lock [[Huw Richards]] was the first, while playing against New Zealand in 1987. No player has been red carded more than once.<ref name=RWCRecords />

== See also ==
{{Portal|Rugby union}}
* [[Women's Rugby World Cup]]
* [[Rugby World Cup Sevens]]

== References ==

=== Printed sources ===
* {{cite book | last = Collins | first = Tony | editor-last = Ryan | editor-first = Greg | title = The Changing Face of Rugby: The Union Game and Professionalism since 1995 | year = 2008 | publisher = Cambridge Scholars Publishing | chapter = 'The First Principle of Our Game': The rise and fall of amateurism: 1886–1995 | isbn = 1-84718-530-4 }}
* Davies, Gerald (2004). ''The History of the Rugby World Cup'' Sanctuary Publishing Ltd. ISBN 1-86074-602-0.
* [[Nick Farr-Jones|Farr-Jones, Nick]], (2003). ''Story of the Rugby World Cup'', [[Australia Post|Australian Post Corporation]]. ISBN 0-642-36811-2.
* {{cite book | last1=Harding |first1=Grant |last2=Williams |first2=David |title=The Toughest of Them All: New Zealand and South Africa: The Struggle for Rugby Supremacy |year=2000 | publisher=Penguin Books |location=[[Auckland]], New Zealand |isbn=0-14-029577-1}}
* {{cite thesis| last = Martin | first = Gerard John | title = The Game is not the Same – a History of Professional Rugby in New Zealand | date = 2005 | publisher = Auckland University of Technology}}
* {{cite book | last = Peatey | first = Lance | title = In Pursuit of Bill: A Complete History of the Rugby World Cup | publisher = New Holland Publishers | year = 2011 | isbn=978-1-74257-191-1 }}
* {{cite thesis| last = Phillpots | first = Kyle | title = The Professionalisation of Rugby Union | date = 2000 | publisher = University of Warwick}}
* {{cite journal | last = Williams | first= Peter | year = 2002 | title = Battle Lines on Three Fronts: The RFU and the Lost War Against Professionalism |journal=The International Journal of the History of Sport |volume=19 |issue=4 |pages=114–136 | publisher=Routledge |doi=10.1080/714001793}}

=== Notes ===
{{Reflist|group=lower-alpha}}

=== Citations ===
{{Reflist|20em}}

== External links ==
{{Commons category|Rugby World Cup}}
{{Wikinews|Category:Rugby World Cup|Rugby World Cup}}
*{{official website|http://www.rugbyworldcup.com|Rugby World Cup}} – official site
*[http://www.worldrugby.org/ World Rugby]

{{Rugby Union World Cup}}
{{Rugby World Cup winners}}
{{International rugby union}}
{{Main world cups}}
{{Main world championships}}
{{featured article}}

[[Category:Rugby World Cup| ]]
[[Category:Quadrennial sporting events]]
[[Category:Recurring sporting events established in 1987]]
[[Category:World championships]]
[[Category:World Rugby competitions]]