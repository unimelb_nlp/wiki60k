{{Orphan|date=June 2016}}

{{Infobox journal
| title = Current Issues in Education
| discipline = [[Education]]
| abbreviation = Curr. Issues Educ.
| editor = Constantin Schreiber
| publisher = [[Mary Lou Fulton Teachers College]]
| country = United States
| frequency = Triannually
| history = 1998-present
| openaccess = Yes
| website = http://cie.asu.edu
| link1 = http://cie.asu.edu/ojs/index.php/cieatasu/issue/current
| link1-name = Online access
| link2 = http://cie.asu.edu/ojs/index.php/cieatasu/issue/archive
| link2-name = Online archive
| eISSN = 1099-839X
| OCLC = 39224056
| LCCN = sn98004512 
}}
'''''Current Issues in Education''''' is a triannual [[peer-reviewed]] [[open access]] [[academic journal]] sponsored by [[Arizona State University]]'s [[Mary Lou Fulton Teachers College]]. The journal is run by graduate students and covers all aspects of [[education]]. The journal is abstracted and indexed in [[Scopus]].<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2015-06-02}}</ref>

== History ==
The journal was established in 1998 by faculty advisers [[David Berliner]], Jim Middleton, and [[Gene V. Glass]] (Arizona State University) and [[editor-in-chief]] Leslie Poynor.<ref>{{cite web|url=http://cie.asu.edu/volume1/number1/index.html |accessdate=August 27, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20100614131350/http://cie.asu.edu/volume1/number1/index.html |archivedate=June 14, 2010 }}</ref> It ceased operation at the close of 2006 and underwent reorganization in 2007.<ref>{{cite web|url=http://cie.asu.edu/volume10/number4/index.html |accessdate=August 27, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110813151326/http://cie.asu.edu/volume10/number4/index.html |archivedate=August 13, 2011 }}</ref> Faculty advisors Finbarr Sloane ([[University of Colorado Boulder]]) and Sarah Brem (Arizona State University) and then-newly appointed editors Lori Ellingford and Jeffrey Johnson restarted the journal in December 2008.<ref>{{cite web|url=http://cie.asu.edu/volume10/index.html |accessdate=August 27, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110719142030/http://cie.asu.edu/volume10/index.html |archivedate=July 19, 2011 }}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://cie.asu.edu/}}

[[Category:Education journals]]
[[Category:Articles created via the Article Wizard]]
[[Category:Academic journals edited by students]]
[[Category:Arizona State University]]
[[Category:Open access journals]]
[[Category:Publications established in 1998]]
[[Category:English-language journals]]
[[Category:Triannual journals]]