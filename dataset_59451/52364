{{Infobox Journal
| title = Natural Language Semantics
| cover = [[File:Natural Language Semantics.jpg]]
| editor = [[Irene Heim]], [[Angelika Kratzer]]
| discipline = [[Linguistic semantics]]
| abbreviation = Nat. Lang. Semant.
| publisher = [[Springer Science+Business Media]]
| country =
| frequency = Quarterly
| history = 1993–present
| impact= 0.480
| impact-year = 2012
| openaccess =
| website = http://www.springer.com/journal/11050/
| link1 = http://www.springerlink.com/content/0925-854X
| link1-name = Online access
| link2 =
| link2-name =
| JSTOR = 0925854X
| CODEN = NLSEEM
| OCLC = 243539944
| LCCN = 93643719
| ISSN = 0925-854X
| eISSN = 1572-865X
}}
'''''Natural Language Semantics''''' is a quarterly [[Peer review|peer-reviewed]] [[academic journal]] of [[semantics]] published by [[Springer Science+Business Media]]. It covers semantics and its interfaces in [[grammar]], especially in [[syntax]]. The [[editors-in-chief]] are [[Irene Heim]] ([[MIT]]) and [[Angelika Kratzer]] ([[University of Massachusetts Amherst]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Social Science Citation Index]]
* [[Scopus]]
* [[EBSCO Publishing|EBSCO databases]]
* [[Academic OneFile]]
* [[Arts & Humanities Citation Index]]
* [[Linguistic Bibliography]]
* [[Bibliography of Linguistic Literature]]
* [[FRANCIS]]
* [[Linguistics Abstracts]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 0.480.<ref name=WoS>{{cite book |year=2013 |chapter=Natural Language Semantics |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.springer.com/journal/11050/}}

[[Category:Linguistics journals]]
[[Category:Semantics]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1993]]