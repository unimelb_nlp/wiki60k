{{For|the Australian musician|Black Allan Barker}}
{{good article}}
{{Use mdy dates|date=March 2016}}
{{Use American English|date=March 2016}}

{{Infobox named horse
| horsename     = Black Allan
| image         = AllenF1.jpg
| caption       = Black Allan at James Brantley's farm, {{circa|1905}}
| breed         = [[Tennessee Walking Horse]]
| sire          = Allendorf
| grandsire     = Onward
| dam           = Maggie Marshall
| damsire       = Bradfords Telegraph
| foaled        = 1886
| country       = United States of America
| color         = {{flatlist|
* Black
* star
* hind sock
* hind coronet
}}
| breeder       =
| owner         =James Brantley <br /> [[Albert Dement]]
| trainer       =
| racerecord    =
| raceearnings  =
| racewins      =
| raceawards    =
| otherawards   =
| honors        =  Posthumously given registration number F-1 by the [[Tennessee Walking Horse Breeders' and Exhibitors' Association]]
}}

'''Black Allan''' or '''Allan F-1''' (1886{{ndash}}1910) was the [[Foundation bloodstock|foundation sire]] of the [[Tennessee Walking Horse]]. He was out of a [[Morgan horse|Morgan]] mare named Maggie Marshall and by Allendorf, a stallion descended from [[Hambletonian 10|Hambletonian]] lines. Black Allan was registered as No.{{nbsp}}7623 by the American Trotting Registry. Although Black Allan was supposed to be a [[harness racing|trotter]], he preferred to [[horse gait#pace|pace]] and so never raced. Besides the pace, he performed a lateral [[ambling]] gait now known as the [[ambling|running walk]]. He was a [[black (horse)|black]] stallion standing {{hands|15}} high. He was given the designation Allan F-1 when the Tennessee Walking Horse Breeders' Association, precursor to the [[Tennessee Walking Horse Breeders' and Exhibitors' Association]], was formed in 1935. He had multiple owners throughout his life, but his last owners, James Brantley and [[Albert Dement]], were the only ones to recognize Black Allan's use as a breeding stallion. Black Allan sired 40 known foals in his lifetime, among them [[Roan Allen]], registration number F-38, Hunters Allen F-10, and [[Merry Legs]] F-4. Black Allan died September 16, 1910, at the age of 24.

== Life ==

Black Allan{{efn|Although Black Allan's name is sometimes incorrectly spelled "Allen"{{snds}}he was registered as "Black Allan"{{snds}}the misspelling probably arose from a [[scrivener's error]] by the registrar, so in his descendants the name is spelled "Allen".}}<ref name="Patten" /> was [[foal]]ed in [[Lexington, Kentucky]] in 1886, out of the Morgan mare Maggie Marshall and by Allendorf, who descended from Hambletonian lines. Black Allan was a [[black horse|black]] stallion standing {{hands|15}}, with a [[horse markings#leg markings|sock on his left hind foot]], [[horse markings#leg markings|coronet]] on his right hind foot, and a [[horse markings#facial markings|star on his forehead]].<ref name=Walkerswest />  He was registered with the American Trotting Registry and given registration number 7623.<ref name=Hendricks>{{cite book|last1=Hendricks|first1=Bonnie L.|title=International encyclopedia of horse breeds|date=2007|publisher=[[University of Oklahoma Press]] |location=Norman |isbn=978-0-8061-3884-8|page=414 |type=Pbk. |accessdate=}}</ref>
He was sold many times throughout his life, the first time at the side of his dam. He was bought by George Ely, who already owned an 1882 colt out of Maggie Marshall, Elyria, whose record for trotting the mile was 2:25. Ely hoped Black Allan would compare to the older colt, but sold him in 1891 when he discovered that Black Allan was a 'pacer'. It is now known that Black Allan also performed the lateral [[ambling gait]] known as the running walk.<ref>{{cite book | url=https://books.google.com/books?id=EizYYhYeDwQC  | title=The Tennessee Walking Horse | location=Mankato, Minnesota | publisher=[[Capstone Publishers]] | author=Coleman, Lori | date=January 1, 2006 | page=8 | isbn=0-7368-5461-4 | id=ISBN 978-0736854610 | accessdate=March 28, 2016}}</ref> He was lightly raced, but was unable to produce a burst of speed toward the end of his races and generally finished last.  Nonetheless, due to his looks, early speed and long stride, he was put to stud.<ref name="Moody">{{cite book | last1=Moody | first1=Ralph | title=American horses | date=2004 | publisher=[[University of Nebraska Press]] | pages=137–141 | isbn=978-0-8032-3248-8 | edition=Bison books | url=https://books.google.com/books?id=s5ec9FwrRegC | accessdate=March 28, 2016}}</ref>  Allan was bought by John P. Mankin of [[Murfreesboro, Tennessee]] for {{US$|335|link=yes}}, only to be sold again a few years later.  One owner, J.A. McCulloch, used Black Allan as a "teaser" to see if mares were in [[estrus]] before they were bred to jack donkeys to produce [[mule]]s. Another owner traded him for a black filly, a milk cow and $20. When Black Allan was sold to his most famous owner, James Brantley, in 1903, his purchase price was $110.<ref name="westwoodfarms.net" /> He was sold without papers, but Brantley eventually recovered his registration certificate.<ref>{{cite book | first1=Ken | last1=McCarr | location=Lexington, Kentucky | url=https://books.google.com/books?id=hMseBgAAQBAJ&pg=PA12 | title=The Kentucky Harness Horse | isbn=978-0-8131-5969-0 | publisher=[[The University Press of Kentucky]] | date=February 2015 | pages=12–13 | accessdate=March 28, 2016}}</ref> Brantley rode Black Allan himself, and his son French Brantley sometimes rode the horse to school.<ref name="westwoodfarms.net">{{cite web | url=http://www.westwoodfarms.net/reference/allan.html | publisher=Westwood Farms  | title=Reference: Allan F-1 aka "Black Allan" | accessdate=February 28, 2016}}</ref> At the very end of Black Allan's life,  he was sold by James Brantley to Albert Dement of [[Wartrace, Tennessee]], one of the earliest Tennessee Walking Horse breeders. The horse's price was $140, and he was sold with the guarantee that he would live through the breeding season.<ref name="westwoodfarms.net" /> Dement stood Allan at stud for only a few months before Allan's death, during which the stallion was bred to 111 mares.<ref name=TWHBEA>{{cite web | url = http://twhbea.com/features/allanf1 | title=Allan F1 | website=TWHBEA}}{{dead link|date=March 2016}}</ref> He died at Dement's farm on September 16, 1910, due to having been fed green [[sorghum]]. Black Allan was 24 years old.<ref name=Walkerswest>{{cite web|url =  http://www.walkerswest.com/Champs/AllenF1.htm |title=In Memory of Allan F1|website=Walkers West}}</ref>

== Bloodlines and offspring ==

[[File:Hambletonian10.jpg|thumb|right|Hambletonian 10 is an ancestor of Black Allan]]

Black Allan's pedigree traced back to [[Figure (horse)|Justin Morgan]], the foundation sire of the Morgan breed, on his dam's side and to [[Hambletonian 10]], the foundation sire of the [[Standardbred]] on his sire's side.<ref name=pedigree /> Black Allan sired an estimated 40 foals.<ref name=Walkerswest />  Three of his offspring, [[Roan Allen]] F-38, [[Merry Legs]] F-4, and Hunters Allen F-10, were given special registration numbers beginning in the designation F, which mark them as [[foundation bloodstock]]. Most of Black Allan's best offspring, including Roan Allen and Merry Legs, were produced from [[crossbreeding|crosses]] on [[American Saddlebred]] mares, especially those from [[Denmark (horse)|Denmark]] bloodlines.<ref name=Patten>{{cite web|url=http://babel.hathitrust.org/cgi/pt?id=coo.31924000436281;view=1up;seq=137|author=Patten, John W.|title=The light horse breeds: their origin, characteristics, and principal uses | publisher=A. S. Barnes |year=1960 |page=145 |via=[[Hathitrust]] |accessdate=March 24, 2016}}</ref>

Due to Black Allan's influence and potency in passing his gait and conformation to his offspring, he was given registration number F-1 when the Tennessee Walking Horse Breeders' Association, the precursor to the [[Tennessee Walking Horse Breeders' and Exhibitors' Association]], was formed in 1935. Today he is considered the foundation sire of the Tennessee Walking Horse breed, one of the few American breeds that names a single horse with this honor.<ref name=Patten /><ref name=Self>{{cite web | url=http://babel.hathitrust.org/cgi/pt?id=mdp.39015069793175;view=1up;seq=311 | title=The horseman's encyclopedia | last=Self | first=Margaret Cabell | publisher=[[Alfred Smith Barnes|A.S. Barnes and Company]] | year=1946 | accessdate=February 28, 2016}}</ref><ref name="Gazette">{{cite news | url=http://www.t-g.com/story/1885824.html | title=Origins of the Tennessee walking horse | date=August 25, 2012 | newspaper=[[The Times-Gazette]] | location=Hillsboro, Ohio | publisher=[[Ohio Community Media]] | quote=...point to one sire and say, "This is where it all began."  | accessdate=March 17, 2016}}</ref>
{{clear}}

== Pedigree ==

{| class="wikitable" style="clear:both; margin:0 auto; width:90%;"
|+ style="padding-bottom:0.4em;"| Pedigree of Black Allan<ref name=Walkerswest /><ref name=pedigree>{{cite web | title=Black {{not a typo|Allen}} pedigree | url=http://www.allbreedpedigree.com/black+allen | publisher=All breed database | accessdate=March 24, 2016}}</ref>

|-
!rowspan="8"|Sire<br />Allendorf
|rowspan="4"|Onward
|rowspan="2"|George Wilkes
|[[Hambletonian 10]]
|-
|Dolly Spanker
|-
|rowspan="2"|Dolly
|Mambrino Chief
|-
|Fanny
|-
|rowspan="4"|Alma Mater
|rowspan="2"|Mambrino Patchen
|Mambrino Chief
|-
|Rodes Mare
|-
|rowspan="2"|Estella
|Australian
|-
|Fanny G
|-
!rowspan="8"|Dam<br />Maggie Marshall
|rowspan="4"|Bradford's Telegraph
|rowspan="2"|Black Hawk 5
|Sherman Morgan
|-
|Queen of the Neck
|-
|rowspan="2"|Nathan Hardy mare
|Sir Walter
|-
|unknown
|-
|rowspan="4"|Truman Pollock mare
|rowspan="2"|Truman Pollock
|unknown
|-
|unknown
|-
|rowspan="2"|unknown
|unknown
|-
|unknown
|}

== Notes ==

{{Portal|Horses}}

{{Notes}}

== References ==

{{Reflist|30em}}

== Bibliography ==
* The Tennessee Walking Horse, [[Western Horseman]], October 1994
{{authority control}}

[[Category:1886 animal births]]
[[Category:Individual Tennessee Walking Horses]]