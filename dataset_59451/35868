{{Other uses|Æthelric (disambiguation)}}

{{Infobox Christian leader
| name           = Æthelric II
| image          = 
| image_size = 250
| caption = Modern view of Penenden Heath
| see            =[[Diocese of Selsey]]
| title          = [[Bishop of Chichester|Bishop of Selsey]]
| consecration         = 1058
| ended = deposed 1070
| predecessor    = [[Heca]]
| successor      =[[Stigand of Selsey|Stigand]]
| death_date =[[circa]] 1076
}}

'''Æthelric'''{{efn|Called '''Æthelric II''' to distinguish him from an earlier Æthelric who was also bishop of Selsey and also spelled '''Ethelric'''.}} (died [[circa|c.]] 1076) was the second to last medieval [[List of bishops of Chichester and precursor offices|Bishop of Selsey]] in England before the see was moved to [[Chichester]]. [[Consecration|Consecrated]] a bishop in 1058, he was deposed in 1070 for unknown reasons and then imprisoned by King [[William I of England]]. He was considered one of the best legal experts of his time, and was even brought from his prison to attend the trial on [[Penenden Heath]] where he gave testimony about English law before the [[Norman Conquest of England]].

==Early life==

Æthelric was a monk at [[Canterbury Cathedral|Christ Church Priory]] at [[Canterbury]] prior to his becoming a bishop.<ref name=EdConf>Barlow ''Edward the Confessor'' p. 198</ref> Several historians opine that he might have been the same as the Æthelric who was a monk of Canterbury and a relative of [[Godwin, Earl of Wessex]]. That Æthelric was elected by the monks of Canterbury to be [[Archbishop of Canterbury]] in 1050, but was not confirmed by King [[Edward the Confessor]] who insisted on [[Robert of Jumièges]] becoming archbishop instead.<ref name=Godwins56>Barlow ''Godwins'' p. 56</ref><ref name=Mason93>Mason ''House of Godwine'' p. 93</ref> The evidence is not merely that they shared the same name, because the name was a relatively common one in Anglo-Saxon England. Other evidence pointing to the possibility of them being the same person includes the fact that he was felt to have been unfairly deposed in 1070 as well as the bishop's great age in 1076.<ref name=Walker203>Walker ''Harold'' p. 203</ref>

Æthelric was consecrated bishop in 1058 by [[Stigand]], the Archbishop of Canterbury.<ref name=BHOChich>Greenway ''[http://british-history.ac.uk/report.aspx?compid=34293 Fasti Ecclesiae Anglicanae 1066–1300: volume 5: Chichester: Bishops]''</ref> Æthelric was consecrated by Stigand, unlike most of the English bishops of the time period, because at that point, Stigand held a valid [[pallium]], or symbol of an archbishop's authority and ability to consecrate bishops.<ref name=Walker137>Walker ''Harold'' pp. 137-138</ref>

==Deposition==

Æthelric was deposed by the [[Council of Windsor]] on 24 May 1070<ref name=BHOChich/> and imprisoned at [[Marlborough, Wiltshire|Marlborough]], being replaced by [[Stigand of Selsey|Stigand]] (not the same as the archbishop), who later moved the seat of the diocese to Chichester. It is possible, that his deposition was tied to the fact that about that time, King [[Harold Godwinson|Harold of England]]'s mother and sister took refuge with the count of Flanders. If Æthelric was related to the Godwin's, King [[William I of England]] may have feared that the bishop would use his diocese to launch a rebellion.<ref name=Walker193>Walker ''Harold'' p. 193</ref> Other reasons put forward include the fact that Æthelric had been consecrated by Stigand, but the other bishop that Stigand had consecrated, [[Siward (Bishop of Rochester)|Siward]] the [[Bishop of Rochester]] was not deposed.<ref name=Walker203/> Æthelric was a monk, and while not having a great reputation for sanctity, he was not held to be immoral either.<ref name=Stafford105>Stafford ''Unification and Conquest'' p. 105</ref> The pope did not feel that his deposition had been handled correctly,<ref name=Stenton661>Stenton ''Anglo-Saxon England'' p. 661</ref> so his deposition was confirmed at the [[Council of Winchester]] on 1 April 1076.<ref name=BHOChich/> It continued to be considered uncanonical, but Æthelric was never restored to his bishopric.<ref name=Williams46>Williams ''English and the Norman Conquest'' p. 46</ref>

==Penenden Heath==

He was carted from imprisonment to the [[Trial of Penenden Heath]] of [[Odo of Bayeux]], [[earl of Kent]].<ref name=Hindley347/> This took place sometime between 1072 and 1076.<ref name=Forgery10>O'Brien "Forgery and the Literacy" ''Albion'' p. 10</ref>{{efn|For a discussion of the dating issues of the trial as well as other concerns connected to Æthelric's attendance at the trial, see a 2001 article by Alan Cooper in ''[[The English Historical Review]]'', that is listed in the further reading section.}} At that time, he was the most prominent legalist in England.<ref name=Hindley347>Hindley ''Brief History of the Anglo-Saxons'' p. 347</ref><ref name=Stafford107>Stafford ''Unification and Conquest'' p. 107</ref> He helped clarify Anglo-Saxon land laws, as the trial was concerned with the attempts of Lanfranc to recover lands from Odo.<ref name=Lyon182>Lyon ''Constitutional and Legal History'' p. 182</ref> The medieval writer [[Eadmer]] also consulted Æthelric for information on Eadmer's ''Life of St Dunstan''.<ref name=Walker95>Walker ''Harold'' p. 95</ref>

Presumably Æthelric died soon after the trial, as he was already an old man when he attended the trial.<ref name=Bates153>Bates ''William the Conqueror'' p. 153</ref>

==Note==
{{notelist}}

==Citations==
{{reflist|colwidth=40em}}

==References==
{{refbegin|colwidth=60em}}
* {{cite book |author=Barlow, Frank |authorlink=Frank Barlow (historian) |title=Edward the Confessor |publisher=University of California Press |location=Berkeley, CA |year=1970 |isbn=0-520-01671-8 }}
* {{cite book |author=Barlow, Frank |authorlink=Frank Barlow (historian)|title=The Godwins: The Rise and Fall of a Noble Dynasty |publisher=Pearson/Longman |location=London |year=2003 |isbn=0-582-78440-9}}
* {{cite book |author=Bates, David |authorlink=David Bates (historian) |title=William the Conqueror |publisher=Tempus  |location=Stroud, UK |year=2001 |isbn=0-7524-1980-3 }}
* {{cite book |author=Greenway, Diana E. |url=http://british-history.ac.uk/report.aspx?compid=34293 |title= Fasti Ecclesiae Anglicanae 1066–1300: volume 5: Chichester: Bishops|publisher= Institute of Historical Research|year=1996 |accessdate= 20 October 2007}}
* {{cite book |author= Hindley, Geoffrey |title=A Brief History of the Anglo-Saxons: The Beginnings of the English Nation |year= 2006|publisher= Carroll & Graf Publishers |location=New York |isbn=978-0-7867-1738-5  }}
* {{cite book |author=Lyon, Bryce Dale |title=A Constitutional and Legal History of Medieval England|edition=Second |publisher=Norton |location=New York |year=1980  |isbn=0-393-95132-4 }}
* {{cite book |author=Mason, Emma |title=House of Godwine: The History of Dynasty |publisher=Hambledon & London |location=London |year=2004 |isbn=1-85285-389-1 }}
* {{cite journal |author=O'Brien, Bruce |title=Forgery and the Literacy of the Early Common Law |journal=[[Albion (history journal)|Albion]] |volume=27 |issue=1 |date=Spring 1995 |pages=1–18 |doi=10.2307/4052668 |jstor= 4052668}}
* {{cite book |author=Stafford, Pauline|authorlink=Pauline Stafford |title= Unification and Conquest: A Political and Social History of England in the Tenth and Eleventh Centuries |publisher=Edward Arnold |location=London |year=1989  |isbn=0-7131-6532-4 }}
* {{cite book |author=Stenton, F. M. |authorlink= Frank Stenton |title= Anglo-Saxon England  |year= 1971|publisher= Oxford University Press |location=Oxford, UK |edition=Third |isbn=978-0-19-280139-5  }}
* {{cite book |author=Walker, Ian |title=Harold the Last Anglo-Saxon King |publisher=Wrens Park |location=Gloucestershire, UK |year=2000|isbn=0-905778-46-4 }}
* {{cite book |author=Williams, Ann |authorlink= Ann Williams (historian) |title=The English and the Norman Conquest |publisher=Boydell Press |location=Ipswich, UK |year=2000 |isbn=0-85115-708-4 }}
{{refend}}

==Further reading==
{{refbegin}}
* {{cite journal |author=Cooper, Alan |title=Extraordinary Privilege: The Trial of Penenden Heath and the Domesday Inquest|journal=[[The English Historical Review]] |volume=116 |issue=469 |date=November 2001|pages=1167–1192 |doi=10.1093/ehr/116.469.1167 |jstor= 1562290}}
* {{cite journal |author=LePatourel, John | title=The Date of the Trial on Penenden Heath |journal=[[The English Historical Review]] |volume=61 |issue=241 |date=September 1946 |pages=378–388 |doi=10.1093/ehr/LXI.CCXLI.378 |jstor= 556201}}
{{refend}}

==External links==
* {{PASE|18345|Æthelric 64}}

{{s-start}}
{{s-rel|ca}}
{{s-bef | before = [[Heca]] }}
{{s-ttl   | title = [[Bishop of Selsey]]  | years =1058–1070 }}
{{s-aft  | after = [[Stigand of Selsey|Stigand]]}}
{{s-end}}

{{Bishops of Selsey}}

{{Good article}}
{{Authority control}}

{{DEFAULTSORT:Aethelric 2}}
[[Category:1070s deaths]]
[[Category:Bishops of Selsey]]
[[Category:11th-century Roman Catholic bishops]]
[[Category:Year of birth unknown]]