{{Use dmy dates|date=February 2017}}
{{EngvarB|date=February 2017}}
{{featured article}}
{{taxobox
 | name                = Newton's parakeet
 | image               = Newton's Parakeet.jpg
 | image_width         = 250px
 | image_caption       = Life drawing by Paul Jossigny, 1770s
 | status              = EX
 | status_system       = IUCN3.1
 | status_ref          =<ref name=IUCN>{{cite journal | author = [[BirdLife International]] | title = ''Psittacula exsul'' | journal = [[IUCN Red List of Threatened Species]] | volume = 2016 | page = e.T22685465A93074571 | publisher = [[IUCN]] | year = 2016 | url = http://www.iucnredlist.org/details/22685465/0 | doi =  10.2305/IUCN.UK.2016-3.RLTS.T22685465A93074571.en | accessdate = 17 February 2017}}</ref>
 | extinct             = ca. 1875
 | regnum              = [[Animal]]ia
 | phylum              = [[Chordate|Chordata]]
 | classis             = [[bird|Aves]]
 | ordo                = [[Psittaciformes]]
 | superfamilia        = [[true parrot|Psittacoidea]]
 | familia             = [[Psittaculidae]]
 | subfamilia          = [[Psittaculinae]]
 | tribus              = [[Psittaculini]]
 | genus               = ''[[Psittacula]]''
 | species             = '''''P. exsul'''''
 | binomial            = ''Psittacula exsul''
 | binomial_authority  = ([[Alfred Newton|Newton]], 1872)
 | range_map           = LocationRodrigues.PNG
 | range_map_width     = 250px
 | range_map_caption   = Location of [[Rodrigues]]
 | synonyms            =
* ''Palaeornis exsul'' <small>Newton, 1872</small>
}}
'''Newton's parakeet''' (''Psittacula exsul''), also known as the '''Rodrigues parakeet''' or '''Rodrigues ring-necked parakeet''', is an [[extinction|extinct]] [[species]] of [[parrot]] that was [[Endemism in birds|endemic]] to the [[Mascarene]] island of [[Rodrigues]] in the western [[Indian Ocean]]. Several of its features [[genetic divergence|diverged]] from related species, indicating long-term isolation on Rodrigues and subsequent adaptation. The [[rose-ringed parakeet]] of the same [[genus]] is a close relative and probable ancestor. Newton's parakeet may itself have been ancestral to the endemic parakeets of nearby [[Mauritius]] and [[Réunion]].

Around {{convert|40|cm|in}} long, Newton's parakeet was roughly the size of a rose-ringed parakeet. Its [[plumage]] was mostly greyish or [[slate blue]] in colour, which is unusual in ''[[Psittacula]]'', a genus containing mostly green species. The male had [[sexual dimorphism|stronger colours]] than the female and possessed a reddish instead of black [[beak]], but details of a mature male's appearance are uncertain; only one male specimen is known, and it is believed to be immature. Mature males might have possessed red patches on the wing like the related [[Alexandrine parakeet]]. Both sexes had a black collar running from the chin to the [[nape]], but this was clearer in the male. The legs were grey and the [[Bird vision#Anatomy of the eye|iris]] yellow. 17th-century accounts indicate that some members of the species were green, which would suggest that there were both blue and green [[polymorphism (biology)|colour morphs]], but there is no definitive explanation for these reports. Little is known about its behaviour in life, but it may have fed on the nuts of the [[Cassine orientalis|bois d'olive]] tree, along with leaves. It was very tame and was able to [[talking bird|mimic speech]].

Newton's parakeet was first written about by the French [[Huguenot]] [[François Leguat]] in 1708 and was only mentioned a few times by other writers afterwards. The specific name "''exsul''" is a reference to Leguat, who was exiled from France. Only two life drawings exist, both of a single specimen held in captivity in the 1770s. The species was scientifically described in 1872, with a female specimen as the [[holotype]]. A male, the last specimen recorded, was collected in 1875, and these two specimens are the only ones that exist today. The bird became scarce due to deforestation and perhaps hunting, but it is thought that it was finally wiped out by a series of [[cyclone]]s and storms that hit Rodrigues in the late 19th century. There was unfounded speculation about the possible survival of the species as late as 1967.

== Taxonomy ==
[[File:Psittacula exsul.jpg|thumb|left|upright|[[John Gerrard Keulemans]]' 1875 illustration of the female [[holotype]] specimen]]
Newton's parakeet was first recorded by [[François Leguat]] in his 1708 memoir ''A New Voyage to the East Indies''. Leguat was the leader of a group of nine [[French Huguenot]] refugees who colonised [[Rodrigues]] between 1691 and 1693 after they were [[marooning|marooned]] there. Subsequent accounts were by Julien Tafforet, who was marooned on the island in 1726, in his ''Relation de l'Île Rodrigue'', and then by the French mathematician [[Alexandre Pingré]], who travelled to Rodrigues to view the [[1761 transit of Venus]].<ref>{{cite book | last= Leguat | first= F. | editor= Oliver, S. P.  | editor-link= Samuel Pasfield Oliver | year= 1891 | title= The voyage of François Leguat of Bresse, to Rodriguez, Mauritius, Java, and the Cape of Good Hope | volume= Volume 1 | publisher= Hakluyt Society | location= London | url=https://archive.org/stream/voyagefranoisle01missgoog#page/n200/mode/1up | pages=84-85}}</ref><ref name="Mascarene Parrots" /><ref>{{cite journal  | last = Newton  | first =  A.   | title = Additional evidence as to the original fauna of Rodriguez | journal = Proceedings of the Zoological Society of London  | series =   | volume =  | edition = | pages = 39–43  | url = http://www.biodiversitylibrary.org/page/28501177#page/77/mode/1up  | year = 1875}}</ref>

In 1871, George Jenner, the [[magistrate]] of Rodrigues, collected a female specimen; it was preserved in alcohol and given to [[Edward Newton]], a [[British Mauritius|colonial administrator]] in [[Mauritius]], who sent it to his brother, the British ornithologist [[Alfred Newton]]. A. Newton scientifically described the species in 1872 and gave it the [[scientific name]] ''Palaeornis exsul''. "''Exsul''" ("exiled") refers to Leguat, in that he was exiled from France when he gave the first description of the bird. Newton had tried to find a more descriptive name, perhaps based on colouration, but found it difficult. He refrained from publishing a figure of the female in his original description, though the journal ''[[Ibis (journal)|Ibis]]'' had offered him the space. He instead wanted to wait until a male specimen could be procured since he imagined it would be more attractive.<ref name=Newton1872>{{Cite journal | last1 = Newton | first1 = A. | doi = 10.1111/j.1474-919X.1872.tb05858.x | title = On an undescribed bird from the island of Rodriguez | journal = Ibis | volume = 14 | pages = 31–34 | year = 1872}}</ref> The female, which is the [[holotype specimen]] of the species, is housed in the [[Cambridge University Museum]] as specimen UMZC 18/Psi/67/h/1.<ref name="Mascarene Parrots" />

A. Newton requested further specimens, especially males, but in 1875 he finally published a plate of the female, lamenting that no male specimens could be found. Tafforet's 1726 account had been rediscovered the previous year, and A. Newton noted that it confirmed his assumption that the male would turn out be much more colourful than the female. Newton's collector, Henry H. Slater, had seen a live Newton's parakeet the year before, but was not carrying a gun at the time.<ref name="Palaeornis">{{Cite journal | last1 = Newton | first1 = A. | title = Note on ''Palaeornis exsul''| doi = 10.1111/j.1474-919X.1875.tb05978.x | journal = Ibis | volume = 17 | issue = 3 | pages = 342–343 | year = 1875}}</ref> On 14 August 1875, William Vandorous shot a male specimen.<ref name="Psittaci" /> It may have been the same specimen Slater had observed. It was subsequently sent to E. Newton by William J. Caldwell.<ref>{{Cite journal | doi = 10.1080/08912963.2014.886203| title = In the footsteps of the bone collectors: Nineteenth-century cave exploration on Rodrigues Island, Indian Ocean| journal = Historical Biology| volume = 27| issue = 2| pages = 1| year = 2014| last1 = Hume | first1 = J. P.| last2 = Steel | first2 = L.| last3 = André | first3 = A. A.| last4 = Meunier | first4 = A.}}</ref> This is the [[paratype]] of the species, numbered UMZC 18/Psi/67/h/2 and housed in the Cambridge Museum.<ref name="Mascarene Parrots" />

E. Newton noted that he had expected the male would be adorned with a red patch on the wing, but that the absence of this indicated it was immature. He still found it more beautiful than the female.<ref name="Psittaci">{{Cite journal | last1 = Newton | first1 = E. | title = XXVII.-On the psittaci of the Mascarene Islands | doi = 10.1111/j.1474-919X.1876.tb06925.x | journal = Ibis | volume = 18 | issue = 3 | pages = 281–289 | year = 1876| url = http://biodiversitylibrary.org/item/35122#page/313/mode/1up }}</ref> These two specimens are the only preserved individuals of the species.<ref name="Rothschild">{{Cite book
  | last = Rothschild
  | first = W.
  | authorlink = Walter Rothschild, 2nd Baron Rothschild
  | title = Extinct Birds
  | publisher = Hutchinson & Co
  | year = 1907
  | location = London
  | page = 65
  | url = https://archive.org/stream/extinctbirdsatte00roth#page/64/mode/2up
}}</ref> The [[mandible]] and [[sternum]] were extracted from the female specimen, and [[subfossil]] remains have since been found in the Plaine Corail caverns on Rodrigues.<ref name="Mascarene Parrots" /> The genus ''Palaeornis'' was later declared a [[junior synonym]] of ''[[Psittacula]]'', and all species within the former were transferred to the latter.<ref>{{Cite journal | last1 = Mayr | first1 = G. | title = Parrot interrelationships – morphology and the new molecular phylogenies | doi = 10.1071/MU10035 | journal = Emu | volume = 110 | issue = 4 | pages = 348 | year = 2010 }}</ref> The genus name ''Psittacula'' is derived from the Latin words ''Psittacus'', which means parrot, and ''-ula'', which is a [[diminutive suffix]].<ref name="Mascarene Parrots" />

=== Evolution ===
[[File:P. exsul.jpg|thumb|[[Sternum]] and [[mandible]] extracted from the female specimen, 1875]]
Based on [[morphology (biology)|morphological]] features, the [[Alexandrine parakeet]] (''Psittacula eupatria'') has been proposed as the [[founder population]] for all ''Psittacula'' species on Indian Ocean islands, with new populations settling during the species' southwards colonisation from its native South Asia. Features of that species gradually disappear in species further away from its range. Subfossil remains of Newton's parakeet show that it differed from other Mascarene ''Psittacula'' species in some [[osteology|osteological]] features, but also had similarities, such as a reduced sternum, which suggests a close relationship. Skeletal features indicate an especially close relationship with the Alexandrine parakeet and the [[rose-ringed parakeet]] (''Psittacula krameri''), but the many [[synapomorphy|derived features]] of Newton's parakeet indicates it had long been isolated on Rodrigues.<ref name="Mascarene Parrots" /><ref name="Extinct Birds">{{cite book
| last1 = Hume
| first1 = J. P.
| first2 = M.
| last2 = Walters
|pages= 175–176
|year= 2012
|location= London
|title= Extinct Birds
|publisher= A & C Black
|isbn=1-4081-5725-X}}</ref>

Many endemic Mascarene birds, including the [[dodo]], are descended from South Asian ancestors, and the English palaeontologist [[Julian Hume]] has proposed that this may also be the case for all parrots there. [[Past sea level|Sea levels]] were lower during the [[Pleistocene]], so it was possible for species to colonise some of these less isolated islands.<ref name="Lost Land" /> Although most extinct parrot species of the Mascarenes are poorly known, subfossil remains show that they shared common features such as enlarged heads and jaws, reduced [[Pectoralis major muscle|pectoral]] bones, and robust leg bones. Hume has suggested that they all have a common origin in the [[evolutionary radiation|radiation]] of the [[tribe (biology)|tribe]] [[Psittaculini]], members of which are known as Psittaculines, basing this theory on morphological features and the fact that ''Psittacula'' parrots have managed to colonise many isolated islands in the Indian Ocean.<ref name="Mascarene Parrots">{{Cite journal|last=Hume|first=J. P. | pages = 4–29|year=2007 |url=http://julianhume.co.uk/wp-content/uploads/2010/07/Hume-Mascarene-Parrots.pdf |title=Reappraisal of the parrots (Aves: Psittacidae) from the Mascarene Islands, with comments on their ecology, morphology, and affinities |journal=[[Zootaxa]] |volume=1513}}</ref> The Psittaculini could have invaded the area several times, as many of the species were so specialised that they may have evolved significantly on [[hotspot island]]s before the Mascarenes emerged from the sea. Other members of the genus ''Psittacula'' from the Mascarenes include the extant [[echo parakeet]] (''Psittacula eques echo'') of Mauritius, as well as the extinct [[Réunion parakeet]] (''Psittacula eques eques''), and [[Mascarene grey parakeet]] (''Psittacula bensoni'') of both Mauritius and [[Réunion]].<ref name="Lost Land">{{cite book
| last1 = Cheke
| first1 = A. S.
| first2 = J. P.
| last2 = Hume
| year = 2008
| title = Lost Land of the Dodo: an Ecological History of Mauritius, Réunion & Rodrigues
| pages = 46–56
| publisher = T. & A. D. Poyser
| location = New Haven and London
| isbn = 978-0-7136-6544-4
}}</ref>
[[File:Vadaspark Madarak 01.jpg|thumb|Statues in Hungary of Newton's parakeet and the also extinct [[broad-billed parrot]] of Mauritius]]
A 2011 [[genetic study]] of parrot [[phylogeny]] was unable to include Newton's parakeet, as no viable [[DNA]] could be extracted. The same paper found that the [[Mascarene parrot]] (''Mascarinus mascarin'') of Réunion was most closely related to the [[lesser vasa parrot]] from [[Madagascar]] and nearby islands, and therefore unrelated to the ''Psittacula'' parrots, disputing the theory of their common origin.<ref>{{Cite journal | last1 = Kundu | first1 = S. | last2 = Jones | first2 = C. G. | last3 = Prys-Jones | first3 = R. P. | last4 = Groombridge | first4 = J. J. | title = The evolution of the Indian Ocean parrots (Psittaciformes): Extinction, adaptive radiation and eustacy | doi = 10.1016/j.ympev.2011.09.025 | journal = Molecular Phylogenetics and Evolution | volume = 62 | issue = 1 | pages = 296–305 | year = 2011| pmid = 22019932}}</ref> A 2015 genetic study by Jackson et al. included viable DNA from the toe-pad of the female Newton's parakeet specimen. It was found to group within a [[clade]] of rose-ringed parakeet [[subspecies]] (from Asia and Africa), which it had diverged from 3.82&nbsp;million years ago. Furthermore, Newton's parakeet appeared to be ancestral to the parakeets of Mauritius and Réunion. The [[cladogram]] accompanying the study is shown below:<ref name="Indian Ocean parrots">{{Cite journal | last1 = Jackson | first1 = H. | last2 = Jones | first2 = C. G. | last3 = Agapow | first3 = P. M. | last4 = Tatayah | first4 = V. | last5 = Groombridge | first5 = J. J. | year = 2015 | title = Micro-evolutionary diversification among Indian Ocean parrots: temporal and spatial changes in phylogenetic diversity as a consequence of extinction and invasion | journal = Ibis | volume = 157 | issue = 3 | pages = 496–510 | doi = 10.1111/ibi.12275 }}</ref>
{{clade| style=font-size:100%; line-height:100%
 |1={{clade
  |1={{clade
   |1={{clade
    |1=''Psittacula krameri parvirostris'' (Abyssinian rose-ringed parakeet)
    |2={{clade
     |1=''Psittacula krameri manillensis'' (Indian rose-ringed parakeet)
     |2=''Psittacula krameri borealis'' (Boreal rose-ringed parakeet)}} }}
   |2={{clade
    |1={{clade
     |1=''[[Psittacula eques echo]]'' (echo parakeet)
     |2=''[[Psittacula eques eques]]'' (Réunion parakeet)}}
    |2='''''Psittacula exsul''''' ('''Newton's parakeet''')}} }}
  |2=''[[Psittacula krameri krameri]]'' (African rose-ringed parakeet)}} }}
== Description ==
[[File:Psittacula exsul (extinct) by J.G. Keulemans.jpg|thumb|upright|left|Keulemans' plate from [[Walter Rothschild]]'s 1907 book ''[[Extinct Birds (Rothschild book)|Extinct Birds]]'', based on his 1875 illustration of the female specimen]]
Newton's parakeet was about {{convert|40|cm|in|abbr=on}} long – roughly the size of the rose-ringed parakeet.<ref name="Lost Land" /> The wing of the male specimen was {{convert|198|mm|in|abbr=on}}, the tail {{convert|206|mm|in|abbr=on}}, the [[culmen (bird)|culmen]] {{convert|25|mm|in|abbr=on}}, and the [[tarsus (skeleton)|tarsus]] was {{convert|22|mm|in|abbr=on}}. The wing of the female specimen was {{convert|191|mm|in|abbr=on}}, the tail {{convert|210|mm|in|abbr=on}}, the culmen {{convert|24|mm|in|abbr=on}}, and the tarsus was {{convert|22|mm|in|abbr=on}}. The male specimen was greyish blue (also described as "[[slatey]] blue") tinged with green, and darker above. The head was bluer, with a dark line running from the eye to the [[cere]]. It had a broad black collar running from the chin to the [[nape]], where it became gradually narrower. The underside of the tail was greyish, the upper beak was dark reddish brown, and the mandible was black. The legs were grey and the iris yellow. The female was similar but had a greyer head and a black beak. The black collar was not so prominent as that of the male and did not extend to the back of the neck.<ref name = Fuller /> The general appearance of Newton's parakeet was similar to the extant ''Psittacula'' species, including the black collar, but the bluish grey colouration set it apart from other members of its genus, which are mostly green.<ref name = Fuller>{{Cite book|last = Fuller |first = E. |title = Extinct Birds |pages = 225–227 | year = 2000| publisher = Oxford University Press | location = Oxford |isbn = 0-670-81787-2}}</ref>
[[File:Psittacula exsul Jossigny.jpg|thumb|Jossigny's other 1770s life drawing]]
The French naturalist [[Philibert Commerson]] received a live specimen on Mauritius in the 1770s and described it as "greyish blue". Paul Jossigny made two illustrations of this specimen, the only known depictions of Newton's parakeet in life, unpublished until 2007.<ref name="Mascarene Parrots" /> Though both existing specimens are blue, some early accounts from Rodrigues have caused confusion over the colouration of the plumage.<ref name = Fuller /> One of these is Leguat's following statement:
{{Quotation|There are abundance of green and blew Parrets, they are of a midling and equal bigness; when they are young, their Flesh is as good as young Pigeons.<ref name="Mascarene Parrots" />}}
If the green parrots Leguat referred to were not the [[Rodrigues parrot]] (''Necropsittacus rodericanus''), they might perhaps have been a green [[colour morph]] of Newton's parakeet, as Julian Hume has suggested. As A. Newton observed in his original description, some feathers of the female specimen display both blue and green tinges, depending on the light. This may explain some of the discrepancies.<ref name="Mascarene Parrots" /> The green parrots mentioned could also instead have been storm-blown members of ''Psittacula'' species from other islands, that survived on Rodrigues for a short time.<ref name = Fuller />

The two existing specimens were originally preserved in alcohol, but though this can discolour specimens, it is not probable that it could turn green to blue.<ref name="Mascarene Parrots" /> Hume and Hein van Grouw have also suggested that due to an inheritable [[mutation]], some Newton's parakeets may have lacked [[psittacin]], a [[pigment]] that together with [[eumalanin]] produces green colouration in parrot feathers. Complete lack of psittacine produces blue colouration, whereas reduced psittacine can produce a colour between green and blue called parblue, which corresponds to the colour of the two preserved Newton's parakeet specimens.<ref name="Aberrations">{{Cite journal|last=Hume |first=J. P. |last2=van Grouw |first2= H. | pages = 168–193 |year= 2014 |title= Colour aberrations in extinct and endangered birds |journal= Bulletin of the British Ornithologists' Club |volume= 134 }}</ref>
[[File:Alexandrine Parakeet (Psittacula eupatria), Jurong Bird Park, Singapore - 20090613.jpg|thumb|upright|The related [[Alexandrine parakeet]] has red shoulder patches, as seen in this male.]]
Tafforet also described what appears to be green Newton's parakeets, but the issue of colouration was further complicated:
{{Quotation|The parrots are of three kinds, and in quantity&nbsp;... The second species [mature male Newton's parakeet?] is slightly smaller and more beautiful, because they have their plumage green like the preceding [Rodrigues Parrot], a little more blue, and above the wings a little red as well as their beak. The third species [Newton's parakeet] is small and altogether green, and the beak black.<ref name="Mascarene Parrots" />}}
In 1987, the British ecologist Anthony S. Cheke proposed that the last two types mentioned were male and female Newton's parakeets, and that the differences between them were due to [[sexual dimorphism]]. The last bird mentioned had earlier been identified as introduced [[grey-headed lovebirds]] (''Agapornis canus'') by A. Newton, but Cheke did not find this likely, as their beaks are grey.<ref>{{Cite book| last1 = Cheke | first1 = A. S. | editor1-last = Diamond| editor1-first = A. W.| doi = 10.1017/CBO9780511735769.003 | chapter = An ecological history of the Mascarene Islands, with particular reference to extinctions and introductions of land vertebrates | title = Studies of Mascarene Island Birds | pages = 5–89 | year = 1987 | isbn = 978-0-521-11331-1| location = Cambridge | publisher = Cambridge University Press }}</ref> Alexandre Pingré also mentioned green birds, perhaps with some red colours, but his account is partially unintelligible and therefore ambiguous. A red shoulder patch is also present on the related Alexandrine parakeet.<ref name="Mascarene Parrots" /> None of the existing Newton's parakeet specimens have red patches. The single known male specimen may have been immature, judged on the colour of its beak, and this may also explain the absence of the red patch.<ref name = Fuller /> When ''Psittacula'' are bred by [[aviculture|aviculturalists]], blue is easily produced from green; the production of blue may suppress red colouration, so blue morphs may have lacked the red patch.<ref name="Mascarene Parrots" />

== Behaviour and ecology ==
[[File:Rodrigues.jpg|thumb|left|[[Leguat]]'s 1708 map of pristine Rodrigues; his settlement can be seen to the northeast.|alt=Map of Rodrigues, decorated with Solitaires]]
Almost nothing is known about the behaviour of Newton's parakeet, but it is probable that it was similar to that of other members of its genus. Leguat mentioned that the parrots of the island ate the nuts of the bois d'olive tree (''[[Cassine orientalis]]''). Tafforet also stated that the parrots ate the seeds of the bois de buis [[shrub]] (''[[Fernelia buxifolia]]''), which is endangered today, but was common all over Rodrigues and nearby islets during his visit.<ref name="Mascarene Parrots" /> Newton's parakeet may have fed on leaves as the related echo parakeet does. The fact that it survived long after Rodrigues had been heavily deforested shows that its ecology was less vulnerable than that of, for example, the Rodrigues parrot.<ref name="Lost Land" />

Leguat and his men were hesitant to hunt the parrots of Rodrigues because they were so tame and easy to catch.<ref name="Extinct Birds" /> Leguat's group took a parrot as a pet and were able to [[talking bird|teach it to speak]]:
{{Quotation|Hunting and Fishing were so easie to us, that it took away from the Pleasure. We often delighted ourselves in teaching the Parrots to speak, there being vast numbers of them. We carried one to Maurice Isle [Mauritius], which talk'd French and Flemish.<ref name="Lost Land" />}}
The authors of the 2015 study which resolved the phylogenetic placement of the Mascarene island parakeets suggested that the echo parakeet of Mauritius would be a suitable [[ecological replacement]] for the Réunion parakeet and Newton's parakeet, due to their close evolutionary relationship. The echo parakeet was itself close to extinction in the 1980s, numbering only twenty individuals, but has since recovered, so introducing it to the nearby islands could also help secure the survival of this species.<ref name="Indian Ocean parrots" />

Many other species endemic to Rodrigues became extinct after humans arrived, and the island's ecosystem remains heavily damaged. Forests had covered the entire island before humans arrived, but very little forestation can be seen today. Newton's parakeet lived alongside other recently extinct birds such as the [[Rodrigues solitaire]], the Rodrigues parrot, the [[Rodrigues rail]], the [[Rodrigues starling]], the [[Rodrigues owl]], the [[Rodrigues night heron]], and the [[Rodrigues pigeon]]. Extinct reptiles include the [[domed Rodrigues giant tortoise]], the [[saddle-backed Rodrigues giant tortoise]], and the [[Rodrigues day gecko]].<ref name="Lost Land" />

== Extinction ==
[[File:Leguat1891frontispieceFr1708.jpg|upright|thumb|[[Book frontispiece|Frontispiece]] to Leguat's 1708 memoir, showing his settlement on Rodrigues|alt=Drawing of houses on Rodrigues]]
Of the roughly eight parrot species endemic to the Mascarenes, only the echo parakeet has survived. The others were likely all made extinct by a combination of excessive hunting and [[deforestation]] by humans.<ref name="Mascarene Parrots" /> Leguat stated that Newton's parakeet was abundant during his stay. It was still common when Tafforet visited in 1726, but when Alexandre Pingré mentioned it in 1761, he noted that the bird had become scarce. It was still present on southern islets off Rodrigues (Isle Gombrani), along with the Rodrigues parrot. After this point, much of Rodrigues was severely deforested and used for [[livestock]].<ref name="Mascarene Parrots" />

According to early accounts praising its flavour, it appears visitors commonly ate Newton's parakeet.<ref name="Lost Land" /> Several individuals would likely be needed to provide a single meal, owing to the bird's small size.<ref name = Fuller /> Pingré stated:
{{Quotation|The perruche [Newton's parakeet] seemed to me much more delicate [than the flying-fox]. I would not have missed any game from France if this one had been commoner in Rodrigues; but it begins to become rare. There are even fewer perroquets [Rodrigues parrots], although there were once a big enough quantity according to François Leguat; indeed a little islet south of Rodrigues still retains the name Isle of Parrots [Isle Pierrot].<ref name="Mascarene Parrots" />}}
According to government surveyor Thomas Corby, Newton's parakeet may still have been fairly common in 1843. Slater reported that he saw a single specimen in southwestern Rodrigues during his three-month stay to observe the [[1874 Transit of Venus]], and assistant colonial secretary William J. Caldwell saw several specimens in 1875 during his own three-month visit. The male that he received in 1875 and gave to Newton is the last recorded member of the species. A series of [[cyclone]]s struck the following year and may have devastated the remaining population.<ref name="Mascarene Parrots" /> Further severe storms hit in 1878 and 1886, and since few forested areas were left by this time, there was little cover to protect any remaining birds. The male could, therefore, have been the last of the species alive.<ref name="Lost Land" />

There were unfounded rumours of its continued existence until the beginning of the 20th century.<ref name="Lost Land" /> In 1967, the American ornithologist [[James Greenway]] stated that an extremely small population might still survive on small offshore islets, since this is often the last refuge of endangered birds.<ref name =Greenway>{{cite book
 | last = Greenway
 | first = J. C.
 | title = Extinct and Vanishing Birds of the World
 | publisher = American Committee for International Wild Life Protection 13
 | location = New York
 | year = 1967
 | pages = 107–108
 | isbn = 0-486-21869-4
}}</ref> Hume countered that these islets were probably too small to sustain a population.<ref name="Mascarene Parrots" />

== References ==
{{Reflist|30em}}

== External links ==
*{{Commons category-inline|Psittacula exsul}}
*{{Wikispecies-inline|Psittacula exsul}}

{{Psittaculini}}
{{Birds}}
{{portalbar|Birds|Animals|Biology|Mauritius|Madagascar|Extinction|Paleontology}}
{{taxonbar}}

[[Category:Psittacula]]
[[Category:Extinct birds of Indian Ocean islands]]
[[Category:Bird extinctions since 1500]]
[[Category:Birds described in 1872]]
[[Category:Fauna of Rodrigues]]