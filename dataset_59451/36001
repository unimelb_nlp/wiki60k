{{Infobox road
|state=AL
|type=AL
|route=73
|length_mi=11.219
|length_ref=<ref name=aldot>{{cite map|publisher=[[Alabama Department of Transportation]] |title=Milepost Map Jackson County, Alabama |url=http://aldotgis.dot.state.al.us/MilepostPDF/co36mp.pdf |edition=1999 |section= |accessdate=March 27, 2011 |format=PDF }}{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>
|map=Alabama 73 map.png
|established=
|direction_a=South
|direction_b=North
|terminus_a={{jct|state=AL|AL|71}} at [[Higdon, Alabama|Higdon]]
|junction=
|terminus_b={{jct|state=TN|Sec|377}} near [[South Pittsburg, Tennessee|South Pittsburg, TN]]
|counties=[[Jackson County, Alabama|Jackson]]
|previous_type=US
|previous_route=72
|next_type=AL
|next_route=74
}}
'''State Route&nbsp;73''' ('''SR&nbsp;73'''), is a {{convert|11.219|mi|km|adj=mid|-long}} [[State highway (US)|state highway]] in the U.S. state of [[Alabama]]. The southern terminus of the highway is at an [[Intersection (road)|intersection]] with [[Alabama State Route 71|SR&nbsp;71]] in [[Higdon, Alabama|Higdon]] north to the [[Tennessee]] state line, where it becomes [[Tennessee State Route&nbsp;377]] (SR&nbsp;377). SR&nbsp;73 travels through [[rural area]]s in eastern [[Jackson County, Alabama|Jackson County]], serving the community of [[Bryant, Alabama|Bryant]].

The SR&nbsp;73 designation was first used for present-day CR&nbsp;29 between [[Piedmont, Alabama|Piedmont]] in [[Calhoun County, Alabama|Calhoun County]] and [[Forney, Alabama|Forney]] in [[Cherokee County, Alabama|Cherokee County]], existing in the 1930s and 1940s. In the 1950s, [[Alabama State Route 207|SR&nbsp;207]] was briefly assigned to the road between Higdon and Bryant before it received the SR&nbsp;73 designation. SR&nbsp;73 was extended in the 1980s north to the Tennessee state line to connect to SR&nbsp;377.

==Route description==
[[File:AL SR 73 northbound at CR 318.jpg|thumb|left|SR&nbsp;73 northbound at CR&nbsp;318]]
SR&nbsp;73 begins at an intersection with [[Alabama State Route 71|SR&nbsp;71]] in the community of [[Higdon, Alabama|Higdon]], heading north on a two-lane undivided road. The highway heads through rural areas of farms and woods with some homes, curving to the northwest. The highway heads north again as it crosses under several power lines radiating from the [[Widows Creek Power Plant]] along the [[Tennessee River]]. SR&nbsp;73 continues northeast past homes and businesses in the [[Bryant, Alabama|Bryant]] area on top of [[Sand Mountain (Alabama)|Sand Mountain]] prior to going north through more forested areas with a few rural homes. The highway travels through a mix of farm and woodland before it turns east into forests. SR&nbsp;73 makes a sharp curve north again to traverse Sand Mountain as it comes to the [[Tennessee]] state line. At this point, the road continues into Tennessee as [[Tennessee State Route&nbsp;377]] (SR&nbsp;377), which ascends Sand Mountain to end at [[Tennessee State Route 156|SR&nbsp;156]], east of [[South Pittsburg, Tennessee]].<ref name=aldot/><ref name=aldot2>{{cite map|publisher=Alabama Department of Transportation |title=General Highway Map Jackson County, Alabama |url=http://aldotgis.dot.state.al.us/GeneralHighwayPDF/co36.pdf |edition=2006 |section= |accessdate=March 27, 2011 |format=PDF }}{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref><ref name="google">{{google maps |url=https://maps.google.com/maps?f=d&source=s_d&saddr=Alabama+71+%26+Alabama+73,+Long+Island,+Jackson,+Alabama+35979&daddr=alabama+73+and+tennessee+377&hl=en&geocode=FTLmEwIdt6Tl-im5YaGPurRhiDELBOcS-HTThQ%3BFZbSFQId9azl-ilrF5UKN61hiDEsN3a3XJm0gg&mra=ls&sll=34.926475,-85.630531&sspn=0.126387,0.338173&ie=UTF8&ll=34.92169,-85.623322&spn=0.126394,0.338173&t=h&z=12|title=overview of Alabama State Route 73|accessdate=March 27, 2011}}</ref>

==History==
The SR&nbsp;73 designation was first assigned in 1934 to an unimproved road connecting [[Alabama State Route 74|SR&nbsp;74]] in [[Piedmont, Alabama|Piedmont]] and [[Alabama State Route 62|SR&nbsp;62]] in [[Forney, Alabama|Forney]], traveling through [[Spring Garden, Alabama|Spring Garden]] and [[Rock Run, Alabama|Rock Run]].<ref name=al1934>{{cite map|publisher=[[General Drafting]] |title=State Road Map of Alabama |url=http://cartweb.geography.ua.edu:9001/StyleServer/calcrgn?cat=North%20America%20and%20United%20States&item=States/Alabama/State%20Roads/Alabama1934a.sid&wid=500&hei=400&props=item%28Name,Description%29,cat%28Name,Description%29&style=simple/view-dhtml.xsl |edition=1934 |section= |accessdate=March 27, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110407104935/http://cartweb.geography.ua.edu:9001/StyleServer/calcrgn?cat=North%20America%20and%20United%20States&item=States/Alabama/State%20Roads/Alabama1934a.sid&wid=500&hei=400&props=item(Name,Description),cat(Name,Description)&style=simple/view-dhtml.xsl |archivedate=April 7, 2011 |df= }}</ref> By 1948, this road was removed from the state highway system, becoming CR&nbsp;29 in [[Calhoun County, Alabama|Calhoun]] and [[Cherokee County, Alabama|Cherokee]] counties by 1955.<ref name=al1948>{{cite map|publisher=[[Alabama State Highway Department]] |title=Alabama Highways |url=http://cartweb.geography.ua.edu:9001/StyleServer/calcrgn?cat=North%20America%20and%20United%20States&item=States/Alabama/State%20Roads/Alabama1948a.sid&wid=500&hei=400&props=item%28Name,Description%29,cat%28Name,Description%29&style=simple/view-dhtml.xsl |edition=1948 |section= |accessdate=March 27, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110407105714/http://cartweb.geography.ua.edu:9001/StyleServer/calcrgn?cat=North%20America%20and%20United%20States&item=States/Alabama/State%20Roads/Alabama1948a.sid&wid=500&hei=400&props=item(Name,Description),cat(Name,Description)&style=simple/view-dhtml.xsl |archivedate=April 7, 2011 |df= }}</ref><ref name=al1955/> What would become the current SR-73 south of Bryant became a part of [[Alabama State Route 207|SR-207]] by 1955; at this time, the portion of road was a paved highway.<ref name=al1955>{{cite map|publisher=Alabama State Highway Department |title=Alabama Highways |url=http://cartweb.geography.ua.edu:9001/StyleServer/calcrgn?cat=North%20America%20and%20United%20States&item=States/Alabama/State%20Roads/Alabama1955a.sid&wid=500&hei=400&props=item%28Name,Description%29,cat%28Name,Description%29&style=simple/view-dhtml.xsl |edition=1955 |section= |accessdate=March 27, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20090918194723/http://cartweb.geography.ua.edu:9001/StyleServer/calcrgn?cat=North%20America%20and%20United%20States&item=States/Alabama/State%20Roads/Alabama1955a.sid&wid=500&hei=400&props=item(Name,Description),cat(Name,Description)&style=simple/view-dhtml.xsl |archivedate=September 18, 2009 |df= }}</ref> SR&nbsp;207 was redesignated SR&nbsp;73 by 1957.<ref name=al1957>{{cite map|publisher=Alabama State Highway Department |title=Official 1957 Alabama Highway Map |url=http://cartweb.geography.ua.edu:9001/StyleServer/calcrgn?cat=North%20America%20and%20United%20States&item=States/Alabama/State%20Roads/Alabama1957a.sid&wid=500&hei=400&props=item%28Name,Description%29,cat%28Name,Description%29&style=simple/view-dhtml.xsl |edition=1957 |section= |accessdate=March 27, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110720062351/http://cartweb.geography.ua.edu:9001/StyleServer/calcrgn?cat=North%20America%20and%20United%20States&item=States/Alabama/State%20Roads/Alabama1957a.sid&wid=500&hei=400&props=item(Name,Description),cat(Name,Description)&style=simple/view-dhtml.xsl |archivedate=July 20, 2011 |df= }}</ref> By 1987, SR&nbsp;73 was extended north from Bryant to the Tennessee state line.<ref name=al1987>{{cite map|publisher=Alabama State Highway Department |title=Official Alabama Highway Map, 1987-88 |url=http://cartweb.geography.ua.edu:9001/StyleServer/calcrgn?cat=North%20America%20and%20United%20States&item=States/Alabama/State%20Roads/Alabama1987a.sid&wid=500&hei=400&props=item%28Name,Description%29,cat%28Name,Description%29&style=simple/view-dhtml.xsl |edition=1987 |section= |accessdate=March 27, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20090918103949/http://cartweb.geography.ua.edu:9001/StyleServer/calcrgn?cat=North%20America%20and%20United%20States&item=States/Alabama/State%20Roads/Alabama1987a.sid&wid=500&hei=400&props=item(Name,Description),cat(Name,Description)&style=simple/view-dhtml.xsl |archivedate=September 18, 2009 |df= }}</ref>

==Major intersections==
{{Jcttop|state=AL|county=Jackson|length_ref=<ref name=aldot/>}}
{{ALint
|mile=0.000
|road={{jct|state=AL|AL|71}}
|location=Higdon
|notes=Southern terminus
}}
{{ALint
|mile=11.219
|road={{jct|state=TN|Sec|377|dir1=north}}
|location=Bryant
|notes=Tennessee state line
}}
{{jctbtm}}

==See also==
* {{Portal-inline|U.S. Roads}}
* {{Portal-inline|Alabama}}

==References==
{{reflist}}

==External links==
{{commons category-inline}}
{{Attached KML|display=inline,title}}

{{good article}}

[[Category:State highways in Alabama|073]]
[[Category:Transportation in Jackson County, Alabama]]