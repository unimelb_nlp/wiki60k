'''Digital hoarding''' (also known as '''e-hoarding''') is excessive acquisition and reluctance to delete electronic material no longer valuable to the user. The behavior includes the mass storage of digital artifacts and the retainment of unnecessary or irrelevant electronic data. The term is increasingly common in pop culture, used to describe the habitual characteristics of [[compulsive hoarding]], but in [[cyberspace]]. As with physical space in which excess items are described as "clutter" or "junk," excess digital media is often referred to as "digital clutter."<ref name=lifehacker /><ref name=minimalist>{{cite web|last=Becker|first=Joshua|title=25 Areas of Digital Clutter to Minimalize|url=http://www.becomingminimalist.com/25-areas-of-digital-clutter-to-minimalize/|work=Becoming Minimalist}}</ref><ref name=trendmicro>{{cite web|title=Putting an End to Digital Clutter|url=http://www.trendmicro.co.uk/media/resource_lib/general/putting-an-end-to-digital-clutter-en.pdf|work=Trend Micro}}</ref>

== Related Concepts ==

[[Digital clutter]] is the term often used to describe the resulting (digital) artefacts of digital hoarding, but it should not be understood as exclusively the result of hoarding. Digital clutter can be created as a side-effect of high occurrences of another user activity, such as the computer desktop icons created through frequent installation of applications. In such a case the clutter does not reflect the user's intent to hoard.

[[Housekeeping]] is the term often used to refer to the activity by which digital clutter moves out of the 'clutter' designation, either by being thrown away, or by the recognition of its importance, thus no longer making it part of the 'clutter'.


== Virtual spaces ==

Digital hoarding occurs in any electronic spaces where information is stored. These are common areas where digital clutter may exist:
* [[Web browser|Browser]] [[Tab (GUI)|tabs]]
* Excessive [[computer icon|Desktop icons]]
* [[digital image|Digital photographs]]
* Old [[electronic document|documents]]
* [[Directory (computing)|Electronic file folders]]
* [[Email|Email inboxes]]
* [[Bookmark (World Wide Web)|Internet bookmarks]] no longer being referenced
* Music and movie files
* Old [[software]]/[[computer programs]]/[[application software|apps]] no longer being used

A cluttered email inbox arises when a user does not have a system for archiving some messages and deleting others that are no longer wanted. Electronic documents can become clutter if a user does not delete extraneous files, or if the files are poorly organized (e.g. inconsistent folder structure, empty folders). 

Some [[social media]] platforms also provide opportunity for digital hoarding. On the [[social networking site]] [[Facebook]], for example, one can accumulate a vast number of “[[facebook features#Friend|friends]]” that may merely be acquaintances or lapsed contacts.<ref name=eduniverse>{{cite web|last=Cabellon|first=Ed|title=Stop Digital Hoarding|url=http://www.eduniverse.org/stop-digital-hoarding|work=EDUniverse}}</ref> [[facebook features#Networks_and_groups|Groups]] and Pages can also contribute to clutter when users join and like new ones, respectively, without leaving or unfollowing those in which they are no longer interested.<ref name=eduniverse />

== Causes ==

Digital hoarding stems from a variety of individual traits and habits, corporate conditions, and societal trends: 
*Some individuals experience anxiety when faced with disposing of digital items,<ref name=drowning /> particularly if they fear losing something important.<ref name=combating>{{cite web|last=Davidson|first=Jim|title=Combating 5 Signs of Digital Hoarding Behavior|url=http://www.clickz.com/clickz/column/2259992/combating-5-signs-of-digital-hoarding-behavior|work=ClickZ}}</ref>
*Many digital hoarders don't know how to organize their [[digital content]] or aren't in the habit of doing so, and they lack a methodology for determining which content is worth keeping.<ref name=drowning>{{cite web|last=Beck|first=Melinda|title=Drowning in Email, Photos, Files? Hoarding Goes Digital|url=https://www.wsj.com/news/articles/SB10001424052702303404704577305520318265602|work=The Wall Street Journal}}</ref>
*Keeping all of one's digital files requires less time and effort than evaluating and deleting them.<ref name=cyborg /> 
*Many businesses rely on email correspondence for decision-making and formal approvals, so employees are often careful to keep work emails in case they are needed to verify a decision later.
*[[Data storage device|Data storage devices]] are now so large and inexpensive that individuals and companies often do not feel the need to save data selectively.<ref name=PCMag>{{cite web|title=Definition of: e-hoarder|url=http://www.pcmag.com/encyclopedia/term/64114/e-hoarder|work=PCMag}}</ref><ref name=gayle>{{cite web|last=Gatchelian|first=Gayle|title=Hoarding the ethereal: How we have more things (and more problems) but with less clutter|url=http://web.mit.edu/comm-forum/mit7/papers/GayleGatchalian-DigitalHoarding.pdf|work=Massachusetts Institute of Technology}}</ref>
*The widespread availability and rapid dissemination of [[open content]] on the Internet makes it easier for users to obtain [[digital media]], which can accumulate more quickly than ever.
*Since digital media do not take up physical space, they're less likely to be perceived as clutter, and users can more easily forget the extent of what they own.
*Unlike many physical items, electronic content does not die or decay on its own; users must consciously choose to delete it.<ref name=networkcomputing>{{cite web|last=Fogarty|first=Kevin|title=Digital Hoarding: Do We Have a Problem?|url=http://www.networkcomputing.com/storage-networking-management/digital-hoarding-do-we-have-a-problem/240147047|work=Network Computing}}</ref>

== Repercussions ==

Digital hoarding can lead to many problems:
*Digital clutter can make it more difficult to locate specific files among the extra irrelevant material.
*Excessive digital content takes up more [[hard disk drive|hard drive]] space than it merits, and may even require the addition of extra digital storage to one’s computer or [[mobile phone]].
*[[server farm|Server farms]] use more electricity as they need more disk drives. The extra load is especially notable in corporate domains.<ref name=forbes>{{cite web|last=Sloane|first=Stanton D.|title=The Problem With Packrats:The High Costs Of Digital Hoarding|url=http://www.forbes.com/sites/ciocentral/2011/03/25/the-problem-with-packrats-the-high-costs-of-digital-hoarding/|work=Forbes}}</ref> This adds to an individual’s or company’s electricity expenses and [[carbon footprint]].<ref name=forbes />
*Digital clutter can be mentally draining, requiring time and attention. For example, hoarded emails can make an inbox seem overwhelming. The user wastes time sifting through excess emails, which can result in lowered employee productivity.<ref name=pro>{{cite web|last=Egan|first=Marsha|title=Pro: Don't Litter In Your Electronic Yard|url=http://www.businessweek.com/debateroom/archives/2011/09/e-hoarding_is_unhealthy.html|work=Bloomberg Businessweek}}</ref> 
*Digital hoarding can create an unhealthy attachment to digital content and foster a sort of “media addiction.”<ref name=lifehacker>{{cite web|last=Alan|first=Henry|title=How to Break Your Media Addiction and Clean Up Your Digital Clutter|url=http://lifehacker.com/5948925/how-to-break-your-media-addiction-and-clean-up-your-digital-clutter|work=Lifehacker}}</ref> It is often good for one’s [[mental health]] to let go of useless clutter, and decluttering digital devices can help with decluttering the mind.<ref name=digitaltrends>{{cite web|last=Hill|first=Simon|title=Is Digital Hoarding Dragging You Down?|url=http://www.digitaltrends.com/cool-tech/is-digital-hoarding-dragging-you-down/#!DgIqw|work=Digital Trends}}</ref>

== In the media ==

Many American [[Television documentary|documentary television]] series depict the struggles of compulsive hoarders, such as "[[Hoarding: Buried Alive]]" on [[TLC (TV network)|TLC]] and "[[Hoarders]]" on [[A&E (TV channel)|A&E]]. These shows have popularized awareness of hoarding, showing the consequences of accumulating clutter. However, these programs usually focus on physical hoarding. The [[WPTV-TV|WPTV]] story of [[Fort Lauderdale, Florida]], resident Larry Fisher is a notable exception. This program focused on digital hoarding, depicting Fisher's longstanding refusal to delete any digital content. Instead, Fisher purchased an additional computer every time he ran out of hard drive space.<ref name=WPTV>{{cite web|last=Anfinsen|first=Jason|title=E-hoarding is a new phenomenon that is  quickly spreading amongst computer users|url=http://www.wptv.com/news/science-tech/e-hoarding-is-a-new-phenomenon-spreading-like-a-computer-virus-for-users|work=WPTV}}</ref> The [[BBC News]] story of [[Washington, D.C.]], resident Chris Yurista expresses a counterpoint to this perspective. The program portrayed Yurista as a "21st century [[minimalist]]" for living with hardly any physical assets, substituting [[digital goods]] wherever possible.<ref name="BBC News">{{cite web|last=Danzico|first=Matthew|title=Cult of less: Living out of a hard drive|url=http://www.bbc.co.uk/news/world-us-canada-10928032|work=BBC News}}</ref>

== Criticism ==

Though digital hoarding is often given a negative [[connotation]], some counter that it is not an unhealthy or detrimental practice. One argument states that a large amount of digital content is not a problem in itself; rather, the problem is content [[findability]]. The size of the [[World Wide Web]] illustrates this point: a vast amount of content is available, but [[web search engine|search engines]] such as [[Google]] have mastered effective [[search algorithm|algorithms]] for instantaneous findability. Digital hoarding can also be logical for email correspondence. Businesses often use email as the primary form of communication, so deleting conversations and documents that seem unimportant could be problematic if they are needed later.<ref name=con>{{cite web|last=Waller|first=John|title=Con: Why Delete That Which Takes Up No Space?|url=http://www.businessweek.com/debateroom/archives/2011/09/e-hoarding_is_unhealthy.html|work=Bloomberg Businessweek}}</ref> [[Disk storage]] is increasingly abundant and inexpensive, so concern over the cost of digital hoarding is rarely necessary. In addition, digital hoarding is clearly more benign than physical hoarding, which is more visible and takes up physical space.<ref name=cyborg>{{cite web|last=Case|first=Amber|title=Digital Hoarding|url=http://cyborganthropology.com/Digital_Hoarding|work=Cyborg Anthropology}}</ref> Finally, on a [[subjectivity|subjective level]], digital hoarding can hardly be viewed as problematic if the consumer simply does not feel burdened by their collection of [[digital data]].

== See also ==
* [[Harold T. Martin III]]
* [[Web archiving]]
* [[Digital preservation]]
* [[Kari E.]]

== References ==

{{reflist|2}}

== Further reading ==

* {{cite book | title = Delete: The Virtue of Forgetting in the Digital Age | first=Viktor | last=Mayer-Schönberger | publisher=Princeton University Press | location=Princeton | year=2010 | isbn=978-0691150369}}

[[Category:Compulsive hoarding]]
[[Category:Computing and society]]