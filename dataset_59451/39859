{{Infobox military person
| name          =Lloyd Andrews Hamilton
| image         =Lloyd Andrews Hamilton.jpg
| image_size        = 250
| caption       = Lloyd Andrews Hamilton, 1918
| birth_date    ={{Birth date|df=y|1894|6|13}}
| death_date    ={{Death date and age|df=y|1918|8|24|1894|6|13}}
| birth_place   =[[Troy, New York|Troy]], [[New York (state)|New York]], United States
| death_place   =Near [[Lagnicourt-Marcel]]
| placeofburial       =[[Pittsfield Cemetery]], [[Pittsfield, Massachusetts]]
| placeofburial_label = 
| placeofburial_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| nickname      =
| allegiance    = {{flag|United States|23px}}
| branch        = [[Royal Air Force]] (United Kingdom)<br>[[Air Service, United States Army]]
| serviceyears  =
| rank          =First Lieutenant
| unit          = Royal Air Force
* [[No. 3 Squadron RAF]]
[[Air Service, United States Army]]
* [[17th Aero Squadron]]
| battles       = [[File:World War I Victory Medal ribbon.svg|50px]]&nbsp;[[World War I]]
| commands      =
| awards        =
{{plainlist|
*[[Distinguished Service Cross (United States)|Distinguished Service Cross]]
*[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] (British)
}}
| relations     =
}}
First Lieutenant '''Lloyd Andrews Hamilton''' (13 June 1894 – 24 August 1918) was a World War I [[flying ace]] credited with ten aerial victories.<ref name=Aerodrome>{{cite web |url=http://www.theaerodrome.com/aces/usa/hamilton3.php |title=Lloyd Hamilton |publisher=theaerodrome.com |accessdate=28 February 2010}}</ref> During five months of 1918 he became an ace with the [[Royal Flying Corps]] (RFC) and then again with the [[United States Air Service]] (USAS).<ref name=Novato>{{cite web|url=http://www.cityofnovato.org/Index.aspx?page=387|title=Hamilton Army Field|work=Novato History Museum|publisher=City of Novato|accessdate=January 31, 2011}}</ref> [[Hamilton Air Force Base]] is named after him.

==Early life==
Lloyd Andrews Hamilton was born in [[Troy, New York]], the only child of [[Methodist]] minister Reverend John A. Hamilton and his wife Jennie Andrews Hamilton.<ref name=CrossCockade1979/> He was a bright scholar who took his Baccalaureate Degree ''magnum cum laude'' from [[Syracuse University]] in 1916 and was a member of the Psi Upsilon fraternity. He initiated post-graduate studies at [[Harvard Business School]] in September 1916. When America entered [[World War I]], he enlisted in the USAS,<ref name=Aces>{{cite book |last1=Franks |first1=Norman |first2=Harry |last2=Dempsey |title=American Aces of World War I |publisher=Osprey Publishing |year=2001 |series=Aircraft of the Aces |volume=42 |isbn=1-84176-375-6 |pages=48, 92 }}</ref> on 28 April and in May he reported to [[Plattsburgh (city), New York|Plattsburgh, New York]], for officer training.<ref name=CrossCockade1979/>

==Aviation service==
Hamilton shipped out to England in late 1917 where he trained in early 1918 in an [[Avro 504]], perhaps at RFC Bramham Moor which was then renamed RAF Tadcaster, near [[Bramham cum Oglethorpe]] in Yorkshire.<ref name=CrossCockade1979/> Hamilton was temporarily posted as a [[first lieutenant]] to United Kingdom [[No. 3 Squadron RAF|No. 3 Squadron, Royal Flying Corps]], on 2 March 1918<ref name=Guttman2008>{{cite book|last=Guttman |first=Jon |title=Sopwith Camel Vs Fokker Dr I: Western Front 1917–18 |url=https://books.google.com/books?id=Qswwiv7No80C&pg=PA64 |series=Duel Series |volume=7 |year=2008 |publisher=Osprey Publishing |isbn=1-84603-293-8 |page=64}}</ref> He first scored as a combat pilot on 11 April 1918, flying the [[Sopwith Camel]] against his first German opponent, an [[LVG C.VI]] observation aircraft.<ref name=CrossCockade1979>{{cite journal|year=1979|journal=Cross & Cockade|publisher=The Society of World War I Aero Historians|volume=20|pages=69–82}}</ref> The next day he made his second aerial kill; his third a week later. On 20 April he was flying at the tail end of 'C' flight when his commander [[Richard Raymond-Barker]] was attacked and killed by [[Manfred von Richthofen]], known as the Red Baron. From far away Hamilton saw Richthofen shoot down a second aviator (who crashed but lived), then Hamilton was near enough to engage a blue [[Fokker Dr.I]] triplane, expending more than 300 rounds at it after which it went into a dive and spin, but Hamilton's own maneuvers prevented him from seeing what happened to it. Hamilton returned to base and claimed a kill but it was never confirmed—all of Richthofen's flight had returned safe from the engagement.<ref name=Guttman2008/> On 3 June 1918, Hamilton became an ace, scoring his fifth confirmed victory.<ref name=CrossCockade1979/>

Hamilton was assigned to the USAS 17th Aero Squadron to help them complete their training. When they moved into combat, he was one of their [[Flight commander (position)|Flight Commander]]s. In his service he downed three enemy aircraft and two [[observation balloon]]s, becoming a double ace—once flying under RFC command and once again for USAS.<ref name=Aces/> His first USAS victory on 7 August was during an offensive patrol; the squadron was flying high at {{convert|16000|ft|m|-3}} when they noticed eight [[Fokker D.VII]]s well below them over [[Armentières]]. The 17th squadron dove to attack and Hamilton downed an enemy aircraft after firing 200 rounds.<ref name=CrossCockade1979/>

On Hamilton's final mission he was paired with Lt. Jesse F. Campbell to bomb and strafe transports along the Bapaume–Cambrai road, to strafe enemy troops in retreat and to attack an observation balloon that had been spotted to the north. After dropping their bombs on a small building and transports, Campbell and Hamilton turned to [[Balloon buster|bust the balloon]].<ref name=Aces/> Hamilton sprayed the balloon with machine gun rounds, and its German observer officer was seen to jump from the basket as the balloon exploded in flame. Hamilton was then killed by defensive fire from ground forces.<ref name=CrossCockade1979/>

==Legacy==
[[File:Hamilton AFB plaque.jpg|thumb|A plaque dedicated at [[Hamilton Air Force Base]]]]
Hamilton scored five victories each in 3 Squadron RAF and in the 17th Aero Squadron. He shared some of his victories teamed with such other aces as [[Douglas John Bell]], [[William Tipton]], [[Adrian Franklyn]], [[Will Hubbard]], and [[Robert Miles Todd]].<ref name=Aerodrome/>

Hamilton's body was laid to rest at [[Pittsfield Cemetery]] in [[Pittsfield, Massachusetts]]. His father mentioned Hamilton in Sunday sermons for many years after the war.<ref name=CrossCockade1979/>

In July 1932, the new military airfield at [[Novato]], California was named [[Hamilton Air Force Base|Hamilton Field]] in tribute.<ref name=CrossCockade1979/><ref name=Aces/> In April 1934, a plaque was emplaced to commemorate him.

==Honors and awards==
'''Distinguished Service Cross (DSC)'''

The Distinguished Service Cross is presented to Lloyd A. Hamilton, First Lieutenant (Air Service), U.S. Army, for extraordinary heroism in action at Varssenaere, Belgium, August 13, 1918. Leading a low bombing attack on a German aerodrome, 30 miles behind the line, Lieutenant Hamilton destroyed the hangars on the north side of the aerodrome and then attacked a row of enemy machines, flying as low as 20 feet from the ground despite intense machine-gun fire, and setting fire to three of the German planes. He then turned and fired bursts through the windows of the chateau in which the German pilots were quartered, 26 of whom were afterwards reported killed. General Orders No. 20, W.D., 1919.<ref name=Aerodrome/>

'''Distinguished Flying Cross (DFC)'''

On 13 August 1918, Lt. Hamilton led his flight on a special mission against Varssenaere aerodrome. He dropped four bombs from 200 feet on some aeroplane hangars, making two direct hits and causing a large amount of damage. He then machine gunned the German officers' billets and made four circuits of the aerodrome, shooting up various targets. On the first circuit, he destroyed one EA on the ground which burst into flames when he shot it up. On the third circuit he repeated this performance, setting afire another Fokker biplane. His dash and skill very materially helped in the success of the operation. In addition this officer destroyed a Fokker biplane over Armentières on 7 August 1918. On 12 July he brought down two EA in flames and on two other occasions has driven down out of control enemy machines. He is an excellent patrol leader.<ref name=Aerodrome/>

==See also==
{{Portal|Military of the United States|World War I|Biography}}
* [[List of World War I flying aces from the United States]]

==References==
{{Reflist}}
{{Refbegin}}
{{Refend}}

==External links==
*[https://books.google.com/books?id=NWzlIvgLo2UC&pg=PA11 Army portrait of Hamilton] in ''Hamilton Field'', Novato Historical Guild

{{DEFAULTSORT:Hamilton, Lloyd}}
[[Category:1894 births]]
[[Category:1918 deaths]]
[[Category:American World War I flying aces]]
[[Category:Aviators from New York]]
[[Category:Aviators killed by being shot down]]
[[Category:Recipients of the Distinguished Service Cross (United States)]]
[[Category:Recipients of the Distinguished Flying Cross (United Kingdom)]]
[[Category:Royal Flying Corps officers]]
[[Category:United States Army Air Service pilots of World War I]]