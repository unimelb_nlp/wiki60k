{{good article}}
{{Infobox hurricane season
| Basin=NIO
| Year=1995
| Track=1995 North Indian Ocean cyclone season summary map.png
| First storm formed=May 6, 1995
| Last storm dissipated=November 25, 1995
| Strongest storm name=BOB 07
| Strongest storm pressure=956
| Strongest storm winds=102
| Average wind speed=3
| Total disturbances=8
| Total depressions=6
| Total storms=3
| Total hurricanes=2
| Total intense=2
| Fatalities=554 <!-- 146 from May depressions, 236 from 03B, 172 from 04B -->
| Damages=46.3
| five seasons=[[1993 North Indian Ocean cyclone season|1993]], [[1994 North Indian Ocean cyclone season|1994]], '''1995''', [[1996 North Indian Ocean cyclone season|1996]], [[1997 North Indian Ocean cyclone season|1997]]
|Atlantic season=1995 Atlantic hurricane season
|East Pacific season=1995 Pacific hurricane season
|West Pacific season=1995 Pacific typhoon season
}}
The '''1995 North Indian Ocean cyclone season''' was below-average and was primarily confined to the autumn months, with the exception of three short-lived [[Tropical cyclone scales#North Indian Ocean|deep depressions]] in May. There were eight depressions in the basin, which is [[Indian Ocean]] north of the [[equator]]. The basin is subdivided between the [[Bay of Bengal]] and the [[Arabian Sea]] on the east and west coasts of India, respectively. Storms were tracked by the [[India Meteorological Department]] (IMD), which is the basin's [[Regional Specialized Meteorological Center]], as well as the American-based [[Joint Typhoon Warning Center]] (JTWC) on an unofficial basis.

Tropical activity was largely affected by the [[monsoon trough]], which spawned the three deep depressions in May, as well as the two strongest cyclones in November. The first storm of the season formed on May&nbsp;5 in the Bay of Bengal, the same location as two additional depressions later in the month. Collectively, the three systems killed 146&nbsp;people, mostly related to the third system which produced a deadly [[storm surge]] in [[Bangladesh]]. After two weak depressions in September, the season's lone Arabian Sea storm developed on October&nbsp;12, and remained largely away from land. The final two storms of the season were the most notable. On November&nbsp;10, [[1995 India cyclone|a cyclone]] struck southeastern India, killing 173&nbsp;people in India and Bangladesh. Its remnants produced a rare snowstorm in eastern Nepal that caused landslides and avalanches, killing 63. The last storm of the season was also the most powerful, causing 172&nbsp;deaths when it struck southeastern Bangladesh.
__TOC__
{{Clear}}

==Season summary==
<center>
<timeline>
ImageSize = width:900 height:200
PlotArea  = top:10 bottom:80 right:50 left:20
Legend    = columns:3 left:30 top:58 columnwidth:270
AlignBars = early
DateFormat = dd/mm/yyyy
Period     = from:01/05/1995 till:01/12/1995
TimeAxis   = orientation:horizontal
ScaleMinor = grid:black unit:month increment:1 start:01/05/1995
Colors     =
  id:canvas value:gray(0.88)
  id:GP     value:red
  id:TD     value:rgb(0.5,0.8,1)      legend:Depression_=_≤51_km/h_(≤31_mph)
  id:DD     value:rgb(0.37,0.73,1)    legend:Deep_Depression_=_52–61_km/h_(32–38_mph)
  id:TS     value:rgb(0,0.98,0.96)    legend:Cyclonic_Storm_=_62–87_km/h_(39–54_mph)
  id:ST     value:rgb(0.80,1,1)       legend:Severe_Cyclonic_Storm_=_88–119_km/h_(55–73_mph)
  id:VS     value:rgb(1,1,0.8)        legend:Very_Severe_Cyclonic_Storm_=_118–165_km/h_(73–102_mph)
  id:ES     value:rgb(1,0.76,0.25)    legend:Extremely_Severe_Cyclonic_Storm_=_166–221_km/h_(103–137_mph)
  id:SU     value:rgb(1,0.38,0.38)    legend:Super_Cyclonic_Storm_=_≥222_km/h_(≥138_mph)
Backgroundcolors = canvas:canvas
BarData =
  barset:Hurricane
  bar:Month
PlotData=
  barset:Hurricane width:10 align:left fontsize:S shift:(4,-4) anchor:till
  from:06/05/1995 till:06/05/1995 color:DD text:BOB 01
  from:09/05/1995 till:10/05/1995 color:DD text:BOB 02
  from:15/05/1995 till:17/05/1995 color:DD text:BOB 03
  from:15/09/1995 till:17/09/1995 color:TD text:BOB 04
  from:26/09/1995 till:28/09/1995 color:TD text:BOB 05
  from:13/10/1995 till:17/10/1995 color:TS text:ARB 01
  from:07/11/1995 till:10/11/1995 color:VS text:BOB 06
  from:21/11/1995 till:25/11/1995 color:ES text:BOB 07
  bar:Month width:5 align:center fontsize:S shift:(0,-20) anchor:middle color:canvas

  from:01/05/1995 till:01/06/1995 text:May
  from:01/06/1995 till:01/07/1995 text:June
  from:01/07/1995 till:01/08/1995 text:July
  from:01/08/1995 till:01/09/1995 text:August
  from:01/09/1995 till:01/10/1995 text:September
  from:01/10/1995 till:01/11/1995 text:October
  from:01/11/1995 till:01/12/1995 text:November

</timeline>
</center>
The [[India Meteorological Department]] (IMD) in [[New Delhi]] &ndash; the official [[Regional Specialized Meteorological Center]] for the northern [[Indian Ocean]] as recognized by the [[World Meteorological Organization]] &ndash; issued warnings for tropical cyclones developing in the region, using satellite imagery and surface data to assess and predict storms.<ref name="wmo"/> The agency also utilized a [[tropical cyclone forecast model]] that used climatology and a storm's persistence to forecast future movement. Warnings and advisories were broadcast throughout India by telegraph and news media.<ref name="imd"/> The basin's activity is sub-divided between the [[Arabian Sea]] and the [[Bay of Bengal]] on opposite coasts of India, and is generally split before and after the [[monsoon]] season.<ref name="wmo">{{cite report|publisher=World Meteorological Organization|title=Annual Summary of the Global Tropical Cyclone Season 2000|accessdate=2015-05-22|url=http://www.wmo.int/pages/prog/www/tcp/documents/pdf/TD1082-TCseason2000.pdf|format=PDF}}</ref> Storms were also tracked on an unofficial basis by the American-based [[Joint Typhoon Warning Center]].<ref name="atcr"/>

The JTWC only tracked the longer-lived and stronger cyclonic storms, which all formed after September; by their assessment, this was the fifth such occurrence since 1975 where all storms developed in the autumn.<ref name="atcr"/> Throughout the year, tropical systems generally lasted longer than in [[1994 North Indian Ocean cyclone season|1994]]. The systems that affected land generally struck [[Andhra Pradesh]] and eastward through Bangladesh.<ref name="imd"/> The three cyclonic storms was less than the average of 5.4, and the two severe cyclonic storms was slightly below the average of 2.5.<ref>{{cite report|publisher=India Meteorological Department|title=Frequently Asked Questions on Tropical Cyclones|accessdate=February 29, 2016|url=http://www.rsmcnewdelhi.imd.gov.in/images/pdf/cyclone-awareness/terminology/faq.pdf|page=15|format=PDF}}</ref> In addition to the storms tracked by the IMD, a monsoon depression struck northern Oman in late July, producing heavy rainfall that totaled {{convert|300|mm|in|abbr=on}} on [[Jebel Shams]] mountain. The system later affected the remainder of the [[Arabian Peninsula]].<ref>{{cite book|title=Vegetation of the Arabian Peninsula|accessdate=2015-12-04|author=Shahina Ghazanfar|author2=Martin Fisher|year=1998|url=https://books.google.com/books?id=uc_tCAAAQBAJ&pg=PA17&lpg=PA17}}</ref>

==Systems==

===May deep depressions===
During two weeks in the middle of May, a series of three [[Tropical cyclone scales#North Indian Ocean|deep depressions]] developed in unusual succession in the western Bay of Bengal.<ref name="imd">{{cite report|publisher=India Meteorological Department|date=January 1996|title=Report on cyclonic disturbances over north Indian Ocean during 1995|accessdate=2015-11-23|url=http://www.rsmcnewdelhi.imd.gov.in/images/pdf/archive/rsmc/1995.pdf|format=PDF}}</ref>

Originating from the [[monsoon]] trough,<ref name="mbom">{{cite journal|page=2|author=Darwin Regional Specialised Meteorological Centre|publisher=Bureau of Meteorology|volume=14|number=5|date=May 1995|title=Darwin Tropical Diagnostic Statement|accessdate=2015-11-23|url=http://www.bom.gov.au/climate/ntregion/statements/tropical/dtds-199505.pdf|format=PDF}}</ref> a low pressure area formed just north of Sri Lanka on May&nbsp;5. By 21:00&nbsp;[[Coordinated Universal Time|UTC]] that night, the system organized into a depression while moving west-northwestward toward India. It developed a [[central dense overcast]] of deep convection, prompting the IMD to upgrade it to a 55&nbsp;km/h (35&nbsp;mph) deep depression.<ref name="imd"/> Still associated with the monsoon, the system had several small circulations and gale force winds.<ref name="mbom"/> At 11:00&nbsp;UTC on May&nbsp;6, the system moved ashore [[Tamil Nadu]] near [[Cuddalore]], and by the next day degenerated into a remnant low.<ref name="imd"/> The second deep depression formed on May&nbsp;8 about 120&nbsp;km (75&nbsp;mi) southeast of the [[Andhra Pradesh]] coastline, north of the previous system. It moved to the northeast, intensifying into a 55&nbsp;km/h (35&nbsp;mph) deep depression on May&nbsp;9. At 17:00&nbsp;UTC that day, the system struck Andhra Pradesh near [[Tuni]] as it progressed northward, degenerating into a remnant low on May&nbsp;10.<ref name="imd"/> The third deep depression was also the longest lasting. It formed on May&nbsp;14 off the coast of [[Odisha]], northeast of the previous system. Moving parallel to the coast, it also intensified into a 55&nbsp;km/h (35&nbsp;mph) deep depression on May&nbsp;15. Early the next day, the system made landfall on [[Sagar Island]] in [[West Bengal]] state, and weakened while progressing northeastward into [[Bangladesh]]. On May&nbsp;18, the depression was downgraded to a remnant low over [[Assam]] state.<ref name="imd"/>

The series of storms helped end a drought in eastern India by bringing heavy monsoonal rainfall.<ref name="ew">{{cite news|author=Steve Newman|newspaper=Toronto Star|date=1995-05-20|title=Earthweek: Diary of the planet}}{{subscription required|via=Lexis Nexis}}</ref> The first system brought heavy rainfall to Tamil Nadu and neighboring Andhra Pradesh, while the second storm mainly dropped rainfall in the latter state. The third system brought precipitation to Odisha, West Bengal, and Bangladesh;<ref name="imd"/> [[Bhubaneswar]] in Odisha reported {{convert|567|mm|in|abbr=on}} of rainfall over six days.<ref>{{cite news|newspaper=The Guardian|location=London|date=1995-05-16|title=The Week that the Rains Came Down; Weatherwatch}}{{subscription required|via=Lexis Nexis}}</ref> The rains resulted in flooding and damages to crops,<ref name="imd"/> while wrecking dozens of homes.<ref>{{cite news|agency=Associated Press|date=1995-05-09|title=Heavy Rains Kill 17 People in Southern India}}{{subscription required|via=Lexis Nexis}}</ref> Collectively, the storms killed 86&nbsp;people in India.<ref name="imd"/>

In Bangladesh, the third storm produced a 3&nbsp;m (10&nbsp;ft) [[storm surge]] and heavy rainfall, reaching {{convert|147|mm|in|abbr=on}} over 24&nbsp;hours in [[Chittagong]].<ref name="afp516"/> About 100,000&nbsp;people evacuated their houses to storm shelters due to the floods, while another 100,000 were stranded in their homes due to floods.<ref name="ap516"/> Many [[embankment dam]]s were damaged, furthering flooding.<ref name="undha65"/> On [[Hatia Island]], the storm wrecked over 5,000&nbsp;homes and {{convert|10000|ha|acre|abbr=on}} of crops,<ref name="afp516">{{cite news|agency=Agence France-Presse|date=1995-05-16|title=Three killed in cyclone, tidal surge on Bangladesh coast}}{{subscription required|via=Lexis Nexis}}</ref> with salt and shrimp farms in the region also destroyed. A {{convert|6|ft|m|abbr=on|order=flip}} storm tide flooded dozens of villages around [[Cox's Bazar]], destroying about 1,000&nbsp;houses.<ref name="ap516"/> Two bridges were destroyed, severing traffic between Cox's Bazar and Chittagong. In the latter city, 20,125&nbsp;houses were damaged or destroyed.<ref name="dha522">{{cite report|date=1995-06-05|agency=United Nations Department of Humanitarian Affairs|publisher=ReliefWeb|title=Bangladesh - Rainstorm/Tidal Surge Information Report No. 3|accessdate=2015-11-23|url=http://reliefweb.int/report/bangladesh/bangladesh-rainstormtidal-surge-information-report-no-3}}</ref> Over 60,000&nbsp;people were left homeless in the country, and there were 60&nbsp;deaths related to the storm.<ref name="imd"/> However, the rains also helped end a damaging five-month drought in the country.<ref name="ap516">{{cite news|agency=Associated Press|date=1995-05-16|title=Floods Kill 5 People, Maroon 100,000 Others}}{{subscription required|via=Lexis Nexis}}</ref> The government provided wheat and cash to affected residents to help cope with the disaster.<ref name="dha522"/> After the storm moved through the area, most freshwater ponds were intruded by saltwater, furthering damage to crops and causing a shortage of drinking water.<ref name="undha65">{{cite report|date=1995-06-05|agency=United Nations Department of Humanitarian Affairs|publisher=ReliefWeb|title=Bangladesh - Rainstorm/Tidal Surge Information Report No. 4|accessdate=2015-11-23|url=http://reliefweb.int/report/bangladesh/bangladesh-rainstormtidal-surge-information-report-no-4}}</ref> About 50,000&nbsp;people became ill after drinking contaminated water, killing around 400&nbsp;people due to a diarrhea outbreak.<ref>{{cite news|agency=Agence France-Presse|date=1995-05-21|title=Cyclone death toll nears 400}}{{subscription required|via=Lexis Nexis}}</ref>

===Depression BOB 04 (01B)===
{{Infobox Hurricane Small
|Basin=NIO
|Image=01B Sept 17 1995 0751Z.png
|Track=Cyclone 01B 1995 track.png
|Formed=September 15
|Dissipated=September 17
|1-min winds=45
|3-min winds=25
|Pressure=998
}}
On September&nbsp;9, a [[1995 Pacific typhoon season#Tropical Depression 16W|tropical depression]] formed in the [[South China Sea]] in the west Pacific basin. Two days later, the system struck [[Vietnam]] and progressed westward through [[Indochina]]. The remnants entered the Bay of Bengal around September&nbsp;13, accompanied by increasing convection. Moving to the west-northwest, the system resembled a monsoon depression at times, although the thunderstorms gradually became more concentrated.<ref name="atcr">{{cite report|author=Eric J. Trehubenko|author2=Charles P. Guard|author3=Gary B. Kubat|author4=William J. Carle|publisher=Joint Typhoon Warning Center|date=1996|title=1995 Annual Tropical Cyclone Report|series=[http://www.usno.navy.mil/JTWC/annual-tropical-cyclone-reports Annual Tropical Cyclone Reports]|accessdate=2015-11-24|url=http://www.usno.navy.mil/NOOC/nmfc-ph/RSS/jtwc/atcr/1995atcr.pdf|pages=214–238|location=Hagåtña, Guam}}</ref> On September&nbsp;16, the system developed into a depression,<ref name="imd"/> and on the same day the JTWC classified it as Tropical Cyclone 01B.<ref name="atcr"/> As the system approached the Indian coastline, it developed good [[outflow (meteorology)|outflow]],<ref name="sbom">{{cite journal|page=2|author=Darwin Regional Specialised Meteorological Centre|publisher=Bureau of Meteorology|volume=14|number=9|date=September 1995|title=Darwin Tropical Diagnostic Statement|accessdate=2015-11-24|url=http://www.bom.gov.au/climate/ntregion/statements/tropical/dtds-199509.pdf|format=PDF}}</ref> and was intensifying quickly. The JTWC estimated peak 1&nbsp;minute winds of 85&nbsp;km/h (50&nbsp;mph) at 18:00&nbsp;UTC on September&nbsp;16.<ref name="atcr"/> However, the IMD never assessed winds beyond 45&nbsp;km/h (30&nbsp;mph). Around 01:00&nbsp;UTC on September&nbsp;17, the depression moved ashore India near [[Balasore]], Odisha. That night, the system degenerated into a remnant low over [[Bihar]],<ref name="imd"/> although the remnants persisted until September&nbsp;20, when they dissipated near [[Delhi]].<ref name="atcr"/> The depression brought heavy rainfall over Odisha and Bihar.<ref name="imd"/>
{{Clear}}

===Depression BOB 05===
Later in September, another depression formed on September&nbsp;26 in the northwestern Bay of Bengal. Moving northwestward, it quickly moved ashore near Balasore, Odisha, failing to intensify beyond winds of 85&nbsp;km/h (50&nbsp;mph). A [[ridge (meteorology)|ridge]] turned the system to the northeast, and the depression dissipated on September&nbsp;28 over West Bengal. The depression brought heavy rainfall to Odisha, Bihar, and West Bengal, peaking at {{convert|570|mm|in|abbr=on}} in [[Malda district]] in West Bengal.<ref name="imd"/>

===Cyclonic Storm ARB 01 (02A)===
{{Infobox Hurricane Small
|Basin=NIO
|Track=Cyclone 02A 1995 track.png
|Formed=October 13
|Dissipated=October 17
|1-min winds=50
|3-min winds=45
|Pressure=996
}}
A low pressure area accompanied by a well-defined circulation persisted over central India on October&nbsp;11. By the following day, the system emerged into the Arabian Sea, whereupon its convection organized west of a sheared circulation. On October&nbsp;12, the system organized into a depression, classified Tropical Cyclone 02A by the JTWC. Steered by a ridge, it moved to the west-northwest and gradually intensified. The IMD upgraded it to a cyclonic storm on October&nbsp;14, estimating peak 3&nbsp;minute winds of 85&nbsp;km/h (50&nbsp;mph). The JTWC assessed slightly higher 1&nbsp;minute winds of 95&nbsp;km/h (60&nbsp;mph). Increased wind shear stripped away the convection, causing the storm to weaken. By October&nbsp;17, the system deteriorated into a depression and began drifting to the southwest, having moved between two ridges. Later that day, the system degenerated into a remnant low, which the JTWC tracked for an additional day until dissipation east of the [[Somalia]] coastline.<ref name="imd"/><ref name="atcr"/>
{{Clear}}

===Very Severe Cyclonic Storm BOB 06 (03B)===
{{Infobox Hurricane Small
|Basin=NIO
|Image=03B Nov 9 1995 0300Z.png
|Track=Cyclone 03B 1995 track.png
|Formed=November 6
|Dissipated=November 10
|1-min winds=70
|3-min winds=65
|Pressure=984
}}
{{main|1995 India cyclone}}
The storm originated from the monsoon trough on November&nbsp;7 in the Bay of Bengal, east of India. Moving northwestward, the system gradually intensified while moving toward land, eventually developing an [[eye (cyclone)|eye]] in the middle of the convection. Reaching peak 3&nbsp;minute winds of 120&nbsp;km/h (75&nbsp;mph), the IMD classified the system as a very severe cyclonic storm, in line with the 130&nbsp;km/h (80&nbsp;mph) wind estimate from the JTWC. On November&nbsp;9, the cyclone made landfall near the border of [[Andhra Pradesh]] and [[Orissa, India|Orissa]].<ref name="imd"/><ref name="atcr"/> Atypical for most November storms, the system continued to the north and dissipated over Nepal on November&nbsp;11.<ref name="imd"/><ref name="arc">{{cite conference |title=Storms and Avalanches of November 1995, Khumbu Himal, Nepal |author=Richard Kattelmann |author2=Tomomi Yamada|year=1997 |conference=Proceedings International Snow Science Workshop|url=http://arc.lib.montana.edu/snow-science/objects/issw-1996-276-278.pdf|access-date=2015-11-30}}</ref>

In India, the cyclone's strong winds were accompanied by heavy rainfall and a [[storm surge]] of {{convert|1.5|m|ft|abbr=on}} that inundated the coastline several hundred feet inland. Power lines, crops, and houses were damaged, and many boats were damaged, causing several nautical fatalities.<ref name="imd"/> The cyclone killed 128&nbsp;people and caused [[United States dollar|US$]]46.3&nbsp;million in damage.{{EM-DAT}} In neighboring Bangladesh, high waves killed 45&nbsp;people after sinking or sweeping away four ships.<ref>{{cite news|agency=United Press International|date=1995-05-10|title=Bangladesh floods}}{{subscription required|via=Lexis Nexis}}</ref> The cyclone later spawned a rare November snowstorm across eastern Nepal, with depths reaching {{convert|2000|mm|in|abbr=on}}. The snowfall occurred without warning amid the busy mountain trekking season, and there were several avalanches and landslides across the country. One such incident killed 24&nbsp;people at a lodge near [[Gokyo]], and there were 63&nbsp;deaths related to the cyclone in the country. The Nepal government launched the largest search and rescue mission in the country's history, rescuing 450&nbsp;people,<ref name="arc"/> some of whom trapped for days in the snow.<ref>{{Cite news|location=Bryan, Ohio|date=1995-11-15|agency=Associated Press|title=Avalanche, landslide survivors are returning to Nepal's capital|accessdate=2015-12-01|url=https://news.google.com/newspapers?nid=799&dat=19951115&id=3ZNTAAAAIBAJ&sjid=v4cDAAAAIBAJ&pg=6195,686516&hl=en}}</ref>
{{Clear}}

===Extremely Severe Cyclonic Storm BOB 07 (04B)===
{{Infobox Hurricane Small
|Basin=NIO
|Image=04B Nov 24 1995 0600Z.png
|Track=Cyclone 04B 1995 track.png
|Formed=November 21
|Dissipated=November 25
|1-min winds=105
|3-min winds=102
|Pressure=956
}}
An area of convection blossomed near northern [[Sumatra]] on November&nbsp;18,<ref name="atcr"/> associated with the monsoon. Aided by the same westerly wind burst that assisted [[1995–96 Australian region cyclone season#Severe Tropical Cyclone Daryl-Agnielle|Cyclone Daryl]] in the southern hemisphere, the disturbance gradually organized and persisted while moving west-northwestward through the Bay of Bengal.<ref name="atcr"/><ref name="nbom">{{cite journal|page=2|author=Darwin Regional Specialised Meteorological Centre|publisher=Bureau of Meteorology|volume=14|number=11|date=November 1995|title=Darwin Tropical Diagnostic Statement|accessdate=2015-12-04|url=http://www.bom.gov.au/climate/ntregion/statements/tropical/dtds-199511.pdf|format=PDF}}</ref> Late on November&nbsp;21, the system developed into a depression,<ref name="imd"/> which the JTWC classified as Tropical Cyclone 04B. Steady intensification ensued; by November&nbsp;23, the JTWC upgraded the cyclone to the equivalent of a minimal hurricane,<ref name="atcr"/> and the IMD steadily upgraded the storm to increasing categories.<ref name="imd"/> The storm turned to the north and northeast around a ridge, accelerating toward land. At 06:00&nbsp;UTC on November&nbsp;24, the JTWC estimated peak 1&nbsp;minute winds of 195&nbsp;km/h (120&nbsp;mph).<ref name="atcr"/> Shortly thereafter, the IMD estimated peak 3&nbsp;minute winds of 190&nbsp;km/h (115&nbsp;mph), making the system an [[Tropical cyclone scales#North Indian Ocean|extremely severe cyclonic storm]]. By that time, the system had a well-defined eye in the center of deep convection. Due to increasing wind shear, the storm weakened and made landfall over southeastern Bangladesh with winds of around 120&nbsp;km/h (75&nbsp;mph), south of Cox's Bazaar around 09:00&nbsp;UTC on November&nbsp;25. A few hours later, the system degenerated into a remnant low over northern Myanmar.<ref name="imd"/>

Along the coast of Bangladesh, Cox's Bazaar reported winds of 93&nbsp;km/h (58&nbsp;mph).<ref name="atcr"/> The storm brought heavy rainfall and produced high waves that flooded offshore islands with a {{convert|3|to|4|ft|m|abbr=on|order=flip}} storm surge. Most residents of the offshore islands were evacuated ahead of the storm,<ref>{{cite report|date=1995-11-25|agency=United Nations Department of Humanitarian Affairs|publisher=ReliefWeb|title=Bangladesh - Cyclone Information Report No.3|accessdate=2015-12-04|url=http://reliefweb.int/report/bangladesh/bangladesh-cyclone-information-report-no3}}</ref> totaling 300,000&nbsp;evacuees. About 10,000&nbsp;huts were destroyed, mostly made of mud and straw, while crops in the region were damaged. The storm's winds cut power lines and communication links in the hardest hit areas.<ref>{{cite news|author=Tofael Ahmed|agency=Associated Press|date=1995-11-25|title=International News}}{{subscription required|via=Lexis Nexis}}</ref> Initially, about 500&nbsp;fishermen were missing after the storm's passage; most were rescued or swam ashore, but over 100&nbsp;people were killed when 10&nbsp;boats were lost.<ref>{{cite news|agency=Associated Press|date=1995-11-30|title=100 Fishermen Killed In Weekend Cyclone}}{{subscription required|via=Lexis Nexis}}</ref> The International Disaster Database listed 172&nbsp;fatalities associated with the storm.{{EM-DAT}} In neighboring Myanmar, the cyclone destroyed most of the rice crop in [[Rakhine State]] just before the harvest, forcing many [[Rohingya people|Rohingya farmers]] to borrow money to compensate for lost income.<ref>{{cite journal|journal=Human Rights Watch|date=September 1996|volume=8|number=8|title=The Rohingya Muslims: Ending a Cycle of Exodus?|publisher=Burma Library|accessdate=2015-12-04|url=http://burmalibrary.org/docs/ROHINGYA.cycle.htm}}</ref> 
{{Clear}}

==Season effects==
This is a table of all storms in the 1995 North Indian Ocean cyclone season. It mentions all of the season's storms and their names, duration, peak intensities (according to the IMD storm scale), damage, and death totals. Damage and death totals include the damage and deaths caused when that storm was a precursor wave or extratropical low, and all of the damage figures are in 1995 USD.

{{North Indian Ocean areas affected (Top)}}"
|-
| BOB 01 || {{Sort|01|May 6}} || bgcolor=#{{storm colour|deepdepression}}|{{Sort|2|Deep depression}} || bgcolor=#{{storm colour|deepdepression}}|{{Sort|030|Not specified}} || bgcolor=#{{storm colour|deepdepression}}|{{sort|1016|Not specified}} || South India || None || None || 
|-
| BOB 02 || {{Sort|02|May 8&nbsp;– 10}} || bgcolor=#{{storm colour|deepdepression}}|{{Sort|2|Deep depression}} || bgcolor=#{{storm colour|deepdepression}}|{{Sort|030|Not specified}} || bgcolor=#{{storm colour|deepdepression}}|{{sort|1016|Not specified}} || Andhra Pradesh || None || {{nts|86}} || 
|-
| BOB 03 || {{Sort|03|May 14&nbsp;– 18}} || bgcolor=#{{storm colour|deepdepression}}|{{Sort|2|Deep depression}} || bgcolor=#{{storm colour|deepdepression}}|{{Sort|030|Not specified}} || bgcolor=#{{storm colour|deepdepression}}|{{sort|1016|Not specified}} || Odisha || None || {{nts|60}} || 
|-
| BOB 04 || {{Sort|04|September 15&nbsp;– 17}} || bgcolor=#{{storm colour|niodepression}}|{{Sort|1|Depression}} || bgcolor=#{{storm colour|niodepression}}|{{Sort|045|45&nbsp;km/h (30&nbsp;mph)}} || bgcolor=#{{storm colour|niodepression}}|{{sort|0998|998&nbsp;hPa (29.47&nbsp;inHg)}} || Myanmar, Odisha, East India ||None || None ||
|-
| BOB 05 || {{Sort|05|September 26&nbsp;– 28}} || bgcolor=#{{storm colour|niodepression}}|{{Sort|1|Depression}} || bgcolor=#{{storm colour|niodepression}}|{{Sort|030|Not specified}} || bgcolor=#{{storm colour|niodepression}}|{{sort|1016|Not specified}} || West Bengal, East India ||None|| None ||
|-
| ARB 01 || {{Sort|06|October 13&nbsp;– 17}} || bgcolor=#{{storm colour|CS}}|{{Sort|3|Cyclonic storm}} || bgcolor=#{{storm colour|CS}}|{{Sort|085|85&nbsp;km/h (50&nbsp;mph)}} || bgcolor=#{{storm colour|CS}}|{{sort|0996|996&nbsp;hPa (29.41&nbsp;inHg)}} || West India, Oman, Yemen, Somalia || None ||  None ||
|-
| [[1995 India cyclone|BOB 06]] || {{Sort|07|November 7&nbsp;– 11}} || bgcolor=#{{storm colour|VSCS}}|{{Sort|4|Very severe cyclonic storm}} || bgcolor=#{{storm colour|VSCS}}|{{Sort|120|120&nbsp;km/h (75&nbsp;mph)}} || bgcolor=#{{storm colour|VSCS}}|{{sort|0984|984&nbsp;hPa (29.06&nbsp;inHg)}} || India, Bangladesh, Nepal ||{{ntsp|46300000||$}} || {{nts|236}} ||
|-
| BOB 07 || {{Sort|08|November 21&nbsp;– 25}} || bgcolor=#{{storm colour|ESCS}}|{{Sort|5|Extremely severe cyclonic storm}} || bgcolor=#{{storm colour|ESCS}}|{{Sort|190|190&nbsp;km/h (115&nbsp;mph)}} || bgcolor=#{{storm colour|ESCS}}|{{sort|0956|956&nbsp;hPa (28.23&nbsp;inHg)}} ||Sumatra, Myanmar, Bangladesh ||Unknown || {{nts|172}} ||
|-
{{TC Areas affected (Bottom)|TC's=8&nbsp;systems|dates=May 6&nbsp;– November 25|winds=190&nbsp;km/h (115&nbsp;mph)|pres=956&nbsp;hPa (28.23&nbsp;inHg)|damage={{ntsp|46300000||$}}|deaths={{nts|554}}|Refs=}}

==See also==
{{Portal|Tropical cyclones}}
*[[North Indian Ocean cyclone season]]
*[[1995 Atlantic hurricane season]]
*[[1995 Pacific hurricane season]]
*[[1995 Pacific typhoon season]]
*South-West Indian Ocean cyclone season: [[1994-95 South-West Indian Ocean cyclone season|1994–95]], [[1995-96 South-West Indian Ocean cyclone season|1995–96]]
*Australian region cyclone season: [[1994-95 Australian region cyclone season|1994–95]], [[1995-96 Australian region cyclone season|1995–96]]
*South Pacific cyclone season: [[1994-95 South Pacific cyclone season|1994–95]], [[1995-96 South Pacific cyclone season|1995–96]]

==References==
{{Reflist|30em}}

==External links==
*[http://www.ndmindia.nic.in/WCDRDOCS/Primer%20for%20Parliamentarians.pdf Primer for Parliamentarians]
*[http://australiasevereweather.com/cyclones/ Gary Padgett Summaries]
*[http://www.weather.unisys.com/hurricane/indian_oc/index.html Unisys Track]
*[http://www.usno.navy.mil/JTWC/annual-tropical-cyclone-reports JTWC Archive]

{{TC Decades|Year=1990|basin=North Indian Ocean|type=cyclone}}
{{1995 North Indian Ocean cyclone season buttons}}

{{DEFAULTSORT:1995 North Indian Ocean Cyclone Season}}
[[Category:1995 North Indian Ocean cyclone season]]