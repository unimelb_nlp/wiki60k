{{Indonesian name}}
{{Infobox person
| name        = Andjar Asmara
| image       = Andjar Asmara p17.JPG
| alt         = A man, wearing a suit and bow tie, looking at the camera, his head tilted to one side
| caption     = Andjar, 1930
| birth_name  = Abisin Abbas
| birth_date  = {{birth date|df=yes|1902|02|26}}
| birth_place = [[Alahan Panjang]], [[West Sumatra]], [[Dutch East Indies]]
| death_date  = {{death date and age|df=yes|1961|10|20|1902|02|26}}
| death_place = Cipanas, [[West Java]], [[Indonesia]]
| nationality = [[Demographics of Indonesia|Indonesian]]
| spouse      = [[Ratna Asmara]]
| known_for   = {{Flatlist|
* Journalism
* stage plays
* filmmaking
}}
}}

'''Abisin Abbas''' ({{IPA-id|aˈbisɪn aˈbas|}}; 26 February 1902&nbsp;– 20 October 1961), better known by his pseudonym '''Andjar Asmara''' ({{IPA-id|anˈdʒar asˈmara|}}), was a dramatist and filmmaker active in the cinema of the [[Dutch East Indies]]. Born in [[Alahan Panjang]], [[West Sumatra]], he first worked as a reporter in [[Jakarta|Batavia]] (modern-day Jakarta). He became a writer for the Padangsche Opera in [[Padang]], where he developed a new, dialogue-centric style, which later spread throughout the region. After returning to Batavia in 1929, he spent over a year as a theatre and film critic. In 1930 he joined the [[Dardanella (theatre company)|Dardanella touring troupe]] as a writer. He went to India in an unsuccessful bid to film his [[stage play]] ''Dr Samsi''.

After leaving Dardanella in 1936, Andjar established his own troupe. He also worked at a publishers, writing [[Serial (literature)|serials]] based on successful films. In 1940 he was asked to join [[The Teng Chun]]'s company, Java Industrial Film, helping with marketing and working as a director for two productions. After the [[Japanese occupation of Indonesia|Japanese occupation]], during which time he stayed in theatre, Andjar made a brief return to cinema. He directed three films in the late 1940s and wrote four [[screenplay]]s, which were produced as films in the early 1950s. He published a novel, ''Noesa Penida'' (1950). Afterward he worked for the remainder of his life writing serials based on local films and publishing film criticism. Historians recognise him as a pioneer of theatre and one of the first [[native Indonesian]] film directors, although he had little creative control of his productions.
{{TOC limit|2}}

== Early life and theatre ==
Andjar was born Abisin Abbas{{sfn|Encyclopedia of Jakarta, Andjar Asmara}} in [[Alahan Panjang]], [[West Sumatra]], on 26&nbsp;February 1902. He gravitated toward traditional theatre at a young age after visits from the wandering Wayang Kassim and Juliana Opera ''stambul'' troupes;{{sfn|Said|1982|pp=136–137}}{{sfn|Cohen|2003|pp=215–216}} he pretended to act with his friends in stage plays which they had seen.{{sfn|TIM, Andjar Asmara}} After completing his formal education up to the [[Meer Uitgebreid Lager Onderwijs]] (junior high school) level&nbsp;– first in [[Malay-language]] schools then Dutch ones{{sfn|Eneste|2001|p=23}}&nbsp;– he moved to [[Jakarta|Batavia]] (modern-day Jakarta). He worked as a reporter for two [[daily newspaper]]s, ''Bintang Timoer'' and ''Bintang Hindia''; he may have also worked on a farm.{{sfn|TIM, Andjar Asmara}}{{sfn|I.N.|1981|p=212}}

Around 1925, having had little success in Batavia, Andjar moved to [[Padang]], where he was a reporter for the [[Daily newspaper|daily]] ''Sinar Soematera''. At the same time, he worked with the city's Padangsche Opera, writing [[stage play]]s.{{sfn|Said|1982|pp=136–137}}{{sfn|Filmindonesia.or.id, Dr Samsi}} In contrast to the standard musical theatre of the time, ''[[bangsawan]]'', he promoted a more natural style, using dialogue instead of song to convey the story;{{sfn|TIM, Andjar Asmara}} he referred to this as ''toneel'', based on the Dutch word for theatre.{{sfn|Cohen|2003|pp=215–216}} Among the works he wrote for the Padangsche Opera were adaptations of ''Melati van Agam'', a 1923 work by Swan Pen,{{efn|Pen may have been Parada Harahap, who had been Andjar's editor at ''Bintang Hindia'' {{harv|I.N.|1981|p=213}}.}}{{sfn|TIM, Andjar Asmara}}{{sfn|Biran|2009|pp=99, 108}} and ''[[Sitti Nurbaya]]'', a 1922 novel by [[Marah Roesli]].{{sfn|Cohen|2003|pp=215–216}} These works were well received.{{sfn|TIM, Andjar Asmara}}

[[File:Dr Samsi advertisement, Doenia Film October 1931 p14.jpg|thumb|left|Advertisement for the premiere of Andjar's stageplay, ''Dr Samsi'']]
In the late 1920s, after spending some two years in [[Medan]] with the daily ''Sinar Soematra'',{{sfn|I.N.|1981|p=214}} Andjar returned to Batavia and in 1929 helped establish the magazine ''Doenia Film'', a Malay adaptation of the Dutch-language magazine ''Filmland''; although an adaptation, ''Doenia Film'' also contained original coverage of the domestic theatre and film industry.{{sfn|Said|1982|pp=136–137}}{{sfn|Biran|2009|pp=43–44}} At the time, the cinema of the Indies was becoming established: the first domestic film, ''[[Loetoeng Kasaroeng]]'' (''The Lost Lutung''), was released in 1926, and four additional films were released in 1927 and 1928.{{sfn|Biran|2009|p=379}} Andjar wrote extensively regarding local cinematic and theatrical productions; for example, the Indonesian film critic Salim Said writes Andjar inspired the marketing for 1929's ''[[Njai Dasima (1929 film)|Njai Dasima]]'', which emphasised the exclusively [[native Indonesians|native]] cast.{{efn|During this period the [[Indonesian National Awakening]] was in full force; the year before the [[Youth Pledge]], which affirmed that all natives of the [[Indonesian archipelago]] are one people, had been read at the Second Youth Congress {{harv|Ricklefs|2001|p=233}}. }}{{sfn|Said|1982|p=20}} In 1930 Andjar left ''Doenia Film'' and was replaced by [[Bachtiar Effendi]].{{sfn|Biran|2009|pp=6, 14, 20}}

Andjar became a writer for the theatrical troupe [[Dardanella (theatre company)|Dardanella]] in November 1930, working under the group's founder Willy A. Piedro.{{efn|Piedro, the son of a circus performer, was of Russian descent and initially wrote many of Dardanella's stage plays {{harv|Biran|2009|pp=14, 17}}.}} Andjar believed the troupe to be dedicated to the betterment of the ''toneel'' as an art form and not only motivated by financial interests, as were the earlier ''stambul'' troupes.{{efn|In his final letter as editor of ''Doenia Film'', as quoted by {{harvtxt|Biran|2009|pp=6, 14, 20}}, Andjar wrote that he was joining Dardanella to help promote ''toneel'' as an art form to the best of his abilities.}}{{sfn|Biran|2009|pp=6, 14, 20}} He wrote and published many plays with the group's backing, including ''Dr Samsi'' and ''Singa Minangkabau'' (''The Lion of Minangkabau'').{{sfn|TIM, Andjar Asmara}}{{sfn|Biran|2009|pp=6, 14, 20}} Andjar also worked as a theatre critic, writing several pieces on the history of local theatre, sometimes using his birth name and sometimes his pseudonym.{{sfn|Cohen|2006|pp=347, 402}} In 1936 Andjar went with Dardanella to India to record a film adaptation of his drama ''Dr Samsi'', which followed a doctor who was blackmailed after an unscrupulous [[Indo people|Indo]] discovered he had an illegitimate child.{{sfn|Said|1982|pp=136–138}}{{sfn|Biran|2009|p=23}} The deal fell through, however, and Andjar left India with his wife [[Ratna Asmara|Ratna]].{{sfn|Biran|2009|p=25}}{{sfn|JCG, Dardanella}}

== Film career and death ==
[[File:Ratna Asmara p298.JPG|thumb|upright|alt=A woman with her hair tied back, looking at the camera and smiling|Andjar's wife, [[Ratna Asmara|Ratna]], was the first female film director in Indonesian history.]]
Upon his return to the Indies, Andjar formed another theatrical troupe, Bolero, with Effendi, but left the troupe around 1940 to work at Kolf Publishers in [[Surabaya]]. Effendi was left as the head of Bolero,{{sfn|Said|1982|pp=136–138}} which then became more politicised.{{sfn|Bayly|Harper|2007|p=116}} At Kolf Andjar edited the publisher's magazine ''Poestaka Timoer''.{{sfn|Encyclopedia of Jakarta, Andjar Asmara}} As his work entailed writing synopses and [[serial (literature)|serials]] based on popular films for Kolf's magazine, he became increasingly involved in the film industry. He was soon asked by [[The Teng Chun]], with whom he had maintained a business relationship, to direct a film for his company Java Industrial Film (JIF);{{sfn|Said|1982|pp=136–137}} with this Andjar became one of several noted theatrical personnel who migrated to film following [[Albert Balink]]'s 1937 hit ''[[Terang Boelan]]'' (''Full Moon'').{{sfn|Said|1982|p=27}}{{sfn|Biran|2009|p=169}}

After handling the marketing for ''[[Rentjong Atjeh]]'' (''Rencong of Aceh'', 1940),{{sfn|Biran|2009|p=210}} Andjar made his directorial debut in 1940 with ''[[Kartinah]]'', a war-time romance starring Ratna Asmara.{{sfn|Said|1982|pp=136–137}}{{sfn|Biran|2009|p=213}} ''[[Academia]]'' was critical of the film, believing it to lack educational value.{{sfn|Biran|2009|p=266}} In 1941 he directed ''[[Noesa Penida]]'', a tragedy based in [[Bali]], for JIF;{{sfn|Biran|2009|p=217, 278}} the film was remade in 1988.{{sfn|Filmindonesia.or.id, Filmografi}} In these films, he had little creative control, and performed as what the Indonesian [[Entertainment journalism|entertainment journalist]] Eddie Karsito describes as a dialogue coach. Camera angles and locations were chosen by the [[cinematographer]], who was generally also the producer.{{sfn|Karsito|2008|p=23}}

During the [[Japanese occupation of Indonesia|Japanese occupation]] from 1942 to 1945, the nation's film industry nearly ceased to exist: all but one studio were closed, and all films released were [[Japanese propaganda during World War II|propaganda]] pieces to assist the Japanese war effort and promote the [[Greater East Asia Co-Prosperity Sphere]].{{sfn|Biran|2009|pp=334–351}} Andjar was not involved in these but was excited by the artistic merits of [[Japanese cinema|Japanese films]].{{sfn|Said|1982|p=35}} Although he wrote several short stories during this time, three of which [[List of literary works published in Asia Raja|were published]] in the pro-Japanese newspaper ''[[Asia Raja]]'' in 1942,{{sfn|Mahayana|2007|pp=209–215}} Andjar focused on theatre, forming the troupe Tjahaya Timoer.{{sfn|Biran|2009|p=329}} He often visited the Cultural Centre (Keimin Bunka Sidosho) in Jakarta, where two employees, [[D. Djajakusuma]] and [[Usmar Ismail]], discussed filmmaking with him. Both became influential film directors during the 1950s.{{efn|Ismail's 1950 film ''[[Darah dan Doa]]'' (released internationally as ''The Long March'', literally ''Blood and Prayers'') is generally considered the first truly Indonesian film {{harv|Biran|2009|p=45}}, while Djajakusuma was known for incorporating various aspects of traditional [[Indonesian culture]] in his films {{harv|Sen|Hill|2000|p=156}}.}}{{sfn|Said|1982|p=34}}

After [[Proclamation of Indonesian Independence|Indonesia's independence]], Andjar moved to [[Purwokerto]] to lead the daily ''Perdjoeangan Rakjat''.{{sfn|Encyclopedia of Jakarta, Andjar Asmara}} After the paper collapsed,{{sfn|I.N.|1981|p=215}} he returned to film, film a piece entitled ''[[Djaoeh Dimata]]'' for the [[Netherlands Indies Civil Administration]] in 1948.{{sfn|Said|1982|pp=36–37}} This was followed by two additional films, ''Anggrek Bulan'' (''Moon Orchid''; 1948) and ''[[Gadis Desa]]'' (''Maiden from the Village''; 1949), both based on plays he wrote several years earlier.{{sfn|Encyclopedia of Jakarta, Andjar Asmara}} In 1950, Andjar published his only novel, ''Noesa Penida'', a critique of the [[Balinese caste system]], which followed lovers from different levels of the social hierarchy.{{sfn|Mahayana|Sofyan|Dian|1995|pp=86–88}} Meanwhile, he continued to write and publish [[paperback]] serials adapted from local films.{{sfn|Filmindonesia.or.id, Abisin Abbas}}

Andjar's screenplay ''Dr Samsi'' was finally adapted as a film in 1952 by Ratna Asmara, who had become Indonesia's first female film director with her 1950 film ''[[Sedap Malam]]'' (''Sweetness of the Night''). The adaptation starred Ratna and Raden Ismail.{{sfn|Filmindonesia.or.id, Dr Samsi}}{{sfn|Swestin|2009|p=104}} It would prove Andjar's last screenwriting credit during his lifetime.{{sfn|Filmindonesia.or.id, Filmografi}} Although no longer writing films, Andjar remained active in the country's film industry. In 1955 he headed the inaugural [[Indonesian Film Festival]], which was criticized when it gave the Best Picture Award to two films, Usmar Ismail's ''[[Lewat Djam Malam]]'' (''After the Curfew'') and [[Lilik Sudjio]]'s ''[[Tarmina]]''. Critics wrote that ''Lewat Djam Malam'' was easily the stronger of the two and suggested that [[Djamaluddin Malik]], ''Tarmina''{{'s}} producer, had influenced the jury's decision.{{efn|{{harvtxt|Said|1982|p=44}} writes that Malik had previously influenced a contest for favourite actress in 1954, ensuring that an actress from his company, Persari, was chosen.}}{{sfn|Said|1982|p=43}}

In 1958 Asmara became the head of the entertainment magazine ''Varia'', where the fellow director [[Raden Ariffien]] served as his deputy. Asmara held the position until his death; among other roles, he wrote a series of memoires on the history of theatre in the country.{{sfn|Encyclopedia of Jakarta, Andjar Asmara}}{{sfn|Pringgodigdo|Shadily|1973|pp=96–97}} He died on 20&nbsp;October 1961 in Cipanas, [[West Java]], during a trip to [[Bandung]]{{sfn|Filmindonesia.or.id, Abisin Abbas}} and was buried in Jakarta.{{sfn|Pringgodigdo|Shadily|1973|pp=96–97}}

== Legacy ==
Andjar's ''toneels'' were generally based on day-to-day experiences, rather than the tales of princes and ancient wars which were standard at the time.{{sfn|Filmindonesia.or.id, Dr Samsi}} Regarding Andjar's ''toneels'', the Indonesian literary critic [[Bakri Siregar]] writes that Andjar's stage plays, as well as those of fellow dramatist [[Njoo Cheong Seng]], revitalised the genre and made the works more realistic. However, he considered the conflict in these works to have been poorly developed.{{sfn|Siregar|1964|p=68}} Andjar believed that the Padangsche Opera's performances influenced other troupes in [[West Sumatra]] to adapt the ''toneel'' format, which later spread throughout the Indies.{{sfn|Cohen|2003|pp=215–216}}{{sfn|Cohen|2006|p=347}}

Matthew Isaac Cohen, a scholar of Indonesian performing arts, describes Andjar as "Indonesia's foremost theater critic during the colonial period", noting that he wrote extensively on the history of theatre in the Indies. Cohen also believes that Andjar also worked to justify the ''toneel'' style and distance it from the earlier ''stambul''.{{sfn|Cohen|2006|pp=347–348, 394}} Even after entering the film industry, Andjar considered the theatre more culturally significant than cinema.{{sfn|Biran|2009|p=25}} However, the Indonesian journalist Soebagijo I.N. writes that Andjar remains best known for his film work.{{sfn|I.N.|1981|p=212}}

Andjar was one of the first native Indonesian film directors, with Bachtiar Effendi, Soeska, and [[Inoe Perbatasari]].{{efn|The earliest [[List of film directors of the Dutch East Indies|film directors in the Dutch East Indies]], such as L. Heuveldorp, [[G. Kruger]], the [[Wong brothers]], and [[Lie Tek Swie]], were either European or [[Chinese Indonesians|ethnic Chinese]] {{harv|Biran|2009|pp=97, 102}}}}{{sfn|Said|1982|p=107}} Said writes that Andjar was forced to follow the whims of the [[Chinese Indonesians|ethnic Chinese]] film moguls, which resulted in the films' shift toward commercial orientation, rather than the prioritisation of artistic merit.{{sfn|Said|1982|p=30}} The film historian [[Misbach Yusa Biran]] writes that Andjar and his fellow journalists, upon joining JIF, brought with them new ideas that helped the company flourish until it closed after the arrival of the Japanese;{{sfn|Biran|2009|p=220}} the company and its subsidiaries released fifteen films in two years.{{sfn|Biran|2009|pp=383–385}}

== Filmography ==
[[File:Kartinah poster.jpg|thumb|upright|Poster for ''[[Kartinah]]'', Andjar's directorial debut]]
* ''[[Kartinah]]'' (1940)&nbsp;– as director, scriptwriter, and story writer
* ''[[Noesa Penida]]'' (1941)&nbsp;– as director and story writer
* ''[[Djaoeh Dimata]]'' (''Out of Sight''; 1948)&nbsp;– as director and story writer
* ''Anggrek Bulan'' (''Moon Orchid''; 1948)&nbsp;– as director
* ''[[Gadis Desa]]'' (''Maiden from the Village''; 1949)&nbsp;– as director and story writer
* ''Sedap Malam'' (''Sweetness of the Night''; 1950)&nbsp;– as story writer
* ''Pelarian dari Pagar Besi'' (''Escape from the Iron Fence''; 1951)&nbsp;– as story writer
* ''Musim Bunga di Selabintana'' (''Flowers in Selabintana''; 1951)&nbsp;– as story writer
* ''Dr Samsi'' (1952)&nbsp;– as story writer
* ''Noesa Penida (Pelangi Kasih Pandansari)'' (''Noesa Penida [Pandansari's Rainbow of Love]''; 1988)&nbsp;– as story writer (posthumous credit)

== Notes ==
{{notelist}}

== References ==

=== Footnotes ===
{{reflist|30em}}

=== Bibliography ===
{{refbegin|40em}}
* {{cite web
 |title=Abisin Abbas 
 |language=Indonesian 
 |url=http://filmindonesia.or.id/movie/name/nmp4b9bad3846f1c_andjar-asmara 
 |work=Filmindonesia.or.id 
 |publisher=Konfidan Foundation 
 |location=Jakarta 
 |accessdate=7 August 2012 
 |archiveurl=http://www.webcitation.org/69jM1e7P0?url=http%3A%2F%2Ffilmindonesia.or.id%2Fmovie%2Fname%2Fnmp4b9bad3846f1c_andjar-asmara 
 |archivedate=7 August 2012 
 |ref={{sfnRef|Filmindonesia.or.id, Abisin Abbas}} 
 |deadurl=no 
 |df=dmy 
}}
* {{cite web
 |title=Andjar Asmara 
 |language=Indonesian 
 |url=http://www.jakarta.go.id/jakv1/encyclopedia/detail/709 
 |work=Encyclopedia of Jakarta 
 |publisher=Jakarta City Government 
 |ref={{sfnRef|Encyclopedia of Jakarta, Andjar Asmara}} 
 |accessdate=7 August 2012 
 |archivedate=7 August 2012 
 |archiveurl=http://www.webcitation.org/69jK75pUT?url=http%3A%2F%2Fwww.jakarta.go.id%2Fjakv1%2Fencyclopedia%2Fdetail%2F709 
 |deadurl=no 
 |df=dmy 
}}
* {{cite web
 |url=http://www.tamanismailmarzuki.com/tokoh/andjar.html 
 |title=Andjar Asmara 
 |language=Indonesian 
 |publisher=[[Taman Ismail Marzuki]] 
 |accessdate=2 September 2012 
 |archivedate=2 September 2012 
 |archiveurl=http://www.webcitation.org/6AMwC5VtN?url=http%3A%2F%2Fwww.tamanismailmarzuki.com%2Ftokoh%2Fandjar.html 
 |ref={{sfnRef|TIM, Andjar Asmara}} 
 |deadurl=no 
 |df=dmy 
}}
* {{cite book
  | url = https://books.google.com/books?id=0M4Pl_VCExgC
  | title = Forgotten Wars: Freedom and Revolution in Southeast Asia
  | isbn = 978-0-674-02153-2
  | last1 = Bayly
  | first1 = Christopher Alan
  | last2 = Harper
  | first2 = Timothy Norman
  | year = 2007
  | ref = harv
  | location = Cambridge
  | publisher = Belknap Press
  }}
* {{cite book
  | title = [[Sejarah Film 1900–1950: Bikin Film di Jawa]]
  | trans_title=History of Film 1900–1950: Making Films in Java
  | language = Indonesian
  | last = Biran
  | first = Misbach Yusa
  | author-link=Misbach Yusa Biran
  | location = Jakarta
  | publisher = Komunitas Bamboo working with the Jakarta Art Council
  | year = 2009
  | isbn = 978-979-3731-58-2
  | ref = harv
  }}
* {{cite journal
 |last=Cohen 
 |first=Matthew Isaac 
 |url=http://digirep.rhul.ac.uk/file/df8cdd75-d82b-c43b-6c8b-be52abbbc6a4/1/CohenClouds.pdf 
 |title=Look at the Clouds: Migration and West Sumatran 'Popular' Theatre 
 |journal=New Theatre Quarterly 
 |issue=3 
 |doi=10.1017/s0266464x03000125 
 |volume=19 
 |pages=214–229 
 |date=August 2003 
 |editor1-last=Trussler 
 |editor1-first=Simon 
 |editor2-last=Barker 
 |editor2-first=Clive 
 |issn=0266-464X 
 |publisher=Cambridge University Press 
 |location=Cambridge 
 |ref=harv 
 |archivedate=2 September 2012
 |archiveurl=http://www.webcitation.org/6AOAFOi58?url=http%3A%2F%2Fdigirep.rhul.ac.uk%2Ffile%2Fdf8cdd75-d82b-c43b-6c8b-be52abbbc6a4%2F1%2FCohenClouds.pdf 
 |accessdate=3 September 2012 
 |deadurl=no 
 |df=dmy 
}}
* {{cite book
  | last = Cohen
  | first = Matthew Isaac
  | url = https://books.google.com/books?id=-fbw-06ovVIC
  | title = The Komedie Stamboel: Popular Theater in Colonial Indonesia, 1891–1903
  | year = 2006
  | publisher = Ohio University Press
  | location = Athens
  | isbn = 978-0-89680-246-9
  | ref = harv
  }}
* {{cite web
 |title=Dardanella 
 |publisher=Jakarta City Government 
 |work=Encyclopedia of Jakarta 
 |url=http://www.jakarta.go.id/english/encyclopedia/detail/447 
 |accessdate=4 September 2012 
 |archivedate=4 September 2012 
 |archiveurl=http://www.webcitation.org/6AQSv0XZ0?url=http%3A%2F%2Fwww.jakarta.go.id%2Fenglish%2Fencyclopedia%2Fdetail%2F447 
 |ref={{sfnRef|JCG, Dardanella}} 
 |deadurl=no 
 |df=dmy 
}}
* {{cite web
 |title=Dr Samsi 
 |language=Indonesian 
 |url=http://filmindonesia.or.id/movie/title/lf-d007-52-843386_dr-samsi 
 |work=Filmindonesia.or.id 
 |publisher=Konfidan Foundation 
 |location=Jakarta 
 |accessdate=7 August 2012 
 |archiveurl=http://www.webcitation.org/69jLFswWd?url=http%3A%2F%2Ffilmindonesia.or.id%2Fmovie%2Ftitle%2Flf-d007-52-843386_dr-samsi 
 |archivedate=7 August 2012 
 |ref={{sfnRef|Filmindonesia.or.id, Dr Samsi}} 
 |deadurl=no 
 |df=dmy 
}}
* {{cite book
  | last = Eneste
  | first = Pamusuk
  | year = 2001
  | language = Indonesian
  | title = Buku Pintar Sastra Indonesia
  | trans_title = Handbook of Indonesian Literature
  | publisher = Kompas
  | edition = 3rd
  | location = Jakarta
  | isbn = 978-979-9251-78-7
  | ref = harv
  }}
* {{cite web
 |title=Filmografi 
 |trans_title=Filmography 
 |language=Indonesian 
 |url=http://filmindonesia.or.id/movie/name/nmp4b9bad3846f1c_andjar-asmara/filmography 
 |work=Filmindonesia.or.id 
 |publisher=Konfidan Foundation 
 |location=Jakarta 
 |accessdate=7 August 2012 
 |archiveurl=http://www.webcitation.org/69jLSwfx6?url=http%3A%2F%2Ffilmindonesia.or.id%2Fmovie%2Fname%2Fnmp4b9bad3846f1c_andjar-asmara%2Ffilmography 
 |archivedate=7 August 2012 
 |ref={{sfnRef|Filmindonesia.or.id, Filmografi}} 
 |deadurl=no 
 |df=dmy 
}}
* {{cite book
  | last = I.N.
  | first = Soebagijo
  | year = 1981
  | language = Indonesian
  | title = Jagat Wartawan Indonesia
  | trans_title = Universe of Indonesian Journalists
  | publisher = Gunung Agung
  | location = Jakarta
  | oclc = 7654542
  | ref = harv
  }}
* {{cite book
  | url = https://books.google.com/books?id=l4lSeZAZ2DMC
  | title = Menjadi Bintang
  | trans_title=Becoming a Star
  | language = Indonesian
  | last = Karsito
  | first = Eddie
  | year = 2008
  | publisher = Ufuk Press
  | location = Jakarta
  | oclc = 318673348
  | ref = harv
  }}
*{{cite book
 |title=Ekstrinsikalitas Sastra Indonesia
 |trans_title=Extrinsic Events in Indonesian Literature
 |language=Indonesian
 |last=Mahayana
 |first=Maman S.
 |publisher=RajaGrafindo Persada
 |location=Jakarta
 |ref=harv
 |isbn=978-979-769-115-8
 |year=2007
}}
* {{cite book
  | last = Mahayana
  | first = Maman S.
  | last2 = Sofyan
  | first2 = Oyon
  | last3 = Dian
  | first3 = Achmad
  | year = 1995
  | title = Ringkasan dan Ulasan Novel Indonesia Modern
  | trans_title = Summaries and Commentary on Modern Indonesian Novels
  | location = Jakarta
  | publisher = Grasindo
  | language = Indonesian
  | isbn = 978-979-553-123-4
  | ref = harv
  }}
* {{cite book
  | title = A History of Modern Indonesia since c. 1200
  | last = Ricklefs
  | first = M. C.
  | authorlink=M. C. Ricklefs
  | year = 2001
  | edition=3rd
  | publisher = Palgrave
  | location = Hampshire
  | ref = harv
  | isbn = 978-0-333-24380-0
  | url = https://books.google.com/books?id=0GrWCmZoEBMC
  }}
*{{cite encyclopedia
 | last1 =Pringgodigdo
 | first1 =Abdul Gaffar
 | author1-link =Abdoel Gaffar Pringgodigdo
 |last2=Shadily
 |first2=Hassan
 | encyclopedia =Ensiklopedi Umum
 | title =Andjar Asmara
 | url =https://books.google.com/books?id=BJrFsQ0SwzgC
 | language =Indonesian
 | year =1973
 | publisher =Kanisius
 | oclc =4761530
 | pages =96–97
 | ref =harv
}}
* {{cite book
  | title = Profil Dunia Film Indonesia
  | trans_title=Profile of Indonesian Cinema
  | language = Indonesian
  | last = Said
  | first = Salim
  | publisher = Grafiti Pers
  | location = Jakarta
  | year = 1982
  | oclc = 9507803
  | ref = harv
  }}
* {{cite book
  | title = Media, Culture and Politics in Indonesia
  | last1 = Sen
  | first1 = Krishna
  | last2 = Hill
  | first2 = David T.
  | year = 2000
  | publisher = Oxford University Press
  | location = Melbourne
  | isbn = 978-0-19-553703-1
  | url = https://books.google.com/books?id=xMhWm38KQcsC
  | ref = harv
  }}
* {{cite book
  | last = Siregar
  | first = Bakri
  | year = 1964
  | language = Indonesian
  | title = Sedjarah Sastera Indonesia
  | trans_title = History of Indonesian Literature
  | volume = 1
  | series =
  | publisher = Akademi Sastera dan Bahasa "Multatuli"
  | location = Jakarta
  | oclc = 63841626
  | ref = harv
  }}
* {{cite journal
 |title=In the Boys' Club: A Historical Perspective on the Roles of Women in the Indonesian Cinema 1926&nbsp;– May 1998 
 |pages=103–111 
 |date=July 2009 
 |url=http://puslit2.petra.ac.id/ejournal/index.php/iko/article/viewFile/18314/18162 
 |archiveurl=http://www.webcitation.org/69zwHVXHA?url=http%3A%2F%2Fpuslit2.petra.ac.id%2Fejournal%2Findex.php%2Fiko%2Farticle%2FviewFile%2F18314%2F18162 
 |archivedate=18 August 2012 
 |ref=harv 
 |last=Swestin 
 |issn=1978-385X 
 |first=Grace 
 |work=Scriptura 
 |issue=2 
 |location=Surabaya 
 |publisher=Petra Christian University 
 |volume=3 
 |deadurl=no 
 |df=dmy 
}}
{{refend}}

== External links ==
* {{IMDb name|4339453|Andjar Asmara}}
{{Andjar Asmara}}

{{Use British English|date=August 2012}}
{{Use dmy dates|date=September 2012}}

{{featured article}}

{{DEFAULTSORT:Asmara, Andjar}}
[[Category:1902 births]]
[[Category:1961 deaths]]
[[Category:Film directors of the Dutch East Indies]]
[[Category:Indonesian dramatists and playwrights]]
[[Category:Indonesian film directors]]
[[Category:Indonesian journalists]]
[[Category:Indonesian screenwriters]]
[[Category:Minangkabau people]]
[[Category:People from West Sumatra]]
[[Category:Screenwriters of the Dutch East Indies]]
[[Category:20th-century dramatists and playwrights]]