{{good article}}
{{EngvarB|date=July 2014}}
{{Use dmy dates|date=July 2014}}
{{Infobox University Boat Race
| name= 120th Boat Race
| winner = Oxford
| margin = 5 and 1/2 lengths
| winning_time= 17 minutes 35 seconds
| date= 6 April 1974
| umpire = [[Ran Laurie|W. G. R. M. Laurie]]<br>(Cambridge)
| prevseason= [[The Boat Race 1973|1973]]
| nextseason= [[The Boat Race 1975|1975]]
| overall =67&ndash;52
| reserve_winner =Goldie
| women_winner =Cambridge
}}
The 120th [[The Boat Race|Boat Race]] took place on 6 April 1974. Held annually, the Boat Race is a [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]].  It was won by Oxford who passed the finishing post five-and-a-half lengths ahead of Cambridge, in a winning time of 17 minutes 35 seconds, the fastest in the history of the race, beating the existing record set in the [[The Boat Race 1948|1948 race]].  It was umpired by [[Ran Laurie]].

In the reserve race, [[Goldie (Cambridge University Boat Club)|Goldie]] beat Isis, and in the [[Women's Boat Race]], Cambridge were victorious.

==Background==
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 8 April 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  First held in 1829, the race takes place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 3 June 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref>  The rivalry is a major point of honour between the two universities and followed throughout the United Kingdom and broadcast worldwide.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford&ndash;Cambridge Boat Race crew|date=6 April 2014|publisher=CBC News|url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=7 April 2014}}</ref><ref>{{Cite web | url = http://theboatraces.org/tv-and-radio | title = TV and radio | publisher = The Boat Race Company Limited | accessdate = 5 July 2014}}</ref>  Cambridge went into the race as reigning champions, having beaten Oxford by a thirteen lengths in the [[The Boat Race 1973|previous year's race]].  Cambridge held the overall lead, with 67 victories to Oxford's 51 (excluding the "dead heat" of 1877).<ref>{{Cite web | url= http://theboatraces.org/classic-moments-the-1877-dead-heat | publisher = The Boat Race Company Limited | title = Classic moments – the 1877 dead heat | accessdate = 12 October 2014}}</ref><ref name=results>{{Cite web | url = http://theboatraces.org/results | title =Men &ndash; Results | publisher = The Boat Race Company Limited | accessdate = 19 July 2014}}</ref>

The first [[Women's Boat Race]] took place in 1927, but did not become an annual fixture until the 1960s. Up until 2014, the contest was conducted as part of the [[Henley Boat Races]], but as of the [[The Boat Races 2015|2015 race]], it is held on the River Thames, on the same day as the men's main and reserve races.<ref>{{Cite web | url = http://theboatraces.org/women/history | archiveurl = https://web.archive.org/web/20141006112628/http://theboatrace.org/women/history| archivedate= 6 October 2014| title = A brief history of the Women's Boat Race | publisher = The Boat Race Company Limited| accessdate = 5 July 2014}}</ref>  The reserve race, contested between Oxford's Isis boat and Cambridge's Goldie boat has been held since 1965.  It usually takes place on the Tideway, prior to the main Boat Race.<ref name=results/>

During preparation for the race, both crews were defeated on the Thames by the Lubrication Laboratory boat from [[Imperial College London]].<ref name=liven>{{Cite news | title = Two indifferent crews could liven up Boat race series | first = Jim | last = Railton | date = 3 April 1974 | page = 7 | work = [[The Times]] | issue = 59055}}</ref>  Jim Railton, writing in ''[[The Times]]'' was not impressed: "It has already been well established that there are no top class crews in this&nbsp;... Boat Race.  Each day on the Tideway in the final days of training have simply produced more negatives".<ref name=liven/>  Cambridge also lost twice in short races against their reserve crew Goldie, whom Oxford had defeated at the [[Reading University Head of the River Race]] by eight seconds.<ref name=tails>{{Cite news | title = Oxford tails high as Cambridge lose twice to reserve crew | first = Jim | last = Railton | date = 4 April 1974| page = 19 | issue = 59056 | work = [[The Times]]}}</ref>  In appalling conditions, Cambridge won an interrupted race against a [[Barclays Bank]] crew.<ref>{{Cite news | title = Conditions present crews with problems | first = Jim | last = Railton | work = [[The Times]] | date = 5 April 1974 | page = 13 | issue = 59057}}</ref>  Meanwhile, Oxford defeated a "motley eight" from [[Tideway Scullers School]] by four lengths along the full course.<ref name=tails/>  Oxford's finishing coach was [[Daniel Topolski]] while Cambridge were guided by British Olympic rower [[David Jennens]],<ref name=key/> and the race was umpired by [[Ran Laurie]], who had rowed for Cambridge in the [[The Boat Race 1934|1934]], [[The Boat Race 1935|1935]] and [[The Boat Race 1936|1936 races]].<ref>{{Cite news| title = Equality of crews to restore excitement | work = [[The Observer]] | first = John | last = Rodda| date= 6 April 1974 | page =17}}</ref><ref>Dodd pp. 328&ndash;29</ref>

==Crews==
The Cambridge crew was marginally heavier than their opponents, weighing an average of 13&nbsp;[[Stone (unit)|st]] 5.25&nbsp;[[Pound (mass)|lb]] (84.7&nbsp;kg), {{convert|0.25|lb|kg|1}} per rower more than Oxford.<ref name=key/>  Oxford saw four [[Blue (university sport)|Blues]] return to their crew, Nick Tee, Sam Nevin, Dennis Payne and American Olympian David Sawyier, while Cambridge welcomed back two &ndash; Ben Duncan and Howard Jacobs.<ref name=tails/>
[[File:David Rendel - Newbury declaration.jpg|right|thumb|upright|[[David Rendel]] rowed at number three for Oxford.]]
{| class="wikitable"
|-
! rowspan="2" scope="col| Seat
! colspan="3" scope="col| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
! colspan="3" scope="col| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]]|| N. D. C. Tee || [[Balliol College, Oxford|Balliol]] || 12&nbsp;st 1&nbsp;lb || R. P. B. Duncan (P)|| [[St Catharine's College, Cambridge|St Catharine's]] || 13&nbsp;st 8&nbsp;lb
|-
| 2 || G. S. Innes || [[Oriel College, Oxford|Oriel]] || 13&nbsp;st 2&nbsp;lb || H. R. Jacobs || [[Pembroke College, Cambridge|Pembroke]] || 13&nbsp;st 6&nbsp;lb
|-
| 3 || [[David Rendel|D. Rendel]] || [[St Cross College, Oxford|St Cross]] || 13&nbsp;st 10&nbsp;lb ||D. J. Walker || [[Clare College, Cambridge|Clare]] || 13&nbsp;st 9&nbsp;lb
|-
| 4 || S. D. Nevin || [[Christ Church, Oxford|Christ Church]] || 13&nbsp;st 13&nbsp;lb || D .P. Sprague || [[Emmanuel College, Cambridge|Emmanuel]] || 13&nbsp;st 2&nbsp;lb
|-
| 5 || P. G. P. Stoddart || [[University College, Oxford|University]] || 13&nbsp;st 6&nbsp;lb || J. H. Smith || [[Gonville and Caius College, Cambridge|Gonville & Caius]] || 14&nbsp;st 12&nbsp;lb
|-
| 6 || P. J. Marsden || [[Lincoln College, Oxford|Lincoln]] || 13&nbsp;st 6&nbsp;lb || [[Henry Clay (rower)|J. H. Clay]] || [[Pembroke College, Cambridge|Pembroke]] || 13&nbsp;st 4&nbsp;lb
|-
| 7 || D. R. Payne || [[Balliol College, Oxford|Balliol]] || 13&nbsp;st 5&nbsp;lb || T. F. Yuncken|| [[Pembroke College, Cambridge|Pembroke]] || 12&nbsp;st 12&nbsp;lb
|-
| [[Stroke (rowing)|Stroke]] || D. R. Sawyier (P)|| [[Christ Church, Oxford|Christ Church]] || 14&nbsp;st 2&nbsp;lb || N. C. A. Bradley || [[Pembroke College, Cambridge|Pembroke]] || 12&nbsp;st 3&nbsp;lb
|-
| [[Coxswain (rowing)|Cox]] || G. E. Morris || [[Oriel College, Oxford|Oriel]] || 8&nbsp;st 12&nbsp;lb || H. J. H. Wheare || [[Jesus College, Cambridge|Jesus]] || 8&nbsp;st 11&nbsp;lb
|-
!colspan="7"| Sources:<ref name=key>{{Cite news| title = Battle of strokes is key to Boat Race outcome | first = Jim | last = Railton | date = 6 April 1974 | work = [[The Times]] | page = 17 | issue = 59058}}</ref><ref>Burnell, pp. 51&ndash;52</ref><br>(P) &ndash; Boat club president<ref>{{Cite news | title = Monday blues on the Tideway | work = [[The Times]] | date = 2 April 1974 | page = 9 | issue = 59054| first = Jim | last = Railton}}</ref>
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]] along which the Boat Race is contested]]
Cambridge won the toss and elected to start from the Middlesex station.<ref name=lifts>{{Cite news | title = Aggression of Sawyier lifts Oxford from gloom | work = [[The Times]] | first = Jim | last = Railton | date = 8 April 1974 | page = 8 | issue = 59059}}</ref>  Oxford took an early lead with their stroke Sawyier making a good start.  Despite this, by the Mile Post, Oxford were just one-third of a length clear, and while Cambridge's [[Glossary of rowing terms#The stroke|stroke rate]] was lower than their Dark Blue opposition, they remained in contention.  By [[Hammersmith Bridge]], Oxford held a two-length lead, and despite favourable wind and ride, Cambridge failed to respond.  At Chiswick Steps, the lead had extended to four lengths and by [[Barnes Railway Bridge|Barnes Bridge]] it was five.  Oxford passed the finishing post in a record-breaking time of 17 minutes 35 seconds, breaking the existing record from the [[The Boat Race 1948|1948 race]], five-and-a-half lengths clear of the Light Blues.<ref name=lifts/>  Railton commented: "Tideway records reflect conditions more than comparisons with previous Boat Race crews" but noted "considering the slow conditions ... Oxford's performance must have been particularly inspired."<ref name=lifts/>  The winning time was two seconds faster than the best time over the course, set by Oxford in practice in 1965, and 15 seconds quicker than the race record, set in the [[The Boat Race 1948|1948 race]] by Cambridge.<ref>{{Cite news | title = Oxford had it sewn up at Hammersmith| first = Michael | last= Donne | accessdate= 27 November 2014 | work = [[Financial Times]] | date = 8 April 1974 | issue = 26333| page = 3}}</ref>

In the reserve race, Cambridge's Goldie beat Oxford's Isis by five lengths, their seventh consecutive victory.<ref name="results"/> In the [[Women's Boat Race 1974|28th running]] of the [[Women's Boat Race]], Cambridge triumphed, their eleventh consecutive victory.<ref name=results/>

==References==
'''Bibliography'''
*{{Cite book | title = The Oxford & Cambridge Boat Race | first = Christopher| last = Dodd | isbn= 0-09-151340-5 | publisher =Stanley Paul |year= 1983}}
*{{Cite book | title = One Hundred and Fifty Years of the Oxford and Cambridge Boat Race | first = Richard | last = Burnell | authorlink = Dickie Burnell | year=1979| isbn= 0-9500638-7-8 | publisher = Precision Press}}

'''Notes'''
{{reflist|30em}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1974}}
[[Category:The Boat Race]]
[[Category:1974 in English sport]]
[[Category:1974 in rowing]]
[[Category:April 1974 sports events]]