{{Infobox venue
| stadium_name        = Harry Jerome Sports Centre
| nickname            = The Bubble
| fullname            = 
| former_names        = 
| logo_image   	      = BVC_Logo.png
| logo_caption 	      = 
| image               = 
| caption             =
| location     	      = 7564 Barnet Highway, [[Burnaby, British Columbia|Burnaby]], [[British Columbia]]
| coordinates  	      = {{Coord|49.28905|N|122.94051|W|region:CA-BC_type:landmark|display=inline,title}}
| broke_ground        = 
| built        	      = 1991-1997
| opened       	      = November 1997 <!--{{Start date|1997|11|DD|df=y}}-->
| renovated           = 
| expanded            = 
| closed              = 
| demolished          = 
| owner        	      = [[Burnaby, British Columbia|City of Burnaby]]
| operator            = 
| surface             = 
| scoreboard          = 
| cost                = $1,500,000 [[Canadian dollar|CAD]]
| architect           = 
| project_manager     = 
| structural engineer = 
| services engineer   = 
| general_contractor  = 
| main_contractors    = 
| capacity            = 
| dimensions   	      = 200m oval track in a 53,000 sq. f. facility
| tenants      	      = [[Burnaby Velodrome Club]]<br>[[Volleyball British Columbia]]
| website             = {{URL|burnabyvelodromeclub.ca}}
}}
One of only three indoor bicycle racing tracks in Canada,<ref>{{cite news |last=Ewing |first=Lori |date=5 January 2015 |title=New velodrome in Milton is star of Canadian track cycling championships |url=http://www.winnipegfreepress.com/sports/new-velodrome-in-milton-is-the-star-of-canadian-track-cycling-championships-287568671.html |work=Winnipeg Free Press |agency=The Canadian Press |accessdate=29 March 2015 |quote=There is a 150-metre covered track in London, Ont., and one in Burnaby, B.C., that measures 200 metres. But neither is near the calibre of the oval in Milton.}}</ref> the '''Burnaby Velodrome''' is located in Burnaby, British Columbia.<ref name=tourism>{{cite web|title=Harry Jerome Sports Centre|url=http://tourismburnaby.com/sport-facilities/harry-jerome-sports-centre|website=Tourism Burnaby|accessdate=11 March 2015}}</ref> It is operated by the non-profit Burnaby Velodrome Club (BVC),<ref>{{cite news |last=Gallagher |first=Margaret |date=2 May 2014 |title=It's like riding on a roller coaster you're in control of |url=http://www.cbc.ca/news/canada/british-columbia/it-s-like-riding-on-a-roller-coaster-you-re-in-control-of-1.2629500 |work=CBC News}}</ref> and is an affiliated member of Cycling British Columbia.<ref>{{cite web|title=Affiliates|url=http://cyclingbc.net/clubs/affiliates/|website=Cycling BC|accessdate=29 March 2015}}</ref> The track is located inside the Harry Jerome Sports Centre, which is also used by Volleyball BC for game play and administration purposes.<ref>{{cite web|title=Volleyball BC|url=http://www.volleyballbc.ca/|website=Volleyball BC|accessdate=11 March 2015}}</ref>

==History==
Built between 1991 and 1997, the Burnaby Velodrome replaced the China Creek Velodrome, which was in operation from 1954 to 1980. The China Creek Velodrome was built for the [[Commonwealth Games|Empire Games]] in 1954 and dismantled because of the construction of [[Vancouver Community College]]'s Broadway campus.<ref name=tourism/>
The track surface and support structure were designed in part by the CTA Design Group,<ref>{{cite web |url=https://burnaby.civicweb.net/Documents/DocumentList.aspx?Id=1388#JD_2001-27-10 |title=Item 10 - Harry Jerome Sports Centre - Response to Cycling B.C. and Burnaby Velodrome Club Delegation |date=31 October 2001 |website=Burnaby City Council |accessdate=28 March 2015}}</ref> and cost approximately $1.5 million to build.<ref>{{cite journal |url=http://www.bicyclepaper.com/issues/49.pdf |title=March 1992. Vancouver prepares for velodrome opening this fall (Burnaby, B.C.) (1997) |format=PDF |journal=Bicycle Paper |issue=49 |accessdate=28 March 2015 |quote=will cost $1.5 million to complete}}</ref> The velodrome opened on November 17, 1997.<ref>{{cite web |url=https://burnaby.civicweb.net/Documents/DocumentList.aspx?Id=236 |title=1997 November 17 |website=Burnaby City Council |accessdate=28 March 2015}}</ref>

In October 2000, an engineering report was produced, which identified concerns about the roof structure and condition of the cycle track surface. The track was closed in May 2001, citing safety concerns by the risk manager for the City of Burnaby.<ref>{{cite web |url=https://burnaby.civicweb.net/Documents/DocumentList.aspx?Id=1360 |title=Item 09 - Repairs to the Cycling Track at Harry Jerome Sports Centre |website=City of Burnaby Council |accessdate=29 March 2015}}</ref> In October 2001, the director of the Parks, Recreation and Cultural Services Department informed the Burnaby City Council that the BVC wanted to repair and upgrade the track.<ref>{{cite web |url=https://burnaby.civicweb.net/Documents/DocumentList.aspx?Id=102 |title=2001 October 15 |website=Burnaby City Council |accessdate=29 March 2015}}</ref> Cycling BC<ref>{{cite web |url=https://burnaby.civicweb.net/Documents/DocumentList.aspx?Id=104 |title=2001 November 05 |website=Burnaby City Council |accessdate=30 March 2015}}</ref> and the president of the Marymoor Velodrome<ref>{{cite web |title=2001 November 26 |url=https://burnaby.civicweb.net/Documents/DocumentList.aspx?Id=106 |website=Burnaby City Council |accessdate=30 March 2015}}</ref> supported the repairs and re-opening.

In April 2002, the BVC's plan was chosen from several competing proposals, including the removal of the track surface.<ref>{{cite web |url=https://burnaby.civicweb.net/Documents/DocumentList.aspx?Id=76 |title=2002 April 08 |website=Burnaby City Council |accessdate=30 March 2015}}</ref><ref name=introuble>{{cite web |url=http://www.canadiancyclist.com/dailynews.php?id=4681 |title=Burnaby Track in Trouble |date=5 April 2002 |website=Canadian Cyclist |accessdate=30 March 2015}}</ref> Professional cyclist [[Alex Stieda]] and members of the public wrote in support of the BVC's proposal.<ref>{{cite web |url=https://burnaby.civicweb.net/Documents/DocumentList.aspx?Id=77 |title=2002 April 15 |website=Burnaby City Council |accessdate=30 March 2015}}</ref><ref>{{cite web |url=https://burnaby.civicweb.net/Documents/DocumentList.aspx?Id=4364 |title=2002 May 06 |website=Burnaby City Council |accessdate=30 March 2015}}</ref> In November 2002, the club re-opened and held a successful open house.<ref>{{cite web |url=https://burnaby.civicweb.net/Documents/DocumentList.aspx?Id=4804 |title=2002 November 25 |website=Burnaby City Council |accessdate=29 March 2015}}</ref>

==Facilities==
The velodrome is 6 meters wide and 200 meters long, with bankings of 47 degrees in the corners and 15 degrees in the straightaways. The track surface is a combination of strip planks and plywood sheeting.<ref>{{cite web |last=Pospisil |first=Rudy |date=26 November 2013 |title=Velodrome Cycling |url=http://blogs.theprovince.com/2013/11/26/what-is-a-velodrome/ |work=The Province |accessdate=19 March 2015}}</ref> Riders must maintain a speed of at least  {{convert|30|kph|mph}} in order to avoid falling off the track.<ref>{{cite news |last1=Walker |first1=Ian |date=10 May 2008 |title=Steep Learning Curve |url=http://www.canada.com/story_print.html?id=77336a1a-8c54-4483-b354-75882a75b1bb&sponsor= |newspaper=The Vancouver Sun |publisher=Postmedia Network Inc |accessdate=30 March 2015}}</ref>

High-level cyclists who have trained or raced at the facility include [[Brian Walton (cyclist)|Brian Walton]], [[Tanya Dubnicoff]], [[Lori-Ann Muenzer]],<ref name=introuble/> and Mandy Poitras.<ref>{{cite journal |title=Burnaby Track Closure Affects National Track Team |url=http://pedalmag.com/burnaby-track-closure-affects-national-track-team/ |journal=Pedal Magazine |accessdate=30 March 2015}}</ref>

In 2000, the Burnaby Mountain Conservation Area was formed, which includes the land under the Harry Jerome Sports Centre.<ref>{{cite web |url=https://burnaby.civicweb.net/Documents/DocumentList.aspx?Id=1172 |title=Item 1 - Burnaby Mountain Conservation Area Management Plan |website=Burnaby City Council |accessdate=28 March 2015}}</ref> The Velodrome Trail (pedestrians only) starts just behind the HJSC.<ref>{{cite web |url=http://www.burnaby.ca/Things-To-Do/Explore-Outdoors/Parks/Burnaby-Mountain-Conservation-Area/Velodrome-Trail.html |title=Velodrome Trail |website=City of Burnaby |accessdate=28 March 2015}}</ref> In 2006, a trail was cut between the Harry Jerome Sports Centre and the former gun club site to facilitate movement of emergency personnel attending to operations on [[Burnaby Mountain]].<ref>{{cite web |url=https://burnaby.civicweb.net/Documents/DocumentList.aspx?Id=7623 |title=Trail Safety on Burnaby Mountain |website=Burnaby City Council |accessdate=28 March 2015}}</ref>

==Programs==
In 2010, the BVC started the Aboriginal Youth Cycling program under the direction of program coordinator Kelyn Akuna.<ref>{{cite news |last=Dobie |first=Cayley |date=14 July 2014 |title=A Passion for Pedaling |url=http://www.burnabynow.com/community/a-passion-for-pedaling-1.1206075 |newspaper=BurnabyNOW |accessdate=28 March 2015}}</ref> This program, the first of its kind, is a way for aboriginal youth to learn about track cycling.<ref>{{cite web |last=Bartel |first=Mario |date=10 January 2012 |title=Cycling to Change the World |url=http://www.newwestnewsleader.com/news/136979498.html |website=New Westminster News Leader |accessdate=28 March 2015}}</ref>

In 2011, BVC President, Scott Laliberte, made a presentation to Burnaby City Council, stating that membership had increased for each of the past two years, and that a variety of training programs were in place.<ref>{{cite web |url=https://burnaby.civicweb.net/Documents/DocumentList.aspx?Id=11250 |title=2011 April 04|website=Burnaby City Council |accessdate=28 March 2015}}</ref>

The velodrome plays host to a variety of races every year, including the Feature Friday Night Race Series,<ref>{{cite web  |url=http://www.canadiancyclist.com/dailynews.php?id=28992&title=friday-night-race-series-finale-at-burnaby-velodrome |title=Feature Friday Night Race Series |date=11 February 2015 |website=Canadian Cyclist |accessdate=19 March 2015}}</ref> the Burnaby 4 Day,<ref>{{cite news |date=24 December 2014 |title=Burnaby 4-Day Bike Race |url=http://www.burnabynow.com/sports/burnaby-velodrome-hosts-4day-races-1.1696061 |newspaper=BurnabyNOW |accessdate=19 March 2015}}</ref> and club races throughout the season.<ref>{{cite web |url=http://www.theholeshot.ca/friday-night-racing-at-the-burnaby-velodrome/ |title=Friday Night Racing |website=The Holeshot |date=15 February 2015 |accessdate=19 March 2015}}</ref> In the past, they have hosted the Junior and U17 National Track Championships,<ref>{{cite web|title=Junior and U17 Track Championships|url=http://cyclingbc.net/news/2014/11/26/young-cyclists-emerge-from-canadian-track-championships-at-burnaby-velodrome/|website=Cycling British Columbia|accessdate=19 March 2015}}</ref> as well as the BC Provincial Track Championships.<ref>{{cite web|title=BC Provincial Track Championships|url=http://cyclingbc.net/news/2014/05/13/provincial-championships-2014//|website=Cycling British Columbia|accessdate=19 March 2015}}</ref><ref>{{cite news |last=Burritt |first=Dan |date=27 December 2010 |title=World champs head to Burnaby Velodrome this week |url=http://www.news1130.com/2010/12/27/world-champs-head-to-burnaby-velodrome-this-week/ |work=News 1130}}</ref> Visitors will need a UCI license to ride or race during their visit.

The velodrome also hosts a variety of youth camps, training sessions, and race clinics year-round.<ref>{{cite web |title=Junior Skill Development Camp |url=http://www.cyclingcanada.ca/wp-content/uploads/2012/05/Burnaby-Junior-Skill-Development-Camp-Nov.pdf |format=PDF |website=Cycling Canada |accessdate=19 March 2015}}</ref><ref>{{cite web |url=http://bikeracing.ca/burnaby-velodrome-youth-training-camps/ |title=Burnaby Velodrome Youth Training Camps |work=Mid Island Velo Association}}</ref> There are monthly sprint or time trial events as well.<ref>{{cite web|title=Racing|url=http://cyclingbc.net/news/2013/04/16/burnaby-velodrome-bi-monthly-time-trial-series-coming-may-17th/|website=Cycling British Columbia|accessdate=19 March 2015}}</ref>

In 2014, the club's redesigned logo by Jonathan Wood won a Communication Arts Typography award.<ref>{{cite web |url=http://www.commarts.com/annuals/2014-Typography/winners |title=2014 Typography Annual Winners |website=Communication Arts |accessdate=28 March 2015}}</ref>

==Major competitions hosted==
{|class="sortable wikitable" style="font-size: 95%;"
! Year
! Date
! Event
! Level
|-
| 2014
| December 27–30
| Burnaby 4-Day Bike Race
| Regional
|-
| 2014
| September 12–14
| BC Provincial Track Championship
| Regional
|-
| 2014
| November 21–24
| National Junior and U17 Track Championship
| National
|-
| 2013
| December 27–30
| Burnaby 4-Day Bike Race
| Regional
|-
| 2012
| December 27–30
| Superior Glass 4-Day
| Regional
|-
| 2012
| November 9–11
| Superior Glass Track Classic
| Regional
|-
| 2010
| December 27–30
| Saputo Burnaby 4
| Regional
|}

{{Portalbar|Vancouver|Cycling|Sport in Canada}}

==References==
{{reflist|30em}}

[[Category:Velodromes in Canada]]
[[Category:Sports venues in British Columbia]]
[[Category:Sport in Burnaby]]