{{Use mdy dates|date=January 2012}}
{{good article}}
{{Infobox ice hockey player
| image = Drayson Bowman 2013-2.jpg
| caption =
| image_size = 230px
| team = [[Düsseldorfer EG]]
| league = [[Deutsche Eishockey Liga|DEL]]
| prospect_team =
| prospect_league = 
| former_teams = [[Carolina Hurricanes]] <br> [[Montreal Canadiens]]
| position = [[Centre (ice hockey)|Center]]
| shoots = Left
| height_ft = 6
| height_in = 0
| weight_lb = 190
| birth_date = {{birth date and age|1989|3|8}}
| birth_place = [[Grand Rapids, Michigan|Grand Rapids]], [[Michigan|MI]], [[United States|USA]]
| ntl_team = USA
| draft = 72nd overall
| draft_year = 2007
| draft_team = [[Carolina Hurricanes]]
| career_start = 2009
}}
'''Drayson Jack Bowman''' (born March 8, 1989) is an American professional [[ice hockey]] [[center (ice hockey)|center]]. He is currently playing with [[Düsseldorfer EG]] of the [[Deutsche Eishockey Liga]] (DEL), after previously playing for the [[Montreal Canadiens]] and [[Carolina Hurricanes]] of the [[National Hockey League]] (NHL). Bowman was selected 72nd overall by the Hurricanes in the [[2007 NHL Entry Draft]].

Bowman spent four years at the [[major junior]] level with the [[Spokane Chiefs]] of the [[Western Hockey League]] (WHL). He won a [[Memorial Cup]] with the Chiefs in [[2008 Memorial Cup|2008]] and was named a WHL West Second Team All-Star in [[2008–09 WHL season|2009]]. He turned professional in [[2009–10 NHL season|2009–10]] and has spent the majority of his tenure with the Hurricanes in the team's [[farm system]] with the [[Albany River Rats]] and [[Charlotte Checkers (2010–)|Charlotte Checkers]] of the [[American Hockey League]] (AHL). Internationally, Bowman has competed for the [[USA Hockey|United States]] at the [[2009 IIHF World U20 Championships]].

==Early life==
Bowman was born in [[Grand Rapids, Michigan|Grand Rapids]], [[Michigan]], and was raised in [[Littleton, Colorado|Littleton]], [[Colorado]], after his family moved in the early 1990s.<ref name=move/> His father, Mark Bowman, owns a [[financial consultant|financial consulting]] company in Colorado.<ref name=move/> His younger brother, Collin, is also a hockey player and went on to also compete in the [[Western Hockey League]] with the [[Kelowna Rockets]], [[Moose Jaw Warriors]] and [[Calgary Hitmen]].<ref name=move/><ref>{{cite web|url=http://www.eliteprospects.com/player.php?player=30441|publisher=Eliteprospects.com|title=Collin Bowman|accessdate=2011-12-19}}</ref> While in Littleton, Bowman attended [[Deer Creek Middle School]].<ref name=move/> As a [[Colorado Avalanche]] fan, he has listed [[Joe Sakic]] as a player he looked up to.<ref name=move/> In 2003, he and his family moved once more to [[Vancouver]], [[British Columbia]] to better his opportunities in hockey.<ref name=move/> He attended [[Vancouver Christian School]] while playing at the [[minor hockey|bantam level]] for the North Vancouver Winter Hawks.<ref name=move>{{cite web|title=Move made by Bowman pays off |url=http://www.denverpost.com/avalanche/ci_9408797 |publisher=Dever Post |accessdate=2008-07-30 |date=2008-05-28 |last=Frei |first=Terry |archiveurl=http://www.webcitation.org/5uxyS4cnO?url=http://www.denverpost.com/avalanche/ci_9408797 |archivedate=December 14, 2010 |deadurl=yes |df=mdy }}</ref>

==Playing career==

===Junior===
Bowman was selected eighth overall by the [[Spokane Chiefs]] in the [[2004 WHL Bantam Draft]].<ref name=call>{{cite news|title=Bowman Waits For The Call|url=http://www.oursportscentral.com/services/releases/?id=3491510|accessdate=2010-04-08|date=2007-06-20|publisher=OurSports Central|archiveurl=https://web.archive.org/web/20110629170017/http://www.oursportscentral.com/services/releases/?id=3491510|dead-url=no|archivedate=2011-06-29}}</ref> He debuted in four games with the Chiefs in [[2004–05 WHL season|2004–05]], a season he spent primarily at the Junior B level{{#tag:ref|Junior B represents the third-highest level of under-20 competition in Canada, after major junior and Junior A.|group=notes}} with the [[Kimberley Dynamiters (KIJHL)|Kimberley Dynamiters]] of the [[Kootenay International Junior Hockey League]] (KIJHL). He recorded 29 goals and 59 points over 47 games with the Dynamiters to be named the Eddie Mountain Division's rookie of the year.<ref name=kijhl>{{cite web|title=Year End Award Winners|url=http://www.kijhl.ca/leagues/custom_page.cfm?clientID=2223&leagueID=5221&pageid=7066|accessdate=2010-04-07|publisher=[[Kootenay International Junior Hockey League]]}}</ref> Bowman joined the Chiefs full-time in [[2005–06 WHL season|2005–06]] and notched 17 goals and 34 points over 72 games (17th in WHL rookie scoring)<ref>{{cite web|title=2005-06 WHL Season - Rookies|url=http://www.whl.ca/stats/show/type/top_scorers/ls_season/225/subtype/4|accessdate=2011-12-18|publisher=[[Western Hockey League]]}}</ref> to be named the team's rookie of the year.<ref name=call/> On a team basis, the Chiefs finished last in the Western Conference and failed to qualify for the playoffs.<ref>{{cite web|title=2005-06 WHL Season - Division|url=http://www.whl.ca/standings/show/ls_season/225/subtype/0|accessdate=2011-12-18|publisher=[[Western Hockey League]]}}</ref>

Bowman entered the [[2006–07 WHL season|2006–07 season]] listed as the ninth-best WHL prospect in the [[NHL Central Scouting Bureau]] (CSB)'s preliminary rankings of draft-eligible players.<ref name=call/> He was invited to play in the [[CHL Top Prospects Game]] and was subsequently listed in the CSB's midterm rankings as 44th among North American skaters.<ref name=call/> Bowman finished the season with an improved 24 goals and 43 points in 61 games. He played in his first WHL playoffs after the Chiefs ranked fourth in the U.S. Division.<ref>{{cite web|title=2006-07 WHL Season - Division|url=http://www.whl.ca/standings/show/ls_season/227/subtype/0|accessdate=2011-12-18|publisher=[[Western Hockey League]]}}</ref> Playing in six post-season games, Bowman recorded a team-leading seven points (two goals and five assists).<ref>{{cite web|title=2006-2007 Spokane Chiefs Player Statistics|url=http://www.eliteprospects.com/team.php?team=688&year0=2007&status=stats|accessdate=2011-12-18|publisher=EliteProspects.com}}</ref> The Chiefs were eliminated in the first round by the [[Everett Silvertips]].<ref>{{cite web|title=2006-07 WHL Playoff Results|url=http://www.hockeydb.com/ihdb/stats/playoffdisplay.php?league=whl1979&season=2007&leaguenm=WHL|accessdate=2011-12-18|publisher=Hockeydb.com}}</ref> Entering the [[2007 NHL Entry Draft]] in the off-season, he moved up to 36th among North American skaters in the NHL CSB's final rankings.<ref name=call/> Bowman was selected 72nd overall by the [[Carolina Hurricanes]].

[[Image:Drayson Bowman 2009.jpg|thumb|right|Bowman with the [[Spokane Chiefs]] during the [[2008–09 WHL season|2009]] [[Western Hockey League|WHL]] playoffs]]

Following his draft, Bowman participated in his first NHL [[training camp]] in September 2007 before being returned to Spokane to continue playing at the junior level.<ref name=tsn/> Playing in his third full WHL season, Bowman recorded a team-leading 82 points in 66 games.<ref>{{cite web|title=2007–08 Season – Spokane Chiefs|url=http://whl.ca/stats/statdisplay.php?type=skaters&subType=18&season_id=229&leagueId=26&lastActive=&singleSeason=&confId=0|accessdate=2010-04-08|publisher=[[Western Hockey League]]}}</ref> His 42 goals tied for fourth in the league.<ref>{{cite web|title=2007–08 Regular Season – Goals|url=http://whl.ca/stats/statdisplay.php?type=records&season_id=229|accessdate=2010-04-08|publisher=[[Western Hockey League]]}}</ref> Bowman added a team-leading 20 points in 21 playoff games<ref>{{cite web|title=2008 WHL Playoffs – Spokane Chiefs|url=http://whl.ca/stats/statdisplay.php?type=skaters&subType=12&season_id=230&leagueId=26&lastActive=&singleSeason=&confId=0|accessdate=2010-04-08|publisher=[[Western Hockey League]]}}</ref> as the Chiefs captured the [[Ed Chynoweth Cup]] as WHL champions. The league title earned the Chiefs a berth in the [[2008 Memorial Cup]] in [[Kitchener, Ontario]]. Bowman notched a [[hat trick]] in the opening game of the tournament, including the game-tying goal late in the third period of a 5–4 overtime win over the [[Belleville Bulls]].<ref name=memcup/> He went on to score in all four games of the tournament, including [[game-winning goal|game-winners]] against the [[Kitchener Rangers]] in the round-robin and final. The Chiefs went undefeated in the tournament to capture the Memorial Cup as Canadian major junior champions, beating the [[Kitchener Rangers]] 4–1 in the final.<ref name=memcup/> With a team-high eight points (third in tournament scoring behind [[Justin Azevedo]] and [[Matt Halischuk]] of the Rangers), including a tournament-leading six goals, in four games,<ref>{{cite web|title=2007-2008 M-Cup|url=http://www.eliteprospects.com/league.php?leagueid=M-Cup&season=2007|accessdate=2011-12-18|publisher=EliteProspects.com}}</ref> Bowman was named to the Memorial Cup All-Star Team.<ref name=memcup>{{cite web|title=The 2008 Memorial Cup History|url=http://www.mastercardmemorialcup.com/page/the-2008-memorial-cup-history|accessdate=2011-12-18|author=Bell, Aaron|publisher=[[Canadian Hockey League]]}}</ref>

Following his Memorial Cup performance, the Hurricanes signed him to a three-year, [[United States dollar|US$]]2.06 million contract on July 31, 2008.<ref>{{cite news|title=Hurricanes sign teenager Bowman to entry-level deal|url=http://www.tsn.ca/nhl/story/?id=244878&lid=headline&lpos=secStory_nhl|accessdate=2010-04-08|date=2008-07-30|publisher=[[The Sports Network]]}}</ref> Playing in his final season with the Chiefs in [[2008–09 NHL season|2008–09]], Drayson was named an [[alternate captain (hockey)|alternate captain]] to [[Justin McCrae]] along with [[Seth Compton]] and [[Jared Spurgeon]].<ref>{{cite web|title=McCrae Named Captain|url=http://www.oursportscentral.com/services/releases/?id=3746622|accessdate=2008-12-03|date=2008-12-01|publisher=OurSports Central}}</ref> He was named WHL and CHL Player of the Week after recording 12 points in 3 games for the week ending on February 1, 2009.<ref name=chlpow>{{cite news|title=Chiefs' Drayson Bowman Named Boston Pizza CHL Player of the Week|url=http://media.whl.ca/chiefs-drayson-bowman-named-boston-pizza-chl-player-of-the-week-p127556|accessdate=2011-09-12|date=2009-02-05|publisher=[[Western Hockey League]]}}</ref> The next month, he earned his second WHL and CHL Player of the Week distinctions with an eight-point effort in two games for the week ending on March 15, 2009.<ref name=chlpow2>{{cite news|title=Chiefs' Drayson Bowman Named Boston Pizza CHL Player of the Week|url=http://media.whl.ca/chiefs-drayson-bowman-named-boston-pizza-chl-player-of-the-week-p127870|accessdate=2011-09-12|date=2009-03-17|publisher=[[Western Hockey League]]}}</ref> He finished the season with 47 goals, fourth in the league,<ref>{{cite web|title=Leaders: 2008–09 Regular Season, Goals|url=http://www.whl.ca/stats/statdisplay.php?type=records|accessdate=2009-03-18|publisher=[[Western Hockey League]]}}</ref> and a junior career-high 83 points to lead his team in scoring for the second consecutive year.<ref>{{cite web|title=2008-09 Regular Season - Spokane Chiefs|url=http://www.whl.ca/stats/show/type/skaters/ls_season/231/subtype/18|accessdate=2011-12-18|publisher=[[Western Hockey League]]}}</ref> He was named to the WHL West Second All-Star Team along with goaltending teammate [[Dustin Tokarski]].<ref name=whlallstar>{{Cite news|title=WHL Announces Western Conference All-Star Team|url=http://www.tsn.ca/cfl/story/?id=271693|accessdate=2011-09-12|date=2009-03-18|publisher=[[The Sports Network]]|agency=Canadian Press}}</ref> Bowman and the Chiefs were not able to defend their WHL or CHL titles as they were eliminated in seven games in the second round of the WHL playoffs by the [[Vancouver Giants]]. Spokane's elimination marked the end of Bowman's junior career. He left the Chiefs fifth on the team's all-time goals scored list with 136, 10 behind leader [[Pat Falloon]].<ref>{{cite web|title=Franchise All-Time Stats for Spokane Chiefs|url=http://www.eliteprospects.com/team_all-time_stats.php?team=688&alltime=G&leaguename=|accessdate=2011-12-18|publisher=EliteProspects.com}}</ref> He had 114 assists for 250 points total over 269 games.

===Professional===
Upon the completion of Bowman's final WHL season, he was called up by the Hurricanes to travel and practice with the team during their [[2009 Stanley Cup playoffs|2009 playoff season]].<ref name=tsn/><ref>{{cite news|title=Game Two: Canes 2, Devils 1 (OT)|url=http://blogs.newsobserver.com/canes/game-two-canes-2-devils-1-ot|accessdate=2011-12-18|date=2009-04-17|publisher=[[News & Observer]]}}</ref> Carolina advanced to the [[Eastern Conference (NHL)|Eastern Conference]] Finals, where they were eliminated by the [[Pittsburgh Penguins]] in four games.<ref>{{cite web|title=2008-09 NHL Playoff Results|url=http://www.hockeydb.com/ihdb/stats/playoffdisplay.php?league=nhl1927&season=2009&leaguenm=NHL|accessdate=2011-12-18|publisher=Hockeydb.com}}</ref> The [[2009–10 AHL season|following season]], he was assigned to the Hurricanes' [[American Hockey League]] (AHL) affiliate, the [[Albany River Rats]].<ref>{{cite web|title='Canes send 5 players to Albany|url=http://www.usatoday.com/sports/hockey/nhl/2009-09-22-2783103125_x.htm|accessdate=2009-10-11|date=2009-09-22|publisher=''[[USA Today]]''}}</ref> He scored his first professional goal in his AHL debut with Albany on October 3, 2009, in a 6–3 loss to the [[Manchester Monarchs (AHL)|Manchester Monarchs]].<ref>{{cite web|title=Rats lose opener 6–3 in front of 6,507|url=http://blog.timesunion.com/hockey/rats-lose-opener-6-3-in-front-of-6507/2281/|accessdate=2009-10-11|date=2009-10-03|publisher=''[[Times Union (Albany)|Times Union]]''}}</ref> Midway through the season, he was called up by the Hurricanes and made his NHL debut on January 16, 2010.<ref>{{cite news|title=New Canes in fast lane|url=http://www.newsobserver.com/2010/01/18/289975/new-canes-in-fast-lane.html|accessdate=2010-04-08|date=2010-01-18|publisher=''[[News & Observer]]''|author=Chip Alexander}}</ref> Bowman recorded one shot on goal in 10 minutes of ice time in a 5–3 loss to the [[Atlanta Thrashers]].<ref>{{cite web|title=Thrashers 5, Hurricanes 3 Boxscore|url=http://hurricanes.nhl.com/club/boxscore.htm?id=2009020715|accessdate=2011-12-18|date=2010-01-16|publisher=[[Carolina Hurricanes]]}}</ref> After being sent back down to Albany, he received another call-up on March 24 in light of an injury to forward [[Tuomo Ruutu]].<ref>{{cite news|title=Hurricanes Recall Drayson Bowman|url=http://hurricanes.nhl.com/club/news.htm?id=522605|accessdate=2010-04-08|date=2010-03-24|publisher=[[Carolina Hurricanes]]|author=Preston, Ken}}</ref> During that call-up, he scored his first and second career NHL goals against goaltender [[Antero Niittymaki]] in the first period of an 8–5 win against the [[Tampa Bay Lightning]] on April 6.<ref>{{Cite news|title=Hurricanes 8, Lightning 5|url=http://hurricanes.nhl.com/club/recap.htm?id=2009021187|accessdate=2011-12-18|date=2010-04-06|publisher=[[Carolina Hurricanes]]|agency=Associated Press}}</ref> The milestone occurred in his seventh NHL game.<ref name=boost>{{cite news|title=Goals Boost Bowman's Confidence|url=http://hurricanes.nhl.com/club/news.htm?id=524353|accessdate=2010-04-08|date=2010-04-07|publisher=[[Carolina Hurricanes]]|author=Paul Branecky}}</ref> Bowman completed the [[2009–10 NHL season]] with two goals in nine games, while averaging 12 minutes of ice time.<ref>{{cite web|title=2009-2010 - Regular Season - Carolina Hurricanes - Skater - Time On Ice - Time On Ice Per Game|url=http://www.nhl.com/ice/playerstats.htm?fetchKey=20102CARSASALL&sort=avgTOIPerGame&viewName=timeOnIce|accessdate=2011-12-19|publisher=[[National Hockey League]]}}</ref> Hurricanes head coach [[Paul Maurice]] heralded him as a player with "a good set of hands and a really good hockey IQ" during his first stint in the NHL.<ref name=boost/> As Carolina failed to qualify for the [[2010 Stanley Cup playoffs|2010 playoffs]], ranking 11th in the East,{{#tag:ref|The top eight teams in each conference qualified for the playoffs.|group=notes}}<ref>{{cite web|title=2009-2010 Standings|url=http://www.nhl.com/ice/standings.htm?season=20092010&type=CON|accessdate=2011-12-18|publisher=[[National Hockey League]]}}</ref> Bowman was reassigned to the River Rats for their [[2010 Calder Cup Playoffs|2010 playoff season]]. In the AHL, he finished the regular season with 32 points (17 goals and 15 assists) over 56 games, tying for 31st among league rookies and 10th among River Rats players.<ref>{{cite web|title=2009-10 Regular Season -Rookies|url=http://theahl.com/stats/statdisplay.php?first=20&type=top_scorers&subType=4&season_id=30&lastActive=&singleSeason=&leagueId=4&confId=0|accessdate=2011-12-18|publisher=[[American Hockey League]]}}</ref><ref>{{cite web|title=2009-10 Regular Season - Albany River Rats|url=http://theahl.com/stats/statdisplay.php?type=skaters&subType=2&season_id=30&leagueId=4&lastActive=&singleSeason=&confId=0|accessdate=2011-12-18|publisher=[[American Hockey League]]}}</ref> Bowman added nine points (three goals and six assists) over eight games in the playoffs (ranking fifth among rookies and tying for first in team scoring)<ref>{{cite web|title=2010 Calder Cup Playoffs - Rookies|url=http://theahl.com/stats/statdisplay.php?type=top_scorers&subType=4&season_id=33&leagueId=4&lastActive=&singleSeason=&confId=0|accessdate=2011-12-18|publisher=[[American Hockey League]]}}</ref><ref>{{cite web|title=2010 Calder Cup Playoffs - Albany River Rats|url=http://theahl.com/stats/statdisplay.php?type=skaters&subType=1&season_id=33&leagueId=4&lastActive=&singleSeason=&confId=0|accessdate=2011-12-18|publisher=[[American Hockey League]]}}</ref> as the River Rats were eliminated in the second round by the [[Hershey Bears]].<ref>{{cite web|title=2010 Calder Cup Playoffs|url=http://theahl.com/2010-playoffs-p141904|accessdate=2011-12-18|publisher=[[American Hockey League]]}}</ref>

Bowman made the Hurricanes' roster out of training camp in [[2010–11 NHL season|2010–11]], but was returned to the AHL within a month.<ref name=tsn>{{cite web|title=Drayson Bowman|url=http://tsn.ca/nhl/teams/players/bio/?id=6182|accessdate=2011-09-12|publisher=[[The Sports Network]]}}</ref> With the Hurricanes having changed their minor league affiliate, he joined a new team, the [[Charlotte Checkers (2010–)|Charlotte Checkers]]. After recording 30 points (12 goals and 18 assists) over 51 games with the Checkers (10th in team scoring),<ref>{{cite web|title=2010-11 Regular Season - Charlotte Checkers|url=http://theahl.com/stats/statdisplay.php?type=skaters&subType=5&season_id=34&leagueId=4&lastActive=&singleSeason=&confId=0|accessdate=2011-12-18|publisher=[[American Hockey League]]}}</ref> he was recalled on March 10, 2011.<ref name=tsn/> Spending the remainder of the season with the Hurricanes, he finished 2010–11 with one assist over 23 games, while averaging 10 minutes of ice time.<ref>{{cite web|title=2010–2011 – Regular Season – Carolina Hurricanes – Skater – Time on Ice – Time on Ice Per Game|url=http://www.nhl.com/ice/playerstats.htm?fetchKey=20112CARSASALL&sort=avgTOIPerGame&viewName=timeOnIce|accessdate=2011-09-12|publisher=[[National Hockey League]]}}</ref> Carolina failed to qualify for the playoffs, coming within three points of the eighth and final seed in the Eastern Conference.<ref>{{cite web|title=2010-2011 Standings|url=http://www.nhl.com/ice/standings.htm?season=20102011&type=CON|accessdate=2011-12-18|publisher=[[National Hockey League]]}}</ref> With his NHL season over, the Hurricanes returned him to the AHL,<ref name=tsn/> where the Checkers had qualified for the [[2011 Calder Cup Playoffs]]. Bowman contributed 8 points (2 goals and 6 assists) over 15 games as the Checkers were eliminated in the Conference Finals by the [[Binghamton Senators]].<ref>{{cite web|title=2011 Calder Cup Playoffs Bracket|url=http://theahl.com/stats/bracket.php?view=brief&season_id=36|accessdate=2011-12-18|publisher=[[American Hockey League]]}}</ref>

Bowman remained with Charlotte for the beginning of the [[2011–12 NHL season|2011–12 season]], failing to make the Hurricanes' roster out of training camp.<ref name=tsn/> In November 2011, he received a call-up to Carolina that lasted five days.<ref name=tsn/> The following month, he was recalled again and recorded a two-goal game against the [[Vancouver Canucks]] on December 15, 2011.<ref>{{cite news|title=Hurricanes 4, Canucks 3|url=http://hurricanes.nhl.com/club/recap.htm?id=2011020452|accessdate=2011-12-18|date=2011-12-15|publisher=[[Carolina Hurricanes]]|author=Smith, Michael}}</ref> The goals were his first in the NHL in over a year and eight months. After being reassigned, he received two more call ups before the end of the season.<ref name=tsn/> Bowman finished the season with 13 points over 37 NHL games in Carolina and 26 points over 42 AHL games in Charlotte. In the off-season, he was tendered a qualifying offer from the Hurricanes in order to retain his [[restricted free agent]] status.<ref name=tsn/>

A free agent following his first full season in the NHL with the Hurricanes in the [[2013-14 NHL season|2013–14]] season, Bowman agreed to attend the [[Montreal Canadiens]] training camp on a try-out contract on September 2, 2014.<ref>{{cite web| url = http://www.thescore.com/news/568920 | title = Canadiens sign Drayson Bowman to PTO | publisher = ''[[TheScore Inc.|The Score]]'' | date = 2014-09-02 | accessdate = 2014-09-02}}</ref> On October 2 Bowman agreed to a one-year two way contract with the [[Montreal Canadiens]].

Bowman was not re-signed by the Canadiens and on October 12, 2015, without any NHL interest, Bowman signed a one-year deal with the [[Colorado Eagles]] of the ECHL. After registering 3 assists in 3 games with the Eagles to start the [[2015-16 ECHL season|2015–16]] season, Bowman was loaned to former club, the Charlotte Checkers of the AHL on October 22, 2015. Bowman played a further 16 games with the Checkers before he left the club to pursue a European career in agreeing to a contract for the remainder of the season in Germany with Düsseldorfer EG of the DEL on December 21, 2015.

==International play==
Bowman was named to the [[United States men's national junior ice hockey team|United States' under-20 team]] for the [[2009 World Junior Ice Hockey Championships|2009 World Junior Championships]] in [[Ottawa]], [[Ontario]]. He was joined on the national team by Spokane Chiefs teammates [[Tyler Johnson (ice hockey)|Tyler Johnson]] and [[Mitchell Wahl]].<ref>{{cite web|title=Entry List By Team – USA|url=http://stats.iihf.com/Hydra/172/IHM1720USA_83_7_0.pdf|accessdate=2011-09-12|date=2008-12-15|publisher=[[International Ice Hockey Federation]]|format=PDF}}</ref> Playing against [[Germany men's national junior ice hockey team|Germany]] in the first game of preliminaries, he scored twice and was named player of the game.<ref>{{cite web|title=Game Summary|url=http://stats.iihf.com/Hydra/172/IHM172A02_74_3_0.pdf|accessdate=2011-09-12|date=2008-12-26|publisher=[[International Ice Hockey Federation]]|format=PDF}}</ref><ref>{{cite web|title=IIHF World U20 Championships – Best Players Per Game|url=http://stats.iihf.com/Hydra/172/IHM172000_85K_7_0.pdf|accessdate=2009-01-01|publisher=[[International Ice Hockey Federation]]|format=PDF}}</ref> Over six games at the tournament, Bowman totalled three goals and one assist, tying for fifth in team point-scoring.<ref>{{cite web|title=Player Statistics By Team – USA|url=http://stats.iihf.com/Hydra/172/IHM1720USA_83_7_0.pdf|accessdate=2011-09-12|publisher=[[International Ice Hockey Federation]]|format=PDF}}</ref> After losing to [[Slovakia men's national junior ice hockey team|Slovakia]] 5–3 in the quarterfinal,<ref>{{cite web|title=Game Summary|url=http://stats.iihf.com/Hydra/172/IHM172321_74_4_0.pdf|accessdate=2011-09-12|date=2009-01-02|publisher=[[International Ice Hockey Federation]]|format=PDF}}</ref> the United States beat the [[Czech Republic men's national junior ice hockey team|Czech Republic]] 3–2 in overtime of their placement game to rank fifth in the tournament.<ref>{{cite web|title=Game Summary|url=http://stats.iihf.com/Hydra/172/IHM172329_74_5_0.pdf|accessdate=2011-09-12|date=2009-01-04|publisher=[[International Ice Hockey Federation]]|format=PDF}}</ref><ref>{{cite web|title=Final Ranking|url=http://stats.iihf.com/Hydra/172/IHM172Z012_Final_Ranking_1_0.pdf|accessdate=2011-09-12|date=2009-01-06|publisher=[[International Ice Hockey Federation]]|format=PDF}}</ref>

==Career statistics==

===Regular season and playoffs===
{| border="0" cellpadding="1" cellspacing="0" style="text-align:center; width:60em"
|- style="background:#e0e0e0;"
! colspan="3" style="background:#fff;"|  
! rowspan="99" style="background:#fff;"|  
! colspan="5"                    | [[Regular season]]
! rowspan="99" style="background:#fff;"|  
! colspan="5"                    | [[Playoffs]]
|- style="background:#e0e0e0;"
! [[Season (sports)|Season]]
! Team
! League
! GP
! [[Goal (ice hockey)|G]]
! [[Assist (ice hockey)|A]]
! [[Point (ice hockey)|Pts]]
! [[Penalty (ice hockey)|PIM]]
! GP
! G
! A
! Pts
! PIM
|- 
| 2004–05
| [[Kimberley Dynamiters (KIJHL)|Kimberley Dynamiters]]
| [[Kootenay International Junior Hockey League|KIJHL]]
| 47 || 29 || 30 || 59 || 108
| — || — || — || — || —
|- style="background:#f0f0f0;"
| [[2004-05 WHL season|2004–05]]
| [[Spokane Chiefs]]
| [[Western Hockey League|WHL]]
| 4 || 0 || 0 || 0 || 0 
| — || — || — || — || —
|- 
| [[2005-06 WHL season|2005–06]]
| Spokane Chiefs 
| WHL 
| 72 || 17 || 17 || 34 || 51
| — || — || — || — || —
|- style="background:#f0f0f0;"
| [[2006-07 WHL season|2006–07]]
| Spokane Chiefs
| WHL
| 61 || 24 || 19 || 43 || 55
| 6 || 2 || 5 || 7 || 4
|- 
| [[2007-08 WHL season|2007–08]]
| Spokane Chiefs
| WHL
| 66 || 42 || 40 || 82 || 62
| 21 || 11 || 9 || 20 || 8 
|- style="background:#f0f0f0;"
| [[2008-09 WHL season|2008–09]]
| Spokane Chiefs
| WHL
| 62 || 47 || 36 || 83 || 107
| 12 || 8 || 5 || 13 || 8
|- 
| [[2009-10 AHL season|2009–10]]
| [[Albany River Rats]] 
| [[American Hockey League|AHL]]
| 56 || 17 || 15 || 32 || 29
| 8 || 3 || 6 || 9 || 12
|- style="background:#f0f0f0;"
| [[2009-10 NHL season|2009–10]]
| [[Carolina Hurricanes]]
| [[NHL]] 
| 9 || 2 || 0 || 2 || 4
| — || — || — || — || —
|- 
| [[2010-11 AHL season|2010–11]]
| [[Charlotte Checkers (2010-)|Charlotte Checkers]] 
| AHL
| 51 || 12 || 18 || 30 || 53
| 15 || 2 || 6 || 8 || 6
|- style="background:#f0f0f0;"
| [[2010-11 NHL season|2010–11]]
| Carolina Hurricanes
| NHL 
| 23 || 0 || 1 || 1 || 12
| — || — || — || — || —
|- 
| [[2011-12 AHL season|2011–12]]
| Charlotte Checkers 
| AHL
| 42 || 13 || 13 || 26 || 45
| — || — || — || — || —
|- style="background:#f0f0f0;"
| [[2011-12 NHL season|2011–12]]
| Carolina Hurricanes
| NHL 
| 37 || 6 || 7 || 13 || 4
| — || — || — || — || —
|- 
| [[2012-13 AHL season|2012–13]]
| Charlotte Checkers 
| AHL
| 37 || 14 || 8 || 22 || 21
| — || — || — || — || —
|- style="background:#f0f0f0;"
| [[2012-13 NHL season|2012–13]]
| Carolina Hurricanes
| NHL 
| 37 || 3 || 2 || 5 || 17
| — || — || — || — || —
|- 
| [[2013-14 NHL season|2013–14]]
| Carolina Hurricanes
| NHL 
| 70 || 4 || 8 || 12 || 16
| — || — || — || — || —
|- style="background:#f0f0f0;"
| [[2014-15 AHL season|2014–15]]
| [[Hamilton Bulldogs (AHL)|Hamilton Bulldogs]]
| AHL 
| 62 || 14 || 19 || 33 || 33
| — || — || — || — || —
|- 
| [[2014-15 NHL season|2014–15]]
| [[Montreal Canadiens]]
| NHL 
| 3 || 0 || 0 || 0 || 0
| — || — || — || — || —
|- style="background:#f0f0f0;"
| [[2015-16 ECHL season|2015–16]]
| [[Colorado Eagles]]
| ECHL 
| 3 || 0 || 3 || 3 || 2
| — || — || — || — || —
|- 
| [[2015-16 AHL season|2015–16]]
| Charlotte Checkers
| AHL 
| 16 || 2 || 2 || 4 || 10
| — || — || — || — || —
|- style="background:#f0f0f0;"
| [[2015-16 DEL season|2015–16]]
| [[Düsseldorfer EG]]
| [[Deutsche Eishockey Liga|DEL]]
| 24 || 10 || 7 || 17 || 33
| 5 || 1 || 0 || 1 || 4 
|- 
| [[2016-17 DEL season|2016–17]]
| Düsseldorfer EG
| DEL 
| 51 || 11 || 9 || 20 || 24
| — || — || — || — || —
|- style="background:#e0e0e0;"
! colspan="3" | NHL totals
! 179 !! 15 !! 18 !! 33 !! 53
! — !! — !! — !! — !! —
|}

===International===
{| border="0" cellpadding="1" cellspacing="0" ID="Table3" style="text-align:center; width:40em"
|-  style="text-align:center; background:#e0e0e0;"
! Year
! Team
! Event
! Result
! rowspan="99" style="background:#fff;"| 
! GP
! G
! A
! Pts
! PIM
|-
| [[2009 World Junior Ice Hockey Championships|2009]]
| [[United States men's national junior ice hockey team|United States]]
| [[World Junior Ice Hockey Championships|WJC]]
| 5th
| 6 || 3 || 1 || 4 || 6
|- style="background:#e0e0e0;"
! colspan="4" | Junior totals
! 6 !! 3 !! 1 !! 4 !! 6
|}

==Awards==
{| class="wikitable"
|-
! Award
! Year
! Ref
|- 
! colspan="2" | KIJHL
|-
| Eddie Mountain Division rookie of the year || 2005 || <ref name=kijhl/>
|-
! colspan="2" | Major junior
|-
| [[Ed Chynoweth Cup]] <small>(Spokane Chiefs)</small> || [[2007–08 WHL season|2008]] || 
|-
| [[Memorial Cup]] <small>(Spokane Chiefs)</small> || [[2008 Memorial Cup|2008]] || 
|-
| [[Canadian Hockey League|CHL]] [[Memorial Cup]] All-Star Team || [[2008 Memorial Cup|2008]] || <ref>[http://www.mastercardmemorialcup.ca/historyallstar Memorial Cup All-Star Teams] {{webarchive |url=https://web.archive.org/web/20160107181233/http://www.mastercardmemorialcup.ca/historyallstar |date=January 7, 2016 }}</ref>
|-
| [[Western Hockey League|WHL]] and [[Canadian Hockey League|CHL]] player of the week || January 26 – February 1, [[2008–09 WHL season|2009]]<br>March 9–15, 2009 || <ref name=chlpow/><br><ref name=chlpow2/>
|-
! colspan="2" | International
|-
| [[World Junior Ice Hockey Championships|World Junior Championships]] player of the game || vs. Germany, preliminaries; [[2009 World Junior Ice Hockey Championships|2009]] || 
|}

==Notes==
{{reflist|group=notes}}

==References==
{{reflist|2}}

==External links==
*{{Eliteprospects}}
*{{nhlprofile|8474137}}
*[http://whl.ca/players/24280 Drayson Bowman's WHL Profile]
*{{hockeydb|82947}}

{{Commons category|Drayson Bowman}}

{{DEFAULTSORT:Bowman, Drayson}}
[[Category:1989 births]]
[[Category:Albany River Rats players]]
[[Category:American ice hockey centers]]
[[Category:Carolina Hurricanes draft picks]]
[[Category:Carolina Hurricanes players]]
[[Category:Charlotte Checkers (2010–) players]]
[[Category:Colorado Eagles players]]
[[Category:Düsseldorfer EG players]]
[[Category:Hamilton Bulldogs (AHL) players]]
[[Category:Ice hockey people from Michigan]]
[[Category:Living people]]
[[Category:Memorial Cup winners]]
[[Category:Montreal Canadiens players]]
[[Category:Spokane Chiefs players]]
[[Category:Sportspeople from Grand Rapids, Michigan]]