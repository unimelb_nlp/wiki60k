{{refimprove|date=May 2016}}
{{Infobox organization
|image        =
|abbreviation = MESA
|motto        =
|formation    = 1966
|type         = [[learned society]]
|location     = [[Tucson, Arizona]]
|leader_title = President
|leader_name  = Nathan Brown
|website      = [http://www.mesana.org/ mesana.org]}}

'''Middle East Studies Association''' (often referred to as '''MESA''') is a [[learned society]], and according to its website, "a non-political association that fosters the study of the [[Middle East]], promotes high standards of scholarship and teaching, and encourages public understanding of the region and its peoples through programs, publications and services that enhance education, further intellectual exchange, recognize professional distinction, and defend [[academic freedom]]."<ref>{{cite web|url=http://www.mesa.arizona.edu/about/index.html|title=Middle East Studies Association|date= |accessdate=2014-12-02}}</ref>

==History==
{{Unreferenced section|date=August 2015}}
MESA was founded in 1966 with 50 original members. Its current{{When|date=August 2015}} membership exceeds 2,900 and it "serves as an umbrella organization for more than sixty institutional members and thirty-nine affiliated organizations".{{Cite quote|date=August 2015}} It is a constituent society of the [[American Council of Learned Societies]] and the [[National Council of Area Studies Associations]], and a member of the [[National Humanities Alliance]].

Regions of interest to MESA members include Iran, Turkey, Afghanistan, Israel, Pakistan, and the countries of the Arab world from the seventh century to modern times. Spain, Southeastern Europe, the Soviet Union and other regions also are included for the periods in which their territories were part of the Middle Eastern empires or were under the influence of Middle Eastern civilization. Historians comprise the largest group of disciplinary specialists in MESA followed by political science/international relations, anthropology, and language and literature. The interdisciplinary gender and sexuality studies group is quite strong.{{Clarify|reason=vague|date=August 2015}}

==Activities==
The current president is [[Nathan J. Brown (political scientist)|Nathan Brown]], [[George Washington University]].

===Publications===
''[[The International Journal of Middle East Studies]]'' (IJMES) is a quarterly journal published by [[Cambridge University Press]] under the auspices of MESA. The editor is Akram Khater of [[North Carolina State University]].

The ''Review of Middle East Studies'' (RoMES) is MESA’s journal of review. MESA policy has established the focus of RoMES as the state of the craft in all fields of Middle East studies. The Editor is Richard C. Martin and the journal is based at [[Virginia Tech]].

MESA has a very active Committee on Academic Freedom (CAF) that has two wings: CAFMENA (Middle East and North Africa, established in 1990) and CAFNA (North America, established in 2005). Through CAF, MESA monitors infringements on [[Academic Freedom in the Middle East|academic freedom on the Middle East and North Africa]] worldwide.<ref>{{cite web|url=http://www.mesa.arizona.edu/committees/academic-freedom/index.html|title=Committee on Academic Freedom (CAF)|work=arizona.edu|accessdate=August 23, 2015}}</ref>

Each year CAF nominates candidates for MESA’s Academic Freedom Award. The winners are confirmed by the Board of Directors.

===Controversies===
Israeli-American historian [[Martin Kramer]] in his 2001 book ''Ivory Towers on Sand: The Failure of Middle Eastern Studies in America'' accused Middle Eastern studies programs of ignoring the mounting threat of Islamic terrorism. In a ''Wall Street Journal'' article published in 2001, Kramer claimed that Middle Eastern studies courses, as they stood, were "part of the problem, not its remedy". In a ''Foreign Affairs'' review of the book, F. Gregory Gause said his analysis was, in part, "serious and substantive" but "far too often his valid points are overshadowed by academic score-settling and major inconsistencies."[2]{{Cite quote|date=August 2015}}

In 2002, American writer [[Daniel Pipes]] established an organization called Campus Watch to combat what he perceived to be serious problems within the discipline, including "analytical failures, the mixing of politics with scholarship, intolerance of alternative views, apologetics, and the abuse of power over students".{{Cite quote|date=August 2015}} He encouraged students to advise the organization of problems at their campuses. In turn critics within the discipline such as [[John Esposito]] accused him of "McCarthyism".

In 2010, foreign policy analyst [[Mitchell Bard]] claimed in his 2010 book ''The Arab Lobby: The Invisible Alliance That Undermines America's Interests in the Middle East'' that elements of the Arab lobby, particularly Saudi Arabia and pro-Palestinian advocates, were hijacking the academic field of Middle Eastern studies within several prominent American universities including Georgetown University, Harvard University, and Columbia University.[3] This has involved Saudi Arabia and other Gulf States funding centers and chairs at universities to promote a pro-Arabist agenda.[4] Bard has also accused several prominent Middle Eastern studies academics including John Esposito and Rashid Khalidi of abusing positions by advancing a pro-Palestinian political agenda.[5]

In addition, Bard has criticized the Middle Eastern Studies Association (MESA) for adopting a pro-Palestinian standpoint. Bard has also alleged that MESA marginalizes non-Israel-related topics including the Kurdish–Turkish conflict and the persecution of religious minorities like Christians and Jews and ethnic minorities that are non-Arabs such as Kurds.[6] Finally, Bard has contended that since the September 11 attacks, the Arab lobby working through Middle Eastern Studies university departments have sought to influence pre-university education by tailoring education programs and resources to reflect a pro-Arabist agenda.[7]

===Awards ===
{{Unreferenced section|date=August 2015}}
'''Albert Hourani Book Award'''

Since 1991 MESA has awarded the [[Albert Hourani Book Award]] to recognize "the very best in Middle East studies scholarship". The prize is named after [[Albert Hourani]], "to recognize his long and distinguished career as teacher and mentor".{{Cite quote|date=August 2015}}The MESA Dissertation Awards were established in 1982 to recognize exceptional achievement in research and writing for/of dissertations in Middle East studies. In 1984 the award was named for Malcolm H. Kerr to honor his significant contributions to Middle East studies. Awards are given in two categories: Social Sciences and Humanities.

'''MESA Mentoring Award'''

Since 1996 the MESA Mentoring Award has recognized exceptional contributions retired faculty have made to the education and training of others.

'''Jere L. Bacharach Service Award'''

Since 1997 [[Jere L. Bacharach]] Service Award has recognized the contributions of individuals through their outstanding service to MESA or the profession. Service is defined broadly to include work in diverse areas, including but not limited to outreach, librarianship, and film. 
{| class="wikitable"
|+'''List of Recipients'''
!Year
!Recipient
!Institution
|-
|1997
|Ellen-Fairbanks D. Bodman
[[I. William Zartman]]
|[[University of North Carolina at Chapel Hill]]
[[Johns Hopkins University]]
|-
|1998
|Richard L. Chambers
|[[University of Chicago]]
|-
|1999
|[[George N. Atiyeh]]
|[[Library of Congress]]
|-
|2000
|Louisa Moffitt
|[[Marist School (Georgia)|Marist School]]
|-
|2001
|Elizabeth W. Fernea
|[[University of Texas at Austin]]
|-
|2002
|Jeanne Jeffers Mrad
|Center for Maghrib Studies in Tunisia
|-
|2004
|[[Jere L. Bacharach]]
|[[University of Washington]]
|-
|2005
|Ernest N. McCarus
|[[University of Michigan]]
|-
|2006
|Howard A. Reed
|[[University of Connecticut]]
|-
|2008
|[[Fred Donner|Fred McGraw Donner]]
|[[University of Chicago]]
|-
|2009
|Mary Ellen Lane
|[[Council of American Overseas Research Centers]]
|-
|2010
|McGuire Gibson
|[[University of Chicago]]
|-
|2011
|Bruce Craig
[[Michael Hudson (political scientist)|Michael C. Hudson]]
|[[University of Chicago]]
[[National University of Singapore]]
|-
|2012
|Erika H. Gilson
|[[Princeton University]]
|-
|2014
|Günter Meyer
|[[University of Mainz]]
|-
|2015
|[[Virginia H. Aksan]]
|[[McMaster University]]
|}

== Former presidents ==
{{Unreferenced section|date=August 2015}}
The following persons have been presidents of the association:
{{div col|cols=2}}
* 2013 Peter Sluglett
* 2012 [[Fred Donner]]
* 2011 [[Suad Joseph]] 
* 2010 Roger M.A. Allen
* 2009 Virginia Aksan 
* 2008 Mervat Hatem 
* 2007 Zachary Lockman
* 2006 [[Juan Cole]]
* 2005 Ali Banuazizi 
* 2004 [[Laurie Brand]] 
* 2003 Lisa Anderson 
* 2002 [[Joel Beinin]]
* 2001 R. Stephen Humphreys 
* 2000 Jere L. Bacharach
* 1999 Barbara Stowasser
* 1998 [[Philip S. Khoury]]
* 1997 Leila Fawaz
* 1996 Farhad Kazemi
* 1995 Ann M. Lesch
* 1994 [[Rashid I. Khalidi]]
* 1993 John O. Voll
* 1992 Barbara Aswad
* 1991 Dale F. Eickelman
* 1990 Yvonne Y. Haddad
* 1989 [[John L. Esposito]]
* 1988 [[William B. Quandt]]
* 1987 [[Michael Hudson (political scientist)|Michael C. Hudson]]
* 1986 [[Elizabeth W. Fernea]]
* 1985 Kemal Karpat
* 1984 Ira M. Lapidus
* 1983 Richard T. Antoun
* 1982 I. William Zartman
* 1981 Nikki R. Keddie
* 1980 Farhat Ziadeh
* 1979 Afaf Lutfi al‐Sayyid Marsot
* 1978 Wilfred Cantwell Smith
* 1977 George Makdisi
* 1976 L. Carl Brown
* 1975 Roderic H. Davison
* 1974 [[Leonard Binder]]
* 1973 [[Charles Issawi]]
* 1972 [[Malcolm H. Kerr]]
* 1971 John S. Badeau
* 1970 William M. Brinner
* 1969 R. Bayly Winder
* 1968 [[George Hourani]]
* 1967 Morroe Berger
* 1966 G.E. von Grunebaum (Honorary)
{{div col end}}

==References==
<references />

==External links==
* {{Official website|http://www.mesa.arizona.edu/ }}

[[Category:Middle East]]
[[Category:Learned societies]]
[[Category:Middle Eastern studies in the United States]]
[[Category:Member organizations of the American Council of Learned Societies]]