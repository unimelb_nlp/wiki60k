{{good article}}
{{Infobox protected area 
| name = Boundary Waters Canoe Area Wilderness
| photo = Pose lake Minnesota.jpg
| photo_caption = Sunset over Pose Lake, a small lake accessible only by foot.
| iucn_category = Ib
| map = USA 
| relief = 1 
| map_caption = 
| location = [[Cook County, Minnesota|Cook]] / [[Lake County, Minnesota|Lake]] / [[St. Louis County, Minnesota|Saint Louis]] counties, [[Minnesota]], United States
| coordinates = {{coords|48|N|91|W|display=inline, title}}
| area_acre = 1090000
| area_ref = <ref>{{cite web |url=http://www.fs.fed.us/land/staff/lar/LAR04/table7.htm |title=Land Area Report |publisher=U.S. Forest Service |year=2004 |accessdate=28 August 2006}}</ref>
| established = 1964
| visitation_num = 250,000+
| visitation_year =
| governing_body = [[United States Forest Service|U.S. Forest Service]] 
}}
<!-- End Infobox template table -->
The '''Boundary Waters Canoe Area Wilderness''' ('''BWCAW''' or '''BWCA'''), is a {{convert|1090000|acre|km2|abbr=on|adj=on}} [[U.S. Wilderness Area|wilderness area]] within the [[Superior National Forest]] in northeastern [[Minnesota]] (United States) under the administration of the [[United States Forest Service|U.S. Forest Service]]. A mixture of [[north woods]] forests and glacial lakes and streams, the BWCAW's preservation as a primitive wilderness began in the 1900s and culminated in the [[Boundary Waters Canoe Area Wilderness Act]] of 1978. It is a popular destination for both [[canoeing]] and [[fishing]] on its many lakes and is one of the most visited wildernesses in the United States.<ref name=tripguide/>{{rp|10}}

== Geography ==
[[File:Bwca map.png|thumb|left|290px|The BWCAW within the [[Superior National Forest]]]]

The BWCAW extends along {{convert|150|miles}} of the U.S.–Canada border in the [[Arrowhead Region]] of Minnesota. The combined region of the BWCAW, Superior National Forest, [[Voyageurs National Park]] and [[Ontario|Ontario's]] [[Quetico Provincial Park|Quetico]] and [[La Verendrye Provincial Park|La Verendrye]] [[Ontario Parks|Provincial Parks]] make up a large area of contiguous wilderness lakes and forests called the "Quetico-Superior country", or simply the [[Boundary Waters]]. [[Lake Superior]] lies to the south and east of the Boundary Waters.<ref name=tripguide>{{cite web|title=Boundary Waters Canoe Area Wilderness Trip Planning Guide|url=http://www.fs.usda.gov/Internet/FSE_DOCUMENTS/stelprd3799760.pdf|website=USDA – Forest Service|format=pdf}}</ref><ref name=searle>{{cite book|last1=Searle|first1=R. Newell|title=Saving Quetico-Superior, A Land Set Apart|date=1977|publisher=Minnesota Historical Society Press|isbn=0-87351-116-6}}</ref>{{rp|1–3}}

{{convert|190000|acre|km2}}, nearly 20% of the BWCAW's total area is water. Within the borders of the area are over 1,100 lakes and hundreds of miles of rivers and streams. Much of the other 80% of the area is forest. The BWCAW is the largest remaining area of uncut forest in the eastern portion of the United States.<ref name=tripguide/>

The [[Laurentian Divide]] between the [[Great Lakes]] and [[Hudson Bay]] [[drainage basin|watershed]]s runs northeast–southwest through the east side of the BWCAW, following the crest of the [[Superior Upland]] and [[Gunflint Range]].<ref name=paddling>{{cite book|last1=Churchill|first1=James|title=Paddling the Boundary Waters and Voyageurs National Park|date=2003|publisher=Globe Pequot|isbn=978-0-7627-1148-2}}</ref>{{rp|9}} The crossing of the divide at [[Height of Land Portage]] was an occasion for ceremony and initiation rites for the [[fur trade|fur-trading]] [[Coureur des bois#Voyageurs|Voyageur]]s of the 18th and early 19th centuries.<ref name=baptizing>{{cite journal|last=Podruchny|first=Carolyn|title=Baptizing Novices: Ritual Moments among French Canadian Voyageurs in the Montreal Fur Trade, 1780–1821|journal=Canadian Historical Review|volume=83|issue=2|pages=165–95|publisher=University of Toronto Press, Journals Division|date=June 2002|url = http://www.utpjournals.press/doi/abs/10.3138/CHR.83.2.165|doi =10.3138/CHR.83.2.165}} (Abstract)</ref> The wilderness also includes the highest peak in Minnesota, [[Eagle Mountain (Minnesota)|Eagle Mountain]] ({{convert|2301|feet}}), part of the [[Misquah Hills]].<ref name=tripguide/>

Located around the perimeter of the BWCAW are six ranger stations: in [[Cook, Minnesota|Cook]], [[Aurora, Minnesota|Aurora]], [[Ely, Minnesota|Ely]], [[Isabella, Minnesota|Isabella]], [[Tofte, Minnesota|Tofte]] and [[Grand Marais, Minnesota|Grand Marais]]. The two nearby communities with most visitor services are Ely and Grand Marais.<ref name=tripguide/> Several historic roads such as the [[Gunflint Trail]], Echo Trail (County Road&nbsp;116) and Fernberg Road (County Road&nbsp;18) allow access to many wilderness entry points.<ref name=paddling/>

== Natural history ==
[[File:Pinus strobus NinaMoose.jpg|thumb|right|An [[eastern white pine]] growing on glacially scoured bedrock, Nina Moose Lake]]

=== Geology ===
{{main article|Geology of Minnesota}}

The lakes of the BWCAW are located in depressions formed by differential erosion of the tilted layers of the [[Canadian Shield]]. For the past [[Quaternary glaciation|two million years]], massive [[ice sheet|sheets of ice]] have repeatedly [[Glacier#Glacial erosion|scoured]] the landscape. The [[last glacial period]] ended with the retreat of the [[Laurentide Ice Sheet]] from the Boundary Waters about 17,000 years ago. The resulting depressions in the landscape later filled with water, becoming the lakes of today.<ref name = MnGeol>{{Cite journal
  | last = Ojakangas
  | first = Richard
  | author-link =
  | last2 = Matsch
  | first2 = Charles
  | author2-link =
  | title = Minnesota's Geology
  | place= Minneapolis
  | publisher = [[University of Minnesota Press]]
  | year = 1982
  | location =
  | volume =
  | edition =
  | url =
  | doi =
  | isbn = 0-8166-0953-5}}</ref><ref name="Heinselman"/>

Many varieties of [[Precambrian]] bedrock are exposed including: [[granite]], [[basalt]], [[greenstone belt|greenstone]], [[gneiss]], as well as [[metamorphic rock]]s derived from [[volcanic rock|volcanic]] and [[sedimentary rock]]s. Greenstone of the [[Superior craton]] located near Ely, Minnesota, is up to 2.7&nbsp;billion years old.<ref>{{cite journal|last1=Weiblen|first1=Paul W.|title=It's Written in the Rocks: The BWCA History|journal=The Conservation Volunteer|date=1971|url=http://conservancy.umn.edu/bitstream/handle/11299/93796/bwca.pdf?sequence=1&isAllowed=y}}</ref> [[Igneous rock]]s of the [[Duluth Complex]] comprise the bedrock of the eastern Boundary Waters.<ref name = MnGeol/> Ancient [[microfossils]] have been found in the [[banded iron formation]]s of the [[Gunflint Chert]].<ref>{{cite journal|last1=Barghoorn|first1=Elso S.|last2=Tyler|first2=Stanley A.|title=Microorganisms from the Gunflint Chert|journal=Science|date=1965|volume=147|issue=3658|pages=563–577|doi=10.1126/science.147.3658.563}}</ref>

=== Forest ecology ===
The Boundary Waters area is within the [[Laurentian Mixed Forest Province]] (commonly called the "North Woods"), a transitional zone between the [[taiga|boreal forest]] to the north and the [[temperate broadleaf and mixed forest|temperate hardwood forest]] to the south that contains characteristics of each. Trees found within the wilderness area include [[conifer]]s such as [[red pine]], [[eastern white pine]], [[jack pine]], [[Abies balsamea|balsam fir]], [[Picea glauca|white spruce]], [[Picea mariana|black spruce]], and [[Thuja occidentalis|white-cedar]], as well as [[deciduous]] [[Betula papyrifera|birch]], [[Populus tremuloides|aspen]], [[Fraxinus|ash]], and [[maple]]. [[Vaccinium angustifolium|Blueberries]] and [[Rubus strigosus|raspberries]] can be found in cleared areas. The BWCAW is estimated to contain {{convert|455,000|acres|km2}} of [[old growth forest]], woods that may have burned but have never been logged.<ref>Heinselman (1996), p. 18, Table 4.1.  This figure is for solid contiguous areas of virgin forest; it does not include some smaller areas within cut-over forests, nor some shoreline areas of virgin woods.  ''Id.''</ref>  Before fire suppression efforts began during the 20th century, [[wildfire|forest fires]] were a natural part of the Boundary Waters ecosystem, with recurrence intervals of 30 to 300 years in most areas.<ref name="Heinselman">{{cite book |title=The Boundary Waters Wilderness Ecosystem |last=Heinselman |first=Miron |publisher=University of Minnesota Press |location=Minneapolis, Minnesota |year=1996|isbn=0 8166 2804 1 }}</ref><ref>{{cite web|title=ECS: Border Lakes Subsection|url=http://www.dnr.state.mn.us/ecs/212La/index.html|website=Minnesota Department of Natural Resources}}</ref>

On July 4, 1999, a powerful wind storm, or [[derecho]], swept across Minnesota, central Ontario, and southern Quebec. Winds as high as {{convert|100|mph}} knocked down millions of trees, affecting about {{convert|370000|acres|km2}} within the BWCAW and injuring 60 people. This event became known officially as the [[Boundary Waters – Canadian derecho]], commonly referred to as "the Boundary Waters blowdown". Although [[campsite]]s and portages were quickly cleared after the storm, an increased risk of wildfire due to the large number of downed trees became a concern. The U.S. Forest Service undertook a schedule of [[controlled burn|prescribed burns]] to reduce the forest fuel load in the event of a wildfire.<ref>{{cite web|title=10 years after the big blowdown|url=http://www.friends-bwca.org/2009/07/10-years-after-the-big-blowdown/|website=Friends of the Boundary Waters Wilderness}}</ref>
 
[[File:Pagami creek smoke plume.jpeg|thumb|right|Smoke from the 2011 [[Pagami Creek Fire]]]]

The first major wildfire within the blowdown area occurred in August 2005, burning {{convert|1335|acres|km2}} between Alpine Lake and Seagull Lake in the northeastern BWCAW.<ref>{{cite web|title=Alpine Lake Fire|url=http://bwca.cc/news/2005/AlpineLakeFire/|website=bwca.cc}}</ref> In 2006, two fires at Cavity Lake and Turtle Lake burned more than {{convert|30000|acres|km2}}.<ref>{{cite web|title=BWCA Cavity Lake Wildfire|url=http://www.bwca.cc/bwcafire/cavity/cavity.htm|website=bwca.cc}}</ref><ref>{{cite web|title=Turtle Lake Fire|url=http://www.bwca.cc/bwcafire/westzone/turtlelake-06.htm|website=bwca.cc}}</ref> In May 2007, the [[Gunflint Trail#Ham Lake Fire|Ham Lake Fire]] started near the location of the Cavity Lake fire, eventually covering {{convert|76000|acre|km2}} in Minnesota and Ontario and becoming the most extensive wildfire in Minnesota in 90 years.<ref>{{cite web |url=http://archive.kare11.com/news/news_article.aspx?storyid=508432|archive-url=https://web.archive.org/web/20150122060814/http://archive.kare11.com/news/news_article.aspx?storyid=508432|dead-url=yes|archive-date=2015-01-22|title=Gunflint Trail fire anniversary to be marked by tree plantings|website=[[KARE]]11.com}}  </ref> In 2011, the [[Pagami Creek Fire]] ultimately grew to over {{convert|92000|acre|km2}}, spreading beyond the wilderness boundary to threaten homes and businesses.<ref>{{cite news|url=http://www.lakelandtimes.com/main.asp?SectionID=9&SubSectionID=9&ArticleID=13839|title=Boundary Waters fire threatening homes, cabins|first=Douglas|last=Etten|publisher=Lakeland Times|date=2011-09-13}}</ref> Smoke from the Pagami Creek Fire drifted east and south as far as the [[Upper Peninsula of Michigan]], [[Ontario]], [[Chicago]], [[Poland]], [[Ukraine]], and [[Russia]].<ref>{{cite web|last1=Gabbert|first1=Bill|title=Smoke from Pagami Creek fire detected over eastern Europe today|url=http://wildfiretoday.com/2011/09/16/smoke-from-pagami-creek-fire-detected-over-eastern-europe-today/|website=Wildfire Today}}</ref><ref>{{cite news|url=http://toronto.ctv.ca/servlet/an/local/CTVNews/20110913/smoke-ontario-forest-fire-minnesota-110913/20110913/?hub=TorontoNewHome|title=Ont. smells smoke from Minnesota forest fire|publisher=[[CTV Toronto]]|date=2011-09-13|accessdate=2011-09-13}}</ref><ref>{{cite news|url=http://www.chicagotribune.com/news/local/breaking/chi-smoke-from-minnesota-wildfire-reaches-illinois-20110913,0,1457256.story|title=Smoke from Minnesota forest fire makes its way to Chicago's suburbs|publisher=[[Chicago Tribune]]|date=2011-09-13|accessdate=2011-09-13}}</ref>

=== Fauna ===
[[File:Common Loon.jpg|thumb|left|Common loon]]
Animals found in the BWCAW include [[white-tailed deer|deer]], [[western moose|moose]], [[North American beaver|beaver]], [[Great Lakes boreal wolf|timber wolves]], [[American black bear|black bears]], [[bobcat]]s, [[bald eagle]]s, [[peregrine falcon]]s and [[great northern diver|loons]]. It is within the range of the largest population of wolves in the [[contiguous United States]], as well as an unknown number of [[Canada lynx]]. It has also been identified by the [[American Bird Conservancy]] as a globally important bird habitat.<ref>{{cite web|title=Superior National Forest – About the Forest|url=http://www.fs.usda.gov/detail/superior/about-forest/?cid=fsm91_049837|website=USDA – Forest Service}}</ref>

[[Reindeer|Woodland caribou]] once inhabited the region but have disappeared due to predation by wolves, encroachment by deer, and the effects of a [[Parelaphostrongylus tenuis|brainworm parasite]] carried by deer which is harmful to both caribou and moose populations. Very rare sightings have been reported in nearby areas.<ref>{{cite journal|last1=Mech|first1=L. David|last2=Nelson|first2=Michael E.|last3=Drabik|first3=Harry F.|title=Reoccurrence of Caribou in Minnesota|journal=The American Midland Naturalist|date=1982|volume=101|issue=8|pages=206–208|doi=10.2307/2425312|jstor=2425312}}</ref>

== Human history ==

=== Native Americans ===
[[File:Hegman Lake Pictographs.jpg|thumb|200px|right|Pictographs at Hegman Lake, as they looked in 2003]]
Within the BWCAW are hundreds of prehistoric [[rock painting|pictograph]]s and [[petroglyph]]s on rock ledges and cliffs. The BWCAW is part of the historic homeland of the [[Ojibwa|Ojibwe]] people, who traveled the waterways in [[canoe]]s made of [[birch]] [[bark]]. Prior to Ojibwe settlement, the area was sparsely populated by the [[Sioux]], who migrated westward following the arrival of the Ojibwe. It is thought that the [[Hegman Lake Pictograph]] located on a large overlooking rock wall on North Hegman Lake were most likely created by the Ojibwe. The pictograph appears to represent Ojibwe meridian constellations visible in winter during the early evening, knowledge of which may have been useful for navigating in the deep woods during the winter hunting season. The [[Grand Portage Indian Reservation]], just east of the BWCAW at the community of [[Grand Portage (community), Minnesota|Grand Portage]], is home to a number of Ojibwe to this day.<ref>{{cite book|last1=Furtman|first1=Michael|title=Magic on the Rocks: Canoe Country Pictographs|date=2000|publisher=Birch Portage Press|location=Duluth, Minn.|isbn=978-0-916691-02-8}}</ref>

=== European exploration and development ===
[[File:Voyageur canoe.jpg|thumb|left|A [[Voyageurs|Voyageur]] canoe during the fur trade era]]
In 1688 the [[France|French]] explorer [[Jacques de Noyon]] became the first European known to have traveled through the BWCAW area. Later, during the 1730s, [[Pierre Gaultier de Varennes, sieur de La Vérendrye|La Vérendrye]] and others opened the region to trade, mainly in beaver pelts. By the end of the 18th century, the [[fur trade]] had been organized into groups of canoe-paddling [[voyageurs]] working for the competing [[North West Company|North West]] and [[Hudson's Bay Company|Hudson's Bay]] Companies, with a North West Company fort located at the [[Grand Portage National Monument|Grand Portage]] on Lake Superior. The final [[Voyageurs#Rendezvous|rendezvous]] was held at Grand Portage in 1803, after which the North West Company moved its operations further north to Fort William (now [[Thunder Bay]]). In 1821 the North West Company merged with the Hudson's Bay Company and the center of the fur trade moved even further north to the posts around Hudson Bay.<ref name=searle/>{{rp|5–8}}<ref name = "CanoeRoutesMorse">{{cite book|last1=Morse|first1=Eric W.|title=Fur Trade Canoe Routes of Canada/Then and Now|date=1969|publisher=Queen's Printer and Controller of Stationery|location=Ottawa, Canada|url=http://parkscanadahistory.com/publications/fur-trade-canoe-routes.pdf}}</ref>

During the late 18th and early 19th centuries, the area's legal and political status was disputed. The [[Treaty of Paris (1783)|Treaty of Paris]], which ended the [[American Revolutionary War]] in 1783, had defined the northern border between the United States and Canada based on the inaccurate [[Mitchell Map]]. Ownership of the area between [[Lake of the Woods]] and Lake Superior was unclear, with the United States claiming the border was further north at the [[Kaministiquia River]] and Canada claiming it was further south beginning at the [[Saint Louis River]].<ref>Lass, William E. (1980). ''Minnesota's Boundary with Canada'', pp. 47-53.  Minnesota Historical Society, ISBN 0-87351-153-0.</ref> In 1842, the [[Webster–Ashburton Treaty]] clarified the border between the United States and Canada using the old trading route running along the [[Pigeon River (Minnesota–Ontario)|Pigeon River]] and [[Rainy River (Minnesota–Ontario)|Rainy River]] (today the BWCAW's northern border).<ref name=searle/>{{rp|8–9}}

The BWCAW area remained largely undeveloped until gold, silver and iron were found in the surrounding area during the 1870s, 1880s and 1890s. Logging in the area began around the same time to supply lumber to support the mining industries, with production peaking in the late 1910s and gradually trailing off during the 1920s and 1930s.<ref name=searle/>{{rp|9–12}}

=== Protection ===
In 1902, Minnesota's Forest Commissioner [[Christopher C. Andrews]] persuaded the state to reserve {{convert|500000|acres|km2}} of land near the BWCAW from being sold to loggers. In 1905 he visited the area on a canoe trip and was impressed by the area's natural beauty. He was able to save another {{convert|141000|acres|km2}} from being sold for development. He soon reached out to the Ontario government to encourage them to preserve some of the area's land on their side of the border, noting that the area could be "an international forest reserve and park of very great beauty and interest". This collaboration led to the creation of the Superior National Forest and the Quetico Provincial Park in 1909.<ref name=searle/>{{rp|15–16}}

The BWCAW itself was formed gradually through a series of actions. By the early 1920s, roads had begun to be built through the Superior National Forest to promote public access to the area for recreation. In 1926 a section of {{convert|640000|acres|km2}} within the Superior National Forest was set aside as a roadless wilderness area by Secretary of Agriculture [[William Marion Jardine]]. This area became the nucleus of the BWCAW. In 1930, Congress passed the Shipstead-Newton-Nolan Act, which prohibited logging and dams within the area to preserve its natural water levels. Through additional land purchases and shifts in boundaries, the amount of protected land owned by the government in the area grew even further. In 1938, the area's borders were expanded and altered (roughly matching those of the present day BWCAW), and it was renamed the Superior Roadless Primitive Area.<ref name="bwcawhistory">{{cite web|title=History of the BWCAW|url=http://www.fs.usda.gov/detail/superior/specialplaces/?cid=stelprdb5127455|website=United States Department of Agriculture – Forest Service}}</ref><ref name="bwcahistory2">{{cite web |url=http://www.wilbers.com/BoundaryWatersCanoeAreaWildernessChronologyWelcome.htm |title=Boundary Waters Chronology |last=Wilbers |first=Stephen |accessdate=27 September 2009}}</ref>

Additional laws focused on protecting the area's rustic and undeveloped character. In 1948, the Thye-Blatnik Bill authorized the government to purchase the few remaining privately owned homes and resorts within the area. In 1949, President [[Harry Truman]] signed Executive Order 10092 which prohibited aircraft from flying over the area below 4,000 feet. The area was officially named the Boundary Waters Canoe Area in 1958. The [[Wilderness Act]] of 1964 organized it as a unit of the [[National Wilderness Preservation System]]. The 1978 [[Boundary Waters Canoe Area Wilderness Act]] established the Boundary Waters regulations much as they are today, with limitations on [[motorboat]]s and [[snowmobile]]s, a permit-based quota system for recreational access, and restrictions on logging and mining within the area.<ref name="bwcawhistory"/><ref name="bwcahistory2"/>

===Land use disputes===

Some aspects of the BWCAW's management and conservation have been controversial. During the 1970s, a legislative proposal was made to loosen restrictions on motorized vehicles and allow some logging in parts of the BWCAW. The proposal was unsuccessful. During the mid-1990s, a dispute over using trucks to move boats between two portages required mediation. In 1998, using trucks was ultimately allowed. While snowmobiles are not allowed within the BWCAW, a snowmobile trail located 400 feet from the border provoked a lawsuit in 2006. In 2015, a judge ruled that the snowmobile trail did not violate the Wilderness Act.<ref>{{cite web|last1=Kelleher|first1=Bob|title=The Boundary Waters: 25 years later|url=http://news.minnesota.publicradio.org/features/2003/10/21_kelleherb_bwca/|website=Minnesota Public Radio}}</ref><ref>{{cite news|last1=Furst|first1=Randy|title=Judge rules in favor of proposed snowmobile route bordering BWCA|url=http://www.startribune.com/judge-allows-planned-snowmobile-route-bordering-the-bwca/291876051/|work=Minneapolis Star-Tribune|date=14 February 2015}}</ref>

Renewed interest in copper and nickel mining in northern Minnesota has also been a source of tension. While the mines would be situated south and west of the BWCAW, concerns about [[Surface runoff|runoff]] that could ultimately enter the BWCAW's watershed have caused concerns among environmental and conservationist groups. A 2015 bill proposed by Representative [[Betty McCollum]] seeks to block nearby mining.<ref>{{cite web|last1=Hemphill|first1=Stephanie|title=At the edge of the Boundary Waters, miners probe for copper, nickel|url=http://www.mprnews.org/story/2010/06/23/boundary-waters-mining|website=Minnesota Public Radio}}</ref><ref>{{cite news|last1=Meyers|first1=John|title=St. Paul representative's bill would restrict mining in BWCAW watershed|url=http://www.duluthnewstribune.com/news/politics/3722762-st-paul-representatives-bill-would-restrict-mining-bwcaw-watershed|work=Duluth News Tribune|date=14 April 2015}}</ref>

== Recreation ==
[[File:Rainy River.jpg|thumb|right|Canoe campers on a trip in the BWCAW]]
The BWCAW attracts approximately 250,000 visitors per year, making it one of the most visited wilderness areas in the United States.<ref name=tripguide/>{{rp|10}} It contains more than 2,000 backcountry campsites, {{convert|1200|miles|km}} of canoe routes, and 12 different hiking trails and is popular for [[canoeing]], [[canoe camping|canoe touring]], [[fishing]], [[backpacking (wilderness)|backpacking]], [[mushing|dog sledding]], and enjoying the area's remote [[wilderness]] character.<ref name=tripguide/>

Permits are required for all overnight visits to the BWCAW. Quota permits are required for groups taking an overnight paddle, motor, or hiking trip, or a motorized day-use trip into the BWCAW from May 1 through September 30. These permits must be reserved in advance. Day use paddle and hiking permits do not require advance reservation and can be filled out at BWCAW entry points. From October 1 through April 30, permit reservations are not necessary, but a permit must be filled out at the permit stations located at each entry point. Each permit must specify the trip leader, the specific entry point and the day of entry. The permits are for an indefinite length, although visitors are only allowed one entry into the wilderness and cannot stay in one campsite for more than 14 nights.<ref name=tripguide/>

=== Canoeing ===
[[File:Bwca-and-wooden-canoe.jpg|thumb|left|A BWCA paddler with her wood-and-canvas canoe]]
Canoeing or other non-motorized boating is the most popular method of exploring the BWCAW. A 2007 study found more than 94% of overnight visitors used a non-motorized boat to travel through the park.<ref name=2012report>{{cite web|last1=Dvorak|first1=Robert G.|last2=Watson|first2=Alan E.|last3=Christensen|first3=Neal|last4=Borrie|first4=William T.|last5=Schwaller|first5=Ann|title=The Boundary Waters Canoe Area Wilderness: Examining Changes in Use, Users, and Management Challenges|url=http://www.fs.fed.us/rm/pubs/rmrs_rp091.pdf|website=United States Department of Agriculture – Forest Service|page=11|format=pdf}}</ref> The BWCAW's size and abundance of campsites, lakes, rivers and [[portage]] trails allow for almost countless options for different routes. Many online maps and guidebooks offer suggested routes based on entry point, duration and difficulty.<ref>{{cite book|last1=Pauly|first1=Daniel|title=Exploring the Boundary Waters|date=2005|publisher=University of Minnesota Press|isbn=978-1-4529-0646-1}}</ref><ref>{{cite book|last1=Beymer|first1=Robert|last2=Dzierzak|first2=Louis|title=Boundary Waters Canoe Area: Western Region|date=2009|publisher=Wilderness Press|isbn=978-0-89997-610-5}}</ref><ref>{{cite book|last1=Beymer|first1=Robert|last2=Dzierzak|first2=Louis|title=Boundary Waters Canoe Area: Eastern Region|date=2009|publisher=Wilderness Press|isbn=978-0-89997-461-3}}</ref>

=== Fishing ===
[[File:EagleMountainTrail.jpg|thumb|Junction of the Eagle Mountain and Brule Lake Trails]]
Fishing is a popular activity in the BWCAW. Game species include [[northern pike]], [[muskellunge]], [[walleye]], [[largemouth bass|largemouth]] and [[smallmouth bass]], and [[panfish]]. Trout including [[brook trout]], [[brown trout]], [[lake trout]], [[rainbow trout]] and [[splake]] are also found. Limited stocking of walleye, brown trout and lake trout is done on some lakes.<ref name=tripguide/>

=== Hiking ===
The BWCAW contains a variety of hiking trails. Shorter hikes include the trail to Eagle Mountain ({{convert|7|mi|km}}). Loop trails include the [[Pow Wow Trail]], the [[Snowbank Trail]], and the [[Sioux-Hustler Trail]]. The [[Border Route Trail]] and [[Kekekabic Trail]] are the two longest trails running through the BWCAW. The Border Route Trail runs east-west for over {{convert|65|mi|km}} through the eastern BWCAW, beginning at the northern end of the [[Superior Hiking Trail]] and following ridges and cliffs west until it connects with the [[Kekekabic Trail]]. The Kekekabic Trail continues for another {{convert|41|mi|km}}, beginning near the Gunflint Trail and passing through the center of the BWCAW before exiting it near Snowbank Lake. Both the Border Route and the Kekekabic Trail are part of the longer [[North Country National Scenic Trail]].<ref>{{cite web|title=BWCA Trails|url=http://www.canoecountry.com/hike/|website=CanoeCountry.com}}</ref><ref>{{cite web|title=Border Route Trail Association|url=https://northcountrytrail.org/trail/states/minnesota/explore-by-section/border-route-trail-association/|website=North Country Trail Association}}</ref><ref>{{cite web|title=Kekekabic Trail Chapter|url=https://northcountrytrail.org/trail/states/minnesota/explore-by-section/kekekabic-trail-club/|website=North Country Trail Association}}</ref>

==Notable people associated with the BWCAW==
* [[Ernest Oberholtzer]], is recognized today as a leading advocate for the preservation of the Quetico-Superior lake area and what would become the BWCA.
* [[Sigurd Olson]], Minnesota author and [[conservation movement|conservationist]], wrote extensively about the Boundary Waters and worked to ensure preservation of the wilderness.
* [[Dorothy Molter]], known as the "Rootbeer Lady", lived in the BWCAW for 56 years (alone after 1948) until her death in 1986, and was the last resident of the BWCA.
* Benny Ambrose lived alone on Ottertrack Lake until his death in 1982, leaving Dorothy Molter as the last remaining full-time resident.

== See also ==
{{col-begin}}
{{col-break|width=50%}}
* [[Canoe]]
* [[List of U.S. Wilderness Areas]]
* [[Superior National Forest]]
* [[Boundary Waters-Canadian Derecho]]
* [[Hegman Lake Pictograph]]
{{col-break|width=50%}}
* The [[Boundary Waters]] region
** [[Voyageurs National Park]]
** [[Quetico Provincial Park]]
** [[La Verendrye Provincial Park]]
* [[North Shore (Lake Superior)]]
** [[Grand Portage National Monument]]
{{col-end}}

== References ==
{{Reflist|25em}}

== External links ==
{{commons category|Boundary Waters Canoe Area Wilderness}}
* [http://www.fs.fed.us/r9/forests/superior/bwcaw/ Superior National Forest: BWCAW]
* [http://www.wilderness.net/index.cfm?fuse=NWPS&sec=wildView&WID=70&tab=General Wilderness.net: BWCAW]
* [http://www.friends-bwca.org/ Friends of the Boundary Waters Wilderness (political advocacy)]
* [http://www.bwca.com/ Boundary Waters Canoe Area Information Resource]
* [http://www.mnopedia.org/place/boundary-waters-canoe-area-wilderness-bwcaw MNopedia: BWCAW]

== Further reading ==
* Fox, Porter, [https://www.nytimes.com/2016/10/23/travel/boundary-waters-minnesota-canada-into-the-wild.html?_r=0 "On the Water, and into the Wild"].  [[The New York Times]], October 21, 2016, p.&nbsp;1.
* Proescholdt, Kevin; Rapson, Rip: and Heinselman, Miron L. (1995).  ''Troubled Waters''.  North Star Press of St. Cloud.  ISBN 0 87839 100 2.

{{Protected areas of Minnesota}}
{{Canoeing and kayaking}}

[[Category:Canoeing and kayaking venues in the United States]]
[[Category:Protected areas of Cook County, Minnesota]]
[[Category:Protected areas of Lake County, Minnesota]]
[[Category:Protected areas of St. Louis County, Minnesota]]
[[Category:Wilderness Areas of Minnesota]]
[[Category:Protected areas established in 1964]]
[[Category:Superior National Forest]]