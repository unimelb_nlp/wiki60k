{{Infobox publisher
| image        = [[Image:UCPRESS LOGO.png|220px|University of California Press]]
| parent       = [[University of California]]
| status       = 
| founded      = 1893
| founder      = 
| successor    = 
| country      = [[United States]]
| headquarters = [[Oakland, California|Oakland]], [[California]]
| distribution = 
| keypeople    = 
| publications = [[Book]]s, [[Academic journal|journal]]s
| topics       = 
| genre        = 
| imprints     = 
| revenue      = 
| numemployees = 
| nasdaq       = 
| url          = {{URL|http:/ucpress.edu}}
}}
[[File:ASA conference 2008 - 19.JPG|thumb|2008 conference booth]]
'''University of California Press''', otherwise known as '''UC Press''', is a [[publishing house]] associated with the [[University of California]] that engages in [[academic publishing]]. It was founded in 1893<ref name="Camhi2013">{{cite book|first=Jeff|last=Camhi|title=A Dam in the River: Releasing the Flow of University Ideas|url=https://books.google.com/books?id=3qodTxXkEgEC&pg=PA149|accessdate=31 August 2013|date=15 April 2013|publisher=Algora Publishing|isbn=978-0-87586-989-6|pages=149–}}</ref> to publish books and papers for the faculty of the University of California, established 25 years earlier in 1868. Its headquarters are located in [[Oakland, California]].

The University of California Press publishes in the following general subject areas: anthropology, art, California and the West, classical studies, film, food and wine, global issues, history, literature/poetry, music, natural sciences, public health and medicine, religion, and sociology. It also distributes titles published by the Huntington Library, Watershed Media, and publishing programs within the University of California system.

Each year it publishes approximately 180 new books and 31 journals in the humanities, social sciences, and natural sciences and keeps about 3,500 book titles in print.{{citation needed|date=August 2013}}

The Press commissioned as its corporate typeface [[University of California Old Style]] from type designer [[Frederic Goudy]] from 1936-8, although it no longer always uses the design.<ref name="A Half-Century of Type Design and Typography: 1895-1945, Volume 2">{{cite book|last1=Goudy|first1=Frederic|author-link=Frederic Goudy|title=A Half-Century of Type Design and Typography: 1895-1945, Volume 1|date=1946|publisher=The Typophiles|location=New York|pages=216–9|url=https://archive.org/details/GoudyHalfCentury1946V2|accessdate=26 February 2016}}</ref><ref name="Carter">{{cite web|last1=Carter|first1=Matthew|title=Goudy, the good ol’ boy (Bruckner biography review)|url=http://www.eyemagazine.com/review/article/goudy-the-good-ol-boy|website=Eye Magazine|accessdate=5 February 2016}}</ref><ref name="Shaw on Goudy">{{cite web|last1=Shaw|first1=Paul|title=An appreciation of Frederic W. Goudy as a type designer|url=http://www.paulshawletterdesign.com/2014/03/an-appreciation-of-frederic-w-goudy-as-a-type-designer/|accessdate=12 July 2015}}</ref><ref name="A Bull in the Typography Shop">{{cite web|last1=Updike|first1=John|author-link=John Updike|title=A Bull in the Typography Shop: a review of ''Frederic Goudy'' by D. J. R. Bruckner|url=https://www.nytimes.com/1990/12/16/books/a-bull-in-the-typography-shop.html|website=[[New York Times]]|accessdate=5 February 2016}}</ref>
 
== Notable books ==
*''[[Language As Symbolic Action]]'', [[Kenneth Burke]] (1966)
*''[[The Teachings of Don Juan|The Teachings of Don Juan: A Yaqui Way of Knowledge]]'', [[Carlos Castaneda]] (1968)
*''[[The Mysterious Stranger]]'', [[Mark Twain]] (definitive edition) (1969, based on work first published in 1916)
*''[[Basic Color Terms: Their Universality and Evolution]]''
*''[[The Making of a Counter Culture]]''
*''[[Self-Consuming Artifacts|Self-Consuming Artifacts: The Experience of Seventeenth-Century Literature]]'', [[Stanley Fish]] (1972)
*''[[The Ancient Economy (book)|The Ancient Economy]]'', [[Moses I. Finley]] (1973)
*''[[Joan of Arc: The Image of Female Heroism]]'', [[Marina Warner]] (1981)
*''[[Strong Democracy|Strong Democracy: Participatory Politics for a New Age]]'', [[Benjamin R. Barber]] (1984)
*''[[Art in the San Francisco Bay Area (book)|Art in the San Francisco Bay Area]]'', Thomas Albright (1985)
*''[[Religious Experience (book)|Religious Experience]]'', [[Wayne Proudfoot]] (1985)
*''[[The War Within: America's Battle over Vietnam]]'', [[Tom Wells (author)|Tom Wells]] (1994)
*''[[A Little Yes and a Big No|George Grosz: An Autobiography]]'', [[George Grosz]] (translated by Nora Hodges) (published 1998, written in 1946, translated in 1955)
*''[[The Confusions of Pleasure: Commerce and Culture in Ming China]]''
*''[[Disposable People: New Slavery in the Global Economy]]'', [[Kevin Bales]] (1999)
*''[[Mama Lola|Mama Lola: A Vodou Priestess in Brooklyn]]'', [[Karen McCarthy Brown]] (2001)
*''[[A Culture of Conspiracy: Apocalyptic Visions in Contemporary America]]'', [[Michael Barkun]] (2003)
*''[[Bounded Choice]]''
*''[[Beyond Chutzpah|Beyond Chutzpah: On the Misuse of Anti-Semitism and the Abuse of History]]'', [[Norman G. Finkelstein]] (2005)
*''[[Brewing Justice: Fair Trade Coffee, Sustainability and Survival]]''
*''[[China Candid]]''
*''[[Autobiography of Mark Twain]]'', [[Mark Twain]] (2010)

== Notable series ==
The University of California Press re-printed a number of novels under the ''California Fiction'' series from 1996-2001. These titles were selected for their literary merit and for their illumination of California history and culture.<ref>{{cite book|last1=See|first1=Carolyn|title=Golden Days|date=1996|publisher=University of California Press|location=Berkeley|isbn=0520206738}}</ref><ref>{{cite web|title=California Fiction|url=http://www.ucpress.edu/series.php?ser=cf&s=pd&o=desc&r=10&page=1|website=University of California Press|accessdate=14 October 2015}}</ref> 
*''The Ford'' by [[Mary Hunter Austin|Mary Austin]]
*''Thieves' Market'' by [[A.I. Bezzerides]]
*''Disobedience'' by Michael Drinkard
*''Words of My Roaring'' by Ernest J. Finney
*''Skin Deep'' by [[Guy Garcia]]
*''Fat City'' by Leonard Gardiner
*''Chez Chance'' by Jay Gummerman
*''Continental Drift'' by [[James D. Houston]]
*''The Vineyard'' by [[Idwal Jones (academic)|Idwal Jones]]
*''In the Heart of the Valley of Love'' by [[Cynthia Kadohata]]
*''[[Always Coming Home]]'' by [[Ursula K. Le Guin]]
*''[[The Valley of the Moon (novel)|The Valley of the Moon]]'' by [[Jack London]]
*''Home and Away'' by [[Joanne Meschery]]
*''Bright Web in the Darkness'' by [[Alexander Saxton]]
*''Golden Days'' by [[Carolyn See]]
*''[[Oil!]]'' by [[Upton Sinclair]]
*''Understand This'' by Jervey Tervalon
*''Ghost Woman'' by [[Lawrence Thornton]]
*''Who is Angelina?'' by [[Al Young]]

==References==
{{Reflist}}

==External links==
{{Portal|Books|California}}
* [http://www.ucpress.edu/ Official '''University of California Press''' website]
* [http://www.cdlib.org/ California Digital Library (CDL)] - [[University of California Libraries]]
* [http://publishing.cdlib.org/ucpressebooks/search?facet=subject;rights=Public Free Online - UC Press E-Books Collection]
* [http://www.marktwainproject.org/ Mark Twain Project Online]

{{University of California|state=collapsed}}
{{Authority control}}

[[Category:University of California Press| ]]
[[Category:University of California|Press]]
[[Category:Book publishing companies based in California]]
[[Category:Publishing companies based in Berkeley, California]]
[[Category:University presses of the United States|California]]
[[Category:Publishing companies established in 1893]]
[[Category:1893 establishments in California]]