{{primary sources|date=March 2015}}
{{Use mdy dates|date=March 2015}}

{{Infobox company
| type = Private
| name = Clusterpoint Ltd.
| logo = 
| industry = [[enterprise software]]<br>[[database software]]<br>[[cloud computing]]
| location_city = London
| location_country = United Kingdom
| foundation = August 21, 2006
| founder = [https://www.linkedin.com/profile/view?id=46998920 Gints Ernestsons]<br>[https://www.linkedin.com/in/jurgisorups Jurgis Orups]<br>[https://www.linkedin.com/profile/view?id=23124571 Zigmars Rasscevskis]<br>Oskars Viksna
| products = [https://www.clusterpoint.com/product/instantly-scalable Clusterpoint DBMS]<br>[https://www.clusterpoint.com/pricing#cloud-version Clusterpoint DBaaS]<br>[https://www.clusterpoint.com/for-business/ntss-net-security NTSS]<br>[https://www.clusterpoint.com/for-business/gol-log-analytics GOL]
| website = {{URL|http://www.clusterpoint.com/}}
}}
{{Infobox software
| name = Clusterpoint Database
| screenshot =
| caption =
| author =
| developer = Clusterpoint Ltd.
| released = 2006
| status = Active
| latest release version = 4.0
| latest release date = {{release date|2015|10|08}}
| latest preview version =
| latest preview date =
| frequently updated = yes
| programming language = [[C (programming language)|C]], [[C++]]
| operating system = [[Cross-platform]]
| language = English
| genre = [[distributed database]]<br>[[enterprise search]]<br>[[operational database]]<br>[[document-oriented database|document-oriented]]<br>[[NoSQL]], [[XML]], [[JSON]], [[SQL]] [[database]]<br>[[cloud database|cloud DBAAS]]
| license = free-commercial
}}

Clusterpoint is a European software technology company developing and supporting '''Clusterpoint database''' management system platform.
<ref name="Companies House Information">{{cite web|url= http://www.endole.co.uk/company/09366103/clusterpoint-group-limited |title=Clusterpoint Group Limited |publisher=Companies House (UK) |date= |accessdate=March 5, 2015}}</ref><ref name="Lursoft Information">{{cite web|url= http://company.lursoft.lv/en/clusterpoint/40003850104 |title=Clusterpoint Development Center |publisher=Lursoft (LV) |date= |accessdate=March 5, 2015}}</ref><ref name="Firmas.lv Information">{{cite web|url= http://www.firmas.lv/profile/clusterpoint-sia/000385010 |title=Clusterpoint Profile on Firmas.lv |publisher=Firmas.lv (LV) |date= |accessdate=March 5, 2015}}</ref>

Company was founded by software engineers.<ref name="Bring the Power of Big Data to Small Businesses">{{cite web|url=http://data-informed.com/bring-the-power-of-big-data-to-small-businesses/ |title=Bring the Power of Big Data to Small Businesses |publisher=Data-Informed.com (US) |date= |accessdate=July 1, 2015}}</ref>  Company is venture capital backed.
<ref name="Imprimatur Capital London">{{cite web|url=http://www.imprimaturcapital.com/portfolio/portfolio-companies-(imprimatur-capital)/clusterpoint/ |title=Imprimatur Capital About Clusterpoint |publisher=Imprimatur Capital |date= |accessdate=March 9, 2015}}</ref>
<ref name="privateequitywire">{{cite web|url=http://www.privateequitywire.co.uk/2012/04/12/164993/clusterpoint-raises-eur1-million-baltcap |title=Clusterpoint Raises EUR1 Million From BaltCap|publisher=Privateequitywire |date= |accessdate=June 14, 2013}}</ref><ref name="arcticstartup 1">{{cite web|url=http://www.arcticstartup.com/2012/02/10/clusterpoint-baltcap-funding |title=Clusterpoint Receives €1 Million From BaltCap|publisher=Arcticstartup.com |date= |accessdate=June 14, 2013}}</ref>
<ref name="arcticstartup 2">{{cite web|url=http://arcticstartup.com/article/latvian-database-platform-clusterpoint-secures-e1-25-million/ |title=Latvian Database Platform Clusterpoint secures 1.25 million|publisher=Arcticstartup.com |date= |accessdate=March 16, 2015}}</ref>

Clusterpoint is a schema-free [[document database]] that removes complexity, scalability problems and performance limitations of [[relational database management system|relational database architecture]].<ref name="Clusterpoint 4 Computing Engine Combines Instantly Scalable Database and Computational Power">{{cite web|url=http://insidebigdata.com/2015/10/06/clusterpoint-4-computing-engine-combines-instantly-scalable-database-and-computational-power/ |title=Clusterpoint 4 Computing Engine Combines Instantly Scalable Database and Computational Power |publisher=InsideBigdata.com (US) |date= |accessdate=October 6, 2015}}</ref>

'''Clusterpoint database''' eliminates customer integration efforts among database, search and analytics platforms.  Clusterpoint database replaces integrated multi-platform solutions with a single-platform and one-[[API]] solution, typically, where [[SQL]] [[RDBMS]] data is used in combination with an [[enterprise search]] engine to address performance and scalability needs of web and mobile applications, or where [[Big data]] and [[analytics]] tools such as [[Hadoop]] might be needed due to sheer volume of data or large computing workloads.<ref name="Clusterpoint launches document database">{{cite web|url=http://www.infoworld.com/article/2940140/application-development/clusterpoint-launches-document-database-as-a-service.html |title=A new document database emerges from the cloud |publisher=infoworld.com (US) |date= |accessdate=June 25, 2015}}</ref>

The first version of the Clusterpoint database was released in 2006. The most recent Clusterpoint version 4 includes [[JavaScript]] computing engine and JS/SQL [[query language]], it was released in October, 2015.<ref name="Clusterpoint adds computation to NoSQL database engine">{{cite web|url=http://siliconangle.com/blog/2015/10/08/clusterpoint-adds-computation-to-nosql-database-engine/ |title=Clusterpoint adds computation to NoSQL database engine |publisher=SiliconAngle.com (US) |date= |accessdate=October 8, 2015}}</ref>

'''Clusterpoint database''' is a [[document-oriented database|document-oriented]] [[database server]] platform for storage and processing of [[XML]] and [[JSON]] data in a distributed fashion on large clusters of commodity hardware. Database architecture blends [[ACID]]-compliant [[OLTP]] transactions, [[full-text search]] and analytics in the same code, delivering [[high availability cluster|high availability]], [[fault-tolerance]], [[data replication]] and security.<ref name="nosql-database">{{cite web|url= http://nosql-database.org |title=List of NOSQL Databases |publisher=Nosql-database.org |date= |accessdate=March 9, 2015}}</ref><ref name="the-nosql-movement">{{cite web|url=http://www.dataversity.net/the-nosql-movement-document-databases/ |title=The NoSQL movement: document databases |publisher=Dataversity |date= |accessdate=June 14, 2013}}</ref>

'''Clusterpoint database''' enables to perform [[transaction processing|transactions]] in a distributed document database model in the same way as in a [[SQL]] database.  Users can perform secure real-time updates, free text search, analytical SQL querying and reporting at high velocity in [[very large database|very large distributed databases]] containing XML or JSON document type data. [[Transaction processing|Transactions]] are implemented without database consistency issues plaguing most of [[NoSQL]] databases and can safely run at high-performance speed previously available only with [[relational databases]].<ref name="big-data-startup">{{cite web|url=http://www.bigdata-startups.com/open-source-tools/document-store/ |title=Big data startups / document stores |publisher=Bigdata-startups.com |date= |accessdate=June 14, 2013}}</ref> [[Real-time computing|Real time]] [[Big data]] analytics, replication, loadsharing and high-availability are standard features of Clusterpoint database software platform.<ref name="Technology Behind Clusterpoint Database">{{cite web|url=http://www.slideshare.net/clusterpoint/technical-aspects-of-technology-behind-clusterpoint |title=Technology Behind Clusterpoint Database|publisher=Gints Ernestsons, Founder |date= |accessdate=March 9, 2015}}</ref>

'''Clusterpoint database''' enables web-style free text search with natural language keywords and programmable relevance sorting of results. Constant and predictable search response time with [[latency (engineering)|latency]] in milliseconds and high quality of search results are achieved using policy-based inverted indexation and unique relevance ranking method. Clusterpoint database version 4 supports JS/SQL query language.  Classic SQL queries can be combined with free text search and with custom [[distributed computing]] functions written in [[JavaScript]], executed in a single [[REST API]] call.<ref name="fulltext-search-engines">{{cite web|url=http://www.mediawiki.org/wiki/Fulltext_search_engines |title=Fulltext search engines |publisher=Mediawiki.org |date=|accessdate=June 14, 2013}}</ref>

For most of its history Clusterpoint was servicing business customers as an [[enterprise software]] vendor.<ref name="Bloomberg clusterpoint Profile">{{cite web|url=https://www.bloomberg.com/research/stocks/private/snapshot.asp?privcapId=83976143 |title=Bloomberg Company Research Profile |publisher=Bloomberg.com |date= |accessdate=March 9, 2015}}</ref><ref name="Crunchbase Clusterpoint Profile">{{cite web|url=http://www.crunchbase.com/company/clusterpoint |title=Crunchbase Clusterpoint Profile |publisher=Crunchbase.com |date= |accessdate=June 14, 2013}}</ref><ref name="businessweek">{{cite web|url=http://investing.businessweek.com/research/stocks/private/snapshot.asp?privcapId=83976143 |title=BusinessWeek Clusterpoint Profile |publisher=Businessweek |date= |accessdate=June 14, 2013}}</ref>

==Use cases==
Clusterpoint database delivers real-time business information management in electronic XML or JSON document format.  It can be used as a high-performance [[operational database]] for web and mobile database services requiring scalability, fast speed and strong security.  Software enables to safely handle financial, billing, security, medical, travel, information services, e-commerce, government and municipal open data and other data stored in electronic document data format that uses industry standard XML and JSON markup.<ref name="Business Directory Use Case">{{cite web|url= http://www.yellowsearchtoday.eu/de/portfolio_item/drinks-sponsor-conference-2014/ |title=Business Directory Use Case |publisher=Yellow Search Today |date= |accessdate=March 4, 2015}}</ref><ref name="Clusterpoint Use Case E-commerce">{{cite web|url= http://www.exim.lv/clusterpoint |title=Clusterpoint Use Case In E-commerce |publisher=Exim.lv |date= |accessdate=March 4, 2015}}</ref><ref name="Open Public Data">{{cite web|url= http://garage48.org/events/riga-open-data |title=Open Data and Public Services 2015 |publisher=Garage48 Foundation |date= |accessdate=March 9, 2015}}</ref>

Generic database use cases can also be where flexible XML or JSON document data model commonly fits best: processing mix of variable data, including structured data, [[unstructured data]] (textual), [[semi-structured data]] and [[binary large object|blob]]s such as images, voice, video files. Software can be used for computing tasks requiring low millisecond-range latency data processing services in distributed databases, for instance, to feed data at high speed to interactive NoSQL visualizations, Big data online analytics and safe reporting in large databases.<ref name="Clusterpoint and Zoomcharts">{{cite web|url= https://www.youtube.com/watch?v=3rGquJr9sAw |title=Clusterpoint and ZoomCharts |publisher=Zoomcharts.com |date= |accessdate=March 4, 2015}}</ref>

==Distinctive technology==

===High-speed ACID-compliant Transactions in Distributed Document Database===
Clusterpoint database provides distributed, [[ACID]]-compliant transactions, including basic SQL support, in a document model database that is massively scalable for [[Big data]] volumes.  Distributed transactions, data storage, search and analytics can be performed at high performance and high availability, while delivering strong database consistency and security.  It gives Clusterpoint performance and scalability advantage over other [[NoSQL]] document databases, that are compromising on security and integrity of customer data, typically providing only limited [[eventual consistency]] at high availability.<ref name="High-performance ACID-compliant Transactions in Document Database">{{cite web|url=https://www.youtube.com/watch?v=Gx4RdmbJG_0 |title=Developers Club NoSQL Meetup with Clusterpoint |publisher=Dev Club Riga |date= |accessdate=April 16, 2015}}</ref>

===Programmable database ranking for search relevance in Big data===
Another distinction is programmable [[ranking]] index, that can be flexibly customized through relevance rules assigned in the Document Policy configuration file.  It is a small XML configuration file accompanying each Clusterpoint database.  Database search behavior can be quickly changed through configuring of ranking index rules vs modifying software code.  The increasing importance of ranking is directly derived from the explosion in the volume of data handled by current applications. The user would be overwhelmed by too many unranked results. Furthermore, the sheer amount of data makes it almost impossible to process queries in the traditional compute-then-sort approach.
Customer application software code can be simplified by delegating most indexing and search sorting details, including ranking algorithms, to the Document policy configuration attributes in Clusterpoint database.  Document policy, when customized for a particular web or mobile application need, determines the particular ranking index organization at the physical storage level by presorting the actual index data for custom [[relevance (information retrieval)|relevance]] algorithms.  Developers can avoid most of complex SQL programming for data sorting and grouping in their application software code, while database hardware can be liberated from the excessive [[Big data]] sorting per each database query.  Instead the Clusterpoint database ranking index delivers fast search and relevance sorting functionality, without performance degradation characteristic to relational SQL databases.

Ranking index method, applied to document database model, enables Clusterpoint to outperform [[SQL]] databases at search by several orders of magnitude.  It solves information overload and latency problem for interactive web and mobile applications processing Big data.  Today limited-size mobile device screens and network bandwidth restrictions prevent users requesting and processing large size data volumes per each query. Database search and querying need to be interactive and transactional to satisfy Internet users.  Clusterpoint ranking index was designed for this computing model.  It extracts relevant data first and returns information page by page in decreasing relevance. For instance, using only free text search, latency in large databases containing billions of document will be milliseconds, while relevance ranking will prevent overwhelming end-user with too much low-quality search results. This is also a crucial design element for distributed document database architecture: it makes its index scalable so that it can be safely shared across large cluster of servers without ignificant performance loss at data injection, free text search and access.<ref name="Big data analytics today">{{cite web|url= http://www.bigdataanalyticstoday.com/top-nosql-document-databases/ |title=Top NOSQL document databases |publisher=Big Data Analytics Today |date= |accessdate=March 9, 2015}}</ref>

Additionally Clusterpoint ranking index can be fine-tuned by developers to match the natural language terms in queries to the most relevant textual data content in a customer database.  When querying a distributed database with free text format keywords in natural language or with phrases, ranking index sorts out the best [[relevance|relevant documents]] where query is matching textual content parts in the database, taking into account natural language density, word statistics and language-specific grammatics attributes (incl. stemming, spelling, collation), performing automatic self merged joins.  Very few database products support similar type of self-merge joins.<ref name="Selfe-merge joins">{{cite web|url= https://code.google.com/p/guestbook-example-appengine-full-text-search/wiki/HowTo |title=How to make a Google App Engine application searchable using self merge joins |publisher=Google, Inc. |date= |accessdate=March 9, 2015}}</ref>

Adjusting ranking rules, customers can configure various grouping, ordering and positioning algorithms for their search results through the ranking index so that it starts delivering the best end-user search experience.  A set of ranking configuration rules, once established for a particular database, is then being applied and maintained automatically by Clusterpoint database when customer data is loaded or updated through Clusterpoint database [[CRUD]] [[API]] commands.

Developers can freely use [[full text search]] as the fastest information access method in Clusterpoint databases, while having capability to flexibly query the database structure with standard analytics using SQL.  In Clusterpoint database both methods can be combined in a single query, enabling combined analytical and search queries in mixed structured and unstructured data content.

==Clusterpoint database deployments==
Clusterpoint database is used in production deployments of enterprise customers operating their 24/7 web and mobile services from 2006. Vendor has built partnerships that provide solutions in different industry sectors, such as:
* Governance, Risk Management and Regulatory Compliance<ref name="InfoGov">{{cite web|url= http://www.infogov.co.uk/ |title=Infogov Proteus iGRC (Internet Governance and Regulatory Compliance) |publisher=Infogov Ltd (United Kingdom) |date= |accessdate=March 9, 2015}}</ref>
* Agile Web Software Development<ref name="Agile Toolkit">{{cite web|url= http://www.agiletoolkit.org/intro/3 |title=Agile Web Software Development |publisher=Agile.org |date= |accessdate=March 9, 2015}}</ref>
* Online Business Intelligence in NoSQL and Big Data<ref name="Ambienttech">{{cite web|url= http://www.ambienttech.lv/ |title=Turbocharge HTML5 web applications |publisher=Ambienttech |date= |accessdate=March 9, 2015}}</ref>
* Cloud Computing Services
* Web Site Design<ref name="Rixtellab">{{cite web|url= http://www.rixtellab.com/en/other-services |title=Converting web sites to NoSQL |publisher=Rixtellab |date= |accessdate=March 9, 2015}}</ref>
* Cybersecurity and Lawful Intercept<ref name="Bit IT Solutions">{{cite web|url= http://www.bit.lv/en/bit-klientu-seminars/ |title=Bit IT Solution for Network Traffic Control |publisher=Bit IT solutions |date= |accessdate=March 9, 2015}}</ref>

A public demonstration solution powered by Clusterpoint database, illustrating how document type data of the entire [[Wikipedia]] and [[DBpedia]] (English) data corpus can be efficiently managed within a single consolidated database platform is available on the Web site [http://www.wikisearch.net '''Wikisearch.net'''].

==Competitors==
Clusterpoint database technology is positioned by industry experts among other emerging NoSQL and Big data technologies having distributed data management architecture.<ref name="Intel Corp">{{cite web|url=https://software.intel.com/sites/default/files/Configuration_and_Deployment_Guide_for_Cassandra_on_IA.pdf |title=NoSQL Scaling Beyond Traditional SQL |publisher=Intel Corp. |date= |accessdate=March 9, 2015}}</ref>

==Platform Components==
The Clusterpoint database software source code is being developed in [[C (programming language)|C and C++]] programming languages and supports multi-threading, multi-core CPUs and [[distributed computing]]. Primary method of developer's access to the platform capabilities is REST API. Clusterpoint database software is being managed across the large cluster of commodity hardware with Clusterpoint Console application.  Console provides centralized administration and control for all customer databases through a single web GUI.  In order to access Clusterpoint Console, or download it along Clusterpoint database software for on-premises use, customers have to sign up for Clusterpoint Cloud Database Account on the vendor website.  Sign-up is free, no credit card required.

==Architecture==
Clusterpoint database has multi-master [[shared-nothing]], distributed, document-oriented database architecture storing XML and JSON data types.
<ref name="search">{{cite web|url=http://h30499.www3.hp.com/t5/HP-Software-Developers-Blog/Guide-to-NoSQL-part-2/ba-p/6137493 |title=HP Guide to NoSQL |publisher=Hewlett-Packard Corp. |date=March 5, 2015}}</ref>
  
It works as transactional high-speed [[OLTP]] database for XML and JSON data objects.  New content can be added, updated and deleted in real-time, with real-time all changed data indexing, including full text, date, numeric, geospatial data.  Index data immediately can be read for search and analytics after each document has been inserted, updated or deleted, while ACID-compliant transactions provide security and consistency.  Database API also supports storage and processing of binary data as part of document data object model.

It supports no-single-point-of failure [[fault-tolerant]] infrastructure hardware setup with multi-datacenter replication capability for the entire [[distributed database]] cluster.

===Query syntax===
To query a database customers can use either free text query, XML-based syntax, JS/SQL query or Clusterpoint REST API that supports JSON.

===General features===
* Data is managed in open, cross-platform, industry standard [[XML]] or [[JSON]] format using open API, for instance, Python API<ref name="Clusterpoint API on Github">{{cite web|url=https://github.com/clusterpoint/pycps |title=Clusterpoint API on Github|publisher=Github.com |date=|accessdate=March 9, 2015}}</ref><ref name="Python API for Clusterpoint Server">{{cite web|url=https://pypi.python.org/pypi/clusterpoint-api/ |title=Python API for Clusterpoint Server|publisher=Python.org |date=|accessdate=March 9, 2015}}</ref> or JavaScript Node.js API<ref name="Clusterpoint Node.js API">{{cite web|url=https://www.npmjs.com/package/cps-api |title=Clusterpoint Node.js API |publisher=NPM, inc. |date=|accessdate=March 9, 2015}}</ref>
* Data structure agnostic and type-rich database, handles variable data structure XML or JSON documents in a single database. Supports unstructured textual data, dates, numbers, meta-data (all [[XML]] and [[JSON]] types)
* Cross-platform support: binaries are available for [[Linux]], [[FreeBSD]], [[Mac OS X]] and [[Windows]]. Clusterpoint database software can be compiled on other [[operating systems]].
* Multi-master cluster software architecture: no single point of failure, any cluster node can serve as a master and run the management application
* Horizontal database scalability: scales out from a single server to few thousands of servers networked into a cluster infrastructure

===Access features===
* REST API is used for [[XML]] and [[JSON]] document format management, search and data manipulation.
* Consistent [[UTF-8]] encoding. Non-UTF-8 data can be saved, queried, and retrieved with a special binary data type.
* XML and JSON objects for API queries and responses: enable direct integration in other programming languages supporting XML or JSON parsing, no specific client software required

===Search/query features===
* Built-in rich [[full text search]] functionality, with fast and free use of keywords and phrases, result snippeting, highlighting, term proximity search and other full-text search options<ref name="Full Text Search">{{cite web|url=http://everything.explained.at/Full_text_search/ |title=Full Text Search Explained |publisher=Everything.Explained.At |date= |accessdate=March 9, 2015}}</ref>
* Querying with term stemming, term wildcards and character position patterns, for inflected words and plural word forms delivering automagical self merge-joins<ref name="self-merge-joins">{{cite web|url=http://googleappengine.blogspot.com/2010/04/making-your-app-searchable-using-self.html |title=Making you app searchable using self merge-joins|publisher=Google |date= |accessdate=June 14, 2013}}</ref>
* SQL-like XML-structured (fielded) queries like in SQL SELECT ... WHERE ... statements
* Cluster-wide analytics aggregation with MIN(), MAX(), COUNT(), AVG() like in SQL SELECT ... GROUP BY ..., ORDER BY ... statements
* JS/SQL querying language, than combines well-known familiarity of SQL language with ubiquitous JavaScript code and the Web programming skills
* Sorting of results in alphabetic, numeric, date order or according to result relevance
* Autocomplete (instant search as you type) using the actual index data
* Spell-check of query terms with alternative spelling suggestions for "Did you mean <u>that</u>?" functionality
* Boosting of search query terms at query time, in order to increase, decrease or overwrite through the API relevancy weights or sorting rules built into the ranking index
* Dynamic data classification per query by multi-level customer defined facets with exact hit counting (examples: categories, themes, product catalogs, geographic locations etc.)
* Text-analytics driven similar content search across the entire database
* XML or JSON data structure relevance ranking by tag weighting and document relevance ranking by document rating
* Textual relevance ranking for matching search query terms to context, taking into account frequency and density of natural language terms
* Predictive calculation of expected number of results based on the actual index statistics in large size databases to optimize performance

===Administration/production use features===
* Granular security partitioning: API users and their access rights are based on groups and permissions assigned per specific databases and API commands
* Transaction journaling, redo logs, access logs, error logs and audit logs enabled by default
* Document versioning enabled by default (preserving previous document versions for a certain time period)
* Reindexing in background with automatic switchover provides availability during reindexation
* Online, offline and incremental database backup
* Automatic or manual synchronization of database replicas
* Multiple administrator accounts for secure multi-tenancy of different customer databases on the same hardware
* Centralized web GUI based database administration Console, including one-click configuration of clustered and replicated databases across all nodes

===Automatic full database content indexing===
Clusterpoint software automatically builds and maintains document-type XML and JSON data content index when data us loaded, updated or deleted.  A single database index (ranking index) is maintained to support these types of querying:
* natural language based full text search indexing, including language-specific [[stemming]] and [[collation]] rules
* XML or JSON data structure queries (with full-text, exact match and binary match options) or Essential SQL queries for analytics
* virtual data structure search created from [[aliasing (computing)|aliasing]] multiple real tags values to speed up Boolean OR queries
* ad hoc search across all database content irrespectively from the database structure
* numeric and date range search
* geospatial search by range, distance or polygon coordinates and ordering by distance from a certain point
* multi-level faceted search with automatic results classification by XML / JSON tags assigned as containing facets
* combination of any of the above database search criteria into complex nested multi-part query expressions using Boolean AND, OR, NOT logic

===Database administration===
Clusterpoint database can be controlled centrally through the Clusterpoint Console application.  It is a web-GUI dashboard that enables to control all database services enterprise-wide, including cluster database administration, configuration of indexing and ranking policy, secure user account management, audit and log file view, database backup/restore, database sharding and replication.

Each customer database is being started and stopped as an isolated database server process for the controlled management of CPU resources, RAM memory and disk storage.  All databases share a single networked computing and storage infrastructure.

Clusterpoint Console is used to manage underlying hardware (cluster nodes) to share computing resources among different databases in parallel.

===Process and storage architecture===
Clusterpoint database processes are safely isolated, each [[computer process|process]] runs only in its own RAM memory address space.  It can access only its own local file system storage folder with the same name containing the particular database XML or JSON documents, index, configuration and log files stored on that local cluster node (shard). This architecture delivers elastic horizontal scale out ability and cluster-wide control over resource consumption for a particular customer database.  It also prevents unauthorized access to multi-tenant databases using the same computing hardware infrastructure, with option to fully encrypt sensitive data.

===Multi-tenancy and virtualization===
Clusterpoint supports secure multi-tenant database services.  Software platform takes care about safe partitioning of runtime database computing environment among all cluster CPUs nodes, all RAM processes and all storage resources within a larger cluster, while operating databases in parallel on the same hardware equipment. This method delivers the best utilization of modern [[multi-core CPU]] hardware arranged in large distributed clusters.

Use of native multi-tenancy is the preferred method for high-performance database computing with Clusterpoint software vs [[operating system]] level [[virtualization]] or software [[containerization]] for safe multi-tenancy. [[Operating-system-level virtualization|OS-level virtualization]] may decrease available network bandwidth and computing resource, creating also unexpected bottlenecks at storage I/O level, that could result in increased application latencies.  Database virtualization can be best use for prototyping and development where operational performance guarantees and low latency are not the first priority.<ref name="Database Virtualization">{{cite web|url=http://www.networkcomputing.com/data-centers/the-dos-and-donts-of-virtualizing-database-servers/a/d-id/1230996 |title=The Do's and Don'ts of Virtualizing Database Servers |publisher=Network Computing |date= |accessdate=March 9, 2015}}</ref>

Clusterpoint Cloud Database as A Service (DBaaS) is a secure multi-tenant database platform, with isolated data for each customer account and encrypted access security. Clusterpoint software does not need virtualization for safe and efficient multi-tenancy.

===Multi-copy database replication===
Automatic multi-copy replication for the entire database is built into the Clusterpoint database software.  It is active replication, with workload sharing within a cluster.  Clusterpoint supports high-performance OLTP transactions, ACID-compliant, within a main cluster in a single data center, while providing fail-over to more datacenters running database replica clusters.  Fail-over takes only few seconds, if communication latency among data centers is minor.

Database replicas in Clusterpoint architecture are used for automatic [[load balancing (computing)|load balancing]] of database search queries through Clusterpoint API.

In multi-datacenter use network bandwidth among locations may become the critical issue for Clusterpoint architecture because of increased latencies for database updates and synchronization delays among replicas, in particular, if encrypted [[VPN]] networking over the Internet links is used.

A high-capacity bandwidth might be required for high-performance database replication among geographically different location datacenters.

===Extendable server-side scripting with Lua===
The [[Lua programming language|Lua]] extends Clusterpoint Server functionality with custom server-side scripts.  Lua scripts can implement customer-specific functions such as data aggregation, [[extract, transform, load|ETL]] tasks, meta-data [[markup language|markup]], call-back to external programming languages using web services for extra functionality, real-time alerting or asynchronous triggers.  Scripts can be executed before, during or after Clusterpoint API transactions of interest.  Built-in configurable server-side hooks activate Lua scripts in different stages of each Clusterpoint transaction execution process.

Custom Lua scripts can be stored in Clusterpoint Server to work as "stored procedures".

===Extendable server-side scripting with JavaScript computing engine===
Starting from Clustepoint database version 4, JS/SQL has been added as main scripting engine.  JS/SQL is representing SQL query language that can be custom extended with free [[JavaScript]] user code.  JavaScript can be used within WHERE, GROUP BY, ORDER BY and other SQL statament clauses.  This feature enables to custom extend Clusterpoint database functionality beyond standard database and search features.  For example, users can perform highly parallel computing tasks within a database where local data storage will provide the fastest possible performance, while only using familiar SQL syntax extended with own JavaScript functionality, all within a single JS/SQL query in Clusterpoint database architecture.

===Programming language support===
Clusterpoint database uses REST principles and HTTP/HTTPS messaging for client-server communications between customer software applications and Clusterpoint database server. Any client programming language or development environment, supporting HTTP POST/GET messaging, can connect to Clusterpoint Server directly and read, write, update, delete and search XML and JSON documents.

In versions 1.x, 2.x and 3.0 REST API interface for JSON data format transforms customer data between JSON and XML, while only XML is used for internal server-side data storage and processing by Clusterpoint Server.

Clusterpoint Server has native client API Libraries using HTTP and faster TCP/IP transport protocol for the following popular programming environments:
* [[XML]]
* [[JSON]]
* [[Representational state transfer|REST (http / https)]]
* [[Internet protocol suite|TCP IP (wire-format drivers)]]
* [[PHP]]
* [[.NET]]
* [[Java (programming language)|Java]]
* [[Python (programming language)|Python]]
* [[Node.js|JavaScript Node.js]]
* [[C (programming language)|C, C++]]

Please check the vendor web site for API support in other languages.

==Licensing and support==
Clusterpoint offers two database licensing options based on functionality and scalability:

- Clusterpoint Enterprise - The most comprehensive DBMS product solution, delivering unlimited scalability and the highest standards of enterprise grade functionality, fulfilling the most demanding of customer requirements.

- Clusterpoint Lite - The Clusterpoint DBMS solution for smaller organisations who require high standards in basic database functionality, supported by replication on 2 servers, but for whom scalability and sharding is not an immediate operational requirement.

There are four types of on-premise licencing models available - Perpetual licence, Subscription licence, OEM licence and Developer licence.

Vendor provides standard software maintenance and technical support service based on subscription model (on premises or Clusterpoint Database Cloud), delivering it over [[email]], [[Skype]] or phone.<ref name="Clusterpoint Software">{{cite web|url=https://www.facebook.com/ClusterpointDB |title=Clusterpoint DBaaS Cloud Service  |publisher=Facebook |date= |accessdate=March 9, 2015}}</ref>

Premium technical support for customers using the software in 24h/7d production environments includes remote problem diagnostics and resolution based on [[Service-level agreement]].  Vendor provides installation support, help-desk, training and partnership programs.<ref name="Partnership">{{cite web|url=http://www.1datagroup.com/products/clusterpoint/ |title=Clusterpoint DBMS by 1DataGroup |publisher=1DataGroup |date= |accessdate=March 9, 2015}}</ref><ref name="Training">{{cite web|url=http://www.theknowledgeacademy.com/courses/clusterpoint-programming-training/clusterpoint-dbms/ |title=Knowledge Academy Training Course in Clusterpoint DBMS|publisher=Knowledge Academy |date= |accessdate=March 9, 2015}}</ref><ref name="Big Data XML NoSQL Engine">{{cite web|url=http://www.meetup.com/SyncHerts/events/219128144/ |title=Big Data Meetup. Clusterpoint XML Database Engine |publisher=Meetup.com |date= |accessdate=March 9, 2015}}</ref>

==Clusterpoint Products==
* ''Clusterpoint DBMS'' - clustered NoSQL database, which uses approach of multiple server system to spread load and increase performance. Clusterpoint database facilitates high parallelism of computing and distribution of data.
* ''GOL: Big Data SIEM Analytics tool from Clusterpark'' - Log, Events and Security Records Search and Analytics.<ref name="Clusterpark">{{cite web|url=https://www.clusterpoint.com/for-business/gol-log-analytics |title=Clusterpoint GOL - fast log data analytics & search application software|publisher=Clusterpoint |date= |accessdate=March 4, 2016}}</ref>
* ''DigiBrowser: Quick SQL denormalization into NoSQL database'' - imports multi-table SQL database into one Clusterpoint database using automagic denormalization.<ref name="DigiBrowser">{{cite web|url=http://digibrowser.com |title=DigiBrowser: Quick SQL denormalization into NoSQL database|publisher=Datorikas Instituts DIVI. |date= |accessdate=March 4, 2015}}</ref>
* ''NTSS: Network Traffic Sureveillance System for Lawful Intercept'' - High-speed capture, store, search and analysis of all Internet traffic for the corporate network.<ref name="Clusterpoint NTSS Product Review">{{cite web|url=http://community.spiceworks.com/product/50664-clusterpoint-network-traffic-security-system |title=Clusterpoint NTSS Product Review|publisher=SpiceWorks, Inc. |date= |accessdate=March 9, 2015}}</ref><ref name="Clusterpoint NTSS">{{cite web|url=http://iigrowth.com/2010/11/clusterpoint/ |title=Clusterpoint Network Traffic Surveillance System|publisher=iiGrowth LLC|date= |accessdate=March 9, 2015}}</ref>

==See also==

* [[NoSQL]]
* [[Operational database]]
* [[Structured storage]]
* [[XML]]
* [[SQL]]
* [[CRUD]]
* [[REST]]

==References==
{{Reflist|2}}

{{Refend}}

{{DEFAULTSORT:Clusterpoint}}
[[Category:Proprietary database management systems]]
[[Category:Document-oriented databases]]
[[Category:XML databases]]
[[Category:Distributed computing architecture]]
[[Category:NoSQL]]
[[Category:XML]]
[[Category:Database-related software for Linux]]