{{Infobox character
| name        = Roose Bolton
| series      = [[A Song of Ice and Fire]] ''character'' <br> [[Game of Thrones]]
| portrayer   = [[Michael McElhatton]]<br>(''[[Game of Thrones]]'')
| creator     = [[George R. R. Martin]]
| image       = File:Roose Bolton-Michael McElhatton.jpg
| caption     = [[Michael McElhatton]] as Roose Bolton
| first       = '''Novel''': <br>''[[A Game of Thrones]]'' (1996) <br>'''Television''': <br>"[[Garden of Bones]]" (2012)
| last        = '''Television''': <br>"[[Home (Game of Thrones)|Home]]" (2016)
| occupation  =
| title       = Lord of [[Winterfell]]<br>Lord of the Dreadfort<br>Warden of the North<br>Lord Paramount of the North
| alias       =
| gender      = Male
| family      = [[House Bolton]]
| spouse      = Unnamed first wife<br>Bethany Ryswell<br>"Fat" Walda Frey
| children    = Domeric Bolton<br>[[Ramsay Bolton]]<br>Unnamed newborn with Walda Frey
| lbl21       = Kingdom
| data21      = [[The North (A Song of Ice and Fire)|The North]]
}}

'''Roose Bolton''' is a [[fictional character]] in the ''[[A Song of Ice and Fire]]'' series of [[fantasy]] novels by American author [[George R. R. Martin]], and its television adaptation ''[[Game of Thrones]]''.

Introduced in 1996's ''[[A Game of Thrones]]'', Bolton, a northern lord with his seat at the Dreadfort, is a retainer of Lord [[Eddard Stark]]. Bolton is known for being cold and calculating. Bolton's heir is his bastard son [[Ramsay Snow]]. Bolton joins [[Robb Stark]]'s rebellion as (along with Greatjon Umber), one of Stark's lieutenants. He marries Fat Walda (Frey), after having been offered any Frey bride's weight in silver as a dowry by Lord [[Walder Frey]]. With the help of [[Arya Stark]] and the Brave Companions he takes and holds [[Harrenhal]] until rejoining Robb Stark ostensibly to help retake the Neck (occupied by [[House Greyjoy|the Greyjoys]]), but in truth as a conspirator orchestrating the Red Wedding, receiving the title of Warden of the North from the [[House Lannister|Lannisters]].

Roose is portrayed by [[Michael McElhatton]] in the [[HBO]] television adaptation.<ref name=HBOCast>{{cite web | url=http://www.hbo.com/game-of-thrones/index.html#/game-of-thrones/cast-and-crew/roose-bolton/bio/roose-bolton.html |title=''Game of Thrones'' Cast and Crew: Roose Bolton played by Michael McElhatton |publisher=[[HBO]] | accessdate=December 25, 2015}}</ref><ref>{{cite web|url=http://www.hbo.com/game-of-thrones/index.html#/game-of-thrones/cast-and-crew/index.html|title=The Official Website for the HBO Series Game of Thrones - Season 4|work=HBO}}</ref><ref>{{cite web|url=http://grrm.livejournal.com/164794.html |title=From HBO |publisher= |deadurl=yes |archiveurl=https://web.archive.org/web/20160307150640/http://grrm.livejournal.com/164794.html |archivedate=2016-03-07 |df= }}</ref>

==Character description==
Lord Roose Bolton is a significant [[vassal]] of Lord [[Eddard Stark]]. His seat is the Dreadfort and his sigil is a flayed man, an homage to the ancient Bolton tradition of [[flaying]] enemies. He is nicknamed "the Leech Lord" for regular [[Hirudo medicinalis#Medicinal use|leechings]] meant to improve his health. He is known to be a cold, cautious and calculating man.

== Overview ==
Roose Bolton is not a [[Narration#Third-person|point of view]] character in the novels, so his actions are witnessed and interpreted through the eyes of other people, such as [[Catelyn Stark]], [[Arya Stark]] and [[Theon Greyjoy]]/Reek.<ref>{{cite web|url=http://viewers-guide.hbo.com/map/special/areas-of-control/4/|title=Game of Thrones Viewer's Guide|publisher=HBO}}</ref>

== Portrayal and reception ==
Roose is portrayed in the HBO TV series by Irish actor [[Michael McElhatton]], who has received positive reviews for his performance.<ref>{{cite web|url=http://www.vulture.com/2015/05/game-of-thrones-michael-mcelhatton-on-the-bolton-boys.html|title=Game of Thrones’ Michael McElhatton on the Bolton Boys and That Dysfunctional Family Dinner|publisher=Vulture}}</ref>

== Storylines ==
===Books===
====Early history====
[[File:A Song of Ice and Fire arms of House Bolton pink scroll.png|thumb|200px|alt=Coat of arms of House Bolton|Coat of arms of House Bolton]]
At some point prior to Robert's Rebellion, Roose was hunting along the Weeping Water when he came across a pretty young miller's wife. Desiring the woman, Roose has her husband hanged for not informing him of the marriage (see [[Droit du seigneur]]) and rapes the woman beneath his corpse. A year later, the woman comes to the Dreadfort with a newborn boy who she claims is Roose's bastard Ramsay. Roose almost has the baby killed before realising that the baby is indeed his, and gives the woman money to raise Ramsay. Roose later participates in Robert's Rebellion and (according to a semi-canonical source) the Greyjoy Rebellion.

Despite Roose's orders, Roose's only trueborn son Domeric seeks out Ramsay. Domeric dies soon after, and Roose suspects that Ramsay poisoned him to become his heir. Roose, left without a trueborn heir, brings Ramsay to the Dreadfort.

====''A Game of Thrones''====
Roose is amongst the lords who travel to Winterfell to aid [[Robb Stark]] in his campaign against the Lannisters. His intelligence and caution sees him given command of part of the Northern host when the army splits up at the Twins, and he leads the attack on [[Tywin Lannister]]'s army in the Battle of the Green Fork. The battle ends in a Lannister victory and Roose retreats with the survivors to the causeway of Moat Cailin.
====''A Clash of Kings''====
Roose marries "Fat" Walda Frey, a granddaughter of Lord [[Walder Frey]], to form an alliance between the two houses. He also makes an alliance with the Brave Companions, Essosi sellswords employed by Tywin, to help the Northerners capture Harrenhal from the Lannister force occupying it. After capturing Harrenhal Roose takes [[Arya Stark]] as his cupbearer, mistaking her for a commoner.
====''A Storm of Swords''====
Hoat brings [[Jaime Lannister]] to Harrenhal, having cut off Jaime's hand in hope of blaming Roose and preventing the Boltons allying with the Lannisters. Roose has Jaime sent back to King's Landing after Jaime assures Roose that he will not blame him. Roose then travels to the Twins for Edmure Tully's wedding to Roslin Frey, but at the wedding the Freys turn on the Starks and Roose personally kills Robb Stark. It is revealed that Roose had conspired with the Freys and Tywin Lannister to betray the Starks. As reward for his service, Tywin names Roose the new Warden of the North
====''A Feast for Crows'' and ''A Dance With Dragons''====
Roose returns North with his forces, joined by two thousand Frey men. Meeting with Ramsay (now legitimised as a Bolton) and a captive [[Theon Greyjoy]], the Boltons travel to Barrowton for Ramsay's wedding to Jeyne Poole, forced to assume the identity of Arya Stark. After hearing that [[Stannis Baratheon]] has captured Deepwood Motte, Roose decides to move the wedding to Winterfell to bait Stannis out. The Boltons and their Northern allies (many of whom are only grudgingly pledging fealty to the Boltons, or plan to betray them) remain at Winterfell after the wedding in anticipation of Stannis' attack. Tensions are high during the wedding due to the anger of the Northmen at the Freys. Three of the Freys who had been travelling with Lord Wyman Manderly of White Harbor, who lost his younger son Ser Wendel Manderly at the Red Wedding, have disappeared, and are heavily implied to have been put in pies which Wyman gives to the Freys and Boltons, eating some himself. Lady Barbrey Dustin of Barrowton, the younger sister of Bethany Ryswell, tells Theon that Roose has no feelings and plays with people for amusement. When one of Walder Frey's grandsons, Little Walder Frey, is found murdered (possibly by their cousin Big Walder Frey), their uncle Ser Hosteen Frey attacks Wyman, leading to a fight in which White Harbor and Frey men are killed. Roose is forced to send them both out of Winterfell to encounter Stannis.

=== Family tree of House Bolton ===
{{chart/start|style=font: 100% sans-serif;|align=center}}
{{chart | | | | | | | | | | | | Bh |-|-|v|-|-| Rs |-| Fa | |Bh=Bethany<br>Ryswell|Rs='''Roose'''|Fa="Fat" Walda<br>Frey}} 
{{chart | | | | | | | | | | | | |,|-|-|-|'| | | |:| | | | | | | | | | | | | | | | | | | | }}
{{chart | | | | | | | | | | | | Dm | | Do |-| Rm |-| Je | | | |Do=Donella<br>Hornwood|Rm=[[Ramsay Bolton|Ramsay]]|Je=[[Jeyne Poole|Jeyne<br>Poole]]{{efn|name=Sansa}}|Dm=Domeric}}
{{chart/end}}
;Notes
{{notelist|refs=
{{efn|name=Sansa|Ramsay marries [[Sansa Stark]] in the television adaptation ''Game of Thrones''.}}
}}

== TV adaptation ==
[[File:Showbiz-michael-mcelhatton.jpg|thumb|right|upright|[[Michael McElhatton]] played the role of Roose Bolton in the [[Game of Thrones|television series]].]]
Roose Bolton is played by [[Michael McElhatton]] in the HBO television adaption of the series of books.<ref>{{cite web|url=http://www.accesshollywood.com/articles/game-of-thrones-qa-michael-mcelhatton-talks-roose-bolton-144980/|title=‘Game Of Thrones’ Q&A: Michael McElhatton Talks Roose Bolton|work=Accesshollywood}}</ref>

===Storylines===
A Bannerman of the North and [[Lord]] of the Dreadfort. The Bolton family have a nasty history of keeping to very old, and barbaric ways, including flaying their enemies alive, and Roose is no exception, being suspected of not feeling any emotion. His cunning makes him a valuable ally, but his unpredictable nature makes him a dangerous one.

Roose's background in the television series is much the same as in the books. However, in the television series he adopts Ramsay of his own accord when his mother dies soon after Ramsay's birth.

====Season 2====
Roose declares for King in the North Robb Stark and serves as a chief member of his war council, although Robb sternly admonishes Roose when he advocates flaying Lannister prisoners to obtain information. After Theon Greyjoy betrays the Starks and seizes Winterfell, Roose brings the news to Robb and offers to send his bastard son Ramsay Snow with a force of Dreadfort men to oust Theon and the Ironborn from Winterfell.

====Season 3====
[[File:Game of Thrones Oslo exhibition 2014 - Red Wedding weapons.jpg|thumb|The dagger with which Roose Bolton kills Robb Stark, and the knife with which Catelyn Stark kills Walder Frey's wife.]]
Following the Northern army's arrival at Harrenhal, Roose presents a letter from Ramsay claiming that the Ironborn sacked Winterfell before fleeing. Robb orders Roose and the Bolton forces to hold Harrenhal while the rest of his army rides to Riverrun. One of Roose's man-at-arms, Locke, captures the escaped Jaime Lannister and his escort Brienne of Tarth, cutting off Jaime's swordhand in the process, before bringing the two to Harrenhal. Roose agrees to let Jaime go, but keeps Brienne as a hostage, though Jaime later returns to secure her release. He then meets up with the Stark army at the Twins for the wedding of Edmure Tully and Roslin Frey. However, it is revealed that Roose has conspired with Lord Walder Frey to betray the Starks, and after the wedding the Freys and Bolton slaughter the Stark forces, with Roose personally killing Robb. As part of the Bolton-Frey alliance, Roose agrees to marry Walder's daughter Walda - Walder offers him the bride's weight in silver as dowry, so Roose decides to marry the fattest of Walder's daughters. In the aftermath of the massacre, Roose hints to Walder that his betrayal of Robb was motivated by resentment at having his advice ignored by Robb. Roose also reveals that Winterfell was actually sacked by his bastard Ramsay, who subsequently flayed the Ironborn garrison there and took Theon prisoner, for his own amusement. As reward for his defection, Tywin Lannister names Roose the Warden of the North.

====Season 4====
With the Ironborn holding Moat Cailin - the fortification barring passage between the North and the rest of Westeros - Roose is forced to smuggle himself back into the North. Upon his return to the Dreadfort, he chastises Ramsay for having gelded Theon and sending terms of surrender to the Greyjoys without his approval, while reminding Ramsay of his bastard parentage. Insulted, Ramsay demonstrates how effectively he has broken Theon (whom he has since renamed "Reek") by having Reek shave him, even after revealing Roose's murder of Robb, while also coaxing Reek into revealing he faked the deaths of Bran and Rickon Stark. After Ramsay points out that the other Northerners will turn on the Boltons if it is revealed that there is a living male Stark, Roose tasks Locke with hunting down Bran and Rickon and killing Jon Snow, Robb's bastard half-brother. Roose also sends Ramsay and Reek to lift the siege of Moat Cailin; when Ramsay is successful, Roose presents him with a royal decree of legitimisation as a trueborn Bolton. Roose subsequently moves to rebuild and occupy Winterfell.

====Season 5====
In the aftermath of Tywin Lannister's death and Ramsay's murder of a disobedient vassal and his family, Roose seeks to secure House Bolton's position by arranging to have Ramsay marry Sansa Stark, supposedly the last trueborn Stark alive. In doing so, Roose seemingly secures an alliance with the forces of the Vale and its Lord Protector Petyr "Littlefinger" Baelish (unaware that Baelish intends on having the Bolton army decimated by the approaching Baratheon army before defeating the victor with the Vale's army). After Ramsay torments Sansa by having Reek serve them at dinner, Roose announces that he and Walda are expecting a son. However, later Roose privately reassures Ramsay of his position as his heir, and asks him to assist in defeating Stannis Baratheon's army. To this end, Roose permits Ramsay and his men to launch a sneak attack on Stannis' camp, destroying the army's supplies. With the supplies destroyed and most of Stannis' army subsequently deserting him, the Boltons easily defeat the Baratheons when they attempt to lay siege to Winterfell, but in the aftermath of the battle Reek and Sansa manage to escape, severely jeopardizing House Bolton's rule in the North.

====Season 6====
Despite their victory over Stannis, Roose warns Ramsay that the North will someday have to face the Lannisters, and chastises him for allowing Sansa and Theon to escape, as Sansa was crucial to unifying the North. He implies that if Sansa is not recovered, Ramsay's position as heir may be usurped by Walda's baby. Soon afterwards, it is announced that Walda has given birth to a boy; Ramsay immediately kills Roose by stabbing him in the stomach, before setting his dogs upon Walda and the baby, severely jeopardizing House Bolton and leaving Ramsay as the last remaining Bolton.

== References ==
{{Reflist|30em}}

{{ASOIAF}}

{{DEFAULTSORT:Bolton, Roose}}
[[Category:A Song of Ice and Fire characters]]
[[Category:Characters in American novels of the 20th century]]
[[Category:Characters in American novels of the 21st century]]
[[Category:Fictional assassinated people]]
[[Category:Fictional characters introduced in 1996]]
[[Category:Fictional swordsmen]]
[[Category:Fictional traitors and defectors]]
[[Category:Fictional mass murderers]]
[[Category:Fictional domestic abusers]]
[[Category:Fictional nobility]]
[[Category:Fictional rapists]]
[[Category:Patricide in fiction]]