{{Infobox road
|state=CA
|type=SR
|route=139
|section=439
|maint=[[California Department of Transportation|Caltrans]]
|length_mi=143.26
|length_round=2
|length_ref=<ref name=trucklist/><ref name="bridgelog"/>
|history=State highway in ca. 1940 and 1959; numbered by 1946
|direction_a=South
|terminus_a={{jct|state=CA|SR|36}} in [[Susanville, California|Susanville]]
|junction={{jct|state=CA|SR|299}} from [[Adin, California|Adin]] to [[Canby, California|Canby]]
|direction_b=North
|terminus_b={{jct|state=OR|OR|39|dir1=north}} and {{jct|state=CA|CA|161|dir1=west}} at [[Oregon]] state line
|previous_type=SR
|previous_route=138
|next_type=SR
|next_route=140
}}
'''State Route 139''' ('''SR 139''') is a [[state highway]] in the [[U.S. state]] of [[California]]. Running from [[California State Route 36|SR 36]] in [[Susanville, California|Susanville]] north to [[Oregon Route 39]], it forms part of the shortest route between [[Reno, Nevada]], and [[Klamath Falls, Oregon]]. SR 139 cuts through much [[Modoc National Forest]] and passes near [[Antelope Mountain]] and [[Tule Lake]]. The entire route is part of the [[California Freeway and Expressway System]], but is a two-lane road. North of the [[California State Route 299|SR 299]] [[overlap (road)|overlap]], SR 139 is an eligible [[State Scenic Highway (California)|State Scenic Highway]], but has not been designated as such;<ref>{{cite web|url=http://www.dot.ca.gov/hq/LandArch/scenic_highways/|title=Officially Designated State Scenic Highways and Historic Parkways|publisher= California Department of Transportation|accessdate=February 1, 2011|date=December 7, 2007}}</ref> the short piece north of [[Tulelake, California|Tulelake]] is however part of the federal [[Volcanic Legacy Scenic Byway]]. The portion north of SR 299 is also in the [[Interregional Road System]] as a High Emphasis Route and in the [[National Highway System (United States)|National Highway System]]. North of SR 299, the highway was built by the [[Federal government of the United States|federal government]] and turned over to the state in about 1940; the remainder was built by a joint highway district of [[Lassen County, California|Lassen]] and [[Modoc County, California|Modoc]] Counties, completed in 1956, and given to the state in 1959.

==Route description==
[[Image:Modoc-map.png|thumb|left|SR 139 through the [[Modoc National Forest]]]]
State Route 139 begins at [[California State Route 36|SR 36]] in [[Susanville, California|Susanville]], and heads northeast up [[Antelope Mountain]] along the eastern edge of [[Susanville Ranch Park]] before turning north and descending into the [[Eagle Lake Basin]], passing along the eastern edge of [[Eagle Lake (Lassen County)|Eagle Lake]] and later the [[Said Valley Reservoir]]. The highway continues north and northwest through valleys and over hills and through a part of the [[Modoc National Forest]], through the community of [[Avila Place, California|Avila Place]], and then enters the east end of the [[Big Valley (Pit River)|Big Valley]], where it begins to [[overlap (road)|overlap]] [[California State Route 299|SR 299]] through [[Adin, California|Adin]]. The combined routes continue northerly through another part of the forest and over [[Adin Pass]] into the [[Warm Springs Valley]], where SR 139 splits to the northwest near [[Canby, California|Canby]].<ref name="maps" /><ref name="gmaps" />

SR 139 runs northwest and north over mostly flat terrain through the center of the national forest and the communities of [[Ambrose, Modoc County, California|Ambrose]] and [[Perez, California|Perez]] before turning northwest. The route continues through [[Copic, California|Copic]], [[Newell, California|Newell]], [[Stronghold, California|Stronghold]], [[Homestead, Modoc County, California|Homestead]], and [[Tuber, California|Tuber]] before cutting across the intermittent [[Tule Lake]] as the Lava Beds Highway through the city of [[Tulelake, California|Tulelake]]. During this segment, SR 139 passes directly through the former [[Tule Lake Unit, World War II Valor in the Pacific National Monument|Tule Lake Segregation Unit]], used to house Japanese-Americans during WWII. After passing through the former internment camp, SR 139 cntinues to the [[Oregon]] state line at [[Hatfield, California|Hatfield]]. At an intersection right on the state line, [[California State Route 161|SR 161]] begins to the west, and [[Oregon Route 39]] continues northwest towards [[Klamath Falls, OR|Klamath Falls]].<ref name=maps>{{cite map|publisher=ACME Maps|title=ACME Mapper 2.0|url=http://mapper.acme.com/?ll=41.19886,-120.94414&z=8&t=M|accessdate=January 6, 2012}}</ref><ref name="gmaps">{{Google maps | url = https://maps.google.com/?ll=41.191056,-120.036621&spn=1.831155,4.284668&t=m&z=8&vpsrc=6 | title = SR 139 | accessdate = January 11, 2012}}</ref>

==History==
In 1925, a state-created "California Highway Advisory Committee" recommended a number of additions to the state highway system; among these was a route from [[Susanville, California|Susanville]] to the [[Oregon]] state line towards [[Klamath Falls, OR|Klamath Falls]], via [[Bieber, California|Bieber]]. This would be part of a road connecting [[Reno, Nevada]] and Klamath Falls east of the [[Sierra Nevada (California)|Sierra Nevada]], which would attract heavy traffic and improve access to [[Crater Lake National Park|Crater Lake]] and [[Lassen Volcanic National Park|Lassen Volcanic]] [[National Park Service|National Park]]s.<ref name=1925report>California Highway Advisory Committee and Arthur Hastings Breed, Report of a Study of the State Highway System of California, California State Printing Office, 1925, p. 98</ref> A local [[county road]] already followed this path, but it was [[unpaved road|unpaved]], mostly [[dirt road|dirt]] and [[gravel road|gravel]] but with sections of rock and bad sand. This was close to the present SR 139, with notable deviations around the areas of [[Hayden Hill, California|Hayden Hill]], Bieber and [[Lookout, California|Lookout]], and [[Malin, Oregon]] (as [[Tule Lake]] covered SR 139's current location).<ref>{{cite book|title=Official Automobile Blue Book Volume Eight|url=https://books.google.com/books?id=Mt0NAAAAYAAJ|pages=266–268|year=1918}}</ref>

By the mid-1920s, the main road southeast from Klamath Falls, still unimproved in California, headed southeast to [[Legislative Route 28 (California pre-1964)|State Highway Route 28]] (now [[California State Route 299|SR 299]]) at [[Canby, California|Canby]] rather than south to Bieber. There travelers could head east on Route 28 to [[Alturas, California|Alturas]] and south on the present [[U.S. Route 395 in California|US 395]] (not a state highway north of Susanville until 1933) towards Reno.<ref>[[Clason Map Company]], [http://www.usautotrails.com/CaliforniaPage/ClasonsCaliforniaPage/image1.html Touring Atlas of the United States], 1925</ref><ref>[[Rand McNally & Company]], [http://www.broermapsonline.org/members/NorthAmerica/UnitedStates/Southwest/California/unitedstates1926ra_072.html Auto Road Atlas], 1926</ref> The California state legislature passed a law in 1939, providing for state takeover of the Canby-Oregon road if the [[U.S. Forest Service]] and [[Bureau of Public Roads]] were to construct and pave it.<ref>{{cite CAstat|year=1939|ch=338|p=1677}}</ref> The road was in fact mostly paved by mid-1939,<ref>[[Nevada State Journal]], June 30, 1939: "A one-mile stretch at the Modoc-Siskiyou county line is gravel surfaced and from Tule Lake to the Oregon state line the oiled surface is broken and somewhat rough."</ref> and under construction or completed by mid-1940, when [[Oregon Route 58]] opened, continuing the corridor northwest from Klamath Falls.<ref>[[Reno Evening Gazette]], August 2, 1940: "With the opening of the new Willamette highway in Oregon, connecting U. S. 99 with U. S. 97, a new avenue for direct travel is available to motorists between Reno and points in the Pacific Northwest...Short stretches are being oiled, with flagmen controlling traffic and caution is required. The balance of this route is entirely paved and in excellent condition."</ref> In 1943 the legislature gave it the Route 210 designation;<ref>{{cite CAstat|year=1943|ch=964|p=2849}}: "Route 210 is from Route 28 near Canby to the Oregon line near Merrill."</ref> Oregon had added the short connecting [[Hatfield Highway]] to its state highway system in 1937.<ref name=HSHO>[[Oregon Department of Transportation]], [ftp://ftp.odot.state.or.us/ROW_Eng/HistoryHighwaysOregon/HSHO.pdf History of State Highways in Oregon], January 2007, p. 677</ref>

[[Lassen County, California|Lassen]] and [[Modoc County, California|Modoc]] Counties organized Joint Highway District No. 14 on December 21, 1929 to construct and maintain a road from Susanville via [[Adin, California|Adin]] to Oregon. However, since the state took over the part north of Adin, the district's scope was narrowed to Susanville-Adin.<ref>[[Division of Highways (California)|Division of Highways]], Sixth Annual Report to the Governor on the Activities of the Division of Highways for the Year July 1, 1951 to June 30, 1952, p. 124</ref> It finally completed work in 1956, and held a ceremony on August 26, in which it placed a monument at a point near [[Eagle Lake (Lassen County)|Eagle Lake]].<!--I can't determine where - it says "Colley Point" but that's not on USGS topos; it says it provides a scenic view of the lake and is about 22 mi north of Susanville--><ref>[[Reno Evening Gazette]], Lassen Highway Dedication Held, September 4, 1956</ref> The legislature added the road to the state highway system as Route 216 in 1959.<ref>{{cite CAstat|year=1959|ch=1853|p=4409}}: "Route 216 is from Route 20 north of Susanville to Route 28 near Adin."</ref> The portion south of Horse Lake Road became an extension of [[Legislative Route 20 (California pre-1964)|Route 20]] instead;<ref>{{cite CAstat|year=1959|ch=1062|p=3116}}: "Route 20 is from:...(c) Route 29 near Susanville to Route 73 near Ravendale."</ref> this route from Susanville to [[Ravendale, California|Ravendale]] (later [[Termo, California|Termo]]) was never constructed by the state, and was deleted from [[California State Route 36|SR 36]] in 1998. Also in 1959, a spur of Route 210 west to [[Dorris, California|Dorris]] was added; this became [[California State Route 161|SR 161]] in 1964.<ref>{{cite CAstat|year=1959|ch=1062|p=3118}}: "Route 210 is from: (a) Route 28 near Canby to the Oregon line near Hatfield. (b) A point on the highway specified in subdivision (a) of this section near Hatfield to Route 72 near Dorris."</ref>

By 1946, the Canby-Oregon portion had been marked as Sign Route 139, connecting with [[Oregon Route 39]];<ref>[[Rand McNally & Company]], [http://www.broermapsonline.org/members/NorthAmerica/UnitedStates/Southwest/randmcnally_ra_1946_018.html Road Atlas], 1946</ref> it was extended south over [[U.S. Route 299 in California|US 299]] to Adin and Routes 216 and 20 to Susanville by 1960.<ref>[[Oakland Tribune]], May 31, 1960: "Nichols was killed when his car missed a turn yesterday morning and overturned on State Highway 139, two miles north of Susanville."</ref> The number was legislatively adopted, replacing Routes 210 and 216, in the [[1964 renumbering (California)|1964 renumbering]].<ref name=law-renumbering>{{cite CAstat|year=1963|ch=385|p=1182}}: "Route 139 is from: (a) Route 36 near Susanville to Route 299 near Adin. (b) Route 299 near Canby to the Oregon state line near Hatfield."</ref> It has remained a two-lane road,<ref name=maps/> despite being added to the [[California Freeway and Expressway System]] in 1959 (Canby to Oregon) and 1965 (Susanville to Adin).<ref>{{cite CAstat|year=1959|ch=1062|p=3115}}</ref><ref>{{cite CAstat|year=1965|ch=1372|p=3269}}</ref>

==Major intersections==
{{CAinttop|post_ref=<br /><ref name=trucklist /><ref name=bridgelog>{{cite web|title=Log of Bridges on State Highways|url=http://www.dot.ca.gov/hq/structur/strmaint/brlog2.htm|publisher=California Department of Transportation|date=July 2007}}</ref><ref>{{cite web|title=All Traffic Volumes on CSHS|url=http://traffic-counts.dot.ca.gov/|publisher=California Department of Transportation|year=2005–2006|accessdate=February 10, 2008}}</ref>
}}
{{CAint
|county=Lassen
|cspan=4
|county_note=LAS 0.00-66.44
|location=Susanville
|postmile=0.00
|road={{jct|state=CA|SR|36|name1=Main Street|location1=[[Reno, Nevada|Reno]]|city2=Red Bluff}}
|notes=South end of SR 139
}}
{{CAint
|location=none
|postmile=none
|road={{jct|state=CA|CR|A1|county1=Lassen|name1=Eagle Lake Road}}
|notes=
}}
{{CAint
|location=none
|postmile=43.27
|road=Termo-Grasshopper Road – [[Termo, California|Termo]]
|notes=
}}
{{CAint
|location=none
|postmile=61.46
|road={{jct|state=CA|CR|A2|county1=Lassen|name1=Susanville Road|city1=Bieber|city2=Mount Shasta}}
|notes=
}}
{{CAint
|county=Modoc
|cspan=4
|county_note=MOD 0.00-50.68
|location=Adin
|type=concur
|postmile=0.23
|line=yes
|postmile2=0.33<ref group="N" name="SR 299">Indicates that the postmile represents the distance along [[California State Route 299|SR 299]] rather than SR 139.</ref>
|road={{jct|state=CA|SR|299|dir1=west|city1=Redding}}
|notes=South end of SR 299 overlap
}}
{{CAint
|type=concur
|location=Canby
|postmile=21.75<ref group="N" name="SR 299"/>
|line=yes
|postmile2=0.23
|road={{jct|state=CA|SR|299|dir1=east|city1=Alturas}}
|notes=North end of SR 299 overlap
}}
{{CAint
|location=none
|postmile=17.35
|road=Lookout-Hackamore Road – [[Lookout, California|Lookout]], [[Bieber, California|Bieber]]
|notes=
}}
{{jctplace
|state=CA
|location=none
|postmile=23.20
|place=Agricultural Inspection Station (southbound only)
}}
{{CAint
|county=Siskiyou
|cspan=3
|county_note=SIS 0.00-5.04
|location=Tulelake
|postmile=1.04
|road=East West Road, E Street ([[Volcanic Legacy Scenic Byway]]), Osborne Road
|notes=
}}
{{CAint
|location=none
|postmile=5.04
|road={{jct|state=CA|SR|161|name1=State Line Road, [[Volcanic Legacy Scenic Byway]]|city1=Dorris|location2=[[Malin, Oregon|Malin]]}}
|notes=North end of SR 139
}}
{{CAint
|location=none
|postmile=5.04
|road={{jct|state=OR|OR|39|city1=Klamath Falls}}
|notes=Continuation beyond SR 161 and the Oregon state line
}}
{{Jctbtm|keys=concur}}
{{reflist|group="N"}}

==See also==
* {{portal-inline|California Roads}}

==References==
{{reflist|30em}}

==External links==
{{Attached KML|display=title,inline}}
{{commonscat}}
*[http://www.cahighways.org/137-144.html#139 California Highways: Route 139]
*[http://www.aaroads.com/california/ca-139.html California @ AARoads.com - State Route 139]
*[http://www.dot.ca.gov/hq/roadinfo/sr139 Caltrans: Route 139 highway conditions]

[[Category:State highways in California|139]]
[[Category:Roads in Lassen County, California|State Route 139]]
[[Category:Roads in Modoc County, California|State Route 139]]
[[Category:Roads in Siskiyou County, California|State Route 139]]


{{good article}}