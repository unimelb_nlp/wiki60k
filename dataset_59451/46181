{{Use dmy dates|date=January 2016}}
{{Use Australian English|date=January 2016}}
{{Infobox AFL biography
| name = Rhyian Anderson-Morley
| image = 
| fullname = Rhyian Anderson-Morley
| birth_date = {{Birth date and age|1990|06|21|df=yes}}
| birth_place = Adelaide, South Australia
| death_date = 
| death_place = 
| originalteam = [[Yarraville Seddon Eagles]]
| heightweight = 196 cm / 97&nbsp;kg
| position = Forward
| currentclub = [[Yarraville Seddon Eagles]]
| guernsey = 11
| club1 = [[Yarraville Seddon Eagles]]
| years1 = 2013–
| games_goals1 = 
| careerhighlights = 
}}

'''Rhyian Anderson-Morley''' (born 21 June 1990) is an [[Australian rules football]] player currently playing for the [[Yarraville Seddon Eagles]] in the [[Western Region Football League]] in [[Melbourne]], Australia. Rhyian was born in [[Adelaide]], South Australia but moved to Melbourne, Victoria in 2012 to play football. Anderson-Morley plays in the forward line in the Reserves squad and is also the current Team Manager for the Seniors team and a Committee Board Member of the Yarraville Seddon Eagles

== Early life==
Anderson-Morley was born in 1990, and did not begin to play [[Australian rules football]] until the age of 22 due to his thoughts growing up that "[[gay]] [[Bloke#Australian_bloke|blokes]] don't play footy".<ref>http://www.starobserver.com.au/news/sport/come-out-and-then-what/125208</ref> When Anderson-Morley moved to [[Melbourne]], he began playing football at [[Yarraville Seddon Eagles]] and began to work on his skills as well as fitness and became a regular player in the forward line in the Reserves squad.

== Coming out ==
Anderson-Morley became the second ever Australian Rules Football player to [[Coming out|come out]] publicly as gay in an article written by himself and published on the Front Page of ''[[The Age]]'' newspaper in Melbourne.<ref>http://www.theage.com.au/comment/im-gay-and-i-play-footy-whats-the-big-deal-20140531-zrt07.html</ref> Anderson-Morley cited several public statements regarding homosexuality in sport, such as [[Sam Newman]]'s comments that [[Michael Sam]] kissing his boyfriend on TV was "annoyingly gratuitous",<ref>http://www.theage.com.au/afl/afl-news/nfl-draftee-michael-sams-kiss-annoyingly-gratuitous-sam-newman-20140515-zrd41.html</ref> as encouraging him to come out so publicly so as to provide a positive role model to young gay people who would want to play sport.<ref>http://www.starobserver.com.au/news/sport/aussie-rules-player-comes-out-says-it-shouldnt-be-a-big-deal/123659</ref>

== Controversy ==
Anderson-Morley's article suffered some criticism from the [[LGBT|LGBTI]] community for his use of the word "normal" in his article. Anderson-Morley defended his comments in an interview stating that when he, and presumably other, kids realised they were gay they felt as if they were not "normal" and as he became an adult, he realised that being gay did not make him different and he could do those "normal" things he did not think he could, such as playing football.<ref>http://gaynewsnetwork.com.au/news/victoria/young-gay-footballer-explains-his-normal-remark-14043.html</ref>

== References ==
{{reflist}}

{{DEFAULTSORT:Anderson-Morley, Rhyian}}
[[Category:1990 births]]
[[Category:Australian rules footballers from Western Australia]]
[[Category:Gay sportsmen]]
[[Category:Living people]]
[[Category:LGBT sportspeople from Australia]]
[[Category:Sportspeople from Adelaide]]