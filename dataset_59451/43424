<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Ponnier M.1
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=Single seat [[fighter aircraft|fighter]]
 | national origin=[[France]]
 | manufacturer=Avions Ponnier 
 | designer=Emile Eugène Dupont
 | first flight=1915
 | introduced=1916
 | retired=November 1916
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br />  -->
 | produced= <!--years in production-->
 | number built=c.20
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Ponnier M.1''' was an early [[France|French]] [[World War I]] [[fighter aircraft]] Most of those produced were operated by the [[Aviation Militaire Belge]].  They were deemed unusable by the [[Belgium|Belgian]] ace [[Willy Coppens]] and rapidly retired.

==Design and development==
Avions Ponnier had attempted to win a pre-World War I contract from the French military with their 1913 [[Ponnier L.1]] scout, designed by Alfred Pagny, but were not successful.<ref name=Flight1/><ref name=Opdycke/> After the start of the war, they returned with a new design, clearly informed by their experience with the L.1.<ref name=Flight2/>  This aircraft, the M.1, was supposedly designed by Emile Dupont despite its similarities with the L.1; Dupont was later employed by the reformed Hanriot concern to design the 1916 [[Hanriot HD.1]] fighter.<ref name=G&S/>

The Ponnier M.1 was a [[biplane#Bays|single bay biplane]] with a pair of parallel [[interplane strut]]s on each side, braced with pairs of flying and [[flying wires|landing wires]].  There was mild [[biplane#Stagger|stagger]] but no sweep or [[dihedral (aircraft)|dihedral]].  In plan the wings were almost rectangular; the lower plane was smaller both in span and [[chord (aircraft)|chord]].  Low [[aspect ratio (wing)|aspect ratio]] [[aileron]]s were mounted on the upper planes only.<ref name=G&S/>

The M.1 was powered by a {{convert|80|hp|kW|abbr=on|0}} [[le Rhône 9C]] nine cylinder [[rotary engine]], fitted with a two blade [[propeller (aircraft)|propeller]] and an unusually large domed [[spinner (aircraft)|spinner]] which left only a small gap for cooling air between it and the almost complete cylindrical engine enclosure.  Behind the engine the [[fuselage]] was flat sided, with a curved upper decking.  The open single [[cockpit]] was at the wing [[trailing edge]], and there were cut-outs in both planes to improve the pilot's view.  The fuselage tapered aft where the horizontal tail was mounted on top of the fuselage; the original tail surfaces were very small and without a fixed [[fin]].  Later versions had an enlarged straight edged [[tailplane]] and split pair of angle tipped [[Elevator (aeronautics)|elevator]]s, a wide chord fin and enlarged [[rudder]].   The M.1 had a fixed [[conventional undercarriage]], with mainwheels on a single axle mounted on a pair of V-struts from the lower fuselage [[longeron]]s and wire cross braced. The only armament was a single [[Lewis gun]], mounted well above the upper wing surface.<ref name=G&S/>

The M.1 made its first flight in 1915.<ref name=G&S/>

==Operational history==
Despite the loss of an early example in January 1916, at least twenty M.1s were produced by [[S.A. (corporation)|S.A.]] Française de Constructions Aéronautiques, Ponnier's successor company to Avions Ponnier. Most of these, probably more than eighteen, were bought by the [[Aviation Militaire Belge]] which according to their ace Willy Coppens found them ineffective despite modifications which included the larger [[empennage]] and spinner removal. Like the few examples which remained in France, where no unit was equipped with them, they were rapidly discarded.<ref name=G&S/>

==Variants==
Details from Green and Swanborough p.&nbsp;479<ref name=G&S/>
;M.1: The single-seat fighter.
;M.2: A slightly larger two-seat version offered to the [[Royal Flying Corps|RFC]], probably not built.

==Operators==
{{BEL}}
*[[Belgian Air Force]]

==Specifications==
{{Aircraft specs
|ref=Green and Swanborough p.479<ref name=G&S/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=One
|length m=5.75
|length note=
|span m=6.18
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|height m=2.30
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=304
|empty weight note=
|gross weight kg=464
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Le Rhône 9C]]
|eng1 type=9-cylinder [[rotary engine|rotary]]
|eng1 hp=80
|eng1 note=
|power original=
|more power=

|prop blade number=2
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=167
|max speed note=at sea level
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=4.67 min to 1,000 m (3,280 ft)
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=

|power/mass=

|more performance=
<!--
        Armament
-->
|guns=1×7.7 mm (0.303 in) [[Lewis machine gun]] above upper wing

}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

<!--==Notes==-->

==References==
{{reflist|refs=

<ref name=G&S>{{cite book |last=Green |first=William |first2= Gordon|last2= Swanborough |title=The Complete Book of Fighters |date=1994|publisher=Salamander Books|location=Godalming, UK|isbn=1-85833-777-1|page=479}}</ref>

<ref name=Opdycke>{{cite book |title=French aeroplanes before the Great War |last=Opdycke |first=Leonard E.|year=1999|page=209|publisher=Shiffer Publishing Ltd|location=Atglen, PA, USA |isbn=0-7643-0752-5}}</ref>

<ref name=Flight1>{{cite magazine|date=14 August 1914|title=The Ponnier Scouting Biplane|magazine=[[Flight International|Flight]]|volume=VI|issue=38|pages=863–5|url=http://www.flightglobal.com/pdfarchive/view/1914/1914%20-%200863.html}}</ref>

<ref name=Flight2>{{cite magazine|date=16 March 1916|title=Eddies|magazine=[[Flight International|Flight]]|volume=VIII|issue=11|page=219|url=http://www.flightglobal.com/pdfarchive/view/1916/1916%20-%200219.html}}</ref>

}}

<!-- ==Further reading== -->
<!-- ==External links== -->

[[Category:French fighter aircraft 1910–1919]]