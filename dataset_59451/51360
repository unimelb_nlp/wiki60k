{{Infobox journal
| title = Fractals
| cover =
| discipline = [[Fractal]]s
| abbreviation = Fractals
| editor = M. Frame, S. S. Manna, M. M. Novak
| publisher = [[World Scientific]]
| country =
| history = 1993-present
| frequency = Quarterly
| impact = 0.448
| impact-year = 2011
| website = http://www.worldscientific.com/worldscinet/fractals
| link1 = http://www.worldscientific.com/loi/fractals
| link1-name = Online access
| ISSN = 0218-348X
| eISSN = 1793-6543
| CODEN = FRACEG
| LCCN = 93648954
| OCLC = 28056668
}}
'''''Fractals''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] devoted to explaining complex phenomena using [[fractal]] geometry and scaling. It is published by [[World Scientific]] and has explored diverse topics from [[turbulence]] and [[colloid]]al aggregation to [[stock market]]s.

== Abstracting and indexing ==
The journal is indeed and abstracted in:
{{columns-list|colwidth=30em|
* [[Science Citation Index]]
* [[Current Contents]]/Physical, Chemical & Earth Sciences
* [[Mathematical Reviews]]
* [[Inspec]]
* [[Calcium and Calcified Tissues Abstracts]]
* [[Pollution Abstracts]]
* [[Aquatic Sciences and Fisheries Abstracts]]
* [[Selected Water Resources Abstracts]]
* [[Microbiology Abstracts]]
* [[Zentralblatt MATH]]
* [[Compendex]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2011 [[impact factor]] of 0.448.<ref name=WoS>{{cite book |year=2012 |chapter=Fractals |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2012-07-11 |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{official|http://www.worldscientific.com/worldscinet/fractals}}

[[Category:Mathematics journals]]
[[Category:English-language journals]]
[[Category:World Scientific academic journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1993]]