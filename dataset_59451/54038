{{EngvarB|date=September 2014}}
{{Use dmy dates|date=September 2014}}

'''Alfred Rutter Clarke''' (17 November 1867 – 10 December 1932), generally referred to as "Rutter Clarke" or "A. Rutter Clarke", was an Australian stockbroker and investor whose company Clarke and Co., founded by his grandfather, William Clarke, operated in three states, and specialised in mining ventures.

==History==
He was born in [[Prahran, Victoria]] the son of Alfred Edward Clarke (1843–1913) and his wife Caroline (née Long) (1844–1884) and brought up in the family home, "Royston", Dandenong Road, [[Malvern, Victoria]]. He was elected to the newly formed [[Melbourne Stock Exchange]] in 1891, its youngest member.<ref>{{cite news |url=http://nla.gov.au/nla.news-article63617450 |title=The New Melbourne Stock Exchange. |newspaper=[[Illustrated Sydney News]] |date=5 December 1891 |accessdate=15 January 2013 |page=18 |publisher=National Library of Australia}}</ref> Around 1892 he moved to [[Adelaide]], bought a house "Merriwa" on North-East Road, [[Gilberton, South Australia|Gilberton]] (which he sold in 1906) and established a sharebroking company there. He returned to Melbourne in 1905 and joined Clarke and Co., which he took over from his father A. E. Clarke in 1908. He was a member of the Stock Exchange committee from 1906 to 1911 and resigned his membership of the Exchange in 1928.

In 1897 he organised the sale of the Ivanhoe mining company.<ref>{{cite news |url=http://nla.gov.au/nla.news-article87762890 |title=Interview with Mr Rutter Clarke |newspaper=[[Kalgoorlie Miner]] |location=WA |date=22 September 1897 |accessdate=14 January 2013 |page=2 |publisher=National Library of Australia}}</ref>

In 1909 he was a director of the Mararoa mine at [[Norseman, Western Australia]].

He was a director of the Great Boulder Mining Company

==Associates==
'''Richard E. P. Osborne''' (ca.1860 – 24 August 1932) left the Isle of Man in 1868 when his father started work at the [[Kapunda, South Australia|Kapunda]] copper mine, was educated at [[Whinham College]] and was a member of the Adlaide Stock Exchange from 1896 to 1924, when he joined the Adelaide office of Clarke and Co. He was a dog judge for the [[Royal Agricultural and Horticultural Society of South Australia|RAHS]] and lived at [[Aldgate, South Australia|Aldgate]].<ref>{{cite news |url=http://nla.gov.au/nla.news-article90633027 |title=Obituary |newspaper=[[The Chronicle (Adelaide)|The Chronicle]] |location=Adelaide |date=1 September 1932 |accessdate=15 January 2013 |page=47 |publisher=National Library of Australia}}</ref>

'''R. E. Wallen''' was a partner for some time of both E. A. Clarke and A. Rutter Clarke.

'''F. H. Bathurst''' was a partner then joined ''[[The Argus (Melbourne)|The Argus]]'' as finance correspondent.

'''Geoffrey Rutter Clarke''', his son was a partner

'''A. J. Taylor''', previously secretary of the Melbourne Stock Exchange, was a partner.

==Personal==
On 27 June 1895 Rutter Clarke married Edith Marion Sargood (1867–1952), a daughter of [[Frederick Thomas Sargood|Sir Frederick Sargood]].<ref>{{cite news |url=http://nla.gov.au/nla.news-article71231694 |title=Victorian Letter. |newspaper=[[Australian Town and Country Journal]] |location=NSW |date=29 June 1895 |accessdate=15 January 2013 |page=38 |publisher=National Library of Australia}}</ref> Their children were: 
*Geoffrey Rutter Clarke (3 June 1896 – 1971) married Susan May "Susie" Pender of [[Naracoorte, South Australia]] on 28 March 1923.
*Kenneth Sargood Rutter Clarke (2 February 1898 – 23 October 1955)<!--married Lysbeth ????-->
*Audrey (3 April 1903 – 1925)<ref name=obit>{{cite news |url=http://nla.gov.au/nla.news-article4513636 |title=Obituary |newspaper=[[The Argus (Melbourne)|The Argus]] |location=Melbourne |date=12 December 1932 |accessdate=15 January 2013 |page=9 |publisher=National Library of Australia}}</ref>
<!-- a son 3 June 1896-->
Mr Rutter Clarke was fond of sport and was a keen cricketer and tennis player. He wrote interesting articles on international cricket and horse racing for newspapers such as ''[[The Barrier Miner]]''.<ref>{{cite news |url=http://nla.gov.au/nla.news-article46675272 |title=Mr. A. Rutter Clarke Dead |newspaper=[[The Barrier Miner]] |location=Broken Hill, NSW |date=14 December 1932 |accessdate=14 January 2013 |page=2 |publisher=National Library of Australia}}</ref>

In 1907 he built a magnificent home, "Merriwa" on Orrong Road, [[Toorak, Victoria|Toorak]], under architect Walter Richmond Butler,<ref>http://www.slv.vic.gov.au/pictoria/gid/slv-pic-aab70778</ref> and established a large and exquisitely planned garden (he was a keen gardener, especially of [[rose]]s) which featured an expanse of indigenous flowering plants unmatched outside [[Botanical Garden]]s.<ref>{{cite news |url=http://nla.gov.au/nla.news-article90864250 |title=Cultivating Indigenous Flowers |newspaper=[[The Leader (Melbourne)|The Leader]] |location=Melbourne |date=17 November 1917 |accessdate=14 January 2013 |page=13 Edition: Town and Weekly |publisher=National Library of Australia}}</ref><ref>{{cite news |url=http://nla.gov.au/nla.news-article90188918 |title=Social Circle |newspaper=[[The Leader (Melbourne)|The Leader]] |location=Melbourne |date=18 December 1915 |accessdate=14 January 2013 |page=41 Edition: Weekly |publisher=National Library of Australia}}</ref> In February 1917 the car he was driving on High Street, [[St Kilda, Victoria|St Kilda]], struck and killed William Fitzsimmons, who had jumped down from the seat of his [[lorry]] into the path of the oncoming vehicle.<ref>{{cite news |url=http://nla.gov.au/nla.news-article1600612 |title=Casualties and Fatalities |newspaper=[[The Argus (Melbourne)|The Argus]] |location=Melbourne |date=1 March 1917 |accessdate=15 January 2013 |page=8 |publisher=National Library of Australia}}</ref> The following September he sold "Meeriwa" to Mrs. Charles Moore, the wealthy owner of [[Charles Moore and Co.]]. His next home was "Myoora",<ref>{{cite news |url=http://nla.gov.au/nla.news-article1892022 |title=Family Notices. |newspaper=[[The Argus (Melbourne)|The Argus]] |location=Melbourne |date=21 April 1923 |accessdate=15 January 2013 |page=11 |publisher=National Library of Australia}}</ref> previously the home of [[Robert Harper (Australian politician)|Robert Harper, MLA]], close to the Toorak tram terminus. Dame [[Nellie Melba]] had been a tenant of this stately mansion for five months in 1902.

His palatial [[holiday cottage|weekender]], "Ellerslie" at [[Mornington, Victoria]] previously owned by Sir Frederick Sargood, was destroyed by fire in April 1908.<ref>{{cite news |url=http://nla.gov.au/nla.news-article56877450 |title=Mr. Rutter Clarke's Residence |newspaper=[[South Australian Register]] |location=Adelaide |date=4 May 1908 |accessdate=15 January 2013 |page=6 |publisher=National Library of Australia}}</ref> Arson was suspected. A new residence was built and the property on 30 acres was sold in 1920.<ref>{{cite news |url=http://nla.gov.au/nla.news-article4578651 |title=Classified Advertising. |newspaper=[[The Argus (Melbourne)|The Argus]] |location=Melbourne |date=9 October 1920 |accessdate=15 January 2013 |page=3 |publisher=National Library of Australia}}</ref>

His last residence was "Braemar", Grandview Grove, [[Armadale, Victoria|Armadale]].<ref name=obit/>

== References ==
{{Reflist}}

{{DEFAULTSORT:Clarke, Alfred Rutter}}
[[Category:1867 births]]
[[Category:1932 deaths]]
[[Category:Australian businesspeople]]
[[Category:Australian stockbrokers]]