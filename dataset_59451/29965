{{Starbox begin
| name = Barnard's Star
}}
{{Starbox image
| image = [[Image:Barnardstar2006.jpg|250px]]
| caption = The location of Barnard's Star, ca. 2006 (south is up)
}}
{{Starbox observe
| epoch = [[J2000.0]]
| constell = [[Ophiuchus]]
| pronounce = {{IPAc-en|ˈ|b|ɑr|n|ər|d}}
| ra = {{RA|17|57|48.49803}}<ref name=hipparcos>{{cite journal|bibcode=2007A&A...474..653V|title=Validation of the new Hipparcos reduction|journal=Astronomy and Astrophysics|volume=474|issue=2|pages=653|author1=Van Leeuwen|first1=F.|year=2007|doi=10.1051/0004-6361:20078357|arxiv = 0708.1752 }}</ref>
| dec = {{DEC|+04|41|36.2072}}<ref name=hipparcos/>
| appmag_v = 9.511<ref name=koen/>
}}
{{Starbox character
| class = M4.0V<ref name=gizis>{{cite journal|bibcode=1997AJ....113..806G|title=M-Subdwarfs: Spectroscopic Classification and the Metallicity Scale|journal=Astronomical Journal |volume=113|pages=806|author1=Gizis|first1=John E.|year=1997|doi=10.1086/118302|arxiv = astro-ph/9611222 }}</ref>
| appmag_1_passband = U
| appmag_1 = 12.497<ref name=koen/>
| appmag_2_passband = B
| appmag_2 = 11.240<ref name=koen>{{cite journal|bibcode=2010MNRAS.403.1949K|title=UBV(RI)C JHK observations of Hipparcos-selected nearby stars|journal=Monthly Notices of the Royal Astronomical Society|volume=403|issue=4|pages=1949|author1=Koen|first1=C.|last2=Kilkenny|first2=D.|last3=Van Wyk|first3=F.|last4=Marang|first4=F.|year=2010|doi=10.1111/j.1365-2966.2009.16182.x}}</ref>
| appmag_3_passband = R
| appmag_3 = 8.298<ref name=koen/>
| appmag_4_passband = I
| appmag_4 = 6.741<ref name=koen/>
| appmag_5_passband = J
| appmag_5 = 5.24<ref name=2mass>{{cite journal|bibcode=2003yCat.2246....0C|title=VizieR Online Data Catalog: 2MASS All-Sky Catalog of Point Sources (Cutri+ 2003)|journal=VizieR On-line Data Catalog: II/246.  Originally published in: 2003yCat.2246....0C|volume=2246|pages=0|author1=Cutri|first1=R. M.|last2=Skrutskie|first2=M. F.|last3=Van Dyk|first3=S.|last4=Beichman|first4=C. A.|last5=Carpenter|first5=J. M.|last6=Chester|first6=T.|last7=Cambresy|first7=L.|last8=Evans|first8=T.|last9=Fowler|first9=J.|last10=Gizis|first10=J.|last11=Howard|first11=E.|last12=Huchra|first12=J.|last13=Jarrett|first13=T.|last14=Kopan|first14=E. L.|last15=Kirkpatrick|first15=J. D.|last16=Light|first16=R. M.|last17=Marsh|first17=K. A.|last18=McCallon|first18=H.|last19=Schneider|first19=S.|last20=Stiening|first20=R.|last21=Sykes|first21=M.|last22=Weinberg|first22=M.|last23=Wheaton|first23=W. A.|last24=Wheelock|first24=S.|last25=Zacarias|first25=N.|year=2003}}</ref>
| appmag_6_passband = H
| appmag_6 = 4.83<ref name=2mass/>
| appmag_7_passband = K
| appmag_7 = 4.524<ref name=2mass/>
| r-i = <!--R-I color-->
| v-r = <!--V-R color-->
| b-v = 1.713<ref name=koen/>
| u-b = 1.257<ref name=koen/>
| variable = [[BY Draconis variable|BY Draconis]]
}}
{{Starbox astrometry
| radial_v = −110.6 ± 0.2<ref name="Bobylev:arXiv1003.2160">{{cite journal |last=Bobylev |first=Vadim V. |date=March 2010 |title=Searching for Stars Closely Encountering with the Solar System |journal=Astronomy Letters |volume=36 |issue=3 |pages=220–226 |doi=10.1134/S1063773710030060 |arxiv=1003.2160 |bibcode=2010AstL...36..220B}}</ref>
| prop_mo_ra = −798.71<ref name=hipparcos/>
| prop_mo_dec = 10337.77<ref name=hipparcos/>
| parallax = 545.62
| p_error = 0.2
| parallax_footnote = <ref>This parallax measurement and the subsequent distance calculation are based on the averages of the distance measurements provided below, taking into account the accuracy of each measurement. SIMBAD suggests less precise parallax by van Leeuwen (2007) of 548.31 ± 1.51 [[Minute of arc#Symbols and abbreviations|mas]] and thus a slightly lesser distance from the Sun of 5.95 [[light-year|ly]] (1.82 [[parsec|pc]]).</ref>
| dist_ly =
| dist_pc =
| absmag_v = 13.21<ref name=koen/>
}}
{{Starbox detail
| source = <!--[source url]-->
| mass = 0.144<ref name=al36_3_220/>
| radius = {{nowrap|0.196 ± 0.008}}<ref name=aaa505_1_205/>
| gravity = <!--Surface gravity (given as the base 10 logarithm expressed in cgs units)-->
| luminosity_visual = 0.0004<ref name="Dawson" />
| luminosity_bolometric = 0.0035<ref name="Dawson" />
| temperature = 3,134 ± 102<ref name="Dawson" />
| metal = 10–32% [[Sun]]<ref name="Gizis">{{cite journal | author = Gizis, John E. |date=February 1997| title = M-Subdwarfs: Spectroscopic Classification and the Metallicity Scale | journal = The Astronomical Journal | bibcode = 1997AJ....113..806G | volume = 113 | issue = 2 | page = 820 | doi=10.1086/118302|arxiv = astro-ph/9611222 }}</ref>
| rotation = 130.4 [[day|d]]<ref name="Benedict1998" />
| age = ≈ 10 billion <ref name="Riedel2005" />
}}
{{Starbox catalog
| names = "Barnard's Runaway Star", "Greyhound of the Skies",<ref name=bis11_12_170/> [[Bonner Durchmusterung|BD]]+04°3561a, [[General Catalogue of Trigonometric Parallaxes|GCTP]]&nbsp;4098.00, [[Henry Lee Giclas|Gl]]&nbsp;140-024, [[Gliese-Jahreiss catalogue|Gliese]]&nbsp;699, [[Hipparcos Catalogue|HIP]]&nbsp;87937, [[Luyten Five-Tenths catalogue|LFT]]&nbsp;1385, [[Luyten Half-Second catalogue|LHS]]&nbsp;57, [[Luyten Two-Tenths catalogue|LTT]]&nbsp;15309, Munich&nbsp;15040, [[List of nearest stars|Proxima Ophiuchi]],<ref name=an230_77/> [[Variable star designation|V2500 Ophiuchi]], ''[[Latin language|Velox Barnardi]]'',<ref name=rukl1999/> [[Alexander N. Vyssotsky|Vyssotsky]]&nbsp;799
}}
{{Starbox reference
| Simbad = BD%2B043561a
| ARICNS = 01453
}}
{{Starbox end}}

'''Barnard's Star''' {{IPAc-en|ˈ|b|ɑr|n|ər|d}} is a very-low-mass [[red dwarf]] about six [[light-year]]s away from Earth in the [[constellation]] of [[Ophiuchus]]. It is the [[List of nearest stars|fourth-closest]] known individual star to the Sun (after the three components of the [[Alpha Centauri]] system) and the closest star in the Northern Hemisphere.<ref>http://shiva.uwp.edu/p120/astro_survey.html</ref>  Despite its proximity, at a dim [[apparent magnitude]] of about nine, it is not visible with the unaided eye; it is much brighter in the [[infrared]] than it is in [[visible light]].

The star is named after the American [[astronomer]] [[Edward Emerson Barnard|E.&nbsp;E.&nbsp;Barnard]]. He was not the first to observe the star (it appeared on Harvard University plates in 1888 and 1890), but in 1916 he measured its [[proper motion]] (which is a function of its close proximity to earth, and not of its actual [[Stellar_kinematics#Space_velocity|space velocity]]) as 10.3&nbsp;[[minute of arc|arcsecond]]s per year, which remains the largest proper motion of any star relative to the [[Sun]]. <ref name="EEB">{{cite journal
 | first=E. E. | last=Barnard
 | authorlink=Edward Emerson Barnard | year=1916
 | title=A small star with large proper motion
 | journal=Astronomical Journal | volume=29 | issue=695
 | page=181 | bibcode = 1916AJ.....29..181B
 | doi = 10.1086/104156 }}</ref>  In 2016, the [[International Astronomical Union]] organized a [[IAU Working Group on Star Names|Working Group on Star Names]] (WGSN)<ref name="WGSN">{{citation
 | url=https://www.iau.org/science/scientific_bodies/working_groups/280/
 | title=IAU Working Group on Star Names (WGSN)
 | publisher=International Astronomical Union
 | accessdate=22 May 2016 | postscript=. }}</ref> to catalogue and standardize proper names for stars. The WGSN approved the name ''Barnard's Star'' for this star on 1 February 2017 and it is now so entered in the IAU Catalog of Star Names.<ref name="IAU-CSN">{{cite web | url=http://www.pas.rochester.edu/~emamajek/WGSN/IAU-CSN.txt | title=IAU Catalog of Star Names |accessdate=23 February 2017}}</ref>

Barnard's Star is among the most studied red dwarfs because of its proximity and favorable location for observation near the [[celestial equator]].<ref name="Dawson">{{Cite journal | last1 = Dawson | first1 = P. C. | last2 = De Robertis | first2 = M. M. | doi = 10.1086/383289 | title = Barnard's Star and the M Dwarf Temperature Scale | journal = The Astronomical Journal | volume = 127 | issue = 5 | pages = 2909 | year = 2004 | pmid =  | pmc = | url = http://iopscience.iop.org/1538-3881/127/5/2909/203416.text.html|bibcode = 2004AJ....127.2909D }}</ref> Historically, research on Barnard's Star has focused on measuring its stellar characteristics, its [[astrometry]], and also refining the limits of possible [[extrasolar planet]]s. Although Barnard's Star is an ancient star, it still experiences [[flare star|star flare]] events, one being observed in 1998.

The star has also been the subject of some controversy. For a decade, from the early 1960s to the early 1970s, [[Peter van de Kamp]] claimed that there were one or more [[gas giant]]s in orbit around it. Although the presence of small terrestrial planets around Barnard's Star remains a possibility, Van de Kamp's specific claims of large gas giants were refuted in the mid-1970s.

== Overview ==
Barnard's Star is a red dwarf of the dim [[stellar classification|spectral type]] M4, and it is too faint to see without a [[telescope]]. Its [[apparent magnitude]] is 9.5. 

At 7–12&nbsp;billion years of age, Barnard's Star is considerably older than the Sun, which is 4.5&nbsp;billion years old, and it might be among the oldest stars in the [[Milky Way]] galaxy.<ref name="Riedel2005">{{cite journal
 | first=A. R. | last=Riedel |author2= Guinan, E. F.|author3= DeWarf, L. E.|author4= Engle, S. G.|author5= McCook, G. P.
 | bibcode = 2005AAS...206.0904R
 | title=Barnard's Star as a Proxy for Old Disk dM Stars: Magnetic Activity, Light Variations, XUV Irradiances, and Planetary Habitable Zones |date=May 2005
 | journal=Bulletin of the American Astronomical Society
 | volume=37 |page=442 }}</ref> Barnard's Star has lost a great deal of rotational energy, and the periodic slight changes in its brightness indicate that it rotates once in 130 days<ref name="Benedict1998" /> (the [[Sun]] rotates in 25). Given its age, Barnard's Star was long assumed to be quiescent in terms of stellar activity. In 1998, astronomers observed an intense [[stellar flare]], showing that Barnard's Star is a [[flare star]].<ref name=Flare>{{cite web
 | first=Ken | last=Croswell |date=November 2005
 | url=http://www.astronomy.com/news/2005/11/a-flare-for-barnards-star
 | accessdate=2006-08-10 | title=A Flare for Barnard's Star
 | work=Astronomy Magazine | publisher=Kalmbach Publishing Co }}</ref> Barnard's Star has the [[variable star designation]] V2500 Ophiuchi. In 2003, Barnard's Star presented the first detectable change in the [[radial velocity]] of a star caused by its motion. Further variability in the radial velocity of Barnard's Star was attributed to its stellar activity.<ref name="Kurster" />

[[File:Barnard2005.gif|thumb|left|Barnard's Star, showing position every 5 years in the period 1985–2005.]]
[[File:Near-stars-past-future-en.svg|left|thumb|350px|Distances to the [[List of nearest stars|nearest stars]] from 20,000 years ago until 80,000 years in the future.]]
The proper motion of Barnard's Star corresponds to a relative lateral speed of 90&nbsp;[[kilometers per second|km/s]]. The 10.3&nbsp;seconds of arc it travels annually amount to a quarter of a degree in a human lifetime, roughly half the angular diameter of the full Moon.<ref name="Kaler">{{cite web | first = James B. | last = Kaler | url = http://www.astro.uiuc.edu/~kaler/sow/barnard.html | title = Barnard's Star (V2500 Ophiuchi) |date=November 2005 | work = Stars | publisher = James B. Kaler | accessdate = September 7, 2006| archiveurl= https://web.archive.org/web/20060905110505/http://www.astro.uiuc.edu/~kaler/sow/barnard.html| archivedate= 5 September 2006 <!--DASHBot-->| deadurl= no}}</ref>

The radial velocity of Barnard's Star towards the Sun is measured from its [[blue shift]] to be 110&nbsp;km/s. Combined with its proper motion, this gives a [[Stellar_kinematics#Space_velocity|space velocity]] (actual velocity relative to the Sun) of 142.6 ± 0.2&nbsp;km/s.  Barnard's Star will make its closest approach to the Sun around AD 11,800, when it will approach to within about 3.75&nbsp;light-years.<ref name=al36_3_220>{{citation | last1=Bobylev | first1=V. V. | title=Searching for stars closely encountering with the solar system | journal=Astronomy Letters | volume=36 | issue=3 | pages=220–226 |date=March 2010 | doi=10.1134/S1063773710030060 | bibcode=2010AstL...36..220B |arxiv = 1003.2160 }}</ref> 

[[Proxima Centauri]] is the closest star to the Sun at a position currently 4.24 light-years distant from it.  However, despite Barnard's star's even closer pass to the Sun in 11,800 AD, it will still not then be the nearest star, since by that time [[Proxima Centauri]] will have moved to a yet nearer proximity to the Sun.<ref>{{cite journal | last=Matthews | first=R. A. J. | title=The Close Approach of Stars in the Solar Neighborhood | journal=Quarterly Journal of the Royal Astronomical Society | year=1994 | volume=35 | pages=1–9 | doi= | bibcode=1994QJRAS..35....1M | last2=Weissman | first2=P. R. | last3=Preston | first3=R. A. | last4=Jones | first4=D. L. | last5=Lestrade | first5=J.-F. | last6=Latham | first6=D. W. | last7=Stefanik | first7=R. P. | last8=Paredes | first8=J. M. }}</ref> At the time of the star's closest pass by the Sun, Barnard's Star will still be too dim to be seen with the naked eye, since its apparent magnitude will only have increased by one magnitude to about 8.5 by then, still being 2.5 magnitudes short of visibility to the naked eye.

Barnard's Star has a mass of about 0.14&nbsp;[[solar mass]]es ({{Solar mass|link=y}}),<ref name=al36_3_220/> and a radius 15% to 20% of that of the Sun.<ref name="Dawson" /><ref name="Ochsenbein">{{cite journal |last=Ochsenbein |first=F. |date=March 1982 |title=A list of stars with large expected angular diameters |journal=Astronomy and Astrophysics Supplement Series |volume=47 |pages=523–531 | bibcode = 1982A&AS...47..523O }}</ref> Thus, although Barnard's Star has roughly 150 times the mass of Jupiter ({{Jupiter mass|link=y}}), its radius is only 1.5 to 2.0 times larger, due to its much higher density. Its [[effective temperature]] is 3,100&nbsp;[[kelvin]]s, and it has a visual luminosity of 0.0004&nbsp;[[solar luminosity|solar luminosities]].<ref name ="Dawson" /> Barnard's Star is so faint that if it were at the same distance from Earth as the Sun is, it would appear only 100 times brighter than a full moon, comparable to the brightness of the Sun at 80&nbsp;[[astronomical unit]]s.<ref name="SolStation">{{cite web | url = http://www.solstation.com/stars/barnards.htm | title = Barnard's Star | publisher = Sol Station|accessdate = August 10, 2006| archiveurl= https://web.archive.org/web/20060820111502/http://www.solstation.com/stars/barnards.htm| archivedate= 20 August 2006 <!--DASHBot-->| deadurl= no}}</ref>

Barnard's Star's has 10–32% of the solar [[metallicity]].<ref name="Gizis" /> Metallicity is the proportion of stellar mass made up of elements heavier than [[helium]] and helps classify stars relative to the galactic population. Barnard's Star seems to be typical of the old, red dwarf [[population II star]]s, yet these are also generally metal-poor [[halo star]]s. While sub-solar, Barnard's Star's metallicity is higher than that of a halo star and is in keeping with the low end of the metal-rich [[disk star]] range; this, plus its high space motion, have led to the designation "intermediate population II star", between a halo and disk star.<ref name="Gizis" /><ref name="Kurster">{{cite journal | last=Kürster | first=M. | year = 2003 | title = The low-level radial velocity variability in Barnard's Star | journal = Astronomy and Astrophysics | bibcode = 2003A&A...403.1077K | doi = 10.1051/0004-6361:20030396 | volume = 403 | issue = 6 | page = 1077 | last2=Endl | first2=M. | last3=Rouesnel | first3=F. | last4=Els | first4=S. | last5=Kaufer | first5=A. | last6=Brillant | first6=S. | last7=Hatzes | first7=A. P. | last8=Saar | first8=S. H. | last9=Cochran | first9=W. D. | arxiv = astro-ph/0303528 }}</ref>

== Claims of a planetary system ==
For a decade from 1963 to about 1973, a substantial number of astronomers accepted a claim by [[Peter van de Kamp]] that he had detected, by using [[Methods of detecting extrasolar planets#Astrometry|astrometry]], a perturbation in the [[proper motion]] of Barnard's Star consistent with its having one or more planets comparable in mass with [[Jupiter]]. Van de Kamp had been observing the star from 1938, attempting, with colleagues at the [[Swarthmore College]] observatory, to find minuscule variations of one [[micrometre]] in its position on [[photographic plates]] consistent with [[perturbation (astronomy)|orbital perturbations]] that would indicate a planetary companion; this involved as many as ten people averaging their results in looking at plates, to avoid systemic individual errors.<ref name="Blunder">{{cite web | url = http://www.astrobio.net/index.php?option=com_retrospection&task=detail&id=1635 | title = The Barnard's Star Blunder |date=July 2005| work = Astrobiology Magazine | accessdate = January 26, 2014}}</ref> Van de Kamp's initial suggestion was a planet having about {{Jupiter mass|1.6}} at a distance of 4.4&nbsp;AU in a slightly eccentric orbit,<ref>{{cite journal | author = Van de Kamp, Peter. | year = 1963 | title = Astrometric study of Barnard's star from plates taken with the 24-inch Sproul refractor | journal = Astronomical Journal | volume = 68 | issue = 7 | page = 515|bibcode = 1963AJ.....68..515V|doi = 10.1086/109001 }} [http://www.webcitation.org/5ni3oNB9x?url=http://articles.adsabs.harvard.edu/cgi-bin/nph-iarticle_query?1963AJ.....68..515V%26defaultprint%3DYES%26filetype%3D.pdf Archived]</ref> and these measurements were apparently refined in a 1969 paper.<ref>{{cite journal | author = Van de Kamp, Peter. | year = 1969 | title = Parallax, proper motion acceleration, and orbital motion of Barnard's Star | journal = Astronomical Journal | volume = 74 | issue = 2 | page = 238|bibcode = 1969AJ.....74..238V|doi = 10.1086/110799 }}</ref> Later that year, Van de Kamp suggested that there were two planets of 1.1 and {{Jupiter mass|0.8}}.<ref>{{cite journal | author = Van de Kamp, Peter. | year = 1969 | title = Alternate dynamical analysis of Barnard's star | journal = Astronomical Journal | volume = 74 | issue = 8 | page = 757 | bibcode = 1969AJ.....74..757V | doi = 10.1086/110852}}</ref>

[[Image:RedDwarfPlanet.jpg|thumb|right|Artist's conception of a planet in orbit around a red dwarf]]

Other astronomers subsequently repeated Van de Kamp's measurements, and two papers in 1973 undermined the claim of a planet or planets. [[George Gatewood]] and Heinrich Eichhorn, at a different observatory and using newer plate measuring techniques, failed to verify the planetary companion.<ref>{{cite journal |author1=Gatewood, George |author2=Eichhorn, H.  |lastauthoramp=yes | year = 1973 | title = An unsuccessful search for a planetary companion of Barnard's star (BD +4 3561) | bibcode = 1973AJ.....78..769G | journal = Astronomical Journal | volume = 78 | issue = 10 | page = 769 | doi = 10.1086/111480}}</ref> Another paper published by John L. Hershey four months earlier, also using the Swarthmore observatory, found that changes in the astrometric field of various stars correlated to the timing of adjustments and modifications that had been carried out on the refractor telescope's objective lens;<ref>{{cite journal | author = John L. Hershey | year = 1973 | title = Astrometric analysis of the field of AC +65 6955 from plates taken with the Sproul 24-inch refractor | journal = Astronomical Journal | volume = 78 | issue = 6 | page = 421
| bibcode = 1973AJ.....78..421H
| doi = 10.1086/111436}}</ref> the claimed planet was attributed to an artifact of maintenance and upgrade work. The affair has been discussed as part of a broader scientific review.<ref name=Bell>{{cite web|first=George H. |last=Bell |url=http://www.public.asu.edu/~sciref/exoplnt.htm |date=April 2001 |title=The Search for the Extrasolar Planets: A Brief History of the Search, the Findings and the Future Implications, Section 2 |publisher=Arizona State University |accessdate=August 10, 2006 |archiveurl=https://web.archive.org/web/20060813111219/http://www.public.asu.edu/~sciref/exoplnt.htm |archivedate=13 August 2006 |deadurl=bot: unknown |df= }} ''Full description of the Van de Kamp planet controversy.'' </ref>

Van de Kamp never acknowledged any error and published a further claim of two planets' existence as late as 1982;<ref>{{cite journal | author = Van de Kamp, Peter. | year = 1982 | title = The planetary system of Barnard's star | journal = Vistas in Astronomy | volume = 26 | issue = 2 | page = 141 | bibcode = 1982VA.....26..141V | doi = 10.1016/0083-6656(82)90004-6 }}</ref> he died in 1995. [[Wulff Heintz]], Van de Kamp's successor at Swarthmore and an expert on [[double stars]], questioned his findings and began publishing criticisms from 1976 onwards. The two men were reported to have become estranged from each other because of this.<ref name=Swathmore>{{cite web|first=Bill |last=Kent |url=http://media.swarthmore.edu/bulletin/wp-content/archived_issues_pdf/Bulletin_2001_03.pdf |title=Barnard's Wobble |year=2001 |work=Bulletin |publisher=Swarthmore College |accessdate=June 2, 2010 |deadurl=bot: unknown |archiveurl=http://www.webcitation.org/5qBBj8cDN?url=http%3A%2F%2Fmedia.swarthmore.edu%2Fbulletin%2Fwp-content%2Farchived_issues_pdf%2FBulletin_2001_03.pdf |archivedate=June 2, 2010 |df= }} </ref>

=== Refining planetary boundaries ===
While not completely ruling out the possibility of planets, null results for planetary companions continued throughout the 1980s and 1990s, the latest based on [[interferometric]] work with the [[Hubble Space Telescope]] in 1999.<ref name = Hubble99>{{cite journal  |last=Benedict |first=G. Fritz  |title=Interferometric Astrometry of Proxima Centauri and Barnard's Star Using HUBBLE SPACE TELESCOPE Fine Guidance Sensor 3: Detection Limits for Substellar Companions  |year=1999  |journal=[[The Astronomical Journal]]  |volume=118 | issue=2 | pages=1086–1100  |arxiv=astro-ph/9905318  |doi=10.1086/300975  |bibcode=1999AJ....118.1086B|last2=McArthur  |first2=Barbara  |last3=Chappell  |first3=D. W.  |last4=Nelan  |first4=E.  |last5=Jefferys  |first5=W. H.  |last6=Van Altena  |first6=W.  |last7=Lee  |first7=J.  |last8=Cornell  |first8=D.  |last9=Shelus  |first9=P. J.  |last10=Hemenway  |first10=P. D.  |last11=Franz  |first11=Otto G.  |last12=Wasserman  |first12=L. H.  |last13=Duncombe  |first13=R. L.  |last14=Story  |first14=D.  |last15=Whipple  |first15=A. L.  |last16=Fredrick  |first16=L. W.  }}</ref> By refining the values of a star's motion, the mass and orbital boundaries for possible planets are tightened: in this way astronomers are often able to describe what types of planets cannot orbit a given star.
[[File:PIA18003-NASA-WISE-StarsNearSun-20140425-2.png|thumb|300px|left|Stars closest to the [[Sun]], including ''Barnard's Star'' (25 April 2014).<ref name="NASA-20140425a">{{cite web |last1=Clavin |first1=Whitney |last2=Harrington |first2=J.D. |title=NASA's Spitzer and WISE Telescopes Find Close, Cold Neighbor of Sun |url=http://www.nasa.gov/jpl/wise/spitzer-coldest-brown-dwarf-20140425/ |date=25 April 2014 |work=[[NASA]] |deadurl=no |archivedate=2014-04-26 |archiveurl=https://web.archive.org/web/20140426004939/http://www.nasa.gov/jpl/wise/spitzer-coldest-brown-dwarf-20140425 |accessdate=2014-04-25 }}</ref>]]
M dwarfs such as Barnard's Star are more easily studied than larger stars in this regard because their lower masses render perturbations more obvious.<ref>{{cite journal
  |last1 = Endl
  |first1 = Michael
  |last2 = Cochran
  |first2 = William D.
  |last3 = Tull
  |first3 = Robert G.
  |last4 = MacQueen
  |first4 = Phillip J.
  |year = 2003
  |title = A Dedicated M Dwarf Planet Search Using the Hobby-Eberly Telescope
  |journal = [[The Astronomical Journal]]
  |volume = 126
  |issue = 12
  |pages = 3099–107
  |bibcode = 2003AJ....126.3099E
  |arxiv = astro-ph/0308477
  |doi = 10.1086/379137
}}</ref> Gatewood was thus able to show in 1995 that planets with {{Jupiter mass|10}}  were impossible around Barnard's Star,<ref name="Bell" /> in a paper which helped refine the negative certainty regarding planetary objects in general.<ref name="Gatewood95">{{cite journal|author=George D. Gatewood|title=A study of the astrometric motion of Barnard's star |journal= Journal Astrophysics and Space Science |volume= 223|issue = 1 |year= 1995|pages= 91–98| doi=10.1007/BF00989158|url=http://www.springerlink.com/content/q722p32q2h28kp50/ |bibcode=1995Ap&SS.223...91G}}</ref> In 1999, work with the [[Hubble Space Telescope]] further excluded planetary companions of {{Jupiter mass|0.8}} with an orbital period of less than 1,000 days (Jupiter's orbital period is 4,332 days),<ref name="Hubble99" /> while Kuerster determined in 2003 that within the [[habitable zone]] around Barnard's Star, planets are not possible with an "''M''&nbsp;sin&nbsp;''i''" value<ref>"''M''&nbsp;sin&nbsp;''i''" means the mass of the planet times the sine of the angle of inclination of its orbit, and hence provides the minimum mass for the planet.</ref> greater than 7.5 times the mass of the Earth ({{Earth mass|link=y}}), or with a mass greater than 3.1 times the mass of Neptune (much lower than van de Kamp's smallest suggested value).<ref name="Kurster" />

Even though this research has greatly restricted the possible properties of planets around Barnard's Star, it has not ruled them out completely; [[terrestrial planets]] would be difficult to detect. [[NASA]]'s [[Space Interferometry Mission]], which was to begin searching for extrasolar Earth-like planets, was reported to have chosen Barnard's Star as an early search target.<ref name="SolStation" /> This mission was shut down in 2010.<ref name=SIM>{{cite web | first = James | last = Marr | url = http://archive.wikiwix.com/cache/?url=http://planetquest.jpl.nasa.gov/SIM/projectNews/projectManagerUpdates/&title=Updates%20from%20the%20Project%20Manager | title = Updates from the Project Manager | date = 8 November 2010 | publisher = NASA | accessdate = January 26, 2014}}</ref> [[ESA]]'s similar [[Darwin (ESA)|Darwin]] interferometry mission had the same goal, but was stripped of funding in 2007.<ref>{{cite web | url=http://www.esa.int/esaSC/SEMZ0E1A6BD_index_0.html | title=Darwin factsheet: Finding Earth-like planets | publisher=[[European Space Agency]] |date=October 23, 2009 | accessdate=September 12, 2011}}</ref>

== Exploration ==

=== Project Daedalus ===
{{main|Project Daedalus}}

<!--Excepting the planet controversy, the best known study of -->Barnard's Star was studied as part of [[Project Daedalus]]. Undertaken between 1973 and 1978, the study suggested that rapid, unmanned travel to another star system was possible with existing or near-future technology.<ref name="Daedalus76">{{cite journal |author1=Bond, A. |author2=Martin, A.R.  |lastauthoramp=yes | year = 1976 | title = Project Daedalus&nbsp;— The mission profile | journal = [[Journal of the British Interplanetary Society]] | url = http://md1.csa.com/partners/viewrecord.php?requester=gs&collection=TRD&recid=A7618970AH&q=project+daedalus&uid=788304424&setcookie=yes | volume = 29 | issue = 2 | page = 101 | accessdate = August 15, 2006|bibcode = 1976JBIS...29..101B }}</ref> Barnard's Star was chosen as a target partly because it was believed to have planets.<ref name=DarlingDaedalus>{{cite web | first = David | last = Darling | url = http://www.daviddarling.info/encyclopedia/D/Daedalus.html | title = Daedalus, Project |date=July 2005 | work = The Encyclopedia of Astrobiology, Astronomy, and Spaceflight | accessdate = August 10, 2006| archiveurl= https://web.archive.org/web/20060831043940/http://www.daviddarling.info/encyclopedia/D/Daedalus.html| archivedate= 31 August 2006 <!--DASHBot-->| deadurl= no}}</ref>

The theoretical model suggested that a nuclear pulse rocket employing [[nuclear fusion]] (specifically, electron bombardment of [[deuterium]] and [[helium-3]]) and accelerating for four years could achieve a velocity of 12% of the [[speed of light]]. The star could then be reached in 50 years, within a human lifetime.<ref name="DarlingDaedalus" /> Along with detailed investigation of the star and any companions, the [[interstellar medium]] would be examined and baseline astrometric readings performed.<ref name="Daedalus76" />

The initial Project Daedalus model sparked further theoretical research. In 1980, [[Robert Freitas]] suggested a more ambitious plan: a [[self-replicating spacecraft]] intended to search for and make contact with [[extraterrestrial life]].<ref name=Repro>{{cite journal | first=Robert A., Jr. | last=Freitas | title=A Self-Reproducing Interstellar Probe | journal=[[Journal of the British Interplanetary Society]] | volume=33 |date=July 1980 | pages=251–264 | url=http://www.rfreitas.com/Astro/ReproJBISJuly1980.htm | accessdate =October 1, 2008|bibcode = 1980JBIS...33..251F }}</ref> Built and launched in [[Jupiter]]'s orbit, it would reach Barnard's Star in 47 years under parameters similar to those of the original Project Daedalus. Once at the star, it would begin automated self-replication, constructing a factory, initially to manufacture exploratory probes and eventually to create a copy of the original spacecraft after 1,000 years.<ref name="Repro" />

== 1998 flare ==
In 1998 a [[stellar flare]] on Barnard's Star was detected based on changes in the [[spectral emissions]] on July 17, 1998, during an unrelated search for variations in the proper motion.  Four years passed before the flare was fully analyzed, at which point it was suggested that the flare's temperature was 8000&nbsp;K, more than twice the normal temperature of the star.<ref name="Paulson">{{cite journal | first = Diane B. | last = Paulson | year = 2006 | title = Optical Spectroscopy of a Flare on Barnard's Star | journal = Publications of the Astronomical Society of the Pacific | volume = 118 | issue = 1 | page = 227 | doi=10.1086/499497| last2 = Allred | first2 = Joel C. | last3 = Anderson | first3 = Ryan B. | last4 = Hawley | first4 = Suzanne L. | last5 = Cochran | first5 = William D. | last6 = Yelda | first6 = Sylvana | bibcode=2006PASP..118..227P|arxiv = astro-ph/0511281 }}</ref> Given the essentially random nature of flares, Diane Paulson, one of the authors of that study, noted that "the star would be fantastic for amateurs to observe".<ref name="Flare" />

[[Image:RedDwarfNASA-hue-shifted.jpg|right|thumb|Artist's conception of a [[red dwarf]]]]
The flare was surprising because intense stellar activity is not expected in stars of such age. Flares are not completely understood, but are believed to be caused by strong [[magnetic fields]], which suppress [[plasma (physics)|plasma]] [[convection]] and lead to sudden outbursts: strong magnetic fields occur in rapidly rotating stars, while old stars tend to rotate slowly. For Barnard's Star to undergo an event of such magnitude is thus presumed to be a rarity.<ref name="Paulson" /> Research on the star's periodicity, or changes in stellar activity over a given timescale, also suggest it ought to be quiescent; 1998 research showed weak evidence for periodic variation in the star's brightness, noting only one possible starspot over 130 days.<ref name="Benedict1998">{{cite journal | last= Benedict | first=G. Fritz | year = 1998 | title = Photometry of Proxima Centauri and Barnard's star using Hubble Space Telescope fine guidance senso 3 | journal = The Astronomical Journal | bibcode = 1998AJ....116..429B | volume = 116 | issue = 1 | page = 429 | doi= 10.1086/300420 | last2= McArthur | first2= Barbara | last3= Nelan | first3= E. | last4= Story | first4= D. | last5= Whipple | first5= A. L. | last6= Shelus | first6= P. J. | last7= Jefferys | first7= W. H. | last8= Hemenway | first8= P. D. | last9= Franz | first9= Otto G. | last10=Wasserman | first10=L. H. | last11=Duncombe | first11=R. L. | last12=Van Altena | first12=W. | last13=Fredrick | first13=L. W. |arxiv = astro-ph/9806276 }}</ref>

Stellar activity of this sort has created interest in using Barnard's Star as a proxy to understand similar stars. It is hoped that photometric studies of its [[X-ray]] and [[Ultraviolet|UV]] emissions will shed light on the large population of old M dwarfs in the galaxy. Such research has [[astrobiology|astrobiological]] implications: given that the habitable zones of M dwarfs are close to the star, any planets would be strongly influenced by solar flares, winds, and plasma ejection events.<ref name="Riedel2005" />

== Environment ==
Barnard's Star shares much the same neighborhood as the Sun. The neighbors of Barnard's Star are generally of red dwarf size, the smallest and most common star type. Its closest neighbor is currently the red dwarf [[Ross 154]], at 1.66&nbsp;parsecs (5.41&nbsp;light years) distance. The Sun and [[Alpha Centauri]] are, respectively, the next closest systems.<ref name=SolStation/> From Barnard's Star, the Sun would appear on the diametrically opposite side of the sky at coordinates RA={{RA|5|57|48.5}}, Dec={{DEC|−04|41|36}}, in the eastern part of the constellation [[Monoceros]]. The absolute magnitude of the Sun is 4.83, and at a distance of 1.834&nbsp;parsecs, it would be a first-magnitude star, as [[Pollux (star)|Pollux]] is from the Earth.<ref>The Sun's apparent magnitude from Barnard's Star, assuming negligible [[Extinction (astronomy)|extinction]]: <math>\begin{smallmatrix} m = 4.83 + 5\cdot((\log_{10} 1.834) - 1) = 1.15 \end{smallmatrix}</math>.</ref>

== See also ==
* [[Stars and planetary systems in fiction#Barnard.27s Star|Barnard's star in fiction]]
* [[List of nearest stars and brown dwarfs]]
* [[Stars named after people]]

== Notes and references ==
{{reflist|colwidth=30em|refs=
<ref name=an230_77>{{cite journal | title=Einweißer Stern mit bedeutender absoluter Größe | last=Perepelkin | first=E. | journal=Astronomische Nachrichten | volume=230 | issue=4 | page=77 |date=April 1927 | language=German | bibcode=1927AN....230...77P | doi=10.1002/asna.19272300406}}</ref>

<ref name=rukl1999>{{cite journal | first=Antonin | last=Rukl | year=1999 | title=Constellation Guidebook | page=158 | publisher=Sterling Publishing | isbn=0-8069-3979-6 }}</ref>

<ref name=bis11_12_170>{{cite journal | title=Barnard's Star and its Perturbations | journal=Spaceflight | volume=11–12 | page=170 | year=1969 | publisher=British Interplanetary Society }}</ref>

<ref name=aaa505_1_205>{{citation | last1=Demory | first1=B.-O. | last2=Ségransan | first2=D. | last3=Forveille | first3=T. | last4=Queloz | first4=D. | last5=Beuzit | first5=J.-L. | last6=Delfosse | first6=X. | last7=di Folco | first7=E. | last8=Kervella | first8=P. | last9=Le Bouquin | first9=J.-B. | last10=Perrier | first10=C. | last11=Benisty | first11=M. | last12=Duvert | first12=G. | last13=Hofmann | first13=K.-H. | last14=Lopez | first14=B. | last15=Petrov | first15=R. | title=Mass-radius relation of low and very low-mass stars revisited with the VLTI | journal=Astronomy and Astrophysics | volume=505 | issue=1 | pages=205–215 |date=October 2009 | doi=10.1051/0004-6361/200911976 | bibcode=2009A&A...505..205D |arxiv = 0906.0602 | display-authors=1 }}</ref>

}}

===Notes===
{{Reflist|group=nb}}

== External links ==
{{commons category}}
*{{cite web |url=http://www.solstation.com/stars/barnards.htm |title=Barnard's Star|work=SolStation}}
*{{cite web | first = David | last = Darling | url = http://www.daviddarling.info/encyclopedia/B/BarnardsStar.html | title = Barnard's Star | work = The Encyclopedia of Astrobiology, Astronomy, and Spaceflight}}
*{{cite web | last = Schmidling | first = Jack | url = http://schmidling.com/barnard.htm | title = Barnard's Star | work = Jack Schmidling Productions, Inc}} Amateur work showing Barnard's Star movement over time.
*{{cite web | first = Rick | last = Johnson | url = http://www.prairieastronomyclub.org/astrophotos/Photos%20by%20Rick%20Johnson/BS2012.GIF | title = Barnard's Star}} Animated image with frames approx. one year apart, beginning in 2007, showing the movement of Barnard's Star.
{{featured article}}
{{Sky|17|57|48.5|+|04|41|36|6}}
{{Nearest systems|2}}
{{Stars of Ophiuchus}}

[[Category:Discoveries by Edward Emerson Barnard]]
[[Category:High-proper-motion stars]]
[[Category:M-type main-sequence stars]]
[[Category:Ophiuchus (constellation)]]
[[Category:BY Draconis variables]]
[[Category:Stars with proper names]]
[[Category:Objects named with variable star designations|Ophiuchi, V2500]]
[[Category:Gliese and GJ objects|0699]]
[[Category:Durchmusterung objects|BD+04 3561A]]
[[Category:Hipparcos objects|087937]]
[[Category:Astronomical objects discovered in 1916|?]]
[[Category:Local Interstellar Cloud]]