{{italic title}}

{{Unreferenced|date=October 2010}}

[[Image:Spitfire 944 fest poster a.jpg|right|thumb|''Spitfire 944'' Festival Poster]]
'''''Spitfire 944''''' is a short [[Documentary film|documentary]] in which an 83-year-old [[World War II]] pilot views [[16mm]] footage of his 1944 [[Supermarine Spitfire|Spitfire]] crash-landing for the first time, sixty-one years after the event.
__TOC__

== Behind the scenes ==
In October 2005, filmmaker William Lorton inherited two suitcases of [[16mm]] home movies which his great uncle, James R. Savage, MD., shot while serving as a [[Flight Surgeon]] for the [[US Army Air Corps]] during [[World War II]] .  The most compelling shot in the three hours of war footage was the crash landing of a [[Supermarine Spitfire (late Merlin powered variants)#PR Mk X and PR Mk XI (types 387, 365 and 370)|Spitfire Mk XI]] fighter plane at Mount Farm Airbase in [[Great Britain]].  Being the flight surgeon at the base, Captain Savage was alerted to the impending accident and had the presence of mind to bring his movie camera to the landing strip.

Within 30 seconds of entering the Spitfire's tail number into [[Google]], the filmmaker was able to ascertain the date of the crash, the location of the crash and the name of the pilot: John S. Blyth.

The filmmaker sent a letter to the pilot requesting a general interview about [[World War II]] aviation and received a positive response.  He did not reveal the existence of the [[16mm]] footage until the interview took place about two weeks later near [[Tacoma, Washington|Tacoma]], WA.  At the end of a three-hour interview about the pilot's [[World War II]] exploits, the filmmaker asked the pilot to review "about one minute" of footage.  John S. Blyth was quite surprised to suddenly be watching his death-defying landing of 61&nbsp;years earlier.

==Critical dimensions==
The filmmaker's object was to capture what it looks like when a man is unexpectedly confronted with a [[motion picture]] of the most dangerous moment in his life, which took place during a major historical event, before the proliferation of consumer video cameras.  John S. Blyth and his family later commented that his "Spitfire Crash-Landing" story had been told so many times, some considered it a "tall tale."  No longer.

The film is complex in that it is a photographic record of a man reviewing a photographic record of his own photographic record-making process (a wartime photo-[[reconnaissance]] flight) across a timespan of 61 years.  The film includes several high-altitude [[World War II]] [[reconnaissance]] photos actually taken by the subject pilot.

== Historical context ==
Lieutenant John S. Blyth was flying with the [[United States Army Air Corps]]' 14th Photo Reconnaissance Squadron based at Mount Farm, UK.  He began the war flying a [[Lockheed Corporation|Lockheed]] [[P-38 Lightning]] variant modified for photo reconnaissance work (United States Army Air Corps designation "F-5")  His unit soon switched to the British [[Supermarine Spitfire (late Merlin powered variants)#PR Mk X and PR Mk XI (types 387, 365 and 370)|Supermarine Spitfire Mk XI]], which was better suited to high-altitude, low-temperature flight conditions than the American planes.

== Articles in the media ==
* "Love for Spitfire Served Pilot Well," ''[[Tacoma News Tribune]]'', 29 March 2007 by Soren Andersen
* "Scenes from Sundance,"  ''[[USA Today]]'', 29 January 2007 by Anthony Breznican
* "Bay Area Films Keep It Real at Sundance Festival," ''[[Oakland Tribune]]'', 16 January 2007 by Chris De Benedetti
* "Sundance Fest Unveils Shorts Program," ''[[Daily Variety]]'', 6 December 2006 by Jeff Sneider

== Awards ==
* ''Spitfire 944'' won an Honorable Mention for Short Filmmaking at the [[2007 Sundance Film Festival]].
* ''Spitfire 944'' won the Best Documentary Short Award at the 2008 [[GI Film Festival]].

== External links ==
*[http://www.sundance.org/festival/ The Official Sundance Film Festival Website]
*{{IMDb title|0823692}}
*[http://www.evidenceincamera.co.uk/ The Aerial Reconnaissance Archives (TARA) at Keele]
*{{IMDb name|0521187|William Lorton}}
{{Use dmy dates|date=October 2010}}

[[Category:2006 films]]
[[Category:American films]]
[[Category:American documentary films]]
[[Category:American short films]]
[[Category:Documentary films about aviation accidents or incidents]]
[[Category:Documentary films about World War II]]