{{Infobox book | 
| name          = Two Planets
| title_orig    = Auf zwei Planeten
| translator    = [[Hans H. Rudnick]]
| image         = Two Planets.jpg
| caption       = dust cover of the first English edition
| author        = [[Kurd Lasswitz]]
| illustrator   = 
| cover_artist  = 
| country       = [[Germany]]
| language      = [[German language|German]]
| series        = 
| genre         = [[Science fiction]]
| publisher     = Felber
| pub_date      = 1897
| english_pub_date =
| media_type    = Print ([[Hardcover]])
| pages         = iv, 421 pp
| preceded_by   = 
| followed_by   = 
}}
'''''Two Planets''''' ({{lang-de|Auf zwei Planeten}}, lit. ''On Two Planets'', [[1897 in literature|1897]]) is an influential [[science fiction]] novel postulating intelligent [[life on Mars]] by [[Kurd Lasswitz]]. It was first published in hardcover by Felber in two volumes in 1897; there have been many editions since, including abridgements by the author's son Erich Lasswitz (Cassianeum, 1948) and Burckhardt Kiegeland and Martin Molitor (Verlag Heinrich Scheffler, 1969). The 1948 abridgement, with "incidental parts" of the text taken from the 1969 version, was the basis of the first translation into English by [[Hans H. Rudnick]], published in hardcover by [[Southern Illinois University Press]] in 1971. A paperback edition followed from [[Popular Library]] in 1976.<ref>{{isfdb title|id=1045506|title=Auf zwei Planeten}}</ref>

==Summary==
A group of Arctic explorers seeking the [[North Pole]] find a [[Martian]] base there. The Martians can only operate in a polar region not because of climatic requirements, but because their spacecraft cannot withstand the rotation of the Earth at other [[latitude]]s. The aliens resemble Earth people in every respect except that they have much larger eyes, with which they can express more emotions. Their name for the inhabitants of Earth is "the small-eyed ones". Lasswitz's Martians are highly advanced, and initially peaceable; they take some of the explorers back with them to visit [[Mars]] dominated by [[Martian canal|canal]]s. The story concludes the contemporary [[battleship]] armaments race between Germany and Britain by having the Martians defeat the [[Royal Navy]].

==Mars as depicted by Lasswitz==
Lasswitz hewed closely to the description by the astronomer [[Giovanni Schiaparelli]] of [[Martian canal|Martian channels]] (''canali''), and even closer to that of [[Percival Lowell]], who viewed them as actual canals engineered by intelligent beings. Lasswitz's depiction is more reflective of the views of these astronomers than those of other science fiction stories of the era dealing with the planet, including [[H. G. Wells]]'s ''[[The War of the Worlds]]'', [[Edwin Lester Arnold]]'s ''[[Lieut. Gullivar Jones: His Vacation]]'' and [[Edgar Rice Burroughs]]'s tales of [[Barsoom]], all of which were all written in the wake of Lasswitz's book.

==Literary significance==
This novel was popular in the Germany of its day. [[Wernher von Braun]] and [[Walter Hohmann]] were inspired by reading it as children just as [[Robert H. Goddard]] was by reading ''The War of the Worlds''. While there was no English translation before 1971, [[Everett F. Bleiler]] notes that it likely influenced American genre [[Science fiction|SF]] via [[Hugo Gernsback]]: "Hugo Gernsback would have been saturated in Lasswitz's work, and Gernsback's theoretical position of technologically based liberalism and many of his little scientific crotchets resemble ideas in Lasswitz's work."<ref name=bleiler>[[Everett F. Bleiler]], ''Science-Fiction: The Early Years'', Kent State University Press, 1990, pp.422-24</ref>

==Reception==
[[Theodore Sturgeon]], reviewing that 1971 translation for ''[[The New York Times]]'', found ''Two Planets'' "curious and fascinating . . . full of quaint dialogue, heroism, decorous lovemaking, and gorgeous gadgetry."<ref>"If . . .?", ''[[The New York Times]]'', May 14, 1972.</ref> Bleiler noted that the translated text was severely abridged, losing 40% of the original text; although the quality of the translation was good, he characterized the abridgment as "a bad emasculation . . . This loss of detail results in a skeletization that omits important background and weakens motivations and plot connections.<ref name=bleiler /> [[Lester del Rey]] similarly dismissed the 1971 translation as a bowdlerization" which is "bad scholarship, . . . unfair to readers [and] grossly unfair to Lasswitz." Del Rey noted that the translation was based on a 1948 abridgment prepared by the author's son, with other modifications made by the translator.<ref>"Reading Room", ''[[If (magazine)|If]]'', June 1972, p.111</ref>

==References==
{{Reflist}}

[[Category:1897 novels]]
[[Category:German science fiction novels]]
[[Category:1890s science fiction novels]]
[[Category:Mars in fiction]]
[[Category:Planetary romances]]
[[Category:Extraterrestrial life in popular culture]]