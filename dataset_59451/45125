{{About|the Aerosmith album|the song of the same name|Just Push Play (song)}}
{{Infobox album | <!-- See Wikipedia:WikiProject_Albums -->
|  Name        = Just Push Play
|  Type        = studio
|  Artist      = [[Aerosmith]]
|  Cover       = Aerosmith - Just Push Play.JPG
|  Released    = {{Start date|2001|03|05}}<ref name="AM">{{cite web |url=http://www.allmusic.com/album/just-push-play-mw0000115632 |title=''Just Push Play'' – Aerosmith: Songs, Reviews, Credits, Awards |first=Stephen Thomas |last=Erlewine |authorlink=Stephen Thomas Erlewine |work=[[AllMusic]] |publisher=[[Rovi Corporation]] |accessdate=November 12, 2012}}</ref>
|  Recorded    = The Boneyard & The Bryer Patch, [[Long View Farm|Long View Farms Studio]], [[Ocean Way Recording|Ocean Way Studios]], Sound Techniques, [[The Village (studio)|Village Recorders]], Pearl White Studios, Whatinthewhatthe? Studios, The Studio in the Sunset Marquis<ref name="notes">{{cite AV media notes |title=Just Push Play |others=[[Aerosmith]] |year=2001 |pages=4–14 |publisher=[[Columbia Records]] |id=CK 6088 |location=U.S.A. }}</ref>
|  Genre       = [[Hard rock]], [[blues rock]]
|  Length      = {{Duration|m=48|s=56}}
|  Label       = [[Columbia Records|Columbia]]
|  Producer    = [[Steven Tyler]], [[Joe Perry (musician)|Joe Perry]], [[Mark Hudson (musician)|Mark Hudson]], [[Marti Frederiksen]]<ref name="notes" />
|  Last album  = ''[[Nine Lives (Aerosmith album)|Nine Lives]]''<br />(1997)
|  This album  = '''''Just Push Play'''''<br />(2001)
|  Next album  = ''[[Honkin' on Bobo]]''<br />(2004)
| Misc        = {{Singles
  | Name           = Just Push Play
  | Type           = studio
  | Single 1       = [[Jaded (Aerosmith song)|Jaded]]
  | Single 1 date  = December 21, 2000
  | Single 2       = [[Fly Away from Here]]
  | Single 2 date  = June 2, 2001
  | Single 3       = [[Sunshine (Aerosmith song)|Sunshine]]  
  | Single 3 date  = Mid 2001
  | Single 4       =  [[Just Push Play (song)|Just Push Play]]
  | Single 4 date  = September 2001 
  }}
}}

'''''Just Push Play''''' is the thirteenth studio album by American [[rock music|rock]] band [[Aerosmith]]. It was released on March 5, 2001, making it the first Aerosmith album of the 21st century. The album was co-produced by song collaborators [[Marti Frederiksen]] and [[Mark Hudson (musician)|Mark Hudson]]. The album's first single, "[[Jaded (Aerosmith song)|Jaded]]", became a major Top 10 hit in the U.S. and around the world.  As a result, ''Just Push Play'' was certified platinum within a month of its release.  Subsequent singles "[[Fly Away from Here]]", "[[Sunshine (Aerosmith song)|Sunshine]]", and "[[Just Push Play (song)|Just Push Play]]", though garnering some airplay, failed to impact the Hot 100 much, although the latter two charted on the U.S. [[Mainstream Rock Tracks|Mainstream Rock]] chart and the former charted on the [[Adult Top 40]].

In 2010, guitarist [[Joe Perry (musician)|Joe Perry]] revealed his distaste for the album, stating that:<ref>''[[Classic Rock (magazine)|Classic Rock]]'', issue 142, pg 48-49</ref> {{cquote|I don't think we've made a decent album in years. ''Just Push Play'' is my least favorite. When we recorded it there was never a point where all five members were in the room at the same time and Aerosmith's major strength is playing together. It was a learning experience for me: it showed me how ''not'' to make an Aerosmith record.}}

==Album cover==
The album's cover features a [[gynoid]] resembling [[Marilyn Monroe]] and was designed by [[Hajime Sorayama]]{{citation needed|date=November 2015}}.

In 2011, drummer [[Joey Kramer (musician)|Joey Kramer]] answered some questions about the album during a fan "Q & A", revealing that he does not like the cover, and that he thought the playing on the album was superb{{citation needed|date=November 2015}}.

A poster of the album cover is shown in the title character's room in the [[Nickelodeon]] TV series ''[[Drake & Josh]]''{{citation needed|date=November 2015}}.

Several songs were recorded for the album that went unused. "Ain't It True", "Easy", "Innocent Man", "I Love You Down", and "Sweet Due" can be linked as originating from these sessions. "Angel's Eye" was used for the soundtrack to "Charlie's Angels." "Face" and "Won't Let You Down" were issued as bonus tracks on later pressings of the album. These songs are listed in copyright repertories. The track "Do You Wonder" was supposedly recorded for this album, as well.

==Commercial and critical reception==
{{Album ratings
| MC = 65/100<ref name="Metacritic">{{cite web |url=http://www.metacritic.com/music/just-push-play |title=Just Push Play Reviews, Ratings, Credits, and More at Metacritic |work=[[Metacritic]] |accessdate=May 15, 2012}}</ref>
| rev1 = [[AllMusic]]
| rev1Score = {{Rating|3|5}}<ref name="AM" />
| rev2 = ''[[Blender (magazine)|Blender]]''
| rev2Score = {{Rating|2|5}}<ref name="Blender">{{cite web |url=http://www.blender.com/guide/new/52231/just-push-play.html |title=Aerosmith – ''Just Push Play'': Review |first=Ben |last=Mitchell |date=June 4, 2004 |work=Blender.com |publisher=Alpha media Group }}{{Dead link|date=May 2012}}</ref>
| rev3 = [[Canadian Online Explorer|Canoe.ca]]
| rev3Score = unfavorable<ref name="Canoe.ca">{{cite web |url=http://jam.canoe.ca/Music/Artists/A/Aerosmith/AlbumReviews/2001/03/09/770189.html |title=Album Review: Aerosmith – ''Just Push Play'' |first=Darryl |last=Sterdan |work=jam.canoe.ca |accessdate=November 19, 2010 }}</ref>
| rev4 = ''[[Entertainment Weekly]]''
| rev4Score = B<ref name="EW">{{cite news |url=http://www.ew.com/ew/article/0,,280329,00.html |title=Music Review: ''Just Push Play'' – Aerosmith |first=Chris |last=Willman |work=EntertainmentWeekly.com |accessdate=November 19, 2010 |date=March 16, 2001}}</ref>
| rev5 = ''[[NME]]''
| rev5Score = 6/10<ref name="NME">{{cite web |url=http://www.nme.com/reviews/aerosmith/4350 |title=NME Reviews – Aerosmith: ''Just Push Play'' |first=John |last=Mulvey |work=nme.com |accessdate=November 19, 2010 }}</ref>
| rev6 = [[Robert Christgau]]
| rev6Score = {{Rating-Christgau|cut}}<ref name="Christgau">{{cite web |url=http://www.robertchristgau.com/get_artist.php?name=aerosmith |title=CG: Aerosmith |last=Christgau |first=Robert |publisher=RobertChristgau.com |accessdate=May 15, 2012}}</ref>
| rev7 = ''[[Rolling Stone]]''
| rev7Score = {{Rating|3.5|5}}<ref name="RS">{{cite web |url=http://www.rollingstone.com/music/albumreviews/just-push-play-20010316 |title=''Just Push Play'' by Aerosmith |first=David |last=Fricke |authorlink=David Fricke |work=RollingStone.com |publisher=Jann S. Wenner |date=March 16, 2001 |accessdate=November 19, 2010 }}</ref>
}}

''Just Push Play'' debuted at #2 on the Billboard 200, selling over 240,000 copies in its first week.<ref>{{cite web|url=http://abcnews.go.com/Entertainment/story?id=108121|title=Chart Watch: DMB, Aerosmith, Eve - ABC News|accessdate=May 24, 2014}}</ref>

[[Metacritic]] gave the album 65 out of 100 based on 14 generally favorable reviews.<ref name="Metacritic" /> For his review of ''Just Push Play'' for [[Allmusic]], [[Stephen Thomas Erlewine]] said that it was their best album since ''[[Pump (album)|Pump]]'' in 1989, but he criticized them "for not acting their age" and said the album was not much compared to ''Pump'' and ''[[Permanent Vacation (album)|Permanent Vacation]]''. He compared their production name, "The Boneyard Boys", to [[Mick Jagger]] and [[Keith Richards]]' production name, "[[Jagger/Richards|The Glimmer Twins]]."<ref name="AM" /> Ben Mitchell of ''[[Blender (magazine)|Blender]]'' magazine said the album was better than ''Nine Lives'' by a little bit and feels the album was not with the times and was rather poor.<ref name="Blender" /> Darryl Stredan had a very strong dislike for both the album and the artist saying that they were not a band anymore, that they were instead a marketing company, and was of the opinion that they should stop producing new music.<ref name="Canoe.ca" /> Chris Willman of ''[[Entertainment Weekly]]'' said that songs to not listen to on the album are "Fly Away from Here", "Luv Lies", "Avant Garden", "Just Push Play", "Drop Dead Gorgeous", and "Outta Your Head", but the rest of the songs on the album are tolerable including several with [[string section]]s ("Jaded", "Beyond Beautiful",  and "Sunshine") and thinks the album overall is good but not great.<ref name="EW" /> In the ''[[NME]]'' magazine review of ''Just Push Play'' said that most of the album is not new, but the album was Aerosmith's first album to feature [[rap metal]] with songs like "Just Push Play" & "Outta Your Head".<ref name="NME" /> [[David Fricke]] of ''[[Rolling Stone]]'' magazine said that they are still a good band and it is lucky for them to still be around, but he also said that they had not made a "great album" since ''[[Rocks (Aerosmith album)|Rocks]]'', although it came close as did ''Pump''. He compared "Jaded" to an [[Electric Light Orchestra]] song and said that the weak spots on the album were the [[Ballad|power ballad]]s.<ref name="RS" /> [[Robert Christgau]] picked out one song from the album, "Jaded", as a choice cut.<ref name="Christgau" />

==Track listing==
{{Track listing
| total_length    = 48:59
| title1          = Beyond Beautiful
| writer1         = [[Steven Tyler]], [[Joe Perry (musician)|Joe Perry]], [[Marti Frederiksen]]
| length1         = 4:45
| title2          = [[Just Push Play (song)|Just Push Play]]
| writer2         = Tyler, [[Mark Hudson (musician)|Mark Hudson]], Steve Dudas
| length2         = 3:51
| title3          = [[Jaded (Aerosmith song)|Jaded]]
| writer3         = Tyler, Frederiksen
| length3         = 3:34
| title4          = [[Fly Away from Here]]
| writer4         = Frederiksen, Todd Chapman
| length4         = 5:01
| title5          = Trip Hoppin'
| writer5         = Tyler, Perry, Frederiksen, Hudson
| length5         = 4:27
| title6          = [[Sunshine (Aerosmith song)|Sunshine]]
| writer6         = Tyler, Perry, Frederiksen
| length6         = 3:37
| title7          = Under My Skin
| writer7         = Tyler, Perry, Frederiksen, Hudson
| length7         = 3:45
| title8          = Luv Lies
| writer8         = Tyler, Perry, Frederiksen, Hudson
| length8         = 4:26
| title9          = Outta Your Head
| writer9         = Tyler, Perry, Frederiksen
| length9         = 3:22
| title10         = Drop Dead Gorgeous
| writer10        = Tyler, Perry, Hudson
| length10        = 3:42
| title11         = Light Inside
| writer11        = Tyler, Perry, Frederiksen
| length11        = 3:34
| title12         = Avant Garden
| writer12        = Tyler, Perry, Frederiksen, Hudson
| length12        = 4:52
}}

{{Track listing
| collapsed       = yes
| headline        = International version
| extra_column    = 
| total_length    = 53:37

| title13         = Face
| writer13        = Tyler, Perry, Frederiksen
| length13        = 4:25
| title14         = [[Jaded (Aerosmith song)|Jaded]]
| note14          = video
| writer14        = Tyler, Frederiksen
| length14        = 3:47
}}

{{Track listing
| collapsed       = yes
| headline        = Japanese version
| extra_column    = 
| total_length    =

| title13         = Won't Let You Down
| writer13        = Tyler, Perry, Frederiksen
| length13        = 3:38
| title14         = [[I Don't Want to Miss a Thing]]
| writer14        = [[Diane Warren]]
| length14        = 4:58
}}

{{Track listing
| collapsed       = yes
| headline        = Japanese limited edition – Disc two
| total_length    =

| title1          = Just Push Play
| note1           = Radio Remix
| writer1         = Tyler, Hudson, Dudas
| length1         = 3:16
| title2          = [[Same Old Song and Dance]]
| note2           = Live from California Jam II '78
| writer2         = Tyler, Perry
| length2         = 5:13
| title3          = [[Draw the Line (song)|Draw the Line]]
| note3           = Live from California Jam II '78
| writer3         = Tyler, Perry
| length3         = 4:32
| title4          = [[Chip Away the Stone]]
| note4           = Live from California Jam II '78
| writer4         = [[Richard Supa]]
| length4         = 4:24
| title5          = Big Ten Inch Record
| note5           = Live from Texxas Jam '78
| writer5         = Fred Weismantel
| length5         = 3:58
| title6          = [[Lord of the Thighs]]
| note6           = Live from Texxas Jam '78
| writer6         = Tyler
| length6         = 7:13
}}
'''NB''': On the original version, roughly 45 seconds after "Avant Garden" a hidden track entitled "Under My Skin Reprise" plays for about one minute. On the international version, the track is roughly 40 seconds after "Face" and on the Japanese version after "I Don't Want to Miss a Thing".

==Personnel==
''Per liner notes<ref name="notes" /><ref>[http://www.discogs.com/Aerosmith-Just-Push-Play/release/370532 Aerosmith - ''Just Push Play'' @Discogs.com] Retrieved 9-12-2015.</ref>''
;Aerosmith
*[[Steven Tyler]] – [[Lead vocalist|lead vocals]], [[piano]], [[Squeezebox]], [[harmonica]], [[percussion]], additional [[guitar]] and additional [[Drum kit|drums]], [[conga]], [[backing vocals]] on "Drop Dead Gorgeous", [[Audio mixing (recorded music)|mixing]], [[Record producer|production]]
*[[Joe Perry (musician)|Joe Perry]] – [[guitar]], [[slide guitar]], [[pedal steel guitar]], [[hurdy-gurdy]], [[Backing vocalist|backing vocals]], lead vocals on "Drop Dead Gorgeous," mixing, production
*[[Brad Whitford]] – guitar
*[[Tom Hamilton (musician)|Tom Hamilton]] – [[Bass guitar|bass]], [[fretless bass]]
*[[Joey Kramer]] – [[drum kit|drums]]
;Additional musicians
*Jim Cox – [[piano]] on "Fly Away From Here"
*Paul Santo – [[Keyboard instrument|keyboards]] [[Kurzweil K250|Kurzweil]] on "Fly Away from Here", [[Hammond organ]] on "Avant Garden"
*[[Tower of Power]] – [[Horn section|horn]]s on "Trip Hoppin'"
*[[Dan Higgins]] – [[clarinet]], [[saxophone]] on "Trip Hoppin'"
*Chelsea Tyler – backing vocals on "Under My Skin"
*[[Paul Caruso (drummer)|Paul Caruso]] – loop programming on "Drop Dead Gorgeous"
*[[Liv Tyler]] – [[Whispering|whisper]]s on "Avant Garden"
*[[Tony Perry]] - Scratching on "Just Push Play"
;Production
*[[Marti Frederiksen]] – production, mixing, [[Sound recording and reproduction|recording]]
*[[Mark Hudson (musician)|Mark Hudson]] – production, mixing
*[[Mike Shipley]] – mixing
*Richard Chycki – recording
*[[Paul Caruso (drummer)|Paul Caruso]] – [[Audio engineering|engineering]] at the Boneyard
*Paul Santo – engineering at the Bryer Patch
*[[Bryan Carrigan]] – additional engineering
*[[David Campbell (composer)|David Campbell]] – [[Arrangement|strings arrangement]]s, except on "Sunshine", "Luv Lies", and "Avant Garden"
*Jim Cox – string arrangements on "Sunshine", "Luv Lies", and "Avant Garden", horns arrangements
*Alan Sides – engineering on strings
*Scott Gordon – engineering on horns and strings
*George Marino – [[Audio mastering|mastering]]
*[[John Kalodner]]: John Kalodner
*Lesie Langlo – [[Artists and repertoire|artist and repertoire coordination]]
*Kevin Reagan – [[Art director|art direction]], [[design]]
*Matthew Lindauer – design
*[[Mark Seliger]] – [[photography]]
*[[Hajime Sorayama]] – [[illustration]]s

==Charts==
;Album
{| class="wikitable sortable"
|-
!scope="col"|Chart (2001)
!scope="col"|Peak<br />position
|-
|Australian Albums Chart<ref>{{cite web|first=Steffen |last=Hung |url=http://australian-charts.com/showitem.asp?interpret=Aerosmith&titel=Just+Push+Play&cat=a |title=Aerosmith – ''Just Push Play'' |work=australian-charts.com |accessdate=January 14, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20121024145946/http://australian-charts.com/showitem.asp?interpret=Aerosmith&titel=Just+Push+Play&cat=a |archivedate=October 24, 2012 |df= }}</ref>
|style="text-align:center;"|27
|-
|Austrian Albums Chart<ref>{{cite web|first=Steffen |last=Hung |url=http://austriancharts.at/showitem.asp?interpret=Aerosmith&titel=Just+Push+Play&cat=a |title=Aerosmith – ''Just Push Play'' |work=austriancharts.at |accessdate=January 14, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110901005652/http://www.austriancharts.at/showitem.asp?interpret=Aerosmith&titel=Just+Push+Play&cat=a |archivedate=September 1, 2011 |df= }}</ref>
|style="text-align:center;"|7
|-
|Belgian Albums Chart (Flanders)<ref>{{cite web|url=http://www.ultratop.be/nl/showitem.asp?interpret=Aerosmith&titel=Just+Push+Play&cat=a |title=Aerosmith – ''Just Push Play'' |work=ultratop.be |accessdate=January 14, 2011}}</ref>
|style="text-align:center;"|35
|-
|Belgian Albums Chart (Wallonia)<ref>{{cite web|url=http://www.ultratop.be/fr/showitem.asp?interpret=Aerosmith&titel=Just+Push+Play&cat=a |title=Aerosmith – ''Just Push Play'' |work=ultratop.be |accessdate=January 14, 2011}}</ref>
|style="text-align:center;"|27
|-
|[[Canadian Albums Chart]]<ref name="Billboard1" />
|style="text-align:center;"|2
|-
|Danish Albums Chart<ref>{{cite web|first=Steffen |last=Hung |url=http://danishcharts.com/showitem.asp?interpret=Aerosmith&titel=Just+Push+Play&cat=a |title=Aerosmith – ''Just Push Play'' |work=danishcharts.com |accessdate=January 14, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20121025024120/http://danishcharts.com/showitem.asp?interpret=Aerosmith&titel=Just+Push+Play&cat=a |archivedate=October 25, 2012 |df= }}</ref>
|style="text-align:center;"|19
|-
|Finnish Albums Chart<ref>{{cite web|first=Steffen |last=Hung |url=http://finnishcharts.com/showitem.asp?interpret=Aerosmith&titel=Just+Push+Play&cat=a |title=Aerosmith – ''Just Push Play'' |work=finnishcharts.com |accessdate=January 14, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20121020161151/http://finnishcharts.com/showitem.asp?interpret=Aerosmith&titel=Just+Push+Play&cat=a |archivedate=October 20, 2012 |df= }}</ref>
|style="text-align:center;"|8
|-
|French Albums Chart<ref>{{cite web|first=Steffen |last=Hung |url=http://lescharts.com/showitem.asp?interpret=Aerosmith&titel=Just+Push+Play&cat=a |title=Aerosmith – ''Just Push Play'' |work=lescharts.com |accessdate=January 14, 2011}}</ref>
|style="text-align:center;"|32
|-
|German Albums Chart<ref>{{cite web |url=http://www.officialcharts.de/album.asp?artist=Aerosmith&title=Just+Push+Play&country=de |title=Album – Aerosmith, ''Just Push Play'' |work=charts.de |publisher=[[Media Control Charts|Media Control]] |accessdate=November 12, 2012}}</ref>
|style="text-align:center;"|6
|-
|Irish Albums Chart<ref>{{cite web |url=http://www.chart-track.co.uk/index.jsp?c=p%2Fmusicvideo%2Fmusic%2Farchive%2Findex_test.jsp&ct=240002&arch=t&lyr=2001&year=2001&week=12 |title=Irish Music Charts Archive – Top 75 Artist Album, Week Ending March 22, 2001 |work=chart-track.co.uk |accessdate=November 12, 2012}}</ref>
|style="text-align:center;"|55
|-
|Italian Albums Chart<ref>{{cite web|first=Steffen |last=Hung |url=http://italiancharts.com/showitem.asp?interpret=Aerosmith&titel=Just+Push+Play&cat=a |title=Aerosmith – ''Just Push Play'' |work=italiancharts.com |accessdate=January 14, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20121014065351/http://italiancharts.com/showitem.asp?interpret=Aerosmith&titel=Just+Push+Play&cat=a |archivedate=October 14, 2012 |df= }}</ref>
|style="text-align:center;"|8
|-
|[[Oricon|Japanese Albums Chart]]<ref>{{cite web|url=http://www.oricon.co.jp/prof/artist/46082/ranking/cd_album/|title=エアロスミスのCDアルバムランキング、エアロスミスのプロフィールならオリコン芸能人事典-ORICON STYLE |publisher=Oricon.co.jp |accessdate=2013-05-02}}</ref>
| style="text-align:center;"|4
|-
|Netherlands Albums Chart<ref>{{cite web|first=Steffen |last=Hung |url=http://www.dutchcharts.nl/showitem.asp?interpret=Aerosmith&titel=Just+Push+Play&cat=a |title=Aerosmith – ''Just Push Play'' |work=dutchcharts.nl |accessdate=January 14, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20121020210417/http://www.dutchcharts.nl/showitem.asp?interpret=Aerosmith&titel=Just+Push+Play&cat=a |archivedate=October 20, 2012 |df= }}</ref>
|style="text-align:center;"|32
|-
|Norwegian Albums Chart<ref>{{cite web|first=Steffen |last=Hung |url=http://norwegiancharts.com/showitem.asp?interpret=Aerosmith&titel=Just+Push+Play&cat=a |title=Aerosmith – ''Just Push Play'' |work=norwegiancharts.com |accessdate=January 14, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20121103200033/http://norwegiancharts.com/showitem.asp?interpret=Aerosmith&titel=Just+Push+Play&cat=a |archivedate=November 3, 2012 |df= }}</ref>
|style="text-align:center;"|24
|-
|[[Polish Music Charts|Polish Albums Chart]]<ref>{{cite web|url=http://olis.onyx.pl/listy/index.asp?idlisty=21&lang=en|title=OLiS: sales for the period 19.03.2001 – 25.03.2001|work=[[OLiS]]}}</ref>
| style="text-align:center;"| 24
|-
|Swedish Albums Chart<ref>{{cite web|first=Steffen |last=Hung |url=http://swedishcharts.com/showitem.asp?interpret=Aerosmith&titel=Just+Push+Play&cat=a |title=Aerosmith – ''Just Push Play'' |work=swedishcharts.com |accessdate=January 14, 2011}}</ref>
|style="text-align:center;"|19
|-
|Swiss Albums Chart<ref>{{cite web|first=Steffen |last=Hung |url=http://swisscharts.com/showitem.asp?interpret=Aerosmith&titel=Just+Push+Play&cat=a |title=Aerosmith – ''Just Push Play'' |work=swisscharts.com |accessdate=January 14, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110613110352/http://swisscharts.com/showitem.asp?interpret=Aerosmith&titel=Just+Push+Play&cat=a |archivedate=June 13, 2011 |df= }}</ref>
|style="text-align:center;"|3
|-
|UK Albums Chart<ref>{{cite web|url=http://www.officialcharts.com/archive-chart/_/3/2001-03-24/ |title=2001-03-24 Top 40 Official UK Albums Archive |work=[[Official Charts Company|officialcharts.com]] |accessdate=November 12, 2012}}</ref>
|style="text-align:center;"|7
|-
|US ''[[Billboard (magazine)|Billboard]]'' [[Billboard 200|200]]<ref name="Billboard1">{{cite web |url={{BillboardURLbyName|artist=aerosmith|chart=all}} |title=''Just Push Play'' – Aerosmith |work=[[Billboard (magazine)|billboard.com]] |accessdate=November 18, 2010 }}</ref>
|style="text-align:center;"|2
|-
|}

===Singles===
{| class="wikitable"
! rowspan="2" width="33"|Year
! width="200" rowspan="2"| Title
! colspan="10"| Peak chart positions
|-
!style="width:3em;font-size:75%"|U.S.<br><ref name="allmusic">{{cite web|url={{Allmusic|class=artist |id=p604952/charts-awards/billboard-singles |pure_url=yes}} |title=Aerosmith - Awards |publisher=AllMusic |date= |accessdate=2012-06-26}}</ref>
!style="width:3em;font-size:75%"|U.S. Rock<br><ref name="allmusic"/>
!style="width:3em;font-size:75%"|U.S. Main<br><ref name="allmusic"/>
!style="width:3em;font-size:75%"|AUS<br><ref name="AUS">[http://australian-charts.com/search.asp?search=Aerosmith&cat=s Australian peaks]</ref>
!style="width:3em;font-size:75%"|[[Ö3 Austria Top 40|AUT]]<br><ref name="AUT">[http://austriancharts.at/search.asp?search=Aerosmith&cat=s Austrian peaks]</ref>
!style="width:3em;font-size:75%"|CAN<br><ref name="rpm">[http://www.collectionscanada.gc.ca/rpm/028020-110.01-e.php?PHPSESSID=4b4hk696a43h743affk2vjggm1&q1=Aerosmith&q2=Top+Singles&interval=20 Canadian peaks]</ref>
!style="width:3em;font-size:75%"|GER<br><ref name="GER">[http://www.officialcharts.de/suche.asp?search=Aerosmith&x=0&y=0&country=de&kategorie=single German peaks]</ref>
!style="width:3em;font-size:75%"|NLD<br><ref name="NLD">[http://dutchcharts.nl/search.asp?search=Aerosmith&cat=s Dutch peaks]</ref>
!style="width:3em;font-size:75%"|SWI<br><ref name="SUI">[http://hitparade.ch/search.asp?search=Aerosmith&cat=s Swiss peaks]</ref>
!style="width:3em;font-size:75%"|UK<br><ref name="British Hit Singles & Albums">{{cite book| first= David| last=Roberts| year= 2006| title= British Hit Singles & Albums| edition= 19th| publisher= Guinness World Records Limited | location= London| isbn= 1-904994-10-5| page= 157}}</ref>
|-
|align="center" rowspan="4"|2001
|"[[Jaded (Aerosmith song)|Jaded]]"
|align="center"|7
|align="center"|1
|align="center"|6
|align="center"|51
|align="center"|47
|align="center"|6
|align="center"|48
|align="center"|54
|align="center"|30
|align="center"|13
|-
|"[[Fly Away from Here]]"
|align="center"|103
|align="center"|—
|align="center"|24
|align="center"|—
|align="center"|—
|align="center"|—
|align="center"|—
|align="center"|99
|align="center"|44
|align="center"|—
|-
|"[[Sunshine (Aerosmith song)|Sunshine]]"
|align="center"|—
|align="center"|23
|align="center"|—
|align="center"|—
|align="center"|—
|align="center"|—
|align="center"|—
|align="center"|—
|align="center"|—
|align="center"|—
|-
|"[[Just Push Play (song)|Just Push Play]]"
|align="center"|—
|align="center"|10
|align="center"|38
|align="center"|—
|align="center"|—
|align="center"|—
|align="center"|—
|align="center"|—
|align="center"|—
|align="center"|—
|-
| align="center" colspan="15" style="font-size:8pt"| "—" denotes releases that did not chart
|-
|}

==Certifications==
{| class="wikitable"
!Organization
!Level
!Date
|-
|rowspan="2"|[[RIAA]] - USA
|Gold<ref name="RIAA">{{cite web
| url=http://www.riaa.com/goldandplatinumdata.php?resultpage=1&table=SEARCH_RESULTS&action=&title=Just%20Push%20Play&artist=Aerosmith&format=&debutLP=&category=&sex=&releaseDate=&requestNo=&type=&level=&label=&company=&certificationDate=&awardDescription=&catalogNo=&aSex=&rec_id=&charField=&gold=&platinum=&multiPlat=&level2=&certDate=&album=&id=&after=&before=&startMonth=1&endMonth=1&startYear=1958&endYear=2009&sort=Artist&perPage=25
| title=Gold and Platinum Database Search
| accessdate=2009-11-24}}</ref>
|rowspan="@"|April 3, 2001
|-
|Platinum<ref name="RIAA"/>
|-
|[[British Phonographic Industry|BPI]] - UK
|Silver<ref name="BPI">{{cite web|url=http://www.bpi.co.uk/certifiedawards/search.aspx |title=Certified Awards Search |work=[[British Phonographic Industry]] |format=ASPX |deadurl=yes |archiveurl=https://web.archive.org/web/20130115055129/http://www.bpi.co.uk/certifiedawards/search.aspx |archivedate=2013-01-15 |df= }} Search for "Aerosmith" to see results.</ref>
|July 22, 2013
|-
|rowspan="2"|[[Associação Brasileira dos Produtores de Discos|ABPD]] - Brazil
|Gold<ref name="Aerosmith certifications">[http://www.abpd.org.br/certificados_interna.asp Aerosmith certifications] {{webarchive |url=https://web.archive.org/web/20110928074707/http://www.abpd.org.br/certificados_interna.asp |date=September 28, 2011 }} at ABPD</ref>
|2001
|-
|Platinum<ref name="Aerosmith certifications"/>
|2007
|}

== References ==
{{reflist}}

==External links==
*{{MusicBrainz release group|id=2fb275a3-1cd9-33b6-95e4-70fe3f092649|name=Just Push Play}}
*{{metacritic album|just-push-play}}

{{Aerosmith}}

[[Category:Aerosmith albums]]
[[Category:2001 albums]]
[[Category:Albums produced by Marti Frederiksen]]
[[Category:Albums produced by Mark Hudson (musician)]]
[[Category:Columbia Records albums]]