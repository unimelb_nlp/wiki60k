{{Italic title}}
'''''Ambix''''' is a [[peer-reviewed]] [[academic journal]] on the history of [[chemistry]] and [[alchemy]] that was established in 1937. It was not published from 1939 to 1945. It was one of the first journals of the [[history of science]] in the English-speaking world, preceded by ''[[Isis (journal)|Isis]]'' (1912) and ''[[Annals of Science]]'' (1936).<ref>pp. viii-ix, ''Alchemy and Early Modern Chemistry: Papers From Ambix'', ed. Allen G. Debus, 2004, ISBN 0-9546484-1-2.</ref> It is currently published for the [[Society for the History of Alchemy and Chemistry]] by [[Taylor & Francis]], which in 2015 purchased [[Maney Publishing]], the previous publisher of Ambix.<ref>[http://www.maney.co.uk/index.php/journals/amb Maney Publishing - Ambix], accessed online January 10, 2011.</ref> The name of the journal comes from the Greek word for a still-head (ἄμβιξ), which later gave rise to the word [[alembic]].<ref>p. 25, ''The chemical tree: a history of chemistry'', William Hodson Brock, W. W. Norton & Company, 2000, ISBN 0-393-32068-5.</ref><ref>Forbes, Robert James, p. 23, ''A Short History of the Art of Distillation: from the beginnings up to the death of Cellier Blumenthal'', Leyden: E. J. Brill, 1970, ISBN 978-90-04-00617-1.</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.maneyonline.com/loi/amb}}
* [http://www.ambix.org/ Society for the History of Alchemy and Chemistry]

[[Category:English-language journals]]
[[Category:Publications established in 1937]]
[[Category:History of science journals]]
[[Category:Maney Publishing academic journals]]
[[Category:Academic journals associated with learned and professional societies]]


{{Chem-journal-stub}}