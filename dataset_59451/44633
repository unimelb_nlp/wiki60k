{{For|the Formula team|Al Ain (Superleague Formula team)}}
{{refimprove|date=October 2014}}
{{Infobox football club
| clubname = Al-Ain
| image    = [[File:Alainnewlogo.png|150px]]
| fullname = Al-Ain Football Club<br>نادي العين لكرة القدم
| nickname = The Boss

| founded  = August {{start date and age|1968}}
| ground   = [[Hazza Bin Zayed Stadium]]
| capacity = 22,717
| Last title :Bin Zayed stadium 
| chrtitle = President
| chairman = [[Mohammed Bin Zayed Al Nahyan|Mohammed Bin Zayed]]
| manager = [[Zoran Mamić]]
| league   = [[UAE Arabian Gulf League|UAE Pro-League]]
| season   = [[2015–16 UAE Pro-League|2015–16]]
| position = [[UAE Arabian Gulf League|UAE Pro-League]], 2nd
| current  = 2014–15 Al Ain FC season
| website  = http://www.alainfc.ae
| twitter  = http://twitter.com/alainfcae/
| pattern_la1 = _alainfc1314h
| pattern_b1  = _alainfc1314h
| pattern_ra1 = _alainfc1314h
| pattern_sh1 = _alainfc1314h
| pattern_so1 = _alainfc1314h
| leftarm1    = ECECEC
| body1       = ECECEC
| rightarm1   = ECECEC
| shorts1     = ECECEC
| socks1      = ECECEC
| pattern_la2 = _alainfc1314a
| pattern_b2  = _alainfc1314a
| pattern_ra2 = _alainfc1314a
| pattern_sh2 = _alainfc1314a
| pattern_so2 = _alainfc1314a
| leftarm2    =3A3B71
| body2       =3A3B71
| rightarm2   =3A3B71
| shorts2     =3A3B71
| socks2      =3A3B71
}}

'''Al-Ain Football Club''' ({{lang-ar|نادي العين لكرة القدم}}; [[Arabic transliteration|transliterated]]: Nady al-'Ayn) or '''Al-Ain FC''' or simply '''Al-Ain''' is a professional [[Football team|football club]], based in the city of [[Al Ain]], [[Abu Dhabi (emirate)|Abu Dhabi]], United Arab Emirates. It is one of many sport sections of the multi-sports club '''Al Ain Sports and Cultural Club''' ({{lang-ar|نادي العين الرياضي الثقافي}}) '''Al Ain SCC''' for short.

The club was founded in 1968 by players from Al Ain, members of a Bahraini group of exchange students and the Sudanese community working in the United Arab Emirates.<ref name="club Foundation3">{{cite web |title=club Foundation3|url=http://www.alainclub.com/English/club/clubFoundation3.aspx |archive-url=http://archive.is/20070801210607/http://www.alainclub.com/English/club/clubFoundation3.aspx |dead-url=yes |archive-date=1 August 2007 |work=alainclub.com |publisher= |accessdate=23 June 2014}}</ref>

Al Ain is by far the [[United Arab Emirates football records|most successful club in the UAE]].<ref>{{cite web| url=http://www.fifa.com/news/y=2008/m=5/news=ain-look-the-future-780183.html|title=Al Ain look to the future| publisher=Fifa}}</ref> The team quickly gained popularity and recognition throughout the country, being the team with the most tournament titles (33 in total).<ref>{{cite web| url=http://www.emaratalyoum.com/sports/local/2014-04-14-1.666829|title=40 years of UAE Football| publisher=EmaratAlYoum}}</ref> Al Ain has won a record 12 [UAE Arabian Gulf League] titles, 6 [[UAE President's Cup|President's Cups]], 3 [[UAE Federation Cup|Federation Cups]], 1 [[UAE Arabian Gulf Cup|Arabian Gulf Cup]], a record 5 [[Arabian Gulf Super Cup|Super Cups]], two Abu Dhabi Championship Cups, one Joint League Cup, Emirati-Moroccan Super Cup, [[Gulf Club Champions Cup]] and [[AFC Champions League]]. The club is the first and only UAE side so far to win the AFC Champions League.<ref>{{cite web| url=http://www.emaratalyoum.com/sports/local/2014-04-09-1.665451|title=Al Ain "The Boss" with 58 titles| publisher=EmaratAlYoum}}</ref>

==History==

===Foundation and early years===

In 1971, a group of young men learned the rules of the game by watching British soldiers playing football and formed their own team. The first pitch was very simple and small, taking the shape of a square sandy plot of land on the main street near the Clock Roundabout in Al Ain.<ref name="club Foundation2">{{cite web |title= The Beginning|url=http://www.alainclub.com/english/club/clubFoundation2.aspx|work=alainclub.com |publisher= |accessdate=1 July 2014| archiveurl=https://web.archive.org/web/20070801210607/http://www.alainclub.com/english/club/clubFoundation2.aspx| archivedate= 1 August 2007| deadurl= no}}</ref>

In August 1971, the club was officially established, taking its name from that of the [[Al Ain|city]]. The founders thought it was necessary to have a permanent headquarters for the club and rented a house on the current Khalifa Road for club meetings. The club's founders took responsibility for all the club's affairs, from planning the stadium to cleaning the club headquarters and washing the [[football kit|kit]].<ref name="club Foundation2" /> Sheikh [[Khalifa bin Zayed Al Nahyan]] was approached for assistance and he provided the club with a permanent headquarters in the Al Jahili district and a [[Land Rover]] to serve the club and the team.<ref name="club Foundation4">{{cite web |title=club Foundation4|url=http://www.alainclub.com/english/club/clubFoundation4.aspx|work=alainclub.com |publisher= |accessdate=4 July 2014}}</ref> Al Ain made a successful debut by beating a team made up of British soldiers and went on to play friendly matches against other Abu Dhabi clubs. In 1971, the team played their first match against international opposition when they were defeated 7–1 by the Egyptian club [[Ismaily SC|Ismaily]] in a friendly match for the war effort. In 1971, a group members of the club (Hadher Khalaf Al Muhairi, Saleem Al Khudhrawi, Mohammed Khalaf Al Muhairi and Mahmoud Fadhlullah) broke away and founded Al Tadhamun Club.<ref name="club Foundation5" /> In 1971, Sheikh Khalifa bin Zayed Al Nahyan provided the club with new headquarters with modern specifications: the [[Sheikh Khalifa International Stadium|Khalifa Stadium]] in Al Sarooj district.<ref name="club Foundation4" /> In 1972, Al Ain a friendly in the UAE against the leading Brazilian team [[São Paulo FC|São Paulo]] ended in a 13–0 win for the South American visitors. In 1974 Al Ain combined with the breakaway Al Tadhamun, to form the ''Al Ain Sports Club''. The first board of directors of the club was formed after the merger under the chairmanship Mohammed Salem Al Dhaheri.<ref name="club Foundation5" />

The founders were Mohammed Saleh Bin Badooh and Khalifa Nasser Al Suwaidi, Saeed Bin Ghannoum Al Hameli, Abdullah Hazzam, Salem Hassan Al Muhairi, Abdullah and Mane'a Ajlan, Abdullah Al Mansouri, Saeed Al Muwaisi, Nasser Dhaen, Abdullah Matar, Juma Al Najem, Ibrahim Al Mahmoud, Ibrahim Rasool and Ali Al Maloud and Ali Bu Majeed, who were the members of the Bahraini group of exchange students, and Ma'moun Abdul Qader, Mahmoud Fadhlullah, Al Fateh Al Talb, Hussain Al Meerghani and Abbas Ali from the Sudanese community working in the UAE.<ref name="club Foundation3">{{cite web |title=club Foundation3|url=http://www.alainclub.com/English/club/clubFoundation3.aspx |archive-url=http://archive.is/20070801210607/http://www.alainclub.com/English/club/clubFoundation3.aspx |dead-url=yes |archive-date=1 August 2007 |work=alainclub.com |publisher= |accessdate=23 June 2014}}</ref>

===First titles and Entry to the Football League (1974–1997)===
In February 1974, the club won its first title, the Abu Dhabi League. On 13 November 1974, Sheikh Khalifa was named honorary president of Al Ain, in recognition of his continuing support for the club.<ref name="club Foundation5">{{cite web |title=club Foundation5|url=http://www.alainclub.com/english/club/clubFoundation5.aspx|work=alainclub.com |publisher= |accessdate=4 July 2014| archiveurl=https://web.archive.org/web/20070801210607/http://www.alainclub.com/English/club/clubFoundation5.aspx| archivedate= 1 August 2007| deadurl= no}}</ref> On 21 May 1975, Sheikh [[Sultan bin Zayed bin Sultan Al Nahyan|Sultan bin Zayed Al Nahyan]] was elected Chairman of Board of Directors. In 1975, Al Ain won its second Abu Dhabi League<ref name="club Foundation - 2">{{cite web |title=club Foundation&nbsp;– 2|url=http://www.alainteam.com/English/theclub/clubfoundation_2.asp|work=alainteam.com |publisher= |accessdate=31 July 2014| archiveurl=https://web.archive.org/web/20050327000748/http://www.alainteam.com/English/theclub/clubfoundation_2.asp| archivedate= 27 March 2005| deadurl= no}}</ref> In the same year on 21 March 1975, the club played its first [[UAE President's Cup|UAE President Cup]] losing 4–5 on penalties in the Round of 16 against [[Al-Shaab CSC|Al Shaab]] after drawing 1–1 in normal time. In 1975–76 season, the team participated for the first time in the [[UAE Arabian Gulf League|UAE Football League]], finishing runners-up behind [[Al Ahli Club (Dubai)|Al Ahli]]. Al Ain won its first League title in the [[1976–77 UAE Football League|1976–77 season]], after drawing 1–1 with [[Al Sharjah SC|Al Sharjah]] in the last match. In the following season, they finished runners-up to [[Al-Nasr SC (Dubai)|Al Nasr]]; Mohieddine Habita was the top scorer with 20 goals. In the 1978–79 season, Al Ain secure third place with 27 points in the league and defeated by Al Sharjah in the [[UAE President's Cup|President Cup]] final.

[[Mohammed bin Zayed Al Nahyan|Mohammed Bin Zayed Al Nahyan]] became president of Al Ain on 19 January 1979. Al Ain won the League again in the [[1980–81 UAE Football League|1980–81 season]] and lost the [[UAE President's Cup|President Cup]] final to [[Al Shabab Al Arabi Club|Al Shabab]] of Dubai. In 1983–84, the team won Joint League Cup and followed with its third League title, becoming the second with Al Ahli to have won the championship three times. The team had the strongest attack with 35 goals, and Ahmed Abdullah, with 20 goals was the joint-winner of the Arab League Golden Boot award for top corer, alongside [[Al Wasl FC|Al Wasl]] striker [[Fahad Khamees]]. This season was the first season in which foreign players were excluded from the UAE League, a restriction which was opposed by Al Ain. The team were eliminated in the ualifying stages of the [[Asian Club Championship 1986|1986]] [[Asian Club Championship]]. After winning the League title in [[1983–84 UAE Football League|1983–84]] season, Al Ain failed to win any trophies till 1989 when they won the [[UAE Federation Cup|Federation Cup]]. In the following year they reached the final of the [[UAE President's Cup|President Cup]], losing to Al Shabab.

The 1992–93 season began with several new signings: Saif Sultan ([[Al Ittihad Kalba|Ittihad Kalba]]), Salem Johar ([[Ajman Club|Ajman]]), Majed Al Owais ([[Al Thaid]]), Saeed Juma ([[Emirates Club|Emirates]]). Al Ain won their fourth League title with three games left to play, after a 5–0 win at [[Al Khaleej Club|Al Khaleej]]. In the following season, they finished second in the [[UAE Arabian Gulf League|Football League]] and were runners-up the [[Arabian Gulf Super Cup|1993 UAE Super Cup]] losing 2–1 against [[Al-Shaab CSC|Al Shaab]] of [[Sharjah (emirate)|Sharjah]]. They also reached the [[UAE President's Cup|President Cup]] final but were beaten 1–0 by Al Shabab, failing for the fourth time to win the Cup. In 1994 and 1995, Al Ain lost two [[UAE President's Cup|President Cup]] finals, finished second in the [[1994–95 UAE Football League|League]], won the [[UAE Super Cup|1995 UAE Super Cup]] and lost out in the [[1995 Asian Cup Winners' Cup|Asian Cup Winners' Cup]] second round to the  Kuwaiti team [[Kazma Sporting Club|Kazma]]. In the 1996–97 season, Al Ain were eliminated in the round of 16 of the [[UAE President's Cup|President Cup]] by [[Hatta Club|Hatta]] of Dubai and finished fourth in the [[UAE Arabian Gulf League|Football League]].

===The Golden Age (1997–2003)===

Before the start of the 1997–98 season, the honorary board was formed on 7 June 1997.<ref name="The honorary board">{{cite web |title=The honorary board|url=http://www.alainclub.com/arabic/theclub/Council.asp|work=alainteam.com |publisher= |accessdate=13 August 2014| archiveurl=https://web.archive.org/web/20041211225046/http://www.alainclub.com/arabic/theclub/Council.asp| archivedate= 11 December 2004| deadurl= yes|language=Arabic}}</ref>  After this important quantum leap, Al Ain won the [[1997–98 UAE Football League|league championship]]. In the following season, they won the [[UAE President's Cup|President Cup]] and finished runner-up in the league and secured the third place in their second appearance in [[1998–99 Asian Club Championship|Asian Club Championship]], after the [[1985 Asian Club Championship|1985]]. [[Ilie Balaci]] took charge in 1999. He led them to their sixth [[1999–00 UAE Football League|League championship]], while in the [[1999–2000 Asian Cup Winners' Cup|Asian Cup Winners' Cup]] they were eliminated by [[Al-Jaish SC (Damascus)|Al Jaish]] on the [[away goals rule]] in the first round.

In 2003, Al Ain contested the [[2002–03 AFC Champions League|AFC Champions League]] competition. In the Group stage they won all three matches, beating [[Al-Hilal FC|Al Hilal]] of [[Saudi Arabia]], [[Al Sadd SC|Al Sadd]] of [[Qatar]] and [[Esteghlal F.C.|Esteghlal]] of [[Iran]]. In the semi-final they were matched against the Chinese side [[Dalian Shide F.C.|Dalian Shide]] over two legs. In the first game, Al Ain won 4–2 at home, with [[Boubacar Sanogo]] scoring twice. In the return match in China Al Ain went 4–2 down with six minutes to play but won 7–6 on aggregate after a late goal by [[Farhad Majidi]]. The final saw Al Ain face [[BEC Tero Sasana F.C.|BEC Tero Sasana]] of [[Thailand]]. In the home leg, Al Ain prevailed 2–0 with goals from Salem Johar and [[Mohammad Omar (footballer)|Mohammad Omar]]. At the [[Rajamangala Stadium]] on 11 October, Al Ain were beaten 1–0 by Tero Sasano, but won 2–1 on aggregate to become the first Emirati club to win the Champions League.

==Grounds==
{{Main|Hazza Bin Zayed Stadium|Tahnoun bin Mohammed Stadium|Sheikh Khalifa International Stadium}}

{{Infobox stadium
| stadium_name     = Hazza Bin Zayed
| image            =<!-- Deleted image removed:  [[File:Hazza Bin Zayed Stadium.jpg|300px]] -->
| broke_ground     =
| opened = {{Start date|2014|01|14|df=y}}
| architect        = Pattern Design Limited (2014)
| seating_capacity = 25,000
| dimensions       = {{convert|45|x|50|m|abbr=on}}
}}
Al Ain first playground was set up on the main street near the Clock Roundabout. Took the shape of a square sandy plot of land.<ref name="first playground">{{cite web |title= first playground|url=http://www.alainclub.com/english/club/clubFoundation2.aspx|work=alainclub.com |publisher= |accessdate=5 July 2014| archiveurl=https://web.archive.org/web/20070801210607/http://www.alainclub.com/english/club/clubFoundation2.aspx| archivedate= 1 August 2007| deadurl= no}}</ref>

Al Ain owns three home ground, [[Tahnoun bin Mohammed Stadium|Tahnoun bin Mohammed]], [[Sheikh Khalifa International Stadium|Sheikh Khalifa International]], Hazza Bin Zayed which opened on 14 January 2014.

==Crest and colours==
The Al Jahili Castle is considered as a symbol of the club, because it reflects the history of the city and also was the formal home of Sheikh [[Zayed bin Sultan Al Nahyan]] since 1946 when he was a ruler's representative. It officially became a crest for the club in 1980.<ref name="Club Emblem">{{cite web |title=Club Emblem|url=http://alainclub.com/arabic/theclub/ClubEmblem.asp |publisher=AlAinClub.com |accessdate=2 August 2014| archiveurl=https://web.archive.org/web/20040511232621/http://alainclub.com/arabic/theclub/ClubEmblem.asp| archivedate= 11 May 2004| deadurl= no|language=Arabic}}</ref>

The team began playing in green and white in 1968. After merging with Al Tadhamun in 1974, their red color became Al Ain's from season 1974–75 till the start of season 1976–77. During the first team training camp in Morocco in 1977, a friendly tournament was held by Moroccan club [[Wydad Casablanca]] with the [[OGC Nice|Nice]],  [[Sporting Clube de Portugal|Sporting CP]], and [[R.S.C. Anderlecht|Anderlecht]]. Al Ain admired Anderlecht's purple colors, and an idea  came to change Al Ain's colors to purple. The  idea was presented to Sheikh [[Hamdan bin Mubarak Al Nahyan]], who agreed to change the club colors officially to the purple with the beginning of the season 1977–78.<ref name="The Purple Story">{{cite web |title= The Purple Story|url=http://www.alainclub.com/english/club/clubFoundation3.aspx|work=alainclub.com |publisher= |accessdate=5 July 2014| archiveurl=https://web.archive.org/web/20100114133443/http://alainclub.com/arabic/club/clubFoundation3.aspx| archivedate= 14 January 2010| deadurl= no|language=Arabic}}</ref>

<center>
<gallery>
File:AlAinClub.png|1980–11
File:Al Ain FC new logo.png|2011–14
File:Alainnewlogo.png|2014–present
</gallery>
</center>

==Players==

''As of [[UAE Arabian Gulf League]]:'' <br/>
{|
|-
| valign="top" |
{| class="wikitable"
|-
! No
! Position
! Player
! Nation
|-
{{football squad player2 |no=17|nat=UAE|name=[[Khalid Eisa]] |pos=GK}}
{{football squad player2 |no=22|nat=UAE|name=[[Mahmoud Almas]] |pos=GK}}
{{football squad player2 |no=36|nat=UAE|name=[[Dawoud Sulaiman]]|pos=GK}}
{{football squad player2 |no=40|nat=UAE|name=[[Mohammed Abo Sandah]]|pos=GK}}
{{fs break}}
{{football squad player2 |no=2|nat=UAE|name=Ali Mostafa|pos=DF}}
{{football squad player2 |no= 5|nat=UAE|name=[[Mohamed Ismail Ahmed Ismail|Ismail Ahmed]] |pos=DF|other=[[Captain (association football)|vice-captain]]}}
{{football squad player2 |no= 14|nat=UAE|name=[[Mohammed Al-Dhahri]]|pos=DF| other= on loan from [[Al Wahda FC|Al-Wahda]] }}
{{football squad player2 |no=15|nat=UAE|name=[[Khaled Abdulrahman]]|pos=DF}}
{{football squad player2 |no=19|nat=UAE|name=[[Mohanad Salem]] |pos=DF}}
{{football squad player2 |no=21|nat=UAE|name=[[Fawzi Fayez]] |pos=DF}}
{{football squad player2 |no=23|nat=UAE|name=[[Mohamed Ahmed (footballer)|Mohamed Ahmed]]|pos=DF}}
{{football squad player2 |no=25|nat=UAE|name=Abdullah Fahem|pos=DF}}
{{football squad player2 |no=31|nat=UAE|name=Ahmed Al−Farsi|pos=DF}}
{{football squad player2 |no= 37|nat=UAE|name=[[Rashed Muhayer]]|pos=DF}}
{{football squad player2 |no=50|nat=UAE|name=[[Mohammed Fayez]] |pos=DF|other=[[Captain (association football)|third captain]]}}
{{fs break}}
{{football squad player2 |no=3|nat=UAE|name=[[Dawood Ali]] |pos=MF| other= on loan from [[Al Shabab (Dubai)|Al-Shabab]] }}
{{football squad player2 |no=6|nat=UAE|name=[[Amer Abdulrahman]] |pos=MF}}
{{football squad player2 |no= 7|nat=BRA|name=[[Caio Lucas Fernandes|Caio]] |pos=MF}}
{{football squad player2 |no=10|nat=UAE|name=[[Omar Abdulrahman]] |pos=MF|other=[[Captain (association football)|captain]]}}
{{football squad player2 |no=13|nat=UAE|name=[[Ahmed Barman]] |pos=MF}}
{{football squad player2 |no=16|nat=UAE|name=[[Mohammed Abdulrahman]] |pos=MF}}
{{football squad player2 |no=18|nat=UAE|name=[[Ibrahim Diaky]] |pos=MF }}
{{football squad player2 |no=27|nat=UAE|name=Mohsen Abdullah|pos=MF}}
{{football squad player2 |no=29|nat=Korea Republic|name=[[Lee Myung-Joo]] |pos=MF}}
{{football squad player2 |no=70|nat=Colombia|name=[[Danilo Moreno Asprilla|Danilo Asprilla]] |pos=MF}}
{{football squad player2 |no=77|nat=UAE|name=[[Bandar Al-Ahbabi]] |pos=MF}}
{{football squad player2 |no=79|nat=UAE|name=Hussain Abdullah |pos=MF}}
{{fs break}}
{{football squad player2 |no=11|nat=UAE|name=[[Saeed Al-Kathiri|Saeed Al Katheeri]] |pos=FW}}
{{football squad player2 |no=20|nat=KSA|name=[[Nasser Al-Shamrani]] |pos=FW| other= on loan from [[Al-Hilal FC|Al-Hilal]] }} <ref>{{cite web|title=العين الإماراتي يضم الشمراني رسميًا|url=http://www.kooora.com/default.aspx?n=555661|website=كووورة|accessdate=18 January 2017}}</ref>
{{football squad player2 |no=26|nat=UAE|name=Khaled Khalvan|pos=FW}}
{{football squad player2 |no=35|nat=UAE|name=[[Yousef Ahmed]] |pos=FW}}
{{football squad player2 |no=52|nat=UAE|name=Ali Eid |pos=FW}}
|}
|}


===Out on loan===

{|
|-
| valign="top" |
{| class="wikitable"
|-
! No
! Position
! Player
! Nation
|-
{{football squad player2 |no=--|nat=UAE|name=[[Abdullah Ghamran]]|pos=DF|other= on loan to [[Al-Ittihad Kalba SC|Ittihad Klaba]] }}
{{football squad player2 |no=--|nat=UAE|name=[[Saqer Mohammed]]|pos=MF|other= on loan to [[Al-Ittihad Kalba SC|Ittihad Klaba]] }}
|}
|}

==Personnel==

===Current technical staff===
<!--
     Instructions how to use these templates are in the bottom
-->
{| class="wikitable"
|-
! Position
! Name
|-
|First team head coach
|{{flagicon|Croatia}} [[Zoran Mamić]]
|-
|1st Assistant coach
|{{flagicon|Croatia}} [[Damir Krznar]]
|-
|2nd Assistant coach
|{{flagicon|Croatia}} [[Dean Klafurić]]
|-
|Fitness coach
|{{flagicon|Croatia}} [[Ivan Štefanić]]
|-
|Goalkeeping coach
|{{flagicon|BIH}} [[Miralem Ibrahimović]]
|-
|First team tehnical analyst
|{{flagicon|Croatia}} [[Vedran Attias]]
|-
|Club doctor
|{{flagicon|Croatia}} Jurica Rakic
|-
|Nutritionist
|{{flagicon|MAR}} Mohsen Belhoz
|-
|Physiotherapist
|{{flagicon|Croatia}} Ivica Orsolic<br />{{flagicon|Croatia}} Marin Polonijo <br />{{flagicon|Croatia}} Bozo Sinkovic <br />{{flagicon|Egypt}} Abdul Nasser Al Juhani
|-
|U-21 team head coach
|{{flagicon|Croatia}} [[Joško Španjić]]
|-
|Team Manager
|{{flagicon|UAE}} Matar Obaid Al Sahbani
|-
|Team Supervisor
|{{flagicon|UAE}} Mohammed Obeid Hammad
|-
|Team Administrator
|{{flagicon|UAE}} Essam Abdulla
|-
|Director of football
|{{flagicon|UAE}} Sultan Rashed
{{Fb cs footer|u=August 2016 |s=[http://www.alainfc.net/press/detail/id/441  Al Ain FC]|date=March 2013}}
<!--
Template:Fb cs staff (Football - coach staff - staff)

Parameters
bg : background color. y = yes; blank = no
p  : staff position
s  : staff

Template:Fb cs footer (Football - coach staff - footer)

Parameters
u  : date of last update
s  : source

-->

===Management===
{{fb oi header}}
{{Fb oi information |bg=  |id=President |i={{flagicon|UAE}} [[Mohammed Bin Zayed Al Nahyan]]}}
{{Fb oi information |bg=  |id=Vice President |i={{flagicon|UAE}} [[Hazza bin Zayed bin Sultan Al Nahyan|Hazza Bin Zayed Al Nahyan]]}}
{{Fb oi information |bg=  |id=Chairman of Board of Directors |i={{flagicon|UAE}} Ghanem Mubarak Al Hajeri}}
{{Fb oi information |bg=  |id=Vice Chairman of Board of Directors |i={{flagicon|UAE}} Ahmed Humaid Al Mazroui }}
{{Fb oi information |bg=  |id=Board of Directors Member |i={{flagicon|UAE}} Mohammed Obeid Hammad }}
{{Fb oi information |bg=  |id=Board of Directors Member |i={{flagicon|UAE}} Sultan Rashed Al Kalbani }}
{{Fb oi information |bg=  |id=Board of Directors Member |i={{flagicon|UAE}} Ali Msarri Al Dhaheri  }}
{{Fb oi information |bg=  |id=Honorary President |i={{flagicon|UAE}} [[Sultan bin Zayed bin Sultan Al Nahyan|Sultan bin Zayed Al Nahyan]]  }}
{{Fb oi footer|u=August 2016 |s=[http://www.alainfc.net/page/index/id/7?link=about Al Ain FC] |date=May 2011}}

==Honours==
'''32''' official Championships

===Domestic competitions===
* '''[[UAE Arabian Gulf League|League]]'''<ref name="Club Milestones">{{cite web|url=http://www.alainfc.net/page/index/id/8?link=about|title=Club Milestones |publisher=Al Ain FC |date= |accessdate=}}</ref><ref>{{cite web|url=http://www.rsssf.com/tablesu/uaecuphist.html|title=List of Cup Winners|publisher=[[Rec.Sport.Soccer Statistics Foundation|RSSSF]] |date= |accessdate=}}</ref>
:'''Winners (12) (record):''' [[1976–77 UAE Football League|1976–77]], [[1980–81 UAE Football League|1980–81]], [[1983–84 UAE Football League|1983–84]], [[1992–93 UAE Football League|1992–93]], [[1997–98 UAE Football League|1997–98]], [[1999–2000 UAE Football League|1999–00]], [[2001–02 UAE Football League|2001–02]], [[2002–03 UAE Football League|2002–03]], [[2003–04 UAE Football League|2003–04]], [[2011–12 UAE Pro-League|2011–12]], [[2012–13 UAE Pro-League|2012–13]], [[2014–15 UAE Pro-League|2014–15]]
:''Runners-up (8):''<ref>{{cite web|url=http://www.rsssf.com/tablesu/uaechamp.html |title=List of Champions |publisher=[[Rec.Sport.Soccer Statistics Foundation|RSSSF]] |date= |accessdate= |deadurl=yes |archiveurl=https://web.archive.org/web/20140126042936/http://www.rsssf.com:80/tablesu/uaechamp.html |archivedate=26 January 2014 |df=dmy }}</ref> [[1975–76 UAE Football League|1975–76]], [[1977–78 UAE Football League|1977–78]], [[1981–82 UAE Football League|1981–82]], [[1993–94 UAE Football League|1993–94]], [[1994–95 UAE Football League|1994–95]], [[1998–99 UAE Football League|1998–99]], [[2004–05 UAE Football League|2004–05]], [[2015–16 UAE Pro-League|2015–16]]
* '''[[UAE President's Cup|President's Cup]]'''<ref name="Club Milestones" />
:'''Winners (6):''' 1999, 2001, 2005, 2006, [[2008–09 UAE President's Cup|2009]], 2014
:''Runners-up (7):'' 1979, 1981, 1990, 1994, 1995, 2007, 2016
* '''[[Federation Cup (United Arab Emirates)|Federation Cup]]'''<ref name="Club Milestones" /> (Defunct)
:'''Winners (3):''' 1989, 2005, 2006
* '''[[UAE Arabian Gulf Cup|League Cup]]'''<ref name="Club Milestones" />
:'''Winners (1):''' [[2008–09 Etisalat Emirates Cup|2008–09]]
:''Runners-up (1):'' [[2010–11 Etisalat Emirates Cup|2010–11]]
* '''[[Arabian Gulf Super Cup|Super Cup]]'''<ref name="Club Milestones" />
:'''Winners (5) (record):''' 1995, 2003, 2009, 2012, 2015
:''Runners-up (3):''1993, 2002, [[2013 Arabian Gulf Super Cup|2013]]
* '''Abu Dhabi Championship Cup'''<ref name="Club Milestones" />
:'''Winners (2):''' 1974, 1975
*'''[[United Arab Emirates football records#Joint League Cup|Joint League Cup]]'''<ref name="Club Milestones" /><ref name="Joint League">{{cite web|url=http://www.uaefa.ae/index.php?got=local_champ7|title=Joint League|publisher=UAEFA.ae|language=ar-AR}}</ref>
:'''Winners (1):''' 1983

===Regional competitions===
* '''[[Gulf Club Champions Cup]]'''<ref name="Club Milestones" />
:'''Winners (1) :''' 2001

===Continental competitions===
* '''[[AFC Champions League]]'''<ref name="Club Milestones" />
:'''Winners (1) :''' [[2002–03 AFC Champions League|2003]]
:''Runners-up (2) : '' [[2005 AFC Champions League|2005]], [[2016 AFC Champions League|2016]]

===Friendly competitions===
* '''Emirati-Moroccan Friendship Super Cup'''
:'''Winners (1):''' 2015

==Managerial history==
{{unreferenced section|date=September 2015}}
{{col-begin-small}}
{{col-2}}
{| class="wikitable" style="width:99%; text-align:center; text-align:centre;"
|-
! No.
! Nationality
! Head coach
! From
! Until
! Honours
|-
|1
|{{flagicon|UAE}}
|align=center| {{sort|Dhaen, Nasser|Nasser Dhaen (No such name in FIFA)}}<sup>*</sup>
||1968||1971 {{citation needed|date=July 2016}}
|
|-
|2
|{{flagicon|EGY}}
|align=center| {{sort|Hammami, Abdel Aziz|Abdel Aziz Hammami}}
||1971||1973
|
|-
|3
|{{flagicon|Syria}}
|align=center| {{sort|Hajeer, Ahmed|Ahmed Hajeer}}
||1973||1976
|
|-
|4
|{{flagicon|TUN}}
|align=center| {{sort|Dhib, Hamid|[[Ahmed Dhib|Hamid Dhib]]}}
||1976||1976
|
|-
|5
|{{flagicon|Syria}}
|align=center| {{sort|Alyan, Ahmed|Ahmed Alyan}}
||1976||1979
|1 Championship
|-
|6
|{{flagicon|TUN}}
|align=center| {{sort|Chetali, Abdelmajid|[[Abdelmajid Chetali]]}}
||1979||1980
|
|-
|7
|{{flagicon|MAR}}
|align=center| {{sort|Nagah, Ahmed|Ahmed Nagah}}
||1980||1982
|1 Championship
|-
|8
|{{flagicon|BRA}}
|align=center| {{sort|Rosa, Nelsinho|Nelsinho Rosa}}
||1982||1984
|1 Championship
|-
|9
|{{flagicon|Yugoslavia}}
|align=center| {{sort|Milianich, Milian|Milian Milianich}}
||1984||1986
|
|-
|10
|{{flagicon|BRA}}
|align=center| {{sort|Picerni, Jair|[[Jair Picerni]]}}
||1986||1986
|
|-
|11
|{{flagicon|BRA}}
|align=center| {{sort|Francisco, João|[[João Francisco]]}}
||1986||1988
|
|-
|12
|{{flagicon|BRA}}
|align=center| {{sort|Mario, Zé|[[Zé Mário (footballer, born 1949)|Zé Mario]]}}
||1988||1990
|1 Federation Cup
|-
|13
|{{flagicon|ALG}}
|align=center| {{sort|Khalef, Mahieddine|[[Mahieddine Khalef]]}}
||1990||1992
|
|-
|14
|{{flagicon|EGY}}
|align=center| {{sort|Abdul Ghani, Yusri|Yusri Abdul Ghani}}
||1992||1992
|
|-
|15
|{{flagicon|BRA}}
|align=center| {{sort|Amarildo|[[Amarildo Tavares da Silveira|Amarildo]]}}
||1992||1995
|1 Championship
|-
|16
|{{flagicon|EGY}}
|align=center| {{sort|Abdel-Fattah, Shaker|Shaker Abdel-Fattah}}
||1995||1995
|1 Supercup
|-
|17
|{{flagicon|ARG}}
|align=center| {{sort|Marcos, Ángel|[[Ángel Marcos]]}}
||1995||1996
|
|-
|18
|{{flagicon|BRA}}
|align=center| {{sort|Sandri, Lori|[[Lori Sandri]]}}
||1996||1996
|
|-
|19
|{{flagicon|BRA}}
|align=center| {{sort|Cabral, Carlos|[[Carlos Roberto Cabral|Cabralzinho]]}}
||1997||1997
|
|-
|20
|{{flagicon|EGY}}
|align=center| {{sort|Abdel-Fattah, Shaker|Shaker Abdel-Fattah}}
||1997||1998
|1 Championship
|-
|21
|{{flagicon|POR}}
|align=center| {{sort|Vingada, Nelo|[[Nelo Vingada]]}}
||1998||1999
|1 President's Cup
|-
|22
|{{flagicon|ROM}}
|align=center| {{sort|Balaci, Ilie|[[Ilie Balaci]]}}
||1999||2000
|1 Championship
|-
|23
|{{flagicon|ARG}}
|align=center| {{sort|Fulloné, Oscar|[[Oscar Fulloné]]}}
||2000||2000
|
|-
|24
|{{flagicon|TUN}}
|align=center| {{sort|Mahjoub, Mrad|Mrad Mahjoub}}
||2000||2001
|1 Gulf Club Champions Cup
|-
|25
|{{flagicon|ROM}}
|align=center| {{sort|Iordănescu, Anghel|[[Anghel Iordănescu]]}}
||2001||2002
|1 President's Cup
|-
|26
|{{flagicon|UAE}}
|align=center| {{sort|Abdullah, Ahmed|Ahmed Abdullah}}<sup>*</sup>
||2002||2002
|
|-
|27
|{{flagicon|BIH}}
|align=center| {{sort|Haji, Jamal|[[Džemal Hadžiabdić|Jamal Haji]]}}
||Jan 2002||2002
|1 Championship
|}
{{col-2}}
{| class="wikitable" style="width:99%; text-align:center;  text-align:centre;"
|-
! No.
! Nationality
! Head coach
! From
! Until
! Honours
|-
|28
|{{flagicon|FRA}}
|align=center| {{sort|Metsu, Bruno|[[Bruno Metsu]]}}
||Aug 2002||May 2004
| 2 Championships, <br>1 Champions League, <br>1 Supercup
|-
|29
|{{flagicon|FRA}}
|align=center| {{sort|Perrin, Alain|[[Alain Perrin]]}}
||July 2004||Oct 2004
|
|-
|30
|{{flagicon|TUN}}
|align=center| {{sort|El Mansi, Mohammad|Mohammad El Mansi}}<sup>*</sup>
||Oct 2004||Jan 2005
|1 Federation Cup
|-
|31
|{{flagicon|CZE}}
|align=center| {{sort|Máčala, Milan|[[Milan Máčala]]}}
||Jan 2005||Jan 2006
|1 President's Cup
|-
|32
|{{flagicon|TUN}}
|align=center| {{sort|El Mansi, Mohammad|Mohammad El Mansi}}<sup>*</sup>
||2006||2006
||1 President's Cup, <br>1 Federation Cup
|-
|33
|{{flagicon|ROM}}
|align=center| {{sort|Iordănescu, Anghel|[[Anghel Iordănescu]]}}
||June 2006||Dec 2006
|
|-
|34
|{{flagicon|NED}}
|align=center| {{sort|Ruys, Tiny|[[Tiny Ruys]]}}<sup>*</sup>
||2006||2007
|
|-
|35
|{{flagicon|ITA}}
|align=center| {{sort|Zenga, Walter|[[Walter Zenga]]}}
||Jan 2007||June 2007
|
|-
|36
|{{flagicon|BRA}}
|align=center| {{sort|Tite|[[Tite (football manager)|Tite]]}}
||July 2007||Dec 2007
|
|-
|37
|{{flagicon|GER}}
|align=center| {{sort|Schäfer, Winfried|[[Winfried Schäfer]]}}
||2007||Dec 2009
|1 Cup, <br>1 President's Cup, <br>1 Supercup
|-
|38
|{{flagicon|MAR}}
|align=center| {{sort|Mahmoud, Rasheed|Rasheed Mahmoud}}<sup>*</sup>
||Dec 2009||Dec 2009
|
|-
|39
|{{flagicon|BRA}}
|align=center| {{sort|Cerezo, Toninho|[[Toninho Cerezo]]}}
||Dec 2009||April 2010
|
|-
|40
|{{flagicon|UAE}}
|align=center| {{sort|Al Mistaki, Abdul Hameed|Abdul Hameed Al Mistaki}}<sup>*</sup>
||April 2010||Dec 2010
|
|-
|41
|{{flagicon|UAE}}
|align=center| {{sort|Abdullah, Ahmed|Ahmed Abdullah}}<sup>*</sup>
||2010||2010
|
|-
|42
|{{flagicon|BRA}}
|align=center| {{sort|Gallo, Alexandre|[[Alexandre Gallo]]}}
|| 21 Dec 2010||6 June 2011
|
|-
|43
|{{flagicon|ROM}}
|align=center| {{sort|Olăroiu, Cosmin|[[Cosmin Olăroiu]]}}
||6 June 2011||6 July 2013
|2 Championships, <br>1 Supercup
|-
|44
|{{flagicon|Uruguay}}
|align=center| {{sort|Fossati, Jorge|[[Jorge Fossati]]}}
||29 July 2013||13 Sept 2013
|
|-
|45
|{{flagicon|UAE}}
|align=center| {{sort|Abdullah, Ahmed|Ahmed Abdullah}}<sup>*</sup>
||13 Sept 2013||27 Sept 2013
|
|-
|46
|{{flagicon|Spain}}
|align=center| {{sort|Sánchez Flores, Quique|[[Quique Sánchez Flores]]}}
||27 Sept 2013||8 March 2014
|
|-
|47
|{{flagicon|Croatia}}
|align=center| {{sort|Dalić, Zlatko|[[Zlatko Dalić]]}}
||8 March 2014||23 January 2017
|1 Championship,<br>1 President's Cup,<br>1 Supercup
|}
{{col-end}}

<sup>*</sup> <small>Served as caretaker coach.</small>

==Top scorers==
''Note: this includes goals scored in '''all''' competitions.''<ref name="Top Scorers">{{cite web |title=Top Scorers|url=http://www.alainteam.com/Records/bin-lib/Top.asp|work=alainteam.com |publisher= |accessdate=17 July 2014| archiveurl=https://web.archive.org/web/20040120172628/http://www.alainteam.com/Records/bin-lib/Top.asp| archivedate= 20 January 2004| deadurl= no}}</ref>
{| class="wikitable sortable"
|-
! No.
! Nationality
! Player
! Goals
|-
|1
|{{flagicon|UAE}}
|Ahmed Abdullah
|180
|-
|2
|{{flagicon|GHA}}
|[[Asamoah Gyan]]
|128
|-
|3
|{{flagicon|UAE}}
|Matar Al Sahbani
|93
|-
|4
|{{flagicon|TUN}}
|Mohieddine Habita
|71
|-
|5
|{{flagicon|UAE}}
|Majid Al Owais
|70
|-
|6
|{{flagicon|UAE}}
|Salem Johar
|60
|-
|7
|{{flagicon|UAE}}
|Saif Sultan
|55
|-
|8
|{{flagicon|UAE}}
|Abdul Hameed Al Mistaki
|45
|}

==References==
{{Reflist|colwidth=30em}}

==External links==
{{Commons category|Al Ain FC}}
* [http://www.alainfc.ae/ Official website]
* [http://web.agleague.ae/en/club/al-ain.html Al Ain FC] on [[UAE Arabian Gulf League|Arabian Gulf League]] official website

{{S-start}}
{{s-ach|ach}}
{{succession box|title=[[Asian Champions League#Asian Champions Cup & Champions League Finals|Champions of Asia]]|before=[[Suwon Samsung Bluewings]]<br />{{flagicon|South Korea}}|after=[[Al-Ittihad (Jeddah)|Al-Ittihad]]<br />{{flagicon|Saudi Arabia}}|years=[[AFC Champions League 2002–03|2002–03]]}}
{{S-end}}

{{Al Ain S.C.C. squad}}
{{Al Ain FC}}
{{Al Ain FC seasons}}
{{Arabian Gulf League teamlist}}
{{AFC Champions League Winners}}
{{AFC Club of the Year}}

{{Use dmy dates|date=April 2013}}

[[Category:Al Ain FC| ]]
[[Category:Association football clubs established in 1968]]
[[Category:1968 establishments in the Trucial States]]
[[Category:Football clubs in Abu Dhabi (emirate)|Al Ain]]
[[Category:Football clubs in the United Arab Emirates|Ain]]
[[Category:Multi-sport clubs in the United Arab Emirates]]