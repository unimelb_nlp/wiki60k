{{Infobox road
|state=NY
|type=NY
|route=311
|map_notes=Map of southeastern New York and western Connecticut with NY&nbsp;311 highlighted in red
|map_alt=NY 311 follows a southwest–northeast alignment from NY 52 in Lake Carmel to NY 22 in Patterson. It intersects Interstate 84 northeast of Lake Carmel.
|length_mi=6.14
|length_ref=<ref name="2008tdr" />
|established=1930<ref name=gb />
|direction_a=West
|terminus_a={{jct|state=NY|NY|52}} in [[Kent, New York|Kent]]
|junction={{jct|state=NY|I|84}} in [[Patterson, New York|Patterson]]
|direction_b=East
|terminus_b={{jct|state=NY|NY|22}} in Patterson
|counties=[[Putnam County, New York|Putnam]]
|previous_type=NY
|previous_route=310
|next_type=NY
|next_route=312
}}
'''New York State Route&nbsp;311''' ('''NY&nbsp;311''') is a [[state highway]] located entirely within [[Putnam County, New York]], in the United States. The highway begins at [[New York State Route 52|NY&nbsp;52]] in Lake Carmel, and intersects [[Interstate 84 in New York|Interstate&nbsp;84]] (I-84) shortly thereafter. It crosses [[New York State Route 164|NY&nbsp;164]] and [[New York State Route 292|NY&nbsp;292]] as it heads into the northeastern part of the county, finally curving east to reach its northern terminus at [[New York State Route 22|NY&nbsp;22]] just south of the [[Dutchess County, New York|Dutchess County]] line. The route passes several historical sites.

Part of modern-day Route&nbsp;311 was originally the Philipstown Turnpike, a road built in 1815 to overcome a lack of transportation when the [[Hudson River]] froze during the winter months. The [[Toll road|turnpike]] was a large business center for the county, though it was abandoned due to insufficient tolls to maintain it. Another section was constructed in the early 1900s, from the Patterson [[Baptists|Baptist Church]] near the modern-day intersection of Route&nbsp;311 and [[New York State Route 164|Route&nbsp;164]] to the Village of Patterson, by a group of Italian immigrants.

In the [[1930 state highway renumbering (New York)|1930 renumbering of state highways in New York]], the segment of former [[New York State Route 39 (1920s)|NY&nbsp;39]] east of West Patterson was renumbered to NY&nbsp;311. NY&nbsp;52 was realigned {{circa|1937}} to follow its current alignment between [[Stormville, New York|Stormville]] and Lake Carmel. The former routing of NY&nbsp;52 from West Patterson to Lake Carmel became part of an extended NY&nbsp;311.

==Route description==
[[File:NY 311 crossing Lake Carmel.jpg|left|thumb|NY&nbsp;311 crossing [[Lake Carmel (New York)|Lake Carmel]]|alt=A road crossing a lake via a causeway. A sign is visible on the right-hand side of the road depicting the 311 designation. A car is visible in the distance.]]
NY&nbsp;311 begins at [[New York State Route 52|NY&nbsp;52]] in the [[Kent, New York|Kent]] hamlet of [[Lake Carmel, New York|Lake Carmel]]. It heads northeast, crossing over the northernmost portion of [[Lake Carmel (New York)|Lake Carmel]] on a short [[causeway]]. The lake has a surface area of about {{convert|200|acre|ha}}, and it sits at {{convert|618|ft}} in elevation.<ref name="NYT story">{{cite news|last=Brenner|first=Elsa|title=Good Prices and the Great Outdoors|url=https://www.nytimes.com/2005/11/27/realestate/27living.html |newspaper=[[The New York Times]]|date=November 27, 2005|accessdate=August 1, 2008|quote=About a third of the town's housing stock&nbsp;&ndash; more than 5,400 living units, according to 2000 census data&nbsp;&ndash; is in eastern Kent on the {{convert|200|acre|ha|sing=on}} Lake Carmel.}}</ref> It was created by developers in the early 20th century by damming the Middle Branch of the [[Croton River]],<ref name="Smadbeck at Patterson history">{{cite web |title=The Putnam Lake Community of Patterson |url=http://www.historicpatterson.org/Exhibits/ExhPutLake.php |work=Historic Patterson |publisher=[[Patterson, New York|Town of Patterson, New York]] |accessdate=August 2, 2008}}</ref> and is one of the few large bodies of water in [[Putnam County, New York|Putnam County]] not used as a [[reservoir (water)|reservoir]] by [[New York City]]. The road then crosses into the town of [[Patterson, New York|Patterson]] and meets [[Interstate 84 in New York|I-84]] by way of an interchange. Construction is planned to start in summer 2012 for general bridge rehabilitation of the I-84 interchange.<ref>{{cite news|author=|title=I-84/MNRR Harmel Line & I-84/RT. 311 Bridges|year=2008|publisher=New York State Department of Transportation <!--|accessdate=August 24, 2008-->}}</ref> Proceeding eastward, it follows an erratic path, turning in different directions due to the hilly terrain of the area. It passes by a Christian youth camp and intersects with [[New York State Route 164|NY&nbsp;164]].<ref name="google"/>

[[File:New York State Route 311 2.jpg|thumb|right|The junction with CR&nbsp;64]]
NY&nbsp;311 curves gradually northeast before turning almost due north to an intersection with [[New York State Route 292|NY&nbsp;292]] in the community of West Patterson. Past NY&nbsp;292, NY&nbsp;311 parallels the [[Putnam County, New York|Putnam]]–[[Dutchess County, New York|Dutchess]] county line east into the hamlet of Patterson, serving as the main street of the community. After crossing the [[Harlem Line]] tracks, NY&nbsp;311 crosses the [[Croton River|East Branch Croton River]] at the north end of the wetland area known as the [[Great Swamp (New York)|Great Swamp]]. At nearly {{convert|6000|acre|ha}}, the Great Swamp is the second largest [[wetland]] in New York, extending as far north as [[Dover, New York|Dover]] in Dutchess County.<ref>{{cite web|title=Eastern: The Great Swamp|year=2008|publisher=The Nature Conservancy|accessdate=August 25, 2008|url=http://www.nature.org/wherewework/northamerica/states/newyork/preserves/art13513.html}}</ref> Continuing eastward, NY&nbsp;311 passes the Patterson Fire Department, and terminates at [[New York State Route 22|NY&nbsp;22]] at a junction known as Akins Corners.<ref name="google">{{Google maps|url=https://maps.google.com/maps?hl=en&q=from:+RT-311%20@41.466800,%20-73.665740+to:+RT-311%20@41.514801,%20-73.591308|accessdate=March 7, 2008}}</ref> In the [[Hamlet (New York)|hamlet]] of Patterson, an [[historic district]] exists along the route. A number of historic sites are located on NY&nbsp;311, including the [[:File:Patterson Church 800.jpg|Patterson Presbyterian Church]], the Fellowship Hall, Christ Episcopal Church, the Maple Avenue Cemetery and the Grange Hall.<ref>{{cite web |work=Historic Patterson |title=The Patterson Historic District 1 |publisher=Town of Patterson, New York |accessdate=August 31, 2008 |url=http://www.historicpatterson.org/Exhibits/ExhHistoricDistrict1.php}}</ref>

==History==

===Pre-designation history===
Part of modern-day NY&nbsp;311 from the NY&nbsp;292 intersection to the route's ending terminus was once part of the Philipstown Turnpike. Initially, the county's proximity to the [[Hudson River]] supplied cheap means of transporting goods to [[Albany, New York|Albany]] and [[New York City]], though in the winter months, the river froze over.<ref name="Putnam History">{{cite web|title=A Brief History of Putnam County|year=2008|publisher=Putnam County Government|accessdate=August 24, 2008|url=http://www.putnamcountyny.com/historian/ourhistory.html}}</ref>
To resolve the issue, in 1815, the Philipstown Turnpike Company was organized to improve upon a toll road from [[Cold Spring, New York|Cold Spring]] to the [[Connecticut]] border.<ref>{{cite book|first=William Smith |last=Pelletreau|title=History of Putnam County, New York|year=1886|publisher=University of Michigan|page= 519<!--|accessdate=August 24, 2008-->}}</ref> On April&nbsp;15, 1815, "an act to incorporate the Philipstown turnpike company in the county of Putnam" was passed.<ref>{{cite book|page= 449|author=New York (State) Legislature|year=1834|publisher=John Shea|location=Oxford University|title=Journal of the Assembly of the State of New York|accessdate=August 24, 2008|url=https://books.google.com/books?id=xLQFAAAAQAAJ&pg=PA449&dq=Philipstown+Turnpike}}</ref> East of the Connecticut border, the turnpike continued as the [[New Milford and Sherman Turnpike]].<ref name="ct">{{cite book|page= 1335|author=Connecticut, General Assembly|title=Special Acts and Resolutions of the State of Connecticut ...|year=1857|publisher=Case, Lockwood & Brainard|accessdate=August 24, 2008|url=https://books.google.com/books?id=ggc4AAAAIAAJ&pg=PA1334&dq=Philipstown+Turnpike#PPA1335,M1}}</ref> On the turnpike, wagons transported manufactures inland, and carried produce from the eastern part of the county.<ref name="Putnam History"/> Before the advent of the [[railroad]], the road was a business center for much of the county.<ref>{{cite web|author=Goist & Gilbert|title=History of Putnam County Chapter XXVII Town of Kent|year=2004|publisher=NYGenWeb|accessdate=August 24, 2008|url=http://www.rootsweb.ancestry.com/~nyputnam/history/index.htm}}</ref> One of the intentions of the turnpike was to "greatly promote the public good, as well contribute to their individual interest". However, the turnpike was eventually abandoned, because the tolls received were not sufficient to defray the expense of maintaining the road and associated [[bridge]]s.<ref name="ct"/> The tolls were also inadequate for investors in the Philipstown Turnpike Company to make a profit.<ref name="Putnam History"/>
[[File:Downtown Patterson, NY.jpg|left|thumb|Looking east on NY&nbsp;311 in [[Patterson, New York|Patterson]]|alt=A road heading through a town. A mountain is visible in the distance. On the left side of the road is a series of houses, while numerous trees line the right side. Many wooden utility poles are present.]]

In November 1901, the Putnam County's [[Board of Supervisors]] hired an engineer to create plans for a new road that would run from the [[Westchester County, New York|Westchester]]–Putnam County border into [[Dutchess County, New York|Dutchess County]]. By early the next year, a group of engineers led by H. W. Degaff surveyed the region, with the goal of constructing a {{convert|16|ft|m|sing=on}} wide road. In October 1902, the Board of Supervisors was informed that the engineers planned to build the new road along a path similar to an existing road. Surveys were completed in 1907, resulting in the elimination of a dangerous [[Level crossing|railroad crossing]] via a [[trestle]]. Actual construction began on the Patterson portion of the state road in April 1909, beginning from the Patterson Baptist Church near the modern-day intersection of NY&nbsp;311 and NY&nbsp;164. The working crew was composed of [[Italians|Italian]] immigrants, some of whom were given temporary residence within the Putnam Cigar Factory. By June, the construction had reached the Village of Patterson, and was thus completed.<ref name="Early construction">{{cite web |work=Historic Patterson |title=From Dirt Roads to the Interstate Highway |publisher=Town of Patterson, New York |accessdate=August 24, 2008 |url=http://www.historicpatterson.org/Exhibits/ExhHighways.php}}</ref>

===Designation===
The portion of modern NY&nbsp;311 from [[New York State Route 292|NY&nbsp;292]] in West Patterson and [[New York State Route 22|NY&nbsp;22]] in Patterson was designated in the mid-1920s as part of [[New York State Route 39 (1920s)|NY&nbsp;39]], an east–west route extending from [[Poughkeepsie (city), New York|Poughkeepsie]] to Patterson via [[East Fishkill, New York|East Fishkill]] and West Pawling.<ref>{{cite news |title=New York's Main Highways Designated by Numbers |newspaper=The New York Times |date=December 21, 1924 |page=XX9}}</ref><ref>{{cite map|url=http://www.broermapsonline.org/members/NorthAmerica/UnitedStates/Midatlantic/NewYork/unitedstates1926ra_009.html|title=Rand McNally Auto Road Atlas (eastern New York)|publisher=[[Rand McNally]]|year=1926|accessdate=February 2, 2008}}</ref> In the [[1930 state highway renumbering (New York)|1930 renumbering of state highways in New York]], the segment of NY&nbsp;39 east of West Patterson was renumbered to NY&nbsp;311 while the portion of 1920s NY&nbsp;39 from East Fishkill to West Patterson became part of [[New York State Route 52|NY&nbsp;52]]. NY&nbsp;52 also continued south from West Patterson through what is now [[Lake Carmel, New York|Lake Carmel]] to [[Carmel, New York|Carmel]], where it continued east on an [[overlap (road)|overlap]] with [[U.S. Route 6 in New York|U.S. Route&nbsp;6]] (US&nbsp;6).<ref name=gb>''Automobile Legal Association (ALA) Automobile Green Book'', 1930–31 and 1931–32 editions, (Scarborough Motor Guide Co., Boston, 1930 and 1931). The 1930–31 edition shows New York state routes prior to the [[1930 renumbering (New York)|1930 renumbering]]</ref><ref>{{cite news|first=Leon A. |last=Dickinson|title=New Signs for State Highways|newspaper=The New York Times|date=January 12, 1930|page=136}}</ref>

In March 1936, a "great and unusual ice flood" caused local water levels to rise.<ref name="ct"/> The bridge carrying the highway over the Great Swamp received cracks in its foundation due to the pressure of the water and melting ice, and was lifted off its foundation and swept into the swamp.<ref name="Early construction"/> NY&nbsp;52 was realigned {{circa|lk=no|1937}} to follow its current alignment between [[Stormville, New York|Stormville]] and Lake Carmel. The former routing of NY&nbsp;52 from West Patterson to Lake Carmel became part of an extended NY&nbsp;311.<ref>{{cite map |title=New York |publisher=[[Standard Oil Company]] |year=1936 |cartography=[[General Drafting]]}}</ref><ref>{{cite map |title=New York |publisher=Standard Oil Company |year=1937 |cartography=General Drafting}}</ref>

===Recent history===
[[File:Crossing 800.jpg|right|thumb|The NY&nbsp;311 Harlem Line railroad crossing|alt=A railroad crossing in a mainly rural area. A mountain is visible in the distance.]]
In June 1960, the [[Presbyterian]] Men's Club produced a film called ''Our Town'', part of which was filmed on the Main Street portion of NY&nbsp;311 at Boot Hill, a replica western [[frontier]] town. Children were asked to participate in the filming by dressing in western or American Indian apparel; girls dressed as frontier women. The film debuted at the Patterson [[Town Hall]] on September&nbsp;24, 1960.<ref name="1960s">{{cite web |work=Historic Patterson |title=Patterson in the 1960s |publisher=Town of Patterson, New York |accessdate=August 25, 2008 |url=http://www.historicpatterson.org/Exhibits/Exh1960s.php}}</ref>

By 1966, Putnam County's men began drafting into the armed forces to fight in the [[Vietnam War]]. In November of that year, Local Board No. 14 announced that 21 local men would be drafted into the military, the highest total to be called since the [[Korean War]]. Of the 21, two were residents of Patterson.<ref name="1960s"/> To honor the Patterson veterans who lost their lives, a plaque commemorating the Vietnam War was added to the War Memorial by [[American Legion|the American Legion]] at the intersection of NY&nbsp;311 and Maple Avenue.<ref>{{cite web |work=Historic Patterson |title=The Patterson Historic District 2 |publisher=Town of Patterson, New York |accessdate=August 25, 2008 |url=http://www.historicpatterson.org/Exhibits/ExhHistoricDistrict2.php}}</ref>

The Patterson [[Post Office]] underwent several re-locations throughout the years. After the Lloyd Lumber Company moved to a larger building along NY&nbsp;311, the post office occupied the smaller structure; both were situated on the east side of the New York Central tracks. Residents complained that the new location was inadequate, claiming that [[parking]] was insufficient at nearby companies, and that walking along a busy road to reach the post office was dangerous. After a bidding process to determine the future location, the office was again moved to a former car [[Filling station|service station]] along Front Street.<ref>{{cite web |work=Historic Patterson |title=The Patterson Post Office |publisher=Town of Patterson, New York |accessdate=August 25, 2008 |url=http://www.historicpatterson.org/Exhibits/ExhPostOffice.php}}</ref>

==Major intersections==
{{NYinttop|length_ref=<ref name="2008tdr">{{cite web |url=https://www.dot.ny.gov/divisions/engineering/technical-services/hds-respository/Traffic%20Data%20Report%202008.pdf |title=2008 Traffic Data Report for New York State |date=June 16, 2009 |format=PDF |pages=291–292 |publisher=[[New York State Department of Transportation]] |accessdate=October 13, 2009}}</ref>|county=Putnam}}
{{NYint
|mile=0.00
|road={{jct|state=NY|NY|52|name1=Snadbeck Avenue}}
|location=Kent
|lspan=2
|notes=[[Hamlet (New York)|Hamlet]] of [[Lake Carmel, New York|Lake Carmel]]
}}
{{NYint
|mile=0.39
|road={{jct|state=NY|CR|46|county1=Putnam}}
}}
{{NYint
|mile=1.09
|road={{jct|state=NY|I|84|name1=Terry Hill Road}}
|location=Patterson
|lspan=4
|notes=Exit&nbsp;18 (I-84)
}}
{{NYint
|mile=2.57
|road={{jct|state=NY|NY|164}}
}}
{{NYint
|mile=4.78
|road={{jct|state=NY|NY|292}}
}}
{{NYint
|mile=6.14
|road={{jct|state=NY|NY|22}}
}}
{{NYintbtm}}

==See also==
*{{Portal-inline|New York Roads}}

==References==
{{Reflist|30em}}

==External links==
{{Commons category|New York State Route 311}}
{{Attached KML}}
{{NYSR external links|type=N|nyroutes=yes|termini=yes|route=311|alps=yes}}

{{Featured article}}

[[Category:State highways in New York|311]]
[[Category:Transportation in Putnam County, New York]]