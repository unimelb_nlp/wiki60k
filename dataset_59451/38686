{{for|the cancelled battleship of the same name|Charles Martel-class ironclad}}
{{good article}}
{| {{Infobox ship begin |infobox caption=''Brennus''}}
{{Infobox ship image
| Ship image   = [[File:Brennus-Marius Bar-img 3134.jpg|300px]]
| Ship caption = 
}}
{{Infobox ship career
| Hide header = 
| Ship country = France
| Ship flag = {{shipboxflag|France|naval}}
| Ship name = ''Brennus''
| Ship namesake = [[Brennus]]
| Ship ordered = 1888
| Ship builder = [[Lorient]]
| Ship laid down = 12 January 1889
| Ship launched = 17 October 1891
| Ship commissioned = 16 December 1896
| Ship struck = 1919
| Ship fate = Broken up in 1922
}}
{{Infobox ship characteristics
| Hide header = 
| Header caption = 
| Ship type = [[Pre-dreadnought battleship]]
| Ship displacement = {{convert|11190|MT|abbr=on}}
| Ship length = {{convert|110.29|m|abbr=on}}
| Ship beam = {{convert|20.4|m|abbr=on}}
| Ship draft = {{convert|8.28|m|abbr=on}}
| Ship propulsion =*  2 [[triple-expansion engine]]s
* 32 [[Belleville boiler]]s
* {{convert|13,900|ihp|abbr=on}}
  
| Ship speed = {{convert|18|kn|lk=in}}
| Ship range = 
| Ship complement = 673
| Ship armament =* 3 × [[Canon de 340 mm/42 Modèle 1887 gun|340 mm/42 Modèle 1887 gun]]s
* 10 × 1 – [[Canon de 164 mm Modèle 1893|164&nbsp;mm Mle 1893]] guns
* 4 × 65 mm
* 14 × [[QF 3 pounder Hotchkiss|{{convert|47|mm|in|abbr=on}} Mle 1885 Hotchkiss guns]]
* 8 × 37 mm
* 4 × {{convert|460|mm|in|abbr=on}} torpedo tubes
  
| Ship armor =* [[Belt armor|Belt]]: {{convert|460|mm|abbr=on}}
* Deck: {{convert|60|mm|abbr=on}}
* [[Conning tower]]: {{convert|150|mm|abbr=on}}
* [[Gun turret|Turrets]]: 460&nbsp;mm
  
| Ship notes = 
}}
|}

'''''Brennus''''' was the first [[pre-dreadnought battleship]] of the [[French Navy]] built in the late 19th century. She was laid down in January 1889, launched in October 1891, and completed in 1896. Her design was unique and departed from earlier ironclad battleship designs by introducing a number of innovations. These included a main battery of heavy guns mounted on the [[centerline (ship marking)|centerline]] and the first use of [[Belleville boiler]]s. She formed the basis for several subsequent designs, beginning with [[French battleship Charles Martel|''Charles Martel'']].

''Brennus'' spent the majority of her career in the Mediterranean Squadron, and she served as its [[flagship]] early in her career. In 1900, she accidentally rammed and sank the destroyer [[French destroyer Framée|''Framée'']]. As newer battleships were commissioned into the fleet, ''Brennus'' was relegated to the Reserve Squadron in the early 1900s. By the outbreak of [[World War I]] in August 1914, her old age and poor condition prevented her from seeing action. She was ultimately stricken from the [[naval register]] in 1919 and sold for scrap three years later.

== Design ==
An earlier vessel, also named [[Charles Martel-class ironclad|''Brennus'']], was laid down in 1884 and cancelled under the tenure of Admiral [[Théophile Aube]]. The vessel, along with a sister ship named ''Charles Martel'', was a modified version of the {{sclass-|Marceau|ironclad|0}} ironclad battleships. After Aube's retirement, the plans for the ships were reworked entirely for the ships actually completed, though they are sometimes conflated with the earlier, cancelled vessels.<ref>Ropp, p. 222</ref> This confusion may be a result of the same shipyard working on both of the ships named ''Brennus'', along with use of material assembled for the first vessel to build the second.<ref>Brassey (1889), p. 65</ref> The two pairs of ships were, nevertheless, distinct vessels.<ref name="G283">Gardiner, p. 283</ref> The second ''Brennus'' was ordered in 1888.<ref name="R230" />

''Brennus'' was the first [[pre-dreadnought]] style battleship built in the French Navy; the previous ''Magenta''-class ships were barbette ships, a type of [[ironclad warship|ironclad]] battleship.<ref name="G283" /> ''Brennus'' formed the basis for the subsequent group of five broadly similar battleships built to the same design specifications, begun with [[French battleship Charles Martel|''Charles Martel'']], though they reverted to the armament layout of the earlier ''Magenta''s which saw the main guns distributed in single turrets in a lozenge pattern.<ref>Ropp, p. 223</ref>

=== General characteristics and machinery ===
[[File:Brennus line-drawing.png|thumb|left|Line-drawing of ''Brennus'']]

''Brennus'' was {{convert|110.29|m|ftin|sp=us}} [[length between perpendiculars|long between perpendiculars]], and had a [[beam (nautical)|beam]] of {{convert|20.4|m|ftin|abbr=on}} and a [[Draft (hull)|draft]] of {{convert|8.28|m|ftin|abbr=on}}. She had a displacement of {{convert|11190|t|LT|0}}. As built, the ship was significantly overweight, and her draft was {{convert|.38|m|abbr=on}} greater than intended, without a full load of ammunition. Most of her armored belt was submerged. Her [[superstructure]] had to be cut down and her mainmast, intended to be a fighting mast, had to be replaced with a lighter pole mast. Unlike most battleships of the period, she was built without a [[naval ram|ram bow]]. ''Brennus'' had a crew of 673 officers and enlisted men.<ref name="G292">Gardiner, p. 292</ref>

''Brennus'' had two vertical [[triple expansion engine]]s each driving a single screw, with steam supplied by thirty-two [[Belleville boiler|Belleville]] [[water-tube boiler]]s.<ref name="G292" /> The decision to fit ''Brennus'' with water-tube boilers was made in 1887, and she was the first large ship to be equipped with them.<ref name="R230">Ropp, p. 230</ref> Her propulsion system was rated at {{convert|13900|ihp|lk=in}}, which allowed the ship to steam at a speed of {{convert|17.5|to|18|kn|lk=in}}. As built, she could carry {{convert|600|MT|abbr=on}} of coal, though additional space allowed for up to {{convert|980|MT|abbr=on}} in total.<ref name="G292" />

=== Armament and armor ===
[[File:French battleship Brennus.png|thumb|''Brennus'', c. 1896]]
''Brennus''{{'}}s main armament consisted of three [[Canon de 340 mm/42 Modèle 1887 gun]]s, two in a twin [[gun turret|turret]] forward, and the third in a single turret aft. Her secondary armament consisted of ten [[Canon de 164 mm Modèle 1893]] guns, four of which were mounted in single turrets amidships; the other six were located directly underneath them in [[casemate]]s. The ship also carried four 9-pounder quick-firing guns, fourteen 3-pounders, and eight 1-pounder guns, and six 1-pounder revolver cannons. Her armament suite was rounded out by four above-water {{convert|450|mm|abbr=on}} [[torpedo tube]]s, all of which were later removed.<ref name="G292" />

The ship's armor was constructed with both steel and [[compound armor]]. The main belt was {{convert|460|mm|abbr=on}} thick amidships, and tapered down to {{convert|305|mm|abbr=on}} at the lower edge. On either end of the central citadel, the belt was reduced to 305&nbsp;mm at the waterline and {{convert|250|mm|abbr=on}} on the lower edge; the belt extended for the entire length of the hull. Above the belt was {{convert|100|mm|abbr=on}} thick side armor. The main battery guns were protected with a maximum thickness of 460&nbsp;mm of armor, and the secondary turrets had 100&nbsp;mm thick sides. The main armored deck was {{convert|60|mm|abbr=on}} thick. The [[conning tower]] had {{convert|150|mm|abbr=on}} thick sides.<ref name="G292" />

== Service career ==
[[File:French battleship Brennus before reconstruction.png|thumb|''Brennus'' before her reconstruction
]]

''Brennus'' was laid down at the [[Lorient]] dockyard in January 1889 and launched on 17 October 1891. [[Fitting-out]] work was completed in 1896 and she was commissioned into the French Navy.<ref name="G292" /><ref name="GG191">Gardiner & Gray, p. 191</ref> In 1897, the French Navy issued a new doctrine for gunnery control. During gunnery training exercises to test the new system, ''Brennus'' and the ironclad battleships [[French battleship Neptune (1887)|''Neptune'']] and [[French battleship Marceau (1887)|''Marceau'']] got 26% hits at a range of {{convert|3000|to|4000|m|yd|abbr=on}}. Their success prompted the Navy to make the method the standard for the fleet in February 1898.<ref>Ropp, p. 301</ref>

In July and August 1900, the French fleet conducted maneuvers in the [[English Channel]]. At the time, ''Brennus'' was the [[flagship]] of the Mediterranean Squadron, under Vice Admiral Fournier. On 10 August off [[Cape St. Vincent]], while returning from the maneuvers, she collided with the destroyer [[French destroyer Framée|''Framée'']].<ref>Brassey (1901), p. 41</ref> The destroyer quickly sank, and only fourteen men from her crew of 50 were rescued.<ref>Johnson, pp. 682–683</ref> By 1903, ''Brennus'' was transferred to the Reserve Squadron, along with three other battleships and three [[armored cruiser]]s.<ref>Brassey (1903), p. 57</ref> There, she flew the flag of Rear Admiral Besson during the annual summer maneuvers in July–August 1903.<ref>Brassey (1903), p. 140</ref>

''Brennus'' continued on in the Reserve Squadron through 1907, during which time she was again the flagship of Vice Admiral Fournier.<ref>Brassey (1907), p. 103</ref> Fournier was the commander in chief of the annual summer maneuvers, which began in late June and concluded on 4 August 1907.<ref>Brassey (1907), pp. 103–106</ref> The following year, the Mediterranean Fleet was reorganized into three squadrons; ''Brennus'' again repeated her role as flagship, of the Third Squadron, under the command of Rear Admiral Germinet.<ref>Brassey (1908), p. 68</ref> Due to her age and condition by the outbreak of [[World War I]] in August 1914, ''Brennus'' was not mobilized and did not see action. ''Brennus'' was ultimately stricken from the [[naval register]] in 1919 and sold for scrapping in 1922.<ref name="GG191" />

== Footnotes ==
{{Commons category|French battleship Brennus (1891)}}

{{reflist|20em}}

== References ==
* {{cite journal | editor-last = Brassey | editor-first = Thomas A. | authorlink = Thomas Brassey, 1st Earl Brassey | year = 1889 | journal = [[Brassey's Naval Annual]] | volume = | publisher = J. Griffin & Co. | location = Portsmouth, UK }}
* {{cite journal | editor-last = Brassey | editor-first = Thomas A. | year = 1901 | journal = Brassey's Naval Annual | volume = | publisher = J. Griffin & Co. | location = Portsmouth, UK }}
* {{cite journal | editor-last = Brassey | editor-first = Thomas A. | year = 1903 | journal = Brassey's Naval Annual | volume = | publisher = J. Griffin & Co. | location = Portsmouth, UK }}
* {{cite journal | editor-last = Brassey | editor-first = Thomas A. | year = 1907 | journal = Brassey's Naval Annual | volume = | publisher = J. Griffin & Co. | location = Portsmouth, UK }}
* {{cite journal | editor-last = Brassey | editor-first = Thomas A. | year = 1908 | journal = Brassey's Naval Annual | volume = | publisher = J. Griffin & Co. | location = Portsmouth, UK }}
* {{cite book | editor-last = Gardiner | editor-first = Robert | title = Conway's All the World's Fighting Ships 1860–1905 | publisher = Conway Maritime Press | location = Greenwich, UK | year = 1979 | isbn = 978-0-8317-0302-8 }}
* {{cite book |editor1-last=Gardiner|editor1-first=Robert|editor2-last=Gray|editor2-first=Randal|title=Conway's All the World's Fighting Ships, 1906–1921|year=1984|location=Annapolis, MD|publisher=Naval Institute Press|isbn=978-0-87021-907-8 }}
* {{cite journal | editor-last = Johnson| editor-first = Alfred S. | year = 1900 | journal = The Cyclopedic Review of Current History | volume = 10 | publisher = Current History Company | location = Boston, MA }}
* {{cite book | last = Ropp | first = Theodore | editor-last = Roberts | editor-first = Stephen S. | title = The Development of a Modern Navy: French Naval Policy, 1871–1904 | year = 1987 | location = Annapolis | publisher = Naval Institute Press | isbn = 978-0-87021-141-6 }}

{{French battleship Brennus}}
{{WWIFrenchShips}}

{{DEFAULTSORT:Brennus (1891)}}
[[Category:Victorian-era battleships of France]]
[[Category:1891 ships]]
[[Category:Battleships of the French Navy]]
[[Category:Ships built in France]]