{{mergefrom|SAAC-23|date=March 2017}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name         = Learjet 23
 |image        = Learjet 23 der NASA.jpg
 |caption      = [[NASA]] Learjet 23
}}{{Infobox Aircraft Type
 |type         = [[Business jet]]
 |manufacturer = [[Learjet]]
 |designer     = [[William Powell Lear]], based on a design by Dr.eng. Hans-Luzius Studer
 |first flight = 7 October 1963
 |introduced   = October 13, 1964
 |retired      = 
 |status       = Active
 |primary user = Private
 |more users   = [[Military]]  
 |produced     = 1962-1966
 |number built = 104
 |unit cost    = 
}}
|}

The '''Learjet 23''' is an [[United States|American]] built six-to-eight-seat (two crew and four to six passengers) twin-engine, high-speed [[business jet]] manufactured by [[Learjet]].  Introduced in 1964, it was Learjet's first model and created a completely new market for fast and efficient business aircraft.  104 units were manufactured before production ceased just two years later in 1966.

==History==
[[Bill Lear|William (Bill) Powell Lear, Sr.]] recognized the [[FFA P-16]]'s potential and established Swiss American Aviation Corporation (SAAC) to produce a passenger version as the '''SAAC-23 Execujet'''. The company moved to [[Wichita, Kansas]] and was renamed as the Lear Jet Corporation, production was started on the first Model 23 Learjet on February 7, 1962. The first flight of the Learjet 23 took place on 7 October 1963 with test pilots Hank Beaird and Bob Hagen.<ref>{{cite journal|magazine=Flying Magazine|date=December 1993|page=38}}</ref> Although the prototype crashed in June 1964 the type Learjet 23 was awarded a type certificate by the Federal Aviation Administration on 31 July 1964. On October 13, 1964, the first production aircraft was delivered.

With this jet a completely new market for fast and efficient business aircraft was opened. The Model 23 was the basis for a whole set of similar aircraft which remain in production.

Production of the Learjet 23 stopped in 1966 after a total of 104 had been built. In 1998 there were still 39 Model 23s in use. A total of 27 have been lost or damaged beyond repair through accidents during the aircraft's lengthy career, the most recent in 2008.<ref>[http://aviation-safety.net/database/type/type.php?type=LJ-23 Aviation Safety Network: Learjet 23]</ref>

==Noise compliance==
In 2013, the FAA modified 14 CFR part 91 rules to prohibit the operation of jets weighing 75,000 pounds or less that are not stage 3 noise compliant after December 31, 2015.  The Learjet 23 is listed explicitly in Federal Register [http://www.gpo.gov/fdsys/granule/FR-2013-07-02/2013-15843/content-detail.html 78 FR 39576].  Any Learjet 23s that have not been modified by installing Stage 3 noise compliant engines or have not had "hushkits" installed for non-compliant engines will not be permitted to fly in the contiguous 48 states after December 31, 2015, and the rule points out that appropriate hushkits are not currently available for the Learjet 23. ''14 CFR §91.883 Special flight authorizations for jet airplanes weighing 75,000 pounds or less'' - lists special flight authorizations that may be granted for operation after December 31, 2015.

==Aircraft on display==
* 23-002 – Model 23 on static display at the [[Udvar-Hazy Center]] of the [[National Air and Space Museum]] in [[Chantilly, Virginia]].<ref>{{cite web|title=Lear Jet 23|url=https://airandspace.si.edu/collection-objects/lear-jet-23|website=Smithsonian National Air and Space Museum|publisher=Smithsonian Institution|accessdate=8 November 2016}}</ref><ref name="rzjets">{{cite web|title=Learjet 23/24 production list|url=http://rzjets.net/aircraft/?typeid=264|website=rzjets|publisher=rzjets.net|accessdate=8 November 2016}}</ref>
* 23-006 – Model 23 on static display at the [[Kansas Aviation Museum]] in [[Wichita, Kansas]].<ref>{{cite web|title=Learjet Model 23|url=http://kansasaviationmuseum.org/visit/aircraft/learjet-model-23|website=Kansas Aviation Museum|accessdate=8 November 2016}}</ref><ref name="rzjets" />
* 23-009 – Model 23 on display at the Arkansas Air & Military Museum in [[Fayetteville, Arkansas]]. This airframe was flown by Bobby Younkin in air shows.<ref>{{cite news|last1=Mathews|first1=Kay|title=Since 1986 aviation history flies high at the Arkansas Air Museum|url=http://www.digitaljournal.com/article/303298|accessdate=8 November 2016|work=Digital Journal|publisher=digitaljournal.com|date=4 February 2011}}</ref><ref>{{cite web|title=Aircraft N23BY Data|url=http://www.airport-data.com/aircraft/N23BY.html|website=Airport-Data.com|publisher=Airport-Data.com|accessdate=8 November 2016}}</ref>
* 23-015 – Model 23 on static display at the [[Pima Air & Space Museum]] in [[Tucson, Arizona]].<ref>{{cite web|title=LEARJET MODEL 23|url=http://www.pimaair.org/aircraft-by-name/item/learjet-model-23|website=Pima Air & Space Museum|publisher=Pimaair.org|accessdate=8 November 2016}}</ref>
* 23-034 – Model 23 on static display at the [[Museum of Flight]] in [[Seattle, Washington]].<ref>{{cite web|title=Learjet 23|url=http://www.museumofflight.org/aircraft/learjet-23|website=The Museum of Flight|publisher=The Museum Of Flight|accessdate=8 November 2016}}</ref>
* 23-068 – Model 23 in storage at the [[Yanks Air Museum]] in [[Chino, California]].<ref>{{cite web|title=Airframe Dossier - Swiss American Aviation CorporationLearjet, c/n 23-068, c/r N73CE|url=http://aerialvisuals.ca/AirframeDossier.php?Serial=93300|website=Aerial Visuals|publisher=AerialVisuals.ca|accessdate=8 November 2016}}</ref>

==Operators==
;{{USA}}
*[[NASA]]
*[[Netjets|Executive Jet Aviation]]

==Specifications==
{{Aircraft specs
|ref=Jane's All The World's Aircraft 1965–66<ref name="Janes 65 p252-3">Talyor 1965, pp. 252–253.</ref>
|prime units?=imp
<!--
        General characteristics
-->
|crew=Two pilots
|capacity=6 passengers
|length m=13.18
|span m=10.84
|height m=3.84
|wing area sqm=21.48
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight lb=6,150
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight lb=12,499
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=2
|eng1 name=[[General Electric CJ610-4]]
|eng1 type=turbojet engines
|eng1 lbf=2,850

<!--
        Performance
-->
|max speed mph=561
|max speed note=at {{convert|24000|ft|abbr=on}}
|max speed mach=0.82
|cruise speed mph=518
|cruise speed note=at {{convert|40000|ft|abbr=on}}
|stall speed mph=104 
|stall speed note= wheels and flaps down
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range miles=1830
|range note=max fuel at  {{convert|485|mph|km/h kn|abbr=on}} and {{convert|40000|ft|abbr=on}}
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling ft=45000
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|climb rate ftmin=6900
|time to altitude=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|thrust/weight=

|more performance=
|avionics=
}}

==See also==
{{aircontent|
|related=
|similar aircraft=

|lists=
*[[List of aircraft]]
}}

==References==
<references />
* Ogden, Bob, "Aviation Museums and Collections of North America", 2007, Air-Britain (Historians) Ltd, ISBN 0-85130-385-4.
* [[John W. R. Taylor|Taylor, John W. R.]] ''Jane's All The World's Aircraft 1965–1966''. London:Sampson Low, Marston & Company, 1965.
*[http://www.airwar.ru/enc_e/aliner/ljet23.html Specs of the Learjet Model 23]
*[http://www.wingsoverkansas.com/history/article.asp?id=199 Learjet timeline from Wings of Kansas]

==External links==
{{commons category|Learjet 23}}
*[http://www.airliners.net/info/stats.main?id=264 A history of the Learjet 23-29 series on Airliners.net]

{{Learjet}}

[[Category:Learjet aircraft|23]]
[[Category:United States business aircraft 1960–1969]]
[[Category:Twinjets]]