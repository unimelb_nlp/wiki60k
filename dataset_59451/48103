{{Orphan|date=May 2015}}

[[File:Philippe Gentil.jpg|thumb|Philippe Gentil M.B.E. esq.]]

'''Joseph Philippe Gentil''' (11 February 1928), is a Mauritian [[composer]]<ref name="ProsperGentil2008">{{cite book|author1=Jean-Georges Prosper|author2=Philippe Gentil|title=Gloire à la mère patrie: inspirations de l'hymne national : XL|url=https://books.google.com/books?id=RRkhAQAAIAAJ|year=2008|publisher=ELP Publications|isbn=978-99903-87-61-2}}</ref> best known for composing [[Motherland (anthem)|The Motherland]], the [[National Anthem]] of [[Mauritius]].<ref name="Dukhira1992">{{cite book|author=Chit Geerjanand Dukhira|title=Mauritius and local government management|url=https://books.google.com/books?id=jPJEAQAAIAAJ|year=1992|publisher=All India Institute of Local Self-Government|page=43}}</ref>

Joseph Philippe Gentil was born in the island of [[Mauritius]], Indian Ocean of Irene Annibal and Eugene Gentil son of photographer Gabriel Gentil. He worked for over 30 years in the Mauritius Police force, more precisely with the Mauritius Police Band as a musician and composer. He was known in his younger days for his comedies played live in open air such as at the Jardin De La Compagnie, [[Port Louis]]. He later became well known on [[Mauritius Broadcasting Corporation|MBC TV]] for his comedy gigs. He has introduced various European and American musical talents to the Mauritian community by performing their songs, such as [[Fernandel]], [[Bourvil]], [[Glenn Miller Band]], [[Louis Armstrong]], and [[Henri Salvador]]. He has composed various marches performed by the Mauritius Police Band; unfortunately, many of his compositions got destroyed when the band's room in [[Vacoas]] burned down. However one of his compositions remains as the [[List of national anthems|National Anthem of Mauritius, the Motherland]] which he won as a competition when Mauritius became independent from the [[British Empire]] in 1968.

The [[Motherland (anthem)|lyrics]] were written by Mauritian poet Jean-Georges Prosper.

On the day of Independence, 12 March 1968 a national newspaper mistakenly published the name and photograph of Philippe Oh San who was the Mauritius Police Band's maestro as the National Anthem's composer. The remaining newspapers were returned to be reprinted with the correction.<ref>http://www.nationalanthems.info/mu.htm</ref> As a result, it has left the name of Mr. Oh San in the mind of many Mauritians as being the anthem's composer. However it was Mr. Oh San who after hearing Mr. Gentil's anthem being performed by the band musicians, encouraged for the work to be entered into the contest for a new National Anthem and after receiving Mr. Jean Georges Prosper's lyric as proposal for the anthem found that the lyric perfectly matched the melody composed by Philippe Gentil, the two being created separately.

Following the release of the  ''Motherland'' as the National Anthem, Gentil was appointed Member of the Order of the British Empire [[M.B.E.]] from [[Queen Elizabeth II]] of [[England]] and in 2008 and 2010 obtained other titles of the [[Order of the Star and Key of the Indian Ocean|Officer of the Order of the Star and Key of the Indian Ocean O.S.K.]] and [[Order of the Star and Key of the Indian Ocean|Commander of the Order of the Star and Key of the Indian Ocean C.S.K.]] from the Mauritian government for his musical service to the Mauritian nation.

Philippe Gentil has been the tutor and mentor for many musicians and professional performers for the largest part of his life. He still lives in Mauritius with his spouse Claudette Ivy Diane Gentil, born Castel (1940) with whom he has a daughter named Marie-Danielle Gentil (b 1960) and a son Mario Jacques Philippe Gentil (b 1961).

== References ==

<!--- See [[Wikipedia:Footnotes]] on how to create references using<ref></ref> tags which will then appear here automatically -->
{{Reflist}}

== External links ==

* The National Anthems of The World (Naxos) [http://www.naxos.com/catalogue/item.asp?item_code=8.225323]

{{DEFAULTSORT:Gentil, Joseph Philippe}}
[[Category:Living people]]
[[Category:1928 births]]
[[Category:Mauritian musicians]]