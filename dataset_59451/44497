{{Other uses}}
{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{Infobox book
| name           = Hard Times
| title_orig     = Hard Times – For These Times
| image          = Hardtimes serial cover.jpg
| caption        = Title page of the serial in [[Household Words]], April 1854
| author         = [[Charles Dickens]]
| illustrator    =
| cover_artist   =
| country        = England
| language       = English
| series         = 
| genre          = [[Novel]]
| publisher      = [[Bradbury & Evans]]
| published   = Serialised April 1854 –12 August 1854; book format 1854
| media_type     = Print
| pages          =
| isbn           =
| preceded_by    =
| followed_by    =
}}
'''''Hard Times – For These Times''''' (commonly known as '''''Hard Times''''') is the tenth novel by [[Charles Dickens]], first published in 1854. The book appraises English society and highlights the social and economic pressures of the era.

==Background==
{{refimprove section|date=April 2016}}

''Hard Times'' is unusual in several respects. It is by far the shortest of Dickens' novels, barely a quarter of the length of those written immediately before and after it.<ref>Phillip Collins, introduction to ''Hard Times''</ref> Also, unlike all but one of his other novels, ''Hard Times'' has neither a preface nor illustrations. Moreover, it is his only novel not to have scenes set in London.<ref>Philip Collins, introduction to ''Hard Times'', Everyman's Library, 1992</ref> Instead the story is set in the fictitious [[Victorian Era|Victorian]] industrial Coketown, a generic Northern English mill-town, in some ways similar to [[Manchester]], though smaller. Coketown may be partially based on 19th-century [[Preston, Lancashire|Preston]].

One of Dickens's reasons for writing ''Hard Times'' was that sales of his weekly periodical, ''[[Household Words]]'', were low, and it was hoped the novel's publication in instalments would boost circulation – as indeed proved to be the case. Since publication it has received a mixed response from critics. Critics such as [[George Bernard Shaw]] and [[Thomas Macaulay]] have mainly focused on Dickens's treatment of trade unions and his post–[[Industrial Revolution]] pessimism regarding the divide between capitalist mill owners and undervalued workers during the Victorian era.  [[F. R. Leavis]], a great admirer of the book, included it — but not Dickens' work as a whole — as part of his Great Tradition of English novels.

==Prevalence of utilitarianism==
{{unreferenced section|date=March 2013}}
The [[List of Utilitarians|Utilitarians]] were one of the targets of this novel. [[Utilitarianism]] was a prevalent school of thought during this period, its founders being [[Jeremy Bentham]] and [[James Mill]], father to political theorist [[John Stuart Mill]]. 
Bentham's former secretary, [[Edwin Chadwick]], helped design the [[Poor Law Amendment Act 1834|Poor Law of 1834]], which deliberately made workhouse life as uncomfortable as possible. In the novel, this attitude is conveyed in Bitzer's response to Gradgrind's appeal for compassion.

Dickens was appalled by what was, in his interpretation, a selfish philosophy, which was combined with materialist ''laissez-faire'' capitalism in the education of some children at the time, as well as in industrial practices. In Dickens's interpretation, the prevalence of utilitarian values in educational institutions promoted contempt between mill owners and workers, creating young adults whose imaginations had been neglected, due to an over-emphasis on facts at the expense of more imaginative pursuits.

Dickens wished to satirise radical Utilitarians whom he described in a letter to [[Charles Knight (publisher)|Charles Knight]] as "see[ing] figures and averages, and nothing else." He also wished to campaign for reform of [[List of topics on working time and conditions|working conditions]]. Dickens had visited factories in [[Manchester]] as early as 1839, and was appalled by the environment in which workers toiled. Drawing upon his own childhood experiences, Dickens resolved to "strike the heaviest blow in my power" for those who laboured in horrific conditions.

John Stuart Mill had a similar, rigorous education to that of Louisa Gradgrind, consisting of analytical, logical, mathematical, and statistical exercises. In his twenties, Mill had a nervous breakdown, believing his capacity for emotion had been enervated by his father's stringent emphasis on analysis and mathematics in his education. In the book, Louisa herself follows a parallel course, being unable to express herself and falling into a temporary depression as a result of her dry education.

==Publication==
The novel was published as a serial in Dickens's weekly publication, ''[[Household Words]]''. Sales were highly responsive and encouraging for Dickens who remarked that he was "Three parts mad, and the fourth delirious, with perpetual rushing at ''Hard Times''". The novel was serialised, in twenty weekly parts, between 1 April and 12 August 1854. It sold well, and a complete volume was published in August, totalling 110,000 words. Another related novel, ''[[North and South (1855 novel)|North and South]]'' by [[Elizabeth Gaskell]], was also published in this magazine.

{|
! Date of publication
! Chapters
! Book (from the novelisation)
|-
| 1 April 1854
| 1–3
|   Book I
|-
| 8 April 1854
| 4–5
|-
| 15 April 1854
| 6
|-
| 22 April 1854
| 7–8
|-
| 29 April 1854
| 9–10
|-
| 6 May 1854
| 11–12
|-
| 13 May 1854
| 13–14
|-
| 20 May 1854
| 15–16
|-
| 27 May 1854
| 17
|   Book II
|-
| 3 June 1854
| 18–19
|-
| 10 June 1854
| 20–21
|-
| 17 June 1854
| 22
|-
| 24 June 1854
| 23
|-
| 1 July 1854
| 24
|-
| 8 July 1854
| 25–26
|-
| 15 July 1854
| 27–28
|-
| 22 July 1854
| 29–30
|   Book III
|-
| 29 July 1854
| 31–32
|-
| 5 August 1854
| 33–34
|-
| 12 August 1854
| 35–37
|}

==Synopsis==
The novel follows a classical tripartite structure, and the titles of each book are related to ''Galatians'' 6:7, "For whatsoever a man soweth, that shall he also reap." Book I is entitled "Sowing", Book II is entitled "Reaping", and the third is "Garnering."

===Book I: Sowing===
Superintendent [[Gradgrind|Mr. Gradgrind]] opens the novel at his school in Coketown stating, "Now, what I want is, Facts", and interrogates one of his pupils, Sissy, whose father works at a circus. Because her father works with [[horse]]s, Gradgrind demands the definition of 'horse'. When she is scolded for her inability to factually define a horse, her classmate Bitzer gives a zoological profile; and Sissy is censured for suggesting that she would carpet a floor with pictures of flowers.

Louisa and Thomas, two of Mr. Gradgrind's children, go after school to see the touring circus run by Mr. Sleary, only to meet their father, who orders them home. Mr. Gradgrind has three younger children: Adam Smith, (after [[Adam Smith|the famous theorist]] of laissez-faire policy), Malthus (after Rev. [[Thomas Malthus]], who wrote ''An Essay on the Principle of Population'', warning of the dangers of future overpopulation), and Jane.

[[Image:Gradgrindapprehendshischildren.jpg|thumb|Gradgrind apprehends Louisa and Tom, his two eldest children, at the [[circus]].]]

Josiah Bounderby, "a man perfectly devoid of sentiment", is revealed as Gradgrind's close friend. Bounderby is a manufacturer and mill owner who is affluent as a result of his [[Entrepreneur|enterprise]] and [[Capital (economics)|capital]]. He often gives dramatic and falsified accounts of his childhood, which terrify Mr. Gradgrind's wife.

As they consider her a bad influence on the other children, Gradgrind and Bounderby dismiss Sissy from the school; but the three soon discover her father has abandoned her thereto, in hope that she will lead a better life without him. At this point members of the circus appear, led by their manager, Mr. Sleary. Mr. Gradgrind gives Sissy a choice: to return to the circus and forfeit her education, or to continue her education and work for Mrs. Gradgrind, never returning to the circus. Sissy accepts the latter, hoping to be reunited with her father. At the Gradgrind house, Tom and Louisa are discontented by their education.

Amongst the mill workers, known as "hands", is a "man of perfect integrity" named Stephen Blackpool, called "Old Stephen": another of the story's protagonists. When introduced, he has ended his day's work and meets his close friend Rachael. On entering his house he finds that his drunken wife, who has been living apart from him, has made an unwelcome return. Stephen is greatly perturbed, and visits Bounderby to ask how he can legally end his marriage. Mrs. Sparsit, Mr. Bounderby's paid companion, disapproves of Stephen's query and Bounderby explains that ending a marriage would be complex and prohibitively costly. Leaving the house, Stephen meets an old woman who seems interested in Bounderby and says she visits Coketown once a year.

Gradgrind tells Louisa that Josiah Bounderby, 30 years her senior, has proposed marriage to her, and quotes statistics to prove that an age difference does not make a marriage unhappy or short. Louisa passively accepts the offer, and the newlyweds set out to Lyons (Lyon), where Bounderby wants to observe how labour is used in the factories there. Tom, her brother, elatedly bids her farewell.

===Book 2: Reaping===
Book Two opens on Bounderby's new bank in Coketown, over which the "light porter", Sissy's old classmate Bitzer, and the austere Mrs. Sparsit keep watch at night. A well-dressed gentleman asks for directions to Bounderby's house, as Gradgrind has sent him from London with a letter of introduction. It is James Harthouse, who has tried several occupations and been bored by all of them.

Harthouse is introduced to Bounderby, who regales him with improbable stories of his childhood. Harthouse is utterly bored by him, but enamoured of the now melancholy Louisa. Louisa's brother Tom works for Bounderby, and has become reckless and wayward in his conduct. Tom admires Harthouse, who holds him in some contempt, and Tom discloses contempt for Bounderby in the presence of Harthouse, who notes Louisa's affection for Tom and later learns that Tom has money problems - and that Tom persuaded Louisa to marry Bounderby to make his own life easier.

At a crowded union meeting, the agitator Slackridge accuses Stephen Blackpool of treachery because he will not join the union, and Stephen learns he is to be 'sent to Coventry' - shunned by all his fellow workers. Summoned by Bounderby, he is asked what the men are complaining of; and when Stephen tries to explain,  Bounderby accuses Stephen of being a troublemaker and sacks him. Later Louisa and Tom visit Stephen, expressing regret, and Louisa gives him some money. Privately, Tom tells him to wait outside the bank after work.

When a robbery takes place at the bank, Stephen is suspected of the crime. Mrs. Sparsit observes the advancing relationship between James Harthouse and Louisa and suspects an adulterous liaison. Unable to hear their dialogue, she assumes the affair is progressing. When Harthouse confesses his love for Louisa, Louisa refuses him. They leave separately and Mrs. Sparsit follows Louisa to the station, where Louisa boards a train to her father's house and Mrs Sparsit loses her. When Louisa arrives, she is in an extreme state of distress. Having argued that her rigorous education has stifled her ability to express her emotions, Louisa collapses at her father's feet in a dead faint.

===Book 3: Garnering===
At Bounderby's London hotel Mrs. Sparsit gives him the news her surveillance has brought. Bounderby takes her back to Coketown and to Stone Lodge, where Louisa is resting. Gradgrind tells Bounderby that Louisa resisted Harthouse's advances but has experienced a crisis and needs time to recover. Bounderby is immensely indignant and ill-mannered, especially towards Mrs. Sparsit for misleading him. Ignoring Gradgrind's pleas, he announces that unless Louisa returns to him the next day, the marriage will terminate. She does not come back.

Harthouse leaves Coketown after Sissy tells him never to return. Rachael goes to the bank to say she knows where Stephen Blackpool is (he has left town to seek work elsewhere) and that she will write asking him to return to Coketown to clear his name. Bounderby is suspicious when she tells him Stephen was visited by Louisa and Tom the night he was dismissed, and brings her to Gradgrind's house where Louisa confirms Rachael's account.

With information from Rachael, Mrs. Sparsit tracks down Mrs. Pegler, the old woman who makes a mysterious annual visit to see Bounderby's house, and brings her to the house where she is revealed as Bounderby's mother. Far from having abandoned him to a life of hardship, she gave him a good upbringing and, when he became successful, allowed herself to be persuaded never to visit him. Bounderby is now publicly exposed as a ridiculous "Humbug".

On a Sunday outing, Rachael and Sissy find Stephen, who has fallen down an abandoned pit shaft while walking back to Coketown. He is rescued by villagers; but after professing his innocence and speaking to Rachael for the last time, he dies. Louisa and Sissy now suspect that Tom has committed the bank robbery, and told Stephen to loiter outside the bank in order to incriminate him. Sissy has already helped Tom escape by sending him to join Mr. Sleary's circus. Louisa and Sissy find Tom there, disguised in [[blackface]]. Gradgrind arrives and a plan is hatched with Sleary's co-operation to get Tom to Liverpool where he can escape abroad. The plan is temporarily foiled by the arrival of Bitzer, who hopes to obtain promotion from Bounderby by bringing Tom to justice; but Sleary arranges an ambush and Tom is taken to Liverpool where he boards ship.

Bounderby punishes Mrs Sparsit for his humiliation by turning her out. Five years later (says the narration), he will die of a fit in the street, while Mr. Gradgrind, having abandoned his Utilitarian ideas, will suffer the contempt of his fellow M.P.s. Tom will die in the Americas, having expressed penitence in a half-written letter to Louisa. Louisa herself will grow old and never remarries. Rachael will continue her life of honest hard work. Louisa, showing kindness to the less fortunate and being loved by Sissy's children, will spend her life encouraging imagination and fancy in all she encounters.

==Major characters==

===Mr. Gradgrind===
'''Thomas Gradgrind''' is the notorious school board Superintendent in Dickens's novel Hard Times who is dedicated to the pursuit of profitable enterprise. His name is now used generically to refer to someone who is hard and only concerned with cold facts and numbers. He is an intense follower of Utilitarian ideas. He soon sees the error of these beliefs however, when his children's lives fall into disarray. 

===Mr. Bounderby===
'''Josiah Bounderby''' is a business associate of Mr. Gradgrind. Given to boasting about being a self-made man, he employs many of the other central characters of the novel. He has risen to a position of power and wealth from humble origins (though not as humble as he claims). He marries Mr. Gradgrind's daughter Louisa, some 30 years his junior, in what turns out to be a loveless marriage. They have no children. Bounderby is callous, self-centred and ultimately revealed to be a liar and fraud.

===Louisa ===
'''Louisa (Loo) Gradgrind''', later '''Louisa Bounderby''', is the eldest child of the Gradgrind family. She has been taught to suppress her feelings and finds it hard to express herself clearly, saying as a child that she has "unmanageable thoughts."  After her unhappy marriage, she is tempted to adultery by James Harthouse, but resists him and returns to her father. Her rejection of Harthouse leads to a new understanding of life and of the value of emotions and the imagination. She reproaches her father for his dry and fact-based approach to the world and convinces him of the error of his ways.

===Sissy Jupe===
'''Cecilia (Sissy) Jupe''' is a circus girl of Sleary's circus, as well as a student of Thomas Gradgrind's very strict classroom. Sissy has her own set of values and beliefs which make her seem unintelligent in the Gradgrind household. At the end of the novel, when the Gradgrinds' philosophy of religiously adhering solely to facts breaks down, Sissy is the character who teaches them how to live.

Sissy Jupe is first introduced to the readers as Girl Number Twenty in Gradgrind's classroom. She struggles to keep up with Gradgrind's extreme reliance on the recitation of facts, and therefore is seen as not worthy of the school. Sissy is also representative of creativity and wonderment because of her circus background, and those were things that the Gradgrind children were not allowed to engage in. With the urging of Josiah Bounderby, Mr. Gradgrind goes to inform Sissy's father that she can no longer attend his school.

Gradgrind and Bounderby arrive at the Pegasus' Arms, the Coketown public-house where Sissy, her father, and the rest of Sleary's circus were staying. While Sissy and her father were very close once, Mr. Jupe packed up and abandoned his daughter, leaving Sissy alone. In a moment of compassion, Mr. Gradgrind takes Sissy into his home and gives her a second chance at the school. Sissy continues to fall behind in the school, so Mr. Gradgrind keeps her at home to tend to his invalid wife.

While Sissy is the device of imagination and fantasy in the novel, she also serves as the voice of reason. The reason she cannot grasp the philosophy of Gradgrind's classroom is because she actually has a more realistic view of how the world should be perceived. After Louisa and Mr. Gradgrind come to terms with the fact that their way of life is not working, Sissy is the one they come to; she takes care of Louisa and helps her live a new, happy life.
<ref name="Shmoop Editorial Team">{{cite web|title=Sissy Jupe in Hard Times|url=http://www.shmoop.com/search?q=sissy+jupe|accessdate=18 November 2012}}</ref>

===Tom===
'''Thomas (Tom) Gradgrind, Junior''' is the oldest son and second child of the Gradgrinds. Initially sullen and resentful of his father's Utilitarian education, Tom has a strong relationship with his sister Louisa. He works in Bounderby's bank (which he later robs), and turns to gambling and drinking. Louisa never ceases to adore Tom, and she aids Sissy and Mr. Gradgrind in saving her brother from arrest.

===Stephen Blackpool===
'''Stephen Blackpool''' is a worker at one of Bounderby's mills. He has a drunken wife who no longer lives with him but who appears from time to time. He forms a close bond with Rachael, a co-worker, whom he wishes to marry. After a dispute with Bounderby, he is dismissed from his work at the Coketown mills and, shunned by his former fellow workers, is forced to look for work elsewhere. While absent from Coketown, he is wrongly accused of robbing Bounderby's bank. On his way back to vindicate himself, he falls down a mine-shaft. He is rescued but dies of his injuries.

===Other characters===
'''Bitzer''' – is a very pale classmate of Sissy's who is brought up on facts and taught to operate according to self-interest. He takes up a job in Bounderby's bank, and later tries to arrest Tom.

'''Rachael''' – is the friend of Stephen Blackpool who attests to his innocence when he is accused of robbing Bounderby's bank by Tom. She is a factory worker, childhood friend of Blackpool's drunken and often absent wife, and becomes the literary tool for bringing the two parallel story lines together at the brink of Hell's Shaft in the final book.

'''Mrs. Sparsit''' – is a "classical" widow who has fallen on hard times. She is employed by Bounderby, and is jealous when he marries Louisa, delighting in the belief that Louisa is later about to elope with James Harthouse. Her machinations are unsuccessful and she is ultimately sacked by Bounderby.

'''James Harthouse''' – is an indolent, languid, upper-class gentleman, who attempts to woo Louisa.

'''Mrs. Gradgrind''' – the wife of Mr. Gradgrind, is an invalid and complains constantly. Tom Sr.'s apparent attraction to her is that she totally lacked 'fancy,' though she also seems to lack intelligence and any empathy for her children.

'''Mr. Sleary''' - the owner of the circus which employs Sissy's father. He speaks with a lisp. A kind man, he helps both Sissy and young Tom when they are in trouble.

'''Mrs. Pegler''' - an old woman who sometimes visits Coketown to observe the Bounderby estate. She is later revealed to be Bounderby's mother, proving his "rags-to-riches" story to be fraudulent.

==Major themes==
{{refimprove section|date=April 2016}}
Relating back to Dickens' aim to "strike the heaviest blow in my power," he wished to educate readers about the working conditions of some of the factories in the industrial towns of [[Manchester]], and [[Preston, Lancashire|Preston]]. Relating to this also, Dickens wished to confront the assumption that prosperity runs parallel to morality, a notion which is systematically deconstructed in this novel through his portrayal of the moral monsters, Mr. Bounderby and James Harthouse. Dickens was also campaigning for the importance of imagination in life, and for people's life to not be reduced to a collection of material facts and statistical analyses. Dickens's favourable portrayal of the Circus, which he describes as caring so "little for Plain Fact", is an example of this.

===Fact vs. Fancy===
This theme is developed early on, the bastion of Fact being the eminently practical Mr. Gradgrind, and his model school, which teaches nothing but [[Fact]]s. Any [[imaginative]] or [[aesthetic]] subjects are eradicated from the curriculum, while analysis, deduction and mathematics are emphasised. Fancy, the opposite of Fact, encompasses anything not purely functional and is epitomised by Sleary's circus. Sleary is reckoned a fool by Gradgrind and Bounderby, but it is Sleary who understands that people must be amused. Sissy, the circus performer's daughter, does badly at school, failing to remember the many facts she is taught, but is  genuinely virtuous and fulfilled. Gradgrind's own son Tom revolts against his upbringing, becoming a gambler and a thief, while Louisa becomes emotionally stunted, virtually [[soul]]less both as a young child and as an unhappily married woman. Bitzer, who adheres to Gradgrind's teachings, turns out an uncompassionate [[egotist]].

===Officiousness and spying===
Prying and knowledge is key to several characters, namely Mrs. Sparsit and Mr. Bounderby. Mr. Bounderby spends his whole time fabricating stories about his childhood, covering up the real nature of his upbringing, which is solemnly revealed at the end of the novel. While not a snooper himself, he is undone by Sparsit unwittingly revealing the mysterious old woman to be his own mother, and she unravels Josiah's secrets about his upbringing and fictitious stories. Mr. Bounderby himself superintends through calculating tabular statements and statistics, and is always secretly rebuking the people of Coketown for indulging in conceitful activities. This gives Bounderby a sense of superiority, as it does with Mrs. Sparsit, who prides herself on her salacious knowledge gained from spying on others. Bounderby's grasp for superiority is seen in Blackpool's talks to Bounderby regarding divorce proceedings and a union movement at his factory, accusing him that he is on a quest 'to feast on turtle soup and venison, served with a golden spoon.' All "superintendents" of the novel are undone in one way or another.

===Morality===
This is closely related to Dickens's typical social commentary, which is a theme he uses throughout his entire ''œuvre''. Dickens portrays the wealthy in this novel as being morally corrupt. Bounderby has no moral scruples; he fires Blackpool "for a novelty". He also conducts himself without any shred of decency, frequently losing his temper. He is cynically false about his childhood. Harthouse, a leisured gent, is compared to an "iceberg" who will cause a wreck unwittingly, due to him being "not a moral sort of fellow", as he states himself. Stephen Blackpool, a destitute worker, is equipped with perfect morals, always abiding by his promises, and always thoughtful and considerate of others, as is Sissy Jupe.

===The role of status on morality===
Another theme repeated by Dickens throughout Hard Times is the effects of social class on the morality of an individual. Some contrasting characters relating to this theme are Stephen and Rachel, and Tom and Mr. Bounderby. Stephen's honesty and Rachel's caring actions are qualities not shown in people from higher classes, but among hard working individuals who are browbeaten by the uncaring factory owners such as Bounderby. These qualities appear repeatedly, as Stephen works hard every day, until he decides to leave town to save the names of his fellow workers, and Rachel supports Stephen through this, while struggling to provide for herself as well. In contrast to these behaviours, Mr. Bounderby refuses to recognize the difficulties faced by those in lower classes, as seen by him completely casting aside Stephen's request for help. Other aristocratic characters simply carry out blatantly immoral actions, such as Tom throwing away his sister's money, falling into debt, then robbing a bank, and even framing someone else for his actions. Tom is also seen to be incredibly deceitful as he is able to keep his guilt hidden until the evidence points only toward him. On the contrary, when the news comes out that Stephen had robbed the bank, Stephen begins to head back to Coketown to face his problems and clear his name. Overall, the stark difference in morality between characters of dissimilar social status suggests Dickens’ idea that there is a form of innate natural law that may remain unhampered in those leading less titled lives. Stephen's concept of right and wrong is untainted by the manufactured values of utilitarianism, instilled into Tom and Bounderby.

==Literary significance and criticism==
[[Image:George bernard shaw.jpg|150px|thumb|[[George Bernard Shaw]] was critical of the book's message]]
{{refimprove section|date=April 2016}}

[[Literary criticism|Critics]] have had a diverse range of opinions on the novel. The critic [[John Ruskin]] declared ''Hard Times'' to be his favourite Dickens work due to its exploration of important social questions. However, [[Thomas Macaulay]] branded it "sullen socialism", on the grounds that Dickens did not fully comprehend the politics of the time. This point was also made by [[George Bernard Shaw]], who decreed ''Hard Times'' to be a novel of "passionate revolt against the whole industrial order of the modern world." Shaw criticised the novel for its failure to provide an accurate account of [[trade unionism]] of the time, deeming Dickens's character of Slackbridge, the poisonous orator, "a mere figment of middle-class imagination." However, believing it to be very different from Dickens's other novels, he also said: "Many readers find the change disappointing. Others find Dickens worth reading almost for the first time." <ref>George Bernard Shaw, quoted in ''Hard Times: The Dickens Critics'', 1966.</ref>

[[F. R. Leavis]], in ''The Great Tradition'', described the book as essentially a moral fable, and said that 'of all Dickens' works (it is) the one that has all the strengths of his genius – that of a completely serious work of art'.<ref>F.R. Leavis, ''The Great Tradition'', 1948</ref> This, however, was a view which he later revised in ''Dickens the Novelist'', which recognised that Dickens's strengths and artistry appeared fully in other works.

[[Walter Allen]], in an introduction to an alternative edition, characterised ''Hard Times'' as being an unsurpassed "critique of industrial society", which was later superseded by works of [[D. H. Lawrence]]. Other writers have described the novel as being, as [[G. K. Chesterton]] commented in his work ''Appreciations and Criticisms'', "the harshest of his stories"; whereas [[George Orwell]] praised the novel (and Dickens himself) for "generous anger."

==Adaptations==
The novel was adapted as a 1915 silent film, ''[[Hard Times (1915 film)|Hard Times]]'', directed by [[Thomas Bentley]].

In 1988 Portuguese director adapted the novel to the big screen in ''[[Hard Times (1988 film)|Hard Times]]'' (shot entirely in black & white) transferring the action to an unspecified industrial Portuguese city of the 1980s.

''Hard Times'' has been adapted twice for BBC Radio, first in 1998 starring [[John Woodvine]] as Gradgrind, [[Tom Baker]] as Josiah Bounderby and [[Anna Massey]] as Mrs. Sparsit, and again in 2007 starring [[Kenneth Cranham]] as Gradgrind, [[Philip Jackson (actor)|Philip Jackson]] as Bounderby, [[Alan Williams (actor)|Alan Williams]] as Stephen, [[Becky Hindley]] as Rachael, [[Helen Longworth]] as Louisa, [[Richard Firth]] as Tom and [[Eleanor Bron]] as Mrs. Sparsit.

In the theatre, ''Hard Times'' was adapted for the stage by Michael O'Brien and directed by [[Marti Maraden]] at Canada's [[National Arts Centre]] in 2000.<ref>Playwrights Canada Press Ltd [http://www.playwrightscanada.com/index.php/michael-o-brien.html]</ref>

The novel has also been adapted twice as a mini-series for British television, once in 1977 with [[Patrick Allen]] as Gradgrind, [[Timothy West]] as Bounderby, [[Rosalie Crutchley]] as Mrs. Sparsit and [[Edward Fox (actor)|Edward Fox]] as Harthouse, and again in 1994 with [[Bob Peck]] as Gradgrind, [[Alan Bates]] as Bounderby, [[Dilys Laye]] as Mrs. Sparsit, [[Bill Paterson (actor)|Bill Paterson]] as Stephen, [[Harriet Walter]] as Rachael and [[Richard E. Grant]] as Harthouse.

==References==
{{reflist|colwidth=40em}}

== Sources ==
* {{cite book| author = Ackroyd, Peter | title=Dickens: A Biography | publisher=[[Harpercollins]] | year=1991 | isbn= 0-06-016602-9}}
* {{cite book| author = Dickens, Charles| title=Hard Times| location= Wordsworth | publisher=Printing Press | year = 1854 | isbn= 1-85326-232-3}}
* {{cite book| author = House. M.; Storey. G. & Tillotson. K. | title=The Pilgrim Edition of the letters of Charles Dickens, Vol VIII. | publisher=[[Oxford University Press]] | year=1993 | isbn= 0-19-812617-4}}
* {{cite book| author = Leavis, F. R.| title=The Great Tradition| publisher = Chatto and Windus| year = 1970}}
* {{cite book| author = Thorold, Dinny | title=Introduction to Hard Times | location=Wordsworth | publisher=Printing Press | year=1995}}
* {{cite web| title = 1870 illustrations of Hard Times| work = Harry French's Twenty Plates for Dickens's "Hard Times for These Times " in the British Household Edition (1870s) | url = http://www.victorianweb.org/art/illustration/french/pva202.html | accessdate=23 May 2005}}
* {{cite web| title = Basic summary of Hard Times| work = ClassicNotes: Hard Times Short Summary | url = http://www.gradesaver.com/ClassicNotes/Titles/hard/shortsumm.html | accessdate=23 May 2005}}
* {{cite web| url = https://ebooks.adelaide.edu.au/c/chesterton/gk/appreciations-and-criticisms-of-the-works-of-charles-dickens/chapter16.html | title = Hard Times | work = Appreciations and Criticisms of the Works of Charles Dickens by G.K. Chesterton | accessdate=3 April 2016}}
* {{cite web|title=Hard Times: An Introduction |work=Hard Times: An Introduction by Walter Ellis |url=http://home.vicnet.net.au/~hartwell/hardtime/introd.htm |accessdate=23 May 2005 |deadurl=yes |archiveurl=https://web.archive.org/web/20041105064003/http://home.vicnet.net.au/~hartwell/hardtime/introd.htm |archivedate=5 November 2004 }}

==External links==
{{Wikisource}}
{{Commons category|Hard Times}}
;Online editions
* [https://archive.org/stream/christmasbooksha00dickiala#page/n409/mode/2up ''Hard Times''] at the [[Internet Archive]]
* {{gutenberg|no=786|name=Hard Times}}
* [http://www.dickens-literature.com/Hard_Times/ ''Hard Times'']—Searchable HTML version
* [http://etext.library.adelaide.edu.au/d/dickens/charles/d54ht/ ''Hard Times'']—Easy-to-read HTML version
* [http://dickens.stanford.edu/hard/times.html ''Hard Times'']—PDF scan of entire novel as it appeared in ''Household Words''
* {{librivox book | title=Hard Times | author=Charles Dickens}}

{{Charles Dickens}}
{{Hard Times}}

[[Category:1854 novels]]
[[Category:British novels adapted into films]]
[[Category:Novels adapted into television programs]]
[[Category:Novels by Charles Dickens]]
[[Category:Novels first published in serial form]]
[[Category:English novels]]
[[Category:Works originally published in Household Words]]
[[Category:Victorian novels]]