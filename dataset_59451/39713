{{redirect|Kerguelen}}
{{Use dmy dates|date=March 2014}}
{{Infobox country
|conventional_long_name = Kerguelen Islands
|native_name = ''{{lang|fr|Îles Kerguelen}}''
|common_name = Kerguelen Islands
|image_flag = Flag of the French Southern and Antarctic Lands.svg
|image_coat = |symbol_type = Coat of arms
|national_motto = |national_anthem =
|image_map = Kerguelen-pos.png
|map_caption = Location of the Kerguelen Islands in the [[Southern Ocean]]
|image_map2 = Kerguelen Map.png
|map_caption2 = Map of the Kerguelen Islands
|capital =
|largest_settlement = [[Port-aux-Français]]
|official_languages = French
|demonym =
|government_type = District of the [[French Southern and Antarctic Lands]]
|leader_title1 = [[President of France|President]]
|leader_name1 = [[François Hollande]]
|leader_title2 = Administrator
|leader_name2 = Pascal Bolot<ref name="Official organisational chart">[http://www.taaf.fr/IMG/pdf/organigramme_suite_reorg_v29_octobre_2012_avec_districts.pdf Official organisational chart]{{dead link|date=January 2017}}</ref>
|leader_title3 = Head of District
|leader_name3 = Disker<ref name="Official organisational chart"/>
|sovereignty_type = {{nowrap|[[French overseas territory]]}}
|established_event1 = Discovery
|established_date1 = February 1772
|established_event2 =
|established_date2 = 1944
|area_rank = |area_magnitude =
|area_km2 = 7215
|area_sq_mi =
|percent_water =
|population_estimate = around 45 (winter)<br/>around 110 (summer)
|population_estimate_rank = |population_estimate_year =
|population_census = |population_census_year = 0
|currency = [[Euro]]
|currency_code = EUR
|utc_offset = +4
|time_zone_DST = |utc_offset_DST = |DST_note =
|cctld = [[.tf]]
|calling_code = [[+262]]
|footnote_a =
}}

The '''Kerguelen Islands''' ({{IPAc-en|ˈ|k|ɜr|ɡ|ə|l|ɛ|n}} or {{IPAc-en|ˈ|k|ɜr|ɡ|ə|l|ən}};<ref>[[Oxford English Dictionary]]</ref> in [[French language|French]] ''{{lang|fr|Îles Kerguelen}}'' but officially ''{{lang|fr|Archipel des Kerguelen}}'', {{IPA-fr|kɛʁɡeˈlɛn|pron}}), also known as the '''Desolation Islands''' (''{{lang|fr|Îles de la Désolation}}'' in French), are a [[Archipelago|group of islands]] in the southern Indian Ocean constituting one of the two exposed parts of the mostly submerged [[Kerguelen Plateau]]. They are among the [[Extreme points of Earth#Remoteness|most isolated places on Earth]], located 450&nbsp;km (280&nbsp;mi) northwest of the uninhabited [[Heard Island and McDonald Islands]] and more than {{Convert|3300|km|mi|0|abbr=on}} from Madagascar, the nearest populated location (excluding the [[Alfred Faure]] scientific station in [[Île de la Possession]], about 1340&nbsp;km from there, and the non-permanent station located in [[Île Amsterdam]], 1,440&nbsp;km away). The islands, along with [[Adélie Land]], the [[Crozet Islands]], [[Amsterdam Island|Amsterdam]], and [[Île Saint-Paul|Saint Paul]] Islands, and France's [[Scattered Islands in the Indian Ocean]] are part of the [[French Southern and Antarctic Lands]] and are administered as a separate district.

The main island, Grande Terre, is {{Convert|6675|km2|mi2|0|abbr=on}} in area and is surrounded by a further 300 smaller islands and islets,<ref name="Britannica1">{{cite web | url=https://www.britannica.com/place/Kerguelen-Islands | title=Kerguelen Islands | work=Encyclopædia Britannica | accessdate=22 October 2016}}</ref>  forming an [[archipelago]] of {{Convert|7215|km2|mi2|0|abbr=on}}. The climate is raw and chilly with frequent high winds throughout the year. The surrounding seas are generally rough and they remain ice-free year-round. There are no indigenous inhabitants, but France maintains a permanent presence of 45 to 100 scientists, engineers and researchers.<ref name="UNESCO">[http://www.oceandocs.org/bitstream/1834/993/1/Comoros.pdf Sea Level Measurement and Analysis in the Western Indian Ocean], UNESCO Intergovernmental Oceanographic Commission</ref> There are no airports on the islands, so all travel and transport from the outside world is conducted by ship.

== History ==
[[File:Yves de Kerguelen.jpg|thumb|left|The islands are named after the French explorer [[Yves Joseph de Kerguelen de Trémarec]].|upright]]
Kerguelen Islands appear as the "Ile de Nachtegal" on [[Philippe Buache]]'s map from 1754 before the island was officially discovered in 1772. The Buache map has the title ''Carte des Terres Australes comprises entre le Tropique du Capricorne et le Pôle Antarctique où se voyent les nouvelles découvertes faites en 1739 au Sud du Cap de Bonne Esperance'' ('Map of the Southern Lands contained between the Tropic of Capricorn and the Antarctic Pole, where the new discoveries made in 1739 to the south of the Cape of Good Hope may be seen'). It is possible this early name was after Tasman's ship "De Zeeuwsche Nachtegaal." On the Buache map, "Ile de Nachtegal" is located at 43°S, 72°E, about 6 degrees north and 2 degrees east of the accepted location of Grande Terre.

The islands were officially discovered by the French navigator [[Yves-Joseph de Kerguelen-Trémarec]] on 12 February 1772. The next day Charles de Boisguehenneuc landed and claimed the island for the French crown.<ref>{{cite web|url=http://www.kerguelen-voyages.com/c/95/p/b0ef218cd2b18874c9a7d4b61925146f/Kerguelen-yves-tremarec-james-cook-asia-hillsborough-rhodes.html |title=Kerguelen – yves trémarec – james cook – asia – hillsborough – rhodes |publisher=Kerguelen-voyages.com |date= |accessdate=}}</ref> Yves de Kerguelen organised a second expedition in 1773 and arrived at the ''"baie de l'Oiseau"'' by December of the same year. On 6 January 1774 he commanded his lieutenant, Henri Pascal de Rochegude, to leave a message notifying any passers-by of the two passages and of the French claim to the islands.<ref name="Cook">[https://books.google.com/books?id=h6UFAAAAMAAJ&pg=PA149&dq=%22christmas+harbour%22+rochegude&hl=fr&sa=X&ei=SJpGUvumJM7W4ATm14GoAQ&ved=0CDkQ6AEwAQ#v=onepage&q=%22christmas%20harbour%22%20rochegude&f=false ''The Three Voyages of Captain James Cook Round the World''], volume 5, [[James Cook]], pub. Longman, Hurst, Rees, Orme, et Brown, Londres, 1821, pp. 146–151.</ref> Thereafter, a number of expeditions briefly visited the islands, including that of [[James Cook|Captain James Cook]] in December 1776 during his [[Third voyage of James Cook|third voyage]], who verified and confirmed the passage of de Kerguelen by discovering and annotating the message left by the French navigator.<ref name="Cook"/>
[[File:Christmas Harbour Kerguelens Land, 1811.jpg|thumb|left|250px|''Christmas Harbour, Kerguelens Land'', dated 1811 by [[George Cooke (painter)|George Cooke]]]]

Soon after their discovery, the archipelago was regularly visited by whalers and sealers (mostly British, American and Norwegian) who hunted the resident populations of whales and seals to the point of near extinction, including [[fur seal]]s in the 18th century and [[elephant seal]]s in the 19th century. Since the end of the whaling and sealing era, most of the islands' species have been able to increase their population again.<ref name="Estes">[https://books.google.com/books?id=daY_utPoJGAC&lpg=PA266&ots=5G6Y51ADaZ&dq=kerguelen%20whale%20population&pg=PA266#v=onepage&q=kerguelen%20whale%20population&f=false Whales, whaling, and ocean ecosystems], James A. Estes</ref>

In 1800, ''[[Hillsborough (1782 ship)|Hillsborough]]'' spent eight months sealing and [[whaling]] around the islands. During this time Captain Robert Rhodes, her master, prepared a chart of the islands.<ref>Clayton, Jane M. (2014) ''Ships employed in the South Sea Whale Fishery from Britain: 1775–1815: An alphabetical list of ships''. (Berforts Group), p.141. ISBN 978-1908616524</ref>

In 1825, the British sealer John Nunn and three crew members from ''Favourite'',  were shipwrecked on Kerguelen until they were rescued in 1827 by Captain Alexander Distant during his hunting campaign.<ref>{{cite web|url=http://www.kerguelen-voyages.com/consulter/pageperso.asp?IsMenuHaut=1&LangueID=1&PagePersoID=393 |title=Kerguelen – morell – john nunn – ross – ofley – challenger – fuller – eure – bossière |publisher=Kerguelen-voyages.com |date= |accessdate=}}</ref>
<ref>{{cite book|last1=Nunn|first1=John|title=Narrative of the Wreck of the "Favourite" on the Island of Desolation: detailing the adventures, sufferings and privations of J. Nunn, an historical account of the Island, and its whale and seal fisheries|date=1850|publisher=William Edward Painter|location=London|page=236|url=http://catalogue.nla.gov.au/Record/1912126|accessdate=29 November 2014}}</ref>
[[File:NUNN(1850) p182 THE EGG-CART.jpg|thumb|Illustration from John Nunn's book about the three years he and his shipwrecked crew survived on the island in the 1820s.]]

The islands were not completely surveyed until the [[Ross expedition]] of 1840.<ref name=Quanchi>{{cite book | last = Quanchi | first = Max|authorlink=| year = 2005 | title = Historical Dictionary of the Discovery and Exploration of the Pacific Islands | publisher = The Scarecrow Press  | location =  |pages=87–88| isbn = 0810853957}}</ref>

For the 1874 [[transit of Venus]], [[George Biddell Airy]] at the [[Royal Observatory, Greenwich|Royal Observatory]] of the UK organised and equipped five expeditions to different parts of the world. Three of these were sent to the Kerguelen Islands. The Reverend [[Stephen Joseph Perry]] led the British expeditions to the Kerguelen Islands. He set up his main observation station at Observatory Bay and two auxiliary stations, one at Thumb Peak ({{coord|49|30|47.3|S|70|10|18.1|E}}) led by Sommerville Goodridge, and the second at Supply Bay ({{coord|49|30|47.3|S|69|46|13.2|E}}) led by Cyril Corbet. Observatory Bay was also used by the German Antarctic Expedition led by [[Erich Dagobert von Drygalski]] in 1902–03. In January 2007, an archaeological excavation of this site was carried out.

In 1874–1875, British, German and U.S. expeditions visited Kerguelen to observe the [[transit of Venus]].<ref>[https://books.google.com/books?id=PYdBH4dOOM4C&pg=PA346 Exploring Polar Frontiers, p. 346], William James Mills, 2003</ref>

In 1877 the French started a coal mining operation; however, this was abandoned soon after.<ref>{{cite web|url=http://www.btinternet.com/~sa_sa/kerguelen/kerguelen_history_19.html |title=19th Century History of Kerguelen Island, South Indian Ocean |publisher=Btinternet.com |date=29 June 2003 |accessdate=30 March 2012 |archiveurl=http://archive.is/20120730074248/http%3A%2F%2Fwww.btinternet.com%2F%7Esa_sa%2Fkerguelen%2Fkerguelen_history_19.html |archivedate=30 July 2012 |deadurl=yes |df=dmy }}</ref>
[[File:Port-Gazelle le 8 janvier 1893.jpg|thumb|French sailors officially taking possession of the Islands on 8 January 1893]]
The Kerguelen Islands, along with the islands of Amsterdam and St Paul, and the Crozet archipelago were officially annexed by France in 1893, and were included as possessions in the French constitution in 1924 (in addition to that portion of Antarctica claimed by France and known as [[Adélie Land]]; as with all Antarctic territorial claims, France's possession on the continent is held in abeyance until a new international treaty is ratified that defines each claimant's rights and obligations).

The German [[auxiliary cruiser]] [[German auxiliary cruiser Atlantis|''Atlantis'']] called at Kerguelen during December 1940. During their stay the crew performed maintenance and replenished their water supplies. This ship's first fatality of the war occurred when a sailor, Bernhard Herrmann, fell while painting the funnel. He is buried in what is sometimes referred to as "the most southerly German war grave" of [[World War II]].

Kerguelen has been continually occupied since 1950 by scientific research teams, with a population of 50 to 100 frequently present.<ref name="UNESCO"/> There is also a French [[satellite]] tracking station.

Until 1955, the Kerguelen Islands were administrative-wise part of the French [[French Madagascar|Colony of Madagascar and Dependencies]]. That same year they collectively became known as ''{{lang|fr|Les Terres australes et antarctiques françaises}}'' (French Southern and Antarctic Lands) and were administratively part of the French ''{{lang|fr|Départment d'outre-mer de la Réunion}}''. In 2004 they were permanently transformed into their own entity (keeping the same name) but having inherited another group of five very remote tropical islands, ''[[Scattered islands in the Indian Ocean|{{lang|fr|nocat=true|les îles Éparses}}]]'', which are also owned by France and are dispersed widely throughout the southern Indian Ocean.{{clarify|date=May 2012}}

==Grande Terre==
[[File:Kerguelen RallierDuBatty.JPG|thumb|right|300px|[[Péninsule Rallier du Baty]]]]
[[File:Port aux Français.JPG|thumb|right|300px|[[Port aux Français]]]]
[[File:Kerguelen - Monts des Deux Frères.jpg|thumb|right|300px|Two Brothers Mountains (''Monts des Deux Frères'')]]
[[File:Kerguelen CookGlacier.JPG|thumb|300px|Terminus of a glacier of the [[Cook Ice Cap]]]]

The main island of the archipelago is called ''{{lang|fr|La Grande Terre}}''. It measures {{convert|150|km|abbr=on}} east to west and {{convert|120|km|abbr=on}} north to south.

[[Port-aux-Français]], a scientific base, is along the eastern shore of the [[Golfe du Morbihan (Kerguelen)|Gulf of Morbihan]] on La Grande Terre at {{Coord|49|21|S|70|13|E |region:FR-TF_type:city |name=Port-aux-Français}}. Facilities there include scientific-research buildings, a satellite tracking station, dormitories, a hospital, a library, a gymnasium, a pub, and the chapel of [[Notre-Dame des Vents]].

The highest point is [[Mont Ross]] in the [[Gallieni Massif]], which rises along the southern coast of the island and has an elevation of {{convert|1850|m}}. The [[Cook Ice Cap]] ({{lang fr|Calotte Glaciaire Cook}}),<ref name="mapcartaCGC">{{cite web|url=http://mapcarta.com/15439580|title=Calotte Glaciaire Cook|work=Mapcarta|accessdate=25 September 2016}}</ref> France's largest glacier with an area of about {{convert|403|km²|abbr=on}}, lies on the west-central part of the island. Overall, the glaciers of the Kerguelen Islands cover just over {{convert|500|km²|abbr=on}}. Grande Terre has also numerous bays, inlets, fjords, and coves, as well as several peninsulas and promontories. The most important ones are listed below:

* {{lang|fr|[[Courbet Peninsula]]}}
* {{lang|fr|[[Péninsule Rallier du Baty]]}}
* {{lang|fr|Péninsule Gallieni}}
* {{lang|fr|[[Péninsule Loranchet]]}}
* {{lang|fr|[[Péninsule Jeanne d'Arc]]}}
* {{lang|fr|[[Presqu'île Ronarc'h]]}}
* {{lang|fr|Presqu'île de la Société de Géographie}}
* {{lang|fr|Presqu'île Joffre}}
* {{lang|fr|Presqu'île du Prince de Galles}}
* {{lang|fr|Presqu'île du Gauss}}
* {{lang|fr|Presqu'île Bouquet de la Grye}}
* {{lang|fr|Presqu'île d'Entrecasteaux}}
* {{lang|fr|Presqu'île du Bougainville}}
* {{lang|fr|Presqu'île Hoche}}

===Notable localities===
There are also a number of notable localities, all on La Grande Terre (see also the main map):

* Anse Betsy [Betsy Cove] (a former geomagnetic station at {{Coord|49|10|S|70|13|E |region:FR-TF_type:landmark |name=Anse Betsy}}), on Baie Accessible [Accessible Bay], on the north coast of the Courbet Peninsula. On this site an astronomical and geomagnetic observatory was erected on 26 October 1874 by a German research expedition led by Georg Gustav Freiherr von Schleinitz. The primary goal of this station was the 1874 observation of the [[transit of Venus]].
* Armor (Base Armor), established in 1983, is located {{convert|40|km|abbr=on}} west of Port-aux-Français at the bottom of Morbihan Gulf, for the acclimatization of salmon to the Kerguelen islands.<ref>{{cite book |last=Kauffmann |first=Jean-Paul |authorlink= |title=Voyage to Desolation Island |url=https://books.google.com/books?id=wMYPAQAAIAAJ |accessdate=18 December 2012 |year=2001 |publisher=Random House |location= |isbn=1860469264 |pages=77–78 }}</ref>
* Baie de l'Observatoire [Observatory Bay] (a former geomagnetic observation station at {{Coord|49|21|S|70|12|E |region:FR-TF_type:landmark |name=Baie de l'Observatoire}}), just west of Port-Aux-Français, on the eastern fringe of the Central Plateau, along the northern shore of the Golfe du Morbihan.
* Cabane Port-Raymond (scientific camp at {{Coord|49|20|S|69|49|E |region:FR-TF_type:landmark |name=Cabane Port-Raymond}}), at the head of a fjord cutting into the Courbet Peninsula from the south.
* Cap Ratmanoff (geomagnetic station at {{Coord|49|14|S|70|34|E |region:FR-TF_type:landmark |name=Cap Ratmanoff}}), the eastmost point of the Kerguelens.
* La Montjoie (scientific camp at {{Coord|48|59|S|68|50|E |region:FR-TF_type:landmark |name=La Montjoie}}), on the south shore of Baie Rocheuse, along the northwestern coast of the archipelago.
* Molloy (Pointe Molloy), a former observatory ten kilometers west of the present-day Port-Aux-Français, along the south coast of the Courbet Peninsula, or northern shore of the Golfe du Morbihan (Kerguelen), at {{Coord|49|21|38|S|70|3|50|E |region:FR-TF_type:landmark |name=Molloy}}. An American expedition led by G.&nbsp;P. Ryan erected a station at this site on 7 September 1874. That station was also established to observe the 1874 transit of Venus.
* Port [[Bizet]] (seismographic station at {{Coord|49|31|12|S|69|54|36|E |region:FR-TF_type:landmark |name=Port Bizet}}), on the northeastern coast of Île Longue. This also serves as the principal sheep farm for the island's resident flock of [[Bizet (sheep)|Bizet sheep]].
* Port Christmas (a former geomagnetic station at {{Coord|48|41|S|69|03|E |region:FR-TF_type:landmark |name=Port Christmas}}), on Baie de l'Oiseau, in the extreme northwest of the Loranchet Peninsula. This place was so named by Captain [[James Cook]], who re-discovered the islands and who anchored there on Christmas Day, 1776. This is also the place where Captain Cook coined the name "Desolation Islands" in reference to what he saw as a sterile landscape.
* Port Couvreux (a former whaling station, experimental sheep farm, and geomagnetic station, at {{Coord|49|17|S|69|42|E |region:FR-TF_type:landmark |name=Port Couvreux}}), on Baie du Hillsborough, on the southeast coast of Presqu'île Bouquet de&nbsp;la Grye. Starting in 1912, sheep were raised here to create an economic base for future settlement.  However, the attempt failed and the last inhabitants had to be evacuated, and the station abandoned, in 1931. The huts remain, as well as a graveyard with five anonymous graves. These are those of the settlers who were unable to survive in the harsh environment.
* Port Curieuse (a harbor on the west coast across Île de l'Ouest {{Coord|49|22|S|68|48|E |region:FR-TF_type:landmark |name=Port Curieuse}}). The site was named after the ship ''La&nbsp;Curieuse'', which was used by [[Raymond Rallier du Baty]] on his second visit to the islands (1913–14).
* Port Douzième (literally Twelfth Port, a hut and former geomagnetic station at {{Coord|49|31|S|70|09|E |region:FR-TF_type:landmark |name=Port Douzième}}), on the north coast of Presqu'île Ronarch, southern shore of the Golfe du Morbihan.
* Port Jeanne d'Arc (a former whaling station founded by a Norwegian whaling company in 1908, and a former geomagnetic station at {{Coord|49|33|S|69|49|E |region:FR-TF_type:landmark |name=Port Douzième}}), in the northwestern corner of Presqu'île Jeanne d'Arc, looking across the Buenos Aires passage to Île&nbsp;Longue ({{convert|4|km|abbr=on}} northeast). The derelict settlement consists of four residential buildings with wooden walls and tin roofs, and a barn. One of the buildings was restored in 1977, and another in 2007.

From 1968 to 1981, {{Coord|49|21|S|70|16|E |region:FR-TF_type:landmark |name=Rocket launch site}} just east of Port-aux-Français was a launching site for [[sounding rocket]]s, some for French ([[Dragon (rocket)|Dragon rockets]]), American ([[Arcas (rocket)|Arcas]]) or French-Soviet ([[Eridan]]s) surveys, but at the end mainly for a Soviet program ([[M-100 (rocket)|M-100]]).<ref>{{fr}} [http://fuseurop.univ-perp.fr/b_tmp_f.htm bases temporaires de lancements de fusées]</ref> It is understood the islands have received the attention of the French military.

==Islands==
The following is a list of the most important adjacent islands:

* '''[[Île Foch]]''' in the north of the archipelago, at {{Coord|49|0|S|69|17|E |type:isle_region:FR-TF |name=Île Foch}}, is the largest satellite island with an area of 206.2&nbsp;km². Its highest point, at 687&nbsp;m, is called ''La Pyramide Mexicaine''.
* '''[[Île Howe]]''' which lies less than one kilometre off the northern coast of Ile Foch is, at ~54&nbsp;km², the second most important offlier in the Kerguelens ({{Coord|48|52|S|69|27|E |type:isle_region:FR-TF |name=Île Howe}}).
* '''[[Île Saint-Lanne Gramont]]''', is to the west of Île Foch in the Golfe Choiseul. It has an area of 45.8&nbsp;km². Its highest point reaches 480&nbsp;m ({{Coord|48|55|S|69|12|E |type:isle_region:FR-TF |name=Île Saint-Lanne Gramont}}).
* '''[[Île du Port]]''', also in the north in the [[Golfe des Baleiniers]] at {{Coord|49|11|S|69|36|E |type:isle_region:FR-TF |name=Île du Port}}, is the fourth largest satellite island with an area of 43&nbsp;km², near its centre it reaches an altitude of 340&nbsp;m.
* '''[[Île de l'Ouest]]''' (west coast, about 33&nbsp;km², {{Coord|49|21|S|68|44|E |type:isle_region:FR-TF |name=Île de l'Ouest}})
* '''[[Île Longue (Kerguelen Islands)|Île Longue]]''' (southeast, about 35&nbsp;km² {{Coord|49|32|S|69|54|E |type:isle_region:FR-TF |name=Île Longue}})
* '''[[Îles Nuageuses]]''' (northwest, including île de Croÿ, île du Roland, îles Ternay, îles d'Après, {{Coord|48|37|S|68|44|E |type:isle_region:FR-TF |name=Îles Nuageuses}})
* '''Île de Castries''' ({{Coord|48|41|S|69|29|E |type:isle_region:FR-TF |name=Île de Castries}})
* '''[[Îles Leygues]]''' (north, including île de Castries, île Dauphine, {{Coord|48|41|S|69|29|E |type:isle_region:FR-TF |name=Îles Leygues}})
* '''Île Violette''' ({{Coord|49|07|S|69|40|E |type:isle_region:FR-TF |name=Île Violette}})
* '''[[Île Australia]]''' (also known as ''Île aux Rennes'' – ''Reindeer Island'') (western part of the Golfe du Morbihan, area 36.7&nbsp;km², altitude 145&nbsp;m, {{Coord|49|27|S|69|51|E |type:isle_region:FR-TF |name=Île Australia}})
* '''[[Île Haute]]''' (western part of the Golfe du Morbihan, altitude 321&nbsp;m, {{Coord|49|23|S|69|55|E |type:isle_region:FR-TF |name=Île Haute}})
* '''Île Mayès''' ({{Coord|49|28|20|S|69|55|55|E |type:isle_region:FR-TF |name=Île Mayès}})
* '''[[Îles du Prince-de-Monaco]]''' (south, in the Audierne bay, {{Coord|49|36|S|69|14|E |type:isle_region:FR-TF |name=Îles du Prince-de-Monaco}})
* '''[[Îles de Boynes]]''' (four small islands 30&nbsp;km south of [[Presqu'ile Rallier du Baty]] on the main island, {{Coord|50|01|S|68|52|E |type:isle_region:FR-TF |name=Îles de Boynes}})
* '''[[Île Altazin]]''' (a small island in the Swains Bay, {{Coord|49|38|S|69|45|E |type:isle_region:FR-TF |name=Île Altazin}})
* '''[[Île Gaby]]''' (a small island in the Swains Bay, {{Coord|49|39|S|69|46|E |type:isle_region:FR-TF |name=Île Gaby}})

==Economy==
[[File:800px-RV Marion Dufresne.jpg|thumb|The French supply ship ''[[Marion Dufresne (1995)|Marion Dufresne]]'' makes regular calls at the Kerguelen Islands and typically carries a small contingent of tourists.]]
Principal activities on the Kerguelen Islands focus on scientific research – mostly earth sciences and biology.

The former sounding rocket range to the east of Port-aux-Français {{Coord|49|21|S|70|16|E |region:FR-TF_type:landmark |name=FUSOV}} is currently the site of a [[SuperDARN]] radar.

Since 1992, the French [[Centre National d'Études Spatiales]] (CNES) has operated a [[satellite]] and [[rocket]] [[tracking station]] which is located four kilometers east of [[Port-aux-Français]]. CNES needed a tracking station in the [[Southern Hemisphere]], and the French government required that it be located on French territory, rather than in a populated, but foreign, place like Australia or [[New Zealand]].

Agricultural activities were limited until 2007 to raising sheep (about 3,500&nbsp;[[Bizet (sheep)|Bizet]] sheep – a breed of sheep that is [[Rare breed (agriculture)|rare]] in mainland France) on Longue Island for consumption by the occupants of the base, as well as small quantities of vegetables in a greenhouse within the immediate vicinity of the main French base. There are also feral rabbits and sheep that can be hunted, as well as wild birds.

There are also 5 fishing boats and vessels, owned by fishermen on [[Réunion Island]] (a ''department'' of France about 3,500&nbsp;km (2,300 miles) to the north) who are licensed to fish within the archipelago's [[Exclusive Economic Zone]].

==Geology==
[[File:Kerguelen-geo-en.png|thumb|right|300px|Simplified geological map of the Kerguelen Islands]]

The Kerguelen islands form an emerged part of the submerged [[Kerguelen Plateau]], which has a total area nearing 2.2&nbsp;million square kilometres.{{Citation needed|date=March 2011}} The [[Kerguelen Plateau|plateau]] was built by volcanic eruptions associated with the [[Kerguelen hotspot]], and now lies on the [[Antarctic plate]].<ref>[http://t-extreme.ifrance.com/extreme/extreme5/plateau_k.htm article by Roland Shlich (Research Manager at the [[CNRS]]) {{dead link|date=January 2017}}]</ref>

The major part of the volcanic formations visible on the islands is characteristic of an effusive volcanism, which caused a [[trap rock]] formation to start emerging above the level of the ocean 35&nbsp;million years ago. The accumulation is of a considerable amount; [[basalt]] flows, each with a thickness of three to ten metres, stacked on top of each other, sometimes up to a depth of 1,200&nbsp;metres. This form of volcanism creates a monumental relief shaped as stairs of pyramids.

Other forms of volcanism are present locally, such as the [[strombolian]] volcano [[Mont Ross]], and the volcano-plutonic complex on the Rallier du Baty peninsula. Various veins and extrusions of lava such as [[trachyte]]s, trachyphonolites and [[phonolite]]s are common all over the islands.

No eruptive activity has been recorded in historic times, but some [[fumarole]]s are still active in the South-West of the Grande-Terre island.

[[File:Kerguelen MontRoss.JPG|thumb|left|260px|[[Mont Ross]]]]

A few [[lignite]] strata, trapped in basalt flows, reveal fossilised [[Araucariaceae|araucarian]] fragments, dated at about 14&nbsp;million years of age.

[[Glacier|Glaciation]] caused the depression and tipping phenomena which created the gulfs at the north and east of the archipelago. Erosion caused by the glacial and fluvial activity carved out the valleys and fjords; erosion also created conglomerate [[Detritus (geology)|detrital]] complexes, and the plain of the [[Courbet Peninsula]].

The islands are part of a submerged [[microcontinent]] called the [[Kerguelen Plateau|Kerguelen sub-continent]].<ref>[https://web.archive.org/web/20031014200102/http://www.utexas.edu:80/opa/news/99newsreleases/nr_199905/nr_continent990528.html UT Austin scientist plays major role in study of underwater "micro-continent".] Retrieved on 29 June 2007</ref> The microcontinent emerged substantially above sea level for three periods between 100&nbsp;million years ago and 20 million years ago. The so-called Kerguelen sub-continent may have had tropical [[flora]] and [[fauna]] about 50&nbsp;million years ago. The Kerguelen sub-continent finally sank 20&nbsp;million years ago and is now one to two kilometers below sea level. Kerguelen's [[sedimentary rock]]s are similar to ones found in Australia and [[India]], indicating they were all once connected. Scientists hope that studying the Kerguelen sub-continent will help them discover how Australia, [[India]], and [[Antarctica]] broke apart.<ref>[http://news.bbc.co.uk/2/hi/science/nature/353277.stm Sci/Tech 'Lost continent' discovered] Retrieved on 29 June 2007</ref>
{{Clear}}

==Climate==
[[File:Kerguelen Islands satfoto.jpg|thumb|Kerguelen Islands from space, 2016|350px]]
Kerguelen's climate is oceanic, cold and extremely windswept. Under the [[Köppen climate classification]], Kerguelen's climate is considered to be an ''ET'' or [[tundra climate]], which is technically a form of [[polar climate]], as the average temperature in the warmest month is below {{convert|10|C}}.<ref name=Peel>{{cite journal | author=Peel, M. C. and Finlayson, B. L. and McMahon, T. A. | year=2007 | title= Updated world map of the Köppen–Geiger climate classification | journal=Hydrol. Earth Syst. Sci. | volume=11 | pages=1633–1644 |doi=10.5194/hess-11-1633-2007 | url=http://www.hydrol-earth-syst-sci.net/11/1633/2007/hess-11-1633-2007.html | issn = 1027-5606}} ''(direct: [http://www.hydrol-earth-syst-sci.net/11/1633/2007/hess-11-1633-2007.pdf Final Revised Paper])''</ref> Comparable climates include: the [[Aleutian Islands]], [[Campbell Island, New Zealand|Campbell Island]] (New Zealand), the [[Crozet Islands]], [[Iceland]], northern [[Kamchatka Peninsula]], [[Labrador]] and [[Tierra del Fuego]].

All climate readings come from the [[Port-aux-Français]] base, which has one of the more favourable climates in Kerguelen due to its proximity to the coast and its location in a gulf sheltered from the wind.

The average annual temperature is {{convert|4.9|C}} with an annual range of around {{convert|6|C-change}}. The warmest months of the year include January and February, with average temperatures between {{convert|7.8|and|8.2|C}}.The coldest month of the year is August with an average temperature of {{convert|2.1|C}}. Annual high temperatures rarely surpass {{convert|20|C}}, while temperatures in winter have never been recorded below {{convert|−10|C}} at sea level.

Kerguelen receives frequent precipitation, with snow throughout the year as well as rain. Port-aux-Français receives a modest amount of precipitation ({{convert|708|mm|abbr=on}} per year) compared to the west coast which receives an estimated three times as much precipitation per year.

The mountains are frequently covered in snow but can thaw very quickly in rain. Over the course of several decades, many permanent glaciers have shown signs of retreat, with some smaller ones having disappeared completely.

The west coast receives almost continuous wind at an average speed of {{convert|35|km/h|abbr=on}}, due to the islands' location in between the [[Roaring Forties]] and the Furious Fifties. Wind speeds of {{convert|150|km/h|abbr=on}} are common and can even reach {{convert|200|km/h|abbr=on}}.

Waves up to {{convert|12|–|15|m|abbr=on}} high are common, but there are many sheltered places where ships can dock.

Due to the island's southern latitude it experiences Astronomical Twilight (sun illumination is barely distinguishable at nighttime) for a couple of weeks during the summer.

{{Weather box
|location = Port aux Français, Kerguelen
|metric first = yes
|single line = yes
|Jan record high C = 22.3
|Feb record high C = 22.3
|Mar record high C = 21.0
|Apr record high C = 23.0
|May record high C = 16.8
|Jun record high C = 14.5
|Jul record high C = 13.4
|Aug record high C = 14.4
|Sep record high C = 15.8
|Oct record high C = 19.1
|Nov record high C = 21.3
|Dec record high C = 21.6
|Jan high C = 11.1
|Feb high C = 11.5
|Mar high C = 10.5
|Apr high C = 9.0
|May high C = 6.7
|Jun high C = 5.2
|Jul high C = 4.7
|Aug high C = 4.6
|Sep high C = 5.3
|Oct high C = 7.0
|Nov high C = 8.6
|Dec high C = 10.1
|year high C = 7.8
|Jan mean C = 7.8
|Feb mean C = 8.2
|Mar mean C = 7.3
|Apr mean C = 6.1
|May mean C = 4.2
|Jun mean C = 2.8
|Jul mean C = 2.2
|Aug mean C = 2.1
|Sep mean C = 2.5
|Oct mean C = 3.9
|Nov mean C = 5.3
|Dec mean C = 6.8
|year mean C = 4.9
|Jan low C = 4.4
|Feb low C = 4.7
|Mar low C = 4.1
|Apr low C = 3.2
|May low C = 1.5
|Jun low C = 0.4
|Jul low C = -0.3
|Aug low C = -0.4
|Sep low C = -0.2
|Oct low C = 0.7
|Nov low C = 2.0
|Dec low C = 3.4
|year low C = 1.9
|Jan record low C = -1.5
|Feb record low C = -1.0
|Mar record low C = -0.9
|Apr record low C = -2.7
|May record low C = -5.9
|Jun record low C = -8.3
|Jul record low C = -8.0
|Aug record low C = -9.5
|Sep record low C = -7.7
|Oct record low C = -5.0
|Nov record low C = -3.7
|Dec record low C = -1.2
|Jan precipitation mm = 72.2
|Feb precipitation mm = 49.5
|Mar precipitation mm = 57.5
|Apr precipitation mm = 59.6
|May precipitation mm = 59.9
|Jun precipitation mm = 75.9
|Jul precipitation mm = 62.9
|Aug precipitation mm = 63.4
|Sep precipitation mm = 62.3
|Oct precipitation mm = 59.3
|Nov precipitation mm = 51.9
|Dec precipitation mm = 55.1
|Jan humidity = 78
|Feb humidity = 79
|Mar humidity = 82
|Apr humidity = 86
|May humidity = 88
|Jun humidity = 89
|Jul humidity = 89
|Aug humidity = 87
|Sep humidity = 84
|Oct humidity = 80
|Nov humidity = 75
|Dec humidity = 77
|source 1 = MeteoStats<ref name="MeteoStats">{{cite web | url = http://meteostats-bzh.ath.cx:93/meteostats/index.php?page=stati&id=23|title = Le climat à Port aux Français (en °C et mm, moyennes mensuelles 1971/2000 et records depuis 1973) sur MeteoStats}}{{dead link|date=January 2017}}</ref>
|date=August 2010
}}

==Flora and fauna==
{{Main article|Flora and fauna of the Kerguelen Islands}}
[[File:Pringlea antiscorbutica Mayes fake.jpg|thumb|Kerguelen cabbages]]
The islands are part of the [[Southern Indian Ocean Islands tundra]] [[ecoregion]] that includes several [[subantarctic]] islands. Plant life is mainly limited to grasses, [[moss]]es and [[lichen]]s, although the islands are also known for the indigenous, edible [[Kerguelen cabbage]], a good source of [[vitamin C]] to [[mariner]]s.{{citation needed|date=July 2013}} The main indigenous animals are insects along with large populations of ocean-going [[seabird]]s, [[Pinniped|seal]]s and penguins.<ref>{{WWF ecoregion|id=an1104|name=Southern Indian Ocean Islands tundra}}</ref>

The wildlife is particularly vulnerable to [[introduced species]] and one particular problem has been cats. The main island is the home of a well-established [[feral cat]] population, descended from [[ships' cats]].<ref>[http://blogs.senat.fr/iles-subantarctiques/2010/04/06/minou-ce-dangereux-predateur/ Minou, ce dangereux prédateur]</ref> They survive on sea birds and the [[feral]] rabbits that were introduced to the islands. There are also populations of [[mouflon|wild sheep]] (''[[Ovis orientalis orientalis]]'')  and [[reindeer]].

In the 1950s and 1960s, Edgar Albert de la Rue, a French geologist began the introduction of several species of salmonids. Of the seven species introduced, only [[brook trout]] {{nowrap|''Salvelinus fontinalis''}} and [[brown trout]] {{nowrap|''Salmo trutta''}} survived to establish wild populations.<ref>{{cite book |author=Newton, Chris |title=The Trout's Tale – The Fish That Conquered an Empire |publisher=Medlar Press |location=Ellesmere, Shropshire |year=2013 |isbn=978-1-907110-44-3 |chapter=The Monsters of Kerguelen |pages=161–170}}</ref>

===Coleoptera===
* [[Carabidae]]
** [[species:Oopterus soledadinus|''Oopterus soledadinus'']] [nonnative]
* [[Hydraenidae]]
** [[species:Meropathus chuni|''Meropathus chuni'']] [endemic]

== In popular culture==
The islands figure in a number of fictional works. The title character in [[Edgar Allan Poe]]'s 1838 novel, ''[[The Narrative of Arthur Gordon Pym of Nantucket]]'', visits the islands.<ref>{{cite book |last1=Sharma |first1=Raja |title=Ready Reference Treatise: The Narrative of Arthur Gordon Pym of Nantucket |date=2016 |publisher=Lulu Press |isbn=9781329804036 |page=41 |url=https://books.google.com/books?id=RRdnCwAAQBAJ&pg=PT41}}</ref> The 1874 short story "[[The Tachypomp]]" by [[Edward Page Mitchell]] tells of a hole through the center of the earth with one end in the United States and the other in "Kerguellen's Land" (which is roughly [[Antipodes|antipodal]] to the United States and Canada). [[Henry De Vere Stacpoole]] set his 1919 novel ''The Beach of Dreams'' on the islands.<ref>{{cite book |last1=Stilgoe |first1=John R. |title=Lifeboat |date=2003|publisher=University of Virginia Press |isbn=9780813922218 |page=36 |url=https://books.google.com/books?id=KvB6NPbSByAC&pg=PA36}}</ref> The Kerguelen Islands were the setting for a post-Second World War confrontation between [[W. E. Johns]]'s recurring hero, [[Biggles]] and the crew of a [[gold bullion]]-bearing German  [[U-boat]], in the 1951 novel ''[[Biggles' Second Case]]''. The fifth book in [[Patrick O'Brian]]'s [[Aubrey–Maturin series]], published in 1978, is entitled ''[[Desolation Island (novel)|Desolation Island]]''.<ref>{{cite book |last1=O'Brian |first1=Patrick |title=Desolation island (Jack Aubrey vol 5) |date=1978 |publisher=Collins |isbn=9780006499244 |url=https://www.worldcat.org/oclc/655256523}}</ref>

The islands inspired the 2008 song "[[Sparks of Ancient Light|The Loneliest Place on the Map]]" by singer/songwriter [[Al Stewart]].<ref>{{cite news |last1=Barnard |first1=Jason |last2=Stewart |first2=Al |title=Al Stewart – Past, Present and Future |url=http://thestrangebrew.co.uk/articles/al-stewart-past-present-and-future |accessdate=April 12, 2016 |work=The Strange Brew |date=2016}}</ref>

==See also==
{{Portal|Geography|France}}

* [[Administrative divisions of France]]
* [[French overseas departments and territories]]
* [[Islands controlled by France in the Indian and Pacific oceans]]
* [[List of Antarctic and subantarctic islands]]

==References==
{{Reflist|30em}}

==External links==
{{Wikivoyage|Kerguelen}}
{{Commons|Kerguelen Islands}}
{{Wikibooks|1= Geography of France|2= Réunion, Mayotte, and the French Southern and Antarctic Lands#Kerguelen Islands|3= Kerguelen Islands}}
* {{Official website|1=www.outre-mer.gouv.fr/outremer/front?id=outremer/decouvrir_outre_mer/taaf}} {{fr}}
* {{Official website|www.taaf.fr}} {{fr}}
* {{cite web|url=http://membres.lycos.fr/ker18/Carto/frameIgn.html |title=Cartography of the Kerguelen |accessdate=2007-04-02 |deadurl=bot: unknown |archiveurl=https://web.archive.org/web/20030413030523/http://membres.lycos.fr/ker18/Carto/frameIgn.html |archivedate=13 April 2003 |df=dmy }}Including a toponymy index.
* [http://www.kerguelen-island.org/ Personal site with many pictures]
* [http://www.astronautix.com/sites/keruelen.htm Rocket launches on the Kerguelen Islands]
* {{cite web|url=http://www.btinternet.com/~sa_sa/kerguelen/kerguelen_islands.html |title=South Atlantic & Subantarctic Islands site, Kerguelen Archipelago page |archiveurl=http://archive.is/20121129220657/http%3A%2F%2Fwww.bt.com%2Fsorrypages%2Fsorry%2Fplatform%2Fbtinternet.html |archivedate=29 November 2012 |deadurl=yes |df=dmy }}

{{Districts of the French Southern and Antarctic Lands}}
{{Important Bird Areas of French Southern Territories}}
{{French overseas departments and territories}}
{{Outlying territories of European countries}}

{{Coord|49|15|S|69|10|E |region:FR-TF_scale:1250000 |display=title}}

{{Authority control}}

[[Category:Kerguelen Islands| 01]]
[[Category:Tundra]]
[[Category:Ecoregions of the Antarctic]]
[[Category:Lists of coordinates]]
[[Category:Subantarctic islands]]
[[Category:Volcanic islands]]
[[Category:Volcanoes of the French Southern and Antarctic Lands]]
[[Category:Archipelagoes of the French Southern and Antarctic Lands]]