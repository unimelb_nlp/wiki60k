{{featured article}}
{| {{Infobox ship begin}}
{{Infobox ship image
| Ship image   = [[File:SMS Kaiser Karl der Grosse.jpg|300px]]
| Ship caption = ''Kaiser Karl der Grosse'' in 1902
}}
{{Infobox ship career
| Hide header = 
| Ship country = [[German Empire]]
| Ship flag = {{shipboxflag|German Empire|naval}}
| Ship name = ''Kaiser Karl der Grosse''
| Ship namesake = [[Charlemagne]] (''Karl der Grosse'' in German)
| Ship builder = [[Blohm & Voss]], [[Hamburg]]
| Ship laid down = September 1898
| Ship launched = 18 October 1899
| Ship commissioned = 4 February 1902
| Ship decommissioned = 19 November 1915
| Ship struck = 6 December 1919
| Ship fate = Scrapped in 1920
}}
{{Infobox ship characteristics
| Hide header = 
| Header caption = 
| Ship class = {{sclass-|Kaiser Friedrich III|battleship|0}} [[pre-dreadnought battleship]]
| Ship displacement =Full load: {{convert|11785|t|LT|abbr=on}}
| Ship length = {{convert|125.3|m|ftin|abbr=on}}
| Ship beam = {{convert|20.4|m|ftin|abbr=on}}
| Ship draft = {{convert|7.89|m|ftin|abbr=on}}
| Ship propulsion = 3 shafts [[triple expansion engine]]s
| Ship power=
*12 [[water-tube boiler]]s
*{{convert|13000|PS|ihp kW|lk=on|abbr=on|-1}}
| Ship speed = {{convert|17.5|kn|lk=in}}
| Ship range = {{convert|3420|nmi|abbr=on|lk=in}} at {{convert|10|kn}}
| Ship complement =
*39 officers
*612 enlisted
| Ship armament =
*  4 × [[24 cm SK L/40|{{convert|24|cm|in|abbr=on}} 40&nbsp;]][[Caliber (artillery)|cal]] guns
* 18 × {{convert|15|cm|in|abbr=on}} guns
* 12 × {{convert|8.8|cm|in|abbr=on}} guns
* 12 × [[3.7 cm Maxim machine cannon|1-pdr guns]]
*  6 × {{convert|45|cm|in|abbr=on}} torpedo tubes
| Ship armor =* Belt:          {{convert|300|to|150|mm|in|abbr=on}}
* Deck:          {{convert|65|mm|in|abbr=on}}
* Conning Tower: {{convert|250|mm|in|abbr=on}}
* Turrets:       250&nbsp;mm
* Casemates:     150&nbsp;mm

| Ship notes = 
}}
|}

'''SMS ''Kaiser Karl der Grosse''''' (His Majesty's Ship "[[Charlemagne]]") was a German [[pre-dreadnought]] [[battleship]] of the {{sclass-|Kaiser Friedrich III|battleship|4}}, built around the turn of the 20th century for the ''[[Kaiserliche Marine]]'' (Imperial Navy).{{efn|name=sharp S}} ''Kaiser Karl der Grosse'' was built in [[Hamburg]], at the [[Blohm + Voss|Blohm and Voss]] shipyard. She was laid down in September 1898 and launched in October 1899. A shipyard [[Strike action|strike]] and an accidental grounding delayed her completion until February 1902; she was therefore the last member of her class to enter service. The ship was armed with four {{convert|24|cm|in|sp=us|adj=on}} guns in two twin [[gun turret]]s and had a top speed of {{convert|17.5|kn|lk=in}}.

''Kaiser Karl der Grosse'' served with the active fleet until 1908, participating in the normal peacetime routine of training cruises and fleet maneuvers. By 1908, the new "all-big-gun" [[dreadnought battleship]]s were entering service. As she was completely obsolete, ''Kaiser Karl der Grosse'' was withdrawn from active service and placed in the Reserve Division. At the outbreak of [[World War I]] in August 1914, the ship was placed back in active duty as a [[coastal defense ship]] in the [[V Battle Squadron]], though by February 1915 she was again placed in reserve. ''Kaiser Karl der Grosse'' was briefly used as a training ship and ended her career as a prison ship for [[prisoner of war|prisoners of war]] in [[Wilhelmshaven]]. Following the German defeat in November 1918, she was sold to [[ship breaking|ship-breakers]] and scrapped in 1920.

== Design ==
[[File:Kaiser Friedrich III linedrawing.png|thumb|left|Line-drawing of the ''Kaiser Friedrich III'' class]]
{{main|Kaiser Friedrich III-class battleship}}

''Kaiser Karl der Grosse'' was {{convert|125.3|m|ftin|abbr=on}} [[length overall|long overall]] and had a [[beam (nautical)|beam]] of {{convert|20.4|m|ftin|abbr=on}} and a [[Draft (hull)|draft]] of {{convert|7.89|m|ftin|abbr=on}} forward and {{convert|8.25|m|ftin|abbr=on}} aft. She displaced up to {{convert|11785|t|LT|abbr=on|lk=on}} at full load. The ship was powered by three 3-cylinder vertical [[triple-expansion engine|triple-expansion]] [[steam engine]]s, each driving one screw propeller. Steam was provided by four Marine-type and eight cylindrical boilers, all of which burned coal. ''Kaiser Karl der Grosse''{{'}}s powerplant was rated at {{convert|13000|PS|ihp kW|lk=on|0}}, which generated a top speed of {{convert|17.5|kn}}. She had a normal complement of 39 officers and 612 enlisted men.{{sfn|Gröner|p=15}}

The ship's armament consisted of a [[Artillery battery#Naval usage|main battery]] of four [[24 cm SK L/40|24&nbsp;cm (9.4&nbsp;in) SK L/40 guns]] in twin [[gun turret]]s,{{efn|name=gun nomenclature}} one fore and one aft of the central [[superstructure]].{{sfn|Hore|p=67}} Her secondary armament consisted of eighteen [[15 cm SK L/40 naval gun|15&nbsp;cm (5.9&nbsp;inch) SK L/40]] guns and twelve [[8.8 cm SK L/30 naval gun|8.8&nbsp;cm (3.45&nbsp;in) SK L/30]] quick-firing guns mounted in [[casemate]]s. The armament suite was rounded out with six 45&nbsp;cm [[torpedo tube]]s, all in above-water swivel mounts. The ship's [[belt armor]] was {{convert|300|mm|abbr=on|1}} thick, and the [[deck (ship)|deck]] was {{convert|65|mm|abbr=on}} thick. The [[conning tower]] and main battery turrets were protected with {{convert|250|mm|abbr=on}} of armor plating, and the secondary casemates received {{convert|150|mm|abbr=on}} of armor protection.{{sfn|Gröner|p=15}}

== Service history ==

=== Construction through 1904 ===
[[Kaiser Wilhelm II]], the emperor of Germany, believed that a strong navy was necessary for the country to expand its influence outside continental Europe. As a result, he initiated a program of naval expansion in the late 1880s; the first battleships built under this program were the four {{sclass-|Brandenburg|battleship|0}} ships. These were immediately followed by the five {{sclass-|Kaiser Friedrich III|battleship|2}}s, of which ''Kaiser Karl der Grosse'' was a member.{{sfn|Herwig|pp=24–26}} She was ordered under the contract name "B" as a new ship of the fleet. The ship's keel was laid on 17 September 1898 at the [[Blohm + Voss|Blohm & Voss]] in [[Hamburg]] under [[yard number]] 136.{{sfn|Gröner|p=15}}{{sfn|Hildebrand Röhr & Steinmetz|p=39}} She was the first [[capital ship]] to be built by the yard, and the second warship of any type.{{sfn|Hurd & Castle|p=366}} The new battleship was launched on 18 October 1899 and named for [[Charlemagne]] (Karl der Grosse in German); Wilhelm II gave the launching speech and [[Johann Georg Mönckeberg]], the ''[[Government of Hamburg#Executive|Erster Bürgermeister]]'' (First Mayor) of Hamburg, christened the ship. A major strike by shipyard workers in late 1900 significantly delayed completion of the ship. In October 1901, a shipyard crew took the ship to the naval base at [[Wilhelmshaven]], though while en route she ran aground in the lower [[Elbe]] river. The hull was damaged in the incident and the necessary repairs further delayed her entry into service; she was not commissioned until 4 February 1902.{{sfn|Hildebrand Röhr & Steinmetz|p=39}}

[[File:SMS Kaiser Karl der Grosse.png|thumb|left|''Kaiser Karl der Grosse'' underway, c. 1902]]

''Kaiser Karl der Grosse'' was assigned to the II Division of the I Squadron on 19 February, the last member of her class to enter active service. With the assignment of ''Kaiser Karl der Grosse'', the I Squadron was now fully composed of modern battleships. The squadron went on a training cruise to Britain in April and May, followed by a tour of the [[Kiel Week]] sailing [[regatta]] in late June. The ships then took part in another training cruise to Norway in July and then the autumn maneuvers, which began in the Baltic on 31 August.{{sfn|Hildebrand Röhr & Steinmetz|pp=39, 47}} During the exercises, ''Kaiser Karl der Grosse'' was assigned to the "[[Opfor|hostile]]" force, as were several of her sister ships. The "hostile" force was first tasked with preventing the "German" squadron from passing through the [[Great Belt]] in the Baltic. ''Kaiser Karl der Grosse'' and several other battleships were then tasked with forcing an entry into the mouth of the [[Elbe River]], where the [[Kaiser Wilhelm Canal]] and Hamburg could be seized. The "hostile" flotilla accomplished these tasks within three days.{{sfn|''German Naval Manoeuvres''|pp=91–96}} The maneuvers concluded in the North Sea with a fleet review in the [[Jade Bight]]. From 1 to 12 December, the squadron went on its normal winter cruise to Norway.{{sfn|Hildebrand Röhr & Steinmetz|pp=39, 48}}

In 1903, the fleet, which was composed of only one squadron of battleships (along with its attendant scouting vessels and [[torpedo boat]]s), was reorganized as the "Active Battle Fleet." ''Kaiser Karl der Grosse'' remained in the I Squadron along with her sister ships and the newest {{sclass-|Wittelsbach|battleship|2}}s, while the older ''Brandenburg''-class ships were placed in [[Reserve fleet|reserve]] in order to be rebuilt.{{sfn|Herwig|p=45}} The first quarter of 1903 followed the usual pattern of training exercises. The squadron went on a training cruise in the Baltic, followed by a voyage to Spain that lasted from 7 May to 10 June. In July, she joined the I Squadron for the annual cruise to Norway. The autumn maneuvers consisted of a blockade exercise in the North Sea, a cruise of the entire fleet first to Norwegian waters and then to [[Kiel]] in early September, and finally a mock attack on Kiel. The exercises concluded on 12 September. The winter training cruise began on 23 November in the eastern Baltic and continued into the [[Skagerrak]] in early December.{{sfn|Hildebrand Röhr & Steinmetz|pp=48–49}}

''Kaiser Karl der Grosse'' participated in an exercises in the Skagerrak from 11 to 21 January 1904, with her squadron from 8 to 17 March, and with the fleet in the North Sea in May. In July, the I Squadron and the [[I Scouting Group]] visited Britain, including a stop at [[Plymouth]] on 10 July. The German fleet departed on 13 July, bound for the Netherlands; the I Squadron anchored in [[Vlissingen]] the following day, where the ships were visited by Queen [[Wilhelmina of the Netherlands|Wilhelmina]]. Departing on 20 July for a cruise in the northern North Sea with the rest of the fleet, the squadron stopped in [[Molde]], Norway, nine days later while the other units went to other ports.{{sfn|Hildebrand Röhr & Steinmetz|pp=51–52}} The fleet reassembled on 6 August and steamed back to Kiel, where it conducted a mock attack on the harbor on 12 August. Immediately after returning to Kiel, the fleet began preparations for the autumn maneuvers, which began on 29 August in the Baltic. The fleet moved to the North Sea on 3 September, where it took part in a major [[amphibious warfare|landing operation]], after which the ships took the ground troops from the [[IX Corps (German Empire)|IX Corps]] that participated in the exercises to [[Altona, Hamburg|Altona]] for a parade for Wilhelm II. The ships then conducted their own parade for the Kaiser off the island of [[Helgoland]] on 6 September. Three days later, the fleet returned to the Baltic via the Kaiser Wilhelm Canal, where it participated in further landing operations with the IX Corps and the [[Guards Corps (German Empire)|Guards Corps]]. On 15 September, the maneuvers came to an end.{{sfn|Hildebrand Röhr & Steinmetz|p=52}} The I Squadron went on its winter training cruise, this time to the eastern Baltic, from 22 November to 2 December.{{sfn|Hildebrand Röhr & Steinmetz|p=54}}

=== 1905–14 ===
[[File:North and Baltic Seas, 1911.png|thumb|500px|Map of the North and Baltic Seas in 1911]]

In January and February 1905, ''Kaiser Karl der Grosse'' served briefly as the [[flagship]] of the squadron.{{sfn|Hildebrand Röhr & Steinmetz|p=39}} During this period, she took part in a pair of training cruises with the I Squadron during 9–19 January and 27 February&nbsp;– 16 March 1905. Individual and squadron training followed, with an emphasis on gunnery drills. On 12 July, the fleet began a major training exercise in the North Sea.{{sfn|Hildebrand Röhr & Steinmetz|pp=54–55}} While on the cruise on 18 July, ''Kaiser Karl der Grosse'' was detached to visit [[Antwerp]] to represent Germany during the celebrations for the 75th anniversary of the [[Belgian Revolution]].{{sfn|Hildebrand Röhr & Steinmetz|p=39}} The rest of the fleet then cruised through the Kattegat and stopped in [[Copenhagen]] and [[Stockholm]]; ''Kaiser Karl der Grosse'' rejoined them on 3 August in [[Karlskrona]]. The summer cruise ended on 9 August, though the autumn maneuvers that would normally have begun shortly thereafter were delayed by a visit from the British [[Channel Fleet]] that month.{{sfn|Hildebrand Röhr & Steinmetz|pp=54–55}}

The British fleet stopped in Danzig, [[Swinemünde]], and [[Flensburg]], where it was greeted by units of the German Navy; ''Kaiser Karl der Grosse'' and the main German fleet was anchored at Swinemünde for the occasion. The visit was strained by the growing [[Anglo-German naval arms race]].{{sfn|Hildebrand Röhr & Steinmetz|pp=54–55}}{{sfn|Gardiner & Gray|p=134}} As a result of the British visit, the 1905 autumn maneuvers (6 to 13 September) were shortened considerably, consisting only of exercises in the North Sea. The first exercise presumed a naval blockade in the German Bight, and the second envisioned a hostile fleet attempting to force the defenses of the Elbe.{{sfn|Hildebrand Röhr & Steinmetz|p=55}} In November, the I Squadron cruised in the Baltic. In early December, the I and II Squadrons went on their regular winter cruise, this time to Danzig, where they arrived on 12 December. While on the return trip to Kiel, the fleet conducted tactical exercises.{{sfn|Hildebrand Röhr & Steinmetz|p=58}}

The fleet undertook a heavier training schedule in 1906 than in previous years. The ships were occupied with individual, division and squadron exercises throughout April. Starting on 13 May, major fleet exercises took place in the North Sea and lasted until 8 June with a cruise around the [[Skagen]] into the Baltic.{{sfn|Hildebrand Röhr & Steinmetz|p=58}} The fleet began its usual summer cruise to Norway in mid-July. ''Kaiser Karl der Grosse'' and the I Squadron anchored in Molde, where they were joined on 21 July by Wilhelm II aboard the steamer [[USS Powhatan (ID-3013)|SS ''Hamburg'']]. The fleet was present for the birthday of Norwegian King [[Haakon VII of Norway|Haakon VII]] on 3 August. The German ships departed the following day for Helgoland to join exercises being conducted there, arriving back in Kiel by 15 August, where preparations for the autumn maneuvers began. On 22–24 August, the fleet took part in landing exercises in [[Eckernförde Bay]] outside Kiel. The maneuvers were paused from 31 August to 3 September when Kiel hosted vessels from Denmark and Sweden, along with a Russian squadron from 3 to 9 September.{{sfn|Hildebrand Röhr & Steinmetz|p=59}} The maneuvers resumed on 8 September and lasted five more days. The ship participated in the uneventful winter cruise into the Kattegat and Skagerrak from 8 to 16 December.{{sfn|Hildebrand Röhr & Steinmetz|p=60}}

The first quarter of 1907 followed the previous pattern and, on 16 February, the Active Battlefleet was re-designated the [[High Seas Fleet]]. From the end of May to early June the fleet went on its summer cruise in the North Sea, returning to the Baltic via the Kattegat. This was followed by the regular cruise to Norway from 12 July to 10 August. During the autumn maneuvers, which lasted from 26 August to 6 September, the fleet conducted landing exercises in northern [[Schleswig]] with the IX Corps. The winter training cruise went into the Kattegat from 22 to 30 November.{{sfn|Hildebrand Röhr & Steinmetz|pp=60–61}} In May 1908, the fleet went on a major cruise into the Atlantic instead of its normal voyage in the North Sea. The fleet returned to Kiel on 13 August to prepare for the autumn maneuvers, which lasted from 27 August to 7 September. Division exercises in the Baltic immediately followed from 7 to 13 September.{{sfn|Hildebrand Röhr & Steinmetz|p=62}} Following the conclusion of these maneuvers on 18 September, ''Kaiser Karl der Grosse'' was decommissioned in Kiel and assigned to the Reserve Division in the Baltic.{{sfn|Hildebrand Röhr & Steinmetz|p=39}} During this period, her sister ships were rebuilt, though ''Kaiser Karl der Grosse'' did not receive this treatment.{{sfn|Gröner|p=15}} By this time, the new [[dreadnought battleship|"all-big-gun" battleships]], which rendered pre-dreadnoughts like ''Kaiser Karl der Grosse'' obsolete, began to enter service.{{sfn|Gardiner & Gray|p=141}} In June and July 1911, the ship underwent a major [[:wikt:overhaul|overhaul]].{{sfn|Hildebrand Röhr & Steinmetz|p=39}} By 1914, the ship had been assigned to the V Squadron of the Reserve Fleet, alongside her four sister ships and the battleship {{SMS|Wettin||2}}.{{sfn|''European War Notes: Organization of the German Fleet''|p=1564}}

=== World War I ===
As a result of the outbreak of World War I, ''Kaiser Karl der Grosse'' and her sisters were brought out of reserve and mobilized as the V Battle Squadron on 5 August 1914. The ships were prepared for war very slowly, and they were not ready for service in the North Sea until the end of August.{{sfn|Hildebrand Röhr & Steinmetz|p=62}} They were initially tasked with coastal defense, though they served in this capacity for a short time.{{sfn|Gardiner & Gray|p=141}} In mid-September, the V Squadron was transferred to the Baltic, under the command of [[Prince Heinrich]]. He initially planned to launch a major amphibious assault on [[Windau]], but a shortage of transports forced a revision of the plan. Instead, the V Squadron was to carry the landing force, but this too was cancelled after Heinrich received false reports of British warships having entered the Baltic on 25 September.{{sfn|Hildebrand Röhr & Steinmetz|pp=62–63}} ''Kaiser Karl der Grosse'' and her sisters returned to Kiel the following day, disembarked the landing force, and proceeded to the North Sea, where they resumed guard ship duties. Before the end of the year, the V Squadron was once again transferred to the Baltic.{{sfn|Hildebrand Röhr & Steinmetz|p=63}}

Prince Heinrich ordered a foray toward [[Gotland]]. On 26 December 1914, the battleships rendezvoused with the Baltic cruiser division in the [[Bay of Pomerania]] and then departed on the sortie. Two days later, the fleet arrived off Gotland to [[:wikt:show the flag|show the German flag]], and was back in Kiel by 30 December.{{sfn|Hildebrand Röhr & Steinmetz|p=63}} ''Kaiser Karl der Grosse'' briefly replaced her sister {{SMS|Kaiser Wilhelm II||2}} as the squadron flagship, from 23 January 1915 to 23 February.{{sfn|Hildebrand Röhr & Steinmetz|p=39}} The squadron returned to the North Sea for guard duties, but was withdrawn from front-line service by February. Shortages of trained crews in the High Seas Fleet, coupled with the risk of operating older ships in wartime, necessitated the deactivation of ''Kaiser der Grosse'' and her sisters.{{sfn|Hildebrand Röhr & Steinmetz|p=63}} Starting in October, she served briefly as a training ship for engine room personnel, though on 19 November she was decommissioned in Kiel and disarmed.{{sfn|Hildebrand Röhr & Steinmetz|p=39}} She was thereafter employed as a prison ship for prisoners of war in Wilhelmshaven.{{sfn|Gröner|p=16}} In November 1918, Germany capitulated and signed the [[First Armistice at Compiègne]], which ended hostilities so a peace treaty could be negotiated. According to [[s:Treaty of Versailles/Part V#Section II. Naval Clauses|Article 181]] of the [[Treaty of Versailles]], signed on 28 June 1919, Germany was permitted to retain only six battleships of the "{{sclass-|Deutschland|battleship|5}} or [[Braunschweig-class battleship|''Lothringen'']] types".{{refn|[[s:Treaty of Versailles/Part V#Section II. Naval Clauses|Treaty of Versailles]] Section II, Article 181.}} On 6 December 1919, the ship was struck from the naval list and sold to ship-breakers. The following year, ''Kaiser Karl der Grosse'' was broken up for scrap metal in [[Rönnebeck]].{{sfn|Gröner|p=16}}

== Notes ==

'''Footnotes'''

{{notes
| notes =

{{efn
| name = sharp S
| In German, the ship's name was rendered with an [[eszett]], as ''Kaiser Karl der Große''. Most English-language sources replace the eszett.
}}

{{efn
| name = gun nomenclature
| In Imperial German Navy gun nomenclature, "SK" (''Schnelladekanone'') denotes that the gun is [[quick-firing gun|quick firing]], while the L/40 denotes the length of the gun. In this case, the L/40 gun is 40 [[Caliber (artillery)|caliber]], meaning that the gun is 40 times as long as it is in diameter. See: {{harvnb|Grießmer|p=177}}.
}}

}}

'''Citations'''

{{reflist|20em}}

== References ==
* {{cite journal
  | title = European War Notes: "Organization of the German Fleet"
  | year = 1914
  | journal = Proceedings
  | volume = 40
  | pages = 1564–1565
  | publisher = United States Naval Institute
  | location = London
  | ref = {{sfnRef|''European War Notes: Organization of the German Fleet''}}
  }}
* {{cite book
  | editor1-last = Gardiner
  | editor1-first = Robert
  | editor2-last = Gray
  | editor2-first = Randal
  | year = 1985
  | title = Conway's All the World's Fighting Ships: 1906–1921
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-0-87021-907-8
  | oclc = 12119866
  | ref = {{sfnRef|Gardiner & Gray}}
  }}
* {{cite journal
  | title = German Naval Manoeuvres
  | year = 1903
  | journal = R.U.S.I. Journal
  | volume = 47
  | pages = 90–97
  | publisher = Royal United Services Institute for Defence Studies
  | location = London
  | ref = {{sfnRef|''German Naval Manoeuvres''}}
  }}
* {{cite book
  | last = Grießmer
  | first = Axel
  | year = 1999
  | language = German
  | title = Die Linienschiffe der Kaiserlichen Marine
  | publisher = Bernard & Graefe Verlag
  | location = Bonn
  | isbn = 978-3-7637-5985-9
  | oclc = 
  | ref = {{sfnRef|Grießmer}}
  }}
* {{cite book
  | last = Gröner
  | first = Erich
  | year = 1990
  | title = German Warships: 1815–1945
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-0-87021-790-6
  | oclc = 22101769
  | ref = {{sfnRef|Gröner}}
  }}
* {{cite book
  | last = Herwig
  | first = Holger
  | year = 1998
  | origyear = 1980
  | title = "Luxury" Fleet: The Imperial German Navy 1888–1918
  | publisher = Humanity Books
  | location = [[Amherst, New York]]
  | isbn = 978-1-57392-286-9
  | oclc = 57239454
  | ref = {{sfnRef|Herwig}}
  }}
* {{cite book
  | last1 = Hildebrand
  | first1 = Hans H.
  | last2 = Röhr
  | first2 = Albert
  | last3 = Steinmetz
  | first3 = Hans-Otto
  | year = 1993
  | title = Die Deutschen Kriegsschiffe (Volume 5)
  | publisher = Mundus Verlag
  | location = Ratingen, DE
  |asin=B003VHSRKE |asin-tld=de
  | oclc = 
  | ref = {{sfnRef|Hildebrand Röhr & Steinmetz}}
  }}
* {{cite book
  | last = Hore
  | first = Peter
  | year = 2006
  | title = The Ironclads
  | publisher = Southwater Publishing
  | location = London
  | isbn = 978-1-84476-299-6
  | oclc = 70402701
  | ref = {{sfnRef|Hore}}
  }}
* {{cite book
  |last1=Hurd
  |first1=Archibald
  |last2=Castle
  |first2=Henry
  |title=German Sea Power: Its Rise, Progress, and Economic Basis
  |publisher=Charles Scribner's Sons
  |year=1913
  |location=New York
  |ref={{sfnRef|Hurd & Castle}}
  |oclc=607759576
  }}

{{Kaiser Friedrich class battleship}}
{{Subject bar
| portal1=Battleships
| portal2=Military of Germany
| portal3=World War I
}}


{{DEFAULTSORT:Kaiser Karl der Grosse}}
[[Category:Kaiser Friedrich III-class battleships|Karl der Grosse]]<!-- different sortkey because all ships of class begin with "Kaiser..." -->
[[Category:Ships built in Hamburg]]
[[Category:1899 ships]]
[[Category:World War I battleships of Germany]]