'''Tamaz Nadareishvili''' ({{lang-ka|თამაზ ნადარეიშვილი}}) (19 July 1954 – 31 August 2004) was a [[Georgia (country)|Georgian]] politician who served as head of the Council of Ministers of [[Abkhazia]], a [[Government of Abkhazia-in-exile|government-in-exile]] for the breakaway province.

Nadareishvili was a great grandson of the famous Abkhaz prince [[Shervashidze]]. Born and raised in Sukhumi, Nadareishvili attended [[Sukhumi University]], graduated in the early 1980s, and settled down to life as an academic writer. 

During the break-up of the Soviet Union, Nadareishvili became involved in [[Georgian National Liberation]] movement.  After the war in Abkhazia, Georgia, Nadareishvili was elected by fellow Georgian refugees as the head of an [[De jure Government of Abkhazia|exile government]].  In the 1990s he served at times in the Georgian parliament, continuing to draw support from refugees, to whom he helped distribute government aid.  

As head of the Council, Nadareishvili loudly supported military action to retake Abkhazia. Allegations were made claiming him being involved in paramilitary operations on the Abkhaz/Georgia administrative border.  The [[Forest Brothers (Georgia)|Forest Brothers]] under [[Dato Shengelia]] and the [[White Legion]] under [[Zurab Samushia]] were both allegedly associated with the Supreme Council. {{Citation needed|date=May 2007}}

Nadareishvili remained neutral during the [[Rose Revolution]] that ousted [[Eduard Shevardnadze]] in favor of [[Mikhail Saakashvili]]. However, Nadareishvili soon faced a revolt of his own, and resigned in January 2004 after a [[vote of no confidence]] by the Supreme Council. In 2005 he published a book ("Conspiracy Against Georgia"), which mostly includes information about [[Ethnic cleansing of Georgians in Abkhazia|ethnic cleansing and genocide]] of Georgian population in Abkhazia.  

Nadareishvili died of a [[myocardial infarction|heart attack]] on August 31, 2004.

== External links ==
*[http://www.abkhazia.gov.ge/ Government of Abkhazia (-in-exile)]
*[http://www.abkhazeti.info Abkhazia in-exile]
*[http://www.abkhazeti.ru abkhazeti.ru]
*[http://www.abkhazia.com abkhazia.com]
*[http://www.apsny.ge apsny.ge]
*[http://www.parliament.ge/GENERAL/HotPoints/ABKHAZIA/nadarei.html Georgian Parliament] speech by Nadareishvili.
*[http://www.newsgeorgia.org/nadareishvili/intro.shtml Genocide in Georgia] written by Nadareishvili.
{{Prime Ministers of Abkhazia}}

{{DEFAULTSORT:Nadareishvili, Tamaz}}
[[Category:1954 births]]
[[Category:2004 deaths]]
[[Category:People from Sukhumi]]
[[Category:Georgian–Abkhazian conflict]]
[[Category:Prime Ministers of Abkhazia]]