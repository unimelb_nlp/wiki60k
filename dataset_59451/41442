{{pp|small=yes}} 
{{semiprotected|small=yes}}
{{Infobox civil conflict
| title = Little Rock Crisis
| partof = the [[Civil Rights Movement]]
| image = [[File:101st Airborne at Little Rock Central High.jpg|333px]]
| place = [[Little Rock Central High School]] in [[Little Rock, Arkansas]]
| date = 
| caption = The "Little Rock Nine" are escorted inside [[Little Rock Central High School]] by troops of the [[101st Airborne Division]] of the [[United States Army]].
| map_type = 
| map_caption = 
| map_size = 
| coordinates = 
| latitude = 
| longitude = 
| coord_parameters = 
| causes = 
*[[Racial segregation]] in [[School integration in the United States|public schools]]
*''[[Brown v. Board of Education]]'' (1954)
| status  = 
| result  = 
* ''[[Cooper v. Aaron]]'' (1958)
| concessions =
| side1= 
*[[National Association for the Advancement of Colored People|National Association for the Advancement of Colored People (NAACP)]]
*Little Rock Nine
| side2= 
*[[List of Governors of Arkansas|Governor of Arkansas]]
| leadfigures1= 
'''NAACP member'''
*[[Daisy Bates (civil rights activist)|Daisy Bates]]
'''Little Rock Nine'''
*[[Melba Pattillo Beals]]
*[[Minnijean Brown-Trickey|Minnijean Brown]]
*[[Elizabeth Eckford]]
*[[Ernest Green]]
*[[Gloria Ray Karlmark]]
*[[Carlotta Walls LaNier]]
*[[Thelma Mothershed-Wair|Thelma Mothershed]]
*[[Terrence Roberts]]
*[[Jefferson Thomas]]
| leadfigures2= 
'''State of Arkansas'''
* [[Orval Faubus]], governor
}}
{{Eisenhower series}}

The '''Little Rock Nine''' was a group of nine African American students enrolled in [[Little Rock Central High School]] in 1957. Their enrollment was followed by the '''Little Rock Crisis''', in which the students were initially prevented from entering the [[racial segregation|racially segregated]] school by [[Orval Faubus]], the [[Governor of Arkansas]]. They then attended after the intervention of [[President of the United States|President]] [[Dwight D. Eisenhower]].

[[Supreme Court of the United States|The U.S. Supreme Court]] issued its historic ''[[Brown v. Board of Education]]'' of [[Topeka, Kansas|Topeka]], [[Kansas]], 347 U.S. 483, on May 17, 1954. Tied to the [[Fourteenth Amendment to the United States Constitution|14th Amendment]], the decision declared all laws establishing segregated schools to be [[constitutionality|unconstitutional]], and it called for the [[desegregation]] of all schools throughout the nation.<ref name="BrownVBoardOfEducation">{{cite court|authorlink=Earl Warren|last=Warren|first=Earl, Chief Justice of the United States Supreme Court|publisher=Cornell|url=http://www.law.cornell.edu/supct/html/historics/USSC_CR_0347_0483_ZO.html|litigants=Brown v. Topeka Board of Education|date=1954|court=U.S.}}.</ref> After the decision, the [[National Association for the Advancement of Colored People]] (NAACP) attempted to register black students in previously all-white schools in cities throughout the [[Southern United States|South]]. In [[Little Rock]], the capital city of Arkansas, the [[Little Rock School District|Little Rock School Board]] agreed to comply with the high court's ruling. [[Virgil Blossom]], the Superintendent of Schools, submitted a plan of gradual integration to the school board on May 24, 1955, which the board unanimously approved. The plan would be implemented during the fall of the 1957 school year, which would begin in September 1957.

By 1957, the NAACP had registered nine black students to attend the previously all-white Little Rock Central High, selected on the criteria of excellent grades and attendance.<ref name="LittleRockCentralHigh">{{cite web|first=Craig|last=Rains|url=http://www.centralhigh57.org/1957-58.htm|title=Little Rock Central High 40th Anniversary}}.</ref> Called the "Little Rock Nine", they were [[Ernest Green]] (b. 1941), [[Elizabeth Eckford]] (b. 1941), [[Jefferson Thomas]] (1942–2010), [[Terrence Roberts]] (b. 1941), [[Carlotta Walls LaNier]] (b. 1942), [[Minnijean Brown-Trickey|Minnijean Brown]] (b. 1941), [[Gloria Ray Karlmark]] (b. 1942), [[Thelma Mothershed-Wair|Thelma Mothershed]] (b. 1940), and [[Melba Pattillo Beals]] (b. 1941). Ernest Green was the first African American to graduate from Central High School.

==The Blossom Plan==
One of the plans created during attempts to desegregate the schools of Little Rock was by school superintendent [[Virgil Blossom]]. The initial approach proposed substantial integration beginning quickly and extending to all grades within a matter of many years.<ref>Tony A. Freyer, "Politics and Law in the Little Rock Crisis, 1954–1957," ''The Arkansas Historical Quarterly'', 60/2, (Summer 2007): 148</ref> This original proposal was scrapped and replaced with one that more closely met a set of minimum standards worked out in attorney Richard B. McCulloch's brief.<ref name="Freyer 149">Tony A. Freyer, "Politics and Law in the Little Rock Crisis, 1954–1957," The Arkansas Historical Quarterly, 60/2, (Summer 2007): 149</ref> This finalized plan would start in September 1957 and would integrate one high school, Little Rock Central. The second phase of the plan would take place in 1960 and would open up a few junior high schools to a few black children. The final stage would involve limited desegregation of the city's grade schools at an unspecified time, possibly as late as 1963.<ref name="Freyer 149" />

This plan was met with varied reactions from the [[NAACP]] branch of Little Rock. Militant members like the Bateses opposed the plan on the grounds that it was "vague, indefinite, slow-moving and indicative of an intent to stall further on public integration."<ref name="Kirk">John A. Kirk, "The Little Rock Crisis and Postwar Black Activism in Arkansas," ''The Arkansas Historical Quarterly'', 60/2, (Summer 2007): 239</ref> Despite this view, the majority accepted the plan; most felt that Blossom and the school board should have the chance to prove themselves, that the plan was reasonable, and that the white community would accept it.

This view was short lived, however. Changes were made to the plan, the most detrimental being a new transfer system that would allow students to move out of the attendance zone to which they were assigned.<ref name="Kirk" /> The unaltered Blossom Plan had [[Gerrymandering|gerrymandered]] school districts to guarantee a black majority at Horace Mann High and a white majority at Hall High.<ref name="Kirk" /> This meant that, even though black students lived closer to Central, they would be placed in Horace Mann thus confirming the intention of the school board to limit the impact of desegregation.<ref name="Kirk" /> The altered plan gave white students the choice of not attending Horace Mann, but didn't give black students the option of attending Hall. This new Blossom Plan did not sit well with the NAACP and after failed negotiations with the school board; the NAACP filed a lawsuit on February 8, 1956.

This lawsuit, along with a number of other factors contributed to the Little Rock School Crisis of 1957.

==National Guard blockade==
{{Main|Arkansas National Guard and the integration of Central High School}}
Several segregationist councils threatened to hold protests at Central High and physically block the black students from entering the school. Governor [[Orval Faubus]] deployed the [[Arkansas National Guard]] to support the segregationists on September 4, 1957. The sight of a line of soldiers blocking out the students made national headlines and polarized the nation. Regarding the accompanying crowd, one of the nine students, [[Elizabeth Eckford]], recalled:

{{quote|They moved closer and closer. ... Somebody started yelling. ... I tried to see a friendly face somewhere in the crowd—someone who maybe could help. I looked into the face of an old woman and it seemed a kind face, but when I looked at her again, she spat on me.<ref>{{cite news|url=http://connection.ebscohost.com/c/articles/27019977/little-rock-nine-paved-way|title=Little Rock Nine paved the way|accessdate=March 4, 2009|last=Boyd|first =Herb|date=September 27, 2007|volume=98|issue=40|newspaper=[[New York Amsterdam News]]}}</ref>}}

On September 9, the Little Rock School District issued a statement condemning the governor's deployment of soldiers to the school, and called for a citywide prayer service on September 12. Even President [[Dwight D. Eisenhower|Dwight Eisenhower]] attempted to de-escalate the situation by summoning Faubus for a meeting, warning him not to defy the Supreme Court's ruling.<ref name="Newport">{{cite journal|url=http://www.time.com/time/magazine/article/0,9171,893684,00.html|title=Retreat from Newport|journal=[[Time (magazine)|Time]]|date=September 23, 1957}}.</ref>

==Armed escort==
[[Woodrow Wilson Mann]], the mayor of Little Rock, asked President Eisenhower to send federal troops to enforce integration and protect the nine students. On September 24, the President ordered the [[101st Airborne Division]] of the [[United States Army]]—without its black soldiers, who rejoined the division a month later—to Little Rock and [[United States National Guard#Constitutional basis|federalized]] the entire 10,000-member Arkansas National Guard, taking it out of the hands of Faubus.<ref name="smith2012">{{cite book | title=Eisenhower in War and Peace | publisher=Random House | author=Smith, Jean Edward | year=2012 | isbn=978-0-679-64429-3 | pages=723}}</ref>

==A tense year==
By the end of September 1957, the nine were admitted to Little Rock Central High under the protection of the [[101st Airborne Division]] (and later the [[Arkansas National Guard]]), but they were still subjected to a year of [[Physical abuse|physical]] and [[verbal abuse]] (being spat on and called names) by many of the white students. [[Melba Pattillo Beals|Melba Pattillo]] had acid thrown into her eyes<ref>{{cite web|url=http://www.teachersdomain.org/resources/iml04/soc/ush/civil/beals/index.html|title=Melba Pattillo Beals|accessdate=February 2, 2008|work=Teachers' Domain|publisher=WGBH Educational Foundation}}</ref> and also recalled in her book, ''Warriors Don't Cry'', an incident in which a group of white girls trapped her in a stall in the girls' washroom and attempted to burn her by dropping pieces of flaming paper on her from above. Another one of the students, [[Minnijean Brown-Trickey|Minnijean Brown]], was verbally confronted and abused. She said <blockquote>I was one of the kids 'approved' by the school officials. We were told we would have to take a lot and were warned not to fight back if anything happened. One girl ran up to me and said, 'I'm so glad you're here. Won't you go to lunch with me today?' I never saw her again.<ref>{{Cite news|last=Brown|first=Minnijean|author-link=Minnijean Brown-Trickey|last2=Moskin|first2=J. Robert|title=One Girl's Little Rock Story|magazine=[[Look (American magazine)|Look]]|date=June 24, 1958}}</ref></blockquote>

Minnijean Brown was also taunted by members of a group of white male students in December 1957 in the school cafeteria during lunch. She dropped her lunch, a bowl of chili, onto the boys and was suspended for six days. Two months later, after more confrontation, Brown was suspended for the rest of the school year. She transferred to New Lincoln High School in [[New York City]].<ref name="LittleRockCentralHigh"/> As depicted in the 1981 made-for-TV docudrama ''[[Crisis at Central High]]'', and as mentioned by Melba Pattillo Beals in ''Warriors Don't Cry'', white students were punished only when their offense was "both egregious and witnessed by an adult".<ref name=CaCH>{{cite news|last=Collins|first=Janelle|title=Easing a Country's Conscience: Little Rock's Central High School in Film|url=http://findarticles.com/p/articles/mi_qa4074/is_200810/ai_n31110541/|date=Fall 2008|work=The Southern Quarterly|publisher=The [[University of Southern Mississippi]]|accessdate=August 2, 2009}} {{Dead link|date=October 2010|bot=H3llBot}}</ref> The drama was based on a book by [[Elizabeth Huckaby]], a vice-principal during the crisis.

=="The Lost Year"==
In the summer of 1958, as the school year was drawing to a close, Faubus decided to petition the decision by the Federal District Court in order to postpone the desegregation of public high schools in Little Rock.<ref>Bates, Daisy. ''The Long Shadow of Little Rock: A Memoir''. New York: David McKay, 1962, p. 151.</ref> In the ''[[Cooper v. Aaron]]'' case, the Little Rock School District, under the leadership of [[Orval Faubus]], fought for a two and a half year delay on de-segregation, which would have meant that black students would only be permitted into public high schools in January 1961.<ref>Gordy, Sondra. "Empty Hearts: Little Rock Secondary Teachers, 1958–1959". ''The Arkansas Historical Quarterly'', 1997, p. 428.</ref> Faubus argued that if the schools remained integrated there would be an increase in violence. However, in August 1958, the Federal Courts ruled against the delay of de-segregation, which incited Faubus to call together an Extraordinary Session of the State Legislature on August 26 in order to enact his segregation bills.<ref>Bates, Daisy. ''The Long Shadow of Little Rock: A Memoir''. New York: David McKay, 1962, p. 152.</ref>

Claiming that Little Rock had to assert their rights and freedom against the federal decision, in September 1958, Faubus signed acts that enabled him and the Little Rock School District to close all public schools.<ref>Bates, Daisy. ''The Long Shadow of Little Rock: A Memoir''. New York: David McKay, 1962, p. 154.</ref> Thus, with this bill signed, on Monday September 15, Faubus ordered the closure of all four public high schools, preventing both black and white students from attending school.<ref name="GordySondra">Gordy, Sondra. "Empty Hearts: Little Rock Secondary Teachers, 1958–1959". ''The Arkansas Historical Quarterly'', 1997, p. 429.</ref> Despite Faubus's decree, the town's population had the chance of refuting the bill since the school-closing law necessitated a [[referendum]]. The referendum, which would either condone or condemn Faubus's law, was to take place within thirty days.<ref name="GordySondra" /> A week before the referendum, which was scheduled to take place on September 27, Faubus addressed the citizens of Little Rock in an attempt to secure their votes. Faubus urged the population to vote against integration since he was planning on leasing the public school buildings to private schools, and, in doing so, would educate the white and black students separately.<ref>Gordy, Sondra. "Empty Hearts: Little Rock Secondary Teachers, 1958–1959". ''The Arkansas Historical Quarterly'', 1997, p. 431.</ref> Faubus was successful in his appeal and won the referendum. This year came to be known as the "Lost Year."

Faubus's victory led to a series of consequences that affected Little Rock society. Faubus's intention to open private schools was denied{{clarify|date=August 2014}} the same day the referendum took place, which caused some citizens of Little Rock to turn on the black community. The black community became a target for hate crimes since people blamed them for the closing of the schools.<ref>Bates, Daisy. ''The Long Shadow of Little Rock: A Memoir''. New York: David McKay, 1962, p. 155.</ref> [[Daisy Bates (civil rights activist)|Daisy Bates]], head of the NAACP chapter in Little Rock, was a primary victim to these crimes, in addition to the black students enrolled at Little Rock Central High School and their families.<ref>Bates, Daisy. ''The Long Shadow of Little Rock: A Memoir''. New York: David McKay, 1962. p. 159.</ref>

The town's teachers were also placed in a difficult position. They were forced to swear loyalty to Faubus's bills.<ref name= "GordySondra" /> Even though Faubus's idea of private schools never played out, the teachers were still expected to attend school every day and prepare for the possibility of their students' return.<ref>Gordy, Sondra. "Empty Hearts: Little Rock Secondary Teachers, 1958–1959". ''The Arkansas Historical Quarterly'', 1997, p. 436.</ref> The teachers were completely under Faubus's control and the many months that the school stayed empty only served as a cause for uncertainty in their professional futures.<ref>Gordy, Sondra. "Empty Hearts: Little Rock Secondary Teachers, 1958–1959". ''The Arkansas Historical Quarterly'', 1997, p. 441.</ref>

In May 1959, after the firing of forty-four teachers and administrative staff from the four high schools, three segregationist board members were replaced with three moderate ones. The new board members reinstated the forty-four staff members to their positions.<ref name="GordySondra_a">Gordy, Sondra. "Empty Hearts: Little Rock Secondary Teachers, 1958–1959". ''The Arkansas Historical Quarterly'', 1997, p. 442.</ref> The new board of directors then began an attempt to reopen the schools, much to Faubus's dismay. In order to avoid any further complications, the public high schools were scheduled to open earlier than usual, on August 12, 1959.<ref name="GordySondra_a" />

Although the Lost Year had come to a close, the black students who returned to the high schools were not welcomed by the other students. Rather, the black students had a difficult time getting past mobs to enter the school, and, once inside, they were often subject to physical and emotional abuse.<ref>Bates, Daisy. ''The Long Shadow of Little Rock: A Memoir''. New York: David McKay, 1962, p. 165.</ref> The students were back at school and everything would eventually resume normal function, but the Lost Year would be a pretext for new hatred towards the black students in the public high school.

==Motivations==
{{refimprove section|date=August 2011}}
Faubus's opposition to desegregation was likely both politically and racially motivated.<ref>{{cite web|title=Little Rock Nine|url=http://originalpeople.org/little-rock-nine/|website=Originalpeople|accessdate=6 May 2015}}</ref> Although Faubus had indicated that he would consider bringing Arkansas into compliance with the high court's decision in 1956, desegregation was opposed by his own [[Dixiecrats|southern Democratic Party]], which dominated all Southern politics at the time. Faubus risked losing political support in the upcoming 1958 Democratic gubernatorial primary if he showed support for integration.<ref>{{cite news|title=Showdown over Segregation|url=http://www.washingtonpost.com/wp-dyn/content/article/2007/03/16/AR2007031600131.html|website=The Washington Post|accessdate=6 May 2015|first=Juan|last=Williams|date=March 18, 2007}}</ref>

Most histories of the crisis conclude that Faubus, facing pressure as he campaigned for a third term, decided to appease racist elements in the state by calling out the National Guard to prevent the black students from entering Central High. Former associate justice of the [[Arkansas Supreme Court]] [[James D. Johnson]] claimed to have hoaxed Governor Faubus into calling out the National Guard, supposedly to prevent a white mob from stopping the integration of [[Little Rock Central High School]]: "There wasn't any caravan. But we made Orval believe it. We said. 'They're lining up. They're coming in droves.' ... The only weapon we had was to leave the impression that the sky was going to fall." He later claimed that Faubus asked him to raise a mob to justify his actions.<ref name=notgone>{{cite news|url=http://www.salon.com/2010/02/18/justice_4/ |title=Racist "Justice" is dead, but not gone |publisher=Salon |date=February 18, 2010 |accessdate=October 5, 2014}}</ref>

[[Harry Ashmore]], the editor of the ''Arkansas Gazette'', won a 1958 Pulitzer Prize for his editorials on the crisis. Ashmore portrayed the fight over Central High as a crisis manufactured by Faubus; in his interpretation, Faubus used the Arkansas National Guard to keep black children out of Central High School because he was frustrated by the success his political opponents were having in using segregationist rhetoric to stir white voters.<ref>{{cite web|title=The Pulitzer Prize Winners 1958|url=http://www.pulitzer.org/awards/1958|publisher=the Pulitzer Board|accessdate=7 September 2011}}</ref>

Congressman [[Brooks Hays]], who tried to mediate between the federal government and Faubus, was later defeated by a last minute write-in candidate, [[Dale Alford]], a member of the Little Rock School Board who had the backing of Faubus's allies.<ref>{{cite book|title=Profiles in Hue|publisher=Xlibris Corporation|isbn=1456851209|pages=366|url=https://books.google.com/books?id=OANpT_Bl1c8C&pg=PA366&lpg=PA366&dq=A+few+years+later,+despite+the+incident+with+the+%22Little+Rock+Nine%22,+Faubus+ran+as+a+moderate+segregationist+against+Dale+Alford,+who+was+challenging+Faubus+for+the+Democratic+nomination+for+governor+in+1962.&source=bl&ots=vuX-5Fktni&sig=sD8cR5K19MNx9DANjZ0aV1U-wjk&hl=en&sa=X&ei=n3VKVenWHY7ggwSqxoGQDQ&ved=0CCUQ6AEwAQ#v=onepage&q=A%20few%20years%20later%2C%20despite%20the%20incident%20with%20the%20%22Little%20Rock%20Nine%22%2C%20Faubus%20ran%20as%20a%20moderate%20segregationist%20against%20Dale%20Alford%2C%20who%20was%20challenging%20Faubus%20for%20the%20Democratic%20nomination%20for%20governor%20in%201962.&f=false|accessdate=6 May 2015}}</ref> A few years later, despite the incident with the "Little Rock Nine", Faubus ran as a moderate segregationist against Dale Alford, who was challenging Faubus for the Democratic nomination for governor in 1962.

==Legacy==
[[Image:LittleRockUncObv.jpg|thumb|A commemorative silver dollar]]
[[File:ArkLRNine - 31409(90).JPG|thumb|right|Memorial at [[Arkansas State Capitol]]]]
[[File:Ernest Green, Carlotta Walls LaNier, Terrence Roberts (DIG13683-010).jpg|thumb|Three members of the “Little Rock Nine” (L-R) Ernest Green, Carlotta Walls LaNier, and Terrence Roberts - stand together on the steps of the LBJ Presidential Library in 2014]]
Little Rock Central High School still functions as part of the Little Rock School District, and is now a National Historic Site that houses a [[Civil Rights]] Museum, administered in partnership with the [[National Park Service]], to commemorate the events of 1957.<ref>United States National Park Service, [http://www.nps.gov/chsc/ Little Rock Central High School, National Historic Site.]</ref>  The [[Daisy Bates House]], home to [[Daisy Bates (civil rights activist)|Daisy Bates]], then the president of the Arkansas NAACP and a focal point for the students, was designated a [[National Historic Landmark]] in 2001 for its role in the episode.<ref>{{cite web|url={{NHLS url|id=01000072}}|title=NHL nomination for Daisy Bates House|publisher=National Park Service|accessdate=2014-10-27}}</ref>

In 1958, Cuban poet [[Nicolás Guillén]] published "Little Rock", a bilingual composition in English and Spanish denouncing the [[racial segregation in the United States]].<ref name="GuillénMárquez2003">{{cite book|last1=Guillén|first1=Nicolás|authorlink1=Nicolás Guillén|last2=Márquez|first2=Robert|last3=McMurray|first3=David Arthur|title=Man-making words: selected poems of Nicolás Guillén|url=https://books.google.com/books?id=xV2zdIzadWAC&pg=PA58|accessdate=7 September 2011|date=August 2003|publisher=Univ of Massachusetts Press|isbn=978-1-55849-410-7|pages=58–61}}</ref>

Melba Pattillo Beals wrote a memoir titled ''Warriors Don't Cry'', published in the mid-1990s.

Two [[made-for-television]] movies have depicted the events of the crisis: the 1981 [[CBS]] movie ''[[Crisis at Central High]]'', and the 1993 [[Disney Channel]] movie ''[[The Ernest Green Story]]''.

In 1996, seven of the Little Rock Nine appeared on ''[[The Oprah Winfrey Show]]''. They came face to face with a few of the white students who had tormented them as well as one student who had befriended them.

President [[Bill Clinton]] honored the Little Rock Nine in November 1999 when he presented them each with a [[Congressional Gold Medal]]. The medal is the highest civilian award bestowed by [[United States Congress|Congress]].<ref>{{cite web|url=http://www.npr.org/templates/story/story.php?storyId=1066420|title=Little Rock Nine|date=November 9, 1999|accessdate=August 28, 2012}}</ref> It is given to those who have provided outstanding service to the country. To receive the Congressional Gold Medal, recipients must be co-sponsored by two-thirds of both the [[United States House of Representatives|House]] and [[United States Senate|Senate]].

In 2007, the [[United States Mint]] made available a commemorative [[Dollar (United States coin)|silver dollar]] to "recognize and pay tribute to the strength, the determination and the courage displayed by African-American high school students in the fall of 1957." The obverse depicts students accompanied by a soldier, with nine stars symbolizing the Little Rock Nine. The reverse depicts an image of Little Rock Central High School, c. 1957. Proceeds from the coin sales are to be used to improve the National Historic Site.

On December 9, 2008, the Little Rock Nine were invited to attend the inauguration of President-elect [[Barack Obama]], the first African-American to be elected President of the United States.<ref>"[http://www.washingtonpost.com/wp-dyn/content/article/2008/12/12/AR2008121204106.html We've Completed Our Mission]". ''Washington Post'', December 13, 2009, p. B01.</ref>

On February 9, 2010, [[Marquette University]] honored the group by presenting them with the Père Marquette Discovery Award, the university's highest honor, one that had previously been given to [[Mother Teresa]], [[Archbishop Desmond Tutu]], [[Karl Rahner]], and the [[Apollo 11]] astronauts, among other notables.

==See also==
* [[Black school]]
* [[Stand in the Schoolhouse Door]]
* [[Little Rock (poem)]]
* ''[[Nine from Little Rock]]'', an Academy Award winning documentary film about the Little Rock Nine
* ''[[Fables of Faubus]]'', a song written by [[jazz]] bassist [[Charles Mingus]]

==Footnotes==
{{reflist|2}}

==References==
* Anderson, Karen. ''Little Rock: Race and Resistance at Central High School'' (2013)
* Baer, Frances Lisa. ''Resistance to Public School Desegregation: Little Rock, Arkansas, and Beyond'' (2008) 328 pp.&nbsp;ISBN 978-1-59332-260-1
* Beals, Melba Pattillo. ''Warriors Don't Cry: A Searing Memoir of the Battle to Integrate Little Rock's Central High''. (ISBN 0-671-86638-9)
* Branton, Wiley A. "Little Rock Revisited: Desegregation to Resegregation." ''Journal of Negro Education'' 1983 52(3): 250–269. {{ISSN|0022-2984}} [http://www.jstor.org/stable/2294663 Fulltext in Jstor]
* Jacoway, Elizabeth. ''Turn Away Thy Son: Little Rock, the Crisis That Shocked the Nation'' (2007).
* Kirk, John A. "Not Quite Black and White: School Desegregation in Arkansas, 1954-1966," ''Arkansas Historical Quarterly'' (2011) 70#3 pp 225–257 [http://www.jstor.org/stable/23193404  in JSTOR]
* Kirk, John A., ed. ''An Epitaph for Little Rock: A Fiftieth Anniversary Retrospective on the Central High Crisis'' (University of Arkansas Press, 2008).
* Kirk, John A. ''Beyond Little Rock: The Origins and Legacies of the Central High Crisis'' (University of Arkansas Press, 2007).
* Kirk, John A., ''Redefining the Color Line: Black Activism in Little Rock, Arkansas, 1940–1970'' (University of Florida Press, 2002).
* Reed, Roy. ''Faubus: The Life and Times of an American Prodigal'' (1997).
* Lanier, Carlotta, A Mighty Long Way: My Journey to Justice at Little Rock Central High School, Random House, 2009

===Historiography===
* Pierce, Michael. "Historians of the Central High Crisis and Little Rock's Working-Class Whites: A Review Essay," ''Arkansas Historical Quarterly'' (2011) 70#4 pp.&nbsp;468–483 [http://www.jstor.org/stable/23188020 in JSTOR]

===Primary sources===
* Faubus, Orval Eugene. ''Down from the Hills.'' Little Rock: Democrat Printing & Lithographing, 1980. 510 pp. autobiography.

===External links===
* "[http://www.vanityfair.com/politics/features/2007/09/littlerock200709 Through a Lens, Darkly]," by David Margolick. ''[[Vanity Fair (magazine)|Vanity Fair]]'', September 24, 2007.
* [http://www.centralhigh57.org/the_tiger.htm The Tiger, Student Paper of Little Rock Central High.]
* [http://www.time.com/time/magazine/article/0,9171,1663841,00.html The Legacy of Little Rock] on Time.com (a division of Time Magazine)
* [http://www.army.mil/arkansas Guardians of Freedom—50th Anniversary of Operation Arkansas], by [[United States Army]]
* [http://www.eisenhower.archives.gov/research/online_documents/civil_rights_citizens_letters.html Letters from U.S. citizens regarding the Little Rock Crisis], [[Dwight D. Eisenhower Presidential Library]]
* [http://eisenhower.archives.gov/research/online_documents/civil_rights_little_rock.html Documents regarding the Little Rock Crisis, Dwight D. Eisenhower Presidential Library]
* [[United States National Park|National Park Service]]. [http://www.nps.gov/chsc/ Little Rock Central High School, National Historic Site.]
* ''[[Encyclopedia of Arkansas History & Culture]]'' entry: [http://www.encyclopediaofarkansas.net/encyclopedia/entry-detail.aspx?entryID=723 Little Rock Nine]
* [http://www.nps.gov/history/nr/twhp/wwwlps/lessons/crandall/crandall.htm ''"From Canterbury to Little Rock: The Struggle for Educational Equality for African Americans"''], a [[National Park Service]] [[Teaching with Historic Places]] (TwHP) lesson plan
* [http://www.booknotes.org/Watch/61780-1/Melba+Pattillo+Beals.aspx Interview] with [[Melba Pattillo Beals]] about her book ''Warriors Don't Cry: A Searing Memoir of the Battle to Integrate Little Rock's Central High'', ''[[Booknotes]]'', November 27, 1994.
* [http://scipio.uark.edu/cdm/compoundobject/collection/Civilrights/id/1683/rec/6 Letter by segregationist lawyer Amis Guthridge Defending Segregation] to Little Rock School Board and Superintendent Blossom, July 10, 1957.
* {{cite journal|last=McMillen|first=Neil R.| authorlink =Neil R. McMillen|date=Summer 1971|title=White Citizens' Council and Resistance to School Desegregation in Arkansas|journal=[[The Arkansas Historical Quarterly]]|publisher=[[Arkansas Historical Association]]|volume=30|issue=2|pages=95–122|url=http://dynamicsofsocialchange.files.wordpress.com/2012/02/resistence-to-school-desegregation-in-arkansas.pdf<!-- Stable URL: http://www.jstor.org/stable/40038072 -->}}
* {{cite web |url=http://thelostyear.com/ |author1=Sandra Hubbard |author2=Dr. Sondra Gordy |title=The Lost Year}} a documentary,  entitled "The Lost Year" by Sandra Hubbard and a book, entitled "Finding the Lost Year" By Dr. Gordy. An account by teachers and classmates of the closed high schools of Little Rock after the Crisis at Central High and the Little Rock Nine.

{{African-American Civil Rights Movement|state=uncollapsed}}

[[Category:Little Rock Nine| ]]
[[Category:History of racism in the United States]]
[[Category:Congressional Gold Medal recipients]]
[[Category:History of Little Rock, Arkansas]]
[[Category:Little Rock Central High School]]
[[Category:Presidency of Dwight D. Eisenhower]]
[[Category:School segregation in the United States]]