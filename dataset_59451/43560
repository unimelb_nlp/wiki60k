<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Ka 3
 | image=1954_Egelsbach_Ka-3_Prototyp.jpg
 | caption=Prototype Ka 3 in 1954
}}{{Infobox Aircraft Type
 | type=Single seat [[training aircraft|training]] [[glider (sailplane)|glider]]
 | national origin=[[Germany]]
 | manufacturer=[[Alexander Schleicher]] Segelflugzeugbau
 | designer=[[Rudolf Kaiser]]
 | first flight=1954
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=15
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Schleicher Ka 3''' or '''Kaiser Ka 3''', is a 1950s single seat [[training aircraft|training]] [[glider (sailplane)|glider]], mostly sold in kit form.

==Design and development==

The Ka 3 and its predecessor the Ka 1 were mostly sold as kits for home assembly.  Apart from their [[fuselage]]s the two types are very close in appearance, simplicity, weight and performance.  They share a round [[wing tip|tipped]] [[high wing]] with constant [[chord (aircraft)|chord]] and no sweep, mounted with 2.5° of [[dihedral (aircraft)|dihedral]] and braced with a single [[lift strut]] on each side from the lower fuselage to the wing at about one third span. Plain, constant-chord [[aileron]]s reach almost from the tips to about mid span and upper wing [[spoiler (aeronautics)|spoiler]]s are placed at mid chord, inboard of the ailerons.  Both have 37° [[V tail|butterfly tails]] with straight [[leading edge]]s and round tips and [[trailing edge]]s.<ref name=OSTIV1/>

The Ka 1 has a [[plywood|ply-covered]], rounded wooden-framed fuselage but that of the Ka 3 is more angular, steel tube-framed, [[aircraft fabric covering|fabric covered]] and slightly longer.  Both have a simple, deep, sprung landing skid reaching from the nose to under the trailing edge of the wing, assisted by a tail bumper.  Their [[cockpit]]s are under the wing leading edge, into which the clear [[aircraft canopy|canopy]] extends.<ref name=OSTIV1/>

The Ka 1 first flew in 1951 and ten were built;<ref name=DMFS/> the Ka 3 followed in 1954 with fifteen eventually completed.<ref name=OSTIV1/> One Ka 1 and two Ka 3s remained on the German civil aircraft register in 2010.<ref name=EuReg/>
<!-- ==Operational history== -->

==Variants==
[[File:Kaiser Ka 1 crop.jpg|thumb| Ka 1 in the [[Deutsches Museum Flugwerft Schleissheim]]]]
[[File:Kaiser Ka 1.jpg|thumb| Ka 1 in the [[Wasserkuppe]] museum]]
;Ka 1 Rhönlaus: Original version with rounded, wooden fuselage.<ref name=OSTIV1/> First flown 1951. 10 built.<ref name=DMFS/>
;Ka 3: Development with a slightly longer, fabric-covered steel-tube fuselage.  Performance and weights as Ka 1. First flown in 1954. 15 produced.<ref name=OSTIV1/>

==Specifications (Ka 3)==
{{Aircraft specs
|ref=The World's Sailplanes pp.83-5<ref name=OSTIV1/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=One
|length m=5.45
|length note=
|span m=10.00
|span note=
|height m=
|height ft=
|height in=
|height note=over tail
|wing area sqm=9.9
|wing area note=
|aspect ratio=10.1
|airfoil=Göttingen 549
|empty weight kg=100
|empty weight note=equipped
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=195
|max takeoff weight note=
|more general=
*'''Wing loading:''' 19.5 kg/m<sup>2</sup> (5.2 lb/sq ft) maximum
<!--
        Performance
-->
|perfhide=

|max speed kmh=160
|max speed note=placard, smooth air. All performance figures at a flying weight of {{convert|180|kg|lb|abbr=on|0}}
*'''Maximumun speed, placard, rough air:''' {{convert|100|km/h|mph   kn|abbr=on|0}}
*'''Maximumun aero-tow speed:''' {{convert|100|km/h|mph   kn|abbr=on|0}}
*'''Maximumun winch launch speed:''' {{convert|90|km/h|mph   kn|abbr=on|0}}
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=53
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=best, 17.5:1 at {{convert|75|km/h|mph kn|abbr=on|0}}
|sink rate ms=1.0
|sink rate note=at {{convert|65|km/h|mph kn|abbr=on|0}}
|lift to drag=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

<!--==Notes==-->

==References==
{{reflist|refs=

<ref name=OSTIV1>{{cite book |title= The World's Sailplanes - Die Segelflugzeuge der Welt  - Les Planeurs de le Monde|volume=I|first= K.G.|last=Wilkinson|first2=Peter|last2= Brooks|first3=B.S.|last3=Shenstone|year=1958|publisher=Organisation Scientifique et Technique International de Vol à Voile (OSTIV) & Schweizer Aero-Revue|language=English, German, French|isbn=|pages=81, 83-5146-9}}</ref>

<ref name=EuReg>{{cite book |title=European registers handbook 2010 |last= Partington |first=Dave |coauthors= |edition= |year=2010|publisher= Air Britain (Historians) Ltd|location= |isbn=978-0-85130-425-0|page=}}</ref>

<ref name=DMFS>{{cite web |url=http://www.deutsches-museum.de/en/flugwerft/collections/sailplanes/kaiser-ka-1|title=Kaiser Ka 1 |author= |date= |work= |publisher= |accessdate=17 November 2012}}</ref>

}}
<!-- ==Further reading== -->
<!-- ==External links== -->
{{Schleicher}}

[[Category:Homebuilt aircraft]]
[[Category:German sailplanes 1950–1959]]