The '''''International Journal of Modern Physics''''' is a series of Physics journals published by [[World Scientific]].

== A ==
{{Infobox journal
| title = International Journal of Modern Physics A
| cover = [[File:IJMPAcover.gif]]
| discipline = [[Physics]]
| abbreviation = Int. J. Mod. Phys. A
| editor = I. Antoniadis, A.P. Balachandran, L. Brink, M. Ninomiya, V.A. Rubakov, P. Sphicas, I. Tsutsui
| publisher = [[World Scientific]]
| country =
| impact = 1.699
| impact-year = 2014
| website = http://www.worldscientific.com/worldscinet/ijmpa
| ISSN = 0217-751X
| eISSN = 1793-656X
}}
The '''''International Journal of Modern Physics A''''' was established in 1986, and covers specifically particles and fields, [[gravitation]], [[cosmology]], and [[nuclear physics]].

The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Science Citation Index]]
* [[Current Contents]]/Physical, Chemical & Earth Sciences
* [[Astrophysics Data System]] (ADS) Abstract Service
* [[Mathematical Reviews]]
* [[Inspec]]
* [[Zentralblatt MATH]]
}}

== B ==
{{Infobox journal
| title = International Journal of Modern Physics B
| cover = 
| discipline = Physics
| abbreviation = Int. J. Mod. Phys. B
| impact = 0.455
| impact-year = 2013
| publisher = World Scientific
| history = 1987-present
| country =
| website = http://www.worldscientific.com/worldscinet/ijmpb
| ISSN = 0217-9792
| eISSN = 1793-6578
}}
The '''''International Journal of Modern Physics B''''' was established in 1987. It covers specifically developments in condensed matter, statistical and applied physics, and high Tc [[superconductivity]].

The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Science Citation Index]]
* Current Contents/Physical, Chemical & Earth Sciences
* [[Astrophysics Data System]] (ADS) Abstract Service
* [[Chemical Abstracts Service]]
* Mathematical Reviews
* [[Inspec]] 
* [[CSA Meteorological & Geoastrophysical Abstracts]]
* Zentralblatt MATH
* [[Scopus]]
}}

== C ==
{{Infobox journal
| title = International Journal of Modern Physics C
| cover = 
| discipline = Physics
| abbreviation = Int. J. Mod. Phys. C
| history = 1990
| impact = 1.260
| impact-year = 2014
| publisher = World Scientific
| country =
| website = http://www.worldscientific.com/worldscinet/ijmpc
| ISSN = 0217-9792
| eISSN = 1793-6578
}}
The '''''International Journal of Modern Physics C''''' was established in 1990. It covers specifically [[computational physics]], physical computation and related subjects, with topics such as [[astrophysics]], computational biophysics, [[materials science]], and statistical physics.

The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Scopus]]
* [[Science Citation Index]]
* [[CompuMath Citation Index]]
* Current Contents/Physical, Chemical & Earth Sciences
* [[Computer & Control Abstracts]]
* [[Electrical & Electronics Abstracts]]
* [[Physics Abstracts]]
* [[Energy Science and Technology Database]]
* Astrophysics Data System (ADS) Abstract Service
* Mathematical Reviews
* [[Inspec]]
* CSA Meteorological & Geoastrophysical Abstracts
* Zentralblatt MATH
}}

==  D ==
{{Infobox journal
| title = International Journal of Modern Physics D
| cover = 
| discipline = Physics
| abbreviation = Int. J. Mod. Phys. D
| publisher = World Scientific
| impact = 1.963
| impact-year = 2015
| country =
| website = http://www.worldscientific.com/worldscinet/ijmpd
| ISSN = 0218-2718
| eISSN = 1793-6594
}}
The '''''International Journal of Modern Physics D''''' was established in 1992. It covers specifically gravitation, astrophysics and cosmology, with topics such as general relativity, quantum gravity, cosmic particles and radiation.

The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Science Citation Index]]
* ISI Alerting Services
* Current Contents/Physical, Chemical & Earth Sciences
* Astrophysics Data System (ADS) Abstract Service
* Mathematical Reviews
* [[Inspec]]
* Zentralblatt MATH
}}

== E ==
{{Infobox journal
| title = International Journal of Modern Physics E
| cover = 
| discipline = Physics
| abbreviation = Int. J. Mod. Phys. E
| impact = 1.343
| impact-year = 2014
| history = 1992
| publisher = World Scientific
| country = Singapore
| website = http://www.worldscientific.com/worldscinet/ijmpe
| ISSN = 0218-3013
| eISSN = 1793-6608
}}
The '''''International Journal of Modern Physics E''''' was established in 1992. It covers specifically topics on experimental, theoretical and computational nuclear science, and its applications and interface with astrophysics and particle physics.

The journal is abstracted and indexed in:
* Current Contents/Physical, Chemical & Earth Sciences
* Astrophysics Data System (ADS) Abstract Service
* [[Inspec]]

==References==
<references/>

[[Category:World Scientific academic journals]]
[[Category:Physics journals]]
[[Category:English-language journals]]