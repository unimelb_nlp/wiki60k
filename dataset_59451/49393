'''Jens Hansen Lundager''' (4 May 1853 – 7 March 1930) was a Danish-born Australian photographer, newspaper editor and politician.

==Early life==
Lundager was born, along with a twin sister, in [[Vejlby]], [[Denmark]] on 4 May 1853 to his parents Hans Jensen Hansen and Else AndersDatte Hansen.<ref name=Danes>[http://www.danesinaustralia.com/jens-hansen-lundager-new.html Jens Hansen Lundager (1853-1930) - Photographer, Newspaper Editor and Politician], Danes In Australia website.  Retrieved 7 April 2017.</ref>

At birth, Lundager was named Jens Larsen Hansen.  It was not until after he emigrated to Australia that he changed his surname to Lundager, which is believed to have been the name of his mother's home village.<ref name=Danes/>

Growing up in [[Bogense]], Lundager's mother died when he was eleven, forcing him to find work as a servant before eventually relocating to [[Odense]] where he learnt [[pottery]].<ref name=Danes/>

Lundager was struck down with [[tuberculosis]] leaving him unable to work for two years.<ref name=Danes/>

When he was ready to work again, he was compelled to find less physically challenging work, and therefore entered the field of [[photography]], establishing his own photography business in [[Fredericia]].<ref name=Danes/>

However, Lundager still struggled to recover from the illness prompting his doctor to suggest that he consider seeking a warmer climate.  Taking his doctor's advice, Lundager decided to [[emigrate]] to [[Australia]].<ref name=Danes/>

==Arrival in Australia==
Lundager made the journey from [[Hamburg]] to [[Rockhampton]] aboard the immigrant ship ''Charles Dickens'', arriving in [[Keppel Bay]] on 26 February 1879.<ref>Queensland State Archives; Registers of Immigrant Ships' Arrivals; Series: Series ID 13086; Roll: M1698.  Accessed 7 April 2017.</ref>

Lundager was among the 208 Danes that arrived in Rockhampton from the ''Charles Dickens''.<ref name=immigrant>[http://trove.nla.gov.au/newspaper/article/51990685 The Immigrants by the ship Charles Dickens...], ''[[The Morning Bulletin]]'', 3 March 1879. Retrieved (via [[National Library of Australia|NLA]]) 7 April 2017.</ref>  The other passengers consisted of 178 Germans, 60 Norwegians, 43 Swedes, 22 Swiss and 17 Italians.<ref name=immigrant/>  Seven people, including two children, had died during the journey and four babies were born between Hamburg and Rockhampton.<ref name=immigrant/>

The ''Charles Dickens''' arrival in Rockhampton was not without controversy.  It was reported that a Danish man had lost his life when the ''Lady Bowen'', the vessel used to transfer the immigrants from the ''Charles Dickens'' to the Rockhampton Wharf collided with a [[schooner]].  It was believed that the jolt from the collision had caused the man to be thrown overboard.<ref name=immigrant/>  However, it was later discovered that the man hadn't been on the ''Lady Bowen'' at all.  He was found asleep still aboard the ''Charles Dickens'' while his fellow immigrants were being transported up the [[Fitzroy River (Queensland)|Fitzroy River]].  When notified of his whereabouts, a [[cutter (boat)|cutter]] was sent to retrieve the man from the ''Charles Dickens''.  On the return journey, the immigrant and the crew on the cutter saw police divers, who were yet to be notified of the man's sudden reappearance, searching for his body near Lakes Creek.<ref>[http://trove.nla.gov.au/newspaper/article/51989334 The immigrant who was supposed to be drowned...], ''[[The Morning Bulletin]]'', 6 March 1879. Retrieved (via [[National Library of Australia|NLA]]) 7 April 2017.</ref>

It was also claimed that a number of passengers had arrived in Rockhampton under the impression that they would be entitled to receive a land order, enabling them to settle on and cultivate a parcel of land.  This was despite the land order system being abolished several years earlier.<ref>[http://trove.nla.gov.au/newspaper/article/51990590 Editorial], ''[[The Morning Bulletin]]'', 4 March 1879. Retrieved (via [[National Library of Australia|NLA]]) 6 April 2017.</ref>

There was also criticism of the timing of ''Charles Dickens''' arrival due to the fact that passengers aboard a previous immigrant ship, ''The Carnatic'' had "gutted" the local labour market which many believed would make it particularly difficult for the immigrants from the ''Charles Dickens'' to find work.<ref>[http://trove.nla.gov.au/newspaper/article/51991203 The Week], ''[[The Morning Bulletin]]'', 1 March 1879. Retrieved (via [[National Library of Australia|NLA]]) 6 April 2017.</ref>

After his arrival in Rockhampton, Lundager undertook some photography work but was briefly lured to the goldfields at [[Temora, New South Wales|Temora]] in the [[Riverina]] district of [[New South Wales]].<ref name=Danes/>

Upon his return to Rockhampton, he took over a photographic studio originally established by French photographer Louis Buderus.<ref name=Danes/> who moved to [[Clermont, Queensland]] to open a photographic studio there in 1884.<ref>[https://www.daao.org.au/bio/louis-buderus Biography: Louis Buderus], Design & Art Australia Online website.  Accessed 7 April 
2017.</ref>

==Personal life==
After settling in Rockhampton, Lundager married Mathilde Helene in 1882 and was naturalised as an Australian in 1883.<ref name=Danes/> 

Lundager and his wife had seven children: Else Johanna, Marie Chrestine, Henry Walter, Hulda Hellene, Mary Christina, Alma May and Dagmar Mathilde.  Two of their children, Marie and Henry, both succumbed to [[diphtheria]] in 1890.<ref name=Danes/>

==Photography==
He soon became well known in the local area for his high quality photography.

The [[Mount Morgan Mine]] commissioned Lundager to take portraits of mine owners, managers, guests, mine workers and of the actual mine operations at [[Mount Morgan, Queensland|Mount Morgan]].<ref name=Danes/>

In 1885, the Queensland Government commissioned Lundager to create an album for the [[Colonial and Indian Exhibition]] of 1886 in [[London]], for which he received praise and a bronze medal.<ref>[http://historyofworldphotography.weebly.com/jens-hansen-lundager-fotograf-i-australien.html Jens Hansen Lundager, Queensland], History of World Photography], [[Weebly]].  Accessed 7 April 2017.</ref>  His work was praised in London's ''Photographic News'' magazine in which they described a South Australian work as dull, with the "dullness" more noticeable against the works of Lundager.  According to the magazine, Lundager had succeeded in his works of the Mount Morgan Mine, obtaining a quality that the English regarded as inseparable from good landscape work.  The publication pondered if Queensland was more suitable for photography than South Australia or whether the difference was in the photographer who created the work.<ref>[http://trove.nla.gov.au/newspaper/article/52064385 Photographic News], ''[[The Morning Bulletin]]'', 12 November 1886. Retrieved (via [[National Library of Australia]]) 7 April 2017.</ref>

In 1889, the Rockhampton Reception Committee presented an album of Lundager's works were presented to visiting Irish MP and [[Irish Home Rule movement|Home Rule]] advocate [[John Dillon]].<ref>[http://onesearch.slq.qld.gov.au/SLQ:SLQ_PCI_EBSCO:slq_alma21148829810002061 John Dillon Presentation Album, 1887-1888], J. H. Lundager, Rockhampton Reception Committee, Accessed (via [[State Library of Queensland|SLQ]]) 7 April 2017.</ref>

Dillon was on an Australian fundraising tour for the [[Irish nationalism|Irish National Movement]] and gave lectures at the [[Hibernian Bible Society|Hibernian]] Hall in Rockhampton<ref>[http://trove.nla.gov.au/newspaper/article/213466455 John Dillon in Rockhampton: An Enthusiastic Gathering], [[The Northern Argus|The Daily Northern Argus]], 12 June 1889. Retrieved (via [[National Library of Australia|NLA]]) 7 April 2017.</ref> and at the [[Mount Morgan School of Arts]].<ref>[http://trove.nla.gov.au/newspaper/article/52279706 Mr Dillon at Mount Morgan], ''[[The Morning Bulletin]]'', 14 June 1889. Retrieved (via [[National Library of Australia|NLA]]) 7 April 2017.</ref>

Throughout his busy public life, Lundager continued his passion for photography and in 1911, released ''Central Queensland Illustrated'', a compilation of photos he had taken in [[Central Queensland]] as a tribute and record to commemorate the region's 50th anniversary.<ref>[http://handle.slv.vic.gov.au/10381/152331 Central Queensland Illustrated], J. H. Lundager, 1911. Accessed (via [[State Library of Victoria|SLV]]) 7 April 2017.</ref>

==Studio fire==
In early November 1889, Lundager's shop in Rockhampton's East Street was destroyed in an overnight blaze.  Although some equipment including a camera and some photographs, were able to be saved, many valuable photos and negatives were destroyed in the fire.<ref>[http://trove.nla.gov.au/newspaper/article/52286631 Fire in East Street], ''[[The Morning Bulletin]]'', 5 November 1889. Retrieved (via [[National Library of Australia|NLA]]) 7 April 2017.</ref><ref>[http://trove.nla.gov.au/newspaper/article/213397552 Another Destructive Fire: Mr Lundager's Studio Burnt], ''[[The Northern Argus|The Daily Northern Argus]]'', 5 November 1889. Retrieved (via [[National Library of Australia|NLA]]) 7 April 2017.</ref><ref>[http://trove.nla.gov.au/newspaper/article/3503536 Fire At Rockhampton], ''[[The Courier-Mail|The Brisbane Courier]]'', 6 November 1889. Retrieved (via [[National Library of Australia|NLA]]) 7 April 2017.</ref>

After the fire, Lundager made the decision to permanently take up residence in Mount Morgan, 25 miles from Rockhampton.<ref name=Danes/>

==Relocation==
After relocating to Mount Morgan, Lundager again established a photographic business.  He also became a bookseller and stationery agent to supplement his income.

During his time in Mount Morgan, Lundager became heavily involved in all aspects of public life.

In 1905, Lundager was unanimously elected mayor at the [[Shire of Mount Morgan|Mount Morgan Town Council]].<ref>[http://trove.nla.gov.au/newspaper/article/53033119 Mount Morgan Council], ''The Morning Bulletin'', 9 February 1905. Retrieved (via [[National Library of Australia|NLA]]) 7 April 2017.</ref>.  Before being officially elected as mayor, Lundager was already an alderman with the council and was serving as acting mayor.  Lundager was again elected mayor of Mount Morgan Town Council in 1906.<ref>[http://trove.nla.gov.au/newspaper/article/53061454 Mount Morgan Council: Election of Mayor; Alderman Lundager Re-Elected], ''The Morning Bulletin'', 7 February 1906. Retrieved (via [[National Library of Australia|NLA]]) 7 April 2017.</ref>

His interest in politics also extended to a Federal level.  Lundager was a [[Candidates of the Australian federal election, 1906#Queensland|candidate]] for a seat in the [[Australian Senate]] at the [[Australian federal election, 1906|1906 Federal Election]]<ref>[http://trove.nla.gov.au/newspaper/article/70819200 Letter from Candidate Lundager], ''[[The Worker (Brisbane)|The Worker]]'', 29 September 1906. Retrieved (via [[National Library of Australia|NLA]]) 7 April 2017.</ref> winning over 47,000 votes.<ref name=Morgan>[http://trove.nla.gov.au/newspaper/article/53257178 Mount Morgan: Mr. J. H. Lundager], ''[[The Morning Bulletin]]'', 12 January 1912. Retrieved (via [[National Library of Australia|NLA]]) 7 April 2017.</ref>

Throughout his time in Mount Morgan, Lundager was involved in the Mount Morgan Progress Association, the Mount Morgan School of Arts, the Mount Morgan Hospital Committee, the Mount Morgan Technical College, the Mount Morgan Boys' School Committee, the Mount Morgan Girls' School Committee, the Penny Savings Bank, the Gordon Club, the Mount Morgan Licensing Bench, the [[Mount Morgan Masonic Temple|Mount Morgan Masonic Lodge]], the Workers' Political Organisation and the Australian Workers' Association.<ref name=Morgan/>

Additionally, he was editor and part-proprietor of local newspaper ''Mount Morgan Argus'' for six years<ref name=Morgan/> - a version of which is still being published today.<ref>[http://www.mountmorgan.org.au/argus.php Mount Morgan Argus website].  Accessed 7 April 2017.</ref>  He was also vocal proponent for the [[Dawson Valley railway line]].<ref name=Morgan/>

Following declining health including the re-emergence of tuberculosis<ref name=Danes/> Lundager began to withdraw from his various public and political interests in Mount Morgan in 1912, following advice from doctors after experiencing poor health.  In an article published in ''The Morning Bulletin'', the writer inferred that Lundager's health was inevitably going to suffer due to the strenuous life he had led for many years, without taking a break.<ref name=Morgan/>

In 1919, Lundager made the decision to leave Mount Morgan and relocate to [[Sydney]]<ref>[http://trove.nla.gov.au/newspaper/article/69778805 Departure of Mr. J. H. Lundager], ''[[The Capricornian]]'', 13 December 1919. Retrieved (via [[National Library of Australia|NLA]]) 7 April 2017.</ref> but not before his shop in Mount Morgan was ransacked in 1916 when thieves gained access to the premises through an unlocked window to steal a number of items, although they overlooked a number of valuable possessions during the break-in.<ref>[http://trove.nla.gov.au/newspaper/article/53383124 Thefts from Mr. Landager's Shop], ''[[The Morning Bulletin]]'', 4 July 1916. Retrieved (via [[National Library of Australia|NLA]]) 7 April 2017.</ref>

==Death==
Lundager died at his home in [[Chatswood, New South Wales]] on 7 March 1930 at the age of 76.<ref name=Danes/><ref>[http://trove.nla.gov.au/newspaper/article/55356505 Obituary: Mr. J. H. Lundager], ''The Morning Bulletin'', 12 March 1930. Retrieved (via [[National Library of Australia|NLA]]) 7 April 2017.</ref>  He is buried at the Methodist Cemetery in North Sydney.<ref>[https://www.facebook.com/VintageQueensland/photos/a.154324174754972.1073741845.148559738664749/154332108087512 Photo: Jens Hansen Lundager], Vintage Queensland Facebook page, 29 June 2013. Retrieved 7 April 2017.</ref>

==Legacy==
Lundager's photographic work is still regularly used to illustrate the various developments, events and people of Central Queensland of the late 19th century and the early 20th century.<ref>[http://trove.nla.gov.au/picture/result?q=creator%3A%22Lundager%2C+J.+H.+%28Jens+Hansen%29%2C+1853-1930%22&s=0 Pictures, Photos, Objects created by Jens Hansen Lundager, Rockhampton], [[National Library of Australia]].  Accessed 7 April 2017.</ref><ref>[http://trove.nla.gov.au/newspaper/article/56945682 These Were Mt. Morgan's Yesterdays], ''[[The Morning Bulletin]]'', 7 June 1950. Retrieved (via [[National Library of Australia|NLA]]) 7 April 2017.</ref><ref>[https://www.facebook.com/OLD.PICS.AND.YARNS/photos/a.402153283145167.114676.271976536162843/1437012179659267 Blacks' Camp near Mt. Morgan], J. H. Lundager, Photo: Mt. Morgan (Series 4), Central Queensland Old Pics and Yarns Facebook page, 29 July 2016. Retrieved 7 April 2017.</ref><ref>[https://www.facebook.com/artgalleryrockhampton/photos/a.229745413753358.57879.211434578917775/1405041809557040 Photo: Moore's Creek, Rockhampton (1890)], Offset photo-lithograph (postcard), J. H. Lundager, Gift of Ross Searle (2009), Rockhampton Art Gallery Facebook page, 3 April 2017. Retrieved 7 April 2017.</ref><ref>[https://www.flickr.com/photos/tags/Jens%20Hansen%20Lundager All photos tagged 'Jens Hansen Lundager'], [[Flickr]] website. Accessed 7 April 2017.</ref>

Lundager was also the subject of a 1992 Journal Article by Grahame Griffin, entitled ''J. H. Lundager, Mount Morgan politician and photographer: company hack or subtle subversive?''<ref>[http://hdl.cqu.edu.au/10018/53495 Griffin, G 1992, 'J.H. Lundager, Mount Morgan politician and photographer: company hack or subtle subversive?'], [[Central Queensland University]] Institutional Repository, Journal of Australian Studies , vol 16, no 34, pp.15-31</ref>

==Miscellaneous==
Lundager's daughter Hulda was among the casualties of the serious tourist coach accident on the [[Gillies Range]] near [[Gordonvale, Queensland]] in 1939.  She was one of ten people injured when the vehicle plunged over an embankment on its way from [[Cairns]] to the [[Atherton Tableland]], killing two tourists instantly.<ref>[http://trove.nla.gov.au/newspaper/article/150817152 Tragic Smash On Range Road; Two Killed: Ten Injured; Tourist Car Goes Over Embankment; Five Pinned Beneath Wreckage], ''[[Northern Herald (Queensland)|The Northern Herald]]'', 19 August 1939. Retrieved (via [[National Library of Australia|NLA]]) 7 April 2017.</ref><ref>[http://trove.nla.gov.au/newspaper/article/56042876 Miss Hulda Lundager], ''[[The Morning Bulletin]]'', 26 August 1939. Retrieved (via [[National Library of Australia|NLA]]) 7 April 2017.</ref>

== References ==
{{reflist|30em}}
{{authority control}}
{{DEFAULTSORT:Lundager, Jens Hansen}}
[[Category:Australian photographers]]
[[Category:Danish photographers]]
[[Category:1853 births]]
[[Category:1930 deaths]]
[[Category:People from Aarhus]]
[[Category:20th-century Australian politicians]]
[[Category:Mayors of places in Queensland]]
[[Category:Danish emigrants to Australia]]
[[Category:Australian newspaper editors]]
[[Category:Australian newspaper proprietors]]