{{Infobox journal
| title = International Journal of Mobile and Blended Learning
| cover =
| editor = David Parsons
| discipline = [[Educational technology]]
| abbreviation = Int. J. Mobile Blended Learn.
| publisher = [[IGI Global]] on behalf of the International Association for Mobile Learning
| country =
| frequency = Quarterly
| history = 2009-present
| openaccess =
| website = http://www.igi-global.com/ijmbl
| link1 =
| link1-name =
| link2 = http://www.igi-global.com/journal-contents/international-journal-mobile-blended-learning/1115
| link2-name = Online archive
| JSTOR =
| OCLC = 216884549
| LCCN =
| ISSN = 1941-8647
| eISSN = 1941-8655
}}
The '''''International Journal of Mobile and Blended Learning''''' is a quarterly [[peer-reviewed]] [[academic journal]] which focuses on [[educational technology]], specifically on theoretical, technical, and pedagogical aspects of learning in mobile and blended environments. It is the official journal of the International Association for Mobile Learning and is published on their behalf by [[IGI Global]]. The journal was established in 2009 by David Parsons ([[Massey University]]), who remains the [[editor-in-chief]]. Annual collections of papers from the journal are published as a series of edited books.

==Abstracting and indexing==
The journal is abstracted and indexed in:

*[[ACM Digital Library]]<ref>{{cite web|title=International Journal of Mobile and Blended Learning|url=http://dl.acm.org/citation.cfm?id=J1414&CFID=682848588&CFTOKEN=60735399|website=ACM Digital Library|publisher=Association for Computing Machinery|accessdate=17 October 2016}}</ref>
*[[Inspec]]<ref name=Inspec>{{cite web |url=http://www.theiet.org/resources/inspec/support/docs/loj.cfm?type=pdf |title=Inspec list of journals |format=[[PDF]] |publisher=[[Institution of Engineering and Technology (professional society)|Institution of Engineering and Technology]] |work=Inspec |accessdate=2016-08-26}}</ref>
*[[Compendex]]<ref name=Compendex>{{cite web |url=http://www.elsevier.com/online-tools/engineering-village/contentdatabase-overview |title=Content/Database Overview - Compendex Source List |publisher=[[Elsevier]] |work=Engineering Village |accessdate=2016-08-26}}</ref>
*[[Emerging Sources Citation Index]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2016-08-26}}</ref>
*[[PsycINFO]]<ref>{{cite web |url=http://www.apa.org/pubs/databases/psycinfo/coverage.aspx |publisher=[[American Psychological Association]] |title=PsychINFO Journal Coverage |accessdate=2016-08-26}}</ref>
*[[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2016-08-26}}</ref>

==References==
{{Reflist}}

==External links==
* {{Official website|http://www.igi-global.com/ijmbl}}

{{DEFAULTSORT:International Journal of Mobile and Blended Learning}}
[[Category:Publications established in 2009]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Educational technology journals]]
[[Category:IGI Global academic journals]]

{{edu-journal-stub}}