{{Use dmy dates|date=February 2012}}
{{Use Australian English|date=October 2015}}
{{good article}}
{{Infobox organisation
|name         = Australian Academy of Cinema and Television Arts
|image        = Australian Academy of Cinema and Television Arts (logo).jpg
|image_border =
|size         =
|caption      =
|map          =
|msize        =
|mcaption     =
|abbreviation = AACTA
|motto        =
|formation    = 18 August 2011
|extinction   =
|type         = Film and television organisation
|status       =
|purpose      = "to identify, award, promote and celebrate Australia's greatest achievements in film and television."<ref name="academyoverview">{{cite web|url=http://aacta.org/the-academy.aspx|title=AACTA - The Academy|publisher=Australian Academy of Cinema and Television Arts (AACTA)|accessdate=15 December 2011}}</ref>
|headquarters = [[South Melbourne, Victoria]]
|location     = 236 Dorcas Street, South Melbourne, Victoria 3205
|region_served = [[Australia]]
|membership   =
|language     =
|leader_title = President
|leader_name  = [[Geoffrey Rush]]
|leader_title2 = Patron
|leader_name2  = [[George Miller (filmmaker)|George Miller]]
|main_organ =
|parent_organisation = [[Australian Film Institute]] (AFI)
|affiliations =
|num_staff    =
|num_volunteers =
|budget       =
|website      = {{URL|aacta.org}}
|remarks =
}}

The '''Australian Academy of Cinema and Television Arts''' ('''AACTA''') is a professional organisation of film and television practitioners in Australia. The Academy's aim is "to identify, award, promote and celebrate Australia's greatest achievements in film and television."<ref name="academyoverview" />

It was established in August 2011 with the backing of the [[Australian Film Institute]] (AFI) to act as its industry engagement arm and to administer the [[AACTA Awards]] (formerly the Australian Film Institute Awards, also known as the AFI Awards) which rewards achievements in Australian feature film, television, documentary and short films.<ref>{{cite news|url=http://www.abc.net.au/news/2011-08-19/geoffrey-rush-australian-academy/2846536|title=Rush named president of Australian Oscars|publisher=[[Australian Broadcasting Corporation]] (ABC)|author=Staff|date=19 August 2011|accessdate=15 December 2011}}</ref><ref name="AACTAoverview" />

The Academy is composed of 15 Chapters, each of which represents different screen artists including actors, directors, producers and writers, and it is overseen by the Academy's president and the Honorary Council. Australian actor [[Geoffrey Rush]] is the inaugural President and hosted the [[2011 AACTA Film Awards|inaugural AACTA Awards]] in January 2012.<ref name="AACTAoverview" />

==Background==
{{See also|Australian Film Institute}}
The Australian Academy of Cinema and Television Arts (AACTA), is a not for profit, membership based, organisation whose aim is "to identify, award, promote and celebrate Australia's greatest achievements in film and television."<ref name="academyoverview" /> The Academy is a subsidiary of the [[Australian Film Institute]] (AFI), a non-profit organisation which was established in 1958 to develop an active film culture in Australia and to foster engagement between the general public and the Australian film industry.<ref name=AFI2010>{{cite web|url=http://www.afi.org.au/AM/ContentManagerNet/HTMLDisplay.aspx?ContentID=12382&Section=About|title=The Australian Film Institute - About AFI|publisher=[[Australian Film Institute]] (AFI)|accessdate=15 December 2011}}</ref><ref name="SMH19Aug2011" /> The AFI was also responsible for administering the Australian Film Institute Awards (more commonly known as the AFI Awards), which until 2011 rewarded Australian practitioners in feature film, television, documentary and short film screen crafts.<ref name=AFI2010 /> The Academy receives funding by the AFI, and Australian [[States and territories of Australia|state]] and [[Government of Australia|federal governments]].<ref name="AFI2010" /><ref>{{cite web|url=http://www.afi.org.au/AM/ContentManagerNet/HTMLDisplay.aspx?ContentID=13084&Section=Sponsors_and_Partners|title=The Australian Film Institute - Sponsors and Partners|publisher=Australian Film Institute (AFI)|accessdate=15 December 2011}}</ref>

In June 2011, the AFI proposed the establishment of an "Australian Academy".<ref>{{cite news |url=http://www.smh.com.au/entertainment/movies/afi-looks-to-academy-awards-in-reinvention-20110628-1gp2j.html |title=AFI looks to Academy Awards in reinvention |author=Paul Kalina|work=Sydney Morning Herald|publisher=[[Fairfax Media]] |accessdate=23 July 2011|date=29 June 2011}}</ref> The objectives for the proposed academy was to raise the profile of Australian film and television in Australia and abroad, and to change the way it rewards talent by mimicking the methods used in foreign film organisations, such as [[Academy of Motion Picture Arts and Sciences]] (AMPAS) and [[British Academy of Film and Television Arts]] (BAFTA).<ref name="AFIannouncement">{{cite web | url = http://www.afi.org.au/AM/ContentManagerNet/HTMLDisplay.aspx?ContentID=12535&Section=Consultation#prop_dev| title = Proposed AFI Developments| publisher = Australian Film Institute (AFI) | date = | accessdate = 21 July 2011| archiveurl= https://web.archive.org/web/20110613041738/http://www.afi.org.au/AM/ContentManagerNet/HTMLDisplay.aspx?ContentID=12535&Section=Consultation| archivedate= 13 June 2011 <!--DASHBot-->| deadurl= no}}</ref> The voting system would change through the establishment of an "Honorary Council", which will govern fifteen chapters composed of professionals from industry guilds and organisations including [[actor]]s, [[Film director|directors]], [[Film producer|producers]] and [[screenwriter]]s.<ref name="AFIannouncement" /> It was also stated that the Academy would not replace the AFI and past winners of the AFI Awards would "[...] constitute the founding heritage of an ‘Australian Academy.’"<ref>{{cite web | url = http://www.afi.org.au/AM/ContentManagerNet/HTMLDisplay.aspx?ContentID=12535&Section=Consultation#prop_dev| title = Building the Australian Film and Television brand| publisher = Australian Film Institute (AFI)| date = | accessdate = 21 July 2011| archiveurl= https://web.archive.org/web/20110613041738/http://www.afi.org.au/AM/ContentManagerNet/HTMLDisplay.aspx?ContentID=12535&Section=Consultation| archivedate= 13 June 2011 <!--DASHBot-->| deadurl= no}}</ref> When the announcement of the proposal was made, the AFI began the consultation phase where members of the public and screen industry gave their feedback on the proposed changes throughout June, 2011.<ref>{{cite news |url=http://www.skynews.com.au/showbiz/article.aspx?id=620194&vId= |title=AFI awards to move to January |author=Staff|work=[[Sky News Australia]]|publisher=Australian News Channel Pty Ltd  |accessdate=30 August 2011|date=1 June 2011}}</ref> Of the announcement Damian Trewhella, CEO of the AFI said, "We thought a better way to engage with the industry would be to try and improve our professional membership structure[...] It's quite a big improvement on the way the AFI does things."<ref>{{cite news |url=http://www.filmink.com.au/news/change-ahead-for-the-afi/ |title=AFI looks to Academy Awards in reinvention |author=Cara Nash|publisher=''[[Filmink]]'' |accessdate=19 August 2011|date=1 June 2011}}</ref>

By 20 July, weeks after the consultation period ended, the AFI announced that it would go ahead with the proposed changes and the Australian Academy.<ref>{{cite news |url=http://www.theaustralian.com.au/business/opinion/afi-moves-ahead-with-plans-to-establish-academy/story-e6frg9sx-1226097810423 |title=AFI moves ahead with plans to establish academy |author=Michael Bodey|work=[[The Australian]]|publisher=[[News Limited]] ([[News Corporation]]) |accessdate=30 August 2011|date=20 July 2011| archiveurl= https://web.archive.org/web/20110725004151/http://www.theaustralian.com.au/business/opinion/afi-moves-ahead-with-plans-to-establish-academy/story-e6frg9sx-1226097810423| archivedate= 25 July 2011 <!--DASHBot-->| deadurl= no}}</ref> When asked about the timing of the announcement Trewhella stated that, "Based on the overwhelming industry support we have received, we are now confident that we are moving in the right direction, and therefore that we can move briskly to establish the initial phase of the Academy."<ref>{{cite news|author=Paul Chai  |url=http://www.variety.com/article/VR1118040164?refcatid=19&printerfriendly=true |title=Aussies to open 'Australian Academy' |work=[[Variety (magazine)|Variety]] |publisher=[[Reed Elsevier]]|date=20 July 2011 |accessdate=14 February 2012}}</ref> On 18 August 2011, the AFI announced, in a special event at the [[Sydney Opera House]], that the academy would be called the Australian Academy of Cinema and Television Arts (AACTA) and the inaugural awards ceremony would be renamed the [[AACTA Awards]], but serve as a continuum to the annual AFI Awards.<ref name="AACTAoverview">{{cite web|url=http://aacta.org/the-awards.aspx|title=AACTA - The Awards, Overview|publisher=Australian Academy of Cinema and Television Arts (AACTA)|accessdate=31 August 2011}}</ref><ref>{{cite news |url=http://www.theage.com.au/entertainment/afi-gong-gone-in-hustle-for-global-muscle-20110818-1j063.html |title=AFI gong gone in hustle for global muscle |author=Karl Quinn|work=[[The Age]]|publisher=Fairfax Media |accessdate=30 August 2011|date=19 August 2011}}</ref> During the event it was also made known that the president of the inaugural awards would be [[Geoffrey Rush]]. On the night a new gold statuette was revealed, created by Australian sculptor [[Ron Gomboc]], which depicts "a human silhouette based on the shape of the [[Southern Cross]] constellation."<ref name="SMH19Aug2011">{{cite news |url=http://www.smh.com.au/entertainment/movies/afi-is-recast-with-new-academy-taking-a-bow-20110818-1izyb.html |title=AFI is recast with new academy taking a bow |author=Adam Fulton|work=[[Sydney Morning Herald]]|publisher=Fairfax Media |accessdate=30 August 2011|date=19 August 2011}}</ref><ref>{{cite web|title=The Story of the Statuette |publisher=Australian Academy of Cinema and Television Arts (AACTA) |url=http://aacta.org/story-of-the-statuette.aspx|date=|accessdate=5 September 2011}}</ref>

==Structure==
The Academy, which has between 1,500 and 2,000 members,<ref>{{cite news|author=Christy Grosz  |url=http://www.variety.com/article/VR1118049398/ |title=Australian Academy lauds 'Artist' |work=Variety|publisher=Reed Elsevier |date=27 January 2012 |accessdate=28 January 2012}}</ref> comprises fifteen Chapters, with each representing a different area of speciality in feature film, television, documentary and short film. It is overseen by the Academy's president and the Honorary Council. The role of the Honorary Council is to determine policies and strategies for the way the Academy rewards practitioners.<ref name="academyoverview" /> The Chapters are as follows:<ref>{{cite web|url=http://aacta.org/files/AACTA-Honorary-Council.pdf|title=AACTA - Honorary Council|publisher=Australian Academy of Cinema and Television Arts|accessdate=15 December 2011}}</ref>
{{col-begin}}
{{col-3}}
*Actors
*Animation
*Cinematographers
*Composers
*Costume Designers
{{col-3}}
*Directors
*Editors
*Executives
*Hair and Make-up Artists
*Media and Public Relations
{{col-3}}
*Producers
*Production Designers
*Screenwriters
*Sound
*Visual and Special Effects
{{col-end}}

==Honorary Councillors==
{{col-begin}}
{{col-3}}
* [[Stuart Beattie]], 2011–present
* [[Jan Chapman]], 2011–present
* Jonathan Chissick, 2011–present
* [[Abbie Cornish]], 2011–present
* [[Rolf de Heer]], 2011–present
* Elizabeth Drake, 2011–present
* [[Adam Elliot]], 2011–present
*[[Antony I. Ginnane]]<ref>{{cite web|url=http://blogafi.org/2012/08/09/last-dance-an-interview-with-producer-antony-i-ginnane/|title=Last Dance – an interview with producer Antony I. Ginnane|publisher=Australian Film Institute (AFI) Blog|first= Rochelle|last=Siemienowicz|date=9 August 2012|accessdate=4 March 2013}}</ref> 
{{col-3}}
* [[Nikki Gooley]], 2011–present<ref>{{cite web|url=http://blogafi.org/2012/08/16/aacta-member-spotlight-nikki-gooley-hair-make-up-artist/|title=AACTA Member Spotlight: Nikki Gooley – Hair & Make-Up Artist|publisher=Australian Film Institute (AFI) Blog|first= Lia|last=McCrae-Moore|date=16 August 2012|accessdate=4 March 2013}}</ref>
* Ian Gracie, 2011–present
* [[David Hirschfelder]], 2011–present
* Jessica Hobbs, 2011–present<ref>{{cite web|url=http://blogafi.org/2011/12/21/focus-on-the-television-nominees-part-2-direction-and-screenplay/|title=Focus on the Television Nominees: Part 2 – Direction and Screenplay|publisher=Australian Film Institute (AFI) Blog|first= Rochelle|last=Siemienowicz|date=21 December 2011|accessdate=4 March 2013}}</ref>
* Cappi Ireland, 2011–present
* [[Peter James (cinematographer)|Peter James]] ACS ASC, 2011–present
* [[Claudia Karvan]], 2011–present
* Aphrodite Kondos, 2011–present
{{col-3}}
* Andrew Mason, 2011–present
* [[Deborah Mailman]], 2011–present
* Tony Murtagh, 2011–present
* [[Antony Partos]], 2011–present
* [[Jan Sardi]], 2011–present
* [[Fred Schepisi]], 2011–present
* Emile Sherman, 2011–present
* [[Jack Thompson (actor)|Jack Thompson]], 2011–present
{{col-end}}

==Events==

===Festival of film===
The Festival of Film, which is held in conjunction with the Australian Film Institute, showcases the films in competition for the [[AACTA Awards]], with the inaugural festival held in [[Sydney]] and [[Melbourne]] from October to November in 2011.<ref>{{cite web|url=http://www.encoremagazine.com.au/afiaacta-festival-of-films-announced-10666|title=AFI/AACTA Festival of Films announced|publisher=Encore Magazine|date=21 September 2011|accessdate=23 September 2011}}</ref> The festival marks the beginning of the Australian film awards season, and members of the Academy can commence voting for films in all categories, while members of the Institute vote for the [[AACTA Award for Best Short Animation|Best Short Animation]], [[AACTA Award for Best Short Fiction Film|Best Short Fiction Film]] and [[AFI Members' Choice Award|Members' Choice Award]] only.<ref>{{cite web|url=http://aacta.org/media/112227/samsung-afi-aacta-festival-of-film_pr.pdf|title=New Festival of Film sees exciting start to Australian screen awards season|publisher=Australian Academy of Cinema and Television Arts (AACTA)|author=Staff|date=21 September 2011|accessdate=23 September 2011}}</ref><ref>{{cite web|url=http://afi.informz.net/afi/archives/archive_431314.html|title=Festival of Film Starts Exciting Awards Season|publisher=Australian Film Institute (AFI)|date=29 September 2011|accessdate=3 October 2011}}</ref>

===Awards===
{{Main|AACTA Awards}}
The [[AACTA Awards]] replaced the previous Australian Film Institute Awards, but serve as a continuum to past ceremonies.<ref name="AACTAoverview" /> The awards were first instituted by the Australian Film Institute in 1958 (as the Australian Film Awards) as part of the [[Melbourne International Film Festival]], until 1972.<ref name="shinelight29">{{cite book|last=French|first=Lisa|last2=Poole|first2=Mark|title=Shining a Light: 50 Years of the Australian Film Institute|publisher=Australian Teachers of Media|year=2009|isbn=1-876467-20-7|page=29}}</ref> Before 1969, awards were presented as a prize to non-feature films due to a lack of feature films produced in Australia.<ref>{{cite book|last=French|first=Lisa|last2=Poole|first2=Mark|title=Shining a Light: 50 Years of the Australian Film Institute|publisher=Australian Teachers of Media|year=2009|isbn=1-876467-20-7|page=108}}</ref> By 1976 competitive film awards were established and in 1987, awards for television was introduced.<ref>{{cite book|last=French|first=Lisa|last2=Poole|first2=Mark|title=Shining a Light: 50 Years of the Australian Film Institute|publisher=Australian Teachers of Media|year=2009|isbn=1-876467-20-7|page=|pages=142–182}}</ref><ref>{{cite book|last=French|first=Lisa|last2=Poole|first2=Mark|title=Shining a Light: 50 Years of the Australian Film Institute|publisher=Australian Teachers of Media|year=2009|isbn=1-876467-20-7|page=113}}</ref> The awards were usually held at the end of each year in [[Melbourne]] but, prior to the announcement of the Academy, the AFI announced that it would move the awards to January 2012 at the [[Sydney Opera House]], in order to align them with the international film awards season.<ref name="SMH19Aug2011"/><ref>{{cite news|url=http://www.theaustralian.com.au/news/nation/geoffrey-rush-at-the-helm-for-aussie-oscars/story-e6frg6nf-1226117741894|title=Geoffrey Rush at the helm for Aussie Oscars|work=The Australian|publisher=News Limited (News Corporation)|author=Staff|date=19 August 2011|accessdate=15 December 2011}}</ref> The awards are held over two events: the AACTA Awards Luncheon, a black tie event where accolades are handed out for non-feature and short films, film production, non-drama related television programs and the [[Raymond Longford Award]], and the AACTA Awards Ceremony which hands out the awards in all other categories at a larger venue and is broadcast on television.<ref>{{cite web|url=http://www.aacta.org/the-awards/dates.aspx |title=AACTA - The Awards - Dates |publisher=Australian Academy of Cinema and Television Arts (AACTA) |date= |accessdate=8 November 2011 }}{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref><ref>{{cite web|last=Daly|first=Brooke|title=Australian Academy Launches New International Awards in Los Angeles|url=http://aacta.org/media/173365/australian%20academy%20launches%20new%20international%20awards%20in%20los%20angeles.pdf|publisher=Australian Academy of Cinema and Television Arts (AACTA)|date=15 January 2012|accessdate=14 February 2012}}</ref> Additionally, awards for achievements in foreign film were presented once at the AACTA International Awards in [[Los Angeles]] in 2012.<ref>{{cite web | url = http://aacta.org/media/173365/australian%20academy%20launches%20new%20international%20awards%20in%20los%20angeles.pdf| title = AACTA - Awards Presented| publisher = Australian Academy of Cinema and Television Arts (AACTA) | date = | accessdate = 12 November 2011}}</ref>

==See also==
*[[Cinema in Australia]]

==References==
{{Reflist|2}}

==External links==
*{{Official website|aacta.org}}

{{Australian Film Institute Awards}}
{{CinemaofAustralia}}

[[Category:Arts organizations established in 2011]]
[[Category:2011 establishments in Australia]]
[[Category:Australian Academy of Cinema and Television Arts| ]]
[[Category:Australian Film Institute]]
[[Category:Cinema of Australia]]
[[Category:Film-related professional associations]]
[[Category:Film organisations in Australia]]
[[Category:Organisations based in Melbourne]]
[[Category:Television organisations in Australia]]