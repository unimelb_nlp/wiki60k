{{italic title}}
{{Infobox Journal
| title        = The Chaucer Review
| cover        = [[Image:The Chaucer Review.jpg]]
| editor       = Susanna Fein, David Raybin
| discipline   = [[Literature]]
| language     = English
| abbreviation = Chaucer Rev.
| publisher    = [[Penn State University Press]]
| country      = United States
| frequency    = Quarterly
| history      = 1966-present
| openaccess   = 
| impact       = 
| impact-year  = 
| website      = http://www.psupress.org/journals/jnls_chaucer.html
| link1        = http://muse.jhu.edu/journals/chaucer_review/
| link1-name   = Online access
| link2        = 
| link2-name   = 
| RSS          = 
| atom         = 
| JSTOR        = 00092002
| OCLC         = 43359050
| LCCN         = 
| CODEN        = 
| ISSN         = 0009-2002  
| eISSN        = 1528-4204
}}
'''''The Chaucer Review: A Journal of Medieval Studies and Literary Criticism''''' is an [[academic journal]] published by the [[Penn State University Press]]. Founded in 1966 by Robert W. Frank, Jr. (who continued as editor through 2002) and Edmund Reiss, ''The Chaucer Review'' acts as a forum for the presentation and discussion of research and concepts about [[Geoffrey Chaucer|Chaucer]] and the literature of the [[Middle Ages]].  The journal publishes studies of language, social and political contexts, aesthetics, and associated meanings of Chaucer's poetry, as well as articles on medieval literature, philosophy, theology, and mythography relevant to study of the poet and his contemporaries, predecessors, and audiences.  

''The Chaucer Review'' has been edited since 2001 by Susanna Fein ([[Kent State University]]) and David Raybin ([[Eastern Illinois University]]).  The four annual issues are published in January, April, July, and October and are distributed by the [[Johns Hopkins University Press]].

== External links ==
* {{Official website|http://www.psupress.org/journals/jnls_chaucer.html}}
*[http://muse.jhu.edu/journals/chaucer_review/  ''The Chaucer Review'' at [[Project MUSE]]]
*[http://www.press.jhu.edu/journals/chaucer_review/ ''The Chaucer Review'' on the JHU Press website]

{{DEFAULTSORT:Chaucer Review}}
[[Category:American literary magazines]]
[[Category:Medieval studies literature]]
[[Category:Literary criticism]]
[[Category:European literature]]
[[Category:British literature]]
[[Category:English literature]]
[[Category:Middle English literature]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1966]]
[[Category:Penn State University Press academic journals]]
[[Category:Geoffrey Chaucer]]