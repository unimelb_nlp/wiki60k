{{Infobox model
| name          = Amy Lemons
| image         = 
| birth_place   = [[Doswell, Virginia]], [[United States|U.S.]]
| height        = {{convert|5|ft|11|in|cm|abbr=on}}
| parents       = [[Donald W. Lemons]]
| hair_color    = Blonde
| eye_color     = Green/Blue
| occupation = Model
| yearsactive = 1995–present
}}

'''Amy Lemons''' is an American fashion [[model (person)|model]] and model advocate. As a straight sized (sizes typically used in editorial fashion) model, she rose to fame quickly, by landing the cover of Italian Vogue at age 14. Her ascent in the modeling industry included shooting the covers of ''[[Vogue (magazine)|Vogue]]'', ''[[Harpers Bazaar]]'', ''[[Elle (magazine)|Elle]]'' and ''[[Marie Claire]]''.  She also landed campaigns for [[Abercrombie and Fitch]], [[Tommy Hilfiger]], [[Calvin Klein]], [[Jil Sander]] and [[Louis Vuitton]].  After a brief hiatus to earn her college degree from [[UCLA]], Amy came back to the modeling industry as a [[fashion model]] and began speaking out about the industry's "zero-sized standard" and healthy self-esteem for young women.

== Biography ==
Amy Lemons was born in [[Doswell, Virginia]].  Her father is [[Donald W. Lemons]], Chief Justice of the [[Supreme Court of Virginia]].  Amy began her career as a fashion model at the age of 12 after being discovered in her dentist's office.  At the age of 14 she landed the cover of [[Italian Vogue]].  In 2009, Amy Lemons graduated from [[UCLA]] with a B.A. in History.

== Career and projects ==
Amy Lemons is currently represented by One Management and is an avid spokesperson about both the perils of the Fashion modeling industry and the honest portrayals of the Fashion modeling business.   She is an advocate for positive body image around the world.  In September 2009, Amy was featured in [[Glamour Magazine]] <ref>{{cite journal|last=Field|first=Genevieve|title=These Bodies are Beautiful at Every Size|journal=Glamour Magazine|date=2009-09-21|url=http://www.glamour.com/health-fitness/2009/10/these-bodies-are-beautiful-at-every-size}}</ref><ref>{{cite journal|last=Fields|first=Genevieve|title=Supermodels Who Aren’t Superthin: Meet the Women Who Proudly Bared it All|journal=Glamour Magazine|date=2009-09-21|url=http://www.glamour.com/health-fitness/2009/10/supermodels-who-arent-superthin#slide=7}}</ref><ref>{{cite web|last=Carmon|first=Irin|title=Spot The Plus-Size Model In Glamour|url=http://jezebel.com/5404201/spot-the-plus+size-model-in-glamour|work=Jezebel}}</ref> along with 6 other fashion models ([[Crystal Renn]], [[Ashley Graham (model)|Ashley Graham]], [[Kate Dillon Levin]], Anansa Sims, and [[Jennie Runk]]) who are larger than standard sample size.

In 2010, Amy appeared in [[New York Magazine]] as part of [[Sara Ziff]]'s "Picture Me" documentary on the often hidden side of fashion modeling.<ref>{{cite news|title=Sara Ziff's 'Picture Me' Mini-Series: Models Told To Eat Half A Rice Cake, Cotton Balls|url=http://www.huffingtonpost.com/2010/09/23/sara-ziff-picture-me-model_n_736416.html|work=Huffington Post | first=Hilary|last=Moss|date=2010-09-23}}</ref><ref>{{cite journal|last=Black|first=Rosemary|title='I saw models eating cotton balls': 'Picture Me' filmmakers reveal stark side of fashion|journal=NY Daily News|date=2010-09-24|url=http://articles.nydailynews.com/2010-09-24/entertainment/29441054_1_modeling-industry-young-models-shooting-with-flip-cams}}</ref>

Amy spoke about the issue of positive body image on [[The Ellen DeGeneres Show]].<ref>{{cite web|last=DeGeneres|first=Ellen|title=Video of the Day: Glamour Magazine's Normal-Sized Models|url=http://ellen.warnerbros.com/2009/10/glamour_magazines_normal_sized.php|work=The Ellen DeGeneres Show}}</ref>  Amy Lemons is involved with the "Let's Talk About it Campaign"<ref>{{cite web|last=Lemons|first=Amy|title=Let's Talk About It|url=http://loveyourbody.nowfoundation.org/letstalk/amy-lemons.html|work=NOW Foundation}}</ref> which is part of the NOW Foundation's Love Your Body Campaign.  "For more than a decade, NOW Foundation's Love Your Body Campaign has been calling out the fashion, cosmetics and advertising industries for promoting unrealistic images of women. The campaign encourages women and girls to celebrate their bodies and reject the narrow beauty ideals endorsed in the media." Amy Lemons has appeared on VogueTV,<ref>{{cite web|last=Lemons|first=Amy|title=Glam and Curvy|url=http://www.vogue.it/en/vogue-tv/vogue-curvy/glam-and-curvy/Amy%20Lemons/player|work=Italian Vogue}}</ref> did a segment for [[The Oprah Winfrey Show]]<ref>{{cite web|last=Winfrey|first=Oprah|title=Oprah's Favorite Jeans|url=http://www.oprah.com/style/Oprahs-New-Favorite-Jeans-CJ-by-Cookie-Johnson/3|work=Oprah Winfrey Show}}</ref><ref>{{cite web|last=Winfrey|first=Oprah|title=Adam's Jean-ius Picks|url=http://www.oprah.com/style/Adam-Glassmans-Favorite-Affordable-Jeans/4|work=Oprah Winfrey Show}}</ref>  and was featured in [[Maxim Magazine]].<ref>{{cite web|last=Lemons|first=Amy|title=Amy Lemons|url=http://www.maxim.com/girls-of-maxim/amy-lemons|work=Maxim}}</ref>

== Clients ==
Lemons has worked with many top brands and publications including [[Abercrombie and Fitch]], [[Calvin Klein]], [[Elena Miro]], [[Elle Magazine]], [[Glamour Magazine]], [[Louis Vuitton]], [[Macy’s]], [[Nordstrom]], [[Ralph Lauren]], [[Tommy Hilfiger]], [[Valentino SpA|Valentino]], and [[Versace]].

==Filmography ==

* ''Johnny Benson's Adventures in the Blogosphere'' (2005)
* ''The Gymnast'' (2006)

==References==
{{Reflist|30em}}

==External links==
* [http://amylemons.wordpress.com Amy Lemons official website]
* [http://modelalliance.org/ Model Alliance]
* {{Fashionmodel}}
* {{IMDb name|id=1993774|name=Amy Lemons}}

{{DEFAULTSORT:Lemons, Amy}}
[[Category:1981 births]]
[[Category:American female models]]
[[Category:Living people]]
[[Category:People from Hanover County, Virginia]]
[[Category:University of California, Los Angeles alumni]]