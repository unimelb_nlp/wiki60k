{{refimprove|date=January 2016}}
{{Infobox journal
| title = Language
| cover = [[File:Language (journal).jpg]]
| editor = [[Andries Coetzee (linguist)|Andries Coetzee]]
| discipline = [[Linguistics]]
| abbreviation =
| publisher = [[Linguistic Society of America]]
| country = [[United States]]
| frequency = Quarterly
| history = 1925–present
| openaccess = After one-year embargo<ref name="Jaschik">
Jaschik, Scott. "[https://www.insidehighered.com/news/2015/11/02/editors-and-editorial-board-quit-top-linguistics-journal-protest-subscription-fees Language of Protest]". ''Inside Higher Ed''. 2015-11-02. Accessed 2015-11-08.</ref>
| impact        = 1.886
| impact-year   = 2009
| website = http://www.linguisticsociety.org/lsa-publications/language
| link1 =
| link1-name =
| link2 =
| link2-name =
| JSTOR = 00978507
| OCLC = 50709582
| LCCN = 27011255 
| ISSN = 0097-8507
| eISSN = 1535-0665
}}
'''''Language''''' is a [[Peer review|peer-reviewed]] quarterly [[academic journal]] published by the [[Linguistic Society of America]] since 1925. It covers all aspects of [[linguistics]], focusing on the area of [[theoretical linguistics]]. Its current [[editor-in-chief]] is [[Andries Coetzee (linguist)|Andries Coetzee]] ([[University of Michigan]]).

Under the editorship of [[Yale University|Yale]] linguist [[Bernard Bloch (linguist)|Bernard Bloch]], ''Language'' was the vehicle for publication of many of the important articles of American [[Structuralism#Structuralism in linguistics|structural linguistics]] during the second quarter of the 20th century, and was the journal in which many of the most important subsequent developments in linguistics played themselves out.{{Citation needed|date=January 2010}}

One of the most famous articles to appear in ''Language'' was the scathing 1959 review by the young [[Noam Chomsky]] of the book [[Verbal Behavior (book)|''Verbal Behavior'']] by the [[Behaviorism|behaviorist]] [[Cognitive psychology|cognitive psychologist]] [[B. F. Skinner]].<ref>Chomsky, Noam. 1959. “A Review of B.F. Skinner’s Verbal Behavior.” Language 35 (1): 26–58.
</ref> This article argued that Behaviorist psychology, then a dominant paradigm in linguistics (as in psychology at large), had no hope of explaining complex phenomena like language. It followed by two years another book review that is almost as famous—the glowingly positive assessment of Chomsky's own 1957 book ''[[Syntactic Structures]]'' by [[Robert Lees (linguist)|Robert B. Lees]] that put Chomsky and his [[generative grammar]] on the intellectual map as the successor to American structuralism.

By far the most cited article in ''Language''<ref>[[Google Scholar]] counts over 9800 citations to Sacks, Schegloff & Jefferson 1974; the next most cited article is Chomsky's review of Skinner with 3500+ citations ([https://scholar.google.com/scholar?as_q=&num=50&btnG=Search+Scholar&as_epq=&as_oq=&as_eq=&as_occt=any&as_sauthors=&as_publication=Language&as_ylo=1925&as_yhi=2000&as_sdt=1.&as_sdtp=on&as_sdts=5&hl=en as shown here)]</ref> is the 1974 description on the [[Turn-Taking|turn-taking]] system of ordinary conversation by the founders of [[Conversation Analysis]], Harvey Sacks, Emanuel Schegloff and Gail Jefferson. This article describes the socially normative system of rules that accounts for the complex and turn-taking behaviour of participants in conversation, demonstrating the system in detail using recordings of actual conversation.

''Language'' continues to be an influential journal in the field of linguistics: it is ranked sixth out of 47 in the Linguistics category in the 2006 [[Journal Citation Reports]], with an impact factor of 1.79 and a half-life of more than 10 years.

== References ==
{{Reflist}}

== External links ==
{{Portal|Languages}}
* {{Official website|http://www.linguisticsociety.org/lsa-publications/language}}
* [http://muse.jhu.edu/journals/language Free Access to ''Language'' for LSA members since 2001]
* [http://muse.jhu.edu/journals/language ''Language''] at [[Project MUSE]]

[[Category:Linguistics journals]]
[[Category:Publications established in 1925]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]