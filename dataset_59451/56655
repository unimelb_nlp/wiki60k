{{Infobox journal
| title = The Economic and Labour Relations Review
| cover = [[File:The_Economic_and_Labour_Relations_Review_Journal_cover_small.gif.jpg]]
| editor =
| discipline = [[Economics]], [[labour relations]]
| former_names = 
| abbreviation = Econ. La. Relat. Rev.
| publisher = [[Sage Publications]]
| country = 
| frequency = Quarterly
| history = 1990-present
| openaccess = 
| license = 
| impact = 0.481
| impact-year = 2013
| website = http://www.sagepub.com/journals/Journal202205/title
| link1 = http://elr.sagepub.com/content/current
| link1-name = Online access
| link2 = http://elr.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 1035-3046
| eISSN = 1838-2673
| OCLC = 746947141
| LCCN = 96658676
}}
'''''The Economic and Labour Relations Review''''' is a [[peer-reviewed]] [[academic journal]] covering the fields of [[economics]], [[labour relations]], and [[policy]]. The [[editors-in-chief]] are Hazel Bateman, Michael Johnson, Anne Junor, P. N. (Raja) Junankar, Peter Kriesler, Michael Quinlan, and Peter Sheldon ([[University of New South Wales]]). It was established in 1990 and is published by [[Sage Publications]] in association with the [[Australian School of Business]] (University of New South Wales).

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2013 [[impact factor]] is 0.481, ranking it 18 out of 26 journals in the category "Industrial Relations & Labor"<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Industrial Relations & Labor |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |work=[[Web of Science]] |postscript=.}}</ref> and 235 out of 332 journals in the category "Economics".<ref name=WoS1>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Economics |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |work=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.sagepub.com/journals/Journal202205/title}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Economics journals]]
[[Category:Labour journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1990]]