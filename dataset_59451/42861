<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Glasflügel 604
 | image=Glasflügel H604 (D-2085).jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Glider (sailplane)|Glider]]
 | national origin=[[West Germany]]
 | manufacturer=[[Glasflügel]]
 | designer=
 | first flight=early 1970
 | introduced=1970
 | retired=
 | status=No longer in production
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=11
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= [[Glasflügel Kestrel 17]]
 | variants with their own articles=
}}
|}
The '''Glasflügel 604''' is a [[high-wing]], [[T-tail]]ed, single seat, [[FAI Open Class]] [[Glider (sailplane)|glider]] that was designed and produced in [[West Germany]] by [[Glasflügel]] starting in 1970.<ref name="SD">{{Cite web|url=http://www.sailplanedirectory.com/PlaneDetails.cfm?planeID=3 |title=604 Glasflugel |accessdate=11 July 2011 |last=Activate Media |authorlink= |year=2006 |deadurl=yes |archiveurl=https://web.archive.org/web/20120825221005/http://www.sailplanedirectory.com/PlaneDetails.cfm?planeID=3 |archivedate=25 August 2012 |df= }}</ref><ref name="SoaringNov83">Said, Bob: ''1983 Sailplane Directory, [[Soaring Magazine]]'', page 81, [[Soaring Society of America]] November 1983. USPS 499-920</ref>

Developed from the [[Glasflügel Kestrel 17|Kestrel 17]] the 604 is often erroneously called a ''Kestrel 22''. In fact the company never gave it a formal name beyond its number designation.<ref name="SoaringNov83" />

==Design and development==
The 604 was originally intended as a test aircraft for a proposed two-seat glider that was never built. The prototype did so well in competition that a ten-aircraft production run was completed, resulting in a total of eleven 604s being completed, including the prototype.<ref name="SD" /><ref name="SoaringNov83" />

Due to the aircraft's huge size, including its {{convert|22.0|m|ft|1|abbr=on}} three-piece wing that weighs over {{convert|272|kg|lb|0|abbr=on}}, the aircraft picked up the nickname "the Jumbo". The wing centre section alone weighs {{convert|115|kg|lb|0|abbr=on}}, making rigging it for flight a chore that requires a large crew.<ref name="SoaringNov83" />

The 604 is constructed from [[fibreglass]]. The wing employs a modified Wortmann FX 67-K-170 [[airfoil]] at the [[wing root]], changing to a Wortmann FX 67-K-150 at the [[wing tip]]. The wing features six [[Flap (aircraft)|flaps]], with the outer pair moving at a 2:1 differential ratio with the [[aileron]]s. For glidepath control the 604 has wing top-surface [[Spoiler (aeronautics)|spoilers]] and a tail-mounted [[drag chute]]. The aircraft can carry {{convert|100|kg|lb|0|abbr=on}} of water ballast. The [[landing gear]] is a retractable monowheel.<ref name="SD" /><ref name="SoaringNov83" /><ref name="Incomplete">{{Cite web|url = http://www.ae.illinois.edu/m-selig/ads/aircraft.html|title = The Incomplete Guide to Airfoil Usage |accessdate = 11 July 2011|last = Lednicer |first = David |authorlink = |year = 2010}}</ref>

==Operational history==
[[File:Glasflügel 604 in Gap.JPG|thumb|right|Glasflügel 604 on the start grid, at Südfrankreich.]]
The 604 prototype, flown by [[Walter Neubert]], came in sixth in the 1970 [[World Gliding Championships]] held at [[Marfa, Texas]], even though the pilot was lost overnight after a land-out during the competition and missed the following day. A 604 came second in the Open Class at the 1974 World Championships held at [[Waikerie, South Australia]]. The aircraft also set several world records between 1970 and 1974.<ref name="SD" /><ref name="SoaringNov83" />

In 1981 Marion Griffith Jr. flew a 604 for {{convert|645|mi|km|0|abbr=on}} from [[Refugio, Texas]] to [[Liberal, Kansas]] to win the [[Barringer Trophy]], as well as set a US distance to a goal record.<ref name="Soaring Feb 1982">{{cite journal|last=Griffith Jr.|first=Marion|title=645 Miles in a 604|journal=Soaring|date=February 1982|volume=46|issue=2|pages=41–44|accessdate=11 July 2011}}</ref>

In July 2011 five of the eleven 604s built were located in the [[United States]] and registered with the [[Federal Aviation Administration]] in the ''Experimental - Racing/Exhibition'' category.<ref name="FAAReg">{{Cite web|url = http://registry.faa.gov/aircraftinquiry/AcftRef_Results.aspx?Mfrtxt=GLASFLUGEL+&Modeltxt=604&PageNo=1|title = Make / Model Inquiry Results Glasflugal 604|accessdate = 11 July 2011|last = [[Federal Aviation Administration]]|authorlink = |date=July 2011}}</ref>
<!-- ==Aircraft on display== -->

==Specifications (604) ==
{{Aircraft specs
|ref=Sailplane Directory and Soaring<ref name="SD" /><ref name="SoaringNov83" />
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=
|length m=
|length ft=
|length in=
|length note=
|span m=22.0
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=175
|wing area note=
|aspect ratio=29.8:1
|airfoil=Wing root: Wortmann FX 67-K-170 mod, wing tip: Wortmann FX 67-K-150
|empty weight kg=
|empty weight lb=926
|empty weight note=
|gross weight kg=
|gross weight lb=1323
|gross weight note=
|more general=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=155
|never exceed speed kts=
|never exceed speed note=in rough air
|g limits=
|roll rate=
|glide ratio= 49:1 at {{convert|61|mph|km/h|0|abbr=on}}
|sink rate ms=
|sink rate ftmin=98
|sink rate note= at {{convert|45|mph|km/h|0|abbr=on}}
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=7.57
|wing loading note=
|more performance=
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=
*[[Schempp-Hirth Nimbus]]
|lists=
* [[List of gliders]]
}}

==References==
{{reflist}}

==External links==
{{Commons category|Glasflügel 604}}
*[http://www.fluggeil.de/images/h604/g-604gr.jpg Photo of a 604 in flight]
*[http://www.pilotspace.eu/user_docs/601_b_aircraft_0.jpg Photo of a 604 in flight]

{{Glasflügel aircraft}}

{{DEFAULTSORT:Glasflugel 604}}
[[Category:German sailplanes 1970–1979]]
[[Category:Glasflügel aircraft]]