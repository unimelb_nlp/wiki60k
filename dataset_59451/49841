{{ Infobox book
| name          = The Lofty and the Lowly, or Good in All and None All Good
| image         = 
| caption = 
| author        = [[Maria Jane McIntosh]]
| country       = United States
| language      = English
| genre         = [[Anti-Tom literature|Plantation literature]]
| publisher     = [[D. Appleton & Company]]
| release_date  = 1853
| media_type    = Print 
| pages         = c.300
}}

'''''The Lofty and the Lowly, or Good in All and None All Good''''' is a novel by [[Maria Jane McIntosh]] published by [[D. Appleton & Company]] in 1853. It was one of many anti-Tom novels published in response to [[Harriet Beecher Stowe]]'s ''[[Uncle Tom's Cabin]]''. The story is set is Georgia and tells of a plantation owner's efforts to avoid bankruptcy with the help of his loyal slave Daddy Cato. Their efforts are challenged by a northern [[usurer]] and devious northern capitalists. The book sold well across the United States upon release, making it one of the most successful anti-Tom novels in the middle 19th century.

== Overview ==
''The Lofty and the Lowly'' is one of several examples of the pro-slavery [[Anti-Tom literature|plantation literature]] genre that emerged from the Southern United States in response to the [[Abolitionism in the United States|abolitionist]] novel ''[[Uncle Tom's Cabin]]'', which was criticised in the South for its portrayal of the evils of slaveholding. The majority of these "anti-Tom" novels often focused on the benevolence of plantation owners, and the evils of abolitionism and capitalism practised in the Northern United States. McIntosh's novel follows this latter route, although McIntosh claims in the preface of her novel that she is attempting to display a neutral image of slavery in her novel.<ref>http://www.iath.virginia.edu/utc/proslav/prfimjma1t.html</ref>

== Plot ==
The novel takes place along the [[Georgia (U.S. state)|Georgia]] coastline in 1837, where the prosperous Montrose plantation continues to yield a rich harvest of cotton each year, which is gathered by the slaves of the plantation. The elderly owner of the plantation, Colonel Montrose, has died of old age, leaving his son to manage the plantation and tend to his slaves. However, with the onset of the [[Panic of 1837]], Young Montrose faces [[bankruptcy]] unless he is able to maintain the plantation efficiently and keep it working properly. With the aid of his [[Christianization|Christianized]] slave Daddy Cato, Young Montrose sets to work on getting the plantation back up to speed, but his efforts come under the scrutiny of a [[usurer]] named Uriah Goldwire, who is employed by a group of devious capitalists from the North who wish to see the Montrose plantation ruined in order to keep their own pockets filled. Montrose and Cato eventually begin to fight against the efforts of Goldwire to sabotage their work, even going so far as to quell a pro-abolitionist riot intended to force the Montrose slaves into running away from their homes in Georgia to the North.

== Characters ==
*'''Young Montrose''' - The son of Colonel Montrose, who inherits the plantation, slaves and land upon the death of his father.
*'''Daddy Cato''' - A loyal, pious slave of the Montrose plantation who fills the role of [[Uncle Tom]] from ''Uncle Tom's Cabin'', comforting his master in times of stress and despair.
*'''Uriah Goldwire''' - A malevolent Northerner who is recruited as a saboteur by his employees in order to ruin the Montrose plantation and force Montrose into destitution.
*'''Colonel Montrose''' - The kindly, elderly plantation owner who passes away at the beginning of the novel of old age. His military career is not expanded in any great detail in the novel.

== Reception ==
According to the [[University of Virginia]], ''The Lofty and the Lowly'' was a critical success in both north and south upon its original release in 1853. In the opening weeks of publishing, 8,000 copies of the novel were sold in the entire United States.<ref>http://www.iath.virginia.edu/utc/proslav/mcintoshhp.html</ref> This would have made ''The Lofty and the Lowly'' the most critically successful anti-Tom novel since the publication of ''[[Aunt Phillis's Cabin]]'' in 1852, which sold between 20,000 and 30,000 copies for the entire year.<ref>http://www.iath.virginia.edu/utc/proslav/eastmanhp.html</ref>

== Publication history ==
''The Lofty and the Lowly'' was first published in 1853 by [[D. Appleton & Company]]. The novel was one of few anti-Tom novels to be published in separate volumes rather than a single, collected novel.<ref>http://www.iath.virginia.edu/utc/proslav/mcintoshhp.html</ref> D. Appleton & Company. would later publish other anti-Tom novels, including the 1860 novel ''[[The Ebony Idol]]'' by [[G. M. Flanders]].<ref>http://utc.iath.virginia.edu/proslav/flandershp.html</ref>

== References ==
{{reflist}}

== External links ==
*[http://www.iath.virginia.edu/utc/proslav/mcintoshhp.html ''The Lofty and the Lowly'' at the University of Virginia]

{{Uncle Tom's Cabin}}

{{DEFAULTSORT:Lofty And The Lowly}}
[[Category:1853 novels]]
[[Category:Anti-Tom novels]]
[[Category:19th-century American novels]]
[[Category:Novels set in Georgia (U.S. state)]]
[[Category:1837 in fiction]]
[[Category:D. Appleton & Company books]]