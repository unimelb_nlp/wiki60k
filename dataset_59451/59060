{{Infobox Journal
| title        = Nature reviews. Genetics
| cover        = [[File:Nature Reviews Genetics.jpg]]
| editor       =
| discipline   = [[Genetics]]
| language     = [[English language|English]]
| abbreviation = Nat. Rev. Genet.
| publisher    = [[Nature Publishing Group]]
| country      = [[England]]
| frequency    = Monthly
| history      = 2000 to present
| openaccess   =
| impact       =35.898
| impact-year  =2015
| website      =http://www.nature.com/nrg/index.html
| link1        =
| link1-name   =
| link2        =http://www.ncbi.nlm.nih.gov/sites/entrez?Db=nlmcatalog&doptcmdl=Expanded&cmd=search&Term=100962779%5BNlmId%5D
| link2-name   = National Library Of Medicine
| RSS          =
| atom         =
| JSTOR        =
| OCLC         =
| LCCN         =
| CODEN        =
| ISSN         = 1471-0056
| eISSN        = 1471-0064
}}
'''''Nature Reviews Genetics''''' is a monthly review journal in [[genetics]] and covers the full breadth of modern genetics.  The journal publishes [[review]] and perspective articles written by experts in the field subject to [[peer review]] and [[copy editing]] to provide authoritative coverage of topics. Each issue also contains Research Highlight articles – short summaries written by the editors that describe recent research papers. 

Coverage includes: 

*[[Genomics]] ([[genome projects]], [[genome sequencing]], [[bioinformatics]]) 
*Functional genomics (transcript profiling, [[mutant screening|mutant screens]], bioinformatics) 
*Evolutionary genetics (evo-devo, [[comparative genomics]], [[population genetics]], [[phylogenetics]]) 
*Multifactorial genetics (complex disease, disease susceptibility/resistance, association studies, technology) 
*Disease (disease gene identification, relationship between [[genotype]] and [[phenotype]], molecular pathology of genetic disease) 
*[[Chromosome]] biology ([[telomeres]], [[centromere]], [[transposons]], artificial chromosomes, chromosome stability) 
*[[Epigenetics]] ([[DNA methylation]], [[histone]] modification, [[chromatin structure]], [[Imprinting (genetics)|imprinting]], [[chromatin remodeling]]) 
*[[Developmental biology]] ([[reproductive technology]], patterning, [[Cellular differentiation|differentiation]], evo-devo) 
*[[Gene expression]] (DNA elements, LCRs, insulators, [[Enhancer (genetics)|enhancers]], [[Silencer (DNA)|silencers]], broad perspectives on gene regulation)
*Technology (new techniques, experimental strategies, therapy, [[applied genetics]] and genomics, [[computational biology]])  

==External links==
* [http://www.nature.com/nrg/index.html Nature Reviews Genetics website]
* [http://www.nature.com/reviews/index.html Nature Reviews website]
* [http://www.ncbi.nlm.nih.gov/sites/entrez?Db=nlmcatalog&doptcmdl=Expanded&cmd=search&Term=100962779%5BNlmId%5D National Library Of Medicine]

{{Georg von Holtzbrinck Publishing Group}}
{{DEFAULTSORT:NATURE REVIEWS GENETICS}}
{{Sci-journal-stub}}

[[Category:Genetics journals]]
[[Category:Nature Publishing Group academic journals]]
[[Category:Publications established in 2000]]
[[Category:Genetics literature]]
[[Category:Review journals]]