{|{{Infobox Aircraft Begin 
|name =I.C.A.R. 36 Comercial
|image = I.C.A.R.-Comercial.jpg
|caption =
}}{{Infobox Aircraft Type 
|type =Passenger aircraft
|manufacturer =[[Întreprinderea de Construcții Aeronautice Românești]] (ICAR)
| national origin=[[Germany]] & [[Romania]]
|designer = [[Willy Messerschmitt]]
|first flight = [[1934 in aviation|1934]]
|introduced = 1936
|retired =
|status = 
|primary user =[[TAROM|LARES]]
|more users = 
|produced =
|number built =1<ref name=aviatia>{{cite web|last1=Bernád|first1=Dénes|title=Rumania's Aircraft Production. The First Twenty-Five Years|url=http://aviatia.cda.ro/15years.htm|website=Aviatia|accessdate=27 February 2016}}</ref>
|unit cost = 
|variants with their own articles = 
}}
|}

The '''ICAR 36''' / '''ICAR Comercial''' (sic), variously also known as the '''ICAR M 36''', '''Messerschmitt M 36''' or '''BFW M.36''' was a Messerschmitt design built and tested by the Romanian company ICAR in the mid-1930s.  It was a small, single-engine high-wing [[airliner]], the first civil transport aircraft built in Romania.

==Design & development==
In April 1933, [[Erhard Milch]], previously head of [[Deutsche Luft Hansa]] was appointed Secretary of State for Air. Relations between Milch and [[Willy Messerschmitt]] had been bad ever since the cancellation and later re-ordering of the [[Messerschmitt M 20|BFW M.20]] by Luft Hansa, and the future of orders for BFW looked bleak. That summer, a visit was made to Romania where an order was placed by [[Întreprinderea de Construcții Aeronautice Românești]] (ICAR) for the design of a small airliner, to be built by them in Romania. ICAR designated it the ICAR 36; Messerschmitt, working at [[BFW]] referred to it as the M 36.<ref name="Mess36">{{cite book |title= Messerschmitt an aircraft album|last=Smith|first = J Richard|year =1971|publisher = Ian Allan|location = London|isbn=0-7110-0224-X}}</ref>

==Description==
The ICAR 36 was a high-wing cantilever [[monoplane]] of mixed construction, with closed cockpit, single engine, and a fixed [[landing gear]]; a welded steel tube fuselage covered with plywood and tapered single-spar, plywood covered wings. The crew of two sat in the cockpit forward of the wing, which was equipped with a radio and could be fitted with twin controls. The cabin for 6 passengers, with wide rectangular windows and access doors at the rear, was aft and below the cockpit / wings. There were also two baggage compartments.<ref name="Mess36"/>
[[File:I.C.A.R.-Comercial 3v.gif|thumb]]
One aircraft (YR-ACS) was built, later modified with a cabin for five passengers and two luggage compartments. Initially intended to be powered by a licence-built {{convert|450|hp|kW|abbr=on|disp=flip}} [[Gnome & Rhône 7K]] radial engine, the aircraft was eventually fitted with a {{convert|380|hp|kW|abbr=on|disp=flip}} [[Armstrong Siddeley Serval I]] 10-cyl radial engine in a [[NACA cowling]].,<ref name=aviatia/> which was, in turn, replaced with a {{convert|300|hp|kW|abbr=on|disp=flip}} [[Lorraine 7M Mizar]] 47, driving a three-bladed propeller.<ref name="Mess36"/> The fixed landing gear was supported by long [[Aircraft fairing|faired]] vertical struts to the wings, with tear-drop spats over the main-wheels and a tail-wheel at the end of the fuselage.

A tri-motor development was planned, but not realized.

==Operational history==
Only one aircraft was built, operated by [[TAROM|LARES]] on several internal routes including the [[Bucharest]] to [[Cernauti]] route.<ref name="Mess36"/>

==Operators==
;{{flag|Romania}}
*[[TAROM|LARES]]

==Specifications (I.C.A.R. 36 Comercial) ==
{{Aircraft specs
|ref=<ref name="Mess36"/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=2
|capacity=6 / {{convert|960|kg|abbr=on}} payload
|length m=9.8
|length ft=
|length in=
|length note=
|span m=15.4
|span ft=
|span in=
|span note=
|height m=2.8
|height ft=
|height in=
|height note=
|wing area sqm=155
|wing area sqft=
|wing area note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=1320
|empty weight lb=
|empty weight note=
|gross weight kg=2250
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Armstrong Siddeley Serval I]]
|eng1 type=10-cyl. air-cooled radial piston engine
|eng1 kw=<!-- prop engines -->
|eng1 hp=340
|eng1 shp=<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=235
|max speed mph=
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=220
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=89
|stall speed mph=
|stall speed kts=
|stall speed note=
|range km=700
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=4500
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=

|more performance=
}}

== See also ==
{{aircontent|
|related=
*[[Messerschmitt M.18]]
|similar aircraft=<!-- similar or comparable aircraft -->
*[[PWS-24]]bis - [[Lockheed Vega]]
|lists=<!-- related lists -->
|see also=<!-- other relevant information -->
}}

==References==
{{reflist}}

==Further reading==
*{{cite book |last=Gugju |first=Ion |title=Romanian Aeronautical Constructions 1905 - 1974 |location=Brasov |author2=Gheorghe Iacobescu |author3=ovidiu Ionescu }}

==External links==
{{commons category|ICAR Comercial}}
*[http://www.airwar.ru/enc/cw1/icarcomercial.html Photos and drawings at Ugolok Neba site]

{{Messerschmitt aircraft}}
{{ICAR aircraft}}

{{DEFAULTSORT:Icar Comercial}}
[[Category:Romanian airliners 1930–1939]]
[[Category:IAR aircraft|ICAR 36]]