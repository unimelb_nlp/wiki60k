{{Infobox Poker player
|firstname=Joe  
|lastname=Cada
|nickname=jcada99 (online alias), The Kid
|image=JoeCada.jpg
|caption=Joe Cada after winning the [[2009 World Series of Poker]] Main Event.
|hometown=Shelby Charter Township, Michigan
|birth_date = {{birth date and age|1987|11|18}}
|birth_place =
|wsop bracelet count=2
|wsop final tables=5
|wsop money finishes=15
|wsop main event best finish rank=Winner
|wsop main event best finish year=2009
|multi-year wsop winner=
|ept titles=''None''
|ept final tables=''None''
|ept money finishes=1
|last updated={{nobold|2015-10-1}}
}}
'''Joseph "Joe" Cada''' (born November 18, 1987) is an American professional [[poker]] player from [[Shelby Charter Township, Michigan]], best known as the winner of the [[World Series of Poker#Main Event|Main Event]] at the [[2009 World Series of Poker]] (WSOP).

By winning the 6,494-entrant Main Event at the age of 21, Cada surpassed [[Peter Eastgate]] as the youngest champion ever. Cada had two previous WSOP [[in the money (poker)|in the money]] finishes, both in 2009. Cada had been a regular [[online poker]] player for several years prior to winning the live WSOP event. He is primarily an online poker player, with more than $500,000 in online tournament winnings at present.<ref>[http://www.pocketfives.com/profiles/jcada99 Pocketfives.com: Joseph Cada profile]</ref> As of October 2016, his total live tournament winnings exceed $10,460,000.<ref name=JCthm/> Cada became a representative of [[Team PokerStars]] in 2009 in the weeks prior to becoming World Champion. On June 18, 2012, he lost heads up in the 2,811-entrant $1,500 No Limit [[Texas hold 'em]] event at the [[2012 World Series of Poker]].

He is from a family of card enthusiasts, although his parents disapproved of his chosen profession.  His agent dubbed him "The Kid" and he has also taken on the role as a statesman of the profession in the media and political circles, where he is a proponent of legalization of gambling.

== Poker career ==

=== Summary ===
Cada started playing [[online poker]] at about the age of 16. He twice staked accounts, but lost all the money in the accounts that he shared with his brother Jerome.<ref name=Sir/>  His first online account was with [[PartyGaming|PartyPoker]].<ref>{{cite web|url=http://www.bluffmagazine.com/magazine/The-November-Nine-Viewer%27s-Guide-Bluff-Staff-1801.htm|title=The November Nine Viewer's Guide|accessdate=2009-11-11|date=September 2009|work=[[Bluff Magazine]]}}</ref> Although he was not legally able to play in casinos prior to age 21 in the United States, he could in Canada at age 19 and play online.<ref name=Lfarieatp>{{cite web|url=http://docs.newsbank.com/openurl?ctx_ver=z39.88-2004&rft_id=info:sid/iw.newsbank.com:NewsBank:USTB&rft_val_format=info:ofi/fmt:kev:mtx:ctx&rft_dat=12BE5ACAF7B035D8&svc_dat=InfoWeb:aggregated5&req_dat=0D0CB579A3BDA420|title=Long-shot finalists are richer in experience and their pockets|accessdate=2009-11-12|date=2009-11-09|work=[[USA Today]]|author=Mihoces, Gary|page=7C}}</ref>  After a brief sabbatical from the game subsequent to losing his money, he began to play at a [[casino]] in [[Windsor, Ontario]] across the Canada–US border from his Detroit-area home.<ref name=Sir/> He earned enough to enter contests in the Bahamas and Costa Rica.<ref name=Lfarieatp/>

At the time of his WSOP success, he was playing approximately 2,000 [[Glossary of poker terms#hand|hands]] per day online at [[PokerStars]] under the User ID jcada99.<ref name="newsbank">{{cite web|url=http://docs.newsbank.com/openurl?ctx_ver=z39.88-2004&rft_id=info:sid/iw.newsbank.com:NewsBank:SBUB&rft_val_format=info:ofi/fmt:kev:mtx:ctx&rft_dat=12BA636548C8A070&svc_dat=InfoWeb:aggregated5&req_dat=0D0CB579A3BDA420|title=One of a kind – Utica High grad makes way onto poker's biggest stage|accessdate=2009-11-11|date=2009-07-29|work=[[Shelby-Utica News]]|author=Davis, Christian|pages=12A, 14A}}</ref> Cada had been a professional poker player for six years at the time of his world championship.<ref name=JCPNC/> Between the end of 2008 and the 2009 [[November Nine]], Cada had earned $551,788 online.<ref name=GHAH>{{cite web|url=http://docs.newsbank.com/openurl?ctx_ver=z39.88-2004&rft_id=info:sid/iw.newsbank.com:NewsBank:DTNB&rft_val_format=info:ofi/fmt:kev:mtx:ctx&rft_dat=12BC61402F8A6900&svc_dat=InfoWeb:aggregated5&req_dat=0D0CB579A3BDA420|title=Give Him A Hand|accessdate=2009-11-12|date=2009-11-04|work=[[The Detroit News]]|author=Twentyman, Tim|page=1B}}</ref> Prior to the tournament, he had a $150,000 downswing that necessitated him finding a financial backer for the WSOP. Professional poker financiers Eric Haber and [[Cliff Josephy]] paid his entry fee in exchange for half of his winnings.<ref name=GHAH/><ref name=2wWSoP$M/>

Cada enjoys playing the "Sunday Major" tournaments at various online sites and says he honed his skill pursuing the knowledge and experience of better players at online training sites.  He typically hosts many poker playing friends on Sundays to keep each other company while going through the grueling Sundays of playing the various majors.<ref name=TKrtmh>{{cite web|url=http://sports.espn.go.com/espn/poker/columns/story?columnist=feldman_andrew&id=4588287|title="The Kid" ready to make history|accessdate=2010-02-08|date=2009-10-24|publisher=[[ESPN]]|author=Feldman, Andrew}}</ref> Usually, on Sundays, Cada hosts about fifteen friends to play online at his house.<ref name=GHAH/>  According to his ''[[CardPlayer]]'' magazine profile, Cada used several similar aliases to play with various online poker hosts: jcada99 and Joe Cada on the [[Full Tilt Poker]] website, jcada99 on PokerStars, and JCADA99 on [[Absolute Poker]].<ref>{{cite web|url=http://www.cardplayer.com/poker-players/123919-joseph-cada|title=Joseph Cada: Overview|accessdate=2010-02-08|work=[[CardPlayer]]}}</ref>

==World Series of Poker==

{| class="wikitable"
|-
|+ World Series of Poker results
|-
!Year
!Cashes
!Final Tables
!Bracelets
|-
|[[2009 World Series of Poker|2009]]
|3
|1
|1
|-
|[[2011 World Series of Poker|2011]]
|2
|
|
|-
|[[2012 World Series of Poker|2012]]
|3
|1
|
|-
|[[2013 World Series of Poker|2013]]
|3
|2
|
|-
|[[2014 World Series of Poker|2014]]
|1
|1
|1
|-
|[[2015 World Series of Poker|2015]]
|3
|
|
|}

{| class="wikitable"
|-
|+ World Series of Poker bracelets
|-
!Year
!Tournament
!Prize (US$)
|-
|[[2009 World Series of Poker|2009]]
|$10,000 No Limit Hold'em Main Event
|$8,547,044
|-
|[[2014 World Series of Poker|2014]]
|$10,000 No Limit Texas Hold 'em six handed
|$670,041
|}

===2009 World Series of Poker===
At the [[2009 World Series of Poker]], he had three [[glossary of poker terms#I|in the money]] finishes (all in [[Betting (poker)#No limit|No limit]] [[Texas hold 'em]]): 64th in the 1,088-entrant June 5–7 Event 13, $2,500 No Limit Hold'em, which earned him $6,681;<ref>{{cite web|url=http://www.wsop.com/tourney/tournament-results.asp?tid=7238&grid=607|title=40th Annual World Series of Poker No-Limit Holdem (Event 13)|accessdate=2009-11-11|date=2009-06-07|publisher=Harrah's License Company, LLC.}}</ref>  17th in the 2,095-entrant June 16–18 Event 35, $1,500 No Limit Hold'em, which earned him $21,533;<ref>{{cite web|url=http://www.wsop.com/tourney/tournament-results.asp?tid=7259&grid=607|title=40th Annual World Series of Poker No-Limit Holdem (Event 34)|accessdate=2009-11-11|date=2009-06-18|publisher=Harrah's License Company, LLC.}}</ref> and 1st in the 6,494-entrant July 3–15, November 7 and November 9 Event 57 $10,000 World Championship No Limit Hold'em, which earned him $8,546,435.<ref>{{cite web|url=http://www.wsop.com/tourney/tournament-results.asp?tid=7283&grid=607|title=40th Annual World Series of Poker World Championship NL Texas Hold'em (Event 57)-Results |accessdate=2009-11-11|date=2009-11-09|publisher=Harrah's License Company, LLC.}}</ref>

Prior to his final table victory, Cada earned a $1 million contract with PokerStars that pays for all his hotels, travels, and some of his buy-ins.<ref name=GHAH/> His signing with Pokerstars resulted from an interview on [[ESPN]] with [[Phil Gordon]] where he expressed an interest in signing with that specific company.  His agent had already procured him offers from [[UltimateBet]] and [[PokerHost]] as a result of his November Nine qualification.  Cada was represented by [[agent (sports)|agent]] Dan Frank.<ref>{{cite web|url=http://www.pokernewsdaily.com/joe-cada-turns-down-pokerhost-ultimate-bet-to-sign-with-pokerstars-4561/|title=Joe Cada Turns Down PokerHost, Ultimate Bet to Sign with PokerStars|accessdate=2010-02-08|date=2009-08-29|work=[[Poker News Daily]]|author=Cypra, Dan}}</ref>

In the main event, Cada was the tournament chip leader after day 1C,<ref name=JCPPP>{{cite web|url=http://www.pokernewsdaily.com/joe-cada-poker-player-profile-3437/|title=Joe Cada – Poker Player Profile|accessdate=2010-02-08|date=2009-11-05|work=[[Poker News Daily]]|author=Cypra, Dan}}</ref> which was the third of the four opening day sessions, but he began the final table with the fifth largest chipstack.<ref>{{cite web|url=http://www.cardplayer.com/poker-news/7928-wsop-main-event-episode-crowns-winner-tonight-on-espn|title=WSOP Main Event Episode Crowns Winner Tonight on ESPN Tune In at 9 P.M. ET|accessdate=2009-11-11|date=2009-11-10|work=[[Card Player]]|author=Arnett, Kristy}}</ref> In the 122nd final table hand Cada's stack was reduced to 2,275,000—enough for only four big [[blind (poker)|blind]]s and about 1.2% of the combined total stack at play—due to calling [[Jeff Shulman]]'s [[betting (poker)#"All in"|"all in"]] pre-[[flop (poker)|flop]] with {{cards|Ad|Jc}} against  {{cards|Ac|Kh}}.<ref>{{cite web|url=http://www.wsop.com/tourney/updates.asp?tid=7283&grid=607&dayof=1218&curpage=10|title=2009 40th Annual World Series of Poker  – World Championship NL Texas Hold'em (Event 57) |date=2009-11-07|publisher=The official site of the World Series of Poker, WSOP.com|accessdate=2009-11-18}}</ref> No [[community cards]] hit either player, and Shulman was rewarded by his better high card. Cada later went all in twice pre-flop with small pocket pairs and was dominated by higher pocket pairs on both occasions, once with {{cards|3h|3c}} vs. {{cards|Jd|Jc}} and the other with {{cards|2s|2c}} vs {{cards|Qs|Qh}}. Both times Cada flopped the winning three of a kind.<ref>{{cite web|url= http://pokerworks.com/poker-news/2009/11/08/2009-wsop-main-event-down-to-two-cada-and-moon.html|title=2009 WSOP Main Event Final Table: Moon and Cada Set for Final Showdown|accessdate=2010-05-02|date=2009-11-08}}</ref> By the time the field was reduced to two players he had 135 million chips to 58 million for [[Darvin Moon]].<ref name=JCPPP/> During the final heads-up duel, Cada surrendered the chip lead, but he eventually climbed back to 120.1 million before the last hand of the heads-up with Moon.<ref name="last hand">{{cite web|url=http://www.wsop.com/tourney/updates.asp?rr=3&grid=607&tid=7283|title=40th Annual World Series of Poker World Championship NL Texas Hold'em (Event 57)-Tournament Updates |accessdate=2009-11-11|date=2009-11-09|publisher=Harrah's License Company, LLC.}}</ref> He regained the chip lead on the 80th hand.<ref>{{cite web|url=http://sports.espn.go.com/espn/poker/columns/story?columnist=feldman_andrew&id=4643459|title=Joe Cada cashes in|accessdate=2010-02-08|date=2009-11-11|publisher=[[ESPN]]|author=Feldman, Andrew}}</ref> His winning hand was {{cards|9c|9d}}, which he got all-in pre-flop against Moon's {{cards|Qd|Jd}}, which was a classic race situation prior to the flop. The board ran {{cards|8c|2C|7s|Kh|7c}}.<ref name="last hand" />  This hand was the 88th hand of heads-up play between Cada and Moon.<ref>{{cite web|url=http://news.bluffmagazine.com/wsop-joe-cada-wins-2009-wsop-main-event-7954/|title=WSOP: Joe Cada Wins 2009 WSOP Main Event |accessdate=2009-11-11|date=2009-11-10|work=[[Bluff Magazine]]|author=Bradley, Lance}}</ref><ref>{{cite web|url=http://www.cardplayer.com/poker-news/7925-joseph-cada-wins-2009-world-series-of-poker-main-event|title=Joseph Cada Wins 2009 World Series of Poker Main Event: Cada Earns $8,546,435, Moon Takes Home $5,182,601|accessdate=2009-11-11|date=2009-11-10|work=[[Card Player]]|author=Rodriguez, Julio}}</ref>

These three events account for his total cumulative career live event earnings of over $8.5 million.<ref name=JCthm>{{cite web|url=http://pokerdb.thehendonmob.com/player.php?a=s&n=129427|title=Joe Cada|accessdate=2009-11-11|publisher=TheHendonMob.com}}</ref> With the November 2009 victory, which occurred just over a week before his 22nd birthday, Cada supplanted Peter Eastgate, who won at age 22, as the youngest World Series of Poker Main Event champion.<ref name=JCPNC/>  He was 340 days younger than Eastgate had been at the time of becoming world champion.<ref>{{cite web|url=http://docs.newsbank.com/openurl?ctx_ver=z39.88-2004&rft_id=info:sid/iw.newsbank.com:NewsBank:NWDB&rft_val_format=info:ofi/fmt:kev:mtx:ctx&rft_dat=12BDF817DE6E67C0&svc_dat=InfoWeb:aggregated5&req_dat=0D0CB579A3BDA420|title=Briefs|accessdate=2009-11-12|date=2009-11-09|page=A41|work=[[Newsday]]}}</ref>  In the week following the WSOP win, Cada made numerous publicity appearances as a poker ambassador.  His media events included appearances on [[CNN]], [[CBS News]], [[CNBC]], ''[[Late Show with David Letterman]]'', numerous [[ESPN]] outlets including ''[[ESPN First Take|First Take]]'', ''[[The Scott Van Pelt Show]]'', [[ESPNU]], [[ESPNews]], ESPN Inside Deal, and [[ESPN.com]] as well as a taping for ''[[SportsCenter]]'' that was never aired plus a visit to ''[[WWE Raw]]''.  During the publicity run, [[Dennis Phillips (poker player)|Dennis Phillips]] served as Cada's advisor.<ref>{{cite web|url=http://sports.espn.go.com/espn/poker/columns/story?columnist=wise_gary&id=4674907|title=What a week for Joe Cada|accessdate=2010-02-08|date=2009-11-20|publisher=[[ESPN]]|author=Wise, Gary}}</ref>

He has a total of seven cashes in the World Series of Poker, with three in the [[2009 World Series of Poker|2009 event]], two in the [[2011 World Series of Poker|2011 event]], and two in the [[2012 World Series of Poker|2012 event]].<ref name=JCthm/> Aside from his Main Event win, his most notable WSOP finish was second in the 2,811-entrant June 16–18, 2012 $1,500 No-Limit Hold'em Event #31, which earned him $412,424.<ref>{{cite web|url=http://www.wsop.com/tournaments/results.asp?rr=5&grid=887&tid=12131&dayof=|title=43rd Annual World Series of Poker: Saturday, June 16, 2012 to Monday, June 18, 2012: Event #31: No-Limit Hold'em|accessdate=2012-06-19|date=2012-06-18|publisher=[[World Series of Poker]]}}</ref>

== Personal life ==
Anne Cada, Joe's mother, is a [[blackjack]] [[Croupier|card dealer]] at the [[MotorCity Casino]].<ref name=Sir>{{cite web|url=http://docs.newsbank.com/openurl?ctx_ver=z39.88-2004&rft_id=info:sid/iw.newsbank.com:NewsBank:DTNB&rft_val_format=info:ofi/fmt:kev:mtx:ctx&rft_dat=129B6A6FAA0AD910&svc_dat=InfoWeb:aggregated5&req_dat=0D0CB579A3BDA420|title=Striking it rich|accessdate=2009-11-11|date=2009-07-17|work=[[The Detroit News]]|author=Twentyman, Tim|page=1A}}</ref> Cada has an older brother, Jerome.<ref name=Sir/> Cada's father, Jerry, was affected by the [[late-2000s recession]] when his automobile industry job was eliminated in a layoff.<ref name=2wWSoP$M>{{cite web|url=http://docs.newsbank.com/openurl?ctx_ver=z39.88-2004&rft_id=info:sid/iw.newsbank.com:NewsBank:USTB&rft_val_format=info:ofi/fmt:kev:mtx:ctx&rft_dat=12BECC3D8B2A1010&svc_dat=InfoWeb:aggregated5&req_dat=0D0CB579A3BDA420|title=21-year-old wins World Series of Poker, $8.5 million|accessdate=2009-11-12|date=2009-11-10|work=[[USA TODAY]]|author=Mihoces, Gary|page=1A}}</ref> During the 2009 World Series of Poker's [[November Nine]], dozens of Cada's fans wore T-shirts with [[Michigan Wolverines]] team colors ([[Maize (color)|maize]] and blue) with the words, "PokerStars Michigan Joe Cada 'The Kid'" emblazoned across the front and Michigan baseball caps with "The Kid" on the back.<ref name=Lfarieatp/><ref>{{cite web|url=http://www.bluffmagazine.com/videos/index.asp?curPage=1&videocatid=0&videoid=1123|title= Joseph Cada Fans: Lacey finds a few of the Joseph Cada Fans before the heads up gets started |accessdate=2009-11-11|date=2009-11-07|work=[[Bluff Magazine]]}}</ref>  His agent came up with the nickname "The Kid".<ref name=TKrtmh/> In an interview in ''[[Time (magazine)|Time]]'', Cada estimates he had about 100 friends in his cheering section.<ref name=JCPNC>{{cite news|url=http://www.time.com/time/world/article/0,8599,1937413,00.html|title=Joe Cada, Poker's New Champion|accessdate=2009-11-12|date=2009-11-11|work=[[Time (magazine)|Time]]|author=Villano, Matt}}</ref>

In the ''Time'' interview, Cada expressed his thoughts on legislation related to the legality of gambling: "I support the right to play poker online. Poker isn't gambling. It's a hobby, an activity, a game. It's not about luck—it's about logic, decision-making, math. We all should be able to play poker on the Web if we want to, and I believe that making it illegal strips us of our rights. This is an important issue, and hopefully we'll see it resolved soon."<ref name=JCPNC/> Outside of poker, Cada also plays [[indoor soccer]].<ref>{{cite web|url=http://docs.newsbank.com/openurl?ctx_ver=z39.88-2004&rft_id=info:sid/iw.newsbank.com:NewsBank:USTB&rft_val_format=info:ofi/fmt:kev:mtx:ctx&rft_dat=12BECC3D8A261D30&svc_dat=InfoWeb:aggregated5&req_dat=0D0CB579A3BDA420|title=Family, friends in awe of Mich. poker phenom|accessdate=2009-11-12|date=2009-11-10|publisher=[[USA TODAY]]|author=Wilkins, Korie and Tammy Stables Battaglia}}</ref> {{Asof|2009}}, he was considering purchasing a second home in [[Las Vegas Valley|Las Vegas]], [[Nevada]] and possibly opening a bar.<ref name=GHAH/>

== References ==
{{reflist|35em}}

== External links ==
* [http://www.cardplayer.com/poker-players/123919-joseph-cada Joseph Cada at Cardplayer.com – player profile]
* [http://www.wsop.com/players/playerprofile.asp?playerid=61471 Joseph Cada at WSOP.com – player profile]
* [http://www.bluffmagazine.com/players/joe-cada/61471/player-profile.asp Joseph Cada – player profile] at ''[[Bluff Magazine]]''
* [http://pokerdb.thehendonmob.com/player.php?a=r&n=129427 Joseph Cada at Hendon Mob – player profile]

{{WSOP Main Event champions}}
{{2000s WSOP Bracelet Winners}}
{{2010s WSOP Bracelet Winners}}
{{Team PokerStars}}

{{Good article}}

{{DEFAULTSORT:Cada, Joseph}}
[[Category:1987 births]]
[[Category:American poker players]]
[[Category:Living people]]
[[Category:People from Macomb County, Michigan]]
[[Category:World Series of Poker bracelet winners]]
[[Category:World Series of Poker Main Event winners]]