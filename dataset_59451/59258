{{Infobox journal
| title = Optometry and Vision Science
| cover = [[File:2013 cover Optom Vis Sci.jpg]]
| editor = Michael D. Twa
| discipline = [[Optometry]], [[vision science]]
| former_names = American Journal of Optometry and Physiological Optics, American Journal of Optometry and Archives of American Academy of Optometry, American Journal of Optometry
| abbreviation = Optom. Vis. Sci.
| publisher = [[Lippincott Williams & Wilkins]]
| country =
| frequency = Monthly
| history = 1924-present
| openaccess =
| license =
| impact = 1.895
| impact-year = 2012
| website = http://journals.lww.com/optvissci/pages/default.aspx
| link1 = http://journals.lww.com/optvissci/pages/currenttoc.aspx
| link1-name = Online access
| link2 = http://journals.lww.com/optvissci/pages/issuelist.aspx
| link2-name = Online archive
| JSTOR =
| OCLC =
| LCCN =
| CODEN = OVSCET
| ISSN = 1040-5488
| eISSN = 1538-9235
}}
'''''Optometry and Vision Science''''' is a monthly [[Peer review|peer-reviewed]] [[medical journal]] published by [[Lippincott Williams & Wilkins]] on behalf of the [[American Academy of Optometry]]. The journal was established in 1924 as the ''American Journal of Optometry''. It was renamed ''American Journal of Optometry and Archives of American Academy of Optometry'' in 1941, then ''American Journal of Optometry and Physiological Optics'' in 1974, before obtaining its current title in 1989. The [[editor-in-chief]] is [[Michael D. Twa]] ([[University of Alabama at Birmingham]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Index Medicus]]/[[MEDLINE]]/[[PubMed]],<ref name=PubMed>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/8904931 |title=Optometry and Vision Science |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |format= |accessdate=2013-10-31}}</ref> the [[Science Citation Index]], and [[Current Contents]]/Clinical Medicine.<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2013-10-31}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 1.895.<ref name=WoS>{{cite book |year=2013 |chapter=Optometry and Vision Science |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://journals.lww.com/optvissci/pages/default.aspx}}

[[Category:Ophthalmology journals]]
[[Category:Publications established in 1924]]
[[Category:Lippincott Williams & Wilkins academic journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]