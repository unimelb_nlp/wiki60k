{{Use dmy dates|date=March 2015}}
{{Use British English|date=March 2015}}
{{good article}}
{{Infobox football match
|                   title = 1933 FA Cup Final
|                   image = [[File:Old Wembley Stadium (external view).jpg|200px]]
|                   event = [[1932–33 FA Cup]]
|                   team1 = [[Everton F.C.|Everton]]
|        team1association = 
|              team1score = 3
|                   team2 = [[Manchester City F.C.|Manchester City]]
|        team2association = 
|              team2score = 0
|                 details = 
|                    date = 29 April 1933
|                 stadium = [[Wembley Stadium (1923)|Wembley Stadium]]
|                    city = [[London]]
|      man_of_the_match1a = 
| man_of_the_match1atitle = 
|      man_of_the_match1b = 
| man_of_the_match1btitle = 
|                 referee = E. Wood (Sheffield)
|              attendance = 120,950
|                 weather = 
|                previous = [[1932 FA Cup Final|1932]]
|                    next = [[1934 FA Cup Final|1934]]
}}
The '''1933 FA Cup Final''' was a [[association football|football]] match between [[Everton F.C.|Everton]] and [[Manchester City F.C.|Manchester City]] on 29 April 1933 at [[Wembley Stadium (1923)|Wembley Stadium]] in [[London]]. The deciding match of English football's primary cup competition, the Football Association Challenge Cup (better known as the [[FA Cup]]), it was the 62nd final, and the 11th at Wembley. The 1933 final was the first where the players, including goalkeepers, were issued numbers for identification. Everton were allocated numbers 1–11 and Manchester City numbers 12–22.

Each team progressed through five rounds to reach the final. Everton won 3–0, with goals from [[Jimmy Stein]], [[Dixie Dean]] and [[James Dunn (Scottish footballer)|James Dunn]], and won the cup for the first time since 1906.

==Route to the final==

===Everton===
{| class="wikitable" style="text-align:center;margin-left:1em;float:right"
!width="25" | Round
!width="150" | Opposition
!width="50" | Score
|-
| 3rd
| [[Leicester City F.C.|Leicester City]] (a)
| 3–2
|-
| 4th
| [[Bury F.C.|Bury]] (h)
| 2–0
|-
|5th
| [[Leeds United F.C.|Leeds United]] (h)
| 4–2
|-
| 6th
| [[Luton Town F.C.|Luton Town]] (h)
| 6–0
|-
| Semi-final
| [[West Ham United F.C.|West Ham United]] ([[Molineux Stadium|n]])
| 2–1
|}
Both teams entered the competition in the third round, the entry point for First Division clubs. Everton were drawn to play [[Leicester City F.C.|Leicester City]] at [[Filbert Street]], an all First Division tie. The match was close; [[Dixie Dean]] scored for Everton after three minutes, but Leicester quickly levelled the score. A goal by [[Jimmy Stein]] gave Everton a 2–1 half-time lead, but Leicester again equalised. [[James Dunn (Scottish footballer)|James Dunn]] eventually scored to secure a 3–2 win for Everton.<ref>{{cite news|author= JAH Catton|title=Amazing Cup-ties: The Arsenal Put Out|work=The Observer|date=1933-01-15|page=24}}</ref> Second Division [[Bury F.C.|Bury]] provided the opposition in the fourth round. [[Tommy Johnson (footballer born 1900)|Tommy Johnson]] scored twice for Everton in the opening half-hour. From that point, though Bury enjoyed significant spells of possession, Everton thwarted their efforts by preying on Bury mistakes. In the second half Dean added a third goal from a rebounded [[Cliff Britton]] free-kick, and Bury scored a late consolation goal.<ref>{{cite news|title=Bury's Skill at Half-back Wasted: Failure of Inside Forwards Gives Everton an Easy Victory|work=The Manchester Guardian|date=1933-01-30|page=3}}</ref> Everton were drawn at to play [[Leeds United A.F.C.|Leeds United]] at home in the fifth round. Leeds' strong league form meant Everton entered the match as slight underdogs despite home advantage.<ref name="Guardian5th">{{cite news|title=FA Cup Fifth Round Matches: Lancashire's Three in the Last Eight, Everton Beat Leeds|work=The Manchester Guardian|date=1933-02-20|page=3}}</ref> Everton goalkeeper [[Ted Sagar]] made two important saves in the first half to deny [[Arthur Hydes]] and [[Billy Furness]].<ref name="Guardian5th"/> Everton the gained the upper hand and scored twice, Dean with the first, and Stein with the second, direct from a corner.<ref name="Observer5th">{{cite news|author= JAH Catton|title=Association Football: Masterly Everton|work=The Observer|date=1933-02-19|page=24}}</ref>

Against Third Division [[Luton Town F.C.|Luton Town]] in the quarter-final, Everton won comfortably. The match remained scoreless for half an hour, but after Stein opened the scoring for Everton the match became one-sided, and ended 6–0. Stein and Johnson both scored twice, along with a goal each for Dunn and Dean, the latter maintaining his record of scoring in every round.<ref name="Observer6th">{{cite news|author= JAH Catton|title=Association Football: The Fight For The Cup|work=The Observer|date=1933-03-05|page=26}}</ref> By this time Everton were viewed as favourites to win the competition.<ref name="Guardian6th">{{cite news|title=FA Cup Semi-finalists|work=The Manchester Guardian|date=1933-03-06|page=3}}</ref> In the semi-final they played [[West Ham United F.C.|West Ham]] at [[Molineux Stadium|Molineux]], [[Wolverhampton]]. Everton took the lead in the sixth minute. A corner kick by Stein was headed on by Johnson, and then headed into the net by Dunn.<ref name="GuardianEvertonsemi">{{cite news|title=Everton's victory over West Ham|work=The Manchester Guardian|date=1933-03-20|page=3}}</ref> Everton had the better of the play in the first half, but [[Vic Watson]] scored for West Ham just before half-time. In the second half West Ham's Woods missed an open goal from six yards (5.5m).<ref name="GuardianEvertonsemi"/> Everton then capitalised on their reprieve. With seven minutes remaining, a mistake by [[Jim Barrett, Sr.|Jim Barrett]] allowed [[Edward Critchley]] to go clear on goal and score the winner.<ref name="Observersemi">{{cite news|author= JAH Catton|title=Association Football: A Lancashire Final|work=The Observer|date=1933-03-19|page=29}}</ref>

===Manchester City===
{| class="wikitable" style="text-align:center;margin-left:1em;float:right"
!width="25" | Round
!width="150" | Opposition
!width="50" | Score
|-
| rowspan="2" | 3rd
| [[Gateshead A.F.C.|Gateshead]] (a)
| 1–1
|-
| Gateshead (h)
| 9–0
|-
| 4th
| [[Walsall F.C.|Walsall]] (h)
| 2–0
|-
|5th
| [[Bolton Wanderers F.C.|Bolton Wanderers]] (a)
| 4–2
|-
| 6th
| [[Burnley F.C.|Burnley]] (a)
| 1–0
|-
| Semi-final
| [[Derby County F.C.|Derby County]] ([[Leeds Road|n]])
| 3–2
|}
Manchester City started the competition at Third Division [[Gateshead A.F.C.|Gateshead]]. Despite the disparity in league positions, a heavy pitch made for an even game, which finished 1–1.<ref>Ward, ''The Manchester City Story'', p. 31.</ref> The replay at [[Maine Road]] was one-sided. A 9–0 Manchester City win featured six different scorers, including a hat-trick from [[Fred Tilson]].<ref>Maddox, Saffer and Robinson, ''Manchester City Cup Kings 1956'', p. 13.</ref> In the fourth round Manchester City faced another Third Division side, [[Walsall F.C.|Walsall]], who had provided the surprise result of the third round by defeating league leaders [[Arsenal F.C.|Arsenal]].<ref>{{cite news|title=Arsenal's Conquerors Visit Manchester City: Bury Visit Everton|work=The Manchester Guardian|date=1933-01-28|page=6}}</ref> Brook scored both goals in a 2–0 win, in which Walsall's Reed was [[sent off]] for a foul on Brook.<ref>{{cite news|author= JAH Catton|title=Topsy-turvy Cup-ties. Many Stalwarts Fallen|work=The Observer|date=1933-01-29|page=22}}</ref> The fifth round brought a short trip to [[Bolton Wanderers F.C.|Bolton Wanderers]], where the attendance of 69,920 was the highest of the round.<ref name="Guardian5th"/> Bolton took the lead, but Brook scored twice in quick succession to give Manchester City the advantage at the interval.<ref name="Observer5th"/> Bolton equalised when a gust of wind caught [[Raymond Westwood]]'s corner. Brook completed a [[hat trick]] with a [[Penalty kick (association football)|penalty]] to regain the lead, and in the closing minutes Tilson completed a 4–2 victory.<ref name="Observer5th"/> The ''Manchester Guardian'' suggested Brook's "magnificent display" made him a contender for an [[England national football team|England]] call-up.<ref name="Guardian5th"/>

Manchester City's quarter-final was against [[Burnley F.C.|Burnley]] of the Second Division. City took the lead early in the match following a solo goal by Tilson. In the second-half Burnley discarded their passing game in favour of a direct approach, and pressured the Manchester City goal. The City defence stood firm, and the match finished 1–0.<ref name="Observer6th"/> City's opponents for the semi-final, held at [[Leeds Road]], [[Huddersfield]], were [[Derby County F.C.|Derby County]]. Derby had two chances to score in the first half, but both were missed.<ref name="GuardianCitysemi">{{cite news|title=Manchester City's Splendid Form|work=The Manchester Guardian|date=1933-03-20|page=3}}</ref> A Manchester City counter-attack produced the opening goal, when Brook crossed and Toseland headed in.<ref name="Observersemi"/> By midway through the second half Manchester City led by three goals. The second was scored by Tilson, a follow-up after an initial saved shot.<ref name="GuardianCitysemi"/> McMullan scored the third after dribbling through the Derby defence.<ref name="GuardianCitysemi"/> Derby mounted a late comeback. A goal by [[Howard Fabian]] reduced the deficit to two, and [[Sammy Crooks]] added a late second for Derby, but it was too late to affect the result of the match, which ended 3–2.<ref name="GuardianCitysemi"/>

==Build-up==
Everton had contested the final on four previous occasions. They beat [[Newcastle United F.C.|Newcastle United]] 1–0 to win the Cup in [[1906 FA Cup Final|1906]], but were defeated in the [[1893 FA Cup Final|1893]], [[1897 FA Cup Final|1897]] and [[1907 FA Cup Final|1907]] finals. The 1933 final was Manchester City's third. Both their previous finals were against [[Bolton Wanderers F.C.|Bolton Wanderers]]. Manchester City won by a goal to nil in [[1904 FA Cup Final|1904]], and lost by the same scoreline in [[1926 FA Cup Final|1926]]. Both teams had performed well in the previous season. Manchester City reached the semi-finals of the 1932 FA Cup; Everton were reigning league champions.<ref>{{cite book |last=Pawson |first=Tony |title=100 Years of the FA Cup |publisher=Heinemann |location=London |year=1972 |isbn=0-330-23274-6|page=145}}</ref> The clubs had never previously met in cup competition. The league matches between the two earlier in the season each finished as a win for the home team.<ref name="graun preview">{{cite news|title=To-day's Cup Final – Everton's Advantage in Skill, Manchester City's in Temperament|work=The Guardian|date=1933-04-29|page=7}}</ref> At the time of the final, Everton's league position was tenth, and Manchester City's sixteenth.<ref>{{cite web |url=http://www.statto.com/football/stats/england/division-one-old/1932-1933/table/1933-04-26 |title=English Division One (old) 1932–1933 : Table on 27.04.1933 |publisher=Statto Organisation |accessdate=2011-09-26}}</ref> Newspapers did not declare a clear favourite for the win. Everton were viewed as having the more skilful players, particularly their forwards, whereas Manchester City were seen as having greater strength and determination.<ref name="graun preview"/><ref name="Timespreview"/>

Everton spent the week before the match in the spa town of [[Buxton]], and travelled to [[Dorking]] on the eve of the match. Manchester City spent the week in [[Bushey]].<ref name="Timespreview"/> Everton's James Dunn received treatment on a thigh injury in the ten days preceding the game, but was anticipated to be fit enough to play.<ref>{{cite news|title=Dunn and the Cup Final|work=The Scotsman|date=1933-04-20|page=14}}</ref> Manchester City's main injury worry was Fred Tilson, who was troubled by a leg injury.<ref name="Timespreview"/> Dunn was passed fit well before the game, allowing Everton to field the same line-up that played in four of their five previous cup ties.<ref>{{cite news|title=Everton to play full team|work=The Guardian|date=1933-04-28|page=3}}</ref>

Ten miles (16&nbsp;km) of barbed wire was used to secure Wembley Stadium against unauthorised entry.<ref>{{cite news|title=Preparing for the Cup Final|work=The Guardian|date=1933-04-25|page=4}}</ref> The pre-match entertainment was music by the [[Band of the Irish Guards]], and communal singing backed by the band of the [[Royal Horse Guards]].<ref name="Times29Apr">{{cite news|title=The Cup Final: To-day's atch At Wembley|work=The Times|date=1933-04-29|page=6}}</ref> Inclement weather prevented the attendance of [[George V of the United Kingdom|King George V]].<ref>{{cite news|title=93,000 See Everton Win The Cup|work=The Observer|date=1933-04-30|page=17}}</ref> Instead the guest of honour was the [[George VI of the United Kingdom|Duke of York]]. Other guests present included [[Clive Wigram, 1st Baron Wigram|Baron Wigram]], [[Lionel Halsey|Admiral Sir Lionel Halsey]], Austrian envoy [[Georg von und zu Franckenstein|Baron von Franckenstein]] and the [[West Indies cricket team]].<ref name="Times29Apr"/>

The Manchester City line-up contained two survivors from the 1926 team, [[Sam Cowan]] and [[Jimmy McMullan]]. The only Everton player with cup final experience was [[Tommy Johnson (footballer born 1900)|Tommy Johnson]], who also played for Manchester City in the 1926 final. He represented the Manchester club between 1919 and 1930, and at the time of the 1933 final was Manchester City's all-time highest goalscorer.

Both teams usually wore blue, causing a colour clash. The competition rules required both teams to wear alternative colours.<ref>{{cite news|title=Cup Final Colour Clash|work=The Scotsman|date=1933-04-06|page=15}}</ref> For the first time in a cup final, the players wore numbered shirts.<ref>{{cite book | title= The F.A. Cup – The Complete Story|last= Lloyd |first= Guy |author2= Holt, Nick |year= 2005|publisher= Aurum Press|isbn=1-84513-054-5|pages=129–130}}</ref> Everton were numbered 1–11, and Manchester City 12–22. Everton goalkeeper Sagar wore 1, with the forwards bearing the higher numbers. Manchester City were the reverse. Forward Brook wore 12, through to goalkeeper Langford who wore 22.<ref name="Timespreview">{{cite news|title=F.A. Cup Final: Preparations at Wembley|work=The Times|date=1933-04-27|page=6}}</ref><ref>{{cite book |last=Ward |first=Andrew |title=The Manchester City Story |publisher=Breedon|location=Derby |year=1984 |isbn=0-907969-05-4|page=32}}</ref>

==Match==
Each team played the formation typical of the era: two full-backs, three half-backs and five forwards. With Tilson absent from the Manchester City line-up, [[Alec Herd]] moved across to Tilson's usual centre-forward position, and [[Bobby Marshall (footballer, born 1903)|Bobby Marshall]] was selected at inside-right.<ref name="Scotsmanreport">{{cite news|title=English Cup final: Manchester City Outclassed By Everton|work=The Scotsman|date=1933-05-01|page=6}}</ref>

Manchester City had the first attack of the match, but it came to nothing.<ref name="Guardianreport">{{cite news|title=Everton's Easy Cup Victory|work=Manchester Guardian|date=1933-05-01|page=3}}</ref> Soon Everton began to dominate the match, with Dean frequently involved in the attacking play.<ref name="Observerreport">{{cite news| author= JAH Catton | title=Everton Win the Cup: An Emphatic Victory|work=The Observer|date=1933-04-30|page=28}}</ref> Several Everton attacks came on their left flank. Stein caused Manchester City right-back [[Sid Cann]] problems,<ref>{{cite news| author= JAH Catton | title=Everton Win the Cup: An Emphatic Victory|work=The Observer|date=1933-04-30|page=28 |quote= [Stein] was frequently plied with the ball because it was patent the {{sic|?|the|reason=perhaps "that the"}} right-back could not cope with this spirited and wily Scot}}</ref> and Cann was forced to concede a corner kick on several occasions.<ref name="Guardianreport"/> Just after the half-hour Everton had their first shot on target, when Stein's effort was saved by Langford.<ref name="Guardianreport"/> Another chance quickly arrived. Stein's cross passed in front of goal, but Dean was unable to connect with the ball.<ref name="Observerreport"/> Two minutes later Manchester City goalkeeper Langford attempted to catch a cross from Britton, but dropped the ball under pressure from Dean. The ball fell into the path of Stein, who put the ball into the empty net to give Everton the lead.<ref name="Guardianreport"/><ref name="Observerreport"/><ref name="ck1956">{{cite book |last=Maddox |first=John |author2=Saffer, David|author3= Robinson, Peter |title=Manchester City Cup Kings 1956 |publisher=Over the Moon|location=Liverpool |year=1999 |isbn=1-872568-66-1|page=15}}</ref> At half-time Everton led 1–0.

Everton continued to control the game in the second half. Manchester City took shots from long range, but none required Sagar to make a save.<ref name="Scotsmanreport"/> Seven minutes into the second half, Langford again failed to catch a Britton cross, and Dean charged to the net. Dean, ball and goalkeeper all landed in the goal, making the score 2–0.<ref>{{cite news| author= JAH Catton | title=Everton Win the Cup: An Emphatic Victory|work=The Observer|date=1933-04-30|page=28 |quote= Langford jumped to handle. The ball seemed under the crossbar, but Dean to make sure bumped the keeper and the ball into the net.}}</ref><ref>{{cite news|title=Everton's Easy Cup Victory|work=Manchester Guardian|date=1933-05-01|page=3 |quote= Langford waited a fraction of a second too long to catch Britton's bar-high centre, and before he knew what had happened he was lying on the ground and Dean and the ball were inside the net.}}</ref> Manchester City then made a few fruitless attacks. As was the case throughout the game, the Everton defence outplayed the Manchester City forwards. The ''Manchester Guardian'' singled out [[Warney Cresswell]] for particular praise, describing his performance as "an almost perfect display".<ref name="Guardianreport"/> Ten minutes from time a Dunn header from a corner made the score 3–0 to Everton.<ref name="Scotsmanreport"/> Just before the end Everton's Johnson had a chance to make it 4–0, but the referee blew his whistle for full-time before Johnson could take his shot.<ref name="Guardianreport"/>

==Match details==
{{football box
|date=29 April 1933
|time=15:00 [[Western European Summer Time|BST]]
|team1=[[Everton F.C.|Everton]]
|score=3–0<ref>{{cite web|url=http://www.fa-cupfinals.co.uk/1933.htm |title=FA Cup Final 1933 |work=fa-cupfinals.co.uk |accessdate=2011-10-04 |deadurl=yes |archiveurl=https://web.archive.org/web/20071029212158/http://www.fa-cupfinals.co.uk/1933.htm |archivedate=29 October 2007 }}</ref>

|team2=[[Manchester City F.C.|Manchester City]]
|goals1=[[Jimmy Stein|Stein]] {{goal|41}}<br />[[Dixie Dean|Dean]] {{goal|52}}<br />[[James Dunn (Scottish footballer)|Dunn]] {{goal|80}}
|goals2=
|stadium=[[Wembley Stadium (1923)|Wembley Stadium]], [[London]]
|attendance=92,950
|referee=[[Eddie Wood (referee)|Eddie Wood]] }}

{| width=92% |
|-
|{{Football kit
 | pattern_la =
 | pattern_b  = 
 | pattern_ra =
 | pattern_so = _2bluestripes
 | leftarm    = FFFFFF
 | body       = FFFFFF
 | rightarm   = FFFFFF
 | shorts     = 000000
 | socks      = 000000
 | title      = Everton
}}
|{{Football kit
 | pattern_la = 
 | pattern_b  = _collarwhite
 | pattern_ra = 
 | leftarm    = 802131
 | body       = 802131
 | rightarm   = 802131
 | shorts     = FFFFFF
 | socks      = 802131
 | title      = Manchester City
}}
|}

{| width="100%"
|valign="top" width="50%"|
{| style="font-size: 90%" cellspacing="0" cellpadding="0"
|colspan="4"|
|-
!width="25"| !!width="25"|
|-
| ||'''1''' ||{{flagicon|ENG}} [[Ted Sagar]] 
|-
| ||'''2''' ||{{flagicon|NIR}} [[Billy Cook (Northern Irish footballer)|Billy Cook]]
|-
| ||'''3''' ||{{flagicon|ENG}} [[Warney Cresswell]]
|-
| ||'''4''' ||{{flagicon|ENG}} [[Cliff Britton]]
|-
| ||'''5''' ||{{flagicon|ENG}} [[Tommy White (footballer born 1908)|Tommy White]]
|-
| ||'''6''' ||{{flagicon|SCO}} [[Jock Thomson]]
|-
| ||'''7''' ||{{flagicon|ENG}} [[Albert Geldard]]
|-
| ||'''8''' ||{{flagicon|SCO}} [[James Dunn (Scottish footballer)|James Dunn]]
|-
| ||'''9''' ||{{flagicon|ENG}} [[Dixie Dean]] ([[Captain (association football)|c]]) 
|-
| ||'''10'''||{{flagicon|ENG}} [[Tommy Johnson (striker)|Tommy Johnson]]
|-
| ||'''11'''||{{flagicon|SCO}} [[Jimmy Stein]]
|-
|colspan=4|'''Manager:'''
|-
|colspan="4"|None – team selection made in consultation with Dean<ref>{{cite web|url= http://www.evertonfc.com/history/fa-cup-final-1933.html |title= FA Cup Final 1933 |publisher=Everton FC |accessdate=2011-10-04}}</ref>
|}
|valign="top" width="50%"|
{| style="font-size: 90%" cellspacing="0" cellpadding="0" align=center
|colspan="4"|
|-
!width="25"| !!width="25"|
|-
| ||'''22'''||{{flagicon|ENG}} [[Len Langford]]
|-
| ||'''21'''||{{flagicon|ENG}} [[Sid Cann]]
|-
| ||'''20'''||{{flagicon|ENG}} [[Bill Dale (footballer)|Bill Dale]]
|-
| ||'''19'''||{{flagicon|SCO}} [[Matt Busby]]
|-
| ||'''18'''||{{flagicon|ENG}} [[Sam Cowan]]([[Captain (association football)|c]]) 
|-
| ||'''17'''||{{flagicon|ENG}} [[Jackie Bray]]
|-
| ||'''16'''||{{flagicon|ENG}} [[Ernie Toseland]]
|-
| ||'''15'''||{{flagicon|ENG}} [[Bobby Marshall (footballer, born 1903)|Bobby Marshall]]
|-
| ||'''14'''||{{flagicon|SCO}} [[Alec Herd]] 
|-
| ||'''13'''||{{flagicon|SCO}} [[Jimmy McMullan]]
|-
| ||'''12'''||{{flagicon|ENG}} [[Eric Brook]]
|-
|colspan=4|'''Manager:'''
|-
|colspan="4"|{{flagicon|ENG}} [[Wilf Wild]]
|}
|}
{| width=100% style="font-size: 90%"
| width=50% valign=top|
|}

==Post-match==
Everton captain Dixie Dean led his team to the Royal Box and received the cup from the [[George VI|Duke of York]].<ref>{{cite book |last=James |first=Gary |title=Manchester: The Greatest City (1919–1937) (Kindle Edition) |publisher=James Ward |location=Halifax |year=2011 |at=location 1241}}</ref> Everton returned to Liverpool on the Monday evening, and paraded the city in the same horse-drawn carriage used in the celebrations of their previous cup win in 1906.<ref name="parade">{{cite news |title=Cup Winners' Tour in Four-in-hand |work=Manchester Guardian |date=1933-05-01 |page=13 }}</ref> The players attended a reception at the town hall, where large crowds greeted them.<ref>{{cite news |title=The Cup Reaches Liverpool|work=Manchester Guardian |date=1933-05-02 |page=17 }}</ref> After the reception the cup was taken to Goodison Park for public viewing.<ref name="parade"/>

Newsreels of the final featured post-match toasts by the two captains. First Dixie Dean, raising his glass, said "Here's to Lancashire, and may the cup stay in Lancashire. If Everton don't win it, may another Lancashire club win it." Cowan replied "I hope the next Lancashire club that wins it is Manchester City, my club".<ref>{{cite video
 | date        = 2003 | title       = A History of Football | type      = DVD | publisher   = Marks and Spencer}}</ref> The [[1934 FA Cup Final|following year's final]] made the captains' remarks look perceptive. Cowan and his Manchester City team returned, and beat [[Portsmouth F.C.|Portsmouth]] 2–1 to win the 1934 cup.<ref>{{cite book |last=James |first=Gary |title=Manchester City – The Complete Record |publisher=Breedon |location=Derby |year=2006 |isbn=1-85983-512-0|page= 46.}}</ref> Both Manchester City and Everton also went on to win the league championship later in the decade; Manchester City in 1937, and Everton in 1939.

==References==
{{reflist|colwidth=30em}}

==External links==
*[https://web.archive.org/web/20071029212158/http://www.fa-cupfinals.co.uk/1933.htm Match report at www.fa-cupfinals.co.uk]
*[https://www.youtube.com/watch?v=k1BTYQSa8WA Match Highlights]

{{FA Cup Finals}}
{{Everton F.C. matches}}
{{Manchester City F.C. matches}}
{{1932–33 in English football}}

[[Category:FA Cup Finals|1933]]
[[Category:1932–33 in English football|FA Cup Final]]
[[Category:Everton F.C. matches|FA Cup Final 1933]]
[[Category:Manchester City F.C. matches|FA Cup Final 1933]]
[[Category:April 1933 sports events]]