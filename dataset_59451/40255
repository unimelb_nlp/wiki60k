{{Essay|date=January 2012}}
'''Childbirth in rural Appalachia''' has long been a subject of concern. [[Infant mortality]] rates are higher in [[Appalachia]] than in other parts of the [[United States]]. Additionally, poor health in utero, at birth, and in childhood can contribute to poor health throughout life. The region's low income, geographic isolation, and low levels of educational attainment reduce both access to and utilization of modern medical care. [[Traditional medicine|Traditional medical practices]], including [[lay midwife]]ry, persisted longer in Appalachia than in other U.S. regions.

==Health statistics==
The Appalachian region has higher overall mortality rates than the United States as a whole. Factors considered to contribute to the region's poor health outcomes include low income, geographic isolation, and low levels of educational attainment.<ref>{{cite journal|journal=Preventing Chronic Disease|issue=2006 October; 3(4): A113 |year=2006|title=Appalachia: Where Place Matters in Health |first1=Bruce |last1=Behringer|first2=Gilbert H |last2=Friedell |pmc=1779277 |pmid=16978488 |volume=3 |page=A113}}</ref><ref>{{cite book|title=Appalachian Legacy: Economic Opportunity After the War on Poverty |editor=Ziliak, James P.|chapter=Socioeconomic Status, Child Health, and Future Outcomes: Lessons for Appalachia |year=2012 |publisher=Brookings Institution Press |first1=Janet |last1=Currie |first2=Mariesa |last2=Herrmann |pages=109–148 |isbn=9780815722144}}</ref>

Statistics compiled in 1916 indicated that women in southern Appalachia were less likely to die from causes related to childbirth than women in the rest of the U.S., but their babies were less likely to survive their first year. The [[maternal death]] rate in southern Appalachia was 12.7 per 100,000 births, compared with rates of 15.1 per 100,000 for the entire rural United States and 16.3 per 100,000 in the nation as a whole. Nationally, the rate of infant mortality was 211.1 per 100,000, but it was higher in five of six southern Appalachian states (all except Tennessee). Infant mortality accounted for one-fourth to one-fifth of all deaths in Kentucky, Maryland, and North Carolina, compared with one-sixth of all deaths in the whole United States.  Some observers attributed the high infant mortality to premature births and a practice of feeding infants adult food at too early an age.<ref name=Cavender>
{{cite book
  |last=Cavender|first=Anthony
  |year=2003
  |title=Folk Medicine in Southern Appalachia
  |location=Chapel Hill
  |publisher=The University of North Carolina Press |isbn=9780807854938 |url=https://books.google.com/books?id=83To12NoFa4C 
}}</ref>{{rp|19-21}}

An analysis performed for the [[Appalachian Regional Commission]] (ARC) found that, as of 2000, 108 of the 406 counties in the ARC region had county-wide shortages of health professionals, 189 counties had shortages in part of the county, and 109 counties had no shortages. Clusters of counties with county-wide shortages were identified in central West Virginia, eastern Kentucky, northeastern Mississippi, and central Alabama.<ref name=Halverson>
{{Citation
  | last1 = Halverson | first1 = Joel A.
  | last2 = Ma| first2 = Lin
  | last3 = Harner| first3 = E. James
  | title = An Analysis of Disparities in Health Status and Access to Health Care in the Appalachian Region
  | publisher = [[Appalachian Regional Commission]]
  | place = Washington, D.C.
  | pages = 
  | year = 2004
   |url=http://www.arc.gov/research/researchreportdetails.asp?REPORT_ID=82
}}</ref>{{rp|207}}

===Availability of hospitals===

As of 1999 there were 81 counties in the region that had no hospitals and 203 had a single hospital. Large cities in the region, such as [[Pittsburgh]] and [[Birmingham, Alabama|Birmingham]], had more access to hospitals, including teaching hospitals and medical schools, than the typical Appalachian county.<ref name=Halverson/>{{rp|208}}
As of 2006, nearly two thirds of counties in the Appalachian region lacked a hospital offering obstetrical services.  More than three fourths of the counties lacked facilities for the treatment of the mentally ill.  Nearly 90 percent were without programs to address substance abuse.  Distressed counties had, on average, one primary care physician for every 2,128 persons and one specialist for every 2,857 individuals.<ref name=Abramson/>

===Infant mortality rates in white and non-white populations===

A clear disparity in the level of infant mortality rates between white and non-white populations is made clear by examining the two distributions. Infant mortality rates for the white population range from 1.6 to 17.1 deaths per 1,000 live births. In contrast, infant mortality rates for non-white populations range from 2.3 to 500.0 deaths per 1,000 live births.<ref name=Halverson/>

===March of Dimes statistics===

[[The March of Dimes]] reports the following 2011 statistics regarding perinatal health in some states that contain the Appalachian Region:<ref name=March>
{{cite web
  |title=Perinatal Statistics
  |year=2011
  |publisher=[[March of Dimes]]
  |url=http://www.marchofdimes.org/peristats/
}}</ref>

====Kentucky====

In an average week in Kentucky:
1,123 babies are born.
158 babies are born preterm.
103 babies are born low birth weight.
8 babies die before their first birthday

====Alabama====

In an average week in Alabama:
1,241 babies are born.
195 babies are born preterm.
131 babies are born low birth weight.
11 babies die before reaching their first birthday.

====Mississippi====

In an average week in Mississippi:
864 babies are born.
156 babies are born preterm.
102 babies are born low birth weight.
9 babies die before reaching their first birthday.

====West Virginia====

In an average week in West Virginia:
413 babies are born.
57 babies are born preterm.
39 babies are born low birth weight.
3 babies die before reaching their first birthday.

==Health beliefs==
Many people in the region consider health to be the absence of illness with the ability to be physically active, and are focused on the present.  Many tend to delay treatment until symptoms become severe.  Others turn to alternative healing methods and herbal remedies.  No evidence exists, though, that Appalachians are more likely to postpone care or seek alternative methods of healing than are people elsewhere.<ref name=Abramson/>

The family is the focus for health concerns.  If there is a balance of physical, emotional, psychological, spiritual, and social well-being of the family members, then they consider themselves healthy.  Some define health as the ability of the family to respond to each other's needs.  A family with a disabled or sick member can still consider itself healthy if the needs of that person are addressed and the family cooperates to insure individual health.  When a member is gravely ill, the  extended family, often gathering together, makes the medical decisions.<ref name=Abramson/>

==Granny midwives==
{{see also|Granny women}}

Until the middle of the 20th century, women commonly known as “granny midwives” attended most births in southern Appalachia.  They were members of the community and were not officially trained; they learned how to assist in birth by watching other midwives.  While the term “granny” implies that they were old, this was not necessarily the case.  They attended births at home and eventually moved into clinics.  Eventually obstetricians and physicians entered the scene and more births were occurring outside the home.  While “granny midwives” are no longer common in southern Appalachia, direct-entry midwives still practice in certain communities.

Granny midwives were greatly respected in their communities.  If a family had a horse and buggy, they would send for the midwife and bring her to the home.  Many families could not afford this luxury.  In this case the midwife would travel by foot to the house.  Granny midwives were not experts on birth only.  They attended to sick members of the community, educated the people about health, and comforted the dying and their families.<ref name=Abramson/>

Some midwives did not charge for their services.  Others had a sliding scale depending on the income of the family.  Many families who could not afford [[midwifery]] services paid the midwife in other commodities such as food.<ref name=Abramson/>

Prior to the 1940s the midwives were not officially trained.  In the latter half the century, however, physicians entered the scene and began offering workshops and classes for these birth attendants.  In 1939 the Frontier Graduate School of Midwifery opened in Kentucky.  After the opening of the school granny midwives began to be replaced by formally trained women.<ref name=Abramson/>

Today there are very few granny midwives left.  They are the last generation, aged now, and unable to practice legally due to restrictive legislation.  Midwifery, however, continues to be popular and certified nurse midwives who were trained in nursing schools across the country and then received their master's degree in midwifery at an accredited university often attend births.

==Frontier Nursing Service==
{{main|Frontier Nursing Service}}
In 1925 [[Mary Carson Breckinridge|Mary Breckinridge]] founded the Kentucky Committee For Mothers and Babies, which three years later became the [[Frontier Nursing Service]].  Breckinridge created the organization to address the inadequate health care access throughout the isolated areas of Appalachia.  She focused on community involvement and brought midwives from England and Scotland to provide prenatal and maternal care to all who needed it.  Her nurses wore blue uniforms and traveled on horseback to deliver care.  She originally reached out to nurses abroad because few Americans had the requisite training.  Breckenridge founded a hospital in [[Leslie County, Kentucky]], as well as the Frontier Graduate School of Midwifery which opened its doors in 1939.  The organization struggled after Breckinridge's death in 1965 but it continues to provide services to the area.  Current efforts of the staff concentrate on operating its small rural hospital, running the midwifery school, and providing community-based educational programs.<ref name=Abramson/>

==Pregnancy==

===Prenatal care===
In the first half of the century (and prior) prenatal care was practically unheard of in the region.  Midwives were not called on until the due date was approaching or labor pains started.  In the 1940s and 50s midwives began to be officially trained by outside physicians and were told by physicians to encourage women to come in for a prenatal checkup.<ref name=Abramson/>

===Herbal healing===
Granny midwives often used herbs for healing and soothing.  Use of herbs in general is a large part of Appalachian culture.  Raspberry leaf tea is believed to have effects on the uterus and to be high in vitamins and minerals.  It was often recommended for pregnant women and laboring mothers.  Nettle tea is recommended for its vitamin K and calcium content, which helps contain bleeding.<ref name=Abramson/>

==Labor==

===Expected due date===
Long ago, the expected date of delivery was determined by taking the [[phase of the moon]] into consideration.  A delivery during a full moon was considered dangerous to the child and the mother. The arrival of the midwife at the home of the expectant mother varied.  Sometimes the family would call the midwife when the labor started and sometimes she would arrive at the home a few days before the anticipated birth.  This was probably dependent on the proximity of the midwife to the woman’s house.  The midwife and her assistant would assist with chores around the house if they were able to arrive early.<ref name=Cavender/>

===Place of birth===
Until the 1940s most births in the region occurred at home.  The birth of a child was considered an opportunity for family to gather and socialize.  Some considered it akin to quilting or corn shucking.  Women attended to the birthing woman while men socialized in a different room in the house or out on the porch.  Family and friend would come and go throughout the labor, especially if labor was long.<ref name=Scott>
{{cite journal
  |last1=Scott
  |year=2008
  |title=Grannies, Mothers and Babies: An Examination of Traditional Southern Appalachian Midwifery
  |journal=Central Issues in Anthropology
  |volume=4|number=2
}}</ref>

===Examinations during labor===
Midwives performed intrauterine examinations to inspect if the child was appropriately head first in the birth canal.  If the child were not in a proper position midwives would attempt to manipulate the child by hand.  Women were allowed to labor in a sitting position if they felt that was comfortable for them.<ref name=Scott/>

===Speeding up labor===
The following were believed to speed up childbirth: ingestion of [[quinine]], turpentine, gunpowder, tansy tea, flaxseed, or slipper elm.  Sneezing, which was also believed to hasten labor, would be induced by blowing red pepper or gunpowder through a quill into the mother’s nose (a practice known as “quilling”).  Labor could also be quickened by placing a snakeskin around the thigh.<ref name=Abramson>
{{cite book
  |last=Abramson|first=Rudy
  |last2=Haskell|first2=Jean
  |year=2006
  |title= Encyclopedia of Appalachia
  |location=Knoxville
  |publisher=University of Tennessee Press
}}</ref>   A sharp object placed under the bed was believed to “cut” the labor pains or stop [[hemorrhaging]].  If an ax was used, one that has cut many trees was considered to be the best.<ref name=Cavender/>

==Birth practices==
Folk beliefs dominated the region in the past and continue to influence birth practices.  Bad luck during labor could be caused by a number of things such as the mother raising her hands above her head, a dove mourning outside the window, or a member of the household sweeping the steps after sundown.<ref name=Abramson/>

The practices related to the [[placenta]] varied.  Some midwives believed the woman would pass the placenta faster if she blew with great force into her fist or clasped her hands together really tight (Encyclopedia).  Some women were encouraged to sit on a pot filled with hot water to pass the placenta faster.  Placentas were buried deep enough so that they could not be dug up by a human or an animal.  If the placenta were dug up, this would bring bad luck, illness, or death to the mother and child.  Sometimes the placenta were buried or disposed of in a stream of running water to prevent fever in the mother.<ref name=Cavender/><ref name=Abramson/>

==Postpartum practices==
Early midwifery in the region was rather reliant on what was found in the house.  String was used to tie cords and oven sterilized cloth was used for diapers.  One midwife who practiced in northeast Tennessee mentioned that sometimes there was only enough water to wash her hands after birth, and none to wash the baby.  Some midwives refused to attend to unmarried women due to their own religious beliefs.<ref name=Abramson/>

While midwives did not typically offer prenatal care, postnatal care was more common.  Midwives sometimes stayed in the house to care for the woman and child, especially if the delivery had been a difficult one.<ref name=Scott/>

Soft masses such as milk and bread, onion and cornmeal, cow dung, pancakes, or potato scrapings were used to treat [[mastitis]].  A cloth soaked in [[camphor]] was applied to engorged breasts to draw out milk.  Since [[breast pump]]s were not available, puppies, piglets, and goats were encouraged to suckle the breasts to also draw out milk.<ref name=Scott/>

==Care of the newborn==
Newborns were held upside down by their feet and lifted up and down to prevent livergrown disorder. Some midwives believed that placing the child next to the mother under the quilt would force the hives out of the baby's body. Others recommended a little [[catnip]] or ground ivy tea, a drop or two of [[turpentine]], or a spoonful of whiskey in order to "hive" the baby. A piece of cloth was tied around the newborn's waist for six week to protect the navel area which was thought to easily rupture due to its weakness.<ref name=Cavender/> The newborn’s hair could not be cut during the first few weeks of life for fear of death before six months of age. If a child whose hair was cut too early did not die, it was feared that the child would become a thief later in life. If someone stepped over the child, this would stunt the newborn’s growth. The day of the week on which the child was born also said a lot about the child. Children born on a Tuesday were said to be unlucky.<ref name=Abramson/> According to medical professionals in western Kentucky, mothers in rural Appalachia routinely bottlefeed their infants with [[Mountain Dew]] and juice.<ref name=mtndew>{{cite news|last1=Browning|first1=Frank|title=“Don’t put Mountain Dew in a baby bottle”|url=http://www.salon.com/2012/08/10/dont_put_mountain_dew_in_a_baby_bottle/|accessdate=13 October 2015|work=[[Salon (website)|Salon]]|publisher=Salon Media Group|date=August 10, 2012}}</ref>

== References ==
{{Reflist}}

[[Category:Childbirth in the United States]]
[[Category:Appalachian society]]