{{Infobox journal
| title = Journal of Electroceramics
| cover = [[File:Journal of Electroceramics displayimage.jpg]]
| editor = Harry L. Tuller
| discipline = [[Materials science]]
| formernames =
| abbreviation = J. Electroceram.
| publisher = [[Springer Science+Business Media]] on behalf of [[The Minerals, Metals & Materials Society]]
| country = United States
| frequency = 8/year
| history = 1997-present
| openaccess = Hybrid
| license =
| impact = 1.422
| impact-year = 2013
| website = http://www.springer.com/materials/optical+%26+electronic+materials/journal/10832
| link1 = http://www.springerlink.com/openurl.asp?genre=issue&issn=1385-3449&issue=current
| link1-name = Online access
| link2 =
| link2-name =
| JSTOR =
| OCLC = 177653710
| LCCN = sn98038133
| CODEN = JOELFJ
| ISSN = 1385-3449
| eISSN = 1573-8663
}}
The '''''Journal of Electroceramics''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] that was established in 1997. It is published by [[Springer Science+Business Media]] on behalf of [[The Minerals, Metals & Materials Society]]. The [[editor-in-chief]] is Harry L. Tuller.

This journal covers research on [[electroceramics|electrical, optical, and magnetic ceramics]], including silicon-electroceramic integration, [[nanotechnology]], [[ceramic]]-[[polymer]] composites, [[grain boundary]] and defect engineering. Invited papers occasionally appear in the journal which provide information that encompasses significant progress and analysis of future trends in the various [[interdisciplinary]] sub-fields.

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Academic OneFile]]
* [[Chemical Abstracts Service]]
* [[ChemWeb]]
* [[Compendex]]
* [[Current Contents]]/Physical, Chemical and Earth Sciences
* [[Inspec]]
* [[Materials Science Citation Index]]
* [[PASCAL (database)|PASCAL]]
* [[ProQuest]]
* [[Science Citation Index]]
* [[Scopus]]
* [[TOC Premier]]
* [[VINITI Database RAS]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 1.422.<ref name=WoS>{{cite book |year=2014 |chapter=Journal of Electroceramics |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References == 
{{Reflist}}

== External links ==
* {{Official|1=http://www.springer.com/materials/optical+%26+electronic+materials/journal/10832}}

[[Category:English-language journals]]
[[Category:Materials science journals]]
[[Category:Publications established in 1997]]
[[Category:Quarterly journals]]
[[Category:Springer Science+Business Media academic journals]]