{{Infobox journal
| title = Ateneo Law Journal
| discipline = [[Law]], [[Legal Studies]]
| publisher = [[Ateneo de Manila University School of Law]]
| country = Philippines
| frequency = Quarterly
| history = 1951-present
| ISSN = 0115-6136
| website = http://ateneolawjournal.com

}}

{{Italic title}}
The '''''Ateneo Law Journal''''' is an [[academic journal]] of legal scholarship published by an independent student group at [[Ateneo Law School]].

==Overview==
The journal is published four times a year, with occasional special issues. Topics covered are not restricted to local themes and the journal publishes foreign and international essayists whom it considers noteworthy. The [[Supreme Court of the Philippines]] has cited articles published in the journal in its decisions, the latest of which is Heirs of Dicman v. Cariño, G.R. No. 146459, June 8, 2006. Since volume 47, each June issue is covers the most important cases decided by the Supreme Court for the previous year.

==History==
The ''Ateneo Law Journal'' published its first issue in 1951. It began as a bi-monthly publication and early volumes featured the digests of Supreme Court decisions and questions and suggested answers to the [[Philippine Bar Examination]]. The journal celebrated its 50th anniversary in 2001 with the publication of its 46th volume (there was no publication in 1971 to 1973 when all co-curricular activities in the [[Ateneo de Manila University]] were suspended during the early [[History of the Philippines (1965–1986)#Martial Law and the Fourth Republic (1972–1986)|years of martial law]]). Isabelita A. Tapia served as the journal's first female Editor-in-chief in 1970.

==Selection==
Beginning with Volume 47, the journal stopped the admission of staff members and adopted a more stringent admission process and criteria where only editors would be admitted, directly to the journal's Board of Editors. The admission process now includes the setting of a minimum cumulative grade or quality point index for candidates, the exclusion of candidates on academic probation, an editing examination, a written commentary on a novel legal issue or Supreme Court decision, and a panel interview conducted by the Membership Committee of the Journal. Under this new policy, the journal ceased to be headed by an Editor-in-chief and an Executive Committee was established to head the Board of Editors. Three executive editors are selected by the Board of Editors through election.

The Board of Editors is made up of around 20 student editors. Around 10 new editors are admitted each year from the first, second and third year classes of the [[Ateneo Law School]] (totaling around 600 students).

==Editors==
The current Board of Editors of the Journal's 60th volume is composed of:<ref>http://www.ateneolawjournal.com/main/editors</ref>

* Ray Maynard J. Bacus (Member of the Executive Committee)
* Kenneth Paul T. Cajigal
* Miguel Luigi L. Calayag
* Paolo Miguel S. Consignado
* Fidel Maximo M. Diego III (Member of the Executive Committee)
* Eleonor Dyan R. Garcia
* Klarika Angela C. Garcia
* Sang Mee A. Lee (Member of the Executive Committee)
* Elaine S. Mendoza
* Jewelle Ann Lou P. Santos

==Alumni==
Prominent alumni of the ''Ateneo Law Journal'' include Vice President [[Teofisto Guingona]], Senator Ernesto Maceda, [[Supreme Court of the Philippines]] Justice [[Adolfo Azcuna]], [[Renato Corona]], Court of Appeals Justice Hector Hofileña, [[Sandiganbayan]] Justice Francis E. Garchitorena, Congressmen [[Sergio Apostol]], Regalado E. Maambong, and Exequiel B. Javier, Department of Finance Undersecretary Gaudencio R. Mendoza, Jr., Central Bank Governor Gabriel Singson,  Presidential Commission for Good Government Chairmen [[Camilo Sabio]] and Magtanggol T. Gunigundo, Bureau of Customs Commissioner Antonio M. Bernardo, [[Philippine Stock Exchange]] Presidents [[Eduardo De Los Angeles]] and Francisco Ed. Lim, Professor Jacinto D. Jimenez, Deans [[Cynthia Roxas-Del Castillo]], [[Cesar L. Villanueva]], and [[Andres D. Bautista]], and media personality [[Dong Puno]].

==Significant articles==
*Simeon N. Ferrer, Varying a Shareholder's Statutory Participation in Management by the Use of Non-statutory Devices: Is it Possible under our Corporation Statute?, Vol. 9, Issue 1 (1959).
*[[Claudio Teehankee]], The Very Essence of Constitutionalism, Vol. 21, Issue 1 (1976).
*[[Cynthia Roxas-Del Castillo]], Legal Recovery of Illegally Acquired Wealth, Vol. 31, Issue 1 (1987).
*[[Adolfo S. Azcuna]], The [[Writ of Amparo and Habeas Data (Philippines)|Writ of Amparo]]: A Remedy to Enforce Fundamental Rights, Vol. 37, p.&nbsp;15 (1993).
*Jacinto D. Jimenez, Taking Private Property for Public Purpose, Vol. 45, Issue 1 (2001).
*Anna Leah Fidelis T. Castañeda, The Origins of Philippine Judicial Review, 1900–1935, Vol. 46, Issue 1 (2002).
*Ricardo C. Puno, Sr., Legacies in Civil Law from Justice Arsenio P. Dizon and His Peers, Vol. 46, Issue 3 (2002).
*[[Cesar L. Villanueva]], Revisiting the Philosophical Underpinnings of Philippine Commercial Laws, Vol. 46, Issue 3 (2002).
*[[Joaquin G. Bernas]], From One-Man Rule to "People Power", Vol. 46, Issue 1 (2002).
*Aloysius P. Llamzon, "The Generally Accepted Principles of International Law" as Philippine Law: *Towards a Structurally Consistent Use of Customary International Law in Philippine Courts, Vol. 47, Issue 1 (2003).
*[[Renato Corona]], The Importance of Public Attention to Far-reaching Public Issues and Policies, Vol. 48, Issue 1 (2004).
*Vicente V. Mendoza, Towards Meaningful Reforms in the Bar Examinations, Vol. 48, Issue 3 (2004).
*Ateneo Law Journal Editorial Board, The Father of Philippine Liberal Constitutionalism (In the [[Festschrift]] issue for [[Joaquin G. Bernas]]), Vol. 49, Issue 2 (2004)
*Theoben Jerdan C. Orosa, Constitutional Kritarchy Under the Grave Abuse Clause, vol. 49 p.&nbsp;565 (2004).

==References==
{{Reflist}}

==External links==
*[https://web.archive.org/web/20070613134941/http://ateneolawjournal.com:80/about.php About the Ateneo Law Journal]
*[http://www.ateneolawjournal.com/index.php?id=17 Ateneo Law Journal online]

{{Ateneo de Manila University}}

[[Category:Ateneo de Manila University]]
[[Category:Philippine law journals]]
[[Category:Publications established in 1951]]
[[Category:Law journals edited by students]]