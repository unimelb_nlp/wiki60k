{{primarysources|date=March 2011}}
{{Infobox journal
| title  = Journal of the International AIDS Society
| editor = [[Susan Kippax]], [[Papa Salif Sow]], [[Mark Wainberg]]
| discipline = [[HIV]], [[AIDS]]
| abbreviation = J. Int. AIDS Soc.
| publisher = [[International AIDS Society]]
| country =
| frequency = Upon acceptance
| history = 2004–present
| openaccess = Yes
| license = [[Creative Commons license#Attribution|Creative Commons Attribution License 3.0]]
| impact = 6.256
| impact-year = 2015
| website = http://www.jiasociety.org/index.php/jias
| link1  =
| link1-name =
| link2  =
| link2-name =
| cover  =
| former_names =
| JSTOR  =
| OCLC  = 271427446
| LCCN  =
| CODEN  =
| ISSN  = 1758-2652
| eISSN  =
}}
The '''''Journal of the International AIDS Society''''' (JIAS) is an official [[open-access|open-access,]] [[Peer review|peer-reviewed]], [[medical journal]] published by the [[International AIDS Society]]. Founded in 2004, the journal covers all aspects of research on [[HIV]] and [[AIDS]]. 

The journal’s primary purpose is to provide an open-access platform for the generation and dissemination of evidence from a wide range of HIV-related disciplines, encouraging research from low- and middle-income countries. In addition, JIAS aims to strengthen capacity and empower less-experienced researchers from resource-limited countries. 

The journal operates under the leadership of its Editors-in-Chief, Susan Kippax (Australia), Papa Salif Sow (Senegal) and Mark Wainberg (Canada), as well as an Editorial Board made up of 46 leading scientists specialized in the disciplines covered by the journal.

The journal welcomes submissions from a variety of disciplines with the objectives of making available the most relevant information and of reaching all relevant stakeholders involved in all aspects of the HIV response. The journal strongly encourages submission of papers in the areas of implementation science and operational research, in particular from resource-limited settings. This is to ensure that information on best practices and culturally relevant research findings reaches the broadest possible international audience and stakeholders. 

== Abstracting and indexing ==
The journal is abstracted and indexed in [[PubMed Central]], [[PubMed]]/[[MEDLINE]], [[Science Citation Index Expanded]], [[Chemical Abstracts Service]], [[CINAHL]], [[EMBASE]], and [[Scopus]].<ref>{{cite web |url=http://www.jiasociety.org/index.php/jias/about/editorialPolicies |title=Indexing |publisher=[[International AIDS Society]] |work=Journal of the International AIDS Society Official website |accessdate=2011-03-29}}</ref>

The journal received its first impact factor of 3.256 in 2012, which rose to 6.256 in 2016 (2015 Journal Citation Reports ® Science Edition). In 2016, the journal is ranked 6<sup>th</sup> out of 83 infectious diseases journals and 22<sup>nd</sup> out of 150 immunology titles on the Science Citation Indexes.
== References ==
{{reflist}}

== External links ==
* {{Official|http://www.jiasociety.org/index.php/jias}}

[[Category:English-language journals]]
[[Category:Publications established in 2004]]
[[Category:HIV/AIDS journals]]
[[Category:Creative Commons Attribution-licensed journals]]
[[Category:Academic journals published by learned and professional societies]]