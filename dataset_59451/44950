[[File:Single bubble cropped.jpg|right|thumb|Single-bubble sonoluminescence – a single, cavitating bubble.]]
[[File:Sonoluminescence of Synthetic Ordnance Gel.ogv|thumb|Video of synthetic wound cavity collapsing creating sonoluminescence.]]
[[Image:Sonoluminescence.jpg|right|thumb|Long exposure image of multi-bubble sonoluminescence created by a high-intensity [[Ultrasound|ultrasonic]] horn immersed in a beaker of liquid]]
'''Sonoluminescence''' is the emission of short bursts of light from [[Implosion (mechanical process)|imploding]] [[Liquid bubble|bubble]]s in a liquid when excited by sound.

==History==
The sonoluminescence effect was first discovered at the [[University of Cologne]] in 1934 as a result of work on [[sonar]].{{citation needed|date=January 2014}} [[H. Frenzel]] and [[H. Schultes]] put an ultrasound [[transducer]] in a tank of photographic [[developer fluid]]. They hoped to speed up the development process. Instead, they noticed tiny dots on the film after developing and realized that the bubbles in the fluid were emitting light with the ultrasound turned on.{{citation needed|date=January 2014}} It was too difficult to analyze the effect in early experiments because of the complex environment of a large number of short-lived bubbles. (This experiment is also ascribed to N.&nbsp;Marinesco and J.&nbsp;J. Trillat in 1933, which also credits them with independent discovery). This phenomenon is now referred to as multi-bubble sonoluminescence (MBSL).

In 1960 Dr. Peter Jarman from Imperial College of London proposed the most reliable theory of SL phenomenon. The collapsing bubble generates an imploding shock wave that compresses and heats the gas at the center of the bubble to extremely high temperature.

In 1989 an experimental advance was introduced by [[D. Felipe Gaitan]] and [[Lawrence Crum]], who produced stable single-bubble sonoluminescence (SBSL).{{citation needed|date=January 2014}} In SBSL, a single bubble trapped in an acoustic standing wave emits a pulse of light with each compression of the bubble within the [[standing wave]]. This technique allowed a more [[system]]atic study of the [[phenomenon]], because it isolated the complex effects into one stable, predictable bubble. It was realized that the temperature inside the bubble was hot enough to melt [[steel]].{{citation needed|date=January 2014}} Interest in sonoluminescence was renewed when an inner [[temperature]] of such a bubble well above one million [[kelvin]]s was postulated.{{citation needed|date=January 2014}} This temperature is thus far not conclusively proven; rather, recent experiments conducted by the [[University of Illinois at Urbana–Champaign]] indicate temperatures around {{convert|20000|K|C F}}.<ref name=Illinois/>

==Properties==
Sonoluminescence can occur when a sound wave of sufficient intensity induces a gaseous cavity within a liquid to collapse quickly. This cavity may take the form of a pre-existing bubble, or may be generated through a process known as [[cavitation]]. Sonoluminescence in the laboratory can be made to be stable, so that a single bubble will expand and collapse over and over again in a periodic fashion, emitting a burst of light each time it collapses. For this to occur, a standing acoustic wave is set up within a liquid, and the bubble will sit at a pressure [[anti-node]] of the standing wave. The [[frequency|frequencies]] of [[resonance]] depend on the shape and size of the container in which the bubble is contained.

Some facts about sonoluminescence: {{Citation needed|date=January 2016}}
* The light flashes from the bubbles are extremely short—between 35 and a few hundred [[picosecond]]s long—with peak intensities of the order of 1–{{val|10|ul=mW}}.
* The bubbles are very small when they emit the light—about 1 [[micrometre]] in diameter—depending on the ambient fluid (e.g., water) and the gas content of the bubble (e.g., [[Earth's atmosphere|atmospheric air]]).
* Single-bubble sonoluminescence pulses can have very stable periods and positions. In fact, the frequency of light flashes can be more stable than the rated frequency stability of the oscillator making the sound waves driving them. However, the stability analyses of the bubble show that the bubble itself undergoes significant geometric instabilities, due to, for example, the [[Bjerknes forces]] and [[Rayleigh–Taylor instability|Rayleigh–Taylor instabilities]].
* The addition of a small amount of [[noble gas]] (such as [[helium]], [[argon]], or [[xenon]]) to the gas in the bubble increases the intensity of the emitted light.

Spectral measurements have given bubble temperatures in the range from {{val|2300|u=K}} to {{val|5100|u=K}}, the exact temperatures depending on experimental conditions including the composition of the liquid and gas.<ref name=flannigan>{{cite journal
 |author1=Didenko, Y.T. |author2=McNamara, III, W.B. |author3=Suslick, K.S. | title = Effect of Noble Gases on Sonoluminescence Temperatures during Multibubble Cavitation
 | journal = Physical Review Letters
 | date = January 2000
 | volume = 84
 | pages = 777–780
 | doi = 10.1103/PhysRevLett.84.777
 | bibcode = 2000PhRvL..84..777D
 | pmid = 11017370
 | issue = 4
}}</ref> Detection of very high bubble temperatures by spectral methods is limited due to the opacity of liquids to short wavelength light characteristic of very high temperatures.

Writing in ''[[Nature (journal)|Nature]]'', chemists [[David J. Flannigan]] and [[Kenneth S. Suslick]] describe a method of determining temperatures based on the formation of [[Plasma (physics)|plasmas]]. Using argon bubbles in [[sulfuric acid]], their data show the presence of ionized molecular oxygen O<sub>2</sub><sup>+</sup>, [[sulfur monoxide]], and atomic argon populating high-energy excited states, which confirms a hypothesis that the bubbles have a hot plasma core.<ref>{{cite journal|author1=David J. Flannigan  |author2=Kenneth S. Suslick |lastauthoramp=yes |title=Plasma formation and temperature measurement during single-bubble cavitation|journal=Nature|volume=434|pages=52–55|year=2005|doi=10.1038/nature03361|pmid=15744295|issue=7029|bibcode = 2005Natur.434...52F }}</ref> The [[ionization]] and [[Excited state|excitation]] energy of [[Ozonide|dioxygenyl]] [[cations]], which they observed, is 18 [[electronvolt]]s. From this they conclude the core temperatures reach at least 20,000 Kelvin.<ref name=Illinois>{{cite web|url=http://news.illinois.edu/news/05/0302bubbles.html |title=Temperature inside collapsing bubble four times that of sun &#124; Archives &#124; News Bureau &#124; University of Illinois |publisher=News.illinois.edu |date=2005-02-03 |accessdate=2012-11-14}}</ref>

==Rayleigh–Plesset equation==
:{{main|Rayleigh–Plesset equation}}
The dynamics of the motion of the bubble is characterized to a first approximation by the Rayleigh–Plesset equation (named after [[John Strutt, 3rd Baron Rayleigh|Lord Rayleigh]] and [[Milton S. Plesset|Milton Plesset]]):

:<math>R\ddot{R} + \frac{3}{2}\dot{R}^{2} = \frac{1}{\rho}\left(p_g - P_0 - P(t) - 4\mu\frac{\dot{R}}{R} - \frac{2\gamma}{R}\right)</math>

This is an approximate equation that is derived from the incompressible [[Navier–Stokes equations]] (written in [[spherical coordinate system]]) and describes the motion of the radius of the bubble ''R'' as a function of time ''t''. Here, ''μ'' is the [[viscosity]], ''p'' the [[pressure]], and ''γ'' the [[surface tension]]. The over-dots represent time derivatives. This equation, though approximate, has been shown to give good estimates on the motion of the bubble under the [[acoustics|acoustically]] driven field except during the final stages of collapse.  Both simulation and experimental measurement show that during the critical final stages of collapse, the bubble wall velocity exceeds the speed of sound of the gas inside the bubble.<ref>Bradley P. Barber and Seth J. Putterman, "Light Scattering Measurements of the Repetitive Supersonic Implosion of a Sonoluminescing Bubble", Phys Rev Lett 69, 3839-3842 (1992)</ref> Thus a more detailed analysis of the bubble's motion is needed beyond Rayleigh–Plesset to explore the additional energy focusing that an internally formed shock wave might produce.

==Mechanism of phenomenon==
{{main|Mechanism of sonoluminescence}}
The mechanism of the phenomenon of sonoluminescence is unknown. [[Hypothesis|Hypotheses]] include: hotspot, [[Bremsstrahlung|bremsstrahlung radiation]], collision-induced radiation and [[corona discharge]]s, [[nonclassical light]], [[Quantum tunnelling|proton tunneling]], [[electrodynamic]] [[Jet (fluid)|jets]] and [[fractoluminescence|fractoluminescent jets]] (now largely discredited due to contrary experimental evidence){{citation needed|date=August 2015}}.

[[Image:Sonoluminescence.png|center|600px|thumb|From left to right: apparition of bubble, slow expansion, quick and sudden contraction, emission of light]]

In 2002, M. Brenner, S. Hilgenfeldt, and D. Lohse published a 60-page review that contains a detailed explanation of the mechanism.<ref>[http://prola.aps.org/abstract/RMP/v74/i2/p425_1?"Single bubble sonoluminescence" (''Reviews of Modern Physics'' 74, 425)]</ref> An important factor is that the bubble contains mainly inert noble gas such as argon or xenon (air contains about 1% argon, and the amount dissolved in water is too great; for sonoluminescence to occur, the concentration must be reduced to 20–40% of its equilibrium value) and varying amounts of [[water vapor]]. Chemical reactions cause [[nitrogen]] and [[oxygen]] to be removed from the bubble after about one hundred expansion-collapse cycles. The bubble will then begin to emit light.<ref>[http://prola.aps.org/abstract/PRL/v80/i4/p865_1?"Evidence for Gas Exchange in Single-Bubble Sonoluminescence", Matula and Crum, ''Phys. Rev. Lett.'' 80 (1998), 865-868)]</ref> The light emission of highly compressed noble gas is exploited technologically in the [[argon flash]] devices.

During bubble collapse, the inertia of the surrounding water causes high pressure and high temperature, reaching around 10,000 Kelvin in the interior of the bubble, causing the ionization of a small fraction of the noble gas present. The amount ionized is small enough for the bubble to remain transparent, allowing volume emission; surface emission would produce more intense light of longer duration, dependent on [[wavelength]], contradicting experimental results. Electrons from ionized atoms interact mainly with neutral atoms, causing thermal bremsstrahlung radiation. As the wave hits a low energy trough, the pressure drops, allowing electrons to [[Carrier generation and recombination|recombine]] with atoms and light emission to cease due to this lack of free electrons. This makes for a 160-picosecond light pulse for argon (even a small drop in temperature causes a large drop in ionization, due to the large [[ionization energy]] relative to photon energy). This description is simplified from the literature above, which details various steps of differing duration from 15 microseconds (expansion) to 100 picoseconds (emission).

Computations based on the theory presented in the review produce radiation parameters (intensity and duration time versus wavelength) that match experimental results{{citation needed|date=January 2015}} with errors no larger than expected due to some simplifications (e.g., assuming a uniform temperature in the entire bubble), so it seems the phenomenon of sonoluminescence is at least roughly explained, although some details of the process remain obscure.

Any discussion of sonoluminescence must include a detailed analysis of metastability. Sonoluminescence in this respect is what is physically termed a bounded phenomenon meaning that the sonoluminescence exists in a bounded region of parameter space for the bubble; a coupled magnetic field being one such parameter. The magnetic aspects of sonoluminescence are very well documented.<ref>[http://prl.aps.org/abstract/PRL/v77/i23/p4816_1?"Sonoluminescence in High Magnetic Fields, J.B. Young, T. Schiemedel and Woowan Kang" (''Physical Review Letters'' 77, 4816)]</ref>

===Other proposals===

====Quantum explanations====
An unusually exotic hypothesis of sonoluminescence, which has received much popular attention, is the Casimir energy hypothesis suggested by noted physicist [[Julian Schwinger]]<ref>{{cite web|url=http://www.infinite-energy.com/iemagazine/issue1/colfusthe.html |title=Within article "Cold Fusion: A History of Mine" |publisher=Infinite-energy.com |date=1989-03-23 |accessdate=2012-11-14}}</ref> and more thoroughly considered in a paper by [[Claudia Eberlein]]<ref>Phys. Rev. Lett. 76, 3842–3845 (1996); http://arxiv.org/abs/quant-ph/9506024v1</ref> of the [[University of Sussex]]. Eberlein's paper suggests that the light in sonoluminescence is generated by the vacuum within the bubble in a process similar to [[Hawking radiation]], the radiation generated at the [[event horizon]] of [[black hole]]s. According to this vacuum energy explanation, since quantum theory holds that vacuum contains [[virtual particle]]s, the rapidly moving interface between water and gas converts virtual photons into real photons. This is related to the [[Unruh effect]] or the [[Casimir effect]]. If true, sonoluminescence may be the first observable example of [[Vacuum state|quantum vacuum radiation]]. The argument has been made that sonoluminescence releases too large an amount of energy and releases the energy on too short a time scale to be consistent with the vacuum energy explanation,<ref>K.A. Milton, "Dimensional and dynamical aspect of the
Casimir effect: understanding the reality and significance
of vacuum energy", hep-th/0009173 (2000) http://arxiv.org/abs/hep-th/0009173</ref> although other credible sources argue the vacuum energy explanation might yet prove to be correct.<ref>S.Liberati, F.Belgiorno, Matt Visser, "Comment on ``Dimensional and dynamical aspects of the Casimir effect: understanding the reality and significance of vacuum energy''", hep-th/0010140v1 (2000) http://arxiv.org/abs/hep-th/0010140v1</ref>

====Nuclear reactions====
{{main|Bubble fusion}}
Some have argued that the Rayleigh–Plesset equation described above is unreliable for predicting bubble temperatures and that actual temperatures in sonoluminescing systems can be far higher than 20,000 kelvins. Some research claims to have measured temperatures as high as 100,000 kelvins, and speculates temperatures could reach into the millions of kelvins.<ref>{{cite web|author=Nature China |url=http://www.nature.com/nchina/2008/081015/full/nchina.2008.241.html |title=Sonoluminescence: Baking bubbles : Article : Nature China |publisher=Nature.com |date=2008-10-15 |accessdate=2012-11-14}}</ref> Temperatures this high could cause [[thermonuclear fusion]]. This possibility is sometimes referred to as [[bubble fusion]] and is likened to the implosion design used in the fusion component of [[thermonuclear weapon]]s.

On January 27, 2006, researchers at [[Rensselaer Polytechnic Institute]] claimed to have produced fusion in sonoluminescence experiments.<ref>{{cite web|url=http://news.rpi.edu/luwakkey/1322 |title=RPI: News & Events – New Sonofusion Experiment Produces Results Without External Neutron Source |publisher=News.rpi.edu |date=2006-01-27 |accessdate=2012-11-14}}</ref><ref>{{cite web|url=http://www.sciencedaily.com/releases/2006/01/060130155542.htm |title=Using Sound Waves To Induce Nuclear Fusion With No External Neutron Source |publisher=Sciencedaily.com |date=2006-01-31 |accessdate=2012-11-14}}</ref>

Experiments in 2002 and 2005 by [[Rusi Taleyarkhan|R. P. Taleyarkhan]] using deuterated [[acetone]] showed measurements of [[tritium]] and neutron output consistent with fusion.  However, the papers were considered low quality and there were doubts cast by a report about the author's scientific misconduct.  This made the report lose credibility among the scientific community.<ref name="latimes">[http://www.latimes.com/news/nationworld/nation/la-sci-misconduct19-2008jul19,0,1765099.story Purdue physicist found guilty of misconduct], Los Angeles Times, July 19, 2008, Thomas H. Maugh II</ref><ref name="Nature_India_2008">{{cite journal
  | first = K. S.
  | last  = Jayaraman
  | title = Bubble fusion discoverer says his science is vindicated
  | journal = [[Nature India]]
  | doi = 10.1038/nindia.2008.271
  | year = 2008
  }}</ref><ref name="SFC">{{cite news
  | agency=Associated Press
  | url   = http://www.usatoday.com/tech/science/2008-08-27-purdue-scientist_N.htm
  | title = Purdue reprimands fusion scientist for misconduct
  | work=USA Today
  | date  = August 27, 2008
  | accessdate = 2010-12-28
  }}</ref>

==Biological sonoluminescence==<!-- This section is linked from [[Optical phenomenon]] -->
[[Alpheidae|Pistol shrimp]] (also called ''snapping shrimp'') produce a type of sonoluminescence from a collapsing bubble caused by quickly snapping its claw. The animal snaps a specialized claw shut to create a cavitation bubble that generates acoustic pressures of up to 80 kPa at a distance of 4&nbsp;cm from the claw. As it extends out from the claw, the bubble reaches speeds of 60 miles per hour (97&nbsp;km/h) and releases a sound reaching 218 decibels. The pressure is strong enough to kill small fish. The light produced is of lower intensity than the light produced by typical sonoluminescence and is not visible to the naked eye. The light and heat produced may have no direct significance, as it is the shockwave produced by the rapidly collapsing bubble which these shrimp use to stun or kill prey. However, it is the first known instance of an animal producing light by this effect and was whimsically dubbed "shrimpoluminescence" upon its discovery in 2001.<ref>{{cite journal | journal = Nature | volume = 413 | pages = 477–478 | year = 2001 | doi = 10.1038/35097152 | title = Snapping shrimp make flashing bubbles |author1=Detlef Lohse, Barbara Schmitz  |author2=Michel Versluis  |lastauthoramp=yes | pmid = 11586346 | issue = 6855|bibcode = 2001Natur.413..477L }}</ref> It has subsequently been discovered that another group of crustaceans, the [[mantis shrimp]], contains species whose club-like forelimbs can strike so quickly and with such force as to induce sonoluminescent cavitation bubbles upon impact.<ref name="Patek and Caldwell">{{cite journal |author1=S. N. Patek  |author2=R. L. Caldwell  |lastauthoramp=yes |year=2005 |journal=[[Journal of Experimental Biology (journal)|Journal of Experimental Biology]] |volume=208 |pages=3655–3664 |title=Extreme impact and cavitation forces of a biological hammer: strike forces of the peacock mantis shrimp |doi=10.1242/jeb.01831 |pmid=16169943 |issue=Pt 19}}</ref>

==See also==
* [[Bubble fusion]]
* [[List of light sources]]
* [[Sonochemistry]]

==Notes==
{{reflist}}

==References==
* {{cite journal
 |author1=H. Frenzel  |author2=H. Schultes
  |lastauthoramp=yes |year = 1934
 |title = Luminescenz im ultraschallbeschickten Wasser
 |journal = Z. Phys. Chem.
 |volume = B27
 |page = 421
}}
* {{cite journal
 |last = Gaitan
 |first = D. F.
 |author2=L. A. Crum |author3=R. A. Roy |author4=C. C. Church
  |date = June 1992
 |title = Sonoluminescence and bubble dynamics for a single, stable, cavitation bubble
 |journal = The Journal of the Acoustical Society of America
 |volume = 91
 |issue = 6
 |pages = 3166–3183
 |doi = 10.1121/1.402855
 |bibcode = 1992ASAJ...91.3166G
}}
* {{cite journal
 | last = Brenner
 | first = Michael P. |author2=Hilgenfeldt, Sascha |author3=Lohse, Detlef
 | date = 2002-05-13
 | title = Single bubble sonoluminescence
 | journal = Reviews of Modern Physics
 | volume = 74
 | issue = 2
 | pages = 425–484
 | publisher = The American Physical Society
 | doi = 10.1103/RevModPhys.74.425
 | url = http://doc.utwente.nl/42577/1/single-bubble_sonoluminescence.pdf
 | format = PDF
 | accessdate = 2008-05-27
 | bibcode=2002RvMP...74..425B
}}
* {{cite journal
 |authorlink = Rusi Taleyarkhan
 |authors = R. P. Taleyarkhan, C. D. West, J. S. Cho, R. T. Lahey, Jr., R. Nigmatulin, and R. C. Block
 |date = 2002-03-08
 |title = Evidence for Nuclear Emissions During Acoustic Cavitation
 |journal = Science
 |volume = 295
 |issue = 1868
 |issn = 0036-8075
 | doi = 10.1126/science.1067589
 |url = http://www.sciencemag.org/feature/data/hottopics/bubble/index.shtml
 |accessdate = 2007-05-13
 |pmid=11884748
 |bibcode=2002Sci...295.1868T
 |pages=1868–73
}}
* {{Cite news |url=https://www.nytimes.com/2005/03/15/science/15soni.html |title=Tiny Bubbles Implode With the Heat of a Star |work=New York Times |date=March 15, 2005 |author=Kenneth Chang}}
* John D. Wrbanek, et al.(2009): ''Investigating Sonoluminescence as a Means of Energy Harvesting.'' pages 605–637, in: Marc G. Millis, Eric W. Davis: ''Frontiers of Propulsion Science.'' American Inst. of Aeronautics & Astronautics, Reston, ISBN 1-56347-956-7, [http://naca.larc.nasa.gov/search.jsp?N=0&Ntk=AuthorList|DocumentID&Ntx=mode%20matchall|mode%20matchall&Ntt=Wrbanek|20080006795 Abstract] NASA Technical Reports Server
* {{cite journal
 |author = For a "How to" guide for student science projects see: Robert Hiller and Bradley Barber
 |year = 1995
 |title = Producing Light from a Bubble of Air
 |journal = Scientific American
 |volume = 272
 |issue =  2
 |pages = 96–98
 |doi = 10.1038/scientificamerican0295-96
}}
*{{cite journal
 |first = Paweł
 |last = Tatrocki
 |year = 2006
 |title = Difficulties in Sonoluminescence Theory Based on Quantum Phenomenon of Vacuum Radiation
 |journal = PHILICA.com

 |id = Article number 19

 |url = http://philica.com/display_article.php?article_id=19
}} This article was created in 1996 together with the alternative theory; both were seen by Ms Eberlein. It contains many references to the crucial experimental results in this field.

==External links==
{{Wiktionary}}
{{Commons category|Sonoluminescence}}
* Buzzacchi, Matteo, E. Del Giudice, and G. Preparata, "[http://xxx.lanl.gov/abs/quant-ph/9804006 Sonoluminescence Unveiled?]" ''Quantum Physics'', abstract (quant-ph/9804006). Thursday, 2 April 1998 [''ed''. Single Bubble Sonoluminescence (SBSL) phenomenology.]
* [http://www.physik3.gwdg.de/~rgeisle/nld/sbsl-howto.html A how-to guide to setting up a sonoluminescence experiment]
* [http://www.techmind.org/sl/ Another detailed description of a sonoluminescence experiment]
* [http://www.webcitation.org/query?url=http://www.geocities.com/hbomb41ca/sono.html&date=2009-10-25+23:46:32 A description of the effect and experiment, with a diagram of the apparatus]
* [http://www.scs.uiuc.edu/suslick/images/matula.singlebubble.2cycles.mpg An mpg video of the collapsing bubble (934 kB)]
* [http://stilton.tnw.utwente.nl/shrimp/ Shrimpoluminescence]
* [http://www.impulsedevices.com/ Impulse Devices]
* [http://www.chm.bris.ac.uk/webprojects2004/eaimkhong/sonoluminescence.htm Applications of sonochemistry]
* [http://physicsworld.com/cws/article/news/5032 Sound waves size up sonoluminescence]
* [http://www.physics.ucla.edu/Sonoluminescence/sono.pdf Sonoluminescence: Sound into light]

'''Newer research papers largely ruling out the vacuum energy explanation'''
* [http://arxiv.org/abs/quant-ph/9904013 quant-ph/9904013 S. Liberati, M. Visser, F. Belgiorno, D. Sciama: Sonoluminescence as a QED vacuum effect]
* [http://arxiv.org/abs/hep-th/9811174 hep-th/9811174 K. A. Milton: Sonoluminescence and the Dynamical Casimir Effect]

[[Category:Luminescence]]
[[Category:Ultrasound]]
[[Category:Light sources]]
[[Category:Unsolved problems in physics]]
[[Category:Articles containing video clips]]
[[Category:1934 in science]]