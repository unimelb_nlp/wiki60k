{{Use dmy dates|date=July 2013}}
{{good article}}
{{Infobox newspaper
| name                 = ''Arbeideren''
| logo                 = 
| image                = 
| owners               = 
| founder              = [[Olav Kringen]]
| editor               = ''see text''.
| foundation           = 15 September 1909
| political            = [[Labour Party (Norway)|Labour]]<br>(1909–1923)<br>[[Communist Party of Norway|Communist]]<br>(1924–1929)
| language             = Norwegian
| ceased publication   = 4 October 1929
| headquarters         = [[Hamar]]
}}

'''''Arbeideren''''' ("The Worker") was a Norwegian newspaper, published in [[Hamar]], [[Hedmark]] county. It was started in 1909 as the press organ of the [[Labour Party (Norway)|Labour Party]] in [[Hedemarken]] and its adjoining regions, and was called '''''Demokraten''''' ("The Democrat") until 1923. It was issued three days a week between 1909 and 1913, six days a week in 1914, three days a week again between 1914 and 1918 before again increasing to six days a week. It was renamed to ''Arbeideren'' in 1923, and in the same year it was taken over by the [[Communist Party of Norway|Norwegian Communist Party]]. The Communist Party incorporated the newspaper ''[[Gudbrandsdalens Arbeiderblad]]'' into ''Arbeideren'' in 1924, and until 1929 the newspaper was published under the name '''''Arbeideren og Gudbrandsdalens Arbeiderblad'''''. After ''Arbeideren'' had gone defunct, the name was used by the Communist Party for other newspapers elsewhere.

The chief editors of the newspaper were [[Olav Kringen]] (1909–1913), Ole Holmen (1912–1913), [[Fredrik Monsen]] (1913–1916), [[Paul O. Løkke]] (1916–1919), Alfred Aakermann (1919–1920), [[Olav Larssen]] (1920–1927), and finally [[Trond Hegna]], [[Ingvald B. Jacobsen]], [[Olav Scheflo]], [[Eivind Petershagen]], and [[Jørgen Vogt]] (between 1927 and 1929). Fredrik Monsen, [[Evald O. Solbakken]] and [[Knut Olai Thornæs]] were acting editors from 1924 to 1925.

==Pre-history==
''Demokraten'' was originally the name of a short-lived newspaper in Hamar started by [[Leopold Rasmussen]] in 1852, connected to the [[Marcus Thrane]] movement. Rasmussen started a second newspaper, ''Oplands-Posten'', in Hamar later in 1852, to compete with his own ''Demokraten''.{{sfn|Solbakken|1951|p=125}} An organ for the [[Social liberalism|social liberal]] labour movement in the district, ''Arbeiderbladet'' existed from 1889 to 1892 and was published out of different cities, including in Hamar in the year 1890.{{sfn|Solbakken|1951|p=126}}

A countywide chapter of the [[Labour Party (Norway)|Labour Party]] was established in Hedmark in mid-November 1904.{{sfn|Solbakken|1951|p=28}} After the countywide party convention in [[Stange]] in 1906, the convention summary had to be printed in the [[Oslo|Kristiania]]-based newspaper ''[[Dagsavisen|Social-Demokraten]]'', as it lacked its own local newspaper. The county board thus decided to buy 1,500 copies of the ''Social-Demokraten'' to distribute to its members. There was a growing notion that the party needed its own newspaper.{{sfn|Solbakken|1951|p=33}} In the same year, the labour movement in [[Solør]] (south of Hedmark) bought the paper ''[[Solungen (defunct newspaper)|Solungen]]'', which had existed since 1904. The takeover came into effect on 1 January 1907, and publishing began the following year. ''Solungen'' pretended to be the labour movement organ for the whole of Hedmark, and outside of Solør it was published as ''Hedemarkens Amts Socialdemokrat (Solungen)''.{{sfn|Solbakken|1951|p=127–128}} However, the rest of Hedmark county was not satisfied with this solution.{{sfn|Solbakken|1951|p=131–133}}

==Labour Party period==

===1909–1913===
[[File:Domssogn i Hedmark.svg|thumb|150px|An approximate map where Hedemarken is shown in <span style="color:#6082B6">blue</span>, northern Østerdalen in <span style="color:#FF7F50">red</span> and southern Østerdalen in <span style="color:#FFEF00">yellow</span>.]]

The [[Hamar]]-based newspaper ''Demokraten'' ("The Democrat") was started on 15 September 1909. The initiator and first editor was [[Olav Kringen]], who had ample experience as the editor of ''Social-Demokraten'' from 1903 until 1906. ''Demokraten'' was the Labour Party organ for the [[Mjøsa Cities]] and [[Hedemarken]],{{sfn|Rønning|2010a}}<ref name=snl>{{cite encyclopedia|title=Demokraten – eldre avis i Hamar|editor=Godal, Anne Marit|editor-link=Anne Marit Godal|encyclopedia=[[Store norske leksikon]]|publisher=Kunnskapsforlaget|location=Oslo|url=http://www.snl.no/Demokraten/eldre_avis_i_Hamar|language=Norwegian|accessdate=29 August 2010}}</ref><ref>{{cite encyclopedia|year=|title=Olav Kringen|encyclopedia=[[Norsk biografisk leksikon]]|first=Arnfinn|last=Engen|editor=[[Knut Helle|Helle, Knut]]|publisher=Kunnskapsforlaget|location=Oslo|url=http://www.snl.no/.nbl_biografi/Olav_Kringen/utdypning|language=Norwegian|accessdate=29 August 2010}}</ref> but in its first years it also covered [[Gudbrandsdalen]] and [[Østerdalen]], two northern regions. The name ''Østoplandenes Socialistiske Partiblad'' was considered for the newspaper, but the historical name ''Demokraten'' prevailed.{{Sfn|Solbakken|1951|p=133}} The name was suggested by local Labour MP [[Karl Amundsen]].<ref>{{Harvnb|Solbakken|1950|p=}}</ref> ''Demokraten''<nowiki>'</nowiki>s coverage of Gudbrandsdalen soon ended, and in southern Østerdalen a new labour newspaper, ''[[Østerdalens Arbeiderblad]]'', was set up in 1915. In northern Østerdalen, ''[[Arbeidets Rett]]'' was popular among the labour movement.<ref>{{Harvnb|Solbakken|1951|loc=pp.&nbsp;133, 135, 139}}</ref> According to reports in ''Demokraten'' the newspaper again began to cover news from a part of Gudbrandsdalen, namely the city [[Lillehammer]], in 1912.<ref>{{cite news|title=Lillehammer. Oprop til partifæller!|date=2 April 1912|work=Demokraten|page=2|language=Norwegian}}</ref>

When it came to building up a new newspaper, Kringen had a certain personal drive, as he ran for parliament in [[Norwegian parliamentary election, 1909|1909]]. When he lost the election, he also lost interest to a certain degree. He resigned in 1912 and Ole Holmen, a member of the [[Vang, Hedmark|Vang]] [[municipal council (Norway)|municipal council]], took over as chief editor. However, he ran afoul of other people involved with the newspaper and was fired in 1913.<ref name="Larssen 1969 loc=pp. 22–25">{{Harvnb|Larssen|1969|loc=pp.&nbsp;22–25}}</ref>

[[File:Demokraten november 1913.PNG|thumb|left|Title, November 1913.]]
The newspaper originally had the tagline {{lang|no|''Socialistisk blad for Oplandene''}} ("Socialist Paper for Oplandene"), but in 1910 this was changed to {{lang|no|''Talsmand for Arbeiderbevægelsen''}} ("Spokesman for the Labour Movement"). It was printed by the company ''A. Sæther''. The newspaper was issued three times a week until 1 July 1913, from which point it was increased to six times a week. As part of this ambitious increase, ''Demokraten'' also had 3,000 copies in circulation, unprecedented in its history.{{sfn|Larssen|1935|pp.&nbsp;7–8}}

===1913–1916===
In 1913 the newspaper's supervisory council hired school teacher [[Fredrik Monsen]] to be the new editor.<ref name="Larssen 1969 loc=pp. 22–25"/> [[Olav Larssen]] started his journalist career as a subeditor in the same year.{{sfn|Larssen|1969|p=31}} In the newspaper's supervisory council vote, Monsen edged out [[Waldemar Carlsen]] with 22 to 4 votes, and also prevailed over other applicants who were seasoned editors, such as [[Ingvald Førre]] and [[Eugène Olaussen]]. Larssen prevailed over Carlsen and Førre in the vote for the new subeditor.<ref>{{cite news|title='Demokraten' som dagblad. Nyt redaktionspersonale ansat|date=16 June 1913|work=Demokraten|page=2|language=Norwegian}}</ref>

Only Monsen and Larssen were employed in the newspaper to work with editorial content.{{sfn|Larssen|1969|p=31}} In 1913, Monsen managed to contract known personalities from the labour movement as "regular contributors". These were the nationally known figures Olav Kringen, [[Gunnar Ousland]] and [[Johan Falkberget]], in addition to [[Lillehammer]] politician [[Petter Nilssen]] and the locally known politicians [[Arne Juland]] (later MP) and Andr. Juell.<ref>{{cite news|title=Til arbeide, partifæller!|date=19 July 1913|work=Demokraten|page=2|language=Norwegian}}</ref> Danish expatriate [[Alfred Kruse]] joined in the autumn of 1913.<ref>{{cite news|title=|date=19 November 1913|work=Demokraten|page=2|language=Norwegian}}</ref> However, according to Larssen, the prominent writers contracted to ''Demokraten'' "seldomly wrote" anything.{{sfn|Larssen|1969|p=32}}

In his memoirs, Larssen wrote that Monsen was "often aggressive" as editor-in-chief, especially when writing editorials. He got several adversaries in the city's conservative community, especially after donning a badge with [[Peace symbols#The broken rifle|the broken rifle]], a well-known anti-war symbol.<ref>{{Harvnb|Larssen|1969|loc=pp.&nbsp;27–29}}</ref> The newspaper competed with the old and popular [[Conservative Party (Norway)|conservative]] ''[[Hamar Stiftstidende]]'', the [[Liberal Left Party|liberal left]] ''[[Oplandenes Avis]]'', and the [[Liberal Party (Norway)|liberal]] ''[[Oplandet]]''.<ref>{{Harvnb|Solbakken|1955|p=9}}</ref><ref>{{Harvnb|Lillevold|1948|p=354}}</ref>

The practice of issuing the newspaper six days a week became harder after the outbreak of the [[First World War]]. The war caused a general rise in prices, and newspaper subscriptions and advertisements both declined. ''Demokraten'' had to revert to being issued three times a week starting 1 September 1914.{{sfn|Larssen|1935|pp.&nbsp;7–8}} In December 1914 it adopted a new tagline, {{lang|no|''Organ for arbeiderpartiet i Hamar og Hedemarksbygdene''}} ("Organ for the Labour Party in Hamar and the Hamlets of Hedemarken").{{sfn|Høeg|1973|p=108}}

===1916–1923===
Monsen and Larssen both left ''Demokraten'' in 1916.<ref name=nbl-larssen>{{cite encyclopedia|year=|title=Olav Larssen|encyclopedia=[[Norsk biografisk leksikon]]|first=Egil|last=Helle|authorlink=Egil Helle|editor=[[Knut Helle|Helle, Knut]]|publisher=Kunnskapsforlaget|location=Oslo|url=http://www.snl.no/.nbl_biografi/Olav_Larssen/utdypning|language=Norwegian|accessdate=29 August 2010}}</ref> The next editors were [[Paul O. Løkke]], who served from 1916 to 1919, and Alfred Aakermann, from 1919 to 1920.{{sfn|Solbakken|1951|p=134}} Larssen returned in 1920 as editor-in-chief.<ref name=nbl-larssen/> [[Georg Svendsen]] was the subeditor from 1918 until 1921,{{sfn|Larssen|1935|pp.&nbsp;7–8}} when [[Evald O. Solbakken]] started in the newspaper as subeditor.{{sfn|Rønning|2010b}} Still, there were only two people to deliver the editorial content.{{sfn|Larssen|1969|p=156}}

As the war years went, the newspaper's finances gradually improved. The Norwegian state became more active in production and trade and contributed many advertisements. ''Demokraten'' acquired its own [[type-setting]] machine in October 1918 and a printing press in 1917, which it used from 1 January 1918. From 1 July 1918, circulation once again increased to six days a week.{{sfn|Larssen|1935|pp.&nbsp;7–8}}{{sfn|Høeg|1973|p=108}}

==Communist Party period==
In 1923, the newspaper was renamed ''Arbeideren'' ("The Worker"), and the first issue with this name was released on 1 May 1923, the [[International Workers' Day]].{{sfn|Rønning|2010a}} The change followed a letter in 1922 from the [[Comintern]] [[Executive Committee of the Communist International|Executive]], which stated that no newspaper belonging to a Comintern member organization should have "Social Democrat" or "Democrat" as a part of its title.{{sfn|Maurseth|1987|p=269}} The printing press of the party changed its name accordingly, to ''Arbeiderens trykkeri''.{{sfn|Høeg|1973|p=38}}

[[File:Arbeideren og Gudbrandsdalens Arbeiderblad.jpg|thumb|Logo of ''Arbeideren og Gudbrandsdalens Arbeiderblad''.]]
In the same year, 1923, the Labour Party broke out of the Comintern. Subsequently the [[Communist Party of Norway|Communist Party]] broke away from the Labour Party. The local chapter of the Labour Party in Hamar decided to side with the Communist Party in November 1923, in a 123–22 vote.{{sfn|Solbakken|1951|p=89}} ''Arbeideren'' was then taken away from Labour, as the supervisory council decided by a 65 to 5 vote that it should follow the Communists.{{sfn|Solbakken|1951|p=134}} ''Arbeideren'' was one of thirteen Labour newspapers that broke away from the party and followed the Communists (one, ''[[Nordlys]]'', later returned to Labour).<ref>{{Harvnb|Lorenz|1983|loc=pp.&nbsp;37, 169, 229, 270}}</ref> Since 15 February 1924 the newspaper was published under the name ''Arbeideren og Gudbrandsdalens Arbeiderblad'', as the Communist Party had seen fit to merge ''Arbeideren'' with Lillehammer-based ''[[Gudbrandsdalens Arbeiderblad]]''.{{sfn|Høeg|1973|loc=pp.&nbsp;39–40}}

Editor Larssen and subeditor Solbakken both joined the Communist Party in 1923 and continued running the newspaper.<ref name=nbl-larssen/><ref>{{cite encyclopedia|year=1936|volume=6|title=Solbakken, Evald O.|encyclopedia=[[Arbeidernes Leksikon]]|editor1=[[Jakob Friis|Friis, Jakob]] |editor2=[[Trond Hegna|Hegna, Trond]] |editor3=[[Dagfin Juel|Juel, Dagfin]] |publisher=Arbeidermagasinets Forlag|location=Oslo|language=Norwegian|page=379}}</ref> As Olav Larssen was asked by the party to be the acting editor of ''[[Norges Kommunistblad]]'' in the winter of 1924–1925,{{sfn|Maurseth|1987|loc=pp.&nbsp;414–415}} Fredrik Monsen, Evald Solbakken, and [[Knut Olai Thornæs]] were acting editors between 1924 and 1925.{{sfn|Solbakken|1951|p=134}} Larssen eventually drifted away from the mainstream of the Communist Party. In late 1926 and early 1927 he voiced his opinion in columns that the Communist Party should contribute to the imminent merger of the Labour Party and the [[Social Democratic Labour Party of Norway|Social Democratic Labour Party]]. A local party convention strongly rebuked this opinion. Larssen was thus replaced in January 1927 and left the Communist Party, and Solbakken soon followed suit.<ref name=nbl-larssen/>{{sfn|Solbakken|1955|p=17}}{{sfn|Maurseth|1987|p=431}}{{sfn|Rønning|2010b}} Fredrik Monsen left the party at the same time.<ref>{{cite encyclopedia|year=|title=Fredrik Monsen|encyclopedia=[[Norsk biografisk leksikon]]|first=Tore|last=Pryser|authorlink=Tore Pryser|editor=[[Knut Helle|Helle, Knut]]|publisher=Kunnskapsforlaget|location=Oslo|url=http://www.snl.no/.nbl_biografi/Fredrik_Monsen/utdypning|language=Norwegian|accessdate=29 August 2010}}</ref>

Information differs as to who replaced Larssen. According to Evald Solbakken, and also to the reference bibliography ''Norske aviser 1763–1969'', the replacement was [[Olav Scheflo]], who needed a stand-in, [[Ingvald B. Jacobsen]], for the first period.{{sfn|Solbakken|1951|p=135}}{{sfn|Høeg|1973|loc=pp.&nbsp;39–40}} According to the encyclopaedia ''[[Arbeidernes Leksikon]]'' and historian [[Einhart Lorenz]], [[Trond Hegna]] was the editor in 1927, before he took over ''Norges Kommunistblad'' in the summer of 1927. Hegna's main job was to edit the periodical ''[[Mot Dag]]'', but in this period the people of ''Mot Dag'' had an informal influence on the Communist Party and several of their newspapers.<ref>{{harvnb|Lorenz|1983|loc=pp.&nbsp;78–79, 90–92}}</ref><ref>{{cite encyclopedia|year=1933|volume=3|title=Hegna, Trond|encyclopedia=[[Arbeidernes Leksikon]]|editor1=[[Jakob Friis|Friis, Jakob]] |editor2=[[Trond Hegna|Hegna, Trond]] |editor3=[[Dagfin Juel|Juel, Dagfin]] |publisher=Arbeidermagasinets Forlag|location=Oslo|language=Norwegian|pages=791–792}}</ref> Scheflo formally edited the newspaper from 1927 to 1928, with [[Eivind Petershagen]] as acting editor from late 1927. In 1928 Petershagen formally took over, only to have [[Jørgen Vogt]] become acting editor later that year. Vogt took over in 1929.{{sfn|Høeg|1973|loc=pp.&nbsp;39–40}}

As many newspapers belonging to the dwindling Communist Party, ''Arbeideren'' would cease to exist before the end of the 1920s. It was still published six times a week, but had to give up its printing press in 1929, switching to Samtrykk in [[Oslo]]. The last ever issue of ''Arbeideren og Gudbrandsdalens Arbeiderblad'' was published on 4 October 1929.{{sfn|Rønning|2010a}}{{sfn|Høeg|1973|p=40}}

==Aftermath==
A month after ''Arbeideren'' went defunct, the Communist Party gave its name to [[Arbeideren|a new newspaper]], which was set up as the new main newspaper of the Communist Party in 1930. This new paper was based in Oslo as the replacement of ''Norges Kommunistblad'', which had been liquidated as well.{{sfn|Rønning|2010a}} Olav Larssen and Evald Solbakken found a new outlet in ''[[Hamar Arbeiderblad]]'', which had been set up as the new Hamar organ of the Labour Party in 1925.{{sfn|Rønning|2010b}} The Communist Party later tried to create a weekly newspaper in Hamar, ''[[Rød Front (newspaper)|Rød Front]]'', but it was short-lived and existed only between 1932 and 1933.{{sfn|Rønning|2004|p=91}}{{sfn|Høeg|1973|p=433}} The Oslo version of ''Arbeideren'' went defunct in 1940, and many years after that, the name was used from 1951 to 1953 for [[Arbeideren (Brumunddal)|a third newspaper]], published in [[Brumunddal]], not far from Hamar city.{{sfn|Rønning|2010a}}

==References==
;Notes
{{Reflist|2}}
;Bibliography
{{Refbegin}}
*{{cite book|editor-last=Høeg|editor-first=Tom Arbo|title=Norske aviser 1763–1969: en bibliografi|year=1973|volume=1|publisher=[[University Library of Oslo]]|location=Oslo|language=Norwegian| ref = harv}}
*{{cite book|last=Larssen|first=Olav|authorlink=Olav Larssen|title=Beretning for Hamar Arbeiderblad gjennem 10 år|year=1935|publisher=''Hedmark Arbeiderblad''|location=Hamar|language=Norwegian| ref = harv}}
*{{cite book|last=Larssen|first=Olav|authorlink=Olav Larssen|year=1969|title=Sti gjennom ulendt terreng. Læretid, partistrid, ny vekst|location=Oslo|publisher=Aschehoug|language=Norwegian}}
*{{cite book|editor-last=Lillevold|editor-first=Eyvind|title=Hamars historie|year=1948|publisher=Hamar Stiftstidendes Trykkeri|location=Hamar|language=Norwegian| ref = harv}}
*{{Cite book| last=Lorenz|first=Einhart|authorlink=Einhart Lorenz|title=Det er ingen sak å få partiet lite. NKP 1923–1931|year=1983|publisher=Pax
 | location = Oslo
 | language=Norwegian|isbn = 82-530-1255-1|ref = harv}}
*{{Cite book| last = Maurseth
 | first = Per
 | authorlink = Per Maurseth
 | title = Gjennom kriser til makt 1920-1935
 | year = 1987
 | series = Volume three of ''[[Arbeiderbevegelsens historie i Norge]]''
 | publisher = Tiden| location = Oslo| language = Norwegian| isbn = 82-10-02753-0| ref = harv}}
*{{Cite journal| last = Rønning
 | first = Ole Martin
 | year = 2004
 | title = Norges Kommunistiske Partis presse
 | journal = Arbeiderhistorie
 | publisher = [[Labour Movement Archive and Library]]
 | location = Oslo
 | issn = 0801-7778
 | language = Norwegian
 | ref = harv}}
*{{Cite book
 | last = Rønning
 | first = Ole Martin
 | editor-last = Flo
 | editor-first = Idar
 | title = Norske aviser fra A til Å
 | series = Volume four of ''[[Norsk presses historie 1660–2010]]''
 | year = 2010a
 | publisher = Universitetsforlaget
 | location = Oslo
 | language = Norwegian
 | isbn = 978-82-15-01604-7
 | page = 39
 | chapter = ''Arbeideren''
 | ref = harv}}
*{{Cite book
 | last = Rønning
 | first = Ole Martin
 | editor-last = Flo
 | editor-first = Idar
 | title = Norske aviser fra A til Å
 | series = Volume four of ''[[Norsk presses historie 1660–2010]]''
 | year = 2010b
 | publisher = Universitetsforlaget
 | location = Oslo
 | language = Norwegian
 | isbn = 978-82-15-01604-7
 | chapter=''Hamar Arbeiderblad''
 | page=156
 | ref = harv}}
*{{cite book|last=Solbakken|first=Evald O.|authorlink=Evald O. Solbakken|title=Hamar Arbeiderblad gjennom 25 år|year=1950|publisher=Hedmark Labour Party|location=Hamar|language=Norwegian| ref = harv}}
*{{Cite book
 | last = Solbakken
 | first = Evald O.
 | authorlink = Evald O. Solbakken
 | title = Det røde fylke. Trekk av den politiske arbeiderbevegelse i Hedmark gjennom 100 år
 | year = 1951
 | publisher = Hedmark Labour Party
 | location = Hamar
 | language = Norwegian
 | ref = harv}}
*{{cite book|editor-last=Solbakken|editor-first=Evald O.|editor-link=Evald O. Solbakken|title=Hamar Arbeiderblad's festskrift | year = 1955|publisher=''Hedmark Arbeiderblad''|location=Hamar|language=Norwegian| ref = harv}}

[[Category:1909 establishments in Norway]]
[[Category:1929 disestablishments in Norway]]
[[Category:Communist Party of Norway newspapers]]  <!--after 1923-->
[[Category:Defunct newspapers of Norway]]
[[Category:Labour Party (Norway) newspapers]]     <!--before 1923-->
[[Category:Media in Hamar]]
[[Category:Norwegian-language newspapers]]
[[Category:Publications established in 1909]]
[[Category:Publications disestablished in 1929]]