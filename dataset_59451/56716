{{ about|the journal called "Electronic Markets"|electronic marketplaces|electronic markets}}
{{Infobox journal
| title = Electronic Markets
| cover = [[File:Electronic Markets.jpg]]
| discipline =  [[e-commerce|Electronic commerce]]
| abbreviation = Electron. Mark.
| editor = Rainer Alt, Hans-Dieter Zimmermann
| publisher = [[Springer Science+Business Media]]
| history = 1991-present
| frequency = Quarterly
| impact = 0.935
| openaccess   = No
| impact-year = 2014
| website = http://www.electronicmarkets.org
| link1 = http://www.springer.com/business/business+information+systems/journal/12525
| link1-name = Journal page at publisher's website
| link2 = http://link.springer.com/journal/volumesAndIssues/12525
| link2-name = Online archive
| ISSN = 1019-6781
| eISSN = 1422-8890
| OCLC = 47837080
}}
'''''Electronic Markets - The International Journal on Networked Business''''' is a quarterly [[peer-reviewed]] [[academic journal]] that covers research on the implications of [[information systems]] on [[e-commerce]]. It was established in 1991 and is published by [[Springer Science+Business Media]]. The [[editors-in-chief]] are [[Rainer Alt]] ([[Leipzig University]]) and [[Hans-Dieter Zimmermann]] ([http://www.fhsg.ch/fhs.nsf/en/en-home FHS St. Gallen University of Applied Sciences]).

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]], [[Inspec]], [[ProQuest]], [[Academic OneFile]], [[Current Contents]]/Social & Behavioral Sciences, [[International Bibliography of the Social Sciences]], and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 1.404.<ref name=WoS>{{cite book |year=2016 |chapter=Electronic Markets |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

==References==
{{Reflist}}

== External links ==
[http://www.electronicmarkets.org Official Website]

[http://www.sherpa.ac.uk/romeo/issn/1019-6781/ Copyright policies & self-archiving]


[[Category:Business and management journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Publications established in 1991]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]


{{business-journal-stub}}