{{Infobox flag
| Name = Republic of Abkhazia
| Article =  
| Use = 
| Image = Flag of Abkhazia.svg
| Design =  Seven horizontal stripes alternating green and white; in the canton, a white open hand below a semicircle of seven five-pointed stars on a red field.
| Adoption = 23 July 1992
| Proportion = 1:2
| Image2 = Presidential Standard of Abkhazia.svg
| Nickname2 =
| Morenicks2 =
| Use2 = [[President of Abkhazia|Presidential Standard]]
| Symbol2 =
| Proportion2 =
| Adoption2 =
| Design2 = 
| Designer2 =
}}

[[Abkhazia]] is claimed and controlled by the partially recognized Republic of Abkhazia, and functions as a ''[[de facto]]'' independent Abkhazian state. [[Georgia (country)|Georgia]], the [[United Nations]] and the majority of the world's governments consider Abkhazia to be part of Georgia, legally governed by the ''[[de jure]]'' [[Government of the Autonomous Republic of Abkhazia]].

== Republic of Abkhazia ==
The '''flag of the [[Republic of Abkhazia]]''' was created in 1991 by V. Gamgia.<ref name=Argun>Аргун Ю.Г. (''Argun Yu. G.''), [http://www.kolhida.ru/index.php3?path=_abigi/book/XLVIII&source=argun О Государственном флаге] (''On the state flag'') (XLVIII итоговая научная сессия (11-13 мая). тезисы докладов. - Сухум: АбИГИ, 2004, с. 3-4)</ref> It was officially adopted on 23 July 1992.<ref name="fotw">{{cite web|url=http://www.crwflags.com/fotw/flags/ge-abkha.html|title=Abkhazia (Georgia)|date=2009-08-15|publisher=FOTW Flags Of The World|accessdate=2009-10-31}}</ref> The design of the red [[canton (flag)|canton]] is based on the banner of the medieval [[Kingdom of Abkhazia]]. The open right hand means "''Hello to friends! Stop to Enemies!''".<ref>http://www.ahakuytra.com/abhazya-sehitleri/247-abhazya-bayraginin-anlami.html</ref> The seven stars in the canton have since been reinterpreted to correspond to the seven historical regions of the country - [[Sadzen]], Bzyp, Gumaa, [[Abzhywa]], Samurzaqan, Dal-Tsabal and Pskhuy-Aibga.<ref name=abkhazgov>[http://www.abkhaziagov.org/ru/state/sovereignty/symbols.php Государственные символы<!-- Bot generated title -->]</ref>

Seven is a number sacred to the Abkhaz and the green and white stripes represent the tolerance that allows [[Christianity]] and [[Islam]] to cohabit.<ref>[http://www.flagsinformation.com/abkhazia-country-flag.html Abkhazia Flag from the Flags of the World Database.]</ref>

<gallery>
File:Apsua Ladies in Flag Clothes.jpg|Woman wearing dress modelled after the Abkhazian flag
File:Apsua Childs waving Apsny Flag.jpg|Children waving miniature Abkhazian flags
File:Apsny Flag With Helicopter.jpg|Helicopter towing the Abkhazian flag
File:Apsua Holding Apsny Flag.jpg|Parade exhibiting Abkhazian flags
</gallery>

== Autonomous Republic of Abkhazia ==
{{Infobox flag
| Name = Autonomous Republic of Abkhazia
| Article =  
| Use = 
| Image = Flag of Abkhazia (GE).svg
| Design =  Proposed: Seven horizontal stripes alternating green and white in the right half, [[Flag of Georgia (country)|flag of Georgia]] in the left half.
| Adoption = 
| Proportion = 
}}

{{Morerefs-section|date=January 2014}}

A flag and emblem have been proposed by the Georgian State Council of Heraldry for the [[Government of the Autonomous Republic of Abkhazia]] that combines the flag of Georgia with the green and white stripes found on the flag of the breakaway state.<ref>http://heraldry.ge/index.php?m=844&lng=eng</ref>

== Historical flags ==
=== Socialist Soviet Republic of Abkhazia ===
The '''flag of the SSR Abkhazia''' was adopted in 1925 when the [[Socialist Soviet Republic of Abkhazia|SSR Abkhazia]] ratified its constitution. It was used until 1931, when the SSR Abkhazia was transformed into the [[Abkhaz Autonomous Soviet Socialist Republic|Abkhaz ASSR]] with a different flag.

=== Abkhaz Autonomous Soviet Socialist Republic ===
This flag of the [[Abkhaz Autonomous Soviet Socialist Republic|Abkhaz ASSR]] was introduced in 1978 and used until the collapse of the Soviet union in 1991. The previous flag used between 1931 and 1978 was identical to the flag of the Georgian SSR, and in 1978 the name of the Abkhaz ASSR was added written in the Abkhaz [[Abkhaz language|language]] and [[Abkhaz alphabet|script]].

<gallery>
File:Flag of Abkhazian SSR.svg|[[File:FIAV historical.svg|23x15px]] Flag of the Socialist Soviet Republic Abkhazia (1925–31)
File:Flag of Abkhazian ASSR.svg|[[File:FIAV historical.svg|23x15px]] Flag of the Abkhaz ASSR introduced in 1978
</gallery>

== See also ==
* [[Emblem and logo of Abkhazia]]
* [[Coat of arms of the SSR Abkhazia]]

== References ==
{{reflist}}

== External links ==
{{Commons category|Flags of Abkhazia}}
* [http://www.crwflags.com/fotw/flags/ge-abkha.html Flag of Abkhazia at FOTW]
* {{FOTW|id=su-ab|title=Abkhazia in the Soviet Union (1940-1956)}}

{{USSR flags}}
{{Flags of Europe}}
{{National flags}}

{{DEFAULTSORT:Abkhazia, Flag of}}
[[Category:National flags]]
[[Category:National symbols of Abkhazia|Flag]]
[[Category:Flags of unrecognized or largely unrecognized states]]
[[Category:1991 introductions]]
[[Category:History of Abkhazia|Flag of the Socialist Soviet Republic]]