<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox aircraft begin
 | name=Sunwheel
 | image=WD Flugzeugbau Sunwheel R.JPG
 | caption=
}}{{Infobox aircraft type
 | type=Two seat sport [[homebuilt aircraft|kit built]] [[biplane]]
 | national origin=[[Germany]]
 | manufacturer=WD Flugzeug Leichtbau<br/>[[UL-Jih Sedláĉek Spol s.r.o.]]
 | designer=Wolfgang Dallach
 | first flight=
 | introduced=
 | retired=
 | status=In production
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=at least 48 built or under construction
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from=
 | variants with their own articles=
}}
|}

The '''Dallach D.3 Sunwheel''', also known as the '''WDFL Sunwheel''' and more recently the '''Fascination Sunwheel''', is a fully [[aerobatic]], single-engine, two-seat [[biplane]], [[homebuilt aircraft|homebuilt]] from kits. It was designed and marketed in [[Germany]].<ref name="WDLA11"/>

==Design and development==

This [[Wolfgang Dallach]] design, like his [[Dallach Sunrise]], was marketed by WD Flugzeug Leichtbau (WDFL),<ref name=Simpson/> the kits at least partly built in the [[Czech Republic]] by [[UL-Jih Sedláĉek Spol s.r.o.]]<ref name=uljih/> The Sunwheel is constructed from metal tube and is [[aircraft fabric covering|fabric covered]].  It is a [[wing configuration#Wing support|single bay]] biplane with a single I-form, [[fairing (aircraft)|faired]] [[interplane strut]] on each side.  The wings are both swept and [[stagger (aviation)|staggered]].  There are [[aileron]]s on both upper and lower wings, externally interconnected. [[Cabane struts]] support the upper wing over the [[tandem]], open [[cockpit]]s.  The Sunwheel has a [[conventional undercarriage|fixed tailwheel undercarriage]], with the mainwheels hinged from the [[fuselage]] on V-form, cross-connected struts.  Some Sunwheels have [[Aircraft fairing|wheel fairings]].<ref name=Simpson/>

The Sunwheel is usually powered either by a 60&nbsp;kW (80&nbsp;hp) [[Rotax 912 UL]] [[flat four]] or a 48&nbsp;kW (65&nbsp;hp) [[Sauer ULM 2000]] engine.<ref name=Simpson/>

==Operational history==
48 Sunwheels appeared in mid-2010 on the civil registers of [[Europe]]an countries excluding Russia.<ref name=EuReg/>

Reviewer Marino Boric said of the design in a 2015 review, "its sturdiness and its relatively low speeds make it practically indestructible in flight, though you will need something more powerful than a Rotax 912to make the most of its abilities."<ref name="WDLA15">Tacke, Willi; Marino Boric; et al: ''World Directory of Light Aviation 2015-16'', page 50. Flying Pages Europe SARL, 2015. {{ISSN|1368-485X}}</ref>
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (Rotax 912UL)==
{{Aircraft specs
|ref=Airelife's World Aircraft<ref name=Simpson/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=
|crew = one
|capacity=one passenger
|length m=5.7
|length note=
|span m=7.0
|height m=2.2
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=450
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Rotax 912UL]]
|eng1 type=four cylinder horizontally opposed four stroke, air- and water-cooled
|eng1 hp=80
|eng1 note=
|power original=
|more power=
|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=
|max speed kmh=145
|max speed note=
|cruise speed kmh=130
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=350
|range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=0.62
|climb rate note=initial
|time to altitude=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|more performance=
|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{commons category|Dallach Sunwheel}}
{{reflist|refs=

<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 49. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref>

<ref name=Simpson>{{cite book |title= Airlife's World Aircraft|last= Simpson |first= Rod |coauthors= |edition= |year=2001|publisher= Airlife Publishing Ltd|location= Shrewsbury|isbn=1-84037-115-3|page=169 }}</ref>

<ref name=EuReg>{{cite book |title=European registers handbook 2010 |last= Partington |first=Dave |coauthors= |edition= |year=2010|publisher= Air Britain (Historians) Ltd|location= |isbn=978-0-85130-425-0|page=}}</ref>

<ref name=uljih>{{cite web |url=http://www.uljih.cz/homeeng.php|title=UL-Jih aircraft |author= |date= |work= |publisher= |accessdate=2 December 2011}}</ref>
}}
<!--==External links==-->
{{UL-Jih aircraft}}

[[Category:German sport aircraft 1990–1999]]
[[Category:UL-Jih aircraft]]