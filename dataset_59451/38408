{{Use British English|date=March 2012}}
{{Infobox military conflict
| partof      = the [[French invasion of the Isle of Wight (1545)|French invasion of the Isle of Wight]] during the [[Italian War of 1542–1546]].
| image       = [[File:Monks Bay Battle of Bonchurch.jpg]]
| caption     = Monks Bay in 2008. French troops advanced from the bay before they reached St. Boniface Down, the location where the fighting took place.
| conflict    = Battle of Bonchurch
| date        = July, 1545
| place       = [[Bonchurch]], [[The Isle of Wight]], England
| coordinates = {{Coord|50|36|12.46|N|1|11|55.43|W|display=inline,title}}
| result      = English victory<ref name=False>{{Citation|title=False Prophets |url=http://web.ukonline.co.uk/lordcornell/iwhr/plaq.htm |accessdate=2 January 2008 |deadurl=yes |archiveurl=https://web.archive.org/web/20041205155722/http://web.ukonline.co.uk/lordcornell/iwhr/plaq.htm |archivedate=5 December 2004 }}</ref><ref name="Bonchurch A-Z">Goodwin, ''Bonchurch from A-Z'', 7.</ref>
| combatant1  = [[File:Pavillon royal de la France.png|22px]] [[Early Modern France|France]]
| combatant2  = [[File:Flag of England.svg|22px]] [[Kingdom of England|England]]
| commander1  = [[File:Pavillon royal de la France.png|22px]] [[Le Seigneur de Tais]]<ref name=False/><ref name=Lastinvasion>{{Citation|title=The Last Invasion of the Isle of Wight |url=http://www.iwbeacon.com/The-Last-Invasion-of-the-Isle-of-Wight-The-Battle-of-Portsmouth.aspx |accessdate=14 February 2008 |deadurl=yes |archiveurl=https://web.archive.org/web/20110713070925/http://www.iwbeacon.com/The-Last-Invasion-of-the-Isle-of-Wight-The-Battle-of-Portsmouth.aspx |archivedate=13 July 2011 }}</ref>
| commander2  = [[File:Flag of England.svg|22px]] [[Robert Fyssher]]<ref name="Bonchurch A-Z"/>
| strength1   = Approx 500 soldiers<ref name="Bonchurch A-Z"/>
| strength2   = 300<ref name="Bonchurch A-Z"/>-2800<ref name=Lastinvasion/> militiamen
| casualties1 = Unknown
| casualties2 = Unknown
}}
{{Campaignbox Italian War of 1542-1546}}

The '''Battle of Bonchurch''' took place in late July 1545 at [[Bonchurch]] on the [[Isle of Wight]].<ref name="Bonchurch A-Z"/> No source gives the precise date, although 21 July is possible from the sequence of events. The battle was a part of the wider [[Italian War of 1542–1546]], and took place during the [[French invasion of the Isle of Wight (1545)|French invasion of the Isle of Wight]].  Several landings were made, including at Bonchurch.<ref name=Lastinvasion/><ref name="Bonchurch A-Z"/> Most accounts suggest that England won the battle, and the French advance across the island was halted.<ref name="Bonchurch A-Z"/>

The battle was between French regular soldiers, and local English militiamen.<ref name=Lastinvasion/> The number of French soldiers involved is believed to be about 500,<ref name="Bonchurch A-Z"/> with the number of militiamen uncertain, with one source stating 300 and another 2,800.<ref name="Bonchurch A-Z"/><ref name=Lastinvasion/>  The English forces are believed to have been commanded by Captain [[Robert Fyssher]], and the French by [[Le Seigneur de Tais]].<ref name=False/><ref name="Bonchurch A-Z"/><ref name="Lastinvasion"/>

The battle was one of several fought between English and French on the Isle of Wight.<ref name=Lastinvasion/>  The majority of sources state that the English won,<ref name="False"/><ref name="Bonchurch A-Z"/> although one suggests that the French were victorious.<ref name=Lastinvasion/>  The battle was fought as part of the French attempt to cause enough damage to force English ships to leave their defensive positions and attack in less favourable conditions.<ref name=Lastinvasion/> Other French landings were made at [[Sandown]], [[Bembridge]] and [[St Helens, Isle of Wight|St Helens]].<ref name=Lastinvasion/>

==Background==
{{Main|Italian War of 1542-1546|French invasion of the Isle of Wight (1545)|Battle of the Solent}}

The [[Italian War of 1542-1546]] arose from a dispute between [[Holy Roman Emperor]] [[Charles V, Holy Roman Emperor|Charles V]] and [[Francis I of France]], which had not been settled by the [[Italian War of 1535-1538]].  This led to war between France, backed by the [[Ottoman Empire]] and [[United Duchies of Jülich-Cleves-Berg|Jülich-Cleves-Berg]], and the [[Holy Roman Empire]], backed by the [[Kingdom of England]], Spain, [[Saxony]], and [[Brandenburg]]. After two years of fighting Charles V, and [[Henry VIII of England|Henry VIII]] invaded France. In September 1544, English forces captured [[Boulogne-sur-Mer|Boulogne]]. France failed to re-capture the city by force.  Peace talks between England and France were unsuccessful, partly because Henry VIII refused to return Boulogne.<ref>[[Robert Knecht]], ''Renaissance Warrior'', 501; Scarisbrick, ''Henry VIII'', 397–398.</ref> As a result, Francis I decided to invade England, hoping that Henry VIII would return Boulogne in return for his leaving England. Thirty thousand French troops and a fleet of some 400 vessels were assembled,<ref>Knecht, ''Renaissance Warrior'', 502; Phillips, "Testing the 'Mystery'", 50–51.</ref> and sailed from [[Le Havre]] on 16 July.

On 18 July,  French and English ships engaged off the English coast, marking the beginning of the [[Battle of the Solent]]. The outnumbered English ships withdrew,<ref name=Lastinvasion/> hoping to lure the French into the shallows of Spithead, but the French wanted to fight in the more open waters of eastern Spithead where the English could be encircled.<ref name=Lastinvasion/> To entice the English to abandon their defensive position and engage the larger French fleet, they decided to invade the Isle of Wight and burn buildings and crops.<ref name=Lastinvasion/> France also hoped that the residents of the island might support them and rebel against England, so that it could be used a base.<ref>{{Citation|title=Mary Rose Dossier disaster |url=http://www.thenewscentre.co.uk/rose/disaster.htm |accessdate=2 January 2007 |deadurl=yes |archiveurl=https://web.archive.org/web/20060513221510/http://www.thenewscentre.co.uk:80/rose/disaster.htm |archivedate=13 May 2006 |df=dmy }}</ref> French troops landed on 21 July.

During the [[Hundred Years War]], society had become militarised:  male adults were obliged to fight if needed, and they received regular military training. The Captain of the Isle, Sir [[Richard Worsley]] of [[Appuldurcombe House]], was considered a “capable and energetic commander”.  He was assisted by Sir [[Edward Bellingham]], an officer in the regular army, and a headquarters staff.  The English militia were equipped with "long [[pike (weapon)|pike]]s topped with a bill hook, and daggers, knives and clubs for close fighting", as well as the [[Welsh longbow]].  French soldiers were equipped with firearms, and steel blades.  The militiamen had the advantage of superior morale, speed and agility.<ref name=Lastinvasion/>

The French plan at Bonchurch may have been to burn [[Wroxall, Isle of Wight|Wroxall]] and [[Appuldurcombe]], capture and consolidate a position on the heights of [[St. Boniface Down]], and then link up with another French landing near [[Sandown]].<ref name="Bonchurch A-Z"/>  The area around Bonchurch was important because nearby [[Dunnose Point]] offered safe anchorage, and had a fresh water source.<ref name=Lastinvasion/>

==Prelude==
2000 French troops landed at three locations on the coast,<ref>{{Citation |title=Isle of Wight Heritage |url=http://www.greenislandtourism.org/heritage.htm |accessdate=18 October 2007 |format=  – <sup>[http://scholar.google.co.uk/scholar?hl=en&lr=&q=intitle%3AIsle+of+Wight+Heritage&as_publication=&as_ylo=&as_yhi=&btnG=Search Scholar search]</sup>|archiveurl = https://web.archive.org/web/20070506202312/http://www.greenislandtourism.org/heritage.htm |archivedate = 6 May 2007|deadurl=yes}}</ref> including about 500 at Bonchurch.<ref name="Bonchurch A-Z"/> The landing was unopposed and the French began to advance inland, up the steep and thickly wooded slopes.<ref name=Lastinvasion/> The Isle of Wight [[militia]] learned of the French invasion quickly; 300 of them, under the command of Captain [[Robert Fyssher]], were already waiting at [[St. Boniface Down]] for the French to advance from [[Monks Bay]].<ref name="Bonchurch A-Z"/>

==Battle==
There is no comprehensive account of the battle. However, it could have taken place at dawn and lasted until midday.<ref name="Bonchurch A-Z"/> Some accounts suggest that local women participated by shooting arrows at the French.<ref name="Bonchurch A-Z"/>

===Did the French win?===
One source claims that the French won the battle at Bonchurch, and that the English were not local militiamen, but from [[Hampshire]].  The English forces took up a defensive position flanked by cliffs and screened by woods. According to this account, the English numbered 2,800.  The first French attack was apparently repelled but the French commander  [[Le Seigneur de Tais]] rallied his troops.  A second attack was launched, with the French forces in the 'array' fighting formation.  The account concludes by claiming that, after heavy casualties on both sides, the English line broke and the militia routed, and that Captain [[Robert Fyssher]] shouted an offer of £100 for anyone who could bring him a horse to escape, his being too fat to run.  Sir [[John Oglander]] is claimed to have said: “but none could be had even for a kingdom”.  The captain was never heard from again, and the account suggests he was either killed, or captured and buried at sea.<ref name=Lastinvasion/>

==Aftermath==
Casualties on both sides were heavy.<ref name=Lastinvasion/><ref name="Bonchurch A-Z"/> Another skirmish took place several days later, when the English engaged Frenchmen, disembarked from ships retreating from Portsmouth to look for fresh water.<ref name="Bonchurch A-Z"/> A senior French commander, [[Chevalier D'Aux]], was killed.  The assumed English victory at Bonchurch only had a marginal impact on the course of the [[Italian War of 1542-1546|war]], because it only involved a fraction of the forces engaged throughout. Had the French captured the island, it is unlikely this would have drastically affected the war, because more significant territory was being contested. However, the island could have been used to support French operations against England; [[Claude d'Annebault]], commander of the French armada, recorded: “having it [the Isle of Wight] under our control, we could then dominate Portsmouth... and so put the enemy to extraordinary expense in maintaining a standing army and navy to contain us.”<ref name=Lastinvasion/> Although some sources do claim that the victory at Bonchurch was responsible for the French withdrawal, the source that claims a French victory says that fighting at [[Bembridge]] was ultimately responsible for forcing the French to leave.<ref name=Lastinvasion/>

==Sources==
{{Refbegin}}
* Goodwin, John. ''Bonchurch from A-Z''. Bonchurch: The Bonchurch Trading Company, 1992. ISBN 1-873009-00-3
* Knecht, Robert J. ''Renaissance Warrior and Patron: The Reign of Francis I''. Cambridge: Cambridge University Press, 1994. ISBN 0-521-57885-X.
* Scarisbrick, J. J. ''Henry VIII''. London: The Folio Society, 2004.
{{Refend}}

==References==
{{Reflist|2}}
{{Good article}}
{{Use dmy dates|date=March 2012}}

{{DEFAULTSORT:Bonchurch, Battle Of 1545}}
[[Category:1545 in England]]
[[Category:1545 in France]]
[[Category:Conflicts in 1545]]
[[Category:Battles involving England]]
[[Category:Battles involving France]]
[[Category:Battles of the Italian Wars]]
[[Category:Military history of the Isle of Wight]]
[[Category:16th century in Hampshire]]
[[Category:July 1545 events]]