{{Infobox civilian attack
| title       = Bombo shooting
| partof      =
| image       = 
| image_size  = 
| alt         = 
| caption     = 
| map         =
| map_size    =
| map_alt     =
| map_caption =
| location    = [[Bombo, Uganda|Bombo]], [[Uganda]]
| target      =
| coordinates = 
| date        = March 9, 2013
| time        = 12:12 a.m.
| timezone    = 
| type        = [[Mass murder]]
| fatalities  = 10
| injuries    = 3
| perp        = Patrick Okot Odoch
| weapons     = [[AK-47]]
| motive      =
}}

The '''Bombo shooting''' was a [[mass murder]] that occurred in [[Bombo, Uganda|Bombo]], [[Uganda]] on March 9, 2013. The perpetrator, Patrick Okot Odoch, a [[Private (rank)|private]] in the [[Uganda People's Defence Force]], shot and killed nine people in a bar and, while fleeing, a tenth victim. Odoch fled and was arrested in ten days later, and was charged with murder and [[attempted murder]].<ref>[http://www.newvision.co.ug/news/640840-soldier-arrested-over-bombo-massacre.html Soldier arrested over Bombo massacre], ''[[New Vision]]'' (March 20, 2013)</ref> On June 4, Odoch was found guilty of murder and sentenced to 90 years [[imprisonment]].<ref>[http://www.newvision.co.ug/news/643564-bombo-killer-soldier-gets-90-year-jail-sentence.html Bombo killer soldier gets 90-year jail sentence], ''[[New Vision]]'' (June 4, 2013)</ref>

==Background==
On March 5, 2013, a combined group of [[deserters|army deserters]] and assorted armed criminals attacked the Mbuya barracks belonging to the [[Uganda People's Defence Force]] (UPDF) on March 5, 2013, to steal the weapons located there. 

Patrick Okot Odoch, a [[Private (rank)|private]] in the UPDF who had a history of poor behavior and previously had been arrested for consuming [[bhang]], was attached to the nearby Bombo barracks.<ref>[http://www.redpepper.co.ug/kulayigye-says-bombo-shooting-not-related-to-mbuya-attack/ Kulayigye Says Bombo Shooting Not Related To Mbuya Attack], ''[[Red Pepper (newspaper)|Red Pepper]]'' (March 9, 2013)</ref> Inspired by the chaos at the Mbuya barracks, on the following night of March 6, Odoch had wanted to [[rape]] Grace Chandiru, the 14-year-old daughter of his commander, [[Sergeant]] Onesmus Odule. Odoch saw Grace at the Yumbe Boys' Bar in Pakele, a bar frequented by local military personnel and their families, and attempted to rape her.<ref>[http://direct.ugandaradionetwork.com/a/story.php?s=50862 Residents Welcome Arrest of Killer Soldier], ''[[ugandaradionetwork.com]]'' (March 20, 2013)</ref> Odoch fled when Grace raised alarm, prompting her father Sergeant Odule to report the incident to police on the next day, March 7, for which Odoch promised revenge.

Odoch entered the bar, locked the doors, before opening them again and leaving, probably planning his shooting.<ref name=OBS1303>[http://www.observer.ug/index.php?option=com_content&view=article&id=24179:bombo-picks-the-pieces-after-deadly-gun-attack&catid=34:news&Itemid=114 Bombo picks the pieces after deadly gun attack], ''[[The Observer (Uganda)|The Observer]]'' (March 13, 2013)</ref> Odoch entered the bar again the next day at 9 p.m. while the Odules were present, this time with a colleague, however the owner told him to drink somewhere else, referring him to an incident that occurred on March 7.<ref name=NV1103>[http://www.newvision.co.ug/news/640519-bombo-killer-soldier-sought-revenge.html Bombo killer soldier sought 'revenge'] {{webarchive |url=https://web.archive.org/web/20130515090111/http://www.newvision.co.ug/news/640519-bombo-killer-soldier-sought-revenge.html |date=May 15, 2013 }}, ''[[New Vision]]'' (March 11, 2013)</ref>

== Shooting ==
During the night of March 8, while Odoch was on duty as part of the 23rd Air Defence Regiment, he deserted his post armed with an [[AK-47]] [[assault rifle]] and 30 rounds of [[ammunition]].<ref>[http://www.chimpreports.com/index.php/news/8663-nine-killed-in-shooting-at-bombo-town.html Army Speaks Out On Bombo Shootings] {{webarchive |url=https://web.archive.org/web/20130312012359/http://www.chimpreports.com/index.php/news/8663-nine-killed-in-shooting-at-bombo-town.html |date=March 12, 2013 }}, ''[[chimpreports.com]]'' (March 9, 2013)</ref><ref name=NV1103/> Upon entering the bar on March 9 at 12:12am, Odoch opened fire on the people present, killing Sergeant Odule, his wife Florence, the owner of the bar Amina Aseru, as well as four other soldiers and two civilians, while two people were wounded.<ref name=OBS1303/> Odoch then left and went towards Gogonya, 1.5km away from the bar, where he killed another person. Before finally fleeing he also [[robbed]] Halima Lukiya, stealing her [[hand bag]], a [[mobile phone]], and 2000 [[Ugandan shilling|USh]].<ref>[http://www.observer.ug/index.php?option=com_content&task=view&id=24139&Itemid=116 Mbuya attackers linked to rebels], ''[[The Observer (Uganda)|The Observer]]'' (March 11, 2013)</ref><ref name=DM2703>[http://www.monitor.co.ug/News/National/Bombo-shooting--UPDF-soldier-remanded-over-murder/-/688334/1732552/-/vojfj9/-/index.html Bombo shooting: UPDF soldier remanded over murder], ''[[Daily Monitor]]'' (March 27, 2013)</ref>

Nine of his victims died on the spot, while David Komakech died one hour later from excessive loss of blood.<ref>[http://ugandaradionetwork.com/a/story.php?s=50583  Suspected UPDF Officer Shoots 10 People Dead in Bombo], ''[[ugandaradionetwork.com]]'' (March 9, 2013)</ref> There were 23 spent rounds recovered by police from the crime scene.<ref name=DM1703>[http://www.monitor.co.ug/News/National/Gun-used-in-Bombo-killing-recovered/-/688334/1722028/-/m03ci7z/-/index.html Gun used in Bombo killing recovered], ''[[Daily Monitor]]'' (March 17, 2013)</ref>

===Victims===
{| style="background-color: transparent; width: 600px"
| width="300px" align="left" valign="top" |
*Sgt. Onesmus Odule
*Florence Akullu, wife of Onesmus Odule
*Sarah Akole
*Amina Aseru, the bar owner
*Pte. Teddy Namatovu
| width="300px" align="left" valign="top" |
*Sgt. Joel Obote
*Wilber Odiba
*Cpl. Isaac Osere 
*David Komakech
*Warrant Officer Francis Musana
|}

Wounded were Joyce Asiyo, 44, and Ismail Akbar, 14, son of Amina Aseru.<ref name=URN1203>[http://ugandaradionetwork.com/a/story.php?s=50620 Bombo Shooting Survivors Out Of Danger], ''[[ugandaradionetwork.com]]'' (March 12, 2013)</ref> Reportedly Sgt. Francis Ogugu was also injured.<ref>[http://www.monitor.co.ug/News/National/Court-sends-UPDF-soldier-on-remand-over-Bombo-shooting/-/688334/1733334/-/xcie1oz/-/index.html Court sends UPDF soldier on remand over Bombo shooting], ''[[Daily Monitor]]'' (March 29, 2013)</ref>

After the shooting the army offered every family of the victims 2,150,000 million shillings as condolence and for burial arrangements, as well as coffins and transportation of the dead to their ancestral homes.<ref name=URN1203/>

== Arrest and conviction ==
Following the shooting, security measures were tightened, and all army deserters in the area were ordered to leave, or they would risk arrest. Also a night curfew was imposed, forcing all businesses to close at 10pm, and limiting movement from 11pm onward.<ref name=DM1103>[http://www.monitor.co.ug/News/National/Bombo-barracks-issues-tight-security-guidelines/-/688334/1716560/-/hv3s84/-/index.html Bombo barracks issues tight security guidelines], ''[[Daily Monitor]]'' (March 11, 2013)</ref><ref>[http://ugandaradionetwork.com/a/story.php?s=50595  Bombo Shootings: Army Imposes Night Curfew], ''[[ugandaradionetwork.com]]'' (March 11)</ref>

Joint forces of the police and the army searched for Odoch, and laid traps to capture him.<ref name=DM1703/><ref name=DM1103/> The search operations resulted in the finding of Odoch's uniform on March 10, and on March 14 of the rifle used in the shooting, which was recovered near a factory in Nakatonya Village in Nyimbwa Sub-county and was still loaded with three rounds.<ref name=DM1703/><ref name=DM1703/> When Odoch called his colleagues to ask for financial help, investigators managed to track his mobile phone. He was arrested between March 18 and March 19 at Abere Trading Centre in Ngai Sub-county in [[Oyam District]], brought to the army barracks in [[Lira, Uganda|Lira]] and charged with murder, [[attempted murder]], misuse of a [[firearm]], [[aggravated robbery]], and failing to protect war materials, before being transferred back to Bombo, where he was held to be [[court-martialed]] in a public trial.<ref>[http://www.monitor.co.ug/News/National/Bombo-shootings--suspected-killer-arrested-in-Oyam/-/688334/1724900/-/f59kwv/-/index.html Bombo shootings: Suspected killer arrested in Oyam], ''[[Daily Monitor]]'' (March 19, 2013)</ref><ref name=DM2703/> <ref>[http://www.monitor.co.ug/News/National/ANDREW-BAGALA---HUDSON-APUNYO/-/688334/1726082/-/drvnxv/-/index.html Army set to hold public trial for Bombo suspect], ''[[Daily Monitor]]'' (March 21, 2013)</ref> 

After denying all charges raised against him, Odoch was put in remand at [[Makindye]] Military Prison until April 17.<ref name=DM2703/> Preliminary hearings for the case began on April 22 at Bombo Town Health Centre.<ref>[http://archive.is/20130701152112/http://www.simba.fm/2013/04/17/bombo-killer-soldier-to-face-public-trial/ Bombo killer soldier to face public Trial], ''[[Simba Radio]]'' (April 17, 2013)</ref><ref>[http://archive.is/20130628212247/http://www.chimpreports.com/index.php/mobile/special-reports/crime-investigation/9546-bombo-shootings-suspect%E2%80%99s-trial-begins.html Bombo Shootings Suspect’s Trial Begins], ''[[chimpreports.com]]'' (April 22, 2013)</ref> The maximum punishment Odoch is facing for the crime is a death sentence.<ref>[http://www.nbs.ug/details.php?option=acat&id=1&cid=&a=660#.UXVY80oQZkh Court: UPDF A Peoples's Army, Takes Justice To People]{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}, ''[[nbs.ug]]'' (April 18, 2013)</ref>

==See also==
*[[Kampala wedding massacre]]
*[[Kamwenge Trading Centre shooting]]

==References==
{{reflist}}

==External links==
*[http://www.monitor.co.ug/News/National/Bombo-killings--Four-testify-against-soldier/-/688334/1755614/-/mccnlf/-/index.html Bombo killings: Four testify against soldier], ''[[Daily Monitor]]'' (April 23, 2013)
*[http://www.monitor.co.ug/News/National/Bombo-shooting--Army-court-denies-soldier-bail/-/688334/1756562/-/wkoajhz/-/index.html Bombo shooting: Army court denies soldier bail], ''[[Daily Monitor]]'' (April 24, 2013)
*[http://www.monitor.co.ug/News/National/Bombo-killings--Court-adjourns-public-hearing/-/688334/1758590/-/3wtodfz/-/index.html Bombo killings: Court adjourns public hearing], ''[[Daily Monitor]]'' (April 26, 2013)
*[http://www.observer.ug/index.php?option=com_content&view=article&id=24945:-bombo-killer-faces-12-witnesses&catid=34:news&Itemid=114 Bombo killer faces 12 witnesses], ''[[The Observer]]'' (April 26, 2013)
*[https://web.archive.org/web/20130502045536/http://www.newvision.co.ug/mobile/Detail.aspx?NewsID=641929&CatID=1 UPDF soldier on trial for killing 10 people], ''[[New Vision]]'' (April 22, 2013)
*[http://www.nbs.ug/details.php?option=acat&id=1&cid=&a=680#.UXwMnEoQZkg Angry Mob Crave For Bombo Murderer's Flesh]{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}, ''[[Nile Broadcasting Services|NBS]]'' (April 22, 2013)
*[http://www.nbs.ug/details.php?option=acat&id=1&cid=&a=683#.UXwMmUoQZkg Bombo Court: More Witnesses Pin Soldier]{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}, ''NBS'' (April 23, 2013)
*[http://www.nbs.ug/details.php?option=acat&id=&cid=1&a=681#.UXwNPUoQZkg Bomb Court Martial: More witnesses pin Private Okot Odoch]{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}, ''NBS'' (April 23, 2013)
*[http://www.nbs.ug/details.php?option=acat&id=1&cid=&a=690#.UXwNOEoQZkg Witness: I Saw PTE. Odoch Shoot My Friend Dead]{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}, ''NBS'' (April 23, 2013)
*[http://kfm.co.ug/news/bombo-murder-suspect-in-court-today.html Bombo murder suspect in court today], ''[[KFM Radio (Uganda)|KFM]]'' (April 22, 2013)
*[http://kfm.co.ug/news/trial-of-bombo-shooting-suspect-kicks-off.html Trial of Bombo shooting suspect kicks off], ''KFM'' (April 22, 2013)
*[http://kfm.co.ug/news/updf-soldier-denied-bail-murder-trial-continues.html UPDF soldier denied bail, trial continues tomorrow], ''KFM'' (April 22, 2013)

[[Category:2013 crimes in Uganda]]
[[Category:Mass murder in 2013]]
[[Category:Spree shootings in Uganda]]
[[Category:Luweero District]]
[[Category:March 2013 events]]