{{Infobox airport 
| name         = Kautokeino Airport
| nativename   = Kautokeino flyplass
| nativename-a = 
| nativename-r = 
| image        =
| image-width  =
| caption      = 
| IATA         = QKX
| ICAO         = ENKA
| type         = Private
| owner        = [[Norwegian Directorate of Public Construction and Property]]
| operator     =
| city-served  = 
| location     = [[Kautokeino]], [[Finnmark]], [[Norway]]
| elevation-f  = 1165
| elevation-m  = 355
| coordinates  = {{coord|69|02|25|N|023|02|03|E|region:NO|display=inline,title}}
| website      = 
| pushpin_map            = Norway
| pushpin_relief         = 1
| pushpin_label_position =
| pushpin_label          = QKX
| pushpin_map_alt        =
| pushpin_mapsize        = 250
| pushpin_image          =
| pushpin_map_caption    = Location in Norway
| metric-elev  = yes
| metric-rwy   = yes
| r1-number    = 01–19
| r1-length-f  = 3739
| r1-length-m  = 1200
| r1-surface   = Gravel
| footnotes    = 
}}

'''Kautokeino Airport''' ({{lang-no|Kautokeino flyplass}}; {{airport codes|QKX|ENKA|p=n}}) is a [[general aviation]] airport located in [[Kautokeino]], [[Norway]]. It consists of a {{convert|1200|m|sp=us|adj=on}} gravel [[runway]]. The airport was built by the [[Luftwaffe]] during [[World War II]]. It was rebuilt in 1958 by the [[Royal Norwegian Air Force]] to supply its radar station at Kautokeino. It is largely unused and is now owned by the [[Norwegian Directorate of Public Construction and Property]] and the [[Finnmark Estate]]. Local politicians have called for the airport to be upgraded to a regional airport, but this has been rejected by [[Avinor]].

==History==
[[File:Ju 52 at Kautokeino.jpg|thumb|left|[[Junkers Ju 52]] at Kautokeino during [[World War II]]]]
[[File:Tysk fly på flyplassen i Kautokeino (17181460117).jpg|thumb|left|Norwegian soldiers with the wreck of a German aircraft at Kautokeino Airport in spring 1945]]
The airfield was built by the Luftwaffe as an emergency landing field during the early 1940s.<ref name=arheim>{{cite book |last=Arheim |first=Tom |last2=Hafsten |first2=Bjørn |last3=Olsen |first3=Bjørn |last4=Thuve |first4=Sverre |title=Fra Spitfire til F-16: Luftforsvaret 50 år 1944–1994 |location=Oslo |publisher=Sem & Stenersen |year=1994 |language=Norwegian |isbn=82-7046-068-0 |page=214}}</ref> In addition to this, it stationed a detachment of [[reconnaissance aircraft]].<ref>{{cite book |last=Hafsten |first=Bjørn |last2=Larsstuvold |first2=Ulf |last3=Olsen |first3=Bjørn |last4=Stenersen |first4=Sten |title=Flyalarm: Luftkrigen over Norge 1939–1945 |year=1991 |publisher=Sem & Stenersen |location=Oslo |language=Norwegian |isbn=82-7046-058-3 |page=225}}</ref> The Royal Norwegian Air Force established a radio station at Kautokeino in 1945. Transport to the new airport was among other means carried out using seaplanes which used the [[Altaelva]] river to land. The station was upgraded in 1955 and received a radar and was designated as a reporting post. Its first upgrade took place in 1958, the same year as renovations of the airfield were carried out. The main users of the airfield were [[Twin Otter]]s from [[Bodø Main Air Station]]. On occasion supplies would be dropped by parachute.<ref name=arheim /> Once only a jet fighter has landed here, even if the field is too short for them. In June 1970 an [[Northrop F-5|F5]] landed and took off, then using a [[drogue parachute|parachute]] and [[JATO|extra rockets]]. [[Enontekiö Airport]] in Finland started marketing itself as Enontekiö–Kautokeino Saami Airport from 2008,<ref name=enontekio>{{cite news |url=http://www.nrk.no/kanal/nrk_sapmi/1.6732845 |title=Tomlene opp for Saami airport |last=Pulk |first=Åse |date=15 August 2008 |publisher=[[Norwegian Broadcasting Corporation]] |accessdate=17 February 2013 |language=Norwegian}}</ref> although [[Finavia]] does not use the term any more.<ref>{{cite web |url=http://www.finavia.fi/airports/airport_enontekio?pg=9577180 |title=Enontekiö Airport |publisher=[[Finavia]] |accessdate=17 February 2013}}</ref> Enontekiö is located {{convert|90|km|sp=us}} from Kautokeino.<ref name=enontekio />

Local politicians have proposed that Kautokeino Airport be rebuilt as a regional airport. In 2007 a unison municipal council supported an upgrade to the airport. They cited that Kautokeino would receive a hotel from 2008 and that it would be necessary to have an airport to support the village's tourism industry.<ref>{{cite news |url=http://www.altaposten.no/lokalt/nyheter/article108867.ece |title=Vil bygge flyplass på gammelstripa |last=Kveseth |first=Magne |date=19 September 2007 |work=[[Altaposten]] |accessdate=17 February 2013 |language=Norwegian}}</ref> A secondary argument is that an upgraded airport could be used for an [[air ambulance]] service.<ref>{{cite news |url=http://www.altaposten.no/lokalt/nyheter/article420784.ece |title=Krever ambulanse-flyplass |last=Kveseth |first=Magne |date=12 January 2011 |work=[[Altaposten]] |accessdate=17 February 2013 |language=Norwegian}}</ref> [[Finnmark County Municipality|Finnmark County Council]] voted with a single decisive vote in 2010 to work towards making Kautokeino Finnmark's twelfth regional airport.<ref>{{cite news |url=http://www.finnmarken.no/lokale_nyheter/article5409251.ece |title=Vil ha flyplass i Kautokeino |last=Karstensen |first=Kari |date=8 December 2010 |work=[[Finnmarken (newspaper)|Finnmarken]] |accessdate=17 February 2013 |language=Norwegian}}</ref>

==Facility==
The airport is located {{convert|3|km|sp=us|0}} north of the village center. It consists of a {{convert|1200|by|40|m|sp=us|adj=on}} gravel runway aligned 01–19 (roughly north–south). There is no regular traffic on the airfield. The airport is located at an elevation of {{convert|355|m|sp=us}} [[above mean sea level]] in a flat area.<ref>{{cite web |url=http://generator.firmanett.no/%28t5xg24y52lhra155lt212055%29/generator.aspx?PID=176992&M=0 |title=Kautokeino |last=Reitås |first=Hans Olav |publisher=Norske Flyplasser |accessdate=17 February 2013 |language=Norwegian}}</ref> The airport is owned by the Norwegian Directorate of Public Construction and Property, which leases the land from the Finnmark Estate.<ref name=ntp>{{cite web |url=http://www.avinor.no/tridionimages/NTP%20Avinor%20Hovedrapport_tcm181-141224.pdf |title=Nasjonal transportplan 2014–2023: Framtidsrettet utvikling av lufthavnstrukturen |publisher=[[Avinor]] |date=15 February 2012 |language=Norwegian |accessdate=17 February 2013  |archivedate=28 February 2013  |archiveurl=http://www.webcitation.org/6ElaccQdF}}</ref>

==Future==
Avinor, the agency responsible for running state-owned airports, conducted an analysis in 2012 of upgrading Kautokeino to a regional airport with a {{convert|1199|m|sp=us|adj=on}} runway. Kautokeino's population of 2,935 make an estimated 10,000 annual flights from [[Alta Airport]], located {{convert|135|km|sp=us}} by road and 1 hour and 50 minutes from the village. Avinor studied two alternative services, one with three daily flights directly to Tromsø and one using the existing [[Bombardier Dash 8|Dash 8]] aircraft network. The latter would require a stop-over at [[Sørkjosen Airport]] before Tromsø, allowing for two daily services, or directly with only one. Either way this gives an average 20 daily passengers per direction. Such a route would need [[subsidies]] of about 10&nbsp;million [[Norwegian krone]] per year (NOK).<ref name=ntp />

Construction of an airport is estimated to cost NOK&nbsp;530 million and have an annual operating cost of NOK&nbsp;28 million. An economic analysis showed that the airport would have a negative [[net present value]] for society of NOK&nbsp;1014 million, excluding operating subsidies for the airline. There is little time gain from the project as travelers to Oslo would have to fly via Sørkjosen to Tromsø and there change aircraft, instead of taking direct flights from Alta. Only passengers having Tromsø as travel target would have a gain from an airport. Avinor has recommended not upgrading the Kautokeino Airport.<ref name=ntp />

==Accidents and incidents==
A [[Junkers Ju 52|Junkers Ju 52/3m]] transport aircraft of the Luftwaffe crashed at the airport in 1944, resulting in a [[write-off]].<ref>{{cite web |url=http://aviation-safety.net/database/record.php?id=19449999-3 |title=Accident description |publisher=[[Aviation Safety Network]] |accessdate=17 February 2013}}</ref>

==References==
{{commons category|Kautokeino Airport}}
{{reflist}}

{{Airports in Norway}}
{{Portal bar|Aviation|World War II|Norway|Arctic}}

[[Category:Airports in Finnmark]]
[[Category:Kautokeino]]
[[Category:Luftwaffe airports in Norway]]
[[Category:Royal Norwegian Air Force airfields]]
[[Category:Military installations in Finnmark]]