{{prose|date=March 2015}}
'''Debt crisis''' is the general term for a proliferation of massive [[public debt]] relative to [[tax]] [[revenue]]s, especially in reference to Latin American countries during the 1980s, and the United States and the European Union since the mid-2000s. As well as the Chinese debt crises of 2015.<ref>[http://www.sfgate.com/cgi-bin/article.cgi?f=/g/a/2011/11/08/bloomberg_articlesLUCKTZ0D9L35.DTL "Europe Banks Selling Sovereign Bonds May Worsen Debt Crisis" - SFGate]</ref><ref>"Who is Handling Debt Crisis Better, United States or Europe" - US News [http://www.usnews.com/debate-club/who-is-handling-its-debt-crisis-better-united-states-or-europe]</ref><ref>[https://www.nytimes.com/interactive/2010/05/02/weekinreview/02marsh.html "Europe's Web of Debt" by Nelson D. Schwartz, New York Times]</ref><ref>[http://marginalrevolution.com/marginalrevolution/2011/11/hows-the-argentina-recovery-coming-along.html "How's the Argentina Recovery Coming Along?" by Tyler Cowen]</ref> <ref>http://fortune.com/2015/07/07/china-market-greece-debt/</ref>

==Current and recent debt crises==
===Europe===

==== European debt crisis ====
{{Main|European debt crisis}}
The [[Europe|European]] debt crisis is a crisis affecting several [[eurozone]] countries since the end of 2009.<ref name=bbceurocrisis>{{cite web|title=Timeline: The unfolding eurozone crisis|url=http://www.bbc.com/news/business-13856580|publisher=[[BBC News]]|accessdate=6 March 2015}}</ref><ref name=oecdeurocrisis>{{cite web|last1=Blundell-Wignall|first1=Adrian|title=Solving the Financial and Sovereign Debt crisis|url=http://www.oecd.org/finance/financial-markets/49481502.pdf|publisher=[[Organisation for Economic Co-operation and Development]]|accessdate=6 March 2015|date=2011}}</ref> Member states affected by this crisis were unable to repay their [[government debt]] or to bailout indebted financial institutions without the assistance of third-parties (namely the [[International Monetary Fund]], [[European Commission]], and the [[European Central Bank]]).<ref>{{cite web|title=Troika ‘governor’ of European ‘colonies’: Lawmakers fed up with debt crisis tactics|url=http://rt.com/business/troika-governor-european-colonies-822/|publisher=RT|accessdate=6 March 2015|date=27 February 2014}}</ref> The causes of the crisis included high-risk lending and borrowing practices, burst [[real estate bubble]]s, and hefty [[deficit spending]].<ref>{{cite web|last1=Brown|first1=Mark|last2=Chambers|first2=Alex|title=How Europe's governments have enronized their debts|url=http://www.euromoney.com/Article/1000384/BackIssue/50007/How-Europes-governments-have-enronized-their-debts.html|publisher=[[Euromoney]]|accessdate=6 March 2015|date=September 2005}}</ref> As a result, investors have reduced their exposure to European investment products, and the value of the [[Euro]] has decreased.<ref>{{cite web|last1=Johnson|first1=Steve|title=Investors slash exposure to the euro|url=http://www.ft.com/intl/cms/s/0/c7189960-be81-11e4-a341-00144feab7de.html#axzz3TdQjraab|publisher=[[Financial Times]]|accessdate=6 March 2015|date=1 March 2015|subscription=yes}}</ref>

<!-- summaries: work in progress -->

==== Other European debt crisises ====

* [[Greek government debt crisis]]
* [[Post-2008 Irish economic downturn|Irish financial crisis]]
* [[Economic history of Portugal#Economic crisis: the 2010s|Portuguese economic crisis]]

===Latin America===
* [[Argentine debt restructuring]]
* [[Latin American debt crisis]]

===North America===
* [[United States debt-ceiling crisis of 2011]]
* [[United States debt-ceiling crisis of 2013]]

==See also==
* [[Debt]]
* [[Debt of developing countries]]
* [[Government debt]]
* [[List of countries by credit rating]]
* [[List of countries by net international investment position]]
* [[List of countries by public debt]]
* [[Monetary sovereignty]]
* [[Sovereign default]]
* [[State (polity)]]
* [[Tax]]

==References==
{{Reflist}}

{{Debt}}

[[Category:Credit]]
[[Category:Debt|*]]
[[Category:Government finances]]
[[Category:Financial crises]]