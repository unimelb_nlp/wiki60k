{{distinguish|Food Addicts in Recovery Anonymous}}
'''Food Addicts Anonymous''' is a [[twelve-step program]] for people with [[compulsive overeating|food addiction]]s, that is patterned after the [[Alcoholics Anonymous]] program. It is based on the premise that some people are [[addiction|addict]]ed to refined high-[[carbohydrate]] foods and need to abstain from those foods in order to avoid overconsumption.<ref>[http://www.highbeam.com/doc/1G1-63385140.html Abstain from Carbs? 'Food Addicts' Do It One Day at a Time], ''[[Seattle Post-Intelligencer]]'', March 6, 2000</ref> 

==History and description==

Food Addicts Anonymous (FAA) was founded in 1987 in [[West Palm Beach, Florida]] by a founder who calls herself "Judith C."<ref name=FAAabout>[http://www.foodaddictsanonymous.org/about.htm What is Food Addicts Anonymous?], Food Addicts Anonymous website, accessed June 4, 2010a</ref><ref name=Massapequa>[http://www.massapequapost.com/news/2007/1128/Events_&_Calendar/032.html Food Addicts Anonymous to celebrate], ''Massapequa Post'', November 28, 2007</ref> By 2007 there were over 150 weekly meetings around the world in addition to phone and online meetings.<ref name=Massapequa/><ref>[http://www.nctimes.com/news/local/article_47b66be0-35ad-5e31-bbbf-a2907ee25ce6.html Food addicts find support in local meetings], ''North Country Times'', November 13, 2005</ref> The organization has meetings in the USA, Canada, Australia, England, Norway, Sweden and Ireland.

FAA holds that some people are addicted to certain foods and must abstain from them; like other twelve-step programs, FAA members believe that help from a higher power is necessary for them to avoid the substances they crave. The organization has a suggested food plan that calls for abstinence from [[sugar]], [[flour]], and [[wheat]]. (The call for abstinence from these specific foods is one difference between FAA and [[Overeaters Anonymous]].) All sugars, sugar substitutes, and [[artificial sweeteners]] are restricted by the plan. All forms of wheat and flour, including flours not made from wheat, are also restricted. Dietary fats are limited. FAA suggests that its members eat a variety of foods at specified intervals and in set proportions and keep track of what they eat.<ref name="FAAabout"/>

==References==
{{reflist}}

==External links==
* [http://www.foodaddictsanonymous.org/ Official website] 
* [http://www.foodaddictsanonymous.org/faa-food-plan FAA Food Plan]
* {{worldcat id|id=lccn-no96-68843}}

[[Category:Addiction and substance abuse organizations]]
[[Category:Eating disorders]]
[[Category:Organizations established in 1987]]
[[Category:Twelve-step programs]]

{{nonprofit-org-stub}}