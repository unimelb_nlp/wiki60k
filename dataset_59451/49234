{{Orphan|date=June 2016}}

{{Infobox scientist
| name              = Hong Kum Lee
| image    = Antarctic Women Hong Kum Lee.jpg
|caption = Prof Hong Kum Lee
| birth_name        = 
| nationality       = [[South Korea]]n
| fields            = Polar [[Biodiversity|microbial diversity]]<br>marine [[biotechnology]]
| workplaces        = Korea Polar Research Institute (KOPRI)
| alma_mater        = [[TU Braunschweig]]
| known_for         = Director General, Korea Polar Research Institute
| awards            = Hyeosin Medal Order of Science and Technology Merit
| module            = {{Infobox Korean name|child=yes
| hangul            = 이홍금
| hanja             = !
| mr                = I Hong'gŭm
| rr                = I Hong-geum}}
}}

'''Hong Kum Lee''' is an [[Antarctic]] researcher, best known for work as the Director General of the Korea Polar Research Institute (KOPRI).

==Early life and education==
Lee graduated from [[Seoul National University]] in microbiology and received her PhD degree at [[Braunschweig University of Technology|TU Braunschweig]], Germany in 1989. She completed a postdoc at the Seoul National University in 1990 and was then made a principal research scientist at Korea Ocean Research Institute (KORDI) from 1991 to 2004. She was head of national research laboratory for marine microbial diversity from 2001 to 2005. Since 2004 she is a principal research scientist at the Korea Polar Research Institute (KOPRI).<ref>{{Cite web|url=http://www.kopri.re.kr/english/eng_about/eng_staff/staffEng.do|title=Staff|date=|website=kopri.re.kr|publisher=Korea Polar Research Institute|access-date=2016-07-02}}</ref>

==Career and impact==
As President of KOPRI during 2007-2013<ref>{{Cite web|url=http://enews.kordi.re.kr/newshome/mtnmain.php?mtnkey=articleview&mkey=scatelist&mkey2=189&aid=791|title=기사보기|website=enews.kordi.re.kr|publisher=KIOST NEWS Today|trans-title=Polar Research Institute director re-elected|access-date=2016-06-18}}</ref> she was active in enhancing polar research infra-structure and strengthening international cooperation. She supported construction of South Korea's first research icebreaker, ARAON, delivered in 2009.<ref>{{Cite news|url=http://www.etnews.com/201005190206|date=2010-05-19|work=Electronic Times|script-title=국내 첫 쇄빙선 '아라온' 취항이 가장 보람|trans-title=Korea's first icebreaker ARA|access-date=2016-06-18}}</ref> ARAON equipped with state-of-the–art research facilities conducts multidisciplinary scientific research in geophysics and geology, oceanography and biology. As Chair of Local Organizing Committee, she devoted herself to the ASSW 2011(Arctic Science Summit Week 2011) held in Seoul.<ref>{{Cite web|url=http://www2.dmu.dk/1_om_dmu/2_afdelinger/3_am/faro-arctic/faro-arctic2ocproject/projectfiles/cms_mim_dk/nr/rdonlyres/a1c533bb-6b4a-4f6b-bb88-4fd7f7e57f23/0/assw_2011_2nd_circular.pdf|title=2011 Arctic Science Summit Week: Second Circular|date=2011|publisher=Local Organizing Committee of ASSW 2011|access-date=2016-06-19}}</ref>

She also supported the construction of the Jang-Bogo Antarctic Research Station in Terra Nova Bay of Northern Victoria Land in Antarctica, which was completed in 2014.<ref>{{Cite news|url=http://www.yonhapnews.co.kr/economy/2012/01/17/0325000000AKR20120117040151003.HTML?audio=Y|title=남극 장보고 과학기지 '첫삽'|trans_title='First shovel' for Jang Bogo scientific base in Antarctica|agency=[[Yonhap News Agency]]|date=2012-01-17|access-date=2016-06-18}}</ref> As a year-round station, Jang-Bogo Station serves as a platform for the research on climate change and developing the West Antarctic observatory network. As a member of IASC (International Arctic Science Committee) Review Committee from 2014 to 2015, she was appointed to carry out a review of progress for the period 2006-2016, including progress in implementing the recommendations of the 2006 Review Committee, and to recommend strategies for the future.<ref>{{Cite web|url=http://iasc.info/iasc/organization/2016-review|title=2016 Review - International Arctic Science Committee|last=User|first=Super|website=iasc.info|access-date=2016-06-18}}</ref> She joined ICSU RCAP (International Council of Science, Regional Committee for Asia and the Pacific) from 2009-2014. As Chair of ICSU RCAP during 2011-2014, she participated in the implementation of the ICSU Strategic Plan 2012-2017, and in developing Future Earth in Asia and the Pacific.<ref>{{Cite web|url=http://www.icsu.org/icsu-asia/news-centre/news/top-news/icsu-rcap-chair-for-2012-2015/image/image_view_fullscreen|title=ICSU RCAP Chair for 2012 - 2014|website=www.icsu.org|access-date=2016-06-18}}</ref> As an expert in marine and polar microbial diversity, she has strengthened her research activity as the head of National Research Laboratories for Marine Microbial Diversity, Ministry of Science and Technology. Not only reporting new microbial species, she has also screened and developed microbial exopolysaccharides and small molecules, which revealed biological activities such as antiviral, antioxidant activities and red tide algae-killing activities. She published more than 100 SCI papers and 25 patents.<ref>{{Cite web|url=https://www.researchgate.net/profile/Hong_Kum_Lee|title=Hong Kum Lee|website=ResearchGate|access-date=2016-06-18}}</ref>

Lee established and operates the Polar and Alpine Microbial Collection (PAMC), which shares biodiversity information and research bio-resources collected from polar and alpine areas.<ref>{{Cite web|url=http://pamc.kopri.re.kr/|title=Polar and Alpine Microbial Collection |website=pamc.kopri.re.kr|access-date=2016-06-18}}</ref> Approximately 2,800 microbial strains maintained in PAMC are ready to be provided in science and public communities with information on taxonomy, geographical origin, habitat and physiological characterization. PAMC was registered to international networks, the World Federation of Culture Collection (WFCC) and now functioning as an official depository institution.<ref>{{Cite web|url=http://www.wfcc.info/collections/|title=World Federation for Culture Collections|website=www.wfcc.info|access-date=2016-06-18}}</ref>

==Awards and honours==
Lee was awarded the Korea [[L'Oréal-UNESCO Awards for Women in Science|L'Oréal-UNESCO Award for Women in Science]] in Biological Sciences in 2007.<ref>{{Cite web|url=http://www.hankyung.com/news/app/newsview.php?type=2&aid=2007061932281&nid=910&sid=0104|title=한국로레알여성생명과학상 이홍금씨|trans-title=Lee Hong-kum receives L'Oréal-UNESCO Award for Women in Science|work=[[Korea Economic Daily]]|access-date=2016-06-18}}</ref> She was also awarded the Distinguished Woman Scientist of the Year by South Korea's [[Ministry of Science, ICT and Future Planning]] in 2015.<ref>{{Cite news|url=http://news.donga.com/3/all/20151222/75508162/1|script-title=ko:'2015년의 여성과학기술자상' 이홍금 김성연 박문정씨|trans_title=2015 Award for Women in Science, Engineering, and Technology to Lee Hong-kum, Kim Seong-yeon, Park Moon-jung|work=[[Dong-A Ilbo]]|date=2015-12-22|access-date=2016-06-18}}</ref>  Lee also was awarded the Hyeosin Medal Order of Science and Technology Merit by the [[President of South Korea]] in 2016.<ref>{{Cite web|url=http://www.metabuild.co.kr/ReportView.do?no=612|script-title=ko:'과학의날·정보통신의날' 합동 기념식…유공자 포상 전수|website=www.metabuild.co.kr|date=2016-04-21|access-date=2016-06-18}}</ref>

== Selected works ==
* Yim, Joung Han; Kim, Sung Jin; Ahn, Se Hun; Lee, Hong Kum (2007) Characterization of a novel bioflocculant, p-KG03, from a marine dinoflagellate, ''Gyrodinium impudicum'' KG03",Bioresource Technology 98(2):361-367<ref>{{Cite journal|last=Yim|first=Joung Han|last2=Kim|first2=Sung Jin|last3=Ahn|first3=Se Hun|last4=Lee|first4=Hong Kum|date=2007-01-01|title=Characterization of a novel bioflocculant, p-KG03, from a marine dinoflagellate, Gyrodinium impudicum KG03|url=http://www.sciencedirect.com/science/article/pii/S0960852406000058|journal=Bioresource Technology|volume=98|issue=2|pages=361–367|doi=10.1016/j.biortech.2005.12.021}}</ref>
* Lee, Yoo Kyung; Lee, Jung-Hyun; Lee, Hong Kum (2001) Microbial symbiosis in marine sponges Journal of Microbiology 39(4):254-264<ref>{{Cite web|url=https://www.researchgate.net/profile/Yoo_Kyung_Lee2/publication/268411386_Microbial_Symbiosis_in_Marine_Sponges/links/5546f8c80cf23ff71686f721.pdf|title=Microbial symbiosis in marine sponges|last=|first=|date=2001|website=|publisher=Journal of Microbiology|access-date=}}</ref>
* Jeong, Haeyoung; Yim, Joung Han; Lee, Choonghwan; Choi, Sang-Haeng; Park, Yon Kyoung; Yoon, Sung Ho; Hur, Cheol-Goo; Kang, Ho-Young; Kim, Dockyu; Lee, Hyun Hee (2005) Genomic blueprint of ''Hahella chejuensis'', a marine microbe producing an algicidal agent. Nucleic acids research 33(22)7066-7073<ref>{{Cite journal|last=Jeong|first=Haeyoung|last2=Yim|first2=Joung Han|last3=Lee|first3=Choonghwan|last4=Choi|first4=Sang-Haeng|last5=Park|first5=Yon Kyoung|last6=Yoon|first6=Sung Ho|last7=Hur|first7=Cheol-Goo|last8=Kang|first8=Ho-Young|last9=Kim|first9=Dockyu|date=2005-01-01|title=Genomic blueprint of Hahella chejuensis, a marine microbe producing an algicidal agent|url=http://nar.oxfordjournals.org/content/33/22/7066|journal=Nucleic Acids Research|language=en|volume=33|issue=22|pages=7066–7073|doi=10.1093/nar/gki1016|issn=0305-1048|pmc=1312362|pmid=16352867}}</ref>
* Yim, Joung Han; Kim, Sung Jin; Ahn, Se Hun; Lee, Chong Kyo; Rhie, Ki Tae; Lee, Hong Kum (2004) Antiviral effects of sulfated exopolysaccharide from the marine microalga ''Gyrodinium impudicum'' strain KG03,Marine biotechnology 6(1):17-25.<ref>{{Cite journal|last=Yim|first=Joung Han|last2=Kim|first2=Sung Jin|last3=Ahn|first3=Se Hun|last4=Lee|first4=Chong Kyo|last5=Rhie|first5=Ki Tae|last6=Lee|first6=Hong Kum|date=2003-09-29|title=Antiviral Effects of Sulfated Exopolysaccharide from the Marine Microalga Gyrodinium impudicum Strain KG03|url=http://link.springer.com/article/10.1007/s10126-003-0002-z|journal=Marine Biotechnology|language=en|volume=6|issue=1|pages=17–25|doi=10.1007/s10126-003-0002-z|issn=1436-2228}}</ref>

==References==
{{Reflist|35em}}

==External links==
* [https://scholar.google.com.au/citations?user=o8s7MYUAAAAJ&hl=en&oi=sra Hong Kum Lee] on [[Google scholar]]

{{Authority control}}

{{DEFAULTSORT:Lee, Hong Kum}}
[[Category:Seoul National University alumni]]
[[Category:Living people]]
[[Category:South Korean women scientists]]
[[Category:Year of birth missing (living people)]]
[[Category:Antarctic scientists]]
[[Category:Women Antarctic scientists]]