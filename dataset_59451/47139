[[File:Shepard Hall, CCNY Campus Harlem.jpg|thumb|Shepard Hall, CCNY Campus Harlem]]

'''The Colin L. Powell Center for Leadership and Service''' at the [[City College of New York]] (CCNY) is a nonpartisan educational, training, and research center named for its founder, [[Colin Powell|General Colin L. Powell, USA (Retired)]], a graduate of CCNY. The goals of the center are to build leaders for the common good, promote civic engagement, and strengthen connections between the campus and neighboring communities.

The Center is located at 160 Convent Avenue, on the fifth floor of Shepard Hall on the CCNY campus, in the [[Hamilton Heights]] section of [[Harlem|West Harlem]].

== Mission ==

The Center's mission is to prepare new generations of publicly engaged leaders from populations previously underrepresented in public service and policy circles, to build a strong culture of civic engagement at City College, and to mobilize campus resources to meet pressing community needs and serve the public good. The Center focuses its efforts in areas of community and economic development, education, health care, environmental concerns, international development, and global security issues.<ref>{{cite web|title=For Educators: Partners|url=http://www.cfr.org/educators/partners.html|publisher=Council on Foreign Relations|accessdate=2 August 2012}}</ref>

The Colin Powell Center works to forward the mission of the City College, which was founded in 1847 as the nation's first public higher education institution: to provide an affordable, world class education to “the children of the whole people," regardless of race, creed, class or economic status.<ref>{{cite web|title=CUNY City College College of Liberal Arts & Science|url=http://www.princetonreview.com/schools/graduate/GradBasics.aspx?iid=1034730|work=The Princeton Review 2012|publisher=The Princeton Review|accessdate=23 July 2012}}</ref>  The Center works with faculty and students to integrate its vision of leadership and service-oriented education in courses and research, and to act as a bridge between academia and the world of policy making.<ref>{{cite web|title=The New York Life Endowment for Emerging African-American Issues at the Colin Powell Center for Policy Studies|url=http://www.cuny.edu/about/invest-in-cuny/news-redirect/ev/gifts.html|publisher=CUNY.edu|accessdate=4 August 2012}}</ref>

== History ==

General Powell, who graduated from the City College in 1958, established the Center in 1997 as the Colin Powell Center for Policy Studies. Its original mission was to provide a base for the study of social and economic forces and conditions that impact New York City, by encouraging students and faculty to engage and partner with community organizations.<ref>{{cite web|last=Huffman|first=J. Ford|title=Portraits of Transformation: These Lives Were Changed by Carnegie Corporation of New York|url=http://carnegie.org/publications/carnegie-reporter/single/view/article/item/275/#colin|publisher=Carnegie Corporation of New York|accessdate=23 July 2012}}</ref>

Over the years, the Center's mission expanded to forward more fully the goals and vision of General Powell: to develop leadership skills and encourage service-learning and to address five key outcome areas: international development and global security, education, the environment, community and economic development, and health.

The name was changed to the Colin L. Powell Center for Leadership and Service in 2011 to reflect the shift toward this greater and more dynamic mission.<ref>{{cite web|title=New Name for the Center!|url=http://www1.ccny.cuny.edu/ci/powell/news/news_newname.cfm|publisher=City College of New York}}</ref>

== Service-Learning ==

The Colin Powell Center serves as the CCNY hub for [[service-learning]], a course-based, credit-bearing educational approach that links academic theory to concrete action. The pedagogy connects students, faculty and community partners through sustained relationships that build the capacity of those organizations, and gives students a practical perspective on classroom lessons as well as the opportunity to perform valuable service that addresses real needs.<ref>{{cite web|last=New York Metro Area Partnership for Service-Learning|title=Issues in Engaged Scholarship: An Exploration of Community-Campus Collaborations Vol. 1|url=http://issuu.com/colinpowellcenter/docs/issues_in_engaged_scholarship_vol._1_final|publisher=issuu|accessdate=2 August 2012}}</ref>

The Center works with the College to embed these concepts as an essential part of curricula. Since the inception of the service-learning program in 2005, more than 1,000 CCNY students have enrolled in these courses, providing over 25,000 hours of service to more than 50 nonprofit community organizations. Their work includes providing tutoring to East Harlem students, creating a media campaign for the New York Organ Donor Network, and working with the [[International Rescue Committee]]. Learning from the leaders and members of the organizations with which they work adds an indispensable perspective to the students' educations.

== NYMAPS ==

The Center leads the New York Metro Area Partnership for Service-Learning (NYMAPS), a coalition of more than 18 universities, colleges and community-based organizations that promotes experiential learning, active citizenship, and social responsibility among college students and faculty. Established in 2006, NYMAPS hosts an annual symposium, organized by the Center, that brings members together to report on their activities and results.<ref>{{cite news|last=Strom|first=Stephanie|title=Does Service Learning Really Help?|url=https://www.nytimes.com/2010/01/03/education/edlife/03service-t.html?pagewanted=all|accessdate=23 July 2012|newspaper=The New York Times|date=29 December 2009}}</ref>

NYMAPS member organizations include [[Columbia University]], [[Fordham University]], [[New York University]], the International [[YMCA]] and University Settlement.

== Fellowships and Scholarships ==

The Center operates a wide range of fellowship and scholarship programs for students. In 2012-2013, it will provide $690,000 of scholarship and internship support to 80 fellows.

=== Colin Powell Program in Leadership and Service ===
This two-year (one year for graduate students) program is designed for high achieving City College students of any CCNY major, program, or school. The program provides between $10,000 and $12,000 annually for two years for undergraduates, and a $15,000 stipend for graduate students. Fellows participate in specialized workshops, seminars, and mentoring. They become integrally involved in the Center’s activities and have the opportunity to participate in unique optional activities.

Current fellows have been awarded the prestigious Harry S. Truman Fellowship for Public Service<ref>{{cite web|title=Ayodele Oti: Working for World Health|url=http://urprod2.cuny.edu/about/people/notable-students.html?id=ayodeleoti|publisher=City University of New York|accessdate=2 August 2012}}</ref> and secured internships at the [[Council on Foreign Relations]],<ref>{{cite web|last=Jallow|first=Mohammed|title=Sierra Leone: Change You Can (Not) Believe In|url=http://blogs.cfr.org/campbell/2011/11/30/sierra-leone-change-you-can-not-believe-in/|work=Africa in Transition|publisher=Council on Foreign Relations|accessdate=2 August 2012}}</ref> the [[Ocean Alliance]], and the office of the Attorney General, among other notable establishments. Ninety percent of program alumni have gone on to careers in public service and public policy, at organizations such as the [[United States Department of Defense|Department of Defense]], the DELTA Enterprise Network, the Social Science Research Council, and the U.S. General Services Administration.

Many alumni pursue graduate studies, and have studied at Harvard University, Massachusetts Institute of Technology, Columbia University, the School of Foreign Service at Georgetown University and other leading institutions. In 2011, the Center received a record number of applications for fellowships.

Fellows in the Leadership and Service program are eligible for one of five scholarships:

* Ambassador John Price Scholarship is a one-year scholarship which provides $10,000 in support to a CCNY student whose academic interest focuses on a country or countries in Africa or on the Africa Diaspora.
* Fulvio Dobrich New Americans Scholarships are targeted toward first- and second-generation immigrants and are designed to extend the City College's historic mission of supporting new immigrants in New York City. The award was established in 2006 by Fulvio Dobrich, managing partner of Galileo Asset Management and a Colin Powell Center advisory council member.
* New York Life Fellowships and Scholarships for Emerging African-American Issues for both the graduate and undergraduate students focus on the study and understanding of African-American and other minority perspectives on a broad set of public problems and policy issues.<ref>{{cite web|title=City College of New York Receives $10 Million for Powell Center Endowment|url=http://foundationcenter.org/pnd/news/story_print.jhtml;jsessionid=K3FBEKPASD4FNLAQBQ4CGW15AAAACI2F?id=164000046|publisher=Philanthropy News Digest|accessdate=23 July 2012}}</ref><ref>{{cite web|title=Scholarships and Programming to Focus on Emerging African–American Issues|url=http://www.newyorklife.com/nyl/v/index.jsp?vgnextoid=49ad1219a49d2210a2b3019d221024301cacRCRD|publisher=New York Life|accessdate=2 August 2012}}</ref>  Graduate students receive $15,000 for the one-year program and participate in the Colin Powell Program in Leadership and Public Service, which includes mentoring, instruction and opportunities to develop the public and policy implications of their graduate research. Undergraduates are given $10,000 for each of two years and participate in the Colin Powell Program in Leadership and Public Service.<ref>{{cite web|last=Watson|first=Jamal|title=Colin Powell Center Gets $10 Million To Develop More Black Policy Makers|url=http://connection.ebscohost.com/c/articles/23705406/colin-powell-center-gets-10-million-develop-more-black-policy-makers|work=Diverse: Issues in Higher Education|accessdate=2 August 2012}}</ref>  These programs are supported through the New York Life Endowment for Emerging African-American Issues, established in 2006 with a $10 million grant from [[New York Life]].
* Korean Heritage Scholarship supports Korean language instruction to prepare students for participation in the Colin Powell Leadership Program.
* Colin Powell Leadership Fellowship is designed for students who do not receive one of the other four scholarships.

=== Partners for Change Fellowship Program ===
Fellows of the Partners for Change Fellowship Program are awarded $5,000 for the one-year program, inaugurated in 2011-2012. They engage in research in the field, advocacy, and service with a community organization. Fellows work in one of two areas: college access for Harlem residents or improving health care in Harlem and the South Bronx. Starting in 2012-2013, an environmental initiative will be added.

=== Community Engagement Fellowships Program ===
Community Engagement Fellows design and carry out a project that addresses community needs in a sustainable way. Projects generally concentrate on the Center's priority areas: international development and security, community and economic development, education, the environment, and health.<ref>{{cite web|title=Colin Powell Center Community Engagement Fellowships|url=http://www1.ccny.cuny.edu/ci/powell/aid/aid_engagement.cfm|work=Student Scholarships|publisher=Colin L Powell Center for Leadership and Service|accessdate=23 July 2012}}</ref>

=== Edward I. Koch Scholarship Program ===
Students who receive the Edward I. Koch Scholarship receive $5,000 of financial support and devote 200 or more hours of service each year to nonprofit organizations and government agencies throughout New York City. The scholarship is named for former New York City Mayor [[Ed Koch]], a graduate of the City College of New York. Fellows also participate in workshops and discussions that enhance their ability to engage communities in meaningful ways and that provide opportunities for leadership development, reflection and engaged learning.<ref>{{cite web|title=The Edward I. Koch Scholarship in Public Service|url=http://www.meritaid.com/page/meritAid/programDetail.jsp?id=190567&program=205709|work=CUNY City College Scholarships|publisher=MeritAid.com|accessdate=23 July 2012}}</ref>

== Research ==

The Center works to encourage [[community-based participatory research]], a collaborative approach through which academics work closely with members of community-based organizations on research to solve a pressing community problem or to address policy change. CCNY faculty are eligible for the Center's Community-Based Participatory Research Grant Program.

Additionally, faculty are eligible for the Center's Public Scholarship Program, designed to enable faculty to apply their expertise and research to advocate for public policy change or to shape the public debate in their field.

CCNY Professor Jean Krasno, the Center's initiative director for multilateral diplomacy and international organizations, recently led an effort to publish the collected papers of former United Nations Secretary General [[Kofi Annan]]. The six-year joint CCNY-Yale University project produced a five-volume set that contributes an organized historical record of Annan's selected public and declassified papers, and makes the breadth and depth of his work accessible to scholars, students, and policymakers.<ref>{{cite book|last=Krasno|first=edited by Jean E.|title=The collected papers of Kofi Annan, UN Secretary-General, 1997-2006|year=2012|publisher=Lynne Rienner Publishers|location=Boulder, Colo.|isbn=978-1-58826-803-7}}</ref>

== Advisory Council ==
The Center's advisory council, chaired by Colin Powell, is a noteworthy group of public figures, former government officials, business leaders, writers, and journalists, including:
* [[Madeleine K. Albright]]
* [[Tom Brokaw]]
* [[Carly Fiorina]]
* [[Richard N. Haass]]
* [[Henry A. Kissinger]]
* [[Sy Sternberg]]
* [[Barbara Walters]]
* [[Elie Wiesel]]
* [[Fareed Zakaria]]

==See also==
*[[City College of New York]]

==References==
{{reflist}}

== External links ==
* [http://www1.ccny.cuny.edu/ci/powell/index.cfm Colin L. Powell Center for Leadership and Service]
* [http://www1.ccny.cuny.edu/index.cfm City College of New York]
* [http://nymaps.org/2011/ New York Metro Area Partnership for Service-Learning]

{{Commons category|City College of New York}}

{{coord|40.82048|-73.94849|type:edu_globe:earth_region:US-NY|display=title}}

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->

{{DEFAULTSORT:Colin L. Powell Center for Leadership and Service}}
[[Category:City College of New York]]