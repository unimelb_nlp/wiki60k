{{Infobox former country
| conventional_long_name = Socialist Soviet Republic<br/>of Abkhazia
| native_name = Социалистическая Советская Республика Абхазия<br/>''Sotsalisticheskaya Sovetskaya Respublika Abkhaziya''
| common_name = Abkhazia
| continent = Asia
| status = 
| p1 = Democratic Republic of Georgia
| flag_p1 = Flag of Georgia (1990-2004).svg
| s1 = Abkhaz ASSR
| national_motto = ''[[Workers of the world, unite!|Proletarii vsekh stran, soyedinyaytes'!]]''<br/>Пролетарии всех стран, соединяйтесь!<br/><small>"Proletarians of all countries, unite!"</small>
| national_anthem = ''[[The Internationale|Internatsional]]''<br/>Интернационал<br/><small>"The Internationale"</small><br/><center>[[File:Internationale-ru.ogg]]</center>
| image_map = Europe location Abkhazia 1921.PNG
| image_map_caption = The Socialist Soviet Republic of Abkhazia in 1921.
| flag_s1 = Flag of Abkhazian ASSR.svg
| image_flag = Flag of Abkhazian SSR.svg
| image_coat = Coat of arms of the Abkhazian SSR.png
| symbol = Emblem of the Socialist Soviet Republic of Abkhazia
| symbol_type = State Emblem
| capital = Sukhumi
| common_languages = [[Russian language|Russian]], [[Abkhaz language|Abkhaz]], [[Armenian language|Armenian]], [[Georgian language|Georgian]]<sup>a</sup>
| government_type = [[Socialist Republic]]
| legislature = [[Congress of Soviets]]
| date_start = 31 March
| year_start = 1921
| date_end = 19 February
| year_end = 1931
| stat_year1 = 1921
| stat_area1 = 8600
| stat_pop1 = 
| currency = [[Soviet ruble|Ruble]]
| footnote_a = Also other languages, all of which could be used by public institutions per the 1925 constitution.<big><ref name=constitution/></big>
}}
The '''Socialist Soviet Republic of Abkhazia''' ({{lang-ru|Социалистическая Советская Республика Абхазия}}; ''Sotsalisticheskaya Sovetskaya Respublika Abkhaziya''), was a short-lived Soviet republic<ref>{{cite book |title=Small Nations and Great Powers: A Study of Ethnopolitical Conflict in the Caucasus |last=Cornell |first=Svante E. |year=2001 |publisher=Routledge |isbn=0-7007-1162-7 |pages=149}}</ref> in the territory of [[Abkhazia]] that existed from 31 March 1921 to 19 February 1931. It was an independent state from 21 May to 16 December 1921, when Abkhazia became a federal part of the [[Georgian SSR]]. SSR Abkhazia never became a [[Republics of the Soviet Union|Union-level republic]] within the [[Soviet Union]], despite the explicit expression of willingness and intent in its 1925 Constitution (Article 4).<ref name=constitution>[http://abkhazia.narod.ru/constitution1.htm 1925 Constitution of SSR Abkhazia]</ref> It had a special status of a “treaty republic” associated with the [[Georgian Soviet Socialist Republic]]<ref>Union treaty with Georgian SSR according to two constitutions. [http://abkhazia.narod.ru/constitution1.htm '''1925 Constitution of SSR Abkhazia''']: "The SSR Abkhazia, having united on the basis of a special union treaty with the SSR Georgia, enters through it the Transcaucasian Soviet Federative Socialist Republic and as part of the latter the Union of Soviet Socialist Republics"; '''[http://www.rrc.ge/law/konst_1922_03_02_ru.htm?lawid=127&lng_3=ru 1922 Constitution of the Socialist Soviet Republic of Georgia]''': "The Socialist Soviet Republic of Georgia includes on the basis of voluntary self-determination ... the Socialist Soviet Republic of Abkhazia, which is united with the Socialist Soviet Republic of Georgia on the basis of a special union treaty between these republics".</ref> through which Abkhazia was part of the [[Transcaucasian SFSR|Transcaucasian Soviet Federative Socialist Republic]] (since 12 March 1922) and thus also part of the Soviet Union (the Transcaucasian SFSR was a Union-level republic of the USSR from 1922 to 1936). The SSR Abkhazia was abolished in 1931 and transformed into the [[Abkhaz Autonomous Soviet Socialist Republic]] within the Georgian SSR.

== Creation ==

Abkhazia, hitherto an [[autonomous province]] within the [[Democratic Republic of Georgia]], came under the Soviet control in the course of the [[Russian SFSR|Soviet Russian]] [[Red Army invasion of Georgia|Red Army invasion]] of February–March 1921. On March 4, 1921, the Red Army, in conjunction with local revolutionary guerrillas, took control of Abkhazia’s capital, [[Sukhumi]], where a provisional Soviet administration – the Abkhaz [[Revolutionary committee (Soviet Union)|Revolutionary committee]] (Revkom) – was established. On March 31, 1921, a special conference attended by [[Sergo Orjonikidze]], [[Shalva Eliava]], [[Efrem Eshba]] and [[Nestor Lakoba]], declared the SSR of Abkhazia, but the question of the form of the republic's relations with both Georgia and Russia was left open. On May 21, 1921, the Georgian Revkom welcomed the formation of the "independent Socialist Soviet Republic of Abkhazia", and said the form of relations should be settled by the first Workers' Congresses of both republics.<ref>[http://www.rrc.ge/law/declar_1921_05_21_e.htm?lawid=112&lng_3=en Declaration of the Revolutionary Committee of the Soviet Socialist Republic of Georgia on Independence of the Soviet Socialist Republic of Abkhazia]. May 21, 1921. ''Regionalism Research Center'', February 4, 2008.</ref>

== Status ==

Finally, on December 16, 1921, Abkhazia signed a special treaty of alliance delegating some of its sovereign powers to the Georgian SSR. The treaty defined Abkhazia’s status as a “contractual republic” (Russian: договорная республика) and established a military, political and financial union between the two Soviet republics, subordinating the SSR of Abkhazia to the Georgian SSR in some of these areas. Thus, through the Georgian SSR, Abkhazia joined the [[Transcaucasian SFSR|Transcaucasian Soviet Federative Socialist Republic]] on March 12, 1922 and the [[Soviet Union]] on December 30, 1922.<ref>Bell, Imogen (2002), ''Eastern Europe, Russia and Central Asia'', p. 176. Taylor & Francis, ISBN 1-85743-137-5.</ref><ref>[[Stephen F. Jones|Jones, Stephen F.]] (Oct., 1988), The Establishment of Soviet Power in Transcaucasia: The Case of Georgia 1921-1928. ''Soviet Studies'', Vol. 40, No. 4, pp. 616-639.</ref>
[[File:Soviet caucasus1922.png|thumb|left|220px|A map of the Socialist Soviet Republic of Abkhazia, showing it in relation to the rest of the [[Caucasus]] in 1922]]
Abkhazia's ambiguous status of a "contractual republic" was written down into that republic’s April 1, 1925 Constitution which specified that "the SSR of Abkhazia, having united with the SSR of Georgia on the basis of a special treaty of union" was, through it, a part of the Transcaucasian SFSR and the USSR.<ref>[http://www.rrc.ge/law/konst_1992_01_04_e.htm?lawid=1398&lng_3=en Constitution of the Soviet Socialist Republic of Abkhazia]. April 1, 1925. ''Regionalism Research Center'', February 4, 2008.</ref> However, the [[1924 Soviet Constitution]] earlier referred to Abkhazia as an [[autonomous republic]].<ref>{{ru icon}} [http://ru.wikisource.org/wiki/%D0%9A%D0%BE%D0%BD%D1%81%D1%82%D0%B8%D1%82%D1%83%D1%86%D0%B8%D1%8F_%D0%A1%D0%A1%D0%A1%D0%A0_%281924%29_%D0%BF%D0%B5%D1%80%D0%B2%D0%BE%D0%BD%D0%B0%D1%87%D0%B0%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F_%D1%80%D0%B5%D0%B4%D0%B0%D0%BA%D1%86%D0%B8%D1%8F Конституция СССР (1924) первоначальная редакция]. [[Wikisource]] [http://users.cyberone.com.au/myers/ussr1924.html (English translation)]. Retrieved on February 4, 2008.</ref>  

On February 19, 1931, Abkhazia’s republican status was downgraded, on the orders of [[Joseph Stalin]], to that of an [[Abkhaz ASSR|Autonomous Soviet Socialist Republic]] within the Georgian SSR, reputedly as punishment of the Abkhaz Communist leadership under [[Nestor Lakoba]] for their failure to overcome the peasants' resistance to [[collectivization]].<ref name="Lang">[[David Marshall Lang|Lang, David Marshall]] (1962), ''A Modern History of Georgia'', p. 256. [[London]]: Weidenfeld and Nicolson.</ref>

== See also ==
*[[First Secretary of the Abkhaz Communist Party]]

== Notes ==
{{reflist}}

{{Republics of the Soviet Union}}

{{Coord|43|00|N|41|01|E|region:GE_type:adm1st|display=title}}

{{DEFAULTSORT:Abkhazia, Socialist Soviet Republic of}}
[[Category:1931 disestablishments in the Soviet Union]]
[[Category:Early Soviet republics]]
[[Category:Former socialist republics]]
[[Category:History of Abkhazia|Socialist Soviet Republic of Abkhazia]]
[[Category:History of Georgia (country)]]
[[Category:Georgian Soviet Socialist Republic]]
[[Category:States and territories established in 1921]]
[[Category:States and territories disestablished in 1931]]
[[Category:Republics of the Soviet Union]]
[[Category:1921 establishments in the Soviet Union]]

{{Georgia-hist-stub}}