{{featured article}}
{{featured article}}{{italic title}}
{{Infobox video game
| title         = Thirty Flights of Loving
| collapsible   =
| state         =
| show image    =
| image         = Thirty Flights of Loving cover.png
| caption       =
| developer     = [[Blendo Games]]
| publisher     =
| distributor   =
| series        =
| engine        = [[Quake II engine|id Tech 2]]
| platforms     = [[Microsoft Windows]], [[OS X]]
| released      = '''Microsoft Windows'''{{Video game release|WW|August 2012}}'''OS X'''{{Video game release|WW|November 2012}}
| genre         = [[Adventure game|Adventure]]
| modes         = [[Single-player video game|Single-player]]
| designer      =
| composer      = [[Chris Remo]]
}}
'''''Thirty Flights of Loving''''' is a [[First-person (video games)|first-person]] [[Adventure game|adventure]] video game developed by Brendon Chung's [[independent video game development|indie video game]] studio, [[Blendo Games]]. It was released in August 2012 for [[Microsoft Windows]], and in November 2012 for [[OS X]]. The game employs a modified version of [[id Software]]'s 1997-era [[id Tech 2]] engine—originally used for ''[[Quake 2]]''—and incorporates music composed by [[Idle Thumbs]] member [[Chris Remo]]. It follows three people as they prepare for an alcohol heist and the aftermath of the operation.

The game is a non-direct sequel to ''[[Gravity Bone]]'' (2008) and features the same main character—an unnamed spy. It was developed as part of the [[Kickstarter]] campaign for the revival of the Idle Thumbs podcast and included a free copy of its predecessor. ''Thirty Flights of Loving'' received generally favorable reviews from [[Video game journalism|video game media outlets]], scoring 88 out of 100 on aggregate website [[Metacritic]]. A follow-up, ''[[Quadrilateral Cowboy]]'', was released on July 25, 2016.

== Gameplay ==
''Thirty Flights of Loving'' is a [[First-person (video games)|first-person]] [[Adventure game|adventure]] video game that is estimated to take about 15 minutes on average to complete.<ref name="pcgamer 13min"/> Using the [[Arrow keys#WASD keys|WASD keys]] and mouse,<ref name="DevMan"/> the player controls the main character, an unnamed spy who participates in an alcohol-smuggling operation. The player works alongside [[non-playable character]]s Anita, a demolitions expert, and Borges, a forger.<ref name="eurogamer review"/> The game follows the group as they prepare for a heist and experience its aftermath. The robbery is omitted from the game, although it is revealed that it went wrong.<ref name="eurogamer review"/>

[[File:Thirty flights of loving screenshot.jpg|thumb|left|''Thirty Flights'' uses the same blocky style of art as ''Gravity Bone''. Here, the player and Borges are in their hideout, planning the heist.]]

Unlike ''Gravity Bone'', ''Thirty Flights of Loving'' employs [[non-linear storytelling]], forcing the player to piece together the narrative.<ref name="eurogamer review"/> During gameplay, objectives and guidance are provided through the player's interactions with objects. The player has little control over the game mechanics and is only able to move freely and pick up objects as needed to progress. Several optional actions, such as drinking alcohol, are available at several stages of the game.<ref name="eurogamer review"/>

=== Story ===
''Thirty Flights of Loving'' begins with the player walking through a small corridor where individual gameplay elements such as movement and key allocations are explained. After walking through a bar and several more corridors, Anita and Borges are introduced. All three characters then exit on a plane. A [[smash cut]] skips the narrative forward to a scene with Anita and Borges lying shot in a room full of crates. The player character lifts Borges and takes him outside to what looks to be an airport. The player is then taken to a dark room with Anita sitting on a chair, peeling and eating oranges. After walking through another corridor, Anita, Borges, and the player join a wedding.

Anita and the player get drunk on a table while the rest of the characters start dancing and flying across the room. Then the player is taken again to the room where Anita was peeling oranges, and then back to the room where both she and Borges were lying<!--diction?  are they putting shot down on a substrate?--> shot. The player is then shown leaving the airport carrying Borges on a luggage cart. They arrive at a small place where the gunfight sequence takes place, followed by the motorcycle ride sequence, which ends with a crash that leads the player into a museum. In this area, there are several plaques showing the game's name and credits. The player leaves the area and goes into a new one where [[Bernoulli's principle]] about low and high air pressures is explained. Then, the player is again moved to the motorcycle sequence, where the game ends.

== Development ==
[[File:Brendon Chung at GDC 2012.jpg|thumbnail|right|Brendon Chung, developer of ''Thirty Flights of Loving'']]

''Thirty Flights of Loving'' was developed by Brendon Chung's video game studio [[Blendo Games]]. Chung, who worked as a [[Level design#Level designer|level designer]] for [[Pandemic Studios]], has contributed to the development of ''[[Full Spectrum Warrior]]'' and ''[[Lord of the Rings: Conquest]]''. ''Thirty Flights of Loving'' was created using a modified version of KMQuake II, a port of [[id Software]]'s [[id Tech 2]], the [[game engine|graphics engine]] for ''Quake 2''.<ref name="Dev1"/> It incorporates a gameplay enhancement add-on named Lazarus, developed by David Hyde and Mad Dog.<ref name="gamasutra igf"/> Chung acknowledged that although he has worked with newer, "powerful and flexible" engines, he preferred the older engine because it was released as an [[open-source platform]], "so you can redistribute it for free."<ref name="Dev1"/> The source code of ''Thirty Flights of Loving'' itself has been released under version 2 of the [[GNU General Public License]], making it [[free software]].<ref>{{cite web | url = http://blendogames.com/thirtyflightsofloving/faq.htm | title = Thirthy Flight of Loving FAQ | accessdate = November 11, 2016 }}</ref>

The game was first conceived as a prototype to ''Gravity Bone'', and was scrapped because it was "too dialogue heavy." However, Chung revived the idea after being contacted by [[Idle Thumbs]] to develop a game for their Kickstarter campaign.<ref name="DevComm"/> The main development phase, in which content creation took place, was finished within three months. Several more months were spent polishing the game and fixing [[software bug]]s. Chung brought multiple existing assets from ''Gravity Bone'' to develop ''Thirty Flights of Loving'',<ref name="gamasutra igf"/> and used a diverse set of tools to create the elements of the game. [[Blender (software)|Blender]] was picked for the creation of models, while [[Audacity (audio editor)|Audacity]] and [[Adobe Photoshop]] were used for audio and texture work. Another tool, [[GtkRadiant]], was used to create the game's levels.<ref name="gamasutra igf"/>

Chung developed ''Thirty Flights of Loving''{{'s}} environment as a way to present the criminal nature of the group. He intentionally avoided the use of voice-overs, and instead modeled the environment to bridge "the disconnect between the player's knowledge and the player's character's knowledge."<ref name="DevComm"/> Characters Anita and Borges were to be introduced using dialogue, but this was removed. However, montages were later added after Idle Thumbs' crew expressed concerns that the characters' relationships were unclear.<ref name="DevComm"/> Chung included a system to automate the generation of non-playable characters to replace the process of manually scripting every person in the game. He explained that although it allows characters to "randomly wander near waypoints," the software is "occasionally glitchy and behaves badly around staircases." This automation code was originally developed for a surveillance game prototype "that never panned out."<ref name="DevComm"/>

A first-person meal simulator was designed for ''Thirty Flights of Loving''. The sequence included the main characters "enjoy[ing] street noddles." However, the idea was scrapped and replaced with the motorcycle ride featured in the final version.<ref name="DevComm"/> The gunfight scene portrayed in the game was supposed to have a "musical rhythm," inspired by the film ''Koyaanisqatsi and Baraka''.<ref name="DevComm"/> The last level of the game is modeled from the [[National Museum of Natural History (France)|French National Museum of Natural History]].<ref name="DevComm"/> Chung explained that when developing levels, he first spends time researching and "learning how things work." He elaborated that researching is important in "how it gives specificity and grounding" to a game.<ref name="DevComm"/> ''Thirty Flights of Loving'' is the seventh "Citizen Abel" game developed by Chung. The first two games were coded in 1999, while the following three were written between 2000 and 2004.<ref name="DevComm"/> The sixth game in the series, ''Gravity Bone'' (2008), became the first to be published.<ref name="DevComm"/> On the [[Idle Thumbs|Tone Control]] podcast, he spoke about how every game he has produced, including ''Thirty Flights of Loving'', takes place in the same shared universe.<ref name="tone control"/>

''Thirty Flights of Loving'' includes references and [[Easter egg (media)|Easter eggs]], as did ''Gravity Bone''. Films such as ''[[Three Days of the Condor]]'' and ''[[The Conversation]]'', film directors [[Steven Soderbergh]] and [[Quentin Tarantino]], games such as ''[[Zork]]'' and ''[[Saints Row: The Third]]'', and animated shows like ''[[Animaniacs]]'' and ''[[TaleSpin]]'' are referenced in the campaign.<ref name="rps ref"/> Unlike most of Chung's previous games, ''Thirty Flights of Loving'' was not framed around a certain musical composition.<ref name="DevComm"/> It incorporates music composed by Idle Thumbs member [[Chris Remo]],<ref name="1up 2012"/> while additional audio was provided by [[Jared Emerson-Johnson]] and [[A.J. Locascio]]. It makes use of Soundsnap's sound library.<ref name="DevMan"/>

== Release ==
''Thirty Flights of Loving'' was announced in February 2012 as part of the [[Kickstarter]] campaign for Idle Thumbs' podcast.<ref name=sequel1/><ref name=sequel2/> The Idle Thumbs team talked to Chung about a possible sequel to ''Gravity Bone'', which was offered as one of the rewards of their Kickstarter campaign.<ref name="Kicks"/> Those who supported the campaign received ''Thirty Flights of Loving'' before its official release on August 2012.<ref name="steam release"/> They also gained access to an exclusive "Goldblum mode" that was not part of the general release. It replaced the character model with ones resembling actor [[Jeff Goldblum]].<ref name="steam release"/><ref name="gametrailers goldblum"/> The game, alongside a free copy of ''Gravity Bone'', was made available to early supporters in July 2012 and to the general public a month later via [[Steam (software)|Steam]].<ref name="steam release"/> A [[Mac OS X]] release followed in November 2012.<ref name="Joystiq"/>

== Reception ==
{{Video game reviews
| MC = 88/100<ref name="Metac"/>
| Destruct = 9.5/10<ref name="destruct"/>
| Edge = 9/10<ref name="edge review"/>
| EuroG = 9/10<ref name="eurogamer review"/>
| IGN = 8/10<ref name="ign review"/>
}}
''Thirty Flights of Loving'' received generally favorable reviews upon release. On [[Metacritic]], which assigns a normalized rating out of 100 to reviews from mainstream critics, the game received an average score of 88 out of 100, based on 10 reviews.<ref name="Metac"/> [[Destructoid]]{{'s}} Patrick Hancock awarded the game 9.5 out of 10, stating that "you'll never look at linear storytelling the same way again."<ref name="destruct"/>

[[GameSpot]]'s Carolyn Petit wrote that "the pleasure of ''Thirty Flights of Loving'' emerges from the things left unshown", allowing the player to infer and imagine the events, such as the heist itself, that are not otherwise shown.<ref name="GP1"/> Graham Smith of ''[[PC Gamer]]'' extolled the minimalist storytelling, asserting that ''Thirty Flights of Loving'' "tells a better story in 13 minutes than most games do in 13 hours".<ref name="pcgamer 13min"/> Mark Brown from ''[[Wired UK]]'' classified the game as a "brassy, super-short, cubic heist drama," and stated that Chung "spins a memorable yarn, delivers it with confidence and panache [...] with a 15-year-old engine, without voice acting, in 20 minutes."<ref name=wired1/>

[[IGN]]'s Nathan Meunier said the game "gets off to a fascinating start before completely throwing any and all expectations you might form during its first few minutes into the wood chipper."<ref name="ign review"/> British video game magazine ''[[Edge (magazine)|Edge]]'' found ''Thirty Flights of Loving'' to be "an intriguing psychological thriller that feels like [[Wes Anderson]] taking on [[Alfred Hitchcock|Hitchcock]]."<ref name="edge review"/> The magazine added that the game had a "wonderfully ambiguous" story, crafted by replacing dialogue with "artful framing and shrewd gestures, and booting out cutscenes in favour of prickly jump-cuts."<ref name="edge review"/> Greek magazine ''[[PC Master]]'' praised the game's storytelling and stated that ''Thirty Flights of Loving'' "attempts to blur the lines between gaming and art."<ref name=pcmaster/> ''Thirty Flights of Loving'' was a Narrative Award finalist at the 2013 [[Independent Games Festival]]. However, Richard Hofmeier's ''[[Cart Life]]'' (2011) became the winner.<ref name=IGF-Winners/>
{{Clear}}

== Sequel ==
{{main article|Quadrilateral Cowboy}}
A follow-up to ''Thirty Flights of Loving'', ''[[Quadrilateral Cowboy]]'', is being developed by Chung. The game takes place in the same universe as ''Gravity Bone'' and ''Thirty Flights of Loving'' but is not a direct sequel. It follows a hacker who oversees agents who infiltrate buildings and steal documents.<ref name="eurogamer preview"/> Unlike its predecessors, ''Quadrilateral Cowboy'' uses id Software's [[id Tech 4]] engine—originally used for ''[[Doom 3]]''. According to Chung, the new engine provides "a lot more modern functionality" than the earlier engine used in the first two games.<ref name="ign preview"/>

== References ==
{{reflist|30em|refs=
<ref name=wired1>{{cite web|last=Brown|first=Mark|title=Dramatic, minimalist 30 Flights of Loving is a heist game without the heist|url=http://www.wired.co.uk/news/archive/2012-08/22/30-flights-of-loving|work=[[Wired UK]]|accessdate=22 January 2014|date=22 August 2012|archivedate=26 March 2013|archiveurl=https://web.archive.org/web/20130326082148/http://www.wired.co.uk/news/archive/2012-08/22/30-flights-of-loving}}</ref>

<ref name=IGF-Winners>{{cite news|title=2013 Independent Games Festival Winners & Finalists|url=http://www.igf.com/2013finalistswinners.html|work=[[Independent Games Festival]]|accessdate=17 January 2014|archiveurl=https://web.archive.org/web/20130808015049/http://www.igf.com/2013finalistswinners.html|archivedate=2 January 2014|location=United States|year=2013}}</ref>

<ref name="DevComm">Chung, Brendon. ''Thirty Flights of Loving'' (developer commentary). [[Blendo Games]] (2012). Retrieved 13 January 2014.</ref>

<ref name="DevMan">Chung, Brendon. ''Thirty Flights of Loving'' (manual). [[Blendo Games]] (2012). Retrieved 13 January 2014.</ref>

<ref name="pcgamer 13min">{{cite web|last=Smith|first=Graham|title=Thirty Flights of Loving tells a better story in 13 minutes than most games do in 13 hours|url=http://www.pcgamer.com/2012/03/06/thirty-flights-of-loving-does-more-with-story-in-13-minutes-than-most-games-do-in-13-hours/|work=[[PC Gamer]]|accessdate=January 22, 2013|date=March 6, 2012}}</ref>

<ref name="1up 2012">{{cite web | url = http://www.1up.com/news/1up-favorite-games-2012-flights | title = 1UP's Favorite Games of 2012: Thirty Flights of Loving | first = Marty | last = Sliva | date = December 25, 2012 | accessdate = January 31, 2013 | website= [[1UP.com]] }}</ref>

<ref name="rps ref">{{Cite web | url = http://www.rockpapershotgun.com/2013/02/01/the-27-homages-of-thirty-flights-of-loving/ | title = The 27 Homages Of Thirty Flights Of Loving | first = David | last=  Valjalo | date = 2013-02-01 | accessdate = 2013-02-01 | work= [[Rock, Paper, Shotgun]] }}</ref>

<ref name="Dev1">{{cite web|last=Chick|first=Tom|title=The man behind the strange wonderful world of Gravity Bone|url=http://fidgit.com/archives/2009/01/the-man-behind-the-strange-won.php|work=FidGit|publisher=Sci Fi|accessdate=November 15, 2012|archiveurl=https://web.archive.org/web/20090227075049/http://fidgit.com/archives/2009/01/the-man-behind-the-strange-won.php|archivedate=February 27, 2009|date=January 1, 2009}}</ref>

<ref name="gamasutra igf">{{cite web | url = http://www.gamasutra.com/view/news/184739/Road_to_the_IGF_Blendo_Games_Thirty_Flights_of_Loving.php | title = Road to the IGF: Blendo Games' Thirty Flights of Loving | first = Mike | last = Rose | date = January 16, 2013 | accessdate  = January 30, 2013 | website= [[Gamasutra]] }}</ref>

<ref name=sequel1>{{cite web | last=Meer |first=Alec | date=February 28, 2012 | url=http://www.rockpapershotgun.com/2012/02/28/gravity-bones-sequel-thirty-flights-of-loving/| title=Gravity Bone's Sequel: Thirty Flights Of Loving| work= [[Rock, Paper, Shotgun]] | accessdate=January 30, 2013}}</ref>

<ref name=sequel2>{{cite web|last=Hamilton|first=Kirk|date=February 28, 2012|url=http://kotaku.com/5889082/indie-darling-gravity-bone-gets-a-sequel|title=Indie Darling Gravity Bone Gets a Sequel|work=[[Kotaku]]|accessdate=March 8, 2012}}</ref>

<ref name="Kicks">{{Cite web | url = http://www.kickstarter.com/projects/idlethumbs/idle-thumbs-video-game-podcast/posts/181780 | title = Thirty Flights of Loving Trailer | date = February 28, 2012 | accessdate = January 30, 2013 | publisher = [[Kickstarter]] }}</ref>

<ref name="gametrailers goldblum">{{cite web | url = http://www.gametrailers.com/side-mission/25309/reset-thirty-flights-of-loving-is-the-best-cutscene-ever | title = Reset: Thirty Flights of Loving is the Best Cutscene Ever | first = Ryan | last=  Stevens | website= [[GameTrailers]] | date = August 11, 2012 | accessdate = January 10, 2014 }}</ref>

<ref name="steam release">{{Cite web | url = http://www.polygon.com/gaming/2012/8/21/3257862/thirty-flights-of-loving-now-available-gravity-bone | title = 'Thirty Flights of Loving' now available, includes 'Gravity Bone' | website= [[Polygon (website)|Polygon]] | date = August 21, 2012 | accessdate  =January 30, 2013| first = Samit | last =  Sarkar }}</ref>

<ref name="Joystiq">{{cite web | url = http://www.joystiq.com/2012/11/14/thirty-flights-of-loving-now-seducing-mac/ | title = Thirty Flights of Loving now seducing Mac | first = David | last=  Hinkle | date = November 14, 2012 | accessdate = January 31, 2013 | website= [[Joystiq]] }}</ref>

<ref name="Metac">{{cite web|title=Thirty Flights of Loving|url=http://www.metacritic.com/game/pc/thirty-flights-of-loving|work=[[Metacritic]]|accessdate=January 22, 2013}}</ref>

<ref name="edge review">{{cite web | url = http://www.edge-online.com/review/thirty-flights-loving-review/ | title = Thirty Flights Of Loving review | date = August 28, 2012 | accessdate = January 31, 2013 | work = [[Edge (magazine)|Edge]] | archiveurl = https://web.archive.org/web/20121001092455/http://www.edge-online.com/review/thirty-flights-loving-review/ | archivedate = October 1, 2012 }}</ref>

<ref name="destruct">{{cite web|last=Hancock|first=Patrick|title=Review: Thirty Flights of Loving|url=http://www.destructoid.com/review-thirty-flights-of-loving-235183.phtml|work=[[Destructoid]]|accessdate=January 22, 2013|date=September 18, 2012}}</ref>

<ref name="GP1">{{Cite web | url = http://www.gamespot.com/features/thirty-flights-of-loving-one-of-the-least-reviewable-games-of-the-year-6397958/ | title = Thirty Flights of Loving: One of the Least Reviewable Games of the Year | work = [[GameSpot]] | date = October 9, 2013 | accessdate = February 5, 2013 | first = Carolyn | last = Petit }}</ref>

<ref name="ign review">{{cite web|last=Meunier|first=Nathan|title=Thirty Flights of Loving Review|url=http://www.ign.com/articles/2012/08/29/thirty-flights-of-loving-review|work=[[IGN]]|accessdate=January 22, 2013|date=August 29, 2012}}</ref>

<ref name=pcmaster>{{cite news|title=Thirty Flights of Loving review|newspaper=[[PC Master]]|date=October 2012|location=Greece}}</ref>

<ref name="ign preview">{{cite web | url = http://www.ign.com/articles/2013/02/04/what-the-hell-is-quadrilateral-cowboy | title = What the Hell is Quadrilateral Cowboy? | first = Colin | last = Campbell | date = February 4, 2013 | accessdate = February 7, 2013 |work=[[IGN]] }}</ref>

<ref name="eurogamer preview">{{cite web | url = http://www.eurogamer.net/articles/2013-02-06-thirty-flights-of-loving-dev-shows-off-upcoming-cyberpunk-game-quadrilateral-cowboy | title = Thirty Flights of Loving dev shows off upcoming cyberpunk game Quadrilateral Cowboy | work = [[Eurogamer]] | date = February 6, 2013 | accessdate = February 7, 2013 | first = Jeffrey | last = Matulef }}</ref>

<ref name="eurogamer review">{{cite web | url = http://www.eurogamer.net/articles/2012-08-23-thirty-flights-of-loving-review | title = Thirty Flights of Loving Review | work = [[Eurogamer]] | date = August 23, 2012 | accessdate = January 30, 2013 | first = Oli | last = Welsh }}</ref>

<ref name="tone control">{{cite web | url = https://www.idlethumbs.net/tonecontrol/episodes/brendon-chung | title = Tone Control - Episode 7, Brendon Chung | work=[[Idle Thumbs]] | date = January 15, 2014 | accessdate = January 22, 2014 }}</ref>

}}
{{Blendo Games}}

[[Category:2012 video games]]
[[Category:Kickstarter projects]]
[[Category:Crowdfunded video games]]
[[Category:MacOS games]]
[[Category:Single-player-only video games]]
[[Category:Video game sequels]]
[[Category:Video games developed in the United States]]
[[Category:Video games with commentaries]]
[[Category:Windows games]]
[[Category:Exploration video games]]