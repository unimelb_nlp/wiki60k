{{good article}}
{{Infobox cycling race report 
| name               = 1912 Giro d'Italia
| image              = Giro Italia 1912-map.png 
| image_caption      = Overview of the stages: route clockwise from Milan, down 
to Pescara, over to Rome, and up to Bergamo
| date               = 19 May – 4 June
| stages             = 9
| distance           = 2439.6
| unit               = km
| time               = 100h 02' 57"
| speed              = 27.323
| first              = [[Atala (cycling team)|Atala-Dunlop]]
| second             = [[Peugeot (cycling team)|Peugeot]]
| third              = Gerbi
| previous = [[1911 Giro d'Italia|1911]]
|next = [[1913 Giro d'Italia|1913]]
}}
The '''1912 Giro d'Italia''' was the 4th&nbsp;edition of the [[Giro d'Italia]], a cycling race set up and sponsored by the [[newspaper]] ''[[La Gazzetta dello Sport]]''. The race began on 19 May in [[Milan]] with a stage that stretched {{convert|398.8|km|0|abbr=on}} to [[Padua]]. The race was composed of nine stages that covered a total distance of {{convert|2733.6|km|0|abbr=on}}. The race came to a close in [[Bergamo]] on 4 June after a {{convert|235|km|0|abbr=on}} stage. The race was won by the [[Atala (cycling team)|Atala-Dunlop]] team that finished with [[Carlo Galetti]], [[Eberardo Pavesi]], and [[Giovanni Micheletto]]. Second and third respectively were [[Peugeot (cycling team)|Peugeot]] and Gerbi.

The calculation of the [[General classification in the Giro d'Italia|general classification]] changed from the previous editions of the race, shifting to a team-based event, with each team only allowed four riders. Points were awarded to teams based upon winning the stage, having multiple riders in the top four places in each stage, and finishing the stage with a minimum of three riders. A total of fourteen teams participated, with 56 riders registering and 54 officially starting the race.

Micheletto won the opening stage of the race to give Atala-Dunlop the first lead in the race. Despite two consecutive stage wins by [[Legnano-Pirelli|Legnano]] riders, Atala-Dunlop still retained the lead going into the fourth stage. The fourth leg was held in rainy conditions that caused some rivers to overflow on the course and riders to take the wrong roads for extensive distances; this led to the cancellation of the stage and addition of a ninth leg that drew some of its route from the [[Giro di Lombardia]]. An Atala-Dunlop rider won the fifth stage, but the team lost the lead to Peugeot for one stage. Following the stage, Atala-Dunlop regained the lead and held that to the race's finish in Bergamo.

==Changes from the 1911 Giro d'Italia==

Outside the yearly changes in the route, race length, and number of stages, the biggest change to the how the general classification was to be calculated.<ref name="BRI 1912">{{cite web|url=http://bikeraceinfo.com/giro/giro1912.html |title=1912 Giro d'Italia |work=Bike Race Info |date= |author= Bill and Carol McGann |publisher=Dog Ear Publishing|accessdate=2012-07-10|archiveurl=https://web.archive.org/web/20141218040432/http://bikeraceinfo.com/giro/giro1912.html|archivedate=17 December 2014|deadurl=no}}</ref> The organizers chose to make the general classification centered around teams rather than individuals like the race was contested in the first three years of its existence.<ref name="BRI 1912"/> They chose to only allow teams of four riders to compete in the race, making this the first Giro d'Italia to not contain any independent riders.<ref name="BRI 1912"/><ref name="CR 12">{{cite web|url=http://www.cyclingrevealed.com/timeline/Race%20Snippets/GdI/GdI_1912.htm |title=The Giro in 1912&nbsp;... A Team Competition |work=CyclingRevealed |author=Barry Boyce |publisher=CyclingRevealed|accessdate=2012-07-10}}</ref> Each team was required to have three riders finish each stage in order to remain in the race.<ref name="BRI 1912"/> The general classification was still determined by a point system: four points went to the team that the stage winner came from, if a team got two riders in the top four placings on a stage they got two points, and if a team completed the stage with the minimum three riders, they earned one point.<ref name="BRI 1912"/>

The new point system for the general classification was met with some resistance as many people were very confused by how the new system operated or simply preferred the original system the organizers had used in years past.<ref name="LS GC Confus">{{cite news|url=http://www.archiviolastampa.it/component/option,com_lastampa/task,search/mod,libera/action,viewer/Itemid,3/page,7/articleid,1193_01_1912_0138_0007_17939762/anews,true/|title=Il IV Goro d'Italia è incominciato!&nbsp;... |language=Italian|date=19 May 1912|page=7|newspaper=La Stampa|publisher=Editrice La Stampa|accessdate=27 May 2012|trans_title=The Fourth Tour of Italy Has Begun!&nbsp;...}}</ref> In response to the confusion surrounding the general classification format, the race organizers released a supplement to help better explain how the new points system operated.<ref name="LS GC Confus"/> A ''La Stampa'' writer claimed that the supplement provided several lengthy example calculations and stated that they would know after the first stage how successful this new system will be.<ref name="LS GC Confus"/>

The team that won general classification won a grand prize of 4000 lire.<ref name="LS B4">{{cite news|url=http://www.archiviolastampa.it/component/option,com_lastampa/task,search/mod,libera/action,viewer/Itemid,3/page,5/articleid,1193_01_1912_0137_0005_24837122/|title=Il IV Giro ciclistico d'Italia|language=Italian|date=18 May 1912|page=5|newspaper=La Stampa|publisher=Editrice La Stampa|accessdate=27 May 2012|trans_title=The Fourth Cycling Tour of Italy}}</ref> The runner up of the race received 2000 lire, third was rewarded with 1000 lire, fourth got 600 lire, and fifth place received 400 lire.<ref name="LS B4"/> Each stage winner received 600 lire and second place on the day got 300 lire.<ref name="LS B4"/>

==Participants==

{{mainlist|List of teams and cyclists in the 1912 Giro d'Italia}}
Of the 56 riders that signed up to participate in the 1912 Giro d'Italia, 54 of them began the race on 19 May.<ref name="BRI 1912"/> Since the race's general classification was to be based around team points there were no independent riders as in years past.<ref name="BRI 1912"/> The organizers allowed the participating teams to have up to four riders,<ref name="CR 12"/> with each team needing three riders to finish each stage to remain in the race.<ref name="BRI 1912"/> There were a total of fourteen teams that started the 1912 Giro d'Italia.<ref name="BRI 1912"/><ref name="CR 12"/><ref name="LS B4"/> A French team was planning on competing in the race, but opted not to and so all the teams that competed were based in Italy.<ref name="BRI 1912"/> This edition of the Giro d'Italia saw the first teams enter the race that were not sponsored by a bicycle related industry.<ref name="LR 1912">{{cite web |last=Capodacqua |first=Eugenio Capodacqua |language=Italian |date=10 May 2007 |url=http://www.repubblica.it/2007/05/speciale/altri/2007giroditalia/storia/storia.html |title=La storia del Giro d'Italia (1909–1950)|newspaper=La Repubblica|publisher=Gruppo Editoriale L'Espresso |accessdate=27 December 2007 <!--DASHBot-->| archiveurl= http://web.archive.org/web/20071224103726/http://www.repubblica.it/2007/05/speciale/altri/2007giroditalia/storia-3/storia-3.html| archivedate=24 December 2007 |deadurl=no |trans_title=The history of the Tour of Italy (1909–1950)}}</ref>

The fourteen teams that took part in the race were:<ref name="BRI 1912"/><ref name="LS B4"/>

{|
|-
|  style="vertical-align:top; width:25%;"|
* [[Atala (cycling team)|Atala-Dunlop]]
* Bergami
* [[Bianchi cycling team|Bianchi]]
* Favero-Dunlop
* Gerbi
|  style="vertical-align:top; width:25%;"|
* Globo
* Goericke
* [[Legnano-Pirelli|Legnano]]
* L'Italiana
* [[Peugeot (cycling team)|Peugeot]]
|  style="vertical-align:top; width:25%;"|
* Ranella
* Senior
* Soriani
* Stucchi
|}

Atala-Dunlop was the favorite going into the race as the team was composed of [[1909 Giro d'Italia|1909]] winner [[Luigi Ganna]], two-time winner [[Carlo Galetti]], [[Giovanni Micheletto]], and [[Eberardo Pavesi]].<ref name="BRI 1912"/><ref name="CR 12"/> The team was collectively known as "The Four Musketeers."<ref name="BRI 1912"/> The Gerbi team contained last year's third-place finisher [[Giovanni Gerbi]], last year's runner-up and stage winner [[Giovanni Rossignoli]], and former stage winner [[Pierino Albini]].<ref name="BRI 1912"/> Legnano, Bianchi and Peugeot were also seen as contenders for the overall victory.<ref name="BRI 1912"/> The race also featured future Giro d'Italia winner [[Carlo Oriani]] who competed for the Stucchi team.<ref name="BRI 1912"/><ref name="elm 1913">{{cite news|url=http://hemeroteca-paginas.mundodeportivo.com/EMD02/HEM/1913/05/29/MD19130529-004.pdf|title=La Vuelta De Italia|language=Spanish|date=29 May 1913|page=4|publisher=El Mundo Deportivo|accessdate=27 May 2012|format=PDF|trans_title=The Giro d'Italia|archiveurl=https://web.archive.org/web/20150310045715/http://hemeroteca-paginas.mundodeportivo.com/EMD02/HEM/1913/05/29/MD19130529-004.pdf|archivedate=10 March 2015|deadurl=no}}</ref>

==Race overview==
[[File:Giovanni Micheletto.jpg|thumb|right|Italian [[Giovanni Micheletto]], of [[Atala (cycling team)|Atala-Dunlop]], won the opening stage of the race.]]
The race was planned to begin at 2:30 AM local time by the organizers, but due to large crowds and some participants arriving late, the race officially started at 2:43.<ref name="LS 1">{{cite news|url=http://www.archiviolastampa.it/component/option,com_lastampa/task,search/mod,avanzata/action,viewer/Itemid,3/page,5/articleid,1193_01_1912_0139_0005_24837157/|title=Il IV Giro Ciclisto d'Italia|language=Italian|date=20 May 1912|page=5|newspaper=La Stampa|publisher=Editrice La Stampa|accessdate=27 May 2012|trans_title=The Fourth Cycling Tour of Italy}}</ref> The riders formed three groups on the road, of which the first one contained twenty riders when it passed through the checkpoint in [[Brescia]].<ref name="LS 1"/> The group further thinned to sixteen by the time it reached [[Verona]], and to eleven when it arrived at the checkpoint in [[Vicenza]].<ref name="LS 1"/> From the group, the sprint to the finish line was mainly contested by Giovanni Micheletto and [[Giuseppe Santhià]], of which the former managed to win the leg.<ref name="BRI 1912"/><ref name="LS 1"/> With Atala-Dunlop's Micheletto winning the stage and his teammate Carlo Galetti finishing third, Atala-Dunlop acquired seven points and the lead in the race.<ref name="BRI 1912"/><ref name="LS 1"/>

The second stage of the race saw a large group of around forty riders enter [[Mantua]] after over one hundred kilometers of racing.<ref name="LS 2">{{cite news|url=http://www.archiviolastampa.it/component/option,com_lastampa/task,search/mod,libera/action,viewer/Itemid,3/page,6/articleid,1193_01_1912_0141_0006_24289094/anews,true/|title=Borgarello vince la 2 tappa Padova-Bologna|language=Italian|date=22 May 1912|page=6|newspaper=La Stampa|publisher=Editrice La Stampa|accessdate=27 May 2012|trans_title=Borgarello won the second stage Padua-Bologna}}</ref> After the group broke into five or six smaller groups due to a large crash and in part due issues brought about by heavy fog.<ref name="LS 2"/> A group of seven was first to reach the stage's finish in [[Bologna]], where [[Bianchi cycling team|Bianchi]]'s [[Vincenzo Borgarello]] won the sprint to the line.<ref name="LS 2"/> Stage 3 began under heavy cloud coverage.<ref name="LS 3">{{cite news|url=http://www.archiviolastampa.it/component/option,com_lastampa/task,search/mod,libera/action,viewer/Itemid,3/page,5/articleid,1193_01_1912_0143_0005_17940195/anews,true/|title=Azzini Ernesto vince la 3 tappa del Giro d'Italia|language=Italian|date=24 May 1912|page=5|newspaper=La Stampa|publisher=Editrice La Stampa|accessdate=27 May 2012|trans_title=Ernesto Azzini won the 3rd stage of the Tour of Italy}}</ref> The leading group on the road was reduced as the stage traversed the hillier portions of the stage.<ref name="LS 3"/> [[Ernesto Azzini]] of the Legnano team won the stage after edging out [[Eberardo Pavesi]].<ref name="LS 3"/><ref name="elm stages 2,3">{{cite news|url=http://hemeroteca-paginas.mundodeportivo.com/EMD02/HEM/1912/05/30/MD19120530-007.pdf|title=La Vuelta De Italia|language=Spanish|date=30 May 1912|page=7|newspaper=El Mundo Deportivo|publisher=El Mundo Deportivo S.A.|accessdate=27 May 2012|format=PDF|trans_title=The Giro d'Italia|archiveurl=https://web.archive.org/web/20150310045548/http://hemeroteca-paginas.mundodeportivo.com/EMD02/HEM/1912/05/30/MD19120530-007.pdf|archivedate=10 March 2015|deadurl=no}}</ref>

The race's fourth stage saw very rainy conditions throughout, which caused the roads to turn muddy and streams nearby the roads to overflow.{{sfn|Franzinelli|2013|p=83}} The thirty riders that began the stage took the wrong turn onto Sabina rode for over {{convert|50|km|0|abbr=on}}, once they had reached [[Civita Castellana]], before realizing their errors.<ref name="elm stage 4, 5, 6, 7">{{cite news|url=http://hemeroteca-paginas.mundodeportivo.com/EMD02/HEM/1912/06/06/MD19120606-003.pdf|title=La Vuelta De Italia|language=Spanish|date=6 June 1912|page=3|newspaper=El Mundo Deportivo|publisher=El Mundo Deportivo S.A.|accessdate=27 May 2012|format=PDF|trans_title=The Giro d'Italia|archiveurl=https://web.archive.org/web/20150310045423/http://hemeroteca-paginas.mundodeportivo.com/EMD02/HEM/1912/06/06/MD19120606-003.pdf|archivedate=10 March 2015|deadurl=no}}</ref> This, when combined with the weather and road conditions, caused the riders to protest to the race jury and get the stage cancelled.{{sfn|Franzinelli|2013|p=83}}<ref name="elm stage 4, 5, 6, 7"/> The riders then road a train to [[Rome]] for the start of the next stage.<ref name="LR 1912"/><ref name="elm stage 4, 5, 6, 7"/> At the finish in Rome there were over 20,000 paying spectators waiting to see the finish of the stage.<ref name="LR 1912"/><ref name="LS 4">{{cite news|url=http://www.archiviolastampa.it/component/option,com_lastampa/task,search/mod,libera/action,viewer/Itemid,3/page,2/articleid,1193_01_1912_0145_0002_24837202/|title=La IV tappa del Giro d'Italia interrotta a Magliano Sabina|language=Italian|date=26 May 1912|page=2|newspaper=La Stampa|publisher=Editrice La Stampa|accessdate=27 May 2012|trans_title=The fourth stage of the Tour of Italy stopped in Magliano Sabina}}</ref> When news broke of the stage's cancelling, the spectators grew angry and the organizers refunded their tickets they had purchased to view the race.<ref name="LR 1912"/> The route of the added, ninth stage came from the route of the [[Giro di Lombardia]].<ref name="elm stage 4, 5, 6, 7"/> The prize money for the stage was split among the fourteen riders that noticed the mistake.<ref name="elm stage 4, 5, 6, 7"/>

Atala-Dunlop's [[Luigi Ganna]] abandoned the race during the fifth stage due to injuries suffered from a crash during the fourth stage before it was cancelled.<ref name="elm stage 4, 5, 6, 7"/> Galetti won the fifth leg, but with the placings on the stage, Peugeot took the race lead from Atala-Dunlop.<ref name="elm stage 4, 5, 6, 7"/><ref name="LS 5">{{cite news|url=http://www.archiviolastampa.it/component/option,com_lastampa/task,search/mod,libera/action,viewer/Itemid,3/page,6/articleid,1193_01_1912_0147_0006_24837295/|title=La tappa Roma-Firenze del Giro d'Italia vinta da Galetti|language=Italian|date=28 May 1912|page=6|newspaper=La Stampa|publisher=Editrice La Stampa|accessdate=27 May 2012|trans_title=The Rome-Florence stage of the Tour of Italy won by Galetti}}</ref> Rain marred the start of the sixth stage at 6:40 local time, as well as most of the stage itself.<ref name="LS 6">{{cite news|url=http://www.archiviolastampa.it/component/option,com_lastampa/task,search/mod,libera/action,viewer/Itemid,3/page,2/articleid,1193_01_1912_0149_0002_24270905/|title=La VI tappa del Giro d'Italia vinta da Bordin|language=Italian|date=30 May 1912|page=2|newspaper=La Stampa|publisher=Editrice La Stampa|accessdate=27 May 2012|trans_title=The sixth stage of the Tour of Italy won by Bordin}}</ref> A group of sixteen riders formed at the front of the race and was later reduced to ten before passing through [[Lucca]].<ref name="LS 6"/> [[Lauro Bordin]] of the Gerbi team attacked and went on to ride solo, reaching the checkpoint in [[Spezia]] fifteen minutes quicker than Azzini, Cocchi, and Agostoni.<ref name="LS 6"/> Bordin reached the finish first, eighteen minutes ahead of the second-place finisher.<ref name="LS 6"/>

Borgarello won his second stage with his victory in the seventh stage, out-sprinting Micheletto and [[Carlo Durando]].<ref name="LS 7">{{cite news|url=http://www.archiviolastampa.it/component/option,com_lastampa/task,search/mod,libera/action,viewer/Itemid,3/page,4/articleid,1193_01_1912_0151_0004_24837312/|title=La VII tappa del Giro d'Italia|language=Italian|date=1 June 1912|page=4|newspaper=La Stampa|publisher=Editrice La Stampa|accessdate=27 May 2012|trans_title=The seventh stage of the Tour of Italy}}</ref> In the final sprint, Micheletto felt that Durando encroached him and moved ahead; this action caused the Italian fans to rush on to the track in anger.<ref name="BRI 1912"/> The police then moved the two riders to a bar, until the crowd dispersed.<ref name="BRI 1912"/> Micheletto won his second stage of the race two days later.<ref name="BRI 1912"/><ref name="elm stages 8, 9, finals">{{cite news|url= http://hemeroteca-paginas.mundodeportivo.com/EMD02/HEM/1912/06/13/MD19120613-004.pdf|title=La Vuelta De Italia|language=Spanish|date=13 June 1912|page=4|newspaper=El Mundo Deportivo|publisher=El Mundo Deportivo S.A.|accessdate=27 May 2012|format=PDF|trans_title=The Giro d'Italia|archiveurl=https://web.archive.org/web/20150310045513/http://hemeroteca-paginas.mundodeportivo.com/EMD02/HEM/1912/06/13/MD19120613-004.pdf|archivedate=10 March 2015|deadurl=no}}</ref> Before the start of the final stage, Micheletto became sick and Pavesi convinced him to continue the race and finish the final stage.<ref name="BRI 1912"/> Borgarello won his third stage of the race, while Atala-Dunlop consolidated their lead and the race win by having two riders, Micheletto and Galetti, finish in the top four.<ref name="BRI 1912"/><ref name="elm stages 8, 9, finals"/>

==Final standings==

===Stage results===

{| class="wikitable"
|+ Stage results<ref name="BRI 1912"/><ref name="CR 12"/>
|- style="background:#efefef;"
!Stage
!Date
!Course
!Distance
!colspan="2"|Type<ref group="Notes">In 1912, there was no distinction in the rules between plain stages and mountain stages; the icons shown here indicate that the second, fifth, sixth, seventh, eighth, and ninth stages included major mountains.</ref>
!Winner<ref group="Notes">The name of the stage winner's team is listed in parenthesis.</ref>
!Race Leader
|-
!style="text-align:center"|1 
| 19 May 
| [[Milan]] to [[Padua]] 
|style="text-align:center"| {{convert|398.8|km|0|abbr=on}} 
| style="text-align:center;"| [[Image:Plainstage.svg|22px|link=|alt=]]
|Plain stage
| {{flagathlete|[[Giovanni Micheletto]]|ITA|1861}} ([[Atala (cycling team)|Atala-Dunlop]])
| [[Atala (cycling team)|Atala-Dunlop]]
|- 
!style="text-align:center"|2 
| 21 May 
| [[Padua]] to [[Bologna]] 
|style="text-align:center"| {{convert|328.8|km|0|abbr=on}} 
| style="text-align:center;"| [[Image:Mountainstage.svg|22px|link=|alt=]]
|Stage with mountain(s)
| {{flagathlete|[[Vincenzo Borgarello]]|ITA|1861}} (Legnano)
| [[Atala (cycling team)|Atala-Dunlop]]
|-
!style="text-align:center"|3 
| 23 May 
| [[Bologna]] to [[Pescara]] 
|style="text-align:center"| {{convert|362.5|km|0|abbr=on}} 
| style="text-align:center;"| [[Image:Plainstage.svg|22px|link=|alt=]]
|Plain stage
| {{flagathlete|[[Ernesto Azzini]]|ITA|1861}} (Legnano)
| [[Atala (cycling team)|Atala-Dunlop]]
|-
!style="text-align:center"|4 
| 25 May 
| [[Pescara]] to [[Rome]] 
|style="text-align:center"| <del>{{convert|294|km|0|abbr=on}}</del><ref name="elm stage 4, 5, 6, 7"/>
| style="text-align:center;"| [[Image:Plainstage.svg|22px|link=|alt=]]
|Plain stage
| style="text-align:center;"| ''Stage Cancelled''
| [[Atala (cycling team)|Atala-Dunlop]]
|- 
!style="text-align:center"|5 
| 27 May 
| [[Rome]] to [[Florence]] 
|style="text-align:center"| {{convert|337|km|0|abbr=on}} 
| style="text-align:center;"| [[Image:Mountainstage.svg|22px|link=|alt=]]
|Stage with mountain(s)
| {{flagathlete|[[Carlo Galetti]]|ITA|1861}} ([[Atala (cycling team)|Atala-Dunlop]])
| [[Peugeot (cycling team)|Peugeot]]
|- 
!style="text-align:center"|6 
| 29 May 
| [[Florence]] to [[Genoa]] 
|style="text-align:center"| {{convert|267.5|km|0|abbr=on}} 
| style="text-align:center;"| [[Image:Mountainstage.svg|22px|link=|alt=]]
|Stage with mountain(s)
| {{flagathlete|[[Lauro Bordin]]|ITA|1861}} (Gerbi) 
| [[Atala (cycling team)|Atala-Dunlop]]
|-
!style="text-align:center"|7 
| 31 May 
| [[Genoa]] to [[Turin]] 
|style="text-align:center"| {{convert|230|km|0|abbr=on}} 
| style="text-align:center;"| [[Image:Mountainstage.svg|22px|link=|alt=]]
|Stage with mountain(s)
| {{flagathlete|[[Vincenzo Borgarello]]|ITA|1861}} (Legnano)
| [[Atala (cycling team)|Atala-Dunlop]]
|- 
!style="text-align:center"|8 
| 2 June 
| [[Turin]] to [[Milan]] 
|style="text-align:center"| {{convert|280|km|0|abbr=on}} 
| style="text-align:center;"| [[Image:Mountainstage.svg|22px|link=|alt=]]
|Stage with mountain(s)
| {{flagathlete|[[Giovanni Micheletto]]|ITA|1861}} ([[Atala (cycling team)|Atala-Dunlop]]) 
| [[Atala (cycling team)|Atala-Dunlop]]
|-
!style="text-align:center"|9 
| 4 June 
| [[Milan]] to [[Bergamo]] 
|style="text-align:center"| {{convert|235|km|0|abbr=on}} 
| style="text-align:center;"| [[Image:Mountainstage.svg|22px|link=|alt=]]
|Stage with mountain(s)
| {{flagathlete|[[Vincenzo Borgarello]]|ITA|1861}} (Legnano) 
| [[Atala (cycling team)|Atala-Dunlop]] 
|-
!
| colspan="2" style="text-align:center;"| Total
| colspan="5" style="text-align:center;"| {{convert|2439.6|km|0|abbr=on}}<ref group="Notes">This total excludes the fourth stage's planned length because the stage was cancelled.</ref>
|}

===General classification===

There were five teams who had completed all nine stages with the required number of riders.<ref name="elm stages 8, 9, finals"/> For these teams, the points they received from each of their stage placing's were added up for the [[General classification in the Giro d'Italia|general classification]].<ref name="BRI 1912"/> The team with the most accumulated points was the winner.<ref name="BRI 1912"/>

{| class="wikitable" style="width:20em;margin-bottom:0;"
|+ Final general classification (1–5)<ref name="BRI 1912"/><ref name="elm stages 8, 9, finals"/>
|-
!Rank
!Team
!Points
|-
!style="text-align:center"| 1
| [[Atala (cycling team)|Atala-Dunlop]]
| style="text-align:center;"| 33
|-
!style="text-align:center"| 2
| [[Peugeot (cycling team)|Peugeot]]
| style="text-align:center;"| 23
|-
!style="text-align:center"| 3
| Gerbi
| style="text-align:center;" rowspan="2"| 8
|-
!style="text-align:center"| 4
| Goericke
|-
!style="text-align:center"| 5
| Globo
| style="text-align:center;"| 7
|-
|}

==Aftermath==

The race organizers, ''La Gazzetta dello Sport'' recognized the team point system was not a success and reverted to the individual point based system for the general classification for the [[1913 Giro d'Italia|1913 edition]].<ref name="BRI 13">{{cite web|url=http://bikeraceinfo.com/giro/giro1913.html |title=1913 Giro d'Italia |work=Bike Race Info |date= |author= Bill and Carol McGann |publisher=Dog Ear Publishing|accessdate=2012-07-10|archiveurl=https://web.archive.org/web/20140304172928/http://www.bikeraceinfo.com/giro/giro1913.html|archivedate=4 March 2014|deadurl=no}}</ref> Despite the failure of the team system, the newspaper still achieved success with the race, causing them to shift to a daily newspaper rather than three times a week.<ref name="BRI 13"/> If the race had been contested based upon the time it took to complete each stage and their respective sum for each rider, the race would have been won by Carlo Galetti.<ref name="BRI 1912"/>

==References==

;Notes
{{Reflist|group="Notes"}}

;Citations
{{reflist|30em}}

;Bibliography
{{Refbegin}}
*{{cite book |last1=Franzinelli |first1=Mimmo |location= Milan, Italy |title=Il Giro d'Italia: Dai pionieri agli anni d'oro |publisher=Feltrinelli Editore |year= 2013 |url=https://books.google.com/books?id=qXSkik_g_YUC&printsec=frontcover&hl=it&source=gbs_ge_summary_r&cad=0#v=onepage&q=pescara&f=false |isbn=978-88-07-11126-6 |ref=harv |language=Italian|trans_title=The Tour of Italy: From the Pioneers to the Golden Years}}
{{Refend}}

{{Giro d'Italia}}

{{DEFAULTSORT:1912 Giro D'italia}}
[[Category:1912 in Italian sport|Giro Ditalia, 1912]]
[[Category:Giro d'Italia by year]]
[[Category:1912 in road cycling|Giro Ditalia, 1912]]