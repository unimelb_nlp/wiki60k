{{Orphan|date=November 2013}}
{{italic title}}
{{Taxobox 
| name               = ''Ditylum brightwellii''
| image              = Ditylum_brightwellii2.jpg
| image_upright      = 0.7
| image_alt          = 
| image_caption      = 
| domain             = [[Eukaryota]]
| unranked_regnum    = [[SAR supergroup|SAR]]
| phylum             = [[Heterokontophyta]]
| classis            = [[Coscinodiscophyceae]]
| ordo               = [[Lithodesmiales]]
| familia            = [[Lithodesmiaceae]]
| genus              = ''[[Ditylum]]''
| species            = '''''D. brightwellii'''''
| binomial           = ''Ditylum brightwellii''
| binomial_authority = (T.West) Grunow ''in'' Van Heurck 
}}

'''''Ditylum brightwelli''''' is a species of cosmopolitan marine centric [[diatom]]s.  It is a [[unicellular]] [[photosynthetic]] [[autotroph]] that has the ability to divide rapidly and contribute to spring phytoplankton blooms.<ref name="bloom">{{cite journal|last1=Rynearson|first1=Tatiana A.|last2=Armbrust|first2=E. Virginia |year=2005 |title=Maintenance of clonal diversity during a spring bloom of the centric diatom ''Ditylum brightwellii'' |journal=Molecular Ecology |volume=14 |issue=6 |pages=1631–1640 |doi=10.1111/j.1365-294X.2005.02526.x |pmid=15836638}}</ref>

==Description==
The ''D. brightwellii'' [[Cell (biology)|cell]] has a high length to diameter ratio.  The cell wall is silicified, as is characteristic of all diatoms.  This hard, porous covering is known as the [[frustule]] and causes the cell to be more dense than the surrounding water.  Oceanic currents and surface winds prevent ''D. brightwellii'' cells from sinking beneath the [[euphotic zone]].  Cells range in size from 25-100μm in diameter and 80-130μm in length.<ref name="sms"/>  The valve is most often triangular in shape, but can also be biangular or quadrangular.<ref name="ab">{{cite web|url=http://www.algaebase.org/search/genus/detail/?genus_id=44389 |title=Ditylum J.W.Bailey, 1861: 163 |publisher=Algaebase |date= |accessdate=2013-11-04}}</ref>  A long hollow tube called the rimoportula is located centrally and extends from each valve<ref name="ab"/>

==Distribution==
''D. brightwellii'' is found in all global oceans except in polar waters.<ref name="sms">{{cite web|url=http://www.sms.si.edu/irlspec/Ditylu_bright.htm |title=Ditylum brightwellii |publisher=mithsonian Institution |date=2011-09-25 |accessdate=2013-11-04}}</ref>  Genetically distinct populations were observed over the course of a [[spring bloom]] in [[Puget Sound]], suggesting that certain genetic lineages are better adapted to certain environmental conditions.<ref name="aslo"/>

==Life cycle==
[[File:Dytilum Brightwelli 3.jpg|thumb|left|200px|Sample in [[Black Sea]]]]
''D. brightwellii'' reproduces primarily [[asexual reproduction|asexually]], creating clonal lineages.<ref name="spore"/>  Vegetative cells are capable of enlargement and may also produce resting spores.<ref name="spore">Hargraves PE (1984) Resting spore formation in the marine diatom ''Ditylum brightwellii'' (West) Grun. ex V.H. In: Proceedings of the Seventh International Diatom Symposium, Philadelphia, August 22–27, 1982 (ed. Mann DG), pp. 33–46. Otto Koeltz-Science, Koenigstein.</ref> However, samples from Puget Sound, WA display high [[genetic diversity]].<ref name="aslo">{{cite journal|last1=Rynearson|first1=T. A.|last2=Newton|first2=J. A.|last3=Armbrust|first3=E. V. |year=2006 |title=Spring bloom development, genetic variation, and population succession in the planktonic diatom ''Ditylum brightwellii'' |journal=Limnology and Oceanography |volume=51 |issue=3 |pages=1249–1261 |doi=10.4319/lo.2006.51.3.1249 |url=http://aslo.org/lo/toc/vol_51/issue_3/1249.pdf}}</ref> This is indicative of sexual reproduction ([[auxospore]] formation).  Clonal isolates have observed to produce both sperm and eggs.<ref name="jak">{{cite journal|last1=Koester|first1=Julie A.|last2=Brawley|first2=Susan H.|last3=Karp-Boss|first3=Lee|last4=Mann|first4=David G.|title=Sexual reproduction in the marine centric diatom ''Ditylum brightwellii'' (Bacillariophyta)|journal=European Journal of Phycology|volume=42|issue=4|year=2007|pages=351–366|issn=0967-0262|doi=10.1080/09670260701562100}}</ref> Two [[Egg (biology)|eggs]] are produced from each [[oogonium]] and 64 [[sperm]] are produced from each [[spermatogonangium]].<ref name="jak"/> The frequency of sexual reproduction in ''D. brightwellii'' is not clear, although conditions including increased nutrients, temperatures ranging from 10&nbsp;°C-14&nbsp;°C, and a short photoperiod may be favorable for sexual reproduction.<ref name="jak"/>

==References==
{{Reflist|30em}}

==Further reading==
*{{cite journal|last1=Eppley|first1=Richard W.|last2=Holmes|first2=Robert W.|last3=Paasche|first3=Eystein|title=Periodicity in cell division and physiological behavior of ''Ditylum brightwellii'', a marine planktonic diatom, during growth in light-dark cycles|journal=Archiv für Mikrobiologie|volume=56|issue=4|year=1967|pages=305–323|issn=0302-8933|doi=10.1007/BF00425206}}
*{{cite journal|last1=Guo|first1=Ruoyu|last2=Lee|first2=Min-Ah|last3=Ki|first3=Jang-Seu|title=Normalization genes for mRNA expression in the marine diatom ''Ditylum brightwellii'' following exposure to thermal and toxic chemical stresses|journal=Journal of Applied Phycology|volume=25|issue=4|year=2012|pages=1101–1109|issn=0921-8971|doi=10.1007/s10811-012-9908-z}}
*{{cite journal|last1=Jung|first1=Seung Won|last2=Youn|first2=Seok Jae|last3=Shin|first3=Hyeon Ho|last4=Yun|first4=Suk Min|last5=Ki|first5=Jang-Seo|last6=Lee|first6=Jin Hwan|title=Effect of temperature on changes in size and morphology of the marine diatom, ''Ditylum brightwellii'' (West) Grunow (Bacillariophyceae)|journal=[[Estuarine, Coastal and Shelf Science]]|date=December 2013|volume=135|pages=128–136|doi=10.1016/j.ecss.2013.05.007|url=http://ac.els-cdn.com/S0272771413002308/1-s2.0-S0272771413002308-main.pdf?_tid=984beeac-af0e-11e4-bf77-00000aab0f6b&acdnat=1423343974_8679c0ddcce59ab48fe2e49931bde0b1|accessdate=7 February 2015}}
*{{cite journal|last1=Choudhury|first1=Avik Kumar|last2=Pal|first2=Ruma|title=Phytoplankton and nutrient dynamics of shallow coastal stations at Bay of Bengal, Eastern Indian coast|journal=Aquatic Ecology |year=2009 |volume=44 |issue=1 |pages=55–71 |doi=10.1007/s10452-009-9252-9}}

==External links==
{{taxonbar}}

[[Category:Centrales]]