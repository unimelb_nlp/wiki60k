{{Infobox journal
| title = Journal of Public Health Policy
| cover =
| editor = Anthony Robbins, Phyllis Freeman
| discipline = [[Public health]]
| abbreviation = J. Public Health Policy
| publisher = [[Palgrave Macmillan]]
| country =
| frequency = Quarterly
| history = 1980-present
| openaccess =
| impact = 2.113
| impact-year = 2011
| website = http://www.palgrave-journals.com/jphp/
| link1 = http://www.palgrave-journals.com/jphp/archive/index.html
| link1-name = Online access
| link2 =
| link2-name =
| JSTOR = 
| OCLC = 55053767
| LCCN = 80644507
| CODEN = JPPODK
| ISSN = 0197-5897
| eISSN = 1745-655X
}}
The '''''Journal of Public Health Policy''''' is a [[Peer review|peer-reviewed]] [[healthcare journal|medical journal]] established in 1980 by [[Milton Terris]]. It covers the field of [[public health]] and is the official journal of the [[National Association for Public Health Policy]].

== Abstracting and indexing ==
The journal is abstracted and indexed in [[CSA (database company)|CSA Sociological Abstracts]], [[EMBASE]], [[Index Medicus]]/[[MEDLINE]], [[PsycINFO]], [[Science Citation Index Expanded]], [[Scopus]], and [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2011 [[impact factor]] of 2.113.<ref name=WoS1>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Health Care Sciences & Services |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2012-12-30 |series=[[Web of Science]] |postscript=.}}</ref><ref name=WoS2>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Public, Environmental & Occupational Health |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2012-12-30 |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.palgrave-journals.com/jphp/}}

{{Holtzbrinck}}

[[Category:Publications established in 1980]]
[[Category:Public health journals]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Palgrave Macmillan academic journals]]
[[Category:Academic journals associated with learned and professional societies]]
[[Category:Health policy journals]]