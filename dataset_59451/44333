{{Coord|01|25|08.4|N|103|51|57.0|E|display=title}}
{{Use British English|date=August 2011}}
{{Use dmy dates|date=March 2011}}
<!-- This article is NOT written in US-English. Please do not change it to US-English contrary to [[WP:ENGVAR]]:
"An article on a topic that has strong ties to a particular English-speaking nation uses the appropriate variety of English for that nation." -->
{{Infobox airport
|name= Seletar Airport
|nativename= ''Lapangan Terbang Seletar''
|nativename-a= {{lang|zh-sg|{{nobold|实里达机场}}}}<br>{{small|(''Shí Lǐ Dá Jīchǎng'')}}
|nativename-r= செலட்டர் வான்முகம்<br>{{small|(''Celaṭṭar Vāṉmukam'')}}
|image = Seletar Airport logo.png
|image-width = 200
|caption =
|IATA = XSP
|ICAO = WSSL
|type = [[Civilian]] public usage
|owner = [[Government of Singapore]]
|operator = [[Changi Airport Group]]
|city-served = [[Singapore]]
|location = [[Seletar]]
|elevation-f = 36
|elevation-m = 11
|coordinates = {{coord|01|25|01|N|103|52|04|E|type:airport}}
|website = {{URL|http://www.seletarairport.com/}}
|metric-elev = yes
|metric-rwy = yes
|r1-number = 03/21
|r1-length-f = 6,023
|r1-length-m = 1,836
|r1-surface = [[Asphalt]]
|r2-number =
|r2-length-f =
|r2-length-m =
|r2-surface =
|r3-number =
|r3-length-f =
|r3-length-m =
|r3-surface =
|stat-year =
|stat1-header =
|stat1-data =
|stat2-header =
|stat2-data =
|footnotes =
}}

'''Seletar Airport''' {{Airport codes|XSP|WSSL}} is a civilian airport located at [[Seletar]], in the [[North-East Region, Singapore|northeastern region of Singapore]], and is managed by the [[Changi Airport Group]]. Originally, the airport was completed in 1928 as a [[Flight|flying]] [[Royal Air Force station]] ('''RAF Seletar''') and was also Singapore's first international airport.

There had been a proposal to extend its runway to {{convert|2000|m}}, so as to be able to receive the [[Boeing 737]] used by many budget airlines. However, after considerations by the Singapore Government and the CAAS, they decided to build a Budget Terminal in [[Singapore Changi Airport]] instead.

==History==
===RAF Seletar===
{{See also|Singapore strategy}}
[[File:RAF Seletar Crest.jpg|thumb|left|RAF Seletar Crest Badge]]

'''RAF Seletar''' was a [[Royal Air Force]] station in [[Singapore]] between 1928 and 1971. Plans for establishing an airfield, flying boat and naval base in Singapore were first agreed by the RAF in 1921. In 1923, two sites in the northern region of the island were approved. The first planes to arrive at the base were four [[Supermarine Southampton]] seaplanes on 28 February 1928.

RAF Seletar served as a civil airport from 1930 before the opening of Singapore's first civil airport at [[Kallang Airport|Kallang]] on 12 June 1937 (to the late 1940s).

The air base was briefly host to [[Amy Johnson]] during May 1930 on her UK – Australia flight in her [[De Havilland Gipsy Moth|Gipsy Moth]] named 'Jason', and also to [[Amelia Earhart]] during June 1937 on her world flight attempt in her [[Lockheed 10 Electra]].

====World War II====
With the threat of war in the area, the RAF started building up their forces in the Far East in the late 1930s and early 1940s. Seletar airfield was the target of [[carpet bombing]] when Japanese navy bombers conducted the [[first air raid on Singapore]], sometime after their ground forces [[Japanese Invasion of Malaya|invaded Kota Bahru]]. It was abandoned when the Japanese took [[Johore Bahru]], which brought their artillery in range of the airfield.

When the Japanese launched their invasion of [[Battle of Malaya|Malaya]] and [[Battle of Singapore|Singapore]], Seletar housed the RAF’s [[No. 205 Squadron RAF|205 Sqn]] with [[PBY Catalina]] flying boats and [[No. 36 Squadron RAF|36]] and [[No. 100 Squadron RAF|100]] Sqns with obsolete [[Vickers Vildebeest]] torpedo bombers (including five [[Fairey Albacore]]s acquired by 36 Sqn to supplement its Vildebeests), along with 151 Maintenance Unit. These units stayed until Jan–Feb 1942, soon before the surrender to the invading Japanese.<ref>Shores et al. 1992, p. 146.</ref>

During the Japanese occupation, Seletar as was in the case of [[Sembawang Air Base|Sembawang]] came under the [[Imperial Japanese Navy Air Service]] while [[Tengah Air Base|Tengah]] fell under the jurisdiction of the [[Imperial Japanese Army Air Force]]. From 1942 through 1945, a number of IJN squadrons were based or transited through Seletar mainly, for training. Among the units known to be based at Seletar during this time were ''936th [[Kōkūtai]]'' (B5N Kate, D3A Val and E13A1 Jake), ''381st Kōkūtai'' (A6M Zero and J2M Raiden). The ''601st Kōkūtai'' was also stationed there for training in early before its destruction on board Japanese aircraft carriers during the Battle of Philippine Sea (Marianas Turkey Shoot) in June. Seletar’s present runway was built during the Japanese Occupation.

====Post-World War II====
[[File:Formal takeover of Seletar airfield.jpg|thumb|Royal Navy and RAF officers watch as Vice Admiral Kogure appends his signature to the document marking the formal takeover of Seletar airfield from the Japanese, 8 September 1945.]]

After World War II, the base went back to the RAF and, in the late 1940s and 1950s, the base was heavily involved in the Malayan Emergency, with Beaufighters, Spitfires and Mosquitos based there while operating against Malayan Communist insurgents. Among the many squadrons based there during this time were Nos [[No. 60 Squadron RAF|60]], 81 and [[No. 205 Squadron RAF|205]] Sqns of the [[RAF]].<ref name="CGJ">Jefford 2001, pp. 48–49.</ref> The base was also the home of 390 MU – the Maintenance Base for the whole of the Far East Air Force.

During the 1960s, RAF Seletar was home base to No's [[No. 103 Squadron RAF|103]] and [[No. 110 Squadron RAF|110 Squadrons]], both of which were equipped with [[Westland Whirlwind (helicopter)|Westland Whirlwind]] Mk 10 helicopters and to [[No. 34 Squadron RAF|34]] Squadron, which was equipped with [[Blackburn Beverley]]s. All three Squadrons (among several others) were involved with support of operations in North Borneo during the [[Indonesia-Malaysia confrontation]]. From June 1962, [[No. 66 Squadron RAF|66 Squadron]] (led by Sqn Leader Gray) with their [[Bristol Belvedere|Bristol 192 Belvedere]] helicopters were also based at Seletar, and were sent on frequent tours and detachments to Kuching, Brunei, Labuan and Butterworth as part of the Borneo hearts and minds campaign (the squadron was later disbanded in March 1969).<ref name="CGJ"/> The helicopter squadrons provided a search and rescue service for the Singapore area. The station was also, at that time, home to [[No. 209 Squadron RAF|209 Squadron]], equipped with [[Scottish Aviation Pioneer|Single]] and [[Scottish Aviation Twin Pioneer|Twin]] Pioneer aircraft. [[No. 65 Squadron RAF|65 Squadron]] based at Seletar operated [[Bristol Bloodhound|Bloodhound]] Mk II surface-to-air missiles as anti-aircraft defence from 1 January 1964 until the squadron was disbanded on 30 March 1970 with the equipment and role handed over to 170 Squadron, Republic of Singapore Air Force.<ref name="CGJ"/> Auster aircraft were flown during the Emergency and Confrontation periods in troop/enemy spotting patrols.

In December 1966, three Andover CC Mk1 arrived to replace the ageing Vickers Valetta C1 aircraft of 52 Sqn. 52 Squadron was later reformed in March 1967 after the arrival of a further three aircraft. By now, Confrontation had finished and with no purpose the squadron moved to Changi in 1968 before being disbanded in January 1970.<ref name="CGJ"/>

====British Pullout====
{{externalimage
|topic=
|width=
|align=right
|image1=[http://www.freewebs.com/gajrevor/Seletar%20Airfield%20Entrance.jpg A 1958 photo of RAF Seletar's main entrance/gate]
|image2=[http://www.freewebs.com/gajrevor/Sunderland%20Scrap%20Heap%203.%201958.jpg Last off the production line, Sunderland Mark V ''SZ572'' '''"M"''' of 205 Sqn being scrapped prematurely at ''Seletar'' on 24 July 1958 due to a ground handling accident which broke its back]
}}

The RAF station closed at the end of March 1971 (see [[East of Suez]]) and Seletar was handed over to the Singapore Air Defence Command (SADC), which by 1973, after the British pullout, became the [[Republic of Singapore Air Force]] (RSAF).

Among Seletar’s claim to fame was that several classic aircraft type flew their last RAF Operational sorties from there including the [[Short Singapore]] flying boat (Mk.III ''K6912'' of [[No. 205 Squadron RAF]] 14 October 1941, aircraft transferred to [[No. 5 Squadron RNZAF]]),<ref>{{cite web |url= http://www.adf-serials.com/nz-serials/K6912.shtml |title= New Zealand Military Aircraft Serial Numbers – Short Singapore Mk III |publisher= www.adf-serials.com}}</ref> [[Supermarine Spitfire]] (PR.XIX ''PS888'' of 81 Sqn 1954), [[De Havilland Mosquito]] (PR.34 ''RG314'' of 81 Sqn 1955), [[Short Sunderland]] flying boat (GR.5 ''ML797'' '''"P"''' of 205 Sqn, 15 May 1959) and [[Bristol Beaufighter]] (TT.X ''RD761'' Station Flight 1960). The Short Sunderland flying boats started in RAF service from ''Seletar'' on 22 June 1938 with [[No. 230 Squadron RAF|230 Sqn]],<ref>Rawlings 1969, p. 242.</ref> a sister squadron of 205 Sqn.

===Seletar Airbase===
The formative years of the SADC (later the RSAF) was established at Seletar Airbase in September 1968, with the setting up of the Flying Training School (FTS) utilising three [[Cessna 172]]G/H on loan from the Singapore Flying Club. The subsequent arrival of eight new Cessna 172Ks in May 1969, took over the duty from the former and contributed to the increase of training tempo for more selected trainees to participate in the basic flight-training course.

==Current operations==
Seletar Airport now operates as a [[general aviation]] airport, mainly for chartered flights and training purposes. Currently, the airport is open 24 hours a day, has a single runway with 27 aircraft stands, has 100 square metres of warehouse space and can handle 840 tons of freight per day. In 1998, the airport recorded receiving a total of 7,945 scheduled flights, handled 23,919 passengers and 6,025 tons of cargo.

The airport fire service, AES Seletar, is provided by Changi Airport Group. AES Seletar has 1 station housing 6 apparatus (water tender, foam tender and others) and provides Level 7 protection.<ref>{{cite web|url=http://changiairportgroup.com/cag/html/our-services/airport-emergency-services/fire-stations |title=Our Expertise |website=Changiairportgroup.com |date= |accessdate=2017-03-03}}</ref>

The Republic of Singapore Flying Club, Seletar Flying Club and [[Singapore Flying College]] are situated at Seletar Airport. The Singapore Flying College also conducts its flying training at [[Jandakot Airport]] in [[Perth, Western Australia|Perth]], [[Western Australia]] and at [[Sunshine Coast Airport]] in [[Maroochydore, Queensland|Maroochydore]], [[Queensland]]. Another prominent flying school is the [[Singapore Youth Flying Club]], which has its headquarters built on western side of the airport's [[runway]]. Completed in June 2001, the clubhouse also has its own parking bays for its fleet of [[Piper Cherokee|Piper Warrior II]], [[PAC CT/4|CT-4E]] and [[Diamond DA40]]. Also, the rotary training unit of Republic Singapore Air Force – 124 Squadron, has a training detachment at the civilian airport although it is normally headquartered at [[Sembawang Air Base]].

Previously, [[Berjaya Air]] operated scheduled flights to [[Tioman]] and [[Redang]]. The Berjaya Air service ended on 31 October 2010 and relocated to [[Changi Airport]].

==Future expansion and plans==
In 2007, [[JTC Corporation]] announced the plan to upgrade the Seletar Airport to support the upcoming [[Seletar Aerospace Park]]. The plan included lengthening the airport's [[runway]] to 1,800 metres and the upgrading of its [[avionics]] systems to allow bigger aircraft to land and take off.<ref name="Karamjit">{{cite news |author= Karamjit Kaur |title= Seletar gets ready for makeover as aerospace hub |publisher= [[The Straits Times]] |date= 27 June 2007}}</ref> [[Changi Airport Group]] took over the management of the airport from the [[Civil Aviation Authority of Singapore]] on 1 July 2009.<ref>{{cite web|url=http://www.changiairportgroup.com.sg/cag/html/our-services/seletar_airport/ |title=Our Story |website=Changiairportgroup.com.sg |date= |accessdate=2017-03-03}}</ref>

The airport will undergo refurbishment in 2015 as part of plans to relieve pressure on Changi Airport. The plan is to optimise its capacity and move smaller and slower aircraft to the airport.<ref>{{cite journal|title=Expansion Plans for Singapore Airport|journal=Airliner World|date=February 2015|page=14}}</ref>

Given the recent requirements to engage a ground handling agent for operations at the airport,<ref>[https://web.archive.org/web/20160422032738/http://www.caas.gov.sg/caasWeb2010/export/sites/caas/en/Regulations/Aeronautical_Information/AIP/aerodrome/AD_WSSL/WSSL-AD2-1.pdf] </ref> the application and enforcement of very strict rules that would normally apply to large-scale aircraft and the aforementioned refurbishment plans, the airport management's and Singapore's commitment to supporting and accepting the small private and leisure operators' light aircraft General Aviation segment has often been called into question.

On 19 May 2016, the Civil Aviation Authority of Singapore (CAAS) and Changi Airport Group (CAG) announced that a new passenger terminal will be built by end 2018. The new passenger terminal building will provide improved facilities and amenities, and will allow the airport to accommodate growth of traffic. Operations at the western side of the airport will cease. Turboprop aircraft operations will be shifted from Changi Airport to Seletar Airport once the building is ready, helping to free up capacity for jet aircraft operations at Changi Airport.<ref>{{cite web | url=https://www.caas.gov.sg/caasWeb2010/opencms/caas/en/Media/news_details.html?newsURL=http://appserver1.caas.gov.sg/caasmediaweb2010/opencms/Journalist/Press_Releases/2016/news_0011.html | title=New Passenger Terminal Building to Be Constructed at Seletar Airport to Support Aviation Growth | publisher=Civil Aviation Authority of Singapore (CAAS) | date=19 May 2016}}</ref><ref>{{cite web | url=http://www.changiairport.com/corporate/media-centre/newsroom.html#/pressreleases/new-passenger-terminal-building-to-be-constructed-at-seletar-airport-to-support-aviation-growth-1410101 | title=New Passenger Terminal Building To Be Constructed At Seletar Airport To Support Aviation Growth | publisher=Changi Airport Group | date=19 May 2016}}</ref><ref>{{cite web | url=http://www.channelnewsasia.com/news/singapore/new-passenger-terminal/2798174.html | title=New passenger terminal building at Seletar Airport in the works | publisher=Channel NewsAsia | date=19 May 2016}}</ref>

==Airlines and destinations==
{| class="wikitable sortable"
|-
! Airline
! Destination
|-
| [[Airmark Indonesia|Airmark Aviation]]
| '''Charter:''' [[Hang Nadim International Airport|Batam]]
|-
| [[EastIndo]]
| '''Charter:''' [[Sultan Syarif Kasim II International Airport|Pekanbaru]]
|}

==Photo gallery==
<gallery>
File:Vildebeest Mark ll.jpg|Vickers Vildebeest Mk IIs, K2918 and K2921, of 'A' Flight, [[No. 100 Squadron RAF|No. 100 (TB) Squadron]], at RAF Seletar.
File:Vildebeest Mk III.png|A [[Vickers Vildebeest]] Mk III of [[No. 36 Squadron RAF]] in flight over [[Singapore|Singapore City]].
File:Short Singapore flying boat of 205 Sqn RAF with Vickers Vildebeests of 100 Sqn RAF.jpg|[[Short Singapore]] Mk III flying boat of [[No. 205 Squadron RAF|205 Sqn]], in flight below three 'vic' formations of Vickers Vildebeest torpedo bombers of 100 Sqn.
File:Aircraft of the Royal Air Force 1939-1945- Short S.19 Singapore CH2556.jpg |A Short Singapore Mk III flying boat, similar to those operated by 205 Sqn.
File:PBYs 205 Sqn RAF in hangar Singapore 1941.jpg|[[Consolidated PBY Catalina|Catalina I]] of 205 Sqn undergoing servicing in their hangar at RAF Seletar. One of the squadron's Short Singapore Mk III biplane flying boats can be seen in the right background.
File:J2M in Malaya.jpg|December 1945, captured [[Mitsubishi J2M]] Raiden fighters belonging to the 381st [[Kōkūtai]] of [[Imperial Japanese Navy Air Service]] being evaluated at Seletar airfield.
File:J2M over Malaya.jpg|The same Mitsubishi J2M Raiden fighters being test flown by Japanese naval aviators under close supervision of RAF officers from Seletar.
File:RAF Seletar ramp 205 Sqn Sunderland.jpg|A 205 Sqn [[Short Sunderland]] Mark V ''ML797'' '''"P"''' at the ramp of '''RAF Seletar''', this particular airframe became the last of its type to retire from active RAF service on 30 June 1959.
File:Aerial view of RAF Seletar in late 1945.jpg|An aerial view of Seletar airfield, Singapore, with RAF [[de Havilland Mosquito|Mosquito]] and [[Douglas C-47 Skytrain#RAF designations|Dakota I]] aircraft parked up.
File:Spitfire mark19 ps853 takeoff arp.jpg|A [[Supermarine Spitfire (Griffon powered variants)#Mk XIX (Mk 19) (types 389 and 390)|Spitfire PR Mk 19]], similar to those operated by [[No. 81 Squadron RAF]] from RAF Seletar.
File:Viewing gallery.jpg|The viewing gallery of [[Singapore Youth Flying Club]] overlooking the runway of Seletar. Note the club's [[PAC CT/4 Airtrainer|CT-4E]] [[taxiing]] on the runway.
</gallery>

==Ground transportation==
===Bus===
Two bus services (services 103 and 117) are available from the airport terminal.

===Taxi===
Taxis are available at the [[Taxicab stand|taxi stands]] at the arrival hall. There is an additional airport surcharge for all trips originating from the airport.

==See also==
{{Portal|Singapore|Aviation}}
* [[Singapore strategy]]
* [[British Far East Command]]
* [[Far East Air Force (Royal Air Force)]]
* [[Far East Strategic Reserve]]
* [[List of former Royal Air Force stations#Former Overseas RAF Stations|Former overseas RAF bases]]
* [[Battle of Singapore]]
* [[Malayan Emergency]]
* [[Indonesia–Malaysia confrontation]]
* [[Seletar Aerospace Park]]
* [[Institute of Mental Health (Singapore)|Woodbridge Hospital]]

==References==
;Notes
{{Reflist}}
;Bibliography
{{Refbegin}}
* {{Cite book|last=Wing Commander C. G. Jefford|first=|title=RAF Squadrons: A Comprehensive Record of the Movement and Equipment of All RAF Squadrons and Their Antecedents Since 1912|year=2001|publisher=Airlife Publishing, 1988|location=Shrewsbury, Shropshire, UK|isbn=978-1-84037-141-3}}
* Rawlings, J.D.R. "History of 230 Squadron". ''Air Pictorial'', July 1969. Vol. 31 No.7. pp.&nbsp;242–244.
* Shores, Christopher, Brian Cull and Yasuho Izawa. ''Bloody Shambles: Volume One: The Drift to War to the Fall of Singapore''. London: Grub Street, 1992. ISBN 0-948817-50-X.
{{Refend}}

==External links==
{{Commons category|Seletar Airport}}
*[http://www.changiairportgroup.com.sg/cag/html/our-services/seletar_airport/ Seletar Airport at Changi Airport Group]
*[http://www.raf.mod.uk/history_old/histories.html History of RAF]
*[http://www.rafweb.org/Stations/Stations-S.htm#Seletar Crest badge and Information of RAF Seletar]
*[http://www.rafseletar.co.uk/ RAF Seletar Association]
*[http://www.army.mod.uk/royalsignals/rsa/19sigregt/book_reviews.htm Review of book: "''SELETAR, Crowning Glory – The History of the RAF in Singapore''"]
*[http://www.northlincsweb.net/103Sqn/ History of 103 Squadron RAF 1917 - 1975]
*{{WAD|WSSL}}

{{Airports in Singapore}}

[[Category:1928 establishments in British Malaya]]
[[Category:Airports established in 1928]]
[[Category:Airports in Singapore]]
[[Category:Buildings and structures in North-East Region, Singapore]]
[[Category:Military of Singapore under British rule]]
[[Category:Seletar|Airport]]
[[Category:Transport in North-East Region, Singapore]]
[[Category:World War II sites in Singapore]]