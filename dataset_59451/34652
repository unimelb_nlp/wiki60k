{{Other uses|2/29th Battalion (Australia)}}
{{good article}}
{{Use dmy dates|date=January 2017}}
{{Use Australian English|date=January 2017}}
{{infobox military unit
|unit_name=29th Battalion
|image=8th Brigade (Australia) infantry school map reading class, France 1918.jpg
|image_size=300px
|caption=Members of the 8th Brigade, including soldiers from the 29th Battalion, AIF, undertaking a map reading class on the Somme in July 1918
|dates=1915–18<br>1921–30<br>1939–42
|country=Australia
|allegiance=
|branch=[[Australian Army]]
|type=[[Infantry]]
|role=
|size=~900–1,000 men{{#tag:ref|During the First World War, the authorised establishment of an Australian infantry battalion was 1,023 men. By the start of the Second World War, the normal size of an Australian infantry battalion was 910 men all ranks.<ref>{{harvnb|Kuring|2004|pp=47 and 176}}.</ref><ref>{{harvnb|Palazzo|2004|p=94}}.</ref>|group=Note}}
|command_structure=[[8th Brigade (Australia)|8th Brigade]], [[5th Division (Australia)|5th Division]] <br>[[4th Brigade (Australia)|4th Brigade]], [[3rd Division (Australia)|3rd Division]]
|garrison=
|garrison_label=
|nickname=East Melbourne Regiment
|patron=
|motto=
|colors=Black alongside Gold
|colors_label=Colours
|march=
|mascot=
|equipment=
|equipment_label=
|battles=[[First World War]]
*[[Western Front (World War I)|Western Front]]
[[Second World War]]
|anniversaries=
|decorations=
|battle_honours=
|battle_honours_label=
|disbanded=
|flying_hours=
|website=
<!-- Commanders -->
|commander1=
|commander1_label=
|commander2=
|commander2_label=
|commander3=
|commander3_label=
|commander4=
|commander4_label=
|notable_commanders=
<!-- Insignia -->
|identification_symbol=[[File:29th Battalion AIF Unit Colour Patch.PNG|50px]]
|identification_symbol_label=[[Unit Colour Patch]]
|identification_symbol_2=
|identification_symbol_2_label=
|identification_symbol_3=
|identification_symbol_3_label=
|identification_symbol_4=
|identification_symbol_4_label=
}}
The '''29th Battalion''' was an [[infantry]] [[battalion]] of the [[Australian Army]]. First formed in 1915 for service during the [[First World War]] as part of the [[First Australian Imperial Force|Australian Imperial Force]] (AIF), it fought in the trenches of the [[Western Front (World War I)|Western Front]] in [[France]] and [[Belgium]] before being disbanded in late 1918 to provide reinforcements for other heavily depleted Australian units. In 1921, following the demobilisation of the AIF, the battalion was re-raised as a unit of Australia's part-time military forces, based in [[Melbourne, Victoria]], before being amalgamated with the [[22nd Battalion (Australia)|22nd Battalion]] in 1930. It was later re-raised in its own right and, following the outbreak of the [[Second World War]],  undertook garrison duties in Australia before being amalgamated with the [[46th Battalion (Australia)|46th Battalion]] to form the [[29th/46th Battalion (Australia)|29th/46th Battalion]] in late 1942, subsequently seeing service against the Japanese in [[New Guinea Campaign|New Guinea]] and on [[New Britain Campaign|New Britain]].

==History==

===First World War===
The 29th Battalion was originally formed during the [[First World War]], being raised in [[Victoria, Australia|Victoria]] as part of the [[First Australian Imperial Force|Australian Imperial Force]] (AIF) on 10 August 1915.<ref>{{harvnb|Austin|1997|p=1}}.</ref><ref name=AWM>{{cite web|url=http://www.awm.gov.au/units/unit_11216.asp |title=29th Battalion |work=First World War, 1914&ndash;1918 units |publisher=Australian War Memorial|accessdate=15 November 2010}}</ref> Under the command of [[Lieutenant Colonel]] Alfred Bennett, an officer with over 20 years service in the part-time military forces, the battalion undertook initial training at [[Seymour, Victoria|Seymour]] and then later [[Broadmeadows, Victoria|Broadmeadows Camp]] along with the three other battalions of the [[8th Brigade (Australia)|8th Brigade]], to which it was assigned.<ref>{{harvnb|Austin|1997|p=6}}.</ref> In November 1915, the battalion embarked upon the troopship HMAT ''Ascanius'' in [[Port Melbourne]] and departed Australian waters, disembarking at [[Port Suez]], [[Egypt]] on 7 December 1915.<ref>{{harvnb|Austin|1997| pp=9–10}}.</ref> The battalion arrived in the [[Middle East]] too late to take part in the fighting at [[Gallipoli Campaign|Gallipoli]],<ref>{{harvnb|Austin| 1997|p=12}}.</ref> and as a result they were initially used to undertake defensive duties to protect the [[Suez Canal]] from [[Ottoman Empire|Ottoman]] forces. They also undertook a comprehensive training program and by the time their orders arrived to transfer to Europe in June 1916, they had reached their peak.<ref>{{harvnb|Austin|1997|p=25}}.</ref>  They subsequently embarked the troopship HMT ''Tunisian'' in [[Alexandria]], bound for [[France]] on 14 June.<ref>{{harvnb|Austin|1997|p=26}}.</ref> Upon the battalion's arrival in Egypt, the 8th Brigade had been unattached at divisional level, but in early 1916, it was assigned to the [[5th Division (Australia)|5th Division]], after a reorganisation that saw the AIF expanded from two infantry divisions to five.<ref>{{harvnb|Bean|1941|p=42}}.</ref><ref>{{harvnb|Grey|2008|pp=99&ndash;100}}.</ref>

The battalion arrived at [[Marseilles]] on 23 June and afterwards was transported by rail to [[Hazebrouck]].<ref>{{harvnb|Austin|1997|pp=28–29}}.</ref> On 8 July the 5th Division was called up to the front from training behind lines in order to replace the battalions of the [[4th Division (Australia)|Australian 4th Division]] which were being transferred to the Somme. The 29th Battalion undertook a difficult two-day {{convert|29|mi|adj=on}} approach march over cobbled roads with loads of up to {{convert|70|-|75|lb|kg}} before arriving at the front on the night of 10/11 July.<ref>{{harvnb|Austin|1997|pp=30–31}}.</ref><ref>{{harvnb|Bean|1941|pp=334–335}}.</ref> Taking up a position between Boutillerie and Condonerrie in the Bois Grenier, they relieved the [[13th Battalion (Australia)|13th Battalion]] and on 19 July subsequently took part in an attack against the German positions around the "Delangre Farm" which was being held by the [[21st Bavarian Reserve Infantry Regiment]].<ref>{{harvnb|Bean|1941|pp=336 and 351}}.</ref> Following the attack, the battalion held the line for another 11 days, beating off a particularly heavy German counterattack on 20 July, before they were eventually relieved. During their introduction to [[trench warfare]], the 29th Battalion lost 52 men killed in action, and another 164 men wounded.<ref>{{harvnb|Austin|1997|p=42}}.</ref>

[[File:29 Bn (AWM E02790).jpg|thumb|left|A platoon commander from the 29th Battalion addresses his troops, 8 August 1918]]

For the next two and half years they fought in a number of major battles in the trenches along the [[Western Front (World War I)|Western Front]] including [[Battle of Polygon Wood|Polygon Wood]], [[Battle of Amiens (1918)|Amiens]] and the [[Battle of the St Quentin Canal|St Quentin Canal]], as well as playing a supporting role in a number of others including [[Second Battle of Bullecourt|Bullecourt]] and [[Spring Offensive|Morlancourt]].<ref name=AWM/> During the Allied [[Hundred Days Offensive]] that was launched on 8 August 1918, the battalion took part in the 8th Brigade's advance up the treacherous Morcourt Valley, subsequently achieving a considerable feat by capturing the town of [[Vauvillers, Somme|Vauvillers]].<ref>{{harvnb|Austin|1997|pp=141–144}}.</ref> The battalion fought its last battle of the war in late September alongside the [[30th Infantry Division (United States)|US 30th Infantry Division]], when they breached the German defences along the [[Hindenburg Line]] as part of the final Allied offensive of the war.<ref name=AWM/> Aimed at the [[Le Catelet]] Line near [[Bellicourt]], the battalion began its advance on [[Nauroy]] on 29 September, moving on the left flank beside elements of the [[117th Infantry Regiment (United States)|US 117th Infantry Regiment]], with the [[32nd Battalion (Australia)|32nd Battalion]] in support. The attack proved highly successful and 59 prisoners were captured along with four field guns and a quantity of German small arms. Against this the battalion lost 17 men killed and 63 wounded.<ref>{{harvnb|Austin|1997|p=158}}.</ref>

Following this, they were withdrawn from the front line.<ref name=AWM/> By this time casualties amongst the [[Australian Corps]] had reached critical level and as a result many battalions—from an authorised strength of over 1,000 men<ref>{{harvnb|Kuring|2004|p=47}}.</ref>—were only able to field between 300 and 400. As a result, the decision was made to reduce the number of infantry battalions in each brigade from four to three by disbanding one battalion and using its personnel to reinforce the others.<ref name=Austin162>{{harvnb|Austin|1997|p=162}}.</ref> The 29th Battalion was one of those chosen to be broken up and as a result on 19 October 1918, the 29th Battalion was disbanded.<ref name=AWM/> The majority of the battalion's personnel—29 officers and 517 other ranks—were transferred to the 32nd Battalion as reinforcements.<ref name=Austin162/>

During its service on the Western Front, the battalion suffered 485 men killed and another 1,399 men wounded.<ref name=AWM/> Members of the battalion received the following decorations: three [[Distinguished Service Order]]s and one [[Medal bar|bar]], one [[Member of the Order of the British Empire]], 20 [[Military Cross]]es, 17 [[Distinguished Conduct Medal]]s, 94 [[Military Medal]]s with three bars, three [[Meritorious Service Medal (United Kingdom)|Meritorious Service Medals]], 17 [[Mentioned in Despatches]] and five awards from other Allied nations.{{#tag:ref|There is some discrepancy in the sources about the numbers of decorations. Austin provides the following figures: three DSOs and one bar, one [[Order of the British Empire]], 22 MC, 17 DCMs, 97 MMs and four bars, three MSMs, 22 MIDs, eight foreign awards.<ref>{{harvnb|Austin|1997|pp=180–182}}.</ref>|group=Note}}<ref name=AWM/> The 29th Battalion was bestowed 19 [[battle honour]]s in 1927 for its involvement in the war.<ref name=Festberg89/>

===Inter-war years===
The battalion was re-raised in 1921 as part the re-organisation of the Australian military that took place at that time,<ref name=Grey125>{{harvnb|Grey|2008|p=125}}.</ref> with the battalion becoming a part-time unit of the [[Australian Army Reserve|Citizen Forces]], assigned to the [[4th Brigade (Australia)|4th Brigade]], [[3rd Division (Australia)|3rd Division]].<ref name=AWM2>{{cite web|url=http://www.awm.gov.au/units/unit_11313.asp|title=29th/46th Battalion (East Melbourne Regiment/Brighton Rifles) |work=Second World War, 1939&ndash;1945 units |publisher=Australian War Memorial|accessdate=15 November 2010}}</ref><ref>{{harvnb|Palazzo|2002|p=63}}.</ref> Upon formation, the battalion drew its personnel from four previously existing Citizen Forces units: the 2nd Battalion, 29th Infantry Regiment; the 5th Battalion, 6th Infantry Regiment; 5th Battalion, 15th Infantry Regiment and part of the 29th Light Horse,<ref name=Festberg89>{{harvnb|Festberg|1972|p=89}}.</ref><ref name=regts>{{cite web|url=//www.regiments.org/regiments/australia/volmil/vic-inf/29e-melb.htm |author=Mills, T.F |title=29th Battalion (The East Melbourne Regiment) |work=Land Forces of Britain, The Empire and Commonwealth |publisher=Regiments.org (archived) |accessdate=16 November 2010 |deadurl=yes |archiveurl=https://web.archive.org/web/20071111143131/www.regiments.org/regiments/australia/volmil/vic-inf/29e-melb.htm |archivedate=11 November 2007 }}</ref> and perpetuated the battle honours and traditions of its associated AIF battalion.<ref name=Grey125/> Based in [[Melbourne, Victoria|Melbourne]], the battalion was brought up to its authorised strength of around 1,000 men through the [[Conscription in Australia|compulsory training scheme]].<ref>{{harvnb|Palazzo|2002|p=64}}.</ref> The following year, however, the Army's budget was cut in half and the scope of the scheme reduced following the resolution of the [[Washington Naval Treaty]] which arguably improved Australia's strategic outlook. As a result of this, the battalion's authorised strength was reduced to just 409 men of all ranks and training and recruitment were scaled back significantly.<ref>{{harvnb|Palazzo|2002|pp=65–67}}.</ref>

In 1927, territorial titles were introduced into the Australian Army and the battalion adopted the title of the "East Melbourne Regiment".<ref name=AWM2/><ref name=stanley>{{cite web|url=http://www.army.gov.au/AHU/docs/A_Century_of_Service_Stanley.pdf |author=Stanley, Peter |title=Broken Lineage: The Australian Army's Heritage of Discontinuity |publisher=Army History Unit |work=A Century of Service |accessdate=21 November 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110604154120/http://www.army.gov.au/AHU/docs/A_Century_of_Service_Stanley.pdf |archivedate=4 June 2011 |df= }}</ref> At this time, the battalion was afforded the motto ''Nulli Secundus''.<ref name=Festberg89/> In 1929, following the election of the [[James Scullin|Scullin]] [[Australian Labor Party|Labor]] government, the compulsory training scheme was suspended altogether as it was decided to maintain the part-time military force on a volunteer-only basis.<ref name=Grey138>{{harvnb|Grey|2008|p=138}}.</ref> In order to reflect the change, the Citizen Forces was renamed the "Militia" at this time.<ref>{{harvnb|Palazzo|2001|p=110}}.</ref> The end of compulsory training and the fiscal austerity that followed with the economic downturn of the [[Great Depression]] meant that the manpower available to many Militia units at this time dropped well below their authorised establishments and as a result the decision was made to amalgamate a number of units.<ref>{{harvnb|Keogh|1965|p=44}}.</ref> Subsequently, the 29th Battalion was amalgamated with the [[22nd Battalion (Australia)|22nd]] in 1930, forming the [[29th/22nd Battalion (Australia)|29th/22nd Battalion]], although they were later split again in August 1939 and the 29th re-raised in its own right.<ref name=AWM2/><ref>{{cite web|url= http://www.awm.gov.au/units/unit_21820.asp|title=22nd Battalion (South Gippsland Regiment) |work=Second World War, 1939&ndash;1945 units |publisher=Australian War Memorial|accessdate=16 November 2010}}</ref>

===Second World War and beyond===
Following the outbreak of the [[Second World War]], the Australian government decided to raise an all-volunteer force for service overseas due to the provisions of the ''Defence Act (1903)'', which precluded compelling the Militia to serve outside of Australian territory. This force was known as the [[Second Australian Imperial Force]] (2nd AIF).<ref name=Grey146>{{harvnb|Grey|2008|p=146}}.</ref> Although the 2nd AIF would be raised upon a [[en cadre|cadre]] of trained officers and non-commissioned officers drawn from the Militia, the Militia's main role at this time was to provide training to the men called up as part of the compulsory training scheme that was readopted in early 1940.<ref name=Grey146/> Throughout 1940–41 the battalion undertook a number of short periods of continuous training, however, following the Japanese [[Attack on Pearl Harbor|bombing of Pearl Harbor]] and [[Malayan campaign|invasion of Malaya]] in December 1941, it was mobilised for war service.<ref name=AWM2/>

In March 1942, the 4th Brigade was sent to [[Queensland]] to undertake garrison duties and man defences along the coast to defend against a possible Japanese invasion.<ref name=AWM2/> By mid-1942, however, due to manpower shortages that occurred in the Australian economy as a result of over [[Military history of Australia during World War II|mobilisation of its military forces]], the Australian government decided to disband a number of Militia units in order to release their personnel back into the civilian workforce.<ref>{{harvnb|Grey|2008|p=184}}.</ref> As a result of this decision, in August 1942, the 29th Battalion was amalgamated with the [[46th Battalion (Australia)|46th Battalion]] to form the [[29th/46th Battalion (Australia)|29th/46th Battalion]]. This unit went on to serve overseas in [[New Guinea Campaign|New Guinea]] and on [[New Britain Campaign|New Britain]].<ref name=AWM2/> After the war, following the demobilisation of the wartime Army, Australia's part-time military was re-formed in 1948,<ref>{{harvnb|Grey|2008|p=200}}.</ref> but the 29th Battalion was not re-raised at the time. In 1961, although the battalion was in a state of suspended animation, it was entrusted with the four battle honours awarded to the [[2/29th Battalion (Australia)|2/29th Battalion]] for its service in Malaya during World War II and the three earned by the 29th/46th Battalion.<ref name=Festberg89/>

==Battle honours==
The 29th Battalion received the following battle honours (including those inherited from the 2/29th):
* '''First World War''': [[Battle of the Somme (1916)|Somme 1916]], [[Second Battle of the Somme (1918)|'18]]; [[Alberich (World War I German operation)|Bapaume 1917]]; [[Second Battle of Bullecourt|Bullecourt]], [[Battle of Passchendaele|Ypres 1917]]; [[Battle of Menin Road|Menin Road]]; [[Battle of Polygon Wood|Polygon Wood]]; [[Battle of Poelcappelle|Poelcappelle]]; [[Battle of Passchendaele|Passchendaele]]; [[Battle of Ancre (1918)|Ancre 1918]]; [[Battle of Amiens (1918)|Amiens]]; [[Battle of Albert (1918)|Albert 1918]]; [[Battle of Mont St Quentin|Mont St Quentin]]; [[Hindenburg Line]]; [[Battle of St. Quentin Canal|St. Quentin Canal]]; [[Western Front (World War I)|France and Flanders 1916–18]];  [[First Suez Offensive|Egypt 1915–16]].<ref name=Festberg89/>{{#tag:ref|Austin lists the battalion's battle honours as: Somme 1916–18, Bullecourt, Menin Road, Poelcappelle, Ancre 1918, Albert 1918, Hindenburg Line, France, Egypt, Bapaume 1917, Ypres 1917, Polygon Wood, Passchendaele, Amiens, Mont St Quentin, St Quentin Canal, Flanders.<ref>{{harvnb|Austin|1997|p=169}}.</ref>|group=Note}}
* '''Second World War''': [[Malayan Campaign|Malaya 1941–42]], [[Malayan Campaign|Johore]], [[Battle of Muar|The Muar]], [[Fall of Singapore|Singapore Island]], [[South West Pacific theatre of World War II|South West Pacific 1944–45]], [[New Guinea campaign|Liberation of Australian New Guinea]], [[Huon Peninsula campaign|Gusika–Fortification Point]].<ref name=Festberg89/>

==Alliances==
* United Kingdom&nbsp;– [[Worcestershire Regiment|The Worcestershire Regiment]].<ref name=Festberg89/><ref name=regts/>

==Notes==
;Footnotes
{{reflist|group=Note}}

;Citations
{{Reflist|3}}

==References==
{{Refbegin}}
* {{cite book|last = Austin |first = Ron |title=Black and Gold: The History of the 29th Battalion, 1915–1918|year=1997|location=McCrae, Victoria| publisher=Slouch Hat Publications| isbn=0-646-31650-8 |ref=CITEREFAustin1997}}
* {{cite book|last=Bean|first=Charles|title=The Australian Imperial Force in France, 1916 |series= Official History of Australia in the War of 1914–1918|volume=Volume III|edition=12th|year=1941|publisher=Australian War Memorial|location=Canberra, Australian Capital Territory|url=https://www.awm.gov.au/collection/RCDIG1069752/|oclc=220623454 |ref=CITEREFBean1941}}
* {{cite book|last=Festberg |first=Alfred |title=The Lineage of the Australian Army |year=1972 |publisher=Allara Publishing |location= Melbourne, Victoria |isbn= 978-0-85887-024-6 | ref=harv}}
* {{cite book | last=Grey | first=Jeffrey |authorlink=Jeffrey Grey  | year=2008 | title=A Military History of Australia | edition=3rd | publisher=Cambridge University Press | location=Melbourne, Victoria | isbn=978-0-521-69791-0 |ref=CITEREFGrey2008}}
* {{cite book|last=Keogh|first=Eustace|title=The South West Pacific 1941–45|year=1965|publisher=Grayflower Productions |location=Melbourne, Victoria |oclc=7185705 |ref=CITEREFKeogh1965}}
* {{Cite book|last=Kuring|first=Ian|title=Redcoats to Cams: A History of Australian Infantry 1788–2001|year=2004|publisher=Australian Military History Publications|location=Loftus, New South Wales|isbn=1-876439-99-8 |ref=CITEREFKuring2004}}
* {{Cite book |last=Palazzo |first=Albert |authorlink= |title=The Australian Army. A History of its Organisation 1901–2001 |year=2001 |publisher=Oxford University Press |location=South Melbourne, Victoria |isbn=0-19-551507-2 |ref=CITEREFPalazzo2001}}
* {{Cite book| last = Palazzo| first = Albert| authorlink = | year = 2002| title = Defenders of Australia: The 3rd Australian Division 1916–1991| publisher = Australian Military Historical Publications| location = Loftus, New South Wales| isbn = 1-876439-03-3 |ref=CITEREFPalazzo2002}}
* {{cite book|last=Palazzo|first=Albert|chapter=Organising for Jungle Warfare|title=The Foundations of Victory: The Pacific War 1943–1944|year=2004|editor=Dennis, Peter |editor2=Grey, Jeffrey |editorlink2=Jeffrey Grey  |publisher=Army History Unit|location=Canberra, Australian Capital Territory|url=http://www.army.gov.au/Our-history/Army-History-Unit/Chief-of-Army-History-Conference/Previous-Conference-Proceedings/~/media/Files/Our%20history/AAHU/Conference%20Papers%20and%20Images/2003/2003-The_Pacific_War_1943-1944_Part_1.ashx|isbn=978-0-646-43590-9 |ref=CITEREFPalazzo2004}}
{{Refend}}

{{Infantry formations of the First Australian Imperial Force |state=collapsed}}

[[Category:Australian World War I battalions]]
[[Category:Australian World War II battalions]]
[[Category:Military units and formations established in 1915]]
[[Category:Military units and formations disestablished in 1942]]
[[Category:1915 establishments in Australia]]
[[Category:Military units and formations in British Malaya in World War II|B]]