{| {{Infobox ship begin}}
{{Infobox ship image
| Ship image   = [[File:Bundesarchiv DVM 10 Bild-23-63-24, Schwerer Kreuzer "Admiral Hipper".jpg|300px]]
| Ship caption = ''Admiral Hipper'' in 1939
}}
{{Infobox ship career
| Hide header = 
| Ship country = [[Nazi Germany]]
| Ship flag = {{shipboxflag|Nazi Germany|naval}}
| Ship name = ''Admiral Hipper''
| Ship namesake = Admiral [[Franz von Hipper]]
| Ship builder = [[Blohm + Voss|Blohm & Voss]], [[Hamburg]]
| Ship laid down = 6 July 1935
| Ship launched = 6 February 1937
| Ship commissioned = 29 April 1939
| Ship fate = Scuttled, 3 May 1945, raised and scrapped in 1948–1952
}}
{{Infobox ship characteristics
| Hide header = 
| Header caption = 
| Ship class = {{sclass-|Admiral Hipper|cruiser}}
| Ship displacement| Ship displacement =
* {{convert|16170|MT|abbr=on}} (design)
* {{convert|18200|LT|abbr=on}} (full load)
| Ship length = {{convert|202.8|m|ftin|0|abbr=on}} [[Length overall|overall]]
| Ship beam   = {{convert| 21.3|m|ftin|0|abbr=on}}
| Ship draft = Full load: {{convert|7.2|m|abbr=on}}
| Ship propulsion =
* 3 × [[Blohm + Voss|Blohm & Voss]] steam turbines
* 3 × three-blade propellers
* {{convert|132000|shp|MW|abbr=on}}
| Ship speed = {{convert|32|kn|lk=in}}
| Ship range = 
| Ship complement =
*    42 officers
* 1,340 enlisted
| Ship armament =
*  8 × [[20.3 cm SK C/34 naval gun|{{convert|20.3|cm|in|abbr=on}}]] guns
* 12 × {{convert|10.5|cm|in|abbr=on}} guns
* 12 × {{convert| 3.7|cm|in|abbr=on}} guns
*  8 × {{convert| 2  |cm|in|abbr=on}} guns (20 × 1)
*  6 × {{convert|53.3|cm|in|0|abbr=on}} [[torpedo tube]]s
| Ship armor =
* Belt:         {{convert| 70|to|80|mm|abbr=on}}
* Armor deck:   {{convert| 20|to|50|mm|abbr=on}}
* Turret faces: {{convert|105|mm|abbr=on}}
| Ship aircraft = 3 aircraft
| Ship aircraft facilities = 1 catapult
| Ship notes = 
}}
|}

'''''Admiral Hipper''''', the first of five ships of her class, was the [[lead ship]] of the {{sclass-|Admiral Hipper|cruiser|4}} of [[heavy cruiser]]s which served with [[Nazi Germany]]'s ''[[Kriegsmarine]]'' during [[World War II]]. The ship was laid down at the [[Blohm & Voss]] shipyard in [[Hamburg]] in July 1935 and launched February 1937; ''Admiral Hipper'' entered service shortly before the outbreak of war, in April 1939. The ship was named after [[Admiral]] [[Franz von Hipper]], commander of the German [[battlecruiser]] [[Squadron (naval)|squadron]] during the [[Battle of Jutland]] in 1916 and later commander-in-chief of the German [[High Seas Fleet]].

''Admiral Hipper'' saw a significant amount of action during the war, notably present during the [[Battle of the Atlantic]]. She led the assault on [[Trondheim]] during [[Operation Weserübung|Operation ''Weserübung'']]; while en route to her objective, she sank the British destroyer {{HMS|Glowworm|H92|6}}. In December 1940, she broke out into the [[Atlantic Ocean]] to operate against Allied merchant shipping, though this operation ended without significant success. In February 1941, ''Admiral Hipper'' sortied again, sinking several merchant vessels before eventually returning to [[Germany]] via the [[Denmark Strait]]. The ship was then transferred to northern [[Norway]] to participate in operations against convoys to the [[Soviet Union]], culminating in the [[Battle of the Barents Sea]] on 31 December 1942, where she sank the destroyer {{HMS|Achates|H12|2}} and the minesweeper {{HMS|Bramble|J11|2}} but was in turn damaged and forced to withdraw by the light cruisers {{HMS|Sheffield|C24|6}} and {{HMS|Jamaica|44|6}}.

Disappointed by the failure to sink merchant ships in that battle, [[Adolf Hitler]] ordered the majority of the surface warships [[ship breaking|scrapped]], though Admiral [[Karl Dönitz]] was able to convince Hitler to retain the surface fleet. As a result, ''Admiral Hipper'' was returned to Germany and decommissioned for repairs. The ship was never restored to operational status, however, and on 3 May 1945, [[Royal Air Force]] bombers severely damaged her while she was in [[Kiel]]. Her crew [[scuttling|scuttled]] the ship at her [[Mooring (watercraft)|moorings]], and in July 1945, she was raised and towed to [[Heikendorf|Heikendorfer Bay]]. She was ultimately broken up for scrap in 1948–1952 and her bell is currently on display at the [[Laboe Naval Memorial]].

== Construction ==
{{main|Admiral Hipper-class cruiser}}
[[File:El crucero alemán Admiral Hipper en el astillero.jpg|thumb|left|''Admiral Hipper'' during [[fitting-out]]. Hamburg, 1937.]]

''Admiral Hipper'' was ordered by the ''Kriegsmarine'' from the [[Blohm & Voss]] shipyard in [[Hamburg]].{{sfn|Gröner|p=65}} Her keel was laid on 6 July 1935,{{sfn|Williamson|p=12}} under construction number 246.{{sfn|Gröner|p=65}} The ship was launched on 6 February 1937, and was completed on 29 April 1939, the day she was commissioned into the German fleet.{{sfn|Gröner|p=67}} The Commander-in-Chief of the ''Kriegsmarine'', ''[[Großadmiral]]'' (Grand Admiral) [[Erich Raeder]], who had been [[Franz von Hipper]]'s chief of staff during [[World War I]], gave the christening speech and his wife Erika Raeder performed the christening.{{sfn|Koop & Schmolke|p=58}}{{sfn|Philbin|p=27}} As built, the ship had a straight [[Stem (ship)|stem]], though after her launch this was replaced with a [[clipper]] bow. A raked funnel cap was also installed.{{sfn|Williamson|pp=10–11}}

''Admiral Hipper'' was {{convert|202.8|m|ft|sp=us}} [[length overall|long overall]] and had a beam of {{convert|21.3|m|ft|abbr=on}} and a maximum draft of {{convert|7.2|m|ft|abbr=on}}. After the installation of a clipper bow during fitting out, her overall length increased to {{convert|205.9|m|ft|sp=us}}. The ship had a design displacement of {{convert|16170|MT|abbr=on}} and a full load displacement of {{convert|18200|LT|abbr=on}}. ''Admiral Hipper'' was powered by three sets of geared [[steam turbine]]s, which were supplied with steam by twelve ultra-high pressure oil-fired [[boiler]]s. The ship's top speed was {{convert|32|kn|lk=in}}, at {{convert|132000|shp|lk=in}}.{{sfn|Gröner|p=65}} As designed, her standard complement consisted of 42 officers and 1,340 enlisted men.{{sfn|Gröner|p=66}}

''Admiral Hipper''{{'}}s primary armament was eight [[20.3 cm SK C/34 naval gun|{{convert|20.3|cm|in|abbr=on|1}} SK L/60]] guns mounted in four twin [[gun turret]]s, placed in [[superfire|superfiring pairs]] forward and aft.{{efn|name=gun nomenclature}} Her anti-aircraft battery was to have consisted of twelve {{convert|10.5|cm|abbr=on}} L/65 guns, twelve {{convert|3.7|cm|abbr=on}} guns, and eight {{convert|2|cm|abbr=on}} guns. The ship also would have carried a pair of triple {{convert|53.3|cm|abbr=on}} torpedo launchers abreast of the rear superstructure. The ship was to have been equipped with three [[Arado Ar 196]] seaplanes and one catapult.{{sfn|Gröner|p=66}} ''Admiral Hipper''{{'}}s [[belt armor|armored belt]] was {{convert|70|to|80|mm|abbr=on}} thick; her upper deck was {{convert|12|to|30|mm|abbr=on}} thick while the main armored deck was {{convert|20|to|50|mm|abbr=on}} thick. The main battery turrets had {{convert|105|mm|abbr=on}} thick faces and 70&nbsp;mm thick sides.{{sfn|Gröner|p=65}}

== History ==
''[[Kapitän zur See]]'' (Captain at Sea) [[Hellmuth Heye]] was given command of the ship at her commissioning.{{sfn|Williamson|p=10}} After her commissioning in April 1939, ''Admiral Hipper'' steamed into the [[Baltic Sea]] to conduct training maneuvers. The ship also made port calls to various Baltic ports, including cities in [[Estonia]] and [[Sweden]]. In August, the ship conducted live fire drills in the Baltic. At the outbreak of [[World War II]] in September 1939, the ship was still conducting gunnery trials. She was briefly used to patrol the Baltic, but she did not see combat, and was quickly returned to training exercises.{{sfn|Williamson|p=12}} In November 1939, the ship returned to the Blohm & Voss dockyard for modifications; these included the replacement of the straight stem with a clipper bow and the installation of the funnel cap.{{sfn|Williamson|p=11}}

Sea trials in the Baltic resumed in January 1940, but severe ice restrained the ship to port. On 17 February, the Kriegsmarine pronounced the ship fully operational, and on the following day, ''Admiral Hipper'' began her first major wartime patrol.{{sfn|Williamson|p=13}} She joined the [[battleship]]s [[German battleship Scharnhorst|''Scharnhorst'']] and [[German battleship Gneisenau|''Gneisenau'']] and the [[destroyer]]s [[German destroyer Z20 Karl Galster|''Karl Galster'']] and [[German destroyer Z21 Wilhelm Heidkamp|''Wilhelm Heidkamp'']] in a sortie into the North Sea off [[Bergen]], Norway. A third destroyer, [[German destroyer Z9 Wolfgang Zenker|''Wolfgang Zenker'']], was forced to turn back after sustaining damage from ice. The ships operated under the command of Admiral [[Wilhelm Marschall]].{{sfn|Rohwer|p=15}} The ships attempted to locate British merchant shipping, but failed and returned to port on 20 February.{{sfn|Williamson|p=13}}

=== Operation ''Weserübung'' ===
{{main|Operation Weserübung}}
[[File:Bundesarchiv Bild 101II-MW-5607-32, Unternehmen "Weserübung", "Admiral Hipper".jpg|thumb|upright|right|''Admiral Hipper'' loading mountain troops in [[Cuxhaven]]]]

Following her return from the North Sea sortie, ''Admiral Hipper'' was assigned to the forces tasked with the invasion of Norway, codenamed Operation ''Weserübung''.{{sfn|Williamson|p=13}} The ship was assigned as the [[flagship]] of Group 2, along with the destroyers [[German destroyer Z5 Paul Jakobi|''Paul Jakobi'']], [[German destroyer Z6 Theodor Riedel|''Theodor Riedel'']], [[German destroyer Z16 Friedrich Eckoldt|''Friedrich Eckoldt'']], and [[German destroyer Z8 Bruno Heinemann|''Bruno Heinemann'']]. ''KzS'' Heye was given command of Group 2 during the operation.{{sfn|Rohwer|p=18}} The five ships carried a total of 1,700 [[Wehrmacht]] [[List of German divisions in World War II#Mountain divisions|mountain troops]], whose objective was the port of [[Trondheim]]; the ships loaded the troops in [[Cuxhaven]].{{sfn|Williamson|p=13}}{{sfn|Mann & Jörgensen|p=38}} The ships steamed to the [[Schillig]] [[roadstead]] outside [[Wilhelmshaven]], where they joined Group 1, consisting of ten destroyers, and the battleships ''Scharnhorst'' and ''Gneisenau'', which were assigned to cover Groups 1 and 2. The ships steamed out of the roadstead at midnight on the night of 6–7 April.{{sfn|Dildy|p=30}}

While steaming off the Norwegian coast, ''Admiral Hipper'' was ordered to divert course to locate the destroyer [[German destroyer Z11 Bernd von Arnim|''Bernd von Arnim'']], which had fallen behind Group 1. In the mist, the destroyer encountered the British destroyer {{HMS|Glowworm|H92|6}}; the two destroyers engaged each other until ''Bernd von Arnim''{{'}}s commander requested assistance from ''Admiral Hipper''.{{sfn|Mann & Jörgensen|p=42}} Upon arriving on the scene, ''Admiral Hipper'' was initially misidentified by ''Glowworm'' to be a friendly vessel, which allowed the German ship to close the distance and fire first. ''Admiral Hipper'' rained fire on ''Glowworm'', scoring several hits. ''Glowworm'' attempted to flee, but when it became apparent she could not break away from the pursuing cruiser, she turned toward ''Admiral Hipper'' and fired a spread of torpedoes, all of which missed. The British destroyer scored one hit on ''Admiral Hipper''{{'}}s starboard bow before a rudder malfunction set the ship on a collision course with the German cruiser.{{sfn|Williamson|p=14}}

[[File:Bundesarchiv Bild 101I-757-0038N-11A, Norwegen, Schwerer Kreuzer.jpg|thumb|upright|left|''Admiral Hipper'' landing troops in Trondheim]]

The collision with ''Glowworm'' tore off a {{convert|40|m|adj=on|sp=us}} section of ''Admiral Hipper''{{'}}s armored belt on the starboard side, as well as the ship's starboard torpedo launcher.{{sfn|Dildy|p=33}} Minor flooding caused a four degree list to starboard, though the ship was able to continue with the mission.{{sfn|Mann & Jörgensen|p=42}} ''Glowworm''{{'}}s boilers exploded shortly after the collision, causing her to sink quickly. Forty survivors were picked up by the German ship.{{sfn|Williamson|p=13}} ''Admiral Hipper'' then resumed course toward Trondheim.{{sfn|Williamson|p=14}} The British destroyer had survived long enough to send a wireless message to the [[Royal Navy]] headquarters, which allowed the battlecruiser {{HMS|Renown|1916|2}} time to move into position to engage ''Scharnhorst'' and ''Gneisenau'', though the German battleships used their superior speed to break off contact.{{sfn|Mann & Jörgensen|pp=42–43}}

One of ''Admiral Hipper'''s Arado seaplanes had to make an emergency landing in [[Eide]], Norway on 8 April. After trying to purchase fuel from locals, the aircrew were detained and handed over to the police. The [[Royal Norwegian Navy Air Service]] captured the Arado, which was painted in Norwegian colors and used by the Norwegians until 18 April when it was evacuated to Britain.{{sfn|Sivertsen|pp=105, 115–122}}

After arriving off Trondheim, ''Admiral Hipper'' successfully passed herself off as a British warship long enough to steam past the Norwegian coastal artillery batteries. The ship entered the harbor and docked shortly before 05:30 to debark the mountain troops. After the ground troops seized control of the coastal batteries, the ship left Trondheim, bound for Germany. She was escorted by ''Friedrich Eckoldt''; she reached Wilhelmshaven on 12 April, and went into drydock. The dockyard workers discovered the ship had been damaged more severely by the collision with ''Glowworm'' than had previously been thought. Nevertheless, repairs were completed in the span of two weeks.{{sfn|Williamson|p=14}}

Admiral Marschall organized a mission to seize [[Harstad]] in early June 1940; ''Admiral Hipper'', the battleships ''Scharnhorst'' and ''Gneisenau'', and four destroyers were tasked with the operation.{{sfn|Williamson|p=14}} The ships departed on 4 June, and while en route, ''Admiral Hipper'' encountered and sank the empty troopship ''Orama'' on 9 June.{{sfn|Rohwer|p=26}} Before they reached Harstad, the Germans learned that the Allies had already abandoned the port. Marschall's squadron was then tasked with intercepting an Allied convoy that was reported to be in the area. The ships failed to find the convoy, and returned to Trondheim to refuel.{{sfn|Williamson|p=13}}

On 13 June, the ship's anti-aircraft gunners shot down an attacking British bomber.{{sfn|Williamson|p=13}} On 25 July, ''Admiral Hipper'' steamed out on a commerce raiding patrol in the area between [[Spitzbergen]] and [[Tromsø]]; the cruise lasted until 9 August.{{sfn|Rohwer|p=34}} While on the patrol, ''Admiral Hipper'' encountered the Finnish freighter ''Ester Thorden'', which was found to be carrying {{convert|1.75|MT|abbr=on}} of gold. The ship was seized and sent to occupied Norway with a prize crew.{{sfn|Williamson|p=15}}

=== Atlantic operations ===
{{seealso|Battle of the Atlantic}}
[[File:Hidroavión del crucero alemán Admiral Hipper.jpg|thumb|right|One of ''Admiral Hipper''{{'}}s three [[Arado Ar 196]] seaplanes readied for launch in 1942]]

''Admiral Hipper'' left the Norwegian theater in September 1940 for an overhaul in Wilhelmshaven. After the routine maintenance was completed toward the end of the month, the ship attempted to break out into the Atlantic Ocean to raid merchant traffic. The engine oil feed system caught fire and was severely damaged. The fire forced the crew to shut down the ship's propulsion system until the blaze could be brought under control; this rendered ''Admiral Hipper'' motionless for several hours on the open sea. British reconnaissance failed to locate the ship, and after the fire was extinguished, the ship returned to Hamburg's Blohm & Voss shipyard, where repairs lasted slightly over a week.{{sfn|Williamson|p=15}}

The ship made a second attempt to break out into the Atlantic on 30 November; she successfully navigated the [[Denmark Strait]] undetected on 6 December. ''Admiral Hipper'' intercepted a convoy of 20 troopships on 24 December,{{sfn|Williamson|p=15}} some {{convert|700|nmi|lk=in}} west of [[Cape Finisterre]]. Five of the twenty ships were allocated to [[Operation Excess|Operation ''Excess'']]. The convoy was protected by a powerful escort composed of the [[aircraft carrier]]s {{HMS|Furious|47|2}} and {{HMS|Argus|I49|2}}, the cruisers {{HMS|Berwick|65|2}}, {{HMS|Bonaventure|31|2}}, and {{HMS|Dunedin||2}}, and six destroyers.{{sfn|Rohwer|p=53}} ''Admiral Hipper'' did not initially spot the escorting warships, and so began attacking the convoy.{{sfn|Williamson|p=15}} With her main guns she badly damaged two ships,{{sfn|Rohwer|p=53}} one of which was the {{convert|13994|LT|adj=on}} transport [[List of Empire ships (Th–Ty)#Empire Trooper|''Empire Trooper'']], before spotting the heavy cruiser ''Berwick'' and destroyers steaming toward her. She quickly withdrew, using her main guns to keep the destroyers at bay.{{sfn|Williamson|p=15}}

Ten minutes later, ''Berwick'' reappeared off ''Admiral Hipper''{{'}}s port bow;{{sfn|Williamson|p=15}}{{sfn|Rohwer|p=53}} the German cruiser fired several salvos from her forward turrets and scored hits on the British cruiser's rear turrets, waterline, and forward [[superstructure]]. ''Admiral Hipper'' then disengaged, to prevent the British destroyers from closing to launch a torpedo attack. By now, the ship was running low on fuel, and so she put into [[Brest, France|Brest]] in [[German military administration in occupied France during World War II|occupied France]] on 27 December.{{sfn|Williamson|p=15}} While en route, ''Admiral Hipper'' encountered and sank the isolated {{convert|6078|LT|adj=on}} cargo ship ''Jumna''.{{sfn|Rohwer|p=53}} Another round of routine maintenance work was effected while the ship was in Brest, readying her for another sortie into the Atlantic shipping lanes.{{sfn|Williamson|p=16}}

[[File:Admiral Hipper cruiser in dry dock at Brest 1941.jpg|thumb|left|''Admiral Hipper'' in drydock in Brest]]
On 1 February 1941, ''Admiral Hipper'' embarked on her second Atlantic sortie.{{sfn|Rohwer|p=57}} The ''Kriegsmarine'' had initially sought to send the battleships ''Scharnhorst'' and ''Gneisenau'' to operate in concert with ''Admiral Hipper'', but ''Gneisenau'' suffered storm damage in December that prevented the participation of the two ships.{{sfn|Williamson|p=16}} Repairs were effected quickly, however, and the two battleships broke out into the Atlantic in early February.{{sfn|Garzke & Dulin|p=140}} ''Admiral Hipper'' rendezvoused with a tanker off the [[Azores]] to top up her fuel tanks.{{sfn|Williamson|p=16}} On 11 February, the ship encountered and sank an isolated transport from [[convoy HG 53]], which had been dispersed by [[U-boat]] and [[Luftwaffe]] attacks.{{sfn|Rohwer|p=58}} That evening, she picked up the unescorted [[convoy SLS 64]], which contained nineteen merchant ships. The following morning, ''Admiral Hipper'' closed in and sank several of the ships.{{sfn|Rohwer|pp=58–59}} The British reported only seven ships were lost, totaling {{convert|32806|LT}}, along with damage to two more.{{sfn|Williamson|p=16}}{{sfn|Rohwer|p=59}} The Germans claimed ''Admiral Hipper'' had sunk thirteen of the nineteen freighters, while some survivors reported fourteen ships of the convoy were sunk.{{sfn|Williamson|p=16}}

Following the attack on convoy SLS 64, ''Admiral Hipper''{{'}}s fuel stocks were running low. She therefore returned to Brest on 15 February. British bombers were regularly attacking the port, however, and the ''Kriegsmarine'' therefore decided ''Admiral Hipper'' should return to Germany, where she could be better protected. Before the ship could leave, damage caused to the ship's hull by wrecks in the harbor had to be repaired.{{sfn|Williamson|p=16}} On 15 March, the ship slipped out of Brest, unobserved, and passed through the Denmark Strait eight days later.{{sfn|Rohwer|p=64}} While en route, ''Admiral Hipper'' stopped to refuel in [[Bergen]].{{sfn|Williamson|p=16}} By 28 March, the cruiser was docked in Kiel, having made the entire journey without being detected by the British.{{sfn|Rohwer|p=64}} Upon arrival, the ship went into the [[Deutsche Werke]] shipyard for an extensive overhaul, which lasted for seven months. After completion of the refit, ''Admiral Hipper'' conducted sea trials in the Baltic before putting into Gotenhafen on 21 December for some minor refitting. In January 1942, the ship had her steam turbines overhauled at the Blohm & Voss shipyard; a [[degaussing]] coil was fitted to the ship's hull during this overhaul. By March, the ship was again fully operational.{{sfn|Williamson|pp=16–17}}

=== Deployment to Norway ===
[[File:German cruiser Admiral Hipper off Norway 1942.jpg|thumb|''Admiral Hipper'' in Norwegian waters, circa 1942]]
On 19 March 1942, ''Admiral Hipper'' steamed to Trondheim, escorted by the destroyers {{ship|German destroyer|Z24||2}}, {{ship|German destroyer|Z26||2}}, and {{ship|German destroyer|Z30||2}} and the torpedo boats {{ship|German torpedo boat|T15||2}}, {{ship|German torpedo boat|T16||2}}, and {{ship|German torpedo boat|T17||2}}. Several British submarines were patrolling the area, but failed to intercept the German flotilla. ''Admiral Hipper'' and her escorts reached their destination on 21 March.{{sfn|Rohwer|p=152}} There, they joined the heavy cruisers ''Lützow'' and {{ship|German cruiser|Prinz Eugen||2}}, though the latter soon returned to Germany for repairs after being torpedoed. On 3 July, ''Admiral Hipper'' joined the cruisers ''Lützow'' and {{ship|German cruiser|Admiral Scheer||2}} and the battleship {{ship|German battleship|Tirpitz||2}} for [[Operation Rösselsprung (1942)|Operation ''Rösselsprung'']], an attack on [[convoy PQ 17]].{{sfn|Williamson|p=17}} Escorting the convoy were the battleships {{HMS|Duke of York|17|6}} and {{USS|Washington|BB-56|6}} and the aircraft carrier {{HMS|Victorious|R38|6}}.{{sfn|Garzke & Dulin|p=253}} ''Admiral Hipper'', ''Tirpitz'', and six destroyers sortied from Trondheim, while a second task force consisting of ''Lützow'', ''Admiral Scheer'', and six destroyers operated out of [[Narvik]].{{sfn|Garzke & Dulin|pp=253–255}} ''Lützow'' and three of the destroyers struck uncharted rocks while en route to the rendezvous and had to return to port. Swedish intelligence had meanwhile reported the German departures to the British [[Admiralty]], which ordered the convoy to disperse. Aware that they had been detected, the Germans aborted the operation and turned over the attack to [[U-boat]]s and the [[Luftwaffe]]. The scattered vessels could no longer be protected by the convoy escorts, and the Germans sank 21 of the 34 isolated transports.{{sfn|Garzke & Dulin|p=255}}

The British submarine {{HMS|Tigris|N63|2}} unsuccessfully attempted to torpedo ''Admiral Hipper'' on 10 September, while the ship was patrolling with ''Admiral Scheer'' and the light cruiser {{ship|German cruiser|Köln||2}}.{{sfn|Williamson|p=16}} The cruiser escorted the destroyers {{ship|German destroyer|Z23||2}}, {{ship|German destroyer|Z28||2}}, {{ship|German destroyer|Z29||2}}, and ''Z30'' on 24–28 September to lay a minefield off the north-west coast of [[Novaya Zemlya]].{{sfn|Rohwer|p=197}} The goal of the operation was to funnel merchant traffic further south, closer to the reach of German naval units in Norway. After her return to port, ''Admiral Hipper'' was transferred to Bogen Bay near Narvik for repairs to her propulsion system.{{sfn|Williamson|p=16}} On 28–29 October, ''Admiral Hipper'' and the destroyers ''Friedrich Eckoldt'' and ''Richard Beitzen'' were transferred further north from Narvik to the [[Altafjord]].{{sfn|Rohwer|p=206}} Starting on 5 November, ''Admiral Hipper'' and the 5th Destroyer Flotilla, composed of {{ship|German destroyer|Z27||2}}, ''Z30'', ''Richard Beitzen'', and ''Friedrich Eckoldt'', patrolled for Allied shipping in the Arctic. ''[[Vizeadmiral]]'' [[Oskar Kummetz]] commanded the squadron from ''Admiral Hipper.'' On 7 November, the cruiser's Arado Ar 196 floatplane located the {{convert|7925|LT|adj=on}} Soviet tanker ''Donbass'' and its escort, the auxiliary warship ''BO-78''. Kummetz dispatched the destroyer ''Z27'' to sink the two Soviet ships.{{sfn|Rohwer|p=207}}

==== Battle of the Barents Sea ====
{{main|Battle of the Barents Sea}}

In December 1942, convoy traffic to the Soviet Union resumed. ''Großadmiral'' Raeder ordered a plan, [[Operation Regenbogen (Arctic)|Operation ''Regenbogen'']], to use the available surface units in Norway to launch an attack on the convoys. The first convoy of the month, [[Convoy JW 51A|JW 51A]], passed to the Soviet Union without incident. However, the second, [[convoy JW 51B]], was spotted by the submarine {{GS|U-354||2}} south of [[Bear Island (Norway)|Bear Island]]. Raeder ordered the forces assigned to Operation ''Regenbogen'' into action.{{sfn|Miller|p=331}} ''Admiral Hipper'', again served as Kummetz's flagship; the squadron comprised ''Lützow'' and the destroyers ''Friederich Eckoldt'', ''Richard Beitzen'', ''Theodor Riedel'', ''Z29'', ''Z30'', and [[German destroyer Z31|''Z31'']].{{sfn|Rohwer|p=221}} The force left Altafjord at 18:00 on 30 December, under orders to avoid confrontation with even an equal opponent.{{sfn|Miller|p=332}}

Kummetz's plan was to divide his force in half; he would take ''Admiral Hipper'' and three destroyers north of the convoy to attack it and draw away the escorts. ''Lützow'' and the remaining three destroyers would then attack the undefended convoy from the south. At 09:15 on the 31st, the British destroyer {{HMS|Obdurate|G39|2}} spotted the three destroyers screening for ''Admiral Hipper''; the Germans opened fire first. Four of the other five destroyers escorting the convoy rushed to join the fight, while {{HMS|Achates|H12|2}} laid a smoke screen to cover the convoy. ''Admiral Hipper'' fired several salvos at ''Achates'', raining shell splinters on the destroyer that severed steam lines and reduced her speed to {{convert|15|kn}}. Kummetz then turned back north to draw the destroyers away. Captain [[Robert St Vincent Sherbrooke|Robert Sherbrooke]], the British escort commander, left two destroyers to cover the convoy while he took the remaining four to pursue ''Admiral Hipper''.{{sfn|Miller|p=332}}

Rear Admiral [[Robert Burnett]]'s Force R, centered on the cruisers {{HMS|Sheffield|C24|2}} and {{HMS|Jamaica|44|2}}, standing by in distant support of the Allied convoy,{{sfn|Miller|p=331}} raced to the scene. The cruisers engaged ''Admiral Hipper'', which had been firing to port at the destroyer {{HMS|Obedient|G48|2}}. Burnett's ships approached from ''Admiral Hipper''{{'}}s starboard side and achieved complete surprise.{{sfn|Pope|pp=214–215}} In the initial series of salvos from the British cruisers, ''Admiral Hipper'' was hit three times.{{sfn|Rohwer|p=221}} One of the hits damaged the ship's propulsion system; the No. 3 boiler filled with a mix of oil and water, which forced the crew to turn off the starboard turbine engine. This reduced her speed to {{convert|23|kn}}. The other two hits started a fire in her aircraft hangar. She fired a single salvo at the cruisers before turning toward them, her escorting destroyers screening her with smoke.{{sfn|Pope|p=215}}

After emerging from the smoke screen, Hipper was again engaged by Burnett's cruisers. Owing to the uncertainty over the condition of his flagship and the ferocity of the British defense, Kummetz issued the following order at 10:37: "Break off action and retire to the west."{{sfn|Pope|p=219}} Mistakenly identifying ''Sheffield'' as ''Admiral Hipper'', the destroyer ''Friederich Eckoldt'' approached too closely and was sunk.{{sfn|Murfett|p=496}} Meanwhile, ''Lützow'' closed to within {{convert|3|nmi|abbr=on}} of the convoy, but due to poor visibility, she held her fire. She then received Kummetz's order, and turned west to rendezvous with ''Admiral Hipper''. ''Lützow'' inadvertently came alongside ''Sheffield'' and ''Jamaica'', and after identifying them as hostile, engaged them. The British cruisers turned toward ''Lützow'' and came under fire from both German cruisers. ''Admiral Hipper''{{'}}s firing was more accurate and quickly straddled ''Sheffield'', though the British cruiser escaped unscathed. Burnett quickly decided to withdraw in the face of superior German firepower; his ships were armed with {{convert|6|in|abbr=on}} guns, while ''Admiral Hipper'' and ''Lützow'' carried {{convert|20.3|cm|abbr=on}} and {{convert|28|cm|abbr=on}} guns, respectively.{{sfn|Pope|pp=228–229}}

Based on the order issued at the outset of the operation to avoid action with a force equal in strength to his own, poor visibility, and the damage to his flagship, Kummetz decided to abort the attack. In the course of the battle, the British destroyer ''Achates'' was sunk by the damage inflicted by ''Admiral Hipper''. The Germans also sank the [[minesweeper (ship)|minesweeper]] {{HMS|Bramble|J11|2}} and damaged the destroyers {{HMS|Onslow|G17|2}}, ''Obedient'', and ''Obdurate''. In return, the British sank ''Friederich Eckoldt'' and damaged ''Admiral Hipper'', and forced the Germans to abandon the attack on the convoy.{{sfn|Rohwer|p=221}} In the aftermath of the failed operation, a furious Hitler proclaimed that the ''Kriegsmarine'''s surface forces would be paid off and dismantled, and their guns used to reinforce the fortifications of the [[Atlantic Wall]]. Admiral [[Karl Dönitz]], Raeder's successor, convinced Hitler to retain the surface fleet, however.{{sfn|Mann & Jörgensen|pp=139–141}} After returning to Altafjord, emergency repairs to ''Admiral Hipper'' were effected, which allowed her to return to Bogen Bay on 23 January 1943.{{sfn|Williamson|p=20}} That day, ''Admiral Hipper'', ''Köln'', and the destroyer ''Richard Beitzen'' left the Altafjord to return to Germany. The three ships stopped in Narvik on 25 January, and in Trondheim from 30 January to 2 February.{{sfn|Rohwer|p=227}} After resuming the voyage south, the ships searched for Norwegian blockade runners in the [[Skagerrak]] on 6 February before putting into port at Kiel on 8 February.{{sfn|Rohwer|pp=227–228}} On 28 February, the ship was decommissioned in accordance with Hitler's decree.{{sfn|Williamson|p=20}}

=== Fate ===
[[File:Admiral Hipper cruiser in dry dock Kiel 1945.jpg|thumb|''Admiral Hipper'' in dry dock at Kiel 19 May 1945]]

Despite being decommissioned, repair work on the ship continued.{{sfn|Williamson|p=20}} The ship was moved in April to [[Pillau]] in the Baltic, to put ''Admiral Hipper'' out of the reach of Allied bombers. A year later, the ship was moved to [[Gotenhafen]]; the ''Kriegsmarine'' intended to re-commission the ship so she could be used in the Baltic. Over the next five months, ''Admiral Hipper'' ran a series of sea trials in the Baltic, but failed to reach operational status. As the Soviet army pushed the Germans back on the Eastern Front, her crew was drafted into construction work on the defenses of the city, further impairing ''Admiral Hipper''{{'}}s ability to enter active service. The [[Royal Air Force]] also laid an extensive minefield around the port, which forced the ship to remain in the harbor.{{sfn|Williamson|p=20}}

By the end of 1944, the ship was due for another overhaul; work was to have lasted for three months. The Soviet Army had advanced so far, however, that it was necessary to move the ship farther away from the front, despite the fact that she had only one working turbine. On 29 January 1945, the ship left Gotenhafen, arriving in Kiel on 2 February. She entered the [[Germaniawerft]] shipyard for refitting. On 3 May, RAF bombers attacked the harbor and severely damaged the ship.{{sfn|Williamson|p=21}} Her crew scuttled the wrecked ship at her moorings at 04:25 on 3 May. In July 1945, after the end of the war, ''Admiral Hipper'' was raised and towed to [[Heikendorf|Heikendorfer Bay]] and subsequently broken up for scrap in 1948–1952. Her bell was on display at the [[National Maritime Museum]] in [[Greenwich]].{{sfn|Gröner|p=67}} The bell has since been returned to Germany and is on display at the [[Laboe Naval Memorial]].{{sfn|Showell|p=79}}
{{clear}}

== Footnotes ==
'''Notes'''

{{notes
| notes =
{{efn
| name = gun nomenclature
| "L/60" denotes the length of the gun in terms of [[Caliber (artillery)|caliber]]. The length of 60 caliber gun is 60 times greater than it is wide in diameter.
}}

}}

'''Citations'''

{{reflist|20em}}

== References ==
{{commons category|Admiral Hipper (ship, 1937)|<br />Admiral Hipper}}

* {{cite book
  | last = Dildy
  | first = Doug
  | year = 2007
  | title = Denmark and Norway 1940: Hitler's Boldest Operation
  | publisher = Osprey Publishing
  | location = Oxford
  | isbn = 978-1-84603-117-5
  | ref = {{sfnRef|Dildy}}
  }}
* {{cite book
  | last1 = Garzke
  | first1 = William H.
  | last2 = Dulin
  | first2 = Robert O.
  | year = 1985
  | title = Battleships: Axis and Neutral Battleships in World War II
  | publisher = Naval Institute Press
  | location = Annapolis, MD
  | isbn = 978-0-87021-101-0
  | oclc = 
  | ref = {{sfnRef|Garzke & Dulin}}
  }}
* {{cite book
  | last = Gröner
  | first = Erich
  | year = 1990
  | title = German Warships: 1815–1945
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-0-87021-790-6
  | ref = {{sfnRef|Gröner}}
  }}
* {{Cite book
  | last1 = Koop
  | first1 = Gerhard
  | last2 = Schmolke
  | first2 = Klaus-Peter
  | title = Die Schweren Kreuzer der Admiral Hipper-Klasse
  | trans_title = The Heavy Cruisers of the Admiral Hipper Class
  | location = Bonn, Germany
  | publisher = Bernard & Graefe Verlag
  | language = German
  | year = 1992
  | isbn = 978-3-7637-5896-8
  | ref = {{sfnRef|Koop & Schmolke}}
}}
* {{cite book
  | last1 = Mann
  | first1 = Chris
  | last2 = Jörgensen
  | first2 = Christer
  | year = 2003
  | title = Hitler's Arctic War: the German campaigns in Norway, Finland, and the USSR, 1940–1945
  | publisher = Thomas Dunne Books
  | location = New York, NY
  | isbn = 978-0-312-31100-1
  | ref = {{sfnRef|Mann & Jörgensen}}
  }}
* {{cite book
  | last = Miller
  | first = Nathan
  | year = 1997
  | title = War at Sea: A Naval History of World War II
  | publisher = Oxford University Press
  | location = New York, NY
  | isbn = 978-0-19-511038-8
  | ref = {{sfnRef|Miller}}
  }}
* {{cite book
  | last = Murfett
  | first = Malcolm H.
  | year = 2008
  | title = Naval Warfare 1919–1945
  | publisher = Routledge
  | location = London
  | isbn = 978-0-415-45804-7
  | ref = {{sfnRef|Murfett}}
  }}
* {{cite book
  |last=Philbin
  |first=Tobias R., III
  |title=Admiral von Hipper: The Inconvenient Hero
  |year=1982
  |publisher=B. R. Grüner Publishing Co.
  |location=Amsterdam
  |isbn=90-6032-200-2
  | ref = {{sfnRef|Philbin}}
  }}
* {{cite book
  | last = Pope
  | first = Dudley
  | year = 2005
  | title = 73 North: The Battle of the Barents Sea
  | publisher = McBooks Press
  | location = Ithaca, NY
  | isbn = 978-1-59013-102-2
  | ref = {{sfnRef|Pope}}
  }}
* {{cite book
  | last = Rohwer
  | first = Jürgen
  | authorlink = Jürgen Rohwer
  | year = 2005
  | title = Chronology of the War at Sea, 1939–1945: The Naval History of World War Two
  | publisher = US Naval Institute Press
  | location = Annapolis
  | isbn = 978-1-59114-119-8
  | ref = {{sfnRef|Rohwer}}
  }}
* {{cite book
  |last=Showell
  |first= Jak P Mallmann
  |title=German Navy Handbook, 1939&ndash;1945
  |location=Gloucestershire, UK
  |publisher= Thrupp, Stroud
  |year=1999
  |isbn=0750915560
  | ref = {{sfnRef|Showell}}
  }}
* {{cite book 
  | title = Jageren Sleipner i Romsdalsfjord sjøforsvarsdistrikt april 1940
  | editor-last = Sivertsen 
  | editor-first = Svein Carl 
  | year = 1999 
  | publisher = Sjømilitære Samfund ved Norsk Tidsskrift for Sjøvesen 
  | location = Hundvåg 
  | isbn = 82-994738-3-7 
  | language = Norwegian 
  | ref = {{sfnRef|Sivertsen}}
  }}
* {{cite book
  | last = Williamson
  | first = Gordon
  | year = 2003
  | title = German Heavy Cruisers 1939–1945
  | publisher = Osprey Publishing
  | location = Oxford
  | isbn = 978-1-84176-502-0
  | ref = {{sfnRef|Williamson}}
  }}

{{Admiral Hipper class cruiser}}
{{Blohm + Voss}}
{{May 1945 shipwrecks}}
{{Good article}}

{{Authority control}}

{{DEFAULTSORT:Admiral Hipper}}
[[Category:Admiral Hipper-class cruisers]]
[[Category:Ships built in Hamburg]]
[[Category:1937 ships]]
[[Category:World War II cruisers of Germany]]
[[Category:Scuttled vessels of Germany]]
[[Category:Maritime incidents in May 1945]]