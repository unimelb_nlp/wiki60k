{{Infobox pro wrestling championship
|championshipname=AWA Brass Knuckles Championship
|image=
|alt=
|image_size=
|caption=
|currentholder=
|won=
|aired=
|promotion=[[American Wrestling Association]] (AWA)
|created=February 12, 1979
|mostreigns=N/A
|firstchamp=Don Fargo
|shortestreign=[[Gino Martino]] (176 days) <!-- estimated -->
|longestreign=Don Fargo (595 days)
|oldest=
|youngest=
|heaviest=
|lightest=
|pastnames=
|titleretired=2006
|pastlookimages=
}}
The '''AWA Brass Knuckles Championship''' was a [[professional wrestling]] [[championship (professional wrestling)|championship]] owned by the [[American Wrestling Association]] (AWA) [[professional wrestling promotion|promotion]]. The championship was introduced on February 12, 1979 at a [[Mid-South Coliseum]] [[house show|live event]]. It was active until May 1981 when the title belt was abandoned after [[The Crusher (wrestler)|Crusher Lisowski]] left the promotion.

It debuted as a part of a storyline to introduce [[Don Fargo]] to the [[List of NWA territories|Memphis wrestling territory]]; this allowed Fargo to challenge various wrestlers in [[hardcore wrestling|hardcore-themed matches]]. Like most professional wrestling [[Championship (professional wrestling)#Gimmick/style championships|"brass knuckle" championships]], both wrestlers would heavily tape their fists to give the impression that each participant were wearing [[brass knuckles]]; the title was generally defended in [[Professional wrestling match types#No Disqualification match|no-disqualification matches]] and it was legal to punch an opponent. Fargo and Lisowski were the only champions during its original run, with [[Gino Martino]] as the first and last champion of the revived [[Wrestling Superstars Live|AWA Superstars]] version.

==History==
The AWA Brass Knuckles Championship was first defended on February 12, 1979 at the [[Mid-South Coliseum]], in a match between [[Don Fargo]] and [[Robert Gibson (wrestler)|Robert Gibson]]. Fargo was billed by the promotion as the "World Brass Knuckles Champion",<ref>{{cite web |url=http://www.kayfabememories.com/Regions/memphis/memphis-cwa7-2.htm |title=Memphis/CWA #7 Page #2 |author=Dills, Tim |work=Regional Territories: Memphis/CWA |publisher=KayfabeMemories.com |accessdate=May 1, 2012}}</ref> however, the title mostly defended in the [[Continental Wrestling Association]]. Together with manager [[Al Denney|Al Greene]], Fargo would use the title to engage in wild brawls with other stars of the Memphis territory. During his 1979-80 run with the CWA and his brother [[Jackie Fargo]] were allies of [[Jerry Lawler|Jerry "The King" Lawler]]; Fargo also formed tag teams Chris Colt, [[Dick Beyer|The Destroyer]], and [[Dennis Condrey]].<ref>{{cite web |url=http://www.prowrestlinghistory.com/memphis/jarrett/1979.html |title=1979 |author= |work=Mid-South Coliseum 1979 (Jarrett) |publisher=ProWrestlingHistory.com |accessdate=May 1, 2012}}</ref> One of his most notable matches as AWA Brass Knuckles Champion was against [[Jimmy Valiant]] on September 29, 1980, who defeated Fargo via disqualification.<ref>{{cite web |url=http://www.prowrestlinghistory.com/memphis/jarrett/1980.html#092980 |title=1980 |author= |work=Mid-South Coliseum 1980 (Jarrett) |publisher=ProWrestlingHistory.com |accessdate=May 1, 2012}}</ref> The title was abandoned after Fargo left the Memphis territory at the end of the month.<ref name="Solie">{{cite web |url=http://www.solie.org/titlehistories/bktawa.html |title=AWA Brass Knuckles Title History |author=Ferraro, John |year=2005 |work=Solie's Title Histories |publisher=Solie.org |accessdate=May 1, 2012}}</ref>

On October 20, 1980, [[The Crusher (wrestler)|Crusher Lisowski]] won the vacant title after defeating [[Jerry Blackwell|"Crusher" Jerry Blackwell]] in [[St. Paul, Minnesota]]. Lisowski defended the title in the [[American Wrestling Association]] for five months until leaving the promotion in the summer of 1981, after having suffered a career-ending injury at the hands of Jerry Blackwell,<ref>{{cite web |url=http://www.kayfabememories.com/Regions/awa/awa24-2.htm |title=AWA #24 Page #2 |author=Keith, Scott |work=Regional Territories: AWA |publisher=KayfabeMemories.com |accessdate=May 1, 2012}}</ref><ref>{{cite web |url=http://slam.canoe.ca/Slam/Wrestling/2005/10/23/1275272.html |title=SLAM! Wrestling: The Crusher dead at 79 |author=Oliver, Greg |date=October 23, 2005 |work= |publisher=[[Canoe.ca|SLAM! Sports]] |accessdate=May 1, 2012}}</ref> and the title became abandoned once again.<ref name="Solie"/>

The title remained inactive for 25 years until being revived by [[Wrestling Superstars Live|AWA Superstars]] in late-2005. The storyline behind this title began in [[Millennium Wrestling Federation]], then the AWA affiliate promotion for [[New England]],<ref>{{cite web |url=http://www.mwfprowrestling.com/mwfawa.html |title=THE MILLENNIUM WRESTLING FEDERATION BECOMES THE AMERICAN WRESTLING ASSOCIATION'S NEW ENGLAND AFFILIATE |author=Millennium Wrestling Federation |authorlink=Millennium Wrestling Federation |date=March 20, 2005 |work= |publisher=MWFprowrestling.com |accessdate=May 1, 2012}}</ref> during the [[feud (professional wrestling)|feud]] between [[Gino Martino|"The Extreme Strongman" Gino Martino]] and [[Ox Baker]]. Baker claimed that he and The Crusher got into a brawl at Lisowski's bar in [[Milwaukee, Wisconsin]] in 1981, leaving with the brass knuckles title after knocking out Lisowski in the bar-room brawl. Baker then had the title, in actuality a trophy, melted down into a championship belt. He then used the brass knuckles title to lure Martino into a match with a member of Ox Baker's Army. On September 24, Martino won the title after defeating [[Kamala (wrestler)|Kamala the Ugandan Giant]] in a "King of the Jungle" death match at the MWF's "Road to the Gold" in [[Lynn, Massachusetts]].<ref name="Solie"/><ref name="MWF-RTTG">{{cite web |url=http://www.mwfprowrestling.com/mwfnewsline20050925rtgresults.html |title=MWF ROAD TO THE GOLD 2005 RESULTS |author= |date=September 25, 2005 |work= |publisher=MWFprowrestling.com |accessdate=May 1, 2012}}</ref> A title defense against [[The Missing Link (wrestler)|The Missing Link]] at "Soul Survivor III" on November 5, 2005, ended in a no-contest.<ref>{{cite web |url=http://www.mwfprowrestling.com/mwfnewsline20051106-ss3results.html |title=MWF SOUL SURVIVOR III QUICK RESULTS & NOTES |author=Millennium Wrestling Federation |date=November 6, 2005 |work= |publisher=MWFprowrestling.com |accessdate=May 1, 2012}}</ref> In the next few two months he also defended the title against The Outpatient, Leo Venis, and Makua of The Big Islanders in both No Disqualification and Falls Count Anywhere matches.<ref name="OWW-MWF">{{cite web |url=http://www.onlineworldofwrestling.com/results/awanew/_2005/ |title=American Wrestling Association (2005) |author= |work=Results |publisher=OnlineWorldofWrestling.com |accessdate=May 1, 2012}}</ref><ref name="OWW-Martino">{{cite web |url=http://www.onlineworldofwrestling.com/bios/g/gino-martino/ |title=Gino Martino |author= |year=2007 |work=Wrestler Profiles |publisher=OnlineWorldofWrestling.com |accessdate=May 1, 2012}}</ref>

It was considered an [[Championship (professional wrestling)#Unsanctioned championships|"unsanctioned" championship]] by the MWF and was defended not only in the AWA's [[Eastern Canada|eastern Canadian]] affiliate, CWA Montreal, but in rival New England promotions as well. On November 19, 2005, Martino defeated The Outpatient in a [[Falls Count Anywhere match|Cape Cod Collision Match]] at an interpromotional show for the MWF and [[Paulie Gilmore]]'s New World Wrestling in [[East Sandwich, Massachusetts]].<ref name="OWW-MWF"/><ref name="OWW-Martino"/> He was later awarded the NWW Undisputed Brass Knuckles Championship several months later given that Martino also held the AWA and [[NWA New England Brass Knuckles Championship]].<ref>{{cite web |url=http://www.solie.org/titlehistories/ubktnww.html |title=NWW Undisputed Brass Knuckles Title History |author=Ferraro, John |year=2007 |work=Solie's Title Histories |publisher=Solie.org |accessdate=May 1, 2012}}</ref>

On January 16, 2006, Martino made his first title defense in CWA Montreal successfully defended the title in a Bloodbath match against [[Pierre Vachon (wrestler)|Pierre "The Beast" Vachon]]. In the following months he also wrestled Hellraiser Payne and Abdul Hannish<ref name="OWW-Martino"/><ref>{{cite web |url=http://www.onlineworldofwrestling.com/results/awanew/_2006/ |title=American Wrestling Association (2006) |author= |work=Results |publisher=OnlineWorldofWrestling.com |accessdate=May 1, 2012}}</ref><ref>{{cite web |url=http://www.onlineworldofwrestling.com/results/cwa-montreal/ |title=CWA Montreal |author= |work=Results |publisher=OnlineWorldofWrestling.com |accessdate=May 1, 2012}}</ref> before the title was abandoned in March 2006. The championship was permanently deactivated when [[Wrestling Superstars Live|AWA Superstars]] was forced via a court order to rescind the sanctioning rights for the MWF and most of its other affiliates the following year.

==Reigns==
Don Fargo was the first champion in the title's history. He also held the record for longest reign at 595 days. Gino Martino's only championship reign lasted about 5 months. There were three wrestlers who each had one reigns in the title history.
{| class="wikitable"
|-
|'''#'''
|Order in reign history
|-
|'''Reign'''
|The reign number for the specific set of wrestlers listed
|-
|'''Event'''
|The event promoted by the respective promotion in which the titles were won
|-
| —
|Used for vacated reigns so as not to count it as an official reign
|}
{| class="wikitable sortable" width=100% style="text-align: center"
!style="background:#e3e3e3" width=0 data-sort-type="number"|#
!style="background:#e3e3e3" width=20%|Wrestlers
!style="background:#e3e3e3" width=0%|Reign
!style="background:#e3e3e3" width=10%|Date
!style="background:#e3e3e3" width=0%|Days<br />held
!style="background:#e3e3e3" width=16%|Location
!style="background:#e3e3e3" width=17%|Event
!style="background:#e3e3e3" width=60% class="unsortable"|Notes
!style="background:#e3e3e3" width=0% class="unsortable"|Ref(s)
|-
!1
|{{sort|Don Fargo|Don Fargo}}
|1
|{{dts|1979|02|12}}
|{{age in days nts|month1=02|day1=12|year1=1979|month2=09|day2=29|year2=1980}}
|{{sort|Memphis|[[Memphis, Tennessee]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|Don Fargo is billed as champion upon entering the promotion.
|align="left"|<ref name="Solie"/>
|-style="background-color:#e3e3e3"
|{{sort|1.1|—}}
|{{sort|Deactivated|[[Glossary of professional wrestling terms#Vacated|Deactivated]]}}
|{{sort|zz|—}}
|{{dts|link=off|1980|October}}
|{{sort|zz|—}}
|{{sort|zz|N/A}}
|{{sort|zz|N/A}}
|align="left"|The title is abandoned after Don Fargo leaves the Continental Wrestling Association.
|align="left"|<ref name="Solie"/>
|-
!2
|{{sort|Lisowski|[[The Crusher (wrestler)|Crusher Lisowski]]}}
|{{sort|01|1}}
|{{dts|1980|10|12}}
|{{sort|zz|N/A}}
|{{sort|St. Paul|[[St. Paul, Minnesota]]}}
|{{sort|Live event|[[House show|Live event]]}}
|align="left"|
|align="left"|<ref name="Solie"/>
|-style="background-color:#e3e3e3"
|{{sort|2.1|—}}
|{{sort|Deactivated|[[Glossary of professional wrestling terms#Vacated|Deactivated]]}}
|{{sort|zz|—}}
|{{dts|link=off|1981|May}}
|{{sort|zz|—}}
|{{sort|zz|N/A}}
|{{sort|zz|N/A}}
|align="left"|The title is abandoned after Crusher Lisowski leaves the American Wrestling Association.
|align="left"|<ref name="Solie"/>
|-
!3
|{{sort|Martino|[[Gino Martino]]}}
|1
|{{dts|2005|09|24}}
|{{sort|zz|N/A}}
|{{sort|Lynn|[[Lynn, Massachusetts]]}}
|{{sort|Road to the Gold|Road to the Gold (2005)}}
|align="left"|Gino Martino defeated [[Kamala (wrestler)|Kamala the Ugandan Giant]] in a "King of the Jungle" death match to crown a new champion.
|<ref name="Solie"/><ref name="MWF-RTTG"/><ref name="OWW-Martino"/>
|-style="background-color:#e3e3e3"
|{{sort|3.1|—}}
|{{sort|—|Deactivated}}
|{{sort|0.0|—}}
|{{dts|2006|March}}
|{{sort|zz|—}}
|{{sort|zz|—}}
|{{sort|zz|—}}
|align="left"|The title was abandoned in March 2006. [[Wrestling Superstars Live|AWA Superstars]] eventually terminated its relationship with the [[Millennium Wrestling Federation]], thus deactivating the title.
|&nbsp;
|}

==See also==
{{Portal|Professional wrestling}}
*[[American Wrestling Association]]

==References==
{{reflist}}

==External links==
*[http://www.wildcatbelts.com/awa-brass-knuckles-belt.php AWA Brass Knuckles Championship at WildcatBelts.com], a photo gallery of the championship belt used by AWA Superstars

[[Category:American Wrestling Association championships]]
[[Category:Unsanctioned championships]]
[[Category:2005 in professional wrestling]]
[[Category:2006 in professional wrestling]]