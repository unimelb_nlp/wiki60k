{{refimprove|date=August 2016}}
{{Infobox military person
|name=Wolf-Udo Ettel
|birth_date={{birth date|1921|2|26|df=y}}
|death_date={{death date and age|1943|7|17|1921|2|26|df=y}}
|birth_place=[[Hamburg]]
|death_place=[[Catania]], [[Sicily]]
|placeofburial=war cemetery at [[Motta Sant'Anastasia]]
|image=Wolf-Udo Ettel.jpg
|caption=Wolf-Udo Ettel
|nickname=
|allegiance={{flag|Nazi Germany}}
|branch={{Luftwaffe}}
|serviceyears=1939–43
|rank=[[Oberleutnant]]
|commands=8./[[JG 27]]
|unit= [[JG 3]], [[JG 27]]
|battles=[[World War II]]
|awards=[[Knight's Cross of the Iron Cross with Oak Leaves]] (posthumous)
|laterwork=}}

'''Wolf-Udo Ettel''' (26 February 1921 – 17 July 1943) was a German [[World War II]]  [[Luftwaffe]] 124 victories [[Flying ace]] and a posthumous recipient of the coveted [[Knight's Cross of the Iron Cross with Oak Leaves]], awarded by Nazi Germany to recognise extreme battlefield bravery or successful military leadership. Ettel was credited with 124 aerial victories—that is, 124 aerial combat encounters resulting in the destruction of the enemy aircraft—claimed in over 250 missions.  Ettel  was [[killed in action]] by [[Anti-aircraft warfare|anti-aircraft artillery]] on 17 July 1943.

==Early life and career==
Ettel was born on 26 February 1921 in [[Hamburg]] in the [[Weimar Republic]]. He and his two younger brothers attended the [[Potsdam]] [[National Political Institutes of Education]] (''Nationalpolitische Erziehungsanstalt''—Napola) which was  a [[Secondary school|secondary]] [[boarding school]] founded under the recently established [[Nazi Germany|Nazi state]]. The goal of the Napola schools was to raise a new generation for the political, military and administrative leadership of the [[Third Reich]].<ref>Busacker-Lührssen 1997, p. 6.</ref>  Ettel  joined the military service of the ''[[Luftwaffe]]'' on 15 November 1939.

==World War II==
''[[Leutnant]]'' Ettel was posted to 4. ''[[Organization of the Luftwaffe (1933–1945)#Staffel|Staffel]]'' (squadron) of [[Jagdgeschwader 3|''Jagdgeschwader'' 3]] (JG 3—3rd Fighter Wing) on the [[Eastern Front (World War II)|Eastern Front]] in early 1942. On 24 June, he claimed his first two victories when he shot down two [[Il-2]] Sturmovik  aircraft. He, himself, was shot down near [[Voronezh]] on 10 July while destroying a Russian-flown [[Douglas Boston]] bomber. He bailed out of his stricken [[Messerschmitt Bf 109]] F-4 "White 1" behind Russian lines but swam across the River Don to return to his unit four days later. He recorded his 20th victory on 9 August and his 30th by 7 October.

4./JG 3 was relocated to the [[Kuban bridgehead]] in February 1943, and during the months of intensive operations Ettel claimed some 28 Russian aircraft shot down in March and 36 more in April, including five shot down on 11 April. On 28 April 1943, Ettel was credited with his 100th aerial victory. He was the 38th ''Luftwaffe'' pilot to achieve the century mark.<ref>Obermaier 1989, p. 243.</ref> He claimed his 120th victory on 11 May but was shot down by flak and belly-landed his Bf 109 G–4 between the front lines but was able to return to German lines despite Russian rifle fire. That same night Ettel led a Wehrmacht patrol to his damaged aircraft to salvage important equipment. Ettel was awarded the [[Knight's Cross of the Iron Cross]] on 1 June.<ref>Weal 2001, p. 66.</ref>

On 5 June 1943, Ettel was appointed ''Staffelkapitän'' (squadron leader) to 8./[[Jagdgeschwader 27]] (JG 27—27th Fighter Wing) based in Greece. He shot down a [[Royal Air Force]] (RAF) Spitfire over [[Sicily]] on 14 July. On 16 July, he shot down two [[United States Army Air Forces]] (USAAF) [[B-24 Liberator]] bombers and another Spitfire. Ettel was shot down and [[killed in action]] on 17 July 1943 while flying a [[Close air support|ground support]] mission attacking British [[Anti-aircraft warfare|Anti Aircraft artillery]] position south east of [[Lentini]].<ref>Scutts 1994, p. 60.</ref> Ettel was posthumously awarded the 289th [[Knight's Cross of the Iron Cross with Oak Leaves]] on 31 August 1943.<ref>Weal 2003, p. 97.</ref>

Wolf Ettel was credited with 124 victories in over 250 missions. Of his 120 victories over the Eastern Front 21 were Il-2 Sturmovik ground-attack aircraft. He recorded 4 victories over Sicily which included 2 USAAF four-engine bombers.<ref>Obermaier 1989, p. 59.</ref>

==Awards==
* [[Iron Cross]] (1939)
** 2nd Class (24 July 1942)<ref name="Stockert p372">Stockert 1997, p. 372.</ref>
** 1st Class (2 August 1942)<ref name="Stockert p372"/>
* [[Front Flying Clasp of the Luftwaffe]] in Gold (23 October 1942)<ref name="Stockert p372"/>
* [[Ehrenpokal der Luftwaffe]] on 25 June 1943 as ''[[Leutnant]]'' and pilot<ref>Patzwall 2008, p. 74.</ref>
* [[German Cross]] in Gold on 23 December 1942 as ''Leutnant'' in the 4./''Jagdgeschwader'' 3<ref>Patzwall & Scherzer 2001, p. 106.</ref>
* [[Knight's Cross of the Iron Cross with Oak Leaves]]
** Knight's Cross on 1 June 1943 as pilot in the 4./''Jagdgeschwader'' 3 "Udet"<ref name="Scherzer p299">Scherzer 2007, p. 299.</ref>
** 289th Oak Leaves on 31 August 1943 (posthumously) as ''[[Oberleutnant]]'' and ''[[Staffelkapitän]]'' of the 8./''Jagdgeschwader'' 27<ref name="Scherzer p299"/>

==References==

===Citations===
{{Reflist|25em}}

===Bibliography===
{{Refbegin}}
* {{Cite book
  |last=Obermaier
  |first=Ernst
  |year=1989
  |title=Die Ritterkreuzträger der Luftwaffe Jagdflieger 1939 – 1945
  |trans_title=The Knight's Cross Bearers of the Luftwaffe Fighter Force 1939 – 1945
  |language=German
  |location=Mainz, Germany
  |publisher=Verlag Dieter Hoffmann
  |isbn=978-3-87341-065-7
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D.
  |last2=Scherzer
  |first2=Veit
  |year=2001
  |title=Das Deutsche Kreuz 1941 – 1945 Geschichte und Inhaber Band II
  |trans_title=The German Cross 1941 – 1945 History and Recipients Volume 2
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-45-8
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D.
  |year=2008
  |title=Der Ehrenpokal für besondere Leistung im Luftkrieg
  |trans_title=The Honor Goblet for Outstanding Achievement in the Air War
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-08-3
}}
* {{Cite book
  |last=Scherzer
  |first=Veit
  |year=2007
  |title=Die Ritterkreuzträger 1939–1945 Die Inhaber des Ritterkreuzes des Eisernen Kreuzes 1939 von Heer, Luftwaffe, Kriegsmarine, Waffen-SS, Volkssturm sowie mit Deutschland verbündeter Streitkräfte nach den Unterlagen des Bundesarchives
  |trans_title=The Knight's Cross Bearers 1939–1945 The Holders of the Knight's Cross of the Iron Cross 1939 by Army, Air Force, Navy, Waffen-SS, Volkssturm and Allied Forces with Germany According to the Documents of the Federal Archives
  |language=German
  |location=Jena, Germany
  |publisher=Scherzers Militaer-Verlag
  |isbn=978-3-938845-17-2
}}
* {{Cite book
  |last=Scutts
  |first=Jerry
  |year=1994
  |title=Bf 109 Aces of North Africa and the Mediterranean
  |location=London, UK
  |publisher=[[Osprey Publishing]]
  |isbn=978-1-85532-448-0
}}
* {{Cite book
  |last=Spick
  |first=Mike
  |year=1996
  |title=Luftwaffe Fighter Aces
  |location=New York
  |publisher=[[Ivy Books]]
  |isbn=978-0-8041-1696-1
}}
* {{Cite book
  |last=Stockert
  |first=Peter
  |year=1997
  |title=Die Eichenlaubträger 1939–1945 Band 3
  |trans_title=The Oak Leaves Bearers 1939–1945 Volume 3
  |language=German
  |location=Bad Friedrichshall, Germany
  |publisher=Friedrichshaller Rundblick
  |isbn=978-3-932915-01-7
}}
* {{Cite book
  |last=Thomas
  |first=Franz
  |year=1997
  |title=Die Eichenlaubträger 1939–1945 Band 1: A–K
  |trans_title=The Oak Leaves Bearers 1939–1945 Volume 1: A–K
  |language=German
  |location=Osnabrück, Germany
  |publisher=Biblio-Verlag
  |isbn=978-3-7648-2299-6
}}
* {{Cite book
  |last=Weal
  |first=John
  |year=2001
  |title=Bf 109 Aces of the Russian Front
  |location=Oxford, UK
  |publisher=[[Osprey Publishing]]
  |isbn=978-1-84176-084-1
}}
* Weal, John (2003). ''Jagdgeschwader 27 'Afrika'''. London: Osprey Publishing. ISBN 1-841765-38-4.
{{Refend}}

{{Knight's Cross recipients of JG 3}}
{{Knight's Cross recipients of JG 27}}
{{Top German World War II Aces}}
{{Authority control}}
{{Subject bar
| portal1=Aviation
| portal2=Biography
| portal3=Military of Germany
| portal4=World War II
}}

{{DEFAULTSORT:Ettel, Wolf-Udo}}
[[Category:1921 births]]
[[Category:1943 deaths]]
[[Category:People from Hamburg]]
[[Category:Luftwaffe pilots]]
[[Category:German World War II flying aces]]
[[Category:German military personnel killed in World War II]]
[[Category:Recipients of the Gold German Cross]]
[[Category:Recipients of the Knight's Cross of the Iron Cross with Oak Leaves]]