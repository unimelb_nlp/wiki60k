{{Use dmy dates|date=May 2015}}
{{Use British English|date=May 2015}}
{{good article}}
{{Infobox military structure
|name=Beaumaris Castle
|location=[[Beaumaris, Wales|Beaumaris]], [[Wales]]
|map_type = Wales Anglesey
|map_alt = Located in North West Wales
|map_caption = Location in Anglesey
|map_size = 200
|coordinates = {{coord|53.2648|-4.0897|type:landmark|display=inline}}
|image=[[File:Beaumaris aerial.jpg|300px]]
|caption=The castle seen from the air
|type=[[Concentric castle]]
|code=
|built= 1295 &ndash; c. 1330
|builder=[[James of St George]]<br>Nicolas de Derneford
|materials= [[Limestone]], [[sandstone]] and [[schist]]
|height= {{convert|36|ft}}
|used=
|demolished=
|condition= Ruined
|ownership=
|controlledby=[[Cadw]]
|website= [http://cadw.gov.wales/daysout/beaumaris-castle/?lang=en Cadw]
|garrison=
|commanders=
|occupants=
|battles=
|events= Revolt of [[Owain Glyndŵr]] (1400–09) <br>[[English Civil War]] (1642–48)
|module = {{Infobox designation list
  | embed = yes
  | designation1 = UK GRADE I
  | designation1_date = 
  }}
}}

'''Beaumaris Castle''' ({{lang-cy|Castell Biwmares}}), located in the [[Beaumaris|town of the same name]] on the [[Anglesey|Isle of Anglesey]] in [[Wales]], was built as part of [[Edward I of England|Edward I]]'s campaign to conquer the north of Wales after 1282. Plans were probably first made to construct the castle in 1284, but this was delayed due to lack of funds and work only began in 1295 following the [[Madog ap Llywelyn]] uprising. A substantial workforce was employed in the initial years under the direction of [[James of St George]]. Edward's invasion of Scotland soon diverted funding from the project, however, and work stopped, only recommencing after an invasion scare in 1306. When work finally ceased around 1330 a total of £15,000 had been spent, a huge sum for the period, but the castle remained incomplete.

Beaumaris Castle was taken by Welsh forces in 1403 during the rebellion of [[Owain Glyndŵr]], but was recaptured by royal forces in 1405. Following the outbreak of the [[English Civil War]] in 1642, the castle was held by forces loyal to [[Charles I of England|Charles I]], holding out until 1646 when it surrendered to the [[Roundheads|Parliamentary]] armies. Despite forming part of a local royalist rebellion in 1648 the castle escaped [[slighting]] and was garrisoned by Parliament, but fell into ruin around 1660, eventually forming part of a local stately home and park in the 19th century. In the 21st century the ruined castle is managed by [[Cadw]] as a tourist attraction.

Historian [[A. J. Taylor|Arnold Taylor]] described Beaumaris Castle as Britain's "most perfect example of symmetrical concentric planning".<ref name="Taylor125"/> The fortification is built of local stone, with a [[moat]]ed outer ward guarded by twelve towers and two gatehouses, overlooked by an inner ward with two large, D-shaped gatehouses and six massive towers. The inner ward was designed to contain ranges of domestic buildings and accommodation able to support two major households. The south gate could be reached by ship, allowing the castle to be directly supplied by sea. [[UNESCO]] considers Beaumaris to be one of "the finest examples of late 13th century and early 14th century military architecture in Europe", and it is classed as a [[World Heritage site]].<ref>{{cite web| url = http://whc.unesco.org/en/list/374/| title = Castles and Town Walls of King Edward in Gwynedd| mode=cs2| accessdate = 22 September 2012| publisher = [[UNESCO]]}}</ref>

==History==

===13th–14th centuries===

The kings of England and the Welsh princes had vied for control of North Wales since the 1070s and the conflict had been renewed during the 13th century, leading to [[Edward I of England|Edward I]] intervening in North Wales for the second time during his reign in 1282.<ref>{{harvnb|Ashbee|2007|p=5}}; {{harvnb|Taylor|2004|pp=6–7}}</ref> Edward invaded with a huge army, pushing north from [[Carmarthen]] and westwards from [[Montgomery, Powys|Montgomery]] and [[Chester]].<ref>{{harvnb|Ashbee|2007|p=6}}</ref> Edward decided to permanently colonise North Wales and provisions for its governance were set out in the [[Statute of Rhuddlan]], enacted on 3&nbsp;March 1284. Wales was divided into [[Historic counties of Wales|counties and shires]], emulating how England was governed, with three new shires created in the north-west, [[Caernarfonshire|Caernarfon]], [[Merioneth]] and [[Anglesey]].<ref name=Taylor2004P5>{{harvnb|Taylor|2004|p=5}}</ref> New towns with protective castles were established at [[Caernarfon]] and [[Harlech]], the administrative centres of the first two shires, with another castle and walled town built in nearby [[Conwy]], and plans were probably made to establish a similar castle and settlement near the town of [[Llanfaes]] on Anglesey.<ref name=Taylor2004P5/> Llanfaes was the wealthiest borough in Wales and largest in terms of population, an important trading port and on the preferred route from North Wales to Ireland.<ref name=Taylor2004P5/> The huge cost of the building the other castles, however, meant that the Llanfaes project had to be postponed.<ref name=Taylor2004P5/>

[[File:Beaumaris Castle - geograph.org.uk - 28577.jpg|thumb|left|The moated north-west walls of the outer ward]]
In 1294 [[Madog ap Llywelyn]] rebelled against English rule.<ref name=Taylor2004P6>{{harvnb|Taylor|2004|p=6}}</ref> The revolt was bloody and amongst the casualties was Roger de Pulesdon, the sheriff of Anglesey.<ref name=Taylor2004P6/> Edward suppressed the rebellion over the winter and once Anglesey was reoccupied in April 1295 he immediately began to progress the delayed plans to fortify the area.<ref name=Taylor2004P6/> The chosen site was called Beaumaris, meaning "fair marsh", whose name derives from the Norman-French ''Beau Mareys'', and in Latin the castle was termed ''de Bello Marisco''.<ref>{{harvnb|Taylor|2004|pp=3, 6}}</ref> This was about {{convert|1|mi}} from Llanfaes and the decision was therefore taken to move the Welsh population of Llanfaes some {{convert|12|mi}} south-west, where a settlement by the name of [[Newborough, Anglesey|Newborough]] was created for them.<ref name=Taylor2004P6/> The deportation of the local Welsh opened the way for the construction of a prosperous English town, protected by a substantial castle.<ref>{{harvnb|Taylor|2004|pp=5–6}}</ref>

The castle was positioned in one corner of the town, following a similar town plan to that in the town of Conwy, although in Beaumaris no town walls were constructed at first, despite some foundations being laid.<ref>{{harvnb|Taylor|2004|p=36}}; {{harvnb|Lilley|2010|p=104}}</ref> Work began in the summer of 1295, overseen by Master [[James of St George]].<ref name=Taylor2004P6/> James had been appointed the "master of the king's works in Wales", reflecting the responsibility he had in their construction and design. From 1295 onwards, Beaumaris became his primary responsibility and more frequently he was given the title "''magister operacionum de Bello Marisco''".<ref name=Taylor125>{{harvnb|Taylor|1987|p=125}}</ref> The work was recorded in considerable detail on the [[pipe rolls]], the continuous records of medieval royal expenditure, and, as a result, the early stages of construction at Beaumaris are relatively well understood for the period.<ref>{{harvnb|Taylor|1987|pp=125–126}}; {{harvnb|Lyon1980|pp=112–113}}</ref>

[[File:Beaumaris Castle - geograph.org.uk - 1140107.jpg|thumb|The entrance way through the southern gatehouse]]
A huge amount of work was undertaken in the first summer, with an average of 1,800 workmen, 450 stonemasons and 375 quarriers on the site.<ref>{{harvnb|Taylor|2004|p=7}}</ref> This consumed around £270 a week in wages and the project rapidly fell into arrears, forcing officials to issue leather tokens instead of paying the workforce with normal coinage.<ref>{{harvnb|Taylor|2004|p=8}}; {{harvnb|Prestwich|2003|p=17}}</ref>{{refn|It is impossible to accurately compare medieval and modern prices or incomes. For comparison, a typical baron of the period enjoyed an average annual income of around £700. The wage costs at Harlech of £270 a week were around a third of their annual income, the total cost of the castle at £15,000 around twenty times their annual income.<ref>{{harvnb|Pounds|1994|p=147}}</ref>|group="nb"}} The centre of the castle was filled with temporary huts to house the workforce over the winter.<ref>{{harvnb|Taylor|2004|p=21}}</ref> The following spring, James explained to his employers some of the difficulties and the high costs involved:

{{quote|In case you should wonder where so much money could go in a week, we would have you know that we have needed&nbsp;– and shall continue to need 400&nbsp;masons, both cutters and layers, together with 2,000 less skilled workmen, 100&nbsp;carts, 60&nbsp;wagons and 30&nbsp;boats bringing stone and sea coal; 200&nbsp;quarrymen; 30&nbsp;smiths; and carpenters for putting in the joists and floor boards and other necessary jobs. All this takes no account of the garrison&nbsp;... nor of purchases of material. Of which there will have to be a great quantity&nbsp;... The men's pay has been and still is very much in arrears, and we are having the greatest difficulty in keeping them because they have simply nothing to live on.<ref>{{harvnb|McNeill|1992|p=43}}</ref>}}

The construction slowed during 1296, although debts continued to build up, and work dropped off further the following year, stopping entirely by 1300, by when around £11,000 had been spent.<ref>{{harvnb|Taylor|2004|pp=8, 11}}</ref> The halt was primarily the result of Edward's new wars in Scotland, which had begun to consume his attention and financial resources, but it left the castle only partially complete: the inner walls and towers were only a fraction of their proper height and the north and north-west sides lacked outer defences altogether.<ref>{{harvnb|Taylor|2004|p=8}}; {{harvnb|Prestwich2003||p=25}}</ref> In 1306 Edward became concerned about a possible Scottish invasion of North Wales, but the unfinished castle had already fallen into a poor state of repair.<ref>{{harvnb|Taylor|2004|pp=8, 10–11}}</ref> Work recommenced on completing the outer defences, first under James' direction and then, after his death in 1309, Master Nicolas de Derneford.<ref name=Taylor2004P11>{{harvnb|Taylor|2004|p=11}}</ref> This work finally halted in 1330 with the castle still not built to its intended height; by the end of the project, £15,000 had been spent, a colossal sum for the period.<ref name=Taylor2004P11/> A royal survey in 1343 suggested that at least a further £684 would be needed to complete the castle, but this was never invested.<ref name=Taylor2004P14>{{harvnb|Taylor|2004|p=14}}</ref>

===15th–21st centuries===
[[File:Beaumaris, 1610.jpg|thumb|300px|left|A map by [[John Speed]] showing the castle and the adjacent walled town in 1610]]
In 1400 a revolt broke out in North Wales against English rule, led by [[Owain Glyndŵr]].<ref name=Taylor2007P10>{{harvnb|Taylor|2007|p=10}}</ref> Beaumaris Castle was placed under siege and captured by the rebels in 1403, being retaken by royal forces in 1405.<ref name=Taylor2004P14/> The castle was ill-maintained and fell into disrepair and by 1534, when [[Roland de Velville]] was the castle constable, rain was leaking into most of the rooms.<ref>{{harvnb|Taylor|2004|p=14}}; {{harvnb|Weir|2008|p=152}}</ref> In 1539 a report complained that it was protected by an arsenal of only eight or ten small guns and forty bows, which the castle's new constable, Richard Bulkeley, considered to be completely inadequate for protecting the fortress against a potential Scottish attack.<ref name=Taylor2004P14/> Matters worsened and by 1609 the castle was classed as "utterlie decayed".<ref name=Taylor2004P15>{{harvnb|Taylor|2004|p=15}}</ref>

The [[English Civil War]] broke out in 1642 between the [[Cavaliers|Royalist]] supporters of [[Charles I of England|Charles I]] and the [[Roundheads|supporters of Parliament]]. Beaumaris Castle was a strategic location in the war, as it controlled part of the route between the king's bases in Ireland and his operations in England.<ref name=Taylor2004P15/> [[Thomas Bulkeley, 1st Viscount Bulkeley|Thomas Bulkeley]], whose family had been involved in the management of the castle for several centuries, held Beaumaris for the king and may have spent around £3,000 improving its defences.<ref>{{harvnb|Taylor|2004|pp=14–15}}</ref>{{refn|It is difficult to accurately compare 17th century and modern prices or incomes. £3,000 could equate to between £406,000 to £86,000,000 in 2011 terms, depending on the measure used. For comparison, [[Henry Somerset, 1st Marquess of Worcester|Henry Somerset]], one of the richest men in England at the time, had an annual income of around £20,000.<ref>{{cite web| url = http://www.measuringworth.com/ukcompare/| title = Measuring Worth Five Ways to Compute the Relative Value of a UK Pound Amount, 1830 to Present| mode=cs2|accessdate = 12 September 2012| publisher = MeasuringWorth}}; {{harvnb|Pugin|1895|p=23}}</ref>|group="nb"}} By 1646, however, Parliament had defeated the royal armies and the castle was surrendered by Colonel Richard Bulkeley in June.<ref name=Taylor2004P15/> Anglesey revolted against Parliament again in 1648, and Beaumaris was briefly reoccupied by royalist forces, surrendering for a second time in October that year.<ref name="Taylor2004P15"/>

After the war many castles were [[slighting|slighted]], damaged to put them beyond military use, but Parliament was concerned about the threat of a royalist invasion from Scotland and Beaumaris was spared.<ref>{{harvnb|Thompson|1994|pp=153–155}}</ref> Colonel John Jones became the castle governor and a garrison was installed inside, at a cost of £1,703 a year.<ref name=Taylor2004P15/> When [[Charles II of England|Charles II]] returned to the throne in 1660 and restored the Bulkeley family as castle constables, Beaumaris appears to have been stripped of its valuable [[lead]] and remaining resources, including the roofs.<ref name="Taylor 2004 15, 17">{{harvnb|Taylor|2004|pp=15, 17}}</ref>

{{Infobox World Heritage Site
| WHS         = Castles and Town Walls of King Edward in Gwynedd
| State Party = Wales
| Type        = Cultural
| Criteria    = i, iii, iv
| ID          = 374
| Region      = [[List of World Heritage Sites in Europe|Europe and North America]]
| Year        = 1986
| Session     = 10th
| Link        = http://whc.unesco.org/en/list/374
}}
[[Thomas Bulkeley, 7th Viscount Bulkeley|Lord Thomas Bulkeley]] bought the castle from the Crown in 1807 for £735, incorporating it into the park that surrounded his local residence, [[Baron Hill, Anglesey|Baron Hill]].<ref>{{harvnb|Taylor|2004|p=17}}; {{cite web| url = http://cadw.wales.gov.uk/docs/cadw/publications/WHS_part_2_EN.pdf| title = Part 2: Significance and Vision| page =62| accessdate = 12 September 2012| mode=cs2|publisher =Cadw}}</ref>{{refn|£735 in 1807 could equate to between £56,000 to £2,600,000 in 2011 terms, depending on the measure used.<ref>{{cite web| url = http://www.measuringworth.com/ukcompare/| title = Measuring Worth Five Ways to Compute the Relative Value of a UK Pound Amount, 1830 to Present| mode=cs2|accessdate = 12 September 2012| publisher = MeasuringWorth}}</ref>|group="nb"}} By then the castles of North Wales had become attractive locations for visiting painters and travellers, who considered the ivy-clad ruins [[Romanticism|romantic]]. Although not as popular as other sites in the region, Beaumaris formed part of this trend and was visited by the future [[Queen Victoria]] in 1832 for an [[Eisteddfod]] festival and it was painted by [[J. M. W. Turner]] in 1835.<ref name="Taylor 2004 15, 17"/> Some of the castle's stones may have been reused in 1829 to build the nearby [[Beaumaris Gaol]].<ref name=Taylor2004P15/>

In 1925 [[Sir Richard Williams-Bulkeley, 12th Baronet|Richard Williams-Bulkeley]] gave Beaumaris to the [[Office of Works|Commissioners of Works]], who then carried out a large scale restoration programme, stripping back the vegetation, digging out the moat and repairing the stonework.<ref>{{harvnb|Taylor|2004|p=17}}</ref> In  1950 the castle, considered by the authorities to be "one of the outstanding Edwardian medieval castles of Wales", was designated as a Grade I [[listed building]]&nbsp;– the highest grade of listing, protecting buildings of "exceptional, usually national, interest".<ref>{{harvnb|Cadw|2005|p=6}}; {{Cite web|url=http://jura.rcahms.gov.uk/cadw/cadw_eng.php?id=5574|title=Beaumaris Castle|author=[[Cadw]]|publisher=Historic Wales|year=2009|mode=cs2|accessdate=1 October 2012}}</ref>

Beaumaris was declared part of the ''[[Castles and Town Walls of King Edward in Gwynedd]]'' [[World Heritage site]] in 1986, [[UNESCO]] considering it one of "the finest examples of late 13th century and early 14th century military architecture in Europe".<ref name=UNESCOCTW>{{cite web| url = http://whc.unesco.org/en/list/374/| title = Castles and Town Walls of King Edward in Gwynedd| accessdate = 22 September 2012| mode=cs2|publisher = [[UNESCO]]
}}</ref> In the 21st century Beaumaris Castle is managed by [[Cadw]], the [[Welsh Assembly Government]]'s agency for historic [[monument]]s, as a tourist attraction, with 75,000 visitors during the 2007–08 financial year.<ref>{{cite web| url =http://cadw.wales.gov.uk/docs/cadw/publications/InterpplanCastlesEdwardI_EN.pdf| title = Interpretation Plan for the Castles and Town Walls of Edward I, for Cadw| page =3| mode=cs2|accessdate = 20 September 2012| publisher =Cadw}}</ref> The castle requires ongoing maintenance and repairs cost £58,000 over the 2002–03 financial year.<ref>{{cite web| url = http://cadw.wales.gov.uk/docs/cadw/publications/WHS_part_2_EN.pdf| title = Part 2: Significance and Vision| page =56| mode=cs2|accessdate = 12 September 2012| publisher =Cadw}}</ref>

==Architecture==
[[File:Beaumaris plan, Cadw.jpg|thumb|275px|Plan of the castle]]
Beaumaris Castle was never fully built, but had it been completed it would probably have closely resembled [[Harlech Castle]].<ref name=CreightonHighamTaylor2003>{{harvnb|Creighton|Higham|2003|p=49}}; {{harvnb|Taylor|2004|p=11}}</ref> Both castles are [[concentric castle|concentric]] in plan, with walls within walls, although Beaumaris is the more regular in design.<ref name=CreightonHighamTaylor2003/> Historian Arnold Taylor described Beaumaris as Britain's "most perfect example of symmetrical concentric planning" and for many years the castle was regarded as the pinnacle of military engineering during Edward I's reign.<ref>{{harvnb|Taylor|1987|p=125}}; {{harvnb|Creighton|Higham|2003|p=49}}; {{harvnb|Toy|1985|p=161}}</ref> This evolutionary interpretation [[Castles in Great Britain and Ireland#Historiography|is now disputed]] by historians: Beaumaris was as much a royal palace and symbol of English power as it was a straightforward defensive fortification.<ref>{{harvnb|Liddiard|2005|pp=54–58}}</ref> Nonetheless, the castle is praised by UNESCO as a "unique artistic achievement" for the way in which is combines "characteristic 13th century double-wall structures with a central plan" and for the beauty of its "proportions and masonry".<ref name=UNESCOCTW/>

Beaumaris Castle was built at around sea-level on top of the [[till]] and other sediments that form the local coastline, and was constructed from local Anglesey stone from within {{convert|10|mi}} of the site, with some stones brought along the coast by ship, for example from the limestone quarries at [[Penmon]].<ref>{{harvnb|Lott|2010|pp=118–119}}; {{harvnb|Taylor|2004|p=40}}</ref> The stone was a mixture of [[limestone]], [[sandstone]] and green [[schist]]s, which was used fairly randomly within the walls and towers; the use of schists ceased after the pause in the building work in 1298 and as a result is limited to the lower levels of the walls.<ref>{{harvnb|Lott|2010|pp=118}}; {{harvnb|Taylor|2004|p=40}}</ref>

The castle design formed an inner and an outer ward, surrounded in turn by a moat, now partially filled.<ref>{{harvnb|Taylor|2004|p=19}}</ref> The main entrance to the castle was the Gate next the Sea, next to the castle's [[tide|tidal]] [[Dock (maritime)|dock]] that allowed it to be supplied directly by sea.<ref>{{harvnb|Taylor|2004|pp=20, 39}}</ref> The dock was protected by a wall later named the Gunners Walk and a firing platform that may have housed a [[trebuchet]] [[siege engine]] during the medieval period.<ref>{{harvnb|Taylor|2004|p=39}}</ref> The Gate next the Sea led into an outer [[barbican]], protected by a [[drawbridge]], [[arrow slits]] and [[murder-hole]]s, leading on into the outer ward.<ref>{{harvnb|Taylor|2004|pp=20–21}}</ref>

The outer ward consisted of an eight-sided [[Curtain wall (fortification)|curtain wall]] with twelve turrets enclosing an area approximately {{convert|60|ft}} across; one gateway led out to the Gate next the Sea, the other, the Llanfaes Gate, led out to the north side of the castle.<ref>{{harvnb|Taylor|2004|pp=19, 39}}</ref> The defences were originally equipped with around 300 firing positions for archers, including 164 arrow slits, although 64 of the slits close to the ground level have since been blocked in to prevent them being exploited by attackers, either in the early 15th century or during the Civil War.<ref>{{harvnb|Taylor|2004|pp=33, 35}}</ref>

{{Multiple image|direction=horizontal|align=left|image1=The North Gatehouse of Beaumaris Castle - geograph.org.uk - 1526007.jpg|image2=Harlech Castle2.jpg|width=200|caption1=The unfinished north gatehouse in Beaumaris's inner ward...|caption2=...and its equivalent, which was nearly completed, at [[Harlech Castle]]<ref name=Taylor2004P25>{{harvnb|Taylor|2004|p=25}}</ref> }}
The walls of the inner ward were more substantial than those of the outer ward, {{convert|36|ft|adj=on}} high and {{convert|15.5|ft|adj=on}} thick, with huge towers and two large gatehouses, enclosing a {{convert|0.75|acre|adj=on}} area.<ref>{{harvnb|Taylor|2004|pp=19, 21}}</ref> The inner ward was intended to hold the accommodation and other domestic buildings of the castle, with ranges of buildings stretching along the west and east sides of the ward; some of the remains of the fireplaces for these buildings can still be seen in the stonework.<ref>{{harvnb|Taylor|2004|pp=21–22}}</ref> It is uncertain if these ranges were actually ever built or if they were constructed but later demolished after the Civil War.<ref name=Taylor2004P23>{{harvnb|Taylor|2004|p=23}}</ref> If finished, the castle would have been able to host two substantial households and their followers, for example the king and queen, or the king, queen and a prince and his own wife.<ref>{{harvnb|Taylor|2004|p=32}}</ref>

The D-shaped north gatehouse in the inner ward was intended to be two storeys high, with two sets of five, large windows, of which only one floor was actually completed.<ref name=Taylor2004P23/> It would have included a large hall on the first floor, around {{convert|70|ft}} by {{convert|25|ft}} across, divided into two with separate fireplaces for heating.<ref name=Taylor2004P25/> The south gatehouse was designed to be a replica of that on the north side, but building work progressed even less far before finishing in 1330.<ref name=Taylor2004P26>{{harvnb|Taylor|2004|p=26}}</ref> Some of the stonework may since have been removed from the gatehouse, reducing its height even further.<ref name=Taylor2004P26/>

The walls of the inner ward contain extensive first floor passageways, similar to those at [[Caernarfon Castle]].<ref>{{harvnb|Taylor|2004|p=27}}</ref> These were intended to allow members of the castle to move between the towers, accessing the guardrooms, sleeping chambers and the castle latrines.<ref name=Taylor2004P25/> The latrines were designed to be drained by a special system using the water from the moat, but the system does not appear to have worked well in practice.<ref>{{harvnb|Taylor|2004|p=31}}</ref> The six towers were intended to be three storeys high and contained fireplaces.<ref>{{harvnb|Taylor|2004|p=30}}</ref> The castle [[chapel]] was built into one of the towers and would have been used by the king and his family, rather than the wider garrison.<ref>{{harvnb|Taylor|2004|p=29}}</ref>

{{wide image|Beaumaris-01s.jpg|1000px|align-cap=center|The concentric design of Beaumaris meant the outer curtain was overlooked entirely by the castle's inner ward}}

==See also==
*[[Castles in Great Britain and Ireland]]
*[[List of castles in Wales]]
*[[List of Cadw properties]]

==References==

===Notes===
{{reflist|group="nb"}}

===References===
{{reflist|colwidth=30em}}

===Bibliography===
{{refbegin}}
*{{citation| last=Ashbee| first=Jeremy| year=2007| title=Conwy Castle| publisher=Cadw| publication-place=Cardiff, UK| isbn= 9781857602593|ref=harv}}
*{{citation| last =Cadw|url=http://cadw.wales.gov.uk/docs/cadw/publications/What_Is_Listing_EN.pdf|format=PDF|title=Listed Buildings in Wales: What is Listing?|publisher=Cadw|location=Cardiff, UK|year=2005|origyear=1996|edition=3rd|isbn=1-85760-222-6}}
*{{citation|last1=Creighton |first1=Oliver |last2=Higham |first2=Robert |title=Medieval Castles |isbn= 9780747805465 |year=2003| publisher=Shire Archaeology| location=Princes Risborough, UK|ref=harv}}
*{{citation |last=Liddiard |first=Robert |title=Castles in Context: Power, Symbolism and Landscape, 1066 to 1500 |publisher=Windgather Press |location=Macclesfield, UK |year=2005 |isbn=9780954557522}}
*{{citation| last=Lilley| first=Keith D.| editor-last=Williams| editor-first=Diane| editor2-last=Kenyon| editor2-first=John| year=2010| title=The Impact of Edwardian Castles in Wales| chapter=The Landscapes of Edward's New Towns: Their Planning and Design| publisher=Oxbow Books| publication-place=Oxford, UK| pages=99–113| isbn= 978-1-84217-380-0|ref=harv}}
*{{citation| last=Lott| first=Graham| editor-last=Williams| editor-first=Diane| editor2-last=Kenyon| editor2-first=John| year=2010| title=The Impact of Edwardian Castles in Wales| chapter=The Building Stones of the Edwardian Castles| publisher=Oxbow Books| publication-place=Oxford, UK| pages=114–120| isbn= 978-1-84217-380-0|ref=harv}}
*{{citation |last=Lyon |first=Bryce Dale |title=A Constitutional and Legal History of Medieval England |edition=2nd |publisher=Norton |location=New York, US |year=1980 |origyear=1960 |isbn=0-393-95132-4}}
*{{citation |last=McNeill |first=Tom |year=1992 |title=English Heritage Book of Castles |location=London, UK |publisher=English Heritage and B. T. Batsford |isbn=0-7134-7025-9}}
*{{cite book |last=Pounds|first=N. J. G.  |title=The Medieval Castle in England and Wales: A Social and Political History |isbn=978-0-521-45099-7 |year=1994 |publisher=Cambridge University Press| location= Cambridge, UK|ref=harv}}
*{{citation |last=Prestwich|first=Michael |title=The Three Edwards: War and State in England, 1272–1377 |location=London, UK|publisher=Routledge |year=2003 |origyear=1980 |edition=2nd |isbn=9780415303095}}
*{{citation |last=Pugin|first=Augustus  |title=Examples of Gothic Architecture Selected From Various Ancient Edifices in England|oclc= 31592053
|year=1895|publisher=J. Grant|location=Edinburgh, UK|ref=harv}}
*{{citation |last=Taylor |first=Arnold |authorlink=A. J. Taylor |contribution=The Beaumaris Castle Building Account of 1295–1298 |location=Cardiff, UK |publisher=University of Wales Press |title=Castles in Wales and the Marches: Essays in Honour of D. J. Cathcart King |editors=John R. Kenyon and Richard Avent |year=1987 |isbn=0-7083-0948-8 |pages=125–142}}
*{{citation |last=Taylor |first=Arnold |title=Beaumaris Castle |location=Cardiff, UK |publisher=Cadw |year=2004 |origyear=1980 |edition=5th |isbn=1-85760-208-0}}
*{{citation|last=Taylor |first=Arnold  |title=Harlech Castle |isbn= 978-1-85760-257-9 |year=2007|edition=4th |publisher=Cadw| location=Cardiff, UK|ref=harv}}
*{{citation |last=Thompson |first=M. W. |title=The Decline of the Castle |location=Leicester, UK |publisher=Harveys Books |year=1994 |isbn=1-85422-608-8}}
*{{citation|last=Toy |first=Sidney  |title=Castles: Their Construction and History |isbn= 978-0-486-24898-1 |year=1985 |origyear=1939| publisher=Dover| location=New York, US|ref=harv}}
*{{citation |last=Weir|first=Alison |title=Britain's Royal Families: the Complete Genealogy |location=London, UK |publisher=Vintage Books |year=2008 |isbn=9780099539735}}

{{refend}}

==External links==
{{Commons category|Beaumaris Castle}}
*[http://cadw.wales.gov.uk/daysout/beaumaris-castle/?lang=en Official Cadw website]

{{Edwardian castles in Wales}}
{{World Heritage Sites in the United Kingdom}}
{{Anglesey}}

[[Category:Buildings and structures completed in 1330]]
[[Category:Cadw]]
[[Category:Castles in Anglesey]]
[[Category:Scheduled monuments in Anglesey]]
[[Category:World Heritage Sites in Wales]]
[[Category:Unfinished castles]]
[[Category:Museums in Anglesey]]
[[Category:Water castles in the United Kingdom]]
[[Category:Grade I listed castles in Wales]]
[[Category:Grade I listed buildings in Anglesey]]
[[Category:Beaumaris]]
[[Category:Castle ruins in Wales]]