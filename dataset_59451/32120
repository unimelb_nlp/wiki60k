{{featured article}}
{{Use British English|date=September 2012}}
{{Use dmy dates|date=September 2012}}
{{Infobox book| 
| name = Live and Let Die
| image = Live and Let Die first edition novel cover.jpg
| caption = First edition cover, published by Jonathan Cape
| alt = A book cover, in deep red. In large yellow / gold stylised type are the words "Live And Let Die". Underneath, in smaller type "by Ian Fleming, author of CASINO ROYALE".
| author = [[Ian Fleming]]
| cover_artist = Devised by Fleming, completed by Kenneth Lewis
| country = United Kingdom
| series = [[James Bond]]
| genre = [[Spy fiction]]
| publisher = [[Jonathan Cape]]
| release_date = 5 April 1954 (hardback)
| pages = 234
| preceded_by = [[Casino Royale (novel)|Casino Royale]]
| followed_by = [[Moonraker (novel)|Moonraker]]
}}

'''''Live and Let Die''''' is the second novel in [[Ian Fleming]]'s [[James Bond (literary character)|James Bond]] [[List of James Bond novels and short stories|series of stories]], and is set in London, the US and Jamaica. It was first published in the UK by [[Jonathan Cape]] on 5 April 1954. Fleming wrote the novel at his [[Goldeneye (estate)|Goldeneye estate]] in Jamaica before his first book, [[Casino Royale (novel)|''Casino Royale'']], was published; much of the background came from Fleming's travel in the US and knowledge of Jamaica.

The story centres on Bond's pursuit of "Mr Big", a criminal who has links to the American criminal network, the world of [[Haitian Vodou|voodoo]] and [[SMERSH (James Bond)|SMERSH]]—an arm of the Russian secret service—all of which are threats to the West. Bond becomes involved in the US through Mr Big's smuggling of 17th-century gold coins from British territories in the Caribbean. The novel deals with the themes of the ongoing East-West struggle of the [[Cold War]]—including British and American relations, Britain's position in the world, race relations and the struggle between good and evil.

As with ''Casino Royale'', ''Live and Let Die'' was broadly well received by the critics.  The initial print run of 7,500 copies quickly sold out and a second print run was ordered within the year. US sales, when the novel was released there a year later, were much slower. Following a comic-strip adaptation in 1958–59 by [[John McLusky]] in the ''[[Daily Express]]'', the novel was adapted in 1973 as the [[Live and Let Die (film)|eighth film]] in the [[Eon Productions]] Bond series and the first to star [[Roger Moore]] as Bond. Major plot elements from the novel were also incorporated into the Bond films [[For Your Eyes Only (film)|''For Your Eyes Only'']] in 1981 and ''[[Licence to Kill]]'' in 1989.

==Plot==
[[File:Morgan,Henry.jpg|thumb|right|[[Henry Morgan|Sir Henry Morgan]], whose treasure formed the key to the plot.]]
The British [[Secret Intelligence Service|Secret Service]] agent [[James Bond (literary character)|James Bond]] is sent by his superior, [[M (James Bond)|M]], to New York City to investigate "Mr Big", real name '''B'''uonaparte '''I'''gnace '''G'''allia. Bond's target is an agent of the [[Soviet Union|Soviet]] [[counterintelligence]] organisation [[SMERSH (James Bond)|SMERSH]], and an underworld [[Haitian Vodou|voodoo]] leader who is suspected of selling 17th-century gold coins in order to finance Soviet spy operations in America. These gold coins have been turning up in [[Harlem]] and Florida and are suspected of being part of a treasure that was buried in Jamaica by the pirate [[Henry Morgan|Sir Henry Morgan]].

In New York Bond meets up with his counterpart in the [[Central Intelligence Agency|CIA]], [[Felix Leiter]]. The two visit some of Mr Big's nightclubs in Harlem, but are captured. Bond is interrogated by Mr Big, who uses his fortune-telling employee, Solitaire (so named because she excludes men from her life), to determine if Bond is telling the truth. Solitaire lies to Mr Big, supporting Bond's cover story. Mr Big decides to release Bond and Leiter, and has his henchman Tee-Hee Johnson break one of Bond's fingers. On leaving, Bond kills Tee-Hee and several more of Mr Big's men, while Leiter is released with minimal physical harm by a gang member, sympathetic because of a shared appreciation of jazz.

Solitaire later leaves Mr Big and contacts Bond; the couple travel by train to [[St. Petersburg, Florida]], where they meet Leiter. While Bond and Leiter are scouting one of Mr Big's warehouses used for storing exotic fish, Solitaire is kidnapped by Mr Big's minions. Leiter later returns to the warehouse by himself, but is either captured and fed to a shark or tricked into standing on a trap door over the shark tank through which he falls; he survives, but loses an arm and a leg. Bond finds him in their safe house with a note pinned to his chest "He disagreed with something that ate him".{{sfn|Fleming|2006|p=169}} Bond then investigates the warehouse himself and discovers that Mr Big is smuggling gold coins by hiding them in the bottom of tanks holding poisonous tropical fish, which he is bringing into the US. He is attacked in the warehouse by the Robber, another of Mr Big's men; during the resultant gunfight, Bond outwits the Robber and causes him to fall into the shark tank.

Bond continues his mission in Jamaica, where he meets a local fisherman, Quarrel, and John Strangways, the head of the local MI6 station. Quarrel gives Bond training in [[scuba diving]] in the local waters. Bond swims through shark- and barracuda-infested waters to Mr Big's island and manages to plant a [[limpet mine]] on the hull of his yacht before being captured once again by Mr Big. Bond is reunited with Solitaire; the following morning Mr Big ties the couple to a line behind his yacht and plans to drag them over the shallow [[coral reef]] and into deeper water so that the sharks and barracuda that he attracts to the area with regular feedings will eat them.

Bond and Solitaire are saved when the limpet mine explodes seconds before they are dragged over the reef. Though temporarily stunned by the explosion and injured on the coral, they are protected from further harm by the reef and Bond watches as Mr Big, who survived the explosion, is killed by the sharks and barracuda. Quarrel then rescues the couple.

==Background==
Between January and March 1952 the journalist [[Ian Fleming]] wrote ''[[Casino Royale (novel)|Casino Royale]]'', his first novel, at his [[Goldeneye (estate)|Goldeneye estate]] in Jamaica.<ref name="IFP: About IF" />{{sfn|Chancellor|2005|p=4}}{{efn|''Casino Royale'' was subsequently released on 13 April 1953 in the UK as a hardback edition by publishers Jonathan Cape.{{sfn|Lycett|1996|p=244}} Sales were successful enough that his publishers, Jonathan Cape, offered Fleming a contract for three further Bond novels.{{sfn|Black|2005|p=10}}}} Fleming conducted research for ''Live and Let Die'', and completed the novel before ''Casino Royale'' was published in January 1953,{{sfn|Benson|1988|p=6}} four months before his second book was published. Fleming and his wife Ann flew to New York before taking the [[Silver Meteor]] train to St. Petersburg in Florida and then flying on to Jamaica.{{sfn|Benson|1988|p=6}} In doing so they followed the same train route Fleming had taken with his friend Ivar Bryce in July 1943, when Fleming had first visited the island.{{sfn|Parker|2014|p=1}}

Once Fleming and his wife arrived at Goldeneye, he started work on the second Bond novel.{{sfn|Benson|1988|p=4}} In May 1963 he wrote an article for ''Books and Bookmen'' magazine describing his approach to writing, in which he said: "I write for about three hours in the morning&nbsp;... and I do another hour's work between six and seven in the evening. I never correct anything and I never go back to see what I have written&nbsp;... By following my formula, you write 2,000 words a day."{{sfn|Faulks|Fleming|2009|p=320}} As he had done with ''Casino Royale'', Fleming showed the manuscript to his friend, the writer [[William Plomer]], who reacted favourably to the story, telling Fleming that "the new book held this reader like a limpet mine & the denouement was shattering".{{sfn|Lycett|1996|p=245}} On a trip to the US in May 1953 Fleming used his five-day travelling time on [[RMS Queen Elizabeth|RMS ''Queen Elizabeth'']] to correct the proofs of the novel.{{sfn|Lycett|1996|p=245}}

Fleming intended the book to have a more serious tone than his [[debut novel]], and he initially considered making the story a meditation on the nature of evil. The novel's original title, ''The Undertaker's Wind'', reflects this;{{sfn|Simpson|2002|p=36}} the undertaker's wind, which was to act as a metaphor for the story, describes one of Jamaica's winds that "blows all the bad air out of the island".{{sfn|Lycett|1996|p=236}}

The literary critic Daniel Ferreras Savoye considers the titles of Fleming's novels to have importance individually and collectively; ''Live and Let Die'', he writes, "turns an expression of collective wisdom, in this case fraternal and positive, into its exact opposite, suggesting a materialistic [[Epistemology|epistemological]] outlook, individualistic and lucid". This is in keeping with the storyline in that Bond brings order without which "the world would quickly turn into the dystopian, barbarian reality feared by [[Thomas Hobbes|[Thomas] Hobbes]] and celebrated by [[Marquis de Sade|[Marquis] de Sade]]."{{sfn|Savoye|2013|p=152}}

Although Fleming provided no dates within his novels, two writers have identified different timelines based on events and situations within the [[List of James Bond novels and short stories|novel series]] as a whole.  John Griswold and Henry Chancellor—both of whom have written books on behalf of [[Ian Fleming Publications]]—put the events of ''Live and Let Die'' in 1952; Griswold is more precise, and considers the story to have taken place in January and February that year.{{sfn|Griswold|2006|p=13}}{{sfn|Chancellor|2005|pp=98–99}}

==Development==

===Plot inspirations===
[[File:Rufous-throated Solitaire – cropped.jpg|thumb|The [[Rufous-throated solitaire]] bird provided the name for the book's main female character.]]
Much of the novel draws from Fleming's personal experiences: the opening of the book, with Bond's arrival at New York's [[John F. Kennedy International Airport|Idlewild Airport]] was inspired by Fleming's own journeys in 1941 and 1953,{{sfn|Black|2005|p=11}} and the warehouse at which Leiter is attacked by a shark was based on a similar building Fleming and his wife had visited in St. Petersburg, Florida on their recent journey.{{sfn|Parker|2014|p=150}} He also used his experiences on his two journeys on the Silver Meteor as background for the route taken by Bond and Solitaire.{{sfn|Black|2005|p=14}}

Some of Fleming's friends had their names used in the story, with Ivar Bryce giving his name to the alias used by Bond, while another friend, Tommy Leiter, found his surname being used for Felix Leiter;{{sfn|Macintyre|2008|p=93}} Ivar Bryce's middle name of Felix was used for Leiter's first name,{{sfn|Lycett|1996|p=222}} while John Fox-Strangways saw part of his surname being used for the name of the MI6 station chief in Jamaica.{{sfn|Lycett|1996|p=81}} Fleming also used the name of the local Jamaican [[Rufous-throated solitaire]] bird as the name of the book's main female character.{{sfn|Macintyre|2008|p=160}}

[[File:Kreipe Abduction Team.jpg|thumb|left|[[Patrick Leigh Fermor]] (centre); his book on voodoo was used as background by Fleming.]]
Fleming's experiences on his first scuba dive with [[Jacques Cousteau]] in 1953 provided much of the description of Bond's swim to Mr Big's boat,{{sfn|Chancellor|2005|p=43}} while the concept of  limpet-mining is probably based on the wartime activities of the elite [[Decima Flottiglia MAS|10th Light Flotilla]], a unit of Italian navy frogmen.{{sfn|Macintyre|2008|p=104}} Fleming also used, and extensively quoted, information about voodoo from his friend [[Patrick Leigh Fermor]]'s 1950 book ''The Traveller's Tree'',{{sfn|Chancellor|2005|p=43}} which had also been partly written at Goldeneye.{{sfn|Lycett|1996|p=238}}

Fleming had a long-held interest in pirates, from the novels he read as a child, through to films such as ''[[Captain Blood (1935 film)|Captain Blood]]'' (1935) with [[Errol Flynn]], which he enjoyed watching. From his Goldeneye home on Jamaica's northern shore, Fleming had visited [[Port Royal]] on the south of the island, which was once the home port of Sir Henry Morgan, all of which stimulated Fleming's interest.{{sfn|Parker|2014|pp=115–17}} For the background to Mr Big's treasure island, Fleming appropriated the details of Cabritta Island in [[Port Maria]] Bay, which was the true location of Morgan's hoard.{{sfn|Lycett|1996|p=238}}

===Characters===
Fleming builds the main character in ''Live and Let Die'' to make Bond come across as more human than in ''Casino Royale'', becoming "a much warmer, more likeable man from the opening chapter", according to the American novelist [[Raymond Benson]], who between 1997 and 2002 wrote a series of Bond novels and short stories.{{sfn|Benson|1988|p=96}} Savoye sees the introduction of a vulnerable side to Bond, identifying the agent's tears towards the end of the story as evidence of this.{{sfn|Savoye|2013|p=49}} Similarly, over the course of the book, the American character Leiter develops and also emerges as a more complete and human character and his and Bond's friendship is evident in the story.{{sfn|Benson|1988|pp=96–97}} Despite the relationship, Leiter is again subordinate to Bond. While in ''Casino Royale'' his role was to provide technical support and money to Bond, in ''Live and Let Die'' the character is secondary to Bond, and the only time he takes the initiative, he loses an arm and a leg, while Bond wins his own battle with the same opponent.{{sfn|Bennett|Woollacott|1987|p=100}} Although Fleming had initially intended to kill Leiter off in the story, his American literary agent protested, and the character was saved.{{sfn|Parker|2014|p=165}}

{{Quote box
|quote= Fleming did not use class enemies for his villains, instead relying on physical distortion or ethnic identity&nbsp;... Furthermore, in Britain foreign villains used foreign servants and employees&nbsp;... This racism reflected not only a pronounced theme of interwar adventure writing, such as the novels of [[John Buchan|[John] Buchan]], but also widespread literary culture.|source = [[Jeremy Black (historian)|Jeremy Black]], ''The Politics of James Bond''{{thinsp}}{{sfn|Black|2005|p=19}}|align = right|width = 30em|border = 1px|salign = right}}

Quarrel was Fleming's ideal concept of a black person, and the character was based on his genuine liking for Jamaicans, whom he saw as "full of goodwill and cheerfulness and humour".{{sfn|Parker|2014|p=161}} The relationship between Bond and Quarrel was based on a mutual assumption of Bond's superiority.{{sfn|Chapman|2007|p=27}}{{sfn|Parker|2014|p=163}} Fleming described the relationship as "that of a Scots laird with his head stalker; authority was unspoken and there was no room for servility".{{sfn|Fleming|2006|p=206}}

Fleming's villain was physically abnormal—as many of Bond's later adversaries were.{{sfn|Eco|2009|p=38}} Mr Big is described as being intellectually brilliant,{{sfn|Benson|1988|p=97}} with a "great football of a head, twice the normal size and very nearly round" and skin which was "grey-black, taut and shining like the face of a week-old corpse in the river".{{sfn|Fleming|2006|p=71}} For Benson, "Mr Big is only an adequate villain", with little depth to him.{{sfn|Benson|1988|p=97}} According to the literary analyst LeRoy L. Panek, in his examination of 20th century British spy novels, ''Live and Let Die'' showed a departure from the "gentleman crook" that showed in much earlier literature, as the intellectual and organisational skills of Mr Big were emphasised, rather than the behavioural.{{sfn|Panek|1981|p=213}} Within Mr Big's organisation, Panek identifies Mr Big's henchmen as "merely incompetent gunsels" when compared with Bond, who the latter can eliminate with relative ease.{{sfn|Panek|1981|pp=205–06}}

==Style==
Benson analysed Fleming's writing style and identified what he described as the "Fleming Sweep": a stylistic point that sweeps the reader from one chapter to another using 'hooks' at the end of chapters to heighten tension and pull the reader into the next:{{sfn|Benson|1988|p=85}} Benson felt that the "Fleming Sweep never achieves a more engaging rhythm and flow" than in ''Live and Let Die''.{{sfn|Benson|1988|p=95}} The writer and academic [[Kingsley Amis]]—who also subsequently wrote a Bond novel—disagrees, and thinks that the story has "less narrative sweep than most".{{sfn|Amis|1966|pp=154–55}} Parker considers the novel possibly Fleming's best, as it has a tight plot and is well paced throughout; he thinks the book "establishes the winning formula" for the stories that follow.{{sfn|Parker|2014|p=153}}

Savoye, comparing the structure of ''Live and Let Die'' with ''Casino Royale'', believes that the two books have open narratives which allow Fleming to continue with further books in the series. Savoye finds differences in the structure of the endings, with ''Live and Let Die''{{'}}s promise of future sexual encounters between Bond and Solitaire to be more credible than ''Casino Royale''{{'}}s ending, in which Bond vows to battle a super-criminal organisation.{{sfn|Savoye|2013|p=104}}

Within the novel Fleming uses elements that are "pure Gothic", according to the essayist [[Umberto Eco]].{{sfn|Eco|2009|p=49}} This includes the description of Mr Big's death by shark attack, in which Bond watches as "Half of The Big Man's left arm came out of the water. It had no hand, no wrist, no wrist watch."{{sfn|Fleming|2006|p=273}} Eco considers that this is "not just an example of macabre sarcasm; it is an emphasis on the essential by the inessential, typical of the ''{{lang|fr|école du regard}}''."{{sfn|Eco|2009|p=49}}{{efn|The term ''{{lang|fr|école du regard}}'' was originally devised by the French writer [[François Mauriac]], to describe a class of writers as an "objective school" in their descriptions.{{sfn|Hewitt|1992|pp=102 & 220}}}} Benson considers that Fleming's experiences as a journalist, and his eye for detail, add to the verisimilitude displayed in the novel.{{sfn|Benson|1988|pp=95–96}}

==Themes==
''Live and Let Die'', like other Bond novels, reflects the changing roles of Britain and America during the 1950s and the perceived threat from the Soviet Union to both nations. Unlike ''Casino Royale'', where Cold War politics revolve around British-Soviet tensions, in ''Live and Let Die'' Bond arrives in Harlem to protect America from Soviet agents working through the [[Black Power movement]].<ref name="Black (2006)" /> In the novel America was the Soviet objective and Bond comments "that New York 'must be the fattest atomic-bomb target on the whole face of the world'."<ref name="Black (2003)" />

''Live and Let Die'' also gave Fleming a chance to outline his views on what he saw as the increasing American colonisation of Jamaica—a subject that concerned both him and his neighbour [[Noël Coward]]. While the American Mr Big was unusual in appropriating an entire island, the rising number of American tourists to the islands was seen by Fleming as a threat to Jamaica; he wrote in the novel that Bond was "glad to be on his way to the soft green flanks of Jamaica and to leave behind the great hard continent of Eldollarado."{{sfn|Parker|2014|pp=99–100}}

Bond's briefing also provides an opportunity for Fleming to offer his views on race through his characters.  "M and Bond&nbsp;... offer their views on the ethnicity of crime, views that reflected ignorance, the inherited racialist prejudices of London clubland", according to the cultural historian [[Jeremy Black (historian)|Jeremy Black]].{{sfn|Black|2005|p=11}} Black also points out that "the frequency of his references and his willingness to offer racial stereotypes [was] typical of many writers of his age".{{sfn|Black|2005|p=12}} The writer [[Louise Welsh]] observes that "''Live and Let Die'' taps into the paranoia that some sectors of white society were feeling" as the civil rights movements challenged prejudice and inequality.{{sfn|Fleming|2006|p=v}} That insecurity manifested itself in opinions shared by Fleming with the intelligence industry, that the American [[National Association for the Advancement of Colored People]] was a communist front.{{sfn|Lycett|1996|p=237}} The communist threat was brought home to Jamaica with the 1952 arrest of the Jamaican politician [[Alexander Bustamante]] by the American authorities while he was on official business in [[Puerto Rico]], despite the fact that he was avowedly anti-communist. During the course of the year local Jamaican political parties had also expelled members for being communists.{{sfn|Parker|2014|pp=148–49}}

Friendship is another prominent element of ''Live and Let Die'', where the importance of male friends and allies shows through in Bond's relationships with Leiter and Quarrel.{{sfn|Benson|1988|p=96}} The more complete characters profiles in the novel clearly showed the strong relationship between Bond and Leiter. In turn this provides a strengthened motive for Bond to chase Mr Big as revenge for the shark attack on Leiter.{{sfn|Benson|1988|p=96}}

''Live and Let Die'' continues the theme Fleming examined in ''Casino Royale'', that of evil or, as Fleming's biographer, [[Andrew Lycett]], describes it, "the banality of evil".{{sfn|Lycett|1996|p=238}} Fleming uses Mr Big as the vehicle to voice opinions on evil, particularly when he tells Bond that "Mister Bond, I suffer from boredom. I am prey to what the early Christians called '[[accidie]]', the deadly lethargy that envelops those who are sated."{{sfn|Fleming|2006|pp=84–85}} This allowed Fleming to build the Bond character as a counter to the accidie, in what the writer saw as a [[Manichaean]] struggle between good and evil.{{sfn|Lycett|1996|p=238}} Benson considers evil as the main theme of the book, and highlights the discussion Bond has with [[List of James Bond allies#René Mathis|René Mathis]] of the French [[Deuxième Bureau]] in ''Casino Royale'', in which the Frenchman predicts Bond will seek out and kill the evil men of the world.{{sfn|Benson|1988|p=96}}

==Publication and reception==

===Publication history===
{{Quote box
|quote= It is an unashamed thriller and its only merit is that it makes no demands on the minds of the reader.
|source = Fleming to [[Winston Churchill]], in a letter accompanying a copy of ''Live and Let Die''{{sfn|Chancellor|2005|p=43}}
|align = right
|width = 30em
|border = 1px
|salign = right}}

''Live and Let Die'' was published in hardback by Jonathan Cape on 5 April 1954{{sfn|Lycett|1996|p=255}} and, as with ''Casino Royale'', Fleming designed the cover, which again featured the title lettering prominently.{{sfn|Chancellor|2005|p=43}} It had an initial print run of 7,500 copies  which sold out, and a reprint of 2,000 copies was soon undertaken;{{sfn|Benson|1988|p=8}}{{sfn|Upton|1987|p=6}} by the end of the first year, a total of over 9,000 copies had been sold.{{sfn|Bennett|Woollacott|1987|p=23}} In May 1954 ''Live and Let Die'' was banned in Ireland by the Irish [[Censorship in the Republic of Ireland|Censorship of Publications Board]].<ref name="Kelly (2004)" />{{efn|There is no explanation given in the Board's records as to the rationale for the ban, with the only text being "Banned".<ref name="Kelly (2004)" />}} Lycett observed that the ban helped the general publicity in other territories.{{sfn|Lycett|1996|p=255}} In October 1957 [[Pan Books]] issued a paperback version which sold 50,000 copies in the first year.{{sfn|Lindner|2009|p=17}}

''Live and Let Die'' was published in the US in January 1955 by Macmillan; there was only one major change in the book, which was that the title of the fifth chapter was changed from "Nigger Heaven" to "Seventh Avenue".{{sfn|Benson|1988|p=11}}{{efn|The chapter title remained in British print editions. Fleming's biographer, Matthew Parker, wrote that Cape retained the name "presumably assuming that their readership would recognise it as the title of [[Nigger Heaven|an anti-racist novel]] from the 1920s by [[Carl Van Vechten]] about the [[Harlem Renaissance]]."{{sfn|Parker|2014|p=157}} The chapter title still appears in modern UK editions.{{sfn|Fleming|2006|p=48}}}} Sales in the US were poor, with only 5,000 copies sold in the first year of publication.{{sfn|Lycett|1996|p=268}}

===Critical reception===
Philip Day of ''[[The Sunday Times]]'' noted "How wincingly well Mr Fleming writes",{{sfn|Lycett|1996|p=255}} while the reviewer for ''[[The Times]]'', thought that "[t]his is an ingenious affair, full of recondite knowledge and horrific spills and thrills – of slightly sadistic excitements also – though without the simple and bold design of its predecessor".<ref name="Times review (1954)" /> Elizabeth L Sturch, writing in ''[[The Times Literary Supplement]]'', observed that Fleming was "without doubt the most interesting recent recruit among thriller-writers"<ref name="Sturch (1954)" /> and that ''Live and Let Die'' "fully maintains the promise of&nbsp;... ''Casino Royale''."<ref name="Sturch (1954)"/> Tempering her praise of the book, Sturch thought that "Mr Fleming works often on the edge of flippancy, rather in the spirit of a highbrow",<ref name="Sturch (1954)"/> although overall she felt that the novel "contains passages which for sheer excitement have not been surpassed by any modern writer of this kind".<ref name="Sturch (1954)"/> The reviewer for ''[[The Daily Telegraph]]'' felt that "the book is continually exciting, whether it takes us into the heart of Harlem or describes an underwater swim in shark-infested waters; and it is more entertaining because Mr Fleming does not take it all too seriously himself".<ref name="D. Tel" />  George Malcolm Thompson, writing in ''[[The Evening Standard]]'', believed ''Live and Let Die'' to be "tense; ice-cold, sophisticated; [[Peter Cheyney]] for the carriage trade".{{sfn|Chancellor|2005|p=43}}

Writing in ''[[The New York Times]]'', [[Anthony Boucher]]—a critic described by Fleming's biographer, [[John Pearson (author)|John Pearson]], as "throughout an avid anti-Bond and an anti-Fleming man"{{sfn|Pearson|1967|p=99}}—thought that the "high-spots are all effectively described&nbsp;... but the narrative is loose and jerky".<ref name="Boucher (1955)" /> Boucher concluded that ''Live and Let Die'' was "a lurid meller contrived by mixing equal parts of [[E. Phillips Oppenheim|Oppenheim]] and Spillane".<ref name="Boucher (1955)"/>{{efn|Meller is a slang term for melodrama.{{sfn|Kroon|2014|p=414}}}} In June 1955 [[Raymond Chandler]] was visiting the poet [[Stephen Spender]] in London when he was introduced to Fleming, who subsequently sent Chandler a copy of ''Live and Let Die''. In response, Chandler wrote that Fleming was "probably the most forceful and driving writer of what I suppose still must be called thrillers in England".{{sfn|Lycett|1996|p=270}}

==Adaptations==
{{see also|James Bond (comic strip)}}

''Live and Let Die'' was adapted as a daily [[comic strip]] which was published in ''[[Daily Express|The Daily Express]]'' newspaper and syndicated around the world.{{sfn|Pfeiffer|Worrall|1998|p=70}} The adaptation ran from 15 December 1958 to 28 March 1959.{{sfn|Fleming|Gammidge|McLusky|1988|p=6}} The adaptation was written by Henry Gammidge and illustrated by [[John McLusky]], whose drawings of Bond had a resemblance to [[Sean Connery]], the actor who portrayed Bond in ''[[Dr. No (film)|Dr. No]]'' three years later.<ref name="McClusky comic" />

Before ''Live and Let Die'' had been published, the producer [[Alexander Korda]] had read a proof copy of the novel. He thought it was the most exciting story he had read for years, but was unsure whether it was suitable for a film. Nevertheless, he wanted to show the story to the directors [[David Lean]] and [[Carol Reed]] for their impressions, although nothing came of Korda's initial interest.{{sfn|Lycett|1996|p=250}}{{sfn|Chapman|2007|pp=40–41}} In 1955, following the television broadcast of an adaptation of Fleming's earlier novel'' [[Casino Royale (Climax!)|Casino Royale]]'', [[Warner Bros.]] expressed an interest in ''Live and Let Die'', and offered $500 for an [[Option (filmmaking)|option]], against $5,000 if the film was made. Fleming thought the terms insufficient and turned them down.{{sfn|Lycett|1996|p=265}}

''[[Live and Let Die (film)|Live and Let Die]]'', a film based loosely on the novel starring [[Roger Moore]] as Bond, was released in 1973, which played on the cycle of [[blaxploitation]] films produced at the time.{{sfn|Chapman|2007|p=136}} The film was directed by [[Guy Hamilton]], produced by [[Albert R. Broccoli]] and [[Harry Saltzman]], and is the eighth in the [[Eon Productions]] [[List of James Bond films|Bond series]].<ref name="BBC: Bonds" /> Some scenes from the novel were depicted in later Bond films, including the sequence of Bond and Solitaire being dragged behind Mr Big's boat, which was used in the film ''[[For Your Eyes Only (film)|For Your Eyes Only]]'',{{sfn|Barnes|Hearn|2001|p=135}} while Felix Leiter was not fed to a shark until ''[[Licence to Kill]]'', which also faithfully adapts ''Live and Let Die''{{'}}s shoot-out in the warehouse.{{sfn|Barnes|Hearn|2001|p=176}}{{sfn|Chapman|2007|p=206}}

==Notes and references==

===Notes===
{{notes}}

===References===
{{reflist|colwidth=25em|refs=

<ref name="BBC: Bonds">
{{cite web|last1=Thomas|first1=Rebecca|title=The many faces of Bond|url=http://news.bbc.co.uk/1/hi/special_report/1999/11/99/shaken_not_stirred/523370.stm|publisher=[[BBC]]|accessdate=28 March 2015|date=19 November 1999|archivedate=2 September 2015|archiveurl= http://www.webcitation.org/6bEwQftR1}}</ref>

<ref name="McClusky comic">
{{cite web |url= http://www.bbc.co.uk/dna/h2g2/A18107291 | title=The James Bond Films – 2006 onwards|publisher=BBC | accessdate=1 April 2015|archivedate=2 September 2015|archiveurl= http://www.bbc.co.uk/dna/h2g2/A18107291}}</ref>

<ref name="IFP: About IF">
{{cite web|title=Ian Fleming|url= http://www.ianfleming.com/ian-fleming/|work=Ian Fleming|publisher=Ian Fleming Publications|accessdate=2 March 2015|archivedate=2 September 2015|archiveurl= http://www.webcitation.org/6bEwXEZXv}}</ref>

<ref name="Black (2006)">
{{cite web|last=Black|first=Jeremy|title=What we can learn from James Bond|url=http://hnn.us/articles/3556.html|publisher=George Mason University|accessdate=13 July 2007|date=24 January 2006|archivedate=2 September 2015|archiveurl=http://www.webcitation.org/6bEwbHQsN}}</ref>

<ref name="Black (2003)">
{{cite journal|last=Black|first=Jeremy|title='Oh, James' – 007 as International Man of History|journal=National Interest|date=Winter 2002–03|issue=70|pages=106–12|jstor=42897447}} {{subscription}}</ref>

<ref name="Kelly (2004)">
{{cite journal|last=Kelly|first=James|title=The Operation of the Censorship of Publications Board: The Notebooks of C. J. O'Reilly, 1951–55|journal=Analecta Hibernica|year=2004|volume=38|pages=320|jstor=20519909|issn=0791-6167}} {{subscription}}</ref>

<ref name="Times review (1954)">
{{cite news|title=Professional People|newspaper=The Times|location=London|date=7 April 1954|page=10}}</ref>

<ref name="Sturch (1954)">
{{cite news|last=Sturch|first=Elizabeth|title=Progress and Decay|newspaper=The Times Literary Supplement|location=London|date=30 April 1954|page=277}}</ref>

<ref name="D. Tel">
{{cite news|title=Live and Let Die|newspaper=[[The Times]]|date=21 August 1954|location=Multiple Display Advertisements|page=9}}</ref>

<ref name="Boucher (1955)">
{{cite news|last=Boucher|first=Anthony|title=Criminals at Large|newspaper=The New York Times|location=New York, NY|page=BR17|date=10 April 1955}}</ref>
}}

===Sources===
{{refbegin|30em}}
* {{Cite book|last=Amis|first=Kingsley|title=The James Bond Dossier|year=1966|publisher=Pan Books |location=London |oclc=154139618|ref=harv}}
* {{Cite book |last1=Barnes |first1=Alan |last2=Hearn |first2=Marcus |year=2001 |title=Kiss Kiss Bang! Bang!: the Unofficial James Bond Film Companion|publisher=Batsford Books|location=London|isbn=978-0-7134-8182-2|ref=harv}}
* {{Cite book|last1=Bennett|first1=Tony|authorlink1=Tony Bennett (sociologist)|last2=Woollacott|first2=Janet|title=Bond and Beyond: The Political Career of a Popular Hero|year=1987|publisher=Routledge|location=London|isbn=978-0-416-01361-0|ref=harv}}
* {{Cite book |last=Benson |first=Raymond |title=The James Bond Bedside Companion |year=1988 |publisher=Boxtree |location=London |isbn=978-1-85283-233-9|ref=harv}}
* {{Cite book|last=Black|first=Jeremy|title=The Politics of James Bond: from Fleming's Novel to the Big Screen|url=https://books.google.com/books?id=g4-sFrU8Xw0C&lpg=PA101&dq=Clarence%20Leiter&pg=PP1#v=onepage&q&f=false|year=2005|publisher=University of Nebraska Press|location=Lincoln, NE|isbn=978-0-8032-6240-9|ref=harv}}
* {{Cite book |last= Chancellor |first= Henry |title= James Bond: The Man and His World |year=2005 |publisher=John Murray |location=London |isbn=978-0-7195-6815-2 |ref=harv}}
* {{Cite book|last=Chapman|first=James|title=Licence to Thrill: A Cultural History of the James Bond Films|year=2007|publisher=I.B. Tauris|location=New York|isbn=978-1-84511-515-9|ref=harv}}
* {{Cite book|last=Eco|first=Umberto|authorlink=Umberto Eco|contribution=The Narrative Structure of Ian Fleming|editor-last=Lindner|editor-first=Christoph|title=The James Bond Phenomenon: a Critical Reader|year=2009|location=Manchester|publisher=Manchester University Press|isbn=978-0-7190-6541-5|ref=harv}}
* {{cite book|last1=Faulks|first1=Sebastian|last2=Fleming|first2=Ian|authorlink1=Sebastian Faulks|year=2009|title=[[Devil May Care (Faulks novel)|Devil May Care]]|publisher=Penguin Books|location=London|isbn=978-0-14-103545-1|ref=harv}}
* {{cite book|last1=Fleming|first1=Ian|last2=Gammidge|first2=Henry|last3= McLusky|first3=John|title=Octopussy|year=1988|publisher=Titan Books|location=London|isbn=978-1-85286-040-0|ref=harv}}
* {{Cite book |last=Fleming|first=Ian|title=Live and Let Die|year=2006|origyear=1954|publisher=[[Penguin Books]] |location=London |isbn=978-0-14-102832-3|ref=harv}}
* {{cite book|last=Griswold|first=John|title=Ian Fleming's James Bond: Annotations And Chronologies for Ian Fleming's Bond Stories|url=https://books.google.com/books?id=uariyzldrJwC&lpg=PA2&dq=Ian%20Fleming's%20James%20Bond%3A%20Annotations%20And%20Chronologies%20For%20Ian%20Fleming's%20Bond%20Stories&pg=PP1#v=onepage&q&f=false|publisher=AuthorHouse|location=Bloomington, IN|year=2006|isbn=978-1-4259-3100-1|ref=harv}}
* {{cite book|last=Hewitt|first=Leah D.|title=Autobiographical Tightropes: Simone de Beauvoir, Nathalie Sarraute, Marguerite Duras, Monique Wittig, and Maryse Condä|url=https://books.google.com/books?id=bKSjE5ZBCooC&pg=PP1|year=1992|publisher=University of Nebraska Press|location=Lincoln, NE|isbn=978-0-8032-7258-3|ref=harv}}
*{{cite book|last=Kroon|first=Richard W.|title=A/V A to Z: An Encyclopedic Dictionary of Media, Entertainment and Other Audiovisual Terms|url=https://books.google.com/books?id=HjmNAgAAQBAJ&pg=PP1|year=2014|publisher=McFarland|location=Jefferson, NC|isbn=978-0-7864-5740-3|ref=harv}}
* {{Cite book|last=Lindner|first=Christoph |title=The James Bond Phenomenon: a Critical Reader |url= https://books.google.com/books?id=x9-1QY5boUsC&lpg=PP1&pg=PP1#v=onepage&q&f=false |publisher=Manchester University Press|location=Manchester|year=2009|isbn=978-0-7190-6541-5 |ref=harv}}
* {{Cite book |last= Lycett |first= Andrew |title= Ian Fleming |year=1996 |publisher=Phoenix|location=London |isbn= 978-1-85799-783-5 |ref=harv}}
* {{Cite book |last=Macintyre |first=Ben |title=For Your Eyes Only |year=2008 |publisher=Bloomsbury Publishing |location=London |isbn=978-0-7475-9527-4 |ref=harv}}
* {{cite book|last=Panek|first=LeRoy|title=The Special Branch: The British Spy Novel, 1890–1980|url=https://books.google.com/books?id=T7HeaP3cU_gC|year=1981|publisher=Bowling Green University Popular Press|location=Bowling Green, OH|isbn=978-0-87972-178-7|ref=harv}}
* {{Cite book|last=Parker|first=Matthew|title=Goldeneye|year=2014|publisher=Hutchinson|location=London|isbn=978-0-09-195410-9|ref=harv}}
* {{Cite book|last=Pearson|first=John|title=The Life of Ian Fleming: Creator of James Bond|year=1967|publisher=Pan Books|location=London|oclc=60318176|ref=harv}}
* {{Cite book |last1= Pfeiffer |first1= Lee |last2= Worrall |first2=Dave |title=The Essential Bond |year=1998 |publisher=Boxtree Ltd |location=London |isbn=978-0-7522-2477-0 |ref=harv}}
*{{cite book|last=Savoye|first=Daniel Ferreras|title=The Signs of James Bond: Semiotic Explorations in the World of 007|year=2013|publisher=McFarland|location=Jefferson, NC|isbn=978-0-7864-7056-3|ref=harv}}
* {{Cite book|last= Simpson |first= Paul |title= The Rough Guide to James Bond|url= https://books.google.com/books?id=BikCz7XZijEC&lpg=PA1&pg=PA1#v=onepage&q&f=false |publisher=Rough Guides|year=2002|isbn= 978-1-84353-142-5|ref=harv}}
* {{cite journal|last=Upton|first=John|title=The James Bond Books of Ian Fleming|journal=The Book and Magazine Collector|publisher=Diamond Publishing Group|location=London|date=August 1987|issue=41|ref=harv}}
{{Refend}}

==External links==
* {{wikiquote-inline|Ian Fleming#Live and Let Die|''Live and Let Die''}}
* [http://www.ianfleming.com/ Ian Fleming.com] Official website of [[Ian Fleming Publications]]
{{Wikilivres|Live and Let Die}}
* {{FadedPage|id=20151111|name=Live and Let Die}}


{{Bond books}}
{{Ian Fleming}}

[[Category:1954 British novels]]
[[Category:James Bond books]]
[[Category:Novels by Ian Fleming]]
[[Category:Live and Let Die (film)]]
[[Category:Licence to Kill]]
[[Category:For Your Eyes Only (film)]]
[[Category:Underwater adventure novels]]
[[Category:British novels adapted into films]]
[[Category:Novels set in Jamaica]]
[[Category:Jonathan Cape books]]