{{Infobox journal
| title = Proceedings of the American Mathematical Society
| cover = [[File:PAMS.jpg|150px]]
| discipline = [[Mathematics]]
| peer-reviewed = 
| abbreviation = Proc. Am. Math. Soc.
| impact = 0.840
| impact-year = 2013
| editor = [[Ken Ono]]
| website = http://www.ams.org/publications/journals/journalsframework/proc
| publisher = [[American Mathematical Society]]
| country = United States of America
| history = 1950-present
| frequency = Monthly
| formernames = 
| ISSN = 0002-9939
| eISSN = 1088-6826
| CODEN = PAMYAR
| LCCN = 51003937
| OCLC = 1480367
}}
'''''Proceedings of the American Mathematical Society''''' is a monthly mathematics journal published by the [[American Mathematical Society]].   As a requirement, all articles must be at most 15 printed pages.

The [[editor-in-chief]] is [[Ken Ono]],  Emory University, Atlanta, USA. According to the ''[[Journal Citation Reports]]'', the journal has a 2009 [[impact factor]] of 0.640.<ref>[[Journal Citation Reports|Journal Citation Reports, 2010]]</ref>

==Scope==

''Proceedings of the American Mathematical Society'' publishes articles from all areas of [[Pure mathematics|pure]] and [[Applied mathematics|applied]] mathematics, including [[topology]], [[geometry]], [[mathematical analysis|an
alysis]], [[algebra]], [[number theory]], [[combinatorics]], [[logic]], [[probability theory|probability]] and [[statistics]].

==Abstracting and indexing==
This journal is indexed in the following databases:<ref>[http://www.ams.org/publications/journals/journalsframework/aboutjams Indexing and archiving notes]. 2011. American Mathematical Society.</ref>
*[[Mathematical Reviews]] 
*[[Zentralblatt MATH]] 
*[[Science Citation Index]] 
*[[Science Citation Index Expanded]] 
*ISI Alerting Services 
*CompuMath Citation Index 
*[[Current Contents]] / Physical, Chemical & Earth Sciences.

== Other journals from the American Mathematical Society ==
* ''[[Journal of the American Mathematical Society]]''
* ''Memoirs the American Mathematical Society''
* ''[[Notices of the American Mathematical Society]]''
* ''[[Transactions of the American Mathematical Society]]''

==References==
<references/>

==External links==
* {{Official website|http://www.ams.org/proc/}}
* [http://www.jstor.org/journal/procamermathsoci ''Proceedings of the American Mathematical Society''] on [[JSTOR]]

[[Category:American Mathematical Society academic journals]]
[[Category:Mathematics journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1950]]
[[Category:1950 establishments in the United States]]