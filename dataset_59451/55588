{{Infobox journal
| title = Annals of Probability
| abbreviation = Ann. Prob.
| discipline = [[Probability theory]]
| editor = Maria Eulalia Vares
| publisher = [[Institute of Mathematical Statistics]]
| country = United states
| history = 1973–present
| frequency = Bimonthly
| impact = 1.470
| impact-year = 2010
| website = http://www.imstat.org/aop/
| link1 = http://projecteuclid.org/aop
| link1-name = Online access at Project Euclid
| ISSN = 0091-1798
| eISSN =
| JSTOR = 00911798
| LCCN = 73643472
| CODEN = APBYAE
| OCLC = 01786664
}}
The '''''Annals of Probability''''' is a peer-reviewed statistics [[Academic journal|journal]] published by the [[Institute of Mathematical Statistics]]. It was started in 1973 as a continuation in part of the ''[[Annals of Mathematical Statistics]]'', which was split into the ''[[Annals of Statistics]]'' and the ''Annals of Probability''. Articles older than 3 years are available on [[JSTOR]], and all articles since 2004 are freely available on [[arXiv]]. The [[editor-in-chief]] is Maria Eulalia Vares ([[Universidade Federal do Rio de Janeiro]]).

According to the ''[[Journal Citation Reports]]'', the journal has a 2010 [[impact factor]] of 1.470.<ref name=WoS>{{cite book |year=2012 |chapter=Annals of Probability |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.imstat.org/aop/}}

[[Category:Probability journals]]
[[Category:Bimonthly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1973]]
[[Category:Institute of Mathematical Statistics academic journals]]