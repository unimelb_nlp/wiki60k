{{Infobox former country
|native_name            = აფხაზთა სამეფო
|conventional_long_name = Kingdom of Abkhazia
|common_name            = Apkhazta sapemo
|
|continent=Europe
|status                 = [[Monarchy|Kingdom]]
|year_start             = 778
|year_end               = 1008
|
|era                    = [[Early Middle Ages]]
|event_pre              = 
|date_pre               = 
|event_start            = Established
|date_start             = 
|event1                 = Union of Abkhazia and [[Tao-Klarjeti]]
|date_event1            = 1008
|
|p1                     = Lazica
|image_p1               = [[File:Geo_lazi.gif|x30px|link=Lazica]]
|p2                     = Principality of Iberia
|flag_p2                = Kartli - drosha jvari.svg
|s1                     = Kingdom of Georgia
|flag_s1                = Sakartvelo_-_drosha.svg
|image_flag             = Flag of Kingdom of Abkhazia.svg
|flag_type              = 
|
|image_coat             = 
|coa_size               = 
|symbol                 = 
|symbol_type            = 
|image_map              = Kingdom_of_Abkhazia_1001-1008.svg
|image_map_caption      = Kingdom of Abkhazia at the peak of its might
|
|capital                = 
[[Anacopia Fortress|Anacopia]] (778-786)
[[Kutaisi]] (786-1008)
|
|national_motto         = 
|national_anthem        = 
|common_languages       = [[Georgian Language|Georgian]] 
|religion               = [[Georgian Orthodox Church]]
|currency               = 
|
|title_leader           = [[List of kings of Georgia|King]]
|leader1                = [[Leon II of Abkhazia|Leon II]] <small>(first)</small>
|year_leader1           = 767–811
|leader2                = [[Bagrat III of Georgia|Bagrat III]] <small>(last)</small>
|year_leader2           = 978–1014
|stat_pop3              = 
|footnotes              = 
|today                  = {{Collapsible list |titlestyle=font-weight:normal; background:transparent; text-align:left;|title=Countries today|{{flagu|Georgia}}|{{flagu|Russia}}|}}}}
{{History of Georgia (country)}}
{{History of Abkhazia}}
The '''Kingdom of Abkhazia''' ({{lang-ka|აფხაზთა სამეფო}}; ''Apkhazta samepo''), also known as the '''Kingdom of the Abkhazians''', refers to an early [[Middle Ages|medieval]] [[feudalism|feudal]] state in the [[Caucasus]] which lasted from the 780s until being united, through dynastic succession, with the [[Kingdom of Georgia]] in 1008.

== Historiographical conundrum ==
Writing the kingdom's primary history was dominated by [[Georgian people|Georgian]] and [[Byzantine Empire|Byzantine]] sources supported by modern epigraphic and archaeological records.

The problem of the Abkhazian Kingdom, particularly the questions of the nature of its ruling family and its ethnic composition, is a major point of controversy between modern Georgian and Abkhaz scholars. This can be largely explained by the scarcity of primary sources on these issues. Most Abkhaz historians claim the kingdom was formed as a result of the consolidation of the early [[Abkhaz people|Abkhaz tribes]] that enabled them to extend their dominance over the neighboring areas. This is objected to on the side of the Georgian historians, some of them claiming that the kingdom was completely Georgian.

Most international scholars agree that it is extremely difficult to judge the ethnic identity of the various population segments<ref>Graham Smith, Edward A Allworth, Vivien A Law et al., pages 56-58.</ref> due primarily to the fact that the terms "Abkhazia" and "Abkhazians" were used in a broad sense during this period—and for some while later—and covered, for all practical purposes, all the population of the kingdom, comprising both the Georgian (including also [[Mingrelia]]ns, [[Laz people|Laz]], and [[Svan people|Svans]] with [[Kartvelian languages|their distinct languages]] that are sisters to [[Georgian language|Georgian]]) and possible modern Abkhaz ([[Abasgoi]], [[Apsilae]], and [[Zygii]]) peoples.<ref>Graham Smith, Edward A Allworth, Vivien A Law et al., pages 56-58; ''Abkhaz'' by [[W. Barthold]] [[V. Minorsky]] in the [[Encyclopaedia of Islam]].</ref> It seems likely that a significant (if not predominant) proportion of the Georgian-speaking population, combined with a drive of the Abkhazian kings to throw off the Byzantine political and cultural dominance, resulted in Georgian replacing [[Greek language|Greek]] as the language of literacy and culture.<ref>Alexei Zverev, ''Ethnic Conflicts in the Caucasus''; Graham Smith, Edward A Allworth, Vivien A Law et al., pages 56-58; ''Abkhaz'' by W. Barthold [V. Minorsky] in the [[Encyclopaedia of Islam]]; ''The Georgian-Abkhaz State'' (summary), by George Anchabadze, in: Paul Garb, Arda Inal-Ipa, Paata Zakareishvili, editors, Aspects of the Georgian-Abkhaz Conflict: Cultural Continuity in the Context of Statebuilding, Volume 5, August 26–28, 2000.</ref>
[[File:Kingdom_of_Abkhazia_-_%E1%83%90%E1%83%A4%E1%83%AE%E1%83%90%E1%83%96%E1%83%97%E1%83%90_%E1%83%A1%E1%83%90%E1%83%9B%E1%83%94%E1%83%A4%E1%83%9D.svg|thumb|right|Kingdom of Abkhazia at its greatest extent and Caucasus in 1000.]]

== Early history ==
Abkhazia, or Abasgia of classic sources, was a [[princedom]] under [[Byzantine Empire|Byzantine]] authority. It lay chiefly along the [[Black Sea]] coast in what is now the northwestern part of the modern-day disputed [[Republic of Abkhazia]] and extended northward into the territory of today’s [[Krasnodar Krai]] of [[Russia]]. It had [[Anacopia]] as the capital. Abkhazia was ruled by a hereditary [[archon]] who effectively functioned as a Byzantine viceroy. The country was chiefly [[Christianity|Christian]] and the city of [[Pityus]] was a seat of an [[archbishop]] directly subordinated to the [[Patriarch of Constantinople]]. Another Abasgian episcopal see was that of Soteropolis.<ref>''Annuario Pontificio 2013'' (Libreria Editrice Vaticana, 2013, ISBN 978-88-209-9070-1), p. 975</ref> The [[Arab]]s, pursuing the retreating Georgian princes – brothers [[Mir of Egrisi]] and [[Archil of Kartli]] – surged into Abkhazia in 736. [[Dysentery]] and [[flood]]s, combined with a stubborn resistance offered by the archon [[Leon I of Abkhazia|Leon I]] and his [[Kartli]]an and Egrisian allies, made the invaders retreat. Leon I then married Mir’s daughter, and a successor, [[Leon II of Abkhazia|Leon II]] exploited this dynastic union to acquire [[Egrisi]] ([[Lazica]]) in the 770s. Presumably considered as a [[successor state]] of Lazica, this new polity continued to be referred to as Egrisi in some contemporary Georgian (e.g., ''The Vitae of the Georgian Kings'' by [[Leonti Mroveli]]) and [[Armenia]]n (e.g., ''The History of Armenia'' by [[John V the Historian|Hovannes Draskhanakertsi]]) chronicles.

[[File:Bagrat III of Georgia (Gelati mural).jpg|thumb|left|250px|King [[Bagrat II of Abkhazia]] was also King Bagrat III of Georgia from the House of [[Bagrationi]].]]

The successful defense against the Arabs, and new territorial gains, gave the Abkhazian princes enough power to claim more autonomy from the Byzantine Empire. Towards circa 786, Leon won his full independence with the help of the [[Khazars]]; he assumed the title of ''King of the Abkhazians'' and transferred his capital to the western Georgian city of Kutatisi (modern-day [[Kutaisi]]). According to Georgian annals, Leon subdivided his kingdom into eight duchies : Abkhazia proper, [[Sukhumi|Tskhumi]], [[Bedia (Egrisi)|Bedia]], [[Guria]], [[Racha]] and [[Takveri]], [[Svaneti]], [[Argveti]], and Kutatisi.<ref>[[Vakhushti Bagrationi]], ''The History of Egrisi, Abkhazeti or Imereti'', part 1.</ref>

The most prosperous period of the Abkhazian kingdom was between 850 and 950. In the early years of the 10th century, it stretched, according to Byzantine sources, along the Black Sea coast three hundred [[Ancient Greece|Greek]] miles, from the frontiers of the ''[[Theme (Byzantine district)|thema]]'' of [[Chaldia]] to the mouth of the river [[Nicopsis]], with the [[Greater Caucasus|Caucasus]] behind it. The increasingly expansionist tendencies of the kingdom led to the enlargement of its realm to the east. Beginning with [[George I of Abkhazia|George I]] (872/73–878/79), the Abkhazian kings controlled also [[Kartli]] (central and part of eastern [[Georgia (country)|Georgia]]), and interfered in the affairs of the Georgian and [[Armenia]]n [[Bagratuni Dynasty|Bagratid]]s. In about 908 King [[Constantine III of Abkhazia|Constantine III]] (898/99–916/17) had finally annexed a significant portion of Kartli, bringing his kingdom up to the neighborhood of Arab-controlled [[Tbilisi|Tfilisi]] (modern-day Tbilisi). Under his son, [[George II of Abkhazia|George II]] (916/17–960), the Abkhazian Kingdom reached a climax of power and prestige. For a brief period of time, [[Kakheti]] in eastern Georgia and [[Hereti]] in the Georgian-[[Caucasian Albania|Albanian]] marches also recognized the Abkhazian suzerainty. As a temporary ally of the [[Byzantine empire|Byzantines]], George II patronized the missionary activities of [[Nicholas Mystikos]] in [[Alania]].
 
George’s successors, however, were unable to retain the kingdom’s strength and integrity. During the reign of [[Leon III of Abkhazia|Leon III]] (960–969), Kakheti and Hereti emancipated themselves from the Abkhazian rule. A bitter [[civil war]] and feudal revolts which began under [[Demetrius III of Abkhazia|Demetrius III]] (969–976) led the kingdom into complete anarchy under the unfortunate king [[Theodosius III of Abkhazia|Theodosius III the Blind]] (976–978). By that time the hegemony in [[South Caucasus|Transcaucasia]] had finally passed to the Georgian [[Bagrationi Dynasty|Bagratids]] of [[Tao-Klarjeti]]. In 978, the Bagratid prince [[Bagrat III of Georgia|Bagrat]], nephew (sister’s son) of the sonless Theodosius, occupied the Abkhazian throne with the help of his adoptive father [[David III of Tao]]. In 1008, Bagrat succeeded on the death of his natural father [[Gurgen of Kartli|Gurgen]] as the "King of Kings of the Georgians". Thus, these two kingdoms unified through dynastic succession, in practice laying the foundation for the unified Georgian monarchy, officially styled then as the [[Kingdom of Georgia]]ns.

==Seljuk invasion==
The second half of the 11th century was marked by the disastrous invasion of the Seljuk Turks, who, by the end of the 1040s, succeeded in building a vast nomadic empire including most of Central Asia and Iran. In 1071, Seljuk armies destroyed the united Byzantine-Armenian and Georgian forces in the [[Battle of Manzikert]], and by 1081, all of Armenia, [[Anatolia]], [[Mesopotamia]], Syria, and most of Georgia were conquered and devastated by the Seljuks.

Only Abkhazia and the mountainous areas of [[Svaneti]]a, [[Racha]] and [[Khevi]]-[[Khevsureti]] did not acknowledge Seljuk suzerainty, serving as a relatively safe haven for numerous refugees. By the end of 1099, [[David IV of Georgia]] stopped paying tribute to the Seljuks and put most of Georgian lands except Tbilisi and Ereti under his effective control, having Abkhazia and Svanetia as his reliable rear bases. In 1105–1124, Georgian armies under King David undertook a series of successful campaigns against the Seljuk Turks and liberated not only the rest of Georgia but also Christian-populated Ghishi-Kabala area in western [[Shirvan]] and a large portion of [[Armenia]].

== Rulers ==
Most Abkhazian kings, with the exception of John and Adarnase of the Shavliani (presumably of [[Svan people|Svan]] origin), came from the dynasty which is sometimes known in modern history writing as the Leonids after the first king Leon, or Anosids, after the prince Anos from whom the royal family claimed their origin. Prince [[Cyril Toumanoff]] relates the name of Anos to the later Abkhaz noble family of Achba or [[Anchabadze]].<ref>Rapp, pages 481-484.</ref> By convention, the regnal numbers of the Abkhazian kings continue from those of the archons of Abasgia. There is also some lack of consistency about the dates of their reigns. The chronology below is given as per Toumanoff.

=== House of the Anosids (Achba/Anchabadze)===
*[[Leon II of Abkhazia|Leon II]], 767/68–811/12
*[[Theodosius II of Abkhazia|Theodosius II]], 811/12–837/38
*[[Demetrius II of Abkhazia|Demetrius II]], 837/38–872/73
*[[George I of Abkhazia|George I of Aghts’epi]], 872/73–878/79

=== House of Shavliani ===
*[[John of Abkhazia|John Shavliani]], 878/79–c. 880
*[[Adarnase of Abkhazia|Adarnase Shavliani]], c. 880–887/88

=== House of the Anosids (Achba/Anchabadze)===
*[[Bagrat I of Abkhazia|Bagrat I]], 887/88–898/99
*[[Constantine III of Abkhazia|Constantine III]], 898/99–916/17
*[[George II of Abkhazia|George II]], 916/17–960
*[[Leon III of Abkhazia|Leon III]], 960–969
*[[Demetrius III of Abkhazia|Demetrius III]], 969–976
*[[Theodosius III of Abkhazia|Theodosius III]], 976–978

=== House of Bagrationi ===
*[[Bagrat III of Georgia|Bagrat II]], 978–1014

== See also ==
*[[Divan of the Abkhazian Kings]]
*[[Principality of Abkhazia]]
*[[History of Georgia (country)|History of Georgia]]

== Notes ==
{{reflist}}

== References and further reading ==
#{{en icon}} [http://poli.vub.ac.be/publi/ContBorders/eng/ch0101.htm Alexei Zverev, Ethnic Conflicts in the Caucasus 1988-1994], in B. Coppieters (ed.), ''Contested Borders in the Caucasus'', Brussels: '''VUB'''Press, 1996
# Graham Smith, Edward A Allworth, Vivien A Law, Annette Bohr, Andrew Wilson, ''Nation-Building in the Post-Soviet Borderlands: The Politics of National Identities'', Cambridge University Press (September 10, 1998), ISBN 0-521-59968-7
#[http://www.encislam.brill.nl/data/EncIslam/S8/SIM-0148.html Encyclopaedia of Islam]
#{{en icon}}  [[Center for Citizen Peacebuilding]],  [https://web.archive.org/web/20070330115824/http://www.socsci.uci.edu/~cpb/progs/pdfs/english5.htm Aspects of the Georgian-Abkhazian Conflict]
#{{ru icon}} [http://www.vostlit.narod.ru/Texts/rus6/Wachushti/text9.htm Вахушти Багратиони. История царства грузинского. Жизнь Эгриси, Абхазети или Имерети. Ч.1]
#S. H. Rapp, ''Studies In Medieval Georgian Historiography: Early Texts And Eurasian Contexts'', Peeters Bvba (September 25, 2003) ISBN 90-429-1318-5
#{{en icon}} [https://web.archive.org/web/20070812141131/http://gseweb.harvard.edu/~t656_web/peace/Articles_Spring_2003/Gigineishvili_Levan_ConflictingNarrativesAbkhaziaGeorgia.htm Conflicting Narratives in Abkhazia and Georgia. Different Visions of the Same History and the Quest for Objectivity], an article by Levan Gigineishvili, 2003
#{{en icon}} [http://src-home.slav.hokudai.ac.jp/sympo/Proceed97/kitagawa.html The Role of Historiography in the Abkhazo-Georgian Conflict], an article by Seiichi Kitagawa, 1996
#{{en icon}} [http://www.conflicts.rem33.com/images/abkhazia/abkh_histr%202.htm History of Abkhazia. Medieval Abkhazia: 620-1221] by Andrew Andersen
#Georgiy I Mirsky, G I Mirskii, ''On Ruins of Empire: Ethnicity and Nationalism in the Former Soviet Union (Contributions in Political Science)'', Greenwood Press (January 30, 1997) ISBN 0-313-30044-5
#Ronald Grigor Suny, ''The Making of the Georgian Nation'': 2nd edition (December 1994), Indiana University Press, ISBN 0-253-20915-3, page 45
#Robert W. Thomson (translator), ''Rewriting Caucasian History: The Medieval Armenian Adaptation of the Georgian Chronicles: The Original Georgian Texts and Armenian Adaptation (Oxford Oriental Monographs)'', Oxford University Press, USA (June 27, 1996), ISBN 0-19-826373-2
#Toumanoff C., ''Chronology of the Kings of Abasgia and other Problems'' // Le Museon, 69 (1956), S. 73-90.

{{Historical states of Georgia |state=collapsed}}
{{Georgian historical regions}}
{{Georgia (country) topics}}

[[Category:Kingdom of Abkhazia| ]]
[[Category:Former monarchies of Europe|Abkhazia]]
[[Category:780s establishments in Asia]]
[[Category:1008 disestablishments in Asia]]
[[Category:States and territories established in the 8th century]]
[[Category:Former kingdoms|Abkhazia]]