{{Infobox journal
| title = Journal of Infrared, Millimeter, and Terahertz Waves
| cover = 
| editor = M. Koch 
| discipline = [[Engineering]]
| italic title = force
| former_names = International Journal of Infrared and Millimeter Waves 
| abbreviation = 
| publisher = [[Springer Science+Business Media]]
| country = 
| frequency = Monthly
| history = 1980-present
| openaccess = 
| license = 
| impact = 1.891
| impact-year = 2013
| website = http://www.springer.com/engineering/electronics/journal/10762
| link1 = 
| link1-name = 
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 637789070
| LCCN = 2009235073
| CODEN = 
| ISSN = 1866-6892
| eISSN = 1866-6906
}}
The '''''Journal of Infrared, Millimeter, and Terahertz Waves''''' is a monthly [[peer-reviewed]] [[scientific journal]] published by [[Springer Science+Business Media]]. The [[editor-in-chief|editor]] is [[Martin Koch (physicist)|Martin Koch]].<ref>[https://www.uni-marburg.de/sfb1083/members/m_koch/index_html?language_sync=1 Prof. Dr. Martin Koch — Philipps-Universität Marburg]</ref> Its publishing formats are letters and regular full papers. The journal was established in 1980 (with editor-in-chief [[Kenneth Button (physicist)|Kenneth J. Button]]) as ''International Journal of Infrared and Millimeter Waves''.<ref name="LCRef">
{{cite web
 | title = International journal of infrared and millimeter waves
 | work = Library catalog
 | publisher = [[Library of Congress]]
 | date =
 | url = https://catalog.loc.gov/vwebv/holdingsInfo?searchId=12027&recCount=25&recPointer=0&bibId=11243049
 | doi =
 | accessdate = 2012-06-21}}</ref> The journal's first 29 volumes (1980–2008) were published under the old title; beginning with volume 30 (January 2009) the journal has been published under its current title.<ref name=LCRef/>
 
== Scope ==
This journal focuses on original research pertaining to the 30 Gigahertz to 30 Terahertz [[frequency band]] of the [[electromagnetic spectrum]]. Sources, [[detectors]], and other devices that operate in this [[Bandwidth (signal processing)|frequency range]] are given topical coverage. Other subjects covered by this journal are systems, [[spectroscopy]], applications, communications, sensing, [[metrology]], and [[wave|electromagnetic wave]] and [[matter]] interactions.

==Abstracting and indexing==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Science Citation Index]]
* [[Current Contents]]/Engineering, Computing and Technology
* [[Materials Science Citation Index]]
* [[Scopus]]
* [[Inspec]]
* [[Academic OneFile]] 
* [[Astrophysics Data System]]
* [[Chemical Abstracts Service]]
* [[Earthquake Engineering Abstracts]]
* [[EI-Compendex]]
* [[Engineered Materials Abstracts]]
* [[INIS Atomindex]]
* [[ProQuest]]
}}

==References==
{{reflist}}

== External links ==
* {{Official|http://www.springer.com/engineering/electronics/journal/10762}}

[[Category:Engineering journals]]
[[Category:Physics journals]]
[[Category:English-language journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1980]]
[[Category:Springer Science+Business Media academic journals]]