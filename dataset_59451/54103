{{Use dmy dates|date=October 2012}}
{{Infobox Australian place
| type                     = suburb
| name                     = Brompton
| city                     = Adelaide
| state                    = sa
| image                    = Brompton, Adelaide - painted Stobie pole 2.jpg
| image_upright = 0.9
| caption                  = [[Stobie pole]] in Brompton
| image_alt                = 
| relief                   = 1
| latd                     = 34
| latm                     = 53
| lats                     = 49.2
| longd                    = 138
| longm                    = 34
| longs                    = 37.2
| pop                      = 2,930
| pop_year                 = {{CensusAU|2011}}
| pop_footnotes            = <ref name=ABS2011>{{Census 2011 AUS|id =SSC41186|name=Brompton (State Suburb)|accessdate=17 November 2012|quick=on}}</ref>
| density                  = 
| density_footnotes        = 
| established              = 1849
| established_footnotes    = <ref name=Manning>{{cite web|url=http://www.slsa.sa.gov.au/manning/pn/b/brompton.htm|title=Place Names of South Australia|work=The Manning Index of South Australian History|publisher=State Library of South Australia|date=|accessdate=3 June 2011}}</ref>
| postcode                 = 5007<ref name=Postcodes>{{cite web |url=http://www.postcodes-australia.com/areas/sa/adelaide/brompton |title=Brompton, South Australia (Adelaide) |author= |date= |work=Postcodes-Australia |publisher=Postcodes-Australia.com |accessdate=3 June 2011}}</ref>
| elevation                = 
| elevation_footnotes      = 
| area                     = 
| area_footnotes           = 
| timezone                 = 
| utc                      = 
| timezone-dst             = 
| utc-dst                  = 
| dist1                    = 3.8
| dir1                     = NW
| location1                = [[Adelaide city centre]]<ref name=Postcodes/>
| lga                      = [[City of Charles Sturt]]<ref name=CharlesSturt>{{cite web|url=http://www.charlessturt.sa.gov.au/webdata/resources/files/Wards_and_Council_Members_Contact_Details.pdf |title=City of Charles Sturt Wards and Council Members |author= |date= |work= |publisher=City of Charles Sturt |accessdate=1 June 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110805135335/http://www.charlessturt.sa.gov.au/webdata/resources/files/Wards_and_Council_Members_Contact_Details.pdf |archivedate=5 August 2011 |df=dmy }}</ref>
| stategov                 = [[Electoral district of Croydon (South Australia)|Croydon]] <small>''(2011)''</small><ref name=ECSA>{{cite web|url=http://www.ecsa.sa.gov.au/apps/news/?year=2010 |title=Electoral Districts - Electoral District for the 2010 Election |publisher=Electoral Commission SA |date= |accessdate=3 June 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110822191822/http://www.ecsa.sa.gov.au/apps/news/?year=2010 |archivedate=22 August 2011 |df=dmy }}</ref>
| fedgov                   = [[Division of Adelaide|Adelaide]] <small>''(2011)''</small><ref name=AEC>{{cite web|url=http://apps.aec.gov.au/eSearch/LocalitySearchResults.aspx?filter=Adelaide&filterby=Electorate&divid=179|title=Find my electorate: Adelaide|publisher=Australian Electoral Commission|date=|accessdate=3 June 2011}}</ref>
| maxtemp                  = 
| maxtemp_footnotes        = 
| mintemp                  = 
| mintemp_footnotes        = 
| rainfall                 = 
| rainfall_footnotes       = 
| near-n                   = [[Renown Park, South Australia|Renown Park]]
| near-ne                  = [[Renown Park, South Australia|Renown Park]]
| near-e                   = [[Bowden, South Australia|Bowden]]
| near-se                  = [[Bowden, South Australia|Bowden]]
| near-s                   = [[Hindmarsh, South Australia|Hindmarsh]]
| near-sw                  = [[Hindmarsh, South Australia|Hindmarsh]]
| near-w                   = [[Ridleyton, South Australia|Ridleyton]]
| near-nw                  = [[Ridleyton, South Australia|Ridleyton]]
}}
[[File:Chief Street.JPG|thumb|upright|Chief Street, Brompton]]
[[File:Housing development in Brompton.JPG|thumb|right||Housing development in Brompton]]
'''Brompton''' is an inner-northern [[Suburbs and localities (Australia)|suburb]] of [[Adelaide]], [[South Australia]] in the [[City of Charles Sturt]].

==History==
Brompton was established in June 1849 and quickly grew. By October of that year, two thirds of the formerly "bare common ground [was] covered with substantial and genteel cottages, thriving shops and wells of excellent water."<ref name=Manning/>

''Ovingham'' Post Office opened on 1 November 1879, was renamed ''Bowden'' in 1970 and ''Brompton'' in 1991.<ref name = "Post Office">{{Cite web  | last = Premier Postal History  | title = Post Office List  | publisher = Premier Postal Auctions | url = https://www.premierpostal.com/cgi-bin/wsProd.sh/Viewpocdwrapper.p?SortBy=SA&country= | accessdate = 26 May 2011}}</ref>

==Geography==
The suburb lies between [[Torrens Road, Adelaide|Torrens Road]] and the Grange/Outer Harbor railway line.<ref name=UBD>{{cite book |title=Adelaide and surrounds street directory|last= |first= |authorlink= |coauthors= |edition= 47th|year=2009 |publisher=UBD |location= |isbn=978-0-7319-2336-6 |page= |pages= |url= |accessdate=}}</ref>

==Demographics==
The {{CensusAU|2006}} by the [[Australian Bureau of Statistics]] counted 2,487 persons in Brompton on census night. Of these, 48.8% were male and 51.2% were female.<ref name="ABS2006">{{Census 2006 AUS |id =SSC41186 |name=Brompton (State Suburb) |accessdate=3 June 2011| quick=on}}</ref>

The majority of residents (62.2%) are of Australian birth, with other common census responses being [[Greece]] (6.0%), [[England]] (5.2%) and [[Vietnam]] (2.9%).<ref name=ABS2006/>

The age distribution of Brompton residents is skewed towards an older population compared to the greater Australian population. 76.3% of residents were over 25 years in 2006, compared to the Australian average of 66.5%; and 23.7% were younger than 25 years, compared to the Australian average of 33.5%.<ref name=ABS2006/>

==Politics==

===Local government===
Brompton is part of Hindmarsh Ward in the [[City of Charles Sturt]] [[Local government in Australia|local government area]], being represented in that council by Paul Alexandrides and Craig Auricht.<ref name=CharlesSturt/>

===State and federal===
Brompton lies in the state [[Electoral districts of South Australia|electoral district]] of [[Electoral district of Croydon (South Australia)|Croydon]]<ref name=ECSA/> and the federal [[Divisions of the Australian House of Representatives|electoral division]] of [[Division of Adelaide|Adelaide]].<ref name=AEC/> The suburb is represented in the [[South Australian House of Assembly]] by [[Michael Atkinson]]<ref name=ECSA/> and [[Australian House of Representatives|federally]] by [[Kate Ellis]].<ref name=AEC/>

==Community==
The local newspaper is the ''[[Weekly Times Messenger]]''. Other regional and national newspapers such as ''[[The Advertiser (Adelaide)|The Advertiser]]'' and ''[[The Australian]]'' are also available.<ref>{{cite web |url=http://www.newspapers.com.au/SA/ |title=South Australian Newspapers |author= |date= |work=Newspapers.com.au |publisher=Australia G'day |accessdate=3 June 2011}}</ref>

===Community groups===
The Bowden Brompton Community Centre is based at 19 Green Street, Brompton.<ref>{{cite web |url=http://bbcc.org.au/ |title=The Bowden Brompton Community Group Inc. |author= |date= |work=Official website |publisher=The Bowden Brompton Community Group Inc. |accessdate=12 May 2014}}</ref>

===Schools===
Bowden Brompton Community School is located on Torrens Road and Immaculate Heart of Mary School	is located on East Street.<ref>{{cite web |url=http://www.australianschoolsdirectory.com.au |title=Australian Schools Directory |author= |date= |work= |publisher=Australian Schools Directory |accessdate=19 April 2011}}</ref>

==Facilities and attractions==

===Shopping and dining===
The Hawker Street shopping and dining precinct, and the Brompton Hotel and the Excelsior Hotel are in the suburb.

===Parks===
[[File:Hindmarsh Incinerator.JPG|thumb|right|[[Hindmarsh Incinerator]], designed by Walter Burley Griffin in 1935, viewed from the northwest]]

There are several parks in Brompton, the largest of which is '''Josiah Mitton Reserve''', between Wood Avenue and Burley Griffin Boulevard.<ref name=UBD/> The reserve includes the Hindmarsh Incinerator, designed by [[Walter Burley Griffin]] in 1935.<ref>[http://www.architectsdatabase.unisa.edu.au/build_full.asp?B_ID=1108 Building Details - Architects of South Australia > Hindmarsh Incinerator] Accessed 13 May 2014.</ref> Completed in 1936, it is one of his two buildings in SA listed as among the 120 nationally significant 20th-century buildings in South Australia,<ref>[http://www.architecture.com.au/docs/default-source/act-notable-buildings/120-notable-buildings.pdf 120 notable buildings - Australian Institute of Architects] Accessed 8 May 2014.</ref> the other being the Thebarton Incinerator at [[Thebarton, South Australia|Thebarton]].

===Motorsport===
From 1949 until 1979, Brompton was the home to the world famous [[Rowley Park Speedway]] located on the corner of [[Torrens Road, Adelaide|Torrens Road]] and Coglin Street. Rowley Park was a {{convert|358|m|yd|abbr=off}} [[Dirt track racing|dirt track speedway]] built in the old Brompton Brick Pits and operated 23 meetings per season (usually October to April) on Friday nights and was capable of holding over 10,000 spectators. The speedway hosted numerous Australian and South Australian speedway championships through its history. From 1954 until 1973, the speedway was promoted by leading Adelaide identity [[Kym Bonython]] who made Rowley Park 'the' place to be in Adelaide on a Friday night during summer. Rowley Park closed on 4 April 1979 and is now the site of the Kym Bonython housing estate.

In 1965, Rowley Park was the site of Australia's first [[Demolition derby]].

==Transportation==

===Roads===
Brompton is primarily serviced by [[Torrens Road, Adelaide|Torrens Road]], which connects the suburb to [[Adelaide city centre]]. Hawker Street cuts through the centre of the suburb.<ref name=UBD/>

Many of the local streets were established in the 19th century. Consequently, roadways tend to be narrow and, with a small volume of traffic, quiet.<ref>{{cite web |url=http://www.charlessturt.sa.gov.au/webdata/resources/files/Bowden_and_Brompton_LATM_Plan_-_Amended_Final_Report_-_September_2009.pdf |title=Bowden, Brompton, Ridleyton & Ovingham Local Area Traffic Management Plan |author= |date=18 September 2009 |work= |publisher=Tonkin Consulting |accessdate=3 June 2011}}</ref>

===Public transport===
Brompton is serviced by public transport run by the [[Adelaide Metro]].<ref name=Metro>{{cite web|url=http://timetables.adelaidemetro.com.au/ttsearch.php |title=Public Transport in Adelaide |author= |date=12 January 2011 |work=Adelaide Metro official website |publisher=Dept. for Transport, Energy and Infrastructure, Public Transport Division |accessdate=3 June 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110426182058/http://timetables.adelaidemetro.com.au/ttsearch.php |archivedate=26 April 2011 |df=dmy }}</ref>

====Trains====
There is no train stop in Ridleyton but the [[Grange railway line|Grange]] and [[Outer Harbor railway line|Outer Harbor]] railway lines pass nearby. The closest station is [[Bowden railway station|Bowden]]. On the [[Gawler Central railway line|Gawler]] railway line the closest station is [[Ovingham railway station|Ovingham]].

====Buses====
The suburb is serviced by the following bus routes:<ref name=Metro/>
*250, 251, 252
*253, 254, N254

==See also==
{{Commons category|Brompton, South Australia}}
* [[List of Adelaide suburbs]]

==References==
{{reflist}}

==External links==
*{{cite web |url=http://www.charlessturt.sa.gov.au |title=City of Charles Sturt |author= |date= |work=Official website |publisher=City of Charles Sturt |accessdate=3 June 2011}}
{{Commons category|Brompton, South Australia}}

{{City of Charles Sturt suburbs}}

[[Category:Suburbs of Adelaide]]
[[Category:Populated places established in 1849]]
[[Category:1849 establishments in Australia]]