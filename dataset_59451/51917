{{Infobox journal
| title         = Journal of Japanese Studies
| cover         = [[File:Journal of Japanese Studies.gif]]
| editor        = James C. Dobbins, Janet Hunter
| discipline    = [[Japanese studies]]
| peer-reviewed = 
| language      = English
| former_names  = 
| abbreviation  = 
| publisher     = The Society for Japanese Studies
| country       = United States
| frequency     = Biannually
| history       = 1974–present
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       = http://depts.washington.edu/jjs/
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 00956848
| OCLC          = 1798633
| LCCN          = 75641024
| CODEN         = 
| ISSN          = 0095-6848
| eISSN         = 1549-4721
| boxwidth      = 
}}

'''''The Journal of Japanese Studies''''' (JJS) is the most influential journal dealing with research on Japan in the United States.  It is a multidisciplinary forum for communicating new information, new interpretations, and recent research results concerning Japan to the English-reading world.  The Journal publishes broad, exploratory articles suggesting new analyses and interpretations, substantial book reviews, and occasional symposia by Japan scholars from around the world.

JJS appears two times each year, winter and summer, with an annual total of approximately 500 pages.  It was begun in Autumn 1974 with [[Kenneth B. Pyle]] as its first editor and is now coedited by James C. Dobbins and Janet Hunter.  Housed at the [[University of Washington]], JJS is currently supported by generous grants from the [[Japan Foundation]] and the University of Washington and by endowments from the [[Kyocera Corporation]] and the [[National Endowment for the Humanities]].

''The Journal of Japanese Studies'' is published by the Society for Japanese Studies, and its contents are available online in the [[Project Muse]] and [[JSTOR]] databases.

==References==
* {{official website|http://depts.washington.edu/jjs/}}
* {{ISSN|0095-6848}} & {{ISSN search link|1549-4721}}

[[Category:Japanese studies journals]]
[[Category:Publications established in 1974]]
[[Category:Biannual journals]]
[[Category:Academic journals published by learned and professional societies]]