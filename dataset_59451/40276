{{Infobox ancient site
| name           = Comalcalco
| alternate_name = 
| image          = Comalcalco.jpg
| image_size     = 
| alt            = 
| caption        = Comalcalco Temple I
| map            = 
| map_type       = 
| map_alt        = 
| map_caption    = Location within
| map_size       = 
| relief         = 
| coordinates             = {{coord|18|16|46|N|93|12|04|W|display=inline}}
| map_dot_label  = 
| location       = Comalcalco,&nbsp;Tabasco,&nbsp;{{flag|Mexico}}
| region         = Tabasco
| built          = ca. 550 CE
| abandoned      = ca. 1000 CE
| epochs         = Late Classic
| cultures       = Maya
| event          = 
| excavations    = 1892, 1950s, 1960, 1994
| archaeologists = [[Désiré Charnay]], [[Frans Blom]], [[Oliver LaFarge]], George F. Andrews, Ponciano Salazar Ortegón, Gordon Ekholm, Román Piña Chan, Ricardo Armijo
University of Tulane, American Museum of Natural History, INAH
| architectural_styles = Late Classic Maya
| architectural_details = 
Number of temples: 9
| notes          = 
Responsible body: [[INAH]]

| designation1            = 
| designation1_offname    = 
| designation1_type       = 
| designation1_criteria   = 
| designation1_date       = 
| delisted1_date          = 
| designation1_partof     = 
| designation1_number     = 
| designation1_free1name  = 
| designation1_free1value = 
| designation1_free2name  = 
| designation1_free2value = 
| designation1_free3name  = 
| designation1_free3value = 
| designation2            = 
| designation2_offname    = 
| designation2_type       = 
| designation2_criteria   = 
| designation2_date       = 
| delisted2_date          = 
| designation2_partof     = 
| designation2_number     = 
| designation2_free1name  = 
| designation2_free1value = 
| designation2_free2name  = 
| designation2_free2value = 
| designation2_free3name  = 
| designation2_free3value = 
| precolumbian = yes <!-- non-functional tracking parameter, do not remove/change -->
}}

'''Comalcalco'''  is an ancient [[Maya civilization|Maya]]n [[archaeological site]] in the State of [[Tabasco State|Tabasco]], Mexico, adjacent to the modern city of [[Comalcalco]] and near the southern coast of the [[Gulf of Mexico]]. It is the only major Maya city built with bricks rather than limestone masonry and was the westernmost city of the Mayan civilisation. Covering an area of {{convert|7|km2|sqmi|abbrev=on}}, Comalcalco was founded in the [[Mesoamerican chronology|Late Classic]] period and may have been a satellite or colony of [[Palenque]] based on architectural similarities between the two.<ref name=INAH>INAH</ref> The city was a center of the [[Putún Maya|Chontal]] Maya people.<ref name=MM>Mundo Maya</ref>

== Etymology ==

The name is linked to the adjacent modern city of [[Comalcalco]] and derives from the [[Nahuatl]] words ''comalli'' ([[Comal (cookware)|comales]]) ''calli'' (house) and the locative ''co'', literally meaning "At the House of the Comales" or "Place of the House of the Comales". This name is a reference to the bricks of the ruined Maya site, which later people thought resembled ''comales''.<ref name=INAH/> The ancient place name associated with Comalcalco is ''Joy Chan'' {{IPA-myn|hoj tʃan|pron}}, (also spelled Hoi Chan),<ref>Biró, p. 36.</ref> which means "round sky" or "clouded sky".<ref name=MM/><ref name=AJ450>Armijo and Jiménez, p.450</ref>

== Location ==

[[File:Comalcalco location.png|thumb|Comalcalco's location in relation to other eastern Maya cities]]

Located in the [[Chontalpa]] region on an extensive alluvial plain once covered by low evergreen rainforest interspersed with mangrove swamps, Comalcalco lies{{convert|2|km|mi|abbr=on}} west of the modern city of Comalcalco on a small rise surrounded by lowlands. It is {{convert|51|km|mi|abbr=on}} from the city of [[Villahermosa]],<ref>INAFED</ref> and approximately {{convert|100|mi|km|abbr=on}} north of Palenque.<ref name=INAH/> Before modern times the important Mezcalapa River flowed {{convert|900|m|ft|abbr=on}} east of the main buildings in the site.<ref name=AJ450/><ref name=Aetal1496>Armijo et al., p.1496</ref>

== Economy ==

The city's strategic placement on the old Mezcalapa River meant that Comalcalco controlled the important trading link between [[Yucatán (state)|Yucatán]] and the Gulf Coast and the highlands of [[Chiapas]] and [[Guatemala]].<ref name=Aetal1496/> Archaeologist George F. Andrews of the University of Oregon believes that since Comalcalco was in the largest [[Theobroma cacao|cacao]] production zone in Tabasco, it must have been involved in the distribution of this commodity.<ref name=Aetal1498>Armijo et al., p.1498</ref>

=== Pottery ===

Comalcalco was also an important centre of clay figurine production and commerce between 600 and 1000 CE<ref>Gallegos, p.1051</ref> In and around the site, archaeologists have found identical clay moulds and figures, as well as tools, pit kilns and dumping grounds, indicating standardized mass production. Figurines found on [[Jaina Island]], approximately {{convert|350|km|mi|abbr=on}} away on the Yucatán Peninsula, use clay with the same chemical composition as those of Comalcalco. Unlike the funerary objects used on Jaina Island, the figurines of Comalcalco seemed to be meant for daily use. Certain representations appear more frequently in the ceremonial core (rulers, dwarfs, articulated figures), while others (women) are more common in residential areas.<ref>Gallegos, pp. 1053-1056</ref>

== History ==

The site reached its peak during the classic period in around 500 CE, although the area had been inhabited long before this date.<ref name=O98>Ochoa, p. 98</ref> The oldest item found on the site has been dated to August{{nbsp}}10, 561 CE.<ref name=AJ451>Armijo and Jiiménez, p. 451</ref>

The only known ruler of Comalcalco was ''Ox Balam'', who appears in an inscription from the [[Tortuguero (Maya site)|Tortuguero]] archaeological site. According to that text, Ox Balam was defeated by Tortuguero's Balam Ahau on December{{nbsp}}20, 649 CE.<ref>Grube and Schele, p. 3.</ref><ref>Ochoa, p.96.</ref> In later times, Comalcalco used the Emblem Glyph of ''B'aakal'' associated with Palenque and Tortuguero, presumably due to conquest by the latter polity.<ref name=AJ451/>

A tomb found in Temple II (see below) references numerous late 8th century rituals associated with rain deities. The city possibly suffered from the severe drought that may have occurred throughout the region, and may have been at least partly responsible for the [[Maya Collapse|Classical Maya collapse]].<ref name=AJ451/>

The last date found on inscriptions at the site is March{{nbsp}}7, 814,<ref name=AJ451/> although habitation continued into the post-classic period until the site was abandoned around 1000 CE.<ref name=MM/>

=== Modern history ===

[[File:Comalcalco.Désiré Charnay 1.JPG|thumb|A lithograph of Comalcalco by Désiré Charnay]]

[[Désiré Charnay]] was the first person to bring outside attention to the ruins following his 1880 visit. He reported "Thousands of pyramids of colossal dimensions." Charnay examined what would later be termed the Great Acropolis in some detail, noting the architectural affinities of the palace with that of Palenque.<ref>Charnay 1881, p. 397-399</ref>

Comalcalco native Pedro Romero was the first Mexican to report on a dig at the site. He discovered that the buildings were made of bricks in 1892.<ref name=Aetal1496/><ref name=O97/>

[[Frans Blom]] and [[Oliver LaFarge]], conducted a survey of the site in 1925 and refuted Charnay's claims about its monumental size.<ref name=VP1618>Vargas Pacheco, pp. 16-18</ref> Later, in the 1950s, Gordon Ekholm excavated other buildings and removed some bricks that are still stored in the [[American Museum of Natural History]].<ref name=Aetal1496/> Román Piña Chan was in charge of archaeological work when Comalcalco was first surveyed in detail and its structures catalogued by George F. Andrews in 1966.<ref name=Aetal1498>Armijo et al., p.1498</ref><ref name=VP1618/>

Ponciano Salazar Ortegón also worked extensively at the site beginning in 1972 until his death.<ref name=O97>Ochoa, p.97</ref><ref name=VP1618/> Later, Ricardo Armijo was in charge of excavations in the 1990s.<ref name=O97/>

A site museum was opened in 2012.<ref name=MM/>

==The Site==

=== Architecture ===

[[File:Comalcalco.Mapa de zona arqueológica.jpg|thumb|A map of Comalcalco]]

The most unusual feature of the ancient city is its use of fired clay bricks for construction as opposed to the quarried limestone common in the vast majority of Classical Maya sites. The bricks are of irregular sizes and seem to have been made by cutting longer sections of clay rather than with moulds. Many have designs and inscriptions painted, printed or etched into them. The bricks were joined by mortar made of calcinated seashells and oysters possibly gathered at the nearby Mecoacan Lagoon.<ref name=INAH/>
{{clear}}
{{Gallery
|title=Bricks from Comalcalco
|width=160
|height=170
|lines=4
|align=center
|File:Comalcalco Museo de Sitio 15.JPG|
|File:Comalcalco Museo de Sitio 5.JPG|
|File:Comalcalco_brick.jpg|The carved sides of bricks faced inward, and were not visible.|
}}

Archaeologists have identified two distinct construction stages so far.<ref name=INAH/> After conducting his 1966 survey, Andrews concluded that the lack of significant differences in architectural style between the buildings indicated that they were built within a relatively short period of time. Buildings of the first stage were made with flattened earth covered with lime, while those of the second were covered with brickwork.<ref name=Aetal1498/>

Palenque-style architecture was apparently superimposed on the local buildings, giving further credence to the theory that Comalcalco was conquered by Palenque's B'aakal kingdom and perhaps even became a successor state.<ref name=Aetal1498/> Similarities with Palenque's architecture abound and include floor plans of temples, vaulted ceilings and built-in altars. Several bricks show drawings of temples with roof combs, another feature of Palenque, though no roof combs have survived into modern times.<ref name=INAH/><ref>Armijo et al. p.1500-1501</ref>

Well executed stucco decorations, once painted in bright colours, are another characteristic of the site.<ref>Ochoa, p. 98-99</ref> As in Palenque, these refer to religious, mythological or ceremonial elements.<ref name=INAH/>

[[File:Comalcalco.El Palacio (3).jpg|thumb|A photo of the Palace, showing the brickwork.]]

===General Site Description===
To date, 432 structures have been identified at the site,<ref name=AJ451/> including temples, an administrative building, elite dwellings and functional buildings such as a ''pib'naah'' (sweat bath) and a ''popol naah'' (council chamber). These dot the ceremonial core of the site<ref name=Aetal1498/> and cover an area of {{convert|1000|m2|sqft|abbr=on}} surrounded by residential complexes, which can consist of individual mounds, groups of mounds surrounding a central "plaza" or complexes of two to five plazas. These mounds are made of flattened earth and covered with stucco. Beyond these complexes, rural dwellings were built from perishable materials on mounds of flattened earth.<ref name=INAH/>

Three great complexes forming the ceremonial site core survive: the North Plaza, the East Acropolis and the Great Acropolis. The ceremonial center displays the key features of Maya architecture: terraces with temples, palaces, platforms and plazas. Several temples have vaulted inner chambers, which allowed them to also function as tombs.<ref name=INAH/><ref name=MM/>

===North Plaza ===

[[File:Comalcalco Templo 1.JPG|thumb|Temple I]]

[[File:Comalcalco Recreación 2.JPG|400px|thumb|Digital recreation of the North Plaza from the top of Temple I. In the upper right shows the Great Acropolis and The Palace. In this square were made grandiose events of dedication and rituals activities with offerings of blood.]]
The western end of the North Plaza is '''Temple I,''' a large brick pyramid that dominates the complex. Its central stairway rises {{convert|20|m|ft|abbr=on}} from the plaza floor,<ref name=O98/> passing through ten tiers with slanted sides and narrow ledges. The three upper tiers were once decorated with stucco, and were built at a later stage than the lower seven. Atop the pyramid there was a structure with three openings leading to an entryway, from which a sanctuary with a built-in altar was entered.<ref name=INAH/>

The '''North Plaza''' itself is a rectangular enclosure aligned so that the long side runs from east to west. Along the northern and southern edges are raised terraces. Temples II, IIa and IIb sit along the northern terrace, with Temple II on the western end closest to Temple I. On the opposite side of the plaza, the southern terrace also has a temple on its western end (Temple III), followed by temples IIIa, an opening and then temple IIIb. Three small altars are placed along an east-west axis in the center of the plaza. On the eastern end are the remains of Temples IIIc and IIId. The complex has a typical Palenque layout, while the [[E-Group|grouping]] is one of the features of early classical Maya architecture.<ref name=INAH/> Temples I, II and III have been excavated, yielding some of the complex's most important tablets inscriptions and funerary bone objects.<ref name=MM/><ref name=O98/>

'''Temple II''' was reached from the plaza through a single staircase leading into a room with three entrances, and in turn an inner chamber. In 1998, the remains of a man identified as ''Ah Pakal Tan'' (alternatively spelled Aj Pakal {{Not a typo|Tahn}}) were discovered between Temples II and IIb in a funerary urn. Two factors lead archaeologists to speculate that he was an important religious personage: the similarity of objects in the burial with those used by modern Mayan shamans, and inscriptions on burial objects found with him. These associate him with religious ceremonies rather than with the events more typical of a ruler. In total, 260 glyphs appear written on conch pendants and stingray barbs, covering 14 years of Ah Pakal Tan's life. They reference self-sacrifice and bloodletting rituals and narrate events where Ah Pakal Tan was accompanied by rain deities. Only 80 of the 260 glyphs found on the inscriptions have been translated by epigraphers.<ref name=AJ451/><ref>INAH 2009</ref>

The gap on the southern terrace, in between Temples IIIa and IIIb, opens onto to the Great Acropolis.<ref name=INAH/><ref name=MM/>

===Great Acropolis ===

South of the Northern Plaza is a large artificial platform measuring {{convert|35|m|ft|abbr=on}} in height with diverse buildings on top. It was named the '''Great Acropolis''' by Gordon Ekholm.<ref name=INAH/><ref name=MM/>

[[File:Comalcalco.Templo VII de las Figuras Sedentes (2).jpg|thumb|Frieze from Temple VII]]

'''Temple VII''' is on a protruding spur of the Great Acropolis that forms its northern edge. The temple is on a small base with a north-facing central staircase. Like many temples at the site, the structure at the summit had an entryway with three openings into a sanctuary with a constructed altar. It is also known as the '''Temple of the Seated Chieftains''' because of the seated figures modeled from stucco that  can still be seen on the two upper tiers of the Temple's sloped sides.<ref name=INAH/>

[[File:Comalcalco Mascarón.jpg|thumb|Mask of Kinich Ahau in Temple VI]]

On the same spur, immediately east of Temple VII and also facing north, is '''Temple VI''', or '''Andews' Temple'''. During the first constructive stage identified at Comalcalco, the upper structure had mud walls. Later, it was replaced by two rectangular rooms made of bricks. The upper structure is the same design as that of Temples I, V and VII. In the remains of the central stairway, archaeologist Román Piña Chán discovered a mask of the sun God [[Kinich Ahau]] in stucco. The mask is still visible to visitors and has earned the temple the nickname '''Temple of the Mask'''.<ref name=INAH/><ref name=O98/>

The terrace of Temples VI, VII and their ruined neighbour, Temple VIII, is on the northern edge of the Plaza of the Great Acropolis. '''The Palace''' (Structure 1) is on a larger terrace at the eastern end of the plaza and is the centrepiece of the acropolis. The Palace could once entered through eight framed doorways which led to a {{convert|2.5|m|ft|abbr=on}} wide main gallery oriented on a north-south axis. The gallery connected with a long room on the north end, and other rooms on its south end. It was separated from another parallel eastern gallery by a wall with five openings. From this gallery, nine bays exited to the rear of the palace. Two of these bays had built-in benches or altars. The roof of the two galleries of the palace has collapsed, but its [[Corbel arch|corbel]] vault is still hinted at by some standing walls, as are window-like openings. The inner spaces of the galleries show evidence of adaptation through the placement and removal of inner diving walls.<ref name=INAH/>

At the palace's southeast exit lies the '''Sunken Patio Group''' at the southern end of the Great Acropolis terrace. On the north side are the ruins of a residential building (Structure 2). To the east was a long building whose exposed remains seem to indicate two construction phases. The south boundary is Temple IV.<ref name=INAH/>

'''Temple IV''' is also known as the '''Stucco Tomb'''. In ancient times, the central staircase leading to the sanctuary covered the entrance to a vaulted funeral chamber that was once decorated with stucco figures.<ref name=INAH/>

'''Temple V''' is in very poor condition but shares the same features as Temple IV. It had an inner tomb and a sanctuary with two spaces. It is west of the southern end of The Palace.<ref name=INAH/>

West of Temple V, '''Temple IX''' was built above the '''Tomb of the Nine Lords of the Night'''. The tomb's walls were decorated with nine stucco figures, now badly eroded.

On the western side of the Great Acropolis plaza are the overgrown remains of a [[Mesoamerican ballcourt|ballcourt]].

===East Acropolis ===

No brick buildings are immediately visible in this complex, which is a smaller version of the Great Acropolis. Like the North Plaza, it is also aligned to the cardinal directions and the main axis is very close to that of the plaza.<ref name=INAH/>

=== Burials ===

As in Palenque, funerary offerings contain jade beads, obsidian blades and inscribed manta ray barbs.

[[File:Comalcalco Urna funeraria.JPG|thumb|A funerary urn from Comalcalco]]

In late 2010, a survey of a site {{convert|2.8|km|mi|abbr=on}} miles north of the Great Acropolis led to the discovery of the largest burial ground found in the region to date. Three mounds yielded 116 sets of skeletal remains with 66 individuals found in urns showing signs of belonging to the Maya elite through features such as cranial deformation, teeth filing and tooth incrustations. Another 50 people were buried around these urns. Preliminary studies led archaeologists to conclude that the remains were approximately 1,200 years old when discovered, placing the burial to the late classic period.<ref>INAH 2011</ref>

==2012 "Doomsday scenario" claim==
In November 2011, Mark Stevenson authored an article in the [[Seattle Times]] claiming that Mexico's [[National Institute of Anthropology and History]] revealed that there may be a reference to the December 2012 "[[2012 phenomenon|doomsday]]" date on a second glyph found at their Comalcalco ruin site. The inscription is on the carved or molded face of a brick, called The "Comalcalco Brick." The brick has the same Calendar Round as the completion of the thirteenth [[Baktun|b'ak'tun]].<ref>Stephenson, Mark (November 23, 2011). [https://web.archive.org/web/20111128023239/http://seattletimes.nwsource.com/html/nationworld/2016849572_apocalypse25.html? Mexico notes 2nd Mayan reference to 2012 disaster], [[The Seattle Times]], Retrieved September 24, 2015.</ref>

[[File:Comalcalco Templo 1.JPG|250px|thumb|Temple I.]]

== Notes ==

{{reflist|colwidth=20em}}

== References ==

{{refbegin|indent=yes}}<!--BEGIN biblio format. If indent param. is used,  Please use a colon (:) instead of asterisk (*) for bullet markers in the references list -->

: {{cite book |author=Armijo Torres, Ricardo  |author2=Socorro Jiménez Álvarez| editor1-first=J.P. |editor1-last=Laporte|editor2-first=B. |editor2-last=Arroyo|editor3-first=H. |editor3-last=Mejía|title=XIX Simposio de Investigaciones Arqueológicas en Guatemala, 2005|location= Guatemala|publisher=Museo Nacional de Arqueología y Etnología|year=2006 |pages=450–454 |chapter=Ofrendas y ceremonias a la fertilidad durante el Clásico en Comalcalco, Tabasco|url=http://www.asociaciontikal.com/pdf/41_-_Armijo.05_-_Digital.pdf}}

: {{cite book |author=Armijo Torres, Ricardo  |author2=Laura Castañeda Cerecero |author3=Carlos M. Varela | editor1-first=J.P. |editor1-last=Laporte|editor2-first=B. |editor2-last=Arroyo|editor3-first=H. |editor3-last=Mejía|title=XXII Simposio de Investigaciones Arqueológicas en Guatemala, 2008|location= Guatemala|publisher=Museo Nacional de Arqueología y Etnología|year=2008 |pages=1496–1507 |chapter=La arquitectura de Comalcalco, Tabasco, a través de sus ladrillos|url=http://asociaciontikal.com/pdf/107_-_Armijo_et_al.08.pdf}}

: {{cite journal|author=Bíró, Péter   |year=2012 |title=Politics in the Western Maya Region (II): Emblem Glyphs|journal=Estudios de Cultura Maya|location=Mexico, Mexico|publisher=[[UNAM|Universidad Nacional Autónoma de México]], Instituto de Investigaciones Filológicas |volume=XXXIX|pages=31–66 |issn=0185-2574|oclc=1568280}}

: {{cite journal|author=Charnay, Désiré   |date=October 1881 |title=The Ruins of Central America. Part IX|journal=The North American Review|publisher=University of Northern Iowa|volume= 133| issue =  299 |pages=390–404|jstor = 25101006x}}

: {{cite book |author=Gallegos, Miriam Judith  | editor1-first=J.P. |editor1-last=Laporte|editor2-first=B. |editor2-last=Arroyo|editor3-first=H. |editor3-last=Mejía|title=XXII Simposio de Investigaciones Arqueológicas en Guatemala, 2008|location= Guatemala|publisher=Museo Nacional de Arqueología y Etnología|year=2009 |pages=1052–1061 |chapter=Manufactura, Iconografía y Distribución de Figurillas en Comalcalco, Tabasco|url=http://asociaciontikal.com/pdf/076_-_Gallegos.08.pdf}}

: {{cite journal|author=Grube, Nikolai   |author2=Linda Schele | date=November 1993 |title=Naranjo Altar 1 and Rituals of Death and Burial |journal=Texas notes on Precolumbian art, writing, and culture |location=Austin, Texas |publisher=Center of the History and Art of Ancient American Culture of the Art Dept. of the University of Texas at Austin |issue=54 |oclc=50475364}}

: {{cite web |author=INAFED   |url=http://www.e-local.gob.mx/work/templates/enciclo/EMM27tabasco/municipios/27005a.html |title=Enciclopedia de los Municipios y Delegaciones de México, Estado de Tabasco, Comalcalco|accessdate=2013-04-24 |publisher=Instituto Nacional para el Federalismo y el Desarrollo Municipal}} {{es icon}}

: {{cite web |author=INAH   |url=http://www.inah.gob.mx/index.php?option=com_content&view=article&id=5647 |title=Zona Arqueológica de Comalcalco |accessdate=2013-04-24 |publisher=Instituto Nacional de Arqueología e Historia}} {{es icon}}

: {{cite press release | author=INAH  | title = Estudian el Texto Maya más Largo | publisher = Instituto Nacional de Arqueología e Historia | date = December 28, 2009 | url = http://www.inah.gob.mx/boletines/8-investigaciones-y-estudios-historicos/4063-estudian-el-texto-maya-mas-largo | accessdate = April 25, 2013}}

: {{cite press release | author=INAH  | title = Localizan posible cementerio prehispánico Maya | publisher = Instituto Nacional de Arqueología e Historia | date = June 28, 2011 | url = http://www.inah.gob.mx/boletines/14-hallazgos/5107-localizan-posible-cementerio-prehispanico-maya | accessdate = April 25, 2013}}

: {{cite web| author=Mundo Maya   | title =Comalcalco the Ancient Mayan City of Clay Bricks and Oyster and Seashell Mortar | publisher = [[Secretariat of Tourism (Mexico)|Secretaría de Turismo (SECTUR)]]| url =http://www.mundomaya.travel/arqueologia/top-10/item/comalcalco | accessdate = April 25, 2013}}

:{{cite journal|author=Ochoa, Lorenzo   | title =Comalcalco Ancient City of Brick and Stucco | journal =Voices of Mexico | pages =95–99 | year =2004 | number =69 | url =http://www.revistascisan.unam.mx/Voices/pdfs/6918.pdf| location=Mexico |publisher=[[UNAM|Universidad Nacional Autónoma de México]] |ISSN=0186-9418| OCLC=4155186}}

: {{cite book |author=Romero Rivera, José Luis   |editor-first=Ernesto |editor-last=Vargas Pacheco |title=Seis Ensayos Sobre Antiguos Patrones de Asentamiento en el Área Maya|location= Mexico, Mexico|publisher=Universidad Nacional Autónoma de México, Instituto de Investigaciones Antropológicas|year=1995 |pages=15–26 |chapter=Un Estudio del Patrón de Asentamiento en Comalcalco, Tabasco|isbn=968-36-3311-0}}

{{refend}}<!-- END biblio format style -->
<div align=center>
<!-- galería de mapas -->
{| class="toc" cellpadding=0 cellspacing=2 width=800px style="float:center; margin: 0.5em 0.5em 0.5em 1em; padding: 0.5e 
|colspan=8 style="background:Peru; color:white; font-size:100%" align=center bgcolor="green"|'''Images of the Comalcalco Site Museum'''
|-
|align=center valign=center bgcolor="white"|[[File:Comalcalco Museo de Sitio 19.JPG|150px]]
|align=center valign=center bgcolor="white"|[[File:Comalcalco Museo de Sitio 30.jpg|90px]]
|align=center valign=center bgcolor="white"|[[File:Comalcalco Museo de Sitio 22.JPG|150px]]
|align=center valign=center bgcolor="white"|[[File:Comalcalco Museo de Sitio 4.JPG|90px]]
|align=center valign=center bgcolor="white"|[[File:Comalcalco.Museo de Sitio 3.jpg|150px]]
|align=center valign=center bgcolor="white"|[[File:Comalcalco Museo de Sitio 31.JPG|90px]]
|align=center valign=center bgcolor="white"|[[File:Comalcalco Museo de Sitio 21.JPG|150px]]
|-
|style="background:#e9e9e9;" align=center|Funerary offering found in the archaeological.
|style="background:#e9e9e9;" align=center|Female head elaborate in stucco.
|style="background:#e9e9e9;" align=center|Mascaron in stucco adorning the temples
|style="background:#e9e9e9;" align=center|Mascaron in stucco.
|style="background:#e9e9e9;" align=center|Mascaron in stucco adorning the temples.
|style="background:#e9e9e9;" align=center|Mayan head with skull deformation.
|style="background:#e9e9e9;" align=center|Sting ray tail, engraved, which was part of the funerary equipment of ''Aj Pakal Tahn''.
|-
|}
</div>

== External links ==
{{Commons category|Comalcalco archeological zone}}
* [http://www.inah.gob.mx/paseos/comalcalco/ Virtual museum and site tour]

{{Maya sites}}

[[Category:Maya sites]]
[[Category:Maya sites in Tabasco]]
[[Category:Former populated places in Mexico]]
[[Category:Classic period in Mesoamerica]]
[[Category:Comalcalco]]