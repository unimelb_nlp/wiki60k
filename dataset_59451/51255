The '''European Conference on Wireless Sensor Networks''' ('''EWSN''') is an annual [[academic conference]] on [[wireless sensor networks]].

Although there is no official ranking of academic conferences on wireless sensor networks, EWSN is widely regarded as the top European event in sensor networks.

== EWSN Events ==
EWSN started in year 2004:
* [http://www.cister.isep.ipp.pt/ewsn2015/ EWSN 2015], Porto, Portugal, February 9–11, 2015
* [http://www.cs.ox.ac.uk/ewsn14/ EWSN 2014], Oxford, UK, February 17–19, 2014
* [http://ewsn13.intec.ugent.be/ EWSN 2013], Ghent, Belgium, February 13–15, 2013
* [http://ewsn12.disi.unitn.it/ EWSN 2012], Trento, Italy, February 15–17, 2012
* [http://www.nes.uni-due.de/ewsn2011/home/ EWSN 2011], Bonn, Germany, February 23.25, 2011
* [http://ewsn2010.uc.pt EWSN 2010], Coimbra, Portugal, February 17–19, 2010
* [http://www.ewsn.org/ EWSN 2009], Cork, Ireland, February 11–13, 2009
* [http://www.ewsn.org EWSN 2008], Bologna, Italy, January 30–31, February 1, 2008
* [http://www.dritte.org/ewsn/ EWSN 2007], Delft, The Netherlands, January 29–31, 2007
* [http://www.ewsn.org/2006/ EWSN 2006], Zurich, Switzerland, February 13–15, 2006
* [http://ieeexplore.ieee.org/xpl/tocresult.jsp?isnumber=31391&isYear=2005 EWSN 2005], Istanbul, Turkey, January 31 - February 2, 2005
* [http://www.springerlink.com/link.asp?id=ukkcl5jlfkcn EWSN 2004], Berlin, Germany, January 19–21, 2004

== History ==
EWSN started in year 2004 and the prime motivation behind EWSN was to provide the [[European ethnic groups|European]] researchers working in [[sensor networks]] a venue to disseminate their research results. However, over the years EWSN has grown into a truly International event with participants and authors coming from all over the world.  In 2006 it was decided to silently upgrade the event from a workshop to a conference. With this change in effect the [[acronym]] (i.e. EWSN) remains the same.  Therefore, when giving a reference to EWSN 2004 to 2006 use '''European Workshop on Wireless Sensor Networks''', and when giving a reference to EWSN 2007 onwards use '''European Conference on Wireless Sensor Networks'''.

==See also==
* [[Wireless sensor network]]

== External links ==
* [http://www.informatik.uni-trier.de/~ley/db/conf/ewsn/index.html EWSN Bibliography] (from DBLP)

{{Wireless Sensor Network}}

[[Category:Wireless sensor network]]
[[Category:Computer networking conferences]]