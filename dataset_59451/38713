{{about|the former college in Briarcliff Manor, New York|the college on Long Island|Briarcliffe College|the university in [[Sioux City, Iowa]]|Briar Cliff University}}
{{good article}}
{{Infobox university
|name       = Briarcliff College
|image_name = Dow Hall of Briarcliff College (2015).png
|image_size = 250px
|image_alt  = A brick building with a circular drive
|caption    = Dow Hall within Pace University, 2015
|motto      = 
|mottoeng   = 
|established = {{Start date|1904}}
|closed     = {{End date|1977}}
|former_names = {{unbulleted list|Mrs. Dow's School for Girls|Briarcliff Junior College}}
|type       = [[Private university|Private]]
|affiliation = 
|endowment  = 
|calendar   = 
|president  = 
|faculty    = 
|staff      = 
|students   = 
|undergrad  = 
|postgrad   = 
|city       = [[Briarcliff Manor]]
|state      = [[New York (state)|New York]]
|country    = 
|campus     = {{unbulleted list|[[Suburb]]an|{{convert|37|acre|km2|1|abbr=on}}}}
{{Infobox historic site
|embed        = yes
|coordinates  = {{coord|41.138443|-73.824842|region:US-NY_type:landmark_scale:5000|display=inline,title}}
|map_dot_mark = Blue 000080 pog.svg
|map_dot_label = Briarcliff College
|locmapin     = USA Briarcliff#New York#USA
|map_caption  = Location of Briarcliff College
}}
|nickname   = 
|athletics  = 
|sports     = 
|affiliations = 
|colors     = 
|website    = 
|logo       = 
}}
'''Briarcliff College''' was a [[women's college]] in [[Briarcliff Manor, New York]]. The school was founded as Mrs. Dow's School for Girls&nbsp;in 1903 at the [[Briarcliff Lodge]]. After [[Walter W. Law]] donated land and a building for the college, it operated at its location at 235 Elm Road in Briarcliff until 1977; closing due to low enrollment and financial problems. [[Pace University]] subsequently operated it as part of its [[Pleasantville, New York|Pleasantville]] campus from 1977 to 2015. In an effort to consolidate its campuses, Pace University sold the campus in 2017 to the Research Center on Natural Conservation, a host of conferences relating to global warming and conservation.

==History==
[[File:Mary Elizabeth Dow.jpg|thumb|left|upright=0.6|Mary Elizabeth Dow]]
{{multiple images
|width=200|align=left|direction=vertical|footer=Dow Hall c. 1908
|image1=Dow Hall c. 1908 13.png|alt1=Brick building with circular drive
|image2=Dow Hall c. 1908 03.png|alt2=A large brick building
}}
<!--Private girls' preparatory school [[Miss Porter's School]] in [[Farmington, Connecticut]] was founded by education reformer [[Sarah Porter]] in 1843. She hired her former student Mary Elizabeth Dunning in 1884, who ran the school leading up to Porter's death. After her death, her nephew [[Robert Porter Keep]] renovated the school; Dow believed Keep was privately keeping some of the school's funds designated in Porter's will. Her confrontation led to her resignation in 1903.<ref name="Miss Porter's School: A History" /><ref name="Independent">{{cite web|url=http://archive.org/stream/independen79v80newy#page/n41/mode/1up|title=The Independent|publisher=Archive.org|accessdate=April 23, 2013}}</ref><ref>{{cite web|url=http://www.riverjournalonline.com/categoryblog/2474-the-ghosts-of-briarcliff-manor.html |title=The Ghosts of Briarcliff Manor |publisher=River Journal Online |accessdate=April 23, 2013}}</ref><ref name="Newsletter">{{cite journal|title=Mrs. Dow's School: A Brief History|journal=2016 Spring Newsletter|publisher=Briarcliff Manor-Scarborough Historical Society|page=7|url=http://www.briarcliffhistory.org/uploads/3/4/4/5/34455563/bmshs_newsletter-11352-final.pdf|date=June 2016|accessdate=June 12, 2016}}</ref>
She brought 140 students and 16 faculty from the school to found Mrs. Dow's School for Girls. 82,000 square feet, initially with classrooms, a library, dormitories, a gymnasium, and an infirmary. Initially a [[finishing school]], began offering college preparatory classes in 1919. Located at the highest point of Elm Road across from the south end of Birch Road, and bordered by Tuttle Road.<ref name="Newsletter"/>-->
Mrs. Dow's School for Girls was founded in 1903 at the [[Briarcliff Lodge]]; two years later, [[Walter W. Law]] gave Mary Elizabeth Dow {{convert|35|acre}} and built the [[Châteauesque]] Dow Hall ([[Harold Van Buren Magonigle]] was its architect<ref name="LodgeBook"/>). Dow retired in 1919 and Edith Cooper Hartmann began running the school with a two-year postgraduate course; the school became a [[junior college]] in 1933.<ref name="Changing Landscape"/>{{rp|page=71}} Briarcliff remained a junior college until 1957, shortly before the presidency of Charles E. Adkins and when it began awarding four-year [[bachelor's degree]]s.<ref name="Changing Landscape"/>{{rp|page=182}}<ref name="Adkins"/> The school library, which had 5,500 volumes in 1942, expanded to about 20,000 in 1960. By the time of its closing, it had about 300 students. 
 
The school prospered from 1942 to 1961 under President Clara Tead, who had a number of accomplished trustees, including [[Carl Carmer]], [[Norman Cousins]], Barrett Clark, [[Thomas K. Finletter]], [[William Zorach]], and [[Lyman Bryson]]. Tead's husband [[Ordway Tead]] served as chairman of the board of trustees. The school gradually improved its academic scope and standing, and was registered with the [[New York State Education Department|State Education Department]] and accredited by the [[Middle States Association of Colleges and Schools]] in 1944. In 1951, the Board of Regents authorized the college to grant Associate of Arts and Associate of Applied Science degrees. The following year, the [[Army Map Service]] selected the college as the only one in the country for professional training in cartography.<ref name="Changing Landscape"/>{{rp|pages=181–8}}

In 1944, Shelton House, a building across Elm Road, was purchased as a dormitory, and a classroom and office wing was dedicated in 1951. In 1955, after [[Howard Deering Johnson]] joined the board of trustees, the dormitory Howard Johnson Hall was built. From 1963, Briarcliff College rapidly expanded, constructing two dormitories, the fine arts and humanities building, the Woodward Science Building, and a 600-seat dining hall. In 1964, the college began offering the Bachelor of Arts and of Sciences degrees. The Center for Hudson Valley Archaeology was opened in 1964. Enrollment at the college jumped from around 300 to over 500 from 1960 to 1964; by 1967, enrollment was at 623, with 240 freshmen. During the [[Vietnam War]], students protested US involvement, and Adkins and trustees resigned; James E. Stewart became president. In 1969, twelve students, led by student president Edie Cullen, stole the college [[mimeograph]] machines and gave nine demands to the college. The next day, around 50 students participated in a 48-hour [[sit-in]] at Dow Hall. [[Josiah Bunting III]] became president in 1973 and Pace University and New York Medical College of Valhalla began leasing campus buildings. The college had 350 students in 1977, and students enjoyed half-empty dormitory buildings.<ref name="Changing Landscape"/>{{rp|pages=71, 181–8}}

With the growing popularity of coeducation in the 1970s, Briarcliff found itself struggling to survive. President Josiah Bunting III leaving for [[Hampden-Sydney College]] in spring 1977 contributed to the problems the college was having. Rather than continue to struggle, the college's trustees voted to sell the campus to [[Pace University]], a New York City-based institution. Rather than merge Briarcliff with Pace, the trustees attempted to reach a collaboration agreement with [[Bennett College (New York)|Bennett College]], a junior women's college in nearby [[Millbrook, New York|Millbrook]] which was also struggling with low enrollment. The plan did not work, however, and Briarcliff College was sold to Pace in April 1977 for $5.2 million (${{Formatprice|{{Inflation|US|5200000|1977}}}} in {{Inflation-year|US}}{{Inflation-fn|US}}) after both Briarcliff and Bennett entered [[bankruptcy]].<ref name="Changing Landscape"/>{{rp|pages=181–8}}<ref name="Sale"/>
{{multiple images
|total_width=440|align=right|footer=Entrance hall, dining room, and library c. 1908
|image1=Dow Hall c. 1908 08.png|alt1=Early 1900s sitting area and hall
|image2=Dow Hall c. 1908 12.png|alt2=Early 1900s formal dining room
|image3=Dow Hall c. 1908 11.png|alt3=1900s library room
}}
{| class="wikitable floatright" style="font-size:90%; width:40%; border:0; text-align:center; line-height:120%;"
! colspan="22" style="text-align:center;font-size:90%;"|Enrollment in Briarcliff College<ref name="Changing Landscape"/>{{rp|pages=183, 186}}
|-
!  style="background:#9cc; color:navy; height:17px;"| Year
! style="background:#fff; color:navy;"| 1942
! style="background:#fff; color:navy;"| 1951–52
! style="background:#fff; color:navy;"| 1960
! style="background:#fff; color:navy;"| 1964
! style="background:#fff; color:navy;"| 1967
! style="background:#fff; color:navy;"| 1977
|- style="text-align:center;"
!  style="background:#9cc; color:navy; height:17px;"| Enrollment
| style="background:#fff; color:black;"| 42
| style="background:#fff; color:black;"| 220
| style="background:#fff; color:black;"| 300
| style="background:#fff; color:black;"| 500
| style="background:#fff; color:black;"| 623
| style="background:#fff; color:black;"| 350
|}
In 1988, the [[Hastings Center]] moved to Tead Hall, the school's library; the organization later moved to the town of [[Garrison, New York|Garrison]].<ref name="Changing Landscape"/>{{rp|187–8}}<ref name="Hastings"/>

The original Mrs. Dow's School building remains as the [[co-ed]] residence hall Dow Hall at Pace University. Residents of Briarcliff Manor were initially pleased to have another educational institution at the site, although Pace illegally turned its 188-spot parking lot into an 800-spot one, and allowed an extensive number of cars to be parked on the neighboring streets. The village government and school eventually reached a compromise.<ref name="Parking"/>

Pace operated the site as part of its [[Pleasantville, New York|Pleasantville]] campus, centered on [[Choate House (New York)|Choate House]].<ref name="Campus"/> The site currently has nine buildings with a combined {{convert|330,308|sqft}}, with sizes from 13,041 to 111,915 square feet. The buildings were used for offices, student housing, dining, recreation and education. The campus' {{convert|37|acre|km2|1|abbr=on}} also includes tennis courts and ball fields.<ref name="Sale2015"/> The Pleasantville site is about {{convert|3|mi}} away from the Briarcliff College site.<ref name="Dist"/> In an effort to consolidate Pace University's [[Westchester County]] campuses into a single location, Pace University put the site up for sale in 2015.<ref name="Sale2015"/> In October 2016, the [[Briarcliff Manor-Scarborough Historical Society]] hosted an event at Dow Hall to raise awareness of the building and its history, in order to encourage its preservation.<ref name="DowEvent"/> In January 2017, Pace sold the property for $17.4 million to the Research Center on Natural Conservation, a nonprofit organization that hosts conferences relating to global warming and conservation. The China-based nonprofit also owns the nearby [[Arden (estate)|Arden estate]] and the [[New York Military Academy]].<ref name="2017sale">{{cite news|title=CBRE Reps Pace University in $17.4 Million Sale of Briarcliff Manor Campus in Westchester|publisher=CBRE Group|url=http://newyork.citybizlist.com/article/397750/cbre-reps-pace-university-in-174-million-sale-of-briarcliff-manor-campus-in-westchester|date=January 19, 2017|accessdate=February 15, 2017}}</ref>

==Classes==
{{multiple images
|width=180|align=left|direction=vertical|footer=Kitchen and laboratory c. 1910
|image1=Dow Hall c. 1910 06.png|alt1=A teaching kitchen with students
|image2=Dow Hall c. 1910 07.png|alt2=An empty student laboratory
}}
Around 1917 at Mrs. Dow's School, an art assistant taught classes in drawing, painting, and modeling. In addition to their daily tasks, the students prepared monthly compositions which would be critiqued by the school's art director [[Frank DuMond]] through a lecture.<ref name="191718pamphlet"/>{{rp|page=15}} Art history classes included that of Italian Renaissance painting and sculpture, Western European painting, and the history of Greek sculpture, architecture, and interior decoration.<ref name="191718pamphlet"/>{{rp|page=13}}

Mrs. Dow's also held lessons in [[ear training]], elementary harmony, guitar, mandolin, piano, singing, and violin. The school also held occasional informal recitals, and allowed students to attend operas and concerts in New York City. Concerts and lectures were held at the school by notable artists, including [[Daniel Gregory Mason]], [[Guiomar Novaes]], [[Leonard Borwick]], [[Percy Grainger]], [[Efrem Zimbalist]], [[Emilio de Gogorza]], the [[Flonzaley Quartet]], and the [[Kneisel Quartet]].<ref name="191718pamphlet"/>{{rp|page=15}}

Science classes at Mrs. Dow's around 1917 included Physiology, Botany, Chemistry, General Science, and Domestic Science. The latter class involved different curricula each term: [[Diet (nutrition)|dietaries]], cookery, household administration and care, food chemistry, and (advanced) cookery.<ref name="191718pamphlet"/>{{rp|page=9}} Mrs. Dow's held psychology, history of philosophy, political economy, social science, and ethics and logic classes.<ref name="191718pamphlet"/>{{rp|page=13}}

Mathematics classes around that time included Algebra, Plane Geometry, [[Solid Geometry]], [[Trigonometry]], and Arithmetic and Accounts.<ref name="191718pamphlet"/>{{rp|page=9}} Other classes included Dramatic Expression, Bible (required every Sunday), and Poetry (required every Monday).<ref name="191718pamphlet"/>{{rp|page=13}}
{{Clear}}

==Activities and clubs==
[[File:Briarcliff College lawn tea 01.png|thumb|Lawn Tea members, 1932]]
===Sports===
In 1917, Mrs. Dow's required a physical examination for each student, including a doctor's certificate ensuring their heart and lung health. The school limited its student assignments to allow two hours of outdoor exercise daily. Sports included basketball, field hockey, soccer, and tennis.<ref name="191718pamphlet"/>{{rp|page=15}}

===Lawn Tea===
Briarcliff College operated numerous clubs, including one honorary organization, called Lawn Tea. The organization planned social events for the college, and served as the official hostesses for visiting guests. It was the oldest club there. Members were chosen for their "social charm, capabilities, and poise".<ref name="1947y"/><ref name="1932y"/>

==Notable people==
[[File:Briarcliff College 1942.jpg|thumb|Briarcliff Junior College in 1942]]
===Presidents===
{{colbegin||27em}}
* Mary Elizabeth Dow (1903–1919)
* Edith Cooper Hartmann (1919-)
* Doris Flick (-1942)
* Clara Tead (1942–1960)
* Charles E. Adkins (1960–1968)<ref name="Adkins"/>
* James E. Stewart (interim)
* Thomas E. Baker (1970–1973)
* [[Josiah Bunting III]] (1973–1977)
{{colend}}

===Students===
{{colbegin||27em}}
* [[Dorothy Burgess]], a stage and motion picture actress
* [[Pamela Juan Hayes]], head of [[Convent of the Sacred Heart (Connecticut)|Convent of the Sacred Heart]]<ref name="Hayes"/>
* [[Anne Windfohr Marion]], rancher and horsebreeder from [[Fort Worth, Texas]]<ref name="Marion"/><ref name="Marion2"/>
* [[Mary Elsie Moore]], an heiress
* [[Sushma Seth]], an Indian actress
* [[P. J. Soles]], an actress
* [[Diana Walker]], a White House photographer
{{colend}}

===Teachers===
{{colbegin||27em}}
* [[Mary Cheever]], wife of [[John Cheever]]<ref name="Cheever"/>
* [[Frank DuMond]], art director<ref name="DuMont"/>
* [[David E. Mungello]], a historian
* [[Kurt Seligmann]], a painter and engraver<ref name="Seligmann"/>
{{colend}}

==See also==
{{Wikipedia books|Briarcliff Manor}}
* [[Pace University]]
* [[History of Briarcliff Manor]]
{{Portal bar|Schools|Briarcliff Manor, New York|Hudson Valley}}

==Notes==
{{Reflist|30em|refs=
<ref name="LodgeBook">{{cite book|last=Yasinsac|first=Robert|title=Images of America: Briarcliff Lodge|publisher=Arcadia Publishing|location=Charleston, South Carolina|isbn=978-0-7385-3620-0|year=2004|oclc=57480785}}</ref>
<ref name="Changing Landscape">{{cite book|last=Cheever|first=Mary|title=The Changing Landscape: A History of Briarcliff Manor-Scarborough|year=1990|publisher=Phoenix Publishing|location=West Kennebunk, Maine|isbn=0-914659-49-9|oclc=22274920|lccn=90045613|ol=1884671M}}</ref>
<ref name="Adkins">{{cite news|title=Charles E. Adkins; College President, 85|url=https://www.nytimes.com/1995/08/15/obituaries/charles-e-adkins-college-president-85.html|accessdate=September 3, 2014|newspaper=The New York Times|date=August 15, 1995}}</ref>
<ref name="Sale">{{cite news|url= http://www.time.com/time/magazine/article/0,9171,915285,00.html|title=Education: Closing Colleges|date=August 15, 1977|accessdate=June 1, 2014|publisher=Time Magazine}}</ref>
<ref name="Hastings">{{cite news|last=Melvin|first=Tessa|title=Hastings Center to Move to Briarcliff|url=https://www.nytimes.com/1986/09/28/nyregion/hastings-center-to-move-to-briarcliff.html|accessdate=September 3, 2014|newspaper=The New York Times|date=September 28, 1986}}</ref>
<ref name="Parking">{{cite news|title=It was Traffic, Not Pedigree|url=https://www.nytimes.com/1988/01/31/nyregion/l-it-was-traffic-not-pedigree-551688.html|accessdate=September 3, 2014|newspaper=The New York Times|date=January 31, 1988}}</ref>
<ref name="Campus">{{cite news|last=Brown|first=Betsy|title=Hastings Center to Move to Briarcliff|url=https://www.nytimes.com/1986/01/19/nyregion/mount-kisco-center-is-first-of-two-planned-by-pace-university.html|accessdate=September 3, 2014|newspaper=The New York Times|date=January 19, 1986}}</ref>
<ref name="Sale2015">{{cite news|last=Taliaferro|first=Lanning|title=Pace Selling Briarcliff, White Plains Campuses|newspaper=Pleasantville-Briarcliff Manor Patch|url=http://patch.com/new-york/pleasantville/pace-selling-briarcliff-white-plains-campuses|date=June 9, 2015|accessdate=June 20, 2015}}</ref>
<ref name="Dist">{{cite news|last=Feron|first=James|title=Thriving Pace U. is Expanding Again|url=https://www.nytimes.com/1982/05/09/nyregion/thriving-pace-u-is-expanding-again.html|accessdate=September 3, 2014|newspaper=The New York Times|date=May 9, 1982}}</ref>
<ref name="DowEvent">{{cite news|last=Reif|first=Carol|title=Lawn Party To Raise Awareness Of Historic Briarcliff Building|newspaper=Briarcliff Daily Voice|url=http://briarcliff.dailyvoice.com/news/lawn-party-to-raise-awareness-of-historic-briarcliff-building/684193/|date=October 5, 2016|accessdate=October 6, 2016}}</ref>
<ref name="191718pamphlet">{{cite book|title=Mrs. Dow's School (promotional pamphlet)|via=Briarcliff Manor-Scarborough Historical Society|date=1917-18}}</ref>
<ref name="1947y">{{cite book|title=Briarcliff Junior College Yearbook|page=72|via=Briarcliff Manor-Scarborough Historical Society|date=1947}}</ref>
<ref name="1932y">{{cite book|title=Briarcliff Junior College Yearbook|page=75|via=Briarcliff Manor-Scarborough Historical Society|date=1932}}</ref>
<ref name="Hayes">{{cite web|title=Administration|url=http://www.cshgreenwich.org/podium/default.aspx?t=121350|publisher=Covent of the Sacred Heart|accessdate=October 3, 2014}}</ref>
<ref name="Marion">{{cite web|url=https://www.nytimes.com/1988/05/27/style/anne-windfohr-wed-to-john-l-marion.html|title=Anne Windfohr Wed to John L. Marion|newspaper=The New York Times|date=March 27, 1988|accessdate=November 11, 2014}}</ref>
<ref name="Marion2">{{cite book|last=Rogers|first=Mary|title=Dancing Naked: Memorable Encounters with Unforgettable Texans|location=College Station, Texas|publisher=Texas A&M University Press|date=2008|url=https://books.google.com/books?id=JXUHASYaZwAC|chapter=14|accessdate=November 11, 2014}}</ref>
<ref name="Cheever">{{cite news|last=Rauch|first=Ned P.|title=Mary Cheever, John Cheever's widow, dies at 95|url=http://www.lohud.com/story/news/local/westchester/2014/04/10/mary-cheever-wife-late-john-cheever-dead/7547653/|accessdate=September 3, 2014|newspaper=The Journal News|date=April 10, 2014}}</ref>
<ref name="DuMont">{{cite book|title=A Handbook of American Private Schools|date=1920|publisher=Porter E. Sargent|location=[[Boston, Massachusetts]]|page=743|edition=6th|url=https://books.google.com/books?id=CUBAAQAAMAAJ|accessdate=February 16, 2015}}</ref>
<ref name="Seligmann">{{cite web|title=Seligmann, Kurt|url=http://www.jewishvirtuallibrary.org/jsource/judaica/ejud_0002_0018_0_17999.html|website=Jewish Virtual Library|publisher=The American-Israeli Cooperative Enterprise|accessdate=February 16, 2015}}</ref>
}}

==External links==
{{Commons category|Briarcliff College}}
* [http://www.pace.edu/about-us/directions-to-all-campuses/briarcliff-campus Pace University Briarcliff Campus]
* [http://www.mertonsociety.com/Forum/letters/lastroadtrip.html Princeton University letters about the school]
* [https://books.google.com/books?id=KdGx6Z-IEWcC Accounts by] and [https://books.google.com/books?id=LW7rSFFhJh4C accounts of] the school by [[Janet McDonald]]

{{Briarcliff Manor, New York}}
{{authority control}}

[[Category:Educational institutions established in 1904]]
[[Category:Educational institutions disestablished in 1977]]
[[Category:Defunct universities and colleges in New York]]
[[Category:Education in New York]]
[[Category:Pace University]]
[[Category:Briarcliff Manor, New York]]
[[Category:Universities and colleges in Westchester County, New York]]
[[Category:Reportedly haunted locations in New York]]
[[Category:Educational institutions disestablished in 2015]]
[[Category:University and college campuses in New York]]