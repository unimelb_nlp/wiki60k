{{featured article}}

{{Use dmy dates|date=September 2013}}
{|{{Infobox ship begin}} <!-- warships except submarines -->
|+Yugoslav monitor ''Sava''
{{Infobox ship image
|Ship image= [[File:SS Bodrog 1914.jpg|300px|alt=a black and white photograph of a shallow draught ship alongside a dock]]
|Ship caption= SMS ''Bodrog'' on the Danube river in 1914
}}
{{Infobox ship career
|Hide header=
|Ship country=[[Austria-Hungary]]
|Ship flag={{shipboxflag|Austria-Hungary|naval}}
|Ship name=''Bodrog''
|Ship namesake=[[Bodrog|Bodrog River]]
|Ship ordered=
|Ship builder=
|Ship laid down=
|Ship launched=
|Ship acquired=
|Ship commissioned=
|Ship decommissioned=
|Ship in service=2 August 1904
|Ship out of service=1918
|Ship honours=
|Ship fate=Assigned to the [[Kingdom of Yugoslavia|Kingdom of Serbs, Croats and Slovenes]] (KSCS)
|Ship notes=
}}
{{Infobox ship career
|Hide header=title
|Ship country=Kingdom of Yugoslavia
|Ship flag={{shipboxflag|Kingdom of Yugoslavia|naval}}
|Ship name=''Sava''
|Ship namesake= [[Sava|Sava River]]
|Ship acquired=15 April 1920
|Ship commissioned=
|Ship decommissioned=
|Ship in service=
|Ship out of service=
|Ship honours=
|Ship fate=Scuttled by her crew on 11/12 April 1941
|Ship notes=
}}
{{Infobox ship career
|Hide header=title
|Ship country=[[Independent State of Croatia]]
|Ship flag={{shipboxflag|Independent State of Croatia|naval}}
|Ship name=''Sava''
|Ship acquired=Raised and repaired
|Ship out of service=
|Ship struck=
|Ship reinstated=
|Ship honours=
|Ship fate=Scuttled by her crew 8/9 September 1944
|Ship status=
|Ship notes=
}}
{{Infobox ship career
|Hide header=title
|Ship country=[[Socialist Federal Republic of Yugoslavia]]
|Ship flag={{shipboxflag|Socialist Federal Republic of Yugoslavia|naval}}
|Ship name=''Sava''
|Ship acquired=Raised and repaired
|Ship out of service=
|Ship struck=
|Ship reinstated=1952
|Ship honours=
|Ship fate=Transferred to state-run company
|Ship status=Serving as [[Barge|gravel barge]] in [[Serbia]] (2014)
|Ship notes=Naval service ended in 1962
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship class=[[Temes-class river monitor|''Temes''-class river monitor]]
|Ship displacement={{convert|440|t|LT}}
|Ship length={{convert|57.7|m|ftin|abbr=on}}
|Ship beam={{convert|9.5|m|ftin|abbr=on}}
|Ship draught={{convert|1.2|m|ftin|abbr=on}}
|Ship power=*{{convert|1400|ihp|lk=in|abbr=on}}
*2 [[Yarrow Shipbuilders|Yarrow]] [[water-tube boiler]]s
|Ship propulsion=2 [[Marine steam engine|triple-expansion steam engine]]s
|Ship speed={{convert|13|kn|lk=in}}
|Ship range=
|Ship complement=86 officers and enlisted
|Ship armament=*2 × {{convert|120|mm|abbr=on}}/L35 guns (2 × 1)
*1 × {{convert|120|mm|abbr=on}}/L10 howitzer
*2 × {{convert|37|mm|abbr=on}} guns

|Ship armour=*[[Armor belt|Belt]] and [[Bulkhead (partition)|bulkhead]]s: {{convert|40|mm|abbr=on}}
*[[Deck (ship)|Deck]]: {{convert|25|mm|abbr=on}}
*[[Conning tower]]: {{convert|75|mm|abbr=on}}
*[[Gun turret]]s: {{convert|40|mm|abbr=on}}

}}
|}
The '''Yugoslav monitor ''Sava''''' was a [[Temes-class river monitor|''Temes''-class]] [[river monitor]] built for the [[Austro-Hungarian Navy]] as '''SMS ''Bodrog'''''. She fired the first shots of [[World War I]] on the night of 28 July 1914, when she and two other monitors shelled [[Kingdom of Serbia|Serbian]] defences near [[Belgrade]]. She was part of the [[Danube Flotilla (Austria-Hungary)|Danube Flotilla]], and fought the [[Serbian Army|Serbian]] and [[Romanian Land Forces#World War I|Romanian]] armies from Belgrade to the [[Danube Delta|mouth of the Danube]]. In the closing stages of the war, she was the last monitor to withdraw towards [[Budapest]], but was captured by the Serbs when she grounded on a sandbank downstream from Belgrade. After the war, she was transferred to the newly created [[Kingdom of Yugoslavia|Kingdom of Serbs, Croats and Slovenes]] (later Yugoslavia), and renamed ''Sava''. She remained in service throughout the interwar period, although budget restrictions meant she was not always in full commission.

During the [[Nazi Germany|German]]-led [[Axis Powers|Axis]] [[invasion of Yugoslavia]] in April 1941, ''Sava'' served with the 1st Monitor Division. Along with her fellow monitor {{ship|Yugoslav monitor|Vardar||2}}, she laid mines in the [[Danube]] near the [[Kingdom of Romania|Romanian]] border during the first few days of the invasion. The two monitors fought off several attacks by the ''[[Luftwaffe]]'', but were forced to withdraw to Belgrade. Due to high river levels and low bridges, navigation was difficult, and ''Sava'' was scuttled on 11 April. Some of her crew tried to escape cross-country towards the southern [[Adriatic Sea|Adriatic]] coast, but all were captured prior to the Yugoslav surrender. The vessel was later raised by the [[Navy of the Independent State of Croatia|navy]] of the Axis [[puppet state]] known as the [[Independent State of Croatia]] and continued to serve as ''Sava'' until the night of 8 September 1944 when she was again scuttled.

Following World War II, ''Sava'' was raised once again, and was refurbished to serve in the [[Yugoslav Navy]] from 1952 to 1962. She was then transferred to a state-owned company that was eventually privatised. {{as of|2014}}, ''Sava'' was still in service as a [[Barge|gravel barge]]. In 2005, the [[government of Serbia]] granted her limited heritage protection after citizens demanded that she be preserved as a floating museum, but little else has been done to restore her.

==Description and construction==
A [[Temes-class river monitor|''Temes''-class]] [[river monitor]], the ship was built for the [[Austro-Hungarian Navy]] by [[Ganz Works|H. Schönichen]], and designed by Austrian naval architect Josef Thiel. Originally named SMS ''Bodrog'', she was laid down at [[Újpest|Neupest]] on 14 February 1903.{{sfn|Pawlik|Christ|Winkler|1989|p=60}} Like her [[sister ship]] {{SMS|Temes}}, she had an [[Length overall|overall length]] of {{convert|57.7|m|ftin|abbr=on}},{{refn|According to Pawlik, Christ and Winkler, her length overall was {{convert|56.2|m|ftin|abbr=on}}.{{sfn|Pawlik|Christ|Winkler|1989|p=60}}|group = lower-alpha}} a [[Beam (nautical)|beam]] of {{convert|9.5|m|ftin|abbr=on}}, and a normal [[Draft (hull)|draught]] of {{convert|1.2|m|ftin|abbr=on}}. Her [[Displacement (ship)#Standard displacement|standard displacement]] was {{convert|440|t|LT}}, and her crew consisted of 86 officers and enlisted men.{{refn|According to Pawlik, Christ and Winkler, her crew totalled only 77 officers and men.{{sfn|Pawlik|Christ|Winkler|1989|p=60}}|group = lower-alpha}} ''Bodrog'' had two [[Marine steam engine|triple-expansion steam engine]]s, each driving a single propeller shaft. Steam was provided by two [[Yarrow boiler|Yarrow water-tube boiler]]s, and her engines were rated at {{convert|1400|ihp|lk=in}}. As designed, she had a maximum speed of {{convert|13|kn|lk=in}},{{sfn|Greger|1976|p=141}} and carried {{convert|62|t|LT}} of coal.{{sfn|Jane's Information Group|1989|p=315}}

''Bodrog'' was armed with two {{convert|120|mm|abbr=on}}L/35{{refn|L/35 denotes the length of the gun. In this case, the L/35 gun is calibre, meaning that the gun was 35 times as long as the diameter of its bore.|group = lower-alpha}} guns in single [[gun turret]]s, a single {{convert|120|mm|abbr=on}}L/10 [[howitzer]] in a [[Pivot gun|central pivot mount]], and two {{convert|37|mm|abbr=on}} guns.{{sfn|Pawlik|Christ|Winkler|1989|p=60}} The maximum range of her [[Škoda Works|Škoda]] 120&nbsp;mm guns was {{convert|10|km}}, and her howitzer could fire its {{convert|20|kg|abbr=on}} shells a maximum of {{convert|6.2|km|abbr=on}}.{{sfn|Greger|1976|p=10}} Her armour consisted of [[Armor belt|belt]], [[Bulkhead (partition)|bulkhead]]s and gun turrets {{convert|40|mm|abbr=on}} thick, and [[Deck (ship)|deck]] armour {{convert|25|mm|abbr=on}} thick. The armour on her [[conning tower]] was {{convert|75|mm|abbr=on}} thick.{{refn|According to Pawlik, Christ and Winkler, her gun turrets also had armour {{convert|75|mm|abbr=on}} thick.{{sfn|Pawlik|Christ|Winkler|1989|p=60}}|group = lower-alpha}} ''Bodrog'' was [[Ceremonial ship launching|launched]] on 12 April 1904, [[Ship commissioning|commissioned]] on 2 August 1904,{{sfn|Pawlik|Christ|Winkler|1989|p=60}} and completed on 10 November 1904.{{sfn|Greger|1976|p=141}}

==Career==

===World War I===

====Serbian campaign====
''Bodrog'' was part of the [[Danube Flotilla (Austria-Hungary)|Danube Flotilla]], and at the start of [[World War I]] she was based in [[Zemun]], just upstream from [[Belgrade]] on the [[Danube]],{{sfn|Halpern|2012|pp=261–262}} under the command of ''[[Linienschiffsleutnant]]''{{refn|Equivalent to a Austro-Hungarian Army ''Hauptman'' ([[Captain (armed forces)|captain]]).{{sfn|Deak|1990|loc=Introduction}}|group = lower-alpha}} (LSL) Paul Ekl.{{sfn|Pawlik|Christ|Winkler|1989|p=60}} She shared the base with three other monitors and three [[patrol boats]].{{sfn|Halpern|2012|pp=261–262}} Austria-Hungary declared war on [[Kingdom of Serbia|Serbia]] on 28 July 1914, and that night ''Bodrog'' and two other monitors fired the first shots of the war against Serb fortifications on the Zemun–Belgrade railway bridge over the [[Sava]] river and on [[Topčidersko Brdo|Topčider Hill]]. The Serbs were outgunned by the monitors, and by August began to receive assistance from the Russians. This support included the supply and emplacement of naval guns and the establishment of river obstacles and [[Naval mine|mines]].{{sfn|Halpern|2012|pp=263–265}} On 8 September, the Austro-Hungarian base at Zemun was evacuated in the face of a Serbian [[counterattack]].{{sfn|Halpern|2012|p=263}} ''Bodrog'' and the [[minesweeper]] ''Andor'' conducted a deception operation towards [[Pančevo]] on 19 September, and six days later, ''Bodrog'' bombarded Serb positions on the bank of the Sava near Belgrade. On 28 September, she rendezvoused with the monitor {{SMS|Szamos}} at [[Stari Banovci|Banovci]], and the following day the two monitors targeted the [[Belgrade Fortress]] and conducted a reconnaissance of Zemun. On 1 October, ''Bodrog'' sailed to [[Budapest]], where she was placed in [[dry dock]] for two weeks. She returned to the flotilla on 15 October.{{sfn|Pawlik|Christ|Winkler|1989|p=60}} By November, French artillery support had arrived in Belgrade, endangering the monitor's anchorage,{{sfn|Halpern|2012|p=265}} and on 12 November, Ekl was replaced by LSL Olaf Wulff. The stalemate continued until the following month, when the Serbs evacuated Belgrade in the face of an Austro-Hungarian assault.{{sfn|Halpern|2012|pp=265–266}} On 1 December, ''Bodrog'' and the newly commissioned monitor {{SMS|Enns}} engaged the retreating Serbs.{{sfn|Pawlik|Christ|Winkler|1989|p=60}} After less than two weeks, the Austro-Hungarians retreated from Belgrade, and it was soon recaptured by the Serbs with Russian and French support. ''Bodrog'' continued in action against Serbia and her allies at Belgrade until December, when her base was withdrawn to [[Petrovaradin]], near [[Novi Sad]], for the winter.{{sfn|Halpern|2012|pp=265–266}}

The Germans and Austro-Hungarians wanted to transport munitions down the Danube to the [[Ottoman Empire]], so on 24 December 1914, ''Bodrog'' and the minesweeper ''Almos'' escorted the [[Steamboat|steamer]] ''Trinitas'' loaded with [[Ammunition|munitions]], the [[patrol boat]] ''b'' and two [[Tugboat|tugs]] from Zemun past Belgrade towards the [[Iron Gates]] gorge on the Serbian–Romanian border.{{sfn|Pawlik|Christ|Winkler|1989|p=60}}{{sfn|Halpern|2012|p=267}} The convoy ran the gauntlet of the Belgrade defences unharmed, but when it reached [[Smederevo]] it received information that the Russians had established a minefield and log barrier just south of the Iron Gates. It turned back under heavy fire, and withdrew as far as Pančevo without serious damage to any vessel. ''Bodrog'' returned to base, and the monitor {{SMS|Inn}} was sent to guard the munitions and escort the convoy back to Petrovaradin.{{sfn|Halpern|2012|p=267}} In January 1915, British artillery arrived in Belgrade, further bolstering its defences,{{sfn|Halpern|2012|p=266}} and ''Bodrog'' spent the first months of the year at Zemun. On 23 February, LSL Kosimus Böhm took command. On 1 March, ''Bodrog'' and several other vessels including the monitor {{SMS|Körös}} were relocated to Petrovaradin.{{sfn|Pawlik|Christ|Winkler|1989|p=60}} After the commencement of the [[Gallipoli campaign]], munitions supply to the Ottomans became critical, so another attempt was planned. On 30 March, the steamer ''Belgrad'' left Zemun, escorted by ''Bodrog'' and ''Enns''. The convoy was undetected as it sailed past Belgrade at night during a storm, but after the monitors returned to base, ''Belgrad'' struck a mine near [[Vinča]], and after coming under heavy artillery fire, exploded near [[Ritopek]].{{sfn|Halpern|2012|p=267}} On 22 April 1915, a British [[Picket (military)|picket]] boat that had been brought overland by rail from [[Thessaloniki|Salonika]] was used to attack the Danube Flotilla anchorage at Zemun, firing two torpedoes without success.{{sfn|Halpern|2012|pp=270–271}}

In September 1915, the Central Powers were joined by [[Kingdom of Bulgaria|Bulgaria]], and the Serbian Army soon faced an overwhelming Austro-Hungarian, German and Bulgarian ground invasion. In early October, the Austro-Hungarian 3rd Army attacked Belgrade, and ''Bodrog'', along with the majority of the flotilla, was heavily engaged in support of crossings near the Belgrade Fortress and the island of [[Ada Ciganlija]].{{sfn|Halpern|2012|p=272}}

[[File:Great War Island panorama.jpg|1000px|thumb|centre|View from the Belgrade Fortress over the [[Great War Island|Grosser Krieg Island]]. ''Bodrog'' supported the October 1915 crossings of the Danube near the fortress|alt=a colour photograph of a large island in the middle of a river, with a city in the background]]

====Romanian campaign====
Following the capture of Belgrade on 11 October and the initial clearance of mines and other obstacles, the flotilla sailed downstream to [[Orșova]] near the Hungarian–Romanian border and waited for the lower Danube to be [[Minesweeper|swept for mines]]. Commencing on 30 October 1915, they escorted a series of munitions convoys down the Danube to [[Lom, Bulgaria|Lom]] where the munitions were transferred to the Bulgarian railway system for shipment to the Ottoman Empire.{{sfn|Halpern|2012|p=274}}

In November 1915, ''Bodrog'' and the other monitors were assembled at [[Ruse, Bulgaria|Rustschuk]], Bulgaria.{{sfn|Halpern|2012|p=274}} The [[Central Powers]] were aware that the Romanians were negotiating to enter the war on the side of the [[Triple Entente|Entente]], so the flotilla established a sheltered base in the [[Belene Island|Belene Canal]] to protect the {{convert|480|km|mi|adj=on}} Danube border between Romania and Bulgaria.{{sfn|Halpern|2012|p=275}} During 1915, the {{convert|37|mm|abbr=on}} guns on the ''Bodrog'' were replaced with a single {{convert|66|mm|abbr=on}}L/18 gun, and three machine guns were also fitted.{{sfn|Pawlik|Christ|Winkler|1989|p=60}}

When the Romanians entered the war on 27 August 1916, the monitors were again at Rustschuk, and were immediately attacked by three improvised [[torpedo boat]]s operating out of the Romanian river port of [[Giurgiu]]. The torpedoes that were fired missed the monitors but struck a [[Lighter (barge)|lighter]] loaded with fuel. Tasked with shelling Giurgiu the following day, the Second Monitor [[Division (naval)|Division]], consisting of ''Bodrog'' and three other monitors, set fire to oil storage tanks, the railway station and [[Magazine (artillery)|magazines]], and sank several Romanian lighters. While the attack was underway, the First Monitor Division escorted supply ships back to the Belene anchorage. ''Bodrog'' and her companions then destroyed two Romanian patrol boats and an improvised [[minelayer]] on their way back to Belene. This was followed by forays of the Division both east and west of Belene, during which both [[Turnu Măgurele]] and [[Zimnicea]] were shelled.{{sfn|Halpern|2012|p=277}}

On 2 October 1916, ''Bodrog'' and ''Körös'' attacked a Romanian [[pontoon bridge]] being established across the Danube at [[Oryahovo]], obtaining five direct hits, thus contributing to the defeat of the Romanian [[Flămânda Offensive]]. This was followed by action supporting the crossing of ''[[Generalfeldmarschall]]''{{refn|Equivalent to a British Army [[Field marshal (United Kingdom)|field marshal]].{{sfn|Mombauer|2001|p=xv}}|group = lower-alpha}} [[August von Mackensen]]'s Austro-Hungarian Third Army at [[Svishtov|Sistow]]. ''Bodrog'' then wintered at [[Drobeta-Turnu Severin|Turnu Severin]].{{sfn|Pawlik|Christ|Winkler|1989|p=60}}

From 21 February 1917, ''Bodrog'' and ''Körös'' were deployed as guardships at [[Brăila]]. On 1 March, ''Bodrog'' became stuck in ice at nearby [[Măcin]]. LSL Guido Taschler took command of ''Bodrog'' in 1918. That year's spring thaw saw ''Bodrog'', ''Körös'', ''Szamos'', ''Bosna'' and several other vessels sent through the mouth of the Danube into the [[Black Sea]] as part of ''Flottenabteilung Wulff'' (Fleet Division Wulff) under the command of ''Flottenkapitän'' (Fleet Captain) Olav Wulff, arriving in [[Odessa]] on 12 April. On 15 July, she and ''Bosna'' sailed to the port of [[Mykolaiv|Nikolaev]], and from 5 August, ''Bodrog'' was stationed at [[Kherson|Cherson]]. On 12 September, she returned to Brăila along with other vessels.{{sfn|Pawlik|Christ|Winkler|1989|p=60}}

''Bodrog'' was sent to [[Reni, Ukraine|Reni]] near the mouth of the Danube to protect withdrawing Austro-Hungarian troops, arriving there on 1 October. She then sailed upstream, reaching Rustschuk on 11 October, and Giurgiu two days later. On 14 October, she was deployed at Lom.{{sfn|Pawlik|Christ|Winkler|1989|pp=60–61}} She was the last Austro-Hungarian monitor to withdraw towards Budapest and was the only one that failed to reach the city. On 31 October 1918, ''Bodrog'' collided with a sand bank while navigating through heavy fog near Vinča.{{sfn|Fox News|14 April 2014}} She was later captured by the Serbian Army.{{sfn|Fitzsimons|1977|p=843}}

===Interwar period and World War II===
From the [[Armistice of 11 November 1918|Armistice]] to September 1919, ''Bodrog'' was crewed by sailors of the newly created [[Kingdom of Yugoslavia|Kingdom of Serbs, Croats and Slovenes]] ({{lang-sh|Kraljevina Srba, Hrvata i Slovenaca}}, KSCS; later the Kingdom of Yugoslavia). Under the terms of the [[Treaty of Saint-Germain-en-Laye (1919)|Treaty of Saint-Germain-en-Laye]], ''Bodrog'' was transferred to the KSCS along with a range of other vessels, including three other river monitors,{{sfn|Gardiner|1985|p=422}} but was not officially handed over to the [[Royal Yugoslav Navy|KSCS Navy]] and renamed ''Sava'' until 15 April 1920.{{sfn|Gardiner|1985|p=426}}{{sfn|Pawlik|Christ|Winkler|1989|p=61}} Her sister ship ''Temes'' was transferred to Romania and renamed ''Ardeal''.{{sfn|Greger|1976|p=141}} In 1925–26, ''Sava'' was refitted, but by the following year only two of the four river monitors of the KSCS Navy were being retained in full commission at any time.{{sfn|Jarman|1997a|p=732}} In 1932, the British naval [[attaché]] reported that Yugoslav ships were engaging in little gunnery training, and few exercises or manoeuvres, due to reduced budgets.{{sfn|Jarman|1997b|p=451}}

''Sava'' was based at [[Dubovac (Kovin)|Dubovac]] when the [[Nazi Germany|German]]-led [[Axis Powers|Axis]] [[invasion of Yugoslavia]] began on 6 April 1941. She was assigned to the 1st Monitor Division,{{sfn|Niehorster|2013a}} and was responsible for the Romanian border on the Danube, under the operational control of the [[3rd Infantry Division Dunavska|3rd Infantry Division ''Dunavska'']].{{sfn|Terzić|1982|p=168}} Her commander was ''Poručnik bojnog broda''{{refn|group=lower-alpha|This is equivalent to a [[United States Navy]] [[Lieutenant commander (United States)|lieutenant commander]].{{sfn|Niehorster|2013b}}}} S. Rojos.{{sfn|Niehorster|2013a}}
[[File:Junkers Ju 87Ds in flight Oct 1943.jpg|thumb|220px|During their withdrawal towards Belgrade, ''Sava'' and {{ship|Yugoslav monitor|Vardar||2}} were repeatedly attacked by German [[Junkers Ju 87]] ''Stuka'' [[dive bomber]]s.|alt=a black and white photograph of aircraft flying with a mountainous backdrop]]
On that day, ''Sava'' and her fellow monitor {{ship|Yugoslav monitor|Vardar||2}} fought off several attacks by individual ''[[Luftwaffe]]'' aircraft on their base.{{sfn|Terzić|1982|p=297}} Over the next three days, the two monitors laid mines in the Danube near the Romanian border.{{sfn|Terzić|1982|pp=333–334}} On 11 April, they were forced to withdraw from Dubovac towards Belgrade.{{sfn|Terzić|1982|pp=391–392}} During their withdrawal, they came under repeated attacks by [[Junkers Ju 87]] ''Stuka'' [[dive bomber]]s.{{sfn|Shores|Cull|Malizia|1987|p=222}} ''Sava'' and her fellow monitor were undamaged, and anchored at the confluence of the Danube and Sava near Belgrade at about 20:00, where they were joined by the {{ship|Yugoslav monitor| Morava||2}}. The three captains conferred, and decided to scuttle their vessels due to the high water levels in the rivers and low bridges, which meant there was insufficient clearance for the monitors to navigate freely. The crews of the monitors were then transshipped to two tugboats, but when one of the tugboats was passing under a railway bridge, charges on the bridge accidentally exploded and the bridge fell onto the tugboat. Of the 110 officers and men aboard the vessel, 95 were killed.{{sfn|Terzić|1982|pp=391–392}}{{sfn|Chesneau|1980|p=357}}

After the scuttling of the monitors, around 450 officers and men from the ''Sava'' and various other riverine vessels gathered at [[Obrenovac]]. Armed only with personal weapons and some machine guns stripped from the scuttled vessels, the crews started towards the [[Bay of Kotor]] in the southern [[Adriatic Sea|Adriatic]] in two groups. The smaller of the two groups reached its objective,{{sfn|Terzić|1982|p=432}} but the larger group only made it as far as [[Sarajevo]] by 14 April before they were obliged to surrender.{{sfn|Terzić|1982|p=432}}{{sfn|Terzić|1982|p=405}} The remainder made their way to the Bay of Kotor, which was captured by the [[Kingdom of Italy|Italian]] [[XVII Corps (Italy)|XVII Corps]] on 17 April.{{sfn|Terzić|1982|p=457}}

''Sava'' was raised and repaired by the [[Navy of the Independent State of Croatia|navy]] of the Axis [[puppet state]] the [[Independent State of Croatia]],{{sfn|Chesneau|1980|p=357}} and served under that name alongside her fellow monitor ''Morava'', which was raised, repaired, and renamed ''Bosna''. Along with six captured motorboats and ten auxiliary vessels, they made up the riverine police force of the Croatian state.{{sfn|Chesneau|1980|p=359}} ''Sava'' was part of the 1st Patrol Group of the River Flotilla Command, headquartered at Zemun.{{sfn|Niehorster|2013c}} Her crew scuttled her near [[Slavonski Brod]] on the night of 8 September 1944 and defected to the [[Yugoslav Partisans]].{{sfn|Naval Records Club|1965|p=44}}

===Post-war period===
''Sava'' was again raised and refurbished after World War II. Armed with two single {{convert|105|mm|abbr=on}} gun turrets, three single {{convert|40|mm|abbr=on}} gun mounts and six {{convert|20|mm|abbr=on}} weapons,{{sfn|Gardiner|1983|p=392}} she served in the [[Yugoslav Navy]] from 1952 to 1962. Afterwards, she was placed into the hands of a state-owned company, which was privatised after the [[breakup of Yugoslavia]]. As of 2014, ''Sava'' was serving as a gravel barge. In 2005, the [[government of Serbia]] granted her limited heritage protection after citizens demanded that she be preserved as a floating museum, though little else has been done to restore her as of 2014.{{sfn|Fox News|14 April 2014}} The ship is one of only two surviving Austro-Hungarian river monitors from World War I,{{sfn|Zarić|27 April 2014}} the other being [[SMS Leitha|SMS ''Leitha'']], a much older monitor, which has been a museum ship anchored alongside the [[Hungarian Parliament Building]] in Budapest since 2014.{{sfn|Daily News Hungary|15 August 2014}}

==Notes==
{{reflist|group=lower-alpha|40em}}

==Footnotes==
{{reflist|20em}}

==References==

===Books and journals===
{{refbegin|30em}}
* {{cite book
  | editor-last = Chesneau
  | editor-first = Roger
  | year = 1980
  | title = Conway's All the World's Fighting Ships, 1922–1946
  | publisher = Conway Maritime Press
  | location = London, England
  | isbn = 978-0-85177-146-5
  | ref = harv
  }}
* {{cite book
  | last = Deak
  | first = Istvan
  | title = Beyond Nationalism: A Social and Political History of the Habsburg Officer Corps, 1848–1918
  | url = https://books.google.com/books?id=aGZrRnYERr0C&pg=PT19
  | year = 1990
  | publisher = Oxford University Press
  | location = New York, New York
  | isbn = 978-0-19-992328-1
  | ref = harv
  }}
* {{cite book
  | editor-last = Fitzsimons
  | editor-first = Bernard
  | year = 1977
  | title = The Illustrated Encyclopedia of 20th Century Weapons and Warfare
  | volume = 8
  | publisher = Columbia House
  | location = New York, New York
  | oclc = 732716343
  | ref = harv
  }}
* {{cite book
  | editor-last = Gardiner
  | editor-first = Robert
  | year = 1985
  | title = Conway's All the World's Fighting Ships, 1906–1921
  | publisher = Conway Maritime Press
  | location = London, England
  | isbn = 978-0-85177-245-5
  | ref = harv
  }}
* {{cite book
  | editor-last = Gardiner
  | editor-first = Robert
  | year = 1983
  | title = Conway's All the World's Fighting Ships, 1947–1982
  | publisher = Naval Institute Press
  | location = Annapolis, Maryland
  | isbn = 978-0-87021-919-1
  | ref = harv
  }}
* {{cite book
  | last = Greger
  | first = René
  | year = 1976
  | title = Austro-Hungarian Warships of World War I
  | publisher = Allan
  | location = London, England
  | isbn = 978-0-7110-0623-2
  | ref = harv
  }}
* {{cite book
  | last = Halpern
  | first = Paul G.
  | year = 2012
  | title = A Naval History of World War I
  | publisher = Naval Institute Press
  | location = Annapolis, Maryland
  | isbn = 978-0-87021-266-6
  | ref = harv
  }}
* {{cite book
  | last = Jane's Information Group
  | year = 1989
  | origyear = 1946/47
  | title = Jane's Fighting Ships of World War II
  | publisher = Studio Editions
  | location = London, England
  | isbn = 978-1-85170-194-0
  | ref = harv
  }}
* {{cite book
  | editor-last = Jarman
  | editor-first = Robert L.
  | year = 1997a
  | title = Yugoslavia Political Diaries 1918–1965
  | volume = 1
  | publisher = Archives Edition
  | location = Slough, Berkshire
  | isbn = 978-1-85207-950-5
  | ref = harv
  }}
* {{cite book
  | editor-last = Jarman
  | editor-first = Robert L.
  | year = 1997b
  | title = Yugoslavia Political Diaries 1918–1965
  | volume = 2
  | publisher = Archives Edition
  | location = Slough, Berkshire
  | isbn = 978-1-85207-950-5
  | ref = harv
  }}
* {{cite book
  | last = Mombauer
  | first = Annika
  | year = 2001
  | title = Helmuth von Moltke and the Origins of the First World War
  | publisher = Cambridge University Press
  | location = Cambridge, New York
  | isbn = 978-0-521-79101-4
  | ref = harv
  }}
* {{cite book
  | last1=Pawlik
  | first1=Georg
  | last2=Christ
  | first2=Heinz
  | last3=Winkler
  | first3=Herbert
  | title = Die K.u.K. Donauflottille 1870–1918
  | trans-title = The K.u.K. Danube Flotilla 1870–1918
  | language = German
  | year = 1989
  | publisher = H. Weishaupt Verlag
  | location = Graz, Austria
  | isbn = 978-3-900310-45-5
  | ref = harv
  }}
* {{cite book
  | last1 = Shores
  | first1 = Christopher F.
  | last2 = Cull
  | first2 = Brian
  | last3 = Malizia
  | first3 = Nicola
  | title = Air War for Yugoslavia, Greece, and Crete, 1940–41
  | year = 1987
  | publisher = Grub Street
  | location = London, England
  | isbn = 978-0-948817-07-6
  | ref = harv
  }}
* {{cite book
  | last = Terzić
  | first = Velimir
  | title = Slom Kraljevine Jugoslavije 1941: Uzroci i posledice poraza
  | trans_title = The Collapse of the Kingdom of Yugoslavia in 1941: Causes and Consequences of Defeat
  | language = Serbo-Croatian
  | volume = 2
  | publisher = Narodna knjiga
  | location = Belgrade, Yugoslavia
  | year = 1982
  | oclc = 10276738
  | ref = harv
  }}
* {{cite journal
  | author = Naval Records Club
  | title = The Independent Croatian Navy
  | year = 1965
  | journal = Warship International
  | volume = 2
  | publisher = International Naval Research Organization
  | location = Rutland, Massachusetts
  | issn = 0043-0374
  | ref = {{harvid|Naval Records Club|1965}}
  }}
{{refend}}

===Online sources===
{{refbegin|30em}}
* {{cite news
  | website = Daily News Hungary
  | date = 15 August 2014
  | title = Europe's Oldest River Battleship Inaugurated as a Museum at Parliament Pier
  | url = http://dailynewshungary.com/europes-oldest-river-battleship-inaugurated-as-museum-at-parliament-pier/
  | ref = {{harvid|Daily News Hungary15 August 2014}}
  }}
* {{cite news
  | website = Fox News
  | date = 14 April 2014
  | title = Warship that fired first shots of WWI now a gravel barge in Serbia
  | url = http://latino.foxnews.com/latino/entertainment/2014/04/14/warship-that-fired-first-shots-wwi-now-gravel-barge-in-serbia/
  | ref = {{harvid|Fox News14 April 2014}}
  }}
* {{cite web
  | url =  http://www.niehorster.org/040_yugoslavia/41-04-06/navy_danube.html
  | title = Balkan Operations Order of Battle Royal Yugoslavian Navy River Flotilla 6th April 1941
  | accessdate = 14 May 2015
  | publisher  = Dr. Leo Niehorster
  | year   = 2013a
  | last = Niehorster
  | first = Dr. Leo
  | ref = harv
  }}
* {{cite web
  | url =  http://www.niehorster.org/040_yugoslavia/_ranks.htm
  | title = Royal Yugoslav Armed Forces Ranks
  | accessdate = 14 May 2015
  | publisher  = Dr. Leo Niehorster
  | year   = 2013b
  | last = Niehorster
  | first = Dr. Leo
  | ref = harv
}}
* {{cite web
  | url =  http://www.niehorster.org/041_croatia/41-07-01/41-07_navy.html
  | title = Armed Forces of the Independent State of Croatia – Order of Battle Croatian Navy 1st July 1941
  | accessdate = 11 March 2015
  | publisher  = Dr. Leo Niehorster
  | year   = 2013c
  | last = Niehorster
  | first = Dr. Leo
  | ref = harv
}}
* {{cite news
  | last = Zarić
  | first = Slađana
  | date = 27 April 2014
  | title = Aždaje koje su rušile Beograd
  | trans_title = The Beasts That Shelled Belgrade
  | language = Serbian
  | publisher = [[Radio Television of Serbia]]
  | url = http://www.rts.rs/page/stories/sr/%D0%92%D0%B5%D0%BB%D0%B8%D0%BA%D0%B8+%D1%80%D0%B0%D1%82/story/2216/Srbija+u+Velikom+ratu/1582549/A%C5%BEdaje+koje+su+ru%C5%A1ile+Beograd.html
  | ref = {{harvid|Zarić27 April 2014}}
  }}
{{refend}}
{{Yugoslav river monitors}}
{{April 1941 shipwrecks}}
{{September 1944 shipwrecks}}
{{Surviving ocean going ships}}


{{DEFAULTSORT:Sava}}
[[Category:1904 ships]]
[[Category:Ships built in Austria-Hungary]]
[[Category:World War I naval ships of Austria-Hungary]]
[[Category:World War I monitors]]
[[Category:World War II monitors]]
[[Category:Maritime incidents in April 1941]]
[[Category:World War II naval ships of Yugoslavia]]
[[Category:Ships of the Royal Yugoslav Navy]]
[[Category:Riverine warfare]]
[[Category:Navy of the Independent State of Croatia]]
[[Category:Ships of the Yugoslav Navy]]
[[Category:Maritime incidents in September 1944]]