{{BLP sources|date=July 2012}}
'''David Carkeet''' (born November 15, 1946, [[Sonora, California]]) is an American [[novelist]] and [[essayist]].  Three of his novels have been named ''New York Times Book Review'' Notable Books of the Year.<ref>{{cite book|title=''International Who's Who of Authors and Writers''|author=Routledge|year=2003|page=92|url=https://books.google.com/books?id=phhhHT64kIMC&pg=PA92 }}</ref>

==Biography==
Carkeet grew up in the small northern California town where he was born and attended the [[University of California at Davis]] and Berkeley, graduating from the Davis campus with a B.A. degree in German in 1968.  He received an M.A. in English literature from the [[University of Wisconsin]] in 1970 and a Ph.D. in English linguistics from [[Indiana University]] in 1973.  From 1973 to 2002 he taught writing and linguistics at the University of Missouri in St. Louis.  He married Barbara Lubin of Elmira, New York, in 1975, and they raised three daughters, Anne, Laurie, and Molly.  He has lived in [[Middlesex, Vermont]], since 2003.<ref>{{cite web|title=''Directory of Writers''; Poets & Writers|year= 2011|url=http://www.pw.org/content/david_carkeet_1|publisher=Pw.org|accessdate=2012-11-06}}</ref>

==Books==
''Carkeet'' has written six novels for adults, two novels for young adults, and one memoir.<ref>{{cite web|title=WorldCat catalogue|url=http://www.worldcat.org/identities/lccn-n80-68787|publisher=Worldcat.org|accessdate=2012-11-06}}</ref>  A comic writer in the vein of [[Kingsley Amis]], [[David Lodge (author)|David Lodge]], and [[Peter De Vries]], he is best known for his three novels featuring a linguist named Jeremy Cook as the protagonist:
* ''Double Negative'' (Dial, 1980),<ref>{{cite book|url=https://books.google.com/books?id=Ir86OLUK4rEC&printsec=frontcover&dq=inauthor:%22David+Carkeet%22&hl=en&sa=X&ei=Zo7gT__fIuGh6gGroZh_&ved=0CEkQ6AEwAg#v=onepage&q=inauthor%3A%22David%20Carkeet%22&f=false |title=Double Negative - David Carkeet - Google Books |publisher=Books.google.com |date= |accessdate=2012-11-06}}</ref> in which Jeremy Cook tries to solve a murder mystery while simultaneously studying language acquisition in toddlers at The Wabash Institute, a southern Indiana daycare center/research facility.
* ''The Full Catastrophe'' (Simon and Schuster, 1990),<ref>{{cite book|url=https://books.google.com/books?id=RXPKQgAACAAJ&dq=inauthor:%22David+Carkeet%22&hl=en&sa=X&ei=Zo7gT__fIuGh6gGroZh_&ved=0CGMQ6AEwBw |title=The Full Catastrophe - David Carkeet - Google Books |publisher=Books.google.com |date= |accessdate=2012-11-06}}</ref> in which Cook, working for a marriage counseling service known as The Pillow Agency, moves in with a [[St. Louis]] couple to study their communication troubles.
* ''The Error of Our Ways'' (Holt, 1997), in this novel Cook comes to grips with the blunders that have defined his life.

Carkeet's other novels treat a range of subjects:
* ''The Greatest Slump of All Time'' (Harper and Row, 1984), a comic novel about a depressed baseball team.
* ''I Been There Before'' (Harper and Row, 1985),<ref>{{cite book|url=https://books.google.com/books?id=kkHI5Fg9_qoC&q=inauthor:%22David+Carkeet%22&dq=inauthor:%22David+Carkeet%22&hl=en&sa=X&ei=Zo7gT__fIuGh6gGroZh_&ved=0CF4Q6AEwBg |title=I Been There Before - David Carkeet - Google Books |publisher=Books.google.com |date= |accessdate=2012-11-06}}</ref> which brings Mark Twain back to life with the 1985 return of [[Halley's Comet]].
* ''From Away'' (Overlook, 2010), in which an imposter from out-of-state ("from away" in Vermont parlance)  assumes a missing Vermonter's identity.

Two of Carkeet's novels are mysteries (''Double Negative'' and ''From Away''), and mystery figures importantly in his two young adult novels, set in the Sierra foothills of his youth - ''The Silent Treatment''<ref>{{cite book|url=https://books.google.com/books?id=UD5ayhMCGEIC&q=inauthor:%22David+Carkeet%22&dq=inauthor:%22David+Carkeet%22&hl=en&sa=X&ei=Zo7gT__fIuGh6gGroZh_&ved=0CGgQ6AEwCA |title=The Silent Treatment - David Carkeet - Google Books |publisher=Books.google.com |date= |accessdate=2012-11-06}}</ref> and ''Quiver River'' (Harper and Row, 1988, 1991).<ref>{{cite book|url=https://books.google.com/books?id=mQ-CbvWp_ZkC&q=inauthor:%22David+Carkeet%22&dq=inauthor:%22David+Carkeet%22&hl=en&sa=X&ei=Zo7gT__fIuGh6gGroZh_&ved=0CG0Q6AEwCQ |title=Quiver River - David Carkeet - Google Books |publisher=Books.google.com |date= |accessdate=2012-11-06}}</ref><ref>{{cite web|title=''Something About the Author'', Gale, 1994, vol. 75, pp. 21-22|url=http://www.worldcat.org/title/something-about-the-author-volume-75/oclc/705262513|publisher=Worldcat.org|accessdate=2012-11-06}}</ref>

His novel ''The Full Catastrophe'' has been adapted for the stage by [[Michael Weller]]. The play premiered in 2015 at the [[Contemporary American Theater Festival]].

'''His memoir:'''

His memoir, ''Campus Sexpot'' (University of Georgia Press, 2005), tells of the impact on his life made by a 1961 novel of the same name written by a former English teacher at Carkeet's high school. In it, a busty co-ed seduces her English instructor. Carkeet's ''Campus Sexpot'' also details the impact the book had on [[Sonora, California]], where he grew up. The town in which the original novel is set, the fictional burg of Wattsville, sounds much like Sonora, and some of the characters' names are virtually identical to the names of actual Sonorans. Carkeet's ''Campus Sexpot'' has generated some controversy, with some of the original author's descendants objecting to Carkeet's portrayal of him.<ref>{{cite web|author=Post Jobs |url=http://www.insidehighered.com/views/mclemee/mclemee220 |title=Campus Sexpot |publisher=Inside Higher Ed |date=2005-11-03 |accessdate=2012-11-06}}</ref> In addition to inspiring Carkeet's memoir, the original novel generated a fictional sequel, ''From Roundheel To Revolutionary: Linda Franklin After '''Campus Sexpot''''', by Jeff Daiell.

==Plays==
In 2016, Carkeet adapted selected comic Mark Twain short stories into stage plays--"Buck Fanshaw's Funeral," "Cannibalism in the Cars," "The Facts Concerning the Recent Carnival of Crime in Connecticut," "The McWilliamses and the Burglar Alarm," and others. The full collection, "Seven by Twain," consists of six ten-minute plays and one one-hour play ("The Man That Corrupted Hadleyburg").<ref>{{cite web|title=New Play Exchange|url=https://newplayexchange.org/users/8566/david-carkeet}}</ref><ref>{{cite web|title=Broadway World|url=http://www.broadwayworld.com/connecticut/article/SIX-BY-TWAIN--Short-Stories-by-Mark-Twain-Adapted-for-the-Stage-Presented-in-Connecticut-This-Fall-20161020}}</ref>

==Short works==
Carkeet has written some three dozen general interest essays for ''The Village Voice'', ''The New York Times Magazine'', ''Smithsonian'', ''Poets & Writers'', ''The Oxford American'', and the online journals ''Salon'' and ''The Morning News''. In the 1990s he was a regular columnist for ''St. Louis'' magazine.  His short stories have appeared in ''North American Review'', ''Kansas Quarterly'', and ''Carolina Quarterly''.  His critical and scholarly production includes an often-cited analysis of the dialects in Mark Twain's ''Adventures of Huckleberry Finn''<ref>{{cite book|title=''Dictionary of American Scholars''. Bowker, 1982, vol. 2, p. 107|url=https://books.google.com/books?ei=3CTuT9KuEIfM0AHIqtH7DQ&id=R2xBAQAAIAAJ&dq=directory+of+american+scholars+david+carkeet&q=carkeet#search_anchor|publisher=Books.google.com|accessdate=2012-11-06}}</ref><ref>{{cite book|title=''The Mark Twain Encyclopedia'' Routledge, 1993, pp. 219-220|url=https://books.google.com/books?id=zW1k-XS6XLEC&pg=PA219&dq=dialects+mark+twain+encyclopedia&hl=en&sa=X&ei=TF3yT7OhJZOQ0QHRqcH7Ag&ved=0CDQQ6AEwAA#v=onepage&q&f=false|publisher=Books.google.com|accessdate=2012-11-06}}</ref>

==Honors==
In 1981, Carkeet was nominated for an [[Edgar Award]] in the first-novel category by the Mystery Writers of America for ''Double Negative'', published the year before.<ref>{{cite web|title=Edgars Database|url=http://www.theedgars.com/edgarsDB/index.php|publisher=Theedgars.com|accessdate=2012-11-06}}</ref> He won an [[O. Henry Award]] in 1982 for "The Greatest Slump of All Time," a short story originally published in ''Carolina Quarterly'' that he later expanded into the novel of the same title.<ref>{{cite web|url=http://www.randomhouse.com/boldtype/ohenry/0900/winners1919.html |title=Bold Type: O. Henry Award Winners 1919-2000 |publisher=Randomhouse.com |date= |accessdate=2012-11-06}}</ref>  He received a creative writing fellowship from the [[National Endowment for the Arts]] in 1983,<ref>{{cite web|title=''NEA Literature Fellowships: 40 Years of Supporting Writers'' |page=16 |url=http://www.nea.gov/pub/nea_lit.pdf |publisher=Nea.gov |accessdate=2012-11-06 |deadurl=yes |archiveurl=http://www.webcitation.org/5lOncIM6C?url=http://www.nea.gov/pub/nea_lit.pdf |archivedate=2009-11-19 |df= }}</ref> and he won the Creative Nonfiction Award from the [[Association of Writers & Writing Programs]] in 2004.<ref name="Of Writers">{{cite web|url=http://www.awpwriter.org/contests/as2004.htm |title=Association of Writers & Writing Programs |publisher=Awpwriter.org |date= |accessdate=2012-11-06}}</ref>

==References==
{{Reflist}}

==External links==
* [http://www.davidcarkeet.com/ Official David Carkeet website]

{{Authority control}}

{{DEFAULTSORT:Carkeet, David}}
[[Category:Articles created via the Article Wizard]]
[[Category:1946 births]]
[[Category:20th-century American novelists]]
[[Category:21st-century American novelists]]
[[Category:American male novelists]]
[[Category:Indiana University alumni]]
[[Category:Living people]]
[[Category:People from St. Louis County, Missouri]]
[[Category:People from Sonora, California]]
[[Category:People from Washington County, Vermont]]
[[Category:University of California, Davis alumni]]
[[Category:University of Missouri–St. Louis people]]
[[Category:University of Wisconsin–Madison alumni]]
[[Category:Writers from California]]
[[Category:Writers from Missouri]]
[[Category:Writers from Vermont]]