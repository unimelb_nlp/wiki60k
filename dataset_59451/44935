{{For|the Japanese band|Loudness (band)}}

'''Loudness''' is the characteristic of a [[sound]] that is primarily a psycho-physiological correlate of physical strength ([[amplitude]]). More formally, it is defined as, "That attribute of auditory sensation in terms of which sounds can be ordered on a scale extending from quiet to loud."<ref>American National Standards Institute, "American national psychoacoustical terminology" S3.20, 1973, American Standards Association.</ref> The relation of physical attributes of sound to perceived loudness consists of physical, physiological and psychological components.

In different industries, loudness may have different meanings, and different standards exist, each purporting to define the measurement. Some definitions such as [[LKFS]] refer to relative loudness of different segments of electronically reproduced sounds such as for broadcasting and cinema. Others, such as ISO 532A (Stevens loudness, measured in [[sones]]), ISO 532B ([[Eberhard_Zwicker|Zwicker]] loudness), DIN 45631 and ASA/ANSI S3.4, have a more general scope and are often used to characterize loudness of environmental noise.

It is sometimes stated that loudness is a subjective measure, often confused with physical measures of sound strength such as [[sound pressure]], sound pressure [[Level (logarithmic quantity)|level]] (in [[decibels]]), [[sound intensity]] or [[sound power]]. It is often possible to separate the truly subjective components such as social considerations from the physical and physiological. 

Filters such as [[A-weighting]] attempt to adjust sound measurements to correspond to loudness as perceived by the typical human, however this approach is only truly valid for loudness of single tones. [[A-weighting]] follows human sensitivity to sound and describes relative perceived loudness for at quiet to moderate speech levels, around 40 [[phon]]s. However, physiological loudness perception is a much more complex process than can be captured with a single correction curve.<ref name=Olson1972>{{cite journal |last=Olson |first=Harry F. |date=February 1972 |title=The Measurement of Loudness |journal=Audio |pages=18–22 |url=http://www.technicalaudio.com/pdf/Audio_magazine_issues_articles/Harry%20F.%20Olson%20-%20The%20Measurement%20of%20Loudness.pdf}}</ref> Not only do equal-loudness contours vary with intensity, but perceived loudness of a complex sound depends on whether its spectral components are closely or widely spaced in frequency. When generating neural impulses in response to sounds of one frequency, the ear is less sensitive to nearby frequencies, which are said to be in the same [[critical band]]. Sounds containing spectral components in many critical bands are perceived as louder even if the total sound pressure remains constant. 

== Explanation ==
The perception of loudness is related to [[sound pressure level]] (SPL), frequency content and duration of a sound. The human auditory system averages the effects of SPL over a 600–1000&nbsp;[[Millisecond|ms]] interval. A sound of constant SPL will be perceived to increase in loudness as samples of duration 20, 50, 100, 200 ms are heard, up to a duration of about 1 second at which point the perception of loudness will stabilize. For sounds of duration greater than 1 second, the moment-by-moment perception of loudness will be related to the average loudness during the preceding 600–1000&nbsp;ms.{{Citation needed|reason=Reliable source needed for the whole paragraph|date=July 2015}}

For sounds having a duration longer than 1 second, the relationship between SPL and loudness of a single tone can be approximated by [[Stevens' power law]] in which SPL has an exponent of 0.6.{{efn|The relationship between loudness and energy ''intensity'' of sound therefore can be approximated by a power function with an exponent of 0.3.}} More precise measurements indicate that loudness increases with a higher exponent at low and high levels and with a lower exponent at moderate levels.{{cn|date=August 2016}}

[[Image:Lindos1.svg|thumb|400px|right|<center>The horizontal axis shows '''[[frequency]]''' in '''[[Hertz|Hz]]'''</center>]]

The sensitivity of the human ear changes as a function of frequency, as shown in the [[Equal-loudness contours|equal-loudness graph]]. Each line on this graph shows the SPL required for frequencies to be perceived as equally loud, and different curves pertain to different sound pressure levels. It also shows that humans with normal hearing are most sensitive to sounds around 2–4&nbsp;kHz, with sensitivity declining to either side of this region. A complete model of the perception of loudness will include the integration of SPL by frequency.<ref name=Olson1972/>

Historically, loudness was measured using an "ear-balance" [[audiometer]] in which the amplitude of a sine wave was adjusted by the user to equal the perceived loudness of the sound being evaluated. Contemporary standards for measurement of loudness are based on summation of energy in [[critical band]]s as described in [[International Electrotechnical Commission|IEC]] 532, [[DIN]] 45631 and ASA/[[ANSI]] S3.4. A distinction is made between stationary loudness (sounds that remain sensibly constant) and non-stationary (sound sources that move in space or change amplitude over time.)

== Hearing loss ==
When [[sensorineural hearing loss]] (damage to the [[cochlea]] or in the brain) is present, the perception of loudness is altered.  Sounds at low levels (often perceived by those without hearing loss as relatively quiet) are no longer audible to the hearing impaired, but sounds at high levels often are perceived as having the same loudness as they would for an unimpaired listener.  This phenomenon can be explained by two theories: loudness grows more rapidly for these listeners than normal listeners with changes in level.  This theory is called "loudness recruitment" and has been accepted as the classical explanation.  More recently, it has been proposed that some listeners with sensorineural hearing loss may in fact exhibit a normal rate of loudness growth, but instead have an elevated loudness at their threshold.  That is, the softest sound that is audible to these listeners is louder than the softest sound audible to normal listeners.  This theory is called "softness imperception", a term coined by [[Mary Florentine]].<ref>{{citation |author=[[Mary Florentine]] |title=It's not recruitment-gasp!! It's softness imperception |url=http://journals.lww.com/thehearingjournal/Fulltext/2003/03000/It_s_not_recruitment_gasp___It_s_softness.3.aspx |publisher=Hearing Journal |date=March 2003 |volume=56 |issue=3 |doi=10.1097/01.HJ.0000293012.17887.b4 |pages=10, 12, 14, 15}}</ref>

==Compensation==
{{main|Loudness compensation}}
The "loudness" control on some consumer stereos alters the [[frequency response]] curve to correspond roughly with the equal loudness characteristic of the ear.<ref>{{cite book |first=John D. |last=Lenk |title=Circuit Troubleshooting Handbook |publisher=[[McGraw-Hill]] |year=1998 |isbn=0-07-038185-2 |page=163}}</ref> Loudness compensation is intended to make the recorded music sound more natural when played at a lower levels by boosting low frequencies, to which the ear is less sensitive at lower sound pressure levels.

==Normalization==
Loudness normalization is a specific type of [[audio normalization]] that equalizes perceived level such that, for instance, commercials do not sound louder than television programs. Loudness normalization schemes exist for a number of audio applications.

===Broadcast===
*[[Commercial Advertisement Loudness Mitigation Act]]
*[[European Broadcasting Union]] R128<ref>{{citation |title=EBU Recommendation R 128: Loudness normalisation and permitted maximum level of audio signals |url=http://tech.ebu.ch/docs/r/r128.pdf |publisher=[[European Broadcasting Union]] |date=August 2011 |accessdate=2013-04-22}}</ref>

===Movie and home theaters===
*[[Dialnorm]]

===Music playback===
*Sound Check in [[iTunes]]
*[[ReplayGain]]

==Measurement==
Historically [[Sone]] (loudness ''N'') and [[Phon]] (loudness level ''L'') units have been used to measure loudness.

Relative [[loudness monitoring]] in production is measured in accordance with ITU-R BS.1770 in units of [[LKFS]].<ref>{{citation |url=http://www.itu.int/rec/R-REC-BS.1770/ |title=Recommendation BS.1770 |date=August 2012 |publisher=[[International Telecommunication Union]] |accessdate=2013-05-31}}</ref>

Work began on ITU-R BS.1770 in 2001 after 0 dBFS+ level distortion in converters and lossy codecs had become evident; and the original Leq(RLB) loudness metric was proposed by Gilbert Soloudre in 2003.<ref>{{cite web |url=http://www.audiofile-engineering.com/support/manuals/sp/1/html/leq_meter.html |title=Leq Meter |accessdate=2015-12-15}}</ref>

Based on data from subjective listening tests, Leq(RLB) was compared against numerous other algorithms where it did remarkably well.{{why|Why is this remarkable? The word is in [[WP:PEA]]|date=December 2015}} After modification of the frequency weighting, the measurement was made multi-channel ([[monaural]] to [[5.1 surround sound]]). [[Canadian Broadcasting Corporation|CBC]], [[Dolby]] and [[TC Electronics]] and numerous broadcasters contributed to the listening tests.

To make the loudness metric cross-genre friendly, a relative measurement [[Noise gate|gate]] was added. This work was carried out in 2008 by the EBU. The improvements were brought back into BS.1770-2. ITU subsequently updated the true-peak metric (BS.1770-3) and added provision for more audio channels, for instance [[22.2 surround sound]] (BS.1770-4).

==See also==
*[[Loudness war]]
*[[Sending loudness rating]]
*Volume in acoustics is related to:
**[[Amplitude]]
**[[Sound pressure]]
**[[Dynamics (music)|Dynamics]]

==Notes==
{{notelist}}

==References==
{{reflist|30em}}

{{Timbre}}

[[Category:Elements of music]]
[[Category:Acoustics]]