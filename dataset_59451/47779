{{Infobox school
| name = ETEC Lauro Gomes
| image = Escola Técnica Estadual Lauro Gomes (main entrance).jpg
| alt = 
| caption = Main entrance to ETEC Lauro Gomes photographed in March 2014. Note the old abbreviation ''ETE'' is still shown.
| motto = 
| streetaddress = 400 Pereira Barreto Avenue
| city = [[São Bernardo do Campo]]
| state = [[São Paulo (state)|São Paulo]]
| country = [[Brazil]]
| zipcode = 09751-000
| coordinates = {{coord|-23.69|-46.54|type:edu|display=inline,title}}
| established = 1957
| opened = {{start date and age|1964|10|12}}<ref name="OESP19671013">{{cite news| newspaper = O Estado de S. Paulo | page = 13 | date = October 13, 1967 | accessdate = January 30, 2014 | language = Portuguese | title = Escola do ABC recebe aparelhamento alemão | trans_title = ABC (region) school receives German apparatuses | url = http://acervo.estadao.com.br/pagina/#!/19671013-28375-nac-0013-999-13-not/busca/S%C3%A3o+Bernardo+Campo+Escola+T%C3%A9cnica+Industrial}}</ref>
| schooltype = [[Vocational school]], [[Secondary school]]
| fundingtype = [[State school]]
| authority = CEETEPS (''Centro Estadual de Educação Tecnológica Paula Souza'')
| grades = 
| superintendent = 
| principal = 
| viceprincipal = 
| enrollment =
| faculty = 
| campus type = [[Urban area|Urban]]
| campus size = 
| mascot =
| newspaper = 
| colors = {{Color box|green|border=darkgray}}{{Color box|white|border=darkgray}}
| website = 
| feeders = 
| footnotes =<ref name="ETELGSP">{{cite web | url = http://www.centropaulasouza.sp.gov.br/Ete/Escolas/Metrop_Sao_Paulo/Sao_Bernardo_ETE_Lauro.html | title = ETEC Lauro Gomes overview on São Paulo State Government website | language = Portuguese | accessdate = January 30, 2014 }}</ref>
}}

'''ETEC Lauro Gomes''', formerly known as '''ETE Lauro Gomes''', '''ETI Lauro Gomes''' and '''Escola Técnica Industrial de São Bernardo do Campo''' is an educational facility located in [[São Bernardo do Campo]], [[São Paulo (state)|SP]], [[Brazil]]. It offers [[secondary education]] and [[vocational education]] classes. It is named after [[Lauro Gomes de Almeida]], [[mayor]] for São Bernardo do Campo between 1952 and 1954 and 1960 to 1963.<ref name="PMSBCm">{{cite web | website = City Hall of São Bernardo do Campo website | accessdate = January 28, 2014 | language = Portuguese | url = http://www.saobernardo.sp.gov.br/comuns/pqt_container_r01.asp?srcpg=historia_historia_prefeitos_curriculum&area=&ref=7 | title = Lauro Gomes de Almeida }}</ref>
ETEC Lauro Gomes is adjoined to São Paulo Technical School System, conducted by CEETEPS (Centro Estadual de Educação Tecnológica Paula Souza), an independent government body designed to govern technical schools and [[São Paulo State Technological College|faculties]] in São Paulo.<ref name="CEETEPS">{{cite web | url = http://www.centropaulasouza.sp.gov.br/quem-somos/perfil-historico/ | title = Centro Paula Souza - Quem Somos | trans_title = Centro Paula Souza - Who we are | language = Portuguese | accessdate = January 28, 2014 }}</ref>

== History ==

São Bernardo do Campo, a municipality neighboring [[São Paulo]], started to experience a fast growth of the [[manufacturing]] sector after the inauguration of [[Via Anchieta]], a road linking São Paulo to [[Santos, São Paulo|Santos]] crossing the town. In the 1950s, the traditional [[furniture]] making industry began to share space with other activities, such as [[Pharmaceutical industry|pharmaceutical]], [[home appliance]]s and specially [[Automotive industry|automotive]]. In 1958, [[Willys-Overland]], [[Mercedes-Benz]] and [[Volkswagen]] already had plants in the city,<ref name="OESP19580820">{{cite news | newspaper = O Estado de S. Paulo | language = Portuguese | page = 14 | date = August 20, 1968 | title = S. Bernardo do Campo após 405 anos de existência | url = http://acervo.estadao.com.br/pagina/#!/19580820-25552-nac-0014-999-14-not/busca/Bernardo+Campo+S%C3%A3o | trans_title = São Bernardo do Campo after 405 years of life | accessdate = March 1, 2014 | author = <!--Staff writer(s); no by-line.--> }}</ref> thus demanding skilled workers.

In 1956 the [[Ministry of Education (Brazil)|Ministry of Education and Culture - MEC]] granted a [[Brazilian cruzeiro|₢$]] 20 million [[Appropriation (law)|appropriation]] for the installment of the school,<ref name="OESP19560714">{{cite news | language = Portuguese | newspaper = O Estado de S. Paulo | date = July 7, 1956 | accessdate = February 1, 2014 | page = 6 | url = http://acervo.estadao.com.br/pagina/#!/19560714-24907-nac-0006-999-6-not/busca/escola+t%C3%A9cnica+industrial+S%C3%A3o+Bernardo+Campo | title = Construção de Escola Técnica em São Bernardo | trans_title = Construction of a Technical School in São Bernardo }}</ref> and in 1957 the state decree no. 3734 was established by [[List of Governors of São Paulo|Governor]] [[Jânio Quadros]] to enforce the partnership among the São Paulo Government, the Brazilian Ministry of Education and Culture and the São Bernardo do Campo Government to establish the facility.<ref name="JBr">{{cite web | website = jusbrasil.com.br | title = Decree 3734/57 - SP | accessdate = February 1, 2014 | language = Portuguese | url = http://governo-sp.jusbrasil.com.br/legislacao/224809/lei-3734-57 }}</ref> The construction has been started in 1958 at a {{convert|170000|m2|sqft}} site.<ref name="OESP19580729">{{cite news | newspaper = O Estado de S. Paulo | language = Portuguese | date = July 29, 1958 | accessdate = February 1, 2014 | url = http://acervo.estadao.com.br/pagina/#!/19580729-25533-nac-0016-999-16-not/busca/S%C3%A3o+BERNARDO+escola+Bernardo+Campo | title = Construção de escolas em quatro cidades paulistas | trans_title = Building of schools in four São Paulo cities }}</ref>
In 1964 a council was set to manage a cooperation agreement between the [[Brazilian government]] and the former [[Federal Republic of Germany|Federal Republic of Germany government]] to allow the latter to provide equipment, techniques and teachers to improve the teaching level in the school. Such agreement would allow the insertion of German educators for five years into the facility to be gradually substituted by their Brazilian counterparts.<ref name="OESP19640626">{{cite news | url = http://acervo.estadao.com.br/pagina/#!/19640626-27355-nac-0010-999-10-not/busca/Escola+T%C3%A9cnica+Industrial+S%C3%A3o+Bernardo+Campo | language = Portuguese | accessdate = January 30, 2014 | date = June 26, 1964 | newspaper = O Estado de S. Paulo | page = 10 | title = Bonn dará técnicos e material à Escola de S. Bernardo do Campo | trans_title = Bonn will provide technicians and material to São Bernardo do Campo School }}</ref> However, in 1967, the agreement was not fully operational as the school still was in construction, with a 60% completion rate and offering the construction of machines and engines as its only course, although the Industrial Engineering Faculty of the [[Pontifical Catholic University of São Paulo]] (now known as [[Centro Universitário da FEI]]) already was using part of the facilities.<ref name="OESP19671013" /> In the year before, the school has been named Lauro Gomes <ref name="ETELGH">{{cite web | website = ETELG website | language = Portuguese | accessdate = February 27, 2014 | url = http://www.etelg.com.br/paginaete/escola/historico.htm | title = Histórico da Escola | trans_title = School history }}</ref> after the former city mayor died in 1964.<ref name="PMSBCm" />

In the 1970s other programs have been started, such as electronics, electrotechnics, designing of tools and machining fixtures, and industrial laboratory technician.<ref name="ETELGH" /> The [[Electronic data processing]] course has been established in 1985.<ref name="ETELGH" />

== Adjoining the State Technical School Center ==

In 1980, state decree no. 16309 ordered the school to join Centro Paula Souza, effective January 1, 1981, thus being one of the state-managed technical schools in São Paulo. The law has been enacted by Governor [[Paulo Maluf]].<ref name="DE16309">{{cite web | website = jusbrasil.com.br | accessdate = February 27, 2014 | language = Portuguese | url = http://governo-sp.jusbrasil.com.br/legislacao/204378/decreto-16309-80 | title = Decreto 16309/80 | trans_title = State decree 16309/80 }}</ref>

== The Reform ==

The vocational program and the high school programs were fused into a single lesson plan, the latter frequently adapted to match the skills needed in the first one. However the national technical education reform of 1997 deterred, in practice, the continuation of such scheme, stating only 25% of the full educational schedule could be allocated for vocational lessons.<ref name="D2208">{{cite web | url = http://www.planalto.gov.br/ccivil_03/decreto/D2208.htm | accessdate = March 7, 2014 | date = April 17, 1997 | language = Portuguese | title = Decreto 2208/97 | trans_title = Federal Decree no. 2208/97 | quote = Article 5: The vocational education will have its own coursework, independent from the high school plan, to be offered concomitantly or sequentially to it; Sole paragraph: The vocational classes may be taught in the diversified portion of high school classes, limited to 25% of the full minimum schedule (...). | website = Brazilian Presidency's Website }}</ref> Although such resolution has been superseded in 2004, the liaisons between the vocational and the high school classes were reinstalled as an option, thus allowing the continuation of separate courses offering.<ref name="D5154">{{cite web | url = http://www.planalto.gov.br/ccivil_03/_Ato2004-2006/2004/Decreto/D5154.htm#art9 | accessdate = March 7, 2014 | date = July 23, 2004 | language = Portuguese | title = Decreto 5154/04 | trans_title = Federal Decree no. 5154/04 | website = Brazilian Presidency's Website | quote = Article 4, § 1: The articulation between the high school level vocational classes and the high school common classes will be given through: I - integrated (...), being such course planned to conduce the learner to achieve his technical habilitation, in the same learning institution, with a single admittance; II - concomitantly, (...), when the completeness between the vocational and high school courses assumes distinct admittances for each segment}}</ref>

== Habilitations Offered ==

As of 2014, along with the high school classes, the following vocational habilitations are available:<ref name="ETELGSP" />

* [[Accounting]]
* [[Business Administration]]
* [[Chemistry]] (derived from former industrial laboratory technician course)
* [[Electronics]]
* [[Industrial Automation]]
* [[Informatics (academic field)|Informatics]] (derived from former data processing course)
* [[Internet]]-oriented Informatics
* [[Logistics]]
* [[Mechatronics]] (derived from former mechanics course)
* [[Office management]]
* [[Power electronics]] (derived from former electrotechnics course)

== Admission ==

Admission to the school involves an exam called ''vestibulinho'' ([[Portuguese language|Portuguese]] [[diminutive]] form of [[vestibular]], the most dominant admission test employed for those seeking to ingress in a [[college]] in Brazil). It is a [[Multiple choice|multiple choice test]] combined with a [[Composition (language)|written composition]].<ref name="CEETEPSV">{{cite web | url = http://www.centropaulasouza.sp.gov.br/Vestibulinho/ | title = Vestibulinho do Centro Paula Souza | trans_title = Centro Paula Souza's Vestibulinho webpage | accessdate = March 10, 2014 | language = Portuguese }}</ref>

== References ==

<!--- See [[Wikipedia:Footnotes]] on how to create references using<ref></ref> tags which will then appear here automatically -->
{{Reflist}}

== External links ==
* [http://www.centropaulasouza.sp.gov.br/ Centro Paula Souza's website]
* [http://www.etelg.com.br/ ETEC Lauro Gomes's website]

[[Category:Articles created via the Article Wizard]]
[[Category:Secondary schools in Brazil]]
[[Category:Vocational education]]
[[Category:Education in São Paulo (state)]]
[[Category:1957 establishments in Brazil]]