{{featured article}}
{{Use Australian English|date=May 2015}}

{{Use dmy dates|date=November 2014}}
{{infobox military unit
|unit_name=No. 450 Squadron RAAF
|image=[[File:450SqnRAAFCrest.png|140px|alt=Royal Australian Air Force crest depicting a jaguar's head pierced by a rapier; the motto beneath reads "Harass"]]
|caption=Official crest of No. 450 Squadron
|dates=1941–45
|country=Australia
|allegiance=
|branch=[[Royal Australian Air Force]]
|type=
|role=[[Fighter aircraft|Fighter]], [[fighter-bomber]]
|command_structure=[[Desert Air Force]]
|garrison=
|garrison_label=
|nickname="The Desert Harassers"{{sfn|Rawlings|1978|p=441}}
|patron=
|motto=''Harass''{{sfn|Rawlings|1978|p=441}}{{sfn|Halley|1988|p=473}}
|colors=
|colors_label=
|mascot=
|battles=World War II
|anniversaries=
|decorations=
|battle_honours=[[Syria-Lebanon campaign|Syria, 1941]]<br/>South-East Europe, 1942–1945<br/>[[Western Desert Campaign|Egypt & Libya, 1940–1943]]<br/>[[Second Battle of El Alamein|El Alamein]]<br/>[[Operation Pugilist|El Hamma]]<br/>[[North African Campaign|North Africa, 1942–1943]]<br/>[[Allied invasion of Sicily|Sicily, 1943]]<br/>[[Italian Campaign (World War II)|Italy, 1943–1945]]<br/>[[Gustav Line]]<br/>[[Gothic Line]]
|battle_honours_label=[[Battle honour]]s<ref name=AWM450Sqn/>
|disbanded=
|website=[https://web.archive.org/web/20150814171458/http://www.450squadron.org.au/ The Desert Harassers]
<!-- Commanders -->
|notable_commanders=[[Gordon Steege]] (1941–42)<br/>[[John Edwin Ashley Williams|John Williams]] (1942)
<!-- Insignia -->
|identification_symbol=
|identification_symbol_label=
|identification_symbol_2=A [[jaguar]]'s head couped, pierced by a rapier in hand{{sfn|Rawlings|1978|p=441}}{{sfn|Halley|1988|p=473}}
|identification_symbol_2_label=Squadron badge heraldry
|identification_symbol_3='''DJ''' (December 1941&nbsp;– April 1942){{sfn|Flintham|Thomas|2003|p=68}}<br/> '''OK''' (April 1942&nbsp;– August 1945){{sfn|Shores|Williams|1994|p=68}}
|identification_symbol_3_label=Squadron codes
<!-- Aircraft -->
|aircraft_fighter=[[Hawker Hurricane]]<br/>[[Curtiss P-40 Warhawk|Curtiss P-40 Kittyhawk]]<br/>[[North American P-51 Mustang]]
}}
'''No. 450 Squadron''' was a unit of the [[Royal Australian Air Force]] (RAAF) that operated during World War&nbsp;II. Established at [[RAAF Base Williamtown|RAAF Station Williamtown]], New South Wales, in February 1941, it was the first Australian [[Article XV squadron]] formed under the [[Empire Air Training Scheme]].

The squadron embarked for the Middle East in April 1941; its personnel initially consisted solely of ground crew, who joined the pilots of [[No. 260 Squadron RAF]] to form No. 260/450 Squadron, which briefly operated [[Hawker Hurricane]] fighters in [[Syria–Lebanon Campaign|Syria]]. It was not until February 1942 that No. 450 Squadron, now with its own pilots and equipped with [[Curtiss P-40 Warhawk|Curtiss P-40 Kittyhawk]] fighters, commenced operations in earnest. Over the next 15 months, it fought in the [[North Africa Campaign|North African]] and [[Tunisian Campaign]]s in both [[Fighter aircraft|fighter]] and [[fighter-bomber]] roles, claiming 49 German and Italian aircraft destroyed in the air and earning the nickname "The Desert Harassers".

Beginning in July 1943, No. 450 Squadron took part in the [[Allied invasion of Sicily]] and the [[Italian Campaign (World War II)|Italian campaign]], primarily in the [[Close air support|close support]] role. Its aircraft attacked targets in [[Yugoslavia]] as well as in Sicily and Italy. The squadron began converting from Kittyhawks to [[North American P-51 Mustang]] fighters in May 1945, but never saw action with its new aircraft. It was disbanded in August 1945 following the conclusion of hostilities, having suffered 63 fatal casualties during the war. Today, by agreement with the RAAF, the squadron's number is carried by a [[Royal Canadian Air Force]] unit, [[450 Tactical Helicopter Squadron]].

==History==
No. 450 Squadron was formed at [[RAAF Base Williamtown|RAAF Station Williamtown]], near [[Newcastle, New South Wales]], on 7 February 1941. Raised a week before [[No. 451 Squadron RAAF|No. 451 Squadron]], it was the first Australian squadron established for service with the British military under the [[Article XV squadron|Article XV]] of the [[Empire Air Training Scheme]] (EATS).{{sfn|Barnes|2000|pp=250, 255}}{{sfn|Eather|1995|pp=103, 105}} No. 450 Squadron was intended to be an "infiltration" squadron, which would consist initially only of ground crew and would receive a nucleus of experienced pilots after arriving in its designated theatre of operations.{{sfn|RAAF Historical Section|1995|p=105}}

Inaugurated at [[Ottawa]], Canada, in October 1939, EATS was a plan to expand the [[Royal Air Force]]'s (RAF) capacity to train aircrews by creating a pool of personnel from various Commonwealth countries—Australia, Canada, Britain and New Zealand—through the establishment of a common training system consisting of a series of initial, elementary, and advanced training schools. At the conclusion of advanced training, personnel were posted as required to RAF-controlled squadrons in Britain or the Middle East. These squadrons were designated as either RAF, RAAF, [[Royal Canadian Air Force]] or [[Royal New Zealand Air Force]] squadrons, but were paid for and administered by the British government, and personnel could be drawn from any Commonwealth nation. Seventeen RAAF squadrons were formed during the war under Article XV of the agreement.{{sfn|Gillison|1962|pp=79–89}}{{sfn|Barnes|2000|p=3}}

===Middle East and North Africa===
Under the temporary command of [[Flight Lieutenant]] Bruce Shepherd,{{sfn|Barnes|2000|p=254}}<ref>{{cite web |url=http://www.awm.gov.au/units/people_1077918.asp |publisher=Australian War Memorial |title=Squadron Leader Bruce McRae Shepherd |accessdate=22 October 2013}}</ref> No. 450 Squadron left Australia on 11 April 1941, embarking on the troopship [[RMS Queen Elizabeth|''Queen Elizabeth'']] at Sydney, and arriving in Egypt on 3 May.{{sfn|Barnes|2000|p=250}}{{sfn|RAAF Historical Section|1995|pp=105–106}} At [[Abu Suwayr Air Base|RAF Abu Sueir]], Squadron Leader [[Gordon Steege]] took command of the squadron before it was combined with the pilots and [[Hawker Hurricane]]s of [[No. 260 Squadron RAF]], which had been established without ground crew, to form an operational squadron.{{sfn|Barnes|2000|p=250}}{{sfn|Eather|1995|p=103}} The combined unit, known as No. 260/450 (Hurricane) Squadron, then relocated to [[Amman]] in Transjordan. Its first operation was on 29 June 1941, when the Hurricanes attacked [[Vichy France|Vichy French]] airfields and infrastructure during the [[Syria-Lebanon Campaign|invasion of Syria]].{{sfn|RAAF Historical Section|1995|p=106}} No. 260/450 Squadron operated for ten days only and flew 61 sorties against airfields, 20 on offensive patrols and six on bomber-escort duties during the Syrian campaign.{{sfn|Herington|1954|p=95}}

In August 1941, No. 450 Squadron personnel were separated from No. 260 Squadron, when the latter received its own ground crew. No. 450 Squadron moved to [[Rayak Air Base|Rayak airfield]], Lebanon, where it was allocated Hurricanes and [[Miles Magister]] trainers.{{sfn|Barnes|2000|p=250}} A batch of 20 trainee Australian, British and Canadian pilots—the majority being Australian—were posted to the squadron in early October, and it began duties as an Operational Training Unit.{{sfn|Eather|1995|p=103}}{{sfn|Barnes|2000|pp=250–251}} A fortnight later these pilots were posted out and, lacking trained pilots, the squadron's aircraft were re-allocated. On 20 October, the squadron moved to [[Borg El Arab Airport|Burg El Arab]], Egypt, and began operating as an advanced repair, salvage and service unit, taking part in the [[North African Campaign]].<ref name=AWM450Sqn/>{{sfn|Barnes|2000|p=251}}

[[File:450 Sqn (AWM 024694).jpg|upright=1.2|thumb|left|Kittyhawks of No. 450 Squadron in the Western Desert, North Africa, August 1942|alt=Fighter aircraft parked in the desert]]
By December 1941, the squadron was receiving pilots and it began taking delivery of [[Curtiss P-40 Warhawk|Curtiss P-40 Kittyhawk]] fighters.{{sfn|Barnes|2000|p=251}}{{sfn|Herington|1954|p=218}} On 19 December, RAF Middle East Command issued an administrative instruction declaring that although manned primarily by Australians, Nos. 450 and 451 Squadrons were "paid by and loaned to the Royal Air Force under the Empire Air Training Scheme and for all practical purposes they should be regarded as R.A.F. squadrons in every way". Confusion reigned for a time, but after intervention by the British [[Air Ministry]], a further communique on 23 January 1942 announced that "450 and 451 E.A.T.S. Squadrons are to be regarded as R.A.A.F. squadrons".{{sfn|Herington|1954|pp=120–121}} Training began the same month, and No. 450 Squadron commenced operations from [[RAF Gambut]] on 19 February 1942, with an uneventful patrol near [[Tobruk]]. Three days later Sergeant Raymond Shaw became the first pilot from the squadron to claim an aerial victory, after he intercepted a [[Junkers Ju 88]] near [[Gazala]].{{sfn|Eather|1995|p=103}}{{sfn|RAAF Historical Section|1995|p=106}}

The squadron became part of the [[Desert Air Force]]'s newly formed No. 239 Wing on 1 March 1942, serving alongside one Australian squadron, [[No. 3 Squadron RAAF|No. 3]], and two RAF squadrons, [[No. 112 Squadron RAF|Nos. 112]] and [[No. 250 Squadron RAF|250 Squadrons]].{{sfn|Thomas|2005|p=46}} No. 450 Squadron's main roles—escorting daylight raids by [[Douglas A-20 Havoc|Douglas Boston]] bombers, and ground-attack missions in support of the [[Eighth Army (United Kingdom)|Eighth Army]]—were hazardous and resulted in relatively heavy losses.{{sfn|Brown|1983|p=259}} From 26 May, as [[Erwin Rommel|Rommel]] launched an [[Battle of Gazala|assault on the Gazala–Bir Hacheim line]], all Kittyhawk units began to focus on the [[fighter-bomber]] role rather than air-to-air combat, to support retreating Commonwealth forces.{{sfn|Brown|1983|p=115}}{{sfn|Shores|Ring|1969|pp=114–115}} On 29 May, No. 450 Squadron claimed two [[Junkers Ju 87]]s and a [[Messerschmitt Bf 109]], for the loss of three pilots killed, including Shaw.{{sfn|Shores|Ring|1969|p=116}}{{sfn|Brown|1983|pp=117–118}} Flight Sergeant [[Don McBurnie]], the squadron's highest-scoring pilot with five solo victories and one shared, claimed his final "kill" on 4 July 1942 when he shot a [[Messerschmitt Bf 110]] into the sea following a bombing mission on airfields west of [[RAF El Daba|Daba]].{{sfn|Brown|1983|pp=139–140, 263}}{{sfn|Shores|Ring|1969|pp=140, 240}}

[[File:P03372.011 kittybomber.jpg|thumb|right|upright=1.2|left|Kittyhawk of No.&nbsp;450 Squadron, loaded with six {{convert|250|lb|kg|abbr=on}} bombs, in North Africa, c. 1943|alt=Low-angle front view of single-engined military aircraft with three-bladed propeller and six bombs beneath the wings and fuselage]]
No. 450 Squadron took part in the decisive [[Second Battle of El Alamein]], during October and November 1942, attacking enemy airfields and claiming three German and Italian fighters destroyed in the air.{{sfn|RAAF Historical Section|1995|p=108}}  It suffered several losses during this time, including one of its leading scorers, Squadron Leader [[John Edwin Ashley Williams|John Williams]], who was shot down and taken prisoner on 31 October 1942, three days after he had been appointed commanding officer.<ref name=AWMWilliams>{{cite web |url=http://www.awm.gov.au/units/people_1077919.asp |publisher=Australian War Memorial |title=Squadron Leader John Edwin Ashley Williams, DFC |accessdate=6 February 2008}}</ref>{{sfn|Barnes|2000|pp=251–252}} The squadron was frequently on the move as the Allies advanced following Second El Alamein, changing locations six times during November.{{sfn|Eather|1995|p=103}}{{sfn|RAAF Historical Section|1995|p=108}} It often found itself using captured or hastily constructed airfields; one Kittyhawk was destroyed and several ground personnel killed or wounded by [[land mine]]s at [[Marble Arch (Libya)|Marble Arch]], Libya, in December 1942.{{sfn|Eather|1995|pp=103–104}}<ref>{{cite web |url=http://www.raaf.gov.au/raafmuseum/research/units/450sqn.htm |archiveurl=https://web.archive.org/web/20091001224431/http://www.raaf.gov.au/raafmuseum/research/units/450sqn.htm |title= No 450 Squadron |publisher=RAAF Museum Point Cook |archivedate=1 October 2009 |accessdate=13 October 2013}}</ref>

From late 1942, No. 450 Squadron was engaged in the [[Tunisian Campaign]]. By March 1943, as the Allied forces advanced, the squadron was operating from Medinne along the [[Mareth Line]]. That month it flew over 300 sorties. Further moves occurred, and by mid-April it was based at Karouan. Throughout April and early May, 350 sorties were flown, including attacks on Axis shipping in [[Cape Bon]] and in the [[Gulf of Tunis]].{{sfn|Barnes|2000|p=252}} The campaign came to an end in mid-May,{{sfn|Eather|1995|p=104}} but the squadron continued defensive patrols into June.{{sfn|Barnes|2000|p=252}}<!--During the North African campaign the squadron was credited with 39 aircraft destroyed, 18 probables, and a further seven damaged, for the loss of 33 personnel killed.{{sfn|RAAF Historical Section|1995|p=109}}--> Between February 1942 and May 1943, it claimed 49 German and Italian aircraft destroyed in aerial combat, for the loss of 31 pilots, four to accidents.{{sfn|Brown|1983|pp=293–294, 299–300}} As a result of its involvement in the North African fighting, the squadron received the nickname, "The Desert Harassers",{{sfn|Eather|1995|p=103}} and adopted the motto, "Harass", both of which were derived from a comment by the [[Nazi Germany|Nazi]] propaganda broadcaster "[[Lord Haw Haw]]", who described the unit as "Australian [[mercenary|mercenaries]] whose harassing tactics were easily beaten off by the ''[[Luftwaffe]]''."<ref name=AWM450Sqn>{{cite web |url=http://www.awm.gov.au/units/unit_11143.asp|archiveurl=https://web.archive.org/web/20131031204727/http://www.awm.gov.au/units/unit_11143.asp |publisher=Australian War Memorial |title=450 Squadron RAAF |work=Second World War, 1939–1945 units |accessdate=13 October 2013|archivedate=31 October 2013}}</ref>

===Europe===

Following the conclusion of the fighting in the desert, No. 450 Squadron was allocated a ground-attack role during the [[Allied invasion of Sicily]]. Moving to Malta on 13 July 1943,{{sfn|Eather|1995|p=104}} the squadron staged out of [[RAF Luqa]], and undertook its first attack in [[Sicily]] against Carlentini. Four days later, on 17 July, No. 450 Squadron relocated to Pachino, Sicily, from where it continued ground-attack missions. A further move came on 1 August, when Nos. 450 and 3 Squadrons relocated to Agnone, near [[Catania]], where they commenced [[close air support]] operations on 11 August, working closely with Allied ground units around [[Mount Etna]].{{sfn|Barnes|2000|p=252}} On the night of 11 August, the airfield was attacked by Ju 88 bombers, which dropped [[Incendiary ammunition|incendiary]], anti-personnel and high-explosive bombs for more than an hour. No. 450 Squadron's personnel had been located some distance from the airfield and only one Australian was wounded, although casualties among other units amounted to twelve killed and 60 wounded.{{sfn|Turner|1999|p=88}} Eighteen RAAF Kittyhawks were destroyed, including eleven belonging to No. 450 Squadron.{{sfn|RAAF Historical Section|1995|p=109}}{{sfn|Wilson|2005|pp=100–101}} Despite this, the two RAAF squadrons flew 22 sorties the following day.{{sfn|Herington|1954|p=578}}

During the early stages of the [[Italian Campaign (World War II)|Allied campaign on the Italian mainland]], which commenced in early September 1943, the squadron undertook bomber escort missions in support of the Eighth Army's landings. In the middle of September, it reverted to the close air support role, operating from [[Taranto-Grottaglie Airport|Grottaglie]], although it also undertook anti-shipping operations, including an attack on [[Manfredonia]] on 21 September, during which its aircraft sunk two vessels.{{sfn|Barnes|2000|p=253}} The following month, No. 450 Squadron was transferred to [[Foggia]], and then to Mileni, where it was briefly withdrawn from operations to convert to newer model Kittyhawk IVs before rejoining the campaign in late November.{{sfn|Barnes|2000|p=253}}  In December, the squadron moved to [[Cutella]], near [[Termoli]], on the central [[Adriatic Sea|Adriatic]] coast of Italy. There it encountered problems with severe winter weather restricting operations. Cutella airfield was located close to the beach; heavy rains caused a [[storm surge]] on 1 January 1944 and flooded the airfield, damaging some aircraft.{{sfn|Herington|1963|p=70}}

[[File:Royal Air Force- Italy, the Balkans and South-east Europe, 1942-1945. CNA3493.jpg|thumb|left|upright=1.2|Kittyhawk of No. 450 Squadron at Cervia, Italy, where it operated in early 1945|alt=Single-engined fighter plane on airfield with propeller spinning and man sitting on left wing]]
Meanwhile, Williams and another [[prisoner of war]] from No. 450 Squadron, Flight Lieutenant Reginald Kierath, were incarcerated with other Allied POWs at ''[[Stalag Luft III]]'', in eastern Germany. In March 1944, both took part in "[[Stalag Luft III#The .22Great Escape.22The Great Escape|The Great Escape]]" and were among 50 POWs murdered by the ''[[Gestapo]]'', after being recaptured.<ref name=Edlington>{{cite news|url=http://www.defence.gov.au/news/raafnews/EDITIONS/4605/history/story01.htm |author=Edlington, David |title=The Great Crime: Aussies Among Murder Victims |newspaper=Air Force News |volume=46 |issue=5 |date=8 April 2004 |accessdate=6 February 2008}}</ref> Williams, who was 27 years old and from Sydney, was officially an RAF officer, as he had joined the British service under a [[Officer (armed forces)#Commissioned officers|short service commission]] in 1938.<ref name=Edlington/><ref name=Gazette>{{LondonGazette|issue=34501|startpage=2458|date=12 April 1938|accessdate=8 February 2008}}</ref> Kierath, who was 29 and from [[Narromine, New South Wales]], was an RAAF officer.<ref>{{cite web |url=http://www.ww2roll.gov.au/Veteran.aspx?ServiceId=R&VeteranId=1053367 |work=World War II Nominal Roll |title=Kierath, Reginald Victor |publisher=Commonwealth of Australia |accessdate=6 February 2008}}</ref>

Throughout January 1944, No. 450 Squadron undertook anti-shipping operations as well as ground support tasks. It flew missions against targets off [[Dalmatia]] and in the harbour at [[Sibenik]], as well as around the ports of [[Velaluka]] and Zera. In March, the squadron's attention returned to Italy, launching strikes against rolling stock; the following month saw it heavily tasked, flying a total of 430 sorties.{{sfn|Barnes|2000|p=252}} On 29 April 1944, a USAAF [[Republic P-47 Thunderbolt]] pilot strafed Cutella by mistake. No. 450 Squadron suffered no fatalities or aircraft destroyed but the pilot of a [[seaplane|float plane]] belonging to an [[Search and rescue|air-sea rescue]] unit was killed, some ground personnel were wounded, a Kittyhawk of No. 3 Squadron was destroyed and several others were damaged.{{sfn|Franks|2003|p=108}}{{sfn|Herington|1963|p=111}} The following month, No. 450 Squadron moved to San Angelo, mounting a series of attacks against a 200-vehicle convoy near [[Subiaco, Lazio|Subiaco]] in concert with other Kittyhawk squadrons that claimed 123 vehicles destroyed or damaged.{{sfn|Barnes|2000|p=253}}{{sfn|RAAF Historical Section|1995|p=110}} No. 450 Squadron later operated from several airfields in central and northern Italy, under the "[[Taxicab stand|cab rank]]" system, whereby patrolling fighter-bombers would attack as requested by army [[Forward air control|air liaison officer]]s.<ref name=AWM450Sqn/>{{sfn|Herington|1963 |pp=344–345}} It flew over 1,100 sorties in June and July. The Australian Kittyhawk units were regularly lauded for the accuracy of their assaults; following a mission by No. 450 Squadron on 12 July, the Eighth Army wired No. 239 Wing headquarters: "Excellent bombing. Good show and thank you. No further attacks required."{{sfn|Herington|1963 |p=345}}

[[File:AWM MEB0301 No 450 Squadron RAAF group portrait Italy May 1945.jpg|thumb|right|upright=1.2|Personnel of No. 450 Squadron, May 1945|alt=A group of uniformed men posing in front of a single-engined fighter plane]]
No. 450 Squadron took part in the major [[Gothic Line|offensive against the Gothic Line]] in August–September 1944.{{sfn|Herington|1963|pp=346–349}} Its first attack in early August was a strike on an artillery battery, during which three Kittyhawks were shot down; subsequent attacks throughout the following months were made against various targets including rolling stock, armour, and troop concentrations.{{sfn|Barnes|2000|p=253}} From November, after it had moved to Fano on the Italian north-east coast, the squadron also began attacking German forces in [[Yugoslavia]].{{sfn|Herington|1963|p=353}} Its average complement of pilots during the second half of 1944 was 25 from the RAAF, seven from the RAF and five from the [[South African Air Force]].{{sfn|Herington|1963 |p=350}} No. 450 Squadron commenced operations from [[Cervia]] in February 1945; that month, it lost three pilots to prematurely detonating bombs.{{sfn|Eather|1995|p=104}}{{sfn|RAAF Historical Section|1995|pp=110–111}} On 21 March, it took part in [[Operation Bowler]], a major air raid on [[Venice]] harbour.{{sfn|Barnes|2000|p=253}} The attack resulted in the sinking of a [[merchant ship]], a torpedo boat, and a coastal steamer, as well as the destruction of five warehouses and other harbour infrastructure.{{sfn|RAAF Historical Section|1995|p=111}}

In May 1945, following the end of the war in Europe, No. 450 Squadron transferred to [[Lavariano]], a few miles south of [[Udine]] in north-eastern Italy.{{sfn|Barnes|2000|p=254}}{{sfn|RAAF Historical Section|1995|p=111}} It also began replacing its Kittyhawks with [[North American P-51 Mustang]]s.<ref name=AWM450Sqn/>{{sfn|Eather|1995|p=104}} The squadron was disbanded at Lavariano on 20 August 1945.{{sfn|Barnes|2000|p=254}}{{sfn|RAAF Historical Section|1995|p=111}} During the war, it had lost 63 personnel killed in action, of whom 49 were Australian.<ref name=AWM450Sqn/>

==Legacy==
The squadron was not re-raised by the RAAF after the war, although the numerical designation of "450" was assumed by a Canadian helicopter unit, [[450 Tactical Helicopter Squadron]], in March 1968. The use of the "450" designation was the result of an administrative error, as the Canadian 400 series squadrons formed during World War II had been numbered between 400 and 449. An agreement was subsequently reached between the RCAF and RAAF and the squadron kept the designation. It is based at [[Petawawa]], in Ontario, and operates [[Boeing CH-47 Chinook]] helicopters.<ref>{{cite web |url=http://www.rcaf-arc.forces.gc.ca/en/1-wing/450-squadron.page |title=450 Tactical Helicopter Squadron |publisher=Royal Canadian Air Force |accessdate=8 December 2014}}</ref><ref>{{cite web |url=http://www.rcaf-arc.forces.gc.ca/en/article-template-standard.page?doc=450-tactical-helicopter-squadron-reborn/hky1djyr |archiveurl=https://web.archive.org/web/20160304032558/http://www.rcaf-arc.forces.gc.ca/en/article-template-standard.page?doc=450-tactical-helicopter-squadron-reborn/hky1djyr |archivedate=4 March 2016 |title=450 Tactical Helicopter Squadron reborn |date=18 May 2012 |publisher=Royal Canadian Air Force |accessdate=8 December 2014}}</ref>

Gordon Steege, No. 450 Squadron RAAF's first commanding officer, became patron of the squadron association in April 2008; he died in September 2013, aged 95.<ref>{{cite journal|url=http://450squadronraaf.smugmug.com/gallery/5143404_SvnF4#!i=1161599204&k=vtv4tSV&lb=1&s=A |archiveurl=http://wayback.archive.org/web/20110815104255/http://450squadronraaf.smugmug.com/gallery/5143404_SvnF4 |archivedate=15 August 2011 | title=First commanding officer&nbsp;– now 450's patron|work=The Harasser|date=November 2009|page=1|accessdate=5 September 2013}}</ref><ref>{{cite news|url=http://tributes.smh.com.au/obituaries/smh-au/obituary.aspx?n=gordon-steege&pid=166778795#fbLoggedOut|title=Tributes and celebrations: Gordon Steege|work=[[The Sydney Morning Herald]]|date=4 September 2013|accessdate=24 November 2014}}</ref>

==Aircraft operated==
[[File:Royal Air Force- Italy, the Balkans and South-east Europe, 1942-1945. CNA3543.jpg|thumb|right|upright=1.2|Kittyhawk Mk.IV of No. 450 Squadron at Cervia, Italy, after being hit by anti-aircraft fire in 1945|alt=Damaged tail assembly of a single-engined fighter plane]]
No. 450 Squadron operated the following aircraft:{{sfn|Rawlings|1978|p=442}}{{sfn|Halley|1988|p=474}}{{sfn|Jefford|2001|p=94}}
{|class="wikitable"
|+
! From !! To !! Aircraft !! Mark
|-
| May 1941 || December 1941 || [[Hawker Hurricane]] || Mk.I
|-
| December 1941 || September 1942 || [[Curtiss P-40 Warhawk|Curtiss P-40 Kittyhawk]] || Mk.I
|-
| 1942 || September 1942 || Curtiss P-40 Kittyhawk || Mk.Ia
|-
| September 1942 || October 1943 || Curtiss P-40 Kittyhawk || Mk.III
|-
| October 1943 || August 1945 || Curtiss P-40 Kittyhawk || Mk.IV
|-
| May 1945 || August 1945 || [[North American P-51 Mustang]] || Mk.III
|}

==Squadron bases==
[[File:450 Squadron RAAF Kittyhawks in Malta 1943 AWM SUK11288.jpg|thumb|right|upright=1.2|Ground crews of No. 450 Squadron at its base in Malta preparing Kittyhawk fighter-bombers for operations during the invasion of Sicily, 1943|alt=Several men in shorts working on a single-engined fighter plane; two other fighters are parked a short distance away]]
No. 450 Squadron operated from the following bases and airfields:{{sfn|Rawlings|1978|p=442}}{{sfn|Jefford|2001|p=94}}{{sfn|Halley|1988|pp=473–474}}
{|class="wikitable"
|+
! From !! To !! Base !! Remark
|-
| 16 February 1941 || 9 April 1941 || [[RAAF Base Williamtown|RAAF Station Williamtown]], New South Wales ||
|-
| 9 April 1941 || 12 May 1941 || en route to [[Middle East]] ||
|-
| 12 May 1941 || 23 June 1941 || [[Abu Suwayr Air Base|RAF Abu Sueir]], Egypt ||
|-
| 23 June 1941 || 29 June 1941 || [[RAF Aqir]], Palestine ||
|-
| 29 June 1941 || 11 July 1941 || [[Amman|RAF Amman]], Jordan ||
|-
| 11 July 1941 || 18 July 1941 || [[Damascus]], Syria ||
|-
| 18 July 1941 || 4 August 1941 || [[RAF Haifa]], Palestine ||
|-
| 4 August 1941 || 19 August 1941 || [[Al-Bassa|RAF El Bassa]], Palestine ||
|-
| 19 August 1941 || 25 October 1941 || [[Rayak Air Base|Rayak airfield]], Lebanon ||
|-
| 25 October 1941 || 12 December 1941 || [[Borg El Arab Airport|RAF Burg El Arab]], Egypt ||
|-
| 12 December 1941 || 30 January 1942 || LG.207/LG 'Y' (Qassassin), Egypt ||
|-
| 30 January 1942 || 16 February 1942 || [[Sidi Haneish Airfield|LG.12 (Sidi Haneish North)]], Egypt ||
|-
| 16 February 1942 || 22 February 1942 || [[RAF Gambut|LG.139 (Gambut Main)]], Libya || Det. at [[Gamal Abdul El Nasser Air Base|RAF El Adem]], Libya
|-
| 22 February 1942 || 9 March 1942 || [[RAF Gambut|LG.142/143 (Gambut Satellite)]], Libya ||
|-
| 9 March 1942 || 16 April 1942 || LG.139 (Gambut Main), Libya ||
|-
| 16 April 1942 || 17 June 1942 || LG.142/143 (Gambut Satellite), Libya ||
|-
| 17 June 1942 || 18 June 1942 || [[Sidi Azeiz Airfield|LG.148/Sidi Azeiz Airfield]], Libya ||
|-
| 18 June 1942 || 24 June 1942 || LG.75, Egypt ||
|-
| 24 June 1942 || 27 June 1942 || LG.102, Egypt ||
|-
| 27 June 1942 || 30 June 1942 || LG.106, Egypt ||
|-
| 30 June 1942 || 2 October 1942 || LG.91, Egypt ||
|-
| 2 October 1942 || 14 October 1942 || LG.224/Cairo West, Egypt ||
|-
| 14 October 1942 || 6 November 1942 || LG.175, Egypt ||
|-
| 6 November 1942 || 9 November 1942 || LG.106, Egypt ||
|-
| 9 November 1942 || 11 November 1942 || LG.101, Egypt ||
|-
| 11 November 1942 || 14 November 1942 || LG.76, Egypt ||
|-
| 14 November 1942 || 15 November 1942 || [[RAF Gambut|LG.139 (Gambut 1)]], Libya ||
|-
| 15 November 1942 || 19 November 1942 || [[Gazala|Gazala Airfield]], Libya ||
|-
| 19 November 1942 || 8 December 1942 || [[Martuba Air Base|Martuba Airfield]], Libya || Det. at Antelat Airfield, Libya
|-
| 8 December 1942 || 18 December 1942 || [[Belandah Airfield]], Libya ||
|-
| 18 December 1942 || 1 January 1943 || Marble Arch Airfield, Libya ||
|-
| 1 January 1943 || 9 January 1943 || Alem el Chel Airfield,  Libya ||
|-
| 9 January 1943 || 18 January 1943 || [[Ras Lanuf Airport|Hamraiet 3 Airfield]], Libya ||
|-
| 18 January 1943 || 24 January 1943 || Sedadah Airfield, Libya ||
|-
| 24 January 1943 || 14 February 1943 || [[RAF Castel Benito]], Libya ||
|-
| 14 February 1943 || 8 March 1943 || [[El Assa Airfield]], Libya || Det. at [[Ben Gardane Airfield]], Tunisia
|-
| 8 March 1943 || 21 March 1943 || Nefatia Airfield, Tunisia ||
|-
| 21 March 1943 || 6 April 1943 || [[Medenine|Medenine Airfield]], Tunisia ||
|-
| 6 April 1943 || 14 April 1943 || [[El Hamma|El Hamma Airfield]], Tunisia ||
|-
| 14 April 1943 || 18 April 1943 || [[El Djem Airfield]], Tunisia ||
|-
| 18 April 1943 || 18 May 1943 || Alem East Airfield, Tunisia ||
|-
| 18 May 1943 || 13 July 1943 || [[Zuwara|Zuwara Airfield]], Libya ||
|-
| 13 July 1943 || 18 July 1943 || [[RAF Luqa]], Malta ||
|-
| 18 July 1943 || 2 August 1943 || Pachino Airfield, Sicily, Italy ||
|-
| 2 August 1943 || 16 September 1943 || Agnone Airfield, Sicily, Italy ||
|-
| 16 September 1943 || 23 September 1943 || [[Taranto-Grottaglie Airport|Grottaglie Airfield]], Italy ||
|-
| 23 September 1943 || 3 October 1943 || [[Foggia Airfield Complex|Bari Airfield]], Italy ||
|-
| 3 October 1943 || 27 October 1943 || [[Foggia Airfield Complex|Foggia Main Airfield]], Italy ||
|-
| 27 October 1943 || 28 December 1943 || Mileni Airfield, Italy ||
|-
| 28 December 1943 || 22 May 1944 || Cutella Airfield, Italy ||
|-
| 22 May 1944 || 12 June 1944 || San Angelo Airfield, Italy ||
|-
| 12 June 1944 || 23 June 1944 || [[Guidonia Montecelio|Guidonia Airfield]], Italy ||
|-
| 23 June 1944 || 9 July 1944 || Falerium Airfield, Italy ||
|-
| 9 July 1944 || 28 August 1944 || Creti Airfield, Italy ||
|-
| 28 August 1944 || 11 September 1944 || [[Iesi Airfield]], Italy ||
|-
| 11 September 1944 || 20 September 1944 || [[Foiano della Chiana|Foiano Airfield]], Italy ||
|-
| 20 September 1944 || 17 November 1944 || Iesi Airfield, Italy ||
|-
| 17 November 1944 || 25 February 1945 || [[Fano Airport|Fano Airfield]], Italy ||
|-
| 25 February 1945 || 19 May 1945 || [[Cervia Air Force Base|Cervia Airfield]], Italy ||
|-
| 19 May 1945 || 20 August 1945 || Lavariano, Italy ||
|}

==Commanding officers==
[[File:450SqnRAAFCrest (AWM MEB0284).JPG|thumb|upright|Original crest design by No.&nbsp;450 Squadron ground gunner, {{nowrap|Les Rex}}, July 1943<ref>{{cite web |url=https://www.awm.gov.au/collection/MEB0284/ |title=Item MEB0284 |work=Collection |publisher=Australian War Memorial |accessdate=26 July 2015}}</ref> |alt=Drawing of a Royal Australian Air Force crest depicting a jaguar's head pierced by a rapier; the motto beneath reads "Harass"]]
No. 450 Squadron was commanded by the following officers:{{sfn|Barnes|2000|p=264}}{{sfn|RAAF Historical Section|1995|pp=107, 111}}
{|class="wikitable"
|+
! From !! Name
|-
| 25 March 1941  || [[Flight Lieutenant]] Bruce McRae Shepherd (temp)
|-
| 31 May 1941 || [[Squadron Leader]] [[Gordon Steege|Gordon Henry Steege]]
|-
| 7 May 1942 || Squadron Leader Alan Douglas Ferguson
|-
| 18 October 1942   || Squadron Leader [[John Edwin Ashley Williams]]
|-
| 2 November 1942   || Squadron Leader M.H.C. Barber
|-
| 16 March 1943  || Squadron Leader John Phillip Bartle
|-
| 6 November 1943  || Squadron Leader Sydney George Welshman
|-
| 6 December 1943 || Squadron Leader Kenneth Royce Sands
|-
| 7 April 1944   || Squadron Leader Ray Trevor Hudson
|-
| 15 June 1944   || Squadron Leader John Dennis Gleeson
|-
| 25 October 1944  || Squadron Leader Jack Carlisle Doyle

|}

==See also==
*[[List of Royal Air Force aircraft squadrons#Article XV squadrons of World War II|RAAF units under RAF operational control]]

==References==

===Notes===
{{Reflist|30em}}

===Bibliography===
{{refbegin}}
* {{cite book |last=Barnes |first=Norman |title=The RAAF and the Flying Squadrons |year=2000 |location=St Leonards, New South Wales |publisher=Allen & Unwin |isbn=1-86508-130-2 |ref=harv}}
<!--* {{cite book |last1=Bowyer |first1= Michael J.F. |first2=John D.R. |last2=Rawlings |title=Squadron Codes, 1937–56 |location=Cambridge, UK |publisher=Patrick Stephens |year=1979 |isbn=0-85059-364-6 |ref=harv}}-->
* {{cite book |last=Brown |first=Russell |title=Desert Warriors: Australian P-40 Pilots at War in the Middle East and North Africa, 1941–1943 |location=Maryborough, Queensland |publisher= Banner Books |year=1983 |isbn=1-875593-22-5|ref=harv}}
* {{cite book|last=Eather|first=Steve|title=Flying Squadrons of the Australian Defence Force|publisher=Aerospace Publications|location=Weston Creek, Australian Capital Territory|year=1995|isbn=1-875671-15-3|ref=harv}}
* {{cite book |last1=Flintham |first1=Vic |first2=Andrew |last2=Thomas |title=Combat Codes: A Full Explanation and Listing of British, Commonwealth and Allied Air Force Unit Codes Since 1938 |location=Shrewsbury, UK |publisher=Airlife Publishing |year=2003 |isbn=1-84037-281-8 |ref=harv}}
* {{cite book |last=Franks |first=Norman |authorlink=Norman Franks |title=Beyond Courage: Air Sea Rescue by Walrus Squadrons in the Adriatic, Mediterranean and Tyrrhenian Seas 1942–1945 |location=London |publisher=Grub Street|year=2003|isbn=9781904010302|ref=harv}}
* {{cite book |last=Gillison |first= Douglas |year=1962 |url=https://www.awm.gov.au/collection/RCDIG1070209/ |series=Australia in the War of 1939–1945. Series 3&nbsp;– Air. |volume=I |title=Royal Australian Air Force, 1939–1942 |edition=1st |publisher=Australian War Memorial |location=Canberra |oclc=2000369 |ref=harv}}
* {{cite book |last=Halley |first=James J. |title=The Squadrons of the Royal Air Force & Commonwealth 1918–1988 |location=Tonbridge, UK |publisher=Air Britain (Historians) |year=1988 |isbn=0-85130-164-9|ref=harv}}
* {{cite book |last=Herington |first=John |title=Air War Against Germany and Italy, 1939–1943 |volume=III |series=Australia in the War of 1939–1945. Series 3&nbsp;– Air. |location=Canberra |publisher=Australian War Memorial |year=1954  |edition=1st |url=https://www.awm.gov.au/collection/RCDIG1070211/|oclc=3633363 |ref=harv }}
* {{cite book |last=Herington |first=John |title=Air Power Over Europe, 1944–1945 |volume=IV |series=Australia in the War of 1939–1945. Series 3&nbsp;– Air. |location=Canberra |publisher= Australian War Memorial |year=1963 |edition=1st |url=https://www.awm.gov.au/collection/RCDIG1070212/|oclc=3633419 |ref=harv}}
* {{cite book |last=Jefford |first=C.G. |title=RAF Squadrons: A Comprehensive Record of the Movement and Equipment of all RAF Squadrons and Their Antecedents Since 1912 |location= Shrewsbury, UK |publisher=Airlife Publishing |origyear=1988 |year=2001 |edition=2nd |isbn=1-85310-053-6|ref=harv}}
* {{cite book|last=RAAF Historical Section|title=Units of the Royal Australian Air Force: A Concise History |volume=Volume 2: Fighter Units|publisher=Australian Government Publishing Service|location=Canberra|year=1995|isbn=9780644427944|ref=harv}}
* {{cite book |last=Rawlings |first=John D. R. |title=Fighter Squadrons of the RAF and Their Aircraft |location=London |publisher=Macdonald & Jane's |origyear=1976 |year=1978 |edition=2nd |isbn=0-354-01028-X |ref=harv}}
* {{cite book|last1=Shores|first1=Christopher|last2=Ring|first2=Hans|title=Fighters Over the Desert: The Air Battles in the Western Desert June 1940 to December 1942|publisher=Neville Spearman|location=London|year=1969|oclc= 164897156|ref=harv}}
* {{cite book|last1=Shores| first1=Christopher|last2=Williams|first2= Clive|year=1994|origyear=1966| title=Aces High: A Tribute to the Most Notable Fighter Pilots of the British and Commonwealth Air Forces in World War II |location=London|publisher= Grub Street |isbn=1-898697-00-0|ref=harv}}
* {{cite book|last=Thomas|first=Andrew|title=Tomahawk and Kittyhawk Aces of the RAF and Commonwealth|publisher=Osprey|location=Oxford|year=2005|isbn=978-1-84176-083-4|ref=harv}}
* {{cite book |last=Turner |first=Jim |title=The RAAF at War: World War II, Korea, Malaya & Vietnam |year=1999 |publisher=Kangaroo Press |location=East Roseville, New South Wales |isbn=0-86417-889-1 |ref=harv}}
* {{cite book |last=Wilson |first=David |title=Brotherhood of Airmen: The Men and Women of the RAAF in Action |location=Crows Nest, New South Wales |publisher=Allen & Unwin |year= 2005 |isbn=1-74114-333-0 |ref=harv}}
{{refend}}

==Further reading==
{{Commons category|No. 450 Squadron RAAF}}
{{refbegin}}
* {{cite book |last=Barton |first=Leonard L. |title=The Desert Harassers: Being Memoirs of 450 (RAAF) Squadron 1941–1945 |location=Mosman, New South Wales |publisher=Astor |year= 1991|isbn=9780646034829}}
* {{cite book |last=James |first=George A. (ed.) |title=OK: Recollections of the Desert Harassers |location=Illawong, New South Wales |publisher=450 Squadron (RAAF) Association|year= 1996|isbn=9780646278636}}
{{refend}}

{{List of RAAF Squadrons}}

{{DEFAULTSORT:No. 450 Squadron Raaf}}
[[Category:RAAF squadrons|450 Squadron]]
[[Category:Military units and formations established in 1941]]
[[Category:Military units and formations disestablished in 1945]]
[[Category:Military units and formations of the Royal Australian Air Force in World War II]]
[[Category:Article XV squadrons of World War II]]
[[Category:Military units and formations in Mandatory Palestine in World War II]]