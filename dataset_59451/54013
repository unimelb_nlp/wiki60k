{{Use dmy dates|date=September 2016}}
{{Use Australian English|date=September 2016}}
{{Italic title}}
'''''Adelaide Punch''''' (1878–1884) was a short-lived humorous and satirical magazine published in [[Adelaide, South Australia]]. Like the ''[[Melbourne Punch]]'', it was modelled on ''[[Punch (magazine)|Punch]]'' of London.

==History==
The Adelaide Punch had its origin in ''The Rattlesnake or Adelaide Punch'', a fortnightly magazine first published by Haddrick and East on Thursday 24 January 1878. It failed after a few issues, leaving the printers Scrymgour and Sons holding a debt of around £100.

Rather than writing it off, they decided to persevere with its production, and momentarily interested D. W. Melville, at one time with the ''[[South Australian Register|Register]]'', to act as managing editor, but he found it interfered with his lucrative auctioneering business, so they settled on [[J. C. F. Johnson]], of the ''Register'' (later M.P. for [[Electoral district of Onkaparinga|Onkaparinga]]). His team included D. M. "Dan" Magill (ca.1845 – 3 April 1916), also ex-''Register''; William John Kennedy (1848–1894), headmaster of [[Mount Gambier, South Australia|Mount Gambier]] and [[Hindmarsh, South Australia|Hindmarsh]] schools, as cartoonist (also associated with ''[[Quiz (Adelaide newspaper)|Quiz]]'' magazine); and [[C. R. Wilton]], then a promising cadet.

The magazine was a considerable success, and went weekly in September 1878.<ref>{{cite news |url=http://nla.gov.au/nla.news-article42994725 |title=Advertising. |newspaper=[[South Australian Register]] |location=Adelaide |date=7 September 1878 |accessdate=5 January 2015 |page=2 |publisher=National Library of Australia}}</ref> Johnson purchased a half-share from Scrymgour in December that year and assumed the role of managing editor,<ref>{{cite news |url=http://nla.gov.au/nla.news-article42984369 |title=Presentation to Mr. J. C. F. Johnson |newspaper=[[South Australian Register]] |location=Adelaide |date=2 December 1878 |accessdate=5 January 2015 |page=5 |publisher=National Library of Australia}}</ref> and in 1879 became sole proprietor, bringing his brother A. Campbell Johnson in as partner.<ref>{{cite news |url=http://nla.gov.au/nla.news-article43091272 |title=Advertising. |newspaper=[[South Australian Register]] |location=Adelaide |date=18 November 1879 |accessdate=5 January 2015 |page=2 |publisher=National Library of Australia}}</ref>

In mid-1880 they advertised for another cartoonist (by this time the proprietors were Johnson and Scarfe); in July ''Adelaide Punch'' grew in size, and the type and layout were changed to more closely resemble the London ''Punch''; South Australian newspapers greeted the new format with approval.<ref>{{cite news |url=http://nla.gov.au/nla.news-article43152400 |title=Adelaide Punch |newspaper=[[South Australian Register]] |location=Adelaide |date=3 July 1880 |accessdate=5 January 2015 |page=5 |publisher=National Library of Australia}}</ref>

Around October 1881 Johnson hired Godfrey Egremont (died 1923), once the ''Register'''s theatre critic, prolific author and embezzler,<ref>{{cite news |url=http://nla.gov.au/nla.news-article44566615 |title=The Arrest of Godfrey Egremont |newspaper=[[South Australian Register]] |location=Adelaide |date=26 April 1886 |accessdate=5 January 2015 |page=6 |publisher=National Library of Australia}}</ref> as editor then in April 1882 sold out to [[E. H. Derrington]],<ref>Edwin Henry Derrington (1830–1899), later MP for [[Electoral district of Victoria|Victoria]], was owner of the ''Yorke's Peninsula Advertiser'' and the ''Port Adelaide News''</ref> who appointed Henry O'Donnell as editor and engaged Herbert James Woodhouse (1858–1937) as cartoonist.<ref>{{cite news |url=http://nla.gov.au/nla.news-article11071199 |title=Obituary |newspaper=[[The Argus (Melbourne)|The Argus]] |location=Melbourne |date=12 June 1937 |accessdate=5 January 2015 |page=15 |publisher=National Library of Australia}} This reference names H. J. Woodhouse's brother W. J. Woodhouse as ''Adelaide Punch'' cartoonist 1878–1884.</ref> O'Donnell and Woodhouse became owners in early 1884;<ref>{{cite news |url=http://nla.gov.au/nla.news-article97326355 |title=Change of Ownership |newspaper=[[Northern Argus]] |location=Clare, SA |date=29 January 1884 |accessdate=5 January 2015 |page=2 |publisher=National Library of Australia}}</ref> quality suffered, the wit was gone and by October 1884 the magazine was in trouble; it was purchased by Charles A. Murphy, owner with Charles F. Stansbury, of an erstwhile competitor, the ''Lantern'', and publication ceased.<ref>{{cite news |url=http://nla.gov.au/nla.news-article146554028 |title=From the Metropolis. |newspaper=[[The Narracoorte Herald]] |location=SA |date=31 October 1884 |accessdate=4 January 2015 |page=2 |publisher=National Library of Australia}}</ref>

==References==
{{Reflist}}

{{DEFAULTSORT:Punch, Adelaide}}
[[Category:1878 establishments in Australia]]
[[Category:1884 disestablishments in Australia]]
[[Category:Defunct magazines of Australia]]
[[Category:Magazines established in 1878]]
[[Category:Magazines disestablished in 1884]]
[[Category:Media in Adelaide]]
[[Category:Satirical magazines]]
[[Category:Australian weekly magazines]]
[[Category:History of Adelaide]]