{{Infobox airport 
| name         = Bir Gifgafa
| nativename   = 
| nativename-a = 
| nativename-r = 
| image        =
| image-width  = 
| caption      = 
| IATA         = 
| ICAO         = HE36
| type         =
| owner        = 
| operator     =
| city-served  = 
| location     =
| elevation-f  = 1056
| elevation-m  = 322
| coordinates  = {{Coord|30|24|26|N|33|9|15|E|type:airport|display=inline,title}}
| website      = 
| metric-elev  = 
| metric-rwy   = 
| r1-number    = 15/33
| r1-length-f  = 8241
| r1-length-m  = 2512
| r1-surface   = Asphalt
| r2-number    =
| r2-length-f  =
| r2-length-m  = 
| r2-surface   =
| r3-number    =
| r3-length-f  =
| r3-length-m  = 
| r3-surface   = 
| stat-year    = 
| stat1-header = 
| stat1-data   = 
| stat2-header = 
| stat2-data   = 
| footnotes    = 
}}

'''Bir Gifgafa''' (also '''Bir-Jifjafah''', '''Meliz''' or '''Rephidim''') is an airfield in the [[Sinai]], 90&nbsp;km east of the [[Suez Canal]]. During the 1960s and 1970s it played a significant role in Arab-Israeli wars, at different times serving both [[Egypt]] and [[Israel]].

==Construction==
Bir Gifgafa was constructed by the [[Egyptian Air Force]] in the aftermath of the [[Suez Crisis]] of 1956. Assigned base number 244, it was responsible for providing air cover and close support for Egyptian Army units in the Sinai.<ref>{{cite book
  |last = Nordeen
  |first = Lon
  |title = Phoenix Over The Nile - A History of Egyptian Air Power 1922 - 1994
  |publisher = Smithsonian
  |year = 1996
  |page = 197
  |isbn = 978-1-56098-626-3}}</ref>

==The Six Day War==
[[File:Nasser and Egyptian pilots pre-1967.gif|thumb|right|May 22, 1967, Nasser visits [[Egyptian Air Force]] pilots at Bir Gifgafa on the eve of the Six Day War.]]
[[File:6dayswar1.jpg|thumb|right|Destroyed MiG-21 at Bir Gifgafa, June 1967.]]
{{main|Six-Day War}}

On May 22, 1967, as Israel and its Arab neighbours were drawing closer to war, president [[Gamel Abdel Nasser]] of Egypt visited Bir Gifgafa, meeting with Egyptian Air Force (EAF) pilots and commanders. In a much publicized press conference Nasser announced his intention to close the [[Straits of Tiran]] to Israeli shipping and declared that {{'}}We are now on the verge of a confrontation with Israel{{'}}, adding that {{'}}If the Jews threaten us war? I say to them "Welcome, we are ready for war!"{{'}}<ref>Moshe Gat, 2005, [http://www.paulbogdanor.com/israel/gat1967.pdf Nasser and the Six Day War, 5 June 1967: A Premeditated Strategy or An Inexorable Drift to War?], ''Israel Affairs'' 14:4, pp. 609 - 636.</ref>

As the most important Egyptian air base in the Sinai, housing units that could pose a major threat to Israeli aerial supremacy, Bir Gifgafa figured prominently in the [[Israeli Air Force]]'s planning for a pre-emptive strike. On the eve of the war the air base contained elements of the EAF's 15th fighter regiment, including part of the 45th fighter squadron flying MiG-21F-13s or MiG-21PFs. A number of transports and [[Mil Mi-6]] helicopters were stationed there as well.<ref>Nordeen, pp. 199–200</ref> No less than six IAF formations were therefore assigned to attack the base: four [[Dassault Ouragan]] formations from [[113 Squadron (Israel)|113 Squadron]] at [[Hatzor Airbase|Hatzor]] and two [[109 Squadron (Israel)|109 Squadron]] [[Dassault Mystere]] formations from [[Ramat David Airbase|Ramat David]].<ref name="bolt1">{{cite book
  |last = Shalom
  |first = Danny
  |title = Like a Bolt Out of the Blue
  |publisher = Bavir Aviation & Space Publications
  |year = 2002
  |language = Hebrew
  |pages = 229–238
  |isbn = 965-90455-0-6}}</ref><br /> 
When Israel finally launched [[Operation Focus]] on June 5, Bir Gifgafa was the target of the very first formation to take off from Hatzor at 07:14.<ref name="iaf0506">[http://www.iaf.org.il/Templates/FlightLog/FlightLog.aspx?lang=HE&lobbyID=40&folderID=48&subfolderID=321&docfolderID=923&docID=12803&docType=EVENT גל התקיפה הראשון: 204 מטוסי אויב מושמדים בבסיסיהם]</ref> The four Ouragans, led by Captain Ran Alon, struck the field at 07:45, hitting the runway and destroying several aircraft, including one MiG-21 which had just taken off. The MiG was downed by Captain David Yariv, who was himself subsequently hit by anti-aircraft fire and killed. The fierce anti-aircraft fire took its toll on the other aircraft as well, with a second Ouragan forced to perform a [[belly landing]] upon returning to Ramat David, while Captain Mordechai Lavon became a prisoner of war after ejecting his stricken aircraft over the Mediterranean and swimming ashore at [[Gaza City|Gaza]].<ref name="bolt1" />
By 09:05, when the first wave of operation Focus was concluded, Bir Gifgafa had been hit by three more 113 Squadron formations and by four 109 Squadron Dassault Mysteres.

Another 109 Squadron formation arrived at Bir Gifgafa at 09:55, while four [[105 Squadron (Israel)|105 Squadron]] [[Dassault Super Mystere|Super Mysteres]] attacked the field in the afternoon, although by then Bir Gifgafa had been completely disabled and there was little damage to be done.<ref>Shalom 2002, p. 594</ref> The Egyptian air force had lost as many as 20 MiG-21s, at least four Mi-6s and a [[MiG-15|MiG-15UTI]].<ref name="bolt1" /><ref>Nordeen, pp. 208 - 209</ref>

Although defended by the 4th Armored Division of the Egyptian army, plus a motorized infantry brigade,<ref>[http://www.sixdaywar.org/content/southernfront.asp The Six-Day War: Egyptian Front]</ref> these forces were withdrawn as the Egyptian effort in the Sinai collapsed, and the airfield was taken by the [[Israel Defense Forces|IDF]]'s 84th Tal Division on June 7. Once secure, IAF [[Nord Noratlas|Nords]] and [[C-47 Skytrain|Dakotas]] begun flying in supplies for the Israeli army, as well as an [[MIM-23 Hawk]] battery to provide air defence.<ref name="Norton">{{cite book
  |last = Norton
  |first = Bill
  |title = Air War on the Edge - A History of the Israel Air Force and its Aircraft since 1947
  |publisher = [[Ian Allan Publishing|Midland Publishing]]
  |year = 2004
  |page = 165
  |isbn = 1-85780-088-5}}
</ref>
Several dozen [[AA-2 Atoll]] missiles and nine missile launchers were captured in Bir Gifgafa. Testing proved their compatibility with the [[Mirage III]], and these were pressed into service with [[119 Squadron (Israel)|119 Squadron]] in December 1967, after several successful test firings the previous month.<ref name="mirage">{{cite book
  |last = Aloni
  |first = Shlomo
  |title = Israeli Mirage and Nesher Aces
  |publisher = Osprey
  |year = 2004
  |page = 47
  |isbn = 1-84176-653-4}}</ref>

==Rephidim Airbase==

Israeli gains in the Six Day War meant that a great distance now lay between the air force's air bases and the front lines in the Sinai, along the Suez Canal. Israel therefore decided to employ Bir Gifgafa as a forward operating base. Following Egyptian Air Force incursions into the Sinai, four [[101 Squadron (Israel)|101 Squadron]] Mirages first deployed there on July 26, 1967.<ref>{{cite book
  |last = Shalom
  |first = Danny
  |title = Phantoms over Cairo - Israeli Air Force in the War of Attrition (1967-1970)
  |publisher = Bavir Aviation & Space Publications
  |year = 2007
  |language = Hebrew
  |page = 59
  |isbn = 965-90455-2-2}}</ref>
Bir Gifgafa officially resumed operations in May 1968 as Israeli Air Force Base (''Baha'') 3. It was named [[Rephidim]], after the station mentioned in the Biblical account of the [[The Exodus|Exodus]] from Egypt. Despite having the most rudimentary installations at first, the field and its surrounding environs soon became the hub of all IDF operations in the western Sinai, housing various air force, army and logistical units.<ref>Shalom 2007, pp. 60 - 61</ref> It was also the IAF's transportation hub for the Sinai and was often frequented by the IAF's heavy transport aircraft, such as the [[C-97 Stratocruiser]] and [[Boeing 707]], as well as lesser types.

===The War of Attrition===
{{main|War of Attrition}}
As fighting intensified along the Suez Canal, the Israeli Air Force now dispatched its fighter squadrons on regular deployments to the airfield, with four pilots and aircraft rotating every two or three weeks. Two aircraft regularly stood on quick reaction alert (QRA) at the base, ready to scramble within 5 minutes. Many of the kills achieved by Israeli fighter pilots between 1967 and 1973 were claimed by the Rephidim QRA aircraft, and many pilots sought to be stationed there during times of tension, eager to improve their chances of achieving a victory.<ref name="mirage2">Aloni ''Mirage'', p. 45</ref> The base was also the forward recovery base for any aircraft damaged or low on fuel, as well as the premier medical station for soldiers wounded in the fighting along the Canal, from which they were then ferried to Israel.<ref>Shalom 2007, p. 62</ref>

Rephidim was permanently at the forefront of fighting between Israel and Egypt during War of Attrition, taking part in every major incident along the Suez Canal, as well as numerous minor ones. On August 26, 1967, a pair of Egyptian [[Su-7]]s struck at the field, damaging the main runway. One was shot down by anti-aircraft artillery, while the second evaded the two 119 Squadron Mirages scrambled to intercept it.<ref>Shalom 2007, pp. 126–128</ref><ref>[http://www.iaf.org.il/Templates/Headquarters/Headquarters.aspx?lang=EN&lobbyID=33&folderID=39 Air Defense Corps] on the [http://www.iaf.org.il Israeli Air Force website]</ref> The first kill by a Rephidim-based aircraft came on October 10, 1967, when [[Avihu Bin-Nun]], leading a pair of 119 Squadron Mirages, shot down an Egyptian MiG-21 over the Sinai.<ref name ="mirage2" /> It was the QRA aircraft at Rephidim which provided cover for the rescue efforts following the sinking of [[INS Eilat (1955)|INS ''Eilat'']],<ref>Shalom 2007, p. 138</ref> and Mirages from Rephidim also took part in [[Rimon 20]], the July 1970 air battle which saw 5 Soviet-flown MiG-21s shot down.<ref>Norton, p. 206</ref>

===The Yom Kippur War===
{{main|Yom Kippur War}}
During the Yom Kippur War of 1973, Rephidim was once again at the forefront of fighting between Israel and Egypt.
Although a heightened state of alert had been declared throughout the IAF on October 5,<ref>{{cite book
  |last = Gordon
  |first = Shmuel
  |title = Thirty Hours in October
  |publisher = Ma'ariv Book Guild
  |year = 2008
  |language = Hebrew
  |page = 211}}</ref> Rephidim was ill prepared for the Egyptian attack which launched the war. 16 Su-7s, escorted by 6 MiG-21s, struck the field at 14:00 on October 6, escaping unharmed. Although the main runway was disabled for four hours, the parallel taxiway was operational within 30 minutes of the attack, allowing Rephidim to join the fighting now raging along the Suez Canal.<ref>Gordon, p. 275</ref> Two 119 Squadron [[F-4 Phantom II|F-4 Phantoms]] (the squadron had retired the Mirage in 1970) launched from Rephidim to assist Israeli units under attack, and pilot Moshe Melnik with navigator Zvi Tal shot down an [[AS-5 Kelt]] launched by a [[Tu-16]] as well as a Su-7.<ref name="phantom">{{cite book
  |last = Aloni
  |first = Shlomo
  |title = Israeli F-4 Phantom II Aces
  |publisher = Osprey
  |year = 2004
  |pages = 27–29
  |isbn = 1-84176-783-2}}</ref> Later in the afternoon, a mixed pair consisting of a Phantom and an [[IAI Nesher]] scrambled from Rephidim to down several [[Mil Mi-8|Mi-8]] helicopters airlifting Egyptian commandos into the Sinai.<ref name="phantom" /><ref>Gordon, p. 289</ref><ref>Norton, p. 236</ref> That same night also saw the IAF deploying several helicopters to Rephidim for medevac (medical evacuation) duties.<ref>Gordon, p. 291</ref> One Israeli [[CH-53]] was shot down over the field on October 12, killing five crewmen.<ref>[http://www.iaf.org.il/Templates/FlightLog/FlightLog.aspx?lang=EN&lobbyID=40&folderID=48&subfolderID=322&docfolderID=945&docID=13303&docType=EVENT October 12, Southern Front] on the [http://www.iaf.org.il Israeli Air Force website]</ref>

==Israeli withdrawal and aftermath==
Following the [[Camp David Accords]], Rephidim was the very first air base from which Israel withdrew in late 1979.<ref>[http://www.iaf.org.il/Templates/FlightLog/FlightLog.aspx?lang=EN&lobbyID=40&folderID=48&subfolderID=322&docfolderID=373&docID=5161&docType=EVENT 1979] on the [http://www.iaf.org.il Israeli Air Force website]</ref> Although the accords prohibit Egypt from maintaining combat aircraft in the Sinai,<ref>[https://www.knesset.gov.il/process/docs/egypt_eng.htm Peace Treaty Between the State of Israel and the Arab Republic of Egypt], Article III</ref> Bir Gifgafa is still operational as a dual military and civilian airport. Several derelict C-97s may have remained at Bir Gifgafa upon Israel's departure.<ref>Norton, p.196</ref>

==References==
{{reflist|35em}}

[[Category:Israeli Air Force bases]]
[[Category:Airports established in 1956]]
[[Category:Egyptian Air Force bases]]