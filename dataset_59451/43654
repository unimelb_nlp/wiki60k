<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name = TBM 700 / TBM 850 / TBM 900 <br> TBM 930
 |image = File:Daher-Socata TBM 900 Air to Air.jpg
 |caption = A TBM 900 over [[Lake Winnebago]] during the 2015 [[EAA AirVenture Oshkosh]] event.
}}{{Infobox Aircraft Type
 |type = Executive transport and civil utility
 |national origin = France 
 |manufacturer = [[DAHER-SOCATA]]
 |first flight = 14 July 1988 
 |introduction = [[1990 in aviation|1990]]
 |retired = 
 |status = In production
 |primary user = [[French Army]]
 |more users = [[French Air Force]]
 |produced = 1988–present 
 |number built = 822 ({{As of|2016|12|31}})<ref name=170116PR/>
 |unit cost = [[United States dollar|US$]]3.66 million (TBM 900, 2016)<ref name=900specsprice/> <br/>US$3.9 million (TBM 930, 2016)<ref>{{cite magazine |author= Fred George |url= http://www.sellajet.com/adpages/BCA-2016.pdf |title= 2016 Business Airplanes Purchase Planning Handbook |magazine= Business & Commercial Aviation |page= 89 |series= Aviation Week |date= May 2016}}</ref>
 |developed from = 
 |variants with their own articles = 
}}
|}

The '''SOCATA TBM 700''' (also marketed as the '''TBM 850''', '''Daher TBM 900''' and '''Daher TBM 930''') is a high performance single-engine [[turboprop]] light business and utility [[aircraft]] manufactured by [[DAHER-SOCATA]]. An aerodynamically refined version of the TBM 700N is marketed as the TBM 900 from March 2014.<ref name="Durden12Mar14">{{cite news|url = http://www.avweb.com/avwebflash/news/DAHER-SOCATA-Reveals-New-TBM-900221588-1.html|title = DAHER-SOCATA Reveals New TBM 900|accessdate = 15 March 2014|last = Durden|first = Rick|date = 12 March 2014| work = AVweb}}</ref>

==Design and development==
[[File:N96HK (5836775447).jpg|thumb|A TBM 850 in landing configuration]]
[[File:Glass-cockpit.png|thumb|Cockpit panels of a TBM 850]]
In the early 1980s, the [[Mooney Airplane Company]] of [[Kerrville, Texas]] designed a six-seat [[Cabin pressurization|pressurised]] light aircraft powered by a single 360&nbsp;hp (268&nbsp;kW) piston engine, the [[Mooney 301]], which made its maiden flight on 7 April 1983.<ref name="AIFeb06">Simpson, Rob. "TBM 850: EADS Socata challenges the Very Light Jets". ''[[Air International]]'', February 2006, Vol 70 No 2, pp.&nbsp;28–31. {{ISSN|0306-5634}}/, p. 28-29.</ref> Mooney was purchased by French owners in 1985,<ref name="JAWA88">[[John W. R. Taylor|Taylor, John W. R.]] ''Jane's All The World's Aircraft 1988–89''. Coulsdon, UK: Jane's Defence Data, 1988. ISBN 0-7106-0867-5.</ref>{{rp|443}} which resulted in talks between Mooney and the French company SOCATA to build a turboprop derivative of the 301. The result of these discussions was the TBM 700, which was much heavier than the 301 with more than twice the power, with a joint venture, TBM International, being set up in June 1987 between Mooney and Socata's parent company [[Aérospatiale]] to design and build the new aircraft.<ref name="AIFeb06"/><ref name="JAWA88"/>{{rp|135}} In the designation TBM, "TB" stands for [[Tarbes]], the French city in which Socata is located, the "M" stands for Mooney.<ref name="AIFeb06"/>

The TBM 700 is a single-engined turboprop, six to seven-seat low-wing monoplane of mainly aluminium and steel construction, but with the tail surfaces built of [[Nomex]] honeycomb. It has a retractable [[tricycle landing gear]] and is powered by a [[Pratt & Whitney Canada]] [[Pratt & Whitney Canada PT6|PT6A-64]] engine delivering 700 [[Shaft horsepower|shp]] (522&nbsp;kW).<ref name="AIFeb06"/><ref name="JAWA03 p150">Jackson, Paul. ''Jane's All The World's Aircraft 2003–2004''. Coulsdon, UK: Jane's Information Group, 2003. ISBN 0-7106-2537-5, p. 150.</ref> The first prototype TBM 700 made its maiden flight on 14 July 1988,<ref name="JAWA88"/>{{rp|135}} with French [[Type certificate|certification]] following on 31 January 1990 and US [[Federal Aviation Administration|FAA]] certification achieved on 28 August 1990.<ref name="AIFeb06"/>

Two production lines were planned, one at Kerrville to cater to the American market, and the other at SOCATA's factory in Tarbes to build aircraft for the rest of the world. A shortage of money resulted in Mooney withdrawing from the project in May 1991.<ref name="AIFeb06"/> The TBM 700 also comes in a cargo variant.

The TBM 850 is the production name for the TBM 700N, an improved version with the more powerful Pratt & Whitney PT6A-66D engine [[flat rated]] at 850 shp (634&nbsp;kW). The TBM 850 is limited to 700 shp (522&nbsp;kW) for takeoff and landing, but in cruise flight the engine power can be increased to 850 shp (634&nbsp;kW). This extra power gives it a higher cruising speed than the TBM 700 models, especially at high altitudes (due to the flat-rating). The outside appearance of the TBM 850 has remained the same as that of the TBM 700. The TBM 850 has a typical range of {{Convert|1520|nmi|km}}.

Beginning with the 2008 model, the TBM 850 is equipped with the [[Garmin G1000]] integrated flight deck as standard equipment.<ref name=AirbusGroup9March2008PressRelease>{{cite press release |author=<!--Staff writer(s); no by-line.--> |title=EADS SOCATA Unveils the 2008 TBM 850|url=http://www.airbusgroup.com/int/en/news-media/press-releases/Airbus-Group/Financial_Communication/2008/01/20080117_eads_socata_unveils_2008_TBM850.html|location=Tarbes|publisher=[[Airbus Group]]|agency= |date=17 January 2008|access-date=9 March 2008}}</ref>

Introduced in 2014, the TBM 900 is an improved version with 26 modifications including [[winglet]]s, a redesigned air intake and a 5-blade propeller, for better aerodynamics and performance.<ref name="DAHER12March2014PressRelease">{{cite press release |author=<!--Staff writer(s); no by-line.--> |title=DAHER-SOCATA reveals the TBM 900 very fast turboprop aircraft|url=http://www.daher.com/uploads/assets/Communiques_de_presse/en/News_Release__DAHER-SOCATA_reveals_the_TBM_900.pdf|location=Tarbes|publisher=[[Daher|DAHER]]|date=12 March 2014|access-date=12 March 2014}}</ref>

==Variants==
[[File:Socata TBM 850 Le Bourget 133.jpg|thumb|Rear access door of a TBM 850]]
[[File:TBM 6 places.png|thumb|Cabin of a TBM 850 in 6-seat configuration]]
;TBM 700A
:Initial production version with one Pratt & Whitney Canada PT6A-64 turboprop engine.
;TBM 700B
:Variant with wide entrance door, increased maximum zero fuel weight and other improvements. 
;TBM 700 C1
:Improved version with rear unpressurised cargo compartment, reinforced structure, new air conditioning system and other improvements.
;TBM 700C2
:C1 with increased maximum takeoff weight.
;TBM 700N
:Variant with increased maximum cruise/climb power, one Pratt & Whitney Canada PT6A-66D turboprop engine, marketed as the ''TBM 850'' and with modifications as the ''TBM 850 G1000'' and ''TBM 900.''
;TBM 850
:Marketing name for the TBM 700N.
;TBM 850 G1000
:Marketing name for the TBM 700N with a G1000 Integrated Flight Deck and a fuel tank extension modification.
;TBM 850 Elite
:An updated version of the TBM 850. This model includes four cabin seats in a forward-facing configuration, allowing for an increased cargo area aft of the cabin.<ref name="FlightglobalDaher-SocataMakesTBM850AnElite">{{cite news|last1=Sarsfield|first1=Kate|title=Daher-Socata makes TBM 850 an Elite|url=https://www.flightglobal.com/news/articles/aero-2012-daher-socata-makes-tbm-850-an-elite-370982/|accessdate=23 April 2012|publisher=[[Flightglobal]]|date=23 April 2012|location=London}}</ref>
;TBM 900
[[File:N900XH LBG SIAE 2015 (19014605555).jpg|thumb|A TBM 900 with a five-blade propeller]]
:Marketing name for the TBM 700N improved version with aerodynamic inlet and performance optimization.<ref>{{cite journal|magazine=Sport Aviation|date=June 2014|page=76|title=TBM900|author=J. Mac McClellan}}</ref> Maximum cruise speed is increased to 330 [[knot (unit)|kn]] at FL310. A range of 1,730 [[nautical mile|nmi]] (with 45-minute standard IFR reserves) using long-range cruise speed is capable at 252 kn while burning 37 gph or 1,585 nmi at 290 kn.<ref name="DAHER12March2014PressRelease"/> The (previously optional) five-bladed carbon fiber Hartzell propeller is now standard, increasing performance and decreasing cabin noise.
;TBM 910
:New version introduced at [[Sun 'n Fun]] in April 2017. It is a TBM 900 with upgraded avionics, including the [[Garmin G1000 NXi]] avionics suite.<ref>{{cite news|last=Bertorelli |first=Paul|title=Daher Unveils The TBM 910|url=http://www.avweb.com/avwebflash/news/Daher-Unveils-the-TBM-910-228773-1.html|accessdate=5 April 2017|publisher=AVweb|date=4 April 2016}}</ref>
;TBM 930
:New version introduced in April 2016. It is a TBM 900 with upgraded interior and avionics, including the [[Garmin G3000]] touchscreen avionics suite. The TBM 930 is offered alongside the 900 and has not replaced it in the line-up.<ref name="AVwebDaherAddsTBM930ToTurbopropLine">{{cite news|last1=Grady|first1=Mary|title=Daher Adds TBM 930 To Turboprop Line|url=http://www.avweb.com/avwebflash/news/Daher-Adds-TBM-930-To-Turboprop-Line-225990-1.html|accessdate=7 April 2016|publisher=AVweb|date=6 April 2016}}</ref><ref name="AVwebUpdateTBM930NowOnDisplayAtSunNFun">{{cite news|last1=Grady|first1=Mary|title=Update: TBM 930 Now On Display At Sun 'n Fun|url=http://www.avweb.com/avwebflash/news/Update-TBM-930-Now-On-Display-At-Sun-n-Fun-226013-1.html|accessdate=8 April 2016|publisher=AVweb|date=7 April 2016}}</ref>

===Production===
At the end of 2016, the TBM fleet had logged a combined 1.4 million flight hours and 822 aircraft had been produced.<ref name=170116PR>{{cite press release |url= http://www.tbm.aero/daher-delivers-54-tbm-aircraft-in-2016-and-prepares-a-new-integrated-flight-deck-for-the-tbm-900-version/ |title= Daher delivers 54 aircraft in 2016 and prepares a new flight deck for the TBM 900 version |publisher= Daher |date= 16 January 2017}}</ref>
*TBM 700 - 324 built between 1990-2005
*TBM 850 - 338 built between 2006-2013
*TBM 900 - 106 built since 2014

==Operators==
[[File:French Air Force, F-RAXL, Socata TBM700 (27881367003).jpg|thumb|French Air Force TBM, note small door]]

The aircraft is used by both private individuals, corporations and charter and hire companies.

===Military operators===
;France
* [[French Air Force]] – 15 in service (2016).<ref name=AFdir>{{cite news |url= https://www.flightglobal.com/asset/14484 |author= Craig Hoyle |title= World Air Forces Directory 2017 |work= [[FlightGlobal]] |date= 2016}}</ref>
* [[French Army]] [[French Army Light Aviation|Light Aviation]] (ALAT) – 8 in service (2016).<ref name=AFdir/>

==Accidents and incidents==

On 5 September 2014, a TBM 900 (registered N900KN) was found to be flying with an apparently unconscious pilot over [[South Carolina]] (see [[2014 SOCATA TBM crash]]). Pilots of fighter aircraft that were [[Scrambling (military)|scrambled]] to trail the TBM 900 observed that the windows were frosted over. The aircraft reportedly crashed 14 miles northeast of [[Portland Parish]], Jamaica, on the country's northeast coast.<ref>{{cite web|last1=Botelho|first1=Greg|title=Pilot of unresponsive plane asked to descend before contact lost|url=http://www.cnn.com/2014/09/05/us/norad-air-threat/|website=CNN.com|accessdate=8 September 2014}}</ref><ref>{{cite web|last1=Whitefield|first1=Mimi|last2=Charles|first2=Jacqueline|title=Jamaica finds wreckage of runaway plane|url=http://www.miamiherald.com/2014/09/05/4329897/unresponsive-mystery-plane-heads.html|publisher=The Miami Herald|accessdate=8 September 2014}}</ref>

==Specifications (TBM 900)==
{{Aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
|plane or copter?= plane
|jet or prop?= prop
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply leave it blank. For instructions on using |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specifications]]. -->

|ref=TBM<ref name=900specsprice>{{cite web |url= http://www.tbm.aero/wp-content/uploads/2016/03/2016_TBM-900_SpecsPriceList.pdf |title= TBM-900 Specifications & Price list 2016 |date= March 2016 |publisher= TBM}}</ref>
|crew= one or two pilots
|capacity=four to six occupants, including pilots
|length main= 10.736 m
|length alt= 35.22 ft
|span main= 12.833 m 
|span alt= 42.10 ft
|height main= 4.355 m 
|height alt= 14.29 ft 
|area main= 18 m²
|area alt= 193.75 sq ft<ref>{{cite news |url= http://www.tbm.aero/wp-content/uploads/2016/07/2012-01-AOPA-Pilot.pdf |title= Top-of-the-line T-prop |date= January 2012 |work= AOPA Pilot |publisher= aircraft owners and pilots association }}</ref>
|empty weight main= 2,097 kg 
|empty weight alt= 4,629 lb
|useful load main= 636 kg
|useful load alt= 1,403 lb / 
|max takeoff weight main= 3,353 kg 
|max takeoff weight alt= 7,394 lb
|more general=
'''Usable fuel:''' 291 US gal. / 1,100 liters

|engine (prop)= [[Pratt & Whitney Canada PT6]]A-66D 
|type of prop= [[turboprop]]
|number of props=1
|power main= 634 kW
|power alt= 850 hp

| max speed main = 611 km/h
| max speed alt = 330 knots
| max speed more    = FL280
| cruise speed main = 467 km/h
| cruise speed alt= 252 knots
| cruise speed more = Long Range Cruise FL310
| range main        = 3,304 km
| range alt         = 1,730 nmi
| range more        = Long Range Cruise FL310

|ceiling main= 9,450 m
|ceiling alt= 31,000 ft 
|more performance=
* '''Time-to climb to 31,000 ft.:''' 18 min. 45 sec

|avionics=
}}

==See also==
{{Aircontent|
|related=
* [[Mooney 301]]
|similar aircraft=
* [[Aero Ae 270 Ibis]]
* [[Epic LT Dynasty]]
* [[Kestrel K-350]]
* [[Pilatus PC-12]]
* [[Piper PA-46|Piper Meridian]]

|lists=
|see also=

}}

==References==
{{Reflist|30em}}

==External links==
{{Commons and category|Socata TBM}}
* {{official website|http://www.tbm.aero}}
* {{cite web |url= http://www.easa.europa.eu/certification/type-certificates/docs/aircraft/EASA-TCDS-A.010_TBM700-07-25032009.pdf |title= Type Certificate Data Sheet |date= 25 March 2009 |publisher= EASA }}
* {{cite magazine |url= https://books.google.com/books?id=zXLZEaGftB4C&pg=PA64 |title= TBM 700C2 |work= Flying Magazine |date= April 2003}}
* {{cite news |url= http://aviationweek.com/business-aviation/pilot-report-daher-s-new-tbm-930 |title= Pilot Report: Daher’s New TBM 930 |date= Jan 25, 2017 |author= Fred George |work= Business & Commercial Aviation |publisher= Aviation Week}}

{{Socata}}

[[Category:French business aircraft 1980–1989]]
[[Category:SOCATA aircraft|TBM]]
[[Category:1990 introductions]]