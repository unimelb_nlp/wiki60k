{{Use dmy dates|date=January 2015}}
{{Use South African English|date=January 2015}}
{{Infobox Military Award
| name             = Cape of Good Hope General Service Medal
| image            = [[File:Cape of Good Hope General Service Medal & Clasps.jpg|300px]]
| caption          = 
| awarded_by       = [[Queen Victoria |the Monarch of the United Kingdom of Great Britain and Ireland, and Empress of India]]
| country          = {{flagicon|UK}} [[UK]] [[File:Cape Colony flag.png|29px]] [[Cape Colony|Cape of Good Hope]]
| type             = Military [[Campaign medal]]
| eligibility      = [[Cape Colonial Forces]]
| for              = Campaign service
| campaign         = Basutoland 1880–1881<br>Transkei 1880–1881<br>Bechuanaland 1896–1897
| status           = 
| description      = 
| clasps           = BASUTOLAND<br>TRANSKEI<br>BECHUANALAND
| established      = 1900
| first_award      = 
| last_award       = 
| total            = 5,252
| posthumous       = 
| recipients       = 
| precedence_label = Order of wear
| individual       = 
| higher           = [[Kabul to Kandahar Star]]
| same             = 
| lower            = [[Egypt Medal]]
| related          = 
| image2           = [[File:Ribbon - Cape of Good Hope General Service Medal.png|x29px]]
| caption2         = Ribbon bar
}}

The '''Cape of Good Hope General Service Medal''' is a British campaign medal which was awarded to members of the [[Cape Colonial Forces]] who took part in three campaigns in and around the [[Cape Colony|Cape of Good Hope]], in Basutoland in 1880–1881, in Transkei in 1880–1881 and in Bechuanaland in 1896–1897.<ref name="MoW">{{cite web | last =Robertson | first =Megan C.| title =United Kingdom: Cape of Good Hope General Service Medal 1880–97 | work =Medals of the World | publisher = | date =16 July 2008 | url =http://www.medals.org.uk/united-kingdom/united-kingdom126.htm | format = | doi = | accessdate =2015-01-26}}</ref><ref name="Gazette">[http://www.northeastmedals.co.uk/britishguide/cape_good_hope.htm The Cape of Good Hope Government Gazette, Tuesday, December 4th 1900 – Government Notice No. 841 – 1900 (Retrieved 2015-01-26)]</ref><ref name="Cigarette Cards">[http://www.britishempire.co.uk/media/cigarettecards/medals/cigarettemedal44.htm The British Empire – Cigarette Cards, Decorations and Medals – Player's Cigarettes card no. 44 (Retrieved 2015-01-26)]</ref><ref name="Colonial">[http://www.geocities.ws/militaf/mil94.htm South African Medal Website - Colonial Military Forces] (Accessed 6 May 2015)</ref>

==Institution==
The Cape of Good Hope General Service Medal was authorised in December 1900 as a retrospective award for veterans of three campaigns which were fought in South Africa between 1880 and 1897. The medal was for award to the officers, non-commissioned officers and men of the Colonial Forces who were engaged on active service during the campaigns in Basutoland (1880–1881), Tembuland and Griqualand East in Transkei (1880–1881), and Bechuanaland (1896–1897). Three campaign clasps were authorised at the same time.<ref name="Gazette"/><ref name="Colonial"/><ref name="Alexander">Alexander, E.G.M., Barron, G.K.B. and Bateman, A.J. (1986). ''South African Orders, Decorations and Medals''. Human and Rousseau.</ref>

==Award criteria==
The medal was awarded, upon application, to all surviving veterans who had served in the [[Cape Colonial Forces]] in the three campaigns, for active service in the field, for serving as guards at any point where an attack was expected, or who were detailed for some specific or special military service or duty. No medal or clasp could be awarded to any member who had deserted or had been dismissed for misconduct.<ref name="Gazette"/><ref name="Colonial"/>

Because the award of the medal had to be applied for, it was not awarded posthumously. The next-of-kin of members who had been killed in action or who had died while on service therefore received no medal. The published medal roll shows that 5,252 medals were awarded to 5,156 individuals, which included 96 duplicate or triplicate awards. Nearly half of the awards, to 2,422 individuals, were for the Bechuanaland campaign.<ref name="Medal Roll">[http://www.northeastmedals.co.uk/britishguide/cape_good_hope_medal_roll_a_b.htm ''The Cape of Good Hope General Service Medal Roll, arranged alphabetically'', compiled by Martin Wagner with the assistance of Neil Raaff (Retrieved 2015-01-26)]</ref>

===Basuto Gun War===
The duration of the [[Basutoland]] Campaign was from 13 September 1880 to 27 April 1881. Following the end of the Zulu wars  from 1877 to 1879, Cape of Good Hope Governor [[Henry Bartle Frere]] and Prime Minister [[Gordon Sprigg]] attempted to disarm the Basotho and ordered them to hand in their firearms. Some chiefs reluctantly complied, but were almost immediately attacked by chiefs who had refused to comply, such as Lerothodi and Moletsane. In September 1880 they also attacked white administrators and, as a result, troops were mobilised and the [[Basuto Gun War]] broke out. Various encounters ensued until February 1881, when an armistice was arranged. Peace was eventually concluded in May 1881. Veterans of this campaign were awarded the Basutoland Clasp.<ref name="MoW"/><ref name="NortheastMedals">[http://www.northeastmedals.co.uk/britishguide/cape_good_hope.htm North East Medals – The Cape of Good Hope General Service Medal, 1880 – 1897 (Retrieved 2015-01-26)]</ref><ref name="Cape Town Highlanders">[http://natal1906.com/cape-of-good-hope-general-service-medal/ Natal 1906 – A Collection of British Battles Illustrated Through Their Related Campaign Medals – Cape of Good Hope General Service Medal 1880–97 (Retrieved 2015-01-26)]</ref>

===Transkei Campaign===
The duration of the [[Transkei]] Campaign was from 13 September 1880 to 13 May 1881. The medal and the Transkei Clasp were awarded for operations in [[Thembuland]] and [[Griqualand East]], where the native Xhosa populations were particularly hostile to settlers in the districts of [[Tsolo]], [[Maclear, Eastern Cape|Maclear]], [[Matatiele]] and [[Qumbu]].<ref name="MoW"/><ref name="NortheastMedals"/><ref name="Cape Town Highlanders"/>

===Bechuanaland Campaign===
The duration of the [[Bechuanaland]] Campaign was from 24 December 1896 to 30 July 1897. In April 1896, a severe outbreak of cattle disease occurred, which required that all cattle in the area needed to be slaughtered in an attempt to contain the disease. This was resented by the local Tswana population who, as a result, rose up in protest. A number of engagements occurred and, following a build-up of more Colonial reinforcements in July 1897, the conflict ended after a final action at Langberg on 30 July and 1 August 1897, in which most of the Tswana leaders were either killed or surrendered. Veterans of this campaign were awarded the Bechuanaland Clasp.<ref name="MoW"/><ref name="NortheastMedals"/><ref name="Cape Town Highlanders"/>

==Order of wear==
Campaign medals and stars are not listed by name in the order of wear prescribed by the British [[Central Chancery of the Orders of Knighthood]], but are all grouped together as taking precedence after the [[Queen's Medal for Chiefs]] and before the [[Polar Medal]]s, in order of the date of the campaign for which awarded.<ref name="London Gazette"/>

In the order of wear of British campaign medals, the Cape of Good Hope General Service Medal takes precedence after the [[Kabul to Kandahar Star]] and before the [[Egypt Medal]].<ref name="London Gazette">{{londongazette|issue=56878|supp=y|startpage=3352|date=17 March 2003}}</ref>

===South Africa===
{{main|South African military decorations order of wear#Order of wear}}
With effect from 6 April 1952, when a new South African set of decorations and medals was instituted to replace the British awards used to date, the older British decorations and medals applicable to South Africa continued to be worn in the same order of precedence but, with the exception of the [[Victoria Cross]], took precedence after all South African orders, decorations and medals awarded to South Africans on or after that date. Of the official British campaign medals which were applicable to South Africans, the Cape of Good Hope General Service Medal takes precedence as shown.<ref name="London Gazette"/><ref name="Notice1982">Government Notice no. 1982 of 1 October 1954 – ''Order of Precedence of Orders, Decorations and Medals'', published in the Government Gazette of 1 October 1954.</ref><ref name="Gazette 27376">Republic of South Africa Government Gazette Vol. 477, no. 27376, Pretoria, 11 March 2005, {{OCLC|72827981}}</ref>

[[File:South Africa Medal (1880).png|x37px|South Africa Medal (1880)]] [[File:Ribbon - Cape of Good Hope General Service Medal.png|x37px|Cape of Good Hope General Service Medal]] [[File:Queen's South Africa Medal.png|x37px|Queen's South Africa Medal]]
* Preceded by the [[South Africa Medal (1880)]].
* Succeeded by the [[Queen's South Africa Medal]].

==Description==
The medal was struck in silver and is a disk, 36 millimetres in diameter and 3 millimetres thick at the raised rim. It is affixed to the swivelling suspender by means of claws and a pin through the upper edge of the medal. The recipient's rank, name and unit were inscribed on the rim, but the medals were not numbered.<ref name="Colonial"/><ref name="NortheastMedals"/><ref name="Cape Town Highlanders"/>

;Obverse
The obverse depicts the veiled bust of [[Queen Victoria]], with the legend "VICTORIA REGINA ET IMPERATRIX" around the inside of the raised rim.<ref name="Colonial"/><ref name="NortheastMedals"/>

;Reverse
The reverse displays the [[Cape Province|Cape of Good Hope]] coat of arms, with a spray of [[protea]] leaves and a protea flower underneath and the name "CAPE OF GOOD HOPE" on a wide raised rim around the top half of the medal.<ref name="Colonial"/><ref name="NortheastMedals"/><ref name="Cape Town Highlanders"/>

;Ribbon
The ribbon is 32 millimetres wide and dark blue, with a 12 millimetres wide yellow centre band. These were also the ribbon colours of the two earlier campaign medals for service in southern Africa.<ref name="Cape Town Highlanders"/>

;Clasps
The three clasps which were awarded to indicate the campaigns in which recipients had served, were inscribed "BASUTOLAND", "TRANSKEI" and "BECHUANALAND" respectively.<ref name="Gazette"/><ref name="Colonial"/><ref name="Cape Town Highlanders"/>

==References==
{{Reflist|30em}}
{{British campaign medals}}
{{South African military decorations and medals}}

[[Category:British campaign medals]]
[[Category:Military decorations and medals of South Africa]]
[[Category:Military decorations and medals of South Africa in order of precedence|402.1880]]
[[Category:Military decorations and medals of South Africa pre-1952]]
[[Category:Military awards and decorations of the United Kingdom]]