[[File:Florida State Highway 694 01.jpg|thumb|Looking north on [[State Road 694 (Florida)|State Road 694]] from bridge over Intracoastal Waterway. Indian Shores, Florida on left, mangroves on right]]

The '''[[ecology]] of [[Florida]]''' is one of diverse wildlife.  A misconception often associated with the Florida landscape is that the changes of density and diversity of flora seem chaotic or random, however, [[Eugene Odum]]'s research states that "…over the long term [plant habits] showed remarkable continuity."<ref>''Nature's Economy'', Donald Worster</ref> The introduction of different species caused certain evolutionary developments – some plants grew more quickly and in larger numbers, while others could survive longer – to sustain themselves in a dense environment.

==What makes the Florida ecology unique?==
:From the [[Florida Everglades]] to the [[Gulf Coast]], Florida has its own variety of extreme environments.  Florida ecology is affected by a number of various factors including its climate, the surrounding waters, and the diverse wildlife that calls Florida home.

===Climatology===
<ref>{{cite web|url=http://coaps.fsu.edu/climate_center/index.shtml |accessdate=November 2, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20111017061442/http://coaps.fsu.edu/climate_center/index.shtml |archivedate=October 17, 2011 }}</ref>
:The [[Climate of Florida|climatology]] of Florida varies from region to region due to its proximity to the [[equator]].  From central [[Florida]] to the Georgia border, the climate is generally humid subtropical, while South Florida fosters a tropical climate.  The end of spring to mid-fall is characterized by a significant rainy season, subjecting Florida to [[hurricane]]s, [[thunderstorm]]s, and [[tropical cyclone]]s.  The winter and spring is significantly drier, leading to brushfires and strict no-fire laws.  While very uncommon, snow has been recorded in northern Florida; orange groves are damaged by hard freezes.
[[File:PonteVedra.jpg|thumb|A photo of Ponte Vedra Beach, an area attracting many tourists]]

===Water===
<ref>{{cite web|url=http://www.floridasmart.com/sciencenature/naturalflorida/index.htm |title=Florida Nature: Forests, Seashores, Preserves |publisher=Floridasmart.com |date= |accessdate=2015-09-27}}</ref>
:As a [[peninsula]], Florida is surrounded on three sides by two main bodies of water, the [[Gulf of Mexico]] and the [[Atlantic Ocean]].  Due to its water centrality and extremely low ratio of land [[sea level]]s, Florida is composed of [[marshland]], [[swampland]], [[lake]]s, [[spring (hydrosphere)|springs]] and [[river]]s.  Florida's largest river is the St Johns River and its largest lake, Lake Okeechobee, flows into the [[Florida Everglades]].
:Of course, Florida's beaches contributes largely to the wealth of the state.  Tourists from all over the world are attracted to the beautiful beaches and the varying qualities experienced on the Atlantic and Gulf Coasts.

===Fauna===
[[File:Gulf Fritillary Paynes Prairie.jpg|thumb|A [[Gulf fritillary]], taken in Payne's Prairie State Park, Florida]]
:Florida was once home to a very diverse array of wildlife.<ref>{{cite web|url=http://www.floridasmart.com/subjects/animals.htm |title=Florida Animals and The World's Wildlife |publisher=Floridasmart.com |date= |accessdate=2015-09-27}}</ref>  [[Bobcats]] were once very prominent in Florida, but land development, drained marshland and deforestation are pushing this species of [[lynx]] into more northern are. Florida also has many species of [[armadillo]], [[opossum]], [[fox]]es and birds like the [[Bald eagle|American eagle]] and [[osprey]], but are also being forced from their natural environments and into more urban areas, contributing to high percentages of roadkill.  The Wildlife Foundation of Florida<ref>{{cite web|url=http://www.wildlifefoundationofflorida.com/ |title=Fish & Wildlife Foundation of Florida - Foundation |publisher=Wildlifefoundationofflorida.com |date= |accessdate=2015-09-27}}</ref> acts to gain collaboration of the public in order to protect and conserve all types of wildlife from land and air to water.

:The insect<ref>{{cite web|url=http://www.floridabugs.com/Florida-Insects/ |accessdate=November 19, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20111115084938/http://www.floridabugs.com/Florida-Insects/ |archivedate=November 15, 2011 }}</ref> population of Florida is also unique.  The Gulf fritillary is a butterfly native to Florida grasslands.  One such area is Payne's Prairie<ref>{{cite web|url=http://www.prairiefriends.org/ |title=Friends of Paynes Prairie Inc. - Home |publisher=Prairiefriends.org |date=2015-08-03 |accessdate=2015-09-27}}</ref> in Gainesville, Florida.  Other Florida native insects include various species of [[mite]]s including Florida [[termite]]s and many types of ants, like the [[fire ant]].

:Of all the animals of Florida, the [[Florida panther]] is one of the most well-known.  It is an endangered species of [[cougar]] that is only found in the swamps of Florida. Human expansion, even with the conservation geared towards protecting this creature, is sending the panther into extinction.

:For more information on mammals in Florida, see the [[list of mammals of Florida]].

===Forest ecology===
<ref>{{cite web|url=http://www.sfrc.ufl.edu/4h/index.html |title=UF-SFRC : 4-H : Forest Resources |publisher=Sfrc.ufl.edu |date=2013-02-20 |accessdate=2015-09-27}}</ref>
:Because Florida has such a wide variety of climate conditions, there are many types of forest ecosystems, including:

'''Upland hardwoods'''
:Upland hardwoods are often found in patches, surrounded by flatwoods and sandhills.  Many species of trees prefer these types of ecosystems so there isn't a dominant species.  Many Florida State Parks are located in these types of ecosystems.

'''Bottomland hardwoods'''
:Bottomland hardwoods are very low, wet areas that are located within close proximity to lakes, rivers, and sinkholes, making them prone to flooding.  This environment propels the growth of deciduous trees that grow in layers with shrubs and herbaceous plants and are under constant change.

'''Sandhills'''
:Sandhills are very dry, with sandy soils so it is rare that these ecosystems ever flood.  Because of this sort of climate, fire often changes the landscape so that they are predominantly grass and trees with no shrubbery.

'''Scrub'''
:Scrub land is extremely low in nutrients because of its sandy soil caused by frequent fires.  Most often, scrubs consist of open pinelands with various oaks, shrubs, and palmetto.  These plants are called xerophytic because they grow well in dry climates and have roots close to the surface to catch what little nutrients they can.

'''Flatwoods'''
:Pine flatwoods are very low, flat, sandy lands that are subject to fires during some parts of the year, but may be flooded for months due to seasonal rainfall.  Pine needles contribute to a nutrient rich soil so plant growth is often rapid, allowing farmers to feed their livestock.

'''Tropical hammocks'''
:Tropical hammocks include many broad-leaved evergreens.  These forests are restricted to South Florida because of hard freezes in the North.  These areas are often used for land development because of their well-drained soils.

==Human impacts==
:The State of Florida's landscape was mostly made up of a largely forested area, prairies, and the large wetland area now referred to as the Everglades. Throughout its landscape small rivers, swamps and naturally occurring lakes and springs made up the state. At the time the area was inhabited by the native indigenous tribes of Florida. These tribes lead a mostly subsistence-based lifestyle. A subsistence-based lifestyle consists mostly of basic farming that would provide enough food for one family unit. This way of living minimally impacted the landscape as most of the time only fertile areas of non-swamp land were utilized as the technology to drain large portions of lands and redirect water were still not widely available.

:Over time, with the colonization of Florida, more and more people started to become attracted to the area. Once the technology to drain and redirect extensive areas of swamp land presented itself more and more came to lay claims to acres of land for future development. These large influxes of peoples led to the mass manipulation of the Florida landscape thus altering it permanently. Many of the activities that took place dealt with the diversion, draining or redirecting of water through the creation of various types of water ways like canals or manmade lakes, the cutting down of forests and the conversion of lands from natural to agricultural use. This intense and highly complex manipulation of the landscape caused quite a few problems for the native species of animals living there even though it solved many problems for the many new populations of people that would come to live there.<ref>{{cite web|url=http://carmelacanzonieri.com/library/6123/Solecki-HumanEnvInteractSoFloridaEverglades.pdf |format=PDF |title=Human–environment interactions in South Florida's Everglades region: Systems of ecological degradation and restoration |publisher=Carmelacanzonieri.com |accessdate=2015-09-27}}</ref>

=== Human expansion ===
:Once humans enter a natural environment that environment almost always instantly becomes changed. As humans we need certain resources and items to live. It is not uncommon to see an entire natural environment completely cleared or manipulated to make way for the development of residential, industrial, commercial or agricultural zones. Another threat posed by humans is the creation of busy interstate highways and roads that divide whole ecosystems in half and force many migratory land animals to change their migration routes.

:There are currently efforts being made to help improve the construction methods with which homes and businesses are built so that they may cause little to no damage to the environment around them.<ref>{{cite web|url=http://www.swfwmd.state.fl.us/yards/ |title=Florida-Friendly Landscaping |publisher=Swfwmd.state.fl.us |date= |accessdate=2015-09-27}}</ref>

===Water===
:Water is one of our most important and highly valued resources. It is used for farming, providing electricity, as well as plumbing, cleaning, drinking, bathing and many other things. This poses problems for the natural environment. Bodies of water, like lakes or ponds, are drained for the creation of homes or other facilities. Water can also be redirected so that certain areas that are creating new businesses or that have a large population of people moving in can have fresh clean water going directly to them instead of having to import water from other areas or buy it in large quantities to store for personal or commercial use.

:In the natural environment many animal species depend on the regular flow of water as well as specific bodies of water for their survival. Draining small lakes, ponds, and river beds gets rid of a habitat that many different species of fish, alligators, insects, and other animals were dependent on for their survival. Likewise redirecting water poses just as great a threat to native species, as it does to us. When water is redirected the original flow becomes disrupted, and limits the amount of water that can be obtained at other areas.

===Pollution of water===
:Runoff of pesticides, fertilizers, and chemicals from farming, factories, households, commercial and industrial uses causes imbalances within ecosystems. Toxic chemical runoff and byproducts from decomposing materials and foods can contaminate water supplies. Most importantly these chemicals, like mercury, wreak havoc in fisheries and cause problems like infertility, mutations, and sometimes death of the fish. Large agricultural and farming communities, as well as urban areas leak pollutants directly into the water supply that can then flow through natural environments. When pesticides and fertilizers get into the water plants are affected too. Fertilizers often contain phosphorus which can lead to an increase in growth of some water borne plants and foliage. This abnormally rapid growth can in turn cause other populations of water borne plants to dwindle because of competition for space.

===Deforestation===
:Forests provide many benefits to the environment. They create habitats for small and large animals, insects and small organisms like bacteria and fungi that feed on decomposing tree trunks. They also store carbon. Forests are like giant banks of stored carbon. When forests are cut down in large quantities tons and tons of previously stored carbon is released into the atmosphere. Aside from storing vast amounts of carbon they also help prevent soil erosion. Areas that have been dry and arid with bare exposed soil can be recovered by planting trees around a buffer area to prevent further soil erosion. With proper care, maintenance, and patience the area can be recovered.

:Deforestation [[Deforestation]] is the removal of all or some trees from an area of forest for use as something else. Florida is known for having a variety of different ecosystems aside from the wet marshlands we call the Everglades. It is also home to a variety of different kinds of forests. The trees and wood obtained from these forests are used for the construction of furniture, homes, or can be sold as individually sized boards and shapes for construction. In order to obtain these large amounts of wood whole sections of trees need to be cut down. Sometimes trees are cut down simply to get to an area of preference. Another reason for cutting down large sections of trees is for the construction of new homes or buildings in an aesthetically pleasing area. "Florida has lost 22% of forests since 1953 (a loss of 1.6 million ha)."<ref>{{cite web|url=https://fp.auburn.edu/sfws/sfnmc/class/distinguished/sld084.htm |accessdate=November 2, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20120502142538/https://fp.auburn.edu/sfws/sfnmc/class/distinguished/sld084.htm |archivedate=May 2, 2012 }}</ref>

==Climate change==
:The Florida coastline is already feeling the effects of global warming. It could change the look and appearance of Florida's coastline forever. "In fact, scientists have already observed changes in Florida consistent with the early effects of global warming: retreating and eroding shorelines, dying coral reefs, saltwater intrusion into inland freshwater aquifers, an upswing in forest fires, and warmer air and sea-surface temperatures.<ref>{{cite web|url=http://www.nrdc.org/globalwarming/nflorida.asp |accessdate=November 20, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20111028144431/http://www.nrdc.org/globalwarming/nflorida.asp |archivedate=October 28, 2011 }}</ref> "

===Introduced fauna and flora===
:Introduced species are species that are brought over from non native environments, for example from China to Florida. These species often times find that their new environment is perfect for  them and begin to grow and breed at extraordinary rates becoming invasive. "An invasive species is generally defined as a plant, animal or microbe that is found outside of its native range, where it negatively impacts the ecology, economy, or quality of human life.<ref name="WEC218/UW259: The Cuban Treefrog (Osteopilus septentrionalis)  in Florida" >{{cite web|url=http://edis.ifas.ufl.edu/uw259 |title=WEC218/UW259: The Cuban Treefrog (Osteopilus septentrionalis) in Florida |publisher=Edis.ifas.ufl.edu |date= |accessdate=2015-09-27}}</ref> " With nothing to keep them in check, since there  is nothing in the new environment to challenge their boundaries, or that feeds on it, the alien species will continue to take over and sometimes push native plants or animals out of their native environments.

:In Florida local and private groups have formed to help combat some of their invasive species of plants and animals. "The Florida Invasive Species Partnership (FISP) is a collaboration of federal, state and local agencies along with non-government organizations, all with a stake in managing invasive non-native species in Florida<ref>{{cite web|url=http://www.floridainvasives.org/ |title=Home - Florida Invasive Species Partnership |publisher=Floridainvasives.org |date=2015-09-23 |accessdate=2015-09-27}}</ref> "

:One example of a plant species that has spread abnormally is the Old World climbing fern (''[[Lygodium microphyllum]]'' (Cav.) R. Br.). This invasive vine can grow up to 90 feet and more, and can survive through the winter without losing much of its greenness. This vine has been known to cover whole sections, rows, and at times all of the trees in forests and line them in huge veils of sweeping green foliage. These vines pose serious fire hazards in dry areas or during dry seasons. The vine is native to the following countries:
:*Africa
:*Australia
:*Southeast Asia<ref>{{cite web|url=http://www.invasiveplantatlas.org/subject.html?sub=3046 |title=old world climbing fern: Lygodium microphyllum (Polypodiales: Lygodiaceae): Invasive Plant Atlas of the United States |publisher=Invasiveplantatlas.org |date=2015-05-15 |accessdate=2015-09-27}}</ref>
[[File:ARS Lygodium microphyllum.jpg|thumb|Old World fern taking over a forested area]]

:One example of an animal species that has bred without limit is the Cuban tree frog (''[[Osteopilus septentrionalis]]''). The Cuban tree frog found its way to Florida after hitching a ride in shipping containers on trading boats and ships. As its name implies, the frog is native to Cuba. Florida's warm, rainy and humid weather is the perfect environment for the Cuban tree frog to reproduce and breed. The only thing that seems to cause sharp declines in their populations are freezes, or unusually cold winters. These frogs have spread up and down the coast of Florida as well as around the central Florida area. They eat various types of insects and spiders, but are also cannibalistic and have caused the native Florida tree frog populations to decrease as they feed on them. "Cuban Treefrogs are 'sit-and-wait' predators. On warm nights in Florida, it is common to encounter Cuban Treefrogs hanging on walls and windows near lighted areas as they sit and wait for insects (and native treefrogs) to be attracted to the lights. As they feed, they defecate on the windows and walls, and their fecal deposits can become unsightly over time, especially if there are a lot of frogs in the area. Furthermore, when a person enters or exits his or her home at night, Cuban Treefrogs that are waiting for an insect meal may be startled and as a result will occasionally jump onto people or into their homes through open doors.<ref name="WEC218/UW259: The Cuban Treefrog (Osteopilus septentrionalis)  in Florida" /> " The frog is native to areas such as:
:*Cuba
:*Cayman Islands
:*Bahamas

===Endangering/extinction of Florida species===
:When a species becomes listed as endangered it is mostly due to a wide variety of causes usually linked to human activities.
:Some of these activities include:
:*developing land on animal habitats
:*over-hunting
:*intentional killing off of a species
:*the introduction of foreign species which compete for the same resources
:*pollution

:The state of Florida has about 33 animals, and 43 plant species that are endangered. Some of the animals that are listed are the Florida panther, the leather-back sea turtle, the West Indian manatee, and the red-cockaded woodpecker to name a few; and some of the plants that are listed are the bell-flower, scrub plum, Small's milk pea, and the water-willow.
:The following link will show a list of endangered species and their current status by the US Fish and Wildlife Services:[http://ecos.fws.gov/tess_public/pub/stateListingAndOccurrenceIndividual.jsp?state=FL ecos.fws.gov]
:The following link will show a chart listing information comparing states' and their native species by the US Fish and Wildlife Services:[http://ecos.fws.gov/tess_public/StateListingAndOccurrence.do?state=FL ecos.fws.gov]

==Migration patterns==
:When an animal migrates what they are doing is simply moving from one place to another and back to their original location. Animals migrate to find good breeding grounds or areas with large amounts of food. When man made objects or constructs get in the way of an animal's routine migratory path it is forced to change its usual breeding ground or area of sustenance.
'''Changes in the migration patterns of panthers'''
:The Florida panther is listed as endangered. This is because of the encroachment of developing lands and highways and other man made structures that have mostly destroyed or diminished their natural habitats. They also have trouble hunting the white-tailed deer, which is their main source of food, as they have been cut off from each other because of human developments as well.<ref>{{cite web|url=http://earthjustice.org/irreplaceable/florida_panther?gclid=CLyB45-7xawCFWICQAodl0lm9Q |title=Florida Panther |publisher=Earthjustice.org |date= |accessdate=2015-09-27}}</ref> Due to these developments Florida Panthers have had to change their migration routes, as well as become adjusted to smaller hunting and breeding grounds than they previously had.
[[File:Everglades National Park Florida Panther.jpg|thumb|Image of a Florida panther]]

==State policies==
:The [[Florida Department of Environmental Protection]] is the agency responsible for protecting Florida's ecology. Its self-stated mission is to protect "our air, water and land." It is divided into 41 programs that cover three areas of interest: Regulatory Programs, Land and Recreation, and Water Policy and Ecosystem Restoration.

===Regulatory programs===
<ref>{{cite web|url=http://www.floridaplants.com/ecorest.htm |title=Florida's Environment |publisher=Floridaplants.com |date= |accessdate=2015-09-27}}</ref>
:The DEP makes regulations and also follows up to make sure they are adhered to. Besides the normal administrative sections of the agency, there is an office of the Inspector General, which conducts audits and investigations related to preserving Florida's air, land and water. It provides an impartial judge to determine what should be done. They are supported by law enforcement and policy compliance sectors. There is also an office for siting coordination, which regulates the power grid and natural gas pipelines across the entire state.

===Land and recreation===
<ref>{{cite web|url=http://www.floridastateparks.org/ |title=Florida State Parks |publisher=Florida State Parks |date= |accessdate=2015-09-27}}</ref>
[[File:Manatee Springs State Park Florida springs04.jpg|thumb|Manatee Springs State Park Florida springs04]]

:The DEP is responsible for state-owned recreational land. This includes the entire state park system and most of Florida's beaches. There are also separate entities dealing with the trails and greenways ([[Florida Ecological Greenways Network]]) initiative, Green Lodging, and the Clean Marina program, to name just a few. The Front Porch Florida program also falls into this category; it is a program to help neighborhoods regain a sense of community. It hopes to make these communities a fun place to be. The Bureau of Beaches and Coastal Systems monitors Florida's fragile beach environments and works with local initiatives and the [[United States Army Corps of Engineers|Army Corps of Engineers]] to protect and restore the beaches. It also is responsible for disaster response initiatives, such as the [[Deepwater Horizon oil spill]] beach cleanup efforts. State-owned lands are under the supervision of this department, divided into the [[Florida State Parks]] program and the Public Lands program.

===Water policy and ecosystem restoration===
:Some programs from the other two categories also fall into this category, such as the Bureau of Beaches and Coastal Systems, because they deal with the restoration aspect of a larger issue. However, some programs are entirely within this category, such as the Wastewater Program and the [[Restoration of the Everglades|Everglades Restoration program]]. The Springs, Water and Wetlands programs all fall into this category. These programs perform important functions by monitoring the quality and quantity of Florida's drinking water.

==Progress==
[[File:Florida Locator Map.PNG|thumb|Florida Locator Map]]

===FERI===
:In 1998, the Office of Ecosystem Management conducted the Florida Ecological Restoration Inventory (FERI). Using information gathered from the managers of all state-owned lands, they assessed the restoration needs and created a comprehensive map including the urgency of each need. This became an online database of planned, needed and completed restoration projects and the information about them. In 2000, the Bureau of Submerged Lands and Environmental Resources was awarded a grant to update FERI and expand the database to include information from other agencies. There are currently six categories in FERI. They are: cultural resource protection, ecological protection, exotic removal, hydrologic restoration/enhancement, upland restoration/enhancement, and wetland restoration/enhancement.<ref>{{cite web|url=http://www.dep.state.fl.us/water/wetlands/feri/index.htm |title=Florida Ecological Restoration Inventory |publisher=Dep.state.fl.us |date=2011-10-24 |accessdate=2015-09-27}}</ref>

===Recovery Program===
: The DEP has initiated the Recovery Program, which uses [[ARAA]] federal stimulus money to fund environmental programs across the state. Diesel emissions reduction is receiving 1.7 million dollars to add electricity to rest stops so trucks do not have to idle and retrofitting school buses to make them more environmentally friendly. The Superfund program will receive $61 million to clean up hazardous waste from the [[Superfund]] hazardous waste sites. Leaking Underground Storage Tanks will use $11.2 million to clean up "orphan" petroleum storage tanks (abandoned tanks that have no party responsible for them). $750,000 from the EPA will go towards local [[brownfield land]] projects. The Clean Water State Revolving Fund will use $132.3 million to issue loans for communities to improve their [[waste water]] and [[storm water]] systems. The Drinking Water State Revolving Fund will use $88.1 million to issue loans for communities to upgrade their [[drinking water]] infrastructure.

==References==
{{Reflist|colwidth=30em}}

==External links==
*[http://edis.ifas.ufl.edu/mg068 Love bugs introduced species, University of Florida]
*[http://www.nrdc.org/energy/gulfspill/?gclid=CJvv8PHSw6wCFRAj7AodxWVYpQ Effects of the BP oil spill on Florida beaches]
*[http://www.uen.org/themepark/patterns/animalmigration.shtml Uen.org]
*[http://earthjustice.org/irreplaceable/florida_panther?gclid=CLyB45-7xawCFWICQAodl0lm9Q Earthjustice.org]
*[http://www.biologicaldiversity.org/campaigns/overpopulation/urban_wildlands/index.html Biologicaldiversity.org]
*[https://web.archive.org/web/20111028144431/http://www.nrdc.org/globalwarming/nflorida.asp Nrdc.org]
*[http://www.ces.fau.edu/climate_change Ces.fau.edu]
*[http://www.nature.org/ourinitiatives/urgentissues/climatechange/howwework/reducing-emissions-from-deforestation.xml?s_eng=google&s_ce=normal&s_med=ppc&s_dis=search&s_cs=text&s_cid=Deforestation+(GG)&s_ag=Facts&s_kwd=deforestation%2520facts&s_mt=e&gclid=CJrTh46sxawCFQFX7AodIjPHsg Nature.org]

[[Category:Articles created via the Article Wizard]]
[[Category:Ecology of the United States]]