{{Use mdy dates|date=July 2013}}
{{Infobox musical artist
| name          = Jennifer R. Blake
| image         = JBlake.jpg
| image_size    = 220
| birth_name    = 
| birth_date    = 
| birth_place   = 
| other_names   = 
| background   = solo_singer
| occupation   = [[Actress]], [[Singing|singer]]
| instrument   = Vocals
| years_active  = 1999–present
| website       = {{URL|http://www.jenniferblake.tv}}
}}
[[File:Jennifer Blake in the role of Caroline (Ma) in Prairie-Oke at the Cavern Club Theatre, Los Angeles.JPG|thumb|right|Jennifer Blake in the role of Caroline (Ma) in ''[[Prairie-Oke]]'' at the Cavern Club Theatre, Los Angeles]]
[[File:Jennifer Blake in the role of Mindy McCready, McCreadytheMusical.jpg|thumb|Jennifer Blake in the role of Mindy McCready, ''McCready the Musical'']]
'''Jennifer R. Blake''' is an American actress and producer who has performed in television, film and musical theatre.

==Career==
Jennifer Blake is a notable alumna of The [[Boston Conservatory]], where she earned a BFA in Musical Theatre and has appeared in television, film and musical stage plays in Boston, New York and Los Angeles.

She produced and starred in the role of ''[[Mindy McCready]]'' in ''McCready The Musical'' at Spirit Studio in Silver Lake, Los Angeles, May–June, 2015, reimagined and iterated during the Hollywood Fringe Festival, Los Angeles, June, 2016, and reprised Off-Broadway at The Triad Theater, New York City, September, 2016.<ref name="rollingstone">{{cite web|url=http://www.rollingstone.com/music/features/mindy-mccready-musical-to-open-in-los-angeles-20150421 | title=Mindy McCready Musical to Open in Los Angeles| publisher=Rolling Stone | date=21 April 2015 | accessdate=21 April 2015}}</ref><ref name="broadwayworld">{{cite web|url=http://www.broadwayworld.com/los-angeles/article/BWW-Interview-Jennifer-Blake-Chats-MCCREADY-THE-MUSICAL-Opening-in-Silverlake-58-20150507#| title=BWW Interview: Jennifer Blake Chats MCCREADY THE MUSICAL, Opening in Silverlake 5/8| publisher=BroadwayWorld | date=7 May 2015 | accessdate=7 May 2015}}</ref><ref name="popculturalist">{{cite web|url=http://pop-culturalist.com/pop-culturalist-chats-with-jennifer-blake/| title=PopCulturalist Chats with Jennifer Blake| publisher=Pop-Culturalist | date=24 August 2016 | accessdate=24 August 2016}}</ref> She was also co-creator and co-producer of the show in New York City.

She co-starred in ''[[Behaving Badly (film)]]'' (2014).  She guest-starred in the prime-time comedy series ''[[How to Live with Your Parents (for the Rest of Your Life)]]''<ref name="spoilertv">{{cite web|url=http://www.spoilertv.com/2013/05/how-to-live-with-your-parents-episode_6.html | title=How To Live With Your Parents - Episode 1.08 - How to Live With Your Parents for the Rest of Your Life - Press Release | publisher=Spoilertv | date=6 May 2013 | accessdate=11 July 2013}}</ref> in the role of Nirvana on [[American Broadcasting Company|ABC]].

One of her most notable roles in musical theatre has been as the character Susan in the 2010 Los Angeles premiere of ''[[title of show|[title of show]]]'', at the [[Celebration Theatre]].<ref>{{cite web|url=http://latimesblogs.latimes.com/culturemonster/2010/07/theatre-review-title-of-show-at-celebration-theatre.html|title=Theatre review: "[title of show]" at Celebration Theatre | publisher=Los Angeles Times | date=22 July 2010 | accessdate=11 July 2013}}</ref><ref>{{cite web|url=http://variety.com/2010/legit/reviews/title-of-show-1117943188/|title=Review: "[title of show]" | publisher=Variety | date=18 July 2010 | accessdate=11 July 2013}}</ref><ref>{{cite web|url=http://www.latheatrereview.com/2010/07/22/title-of-show-at-the-celebration-theatre/|title=[title of show] at the Celebration Theatre| publisher=L. A. Theatre Review | date=22 July 2010 | accessdate=11 July 2013}}</ref><ref>{{cite web|url=http://dailybruin.com/2010/08/30/theater_review_/|title=Theater Review: "(title of show)"| publisher=UCLA Daily Bruin | date=30 August 2010 | accessdate=11 July 2013}}</ref><ref>{{cite web|url=http://stagehappenings.com/Dale_Reynolds/reviews/_2010/titleofshow.php|title=Theater Review: "(title of show)"| publisher=Stage Happenings | date=22 July 2010 | accessdate=11 July 2013}}</ref><ref>{{cite web|url=http://www.backstage.com/review/la-theater/title-of-show/|title=L. A. Theater Review: "(title of show)"| publisher=Backstage | date=21 July 2010 | accessdate=11 July 2013}}</ref><ref>{{cite web|url=http://losangeles.broadwayworld.com/article/BWW-Reviews-LA-Premiere-of-Witty-title-of-show-Is-Hilarious-Fun-20100719|title=BWW Reviews: L.A. Premiere of Witty '[title of show]' Is Hilarious Fun | publisher=Broadway World | date=19 July 2010 | accessdate=11 July 2013}}</ref><ref>{{cite web|url=http://www.huffingtonpost.com/james-scarborough/title-of-show-celebration_b_698279.html|title=[title of show], Celebration Theatre, Los Angeles | publisher=Huffington Post | date=30 August 2010 | accessdate=11 July 2013}}</ref><ref>{{cite web|url=http://www.laweekly.com/2010-07-29/stage/singin-in-the-rain/|title=Singin' in the Rain [title of show] and Dani Girl | publisher=L. A. Weekly | date=29 July 2010 | accessdate=11 July 2013}}</ref>  She was called in for an audition and remarkably learned the role in three weeks.<ref>{{cite web|url=http://www.lastagetimes.com/2010/08/facing-your-vampires-in-title-of-show/ |title=Facing Your Vampires | publisher=LAStageTimes | date=10 August 2010 | accessdate=12 July 2013}}</ref><ref>{{cite web|url=http://staylorellis.wordpress.com/tag/jennifer-r-blake/ |title=[title of show]: Celebration Theatre, 9/3/10| publisher=wordpress | date=3 Oct 2010 | accessdate=13 July 2013}}</ref>  Her performance was awarded Outstanding Performance by a Lead Actress in a Musical by StagesceneLA.<ref name="2009-2010 StageSceneLA Scenies">[http://www.stagescenela.com/2009-2010-scenies/ ''2009-2010 StageSceneLA Scenies'']</ref>

Jennifer Blake was also critically esteemed for her lead role in ''[[Side By Side By Sondheim]]'' at The Attic Theatre, Los Angeles,<ref>{{cite web|url=http://www.laweekly.com/2010-03-18/stage/theater-reviews-hot-pants-cold-feet-liberty-inn-side-by-side-by-sondheim/|title=Theater Reviews: Hot Pants, Cold Feet, Liberty Inn, Side by Side by Sondheim | publisher=L. A. Weekly | date=18 Mar 2010 | accessdate=11 July 2013}}</ref><ref>{{cite web|url=http://www.laweekly.com/2010-03-18/calendar/side-by-side-by-sondheim/ | title=Side by Side by Sondheim | publisher=L. A. Weekly | date=18 Mar 2010 | accessdate=11 July 2013}}</ref><ref>{{cite web|url=http://blogs.laweekly.com/arts/2010/03/stage_raw_extropia.php |title=Stage Raw: Extropia | publisher=L. A. Weekly | date=15 Mar 2010 | accessdate=11 July 2013}}</ref><ref>{{cite web|url=http://www.playbill.com/news/article/136p854-Ashman-Blake-Duke-Donohoe-and-Sarando-Set-for-Attic-Theatres-Side-by-Side-by-Sondheim |title=Ashman, Blake, Duke, Donohoe and Sarando Set for Attic Theatre's Side by Side by Sondheim | publisher=Playbill | date=12 Feb 2010 | accessdate=12 July 2013}}</ref> and was awarded Outstanding Performance by an Ensemble Cast/Musical by StageSceneLA.<ref name="2009-2010 StageSceneLA Scenies"/>

She was in the original cast of the [[Off-Broadway]] New York City
production of ''[[The Donkey Show (musical)|The Donkey Show]], ''in the roles of Disco Girl/Swing for Hellen/Vinnie/Mia/Oberon''.<ref>{{cite web|url=http://www.lortel.org/LLA_archive/index.cfm?search_by=show&title=The%20Donkey%20Show%3A%20A%20Midsummer%20Night%27s%20Disco  |title=The Donkey Show: A Midsummer Night's Disco | publisher=Lortel Archives, The Off-Broadway Internet Database | date=18 Aug 1999 | accessdate=12 July 2013}}</ref><ref>{{cite web|url=http://broadwayworld.com/shows/cast.php?showid=326170&cast_type=original# |title=The Donkey Show: A Midsummer Night's Disco Off-Broadway Cast | publisher=Broadway World | date=30 Sep 1999 | accessdate=12 July 2013}}</ref><ref>{{cite web|url=http://libserv23.princeton.edu/princetonperiodicals/cgi-bin/princetonperiodicals?a=d&d=Princetonian19990930-01.2.24&srpos=11&e=-------en-20--1--txt-IN-rachel+crew-ILLUSTRATION---# |title='The Donkey Show' shakes up Shakespeare | publisher=The Daily Princetonian | date=30 Sep 1999 | accessdate=12 July 2013}}</ref>

She earned critical acclaim for her 2013 role of Caroline (Ma) in ''[[Prairie-Oke!]]'' at the Cavern Club Theatre, Los Angeles.<ref>{{cite web|url=http://www.lamag.com/laculture/culturefilesblog/2013/03/08/forever-young-judy-blume-laura-ingalls-wilder-become-cheeky-musicals | title=Forever Young: Judy Blume & Laura Ingalls Wilder Become Cheeky Musicals | publisher=LAMag | date=8 Mar 2013 | accessdate=11 July 2013}}</ref><ref>{{cite web|url=http://allevents.in/Los%20Angeles/Little-House-on-the-Prairie-oke/417279948359174 | title=Little House on the Prairie-oke! | publisher=AllEvents | date=5 April 2013 | accessdate=11 July 2013}}</ref><ref>{{cite web|url=http://www.laimyours.com/40317/whoa-nellie-get-ready-for-little-house-on-the-prairie-oke/ | title=Whoa, Nellie: Get Ready For Little House On The Prairie-Oke! | publisher=laimyours | date=29 Mar 2013 | accessdate=11 July 2013}}</ref><ref>{{cite web|url=http://www.out.com/entertainment/popnography/2013/03/28/little-house-prairie-oke-drew-droege | title=Need to Know: 'Little House on the Prairie-Oke' | publisher=Out | date=29 Mar 2013 | accessdate=11 July 2013}}</ref><ref>{{cite web|url=http://www.stagescenela.com/2013/08/prairie-oke/ | title=Prairie-Oke! | publisher=stagescenela | date=16 Aug 2013 | accessdate=31 Aug 2013}}</ref> and for her 2012 lead role in  ''[[Are You There God? It's Me, Karen Carpenter]]'' at the Hudson Mainstage (Los Angeles), reprised in 2013 at the Palm Canyon Theater (Palm Springs, CA).<ref>{{cite web|url=http://articles.latimes.com/2012/aug/23/entertainment/la-et-guidefeature-20120823 | title='Are You There God? It's Me, Karen Carpenter' parodies teen angst | publisher=Los Angeles Times | date=23 Aug 2013 | accessdate=11 July 2013}}</ref><ref>{{cite web|url=http://www.huffingtonpost.com/xaque-gruber/are-you-there-god-musical_b_2235262.html | title=Karen Carpenter Meets Judy Blume in a Glorious Musical at Hollywood's Hudson Theatre | publisher=Huffington Post | date=4 Dec 2012 | accessdate=11 July 2013}}</ref><ref>{{cite web|url=http://www.stagescenela.com/2012/08/are-you-there-god-its-me-karen-carpenter/ | title=ARE YOU THERE GOD? IT'S ME, KAREN CARPENTER | publisher=StageSceneLA | date=17 Aug 2012 | accessdate=11 July 2013}}</ref><ref>{{cite web|url=http://www.palmspringslife.com/Palm-Springs-Life/Desert-Guide/October-2013/Things-to-Do-This-Weekend-Oktoberfest-AIDS-Walk-En-Vogue/ | title=ARE YOU THERE GOD? IT'S ME, KAREN CARPENTER | publisher=Palm Springs Life | date=17 Oct 2013 | accessdate=17 Oct 2013}}</ref>

She appeared in a musical based upon the life of ''[[Charles Bukowski]]'', ''[[Bukowsical!]]'', (Swing) at the [[Sacred Fools Theater Company]], Los Angeles, a production which was taken to New York City and presented at the [[New York International Fringe Festival]], and the show was awarded as Outstanding Musical.<ref>{{cite web|url=http://www.nytheatre.com/Review/lois-spangler-2007-8-12-bukowsical | title=BUKOWSICAL! | publisher=NY Theatre | date=12 August 2007 | accessdate=12 July 2013}}</ref><ref>{{cite web|url=http://variety.com/2006/legit/reviews/bukowsical-1200517590/ |title=Review: "Bukowsical!" | publisher=Variety | date=19 Mar 2006 | accessdate=12 July 2013}}</ref><ref>{{cite web|url=http://articles.latimes.com/2006/mar/24/entertainment/et-stage24 |title='Bukowsical!' is seeking support | publisher=Variety | date=24 Mar 2006 | accessdate=12 July 2013}}</ref><ref>{{cite web|url=http://www.thescl.com/Gary_Stockdale_wins_at_2007_New_York_International_Fringe_Festival | title=Gary Stockdale wins Outstanding Music Award at 2007 New York International Fringe Festival | publisher=thescl | date=Aug 2007 | accessdate=13 July 2013}}</ref> She participated in the original cast recording.<ref>{{cite web|url=http://www.kritzerland.com/buk.htm | title=BUKOWSICAL! | publisher=Kritzerland | date=19 Feb 2011 | accessdate=13 July 2013}}</ref>

Other notable roles include ''[[Hair (musical)|Hair]]'' in the role of Jeannie at the Bay Street Theatre, New York;  ''[[Jesus Christ Superstar]]'' in the role of Mary Magdalene at The Attic Theatre, Los Angeles;<ref>{{cite web|url=http://www.stagescenela.com/2008/03/jesus-christ-superstar-2/ |title=JESUS CHRIST SUPERSTAR | publisher=StageSceneLA | date=Mar 2008 | accessdate=12 July 2013}}</ref>  ''[[Chess (musical)|Chess]]'' in the Ensemble and in the role of Vanity Fair Reporter at The Lyric Theatre, Boston; and ''[[Ex(it) Wounds]]'' at IO Mainstage, Los Angeles.;<ref>{{cite web|url=http://losangeles.broadwayworld.com/article/BWW-Reviews-EXIT-WOUNDS-Takes-a-Trashingly-Refreshing-Look-at-Real-Break-Up-Letters-20130208 |title=BWW Reviews: EX(IT) WOUNDS Takes a Trashingly Refreshing Look at Real Break-Up Letters | publisher=BroadwayWorld | date=19 Mar 2006 | accessdate=12 July 2013}}</ref>  ''[[The Crucible]]'' in the role of Betty Parris at the ''[[Alabama Shakespeare Festival]]'';  ''[[Grease (musical)|Grease]]'' in the role of Marty at the West Virginia Public Theatre;  and  ''[[The Will Rogers Follies]]'' in the role of Ziegfeld Girl at the Galveston Island Outdoor Musicals, Texas.

Her television appearances have included ''[[Late Show with David Letterman]]'', broadcast on [[CBS]], Sketch Artist; ''[[Sex and the City]]'', Co-Star;  ''[[Contest Searchlight]]'', Co-Star;  and in the series ''[[How to Live with Your Parents (for the Rest of Your Life)]]''<ref name="spoilertv" /> in the role of Nirvana on [[American Broadcasting Company|ABC]].  She also sang on ''[[Ryan Adams]]''' album ''[[Love is Hell (Ryan Adams album)|Love is Hell]]'', B sides.

She was producer/actor in "The Bar", a comedy web series about the South.

==Filmography==
{| class="wikitable sortable"
|+ Film
|-
! Year
! Title
! Role
! class="unsortable" | Notes
|-
| 2015
| "The Bar" web comedy series
| T.K.
|
|-
| 2014
| ''[[Behaving Badly (film)|Behaving Badly]]''
| Janice
| 
|-
| 2013
| ''[[How to Live with Your Parents (for the Rest of Your Life)]]''<ref name="spoilertv" />
| Nirvana
|TV series [[American Broadcasting Company|ABC]]
| 
|-
| 2012
| ''[[Jayne Mansfield's Car]]''
| (ADR Loop Group)
| 
|-
| 2010/I
| ''[[Hello (short)(I)]]''<ref>{{cite web|url=http://www.imdb.com/title/tt1683877/  |title=Hello (I) | publisher=imdb | date=19 June 2010 | accessdate=13 July 2013}}</ref>
| Jennifer Randall
| 
|-
| 2008
| ''[[Simon (short)]]''<ref>{{cite web|url=http://www.imdb.com/title/tt1210103/  |title=Simon | publisher=imdb | date=10 Apr 2008 | accessdate=13 July 2013}}</ref>
| Anna
| 
|-
| 2006
| ''[[My Mother's Hairdo (short)]]''<ref>{{cite web|url=http://www.imdb.com/title/tt0810086/  |title=My Mother's Hairdo | publisher=imdb | date=5 Apr 2006 | accessdate=13 July 2013}}</ref>
| Dancer at the Hairdo
|
|-
| 2004
| ''{{sortname|The|Stepford Wives|The Stepford Wives (2004 film)}}''
| Stepford wife
|
|-
| 2001
| ''[[Zoolander]]''
| Fashionista
|  
|-
|}

==Awards==
Outstanding Performance by a Lead Actress in a Musical for the role of Susan in ''[[title of show|[title of show]]]'' from StagesceneLA's Best of LA Theatre Awards 2009-2010<ref name="2009-2010 StageSceneLA Scenies"/>

Outstanding Performance by an Ensemble Cast/Musical for ''[[Side By Side By Sondheim]]'' from StagesceneLA's Best of LA Theatre Awards 2009-2010<ref name="2009-2010 StageSceneLA Scenies"/>

Memorable Performance by an Ensemble Cast/Musical Spoof for ''[[Prairie-Oke]]'' from StagesceneLA's Best of LA Theatre Awards 2012-2013<ref>{{cite web|url=http://www.stagescenela.com/2012-2013-stagescenela-scenies/  |title=2012-2013-stagescenela-scenies | publisher=stagescenela | date=3 September 2013 | accessdate=29 September 2013}}</ref>

==Community volunteer activities==
Blake has given generously of her time to children in need through CoachArt, Los Angeles, a non-profit organization offering free lessons in the arts and athletics to patients with chronic illnesses and their siblings, ages 6 – 18.<ref>{{cite web|url=http://www.coachart.org/volunteer-faq.php  |title=Volunteer FAQs | publisher=CoachArt | date=12 July 2013 | accessdate=12 July 2013}}</ref> She also traveled to Delhi, India, as a volunteer on a build in the Bawana district for [[Habitat for Humanity]] International's Global Village Program.<ref>{{cite web|url=http://gv8319india.wordpress.com/about/ |title=GV in India | publisher=Habitat for Humanity| date=13 July 2013 | accessdate=13 July 2013}}</ref><ref>{{cite web|url=http://www.habitat.org/disaster/2004/asia_tsunami_archive/2006/08_24_2006_Habita_And_Roman_Catholic_Chetanalaya_Build_150_Houses_India.aspx |title=Habitat For Humanity And Roman Catholic Chetanalaya To Build 150 Houses For Former Delhi Slum Dwellers In India | publisher=Habitat for Humanity| date=24 Aug 2006 | accessdate=13 July 2013}}</ref><ref>{{cite web|url=http://www.habitat.org/where-we-build/india |title=Habitat for Humanity India | publisher=Habitat for Humanity| date=12 July 2013 | accessdate=12 July 2013}}</ref>

She produced the ''California for Alabama Benefit at MBAR'', which was for the victims of the 2011 tornadoes that killed hundreds and left thousands homeless or with significant damage.  The event was to benefit the local charity California for Alabama: California residents by way of Alabama collecting and delivering donations to the areas of Alabama ravaged by the April 27, 2011 tornado outbreak.<ref>{{cite web|url=http://allevents.in/Los%20Angeles/California-for-Alabama-Benefit/219000038143972 |title=California for Alabama Benefit | publisher=All Events in Los Angeles | date=3 Aug 2011 | accessdate=12 July 2013}}</ref>

Among her other volunteer interests are East L.A. Classic Theatre, the nation’s only Latino based Theater Company that offers professionally performed Shakespeare adapted for youth in underserved communities of color<ref>{{cite web|url=http://www.eastlaclassic.org/index.html |title=East L.A. Classic Theatre | publisher=East L.A. Classic Theatre | date=12 July 2013 | accessdate=12 July 2013}}</ref> and ENRICHLA Interdisciplinary Garden Program, an Environmental Non-profit focused on helping students build a good work ethic, promoting thoughtful and healthy eating habits, and adding edible school gardens to public schools.<ref>{{cite web|url=http://enrichla.org/garden-programs/ |title=Programs | publisher=ENRICHLA | date=12 July 2013 | accessdate=12 July 2013}}</ref><ref>{{cite web|url=http://enrichla.smugmug.com/GardenBuilds/Quincy-Jones-Elementary-and/22477811_ttDP84#!i=1797450890&k=8HGmqtL |title= Quincy Jones Elementary and Synergy Charter BUILD 4/14/12 | publisher=ENRICHLA | date=14 Apr 2012 | accessdate=12 July 2013}}</ref>

She has also had a part in The Creative Coalition, the premier nonprofit, nonpartisan social and political advocacy organization of the entertainment industry.<ref>{{cite web|url=http://thecreativecoalition.org/ |title=The Creative Coalition | publisher=The Creative Coalition | date=12 July 2013 | accessdate=12 July 2013}}</ref> and The National Lab Network, a national initiative that connects K-12 teachers with science, technology, engineering and mathematics professionals to bring hands-on learning experiences to students in all 50 states<ref>{{cite web|url=http://www.nationallabnetwork.org/ |title=The National Lab Network | publisher=The National Lab Network | date=12 July 2013 | accessdate=12 July 2013}}</ref>

{{Details|List of Boston Conservatory people#The Theater Division}}

== References==
{{Reflist}}

==External links==
{{commons category}}
*{{IMDb name|id=1631600|name=Jennifer R. Blake}}
*[http://jenniferblake.tv Jennifer R. Blake, Actor]

{{DEFAULTSORT:Blake, Jennifer R.}}
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:Actresses from Alabama]]
[[Category:American television actresses]]
[[Category:21st-century American actresses]]
[[Category:American film actresses]]
[[Category:Boston Conservatory alumni]]