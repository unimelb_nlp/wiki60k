{{Infobox journal
| title = Australasian Journal of Bone & Joint Medicine
| discipline = [[Orthopedics]]
| publisher = [[Elsevier|Excerpta Medica Communications]]
| country = Australia
| history = 2002–2005
| OCLC = 223430052
| ISSN = 1447-5529
}}
The '''''Australasian Journal of Bone & Joint Medicine''''' (originally titled the '''''Australasian Journal of Musculoskeletal Medicine'''''<ref name="nsw">{{cite web |title=Australasian journal of musculoskeletal medicine. |work=Catalogue |publisher=[[State Library of New South Wales]] |url=http://library.sl.nsw.gov.au/record=b2144721~S2 |accessdate=May 9, 2009 }}</ref>) was a periodical presented in the style of a [[scientific journal]], published by [[Elsevier]] but established and funded by pharmaceutical company [[Merck & Co.|Merck]]. Publication began in 2002,<ref name="nsw"/> and the last issue appeared in 2005.<ref>{{cite web |title=Australasian journal of bone & joint medicine. |work=Catalogue |publisher=[[State Library of New South Wales]] |url=http://library.sl.nsw.gov.au/record=b2139553~S2 |accessdate=May 9, 2009 }}</ref><ref name="ft">{{cite news
 | title = Elsevier admits journal error
 | author = Salamander Davoudi
 | author2 = Andrew Jack
 | url = http://www.ft.com/cms/s/0/c4a698ce-39d7-11de-b82d-00144feabdc0.html
 | work = [[Financial Times]]
 | location = London
 | issn = 0307-1766
 | date = May 6, 2009
 | accessdate = Nov 20, 2009
 }}</ref> According to ''[[The Scientist (magazine)|The Scientist]]'':

{{quote|Merck paid an undisclosed sum to Elsevier to produce several volumes of [''Australasian Journal of Bone and Joint Medicine''], a publication that had the look of a peer-reviewed medical journal, but contained only reprinted or summarized articles—most of which presented data favorable to Merck products—that appeared to act solely as marketing tools with no disclosure of company sponsorship.<ref>{{cite web
| first = Tom
| last = Lamb
| title = The Tale Of Merck's Fake Medical Journal As Told At A Vioxx Trial In Australia
| url = http://www.drug-injury.com/druginjurycom/2009/04/the-tale-of-mercks-fake-medical-journal-as-told-at-vioxx-trial-in-australia.html
| date = Apr 30, 2009
| accessdate = Nov 20, 2009
}}</ref><ref>{{cite web
| first = Bob
| last = Grant
| title = Merck published fake journal
| url = http://www.the-scientist.com/blog/display/55671/
| work = [[The Scientist (magazine)|The Scientist]]
| date = Apr 30, 2009
| accessdate = Nov 20, 2009
| archiveurl= https://web.archive.org/web/20091031192918/http://www.the-scientist.com/blog/display/55671/| archivedate= 31 October 2009 <!--DASHBot-->| deadurl= no}}</ref>}}

The publication was not included in the [[MEDLINE]] literature database and did not have its own website.<ref>{{cite web
| first = Summer
| last = Johnson
| title = Merck Makes Phony Peer-Review Journal
| url = http://blog.bioethics.net/2009/05/merck-makes-phony-peerreview-journal/
| date = May 1, 2009
| accessdate = Nov 20, 2009
}}</ref>

In May 2009, Elsevier admitted that a series of similar industry sponsored publications had been produced, and that "high standards for disclosure were not followed in this instance".<ref name="ft"/> In a formal statement, the CEO of Elsevier's Health Sciences Division, Michael Hansen, admitted that the practice was "unacceptable", and expressed regret for the publications.<ref>{{cite press release
 | title = Statement From Michael Hansen, CEO Of Elsevier's Health Sciences Division, Regarding Australia Based Sponsored Journal Practices Between 2000 And 2005
 | publisher = [[Elsevier]]
 | date = May 7, 2009
 | format =
 | language =
 | url = http://www.elsevier.com/wps/find/authored_newsitem.cws_home/companynews05_01203
 | accessdate = Nov 20, 2009
 | archiveurl =
 | archivedate =
 | quote = It has recently come to my attention that from 2000 to 2005, our Australia office published a series of sponsored article compilation publications, on behalf of pharmaceutical clients, that were made to look like journals and lacked the proper disclosures. This was an unacceptable practice, and we regret that it took place.
 }}</ref> Merck has denied claims that articles within it were [[ghost writer|ghost written]] by Merck and has stated that the articles were all [[reprint]]ed from [[Peer review|peer-reviewed]] [[medical journals]].<ref>{{cite press release
 |title=Merck Responds to Questions about the Australasian Journal of Bone and Joint Medicine Journal
 |date=Apr 30, 2009
 | url=http://www.merck.com/newsroom/vioxx/pdf/statement_20090430.pdf
 |publisher=Merck & Co.
 | accessdate = Nov 20, 2009
 | format=[[PDF]] | archiveurl= https://web.archive.org/web/20091231104929/http://merck.com/newsroom/vioxx/pdf/statement_20090430.pdf| archivedate= 31 December 2009 <!--DASHBot-->| deadurl= no}}</ref>

Several medical experts stated that their names were included in the Honorary Editorial Board of the ''Australasian Journal of Bone and Joint Medicine'' without their knowledge and consent.<ref>[http://abcnews.go.com/Health/Drugs/story?id=7577646&page=1 Aussie Civil Suit Uncovers Fake Medical Journals.] ABCNews, May 14, 2009. Accessed January 5, 2010</ref><ref>[http://www.theaustralian.com.au/news/doctor-not-told-about-vioxx-role/story-0-1225734663308 Doctor not told about Vioxx 'role'.] [[The Australian]], May 9, 2009. Accessed January 5, 2010</ref>

There were six such "industry-sponsored" publications brought out by Elsevier without proper disclosure of their nature, and which had the superficial appearance of a legitimate independent journal.<ref>{{cite news
 | title = The danger of drugs … and data
 | first = Ben
 | last = Goldacre
 | authorlink = Ben Goldacre
 | url = https://www.theguardian.com/commentisfree/2009/may/09/bad-science-medical-journals-companies
 | location = London
 | issn = 0261-3077
 | work = [[The Guardian]]
 | date = May 9, 2009
 | accessdate = Nov 20, 2009
 | archiveurl= https://web.archive.org/web/20091117200937/http://www.guardian.co.uk/commentisfree/2009/may/09/bad-science-medical-journals-companies| archivedate= 17 November 2009 <!--DASHBot-->| deadurl= no}}</ref><ref>{{cite news
 |title=Elsevier published 6 fake journals 
 |first=Bob 
 |last=Grant 
 |url=http://www.the-scientist.com/templates/trackable/display/blog.jsp?type=blog&o_url=blog/display/55679&id=55679 
 |accessdate=Nov 20, 2009 
 |date=May 7, 2009 
 |work=[[The Scientist (magazine)|The Scientist]] 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20091004183113/http://www.the-scientist.com/templates/trackable/display/blog.jsp?type=blog&o_url=blog/display/55679&id=55679 
 |archivedate=2009-10-04 
 |df= 
}}</ref> The six publications involved were
* ''Australasian Journal of General Practice''
* ''Australasian Journal of Neurology''
* ''Australasian Journal of Cardiology''
* ''Australasian Journal of Clinical Pharmacy''
* ''Australasian Journal of Cardiovascular Medicine''
* ''Australasian Journal of Bone & Joint Medicine''

==See also==
* [[Academic dishonesty]]
* [[Vioxx]]

==References==
{{reflist|30em}}

==External links==
* [http://images.the-scientist.com/pdfs/blogs/MSD0503540001.pdf Volume 2 (2003), Issue 1]
* [http://images.the-scientist.com/pdfs/blogs/MSD0503540027.pdf Volume 2 (2003), Issue 2]

{{DEFAULTSORT:Australasian Journal of Bone and Joint Medicine}}
[[Category:Ethics of science and technology]]
[[Category:Elsevier academic journals]]
[[Category:Publications established in 2002]]
[[Category:Publications disestablished in 2005]]
[[Category:Orthopedics journals]]
[[Category:English-language journals]]
[[Category:2002 establishments in Australia]]