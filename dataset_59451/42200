<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=
 | image=Albert TE.1-flying.png
 | caption=
}}{{Infobox Aircraft Type
 | type=Light [[sports aircraft]]
 | national origin=France
 | manufacturer=[[Avions Albert]]
 | designer=[[Robert Duhamel]]
 | first flight=before April 1926
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=at least 4
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Albert TE.1''' was a single seat [[cantilever]] [[parasol wing]] [[monoplane]], wood framed and skinned and built in France in 1926. It made some notable long flights, set a French altitude record for its class and proved a practical light aircraft. 

==Design and development==

During the early 1920s, considerable effort across northern [[Europe]] went into the development of very small and economical aircraft, exemplified for example by those at the British [[Lympne light aircraft trials ]] and at the 1925 meeting at [[Vauville, Manche|Vauville]].  Despite some progress with, for example, the [[Pander D]] or the [[Caudron C.109]], suitable engines were few; aircraft could take off on as little as {{convert|20|hp|kW|abbr=on|order=flip}} but not do much more and available {{convert|60|hp|kW|abbr=on|order=flip}} units were heavy. Albert Aviation decided that the {{convert|40|hp|kW|abbr=on|order=flip}} [[Salmson 9 AD]] nine-cylinder air-cooled [[radial engine]] was the best compromise.<ref name=Laero1/>

They put this engine into a small, single seat aircraft called the Albert TE.1<ref name=Laero1/><ref name=Flight/> designed by Robert Duhamel.<ref name=NACA/> The TE, or Te in the designation acknowledged the use of multi-layer [[mahogany]] skinning methods developed by [[Alphonse Tellier]] and widely applied to the construction of early [[monocoque]] [[fuselage]]s.<ref name=Opdycke/> It had a cantilever, one-piece parasol wing built around two wooden [[spar (aeronautics)#Box spar|box spar]]s and covered with [[plywood]].<ref name=Laero1/>  In plan its [[trailing edge]] was straight and unswept and over the inner 50% of the span the [[leading edge]] was parallel to it, but beyond curved away semi-elliptically.  The wing was attached to the raised centre of the fuselage and braced to each fuselage side with a pair of very short [[strut]]s. With only six attachment points, involving twelve bolts, it was easy to separate wing and fuselage for transport. Narrow, long span [[aileron]]s filled more than two-thirds of the trailing edge; these were operated by [[control rod]]s, rather than wires.<ref name=NACA/>

The fuselage was constructed from [[spruce]] and ply box girders and was ply covered, with flat sides and bottom and a pitched top.<ref name=NACA/> The engine was mounted uncowled on a steel tube frame<ref name=NACA/> and the open single [[cockpit]] was half under the trailing edge, allowed clear views above and below the wing.<ref name=Laero1/>  Its empenange was conventional and of similar construction to the wing; the [[tailplane]] was  mounted at mid-fuselage and had a plan similar to that of the wing, with full span, narrow [[chord (aeronautics)|chord]] [[elevator (aeronautics)|elevator]]s controlled by rods.  The [[fin]] was [[circular sector|quadrant]] shaped and carried a cable controlled semi-circular [[rudder]] that extended down as far as the tailplane.<ref name=Laero1/><ref name=NACA/>

The TE.1 had a wide track ({{convert|1550|mm|in|abbr=on}}) tailskid [[landing gear|undercarriage]]<ref name=Laero1/> with mainwheels on [[aircraft fairing| faired]], cranked half-axles hinged from the central fuselage underside, their ends independently [[bungee cord|bungee sprung]] from the vertices of faired V-struts from the lower fuselage [[longeron]]s.  Its tailskid was a double [[cantilever]] steel [[leaf spring]].<ref name=NACA/>

The exact date of the first flight of the TE.1 remains uncertain but it had completed its official testing at [[Vélizy – Villacoublay Air Base|Villacoublay]] before April 1926.<ref name=Laero1/> It was economical, with an optimum fuel consumption of about 16 km/l or 45 mpg, and was fully aerobatic.  It was proposed as a potential single-seat trainer, a mail plane or a military communications aircraft; it could also be equipped with a [[machine gun]] "in place of cavalry".<ref name=NACA/>

Three different Albert TE.1s were at the two Orly light plane contests<ref name=Flight/><ref name=Flight1/> and another was built under licence in the U.S..<ref name=Laero4/>

There is a report from the 1928 Orly event that Avions Albert were constructing a more powerful version, the '''Albert TE.2'''.  It had a {{convert|95|hp|kW|abbr=on}} engine and seated two, side-by-side, for training.<ref name=Laero5/>  It is not known if this aircraft was completed.

==Operational history==

In the summer of 1926 Thoret flew a TE.1 on two notable out and return flights.  The first, flown in six stages, each lasting between five and nine hours, was from [[Paris]] (Villacoublay) to Venice. He left on 5 June 1926 and returned eleven days later after flying some {{convert|2500|km|mi|abbr=on}} and crossing the [[Alps]] at {{convert|3000|m|ft|abbr=on}}. The following month he flew from Paris to [[Warsaw]], leaving on 16 July and arriving, via [[Prague]], the next day. Early next morning he took off for Paris, returning non-stop in just over ten hours and ending a round trip of {{convert|3000|km|mi|abbr=on}}.<ref name=Laero2/>

The same aircraft, along with another TE.1, was amongst eight contestants in the ''Concours d'Avions Économiques'' or light-plane contest held at [[Orly]] between 9-15 August 1926.   The aim was to decide the most practical of the five different types, including ease of folding/wing detachment for road transport, the ability to accommodate parachutes and fire protection, as well as performance (take off distance, climb, speed) and fuel efficiency; more controversial was an economy coefficient which deliberately enhanced the final scores of two-seaters on the grounds of their greater practicality.  The two Albert machines were the fastest present and were placed first and second before the economy coefficient was applied, after which they fell behind the two two-seat [[Avia BH-11]]s into third and fourth place.<ref name=Flight/>

There was another Orly light aircraft meeting in 1928, in which a different TE.1 participated. Again it was handicapped against two-seaters and was only at mid-table before the final reliability test, which it failed to complete.<ref name=Flight1/><ref name=Flight2/>

On 20 June 1927 a TE.1, flown by Albert, set a French altitude record in the lightplane class at {{convert|5535|m|ft|abbr=on}}.<ref name=Laero3/>

==Specifications==
[[File:Albert TE.1.jpg|thumb|At Orly, 1928]]
{{Aircraft specs
|ref=L'Aérophile (1926)<ref name=Laero1/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=one
|length m=5.50
|length note=
|span m=8.80
|span ft=
|height m=1.98
|height note=
|wing area sqm=10
|wing area note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=255
|empty weight note=
|gross weight kg=387
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity={{convert|44|kg|lb|abbr=on}}<ref name=NACA/>  
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Salmson 9 AD]]
|eng1 type=9-cylinder [[radial engine|radial]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=40
|eng1 note=at 2000 rpm
|power original=40 CV
|more power=

|prop blade number=2
|prop name=Duhamel
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=150
|max speed note= at ground level
|cruise speed kmh=105
|cruise speed note= economical<ref name=NACA/>
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=1000
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=5500
|ceiling note= theoretical
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=13 min 32 sec to {{convert|2000|m|ft|abbr=on}}
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=

|more performance=
*'''Take-off distance:''' {{convert|98|m|ft|abbr=on|-1}}<ref name=NACA/>
*'''Landing run:''' {{convert|88|m|ft|abbr=on|-1}}<ref name=NACA/>

}}

==References==
{{reflist|2|refs=


<ref name=Opdycke>{{cite book |title=French aeroplanes before the Great War |last=Opdycke |first=Leonard E. |page=251|edition= |year=1999|publisher=Shiffer Publishing Ltd|location=Atglen, PA, USA |isbn=0-7643-0752-5}}</ref>

<ref name=NACA>{{cite web |url=http://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/19930089227.pdf|title=NACA Aircraft Circulars no. 23: Albert TE-1 training aircraft |author=Dwight M. Miner |date=December 1926 |work=L'Aéronautique, April 1926 |publisher= NACA}}</ref>

<ref name=Laero1>{{cite journal|date=1–15 April 1926 |title=Une Avionette Remarquable|journal=L'Aérophile|volume=34 |issue=7-8 |pages=108-9|url=http://gallica.bnf.fr/ark:/12148/bpt6k65547488/f114}}</ref>

<ref name=Laero2>{{cite journal|date=1–15 July 1926 |title=Les deux voyages de lieutenant Thoret en avionette de 40 CV|journal=L'Aérophile|volume=34 |issue=13-14|pages=511-2|url=http://gallica.bnf.fr/ark:/12148/bpt6k65547488/f217}}</ref>

<ref name=Laero3>{{cite journal|date=1–15 August 1927 |title=Commission Sportive|journal=L'Aérophile|volume=35 |issue=15-16|pages=253|url= http://gallica.bnf.fr/ark:/12148/bpt6k6553815x/f259}}</ref>

<ref name=Laero4>{{cite journal|date=1–15 March 1929 |title=Autour de Monde Arien|journal=L'Aérophile|volume=37 |issue=5-6|pages=74|url= http://gallica.bnf.fr/ark:/12148/bpt6k6554360b/f80}}</ref>

<ref name=Laero5>{{cite journal|date=March 1928 |title=Les avions de sport et de tourisme |journal=L'Aéronautique|volume=106 |issue=|pages=64|url=http://gallica.bnf.fr/ark:/12148/bpt6k6568058r/f21}}</ref>

<ref name=Flight>{{cite journal|date=19 August 1926 |title=Concours d'Avions Économiques|journal= [[Flight International|Flight]]|volume=XVIII |issue=33 |pages=489-491, 506-516  |url= http://www.flightglobal.com/pdfarchive/view/1926/1926%20-%200576.html}}</ref>

<ref name=Flight1>{{cite journal|date=20 September 1928 |title=Orly 1928 light 'planes meeting|journal= [[Flight International|Flight]]|volume=XX |issue=38 |pages=789-95  |url= http://www.flightglobal.com/pdfarchive/view/1928/1928%20-%200853.html}}</ref>

<ref name=Flight2>{{cite journal|date=27 September 1928 |title=Orly 1928 light 'planes meeting|journal= [[Flight International|Flight]]|volume=XX |issue=39 |pages=833-4 |url= http://www.flightglobal.com/pdfarchive/view/1928/1928%20-%200897.html}}</ref>

}}
<!-- ==Further reading== -->
<!-- ==External links== -->

<!-- Navboxes go here -->
[[Category:Parasol-wing aircraft]]
[[Category:French sport aircraft 1920–1929]]
[[Category:Avions Albert aircraft|TE01]]
[[Category:Single-engined tractor aircraft]]