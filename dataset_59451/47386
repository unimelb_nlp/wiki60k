{{EngvarB|date=September 2014}}
{{Use dmy dates|date=March 2017}}

{{Infobox officeholder
| name          = Quazi Golam Dastgir
| office        = 
| image         = Ambassador Major General Quazi Golam Dastgir in 1991.jpg
| caption = Dastgir in Chittagong (1991)
| predecessor   =
| primeminister = 
| successor     =
| office2       = [[Permanent Representative]] to the [[Organisation of Islamic Cooperation|OIC]] and [[Islamic Development Bank|IDB]]
| predecessor2  = 
|president2 = [[Hussain Muhammad Ershad]]<br />[[Shahabuddin Ahmed]]<br />[[Abdur Rahman Biswas]]
| successor2    = 
| party         = 
| office1       = [[Bangladesh]] [[Ambassador]] to the Kingdom of [[Saudi Arabia]]
| predecessor1  = Mr. Hedayet Ahmad
|president1 = [[Hussain Muhammad Ershad]]<br />[[Shahabuddin Ahmed]]<br />[[Abdur Rahman Biswas]]
| successor1    = Mr. Abdul Momen Choudhury
| birth_date    = {{birth date|1932|9|23|df=yes}}
| birth_place   = [[Calcutta]], [[Bengal Presidency]], [[British India]]
| death_date    = {{death date and age|2008|10|17|1932|9|23|df=yes}}
| death_place   = [[Dhaka]], Bangladesh
| nationality   = [[Bangladesh]]i
| spouse        = 
| children      = 
| alma_mater    = [[Pakistan Military Academy]]<br />[[University of Peshawar]]
| profession    = [[Military officer]], Statesmen
| allegiance    = {{flag|Bangladesh}}<br />{{flag|Pakistan}} (before 1971)
| branch        = {{nowrap| [[File:Flag of the Bangladesh Army.svg|23px|Bangladesh Army seal]] [[Bangladesh Army]]}}<br />{{army|Pakistan}}
| serviceyears  = 
| rank          = [[File:08.maj gen Bd.jpg|20px]] [[Major General]]<br />[[File:Two star.jpg|40px]]
| unit =Infantry corps
|commands     ={{plainlist|
* Director General of [[Border Guards Bangladesh|BGB]]
* 65 Independent Infantry Brigade
* Commandant the East Bengal Regimental Centre
* 72 Independent Infantry Brigade
* Chief of Logistics, Army HQ
* [[Commanding Officer|CO]] of [[Senior Tigers|First East Bengal Regiment]]
}}
|battles = 
| awards        = 
| office3       = [[Bangladesh]] [[High Commissioner]] to [[Australia]]
| office4       = [[Bangladesh]] [[Ambassador]] to the [[Islamic Republic of Pakistan]]
| office5       = [[Bangladesh]] [[Ambassador]] to the [[Kingdom of  Thailand]]
| office6       = [[Permanent Representative]] to the [[United Nations]] [[Economic and Social Commission for Asia and the Pacific|ESCAP]] and [[Asian Development Bank|ADB]]
| imagesize     = 
| term_start1   = 2 February 1988
| term_end1     = 31 December 1991
| term_start2   = 1988
| term_end2     = 1991
| term_start3   = 1984
| term_end3     = 1988
|president3 = [[Hussain Muhammad Ershad]]
| term_start4   = 1982
| term_end4     = 1984
|president4 = [[Hussain Muhammad Ershad]]<br />[[A. F. M. Ahsanuddin Chowdhury|Ahsanuddin Chowdhury]]
| term_start5   = 27 May 1978
| term_end5     = 2 June 1982
|president5   = [[Ziaur Rahman]]<br />[[Abdus Sattar]]<br />[[Hussain Muhammad Ershad]]<br />[[A. F. M. Ahsanuddin Chowdhury|Ahsanuddin Chowdhury]]
| term_start6   = 1979
| term_end6     = 1982
| predecessor5  = A. K. M. Nazrul Islam
| successor5    = M. Mohsin
|president6   = [[Ziaur Rahman]]<br />[[Abdus Sattar]]<br />[[Hussain Muhammad Ershad]]
}}

'''Quazi Golam Dastgir''' (23 September 1932 – 17 October 2008) was a [[Bangladesh Army|Bangladesh army]] officer and diplomat. From 1975 to 1977, he served as the "Zonal Martial Law Administrator" (the equivalent of a State Governor in the military-backed government headed by President [[Abu Sadat Mohammad Sayem|Abu Sadat Mohammed Sayem]]) for [[Dhaka Division]]. He commanded two of the five independent brigades that comprised the Bangladesh Army up to the mid-1970s, and served as chief of the border forces, holding the office of Director General of [[Bangladesh Rifles]]. He was one of the three major generals in the Bangladesh Army following the promulgation of [[Bangladesh#History|martial law in 1975]]. In 1977, the service of Dastgir was transferred to the [[Ministry of Foreign Affairs (Bangladesh)|Ministry of Foreign Affairs]], and he went on to serve four terms as Ambassador of Bangladesh until his retirement in 1991.

==Early life (1932–1950)==

Dastgir was born on 23 September 1932 in [[Calcutta]], [[British India]]. He studied in [[Calcutta Model School]] and graduated from [[St. Xavier's College, Kolkata|St. Xavier's College]]. He subsequently graduated from [[Peshawar University]].

In 1950, he accompanied his family as immigrants to Dhaka, East Pakistan (later Bangladesh) and was selected for the 4th army pre-cadet training school in [[Quetta]]. He was then admitted to the 7th Batch [[Pakistan Military Academy]] (PMA) in [[Kakul]] and on 14 February 1953 was commissioned as a permanent regular officer with the [[Pakistan Army]] service number PA-4484 in the infantry corps of the Pakistan Army in the First Battalion (called the "[[Senior Tigers]]") of the [[East Bengal Regiment]] with the rank of [[Second lieutenant|second lieutenant]].

==Pakistan Army (1950–1971)==

As a lieutenant, he was a platoon commander and was decorated with a Commonwealth Medal in 1956. Later, as a captain, he held the post of General Staff Officer 3 (G-3), and was selected for infantry training in the United States. He graduated from the [[United States Army Infantry School]] at [[Fort Benning]] in [[Georgia (U.S. state)|Georgia]], US; and Special Warfare School at [[Fort Bragg]] in [[North Carolina]], US, and became a trained rappeller.

In the early 1960s, he was selected to command the army contingent guarding [[Queen Elizabeth II]] during her State Visit to Dhaka. He served as a judge in the diving competitions at the [[Commonwealth Games]] qualifying tournaments in Pakistan.

Promoted to the rank of major in 1965, he graduated from the [[Command and Staff College]] at Quetta, Pakistan. Decorated in the [[Indo-Pakistani War of 1965|1965 Indo-Pakistan War]], he was granted a six-month antedate seniority making his official commissioning date September 1952.  He served as the Brigade Major of an independent brigade from 1965–67 and as a company commander as well as the 2IC (second in command) of his battalion in 1968.

In 1969 he was promoted to the rank  oflieutenant colonel and was appointed commanding officer (CO) of the First East Bengal Regiment (his own battalion).  From 1970–71 he served in the most coveted position of his rank as the General Staff Officer Grade 1 (GSO-1 or G-1) of an independent army division (the 16th Division of the Pakistan Army) in Quetta, Pakistan which bordered Afghanistan's Kandahar province.

==Prisoner of war (1971–1973)==

He was posted in Quetta at the outbreak of the 1971 [[Indo-Pakistan War of 1971|Indo-Pakistan War]], and transferred to corps headquarter in [[Lahore]] initially and then to army headquarters in [[Rawalpindi]]. Following the independence of Bangladesh in 1971, he was put in concentration camps at Bunnoo and Mundi Bahauddin with fellow Bengali officers and families for two years.

==Bangladesh Army (1973–1977)==

Dastgir was repatriated back to Dhaka, Bangladesh after his release from Pakistani confinement, and was promoted to the rank of full colonel in 1973 after absorption in the Bangladesh Army with the Bangladesh Army service number BA-48. He was appointed Chief of Logistics&nbsp;– the combined positions of quartermaster general (QMG) and master general of ordnance (MGO), each post headed by a lieutenant general currently&nbsp;– at the new Army Headquarters in Dhaka, the capital of Bangladesh. In rapid succession during 1974 he commanded two independent brigades&nbsp;– the 72 Infantry Brigade (later upgraded to the 66th Infantry Division) stationed at Rangpur and the 65 Infantry Brigade (later upgraded to the 24th Infantry Division) stationed at Chittagong&nbsp;– and was promoted to the rank of brigadier in 1975. During this time he led Operation Dragon Drive,<ref>[https://books.google.com/books?id=FD2KzBG1ejwC&pg=PA131 Understanding Bangladesh – S. Mahmud Ali – Google Books<!-- Bot generated title -->]</ref> the first successful joint army-navy-air force military operation in Bangladesh, earning him the Bangladesh Army's highest operational medal the Jatiyo Nirapatya Padak. While commanding the Chittagong Area, he also served as the ex officio Commandant of the [[East Bengal Regimental Centre]] (EBRC)&nbsp;– a position nicknamed "Papa Tiger".<ref>{{cite web |url=http://www.bdsdf.org/forum/index.php?showtopic=3730 |title=History of East Bengal Regiment |website=Bangladesh Strategic & Development Forum |archive-url=http://archive.is/2012.08.02-040642/http://www.bdsdf.org/forum/index.php?showtopic=3730 |archive-date=2 August 2012}}</ref>

In August 1975 Dastgir was promoted to the rank of major general, the highest rank possible in the Bangladesh Army at that time, and continued to command 65 Infantry Brigade (making him Bangladesh Army's first formation commander to hold the rank of Major General) until November 1975 when he was appointed chief of the border forces as Director General of the [[Bangladesh Rifles]] (BDR). In November 1975, Dastgir was given the additional responsibility to serve as the first Zonal Martial Law Administrator (ZMLA) (i.e., the de facto military governor) for Dhaka Division, one of the four administrative provinces of the country. In 1977 he founded the [[Rifles Public School and College|Rifles Public School]] at BDR Headquarters and this is now one of the leading academic institutions in the Bangladesh capital Dhaka. ZMLA Dastgir headed a review of the tea industry and the Dastgir Tea Commission (as his review came to be known) forgave outstanding bank loans taken by Bangladesh tea gardens before independence in 1971&nbsp;– thereby reviving the tea industry in the country.

During this time he led the Bangladesh delegation for border talks with India held in Calcutta in 1975 and New Delhi in 1976.<ref>[http://mealib.nic.in/far/1976.pdf MEA | MEA Links : Indian Missions Abroad<!-- Bot generated title -->]</ref>  The border talks had followed Dastgir’s successful counter-offensive against Indian incursions in Bangladesh’s Mymensingh border with India&nbsp;– and this military operation got him his second Jatiyo Nirapatya Padak.

==Bangladesh Foreign Service (1977–1991)==

Despite ample opportunities, Dastgir refused to do politics in uniform and requested to be relieved of his role as ZMLA of Dhaka Division to enable him to focus on his military duties just as the Chief of Army Staff Major General [[Ziaur Rahman]] took over as President of Bangladesh. In December 1977, he was transferred to the Ministry of Foreign Affairs as a full, permanent Secretary to the Government of Bangladesh. Dastgir was appointed the Ambassador of Bangladesh to Thailand (and later concurrently accredited to the Philippines) in May 1978 and served till June 1982.<ref>{{cite web |url=http://www.bdembassybangkok.org/index.php/list-of-ambassadors |title=List of Ambassadors |archive-url=https://web.archive.org/web/20120131025724/http://www.bdembassybangkok.org/index.php/list-of-ambassadors |archive-date=31 January 2012}}</ref> During this time he also served as his country's Permanent Representative to the [[United Nations Economic and Social Commission for Asia and the Pacific]] (ESCAP) and the [[Asian Development Bank]] (ADB). As the country's Permanent Representative to ESCAP, Dastgir played a key role in the election of Bangladesh Foreign Secretary [[Shah A M S Kibria]] as the Executive Secretary of the United Nations commission. It was at a reception at his house in Bangkok, Thailand that he broached the idea of an association of South Asian nations in the [[ASEAN]] model to the Bhutanese Foreign Minister, and this started discussions which led to the proclamation of [[SAARC]] (the South Asian Association for Regional Cooperation) by Bangladesh President [[Ziaur Rahman]], after extensive diplomatic groundwork done by Dastgir's brother-in-law then Bangladesh Foreign Secretary Ambassador [[Humayun Rashid Choudhury|Humayun Rasheed Choudhury]]<ref name=autogenerated2 /><ref>[http://www.un.org/ga/55/president/bio41.htm HUMAYUN RASHEED CHOUDHURY – 41st Session<!-- Bot generated title -->]</ref> who went on to become the President of the 41st [[UN General Assembly]].  General Dastgir's term as Ambassador to Thailand coincided with renowned US Ambassador [[Morton Abramowitz]], and during this time General Dastgir often took advice on foreign service approaches from another soldier turned diplomat, Philippine Foreign Minister [[Carlos Romulo]].

From 1982 to 1983 he served as Ambassador of Bangladesh to Pakistan. On 26 March 1983, Ambassador Dastgir gave a televised speech aired on Pakistan Television (PTV), addressing multifarious bilateral issues between Bangladesh and Pakistan. This helped normalise relations with Pakistan and eventually led to the re-admittance of Pakistan in the [[British Commonwealth]] at the recommendation of Bangladesh.

From 1984 to 1988 the general served as the High Commissioner of Bangladesh to Australia<ref>[https://news.google.com/newspapers?nid=1301&dat=19840418&id=QDdWAAAAIBAJ&sjid=YegDAAAAIBAJ&pg=5530,849049 The Sydney Morning Herald – Google News Archive Search<!-- Bot generated title -->]</ref><ref>{{cite web|url=http://www.parliament.nsw.gov.au/Prod/parlment/hanstrans.nsf/V3ByKey/LA19840823/$file/482LA006.pdf |title=Archived copy |accessdate=31 January 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20160304080926/http://www.parliament.nsw.gov.au/Prod/parlment/hanstrans.nsf/V3ByKey/LA19840823/$file/482LA006.pdf |archivedate=4 March 2016 }}</ref><ref>http://www.slwa.wa.gov.au/pdf/ephemera/pr11148.pdf</ref> with concurrent accreditation to New Zealand, Papua New Guinea<ref>http://www.dtic.mil/cgi-bin/GetTRDoc?AD=ADA341261</ref> and Fiji. In 1984 he attended the [[Commonwealth Heads of Government Regional Meeting]] in Papua New Guinea, and served as the leader of the Bangladesh delegation to the Tenth Asian and Pacific Labour Ministers Conference held in Melbourne in October 1985. Later that year (1985), he led the Bangladesh delegation to the 44th [[International Cotton Advisory Committee]] meeting held in Sydney from 28 October to 1 November.

In 1988 he was appointed Ambassador of Bangladesh to Saudi Arabia<ref>{{cite web |url=http://www.bangladeshembassy.org.sa/emb/history.htm |title=The Embassy |website=Embassy of Bangladesh, Riyadh |archive-url=https://web.archive.org/web/20111110165042/http://www.bangladeshembassy.org.sa/emb/history.htm |archive-date=10 November 2011}}</ref><ref>http://www.dtic.mil/cgi-bin/GetTRDoc?Location=U2&doc=GetTRDoc.pdf&AD=ADA343489</ref> with concurrent accreditation to Jordan, Niger and Yemen. During this time, General Dastgir also served as the Permanent Representative of Bangladesh to the [[Organization of Islamic Conference]] (OIC), the [[Islamic Development Bank]] (IDB), and the [[Saudi Fund for Development]]. As the country's Permanent Representative to the OIC, Dastgir played a key role in the election of Bangladeshi candidate Ambassador Mohammad Mohsin as the Deputy Secretary General of the organisation.<ref>http://www.dtic.mil/cgi-bin/GetTRDoc?AD=ADA336117</ref> During his tenure to Saudi Arabia, Dastgir was instrumental in the decision of the Government of Bangladesh to send a contingent of troops, consisting of the [[Senior Tigers|First East Bengal Regiment]] (his own battalion) as part of [[Operation Desert Storm]]. This was Bangladesh's first participation in an international military coalition, and paved the way for the Bangladesh Armed Forces to take part in future United Nations peacekeeping operations across various world theatres. At this time Dastgir gave an interview to the ''[[Voice of America]]'' and spoke about regional security, including the steps taken by the Saudi Government to ensure the safety of Bangladesh nationals affected by the first [[Gulf War]].<ref name=autogenerated1>[http://www.highbeam.com/doc/1P3-1659991761.html A tribute to General Dastgir – Dhaka Courier | HighBeam Research<!-- Bot generated title -->]</ref> For his distinguished services as the Ambassador of Bangladesh to the Kingdom of Saudi Arabia, Dastgir was decorated in 1991 with the [[Order of Abdulaziz al Saud|King Abdul Aziz Order Class 1]] by (then) [[King Fahd]] of Saudi Arabia.

In September 1991, Dastgir retired from the Government of Bangladesh after completing a career spanning 41 years.

==Retirement (1991–2008)==

Dastgir remained active in military and diplomatic circles after retirement, and spoke at many public forums.

In 1996 he was widely rumoured to be under consideration to take part in the first neutral Caretaker Government which officiated during national elections. Instead, he was nominated by the [[Jatiya Party (Ershad)|Jatiyo Party]] to contest the parliamentary elections from a Dhaka constituency. During this time he was exclusively interviewed by [[CNN]]'s South Asia Anchor [[Anita Pratap]] for a worldwide broadcast in 1996. Despite the election victory of an opponent, Dastgir gained wide popularity in the constituency for his bold yet statesmanlike campaign. He was appointed a Presidium Member of the Jatiyo Party and served as the chief foreign policy advisor to former President [[H M Ershad|H. M. Ershad]]. In 2002 Dastgir resigned his membership in the Jatiyo Party due to policy differences with the party leadership.

In 1997, he was elected chairman of the Retired Armed Forces Officers Welfare Association (RAOWA)&nbsp;– Bangladesh's only veteran's association for army, navy and air force officers—and served a two-year term. Elected to the Bangladesh chapter of the [[Royal Commonwealth Society]] in 1998, Dastgir served as its Presidium Member and Vice-President until 2005.<ref>[http://www.thedailystar.net/2004/05/14/d40514101377.htm The Daily Star Web Edition Vol. 4 Num 339<!-- Bot generated title -->]</ref><ref>[http://www.thedailystar.net/2005/11/16/d511161502109.htm The Daily Star Web Edition Vol. 5 Num 523<!-- Bot generated title -->]</ref>

He had been handpicked by General [[M. A. G. Osmani]] to serve as a member of the Osmany Trust created to look after the estate of the former Commander-in-Chief of the Bangladesh Liberation forces. Dastgir remained an active member of the trust until 2008.

His health began to fail abruptly in 2005 after he attended the 100th Reunion of the [[Command and Staff College]] in Quetta following a personal invitation by Pakistan President [[Pervez Musharraf]].

==Personal life==
In 1965 Dastgir married Kohinoor Rasheed Choudhury, the daughter of industrialist-cum-legislator and Member of British India's [[Central Legislative Assembly]] [[Abdur Rasheed Choudhury]]<ref name=autogenerated2>http://www.salaam.co.uk/knowledge/impact/Vol31No8p48.pdf</ref><ref name=autogenerated3>{{cite news |last=Al-Mahmood |first=Syed Zain |date=25 June 2010 |title=A Legacy of Love |url=http://archive.thedailystar.net/magazine/2010/06/04/tribute.htm |work=Star Weekend Magazine |publisher=The Daily Star}}</ref><ref>{{cite news |last=Siddiquee |first=Iqbal |date=29 September 2007 |title=Aminur Rashid Chowdhury: Culture and Politics |url=http://archive.thedailystar.net/starinsight/2007/09/03/behind.htm |work=Star Insight |publisher=The Daily Star}}</ref> and Member of the [[Pakistan National Assembly]] Serajunnessa Choudhury<ref name=autogenerated2 /><ref name=autogenerated3 /> of Sylhet, East Pakistan (currently Bangladesh). Mrs.  Dastgir is a social worker and recipient of the [[International Red Cross]] Gold Medal. Their son Quazi Rumman Dastgir is a U.S. diplomat and their daughter Ayesha Dastgir is an international civil servant.

==Death and legacy==
In September 2008, Dastgir fell seriously ill with undiagnosed ailments and had to be hospitalised for about a month. He returned home on 10 October 2008. He died in his sleep at his Dhaka residence on 17 October 2008.<ref>[http://www.thedailystar.net/newDesign/news-details.php?nid=206818 Quazi Golam Dastgir<!-- Bot generated title -->]</ref> After funeral services attended by top defence and civilian officials, he was buried in the national army cemetery in Dhaka (the Cantonment Graveyard).

Dastgir was one of the best known Bengali army officers of his time&nbsp;– and was apparently considered by the [[CIA]] to be the most likely person to lead the Bangladesh Army in case of war. Following his active military service, he had a distinguished career as a diplomat during his four terms as Ambassador. Described as an "upright officer of the old school",<ref name=autogenerated1 /> he held conservative views with a strong belief in civilian governance which might have been a product of his US training, and these convictions may have been a deterrent to his rise to the pinnacle of political power.

==References==
{{Reflist|30em}}

{{DEFAULTSORT:Dastgir, Quazi Golam}}
[[Category:Bangladesh Army officers]]
[[Category:Ambassadors of Bangladesh to Thailand]]
[[Category:Ambassadors of Bangladesh to Saudi Arabia]]
[[Category:Ambassadors of Bangladesh to Jordan]]
[[Category:Ambassadors of Bangladesh to the Philippines]]
[[Category:Ambassadors of Bangladesh to Niger]]
[[Category:Ambassadors of Bangladesh to Yemen]]
[[Category:High Commissioners of Bangladesh to Australia]]
[[Category:High Commissioners of Bangladesh to New Zealand]]
[[Category:High Commissioners of Bangladesh to Fiji]]
[[Category:High Commissioners of Bangladesh to Papua New Guinea]]
[[Category:High Commissioners of Bangladesh to Pakistan]]
[[Category:2008 deaths]]
[[Category:1932 births]]
[[Category:Bangladeshi military personnel]]
[[Category:Bangladesh Army generals]]
[[Category:Bangladeshi generals]]