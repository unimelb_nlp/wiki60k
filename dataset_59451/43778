{{Infobox company
| name             = TL-Ultralight
| logo             = [[File:TL Ultralight Logo 2014.png|250px]]
| caption          = 
| type             = [[Privately held company]]
| traded_as        = 
| genre            = <!-- Only used with media and publishing companies -->
| fate             = 
| predecessor      = 
| successor        = 
| foundation       = 1989
| founder          = 
| defunct          = <!-- {{End date|YYYY|MM|DD}} -->
| location_city    = [[Hradec Králové]]
| location_country = [[Czech Republic]]
| location         = 
| locations        = 
| area_served      = 
| key_people       = 
| industry         = [[Aerospace]]
| products         = [[Kit aircraft]]<br  />[[Ultralight aircraft]]
| services         = 
| revenue          = 
| operating_income = 
| net_income       = 
| assets           = 
| equity           = 
| owner            = Jiri Tlusty
| num_employees    = 
| parent           = 
| divisions        = 
| subsid           = 
| homepage         = [http://www.tl-ultralight.cz/en/ www.tl-ultralight.cz]
| footnotes        = 
| intl             = 
}}

'''TL–Ultralight''' is an aircraft manufacturer based in [[Hradec Králové]], [[Czech Republic]]. The company started out as a builder of [[ultralight trike]]s and now specializes in the design and manufacture of composite [[ultralight aircraft]].<ref name="Aerocrafter">Purdy, Don: ''AeroCrafter - Homebuilt Aircraft Sourcebook, Fifth Edition'', page 266. BAI Communications, 15 July 1998. ISBN 0-9636409-4-1</ref>

The company is owned by Jiri Tlusty and was founded in 1989.<ref>{{cite web|url=http://www.tl-ultralight.net/en/contact/index.php |title=History|author= TL-Ultralight Aircraft |publisher=Tl-ultralight.net |date=|accessdate=16 May 2016 |archiveurl = https://web.archive.org/web/20140319030100/http://www.tl-ultralight.net/en/history/index.php |archivedate = 19 March 2014}}</ref>

One of the company's earliest aircraft designs was the now out of production [[TL-22 Duo]], a conventional ultralight trike model.<ref name="Aerocrafter" />

In 2014 the company's product line included the [[TL-3000 Sirius]] and three variants of the [[TL-2000 Sting]]: the S3, S4 and RG models.<ref>{{cite web|url=http://www.tl-ultralight.net/en/index.php |title=TL-Ultralight Aircraft |publisher=Tl-ultralight.net |date=2011-12-23 |accessdate=2014-03-18}}</ref>

== Aircraft ==
[[File:OK-OUR-15 TL-32 Day.JPG|thumb|right|[[TL Ultralight TL-32 Typhoon]]]]
[[File:TL Ultralight TL-96 Star AN1812449.jpg|thumb|right|[[TL Ultralight TL-96 Star]]]]
{| class="wikitable"  style="margin:auto; font-size:90%;"
|-
|+  style="text-align:center; background:#bfd7ff;"| '''Summary of aircraft built by TL Ultralight'''
|- style="background:#efefef;"
! Model name
! First flight
! Number built
! Type

|-
|align=left| '''[[TL Ultralight TL-22 Duo|TL-22 Duo]]'''
|align=center| 1990s
|align=center| 50 (1998)
|align=left| [[Ultralight trike]]
|-
|align=left| '''[[TL Ultralight TL-32 Typhoon|TL-32 Typhoon]]'''
|align=center| 1991
|align=center| >200
|align=left| [[Aircraft fabric covering|Fabric covered]] [[high-wing]] [[ultralight aircraft]]
|-
|align=left| '''[[TL Ultralight TL-96 Star|TL-96 Star]]'''
|align=center| 1997
|align=center| 
|align=left| Composite [[low-wing]] ultralight aircraft
|-
|align=left| '''[[TL Ultralight Condor|TL-132 Condor]]'''
|align=center| 1993
|align=center| 115 (2010)
|align=left| Fabric covered high-wing ultralight aircraft
|-
|align=left| '''[[TL Ultralight Condor|TL-232 Condor Plus]]'''
|align=center| 
|align=center| 
|align=left| Fabric covered high-wing ultralight aircraft
|-
|align=left| '''[[TL Ultralight TL-96 Star|TL-2000 Sting]]'''
|align=center| 2002
|align=center| 
|align=left| Composite low-wing ultralight aircraft
|-
|align=left| '''[[TL Ultralight TL-3000 Sirius|TL-3000 Sirius]]'''
|align=center| 2008
|align=center| 22 (2010)
|align=left| Composite high-wing ultralight aircraft
|-
|'''[[TL-Ultralight Stream|Stream]]'''
|align=center| 2015
|align=center| 
|align=left| Tandem-seat composite low-wing ultralight aircraft
|-
|}

==References==
{{Reflist}}

==External links==
*{{Official website|http://www.tl-ultralight.cz}}
*{{Commons-inline}}
{{TL Ultralight aircraft}}

{{DEFAULTSORT:TL-Ultralight}}
[[Category:Aircraft manufacturers of the Czech Republic and Czechoslovakia]]
[[Category:Ultralight trikes]]
[[Category:Homebuilt aircraft]]