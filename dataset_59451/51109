{{Infobox journal
| title = Diabetologia
| editor = Sally Marshall
| discipline = [[Diabetology]]
| abbreviation = Diabetologia
| publisher = [[Springer Science+Business Media]]
| frequency = Monthly
| history = 1965-present
| impact = 6.206
| impact-year = 2015
| website = http://www.diabetologia-journal.org
| link1 = http://www.springerlink.com/content/0012-186X
| link1-name = Online access
| link2 = http://www.springer.com/medicine/internal/journal/125
| link2-name = journal page at publisher's website
| ISSN = 0012-186X
| CODEN = DBTGAJ
| LCCN = 65009991
| OCLC = 01566567
}}
'''''Diabetologia''''' is a monthly [[peer-reviewed]] [[medical journal]] covering [[diabetology]] and is the official journal of the [[European Association for the Study of Diabetes]]. It is published by [[Springer Science+Business Media]] and the [[editor-in-chief]] is Sally Marshall ([[Newcastle University]]). According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 6.206, ranking it 12th out of 133 journals in the category "Endocrinology & Metabolism".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Endocrinology & Metabolism |title=2015 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== Previous editors ==
The following persons have been editors-in-chief of the journal:<ref>http://www.diabetologia-journal.org/files/previous_editors.pdf</ref>
{{columns-list|colwidth=30em|
* Juleen Zierath 2010-2015
* Edwin Gale 2004-2010
* Werner Waldhäusl 1998-2003
* Ele Ferrannini 1993-1997
* C. Hellerström 1989-1992
* M. Berger 1983-1988
* A. G. Cudworth 1981-1982
* K. G. M. M. Alberti 1977-1980
* W. Creutzfeldt 1973-1976
* K. Schöffling 1973-1976
* K. Oberdisse 1965-1972
}}

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.diabetologia-journal.org}}

[[Category:Springer Science+Business Media academic journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Endocrinology journals]]
[[Category:Publications established in 1965]]