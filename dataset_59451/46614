{{Use dmy dates|date=April 2015}}
{{Use Australian English|date=April 2015}}
{{Infobox musical artist
| name            = Bombs Away
| image           = Bombs Away 2013.jpg
| landscape       = 
| alt             = 
| caption         = Bombs Away at the [[ARIA Music Awards|ARIA Awards]] in 2013
| background      = group_or_band
| alias           = 
| origin          = Australia
| genre           = [[Electro house]], [[Jumpstyle|Melbourne bounce]], [[Trap (music)|trap]], [[Progressive House]], [[Hip house]]
| years_active    = {{start date|2010}}–{{end date|present}}
| label           = Central Station, Ministry of Sound Australia, Bomb Squad Records, Cloud 9 Records, Kontor Records, Radikal Records
| website         = {{URL|www.letmeseeyourswagger.com}}
| current_members = Sketch (Matthew Coleman)<br /> Tommy Shades (Thomas Coleman)
}}

'''Bombs Away''' is an Australian brothers DJ, vocal and production duo, composed of Sketch (born Matthew Ronald John Coleman, October 30th) and Tommy Shades (born Thomas Edward Hart Coleman, May 29th). Born on the [[Gold Coast, Queensland|Gold Coast]], [[Australia]], before relocating to [[Perth]] early in their adult lives, where Bombs Away was formed in late 2009.

In 2010 the duo began to tour nationally with their first single "Big Booty Bitches" and various remixes. They later toured internationally before relocating to the Gold Coast in late 2011, where they produced two further singles, "Super Soaker" and "Party Bass", which were both certified Platinum by [[Australian Recording Industry Association|ARIA]], with "Big Booty Bitches" also certified Gold.<ref name="aria_b">{{cite web|url=http://www.aria.com.au/pages/httpwww.aria.com.aupagesaria-charts-accreditations-singles-2013.htm |title=ARIA Charts - Accreditations - 2013 Singles |publisher=Australian Recording Industry Association |date=2013-12-31 |accessdate=2015-02-26 |deadurl=yes |archiveurl=http://www.webcitation.org/6JdeckIt9?url=http%3A%2F%2Fwww.aria.com.au%2Fpages%2Fhttpwww.aria.com.aupagesaria-charts-accreditations-singles-2013.htm |archivedate=15 September 2013 |df=dmy }}</ref><ref name="aria_a">{{cite web|url=http://www.aria.com.au/pages/httpwww.aria.com.aupagesSINGLEaccreds2012.htm |title=ARIA Charts - Accreditations - 2012 Singles |publisher=Australian Recording Industry Association |date=2012-12-31 |accessdate=2015-05-10 |deadurl=yes |archiveurl=http://www.webcitation.org/6JdeigGMv?url=http%3A%2F%2Fwww.aria.com.au%2Fpages%2Fhttpwww.aria.com.aupagesSINGLEaccreds2012.htm |archivedate=15 September 2013 |df=dmy }}</ref>

In May 2013 Bombs Away included a tour DJ in all performances and since the addition of a tour DJ, DJ Kronic (born Luke Calleja), Dixie (Dan Spec) and KOMES (born Ryan Higginbottom) have all performed the role at various stages, with Dixie being most recent, until Dan Absent took up the role of tour DJ with Bombs Away.

Bombs Away also has a live performance configuration<ref>{{cite web|author= |url=http://www.radikal.com/2012/04/24/bombs-away-adds-a-new-drummer-to-their-live-shows |title=Bombs Away Adds A New Drummer To Their Live Shows &#124; Radikal Records |publisher=Radikal.com |date= |accessdate=2013-08-05}}</ref> in which Tommy Shades performs lead vocals, Sketch plays guitar as well as vocals and they are joined on stage by their drummer, Matt McGuire.

==Music==
Bombs Away's first single release "Big Booty Bitches" reached number 3 on the Beatport Electro Top 100.<ref name="promote-records">{{cite web|url=http://www.promote-records.com/recordtracker/pub/view/24/label-cat-no-bombs-away-big-booty-bitches-20370 |title=bombs away - big booty bitches - CAT.NO. - Record Tracker |publisher=Promote-records.com |date=2011-10-28 |accessdate=2013-08-09}}</ref> It also received in excess of 2 million views on YouTube,<ref name="youtube">{{cite web|url=https://www.youtube.com/watch?v=bBlv4pyytSw |title=Big Booty Bitches - Bombs Away (Official Uncensored) |publisher=YouTube |date= |accessdate=2013-08-05}}</ref> and was [[List of music recording certifications|certified Gold]] in 2013.<ref name="aria_b" /> Their follow-up single "Swagger" was released in early 2011 to coincide with the release of Central Station Records'<ref>{{cite web|url=http://www.centralstation.com.au/ |title=Urban & Dance Music Label |publisher=CentralStation.com.au |date= |accessdate=2013-08-05}}</ref> compilation ''Wild Nights 2011''.<ref name="centralstation">{{cite web|url=http://www.centralstation.com.au/releases/releaseinfo.asp?id=3993#_ |title=WILD NIGHTS 2011 - Mixed by BOMBS AWAY & PUNK NINJA Release Information |publisher=Centralstation.com.au |date=2011-04-19 |accessdate=2013-08-05}}</ref> The track received national airplay on the [[Nova (radio network)|Nova Network]]<ref>{{cite web|url=http://www.novafm.com.au/station/nova969 |title=Sydney 96.9 |publisher=NovaFM |date= |accessdate=2013-08-05}}</ref> and [[Austereo Radio Network]].<ref>{{cite web|url=http://www.southerncrossaustereo.com.au/ |title=Home |publisher=Southern Cross Austereo |date=2013-07-29 |accessdate=2013-08-05}}</ref>

Bombs Away's third single "Super Soaker" was one of the [[ARIA Charts]] Top 50 Dance Singles of 2012<ref name="aria">{{cite web|url=http://www.aria.com.au/pages/aria-charts-end-of-year-charts-dance-singles-2012.htm |title=ARIA Top 50 Dance Singles 2012 |publisher=Australian Recording Industry Association |accessdate=2013-08-05}}</ref> and top 50 overall Australian singles in 2011.<ref>{{cite web|url=http://www.aria.com.au/pages/aria-charts-end-of-year-charts-top-australian-singles-2011.htm |title=ARIA Top 50 Australian Artist Singles 2011 |publisher=ARIA |date= |accessdate=2013-08-05}}</ref> It was completed and added to all of their performances from March 2011, but the final version was not released until September 2011. Upon its release, it became the highest-ranked dance song from an Australian artist on the ARIA Dance Chart in only its second week of release. The track was certified Gold in March 2012, and Platinum in late 2012.<ref name="aria_a" /><ref>{{cite web|url=http://www.soapboxartists.com.au/post/13/04/bombs-away-have-hit-platinum-party-bus |title=DJ and Artist Bookings & Management |publisher=Soapbox Artists |date=2013-04-11 |accessdate=2015-02-26}}</ref> In March 2012, they released a collaboration with DJ Kronic, titled "Looking for Some Girls", which reached number 7 on the ARIA Club Chart.

Bombs Away's fifth single, "Get Stoopid", a collaboration with [[Seany B]] (the performer of the TV Rock single "[[Flaunt It (song)|Flaunt It]]"), was released in late 2012, and reached number 1 on the ARIA Club Chart<ref name="ariacharts">{{cite web|url=http://www.ariacharts.com.au/chart/club-tracks/103 |title=ARIA Australian Top 50 Club Tracks &#124; Australia's Official Top 50 Club Chart |publisher=ARIA Charts |accessdate=2013-08-05}}</ref> and was ranked number 27 in the Top 50 Club Tracks of 2012 on the ARIA Charts.<ref name="aria_d">{{cite web|url=http://www.aria.com.au/pages/aria-charts-end-of-year-charts-club-chart-2012.htm |title=ARIA Top 50 Club Tracks 2012 |publisher=ARIA |accessdate=2013-08-05}}</ref> The majority of the film clip for "Get Stoopid" was filmed on the streets of Surfers Paradise, and featured cameo appearances from "[[The Twins (band)|The Twins]]" (Ellie Kelaart and Brooke Kelaart) and [[DJ Kronic]].

The sixth single from Bombs Away, "Party Bass", released in 2012, was certified Gold within five weeks of its release, and Platinum two weeks after.<ref name="aria_a" /> It also reached number 1 on the Independent Music Chart<ref name="vmusic">{{cite web|url=http://www.vmusic.com.au/pages/main-menu/news/independent-music-news/indie-music-news |title=VMusic - Music News - Latest local and international music news |publisher=[V] Music |date= |accessdate=2013-08-05}}</ref> and the top 3 of the iTunes Dance Chart.<ref name="bare_url">{{cite web|url=http://www.music-chart.info/Australia/archive/genre/17/Dance/2012/11/10#3 |title=Australia Dance Songs Chart Archive - 10 November, 2012 &#124; ITunes Music Chart Archive |publisher=Music-chart.info |date=2012-11-10 |accessdate=2013-08-05}}</ref> It was also the fourth-highest selling Australian dance single of 2012 (26th overall)<ref name="aria"/> and 27th overall Australian single in 2012 on the ARIA Charts.<ref name="aria_c">{{cite web|url=http://www.aria.com.au/pages/aria-charts-end-of-year-charts-top-australian-singles-2012.htm |title=ARIA Top 50 Australian Artist Singles 2012 |publisher=ARIA |date= |accessdate=2013-08-05 |deadurl=yes |archiveurl=http://www.webcitation.org/6DX5tt6Yc?url=http%3A%2F%2Fwww.aria.com.au%2Fpages%2Faria-charts-end-of-year-charts-top-australian-singles-2012.htm |archivedate=9 January 2013 |df=dmy }}</ref>

"Drunk Arcade" was released in March 2013 and debuted at number 15 on the ARIA Dance Singles Chart.<ref name="ariacharts_a">{{cite web|url=http://www.ariacharts.com.au/news/39689/chartifacts---tuesday,-7th-may-2013 |title=Chartifacts - Tuesday, 7th May 2013 - ARIA Music News |publisher=ARIA |date=2013-05-07 |accessdate=2013-08-05}}</ref> The duo's original release "Assassinate", released in July 2013, reached number 4 on the Beatport Electro Release chart on 1 August.<ref name="beatport">{{cite web|url=http://www.beatport.com/genre/electro-house/17/top-100-releases |title=Electro House :: Top 100 Releases |publisher=Beatport |date=2013-02-19 |accessdate=2013-08-05}}</ref>

==Major performances==
[[File:Bombs Away performing live.jpg|thumb|Bombs Away performing at Life In Colour Festival, Melbourne Australia 2013]]
*[[Stereosonic]] Festival 2010<ref>{{cite web|url=http://www.novanation.com.au/photo_stereosonic-2010-at-sydney-showgrounds_159010 |title=Stereosonic 2010 at Sydney Showgrounds - Novanation Photo Gallery |publisher=Novanation.com.au |date= |accessdate=2013-08-05}}</ref>
*Stereosonic Festival 2011<ref>{{cite web|url=http://www.soapboxartists.com.au/post/12/10/steresonic |title=DJ and Artist Bookings & Management |publisher=Soapbox Artists |date=2012-10-19 |accessdate=2015-02-26}}</ref>
*Stereosonic Festival 2012<ref>{{cite web|url=http://everguide.com.au/music/gigs-and-festivals/news/stereosonic-lineup-2012.aspx |title=Stereosonic Lineup 2012 |publisher=Everguide |date=2012-07-05 |accessdate=2013-08-05}}</ref>
*[[Future Music Festival]] 2011<ref>{{cite web|url=http://legacy.novafm.com.au/photosimple_260821 |title=Future Music Festival 2011 timetable - Perth |publisher=Legacy.novafm.com.au |date= |accessdate=2013-08-05}}</ref>
*Future Music Festival 2012<ref>{{cite web|url=http://www.soapboxartists.com.au/post/12/10/bombs-away |title=DJ and Artist Bookings & Management |publisher=Soapbox Artists |date=2012-10-10 |accessdate=2015-02-26}}</ref>
*[[Creamfields Australia|Creamfields Festival]] 2012<ref>{{cite web|url=http://everguide.com.au/sydney/event/2012-apr-29/creamfields |title=Page Unavailable |publisher=Everguide |date= |accessdate=2013-08-05}}</ref>
*Fat As Butter Festival 2011<ref>{{cite web|url=http://www.theloop.com.au/ReckoningEntertainment/project/40928 |title=Fat As Butter Festival 2011 - Reckoning Entertainment Portfolio |publisher=The Loop |date= |accessdate=2013-08-05}}</ref>
*Fat As Butter Festival 2012<ref>{{cite web|url=http://www.fatasbutter.com.au/lineup.aspx |title=Line Up - Fat As Butter 2013 |publisher=Fatasbutter.com.au |date= |accessdate=2013-08-05}}</ref>
*Fat As Butter Festival 2013<ref>{{cite web|url=http://www.fatasbutter.com.au/lineup/bombsaway.aspx |title=Bombs Away - Fat As Butter 2013 |publisher=Fatasbutter.com.au |date= |accessdate=2013-08-05}}</ref>
*Foreshore festival 2011 & 12<ref>{{cite web|url=http://www.foreshorefestival.com.au/foreshore-2012/artists/6522 |title=Bombs Away Foreshore 2012 |publisher=Foreshorefestival.com.au |date= |accessdate=2013-08-05}}</ref>
Life in Color Festival 2012
Life in Color Festival 2013
Bombs Away performed mainstage on the national Stereosonic<ref>{{cite web|url=http://stereosonic.com.au/ |title=Stereosonic 2013 |publisher=Stereosonic.com.au |date= |accessdate=2013-08-05}}</ref> tour along with hosting "The Ultimate Stereosonic Afterparty" for the Brisbane leg of the tour, which was held at Brisbane’s Electric Playground Nightclub. The afterparty saw guest performances by international artists Afrojack (reference) and Redfoo from LMFAO (Reference), as well as promoted performances by Bombs Away, Dainjazone, and DJ Kronic

On New Year's Day 2012, Bombs Away headlined the Ministry of Sound<ref name="ministryofsound">{{cite web|url=http://www.ministryofsound.com.au/ |title=Ministry of Sound Australia |publisher=Ministryofsound.com.au |date= |accessdate=2013-08-05}}</ref> NYD Party held at The Eatons Hill Hotel in Brisbane alongside [[Havana Brown]], [[Tommy Trash]], the [[Stafford Brothers]], [[Timmy Trumpet]] and a number of other national and international touring DJ acts.

Bombs Away performed on the 2012 [[Creamfields Australia]] tour, with sets on the mainstage for each leg of the festival's Australian tour.<ref>{{cite web|url=http://www.beat.com.au/festivals/creamfields-2012-lineup |title=Creamfields 2012 Lineup &#124; Beat Magazine |publisher=Beat.com.au |date=2010-09-13 |accessdate=2013-08-05}}</ref>

==Management==
Bombs Away are represented for touring in Australia by Soapbox Artists,<ref>{{cite web|url=http://www.soapboxartists.com.au/profile/bombs-away/ |title=DJ and Artist Bookings & Management |publisher=Soapbox Artists |date= |accessdate=2015-02-26}}</ref> a subsidiary of [[Ministry of Sound Australia]].<ref name="ministryofsound" /> Soapbox Artists also handle all incoming enquiries for performances in all other parts of the world.

==Achievements and accreditations==
* Two [[List of music recording certifications|Platinum singles]] ("Super Soaker" and "Party Bass") and one Gold single ("Big Booty Bitches")<ref name="aria_b" /><ref name="aria_a" />
* Topped the ARIA Club Chart and was also the highest-selling Australian dance single of 2012
* "Better Luck Next Time" named a ''Billboard'' Breakout Dance track<ref name="aria"/>
* Bombs Away is ranked number 1 in Western Australia and number 8 in Australia according to the In the Mix Top 50 poll<ref>{{cite web|url=http://www.inthemix.com.au/awards/past_results/2012 |title=The Mix Top50 Poll 2012 |publisher=Inthemix.com.au |accessdate=2014-05-25}}</ref>

;"Super Soaker"
* Certified Platinum<ref name="aria_a" />
* Number 1 Australian dance single on the ARIA Charts, one of the Top 50 Dance Singles 2013, and overall top 50 Australian artist singles<ref name="aria" />

;"Party Bass"
* Certified Platinum<ref name="aria_a" />
* Reached number 1 on the Independent Music Chart<ref name="vmusic" />
* Reached number 3 on the iTunes Dance Chart<ref name="bare_url" />
* Fourth-highest selling Australian dance single of 2012<ref name="aria"/>

;"Big Booty Bitches"
* Certified Gold by ARIA<ref name="aria_b" />
* Reached number 3 on the Beatport Electro Top 100<ref name="promote-records" />
;"Get Stoopid"
* Reached number 1 on the ARIA Club Chart<ref name="ariacharts" /> and number 27 on the ARIA Top 50 Club Songs of 2012 and number 27 on the ARIA Australian Artist Singles of 2012<ref name="aria_d" />
;"Drunk Arcade"
* Debuted at number 15 on the ARIA Dance Singles Chart<ref name="ariacharts_a" />
;"Assassinate"
* Peaked at number 4 on Electro Releases on Beatport<ref name="beatport" /><ref>{{cite web|url=http://www.beattracker.com/artist/bombs-away/156230 |title=Bombs Away has 1 items in the top100 on Beatport™ (Aug 09 2013)  |publisher=Beattracker.com |date= |accessdate=2013-08-09}}</ref>
;"Looking for Some Girls"
* Reached number 7 on the ARIA Club Chart

==Discography==
{{Cleanup|section|reason=non use of wikitable(s), formatting|date=August 2013}}

===Singles===
* "Big Booty Bitches" (Central Station Records)
* "Swagger" (Central Station Records)
* "Super Soaker" (Central Station Records)
* "Get Stoopid" featuring [[Seany B]] (Central Station Records)
* "Looking for Some Girls" featuring Kronic (Central Station Records)
* "Party Bass" featuring The Twins (Central Station Records)
* "Drunk Arcade" (Central Station Records)
* "Assassinate" (Central Station Records)
* "Better Luck Next Time" (Central Station Records)
* "Mothertruckers" with Tenzin (Central Station Records)
* "Apple Juice & Vodka" with Komes (Central Station Records)
* "Squats" with Oh Snap! (Central Station Records)

===Official remixes===
* "Driving Around" by [[Pitbull (rapper)|Pitbull]] featuring [[David Rush (rapper)|David Rush]] and Vein
* "Let Go" by Chris Arnott featuring Daddy Longlegs (Onelove)
* "Ridiculous" by Big Nab featuring Bombs Away (Homebrew Records)	
* "[[Can't Fight This Feeling (Junior Caldera song)|Can't Fight This Feeling]]" by [[Junior Caldera]] featuring [[Sophie Ellis-Bextor]] (Xelon Entertainment)	
* "Take Me Away" by Fabian Gray featuring Yianna (Central Station Records)
* "Boys Just Want to Have Sex" by Army of DJs vs. Exude	(Xelon Entertainment)
* "Face Melt" (Bombs Away VIP Edit) by TJR featuring Whiskey Pete (Klub Kids)
* "Speakers 2 Ya Sneakers" by Slappin' Plastic featuring Xamplify and KT (Straight Up!)
* "Not All About the Money" by [[Timati]] and La La Land featuring [[Timbaland]] and Grooya (Hussle Recordings)
* "Getting Nasty" by KCB and Lady Lauryn (LNG Music)
* "Over You" by [[Freestylers]] (Xelon Entertainment)
* "Serotonin" by G-Wizard and Joey Kaz (Bombsquad)
* "Saved in a Bottle" by [[The Potbelleez]] (Hussle Recordings)
* "Watch You" by [[Clinton Sparks]] featuring Pitbull and Disco Fries (Strictly Rhythm)
* "Everybody Get Up" by Jam Xpress and Seany B (Central Station Records)
* "STFU" by Ze! and Lusha featuring Bombs Away (Tuffem Up! Records)
* "The Night" by Mind Electric (Hussle Recordings)
* "Midnight Freak Circus" by Rowan P and Nino Live (Central Station Records)
* "Listen to the Bass" by Che Jose (One Platinum Records)
* "Change Your Heart" by Shave (Hussle Recordings)
* "[[Summer (Calvin Harris song)|Summer]]" by [[Calvin Harris]]
* "[[A Sky Full of Stars]]" by [[Coldplay]]
* "[[Shot Me Down]]" by David Guetta featuring [[Skylar Grey]]

===Compilation albums===
* ''Wild Nights 2011''<ref name="centralstation" />
* ''Wild Nights 2012''<ref>{{cite web|url=http://www.soapboxartists.com.au/post/12/04/wild-nights-tour-bombs-away-dj-kronic |title=DJ and Artist Bookings & Management |publisher=Soapbox Artists |date=2012-04-05 |accessdate=2015-02-26}}</ref>
* ''Ministry of Sound: Addicted to Bass'' (#2 ARIA Compilation Albums)<ref>{{cite web|author= |url=http://www.radikal.com/2012/06/04/bombs-aways-new-compilation-tops-the-aria-charts/#.Ufj22mQpa3U |title=Bombs Away’s New Compilation Tops The Aria Charts &#124; Radikal Records |publisher=Radikal.com |date= |accessdate=2013-08-05}}</ref><ref>{{cite web|url=http://www.ministryofsound.com.au/news/2012/5/10/addicted-bass-bombs-away-kid-kenobi-interview/ |title=Addicted To Bass: Bombs Away & Kid Kenobi Interview &#124; Ministry of Sound Australia |publisher=Ministryofsound.com.au |date= |accessdate=2013-08-05}}</ref>
* ''Ministry of Sound: Maximum Bass''

==References==
{{Reflist|colwidth=30em}}

==External links==
* [http://www.letmeseeyourswagger.com Official Bombs Away website]

{{Authority control}}
[[Category:Australian electronic music groups]]
[[Category:2010 establishments in Australia]]
[[Category:Musical groups established in 2010]]
[[Category:Australian house music groups]]
[[Category:Musical groups from Perth, Western Australia]]
[[Category:Queensland musical groups]]
[[Category:Musicians from Gold Coast, Queensland]]