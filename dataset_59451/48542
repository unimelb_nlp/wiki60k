{{Infobox Officeholder
|name        = Paul Hollis
|image       = Hollis Headshot.jpg
|state_house = Louisiana
|district    = 104th
|term_start  = January 9, 2012
|term_end    = 
|predecessor = [[Nita Hutter]]
|successor   = 
|birthname   = Paul Bryan Hollis
|birth_date  = {{birth date and age|1972|9|1}}
|birth_place = [[Metairie, Louisiana|Metairie]], [[Louisiana]], [[United States|U.S.]]
|death_date  =
|death_place =
|party       = [[Republican Party (United States)|Republican]]
|spouse      = Ashley Tastet
|children    = 2
|education   = [[Louisiana State University|Louisiana State University,<br>Baton Rouge]] {{small|([[Bachelor of Arts|BA]])}}
|website     = {{url|paulhollis.com|Campaign website}}
}}
'''Paul Bryan Hollis''' (born September 1, 1972) is a [[Republican Party (United States)|Republican]] member of the [[Louisiana House of Representatives]] for the revised 104th District in [[St. Tammany Parish, Louisiana|St. Tammany Parish]] in southeastern [[Louisiana]].

A son of the late [[Louisiana State Senate|State Senator]] [[Ken Hollis]], Paul Hollis defeated fellow Republican Christopher Trahan, 3,905 votes (56 percent) to 3,096 (44 percent) in the [[nonpartisan blanket primary]] held on October 22, 2011.<ref>{{cite web|url=http://staticresults.sos.la.gov/10222011/10222011_43589.html|title=Louisiana Secretary of State – Election Results by Parish |publisher=Louisiana Secretary of State|accessdate=October 23, 2011}}</ref>

Hollis was an announced candidate for the [[United States Senate election in Louisiana, 2014]], but withdrew from the race in July. Victory went to another Republican, the physician and [[United States House of Representatives|U.S. Representative]] [[Bill Cassidy]] of [[Baton Rouge]].

==Background==

Paul Hollis was born in [[Metairie, Louisiana|Metairie]] in [[Jefferson Parish, Louisiana|Jefferson Parish]], the third son of Judy and Ken Hollis. He graduated in 1990 from [[Grace King High School]] in Metairie, at which he was the student body president.

Hollis held his first job – for a lawn care service – at the age of thirteen and worked through high school as a restaurant busboy. After studying martial arts for many years, he attained his black belt in [[karate]] and began teaching at Elmwood Fitness Center before his senior year.

At [[Louisiana State University]] in Baton Rouge, Hollis opened his first small business, a karate school. He received a degree in [[political science]] from LSU in 1994.

Paul met Ashley Tastet in 1996, and they married a few years later. In 2003, the couple had their daughter, Bree. In 2006, the Hollises settled near [[Mandeville, Louisiana|Mandeville]] in St. Tammany Parish. In 2013, Paul and Ashley had their second child, son Zachary.

==Business==

Hollis began collecting coins in 1978, at the age of six, when he received a Peace dollar from a grandmother.<ref>{{cite web|url=http://www.coinnews.net/2008/05/06/louisana-coin-dealer-running-for-us-senate-4075/|title=Paul Hollis to Announce Candidacy – Louisiana Coin Dealer Running for U.S. Senate|publisher=coinlink|accessdate=March 6, 2013}}</ref> He still has the keepsake coin.

Hollis was employed by as the youngest salesperson at Blanchard and Company of [[New Orleans, Louisiana|New Orleans]], one of the nation's largest rare coin and precious metal firms. He worked there until 2001, when he attained the position of "[[numismatist]]" or coin expert.

In 2003, Hollis started his own firm, Paul Hollis Rare Coins, supplying collectible coins to television home shopping networks located in [[Tennessee]] and [[Minnesota]]. Paul Hollis Rare Coins now specializes in ancient coins that circulated during the earthly lifetime of [[Jesus Christ]].

Hollis is one of the few numismatists to visit and undertake an in-depth of study of each of the [[United States Mint]]s. He has achieved status of expert in treasure coins, often being interviewed by national and international media outlets.

In the winter of 2004, Hollis joined the [[Odyssey Marine Exploration]] to salvage coins from the [[SS Republic (1853)]].<ref>{{cite web|url=http://www.coinweek.com/Directory/listings-by-state/louisiana/paul-hollis-rare-coins/|title=Paul Hollis Rare Coins|publisher=''Coin Week''|accessdate=March 15, 2013}}</ref>

In 2009, Hollis gave 1,000,000 Lincoln cents.<ref>{{cite web|url=http://www.numismaticnews.net/article/hollis_to_give_away_40000_lincoln_cents|title=Hollis to give away 40,000 Lincoln cents|publisher=Numismatic News|accessdate=March 15, 2013}}</ref> He also gave 1,050 Adams dollars to students at the middle school he attended, John Quincy Adams Middle School.<ref>{{cite web|url=http://www.numismaticnews.net/article/students_given_dollars|title=Students given dollars|publisher=''Numismatic News''|accessdate=March 15, 2013}}</ref>

In 2008, Hollis notably arranged for and escorted the 1844-0 Proof $10 Eagle, worth $2.5 million, to be on display at the Old U.S. Mint of the Louisiana State Museum in Baton Rouge.<ref>{{cite web|url=http://www.coinlink.com/News/us-coins/paul-hollis-brings-25-million-1844-o-proof-10-home/|title=1844-0 Proof Returned Home|publisher=Coin News|accessdate=March 15, 2013}}</ref> This allowed twenty thousand persons to see the coin where it was minted,

==Legislative career==

Hollis was elected to the Louisiana House in November 2011. His District 104 was newly redrawn as a result of the legislative [[redistricting]] plan, Act 1 of the 1st Extraordinary Session of 2011, signed into law by [[governor of Louisiana|Governor]] [[Bobby Jindal]] on April 14, 2011.<ref>{{cite web|url=http://legis.state.la.us/billdata/History.asp?sessionid=111ES&billid=HB1|title=HB1 – 2011 1st Extraordinary Session (Act 1)|publisher=Louisiana Legislature|accessdate=October 23, 2011}}</ref> The district covers parts of the St. Tammany Parish communities of Mandeville, [[Abita Springs, Louisiana|Abita Springs]], [[Covington, Louisiana|Covington]], [[Lacombe, Louisiana|Lacombe]], [[Pearl River, Louisiana|Pearl River]], and [[Slidell, Louisiana|Slidell]].<ref>{{cite web|url=http://house.louisiana.gov/h_redistricting2011/BillsRedsitHouse/BillsRedsitHouse_ACT1.htm|title=Louisiana House of Representatives – Redistricting PSC|publisher=Louisiana Legislature|accessdate=October 23, 2011}}</ref>

During his first year in office, Hollis was part of the Louisiana Bicentennial Task Force and assisted with an exhibit at the Old State Capitol and the writing and production of the "History of Banking and Finance in Louisiana" in cooperation with the Secretary of State's office. 
 
Hollis serves on the Commerce, Education, and Retirement committees.<ref name=votesmart>{{cite web|url=http://votesmart.org/candidate/biography/134639/paul-hollis|title=Paul Hollis' Biography|publisher=votesmart.org|accessdate=January 15, 2014}}</ref>

==Writing career==

Hollis is the author of the book ''American Numismatist'', written for the American Numismatic Association. He traces U.S. history through the coins that were struck along the way. With many significant pictures and images of coins, the book is considered a resource to help beginners learn the trade.<ref name="Silver Towne L.P">{{cite web|url=http://www.silvertowne.com/p-21106-american-numismatist-book-by-paul-hollis.aspx|title=''American Numismatist'' (Book by Paul Hollis)|publisher=Silver Towne L.P.|accessdate=March 15, 2013}}</ref>

John Albanese, one of the world's top-ranked coin graders, wrote a foreword to Hollis book.<ref name="Silver Towne L.P"/>

==Acknowledgments==
Hollis has received the:

*Business Champion Award from Southwest Louisiana Economic Development Alliance<ref>{{cite web|url=http://allianceswla.org/PageDisplay.asp?P1=8708|title=Pro-Business Louisiana Legislators Lauded at Legis-Gator Luncheon |publisher=Southwest Louisiana Economic Development Alliance |accessdate=March 11, 2013}}</ref>
*A 97 percent rating from the [[Louisiana Association of Business and Industry]]ABI<ref>{{cite web|url=http://www.labi.org/legislature-info/legislator-detail/hollis-paul-b|title=LABI Legislator Ranking |publisher=Louisiana Association of Business and Industry |accessdate=March 11, 2013}}</ref>
*Outstanding Family Advocate from the [[Louisiana Family Forum]]<ref>{{cite web|url=http://www.lafamilyforum.org/2012-legislative-award-winners/|title=2012 Legislative Award Winners |publisher=Louisiana Family Forum |accessdate=March 11, 2013}}</ref>
*Best U.S. Coin Book 2012 award for ''American Numismatist''

Hollis was nominated by former [[Speaker of the Louisiana House of Representatives|Louisiana House Speaker]] [[Chuck Kleckley]] to serve as a member of the Louisiana Commission on Marriage and Family. He is also a member of the newly-formed Gulf Coast Legislative Council.

{{Portalbar|Biography|Louisiana|Politics|Conservatism|Methodism|Books|Business and Economics}}

==References==
{{Reflist}}

==External links==
*{{Official website|http://paulhollis.com/}}
*[http://house.louisiana.gov/h_reps/members.asp?ID=104 Rep. Paul Hollis] at the Louisiana House of Representatives
*{{CongLinks | congbio = | ballot = Paul_Hollis | nndb = | votesmart = 134639 | fec = S4LA00131 | followthemoney = 412722 | c-span = | imdb = }}

{{Louisiana House of Representatives}}

{{DEFAULTSORT:Hollis, Paul}}
[[Category:1972 births]]
[[Category:21st-century American politicians]]
[[Category:American non-fiction writers]]
[[Category:Businesspeople from Louisiana]]
[[Category:Grace King High School alumni]]
[[Category:Living people]]
[[Category:Louisiana State University alumni]]
[[Category:Louisiana Republicans]]
[[Category:Members of the Louisiana House of Representatives]]
[[Category:People from Metairie, Louisiana]]
[[Category:People from Mandeville, Louisiana]]
[[Category:American United Methodists]]