{{Use mdy dates|date=February 2013}}
{{Infobox journal
| title = Law and Human Behavior
| cover = [[Image:Law and Human Behaviour journal cover.jpg|Law and Human Behavior]]
| editor = Margaret Bull Kovera
| discipline = [[Legal psychology]], [[forensic psychology]]
| abbreviation =Law Hum Behav
| publisher = [[American Psychological Association]]
| country =
| frequency = Bimonthly
| history = 1977–present
| openaccess =
| impact = 2.542
| impact-year = 2015
| website = http://www.apa.org/pubs/journals/lhb/index.aspx
| link1 = http://content.apa.org/journals/lhb
| link1-name = Online access
| link2 =
| link2-name =
| JSTOR =
| OCLC = 03173559
| LCCN = 77641812
| CODEN = LHBEDM
| ISSN = 0147-7307
| eISSN = 1573-661X
}}
'''''Law and Human Behavior''''' is a bimonthly [[academic journal]] published by the [[American Psychology–Law Society]]. It publishes original empirical papers, reviews, and [[meta-analysis|meta-analyses]] on how the law, legal system, and legal process relate to human behavior, particularly [[legal psychology]] and [[forensic psychology]].<ref>{{cite web |date=January 3, 2012 | url=http://www.apa.org/pubs/journals/lhb/index.aspx |title=Law and Human Behavior |publisher=[[American Psychological Association]] |accessdate=2012-01-03}}</ref> The current [[editor-in-chief]] is Margaret Bull Kovera ([[John Jay College of Criminal Justice]]). Past editors have been Brian Cutler ([[University of Ontario Institute of Technology]]), Richard Weiner ([[University of Nebraska]]), [[Ronald Roesch]] ([[Simon Fraser University]]), [[Michael J. Saks]] ([[Arizona State University]]), and Bruce Sales ([[University of Arizona]]).

== Abstracting and indexing ==
The journal is abstracted and indexed by [[MEDLINE]]/[[PubMed]] and the [[Social Science Citation Index]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 2.542, ranking it 11th out of 62 journals in the category "Psychology, Social"<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Psychology, Social |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2016-10-11 |series=Web of Science |postscript=.}}</ref> and  13th out of 147 journals in the category "Law".<ref name=WoS2>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Law |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2016-10-11 |series=Web of Science |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.apa.org/pubs/journals/lhb/index.aspx}}

[[Category:American law journals]]
[[Category:Forensic psychology journals]]
[[Category:Publications established in 1977]]
[[Category:Bimonthly journals]]
[[Category:English-language journals]]
[[Category:American Psychological Association academic journals]]