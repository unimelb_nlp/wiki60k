{{Refimprove|date=May 2008}}
[[File:Qantas b747 over houses arp.jpg|thumb|upright=1.14|A [[Qantas]] [[Boeing 747-400]] passes close to houses on the boundary of [[London Heathrow Airport]], England.]]

'''Aircraft noise''' is [[noise pollution]] produced by any aircraft or its components, during various phases of a flight: on the ground while parked such as auxiliary power units, while taxiing, on run-up from propeller and jet exhaust, during take off, underneath and lateral to departure and arrival paths, over-flying while en route, or during landing.{{Citation needed|date=September 2008}}

==Mechanisms of sound production==
[[Image:GrummanAA-5BTigerInFlight01.jpg|thumb|250px|Small [[general aviation]] aircraft produce localized aircraft noise.]]
[[Image:RotorwayExec01.jpg|thumb|250px|[[Helicopter]] main and tail rotors produce aerodynamic noise.]]

A moving aircraft including the [[jet engine]] or [[Propeller (aircraft)|propeller]] causes compression and rarefaction of the air, producing motion of air molecules. This movement propagates through the air as pressure waves. If these pressure waves are strong enough and within the audible [[frequency]] spectrum, a sensation of hearing is produced. Different aircraft types have different noise levels and frequencies. The noise originates from three main sources:

* Aerodynamic noise
* Engine and other mechanical noise
* Noise from aircraft systems

===Aerodynamic noise===
Aerodynamic noise arises from the airflow around the aircraft [[fuselage]] and control surfaces. This type of noise increases with aircraft speed and also at low altitudes due to the density of the air. Jet-powered aircraft create intense noise from [[aerodynamic]]s. Low-flying, high-speed military aircraft produce especially loud aerodynamic noise.

The shape of the nose, windshield or [[canopy (aircraft)|canopy]] of an aircraft affects the sound produced. Much of the noise of a propeller aircraft is of aerodynamic origin due to the flow of air around the blades. The [[helicopter]] main and tail rotors also give rise to aerodynamic noise. This type of aerodynamic noise is mostly low frequency determined by the rotor speed.

Typically noise is generated when flow passes an object on the aircraft, for example the wings or landing gear. There are broadly two main types of airframe noise:

* Bluff Body Noise – the alternating vortex shedding from either side of a bluff body, creates low pressure regions (at the core of the shed vortices) which manifest themselves as pressure waves (or sound). The separated flow around the bluff body is quite unstable, and the flow "rolls up" into ring vortices—which later break down into turbulence.<ref name="bluff">{{cite web|url=http://www2.eng.cam.ac.uk/~acf40/research_ov.htm|title=Aircraft Airframe Noise - Research Overview|accessdate=2008-07-13}}</ref>
* Edge Noise – when turbulent flow passes the end of an object, or gaps in a structure (high lift device clearance gaps) the associated fluctuations in pressure are heard as the sound propagates from the edge of the object (radially downwards).<ref name="bluff"/>

===Engine and other mechanical noise===
Much of the noise in propeller aircraft comes equally from the propellers and aerodynamics. Helicopter noise is aerodynamically induced noise from the main and tail rotors and mechanically induced noise from the main gearbox and various transmission chains. The mechanical sources produce narrow band high intensity peaks relating to the rotational speed and movement of the moving parts. In [[computer model]]ling terms noise from a moving aircraft can be treated as a [[line source]].

Aircraft gas turbine engines ([[jet engine]]s) are responsible for much of the aircraft noise during takeoff and climb, such as the ''buzzsaw noise'' generated when the tips of the fan blades reach supersonic speeds. However, with advances in noise reduction technologies—the airframe is typically more noisy during landing.{{citation needed|date=May 2008}}

The majority of engine noise is due to jet noise—although high bypass-ratio [[turbofan]]s do have considerable fan noise. The high velocity jet leaving the back of the engine has an inherent shear layer instability (if not thick enough) and rolls up into ring vortices. This later breaks down into turbulence. The SPL associated with engine noise is proportional to the jet speed (to a high power) therefore, even modest reductions in exhaust velocity will produce a large reduction in Jet Noise.{{citation needed|date=May 2008}}

===Noise from aircraft systems===
[[Cockpit]] and cabin [[pressurization]] and conditioning systems are often a major contributor within cabins of both civilian and military aircraft. However, one of the most significant sources of cabin noise from commercial jet aircraft, other than the engines, is the [[Auxiliary Power Unit]] (APU), an on‑board [[Electric generator|generator]] used in aircraft to start the main engines, usually with [[compressed air]], and to provide electrical power while the aircraft is on the ground. Other internal aircraft systems can also contribute, such as specialized electronic equipment in some military aircraft.

==Health effects==
{{Further|Health effects from noise}}
There are [[health]] consequences of elevated [[Sound exposure level|sound level]]s.  Elevated workplace or other [[noise]] can cause [[hearing impairment]], [[hypertension]], [[ischemic heart disease]], [[annoyance]], [[sleep disturbance]], and decreased school performance. Although some hearing loss occurs naturally with age,<ref name = Rosenhall1990>{{cite journal |vauthors=Rosenhall U, Pedersen K, Svanborg A |title=Presbycusis and noise-induced hearing loss |journal=Ear Hear |volume=11 |issue=4 |pages=257–63 |year=1990 |pmid=2210099 |doi=10.1097/00003446-199008000-00002}}</ref> in many developed nations the impact of noise is sufficient to impair hearing over the course of a lifetime.<ref name=Schmid>{{cite news|url=http://www.cbsnews.com/stories/2007/02/18/ap/health/mainD8NC00AO0.shtml |last=Schmid |first=RE |title=Aging nation faces growing hearing loss |publisher=[[CBS News]] |date=2007-02-18 |accessdate=2007-02-18 |deadurl=yes |archiveurl=https://web.archive.org/web/20071115131020/http://www.cbsnews.com/stories/2007/02/18/ap/health/mainD8NC00AO0.shtml |archivedate=November 15, 2007 }}</ref><ref>Senate Public Works Committee, ''[[Noise Pollution and Abatement Act]] of 1972'', S. Rep. No. 1160, 92nd Cong. 2nd session</ref> Elevated noise levels can create stress, increase workplace accident rates, and stimulate aggression and other anti-social behaviors.<ref name="isbn0-12-427455-2">{{cite book |author=Kryter, Karl D. |title=The handbook of hearing and the effects of noise: physiology, psychology, and public health |publisher=Academic Press |location=Boston |year=1994 |pages= |isbn=0-12-427455-2 |oclc= |doi=}}</ref>

===German environmental study===
A large-scale statistical analysis of the health effects of aircraft noise was undertaken in the late 2000s by Bernhard Greiser for the [[Umweltbundesamt]], Germany's central environmental office. The health data of over one million residents around the Cologne airport were analysed for health effects correlating with aircraft noise. The results were then corrected for other noise influences in the residential areas, and for socioeconomic factors, to reduce possible skewing of the data.<ref name="Tödlicher Lärm' 2009, Page 45">''Tödlicher Lärm'' - [[Spiegel (magazine)|Spiegel]]'', Nr. 51, 14 Dezember 2009, Page 45 {{de icon}}</ref>

The German study concluded that aircraft noise clearly and significantly impairs health.<ref name="Tödlicher Lärm' 2009, Page 45"/> For example, a day-time average sound pressure level of 60 [[decibel]] increasing coronary heart disease by 61% in men and 80% in women. As another indicator, a night-time average sound pressure level of 55 [[decibel]] increased the risk of heart attacks by 66% in men and 139% in women. Statistically significant health effects did however start as early as from an average sound pressure level of 40 [[decibel]].<ref name="Tödlicher Lärm' 2009, Page 45"/>

===FAA advice===
The FAA says that a maximum day-night average sound level of 65&nbsp;dB is incompatible with residential communities.<ref>{{cite web|title=Noise Monitoring|url=http://www.massport.com/environment/environmental-reporting/noise-abatement/noise-monitoring/|publisher=Massport|accessdate=31 January 2014}}</ref> Communities in affected areas may be eligible for mitigation such as soundproofing.

===Inside aircraft===
Noise associated with aircraft does not only affect people on the ground, but also those within the aircraft (e.g., flight crew, cabin crew and passengers). While there appears to be little research in this area, lower noise inside the aircraft is widely promoted as a benefit for new aircraft. The noise levels inside an Airbus A321 during cruise have been reported as approximately 78&nbsp;dB(A). During taxi when the aircraft engines are producing minimal thrust, noise levels in the cabin have been recorded at 65&nbsp;dB(A).<ref>Ozcan HK, Nemlioglu S. In-cabin noise levels during commercial aircraft flights. Can Acoust 2006;34:31-5.</ref> This is approximately 20 decibels louder than recommended acceptable levels for an office but 20 decibels below the occupational noise exposure limits of 85&nbsp;dB(A).<ref>Standards Australia, AS/NZS 2107. Acoustics-Recommended Design Sound Levels and Reverberation Times for Building Interiors. AUS: Standards Australia, Sydney; 2000.</ref>

===Cognitive effects===

Simulated aircraft noise at 65&nbsp;dB(A) has been shown to negatively affect individuals’ memory and recall of auditory information.<ref>Molesworth BR, Burgess M. (2013). Improving intelligibility at a safety critical point: In flight cabin safety. Safety Science, 51, 11-16.</ref> In one study comparing the effect of aircraft noise to the effect of alcohol on cognitive performance, it was found that simulated aircraft noise at 65&nbsp;dB(A) had the same effect on individuals’ ability to recall auditory information as being intoxicated with a Blood Alcohol Concentration (BAC) level of at 0.10.<ref>Molesworth BR, Burgess M, Gunnell B. (2013). Using the effect of alcohol as a comparison to illustrate the detrimental effects of noise on performance. Noise & Health, 15, 367-373.</ref> A BAC of 0.10 is double the legal limit required to operate a motor vehicle in many developed countries such as Australia.

==Noise mitigation programs==
{{further|Noise mitigation}}
[[File:B787-2139.jpg|thumb|250px|Noise-reducing [[chevron (aerospace)|chevrons]] on a [[Rolls-Royce Trent 1000]] turbofan engine.<ref>{{cite journal |url=http://www.afmc.org.cn/13thacfm/invited/201.pdf  |work=Proceedings of the 13th Asian Congress of Fluid Mechanics 17–21 December 2010, Dhaka, Bangladesh|title=Evolution from 'Tabs' to 'Chevron Technology’–a Review | format=PDF |author1=Zaman, K.B.M.Q.|author2=Bridges, J. E.|author3=Huff, D. L.|publisher=[[NASA Glenn Research Center]]. Cleveland, OH.}} 1.34 Mb</ref>]]

In the United States, since aviation noise became a public issue in the late 1960s, governments have enacted legislative controls. Aircraft designers, manufacturers, and operators have developed quieter aircraft and better operating procedures. Modern high-bypass [[turbofan]] engines, for example, are quieter than the [[turbojet]]s and low-bypass turbofans of the 1960s. First, FAA Aircraft Certification achieved noise reductions classified as "Stage&nbsp;3" aircraft; which has been upgraded to "Stage&nbsp;4" noise certification resulting in quieter aircraft. This has resulted in lower noise exposures in spite of increased traffic growth and popularity.<ref name="Stage 4 Aircraft Noise Standards">{{cite web|url=http://rgl.faa.gov/Regulatory_and_Guidance_Library/rgFinalRule.nsf/0/767f48f6311bfa348625703700522b0b!OpenDocument |title=Stage 4 Aircraft Noise Standards |publisher=Rgl.faa.gov |date= |accessdate=2012-09-28}}</ref>

In the 1980s the [[U.S. Congress]] authorized the [[FAA]] to devise programs to insulate homes near airports. While this does not address the external noise, the program has been effective for residential interiors. Some of the first airports at which the technology was applied were [[San Francisco International Airport]] and [[San Jose International Airport]] in California. A computer model is used which simulates the effects of aircraft noise upon building structures. Variations of aircraft type, flight patterns and local [[meteorology]] can be studied. Then the benefits of building retrofit strategies such as roof upgrading, window [[insulated glazing|glazing]] improvement, fireplace baffling, [[caulking]] construction seams can be evaluated.<ref name="Hogan">Hogan, C. Michael and Jorgen Ravnkilde, ''Design of acoustical insulation for existing residences in the vicinity of San Jose Municipal Airport'', 1 January 1984, FAA grant-funded research, ISBN B0007B2OG0</ref>

===Night flying restrictions===
At [[Heathrow Airport|Heathrow]], [[Gatwick Airport|Gatwick]] and [[Stansted Airport|Stansted]] airports in [[London]], UK and [[Frankfurt Airport]] in Germany, [[night flying restrictions]] apply to reduce noise exposure at night.<ref name="DFT">{{cite web|url=http://www.dft.gov.uk/press/speechesstatements/statements/nightflyingrestrictionsathea5940 |title=Night flying restrictions at Heathrow, Gatwick and Stansted Airports |accessdate=2008-07-12 |last=Dept for Transport |authorlink= |date=June 2006 |deadurl=yes |archiveurl=https://web.archive.org/web/20070717055137/http://www.dft.gov.uk/press/speechesstatements/statements/nightflyingrestrictionsathea5940 |archivedate=July 17, 2007 }}</ref><ref name="DFT2">{{cite web|url = http://www.dft.gov.uk/consultations/archive/2005/nrheahtgatst/nightrestrictionsatheathrowg1775?page=19|title =Night restrictions at Heathrow, Gatwick and Stansted (second stage consultation)|accessdate = 2008-07-12|last = Dept for Transport|authorlink = |year = n.d.}}</ref>

=== Satellite-based navigation systems ===
A series of trials were undertaken at London's Heathrow Airport, between December 2013 and November 2014, as part of the UK's "Future Airspace Strategy", and the Europe-wide "Single European Sky" modernisation project. The trials demonstrated that using satellite-based navigation systems it was possible to offer noise relief to more surrounding communities. The study found that steeper angles for take-off and landing led to fewer people experiencing aircraft noise and that noise relief could be shared by using more precise flight paths, allowing control of the noise footprint of departing aircraft. Noise relief could be enhanced by switching flight paths, for example by using one flight path in the morning and another in the afternoon.<ref>{{cite web|url=http://www.heathrow.com/noise/future-plans/modernising-uk-airspace|title=Modernising UK airspace|work=heathrow.com|accessdate=24 September 2015}}</ref>

==See also==
*[[Electric airplane]]
*[[Farley v Skinner]]
*[[Hush kit]]
*[[Helicopter noise reduction]]
*[[Jet noise]]
*[[Noise barrier]]
*[[Rotor-stator interaction]]
*[[Silent Aircraft Initiative]]
*[[Train noise]]
*[[XF-84H Thunderscreech]], the loudest aircraft ever built.

'''General:'''
*[[Health effects from noise]]
*[[Noise pollution]]
*[[Noise regulation]]
*[[Aviation and the environment]]

==References==
{{reflist}}
*''U.S. Noise Control Act of 1972'' United States [[Code]] Citation: 42 U.S.C. 4901 to 4918
*S. Rosen and P. Olin, ''[[Hearing loss]] and [[coronary heart disease]], Archives of Otolaryngology'', 82:236 (1965)

==External links==
*[http://www.southampton.ac.uk/antc Airbus Noise Technology Centre] United Kingdom
*[http://www.airportnoiselaw.org/ Airport Noise Law] United States
*[http://www.easa.europa.eu/document-library/noise-type-certificates-approved-noise-levels EASA approved noise levels] Germany
*[http://www.fican.org/ Federal Interagency Committee on Aviation Noise (FICAN)]
*[http://www.aef.org.uk/ Aviation Environment Federation (AEF)] United Kingdom
*[http://www.soundinitiative.org/ Sound Initiative: A Coalition for Quieter Skies] United States
*[http://airportnoisereport.com/ Airport Noise Report] A weekly newsletter on litigation, regulations, and technological developments
*[http://www.aviation-noise.org/ National Organization to Insure a Sound-controlled Environment(NOISE)] United States
*[http://www.dft.gov.uk/pgr/aviation/environmentalissues/Anase/ Attitudes to Noise from Aviation Sources in England (ANASE)] Study published by the United Kingdom [[Department for Transport]]
*[http://silentaircraft.org/ The 'Silent' Aircraft Initiative]
*[https://www.youtube.com/watch?v=twkaRK74vqQ Cruise altitude plane noise example]

{{DEFAULTSORT:Aircraft Noise}}
[[Category:Noise pollution]]
[[Category:Aviation and the environment]]