{{Italic title}}
{{Infobox journal
| title        = Harvard Civil Rights - Civil Liberties Law Review
| cover        =
| editor       = Jimin He, Shayna Medley, and Matthew Ryan
| discipline   = [[Law review]]
| abbreviation = Harv. Civ. Rights-Civ. Liberties Law Rev.
| publisher    = [[Harvard Law School]]
| country      = [[United States]]
| frequency    = Biannually
| history      = 1966-present
| openaccess   =
| license      =
| impact       = 1.421
| impact-year  = 2008
| website      = http://www.harvardcrcl.org/
| link1        =
| link1-name   =
| link2        =
| link2-name   =
| RSS          =
| atom         =
| JSTOR        =
| OCLC         = 06031621
| LCCN         = sn82005086
| CODEN        =
| ISSN         = 0017-8039
| eISSN        = 2153-2389
}}
The '''''Harvard Civil Rights-Civil Liberties Law Review''''' is a student-run [[law review]] published by [[Harvard Law School]].<ref name=pubs>{{cite web |url=http://www.law.harvard.edu/current/orgs/journals/index.html |title=Journals and Publications |work= |accessdate=2010-02-23}}</ref> The journal is published two times per year and contains articles, essays, and book reviews concerning [[civil and political rights|civil rights]] and [[civil liberties|liberties]].<ref name=CRCL>{{cite web |url=http://harvardcrcl.org/ |title=''Harvard Civil Rights-Civil Liberties Law Review'' Home page |format= |work= |accessdate=2010-02-23}}</ref> In 2009 its online companion ''Amicus'' was launched, which features standard length journal articles coupled with online responses.<ref name=Amicus>{{cite web |url=http://harvardcrcl.org/amicus/ |title=Amicus » Online Supplement to ''Harvard Civil Rights-Civil Liberties Law Review''|format= |work= |accessdate=2010-02-23}}</ref>

==History==
The journal was established in Spring 1966 in the wake of the [[Civil Rights Act of 1964]] and the [[Voting Rights Act]] of 1965. In their first issue the editors of the new publication wrote that the review "is an emblem and achievement of the collaboration" between the Harvard Civil Liberties Research Service, the Law Students Civil Rights Research Council, and the Harvard Civil Rights Committee, three newly formed organizations that had recently noticed the dearth of legal material on civil rights:

<blockquote>
Still, there is today hardly a journal which regularly and completely dedicates its pages to the civil rights revolution and the modern manifestations of the relation between citizen and state. Nor is there any review steadily providing Southern lawyers with library ammunition. Nor does any publication capitalize on the burgeoning interest in rights and liberties among this new generation of law students. Nor does any review endeavor to link together the students and faculties of the various law schools in such a cooperative enterprise.

These are among our aims.

But most important. Ours is to be a review of revolutionary law. Such an ideal is as new as United Nation Declarations on Human Rights and as old as the "Grand Tradition" of Common Law fashioning causes of action to rights and wrongs.<ref>{{cite journal|title=Preface|journal=Harv. Civ. Rights-Civ. Liberties Law Rev. |date=1966|first=|last=|volume=1|issue=|pages=iii|id= |url=|format=|accessdate= }}</ref>
</blockquote>

In its 35th anniversary issue, legal academic [[Morton Horowitz]] wrote that the journal "seeks to catalyze progressive thought and dialogue through publishing innovative legal scholarship from various perspectives and in diverse fields of study."<ref>{{cite journal|title=A Brief History of the Harvard Civil Rights and Civil Liberties Law Review|journal=Harv. Civ. Rights-Civ. Liberties Law Rev. |date=2002|first=Morton|last=Horowitz|volume=37|issue=|pages=259|id= |url=|format=|accessdate= }}</ref>

==Notable alumni==
*[[Charles F. Abernathy]]
*[[Deborah Batts]]
*[[James H. Burnley]]
*[[Morgan Chu]]
*[[Stuart E. Eizenstat]]
*[[John R. Evans]]
*[[Robert Fellmeth]]
*[[Jennifer Granholm]]
*[[Mark J. Green]]
*[[Joseph A. Greenaway, Jr.]]<ref name=whitehouse>{{cite web |url=http://www.whitehouse.gov/the_press_office/President-Obama-Nominates-Judge-Joseph-A-Greenaway-Jr-for-the-Third-Circuit-and-Judge-Beverly-B-Martin-for-United-States-Court-of-Appeals-for-the-Eleventh-Circuit/ |title=President Obama Nominates Judge Joseph A. Greenaway, Jr. for the Third Circuit, and Judge Beverly B. Martin for United States Court of Appeals for the Eleventh Circuit &#124; The White House |format= |work= |accessdate=2010-02-23}}</ref>
*[[William J. Jefferson]]
*[[Susan Oki Mollway]]
*[[Barack Obama]]
*[[Jorge Rangel]]
*[[Daniel P. Sheehan]]
*[[Cass Sunstein]]

==Notable articles==
*{{cite journal|title=The Federal Government's Power to Protect Negroes and Civil Rights Workers Against Privately Inflicted Harm|journal=Harv. Civ. Rights-Civ. Liberties Law Rev. |date=1966|first=Paul A.|last=Brest|volume=1|issue=|pages=|id= |url=|format=|accessdate=}}
*{{cite journal|title=Freedom from Information: The Act and the Agencies|journal=Harv. Civ. Rights-Civ. Liberties Law Rev. |date=1970|first=Ralph|last=Nader|volume=5|issue=|pages=53|id= |url=|format=|accessdate=|authorlink=Ralph Nader}}
*{{cite journal|title=Pornography, Civil Rights and Speech|journal=Harv. Civ. Rights-Civ. Liberties Law Rev. |date=1985|first=Catherine|last=MacKinnon|volume=20|issue=|pages=1|id= |url=|format=|accessdate=|authorlink=Catharine MacKinnon}}

==References==
{{Reflist|2}}

==External links==
* {{Official website|http://www.harvardcrcl.org/}}

{{Harvard}}

[[Category:Publications established in 1966]]
[[Category:Biannual journals]]
[[Category:American law journals]]
[[Category:English-language journals]]
[[Category:Harvard University academic journals]]
[[Category:Law journals edited by students]]