'''Natasha Kroll''' (1914-2004) was a display and production designer born in [[Moscow]], who moved to Germany with her family in 1922. Most widely known for her production design at the BBC in the late 1950s and early 1960s, Kroll went on to design several feature films.

Her archive is held at the [[University of Brighton Design Archives]].<ref>[http://arts.brighton.ac.uk/collections/design-archives/archives/natasha-kroll Natasha Kroll Archive - University of Brighton Design Archives]</ref>

==Education==
Kroll attended the renowned [[Reimann School]] in Berlin specialising in display design, and joined the staff as an assistant teacher when the school moved to [[London]] in 1936.<ref>Suga, Yasuko, ''The Reimann School'', Artmonsky Arts, 2013, ISBN 978-0-9573875-3-9</ref>

==Career==

===Shop display===
Kroll established her career as a window display designer. Some of her early commissions included Rowntree's department stores in [[York]] and [[Scarborough, North Yorkshire|Scarborough]].

In 1942 Kroll was appointed to the position of display manager for the large retail store [[Simpsons of Piccadilly|Simpson (Piccadilly) Ltd]]. Her pioneering approach and her display philosophy, with its roots in European modernism, complemented the innovative new premises designed by [[Joseph Emberton]].<ref>[http://arts.brighton.ac.uk/collections/design-archives/archives/natasha-kroll Natasha Kroll Archive] - University of Brighton Design Archives</ref> Kroll stayed at Simpsons twelve years, working her way up from the role of display manager to taking full responsibility for all the store's design, publicity and display work. During this time she recruited illustrator [[André François]] and gave [[Terence Conran]] his first display commission.<ref>Artmonsky, Ruth, ''Showing Off: Fifty Years of London Store Publicity and Display'', Artmonsky Arts, 2013, ISBN 978-0957387515</ref>

Kroll was also involved in the design of the interior of the restaurant "Sugar and Spice", which opened in [[Dunstable]] in 1966, and was owned by [[J Lyons & Co]].<ref>[http://archiveshub.ac.uk/data/gb1837-des/nkr Natasha Kroll] on The Archives Hub</ref>

===Exhibition design===
Natasha Kroll was also involved in the display design of the following exhibitions:<ref>Barman, Christian, 'Presentation of RDI Diplomas and the Bicentenary Medal for 1966, Together with an Oration', ''Journal of the Royal Society of Arts'', Vol. 115, No. 5132, July 1967, pp. 604-614</ref>
* Lion and Unicorn Pavilion at [[Festival of Britain|The Festival of Britain]], 1951
* Finmar Exhibition at the Tea Centre, 1960
* Milan Triennale, 1964

===Production design===

In 1956 Kroll joined the production design department of the [[BBC]]. As a member of the design department under Richard Levin,  Kroll devised innovative settings for factual programmes and talks. As with her window displays, ideas originating from European modernism and contemporary design were given popular exposure.<ref>Breakell, Sue and Whitworth, Lesley, [http://jdh.oxfordjournals.org/content/early/2014/03/20/jdh.ept006.full ''Émigré Designers in the University of Brighton Design Archives''], Journal of Design History, 2013</ref> Of particular note is the studio design she devised for [[Huw Weldon]]'s ground-breaking arts programme [[Monitor (British TV series)|''Monitor'']].<ref>[https://www.thestage.co.uk/features/obituaries/2004/natasha-kroll/ Obituary - The Stage], May 10, 2004</ref>

Kroll left the BBC to go freelance in 1966, specialising in period dramas. These included ''Mary Queen of Scots'' (1969) and ''Love's Labour's Lost'' (1975).

Kroll gained several feature film credits as a production designer, including:
*''Macbeth'' (1970)
* ''[[The Music Lovers]]'' (1971)
* ''[[The Hireling]]'' (1973)
* ''Age of Innocence'' (1977)
* ''[[Absolution (1978 film)|Absolution]]'' (1978) (producer and production designer)

==Bibliography==
*''The Princess and the Pea'', Collins, 1944, OCLC: 179198949
*''Window Display'', Studio Publications, 1954, ASIN: B0006D85XA

==Awards==
Natasha Kroll was elected to the Faculty of [[Royal Designers for Industry]] in 1966. This award from the Royal Society of Arts recognized both spheres of Kroll's work.<ref>[https://www.thersa.org/action-and-research/rsa-projects/design/royal-designers-for-industry/past-royal-designers/ RSA] - past Royal designers</ref>

In 1974 Kroll won the [[BAFTA]] for Best Art Direction for ''[[The Hireling]]'' (1973).<ref>[http://awards.bafta.org/award/1974/film/art-direction BAFTA] - Film/ Art Direction, 1974</ref>

==External links and further reading==
*Britton, Piers, D. and Barker, Simon, J., ''Reading Between Designs'', University of Texas Press, 2003, ISBN 978-0292709270
*Ede, Laurie, N., ''British Film Design: A History (Cinema and Society)'', I B Tauris & Co Ltd, 2010, ISBN 978-1848851085
*Suga, Yasuko, [http://www.jstor.org/stable/3838660?seq=1&Search=yes&resultItemClick=true&searchText=natasha&searchText=kroll&searchUri=%2Faction%2FdoBasicSearch%3FQuery%3Dnatasha%2Bkroll%26amp%3Bfilter%3Diid%253A10.2307%252Fi371317#page_scan_tab_contents Modernism, Commercialism and Display Design in Britain: The Reimann School and Studios of Industrial and Commercial Art], Journal of Design History, Vol. 19, No. 2, 2006
*[http://www.imdb.com/name/nm0472066/?ref_=fn_al_nm_1 Natasha Kroll] on the IMDB
*[http://explore.bfi.org.uk/4ce2ba96354be Full filmography] from the BFI
*[https://www.theguardian.com/media/2004/apr/07/broadcasting.guardianobituaries Obituary], The Guardian, 7 April 2004
*[https://nishakeshav.files.wordpress.com/2010/04/natasha-kroll-set-designer.jpg Portrait] by Nisha Keshav

==References==
{{reflist}}

{{Authority control}}
{{DEFAULTSORT:Kroll, Natasha}}
[[Category:People from Moscow]]
[[Category:2004 deaths]]
[[Category:Production designers]]
[[Category:1914 births]]
[[Category:Alumni of Reimann School (Berlin)]]
[[Category:BAFTA winners (people)]]