{{Infobox military person
|name=John Plagis
|image=File:Royal Air Force Fighter Command, 1939-1945. CH7610.jpg
|image_size=260
|alt=A young man in a Royal Air Force uniform; a tag marked "RHODESIA" is prominent on his shoulder
|caption=Plagis in England, c. July 1942
|birth_name=Ioannis Agorastos Plagis
|birth_date= {{birth date|1919|3|10|df=yes}}
|birth_place=[[Gadzema]], [[Company rule in Rhodesia|Southern Rhodesia]]
|death_date= {{death year and age|1974|1919}}
|death_place =
|placeofburial=
|placeofburial_label= Place of burial
|allegiance={{plainlist|
*{{flag|Southern Rhodesia|name=Colony of Southern Rhodesia}}
*{{flag|United Kingdom}}}}
|branch= [[Royal Air Force]]
|serviceyears=1941–1948
|rank=[[Wing commander (rank)|Wing Commander]]
|servicenumber=80227{{sfn|Salt|2001|p=263}}
|unit={{plainlist  |
*[[No. 65 Squadron RAF|No. 65 Squadron]] (1941)
*[[No. 266 Squadron RAF|No. 266 Squadron]] (1941–42)
*[[No. 249 Squadron RAF|No. 249 Squadron]] (1942)
*[[No. 185 Squadron RAF|No. 185 Squadron]] (1942)
}}
|commands={{plainlist |
*[[No. 64 Squadron RAF|No. 64 Squadron]] (1943–44)
*[[No. 126 Squadron RAF|No. 126 Squadron]] (1944–45)
*Bentwaters Wing (1945)
*No. 266 Squadron (1946–47)
}}
|battles=[[Second World War]]
|awards={{plainlist|
*[[Distinguished Service Order]]
*[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] & [[Medal bar|Bar]]
*[[Airman's Cross]] (Netherlands)}}
|relations=
|laterwork=Businessman; electoral candidate for the [[Rhodesian Front]] in [[Southern Rhodesian general election, 1962|1962]]; director of [[Central African Airways]]
}}

'''Ioannis Agorastos''' "'''John'''" '''Plagis''',{{refn|{{lang-el|Ιωάννης Αγοραστός "Γιάννης" Πλαγής}}.|group=n|name=greek}} [[Distinguished Service Order|DSO]], [[Distinguished Flying Cross (United Kingdom)|DFC]] & [[Medal bar|Bar]] (1919–1974) was a [[Southern Rhodesia]]n [[flying ace]] in the [[Royal Air Force]] (RAF) during the Second World War, noted especially for his part in the [[Siege of Malta (World War II)|defence of Malta]] during 1942. The son of [[Greeks in Zimbabwe|Greek immigrants]], he was accepted by recruiters only after Greece joined the [[Allies of World War II|Allies]] in late 1940. Following spells with [[No. 65 Squadron RAF|No. 65 Squadron]] and [[No. 266 Squadron RAF|No. 266 (Rhodesia) Squadron]], he joined [[No. 249 Squadron RAF|No. 249 (Gold Coast) Squadron]] in Malta in March 1942. Flying [[Supermarine Spitfire|Spitfire]] Mk Vs, Plagis was part of the multinational group of Allied pilots that successfully defended the strategically important island against numerically superior [[Axis powers|Axis]] forces over the next few months. Flying with [[No. 185 Squadron RAF|No. 185 Squadron]] from early June, he was withdrawn to England in early July 1942.

After a spell as an instructor in the UK, Plagis returned to action in September 1943 as commander of [[No. 64 Squadron RAF|No. 64 Squadron]], flying Spitfire Mk VCs over northern France. He took command of [[No. 126 Squadron RAF|No. 126 (Persian Gulf) Squadron]] in June 1944, and led many attacks on German positions during the [[invasion of Normandy|invasion of France]] and the [[Allied advance from Paris to the Rhine|campaign]] that followed; he was shot down over [[Arnhem]] during [[Operation Market Garden|Operation ''Market Garden'']], but only lightly wounded. After converting to [[North American P-51 Mustang|Mustang IIIs]], he commanded a [[Wing (military aviation unit)|wing]] based at [[RAF Bentwaters]] that supported bombing missions. He finished the war with the rank of [[squadron leader]] and remained with the RAF afterwards, operating [[Gloster Meteor]]s at the head of No. 266 (Rhodesia) Squadron.

Plagis was the top-scoring Southern Rhodesian ace of the war, and the highest-scoring ace of Greek origin, with 16 confirmed aerial victories, including 11 over Malta. Awarded the [[Distinguished Service Order]] and other medals, he was also one of Rhodesia's most decorated veterans. The Southern Rhodesian capital, [[Harare|Salisbury]], honoured his wartime contributions by naming a street in its northern [[Alexandra Park, Harare|Alexandra Park]] neighbourhood after him. On his return home after retiring from the RAF with the rank of [[Wing commander (rank)|wing commander]] in 1948, he set up home at 1&nbsp;John Plagis Avenue, opened a [[liquor store|bottle store]] bearing his name, and was a director of several companies, including [[Central African Airways]] in the 1960s. He contested the Salisbury City constituency in the 1962 [[Southern Rhodesian general election, 1962|general election]], running for the [[Rhodesian Front]], but failed to win. He died in 1974, reportedly by suicide.

==Early life==
John Plagis was born on 10 March 1919 in [[Gadzema]],{{sfn|Government of Southern Rhodesia|1945|pp=38–42}}{{sfn|Who's Who of Southern Africa|1973|p=1230}} a mining village near [[Chegutu|Hartley]], about {{convert|110|km}} south-west of the [[Southern Rhodesia]]n capital [[Harare|Salisbury]]. His parents, Agorastos and Helen Plagis, were [[Greeks in Zimbabwe|Greek immigrants]] from the island of [[Lemnos]];{{sfn|Charousis|2010|p=115}} he had five siblings.{{sfn|St John|2008|p=145}} Christened with the Greek name Ioannis Agorastos, Plagis used the English form of Ioannis, John, from childhood, and attended [[Prince Edward School]] in Salisbury.{{sfn|Who's Who of Southern Africa|1973|p=1230}}

Having been interested in aviation since he was a boy,{{sfn|Charousis|2010|p=115}} Plagis volunteered for the [[Rhodesian Air Force|Southern Rhodesian Air Force]] (SRAF) soon after the outbreak of war in September 1939. He was turned down because he was the son of foreign nationals and therefore not a citizen, despite having lived in Rhodesia all his life. After Italy [[Greco-Italian War|invaded]] Greece in late October 1940, bringing the Greeks into the war on the [[Allies of World War II|Allied]] side, Plagis applied again—this time to join the [[Royal Air Force]], which had absorbed the SRAF in April 1940—and was accepted.{{sfn|Shores|Williams|1966|p=247}} Training first in Southern Rhodesia, then England, Plagis passed out with the rank of [[flight sergeant]] in June 1941 with above-average ratings in all of his flying assessments.{{sfn|Shores|Williams|1966|p=247}}{{sfn|Nichols|2008|p=92}}

Though he was officially in the RAF as a Greek (he became a Rhodesian citizen only after the war), Plagis considered himself a Rhodesian flyer and wore shoulder flashes on his uniform denoting him as such.{{sfn|Nichols|2008|p=20}} He named each aircraft he piloted during the war after his sister Kay,{{sfn|Government of Southern Rhodesia|1945|pp=38–42}} and painted that name on the side of each cockpit.{{sfn|Price|2004|p=41}} After briefly flying [[Supermarine Spitfire|Spitfires]] with [[No. 65 Squadron RAF]], Plagis joined [[No. 266 Squadron RAF|No. 266 (Rhodesia) Squadron]], an almost all-Rhodesian Spitfire unit, on 19 July 1941.{{sfn|Salt|2001|p=93}} He served in the UK for about half a year, during which he was commissioned as a [[pilot officer]], before being posted to the [[Mediterranean and Middle East theatre of World War II|Mediterranean theatre]] in January 1942.{{sfn|Nichols|2008|p=92}}

==Air war in Europe and the Mediterranean==

===First tour of operations===
[[File:HMS EAGLE and HMS MALAYA in the Mediterranean during Operation 'Spotter', which delivered 16 RAF Spitfire Mk Vs to Malta on 7th March 1942. A7840.jpg|thumb|upright=1.6|[[HMS Eagle (1918)|HMS ''Eagle'']] ''(left)'' and [[HMS Malaya|HMS ''Malaya'']] during Operation ''Spotter'', the first of 13 reinforcements of Malta with [[Supermarine Spitfire|Spitfires]] and pilots, on 7 March 1942. Plagis flew one of the Spitfires from ''Eagle''.|alt=An aircraft carrier loaded with fighter aircraft. Another ship follows.]]

Plagis's first major operation was Operation ''Spotter'', the first of many British [[Malta Convoys|endeavours]] to reinforce the [[Siege of Malta (World War II)|besieged island of Malta]] in the face of German and Italian assaults during the [[Battle of the Mediterranean]]. Malta was considered to be of vital strategic importance, and its defence was looking increasingly precarious in March 1942. ''Spotter'' was a plan to strengthen its British garrison with 16 new Spitfire Mk Vs, which would be carried part of the way from [[Gibraltar]] on the aircraft carrier [[HMS Eagle (1918)|HMS ''Eagle'']], then flown to Malta; the pilots would then become part of the severely depleted [[No. 249 Squadron RAF|No. 249 (Gold Coast) Squadron]]. The team of pilots comprised eight British airmen, four Australians, two New Zealanders and two Southern Rhodesians—Plagis and his close friend Pilot Officer Doug Leggo.{{sfn|Shores|Cull|Malizia|1991|p=109}}

The operation, carried out on 7 March 1942, was largely successful and 15 of the 16 Spitfires reached Malta.{{refn|The 16th was found to be faulty just before take-off, so it was left behind along with its pilot, Australian Flight Sergeant Jack Yarro.{{sfn|Nichols|2008|p=15}}|group=n|name="yarro}} Plagis and Leggo arrived to find a third Rhodesian, Flight Officer George "Buck" Buchanan, already attached to the squadron.{{sfn|Nichols|2008|pp=19–20}} A further delivery of 16 Spitfires, Operation ''Picket I'', was attempted on 21 March, but this was less successful; only nine of the planes arrived.{{sfn|Nichols|2008|p=15}} Thirteen Spitfire reinforcement operations were ultimately launched between March and October 1942, playing a key role in the siege.{{sfn|Nichols|2008|p=50}} The ''[[Luftwaffe]]'' and the Italian ''[[Regia Aeronautica]]'' meanwhile attempted to bomb Malta into submission, turning the airfields into "a wilderness of craters, the docks&nbsp;... a shambles, [[Valletta]] a mass of broken limestone&nbsp;..."{{sfn|O'Hara|2012|p=54}}

The ''Luftwaffe'' launched a major attack against key Maltese airfields at dawn on 20 March. Leggo, who had not slept for over 24&nbsp;hours, returned to the airfield in the early hours having spent the night with a girlfriend. As the German planes approached he was ordered to prepare to fly. Plagis attempted to stop his friend from going, but Leggo insisted on flying, and took off at 08:05 as part of a group of four Spitfires and 12 Hurricanes aiming to intercept a squadron of [[Messerschmitt Bf 109]]s. He was soon seen to be flying poorly. A German pilot noticed this and attacked Leggo from close range, seriously damaging his aircraft and forcing him to bale out. Another Bf 109 then swooped and either fired at Leggo or collapsed his parachute with its [[slipstream]], causing him to fall to his death. When Plagis learned what had happened, he was inconsolable, holding himself responsible.{{sfn|Nichols|2008|pp=19–20}} In his journal, he vowed to "shoot down ten for Doug—I will too, if it takes me a lifetime".{{sfn|Shores|Cull|Malizia|1991|p=130}}

[[File:Spitfire VBs 40 Sqn SAAF over Tunisia 1943.jpg|thumb|left|A Spitfire Mk V, as flown by Plagis over Malta with [[No. 249 Squadron RAF|No. 249 Squadron]]|alt=A Spitfire with SAAF markings, flying against a blue sky]]
Plagis shot down his first enemy aircraft on 25 March 1942, and on 1 April achieved four more aerial victories in a single afternoon, thereby becoming the Siege of Malta's first Spitfire [[flying ace|ace]].{{sfn|Nichols|2008|p=92}} His downing of four enemies in a few hours won him much praise from superiors and reporters, and contributed to his growing reputation as an aggressive but skilful combat pilot.{{sfn|Government of Southern Rhodesia|1945|pp=38–42}} He was awarded the [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] (DFC) on 1 May 1942, the citation noting that he had "destroyed 4 and probably destroyed a further 3 hostile aircraft". "With complete indifference to odds against him, he presses home his attacks with skill and courage," it continued—"He has set an outstanding example."<ref name=lg010542>{{LondonGazette|issue=35542|supp=yes|startpage=1903|endpage=1904|date=1 May 1942|accessdate=13 September 2013}}</ref>

On 11 May, Plagis attempted to down an Italian [[Reggiane Re.2001]] by flying straight at it to ram it; taking erratic evasive manoeuvres, the Italian aircraft [[Stall (flight)|stall]]ed and almost crashed into the sea. Thinking he had downed the enemy, Plagis claimed afterwards to have achieved an aerial victory without firing a shot,{{sfn|Nichols|2008|p=35}} but the Italian flight reported no losses. Plagis's Spitfire was lightly hit during this engagement, and the Rhodesian had some luck returning safely; he landed with only three gallons (14 litres) of fuel left.{{sfn|Shores|Cull|Malizia|1991|p=260}} On 16 May, Plagis and an English ace, Pilot Officer Peter Nash, destroyed a Bf 109 for a shared kill that became No. 249 Squadron's 100th victory over Malta.{{sfn|Nichols|2008|p=90}} Amid the continuing siege, the need for a major supply convoy to Malta was becoming urgent; the Governor [[John Vereker, 6th Viscount Gort|Lord Gort]] warned Britain in early June that if no supplies came by August, he would have to surrender to prevent a famine.{{sfn|O'Hara|2012|p=57}}

Plagis was [[battlefield promotion|promoted in the field]] to [[flight lieutenant]] on 4 June 1942 and transferred to [[No. 185 Squadron RAF|No. 185 Squadron]] to [[Flight commander (position)|command]] "B" Flight. He shot down two Re.2001s two days later to bring his tally of victories to ten (thereby fulfilling his pledge following Leggo's death), and destroyed a Bf 109 on 7 June.{{sfn|Nichols|2008|p=92}} A month later, he received a [[medal bar|Bar]] to his DFC, having been adjudged to have shown "exceptional skill and gallantry in combat&nbsp;... Undeterred by superior numbers of attacking aircraft, he presses home his attacks with great determination."<ref name=lg070742>{{LondonGazette|issue=35621|supp=yes|startpage=2979|endpage=2980|date=7 July 1942|accessdate=13 September 2013}}</ref> Plagis left Malta when his tour expired on 7 July 1942, flying first to Gibraltar, then the UK.{{sfn|Nichols|2008|p=92}} The British finally delivered vital supplies to Malta on 15 August with [[Operation Pedestal|Operation ''Pedestal'']] (known in Malta as the "Santa Marija Convoy").{{sfn|Latimer|2002|p=87}}

[[File:Plagis and Hancock, No. 64 Sqn RAF, at Hornchurch c1943.jpg|thumb|upright=1.2|Plagis ''(left)'' as commander of [[No. 64 Squadron RAF|No. 64 Squadron]], with fellow Malta veteran A&nbsp;J Hancock. [[RAF Hornchurch]], England, c. 1943–44|alt=Two men in air force uniforms stand in front of a fighter aircraft]]
On arriving in England, Plagis was found to be suffering from [[malnutrition]], [[scabies]] and physical and mental fatigue. He briefly convalesced in a nursing home,{{sfn|Nichols|2008|p=52}} then spent a year as an instructor in England.{{sfn|Nichols|2008|p=92}} He was promoted to probationary [[flying officer]] on 1 October 1942.<ref name=lg111242>{{LondonGazette|issue=35819|supp=yes|startpage=5395|endpage=5396|date=11 December 1942|accessdate=14 September 2013}}</ref>

===Second tour of operations===
Plagis returned to action in September 1943, when he was appointed commanding officer of [[No. 64 Squadron RAF|No. 64 Squadron]], then flying Spitfire Mk VCs over northern France from [[RAF Coltishall]] in [[Norfolk]]. Plagis downed a Bf 109 over France on 24 September 1943, then a [[Focke-Wulf Fw 190]] on 23 November,{{sfn|Shores|Williams|1966|p=247}} and formally received the rank of flight lieutenant on 8 December 1943.<ref name=lg101243>{{LondonGazette|issue=36280|supp=yes|startpage=5383|endpage=5384|date=10 December 1943|accessdate=14 September 2013}}</ref>

At the start of June 1944, Plagis assumed command of [[No. 126 Squadron RAF|No. 126 (Persian Gulf) Squadron]], flying Spitfire Mk IXs that had recently been moved from Malta to assist in the [[invasion of Normandy]]. Six of the squadron's planes had been purchased by the Persian Gulf Spitfire Fund, and duly named after the donating sheikdoms; Plagis's aircraft, which he chose because of the large letter "K" on its tail (echoing his sister's name), had "[[Muscat, Oman|Muscat]]" painted in English and Arabic script on its side. He added to this a full rendering of "Kay" and other personal decorations.{{sfn|Price|2004|p=52}}

After leading No. 126 Squadron on raids into Normandy during the Allied invasion, Plagis took part in many of the attacks on German positions in northern France and the [[Low Countries]] that [[Allied advance from Paris to the Rhine|followed]] over the next few months.{{sfn|Nichols|2008|p=92}} He was shot down over [[Arnhem]] in the Netherlands during [[Operation Market Garden|Operation ''Market Garden'']] in September 1944, but suffered only minor injuries and quickly returned to action.{{sfn|St John|2008|p=145}} He received the [[Distinguished Service Order]] on 3 November for his "participat[ion] in very many sorties during which much damage has been inflicted on&nbsp;... [German] shipping, radio stations, oil storage tanks, power plants and other installations".<ref name=lg031144/> The citation particularly stressed an engagement in which a small group of Allied fighters led by Plagis had taken on a far superior force of enemy aircraft and shot down five of them, Plagis himself downing two. Plagis was described as "a brave and resourceful leader whose example has proved a rare source of inspiration".<ref name=lg031144>{{LondonGazette|issue=36777|supp=yes|startpage=5034|endpage=5035|date=3 November 1944|accessdate=13 September 2013}}</ref>

Plagis converted to [[North American P-51 Mustang|Mustang IIIs]] along with the rest of his squadron at [[RAF Bentwaters]] in [[Suffolk]] during December 1944 and January 1945, and spent the rest of the war flying bomber escort missions at the head of Bentwaters Wing, which included No. 126 Squadron.{{sfn|Nichols|2008|p=92}}{{sfn|Shores|Cull|Malizia|1991|p=657}} He was promoted to [[squadron leader]] on 28 March 1945.<ref name=lg110545>{{LondonGazette|issue=37074|supp=yes|startpage=2483|endpage=2484|date=11 May 1945|accessdate=14 September 2013}}</ref> [[German instrument of surrender|Germany surrendered]] on 7 May, [[end of World War II in Europe|ending the war in Europe]].{{sfn|King|1947}}

Plagis finished the war with a tally of 16 enemy aircraft confirmed destroyed (including two shared victories counted as half a kill each), two shared probably destroyed, six damaged and one shared damaged.{{sfn|Price|2004|p=52}} This made him Southern Rhodesia's highest-scoring ace of the war,{{sfn|Shores|Williams|1966|p=73}} as well as the top-scoring ace of Greek origin.{{sfn|Charousis|2010|p=117}} He was one of the most-decorated Southern Rhodesian servicemen of the war.{{sfn|MacDonald|1976|loc=Appendix, p. i}}

==Post-war service and later life==

Plagis stayed with the RAF following the end of hostilities, and from September 1946 to December 1947 commanded No. 266 (Rhodesia) Squadron in England and Germany, flying [[Gloster Meteor]] F.3s.{{sfn|King|1947}}{{sfn|Thomas|1993}} He was awarded the [[Airman's Cross]] by the government of the Netherlands in October 1946.<ref name=lg151046>{{LondonGazette|issue=37758|supp=yes|startpage=5079|endpage=5080|date=15 October 1946|accessdate=14 September 2013}}</ref> After retiring from the military with the rank of [[Wing commander (rank)|wing commander]], Plagis returned home to Southern Rhodesia in 1948.{{sfn|St John|2008|p=145}} A street in the north Salisbury suburb of [[Alexandra Park, Harare|Alexandra Park]] had been named after him in recognition of his wartime exploits; he moved into the house at the end of the road, 1&nbsp;John Plagis Avenue.{{sfn|Parker|1972|p=104}} He married in 1954 and had three sons and a daughter.{{sfn|Who's Who of Southern Africa|1973|p=1230}}

Plagis set up and ran a [[liquor store|bottle store]] bearing his name in Salisbury,{{sfn|Parker|1972|p=104}} and was involved in several businesses during the next three decades, serving as a director on company boards, including [[Central African Airways]] from 1963 to 1968.{{sfn|Who's Who of Southern Africa|1973|p=1230}} He joined the [[Rhodesian Front]] on its formation in 1962, and was its candidate in Salisbury City in that year's [[Southern Rhodesian general election, 1962|general election]], losing to the [[United Federal Party]]'s John Roger Nicholson by 631 votes to 501.{{sfn|Willson|1963|p=202}} According to a report published by the [[Zimbabwe African National Union]] in 1969, Plagis was by then working in the office of the Rhodesian Prime Minister [[Ian Smith]] ([[military service of Ian Smith|himself]] a Second World War Spitfire pilot), with responsibility for the premier's written correspondence.{{sfn|ZANU|1969|p=29}}

In later life, Plagis became a friend of British ace [[Douglas Bader]], a prominent supporter of Rhodesia's  [[Rhodesia's Unilateral Declaration of Independence|Unilateral Declaration of Independence]] in 1965. Bader, Smith and Plagis often socialised.{{sfn|Francis|2008|p=193}}{{sfn|Lucas|1981|p=262}} Plagis also knew [[L. Ron Hubbard|L&nbsp;Ron Hubbard]], the American founder of [[Scientology]], who briefly relocated to Salisbury in 1966. Hubbard initiated numerous business schemes in Rhodesia, including the purchase of the [[Bumi Hills Hotel]] at [[Kariba, Zimbabwe|Kariba]]. Plagis was one of two local businessmen who partnered with Hubbard in the Bumi Hills deal.{{sfn|Sunday Mail|1966}} He also sold Hubbard an interest in his holdings before the American was deported.{{sfn|Bulawayo Chronicle|1966}}

Plagis died in 1974, aged 54 or 55;{{sfn|Charousis|2010|p=118}} according to Lauren St John, an author from Gadzema, he had committed suicide, having never truly readjusted to civilian life.{{sfn|St John|2008|p=145}}

==Notes and references==
===Footnotes===
{{reflist|group=n}}

===References===
{{reflist|colwidth=30em}}

===Newspaper and journal articles===
{{refbegin}}
*{{cite journal
|last=Charousis |first=Chariton
|script-title=el:Έλληνες Άσσοι σε Συμμαχικές Αεροπορίες κατά το Β΄ Παγκόσμιο Πόλεμο|trans_title=Greek Aces in Allied Air Forces in World War II
|url=http://www.haf.gr/el/articles/pdf/ae_90.pdf
|archiveurl=https://web.archive.org/web/20150605092555/http://www.haf.gr/el/articles/pdf/ae_90.pdf
|archivedate=5 June 2015
|journal=Aviation Review
|location=Athens
|publisher=[[Hellenic Air Force]]
|date=December 2010
|pages=pp.&nbsp;114–127
|accessdate=14 September 2013
|ref=harv|language=Greek
}}
*{{cite journal
|last=King|first=H F
|title=Five Days With B.A.F.O.
|url=http://www.flightglobal.com/pdfarchive/view/1947/1947%20-%200891.html
|journal=[[Flight International|Flight]]
|location=London
|date=5 June 1947
|pages=pp.&nbsp;513–517
|accessdate=14 September 2013
|ref=harv}}
*{{cite journal
|last=Thomas|first=Andrew
|title=No. 266 (Rhodesia) Squadron, Royal Air Force
|journal=Scale Aircraft Modelling
|publisher=Hall Park
|location=Bletchley
|date=1 November 1993
|volume=16
|number=1
|pages=pp.&nbsp;35–41
|ref=harv}}
*{{cite journal
|title=U.S. Financier is Refused Residence Permit—Told to Leave by Next Monday
|url=http://www.xenu-directory.net/news/images/thecompiler-1966-9.pdf
|journal=The Chronicle
|location=Bulawayo
|publisher=Argus Group
|page=p.&nbsp;1
|date=14 July 1966
|accessdate=10 August 2014
|format=pdf
|ref={{harvid|Bulawayo Chronicle|1966}}}}
*{{cite journal
|title=Millionaire in Bumi Hills Hotel Deal
|url=http://www.xenu-directory.net/news/images/thecompiler-1966-8.pdf
|journal=The Sunday Mail
|location=Salisbury
|publisher=Argus Group
|date=22 May 1966
|accessdate=10 August 2014
|format=pdf
|ref={{harvid|Sunday Mail|1966}}}}
{{refend}}

===Bibliography===
{{refbegin}}
*{{cite book
|last=Francis |first=Martin |ref=harv
|title=The Flyer: British Culture and the Royal Air Force 1939–1945
|date=November 2008 |edition=First
|location=Oxford |publisher=[[Oxford University Press]] |isbn=978-0-19-927748-3}}
*{{cite book
|last=Latimer |first=Jon |ref=harv |authorlink=Jon Latimer
|title=Alamein
|date=October 2002 |edition=First
|location=Cambridge, Massachusetts |publisher=[[Harvard University Press]] |isbn=978-0-674-01016-1}}
*{{cite book
|last=Lucas |first=Laddie |ref=harv |authorlink=Percy Lucas
|title=Flying Colours: The Epic Story of Douglas Bader
|date=October 1981 |edition=First
|location=London |publisher=[[Hutchinson (publisher)|Hutchinson]] |isbn=978-0-09-146470-7}}
*{{cite book
|last=MacDonald |first=J F |ref=harv
|title=The War History of Southern Rhodesia 1939–1945. ''Volume 2''
|year=1976 |origyear=1950
|location=Bulawayo |publisher=Books of Rhodesia |isbn=978-0-86920-140-4}}
*{{cite book
|last=Nichols |first=Steve |ref=harv
|title=Malta Spitfire Aces |series=Osprey Aircraft of the Aces |volume=83
|date=September 2008 |edition=First
|location=Oxford |publisher=[[Osprey Publishing]] |isbn=978-1-84603-305-6}}
*{{cite book
|last=O'Hara |first=Vincent P |ref=harv
|title=In Passage Perilous: Malta and the Convoy Battles of June 1942
|series=Twentieth-Century Battles
|date=November 2012 |edition=First
|location=Bloomington, Indiana |publisher=[[Indiana University Press]] |isbn=978-0-253-00603-5}}
*{{cite book
|last=Parker |first=John |ref=harv
|title=Rhodesia: Little White Island
|year=1972
|location=London |publisher=Pitman |isbn=978-0-273-36167-1}}
*{{cite book
|last=Price |first=Alfred |ref=harv
|title=Late Mark Spitfire Aces, 1942–45 |series=Osprey Aircraft of the Aces |volume=5
|year=2004 |origyear=1995 |edition=Fifth
|location=Oxford |publisher=Osprey Publishing |isbn=978-1-85532-575-3}}
*{{cite book
|last=Salt |first=Beryl|ref=harv
|title=A Pride of Eagles: The Definitive History of the Rhodesian Air Force, 1920–1980
|date=March 2001
|location=Weltevredenpark, South Africa |publisher=Covos Day Books |isbn=978-0-620-23759-8}}
*{{cite book
|last1=Shores |first1=Christopher F |last2=Cull |first2=Brian |last3=Malizia |first3=Nicola |ref=harv
|title=Malta: the Spitfire Year, 1942
|year=1991
|location=London |publisher=Grub Street Publishing |isbn=978-0-948817-16-8}}
*{{cite book
|last1=Shores |first1=Christopher F |last2=Williams |first2=Clive |ref=harv
|title=Aces High: the Fighter Aces of the British and Commonwealth Air Forces in World War II
|year=1966
|location=London |publisher=Neville Spearman Publishing |oclc=654945475}}
*{{cite book
|last=St John |first=Lauren |ref=harv
|title=Rainbow's End: A Memoir of Childhood, War and an African Farm
|date=July 2008 |origyear=2007 |edition=Second
|location=New York |publisher=[[Charles Scribner's Sons|Scribner]] |isbn=978-0-7432-8680-0}}
*{{cite book
|editor-last=Willson |editor-first=F M G |ref=harv
|title=Source Book of Parliamentary Elections and Referenda in Southern Rhodesia, 1898–1962
|year=1963
|location=Salisbury |publisher=Department of Government, [[University of Zimbabwe|University College of Rhodesia and Nyasaland]] |oclc=2885579}}
*{{cite book
|ref={{harvid|Government of Southern Rhodesia|1945}}
|title=Southern Rhodesia: Past and Present
|year=1945
|location=Salisbury |publisher=Government of Southern Rhodesia |oclc=5062140}}
*{{cite book
|ref={{harvid|Who's Who of Southern Africa|1973}}
|title=Who's Who of Southern Africa
|year=1973
|location=Cape Town |publisher=Ken Donaldson |oclc=1769850}}
*{{cite book
|ref={{harvid|ZANU|1969}}
|title=Zimbabwe News. ''Volume 4, Issues 1–16''
|year=1969
|location=Lusaka |publisher=[[Zimbabwe African National Union]] |oclc=3249703}}
{{refend}}

{{Use South African English|date=September 2013}}
{{Use dmy dates|date=September 2013}}

{{featured article}}

{{DEFAULTSORT:Plagis, John}}
[[Category:1919 births]]
[[Category:1974 deaths]]
[[Category:Date of death missing]]
[[Category:Place of death missing]]
[[Category:Alumni of Prince Edward School]]
[[Category:Companions of the Distinguished Service Order]]
[[Category:Greek World War II flying aces]]
[[Category:People from Mashonaland West Province]]
[[Category:Recipients of the Airman's Cross]]
[[Category:Recipients of the Distinguished Flying Cross and Bar (United Kingdom)]]
[[Category:Rhodesian businesspeople]]
[[Category:Rhodesian Front politicians]]
[[Category:Royal Air Force officers]]
[[Category:Royal Air Force personnel of World War II]]
[[Category:Shot-down aviators]]
[[Category:Southern Rhodesian World War II flying aces]]
[[Category:Suicides in Rhodesia]]
[[Category:White Rhodesian people]]
[[Category:Zimbabwean people of Greek descent]]