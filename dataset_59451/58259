{{Orphan|date=March 2017}}
{{Infobox journal
| title = Journal of Literacy Research
| cover = Journal_of_Literacy_Research.gif
| editors = Misty Sailors
| former_names = Journal of Reading Behavior 
| discipline = [[Literacy]], [[education]]
| abbreviation = J. Lit. Res.
| publisher = [[Sage Publications]] on behalf of the [[Literacy Research Association]]
| country = 
| frequency = Quarterly
| history = 1969-present
| openaccess = 
| license = 
| impact = 0.727
| impact-year = 2015
| website = http://journals.sagepub.com/home/jlr
| link1 = http://jlr.sagepub.com/content/current
| link1-name = Online access
| link2 = http://jlr.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 1086-296X
| eISSN = 1554-8430
| OCLC = 44534766
| LCCN = 96660095
}}
The '''''Journal of Literacy Research''''' a quarterly [[peer-reviewed]] [[academic journal]] covering research related to [[literacy]], [[language]], and literacy and language [[education]] from preschool through adulthood. It was established in 1969 and is published by [[Sage Publications]] on behalf of the [[Literacy Research Association]]. The [[editor-in-chief]] is Misty Sailors ([[University of Texas at San Antonio]]).

==Abstracting and indexing==
The journal is abstracted and indexed in [[Current Contents]]/Social & Behavioral Sciences,<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Clarivate Analytics]] |work=Intellectual Property & Science |accessdate=2017-03-03}}</ref> [[Scopus]],<ref name=Scopus>{{cite web |url=https://www.scopus.com/sourceid/4700151738 |title=Source details: Journal of Literacy Research |publisher=[[Elsevier]] |work=Scopus preview |accessdate=2017-03-03}}</ref> and the [[Social Sciences Citation Index]].<ref name=ISI/> According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 0.727.<ref name=WoS>{{cite book |year=2016 |chapter=Journal of Literacy Research |title=2015 [[Journal Citation Reports]] |publisher=[[Clarivate Analytics]] |edition=Science |series=[[Web of Science]]}}</ref> 

==References==
{{reflist}}

==External links==
*{{Official website|http://journals.sagepub.com/home/jlr}}
*[http://www.literacyresearchassociation.org/ Literacy Research Association]

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Education journals]]
[[Category:Publications established in 1969]]