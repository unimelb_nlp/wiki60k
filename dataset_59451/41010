{{Infobox company|
 name = The Foundry Visionmongers Ltd |
 logo = [[Image:The Foundry logo black.svg|200px]] |
 type = [[Private company|Private]] |
 homepage = [http://www.foundry.com foundry.com]|
 foundation = 1996 |
 founder = Bruno Nicoletti |
 key_people = Alex Mahon, CEO<br>Bill Collis, President<br>Simon Robinson, Chief Scientist<br>Bruno Nicoletti, Founder |
 location = [[London]] |
 area_served = [[World]]wide|
 industry = [[Software]] |
 products = Visual Effects and 3D Software|
}}

'''The Foundry Visionmongers''' (known as Foundry) is a [[visual effects]] software development company with headquarters in [[London]], and offices in [[Manchester]], [[Los Angeles]], [[Austin, Texas]] and [[Redwood City, California]].

== History ==

The Foundry was founded in 1996, by Bruno Nicoletti, with Simon Robinson
joining soon afterwards.<ref>{{cite web|url=http://www.thefoundry.co.uk/meet-the-team/|publisher=The Foundry Visionmongers|title=Meet the Team|accessdate=2012-09-12}}</ref> Alex Mahon was named CEO in November 2015. She supersedes Bill Collis, who will remain president and board member.<ref>{{Cite web|title = VFX Software Developer The Foundry Finds New CEO|url = http://www.hollywoodreporter.com/behind-screen/vfx-software-developer-foundry-finds-838602|website = The Hollywood Reporter|accessdate = 2015-11-10}}</ref>

The Foundry was bought by the owners of [[Digital Domain]],
Wyndcrest Holdings, in March 2007, and took over DD's existing
[[Nuke (software)|NUKE]] business.  Subsequently it was subject to a
[[management buyout]] with backing from Advent Venture Partners,
and then acquired by the [[Carlyle Group]] in April 2011.<ref>{{cite web|url=http://www.fxguide.com/featured/D2_Softwares_Nuke_Acquired_by_The_Foundry/|publisher=fxguide|title=D2 Software's Nuke Acquired by The Foundry|date=March 22, 2007|first=John|last=Montgomery|accessdate=September 12, 2012}}</ref><ref>{{cite web|url=http://www.ft.com/cms/s/0/94359572-4fcc-11de-a692-00144feabdc0.html|title=The Foundry returns to former management|date=June 3, 2009|first=Maija|last=Palmer|accessdate=October 3, 2012}}</ref><ref>{{cite news|url=http://www.ft.com/cms/s/0/464bbd2e-4e39-11e0-a9fa-00144feab49a.html|first=Mary|last=Watkins|date=March 15, 2011|accessdate=October 3, 2012|publisher=Financial Times|title=Carlyle buys The Foundry}}</ref>

In September 2012, The Foundry merged with [[Luxology]], a [[Mountain View, California|Mountain View]]-based software house
known primarily for [[modo (software)|modo]], a 3D modelling and animation package.<ref>{{cite web|url=http://www.variety.com/article/VR1118059758?refcatid=1009&printerfriendly=true|accessdate=October 3, 2012|date=September 25, 2012|publisher=Variety|first=David S.|last=Cohen|title=Fx companies the Foundry and Luxology to merge}}</ref>  Earlier the same month, it ranked at number 70 in [[the Sunday Times]] [[Tech Track 100]], with 2011/2012 sales of approximately £15 million, a 49% increase from 2010/2011.<ref>{{cite web|url=http://www.fasttrack.co.uk/fasttrack/leagues/tech100leaguetable.asp?siteID=3&searchName=&yr=2012&sort=num&area1=99|title=2012 Sunday Times Hiscox Tech Track 100 league table|date=September 16, 2012|accessdate=October 3, 2012|publisher=Sunday Times}}</ref>

In 2015 reports were that [[Adobe Systems]] was preparing to buy them from [[The Carlyle Group]] who were the owners at the time.<ref>http://www.telegraph.co.uk/finance/newsbysector/banksandfinance/privateequity/11562472/Adobe-eyes-200m-bid-for-British-visual-effects-firm-The-Foundry.html</ref>

In February 2017, the company rebranded as Foundry, dropping the "The".

== Products ==

The Foundry had its origins in [[plug-in (computing)|plug-in]] development, and its first product was the
Tinder (and later Tinderbox) plugins.  This business was sold to [[GenArts]] in 2010.<ref>{{cite web|url=http://www.fxguide.com/featured/genarts_buys_tinder_plugins_from_the_foundry/|first=Mike|last=Seymour|title=GenArts Buys Tinder plugins from The Foundry|date=February 10, 2010|accessdate=October 3, 2012|publisher=fxguide.com}}</ref> It continues to sell the Furnace motion-estimation based plugins, which won an Academy Scientific and Technical Award in 2006<ref>{{cite web|archiveurl=https://web.archive.org/web/20081006142804/http://www3.oscars.org/scitech/2006/winners.html|title=2006 Scientific & Technical Award Winners|publisher=Academy of Motion Picture Arts & Sciences|archivedate=October 6, 2008|url=http://www3.oscars.org/scitech/2006/winners.html}}</ref> Other plugins include Ocula, a set of tools for [[stereoscopy|stereoscopic]] post-processing; Keylight, a [[chroma key|keyer]]; RollingShutter, which reduces [[CMOS]] artefact distortion; CameraTracker; and Kronos.<ref>{{cite web|url=http://www.thefoundry.co.uk/products/all/|title=All Products for Sale|accessdate=October 3, 2012|publisher=The Foundry}}</ref>

The Foundry continues the development of [[Nuke (software)|Nuke]], a node-based compositor.  Version 10.0v2 was released in June 2016<ref>{{cite web|url=https://s3.amazonaws.com/thefoundry/products/nuke/releases/10.0v2/Nuke10.0v2_ReleaseNotes.pdf|title=Nuke release notes|date=June 8, 2016|accessdate=June 27, 2016}}</ref>

[[Mari (software)|MARI]], a texture painting application was released in July 2010.  This has been originally developed in-house at [[Weta Digital]] for use on [[Avatar (2009 film)|Avatar]].<ref>{{cite web|url=http://www.cgsociety.org/index.php/CGSFeatures/CGSFeatureSpecial/mari|title=Jack Greasley, developer and Product Manager at The Foundry, takes CGSociety for a look around Mari, the new texture application|publisher=CGSociety|first=Paul|last=Hellard|date=May 25, 2010|accessdate=October 3, 2012}}</ref><ref>{{cite web|url=http://www.thefoundry.co.uk/articles/2010/07/16/72/mari-10v1-released/|title=MARI 1.0v1 Released|date=July 16, 2010|accessdate=October 3, 2012|publisher=The Foundry}}</ref>
[[Katana (software)|Katana]], a tool for look development and lighting, originally from [[Sony Pictures Imageworks]], was released in 2011.<ref>{{cite web|url=http://www.3dworldmag.com/2011/10/27/the-foundry-releases-katana-1-0/|date=October 27, 2011|first=Kerrie|last=Hughes|accessdate=October 3, 2012|title=The Foundry releases Katana 1.0|publisher=3D World}}</ref> HIERO, a brand new shot management, conform and review tool was released in March 2012.  The software was designed in-house from the ground up by the Foundry.<ref>{{cite web|url=http://www.fxguide.com/quicktakes/hiero-ships/|title=HIERO Ships|publisher=fxguide.com|first=John|last=Montgomery|date=March 8, 2012|accessdate=October 3, 2012}}</ref>

== References ==
{{reflist|2}}

== External links==
*[http://www.foundry.com foundry.com]

{{DEFAULTSORT:Foundry Visionmongers}}
[[Category:Companies established in 1996]]
[[Category:Visual effects companies]]
[[Category:Software companies based in London]]