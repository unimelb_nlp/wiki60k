{{Primary sources|date=March 2017}}
{{Infobox school
| name = Golden Grove High School
| image = 
| motto = ''iCARE''
| established = 1989
| type = [[State school|Public high school]]
| streetaddress = 1 Adey Place, Golden Grove
| zipcode = 5125
| grades = 8–12
| principal = Peter Kuss
| head_name2 = Exam board
| head2 = [[SSABSA]]
| Sporting areas = VISTA and SSASA
| city = [[Golden Grove, South Australia|Golden Grove]]
| state = [[South Australia]]
| country = [[Australia]]
| students = 1671
| school_colors = Green and Gold
| houses = Tilly, Milne, Stevens and Robertson
| website = [http://www.goldengrovehs.sa.edu.au www.goldengrovehs.sa.edu.au]
}}

'''Golden Grove High school''' is a public [[secondary school]] (8–12) located with [[Gleeson College]] and [[Pedare Christian College]] private schools in [[Golden Grove, South Australia|Golden Grove]], [[South Australia]]. The three schools share a common campus. The school was built as part of the [[Lend Lease Corporation|Delfin]] Golden Grove development as part of a planned community. The school opened in 1989. A [[special education]] unit is included for students with intellectual and physical [[disability]]. It is also a special interest [[dance]]/[[drama]] school.

==Student leadership==
The school since its founding, has incorporated the leadership of students into its development.

==Sporting events==
Golden Grove High School holds an annual sports day on Gleeson Oval, where all students are encouraged to attend and get involved in numerous sporting events.

Golden Grove is a sporting school that actively participates in the north eastern schools competition, Vista. Vista includes a variety of sports including but not limited to soccer, football, cricket, hockey, basketball, tennis, volleyball, badminton, touch football, swimming, athletics and table tennis.

==Facilities==

The three schools have a shared facilities area where mainly senior subjects are located. The shared facilities centre includes numerous computer rooms for computer science classes, chemistry, physics, psychology and biology labs, woodwork and metalwork rooms, senior art studios, a music suite and senior home economics building.

The South Australian Government built an adjoining Community Recreation Centre at a cost of approximately 9 million dollars,{{Citation needed|date=August 2010}} and was completed in 1993. This centre included two individual stadiums (one with retractable grandstand seating), a gymnasium and a 500-seat performance theatre. Special flooring in the stadiums was made from a special rubber compound, designed to prevent sporting injuries. All three schools and the public share the Recreation and Arts Centre.

The local hockey field is also shared by the three schools.

The Dame Roma Mitchell Centre was recently{{When|date=August 2010}} built near the Administration Block of the School. This is a specialist arts centre with facilities for drama and dance. This has an upper level, which has a large lecture theatre used for assemblies and lessons for various subjects. This facility is also shared by the three schools.

==Libraries==

Golden Grove Library is located near the centre of the school. Senior students use Thiele Library (named after [[Colin Thiele]]),{{Citation needed|date=August 2010}} which is located in shared facilities.

==Notable alumni==
2011 Winner of Australia's [[The X Factor (Australia)|X-Factor]] and Sony Music contracted, [[Reece Mastin]] was enrolled at Golden Grove High School. Mastin's first single release 'Good Night' took a matter of hours to become number 1 song downloaded on iTunes.

Thomas Strain, footballer for [[Hednesford Town F.C.]] playing in the [[Conference North]], on loan from [[Aston Villa F.C.]]

==References==

{{Reflist}}

==External links==
*[http://www.goldengrovehs.sa.edu.au/ Golden Grove High School]

{{coord|34|47|37|S|138|41|46|E|display=title|region:AU_type:edu_source:GNS-enwiki}}

[[Category:High schools in South Australia]]
[[Category:Public schools in South Australia]]
[[Category:Schools of the performing arts in Australia]]
[[Category:Dance education in Australia]]
[[Category:Rock Eisteddfod Challenge participants]]
[[Category:Special interest high schools in South Australia]]