{{for|the philosophy journals|Inquiry (disambiguation)}}
{{Infobox journal
| title = Inquiry
| cover =
| editor = Kevin P. Kane
| discipline = [[Health policy]]
| formernames =
| abbreviation = Inquiry
| publisher = Excellus Health Plan
| country = United States
| frequency = Quarterly
| history = 1964–present
| openaccess = [[Hybrid open access journal|Hybrid]]
| license =
| impact = 0.842
| impact-year = 2012
| website = http://www.inquiryjournal.org
| link1 = http://www.inquiryjournalonline.org/inqronline/?request=get-current-issue
| link1-name = Online access
| link2 = http://www.inquiryjournalonline.org/loi/inqr
| link2-name = Online archive
| JSTOR =
| OCLC = 02057017
| LCCN = sf82008019
| CODEN = 
| ISSN = 0046-9580
| eISSN =
}}
'''''Inquiry, The Journal of Health Care Organization Provision and Financing''''' is a quarterly [[Peer review|peer-reviewed]] [[healthcare journal]] covering [[public policy]] issues, innovative concepts, and original research in healthcare provision.

== Abstracting and indexing ==
The journal is abstracted and indexed in [[PubMed]]/[[MEDLINE]],<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/171671 |title=Inquiry |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |format= |accessdate=2013-11-16}}</ref> [[Science Citation Index]], [[Social Sciences Citation Index]], [[Current Contents]]/Social & Behavioral Sciences, and Current Contents/Clinical Medicine.<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2013-11-16}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 0.842.<ref name=WoS>{{cite book |year=2013 |chapter=Inquiry |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.inquiryjournal.org}}

{{DEFAULTSORT:Inquiry (Health Journal)}}
[[Category:Public health journals]]
[[Category:Hybrid open access journals]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1964]]

{{med-journal-stub}}