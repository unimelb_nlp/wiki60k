{{Use dmy dates|date=March 2013}}
{{Use British English|date=March 2013}}
{{featured article}}
{{Infobox film
| name           = Gagak Item
| image          = Gagak Item ad 2.jpg{{!}}300px
| image_size     = 275
| border         = 
| alt            = A black-and-white advertisement; at the left side is a small picture
| caption        = Newspaper advertisement
| director       = {{plain list|
*[[Wong brothers|Joshua Wong]]
*[[Wong brothers|Othniel Wong]]
}}
| producer       =Tan Khoen Yauw
| screenplay     = [[Saeroen]]
| narrator       = 
| starring       ={{plain list|
*[[Rd Mochtar]]
*[[Roekiah]]
*Eddy T. Effendi
}}
| music          = H. Dumas
| cinematography = Wong brothers
| editing        = 
| studio         = [[Tan's Film]]
| distributor    = 
| released       = {{Film date|df=yes|1939|||Dutch East Indies}}
| runtime        = 
| country        = [[Dutch East Indies]]
| language       = [[Malay trade and creole languages|Vernacular Malay]]
| budget         = 
| gross          = 
}}
'''''Gagak Item''''' ({{IPA-id|ɡaˈɡaʔ iˈtəm|}}; [[Malay trade and creole languages|Vernacular Malay]] for ''Black Raven'', also known by the Dutch title '''''De Zwarte Raaf''''') is a 1939 bandit film from the [[Dutch East Indies]] (now Indonesia) directed by [[Wong brothers|Joshua and Othniel Wong]] for [[Tan's Film]]. Starring [[Rd Mochtar]], [[Roekiah]], and Eddy T. Effendi, it follows a masked man known only as "Gagak Item" ("Black Raven"). The [[black-and-white]] film, which featured the cast and crew from the 1937 hit ''[[Terang Boelan]]'' (''Full Moon''), was a commercial success and received positive reviews upon release. It is likely [[lost film|lost]].

==Production==
''Gagak Item'' was directed by brothers [[Wong brothers|Joshua and Othniel Wong]]; filming the work in [[black-and-white]], they also handled [[Sound editor (filmmaking)|sound editing]]. It was produced by Tan Khoen Yauw of [[Tan's Film]] and starred [[Rd Mochtar]], [[Roekiah]], Eddy T. Effendi, and [[Kartolo]].<ref>{{harvnb|Filmindonesia.or.id, Gagak Item}}; {{harvnb|Bataviaasch Nieuwsblad 1939, Gagak Item}}</ref> The Wongs and cast had first worked together on [[Albert Balink]]'s 1937 blockbuster ''[[Terang Boelan]]'' (''Full Moon''), before joining Tan's Film in 1938 for the highly successful ''[[Fatima (1938 film)|Fatima]]''; ''Gagak Item'' was their second production with the company, which hoped to mirror ''Terang Boelan''{{'s}} success. Through these prior films Mochtar and Roekiah had become an established [[celebrity couple|screen couple]].{{sfn|Biran|2009|pp=175–176}}

[[Saeroen]], a journalist-turned-screenwriter for ''Terang Boelan'' and ''Fatima'', returned to write the script to ''Gagak Item''. The film, a love story, followed a girl and a masked man known as "Gagak Item" ("Black Raven"){{sfn|Biran|2009|pp=175–176}} and was set in rural Buitenzorg (now [[Bogor]]).{{efn|{{harvtxt|Biran|2009|pp=175–176}} only provides a single sentence describing ''Gagak Item''{{'s}} plot. The film's entry in the Indonesian Film Database {{harv|Filmindonesia.or.id, Gagak Item}} does not include a plot summary and available contemporary reviews do not give any further details. The novelisation is not held in any of the libraries searchable through [[WorldCat]].}}{{sfn|Bataviaasch Nieuwsblad 1939, Gagak Item}} The titular bandit was similar to [[Zorro]], a character popular in the Indies at the time; such figures had been a staple of travelling theatre troupes beginning in the early 1930s. When writing the script Saeroen continued the formula he had used in ''Terang Boelan'', including action, music, beautiful vistas and [[physical comedy]].{{sfn|Biran|2009|pp=175–176}} The film had six songs performed by Hugo Dumas' musical troupe [[Lief Java]];{{sfn|Bataviaasch Nieuwsblad 1939, Gagak Item}} the troupe was known for its ''[[keroncong]]'' performances, mixing traditional music with [[Portuguese colonialism in Indonesia|Portuguese]] influences.{{sfn|Het Nieuws 1939, (untitled)}} ''Gagak Item'' featured vocals by ''kroncong'' singer [[Annie Landouw]].{{sfn|Bataviaasch Nieuwsblad 1939, Gagak Item}}

==Release and reception==
[[File:Mochtar and Roekiah, at Sampoerna Theatre.jpg|thumb|alt=A man and woman standing close together; both are in their mid-20s.|[[Rd Mochtar]] and [[Roekiah]] in a promotional still]]
''Gagak Item'' was released in late 1939 and was screened in Batavia (now [[Jakarta]]), the capital of the Indies; [[Medan]], [[North Sumatra]]; and [[Surabaya]], [[East Java|eastern Java]].<ref>{{harvnb|Bataviaasch Nieuwsblad 1939, (untitled)}}; {{harvnb|De Sumatra Post 1939, (untitled)}}; {{harvnb|De Indische Courant 1939, Filmniews}}</ref> Some screenings of the film, also advertised under the Dutch title ''De Zwarte Raaf'', had Dutch-language subtitles.{{efn|At the time the Indies was a colony of the Netherlands. Dutch audiences would sometimes watch local films.}}{{sfn|De Indische Courant 1939, Filmniews}} A novelisation of the film, published by the [[Yogyakarta]]-based Kolff-Buning, soon followed.{{sfn|Biran|2009|p=212}} ''Gagak Item'' was one of four [[List of films of the Dutch East Indies|domestic productions]] released in 1939; the film industry had undergone a significant downturn following the onset of the [[Great Depression]], during which time cinemas mainly showed [[Hollywood]] productions, and had only begun recovering following the release of ''Terang Boelan''.{{sfn|Biran|2009|pp=145, 383}}

''Gagak Item'' was a commercial and critical success, although not as much as Tan's earlier production.{{sfn|Biran|2009|p=212}} An anonymous review in ''[[Bataviaasch Nieuwsblad]]'' praised the film, especially its music. The reviewer opined that the film would be a great success and that the film industry in the Indies was showing promising developments.{{sfn|Bataviaasch Nieuwsblad 1939, Gagak Item}} Another review in the same paper found that, although "one may shake one's head over the cultural value of indigenous films",{{efn|Original: "''Al mag men dan het hoofd schudden over de cultureele waarde van de Inheemsche film op het huidige tijdstip,&nbsp;...''"}} the film was a step forward for the industry. The review praised Roekiah's "demure"{{efn|Original: "''ingetogen''"}} acting.{{sfn|Bataviaasch Nieuwsblad 1939, Filmaankondiging}}

Following the success of ''Gagak Item'' the Wongs, Saeroen, Roekiah, and Mochtar continued to work with Tan's Film. Their next production, ''[[Siti Akbari]]'' (1940), was similarly successful, although again not as profitable as ''Terang Boelan'' or ''Fatima''.{{sfn|Biran|2009|p=212}} Saeroen, Joshua Wong, and Mochtar left the company in 1940: Wong and Mochtar after payment disputes, and Saeroen to return to journalism.{{sfn|Biran|2009|pp=212, 224, 232}} Through 1941 Tan's Film produced fewer movies than its competitors, and was ultimately shut down following the [[Japanese occupation of Indonesia|Japanese occupation]] in early 1942.{{sfn|Biran|2009|p=214}}

''Gagak Item'' was screened as late as January 1951.{{sfn|De Nieuwsgier 1951, Agenda}} The film is likely [[lost film|lost]]. Movies in the Indies were recorded on highly flammable [[nitrate film]], and after a fire destroyed much of Produksi Film Negara's warehouse in 1952, old films shot on nitrate were deliberately destroyed.{{sfn|Biran|2012|p=291}} The American visual anthropologist [[Karl G. Heider]] writes that all Indonesian films from before 1950 are lost.{{sfn|Heider|1991|p=14}} However, JB Kristanto's ''Katalog Film Indonesia'' (''Indonesian Film Catalogue'') records several as having survived at [[Sinematek Indonesia]]'s archives, and film historian [[Misbach Yusa Biran]] writes that several Japanese propaganda films have survived at the [[Netherlands Government Information Service]].{{sfn|Biran|2009|p=351}}

==Explanatory notes==
{{notelist}}

==References==
{{reflist|30em}}

==Works cited==
{{refbegin|40em}}
*{{cite news
 |title=Agenda: Vrijdag 26 Januari, Bogor
 |trans_title=Agenda: Friday 26 January, Bogor
 |language=Dutch
 |url=http://kranten.delpher.nl/nl/view/index/query/%22Gagak%20Item%22/coll/ddd/image/ddd:010474895:mpeg21:a0073/page/1/maxperpage/10/facets//facets%5Btype%5D/artikel/sortfield/datedesc#info
 |work=De Nieuwsgier
 |location=Jakarta
 |page=6
 |date=26 January 1950
 |accessdate=5 December 2013
 |ref={{sfnRef|De Nieuwsgier 1951, Agenda}}
}}
* {{cite book
  | title = [[Sejarah Film 1900–1950: Bikin Film di Jawa]]
  | trans_title = History of Film 1900–1950: Making Films in Java
  | language = Indonesian
  | last = Biran
  | first = Misbach Yusa
  | authorlink = Misbach Yusa Biran
  | location = Jakarta
  | publisher = Komunitas Bamboo working with the Jakarta Art Council
  | year = 2009
  | isbn = 978-979-3731-58-2
  | ref = harv
  }}
*{{cite book
 |title=Indonesia dalam Arus Sejarah: Masa Pergerakan Kebangsaan
 |trans_title=Indonesia in the Flow of Time: The Nationalist Movement
 |language=Indonesian
 |last=Biran
 |first=Misbach Yusa
 |chapter=Film di Masa Kolonial
 |trans_chapter=Film in the Colonial Period
 |author-link=Misbach Yusa Biran
 |publisher=Ministry of Education and Culture
 |year=2012
 |volume=V
 |pages=268–93
 |isbn=978-979-9226-97-6
 |ref=harv
}}
*{{cite news
 |title=Filmaankondiging Cinema Palace: 'Gagak Item'
 |trans_title=Film Review, Cinema Palace: 'Gagak Item'
 |language=Dutch
 |url=http://kranten.kb.nl/view/article/id/ddd%3A011123019%3Ampeg21%3Ap012%3Aa0235
 |work=Bataviaasch Nieuwsblad
 |location=Batavia
 |page=12
 |date=21 December 1939
 |publisher=Kolff & Co.
 |accessdate=4 March 2013
 |ref={{sfnRef|Bataviaasch Nieuwsblad 1939, Filmaankondiging}}
}}
*{{cite news
 |title=Filmniews. Sampoerna: 'De zwarte raaf'
 |trans_title=Film News. Sampoerna: 'De Zwarte Raaf'
 |language=Dutch
 |url=http://kranten.kb.nl/view/article/id/ddd%3A010285888%3Ampeg21%3Ap002%3Aa0063
 |work=De Indische Courant
 |location=Surabaya
 |page=12
 |date=27 November 1939
 |accessdate=14 May 2013
 |ref={{sfnRef|De Indische Courant 1939, Filmniews}}
}}
*{{cite news
 |title=Gagak Item
 |language=Dutch
 |url=http://kranten.kb.nl/view/article/id/ddd%3A011123023%3Ampeg21%3Ap003%3Aa0061
 |work=Bataviaasch Nieuwsblad
 |location=Batavia
 |page=3
 |date=19 December 1939
 |publisher=Kolff & Co.
 |accessdate=4 March 2013
 |ref={{sfnRef|Bataviaasch Nieuwsblad 1939, Gagak Item}}
}}
* {{cite web
 |title=Gagak Item 
 |url=http://filmindonesia.or.id/movie/title/lf-g009-39-831646_gagak-item/credit 
 |work=filmindonesia.or.id 
 |publisher=Konfiden Foundation 
 |location=Jakarta 
 |accessdate=4 March 2013 
 |archiveurl=http://www.webcitation.org/6ErSEzrWF?url=http://filmindonesia.or.id/movie/title/lf-g009-39-831646_gagak-item/credit 
 |archivedate=4 March 2013 
 |ref={{sfnRef|Filmindonesia.or.id, Gagak Item}} 
 |deadurl=yes 
 |df=dmy 
}}
*{{cite book
 |url=https://books.google.com/books?id=m4DVrBo91lEC
 |title=Indonesian Cinema: National Culture on Screen
 |isbn=978-0-8248-1367-3
 |author1=Heider
 |first1=Karl G
 |year=1991
 |publisher=University of Hawaii Press
 |location=Honolulu
 |ref=harv
}}
*{{cite news
 |title=(untitled)
 |language=Dutch
 |url=http://kranten.kb.nl/view/article/id/ddd%3A011122978%3Ampeg21%3Ap006%3Aa0119
 |work=Bataviaasch Nieuwsblad
 |location=Batavia
 |page=6
 |date=16 November 1939
 |publisher=Kolff & Co.
 |accessdate=14 May 2013
 |ref={{sfnRef|Bataviaasch Nieuwsblad 1939, (untitled)}}
}}
*{{cite news
 |title=(untitled)
 |language=Dutch
 |url=http://kranten.kb.nl/view/article/id/ddd%3A010383047%3Ampeg21%3Ap008%3Aa0065
 |work=De Sumatra Post
 |location=Medan
 |page=8
 |date=11 November 1939
 |publisher=J. Hallermann
 |accessdate=14 May 2013
 |ref={{sfnRef|De Sumatra Post 1939, (untitled)}}
}}
*{{cite news
 |title=(untitled)
 |language=Dutch
 |url=http://kranten.kb.nl/view/article/id/ddd%3A010227091%3Ampeg21%3Ap010%3Aa0065
 |work=Het Nieuws van den dag voor Nederlandsch-Indië
 |location=Batavia
 |page=10
 |date=8 May 1939
 |publisher=Kolff & Co.
 |accessdate=14 May 2013
 |ref={{sfnRef|Het Nieuws 1939, (untitled)}}
}}
{{refend}}

==External links==
*{{IMDb title|1848901|Gagak Item}}
{{Portal bar|Film|Indonesia|Netherlands}}

[[Category:1939 films]]
[[Category:Dutch black-and-white films]]
[[Category:Dutch films]]
[[Category:Films directed by the Wong brothers]]
[[Category:Indonesian black-and-white films]]
[[Category:Indonesian films]]
[[Category:Lost films]]
[[Category:Tan's Film films]]