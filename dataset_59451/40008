{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{refimprove|date=December 2013}}
{{Infobox person
|name          = John Weston and family
|image         = John_Weston_family.JPG
|image_size    = 250px
|caption       = John and Lily Weston with Anna, Kathleen (left) and Max, probably 1923, Hungary
|birth_name    = Maximilian John Ludwick Weston
|birth_date    = 17 June 1872
|birth_place   = South of Vryheid
|death_date    = 24 July 1950
|death_place   = [[Bergville]]
|death_cause   = Murder
|resting_place =
|resting_place_coordinates =
|residence     =
|nationality   = [[South African]]
|other_names   =
|known_for     = Aviation pioneer
|education     =
|employer      =
|occupation    =
|home_town     =
|title         =
|term          =
|predecessor   =
|successor     =
|party         =
|boards        =
|religion      =
|spouse        =Lily Roux
|partner       =
|children      =Anna, Kathleen and Max
|parents       =
|relatives     =sister, Lucy
|signature     =
|website       =
|footnotes     =
}}

'''John Weston''' (born Maximilian John Ludwick Weston) was a South African aeronautical engineer, pioneer aviator, farmer and soldier. He travelled in a motor caravan ([[RV]]) that he designed and built himself.

==Notability==
Weston was a pioneer of aviation in [[South Africa]].<ref name="Ob">Oberholzer H. "Pioneers of early aviation in South Africa." Memoirs van die Nasionale Museum, Memoir No 7, 1974.</ref><ref name="Illsley">Illsley J. W. "In Southern skies a pictorial history of early aviation in Southern Africa 1816 - 1940." Jonathan Ball Publishers, 2003.</ref> In 1911, Weston founded the Aeronautical Society of South Africa. The Society hosts a bi-annual memorial lecture in his honour.<ref>[http://www.aessa.org.za] Aeronautical Society of South Africa website.</ref><ref>[http://www.johnwestonpioneer.info] Enthusiast website.</ref>

==Early life==
Weston was born on 17 June 1873,<ref>Album Pages Aeronauts, [http://interactive.ancestry.com/1283/31023_A200057-00084/7947?backurl=http%3a%2f%2fsearch.ancestry.com%2fcgi-bin%2fsse.dll%3fdb%3droyalaeroclub%26so%3d2%26pcat%3dROOT_CATEGORY%26gss%3dangs-g%26new%3d1%26rank%3d1%26gsfn%3djohn%26gsfn_x%3d1%26gsln%3dweston%26gsln_x%3d1%26msbdy_x%3d1%26msbdp%3d1%26MSAV%3d1%26msbdy%3d1872%26gskw%3dafrica%26gskw_x%3d1%26cpxt%3d1%26cp%3d11%26catbucket%3drstp%26uidh%3dg92&ssrc=&backlabel=ReturnSearchResults Great Britain, Royal Aero Club Aviators’ Certificates, 1910-1950 for John L Weston] Ancestry.com (pay to view site). Retrieved 11 April 2016.</ref> in an ox wagon at Fort Marshall, northern [[Natal, South Africa|Natal]], South Africa. His father's name is unknown. His parents may have been British. Weston's mother and sister, Lucy, died in China of [[cholera]] in 1928. His registration of birth is missing from the national records in [[Pretoria]].<ref name="Ob"/>

==Early career==

In 1888 Weston was apprenticed as an engineer to the J. Jaspar company in [[Liège|Liege]], [[Belgium]] and then worked for de Puydt and Poncin Lighting and Power Company, where he rose to the position of partner and technical advisor. In 1900 - 1901, Weston established M. Weston and Co., (Manufacture de la Lampe a arc, 1900) a company manufacturing electrical lights in Liege.

Weston returned to South Africa during the [[Second Boer War]] to fight on the side of the Boers.  Left poor after the war, Weston borrowed £100 from a friend and went to [[United States of America|America]].<ref name="Voor">"A Modern Voortrekker." Sunday Times, Johannesburg, Transvaal, 11 May 1924</ref>

In 1903, Weston applied for membership of the British Institute of Electrical Engineers and the company opened an office in [[Birkenhead]], [[Liverpool]].<ref name="Ob"/> With the assistance of the [[Russia]]n Embassy in Washington, Weston was employed as an engineer for the [[Chinese Eastern Railway]], working on a stretch near [[Lake Baikal]]. It was here he learned to speak [[Russian language|Russian]].<ref name="Voor"/> The railway was to be finished by 1905 but on 8 February 1904, Japan declared war on Russia. Initially stranded, Weston escaped via [[Port Arthur, China|Port Arthur]], Manchuria. Subsequently, he travelled widely to seek work.<ref name="Ob"/>  Weston was admitted as a fellow of the [[Royal Geographical Society]] on 17 September 1904.

Weston returned to South Africa in early 1905. In August 1906, he married Elizabeth (Lily) Maria Jacoba Roux in [[Bloemfontein]]. The couple had three children: Anna MacDougal (b. 1908), Kathleen (b. 1912), and Maximilian John (b. 1915). Weston became a farmer in Doornpoort and later in [[Hoopstad|Kalkdam]]. In May 1909, the family moved to [[Brandfort]], [[Free State (province)|Free State]].<ref name="Ob"/>

==Contribution to aviation==
In 1907 - 1908, at Kalkdam, Weston built an aeroplane from a plan by [[Gabriel Voisin]] with a [[Panhard]] engine, but it was under-powered and never flew. On 14 September 1910, Weston arrived in England en route to [[France]] to pursue his interest in aviation.<ref name="National">The National Archives. "UK Incoming Passenger Lists, 1878–1960". www.ancestry.co.uk</ref>

===France===
In France, Weston trained at the [[Henri Farman]] flying school at [[Étampes]]. On 30 December 1910, Weston flew solo at Étampes and on 5 January 1911, passed his pilot test.<ref>''L’Aero'', 1 and 8 January 1911</ref> He was granted aviator certificate No. 357 by the French Aero Club on 3 February 1911.<ref name="Ob" />

===Brandfort===
Weston returned to South Africa in 1911 with an aircraft powered by a 50&nbsp;hp [[Gnome Omega|Gnome]] engine. The aeroplane was called the ''Weston-Farman''.<ref name="Illsley"/> At Brandfort, Weston imported and sold aeroplanes and parts. These included the [[Blériot Aeronautique|Blériot]] monoplane, the Farman and the [[Bristol Aeroplane Company|Bristol]] biplane, Gnome engines and Chauviére propellers.<ref name="Ob"/>

===Aeronautical Society of South Africa===
Weston was a founding member of the Aeronautical Society of South Africa. He also established the John Weston Aviation Company to raise funds for the establishment of a flying school with a permanent aerodrome. The company's wealthy sponsors funded flying demonstrations in South Africa and [[Mozambique]]. In December 1909, the Frenchman, Albert Kimmerling made the first powered flight in South Africa in [[East London, Eastern Cape|East London]]. In June, 1911, Weston flew the Weston-Farman for eight and a half minutes at [[Kimberley, Northern Cape|Kimberley]]. It was a South African record for the duration of a flight. Demonstrations of the company's five aircraft (one Weston-Farman, three Bristols and one Farman) followed at Johannesburg, Lorenzo Marques, Bloemfontein, Cape Town, Kenilworth, East London, King Williams Town and Queenstown.<ref name="Ob"/>

===Difficulties===
In 1912, Weston was unsuccessful in his efforts to be appointed as an adviser to the government of [[Jan Smuts]] in the investment in military aircraft and pilot training. At that time, the flying demonstrations drew large crowds, but little investment. In January 1913, arrangements were made for flying demonstrations at [[Brandfort]]. A large crowd had assembled on the racecourse when a dust storm began and destroyed the aeroplane. Then, in early February, 1912, the Brandfort hangar was destroyed by arson. Weston dismissed the offer of a joint adventure with aviator, [[Paterson Biplane|Cecil Compton Paterson]]. On 1 July 1913, the Paterson Aviation Syndicate was registered in Kimberley and on 10 September 1913, Paterson and the Union government entered into an agreement concerning the training of the first South African military pilots for what would later become the [[South African Air Force]].<ref name="Ob"/>

==World War I==
Weston had moved to the [[United Kingdom|England]] in June 1913 and by October 1913 was working with the [[Willows airships|Willow’s]] Aircraft Company on military [[dirigible]]s.<ref name="Ob"/> In February 1914, he received British Aeronaut’s Certificate No. 38 (for flying balloons) as well as Airship Pilot’s Certificate No.23.<ref>[http://www.flightglobal.com/pdfarchive/view/1914/1914%20-%200138.html "Committee Meeting."][[Flight International|''Flight'']] 7 February 1914.</ref>

At the outbreak of World War I Weston joined the South African forces taking part in the [[South West Africa Campaign]] (present day Namibia). He was responsible for providing and maintaining airfields.<ref>L’Ange G. "Urgent imperial service: South African Forces in German South West Africa 1914 – 1915." Ashanti. Rivonia, South Africa. 1991.</ref> On 6 February 1915 he joined the [[South African Air Force|South Africa Air Corps]] (SAAC) with the rank of [[lieutenant]].<ref name="SWA">[http://samilitaryhistory.org/vol132hp.html] South African Military History organisation website.</ref> It was not until 1 May 1915 that aircraft were available to South Africa's military forces and they were then found to be unserviceable. However, Weston was able to place beacons for pilots as far afield as Garub, now in [[Namibia]].<ref name="HH">[http://www.flightglobal.com/pdfarchive/view/1915/1915%20-%200988.html  "The S. A. Aviation Corps and their doings in S. W. Africa"] ''Flight''. 3 December 1915.</ref>

After German forces capitulated in South West Africa on 9 July 1915, Weston and his family travelled to England, arriving in [[Tilbury]] on 9 September 1915.<ref name="National"/> On 1 July 1916, he was commissioned as a temporary sub-lieutenant in the [[Royal Naval Reserve|Royal Naval Volunteer Reserve]], for duties with the [[RNAS]]. His first posting was to No. 3 Aeroplane Wing, [[RAF Manston|Manston]].<ref>''The London Gazette''. 4 July 1916.</ref> On 28 September 1916, he was promoted to temporary acting lieutenant.<ref>RNR Pay and Appointing Ledger. Fleet Air Arm Museum Archive Department and Research Centre.</ref> No. 3 (Naval) Wing] formed during the spring of 1916. Weston's group was one of the first departures to an airfield at [[Luxeuil-les-Bains]], operating over a strategically important German manufacturing region. Weston's role included calibration of compasses and provision of marked maps to observers and airmen, intelligence work and retrieving downed airmen. He also negotiated reparations for local farmers and residents affected by his unit's operations, since he spoke French.<ref name="manito">Dodds R. V. [http://www.manitobamilitaryaviationmuseum.com/PDF/No3Wing.pdf "Britain's first strategic bombing force: no. 3 naval wing."] ''The Roundel'', July – August 1963, Vol. 15, No. 6.</ref> From 28 July 1916, Weston also worked as a translator.<ref name="3Wing">"No. 3 Wing R.N.A.S. 1916–1917 - Britain’s first strategic bombers." Appendix O, part 1. Fleet Air Arm Museum Archive Department and Research Centre.</ref>

On 2 April 1917, Weston was posted as a compass officer to No. 2 wing, RNAS, at [[Moudros]]on the Greek island of [[Lemnos]] in the Aegean Sea. Weston also managed aerodrome construction, drainage and roadmaking. This posting included a short time as an intelligence officer in [[Port Said]] in Egypt. It also included training of pilots, crew and engineers of the Hellenic naval air service.<ref>Thanos M. L. "Weston J." South African National War Museum file.</ref> Weston was recorded as "a thorough and efficient mapping officer, very energetic and hardworking...a very capable 'E' officer"{{citequote|date=January 2017}}. He remained in Moudros at least until 1918.<ref name="Ob"/> Between 1919 and 1921, Weston made two trips to the [[United States of America]] on Air Ministry business. He left the service on 22 November 1923.<ref>The London Gazette. 4 December 1923</ref>

On the formation of the [[Royal Air Force]] from the [[Royal Naval Air Service]] and the [[Royal Flying Corps]] on 1 April 1918, Weston was commissioned as a  Lieutenant in the RAF.  In August 1918, while was seconded to the British Naval Mission to Greece as head of the technical section, he was promoted to "Major whilst specially employed”.<ref>''The London Gazette'', 2 August 1918</ref> On 9 January 1919, Weston was promoted to Major in recognition of his distinguished service.<ref>"Supplement to The London Gazette". 1 January 1919</ref> In July 1919, Weston was awarded the [[Order of the Redeemer|Cross of Officer of the Royal Hellenic Order of the Redeemer]].<ref>"Flight" 31 July 1919</ref> and in 1923 he was promoted to Vice-Admiral in the [[Royal Hellenic Navy]]. Weston later named his South African property, “''Admiralty Estate''”.<ref name="Ob"/>

==Travel==
==="''Suid Afrika''"===
Weston had a love of travel and encouraged this in his children. While in the US, between 1919 and 1921, Weston purchased a Detroit-manufactured ''Commerce'' one tonne truck with a ''Continental N'' engine. The truck was shipped to England where Weston converted it into a [[motor home]] which could sleep five.<ref name="palace">"Free staters see the world in a caravan." ''The Friend'', Bloemfontein, 17 June 1928.</ref> Weston painted the vehicle yellow with black trim and called it "''Suid Afrika''".<ref name="WC">"Weston Caravan." Winterton Museum, South Africa.</ref> The ''Suid Afrika'' was essentially a large wooden structure on the rear chassis of the truck. Windows were placed at the front of the living quarters and in the sides of the lantern roof.<ref name="outspan">Rosenthal E. A. "South African caravans around the globe." ''The Outspan'', 3 October 1930.</ref> The vehicle could be hoisted upright onto a ship's deck. A sign on the side read, "Our mansion: 7 by 14 feet, Our field: the world, Our family: mankind." and surrounding this, in a circle, "Round the World." Weston and his family took a tour of 18 weeks in the ''Suid Afrika'' from England to Greece. The Westons lived in Athens for two years then in May 1924, returned in the Suid Afrika to England and from there to South Africa. In 1925, Weston made extensive travels through southern Africa. In 1926, the family attempted to return to  England, overland. In 1927, Weston found the motor home was not suitable for the trip and returned to [[Cape Town]]. (The house in Brandfort was sold in April 1928.)

The motor home remains on display as an exhibit of the [[Winterton, KwaZulu-Natal|Winterton]] Museum.

==="''Prairie Schooner''"===
The ''Prairie Schooner'' was a second motor home built by Weston. It had a removable living area with a canvas cover and was water-proofed for river crossings. In 1931 - 1932, Weston travelled from [[Cape Agulhas]], the southernmost point of Africa, to [[Belgrade]], [[Bulgaria]] (and to England and back).<ref name="Egypt">A. D. N. "Across Africa and Europe by car: the Weston family expedition." ''Egyptian Gazette.'' 24 February 1932.</ref> Weston used the ''Prairie Schooner'' for the remainder of his life. In 1975, the ''Prairie Schooner'' featured in the International Veteran and Vintage Car Rally from Durban to Cape Town. It was later donated to the [[Winterton, KwaZulu-Natal|Winterton]] Museum, [[KwaZulu Natal]], South Africa, by Weston’s son in law, Carl Rein Weston.<ref name="WC"/>

==Later life==
On his return to South Africa in 1933, Weston bought a farm in the Bergville district, near the [[Sterkfontein]] dam. On 24 July 1950, Weston and his wife were injured in an attack.{{citation needed|date=January 2017}}  Weston died three days later at the age of 78. His wife survived. She lived with her eldest daughter, Anna, in the [[Transvaal Province|Transvaal]], and died at age 91.

==See also==
*[http://www.zulu.org.za/places-to-go/unallocated/winterton-museum-P56249 Winterton Museum]
*[http://www.johnwestonpioneer.info Maximilian John Ludwick Weston]

==References==
{{reflist|2}}

{{DEFAULTSORT:Weston, John}}
[[Category:1872 births]]
[[Category:1950 deaths]]
[[Category:South African people of World War I]]
[[Category:South African aviators]]
[[Category:German South West Africa]]
[[Category:Royal Naval Air Service aviators]]
[[Category:Royal Air Force personnel of World War I]]
[[Category:Hellenic Navy admirals]]
[[Category:Boer military personnel of the Second Boer War]]
[[Category:Fellows of the Royal Geographical Society]]
[[Category:Electrical engineers]]