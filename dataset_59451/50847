{{Infobox journal
| title = Canadian Studies in Population
| abbreviation = Can. Stud. Popul.
| editor = Frank Trovato
| discipline = [[Demography]]
| language = English with abstracts in English and French 
| publisher = Population Research Laboratory Department of Sociology, The University of Alberta
| country = Canada
| frequency = Biannually
| history = 1974-present
| openaccess = Yes
| license =
| impact =
| impact-year = 
| website = http://web.uvic.ca/~canpop/journal/
| ISSN = 0380-1489 
| eISSN = 1927-629X
| LCCN = 86642598
|OCLC = 85449592
}}
'''''Canadian Studies in Population''''' is a [[peer review|peer-reviewed]] [[academic journal]] publishing original research in areas of [[demography]], [[population studies]], [[demographic analysis]], and the [[demographics of Canada]] and other populations. The journal was established in 1974 and is published biannually by the [http://web.uvic.ca/~canpop/ Canadian Population Society], with support from the Population Research Laboratory at the [[University of Alberta]], the Society of Edmonton Demographers (SED) and the [[Social Sciences and Humanities Research Council]] of Canada.     

Articles are published in English, with abstracts in French and English, while occasional articles may be published in French.  ''Canadian Studies in Population'' is indexed in Web of Science, Current Contents/Social and Behavioral Sciences, Social Sciences Citation Index, and Sociological Abstracts. The journal is available as an online version and in February 2008 the journal became part of the Directory of Open Access Journals. Link to the journal at: http://web.uvic.ca/~canpop/journal/

==Scope==

The ''Canadian Studies in Population'' publishes articles on a broad range of studies in substantive and technical demography and related fields of study.   While ''Canadian Studies in Population'' emphasizes the demography of [[Canada]], the editorial board encourages manuscript submissions from all sectors of the international community. The journal occasionally publishes special volumes of [[interdisciplinarity|interdisciplinary]] research.

==History==
The journal was first established in 1974 through the efforts of several demographers affiliated with the University of Alberta, including P. Krishnan (editor), John Forster, K.L. Gupta, Charles W. Hobart, Leszek Kosinski, Karol J. Krotki and Earle Snider.  The original editorial board included several Canadian demographers who have made an important contribution to demographic research, both in Canada and internationally, including [[Nathan Keyfitz]] (Harvard), T.R. Balakrishnan (Western), Jacques Henripin (University of Montreal), Warren Kalbach (University of Toronto), Karol Krotki (University of Alberta), Anthony Richmond (York University), [[Anatole Romaniuk]] (Statistics Canada), and Leroy Stone (Statistics Canada).   The inaugural issue in 1974 included papers written by Kingsley Davis, Nathan Keyfitz, and Roland Pressat.

==External links==
* {{Official website|http://web.uvic.ca/~canpop/journal}}
* [http://web.uvic.ca/~canpop/ Canadian Population Society]


{{DEFAULTSORT:Canadian Studies in Population}}
[[Category:Sociology journals]]
[[Category:Open access journals]]
[[Category:Publications established in 1974]]
[[Category:Demographics of Canada]]
[[Category:Biannual journals]]
[[Category:University of Alberta]]
[[Category:English-language journals]]