{{Infobox journal
| cover = 
| editor = Howard E. Gendelman
| discipline = [[Neuroscience]]
| abbreviation = J. Neuroimmune Pharmacol.
| publisher = [[Springer Science+Business Media]]
| country =
| frequency = Quarterly
| history = 2006–present
| openaccess =
| license =
| impact = 3.896
| impact-year = 2014
| website = http://www.springer.com/biomed/neuroscience/journal/11481
| link2 =http://link.springer.com/journal/volumesAndIssues/11481
| link2-name = Online archive
| JSTOR =
| OCLC = 830935768
| LCCN = 2005215692
| CODEN =
| ISSN = 1557-1890
| eISSN = 1557-1904
}}
The '''''Journal of Neuroimmune Pharmacology''''' is a quarterly [[peer-reviewed]] [[scientific journal]] covering research on the intersection of immunology, pharmacology, and neuroscience as they relate to each other. The journal occasionally publishes special thematic issues. It was established in 2006 and is published by [[Springer Science+Business Media]] on behalf of the [[Society on NeuroImmune Pharmacology]]. The [[editor-in-chief]] is Howard E. Gendelman ([[University of Nebraska Medical Center]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Academic OneFile]]
* [[AGRICOLA]]
* [[Biological Abstracts]]
* [[BIOSIS Previews]]
* [[EMBASE]]
* [[PubMed]]/[[MEDLINE]]
* [[Science Citation Index Expanded]]
* [[Scopus]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 4.11.<ref name=WoS>{{cite book |year=2015 |chapter=Journal of Neuroimmune Pharmacology |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.springer.com/biomed/neuroscience/journal/11481}}
* [https://s-nip.org/ Society on NeuroImmune Pharmacology]

[[Category:Immunology journals]]
[[Category:Neuroscience journals]]
[[Category:Pharmacology journals]]
[[Category:English-language journals]]
[[Category:Publications established in 2006]]
[[Category:Quarterly journals]]
[[Category:Springer Science+Business Media academic journals]]


{{pharma-journal-stub}}