'''Environmental managers''' are involved in processes that ''seek to'' [[Control (management)|control]] some environmental [[entities]] in orientation to a [[plan]] or idea. Whether such control is possible, however, is contested. Examples for environmental managers range from corporate agents (corporate environmental managers) via managers of a [[nature reserve]], to environmental and resource planning agents but, [[analysis|analytically]] seen, also involve [[indigenous peoples|indigenous]] environmental managers, [[farmer]]s<ref>M. Kaljonen. Co-construction of agency and environmental management. the case of agri-environmental policy implementation at finnish farms. Journal of Rural Studies, 22(2):205 – 216, 2006.</ref> or [[environmental activists]]. In many accounts, [[hope]] is held that environmental managers implement grand plans or political programmes. For example, specific schools of thought like [[ecological modernisation]] but also widespread conceptions of [[environmental management]] and [[environmental activism]] presuppose human [[agency (philosophy)|agents]] who have ideas, make plans and engage in action oriented at the plans' implementation.<ref>Lippert, I. 2010, "[http://www.ems-research.org/agents%20of%20ecological%20modernisation Agents of Ecological Modernisation]", Lübeck, DAV, ISBN 978-3-86247-062-4.</ref> At the heart of the notion of environmental managers is, thus, a [[pragmatism|pragmatic]]<ref>P. Prasad and M. Elmes. In the name of the practical: Unearthing the hegemony of pragmatics in the discourse of environmental management. Journal of Management Studies, 42(4):845–867, 2005.</ref> and [[rational actor]] who optimises environments in orientation to some [[Purpose|aim]]. Critical academics point out that the very idea that such managers exist and are imagined as capable of managing may well be flawed.<ref>D. Levy. Environmental Management as Political Sustainability. Organization & Environment, 10(2):126–147, 1997.
</ref>

== Corporate environmental managers ==

[[Steve Fineman]] studied UK managers and their "'green' selves and roles" in the last decade, suggesting that while [[ecological crisis|environmental problem]]s may be recognised by them, production is seen as legitimising [[pollution]].<ref>S. Fineman. Constructing the green manager. British Journal of Management, 8:31–38, 1997.</ref> Optimistic accounts see managers as stewards of environmental ethics.<ref>W. Brown and N. Karagozoglu. Current practices in environmental management. Business Horizons, 41(4):12–18, Jul.-Aug. 1998.</ref> Literature differentiates different styles by managers to engage with the environment.<ref>
N. Gunningham, R. Kagan, and D. Thornton. Shades of green: business, regulation, and environment. Stanford University Press, Stanford, 2003.</ref>  Critics suggest that corporate environmental managers are systematically positioned in a contradictory situation in which they are supposed to be committed to competing normative orientations (e.g. [[Profit (economics)|profits]] versus [[environmental protection]] measures which do not pay off).<ref>I. Lippert. Disposed to unsustainability?  ecological modernisation as a techno-science enterprise with conflicting normative orientations. In A. Bammé, G. Getzinger, and B. Wieser, editors, Yearbook 2009 of the Institute for Advanced Studies on Science, Technology and Society, pages 275–290. Profil, München, 2010.</ref>

== State environmental managers ==

State institutions can manage directly environments through their staff.<ref>K. Asdal. Enacting things through numbers: Taking nature into account/ing. Geoforum, 39(1):123–132, 2008.</ref> And state institutions can use civil agents on their behalf. Examples for the latter are [[farmer]]s<ref>M. Kaljonen. Co-construction of agency and environmental management. the case of agri-environmental policy implementation at finnish farms. Journal of Rural Studies, 22(2):205 – 216, 2006.</ref> who are to implement environmental regulation, [[citizen]]s subject to e.g. recycling legislation or independent [[auditor]]s who use [[law]]s as [[Environmental standard|standard]]s. [[Military]] agents can also act as environmental managers insofar as their action constitutes planned intervention in some environment (e.g. the burning of a [[forest]], the destruction of [[street]]s or managing an open landscape for [[military training]]), trying to achieve military aims.

== Scientists as environmental managers ==

A variety of [[scientist]]s are involved directly in environmental management. Cases of [[ecologists]] acting as managers of [[ecosystems]]<ref>L. Asplen. Going with the flow: Living the mangle through environmental management practice. In A. Pickering and K. Guzik, editors, The mangle in practice: science, society, and becoming, Science and Cultural Theory, pages 163–184. Duke University Press Books, Durham and London, 2008.</ref> are known.

== Study of environmental managers ==

In the field of [[environmental management]], until now, little attention has been paid to the study of those agents who supposedly put into practices the prescriptions.<ref>Lippert, I. 2010, "[http://www.ems-research.org/agents%20of%20ecological%20modernisation Agents of Ecological Modernisation]", Lübeck, DAV, ISBN 978-3-86247-062-4.</ref> For the realm of environmental management addressed by ecological modernisation, such agents can be termed ''agents of ecological modernisation''.<ref>Lippert, I. 2010, "[http://www.ems-research.org/agents%20of%20ecological%20modernisation Agents of Ecological Modernisation]", Lübeck, DAV, ISBN 978-3-86247-062-4.</ref>

The very notion that humans may be able to manage environments is criticised for being [[top-down]], [[anthropocentric]] and short-sighted.<ref>D. Bavington. Managerial ecology and its discontents: Exploring the complexities of control, careful use and coping in resource and environmental management. Environments - A Journal of Interdisciplinary Studies, 30(3):3–22, 2002. and 
R. Bryant and G. Wilson. Rethinking environmental management. Progress in Human Geography, 22(3):321–343, Sep 1998.
</ref>

Studies of the work reality of practices of environmental managers are rarely classified as such. Studying the work reality of these managers indicates that it should be better conceptualised as [[situated action]] (for example by drawing on [[ethnomethodology|ethnomethodological studies of work practice]]) rather than [[rational action]].<ref>
I. Lippert. Extended carbon cognition as a machine. Computational Culture, 1(1), 2011.</ref> Thus, when attending to managers' practices, the notion of environmental management can be reconceptualised as a ''[[wikt:prescription|prescription]]''. In optimistic accounts of environmental management, the latter is often mistaken for a ''[[description]]''.

== See also ==
* [[Environmental activist]]
* [[Chief sustainability officer]]
* [[Rational planning model]]

== References ==
{{Reflist|2}}

== External links ==
* [http://www.ems-research.org/environmental-management-practised How do you manage? Unravelling the situated practice of environmental management]
* [http://www.environmentalmanager.org The Environmental Manager Symposium]

[[Category:Articles created via the Article Wizard]]
[[Category:Environmental sociology]]
[[Category:Environmental policy]]
[[Category:Sustainability and environmental management]]