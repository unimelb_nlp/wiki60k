{{Infobox artist
| name          = John Baeder
| birth_date    = {{birth-date|1938}}
| birth_place   = [[South Bend, Indiana]]
| nationality   = American
| field         = [[Painting]], [[Fine art photography|Photography]]
| movement      = [[Photorealism]]
| training      = [[Auburn University]], [[Auburn, Alabama]]
}}

'''John Baeder''' (born December 24, 1938) is an American painter closely associated with the [[Photorealism|Photorealist movement]]. He is best known for his detailed paintings of American [[Roadside attraction|roadside]] diners and eateries.

==Early life==

John Baeder was born in 1938 in [[South Bend, Indiana]], but was raised in [[Atlanta, Georgia]]. The interest in small towns across America began when he was young by photographing old cars and other relics with a [[Brownie (camera)|Baby Brownie camera]].<ref>Virginia Anne Bonito, ''Get Real: Contemporary American Realism from the Seavest Collection'', 10-15.</ref> While attending [[Auburn University]] in the late 1950s, he made frequent trips between [[Atlanta]] and [[Alabama]], which drew his attention to rural landscapes and roadside diners.

==Early years==

He started working as an [[art director]] in Atlanta for a branch of a New York [[advertising agency]] in 1960, and subsequently moved to [[New York City]] in 1964. He went on to have a successful career in advertising through the early 1970s, while continuing to paint, draw and photograph on his own time.<ref>''Pleasant Journeys and Good Eats along the Way: A Retrospective Exhibition of Paintings by John Baeder'', The Morris Museum of Art. Traditional Fine Arts Organization. http://www.tfaoi.com/aa/7aa/7aa929.htm.</ref>

One of his ad agency offices in New York City was located near the [[Museum of Modern Art]]. The museum’s photograph department became a source of inspiration for him, especially the work of artists such as [[Berenice Abbott]], [[Walker Evans]], [[Ben Shahn]], and other photographers of the [[Farm Security Administration]]. In the late 1960s he also started collecting postcards of roadside America such as diners, gas stations, campsites, and motels.<ref>Virginia Anne Bonito, ''Get Real: Contemporary American Realism from the Seavest Collection'', 10-15.</ref>

==Artistic career==
[[File:John's Diner by John Baeder.jpg|thumb|250px|''John's Diner with John's Chevelle'', 2007<br> John Baeder, oil on canvas, 30×48 inches.]]
Baeder left the advertising field in 1972 to pursue his artistic career full-time. The same year, [[OK Harris Gallery]] in New York began exhibiting his artworks.<ref>Steven Heller, "Why Does John Baeder Paint Diners?" The design Observer Group, http://observatory.designobserver.com/entry.html?entry=11647.</ref>

Since then, he has had more than thirty solo exhibitions at art galleries such as OK Harris Gallery in New York; Modernism Gallery in San Francisco, Thomas Paul Fine Art in Los Angeles and Cumberland Gallery in Nashville, as well as a traveling retrospective exhibition titled “Pleasant Journeys and Good Eats along The Way,”<ref>Donald Kuspit, introduction to ''Pleasant Journeys And Good Eats Along The Way: A Retrospective Exhibition Of Paintings By John Baeder'', ed. Jay Williams.</ref> which started at the [[Morris Museum of Art]] in [[Augusta, Georgia]] in December 2007.<ref>“Past Exhibitions,” Morris Museum of Art, http://www.themorris.org/pastexhibitions.html#2007.</ref>

His work includes oil paintings, watercolors and photographs and can be found in the permanent collections of the [[Whitney Museum of American Art]], the [[Cooper-Hewitt Museum]], the [[Norton Museum of Art]], the [[Denver Art Museum]], the [[Milwaukee Art Museum]], the [[High Museum of Art]], the [[Detroit Institute of Arts]],the [[Indianapolis Museum of Art]], the [[Cheekwood Botanical Garden and Museum of Art|Cheekwood Museum of Art]], the [[Tennessee State Museum]], the [[Yale University Art Gallery]], and the Morris Museum of Art among others.

According to John Arthur,<ref>John Arthur is an independent curator, writer, and art advisor. He has been acknowledged internationally as a leading authority on contemporary American realism and figurative art.</ref> “John Baeder is much more than a painter of diners. He is a knowledgeable and deeply committed chronicler of that rapidly disappearing facet of American vernacular architecture that has played such a unique role in our social and cultural history.”<ref>John Arthur, foreword to ''Diners; Revised and Updated'', by John Baeder and Vincent Scully.</ref> [[Vincent Scully]], professor of the History of Art in Architecture and author, further comments on Baeder’s visual style in his introduction to ''Diners'', 1978, stating that his "paintings seem to me to differ from most of those of his brilliant [[Magic Realism|Magic-Realist]] contemporaries in that they are gentle, lyrical, and deeply in love with their subjects. Most of the painters of the contemporary Pop scene blow our minds with massive disjunctions, explosive changes of scale, and special kind of wink-less visual focus. Baeder does not employ any of those devices. He sees everything as its own size in its proper environment. His diners fit into their urban context like modest folk heroes."<ref>Vincent Scully, Introduction to ''Diners'', by John Baeder.</ref>

Baeder is the recipient of the Tennessee Governor’s Distinguished Artist Award in 2009.<ref>“Arts Tennessee Winter/Spring 2009 Newsletter,” Tennessee Arts Commission, http://www.arts.state.tn.us/artsTN/artstnwinter2009.pdf, 6.</ref> He lives and works in [[Nashville, Tennessee]].

==Notes==
{{Reflist}}

==Sources and further reading==

===Books and catalogues===
*Baeder, John, ''Diners''. With an introduction by Vincent Scully. New York, NY: Harry N. Abrams, 1978.
*Baeder, John, ''Diners; Revised and Updated''. With a foreword by John Arthur and a preface by Vincent Scully. New York, NY: Harry N. Abrams, 1995.
*Baeder, John, ''Sign Language: Street Signs as Folk Art''. New York, NY: Harry N. Abrams, 1996.
*Baeder, John, ''Gas, Food, and Lodging''. New York, NY: Abbeville Press, 1982.
*Frank, Peter, ''John Baeder’s American Roadside: Early Photographs''. Los Angeles, California: Thomas Paul Fine Art, 2009.
*Baeder, John, Jay Williams, ed., ''Pleasant Journeys And Good Eats Along The Way: A Retrospective Exhibition Of Paintings By John Baeder''. With a preface by Kevin Grogan and an introduction by Donald Kuspit. Jackson, Mississippi: University Press of Mississippi, 2007.
*Edwards, Susan H., ''John Baeder: 1960's Photographs''. Self-published, ltd. ed. of 175, 2009.
*Bonito, Virginia Anne, ''Get Real: Contemporary American Realism from the Seavest Collection''. Durham, North Carolina: Duke University Museum of Art, 1998.
*Leeds, Valerie Ann, Ph.D, Introduction to ''Shock of the Real: Photorealism Revisited''. Boca Raton, Florida: Boca Raton Museum of Art, 2008.
*Meisel, Louis K., ''Photorealism''. New York, NY: Harry N. Abrams, 1980.

===Films===
*''Baeder: Pleasant Journeys and Good Eats Along the Way''. Directed by Curt Hahn. 2009, Nashville, Tennessee: Film House. http://www.filmhouse.com/baeder.php

===Other===
*Heller, Steven, "Why Does John Baeder Paint Diners?," The Design Observer Group, November 17, 2009, http://observatory.designobserver.com/entry.html?entry=11647.
*“Arts Tennessee Winter/Spring 2009 Newsletter,” Tennessee Arts Commission, http://www.arts.state.tn.us/artsTN/artstnwinter2009.pdf, 6.
*“John Baeder”, Indianapolis Museum of Art, http://www.imamuseum.org/art/collections/artist/baeder-john
*“John Baeder opening reception”, Tennessee State Museum, http://www.tnmuseum.org/Membership/Join_the_Fun%21/
*“Past Exhibitions,” Morris Museum of Art, http://www.themorris.org/pastexhibitions.html#2007.
*Nguyen, C. Thi.  "Humble Trucks, Great Food,"  ''Los Angeles Times'',  April 21, 2008. p. A1.
*"Roadside America for the 21st Century,"  ''Los Angeles Times Sunday Magazine'',  June 1, 2003. p.&nbsp;10.
*Reif, Rita.  "A Fading Language Of the Roadway,"  ''New York Times'',  June 30, 1996. p.&nbsp;31, http://www.nytimes.com/1996/06/30/arts/arts-artifacts-a-fading-language-of-the-roadway.html?scp=1&sq=A%20Fading%20Language%20Of%20the%20Roadway&st=cse
*Hudson, Stacey.  "On the Road," '' Metro Spirit'' (Augusta, Georgia),  issue 19.21. December 19–25, 2007, http://www.metrospirit.com/index.php?cat=1993101070593169&ShowArticle_ID=11011812070736572

==External links==
*[http://www.johnbaeder.com/ Official website]
*[http://www.okharris.com/artists/baeder.htm John Baeder at OK Harris Gallery]
*[http://www.artnet.com/awc/john-baeder.html John Baeder on artnet Monographs]
*[http://www.tpaulfineart.com/index.php/artists/bio/john_baeder John Baeder at Thomas Paul Fine Art]
{{Authority control}}

{{DEFAULTSORT:Baeder, John}}
[[Category:1938 births]]
[[Category:Photorealist artists]]
[[Category:20th-century American painters]]
[[Category:American male painters]]
[[Category:21st-century American painters]]
[[Category:Auburn University alumni]]
[[Category:Living people]]
[[Category:People from South Bend, Indiana]]
[[Category:Artists from Atlanta]]
[[Category:Photographers from Georgia (U.S. state)]]
[[Category:Photographers from Indiana]]