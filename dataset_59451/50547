{{About|the biomedical journal|therapy with antiviral agents|antiviral drug}}
{{Orphan|date=February 2017}}

{{Infobox journal
 | cover = [[Image:AVTcover (2007).jpg]]
| discipline = [[Antiviral drug]]s
| abbreviation = Antivir. Ther.
| publisher = [[International Medical Press]]
| country =
| frequency = 8/year
| history = 1996–present
| openaccess = [[Delayed open access|Delayed]], after 12 months
| impact = 3.02
| impact-year = 2014
| website = http://www.intmedpress.com/index.cfm?pid=1
| link1 = http://www.intmedpress.com/index.cfm?pid=88
| link1-name = Online access
| ISSN = 1359-6535
| OCLC = 67723439
| CODEN = ANTHFA
}}
'''''Antiviral Therapy''''' is a [[Peer review|peer-reviewed]] [[medical journal]] published by [[International Medical Press]]. It publishes primary papers and reviews on all aspects of the clinical development of [[antiviral drug]]s, including [[clinical trial]] results, [[drug resistance]], viral [[Diagnosis|diagnostics]], [[drug safety]], [[pharmacoepidemiology]], and [[vaccine]]s. ''Antiviral Therapy'' is an official publication of the [[International Society for Antiviral Research]].

The journal was established in 1996 by [[Douglas Richman|Douglas D. Richman]] ([[University of California, San Diego]]) and [[Joep Lange|Joep M.A. Lange]] ([[University of Amsterdam]]), who still as of 2013 serve as the joint [[editors-in-chief]]. The first two issues were published by MediTech Media.<ref>''Antiviral Therapy'' Vol. 1, issue 2. Front cover</ref> The initial publication frequency was quarterly, rising to bimonthly in 2003 and to eight issues a year in 2005. The journal also publishes supplements containing abstracts from various conferences and workshops, including the International HIV Drug Resistance Workshop, International Workshop on Adverse Drug Reactions and Lipodystrophy in HIV, and the Therapies for Viral Hepatitis Workshop.

Articles from 1998 are archived online in [[PDF]] format, with content [[Delayed open access|over a year old being available for free]]. All online content is available free to those living in developing countries through [[HINARI]].

== Abstracting and indexing ==
The journal is abstracted and indexed by [[BIOSIS Previews]], [[Chemical Abstracts]], [[Current Contents]]/Clinical Medicine, [[EMBASE]]/[[Excerpta Medica]], [[MEDLINE]]/[[Index Medicus]], and the [[Science Citation Index]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 3.02.<ref name=WoS>{{cite book |year=2015 |chapter=Antiviral Therapy |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |accessdate= |series=[[Web of Science]] |postscript=.}}</ref>

== See also ==
* ''[[Antiviral Chemistry & Chemotherapy]]''

== References ==
{{Reflist}}

== External links ==
* {{Official website|1=http://www.intmedpress.com/index.cfm?pid=12}}

[[Category:Microbiology journals]]
[[Category:Delayed open access journals]]
[[Category:Publications established in 1996]]
[[Category:English-language journals]]