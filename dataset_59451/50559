{{Infobox journal
| cover = [[File:Apb cover web.jpg]]
| editor = Dieter Meschede
| discipline = [[Physics]]
| abbreviation = Appl. Phys. B
| publisher = [[Springer Science+Business Media]]
| frequency = Biweekly
| history = 1994–present
| openaccess =
| impact = 1.782
| impact-year = 2012
| website = http://www.springer.com/journal/340
| link1 = http://www.springerlink.com/content/0946-2171
| link1-name = Online access
| link2 =
| link2-name =
| JSTOR =
| OCLC = 29901949
| LCCN = 98640743 
| CODEN = APBOEM
| ISSN = 0946-2171
| eISSN = 1432-0649 
}}
'''''Applied Physics B: Lasers & Optics''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] published by [[Springer Science+Business Media]]. The [[editor-in-chief]] is Dieter Meschede ([[University of Bonn]]).<ref name=about/> Topical coverage includes laser physics, optical & laser materials, linear optics, nonlinear optics, quantum optics, and photonic devices. Interest also includes laser spectroscopy pertaining to atoms, molecules, and clusters. The journal publishes original research articles, invited reviews, and rapid communications.<ref name=about/>

==History==
The journal is a continuation of ''Applied Physics B: Photophysics and Laser Chemistry'' ({{ISSN|0721-7269}}), in existence from September 1981 (volume B: 26 no. 1) to December 1993 (volume B: 57 no. 6) It partly continues ''Applied Physics'' ({{ISSN|0340-3793}}), in existence from January 1973 (volume 1 no. 1) to August 1981 (volume 25 no. 4).<ref name=cassi>[http://cassi.cas.org/publication.jsp?P=LglBQf5Q2NQyz133K_ll3zLPXfcr-WXfbwxZX5Vn9Lb5mQM2hY0t4TLPXfcr-WXfimSBIkq8XcUjhmk0WtYxmzLPXfcr-WXfQkxsor-G3sgI5QWP4EUbNg CASSI]. American Chemical Society. 2011</ref>

==Abstracting and indexing==
The journal is abstracted and indexed in:<ref name=about>[http://www.springer.com/physics/journal/340?detailsPage=aboutThis About this journal]. Springer. 2013</ref>
{{columns-list|colwidth=30em|
* [[Academic OneFile]]
* [[Academic Search]]
* [[Astrophysics Data System]]
* [[Chemical Abstracts Service]]
* [[Chimica]]
* [[Current Abstracts]]
* [[Current Contents Collections]]/Electronics & Telecommunications Collection
* [[Current Contents]]/Physical, Chemical and Earth Sciences
* [[EBSCO]]
* [[EI-Compendex]]
* [[International Nuclear Information System|INIS]] [[Atomindex]]
* [[Inspec]]
* [[Mass Spectrometry Bulletin]]
* [[Materials Science Citation Index]]
* [[PASCAL (database)|PASCAL]]
* [[Science Citation Index]]
}}

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.springer.com/journal/340}}

[[Category:Hybrid open access journals]]
[[Category:Publications established in 1981]]
[[Category:Optics journals]]
[[Category:Physics journals]]
[[Category:Biweekly journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:English-language journals]]