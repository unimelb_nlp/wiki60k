{{Infobox album <!-- See Wikipedia:WikiProject_Albums -->
| Name        = Niandra LaDes and Usually Just a T-Shirt
| Type        = studio
| Artist      = [[John Frusciante]]
| Cover       = Niandra_LaDes_and_Usually_Just_a_TShirt_album_cover.jpg
| Released    = November 4, 1994<ref>http://invisible-movement.net/gallery/d/4759-3/1994bikini_03.jpg</ref>
| Recorded    = 1991-1993
| Genre       = [[Lo-fi music|Lo-fi]], [[avant-garde music|avant-garde]], [[psychedelic rock]], [[experimental rock]]
| Length      = 70:12 (cd version); 76:06 (cassete version)
| Label       = [[American Recordings (US)|American Recordings]]
| Producer    = John Frusciante
| Last album  =
| This album  = '''''Niandra LaDes and Usually Just a T-Shirt'''''<br />(1994)
| Next album  = ''[[Smile from the Streets You Hold]]''<br />(1997)
}}

'''''Niandra LaDes and Usually Just a T-Shirt''''' is the debut solo album by [[John Frusciante]], released on November 22, 1994, on [[American Recordings (US)|American Recordings]]. Frusciante released the album after encouragement from several friends, who told him that there was "no good music around anymore."<ref name=jfcom-niandra>{{cite web|publisher=Johnfrusciante.com|url=http://www.johnfrusciante.com/music/niandra.php|title=''Niandra Lades and Usually Just a T-Shirt''|accessdate=2007-07-31 |archiveurl=https://web.archive.org/web/20071010131249/http://www.johnfrusciante.com/music/niandra.php |archivedate=2007-10-10}}</ref>

''Niandra LaDes and Usually Just a T-Shirt'' combines [[avant-garde]] and [[Stream of consciousness writing|stream-of-consciousness]] styles, with guitar, piano and various effects on a four-track recorder. The album's first half, ''Niandra Lades'', was recorded before Frusciante left the [[Red Hot Chili Peppers]] in 1992; during the recording of ''[[Blood Sugar Sex Magik]]''. The second half, ''Usually Just a T-Shirt'', was recorded while the band was on tour in the months leading up to Frusciante's departure. ''Niandra Lades and Usually Just a T-Shirt'' sold poorly upon its release in 1994, and was taken off the market, only to be re-released in 1999.

== Background ==
Frusciante joined the [[Red Hot Chili Peppers]] in 1988, at the age of 18, and released his first album with the group, ''[[Mother's Milk]]'' the following year. The follow-up album, ''[[Blood Sugar Sex Magik]]'', was recorded in an empty mansion that the band decided to live in for the duration of recording.<ref name=pp274>Kiedis, Sloman, 2004. pp. 274–275</ref> Frusciante adapted well to the environment, and often spent his time alone painting, listening to music, and recording songs that would eventually make up the first half of the album, ''Niandra Lades''.<ref name=pp274/> ''Blood Sugar Sex Magik'' was released on September 24, 1991 and was an instant success. The album peaked at number three in the U.S. and went on to sell over 12 million copies worldwide.<ref name=billboardrhcp>{{cite web|url={{BillboardURLbyName|artist=red hot chili peppers|chart=all}}|title=Red Hot Chili Peppers Albums Charting|accessdate=2007-09-12|publisher=''Billboard''}}</ref><ref name=RIAA>{{cite web|url=http://riaa.com/goldandplatinumdata.php?table=SEARCH|title=Certification search|accessdate=2007-09-21|publisher=RIAA}}</ref><ref>{{cite web|url=http://top40.about.com/od/discographies/a/redhotdisc.htm|title=Red Hot Chili Peppers discography|accessdate=2007-09-12|publisher=Top40}}</ref> Soon after the album's release, Frusciante developed a dislike of the band's newfound popularity. He felt that the band was too famous, and wished they were still playing small nightclubs like they were before he joined the group.<ref>Kiedis, Sloman, 2004. p. 229</ref> By his own admission, the band's rise to popularity took Frusciante by surprise, and he could not cope with it.<ref>{{cite web | last = Gabriella. | url = http://www.nyrock.com/interviews/rhcp_int.htm | title = "Interview with the Red Hot Chili Peppers". The Californication of John Frusciante | work = NY Rock |date=July 1999 | accessdate = 2007-09-11}}</ref> During ''Blood Sugar Sex Magik's'' promotional tour, Frusciante began using [[heroin]] and [[cocaine]] heavily.<ref name="tgm2002">Dalley, Helen (August 2002). "John Frusciante" ''Total Guitar Magazine''. Retrieved on August 27, 2007.</ref> He and vocalist [[Anthony Kiedis]] often argued before and after performances. According to Kiedis, Frusciante purposely sabotaged the ''[[Saturday Night Live]]'' performance of "[[Under the Bridge]]" by playing the wrong intro for the song and out of key.<ref>Kiedis, Sloman, 2004. p. 288</ref> His relationship with the band had become progressively more strained, and he abruptly quit during the Japanese leg of their world tour in 1992.<ref name=vh1btm>{{cite episode | url = http://www.vh1.com/shows/dyn/behind_the_music/51363/episode_about.jhtml |title=Red Hot Chili Peppers | series = [[Behind the Music]] | network = [[VH1]] | airdate = May 30, 1999 | accessdate=2007-08-27}}</ref>

== Writing and recording ==
After leaving the Red Hot Chili Peppers, Frusciante continued to write and record solo material. He had been doing so since the age of nine, but had never considered releasing his material to the public.<ref name=VPRO>[https://www.youtube.com/watch?v=WL6a9ZbGhEw VPRO Interview] with John Frusciante (1994)</ref> That was until several of his friends—including [[Johnny Depp]], [[Perry Farrell]], [[Gibby Haynes]] and former Red Hot Chili Peppers band mate [[Flea (musician)|Flea]]—encouraged him to release the material he wrote in his spare time during the ''Blood Sugar Sex Magik'' sessions.<ref name=VPRO/><ref name=bboard>Kenneally, Tim. (October 1, 1994) "Frusciante Steps Out With American Set". ''Billboard Magazine''</ref> Frusciante began working on final cuts of the songs he had been writing, and [[Record producer|producing]] them at his home in mid-1992. The production process, however, became hampered by his increasingly severe addiction to heroin. ''Usually Just a T-Shirt'' was recorded in the order it appears, with the final tracks being recorded shortly prior to Frusciante's departure from the Chili Peppers.<ref name=jfcom-niandra/> Frusciante's use of heroin and [[cocaine]] became more extreme during the final stages of recording in late 1993; he began viewing drugs as the only way to "make sure you stay in touch with beauty instead of letting the ugliness of the world corrupt your soul."<ref name=jfcom-niandra/><ref name=VPRO/>
{{Listen|filename=My Smile is a Rifle.ogg|title="My Smile is a Rifle"|description="My Smile is a Rifle" exemplifies the album's avant-garde lyrical themes complemented the record's disavowing of conventional song structure.}}

During a 1994 interview, a visibly intoxicated Frusciante noted that he wrote the album in order to create "interesting music", which he felt no longer existed. He felt contemporary artists were not writing material he deemed worth listening to and the mainstream population were settling for mediocrity.<ref name=VPRO/> Drugs were another significant topic Frusciante based ''Niandra Lades and Usually Just a T-Shirt'' on.<ref>Apter, 2004. p. 278</ref> According to Frusciante, he "was stoned for every single note [he] played on the album."<ref>{{cite web|url=http://www.invisible-movement.net/articles/1993-1997/1995-07-hightimes |title=Chilly Pepper &#124; John Frusciante unofficial website|publisher=Invisible-movement.net |date=2008-12-28 |accessdate=2011-08-15}}</ref>  He increased his drug use to cope with worsening depression that was caused by leaving the Red Hot Chili Peppers, and his subsequent isolation. Several songs on the album deal with his dislike for the Chili Peppers' success, such as the album's eleventh track, "Blood on My Neck From Success".<ref name=selvaggio>{{cite web|url=http://www.johnfrusciante.com/interviews/translation_mucchioselvaggio.pdf|title= John Frusciante: Perso e Ritrovato|format=PDF |publisher=johnfrusciante.com Il Mucchio. Selvaggio, No. 570|date=March 2004|accessdate=2007-08-20|archiveurl = https://web.archive.org/web/20070818105938/http://www.johnfrusciante.com/interviews/translation_mucchioselvaggio.pdf |archivedate = August 18, 2007|deadurl=yes}}</ref>

All of the music on the record was written by Frusciante, save for the cover of [[hardcore punk]] band [[Bad Brains]]' song "Big Takeover". The track was intentionally slowed down and recorded melodically because of a pastime in which Frusciante sang punk songs in different time signatures: "It was just something I had been walking around thinking of in my head. Sometimes I'll walk around singing punk rock songs to myself, but as if they were regular songs instead of punk rock songs, you know, slow it down and make a melody instead of just yelling them out. And then the idea occurred me to record it like a Led Zeppelin ballad with mandolins and stuff."<ref name=rockinfreak>Broxvoort, Brian (1994). "John Frusciante Goes Over a Bridge." ''Rockinfreakapotamus''.</ref> [[River Phoenix]], a friend of Frusciante's, had contributed guitar and backing vocals to two songs that were intended be included on the record, but they were ultimately left off due to protests from his family. These were later included under different names on the album ''[[Smile from the Streets You Hold]]'' in 1997.<ref name=rockinfreak/>

''Niandra LaDes and Usually Just a T-Shirt'' incorporated Frusciante's [[avant-garde]] style of song composition, with his [[Stream of consciousness writing|stream-of-consciousness]] methodology.<ref name=amgreview>{{cite web|url={{Allmusic|class=album|id=r220484|pure_url=yes}}|title=''Niandra Lades and Usually Just a T-Shirt'' review|author=Huey, Steve|publisher=Allmusic|accessdate=2007-11-22}}</ref><ref name=rollingstone>{{cite web|url=http://www.rollingstone.com/artists/johnfrusciante/albums/album/146562/review/5944990/niandra_lades__usually_just_a_tshirt|author=Hoard, Christian|title=''Niandra Lades and Usually Just a T-Shirt'' review|publisher=''Rolling Stone''|accessdate=2007-11-22|archiveurl = https://web.archive.org/web/20071128154800/http://www.rollingstone.com/artists/johnfrusciante/albums/album/146562/review/5944990/niandra_lades__usually_just_a_tshirt |archivedate = November 28, 2007|deadurl=yes}}</ref> He recorded, mixed, produced and mastered the entire record by himself, and released it on Rick Rubin's label, [[American Recordings (US)|American Recordings]].<ref name=jfcom-niandra/> [[Warner Bros. Records|Warner Bros.]], the Chili Peppers' label, originally held the rights to the album because of the leaving-artist clause in Frusciante's Chili Peppers contract. Because he was living as a recluse, however, the label gladly handed the rights over to Rubin, who released the album under his label.<ref name="pnt">{{cite web | last = Wilonsky | first = Robert | url = http://www.phoenixnewtimes.com/Issues/1996-12-12/music/music3.html | title = Blood on the Tracks | publisher = ''Phoenix New Times Music'' | date = December 12, 1996 | accessdate = 2007-06-22 |archiveurl = https://web.archive.org/web/20050907083433/http://www.phoenixnewtimes.com/issues/1996-12-12/music/music3.html |archivedate=September 7, 2005 |deadurl=yes}}</ref>

== Release, reception, and aftermath ==
{{Album reviews
| rev1 = [[PopMatters]]
|rev1Score = (favorable)<ref name=Pop />
| rev2 = [[AllMusic]]
|rev2Score = {{Rating|4|5}}<ref name="amgreview" />
| rev3 = [[Entertainment Weekly]]
|rev3Score = (B+)<ref>{{cite news |title=Niandra Lades and Unusally Just a T-Shirt : Music Review : Entertainment Weekly |url=http://www.ew.com/ew/article/0,,304409,00.html |date= 1994-11-11|work=[[Entertainment Weekly]]|accessdate=15 July 2009}}</ref>
| rev4 = [[High Times]]
|rev4Score = (favorable)<ref name=hightimes/>
| rev5 = [[Boston Herald]]
|rev5Score = (mixed)<ref name=bherald/>
| rev6 = [[Rolling Stone]]
|rev6Score = {{Rating|2|5}}<ref name=rollingstone />
}}<!--List Automatically Moved by DASHBot-->
''Niandra LaDes and Usually Just a T-Shirt'' was initially previewed by ''Billboard'' magazine, who said that "Chili Peppers fans might be daunted by the album's elusive experimentalism."<ref name=Apter>Apter, 2004. p. 279</ref> A representative of American Recordings did not foresee the album as being viable in any mainstream music stores, and some retailers went as far as to ban it from being sold.<ref name=Apter/> After the album was released, Frusciante played three small performances, and participated in a few magazine interviews to promote the album, explaining in one interview that people would only be able to understand his work if "their heads are capable of tripping out."<ref name=Apter/> At one point shortly after release, Frusciante began searching for a string quartet to play the album with him on tour. The idea was eventually discarded when he could not find a band that "understands why [[Ringo Starr]] is such a great drummer, can play [[Igor Stravinsky|Stravinsky]], and also smokes pot."<ref name=Apter/> The concept of a tour was ultimately abandoned as well, due to Frusciante's diminishing health.<ref name=Apter/>

''Niandra LaDes and Usually Just a T-Shirt'' was not widely reviewed, but yielded a generally positive response from critics. Steve Huey of [[AllMusic]], who rated the album four out of five stars, said that "[the album was] an intriguing and unexpected departure from Frusciante's work with the Chili Peppers", and that "the sparse arrangements of the first half help set the stage for the gossamer guitar work later on."<ref name=amgreview/> He went on to say that ''Usually Just a T-Shirt''—the latter half of the album—contained "pleasant psychedelic instrumentals with plenty of backward-guitar effects."<ref name=amgreview/> Ned Raggett, also of AllMusic, noted that "there's nothing quite so stunning as [Frusciante's] magnificent remake of [[Bad Brains]]' 'The Big Takeover'."<ref name=smileamg>{{cite web|url={{Allmusic|class=album|id=r312481|pure_url=yes}}|author=Raggett, Ned|title=''Smile From the Streets You Hold'' review|publisher=Allmusic|accessdate=2008-01-12}}</ref> Adam Williams of [[PopMatters]] said the album "fall[s] somewhere between madness and brilliance". He went on to compare Frusciante to [[Syd Barrett]], and felt it was a "hint at a deeply cerebral artist looking within for inspiration and creativity."<ref name=Pop>{{cite web|url=http://www.popmatters.com/review/frusciantejohn-niandra/|title=''Niandra Lades and Usually Just a T-Shirt'' review|author=Williams, Adam|accessdate=2007-12-30|publisher=PopMatters}}</ref> ''[[High Times]]''<nowiki>'</nowiki> Tim Kenneally saw the record as "a revelation, both disturbingly intimate and cryptically veiled. Ladeled straight out of the guitarist's stream of consciousness, it's worlds away from the up-front, balls-out funk assault of his former band," with "an ethereal, otherworldly quality."<ref name=hightimes>Kenneally, Tim (July, 1995). "Chilly Pepper." ''[[High Times]]''.</ref> The album received its share of negative criticism as well. ''[[Rolling Stone|Rolling Stone's]]'' Christian Hoard felt "Frusciante's eccentricities run seriously amok", and that " [the album] sounds like a string of four-track demos. The first part of the album is slightly more tuneful than the more ambient, experimental second section[...] Mostly what you get are Frusciante's acoustic-guitar scratchings and stream-of-conscious ramblings."<ref name=rollingstone/> The first ''Rolling Stone'' review of the record, however, was positive: "All in all, [the album is] a mess—but definitely a fascinating, often lovely mess. As one might expect of an album titled ''Niandra Lades and Usually Just a T-shirt'' this is twisted, cool stuff."<ref name=rsreview1>Wild, David (December, 1994). "As If We Needed One, Here's A Reminder of Dylan's Power." ''Rolling Stone''.</ref> The ''Boston Herald'' said that while the album was "a stark display of Frusciante's acoustic guitar virtuosity" and "eerily beautiful", the singing was "terrible; his high notes will drive the neighborhood dogs into a frenzy."<ref name=bherald>"Music Discs Dionne Farris' arresting vocals bloom into." ''Boston Herald''. December 9, 1994.</ref>

[[Image:JohnNiandraLades1994.jpg|thumb|Frusciante during a VPRO interview at his home in [[Venice, Los Angeles, California|Venice Beach]], [[California]] in 1994.]]Frusciante's drug addiction worsened as the years progressed. An article published by the ''[[New Times LA]]'' described him as "a skeleton covered in thin skin".<ref name="pnt"/> He participated in an interview with Dutch public broadcast station [[VPRO]]—the first media appearance he made since leaving the Chili Peppers.<ref name=VPRO/> In the interview Frusciante speaks of the positive effects drugs have had on his mind and proudly admits to being a "[[Substance dependence|junkie]]". He went on to confess addictions to heroin and crack cocaine, but ultimately described himself as being in the best health of his life.<ref name=VPRO/> In 1997, Frusciante released his second solo album ''[[Smile From the Streets You Hold]]'', primarily for drug money.<ref name = jfsmile/><ref name=AMG>{{cite web|url={{Allmusic|class=album|id=r312481|pure_url=yes}}|title=''Smile From the Streets You Hold'' review|author=Raggett, Ned|accessdate=2007-09-22|publisher=Allmusic}}</ref> ''Niandra Lades and Usually Just a T-Shirt'' was estimated to have sold only 45,000 copies when Frusciante ordered it out of print in 1998—when Frusciante rehabilitated and rejoined the Chili Peppers.<ref name=jfcom-niandra/> ''Smile From the Streets You Hold'' was withdrawn from the market a year later.<ref name=jfsmile>{{cite web|publisher=Johnfrusciante.com|url=http://www.johnfrusciante.com/music/smile.php|title=''Smile from the Streets You Hold''|accessdate=2006-09-04 |archiveurl=https://web.archive.org/web/20070928042137/http://www.johnfrusciante.com/music/smile.php |archivedate=2007-09-28}}</ref><ref name=AMG/> In 1999 ''Niandra Lades and Usually Just a T-Shirt'' was re-released on American Recordings.<ref name=jfcom-niandra/> In the early 2000s, Frusciante said he planned to re-release ''Smile From the Streets You Hold'' sometime in the future, but did not give any indication as to when.<ref>{{cite web|publisher=Johnfrusciante.com|url=http://www.johnfrusciante.com/news.php |title=A Little Message from John to the Fans|date=March 16, 2005|accessdate=2006-06-08 |archiveurl = https://web.archive.org/web/20071019051807/http://www.johnfrusciante.com/news.php |archivedate = 2007-10-19}}</ref> It was eventually re-released on American Records in Europe of 2006.<ref>Discogs</ref>

== Track listing ==
All songs written by John Frusciante, except where noted.

;''Niandra LaDes''
# "As Can Be"&nbsp;– 2:57
# "My Smile Is a Rifle"&nbsp;– 3:48
# "Head (Beach Arab)"&nbsp;– 2:05
# "Big Takeover" (cover of [[Bad Brains]])&nbsp;– 3:18
# "Curtains"&nbsp;– 2:30
# "Running Away into You"&nbsp;– 2:12
# "Ants" (Cassette only bonus track)&nbsp;– 2:23
# "Mascara"&nbsp;– 3:40
# "Been Insane"&nbsp;– 1:41
# "Skin Blues"&nbsp;– 1:46
# "Your Pussy's Glued to a Building on Fire"&nbsp;– 3:17
# "Blood on My Neck From Success"&nbsp;– 3:09
# "Ten to Butter Blood Voodoo"&nbsp;– 1:59

;''Usually Just a T-Shirt''  [commonly referred to as 'Untitled #1-13']
# <li value="13"> Untitled #0 (Cassette only bonus track) &nbsp;– 3:41
# Untitled #1&nbsp;– 0:34
# Untitled #2&nbsp;– 4:21
# Untitled #3&nbsp;– 1:50
# Untitled #4&nbsp;– 1:38
# Untitled #5&nbsp;– 1:30
# Untitled #6&nbsp;– 1:29
# Untitled #7&nbsp;– 1:42
# Untitled #8&nbsp;– 7:55
# Untitled #9&nbsp;– 7:04
# Untitled #10&nbsp;– 0:25
# Untitled #11&nbsp;– 1:51
# Untitled #12&nbsp;– 5:27
# Untitled #13&nbsp;– 1:52

== Personnel ==
* [[John Frusciante]]&nbsp;- vocals, guitar, bass, piano, mandolin, banjo, clarinet, producer
* Toni Oswald&nbsp;- vocals (Untitled #8, Untitled #9) (Uncredited)
* Martyn Atkins&nbsp;- art direction and design
* Dirk Walter&nbsp;- design

== References ==
* {{cite book
 | last = Apter
 | first = Jeff
 | authorlink =
 | title = Fornication: The Red Hot Chili Peppers Story
 | date = 2004
 | publisher = [[Omnibus Press]]
 | isbn = 1-84449-381-4
 }}
* {{cite book
 | last = Kiedis
 | first = Anthony
 | authorlink = Anthony Kiedis
 |author2=Sloman, Larry
  | title = [[Scar Tissue (book)|Scar Tissue]]
 | date = 2004
 | publisher = [[Hyperion (publisher)|Hyperion]]
 | isbn = 1-4013-0101-0
 }}

== Notes ==
{{reflist|30em}}
{{John Frusciante}}
{{featured article}}

{{DEFAULTSORT:Niandra LaDes And Usually Just A T-Shirt}}
[[Category:American Recordings (record label) albums]]
[[Category:English-language albums]]
[[Category:John Frusciante albums]]
[[Category:1994 debut albums]]