{{Multiple issues|
{{primary sources|date=February 2015}}
{{advert|date=April 2016}}
{{COI|date=May 2016}}
}}
[[File:GreyNet logo.jpg|right]]
'''GreyNet International''', the '''Grey Literature Network Service''' is an independent organization founded in 1992, which is dedicated to research, publication, [[open access]], education, and public awareness to [[grey literature]]. Grey literature is often defined as "information produced and distributed on all levels of government, academics, business and industry in electronic and print formats not controlled by commercial publishing i.e. where publishing is not the primary activity of the producing body.".<ref>Luxembourg, 1997 – Expanded in New York, 2004</ref>

GreyNet is corporate author of the [[Proceedings]] issuing from the International Conference Series on Grey Literature, The Grey Journal, An International [[Journal]] on Grey Literature, as well as other types of publications such as reports, program books, and newsletters. GreyNet also maintains a [[Listserv]] and a presence on a number of social media including [[LinkedIn]], [[Netvibes]], [[Twitter]], and [[Facebook]].

GreyNet is a not for profit organisation fostering the production and dissemination of [[scientific literature]]. It is also engaged in the [[open source]] movement and was invited to the 10th Libre Software Meeting 2009<ref>{{cite web|url=http://2009.rmll.info/?lang=en |title=Home – 10th LSM from 7 to 11 July 2009 in Nantes |publisher=2009.rmll.info |date= |accessdate=2015-05-09}}</ref> in Nantes, France, with a communication on [[knowledge sharing]] in the field of [[grey literature]].<ref>{{cite web|url=http://2009.rmll.info/Greynet,791.html |title=Greynet – 10es RMLL à Nantes du 7 au 11 juillet 2009 |publisher=2009.rmll.info |date= |accessdate=2015-05-09}}</ref>

During the 11th International Conference on Grey Literature December 2009, GreyNet signed a Partnership Agreement with ICSTI, International Council for Scientific and Technical Information.<ref>{{cite web|author=&nbsp; |url=http://www.icsti.org/ |title=Icsti.Org |publisher=Icsti.Org |date= |accessdate=2015-05-09}}</ref> This newly established partnership lends to GreyNet a multilateral base, elevating it from a bilateral one that it already shares with a number of ICSTI Members. GreyNet seeks to provide ICSTI with an opportunity to further broaden its information activities to the social sciences and humanities.

== International Conference Series on Grey Literature (ISSN 1386-2316)<ref>{{cite web|url=http://greyguide.isti.cnr.it/index.php/gl-program-books-2003|title=GL Program Books (2003 - …)}}</ref> ==
* 1993 GL1 Amsterdam, “GL’93, Weinberg Report 2000” {{hdl|10068/698053}}
* 1995 GL2 Washington D.C. ”GL’95, Grey Exploitations in the 21st Century” {{hdl|10068/698012}}
* 1997 GL3 Luxembourg, “GL’97, Perspectives on the Design and Transfer of STI” {{hdl|10068/697932}}
* 1999 GL4 Washington D.C., “GL’99, New Frontiers in Grey Literature” {{hdl|10068/697891}}
* 2003 GL5 Amsterdam, “Grey Matters in the World of Networked Information”  {{hdl|10068/697754}}
* 2004 GL6 New York, “Work on Grey in Progress” {{hdl|10068/697756}}
* 2005 GL7 Nancy, France “Open Access to Grey Resources” {{hdl|10068/697757}}
* 2006 GL8 New Orleans, “Harnessing the Power of Grey” {{hdl|10068/697758}}
* 2007 GL9 Antwerp, “Grey Foundations in Information Landscape” {{hdl|10068/697759}}
* 2008 GL10 Amsterdam, “Designing the Grey Grid for Information Society” {{hdl|10068/697786}}
* 2009 GL11 Washington D.C., “The  Grey  Mosaic: Piecing It All Together”
* 2010 GL12 Prague, "Transparency in Grey Literature, Grey Tech Approaches to High Tech Issues"
* 2011 GL13 Washington D.C., "The Grey Circuit, From Social Networking to Wealth Creation", Library of Congress, December 5–6
* 2012 GL14 Rome, Italy, "Tracking Innovation through Grey Literature", National Research Council, CNR, November 29–30
* 2013 GL15 Bratislava, Slovak Republic, "The Grey Audit, A Field Assessment in Grey Literature", December 2–3
* 2014 GL16 Washington D.C. “Grey Literature Lobby, Engines and Requesters for Change”, December 8–9
* 2015 GL17 Amsterdam, “A New Wave of Textual and Non-Textual Grey Literature”, December 1–2

== Other publications ==
[[File:The_Grey_Journal.jpg]]
''The Grey Journal'', TGJ an International Journal on Grey Literature (ISSN print 1574–1796, ISSN e-print 1574-180X) was launched in 2005. It  is the only journal on the topic. It appears three times a year in  thematic issues, published  in print and electronic formats. Articles from the electronic version at an article level are available via EBSCO’s LISTA-FT Database ([[EBSCO Publishing]]). ''The Grey Journal'' is indexed by the [[Scopus]] scientific database and other [[Indexing and abstracting service]]s.

'''The Grey Journal, International Journal on Grey Literature'''<ref>{{cite web|url=http://greyguiderep.isti.cnr.it/listtitoli.php?authority=GreyGuide&collection=TGJ&langver=en&RighePag=100|title=Grey_Guide – PUblication MAnagement|publisher=}}</ref>
 TGJ Volume 12, Number 1, Spring 2016 Mining Textual and Non-Textual [[Data Source]]s
 TGJ Volume 12, Number 2, Summer 2016 Convergence and Change in Grey Literature
 TGJ Volume 11, Number 1, Spring 2015 Raising [[Awareness]] to Grey Literature
 TGJ Volume 11, Number 2, Summer 2015 Publishing, Licensing, and [[Open Access]]
 TGJ Volume 11, Number 3, Autumn 2015 Topical and Technical Advances in Grey Literature
 TGJ Volume 10, Number 1, Spring 2014 Sustaining Good Practices in Grey Literature
 TGJ Volume 10, Number 2, Summer 2014 Research Communities And Data Sharing
 TGJ Volume 10, Number 3, Autumn 2014 Weighing up Public Access to Grey Literature
 TGJ Volume 9,  Number 1, Spring 2013 Adapting New Technologies for Grey Literature
 TGJ Volume 9,  Number 2, Summer 2013 Tracking Grey Literature Across Disciplines
 TGJ Volume 9,  Number 3, Autumn 2013 Improving Grey Literature through [[Innovation]]
 TGJ Volume 8,  Number 1, Spring 2012 Social Networking and Grey Literature
 TGJ Volume 8,  Number 2, Summer 2012 Data Frontiers in Grey Literature
 TGJ Volume 8,  Number 3, Autumn 2012 Managing Change in Grey Literature
 TGJ Volume 7,  Number 1, Spring 2011 [[Transparency (science)|Transparency]] in Grey Literature
 TGJ Volume 7,  Number 2, Summer 2011 System Approaches to Grey Literature
 TGJ Volume 7,  Number 3, Autumn 2011 Research and Education in Grey Literature
 TGJ Volume 6,  Number 1, Spring 2010 Government [[Alliance]] to Grey Literature
 TGJ Volume 6,  Number 2, Summer 2010 Shared [[Strategies]] for Grey Literature
 TGJ Volume 6,  Number 3, Autumn 2010 Research on Grey Literature in Europe
 TGJ Volume 5,  Number 1, Spring 2009 Paperless Initiatives for Grey Literature
 TGJ Volume 5,  Number 2, Summer 2009 [[Archaeology]] and Grey Literature
 TGJ Volume 5,  Number 3, Autumn 2009 Trusted Grey Sources and Resources
 TGJ Volume 4,  Number 1, Spring 2008 Praxis and Theory in Grey Literature
 TGJ Volume 4,  Number 2, Summer 2008 Access to Grey in a Web Environment
 TGJ Volume 4,  Number 3, Autumn 2008 Making Grey more Visible
 TGJ Volume 3,  Number 1, Spring 2007 Grey [[Technical standard|Standard]]s in Transition and Use
 TGJ Volume 3,  Number 2, Summer 2007 Academic and Scholarly Grey
 TGJ Volume 3,  Number 3, Autumn 2007 Mapping Grey Resources
 TGJ Volume 2,  Number 1, Spring 2006 Grey Matters for OAI
 TGJ Volume 2,  Number 2, Summer 2006 Collections on a Grey Scale
 TGJ Volume 2,  Number 3, Autumn 2006 Using Grey to Sustain [[Innovation]]
 TGJ Volume 1,  Number 1, Spring 2005 Publish Grey or Perish
 TGJ Volume 1,  Number 2, Summer 2005 [[Institutional repository|Repositories]] – Home2Grey
 TGJ Volume 1,  Number 3, Autumn 2005 Grey Areas in Education

== GreyNet and the OpenGrey Repository ==
For the past 20 years, GreyNet has sought to serve researchers and authors in the field of grey literature. To further this end, GreyNet has signed on to the OpenGrey repository and in so doing seeks to preserve and make openly available research results originating in the International Conference Series on Grey Literature. GreyNet together with [[INIST]]-[[CNRS]] have designed the format for a metadata record, which encompasses standardized PDF attachments of the full-text conference preprints, PowerPoint presentations, abstracts, biographical notes, and post-publication commentaries. GreyNet's collection of over 270 conference preprints is both current and comprehensive.
Comment from Peter Suber, ''Open Access News'' (Thursday, January 29, 2009): ''GreyNet started making its conference proceedings OA through its repository in May 2008. I applaud its determination to complete the collection retroactively, even if it means buying permission from a publisher. Note to other conference organizers: This is a reason to self-archive your proceedings as you go, or at least to retain the right to self-archive them without a fee.''<ref>{{cite web|last=Suber |first=Peter |url=http://www.earlham.edu/~peters/fos/2009/01/greynet-buys-rights-to-deposit-papers.html |title=Peter Suber, Open Access News |publisher=Earlham.edu |date=2009-01-29 |accessdate=2015-05-09}}</ref>

== GreyNet and the DANS Data Archive ==
GreyNet’s research data is cross-linked to the corresponding conference preprint in OpenGrey via the DANS Data Archive. In this way these results can further serve the international grey literature community, where open access to research data has now become a prerequisite.<ref>{{cite web|url=https://easy.dans.knaw.nl/ui/?wicket:bookmarkablePage=:nl.knaw.dans.easy.web.search.pages.PublicSearchResultPage&q=greynet |title=Published datasets – EASY |publisher=Easy.dans.knaw.nl |date= |accessdate=2015-05-09}}</ref>

== Web-based Resources in Grey Literature: GreySource, GreyText, IDGL, WHOIS ==

'''GreySource''' provides examples of grey and malin-grey literature to the average net-user (see for instance the University of Queensland list <ref>{{cite web|url=http://www.library.uq.edu.au/training/types_information_sources.html#grey |title=Types of Information Sources &#124; UQ Library |publisher=Library.uq.edu.au |date=2015-03-10 |accessdate=2015-05-09}}</ref>) and in so doing profiles organizations responsible for its production and/or processing. Only web-based resources that explicitly refer to the term grey literature (or its equivalent in any language) are listed. GreySource identifies the hyperlink directly embedded in a resource, thus allowing immediate and virtual exposure to grey literature.

The web-based resources appear within categories derived from the COSATI (American) and [[SIGLE]] (European) Classification Systems. The few changes that have been introduced into the classification scheme are intended to facilitate the search and retrieval of net-users. New examples are welcome and will be indexed in GreySource.

'''GreyText''' is an inhouse archive of documents on grey literature. Over 125 documents are indexed by first author followed by the title, source, date of publication and length in printed pages. Free Access to the first page of each document is available for browsing. Also, in most cases the corresponding PowerPoint is online available. The full-text of all documents listed in GreyText are accessible in PDF via email on demand.

'''IDGL''', International Directory of Organizations in Grey Literature provides a list of some 170 organizations in more than 30 countries worldwide that are currently associated with GreyNet either via partnership, membership, sponsorship, or authorship in the field of grey literature. Entries are alphabetical by country and each entry has an embedded link to the corresponding organization's website. GreyNet International is proud in serving the grey literature community and welcomes additions and revisions to this Directory.<ref>{{cite web|url=http://www.greynet.org/internationaldirectory.html|title=International Directory of Organizations in Grey Literature|publisher=}}</ref>

'''WHOIS''' in the Field of Grey Literature is a compilation of over 350 biographical notes provided by authors in the International Conference Series on Grey Literature and The Grey Journal. This online resource is maintained by TextRelease, the Program and Conference Bureau. Records in this directory appear in alphabetical order by last name of author and each record contains a current email address.<ref>{{cite web|url=http://www.textrelease.com/whois2013.html|title=WHOIS in the field of Grey Literature|publisher=}}</ref>

== Education and Curriculum Development ==
In 2007 GreyNet conducted an international survey on grey literature among instructors in LIS higher education. In the same year, GreyNet implemented a distant education course on grey literature at the [[University of New Orleans]]. {{hdl|10068/697878}}

In 2009, GreyNet began the Annual Summer Workshop Series "'''GreyWorks'''", which is now in its 7th year. This series seeks to highlight the state of the art in grey literature by focussing on strategies, [[benchmarking|benchmarks]], good practices, [[mind maps]], and [[transparency (science)|transparency]] in this field of [[library and information science]].

In 2013, GreyNet began the '''GreyForum''', a Thematic Series of Onsite and Online Seminars and Workshops in the field of Grey Literature. Topics dealt with in this series have dealt with [[information ethics]], information rights, [[digital preservation]], and policy development<ref>{{cite web|url=http://www.greynet.org/researchandeducation/workshops.html|title=Grey Literature - GreyNet Workshops and Seminars|publisher=}}</ref>

== GreyNet management ==
The GreyNet Service is powered by [http://www.textrelease.com/TextRelease TextRelease], an independently owned company based in Amsterdam. TextRelease is GreyNet's program and conference bureau. TextRelease was responsible for GreyNet’s relaunch in 2003. Content compiled and edited within the Grey Literature Network Service is published and marketed via TextRelease. Furthermore, all agreements, contracts, and legal matters pertaining to GreyNet are handled through TextRelease.

== TextRelease and GreyNet ==
TextRelease’s main activities are situated in the domain of grey scientific & technical information. Conference organization, information consultancy, research, publication, as well as education and training in the field of grey literature are among its objectives. In this capacity, TextRelease re-launched the Grey Literature Network Service (GreyNet) in 2003 and is the publishing body for this organization.

TextRelease provides Non-Exclusive Rights Agreements to nearly 300 authors, who have published in its Conference Proceedings and/or International Journal on Grey Literature. It maintains an up-to-date ”WHOIS in the field of Grey Literature”.

The TextRelease website likewise serves as the conference site for the International Conference Series on Grey Literature. This web resource provides direct access to Conference Announcements, Call-for-Papers, Official Programs, Registrations, and other conference related information.

== See also ==
* [[European Association for Grey Literature Exploitation]] (EAGLE)
* [[Grey Literature International Steering Committee]] (GLISC)
* [[OpenSIGLE]]
* [[System for Information on Grey Literature in Europe]] (SIGLE)
* [[Grey literature]]
* GreyNet LinkedIn Group

== Bibliography ==
* Farace D. & Schöpfel J. (eds.) (2010). [http://www.degruyter.de/cont/imp/saur/detailEn.cfm?id=IS-9783598117930-1&fg=BB-01 Grey Literature in Library and Information Studies]. De Gruyter Saur.
* Schöpfel J., Stock C., Farace D.J., Frantzen J.   Citation Analysis and Grey Literature: Stakeholders in the Grey Circuit. The Grey Journal 2005, vol. 1, n° 1, p.&nbsp;31-40. {{hdl|10068/697850}}
* Farace D., Frantzen J., Schöpfel J., Stock C.  Knowledge Generation in the Field of Grey Literature: A Review of Conference-based Research Results]. GL8 Conference Proceedings. Eighth International Conference on Grey Literature: Harnessing the Power of Grey. New Orleans, 4–5 December 2006. {{hdl|10068/697768}}
* Farace D.J., Frantzen J., Schöpfel J., Stock C.  Grey Literature: A Pilot Course constructed and implemented via Distance Education. The Grey Journal 2008, vol. 4, n° 1, p.&nbsp;41-45. {{hdl|10068/697878}}
* Farace D., Frantzen J., Schöpfel J., Stock C., Henrot N. OpenSIGLE, [http://www.greynet.org/greytextarchive.html Home to GreyNet’s Research Community and its Grey Literature Collections: Initial Results and a Project Proposal]. GL10 Conference Proceedings. Tenth International Conference on Grey Literature: Designing the Grey Grid for Information Society. Amsterdam, 8–9 December 2008.
* Gelfand J. Interview with Dominic Farace, founder of GreyNet. International Journal on Grey Literature. 2000, vol. 1, n° 2, p.&nbsp;73-76. ''Covers how Dominic Farace, the GreyNet director, first became involved in the grey literature scene, and explains how and why the Grey Literature Network Service has developed. Discusses the future prospects of GreyNet and grey literature. Highlights many of the issues concerning the GreyNet movement and looks at Farace’s inspiration for his career therein.''
* [http://www.thefreelibrary.com/Grey+literature.-a0156005203 Canadian Dental Hygienists Association Staff: Grey Literature]. May 2006.  ''(GreyNet Listserv) is an internationally moderated list that seeks to facilitate communication between organizations involved in the field of grey literature. It also provides an extensive listing of resources by category.''
* Matthews B. [http://wikis.ala.org/acrl/index.php/Gray_Literature Gray literature]. Association of College and Research Libraries (ACRL) wiki. June 2007.  Citation: ''(The GreyNet Listserv) international moderated list seeks “to facilitate dialog and communication between persons and organisations in the field of grey literature.” In addition to the electronic lists, the site includes information about the International Conference Series on Grey Literature and provides an extensive categorical listing of resources.''
* J. Schöpfel & D. J. Farace (2010). `Grey Literature'. In M. J. Bates & M. N. Maack (eds.), Encyclopedia of Library and Information Sciences, Third Edition, pp.&nbsp;2029–2039. CRC Press.
* [http://www.websearchguide.ca/netblog/archives/006008.html Grey literature]. Internet News wiki. April 2007. Citation: ''GreyNet facilitates the study and collection of grey literature through its source index and text archive. The GreyText Archive has articles about grey literature.''

== References ==
{{Reflist}}

== External links ==
* [http://www.greynet.org/ GreyNet]
* [http://www.textrelease.com/ TextRelease]
* [http://www.inist.fr/ INIST-CNRS]
* [http://www.icsti.org/ ICSTI]
* [http://www.nvbonline.nl/4687/prijswinnaars_victorine_van_schaickprijs_nvb_2008.html?PHPSESSID=db99305ba99830983abf225c75379e9c Victorine van Schaick Prijs NVB 2008]
* [http://www.oss.net/extra/page/?action=page_show&id=232&module_instance=1 Golden Candle Award 2000]
* [http://www.greylit.org/about New York Academy of Medicine]

[[Category:Grey literature]]
[[Category:Academic publishing]]
[[Category:Open access (publishing)]]
[[Category:Archival science]]
[[Category:Organizations established in 1992]]