{{Good Article}}
{{Infobox song	
| Name     = Around the World
| Artist   = [[Christina Aguilera]]
| Album    = [[Lotus (Christina Aguilera album)|Lotus]]	
| track_no = 10
| Recorded = 2012
| Format   = [[Music download|Digital download]]
| Genre    = [[Pop music|Pop]]
| Length   = {{Duration|m=3|s=24}}
| Label    = [[RCA Records|RCA]]
| Writer   = Christina Aguilera, [[Supa Dups|Dwayne Chin-Quee]], [[Jason Gilbert]], [[Ali Tamposi]]
| Producer = [[Supa Dups]], Jason Gilbert
}}

"'''Around the World'''" is a song by American recording artist [[Christina Aguilera]] from her seventh studio album, ''[[Lotus (Christina Aguilera album)|Lotus]]'' (2012). It was written by Aguilera and [[Ali Tamposi]] and was co-written and produced by [[Supa Dups]] and [[Jason Gilbert]]. The song is an uptempo [[pop music|pop]] track, with crashing drums and a thumping bassline. The song is about how Aguilera wants to make love in different countries around the world with her lover. It also refers to her 2001 single "[[Lady Marmalade#Moulin Rouge! cover|Lady Marmalade]]", where she whispers the lyrics "Voulez-vous coucher avec moi, se soir?". "Around the World" received mixed reviews from [[Music journalism|music critics]]; some thought it was fun, while others felt it was faceless. Upon the release of ''Lotus'', the song debuted and peaked at number 158 on the [[Gaon Chart|South Korean International singles chart]].

==Background==
Following the release of her sixth studio album, ''[[Bionic (Christina Aguilera album)|Bionic]]'' (2010),<ref name="Idolator1">{{cite news|last=Bain|first=Becky|url=http://idolator.com/6836852/christina-aguilera-your-body-single-listen|title=Christina Aguilera's Demo Of New Single 'Your Body' Surfaces: Listen|work=''[[Idolator (website)|Idolator]]''|publisher=[[Buzz Media]]|date=August 23, 2012|accessdate=September 14, 2012}}</ref> Aguilera filed for divorce from her husband [[Jordan Bratman]], starred in her first feature film, ''[[Burlesque (2010 American film)|Burlesque]]'' and recorded [[Burlesque: Original Motion Picture Soundtrack|the accompanying soundtrack]].<ref name="Billboard Cover Story">{{cite news|last=Hampp|first=Andrew|url=http://www.billboard.com/articles/news/474983/christina-aguilera-billboard-cover-story|title=Christina Aguilera: Billboard Cover Story|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|date=September 21, 2012|accessdate=September 23, 2012}}</ref> She then became a coach on [[NBC]]'s singing competition show ''[[The Voice (U.S. TV series)|The Voice]]''<ref name="Billboard Cover Story"/> and appeared as a featured artist on [[Maroon 5]]'s single "[[Moves like Jagger]]" (2011), which spent four weeks atop the US ''[[Billboard (magazine)|Billboard]]'' [[Billboard Hot 100|Hot 100]] chart.<ref name="Billboard1">{{cite news|last=Schneider|first=Marc|url=http://www.billboard.com/articles/news/496285/christina-aguilera-cee-lo-hit-the-studio|title=Christina Aguilera, Cee Lo Hit the Studio|work=Billboard|publisher=Prometheus Global Media|date=April 11, 2012|accessdate=September 14, 2012}}</ref> Following these events, Aguilera announced that had plans to begin production of her seventh album, stating that she wanted high quality and "personal" songs for the record.<ref name="Billboard1"/> Regarding the creative direction, she revealed that the album would be a "culmination of everything I've experienced up until this point&nbsp;... I've been through a lot since the release of my last album, being on ('The Voice'), having had a divorce&nbsp;... This is all sort of a free rebirth for me."<ref name="Yahoo1">{{cite news|last=Elber|first=Lynn|url=http://music.yahoo.com/news/christina-aguilera-album-rebirth-121427390.html|title=Christina Aguilera: New album is a 'rebirth'|work=[[Yahoo! Music]]|publisher=[[Yahoo!]]|accessdate=September 14, 2012|date=August 28, 2012}}</ref> She further said "I'm embracing many different things, but it's all feel-good, super-expressive [and] super-vulnerable."<ref name="Yahoo1"/> Aguilera continued to say that the album would be about "self&ndash;expression and freedom" because of the personal struggles she had overcome during the last couple of years.<ref name="LATimes1">{{cite news|last=Kennedy|first=Gerrick D.|url=http://articles.latimes.com/2012/sep/13/entertainment/la-et-ms-christina-aguilera-readies-new-album-lotus-20120913|title=Christina Aguilera readies new album 'Lotus'|work=[[Los Angeles Times]]|publisher=[[Eddy Hartenstein]]|date=September 13, 2012|accessdate= October 5, 2012}}</ref> Speaking about her new material during an interview on ''[[The Tonight Show with Jay Leno]]'' in 2012, Aguilera said that the recording process for ''Lotus'' was taking a while because "I don't like to just get songs from producers. I like them to come from a personal place&nbsp;... I'm very excited. It's fun, exciting, introspective, it's going to be great".<ref>{{cite news|url=http://www.digitalspy.co.uk/music/news/a373459/christina-aguilera-new-album-is-quality-over-quantity.html|title=Christina Aguilera: 'New album is quality over quantity'|work=[[Digital Spy]]|publisher=[[Hearst Corporation]]|date=May 27, 2012|accessdate= October 5, 2012}}</ref>

==Recording and composition==
{{Listen
 |filename     = 10-christina_aguilera-around_the_world.ogg 
 |title        = "Around the World"
 |pos          = left
 |description  = An 19 second sample of "Around the World".
}}

"Around the World" was co-written by Aguilera with [[Supa Dups|Dwayne Chin-Quee]], [[Jason Gilbert]] and Ali Thompson. It was produced by [[Supa Dups]] and it was co-produced by Gilbert. Aguilera's vocals were recorded at The Red Lips Room in [[Beverly Hills, California]] by Oscar Ramirez.<ref name="Notes">{{cite AV media notes|others=Christina Aguilera|title=[[Lotus (Christina Aguilera album)|Lotus]]|year=2012|type= inlay cover|publisher= [[RCA Records]]|page=iTunes Digital Booklet}}</ref> It is a [[pop music|pop]] song with a [[ragga]] flavour, and runs for a duration of three minutes and 24 seconds.<ref name="scotsman"/><ref name="LotusGBiTunes">{{cite web|url=https://itunes.apple.com/gb/album/lotus-deluxe-version/id571725689|title=iTunes – Music – Lotus by Christina Aguilera|work=[[iTunes Store]] (GB)|publisher=[[Apple Inc.|Apple]]|date=November 9, 2012|accessdate=November 24, 2012}}</ref> Its [[instrumentation]] contains crashing drums and "a thumping bassline".<ref name="4music"/> According to Chris Younie from [[4Music]], the song's chorus is "insanely catchy".<ref name="4music"/> Lyrically, it talks about Christina wanting to have [[sexual intercourse]] with someone in different locations around the world, including [[Hollywood]] and [[Japan]].<ref name="4music"/><ref name="popjustice"/> The track also refers to her 2001 hit "[[Lady Marmalade#Moulin Rouge! cover|Lady Marmalade]]", where Aguilera whispers the lyrics "Voulez-vous coucher avec moi, ce soir?".<ref name=digitalspyfirstlisten>{{cite news|last=Copsey|first=Robert|title=Christina Aguilera's new album 'Lotus': First listen|url=http://www.digitalspy.co.uk/music/thesound/a435214/christina-aguileras-new-album-lotus-first-listen.html|accessdate=November 2, 2012|work=Digital Spy|publisher=Hearst Corporation|date=November 2, 2012}}</ref>
{{clear}}

== Critical reception ==
[[File:Rihanna, LOUD Tour, Minneapolis-2.jpg|thumb|right|130px|"Around the World" received comparisons to songs recorded by [[Rihanna]].<ref name=observer/><ref name=montreal/><ref name=avclub/>]]
"Around the World" received generally mixed reviews from [[music journalism|music critics]]. Chris Younie of [[4Music]] gave the song a favorable review, calling it a "party-like song of lust and desire" and has "an insanely catchy chorus".<ref name=4music>{{cite news|last=Younie|first=Chris|title=News: Review: Christina Aguilera - Lotus|url=http://www.4music.com/news/news/6712/Review-Christina-Aguilera-Lotus|accessdate=November 2, 2012|work=[[4Music]]|publisher=[[Channel 4]]|date=November 2, 2012}}</ref> ''[[The Scotsman]]'' critic Fiona Sheperd wrote that the song has "the most resounding chorus on the album".<ref name=scotsman>{{cite news|last=Shephard|first=Fiona|date=November 12, 2012|url=http://www.scotsman.com/lifestyle/music/news-and-features/album-review-christina-aguilera-lotus-1-2628530|title=Album review &ndash; Christina Aguilera &ndash; Lotus|work=[[The Scotsman]]|location=Edinburgh|accessdate=December 1, 2012}}</ref> Mike Wass of [[Idolator (website)|Idolator]] gave it a mix review, writing it is "another inoffensive adventure that scores extra points for quoting lyrics from 'Lady Marmalade'", however it "lacks the big pop hook need to wreak havoc on the charts".<ref name=idolator>{{cite news|last=Wass|first=Mike|title=Christina Aguilera's 'Lotus': Album Review|url=http://idolator.com/7001572/christina-aguilera-lotus-album-review|accessdate=December 9, 2012|work=Idolator|publisher=Buzz Media|date=November 13, 2012}}</ref> Writing on behalf of  ''[[The New York Times]]'', Jon Caramanica called it "gauche and aesthetically vulgar in the way Ms. Aguilera once proudly was".<ref name=nytimes>{{cite news|last1=Caramanica|first1=Jon|last2=Pareles|first2=Jon|authorlink2=Jon Pareles|last3=Ratliff|first3=Ben|date=November 13, 2012|url=https://www.nytimes.com/2012/11/13/arts/music/albums-by-christina-aguilera-soundgarden-and-brian-eno.html?_r=0|title=Albums by Christina Aguilera, Soundgarden and Brian Eno|page=34|newspaper=[[The New York Times]]|publisher=[[The New York Times Company]]|accessdate=November 17, 2012}}</ref>

Andrew Hampp of ''[[Billboard (magazine)|Billboard]]'' was mixed, writing that, "Sadly, the second-verse reference to 'Lady Marmalade' remains one of the few moments of fun on this otherwise tepid track".<ref name="BillboardLotusReview">{{cite web|last=Hampp|first=Andrew|url=http://www.billboard.com/articles/review/1066766/christina-aguilera-lotus-track-by-track-review|title=Christina Aguilera, 'Lotus': Track-By-Track Review|work=Billboard|publisher=Prometheus Global Media|date=November 12, 2012|accessdate=November 27, 2012}}</ref> Sam Hine of [[Popjustice]] called it "pop filler" and "it prompts his headache to really kick in".<ref name=popjustice>{{cite news|last=Hine|first=Sam|title=Christina's 'Lotus': a review|url=http://www.popjustice.com/blogs/etc/christinas-lotus-a-review/104438/|accessdate=February 3, 2013|work=[[Popjustice]]|date=November 13, 2012}}</ref> Several critics compared "Around the World" to songs performed by Barbadian recording artist [[Rihanna]]. Kitty Empire of ''[[The Observer]]'' called it a "come-hither tune", also writing that it is a "Rihanna-copping".<ref name=observer>{{cite news|last=Empire|first=Kitty|authorlink=Kitty Empire|url=https://www.theguardian.com/music/2012/nov/11/christina-aguilera-lotus-review|title=Christina Aguilera: Lotus – review|work=[[The Observer]]|location=London|at=The New Review section, p. 29|date=November 10, 2012|accessdate=November 17, 2012}}</ref>  T'cha Dunlevy, a writer for the ''[[Montreal Gazette]]'', agreed with EMpire's sentiments, calling it "a blatant Rihanna ripoff".<ref name=montreal>{{cite news|last=Dunlevy |first=T'cha |title=New music review: Lotus, Christina Aguilera (RCA/Sony) |url=http://blogs.montrealgazette.com/2012/11/12/new-music-review-lotus-christina-aguilera-rcasony/ |accessdate=February 6, 2013 |work=[[The Gazette (Montreal)]] |date=November 12, 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20121116071552/http://blogs.montrealgazette.com:80/2012/11/12/new-music-review-lotus-christina-aguilera-rcasony/ |archivedate=November 16, 2012 |df= }}</ref> Annie Zaleschi of ''[[The A.V. Club]]'' echoed the same thought, calling it a "Rihanna reject".<ref name=avclub>{{cite news|last=Zaleski|first=Annie|url=http://www.avclub.com/articles/christina-aguilera-lotus,88558/|title=Christina Aguilera: Lotus|work=[[The A.V. Club]]|publisher=The Onion, Inc|location=Chicago|date=November 13, 2012|accessdate=November 17, 2012}}</ref>

== Credits and personnel ==
;Recording
*Vocals recorded at The Red Lips Room, Beverly Hills, California

;Personnel
*Songwriting&nbsp;– Christina Aguilera, Dwanye Chin-Quee, Jason Gilbert, Ali Tamposi
*Production&nbsp;– Dwayne "Supa Dups" Chin-Quee, Jason "JG" Gilbert {{small|(co-producer)}}
*Vocal recording&nbsp;– Oscar Ramirez

Credits adapted from the liner notes of ''Lotus'', [[RCA Records]].<ref name="Notes"/>

==Charts==
Upon the release of ''Lotus'', "Around the World" debuted on the [[Gaon Singles Chart|South Korean International download singles chart]] at number 158 during the week of November 11 to 17, 2012, due to digital download sales of 1,958.<ref name="SouthKoreaDebut53">{{cite web|url=http://gaonchart.co.kr/digital_chart/download.php?nationGbn=E&current_week=47&current_year=2012&chart_Time=week |title=''Gaon'' International Download Chart |work=[[Gaon Music Chart]] |date=November 11–17, 2012 |accessdate=December 16, 2013 |language=Korean |publisher=Korea Music Content Industry Association |deadurl=yes |archiveurl=https://web.archive.org/web/20131216091440/http://gaonchart.co.kr/digital_chart/download.php?nationGbn=E&current_week=47&current_year=2012&chart_Time=week |archivedate=December 16, 2013 |df= }}</ref>

{| class="wikitable plainrowheaders" style="text-align:center;"
|-
!scope="col"| Chart (2012)
!scope="col"| Peak<br />position
|-
! scope="row"| South Korea ([[Gaon Music Chart|Gaon]])<ref name="SouthKoreaDebut53"/>
| 158
|}

==References==
{{Reflist|30em}}

==External links==
{{Wikipedia books|Lotus (album)|''Lotus'' (album)}}
* {{MetroLyrics song|christina-aguilera|around-the-world}}<!-- Licensed lyrics provider -->

{{Christina Aguilera songs}}

[[Category:2012 songs]]
[[Category:Christina Aguilera songs]]
[[Category:Songs written by Christina Aguilera]]
[[Category:Songs written by Ali Tamposi]]
[[Category:Songs about sexuality]]
[[Category:Songs written by Jason Gilbert]]