{{Use Australian English|date=May 2013}}
{{Use dmy dates|date=May 2012}}
{{Infobox airport
| name = Maitland Airport
| nativename = Russell Field
| nativename-a =
| nativename-r =
| image =
| image-width =
| caption =
| IATA = MTL
| ICAO = YMND
| type = Public
| owner-oper = Royal Newcastle Aero Club
| city-served = Maitland, Lower Hunter Valley
| location = [[Rutherford, New South Wales|Rutherford]], [[New South Wales]], [[Australia]]
| elevation-f = 85
| coordinates = {{coord|32|42|12|S|151|29|18|E|region:AU-NSW|display=inline,title}}
| pushpin_map = New South Wales
| pushpin_label = YMND
| pushpin_map_caption = Location in New South Wales
| website =
| metric-rwy = Y
| r1-number = 05/23
| r1-length-m = 1,244
| r1-surface = [[Asphalt]]
| r2-number = 08/26
| r2-length-m = 1,023
| r2-surface = Asphalt
| r3-number = 18/36
| r3-length-m = 422
| r3-surface = Grass
| stat-year =
| stat1-header =
| stat1-data =
| stat2-header =
| stat2-data =
| footnotes = Sources: Australian [[Aeronautical Information Publication|AIP]] and aerodrome chart<ref name="AIP">{{AIP AU|YMND|name=Maitland}}, [http://www.airservicesaustralia.com/aip/current/dap/MNDAD01-130.pdf Aeronautical Chart]</ref>
}}

'''Maitland Airport''', also known as '''Russell Field''' {{Airport codes|MTL|YMND}} is a [[general aviation]] airport located in the suburb of [[Rutherford, New South Wales|Rutherford]], approximately {{convert|5|km|abbr=on}} from [[Maitland, New South Wales|Maitland]] in the [[Australia]]n state of [[New South Wales]]. There are currently no airline services, with the airfield catering mostly to general aviation and recreational category aircraft. The airport has been owned and operated by the [[Royal Newcastle Aero Club]] since 1963 and shares a large training area with the nearby [[Cessnock Airport]]. Throughout its history, the airport has played host to many airshows, races and flying competitions. The field is named for the fifth President of the Royal Newcastle Aero Club, Robert Russell, who suffered a fatal heart attack while on the premises in 1966.

== History ==
In 1948, the Rutherford site was chosen by Maitland Aero Club to build an airfield. By 1957, the club was defunct and the land acquired by the flourishing Royal Newcastle Aero Club. Both organisations shared close ties, with the former club having been formed by RNAC members. By 1963 urban development meant that the [[Broadmeadow Aerodrome]] site was no longer viable as the RNACs base of operations and all headquarters and flying activities were relocated to Maitland Airport. A second runway was added in 1968, and sealed in 1972. The main runway, 05/23 was sealed in 1977. Additional land was purchased by the club to the east and west of the airport between 1977 and 1979 to create buffer zones and limit development on the fringes.<ref>{{cite web|url=http://www.rnac.com.au/list.php?s_id=418|title=About RNAC » Royal Newcastle Aeroclub History|publisher=Royal Newcastle Aero Club|date=21 March 2010|accessdate=16 May 2012}}</ref>

During the 1980s, a local company called Club Air linked the airport with Sydney using two 14 seat Australian built [[GAF Nomad]] aircraft. These services were discontinued after the company was purchased by Wings Australia and the aircraft retired around 1990.<ref>{{cite web|url=http://www.pprune.org/dunnunda-godzone-pacific/23067-broken-hill-nomad.html|title=Broken Hill Nomad – PPRuNe Forums|work=Pprune.org|accessdate=16 May 2012}}</ref> [[Yanda Airlines]] also offered commuter flights between Maitland and Sydney from 1988 <ref>{{cite web|url=http://www.aph.gov.au/Parliamentary_Business/Committees/House_of_Representatives_Committees?url%3Dtrs/aviation/report/chap2.pdf+yanda+airlines+maitland+-+sydney&hl=en&gl=au&pid=bl&srcid=ADGEEShXECh2yImxh5xpPX3N85dH7KSCuUKksQ0ylp0vheQQCANgWbUPQ3Iv5K4bsS7AIsnewguyXvt8ZmyrB9fjczT0x3_8Vv0VnnMrJVWy-WqnTN2CRBSGIJCL_4hyEju-2Dz0PL3f&sig=AHIEtbQeQqYtr30mrFklatvMve0ly448GQ|title=House of Representatives Committees|publisher=Parliament of Australia|accessdate=16 May 2012}}</ref> until the airline's Air Operators Certificate was suspended by the [[Civil Aviation Safety Authority]] in 2001 and the company wrapped up.<ref>{{cite web|url=http://www.casa.gov.au/scripts/nc.dll?WCMS:STANDARD::pc=PC_91780|title=Civil Aviation Safety Authority – CASA media release – NSW airline grounded|publisher=[[Civil Aviation Safety Authority]]|accessdate=16 May 2012}}</ref>

== Airport operations and facilities ==
The main runway used for arrivals and departures is 05/23, with a paved surface {{convert|1244|by|15|m|abbr=on|0}}.<ref name="AIP"/> This runway is equipped with low intensity runway lighting available to pilots by prior arrangement for night operations. The airport is restricted to operations between 0600–2300 September to April and between 0630–2300 May to August. Aircraft refuelling is available during operating hours. There is no control tower and pilots are required to co-ordinate aircraft movements using a [[Common Traffic Advisory Frequency]] (CTAF).<ref name="AIP"/> The nearest navigation aid for aircraft is the West Maitland [[Non-directional beacon|NDB]]  installation {{convert|3.8|NM|lk=in}} to the south-east.<ref name="AIP"/>

The airfield's primary user is the Royal Newcastle Aero Club who provide aircraft hire and general aviation flight training, in addition to the management and maintenance of the facilities. The airfield and club constitute an [[Recreational Aviation Australia]] approved flight training facility for recreational pilots.<ref>[http://www.auf.asn.au/docs/ops/ftf_list.pdf Flight Training Facilities]</ref> The RNAC offer scenic joyflights over the Hunter region and regularly host to flying competitions such as the [[Australian Light Aircraft Championships]]. Several vintage aircraft associated with the club are based at, or regular visitors to Russell Field. [[Luskintyre Airfield]], approximately {{convert|15|km|abbrev=on}} away specialises in the restoration of vintage aircraft but does not have refuelling facilities. Because of this many aircraft visiting Luskintyre transit through Maitland for fuel.

Action Aerobatics are based at the airport and offer advanced aerobatic flight training, as well as aerobatic joy flights.<ref>{{cite web|url=http://www.aerobatics.com.au/index.php|title=Action Aerobatics|work=Aerobatics.com.au|accessdate=16 May 2012}}</ref> In June 2011, a development application was approved by [[City of Maitland|Maitland City Council]] to allow Newcastle Helicopters to establish a training school at the airport, in addition to existing rescue and fire-fighting helicopter operations.<ref>[http://www.maitland.nsw.gov.au/MeetingsAgendas/ViewDocumentFile.aspx?id=152255 Maitland City Council]</ref>

== Accidents and incidents ==
* On 27 November 1984, a [[Beechcraft Baron]] 58, registration VH-ETV, operating a charter flight to the airport, suffered a left landing gear failure and was substantially damaged. The aircraft departed the runway surface and came to rest on the grass. None of the occupants were seriously injured.<ref>[http://www.atsb.gov.au/media/32727/aair198401425.pdf Aviation Safety Investigation Report 198401425]</ref>
* On 22 March 2006, a [[Eurocopter AS350]] Squirrel helicopter crashed during a training flight. The helicopter came to rest on its side after control was lost at an altitude approximately {{Convert|7|m|abbr=on}} above the ground. Neither the pilot or instructor were injured in the accident.<ref>{{cite web|url=http://www.pprune.org/rotorheads/218128-helicopter-crash-maitland-nsw-2.html|title=Helicopter Crash Maitland NSW – Page 2 – PPRuNe Forums|work=Pprune.org|accessdate=16 May 2012}}</ref>

==See also==
* [[List of airports in New South Wales]]

== References ==
{{reflist|30em}}

{{Hunter Region places and items of interest}}

{{Airports in New South Wales}}

[[Category:Airports in New South Wales]]
[[Category:RA-Aus Approved Flight Training Facilities]]
[[Category:Air transport in the Hunter Region]]
[[Category:Maitland, New South Wales]]
[[Category:Airports established in 1948]]