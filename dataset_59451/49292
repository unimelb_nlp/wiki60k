{{Use dmy dates|date=June 2015}}
{{Infobox film
| name           = Life! Camera Action...
| image          = RNGFilms.jpg
| caption       = Movie poster
| director       = [[Rohit Gupta]]
| producer       = Rohit Gupta
| executive producer       = [[Rohit Gupta]]<br>Parag Vaishnav
| writer         = Rohit Gupta<br>Amanda Sodhi
| starring       = {{Plainlist|
* [[Dipti Mehta]]
* [[Shaheed Woods]]
* John Crann
* Swati Kapila
* Suneet Kochar
}}
| music          = Manoj Singh<!--music supervisor-->
| cinematography = Ravi Kumar R
| editing        = Rohit Gupta
| studio         = Dot and Feather Entertainment
| distributor    = Viva Entertainment, USA<br>[[Flipkart]] (dvd), India<br>[[Peter Gerard|Distrify]] (online worldwide)
| released       = {{Film date|2012|07|20|df=y}} United States
| country        = United States
| language       = English<br>Hindi<br>Punjabi
| runtime        = 90 minutes

}}

'''''Life! Camera Action...''''' is a [[Drama film|family-drama film]] directed, written, edited, produced by [[Rohit Gupta]] in his directorial debut.<ref name="rohit">{{cite news|title=IMDB|url=http://www.imdb.com/title/tt1682931/}}</ref><ref>{{cite news|title=Life! Camera Action... in FYE, Amazon, HMV|url=http://www.celebmirror.com/2012/11/24/life-camera-action-in-fye-amazon-hmv-canada.html|accessdate=24 November 2012|newspaper=Celeb Mirror|date=24 November 2012}}</ref> Starring [[Dipti Mehta]], [[Shaheed Woods]],<ref>{{cite web|title=Shaheed Woods on Internet Movie Database|url=http://www.imdb.com/name/nm3801249/|website=Imdb.com|accessdate=25 June 2014}}</ref> Noor Naghmi,<ref>{{cite web|title=Noor Naghmi on IMDB|url=http://www.imdb.com/name/nm2214591/|website=Imdb.com|publisher=Internet Movie Database|accessdate=25 June 2014}}</ref> Swati Kapila,<ref name="swati kapila">{{cite news|title=Swati Kapila NRI Actress Making India Proud by editorial |url=http://articles.timesofindia.indiatimes.com/2011-08-18/cinema/29899990_1_film-industry-rohit-life-camera-action|accessdate=18 August 2011|newspaper=The Times of India|date=18 August 2011}}</ref> John Crann,<ref>{{cite web|title=John Crann in Life! Camera Action...|url=http://www.imdb.com/media/rm2421533440/nm3110296|publisher=Internet Movie DB}}</ref> this ninety minutes quasi-autobiographical film follows a girl in pursuit of her dreams of becoming a filmmaker against all odds. The film received several awards and nominations.<ref name="NRI Pulse">{{cite news |title=Movie Making is Magic: Rohit Gupta by Jyothsna Hegde, Atlanta, USA.|url=http://www.nripulse.com/movie-making-is-magic-rohit-gupta/|accessdate=28 May 2013|newspaper=NRI Plus|date=28 May 2013}}</ref><ref>{{cite news|title=Urban Asian – Rohit Gupta's Life! Camera Action... at the International Film Festival Manhattan, 2011 |url=http://urbanasian.com/bollywood/2011/11/rohit-guptas-life-camera-action/|accessdate=15 November 2011|newspaper=Urban Asian|date=15 November 2011}}</ref><ref name="TOI">{{cite news|title=Rohit Gupta's films sweep top ten prestigious awards |url=http://articles.timesofindia.indiatimes.com/2012-04-30/cinema/31505976_1_films-popular-awards-amanda-sodhi|accessdate=30 April 2012|newspaper=Times of India|date=30 April 2012}}</ref><ref>{{cite news |title=New York Film Academy Graduate Rohit Gupta Nominated for Several Awards at World Music and Independent Film Festival |url=http://www.nyfa.edu/film-school-blog/new-york-film-academy-graduate-rohit-gupta-nominated-for-several-awards-at-world-music-and-independent-film-festival/|accessdate=25 April 2011|newspaper=New York Film Academy|date=25 April 2011}}</ref><ref>{{cite news|title=Rohit Gupta's Life! Camera Action... selected to screen at IIDFF in Greece |url=http://www.bollywoodtrade.com/film-festivals-and-markets/rohit-guptas-life-camera-action-to-screen-at-iidff-in-greece/f9ad7ac7-e960-4425-8fed-7e264844a2d5/index.htm|accessdate=14 September 2012|newspaper=Bollywood Trade.com |date=14 September 2012}}</ref> Its theatrical release was limited to [[film festivals]]. The film received [[direct-to-video|direct-to-DVD]] release and subsequently on other [[video on demand]] (VOD) platforms.<ref>{{cite news|title=LIFE! CAMERA ACTION... most-awarded film releases on DVD by Bollywood Trade Editorial.|url=http://www.bollywoodtrade.com/movie-reviews/life-camera-action-most-awarded-film-releases-on-dvd/6BAB5B47-6144-41BE-B17E-A3BDBE46D28D/index.htm|accessdate=21 April 2014|newspaper=Bollywood Trade|date=10 November 2012}}</ref><ref>{{cite news|title=LIFE! CAMERA ACTION... Independent feature film releases on the world's largest social media platform |url=http://www.washingtonbanglaradio.com/content/74448312-life-camera-action-independent-feature-film-releases-world-s-largest-social-media-p|accessdate=21 April 2014|newspaper=WBRI|date=24 July 2012}}</ref>

''Life! Camera Action...'' is of note for its extremely low filming budget ($4000), production value, philosophical inference, and its message widely praised.<ref>{{cite news|title=Follow your own path|url=http://timesofindia.indiatimes.com/business/small-business/case-studies/Movie-business-is-one-ofthe-most-beautiful-businesses-this-universe-has-gifted-to-mankind-Rohit-Gupta/wells_fargo_show/36382923.cms|accessdate=27 December 2015|publisher=TOI|date=11 June 2014}}</ref><ref>{{cite news|title=Life! Camera Action...in DIFF|url=http://www.bindaaskhabar.com/details_life!_camera_action_in_diff_307.html|accessdate=14 June 2014|publisher=Bindaas Khabar|date=26 December 2011}}</ref> Silicon India listed it as "One of the 10 Outstanding Movies by [[Indian American]] Filmmakers".<ref>{{cite news|last=Silicon|first=India|title=10 Outstanding Movies by Indian American Film Makers|url=http://www.siliconindia.com/news/usindians/10-Outstanding-Movies-by-Indian-American-Film-Makers--nid-119597-cid-49.html|date=12 June 2012}}</ref> It made its way to the [[Limca Book of Records]], India's equivalent of the [[Guinness World Records]] for being the first full-length motion picture "shot by just a two-member crew".<ref name="Urban Asian">{{cite news|title=Filmmaker Rohit Gupta honored by the Limca Book of Records by Roopa Modha – Urban Asian|url=http://urbanasian.com/bollywood/2014/01/filmmaker-rohit-gupta-honored-by-the-limca-book-of-records/|accessdate=2 January 2014|newspaper=Urban Asian|date=2 January 2014}}</ref><ref name="Times of India">{{cite news|title=Filmmaker Rohit Gupta honored by the Limca Book of Records – TOI NRI Achievers|url=http://timesofindia.indiatimes.com/nri/nri-achievers/Filmmaker-Rohit-Gupta-honored-by-the-Limca-Book-of-Records/articleshow/28793511.cms|accessdate=17 January 2014|newspaper=The Times of India|date=14 January 2014}}</ref>

==Plot==
''Life! Camera Action...'' is an inspiring story of Reina, who sets off to pursue a career in filmmaking without the consent of her parents. She is threatened to be disowned if she insists on her choice instead of doing what usually is the norm – become a doctor, engineer or an architect. At the center of this family drama, Reina is faced with a hard decision: She must give up her dreams to keep her parents happy or go against their wishes and follow her own path.

==Cast==
* [[Dipti Mehta]] as Reina
* [[Shaheed Woods]] as Mike
* John Crann as Professor Ed
* Swati Kapila as Simi
* Suneet Kochar as Actress<ref>{{cite news|title=Suneet Kochar on Life! Camera Action...|url=http://www.washingtonbanglaradio.com/content/64418912-interview-actress-suneet-kochar-award-winning-film-life-camera-action-and-more|accessdate=30 June 2012|newspaper=WBRI USA|date=30 June 2012}}</ref>
* Chelsi Stahr as producer Teri<ref>{{cite web|title=Actress Chelsi Stahr|url=http://www.imdb.com/name/nm0821529/|publisher=Internet Movie Database}}</ref>
* Subodh Batra as Reina's father
* Noor Naghmi<ref>{{cite news|title=Noor Naghmi wins Best Supporting Actor|url=http://articles.timesofindia.indiatimes.com/2011-08-25/cinema/29926541_1_feature-film-first-international-award-chalo|newspaper=Times of India|date=25 August 2011}}</ref>
* Prabha Batra as Reina's mother
* Nina Mehta as Actress<ref name="nina mehta">{{cite news|title=Actress Nina Mehta talks about Life! Camera Action...|url=http://www.washingtonbanglaradio.com/content/64403112-interview-actress-nina-mehta-rohit-guptas-award-winning-feature-film-life-camera-ac|accessdate=18 June 2012|newspaper=WBRI, USA|date=18 June 2012}}</ref>
* Bhavesh Patel as Patel DVD store owner

==Production==
===Development===
''Life! Camera Action...'' started as Rohit Gupta's ten-minute short film assignment at the [[New York Film Academy]]. Shortly after he dropped out of the academy to make it into a full-length feature film.<ref>{{cite news|title=Los Angeles-based Amanda Sodhi wins Best Screenplay award|url=http://www.nritoday.net/profiles/988-la-based-amanda-sodhi-wins-best-screenplay-award|accessdate=18 July 2014|publisher=NRI Today|date=May 2012}}</ref> Amanda Sodhi, the co-writer of the film, in response to an advertisement had originally submitted a fifteen-page short film script titled "The Last Shot" loosely based on her own life.<ref>{{cite web|title=Amanda Sodhi|url=http://www.imdb.com/name/nm3967222/|publisher=Internet Movie Database}}</ref> Gupta expanded upon the content, calling the extended version ''Life! Camera Action...'' with ''The Last Shot''  becoming a part of it.<ref>{{cite news|title=Indian born Amanda Sodhi wins Best Screenplay award|url=http://articles.timesofindia.indiatimes.com/2012-05-14/cinema/31700718_1_short-film-first-film-amanda-sodhi|accessdate=14 May 2012|newspaper=Times of India|date=14 May 2012}}</ref> 

===Filming===
The film was shot in guerilla style on [[Panasonic AG-DVX100]] in ten days and roughly edited on Gupta's [[Mac Book Pro]].<ref name="BT">{{cite news|title=Rohit Gupta's Life! Camera Action... to screen at IIF, Florida by BT editorial|newspaper=BT|date=8 February 2011}}</ref> It was filmed on an initial budget of around $4000 with money raised from his savings that was cleaned up with [[post-production]] work costing several thousand dollars before its marketing and release. In an interview [[Dipti Mehta]] has said that in an informal meeting before the audition, Gupta thought she was not right for the part, although she was invited at the audition where he failed to recognize her at the first sight. Mehta did get the part and it became her debut film as a lead.<ref name="washingtonbanglaradio.com">{{cite news|title=Interview: Up Close with DIPTI MEHTA – "Best Actress" Award Winner for "LIFE! CAMERA ACTION..." |url=http://www.washingtonbanglaradio.com/content/54354212-interview-close-dipti-mehta-best-actress-award-winner-life-camera-action|newspaper=WBRI, USA|date=5 May 2012}}</ref>

===Filming locations===
Various locations in New York City, [[New York Film Academy]], [[Jersey City]] and [[Newport, Jersey City|Newport]] Waterfront.

===Music===
The background score and songs were composed by Manoj Singh.<ref>{{cite web|title=Manoj Singh|url=http://www.imdb.com/name/nm3981009/|publisher=Internet Movie Database}}</ref> The music was produced in Mumbai, India. The following songs are featured in the film and used as a part of the background score.

* "Hain Yeh Kaisa Safar" (What kind of Journey is this) lyrics by Amanda Sodhi.<ref>{{cite web|title=Tracks|url=http://www.imdb.com/title/tt1682931/soundtrack?ref_=tt_trv_snd|publisher=IMDB}}</ref>
* "Chalte Jaana Hain" (Have to keep Walking) lyrics by Rohit Gupta performed by [[KK (singer)|KK]].<ref>{{cite web|title=Singer KK, Writer Rohit Gupta, Music Manoj Singh|url=http://www.imdb.com/title/tt1682931/soundtrack?ref_=tt_trv_snd|publisher=IMDB}}</ref>

==Release==
[[File:Dipti Mehta.jpg|thumb|200px|right|Actress Dipti Mehta at the ''World Premiere'' in New York]]
''Life! Camera Action...'' opened in 35 [[film festivals|international film festivals]] beginning with its world premiere at the [[Indo-American Arts Council#New York Indian Film Festival (NYIFF)|New York Indian Film Festival]].<ref>{{cite news|title=World premiere of 'Life! Camera Action...'|url=http://zeenews.india.com/entertainment/hollywood/world-premiere-of-life-camera-action_73380.html|accessdate=14 June 2014|issue=Entertainment|publisher=Zee News|date=10 October 2010}}</ref> Thereafter the film went on to receive numerous accolades around the world before it received a limited DVD distribution deal on 29 October 2012 by New Jersey-based ViVa Entertainment for the Asian Indian market spanning the North American continent.<ref>{{cite news|title=Viva Entertainment to distribute Life! Camera Action...|url=http://www.celebmirror.com/2012/10/24/oh-my-god-distributor-acquires-north-american-distribution-rights-to-multiaward-winning-film-life-camera-action.html|accessdate=24 October 2012|newspaper=Celebmirror|date=24 October 2012}}</ref> It was initially released via Facebook on 20 July 2012 by Boston-based idistribute Inc., a new media distribution company.<ref>{{cite news|title=Life! Camera Action... releases via facebook|url=http://www.indiapost.com/life-camera-action-on-facebook/|accessdate=27 July 2012 |newspaper=Indian Post|date=27 July 2012}}</ref> On 29 November 2012, the film received another retail DVD distribution platform via Florida-based Dynasty Records through [[Trans World Entertainment Corporation]]'s national mall-based stores under the brand name [[F.y.e.|FYE]] (For Your Entertainment), Suncoast and Wherehouse.com.<ref>{{cite news|title=Life! Camera Action... DVD release|url=http://www.indiantvtoday.com/life-camera-action-dvd-on-flipkart/2013/02/11/|accessdate=18 July 2014|publisher=India TV today|date=11 February 2013}}</ref> [[Flipkart]], India's largest e-commerce consumer company released the film on 17 February 2013.<ref>{{cite web|title=Life! Camera Action... available on Flipkart|url=http://www.flipkart.com/life-camera-action/p/itmdhmkte5j9fynh|publisher=Flipkart|accessdate=29 March 2014}}</ref> On [[International Women's Day]], 8 March 2013 the film was released worldwide through Distrify, a UK-based VOD movie distribution company for online streaming and movie download in any global currency.<ref>{{cite news|title=A film every filmmaker must watch|url=http://www.washingtonbanglaradio.com/content/34665313-rohit-guptas-life-camera-action-film-every-filmmaker-must-watch-released-world-wide|accessdate=8 March 2013|newspaper=WBRI|date=8 March 2013}}</ref><ref>{{cite web|title=Online Film Distribution Company|url=http://distrify.com/|publisher=Distrify}}</ref> The film was released to other VOD service such as the [[Amazon Video]], [[BoxTV.com|BoxTV]].<ref>{{cite news|title=Life! Camera Action... in F.Y.E, Amazon, HMV Canada.|url=http://www.indiantvtoday.com/life-camera-action-in-f-y-e-amazon-hmv-canada/2012/11/25/|accessdate=24 June 2014|publisher=India TV Today|date=25 November 2012}}</ref>

==Reception==
[[File:LCA IFFPIE 2012.jpg|thumb|upright|200px|left|Wins ''Best Feature Film'' at the Official World Peace Film Festival in Jakarta, Indonesia]]
{{quote box|align=right|width=20% |quote="it's not just good filmmaking, it's real intelligent filmmaking".|source=''[[The Times of India]]''<ref>{{cite news|title=Rohit Gupta's film sweep top 10 prestigious awards|url=http://articles.timesofindia.indiatimes.com/2012-04-30/cinema/31505976_1_films-popular-awards-amanda-sodhi|newspaper=Times of India|date=30 April 2012}}</ref>}}
''Life! Camera Action...'' received critical acclaim. It was invited  at various international film festivals including the [[New York Indian Film Festival]]; [http://willifest.com/2011/films/ Williamsburg International Film Festival (WILLiFest) in New York];<ref>{{cite web|title=Williamsburg International Film Festival 2011 Screening Schedule, New York|url=http://willifest.com/2011/films|publisher=WiLLiFest}}</ref> [http://www.msfilm.org/ Mississippi International Film Festival], USA;<ref>{{cite web|title=Officially Selected Film Screening Schedule Mississippi International Film Festival, USA |url=http://www.mississippifilmfest.com/mainsite/Uploads/2011/03/FINAL_FINAL_SCHEDULE_2011.pdf|publisher=Mississippi International Film Festival}}</ref> [http://www.cviff.org/#/ Cabo Verde International Film Festival (CVIFF), Africa];<ref>{{cite web|title=Guia Oficial do Festival Cabo Verde International Film Festival (CVIFF) in Western Africa|url=http://issuu.com/vivaimagens/docs/guia_do_cviff?mode=window&backgroundColor=%23222222|publisher=Cabo Verde Int Film Festival}}</ref> [http://www.iffab.org/screenings.html/ International Film Festival Antigua & Barbuda];<ref>{{cite web|title=Official Screenings and nominations Antigua & Barbuda International Film Festival|url=http://iffab.org/screenings.html#barbuda|publisher=Antigua Barbuda}}</ref> [http://www.atamerica.or.id/events/696/International-Film-Festivals-for-Peace-Inspiration-and-Equality/ Official World Peace Film Festival, Indonesia];<ref>{{cite news|title=Life! Camera Action.. wins Best Feature Film in Indonesia International Film Festival Peace, Inspiration & Equality aka Official World Peace Film Festival in Indonesia|url=http://articles.timesofindia.indiatimes.com/2012-09-12/cinema/33788726_1_damien-dematra-feature-film-rohit-gupta|newspaper=Times of India|date=12 September 2012}}</ref> [[Golden Door Film Festival]] in USA;<ref>{{cite web|title=Winners 2011 Golden Door Film Festival in USA|url=http://goldendoorfilmfestival.org/archives/4372|publisher=Golden Door Film Festival of Jersey City}}</ref> [http://www.iidff.gr/index.php/en/festival-en/ Ionian International Film Festival in Greece];<ref>{{cite web|title=Official Screening Schedule Ionian International Digital Film Festival in Greece|url=http://www.iidff.gr/index.php?option=com_content&view=article&id=76%3Alifecameraaction&catid=39%3Ascreeningfeatures2012&Itemid=73&lang=en|publisher=Ionian International Digital Film Festival}}</ref> [[India International Film Festival (IIFF) of Tampa Bay|Indian International Film Festival of Tampa Bay]]; Carmarthen Bay International Film Festival in United Kingdom;<ref>{{cite web|title=Winners and nominees 2012 Carmarthen Bay International Film Festival in United Kingdom|url=http://www.carmarthenbayfilmfestival.co.uk/page7.html|publisher=Carmarthen Bay}}</ref> [http://www.tcifilmfest.com/index.html/ Treasure Coast International Film Festival, USA];<ref>{{cite web|title=Screening Schedule Treasure Coast International Film Festival, USA|url=http://www.tcifilmfest.com/Films---Screenings.html|publisher=TCIFF}}</ref> The Social Uprising Resistance and Grass Root Encouragement International Film Festival,in the United States;<ref>{{cite news|title=Life! Camera Action... Tag line The Social Uprising Resistance and Grass Root Encouragement International Film Festival in the United States|url=http://www.celebmirror.com/2012/01/24/life-camera-action-new-tag-line-for-oscars-2012.html|accessdate=24 January 2012|newspaper=Celebmirror|date=24 January 2012}}</ref>[[National Film Development Corporation of India|NFDC]] Film Bazaar in [[Goa]]; [[Beloit International Film Festival]];<ref>{{cite web|title=Life! Camera Action...Showtimes 2012 Beloit International Film Festival|url=http://beloitfilmfest.org/life-camera-action/|publisher=Beloit Int Film Festival}}</ref><ref>{{cite news|title=BIFF to screen Life! Camera Action twice|url=http://www.indiantelevision.com/aac/y2k12/aac216.php|accessdate=25 June 2014|publisher=Indian Television news|date=18 February 2012}}</ref> [https://filums.lums.edu.pk// Filums (LUMS International Film Festival)], [[Pakistan]];<ref>{{cite news|title=Life! Camera Action... to showcase in Pakistan Filums (LUMS International Film Festival)|url=http://articles.timesofindia.indiatimes.com/2012-02-09/cinema/31041624_1_film-festivals-annual-film-project-film-credits|accessdate=9 February 2012|newspaper=TOI|date=9 February 2012}}</ref> [[Boston's Museum of Fine Arts]] (MFA) South Asian Film Series, USA;<ref>{{cite web|title=Life! Camera Action... to open South Asian Film Series Boston's Museum of Fine Arts (MFA) South Asian Film Series, USA|url=http://www.mfa.org/programs/film/life-camera-action|publisher=Museum of Fine Arts, Boston}}</ref> Heart of England International Film Festival; [[Dhaka International Film Festival]] (Cinema of the World section), Bangladesh;<ref>{{cite web|title=2012 Lineup Dhaka International Film Festival (Cinema of the World section)|url=http://www.dhakafilmfest.org/index.php?option=com_content&view=article&id=66&Itemid=73|publisher=Dhaka Int. Film Festival}}</ref> [https://legacymediainstituteinterna2013.sched.org/list/descriptions/type/feature+film/ Legacy Media Institute International Film Festival, Virginia];<ref>{{cite news|title=Rohit Gupta's "Life! Camera Action..." to screen at the Legacy Media Institute in Virginia by Roopa Modha|url=http://urbanasian.com/bollywood/2013/11/life-camera-action-to-screen-at-the-legacy-media-institute-in-virginia/|newspaper=Urban Asian|date=8 November 2013}}</ref> [http://www.delhiinternationalfilmfestival.com/catalouge/#ffs-tabbed-21/ Delhi International Film Festival, India];<ref>{{cite news|title=Life! Camera Action... to screen at DIFF2012|url=http://www.bollywoodtrade.com/film-festivals-and-markets/life-camera-action-to-be-screened-at-diff-2012/index.htm|newspaper=BT}}</ref> International Youth Film Festival in the UK; [[Silent River Film Festival]], USA;<ref>{{cite news|title=Life! Camera Action... Movie screening and meet the Director Silent River Film Festival, USA|url=http://events.pe.com/irvine_ca/events/show/209074246-exclusive-offer-life-camera-action-movie-screening-and-meet-the-director|newspaper=The Press Enterprise}}</ref> and others.

==Awards and nominations==
The film received various accolades including some of the highest honors at its festival run, with the nominations categories mainly ranging from recognition of the film itself (Best Film) to its direction, Film editing, music, screenwriting to the cast's acting performance, mainly Dipti Mehta (Best Actress), Swati Kapila (Best Supporting Actress),<ref>{{cite news|title=Award Nominated American Actress Swati Kapila|url=http://www.washingtonbanglaradio.com/content/64419012-interview-award-nominated-american-actress-swati-kapila-award-winning-feature-film-|accessdate=30 June 2012|newspaper=WBRI, USA|date=30 June 2012}}</ref> Noor Naghmi (Best Supporting Actor)<ref>{{cite web|title=Noor Naghmi wins Best Supporting Actor at the World Music and Independent Film Festival, Washington, D.C.|accessdate=25 June 2014}}</ref> and Shaheed Woods (Best Actor).

[[File:Noor Naghmi-Award.jpg|thumb|upright|200px|right|Actor Noor Naghmi wins ''Best Supporting Actor'' in Washington D.C.]]
[[File:Shaheed Woods.jpg|thumb|right|200px|Actor Shaheed Woods and Actress Dipti Mehta at the ''New Jersey Premiere'']]

{| class="wikitable" style="font-size: 95%;"
|- style="text-align:centr;"
! style="background:#cce;"| Festival/Award
! style="background:#cce;"| Location
! style="background:#cce;"| Category
! style="background:#cce;"| Recipients and nominees
! style="background:#cce;"| Outcome
|-
|rowspan="4"|[http://accoladecompetition.org/success-stories/rohit-gupta-the-just-do-it-filmmaker/ Accolade Film Awards]<ref>{{cite web|title=Accolade Winners 2010|url=http://www.accoladecompetition.org/Jud.aspx?art=_90|publisher=Accolade Film Awards}}</ref>
|rowspan="4"|USA
|Award of Excellence&nbsp;– Best Feature Film
|''Life! Camera Action...''
|{{won}}
|-
|Award of Merit for Direction
|Rohit Gupta
|{{won}}
|-
|Award of Merit for Cinematography
|Ravi Kumar R.,Rohit Gupta
|{{won}}
|-
|Award of Merit for Dramatic Impact
|''Life! Camera Action...''
|{{won}}
|-
|rowspan="2"|[[Action On Film International Film Festival]]<ref>{{cite web|title=2011 Official Award nominees|url=http://www.aoffest.com/pdf/AOF-2011-Film-Video-Award-Winners.pdf|publisher=Action on Film}}</ref>
|rowspan="2"|USA
|Best Composition
|Manoj Singh
|{{nom}}
|-
|Best Art Direction
|Ravi Kumar R, Rohit Gupta
|{{nom}}
|-

||[[California Film Awards]]<ref>{{cite web|title=Winners 2011|url=http://www.calfilmawards.com/Awards/2011_winners/2011_grand_winners.aspx|publisher=California Film Awards}}</ref>
||USA
|Orson Welles Award for Best Narrative Film
|''Life! Camera Action...''
|{{won}}
|-
||[[Canada International Film Festival]]<ref>{{cite web|title=Canada International Film Festival Winners & Nominees 2012|url=http://www.canadafilmfestival.com/Festival/PastWinners/2012/tabid/472/Default.aspx|publisher=Canada Int. Film Festival}}</ref>
||Canada
|Royal Reel Award for Best Feature Film
|''Life! Camera Action...''
|{{won}}
|-
|[http://www.carmarthenbayfilmfestival.co.uk/ Carmarthen Bay Film Festival]<ref>{{cite web|title=Carmarthen Bay Film Festival in the United Kingdom Nominees and Winners 2012|url=http://www.carmarthenbayfilmfestival.co.uk/page7.html|accessdate=|publisher=CBFF, United Kingdom}}</ref>
||UK
|Best Feature Film
|''Life! Camera Action...''
|{{nom}}
|-
|rowspan="4"|[[Golden Door Film Festival]]<ref>{{cite web|title=Golden Door International Film Festival of Jersey City Winners & Nominees 2011|url=http://goldendoorfilmfestival.org/archives/4372|accessdate=10 October 2011|publisher=GDIFFoJC}}</ref>
|rowspan="4"|USA
|Best Feature Film
|''Life! Camera Action...''
|{{nom}}
|-
|Best Director
|Rohit Gupta
|{{nom}}
|-
|Best Male Lead
|[[Shaheed Woods]]
|{{nom}}
|-
|Best Female Lead
|[[Dipti Mehta]]
|{{nom}}
|-
|rowspan="8"|[http://www.bizindia.net/rohit-guptas-life-camera-action-wins-9-awards-at-28th-goldie-film-awards/ 28th Goldie Film Awards]<ref name="Goldie">{{cite web|title=Life! Camera Action wins Top Nine Most Popular and Director's Special Grand Goldie Awards....Goldie Film Awards 2012|url=http://www.imdb.com/event/ev0001362/|accessdate=26 April 2012|publisher=Internet Movie Database}}</ref>
|rowspan="8"|United States
|Best  feature film	
|''Life! Camera Action...''	
|{{won}}	
|-	
|Best Director	
|Rohit Gupta	
|{{won}}	
|-	
|Best Actress	
|[[Dipti Mehta]]	
|{{won}}	
|-	
|Best Supporting Actor	
|Shaheed Woods
|{{won}}	
|-	
|Best Cinematography	
|Ravi Kumar R.,Rohit Gupta
|{{won}}	
|-	
|Best Song ''Chalte Jaana Hain''	
|[[KK (singer)|KK (Singer)]], Manoj Singh	
|{{won}}
|-	
|Best Screenplay	
|Rohit Gupta, Amanda Sodhi	
|{{won}}	
|-
|Best Editing	
|Rohit Gupta
|{{won}}
|-
|[[Indie Fest]]<ref>{{cite web|title=New York Film Festival in Alum makes first feature at the Indie Festival|url=http://nyfa.tumblr.com/post/2062374791/alum-makes-first-feature|accessdate=10 June 2011|publisher=NYFA}}</ref>
|USA
|Award of Merit&nbsp;– Feature film
|''Life! Camera Action...''
|{{won}}
|-
||[http://www.atamerica.or.id/events/696/survey-atamerica/ International World Peace Film Festival]<ref>{{cite web|title=Life! Camera Action... Victorious at the IFFPIE 2012|url=http://www.starblockbuster.com/life-camera-action-victorious-at-iffpie/|publisher=Star Blockbuster|accessdate=10 October 2012}}</ref>
|[[Indonesia]]
|Best New Comer film
|''Life! Camera Action...''
|{{won}}
|-
|rowspan="3"|[http://filmfestinternational.com/london/ International Filmmaker Festival of World Cinema]<ref>{{cite web|title=International Film Festival of World Cinema Life! Camera Action... wins 2 awards|url=http://www.bollywoodtrade.com/film-festivals-and-markets/life-camera-action-wins-the-river-pursuit-and-river-amulet-award-at-the-silent-river-film-festival(srff-2011/index.htm|publisher=BT}}</ref>
|rowspan="3"|UK
|Best Film of the Festival
|''Life! Camera Action...''
|{{nom}}
|-
|Best Director
|Rohit Gupta
|{{nom}}
|-
|Best Supporting Actress
|Swati Kapila
|{{nom}}
|-
||[http://thelamovieawards.com/2011_(I)_Winners.html/ Los Angeles Movie Awards]<ref name="glamsham">{{cite news|title=Life! Camera Action... Wins Award at the Los Angeles Movie Award, USA|url=http://www.glamsham.com/movies/scoops/11/jun/01-life-camera-action-wins-best-feature-film-us-award-051102.asp|accessdate=1 June 2011|newspaper=Glamsham|date=1 June 2011}}</ref>
|USA
|Award of Excellence for Best Experimental Film
|''Life! Camera Action...''
|{{won}}
|-
||[[Nevada International Film Festival]]<ref>{{cite web|title=Life! Camera Action... wins at the Nevada International Film Festival Winners 2011|url=http://www.nevadafilmfestival.com/past_winners/2011_platinum_awards.aspx|publisher=Nevada Film Festival}}</ref>
|USA
|Platinum Reel Award for Best Narrative Feature Film
|''Life! Camera Action...''
|{{won}}
|-
||[http://www.oregonfilmawards.com/2011-oregon-film-awards/ Oregon Film Awards]<ref>{{cite web|title=Life! Camera Action... wins top award at the Oregon Film Awards, Award Winners 2011|url=http://www.oregonfilmfestival.com/docs/11ORFAPressRelease.pdf|publisher=Oregon Film Awards}}</ref>
|USA
|Grand Jury Choice Award for Best Film
|''Life! Camera Action...''
|{{won}}
|-
|rowspan="3"|[http://www.prestigefilmaward.com/2012awards.htm/ Prestige Film Awards]<ref>{{cite news|title=Prestige Film Awards Life! Camera Action... Wins another top three awards|url=http://exclusivenews.co.in/life-camera-action-wins-another-top-three-awards/|accessdate=4 August 2012|newspaper=Exclusive News|date=4 August 2012}}</ref>
|rowspan="3"|USA

|Award for Best Film for its Originality & Creativity| Gold Award – Originality & Creativity
|''Life! Camera Action...''
|{{won}}
|-

|Gold Award for Leading Actress|Gold Award – Leading Actress
|[[Dipti Mehta]]
|{{won}}	
|-
|Silver Award – Dramatic Impact	
|''Life! Camera Action...''
|{{won}}	
|-
|rowspan="3"|[[Silent River Film Festival]]<ref>{{cite news|title=Life! Camera Action... Wins at the Silent River Film Festival in Irvine California|url=http://www.bollywoodtrade.com/film-festivals-and-markets/life-camera-action-wins-the-river-pursuit-and-river-amulet-award-at-the-silent-river-film-festival(srff-2011/index.htm|accessdate=17 September 2011|newspaper=Bollywood Trade|date=17 September 2011}}</ref>
|rowspan="3"|USA
|River Pursuit Award for Best Film
|''Life! Camera Action...''
|{{won}}
|-
|Best Actress
|[[Dipti Mehta]]
|{{nom}}
|-
|River Amulet Award for Best Director
|Rohit Gupta
|{{won}}
|-
||[https://sites.google.com/site/sunsetfilmfestivallosangeles/home/puttamatathail-house-family/ Sunset International Film Festival]<ref>{{cite web|title=2012 Awards|url=https://sites.google.com/site/sunsetfilmfestivallosangeles/home/puttamatathail-house-family|publisher=Sunset Int. Film Festival}}</ref><ref>{{cite news|title=Dipti Mehta wins Award for Life! Camera Action...at the Sunset Int. Film Festival in California|url=https://www.indiawest.com/news/4793-dipti-mehta-wins-award-for-life-camera-action.htm|accessdate=1 June 2012|newspaper=India West|date=1 June 2012}}</ref>
|USA
 	
|Award for Best Female Actor|Best Female Actor
|[[Dipti Mehta]]
|{{won}}	 	
|-
||[http://www.twiff.org/2012-winners/ The World Independent Film Festival (TWIFF)]<ref>{{cite news|last1=Editorial|first1=BT|title=LIFE! CAMERA ACTION... wins Best Inspirational Family Drama Award in San Francisco, USA|url=http://www.bollywoodtrade.com/film-festivals-and-markets/life-camera-action-wins-best-inspirational-family-drama-award-in-san-francisco-usa/2D2C28A8-2B39-4474-980C-EBC7E41638F5/index.htm|accessdate=20 June 2015|publisher=BT|date=18 September 2012}}</ref>
 	
|USA

|Best Inspirational Family Drama

|''Life! Camera Action...''
	
|{{won}}
|-
||[http://www.tifva.com/past-seasons/ Toronto Independent Film & Video Awards]<ref>{{cite web|title=Toronto Independent Film & Video Awards 2011 Nominations|url=http://www.tifva.com/Winner2011.php#|publisher=Toronto Independent Film & Video Awards}}</ref>
|Canada
|Best Experimental Film
|''Life! Camera Action...''
|{{nom}}
|-
|rowspan="7"|[[World Music & Independent Film Festival]]<ref>{{cite news|title=World Music & Independent Film Festival Washington D.C. Awards and nominees. Life! Camera Action...Wins 2 Awards|url=http://articles.timesofindia.indiatimes.com/2011-08-25/cinema/29926744_1_film-festival-south-africa-international-film-festival-thailand-noor-naghmi|newspaper=Times of India}}</ref>
 	
|rowspan="7"|USA
	
|Best Drama
	
|''Life! Camera Action...''
	
|{{nom}}
	
|-

|Best Director
	
|Rohit Gupta
	
|{{nom}}
	
|-
	
|Best Actress
	
|[[Dipti Mehta]]
	
|{{nom}}
 	
|-
	
|Best Original Music
 	
|Manoj Singh
	
|{{won}}
	
|-
	
|Best Screenplay
	
|Rohit Gupta, Amanda Sodhi
	
|{{nom}}
	
|-
	
|Best Actor in Supporting Role
	
|Noor Naghmi
	
|{{won}}
	
|-
|Best Actress in Supporting Role
|Swati Kapila
|{{nom}}
	
|-
||[http://www.yosemitefilmfestival.com/past-winners/2011-winners/ Yosemite International Film Awards]<ref>{{cite web|title=2011 Winners Yosemite International Film Awards|url=http://www.yosemitefilmfestival.com/Default.aspx?TabId=451&AspxAutoDetectCookieSupport=1|publisher=Yosemite International Film Awards}}</ref>
 	
|USA

|Grand Jury Choice Award for Best Film

|''Life! Camera Action...''
	
|{{won}}
|-
|}

==Limca Book of records==
{| class="wikitable" style="width: 85%; font-size: 0.98em;"
!Record
!Honoring body
|-
|Film ''Life! Camera Action...'' was shot by just a two-member crew consisting of director/producer Rohit Gupta & Ravi Kumar R.<ref>{{cite news|title=Filmmaker Rohit Gupta honored by the Limca Book of Records|url=https://www.nyfa.edu/film-school-blog/nyfa-grad-rohit-gupta-honored-by-limca-book-of-records/|publisher=Times of India|date=14 January 2014}}</ref>
|[[Limca Book of Records]]
|-
|}

==See also==
*[[The Pursuit of Happyness]]
*[[Chak De! India|Go for it! India]]
*[[Bend It Like Beckham]]
*[[August Rush]]
*[[3 Idiots]]
*[[October Sky]]

==References==
{{Reflist|colwidth=35em}}

==External links==
*[http://www.RNGfilms.com Official Website]
*{{IMDb title|1682931}}
* {{rotten-tomatoes|Life_Camera_Action|Life! Camera Action...}}

{{Navbox
|name = Rohit Gupta
|title = Films directed by ''[[Rohit Gupta]]''
|state = autocollapse
|listclass = hlist
|list1 =
* ''[[Another Day Another Life (short film)|Another Day Another Life]]'' (2009)
* ''Just do it!'' (2011)
* ''Life! Camera Action...'' (2012)
* ''[[Midnight Delight (film)|Midnight Delight]]'' (2016)
}}

[[Category:2012 films]]
[[Category:2010s drama films]]
[[Category:Films about filmmaking]]
[[Category:American independent films]]
[[Category:Indian films]]
[[Category:Directorial debut films]]
[[Category:English-language films]]
[[Category:2010s Hindi-language films]]
[[Category:Films set in New Jersey]]
[[Category:Films shot in New Jersey]]
[[Category:Films shot in New York City]]
[[Category:American musical films]]
[[Category:American avant-garde and experimental films]]
[[Category:American drama films]]
[[Category:Indian-American films]]
[[Category:Indian art films]]
[[Category:Punjabi-language films]]
[[Category:American films]]
[[Category:Films about women in the Indian diaspora]]
[[Category:Indian musical films]]
[[Category:Films directed by Rohit Gupta]]