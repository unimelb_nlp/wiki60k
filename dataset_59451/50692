{{Hatnote|For computer programs to manage an individual's bibliographic references, see [[Reference management software]]}}
A '''bibliographic database''' is a [[database]] of [[bibliographic record]]s, an organized digital collection of references to published literature, including [[academic journal|journal]] and [[newspaper]] articles, conference [[proceedings]], reports, government and legal publications, [[patent]]s, [[book]]s, etc.  In contrast to [[library catalog]]ue entries, a large proportion of the bibliographic records in bibliographic databases describe articles, conference papers, etc., rather than complete [[monograph]]s, and they generally contain very rich subject descriptions in the form of [[Index term|keyword]]s, subject classification terms, or [[abstract (summary)|abstract]]s.<ref>{{cite book|page=127 |title=International Encyclopedia of Information and Library Science| edition=Second |editor1-last=Feather |editor1-first=John| editor2-last=Sturges |editor2-first=Paul| publisher=Routledge| location=London| year=2003| ISBN=0-415-25901-0}}</ref>

A bibliographic database may be general in scope or cover a specific [[academic discipline]] like [[computer science]].<ref>{{cite journal|last1=Kusserow|first1=Arne|last2=Groppe|first2=Sven|title=Getting Indexed by Bibliographic Databases in the Area of Computer Science|journal=Open Journal of Web Technologies (OJWT)|date=2014|volume=1|issue=2|doi=10.19210/OJWT_2014v1i2n02_Kusserow|url=https://www.ronpub.com/publications/ojwt/OJWT_2014v1i2n02_Kusserow.html|accessdate=26 May 2016}}</ref> A significant number of bibliographic databases are still proprietary, available by licensing agreement from vendors, or directly from the [[indexing and abstracting service]]s that create them.<ref name="Reitz">{{cite book| chapter=bibliographic database | page=70 | title=Dictionary for Library and Information Science |last=Reitz |first= Joan M. |publisher= Libraries Unlimited | location= Westport, Connecticut | year= 2004| isbn=1-59158-075-7 }}</ref>

Many bibliographic databases evolve into [[digital library|digital libraries]], providing the full-text of the indexed contents. Others converge with non-bibliographic scholarly databases to create more complete disciplinary [[search engine]] systems, such as [[Chemical Abstracts]] or [[Entrez]].

==History==
Prior to the mid-20th century, individuals searching for published literature had to rely on printed [[bibliographic index]]es.  "During the early 1960s computers were used to digitize text for the first time; the purpose was to reduce the cost and time required to publish two American abstracting journals, the ''[[Index Medicus]]'' of the [[National Library of Medicine]] and the ''[[Scientific and Technical Aerospace Reports]]'' of the [[National Aeronautics and Space Administration]] (NASA). By the late 1960s such bodies of digitized alphanumeric information, known as bibliographic and numeric databases, constituted a new type of information resource".<ref>{{cite web|title=information processing|work=Encyclopædia Britannica Online| url=http://search.eb.com/eb/article-9106312| year=2010| accessdate=April 29, 2010}}</ref> "Online interactive retrieval became commercially viable in the early 1970s over private telecommunications networks.  The first services offered a few databases of indexes and abstracts of scholarly literature.  These databases contained bibliographic descriptions of journal articles that were searchable by keywords in author and title, and sometimes by journal name or subject heading.  The user interfaces were crude, the access was expensive, and searching was done by librarians on behalf of 'end users'".<ref>{{cite book| pages=89–90 |title=Scholarship in the Digital Age: Information, Infrastructure, and the Internet | last=Borgman |first=Christine L. | publisher=The MIT Press |location = Cambridge, Massachusetts | year= 2007 |isbn=978-0-262-02619-2}}</ref>

==Notable examples==
* [[Bibliographic index]]
* [[Citation index]]
* [[Digital library]]
* [[Document-oriented database]]
* [[Full text database]]
* [[List of academic databases and search engines]]
* [[Institutional repository]]
* [[Online public access catalog]] (OPAC; library catalog)

==References==
{{Reflist}}

{{DEFAULTSORT:Bibliographic Database}}
[[Category:Information science]]
[[Category:Bibliographic databases and indexes| ]]
[[Category:Library 2.0]]

[[es:Referencia bibliográfica]]
[[ru:IBList]]