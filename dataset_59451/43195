<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name= P4M Mercator
 |image= P4M-1 Mercator in flight.jpg
 |caption= United States Navy P4M-1
}}{{Infobox Aircraft Type
 |type= Patrol bomber
 |national origin = [[United States]] 
 |manufacturer= [[Glenn L. Martin Company|Martin]]
 |designer=
 |first flight= 20 October 1946
 |introduced= [[1950 in aviation|1950]]
 |retired= [[1960 in aviation|1960]]
 |status= Retired
 |primary user= [[United States Navy]]
 |more users=
 |produced=
 |number built= 21
 |variants with their own articles=
}}
|}

The '''Martin P4M Mercator''' was a maritime reconnaissance aircraft built by the [[Glenn L. Martin Company]]. The Mercator was an unsuccessful contender for a [[United States Navy]] requirement for a long-range maritime patrol bomber, with the [[Lockheed P-2 Neptune|Lockheed P2V Neptune]] chosen instead. It saw a limited life as a long-range electronic [[reconnaissance]] aircraft. Its most unusual feature was that it was powered by a combination of [[piston engine]]s and [[turbojet]]s, the latter being in the rear of the engine nacelles.

==Design and development==
Work began on the Model 219 in 1944, as a replacement for the [[Consolidated PB4Y Privateer|PB4Y Privateer]] long range patrol bomber, optimised for long range minelaying missions, with the first flight being on 20 October 1946.<ref name="WoF p139">Lake and Dorr 2000, p.139.</ref> A large and complicated aircraft, it was powered by two [[Pratt & Whitney]] [[Pratt & Whitney Wasp Major|R4360 Wasp Major]] 28-cylinder [[radial engine]]s. To give a boost during takeoff and combat, two [[Allison J33]] turbojets were fitted in the rear of the two enlarged engine nacelles, the intakes being beneath and behind the radial engines.<ref name="WoF p138-9">Lake and Dorr 2000, pp. 138–139.</ref> The jets, like those on most other piston/jet hybrids, burned gasoline instead of jet fuel which eliminated the need for separate fuel tanks.

A [[tricycle undercarriage]] was fitted, with the nosewheel retracting forwards. The single-wheel main legs retracted into coverless fairings in the wings, so that the sides of the wheels could be seen even when retracted. The wings themselves, unusually, had a different airfoil cross-section on the inner wings than the outer.

Heavy defensive armament was fitted, with two 20&nbsp;mm (.79&nbsp;in) cannons in an Emerson nose turret and a Martin tail turret, and two 0.5&nbsp;in (12.7&nbsp;mm) machine guns in a Martin dorsal turret. The bomb-bay was, like British practice, long and shallow rather than the short and deep bay popular in American bombers. This gave greater flexibility in payload, including long torpedoes, bombs, mines, depth charges or extended-range fuel tanks.<ref name="Ferret 216-7">Dorr and Burgess 1993, pp. 216–217.</ref>

==Operational history==
[[File:Martin P4M Mercator.jpg|thumb|right|P4M-1 of [[VP-21 (1943-69)|VP-21]]]]
The US Navy chose the smaller, simpler, cheaper and better performing P2V Neptune for the maritime patrol requirement, but nineteen aircraft were ordered in 1947 for high-speed minelaying purposes. The P4M entered service with [[VP-21 (1943-69)|Patrol Squadron 21 (VP-21)]] in 1950, the squadron deploying to [[NAS Port Lyautey]] in [[French protectorate of Morocco|French Morocco]].<ref name="Ferret p217">Dorr and Burgess 1993, p.217.</ref> It remained in use with VP-21 until February 1953.<ref name="DANAS v2 p125">Roberts 2000, p.125.</ref>

From 1951, the 18 surviving production P4Ms were modified for the electronic reconnaissance (or [[SIGINT]], for ''signals intelligence'') mission as the '''P4M-1Q''', to replace the [[PB4Y-2 Privateer]]. The crew was increased to 14 and later 16 to operate all the surveillance gear, and the aircraft was fitted with a large number of different antennas.<ref name="WoF p141-2">Lake and Dorr 2000, pp. 141–142.</ref>

[[File:Martin P4M Mercator VQ-2 06.09.56.jpg|thumb|left|P4M-1Q Mercator of VQ-2 electronics reconnaissance squadron in September 1956 - note extra radar 'bulges' on this variant]]

Starting in October 1951, electronic surveillance missions were flown from [[U.S. Naval Station Sangley Point]] in the [[Philippines]] (and, later from the Naval Air Station Iwakuni, Japan, and later Naval Air Station Atsugi, Japan, by a secretive unit that eventually gained the designation [[VQ-1|Fleet Air Reconnaissance Squadron One (VQ-1)]]. Long missions were flown along the coast (about 30 NM offshore) of Vietnam, China, North Korea and the eastern Soviet Union, and were of a highly secret nature; the aircraft some times masqueraded as regular P2V Neptunes in radio communications, and often flew with false [[United States military aircraft serials#United States Navy and Marine Corps|serial numbers]] (Bureau Numbers) painted under the tail. Operational missions were always flown at night, during the dark of the moon when possible, and with no external running lights.<ref name="Ferret p217,220">Dorr and Burgess 1993, pp. 217, 220.</ref>

One Mercator was shot down near Shanghai by Chinese fighters on 22 August 1956, with its crew of 16 all killed.<ref name="Ferret p220-1">Dorr and Burgess 1993, pp. 220–221.</ref> Another P4M-1Q was attacked by two North Korean [[MiG-17]]s on 16 June 1959 with heavy damage and serious injury to the tail gunner.<ref name="Ferret p221-2">Dorr and Burgess 1993, pp. 221–222.</ref> The aircraft were also operated out of Morocco by [[VQ-2]], where one aircraft was intercepted near Ukrainian airspace by Soviet MiG's. It was shot down by the MIGs and crashed into Mediterranean Sea with the loss of all crew.<ref>Former seaman who served in the Med</ref> Another, on 6 February 1952, [[List of accidents and incidents involving military aircraft (1950–54)#1952|ditched]] north of Cyprus at night, out of fuel, no power, losing only the Aircraft Commander/pilot after they were in the water (See United States Naval Institute, Naval History, March/April 1997). The crew was rescued by [[HMS Chevron|HMS ''Chevron'']]. One P4M-1Q of JQ-3<ref>http://www.portlyautey.com/ECM-2.htm</ref> [[List of accidents and incidents involving military aircraft (1955–1959)#1958|crashed]] at [[Ocean View, Virginia|Ocean View]], Virginia, on 6 January 1958,<ref>http://www.joebaugher.com/navy_serials/thirdseries13.html</ref> when it lost an engine on approach to [[NAS Norfolk]], Virginia, killing four crew and injuring three civilians.<ref>Associated Press, "Four Missing In Air Crash", ''The Anderson Independent'', Anderson, South Carolina, Tuesday 7 January 1958, Volume 41, Number 99, page 1.</ref>

The Mercators were replaced by the [[A-3 Skywarrior|EA-3B Skywarrior]], which, being carrier-based, had a greater degree of flexibility and the larger [[EC-121 Warning Star|Lockheed WV-2Q Warning Star]]. Final withdrawal from service was in 1960 after which all of the remaining P4Ms were scrapped.<ref name="Ferret p222">Dorr and Burgess 1993, p.222.</ref>

==Variants==
;XP4M-1
:Two prototype aircraft with two R-4360-4 engines.
;P4M-1
:Production aircraft with two R-4360-20A engines, 19 built.
;P4M-1Q
:P4M-1s redesignated when modified for radar countermeasures.

==Operators==
;{{USA}}
* [[United States Navy]]

==Specifications (P4M Mercator)==
[[File:P4M-1 3-side drawing.jpg|thumb|right|Martin P4M-1 ''Mercator'']]

{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=both
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For instructions on using |ref=, |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Airtemp]]. -->
|crew=9
|length main=85 ft 2 in
|length alt=26.0 m
|span main=114 ft 0 in
|span alt=34.7 m
|height main=26 ft 1 in
|height alt=8.0 m
|area main=1,311 ft²
|area alt=122 m²
|empty weight main=48,536 lb
|empty weight alt=22,016 kg
|loaded weight main=88,378 lb
|loaded weight alt=40,088 kg
|max takeoff weight main=lb
|max takeoff weight alt=kg
|more general=
|engine (prop)=[[Pratt & Whitney R-4360]] Wasp Major
|type of prop=[[radial engine]]s
|number of props=2
|power main=3,250 hp
|power alt=2,420 kW
|engine (jet)=[[Allison J33]]-A-23
|type of jet=[[turbojet]]s
|number of jets=2
|thrust main=4,600 lbf
|thrust alt=20 kN
|max speed main=410 mph
|max speed alt=660 km/h
|range main=2,840 mi
|range alt=4,570 km
|ceiling main=34,600 ft
|ceiling alt=10,500 m
|climb rate main=ft/min
|climb rate alt=m/s
|loading main=lb/ft²
|loading alt=kg/m²
|avionics=
*AN/APS-33 search radar
|armament=
*4 × 20 mm (.79 in) cannons in nose and tail turrets
*2 × .50 in (12.7 mm) machine guns in dorsal turret
*Up to 12,000 lb (5,400 kg) of bombs, mines, depth charges, or torpedoes
}}

==See also==
{{aircontent
|similar aircraft=
* [[Avro Shackleton]]
* [[Consolidated PB4Y-2 Privateer]]
* [[Lockheed P-2 Neptune]]
* [[Lockheed P-3 Orion]]
|lists=
* [[List of military aircraft of the United States (naval)]]
}}

==References==
{{Commons category}}
{{reflist}}
* Dorr, Robert F. and Richard R. Burgess. "Ferreting Mercators". ''[[Air International]]'', October 1993, Vol.45, No. 4. ISSN 0306-5634. pp.&nbsp;215–222.
*Lake, Jon and Robert F. Dorr. "Martin P4M Mercator". ''Wings of Fame''. Volume 19. London:Aerospace Publishing, 2000. ISBN 1-86184-049-7. pp.&nbsp;138–149.
*Roberts, Michael D. ''[http://www.history.navy.mil/branches/dictvol2.htm Dictionary of American Naval Aviation Squadrons:Volume 2: The History of VP, VPB, VP(HL) and VP(AM) Squadrons]''. Washington, DC:Naval Historical Center, 2000.

{{Martin aircraft}}
{{USN patrol aircraft}}

{{Authority control}}

{{DEFAULTSORT:P4M Mercator}}
[[Category:Mixed-power aircraft]]
[[Category:Aircraft with auxiliary jet engines]]
[[Category:United States patrol aircraft 1940–1949|Martin PM4 Mercator]]
[[Category:Martin aircraft]]