{{Infobox journal
| title = International Political Science Review
| cover = [[File:International Political Science Review journal front cover.jpg]]
| editor = Marian Sawer, Mark Kesselman 
| discipline = [[Political science]]
| language = English, with abstracts in French and Spanish
| former_names = 
| abbreviation = Int. Polit. Sci. Rev.
| publisher = [[Sage Publications]] on behalf of the [[International Political Science Association]]
| country = 
| frequency = 5/year
| history = 1980-present
| openaccess = 
| license = 
| impact = 0.954
| impact-year = 2015
| website = http://ips.sagepub.com/
| link1 = http://ips.sagepub.com/content/current
| link1-name = Online access
| link2 = http://ips.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 44689900
| LCCN = 80644366 
| CODEN = 
| ISSN = 0192-5121
| eISSN = 1460-373X 
}}
The '''''International Political Science Review/Revue Internationale de Science Politique''''' is a [[Peer review|peer-reviewed]] [[academic journal]] covering the field of [[political science]]. The [[editors-in-chief]] are Mark Kesselman ([[Columbia University]]) and Marian Sawer ([[Australian National University]]). It was established in 1980 and is published by [[Sage Publications]] on behalf of the [[International Political Science Association]].<ref>{{cite web |title=Publications - International Political Science Review |url=http://www.ipsa.org/publications/ipsr |work= |publisher=International Political Science Association |accessdate=16 August 2013}}</ref>

== Editors' choice and special issues ==
Editors' choice collections of articles on a particular theme, selected from past issues, were initiated in 2011. So far there have been collections on ideology, regimes and regime change, political parties and party systems, gender and political behaviour, gender and political institutions and religion and politics. Access to the articles in these collections is free.<ref>{{cite web|title=International Political Science Review Editors' Choice Collections|url=http://ips.sagepub.com/site/Editors_Collections/Eds_Choice_Index.xhtml|publisher=Sage Publications|accessdate=23 April 2014}}</ref> The journal also occasionally publishes special issues.

== Meisel-Laponce Award ==
The journal has a cash prize of $1000 for the best article published in the previous four years. The prize was first awarded in 2012 and went to Jorgen Moller and Svend-Erik Skaaning, for ''Beyond the radial delusion: Conceptualising and measuring democracy and non-democracy''. There is free access to the winning and short-listed articles.<ref>{{cite web|url=http://ips.sagepub.com/site/Editors_Collections/Meisel_Laponce_Award.xhtml |title=Meisel-Laponce Award |doi=10.1177/0192512110369522 |publisher=Sage Publications |accessdate=2014-04-23}}</ref> The next award will be made at the International Political Science Association's World Congress in Istanbul in 2016.

==Most cited article==
The most cited paper published in the journal since the beginning of 2002, cited over 300 times according to [[Google Scholar]], is:
* {{cite journal |doi=10.1177/0192512105053784 |title=Politics in the Supermarket: Political Consumerism as a Form of Political Participation |year=2005 |last1=Stolle |first1=D. |last2=Hooghe |first2=M. |last3=Micheletti |first3=M. |journal=International Political Science Review/ Revue internationale de science politique |volume=26 |issue=3 |pages=245–269}}

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 0.954, ranking it 68th out of 163 journals in the category "Political Science".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Political Science |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== See also ==
* [[List of political science journals]]

== References ==
{{reflist}}

== External links ==
* {{Official website|1=http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal200844}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1980]]
[[Category:Political science journals]]