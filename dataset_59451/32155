{{Infobox coin
| Country             = United States
| Denomination        = Louisiana Purchase Exposition dollar
| Value               = 1
| Unit                = [[United States dollar|US dollar]]
| Mass                = 1.672
| Gold_troy_oz      = 0.04837
| Diameter            = 15
| Mintage              = 125,000 of each type minted; 35,000 total coins distributed, remainder melted.
| Edge                = [[Reeding|Reeded]]
| Composition =
  {{plainlist |
* 90% [[gold]]
* 10% [[copper]]
  }}
| Years of Minting    = 1903 (some pieces struck in 1902 with 1903 date) |
| Mint marks          = None. All pieces struck at [[Philadelphia Mint]] without mint mark.
| Obverse             = Louisiana Purchase Jefferson dollar obverse.jpg
| Obverse Design      = [[Thomas Jefferson]]
| Obverse Designer    = [[Charles E. Barber]], after a medal by John Reich
| Obverse Design Date = 1903
| Obverse2             = Louisiana Purchase McKinley dollar obverse.jpg
| Obverse2 Design      = [[William McKinley]]
| Obverse2 Designer    = [[Charles E. Barber]], after a medal by himself
| Obverse2 Design Date = 1903
| Reverse             = Louisiana Purchase Jefferson dollar reverse.jpg
| Reverse Design      = Reverse common to both varieties
| Reverse Designer    = [[Charles E. Barber]]
| Reverse Design Date = 1903
}}
The '''Louisiana Purchase Exposition dollar''' was a commemorative coin issue in gold dated 1903.  Struck in two varieties, the coins were designed by [[United States Mint|United States Bureau of the Mint]] Chief Engraver [[Charles E. Barber]].  The pieces were issued to commemorate the [[Louisiana Purchase Exposition]] held in 1904 in St. Louis; one variety depicted former president [[Thomas Jefferson]], and the other, the [[Assassination of William McKinley|recently assassinated]] president [[William McKinley]]. Although not the [[Columbian half dollar|first American commemorative coins]], they were the first in gold.

Promoters of the Louisiana Purchase Exposition, originally scheduled to open in 1903, sought a commemorative coin for fundraising purposes.  Congress authorized an issue in 1902, and exposition authorities, including numismatic promoter [[Farran Zerbe]], sought to have the coin issued with two designs, to aid sales.  The price for each variety was $3, the same cost whether sold as a coin, or mounted in jewelry or on a spoon.

The coins did not sell well, and most were later melted.  Zerbe, who had promised to support the issue price of the coins, did not do so as prices dropped once the fair (rescheduled for 1904) closed.  This drop, however, did not greatly affect Zerbe's career, as he went on to promote other commemorative coins and become president of the [[American Numismatic Association]].  The coins also recovered, regaining their issue price by 1915; they are now worth between a few hundred and several thousand dollars, depending on condition.

== Background ==
{{main article|Louisiana Purchase}}
Much of the area near the [[Mississippi River]] was explored by French explorers in the 17th and 18th centuries.  In 1682, [[René-Robert Cavelier, Sieur de La Salle]], claimed the entire area drained by the river for France, naming it [[Louisiana (New France)|Louisiana]] for [[Louis XIV of France|Louis XIV]].  Although most French territory in the Western Hemisphere was lost in the [[French and Indian War]] (1756–1763), the Mississippi basin did not pass to the victors in that war (primarily the British) as it had been secretly transferred to Spain by the [[Treaty of Fontainebleau (1762)|1762 Treaty of Fontainebleau]].{{sfn|Slabaugh|pp=22–23}}

[[Napoleon]] came to power in 1799.  Dreaming of a renewed French empire, he secured the return of the Louisiana territory from Spain via the [[Third Treaty of San Ildefonso]] the following year, and through other agreements.  These pacts were initially secret, and newly inaugurated American President [[Thomas Jefferson]] learned of them in 1801.  Fearing that the port of [[New Orleans]] would be closed to American shipping, he sent former Virginia senator [[James Monroe]] to France to assist American Minister [[Robert R. Livingston (chancellor)|Robert Livingston]] in purchasing the lower Mississippi; Congress appropriated $2 million for the purpose.{{sfn|Slabaugh|p=23}}

[[File:Louisiana Purchase7 1903 Issue-10c-crop.jpg|thumb|left|Ten-cent stamp issued for the [[Louisiana Purchase Exposition]] showing the part of the United States that came from the [[Louisiana Purchase]]]]
When the Americans met with Napoleon, they found that the emperor desired to sell the entire territory, much of which was unmapped and unexplored by white men; Napoleon was faced with defeat in revolting [[Haiti]] and feared that the British would capture New Orleans, meaning he would lose Louisiana with no compensation. After some haggling, they agreed on a price of 60 million francs, plus 20 million more to pay claims by American citizens against France—a total of some $15 million, which paid for some {{convert|828500|sqmi}} of land. The treaty was signed on April 30, 1803, and, although there was some question as to whether there was constitutional power for such a purchase, the American Senate ratified the treaty on October 20, 1803. The United States took formal possession two months later.{{sfn|Slabaugh|p=23}}

The [[Louisiana Purchase]] doubled the size of the United States, and today forms much of the center of the country.{{sfn|Slabaugh|p=23}} Desirous of honoring the centennial of the purchase, Congress passed authorizing legislation for an exposition; the bill was signed by President [[William McKinley]] on March 3, 1901. McKinley [[Assassination of William McKinley|was assassinated]] in September of that year.{{sfn|Swiatek & Breen|p=120}}

== Preparation ==
The Louisiana Purchase Exposition dollar was authorized by Congress on June 28, 1902, when President [[Theodore Roosevelt]] signed an appropriations bill that included a $5,000,000 [[rider (legislation)|rider]] to subsidize the [[Louisiana Purchase Exposition]].{{sfn|Swiatek & Breen|p=120}} The bill in question authorized 250,000 gold one-dollar pieces to be paid over to the exposition organizers as part of the appropriation, upon their posting a bond that they would fulfill the requirements of the legislation.  The bill did not specify the wording or design to be placed on the coins, leaving that to the discretion of the [[United States Secretary of the Treasury|Secretary of the Treasury]].{{sfn|Bureau of the Mint|pp=104–105}}

[[File:1903 MS Louisiana Purchase Three-Piece Cardboard Die Trial Impressions.jpg|thumb|left|Die impressions in cardboard of [[pattern coin|pattern]] versions of the gold dollars.  The reverse shows the olive branch larger than on the issued coins.]]

Anthony Swiatek and [[Walter Breen]], in their encyclopedia of commemorative coins, suggested that the decision to have multiple designs was "through some unrecorded agreement".{{sfn|Swiatek & Breen|p=120}}  The legislation was ambiguous enough to permit such an interpretation, and numismatist [[Farran Zerbe]] urged the Mint to strike more than one type of coin, stating that sales would be increased if this was done.{{sfn|Bowers ''Encyclopedia'', Part 4}} Zerbe was not only a collector (he would serve as president of the [[American Numismatic Association]] from 1908 to 1910), but he also promoted numismatics with his traveling exhibition, "Money of the World".  He was involved in the distribution of commemorative coins from the [[Columbian half dollar]] of 1892 to the [[Panama-Pacific issue]] of 1915, and would be the sole distributor of Louisiana Purchase dollars.{{sfn|''The Numismatist''|March 2004|pp=41–42}}{{sfn|American Numismatic Association}}

[[File:McKinley medal 1901 obverse.jpg|thumb|upright|This McKinley medal by Chief Engraver Barber was used as the basis of the McKinley variety of the Louisiana Purchase Exposition dollar.]]

On August 12, 1902, Treasury Secretary [[Leslie M. Shaw]] wrote to former Missouri governor [[David R. Francis]], one of the promoters of the exposition, enquiring what design exposition officials would like to see on the reverse of the coins.  Although Francis's response is not extant, Mint authorities originally determined upon an olive branch surrounding a numeral "1".  This was apparently disliked by the [[Director of the United States Mint|Director of the Mint]], [[George E. Roberts]], for on October 2, 1902, [[Philadelphia Mint]] Superintendent John Landis wrote to him, enclosing cardboard impressions of the original and revised proposed reverses.  The new design had the value spelled out and the letter stated that the changes were being made at Roberts's suggestion.  On October 13, Barber went to Washington (where the director's office was located) to confer with Roberts about the design.  Roberts considered the olive branch "too conspicuous", given the size of the coin and the lettering, and asked that the branch be reduced in size.{{sfn|Taxay|p=21}}  This apparently was done.{{sfn|Taxay|p=21}} By September 1902, work upon the dies for the obverses, showing the heads of McKinley and Jefferson, being worked upon by Mint Chief Engraver [[Charles E. Barber]], was well-advanced.{{sfn|Taxay|p=21}}

In December 1902, the Philadelphia Mint struck 75,080 gold dollars.  These were dated 1903, a violation of normal Mint practice to have the date of striking on the coin.  This was not unprecedented; the 1900-dated [[Lafayette dollar]] had been struck in December 1899.  It is not known which gold dollar was first struck.  In January 1903, an additional 175,178 pieces were coined; the excess of 258 over the authorized mintage was set aside for testing by the annual [[United States Assay Commission|Assay Commission]].{{sfn|Swiatek|p=71}} There is no difference between those pieces struck in 1902 and those minted in 1903.{{sfn|Swiatek & Breen|p=120}} Fifty thousand pieces were sent to the St. Louis sub-treasury on December 22, 1902, to await the organizing committee's compliance with other parts of the law, most likely relating to the required posting of a bond.{{sfn|Swiatek & Breen|p=120}}{{sfn|Bureau of the Mint|pp=104–105}}

The first 100 specimens of each design were struck in a proof finish.  These were mounted on cardboard with presentation certificates and presented to favored insiders and Mint officials; they were not available to the public.  The certificates were signed by Superintendent Landis, and by Rhine R. Freed, Chief Coiner of the Philadelphia Mint. The coin was placed inside a holder with wax paper window, secured into place with heavy string with that mint's seal.{{sfn|Swiatek|p=72}} These were the first commemorative gold coins struck by the United States.{{sfn|Slabaugh|p=20}}

== Design ==
[[File:Jefferson medal Reich obverse.jpg|thumb|left|upright|John Reich's [[Indian Peace Medal]] for Thomas Jefferson served as the basis for the Jefferson obverse of the Louisiana Purchase Exposition issue.]]
Barber took the design for the Jefferson obverse from the former president's [[Indian Peace Medal]], created by engraver [[John Reich (engraver)|John Reich]], who used a bust by [[Jean-Antoine Houdon]] as his model.{{sfn|Swiatek|p=71}}  The chief engraver modeled the McKinley obverse after his own design for the fallen president's medal issued by the Mint.{{sfn|Taxay|p=19}} Barber's medal had been modeled from life; McKinley had sat for the chief engraver.{{sfn|Swiatek & Breen|p=120}} The reverse, for both coins, contains the denomination, a commemorative inscription, and an olive branch above the anniversary dates.{{sfn|Swiatek|p=71}}

Coin dealer B. Max Mehl deemed the issue "the most attractive of all of our commemorative gold dollars".{{sfn|Mehl|p=49}} Others disagreed; Swiatek and Breen criticized the pieces, stating that Jefferson's "facial features, inaccurately rendered by Charles E. Barber, have acquired a resemblance to Napoleon Bonaparte, the other party in the Louisiana Purchase transaction."{{sfn|Swiatek & Breen|p=120}} Stating that McKinley was recognizable by his bow tie, they note of the reverse, "the olive branch—if that is the plant intended—may refer to this 828,000 square mile territory's acquisition by peaceful means".{{sfn|Swiatek & Breen|p=120}} Numismatic historian [[Don Taxay]] criticized Reich's medal, stating that it "is hardly elegant, with Jefferson hunched unpleasantly in the circle as though placed there by a modern [[Procrustes]]".{{sfn|Taxay|p=19}} Taxay noted that Barber's rendition of McKinley for that medal had attracted the insult of "deadly" from the chief engraver's longtime enemy, sculptor [[Augustus Saint-Gaudens]].{{sfn|Taxay|p=19}}

[[File:Panama-Pacific dollar obverse.jpg|thumb|right|upright|Art historian [[Cornelius Vermeule]] considered the Panama-Pacific gold dollar more beautiful than the Louisiana Purchase Exposition issue.]]
Art historian [[Cornelius Vermeule]] criticized the Louisiana Purchase Exposition dollar and the [[Lewis and Clark Exposition dollar]] issued in 1904–1905: "the lack of spark in these coins, as in so many designs by Barber or [Assistant Engraver George T.] [[George T. Morgan|Morgan]], stems from the fact that the faces, hair, and drapery are flat and the lettering is small, crowded, and even."{{sfn|Vermeule|p=105}} He did not believe that the problems he saw were due to the small size of the dollar, stating that the gold dollar of the Panama-Pacific issue, by [[Charles Keck]], is far more beautiful.{{sfn|Vermeule|p=105}} Vermeule noted that contemporary accounts saw the 1903 issue as an innovation; a 1904 article in the ''American Journal of Numismatics'' stated that they "indicate a popular desire for a new departure from the somewhat monotonous types of Liberty which have characterized our money&nbsp;... If this tendency could make itself felt on the regular coinage, it would give a new zeal to collectors."{{sfn|Vermeule|p=105}} Beginning in 1909 with the [[Lincoln cent]], the Mint would depict an actual person on the circulating coinage; this would become more common with the 1932 [[Washington quarter]].{{sfn|Vermeule|p=105}}

== Distribution, aftermath, and collecting ==
{{main article|Louisiana Purchase Exposition}}
[[File:Farran Zerbe (ca. 1908).jpg|thumb|upright|left|Numismatist Farran Zerbe]]

The fair at St. Louis opened on April 30, 1904, a year later than originally planned.  It was one of the largest [[World's Fair]]s in area, set over {{convert|1272|acre}} in [[Forest Park (St. Louis)|Forest Park]].  There were 15 major buildings and a host of smaller exhibits, and it is doubtful if many attendees saw more than a fraction of the attractions—seeing everything in the Agricultural Building alone required a walk of {{convert|9|mi}}.  Twenty million people attended the exposition,{{sfn|Slabaugh|p=21}} which inspired the popular song, "[[Meet Me in St. Louis, Louis|Meet Me in St. Louis]]".{{sfn|Bowers ''Encyclopedia'', Part 5}}

The coins were sold at $3 each.  They were available in a case of issue, or could be purchased mounted in spoons and various sorts of jewelry.  Some were mounted with solder, which has impaired their present-day numismatic value; others were sold with mountings that did not damage the coin.{{sfn|Swiatek & Breen|p=120}}{{sfn|Swiatek|p=72}} Zerbe had thought of these varied ways of selling the coin, and many of the sales at the fair were in this manner.{{sfn|Bowers ''Encyclopedia'', Part 6}} No additional charge was made for these adornments.{{sfn|''Spink's Circular''|May 1904|p=7576}}

Zerbe also promoted the pieces to the numismatic community.  Although the $3 price was not high by later standards, triple face value was considered excessive by many coin collectors, and the coins did not sell well. Efforts by Zerbe to promote the pieces included proposing that the government produce a billion-dollar gold piece to be exhibited at the fair, and co-ordinating sales with the vendors of near-worthless replicas of tiny gold pieces struck privately in California in pioneer days, which were half price with the purchase of a dollar coin.  Thomas L. Elder, a dealer coming into prominence at that time, spoke out against Zerbe, calling him a huckster whose advertising was misleading and who was bringing discredit upon coin dealers.{{sfn|Swiatek & Breen|p=120}}{{sfn|Bowers ''Encyclopedia'', Part 6}}

The organizers, including Zerbe, promised to support the $3 issue price against the possibility of price drops on the secondary market.{{sfn|''Spink's Circular''|May 1904|p=7576}}  Prices of the Columbian half dollar and Lafayette dollar had fallen and remained below their issue prices.{{sfn|''The Numismatist''|January 1903|p=24}} By November 1903, only about 10,000 of the gold pieces had been sold, including sales to the fair's promoters and others interested in it.{{sfn|''The Numismatist''|November 1903|p=219}} According to numismatist [[Q. David Bowers]], fairgoers likely accounted for several thousand coins, but the bulk of the distribution was to coin dealers and collectors.{{sfn|Bowers ''Encyclopedia'', Part 6}}  Zerbe sold them at his coin exhibit for years afterwards; coin dealer B. Max Mehl bought thousands from Zerbe at just over face value.  These were sold in Mehl's mail order sales through the 1920s.{{sfn|Bowers ''Encyclopedia'', Part 7}}  Despite efforts by Zerbe which Bowers finds "enthusiastic or even heroic", only about 35,000 were sold to the public; the remaining 215,000 were returned to the Mint and melted around 1914.{{sfn|Bowers ''Encyclopedia'', Part 6}}

Numismatist David M. Bullowa in 1938 noted that the Mint kept no records of how many of each variety was melted, but that he thought that about 10% more of the McKinley issue was sold.  Bowers, writing about a half century later, opined to the contrary; that in his experience and in grading service reports, the Jefferson coin was slightly more prevalent.{{sfn|Bowers ''Encyclopedia'', Part 6}} Swiatek, in his 2012 book, prints statistics showing the number of pieces examined by the numismatic grading services, indicating more Jefferson dollars than McKinley.{{sfn|Swiatek|pp=73–74}}

Despite Zerbe's statement that he would support the issue price of the coins at $3, he did not do so and the price of the dollars fell to about $2 by late 1905.{{sfn|Bowers ''Encyclopedia'', Part 6}} Their market price again reached $3 by about 1915, and thereafter continued to rise.{{sfn|Bowers ''Encyclopedia'', Part 7}}  The 2014 edition of [[R.S. Yeoman]]'s ''[[A Guide Book of United States Coins]]'' (the ''Red Book'') lists both the Jefferson and the McKinley variety at prices ranging from $500 in Almost Uncirculated (AU-50) [[coin grading|condition]] to $2,150 in near pristine MS-66 condition, though the Jefferson is more expensive in some intermediate grades.{{sfn|Yeoman|p=286}}

Zerbe stated in 1905 that he "was the only man to sell 50,000 dollars at $3 apiece".{{sfn|''Philatelic West''|August 31, 1905}} In 1923, he wrote in an article that the Louisiana Purchase dollars had always sold for $3 or more "for the particular reason that the one in charge of their sale felt a price protection obligation to every purchaser."{{sfn|Bowers ''Encyclopedia'', Part 7}} He did not, however, identify himself as "the one in charge of their sale".{{sfn|Bowers ''Encyclopedia'', Part 7}}

== References and bibliography ==

{{reflist|20em}}

'''Books'''

* {{cite book
  | author = Bureau of the Mint
  | year = 1904
  | title = Laws of the United States Relating to the Coinage
  | publisher = United States Government Printing Office
  | location = Washington, D.C.
  | url = https://books.google.com/books?id=rmtAAAAAYAAJ
  | ref = {{sfnRef|Bureau of the Mint}}
  }}
* {{cite book
  | last = Mehl
  | first = B. Max
  | year = 1937
  | title = The Commemorative Coinage of the United States
  | publisher = B. Max Mehl
  | location = Fort Worth, TX
  | ref = {{sfnRef|Mehl}}
}}
* {{cite book
  | last = Slabaugh
  | first = Arlie R.
  | edition = second
  | year = 1975
  | title = United States Commemorative Coinage
  | publisher = Whitman Publishing (then a division of Western Publishing Company, Inc.)
  | location = Racine, WI
  | isbn = 978-0-307-09377-6
  | ref = {{sfnRef|Slabaugh}}
  }}
* {{cite book
  | last = Swiatek
  | first = Anthony
  | year = 2012
  | title = Encyclopedia of the Commemorative Coins of the United States
  | publisher = KWS Publishers
  | location = Chicago
  | isbn = 978-0-9817736-7-4
  | ref = {{sfnRef|Swiatek}}
  }}
* {{cite book
  | last = Swiatek
  | first = Anthony
  | last2 = Breen
  | first2 = Walter
  | authorlink2 = Walter Breen
  | year = 1981
  | title = The Encyclopedia of United States Silver & Gold Commemorative Coins, 1892 to 1954
  | publisher = Arco Publishing
  | location = New York
  | isbn = 978-0-668-04765-4
  | ref = {{sfnRef|Swiatek & Breen}}
  }}
* {{cite book
  | last = Taxay
  | first = Don
  | authorlink = Don Taxay
  | year = 1967
  | title = An Illustrated History of U.S. Commemorative Coinage
  | publisher = Arco Publishing
  | location = New York
  | isbn = 978-0-668-01536-3
  | ref = {{sfnRef|Taxay}}
  }}
* {{cite book
  | last = Vermeule
  | first = Cornelius
  | authorlink = Cornelius Clarkson Vermeule III
  | year = 1971
  | title = Numismatic Art in America
  | publisher = The Belknap Press of Harvard University Press
  | location = Cambridge, MA
  | isbn = 978-0-674-62840-3
  | ref = {{sfnRef|Vermeule}}
  }}
* {{cite book
  | last = Yeoman
  | first = R.S.
  | authorlink = Richard S. Yeoman
  | year = 2013
  | title = [[A Guide Book of United States Coins]] 2014
  | edition = 67th
  | publisher = Whitman Publishing LLC
  | location = Atlanta, GA
  | isbn = 978-0-7948-4180-5
  | ref = {{sfnRef|Yeoman}}
  }}

'''Other sources'''

* {{cite web
  | title = ANA Presidents
  | publisher = [[American Numismatic Association]]
  | url = http://www.money.org/membership/about-the-ana/ana-presidents.aspx
  | accessdate = June 27, 2013
  | ref = {{sfnRef|American Numismatic Association}}
  }}
* {{cite journal
  | last = Arnold
  | first = George C.
  | title = A gold dollar proposition
  | journal = [[The Numismatist]]
  | publisher = [[American Numismatic Association]]
  | location = Colorado Springs, CO
  | url =https://books.google.com/books?id=WpEUAAAAYAAJ&pg=PA24#v=onepage&q&f=false
  | date = January 1903
  | page =24
 | ref = {{sfnRef|''The Numismatist''|January 1903}}
  }}
* {{cite journal
  | title = Miscellaneous
  | journal = [[The Numismatist]]
  | publisher = [[American Numismatic Association]]
  | location = Colorado Springs, CO
  | url =https://books.google.com/books?id=WpEUAAAAYAAJ&pg=PA219#v=onepage&q&f=false
  | date = November 1903
  | pages = 218–219
 | ref = {{sfnRef|''The Numismatist''|November 1903}}
  }}
* {{cite web
  | last = Bowers
  | first = Q. David
  | authorlink = Q. David Bowers
  | title = Chapter 9: Gold commemoratives, Part 4
  | work = Commemorative Coins of the United States: A Complete Encyclopedia
  | url = http://www.pcgs.com/books/commemoratives/Chapter09-004.aspx
  | publisher = Professional Coin Grading Service
  | accessdate = July 14, 2013
  | ref = {{sfnRef|Bowers ''Encyclopedia'', Part 4}}
  }}
* {{cite web
  | last = Bowers
  | first = Q. David
  | authorlink = Q. David Bowers
  | title = Chapter 9: Gold commemoratives, Part 5
  | work = Commemorative Coins of the United States: A Complete Encyclopedia
  | url = http://www.pcgs.com/books/commemoratives/Chapter09-005.aspx
  | publisher = Professional Coin Grading Service
  | accessdate = July 14, 2013
  | ref = {{sfnRef|Bowers ''Encyclopedia'', Part 5}}
  }}
* {{cite web
  | last = Bowers
  | first = Q. David
  | authorlink = Q. David Bowers
  | title = Chapter 9: Gold commemoratives, Part 6
  | work = Commemorative Coins of the United States: A Complete Encyclopedia
  | url = http://www.pcgs.com/books/commemoratives/Chapter09-006.aspx
  | publisher = Professional Coin Grading Service
  | accessdate = July 14, 2013
  | ref = {{sfnRef|Bowers ''Encyclopedia'', Part 6}}
  }}
* {{cite web
  | last = Bowers
  | first = Q. David
  | authorlink = Q. David Bowers
  | title = Chapter 9: Gold commemoratives, Part 7
  | work = Commemorative Coins of the United States: A Complete Encyclopedia
  | url = http://www.pcgs.com/books/commemoratives/Chapter09-007.aspx
  | publisher = Professional Coin Grading Service
  | accessdate = July 14, 2013
  | ref = {{sfnRef|Bowers ''Encyclopedia'', Part 7}}
  }}
* {{cite journal
  | last = Hunt
  | first = Jim
  | last2 = Wells
  | first2 = Jim
  | title = Numismatics of the Lewis and Clark Exposition
  | journal = [[The Numismatist]]
  | publisher = [[American Numismatic Association]]
  | location = Colorado Springs, CO
  | date = March 2004
  | pages =40–44
 | ref = {{sfnRef|''The Numismatist''|March 2004}}
  }}
* {{cite journal
  | title = When one dollar is worth two <!-- I (Wehwalt) can't prove it, but this was almost certainly written by Farran Zerbe -->
  | journal = The Philatelic West
  | publisher = L.T. Brodstone
  | location = Superior, NE
  | url =https://books.google.com/books?id=O102AQAAMAAJ&pg=PT839&dq=louisiana+zerbe+dollar&hl=en&sa=X&ei=3uHiUejfB8TrrQehxoG4Dw&ved=0CDMQ6AEwAQ#v=onepage&q&f=false
  | date = August 31, 1905
  | pages = unnumbered
 | ref = {{sfnRef|''Philatelic West''|August 31, 1905}}
  }}
* {{cite journal
  | title = Louisiana gold dollars
  | journal = Spink & Son's Monthly Numismatic Circular
  | publisher = Spink & Son's
  | location = London
  | url =https://books.google.com/books?id=wAorAAAAMAAJ&lpg=RA1-PA9&ots=NBzOV1rhzn&dq=the%20numismatist%201903&pg=RA16-PA136#v=onepage&q&f=false
  | date = May 1904
  | pages = 7576–7577
 | ref = {{sfnRef|''Spink's Circular''|May 1904}}
  }}

{{Coinage (United States)}}
{{Portal bar|Missouri|Numismatics||United States}}
{{featured article}}

[[Category:1903 introductions]]
[[Category:Commemorative coins of the United States]]
[[Category:United States gold coins]]