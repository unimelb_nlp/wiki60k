{{Infobox company
| name           = G.H. Bass & Co.
| logo           = 
| logo_size      = 
| logo_alt       = 
| logo_caption   = 
| logo_padding   = 
| image          = G. H. Bass & Co. Shoe Factory, Wilton, ME.jpg
| image_size     = thumb
| image_alt      = 
| image_caption  = The G. H. Bass & Co. shoe factory, Wilton, Maine; from a 1914 postcard. Built on Wilton Stream in 1904 and known as No. 1, the factory closed in 1998.
| native_name    = 
| native_name_lang =  <!-- Use ISO 639-1 code, e.g. "fr" for French. For multiple names in different languages, use {{lang|[code]|[name]}}. -->
| former_name    = 
| type           = 
| industry       = Footwear
| founded        = {{start date|1876}}
| founder        = George Henry Bass
| hq_location    = 
| hq_location_city = Wilson Stream
| hq_location_country = 
| area_served    =         <!-- or: | areas_served = -->
| key_people     = 
| products       = 
| brands         = National Plow Shoe
| services       = 
| owner          =               <!-- or: | owners = -->
| ratio          = <!-- for BANKS ONLY -->
| rating         = <!-- for BANKS ONLY -->
| website        =             <!-- or: | homepage = --><!-- {{URL|example.com}} -->
}}

'''G.H. Bass & Co.''' is an American [[footwear]] [[brand]] founded by George Henry Bass in 1876.

== History ==
George Henry Bass (b.  Wilton, Maine, 1843) began to work in 1876 in the [[shoemaking]] business as junior partner in E.P. Packard & Co. in Wilton.  By 1879, he became the sole owner, and changed the company name to G.H. Bass & Co. In 1887, the factory moved to Wilson Stream in order to use water-powered machinery.

The National Plow Shoe is created for farmers in 1892. In 1906, the 1st Bass [[moccasin]] made is the “Bass Moccasin Cruiser”, designed to be a light and flexible shoe worn by woodsmen. By 1906 G.H. Bass & Co. becomes incorporated. In 1910,  “Rangeley” style moccasin is created, and seven years later  G.H. Bass & Co. created the Ski Moccasin. By order of the government, in 1918,  G.H. Bass & Co. created The official Aviation Boot. It was designed for use in high altitudes wielding protection from the extreme cold. In 1920,  G.H. Bass & Co. created a new style of moccasin called the “Woc-O-Moc”. Due to the evolution of the company’s core products, in 1924 the Bass Shoe “For Hard Service” becomes “Bass Outdoor Footwear” in G.H. Bass & Co. catalogs.<ref>[http://pvh.com/brands_heritage_bass.aspx "Our Brands - Bass."] Our Brands - Bass. N.p., n.d. Web. 26 June 2012.</ref><ref>Breton, Stella. "A Visit to Bass' Attic." n.d.: n. pag. Print.</ref>
After the death of George Henry Bass in 1925, his sons John R. Bass and Willard S. Bass took over   management of the business  (Willard Bass becomes President and John Bass becomes treasurer). In 1926, the firm opened In-Stock Department at 1104 Commerce Street in Dallas, Texas, to service Southern shoe dealers quicker.<ref>Kirchofer, Tom. [http://articles.latimes.com/print/1998/sep/11/business/fi-21683 "Closing Bass Factory Puts an End to an Era."] Los Angeles Times. Los Angeles Times, 11 Sept. 1998. Web. 02 July 2012.
</ref> In 1928, the firm sued  Abbott Co. for infringement of patent for Overlap Seam and trademark of the “Quail Hunter” and “Ike Walton” styles, but lost the  suit; in 1929, Bass bought Abbott Co., in order to reacquire exclusive rights to the overlap seam and Ike Walton, “Sportocasin” (Moccasins with spiked soles, golf shoes).  During 1928 through 1938 Admiral Richard E. Byrd wore Bass ski boots during the First, Second and Third Antarctic Expedition.<ref>Bass, Streeter. G.H. Bass Company, 1876-1976. S.l.: S.n., 1976. Print.
</ref>
In 1931  the firm. opened an office in N.Y. on the 25th floor of 11 West 42nd Street New York, N.Y. in The Salmon Tower Building. During 1936, Bass “Weejuns” are first made. Four years later, in 1940 the original suede “Buc” style is created. In 1948,  the firm. outfitted the American Olympic Team with footwear. During WW II , the firm developed a cold-weather boot for U.S. Army’s 10th Mountain Division. In 1967, Sunjuns, a Women’s sandal is first introduced. As they continue to grow in 1968,  G.H. Bass & Co. acquired Burgess Shoe Store; giving the firm flexibility of direct retail outlet.<ref>Kessler, Eugene O. G.H. Bass & Co Employee Handbook. 4th ed. N.p.: n.p., 1985. Print.</ref> In 1969, the firm acquired Rosemount Engineering Co.’s Consumer Product Division (Minnesota).

In 1978, Chesebrough-Pond’s Inc (Greenwich, Connecticut) purchased G.H. Bass & Co.; they sold it to  PVH Corp. in 1987.<ref>Brooks, Nancy R. [http://articles.latimes.com/print/1987-07-31/business/fi-310_1_phillips-van-heusen "Phillips-Van Heusen Will Buy Shoemaker G. H. Bass."] Los Angeles Times. Los Angeles Times, 31 July 1987. Web. 02 July 2012.
</ref> In 1980, the first  G.H. Bass & Co. Country Shop was opened at the May Company Store in Viejo, California. That same year G.H. Bass & Co. began to publish in-house newspaper called The Bass Inner Soul.

In the fall of 1988,  G.H. Bass & Co. unveils the Signature Collection, of three shoe styles including the Buc, Rangeley and Weejuns. In 1993, The Classic Penny style of Weejuns Collection is renamed- Leavitt Penny. In 1997, the firm entered e-commerce with online retail website www.ghbass.com. The website is currently run under Harbor Wholesale Ltd. operating with the license to sell G.H. Bass & Co. shoes.<ref>[https://bassshoes.harborghb.com/bass-shoes-history "About G.H. Bass."] G.H. Bass & Co. Official Online Store. N.p., n.d. Web. 26 June 2012.
</ref> In 2011, [[Tommy Hilfiger]] launched a limited edition footwear collection in collaboration with  G.H. Bass & Co. The collection was centered around the “Weejuns” penny loafer.<ref>[http://www.gq.com/style/blogs/the-gq-eye/2011/10/this-just-in-tommy-hilfiger-x-gh-bass-weejun-loafers.html "This Just In: Tommy Hilfiger X G.H. Bass "Weejun" Loafers: The GQ Eye: GQ on Style: GQ."] GQ. Sean Hotchkiss, 19 Oct. 2011. Web. 19 July 2012.</ref><ref>* [http://articles.latimes.com/keyword/g-h-bass-co "In The News G.H. Bass & Co."] Los Angeles Times. Los Angeles Times, 11 Aug. 1989. Web. 04 September 2012.
</ref>

== Recent developments ==
In 2006 Harbor Wholesale Ltd. becomes  G.H. Bass & Co. licensee for retail, while PVH services wholesale. In 2008 BASS opened its first two non-outlet stores in Massachusetts. In 2011 Tommy Hilfiger, created an upscale limited-edition collaboration of “The Penny Loafer- Originals with a Twist”. PVH’s  G.H. Bass & Co. retail division opens its first store in Canada in Calgary, AB<ref>[http://www.pvh.com/investor_relations_press_release_article.aspx?reqid=1617175 "Investor Relations - Tommy Hilfiger for G.H. Bass & Co."] Investor Relations - Tommy Hilfiger for G.H. Bass & Co. N.p., May 2011. Web. 26 June 2012.
</ref>
Nov. 2013 G.H Bass was purchased from PVH by G-III.  <ref>[https://www.g-iii.com/index.php/about/giiis-history/ "G-III History" G-III Apparel Group. Web. 28 July 2014.
</ref>

== References ==
{{reflist}}

==Further reading==
* Barry, William D. G.H. Bass and Company: A Vignetted History. 1988. MS. Portland, Maine.

== External links ==
* Official website US & CA - https://www.ghbass.com
* Official website UK & EU - https://www.ghbass-eu.com

{{DEFAULTSORT:G.H. Bass and Co.}}
[[Category:Shoe companies of the United States]]