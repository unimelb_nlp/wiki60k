{{good article}}
{{infobox road
|state=MI
|type=US
|route=23
|maint=none
}}
There have been five different '''business routes of US Highway&nbsp;23''' in the state of Michigan. These [[business routes]] were designated along former sections of [[U.S. Route 23 in Michigan|US Highway&nbsp;23]] (US&nbsp;23) to provide signed access from the main highway to the downtowns of cities bypassed by new routings of US&nbsp;23. Two are still extant, connecting through downtown [[Ann Arbor, Michigan|Ann Arbor]] and [[Rogers City, Michigan|Rogers City]]. Three others have been [[decommissioned highway|decommissioned]]. The former Business US&nbsp;23 (Bus. US&nbsp;23) in [[Fenton, Michigan|Fenton]] was split in half during the 1970s and later completely turned back to local control in 2006. The former business loops through [[Saginaw, Michigan|Saginaw]] and [[Bay City, Michigan|Bay City]] were renumbered as business loops of [[Interstate 75 in Michigan|Interstate 75]] in the 1960s.
{{Clear}}

==Ann Arbor==
{{infobox road small
|state= MI
|type=US
|route=23
|subtype=Bus
|location= [[Ann Arbor, Michigan|Ann Arbor]]
|length_mi=5.781
|length_ref=<ref name=PRFA>{{cite MDOT PRFA |link= yes |access-date= December 18, 2015}}</ref>
|established=1962<ref name=MSDH62A2>{{cite MDOT map |year= 1962 |section= M12}}</ref><ref name=MSDH63A2>{{cite MDOT map |year= 1963 |section= M12}}</ref>
}}
'''Business US Highway&nbsp;23''' ('''Bus. US&nbsp;23''') is a [[business loop]] of [[U.S. Route 23 in Michigan|US&nbsp;23]] through downtown [[Ann Arbor, Michigan|Ann Arbor]].  The southern end is at an interchange with US&nbsp;23 on the city line with [[Pittsfield Township, Michigan|Pittsfield Township]]. This interchange also marks the western terminus of [[M-17 (Michigan highway)|M-17]], and the eastern end of a [[concurrency (road)|concurrency]] with [[Interstate 94 Business (Ann Arbor, Michigan)|Business Loop Interstate&nbsp;94]] (BL I-94). From this interchange westward, BL I-94/ Bus. US&nbsp;23 follows Washtenaw Avenue along a five-lane street past commercial areas to County Farm Park and then continues as a four-lane roadway through residential neighborhoods. Washtenaw Avenue turns more northwesterly at the intersection with Stadium Boulevard southeast of [[Burns Park (Ann Arbor, Michigan)|Burns Park]]. Near the [[University of Michigan#Central Campus|Central Campus of the University of Michigan]], Washtenaw Avenue turns due north to cross part of campus before BL I-94/Bus. US&nbsp;23 turn due west onto Huron Street near Palmer Field. Bus. US&nbsp;23 follows BL I-94 and the four-lane Huron Street into downtown Ann Arbor to the intersection with  Main Street. There, Bus. US&nbsp;23 turns northward onto Main Street and exits downtown. Main Street has four lanes as it runs northward into a residential area. It parallels part of the [[Huron River (Michigan)|Huron River]] before ending at an interchange with the [[M-14 (Michigan highway)|M-14]] freeway about a mile and a third (2.2&nbsp;km) north of downtown. At that interchange, Bus. US&nbsp;23 merges onto the freeway and runs concurrently with M-14, crossing the Huron River. There is one interchange for Barton Drive and Whitmore Lake Road on the northern bank of the river. The freeway runs through a wooded area and then after about {{convert|1|mi|km|spell=in}}, it meets US&nbsp;23 at an interchange in [[Ann Arbor Township, Michigan|Ann Arbor Township]] that marks the northern terminus of the business loop.<ref name=MDOT15AA>{{cite MDOT map |year= 2015 |inset= Detroit |sections= E3–F4}}</ref><ref name=googleAA>{{google maps |url= https://www.google.com/maps/dir/42.2547514,-83.6842829/42.2814436,-83.7484842/42.315995,-83.7397295/@42.2790628,-83.7515045,15380m/data=!3m1!1e3!4m2!4m1!3e0 |title= Overview Map of Bus. US&nbsp;23 in Ann Arbor |access-date= January 6, 2016}}</ref>

[[File:Interstate 94 Business Route Ann Arbor Washtenaw Avenue.JPG|thumb|left|Washtenaw Avenue facing east]]
In 1962, the northern and eastern freeway bypass of Ann Arbor was completed. At that time, the former routing of US&nbsp;23 through downtown and a section of freeway north of the Huron River was redesignated as Bus. US&nbsp;23.<ref name=MSDH62A2/><ref name=MSDH63A2/> Two years later, M-14 was rerouted to follow the US&nbsp;23 23 freeway around the north side of Ann Arbor. It overlapped the business loop from the northern end of its freeway segment to Main Street and along Main Street into downtown.<ref name=MSDH64A2>{{cite MDOT map |year= 1964 |section= M12}}</ref><ref name=MSDH65A2>{{cite MDOT map |year= 1965 |section= M12}}</ref> The next year, in 1965, this overlap was shortened when the rest of the M-14 freeway westward from Main Street to [[Interstate 94 in Michigan|I-94]] was completed.<ref name=MSDH65A2/><ref name=MSDH66A2>{{cite MDOT map |year= 1966 |section= M12}}</ref>
{{Clear}}
'''Major intersections'''<br/>
{{MIinttop|county=Washtenaw|length_ref=<ref name=PRFA/>}}
{{MIint
|location_special=[[Pittsfield Township, Michigan|Pittsfield Township]]–[[Ann Arbor, Michigan|Ann Arbor]] city line
|mile=0.000
|mile2=0.018
|type=concur
|road={{jct|state=MI|BL|94|US|23|dab1=Ann Arbor|dir1=east|city1=Flint|location2=[[Toledo, Ohio|Toledo]]}}<br/>{{jct|state=MI|M|17|dir1=east|city1=Ypsilanti|name1=Washtenaw Avenue}}
|notes=Eastern end of BL I-94 concurrency; exit&nbsp;37 on US&nbsp;23; western terminus of M-17}}
{{MIint
|location=Ann Arbor
|lspan=3
|mile=3.429
|type=concur
|road={{jct|state=MI|BL|94|dir1=west|name=Huron Street}}
|notes=Western end of BL I-94 concurrency}}
{{MIint
|mile=4.795
|type=concur
|road={{jct|state=MI|M|14|dir1=west}}
|notes=Western end of M-14 concurrency; exit&nbsp;3 on M-14}}
{{MIint
|mile=4.991
|road= Barton Drive, Whitmore Lake Road
|notes=Exit&nbsp;4 on Bus. US&nbsp;23/M-14}}
{{MIint
|location=Ann Arbor Township
|mile=5.741
|mile2=5.781
|type=concur
|road={{jct|state=MI|US|23|M|14|dir2=east|city1=Flint|location2=[[Toledo, Ohio]]|city3=Detroit}}
|notes=Eastern end of M-14 concurrency; exit&nbsp;45 on US&nbsp;23}}
{{jctbtm|keys=concur}}

==Fenton==
{{infobox road small
|state= MI
|type=US
|route=23
|subtype=Bus
|location= [[Fenton, Michigan|Fenton]]
|length_mi=3.009
|length_ref=<ref name=PRFA/>
|established=1958<ref name=MSHD58F>{{cite MDOT map |year= 1958 |section= L12}}</ref><ref name=MSDH60F>{{cite MDOT map |year= 1960 |section= L12}}</ref>
|decommissioned=2006<ref name=TOM06>{{cite MDOT map |year= 2006T |section= L12}}</ref><ref name=TOM07>{{cite MDOT map |year= 2007T |section= L12}}</ref>
|history=Southern {{convert|1.723|mi|km|abbr=on}} unsigned after 1975<ref name=MDSHT75F>{{cite MDOT map |year= 1975 |section= L12}}</ref><ref name=MDSHT76F>{{cite MDOT map |year= 1976 |section= L12}}</ref>
}}
'''Business US Highway&nbsp;23''' ('''Bus. US&nbsp;23''') was a [[business route]] through downtown [[Fenton, Michigan|Fenton]]. At the time it was turned over to local control, it was signed as business spur from downtown to the [[U.S. Route 23 in Michigan|US&nbsp;23]] freeway, but the state maintained a southern section that previously completed the route as a loop. At the southern end, this [[unsigned highway]] started at US&nbsp;23 at the Owen Road interchange (exit&nbsp;78) and continued eastward on Owen Road past Fenton High School and various businesses. At the intersection with Shiawassee Avenue, Bus. US&nbsp;23 followed Shiawassee through a residential area toward the southern end of downtown. At the intersection with LeRoy Street, the unsigned business loop turned northward and across the [[Shiawassee River]]. At the intersection with River Street, state maintenance ended. On the northern end of downtown at the intersection of LeRoy Street and Silver Lake Road, it resumed. Bus. US&nbsp;23 followed Silver Lake Road westward out of downtown through a residential area and northwesterly to an interchange with US&nbsp;23 at exit&nbsp;79.<ref name= TOM06/><ref name=googleF>{{google maps |url= https://www.google.com/maps/dir/42.7896872,-83.7381458/42.7958189,-83.7047889/42.8038576,-83.7271594/@42.7968107,-83.7390077,7626m/data=!3m2!1e3!4b1!4m2!4m1!3e0 |title= Overview Map of Former Bus. US&nbsp;23 in Fenton |access-date= January 6, 2016 |link= no}}</ref>

The US&nbsp;23 bypass of Fenton opened as a freeway west of downtown in 1958. The former route of US&nbsp;23 along Shiawassee Avenue and LeRoy Street in Fenton was redesignated as a business loop at this time, and the state assumed control of Silver Lake Road to connect it back to the freeway northwest of town.<ref name=MSHD58F/><ref name=MSDH60F/> Three years later, the US&nbsp;23 freeway was extended southward from the [[Livingston County, Michigan|Livingston]]–[[Genesee County, Michigan|Genesee]] county line; at that time, the freeway connections were reconfigured and Bus. US&nbsp;23 was shifted to use Owen Road between a new freeway interchange and the rest of the business loop at Shiawassee Avenue.<ref name=MSHD61F>{{cite MDOT map |year= 1961 |section= L12}}</ref><ref name=MSDH62F>{{cite MDOT map |year= 1962 |section= L12}}</ref> In the middle of the 1970s, city officials redeveloped downtown and closed two blocks of LeRoy Street in 1975.<ref>{{cite news |title= Leroy Street Opening, an Historical Milestone |date= November 11, 2000 |url= http://www.tctimes.com/site/news.cfm?newsid=6361459&BRD=2524&PAG=461&dept_id=494488&rfi=8 |archive-url= https://web.archive.org/web/20071009114907/http://www.tctimes.com/site/news.cfm?newsid=6361459&BRD=2524&PAG=461&dept_id=494488&rfi=8 |archive-date= October 9, 2007  |work= [[Tri-County Times]] |location= Fenton, MI |access-date= January 6, 2016}}</ref> After this closure, the business loop was split into two sections, and only the northern one was retained as a signed state highway.<ref name=MDSHT75F/><ref name=MDSHT76F/> The southern segment was retained as an unsigned state highway<ref name=TOM06/> until both segments were turned over to local control in 2006.<ref name=TOM06/><ref name=TOM07/>
{{Clear}}
'''Major intersections'''<br/>
{{MIinttop|location=Fenton|county=Livingston|length_ref=<ref name=PRFA/>|former=yes}}
{{MIint
|mile=0.000
|road={{jct|state=MI|US|23|city1=Flint|city2=Ann Arbor}}
|notes=Southern terminus of southern unsigned segment; exit&nbsp;78 on US&nbsp;23}}
{{MIint
|mile=1.723
|road=Leroy Street<br/>River Street
|notes=Northern terminus of southern unsigned segment}}
{{jctgap}}
{{MIint
|mile=0.000
|road=Leroy Street<br/>Silver Lake Road
|notes=Southern terminus of northern signed segment}}
{{MIint
|mile=1.286
|road={{jct|state=MI|US|23|city1=Flint|city2=Ann Arbor}}
|notes=Northern terminus of northern signed segment; exit&nbsp;79 on US&nbsp;23}}
{{jctbtm}}

==Saginaw==
{{infobox road small
|state= MI
|type=US 1948
|route=23
|subtype=Bus
|location= [[Saginaw, Michigan|Saginaw]]
|length_mi= 8.006
|length_ref=<ref name=PRFA/>
|established=1953<ref name=MSHD53-04>{{cite MDOT map |date=1953-04-15 |inset=Saginaw}}</ref><ref name=MSHD53-10>{{cite MDOT map |date=1953-10-01 |inset=Saginaw}}</ref>
|decommissioned=1961<ref name=MSHD61S>{{cite MDOT map |year= 1961 |inset= Saginaw }}</ref><ref name=MSHD62S>{{cite MDOT map |year= 1962 |inset= Saginaw}}</ref>
}}
'''Business US Highway&nbsp;23''' ('''Bus. US&nbsp;23''') was a [[business loop]] that ran through downtown [[Saginaw, Michigan|Saginaw]].  It started at an intersection between [[U.S. Route 10 in Michigan|US&nbsp;10]] and [[U.S. Route 23 in Michigan|US&nbsp;23]] in [[Bridgeport, Michigan|Bridgeport]] southeast of Saginaw. From there, it [[concurrency (road)|ran concurrently]] along US&nbsp;10 (Dixie Highway) northwesterly into Saginaw. Once in downtown, the business loop turned northward on Washington Street, running parallel to the eastern banks of the [[Saginaw River]] through downtown. At the intersection between Washington Avenue, Washington Road and Veterans Memorial Parkway, Bus. US&nbsp;23 terminated.<ref name=MSHD61S/><ref name=googleS>{{google maps |url= https://www.google.com/maps/dir/43.35835,-83.8809185/43.4499853,-83.9122897/@43.4049178,-83.9449429,13z/data=!3m1!4b1!4m9!4m8!1m5!3m4!1m2!1d-83.9389805!2d43.4333414!3s0x8823c27b8ada287b:0x56f2d510f571015f!1m0!3e0 |title= Overview Map of Former Bus. US&nbsp;23 in Saginaw |access-date= January 6, 2016 |link= no}}</ref>

In 1953, the initial eastern bypass of Saginaw was built as a two-lane highway, and the former routing through downtown was redesignated Bus. US&nbsp;23.<ref name=MSHD53-04/><ref name=MSHD53-10/> This bypass was upgraded in 1961 to a full freeway as part of I-75/US&nbsp;23, and the business loop through downtown was redesignated [[Interstate 75 Business (Saginaw, Michigan)|Business Loop I-75]].<ref name=MSHD61S/><ref name=MSHD62S/>
{{Clear}}
'''Major intersections'''<br/>
{{MIinttop|county=Saginaw|length_ref=<ref name=PRFA/>|former=yes}}
{{MIint
|location=Bridgeport Township
|mile=0.000
|type=concur
|road={{jct|state=MI|US 1948|10|US 1948|23|dir1=east|city1=Bay City|city2=Flint}}
|notes=Southern end of US&nbsp;10 concurrency}}
{{MIint
|location=Saginaw
|lspan=3
|mile=4.407
|road={{jct|state=MI|M 1948|46|dir1=west|name1=Holland Avenue|city1=Muskegon|city2=Port Sanilac}}
|notes=}}
{{MIint
|mile=6.005
|type=concur
|road={{jct|state=MI|US 1948|10|dir1=west|city1=Midland}}<br />{{jct|state=MI|M 1948|13|dir1=south|name1=Washington Avenue}}
|notes=Northern end of US&nbsp;10 concurrency; northern terminus of M-13}}
{{MIint
|mile=8.006
|road={{jct|state=MI|US 1948|23|M 1948|81|dir2=east|name1=Veterans Memorial Parkway/Washington Road|name2=Washington Road|city1=Bay City|city2=Flint|city3=Caro}}
|notes=}}
{{jctbtm|keys=concur}}

==Bay City==
{{infobox road small
|state= MI
|type=US 1948
|route=23
|subtype=Bus
|location= [[Bay City, Michigan|Bay City]]
|length_mi=2.714
|length_ref=<ref name=PRFA/>
|established=1941<ref name=MSDH40-12>{{cite MDOT map |date= 1940-12-01 |inset= Bay City}}</ref><ref name=MSHD41-03>{{cite MDOT map |date= 1941-03-21 |inset= Bay City}}</ref>
|decommissioned=1961<ref name=MSHD60BC>{{cite MDOT map |year= 1960 |inset= Bay City}}</ref><ref name=MSHD61BC>{{cite MDOT map |year= 1961 |inset= Bay City}}</ref>
}}
'''Business US Highway 23''' ('''Bus. US&nbsp;23''') was a [[business loop]] through downtown [[Bay City, Michigan|Bay City]]. It started at the intersection of where US&nbsp;23 turned off Broadway Street westward onto Lafayette Avenue. From this point, Bus. US&nbsp;23 ran east on Lafayette Avenue for two blocks and then turned northward onto Garfield Avenue, running parallel to, but inland from, the [[Saginaw River]] into downtown Bay City. As it approached downtown, the business loop jogged off Garfield onto Washington Avenue. At the intersection with 7th Street, Bus. US&nbsp;23 turned westward to cross the Saginaw River. On the opposite side, the business loop followed Jenny Street westbound and Thomas Street eastbound along a [[one-way pair]]ing of streets. At the intersection with US&nbsp;23 (Euclid Avenue), the business loop terminated.<ref name=MSHD60BC/><ref name=googleBC>{{google maps |url= https://www.google.com/maps/dir/43.5796176,-83.8935366/43.5987856,-83.9153222/@43.5892121,-83.9194427,14z/data=!4m9!4m8!1m5!3m4!1m2!1d-83.890757!2d43.5961446!3s0x8823e05324b7019f:0x123b21b68d6c1c62!1m0!3e0 |title= Overview Map of Former Bus. US&nbsp;23 in Bay City |access-date= January 6, 2016 |link= no}}</ref>

With the construction of a new bridge across the Saginaw River in 1941 to connect Lafayette and Salzburg avenues, US&nbsp;23 was rerouted to use that new bridge. The former routing of the mainline highway through downtown was redesignated Bus. US&nbsp;23 at that time.<ref name=MSDH40-12/><ref name=MSHD41-03/> Twenty years later, with the opening of the new freeway for [[Interstate 75 in Michigan|Interstate 75]] (I-75) near Bay City, US&nbsp;23 was rerouted to follow I-75. The route of Bus. US&nbsp;23 was redesignated as a part of [[Interstate 75 Business (Bay City, Michigan)|Business Loop I-75]] at this time.<ref name=MSHD60BC/><ref name=MSHD61BC/>
{{Clear}}
'''Major intersections'''<br/>
{{MIinttop|location=Bay City|county=Bay|former=yes|length_ref=<ref name=PRFA/>}}
{{MIint
|mile=0.000
|road={{jct|state=MI|US 1948|23|city1=Standish|city2=Saginaw}}
|notes=}}
{{MIint
|mile=1.234
|mile2=1.302
|type=concur
|road={{jct|state=MI|M 1948|15|M 1948|25||dir1=south|dir2=east}}
|notes=Eastern end of M-15/M-25 concurrency}}
{{MIint
|mile=1.957
|road={{jct|state=MI|M 1948|47|dir1=north}}
|notes=Northern terminus of M-47}}
{{MIint
|type=concur
|mile=2.714
|road={{jct|state=MI|US 1948|23|M 1948|47|city1=Standish|city2=Saginaw}}<br/>{{jct|state=MI|M 1948|20|dir1=west|city1=Midland}}<br/>{{jct|state=MI|M 1948|15|M 1948|25|dir1=south|dir2=east}}
|notes=Western end of M-15/M-25 concurrency; eastern terminus of M-20}}
{{jctbtm|keys=concur}}

==Rogers City==
{{infobox road small
|state= MI
|type=US
|route=23
|subtype=Bus
|location= [[Rogers City, Michigan|Rogers City]]
|length_mi=4.074
|length_ref=<ref name=PRFA/>
|established=1942<ref name=MSDH41-12RC>{{cite MDOT map |date= 1941-12-01 |section= E12}}</ref><ref name=MSDH41-06RC>{{cite MDOT map |date= 1942-06-01 |section= E12}}</ref>
|tourist=[[File:Lake Huron Circle Tour.svg|20px|alt=|link=]] [[Lake Huron Circle Tour]]

}}
'''Business US Highway&nbsp;23''' ('''Bus. US&nbsp;23''') is a [[business loop]] that runs through downtown [[Rogers City, Michigan|Rogers City]] on two-lane streets. The highway starts at an intersection with [[U.S. Route 23 in Michigan|US&nbsp;23]] in [[Belknap Township, Michigan|Belknap Township]] south of downtown and passes the location of [[Michigan Limestone and Chemical Company]]'s quarry Petersville Road,<ref name=MDOT15RC>{{cite MDOT map |year= 2015 |section= E12}}</ref><ref name=googleRC>{{google maps |url= https://www.google.com/maps/dir/45.3799023,-83.8031379/45.4251302,-83.835024/@45.4026585,-83.8366189,7297m/data=!3m1!1e3!4m9!4m8!1m5!3m4!1m2!1d-83.8223624!2d45.4247045!3s0x4d34f55cae69ae6b:0xbbc6a3fc6629ac18!1m0!3e0 |title= Overview Map of Bus. US&nbsp;23 in Rogers City |access-date= January 6, 2016 |link= no}}</ref> the largest such quarry in the world.<ref>{{cite web |first= Randall |last= Schaetzl |date= n.d. |url= http://geo.msu.edu/extra/geogmich/calcite_quarry.html |title= Calcite Quarry |work= GEO 333: Geography of Michigan and the Great Lakes Region |location= East Lansing  |publisher= [[Michigan State University]] Department of Geography |access-date= January 6, 2015}}</ref> Farther north, the business loop passes the eastern end of the [[Rogers City Airport]] and turns northwesterly, parallel to, but inland from, the [[Lake Huron]] shoreline. Now following 3rd Street, Bus. US&nbsp;23 runs through a residential area on the southern side of town before entering downtown. At the intersection with Erie Street, Bus. US&nbsp;23 meets the eastern terminus of [[M-68 (Michigan highway)|M-68]]. Four blocks later, the business loop comes the closest to Lake Huron in another residential area before turning to the west. Anorther five blocks to the west of this curve, Bus. US&nbsp;23 terminates at an intersection with US&nbsp;23 on the western edge of the city.<ref name=MDOT15RC/><ref name=googleRC/> The business route carries the section of the [[Lake Huron Circle Tour]] (LHCT) through Rogers City.<ref name=MDOT15RC/>

In 1940, a new highway routing for US&nbsp;23 opened between Rogers City and [[Cheboygan, Michigan|Cheboygan]]. At the time, the former routing of US&nbsp;23 through downtown Rogers City was renumbered as a part of [[M-65 (Michigan highway)|M-65]], which was also extended northward along the segment of  [[M-91 (1927–1940 Michigan highway)|M-91]] that was not subsumed into the new US&nbsp;23 routing.<ref name=MSHD40-07>{{cite MDOT map |date=1940-07-15 |sections=F11–F12}}</ref><ref name=MSHD40-12>{{cite MDOT map |date=1940-12-01 |sections=F11–F12}}</ref> Just two years later, M-65 was pared back to end at US&nbsp;23 southeast of Rogers City, and the section of M-65 through downtown was renumbered as Bus. US&nbsp;23.<ref name=MSDH41-12RC/><ref name=MSDH41-06RC/>
{{Clear}}
'''Major intersections'''<br/>
{{MIinttop|county=Presque Isle|length_ref=<ref name=PRFA/>}}
{{MIint
|location=Belknap Township
|mile=0.000
|type=concur
|road={{jct|state=MI|US|23|Tour|LHCT|city1=Cheboygan|city2=Alpena}}
|notes=Southern end of LHCT concurrency}}
{{MIint
|location=Rogers City
|lspan=2
|mile=2.941
|road={{jct|state=MI|M|68|dir1=west|city1=Onaway}}
|notes=Eastern terminus of M-68}}
{{MIint
|mile=4.074
|type=concur
|road={{jct|state=MI|US|23|Tour|LHCT|city1=Cheboygan|city2=Alpena}}
|notes=Northern end of LHCT concurrency}}
{{jctbtm|lkeys=concur}}

==See also==
*{{portal-inline|Michigan Highways}}

==References==
{{reflist|30em}}

==External links==
*{{osmrelation|308345|Bus. US&nbsp;23 in Ann Arbor}}
*[http://www.michiganhighways.org/listings/MichHwysBus2-31.html#US-23BUS-AA Bus. US&nbsp;23 in Ann Arbor] at Michigan Highways
*[http://www.michiganhighways.org/listings/MichHwysBus2-31.html#US-23BUS-F Bus. US&nbsp;23 in Fenton] at Michigan Highways
*{{osmrelation|308344|Bus. US&nbsp;23 in Rogers City|link=no}}
*[http://www.michiganhighways.org/listings/MichHwysBus2-31.html#US-23BUS-RC Bus. US&nbsp;23 in Rogers City] at Michigan Highways

[[Category:U.S. Route 23]]
[[Category:U.S. Highways in Michigan|23]]