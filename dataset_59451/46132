{{Infobox officeholder
|image        =
|name         = Qadhi Saeed Almurooshid
|office7      = Director General - Dubai Health and Medical Services (DOHMS)
|term_start7  = March 2002
|term_end7    = June 2007
|office6      = Chairman of the board of directors - Unified Ambulance Centre
|term_start6  = 2006
|term_end6    = 2007
|office5      = Chairman of the board of directors - Centre of Ambulance Services
|term_start5  = 2007
|term_end5    = 2010
|office4      = Chairman of the board of directors - Dubai Corporation of Ambulance Services
|term_start4  = 2010
|term_end4    = 2012
|office3      = Member - The Executive Council of the Government of Dubai <!--1-->
|term_start3  = 2003
|term_end3    = 2012
|office2      = Director General - Dubai Health Authority (DHA)
|term_start2  = June 2007
|term_end2    = November 2012
|office       = Founding Chairman - Al Murooshid Investment LLC
|term_start   = 2009
|term_end     = Present
|birth_date   = {{birth date and age|1971|01|16|df=y}}
|birth_place  = [[Dubai]], [[Trucial States]]<br><small>(now [[United Arab Emirates]])</small>
}}

'''Qadhi Saeed Almurooshid''' (Arabic:قاضي سعيد المروشد), born 16 January 1971, is an Emirati businessman and a former member of the Government of [[Dubai]]. He currently serves as a member of several Dubai boards and as chairman of Al Murooshid Investment LLC .

== Early life and education ==

After completing his formative education in the [[United Arab Emirates]], Almurooshid continued his studies in the United States, where he graduated from [[Salem Teikyo University]], West Virginia in 1994 with a bachelor's in business administration and marketing.

== Department of Economic Development (1994–2001) ==

Following his graduation, Almurooshid returned to Dubai where he took his first position in the public sector in the [[Department of Economic Development (Dubai)|Department of Economic Development]]. Working within the compliance department he quickly rose to become the head of the newly established Commercial Protection Unit (CPU), and soon after, the head of the Corporate Service Centre. During his time with the CPU, Almurooshid worked closely with major corporations where he earned a reputation for his effective campaigning against issues such as piracy and counterfeit goods.

With a primary focus on supporting international companies and brands successfully establish themselves in the UAE; Almurooshid was part of the team that helped to cultivate a business friendly environment for corporations arriving in the region. Working not only among companies, but also sectors, Almurooshid was also key in establishing initiatives such as the [[Dubai Shopping Festival]] (DSF) in 1996 and Dubai Summer Surprises (DSS). In 1998 he was appointed as head of compliance, and became responsible for overseeing a team of seventy.

Almurooshid references his time with the economic department as an inspirational period of his development that helped to shape his on going career, and indeed, the department has been the foundry for many Emirati business figures and government officials.

== Executive Council of the Government of Dubai ==

Almurooshid was one of the first members of the Executive Council of Dubai, established under Law No.3 issued in 2003 by the royal decree of His Highness [[Sheikh Mohammed Bin Rashid Al Maktoum]], Prime Minister and Vice President of the United Arab Emirates and Ruler of Dubai, in order address the Emirate's security, public services and economic and social progress. Almurooshid served as a founding member and vice chairman of the Executive Council’s Social Committee, member of the Budgetary Committee and member of the Safety and Justice Committee from 2007 till 2010.

== Dubai Health and Medical Services (DOHMS) (2002–2007) ==

Almurooshid was officially appointed director general of the Dubai Health and Medical Services (DOHMS) on 9 March 2002. As director general, Almurooshid, focused his administration towards increasing the integration of technology into national medical services as well as ensuring sufficient medical care for all, in line with the vision of His Highness [[Sheikh Mohammed bin Rashid Al Maktoum]], and under the directive of His Highness [[Sheikh Hamdan bin Rashid Al Maktoum]], Deputy Ruler of Dubai and President of DOHMS.

=== Dubai Cord Blood & Research Centre (2006) ===

Established in 2006, Almurooshid’s team opened the Dubai Cord Blood & Research Centre on 6 June 2006 making it the first and only centre of its kind in the United Arab Emirates. As a licensed government entity, the centre was established to support the ongoing development of stem cell research as well as developing a more substantial registry of stem cells for the country.

=== Rashid Hospital Trauma Centre (2006) ===

Since taking the position as director general of DOHMS, Almurooshid led his team in the creation and development of [[Rashid Hospital]] Trauma Centre, the only fully integrated emergency department and regional facility dedicated to emergency and trauma care in the MENA region. With 130 beds, it operates 24 hours a day, 7 days a week and receives approximately 180,000 cases per annum, catering for a variety of acute emergencies such as medical, surgical, cardiac and neurology. The facility is also equipped with ten ambulance bays, two helipads, four state of the art resuscitation units, six operating theatres and the first STAT scanner in the region, and has an active emergency medicine residency training programme in support of the educational objectives of His Highness [[Sheikh Mohammed bin Rashid Al Maktoum]].

== Dubai Health Authority (DHA)(2007–2012) ==

In June 2007, the issue of Law No.13 by the royal decree of His Highness [[Sheikh Mohammed Bin Rashid Al Maktoum]] transformed the DOHMS into the [[Dubai Health Authority]] (DHA). A separate decree thenselected Almurooshid as the most suitable candidate to again lead Dubai’s health care programme under the direction of His Highness [[Sheikh Hamdan bin Mohammed Al Maktoum]], Crown Prince of Dubai and chairman of the Executive Council of the Government of Dubai, and was subsequently appointed as director general of the [[Dubai Health Authority]].

=== Primary Health Care Facilities ===

Almurooshid’s tenure as director general emphasised the realisation of the vision of His Highness [[Sheikh Mohammed bin Rashid Al Maktoum]] to provide primary health care for all citizens of the UAE. To this end, three primary healthcare centres, strategically placed to offer the maximum amount of coverage to their respective communities and costing approximately $19 million each were established in [[Nad Al Hammar]], [[Al Mizhar]] and [[Al Barsha]]. Today, the DHA has 14 primary healthcare centres, or one centre for every 30,000 individuals in the Emirate.<ref name=PHC>{{cite web|url=http://www.dubib.com/news/29326_dubai-diabetes-centre-winsbest-physical-environment-award-at-hospital-build-2012 |title=Dubai Diabetes Centre winsbest physical environment award at Hospital Build 2012 |publisher=Dubib.com |date= |accessdate=2014-05-10}}</ref>

=== Dubai Diabetic Centre (2012) ===

Diabetes is a widespread issue in the United Arab Emirates with an estimated 20% of the population suffering from the disease. In response to this, during his tenure as the director general of DOHMS, Almurooshid signed a memorandum of understanding with [[Joslin Diabetes Center]] and Harvard Medical Faculty Physicians,<ref>{{cite web|url=https://www.joslin.org/news/dept_of_health_and_medical_services_and_emaar_healthcare_group_join.html |title=Department of Health and Medical Services and Emaar Healthcare Group Join Hands with Joslin Diabetes Center and Harvard Medical Faculty Physicians &#124; Joslin Diabetes Center |publisher=Joslin.org |date=2007-06-10 |accessdate=2014-05-10}}</ref> to work together to set up an advanced diabetes centre in the [[Latifa Hospital]] in Dubai.

In June 2012, Almurooshid then announced the launch of a project for a new, state-of-the-art facility at Dubai Diabetic Centre,<ref>{{cite web|url=http://www.zawya.com/story/New_Dubai_Diabetes_Centre_to_provide_the_most_comprehensive_treatment_in_diabetes_care_and_management-ZAWYA20120612092349/ |title=New Dubai Diabetes Centre to provide the most comprehensive treatment in diabetes care and management |publisher=Zawya |date=2012-06-12 |accessdate=2014-05-10}}</ref> offering two and a half times the capacity of the existing centre to 20,000 square feet. Located on 2 December St, additional features included a paediatric endocrinology service, offering a more ‘child friendly’ experience to children under the age of 18 who suffer from the disease.

The centre also provided extensive podiatry, which is recognised as a highly important aspect of diabetes management. Almurooshid also introduced a new training programme for ten primary health care physicians from the DHA, bolstering the authority’s capacity for diabetes care and management.

=== Al Jalila Children’s Specialty Hospital (2008, estimated completion 2014) ===

Considered by Almurooshid as one of the most important projects overseen by his administration, the [[Al Jalila Children’s Specialty Hospital]]<ref name="PHC"/> was ordered by Sheikh Mohammed bin Rashid Al Maktoum as a gift to the children of the UAE to celebrate his daughter Al Jalila’s first birthday on Tuesday 2 December 2008. The Dubai hospital covers a built up area of 76,500 square metres and is equipped with 200 beds and facilities such as a paediatric trauma, heart and transplant centres, coupled with surgical departments, outpatient facilities, dialysis equipment and oncology care.<ref>{{cite web|url=http://www.dha.gov.ae/En/media/news/pages/dhacommencessecondphaseconstructionofaljalilachildren%E2%80%99sspecialityhospital.aspx |title=DHA commences second phase construction of Al Jalila Children’s Speciality Hospital |publisher=Dha.gov.ae |date= |accessdate=2014-05-10}}</ref>
Al Jalila Hospital has been recognised as the first facility of its kind in the region, designed on the basis of extensive research and visits to globally renowned paediatric facilities in order to ensure that visiting children will be made to feel more at ease and less intimidated by their surroundings. The hospital is already the recipient of two awards, namely the Future Projects Health Award, presented at the [[World Architecture Festival]] in Barcelona (2009) and for Best Sustainable Hospital Projects at the 2011 Hospital Build Awards.

=== Dubai Rehabilitation Centre ===

Offering multi-disciplinary modalities, Almurooshid’s team successfully delivered the country’s first fully functional rehabilitation centre in August 2012,<ref>{{cite web|url=http://gulfnews.com/news/gulf/uae/health/rehabilitation-centre-has-soft-opening-1.1067527 |title=Rehabilitation centre has soft opening |publisher=GulfNews.com |date=2012-08-29 |accessdate=2014-05-10}}</ref> in line with the vision of His Highness [[Sheikh Mohammed bin Rashid Al Maktoum]], Vice-President and Prime Minister of the UAE and Ruler of Dubai, to offer patients the option to fully recover post-surgery in country. The centre offers care to patients aged 14 years or above, and is also designed to cater for those with disabilities, sports injury and geriatric care. It was decided that the facility would be managed by the South Korea-based Bobath Memorial Hospital,<ref>{{cite web|url=http://www.zawya.com/story/Al_Murooshid_signs_a_contract_with_Bobath_Memorial_Hospital_South_Korea_for_the_technical_management_of_the_Dubai_Rehabilitation_Centre-ZAWYA20120829115258/ |title=Al Murooshid signs a contract with Bobath Memorial Hospital, South Korea for the technical management of the Dubai Rehabilitation Centre |publisher=Zawya |date=2012-08-29 |accessdate=2014-05-10}}</ref> a specialised medical centre with extensive experience in the field of rehabilitation. The facility has a capacity of 30 inpatients and between 80-120 outpatient cases per day.

=== Establishment and restructuring of Dubai's ambulatory services ===

Almurooshid led the joint committee between the Unified Ambulance Centre in Dubai that was created under Law No.13 of 2006<ref name=ambulance>{{cite web|url=http://gitex.dubai.ae/en/Lists/Partners%20List/PartnerDetailEng.aspx?ID=30 |title=Dubai eGovernment welcomes you to Gitex |publisher=Gitex.dubai.ae |date= |accessdate=2014-05-10}}</ref> between DOHMS and the [[Dubai Police]] General Headquarters.

By royal decree, the Centre of Ambulance Services (CAS) was restructured under Law No. 17 of 2007,<ref name="ambulance"/> issued by His Highness Sheikh Mohammed Bin Rashid Al Maktoum, Ruler of Dubai, replacing the Unified Ambulance Centre in Dubai. Almurooshid’s team were responsible for preparing, developing and applying the necessary policies that provided ambulance services not only in response to emergencies, but also providing patient mobility for the elderly, injured or disabled. The centre was also charged with operating a centralised control centre within the [[Emirate of Dubai]]. Today DCAS had a fleet of 177 ambulance vehicles, three mass casualty buses and 68 ground stations.

Dubai’s ambulatory services were restructured again by Almurooshid under the directive of Law No. 15 of 2010, issued by His Highness [[Sheikh Mohammed Bin Rashid Al Maktoum]], Ruler of Dubai, as Dubai Corporation for Ambulance Services (DCAS).

Appointed directly by His Highness Sheikh Mohammed, Almurooshid became the first chairman of DCAS when it opened in May 2012,<ref>{{cite web|url=http://www.emirates247.com/news/government/hamdan-opens-dubai-corp-for-ambulance-hq-2012-05-16-1.459094 |title=Hamdan opens Dubai Corp for Ambulance HQ |publisher=Emirates 24/7 |date=2012-05-16 |accessdate=2014-05-10}}</ref> and was further inaugurated in the presence of His Highness [[Sheikh Hamdan bin Rashid Al Maktoum]]. Worth $12.25 million, the facility covers 270,000 square feet and includes training facilities designed specifically for Ambulance Services.

=== Accreditation by the Joint Commission International (JCI) ===

In response to orders from His Highness [[Sheikh Mohammed bin Rashid Al Maktoum]], the [[Dubai Health Authority]], under the leadership of Almurooshid, had sought the accreditation from the [[Joint Commission International]] (JCI), and in March 2007 the Thalassaemia Centre (winner of the ''Outstanding Clinical Department in the Public Sector'' [[Sheikh Hamdan bin Rashid Al Maktoum Award for Medical Sciences]] in 2006), became the first public entity in the Middle East to receive such accreditation. The following year Rashid, Dubai and Al Wasl hospitals, were acknowledged with the same credentials.

One of the more remarkable acknowledgements from the JCI came in 2010 when the Airport Medical Centre, located at Terminal 3 of [[Dubai International Airport]] was given accreditation for ambulatory standards, making it the first airport clinic to receive such recognition. Almurooshid presented the award to His Highness [[Ahmed bin Saeed Al Maktoum]], president of [[Dubai Aviation City Corporation]] and chairman of [[Emirates Airline]].

Part of the Joint Commission’s criteria for must be to maintain its standards, something that was adhered to under Almurooshid’s administration. Today a total of 27 institutions from both the public and private sector are accredited.

== Additional DHA-tenure projects ==

=== Health Funding Department - Enaya ===

Launched on 1 July 2009, Enaya was launched under Almurooshid’s administration and was hailed as ‘by far one of the finest programmes developed in the [[Emirate of Dubai]]’.<ref>{{cite web|url=https://www.dha.gov.ae/En/pages/enaya.aspx |title=Enaya |publisher=Dha.gov.ae |date= |accessdate=2014-05-10}}</ref> Providing a broad range of insurance, Enaya is an initiative designed for all staff based in the 39 government-owned departments as well as their immediate family members, covering over 90,000 people.

Providing cover for both chronic and/or acute ailments, Enaya also covers preventative treatment, highlighting the Government’s commitment to providing health care for its people.

Almurooshid was also behind the launch of the Enaya ‘E-claims’,<ref>{{cite web|url=http://www.ifg.cc/index.php?option=com_content&task=view&id=40234&Itemid=99999999 |title=The Potsdam eGovernment Competence Center |publisher=IfG.CC |date=2012-07-30 |accessdate=2014-05-10}}</ref> an initiative that enables electronic claiming for insurance, in conjunction with the Dubai Employee Health Programme. Supporting the 100,000 public sector employees who use Enaya, ‘E-claims’ was designed to help both regulate health insurance as well as provide a transparent, user friendly experience for its users.

=== HIS (Health Information System) ===

As part of the government's on going initiatives to support technical innovation and integration, Almurooshid introduced H.I.S., (Health Information System<ref name=HISPACS>{{cite web|url=https://www.dha.gov.ae/EN/Facilities/Hospitals/RashidHospital/MedicalSupportService/HealthInformationSystem/Pages/default.aspx |title=H.I.S. (Health Information System) |publisher=Dha.gov.ae |date=2014-01-20 |accessdate=2014-05-10}}</ref>), to [[Rashid Hospital]] in July 2010. Tracking both procedures, as well as the general requirements of the patients, H.I.S provides a totally electronic system for organising everything from physician order entry (including booking Radiological, laboratory, and physiotherapy requests), through to ordering prescriptions. The system was also successfully used to track patients through, as well as posting lab results and clinical information for critically ill patients.

As part of [[Rashid Hospital]]’s investment in Health Information System (H.I.S), Almurooshid was oversaw the introduction of the Picture Archiving Communication System (PACS),<ref name="HISPACS"/> allowing for total digital convergence of all x-ray images allowing doctors unprecedented accessibility to patient information. Special work stations with high resolution monitors were also installed as part of the initiative offering detailed images of the operating rooms for the neurosurgical, orthopaedic-trauma and breast surgery teams, intensive care units and emergency department.

Under Almurooshid’s leadership, the DHA went under a massive overhaul with ‘e-health’ and ‘e-government’. In recognition for the department’s contribution towards improving healthcare in Dubai, the [[Dubai Health Authority]] was awarded Best Technology Project for its PACS at the Dubai Government Excellence Program, awarded in person by His Highness [[Sheikh Hamdan bin Rashid Al Maktoum]].

=== EMR (Electronic Medical Record Project) ===

Launched in the beginning of 2010, the DHA’s Electronic Medical Record Project (EMR), was launched allowing patients throughout the [[Emirate of Dubai]] to view a variety of up to date information about their health ranging from lab reports through to patient history.<ref>{{cite web|url=http://www.thenational.ae/news/uae-news/health/dubai-medical-records-move-online |title=Dubai medical records move online &#124; The National |publisher=Thenational.ae |date=2010-01-26 |accessdate=2014-05-10}}</ref>

Almurooshid presented the EMR to His Highness [[Sheikh Mohammed bin Rashid Al Maktoum]], Prime Minister and Vice President of the United Arab Emirates and Ruler of Dubai at the Arab Health Congress 2010.

=== Health Regulation Department & Sheryan ===

Officially announced in August 2009, Almurooshid oversaw the establishment of the Health Regulation Department. Charged with multiple responsibilities, the core the HRD was to simplify the regulatory sector by managing and coordinating all resources within Dubai’s health sector. From monitoring healthcare facilities, to assessing professional performance the HRD created greater synergy for Dubai’s medical community. One of the most important initiatives introduced by Almurooshid’s administration was Sheryan;<ref>{{cite web|url=http://www.docstoc.com/docs/130425145/DHA-utilizes-Sheryan-services-for-licensing-of-medical-professionals-in |title=DHA utilizes Sheryan services for licensing of medical professionals in |publisher=Docstoc.com |date=2012-09-19 |accessdate=2014-05-10}}</ref> a centralised online licensing system designed for all healthcare professionals and facilities where all licensing and payments can be processed.

=== Guinness World Record: ‘Largest mobile hospital’ ===

As part of His Highness’ vision for improved mobile medical facilities, Almurooshid’s team introduced three mass casualty ambulances into service, including a vehicle recognised by the [[Guinness World Records]] as the ‘Largest ambulance in the world’.<ref>{{cite web|url=http://www.guinnessworldrecords.com/records-1/largest-ambulance/ |title=Largest ambulance |publisher=Guinnessworldrecords.com |date=2009-09-25 |accessdate=2014-05-10}}</ref> Added to the DCAS fleet on 25 September 2009, and measuring 20.03 metres in length, it comes equipped woth an operating theatre, three intensive care units and eight immediate care units. Designed by Dr Martin von Bergh of Global Medical Consulting, it was built by Gebr. Heymann GmBH, outfitted by Cytomed Middle East, and has a total treatment and transport capacity for 123 patients and staff.

=== ‘Baby Friendly Hospital’ ===

In February 2011 in the presence of Her Highness Princess Haya Bint Al Hussein, Almurooshid oversaw one of the DHA’s hospitals (Al Wasl) become recognised by UNICEF as a ‘Baby friendly hospital’.

=== Medical Tourism ===

In response to instructions issued by His Highness [[Sheikh Mohammed bin Rashid Al Maktoum]] and His Highness [[Sheikh Hamdan bin Mohammed Al Maktoum]], Almurooshid initiated dialogue between both the public and privates sectors to coordinate a unified medical tourism initiative in 2012.<ref>{{cite web|url=http://update.dubaitourism.ae/2012/04/08/hh-sheikh-hamdan-meets-officials/ |title=HH Sheikh Hamdan meets officials » Dubai Update |publisher=Update.dubaitourism.ae |date= |accessdate=2014-05-10}}</ref>  Illustrating Dubai’s highly skilled professional medical personnel, including 4,750 doctors and physicians speaking over 40 languages, Almurooshid’s administration had significantly developed Dubai’s medical facilities over the period of a decade making it the ideal location, not just geographically, but also professionally.

As part of the DHA’s commitment to highlighting Dubai’s position as a destination for medical tourism, Almurooshid travelled to countries such as Turkey,<ref>{{cite web|url=https://www.dha.gov.ae/En/media/news/pages/dhareviewspotentialfordubai'smedicaltourisminturkey.aspx |title=DHA reviews potential for Dubai's Medical Tourism in Turkey |publisher=Dha.gov.ae |date= |accessdate=2014-05-10}}</ref> Jordan and the United States, as well as hosting delegations from countries such as Brazil in order promote and spread awareness about the Emirate’s available facilities. Today, it is estimated that Dubai’s medical tourism market is worth over $1.2 billion per annum.

== International Conferences ==

=== FDI Dental Congress 2007 ===

After three years of campaigning Almurooshid successfully brought the FDI Dental Congress to Dubai in 2007.<ref>{{cite web|url=http://www.ameinfo.com/blog/agriculture-&-horticulture/dohms/qadi-saeed-al-murooshid-announces-organizing-committee-for-fdi-dubai/ |title=Qadi Saeed Al Murooshid announces organizing committee for FDI Dubai 2007 &#124; |publisher=Ameinfo.com |date= |accessdate=2014-05-10}}</ref> Acknowledged as the single largest gathering of dental professionals in the world, the event attracted 13,593 attendees and 344 exhibiting companies.

=== World Diabetes Congress Exhibition 2011 ===

Recognised as the largest World Diabetes Congress in IDF history, Almurooshid was chairman the Dubai’s successful bid which subsequently attracted 15,100 participants, a 20% increase on the previous IDF conference held in 2009 in Montreal. Inaugurated by His Highness [[Sheikh Hamdan bin Rashid Al Maktoum]], Deputy Ruler of Dubai, Minister of Finance and president of [[Dubai Health Authority]],<ref>{{cite web|url=http://www.dha.gov.ae/En/media/news/pages/sheikhhamdaninauguratesworlddiabetescongressexhibition.aspx |title=Sheikh Hamdan inaugurates World Diabetes Congress Exhibition |publisher=Dha.gov.ae |date= |accessdate=2014-05-10}}</ref> the exhibition attracted organisations from over 160 countries and showcased a wide range of technologies related to the diagnosis and treatment of diabetes. During the congress Almurooshid also announced the expansion of the Dubai Diabetes Centre to include a paediatric endocrinologist that would specialise in providing care for children under the age of 18 with diabetic care and management, and increase in-patient capacity by 50%. The 2011 congress was also the first time an event of this kind was hosted in the Middle East.

=== World Congress of Cardiology 2012 ===

In 2012 Almurooshid successfully secured the World Congress of Cardiology to be hosted in Dubai,<ref>{{cite web|url=http://www.dha.gov.ae/EN/Media/News/Pages/DubaiGearsuptohosttheprestigiousWorldCongressofCardiologyinApril2012.aspx |title=Dubai Gears up to host the prestigious World Congress of Cardiology in April 2012 |publisher=Dha.gov.ae |date= |accessdate=2014-05-10}}</ref> beating competition from Singapore, Cape Town and Kyoto. The event attracted thousands of cardiologists and other healthcare professionals from over 100 countries who discussed a variety of issues in particular cardiovascular disease.

=== Hospital Build & Infrastructure Middle East ===

In 2012 Almurooshid officially opened the Hospital Build & Infrastructure Middle East conference, during which the UAE won five of the eight available awards including Best Hospital Design (Built), Best Sustainable Hospital Project and Best Physical Environment.<ref>{{cite web|url=http://www.hospitalbuild-me.com/MediaZone/News-List/UAE-triumphs-at-2012-Hospital-Build-Awards-in-Dubai/ |title=Hospital Build and Infrastructure Middle East Trade Show - UAE triumphs at 2012 Hospital Build & Infrastructure Awards in Dubai |publisher=Hospitalbuild-me.com |date= |accessdate=2014-05-10}}</ref>

=== DUPHAT (Dubai International Pharmaceuticals and Technologies Conference and Exhibition) ===

Launched in 1995, DUPHAT is recognised as the largest pharmaceutical and technology event in the Middle East, Almurooshid continuously sought to promote and engage the international community at this annual event hosted in Dubai. Providing a convergence point for the global industry, DUPHAT is held in Dubai annually under the patronage of His Highness [[Sheikh Hamdan bin Rashid Al Maktoum]].

=== UAE International Dental Conference & Arab Dental Exhibition (AEEDC Dubai) ===

Annually attracting representatives from over 120 countries, the AEEDC Dubai has shown strong levels of growth in the past decade, attracting over 20,000 dentists every year. In Almurooshid’s own words, he described the 2010 event as ''a great opportunity for all dentists and decision makers in the private and public sectors, as it catered to the needs of all medical equipment and services displayed by major international companies.''<ref>{{cite web|url=http://www.dha.gov.ae/En/media/news/pages/aeedc2010.aspx |title=Hamdan Bin Rashid inaugurates AEEDC 2010 |publisher=Dha.gov.ae |date= |accessdate=2014-05-10}}</ref>

=== Arab Health ===

Recognised as the largest healthcare exhibition in the Middle East and Asia, [[Arab Health]] annually attracts the key decision makers and leaders from around the world over a four-day event hosted under at the [[Dubai International Convention Centre]]. During Almurooshid’s administration starting at DOHMS through to the end of his administration with the DHA, participation for this event increased from 13,000 attendees and 500 companies to 65,000 attendees and over 3000 exhibitors from over 60 countries.

== Noor Dubai ==

Almurooshid served as the chairman of the board of trustees for the Noor Dubai Foundation from the day of its decree till November 2012. Founded in September 2008 by His Highness Sheikh Mohammed bin Rashid Al Maktoum, Prime Minister and Vice President of the United Arab Emirates and Ruler of Dubai, the Noor Dubai Foundation was established with the aim of delivering preventive eye care services to one million people suffering from either blindness or impairment in the world.<ref>http://www.noordubai.ae/en/NewsCenter/PressReleases/PressReleaseDetail.aspx?News=yemenis.benefit.noordubai</ref>
During Almurooshid’s tenure as chairman, for which he was directly appointed by His Highness Sheikh Mohammed bin Rashid Al Maktoum, the foundation signed a memorandum of understanding whereby the [[Dubai Police]] provided financial and logistic support to prevention of blindness programs in the UAE and abroad. He was also intrinsic in involving further support from other corporations, including Dubai Islamic Bank, through its foundation arm, Al Islami.

Other initiatives instigated by Almurooshid include working with organisations such as [[Lions Clubs International]], the world’s largest volunteer organisation with a global network of 1.3 million members in 202 countries, who within a period of six months working together managed to protect the sight of half a million people.<ref>http://www.lionsclubs.org/FR/news-and-events/newsroom/lions-lcif-sight-text.php</ref>
Almurooshid was also behind the collaboration with Orbis International, a nonprofit organization that works to prevent and treat blindness in partnerships with local health care organisations.
Noor Dubai honoured the Lions Clubs and Orbis International in a ceremony attended by Almurooshid and Sheikh Majid Bin Mohammad Bin Rashid Al Maktoum, Chairman of Dubai Culture and Arts Authority and Noor Dubai.<ref>http://www.ameinfo.com/blog/linguistics/noor-dubai/noor-dubai-honours-orbis-international-and-lions-club-international/{{dead link|date=July 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>
By the end of 2012, the programme provided over six million individuals with services that either prevent blindness or enable healthy vision.

== Board Membership ==

=== Dubai 2020 ===

Almurooshid was a founding committee member of Dubai 2020, an initiative to explore Dubai’s potential to host the World Expo, Olympics and Paralympics. In late November 2013, Dubai was confirmed as the host for the World Expo in 2020, beating off competition from São Paulo, Ekaterinburg and Izmir. As well as being the first World Expo to be hosted in the Middle East, it is estimated that the victory will create approximately 277,000 job opportunities between 2013 and 2020.

=== Sheikh Hamdan Bin Rashid Al Maktoum Medical Award for Medical Sciences ===

Almurooshid served as the vice-chairman of the board of trustees, [[Sheikh Hamdan bin Rashid Al Maktoum]] Medical Award for Medical Sciences from 2007 to 2012.

=== Dubai School of Government ===

Almurooshid served as a member of the board of trustee for the Dubai School of Government in 2012.

=== Dubai Healthcare City Authority ===

Almurooshid served as a member of the board of Dubai Healthcare City Authority, under the chair of Her Royal Highness [[Princess Haya bint Al Hussein]] between January 2011 and November 2012. Noteworthy accomplishments include establishing a mutual recognition of medical licences between DHCA and the [[Dubai Health Authority]], offering greater convenience for the estimated 25,000 healthcare professionals working in the Emirate.

== Sports Clubs ==

=== Dubai Cultural Sport Football Club ===

Established in 1996, Almurooshid is a founding member of the [[Dubai Cultural Sports Club]] football club and was appointed general secretary assistant upon its establishment.

=== Al Nasr Sports Club ===

Recognised as the oldest club in the UAE (est. 1945), Almurooshid served as chairman of the board of Al Nasr SC for the UAE Pro-league team between 2006 and 2009. In addition to the football side, Al Nasr trains athletes in thirteen different sporting disciplines. Almurooshid still retains his position as a member of the board.

== Founding Investor ==

Almurooshid has been a keen investor in a wide range of enterprises, including being a founding investor for some of the UAE’s more notable companies including [[Mawarid Finance]], Emaar Industries & Investments and Al Salam Bank Bahrain.

== Current Local Partnerships ==

Since retiring from his government positions, Almurooshid continues to support the UAE’s national vision through the creation of local partnerships with companies that can offer significant value to the country’s on going development. His notable current partnerships include those with Kele Construction, Bilfinger Sielv, Fairmount Holidays & Travel, Inara Tech and [[RCS Sport]] - Media & Events JLT.

== Union Coop ==

Almurooshid continues to serve as a board member with Union Coop, a society established by Ministerial Resolution in 1982 to establish and manage hypermarkets and shopping malls in Dubai. Assisting to support and improve the social and economic conditions of its members, Union Coop has ten branches spread across the Emirate of Dubai. In February 2014, the society was honoured by [[Dubai Municipality]] for its implementation of e-service for importing and exporting foodstuff.

== Almurooshid Investments LLC ==

Almurooshid is chairman of Al Murooshid Investment LLC, a company that specialises in forging partnerships with international businesses from different sectors who are looking to enter and operate in the GCC.

== References ==

<!--- See [[Wikipedia:Footnotes]] on how to create references using<ref></ref> tags which will then appear here automatically -->
{{Reflist}}

{{DEFAULTSORT:Almurooshid, Qadhi Saeed}}
[[Category:1971 births]]
[[Category:Living people]]
[[Category:Emirati people]]
[[Category:Healthcare in the United Arab Emirates]]