{{Multiple issues|{{more footnotes|date=September 2016}}{{cleanup|date=September 2016|reason=The lead doesn't define why individual is notable. BLP articles need sufficient sources to prove notability.}}{{lead too short|date=September 2016}}}}


'''Magne Jørgensen''' (born 10 October 1964) is a Norwegian [[scientist]] and [[software engineer]] in the field of [[Computational science|scientific computing]]. Jørgensen is Chief Research Scientist at [[Simula Research Laboratory]] and is involved in the Research Group for Programming and Software Engineering as [[Professor]] at the Department for Informatics at the [[University of Oslo]].

== Career ==
Jørgensen received his [[Master of Science]] in [[Economy]] and [[Computer science|Computer Science]] from the [[Karlsruhe Institute of Technology|University of Karlsruhe]], Germany, in 1988. In 1994 he received his [[Doctor of Philosophy|Ph.D.]] degree in [[Software engineering|Software Engineering]] from the University of Oslo, Norway, based on a thesis on “Empirical studies of software maintenance”.<ref>{{Cite book|title=Empirical studies of software maintenance.|last=Jørgensen|first=Magne|publisher=University of Oslo, Department of Informatics|year=1994|isbn=|location=|pages=}}</ref> Jørgensen worked as a senior scientist at [[Telenor]] Research and Development from 1989 until 1998, when he moved shortly to [[Storebrand]]. In 1999 he became [[Associate professor|Associate Professor]] in Software Engineering at the [http://www.mn.uio.no/ifi/english/ Department for Informatics] at the University of Oslo, and in 2002 he was appointed full professorship.<ref>{{Cite web|url=http://www.mn.uio.no/ifi/english/people/aca/magnej/|title=Magne Jørgensen - Department of Informatics|last=|first=|date=|website=www.mn.uio.no|publisher=|access-date=2016-06-08}}</ref> He has been at Simula Research Laboratory as a Chief Research Scientist and member of the software engineering research group since 2001.<ref>{{Cite web|url=https://www.simula.no/people/magnej|title=Magne Jørgensen {{!}} Simula|website=www.simula.no|access-date=2016-06-08}}</ref>

== Research ==
Jørgensen’s fields of research include management of software projects, software development methods, judgment and decision-making in software development, and empirical methods for software engineering. The primary focus of Jørgensen’s research is estimation of ICT projects.

Together with [[Tore Dybå]] and [[Barbara Kitchenham]], he has launched a method for systematic review of empiric experiences in the development of ICT projects, called Evidence-based software engineering.<ref>{{Cite journal|last=Kitchenham|first=Barbara A.|last2=Dyba|first2=Tore|last3=Jorgensen|first3=Magne|date=2004-01-01|title=Evidence-Based Software Engineering|url=http://dl.acm.org/citation.cfm?id=998675.999432|journal=Proceedings of the 26th International Conference on Software Engineering|series=ICSE '04|location=Washington, DC, USA|publisher=IEEE Computer Society|pages=273–281|isbn=0769521630}}</ref> In 2014 Jørgensen, together with Dybå and Kitchenham, received the [[SIGSOFT|ACM Sigsoft]] award for the most influential paper in the last ten years for the initial paper on evidence-based software engineering.<ref>{{Cite web|url=http://www.sigsoft.org/awards/impactPaperAward.html|title=ACM SIGSOFT - Impact Paper Award|website=www.sigsoft.org|access-date=2016-06-08}}</ref>

== Research impact and authorship ==
Jørgensen was ranked as the “top scholar”, i.e. the most productive researcher, in system and software engineering for the periods 2001-2005,<ref>{{Cite web|url=http://hub.hku.hk/bitstream/10722/89099/1/Content.pdf?accept=1|title=An assessment of systems and software engineering scholars and institutions (2001-2005)|last=|first=|date=2008|website=|publisher=The HKU Scholars Hub|access-date=2016-06-08}}</ref> 2002-2006,<ref>{{Cite web|url=http://www-lb.cs.umd.edu/~basili/publications/journals/J115.pdf|title=An assessment of systems and software engineering scholars and institutions (2002–2006)|last=|first=|date=2009-06-17|website=|publisher=UMD University of Maryland|access-date=2016-06-08}}</ref> 2003-2007 and 2004-2008.<ref>{{Cite web|url=http://wwwold.cs.umd.edu/~basili/publications/journals/J117.pdf|title=An assessment of systems and software engineering scholars and institutions (2003–2007 and 2004–2008)|last=|first=|date=2010-10-08|website=|publisher=UMD University of Maryland (Old site)|access-date=2016-06-08}}</ref> The rankings, published in [[Journal of Systems and Software]], is based on number of publications published in the top system and software engineering journal and includes about 4000 researchers.

Since 2004 Jørgensen has been writing a monthly column in the Norwegian magazine Computerworld, transferring research results to software professionals.
Jørgensen is a member of the editorial board of Journal of Systems and Software<ref>{{Cite web|url=http://www.journals.elsevier.com/journal-of-systems-and-software/editorial-board/|title=Journal of Systems and Software Editorial Board|website=www.journals.elsevier.com|access-date=2016-06-09}}</ref> and [[Evidence-based Information Systems]].<ref>{{Cite web|url=http://www.ebisjournal.co.uk/about/editorialTeam|title=Editorial Team|website=www.ebisjournal.co.uk|access-date=2016-06-09}}</ref> Previously he was on the editorial board of [[Software Quality Journal]].

Jørgensen was assessed by Computerworld Norway to be one of the fifty most influential professionals within ICT in Norway in 2012, 2013 and 2014.<ref>{{Cite web|url=http://www.cw.no/artikkel/offentlig-sektor/her-it-norges-100-mektigste|title=Her er It-Norges 100 mektigste {{!}} Computerworld|last=Redaksjonen|website=Computerworld|access-date=2016-06-09}}</ref><ref>{{Cite web|url=http://www.cw.no/artikkel/offentlig-sektor/it-norges-mektigste-2013|title=It-Norges mektigste i 2013 {{!}} Computerworld|last=Redaksjonen|website=Computerworld|access-date=2016-06-09}}</ref><ref>{{Cite web|url=http://www.cw.no/artikkel/offentlig-sektor/it-makt-norge-2014-topp-100|title=It-makt i Norge 2014: Topp 100 {{!}} Computerworld|last=Redaksjonen|website=Computerworld|access-date=2016-06-09}}</ref>

Together with [http://www.scienta.no Scienta], Jørgensen compiled a report on successes and failures in public ICT projects.<ref>{{Cite web|url=https://www.regjeringen.no/no/dokumenter/suksess-og-fiasko-i-offentlige-ikt--prosjekter/id2422617/|title=Suksess og fiasko i offentlige IKT- prosjekter|last=Kommunal- og moderniseringsdepartementet|first=|date=2015-06-16|website=Regjeringen.no|language=no|access-date=2016-06-09}}</ref> Since 2016 Jørgensen has been Member of the Norwegian Digitization Council.<ref>{{Cite web|url=https://www.regjeringen.no/no/dep/kmd/org/styrer-rad-og-utvalg/digitaliseringsradet/id2472347/|title=Digitaliseringsrådet|last=Kommunal- og moderniseringsdepartementet|first=|date=2016-01-28|website=Regjeringen.no|language=no|access-date=2016-06-09}}</ref>

==Selected works==
*Kitchenham, Barbara A., Tore Dybå, and Magne Jørgensen. "Evidence-based software engineering." Proceedings of the 26th international conference on software engineering. IEEE Computer Society, 2004.
*Dybå, Tore, Barbara A. Kitchenham, and Magne Jørgensen. "Evidence-based software engineering for practitioners." Software, IEEE 22.1 (2005): 58-65.
*Jørgensen, Magne, and Martin Shepperd. "A systematic review of software development cost estimation studies." Software Engineering, IEEE Transactions on 33.1 (2007): 33-53.
*Moløkken, Kjetil, and Magne Jørgensen. "A review of software surveys on software effort estimation." Empirical Software Engineering, 2003. ISESE 2003. Proceedings. 2003 International Symposium on. IEEE, 2003.

== References ==
{{Reflist}}

== External links ==
* [https://www.cristin.no/as/WebObjects/cristin.woa/wa/fres?sort=ar&la=en&action=sok&pnr=23476 Cristin]
* [https://www.simula.no Simula Research Laboratory] 
{{authority control}}

{{DEFAULTSORT:Jorgensen, Magne}}
[[Category:Norwegian computer scientists]]
[[Category:University of Oslo alumni]]
[[Category:University of Oslo faculty]]
[[Category:1964 births]]
[[Category:Living people]]
[[Category:Articles created via the Article Wizard]]