{{Infobox company
| name = The Descartes Systems Group Inc.
| logo = Descartes_logo_288x36.png
| logo_size = 
| logo_alt = Descartes logo
| logo_caption = 
| logo_padding = 
| image = 
| image_size = 
| image_alt = 
| image_caption = 
| trading_name = 
| native_name = 
| native_name_lang = <!-- Use ISO 639-1 code, e.g. "fr" for French. For multiple names in different languages, use {{Lang|[code]|[name]}}. -->
| romanized_name = 
| former_name = 
| type = [[Public company|Public]]
| traded_as = {{Unbulleted list|{{TSX|DSG}}|{{NASDAQ|DSGX}}}}
| ISIN = 
| industry = {{Unbulleted list|[[Computer software]]|[[Cloud computing]]|[[Supply chain management]]}}
| genre = 
| fate = 
| predecessor = <!-- or: | predecessors = -->
| successor = <!-- or: | successors = -->
| founded = {{start date and age|1981|05|22}}
| founder = <!-- or: | founders = -->
| defunct = <!-- {{End date|YYYY|MM|DD}} -->
| hq_location = [[Waterloo, Ontario]], Canada
| hq_location_city = 
| hq_location_country = 
| num_locations = 
| num_locations_year = <!-- Year of num_locations data (if known) -->
| area_served = Worldwide
| key_people = {{Unbulleted list|Edward J. Ryan, CEO|J. Scott Pagan, President and COO|Allan Brett, CFO}}
| products = [[Software-as-a-service]] applications for [[logistics]] and supply chain management
| brands = 
| production = 
| production_year = <!-- Year of production data (if known) -->
| services = Descartes Global Logistics Network
| revenue = {{profit}} [[United States dollar|US$]] $203.8 million ([[Fiscal year|FY]] 2017)<ref>{{Cite web|url=https://www.descartes.com/news-events/general-news/descartes-reports-fiscal-2017-fourth-quarter-and-annual-financial-results|title=Descartes Reports Fiscal 2017 Fourth Quarter and Annual Financial Results|last=|first=|date=March 8, 2017|website=Descartes|publisher=|access-date=March 9, 2017}}</ref>
| revenue_year = <!-- Year of revenue data (if known) -->
| operating_income = 
| income_year = <!-- Year of operating_income data (if known) -->
| net_income = <!-- or: | profit = -->
| net_income_year = <!-- or: | profit_year = --><!-- Year of net_income/profit data (if known) -->
| aum = <!-- Only for financial-service companies -->
| assets = 
| assets_year = <!-- Year of assets data (if known) -->
| equity = 
| equity_year = <!-- Year of equity data (if known) -->
| owner = <!-- or: | owners = -->
| members = 
| members_year = <!-- Year of members data (if known) -->
| num_employees = 1000+
| num_employees_year = <!-- Year of num_employees data (if known) -->
| parent = 
| divisions = 
| subsid = 
| module = <!-- Used to embed other templates -->
| ratio = <!-- Basel III ratio, for BANKS ONLY -->
| rating = <!-- credit rating, for BANKS ONLY -->
| website = {{URL|www.descartes.com}}
| footnotes = 
| intl = yes
}}
'''The Descartes Systems Group Inc.''' (commonly referred to as '''Descartes''') is a Canadian [[Multinational corporation|multinational]] [[technology company]] specializing in [[logistics]] software, [[supply chain management software]], and [[Cloud computing|cloud]]-based services for logistics businesses.

Descartes is perhaps best known for its abrupt and unexpected turnaround in the mid-2000s after coming close to bankruptcy in the wake of the dot-com bubble collapse. It is also known as one of the earliest logistics technology companies to adopt an on-demand [[business model]] and sell its [[software as a service]] (SaaS) via the Internet. The company operates the Global Logistics Network, an extensive electronic messaging system used by [[freight companies]], manufacturers, [[Distribution (business)|distributors]], retailers, customs brokers, government agencies, and other interested parties to exchange logistics and [[customs]] information.

Headquartered in [[Waterloo, Ontario|Waterloo]], [[Ontario]], [[Canada]], Descartes is a [[publicly traded company]] with shares listed on the [[Nasdaq Stock Market|NASDAQ Stock Market]] (NASDAQ: DSGX) and [[Toronto Stock Exchange]] (TSX: DSG). It has offices in the Americas, Europe, the Middle East, Africa, and the Asia Pacific region.

== History ==
Descartes was founded in 1981.<ref>{{Cite web|url=http://markets.businessinsider.com/stock/descartes_systems_group/company-profile|title=Descartes Systems Group Inc. - Profile|last=|first=|date=|website=Business Insider|archive-url=|archive-date=|dead-url=|access-date=March 9, 2017}}</ref>

In 1998, the company made an [[initial public offering]] on the Toronto Stock Exchange,<ref name=":0">{{Cite news|url=http://www.theglobeandmail.com/report-on-business/descartes-executives-to-depart/article20449197/|title=Descartes executives to depart|last=|first=|date=April 22, 2009|work=The Globe and Mail|access-date=July 29, 2016|via=}}</ref> where its common shares trade under the [[stock symbol]] DSG.<ref>{{Cite web|url=http://web.tmxmoney.com/company.php?qm_symbol=DSG|title=Descartes Systems Group Inc. (The)|last=|first=|date=|website=TMX Money|publisher=|access-date=August 21, 2016}}</ref> Descartes was first listed on the NASDAQ Stock Market in 1999,<ref name=":0" /> with common shares trading under the symbol DSGX.<ref>{{Cite web|url=http://www.nasdaq.com/symbol/dsgx|title=Descartes Systems Group Inc. (The) Common Stock Quote & Summary Data|last=|first=|date=|website=Nasdaq|publisher=|access-date=August 21, 2016}}</ref> Descartes’ share price peaked during the [[dot-com bubble]] and then fell precipitously in the subsequent crash.<ref name=":0" /><ref name=":1">{{Cite news|url=http://www.theglobeandmail.com/report-on-business/ceos-bold-prescription-for-profit-turnaround/article4284715/|title=CEO's bold prescription for profit turnaround|last=Pitts|first=Gordon|date=July 19, 2009|work=The Globe and Mail|access-date=July 26, 2016|via=}}</ref>

In 2001, Descartes switched its business model from selling full-featured [[enterprise software]] licenses to providing on-demand software on a subscription basis, becoming one of the first SaaS providers in the logistics sector.<ref name=":2" /><ref name=":8">{{Cite web|url=http://www.joc.com/economy-watch/descartes-expansion-strategy_20070902.html|title=Descartes’ expansion strategy|last=Field|first=Alan|date=September 2, 2007|website=The Journal of Commerce|publisher=|access-date=July 28, 2016}}</ref><ref>{{Cite web|url=http://www.joc.com/still-standing_20030406.html|title=Still standing|last=Saccomano|first=Ann|date=April 6, 2003|website=The Journal of Commerce|publisher=|access-date=October 20, 2016}}</ref> After years of losses, Descartes came close to bankruptcy in 2004,<ref name=":1" /><ref name=":2">{{Cite web|url=http://www.canadiansailings.ca/?p=988|title=Descartes Systems Group: Logistics software and services provider helps customers deliver|last=Field|first=Alan|date=March 29, 2010|website=Canadian Sailings|publisher=Great White Publications|access-date=July 25, 2016}}</ref> prompting it to aggressively restructure.<ref>{{Cite web|url=http://www.joc.com/economy-watch/descartes-system-crash_20040523.html|title=Descartes' System Crash|last=|first=|date=May 23, 2004|website=Traffic World/The Journal of Commerce|publisher=|access-date=July 28, 2016}}</ref><ref>{{Cite news|url=http://www.theglobeandmail.com/technology/descartes-profit-exceeds-expectations/article1127774/|title=Descartes profit exceeds expectations|last=|first=|date=December 2, 2005|work=The Globe and Mail|access-date=July 29, 2016|via=}}</ref> The company cut 35% of its workforce<ref>{{Cite news|url=http://www.theglobeandmail.com/technology/descartes-improves-but-year-end-results-weak/article20420041/|title=Descartes improves, but year-end results weak|last=Paddon|first=David|date=March 4, 2005|work=The Globe and Mail|access-date=July 29, 2016|via=}}</ref> and initiated a sweeping transformation of its [[corporate culture]].<ref name=":1" /><ref name=":6">{{Cite news|url=http://business.financialpost.com/corporate-culture-awards/descartes-attributes-success-to-humble-servant-culture|title=Descartes attributes success to ‘humble servant culture’|last=|first=|date=February 4, 2013|work=Financial Post|access-date=July 25, 2016|via=}}</ref><ref name=":7">{{Cite web|url=http://www.cantechletter.com/2013/11/art-mesher-one-canadas-dynamic-ceos-leaves-descartes1127/|title=Art Mesher, one of Canada’s most dynamic CEOs, leaves Descartes|last=Waddell|first=Nick|date=November 27, 2013|website=Cantech Letter|publisher=Cantech Communications|access-date=July 26, 2016}}</ref>

The company returned to profitability in 2005,<ref>{{Cite news|url=http://www.theglobeandmail.com/report-on-business/descartes-posts-record-profit-after-major-restructuring/article18227832/|title=Descartes posts record profit after major restructuring|last=|first=|date=May 26, 2005|work=The Globe and Mail|access-date=July 29, 2016|via=}}</ref> pulling off what one analyst later described as “one of the most dramatic turnarounds in Canadian corporate history.”<ref name=":7" /> Descartes was recognized for this achievement at the 2006 International Business Awards gala, held in New York City.<ref name=":9" />

In December, 2013, Descartes was added to the [[S&P/TSX Composite Index]], an index of the stock (equity) prices of the largest companies on the Toronto Stock Exchange.<ref>{{Cite news|url=http://www.therecord.com/news-story/4273819-descartes-to-join-s-p-tsx-composite-index/|title=Descartes to join S&P/TSX composite index|last=|first=|date=December 16, 2013|work=Waterloo Region Record|access-date=July 27, 2016|via=}}</ref>

By January, 2015, Descartes had posted 41 straight profitable quarters<ref>{{Cite news|url=http://www.therecord.com/news-story/5461025-descartes-posts-41st-consecutive-profitable-quarter/|title=Descartes posts 41st consecutive profitable quarter|last=|first=|date=March 5, 2015|work=Waterloo Region Record|access-date=July 27, 2016|via=}}</ref> and was supplying logistics software and services to more than 10,000 logistics-centric businesses,<ref name=":10">{{Cite web|url=https://raymondjames.ca/en_ca/equity_capital_markets/equity_research/sample_research/docs/Descartes%2020140324.pdf|title=Canada Research: The Descartes Systems Group Inc.|last=Li|first=Steven|last2=Lo|first2=Jonathan|date=March 24, 2014|website=Raymond James Ltd.|publisher=|access-date=July 20, 2016}}</ref> such as [[ground transportation]] companies, airlines, ocean carriers, [[freight forwarder]]s, manufacturers, distributors, and retailers.<ref>{{Cite news|url=http://www.therecord.com/news-story/2625294-descartes-systems-thrives-in-perfect-storm-/|title=Descartes Systems thrives in ‘perfect storm’|last=|first=|date=March 7, 2013|work=Waterloo Region Record|access-date=July 27, 2016|via=}}</ref><ref>{{Cite news|url=http://www.financialpost.com/entrepreneur/without-borders/Deliver+sustainability/4747782/story.html|title=Deliver for sustainability|last=Deveau|first=Denise|date=May 9, 2011|work=Financial Post|access-date=July 27, 2016|via=}}</ref><ref>{{Cite web|url=http://www.clresearch.com/research/detail.cfm?guid=2C5FD36D-3048-79ED-996A-8C1045ADE570|title=Highlights from Descartes Evolution 2016|last=Grackin|first=Ann|date=April 21, 2016|website=ChainLink Research|publisher=|access-date=July 28, 2016}}</ref><ref>{{Cite web|url=http://www.joc.com/international-logistics/logistics-technology/big-data%E2%80%99s-benefits-touted-logistics-sector_20140613.html|title=Big Data’s benefits touted for logistics sector|last=Field|first=Alan|date=June 13, 2014|website=The Journal of Commerce|publisher=|access-date=July 28, 2016}}</ref><ref name=":3">{{Cite web|url=http://www.joc.com/international-logistics/logistics-technology/new-delivery-methods-lower-tms-costs-barriers_20130724.html|title=New Delivery Methods Lower TMS Costs, Barriers|last=Biederman|first=David|date=July 24, 2013|website=The Journal of Commerce|publisher=|access-date=July 28, 2016}}</ref><ref name=":4">{{Cite web|url=http://www.joc.com/trucking-logistics/trucking-through-cloud_20110404.html|title=Trucking Through the Cloud|last=Biederman|first=David|date=April 4, 2011|website=The Journal of Commerce|publisher=|access-date=July 28, 2016}}</ref> Its customers included [[American Airlines]], [[Delta Air Lines|Delta Airlines]], [[Air Canada]], [[British Airways]], [[Maersk Group]], [[Hapag-Lloyd]], [[Con-way]], [[Kuehne + Nagel]], [[DHL International GmbH|DHL International]], [[The Home Depot]], [[Sears Holdings|Sears Brands]], [[Hallmark Cards]], [[Hasbro]], [[Volvo]], [[Ferrellgas]], [[Del Monte Foods|Del Monte]], and [[The Coca-Cola Company]].<ref name=":10" />

In 2016, [[Gartner]] ranked Descartes 6th in its list of the ''Top 20 Supply Chain Management Software Suppliers'', based on revenue of $146 million.<ref>{{Cite web|url=http://www.mmh.com/article/top_20_software_suppliers|title=Top 20 supply chain software suppliers, 2016|last=|first=|date=July 1, 2016|website=Modern Materials Handling|publisher=EH Publishing|access-date=August 18, 2016}}</ref>

== Acquisitions ==
Acquisitions have played a key role in Descartes’ growth.<ref name=":5">{{Cite news|url=http://www.therecord.com/news-story/4604857-descartes-plans-big-share-offering/|title=Descartes plans big share offering|last=Pender|first=Terry|date=June 27, 2014|work=Waterloo Region Record|access-date=July 27, 2016|via=}}</ref><ref name=":8" /><ref name=":10" /><ref>{{Cite news|url=http://www.therecord.com/news-story/2579447-descartes-keeping-eye-out-for-acquisition-opportunities/|title=Descartes keeping eye out for acquisition opportunities|last=|first=|date=June 2, 2011|work=Waterloo Region Record|access-date=July 27, 2016|via=}}</ref><ref name=":12">{{Cite news|url=http://business.financialpost.com/investing/trading-desk/descartes-systems-group-inc-upgraded-as-big-acquisition-may-be-coming?__lsa=8500-1c59|title=Descartes Systems Group Inc upgraded as big acquisition may be coming|last=Ratner|first=Jonathan|date=October 19, 2016|work=Financial Post|access-date=October 20, 2016|via=}}</ref> By acquiring niche technology companies, Descartes has expanded its line of logistics software and services, enlarged its customer base, and extended its business geographically.<ref name=":2" /><ref name=":10" /><ref>{{Cite news|url=https://www.wsj.com/articles/sap-links-with-descartes-for-transportation-management-service-1438199806|title=Software Firm Descartes Raising Funds for More Acquisitions|last=Chao|first=Loretta|date=May 26, 2016|work=The Wall Street Journal|access-date=July 28, 2016|via=}}</ref><ref>{{Cite news|url=https://www.wsj.com/articles/todays-top-supply-chain-and-logistics-news-from-wsj-1464345382|title=Today’s Top Supply Chain and Logistics News From WSJ|last=Baskin|first=Brian|date=May 27, 2016|work=The Wall Street Journal|access-date=July 28, 2016|via=}}</ref><ref name=":13">{{Cite web|url=http://www.joc.com/international-logistics/logistics-technology/descartes/appterra-descartes-grows-cloud-based-logistics_20161013.html|title=With Appterra, Descartes grows cloud-based logistics|last=Cassidy|first=William|date=October 13, 2016|website=The Journal of Commerce|publisher=|access-date=October 20, 2016}}</ref>

Acquisitions going back to 2006 are listed below.
{| class="wikitable"
!Year Acquired
!Company
!Specialty
!Country
!References
|-
|2006
|ViaSafe
|Cross-border and regulatory compliance software
|Canada
|<ref>{{Cite news|url=http://www.theglobeandmail.com/report-on-business/descartes-expands-services-with-acquisition-of-viasafe/article706669/|title=Descartes expands services with acquisition of ViaSafe|last=|first=|date=April 11, 2006|work=The Globe and Mail|access-date=July 20, 2016|via=}}</ref>
|-
|2006
|Flagship Customs Services
|Customs filing software
|USA
|<ref>{{Cite web|url=https://www.joc.com/economy-watch/descartes-acquires-flagship_20060706.html|title=Descartes Acquires Flagship|last=|first=|date=July 6, 2006|website=The Journal of Commerce|publisher=|access-date=August 8, 2016}}</ref>
|-
|2006
|Cube Route
|On-demand logistics management software
|Canada
|<ref>{{Cite web|url=http://mhlnews.com/archive/descartes-acquires-assets-cube-route|title=Descartes Acquires Assets of Cube Route|last=Witt|first=Clyde|date=November 1, 2006|website=Material Handling & Logistics|publisher=|access-date=October 20, 2016}}</ref>
|-
|2007
|Ocean Tariff Bureau and Blue Pacific Services
|Tariff filing & contract publishing and surety bonds for ocean intermediaries
|USA
|<ref name=":8" /><ref name=":10" />
|-
|2007
|Global Freight Exchange
|Electronic air cargo booking system
|UK
|<ref>{{Cite web|url=http://www.joc.com/economy-watch/descartes-acquires-gf-x_20070816.html|title=Descartes acquires GF-X|last=Hoffman|first=William|date=August 16, 2007|website=Traffic World/The Journal of Commerce|publisher=|access-date=October 20, 2016}}</ref>
|-
|2007
|RouteView Technologies
|Delivery management software
|USA
|<ref>{{Cite web|url=http://www.fleetfinancials.com/news/story/2008/01/descartes-acquires-routeview-technologies.aspx|title=Descartes Acquires RouteView Technologies|last=|first=|date=January 1, 2008|website=Fleet Financials|publisher=|access-date=August 8, 2016}}</ref>
|-
|2008
|Pacific Coast Tariff Bureau
|Regulatory compliance services
|USA
|<ref>{{Cite web|url=http://www.itworldcanada.com/article/descartes-snaps-up-pacific-coast-tariff-bureau-for-2-1-million/1684|title=Descartes snaps up Pacific Coast Tariff Bureau for $2.1-million|last=Smith|first=Briony|date=January 9, 2008|website=IT World Canada|publisher=|access-date=August 8, 2016}}</ref>
|-
|2008
|Mobitrac Fleet Management Business
|Fleet routing and scheduling SaaS
|USA
|<ref>{{Cite web|url=http://www.ccjdigital.com/descartes-acquires-mobitrac-fleet-management-business/|title=Descartes acquires Mobitrac fleet management business|last=|first=|date=January 11, 2008|website=Commercial Carrier Journal|publisher=|access-date=August 8, 2016}}</ref>
|-
|2008
|Dexx
|European customs filing and logistics messaging
|Belgium
|<ref>{{Cite web|url=http://www.joc.com/air-cargo/descartes-buys-dexx_20081001.html|title=Descartes buys Dexx|last=|first=|date=October 1, 2008|website=The Journal of Commerce|publisher=|access-date=July 29, 2016}}</ref>
|-
|2009
|Oceanwide
|SaaS customs and regulatory compliance
|Canada/USA
|<ref>{{Cite web|url=http://www.joc.com/economy-watch/descartes-acquires-oceanwide_20090205.html|title=Descartes Acquires Oceanwide|last=|first=|date=February 5, 2009|website=The Journal of Commerce|publisher=|access-date=July 29, 2016}}</ref>
|-
|2009
|Scancode Systems
|Parcel and [[Less than truckload shipping|LTL]] shipping software
|Canada
|<ref>{{Cite web|url=http://mhlnews.com/transportation-amp-distribution/descartes-acquisitions-continue|title=Descartes Acquisitions Continue|last=|first=|date=March 11, 2009|website=Material Handling & Logistics|publisher=|access-date=July 20, 2016}}</ref>
|-
|2010
|Porthus
|Global trade management software
|Belgium
|<ref name=":10" /><ref>{{Cite web|url=http://www.streetinsider.com/Mergers+and+Acquisitions/Descartes+Systems+(DSGX)+Subsidiary+Offers+%E2%82%AC12.50Share+for+Zemblaz+NV/5369421.html|title=Descartes Systems (DSGX) Subsidiary Offers €12.50/Share for Zemblaz NV|last=|first=|date=February 22, 2010|website=Street Insider|publisher=|access-date=August 8, 2016}}</ref>
|-
|2010
|[[Imanet]]
|On-demand Canadian customs filing software
|Canada
|<ref>{{Cite web|url=https://www.joc.com/economy-watch/descartes-acquires-imanet_20100421.html|title=Descartes Acquires Imanet|last=Gallagher|first=Thomas|date=April 21, 2010|website=The Journal of Commerce|publisher=|access-date=August 8, 2016}}</ref>
|-
|2010
|Routing International
|Route planning software
|Belgium
|<ref>{{Cite web|url=http://www.logisticsmgmt.com/article/logistics_technology_descartes_acquires_routing_international|title=Descartes acquires Routing International|last=Berman|first=Jeff|date=June 18, 2010|website=Logistics Management|publisher=|access-date=October 20, 2016}}</ref>
|-
|2011
|Telargo
|Fleet [[telematics]] data gathering software and services
|USA/Slovenia
|<ref>{{Cite news|url=http://www.therecord.com/news-story/2581324-descartes-acquires-telematics-data-company/|title=Descartes acquires telematics data company|last=|first=|date=June 13, 2011|work=Waterloo Region Record|access-date=July 27, 2016|via=}}</ref>
|-
|2011
|IntercommIT
|Electronic exchange of trade data
|Netherlands
|<ref>{{Cite news|url=http://www.therecord.com/news-story/2589677-descartes-acquires-dutch-firm-for-13-8-million-us/|title=Descartes acquires Dutch firm for $13.8 million US|last=|first=|date=November 3, 2011|work=Waterloo Region Record|access-date=July 29, 2016|via=}}</ref>
|-
|2012
|GeoMicro
|Vehicle routing and navigation technology
|USA
|<ref>{{Cite news|url=http://www.theglobeandmail.com/globe-investor/descartes-acquires-california-based-geomicro/article551249/|title=Descartes Acquires California-based GeoMicro|last=|first=|date=January 23, 2012|work=The Globe and Mail|access-date=August 8, 2016|via=}}</ref>
|-
|2012
|Infodis
|Transportation management SaaS
|Netherlands
|<ref>{{Cite web|url=https://www.joc.com/international-logistics/logistics-technology/descartes-acquires-dutch-transportation-software-provider_20120604.html|title=Descartes Acquires Dutch Transportation Software Provider|last=Leach|first=Peter|date=June 4, 2012|website=The Journal of Commerce|publisher=|access-date=August 8, 2016}}</ref>
|-
|2012
|[[IES Ltd|Integrated Export Systems]]
|Logistics SaaS
|USA/Hong Kong
|<ref>{{Cite web|url=https://www.joc.com/international-logistics/global-sourcing/descartes-acquire-ies_20120613.html|title=Descartes to Acquire IES|last=|first=|date=June 13, 2012|website=The Journal of Commerce|publisher=|access-date=July 29, 2016}}</ref>
|-
|2012
|Exentra
|EU driver compliance SaaS
|UK
|<ref>{{Cite web|url=http://www.dcvelocity.com/articles/20121114-descartes-acquires-exentra/|title=Descartes acquires Exentra for $17 million|last=|first=|date=November 14, 2012|website=DC Velocity|publisher=|access-date=August 8, 2016}}</ref>
|-
|2013
|KSD Software Norway
|Electronic customs filing software
for the EU
|Norway
|<ref>{{Cite news|url=http://www.therecord.com/news-story/2627359-descartes-buys-customs-filing-specialist-for-33-million-us/|title=Descartes buys customs filing specialist for $33 million US|last=|first=|date=May 2, 2013|work=Waterloo Region Record|access-date=July 27, 2016|via=}}</ref>
|-
|2013
|Impatex Freight Software
|Electronic customs filing and freight forwarding software
|UK
|<ref>{{Cite web|url=http://www.aircargonews.net/news/single-view/news/descartes-acquires-impatex.html|title=Descartes acquires Impatex|last=|first=|date=December 28, 2013|website=Air Cargo News|publisher=|access-date=October 20, 2016}}</ref>
|-
|2013
|Compudata
|Supply chain integration and e-invoicing software
|Switzerland
|<ref>{{Cite web|url=http://fleetowner.com/news/descartes-announces-two-more-acquisitions|title=Descartes announces two more acquisitions|last=|first=|date=December 27, 2013|website=Fleet Owner|publisher=|access-date=October 20, 2016}}</ref>
|-
|2014
|Computer Management
|Security information filing services and air cargo management software
|USA
|<ref>{{Cite web|url=http://www.joc.com/international-logistics/logistics-technology/descartes/descartes-acquires-us-based-air-freight-solutions-firm_20140401.html|title=Descartes Acquires US-Based Air Freight Solutions Firm|last=Lavigne|first=Grace|date=April 1, 2014|website=The Journal of Commerce|publisher=|access-date=October 20, 2016}}</ref>
|-
|2014
|Customs Info
|Trade data and research tools
|USA
|<ref>{{Cite web|url=https://www.joc.com/international-logistics/logistics-technology/descartes/descartes-buys-trade-data-firm-customs-info_20140602.html|title=Descartes Buys Trade Data Firm Customs Info|last=|first=|date=June 2, 2014|website=The Journal of Commerce|publisher=|access-date=August 8, 2016}}</ref>
|-
|2014
|[[Airclic]]
|Mobile logistics automation
software
|USA
|<ref>{{Cite news|url=http://www.philly.com/philly/blogs/inq-phillydeals/Canadian-firm-buys-Airclic-for-30M.html|title=Canadian firm buys AirClic for $30M|last=DiStefano|first=Joseph|date=November 24, 2014|work=The Philadelphia Inquirer|access-date=October 20, 2016|via=}}</ref>
|-
|2014
|Pentant
|Customs connectivity and import/export software
|UK
|<ref name=":15">{{Cite web|url=http://www.joc.com/regulation-policy/customs-regulations/international-customs-regulations/descartes-expands-european-0|title=Descartes expands European reach with two acquisitions|last=|first=|date=December 8, 2014|website=The Journal of Commerce|publisher=|access-date=October 20, 2016}}</ref><ref name=":16">{{Cite web|url=http://www.americanshipper.com/Main/News/Descartes_acquires_two_UKbased_customs_software_pr_58950.aspx#hide|title=Descartes acquires two UK-based customs software providers|last=Johnson|first=Eric|date=December 8, 2014|website=American Shipper|publisher=|access-date=August 8, 2016}}</ref>
|-
|2014
|e-customs
|Electronic security and customs filing software
|UK
|<ref name=":15" /><ref name=":16" />
|-
|2015
|MK Data Services
|Denied party screening data and software
|USA
|<ref name=":14">{{Cite news|url=http://www.therecord.com/news-story/5746640-descartes-acquires-company-that-keeps-track-of-who-not-to-do-business-with/|title=Descartes acquires company that keeps track of who not to do business with|last=Simone|first=Rose|date=July 23, 2015|work=Waterloo Region Record|access-date=July 20, 2016|via=}}</ref>
|-
|2015
|BearWare
|Mobile carton tracking technology for retailers
|USA
|<ref name=":14" /><ref>{{Cite web|url=http://www.joc.com/international-logistics/logistics-technology/descartes/acquisition-aimed-giving-descartes%E2%80%99-shipper-more-supply-chain-visibility_20150723.html|title=Acquisition aimed at giving Descartes’ shippers more supply chain visibility|last=|first=|date=July 23, 2015|website=The Journal of Commerce|publisher=|access-date=October 20, 2016}}</ref>
|-
|2015
|Oz Development
|Logistics automation software for [[Small and medium-sized enterprises|SMEs]]
|USA
|<ref>{{Cite web|url=http://www.joc.com/international-logistics/logistics-technology/descartes/descartes-acquisition-help-growing-e-commerce-demand_20151125.html|title=Descartes acquisition aimed at serving smaller customers|last=|first=|date=November 25, 2015|website=The Journal of Commerce|publisher=|access-date=October 20, 2016}}</ref>
|-
|2016
|Pixi Software
|e-Commerce order fulfillment and warehouse management software
|Germany
|<ref>{{Cite news|url=http://www.therecord.com/news-story/6520498-descartes-buys-german-firm-for-us-10-4-million/|title=Descartes buys German firm for US$10.4 million|last=|first=|date=April 29, 2016|work=Waterloo Region Record|access-date=August 8, 2016|via=}}</ref>
|-
|2016
|Apterra
|Business-to-business supply chain SaaS
|USA
|<ref name=":13" />
|}

== Global Logistics Network ==
Descartes’ cloud-based logistics messaging system, the Global Logistics Network (GLN), connects more than 13,000 customers in over 160 countries,<ref name=":3" /><ref name=":12" /> making it one of the world’s largest logistics networks.<ref name=":10" /> Each year, the GLN carries more than 4.5 billion messages and manages more than 30 million shipping routes.<ref>{{Cite web|url=http://www.cantechletter.com/2016/10/descartes-acquisition-apterra-gets-thumbs-cantor-fitzgerald/|title=Descartes’ acquisition of Apterra gets thumbs up at Cantor Fitzgerald|last=Waddell|first=Nick|date=October 13, 2016|website=Cantech Letter|publisher=Cantech Communications|access-date=October 20, 2016}}</ref>

Companies use the network to oversee [[shipping]] orders, file customs paperwork, comply with [[National security|security]] regulations, share information across international supply chains, and automate logistics processes.<ref name=":2" /><ref name=":10" /><ref name=":4" /><ref name=":11">{{Cite news|url=https://www.wsj.com/articles/sap-links-with-descartes-for-transportation-management-service-1438199806|title=SAP Links With Descartes for Transportation Management Service|last=Chao|first=Loretta|date=July 29, 2015|work=The Wall Street Journal|access-date=July 29, 2015|via=}}</ref><ref>{{Cite web|url=http://www.joc.com/trucking-logistics/truckload-freight/canadian-emanifest-deadline-looms-us-truckers_20160109.html|title=Canadian eManifest deadline looms for US truckers|last=Cassidy|first=William|date=January 9, 2016|website=The Journal of Commerce|publisher=|access-date=July 28, 2016}}</ref> In 2015, Descartes signed a deal with German [[business software]] giant [[SAP SE]] that allows users of SAP’s transportation management software to access the GLN.<ref name=":11" />

== Conference ==
Descartes holds an annual logistics technology conference for its users and partners. The eleventh Descartes “Evolution” conference was held in 2016.<ref>{{Cite web|url=http://www.warehousinglogisticsinternational.com/descartes-showcases-logistics-technology-platform-innovations-at-global-user-and-partner-conference/|title=Descartes Showcases Logistics Technology Platform Innovations at Global User and Partner Conference|last=|first=|date=April 7, 2016|website=Warehousing & Logistics International|publisher=Quad Publications|access-date=July 28, 2016}}</ref>

== Awards ==
Descartes won the [[Stevie Awards|Stevie Award]] for ''Best Business Turnaround'' at the International Business Awards in 2006.<ref name=":9">{{Cite news|url=http://www.theglobeandmail.com/technology/descartes-wins-stevie/article1099717/|title=Descartes wins Stevie|last=|first=|date=May 24, 2006|work=The Globe and Mail|access-date=July 28, 2016|via=}}</ref><ref>{{Cite web|url=http://stevieawards.com/iba/2006-international-stevie-award-winners|title=2006 International Stevie Award Winners|last=|first=|date=|website=The International Business Awards website|publisher=|access-date=July 28, 2016}}</ref> [[Canadian Business Magazine]] also awarded Descartes an honorary Stevie for being the most notable Canadian entry.<ref>{{Cite web|url=http://www.marketwired.com/press-release/canadian-business-magazine-recognizes-descartes-as-best-canadian-company-during-international-tsx-dsg-603074.htm|title=Canadian Business Magazine Recognizes Descartes as Best Canadian Company During International Business Awards|last=|first=|date=July 11, 2006|website=Marketwired|publisher=|access-date=July 28, 2016}}</ref>

In 2012, Waterstone Human Capital named Descartes to its list of ''Canada’s 10 Most Admired Corporate Cultures'' in the “Mid-Market” category.<ref>{{Cite news|url=http://business.financialpost.com/corporate-culture-awards/canadas-10-most-admired-corporate-cultures-of-2012|title=Canada’s 10 most admired corporate cultures of 2012|last=|first=|date=February 4, 2013|work=Financial Post|access-date=July 25, 2016|via=}}</ref><ref>{{Cite news|url=https://www.descartes.com/content/documents/cmacc-np-nov.29.2012.pdf|title=Special Report on Corporate Culture Awards: The Culture Club|last=Bitti|first=Mary Teresa|date=November 29, 2012|work=National Post|access-date=July 20, 2016|via=}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|https://www.descartes.com/}}{{Finance links|name=The Descartes Systems Group|symbol=DSGX|sec_cik=1050140|hoovers=descartes_systems_group_inc_the.0fa3dee7433af7a4}}

[[Category:Companies listed on the Toronto Stock Exchange]]
[[Category:1981 establishments in Ontario]]
[[Category:Companies established in 1981]]
[[Category:Software companies established in 1981]]
[[Category:Companies based in Waterloo, Ontario]]
[[Category:Software companies of Canada]]
[[Category:S&P/TSX Composite Index]]
[[Category:Companies listed on NASDAQ]]
[[Category:1981 establishments in Canada]]
[[Category:Multinational companies headquartered in Canada]]
[[Category:Business software companies]]
[[Category:Supply chain software companies]]