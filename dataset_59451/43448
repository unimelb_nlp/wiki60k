{{Use dmy dates|date=June 2013}}
{{Infobox project
| projectname = Project Loon
| logo = 
| website = [https://x.company/loon/ x.company/loon]
| missionstatement = Balloon-powered Internet for everyone
| commercial = Yes
| type = [[Internet]] and [[telecommunication]]
| location = Worldwide
}}
[[File:Google Loon Balloon close up.jpg|thumb|A Project Loon balloon at the [[Christchurch]] launch event in June, 2013]]

'''Project Loon''' is a [[research and development]] project being developed by [[X (company)|X]] (formerly Google X) with the mission of providing [[Internet access]] to [[rural area|rural]] and remote areas. The project uses [[high-altitude balloon]]s placed in the [[stratosphere]] at an altitude of about {{convert|18|km|mi|abbr=on}} to create an aerial [[wireless network]] with up to 4G-[[LTE (telecommunication)|LTE]] speeds.<ref name="wired 2013-06">{{cite web|last=Levy|first=Steven|title=How Google Will Use High-Flying Balloons to Deliver Internet to the Hinterlands|url=https://www.wired.com/business/2013/06/google_internet_balloons/all/google.com/loon|date=14 June 2013|work=Wired|accessdate=15 June 2013}}</ref><ref name="AFP 2013-06">{{cite web|url=https://www.google.com/hostednews/afp/article/ALeqM5jzP2DG8WdyTFsRBXkX5IHRDod8UA?docId=CNG.1e0cb853eae78a4ee2a211df3413fa9a.01|archive-url=https://web.archive.org/web/20130617220246/http://www.google.com/hostednews/afp/article/ALeqM5jzP2DG8WdyTFsRBXkX5IHRDod8UA?docId=CNG.1e0cb853eae78a4ee2a211df3413fa9a.01|dead-url=yes|archive-date=17 June 2013|title=Google to beam Internet from balloons|date=15 June 2013|work=Agence France-Presse|publisher=Google|accessdate=16 June 2013}}</ref><ref>{{cite web|title=Google launches Project Loon|url=http://www.nzherald.co.nz/internet/news/article.cfm?c_id=137&objectid=10890750|date=15 June 2013|work=The New Zealand Herald|accessdate=15 June 2013}}</ref><ref>{{cite web|last=Lardinois|first=Frederic|title=Google X Announces Project Loon: Balloon-Powered Internet For Rural, Remote And Underserved Areas|url=http://techcrunch.com/2013/06/14/google-x-announces-project-loon-balloon-powered-internet-for-rural-remote-and-underserved-areas/|date=14 June 2013|work=TechCrunch|accessdate=15 June 2013}}</ref> It was named Project Loon, since even Google itself found the idea of providing Internet access to the remaining 5 billion population unprecedented and crazy/loony.<ref name="wired 2013-06"/>

The balloons are maneuvered by adjusting their altitude in the [[stratosphere]] to float to a wind layer after identifying the wind layer with the desired speed and direction using wind data from the [[National Oceanic and Atmospheric Administration]] (NOAA). Users of the service connect to the balloon network using a special Internet antenna attached to their building. The signal travels through the balloon network from balloon to balloon, then to a ground-based station connected to an [[Internet service provider]] (ISP), then onto the global Internet. The system aims to bring Internet access to remote and rural areas poorly served by existing provisions, and to improve communication during [[natural disaster]]s to affected regions.<ref name="CNET netaccess">{{cite web|last=Mack|first=Eric|title=Meet Google's 'Project Loon:' Balloon-powered 'net access|url=http://news.cnet.com/8301-17938_105-57589464-1/meet-googles-project-loon-balloon-powered-net-access/|work=CNET|accessdate=15 June 2013|date=14 June 2013}}</ref><ref name="arstechnica 2013-06">{{cite web|last=Brodkin|first=Jon|title=Google flies Internet balloons in stratosphere for a "network in the sky"|url=http://arstechnica.com/information-technology/2013/06/google-flies-internet-balloons-in-stratosphere-for-a-network-in-the-sky/|date=14 June 2013|work=ArsTechnica|accessdate=15 June 2013}}</ref> Key people involved in the project include Rich DeVaul, chief technical architect, who is also an expert on [[wearable technology]]; [[Mike Cassidy (entrepreneur)|Mike Cassidy]], a project leader; and Cyrus Behroozi, a networking and telecommunication lead.<ref name="wired 2013-06"/>

The balloons use [[patch antenna]]s - which are [[directional antenna]]s - to transmit signals to ground stations or LTE users. Some smartphones with Google SIM cards can use Google Internet services. The whole infrastructure is based on LTE; the [[eNodeB]] component (the equivalent of the "base station" that talks directly to handsets) is carried in the balloon.

== History ==

In 2008, Google considered contracting with or acquiring Space Data Corp., a company that sends balloons carrying small base stations about {{convert|20|mi|km|Abbr=on}} up in the air for providing connectivity to truckers and oil companies in the southern United States, but didn't do so.<ref>{{cite web|last=Sharma|first=Amol|title=Floating a New Idea For Going Wireless, Parachute Included|url=https://online.wsj.com/public/article/SB120347353988378955.html|archive-url=https://web.archive.org/web/20080223105103/http://online.wsj.com:80/public/article/SB120347353988378955.html|dead-url=yes|archive-date=23 February 2008|date=20 February 2008|work=The Wall Street Journal|accessdate=16 June 2013}}</ref>

Unofficial development on the project began in 2011 under incubation in [[Google X]] with a series of trial runs in [[California]]'s [[Central Valley (California)|Central Valley]]. The project was officially announced as a Google project on 14 June 2013.<ref name="wired 2013-06"/>

On 16 June 2013, Google began a [[pilot experiment]] in [[New Zealand]] where about 30 balloons were launched in coordination with the [[Civil Aviation Authority of New Zealand|Civil Aviation Authority]] from the [[Lake Tekapo (town)|Tekapo area]] in the [[South Island]]. About 50 local users in and around [[Christchurch]] and the [[Canterbury Region]] tested connections to the aerial network using special antennas.<ref name="wired 2013-06"/> After this initial trial, Google plans on sending up 300 balloons around the world at the [[40th parallel south]] that would provide coverage to New Zealand, Australia, Chile, and Argentina. Google hopes to eventually have thousands of balloons flying in the stratosphere.<ref name="wired 2013-06"/><ref name="AFP 2013-06"/>

In May 2014, [[Google X]] laboratories director, [[Astro Teller]], announced that, rather than negotiate a section of bandwidth that was free for them worldwide, they would instead become a temporary base station that could be leased by the mobile operators of the country it was crossing over.

In May–June 2014 Google tested its balloon-powered internet access venture in [[Piauí]], [[Brazil]], marking its first [[LTE (telecommunication)|LTE]] experiments and launch near the equator.<ref>[http://thenextweb.com/google/2014/06/16/google-celebrates-project-loons-birthday-first-lte-experiments-launch-near-equator/ The Next Web: Google celebrates Project Loon's birthday with first LTE experiments and launch near the equator]</ref>

In 2014 Google partnered with [[France]]'s Centre national d'études spatiales ([[CNES]]) on the project.<ref name=awnNeo>{{cite news |url=http://aviationweek.com/space/google-france-partner-balloon-powered-internet |title=Google, France Partner On Balloon-Powered Internet |work=[[Aviation Week & Space Technology]] |first=Amy |last=Svitak |date=12 December 2014 |accessdate=14 December 2014 |archiveurl=https://web.archive.org/web/20141214160826/http://aviationweek.com/space/google-france-partner-balloon-powered-internet |archivedate=14 December 2014 |deadurl=no}}</ref>

In Feb, 2014, the record streak for a balloon lasting in the stratosphere was 50 days. In Nov 2014, the record was 130 days, and in March 2, 2015, the record for a continuous balloon flight is 187 days (over 6 months).

On 28 July 2015, Google signed an agreement with officials of Information and Communication Technology Agency (ICTA) - [[Sri Lanka]], to launch the technology on a mass scale.<ref name="ibtimes.co.uk">[http://www.ibtimes.co.uk/google-project-loon-provide-free-wifi-across-sri-lanka-1513136 Google Project Loon to provide free internet across Sri Lanka<!-- Bot generated title -->]</ref> As a result, by March 2016,<ref name="ibtimes.co.uk"/> Sri Lanka will be the second country in the world to get full coverage of internet using [[LTE (telecommunication)|LTE]], after [[Vatican City]].

On 29 October 2015, Google agreed to partner with [[Indonesia]]'s [[XL Axiata]], [[Indosat]] and [[Telkomsel]] to bring the technology to the country in the hopes of connecting its 17,000 islands.<ref>{{cite web|title=Google Loon In Indonesia: The Large Ambition To Deliver Internet Connectivity|url=http://www.eyerys.com/articles/news/google-loon-indonesia-large-ambition-deliver-internet-connectivity|publisher=Eyerys}}</ref>

On 25 February 2016, Google started testing their autolauncher named "Chicken Little" at former naval station [[Roosevelt Roads Naval Station|Roosevelt Roads]] located in Ceiba, Puerto Rico.<ref>{{cite web|title=Project Loon shows off autolauncher at work in Puerto Rico|url=https://www.engadget.com/2016/02/26/project-loon-autolauncher-peurto-rico/|publisher=Engadget}}</ref>

On September 5, 2016, a balloon was spotted over Newfoundland and Labrador, Canada.

== Technology ==

Project Loon is Google's pursuit to deploy a [[High-altitude platform|high-altitude balloon network]] operating in the [[stratosphere]], at altitudes between 18&nbsp;km and 25&nbsp;km. Google asserts that this particular layer of the stratosphere is advantageous because of its relatively low wind speeds (e.g., wind speeds between 5 and 20&nbsp;mph / 10 to 30 kmph) and minimal [[turbulence]]. Moreover, Google claims that it can model, with reasonable accuracy, the seasonal, longitudinal, and latitudinal variations in wind speeds within the 18–25&nbsp;km stratospheric layer.<ref name="US 13/346,636">{{cite patent
 | country = US
 | number = 13/346,636
 | status = application
 | title = Balloon network with free-space optical communication between super-node balloons and RF communication between super-node and sub-node balloons 
 | pubdate = 2013-07-11
 | fdate = 2012-01-09
 | inventor = Richard Wayne DeVaul, Eric Teller, Clifford L. Biffle, Josh Weaver
 | invent1 = Richard Wayne DeVaul
 | invent2 = Eric Teller
 | invent3 = Clifford L. Biffle
 | invent4 = Josh Weaver
 | assign1 = Google Inc.
}}</ref>

Given a reasonably accurate model of wind speeds within the 18–25&nbsp;km band, Google claims that it can control the latitudinal and longitudinal position of high-altitude balloons by adjusting only the balloon's altitude.<ref name="US 13/346,636" /> By adjusting the volume and density of the gas (e.g., helium, hydrogen, or another lighter-than-air compound) in the balloon, the balloon's variable [[buoyancy]] system is able to control the balloon's altitude.<ref name="US 13/346,636" /> Google has additionally indicated that balloons may be constructed from various materials (e.g., metalized [[Mylar]] or [[BoPET]]) or a highly flexible latex or rubber material (e.g., [[chloroprene]]).<ref name="US 13/346,636" />

Initially, the balloons communicated using unlicensed 2.4 and 5.8&nbsp;GHz [[ISM band]]s,<ref name="Google project page">{{cite web|title=How Loon Works|url=https://www.google.com/loon/how/|work=Google|accessdate=16 June 2013}}</ref> and Google claims that the setup allows it to deliver "speeds comparable to [[3G]]" to users, but they then switched to [[LTE (telecommunication)|LTE]]<ref>[https://www.youtube.com/watch?v=HOndhtfIXSY Project Loon: Scaling Up]</ref> with cellular spectrum by cooperating with local telecommunication operators.<ref>[https://www.google.com/loon/how/ Project Loon<!-- Bot generated title -->]</ref> It is unclear how technologies that rely on short communications times (low latency [[Ping (networking utility)|pings]]), such as [[Voice over IP|VoIP]], might need to be modified to work in an environment similar to mobile phones where the signal may have to relay through multiple balloons before reaching the wider Internet.<ref name="NS 2013-06-20">{{cite web|last=Hodson|first=Hal|title=Google's Project Loon to float the internet on balloons|url=http://www.newscientist.com/article/dn23721-googles-project-loon-to-float-the-internet-on-balloons.html|work=New Scientist|date=18 June 2013|accessdate=20 June 2013}}</ref><ref>{{cite journal|last=Misra|first=Archan|author2=Das, Subir |author3=McAuley, Anthony J. |title=Hierarchical Mobility Management for VoIP Traffic|year=2001|url=http://researchweb.watson.ibm.com/people/a/archan/milcom2001b.pdf|accessdate=17 October 2013}}</ref>

The first person to connect to the "Google Balloon Internet" after the initial test balloons were launched into the stratosphere was a farmer in the town of [[Leeston]], New Zealand, who was one of 50 people in the area around Christchurch who agreed to be a pilot tester for Project Loon. The New Zealand farmer lived in a rural location that couldn't get broadband access to the Internet, and had used a satellite Internet service in 2009, but found that he sometimes had to pay over $1000 per month for the service. The locals knew nothing about the secret project other than its ability to deliver Internet connectivity, but allowed project workers to attach a basketball-sized receiver resembling a giant bright-red party balloon to an outside wall of their property in order to connect to the network.<ref name="AP 2013-06"/><ref>{{cite web|last=Smith|first=Mac|title=Ask Away: How was the antenna casing designed?|url=https://plus.google.com/+ProjectLoon/videos|archive-url=https://web.archive.org/web/20131007120826/https://plus.google.com/+ProjectLoon/videos|dead-url=yes|archive-date=2013-10-07|publisher=Google Project Loon|accessdate=18 October 2013|author2=Heinrich, Mitch |author3=Wasson, Brian |format=Video|date=2013-08-23|quote=6s: "The challenge for us, we have this big network of balloons that can provide Internet connectivity to people on the ground, but the people who are getting that service can't actually see the balloons. In fact, the only thing that they see from day to day is the device that's attached to their house." - Mac Smith ... 35s: "so we decided to iterate on the antenna form to make it more balloon-like." - Brian Wasson}}</ref><!-- Using the same frequencies as cellular networks does not enforce the same latency constraints. The latency constraints are results of the VoIP application. If Google plans to use spectrum usually allocated to cellular networks for data applications only it will be able to do so. -->

The technology designed in the project could allow countries to avoid using expensive fiber cable that would have to be installed underground to allow users to connect to the Internet. Google feels this will greatly increase Internet usage in developing countries in regions such as Africa and Southeast Asia that can't afford to lay underground fiber cable.<ref name="AP 2013-06">{{cite news|title=Google launches Internet-beaming balloons|url=http://bigstory.ap.org/article/google-begins-launching-internet-beaming-balloons|author=Perry, Nick|author2=Mendoza, Martha |date=15 June 2013|work=Associated Press|accessdate=17 June 2013}}</ref>

== Equipment ==
[[File:Google Loon - Launch Event.jpg|thumb|A Project Loon research balloon]]

The [[Balloon (aircraft)#As flying machines|balloon envelopes]] used in the project are made by Raven Aerostar,<ref>{{cite web|title=Project Loon: Raven Aerostar - Google Collaboration|url=http://ravenaerostar.com/about/project-loon-raven-aerostar-google|work=Raven Aerostar|accessdate=15 June 2013}}</ref> and are composed of [[polyethylene]] plastic about {{convert|0.076|mm|in|abbr=on}} thick. The balloons are [[superpressure balloon]]s filled with [[helium]], standing {{convert|15|m|ft|abbr=on}} across and {{convert|12|m|ft|abbr=on}} tall when fully inflated. They carry a custom air pump system dubbed the "Croce"<ref>{{cite web|last=Gartner|first=Keegan|title=Ask Away: How do the balloons move up and down?|url=https://plus.google.com/+ProjectLoon/videos|archive-url=https://web.archive.org/web/20131007120826/https://plus.google.com/+ProjectLoon/videos|dead-url=yes|archive-date=2013-10-07|publisher=Google Project Loon|accessdate=18 October 2013|author2=Ratner, Dan |date=2013-08-14|quote=58s into video: We call this air control system "Croce" because our co-worker lead saw the shape of our impeller housing was bottle shaped and started singing "Time in a Bottle" [by] Jim Croce}}</ref> that pumps in or releases air to [[ballast]] the balloon and control its elevation.<ref name="wired 2013-06"/> A small box weighing {{convert|10|kg|lb|abbr=on}} containing each balloon's electronic equipment hangs underneath the inflated envelope. This box contains [[printed circuit board|circuit boards]] that control the system, [[Antenna (radio)|radio antennas]] and a [[Ubiquiti Networks]] 'Rocket M2'<ref name="ubnt"/> to communicate with other balloons and with Internet antennas on the ground, and [[Battery (electricity)|batteries]] to store solar power so the balloons can operate during the night. Each balloon’s electronics are powered by an array of [[solar panel]]s that sit between the envelope and the hardware. In full sun, the panels produce 100 watts of power, which is sufficient to keep the unit running while also charging a battery for use at night. A parachute attached to the top of the envelope allows for a controlled descent and landing when a balloon is ready to be taken out of service.<ref name="Google project page"/> In the case of an unexpected failure, the parachute deploys automatically.<ref>{{cite web|last=Kelion|first=Leo|title=Google tests balloons to beam internet from near space|url=http://www.bbc.co.uk/news/technology-22905199|work=BBC News|date=15 June 2013|accessdate=21 June 2013}}</ref> When taken out of service, the balloon is guided to an easily reached location, and the [[Helium shortage|helium is vented into the atmosphere]]. The balloons typically have a maximum life of about 100 days, although Google claims that its tweaked design can enable them to stay aloft for closer to 200 days.<ref name="MyUser_Ars_Technica_March_11_2015c">{{cite web |url=http://arstechnica.com/information-technology/2015/03/google-balloons-cell-towers-in-the-sky-can-serve-4g-to-a-whole-state/ |title=Google balloons, "cell towers in the sky," can serve 4G to a whole state |newspaper=Ars Technica |date= March 11, 2015 |author=Jon Brodkin |accessdate= March 11, 2015}}</ref>

The prototype ground stations use a Ubiquiti Networks 'Rocket M5'<ref name="ubnt">{{cite web|first=smweir|title=Re: Internet for all|url=http://community.ubnt.com/t5/The-Lounge/Internet-for-all/m-p/491445#M24270|publisher=Ubiquiti Networks Community Forum|quote=I've just been down to talk to the folks from Google, who are here in Christchurch, New Zealand, launching their pilot for Loon. One engineer told me "we use the Ubiquiti Rocket M2 for transceiving, and the M5 for groundstation uplink". He described the downwards-pointing antenna on the ballon, which sounded to me like a UniFi polar map, but on a bigger scale. They have modified the firmware to only work with other modified firmware Rockets.}}</ref> radio and a custom [[patch antenna]]<ref>{{cite web|last=Behroozi|first=Cyrus|title=Ask Away: What's inside the Loon antenna?|url=https://plus.google.com/+ProjectLoon/videos|archive-url=https://web.archive.org/web/20131007120826/https://plus.google.com/+ProjectLoon/videos|dead-url=yes|archive-date=7 October 2013|publisher=Google Project Loon|accessdate=18 October 2013}}</ref> to connect to the balloons at a height of {{convert|20|km|mi|abbr=on|adj=on}}.<ref name="arstechnica 2013-06"/> Some reports have called Google's project the Google Balloon Internet.<ref name="AFP 2013-06"/><ref>{{cite news|url=http://www.cbsnews.com/8301-205_162-57589468/googles-ambitious-internet-balloons-soar-above-new-zealand/|title=Google's ambitious Internet balloons soar above New Zealand|date=15 June 2013 |publisher=CBS News|agency=Associated Press|accessdate=18 June 2013}}</ref><ref>{{cite news|url=http://www.nbcnews.com/technology/google-begins-launching-internet-beaming-balloons-6C10331940|archive-url=https://web.archive.org/web/20130615185152/http://www.nbcnews.com/technology/google-begins-launching-internet-beaming-balloons-6C10331940|dead-url=yes|archive-date=15 June 2013|title=Google begins launching Internet-beaming balloons|author=Mendoza, Martha|author2=Perry, Nick |date=15 June 2013|agency=Associated Press|publisher=NBCNews|accessdate=18 June 2013}}</ref>

The balloons are equipped with [[automatic dependent surveillance – broadcast]] and so can be publicly tracked (along with other balloons) with the call-sign "HBAL" <ref>[https://www.flightradar24.com/blog/keep-your-eye-on-the-hbal-tracking-project-loon-balloons/ Keep Your Eye on the HBAL—Tracking Project Loon Balloons – Flightradar24 Blog<!-- Bot generated title -->]</ref>

===Incidents===
In May 2014, a Loon balloon crashed into power lines in [[Washington (state)|Washington]], [[United States]].<ref>{{cite web|last1=Lardinois|first1=Frederic|title=One of Google’s Project Loon balloons crashed into power lines in Washington state|url=http://techcrunch.com/2014/06/03/one-of-googles-project-loon-balloons-crashed-into-power-lines-in-washington-state/|website=TechCrunch|publisher=AOL Inc.|accessdate=15 November 2014}}</ref>

On 20 June 2014, New Zealand officials briefly scrambled emergency services personnel when a Loon balloon came down.<ref>{{cite web|last1=Sharwood|first1=Simon|title=That's no plane wreck, that's a Google Wi-Fi balloon: unplanned splashdown scrambles New Zealand emergency services|url=http://www.theregister.co.uk/2014/06/20/thats_no_plane_wreck_thats_a_google_wifi_balloon/|website=The Register|accessdate=15 November 2014|date=20 June 2014}}</ref>

In November 2014, a South African farmer found a crashed Loon balloon in the Karoo desert between Strydenburg and Britstown.<ref name="MyUser_The_Daily_Telegraph_March_11_2015c">{{cite web |url=http://www.telegraph.co.uk/news/worldnews/africaandindianocean/southafrica/11243440/South-African-sheep-farmer-discovers-downed-Google-Loon-balloon-crashed-in-Karoo-Desert.html |title=South African sheep farmer discovers downed Google Loon balloon crashed in Karoo Desert |newspaper=The Daily Telegraph |date= November 20, 2014 |author= |accessdate= March 11, 2015}}</ref>

On 23 April 2015, a Loon balloon crashed in a field near Bragg City, Missouri.<ref>{{cite web|title=Device known as 'Google Loon' lands in Missouri|url=http://www.kctv5.com/story/28906631/device-known-as-google-loon-lands-in-missouri|archive-url=https://web.archive.org/web/20150429180433/http://www.kctv5.com:80/story/28906631/device-known-as-google-loon-lands-in-missouri?|dead-url=yes|archive-date=29 April 2015|website=KCTV 5|accessdate=27 April 2015|date=27 April 2015}}</ref>

On September 12, 2015, a Loon balloon crash landed in the front lawn of a residence on Rancho Hills, Chino Hills, CA.

On 17 February 2016, a Loon balloon crashed in the tea-growing region of Gampola, Sri Lanka while carrying out tests.<ref>{{cite web|title=Google's Internet balloon 'crashes' in Sri Lanka test flight|url=http://phys.org/news/2016-02-google-internet-balloon-sri-lanka.html|website=Phys|accessdate=29 March 2016|date=18 Feb 2016}}</ref>

On April 7, 2016, a Loon balloon landed on a farm in Dundee, KwaZulu-Natal, South Africa.<ref>[http://mybroadband.co.za/vb/showthread.php/810406-Project-Loon-Balloon-found-on-farm-in-KZN 'Project Loon' Balloon found on farm in KZN<!-- Bot generated title -->]</ref>

On April 22, 2016, a Loon balloon crashed in a field in the Ñeembucu department, Paraguay.<ref>[http://www.abc.com.py/ciencia/globo-vino-de-nueva-zelanda-1473302.html Globo de Google aterrizó bajo control - Ciencia - ABC Color<!-- Bot generated title -->]</ref><ref>[http://www.ultimahora.com/globo-google-cae-un-esteral-neembucu-n985467.html Globo de Google cae a un esteral de Ñeembucú<!-- Bot generated title -->]</ref>

On August 22, 2016, a Loon balloon landed on a ranch in Formosa, Argentina about 40 km. West of the Capital of Formosa. <ref>[http://www.clarin.com/sociedad/Cayo-globo-Google-Formosa_0_1637236264.html Cayó un globo de Google en un campo de las modelos Maglietti<!-- Bot generated title -->]</ref>

On August 26, 2016, a Loon balloon landed northwest of Madison, SD.

On January 9, 2017, a Loon Balloon crash in Bocas del Toro, Panama.

On January 8 & 10, 2017 two Loon Balloons landed at 10 km E of Cerro Chato & 40 km NNW of Mariscala, Uruguay.

On February, 17, one Loon Baloon crash in Buri dos Montes, Brazil.<ref>[https://www.oficinadanet.com.br/post/18437-balao-do-project-loon-cai-no-interior-do-piaui Balão do Project Loon cai no interior do Piauí<!-- Bot generated title -->]</ref> 

On March 14, 2017, a Loon Balloon crash in San Luis at Tolima, Colombia.

On March 19, 2017, a Loon Balloon crash in Tacuarembó, Uruguay.

== Reception ==

Project Loon has generally been well received, although [[Square Kilometre Array]] (SKA) project developers and astronomers have raised concerns that the lower of the two ISM bands that Loon uses (2.4&nbsp;GHz) will interfere with the mid-band frequency range (0.5&nbsp;GHz-3&nbsp;GHz) used in the SKA project.<ref>{{cite web|last=Richard Chirgwin|first=Richard|title=Google launches broadband balloons, radio astronomy frets|url=http://www.theregister.co.uk/2013/06/17/google_launches_broadband_balloons_astrophysics_frets/print.html|work=The Register|accessdate=25 June 2013|date=17 June 2013}}</ref>

Google has not yet specified the costs of this project.<ref>{{cite web|last=Hall|first=Brian S.|title=A Handy Guide To Google's Project Loon|url=http://readwrite.com/2013/06/19/a-handy-guide-to-google-project-loon|work=ReadWrite|publisher=Say Media Inc.|accessdate=3 October 2013|date=19 June 2013}}</ref>

New Zealand's Prime Minister [[John Key]] delivered a speech at the launch event in Christchurch stating that the Internet is important for New Zealand to help it globally distribute what it produces in a low cost way as the next 4 billion people come online; Key also acknowledged the potential of utilizing Loon for disaster recovery.<ref>{{cite news|last=Laura Smith-Spark|first=Laura|title=Up, up and away: Google to launch Wi-Fi balloon experiment|url=http://www.cnn.com/2013/06/15/world/asia/new-zealand-google-balloons/index.html|work=CNN.com|publisher=Cable News Network|accessdate=25 October 2013|date=15 June 2013}}</ref>

== See also ==

* [[List of countries by number of Internet users]]
* [[Mobile broadband modem]]
* [[Google Fiber]]
* [[Google X]]
* [[High-altitude balloon]]
* [[Atmospheric satellite]]
* [[Alliance for Affordable Internet]]
* [[Internet.org]]
* [[Geostationary balloon satellite]]
* [[Outernet]]
* [[O3b Networks]]
* [[Google Free Zone]]
* [[Stratovision]]

== References ==
{{Reflist|30em}}

== External links ==
{{Commons category|Project Loon}}
*{{Official website|https://www.google.com/loon/}}
*{{Google+|+ProjectLoon}}
*{{Facebook page|googleprojectloon}}
*{{YouTube|id=m96tYpEk1Ao|title=Introducing Project Loon}}
*{{YouTube|id=mcw6j-QWGMo|title=Project Loon: The Technology}}
*{{YouTube|id=dpIT4mor75o|title=Right Honourable John Key Speaks at Project Loon Launch}}
*[https://www.wired.com/gadgetlab/2013/08/googlex-project-loon/all/ Wired: The Untold Story of Google's Quest to Bring the Internet Everywhere—By Balloon]
* [http://www.economynext.com/Sri_Lanka_inks_deal_for_Google_Loon_broadband_floating_towers-3-2492.html Sri Lanka inks deal for Google Loon broadband floating 'telecom towers]

{{Google Inc.|corporate=no|products=no}}

[[Category:Satellite Internet access]]
[[Category:Balloons (aircraft)]]
[[Category:Internet service providers]]
[[Category:Google X]]