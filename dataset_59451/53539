{{Use mdy dates|date=October 2011}}
{{No footnotes|date=December 2007}}
{{Infobox Politician
|name = Phillips Lee Goldsborough
|image = Phillips Lee Goldsborough photo portrait.jpg
|caption =
|birth_date = {{birth date|1865|8|6}}
|birth_place = [[Princess Anne, Maryland]]
|residence =
|death_date ={{death date and age|1946|10|22|1865|8|6}}
|death_place =
|order1 = 47th
|office1 = Governor of Maryland
|term1 = January 10, 1912&ndash;January 12, 1916
|predecessor1 = [[Austin Lane Crothers|Austin L. Crothers]]
|successor1 = [[Emerson Harrington|Emerson C. Harrington]]
|office2 = [[United States Senator]]<br/>from [[Maryland]]
|term2 = March 4, 1929 &ndash; January 3, 1935
|predecessor2= [[William Cabell Bruce]]
|successor2= [[George L. P. Radcliffe]]
|party = [[Republican Party (United States)|Republican]]
|religion = [[Episcopal Church in the United States of America|Episcopalian]]
|spouse = Ellen Showell
|children = Nancy (1894&ndash;1895), Phillips Lee Jr. (born c. 1898), Brice (born c. 1904)
|website =
|footnotes =
}}
'''Phillips Lee Goldsborough I''' (August 6, 1865{{spaced ndash}}October 22, 1946), was a [[Republican Party (United States)|Republican]] member of the [[United States Senate]] representing [[Maryland|State of Maryland]] from 1929 to 1935. He was also the [[List of Governors of Maryland|47th]] [[Governor of Maryland]] from 1912 to 1916 and Comptroller of the Maryland Treasury from 1898 to 1900.

==Early life and career==
Goldsborough was born in [[Princess Anne, Maryland]] and was educated in public and private schools. While working as a clerk for the [[United States Navy]], he studied law and was admitted to the bar in 1886, commencing practice in [[Cambridge, Maryland]] soon thereafter. He also held an interest in banking. In 1893 he married Mary Ellen Showell (c. 1865 &ndash; 1930) and they had two sons: [[Brice W. Goldsborough]]; and Phillips Lee Goldsborough II.

In 1891 and in 1895, Goldsborough was elected state's attorney for [[Dorchester County, Maryland]]. In 1897, he was elected to the position of [[List of Comptrollers of Maryland|comptroller of the treasury of Maryland]], but was defeated for reelection in 1899 by Dr. [[Joshua W. Hering]].  As of 2016, he is the last Republican to serve as Maryland Comptroller.

He was appointed collector of internal revenue for the district of Maryland in 1902 by President [[Theodore Roosevelt]] and later by President [[William Howard Taft]].

==Governor of Maryland==
Over time, Goldsborough built a large base of support in the state, which encouraged him to run for Governor of Maryland in 1911. He defeated Democratic challenger [[Arthur P. Gorman, Jr.]], becoming only the second Republican governor in state history up to that time.

Goldsborough's tenure as governor saw a great deal of education reform, including the appointment of school boards and teacher certification. It was also during his tenure that the state purchased the Maryland Agricultural College, which is now the [[University of Maryland, College Park]].

[[Image:Governor phillips goldborough of maryland.jpg|left|thumb|Goldsborough]]

==United States Senate==
Goldsborough tried to win the Republican nomination for the [[Classes of United States Senators#Class 1|Class I]] U.S. Senate seat from Maryland in 1916, but was defeated in the Republican primary by [[Joseph I. France]].  He left politics afterwards and resumed his law practice in Cambridge, and also became president of the National Union Bank.

When Republican [[Herbert Hoover]] was elected President of the United States, Goldsborough decided to take this opportunity to again try for the same senate seat in Maryland. He was elected as to the United States Senate in the [[United States Senate elections, 1928|election of 1928]], defeating incumbent [[William Cabell Bruce]].

==Later career and death==
In 1934, he was not a candidate for re-election to the senate, but instead decided to run again for Governor of Maryland. He lost in the Republican primary to [[Harry W. Nice]], who went on to win the election itself.

President [[Franklin Delano Roosevelt]] appointed Goldsborough to the director's board of the Federal Deposit Insurance Corporation in 1935. He served in that position until he died in 1946 in [[Baltimore]], Maryland, and is buried in the old churchyard of [[Christ Episcopal Church and Cemetery (Cambridge, Maryland)|Christ Episcopal Church]] of his hometown of Cambridge.

==References==
{{CongBio|G000262}}
*Essay by Frank F. White, Jr., ''The Governors of Maryland'' 1777&ndash;1970.

{{s-start}}
{{s-off}}
{{succession box | before = [[Robert Patterson Graham]] |title=[[List of Comptrollers of Maryland|Comptroller of Maryland]] | years = 1898&ndash;1900 | after = [[Joshua W. Hering]]}}
{{succession box | before = [[Austin Lane Crothers|Austin L. Crothers]] |title=[[Governor of Maryland]] | years = 1912&ndash;1916 | after = [[Emerson Harrington|Emerson C. Harrington]]}}
{{s-par|us-sen}}
{{U.S. Senator box | before = [[William Cabell Bruce]] | state=Maryland| class=1 | years = 1929–1935 | after = [[George L. P. Radcliffe]] | alongside=[[Millard Tydings]]}}
{{end}}

{{USSenMD}}
{{Governors of Maryland}}
{{USCongRep-start|congresses= 71st–73rd [[United States Congress]]es |state=[[Maryland]]}}
{{USCongRep/MD/71}}
{{USCongRep/MD/72}}
{{USCongRep/MD/73}}
{{USCongRep-end}}

{{Authority control}}

{{DEFAULTSORT:Goldsborough, Phillips Lee}}
[[Category:1865 births]]
[[Category:1946 deaths]]
[[Category:Comptrollers of Maryland]]
[[Category:Governors of Maryland]]
[[Category:People from Cambridge, Maryland]]
[[Category:People from Princess Anne, Maryland]]
[[Category:United States Senators from Maryland]]
[[Category:Republican Party United States Senators]]
[[Category:Maryland Republicans]]
[[Category:American Episcopalians]]
[[Category:Republican Party state governors of the United States]]