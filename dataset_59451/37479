{{Infobox military conflict
| conflict    = Battle of White Plains
| partof      = the [[American Revolutionary War]]
| image       = [[File:Battle of white plains historic site 073105.jpg|300px]]
| caption     = Battle of White Plains Historic Site
| date        = October 28, 1776
| place       = [[White Plains, New York|White Plains]], [[New York (state)|New York]]
| coordinates = {{Coord|41|2|24|N|73|46|43|W|display=title}}
| result      = [[Great Britain|British]] victory
| combatant1  = {{flag|United States|1776}}
| combatant2  = {{flagcountry|Kingdom of Great Britain}} <br>{{flagicon|Hesse}} [[Landgraviate of Hesse-Kassel|Hesse-Kassel]]
| commander1  = {{flagicon|United States|1776}} [[George Washington]]<br>{{flagicon|United States|1776}} [[Alexander McDougall]]<br>{{flagicon|United States|1776}} [[Joseph Spencer]]
| commander2  = {{flagicon|Kingdom of Great Britain}} [[William Howe, 5th Viscount Howe|William Howe]]
| strength1   = McDougall: 1,600<br>Spencer: 1,500<ref>Dawson, p. 269. No exact count is known for the Massachusetts militia.</ref>
| strength2   = 4,000–7,500<ref>British reports do not indicate exactly how many troops were engaged; most historians do not list specific values.  Dawson estimates 7,500 (p. 269), more than one half of Howe's army.  Alden estimates 4,000 (p. 273).</ref>
| casualties1 = McDougall: 28 killed<br>126 wounded<br>16 captured<ref>Dawson, p. 270. Numbers are minimum; casualties are not known for Haslet's regiment or the Massachusetts militia.</ref><br>Spencer: 22 killed<br>24 wounded<br>1 missing<ref name=D270>Dawson, p. 270</ref>
<br>Total: 50 killed<br>150 wounded<br>17 captured or missing
| casualties2 = 47 killed<br>182 wounded<br>4 missing<ref name=D270/>
| campaignbox = {{Campaignbox American Revolutionary War: Northern 1775}}
}}

The '''Battle of White Plains''' was a battle in the [[New York and New Jersey campaign]] of the [[American Revolutionary War]] fought on October 28, 1776, near [[White Plains, New York|White Plains]], [[New York (state)|New York]].  Following the retreat of [[George Washington]]'s [[Continental Army]] northward from [[New York City]], [[Kingdom of Great Britain|British]] General [[William Howe, 5th Viscount Howe|William Howe]] landed troops in [[Westchester County]], intending to cut off Washington's escape route.  Alerted to this move, Washington retreated farther, establishing a position in the village of White Plains but failed to establish firm control over local high ground.  Howe's troops drove Washington's troops from a hill near the village; following this loss, Washington ordered the Americans to retreat farther north.

Later British movements chased Washington across [[New Jersey]] and into [[Pennsylvania]].  Washington then [[Washington's crossing of the Delaware|crossed the Delaware]] and surprised a brigade of [[Hessian (soldiers)|Hessian]] troops in the December 26 [[Battle of Trenton]].

==Background==
{{Main|New York and New Jersey campaign}} 
British [[William Howe, 5th Viscount Howe|General William Howe]], after [[Boston campaign|evacuating Boston]] in March 1776, regrouped in [[City of Halifax|Halifax, Nova Scotia]], and embarked in June on a campaign to gain control of [[New York City]].<ref>Schecter, pp. 85,97</ref>  The campaign began with an unopposed landing on [[Staten Island]] in early July. British troops made another unopposed landing on [[Long Island]] on August 22, south of the areas where General [[George Washington]]'s [[Continental Army]] had organized significant defenses around Brooklyn Heights.<ref>Schecter, pp. 100, 118–127</ref>

After losing the [[Battle of Long Island]] on August 27, General Washington and his army of 9,000 troops escaped on the night of August 29–30 to York Island (as [[Manhattan]] was then called).<ref>[[David McCullough|McCullough]], ''[[1776 (book)|1776]]'', pp. 188–191</ref>  General Howe followed up with a [[Landing at Kip's Bay|landing on Manhattan]] on September 15, but his advance was checked the next day [[Battle of Harlem Heights|at Harlem Heights]].  After an abortive landing at [[Throg's Neck]], he landed troops [[Battle of Pell's Point|with some resistance at Pell's Point]] on October 18 to begin an encircling maneuver that was intended to trap Washington's army between that force, his troops in Manhattan, and the [[Hudson River]], which was dominated by warships of the [[Royal Navy]].<ref>Schecter, pp. 179–230</ref>  Howe established a camp at [[New Rochelle, New York|New Rochelle]], but advance elements of his army were near [[Mamaroneck (town), New York|Mamaroneck]], only {{convert|7|mi|km}} from [[White Plains, New York|White Plains]], where there was a lightly defended Continental Army supply depot.<ref name=S232/>

==Prelude==
On October 20, General Washington sent Colonel [[Rufus Putnam]] out on a reconnaissance mission from his camp at Harlem Heights.  Putnam discovered the general placement of the British troop locations and recognized the danger to the army and its supplies.<ref name=S232>Schecter, p. 232</ref>  When he reported this to Washington that evening, Washington immediately dispatched Putnam with orders to [[William Alexander (American general)|Lord Stirling]], whose troops were furthest north, to immediately march to White Plains.  They arrived at White Plains at 9 am on October 21, and were followed by other units of the army as the day progressed.<ref>Schecter, p. 233</ref>  Washington decided to withdraw most of the army to White Plains,<ref name="Lengel p.161">Lengel, p. 161</ref> leaving a garrison of 1,200 men under [[Nathanael Greene]] to defend [[Fort Washington (New York)|Fort Washington]] on Manhattan.<ref name="Lengel p.161"/>  General Howe's army advanced slowly, with troops from his center and right moving along the road from New Rochelle to White Plains, while a unit of [[Loyalist (American Revolution)|Loyalists]] occupied Mamaroneck.  The latter was attacked that night by a detachment of Lord Stirling's troops under [[John Haslet]], who took more than thirty prisoners as well as supplies, but suffered several killed and 15 wounded.  As a result, Howe moved elements of his right wing to occupy Mamaroneck.<ref>Dawson, pp. 252–253</ref>  On October 22, Howe was reinforced by the landing at New Rochelle of an additional 8,000 troops under the command of [[Wilhelm von Knyphausen]].<ref>Schecter, p. 231</ref>

[[File:Elijahmillerhouse.JPG|thumbnail|right|upright=0.75|The [[Elijah Miller House]], which served as [[George Washington]]'s headquarters in White Plains.]]
Washington established his headquarters at the [[Elijah Miller House]] in North White Plains on October 23,<ref>{{cite web|url=http://parks.westchestergov.com/index.php?option=com_content&task=view&id=2046&Itemid=4465|title=Miller House|publisher=Westchester County Parks Department|accessdate=2011-08-26| archiveurl= https://web.archive.org/web/20110719090919/http://parks.westchestergov.com/index.php?option=com_content&task=view&id=2046&Itemid=4465| archivedate= 19 July 2011 <!--DASHBot-->| deadurl= no}}</ref> and chose a defensive position that he fortified with two lines of [[Trench warfare|entrenchments]].<ref name = "Greene52">Greene, p. 52</ref> The trenches were situated on raised terrain, protected on the right by the [[swamp]]y ground near the [[Bronx River]], with steeper hills further back as a place of retreat. The American defenses were {{convert|3|mi|km}} long. Beyond that, on the right, was Chatterton's Hill, which commanded the plain over which the British would have to advance. The hill was initially occupied by [[Militia_(United_States)#Revolutionary_War_.281775–1783.29|militia]] companies numbering several hundred, probably including John Brooks' Massachusetts militia company.<ref name=D261>Dawson, p. 261</ref>

On October 24 and 25, Howe's army moved from New Rochelle to [[Scarsdale, New York|Scarsdale]], where they established a camp covering the eastern bank of the [[Bronx River]].  This move was apparently made in the hopes of catching [[Charles Lee (general)|Charles Lee]]'s column, which had to alter its route toward White Plains and execute a forced march at night to avoid them.<ref>Dawson, pp. 258–259</ref>  Howe remained at Scarsdale until the morning of October 28, when his forces marched toward White Plains, with British troops on the right under General [[Henry Clinton (American War of Independence)|Henry Clinton]], and primarily [[Hessian (soldiers)|Hessian]] troops on the left under [[Leopold Philip de Heister|General von Heister]].<ref name="Dawson, p. 260">Dawson, p. 260</ref>

==Battle==
[[File:White Plains Battle Plans.jpeg|thumbnail|left|upright=1.5|A 1796 map showing the strategies of the opposing armies.]]

While Washington was inspecting the terrain to determine where it was best to station his troops, messengers alerted him that the British were advancing.<ref name="Lengel p.162">Lengel p.162</ref> Returning to his headquarters, he ordered the [[2nd Connecticut Regiment]] under [[Joseph Spencer]] out to slow the British advance, and sent Haslet and the [[1st Delaware Regiment]], along with [[Alexander McDougall]]'s brigade ([[Rudolphus Ritzema]]'s [[3rd New York Regiment]], [[Charles Webb (American Revolution)|Charles Webb]]'s [[19th Continental Regiment]], [[William Smallwood]]'s [[1st Maryland Regiment]], and the [[1st New York Regiment]] and [[2nd New York Regiment]]s) to reinforce Chatterton Hill.<ref name=D263/>

Spencer's force advanced to a position on the old York road at Hart's corners ([[Hartsdale, New York]]) and there exchanged fire with the Hessians led by Colonel [[Johann Rall]] that were at the head of the British left column.  When Clinton's column threatened their flank, these companies were forced into a retreat across the Bronx River that was initially orderly with pauses to fire from behind stone walls while fire from the troops on Chatterton Hill covered their move, but turned into a rout with the appearance of dragoons.<ref name="Dawson, p. 260"/>  Rall's troops attempted to gain the hill, but were repelled by fire from Haslet's troops and the militia,<ref>Schecter, p. 238</ref> and retreated to a nearby hilltop on the same side of the river.  This concerted defense brought the entire [[British Army during the American War of Independence|British Army]], which was maneuvering as if to attack the entire American line, to a stop.<ref>Dawson, pp. 262-263</ref>

While Howe and his command conferred, the Hessian artillery on the left opened fire on the hilltop position, where they succeeded in driving the militia into a panicked retreat.  The arrival of McDougall and his brigade helped to rally them, and a defensive line was established, with the militia on the right and the Continentals arrayed along the top of the hill.<ref name=D263>Dawson, p. 263</ref>  Howe finally issued orders, and while most of his army waited, a detachment of British and Hessian troops was sent to take the hill.<ref>Dawson, p. 264</ref>

The British attack was organized with Hessian regiments leading the assault.  Rall was to charge the American right, while a Hessian battalion under Colonel [[Carl von Donop]] (consisting of the Linsing, [[Duderstadt|Mingerode]], Lengereck, and Kochler grenadiers, and Donop's own chasseur regiment) was to attack the center. A British column under General [[Alexander Leslie (British Army officer)|Alexander Leslie]] (consisting of the 5th, 28th, 35th, and 49th Foot) was to attack the right.  Donop's force either had difficulty crossing the river, or was reluctant to do so, and elements of the British force were the first to cross the river.  Rall's charge scattered the militia on the American right, leaving the flank of the Maryland and New York regiments exposed as they poured musket fire onto the British attackers, which temporarily halted the British advance.  The exposure of their flank caused them to begin a fighting retreat, which progressively forced the remainder of the American line, which had engaged with the other segments of the British force, to give way and retreat.  Haslet's Delaware regiment, which anchored the American left, provided covering fire while the remaining troops retreated to the north, and were the last to leave the hill.<ref>Dawson, pp. 265-267</ref>  The fighting was intense, and both sides suffered significant casualties before the Continentals made a disciplined retreat.<ref>Schecter, p. 240</ref>

==Casualties==
[[File:WilliamHowe1777ColorMezzotint.jpeg|thumb|right|upright=1.0|[[Mezzotint]] artist rendition of General Howe, by Charles Corbutt, ca. 1777]]
[[File:Battle of White Plains 1926 Issue-2c.jpg|thumb|220px|right|right|<center>''~ Battle of White Plains ~''</center><center><span style="font-size:125%;">150th Anniversary [[Postage stamps and postal history of the United States#Two Cent Red Sesquicentennial Issues of 1926 - 1932|Issue]] of 1926</span><center>]]
[[File:USS White Plains (CVE-66) at San Diego, 8 March 1944.jpg|thumb|right|{{USS|White Plains|CVE-66}}]]

[[John Fortescue (military historian)|John Fortescue]]'s ''History of the British Army'' says that Howe's casualties numbered 214 British and 99 Hessians.<ref name=Boatner1201>Boatner, p. 1201</ref> However, Rodney Atwood points out that Fortescue's figure for the Hessians includes the entire Hessian casualties from 19–28 October and that in fact only 53 of these casualties were incurred at the Battle of White Plains.<ref name=Atwood75>Atwood, p. 75</ref> This revised figure would give a total of 267 British and Hessians killed, wounded or missing at White Plains. Henry Dawson, on the other hand, gives Howe's loss as 47 killed, 182 wounded and 4 missing.<ref name=D270/>
The American loss is uncertain. Theodore Savas and J. David Dameron give a range of 150-500 killed, wounded and captured.<ref name=Savas&Dameron80>Savas and Dameron, p. 80</ref> Samuel Roads numbers the casualties of 47 killed and 70 wounded.<ref name=RoadsVIII53>Roads, Chapter VIII, p. 153</ref> Henry Dawson estimates 50 killed, 150 wounded and 17 missing for McDougall's and Spencer's commands but has no information on the losses in Haslet's regiment.<ref name=D270/>

==Aftermath==
The two generals remained where they were for two days, while Howe reinforced the position on Chatterton Hill, and Washington organized his army for retreat into the hills.  With the arrival of additional Hessian and Waldeck troops under [[Hugh Percy, 1st Duke of Northumberland|Lord Percy]] on October 30, Howe planned to act against the Americans the following day.  However, a heavy rain fell the whole next day,<ref>Schecter, p. 241</ref> and when Howe was finally prepared to act, he awoke to find that Washington had again eluded his grasp.<ref name=S242>Schecter, p. 242</ref>

Washington withdrew his army into the hills to the north on the night of October 31, establishing a camp near [[North Castle, New York|North Castle]].<ref name=S242/>  Howe chose not to follow, instead attempting without success to draw Washington out.<ref>Dawson, pp. 274–276</ref>  On November 5, he turned his army south to finish evicting Continental Army troops from Manhattan, a task he accomplished with the November 16 [[Battle of Fort Washington]].<ref>Schecter, pp. 243–257</ref>

Washington eventually crossed the Hudson River at [[Peekskill, New York|Peekskill]] with most of his army, leaving [[New England]] regiments behind to guard supply stores and important river crossings.<ref>Schecter, p. 245</ref>  Later, British movements chased him across New Jersey and into [[Pennsylvania]], and the British established a chain of outposts across New Jersey. Washington, seeing an opportunity for a victory to boost the nation's morale,  [[Washington's crossing of the Delaware|crossed the Delaware]] and surprised Rall's troops in the December 26 [[Battle of Trenton]].<ref>Schecter, pp. 255–267</ref>

==Legacy==
Each year on or near the anniversary date, the White Plains Historical Society hosts a commemoration of the event at the [[Jacob Purdy House]] in [[White Plains, New York]].<ref>[[#WPHS_Calendar|White Plains Historical Society Event Calendar]]</ref>  Two ships in the [[United States Navy]] were named for the Battle of White Plains. [[USS White Plains (CVE-66)|CVE-66]] was an [[escort carrier]] in [[World War II]]. [[USS White Plains (AFS-4)|AFS-4]] was a [[combat stores ship]] that was decommissioned in 1995.<ref>{{cite web|title=Dictionary of American Naval Fighting Ships &mdash; ''White Plains'' |url=http://www.history.navy.mil/danfs/w7/white_plains-ii.htm |archive-url=https://web.archive.org/web/20040329024348/http://www.history.navy.mil:80/danfs/w7/white_plains-ii.htm |dead-url=yes |archive-date=2004-03-29 |accessdate=2007-08-11 |publisher=Naval History & Heritage Command }}</ref><ref>{{cite news|url=https://www.nytimes.com/1992/08/30/us/after-the-storm-thousands-on-guam-lose-homes-in-typhoon.html?pagewanted=1|publisher=New York Times|date=1992-08-29|title=After the storm; Thousands on Guam lose homes in typhoon|accessdate=2010-02-18}}</ref><ref>{{cite web|title=U.S.S WHITE PLAINS|url=http://www.hullnumber.com/AFS-4|website=HULLNUMBER.COM|publisher=HullNumber.com|accessdate=11 December 2016}}</ref>

According to some historians, the [[Headless Horseman]] depicted in [[Washington Irving]]'s short story "[[The Legend of Sleepy Hollow]]" was inspired by a real-life Hessian soldier who lost his head by cannon fire during this battle.<ref>{{cite web|last1=Smith|first1=Jacqueline|title="Halloween History: The Legend of Sleepy Hollow"|url=http://historydetectives.nyhistory.org/2013/10/halloween-history-the-legend-of-sleepy-hollow/|website=New-York Historical Society Museum & Library: History Detectives|date=October 25, 2013}}</ref>

==See also==
{{Portal|American Revolutionary War}}
*[[List of American Revolutionary War battles]]

==Notes==
{{reflist|colwidth=24em}}

==Bibliography==
{{Wikipedia books|New York and New Jersey campaign}}
{{refbegin}}
*{{cite book|last=Alden|first=John|title=A History of the American Revolution|year=1989|publisher=Da Capo Press|ref=Alden|isbn=978-0-306-80366-6}}
*{{cite book|last=Atwood|first=Rodney|title=The Hessians: Mercenaries from Hessen-Kassel in American Revolution|year=1980|publisher=Cambridge University Press|ref=Atwood|isbn=0-8061-2530-6}}
*{{cite book|url=https://books.google.com/books?id=nl4EAAAAYAAJ&dq=battle%20of%20white%20plains%201776&pg=PA259#v=onepage&q=chatterton&f=false|title=Westchester County, New York in the American Revolution|first=Henry Barton|last=Dawson|year=1886|publisher=self-published|location=Morrisania, New York|ref=Dawson}}
*{{cite book|last=Boatner|first=Mark Mayo|title=Cassell's Biographical Dictionary of the American War of Independence, 1763-1783|year=1966|publisher=Cassell and Company, Ltd.|location=London|ref=Boatner}}
*{{cite book |last= Greene |first= Francis Vinton | title= The Revolutionary War and the Military Policy of the United States |publisher= Charles Scribner's Sons |year= 1911 | url = http://www.questia.com/PM.qst?a=o&d=6248065}}
*{{cite book|last=Lengel|first=Edward|title=General George Washington|location=New York|publisher=Random House Paperbacks|year=2005|isbn=|author6=Edward G. Lengel}}
*{{cite book|last=Roads|first=Samuel, Jr.|authorlink=Samuel Roads, Jr.|title=The History and Traditions of Marblehead|location=Boston|publisher=Osgood|year=1880|isbn=}}
*{{cite book|last1=Savas|first1=Theodore P.|last2=Dameron|first2=J. David|title=A Guide to the Battles of the American Revolution|year=2006|publisher=Savas Beattie LLC|location=New York and El Dorado Hills, CA|ref=Savas and Dameron|isbn=978-1-932714-12-8}}
*{{cite book|last=Schecter|first=Barton|title=The Battle for New York|publisher=Walker|location=New York|year=2002|isbn=0-8027-1374-2|ref=Schecter}}
*{{cite web|url=http://www.whiteplainshistory.org/calendar.html|title=White Plains Historical Society Event Calendar|accessdate=2009-12-17|publisher=White Plains Historical Society|ref=WPHS_Calendar}}
{{refend}}

==External links==
{{Commons category|Battle of White Plains}}
*[https://web.archive.org/web/20050310002828/http://theamericanrevolution.org:80/battles/bat_wpla.asp The Battle of White Plains]
*[http://www.whiteplainshistory.org/ White Plains Historical Society]

{{New York in the American Revolutionary War}}

{{good article}}

[[Category:New York in the American Revolution]]
[[Category:Conflicts in 1776|White Plains]]
[[Category:White Plains, New York]]
[[Category:Battles of the New York Campaign|White Plains, Battle of]]
[[Category:Battles involving the United States|White Plains]]
[[Category:Battles involving Great Britain|White Plains]]
[[Category:Battles involving Hesse-Kassel|White Plains]]
[[Category:1776 in New York]]
[[Category:October 1776 events]]