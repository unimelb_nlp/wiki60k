{{italic title}}
{{primary sources|date=September 2011}}
<!-- should use {{Infobox journal}} -->
{{Infobox Organization
|name                = Internet Archaeology
|image               = Internet Archaeology (logo).png
|caption             = IA logo
|abbreviation        = IA
|formation           = {{start date|1996}}
|status              = Electronic Journal 
|purpose             = To publish articles of a high academic standing which utilise the potential of electronic publication
|location            = [[The King's Manor]], [[York]], [[England]]
|region_served       = International
|leader_title        = Editor
|leader_name         = Judith Winters
|num_staff           = 2
|parent_organization = [[University of York]]
|affiliations        = {{Plainlist|
* [[Archaeology Data Service]]
* [[Council for British Archaeology]]
}}
|website             = {{URL|intarch.ac.uk}}
}}

'''''Internet Archaeology''''' is an international [[scholarly journal]] and one of the first fully [[peer-review]]ed [[electronic journal]]s for [[archaeology]]. It published its first issue in 1996. The journal was part of the eLIb project's electronic journals.<ref name=ariadne>{{cite web |title=Alan Vince Internet Archaeology, Ariadne 3 |url=http://www.ariadne.ac.uk/issue3/intarch/ |publisher=UKOLN|accessdate=5 October 2011}}</ref><ref>M Heyworth, S. Ross and J. Richards Internet archaeology: an international electronic journal for archaeology, 
The Field Archaeologist, Winter 1995, No. 24, pages 12-13.</ref><ref>Mike Heyworth, Seamus Ross, and Julian Richards  'Internet archaeology: an international electronic journal for archaeology' Archaeological Computing Newsletter Number 44: Winter 1995, 20-22.</ref><ref name=ukoln>{{cite web |title=Seamus Ross 1996 INTERNET ARCHAEOLOGY: OVERCOMING THE OBSTACLES AND USING THE OPPORTUNITIES|url=http://www.ukoln.ac.uk/services/papers/bl/rdr6250/ross.html |publisher=UKOLN |accessdate=5 October 2011}}</ref> The journal is produced and hosted at the [[Department of Archaeology at the University of York]], UK and nominally published by the [[Council for British Archaeology]].

''Internet Archaeology'''s first managing Editor was [[Alan Vince|Dr Alan Vince]] (1996-1999). The journal is currently edited by Judith Winters (since 1999). The journal is co-directed by Prof. Julian Richards ([[University of York]]) and Dr Michael Heyworth ([[Council for British Archaeology]]) and supported by an [http://intarch.ac.uk/news/steercom.html Advisory Committee] made up from representatives from the [[Archaeology Data Service]], the archaeological commercial sector, and a range of leading international universities.<ref name=antiquity>{{cite web |title=Mike Heyworth, Julian Richards, Alan Vince and Sandra Garside-Neville  'Internet Archaeology: a quality electronic journal', Antiquity 71 (274), pages 1039–1039.|url=http://www.antiquity.ac.uk/Ant/071/Ant0711039.htm |publisher=Antiquity |accessdate=5 October 2011}}</ref>

Internet Archaeology was established with funding from the [[Jisc]]'s Electronic Libraries ([http://www.ukoln.ac.uk/services/elib/ eLib]) programme and initially explored a subscription model.<ref>J. Richards Internet Archaeology and the myth of free publication. Learned Publishing, Volume 15, Number 3, 1 July 2002, pp. 233-234 url=http://www.ingentaconnect.com/content/alpsp/lp/2002/00000015/00000003/art00013</ref><ref>•J Winters 2003 'Towards Sustainable Electronic Publishing for Archaeology' in M. Doerr and A Sarris (eds) The Digital Heritage of Archaeology CAA 2002. Proceedings of the 30th Conference, Heraklion, Crete. Archive of Monuments and Publications, Hellenic Ministry of Culture, 415-418.</ref> In September 2014, the journal's editor Judith Winters announced that the publication had adopted an [[open access]] approach and that all past and future content would be freely available.<ref name="OA">{{cite web|url=http://intarch.ac.uk/open_access.html|title=Open Access|work=Internet Archaeology|accessdate=30 September 2014}}</ref>

Journal content makes use of the potential of internet publication to present archaeological research (excavation reports, methodology, analyses, applications of information technology) in ways that could not be achieved in print, such as full colour images, photographs, searchable data sets, visualisations/virtual reality models and interactive mapping. The journal's content is archived by the [[Archaeology Data Service]] (ADS).

==References==
{{reflist|2}}

==External links==
*[http://intarch.ac.uk/ Internet Archaeology homepage]

[[Category:Archaeology journals]]
[[Category:Archaeological organisations]]
[[Category:Archaeology of the United Kingdom]]
[[Category:History journals]]
[[Category:Open access journals]]
[[Category:Online-only journals]]