{{for|the 1797 battle|Battle of Cape St Vincent (1797)}}
{{good article}}
{{Use dmy dates|date=February 2017}}
{{Infobox military conflict
| conflict    = Battle of Cape St. Vincent
| partof      = the [[American Revolutionary War]]
| image       = [[File:Holman, Cape St Vincent.jpg|300px|alt=An oil painting depicting a sea battle. The sky has dark clouds with patches of blue, and the sea is grey. Warships are visible in the distance, some of which are exchanging cannon fire.  A British warship occupies the center foreground, obscuring an explosion behind it.]]
| caption     = Moonlight ''Battle off Cape St Vincent, 16 January 1780'' by [[Francis Holman]], painted 1780, shows the ''Santo Domingo'' exploding, with [[George Brydges Rodney, 1st Baron Rodney|Rodney]]'s flagship {{HMS|Sandwich|1759|2}} in the foreground.
| date        = 16 January 1780
| place       = Off [[Cape St. Vincent]], [[Portugal]]
| result      = British victory<ref name="Duffy1992">{{cite book|author=Michael Duffy|title=Parameters of British Naval Power, 1650–1850|url=https://books.google.com/books?id=ysC9rOCxGhgC&pg=PA105|accessdate=12 April 2013|year=1992|publisher=University of Exeter Press|isbn=978-0-85989-385-5|page=105}}</ref>
| combatant1  = {{flagcountry|Kingdom of Great Britain}}
| combatant2  = {{flagicon|Spain|1748}} [[Enlightenment Spain|Spain]]
| commander1  = Sir [[George Brydges Rodney, 1st Baron Rodney|George Rodney]]
| commander2  = Don [[Juan de Lángara]]{{POW}}
| strength1   = 18 ships of the line<br />6 frigates<ref>Beatson, p. 232, as modified by Syrett, pp. 241, 306, 311</ref>
| strength2   = 9 ships of the line<br />2 frigates<ref name="Ulloa33">Ulloa and Pérez-Mallaína Bueno, p. 33</ref>
| casualties1 = 32 killed<br />102 wounded<ref name=B234/>
| casualties2 = 1 ship destroyed<br />4 ships captured<ref name="Ulloa33"/><ref name=B234>Beatson, p. 234</ref><br />2,500 captured, killed or wounded<br />fate of 2 ships disputed (see [[#Aftermath|Aftermath]])<ref name=B233/>
|campaignbox={{Campaignbox American War of Independence: European Waters}}
}}
{{Location map many|Iberia |width=300
 |float=right
 |caption=Map of the [[Iberian Peninsula]]
<!-- first label -->
 |label=Battle
 |mark=<!--dot-->Green pog.svg
 |lat=36.818056
 |long=-8.563611
 |position=top

<!-- second label -->
 |label2=Cadiz
 |mark2=<!--dot-->Red pog.svg
 |lat2=36.528674
 |long2=-6.289043
 |position2=top

<!-- third label -->
 |label3=Gibraltar
 |mark3=<!--dot-->Red pog.svg
 |lat3=36.143
 |long3=-5.353
 |position3=right

<!-- fourth label -->
 |label4=Minorca
 |mark4=<!--dot-->Red pog.svg
 |lat4=39.99
 |long4=4.04
 |position4=left
}}

The '''Battle of Cape St. Vincent''' was a naval battle took place off the southern coast of [[Portugal]] on 16 January 1780 during the [[American Revolutionary War]]. A [[Kingdom of Great Britain|British]] fleet under [[George Brydges Rodney, 1st Baron Rodney|Admiral Sir George Rodney]] defeated a [[Spain|Spanish]] squadron under Don [[Juan de Lángara]]. The battle is sometimes referred to as the '''Moonlight Battle''' because it was unusual for naval battles in the [[Age of Sail]] to take place at night. It was also the first major naval victory for the British over their European enemies in the war and proved the value of [[copper sheathing|copper-sheathing]] the hulls of warships.

Admiral Rodney was escorting a fleet of supply ships to [[Great Siege of Gibraltar|relieve the Spanish siege of Gibraltar]] with a fleet of about twenty [[ships of the line]] when he encountered Lángara's squadron south of [[Cape St. Vincent]]. When Lángara saw the size of the British fleet, he attempted to make for the safety of [[Cádiz]], but the copper-sheathed British ships chased his fleet down. In a running battle that lasted from mid-afternoon until after midnight, the British captured four Spanish ships, including Lángara's flagship. Two other ships were also captured, but their final disposition is unclear; some Spanish sources indicate they were retaken by their Spanish crews, while Rodney's report indicates the ships were grounded and destroyed.

After the battle Rodney successfully resupplied [[Gibraltar]] and [[Minorca]] before continuing on to the [[West Indies]] station. Lángara was released on [[Parole#Prisoners of war|parole]], and was promoted to lieutenant general by [[Carlos III of Spain|King Carlos III]].

==Background==
{{main|Great Siege of Gibraltar}}

One of [[Spain]]'s principal goals upon [[Spain in the American Revolutionary War|its entry into the American War of Independence]] in 1779 was the recovery of [[Gibraltar]], which had been [[Capture of Gibraltar|lost to England in 1704]].<ref>Chartrand, pp. 12, 30</ref>  The Spanish planned to retake Gibraltar by blockading and starving out its garrison, which included troops from Britain and the [[Electorate of Hanover]].<ref>Chartrand, pp. 23, 30–31, 37</ref>  The siege formally began in June 1779, with the Spanish establishing a land blockade around the [[Rock of Gibraltar]].<ref>Chartrand, p. 30</ref>  The matching naval blockade was comparatively weak, however, and the British discovered that small fast ships could evade the blockaders, while slower and larger supply ships generally could not.  By late 1779, however, supplies in Gibraltar had become seriously depleted, and its commander, General [[George Augustus Eliott, 1st Baron Heathfield|George Eliott]], appealed to London for relief.<ref>Chartrand, p. 37</ref>

A supply convoy was organized, and in late December 1779 a large fleet sailed from England under the command of Admiral [[George Brydges Rodney, 1st Baron Rodney|Sir George Brydges Rodney]].  Although Rodney's ultimate orders were to command the [[West Indies]] fleet, he had secret instructions to first resupply Gibraltar and [[Minorca]].  On 4 January 1780 the fleet divided, with ships headed for the West Indies sailing westward.  This left Rodney in command of 19 [[ships of the line]], which were to accompany the supply ships to Gibraltar.<ref>Syrett, pp. 234, 237</ref>

On 8 January 1780 ships from Rodney's fleet spotted a group of sails.  Giving chase with their faster [[copper sheathing|copper-clad]] ships, the British determined these to be a Spanish supply convoy that was protected by a single ship of the line and several frigates.  [[Action of 8 January 1780|The entire convoy was captured]], with the lone ship of the line, ''Guipuzcoana'', [[striking the colours|striking her colours]] after a perfunctory exchange of fire. ''Guipuzcoana'' was staffed with a small prize crew and renamed {{HMS|Prince William|1780|6}}, in honour of [[William IV of the United Kingdom|Prince William]], the third son of the King, who was serving as [[midshipman]] in the fleet.  Rodney then detached {{HMS|America|1777|6}} and the frigate {{HMS|Pearl|1762|6}} to escort most of the captured ships back to England; ''Prince William'' was added to his fleet, as were some of the supply ships that carried items likely to be of use to the Gibraltar garrison.<ref>Syrett, pp. 238, 306</ref>  On 12 January {{HMS|Dublin|1757|6}}, which had lost part of her topmast on 3 January, suffered additional damage and raised a distress flag.  Assisted by {{HMS|Shrewsbury|1758|6}}, she limped into [[Lisbon]] on 16 January.<ref name=Syrett311>Syrett, p. 311</ref>

The Spanish had learnt of the British relief effort.  From the blockading squadron a fleet comprising 11 ships of the line under Admiral [[Juan de Lángara]] was dispatched to intercept Rodney's convoy, and the Atlantic fleet of Admiral [[Luis de Córdova y Córdova|Luis de Córdova]] at [[Cadiz]] was also alerted to try to catch him. Córdova learnt of the strength of Rodney's fleet, and returned to Cadiz rather than giving chase.  On 16 January the fleets of Lángara and Rodney spotted each other around 1:00 pm south of [[Cape St. Vincent]], the southwestern point of [[Portugal]] and the [[Iberian Peninsula]].<ref>Chartrand, p. 38</ref>  The weather was hazy, with heavy swells and occasional squalls.<ref name="Syrett239"/>

==Battle==
[[File:Juan de Lángara y Huarte.jpg|thumb|right|upright|Don [[Juan de Lángara]], c. 1779 portrait by unknown artist|alt=A three quarter length portrait of Admiral Langara, painted when he was younger.  He stands before a dark curtain partially pulled aside, revealing a bookcase.  His left hand rests on his sword.  His coat is a dark color with gold braiding on the lapels, and a red waistcoat is visible underneath.]]

Rodney was ill, and spent the entire action in his bunk.  His [[flag captain]], [[Walter Young (Royal Navy officer)|Walter Young]], urged Rodney to give orders to engage when the Spanish fleet was first spotted, but Rodney only gave orders to form a line abreast.  Lángara started to establish a [[line of battle]], but when he realised the size of Rodney's fleet, he gave orders to make all sail for Cadiz.  Around 2:00 pm, when Rodney felt certain that the ships seen were not the vanguard of a larger fleet, he issued commands for a general chase.<ref>Syrett, pp. 238–239</ref>  Rodney's instructions to his fleet were to chase at their best speed, and engage the Spanish ships from the rear as they came upon them.  They were also instructed to sail to the [[lee shore|lee side]] to interfere with Spanish attempts to gain the safety of a harbour,<ref name="Mahan449">Mahan, p. 449</ref> a tactic that also prevented the Spanish ships from opening their lowest gun ports.<ref name="Syrett239">Syrett, p. 239</ref>  Because of their copper-sheathed hulls (which reduced marine growths and drag), the ships of the [[Royal Navy]] were faster and soon gained on the Spanish.<ref>Willis, p. 34</ref>

The chase lasted for about two hours, and the battle finally began around 4:00 pm. [[Spanish ship Santo Domingo|''Santo Domingo'']], trailing in the Spanish fleet, received broadsides from {{HMS|Edgar|1779|6}}, {{HMS|Marlborough|1767|6}}, and {{HMS|Ajax|1767|6}} before blowing up around 4:40, with the loss of all but one of her crew.<ref name="Mahan449"/><ref>Syrett, pp. 240, 313<!--report of one saved--></ref>  ''Marlborough'' and ''Ajax'' then passed [[Spanish ship Princessa (1750)|''Princessa'']] to engage other Spanish ships. ''Princessa'' was eventually engaged in an hour-long battle with {{HMS|Bedford|1775|6}} before striking her colours at about 5:30.<ref name="Syrett240">Syrett, p. 240</ref>  By 6:00 pm it was getting dark, and there was a discussion aboard {{HMS|Sandwich|1759|6}}, Rodney's flagship, about whether to continue the pursuit.  Although Captain Young is credited in some accounts with pushing Rodney to do so, Dr. [[Sir Gilbert Blane, 1st Baronet|Gilbert Blane]], the fleet physician, reported it as a decision of the council.<ref>Mahan, p. 450</ref>

The chase continued into the dark, squally night, leading to it later being known as the "Moonlight Battle", since it was uncommon at the time for naval battles to continue after sunset.<ref>Stewart, p. 131</ref>  At 7:30 pm, {{HMS|Defence|1763|6}} came upon Lángara's flagship {{Ship|Spanish ship|Fenix|1749|2}}, engaging her in a battle lasting over an hour.  She was broadsided in passing by {{HMS|Montagu|1779|6}} and {{HMS|Prince George|1772|6}}, and Lángara was wounded in the battle.  ''Fenix'' finally surrendered to {{HMS|Bienfaisant|1758|6}}, which arrived late in the battle and shot away her mainmast.<ref name=Syrett240/>  ''Fenix''{{'s}} takeover was complicated by an outbreak of [[smallpox]] aboard ''Bienfaisant''.  Captain [[John MacBride (Royal Navy officer)|John MacBride]], rather than sending over a possibly infected prize crew, apprised Lángara of the situation and put him and his crew on parole.<ref name="DNB428">{{cite book | chapter=MacBride, John (d. 1800) | title=Dictionary of National Biography|year=1893|page=428}}</ref>

[[File:George Bridges Rodney, 1st Baron Rodney by Sir Joshua Reynolds.jpg|thumb|left|upright|Admiral [[George Brydges Rodney, 1st Baron Rodney|Sir George Rodney]], portrait by Sir [[Joshua Reynolds]] (date unknown)|alt=A three-quarter-length portrait of Admiral Rodney in relative youth.  He stands before a mostly dark background; his right hand rests on what looks like a large tree branch, behind which the sea is visible.  He wears a dark coat with gold embroidery over a white waistcoat.]]
At 9:15 ''Montagu'' engaged [[Spanish ship Diligente (1756)|''Diligente'']], which struck after her maintopmast was shot away.  Around 11:00 pm ''San Eugenio'' surrendered after having all of her masts shot away by {{HMS|Cumberland|1774|6}}, but the difficult seas made it impossible to board a prize crew until morning. That duel was passed by {{HMS|Culloden|1776|6}} and ''Prince George'', which engaged ''San Julián'' and compelled her to surrender around 1:00 am.<ref name=Syrett240/>  The last ship to surrender was [[Spanish ship Monarca (1756)|''Monarca'']].  She nearly escaped, shooting away {{HMS|Alcide|1779|6}}'s topmast, but was engaged in a running battle with the frigate HMS ''Apollo''.  ''Apollo'' managed to keep up the unequal engagement until Rodney's flagship ''Sandwich'' came upon the scene around 2:00 am.  ''Sandwich'' fired a broadside, unaware that ''Monarca'' had already hauled down her flag.<ref name=Syrett241>Syrett, p. 241</ref>

The British took six ships.  Four Spanish ships of the line and the fleet's two frigates escaped, although sources are unclear if two of the Spanish ships were even present with the fleet at the time of the battle.  Lángara's report states that ''San Justo'' and ''San Genaro'' were not in his line of battle (although they are listed in Spanish records as part of his fleet).<ref>Duro, pp. 259, 263</ref>  Rodney's report states that ''San Justo'' escaped but was damaged in battle, and that ''San Genaro'' escaped without damage.<ref name=B233/>  According to one account two of Lángara's ships (unspecified which two) were despatched to investigate other unidentified sails sometime before the action.<ref name="Ulloa33"/>

==Aftermath==

With the arrival of daylight, it was clear that the British fleet and their prize ships were dangerously close to a [[lee shore]] with an onshore breeze.<ref name=Syrett241>Syrett, p. 241</ref>  One of the prizes, ''San Julián'', was recorded by Rodney as too badly damaged to save, and was driven ashore.  The fate of another prize, ''San Eugenio'', is unclear.  Some sources report that she too was grounded, but others report that she was retaken by her crew and managed to reach Cadiz.<ref name=B233>Beatson, p. 233</ref><ref name=Mahan449/>  A Spanish history claims that the prize crews of both ships appealed to their Spanish captives for help escaping the lee shore.  The Spanish captains retook control of their ships, imprisoned the British crews, and sailed to Cadiz.<ref>Lafuente, p. 440</ref>

[[File:Moonlight battle Aftermath.jpg|right|thumb|''Rodney's Fleet Taking in Prizes After the Moonlight Battle, 16 January 1780'',  by [[Dominic Serres]] (date unknown).  The painting shows the British fleet with the captured Spanish squadron in the middle centre.|alt=The painting focuses on the morning after the battle when British ships surrounded the fleeing Spanish fleet. The scene is bathed in a golden glow of early morning light. The British flagship is in the centre, indicated by the flag flying from the mainmast. She is at the head of a line of British ships, shown in the act of capturing the Spanish squadron in the middle centre. Land can be seen in the distance on the left.]]
The British reported their casualties in the battle as 32 killed and 102 wounded.<ref name=B234/>  The supply convoy sailed into Gibraltar on 19 January, driving the smaller blockading fleet to retreat to the safety of [[Algeciras]].  Rodney arrived several days later, after first stopping in [[Tangier]].  The wounded Spanish prisoners, who included Admiral Lángara, were offloaded there, and the British garrison was heartened by the arrival of the supplies and the presence of Prince William Henry.<ref name=Syrett241/>  After also resupplying Minorca, Rodney sailed for the West Indies in February, detaching part of the fleet for service in the [[English Channel|Channel]].  This homebound fleet intercepted a French fleet destined for the [[East Indies]], [[Action of 24 February 1780|capturing one warship and three supply ships]].<ref name="Mahan451"/>  Gibraltar was resupplied twice more before the siege was lifted at the [[Treaty of Paris (1783)|end of the war]] in 1783.<ref>Chartrand, p. 31</ref>

Admiral Lángara and other Spanish officers were eventually released on parole, the admiral receiving a promotion to lieutenant general.<ref>Syrett, p. 366</ref>  He continued his distinguished career, becoming Spanish marine minister in the [[French Revolutionary Wars]].<ref>Harbron, p. 85</ref>

Admiral Rodney was lauded for his victory, the first major victory of the war by the Royal Navy over its European opponents.  He distinguished himself for the remainder of the war, notably winning the 1782 [[Battle of the Saintes]] in which he captured the French Admiral [[François Joseph Paul de Grasse|Comte de Grasse]].  He was, however, criticised by Captain Young, who portrayed him as weak and indecisive in the battle with Lángara.<ref name="Syrett239"/><ref>Mahan, p. 535</ref>  (He was also rebuked by the admiralty for leaving a ship of the line at Gibraltar, against his express orders.)<ref>Mahan, p. 452</ref> Rodney's observations on the benefits of copper sheathing in the victory were influential in British Admiralty decisions to deploy the technology more widely.<ref name="Mahan451">Mahan, p. 451</ref><ref name=Syrett244>Syrett, p. 244</ref>

==Order of battle==

None of the listed sources give an accurate accounting of the ships in Rodney's fleet at the time of the action.  [[Robert Beatson]] lists the composition of the fleet at its departure from England, and notes which ships separated to go to the West Indies, as well as those detached to return the prizes captured on 8 January to England.<ref>Beatson, pp. 232–233</ref>  He does not list two ships (''Dublin'' and ''Shrewsbury'', identified in despatches reprinted by Syrett) that were separated from the fleet on 13 January.<ref name=Syrett311/>  Furthermore, HMS ''Prince William'' is sometimes misunderstood to have been part of the prize escort back to England, but she was present at Gibraltar after the action.<ref>See Rodney's despatch (Syrett, p. 305) describing her commissioning, and later references to her in orders at Gibraltar (e.g. Syrett, p. 341).</ref>  Beatson also fails to list a number of frigates, including {{HMS|Apollo|1774|6}}, which played a key role in the capture of ''Monarca''.<ref>Syrett, pp. 241, 274</ref>
{| class="wikitable" width=90%
|-valign="top"
!colspan="7" bgcolor="white"|British fleet
|- valign="top"
! width=20%; align= center rowspan=2 | <small> Ship </small>
! width=10%; align= center rowspan=2 | <small> Rate </small>
! width=5%; align= center rowspan=2 | <small> Guns </small>
! width=45%; align= center rowspan=2 | <small> Commander </small>
! width=15%; align= center colspan=3 | <small>Casualties</small>
|-valign="top"
! width=5%; align= center | <small> Killed </small>
! width=5%; align= center | <small> Wounded </small>
! width=5%; align= center | <small> Total</small>
|- valign="top"
| align= left | {{HMS|Sandwich|1759|2}}
| align= center | [[Second rate]]
| align= center | 90
| align= left | Admiral of the White [[George Brydges Rodney, 1st Baron Rodney|Sir George Rodney]] (fleet commander)<br />[[Walter Young (Royal Navy officer)|Walter Young]]
| align= right |0
| align= right |0
| align= right |0
|- valign="top"
| align= left | {{HMS|Royal George|1756|2}}
| align= center | [[First rate]]
| align= center | 100
| align= left | Rear Admiral of the Blue [[Robert Digby (Royal Navy officer)|Robert Digby]]<br />John Bourmaster
| align= right |0
| align= right |0
| align= right |0
|- valign="top"
| align= left | {{HMS|Prince George|1772|2}}
| align= center | [[Second rate]]
| align= center | 90
| align= left | Rear Admiral of the Blue Sir [[John Lockhart-Ross]]<br />[[Philip Patton]]
| align= right |1
| align= right |3
| align= right |4
|- valign="top"
| align= left | {{HMS|Ajax|1765|2}}
| align= center | [[Third rate]]
| align= center | 74
| align= left | Captain Samuel Uvedale
| align= right |0
| align= right |6
| align= right |6
|- valign="top"
| align= left | {{HMS|Alcide|1779|2}}
| align= center | [[Third rate]]
| align= center | 74
| align= left | Captain John Brisbane
| align= right |0
| align= right |0
| align= right |0
|- valign="top"
| align= left | {{HMS|Bedford|1775|2}}
| align= center | [[Third rate]]
| align= center | 74
| align= left | [[Edmund Affleck]]
| align= right |3
| align= right |9
| align= right |12
|- valign="top"
| align= left | {{HMS|Culloden|1776|2}}
| align= center | [[Third rate]]
| align= center | 74
| align= left | George Balfour
| align= right |0
| align= right |0
| align= right |0
|- valign="top"
| align= left | {{HMS|Cumberland|1774|2}}
| align= center | [[Third rate]]
| align= center | 74
| align= left | Joseph Peyton
| align= right |0
| align= right |1
| align= right |1
|- valign="top"
| align= left | {{HMS|Defence|1763|2}}
| align= center | [[Third rate]]
| align= center | 74
| align= left | James Cranston
| align= right |10
| align= right |12
| align= right |22
|- valign="top"
| align= left | {{HMS|Edgar|1779|2}}
| align= center | [[Third rate]]
| align= center | 74
| align= left | [[John Elliot (Royal Navy officer)|John Elliot]]
| align= right |6
| align= right |20
| align= right |26
|- valign="top"
| align= left | {{HMS|Invincible|1765|2}}
| align= center | [[Third rate]]
| align= center | 74
| align= left | S. Cornish
| align= right |3
| align= right |4
| align= right |7
|- valign="top"
| align= left | {{HMS|Marlborough|1767|2}}
| align= center | [[Third rate]]
| align= center | 74
| align= left | Taylor Penny<ref>Syrett, p. 314</ref>
| align= right |0
| align= right |0
| align= right |0
|- valign="top"
| align= left | {{HMS|Monarch|1765|2}}
| align= center | [[Third rate]]
| align= center | 74
| align= left | [[Adam Duncan, 1st Viscount Duncan|Adam Duncan]]
| align= right |3
| align= right |26
| align= right |29
|- valign="top"
| align= left | {{HMS|Montagu|1779|2}}
| align= center | [[Third rate]]
| align= center | 74
| align= left | John Houlton
| align= right |0
| align= right |0
| align= right |0
|- valign="top"
| align= left | {{HMS|Resolution|1770|2}}
| align= center | [[Third rate]]
| align= center | 74
| align= left | [[Sir Chaloner Ogle, 1st Baronet|Sir Chaloner Ogle]]
| align= right |0
| align= right |0
| align= right |0
|- valign="top"
| align= left | {{HMS|Terrible|1762|2}}
| align= center | [[Third rate]]
| align= center | 74
| align= left | John Leigh Douglas
| align= right |6
| align= right |12
| align= right |18
|- valign="top"
| align= left | {{HMS|Bienfaisant|1758|2}}
| align= center | [[Third rate]]
| align= center | 64
| align= left | [[John MacBride (Royal Navy officer)|John MacBride]]
| align= right |0
| align= right |0
| align= right |0
|- valign="top"
| align= left | {{HMS|Prince William|1780|2}}
| align= center | [[Third rate]]
| align= center | 64
| align= left | Unknown
| align= right |
| align= right |
| align= right |
|- valign="top"
| align= left | {{HMS|Apollo|1774|2}}
| align= center | [[Frigate]]
| align= center | 32
| align= left | [[Philemon Pownoll]]
| align= right |
| align= right |
| align= right |
|- valign="top"
| align= left | {{HMS|Convert|1778|2}}
| align= center | [[Frigate]]
| align= center | 32
| align= left | [[Henry Harvey]]
| align= right |
| align= right |
| align= right |
|- valign="top"
| align= left | {{HMS|Triton|1771|2}}
| align= center | [[Frigate]]
| align= center | 28
| align= left | [[Skeffington Lutwidge]]
| align= right |
| align= right |
| align= right |
|- valign="top"
| align= left | {{HMS|Pegasus|1779|2}}
| align= center | [[Frigate]]
| align= center | 24
| align= left | [[John Bazely]]
| align= right |
| align= right |
| align= right |
|- valign="top"
| align= left | {{HMS|Porcupine|1777|2}}
| align= center | [[Frigate]]
| align= center | 24
| align= left | [[Lord Hugh Seymour|Hugh Seymour-Conway]]
| align= right |
| align= right |
| align= right |
|- valign="top"
| align= left | {{HMS|Hyaena|1778|2}}
| align= center | [[Frigate]]
| align= center | 24
| align= left | [[Edward Thompson (Royal Navy officer)|Edward Thompson]]
| align= right |
| align= right |
| align= right |
|-
|colspan=7 align=center |<small>Unless otherwise cited, table information is from Beatson, pp.&nbsp;232, 234, and Syrett, p.&nbsp;274.  Full captain names are from Syrett, p.&nbsp;259.<br>Blank casualty report fields mean there was no report listed for that ship.</small>
|}

There are some discrepancies between the English and Spanish sources listing the Spanish fleet, principally in the number of guns most of the vessels are claimed to mount.  The table below lists the Spanish records describing Lángara's fleet.  Beatson lists all of the Spanish ships of the line at 70 guns, except ''Fenix'', which he lists at 80 guns.  One frigate, ''Santa Rosalia'', is listed by Beatson at 28 guns.<ref name=B233/>  The identify of the second Spanish frigate is different in the two listings. Beatson records her as ''Santa Gertrudie'', 26 guns, with captain Don Annibal Cassoni, while Duro's listing describes her as ''Santa Cecilia'', 34, captain Don Domingo Grandallana.  Both frigates, whatever their identity, escaped the battle.<ref name=B233/><ref name=Duro263>Duro, p. 263</ref>
{| class="wikitable" width=100%
|-valign="top"
!colspan="5" bgcolor="white"|Spanish fleet
|- valign="top"
! width=15%; align= center | <small> Ship </small>
! width=10%; align= center | <small> Rate </small>
! width=5%; align= center | <small> Guns </small>
! width=35%; align= center | <small> Commander </small>
! width=35%; align= center | <small>Notes</small>
|- valign="top"
| align= left | {{Ship|Spanish ship|Fenix|1749|2}}
| align= center | [[Third rate]]
| align= center | 80
| align= left | Don [[Juan de Lángara]] (fleet commander)<br />Don Francisco Melgarejo
| align= left | Captured, 700 men.
|- valign="top"
| align= left | {{Ship|Spanish ship|Princesa|1750|2}}
| align= center | [[Third rate]]
| align= center | 74
| align= left | Don Manuel León
| align= left | Captured, 600 men.
|- valign="top"
| align= left | {{Ship|Spanish ship|Diligente|1756|2}}
| align= center | [[Third rate]]
| align= center | 74
| align= left | Don Antonio Albornoz
| align= left | Captured, 600 men.
|- valign="top"
| align= left | {{Ship|Spanish ship|Monarca|1756|2}}
| align= center | [[Third rate]]
| align= center | 74
| align= left | Don Antonio Oyarvide
| align= left | Captured, 600 men.
|- valign="top"
| align= left | ''Santo Domingo''
| align= center | [[Third rate]]
| align= center | 74
| align= left | Don Ignacio Mendizábal
| align= left | Blown up.
|- valign="top"
| align= left | {{Ship|Spanish ship|San Agustín|1768|2}}
| align= center | [[Third rate]]
| align= center | 74
| align= left | Don Vicente Doz
| align= left | Escaped.
|- valign="top"
| align= left | ''San Lorenzo''
| align= center | [[Third rate]]
| align= center | 74
| align= left | Don Juan Araoz
| align= left | Escaped with damage.
|- valign="top"
| align= left | ''San Julián''
| align= center | [[Third rate]]
| align= center | 64
| align= left | Marqués de Medina
| align= left | Captured (600 men), either grounded or retaken.
|- valign="top"
| align= left | ''San Eugenio''
| align= center | [[Third rate]]
| align= center | 74
| align= left | Don Antonio Damonte
| align= left | Captured (600 men), either grounded or retaken.
|- valign="top"
| align= left | ''San Jenaro''
| align= center | [[Third rate]]
| align= center | 74
| align= left | Don Félix de Tejada
| align= left | Not listed in Lángara's line of battle.  Listed by Beatson as escaping.
|- valign="top"
| align= left | ''San Justo''
| align= center | [[Third rate]]
| align= center | 74
| align= left | Don Francisco Urreiztieta
| align= left | Not listed in Lángara's line of battle.  Listed by Beatson as escaping with damage.
|- valign="top"
| align= left | ''Santa Cecilia''
| align= center | [[Frigate]]
| align= center | 34
| align= left | Don Domingo Grandallana
| align= left | Identified as ''Santa Gertrudis'' in Beatson.  Escaped.
|- valign="top"
| align= left | ''Santa Rosalia''
| align= center | [[Frigate]]
| align= center | 34
| align= left | Don Antonio Ortega
| align= left | Escaped.
|-
|colspan=8 align=center |<small>Unless otherwise cited, table information is from Duro, pp.&nbsp;259, 263, and Beatson, p.&nbsp;233.</small>
|}

== See also ==
* [[Battle of Cape St. Vincent (disambiguation)]], for several other naval battles fought off Cape St Vincent, the best known in [[Battle of Cape St Vincent (1797)|1797]]

== References ==
{{Reflist|30em}}

==Sources==
* {{cite book|last=Beatson|first=Robert|year=1804|title=Naval and Military Memoirs of Great Britain, from 1727 to 1783, Volume 6|url=https://books.google.com/books?id=btEHAAAAIAAJ&ie=ISO-8859-1&pg=PA233#v=onepage&f=false|location=London|publisher=Longman, Hurst, Rees and Orme|oclc=4643956}}
* {{cite book |last=Chartrand |first=René |others=Courcelle, Patrice |title=Gibraltar 1779–1783: The Great Siege |year=2006 |url=http://www.ospreypublishing.com/title_detail.php/title=S9770 |edition=1st|publisher=Osprey Publishing |location=Oxford |isbn=978-1-84176-977-6 |oclc=255272192}}
* {{cite book|last=Duro|first=Cesáreo Fernández|language=Spanish|title=Armada Española Desde la Unión de los Reinos de Castilla y de León, Volume 7|location=Madrid|publisher=Establecimiento Tipográfico|year=1901|oclc=4413652|url=https://books.google.com/books?id=G1y3VAtRHTIC&ie=ISO-8859-1&pg=PA259#v=onepage&q&f=false}} Reprints Lángara's report.
* {{cite book|last=Harbron|first=John|title=Trafalgar and the Spanish Navy|publisher=Conway Maritime Press|year=1988|location=London|isbn=978-0-85177-477-0|oclc=19096677}}
* {{cite book|last=Lafuente|first=Modesto|url=https://books.google.com/books?id=kBUJAAAAQAAJ&ie=ISO-8859-1&pg=PA440#v=onepage&q&f=false|title=Historia General de España, Volume 20|publisher=Establecimiento Tipográfico de Mellado|year=1858|location=Madrid|oclc=611596|language=Spanish}}
* {{cite book|last=Mahan|first=Arthur T|url=https://books.google.com/books?id=hpI_AAAAYAAJ&ie=ISO-8859-1&pg=PA449#v=onepage&q=langara&f=false|title=Major Operations of the Royal Navy, 1762–1783|publisher=Little, Brown|year=1898|location=Boston|oclc=46778589}}
* {{cite book|last=Stewart|first=William|title=Admirals of the World: a Biographical Dictionary, 1500 to the Present|publisher=McFarland|year=2009|location=Jefferson, NC|isbn=978-0-7864-3809-9|oclc=426390753}}
* {{cite book|last=Syrett|first=David|year=2007|title=The Rodney Papers: Selections From the Correspondence of Admiral Lord Rodney|publisher=Ashgate Publishing|isbn=978-0-7546-6007-1|oclc=506119281|location=Burlington, VT}} Reprints numerous British documents concerning Rodney's entire expedition.
* {{cite book|last=de Ulloa|first=Antonio|author2=Pérez Mallaína-Bueno |author3=Pablo Emilio |year=1995|language=Spanish|title=La campaña de las terceras|location=Salamanca|publisher=Universidad de Sevilla|isbn=978-84-472-0241-6}}
* {{cite book|last=Willis|first=Sam|year=2008|title=Fighting at Sea in the Eighteenth Century: the Art of Sailing Warfare|publisher=Boydell Press|isbn=978-1-84383-367-3|location=Woodbridge, Suffolk|oclc=176925283}}

==Further reading==
* {{cite book|last=de Castro|first=Adolfo|language=Spanish|title=Historia de Cádiz y su Provincia|url=https://books.google.com/books?id=yoCPa8wHa50C&ie=ISO-8859-1&pg=PA516#v=onepage&f=false|publisher=Imprenta de la Revista Médica|year=1858|location=Cádiz|oclc=162549293}}
* Sapherson, C. A. and Lenton, J. R. (1986) ''Navy Lists from the Age of Sail; Vol. 2: 1776–1783''. Leeds: Raider Games
* Spinney, David (1969) ''Rodney''. London: Allen & Unwin ISBN 0-04-920022-4
* Trew, Peter. ''Rodney and The Breaking of the Line'' Leo Cooper Ltd (2005) ISBN 978-1-84415-143-1

{{Coord|36|49|5|N|8|33|49|W|type:event_region:PT|display=title}}

{{DEFAULTSORT:Cape Saint Vincent, Battle of 1780}}
[[Category:Conflicts in 1780]]
[[Category:Naval battles of the American Revolutionary War involving Spain]]
[[Category:Naval battles of the American Revolutionary War]]
[[Category:1780 in the British Empire]]
[[Category:1780 in Portugal]]
[[Category:January 1780 events]]