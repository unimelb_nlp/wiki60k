{{Other uses|Largs Bay (disambiguation){{!}}Largs Bay}}
{{refimprove|date=July 2016}}
{{Use dmy dates|date=October 2012}}
{{Infobox Australian place| type = suburb
| name     = Largs Bay
| city     = Adelaide
| state    = sa
| image    = Largs bay jetty.jpg
| caption  = The Largs Bay jetty
| lga      = [[City of Port Adelaide Enfield]]
| postcode = 5016
| est      = 
| pop      = <!-- ???? <small>''2006 Census''</small> <ref>{{Census 2006 AUS|id=SSC42561|name=Largs Bay (State Suburb)|accessdate=23 June 2008|quick=on}}</ref> -->
| area     = 
|latd=34.821
|longd=138.494
| stategov = [[Electoral district of Lee|Lee]]
| fedgov   = [[Division of Port Adelaide|Port Adelaide]]
| near-nw  = ''[[Gulf St Vincent]]''
| near-n   = [[Largs North, South Australia|Largs North]]
| near-ne  = [[Port Adelaide, South Australia|Port Adelaide]]
| near-w   = ''[[Gulf St Vincent]]''
| near-e   = [[Port Adelaide, South Australia|Port Adelaide]]
| near-sw  = ''[[Gulf St Vincent]]''
| near-s   = [[Semaphore, South Australia|Semaphore]]<br/>[[Peterhead, South Australia|Peterhead]]
| near-se  = [[Port Adelaide, South Australia|Port Adelaide]]
| dist1    = 15
| location1= [[Adelaide|CBD]]
|footnotes=Adjoining suburbs<ref name=PLB>{{cite web|title=Search result for "Largs Bay (suburb)" (Record no. SA0038753) with the following layers selected - "Suburbs and Localities" and " Place names (gazetteer)"  |url=http://maps.sa.gov.au/plb/# |work=Property Location Browser|publisher=Government of South Australia |accessdate=1 July 2016}}</ref>
}}
'''Largs Bay''' is a [[suburb]] in the Australian state of [[South Australia]] located on the [[Lefevre Peninsula]] in the west of [[Adelaide]] about {{convert|16|km}} northwest of the [[Adelaide city centre]]. 

==Description==
Largs bay is bounded to the north by Walcot and Warwick Street, to the south by Wills, Hargrave and Union Streets and in the west and east by [[Gulf St Vincent]] and the centre of the [[Port River]] respectively.  It is adjacent to the suburbs of [[Largs North, South Australia|Largs North]], [[Peterhead, South Australia|Peterhead]], [[Port Adelaide, South Australia|Port Adelaide]] and [[Semaphore, South Australia|Semaphore]].  It is essentially a residential suburb, with a minor harbourside presence on the eastern side of the suburb.  It is located within the local government area of the [[City of Port Adelaide Enfield]].

==History==
Largs Bay originally started as a [[Subdivision (land)|private sub-division]] in Section 1069 in the [[Cadastral divisions of South Australia|cadastral unit]] of the [[Hundred of Port Adelaide]].  The name was “formally submitted by the City of Port Adelaide at a council meeting held on 10 May 1945” and was formally adopted in 1951 by the Nomenclature Committee.  In August 2009, its eastern boundary was extended to the centre of the [[Port River]].<ref name=PLB/>

==Facilities==
The suburb is served by a primary school, [[Largs Bay Primary School]], and the local high school is Ocean View College Gedville Campus, in nearby [[Taperoo, South Australia|Taperoo]]. There is a hospice on Everard Street and a museum on Fletcher Road. Largs Reserve overlooking Woolnough Road is the main outdoor recreational reserve in the suburb, being the site of a lawn bowling club. The [[Largs Pier Hotel]] on the Esplanade was used as a historical landmark in earlier times by sailors. A sailing club and a jetty are present on Largs Bay Beach.

The eastern side of the suburb, by the [[Port River|Port]] riverside is the location of three shipping berths, which are used by [[Caltex]], [[BP]] and [[Mobil]] vessels.

==Heritage listings==

Largs Bay has a number of heritage-listed sites, including:

* Esplanade: [[Largs Bay Jetty]] <ref>{{cite web | url=http://maps.sa.gov.au/heritagesearch/HeritageItem.aspx?p_heritageno=1751 | title=Largs Bay Jetty | publisher=Department of Environment, Water and Natural Resources | work=South Australian Heritage Register | accessdate=15 August 2016}}</ref>
* 138-139 Esplanade: [[Largs Bay College]] <ref>{{cite web | url=http://maps.sa.gov.au/heritagesearch/HeritageItem.aspx?p_heritageno=1749 | title=Dwelling - Two Storey Duplex (sometime Largs Bay College) | publisher=Department of Environment, Water and Natural Resources | work=South Australian Heritage Register | accessdate=15 August 2016}}</ref>
* 198 Esplanade: [[Largs Pier Hotel]] <ref>{{cite web | url=http://maps.sa.gov.au/heritagesearch/HeritageItem.aspx?p_heritageno=1750 | title=Largs Pier Hotel | publisher=Department of Environment, Water and Natural Resources | work=South Australian Heritage Register | accessdate=15 August 2016}}</ref>
* 11-15 Jetty Road: [[Largs Bay Land and Investment Company's Shops]] <ref>{{cite web | url=http://maps.sa.gov.au/heritagesearch/HeritageItem.aspx?p_heritageno=1752 | title=Largs Bay Land and Investment Company's Shops | publisher=Department of Environment, Water and Natural Resources | work=South Australian Heritage Register | accessdate=15 August 2016}}</ref>
* 212 Lady Gowrie Drive: [[Montrose, Largs Bay|Montrose]] <ref>{{cite web | url=http://maps.sa.gov.au/heritagesearch/HeritageItem.aspx?p_heritageno=1753 | title=Dwelling ('Montrose') and Fence | publisher=Department of Environment, Water and Natural Resources | work=South Australian Heritage Register | accessdate=15 August 2016}}</ref>

==Transport==
The 157 and 333 buses service Military Road, while the 150 services Fletcher Road. The suburb is also serviced by [[Largs railway station, Adelaide|Largs]] and [[Largs North railway station|Largs North]] railway stations on the [[Outer Harbor railway line]].

==Politics==

Largs Bay is part of the state [[electoral district of Lee]]. Since 2014 Lee's member of South Australia's House of Assembly has been [[Stephen Mullighan]]. Stephen is a member of the Australian Labor Party. His electoral office is at Shop 4, 173-177 Tapleys Hill Road, Seaton.

Largs Bay is part of the federal [[division of Port Adelaide]]. Since 1997 Port Adelaide's member of the Commonwealth of Australia's House of Representatives has been [[Mark Butler]]. Mark is a member of the Australian Labor Party. His electoral office is at 15 Semaphore Road, Semaphore.

==References==
{{reflist}}

{{City of Port Adelaide Enfield suburbs}}

[[Category:Suburbs of Adelaide]]
[[Category:Lefevre Peninsula]]