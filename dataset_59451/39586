{{Infobox college athletics
| name = Georgia Tech Yellow Jackets
| logo = Georgia Tech Outline Interlocking logo.svg <!-- Primary logo is the interlocking "GT", per http://www.licensing.gatech.edu/visual/art-sheet -->
| logo_width = 200px
| university = Georgia Institute of Technology
| association = NCAA
| conference = [[Atlantic Coast Conference]]
| division = Division I
| director = Todd Stansbury
| city = [[Atlanta]]
| state = [[Georgia (U.S. state)|Georgia]]
| teams = 17
| stadium = [[Bobby Dodd Stadium at Historic Grant Field]]
| basketballarena = [[Hank McCamish Pavilion]]
| baseballfield = [[Russ Chandler Stadium]]  
| mascot = [[Buzz (mascot)|Buzz]] & [[Ramblin' Wreck]]
| nickname = Yellow Jackets, Ramblin' Wreck
| fightsong = "[[Ramblin' Wreck from Georgia Tech]]" and "[[Up with the White and Gold]]"
| pageurl = http://ramblinwreck.com/
}}

The '''Georgia Tech Yellow Jackets''' is the name used for all of the intercollegiate athletic teams that play for the [[Georgia Institute of Technology]] ('''Georgia Tech'''), located in [[Atlanta|Atlanta, Georgia]]. The teams have also been nicknamed the [[Ramblin' Wreck]], Engineers, Blacksmiths and Golden Tornado. There are eight men's and seven women's teams that compete in the [[National Collegiate Athletic Association]] (NCAA) [[NCAA Division I|Division I]] athletics and the [[NCAA Division I Football Bowl Subdivision|Football Bowl Subdivision]]. Georgia Tech is a member of the Coastal Division in the [[Atlantic Coast Conference]].

The official [[school colors]] for Georgia Tech are [[old gold]] and [[white]]. Navy blue is often used as a secondary color and for [[alternate jersey]]s while black has been used on rare occasion. The traditional [[sports rivalry|rival]] in all sports is in-state [[University of Georgia]]. This rivalry is often referred to as [[Clean, Old-Fashioned Hate]]. There are also rivalries with out-of-state [[Auburn University|Auburn]] and [[Clemson – Georgia Tech rivalry|official conference rival]] [[Clemson University|Clemson]].

The athletic department is run by the [[Georgia Tech Athletic Association]], a private organization located on campus. The department dedicates about $53 million per year to its sports teams and facilities. Since April 1, 2013, the [[athletic director]] of Georgia Tech has been [[Mike Bobinski]], who replaced [[Dan Radakovich]] after the latter left for the same position at Clemson.<ref>{{cite press release|url=http://www.gatech.edu/newsroom/release.html?nid=184501|title=Georgia Tech names Mike Bobinski as new Director of Athletics|publisher=Georgia Institute of Technology |date=2013-01-18|accessdate=2013-07-02}}</ref> Most athletic teams have on-campus facilities for competition, including [[Bobby Dodd Stadium]] at Historic Grant Field for [[american football|football]], the [[McCamish Pavilion]] at Cremins' Court for men's and women's [[basketball]], and [[Russ Chandler Stadium]] for [[baseball]].

Georgia Tech was a founding member of the [[Southern Intercollegiate Athletic Association]] in 1895. Georgia Tech was one of the 14 schools that split to found the [[Southern Conference]] in 1921. Thirteen schools including Georgia Tech split in 1932 to form the [[Southeastern Conference]].<ref>{{cite web|url=http://www.soconsports.com/ViewArticle.dbml?DB_OEM_ID=4000&KEY=&ATCLID=177772|title=History of the Southern Conference|accessdate=2007-11-25}}</ref> The Yellow Jackets left the SEC in 1964 and remained independent until becoming a founding member of the [[Metro Conference|Metro-6 Conference]] in 1975.<ref name="Highlights of Georgia Tech History">{{cite web|url=http://www.irp.gatech.edu/apps/factbook/?page=18 |title=Highlights of Georgia Tech History |publisher=Georgia Institute of Technology |year=2007 |accessdate=2008-04-19 |deadurl=yes |archiveurl=https://web.archive.org/web/20080507151251/http://www.irp.gatech.edu/apps/factbook/?page=18 |archivedate=May 7, 2008 }}</ref> Georgia Tech moved to the [[Atlantic Coast Conference|ACC]] in 1978 and began competition within the ACC in 1979.<ref name="Highlights of Georgia Tech History"/>

==Teams==
The Georgia Institute of Technology sponsors teams in nine men's and eight women's [[NCAA]] sanctioned sports:<ref>http://www.ramblinwreck.com/#00</ref>
{{col-start}}{{col-2}}
'''Men's intercollegiate sports'''
* [[College Baseball|Baseball]]
* [[College Basketball|Basketball]]
* [[Cross country running|Cross country]]
* [[College football|Football]]
* [[Golf]]
* [[Swimming (sport)|Swimming]] and [[diving]]
* [[Tennis]]
* [[Track and field]] ([[Track and field#Indoor|indoor]] and [[Track and field#Outdoor|outdoor]])
{{col-2}}
'''Women's intercollegiate sports'''
* [[College Basketball|Basketball]]
* [[Cross country running|Cross country]]
* [[College Softball|Softball]]
* [[Swimming (sport)|Swimming]] and [[diving]]
* [[Tennis]]
* [[Track and field]] ([[Track and field#Indoor|indoor]] and [[Track and field#Outdoor|outdoor]])
* [[Volleyball]]
{{col-end}}

==Football==
[[Image:First GT Football Team 1893.jpg|thumb|right|300px|The first Georgia Tech football team]]
[[Image:Bobby Dodd Stadium Georgia Tech.jpg|thumb|right|300px|[[Bobby Dodd Stadium]] at Historic Grant Field]]
:{{Main|Georgia Tech Yellow Jackets football}}

The football team is traditionally the most popular at the Institute. The games are played at [[Bobby Dodd Stadium at Historic Grant Field]] or simply ''The Flats'', which is the oldest on-campus stadium in Division I FBS football. The stadium was expanded in recent years, increasing the maximum capacity to 55,000.

Georgia Tech plays an Atlantic Coast Conference Coastal Division schedule in addition to yearly cross divisional games against Clemson<ref>{{cite news|first=Neil|last=Paine |url=http://nique.net/issues/2005-10-28/sports/4|title=Clemson rivalry adds to homecoming fun|work=[[The Technique]]|date=2005-10-28|accessdate=2007-05-16 |archiveurl=https://web.archive.org/web/20070929133500/http://nique.net/issues/2005-10-28/sports/4 |archivedate = 2007-09-29}}</ref> and two other Atlantic Division teams. In addition, the team has a yearly out-of-conference meeting with Georgia at the end of the season known as [[Clean, Old-Fashioned Hate]].

The football team is in the top 20 winningest Division I-A programs and was the first team to win all four of the historical big four bowls – the Rose (1929), Orange (1940), Sugar (1944), and Cotton (1955). Georgia Tech has won four national titles in the years 1917 going 9–0 under [[John Heisman]] outscoring opponents 419–17,<ref>{{cite news|hdl=1853/26046|title=Golden Tornado Wins Great Victory|work=[[The Technique]]|date=1917-12-04}}</ref> 1928 going 10–0 under [[William Alexander (football coach)|William Alexander]] outscoring opponents 221–47, 1952 going 12–0 under [[Bobby Dodd]] outscoring opponents 325–59, and 1990 going 11–0–1 under [[Bobby Ross]] outscoring opponents 379–186.<ref name="ballhist">{{cite news|first=Michael|last=Clarke |url=http://nique.net/issues/2005-09-16/sports/5|title=Football program builds on strong history|work=[[The Technique]]|date=2005-09-16|accessdate=2007-05-16 |archiveurl=https://web.archive.org/web/20070929133414/http://www.nique.net/issues/2005-09-16/sports/5 |archivedate = 2007-09-29}}</ref>

The Yellow Jackets have won a total of 15 conference titles. They won 5 SIAA titles in 1916, 1917, 1918, 1920, and 1921; 3 Southern Conference titles in 1922, 1927, and 1928; 5 SEC titles in 1939, 1943, 1944, 1951, 1952, and the 1990, 1998 and 2009 ACC championships along with the 2006 ACC Coastal Division Championship. Paul Johnson and Georgia Tech won the 2009 ACC Championship in Tampa Florida. The team has played in 35 bowl games, posting a record of 22–13. The resulting win percentage of 0.629 is currently the second-highest among teams with over 20 bowl appearances.<ref name="ballhist"/>

The most recent head coach is former [[Dallas Cowboys]] head coach [[Chan Gailey]]. In five seasons, Chan Gailey posted a 37–27 record, 5 straight bowl berths, and a 2–3 record in bowl games.{{Update after|2010|11}} Chan's 5 straight bowl appearances have the Jackets at 10 in a row overall, an active streak tied for 5th longest in the nation.<ref>{{cite news|first=Clark|last=Nelson |url=http://nique.net/issues/2004-09-03/sports/7|title=Beyond the White and Gold: College football has tradition unlike any other|work=[[The Technique]]|date=2004-09-03|accessdate=2007-05-17 |archiveurl = https://web.archive.org/web/20070911040039/http://www.nique.net/issues/2004-09-03/sports/7 |archivedate = 2007-09-11}}</ref>{{Update after|2010|11}} Chan Gailey was fired on Monday, November 26, 2007. A nationwide search for a new head coach then began, which was ended when highly regarded [[Navy Midshipmen football|Navy]] coach [[Paul Johnson (American football coach)|Paul Johnson]] was hired on December 9, 2007.<ref>{{cite news|url=http://ramblinwreck.cstv.com/sports/m-footbl/spec-rel/112607aag.html|title=Gailey Relieved Of Duties As Georgia Tech Head Coach|work=RamblinWreck.com|publisher=Georgia Tech Athletic Association|date=2007-11-26|accessdate=2007-11-26}}</ref>

Three Jacket players were named to the ACC 50th Anniversary Team in 2002: [[Marco Coleman]], OLB; Joe Hamilton, QB; and Ken Swilling, DB. Some notable and more recent Georgia Tech football players are three-time All-American [[Calvin Johnson (American football)|Calvin Johnson]], [[Keith Brooking]], [[Joe Burns (NFL running back)|Joe Burns]], [[Marco Coleman]], [[Joe Hamilton (football player)|Joe Hamilton]], [[Tony Hollings]], [[Dorsey Levens]], and [[Arizona Cardinals]] head coach [[Ken Whisenhunt]]. Three [[Atlantic Coast Conference football honors#Player of the Year|Atlantic Coast Conference Players of the Year]] hailed from Georgia Tech. They were Joe Hamilton (1999), Calvin Johnson (2006), and [[Jonathan Dwyer]] (2008).
{{See also|List of Georgia Tech Yellow Jackets Starting Quarterbacks|List of Georgia Institute of Technology alumni#American football}}

==Men's basketball==
{{Main|Georgia Tech Yellow Jackets men's basketball}}

The Georgia Tech men's basketball team plays its home games in the new [[McCamish Pavilion]] The program saw its most sustained success under the tenure of [[Bobby Cremins]]. Bobby Cremins led his team to the first ACC tournament victory in school history in 1985, and in 1990 he took Georgia Tech to the school's first [[Final Four]] appearance ever. Cremins retired from Georgia Tech in 2000 with the school's best coaching win percentage.

The current head coach of Georgia Tech is [[Brian Gregory]]. Up until March 2011, [[Paul Hewitt]] was the head coach Georgia Tech. Helping revitalize a stagnant program in 2004, he led Georgia Tech to a [[Cinderella (sports)|Cinderella season]] as the school earned its second berth in a NCAA national title game in any sport. That team won the [[National Invitation Tournament|Pre-Season NIT]], ended Duke's 41 game home winning streak, and finished its season losing by 9 points in the national title game to [[Connecticut Huskies|Connecticut]]. Hewitt was fired on March 12, 2011 after having three losing seasons over the previous four years.<ref name=hewitt>[http://ramblinwreck.cstv.com/sports/m-baskbl/spec-rel/031211aab.html "Hewitt Relieved Of Coaching Duties"] ''RamblinWreck.com'' (Georgia Tech Athletic Association). 2011-03-12. Retrieved 2007-03-14.</ref>

Some notable and more recent Georgia Tech basketball players are [[Iman Shumpert]], [[Derrick Favors]], [[Anthony Morrow]], [[Javaris Crittenton]], [[Thaddeus Young]], [[Stephon Marbury]], [[Matt Harpring]], [[Dennis Scott (basketball)|Dennis Scott]], [[Kenny Anderson (basketball)|Kenny Anderson]], [[Travis Best]], [[Chris Bosh]], [[Jarrett Jack]], [[Mark Price]], and [[John Salley]].

==Women's basketball==
{{main|Georgia Tech Yellow Jackets women's basketball}}
Like the men's team, the [[women's basketball]] team plays its home games in [[McCamish Pavilion]]. The current women's coach is [[MaChelle Joseph]].

==Baseball==
{{Main|Georgia Tech Yellow Jackets baseball}}
[[File:Russchancock.jpg|thumb|right|300px|[[Russ Chandler Stadium]]]]
Baseball is a very successful sport at Georgia Tech. The baseball team makes its home at [[Russ Chandler Stadium]] and is one of the premier baseball teams in the [[NCAA]]. Georgia Tech baseball is notable for its high-scoring offenses and stout defenses.

The team's success is guided by head coach [[Danny Hall (baseball)|Danny Hall]]. Danny Hall has coached Tech for 13 seasons and has posted 579 wins over that span. He has led Georgia Tech to 12 years of NCAA regional play and its only three [[College World Series]] appearances in 1994, 2002, and 2006.

The baseball team, under Hall, has become an annual contender for the ACC regular season and tournament titles winning each four and three times respectively.

Some notable and more recent Georgia Tech baseball players are [[Kevin Brown (right-handed pitcher)|Kevin Brown]], [[Nomar Garciaparra]], [[Matt Murton]], [[Kevin Cameron (baseball)|Kevin Cameron]], [[Matt Wieters]], [[Eric Patterson]], [[Brandon Boggs]], [[Jay Payton]], [[Mark Teixeira]], and [[Jason Varitek]].<ref>{{cite news|first=Carroll|last=Rogers |url=http://www.ajc.com/sports/content/sports/braves/stories/2007/08/04/mlbinsider_0803.html|title=Teixeira's glow shines on Jackets too|work=[[Atlanta Journal-Constitution]]|date=2007-08-05 |accessdate=2007-08-09 }}{{dead link|date=September 2016|bot=medic}}{{cbignore|bot=medic}}</ref> Jason Varitek's number 33 is one of two numbers retired, Coach Jim Luck's number 44 is the other.

==Softball==
{{expand section|date=January 2012}}
[[File:Shirley Clements Mewborn Field.JPG|thumb|Shirley Clements Mewborn Field]]
{{See also|Glenn Field (softball)}}
Georgia Tech fields a softball team under coach Shelly Hoerner.<ref>{{cite web|url=http://www.ramblinwreck.com/sports/w-softbl/mtt/shelly_hoerner_850198.html|title=Softball|publisher=[[Georgia Tech Athletic Association]]|accessdate=2012-01-14}}</ref> In 2011, the team won their third straight ACC Regular Season title. Also in 2011, Sharon Perkins was named the ACC Coach of the Year; this is her third consecutive year winning that award, the first ACC coach win it in three consecutive years.<ref>{{cite news|url=http://www.ramblinwreck.com/ot/2011-year-in-review.html|title=2011 Year in Review|publisher=[[Georgia Tech Athletic Association]]|accessdate=2012-01-14}}</ref><ref>{{cite web|url=http://www.ramblinwreck.com/sports/w-softbl/mtt/perkins_sharon00.html|title=Sharon Perkins|publisher=[[Georgia Tech Athletic Association]]|accessdate=2012-01-14}}</ref> In 2009, the team moved from [[Glenn Field]] to Shirley Clements Mewborn Field.

==Golf==
Georgia Tech Yellow Jackets men's golf team is one of the most consistent Yellow Jacket teams. They have won 17 conference championships:<ref name=guide>{{cite web |url=http://grfx.cstv.com/photos/schools/geot/sports/m-golf/auto_pdf/2012-13/misc_non_event/1213infoguide.pdf |title=Georgia Tech 2013 Golf |accessdate=June 14, 2013}}</ref>
*[[Southeastern Conference]] (1): 1949
*[[Atlantic Coast Conference]] (16): 1985, 1991–94, 1999, 2001–02, 2006–07, 2009–12, 2014–15

They have produced three [[NCAA Division I Men's Golf Championships|NCAA individual champions]]: [[Watts Gunn]] in 1927, [[Charlie Yates]] in 1934, and [[Troy Matteson]] in 2002.<ref name=guide/> Their best team finish is second place in 1993, 2000, 2002, and 2005.

The men's golf team made the NCAA Championship round ten straight years (1998–2007) under [[Bruce Heppler]], head coach since 1996. In 2005, the program was rated by ''Golf Magazine'' as the #1 collegiate golf program in the country.

Some notable Georgia Tech golfers are [[David Duval]], [[Stewart Cink]], [[Troy Matteson]], [[Larry Mize]], [[Bryce Molder]], [[Cameron Tringale]] and [[Matt Kuchar]]. The most famous alumnus of the golf program however remains legendary amateur [[Bobby Jones (golfer)|Bobby Jones]], winner of the [[Grand Slam (golf)|Grand Slam]] in 1930 and the founder of [[Augusta National Golf Club]] and the [[Masters Tournament]], a [[men's major golf championships|major championship]] hosted annually by Augusta National.

==Tennis==<!-- This section is linked from [[Georgia Tech Yellow Jackets women's tennis]] -->
{{See also|2006-07 Georgia Tech Yellow Jackets women's tennis team}}
Georgia Tech has both men's and women's [[tennis]] teams. The teams play their home matches at the Ken Byers Tennis Complex, which opened in 2013. The facility houses six indoor courts, six outdoor competition courts, and four outdoor practice courts.

The women's tennis team became the first [[NCAA]] National Champion in Georgia Tech team sports history by winning 4–2 over [[UCLA]] in Athens, Georgia on May 22, 2007.<ref name="wtennis">{{cite news|url=http://www.cstv.com/sports/w-tennis/stories/052207aax.html|title=Georgia Tech Wins NCAA Women's Tennis Title |work=RamblinWreck.com|publisher=Georgia Tech Athletic Association|date=2007-05-22|accessdate=2007-05-23}}</ref><ref>{{cite news|url=http://sports.espn.go.com/ncaa/news/story?id=2879724|title=Georgia Tech captures first NCAA women's tennis title|work=ESPNU|publisher=ESPN.com|date=2007-05-23|accessdate=2007-05-23}}</ref> The 2007 [[NCAA Women's Tennis Championship|National Championship]] team was coached by [[Bryan Shelton]]. As a player in his freshman season (1985), Shelton won Georgia Tech's first individual ACC title. <ref>{{cite news|url=http://grfx.cstv.com/photos/schools/geot/sports/m-tennis/auto_pdf/2015-16/misc_non_event/MTEN-infoguide16.pdf|title=Georgia Tech Men's Tennis 2015–2016 Media Guide|work=ramblinwreck.com|publisher=Georgia Tech Athletic Association|date=|accessdate=2017-03-03}}</ref> The women's tennis team is currently coached by Rodney Harmon, and assisted by Christy Lynch.

The men's team has won six conference titles since 1918 (one in the [[Southern Intercollegiate Athletic Association|SIAA]], two in the [[Southern Conference|SoConn]], and three in the [[Southeastern Conference|SEC]]). They have made 15 NCAA Tournament appearances. The men's tennis team is currently coached by former Georgia Tech player [[Kenny Thorne]], and assisted by Derek Schwandt.<ref>{{cite news|url=http://ramblinwreck.cstv.com/sports/m-tennis/mtt/geot-m-tennis-mtt.html|title=Georgia Tech Men's Tennis Roster|work=ramblinwreck.com|publisher=Georgia Tech Athletic Association|date=|accessdate=2007-05-24}}</ref> In 2011, Thorne was named ITA National Coach of the Year.

==Volleyball==
Georgia Tech Women's Volleyball is one of the newer additions to Georgia Tech's athletic department, having only been started in the past twenty years. Despite the program's youth, it has been a dominant force in the ACC.{{citation needed|date=December 2011}} O'Keefe Gymnasium has served as the home of the Georgia Tech volleyball team since 1995.<ref>{{cite web|title=O'Keefe Gymnasium|url=http://www.ramblinwreck.com/genrel/032102aah.html|publisher=ramblinwreck.com|accessdate= 2015-05-02}}</ref>

Georgia Tech volleyball has become a powerful, perennial ACC title contender and NCAA tournament qualifier. Tech has been in the NCAA tournament three times in recent years and has set several school records for victories in a season, consecutive ACC victories, and consecutive victories.

==Other sports==

Georgia Tech also fields men and women's [[track & field]],<ref name="spotlight">{{cite news|first=Kyle|last=Thomason|url=http://nique.net/issues/2004-08-27/sports/4|title=Beyond the White and Gold: Olympic spotlights quickly dimmed after Games|work=[[The Technique]]|date=2004-08-27|accessdate=2007-05-17 |archiveurl=https://web.archive.org/web/20070929132936/http://nique.net/issues/2004-08-27/sports/4 |archivedate = 2007-09-29}}</ref> men and women's [[swimming (sport)|swimming]] & [[diving]],<ref name="spotlight"/> men and women's [[Cross country running|cross country]], and assorted club sports.

Georgia Tech's [[Angelo Taylor]] won gold medals in 400 m hurdles at the [[2000 Summer Olympics|2000]] and [[2008 Summer Olympics]].

The non-NCAA sanctioned club sports include but are not limited to [[crew]], [[cricket]], [[cycling]], [[equestrianism|equestrian]], [[fencing]], [[field hockey]], [[gymnastics]], [[ice hockey]], [[kayaking]], [[lacrosse]], [[paintball]], [[roller hockey]], [[sport rowing|rowing]], [[rugby football|rugby]], [[sailing]], [[skydiving]], [[soccer]], [[Swimming (sport)|swimming]], [[triathlon]], [[ultimate (sport)|ultimate]], [[water polo]], and [[wrestling]].

==Traditions==
{{Refimprove section|date=January 2010}}
{{Main|Georgia Tech traditions}}

===Mascots===
{{Main|Buzz (mascot)|Ramblin' Wreck}}
[[File:Georgia Tech mascot Buzz.jpg|thumb|right|Buzz, the mascot]] Costumed in [[plush]] to look like a [[yellow jacket]], the official [[mascot]] of Georgia Tech is '''[[Buzz (mascot)|Buzz]]'''. Buzz enters the football games at the sound of swarming yellow jackets and proceeds to do a flip on the fifty-yard line GT logo. He then bull rushes the goal post and has been known to knock it out of alignment before football games. Buzz is also notorious for [[crowd surfing]] and general light-hearted trickery amongst Tech and rival fans.

The '''[[Rambling Wreck|Ramblin' Wreck]]''' was the first official mascot of Georgia Tech. It is a 1930 [[Ford Model A (1927-1931)|Ford Model A]] Sports Coupe. The Wreck has led the football team onto the field every home game since 1961. The Wreck features a gold and white paint job, two gold flags emblazoned with the words "To Hell With Georgia" and "Give 'Em Hell Tech", and a white soft top. The Wreck is maintained by the '''Ramblin' Reck Club''', a selective student leadership organization on campus.<ref>{{cite news|url=http://traditions.gatech.edu/ramblinreck.html|title=Georgia Tech Traditions: The Ramblin' Reck|work=gatech.edu|publisher=Georgia Tech|date=|accessdate=2017-03-03}}</ref>

===Spirit organizations===
[[File:GT Football Score.jpg|thumb|right|Tech cheerleaders waving flags after a touchdown.]]
The Ramblin' Reck Club is charged with upholding all school traditions and creating new traditions such as the SWARM. The SWARM is a 900-member spirit group seated along the north end zone or on the court at basketball games. This is the group that typically features [[body painting]], organized [[chants]], and general [[fanaticism]].

The marching band that performs at halftime and after big plays during the football season is clad in all white and sits next to SWARM at football games providing a dichotomy of white and gold in the North End Zone. The band is also the primary student organization on campus that upholds the tradition of RAT caps, wherein band freshman wear the traditional yellow cap at all band events.

===Fight songs and chants===
The band plays the fight songs ''[[Ramblin' Wreck from Georgia Tech]]'' and ''[[Georgia Tech traditions#Up With the White and Gold|Up With the White and Gold]]'' after every football score and between every basketball period. At the end of a rendition of either fight song, there is a series of drum beats followed by the cheer "Go Jackets" three times (each time followed by a second cheer of "bust their ass"), then a different drum beat and the cheer "Fight, Win, Drink, Get Naked!" The official cheer only includes "Fight, Win" but most present other than the band and cheerleaders will yell the extended version.

It is also tradition for the band to play the [[Here Comes the King|"When You Say Budweiser"]] after the third quarter of football and during the second-to-last official timeout of every basketball game. During the [[Here Comes the King#Popular culture|"Budweiser Song"]], all of the fans in the stadium alternate bending their knees and standing up straight. Other notable band songs are [[Michael Jackson]]'s ''[[Thriller (song)|Thriller]]'' for half-time at the [[Alexander Memorial Coliseum|Thrillerdome]], [[Ludacris]]' ''Move Bitch'' for large gains in football. Another popular chant is called the Good Word and it begins with asking, "What's the Good Word?" The response from all Tech faithful is, "To Hell With Georgia." The same question is asked three times and then the followup is asked, "How 'bout them dogs?" And everyone yells, "Piss on 'em."

==Championships==
===NCAA team championships===
Georgia Tech has won 1 NCAA team national championship.<ref>http://fs.ncaa.org/Docs/stats/champs_records_book/Overall.pdf</ref>

*'''Women's (1)'''
**[[NCAA Division I Women's Tennis Championship#Teams titles|Tennis]] (1):  2007
*see also:
**[[Atlantic Coast Conference#NCAA team championships|ACC NCAA team championships]]
**[[List of NCAA schools with the most NCAA Division I championships#NCAA Division I Team Championships|List of NCAA schools with the most NCAA Division I championships]]

===Other team championships===
Below are 4 national team titles that were not bestowed by the NCAA:

*Men's:
**Football: [[1917 Georgia Tech Yellow Jackets football team|1917]], [[1928 Georgia Tech Yellow Jackets football team|1928]], [[1952 Georgia Tech Yellow Jackets football team|1952]], [[1990 Georgia Tech Yellow Jackets football team|1990]]<ref name="fbschamps">{{cite news|url=http://fs.ncaa.org/Docs/stats/football_records/DI/2009/2009FBS.pdf|title=Football Bowl Subdivision Records|work=NCAA|publisher=NCAA.org|year=2009|accessdate=2011-03-06}}</ref>

==Alumni==
{{main|List of Georgia Institute of Technology athletes}}
Despite their highly technical backgrounds, Tech graduates are no strangers to athletics; approximately 150 Tech students have gone into the [[National Football League|NFL]], with many others going into the [[National Basketball Association|NBA]] or [[Major League Baseball|MLB]]. Well-known [[American football]] athletes include former students [[Calvin Johnson (American football)|Calvin Johnson]], [[Daryl Smith]], and [[Keith Brooking]], former Tech head football coaches [[Pepper Rodgers]] and [[Bill Fulcher]], and all-time greats such as [[Joe Hamilton (American football)|Joe Hamilton]], [[Pat Swilling]], [[Billy Shaw]],  [[Joe Guyon]], and [[Demaryius Thomas]].

Tech's recent entrants into the NBA include [[Iman Shumpert]], [[Derrick Favors]], [[Javaris Crittenton]], [[Thaddeus Young]], [[Jarrett Jack]], [[Luke Schenscher]], [[Stephon Marbury]], and [[Chris Bosh]]. Award-winning baseball stars include [[Kevin Brown (right-handed pitcher)|Kevin Brown]], [[Mark Teixeira]], [[Nomar Garciaparra]], [[Jason Varitek]], and [[Jay Payton]]. In golf, the legendary [[Bobby Jones (golfer)|Bobby Jones]] founded [[Masters Tournament|The Masters]], [[David Duval]] was ranked No.&nbsp;1 in the world in 2001, [[Stewart Cink]] the [[2009 Open Championship]] winner, was ranked in the top ten, and [[Matt Kuchar]] won the U.S. Amateur.

==See also==
{{Portal|ACC}}
*[[1916 Cumberland vs. Georgia Tech football game]] – The most lopsided college football game of all time

==References==
{{Reflist|30em}}

==External links==
* {{Official website}}

{{Georgia Tech Navbox}}
{{Navboxes
|titlestyle = {{CollegePrimaryStyle|Georgia Tech Yellow Jackets|color=#000000}}
|list =
{{Atlantic Coast Conference navbox}}
{{College sports in Georgia}}
{{Atlanta Sports}}
}}
[[Category:Georgia Tech Yellow Jackets|*]]