{{Infobox short story | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = Eleonora
| title_orig    = 
| image         = [[File:The gift cover 1842 poe.jpg|200px]]
| caption       = The cover of ''The Gift for 1842''
| translator    = 
| author        = [[Edgar Allan Poe]]
| country       = United States
| language      = English
| series        = 
| genre         = [[Romance (novel)|Romance]]<br>[[Short story]]
| published_in  = ''The Gift: A Christmas and New Year's Present for 1842''
| publisher     = Carey and Hart
| media_type    = 
| pub_date  = [[1842 in literature|1842]]
| english_pub_date = 
| preceded_by   = 
| followed_by   = 
}}
"'''Eleonora'''" is a [[short story]] by [[Edgar Allan Poe]], first published in [[1842 in literature|1842]] in Philadelphia in the literary annual ''The Gift''.  It is often regarded as somewhat [[autobiographical]] and has a relatively "happy" ending.

== Plot summary ==
[[File:Poe eleonora byam shaw.JPG|thumb|Illustration by [[Byam Shaw]] for a [[London]] edition dated 1909]]
The story follows an unnamed narrator who lives with his cousin and aunt in "The Valley of the Many-Colored Grass", an idyllic paradise full of fragrant flowers, fantastic trees, and a "River of Silence". It remains untrodden by the footsteps of strangers and so they live isolated but happy.

After living like this for fifteen years, "Love entered" the hearts of the narrator and his cousin Eleonora. The [[valley]] reflected the beauty of their young love:
{{quote|text=The passion which had for centuries distinguished our race... together breathed a delirious bliss over the Valley of the Many-Colored Grass. A change fell upon all things. Strange, brilliant flowers, star-shaped, burst out upon the trees where no flowers had been known before. The tints of the green carpet deepened; and when, one by one, the white [[Asteraceae|daisies]] shrank away, there sprang up in place of them, ten by ten of the ruby-red [[asphodelus|asphodel]]. And life arose in our paths; for the tall [[flamingo]], hitherto unseen, with all gay flowing birds, flaunted his scarlet plumage before us.}}

Eleonora, however, was sick — "made perfect in loveliness only to die". She does not fear death, but fears that the narrator will leave the valley after her death and transfer his love to someone else. The narrator emotionally vows to her, with "the Mighty Ruler of the Universe" as his witness, to never bind himself in marriage "to any daughter on Earth".

After Eleonora's death, however, the Valley of the Many-Colored Grass begins to lose its lustre and warmth. The narrator chooses to leave to an unnamed "strange city". There, he meets a woman named Ermengarde and, without guilt, marries her. Eleonora soon visits the narrator from beyond the grave and grants her blessings to the couple. "Thou art absolved", she says, "for reasons which shall be made known to thee in [[Heaven]]."

== Analysis ==
Many biographers consider "Eleonora" an autobiographical story written for Poe to alleviate his own feelings of guilt for considering other women for love.  At the time of the publication of this very short tale, his wife [[Virginia Eliza Clemm Poe|Virginia]] had just begun to show signs of illness, though she would not die for another five years.<ref name=Sova78>Sova, Dawn B. ''Edgar Allan Poe: A to Z''. New York: Checkmark Books, 2001. p. 78. ISBN 0-8160-4161-X</ref> The narrator, then, is Poe himself, living with his young cousin (soon-to-be wife) and his aunt.

The abrupt ending, with the narrator's new love only named in the third to last paragraph, is somewhat unconvincing if this is Poe's attempt at justifying his own feelings. Poe considered the tale "not ended so well as it might be". Perhaps, it is in the vagueness of the reason which will only be revealed in Heaven for permission to break his vow. Even so, compared to the endings of other Poe tales where the dead lover returns from beyond the grave, this is a "happy" ending, free of antagonism, guilt or resentment.<ref>Kennedy, J. Gerald. "Poe, 'Ligeia,' and the Problem of Dying Women" as collected in ''New Essays on Poe's Major Tales'', edited by Kenneth Silverman. Cambridge University Press, 1993. p. 126</ref> In "[[Morella (short story)|Morella]]", for example, the dead wife reincarnates as her own daughter, only to die. In "[[Ligeia]]", the first wife returns from the dead and destroys the narrator's new love. Ultimately, the message in "Eleonora" is that a man is allowed to wed without guilt after the death of his first love.

The narrator readily admits [[insanity|madness]] in the beginning of the story, though he believes it has not been determined if madness is actually the loftiest form of intelligence.<ref>Ruffner, Courtney J. and Jeff Grieneisen. "Intelligence: Genius or Insanity? Tracing Motifs in Poe's Madness Tales" collected in ''Bloom's BioCritiques: Edgar Allan Poe'', Harold Bloom, ed. Philadelphia: Chelsea House Publishers, 2002. p. 55 ISBN 0-7910-6173-6</ref> This may be meant facetiously, but it also may explain the excessively paradise-like description of the valley<ref>Ruffner, Courtney J. and Jeff Grieneisen. "Intelligence: Genius or Insanity? Tracing Motifs in Poe's Madness Tales" collected in ''Bloom's BioCritiques: Edgar Allan Poe'', Harold Bloom, ed. Philadelphia: Chelsea House Publishers, 2002. p. 59 ISBN 0-7910-6173-6</ref> and how it changes with their love and, later, with Eleonora's death. His admission of madness, however, excuses him from introducing such fantastic elements.<ref>Fisher, Benjamin Franklin. "'Eleonora': Poe and Madness" collected in ''Poe and His Times: The Artist and His Milieu'', Benjamin Franklin Fisher, ed. Baltimore: The Edgar Allan Poe Society, 1990. p. 180 ISBN 0-9616449-2-3</ref>

It is unclear why the trio lived in isolation in the valley.

There are also sexual themes in the story. The narrator's name, Pyros, implies fire and passion. As he and Eleonora grow, their innocent relationship turns to love with descriptions of the changing landscape being erotic or sexual - animal life and plant life sprouting forth and multiplying. Eleonora's death serves as a symbolic end to ideal romantic love which is soon replaced with the less passionate married love for Ermengarde.<ref>Benton, Richard P. "Friends and Enemies: Women in the Life of Edgar Allan Poe" collected in ''Myths and Realities: The Mysterious Mr. Poe''. Baltimore: Edgar Allan Poe Society, 1987. pp. 6-7. ISBN 0-9616449-1-5</ref> Eleonora embodies many typical traits in Poe's female character: she is young, passive, and completely devoted to her love.<ref>Weekes, Karen. "Poe's feminine ideal," collected in ''The Cambridge Companion to Edgar Allan Poe'', edited by Kevin J. Hayes. Cambridge University Press, 2002. p. 154. ISBN 0-521-79727-6</ref>

The term "Valley of the Many-Colored Grass" was inspired by "[[Adonaïs]]" by [[Percy Bysshe Shelley]].<ref>Meyers, Jeffrey. ''Edgar Allan Poe: His Life and Legacy''. New York: Cooper Square Press, 1992: 129. ISBN 0-8154-1038-7.</ref>

=== Major themes ===
A woman returning from beyond the grave to visit her former love is a device often used by Poe. See also "[[Ligeia]]" and "[[Morella (short story)|Morella]]". Poe also often wrote about the death of beautiful women, which he considered the most poetical topic in the world.<ref>Silverman, Kenneth. ''Edgar A. Poe: Mournful and Never-ending Remembrance''. New York: Harper Perennial, 1991. p. 239. ISBN 0-06-092331-8</ref>

==Critical response==
Poe's friend [[Thomas Holley Chivers]] praised "Eleonora" for being nearly a [[prose poem]]. He compared its originality and its execution to the work of [[Ossian]].<ref>Chivers, Thomas Holley. ''Chivers' Life of Poe'', edited by Richard Beale Davis. New York: E. P. Dutton & Co., Inc., 1952. p. 79.</ref> Poe biographer Arthur Hobson Quinn called it "one of his finest stories."<ref>Quinn, Arthur Hobson. ''Edgar Allan Poe: A Critical Biography''. Baltimore: The Johns Hopkins University Press, 1998. p. 329. ISBN 0-8018-5730-9</ref>

== Publication history ==
The story was first published in the 1842 edition of ''The Gift: A Christmas and New Year's Present'', an annual publication, as "Eleonora: A Fable". It was later republished in the May 24, 1845 issue of the ''[[Broadway Journal]]''.<ref name=Sova78/> In 1845, Poe added the opening epigraph, a quote from Raymond Lull that translates to "Under the protection of a specific form, my soul is safe."<ref name=Fisher181>Fisher, Benjamin Franklin. "'Eleonora': Poe and Madness" collected in ''Poe and His Times: The Artist and His Milieu'', Benjamin Franklin Fisher, ed. Baltimore: The Edgar Allan Poe Society, 1990. p. 181. ISBN 0-9616449-2-3</ref> The original publication named the narrator Pyrros.<ref name=Fisher181/>

== References ==
{{reflist|2}}

==External links==
*{{wikisource-inline|Eleonora}}
*[http://www.eapoe.org/works/info/pt031.htm Publication history] of "Eleonora" at the Edgar Allan Poe Society online
* {{librivox book | title=Eleonora | author=Edgar Allan Poe}}

{{Edgar Allan Poe}}
[[Category:1842 short stories]]
[[Category:Short stories by Edgar Allan Poe]]