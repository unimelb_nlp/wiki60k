{{orphan|date=May 2010}}

The annual '''International Literature and Psychology Conference (ILPC)''', also called the '''International Conference on Literature and Psychoanalysis''' or '''International Conference in Literature-and-Psychology''',<ref name="clas.ufl.edu">[http://www.clas.ufl.edu/ipsa/2003/leibovic.htm A History of the International Conference in Literature-and-Psychology]</ref> provides a forum for the exchange of ideas on the [[Psychology|psychological]] study of [[literature]] and other arts. The conference welcomes papers that deal with the application of psychology—including psychoanalysis, object relations theory, feminist, Jungian, or Lacanian approaches, cognitive psychology, or neuroscience–to the study of literature, film and visual media, painting, sculpture, music, performance, or the other arts. 

Participants come from nations around the world including [[France]], [[England]], [[Portugal]], [[Spain]], [[Italy]], the [[Netherlands]], [[Germany]], [[Denmark]], [[Finland]], [[Hungary]], [[Serbia]], the [[Czech Republic]], [[Greece]], [[Cyprus]], [[Turkey]], [[Japan]], [[Canada]], and the [[United States of America]]. The PsyArt Foundation, IPSA (at the [[University of Florida]]), along with various other universities, the [[University of Helsinki]], the [[University of Paris]] VII and X, the [[University of Freiburg]], the [[Instituto Superior de Psicologia Applicada]] in [[Lisbon]], and other institutions, have for a number of years sponsored the annual International Conference in Literature and Psychology. <ref>[http://www.clas.ufl.edu/ipsa/intl-gen.htm The International Conference In Literature-And-Psychology<!-- Bot generated title -->]</ref> In 2008, the conference was renamed to '''International Conference on Psychology and the Arts'''.

==International conference history==
*1st: Pècs, Hungary. 1983.<ref name="clas.ufl.edu"/>
*2nd: Montpellier, France.<ref name="clas.ufl.edu"/>
*3rd: Aix-en-Provence, France.<ref name="clas.ufl.edu"/>
*4th: Kent, Ohio, U.S.A.<ref name="clas.ufl.edu"/>
*5th: Kirchberg, Austria.<ref name="clas.ufl.edu"/>
*6th: Janus Pannonius University. Pècs, Hungary. July, 1989.[http://www.hum.ucy.ac.cy/ENG/people/byles.htm]<ref name="clas.ufl.edu"/>
*7th: Centre International de Semiotique et de Linguistique. Urbino, Italy. July, 1990.[http://www.hum.ucy.ac.cy/ENG/people/byles.htm]
*8th: London, England. July, 1991.[http://www.hum.ucy.ac.cy/ENG/people/byles.htm]
*9th: Lisbon, Portugal. July, 1992. [http://www.hum.ucy.ac.cy/ENG/people/byles.htm]
*10th: University of Gröningen. Amsterdam, Holland. June–July, 1993.[http://portal.uni-freiburg.de/ndl/personen/ehemalige/dokumente/pietzcker_publikationen.html/][http://www.hum.ucy.ac.cy/ENG/people/byles.htm]
*11th: Sandbjerg, Denmark. June, 1994.<ref>Frederico Pereira (Ed.), Proceedings of the 11th International Conference on Literature and Psychology. Sandbjerg (Denmark). June 1994. Lisbon: Instituto Superior de Psicologia Aplicada [ISPA], 1995.</ref>
*12th: Freiburg, Germany.<ref name="clas.ufl.edu"/>
*13th: Bentley College. Waltham, Massachusetts, U.S.A. July 2–6, 1996.[http://www.webcitation.org/query?url=http://www.geocities.com/TimesSquare/2459/ijpi1096.html&date=2009-10-25+11:23:25]
*14th: Avila, Spain. July 2–6, 1997. [http://www.clas.ufl.edu/ipsa/1997.htm]
*15th: 1998. St. Petersburg Hotel. St. Petersburg, Russia. July 2–6, 1998. [http://www.clas.ufl.edu/ipsa/1998.htm]
*16th: University of Urbino. Urbino, Italy. July 8–12, 1999. [http://www.clas.ufl.edu/ipsa/urbino/agenda.html]
*17th: Bialystock, Poland. July 6–10, 2000. [http://www.clas.ufl.edu/ipsa/2000.htm]
*18th: University of Cyprus. Nicosia. May 15–20, 2001. [http://www.clas.ufl.edu/ipsa/2001.htm]
*19th: University of Siena. Arezzo, Italy. June 26 - July 1, 2002. [http://www.clas.ufl.edu/ipsa/2002.htm]
*20th: University of Greenwich. London, England. July 2–7, 2003. [http://www.clas.ufl.edu/ipsa/2003/]
*21st: Arles, France. June 30 - July 5, 2004.[http://www.clas.ufl.edu/ipsa/2004/index.htm]
*22nd: Córdoba, Spain. June 29 - July 4, 2005.[http://www.clas.ufl.edu/ipsa/2005/index.htm]
*23rd: University of Helsinki. Helsinki, Finland. June 28 - July 3, 2006.[http://www.clas.ufl.edu/ipsa/2006/]
*24th: University of Belgrade. Belgrade, Serbia. July 4–9, 2007.[http://www.clas.ufl.edu/ipsa/2007/index.htm]
*25th: Instituto Superior de Psicologia Aplicada, Lisbon.[http://www.clas.ufl.edu/ipsa/2008/index.htm]
*26th: University of Viterbo. Viterbo, Italy. July 1 - July 6, 2009. [http://www.clas.ufl.edu/ipsa/2009/]
*27th: University of Pécs. Pécs,Hungary. June 23-28, 2010.[http://www.clas.ufl.edu/ipsa/2010/index.html]
*28th: Roskilde University, Roskilde,Denmark. June 22-26, 2011. [http://conf.psyartjournal.com/2011/]
*29th: University of Ghent, Ghent Belgium. July 4-8, 2012.[http://conf.psyartjournal.com/2012/]
*30th: University of Porto, Portugal. June 26-30, 2013. [http://conf.psyartjournal.com/2013/]
*31st: Universidad Complutense de Madrid, Spain. June 25-29, 2014.[http://conf.psyartjournal.com/2014/]
*32nd: University of Malta, Msida. June 24-28, 2015. [http://conf.psyartjournal.com/2015/]

==References==
{{reflist}}

==External links==
*[http://www.clas.ufl.edu/ipsa/intro.htm Institute for the Psychological Study of the Arts]

[[Category:Academic conferences]]
[[Category:Psychology-related professional associations]]