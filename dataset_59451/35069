{{good article}}
{{Infobox football match
|                   title = 1984 European Super Cup
|                   image = [[File:1984 European Super Cup programme.jpeg|200px]]
|                 caption = Match programme cover
|                   event = [[UEFA Super Cup|European Super Cup]]
|                   team1 = [[Juventus F.C.|Juventus]]
|        team1association = {{flagicon|ITA|size=30px}}
|              team1score = 2
|                   team2 = [[Liverpool F.C.|Liverpool]]
|        team2association = {{flagicon|ENG|size=30px}}
|              team2score = 0
|                 details = 
|                    date = 16 January 1985
|                 stadium = [[Stadio Olimpico di Torino|Stadio Comunale]]
|                    city = [[Turin]]
|      man_of_the_match1a = 
|                 referee = [[Dieter Pauly]] ([[German Football Association#History|West Germany]])
|              attendance = 55,384
|                 weather = 
|                previous = [[1983 European Super Cup|1983]]
|                    next = [[1986 European Super Cup|1986]]
}}

The '''1984 European Super Cup''' was an [[association football]] match between Italian team [[Juventus F.C.|Juventus]] and English team [[Liverpool F.C.|Liverpool]], which took place on 16 January 1985 at the [[Stadio Olimpico di Torino|Stadio Comunale]]. The match was the annual [[UEFA Super Cup|European Super Cup]] contested between the winners of the [[UEFA Champions League|European Cup]] and [[UEFA Cup Winners' Cup|European Cup Winners' Cup]]. At the time, the European Super Cup was generally a two-legged fixture, but only the first leg (in [[Turin]]) was played, due to fixture congestion. 

Juventus were appearing in the Super Cup for the first time. Liverpool were appearing in the competition for the third time, they had won the competition in [[1977 European Super Cup|1977]], and lost in [[1978 European Super Cup|1978]] to Belgian team [[R.S.C. Anderlecht|Anderlecht]]. Juventus won the [[1983–84 European Cup Winners' Cup]], beating Portuguese team [[F.C. Porto|Porto]] 2–1 in the [[1984 European Cup Winners' Cup Final|final]]. Liverpool qualified by winning the [[1983–84 European Cup]]. They beat Italian team [[A.S. Roma|Roma]] 4–2 in a penalty shootout after the [[1984 European Cup Final|final]] had finished 1–1.

Watched by a crowd of 55,384, Juventus took the lead in the first half when [[Zbigniew Boniek]] scored in the 39th minute. Boniek scored again in the second half to give Juventus a 2–0 lead which they held on to until the end of the match to win their first Super Cup. The two clubs met later in the season in the [[1985 European Cup Final]], which resulted in the death of 39 spectators due to a [[Heysel Stadium disaster|disaster]] that occurred prior to kick-off. Juventus won the match 1–0.

==Match==

===Background===
The [[UEFA Super Cup|European Super Cup]] was founded in the early 1970s, as a means to determine the best team in Europe and serve as a challenge to [[AFC Ajax|Ajax]], the strongest club side of its day.<ref name="sco">{{cite web|url=http://www.uefa.com/uefasupercup/history/ |title=Club competition winners do battle |work=UEFA.com |accessdate=24 November 2015}}</ref> The proposal by Dutch journalist [[Anton Witkamp]], a football match between the holders of the [[UEFA Champions League|European Cup]] and [[UEFA Cup Winners' Cup|Cup Winners' Cup]], failed to receive [[UEFA]]'s backing,<ref name="sco"/> given the recent Cup Winners' Cup winners [[Rangers F.C.|Rangers]] had been banned from European competition.{{#tag:ref|In 1972, [[Rangers F.C.|Rangers]] was banned from European competition for two years after fans clashed with Spanish police while celebrating the club's victory over [[FC Dynamo Moscow|Dynamo Moscow]] in the [[1972 European Cup Winners' Cup Final|European Cup Winners' Cup Final]]. The ban was later reduced to one year on appeal.<ref>{{cite news |first=Jonathan |last=Wilson |title='The behaviour of the Scottish fans was shocking and ugly' |url=https://www.theguardian.com/sport/blog/2008/may/13/thebehaviourofthescottish |newspaper=The Observer |location=London |date=13 May 2008 |accessdate=24 November 2015}}</ref>|group="n"}} Witkamp nonetheless proceeded with his vision, a two-legged match played between Ajax and Rangers in January 1973.<ref name="sco"/> The competition was endorsed and recognised by UEFA a year later.<ref name="sco"/>

Juventus qualified for the Super Cup as the reigning [[UEFA Cup Winners' Cup|European Cup Winners' Cup]] winners. They had remained unbeaten throughout the [[1983–84 European Cup Winners' Cup]], and beat [[F.C. Porto|Porto]] 2–1 in the [[1984 European Cup Winners' Cup Final|final]].<ref>{{cite web|url=http://en.archive.uefa.com/competitions/ecwc/history/season=1983/intro.html |title=1983/84: Star-studded Juventus make their mark |publisher=UEFA |date=1 June 1984 |accessdate=28 October 2015 |deadurl=yes |archiveurl=https://web.archive.org/web/20100503062002/http://en.archive.uefa.com/competitions/ecwc/history/season=1983/intro.html |archivedate=3 May 2010 |df= }}</ref> It was Juventus' first appearance in the competition.<ref>{{cite web|url=http://www.uefa.com/teamsandplayers/teams/club=50139/profile/index.html |title=Juventus |publisher=UEFA |accessdate=1 November 2015 }}</ref> 

Liverpool had qualified for the competition as a result of winning the [[1983–84 European Cup]]. They had beaten [[A.S. Roma|Roma]] 4–2 in a penalty shootout, after the match had finished [[1984 European Cup Final|1–1]].<ref>{{cite news|url=https://www.theguardian.com/football/blog/2013/may/23/liverpool-great-european-cup-teams |title=The great European Cup teams: Liverpool 1977–84 |work=The Guardian |location=London |date=23 May 2013 |accessdate=3 June 2015 |first=Paul |last=Wilson }}</ref> Liverpool were appearing in their third Super Cup. They won the competition on their first appearance in [[1977 European Super Cup|1977]], beating German team [[Hamburger SV|Hamburg]] 7–1 on aggregate.<ref>{{cite web |url=http://www.uefa.com/uefasupercup/history/season=1977/index.html |title=1977: McDermott treble lifts Liverpool |publisher=UEFA |accessdate=1 November 2015 }}</ref> Their other appearance in [[1978 European Super Cup|1978]] resulted in a defeat to Belgian team [[R.S.C. Anderlecht|Anderlecht]].<ref>{{cite web|url=http://www.uefa.com/uefasupercup/history/season=1978/index.html |title=1978: Anderlecht back on top |publisher=UEFA |accessdate=1 November 2015 }}</ref>

Traditionally, the Super Cup had been played over two legs, but due to both clubs experiencing fixture congestion, was played as a one-off match in [[Turin]] in January 1985.<ref>{{cite news|language=Italian|url=http://www.archiviolastampa.it/component/option,com_lastampa/task,search/mod,libera/action,viewer/Itemid,3/page,13/articleid,1359_02_1985_0015_0013_19429333/|title=Stasera la Supercoppa, poi quella dei Campioni per fare un bel "poker"|publisher=[[La Stampa|Stampa Sera]]|author=Angelo Caroli|date=16 January 1985|page=13}}</ref>

===Summary===
[[File:Stadio Olimpico Torino Italy.jpg|thumb|right|The [[Stadio Olimpico di Torino|Stadio Comunale]] where the match was held.]]
Bad weather in Turin created doubt about whether the match could be completed. However, the referee decided to go ahead with the match. Liverpool were without striker [[Kenny Dalglish]] who was suspended. Liverpool struggled to gain a foothold in the match in the first half and were behind when Juventus scored in the 40th minute. A mishit pass by [[Massimo Briaschi]] found [[Zbigniew Boniek]] whose subsequent shot from the edge of the Liverpool penalty area beat goalkeeper [[Bruce Grobbelaar]] to give Juventus a 1–0 lead. Liverpool had a chance to equalise before the end of the first half, but midfielder [[John Wark]] put his shot wide of the Juventus goal. Liverpool started the second half without defender [[Mark Lawrenson]] who had injured himself during the first half, he was replaced by [[Gary Gillespie]]. Liverpool tried to level the match in the second half, but their best chances came from midfielder [[Ronnie Whelan]] whose shots from distance did not result in any goals. Juventus extended their lead late in the second half when Boniek scored again. A cross from Briaschi found Boniek, whose shot beat Grobbelaar to extend Juventus' lead to 2–0. Five minutes later, Juventus nearly extended their lead again. However, striker [[Paolo Rossi]]'s shot was saved by Grobbelaar. Juventus held onto their lead to win the match 2–0 and win the Super Cup.<ref>{{cite news |url=http://www.londonhearts.com/scores/images/1985/1985011602.htm |title=Liverpool have no answer to Boniek |work=The Times |location=London |date=17 January 1985 |issue=62038 |page=20}}</ref>

===Details===
{{footballbox
|date=16 January 1985
|time=20:30 [[Central European Time|CET]]
|team1=[[Juventus F.C.|Juventus]] {{flagicon|ITA}}
|score=2–0
|report=[http://www.uefa.com/uefasupercup/history/season=1984/index.html Report]
|team2={{flagicon|ENG}} [[Liverpool F.C.|Liverpool]]
|goals1= [[Zbigniew Boniek|Boniek]] {{goal|40||80}}
|goals2= 
|stadium=[[Stadio Olimpico di Torino|Stadio Comunale]], [[Turin]]
|attendance=55,384
|referee= [[Dieter Pauly]] ([[German Football Association#History|West Germany]])}}

{| width="100%"
|valign="top" width="50%"|
{| style="font-size:90%" cellspacing="0" cellpadding="0"
|colspan=4|
|-
!width=25| !!width=25|
|-
|GK ||'''1''' ||{{flagathlete|[[Luciano Bodini]]|ITA}}
|-
|DF ||'''2''' ||{{flagathlete|[[Luciano Favero]]|ITA}} 
|-
|DF ||'''3''' ||{{flagathlete|[[Antonio Cabrini]]|ITA}}
|-
|MF ||'''4''' ||{{flagathlete|[[Massimo Bonini]]|SMR}}
|-
|DF ||'''5''' ||{{flagathlete|[[Sergio Brio]]|ITA}}
|-
|DF ||'''6''' ||{{flagathlete|[[Gaetano Scirea]]|ITA}}
|-
|FW ||'''7''' ||{{flagathlete|[[Massimo Briaschi]]|ITA}}
|- 
|MF ||'''8''' ||{{flagathlete|[[Marco Tardelli]]|ITA}} 
|-
|FW ||'''9''' ||{{flagathlete|[[Paolo Rossi]]|ITA}}
|-
|MF ||'''10''' ||{{flagathlete|[[Michel Platini]]|FRA}}
|-
|MF ||'''11''' ||{{flagathlete|[[Zbigniew Boniek]]|POL}}
|-
|colspan=3|'''Substitutes:''' 
|-
|GK ||'''12''' ||{{flagathlete|[[Stefano Tacconi]]|ITA}}
|-
|DF ||'''13''' ||{{flagathlete|[[Nicola Caricola]]|ITA}}
|-
|MF ||'''14''' ||{{flagathlete|[[Cesare Prandelli]]|ITA}}
|-
|MF ||'''15''' ||{{flagathlete|[[Bruno Limido]]|ITA}}
|-
|MF ||'''16''' ||{{flagathlete|[[Beniamino Vignola]]|ITA}}
|-
|colspan=3|'''Manager:'''
|-
|colspan=4|{{flagathlete|[[Giovanni Trapattoni]]|ITA}}
|}
|valign="top" width="50%"|
{| style="font-size: 90%" cellspacing="0" cellpadding="0" align=center
|colspan="4"|
|-
!width=25| !!width=25|
|-
|GK ||'''1''' ||{{flagathlete|[[Bruce Grobbelaar]]|ZIM}}
|-
|RB ||'''2''' ||{{flagathlete|[[Phil Neal]]|ENG}} 
|-
|LB ||'''3''' ||{{flagathlete|[[Alan Kennedy]]|ENG}}
|-
|CB ||'''4''' ||{{flagathlete|[[Mark Lawrenson]]|IRL}} || || {{suboff|46}}
|-
|RM ||'''5''' ||{{flagathlete|[[Steve Nicol]]|SCO}}
|-
|CB ||'''6''' ||{{flagathlete|[[Alan Hansen]]|SCO}}
|-
|CF ||'''7''' ||{{flagathlete|[[Paul Walsh]]|ENG}} 
|-
|LM ||'''8''' ||{{flagathlete|[[Ronnie Whelan]]|IRL}}
|-
|CF ||'''9''' ||{{flagathlete|[[Ian Rush]]|WAL}}
|-
|CM ||'''10'''||{{flagathlete|[[Kevin MacDonald (footballer)|Kevin MacDonald]]|SCO}}
|-
|CM ||'''11'''||{{flagathlete|[[John Wark]]|SCO}}
|-
|colspan=3|'''Substitutes:''' 
|-
|DF ||'''12''' ||{{flagathlete|[[Jim Beglin]]|IRL}}
|-
|GK ||'''13''' ||{{flagathlete|[[Bob Bolder]]|ENG}}
|-
|DF ||'''14''' ||{{flagathlete|[[Gary Gillespie]]|SCO}} || || {{subon|46}}
|-
|MF ||'''15''' ||{{flagathlete|[[Sammy Lee (footballer)|Sammy Lee]]|ENG}}
|-
|MF ||'''16''' ||{{flagathlete|[[Jan Mølby]]|DEN}}
|-
|colspan=3|'''Manager:''' 
|-
|colspan=4|{{flagathlete|[[Joe Fagan]]|ENG}}
|}
|}

==Post-match==
The two sides met again at the end of the season in the [[1985 European Cup Final]]. However, the events of the match were overshadowed by the [[Heysel Stadium disaster|disaster]] that occurred before kick-off. Liverpool fans breached a fence separating the two groups of supporters and charged the Juventus fans. The resulting weight of people caused a retaining wall to collapse, killing 39 people and injuring hundreds. English clubs were banned indefinitely from European competition, with a condition that when the ban was lifted, Liverpool would serve an extra three-year ban.<ref>{{harvtxt|Ponting|1992|p=189}}</ref> The ban eventually lasted for five years, clubs returning to European competition in the 1990–91 season.<ref>{{harvtxt|Hutchings,Nawrat|1995|p=251}}</ref> Juventus won the match 1–0 to win the European Cup for the first time.<ref>{{cite news|url=http://news.bbc.co.uk/onthisday/hi/dates/stories/may/29/newsid_2733000/2733979.stm |title=1985: Fans die in Heysel rioting |publisher=BBC News |accessdate=28 June 2015 }}</ref>

Liverpool finished second in the First Division during the  [[1984–85 Football League#First Division|1984–85 Football League]]. They were thirteen points behind champions [[Everton F.C.|Everton]]. Juventus finished the [[1984–85 Serie A]] in sixth place, seven points behind champions [[Hellas Verona F.C.|Hellas Verona]].<ref>{{cite web|url=http://www.rsssf.com/tablesi/ital85.html |title=Italy 1984/85 |publisher=Rec. Sport. Soccer. Statistics. Foundation |date=26 October 2000 |accessdate=28 June 2015 |first=Maurizio |last=Mariani }}</ref>

==Notes==
{{reflist|group=n}}

==Footnotes==
{{reflist|2}}

==References==
*{{cite book |last1=Hale |first1=Steve |last2=Ponting |first2=Ivan |title=Liverpool In Europe |year=1992 |publisher=Guinness Publishing |location=London |isbn=0-85112-569-7 }}
*{{cite book |last1=Hutchings |first1=Steve |last2=Nawrat |first2=Chris |title=The Sunday Times Illustrated History of Football: The Post-War Years |publisher=Chancellor Press |year=1995 |location=London |isbn=1-85153-014-2 }}

==External links==
*[http://www.uefa.com/uefasupercup/history/season=1984/ 1984 European Super Cup at UEFA]

{{UEFA Super Cup}}
{{1984–85 in European football (UEFA)}}
{{Juventus F.C. matches}}
{{Liverpool F.C. matches}}

[[Category:1984–85 in European football|Super Cup]]
[[Category:UEFA Super Cup|1984]]
[[Category:Liverpool F.C. matches|Super Cup 1984]]
[[Category:Juventus F.C. matches|Super Cup 1984]]
[[Category:International club association football competitions hosted by Italy|Super Cup 1984]]
[[Category:1984–85 in Italian football|Super Cup 1984]]
[[Category:1984–85 in English football|Super Cup 1984]]
[[Category:Sports competitions in Turin]]
[[Category:1980s in Turin]]