{{featured article}}
{{Use dmy dates|date=January 2016}}
{{Use Australian English|date=January 2016}}
{{Infobox MP
| honorific-prefix = [[The Honourable]]
| name        = William Joseph Denny
| honorific-suffix = [[Military Cross|MC]]
| image       = B6233 William Joseph Denny.jpg
| alt         = a formal black and white portrait of a male with a moustache wearing a suit
| caption     = William Joseph Denny {{circa}} 1910
| order1=29th
| office1= Attorney-General of South Australia
| term_start1  =  17 April 1930
| term_end1    =  18 April 1933
| predecessor1     = [[Hermann Homburg]]
| successor1       = [[Shirley Jeffries]]
| premier1         = [[Lionel Hill]] then [[Robert Richards (Australian politician)|Robert Richards]]
| term_start2  =  16 April 1924
| term_end2    =  8 April 1927
| predecessor2     = [[Hermann Homburg]]
| successor2       = [[Hermann Homburg]]
| premier2         = [[John Gunn (Australian politician)|John Gunn]]
| term_start3  =  3 June 1910
| term_end3    =  17 February 1912
| predecessor3     = [[Hermann Homburg]]
| successor3       = [[Hermann Homburg]]
| premier3         = [[John Verran]]
| constituency_MP4 = [[Electoral district of Adelaide|Adelaide]]
| parliament4  =  South Australian
| term_start4  =  3 November 1906
| term_end4    =  7 April 1933
| term_start5  =  3 May 1902
| term_end5    =  26 May 1905
| constituency_MP6 = [[Electoral district of West Adelaide|West Adelaide]]
| parliament6  =  South Australian
| term_start6  =  17 March 1900
| term_end6    =  2 May 1902
| birth_date         = {{Birth date|1872|12|06|df=y}}
| birth_place        = [[Adelaide]], [[South Australia]]
| death_date         = {{Death date and age|1946|05|02|1872|12|06|df=y}}
| death_place        = [[Norwood, South Australia]]
| restingplace       = [[West Terrace Cemetery]], Adelaide
| party              = [[Independent politician|Independent]] [[Liberalism|liberal]] (1900–05)<br/>[[Australian Labor Party|United Labor Party]] (1906–17)<br/>[[Australian Labor Party]] (1917–31)<br>{{nowrap|[[Parliamentary Labor Party]] (1931–33)}}
| spouse             = Winefride Mary ({{nee}} Leahy)
| relations          = 
| children           = 
| parents            = Thomas Joseph Denny<br/>Annie Denny ({{nee}} Dwyer)
| education          = 
| alma_mater         = [[Christian Brothers College, Adelaide]]<br>[[University of Adelaide]]
| profession         = [[journalist]], [[solicitor]], [[soldier]]
| religion           = [[Catholic Church|Roman Catholic]]
<!--Military service-->
| nickname           = 
| allegiance         = 
| branch             = [[Australian Army]]
| serviceyears       = 1915–19
| rank               = [[Captain (armed forces)|Captain]]
| unit               = [[9th Light Horse Regiment]]<br>[[5th Division (Australia)|5th Divisional Ammunition Column]]<br>[[1st Division (Australia)|1st Divisional Artillery]]<br>[[First Australian Imperial Force|AIF Administrative Headquarters]]
| commands           = 
| battles            = First World War
* [[Western Front (World War I)|Western Front]]
| mawards            = [[Military Cross]]
| footnotes          = 
}}

'''William Joseph''' "'''Bill'''" '''Denny''', {{post-nominals|country=AUS|size=100%|MC}} (6 December 1872 – 2 May 1946) was a [[South Australia]]n journalist, lawyer, politician and decorated soldier who held the [[South Australian House of Assembly]] seats of [[Electoral district of West Adelaide|West Adelaide]] from 1900 to 1902 and then [[Electoral district of Adelaide|Adelaide]] from 1902 to 1905 and again from 1906 to 1933. After an unsuccessful candidacy as a [[Australian Labor Party|United Labor Party]] (ULP) member in 1899, he was elected as an "[[Independent politician|independent]] [[Liberalism|liberal]]" in a by-election in 1900. He was re-elected in 1902, but defeated in 1905. The following year, he was elected as a ULP candidate, and retained his seat for that party (the Australian Labor Party from 1917) until 1931. Along with the rest of the cabinet, he was ejected from the Australian Labor Party in 1931, and was a member of the [[Parliamentary Labor Party]] until his electoral defeat at the hands of a [[Lang Labor Party (South Australia)|Lang Labor Party]] candidate in 1933.

Denny was the [[Attorney-General of South Australia]] and Minister for the [[Northern Territory]] in the government led by [[John Verran]] (1910–12). In August 1915, Denny enlisted in the [[First Australian Imperial Force]] to serve in World War I, initially as a trooper in the [[9th Light Horse Regiment]]. After being [[Officer (armed forces)|commissioned]] in 1916, he served in the [[5th Division (Australia)|5th Division Artillery]] and [[1st Division (Australia)|1st Divisional Artillery]] on the [[Western Front (World War I)|Western Front]]. He was awarded the Military Cross in September 1917 when he was wounded while leading a convoy into forward areas near [[Ypres]], and ended the war as a [[Captain (armed forces)|captain]].<ref>{{Cite web|url = http://www.adelaidenow.com.au/anzac-centenary/king-george-v-awards-attorney-general-william-joseph-denny-the-military-cross-for-service-during-world-war-i/story-fnra3mm6-1227206799788|title = King George V awards Attorney General William Joseph Denny the Military Cross for service during World War I|date = 3 February 2015 <!-- 4:41PM --> |accessdate = |website = The Advertiser|publisher = |last = Hough|first = Andrew}}</ref>

He was again Attorney-General in the Labor governments led by [[John Gunn (Australian politician)|John Gunn]] (1924–26), [[Lionel Hill]] (1930–33) and [[Robert Richards (Australian politician)|Robert Richards]] (1933), and held other [[Ministry (government department)|portfolios]] in those governments, including housing, irrigation and repatriation. Denny published two memoirs of his military service, and when he died in 1946 aged 73, he was accorded a [[state funeral]].

==Early life==
William Joseph Denny was born in [[Adelaide]], [[South Australia]], on 6 December 1872, one of three children of Thomas Joseph Denny, a publican, and his wife Annie ({{nee}} Dwyer). He attended [[Christian Brothers College, Adelaide]],{{sfn|Lincoln|1981}} then worked as a weather clerk at the [[General Post Office, Adelaide]] under the [[Postmaster General]], [[Charles Todd (pioneer)|Sir Charles Todd]].{{sfn|The Adelaide Observer|1899}} According to a contemporary source, in 1893 he became the editor of the [[Catholic Church|Catholic]] [[The Southern Cross (South Australia)|''The Southern Cross'']] newspaper, which published news about and for the Catholic community of South Australia.{{sfn|The Sunday Times|1919}} A more recent source states he commenced as editor of ''The Southern Cross'' in 1896.{{sfn|Lincoln|1981}} He replaced [[James O'Loghlin (Australian politician)|James O'Loghlin]], who later became a [[Australian Labor Party|United Labor Party]] (ULP) [[Australian Senate|senator]] for South Australia.{{sfn|Powell|2014}} Denny was a [[councillor]] of the [[Adelaide City Council]] from 1898,{{sfn|Lincoln|1981}} representing Grey Ward. During his early twenties he was active in the literary and debating societies of Adelaide, was Chairman of the Christian Brothers Old Collegians Association, and captain of two city rowing clubs.{{sfn|The Adelaide Observer|1899}} He unsuccessfully contested the two-member [[Electoral district of West Adelaide|seat of West Adelaide]] in the [[South Australian colonial election, 1899|1899 South Australian colonial election]] as a ULP candidate, gaining 27.7 per cent of the vote.{{sfn|Jaensch|2007|p=143}}

When a by-election was held for [[West Adelaide colonial by-election, 1900|West Adelaide]] on 17 March 1900, Denny was elected to the single vacancy created by the resignation of the former [[Premier of South Australia]], [[Charles Kingston]].{{sfn|Jaensch|2007|p=145}} He ran as an "[[Independent politician|independent]] [[Liberalism|liberal]]" candidate,{{sfn|Howell|2002|p=115}} gaining 66.8 per cent of the vote.{{sfn|Jaensch|2007|p=145}} Prior to the [[South Australian state election, 1902|1902 state election]] the electoral district of West Adelaide was abolished. Denny contested the new four-member [[electoral district of Adelaide]], and was elected second in the count with 14.3 per cent of the votes cast.{{sfn|Jaensch|2007|p=156}} He was defeated at the [[South Australian state election, 1905|1905 state election]], gaining only 9.9 per cent of the votes.{{sfn|Jaensch|2007|p=167}} The following year, having abandoned his former liberalism,{{sfn|Howell|2002|p=315}} he contested the seat of Adelaide at the [[South Australian state election, 1906|state election]] as a ULP candidate, and was elected first, receiving 19.3 per cent of the votes cast.{{sfn|Jaensch|2007|p=176}} He was again returned first at the [[South Australian state election, 1910|1910 state election]],{{sfn|Jaensch|2007|p=188}} after which the ULP led by [[John Verran]] formed the first Labor government of South Australia on 3 June.{{sfn|Howell|2002|p=312}}{{sfn|Sharman|2014}} Having begun studying law at the [[University of Adelaide]] in 1903,{{sfn|Lincoln|1981}}{{sfn|The Sunday Times|1919}} Denny was [[Articled clerk|articled]] to J.R. Anderson {{post-nominals|country=AUS|size=100%|KC}},{{sfn|The Daily Herald|1921}} and was admitted as a [[solicitor]] in the [[Supreme Court of South Australia]] in 1908.{{sfn|Lincoln|1981}}

==Attorney-General==
Denny was appointed [[Attorney-General of South Australia]] and Minister controlling the [[Northern Territory]] on 3 June 1910. After conducting negotiations with the [[Commonwealth Government]], he relinquished his ministerial responsibility for the Northern Territory on 31 December 1910,{{sfn|Parliament of South Australia}} when its administration was transferred to the Commonwealth.{{sfn|Walker|1999|p=122}} During his time as Attorney-General, Denny drafted and led several important legislative reforms. These included the ''Advances for Homes Act 1910'', which allowed for 80 per cent of the value of a property to be advanced to a worker at 4.5 per cent interest over 36.5 years. In his speeches Denny highlighted that many workers were faced with high rents and poor conditions.{{sfn|Donovan|Painter|1990|p=47}} He also sponsored the ''Female Law Practitioners Act 1911'', which enabled women to practice law in South Australia for the first time. Tall, with "long, spindly legs",{{sfn|Lincoln|1981}} Denny was a favourite of cartoonists.{{sfn|The Daily Herald|1912}}

Verran called an [[South Australian state election, 1912|election in February 1912]], and the ULP were defeated by the [[Liberal Union (Australia)|Liberal Union]], although Denny was again returned first in the seat of Adelaide with 15.8 per cent of votes cast.{{sfn|Jaensch|2007|p=198}} He became a member of the University of Adelaide Council in April 1912, as a representative of the Parliament.{{sfn|The Advertiser|1912}} In 1913, a [[referendum]] to fix the closing time of licensed premises was proposed by the ULP. Even after the governing Liberal Union agreed to the conduct of the referendum at the next state election, Denny attacked them, claiming that they had no intention of implementing the outcome of the referendum if they were re-elected.{{sfn|Brooks|Gill|Weste|2008|pp=39–40}} Denny was returned unopposed at the [[South Australian state election, 1915|March 1915 state election]].{{sfn|Jaensch|2007|p=208}}

==World War I==
Denny enlisted in the [[First Australian Imperial Force|Australian Imperial Force]] (AIF) on 17 August 1915 at the age of 43, initially as a [[Trooper (rank)|trooper]].{{sfn|Lincoln|1981}} Before departing overseas, Denny had always been an advocate of [[conscription]].{{sfn|The Chronicle|1915}} He was later commissioned as a [[second lieutenant]] in the [[9th Light Horse Regiment]]. While in [[Egypt]], he transferred to the divisional [[artillery]] of the [[5th Division (Australia)|5th Division]], which then shipped to France, and he was promoted to [[lieutenant]] in June 1916.{{sfn|Lincoln|1981}} In January 1917, despite his previous stance on conscription, Denny refused requests to endorse it, instead stating that he did not think that intervention would be compatible with his duties as a soldier. He also considered that the majority of soldiers voted against it, and deplored the split in the Labor Party that conscription had created.{{sfn|The Daily Herald|1917}} In mid-1917 he was attached to the divisional artillery of the [[1st Division (Australia)|1st Division]].{{sfn|Lincoln|1981}} On the night of 15 September 1917, he was leading a convoy carrying water to forward areas when it was hit by a heavy artillery barrage, and he was wounded. His recommendation for the [[Military Cross]] read:{{sfn|Australian War Memorial}}

<blockquote>For conspicuous gallantry and devotion to duty whilst engaged in pack transport work near HOGGE on the night of 15 September 1917. Lieutenant DENNY showed great coolness and initiative throughout, especially when his convoy came under very heavy barrage in the vicinity of CLAPHAM JUNCTION. Although wounded himself, Lieut. DENNY personally obtained assistance for two of his men who were wounded. He then reorganised his command and succeeded in reaching his destination. Lieut. DENNY after delivering this water then went to the dressing station where he dictated a report to D.H.Q. before being evacuated.</blockquote>

He was invested with the Military Cross by King [[George V]] at [[Buckingham Palace]] in November 1917.{{sfn|The Adelaide Observer|1917}} After recovering from his wounds, he was attached to the [[repatriation]] section of AIF Administrative Headquarters in London from January 1918. He was promoted to [[captain (armed forces)|captain]] in September that year. He resigned his commission in the AIF in 1919 and published a memoir titled ''The Diggers'',{{sfn|Lincoln|1981}} the foreword of which was written by [[General]] [[William Birdwood, 1st Baron Birdwood|Sir William Birdwood]], who had commanded the AIF from 1915 until the end of the war.{{sfn|The Sunday Times|1919}}

==Return to Parliament==
[[File:Walking willie.jpg|thumb|left|"Walking Willy" – 1912. Denny's tall frame and long legs made him a favourite of cartoonists.|alt=a black and white cutting from a newspaper depicting Denny in cartoon form]]
Still serving overseas at the time of the [[South Australian state election, 1918|1918 state election]], Denny was returned first of three in Adelaide with 30.2 per cent of the ballots cast.{{sfn|Jaensch|2007|p=219}} He was repatriated to Australia via the United States on 2 August 1919,{{sfn|The Sunday Times|1919}} returning to his seat. While in the United States, he had been regularly published in the ''[[New York Herald]]''.{{sfn|The Daily Herald|1921}} He married Winefride Mary Leahy, a pianist and singer,{{sfn|Lincoln|1981}} on 15 January 1920 at St. Ignatius Church, [[Norwood, South Australia|Norwood]]. His brother, the Reverend Richard Denny, officiated at their wedding.{{sfn|The Advertiser|1920}} He was elected second of two in [[South Australian state election, 1921|1921]] and second of three in [[South Australian state election, 1924|1924]] with similar proportions of the vote.{{sfn|Jaensch|2007|pp=232 & 235}} He was appointed Attorney-General in the Labor government of [[John Gunn (Australian politician)|John Gunn]] in April 1924, and was also Minister for Housing, and initially, Assistant Minister for Repatriation. In January 1925 he was appointed as Minister for Irrigation and Minister for Repatriation, while retaining his Attorney-General and housing portfolios.{{sfn|Parliament of South Australia}}

During this period he carried out several significant legislative changes.{{sfn|Lincoln|1981}} In 1924, as Minister for Housing, Denny was closely associated with the [[Thousand Homes Scheme]], which aimed to provide affordable housing, particularly for returned soldiers and their families, and lower income groups.{{sfn|The Advertiser|1946}} The land used for this development was the site of the [[Mitcham, South Australia|Mitcham]] military camp at which Denny had trained before embarking for service overseas.{{sfn|City of Mitcham}} Denny's work on the Scheme resulted in a clash with former Premier Sir [[Henry Barwell]], whom he sued for [[Defamation|libel]] after Barwell made statements suggesting that Denny had made false statements to induce merchants to provide goods and services. Barwell later apologised for his comments.{{sfn|The Adelaide Observer|1926}}

Another change was the transition to the use of judges as the electoral [[returning officer]] for South Australia. This was done to impose state control on a system which had effectively combined the administration of the national and state electoral rolls.{{sfn|Macilwain|2007|pp=9–10}} On 27 May 1925,{{sfn|Macilwain|2007|p=12}} Denny arranged the appointment of Judge [[Herbert Kingsley Paine]] of the Insolvency Court to be appointed as Electoral Officer for the state,{{sfn|Macilwain|2007|p=11}} replacing Charles Mathews, a state public servant who had held the position since 1907.{{sfn|Macilwain|2007|p=4}} Denny had previously worked for Paine as a legal associate.{{sfn|Macilwain|2007|p=11}}

As a returned soldier, Denny was an exception among Labor politicians at both state and federal level in the 1920s. Willing and able to speak about his personal war experiences, he was one of the few Labor politicians invited to unveil memorials. He performed this role for the Soldiers' Memorial Hall at [[Lameroo]] in 1926, where his "address was punctuated with applause". When his political enemies persistently queried the circumstances under which he was awarded the Military Cross, he published the citation in response.{{sfn|Inglis|Brazier|2008|pp=197–198}} Despite Labor's loss in the [[South Australian state election, 1927|1927 state election]], Denny was returned first of three in the seat of Adelaide, with over 25 per cent of the vote.{{sfn|Jaensch|2007|p=238}} At the [[South Australian state election, 1930|April 1930 state election]], he was returned first of three with nearly 82 per cent of the ballots cast.{{sfn|Jaensch|2007|p=243}} Appointed Attorney-General in the Labor government of [[Lionel Hill]], Denny was also Minister of Railways, and for the first six months he was also Minister of Local Government.{{sfn|Parliament of South Australia}} On [[Anzac Day]] 1931, acting as Premier in Hill's absence, he officiated at the unveiling of the [[National War Memorial (South Australia)|National War Memorial]] at the corner of [[North Terrace, Adelaide|North Terrace]] and Kintore Avenue, Adelaide, before a crowd of about 75,000.{{sfn|Inglis|Brazier|2008|pp=281–282}} Denny is one of only a few South Australian ministers to have ever had military experience.{{sfn|Coulthard-Clark|1996|p=106}}

In 1931, Denny was expelled from the Labor Party, along with Hill and the rest of the cabinet, for supporting the "[[Premiers' Plan]]", which sought to impose austerity measures due to the poor economic conditions. The cabinet formed the [[Parliamentary Labor Party]] which continued to govern the state, led by Hill and then by [[Robert Richards (Australian politician)|Robert Richards]], with the support of the opposition until the [[South Australian state election, 1933|1933 state election]]. At the 1933 election, Denny lost his seat to a [[Lang Labor Party (South Australia)|Lang Labor Party]] candidate.{{sfn|Lincoln|1981}}

==Later life==
[[File:William Joseph Denny in uniform.jpg|thumb|right|upright|Captain W.J. Denny, MC|alt=a sepia portrait of a man in uniform]]
In September 1936, Bill's brother, the Catholic priest Reverend Denny, and his sister, Mary Catherine Denny, were involved in a vehicle accident in which Mary received fatal injuries. Reverend Denny suffered from an illness that resulted from the accident which contributed to his death in June 1941.{{sfn|The Narracoorte Herald|1941}} Denny wrote a further autobiographical book, ''A Digger at Home and Abroad'', which was published in 1941. He continued to practice law until his death, despite difficulties associated with [[rheumatoid arthritis]]. He died on 2 May 1946{{sfn|Lincoln|1981}} of a heart attack which developed at his home on Osmond Terrace, Norwood, after he returned from his office in Adelaide.{{sfn|The Australian Worker|1946}} He was survived by his wife, one son and three daughters. He was accorded a [[state funeral]], and was buried at [[West Terrace Cemetery]].{{sfn|Lincoln|1981}}

Denny was the patron of the [[West Adelaide Football Club]] for 20 years ending in 1930.{{sfn|The Register News-Pictorial|1930}} He enjoyed diving for [[crayfish]] under the rocks at the back of [[Rosetta Head]] near [[Victor Harbor]] on [[Encounter Bay]], and was often accompanied by Ephriam "Brownie" Tripp, an [[Aboriginal Australians|Aboriginal]] man from the [[Raukkan, South Australia|Point McLeay]] Aboriginal Mission.{{sfn|The Victor Harbour Times|1944}} According to his entry in the ''[[Australian Dictionary of Biography]]'', "his preferred reading was Shakespeare and the Bible and he quoted liberally from both. His integrity, versatility and wide knowledge were unquestioned, and he was proud of the democratic legislation he had sponsored."{{sfn|Lincoln|1981}}

==Bibliography==
* {{cite book|last=Denny|first=Capt. W. J.|title=The Diggers|url=https://books.google.com/books?id=iX7tnAEACAAJ|year=1919|publisher=Hodder and Stoughton |location=London|oclc=2306667}}
* {{cite book|last=Denny|first=William Joseph|title=A Digger at Home and Abroad|url=https://books.google.com/books?id=oGYoNAAACAAJ|year=1941|publisher=Popular Publications|location=Melbourne|oclc=3836864}}
{{Clear}}

==Notes==
{{Reflist|20em}}

==References==

===Books===
{{refbegin}}
* {{cite book|last1=Coulthard-Clark|first1=Chris|title=Soldiers in Politics: The Impact of the military on Australian Political Life and Institutions|date=1996|publisher=Allen & Unwin|location=St. Leonards, NSW|isbn=1-86448-185-4|ref=harv}}
* {{cite book|last1=Donovan|first1=P. F.|last2=Painter|first2=Alison|title=Real History: The Real Estate Institute of South Australia 1919–1989|url=https://books.google.com/books?id=huJTAAAACAAJ|year=1990|publisher=Real Estate Institute of Australia|location=Canberra, ACT|isbn=978-0-646-02056-3|ref=harv}}
* {{cite book|last=Howell|first=Peter|title=South Australia and Federation|url=https://books.google.com/books?id=Mk1RkwReoIMC|date=2002|publisher=Wakefield Press|location=Kent Town, SA|isbn=978-1-86254-549-6|ref=harv}}
* {{cite book|last1=Inglis|first1=Ken|authorlink1=Ken Inglis|last2=Brazier|first2=Jan|title=Sacred Places: War Memorials in the Australian Landscape|url=https://books.google.com/books?id=4YWbbE5KpRIC|year=2008|publisher=The Miegunyah Press|location=Carlton, VIC|isbn=978-0-522-85479-4|ref=harv}}
* {{cite book|last=Walker|first=David|title=Anxious Nation: Australia and the Rise of Asia, 1850–1939|url=https://books.google.com/books?id=spS6AAAAIAAJ|year=1999|publisher=University of Queensland Press|location=St Lucia, QLD|isbn=978-0-7022-3131-5|ref=harv}}
{{refend}}

===Papers===
{{refbegin}}
* {{cite paper|editor1-last=Brooks|editor1-first=David|editor2-last=Gill|editor2-first=Zoe|editor3-last=Weste|editor3-first=John|year=2008|title=South Australian Referenda, 1896–1991|url=http://www.parliament.sa.gov.au/Library/ReferenceShare/Documents/ResearchPaper7_2008.pdf|publisher=South Australian Parliament Research Library|location=[[Adelaide]], South Australia|issn=0816-4282|ref=harv}}
* {{cite paper|last=Jaensch|first=Dean|authorlink=Dean Jaensch|year=2007|title=History of South Australian Elections 1857–2006 House of Assembly|url=http://www.ecsa.sa.gov.au/component/edocman/?task=document.download&id=480|publisher=State Electoral Office|location=[[Rose Park, South Australia]]|volume=1|isbn=978-0-9750486-3-4|ref=harv}}
* {{cite paper|last=Macilwain|first=Margaret|year=2007|title=History of the State Electoral Office|url=http://www.ecsa.sa.gov.au/publications?task=document.download&id=211|publisher=State Electoral Office|location=[[Rose Park, South Australia]]|isbn=978-0-9750486-2-7|ref=harv}}
{{refend}}

===Newspapers===
{{refbegin}}
* {{cite news |author=|url=http://nla.gov.au/nla.news-article37502017 |title=Captain Denny MP Married |newspaper=[[The Advertiser (Adelaide)|The Advertiser]] |location=Adelaide, SA |date=16 January 1920 |accessdate=18 January 2015 |page=10|publisher=National Library of Australia|ref={{harvid|The Advertiser|1920}} }}
* {{cite news |author=|url=http://nla.gov.au/nla.news-article123217180 |title=Capt. W.J. Denny MC |newspaper=[[The Sunday Times (Sydney)|The Sunday Times]] |location=Sydney, NSW |date=3 August 1919 |accessdate=18 January 2015 |page=3|publisher=National Library of Australia|ref={{harvid|The Sunday Times|1919}} }}
* {{cite news |author=|url=http://nla.gov.au/nla.news-article162355387 |title=Councillor W. J. Denny |newspaper=[[South Australian Register|The Adelaide Observer]] |location=Adelaide, SA |date=4 February 1899 |accessdate=5 April 2015 |page=41 |publisher=National Library of Australia|ref={{harvid|The Adelaide Observer|1899}} }}
* {{cite news |author=|url=http://nla.gov.au/nla.news-article35692660 |title=Death of Mr. W.J. Denny |newspaper=[[The Advertiser (Adelaide)|The Advertiser]] |location=Adelaide, SA |date=3 May 1946 |accessdate=5 April 2015 |page=8 |publisher=National Library of Australia|ref={{harvid|The Advertiser|1946}} }}
* {{cite news |url=http://nla.gov.au/nla.news-article146694556 |title=Death of the Rev. Father R.P. Denny |author=|newspaper=[[The Narracoorte Herald]] |location=Narracoorte, SA |date=13 June 1941 |accessdate=18 January 2015 |page=3|publisher=National Library of Australia|ref={{harvid|The Narracoorte Herald|1941}} }}
* {{cite news |author=|url=http://nla.gov.au/nla.news-article166329703 |title=July |newspaper=[[South Australian Register|The Adelaide Observer]] |location=Adelaide, SA |date=9 January 1926 |accessdate=6 April 2015 |page=18 |publisher=National Library of Australia|ref={{harvid|The Adelaide Observer|1926}} }}
* {{cite news |author=|url=http://nla.gov.au/nla.news-article161096067 |title=Late "Brownie" Tripp |newspaper=The Victor Harbour Times |location=Victor Harbour, SA |date=10 March 1944 |accessdate=18 January 2015 |page=2 |publisher=National Library of Australia|ref={{harvid|The Victor Harbour Times|1944}} }}
* {{cite news |author=|url=http://nla.gov.au/nla.news-article105386564 |title=Lieutenant Denny |newspaper=[[Daily Herald (Adelaide)|The Daily Herald]] |location=Adelaide, SA |date=16 January 1917 |accessdate=6 April 2015 |page=4 |publisher=National Library of Australia|ref={{harvid|The Daily Herald|1917}} }}
* {{cite news |author=|url=http://nla.gov.au/nla.news-article164141050 |title=Military Honours |newspaper=[[South Australian Register|The Adelaide Observer]] |location=Adelaide, SA |date=17 November 1917 |accessdate=18 January 2015 |page=38|publisher=National Library of Australia|ref={{harvid|The Adelaide Observer|1917}} }}
* {{cite news |author=|url=http://nla.gov.au/nla.news-article87237137 |title=Mr. Denny on Camp Life |newspaper=[[The Chronicle (South Australia)|The Chronicle]] |location=Adelaide, SA |date=25 September 1915 |accessdate=6 April 2015 |page=36 |publisher=National Library of Australia|ref={{harvid|The Chronicle|1915}} }}
* {{cite news |author=|url=http://nla.gov.au/nla.news-article146249195 |title=Obituary |newspaper=[[The Australian Worker]] |location=Sydney, NSW |date=8 May 1946 |accessdate=18 January 2015 |page=4 |publisher=National Library of Australia|ref={{harvid|The Australian Worker|1946}} }}
* {{cite news |author=|url=http://nla.gov.au/nla.news-article5339562 |title=The University of Adelaide |newspaper=[[The Advertiser (Adelaide)|The Advertiser]] |location=Adelaide, SA |date=29 April 1912 |accessdate=18 January 2015 |page=14 |publisher=National Library of Australia|ref={{harvid|The Advertiser|1912}} }}
* {{cite news |url=http://nla.gov.au/nla.news-article124826450 |title=Walking Willy |newspaper=[[Daily Herald (Adelaide)|The Daily Herald]] |location=Adelaide, SA |date=21 September 1912 |accessdate=18 January 2015 |page=9|publisher=National Library of Australia|ref={{harvid|The Daily Herald|1912}} }}
* {{cite news |author= |url=http://nla.gov.au/nla.news-article54157378 |title=West Football Sensation: Mr. Edwards Ousts Mr. Denny. |newspaper=[[The Register News-Pictorial]] |location=Adelaide, SA |date=21 February 1930 |accessdate=19 January 2015 |page=2 |publisher=National Library of Australia|ref={{harvid|The Register News-Pictorial|1930}} }}
* {{cite news |author=|url=http://nla.gov.au/nla.news-article107258696 |title=W.J. Denny |newspaper=[[Daily Herald (Adelaide)|The Daily Herald]] |location=Adelaide, SA |date=31 March 1921 |accessdate=6 April 2015 |page=6 |publisher=National Library of Australia|ref={{harvid|The Daily Herald|1921}} }}
{{refend}}

===External links===
{{Commons category}}
{{refbegin}}
* {{cite web |url=http://www2.parliament.sa.gov.au/formermembers/Detail.aspx?pid=3617 |title=Hon William Denny |author= | date=2014 |website=Parliament of South Australia |publisher=State of South Australia |accessdate=17 January 2015|ref={{harvid|Parliament of South Australia}} }}
* {{cite web |url=http://adb.anu.edu.au/biography/denny-william-joseph-bill-5958 |title=Denny, William Joseph (Bill) (1872–1946) |last=Lincoln |first=Merrilyn |date=1981 |website=Australian Dictionary of Biography |publisher=National Centre of Biography |accessdate=17 January 2015|ref=harv}}
* {{cite web |url=http://www.mitchamcouncil.sa.gov.au/mitchamarmycamp |title=Mitcham Army Camp |author= |date=2015 |website= |publisher=City of Mitcham |accessdate=5 April 2015|ref={{harvid|City of Mitcham}} }}
* {{cite web |url=http://biography.senate.gov.au/index.php/james-vincent-ologhlin/|title=O'Loghlin, James Vincent (1852–1925) |last=Powell |first=Graeme |date=2014 |website=The Biographical Dictionary of the Australian Senate |publisher=Parliament of Australia |accessdate=5 April 2015|ref=harv}}
* {{cite web |url=https://www.awm.gov.au/people/rolls/R1619118/ |title=Recommendation of Lieutenant William Joseph DENNY for the Military Cross|author= |website= |publisher=Australian War Memorial |accessdate=17 January 2015|ref={{harvid|Australian War Memorial}} }}
* {{cite web |url=http://elections.uwa.edu.au |title=Parliament of South Australia, Assembly election 2 April 1910 |last=Sharman |first=Campbell |date=2014 |website=Australian Politics and Elections Database |publisher=University of Western Australia |accessdate=17 January 2015|ref=harv}}
{{refend}}

{{s-start}}
{{s-off}}
{{s-bef|before=[[Hermann Homburg]]}}
{{s-ttl|title=[[Attorney-General of South Australia]]|years=1910–1912}}
{{s-aft|after=[[Hermann Homburg]]}}
{{s-bef|before=[[Laurence O'Loughlin]]}}
{{s-ttl|title=Minister for the Northern Territory|years=1910}}
{{s-aft|after=Ministry abolished}}
{{s-bef|before=[[Henry Barwell]]}}
{{s-ttl|title=[[Attorney-General of South Australia]]|years=1924–1927}}
{{s-aft|after=[[Hermann Homburg]]}}
{{s-bef|before=New ministry}}
{{s-ttl|title=Minister for Housing|years=1924–1927}}
{{s-aft|after=Ministry abolished}}
{{s-bef|before=[[Hermann Homburg]]}}
{{s-ttl|title=[[Attorney-General of South Australia]]|years=1930–1933}}
{{s-aft|after=[[Shirley Jeffries]]}}
{{s-bef|before=[[George Jenkins (Australian politician)|George Jenkins]]}}
{{s-ttl|title=Minister for Railways|years=1930–1933}}
{{s-aft|after=[[Malcolm McIntosh (politician)|Malcolm McIntosh]]}}
{{s-par|au-sa}}
{{s-bef | before=[[Charles Kingston]] }}
{{s-ttl | title=Member for [[Electoral district of West Adelaide|West Adelaide]] |  years=1900–1902}}
{{s-aft | after =Electorate abolished}}
{{s-bef | before=New electorate }}
{{s-ttl | title=Member for [[Electoral district of Adelaide|Adelaide]] |  years=1902–1905}}
{{s-aft | after =[[William David Ponder]]<br />[[Ernest Roberts (Australian politician)|Ernest Roberts]]<br />[[James Zimri Sellar]]}}
{{s-bef | before=[[Lewis Cohen (mayor)|Lewis Cohen]] }}
{{s-ttl | title=Member for [[Electoral district of Adelaide|Adelaide]] |  years=1906–1933}}
{{s-aft | after =[[Doug Bardolph]]<br />[[Bob Dale (politician)|Bob Dale]]<br />[[Tom Howard (politician)|Tom Howard]]}}
{{s-end}}

{{Authority control}}

{{DEFAULTSORT:Denny, Bill}}
[[Category:Attorneys-General of South Australia]]
[[Category:Australian military personnel of World War I]]
[[Category:Australian recipients of the Military Cross]]
[[Category:Members of the South Australian House of Assembly]]
[[Category:1872 births]]
[[Category:1946 deaths]]
[[Category:South Australian local government politicians]]
[[Category:Australian Labor Party members of the Parliament of South Australia]]
[[Category:Australian Roman Catholics]]
[[Category:Burials at West Terrace Cemetery]]
[[Category:University of Adelaide Law School alumni]]