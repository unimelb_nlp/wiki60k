'''Pinky Lilani''' [[CBE]] DL [[Deputy Lieutenant]] (full name Nusrat Mehboob Lilani) is an author, motivational speaker, food expert and women’s advocate.<ref name="bbc.co.uk">{{cite web|url=http://www.bbc.co.uk/programmes/profiles/g1f5Hn25f6wrdRQyLNR4tP/pinky-lilani-obe|title=Woman's Hour Power List, Woman's Hour - Pinky Lilani OBE - BBC Radio 4|publisher=|accessdate=22 March 2017}}</ref> She is the founder and chair of several awards recognising influential women and leaders, including the annual Women of the Future Awards and the Asian Women of Achievement Awards.<ref name="brightnetwork.co.uk">{{cite web|url=http://www.brightnetwork.co.uk/blog/spotlight-inspirational-woman-pinky-lilani|title=Home|publisher=|accessdate=22 March 2017}}</ref>

Lilani was appointed [[Officer of the Order of the British Empire]] (OBE) in 2007 for services to charity and as [[Commander of the Order of the British Empire]] (CBE) in 2015 for services to women in business.<ref name="bbc.co.uk1">http://news.bbc.co.uk/1/shared/bsp/hi/pdfs/30_12_06_hons_main.pdf</ref><ref name="auto">{{cite web|url=https://www.gov.uk/government/uploads/system/uploads/attachment_data/file/434869/Queens_birthday_honours_list_2015.pdf |title=Archived copy |accessdate=June 12, 2015 |deadurl=yes |archiveurl=https://web.archive.org/web/20150614225635/https://www.gov.uk/government/uploads/system/uploads/attachment_data/file/434869/Queens_birthday_honours_list_2015.pdf |archivedate=June 14, 2015 }}</ref>

==Early life and education==
Lilani was born on 25 March 1954 in [[Calcutta]], India and studied at the Catholic [[Loreto House]] School, Calcutta. In 1974, she graduated from [[Calcutta University]] with an honours degree in Education and English.<ref name="bbc.co.uk"/> Lilani continued her studies, gaining a post graduate diploma in Social Communication Medias from the [[University of Bombay]] in 1976.

In 1978 Lilani moved to the UK where she completed a diploma in Management Studies at the Council for National Academic Awards and a Diploma in Marketing at The Institute of Marketing in 1988. Lilani is married with two sons.

==Career==
Lilani began her cookery career when she moved to the UK in 1978; she compiled her notes and recipes which eventually became her first book, ‘’Spice Magic: An Indian Culinary Adventure’’, published in 2001 and promoted it by holding culinary demonstrations in book shops.<ref name="theedge-uk.com">{{cite web|url=http://www.theedge-uk.com/view/pinky-lilani|title=Pinky Lilani - Indian Food Specialist - After Dinner Speaker|publisher=|accessdate=22 March 2017}}</ref> The cookbook also surveys the influence of history, culture, geography and religion on the food habits of India. In 2009 she released a second book, ‘’Coriander Makes the Difference’’. 
Lilani has acted as a development consultant with major food companies in Europe, including [[Sharwood’s]],<ref name="brightnetwork.co.uk"/> and has advised on Indian food products stocked by [[Safeway Inc.|Safeway]] and [[Tesco]].<ref>{{cite web|url=http://www.womanthology.co.uk/celebrating-15th-asian-women-achievement-awards-nominations-awards-co-founder-pinky-lilani-obe/|title=Pinky Lilani OBE on the Asian Women of Achievement Awards - Womanthology|date=9 April 2014|publisher=|accessdate=22 March 2017}}</ref>

In 1999, she founded the Asian Women of Achievement Awards, an annual event to acknowledge the achievements of Asian women in Britain. [[Cherie Blair|Cherie Blair QC]] is a patron of the Awards.<ref name="theedge-uk.com"/> Blair is also a patron of the Women of the Future Awards,<ref>{{cite web|url=http://cherieblair.org/charities/2009/11/women-of-the-future-awards.html|title=Cherie Blair|first=Office of Cherie|last=Blair|publisher=|accessdate=22 March 2017}}</ref> which Lilani founded in 2006 to provide a platform for female talent in the UK.
<ref>{{cite web|url=http://www.thetimes.co.uk/tto/life/fashion/mensstyle/article1857760.ece|title=And here to present the next award is . . . me!|publisher=|accessdate=22 March 2017}}</ref><ref name="bbc.co.uk"/> Women of the Future hosts annual global Summit for future women leaders and invites all shortlisted candidates to join a dedicated network.<ref>{{cite web|url=http://www.mentore.co.uk/Mentors/pinky-lilani-obe|title=Pinky Lilani OBE DL - Mentors|first=|last=mentore|publisher=|accessdate=22 March 2017}}</ref> It also incorporates an Ambassadors’ Programme to connect the award winners with school sixth-formers, providing A Level students with mentors and role models.

In 2007 Lilani founded the Women of the Future Network, a network of high potential and high achieving UK women. It provides an opportunity for talented women to come together, share experiences and build business relationships.<ref name="brightnetwork.co.uk"/>

Lilani sits on the advisory boards of Global Diversity Practice, a provider of multi-disciplinary consultancy and learning solutions, and Sapphire Partners, the first executive search firm to actively promote women.<ref>{{cite web|url=http://www.globaldiversitypractice.co.uk/team/advisory-board.|title=Advisory Board - Global Diversity Practice|publisher=|accessdate=22 March 2017}}</ref><ref>{{cite web|url=http://www.sapphirepartners.co.uk/sapphire-team-advisory.html|title=Sapphire Partners - leaders in diverse professional talent|publisher=|accessdate=22 March 2017}}</ref> Lilani is an Associate Fellow of the Saïd Business School and a [[British Red Cross]] Tiffany Circle Ambassador.<ref>{{cite web|url=http://www.redcross.org.uk/Donate-Now/Make-a-major-donation/Tiffany-Circle/Members-of-the-Tiffany-Circle|title=Members of the Tiffany Circle - British Red Cross|publisher=|accessdate=22 March 2017}}</ref>  She is a patron of Frank Water, a charity that partners with grassroots organisations in India to provide safe water<ref>{{cite web|url=http://www.frankwater.com/our-board/|title=Patrons, Trustees and Ambassadors|publisher=|accessdate=22 March 2017}}</ref>

==Honours and awards==
In 2006 Lilani was presented with a lifetime achievement award at the CBI First Women Awards.<ref>{{cite web|url=http://www.ft.com/cms/s/0/e0b05af2-f68a-11da-b09f-0000779e2340.html?siteedition=uk#axzz3h4jf17Cq|title=Achievement awards for businesswomen|first=Ruth|last=Sullivan|date=8 June 2006|publisher=|accessdate=22 March 2017|via=Financial Times}}</ref> In 2012, she was named Woman Entrepreneur of the Year at the Indus Entrepreneurs UK Gala Awards.<ref>{{cite web|url=http://www.telegraph.co.uk/finance/yourbusiness/9021193/Indus-entrepreneurs-hold-first-awards.html|title=Indus entrepreneurs hold first awards|publisher=|accessdate=22 March 2017}}</ref>

Lilani was listed on the [[BBC Radio 4]] Woman’s Hour Power List of 100 most powerful women in the UK in 2013.<ref>{{cite web|url=http://www.bbc.co.uk/programmes/articles/3J92brPmK0hskzhpTV3CrZ0/the-power-list-2013|title=Woman's Hour - The Power List 2013 - BBC Radio 4|publisher=|accessdate=22 March 2017}}</ref> In 2014, she was named as one of [[GQ]] and Editorial Intelligence’s 100 most connected women, and she was commissioned as a [[Deputy Lieutenant]] of [[Greater London]].<ref>{{cite web|url=http://www.gq-magazine.co.uk/100-most-connected/articles/2014-09/29/100-most-connected-women-2014|title=GQ and Editorial Intelligence's 100 Most Connected Women 2014|first=|last=GQ|publisher=|accessdate=22 March 2017}}</ref><ref>{{cite web|url=https://www.thegazette.co.uk/notice/2127370|title=Greater London Lieutenancy|publisher=|accessdate=22 March 2017}}</ref> In 2009, Lilani was named one of the 13 most powerful Muslim women in Britain by ''[[The Times]]'' and ''Emel'' magazine.<ref>{{cite web|url=http://www.thetimes.co.uk/tto/life/article1750293.ece|title=Meet the 13 most powerful Muslim women in Britain|publisher=|accessdate=22 March 2017}}</ref>

Lilani was appointed as [[Officer of the Order of the British Empire]] (OBE) in the 2007 New Year Honours for services to charity and as Commander of the Order of the British Empire (CBE) in the Birthday Honours 2015 for services to women in business.<ref name="bbc.co.uk1"/><ref name="auto"/>

==Books, radio and other recognition==
In 2011 Lilani spoke at TEDxMarrakesh,<ref>{{cite web|url=https://www.youtube.com/watch?v=MqfiOsRNjQU|title=TEDxMarrakesh - Pinky Lilani - Coriander Makes the Difference|first=|last=TEDx Talks|date=10 October 2011|publisher=|accessdate=22 March 2017|via=YouTube}}</ref> sharing inspirational stories and drawing on the kindnesses in our society and how coriander makes the difference. With a demonstration of bombay potatoes thrown in.

On January 8, 2017 Lilani was a guest on BBC Radio 4’s ''Desert Island Discs'',<ref>{{cite web|url=http://www.bbc.co.uk/programmes/b087pl4j|title=Pinky Lilani, Desert Island Discs - BBC Radio 4|publisher=|accessdate=22 March 2017}}</ref> giving insight into her childhood in India and starting her business in the UK.

*Lilani, Pinky. ''Soul Magic: Inspirational Insights; a Collection of Wisdom to Warm Your Heart and Lift Your Spirits.'' Purley: Development Dynamics, 2000. ISBN 9780953635405
*Lilani, Pinky. ''Coriander Makes the Difference.'' Purley: Development Dynamics, 2009.

==References==
{{reflist}}
{{authority control}}

{{DEFAULTSORT:Lilani, Pinky}}
[[Category:Cookbook writers]]
[[Category:Commanders of the Order of the British Empire]]
[[Category:1954 births]]
[[Category:Living people]]
[[Category:Writers from Kolkata]]
[[Category:British writers of Indian descent]]
[[Category:Indian emigrants to the United Kingdom]]
[[Category:University of Calcutta alumni]]
[[Category:University of Mumbai alumni]]
[[Category:BBC 100 Women]]
[[Category:Women cookbook writers]]
[[Category:Women writers of Indian descent]]