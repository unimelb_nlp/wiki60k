{{Infobox protected area
| name = Hellsgate Wilderness
| iucn_category = Ib
| photo = 
| photo_caption = 
| map = USA
| relief = 1
| map_caption = 
| location = [[Gila County, Arizona]] [[United States|USA]]
| nearest_city = [[Payson, Arizona|Payson, AZ]]
| coordinates = {{coords|34.1914275|-111.1287414|region:US|display=inline, title}}
| area = {{convert|37440|acre|km2}}
| established = 1984
| governing_body = [[United States Forest Service|U.S. Forest Service]]
}}

The '''Hellsgate Wilderness''' is a {{convert|37440|acre|km2|adj=on}} protected wilderness within the [[Tonto National Forest]] located in Gila County, Arizona, at the base of the [[Mogollon Rim]]. It was created by the U.S. Congress in 1984 and is managed by the [[U.S. Forest Service]].<ref name="WildNet">{{cite web |url=http://www.wilderness.net/index.cfm?fuse=NWPS&sec=wildView&WID=240&tab=General |title=Wilderness.net -- Hellsgate Wilderness |accessdate=2012-01-26}}</ref><ref name="NFS">{{cite web |url=http://www.fs.usda.gov/wps/portal/fsinternet/!ut/p/c4/04_SB8K8xLLM9MSSzPy8xBz9CP0os3gjAwhwtDDw9_AI8zPwhQoY6BdkOyoCAPkATlA!/?ss=110312&navtype=BROWSEBYSUBJECT&cid=FSE_003853&navid=091000000000000&pnavid=null&position=BROWSEBYSUBJECT&ttype=main&pname=Tonto%20National%20Forest-%20Home/wilderness/wilderness-hells_gate-index.shtml |title=fs.usda.gov -- Tonto National Forest |accessdate=2012-01-26}}</ref>

==Topography==
The Hellsgate Wilderness is located on the edge of the Mogollon Rim.  The elevation change in the wilderness is dramatic 3,480&nbsp;ft, with the highest point being Horse Mountain in the north-east corner.  Tonto Creek is a perennial creek that cuts a 1000&nbsp;ft deep canyon through the wilderness.  The terrain in the wilderness in broken and is subject to steep elevation changes throughout the entire wilderness area.<ref name="WildNet" />

==Wildlife==
Due to available water, the Hellsgate Wilderness is home to a myriad of animal life, including [[American black bear|black bears]], [[mountain lions]], [[mule deer]], [[coyotes]], [[gray foxes]], [[javelinas]], and, [[beavers]]. [[Trout]], [[catfish]], and [[smallmouth bass]] live in the perennial creeks.<ref name="payson">{{cite web |url=http://www.paysonrimcountry.com/MountainRecreation/NaturalLandmarks/HellsgateWilderness.aspx |title=Payson Area: The Hellsgate Wilderness |accessdate=2012-01-26}}</ref>

==History==
The southern part of the Hellsgate Wilderness was home to the native american group known as the [[Salado culture|Salado]].  This civilization flourished along the banks of the [[Salt River (Arizona)|Salt River]] in the 13th and 14th centuries. In the later part of the 14th century, the Salado went into decline and by 1400 the Hellgate Wilderness area was largely abandoned.  Sometime in the 16th century, the [[Apache]] began using this area for a hunting ground and continued to do so until they were driven out by ranchers and miners of European descent.<ref name="payson" />

In 1927, a [[Ryan Brougham]] airplane flown by [[Martin Jensen (aviator)|Martin Jensen]], carrying MGM's [[Leo the Lion (MGM)|Leo the Lion]], was forced to make an emergency landing in a box canyon in the Hellsgate Wilderness.  Both pilot and lion survived with no injuries and the canyon was named Leo Canyon after the incident. The wreckage of the plane was left in the canyon until 1991, when a historic airplane enthusiast named Scott Gifford found it and removed it from the canyon via helicopter.  He plans to restore the plane to airworthiness.<ref name="lion">{{cite web |url=http://www.airspacemag.com/flight-today/brougham.html?c=y&page=1 |title=A Brougham Fit for a King |accessdate=2012-01-26}}</ref>

==Recreation==
The Hellgsate Wilderness contains many trails for hiking as well as camping spots. However, usage of this area is light as trailheads are difficult to reach without 4-wheel drive capability and most trails are rated as challenging.<ref name="GORP">{{cite web |url=http://www.gorp.com/parks-guide/hellsgate-wilderness-outdoor-pp2-guide-cid401694.html |title=GORP.com Hellsgate Wilderness |accessdate=2012-01-26}}</ref>

During the summer months, the wilderness's perennial creeks attract a small number of anglers.<ref name="WildNet" />

== References ==
{{Reflist}}

==Further reading==
* [https://books.google.com/books?id=leH8bUGVQ8oC&pg=PA158&dq=Hellsgate+Wilderness&hl=en&sa=X&ei=SzkpUd3DBKOpiAKop4DwAw&ved=0CDAQ6AEwAA#v=onepage&q=Hellsgate%20Wilderness&f=false Hiking Arizona: A Guide to Arizona's Greatest Hiking Adventures - Bruce Gibbs - Google Books<!-- Bot generated title -->]
* [https://books.google.com/books?id=xUjZSmy6uDkC&pg=PA117&dq=Hellsgate+Wilderness&hl=en&sa=X&ei=SzkpUd3DBKOpiAKop4DwAw&ved=0CDYQ6AEwAQ#v=onepage&q=Hellsgate%20Wilderness&f=false Guide to Arizona's Wilderness Areas - Tom Dollar - Google Books<!-- Bot generated title -->]
* [https://books.google.com/books?id=Qvx1ag2ula4C&pg=PA161&dq=Hellsgate+Wilderness&hl=en&sa=X&ei=SzkpUd3DBKOpiAKop4DwAw&ved=0CDsQ6AEwAg#v=onepage&q=Hellsgate%20Wilderness&f=false Flyfisher's Guide to Arizona - Will Jordan - Google Books<!-- Bot generated title -->]
* [https://books.google.com/books?id=TN2Vv9S7VzgC&pg=PA131&dq=Hellsgate+Wilderness&hl=en&sa=X&ei=SzkpUd3DBKOpiAKop4DwAw&ved=0CEYQ6AEwBA#v=onepage&q=Hellsgate%20Wilderness&f=false Exploring Arizona's Wild Areas: A Guide for Hikers, Backpackers, Climbers ... - Scott S. Warren - Google Books<!-- Bot generated title -->]

[[Category:Articles created via the Article Wizard]]
[[Category:Tonto National Forest| ]]
[[Category:Mogollon Rim]]
[[Category:National Forests of Arizona]]