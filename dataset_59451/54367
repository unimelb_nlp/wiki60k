{{refimprove|date=May 2016}}
{{Use dmy dates|date=October 2012}}
{{Infobox Australian place| type = suburb
| name     = Largs North
| city     = Adelaide
| state    = sa
| image    = 
| caption  = 
| lga      = [[City of Port Adelaide Enfield]]<ref name=PLB/>
| postcode = 5016
| est      = 1945<ref name=PLB>{{cite web|title=Search result for "Largs North (SUB)" (Record no SA0046447) with the following layers being selected - “Suburbs and Localities” |url=http://maps.sa.gov.au/plb/#|work=Property Location Browser|publisher=Government of South Australia|date= |accessdate=11 May 2016}}</ref>	
| pop      = 
| area     = 
| stategov = [[Electoral district of Port Adelaide|Port Adelaide]]<ref name=ECSA>{{cite web|title= Port Adelaide electorate boundaries as of 2014 |url=http://www.ecsa.sa.gov.au/component/edocman/?task=document.download&id=1047&Itemid=0|publisher=Electoral Commission SA|accessdate=11 May 2016}}</ref>
| fedgov   = [[Division of Port Adelaide|Port Adelaide]]<ref name=AEC>{{cite web|title=Federal electoral division of Port Adelaide |url=http://www.aec.gov.au/profiles/sa/files/2011/2011-aec-a4-map-sa-port-adelaide.pdf|publisher=Australian Electoral Commission|accessdate=11 May 2016}}</ref>
| latd= 34.816690
| longd= 138.500650
| near-nw  = ''Gulf St Vincent''
| near-n   = [[Taperoo, South Australia|Taperoo]]
| near-ne  = Port Adelaide
| near-w   = ''[[Gulf St Vincent]]''
| near-e   = [[Port Adelaide, South Australia|Port Adelaide]]
| near-sw  = ''Gulf St Vincent''
| near-s   = [[Largs Bay, South Australia|Largs Bay]]
| near-se  = Port Adelaide
| dist1    = 17
| location1= [[Adelaide city centre]]
|footnotes=Adjoining suburbs<ref name=PLB/>
}}

'''Largs North''' is a [[suburb]] in the Australian state of [[South Australia]] located on the [[Lefevre Peninsula]] in the west of [[Adelaide]] about {{convert|17|km}} northwest of the [[Adelaide city centre]]. 

==Description==
Largs North is bounded to the north by the suburb of [[Taperoo, South Australia|Taperoo]] at Strathfield Terrace, to the south by the suburb of [[Largs Bay, South Australia|Largs Bay]] at Walcot and Warwick Streets and in the west and east by Gulf St Vincent and the suburb of [[Port Adelaide, South Australia|Port Adelaide]] with the boundary in the middle of the Port River respectively.

Largs North is essentially a residential suburb, with a minor harbourside presence on the eastern side of the suburb.

==Facilities==
The local high school is Ocean View College Gedville Campus, in nearby [[Taperoo, South Australia|Taperoo]]. There is a nursing home on Victoria Road and an RSL club on Carnarvon Terrace. Largs North Oval overlooking Victoria Road is the main outdoor recreational reserve in the suburb.

The eastern side of the suburb, by the Port riverside is known as Snowden Beach and is the location of a [[sulphuric acid]] works, which is connected to a freight railway. There is a swinging basin in the area, and it is the site of Port River Sailing Club and Port Adelaide Rowing Club.

==Transport==
The 157 and 333 buses service Military Road, while the 150 services Carnarvon Terrace. The suburb is serviced by two train stations on the [[Outer Harbor railway line]], [[Draper railway station]] and [[Largs North railway station]].

==Governance==
Largs North is located within the federal [[division of Port Adelaide]], the state [[electoral district of Port Adelaide]] and the local government area of the [[City of Port Adelaide Enfield]].<ref name=AEC/><ref name=ECSA/><ref name=PLB/>

==References==
{{reflist}}

{{City of Port Adelaide Enfield suburbs}}

[[Category:Suburbs of Adelaide]]
[[Category:Lefevre Peninsula]]