'''Inflation accounting''' is a term describing a range of accounting models designed to correct problems arising from [[historical cost]] [[accounting]] in the presence of high [[inflation]] and [[hyperinflation]].<ref>[http://financial-dictionary.thefreedictionary.com/inflation+accounting Wall Street Words: An A to Z Guide to Investment Terms for Today's Investor], David L. Scott.</ref> Inflation accounting is used in countries experiencing high inflation or [[hyperinflation]].<ref>[http://hansard.millbanksystems.com/lords/1975/nov/26/inflation-accounting-sandilands-report-1] Inflation Accounting: Sandilands Report - May I also ask 297 him to assure us that, when this new system of current cost accounting is introduced, these new realistic figure will be used for the purposes of company taxation, and not the historic cost figures, which are totally meaningless at a time of '''high inflation'''.</ref> For example, in countries experiencing hyperinflation the [[International Accounting Standards Board]] requires corporations to implement financial capital maintenance in units of constant purchasing power in terms of the monthly published Consumer Price Index. This does not result in capital maintenance in units of constant purchasing power since that can only be achieved in terms of a daily index.

==Historical cost basis in financial statements==

[[Fair value]] accounting (also called replacement cost accounting or current cost accounting) was widely used in the  19th and early 20th centuries, but historical cost accounting became more widespread after values overstated during the 1920s were reversed during the [[Great Depression]] of the 1930s. Most principles of [[historical cost]] accounting were developed after the [[Wall Street Crash of 1929]], including the presumption of a stable currency.<ref>{{cite book |last=Epstein |first=Barry J. |author2=Eva K. Jermakowicz |title=Interpretation and Application of International Financial Reporting Standards |publisher=[[John Wiley & Sons]] |year=2007 |page=965 |isbn=978-0-471-79823-1}}</ref>

===Measuring unit principle===
<blockquote>Under a historical cost-based system of accounting, inflation leads to two basic problems. First, many of the historical numbers appearing on financial statements are not economically relevant because prices have changed since they were incurred. Second, since the numbers on financial statements represent dollars expended at different points of time and, in turn, embody different amounts of purchasing power, they are simply not additive. Hence, adding cash of $10,000 held on December 31, 2002, with $10,000 representing the cost of land acquired in 1955 (when the price level was significantly lower) is a dubious operation because of the significantly different amount of purchasing power represented by the two numbers.<ref>{{cite book |last = Wolk |first = Harry I. |author2=James L. Dodd |author3=Michael G. Tearney |title = Accounting Theory: Conceptual Issues in a Political and Economic Environment, 6th ed |publisher = South-Western |year = 2004 |pages = 448 |isbn = 0-324-18623-1 }}</ref> </blockquote>

By adding dollar amounts that represent different amounts of purchasing power, the resulting sum is misleading, as one would be adding 10,000 dollars to 10,000 Euros to get a total of 20,000.  Likewise subtracting dollar amounts that represent different amounts of purchasing power may result in an apparent [[capital gain]] which is actually a capital loss.  If a building purchased in 1970 for $20,000 is sold in 2006 for $200,000 when its replacement cost is $300,000, the apparent gain of $180,000 is illusory.

===Misleading reporting under historical cost accounting===
"In most countries, primary financial statements are prepared on the historical cost basis of accounting without regard either to changes in the general level of prices or to increases in specific prices of assets held, except to the extent that property, plant and equipment and investments may be revalued."[5]

Ignoring general price level changes in financial reporting creates distortions in financial statements such as<ref>Epstein, pp. 966-997.</ref>
* reported profits may exceed the earnings that could be distributed to shareholders without impairing the company's ongoing operations
* the asset values for inventory, equipment and plant do not reflect their economic value to the business
* future earnings are not easily projected from historical earnings
* the impact of price changes on monetary assets and liabilities is not clear
* future capital needs are difficult to forecast and may lead to increased leverage, which increases the business's risk
* when real economic performance is distorted, these distortions lead to social and political consequences that damage businesses (examples: poor tax policies and public misconceptions regarding corporate behavior)

==History of inflation accounting==
Accountants in the United Kingdom and the United States have discussed the effect of inflation on financial statements since the early 1900s, beginning with [[index number]] theory and [[purchasing power]]. [[Irving Fisher]]'s 1911 book ''The Purchasing Power of Money'' was used as a source by Henry W. Sweeney in his 1936 book ''Stabilized Accounting'', which was about [[Constant Purchasing Power Accounting]].  This model by Sweeney was used by The [[American Institute of Certified Public Accountants]] for their 1963 research study (ARS6) ''Reporting the Financial Effects of Price-Level Changes'', and later used by the [[Accounting Principles Board]] (USA), the Financial Standards Board (USA), and the Accounting Standards Steering Committee (UK). Sweeney advocated using a price index that covers everything in the [[gross national product]]. In March 1979, the [[Financial Accounting Standards Board]] (FASB) wrote ''Constant Dollar Accounting'', which advocated using the [[Consumer price index#United States|Consumer Price Index for All Urban Consumers]] (CPI-U) to adjust accounts because it is calculated every month.<ref>{{cite book |author=Whittington, Geoffrey |title=Inflation accounting: an introduction to the debate |publisher=Cambridge University Press |location=Cambridge, UK |year=1983 |pages=66 |isbn=0-521-27055-3 |oclc= |doi=}}</ref>

During the [[Great Depression]], some corporations restated their financial statements to reflect inflation. At times during the past 50 years,{{When|date=January 2015}} standard-setting organizations have encouraged companies to supplement cost-based financial statements with price-level adjusted statements. During a period of high inflation in the 1970s, the FASB was reviewing a draft proposal for price-level adjusted statements when the [[Securities and Exchange Commission]] (SEC) issued ASR 190, which required approximately 1,000 of the largest US corporations to provide supplemental information based on [[replacement cost]]. The FASB withdrew the draft proposal.<ref>Wolk pp 450-455</ref>

IAS 29 ''Financial Reporting in Hyperinflationary Economies'' is the International Accounting Standard Board's inflation accounting model authorized in April 1989. It is the inflation accounting model required in International Financial Reporting Standards implemented in 174 countries.

==Models==
Inflation accounting is not fair value accounting. Inflation accounting, also called '''price level accounting''', is similar to converting financial statements into another currency using an [[exchange rate]]. Under some (not all) inflation accounting models, historical costs are converted to price-level adjusted costs using general or specific price indexes.<ref>Epstein, pp. 968-969.</ref>

'''Income statement general price-level adjustment example'''<ref>Wolk p. 5.</ref>
:On the income statement, depreciation is adjusted for changes in general price levels based on a general price index.
{| class="wikitable"
|-
! 
! 2001
! 2002
! 2003
! Total
|-
| Revenue ||33,000 ||36,302 ||39,931 ||109,233
|-
| Depreciation ||30,000 ||31,500 (a) ||33,000 (b) ||94,500
|-
| Operating income ||3,000 ||4,802 ||6,931 ||14,733
|-
| Purchasing power loss ||- ||1,500 (c) ||3,000 (d) ||4,500
|-
| Net income ||3,000 ||3,302 ||3,931 ||10,233
|}
:(a) 30,000 x 105/100 = 31,500
:(b) 30,000 x 110/100 = 33,000
:(c) (30,000 x 105/100) - 30,000 = 1,500
:(d) (63,000 x 110/105) - 63,000 = 3,000

===Constant-dollar accounting===
Constant-dollar accounting is an accounting model that converts nonmonetary assets and equities from historical dollars to current dollars using a general price index. This is similar to a currency conversion from old dollars to new dollars. Monetary items are not adjusted, so they gain or lose purchasing power. There are no holding gains or losses recognized in converting values.<ref>Epstein, p. 962.</ref>

==International standard for hyperinflationary accounting==
The International Accounting Standards Board defines [[hyperinflation]] in IAS 29 as: "the cumulative inflation rate over three years is approaching, or exceeds, 100%." <ref>{{cite book |last = International Accounting Standards Committee |first =  |title = International Accounting Standard 1995|publisher = London, International Accounting Standards Committee|year = 1995|pages = Par 3 (e) P. 502 |isbn = 0-905625-26-9 }}</ref>

Companies are required to restate their historical cost financial reports in terms of the period end hyperinflation rate in order to make these financial reports more meaningful.<ref>{{cite book |last = International Accounting Standards Committee |first =  |title = International Accounting Standard 1995|publisher = London, International Accounting Standards Committee|year = 1995|pages = Par 8 P. 503 |isbn = 0-905625-26-9 }}</ref></blockquote> <ref>{{cite book |last = International Accounting Standards Board|first = |title = IAS 29 Financial Reporting in Hyperinflationary Economies  |publisher = IASB |pages = http://www.iasb.org/NR/rdonlyres/C2563EF2-89A8-4ED7-82A3-E31EDF33E428/0/IAS29.pdf   |isbn =  }}</ref></blockquote> <ref>{{cite book |last = Deloitte|first = |title =  FINANCIAL REPORTING IN HYPERINFLATIONARY ECONOMIES |publisher =Deloitte, IAS Plus |pages =   http://www.iasplus.com/standard/ias29.htm  |isbn =  }}</ref>

The restatement of historical cost financial statements in terms of IAS 29 does not signify the abolishment of the historical cost model. This is confirmed by PricewaterhouseCoopers: "Inflation-adjusted financial statements are an extension to, not a departure from, historical cost accounting." <ref>{{cite book |last = PricewaterhouseCoopers |first =  |title = International Financial Reporting Standards Financial Reporting in Hyperinflationary Economies – Understanding IAS 29|publisher = PricewaterhouseCoopers|pages = http://www.pwc.com/gx/eng/about/svcs/corporatereporting/IAS29Publication06.pdf  |isbn =  }}</ref>

IAS 29 ''Financial Reporting in Hyperinflationary Economies'' is the IASB´s inflation accounting model authorized in April 1989. IAS 29 requires the implementation of financial capital maintenance in units of constant purchasing power in terms of the monthly published CPI. That requirement does not result in actual capital maintenance in units of constant purchasing power since that can only be achieved with following all changes in the general price level; i.e., at least daily changes. The ineffectiveness of IAS 29 was clearly demonstrated with its implementation during the final 8 years of hyperinflation in Zimbabwe. IAS 29 had no positive effect in Zimbabwe: the Zimbabwean economy imploded on 20 November 2008 with full implementation of IAS 29. The IASB has not yet changed IAS 29 to require daily indexing.

==See also==
*[[Philosophy of Accounting]]
*[[Constant Purchasing Power Accounting]]
*[[Real versus nominal value (economics)]]

==Notes and references==
{{Reflist}}

[[Category:Accounting systems]]
[[Category:Inflation]]