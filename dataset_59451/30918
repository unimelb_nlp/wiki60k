{{taxobox
 | name                = Eurasian treecreeper
 | status              = LC
 | status_system       = IUCN3.1
 | status_ref          = <ref name=IUCN>{{IUCN|id=22735060 |title=''Certhia familiaris'' |assessor=BirdLife International |assessor-link=BirdLife International |version=2013.2 |year=2012 |accessdate=26 November 2013}}</ref>
 | image               = Certhia familiaris 01.jpg
 | image_caption       = Subspecies ''C. f. macrodactyla'' or ''C. f. familiaris''<br/>
[[File:Eurasian Treecreeper (Certhia familiaris) (W1CDR0001463 BD5).ogg|thumb|center|Contact calls recorded in [[Surrey]], England]]
 | regnum              = [[Animal]]ia
 | phylum              = [[Chordate|Chordata]]
 | classis             = [[bird|Aves]]
 | ordo                = [[passerine|Passeriformes]]
 | familia             = [[treecreeper|Certhiidae]]
 | genus               = ''[[treecreeper|Certhia]]''
 | species             = '''''C. familiaris'''''
 | binomial            = ''Certhia familiaris''
 | binomial_authority  = [[Carl Linnaeus|Linnaeus]], [[10th edition of Systema Naturae|1758]]
 | range_map           = Treecreepermap.png
 | range_map_width     = 250px
 | range_map_caption   = Green – resident all year<br/>
Blue – winter visitor only<br/>Magenta – subspecies now often split as <br/>Hodgson's treecreeper
}}

The '''Eurasian treecreeper''' or '''common treecreeper''' (''Certhia familiaris'') is a small [[passerine]] [[bird]] also known in the [[British Isles]], where it is the only living member of its genus, simply as '''treecreeper'''. It is similar to other [[Certhia|treecreeper]]s, and has a curved bill, patterned brown upperparts, whitish underparts, and long stiff tail feathers which help it creep up tree trunks. It can be most easily distinguished from the similar [[short-toed treecreeper]], which shares much of its [[Europe]]an range, by its different song.

The Eurasian treecreeper has nine or more subspecies which breed in different parts of its range in temperate [[Eurasia]]. This species is found in woodlands of all kinds, but where it overlaps with the short-toed treecreeper in western Europe it is more likely to be found in [[conifer]]ous forests or at higher altitudes. It nests in tree crevices or behind bark flakes, and favours introduced [[Sequoiadendron giganteum|giant sequoia]] as nest sites where they are available. The female typically lays five or six pink-speckled white eggs in the lined nest, but eggs and chicks are vulnerable to attack by woodpeckers and mammals, including squirrels.

The Eurasian treecreeper is [[insectivore|insectivorous]] and climbs up tree trunks like a mouse, to search for insects which it picks from crevices in the bark with its fine curved bill. It then flies to the base of another tree with a distinctive erratic flight. This bird is solitary in winter, but may form communal roosts in cold weather.

==Description==
Similar in appearance, all treecreepers are small birds with streaked and spotted brown upperparts, rufous rumps and whitish underparts. They have long decurved bills, and long rigid tail feathers that provide support as they creep up tree trunks looking for insects.<ref name= Harrap/>

The Eurasian treecreeper is {{convert|12.5|cm|in|abbr=on}} long and weighs 7.0&ndash;12.9&nbsp;[[gram|g]] (0.25&ndash;0.46&nbsp;[[ounce|oz]]). It has warm brown upperparts intricately patterned with black, buff and white, and a plain brown tail. Its belly, flanks and vent area are tinged with buff. The sexes are similar, but the juvenile has duller upperparts than the adult, and its underparts are dull white with dark fine spotting on the flanks.<ref name= Harrap/>

The contact call is a very quiet, thin and high-pitched ''sit'', but the most distinctive call is a penetrating ''tsree'', with a [[vibrato]] quality, sometimes repeated as a series of notes. The male's song begins with ''srrih, srrih'' followed in turn by a few twittering notes, a longer descending ripple, and a whistle that falls and then rises.<ref name= Harrap/>

The range of the Eurasian treecreeper overlaps with that of several other treecreepers, which can present local identification problems. In Europe, the Eurasian treecreeper shares much of its range with the short-toed treecreeper. Compared to that species, it is whiter below, warmer and more spotted above, and has a whiter [[supercilium]] and slightly shorter bill. Visual identification, even in the hand, may be impossible for poorly marked birds. A singing treecreeper is usually identifiable, since short-toed treecreeper has a distinctive series of evenly spaced notes sounding quite different from the song of Eurasian treecreeper; however, both species have been known to sing the other's song.<ref name= Harrap/>

Three Himalayan subspecies of Eurasian treecreeper are now sometimes given full species status as [[Hodgson's treecreeper]], for example by [[BirdLife International]],<ref name =Birdlife >{{cite web|title= Hodgson's Treecreeper ''Certhia hodgsoni'' |work= BirdLife Species Factsheet  |url=http://www.birdlife.org/datazone/search/species_search.html?action=SpcHTMDetails.asp&sid=32349&m=0 |publisher=[[BirdLife International]] |accessdate=2008-05-27}}</ref> but if they are retained as subspecies of Eurasian, they have to be distinguished from three other South Asian treecreepers. The plain tail of Eurasian treecreeper differentiates it from [[bar-tailed treecreeper]], which has a distinctive barred tail pattern, and its white throat is an obvious difference from [[brown-throated treecreeper]]. [[Rusty-flanked treecreeper]] is more difficult to separate from Eurasian, but has more contrasting cinnamon, rather than buff, flanks.<ref name= Harrap/>

The North American [[brown creeper]] has never been recorded in Europe, but an autumn [[vagrancy (biology)|vagrant]] would be difficult to identify, since it would not be singing, and the American species' call is much like that of Eurasian treecreeper. In appearance, brown creeper is more like short-toed than Eurasian, but a vagrant might still not be possible to identify with certainty given the similarities between the three species.<ref name= Harrap/>

==Taxonomy==
[[File:Boomkruiper1reversed.jpg|[[Short-toed treecreeper]], a confusion species in Europe|thumb]]
The Eurasian treecreeper was first described under its current scientific name by [[Carl Linnaeus|Linnaeus]] in his ''[[10th edition of Systema Naturae|Systema naturae]]'' in 1758.<ref>{{la icon}} {{cite book | last=Linnaeus | first=C | authorlink=Carl Linnaeus | title=Systema naturae per regna tria naturae, secundum classes, ordines, genera, species, cum characteribus, differentiis, synonymis, locis. Tomus I. Editio decima, reformata. | publisher=Holmiae. (Laurentii Salvii). | year=1758| quote =  C. supra grisea, subtus alba, remigibus fuscis, decemris macula alba. |pages=118}}</ref> The binomial name is derived from [[Ancient Greek]] ''kerthios'', a small tree-dwelling bird described by [[Aristotle]] and others, and [[Latin]] ''familiaris'', familiar or common.<ref name = BTO>{{cite web|title= Treecreeper ''Certhia familiaris'' [Linnaeus, 1758] |work=BirdFacts  |url= http://blx1.bto.org/birdfacts/results/bob14860.htm  |publisher= [[British Trust for Ornithology]] (BTO) |accessdate=2008-05-20}}</ref>

This species is one of a group of very similar [[Certhia|typical treecreeper]] species, all placed in the single genus ''[[Certhia]]''. Eight species are currently recognised, in two [[evolution]]ary lineages: a  [[Holarctic]] radiation, and a southern Asian group. The Holarctic group has a more warbling song, always (except in ''C. familiaris'' from [[China]]) starting or ending with a shrill ''sreeh''. Species in the southern group, in contrast, have a faster-paced trill without the ''sreeh'' sound. All the species have distinctive vocalizations and some subspecies have been elevated to species on the basis of their calls.<ref>{{cite journal|first=Dieter Thomas|last=Tietze |author2=Martens, Jochen |author3=Sun, Yue-Hua |author4=Paeckert, Martin |year=2008|title=Evolutionary history of treecreeper vocalisations(Aves: Certhia)|journal=Organisms, Diversity & Evolution|volume=8|pages=305–324|doi=10.1016/j.ode.2008.05.001}}</ref> [[File:Hodgson's Treecreeper I IMG 3315.jpg|thumb|left|Hodgson's treecreeper, probably ''C. h. mandelli'', formerly considered to be a subspecies of Eurasian treecreeper]]

The Eurasian treecreeper belongs to the northern group, along with the [[North America]]n brown creeper, ''C. americana'', the short-toed treecreeper, ''C. brachydactyla'', of western Eurasia, and, if it is considered a separate species, Hodgson's treecreeper, ''C. hodgsoni'', from the southern rim of the [[Himalayas]].<ref name=Tietze>{{cite journal|last=Tietze |first=Dieter Thomas |author2=Martens, Jochen |author3=Sun, Yue-Hua |year= 2006|title= Molecular phylogeny of treecreepers (''Certhia'') detects hidden diversity|journal= [[Ibis (journal)|Ibis]] |volume=148 |issue= 3|pages= 477&ndash;488  |doi=10.1111/j.1474-919X.2006.00547.x}}</ref>

The brown creeper has sometimes been considered to be a subspecies of Eurasian treecreeper, but has closer affinities to short-toed treecreeper, and is normally now treated as a full species.<ref name= Harrap>{{cite book | last = Harrap | first =Simon |author2=Quinn, David |title = Tits, Nuthatches and Treecreepers | year = 1996 | publisher = Christopher Helm | pages = 177&ndash;195|isbn = 0-7136-3964-4}}</ref> Hodgson's treecreeper is a more recent proposed split following studies of its [[cytochrome b|cytochrome ''b'']] [[mtDNA]] [[DNA sequence|sequence]] and song structure that indicate that it may well be a distinct species from ''C. familiaris''.<ref name=Tietze/>

There are nine to twelve subspecies of Eurasian treecreeper, depending on the taxonomic view taken, which are all very similar and often interbreed in areas where their ranges overlap. There is a general [[Cline (population genetics)|cline]] in appearance from west to east across Eurasia, with subspecies becoming greyer above and whiter below, but this trend reverses east of the [[Amur River]]. The currently recognised subspecies are as follows:<ref name= Harrap/>

{| width=72% class="wikitable"
!width=18% | Subspecies
!width=24% | Range
!width=30% | Notes<ref name= Harrap/>
|-
| ''C. f. britannica'' 
|[[Great Britain]] and [[Ireland]]
|Irish treecreepers, slightly darker than British ones, have sometimes been given subspecific status
|-
| ''C. f. macrodactyla'' 
| Western  [[Europe]]
|Paler above and whiter below than ''C. f. britannica''
|-
| ''C. f. corsa'' 
|[[Corsica]]
|Buff-tinged underparts and more contrasted upperparts than ''C. f. macrodactyla'' 
|-
|''C. f. familiaris''
|[[Scandinavia]] and eastern Europe east to western [[Siberia]]
|[[Subspecies#Nomenclature|Nominate subspecies]]. Paler above than ''C. f. macrodactyla'', white underparts
|-
| ''C. f. daurica'' 
|Eastern Siberia, northern [[Mongolia]]
|Paler and greyer than the nominate subspecies
|-
| ''C. f. orientalis'' 
||Amur basin, northeast China, [[Korea]] and Hokkaido, Japan
|Similar to nominate, but with stronger streaking above
|-
| ''C. f. japonica'' 
|[[Japan]] south of Hokkaido
|Darker and more rufous than ''C. f. daurica''
|-
| ''C. f. persica'' 
|The [[Crimea]] and [[Turkey]] east to northern [[Iran]]
|Duller and less rufous than the nominate form
|-
| ''C. f. tianchanica'' 
|Northwestern China and adjacent regions of the former [[USSR]] 
|Paler and more rufous than nominate subspecies
|-
|''C. f. hodgsoni''
| Western Himalayas of, [[Kashmir]] 
| Often treated as a full species, Hodgson's treecreeper, ''C. hodgsonii.''<ref name=Tietze/>
|-
|''C. f. mandellii''
| Eastern Himalayas of India, [[Nepal]]
| Often now treated as a subspecies of Hodgson's treecreeper
|-
|''C. f. khamensis''
| China, [[Sichuan]]
|Often now treated as a subspecies of Hodgson's treecreeper
|}

==Distribution and habitat==
[[File:474px-Certhia familiariscroppedmirror.jpg|thumb|Central European bird feeding on a trunk]]
The Eurasian treecreeper is the most widespread member of its genus, breeding in temperate woodlands across Eurasia from [[Ireland]] to [[Japan]]. It prefers mature trees, and in most of Europe, where it shares its range with short-toed treecreeper, it tends to be found mainly in [[conifer]]ous forest, especially [[spruce]] and [[fir]]. However, where it is the only treecreeper, as in European [[Russia]],<ref name= Harrap/> or the [[British Isles]],<ref name= BTO/> it frequents broadleaved or mixed woodland in preference to conifers.

The Eurasian treecreeper breeds down to sea level in the north of its range, but tends to be a highland species further south. In the [[Pyrenees]] it breeds above {{convert|1,370|m|ft|abbr=off}}, in China from {{convert|400|-|2,100|m|ft}} and in southern Japan from {{convert|1,065|-|2,135|m|ft}}.<ref name= Harrap/> The breeding areas have July [[isotherm (contour line)|isotherm]]s between 14&ndash;16&nbsp;°C and {{convert|23|-|24|C|F}} and 72&ndash;73&nbsp;°F).<ref name=BWP>{{cite book| editor1-last = Snow | editor1-first = David |editor2-last=Perrins |editor2-first=Christopher M | title = The Birds of the Western Palearctic concise edition (2 volumes) | publisher = Oxford University Press |year = 1998| location =Oxford | isbn = 0-19-854099-X}} 1411&ndash;1416</ref>

The Eurasian treecreeper is [[bird migration|non-migratory]] in the milder west and south of its breeding range, but some northern birds move south in winter, and individuals breeding on mountains may descend to a lower altitude in winter. Winter movements and post-breeding dispersal may lead to vagrancy outside the normal range. Wintering migrants of the Asian subspecies have been recorded in [[South Korea]] and China, and the nominate form has been recorded west of its breeding range as far as [[Orkney]], [[Scotland]]. The Eurasian treecreeper has also occurred as a vagrant to the [[Channel Islands]] (where the short-toed is the resident species), [[Majorca]] and the [[Faroe Islands]].<ref name= Harrap/>

===Status===
This species has an extensive range of about 10&nbsp;million km<sup>2</sup> (3.8&nbsp;million square miles). It has a large population, including an estimated 11&ndash;20&nbsp;million individuals in Europe alone. Population trends have not been quantified, but the species is not believed to approach the thresholds for the population decline criterion of the [[IUCN Red List]] (declining more than 30% in ten years or three generations). For these reasons, the species is evaluated as Least Concern.<ref name=IUCN/>

It is common through much of its range, but in the northernmost areas it is rare, since it is vulnerable to hard winters, especially if its feeding is disrupted by an ice glaze on the trees or freezing rain. It is also uncommon in Turkey and the [[Caucasus]]. In the west of its range it has spread to the [[Outer Hebrides]] in Scotland, pushed further north in [[Norway]], and first bred in the [[Netherlands]] in 1993.<ref name = Harrap/>

==Behaviour==

===Breeding===
[[File:Certhia familiaris MWNH 1434.JPG|left|thumb|Eggs, Collection [[Museum Wiesbaden]]]]
[[File:Sequoi.westonbirt.600pix.jpg|thumb|Introduced redwoods are the preferred nesting trees where present.]]
The Eurasian treecreeper breeds from the age of one year, nesting in tree crevices or behind bark flakes.<ref name =BTO/> Where present, the introduced North American [[Sequoiadendron giganteum|giant sequoia]] is a favourite nesting tree, since a nest cavity can be easily hollowed out in its soft bark.<ref name= Cocker >{{cite book | last = Cocker | first = Mark |author2=Mabey, Richard  |title = Birds Britannica | year = 2005 |location=London | publisher = Chatto & Windus | isbn = 0-7011-6907-9}} 394</ref> Crevices in buildings or walls are sometimes used, and artificial nest boxes or flaps may be preferred in coniferous woodland.<ref name=Harrap/> The nest has a base of twigs, pine needles, grass or bark, and a lining of finer material such as feathers, wool, moss, [[lichen]] or spider web.

In Europe, the typical clutch of five&ndash;six eggs is laid between March and June, but in Japan three&ndash;five eggs are laid from May to July.<ref name=Harrap/> The eggs are white with very fine pinkish speckles mainly at the broad end,<ref name=Harrap/> measure <!--CheckU-->{{convert|16|x|12|mm|in|abbr=on}} and weigh {{convert|1.2|g|oz|abbr=on}} of which 6% is shell.<ref name =BTO/> The eggs are incubated by the female alone for 13–17&nbsp;days until the [[altricial]] downy chicks hatch; they are then fed by both parents, but brooded by the female alone, for a further 15–17&nbsp;days to fledging.<ref name =BTO/> Juveniles return to the nest for a few nights after [[fledging]]. About 20% of pairs, mainly in the south and west, raise a second brood.<ref name=Harrap/>

Predators of treecreeper nests and young include the [[great spotted woodpecker]], [[red squirrel]], and small [[mustelidae|mustelids]], and predation is about three times higher in fragmented landscapes than in solid blocks of woodland (32.4% against 12.0% in less fragmented woodlands). The predation rate increases with the amount of forest edge close to a nest site, and also the presence of nearby agricultural land, in both cases probably because of a higher degree of mustelid predation.<ref name= Huhta>{{cite journal|last= Huhta |first= Esa |author2=Aho, Teija |author3=Jäntti, Ari |author4=Suorsa, Petri |author5=Kuitunen, Markku |author6=Nikula, Ari |author7= Hakkarainen Harri  |date=February 2004 |title= Forest Fragmentation Increases Nest Predation in the Eurasian Treecreeper |journal= Conservation Biology|volume=18 |issue=1 |pages=148&ndash;155 | doi=10.1111/j.1523-1739.2004.00270.x}}</ref> This species is parasitised in the nest by the [[moorhen flea]], ''Dasypsyllus gallinulae''.<ref name = Rothschild>{{cite book |title= Fleas, Flukes and Cuckoos. A study of bird parasites |author= Rothschild, Miriam |author-link= Miriam Rothschild |author2= Clay, Theresa |year= 1953 |publisher= Collins |location= London |isbn= |pages= 113 |url= http://ia331318.us.archive.org/1/items/fleasflukescucko017900mbp/fleasflukescucko017900mbp.pdf }}</ref> The juvenile survival rate of this species is unknown, but 47.7% of adults survive each year. The typical lifespan is two years, but the maximum recorded age is eight&nbsp;years and ten&nbsp;months.<ref name =BTO/>

===Feeding===
[[File:A formica rufa sideviewmirror.jpg|thumb|''Formica rufa'', a competitor for arthropod prey]]
The Eurasian treecreeper typically seeks [[invertebrate]] food on tree trunks, starting near the tree base and working its way up using its stiff tail feathers for support. Unlike a [[nuthatch]], it does not come down trees head first, but flies to the base of another nearby tree. It uses its long thin bill to extract [[insect]]s and [[spider]]s from crevices in the bark. Although normally found on trees, it will occasionally hunt prey items on walls, bare ground, or amongst fallen pine needles, and may add some conifer seeds to its diet in the colder months.<ref name = BWP/>

The female Eurasian treecreeper forages primarily on the upper parts of the tree trunks, while the male uses the lower parts. A study in [[Finland]] found that if a male disappears, the unpaired female will forage at lower heights, spend less time on each tree and have shorter foraging bouts than a paired female.<ref name= Aho>{{cite journal|last= Aho |first= Teija |author2=Kuitunen, Markku |author3=Suhonen, Jukka |author4=Hakkari, Tomi |author5=Jäntti, Ari |date=July 1997 |title = Effects of male removal on female foraging behavior in the Eurasian treecreeper |journal= Behavioral Ecology and Sociobiology |volume= 41|issue=1 |pages=49&ndash;53 | doi =10.1007/s002650050362}}</ref>

This bird may sometimes join [[mixed-species feeding flock]]s in winter, but it does not appear to share the resources found by accompanying [[tit (bird)|tits]] and [[goldcrest]]s, and may just be benefiting from the extra vigilance of a flock.<ref name = BWP/> [[Formica rufa group|Wood ants]] share the same habitat as the treecreeper, and also feed on invertebrates on tree trunks. The Finnish researchers found that where the ants have been foraging, there are fewer [[arthropod]]s, and male treecreepers spent a shorter time on spruce trunks visited by ants.<ref name= Aho2>{{cite journal|last= Aho |first= Teija |author2=Kuitunen, Markku |author3=Suhonen, Jukka |author4=Hakkari, Tomi |author5=Jäntti, Ari |date=November 1997 |title = Behavioural responses of Eurasian treecreepers, ''Certhia familiaris'', to competition with ants |journal= Animal Behaviour |volume= 54|issue=5 |pages=1283&ndash;1290 | doi =10.1006/anbe.1997.0547 |pmid=9398381}}</ref>

===Habits===
[[File:Certhia familiaris - 01.jpg|thumbnail|The claws of the treecreeper allows it to attach to the trunks and branches.]]
As a small woodland bird with [[crypsis|cryptic]] plumage and a quiet call, the Eurasian treecreeper is easily overlooked as it hops [[mouse|mouse-like]] up a vertical trunk, progressing in short hops, using its stiff tail and widely splayed feet as support. Nevertheless, it is not wary, and is largely indifferent to the presence of humans.<ref name=Harrap/> It has a distinctive erratic and undulating flight, alternating fluttering [[butterfly|butterfly-like]] wing beats with side-slips and tumbles. Migrating birds may fly by day or night, but the extent of movements is usually masked by resident populations. It is solitary in winter, but in cold weather up to a dozen or more birds will roost together in a suitable sheltered crevice.<ref name=BWP/>

==References==<!-- ZoolAbhStaatlMusTierkdDresden56:113. -->
{{reflist}}

==External links==
{{commons category|Certhia familiaris}}
* [http://ibc.lynxeds.com/species/eurasian-treecreeper-certhia-familiaris Eurasian treecreeper videos, photos & sounds] on the Internet Bird Collection.
* [http://www.limicola.de/fileadmin/user_upload/Dateien_Limicola/Limicola_Dokumente/DaunichtBauml%C3%A4uferLimicola1991.pdf Article on the identification of common and short-toed treecreepers] (in German with an English summary)
* [http://www.acopiancenter.am/boa.asp?id=317&name=BirdsofArmenia-EurasianTreecreeper Information and Illustration on European Tree Creeper from A Field Guide to Birds of Armenia]

{{taxonbar}}
{{featured article}}

[[Category:Certhia]]
[[Category:Birds of Eurasia|treecreeper]]
[[Category:Birds described in 1758]]