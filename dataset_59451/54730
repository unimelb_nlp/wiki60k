{{Infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name           = St. Ives
| image          =<!-- prefer 1st edition cover -->
| caption  = 
| author         = [[Robert Louis Stevenson]]
| illustrator    =
| cover_artist   =
| country        = Scotland
| language       = English
| genre          = [[Novel]]
| publisher      = [[Charles Scribner's Sons|Scribner's]]
| release_date   = 1897
| media_type     = Print ([[Hardcover|Hardback]] & [[Paperback]])
| pages          =
}} 
'''''St. Ives: Being The Adventures of a French Prisoner in England''''' ([[1897 in literature|1897]]) is an [[unfinished novel]] by [[Robert Louis Stevenson]]. It was completed in 1898 by [[Arthur Quiller-Couch]].

The book plot concerns the adventures of the dashing Viscomte Anne de Keroual de St. Ives, a Napoleonic soldier enlisted as a private under the name Champdivers, after his capture by the British.  The book is available on Project Gutenberg in both its incomplete and complete form (for the story as completed by Arthur Quiller-Couch look for "The Works of Robert Louis Stevenson volume twenty.").

==Film adaptations==
The 1949 film ''[[The Secret of St. Ives]]'' and the 1998 film ''[[St. Ives (1998 film)|St. Ives]]'', also known as ''All For Love'', were based on the novel. A [[St. Ives (TV series)|television mini-series based on the novel]] was broadcast on the BBC in 1955.

For the movie the character may have been changed to a Captain, and his first name changed to Jacque, as there seems to be confusion on these points.

==External links==
*[https://archive.org/search.php?query=title%3ASt.%20Ives%20creator%3Astevenson%20AND%20mediatype%3Atexts ''St. Ives''], available at [[Internet Archive]] (scanned books original editions illustrated)

{{Robert Louis Stevenson}}

[[Category:1897 novels]]
[[Category:Novels by Robert Louis Stevenson]]
[[Category:Unfinished novels]]
[[Category:Scottish novels]]
[[Category:Novels published posthumously]]
[[Category:19th-century British novels]]
[[Category:British novels adapted into films]]
[[Category:Novels adapted into television programs]]

{{1890s-novel-stub}}