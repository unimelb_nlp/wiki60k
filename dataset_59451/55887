{{Italic title}}
The '''''Biographical Memoirs of the National Academy of Sciences''''' has been published by the United States [[National Academy of Sciences]] since 1877 and presents [[biographies]] of selected members. This series of annual volumes (often abbreviated '''''BMNAS'''''), and the analogous British ''[[Biographical Memoirs of Fellows of the Royal Society]]'', are "important examples of biographical serials".<ref name="elis">{{citation |title=Encyclopedia of Library and Information Science, Volume 26: Role Indicators to St. Anselm-College Library (Rome) |first1=Allen |last1=Kent |first2=Harold |last2=Lancour |first3=Jay E. |last3=Daily |publisher=[[CRC Press]] |year=1979 |isbn=9780824720261 |contribution=Scientific Literature: Biographical Serials |page=448 |url=https://books.google.com/books?id=hr1RHr8lRcUC&pg=PA448}}.</ref>

The entries in the series are written by an academy member familiar with the subject's work and are written after the subject's death.<ref name="elis"/> Each entry includes a biography, photo, and a copy of the subject's [[signature]].<ref>{{citation |title=Publications of the National Academy of Sciences of the United States of America (1915-1926) |volume=13 |issue=1 |at=part II, p.&nbsp;120 |date=January 1927 |journal=[[Proceedings of the National Academy of Sciences of the United States of America]] |url=https://books.google.com/books?id=65crAAAAYAAJ&pg=PA120}}.</ref> Recent biographies from this series have also been made available online.<ref>{{citation |title=Guide to Reference in Medicine and Health |first1=Christa |last1=Modschiedler |first2=Denise Beaubien |last2=Bennett |publisher=[[American Library Association]] |year=2014 |isbn=9780838912218 |url=https://books.google.com/books?id=NwWlAwAAQBAJ&pg=PA114 |page=114}}.</ref>

== References ==
{{reflist|30em}}

== External links ==
* {{Official website|http://www.nasonline.org/publications/biographical-memoirs/}}
* {{ISSN|0077-2933}}

[[Category:United States National Academy of Sciences]]
[[Category:History of science journals]]
[[Category:Biography journals]]
[[Category:Annual journals]]
[[Category:Academic journals published by learned and professional societies]]
[[Category:Publications established in 1877]]
[[Category:English-language journals]]