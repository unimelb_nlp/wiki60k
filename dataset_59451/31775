{{Use dmy dates|date=June 2013}}
{{Infobox coin
| Country             = United States
| Denomination        = Isabella quarter
| Value               = 25 cents (.25
| Unit                = US$)
| Mass                = 6.25
| Diameter            = 24.3
| Edge                = [[reeding|reeded]]
| Silver_troy_oz      = .18084
| Composition =
  {{plainlist |
* 90.0% silver
* 10.0% copper
  }}
| Years of Minting    = 1893
| Obverse             = Isabella quarter obverse.jpg
| Obverse Design      = [[Isabella I of Castile|Queen Isabella I]]
| Obverse Designer    = [[Charles E. Barber]]
| Obverse Design Date = 1893
| Reverse             = Isabella quarter reverse.jpg
| Reverse Design      = Kneeling female with [[distaff]] and [[Spindle (textiles)|spindle]], symbolizing women's industry.
| Reverse Designer    = Charles E. Barber after a sketch by [[George T. Morgan]]
| Reverse Design Date = 1893
}}

The '''Isabella quarter''' or '''Columbian Exposition quarter''' was a [[Early United States commemorative coins|United States commemorative coin]] struck in 1893. Congress authorized the piece at the request of the Board of Lady Managers of the [[World's Columbian Exposition]].  The [[quarter (United States coin)|quarter]] depicts the Spanish queen [[Isabella I of Castile]], who sponsored [[Voyages of Christopher Columbus|Columbus's voyages to the New World]]. It was designed by [[United States Mint|Bureau of the Mint]] Chief Engraver [[Charles E. Barber]], and is the only U.S. commemorative of that denomination that was not intended for circulation.

The Board of Lady Managers, headed by Chicago socialite [[Bertha Palmer]], wanted a woman to design the coin and engaged Caroline Peddle, a sculptor.  Peddle left the project after disagreements with Mint officials, who then decided to have Barber do the work.  The reverse design, showing a kneeling woman spinning flax, with a [[distaff]] in her left hand and a [[Spindle (textiles)|spindle]] in her right, symbolizes women's industry and was based on a sketch by Assistant Engraver [[George T. Morgan]].

The quarter's design was deprecated in the [[Numismatics|numismatic]] press. The coin did not sell well at the Exposition; its price of $1 was the same as for the [[Columbian half dollar]], and the quarter was seen as the worse deal.  Nearly half of the authorized issue was returned to the Mint to be melted; thousands more were purchased at face value by the Lady Managers and entered the coin market in the early 20th century.  Today, they are popular with collectors and range in value between $450 in almost uncirculated  and $6,000 in near-pristine condition according to the 2014 edition of [[R.S. Yeoman]]'s ''[[A Guide Book of United States Coins]]''.{{sfn|Yeoman|p=285}}

== Legislation ==

In August 1892, Congress passed an act authorizing the first [[Early United States commemorative coins|United States commemorative coin]], [[Columbian half dollar|a half dollar]], to be sold at a premium by the managers of the [[World's Columbian Exposition]] in [[Chicago]].{{sfn|Lange|p=126}} The event had been authorised by [[United States Congress|Congress]] two years previously; that legislation created a Board of Lady Managers and a Board of Gentleman Managers to oversee the fair. The Board of Lady Managers was headed by [[Bertha Palmer]], whose husband [[Potter Palmer|Potter]] owned the [[Palmer House]], the leading hotel in Chicago. The decisions of the Lady Managers were often reversed by their male counterparts on controversial matters: for example, Palmer sought to shut the fair's "Egyptian Girls" dancing show after deeming it obscene. The show was one of the exposition's few successful moneymakers, and the Lady Managers were overruled by the men.{{sfn|Swiatek & Breen|pp=113–114}}{{sfn|Bowers ''Encyclopedia'', Part 8}}

[[File:Bertha Palmer (Updated).png|thumb|[[Bertha Palmer]] led the Exposition's Board of Lady Managers]]

Authorization for the Board of Lady Managers had been included in the 1890 law giving federal authority for the Exposition at the insistence of women's advocate, [[Susan B. Anthony]], who was determined to show that women could successfully assist in the management of the fair. To that end, the Lady Managers sought a coin to sell in competition with [[Columbian half dollar|the commemorative half dollar]] at the Exposition, which Congress had approved in 1892.{{sfn|Bowers ''Encyclopedia'', Part 8}}  Passage of the half dollar legislation had been difficult, and the Lady Managers decided to wait until the next session of Congress to make their request. When the half dollar appeared in November 1892, the Lady Managers considered it inartistic and determined to do better. Palmer wanted the Lady Managers "to have credit of being the authors of the first really beautiful and artistic coin that has ever been issued by the government of the United States".{{sfn|Moran|p=87}}

In January 1893, Palmer approached the [[United States House Committee on Appropriations|House Appropriations Committee]], asking that $10,000 of the funds already designated to be paid over to the Lady Managers by the federal government be in the form of souvenir quarters, which they could sell at a premium. On March 3, 1893, Congress duly passed an act authorising the souvenir coin, which was to be to the specifications of [[Barber coinage|the quarter]] struck for circulation, and with a design to be approved by the [[United States Secretary of the Treasury|Secretary of the Treasury]]. Total mintage of the special quarter would be limited to 40,000 specimens.{{sfn|Bowers ''Encyclopedia'', Part 8}}{{sfn|Moran|p=87}}

== Inception ==

Desiring a beautiful coin to sell, Palmer asked artist [[Kenyon Cox]] to produce sketches.  She was, however, determined to have a woman actually design the coin.  She also consulted with Sara Hallowell, who was both the secretary to the fair's Director of Fine Arts and was helping the Palmers amass a major art collection.  Hallowell contacted sculptor [[Augustus Saint-Gaudens]], who recommended his onetime student, Caroline Peddle, who was already engaged in exposition work, having been commissioned by [[Tiffany's]] to produce an exhibit.  Palmer agreed to have Peddle do the work.{{sfn|Moran|pp=87–91}}

[[File:Peddle sketch.jpg|left|thumb|Peddle's sketch for the quarter]]

After Congress authorized the souvenir quarter, the [[Director of the United States Mint|Director of the Bureau of the Mint]], [[Edward O. Leech]], wrote to Palmer on March 14, 1893.  Although he expressed a willingness to have the Lady Managers select the design, Mint Chief Engraver [[Charles E. Barber]] and [[Philadelphia Mint]] Superintendent [[Oliver Bosbyshell]] had already urged Leech to keep the design process in-house at the Mint.  Palmer replied that the Lady Managers had decided that the quarter would bear a portrait of [[Isabella I of Castile|Isabella I]], [[Crown of Castile|Queen of Castile]] (in Spain), whose assistance had helped pay for Columbus's expedition.  Palmer indicated that she was consulting artists and suggested that the Mint submit a design for consideration.  She also met with Illinois Congressman [[Allan C. Durborow, Jr.|Allen Durborow]], chairman of the House of Representatives' Fair Committee and a former colleague of Secretary of the Treasury [[John G. Carlisle]], Leech's superior.  Palmer suggested to the congressman that he advocate for the Lady Managers with Carlisle and Leech.{{sfn|Moran|p=88}}

Palmer, by letter, hired Peddle to do the design work in late March.  She instructed the artist that the coin was to have a figure of Isabella on the obverse, and the inscription "Commemorative coin issued for the Board of Lady Managers of the World's Columbian Exposition by Act of Congress, 1492–1892" on the reverse, as well as the denomination and the name of the country.  The chairwoman did not request that Peddle provide the Lady Managers with the design before sending it to the Mint.  Palmer informed Carlisle and Leech of her instructions. Carlisle had no objection to a coin being designed by a woman, or to the use of Isabella's head.  The secretary told Palmer that the reverse, with its long inscription, would appear like a business advertising token, and he asked that it be revised.{{sfn|Moran|p=91}}  Leech sent a note to Superintendent Bosbyshell informing him that the Lady Managers would likely have an outside sculptor create the obverse and asking him to have Mint Chief Engraver Charles E. Barber create some designs for the reverse for possible use.{{sfn|Moran|p=91}}

Obedient to Palmer's instructions, Peddle sent Leech sketches of a seated Isabella, with the long inscription on the reverse; she hoped the Mint Director would allow her to shorten it.  Leech was unhappy with the reverse, and decided that Barber would design that side of the coin.  Barber and Bosbyshell wrote to Leech that Isabella's legs would appear distorted if the seated figure were used and advocated a head in profile.  Carlisle agreed, stating that he had only given permission for a head of Isabella.  Peddle was informed that Barber would produce the reverse, though the design would be sent to her for approval, and she would have to change her obverse.  Meanwhile, Palmer was growing increasingly anxious: with a timeline of two months from design approval to the availability of the actual coins, she feared that the pieces would not be available for sale until well into the fair's May to October run.  Under pressure from all sides, Peddle threatened to quit the project,{{sfn|Moran|pp=92–93}} writing that she "could not consent to do half of a piece of work".{{sfn|Taxay|p=10}}

What finally wore down Peddle's patience were two letters dated April 7.  One, from Leech, asserted his right as Mint director to prescribe coin designs, and told Peddle that the obverse would be a head of Isabella, while the reverse would be based on sketches by a Mint engraver which she would be free to model.  The second, from Bosbyshell, imposed the additional requirement that Isabella not wear a crown, which he deemed inappropriate on an American coin.  On April 8, 1893, Caroline Peddle withdrew from the project.{{sfn|Moran|p=93}}

Following Peddle's resignation, Leech wrote a conciliatory letter to Palmer, who responded regretting that the three of them had not worked together, rather than at cross-purposes.  Palmer had written to suggest an alternative to the inscription reverse:  that the coin depict the Women's Building at the fair.  Barber prepared sketches and rejected the idea, stating that the building would appear a mere streak on the coin in the required low relief.  Instead, he favored a sketch prepared by Assistant Engraver [[George T. Morgan]], showing a kneeling woman spinning flax, with a [[distaff]] in her hands.  Leech was not fully satisfied with the proposal, stating that the juxtaposition of Isabella on the obverse and the Morgan reverse was "too much woman".{{sfn|Moran|pp=91, 94}} Before accepting Morgan's design, Leech wanted Barber to produce some reverses himself, which the chief engraver did, and Bosbyshell forwarded them to Leech on April 11 and 12.  These showed various uses of a [[spreadeagle (heraldry)|heraldic eagle]].  After considering these efforts, Leech decided on Morgan's design{{sfn|Moran|p=94}} and wrote to Palmer accordingly, stating that "the distaff is used in art to symbolize patient industry, and especially the industry of women."{{sfn|Taxay|p=11}}  In response, the Lady Managers suggested the use of the building's portal, and asked if it was possible to place a living person on the coin.  Leech stated that Secretary Carlisle had selected the distaff reverse, and his determination was binding.{{sfn|Moran|p=97}}

Bosbyshell informed Leech by letter that Stewart Cullin, curator at the [[University of Pennsylvania]], possessed a number of medals depicting Isabella, and former general [[Oliver O. Howard]] was engaged in writing a biography of the late queen and possessed likenesses of her.  Leech agreed that these men be consulted.  Carlisle was reluctant to allow an inscription which made distinctions by sex, such as "Board of Lady Managers", to appear on the coin, but he eventually agreed to that wording.  On April 24, the Mint Director sent Palmer a box containing two plaster models of the obverse, one of Isabella as a young queen, the other showing her more mature.  He also informed her that distaff reverse would be used, with the wording agreed to by Carlisle.{{sfn|Taxay|pp=11–13}}  The obverse models were supposedly made by Barber based on an engraving of Isabella forwarded by Peddle to the Mint at Palmer's request, but Moran suggests that the period of only a day between receipt of the engraving and completion of the models (during which Barber also attended the funeral of Bosbyshell's grandson) means that Barber was working on them before that.  The Board of Lady Managers on May 5 selected the young queen.{{sfn|Moran|p=97}}

== Design and reception ==
[[File:Woman and sister obv.jpg|thumb|upright|left|1838 anti-slavery token "Am I not a woman and a sister"]]
The obverse of the Isabella quarter depicts a crowned and richly clothed bust of that Spanish queen.  According to art historian [[Cornelius Vermeule]], Barber's obverse design "follow[s] Gilbert Scott's Victorian Gothic tradition of photographic classicism, best summed up by the groups of continents and the reliefs of famous persons on the [[Albert Memorial]] in London."{{sfn|Vermeule|p=92}}  The reverse depicts a kneeling woman with distaff and spindle.{{sfn|Yeoman|p=285}}  Vermeule traces that imagery to the figure of a young female servant, carved upon the east pediment of the [[Temple of Zeus]] at [[Olympia, Greece|Olympia]] in the 5th century B.C.  Nevertheless, a contemporary account in the ''American Journal of Numismatics'' compared the reverse to an anti-slavery token with a kneeling woman and the legend "Am I not a woman and a sister".{{sfn|Vermeule|pp=92–93}}  The art historian, writing in 1971, noted that "nowadays the coin seems charming for its quaintness and its Victorian flavor, a mixture of cold Hellenism and Renaissance romance.  Perhaps one of its greatest joys is that none of the customary inscriptions, mottoes and such, appear on it."{{sfn|Vermeule|p=93}}

Numismatic historian [[Don Taxay]], in his study of early U.S. commemoratives, dismissed contemporary accounts (such as in the fair's official book) that Kenyon Cox had provided a design for the quarter; he noted that the artist's son had strongly denied that his father was involved in the coin's creation.  Taxay deemed the design "commonplace" and "typical of Barber's style", stating that "the modeling, though somewhat more highly relieved than on the half dollar, is without distinction".{{sfn|Taxay|p=13}}

The ''American Journal of Numismatics'' had other criticisms of the quarter:

{{quote
| Of its artistic merit, as of the harmony which is reported to have prevailed at the meetings of those [Lady] Managers, perhaps the less said the better; we do not know who designed it, but in this instance, as in the Half Dollar, the contrast between the examples of the numismatic art of the nation, as displayed on the Columbian coins, on the one hand, and the spirited and admirable work of the architects of the [Exposition's] buildings, on the other, is painful.  If these two coins really represent the highest achievements of our medallists and our mints&nbsp;... we might as well despair of its future&nbsp;... We are not ready to admit this to be true.{{sfn|Vermeule|p=93}}
}}

== Release and collecting ==
Minting of what Barber dubbed "showy quarters" began at the Philadelphia Mint on June 13, 1893,{{sfn|Moran|p=98}} six weeks after the exposition opened.{{sfn|Bowers ''Encyclopedia'', Part 1}}  Leech had planned to strike the pieces using polished blanks, or [[planchet]]s,{{sfn|Taxay|p=13}} and workers at that mint handled the coins carefully; unlike the half dollar, surviving specimens display relatively few contact marks from other coins. The first piece struck, along with numbers 400, 1,492, and 1,892, were struck in proof condition and sent to the Lady Managers along with certificates attesting to their status.{{sfn|Bowers|p=42}}  A total of 40,023 pieces were struck, with the 23 coins over the authorized mintage retained by the Mint for inspection by the 1894 [[Assay Commission]].{{sfn|Bowers ''Encyclopedia'', Part 10}}

The pieces did not sell well at the exposition.  They were for sale only at the Women's Building at the fair, or by mail; the half dollar could be purchased at several outlets.{{sfn|Bowers|p=42}}  Some 15,000 quarters were sold to collectors, dealers, and fairgoers, including several thousand of them purchased by the [[John Walter Scott|Scott Stamp and Coin Company]].  Fairgoers viewed the quarter as not as good a deal as the half dollar, as both sold for the same price of $1.  Of the remainder, approximately 10,000 quarters were bought at face value by Palmer and other Lady Managers; 15,809 were returned to the government for melting.  After deducting pieces returned for melting, a total of 24,214 coins were distributed to the public.{{sfn|Bowers|pp=41–42}}{{sfn|Moran|p=113}}

The large quantities possessed by the Lady Managers made their way into the market through dealers and other vendors in the 1920s.  By 1930, prices had risen to the original issue price; by 1955, uncirculated specimens sold for $20.{{sfn|Bowers ''Encyclopedia'', Part 10}}  The pieces are popular among collectors because they are the only U.S. quarter dollars issued strictly as a commemorative, not for circulation.{{sfn|Bowers|pp=41–42}}  The 2014 edition of [[R.S. Yeoman]]'s ''[[A Guide Book of United States Coins]]'' lists the piece as ranging between $450 in almost uncirculated AU-50 on the [[Sheldon coin grading scale]] and $6,000 in near-pristine MS-66.{{sfn|Yeoman|p=285}}

== See also ==
{{Portal|Numismatics}}
* [[Columbian half dollar]]&nbsp;– The [[half dollar (United States coin)|half dollar]] commemorative also minted for the exposition.
* [[Early United States commemorative coins]]
{{Clear}}

== References ==

{{reflist|20em}}

'''Bibliography'''

* {{cite book
  | last = Bowers
  | first = Q. David
  | authorlink = Q. David Bowers
  | year = 2008
  | title = A Guide Book of United States Commemorative Coins
  | publisher = Whitman Publishing
  | location = Atlanta, Ga.
  | isbn = 978-0-7948-2256-9
  | ref = {{sfnRef|Bowers}}
  }}
* {{cite book
  | last = Lange
  | first = David W.
  | year = 2006
  | title = History of the United States Mint and its Coinage
  | publisher = Whitman Publishing
  | location = Atlanta, Ga.
  | isbn = 978-0-7948-1972-9
  | ref = {{sfnRef|Lange}}
  }}
* {{cite book
  | last = Moran
  | first = Michael F.
  | year = 2008
  | title = Striking Change: The Great Artistic Collaboration of Theodore Roosevelt and Augustus Saint-Gaudens
  | publisher = Whitman Publishing
  | location = Atlanta, Ga.
  | isbn = 978-0-7948-2356-6
  | ref = {{sfnRef|Moran}}
  }}
* {{cite book
  | last = Taxay
  | first = Don
  | authorlink = Don Taxay
  | year = 1967
  | title = An Illustrated History of U.S. Commemorative Coinage
  | publisher = Arco Publishing
  | location = New York
  | oclc = 1357564
  | ref = {{sfnRef|Taxay}}
  }}
* {{cite book
  | last = Swiatek
  | first = Anthony
  | last2 = Breen
  | first2 = Walter
  | authorlink2 = Walter Breen
  | year = 1981
  | title = The Encyclopedia of United States Silver & Gold Commemorative Coins, 1892 to 1954
  | publisher = Arco Publishing
  | location = New York
  | isbn = 978-0-668-04765-4
  | ref = {{sfnRef|Swiatek & Breen}}
  }}
* {{cite book
  | last = Vermeule
  | first = Cornelius
  | authorlink = Cornelius Clarkson Vermeule III
  | year = 1971
  | title = Numismatic Art in America
  | publisher = The Belknap Press of Harvard University Press
  | location = Cambridge, Mass.
  | isbn = 978-0-674-62840-3
  | ref = {{sfnRef|Vermeule}}
  }}
* {{cite book
  | last = Yeoman
  | first = R.S.
  | authorlink = Richard S. Yeoman
  | year = 2013
  | title = [[A Guide Book of United States Coins]]
  | edition = 67th
  | publisher = Whitman Publishing
  | location = Atlanta, GA
  | isbn = 978-0-7948-4180-5
  | ref = {{sfnRef|Yeoman}}
  }}

'''Other sources'''

* {{cite web
  | last = Bowers
  | first = Q. David
  | authorlink = Q. David Bowers
  | title = Chapter 8: Silver commemoratives (and clad too), Part 1
  | work = Commemorative Coins of the United States: A Complete Encyclopedia
  | url = http://www.pcgs.com/books/commemoratives/Chapter08-001.aspx
  | accessdate = October 3, 2012
  | ref = {{sfnRef|Bowers ''Encyclopedia'', Part 1}}
  }} For further information on source, see [http://www.pcgs.com/books/commemoratives/ here].
* {{cite web
  | last = Bowers
  | first = Q. David
  | authorlink = Q. David Bowers
  | title = Chapter 8: Silver commemoratives (and clad too), Part 8
  | work = Commemorative Coins of the United States: A Complete Encyclopedia
  | url = http://www.pcgs.com/books/commemoratives/Chapter08-008.aspx
  | accessdate = September 30, 2012
  | ref = {{sfnRef|Bowers ''Encyclopedia'', Part 8}}
  }} For further information on source, see [http://www.pcgs.com/books/commemoratives/ here].
* {{cite web
  | last = Bowers
  | first = Q. David
  | authorlink = Q. David Bowers
  | title = Chapter 8: Silver commemoratives (and clad too), Part 10
  | work = Commemorative Coins of the United States: A Complete Encyclopedia
  | url = http://www.pcgs.com/books/commemoratives/Chapter08-010.aspx
  | accessdate = September 30, 2012
  | ref = {{sfnRef|Bowers ''Encyclopedia'', Part 10}}
  }} For further information on source, see [http://www.pcgs.com/books/commemoratives/ here].

{{featured article}}

{{Coinage (United States)}}

[[Category:1893 introductions]]
[[Category:Commemorative coins of the United States]]
[[Category:Isabella I of Castile]]
[[Category:World's Columbian Exposition]]
[[Category:Twenty-five-cent coins]]