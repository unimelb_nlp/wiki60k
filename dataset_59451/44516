{{Refimprove|date=April 2017}}

{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{italic title}}
[[File:Liza of Lambeth Front cover.jpg|thumb|First edition (publ. [[T. Fisher Unwin]]) ]]
{{Wikisource}}
'''''Liza of Lambeth''''' (1897) was [[W. Somerset Maugham]]'s first novel, which he wrote while working as a doctor at a hospital in [[Lambeth]],<ref>''A fragment of autobiography'', Somerset Maugham</ref> then a working class district of London. It depicts the short life and death of Liza Kemp, an 18-year-old factory worker who lives together with her aging mother in the fictional Vere Street off [[Westminster Bridge Road]] (real) in Lambeth. All in all, it gives the reader an interesting insight into the everyday lives of working class Londoners at the end of the nineteenth century.

==Plot summary==

The action covers a period of roughly four months—from August to November—around the time of [[Victoria of the United Kingdom|Queen Victoria]]'s Jubilee. Liza Kemp is an 18-year-old factory worker and the youngest of a large family, now living alone with her aging mother. Very popular with all the residents of Vere Street, Lambeth, she likes Tom, a boy her age, but not as much as he likes her, so she rejects him when he proposes. Nevertheless, she is persuaded to join a party of 32 who make a coach trip (in a horse-drawn coach, of course) to a nearby village on the August [[Bank Holiday]] Monday. Some of the other members of the party are Tom; Liza's friend Sally and her boyfriend Harry; and Jim Blakeston, a 40-year-old father of 5 who has recently moved to Vere Street with his large family, and his wife (while their eldest daughter, Polly, is taking care of her siblings). The outing is fun, and they all get drunk on beer. On their way back in the dark, Liza realises that Jim Blakeston is making a pass at her by holding her hand. Back home, Jim manages to speak to her alone and to steal a kiss from her.

Seemingly without considering either the moral implications or the consequences of her actions, Liza feels attracted to Jim. They never appear together in public because they do not want the other residents of Vere Street or their workmates to start talking about them. One of Jim Blakeston's first steps to win Liza's heart is to go to a melodramatic play with her on Saturday night. Afterwards, he assaults and rapes her (although the graphic details of their sexual encounter are not explicitly described):

:'I wish yer wasn't goin' ter leave me, Liza.'
:'Garn! I must!' She tried to get her hand away from his, but he held it firm, resting it on the top of the pillar.
:'Leave go my 'and,' she said. He made no movement, but looked into her eyes steadily, so that it made her uneasy. She repented having come out with him. 'Leave go my 'and.' And she beat down on his with her closed fist.
:'Liza!' he said, at last.
:'Well, wot is it?' she answered, still thumping down on his hand with her fist.
:'Liza,' he said a whisper, 'will yer?'
:'Will I wot?' she said, looking down.
:'You know, Liza. Sy, will yer?'
:'Na,' she said.
:He bent over her and repeated—
:'Will yer?'
:She did not speak, but kept beating down on his hand.
:'Liza,' he said again, his voice growing hoarse and thick—'Liza, will yer?'
:She still kept silence, looking away and continually bringing down her fist. He looked at her a moment, and she, ceasing to thump his hand, looked up at him with half-opened mouth. Suddenly he shook himself, and closing :his fist gave her a violent, swinging blow in the belly.

Jim punches her in the stomach, but in the end they "slide down into the darkness of the passage". Despite the violent encounter, Liza is overwhelmed by love. ("Thus began a time of love and joy.")

When autumn arrives and the nights get chillier, Liza's secret meetings with Jim become less comfortable and more trying; they must meet in the third-class waiting-room of [[London Waterloo railway station|Waterloo station]]. To Liza's dismay, people do start talking about them despite their precautions. Only Liza's mother, a drunkard and a simple person, doesn't know about their affair.

After Liza's friend Sally gets married, her husband doesn't want her to earn her own money, so he stops her from working at the factory; besides, Sally soon becomes pregnant. With Sally married and stuck at home, and even Tom seemingly shunning her, Liza feels increasingly isolated, but her love for Jim keeps her going. They do talk about their love affair: about the possibility of Jim leaving his wife and children ("I dunno if I could get on without the kids"); about Liza not being able to leave her mother, who needs her help; about living somewhere else "as if we was married", about [[polygamy|bigamy]]—but, strangely, not about [[adultery]].

The novel builds up to a sad climax, suggesting that all men—with the possible exception of Tom—are alike, since they all beat their wives, especially when they have been drinking. Soon after their wedding, Harry beats up Sally just because she has been away from home chatting with a female neighbor; he even hits his mother-in-law. When Liza drops by, she stays a bit longer to comfort Sally, which makes her late for her meeting with Jim in front of a nearby [[pub]]. When she finally gets there, Jim is aggressive towards her for being late. Without really intending to, he hits her across the face ("It wasn't the blow that 'urt me much; it was the wy you was talkin'") and gives her a black eye.

Soon the situation deteriorates completely. Mrs Blakeston, who is pregnant again, opposes Jim's affair with Liza by refusing to talk to him, then goes around telling other people what she would do with Liza if she caught her, and those people inform Liza, who is frightened because she is weak and she knows Mrs. Blakeston is strong. One Saturday afternoon in November, Liza is on her way home from work when the angry Mrs. Blakeston confronts her, spits in her face, and physically attacks her. Quickly a crowd gathers, not to abate the fight, but to abet it. ("The audience shouted and cheered and clapped their hands.") Eventually, both Tom and Jim stop the fight, and Tom walks Liza home. Liza is now publicly stigmatised as a "wrong one", a fact she herself admits to Tom ("Oh, but I 'ave treated yer bad. I'm a regular wrong 'un, I am"). Despite all her misbehaviour ("I couldn't 'elp it! [...] I did love 'im so!"), Tom still wants to marry Liza, but she tells him that "it's too lite now" because she thinks she is pregnant. Tom says he wouldn't mind that, but she insists on refusing.

Meanwhile, at the Blakestones', Jim beats up his wife. Other residents hear them and young Polly appeals to some for help, but they choose not to interfere in other people's domestic problems ("She'll git over it; an' p'raps she deserves it, for all you know").

When Mrs Kemp comes home and sees her daughter's injuries, all she does is offer her some alcohol (whiskey or gin). That evening they both get drunk, despite Liza's pregnancy. During the next night Liza has a [[miscarriage]]. Mr Hodges, who lives upstairs, fetches a doctor from the nearby hospital, who soon says he can do nothing for her. While her daughter is dying, Mrs Kemp has a long talk with Mrs Hodges, a [[midwife]] and sick-nurse. Liza's last visitor is Jim, but Liza is already in a coma. Mrs Kemp and Mrs Hodges are talking about the funeral arrangements when they hear Liza's death rattle and the doctor declares her dead.

==Major themes==

===Living conditions===

''Liza of Lambeth'' is clearly not a [[muckraking]] novel. People seem to be content with what they have; their poverty is not depicted as unbearable, and it does not prevent them from being fervent patriots ("Every man's fust duty is ter get as many children as 'e bloomin'well can") or from enjoying their spare time (which is often spent in pubs; also Liza drinks a lot). The scene at the theatre where Liza shouts out loud during the performance to warn one of the characters on stage is reminiscent of the [[Elizabethan theatre]].

At one point the narrator deplores the "newish, three-storied buildings" of Vere Street which are "perfectly flat, without a bow window [...] to break the straightness of the line from one end of the street to the other". As the lodgings are rather crowded with people, the residents of Vere Street spend as much time as possible outside, in the street—something which has changed completely in the course of the last hundred years.

===Working conditions and working hours===

It is not mentioned what the factory Liza and Sally work at is producing. What we do learn though is that work at the factory starts at 8 a.m. If you are late you are shut out, do not get a token and, accordingly, do not get any pay for that day. On Saturdays, work is over around 2 p.m. The August Bank Holiday—the day of the excursion—enables the workers to have two days off in a row, something which is quite unusual for them.

===The relationship between men and women===

There is no allusion to the women's and [[suffragette]] movements. All the characters know their places, and [[stereotype]]s of [[gender role]]s are depicted repeatedly. Sally, for instance, is absolutely submissive, blaming only herself when her husband, Harry, beats her.

Apart from Jim Blakeston's illicit affair with Liza Kemp, leading to an unwanted pregnancy, there is just a brief mention of illegitimate children, and no mention of abortion. Nowhere is the question of [[morality]] brought up, neither by the characters, nor by the third-person narrator.

===The value of human life===

From an early 21st-century point of view, the way the characters regard death could be called fatalistic. People do not believe there is anything they can do about sudden or premature deaths. [[Infant mortality]] is very high.

===The use of language===

Unorthodox spelling, used to reflect the characters' dialects, is probably the most jarring aspect of the novel to modern readers, although it was considered almost conventional at the time. This concerns the conveyance of typical [[Cockney]] speech to readers who were expected to find it quaint: the alteration of long vowels and the phenomena of dropping and inserting aitches (not to mention the countless slang expressions). The style served at least two goals: to permit readers to maintain a certain psychologically safe distance from the characters and to add coloration to readers' fantasies. [[Mark Twain]] relied heavily upon it.{{citation needed|date=April 2017}}

==Theatrical adaptations==
A musical based – albeit loosely – on the novel was written by [[Willie Rushton]] and [[Berny Stringle]], with music by [[Cliff Adams Singers|Cliff Adams]]. It opened at the Shaftesbury Theatre in London in June 1976, and ran for 110 performances. It was produced by [[Ben Arbeid]], directed by Berny Stringle, musically directed by John Burrows, and starred [[Angela Richards]] (best known as a regular in the BBC's [[Secret Army (TV series)|''Secret Army'']]) in the title role, Patricia Hayes, [[Ron Pember]], Michael Robbins and [[Eric Shilling]], among others.

The musical style is predominantly [[music hall]], but the show includes a parody of [[Gilbert and Sullivan]], a church choir arrangement with some completely incongruous lyrics (''A Little Bit on the Side''), and some touching ballads.

''The Tart with a Heart of Gold'' was cut from the West End production, and is also missing from the original London cast recording (Thames THA 100), despite it describing the entire ''raison d'être'' of one of the main female characters.

The musical has not been officially published for amateur performance, but it is occasionally licensed for amateurs. The world amateur premiere was performed at the [[Erith Playhouse]] in [[Erith]], Kent, in June 1977, and was attended by members of the London production team. The rights to this musical are currently held by Thames Music in London.

==See also==
{{portal|Novels}}
*A list of other works of literature with eponymous heroines can be found [[Wikipedia:WikiProject Novels/List of literary works with eponymous heroines|here]].

==References==
{{reflist|colwidth=30em}}

==External links==
{{Gutenberg|no=16517|name=Liza of Lambeth}}
*[http://www.guidetomusicaltheatre.com/shows_l/lizalambeth.htm Guide to Musical Theatre information page for ''Liza of Lambeth'' musical]

{{W. Somerset Maugham}}

{{DEFAULTSORT:Liza Of Lambeth}}
[[Category:1897 novels]]
[[Category:Adultery in novels]]
[[Category:Novels by W. Somerset Maugham]]
[[Category:Novels set in London]]
[[Category:Debut novels]]
[[Category:Novels set in Victorian England]]
[[Category:19th-century British novels]]