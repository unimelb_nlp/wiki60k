{{Infobox journal
| title = Journal of Graph Theory
| formernames =
| cover = 
| discipline = [[Mathematics]]
| abbreviation = J. Graph Theory
| editor = [[Paul Seymour (mathematician)|Paul Seymour]], [[Carsten Thomassen]]
| publisher = [[John Wiley & Sons]]
| country = 
| history = 1977-present
| frequency = Monthly
| impact = 0.626
| impact-year = 2012
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1097-0118
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1097-0118/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1097-0118/issues
| link2-name = Online archive
| ISSN = 0364-9024
| eISSN = 1097-0118
| CODEN = JGTHDO
| LCCN = 
| OCLC = 3057169
}}
The '''''Journal of Graph Theory''''' is a [[Peer review|peer-reviewed]] [[mathematics journal]] specializing in [[graph theory]] and related areas, such as structural results about graphs, [[list of algorithms|graph algorithms]] with theoretical emphasis, and [[discrete optimization]] on graphs. The scope of the journal also includes related areas in [[combinatorics]] and the interaction of graph theory with other mathematical sciences. It is  published by [[John Wiley & Sons]]. The journal was established in 1977 by [[Frank Harary]].<ref name=sigact>[http://sigact.acm.org/harary.htm Frank Harary], a biographical sketch at the ACM [[ACM SIGACT|SIGACT]] site</ref> The [[editors-in-chief]] are  [[Paul Seymour (mathematician)|Paul Seymour]] ([[Princeton University]]) and [[Carsten Thomassen]] ([[Technical University of Denmark]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in the [[Science Citation Index Expanded]], [[Scopus]], and ''[[Zentralblatt MATH]]''. According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 0.626.<ref name=WoS>{{cite book |year=2013 |chapter=Journal of Graph Theory |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1097-0118}}

[[Category:Mathematics journals]]
[[Category:Publications established in 1977]]
[[Category:Monthly journals]]
[[Category:John Wiley & Sons academic journals]]
[[Category:English-language journals]]


{{mathematics-journal-stub}}