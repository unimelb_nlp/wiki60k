{{italic title}}
{{Refimprove|date=November 2013}}
'''Green Building Initiative''' (GBI) is a 501(c)(3) nonprofit organization that administers green building assessment and certification services in the United States. It was established in 2004 and is headquartered in [[Portland, Oregon]]. The GBI is the U.S. licensee of the Green Globes rating system. 

==Green Building Initiative background==
The GBI acquired the U. S. rights to the Canadian Green Globes building assessment and certification program in 2004 and adapted it for the U.S. market as an alternative to the LEED building rating systems. This eventually included modules for New Construction (NC), Continual Improvement of Existing Buildings (CIEB), and CIEB Healthcare for the healthcare industry. The GBI offers these programs to builders, designers and building managers.<ref name="gsa">{{cite web|url=http://www.gsa.gov/graphics/ogp/Cert_Sys_Review.pdf |accessdate=June 24, 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20120921021345/http://www.gsa.gov/graphics/ogp/Cert_Sys_Review.pdf |archivedate=September 21, 2012 }}</ref><ref name="datacenterjournal">{{cite web|author=Green Globes |url=http://www.datacenterjournal.com/dcj-magazine/green-globes-practical-webbased-alternative-leed/ |title=Green Globes ® - A Practical, Web-based Alternative to LEED |publisher=The Data Center Journal |date=2013-04-22 |accessdate=2013-11-12}}</ref>
In 2005, the GBI was accredited as a standards developer through the American National Standards Institute (ANSI). The GBI developed the ANSI/GBI 01-2010: Green Building Assessment Protocol for Commercial Buildings to guide the development of Green Globes products.<ref>{{cite web|url=http://publicaa.ansi.org/sites/apdl/Documents/Standards%20Activities/American%20National%20Standards/ANSI%20Accredited%20Standards%20Developers/OCT13ASD_basic.pdf |title=Accredited Standards Developers |publisher=Publicaa.ansi.org |accessdate=2013-11-12}}</ref><ref>{{cite web|url=http://www.buildings.com/article-details/articleid/9756/title/gbi-s-american-national-standard-for-commercial-green-building-receives-formal-approval.aspx |title=GBI's American National Standard for Commercial Green Building Receives Formal Approval |publisher=Buildings.com |date= |accessdate=2013-11-12}}</ref> The nonprofit also provides training and certification for professionals who use their programs.<ref name="boma">{{cite web|url=http://www.boma.org/education/pro-designations/Pages/Green-Globes.aspx |title=GBI’s Green Globes Professional Certification Program |publisher=Boma.org |date= |accessdate=2013-11-12}}</ref>
In 2011 the GBI developed the [[Guiding Principles|Guiding Principles Compliance Program]] (GPC) to measure compliance with the Federal Guiding Principles for Sustainable Buildings as required by [[Executive Order 13514]] signed in 2009.

The Green Globes certification program was launched in the U.S. in 2005 as an online building design management tool for architects and builders of sustainable commercial buildings.<ref>{{cite web|author=Shari Shapiro |url=http://greenlaw.blogspot.com/2008/06/guest-column-overview-of-leed-and-green.html |title=Green Building Law: Guest Column-Overview of LEED and Green Globes Rating System |publisher=Greenlaw.blogspot.com |date=2008-06-30 |accessdate=2013-11-12}}</ref> Green Globes originated from a system started in 1990 in the United Kingdom called [[BREEAM]] (Building Research Establishment Environmental Assessment Method). In an effort to make the tool more user-friendly, the Building Research Establishment, merged the BREEAM standard with a questionnaire-based rating tool. Later, it was converted to a web-based format and renamed Green Globes. Green Globes is marketed through the GBI in the U.S. and through the Building Owners & Managers Association (labeled as BOMA BESt) in Canada.<ref name="datacenterjournal" /><ref>{{cite web|url=http://www.bre.co.uk/about-us.jsp |title=BRE Group: About us |publisher=Bre.co.uk |date= |accessdate=2013-11-12}}</ref><ref>{{cite web|url=http://www.bomabest.com/ |title=Boma Best |publisher=Boma Best |date=2013-07-15 |accessdate=2013-11-12}}</ref>

==Green Globes for New Construction and Continual Improvement of Existing Buildings ==
Green Globes for New Construction (Green Globes-NC) uses two stages for assessment and certification. The preliminary assessment occurs after concept design when construction documents are available, while the final assessment occurs after construction. Users can evaluate their systems based on the amount of applicable points in seven categories.<ref>{{cite web|url=http://www.pnl.gov/main/publications/external/technical_reports/PNNL-15858.pdf |title=Pacific Northwest National Laboratory : Sustainable Building Rating Systems Summary |publisher=Pnl.gov |accessdate=2013-11-12}}</ref>
The process is questionnaire-driven and users are walked through a sequence of online questions. Once the questionnaire is completed a report is generated that provides a list of achievements along with recommendations for sustainable building strategies.<ref>{{cite web|url=http://greenaffordable.org/sustainable/measuring-results |title=Standards and Benchmarks for Measuring Results &#124; Building for Sustainability |publisher=Greenaffordable.org |date=2011-01-01 |accessdate=2013-11-12}}</ref> Building assessments are performed by a third-party, GBI-authorized assessor with expertise in green building design, engineering, construction and facility operations.<ref>Kibert, Charles J. (2008) Sustainable Construction: Green Building Design and Delivery (2nd ed.). John Wiley & Sons, Inc. Retrieved from https://books.google.com</ref><ref>{{cite web|url=http://www.advancedcontrolcorp.com/blog/green-globe-certification-vs-leed.html |title=Green Globe Certification vs. LEED |publisher=Advancedcontrolcorp.com |date=2013-01-07 |accessdate=2013-11-12}}</ref> The verifier performs an onsite assessment of the building to ensure that the self-reported claims made in the online documentation are verified and to suggest how to achieve different levels of certifications.<ref>{{cite web|url=http://www.wbdg.org/resources/gbs.php |title=Green Building Standards and Certification Systems &#124; Whole Building Design Guide |publisher=Wbdg.org |date=2011-09-26 |accessdate=2013-11-12}}</ref><ref>{{cite web|author= |url=http://supermarketnews.com/sustainability/green-globes-difference |title=The Green Globes Difference &#124; Sustainability content from |publisher=Supermarket News |date=2011-11-28 |accessdate=2013-11-12}}</ref>
The Green Globes for Continual Improvement of Existing Buildings program (Green Globes—CIEB) is used by building owners and property managers of existing buildings to evaluate the building's current performance, create a baseline for performance, plan for improvements and monitor ongoing performance. The questionnaire and assessment process is similar to the New Construction process, except without the site assessment.<ref name="datacenterjournal" /><ref>{{cite web|url=http://www.pac-clad.com/pac-green-info/wordpress/wp-content/uploads/2010/07/GreenGlobes.pdf |title=PAC Grren Info : Green Globes |publisher=Pac-clad.com |accessdate=2013-11-12}}</ref>
In June 2011, the GBI launched the Green Globes Continual Improvement of Existing Buildings program for Healthcare (CIEB – Healthcare) tailored to existing hospital and long term care facilities. The program is based on the Green Globes — CIEB program.<ref>{{cite web|url=https://www.premierinc.com/safety/topics/construction/assessment-building-sustain.jsp |title=Assessment - Building sustainability |publisher=Premierinc.com |date= |accessdate=2013-11-12}}</ref>

==Green Globes rating categories==
There are 1,000 available points across seven main sections.<ref name="thegbi">{{cite web|url=http://www.thegbi.org/green-globes-certification/how-to-certify/|title=Green Globes Overview|last=|first=|date=|website=|publisher=Thegbi.org|access-date=|accessdate=2016-09-27}}</ref>
{| class="wikitable"
|-
! Assessment Area !! Description !! New Construction Points !! Existing Building Points
|-
| Project Management-Policies & Practices || Integrated design, environmental purchasing, commissioning, and emergency response plan. || 50 || 100
|-
| Site (not applicable for CIEB) || Site development areas, reduce ecological impacts, enhancement of watershed features, and site ecology improvement. || 115 || 
|-
| Energy || Energy consumption, energy demand minimization, “right sized” energy-efficient systems, renewable sources of energy, energy-efficient transportation. || 390 || 350
|-
| Water || Flow and flush fixtures, water-conserving features, reduce off-site treatment of water. || 110 || 80
|-
| Resources, Building Materials and Solid Waste || Materials with low environmental impact, minimized consumption and depletion of material resources, re-use of existing structures; building durability, adaptability and disassembly; and reduction, re-use and recycling of waste. || 125 || 110
|-
| Emissions, Effluents and other impacts || Air emissions, ozone depletion and global warming, protection of waterways and impact on municipal waste water treatment facilities, minimization of land and water pollution, integrated pest management, and the storage of hazardous materials. || 50 || 175
|-
| Indoor Environment || Effective ventilation systems, source control of indoor pollutants, lighting design and integration of lighting systems, thermal comfort, acoustic comfort. || 160 || 185
|-
| '''Total Points''' ||  || '''1000''' || '''1000'''
|}

==Green Globes ratings==
Projects that have achieved over 35 percent of the 1,000 available points and are third-party verified can earn a rating of 1 to 4 Green Globes.<ref name="thegbi" />

{| class="wikitable"
|-
| 85-100% || 4 Globes || Demonstrates national leadership and excellence in the practice of energy, water, and environmental efficiency to reduce environmental impacts.
|-
| 70-84% || 3 Globes || Demonstrates leadership in applying best practices regarding energy, water, and environmental efficiency.
|-
| 55-69% || 2 Globes || Demonstrates excellent progress in the reduction of environmental impacts and use of environmental efficiency practices.
|-
| 35-54% || 1 Globes || Demonstrates a commitment to environmental efficiency practices.
|}

==Guiding Principles Compliance==
In 2011, the GBI developed the Guiding Principles Compliance Assessment Program (GPC) for use by federal agencies in assessing compliance with the Federal Interagency Sustainability Working Group’s Guiding Principles for sustainable existing buildings as required by Executive Order 13514.<ref>{{cite web|url=http://www.edcmag.com/articles/gbi-launches-guiding-principles-compliance-certification-program- |title=GBI Launches Guiding Principles Compliance Certification Program &#124; 2011-12-07 &#124; ED+C Magazine |publisher=Edcmag.com |date=2011-12-07 |accessdate=2013-11-12}}</ref> The principles are a set of established criteria that employ integrated design, energy performance optimization, water protection and conservation, enhancement of the indoor environment, and the reduction of material impacts as best practices to achieve federal sustainability goals.<ref>{{cite web|url=http://www.durabilityanddesign.com/news/?fuseaction=view&id=8653 |title=GBI Offers Compliance Certification : Durability + Design News |publisher=Durabilityanddesign.com |date=2012-11-05 |accessdate=2013-11-12}}</ref> The two major elements of the GPC are a compliance survey and a third-party on-site assessment. 

The GPC survey is broken down into five topic areas:
# Employ Integrated Assessment, Operation, and Management Principles
# Optimize Energy Performance 
# Protect and Conserve Water 
# Enhance Indoor Environmental Quality 
# Reduce Environmental Impact of Materials

Each of the five topic areas has 20 available points for a total of 100.<ref name="gsa" /> Agencies may choose to pursue either or both Green Globes certification and Guiding Principles Compliance.<ref>{{cite web|author= |url=https://finance.yahoo.com/news/GBI-Launches-Guiding-iw-4100678834.html |title=GBI Launches Guiding Principles Compliance Certification Program Addressing EO 13514 Requirements for Existing Buildings - Veterans Affairs Orders 180 Third-Party Assessments - Yahoo Finance |publisher=Finance.yahoo.com |date=2011-12-07 |accessdate=2013-11-12}}</ref>

==Green Globes Professional Program==
The Green Globes Professionals program (GGP) is a network of certified individuals trained to manage the Green Globes assessment and certification process. These industry professionals may act as consultants on Green Globes projects, facilitate the building certification, provide project management for their clients or employers, or make recommendation on how to achieve different ratings.<ref name="boma" /> The GGP curriculum covers green building concepts, Green Globes assessment protocols, Green Globes rating and certification, and case studies. Each lesson includes an online presentation, study guide and quiz; the course also includes a final certification test. Building professionals who successfully complete the course and pass the final test become Green Globes Certified.<ref>{{cite web|url=http://www.buildingmedia.com/gglobes_news.html |title=Building Media Inc |publisher=Buildingmedia.com |date=2009-12-18 |accessdate=2013-11-12}}</ref>

==References==
{{Reflist|colwidth=30em}}

[[Category:Sustainable building in the United States]]