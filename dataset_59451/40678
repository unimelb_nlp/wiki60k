{{Infobox writer  <!--For more information, see [[:Template:Infobox Writer/doc]].-->

| name = Clive Matson
| image = File:Clive Matson.jpg
| caption = 
| image_size = 
| alt = 
| birth_name = 
| birth_date = {{birth date and age|1941|03|13}}
| birth_place = Los Angeles, California, US
| occupation = Poet, Creative Writing Teacher 
| nationality = American
| education = [[Vista High School (Vista, California)|Vista High School]], 1958
| alma_mater = [[Columbia University]], MFA, 1989
| genre =   Lyric Poetry     
| movement = Beat Literature
| notableworks = ''Equal in Desire, Squish Boots, On the Inside'' 
| spouses =   Erin Black (1963-1968)    Gail Ford (1993-2007)  
| partner =    
| children = Ezra John Matson-Ford
| relatives = 
| awards = 
| signature = 
| signature_alt = 
| years_active = 1962-present
| module = 
| website = http://matsonpoet.com/
| portaldisp =   
}}

'''Clive Matson''' (born March 13, 1941) is an American direct expression lyric poet and creative writing teacher.

== Biography ==

===Early life===
Clive Matson was born in Los Angeles, California in 1941, the middle child of five. His father, Randolph Matson, an electrical engineer with the aircraft industry, moved his family in 1948 to an avocado orchard in San Diego County. His mother, Evelyn Vincent Matson, was an educated woman, daughter of Clement Vincent, linotypist and print shop owner in Los Angeles. A Communist, Vincent published a Spanish-language newspaper, believing the future of California belonged to its Latino population. As a child Matson was interested in rocks, studied [[James Dwight Dana]]'s ''System of Mineralogy'', and found crystals in the hills beyond the farm. His interest in science changed at the age of fourteen, when he wrote a poem for an English class. He was captivated. Matson received a full scholarship to the University of Chicago in 1958 but dropped out after attending one year, in order to pursue poetry. The seminal event came when Matson offered that [[John Milton]] wrote ''Paradise Lost'' "Because life has conflicts like that," instead of giving the literary answer, that Milton was revisiting his issues with the King of England. After briefly attending the University of California at Riverside, Matson hitchhiked around the country and then toured Europe on foot, before settling in New York City in 1962.

===Involvement with the Beats===
Matson was drawn to the East Village and welcomed into the [[Beat Generation]]. He received broad guidance from [[Diane di Prima]] and [[Allen Ginsberg]], and Irving Rosenthal introduced him to the poetry of [[John Wieners]] and [[Michael McClure]]. Friendships with John Ceely, Martha Muhs, and David Rattray brought Alden Van Buskirk to his attention, and Matson helped in the preparation of Van Buskirk's ''LAMI''.<ref>https://www.poetryfoundation.org/features/articles/detail/69834</ref> Throughout the mid-1960s Matson's most significant influence, in writing and in life, was [[Herbert Huncke]]. They discussed writing, ethics, drugs, jazz, Beat figures, and walked the streets of Manhattan, talking with a range of humanity from bums and thieves to workers, artists and members of high society. The environment was ideal for far-ranging explorations of self and the world. In the company of Erin Black and friends, Matson used marijuana, hard drugs, and psychedelics and sought inspiration in literature, in indigenous cultures, in Eastern religion, in music, and in their own creative impulses. Writing was Matson's instrument for articulating insights. The friends strove to separate from the dominant culture and to develop their more native, authentic traits. With the publication of ''Mainline to the Heart'', Matson was coming into his own. In 1968 he traveled to England (a trip planned with Huncke, who withdrew at the last minute) and read poems at University Art Festivals throughout the United Kingdom. ''Mainline'' was confiscated by British authorities and later released as "borderline pornographic." <ref>''Sunday Times'', 12/03/1968, p. 5</ref>

Matson held to and developed his youthful instincts, and this effort was supported by several Beat paradigms: One, that a poem is shaped by the poet's breath, a perception extended by [[Charles Olson]] as "projective verse." <ref>https://www.poetryfoundation.org/resources/learning/essays/detail/69406</ref> Ginsberg disseminated a similar idea from [[William Carlos Williams]], who advised him in the 1940s to "Listen to the rhythm of your own voice. Proceed intuitively by ear." <ref>Kramer, Jane, Ginsberg In America, Random House, New York, 1969, page 111.</ref> A second paradigm suggests that the source of poetry is life itself. Life as lived on streets, in jobs, in relationships, in meditation groups, on farms and in mountains, not something found through scholarship or abstraction. "No ideas but in things," <ref>''Paterson'', New Directions, New York, 1946</ref> also passed on from Williams, and [[Robert Creeley]]'s "Form is never more than an extension of content" <ref>Interview with Leonard Schwartz, http://jacket2.org/commentary/robert-creeley-conversation-leonard-schwartz</ref> were significant ideas in the conversation. Street wisdom, too, informed Beat sensibility, that a homeless drunk was more likely to see God than a professor. A third, and largely unacknowledged, dictum of the Beats is that writing is to communicate. Many times Huncke asked, "What do you mean by that?" The question was legitimate, though undercurrents among intellectuals at the time were palpable, to reframe poetry as an arcane art and make it ripe for the academies to hijack. Matson's original impulse to communicate to others, educated or not, endured for him as a common-sense virtue. A fourth feature of Beat sensibility, the longing to expand consciousness, drove much of Matson's experimentation. That desire was ubiquitous among young artists and found clear articulation in Timothy Leary's [[Turn on, tune in, drop out]]. This code expressed widespread dissatisfaction with mainstream culture and gave license to rebellion. Leary's formulation, plus the emerging hippie ethic, kept the political concerns of Matson's forebears in the foreground. Behind these paradigms, for some, lay the intuition that the feeling in the body is paramount, along with what one observes with one's own eyes. The childhood study of minerals informed Matson's sensibilities, since Dana's early science stipulated that visible qualities of crystals revealed their essence. Seeing and describing with clarity mapped fluidly into his writing. The weakness of cultural imperatives kept the field open, fertile, and largely undescribed.

In 1968 Matson began a decades-long separation from the Beats. The seeds were evident in a discussion with Ginsberg, when Matson asserted that "Every user is a pusher." Ginsberg countered, "That isn’t literally true," at least in the eyes of the judicial system.<ref>Holladay, Hilary, ''Herbert Huncke: The Times Square Hustler Who Inspired Jack Kerouac and the Beat Generation'', p. 117 Schaffner Press, Tuscon, 2013, 2015</ref> Matson's experience, especially with Huncke and Erin Black, led him to question the Beats’ elitism and their patriarchal, anti-feminist bias. Moving away from the bias that savage aspects are at the apex of our personalities, showed Matson the larger landscape. At Huncke's Memorial in 1996, Matson addressed Huncke's impulse that "the only thing you can do with the pain inside is medicate it. That's shitty, Herb." [[Gregory Corso]] complained Matson was criticizing Beat behavior, and Matson replied that other options were available.<ref>''Herbert Huncke: Final Reading & Memorial'', DVD Thin Air Video, 2006, New York, NY</ref>  Matson had become by then an avid participant in 12-step programs. Matson's most influential teachers, besides poetry and Huncke, were women. "I am a person!" Erin Black proclaimed in 1964, with proto-feminist exactness. Eila Kokkinen, who was conversant with the Beats, provided a balanced perspective and included psychoanalytic insights. Relationships with Martha Muhs, Maggie Clougherty, and Phyllis Ideal, and the general atmosphere of the 1970s engendered the sense that whatever strategies worked for Matson needed to include women.  The strategies needed to work for women, also. Fundamental threads prevail throughout Matson's work: the urge to honesty and to give messages from the body first regard. The stage was set, through training with the Beat Generation, for the development of his writing and of his career. Matson's evolution points directly toward his recent conception of "Paleo Poetry" and the primitive heart.

=== Career ===
Matson moved to the San Francisco Bay Area in 1968, settling in Oakland and keeping his main focus on writing poetry. He took odd jobs: as a warehouse clerk in San Leandro; as a taxi driver for Berkeley's Taxi Unlimited, a worker's cooperative; as a laborer for several furniture movers; and he trained as letterpress printer under Clifford Burke at Cranium Press in San Francisco. Matson acquired a hand-lever letterpress from Harold Adler, following in the footsteps of his grandfather, and then a foot-treadle 8x12 platen press from Irving Rosenthal's Free Press in San Francisco. Besides business cards, broadsides and issue number four of the ''Berkeley Poets' Cooperative'',<ref>http://hippocketpress.org/berkeley-poets-cooperative.php</ref> he printed and bound his own book ''Heroin'' and John Ceely's ''The Country is Not Frightening'' as publisher Neon Sun.<ref>Ceely, John, ''The Country is Not Frightening'', Neon Sun, Berkeley CA, 1975</ref> He found that the demands of small-press publishing undercut his energy to write, so he let that enterprise dissolve and kept job printing as a part-time endeavor. He produced fine business cards, broadsides, announcements, and a number of chapbooks for Paul Mariah of ManRoot Press in the 1980's and early 1990's<ref>''Equal in Desire'', ''The Complete Poems of Jean Genet'', Cocteau, ''Appogiatures/Grace Notes''</ref>

In 1978, while Matson was giving readings in the Northwest, he was invited by Jack Estes to teach a workshop at Peninsula State College in Port Angeles, Washington. Matson asked for guidance. "It's easy," Estes replied, "give the exercise [[David Wagoner]] gave last month. He divided the psyche into the same three parts as Transactional Analysis and renamed them Editor, Writer, and Child. Tell the Editor and Writer to take a walk and let the Child write whatever it likes."  When Matson stepped into the classroom, every comment fit those voices: analytical – from the Editor; understanding of the process – from the Writer; and impulsive or emotional – from the Child. Matson felt he was the custodian for everyone's creativity.<ref>''Let the Crazy Child Write!'', New World Library, Novato, 1998, page ix</ref> He returned to the Bay Area, took over John Oliver Simon's Learning Annex writing workshop, and tried out different articulations of the "Child" voice, eventually settling on "Crazy Child." That designation stimulated the most vigorous and most original writing. In 1985 Matson started teaching at [[University of California Berkeley]] Extension, specializing in beginning courses "Developing Your Creative Writing Style" and "Exploring Your Creative Writing Potential." He took notes for his tutorial, ''Let the Crazy Child Write!'' (published by New World Library in 1998).<ref>Review of ''Let the Crazy Child Write'' http://www.goodreads.com/book/show/1910955.Let_the_Crazy_Child_Write_</ref> He taught at various other venues: [[Pacific Oaks College]] in Pasadena, California; [[John F. Kennedy University]] in Orinda; Oakland Summer Arts Camp in the Sierra; and led writing excursions in Scotland, Italy, Costa Rica, and the eastern Sierra.  
Matson returned to New York City in 1987 and enrolled in the writing program at [[Columbia University]]. He worked with Robert Montgomery, [[Sharon Olds]], [[Robert Hass]], and [[Quincy Troupe]]. He earned his MFA in poetry from [[Columbia University School of the Arts]] in 1989 and his thesis became the basis for the poetry volume ''Squish Boots''. He joined Alan Soldofsky and Susan Lurie in organizing readings at Cody's Books in Berkeley 1979 to 1981; joined workshops with Soldofsky, [[Josephine Miles]], Robert Hass, [[Diana O'Hehir]], and Marc Linenthal; attended Squaw Valley Writers’ Conference in 1973; and met with peers from Squaw Valley for several years. With Paul Geffner, Glenn Ingersoll, and Katherine Harer he ran the "Poetry and Pizza" reading series in San Francisco from 1998 to 2010. He began the reading series "Poetry Unbound" in 2013 with Richard Loranger and Harold Adler at Art House Gallery and Cultural Center in Berkeley, which continues today. In 1993 Matson established a small, eight-page quarterly journal, ''The Scribbler'', edited by workshop participants, a role since taken over by Kayla Sussell. It continues today as one of the longest-enduring literary publications in the Bay Area and in the country. Blogs on Matson's website, matsonpoet.com, from 1998 to the present, prepare for ''Writing Your Way In'', a sequel to ''Let the Crazy Child Write!'' Following the tragic attacks of 9/11, Matson wrote ''Towers Down,'' published as a chapbook along with Diane Di Prima's ''Notes Toward a Poem of Revolution'' (Eidolon Editions, San Francisco, 2002). Matson then edited, along with Allen Cohen, ''An Eye for an Eye Makes the Whole World Blind: Poets on 9/11,'' a selection of poems by over 100 American poets.<ref>Review of ''An Eye for an Eye Makes the Whole World Blind'' http://www.nthposition.com/aneyeforaneyemakes.php</ref>  In 2009, Matson reissued his first volume of poetry as ''Mainline to the Heart and Other Poems,'' adding later poems to the collection, to further praise.<ref>Review of ''Mainline to the Heart'' http://savvyverseandwit.com/2009/04/mainline-to-heart-other-poems-by-clive.html</ref>  In 2013 he began a monthly column for Berkeley Times "On Words" about local venues from Hip-Hop and Spoken Word to postmodern poetry at UC Berkeley. Matson has affirmed throughout his career that how a poem comes across when read aloud provides clues on how it might best be revised and also is a measure of its success. Matson continues a 40-year tradition of frequent open readings in the Bay Area to experiment orally with poems in progress.  Poetry readings in Northern California, often with cellist Gael Alcock accompanying, include Bay Area events such as LitCrawl, Bay Area Generations, and [[Literary Death Match]].

=== Poetry ===
The commitment to emotional honesty and to core feelings influences the imagery and style of Matson's poems.  Marc Hofstadter located his language "at the interface of consciousness and unconsciousness."<ref>Review of ''Squish Boots'', Hofstadter, Marc, ''Healing the Split'', Dog Ear Publishing, Indianapolis, IN p. 135-137</ref> Matson would propose, instead, that it's a common state of which we are rarely aware. In his notebooks, Matson calls the poetic apparatus "machine language," as in computer language, a generative language beneath consciousness. ''Mainline to the Heart'' expressed Matson's concerns in hipster phrases and in the cool vernacular of the time, influenced by Van Buskirk and John Wieners.<ref>Review of ''Mainline to the Heart'' http://''jacketmagazine''.com/37/r-matson-rb-ring.shtml</ref> "A state of mind most people don’t know even exists," wrote Jack Foley.<ref>comments in ''Mainline to the Heart and Other Poems'', Regent Press, Berkeley, 2009, pp. 1-2, first published by Diane di Prima's Poets Press in 1966</ref> During that period he began writing two-line poems, take-offs of John Wieners’ couplets and of [[Sappho]]'s fragments, and continues those to the present. These are sympathetic to Ginsberg's American Sentences,<ref>http://paulenelson.com/american-sentences-2/</ref> formulated as a Western, and down-to-earth, version of Japanese forms. ''Space Age'' (Croton Press, NY: 1969) added free-flowing, satiric portraits, inspired in part by the lyrics of [[Bob Dylan]] and apocalyptic visions of Van Buskirk. Matson's political awareness found some larger expression, largely as criticism or riffs on the culture, in that volume and also in shorter poems written through the 1970s.<ref>Chuck Cohen, ''Berkeley Poetry Review'', Spring 1975, p. 58</ref> ''Heroin'' (Neon Sun, 1972) recounts Matson's journey with the hard drug, highlighting positive aspects of what the drug teaches. Psychological ironies and challenges are a continual focus, and the volume includes a lengthy reckoning of how to glean value from the drug and how to dodge its pitfalls of both physical and psychological addiction.<ref>Jon Ford, ''Poetry Flash'' #5, April 1973, p. 2,</ref><ref>Charley Shively, ''The Boston Phoenix,''12/18/1973, p. 14</ref> Ginsberg, on hearing the shorter poems, called them "direct expression."<ref>Personal communication, Intersection Coffeehouse, San Francisco, 1973</ref> These became material for sections of ''On the Inside'' (Cherry Valley Editions, NY: 1982). That volume is an overview of progressive efforts after the turmoil of the 1960s faded from public consciousness. It's a largely intuitive assessment of areas where viable change is in progress, and ends with the observation that cooperation is reliably at the basis of human interactions.<ref>Brown Miller, ''San Francisco Review of Books,'' 1982</ref> Matson sees this as hopeful for progressive change, and this thought is confirmed by recent research.<ref>Curtis W. Marean, "The Most Invasive Species of All," ''Scientific American'', August 2015, v.313, No.2, pp. 32-39</ref>

Matson found valuable support in [[Vipassana]] meditation and extensive psychotherapy during these later decades. Both endeavors brought depth to poems about relationships. ''Equal in Desire'' (ManRoot, San Francisco, 1982 and 1983) also expanded the direct expression style into love poems, at the same time including awareness of gender and pro-feminist concerns.<ref>Max Lerner, New York Post, 1982</ref> ''Hourglass'' (Seagull Press, Oakland, CA: 1987) is a series of eight-syllable line sonnets with rhyme and half-rhyme schemes, each with a creative process travelogue in prose. All the poems celebrate meditation and the relation of mind and body, along with struggles inherent in establishing a practice. These came as his meditation took hold and blossomed. After he began Crazy Child workshops, Matson discovered he was one of many who, on cracking open the door to the creative unconscious, was overwhelmed by floods of images. Chaotic first drafts were eventually shaped into ''Squish Boots'' (Broken Shadow Publications, Oakland, CA: 2002), concerned with childhood and core family issues. The process fit William Carlos Williams’ teaching, "The beauty of writing is…the discovery of something you DON’T know, rather than the synthesis [or] repetition of the things you do already know. It's a jump forward into life, unknown future life…."<ref>Kramer, page 173</ref> Emotional daring and openness helped this effort contribute to the voice of ''Chalcedony Songs'' (Minotaur Press, WA: 2007, 2009). These came as protest of one of Matson's female characters, in an unfinished story, who faked her orgasms. ''Chalcedony'' explores intricate layers of love and relationship in an open and easily involved manner. Matson stayed with the challenge. He wrote from a female perspective and let that voice expand.<ref>Armand Croft, https://www.amazon.com/review/R3DXZS0E7PZGDD</ref> The style, added to the consciousness of ''Squish Boots'', developed into Matson's most authentic voice to date. In the background resonates his learning from Ancient Greek Classics, William Shakespeare, Walt Whitman, Ranier Maria Rilke, Pablo Neruda and Rumi, as well as contemporary poets. The voice aided in making a flexible structure for a series of poems, ''War Allies'', inspired by difficult political times toward the end of the 20th Century, by 9/11, and by the encroachment of marketing into our private lives. The collection is scheduled for production by Torean Horn Press of Sonoma, California, in 2018. These poems, along with ''Chalcedony'' and current work, indicate that the primitive heart is at the basis of Matson's effort. His poems evolve in a roughly straight line from Beat lyrics to direct expression to a deeper level of language, involving the primitive heart and limbic system. The short essay ''Being Present: Paleo Poetry'' came about with help from Marie Martin, painter and childhood friend, and from John Paige (formerly Ceely) who has remained an insightful advisor since 1964. The essay, under consideration by Ambush of San Francisco, delineates the term "Paleo Poetry" precisely.

== Honors and Awards ==
In 2003 Matson won, along with co-editor [[Allen Cohen (poet)|Allen Cohen]], the [[PEN Oakland/Josephine Miles Literary Award]] for editing ''An Eye for an Eye Makes the Whole World Blind -- Poets on 9/11''.<ref>http://www.sfgate.com/entertainment/article/9-11-01-POETRY-INSPIRED-BY-THE-ATTACKS-2799842.php</ref> Best Writing Teacher, Best of the East Bay 2006.<ref>“Arts & Culture,” ''East Bay Express'', Berkeley, California: May 3-9, 2006, p. 34</ref> The City of Berkeley awarded him its Lifetime Achievement Award in Poetry in 2012.<ref>http://litseen.com/clive-matson-honored-at-10th-annual-berkeley-poetry-festival-lifetime-achievement-award-on-sat-may-5/</ref>

== Bibliography ==
Volumes of Poetry: 
* ''Chalcedony's Second Ten Songs'' (2009) ISBN 978-1879457966
* ''Mainline to the Heart and Other Poems'' (2009) ISBN 978-1587901393
* ''Chalcedony's First Ten Songs'' (2007) ISBN 978-1879457706
* ''Squish Boots'' (2002) ISBN 0-9636156-2-9
* ''An Eye for an Eye Makes the Whole World Blind'', co-editor with Allen Cohen, (2002) ISBN 978-1587900341
* ''Hourglass''(1987)
* ''Equal in Desire'' (1983)  
* ''On the Inside'' (1982)  
* ''Heroin'' (1972)  
* ''Space Age'' (1969)  
* ''Mainline to the Heart'' (1966)

Poems featured in over a dozen anthologies, including:
* ''Passionate Hearts'' (New World Library, 1997)
* ''Hang Together'' (Hanging Loose, 1987)
* ''Loves, etc.'' (Doubleday, 1973)
* ''31 New American Poets'' (Hill & Wang, 1969)

Poems published in more than one hundred journals, including:
* [[Ole' (magazine)]],''The Great Society'', ''Berkeley Poetry Review'', ''Blue Unicorn'', ''Dalmo'Ma'', ''Exquisite Corpse'', ''Actor'' (Mexico City), ''Hawaii Review'', ''Intrepid'', ''Jeopardy'', ''Nimrod'', ''Northern Contours'', [http://poetrymagazine.com poetrymagazine.com], ''Rattle'', ''Silver'', ''Tattoo Highway'', ''The Centennial Review'', ''Vision International'', ''Yellow Silk''.

Poetry chapbooks:
* ''Towers Down'' with Diane di Prima (Eidolon Editions, 2002)
* ''Shaved at Dawn'' with John Simon (Aldebaran Review, 1984)

Fiction:
* "Search" in ''Soundings East'' (Salem State College, 2000)
* "Asbestos" in ''Santa Clara Review'' (Fall/Winter 2003)
* "Guards" in ''Word 60'' (New York School of Visual Arts, Fall 2004)
* "Cache" in ''Tulane Review'' (Spring 2005)

Essays (partial listing):
* "All Poetics Are Local: Louis Cuneo and the Berkeley Poetry Festival" (''Caveat Lector'', 2015)
* "Remembering John Wieners," (''Montessart Review'', no. 7, ed. Calder Lowe: fall 2003)
* "Membrane Porous," ''The Spirit of Writing'', ed. Waldman (Tarcher, 2001)
* "Going Public" (''Poets & Writers'', vol. 25, no. 2, March/April 1997)
* "Robert Duncan and His Audience" (''Exquisite Corpse'', #57, 1996) 
* "Mapplethorpe: The Censorship of the Senses" (''Culture Concrete'', 1992)
* "Breath of Inspiration" (''Presumptions'', 1987)

Nonfiction Volume:
*''Let the Crazy Child Write!'' (New World Library, 1998) 257 pp

==Sources==

*Hofstadter, Marc, "Squish Boots," "Mainline to the Heart" in ''Healing the Split, The Collected Essays of Marc Elihu Hofstadter'' 135-141. Dog Ear Publishing,  (2011)

External Links:

*Clive Matson bio [http://www.pw.org/content/clive_matson_3]
*Interview http://www.blogtalkradio.com/the-jane-crown-show/2009/08/22/clive-matson
*http://www.revisitations.com/spring_2010/fiction/Search_Clive_Matson.html
*https://archive.org/details/BeatPoetAldenVanBuskirkMemorial

==References==
{{Reflist}}

{{authority control}}

{{DEFAULTSORT:Matson, Clive}}
[[Category:21st-century American poets]]
[[Category:Poets from California]]
[[Category:Writers from Los Angeles]]
[[Category:University of California, Riverside alumni]]
[[Category:Columbia University alumni]]
[[Category:1941 births]]
[[Category:Living people]]
[[Category:Poets from New York]]
[[Category:Writers from New York City]]