{{Orphan|date=June 2016}}

{{Infobox scientist
| name              = Roberta Farrell
| image             = Roberta Lee Farrell 2012 at Discovery Hut, Hut Point, Ross Island, Antarctica.jpg
| caption           = Roberta Farrell at [[Hut Point]]
| birth_name        = 
| nationality       = United States
| fields            = [[Decomposition|Wood degradation]]<br>[[Bioremediation]]<br>[[Fungi]]<br>[[Enzymology]]
| workplaces        = BSc [[University of Missouri]]<br>PhD [[University of Illinois]]
| alma_mater        = [[University of Illinois]]
| website           = [http://sci.waikato.ac.nz/about-us/people/rfarrell Farrell at the University of Waikato]
}}

'''Roberta Lee Farrell''' {{post-nominals|country=NZL|CNZM|FRSNZ|FIAWS}} is Emeritus Professor at the [[University of Waikato]], New Zealand and a researcher of international renown in the fields of [[Decomposition|wood degradation]], [[bioremediation]], [[mycology]] and [[enzymology]].<ref name=":0">{{Cite web|url=http://www.voxy.co.nz/national/university-waikato-awards-title-emeritus-professor/5/168144|title=University of Waikato awards the title of Emeritus Professor|website=www.voxy.co.nz|access-date=2016-06-05}}</ref><ref name=":1">{{Cite web|url=http://sci.waikato.ac.nz/about-us/people/rfarrell|title=Roberta Farrell - Faculty of Science and Engineering : University of Waikato|website=sci.waikato.ac.nz|access-date=2016-06-05}}</ref>

==Early life and education==
Originally from the USA,<ref name=":2">{{Cite news|url=http://www.stuff.co.nz/business/industries/5759501/The-2011-influencers|title=The 2011 influencers|date=2011-10-10|newspaper=Stuff.co.nz|language=English|access-date=2016-06-05}}</ref> Farrell received her bachelor's degree from the [[University of Missouri]], St. Louis<ref name=":3">{{Cite web|url=http://www.umsl.edu/chemistry/Alumni%20Interests/RobertaL.FarrellBS1975.html|title=Roberta L. Farrell - Research interests|last=|first=|date=|website=umsl.edu|publisher=University of Missouri–St. Louis|access-date=2016-06-05}}</ref> followed by an MSc and PhD from the [[University of Illinois at Urbana–Champaign|University of Illinois]], Urbana.<ref name=":1" /><ref name=":3" /> Farrell conducted postdoctoral research at the [[University of Chicago]] 1979-1980 and at The Massachusetts Institute of Technology [[MIT]] from 1981 to 1984.<ref name=":3" />

==Career and impact==
Farrell started her career as a [[Biotechnology|biotechnologist]] in the USA, as Associate Director of Research - Industrial Enzymes with [[Repligen]] Corporation in Cambridge, Massachusetts.<ref name=":3" /> She then became Executive Vice President and Chief Operating Officer of Sandoz Chemicals Biotech Research Corporation and Repligen Sandoz Research Corporation.<ref name=":0" /><ref name=":3" />  In 1995 she took a sabbatical PAPRO, Forest Research Institute in Rotorua, New Zealand<ref>{{Cite web|url=http://www.umsl.edu/chemistry/Departmental%20News/documents/ScienceinAntarcticacoloredwithlogo.pdf|title=Science in Antarctica: Studying the Historic Huts of the Heroic Era of Antarctic Exploration|last=|first=|date=2008-10-24|website=umsl.edu|publisher=University of Missouri-St. Louis|access-date=}}</ref> and shortly after she emigrated and became a professor at [[University of Waikato|Waikato University]].<ref name=":0" />

Since 2013, Farrell has been an Emeritus Professor at the University of Waikato in Hamilton, New Zealand.<ref name=":0" /> She also was an Adjunct Professor at [[North Carolina State University]] from 2008 until 2013.<ref name=":1" /><ref name=":4">{{Cite news|url=http://www.stuff.co.nz/waikato-times/news/779713/Academic-hailed-in-her-adopted-country|title=Academic hailed in her adopted country|date=2008-12-31|newspaper=Stuff.co.nz|language=English|access-date=2016-06-05}}</ref> Farrell is recognized as an expert in cold-tolerant fungi and in the microbes and enzymes that affect wood;<ref name=":4" /> her work has had substantial impact in the biochemistry and biotechnology fields, specifically in the areas of [[enzymes]], fungal [[ecology]], [[Biological pest control|bio-control]], and [[cellulose]].<ref name=":2" /><ref name=":4" /><ref name=":5">{{Cite web|url=https://www.waikato.ac.nz/php/research.php?mode=show&author=102746|title=Research Publications for Roberta L Farrell: University of Waikato|website=www.waikato.ac.nz|access-date=2016-06-05}}</ref>

Farrell also has led an international team on numerous expeditions to Antarctica.<ref name=":0" /> Her work in Antarctica has contributed to an understanding of the degradation of the Antarctic historic huts and artifacts and associated fungi,<ref name=":0" /><ref name=":2" /><ref>{{Cite news|url=http://news.bbc.co.uk/2/hi/science/nature/1695897.stm|title=Scott's hut needs urgent repair|date=2001-12-07|newspaper=BBC|access-date=2016-06-05}}</ref><ref>{{Cite web|url=http://researchcommons.waikato.ac.nz/handle/10289/2043|title=Scientific evaluation of deterioration of historic huts of Ross Island, Antarctica|last=Farrell|first=Roberta|date=2007|website=|publisher=New Zealand Science Teacher|access-date=}}</ref> and she has performed critical work in the preservation of [[Ernest Shackleton|Ernest Shackleton's]] and [[Robert Scott (explorer)|Robert Scott's]] huts.<ref name=":0" /><ref name=":4" /> She also contributed to some of the pivotal discoveries on microbiology of the [[McMurdo Dry Valleys]].<ref name=":0" />

Farrell also remains connected to private sector scientific research, holding an ongoing Directorship for Parrac Limited, where she is a founding scientist.<ref name=":2" /><ref name=":6">{{Cite web|url=http://sciencelearn.org.nz/Science-Stories/Celebrating-Science/2008-Awards/Taking-care-of-business|title=Taking care of business|website=Sciencelearn Hub|access-date=2016-06-05}}</ref><ref>{{Cite web|url=http://www.parrac.co.nz|title=Parrac Limited - Owner of the intellectual property, know-how and strains of albino Ophiostoma piliferum, known as Cartapip 97, and as Sylvanex 97™|last=Interspeed.co.nz|website=www.parrac.co.nz|access-date=2016-06-05}}</ref> She also was Founding Scientist of ZyGEM Ltd.<ref name=":2" /><ref name=":6" />

In total, Farrell has published over 100 research papers and book chapters,<ref name=":0" /><ref name=":5" /> and holds 30 patents.<ref name=":0" /> Her research was featured in the documentary “''The Green Chain''" as well as in Māori Television’s “''Project Matauranga''”.<ref name=":0" />

==Awards and honours==
Select awards and honours include:

2015 - Appointment to University of Pretoria, Pretoria, South Africa Centre of Microbial Ecology and Genomics Advisory Board.<ref name=":1" />

2011 - Named Influencer of the Year, Unlimited Magazine, Auckland, NZ.<ref name=":0" />

2009 -  Companion of The New Zealand Order of Merit for services to biochemical research.<ref name=":0" /><ref name=":2" /><ref name=":4" />  Distinguished Alumni Award, University of Missouri-St Louis.<ref>{{Cite web|url=http://www.umsl.edu/chemistry/Alumni%20Interests/pda.html|title=UMSL Distinguished Alumni Awards|last=|first=|date=|website=umsl.edu|publisher=University of Missouri–St. Louis|access-date=}}</ref>

2008 - Science Entrepreneur of the Year award at KuDos, the Hamilton Science Excellence Awards,<ref name=":4" /> appointment to Board New Zealand Foundation for Research Science and Technology.<ref name=":2" />

2007 - Invitation to present Sir Holmes Miller Lecture, Wellington Branch New Zealand Antarctic Society, 22 November 2007.<ref name=":0" /><ref name=":1" />

2005 - Elected Fellow of [[The Royal Society of New Zealand]].<ref name=":0" /><ref name=":1" /><ref>{{Cite web|url=http://www.royalsociety.org.nz/organisation/academy/fellowship/current-fellows/?f=roberta-farrell|title=List of Current Fellows of the Royal Society of New Zealand|website=Royal Society of New Zealand|language=en-us|access-date=2016-06-05}}</ref>

1998 - Elected to the Academy Board of International Academy of Wood Science (1998 – 2004).<ref name=":0" /><ref name=":1" />

1990 - Elected Fellow of [[International Academy of Wood Science]]

== Selected works ==
* Kirk, T. Kent, and Roberta L. Farrell. "Enzymatic" combustion": the microbial degradation of lignin." ''Annual Reviews in Microbiology'' 41.1 (1987): 465-501.
* Kirk, T. Kent, et al. "Production of multiple ligninases by Phanerochaete chrysosporium: effect of selected growth conditions and use of a mutant strain." ''Enzyme and Microbial technology'' 8.1 (1986): 27-32.
* Farrell, Roberta L., et al. "Physical and enzymatic properties of lignin peroxidase isoenzymes from Phanerochaete chrysosporium." ''Enzyme and microbial technology'' 11.6 (1989): 322-328.
* Farrell, R.L., Blanchette, R.A., Brush, T.S., Hadar, Y., Iverson, S., Krisa, K., Wendler, P.A., Zimmerman, W.  (1993).  Cartapip™:  a biopulping product for control of pitch and resin acid problems in pulp mills.  J. Biotechnol.  30:  115-122.
* Farrell, R.L. (1998). Science, technology and end-users: optimising R&D for the commercial sector for society’s benefits. In Leadership Priorities for New Zealand Science and Technology sponsored by the Academy Council of The Royal Society of New Zealand, 5–6 November 1998. Miscellaneous series 54, pp.&nbsp;127–131.
* Pointing, S.B., Chan,Y., Lacap, D.C., Lau, M.C.Y., Jurgens, J., Farrell,R.L.. 2009. Highly specialized microbial diversity in hyper-arid polar desert . Proc Natl Acad Sci.USA, volume 106 no. 47 19964-19969. doi: 10.1073/pnas.0908274106. Epub 2009 Oct 22.
* Farrell, R.L., Arenz, B.E., Duncan, S.M., Held, B.W. Jurgens, J.A., Blanchette, R.A. 2011. Introduced and Indigenous Fungi of the Ross Island Historic Huts and Pristine Areas of Antarctica. Polar Biology.  doi: 10.1007/s00300-011-1060-8.
* Chan Y., Van Nostrand J.D., Zhou J., Pointing, S.B., Farrell, R.L. 2013. Functional ecology of an Antarctic Dry Valleys landscape. Proc Natl Acad Sci (USA) 110:  8990-8995.  doi: 10.1073/pnas.1300643110. Epub 2013 May 13.

==References==
{{Reflist|35em}}

==External links==
* [http://sci.waikato.ac.nz/about-us/people/rfarrell Roberta Farrell's webpage]
* [https://scholar.google.com.au/citations?user=1uyFmSgAAAAJ&hl=en&oi=ao Roberta Farrell] on [[Google scholar]]

{{Authority control}}

<!-- Categories for this template, but not the final article -->

<!-- Categories for the final article, but not this template -->

{{DEFAULTSORT:Farrell, Roberta}}
[[Category:University of Waikato faculty]]
[[Category:Created via preloaddraft]]
[[Category:New Zealand women scientists]]
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:Antarctic scientists]]
[[Category:New Zealand biochemists]]
[[Category:New Zealand mycologists]]
[[Category:University of Missouri alumni]]
[[Category:University of Illinois alumni]]
[[Category:North Carolina State University faculty]]
[[Category:Women Antarctic scientists]]