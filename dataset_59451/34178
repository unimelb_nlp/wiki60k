{{Infobox ice hockey player
| image = Vezina19221924.JPG
| image_size = 230px
| position = [[Goaltender]]
| catches = Left
| height_ft = 5
| height_in = 6
| weight_lb = 185
| played_for = [[Montreal Canadiens]]
| birth_date = {{birth date|1887|1|21|mf=y}}
| birth_place = [[Chicoutimi]], [[Quebec|QC]], [[Canada|CAN]]
| death_date = {{death date and age|mf=yes|1926|3|27|1887|1|21}}
| death_place = [[Chicoutimi]], [[Quebec|QC]], [[Canada|CAN]]
| career_start = 1910
| career_end = 1925
| halloffame = 1945
}}
'''Joseph Georges Gonzague Vézina''' ({{IPAc-en|ˈ|v|ɛ|z|ᵻ|n|ə}}; {{IPA-fr|ʒɔʁʒ vezina|lang}}; {{Nowrap|January 21}}, 1887&nbsp;– {{Nowrap|March 27}}, 1926) was a Canadian professional [[ice hockey]] [[goaltender]] who played seven seasons in the [[National Hockey Association]] (NHA) and nine in the [[National Hockey League]] (NHL), all with the [[Montreal Canadiens]]. After being signed by the Canadiens in 1910, Vézina played in 327 consecutive regular season games and a further 39 playoff games, before leaving early during a game in 1925 due to illness. Vézina was diagnosed with [[tuberculosis]], and died on {{Nowrap|March 27}}, 1926.

The only [[goaltender]] to play for the Canadiens between 1910 and 1925, Vézina helped the team win the [[Stanley Cup]] in [[1916 Stanley Cup Finals|1916]] and [[1924 Stanley Cup Finals|1924]], while reaching the [[Stanley Cup Finals]] three more times. Nicknamed the "Chicoutimi Cucumber" for his calm composure while in goal, Vézina allowed the fewest goals in the league seven times in his career: four times in the NHA and three times in the NHL. In 1918, Vézina became the first NHL goaltender to both record a shutout and earn an assist on a goal. At the start of the [[1926–27 NHL season]], the Canadiens donated the [[Vezina Trophy]] to the NHL as an award to the goaltender who allowed the fewest goals during the season. Since 1981, the award has been given to the most outstanding goaltender as determined by a vote of NHL general managers. In Vézina's hometown of Chicoutimi, the sports arena is named the [[Centre Georges-Vézina]] in his honour. When the [[Hockey Hall of Fame]] opened in 1945, Vézina was one of the original nine inductees, and in 2017 the NHL included him on their list of the 100 greatest players in league history.

==Personal life==
Georges, the youngest of eight children, was born on {{Nowrap|January 21}}, 1887, in [[Chicoutimi]], [[Quebec]], to Jacques Vézina, a local baker and an immigrant from St. Nicolas de La Rochelle in France, and his wife Clara.<ref>{{harvnb|Falla|2008|p=141}}</ref> Georges attended school at the Petit Séminaire de Chicoutimi until the age of fourteen, when he left the school to help at his father's bakery.<ref name="Dictionary">{{Citation|url=http://www.biographi.ca/009004-119.01-e.php?&id_nbr=7979&interval=20&&PHPSESSID=alkcc4d9686rh2slqkvvvvtfj1|title=Georges Vézina Page|accessdate=2008-11-27|publisher=Dictionary of Canadian Biography Online|year=2000|author=Vigneault, Michel}}</ref> He played hockey from a young age, participating in informal street hockey matches with others his own age.<ref name="Jenish 2008 pp=34–35">{{harvnb|Jenish|2008|pp=34–35}}</ref> Vézina partook in these matches in his shoes, and used skates for the first time at age 16 when he joined the local team in Chicoutimi.<ref name="One on One with Georges Vezina">{{Citation|url=https://www.hhof.com/htmlSpotlight/spot_oneononep194512.shtml|title=One on One with Georges Vezina|accessdate=2008-11-24|publisher=Legends of Hockey|date=2008-11-07|author=Shea, Kevin}}</ref> As Chicoutimi was in a remote area of [[Quebec]], more than 200 kilometres from [[Quebec City]], the hockey club was not in any organised league. Rather the club, known as the Saguenéens ("People from the Saguenay", the region where Chicoutimi is located), toured the province, playing exhibition games against a variety of clubs.<ref name="Jenish 2008 pp=34–35"/>

Vézina married Marie-Adélaïde-Stella Morin on {{Nowrap|June 3}}, 1908, in Chicoutimi.<ref name="Dictionary"/> After Vézina's death, it was reported that he fathered 22 children. This rumour was started when the Canadiens' manager, [[Leo Dandurand]], told reporters that Vézina "speaks no English and has twenty-two children, including three sets of triplets, and they were all born in the space of nine years." In actuality the Vézinas only had two children and Georges spoke broken English.<ref>{{harvnb|Jenish|2008|pp=67–68}}</ref> In 1912 they had their first child, a son named Jean-Jules. A second son was born the night of the Montreal Canadiens' first Stanley Cup win in 1916. To honour the event, Georges named the child Marcel Stanley.<ref>{{harvnb|Jenish|2008|p=42}}</ref> When not playing hockey, Vézina operated a [[Tanning (leather)|tannery]] in Chicoutimi, living a quiet life.<ref name="Legends of Hockey">{{Citation|url=http://www.legendsofhockey.net/LegendsOfHockey/jsp/LegendsMember.jsp?mem=p194512&type=Player&page=bio&list=ByName#photo|title=Georges Vézina Page|accessdate=2008-11-24|publisher=LegendsofHockey.net|year=2008|author=Legends of Hockey}}</ref>

==Playing career==

===NHA===
[[Image:Vezina1909.jpg|right|thumb|150px|Vézina while a member of the Chicoutimi Hockey Club]]
On February 17, 1910, the Chicoutimi Hockey Club played an exhibition match against the [[Montreal Canadiens]].<ref name="One on One with Georges Vezina"/> Though playing an inferior team the Canadiens failed to score a goal, losing the game. This prompted [[Joseph Cattarinich]], goaltender for the Canadiens, to convince his team to offer a tryout to Georges Vézina, who was Chicoutimi's goaltender.<ref name="Weir 1999 128">{{harvnb|Weir|Chapman|Weir|1999|p=128}}</ref> Vézina initially refused the offer, staying in Chicoutimi until the Canadiens returned in December of that year. This time they convinced Georges, along with his brother [[Pierre Vézina (ice hockey)|Pierre]], to come to Montreal. The two Vézina brothers arrived on {{Nowrap|December 22}}, 1910.<ref name="Jenish 2008 pp=34–35"/> While Pierre failed to make the team, Georges impressed the Canadiens, especially with the use of his stick to block shots.<ref>{{harvnb|Leonetti|Beliveau|2004|p=37}}</ref> Vézina was signed to a contract for [[Canadian dollar|C$]]800 per season,<ref name="Dictionary"/><ref name="One on One with Georges Vezina"/> and made his professional debut {{Nowrap|December 31}}, 1910, against the [[Ottawa Senators (original)|Ottawa Senators]].<ref name="Jenish 2008 pp=34–35"/> He would play all 16 games for the Canadiens in the [[1910–11 NHA season|1910–11 season]], finishing with a record of eight wins and eight losses, while allowing the fewest goals in the league.

[[Image:Vezina19101920.jpg|left|thumb|200px|Georges Vézina with the Canadiens early in his career]]
The following [[1911–12 NHA season|season]] Vézina again led the league in goals against, as well as winning eight games, along with 10 losses.<ref name="Legends of Hockey"/> Vézina recorded his first career [[shutout]] during the [[1912–13 NHA season|1912–13 season]], defeating Ottawa 6–0 on {{Nowrap|January 18}}, 1913, for one of his nine wins in the season.<ref name="1912-13">{{Citation|url=http://ourhistory.canadiens.com/season/1912-1913|title=1912–13 Season|accessdate=2009-04-06|publisher=Canadiens.com|year=2009|author=Montreal Canadiens}}</ref> The Canadiens finished first in the NHA for the first time in [[1913–14 NHA season|1913–14]], in a tie with the [[Toronto Blueshirts]]. Once again, Vézina led the league with the fewest goals against, while posting 13 victories and seven losses. Under the NHA rules, the first place team would play in the Stanley Cup Finals, but due to the tie for first, the Canadiens had to play a two-game, total-goals series against Toronto. Vézina shut out the Blueshirts in the first game, a 2–0 win for Montreal, but let in six goals in the second game, allowing the Blueshirts to play for the Stanley Cup, which they won.

After losing 14 games and finishing last in the NHA in [[1914–15 NHA season|1914–15]], Vézina and the Canadiens won 16 games during the [[1915–16 NHA season|1915–16 season]], placing the team first in the league. As league leaders, the Canadiens earned the right to play in the [[1916 Stanley Cup Finals]], where they faced off against the [[Portland Rosebuds (hockey)|Portland Rosebuds]], champions of the rival [[Pacific Coast Hockey Association]]. The Canadiens defeated the Rosebuds three games to two in the best-of-five-games series, winning the Stanley Cup for the first time in team history.<ref>{{harvnb|Podnieks|2004|p=48}}</ref> Vézina's second son was born the night of the fifth game, which coupled with a bonus of $238 each member of the Canadiens received for the championship, led to him considering the series as the pinnacle of his career.<ref name="Pinnacle">{{Citation|url=http://www.legendsofhockey.net/html/spot_pinnaclep194512.htm |title=Georges Vézina, Pinnacle |accessdate=2008-11-25 |publisher=LegendsofHockey.net |year=2008 |author=Shea, Kevin |deadurl=yes |archiveurl=https://web.archive.org/web/20100706101809/http://www.legendsofhockey.net/html/spot_pinnaclep194512.htm |archivedate=2010-07-06 |df= }}</ref> The following [[1916–17 NHA season|season]] Vézina again led the NHA with the fewest goals against, the fourth time in seven years he did so, helping the Canadiens to again reach the [[1917 Stanley Cup Finals|Stanley Cup Finals]], where they lost to the [[Seattle Metropolitans]].

===NHL===
The NHA gave way to the [[National Hockey League]] (NHL) in {{Nowrap|November 1917}}, with Vézina and the Canadiens joining the new league. On {{Nowrap|February 18}}, 1918, he became the first goaltender in NHL history to record a shutout, by blanking the [[Toronto Arenas|Torontos]] 9–0.<ref name="Shutout">{{Citation|url=http://www.nhl.com/ice/news.htm?id=394414|title=Emerging from the shadows to greatness |accessdate=2008-11-26|publisher=NHL.com|year=2008|author=McGourty, John}}</ref> On December 28, 1918, he became the first goaltender to be credited with an assist, on a goal by [[Newsy Lalonde]], who had just picked up the puck after a save by Vézina.<ref>{{harvnb|Allen|Duff|Bower|2002|p=187}}</ref> He finished the [[1917–18 NHL season|season]] with 12 wins, allowing the fewest goals against.<ref>{{harvnb|Hughes|Fischler|Romain|Duplacey|2003|p=32}}</ref> Vézina also set a record, which was shared with [[Clint Benedict]] of the [[Ottawa Senators (original)|Ottawa Senators]], for the fewest shutouts needed to lead the league, with one.<ref>{{harvnb|Weekes|Banks|2004|p=91}}</ref>

In [[1918–19 NHL season|1918–19]] Vézina won 10 games and helped the Canadiens defeat the Ottawa Senators in the NHL playoffs for the right to play for the Stanley Cup against the PCHA champion, the Seattle Metropolitans. Held in [[Seattle]], the two teams were tied in the best-of-five series when it was cancelled due to the [[Spanish flu]] epidemic, the first time the Stanley Cup was not awarded.<ref>{{harvnb|Hughes|Fischler|Romain|Duplacey|2003|p=34}}</ref> In the 10 playoff games prior to the cancellation, Vézina had won six games, lost three and tied one, with one shutout. Vézina recorded nearly identical records the next two seasons, with 13 wins, 11 losses and a [[goals against average]] above four in both [[1919–20 NHL season|1919–20]] and [[1920–21 NHL season|1920–21]]. He won 12 games the following [[1921–22 NHL season|season]], as the Canadiens again failed to qualify for the Stanley Cup.<ref name="Diamond 2002 550">{{harvnb|Diamond|2002|p=550}}</ref>

[[Image:Vezina19191921.JPG|right|thumb|200px|Georges Vézina c. 1919–21. He led the Canadiens to their first two Stanley Cup championships.]]
After winning 13 games in [[1922–23 NHL season|1922–23]], Vézina led the Canadiens into the NHL playoffs, where they lost the two-game, total-goal series to the Senators, who would win the Stanley Cup. The following [[1923–24 NHL season|season]] saw Vézina return to leading the league in fewest goals against. His average of 1.97 goals per game was the first time a goaltender had averaged fewer than two goals against per game.<ref>{{harvnb|National Hockey League|2008|p=187}}</ref> With another 13-win season in [[1923–24 NHL season|1923–24]], the Canadiens reached the NHL playoffs, where they again faced the Ottawa Senators. This time the Canadiens won the series, then defeated the [[Vancouver Maroons]] of the PCHA before reaching the [[1924 Stanley Cup Finals|Stanley Cup Finals]] for the first time in five years. Playing the [[Calgary Tigers]] of the [[Western Canada Hockey League]], Vézina and the Canadiens won the best-of-three series two games to none, as Vézina recorded a shutout in the second game.<ref>{{harvnb|Hughes|Fischler|Romain|Duplacey|2003|p=57}}</ref> The championship was the Canadiens' first as a member of the NHL and second title as a club. After a 17-win season in [[1924–25 NHL season|1924–25]] where Vézina recorded a goals-against average of 1.81 to again lead the league, the Canadiens reached the [[1925 Stanley Cup Finals|Stanley Cup Finals]]. The Canadiens only qualified after the [[Hamilton Tigers (ice hockey)|Hamilton Tigers]], the regular season champions, were suspended for refusing to play in the playoffs unless they were paid more.<ref>{{harvnb|Hughes|Fischler|Romain|Duplacey|2003|p=60}}</ref> Facing the [[Victoria Cougars]], the Canadiens lost the series three games to one.

Returning to Montreal for training camp for the [[1925–26 NHL season|1925–26 season]], Vézina was noticeably ill, though he said nothing about it. By the time of the Canadiens' first game on {{Nowrap|November 28}} against the [[Pittsburgh Pirates (NHL)|Pittsburgh Pirates]], he had lost 35 pounds in a span of six weeks,<ref>{{harvnb|Diamond|2002|p=125}}</ref> and had a fever of 102 Fahrenheit. Regardless, he took to the ice, and completed the first period without allowing a goal. Vézina began vomiting blood in the intermission before returning for the start of the second period.<ref name="Dryden 1997 141">{{harvnb|Dryden|1997|p=141}}</ref> He then collapsed in his goal area, and left the game, with former U.S. Olympic team goaltender [[Alphonse Lacroix]] taking his place.<ref name="One on One with Georges Vezina"/>

The day after the game, Vézina was diagnosed with tuberculosis and advised to return home.<ref name="Jenish 2008 68">{{harvnb|Jenish|2008|p=68}}</ref> He made a last trip into the Canadiens' dressing room on {{Nowrap|December 3}} to say a final goodbye to his teammates. Dandurand would later describe Vézina as sitting in his corner of the dressing room with "tears rolling down his cheeks. He was looking at his old pads and skates that Eddie Dufour [the Canadiens trainer] had arranged in Georges' corner. Then, he asked one little favour—the sweater he had worn in the last world series."<ref name="One on One with Georges Vezina"/> Vézina returned to his hometown of Chicoutimi with his wife Marie, where he died in the early hours on Saturday, March 27, 1926, at l'Hôtel-Dieu hospital. Though he played only one period for the Canadiens during the entire season, the team honoured his entire $6,000 salary, a testament to how important Vézina had been to the team.<ref name="Jenish 2008 68"/>

==Legacy==
{{Quote box| quote ="Vézina was a pale, narrow-featured fellow, almost frail-looking, yet remarkably good with his stick. He'd pick off more shots with it than he did with his glove. He stood upright in the net and scarcely ever left his feet; he simply played all his shots in a standing position. He always wore a toque—a small, knitted hat with no brim in Montreal colours – bleu, blanc et rouge. I also remember him as the coolest man I ever saw, absolutely imperturbable."|align=right |width=30%|source=—[[Frank Boucher]], player and coach for the [[New York Rangers]]<ref name="One on One with Georges Vezina"/>}}

One of the dominant goaltenders in the NHA and early NHL, Vézina led the Canadiens to five [[Stanley Cup Finals]] appearances, where they won the title twice.<ref>{{harvnb|Romain|Duplacey|1994|p=12}}</ref> Seven times in his career, Vézina had the lowest goals-against average in the league he played, and he had the second-best average another five times.<ref name="Jenish 2008 70">{{harvnb|Jenish|2008|p=70}}</ref> From when he joined the Canadiens in 1910, until being forced to retire in 1925, Vézina never missed a game nor allowed a substitute, playing in 328 consecutive regular season games and an additional 39 playoff games.<ref name="Legends of Hockey"/><ref>{{harvnb|Hughes|Fischler|Romain|Duplacey|2003|p=64}}</ref> Though he played the bulk of his career in an era when goaltenders could not leave their feet to make a save (the rule was changed in 1918),<ref>{{harvnb|National Hockey League|2008|p=10}}</ref> Vézina is regarded as one of the greatest goaltenders in hockey history;<ref>{{harvnb|Diamond|2002|p=1968}}</ref> the ''Montreal Standard'' referred to him as the "greatest goaltender of the last two decades" in their obituary.<ref>{{harvnb|Falla|2008|pp=131–32}}</ref>

Well liked in Montreal, Vézina was often seen as the best player on the ice for the Canadiens, and was respected by his teammates, who considered him the spiritual leader of the team.<ref name="Weir 1999 128"/> Referred to as "le Concombre de Chicoutimi" (the "Chicoutimi Cucumber") for his cool demeanour on the ice, he was also known as "l'Habitant silencieux" (the "silent Habitant", Habitant being a nickname for the Canadiens), a reference to his reserved personality.<ref>{{harvnb|Jenish|2008|p=40}}</ref> He often sat in a corner of the team's dressing room alone, smoking a pipe and reading the newspaper.<ref>{{harvnb|Jenish|2008|p=67}}</ref> When news of Vézina's death was announced, newspapers across Quebec paid tribute to the goalie with articles about his life and career. Hundreds of Catholic masses were held in honour of the devout Vézina, and more than 1,500 people filled the Chicoutimi cathedral for his funeral.<ref name="Jenish 2008 70"/>

A lasting legacy of Vézina was the trophy named after him. At the start of the [[1926–27 NHL season|1926–27 season]], [[Leo Dandurand]], Leo Letourneau and [[Joseph Cattarinich]], owners of the Montreal Canadiens, donated the [[Vezina Trophy]] to the NHL in honour of Vézina.<ref name="Legends of Hockey"/> It was to be awarded to the goaltender of the team who allowed the fewest goals during the regular season. The inaugural winner of the trophy was Vézina's successor in goal for the Canadiens, [[George Hainsworth]]. He went on to win the trophy the next two seasons as well. In 1981, the NHL changed the format of awarding the trophy, instead giving it to the goaltender deemed best in the league based on a poll of NHL general managers.<ref>{{harvnb|National Hockey League|2008|p=205}}</ref> The [[Hockey Hall of Fame]] was established in 1945 and among the first nine inductees was Vézina.<ref name="Diamond 2002 550"/> In 1998 Vézina was ranked number 75 on ''[[The Hockey News]]''' list of the [[List of 100 greatest hockey players by The Hockey News|100 Greatest Hockey Players]].<ref name="Dryden 1997 141"/> In honour of the first professional athlete to come from Chicoutimi, the city renamed their hockey arena the [[Centre Georges-Vézina]] in 1965.<ref>{{Citation|url=http://www.rds.ca/pantheon/chroniques/204808.html|title=Georges Vézina, hockey|accessdate=2008-12-02|publisher=RDS.ca|year=2008|author=RDS|language=French}}</ref> When the NHL announced its 100 greatest players in conjunction with the league's centennial 2017, Vézina was included on the list.<ref>{{cite web|url=https://www.nhl.com/news/georges-vezina-100-greatest-nhl-hockey-players/c-284228760?tid=283865022|title=Georges Vezina: 100 Greatest NHL Players|last=Hackel|first=Stu|date=2017-01-01|publisher=NHL.com|accessdate=2017-01-19}}</ref>

==Career statistics==

===Regular season and playoffs===
{| border="0" cellpadding="1" cellspacing="0" style="width:75%; text-align:center;"
|- style="background:#e0e0e0;"
! colspan="3" style="background:#fff;"| &nbsp;
! rowspan="99" style="background:#fff;"| &nbsp;
! colspan="8" | [[Regular season|Regular&nbsp;season]]
! rowspan="99" style="background:#fff;"| &nbsp;
! colspan="8" | [[Playoffs]]
|- style="background:#e0e0e0;"
! [[Season (sports)|Season]]
! Team
! League
! GP !! W !! L !! T !! Min !! GA !! [[Shutout|SO]] !! [[Goals against average|GAA]]
! GP !! W !! L !! T !! Min !! GA !! SO !! GAA 
|-
| 1909–10
| Chicoutimi Saguenéens
| MCHL
| — || — || — || — || — || — || — || — 
| — || — || — || — || — || — || — || —
|- style="background:#f0f0f0;"
| [[1910–11 NHA season|1910–11]]
| [[Montreal Canadiens]]
| [[National Hockey Association|NHA]]
| 16 || 8 || 8 || 0 || 980 || 62 || 0 || 3.80 
| — || — || — || — || — || — || — || —
|-
| [[1911–12 NHA season|1911–12]]
| Montreal Canadiens
| NHA
| 18 || 8 || 10 || 0 || 1109 || 66 || 0 || 3.57 
| — || — || — || — || — || — || — || —
|- style="background:#f0f0f0;"
| [[1912–13 NHL season|1912–13]]
| Montreal Canadiens
| NHA
| 20 || 9 || 11 || 0 || 1217 || 81 || 1 || 3.99 
| — || — || — || — || — || — || — || —
|-
| [[1913–14 NHA season|1913–14]]
| Montreal Canadiens
| NHA
| 20 || 13 || 7 || 0 || 1222 || 64 || 1 || 3.14 
| 2 || 1 || 1 || 0 || 120 || 6 || 1 || 3.00
|- style="background:#f0f0f0;"
| [[1914–15 NHA season|1914–15]]
| Montreal Canadiens
| NHA
| 20 || 6 || 14 || 0 || 1257 || 81 || 0 || 3.86 
| — || — || — || — || — || — || — || —
|-
| [[1915–16 NHA season|1915–16]]
| Montreal Canadiens
| NHA
| 24 || 16 || 7 || 1 || 1482 || 76 || 0 || 3.08 
| 5 || 3 || 2 || 0 || 300 || 13 || 0 || 2.60
|- style="background:#f0f0f0;"
| [[1916–17 NHA season|1916–17]]
| Montreal Canadiens
| NHA
| 20 || 10 || 10 || 0 || 1217 || 80 || 0 || 3.94 
| 6 || 2 || 4 || 0 || 360 || 29 || 0 || 4.83
|-
| [[1917–18 NHL season|1917–18]]
| Montreal Canadiens
| [[NHL]]
| 21 || 12 || 9 || 0 || 1282 || 84 || 1 || 3.93 
| 2 || 1 || 1 || 0 || 120 || 10 || 0 || 5.00
|- style="background:#f0f0f0;"
| [[1918–19 NHL season|1918–19]]
| Montreal Canadiens
| NHL
| 18 || 10 || 8 || 0 || 1117 || 78 || 1 || 4.19 
| 10 || 6 || 3 || 1 || 636 || 37 || 1 || 3.49
|-
| [[1919–20 NHL season|1919–20]]
| Montreal Canadiens
| NHL
| 24 || 13 || 11 || 0 || 1456 || 113 || 0 || 4.66 
| — || — || — || — || — || — || — || —
|- style="background:#f0f0f0;"
| [[1920–21 NHL season|1920–21]]
| Montreal Canadiens
| NHL
| 24 || 13 || 11 || 0 || 1441 || 99 || 1 || 4.12 
| — || — || — || — || — || — || — || —
|-
| [[1921–22 NHL season|1921–22]]
| Montreal Canadiens
| NHL
| 24 || 12 || 11 || 1 || 1469 || 94 || 0 || 3.84 
| — || — || — || — || — || — || — || —
|- style="background:#f0f0f0;"
| [[1922–23 NHL season|1922–23]]
| Montreal Canadiens
| NHL
| 24 || 13 || 9 || 2 || 1488 || 61 || 2 || 2.46 
| 2 || 1 || 1 || 0 || 120 || 3 || 0 || 1.50
|-
| [[1923–24 NHL season|1923–24]]
| Montreal Canadiens
| NHL
| 24 || 13 || 11 || 0 || 1459 || 48 || 3 || 1.97 
| 6 || 6 || 0 || 0 || 360 || 6 || 2 || 1.00
|- style="background:#f0f0f0;"
| [[1924–25 NHL season|1924–25]]
| Montreal Canadiens
| NHL
| 30 || 17 || 11 || 2 || 1860 || 56 || 5 || 1.81 
| 6 || 3 || 3 || 0 || 360 || 18 || 1 || 3.00
|-
| [[1925–26 NHL season|1925–26]]
| Montreal Canadiens
| NHL
| 1 || 0 || 0 || 0 || 20 || 0 || 0 || 0.00 
| — || — || — || — || — || — || — || —
|- style="background:#e0e0e0;"
! colspan="3" | NHA totals
! 138 !! 70 !! 67 !! 1 !! 8484 !! 510 !! 2 !! 3.61
! 13 !! 6 !! 7 !! 0 !! 780 !! 48 !! 1 !! 3.69
|- style="background:#e0e0e0;"
! colspan="3" | NHL totals
! 190 !! 103 !! 81 !! 5 !! 11592 !! 633 !! 13 !! 3.28
! 26 !! 17 !! 8 !! 1 !! 1596 !! 74 !! 4 !! 2.78
|}

* ''NHA statistics are from ''Trail of the Stanley Cup''.''<ref>{{harvnb|Coleman|1966|pp=210–315}}</ref>
* ''NHL statistics are from NHL.com.''<ref name="NHL Bio">{{Citation|url=http://www.nhl.com/ice/player.htm?id=8450137|title=Georges Vézina's NHL Profile|accessdate=2008-12-05|publisher=NHL.com|year=2008|author=NHL.com}}</ref>

==See also==
* [[History of the Montreal Canadiens]]
* [[List of ice hockey players who died during their playing career]]

==Notes==
{{reflist|colwidth=30em}}

==References==
* {{Citation|last=Allen|first=Kevin|last2=Duff|first2=Bob|last3=Bower|first3=Johnny|title=Without Fear: Hockey's 50 Greatest Goaltenders|year=2002|publisher=Triumph Books|location=Chicago|isbn=978-1-57243-484-4}}
*{{Citation|last=Coleman|first=Charles|year=1966|title=Trail of the Stanley Cup, Vol&nbsp;I.|publisher=Kendall/Hunt|location=Toronto|isbn=0-8403-2941-5}}
* {{Citation|editor=Dan, Diamond|title=Total Hockey: The Official Encyclopedia of the National Hockey League, Second Edition|publisher=Total Sports Publishing|location=New York|publication-date=2002|isbn=1-892129-85-X}}
* {{Citation|last=Dryden|first=Steve|title=The Top 100 NHL Players of All Time|year=1997|publisher=McClelland & Stewart Inc.|location=Toronto|isbn=0-7710-4176-4}}
* {{Citation|last=Falla|first=Jack|year=2008|title=Open Ice: Reflections and Confessions of a Hockey Lifer|publisher=John Wiley & Sons Canada, Ltd.|location=Mississauga, Ontario|isbn=978-0-470-15305-5}}
* {{Citation|last=Hughes|first=Morgan|last2=Fischler|first2=Stan and Shirley|last3=Romain|first3=Joseph|last4=Duplacey|first4=James|title=Hockey Chronicle: Year-by-Year History of the National Hockey League|year=2003|publisher=Publications International, Ltd.|location=Lincolnwood, Illinois|isbn=0-7853-9624-1}}
* {{Citation|last=Jenish|first=D'Arcy|year=2008|title=The Montreal Canadiens: 100 Years of Glory|publisher=Doubleday Canada|isbn=978-0-385-66324-3}}
* {{Citation|last=Leonetti|first=Mark|last2=Beliveau|first2=Jean|year=2004|title=Canadiens Legends: Montreal's Hockey Heroes|publisher=Raincoast Books|isbn=978-1-55192-731-2}}
* {{Citation|last=National Hockey League|title=National Hockey League Official Guide & Record Book 2009|year=2008|publisher=Dan Diamond & Associates, Inc.|location=Toronto|isbn=978-1-894801-14-0}}
* {{Citation|last=Podnieks|first=Andrew|title=Lord Stanley's Cup|year=2004|publisher=Fenn Publishing|location=Bolton, Ontario|isbn=1-55168-261-3}}
* {{Citation|last=Romain|first=Joseph|last2=Duplacey|first2=James|title=Hockey Superstars|year=1994|publisher=Smithbooks|location=Toronto|isbn=0-88665-899-3}}
* {{Citation|last=Weekes|first=Don|last2=Banks|first2=Kerry|year=2004|title=The Unofficial Guide to Even More of Hockey's Most Unusual Records|publisher=Greystone Books|isbn=978-1-55365-062-1}}
* {{Citation|last=Weir|first=Glenn|last2=Chapman|first2=Jeff|last3=Weir|first3=Travis|title=Ultimate Hockey|year=1999|publisher=Stoddart Publishing|location=Toronto|isbn=0-7737-6057-1}}

==External links==
{{Commons|Georges Vézina}}
*{{hockeydb|5573}}
*{{Legendsmember|Player|P194512|Georges Vézina}}
*{{Nhlprofile|8450137|Georges Vézina}}

{{featured article}}

{{DEFAULTSORT:Vezina, Georges}}
[[Category:1887 births]]
[[Category:1926 deaths]]
[[Category:Canadian ice hockey goaltenders]]
[[Category:Canadian Roman Catholics]]
[[Category:20th-century deaths from tuberculosis]]
[[Category:Hockey Hall of Fame inductees]]
[[Category:Sportspeople from Saguenay, Quebec]]
[[Category:Infectious disease deaths in Quebec]]
[[Category:Montreal Canadiens (NHA) players]]
[[Category:Montreal Canadiens players]]
[[Category:Stanley Cup champions]]
[[Category:Ice hockey people from Quebec]]