{{good article}}
{{Infobox oil field
| name        = Adar oilfield
| image       = Sudan Map Oelgas.png
| caption     = Oil concessions in Sudan & South Sudan as of 2004. Block 3 (Adar) in the east.
| location_map       = South Sudan
| location_map_width =
| location_map_text  =
| coordinates = {{coord|10.008014|N|32.958759|E|type:landmark|display=inline,title}}
| coordinates_ref    =
| country     = [[South Sudan]]
| region      = [[White Nile]] state
| location    =
| block       = 3
| offonshore  = Onshore
| operator    = [[PetroDar]]
| partners    =
| contractors =
| discovery   = 1981
| start_development = 1996
| start_production = 1997
| peak_year   =
| abandonment =
| peak_of_production_oil_bbl/d =
| peak_of_production_oil_tpy =
| peak_of_production_gas_mmcuft/d =
| peak_of_production_gas_mmscm/d =
| peak_of_production_gas_bcm/y =
| oil_production_bbl/d =
| oil_production_tpy =
| production_year_oil =
| production_gas_mmcuft/d =
| production_gas_mmscm/d =
| production_gas_bcm/y =
| production_year_gas =
| est_oil_bbl =
| est_oil_t   =
| recover_oil_bbl =
| recover_oil_t   =
| est_gas_bft =
| est_gas_bcm =
| recover_gas_bft =
| recover_gas_bcm =
| formations  = Yabus Formation
}}
The '''Adar oilfield''', also known as the '''Adar Yale''', '''Adar Yeil''' or '''Adaril''' field, is an oilfield situated in the [[Melut Basin]] in [[South Sudan]] estimated to contain about {{convert|276|Moilbbl|m3}} of oil.<ref name="ECOS">{{cite web
 |url=http://www.ecosonline.org/news/2006/white_nile_completes_south_sudan_seismic_operation.doc/
 |title=White Nile completes South Sudan seismic operation
 |date=Nov 7, 2006
 |publisher=ECOS
 |accessdate=9 August 2011}}</ref>
The [[Chevron Corporation]] discovered the Adar Yale field in 1981, shortly before the start of the [[Second Sudanese Civil War]] (1983–2005). Soon after Chevron had suspended operations in 1984, Sudanese government troops began attacking civilian settlements in the area, burning the houses and driving the people away, and in the late 1990s, [[Nuer people|Nuer]] militias from [[Nasir, South Sudan|Nasir]] helped the army in clearing away the people to make way for the roads and infrastructure of the oilfield.

President [[Omar al-Bashir]] inaugurated the site in March 1997, and it initially produced  just {{convert|5000|oilbbl|m3}} a day. Production from this oilfield, which lies close to the borders with Sudan and Ethiopia, has the potential to bring significant economic benefits to the region.<ref name=YongoBure2007/> However, until recently the focus has been on clearing the population away from the oilfield rather than on a longer term strategy for developing the region.<ref name=YongoBure2007/> China has provided a large investment in the Adar oilfield and others in South Sudan and Sudan and has made plans to make extensive further investments.<ref name=Hardenberg20110111/>

==Background and location==
Adar is an area southeast of [[Melut]], which lies on the [[White Nile]] in [[Upper Nile (state)|Upper Nile state]], and to the west of Ethiopia.
The [[Khor Adar]], which drains the [[Machar Marshes]], flows through the region in a northwest direction to the Nile, which it reaches just above Melut.<ref name=Allan1994/>
The Khor Machar swamps lie in a triangle north of the [[Sobat River]] and east of the [[White Nile]].
When flooded during the wet season, they extend for {{convert|6500|km2|sqmi}}.
The swamps and marshes are fed by local rainfall and by many small torrents from the Ethiopian foothills, which extend for {{convert|200|km|mi}} along the eastern border, and by spill water from channels of the Sobat.<ref name="Dumont2009">{{cite book|last=Dumont|first=Henri J.|title=The Nile: Origin, Environments, Limnology and Human Use|url=https://books.google.com/books?id=iF_U1NoknHoC&pg=PA480|accessdate=18 August 2011|date=1 June 2009|publisher=Springer|isbn=978-1-4020-9725-6|page=480}}</ref>

Discharge from the marshes along the Adar river is low except in years with exceptionally heavy rainfall.<ref name=Allan1994/>
Most of the water is lost through evaporation before reaching the Nile.
It has been proposed to build a canal from Machar via Adar to the White Nile to increase the volume of water flowing to Northern Sudan and Egypt, which apparently could be done without major environmental impact, but political instability has prevented the project from starting.<ref>{{cite book
 |url=https://books.google.ca/books?id=eVdyaJcIydwC&pg=PA255
 |page=255
 |title=National and international water law and administration: selected writings
 |author=Dante Augusto Caponera
 |publisher=Kluwer Law International |year=2003
 |ISBN=90-411-2085-8}}</ref>

The [[Chevron Corporation]] discovered the Adar Yale field in 1981, shortly before the start of the [[Second Sudanese Civil War]] (1983–2005).
Four exploratory wells had flow rates that exceeded {{convert|1500|oilbbl|m3}} a day.<ref name=ECOS200605/>
The oil is held in the Yabus Formation sandstone from the [[Paleogene]] age.
The field has an area of about {{convert|20|km2|sqmi}} but the average pay zone is only {{convert|2.9|km2|sqmi}}.
Initially the field was estimated to hold {{convert|168|Moilbbl|m3}}, but Seismic data acquired in 2000 pushed that up to {{convert|276|Moilbbl|m3}}.<ref name="ECOS"/> 
Also, three small oil pools were discovered south of Adar-Yale with another {{convert|129|Moilbbl|m3}}.<ref>{{cite web
 |url=http://www.geoexpro.com/sfiles/7/41/6/file/GEOExPro03Sudan.pdf
 |title=Changing Exploration Focus Paved Way for Success
 |publisher=GEO ExPro
 |date=May 2006
 |author=Tong Xiaoguang and Shi Buqing
 |accessdate=9 August 2011}}</ref>
Chevron started to pull out of Sudan in 1984 when three of its employees were killed, finally selling all of its Sudanese interests in 1992.<ref name=Hardenberg20110111/>

==Development and production==
Chevron suspended operations in 1984, and their concession was later divided into smaller units.<ref name=ECOS200605/>
In 1992 Gulf Petroleum Corporation-Sudan (GPC) was awarded the [[Melut Basin]] - Blocks 3 and 7.
GPC was owned 60% by [[Qatar]]’s [[Gulf Petroleum Corporation]], 20% by [[Sudapet]] and 20% by a company owned by the [[National Islamic Front]] (NIF) financier Mohamed Abdullah Jar al-Nabi.<ref name=ECOS200605/>
The GPC consortium was reported to have invested US$12m in developing the Adar Yale field.<ref name=ECOS200605/>
In October 1996 GPC began to drill and reopen Chevron's wells and to build an all-weather road connecting Adar Yale to the garrison town of Melut.<ref name=ECOS200605/>

President [[Omar al-Bashir]] inaugurated the Adar Yale site in March 1997.
At first, production was just {{convert|5000|oilbbl|m3}} a day, taken by truck to Melut and then by barge down the Nile to [[Khartoum]] for export. Although export volumes were tiny compared to the field's potential, it was the first Sudanese crude to be exported and therefore had symbolic significance.<ref name=ECOS200605/>
In March 2000 Fosters Resources, a Canadian company, signed an agreement with the government of Sudan to develop the concession that covered most of the Melut Basin, including the Adar Yeil field, in partnership with a consortium of Arab and Sudanese companies. Fosters was forced to withdraw in May 2000 when its financial backing collapsed due to pressure from human rights groups.<ref>{{cite book
 |url=https://books.google.ca/books?id=ACPbZNEODA0C&pg=PA175
 |page=175
 |title=Sudan Investment and Business Guide
 |publisher=Int'l Business Publications |year=2002
 |ISBN=0-7397-4177-2}}</ref>
The U.S. government had been highly critical of Canadian involvement in Sudan oil development and in February 2000 the U.S. Treasury announced that American persons were forbidden to do business with the [[Greater Nile Petroleum Operating Company]] (GNPOC) and Sudapet.<ref>{{cite book
 |url=https://books.google.ca/books?id=3WQkACoP3FkC&pg=PA548
 |pages=548–549
 |title=Sudan, oil, and human rights
 |author=[[Jemera Rone]]
 |publisher=Human Rights Watch |year=2003
 |ISBN=1-56432-291-2}}</ref>

[[Petrodar]] was incorporated in October 2001 owned 41% by [[China National Petroleum Company]] (CNPC) and 40% by [[Petronas]] of [[Malaysia]]. Petrodar implemented major upgrades to the oil development infrastructure, including {{convert|31000|oilbbl|m3|adj=on}}/day production facilities at the Adar Yale field, now largely cleared of its original inhabitants.<ref name=ECOS200605/>
In November 2005 CNCP brought the Petrodar pipeline into operation, linking Blocks 3 and 7 (Adar Yale and Palogue fields) to [[Port Sudan]] on the Red Sea. The pipeline has throughput of {{convert|150000|oilbbl|m3}} per day, and maximum capacity of {{convert|500000|oilbbl/d|m3/d}}. By January 2007 combined output from Blocks 3 and 7 was 165,000 with potential to reach a peak of {{convert|200000|oilbbl/d|m3/d}} by late 2007.
By 2009, the two blocks were producing close to {{convert|240000|oilbbl/d|m3/d}} of Dar blend.<ref name=IBP2008/>
Because this blend of crude oil is heavy and highly acidic, it fetches lower prices than benchmark crudes such as [[Brent Crude|Brent]] or [[Minas Crude|Minas]].<ref name=IBP2008/>

==People==

Adar is predominantly inhabited by the [[Dinka people]], who mainly cultivate crops and raise cattle.<ref name=ECOS200605/> Many of the Dinka use riverine pastures along the east bank of the White Nile, but in dry seasons can usually find alternative pasturage along the Khor Adal and Khor Wol and on the edges of the marshes.
The country is well suited to cultivation of crops watered by rain.<ref>{{cite book
 |url=https://books.google.ca/books?id=Tfc8AAAAIAAJ&pg=PA207
 |page=207
 |title=The Jonglei Canal: impact and opportunity
 |author=Paul Philip Howell, Michael Lock
 |publisher=Cambridge University Press |year=1988
 |ISBN=0-521-30286-2}}</ref>

Soon after Chevron had closed operations, government troops began attacking civilian settlements in the area, burning the houses and driving the people away. This resulted in many deaths.<ref name=ECOS200605/>
In the late 1990s, [[Nuer people|Nuer]] militias from [[Nasir, South Sudan|Nasir]] helped the army in clearing away the people to make way for the roads and infrastructure of the oilfield.<ref name=ECOS200605/>
Aid workers were also targeted in attacks, with NGO compounds, farm supply distribution centers and primary health care centers vandalized and destroyed.<ref name=CAid2001/>

One estimate is that 12,000 people were forced to move in 1999-2000 while the all-weather road was being built between Melut, Paloic and Adar.<ref name=ECOS200605/>
Another source said church leaders reported that government militias burned 48 villages and displaced 55,000 people in the Adar area in 2000.<ref name=CAid2001/>
According to [[International Relief and Development]] agency (IRD) director Derek Hammond, the areas around Adar contained "Fields of destroyed crops with no evidence of any type of food, a handful of local people scratching around in a swamp for something to eat".<ref name=CAid2001/>

==Security==

In July 1996 the Government of Sudan attacked [[Sudan People's Liberation Army]] (SPLA) positions at [[Delal Ajak]], west of the Nile.<ref name="Akol2003">{{cite book|last=Akol|first=Lam|title=SPLM/SPLA: the Nasir Declaration|url=https://books.google.com/books?id=tzsUOpcLxbUC&pg=PA266|accessdate=19 August 2011|date=August 2003|publisher=iUniverse|isbn=978-0-595-28459-7|page=266}}</ref>
Their goal was to secure passage for barge shipments of oil from the Adar-1 field.
In November 1996, SPLA leader [[John Garang]] gave warning that his forces would attack the Adar Yale oil field.<ref name=SudanUpdateTimeline/>
In June 1998 the SPLA captured the town of [[Ulu, South Sudan|Ulu]], close to the Adar Yale field, and in March 1999 the SPLA 13th battalion defeated a government brigade at the town. With this victory, the Adar Yale oilfield was within range of the SPLA's artillery.<ref name=SudanUpdateTimeline/>

In April 2001 a Russian-made [[Antonov]] airplane broke in two after it skidded off the runway at Adaril, apparently due to a sandstorm.
The crash killed Sudan's deputy defense minister, Colonel [[Ibrahim Shamsul-Din]], and 13 other high-ranking officers who had been touring the southern military area. 16 people survived the crash.<ref>{{cite web
 |url=http://www.chron.com/CDA/archives/archive.mpl/2001_3294254/sudan-air-crash-kills-14-military-leaders.html
 |work=Houston Chronicle
 |title=Sudan air crash kills 14 military leaders
 |author=Associated Press
 |date=4 May 2001
 |accessdate=10 August 2011}}</ref> A spokesman for the SPLA denied responsibility for the accident, saying they did not have forces in the area.<ref>{{cite web
 |url=http://news.bbc.co.uk/2/hi/africa/1261840.stm
 |date=5 April 2001
 |title='No foul play' in Sudan crash
 |work=BBC News
 |accessdate=10 August 2011}}</ref>

The Sudanese Civil War officially ended in January 2005, and the ''[[Juba Declaration of 8 January 2006]]'' laid out the basis for unifying rival military forces in [[South Sudan]]. [[Gordon Kong Chuol]], Deputy Commander of the [[South Sudan Defense Forces]] (SSDF), which had been supported by the Government of Sudan, resisted the merger. His core faction, the "Nasir Peace Force" was based in the village of Ketbek, just north of Nasir, with 75-80 fighters as of August 2006 and perhaps 300 reserve forces in the area.
His position on the border with Sudan to the north and near to the functioning Adar Yale oilfield was sensitive.<ref>{{cite web
 |url=http://www.smallarmssurveysudan.org/pdfs/HSBA-SWP-1-SSDF.pdf
 |title=The South Sudan Defence Forces in the Wake of the Juba Declaration
 |last=Young |first=John
 |publisher=Small Arms Survey
 |date=November 2006 
 |accessdate=4 August 2011| archiveurl= https://web.archive.org/web/20110724200154/http://www.smallarmssurveysudan.org/pdfs/HSBA-SWP-1-SSDF.pdf| archivedate= 24 July 2011 <!--DASHBot-->| deadurl= no}}</ref>
In July 2006, four busloads of SSDF recruits arrived in the area from Khartoum.
In August 2006 there were reported to be 300-400 active SSDF militiamen in the Adar area.<ref>{{cite book
 |url=https://books.google.ca/books?id=Lmy9e_LGwdoC&pg=PA324
 |page=324
 |title=Small Arms Survey 2007: Guns and the City
 |author=Small Arms Survey, Geneva
 |publisher=Cambridge University Press |year=2007
 |ISBN=0-521-88039-4}}</ref>

China has provided a large investment in the Adar oilfield and others in South Sudan, as well as in oilfields in Sudan, and in the pipeline to Port Sudan. China established a consulate in [[Juba]] in September 2008 and upgraded it to an embassy in November 2010. China has made plans to make significant investments in South Sudan.<ref name=Hardenberg20110111/>
A pipeline to the Kenyan port of [[Lamu]] is being discussed which could provide an alternative route if Sudan chooses to close the northern pipeline. It is in China's interest to resolve security problems, and as a major investor and customer of both countries China may have the leverage to achieve this goal.<ref name=Hardenberg20110111/>

==References==
{{reflist|2|refs=
<ref name=Allan1994>{{cite book
 |url=https://books.google.ca/books?id=M_irG57YsWIC&pg=PA274
 |page=274
 |title=The Nile: sharing a scarce resource : a historical and technical review of water management and of economic and legal issues
 |author=Paul Philip Howell, John Anthony Allan
 |publisher=Cambridge University Press |year=1994
 |ISBN=0-521-45040-3}}</ref>
<ref name=CAid2001>{{cite web
 |url=http://postconflict.unep.ch/sudanreport/sudan_website/doccatcher/data/documents/The%20scorched%20earth.pdf
 |title=The scorched earth: oil and war in Sudan
 |date=March 2001
 |publisher=Christian Aid
 |accessdate=9 August 2011}}</ref>
<ref name=ECOS200605>{{cite web
 |url=http://www.ecosonline.org/reports/2006/ECOS%20Melut%20Report%20final%20-text%20only.pdf
 |title=OIL DEVELOPMENT in northern Upper Nile, Sudan
 |publisher=European Coalition on Oil in Sudan
 |date=May 2006
 |accessdate=9 August 2011}}</ref>
<ref name=Hardenberg20110111>{{cite web
 |url=http://english.aljazeera.net/indepth/features/2011/01/20111910357773378.html
 |work=Al Jazeera
 |title=China: A force for peace in Sudan?
 |author=Donata Hardenberg
 |date=11 Jan 2011
 |accessdate=10 August 2011}}</ref>
<ref name=IBP2008>{{cite book
 |url=https://books.google.ca/books?id=l-rSiphgoQgC&pg=PA44
 |pages=44, 38
 |title=Sudan Oil and Gas Exploration Laws and Regulation Handbook
 |author=IBP USA
 |publisher=Int'l Business Publications |year=2008
 |ISBN=1-4330-7897-X}}</ref>
<ref name=SudanUpdateTimeline>{{cite web
 |url=http://www.sudanupdate.org/REPORTS/Oil/21oc.html
 |title=Sudan Oil and Conflict Timeline
 |work=SudanUpdate
 |accessdate=10 August 2011}}</ref>
<ref name=YongoBure2007>{{cite book
 |url=https://books.google.ca/books?id=Ac6g7jXgqdUC&pg=PA83
 |pages=83, 104
 |title=Economic development of southern Sudan
 |author=Benaiah Yongo-Bure
 |publisher=University Press of America |year=2007
 |ISBN=0-7618-3588-1}}</ref>
}}
{{Use dmy dates|date=August 2011}}

[[Category:Oil fields of Sudan]]
[[Category:Oil fields in South Sudan]]
[[Category:White Nile (state)]]
[[Category:History of South Sudan]]