{{good article}}
{{Infobox television episode 
| title = 6955 kHz
| series = [[Fringe (TV series)|Fringe]]
| image =<!-- Deleted image removed:  [[File:Fringe 6955 kHz.jpg|250px]] -->
| caption = Astrid decodes the mysterious numbers that were taken from the opening scene's radio broadcast
| season = 3
| episode = 6
| airdate = November 11, 2010
| production = 3X6106
| writer = [[Robert Chiappetta]]<br />[[Glen Whitman]]
| director = [[Joe Chappelle]]	
| guests = * [[Kevin Weisman]] as Joseph Feller
* [[Clark Middleton]] as Edward Markham
* [[Ryan McDonald (actor)|Ryan McDonald]] as [[List of Fringe characters|Brandon Fayette]]
* [[Mark Acheson]] as Murray Harkins
* Minh Ly as Dan Liang
* Paula Lindberg as Becky Woomer
* Tyler McClendon as Laird Woomer
* Vincent Tong as Shen Chan
| episode_list = [[Fringe (season 3)|''Fringe'' (season 3)]]<br>[[List of Fringe episodes|List of ''Fringe'' episodes]]
| prev = [[Amber 31422]]
| next = [[The Abducted]]
}}

"'''6955 kHz'''" is the sixth [[List of Fringe episodes|episode]] of the [[Fringe (season 3)|third season]] of the American [[science fiction]] [[drama]] [[television program|television series]] ''[[Fringe (TV series)|Fringe]]''. It first aired on November 11, 2010 in the United States. The third season spent much of its time alternating between the prime and parallel universes, and "6955 kHz" was set in the former. The storyline followed the Fringe team's investigation into a [[numbers station]] that mysteriously gave its listeners [[amnesia]], a case that ultimately ties to a [[doomsday device]].

The episode was written by story editors [[Robert Chiappetta]] and [[Glen Whitman]], and was directed by [[Joe Chappelle]]. Its broadcast took place on the [[Fox Broadcasting Company|Fox network]], and according to [[Nielsen Media Research]], an estimated 4.8 million viewers tuned in. Reviews of the episode ranged from positive to mixed, as some reviewers disagreed about the introduction of a "[[Mythology of Fringe#First People|First People]]" mythology.

==Plot==
Fauxlivia, [[Olivia Dunham]]'s [[doppelgänger]] from the [[parallel universe (fiction)|parallel universe]], continues to pose as Olivia as part of the Fringe division. The Fringe team is brought in on a case where several people, part of an online group attempting to decode the information sent by a [[numbers station]], have been stricken with amnesia. They discover the station broadcasting the signal, finding its workers killed and a strange box connected to the broadcast equipment. They identify fingerprints on the box of a Joseph Feller, but his current location is unknown. [[Walter Bishop (Fringe)|Walter]] attempts to decipher the workings of the box, while giving hope to some of the affected people that they will get their memories back in time.

[[Peter Bishop|Peter]] discovers that the rare book shopkeeper, Edward Markham ([[Clark Middleton]]), was part of the online group but did not listen that night. Edward provides his theory of the numbers stations to Peter and Fauxlivia: that it is a signal left by the "[[Mythology of Fringe#First People|First People]]", an advanced civilization that existed before a [[mass extinction event]]. He provides them with a book about the First People. As they return the book to Walter, Peter notices numbers in the astrological charts in the book are the same as the broadcast numbers. They give the book to [[Astrid Farnsworth|Astrid]], a skilled decoder, along with copious volumes of data from biotechnology corporation [[Mythology of Fringe#Massive Dynamic|Massive Dynamic]] about the numbers stations.

Later, the crash of a small commuter aircraft is attributed to a similar signal from a numbers station, and when Fringe division identifies the source, they find a second box. Taking the box to Walter, Peter identifies one of the electronic components as rare, and engages his contacts to find Feller's address from its purchase. Fauxlivia feigns returning to headquarters to instead travel to Feller's apartment, warning him that Fringe is onto him, but he insists on continuing his job. She throws him out the window as the Fringe team arrives, killing him and revealing him to be a shapeshifter. Fauxlivia claims she killed the man in self-defense.

Meanwhile, Astrid has decoded the numbers as a series of geographical coordinates. The closest one is in Milton, Massachusetts, the site of where a mysterious box was found ("[[The Box (Fringe)|The Box]]"). Teams are quickly sent to the other sites given across the globe, and they discover many more parts of what Walter and Peter believe to be the same [[doomsday device|doomsday machine]] that Walternate, Walter's doppelgänger, has already constructed in the parallel universe, and which the First People book claims can destroy or create universes. Fauxlivia later communicates this finding to the parallel universe through the typewriter shop, and is ordered to initiate "phase two".

The episode ends in the parallel universe where Olivia, having broken Walternate's conditioning making her believe herself to be Fauxlivia, is told that no further tests are needed. A vision of Peter warns Olivia that her usefulness to Walternate has ended and her life is in danger.

==Production==
{{Quote box
 | quote  = "[Fauxlivia is] threatened by anything that's going to lead back to her."
 | source = –Actress Anna Torv on one of her characters<ref name=digitalspy/>
 | width  = 23em
 |bgcolor=#ADD8E6
 | align  =right
}}

"6955 kHz" was written by story editors [[Robert Chiappetta]] and [[Glen Whitman]], while frequent ''Fringe'' collaborator [[Joe Chappelle]] served as the director.<ref name=fearnet/> Leading up to the episode's broadcast, actress [[Anna Torv]] revealed in an official Fox interview that "[The next] episode is all about these co-ordinates that keep getting broadcast on the radio. This is all... a ploy to get Walter and Peter working on Walternate's machine."<ref name=digitalspy/> Chappelle also added that Fauxlivia would be working "to protect Walternate's plan... These pirate broadcasts are created on the other side... by Walternate, to help him in his plans with our universe."<ref name=digitalspy>{{Cite web |url=http://www.digitalspy.com/tv/s118/fringe/news/a287141/anna-torv-teases-next-fringe-episode.html |title=Anna Torv teases next 'Fringe' episode |publisher=[[Digital Spy]] |first=Morgan |last=Jeffery |date=2010-11-10 |accessdate=2011-10-09}}</ref>

The episode featured performances by guest actors [[Kevin Weisman]], Mark Acheson, Minh Ly, Paula Lindberg, Tyler McClendon, and Vincent Tong; previous guest stars [[Ryan McDonald (actor)|Ryan McDonald]] and [[Clark Middleton]] returned as scientist Brandon Fayette and rare bookseller Edward Markham, respectively.<ref>{{Cite web |url=http://www.tvguide.com/tvshows/fringe-2010/episode-6-season-3/6955-khz/293813 |title=Fringe Episode: "6955 kHz" |work=[[TV Guide]] |accessdate=2011-10-09}}</ref><ref>{{Cite web |url=http://www.thefutoncritic.com/listings/20101108fox11/ |title=Listings - FRINGE on Fox |publisher=[[Fox Broadcasting Network]] |first=|last=|date=  |accessdate=2011-10-09}}</ref>

As with other ''Fringe'' episodes,<ref>{{Cite web|url=http://soinc.org/fringe |title=TV Show "Fringe" on Fox Partners with Science Olympiad |publisher=[[Science Olympiad]] |accessdate=2011-07-19}}</ref><ref>{{Cite web|url=http://www.tvguide.com/News/Fringe-Unveils-Science-1025461.aspx |title=Fringe Unveils Science Sites |first=Damian |last=Holbrook |work=[[TV Guide]] |date=2010-11-11|accessdate=2011-07-07}}</ref> Fox released a science lesson plan in collaboration with [[Science Olympiad]] for grade school children, focusing on the science seen in "6955 kHz", with the intention of having "students learn about information encoding, which is [the] process of converting a piece of information into another form or representation."<ref>{{Cite web |url=http://www.fox.com/fringe/_ugc/images/science/3.06_The_Science_of_Fringe_6955_kHz_1.pdf |title=The Science of Fringe: Exploring Information Encoding |publisher=[[Fox Broadcasting Company]] |accessdate=2011-07-19}}</ref>

Torv later remarked in a DVD special featurette that "this episode is actually a nice one because I think she's kind of starting to look at these people as more than just a project. I think she’s starting to think about who they are"<ref>{{cite video |people=[[Jeff Pinkner|Pinkner, Jeff]]; [[J. H. Wyman]]; [[Anna Torv]], [[John Noble]], [[Joshua Jackson]], [[Monica Owusu-Breen]] |date=2011 |title=The Psychology of Duality | medium=DVD |publisher=[[Warner Bros.]] Television |location=''Fringe: The Complete Third Season'' Disc 6 }}</ref>

==Reception==

===Ratings===
On its first American broadcast on November 11, 2010, "6955 kHz" was watched by an estimated 4.8 million viewers, earning a 2.9/5 rating for all households and 1.7/5 for adults 18 to 49. ''Fringe'' and its lead-in show ''[[Bones (TV series)|Bones]]'' helped Fox place third for the night in a tie with [[NBC]].<ref>{{cite web | url = http://tvbythenumbers.zap2it.com/2010/11/12/thursday-final-ratings-fringe-community-30-rock-outsourced-the-office-adjusted-down-bones-my-dad-says-adjusted-up/71871 | title = Thursday Final Ratings: Fringe, Community, 30 Rock, Outsourced, The Office Adjusted Down; Bones, $#*! My Dad Says Adjusted Up | first = Robert | last = Seidman | date = 2010-11-12 | accessdate = 2011-02-21 | publisher = [[TV by the Numbers]] }}</ref><ref>{{cite web | url = http://insidetv.ew.com/2010/11/12/greys-anatomy-remains-healthy-nbc-gets-great-boost-from-local-football-game/ | title = 'Grey's Anatomy' remains healthy, NBC gets great boost from local football game | first = Lynette| last = Rice | date = 2010-11-12 | accessdate = 2011-10-09 | work= [[Entertainment Weekly]] }}</ref> [[Time shifting|Time shifted viewing]] increased the episode's ratings by 53 percent among adults, resulting in a rise from 1.7 to 2.6. This number tied with the [[NBC]] series ''[[The Event (TV series)|The Event]]'' as the largest increase of the week in time-shifting viewers.<ref>{{Cite web|url=http://tvbythenumbers.zap2it.com/2010/11/30/live7-dvr-ratings-greys-anatomy-the-event-fringe-hawaii-five-0-top-weeks-rankings/73714/ |title=Live+7 DVR Ratings: Grey's Anatomy, The Event, Fringe, Hawaii Five-0 Top Week's Rankings |last=Gorman |first=Bill |date=2010-11-30 |publisher=[[TV By the Numbers]] |accessdate=2012-06-13}}</ref>

===Reviews===
The episode received positive to mixed reviews. Writing for ''[[Entertainment Weekly]]'', critic Ken Tucker noted and appreciated that "many key details were provided by characters who don’t often serve that function", such as with Broyles and the numbers stations.<ref name=ewreview/> He called it "a terrific, almost Astrid-centric episode."<ref name=ewreview>{{cite web | url = http://watching-tv.ew.com/2010/11/12/fringe-season-3-episode-6/ | title = 'Fringe' recap: 'I knew my Jimi Hendrix wah-wah pedal would come in handy' | first = Ken | last = Tucker | date = 2010-11-12 | accessdate = 2011-05-17 | work= [[Entertainment Weekly]] }}</ref> ''[[Los Angeles Times]]'' writer Andrew Hanson enjoyed the episode's many puzzles and expressed appreciation for the "First People" revelation as well as Walternate's "long con" concerning the doomsday device.<ref name=latimesreview/> Hanson was skeptical that Peter had not yet figured out Fauxlivia's true identity, but praised Anna Torv's performance ("Anyone who doubted [her] acting chops in the first couple of seasons should be eating their words").<ref name=latimesreview>{{cite web | url = http://latimesblogs.latimes.com/showtracker/2010/11/fringe-recap-puzzle-zoo.html | title = 'Fringe' recap: puzzle zoo | first = Andrew | last = Hanson | date = 2010-11-12 | accessdate = 2011-05-17 | work= [[Los Angeles Times]] }}</ref>

{{Quote box
 | quote  = "Boy, one thing you cannot accuse ''Fringe'' of is doling out information in teasing little bits. Last night’s episode titled '6955 kHz' presented a large amount of information that I’d call an info-dump if that phrase didn’t mar the elegance, the inventive wit, of what we witnessed."
 | source = –''[[Entertainment Weekly]]'' columnist Ken Tucker<ref name=ewreview/>
 | width  = 32em
 |bgcolor=#ADD8E6
 | align  =left
}}

Like Andrew Hanson, [[The A.V. Club]]'s Noel Murray called the introduction of the First People "a masterstroke on the part of the ''Fringe'' writers".<ref name=avclubreview/> Murray graded the episode with a B+, explaining it "was a strange ''Fringe'' for me. The dialogue was often painfully expository, with liberal doses of ADR to make sure that we viewers didn’t miss any of the massive amounts of significant information we need to understand as the story moves forward...And yet the episode was also funky and philosophical in the way I like my ''Fringe'' to be, using the plot and even the setting to put across more than just pieces of the series’ mythology."<ref name=avclubreview>{{cite web | url = http://www.avclub.com/articles/6995-khz,47379/ | title = 6995 kHz | first = Noel | last = Murray| date = 2010-11-11 | accessdate = 2011-05-17 | publisher= [[The A.V. Club]] | archiveurl= https://web.archive.org/web/20110629155813/http://www.avclub.com/articles/6995-khz,47379/| archivedate= 29 June 2011 <!--DASHBot-->| deadurl= no}}</ref>

[[Fearnet]] critic Alyse Wax wrote "This episode made my brain hurt.  And I mean that in the nicest way possible. They were throwing a lot of weird shit at us. It was great because I feel like we are finally getting answers, like we will finally get some closure, and just maybe get rid of the Red universe stiffs. This episode was so ridiculously dense I am sure I missed some of the deeper scientific facts. I got the gist of it, though, and frankly, I much prefer these jam-packed episodes. I like when the puzzle pieces start falling into place. It's calming."<ref name=fearnet>{{cite web | url = http://www.fearnet.com/news/reviews/b20932_6955_khz_fringe_episode_36.html | title = '6955 kHz' - 'Fringe' Episode 3.6 | first = Alyse | last = Wax | date = 2010-11-12 | accessdate = 2011-05-17 | publisher= [[Fearnet]] }}</ref> [[SFScope]]'s Sarah Stegall believed the episode "start[ed] out interestingly enough", but unlike other reviewers, she was very critical of the First People mythology, stating "This is where my disbelief not only stopped being suspended, it got up and walked out of the room... I still don't like the premise of the evolution of an entire hominid species that left not one single solitary fossil behind. It's more like fantasy than science fiction."<ref name=sfscope/> Stegall praised Torv's acting as she plays "two people very subtly, very convincingly", and concluded her review by commenting she will continue to watch the series because she trusts the writers will "supply us with plausible answers from time to time", but is "losing some respect for it".<ref name=sfscope>{{cite web | url = http://sfscope.com/2010/11/by-the-numbersfringes-6955-khz.html  | title = By the Numbers—Fringe's "6955 kHz" | first = Sarah | last = Stegall | date = 2010-11-15 | accessdate = 2011-05-17 | publisher= [[SFScope]] }}</ref>

==References==
{{reflist|30em}}

==External links==
{{Wikiquotehas|a collection of quotations related to|<center>[[q:Fringe (TV series)#6955 kHZ &#x5B;3.06&#x5D;|6955 kHz]]</center>}}
* [http://www.fox.com/fringe/recaps/season-3/episode-6/ "6955 kHz"] at [[Fox Broadcasting Company|Fox]]
* {{IMDb episode|1635959}}
* {{tv.com episode|fringe/6955-khz-1360913}}

{{Fringe show}}

[[Category:Fringe (season 3) episodes]]
[[Category:2010 television episodes]]