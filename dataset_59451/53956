{{Use dmy dates|date=August 2015}}
{{Use Australian English|date=August 2015}}
{{Infobox Grand Prix race report
|Type           = F1
|Country        = Australia
|Grand Prix     = Australian
|Official name  = LI Foster's Australian Grand Prix
|Image          = Adelaide (long route).svg
|Date           = 26 October
|Year           = 1986
|Race_No        = 16
|Season_No      = 16
|Location       = [[Adelaide Street Circuit]]<br>[[Adelaide]], [[South Australia]]
|Course         = Temporary Street circuit
|Course_mi      = 2.362
|Course_km      = 3.780
|Distance_laps  = 82
|Distance_mi    = 193.864
|Distance_km    = 309.960
|Weather        = Sunny
|Pole_Driver    = [[Nigel Mansell]]
|Pole_Team      = [[WilliamsF1|Williams]]-[[Honda Racing F1|Honda]]
|Pole_Time      = 1:18.403
|Pole_Country   = GBR
|Fast_Driver    = [[Nelson Piquet]]
|Fast_Team      = [[WilliamsF1|Williams]]-[[Honda Racing F1|Honda]]
|Fast_Time      = 1:20.787
|Fast_Lap       = 82
|Fast_Country   = BRA
|First_Driver   = [[Alain Prost]]
|First_Team     = [[Team McLaren|McLaren]]-[[Techniques d'Avant Garde|TAG]]
|First_Country  = FRA
|Second_Driver  = [[Nelson Piquet]]
|Second_Team    = [[WilliamsF1|Williams]]-[[Honda Racing F1|Honda]]
|Second_Country = BRA
|Third_Driver   = [[Stefan Johansson]]
|Third_Team     = [[Scuderia Ferrari|Ferrari]]
|Third_Country  = SWE
}}

The '''1986 Australian Grand Prix''' was a [[Formula One]] motor race held on 26 October 1986 at the [[Adelaide Street Circuit]], Adelaide, Australia. It was the last of 16 races in the [[1986 Formula One season]], and decided a three-way tussle for the Drivers' Championship. The drivers in contention for the title were; [[Nigel Mansell]], [[Nelson Piquet]], both of whom were racing for the [[WilliamsF1|Williams]]-[[Honda Racing F1|Honda]] team, and [[McLaren (racing)|McLaren]]'s [[Alain Prost]].

Mansell took [[pole position]] for the race, but a poor start off the grid enabled team-mate Piquet, [[Ayrton Senna]] and [[Keke Rosberg]] to overtake him and demote him to fourth by the end of the first lap.

A few laps into the race, Finland's Keke Rosberg, in his final Grand Prix, took the lead from Piquet. However, the Finn retired with a puncture on lap 63, handing the lead back to Piquet and elevating Mansell into third place, which would have been sufficient for the Englishman to secure the championship. One lap later, Mansell's race ended  as his left-rear tyre exploded on the main straight with 19 laps remaining. The title was then between Piquet and Prost with the Frenchman needing to finish ahead of the Brazilian  to successfully defend his title. Following the tyre failures of Rosberg and Mansell, the Williams team called Piquet in for a precautionary tyre change leaving him 15 seconds behind. He made a late charge to close the gap to 4.2 seconds but Prost took victory to win his second of four titles.

It was not until the [[2007 Brazilian Grand Prix]] that there were again three possible drivers' title contenders entering the final race of the season.

==Background==
Coming into the race, three drivers had a chance of winning the title. British driver [[Nigel Mansell]] was the leader; six points behind was defending champion [[Alain Prost]], and one point behind Prost was Mansell's teammate at [[WilliamsF1|Williams]], [[Nelson Piquet]]. In fourth was [[Ayrton Senna]], who was guaranteed to finish in that position regardless of what happened. Prost was aiming to become the first driver since Australia's [[Jack Brabham]] in {{f1|1959}} and {{f1|1960}} to win back-to-back World Drivers' Championships.

The Williams-[[Honda F1|Honda]] cars of Mansell and Piquet were superior in speed to Prost's McLaren-[[Techniques d'Avant Garde|TAG]]. However, Prost's consistency had seen him accumulate points all year, while the Williams pair battled with one another and their mind games eroded what would have been a dominant season for the team. While Mansell and Piquet had accumulated more than enough points to win the Constructors' Championship for Williams, they had taken enough points from each other to allow the consistent Prost to remain in contention.

To win the championship Mansell needed either third position or higher, or for both Prost and Piquet to finish in second place or lower. For Prost or Piquet to win the championship, they would have to win the race, and see Mansell, who had won more races for the year (5), finish in fourth position or lower.

Unlike the Drivers' Championship, the Constructors' Championship had already been decided in Williams' favour, as they had a 48-point advantage over [[McLaren]] (while Prost had a consistent season winning 3 races with another 6 podiums, unlike at Williams his team mate [[Keke Rosberg]] had suffered numerous retirements while not winning a race). McLaren's place as runners-up was secured, as they were 30 points ahead of third placed [[Team Lotus|Lotus]] who in a similar situation to McLaren had to rely mostly on Senna's points with [[Johnny Dumfries]] having only scored 2 points for the year until Adelaide.

==Race summary==
Mansell took [[pole position]] for the race with a time of 1 minute 18.403 seconds. His teammate, Nelson Piquet, and [[Team Lotus|Lotus]]' Ayrton Senna  were the only drivers within a second of Mansell's time. The third title contender, Alain Prost, was on the second row of the grid in fourth.

The prospect of a three way battle for the Drivers' Championship crown attracted a capacity crowd of 150,000 to the tight, but fast [[Adelaide Street Circuit|Adelaide circuit]].<ref name="Australian Classic">{{cite news |title=Australian classic – Adelaide, 1986 |url=http://www.formula1.com/news/features/2006/3/4171.html |publisher=Official Formula One website |deadurl=yes |archiveurl=https://web.archive.org/web/20141113022943/http://www.formula1.com/news/features/2006/3/4171.html |archivedate=13 November 2014 |date=31 March 2006 |accessdate=24 January 2016}}</ref>

Mansell started from pole position but yielded the lead to Ayrton Senna's Lotus at the second corner on lap 1 and fell behind both Piquet and Keke Rosberg on the same lap. Piquet slipstreamed past Senna on lap 1 down the Brabham Straight to take the lead, but it would last only six laps as on lap 7, Rosberg took the lead from Piquet and began to build a sizeable gap between himself and the rest of the field.

On lap 23 Piquet spun, although no damage was sustained to the car, and he continued the race despite dropping back several places. Prost suffered a puncture a few laps later and he dropped to fourth position after having to pit. Piquet charged back through the field, passing Mansell for second place on lap 44, but Prost closed on the two Williams cars and, with 25 laps to go, all three championship contenders were running together in positions 2, 3 and 4.

The battle became one for the lead on lap 63 when Rosberg suffered a right rear tyre failure and retired from the race. Rosberg later revealed that he would never have won the race anyway unless Prost failed to finish or had sufficient problem not to be able to challenge, as he had promised Prost and the team that he would give best to his team mate to help his bid to win back-to-back championships.<ref>{{cite web |url=http://www.autosport.com/news/report.php/id/14497 |title=Ask Nigel: May 23 |work=AutoSport |date=23 May 2001 |accessdate=13 November 2014}}</ref><ref>{{cite web |url=http://www.f1fanatic.co.uk/2007/10/18/grand-prix-flashback-australia-1986/ |title=Heartbreak for Mansell in dramatic Adelaide finale |publisher=F1Fanatic |date=18 October 2007 |accessdate=13 November 2014}}</ref> Prost had just passed Mansell for third which became second when Rosberg retired, with Piquet now leading. Mansell only needed a third-place finish to win the championship.

Mansell was still in third position when, on lap 64, his left rear tyre exploded at {{Convert|180|mph|km/h|0|abbr=on}} on the high-speed Brabham Straight while lapping the [[Equipe Ligier|Ligier]] of [[Philippe Alliot]], sending a shower of sparks flying behind him and severely damaging his left rear suspension. The Williams coasted to a stop in the run-off area at the end of the straight, Mansell managing to avoid hitting anything. Fearing the same happening to the second car, Williams called Piquet to the pits and Prost took the lead (Piquet for his part believed that Williams made the right call bringing him in for tyres). Piquet would make a late charge, closing the gap from 15.484 seconds with 2 laps remaining to just 4.205 at the finish and Prost claimed both the race and the World Championship. Prost had so little fuel left that he pulled up only metres past the finish line in what had been standard practice since his disqualification for being underweight after 'winning' the [[1985 San Marino Grand Prix]].

In his last race for [[Scuderia Ferrari|Ferrari]], [[Stefan Johansson]] completed the podium in third place, albeit a lap down on Prost and Piquet. [[Martin Brundle]] ran out of fuel as he crossed the line in fourth place in his [[Tyrrell Racing|Tyrrell]]-Renault. His team mate [[Philippe Streiff]] finished fifth 2 laps down, while Johnny Dumfries, who thrilled television audiences with his onboard camera throughout the weekend, finished sixth in his Lotus-Renault.

By winning, Alain Prost became the first and so far only driver (as of [[2016 Australian Grand Prix|2016]]) to ever win the Australian Grand Prix in both Australian domestic and World Championship form, having won the non-championship [[1982 Australian Grand Prix]] run for [[Formula Pacific]] cars at the [[Calder Park Raceway]] in [[Melbourne]].

This was the last race for the Renault [[V6 engine|V6]] turbo engine, the French company being the pioneers in F1 turbocharging back in {{f1|1977}}, as well as Renault's last Formula One race as an engine supplier until their return with Williams at the [[1989 Brazilian Grand Prix]]. It was also the last Formula One race for Australia's {{f1|1980}} World Champion [[Alan Jones (racing driver)|Alan Jones]] and his team mate [[Patrick Tambay]], and the last race for [[Haas Lola|Team Haas]], whom both Jones and Tambay drove for. It was also the last race for Lotus driver Johnny Dumfries, [[Osella]]'s [[Allen Berg]], [[Zakspeed]]'s [[Huub Rothengatter]], and {{f1|1982}} World Champion Keke Rosberg.

== Classification ==

=== Qualifying ===
{| class="wikitable sortable" style="font-size: 95%;"
|- 
! Pos !! No !! Driver !! Constructor !! Q1 !! Q2 !! Gap
|-
! 1
| 5
| {{flagicon|GBR}} [[Nigel Mansell]]
| [[WilliamsF1|Williams]]-[[Honda Racing F1|Honda]]
| 1:19.255
| '''1:18.403'''
|align="center"| —
|-
! 2
| 6
| {{flagicon|BRA}} [[Nelson Piquet]]
| [[WilliamsF1|Williams]]-[[Honda Racing F1|Honda]]
| 1:20.088
| '''1:18.714'''
| +0.311
|-
! 3
| 12
| {{flagicon|BRA}} [[Ayrton Senna]]
| [[Team Lotus|Lotus]]-[[Renault F1|Renault]]
| 1:21.302
| '''1:18.906'''
| +0.503
|-
! 4
| 1
| {{flagicon|FRA}} [[Alain Prost]]
| [[McLaren (racing)|McLaren]]-[[Techniques d'Avant Garde|TAG]]
| 1:19.785
| '''1:19.654'''
| +1.251
|-
! 5
| 25
| {{flagicon|FRA}} [[René Arnoux]]
| [[Equipe Ligier|Ligier]]-[[Renault F1|Renault]]
| 1:20.491
| '''1:19.976'''
| +1.573
|-
! 6
| 20
| {{flagicon|AUT}} [[Gerhard Berger]]
| [[Benetton Formula|Benetton]]-[[BMW in Formula One|BMW]]
| 1:22.260
| '''1:20.554'''
| +2.151
|-
! 7
| 2
| {{flagicon|FIN}} [[Keke Rosberg]]
| [[McLaren (racing)|McLaren]]-[[Techniques d'Avant Garde|TAG]]
| 1:21.295
| '''1:20.778'''
| +2.375
|-
! 8
| 26
| {{flagicon|FRA}} [[Philippe Alliot]]
| [[Equipe Ligier|Ligier]]-[[Renault F1|Renault]]
| 1:22.765
| '''1:20.981'''
| +2.578
|-
! 9
| 27
| {{flagicon|ITA}} [[Michele Alboreto]]
| [[Scuderia Ferrari|Ferrari]]
| '''1:21.709'''
| 1:21.747
| +3.306
|-
! 10
| 4
| {{flagicon|FRA}} [[Philippe Streiff]]
| [[Tyrrell Racing|Tyrrell]]-[[Renault F1|Renault]]
| 1:23.262
| '''1:21.720'''
| +3.317
|-
! 11
| 23
| {{flagicon|ITA}} [[Andrea de Cesaris]]
| [[Minardi]]-[[Motori Moderni]]
| 1:23.476
| '''1:22.012'''
| +3.609
|-
! 12
| 28
| {{flagicon|SWE}} [[Stefan Johansson]]
| [[Scuderia Ferrari|Ferrari]]
| '''1:22.050'''
| 1:22.309
| +3.647
|-
! 13
| 19
| {{flagicon|ITA}} [[Teo Fabi]]
| [[Benetton Formula|Benetton]]-[[BMW in Formula One|BMW]]
| 1:22.584
| '''1:22.129'''
| +3.726
|-
! 14
| 11
| {{flagicon|GBR}} [[John Crichton-Stuart, 7th Marquess of Bute|Johnny Dumfries]]
| [[Team Lotus|Lotus]]-[[Renault F1|Renault]]
| 1:23.786
| '''1:22.664'''
| +4.261
|-
! 15
| 15
| {{flagicon|AUS}} [[Alan Jones (Formula 1)|Alan Jones]]
| [[Lola Racing Cars|Lola]]-[[Ford Motor Company|Ford]]
| 24:46.383
| '''1:22.796'''
| +4.393
|-
! 16
| 3
| {{flagicon|GBR}} [[Martin Brundle]]
| [[Tyrrell Racing|Tyrrell]]-[[Renault F1|Renault]]
| 1:24.061
| '''1:23.004'''
| +4.601
|-
! 17
| 16
| {{flagicon|FRA}} [[Patrick Tambay]]
| [[Lola Racing Cars|Lola]]-[[Ford Motor Company|Ford]]
| 1:24.584
| '''1:23.008'''
| +4.605
|-
! 18
| 24
| {{flagicon|ITA}} [[Alessandro Nannini]]
| [[Minardi]]-[[Motori Moderni]]
| 1:25.593
| '''1:23.052'''
| +4.649
|-
! 19
| 7
| {{flagicon|ITA}} [[Riccardo Patrese]]
| [[Brabham]]-[[BMW in Formula One|BMW]]
| 1:23.396
| '''1:23.230'''
| +4.827
|-
! 20
| 8
| {{flagicon|GBR}} [[Derek Warwick]]
| [[Brabham]]-[[BMW in Formula One|BMW]]
| 1:23.552
| '''1:23.313'''
| +4.910
|-
! 21
| 14
| {{flagicon|GBR}} [[Jonathan Palmer]]
| [[Zakspeed]]
| 1:24.509
| '''1:23.476'''
| +5.073
|-
! 22
| 18
| {{flagicon|BEL}} [[Thierry Boutsen]]
| [[Arrows Grand Prix International|Arrows]]-[[BMW in Formula One|BMW]]
| 1:24.768
| '''1:24.295'''
| +5.892
|-
! 23
| 29
| {{flagicon|NED}} [[Huub Rothengatter]]
| [[Zakspeed]]
| 1:25.746
| '''1:25.181'''
| +6.778
|-
! 24
| 17
| {{flagicon|FRG}} [[Christian Danner]]
| [[Arrows Grand Prix International|Arrows]]-[[BMW in Formula One|BMW]]
| 1:25.296
| '''1:25.233'''
| +6.831
|-
! 25
| 21
| {{flagicon|ITA}} [[Piercarlo Ghinzani]]
| [[Osella]]-[[Alfa Romeo]]
| 3:03.680
| '''1:25.257'''
| +6.855
|-
! 26
| 22
| {{flagicon|CAN}} [[Allen Berg]]
| [[Osella]]-[[Alfa Romeo]]
| 1:28.912
| '''1:27.208'''
| +8.806
|}

=== Race ===
{| class="wikitable" style="font-size: 95%;"
|-
! Pos !! No !! Driver !! Constructor !! Laps !! Time/Retired !! Grid !! Points
|-
! 1
| 1
| {{flagicon|FRA}} '''[[Alain Prost]]'''
| '''[[Team McLaren|McLaren]]-[[Techniques d'Avant Garde|TAG]]'''
| 82
| 1:54:20.388
| 4
| '''9'''
|-
! 2
| 6
| {{flagicon|BRA}} '''[[Nelson Piquet]]'''
| '''[[WilliamsF1|Williams]]-[[Honda Racing F1|Honda]]'''
| 82
| + 4.205
| 2
| '''6'''
|-
! 3
| 28
| {{flagicon|SWE}} '''[[Stefan Johansson]]'''
| '''[[Scuderia Ferrari|Ferrari]]'''
| 81
| + 1 Lap
| 12
| '''4'''
|-
! 4
| 3
| {{flagicon|GBR}} '''[[Martin Brundle]]'''
| '''[[Tyrrell Racing|Tyrrell]]-[[Renault F1|Renault]]'''
| 81
| + 1 Lap
| 16
| '''3'''
|-
! 5
| 4
| {{flagicon|FRA}} '''[[Philippe Streiff]]'''
| '''[[Tyrrell Racing|Tyrrell]]-[[Renault F1|Renault]]'''
| 80
| Out of Fuel
| 10
| '''2'''
|-
! 6
| 11
| {{flagicon|GBR}} '''[[John Crichton-Stuart, 7th Marquess of Bute|Johnny Dumfries]]'''
| '''[[Team Lotus|Lotus]]-[[Renault F1|Renault]]'''
| 80
| + 2 Laps
| 14
| '''1'''
|-
! 7
| 25
| {{flagicon|FRA}} [[René Arnoux]]
| [[Equipe Ligier|Ligier]]-[[Renault F1|Renault]]
| 79
| + 3 Laps
| 5
| &nbsp;
|-
! 8
| 26
| {{flagicon|FRA}} [[Philippe Alliot]]
| [[Equipe Ligier|Ligier]]-[[Renault F1|Renault]]
| 79
| + 3 Laps
| 8
| &nbsp;
|-
! 9
| 14
| {{flagicon|GBR}} [[Jonathan Palmer]]
| [[Zakspeed]]
| 77
| + 5 Laps
| 21
| &nbsp;
|-
! 10
| 19
| {{flagicon|ITA}} [[Teo Fabi]]
| [[Benetton Formula|Benetton]]-[[BMW in Formula One|BMW]]
| 77
| + 5 Laps
| 13
| &nbsp;
|-
! NC
| 16
| {{flagicon|FRA}} [[Patrick Tambay]]
| [[Lola Racing Cars|Lola]]-[[Ford Motor Company|Ford]]
| 70
| Not Classified
| 17
| &nbsp;
|-
! Ret
| 5
| {{flagicon|GBR}} [[Nigel Mansell]]
| [[WilliamsF1|Williams]]-[[Honda Racing F1|Honda]]
| 63
| Tyre
| 1
| &nbsp;
|-
! Ret
| 7
| {{flagicon|ITA}} [[Riccardo Patrese]]
| [[Brabham]]-[[BMW in Formula One|BMW]]
| 63
| Electrical
| 19
| &nbsp;
|-
! Ret
| 2
| {{flagicon|FIN}} [[Keke Rosberg]]
| [[Team McLaren|McLaren]]-[[Techniques d'Avant Garde|TAG]]
| 62
| Tyre
| 7
| &nbsp;
|-
! NC
| 22
| {{flagicon|CAN}} [[Allen Berg]]
| [[Osella]]-[[Alfa Romeo (Formula One)|Alfa Romeo]]
| 61
| Not Classified
| 26
| &nbsp;
|-
! Ret
| 8
| {{flagicon|GBR}} [[Derek Warwick]]
| [[Brabham]]-[[BMW in Formula One|BMW]]
| 57
| Brakes
| 20
| &nbsp;
|-
! Ret
| 17
| {{flagicon|FRG}} [[Christian Danner]]
| [[Arrows Grand Prix International|Arrows]]-[[BMW in Formula One|BMW]]
| 52
| Engine
| 24
| &nbsp;
|-
! Ret
| 18
| {{flagicon|BEL}} [[Thierry Boutsen]]
| [[Arrows Grand Prix International|Arrows]]-[[BMW in Formula One|BMW]]
| 50
| Engine
| 22
| &nbsp;
|-
! Ret
| 12
| {{flagicon|BRA}} [[Ayrton Senna]]
| [[Team Lotus|Lotus]]-[[Renault F1|Renault]]
| 43
| Engine
| 3
| &nbsp;
|-
! Ret
| 23
| {{flagicon|ITA}} [[Andrea de Cesaris]]
| [[Minardi]]-[[Motori Moderni]]
| 40
| Mechanical
| 11
| &nbsp;
|-
! Ret
| 20
| {{flagicon|AUT}} [[Gerhard Berger]]
| [[Benetton Formula|Benetton]]-[[BMW in Formula One|BMW]]
| 40
| Engine
| 6
| &nbsp;
|-
! Ret
| 29
| {{flagicon|NED}} [[Huub Rothengatter]]
| [[Zakspeed]]
| 29
| Suspension
| 23
| &nbsp;
|-
! Ret
| 15
| {{flagicon|AUS}} [[Alan Jones (Formula 1)|Alan Jones]]
| [[Lola Racing Cars|Lola]]-[[Ford Motor Company|Ford]]
| 16
| Engine
| 15
| &nbsp;
|-
! Ret
| 24
| {{flagicon|ITA}} [[Alessandro Nannini]]
| [[Minardi]]-[[Motori Moderni]]
| 10
| Accident
| 18
| &nbsp;
|-
! Ret
| 21
| {{flagicon|ITA}} [[Piercarlo Ghinzani]]
| [[Osella]]-[[Alfa Romeo (Formula One)|Alfa Romeo]]
| 2
| Transmission
| 25
| &nbsp;
|-
! Ret
| 27
| {{flagicon|ITA}} [[Michele Alboreto]]
| [[Scuderia Ferrari|Ferrari]]
| 0
| Collision
| 9
| &nbsp;
|-
!colspan="8"|{{center|Source:<ref>{{cite web |url=http://www.formula1.com/results/season/1986/262/ |title=1986 Australian Grand Prix |publisher=formula1.com |accessdate=23 December 2015 |archiveurl=https://web.archive.org/web/20141113023220/http://www.formula1.com/results/season/1986/262/ |archivedate=13 November 2014}}</ref>}}
|}

==Lap leaders==
[[Nelson Piquet]] 8 (1–6, 63–64), [[Keke Rosberg]] 56 (7–62), [[Alain Prost]] 18 (65–82)

==Championship standings after the race==
* '''Bold text''' indicates the World Champions.
{{col-start}}
{{col-2}}
;Drivers'  Championship standings
{|class="wikitable" style="font-size: 95%;"
|-
! Pos
! Driver
! Points
|-
| 1
| {{flagicon|FRA}} '''[[Alain Prost]]'''
|align="right"| 72 (74)
|-
| 2
| {{flagicon|GBR}} [[Nigel Mansell]]
|align="right"| 70 (72)
|-
| 3
| {{flagicon|BRA}} [[Nelson Piquet]]
|align="right"|  69
|-
| 4
| {{flagicon|BRA}} [[Ayrton Senna]]
|align="right"| 55
|-
| 5
| {{flagicon|SWE}} [[Stefan Johansson]]
|align="right"| 23
|}
{{col-2}}
;Constructors'   Championship standings
{|class="wikitable" style="font-size:95%;"
|-
! Pos
! Constructor
! Points
|-
| 1
| {{flagicon|GBR}} '''[[WilliamsF1|Williams]]-[[Honda Racing F1|Honda]]'''
|align="right"|  141
|-
| 2
| {{flagicon|GBR}} [[McLaren (racing)|McLaren]]-[[Techniques d'Avant Garde|TAG]]      
|align="right"| 96
|-
| 3
| {{flagicon|GBR}} [[Team Lotus|Lotus]]-[[Renault F1|Renault]]
|align="right"| 58
|-
| 4
| {{flagicon|ITA}} [[Scuderia Ferrari|Ferrari]]
|align="right"| 37
|-
| 5
| {{flagicon|FRA}}  [[Equipe Ligier|Ligier]]-[[Renault F1|Renault]]
|align="right"|     29
|}
{{col-end}}

* <small>'''Note''':         Only the top five positions are included for both sets of  standings.</small>

==References==
{{reflist | 30em}}

{{F1 race report
|Name_of_race            = [[Australian Grand Prix]]
|Year_of_race            = 1986
|Previous_race_in_season = [[1986 Mexican Grand Prix]]
|Next_race_in_season     = [[1987 Brazilian Grand Prix]]
|Previous_year's_race    = [[1985 Australian Grand Prix]]
|Next_year's_race        = [[1987 Australian Grand Prix]]
}}
{{F1GP 80-89}}

[[Category:1986 in Australian motorsport|Grand Prix]]
[[Category:1986 Formula One races|Australian Grand Prix]]
[[Category:Australian Grand Prix]]
[[Category:Motorsport in Adelaide]]
[[Category:October 1986 sports events|Australian Grand Prix]]