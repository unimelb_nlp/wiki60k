[[File:ALHAT logo.jpeg|thumb|Autonomous Landing and Hazard Avoidance Technologies (ALHAT) Project logo]]

[[File:Using a helicopter to test the ALHAT equipment.jpg|thumb|Photograph of the ALHAT automatic landing equipment being tested using a helicopter.]]

{| class="wikitable infobox" style="margin: 0.5em 0 0.5em 1em; width: 22em;"
|-
|+ ALHAT
|-
! Description !! Size !! Ref
|-
| Lighting conditions || any || <ref name=ALHref005 />
|-
| Global landing precision || ±90&nbsp;m || <ref name=ALHref005 />
|-
| Local landing precision || ±3&nbsp;m || <ref name=ALHref005 />
|-
| detects hazard elevation (rocks) || > 30&nbsp;cm || <ref name=ALHref005 />
|-
| detects hazard slopes || > 5° || <ref name=ALHref005 />
|-
| Dimensions || (TBD) || –
|-
| Mass || 400&nbsp;lb || <ref name=ALHref025>
{{cite web
|last=Steven Siceloff
|title=Equipped with New Sensors, Morpheus Preps to Tackle Landing on its Own
|url=http://www.nasa.gov/content/equipped-with-new-sensors-morpheus-preps-to-tackle-landing-on-its-own/#.U1msbKIXJDw
|work=NASA website
|publisher=NASA
|accessdate=April 25, 2014
}}</ref> 
|-
| Class of lasers || IV || <ref name=ALHref016/>
|}
'''Autonomous Landing Hazard Avoidance Technology (ALHAT)''' is technology [[NASA]] is developing to autonomously land spacecraft on the Moon, Mars or even an asteroid.<ref name=ALHref007/><ref name=ALHref001>
{{cite web
|url=http://alhat.jpl.nasa.gov/index.php
|accessdate=February 8, 2013
|title= ALHAT at JPL Home page
|publisher=NASA
}}</ref>

According to the NASA web page on the project, it will provide state-of-the-art automated descent and landing system for planetary lander craft.  A surface-tracking sensor suite with real-time hazard avoidance capabilities will assess altitude and velocity of the descending vehicle and the topography of the landing site to permit precision landing.  The descending craft will use the ALHAT algorithms combined with sensor data to navigate to the "pre-mission landing aim point," where it will autonomously identify safe landing areas and guide the craft to touchdown.  The technology will work in any lighting conditions, from the harsh glare of an unshielded Sun to the cloudy, gaseous murk of a distant Solar System body.<ref name=ALHref003>abridged quotation from: 
{{cite web
|url= http://www.nasa.gov/mission_pages/tdm/alhat/alhat_overview.html
|accessdate=February 8, 2013
|title= ALHAT overview
|work= Technology Demonstration Missions
|publisher=NASA
}}</ref>

A landing craft equipped with ALHAT will have the ability to detect and avoid obstacles such as craters, rocks and slopes and land safely and precisely on a surface. The project is led by [[Johnson Space Center]] (JSC) and supported by [[Jet Propulsion Laboratory]] (JPL) and [[Langley Research Center]].<ref name=ALHref001 />  Some of the sensors may also be used to help spacecraft dock.<ref name=ALHref031>
{{cite web
|last1=John M. Carson
|title=@MorpheusLander @A_M_Swallow some of the same sensors are being looked at for just that!
|url=https://twitter.com/jmcarson3/status/532943964987740160
|website=www.twitter.com
|publisher=NASA
|accessdate=November 13, 2014
}}</ref>

The ALHAT technologies include a Hazard Detection System, a [[Laser Doppler velocimetry|lidar Doppler velocimeter]], a laser [[altimeter]], software, sensor algorithms and path-to-space computer processors.  These technologies integrate with the lander’s onboard navigation instrumentation.<ref name=ALHref007 />  The equipment has a mass of {{convert |400 |lb |kg |abbr=on}}.<ref name=ALHref025 />

The instrumentation has been tested by operating from moving vehicles – a truck, NASA's Huey helicopter and the [[Project Morpheus]] lander.  At the end of testing the project is aiming for the ALHAT equipment to have reached [[Technology Readiness Level]] (TRL) 6.<ref name=ALHref005 /><ref name=ALHref001 />

The ALHAT project has been superseded by NASA's CoOperative Blending of Autonomous Landing Technologies (COBALT) project. NASA claim that COBALT's Navigation Doppler Lidar (NDL) is 60 percent smaller, operates at nearly triple the speed and provides longer range measurement.<ref name="ALHATref036">
{{cite web
|last1=Loura Hall
|title=COBALT Flight Demonstrations Fuse Technologies to Gain Precision Landing Results
|url=https://www.nasa.gov/directorates/spacetech/flight_opportunities/feature/COBALT_Flight_Demonstrations_Fuse_Technologies
|website=www.nasa.gov
|publisher=NASA
|accessdate=17 March 2017
}}</ref>

== Technology ==
[[File:Tilt testing of ALHAT on Morpheus lander.jpg|thumb|Testing to ensure the ALHAT can still target when tilted.  It is integrated into a Morpheus lander.]]

Resources needed by future expeditions will frequently be situated in potentially hazardous terrain, consequently robotic and human explorers need to land safely near to these resources. This requires a new generation of planetary landers with the ability to automatically recognize their desired landing site, assess potential landing hazards and adjust as they descend to the surface.<ref name=ALHref002>
{{cite web
|url= http://www.nasa.gov/mission_pages/tdm/alhat/index.html
|accessdate=February 8, 2013
|title= NASA – Autonomous Landing and Hazard Avoidance Technology(ALHAT)
|work= Technology Demonstration Missions
|publisher= NASA
}}</ref>
NASA Langley created three [[lidar]] (light radar) sensors: the flash lidar, Doppler lidar and high-altitude laser altimeter for the ALHAT project.<ref name=ALHref004>
{{cite web
|url= http://www.nasa.gov/centers/langley/news/researchernews/rn_ALHAT.html
|accessdate= February 8, 2013
|title= ALHAT Detects Landing Hazards on the Surface
|work= Research News, Langley Research Center
|publisher= NASA
}}</ref>

The flash [[lidar]] uses imagery technology to detect objects larger than the size of a basketball on a planetary surface under all lighting conditions. If there is an obstacle, the System will divert the vehicle to a safer landing spot.<ref name=ALHref004/>  The three-dimensional camera sensor engine also forms part of the DragonEye space camera used by the Dragon spacecraft to dock with the International Space Station.<ref name=ALHref008>
{{cite web
|url= http://www.advancedscientificconcepts.com/products/dragoneye.html
|accessdate= February 15, 2013
|title= DragonEye 3D Flash LIDAR Space Camera
|publisher= Advanced Scientific Concepts, Inc.
}}</ref>

The flash lidar flashes a laser and acts like a flash camera permitting the generation of lidar maps and images.<ref name=ALHref004 />
The Doppler lidar measures the vehicle's altitude and velocity to precisely land on the surface, and the high-altitude laser altimeter provides data enabling the vehicle to land in the chosen area.<ref name=ALHref004 /> The lidar laser technology scans an area for hazards like craters or rocks before the lander touches down.  The onboard system uses the data to build a terrain and elevation map of potential landing sites in real time.  ALHAT first scans from a high altitude giving the spacecraft sufficient to respond to obstacles or craters at the landing site.  Safe sites are designated based on factors including the tilt angle of the surface, the distance and fuel cost to get to a site and the position of the lander’s footpads.<ref name=ALHref009>
{{cite news
|url= http://www.technologyreview.com/news/414523/how-to-land-safely-back-on-the-moon
|accessdate= February 15, 2013
|title= How to Land Safely Back on the Moon
|first= Anne-Marie 
|last= Corley
|work= MIT Technology Review
|date= July 29, 2009
}}</ref>

== History and plans ==

NASA's Johnson Space Center leads the ALHAT project, begun in early 2006, for NASA's Exploration Technology Development Program. Support is also provided by Charles Stark Draper Labs and the Johns Hopkins Applied Physics Laboratory, Baltimore.  Langley designed two special-purpose light detection and ranging (lidar) sensors. In conjunction with this, NASA's Jet Propulsion Laboratory developed algorithms for analysing the terrain based upon these lidar measurements.<ref name=ALHref006>
{{cite web
|url= http://www.nasa.gov/exploration/home/alhat-project.html
|accessdate= February 8, 2013
|title= Sensors Advance Lunar Landing Project
|work= NASA – Beyond Earth
|publisher= NASA
}}</ref>

The Advanced Exploration Systems program would like to fully demonstrate and space qualify an enhanced ALHAT by launching a Morpheus lander with the ALHAT system to the Moon on the 2017 launch of the Space Launch System.  If both work Morpheus intends to use ALHAT to land safely on one of the lunar poles.<ref name=ALHref007>
{{cite web
|url= http://www.americaspace.com/?p=17926
|accessdate= February 8, 2013
|title= ALHAT – Getting There Safe, Even In The Dark
|first= Jim 
|last= Hillhouse
|work= AmericaSpace
}}</ref>

Human pilots can be helped by ALHAT technology providing them with much better situational awareness when they land their vehicles.<ref name=ALHref018>
{{cite web
|last=MaryAnn Jackson
|title=ALHAT Passes Tethered Tests with Flying Colors
|url=http://www.nasa.gov/larc/alhat-completes-tethered-tests/#.UgJ25qxM68B
|work=NASA website www.nasa.gov
|publisher=NASA
|accessdate=August 7, 2013
}}</ref>

In July 2013 an air cooled ALHAT was integrated into the Morpheus Lander BRAVO and its guidance software.  Successful test flights were made with the vehicle tethered.  The ALHAT and team them went to the [[Kennedy Space Centre]] for free flight testing.<ref name=ALHref017>
{{cite web
|title=Morpheus/ALHAT TT27
|url=https://www.youtube.com/watch?v=z68yXnEpzIE
|work=YouTube
|publisher=NASA
|accessdate=July 27, 2013
}}</ref>

On November 21, 2013 ALHAT on the prototype Morpheus Lander arrived at the [[Kennedy Space Center]] for free flight testing.<ref name=ALHref019>
{{cite web
|title=Morpheus Prototype Lander Arrives At NASA'S Kennedy Space
|url=https://www.youtube.com/watch?v=TMkPjBQLmgo
|work=You Tube – Matthew Travis
|publisher=NASA
|accessdate=December 2, 2013
}}</ref>  In March 2014 ALHAT and the Bravo lander were integrated again and flight testing performed.<ref name=ALHref022/>

In November 2014 additional ALHAT sensors were fitted to the Morpheus Lander.  The new optics permit the Navigation Doppler Lidar to accurately measure the vehicle's velocity relative to the ground.<ref name=ALHref034>
{{cite web
|last1=Project Morpheus
|title=Post on November 12, 2014
|url=https://www.facebook.com/MorpheusLander/photos/pb.156555054405321.-2207520000.1415905964./792642740796546/?type=1&theater
|website=www.Facebook.com
|publisher=NASA
|accessdate=November 13, 2014
}}</ref> The Morpheus/ALHAT successfully conducted Free Flight 15 (FF15) on December 15, 2015 – a closed-loop ALHAT flight and landing.<ref name=ALHref033 />

The connections between the Morpheus vehicle and ALHAT were documented in ICD (Interface Control Documents).<ref name=ALHref035>
{{cite book
|last9=Glenn D.Hines
|last8=Martin, Keith E.
|last7=Trawny, Nikolas
|last6=Pierrottet, Diego F.
|last5=Busa, Joseph L.
|last4=Villalpando, Carlos
|last3=Roback, Vincent E
|last2=Hirsh, Robert L.
|last1=John M.Carson III
|title=Interfacing and Verifying ALHAT Safe Precision Landing Systems with the Morpheus Vehicle
|date=January 5, 2015
|publisher=AIAA SciTech 2015 Conference; 5–9 Jan. 2015; Kissimmee, FL; United States
|edition=JSC-CN-32396
|url=http://hdl.handle.net/2060/20140017031
|accessdate=April 12, 2015
}}</ref>

== Testing ==

A variety of field tests have been performed on the ALHAT equipment.  The tests have been designed to demonstrate that the ALHAT equipment has reached TRL 6.<ref name=ALHref005>
{{cite journal
|url= http://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/20100025705_2010028092.pdf
|accessdate=February 8, 2013
|title= AUTONOMOUS PRECISION LANDING AND HAZARD AVOIDANCE TECHNOLOGY (ALHAT) PROJECT STATUS AS OF MAY 2010
|first1= Scott A. 
|last1= Striepe
|first2= Chirold D. 
|last2= Epp
|first3= Edward A. 
|last3= Robertson
|format= pdf
}}</ref>

The ALHAT's ability to detect items larger than a baseball from a distance of {{convert |2500 |ft |m}} whilst moving was tested by placing the system on a truck.  The ALHAT was able to image and navigate while the team were driving.
<ref name=ALHref010>{{cite web
|title=ALHAT Detects Landing Hazards on the Surface
|url=http://www.nasa.gov/centers/langley/news/researchernews/rn_ALHAT.html
|date=September 7, 2012
|publisher=NASA
|accessdate=March 6, 2013
}}</ref> 
The larger version of ALHAT was tested using helicopter flights at NASA's Dryden Flight Research Center, Edwards, California in 2010.<ref name=ALHref011>
{{cite web
|title=Sensors to Guide Spacecraft to Safe, Distant Landings|url=http://www.nasa.gov/exploration/features/alhat.html
|date=August 19, 2010
|publisher=NASA
|accessdate=March 6, 2013
}}</ref>
A lunar terrain field was constructed at Kennedy Space Centre (KSC) for ALHAT on Morpheus lander testing. The field has an array of different terrain features to test ALHAT's ability to detect hazards.  Initial testing used the lightweight ALHAT on a helicopter.<ref name=ALHref012>
{{cite web
|title=Lunar Terrain Field Constructed at Kennedy Space Center
|url=http://alhat.jpl.nasa.gov/image.php?galleryID=10
|work=ALHAT at JPL
|publisher=NASA
|accessdate=March 6, 2013
}}</ref>

A helicopter test of the integrated ALHAT System with the Morpheus avionics was performed over the ALHAT planetary hazard field at KSC. The KSC helicopter tests included flight profiles approximating planetary approaches, with the entire ALHAT system interfaced with all appropriate Morpheus subsystems and operated in real-time. During these helicopter flights, the ALHAT system imaged the simulated lunar terrain. Use of a helicopter permitted most but not all of the testing to be performed. Good data was obtained from all of the sensors. All of the problems detected have been identified and fixed to support future testing on the Morpheus Lander.<ref name=ALHref021>
{{cite web
|author1=Epp, Chirold D. |author2=Robertson, Edward A. |author3=Ruthishauser, David K. |title=Helicopter Field Testing of NASA’s Autonomous Landing and Hazard Avoidance Technology (ALHAT) System fully integrated with the Morpheus Vertical Test Bed Avionics
|url=http://hdl.handle.net/2060/20140000960
|work=Conference paper on NASA Technical Reports Server
|publisher=AIAA Space 2013 Conference; 10–12 Sep. 2013; San Diego, CA; United States
|accessdate=March 10, 2014
}}</ref>

In July 2013 the integration of ALHAT with the Project Morpheus lander version 1.5 hardware resumed.  Tests included placing the lander on blocks to verify that the ALHAT Inertial Measurement Unit (IMU) worked when tilted.  Tilting tests were performed at several different heights and directions.<ref name=ALHref013>
{{cite web
|last1=Project Morpheus
|title=Facebook posts on July 2, 2013
|url=http://www.facebook.com/MorpheusLander
|work=Facebook
|publisher=NASA
|accessdate=July 2, 2013
}}</ref>

On July 23, 2013 the Morpheus/ALHAT team successfully completed Tethered Test #26 with the ALHAT integrated into Morpheus' Bravo vehicle.   All test objectives were met including ALHAT tracking & imaging.  Imaging was performed at several heights.<ref name=ALHref015>
{{cite web
|title=Morpheus/ALHAT Tether Test 26
|url=https://www.youtube.com/watch?feature=player_embedded&v=hpojrJZKt_o
|work=YouTube
|publisher=NASA
|accessdate=July 23, 2013
}}</ref>  An example of the imaging produced by the LIDAR can be seen in the picture.
[[File:LIDAR imaging of hazard target by ALHAT on hovering Lander.jpg|thumb|The ALHAT LIDAR imaging system detected and identified the target as a hazard whilst flying as part of a Morpheus lander|centre]]

On July 27, 2013 Bravo and ALHAT flew again in TT27.  The ALHAT's tracking & imaging meeting all of the test objectives.<ref name=ALHref017/>

During the rest of 2013 and early 2014 the ALHAT was removed from Bravo whilst various enhancements were made to the lander and flight tested.  In March 2014 the ALHAT was installed back into the lander.  On March 27, 2014 the assembly successfully performed a tethered test hovering at a variety of heights.<ref name=ALHref022>
{{cite web
|title=Morpheus Completes Tethered Flight With Test of Hazard Avoidance System
|url=https://www.youtube.com/watch?v=Pi48DhfenFg
|work=YouTube – NASAKennedy
|publisher=NASA
|accessdate=March 27, 2014
}}</ref>  The ALHAT's laser head can be seen scanning the area in the box during TT34 at the top right of this video.<ref name=ALHref023>
{{cite web
|title=Project Morpheus Tether Test 34
|url=https://www.youtube.com/watch?v=KI3yZyshs9I
|work=YouTube – Morpheus lander
|publisher=NASA
|accessdate=March 28, 2014
}}</ref>  Morpheus Free Flight 10 took place on April 2, 2014.  The ALHAT was in open loop mode.  As well as the flight this video includes shots from a witness camera mounted and carefully aligned to the laser scanning head showing what the laser saw, but in the visible spectrum.  Confirming that the hazard field was scanned.<ref name=ALHref020>
{{cite web
|title=Project Morpheus Free Flight 10
|url=https://www.youtube.com/watch?v=sI5tsetrbpA
|work=YouTube – Morpheus Lander
|publisher=NASA
|accessdate=April 4, 2014
}}</ref>

On April 24, 2014 Free Flight 11 (FF11) was successfully completed at the KSC Shuttle Landing Facility (SLF). In FF11, as in FF10, ALHAT operated in open-loop mode, imaging the Hazard Field and calculating navigation solutions in real time during the flight. The equipment was not (yet) navigating the vehicle, meaning Bravo autonomously flew a pre-programmed trajectory as before. ALHAT engineers will use this flight data to continue tuning and improving their system performance.<ref name=ALHref024>
{{cite web
|title=Project Morpheus Free Flight 11
|url=https://www.youtube.com/watch?v=o5wY0nbjCd8
|work=YouTube – Morpheus lander
|publisher=NASA
|accessdate=April 25, 2014
}}</ref>

A swing test was performed on April 28, 2014 to obtain accurate data on ALHAT's 3-beam Navigation Doppler Lidar to calibrate and confirm its range and velocity measurements.<ref name=ALHref027>
{{cite web
|title=Swing test on ALHAT laser April 28, 2014
|url=https://pbs.twimg.com/media/BmVNSK3IYAE09gc.jpg
|work=Twitter – MorpheusLander
|publisher=NASA
|accessdate=May 1, 2014
}}</ref>  Free Flight 12 on April 30, 2014 was a repeat of the previous flight except the Lander was targeting the HDS identified landing site.<ref name=ALHref028>
{{cite web
|title=Morpheus Free Flight 12
|url=https://www.youtube.com/watch?v=tmkPJUHYdRA
|work=YouTube – Morpheus Lander
|publisher=NASA
|accessdate=May 1, 2014
}}</ref>

Free Flight 13 on May 22, 2014.  First flight of ALHAT and Morpheus Lander with the ALHAT having closed loop control of the lander.  The correct landing location was identified and flown to.  There was a disagreement between the ALHAT and the Morpheus's guidance, navigation, and control over the lander's current location.<ref name=ALHref029>
{{cite web
|title=Morpheus Free Flight 13
|url=https://www.youtube.com/watch?v=1M5qS0Y3tDw
|work=YouTube – Morpheus Lander
|publisher=NASA
|accessdate=May 23, 2014
}}</ref>

Free Flight 14 on May 28, 2014 took place at night.  The ALHAT Hazard Detection System (HDS) performed well, but identified a safe site just 0.5 m outside the conservatively established limits around the center of the landing pad. ALHAT then navigated the vehicle in closed-loop mode through the entire approach, with the vehicle taking over navigation during the descent phase of the trajectory when ALHAT was already dead-reckoning. Had less conservative position error limits allowed ALHAT to continue to navigate to landing, the vehicle still would have landed safely on the pad.<ref name=ALHref030>
{{cite web
|title=Project Morpheus Free flight 14
|url=https://www.youtube.com/watch?v=rv2GoYp8Zaw
|work=YouTube – Morpheus lander
|publisher=NASA
|accessdate=May 29, 2014
}}</ref>

On November 13, 2014 a swing test of the new optics for the ALHAT Navigation Lidar was performed.<ref name=ALHref032>
{{cite web
|last1=Morpheus Lander
|title=Swing test complete. Now realigning the flash Lidar laser and inspecting the pixels. #ALHAT
|url=https://twitter.com/MorpheusLander/status/532988875137110016
|website=www.twitter.com
|publisher=NASA
|accessdate=November 13, 2014
}}</ref> The Morpheus/ALHAT team conducted Free Flight 15 (FF15) on December 15, 2015. The sixth free flight the ALHAT sensor suite on board, and a third attempt at completing a historic closed-loop ALHAT flight.  The vehicle flew and landed successfully under the control of the ALHAT.<ref name=ALHref033>
{{cite web
|title=Morpheus FF15
|url=https://www.youtube.com/watch?v=M3D9m5zhhF8
|website=www.youtube.com
|publisher=NASA – Morpheus Lander
|accessdate=December 17, 2014
}}</ref>

== Health and safety issues ==

The ALHAT is an electrically powered device so the standard techniques for handling and repairing electrical devices apply.<br/>
The equipment contains moving parts which should not be touched when they are moving or powered up.<br/>
The Flash LIDAR and altimeter emit Class IV laser beams.<ref name=ALHref016>
{{cite web
|title=Project Morpheus posting on July 23, 2013
|url=https://www.facebook.com/MorpheusLander|work=Facebook
|publisher=NASA
|accessdate=July 25, 2013
}}</ref>
* Safety goggles should be worn when using Class IV and class 3B Lasers.  That means whenever the ALHAT is powered up.
* The laser beams shall be directed away from people and anything they could damage.
* Aided optics should not be used.
* During tests and operations the outdoor rules may also apply.
The rules for handling lasers at the Johnson Space Centre can be found in Chapter 6-2 of the JSC Handbook.<ref name=ALHref014>
{{cite web
|title=Chapter 6.2 Laser Safety and Health
|url=http://jschandbook.jsc.nasa.gov/docs/JPR1700-1ch6-2.pdf
|work=JSC Safety Handbook JPR1700-1 ch6-2
|publisher=NASA
|accessdate=July 9, 2013
}}</ref>

Use of the [[Project Morpheus]] Thrust Termination System (TTS) by Range Safety shuts down the lander's main engine and stops the Type IV laser in ALHAT's Hazard Detection System (HDS).<ref name=ALHref026>
{{cite web
|last=Morpheus Ops Lean, Ian Young (@ICYprop)
|title=A Typical Morpheus Test Day
|url=http://morpheuslander.blogspot.co.uk/2014/04/a-typical-morpheus-test-day.html
|work=Morpeus website blog
|publisher=NASA
|accessdate=April 26, 2014
}}</ref>

== See also ==

* [[Guidance, navigation, and control]]

== References ==
{{Reflist|33em}}

== External links ==

{{Commons category|Morpheus (lander)}}
* [http://www.nasa.gov/mission_pages/tdm/alhat/index.html Autonomous Landing and Hazard Avoidance Technology (ALHAT) home page]
* [http://morpheuslander.jsc.nasa.gov/ Project Morpheus home page]
* [https://www.youtube.com/watch?v=TtpfLWkokuI&feature=youtube_gdata Video of ALHAT being installed into a Huey Helicopter]
* (pdf) [http://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/20080009586_2008009079.pdf] Autonomous Landing and Hazard Avoidance Technology (ALHAT), 2008 power point by Dr. Chirold Epp
* [http://ieeexplore.ieee.org/xpl/login.jsp?tp=&arnumber=4161571&url=http%3A%2F%2Fieeexplore.ieee.org%2Fxpls%2Fabs_all.jsp%3Farnumber%3D4161571 IEEE Xplore Autonomous Precision Landing and Hazard,  Detection and Avoidance Technology (ALHAT) by C.D. Epp – Aerospace Conference, 2007 IEEE]
* [http://ieeexplore.ieee.org/xpl/login.jsp?tp=&arnumber=4161572&url=http%3A%2F%2Fieeexplore.ieee.org%2Fxpls%2Fabs_all.jsp%3Farnumber%3D4161572  IEEE Xplore ALHAT System Architecture and Operational Concept by T. Brady, Charles Stark and J. Schwartz – Aerospace Conference, 2007 IEEE]

[[Category:Articles created via the Article Wizard]]
[[Category:Spaceflight]]
[[Category:Space applications]]
[[Category:Lidar]]
[[Category:Navigational equipment]]