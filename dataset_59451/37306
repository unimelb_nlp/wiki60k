{{Infobox military person
| name          =Jean-Ambroise Baston, Comte de Lariboisière
| image         =Jean-Ambroise Baston de La Riboisière.jpg
| caption       =Comte de Lariboisière, by [[Jean-Baptiste Mauzaisse]] (1784-1844)
| birth_date          ={{birth-date|18 August 1759}}
| death_date          ={{death-date and age|df=yes|21 December 1812|18 August 1759}} 
| birth_place  =[[Fougères]], [[Britanny]]
| death_place  =[[Königsberg]], [[East Prussia]]
| father        = Ambroise Baston de Lariboisière 
| mother        = Jeanne Monnières
| allegiance    =[[File:Flag of France.svg|22px|link=|alt=]] France
| branch        =[[French Army]]
| serviceyears  =1781–1812
| rank          =[[General of Division]]
| battles       =
[[French Revolutionary Wars]] 
[[Siege of Mainz (1793)|Siege of Mainz]]<br />[[Siege of Toulon (1793)|Siege of Toulon]]<br />[[Army of the Danube|1799 campaign in Germany and Switzerland]]<br><br>
[[Napoleonic Wars]]<br>[[Battle of Austerlitz]]<br />[[Battle of Jena-Auerstadt]]<br />[[Battle of Heilsberg]]<br />[[Battle of Friedland]]<br />[[Siege of Danzig (1807)|Siege of Danzig]]<br />[[Battle of Wagram]]<br>
[[French invasion of Russia]]<br />[[Battle of Smolensk (1812)|Battle of Smolensk]]<br />[[Battle of Borodino]]
| awards        =
Grand Officer of the ''[[Légion d'honneur]]'', 4 June 1807<br>
[[Count of the Empire]], 26 October 1808<ref name=Broughton>Tony Broughton. "The ''Garde Imperiale'' and Its Commanders during the Period 1804&ndash;1815: Artillery."  ''Military Subjects: Organization, Tactics and Strategy.'' [http://www.napoleon-series.org Napoleon Series]. Robert Burnham, Editor in Chief, March 2003. Accessed 17 May 2010.</ref>
| laterwork     =
}}

'''Jean Ambroise Baston de Lariboisière,''' also Count de Lariboisière, was a general of [[artillery]] of the [[First French Empire]]. He fought in the [[French Revolutionary Wars]] and the [[Napoleonic Wars]] and died of fatigue at [[Königsberg]] in [[East Prussia]] on 21 December 1812, during the [[Grand Army]]'s [[French invasion of Russia#Retreat and losses|retreat from Moscow]].

A superb organizer and tactician, Baston de Lariboisière rose rapidly through the artillery ranks and reliably directed the artillery park for the initial engagements of the [[War of the First Coalition]] in 1793&ndash;1794. He also directed the investment and, if necessary, the sieges of [[Fortress of Mainz|Mainz]], [[Ulm]], and [[Danzig]], among others.  In addition, he was a reliable commander of infantry, supporting [[Laurent de Gouvion Saint-Cyr|Laurent Saint-Cyr]]'s corps in northern Italy in the [[War of the Second Coalition|1799 campaign]].

Lariboisière's direction of artillery fire at the [[Battle of Austerlitz]] resulted in the destruction of the ice covering the lake over which Russian army forces retreated. His placement of artillery at the [[Battle of Borodino]] gave the French a tactical advantage in fire on the Russian lines. Lariboisière's artillery also provided the rear guard coverage of the French withdrawal from [[Battle of Berezina|Beresina]]. One of his sons survived the wars and the family founded the [[Lariboisière Hospital]] in Paris.

==Family and education==
He was born on 18 August 1759, in [[Fougères]]. His father, Ambroise Baston de Lariboisière, was lieutenant general of civil and criminal justice and the [[senechal]] of Fougères. His mother was Jeanne Monnières.  Baston de Lariboisière was designated early for a military career.<ref name="Mullié">{{fr icon}} Charles Mullié. "Jean Ambroise Baston de Lariboisière." ''Biographie des célébrités militaires des armées de terre et de mer de 1789 à 1850.'' Paris, 1852</ref>

He was a brilliant student at the military academy and in 1781 received a commission as a lieutenant and entered the same regiment as Napoleon Bonaparte.  Although Lariboisière was a few years older than Bonaparte, they developed a rapport that continued until Lariboisière's death in 1812.<ref name="Mullié"/>

==Military career==
Baston de Lariboisière was cited as a distinguished officer the outset of the [[French Revolution]], for which he showed only moderate support. He was promoted to [[Captain (land)|captain]] in 1791 and assigned to [[Adam Philippe, Comte de Custine]]'s command.  He took part in the invasion of Paville against the Prussians. After the surrender of the French army, he was held as a prisoner of war.<ref name="Mullié"/>

In the following years, he participated in the campaigns of 1796, as adjutant colonel, colonel, and brigadier general, and directed the artillery parks of the [[Army of England]], the [[Army of Helvetia]], the [[Army of the Rhine]].<ref name="Mullié"/> When [[Jean Baptiste Jourdan]] organized the [[Army of the Danube]], Lariboisière received command of the artillery park.<ref>Jean-Baptiste Jourdan. ''A Memoir of the Operations of the Army of the Danube under the Command of General Jourdan, Taken from the Manuscripts of that Officer.'' London: Debrett, 1799, p. 94.</ref>

At the [[First Battle of Zurich]], Lariboisière's artillery defended the Zurich heights, which gave [[Andre Massena]]'s army enough time to evacuate the city and take position on the opposite side of the [[Limmat river]]. Afterward, Lariboisière was sent to northern Italy where he commanded the advance guard division; [[Jean Victor Moreau]]'s right wing, with Lariboisière's infantry and artillery, crossed the [[Ticino river]] at [[Pavia]], marched up the left bank of the [[Po River]] and took position beneath [[Alessandria]]; there, he protected Moreau's flank from Russian skirmishers sufficiently for Moreau to establish a large train of artillery in [[Turin]] and to strengthen the French positions between the Po and the [[Tanaro]] river.<ref>Ramsay Weston Phipps. ''The Armies of the First French Republic and the Rise of the Marshals of Napoleon I: The Armies on the Rhine, in Switzerland, Holland, Italy, Egypt, and the coup d'état of Brumaire 1797&ndash;1799''. Westport, Connecticut: Greenwood Press, [1939] 1980, vol. 5, p. 265.</ref>

At [[Battle of Novi]], his division was part of [[Laurent, Marquis de Gouvion Saint-Cyr|Laurent Saint-Cyr]]'s right wing.<ref>Phipps, vol. 5, p. 315.</ref> At the battle itself, his troops were the only ones properly positioned; the rest of the French army was otherwise out of place and unready for battle. He and Saint-Cyr held the center of the French line, beating off two Russian assaults.<ref>Phipps, vol. 5, p. 323.</ref> After the French defeat at Novi, he went with Saint-Cyr to Savona,<ref>Phipps, vol. 5, p. 333.</ref> where he participated in operations around [[Genoa]]. Eventually, he rejoined Moreau to campaign in southwestern Germany, culminating in the [[Battle of Hohenlinden]].<ref name=revue>''Revue critique d'histoire et de littérature''. Paris, E. Leroux. 18. p. 509.</ref>

===Napoleonic Wars===
Baston de Lariboisière commanded the artillery of the French IV Corps for the 1805 campaign of the [[War of the Third Coalition]]. He directed the investment of [[Ulm]] prior to its capitulation,<ref>{{fr icon}} Faculté des lettres de Rennes. "Discours, pron. a l'inauguration de la Statue du General Lariboisière." ''Annales de Bretagne'', Rennes: Plihon, [ -1973], Volume 8, p. 735&ndash;742.</ref> and contributed to the success of [[Battle of Austerlitz|Austerlitz]]. There he commanded the artillery attached to [[Soult]] corps.<ref name=revue/> By firing on the ice of the lakes over which the Russian columns were retreating, Baston de Lariboisière's artillery fire converted a Russian retreat into a full-scale [[rout]]:<ref name="Mullié"/> in the bombardment, French artillery pounded the soldiers and broke the ice.  The men drowned in the cold ponds, dozens of Russian artillery pieces going down along with them.<ref>David G. Chandler, ''The Campaigns of Napoleon.'' New York: Simon & Schuster, 1995. ISBN 0-02-523660-1, p. 432</ref>

In the [[Battle of Jena-Auerstadt]], Baston de Lariboisière successfully repelled several infantry charges with artillery fire.  Subsequently, Napoleon raised him to major general, and appointed him to command the artillery of the [[Imperial Guard]] at [[Battle of Eylau]], in February 1807. Baston de Lariboisière remained throughout the day-long battle with a battery of 40 guns at the French center.<ref name="Mullié"/>  After the campaigns of 1806, Baston de Lariboisière briefly served as governor of Hanover.<ref name=revue/> He later replaced General [[Nicolas-Marie Songis des Courbons]] prior to the engagements in northeastern Prussia in 1807.<ref>Paul Dawson. "French Artillery in 1807." ''Military Subjects: Organization, Strategy and Tactics.'' [http://www.napoleon-series.org Napoleon Series]. Robert Burnham, Editor in Chief. August 2004. Accessed 17 May 2010.</ref> At the [[Siege of Danzig (1807)|Battle of Danzig]], although wounded by a sniper's bullet, he continued to direct the siege of the city.<ref name=Broughton/> He directed the Imperial guard artillery for the battles of [[Battle of Heilsberg|Heilsberg]] and [[Battle of Friedland|Friedland]]; after Friedland, Baston de Lariboisière organized the security of the raft on the [[Niemen river]], where Napoleon and [[Alexander II of Russia]] conferred on the terms of the [[Peace of Tilsit]].<ref name="Mullié"/>

In February 1808, General Lariboisière took command of the artillery of the [[Army of Spain]]. Recalled to the Grand Army in 1809, Napoleon gave the command of the artillery at the [[Battle of Wagram]].<ref name="Mullié"/>  In 1811 Napoleon raised him to the post of first inspector general of ordnance&mdash;weaponry and ammunition.<ref name="Mullié"/>

==Russian campaign, 1812==
[[File:Gros-General Lariboisière and his son.jpg|thumb|General Lariboisière and his son [[:de:Ferdinand Baston de La Riboisière|''Ferdinand'']], a lieutenant in the 1st company of the 1st squadron in the [[Carabiniers-à-Cheval|1st Carabiniers-à-Cheval regiment]] at the [[battle of Borodino]]. Ferdinand salutes his father before charging with his regiment; he would be mortally wounded during the charge. Painting by [[Antoine-Jean Gros]].]]
In planning the Russian campaign, 1812, Lariboisière immediately foresaw difficulties in transporting the requisite amount of artillery and ammunition the vast distance from the Nieman river into Russia. The army left Prussia with over 1100 artillery pieces of various kinds and sizes.<ref>Richard K Riehn. ''1812 Napoleon's Russian Campaign.'' New York: Wiley 1991, p. 159.</ref> Despite heavy rain, the French arrived in [[Vilnius]]; despite the mud, the artillery was established prior to the battle and contributed with successful targeted fire.<ref name="Mullié"/>

{| class="toccolours" style="float: left; margin-left:  1em; margin-right:  2em; font-size:  85%; background:#c6dbf7; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" | 
'''Honors and Promotions'''
*''Chef-de-Brigade'' 6 November 1796
*''Commander and colonel of Artillery of the Imperial Guard 3 January 1807
*General of Brigade 29 August 1803
*General of Division 3 January 1807
*Grand Officer of the ''Légion d'honneur'' 4 June 1807
*Count of the Empire'' 26 October 1808
|}
On the evening before the [[Battle of Borodino]], Baston de Lariboisière scouted the Russian positions to determine the means of bombarding the redoubts that the Russians had established on their left. During the night, he strategically placed the French artillery park; at daybreak, the artillery bombarded the Russian positions, firing over 70,000 rounds. One of Lariboisière's sons ([[:de:Ferdinand Baston de La Riboisière|''Ferdinand'']]), was fatally wounded in a charge on the Russian lines at Borodino. Grief-stricken and exhausted with fatigue, the general fell sick in [[Vilnius]] and died in [[Königsberg]] on 21 December 1812.<ref name="Mullié"/>

==Legacy==

His body rests in the church of ''[[les Invalides]]'', and on his coffin is the following inscription:<ref name="Mullié"/>

<blockquote>''Ambroise Baston, Count of La Riboisière, major general, commanding the artillery of the [[Grande Armée]], Grand Officer of the Legion of Honor, born in Fougères, died at Königsberg, December 21, 1812.''<ref name="Mullié"/></blockquote>

The General's heart is hosted in a private chapel near his castle of Monthorin at Louvigné-du-Désert, in Brittany.<ref>[http://www.napoleon-empire.com/personalities/lariboisiere.php Lariboisière's short biography in Napoleon & Empire website, displaying photographs of the castle and the private chapel]</ref>

[[:fr:Honoré-Charles Baston de La Riboisière|''Honoré-Charles Baston de Lariboisière'']], another of his sons, also served in the artillery. He survived not only the Battle of Borodino in which his brother ([[:de:Ferdinand Baston de La Riboisière|''Ferdinand'']]) was killed, but also the retreat from Moscow and the subsequent campaigns in Prussia and Saxony. He served in the [[Chamber of Deputies]] and was raised to the peerage of France.<ref>{{fr icon}} Germaine Sarrut. "Honore Baston Comte de Lariboisière." ''Biographie des hommes du jour, industriels,--conseillers-d'État.'' Paris, H. Krabe, 1835–41, p. 43&ndash;44.</ref> He founded the [[Hôpital Lariboisière]].<ref>{{fr icon}} ''Discours'',  p. 735.</ref>

==Sources==

===Notes and citations===
{{Reflist|2}}

===Bibliography===

*Broughton, Tony. "The ''Garde Imperiale'' and Its Commanders during the Period 1804&ndash;1815: Artillery."  ''Military Subjects: Organization, Tactics and Strategy.'' [http://www.napoleon-series.org Napoleon Series]. Robert Burnham, Editor in Chief, March 2003. Accessed 17 May 2010.
*Chandler, David. ''The Campaigns of Napoleon.'' New York: Simon & Schuster, 1995. ISBN 0-02-523660-1.
*Dawson, Paul. "French Artillery in 1807." ''Military Subjects: Organization, Strategy and Tactics.'' [http://www.napoleon-series.org Napoleon Series]. Robert Burnham, Editor in Chief. August 2004. Accessed 17 May 2010.
*{{fr icon}} Faculté des lettres de Rennes. "Discours, pron. a l'inauguration de la Statue du General Lariboisière. (1892)" ''Annales de Bretagne'', Rennes: Plihon, [ -1973], Volume 8.
* Jourdan, Jean-Baptiste. ''A Memoir of the Operations of the Army of the Danube under the Command of General Jourdan, Taken from the Manuscripts of that Officer.'' London: Debrett, 1799.
*{{fr icon}} Mullié, Charles. "Jean Ambroise Baston de Lariboisière." ''Biographie des célébrités militaires des armées de terre et de mer de 1789 à 1850.'' Paris, 1852.
* Phipps, Ramsey Weston. ''Armies of the French Republic'', Westport CT: Greenwood Press, [1939] 1989, volume 5.
*{{fr icon}} Sarrut, Germaine. "Honore Baston Comte de Lariboisière." ''Biographie des hommes du jour, industriels,--conseillers-d'État.'' Paris, H. Krabe, 1835–41, p.&nbsp;43&ndash;44.

==External links==
{{Commons category-inline|Jean Ambroise Baston de Lariboisière}}


{{Good article}}

{{Authority control}}
{{DEFAULTSORT:Baston De Lariboisiere, Jean Ambroise}}
[[Category:French military personnel of the French Revolutionary Wars]]
[[Category:French commanders of the Napoleonic Wars]]
[[Category:Counts of the First French Empire]]
[[Category:1759 births]]
[[Category:People from Fougères]]
[[Category:1812 deaths]]