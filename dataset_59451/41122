{{Infobox mountain
| name = Yandang Mountains
| other_name = {{lang|zh|雁荡山}}
| photo = 
| photo_caption = 
| elevation_m = 1108
| elevation_ref = <ref name="Mount Yandang">[http://www.nationalparkofchina.com/yandangshan.html "Mount Yandang"] on NationalParkOfChina.com. Retrieved 2012-02-20.</ref>
| prominence_m =
| prominence_ref =
| map = China
| map_caption = Location in China
| label_position = bottom
| listing = 
| location = [[Zhejiang]] {{flag|China}}
| range = 
| lat_d = 28.37 |lat_NS = N
| long_d = 121.06 | long_EW = E
| coordinates_ref =
| topo = 
| type = 
| easiest_route = 
}}
{{Contains Chinese text}}

'''Yandang Mountains''' or '''Yandangshan''' ([[Chinese language|Chinese]]:&nbsp;<small>[[traditional characters|t]]</small>&nbsp;{{lang|zh|{{linktext|雁蕩山}}}}, <small>[[simplified characters|s]]</small>&nbsp;{{lang|zh|{{linktext|雁荡山}}}}, <small>[[pinyin|p]]</small>&nbsp;''Yàndàng Shān'', <small>lit.</small>&nbsp;"Wild Goose Pond Mountain(s)") refers, in the broad sense, to a coastal [[mountain range]] in southeastern [[Zhejiang]] [[Province of China|province]] in eastern [[China]], covering much of the [[prefecture-level city]] of [[Wenzhou]] (from [[Pingyang County]] in the south to [[Yueqing|Yueqing County]] in the northeast) and extending to the [[county-level city]] of [[Wenling]] in [[Taizhou, Zhejiang|Taizhou]] prefecture. The mountain range is divided in two by the [[Oujiang River]], the two parts being the North Yandang and South Yandang. More narrowly, Yandangshan is also used more narrowly to refer to {{nowrap|'''Mount Yandang'''}}, a specific part of the North Yandang around an ancient caldera near a small town of the same name ({{lang|zh|{{linktext|雁荡|镇}}}}, ''Yàndàng Zhèn''). The highest peaks of North Yandang are located here, and this is also the main tourist spot. In this article, name "Yandang Mountains" is used to refer the mountain range and "Mt. Yandang" to refer to the caldera.

The main peak of North Yandang, Baigangjian ({{lang|zh|{{linktext|百|岗|尖}}}}, ''Bǎigǎng Jiān'', <small>lit.</small>&nbsp;"Hundred-Peak Point"), rises {{convert|1150|m|ft|abbr=on|sp=us}} above sea level<ref name=wenzhouFAO>[http://www.wzfao.gov.cn/en/index.jsp?id0=z1b6tuus&id1=z1b76r0v&sid=z1fv0s9v Scenic Wenzhou], Foreign Affairs Office of Wenzhou City</ref>{{refn|Other sources give an altitude of {{convert|1056|m|sp=us}}<ref>[http://www.foreignercn.com/index.php?option=com_content&view=article&id=5332:yandang-mountain&catid=68:travel-in-zhejiang&Itemid=140 Yandang Mountain], www.foreignerCN.com</ref> or {{convert|1108|m|sp=us}}<ref name="Mount Yandang"/>).}} There is a radar station on the peak, which is closed to the public.

In 2004, Yandangshan became National Geological Park and in the beginning of 2005, a member of [[geopark|Global Geoparks Network]], with total area of {{convert|450|sqkm|sp=us|abbr=on}}.<ref name=wenzhouFAO/> The Yandangshan National Forest Park has an area of {{convert|841|ha|sp=us}}, covering Mt. Yandang.<ref>[http://www.wzslly.com/main/gyview.asp?id0=1&id1=11&id=26 Yandangshan National Forest Park] Forestry Bureau of Wenzhou City (in Chinese)</ref>

Mt. Yandang is known for its natural beauty, arising from its many vertical rock faces and pinnacles, mountain slopes with lush forests and bamboo groves, streams with clear water, waterfalls and caves. The area also hosts numerous temples and shrines, many of them with a long history; characteristic for the temples is that many of them are built inside caves or in mouths of caves.
[[File:Yandang Mountain.jpg|thumb|[[Qing Dynasty]] view Yandangshan.]]

==Environment==
Yandang Mountains were formed through volcanic activity during the [[Cretaceous]] period {{circa|lk=no|100}}-120  million years (Ma) ago.<ref name=unesco>[http://whc.unesco.org/en/tentativelists/1627/ Yandang Mountain], UNESCO World Heritage Centre, Tentative Lists</ref> Similar igneous rocks are widespread in SE China, forming a ~{{convert|400|km|mi|abbr=on}} wide and ~{{convert|2000|km|mi|abbr=on}} long belt of volcanic-intrusive complexes.<ref name=He_etal2009>{{Cite journal | last1 = He | first1 = Z. | last2 = Xu | first2 = X. | last3 = Yu | first3 = Y. | last4 = Zou | first4 = H. | title = Origin of the Late Cretaceous syenite from Yandangshan, SE China, constrained by zircon U-Pb and Hf isotopes and geochemical data | doi = 10.1080/00206810902837222 | journal = International Geology Review | volume = 51 | issue = 6 | pages = 556 | year = 2009 | pmid =  | pmc = }}</ref>

Yandangshan [[caldera]] is a round [[volcanic]]‐[[Intrusion|intrusive]] complex with a diameter of {{convert|13|sqkm|sp=us}}, of which the north-east part was destroyed by a later regional fault.<ref name=He_etal2009/> Yandangshan volcano erupted in four episodes. The mountain consists of various [[igneous rock]]s: [[ignimbrite]], [[rhyolite]], [[syenite]] and [[tuff]].<ref>[http://en.cnki.com.cn/Article_en/CJFDTOTAL-ZJDZ199701003.htm Petrogenetic sources and tectonic characteristics of Yandangshan caldera, Zhejiang Province] Feng, C.G., Yu, Y.W., and Dong, Y.H., 1997, Geology of Zhejiang 13:18–25 (in Chinese with English abstract)</ref> The mountain shows often conspicuous layering corresponding to the four different episodes of eruptions, giving rise to terrace-like structures where flatter, forested areas are separated by vertical cliffs.

Mt. Yandang is mostly covered by deciduous and evergreen forests, mixed with some ''[[Cunninghamia]]''. Drier ridges can be dominated by small [[pine]] trees. There are also some bamboo grooves. Lower slopes and valleys are used for agricultural purposes, for example for growing tea, or as fruit gardens. Herbs and mushrooms collected from the mountain are sold locally.

A number of plant and animal species have been named after Mt. Yandang. A species of sedge, ''[[Carex]] yandangshanica'', has been described from Mt. Yandang and two other mountainous locations in [[Zhejiang]].<ref>{{Cite journal | last1 = Jin | first1 = X. F. | last2 = Zheng | first2 = C. Z. | doi = 10.1111/j.1756-1051.2010.00817.x | title = ''Carex yandangshanica'' sp. nov. (Cyperaceae; C. Sect. Rhomboidales) from Zhejiang, China | journal = Nordic Journal of Botany | volume = 28 | issue = 6 | pages = 709 | year = 2010 | pmid =  | pmc = }}</ref> Mt. Yandang has also given its name to ''[[Cyclosorus]] yandangensis'', a marsh fern, and ''[[Arachniodes]] yandangshanensis'', a holly fern.<ref>[http://data.sp2000.cn/ Catalogue of life] The Biodiversity Committee of Chinese Academy of Sciences (eds.), 2010, Catalogue of Life China: 2010 Annual Checklist China. Species 2000 China Node, Beijing, China</ref> Among animal species, a mite ''Eustigmaeus yandangensis'' has been named after Mt. Yandang, but this has been challenged.<ref>{{cite journal |last1=Cheng |first1=Hong |last2=Fan |first2=Qing-Hai |year=2008 |title=A catalogue of the Chinese Raphignathoidea (Acari: Prostigmata) |journal=Systematic and Applied Acarology |volume=13 |pages=256–278}}</ref>

Yandang Mountains are everywhere influenced by human activity; there is an extensive network of official tourist paths, clearings for fire-prevention, and unofficial paths used by the local people for collecting resources from the mountains; nevertheless, because of the steep terrain, some parts are difficult to access. Whatever wildlife survives tends to be very elusive. Notable inhabitants of Yandang Mountains include [[Cabot's tragopan]],<ref>{{Cite journal | last1 = Dong | first1 = L. | last2 = Niu | first2 = W. | last3 = Zhou | first3 = Z. | last4 = Hsu | first4 = Y. C. | last5 = Sun | first5 = Y. | last6 = Lloyd | first6 = H. | last7 = Zhang | first7 = Y. | doi = 10.5122/cbirds.2011.0010 | title = Assessing the genetic integrity of captive and wild populations for reintroduction programs: The case of Cabot's Tragopan in China | journal = Chinese Birds | volume = 2 | issue = 2 | pages = 65 | year = 2011 | pmid =  | pmc = }}</ref>  a nationally-protected species in China.<ref>[http://www.birdlife.org/datazone/speciesfactsheet.php?id=241 BirdLife International (2012) Species factsheet: ''Tragopan caboti'']</ref> [[Chinese pangolin]] is also reported to live in Yandang Mountains.<ref>[http://www.ydspark.com/intr.asp?sortid=157 http://www.ydspark.com/](in Chinese)</ref>

Yandang Mountains have been considered as one of the areas where [[South China tiger]] might still persist, but there is no positive evidence and the species is considered functionally extinct in the wild.<ref>[http://chinese.savechinastigers.org/node/275 Is the South China tiger extinct in the wild?] Save China's Tigers, accessed 21 Feb 2012</ref>

==Cultural significance==
[[File:Lingfeng.JPG|thumb|Lingfeng Peak. The big crack is the [[Guanyin]] Cave, with the lower gate of the Guanyin Temple just visible.]]

Yandang Mountains are dotted with temples and shrines, many of them with long history.<ref name=yandanshan>{{cite book |title=Yandang Mountain |last=Zhang |first=Guojian |author2=Huawu Zhang  |year=2002 |publisher=China Travel & Tourism Press |location=Beijing |isbn=7-5032-1989-0 |pages=96 }}</ref> Most of them were destroyed during the [[Cultural Revolution]], but many have been rebuilt.

'''Guanyin Temple''' (or Avalokitesvara Temple) is a Buddhist temple located on Mt. Yandang, in the Lingfeng Peaks scenic area. The temple lies between two rock peaks leaning against each other such that a big crack-like cave is formed; the peaks are together known as the "Holding Palms Peak". The cave is 100 metres high, with the cave bottom sloping steeply up. The temple follows this natural structure and has nine levels (floors), with the [[Guanyin]] shrine occupying the highest, most hidden section of the cave. The temple was first build in 265.<ref name=sacred>{{cite book |title=China's Sacred Sites |last=Nan |first=Shunxun |author2=Beverly Foit-Albert  |year=2007 |publisher=Himalayan Institute Press |location=Honesdale, PA |isbn=978-0-89389-262-3 |pages=259 |url=https://books.google.com/books?id=TeZg4Gy2gOAC}}</ref>

==Tourism==

===North Yandang===
[[File:Lingyan.jpg|thumb|A view from the Lingyan Rock.]]
[[File:Dalongqiu.jpg|thumb|Great Dragon Pond Waterfall.]]
North Yandangshan, in the geographic sense, contains three scenic areas, Yangjiaodong/Fangshan, Middle Yandangshan and North Yandang, the latter commonly referred to as the Yandangshan, the most famous of the scenic areas in the Yandang Mountains.

'''North Yandang Scenic Area''' is centered on Mt. Yandang and has an area of {{convert|450|sqkm|sp=us|abbr=on}}.<ref>[http://www.wzta.gov.cn/lyzn/fjmc_sy.aspx?unitid=3620 Yueqing Yandang] Wenzhou Municipal Bureau of Tourism (in Chinese)</ref> China’s National Tourism Administration rated North Yandang as a "[[5A scenic area]]" in 2007, up from "4A" before.<ref name=wenzhouFAO/> A number of touristic attractions, or "scenic spots", have been develop around Mt. Yandang, the most famous being:
* Lingfeng Peaks ({{zh|s={{linktext|灵|峰}}|l=''Spiritual peaks''}}).
* Lingyan Rock ({{zh|s={{linktext|灵|岩}}|l=''Spiritual rock''}}).
* Great Dragon Pond Waterfall ({{zh|s={{linktext|大|龙|湫}}|p={{linktext|dà|lóng|qiū}}|l=''Big dragon pond''}}).

The great majority of domestic tourists only visit the key scenic areas, which can get very crowded, and not all development has been kind to the surroundings. However, areas outside the formally developed scenic spots can be quite serene. Mt. Yandang is also increasingly popular as a hiking area for urbanites.

Mt. Yandang can be accessed using the [[Yandangshan Railway Station]] on the fast railway track between [[Ningbo]] and [[Wenzhou]].

'''Middle Yandang Scenic Area''' is a "[[4A scenic area]]" in south of Yueqing County.<ref>[http://www.wzta.gov.cn/lyzn/fjmc_sy.aspx?unitid=4815 Middle Yandang Scenic Area] Wenzhou Municipal Bureau of Tourism (in Chinese)</ref> The total area of the scenic area is 93&nbsp;km<sup>2</sup>.

'''Yangjiaodong/Fangshan Scenic Area''' is on the border between Wenzhou and Taizhou. This is a single mountain, but the name of the scenic area changes on the border. Yangjiaodong ({{zh|s=羊角洞|Sheep horn cave}}) is the Wenzhou half, and Fangshan ({{zh|s=方山|Square mountain}}) the Taizhou one.<ref>[http://www.wl.gov.cn/lypd/lyzn/xxyl/20110303/160168_1.htm Wenling Travel Guide] Wenling Municipal People's Government (in Chinese)</ref>

===South Yandang===
South Yandang is a "4A scenic area" and consists of five scenic spots near Nanyanzhen town in [[Pingyang County]].<ref>[http://www.wzta.gov.cn/lyzn/fjmc_sy.aspx?unitid=4817 Pingyang South Yandang Scenic Area] Wenzhou Municipal Bureau of Tourism (in Chinese)</ref> East and West Caves are the most famous sights. The total area of the scenic area is 169&nbsp;km<sup>2</sup>.

== References ==
{{Reflist}}

== External links ==
* [http://www.nationalparkofchina.com/yandangshan.html www.nationalparkofchina.com]
* [http://www.frommers.com/destinations/yandangshan/ Frommer's: Yandangshan]
* [http://www.yds-en.com/ Wenzhou Yandang Mountain Scenery Tourism Administration Bureau] Welcome to Mt. Yandangshan
* [http://www.globalgeopark.org/english/AboutGGN/Memberslist/201009/t20100913_764316.htm Yandangshan Geopark] at the [http://www.globalgeopark.org Global Geoparks Network]

<!--- Categories --->
[[Category:Mountain ranges of China]]
[[Category:Landforms of Zhejiang]]
[[Category:Tourist attractions in Zhejiang]]
[[Category:AAAAA-rated tourist attractions]]