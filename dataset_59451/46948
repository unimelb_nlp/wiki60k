{{Infobox person
| name   = M. R. Chandrasekharan <br>എം. ആർ. ചന്ദ്രശേഖരൻ
| image  = MRCProfile.jpg
| birth_date  = {{Birth date and age|df=yes|1929|2|26}}
| birth_place = [[Pottore]], [[Thrissur]]
| residence   = [[Palghat]], Kerala, India
| nationality = [[Indian people|Indian]]
| ethnicity   = [[Malayalam|Malayali]]
| citizenship    = India
| education      = [[Master of Arts|Master's]]
| alma_mater     = *Lower Secondary School, Tirur
*	Vivekodayam High School, Thrissur 
*	[[Sree Kerala Varma College]] Thrissur
*	[[Madras University]] 
*	[[Kerala University]]
| occupation     =  Literary Critic, Professor , Editor
| title          = * Professor
| spouse         = Vappalakalathil Vijayakumari
| parents        = Malappurath Raman Vaidyar, K.S. Devaki
| children       = Ramkumar, Priya
}}

'''Malappurath Raman Chandrasekharan''' ({{lang-ml| മലപ്പുറത്ത്  രാമൻ ചന്ദ്രശേഖരൻ}}; born 26 Feb 1929), popularly known as '''M.R. Chandrasekharan''' ({{lang-ml|എം. ആർ. ചന്ദ്രശേഖരൻ}}) or simply ''' M. R. C.''' ({{lang-ml|എം.ആര്‍.സി.}}), is a [[Malayalam]] literary critic and author from [[Kerala]], India. Chandrasekharan has published more than 40 books in different literary sections like literary criticism, translations, politics, social etc. He is also noted for his work in the field of [[journalism]] and [[education]]. He won the 2010 [[Kerala Sahitya Akademi Award for Literary Criticism]].

==Biography==

Chandrasekharan was born to Malappurath Raman Vaidyar and K. S. Devaki on 26 Feb 1929 at [[Pottore]], [[Thrissur district|Thrissur]] ([[Trichur]]) in [[Kerala]]. His childhood days were spent in the village where he attended the Tirur Lower Secondary School and Vivekodayam High School, [[Thrissur]].  He took his [[Bachelor's degree]] from [[Sree Kerala Varma College]], [[Thrissur]], BOL from [[University of Madras]] and later the master's degree from [[Kerala University]] in Malayalam language and literature.

Chandrasekharan started his career as a [[journalist]] in Navajeevan edited by [[Joseph Mundassery]] for the [[CPI]]. Subsequently, he joined as a sub-editor in [[Mathrubhumi]] daily in [[Kozhikode]], but was sent out for his [[communist]] leaning.

He worked as a school teacher at Kodakara National High School and Bekal Govt. High School. He joined as a lecturer at [[Malabar Christian College]], [[Kozhikode]],<ref>{{cite web|url=http://mcccalicut.org/page/217/malayalam.html|title=Malabar Christian College|publisher=}}</ref> in 1956 and moved to Payyannur College, [[Kannur]] as senior lecturer in 1965. He retired from Payyannur College, [[Kannur]] after over 30 years of teaching service in the year 1989. After retirement he has also worked as Malayalam Professor in [[Sree Sankaracharya University of Sanskrit]].

He is now settled at [[Ananganadi]] P.O.Panamanna, [[Ottappalam]] in [[Palghat]] district, [[Kerala]]. He is active with his writings and also enjoys the challenge of [[organic farming]]. His wife Vappalakalathil Vijayakumari died in December 2013. His son Ramkumar C and daughter Priya Shankar are both engineering degree holders.

==Positions Held==
Chandrasekharan was very much involved in The All Kerala Private College Teachers’ Association(AKPCTA<ref>{{cite web|url=http://akpcta.in/|title=AKPCTA Official Website - Official Website|publisher=}}</ref>) and served as College Committee Secretary, Regional Secretary, Regional President, General Secretary and President of the association. As association representative, he was elected to the [[Calicut University]] Senate and Syndicate.

He was a member of [[Kerala Sahithya Academy]] General Council and its Executive Committee.<ref>{{cite web|url=http://www.keralasahityaakademi.org/sp/Writers/ksa/History/Html/Admin.htm|title=Administration of the Akademi|publisher=}}</ref>

Chandrasekharan was one of the Directors of the Calicut City Service Co-Operative Bank<ref>{{cite web|url=http://calicutcitybank.com/|title=Calicut City Service Co-Operative Bank Ltd.|publisher=}}</ref> from the year 2006 till 2013

Chandrasekharan is member of the Academic Council of [[Thunchath Ezhuthachan Malayalam University]]<ref>{{cite web|url=http://malayalamuniversity.edu.in/|title=Malayalam Sarvakalasala|publisher=}}</ref> established in 2012<ref>{{cite web|url=http://malayalamuniversity.edu.in/authorities/the-academic-council/|title=The Academic Council - Malayalam Sarvakalasala|publisher=}}</ref>

In 1961,when Kerala Sahithya Samiti was formed with Kuttipuzha Krishna Pillai as President, [[S. K. Pottekkatt]] as Vice President, [[N. V. Krishna Warrier]] as General Secretary in [[Kozhikode]], MRC and [[Vayalar Ramavarma]] were named secretaries. MRC later served as General Secretary and President of the organisation.

He was also involved in the formation of the Deshabhimani Study Circle, sited as a vigorous and widespread Literary Movement in Kerala along with [[E. M. S. Namboodiripad]], M.S.Devadas, and P.Govindapillai in 1969<ref>{{cite journal|jstor=3516305|title=Deshabhimani Study Circles: Literary Movement in Kerala|first=P. Govinda|last=Pillai|date=1 January 1975|publisher=|journal=Social Scientist|volume=4|issue=2|pages=56–60|via=JSTOR|doi=10.2307/3516305}}</ref>

He published the literary critical journal "Sahithya Samithi Masika" from [[Payyanur]] during 1976-80.

In 1969 when he was the Research Officer at [[Kerala Bhasha Institute]], he worked as the Executive Editor of the magazine "Vignana Kairali" ({{lang-ml|വിജ്ഞാനകൈരളി}} ).

After retirement in 1989, he took-up the editorship of a magazine called "Chindana" ({{lang-ml|ചിന്തന}} ) published from [[Kannur]].

He was the Editor of CMP's weekly journal 'Malayala Mannu" (Malayalam:മലയാളമണ്ണ് ). He served the journal from 1990 till 2013

== Awards and honours ==
Chandrasekharan has won the 2010 [[Kerala Sahitya Akademi Award for Literary Criticism]] for his book "''Malayalam Novel Innum Innaleyum''"<ref>[[Kerala Sahitya Akademi Award for Literary Criticism]] [http://www.thehindu.com/news/national/kerala/sahitya-akademi-fellowships-awards-presented/article1451050.ece]
[http://ojnewscom.com/index.php?page=newsDetail&id=4392]
[http://www.mathrubhumi.com/english/news/books/kerala-sahitya-akademi-awards-announced-103042.html]</ref>

He won Dr. C.P.Menon Award for The study of the Progressive School in Literature for his book ''Keralathile purogamana sahitya prastanathinte charithram'' for the year 2005

He has also won the [[M. N. Sathyaardhi]] Award in recognition of his contributions to Malayalam literature as a translator.The award is instituted by the M.N. Sathyarthi Trust in memory of the writer who is remembered for his translations of many popular Indian-language works into Malayalam.

== Bibliography ==

=== Literary Criticism ===

{|cellpadding="2" cellspacing="0" border="1" style="border-collapse: collapse; border: 2px #DEE8F1 solid; font-weight: bold; font-family: verdana"
|<center>List of Literary Criticism by M. R. C.</center>
{| cellpadding="2" cellspacing="0"  border="1" style="background-color:#FFFFFF; border-collapse: collapse; border: 1px #DEE8F1 solid; font-weight: normal; font-family: verdana"
! style="background-color:#B0BFD5; color:white"|#
! style="background-color:#B0BFD5; color:white"|Name
! style="background-color:#B0BFD5; color:white"|Translation in English
! style="background-color:#B0BFD5; color:white"|Year of publishing, Publisher
|----

! align = "left"|  1
|align = "left"| Laghuniroopanangal <br> {{lang-ml|ലഘുനിരൂപണങ്ങള്‍ }}	 
|align = "left"| Light Critical Works
|align = "left"| 1964, P.K.Bros Calicut

|-
! align = "left"|  2
|align = "left"| Niroopakante Raajyabaaram <br>{{lang-ml|നിരൂപകന്‍റെ രാജ്യഭാരം }}
|align = "left"| The Critic's Sway 
|align = "left"| 1965, Current Books Thrissur
|-
! align = "left"|  3
|align = "left"| Sathyavum Kavithayum <br>{{lang-ml|സത്യവും കവിതയും }}
|align = "left"| Reality and Poetry  
|align = "left"| 1979, Poorna Publications Calicut
|-
! align = "left"|  4
|align = "left"| Gopuram <br>{{lang-ml|ഗോപുരം }}
|align = "left"| The Tower  
|align = "left"| 1989, Current Books Thrissur
|-
! align = "left"|  5
|align = "left"| Grandhapooja <br>{{lang-ml|ഗ്രന്ഥപൂജ }}
|align = "left"| Worshiping Books 
|align = "left"| 1996, S.P.C.S.Ltd Kottayam
|-
! align = "left"|  6
|align = "left"| Communist Kavithrayam  <br> {{lang-ml|കമ്മ്യൂണിസ്റ്റ് കവിത്രയം}}<ref>{{cite web|url=http://justbooksclc.com/titles/308173-communist-kavithrayam|title=JustBooksCLC|publisher=}}</ref>
|align = "left"| Three Communist Poets - [[P. Bhaskaran]], [[Vayalar Ramavarma]] , [[O.N.V. Kurup]]
|align = "left"| 1998, Current Books Thrissur
|-
! align = "left"|  7
|align = "left"| Malayala Sahityam Swaathanthrya Labdikku Shesham <br> {{lang-ml|മലയാളസാഹിത്യം സ്വാതന്ത്ര്യലബ്ധിക്കുശേഷം }}
|align = "left"| Malayalam Literature after the attainment of Independence  
|align = "left"| 1999, S.P.C.S.Ltd Kottayam
|-
! align = "left"|  8
|align = "left"| Ezhuthile Ponnu <br> {{lang-ml|എഴുത്തിലെ പൊന്ന് }}
|align = "left"| The Golden Writing  
|align = "left"| 2001,Prabhat Book House Trivandrum
|-
! align = "left"|  9
|align = "left"| Maneeshikal <br>  {{lang-ml|മനീഷികള്‍ }}
|align = "left"| The visionaries among writers  
|align = "left"| 2005, Lipi Book Publishers Calicut 
|-
! align = "left"|  10
|align = "left"| Malayalam Novel Innum Innaleyum  <br> {{lang-ml|മലയാളനോവല്‍ ഇന്നും ഇന്നലെയും }}
|align = "left"| Malayalam Novel - Today and Yesterday  
|align = "left"| 2007, Samayam Books Kannur
|-
! align = "left"|  11
|align = "left"| Mundassery - Sahithya Vimarshanathinte Prathaapa Kaalam <br> {{lang-ml|മുണ്ടശ്ശേരി - സാഹിത്യവിമര്‍ശനത്തിന്‍റെ പ്രതാപകാലം }}<ref>http://www.amazon.in/Joseph-Mundassery-Vimarshanathinte-M-R-Chandrasekharan/dp/B00DCQIYOS</ref><ref>https://www.booknbooks.com/joseph-mundassery-vimarshanathinte%20Prathapakalam-mr-chandrasekharan</ref>
|align = "left"| Mundasseri - Golden Years of Literary Criticism  
|align = "left"| 2008, Green Books Thrissur
|-
! align = "left"|  12
|align = "left"| Krishnagadha Vimarshanam - chila thiruthalukal <br> {{lang-ml|കൃഷ്ണഗാഥാ വിമര്‍ശനം - ചില തിരുത്തലുകള്‍ }}
|align = "left"| Criticism of Krishnagatha- Some Corrections  
|align = "left"| 2008, Poorna Books Calicut
|-
! align = "left"|  13
|align = "left"| Adyathma Ramayanam Vimarshanam <br> {{lang-ml|അദ്ധ്യാത്മരാമായണം വിമര്‍ശനം }}
|align = "left"| Criticism of Ramayana, The Epic  
|align = "left"| 2008, Samayam Books Kannur
|-
! align = "left"|  14
|align = "left"| Shishirathile Pookkal  <br> {{lang-ml|ശിശിരത്തിലെ പൂക്കള്‍ }}
|align = "left"| Flowers in the Winter  
|align = "left"| 2010, S.P.C.S.Ltd Kottayam
|-
! align = "left"|  15
|align = "left"| Karamullum Kaattupookkalude Karachilum <br> {{lang-ml|കാരമുള്ളും കാട്ടുപൂക്കളുടെ കരച്ചിലും }}
|align = "left"| The Thorns and Cries of Wild flowers
|align = "left"| 2012, Kerala Sahithya Academy Thrissur
|-
! align = "left"|  16
|align = "left"| Theechoolayil Venthurukaathe <br> {{lang-ml|തീച്ചൂളയില്‍ വെന്തുരുകാതെ }}
|align = "left"| Life inside a Furnace
|align = "left"| 2012, S.P.C.S.Ltd Kottayam
|-
! align = "left"|  17
|align = "left"| Keralathile purogamana sahitya prastanathinte charithram - Sahithya prastana padanam <br> {{lang-ml|കേരളത്തിലെ പുരോഗമനസാഹിത്യപ്രസ്ഥാനത്തിന്‍റെ ചരിത്രം - സാഹിത്യപ്രസ്ഥാനപഠനം }}
|align = "left"| History of Forward Thinking Literary Movement in Kerala
|align = "left"| 1996, Olive Books Calicut
|-

|}
|}

=== Political Thinking ===

{|cellpadding="2" cellspacing="0" border="1" style="border-collapse: collapse; border: 2px #DEE8F1 solid; font-weight: bold; font-family: verdana"
|<center>List of Political Monographs by M. R. C.</center>
{| cellpadding="2" cellspacing="0"  border="1" style="background-color:#FFFFFF; border-collapse: collapse; border: 1px #DEE8F1 solid; font-weight: normal; font-family: verdana"
! style="background-color:#B0BFD5; color:white"|#
! style="background-color:#B0BFD5; color:white"|Name
! style="background-color:#B0BFD5; color:white"|Translation in English
! style="background-color:#B0BFD5; color:white"|Year of publishing, Publisher
|----

! align = "left"|  1
|align = "left"| Communism chila thiruthalukal <br> {{lang-ml|കമ്മ്യൂണിസം-ചില തിരുത്തലുകള്‍ }}
|align = "left"| Communism - Some Critical notes
|align = "left"| 1999, Kalaveekshanam Books Calicut
|-
! align = "left"|  2
|align = "left"| Keralathile Communism <br> {{lang-ml|കേരളത്തിലെ കമ്മ്യൂണിസം }}
|align = "left"| Communism in Kerala  
|align = "left"| 2000, Kalaveekshanam Books Calicut
|-
! align = "left"|  3
|align = "left"| Kerala modelilekku oru kilivaathil <br> {{lang-ml|കേരളമോഡലിലേക്കു ഒരു കിളിവാതില്‍ }}
|align = "left"| An eye on the Kerala Model 
|align = "left"| 2001, Haritham Books Calicut
|-
|}
|}

=== Social ===
List of Social Books by M. R. C.
{|cellpadding="2" cellspacing="0" border="1" style="border-collapse: collapse; border: 2px #DEE8F1 solid; font-weight: bold; font-family: verdana"
|<center>List of Social Books by M. R. C.</center>
{| cellpadding="2" cellspacing="0"  border="1" style="background-color:#FFFFFF; border-collapse: collapse; border: 1px #DEE8F1 solid; font-weight: normal; font-family: verdana"
! style="background-color:#B0BFD5; color:white"|#
! style="background-color:#B0BFD5; color:white"|Name
! style="background-color:#B0BFD5; color:white"|Translation in English
! style="background-color:#B0BFD5; color:white"|Year of publishing, Publisher
|----

! align = "left"|  1
|align = "left"| Manushyavakaashangal <br> {{lang-ml|മനുഷ്യാവകാശങ്ങള്‍ }}
|align = "left"| Human Rights (Translation) 
|align = "left"| 1962, Southern Languages Book Trust Madras
|-
! align = "left"|  2
|align = "left"| India innu evide nilkkunnu <br> {{lang-ml|ഇന്ത്യ ഇന്ന് എവിടെ നില്‍ക്കുന്നു }}
|align = "left"| whither stands India Today
|align = "left"| 2003, State Institute of Languages Trivandrum
|-

|}
|}

=== Memoirs ===
{|cellpadding="2" cellspacing="0" border="1" style="border-collapse: collapse; border: 2px #DEE8F1 solid; font-weight: bold; font-family: verdana"
|<center>List of Memoirs by M. R. C.</center>
{| cellpadding="2" cellspacing="0"  border="1" style="background-color:#FFFFFF; border-collapse: collapse; border: 1px #DEE8F1 solid; font-weight: normal; font-family: verdana"
! style="background-color:#B0BFD5; color:white"|#
! style="background-color:#B0BFD5; color:white"|Name
! style="background-color:#B0BFD5; color:white"|Translation in English
! style="background-color:#B0BFD5; color:white"|Year of publishing, Publisher
|----

! align = "left"|  1
|align = "left"| Oormayude Pushtakam <br> {{lang-ml|ഓര്‍മ്മയുടെ പുസ്തകം }}
|align = "left"| A Book of Memories
|align = "left"| 2003, Current Books Thrissur
|-
! align = "left"|  2
|align = "left"| Sahithyathil Diggajangalude oru kalaghattam <br> {{lang-ml|സാഹിത്യത്തില്‍ ദിഗ്ഗജങ്ങളുടെ ഒരു കാലഘട്ടം }}
|align = "left"| The Time of Colossal figures in Malayalam Literature  
|align = "left"| 2006, Poorna Publications Calicut
|-
! align = "left"|  3
|align = "left"| Ente jeevithakadhayile N.V parvam <br> {{lang-ml|എന്‍റെ ജീവിതകഥയിലെ എന്‍.വി. പര്‍വ്വം }}
|align = "left"| The N. V. Chapter in my literary life story
|align = "left"| 2010, 2016 2nd Edition Green Books Thrissur
                 
|-
! align = "left"|  4
|align = "left"| Kudumba parvam <br> {{lang-ml|കുടുംബപര്‍വ്വം }}
|align = "left"| The Family Chapter of my life story
|align = "left"| 2011, Maluban Publishers Trivandrum
|-
! align = "left"|  5
|align = "left"| Balikuteerangalkkoru Ormapusthakam <br> {{lang-ml|ബലികുടീരങ്ങള്‍ക്ക് ഒരു ഓര്‍മ്മപുസ്തകം }}
|align = "left"| A memoir for sacrificial stone temples
|align = "left"| 2015,  Green Books Thrissur
|-				
|}
|}

=== Novel Translation ===

{|cellpadding="2" cellspacing="0" border="1" style="border-collapse: collapse; border: 2px #DEE8F1 solid; font-weight: bold; font-family: verdana"
|<center>List of Novel Translation by M. R. C.</center>
{| cellpadding="2" cellspacing="0"  border="1" style="background-color:#FFFFFF; border-collapse: collapse; border: 1px #DEE8F1 solid; font-weight: normal; font-family: verdana"
! style="background-color:#B0BFD5; color:white"|#
! style="background-color:#B0BFD5; color:white"|Name
! style="background-color:#B0BFD5; color:white"|Translation in English
! style="background-color:#B0BFD5; color:white"|Original
! style="background-color:#B0BFD5; color:white"|Year of publishing, Publisher
|----

! align = "left"|  1
|align = "left"| Padivathilkkal <br> {{lang-ml| പടിവാതിൽക്കൽ }}
|align = "left"| At the outer gate
|align = "left"| On the eve, Russian, [[Ivan Turgenev]] 
|align = "left"| 1955, Prabhath Book House Ernakulam
|-
! align = "left"|  2
|align = "left"| [[Kokoro]] <br> {{lang-ml|കൊകോറോ }}
|align = "left"| [[Kokoro]]<ref>[[Kokoro]]</ref>
|align = "left"| Japanese Novel 
|align = "left"| 1962, Southern Languages Book Trust Madras
|-
! align = "left"|  3
|align = "left"| Uzhuthumaricha Puthumannu <br> {{lang-ml|ഉഴുതുമറിച്ച പുതുമണ്ണ് }}
|align = "left"| Virgin Soil Upturned
|align = "left"| Russian, [[Mikhail Sholokhov]] 
|align = "left"| 1962, CICC Books Ernakulam
|-
! align = "left"|  4
|align = "left"| Don Nadeetheerathe Koithu <br> {{lang-ml|ഡോണ്‍നദീതീരത്തെ കൊയ്ത്ത് }}
|align = "left"|   Harvest on the Don
|align = "left"| Russian, [[Mikhail Sholokhov]] 
|align = "left"| 1962, CICC Books Ernakulam <br> 2015, S.P.C.S. Ltd. Kottayam
|-
! align = "left"|  5
|align = "left"| [[Genghis Khan]] <br> {{lang-ml|ഝഖിസ്ഖാന്‍ }}
|align = "left"| [[Genghis Khan]]-The world Conqueror (Historical Biography)
|align = "left"| [[Harold Lamb]] 
|align = "left"| 1963, Current Books Thrissur
|-
! align = "left"|  6
|align = "left"| Timoor <br> {{lang-ml|തിമൂർ }}
|align = "left"| Timoor Historical Biography
|align = "left"| [[Tamerlane]] [[Harold Lamb]] 
|align = "left"| 1963, Current Books Thrissur
|-
! align = "left"|  7
|align = "left"| Maattivecha Talakal <br> {{lang-ml|മാറ്റിവെച്ച തലകള്‍ }}
|align = "left"| The Transposed Heads  
|align = "left"| German, [[Thomas Mann]] 
|align = "left"| 1979, Chintha Books Trivandrum
|-
! align = "left"|  8
|align = "left"| Kalanju Kittiya Mookku <br> {{lang-ml|കളഞ്ഞുകിട്ടിയ മൂക്ക് }}
|align = "left"| [[The Nose (Gogol short story)|The Nose]]  
|align = "left"| Russian, [[Nikolai Gogol]] 
|align = "left"| 2006, Lipi Publications Calicut
|-
|}
|}

=== Science Translation ===
{|cellpadding="2" cellspacing="0" border="1" style="border-collapse: collapse; border: 2px #DEE8F1 solid; font-weight: bold; font-family: verdana"
|<center>List of Science Translation by M. R. C.</center>
{| cellpadding="2" cellspacing="0"  border="1" style="background-color:#FFFFFF; border-collapse: collapse; border: 1px #DEE8F1 solid; font-weight: normal; font-family: verdana"
! style="background-color:#B0BFD5; color:white"|#
! style="background-color:#B0BFD5; color:white"|Name
! style="background-color:#B0BFD5; color:white"|Translation in English
! style="background-color:#B0BFD5; color:white"|Year of publishing
|----
! align = "left"|  1
|align = "left"| Naam jeevikkunna ee lookam <br> {{lang-ml|നാം ജീവിക്കുന്ന ഈ ലോകം }}
|align = "left"| The World We Live in 
|align = "left"| 1959, Mathrubhumi Co Calicut
|-
! align = "left"|  2
|align = "left"| maanathekku nookkumpol <br> {{lang-ml|മാനത്തേക്കു നോക്കുമ്പോള്‍ }}
|align = "left"| Let us Look at the Sky 
|align = "left"| 1961, Southern Languages Book Trust Madras
|-				
|}
|}

=== Children's Literature ===

{|cellpadding="2" cellspacing="0" border="1" style="border-collapse: collapse; border: 2px #DEE8F1 solid; font-weight: bold; font-family: verdana"
|<center>List of Children's Literature by M. R. C.</center>
{| cellpadding="2" cellspacing="0"  border="1" style="background-color:#FFFFFF; border-collapse: collapse; border: 1px #DEE8F1 solid; font-weight: normal; font-family: verdana"
! style="background-color:#B0BFD5; color:white"|#
! style="background-color:#B0BFD5; color:white"|Name
! style="background-color:#B0BFD5; color:white"|Translation in English
! style="background-color:#B0BFD5; color:white"|Original
! style="background-color:#B0BFD5; color:white"|Year of publishing, Publisher
|----

! align = "left"|  1
|align = "left"| Srugaalan enna kurukkan <br> {{lang-ml|സൃഗാലന്‍ എന്ന കുറുക്കന്‍ }}
|align = "left"| A Fox Named Srigalan
|align = "left"| Reynard the fox(British Folklore)
|align = "left"| 2004, Current Books Thrissur
|-	
! align = "left"|  2
|align = "left"| moonnu bala saahithya kruthikal <br> {{lang-ml|മൂന്നു ബാലസാഹിത്യകൃതികള്‍ }} 
|align = "left"| Three Stories for Children
|align = "left"| [[Oscar Wilde]]
|align = "left"| 2010, Lipi Publications Calicut
|-		
|}
|}

=== Other Genres ===

{|cellpadding="2" cellspacing="0" border="1" style="border-collapse: collapse; border: 2px #DEE8F1 solid; font-weight: bold; font-family: verdana"
|<center>List of other genres by M. R. C.</center>
{| cellpadding="2" cellspacing="0"  border="1" style="background-color:#FFFFFF; border-collapse: collapse; border: 1px #DEE8F1 solid; font-weight: normal; font-family: verdana"
! style="background-color:#B0BFD5; color:white"|#
! style="background-color:#B0BFD5; color:white"|Name
! style="background-color:#B0BFD5; color:white"|Translation in English
! style="background-color:#B0BFD5; color:white"|Year of publishing, Publisher
|----

! align = "left"|  1
|align = "left"| Mushaayira <br> {{lang-ml|മുശായിര }}
|align = "left"| Poem Collections Kerala Sahithya Samiti
|align = "left"| 2004, Prabhakar Book Stall Shornur
|-	
! align = "left"|  2
|align = "left"| aana undaayirunna veedu <br> {{lang-ml|ആന ഉണ്ടായിരുന്ന വീട് }}
|align = "left"| A House which owned an elephant (Novel)
|align = "left"| 2012, Samayam Books Kannur
|-	
! align = "left"|  3
|align = "left"| Vilakkukal Puthiyathum Pazhayathum <br> {{lang-ml|വിളക്കുകൾ പുതിയതും പഴയതും}}
|align = "left"| Collection of literary articles
|align = "left"| 2016, S.P.C.S Ltd. Kottayam
|-	
|}
|}

=== Journals Edited by MRC ===

{|cellpadding="2" cellspacing="0" border="1" style="border-collapse: collapse; border: 2px #DEE8F1 solid; font-weight: bold; font-family: verdana"
|<center>List of journels edited by M. R. C.</center>
{| cellpadding="2" cellspacing="0"  border="1" style="background-color:#FFFFFF; border-collapse: collapse; border: 1px #DEE8F1 solid; font-weight: normal; font-family: verdana"
! style="background-color:#B0BFD5; color:white"|#
! style="background-color:#B0BFD5; color:white"|Name
! style="background-color:#B0BFD5; color:white"|Publisher
|----
! align = "left"|  1
|align = "left"| Yuva Bhavana<br> {{lang-ml| യുവഭാവന   }}
|align = "left"| Published from Calicut
|-	
! align = "left"|  2
|align = "left"| Vijnana Kairali <br> {{lang-ml|വിജ്ഞാന കൈരളി }}
|align = "left"| Journal of Kerala Language Institute, Tvm
|-	
! align = "left"|  3
|align = "left"| Sahithya Samithi Masika <br> {{lang-ml| സാഹിത്യ സമിതി മാസിക }}
|align = "left"| Published from Payyannur, Kannur District
|-
! align = "left"|  4
|align = "left"| Chinthana Weekly <br> {{lang-ml| ചിന്തന }}
|align = "left"| Published from Kannur District
|-
! align = "left"|  5
|align = "left"| Malayala Mannu Weekly <br> {{lang-ml| മലയാള മണ്ണ് }}
|align = "left"| Published from Kannur District
|-	
|}
|}

==References==
{{reflist}}

Other Links<br>
http://www.thehindu.com/todays-paper/tp-national/tp-kerala/akpcta-district-convention/article3174584.ece <br>
http://www.amazon.com/M.R.Chandrasekharan/e/B00JQAO64G <br>
http://www.indulekha.com/index.php?route=product/author/product&author_id=1709 <br>
http://www.mkbhasi.com/mypdf/Mazha1/M%20R%20Chandrasekharan.pdf

{{Malayalam Literature |state=collapsed}}

{{DEFAULTSORT:Chandrasekharan, M.R.}}
[[Category:Indian literary critics]]
[[Category:1929 births]]
[[Category:Living people]]
[[Category:People from Thrissur district]]
[[Category:Writers from Kerala]]
[[Category:Indian children's writers]]
[[Category:Indian male writers]]
[[Category:20th-century Indian non-fiction writers]]
[[Category:20th-century Indian translators]]
[[Category:Indian political writers]]
[[Category:Indian literary historians]]
[[Category:Malayalam-language writers]]
[[Category:Indian memoirists]]
[[Category:Indian social sciences writers]]