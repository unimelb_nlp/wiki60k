<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Bergfalke
 | image=OY-FBX Scheibe Bergfalke III.jpg
 | caption=Scheibe Bergfalke III
}}{{Infobox Aircraft Type
 | type=Sailplane
 | national origin=Germany
 | manufacturer=[[Scheibe Flugzeugbau|Scheibe]]
 | designer=[[Egon Scheibe]]
 | first flight=5 August 1951
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=more than 320 by 1982
 | developed from= [[Akaflieg München Mü13]]
 | variants with their own articles=
}}
|}

The '''Scheibe ''Bergfalke''''' (German: "mountain hawk") is a German [[Glider (sailplane)|glider]] designed by [[Egon Scheibe]] as a post-World War II development of the [[Akaflieg München Mü13]] produced before and during the war.

==Design and development==
The prototype flew on 5 August 1951 as the '''Akaflieg München Mü13E Bergfalke I''' and by the end of the year, Scheibe had established his own works at the [[Munich-Riem Airport]] to produce the type as the '''Bergfalke II'''.<ref name = Hardy>{{cite book |last= Hardy |first= Michael |title=Gliders and Sailplanes of the World |year=1982 |publisher=Ian Allan |location=Shepperton |pages=79–80 }}</ref> It was a mid-wing sailplane of conventional design with a non-retractable monowheel undercarriage and a tailskid.<ref name="JAWA">{{cite book |last= Taylor |first= John W. R. |title=Jane's All the World's Aircraft 1977–78 |year=1977 |publisher=Jane's Yearbooks |location=London |pages=528 & 594 }}</ref> The fuselage was a welded steel structure covered in fabric and enclosed two seats in tandem.<ref name="JAWA"/> The wings had a single wooden spar and were covered in plywood.<ref name="JAWA"/>

Subsequent versions introduced forward sweep to the wings, a more aerodynamic canopy, airbrakes, and a tailwheel in place of the tailskid.<ref name=Hardy/> By 1982, Scheibe had built over 300 of these aircraft, and [[Stark Ibérica]] built a number of the '''Bergfalke III''' version under license in Spain.<ref name=Hardy/> Scheibe also developed a motorglider version as the '''Bergfalke IVM'''<ref name=Hardy/> but this did not enter production.<ref name="JAWA"/>

In 1976, two Bergfalke motorgliders participated in the Sixth German Motor Glider Competition. Later, one of these aircraft set a world 300&nbsp;km triangle record.<ref name = Coates>{{cite book |last= Coates |first= Andrew |title=Jane's World Sailplanes and Motor Gliders |year=1978 |publisher=MacDonald and Jane's |location=London |pages=67 }}</ref>
<!-- ==Operational history== -->

==Variants==
;Mü13E Bergfalke I
:Prototype
;Bergfalke II
:First production version, 4° forward sweep on wings<ref name=Hardy/>
;Bergfalke II/55
;Skopil Bergfalke II/55
:Motorglider conversion done by Arnold Skopil of [[Aberdeen, Washington]], [[United States]] in 1957. One converted.<ref name="SoaringNov83">Said, Bob: ''1983 Sailplane Directory, [[Soaring Magazine]]'', page 131. Soaring Society of America, November 1983. USPS 499-920</ref><ref name="SD">{{Cite web|url = http://www.sailplanedirectory.com/PlaneDetails.cfm?planeID=38|title = Bergfalke Scheibe |accessdate = 30 July 2011|last = Activate Media|authorlink = |year = 2006}}</ref>
;Bergfalke III
:Streamlined canopy, taller fin and rudder, Schempp-Hirth airbrakes, 2° forward sweep on wings<ref name=Hardy/>
;Bergfalke IV
:Wing of Wortmann section with 60-cm (2-ft) greater span<ref name=Hardy/>
;Bergfalke IVM
:Motorglider version with 39-kW (52-hp) [[Hirth O-28]] engine mounted on retractable pylon behind cockpit.<ref name=Hardy/>

==Specifications (Bergfalke II/55)==
[[File:BFIV 5 alt.jpg|thumb|Scheibe Bergfalke IV on final]]
[[File:BergfalkeIII.jpg|thumb|1966 Scheibe Bergfalke III]]
{{Aircraft specs
|ref=The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs du Monde<ref name="JAWA"/><ref name=Shenstone>{{cite book|last=Shenstone|first=B.S.|title=The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs du Monde|year=1958|publisher=Organisation Scientifique et Technique Internationale du Vol a Voile (OSTIV) and Schweizer Aero-Revue|location=Zurich|pages=73–80|edition=1st|author2=K.G. Wilkinson |language=English, French, German}}</ref>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=2
|capacity=
|length m=8
|length ft=
|length in=
|length note=
|span m=16.6
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=17.7
|wing area sqft=
|wing area note=
|aspect ratio=15.6
|airfoil=Mü-Profil 14.5%
|empty weight kg=246
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=440
|max takeoff weight lb=
|max takeoff weight note=
|more general=
<!--
        Performance
-->
|perfhide=
|stall speed kmh=60
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=160
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=<br/>
*'''Rough air speed max:''' {{convert|120|km/h|mph kn|abbr=on|1}}
*'''Aerotow speed:''' {{convert|120|km/h|mph kn|abbr=on|1}}
*'''Winch launch max speed:''' {{convert|85|km/h|mph kn|abbr=on|1}}
*'''Terminal velocity:''' {{convert|205|km/h|mph kn|abbr=on|1}} (max all-up weight + full airbrakes / flaps)
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|g limits=+4 -2
|roll rate=<!-- aerobatic -->
|glide ratio=28:1at {{convert|80|km/h|mph kn|abbr=on|1}} 
|sink rate ms=0.72
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=at {{convert|72|km/h|mph kn|abbr=on|1}} 
|lift to drag=
|wing loading kg/m2=24.8
|wing loading lb/sqft=
|wing loading note=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=
*[[Schleicher K13]]
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{refbegin}}
*{{cite book|last=Shenstone|first=B.S.|title=The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs du Monde|year=1958|publisher=Organisation Scientifique et Technique Internationale du Vol a Voile (OSTIV) and Schweizer Aero-Revue|location=Zurich|pages=73–80|edition=1st|author2=K.G. Wilkinson |language=English, French, German}}
* {{cite book |last= Coates |first= Andrew |title=Jane's World Sailplanes and Motor Gliders |year=1978 |publisher=MacDonald and Jane's |location=London |pages=67 }}
* {{cite book |last= Hardy |first= Michael |title=Gliders and Sailplanes of the World |year=1982 |publisher=Ian Allan |location=Shepperton |pages=79–80 }}
* {{cite book |last= Taylor |first= John W. R. |title=Jane's All the World's Aircraft 1977–78 |year=1977 |publisher=Jane's Yearbooks |location=London |pages=528 }}
* {{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London |pages=793 }}
{{refend}}

==External links==
{{Commons category}}
* [http://www.sailplanedirectory.com/scheibe.htm#Bergfalke Bergfalke described on sailplanedirectory.com]

{{Scheibe aircraft}}

[[Category:German sailplanes 1950–1959]]
[[Category:Glider aircraft]]
[[Category:Scheibe aircraft|Bergfalke]]