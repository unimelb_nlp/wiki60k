<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Bohannon B-1
 | image=BohannanB-1.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Homebuilt aircraft]]
 | national origin=[[United States]]
 | manufacturer=
 | designer=Bruce Bohannon
 | first flight=
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=1
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from=[[Van's RV-4]]
 | variants with their own articles=
}}
|}

The '''Bohannon B-1''' is a purpose-built aircraft to set new world records in its class for time-to-climb. It is a development of the [[Van's RV-4]].<ref>{{cite web|title=Bruce Bohannon|url=http://www.avweb.com/news/profiles/181769-1.html|accessdate=20 August 2012}}</ref>

==Development==
Bohannon and Miller built the [[Miller-Bohannon JM-2 Pushy Galore]] to set new time to climb records and compete in Formula One air racing. Bohannon set out to continue time to climb records with a new aircraft. He acquired Exxon as a sponsor for record attempts. His B-1 was painted in a stylized tiger paint scheme and named the "Exxon Flyin' Tiger".<ref>{{cite journal|magazine=Popular Science|date=July 1994|page=8}}</ref> The aircraft went on to set 17 altitude and time to climb records.<ref>{{cite web|title=Retirern' Tiger |url=http://www.eaa.org/news/2008/2008-12-04_bohannon.asp |accessdate=21 August 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20130226075332/http://eaa.org/news/2008/2008-12-04_bohannon.asp |archivedate=26 February 2013 |df= }}</ref><ref>{{cite web|title=Bruce Bohannon|url=http://www.trioavionics.com/bruce_bohannon.htm|accessdate=21 August 2012}}</ref>

==Design==
The Bohannon B-1 is an all-metal single place, [[low-wing]] aircraft with [[conventional landing gear]]. The [[Lycoming IO-540]] engine is augmented with nitrous oxide to increase power from {{convert|260|to|425|hp|kW|0|abbr=on}}.<ref>{{cite journal|magazine=Flying Magazine|date=November 1999|page=35}}</ref> For 2001 attempts, a Lycoming IO-555 was installed.<ref>{{cite journal|magazine=Flying Magazine|date=July 2001|page=36}}</ref>

==Operational history==
The B-1 has set, and beat, its own records several times. The B-1 operates in the FAI C-1b Class. (Piston aircraft {{convert|1102|to|2205|lb|kg|0|abbr=on}}). 
*1999 - Time to climb to {{convert|3000|m|ft|0|abbr=on}} record of 2 minutes and 20 seconds.<ref>{{cite journal|magazine=Flying Magazine|date=November 1999|page=35}}</ref>
*2000 - Time to climb to {{convert|6000|m|ft|0|abbr=on}} - Six minutes, thirty seven seconds.<ref>{{cite journal|magazine=Flying Magazine|date=February 2001|page=35}}</ref>
*2001 - Absolute ceiling for a piston engine aircraft - {{convert|35000|ft|m|0|abbr=on}}.<ref>{{cite journal|magazine=Flying Magazine|date=July 2001|page=36}}</ref>
*2002 - Absolute ceiling - {{convert|37536|ft|m|0|abbr=on}} <ref>{{cite journal|magazine=Flying Magazine|date=July 2002|page=36}}</ref>
*2002 - Time to climb to {{convert|12000|m|ft|0|abbr=on}} in 32 minutes, 2 seconds
*2002 - Absolute ceiling - {{convert|41300|ft|m|0|abbr=on}}.<ref>{{cite journal|magazine=Flying Magazine|date=February 2003|page=34}}</ref>
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (Bohannon B-1) ==
{{Aircraft specs
|ref=Sport Aviation<!-- for giving the reference for the data -->
|prime units?=kts<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=1
|capacity=
|length m=
|length ft=
|length in=
|length note=
|span m=
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Lycoming IO-540]]
|eng1 type=Horizontally opposed piston
|eng1 kw=<!-- prop engines -->
|eng1 hp=425<!-- prop engines -->

|prop blade number=3<!-- propeller aircraft -->
|prop name=[[Hartzell Propeller|Hartzell]]
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=78<!-- propeller aircraft -->
|prop dia note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=240
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=4407
|climb rate note=1999 time to climb record
|time to altitude=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=
*[[Van's RV-4]]<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{reflist|30em}}
<!-- ==External links== -->

[[Category:Homebuilt aircraft]]