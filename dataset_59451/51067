{{Infobox journal
| cover =
| editor = Christian Néri 
| discipline = [[Genomics]]
| formernames = 
| abbreviation = Curr. Genomics
| publisher = [[Bentham Science Publishers]] 
| country =
| frequency = 8/year
| history = 2000–present 
| openaccess = 
| license = 
| impact = 2.868 
| impact-year = 2013
| website = http://www.benthamscience.com/cg/index.htm
| link1 = http://www.benthamscience.com/contents-JCode-CG-Vol-00000013-Iss-00000005.htm
| link1-name = Online access
| link2 = http://www.benthamscience.com/ContentAbstract.php?JCode=CG
| link2-name = Online archive
| JSTOR = 
| OCLC = 47091629
| LCCN =
| CODEN = CMMUBP
| ISSN = 1389-2029
| eISSN = 1875-5488
}}
'''''Current Genomics''''' is a [[peer-reviewed]] [[scientific journal]] covering all aspects of [[genomics]]. It was established in 2000 with [[Stefan M. Pulst]] as founding editor-in-chief and is  published by [[Bentham Science Publishers]]. 
The [[editor-in-chief]] is Christian Néri ([[INSERM]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[BIOSIS Previews]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-04-28}}</ref>
* [[Chemical Abstracts Service]]<ref name=CASSI>{{cite web|url=http://cassi.cas.org/search.jsp |archive-url=http://wayback.archive-it.org/all/20100211181038/http://cassi.cas.org/search.jsp |dead-url=yes |archive-date=2010-02-11 |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2015-04-28 }}</ref>
* [[Embase]]<ref name=Embase>{{cite web |url=http://www.elsevier.com/online-tools/embase/about |title=Journal titles covered in Embase |publisher=[[Elsevier]] |work=Embase |accessdate=2015-04-28}}</ref>
* [[EMBiology]]
* [[Science Citation Index Expanded]]<ref name=ISI/>
* [[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2015-04-28}}</ref>
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 2.868.<ref name=WoS>{{cite book |year=2014 |chapter=Current Genomics |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{reflist}}

== External links ==
*{{Official website|http://benthamscience.com/journal/index.php?journalID=cg}}

[[Category:Bentham Science Publishers academic journals]]
[[Category:Publications established in 2001]]
[[Category:English-language journals]]
[[Category:Genetics journals]]