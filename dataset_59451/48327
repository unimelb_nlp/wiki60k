{{Orphan|date=March 2016}}

{{Infobox officeholder
|name         = Fred C. Haack
|image        = 
|office       = Alderman of the Sheboygan Common Council for the 5th Ward
|term_start   = 1897
|term_end     = 1916
|predecessor  = 
|successor    = Ernst Zehms
|birth_name   = Fred C. Haack
|birth_date   = c. 1873
|birth_place  = [[Germany]]
|death_date   = 1944
|death_place  = Milwaukee, Wisconsin
|party        = Social Democratic Party, Populist Party
|spouse       = Elizabeth Schneider
|children     = 
|alma_mater   = 
|signature    = 
|website      = 
}}
'''Fred C. Haack''' (c.1873-1944) was the first member of the [[Social Democratic Party (United States)|Social Democratic Party]] to hold public office in the United States. He was originally elected to the common council of [[Sheboygan, Wisconsin|Sheboygan]], [[Wisconsin]] as a member of the [[Populist Party (US)|Populist Party]] in 1897, but he soon joined in organizing the local Social Democratic Party. Running on the Socialist ticket, he was re-elected 5th ward alderman in 1898. He served a total of sixteen years. Haack was recognized as the first American Socialist officeholder at the national Socialist Party convention held in [[Milwaukee]] in 1932, despite the fact that socialists had been elected as Chicago aldermen and Illinois legislators as early as 1878. Also elected in 1898 on the Socialist ticket was local baseball manager August L. Mohr.<ref>"Former Sheboygan Alderman Fred C. Haack is Laid to Rest," Sheboygan Press, August 4, 1944</ref><ref>''The Sewer Socialists, A History of the Socialist Party in Wisconsin, 1897 - 1940'' by Elmer Axel Beck, Westburg Associates, Fennimore, WI, 1982, Volume One, Chapter II</ref>

Haack was born in Germany, and in 1882 he traveled to Sheboygan to join up with his father Frederick Sr. who worked as a laborer. In 1891 Frederick Sr. opened a shoe and boot store on the city's south side at 1226 Georgia Avenue, where he made his home on the second floor with his wife Regina and his growing family. By 1892 son Fred was the bookkeeper for the family business. In 1897 he married Elizabeth Schneider, who lived on the same street.<ref>''Sheboygan Times'', June 6, 1897</ref>

As the alderman for Sheboygan's 5th ward, Haack turned from populism to socialism in 1897. He did not advocate a radical agenda, but was often a contentious figure.<ref>"John H. Optenberg Second Choice in Proposed Suit," ''Sheboygan Press'', July 19, 1909</ref><ref>"Another Circular," ''Sheboygan Press'', April 5, 1910</ref> He promoted city ownership of public utilities. He also advanced a bond issue that helped build a new school in his precinct.<ref>''Sheboygan Times'', February 26, 1898</ref> He was also in favor of paying aldermen to attend council meetings. He sat on various committees concerning the judiciary, the poor, printing, licenses, bonds and salaries, and became a powerful chairman of the building committee later in his tenure. In 1900 the city council appointed a committee to investigate Haack in regard to the purchase of additional school grounds.<ref>"Denies the Charges," ''Milwaukee Journal'', January 16, 1900</ref> In 1914 aldermen elected Haack president of the common council.<ref>"Haack Wins the Plum," ''Sheboygan Press'', April 22, 1914</ref> He ran for mayor the next year, but was unsuccessful.<ref>''Sheboygan Press'', March 16, 1915</ref> Months later he objected to the appointment of a Socialist to a vacant council seat previously held by a [[Republican Party (United States)|Republican]].<ref>"Meyer Crosses Haack," ''Sheboygan Press'', May 4, 1915</ref>

By 1908 Haack had moved to a new residence down the block from his business. In 1915 he bought out the interests of other heirs to the shoe store property, which had also become a substation of the local post office.<ref>"Ald. Haack Buys Out Heirs," ''Sheboygan Press'', December 10, 1915.</ref> In April 1916 Haack unexpectedly failed to be re-elected to the council.<ref>"Several Surprises are Sprung," ''Sheboygan Press'', April 5, 1916</ref> Less than a week later his store was damaged by a fire that destroyed much of his stock.<ref>"Haack Shoe Store Burns Last Night," Sheboygan Press, April 11, 1916</ref> That October he was hired as a local insurance agent for the Eureka F. & M. and the Security of Cincinnati.<ref>"Wisconsin Notes," ''The Western Underwriter'', October 26, 1916</ref>

He moved his family to Milwaukee, where Haack purchased rental property.<ref>Advertisement, ''Sheboygan Press'', January 18, 1917</ref> He was still engaged in the insurance business when his wife died in 1924. He joined the 24th ward branch of the city's Socialist Party in 1930, and was re-instated as an honorary member of the 21st ward branch. He held the position of party secretary for many years. He later held clerical positions with various [[Milwaukee County]] institutions. In later life Haack was crippled in an auto accident. A few years later in August 1944 his body was found in the [[Milwaukee River]].<ref>"Former Sheboygan Alderman Fred C. Haack is Laid to Rest," ''Sheboygan Press'', August 4, 1944</ref>

==References==
{{reflist}}

{{DEFAULTSORT:Haack, Fred}}
[[Category:1873 births]]
[[Category:1944 deaths]]
[[Category:German emigrants to the United States]]
[[Category:Politicians from Milwaukee]]
[[Category:People from Sheboygan, Wisconsin]]
[[Category:Businesspeople from Wisconsin]]
[[Category:Wisconsin city council members]]
[[Category:Wisconsin Populists]]
[[Category:Socialist Party of America politicians from Wisconsin]]