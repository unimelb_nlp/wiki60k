__NOTOC__
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox aircraft begin
 |name=Bushmaster 2000
 |image=Stout Bushmaster 2000.jpg
 |caption=Bushmaster 2000 at Oshkosh, Wisconsin, 2005
}}{{Infobox aircraft type
 |type=Airliner
 |manufacturer=[[Bushmaster Aircraft]]
 |national origin=United States
 |designer=
 |first flight=1964
 |introduced=
 |retired=
 |status=
 |primary user=
 |more users=
 |produced=
 |number built=2
 |developed from = [[Ford Trimotor]]
 |variants with their own articles=
}}
|}

The '''Bushmaster 2000''' was a small commuter airliner built in the United States in an attempt to revive the [[Ford Trimotor]] design. Work began in 1953 by testing a vintage Trimotor and in 1954 [[William Bushnell Stout|Bill Stout]] purchased the design rights to the original Trimotor. Due to "Ford Tri-Motor" licensing problems, the Ford 15-AT-D was given the Bushmaster 2000 name.<ref name= "O'Callaghan, p. 124.">O'Callaghan 2002, 124.</ref> On 15 January 1955, Stout and partner Robert Hayden from the Hayden Aircraft Corporation announced they were planning to build 1,000 new Bushmasters, but it would be eleven years before the first prototype of the new design flew.

The Bushmaster 2000 featured significant modernisation of the original 1920s design, particularly in the choice of materials and construction techniques. It also had more powerful engines, enlarged cockpit windows, a lighter and stronger aluminum-alloy skin, a foot-operated hydraulic replacement of the old Trimotor's hand-operated "[[Johnson bar (vehicle)|Johnny Brake]]," a larger stabilizer and a dorsal fin to reduce yaw, modern trim tabs and interior rather than exterior control cables.<ref name= "Time">''Time'' 6 January 1967</ref>

However, even with modern engines and propellers, the aircraft's performance did not compare favourably with contemporary designs of similar capacity, and no sales ensued. Combined with financial, management and marketing problems, only two examples were built with a third fuselage never completed.<ref name= "O'Callaghan, p. 124."/>

The first Bushmaster, N7501V was assembled in 1966, is owned by Greg Herrick's Golden Wings Museum and based at Anoka-Blaine airport near Minneapolis, Minnesota. The second aircraft N750RW was completed 18 January 1985 by Ralph Williams, the President of Hydro-Forming in [[Long Beach, California]]. This aircraft was written off in an accident at Fullerton Municipal Airport, California on 25 September 2004.

==Specifications==
{{aerospecs
|met or eng?=<!-- eng for US/UK aircraft, met for all others -->eng

|crew=Two
|capacity=23 passengers
|length m=50
|length ft=64
|length in=0.5
|span m=23.75
|span ft=77
|span in=11
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=4.09
|height ft=13
|height in=5
|wing area sqm=79.1
|wing area sqft=852
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|empty weight kg=3,402
|empty weight lb=7,500
|gross weight kg=6,750
|gross weight lb=12,500
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->

|eng1 number=3
|eng1 type=[[Pratt & Whitney R-985]]-AN-1 ''Wasp Junior'' 9-cylinder radial engines
|eng1 kw=<!-- prop engines -->336
|eng1 hp=<!-- prop engines -->450
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|max speed kmh=210
|max speed mph=125
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|range km=1,127
|range miles=700
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->

|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=<!-- related developments -->
* [[Ford Trimotor]]
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
|see also=<!-- other relevant information -->
}}

==References==
{{reflist}}
* {{cite book |last= O'Callaghan |first= Timothy J. |title=The Aviation Legacy of Henry & Edsel Ford |year=2002 |publisher=Proctor Publications |location=Ann Arbor, Michigan }}
* {{cite journal |last=O'Leary |first=Michael |date=December 2004 |title=Bushmaster Lost |journal=Air Classics |volume= |issue= |pages= }}
* {{cite journal |title=Return of the Tin Goose |journal=[[Time (magazine)|Time]] |date= 6 January 1967 |url=http://205.188.238.181/time/magazine/article/0,9171,843204,00.html |accessdate=2008-07-29}}
* {{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London |pages=225 }}
* {{cite book |title=World Aircraft Information Files |publisher=Bright Star Publishing|location=London |pages=File 890 Sheet 101 }}

==External links==
{{Commons category-inline|Stout Bushmaster 2000}}
* [https://web.archive.org/web/20070929134734/http://www.ntsb.gov/ntsb/brief2.asp?ev_id=20040929X01529&ntsbno=LAX04FA330&akey=1 NTSB crash report]

[[Category:United States airliners 1960–1969]]
[[Category:Trimotors]]
[[Category:High-wing aircraft]]
[[Category:Hayden aircraft]]