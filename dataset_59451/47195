{{Infobox person
|name          = Daniel D. Conover
|image         = 
|image_size    = 
|caption       = 
|birth_name    = 
|birth_date    = 1822
|birth_place   = [[New Jersey]], [[United States]]
|death_date    = {{death date and age|1896|8|15|1822|1|1}}
|death_place   = [[Bay Shore, New York]]
|death_cause   = 
|resting_place = Oakwood Cemetery
|resting_place_coordinates = 
|residence     = 
|nationality   = [[United States|American]]
|other_names   = 
|known_for     = Industrialist whose land development transformed Long Island into a popular summer vacation spot for New York high society; his appointment as NYC street commissioner was the cause of the [[New York City Police Riot|Police Riot of 1857]]. 
|education     =
|alma_mater    = 
|employer      = 
|occupation    = Public servant, political activist and industrialist
|home_town     = 
|title         = 
|salary        = 
|networth      = 
|height        = 
|weight        = 
|term          = 
|predecessor   = 
|successor     = 
|party         = 
|boards        = 
|religion      = 
|spouse        = Catherine Eliza Whitlock
|partner       = 
|children      = Augustus W. Conover<br>Catherine Conover
|parents       = 
|relations     = 
|signature     = 
|website       = 
|footnotes     = 
}}
'''Daniel Denice Conover''' (1822 – August 15, 1896) was an American public servant, political activist and industrialist. He was the first to invest in [[land development]] in [[Long Island]] and, through his efforts, was partly responsible for transforming the southern coastline, then known as the [[Great South Bay]], as a popular summer resort for many prominent [[New York City|New York]] and [[Brooklyn]] families throughout the mid-to late 19th century. 

His appointment as street commissioner of New York City by Governor [[John Alsop King|John King]] in 1857, which was instead turned over to [[Charles Devlin]] by Mayor [[Fernando Wood]], resulted in the [[New York City Police Riot|Police Riot of 1857]].

==Biography==
Born in 1822, Daniel Conover became involved in local New York politics as a young man. He soon became a well-known political activist, being a member of several prominent clubs, and was involved in both municipal and national elections. He was also an outspoken supporter of the [[New York City Fire Department|New York City Volunteer Fire Department]] and was closely associated with department for decades. In 1853, he was the foreman of Amity Hose which was reportedly ''"a company not only noted for its elegant carriage, but for the character and prominence of its members in the community"''. He was a member of the Common Council and, through his influence, he successfully introduced the resolution to purchase the fire department's first [[steam engine]]. He was also a [[presidential elector]] for [[John C. Frémont]] during the [[United States presidential election, 1856|United States presidential election of 1856]].<ref name="Article">"Obituary Record. Daniel Denice Conover". <u>New York Times.</u> 16 Aug 1896</ref>

In 1857, he was appointed street commissioner by Governor [[John Alsop King|John King]]. On the day he was to assume his office however, Conover was informed that Mayor [[Fernando Wood]] had instead given the position to [[Charles Devlin]] and had Conover thrown out of the building by Municipal police. Conover immediately obtained two arrest warrants for Mayor Wood, one charging him with [[Riot#United States|inciting a riot]] and another for [[assault]], and returned to [[New York City Hall]] with 50 Metropolitan officers. The resulting confrontation between the Municipal and Metropolitan police resulted in the [[New York City Police Riot|Police Riot of 1857]].<ref>[[Herbert Asbury|Asbury, Herbert]]. ''The Gangs of New York: An Informal History of the New York Underworld''. New York: Alfred A. Knopf, 1928. (pg. 97-102) ISBN 1-56025-275-8</ref><ref>Headley, J.T. ''The Great Riots of New York, 1712 to 1873, Including a Full and Complete Account of the Four Days' Draft Riot of 1863''. New York: E.B. Treat, 1873. (pg. 129-131)</ref><ref>Astor, Gerald. ''The New York Cops: An Informal History''. New York: Charles Scribner's Sons, 1971. (pg. 26-27)</ref><ref>Trager, James. ''The New York Chronology: The Ultimate Compendium of Events, People, and Anecdotes from the Dutch to the Present''. New York: HarperCollins, 2004. (pg. 113) ISBN 0-06-074062-0</ref>

Following this incident, Conover became a prominent industrialist especially in New York's 
growing street railway systems. He was one of the projectors for the [[Broadway Line (Midtown Manhattan surface)|Boulevard Line]], the granting of a charter for which precipitated a contest in the [[New York City Council|Board of Aldermen]], as well as the Thirty-Fourth Street line. He later became president of the Fulton Street, Wall Street & Cortland Street Ferries Railroad Company and the Twenty-Eighth & Twenty-Ninth Street Railroad Company.<ref name="Article"/>

Settling in [[Bay Ridge, New York]] during his later years, Conover was responsible for developing real estate in the area surrounding Bay Ridge and [[Islip (CDP), New York|Islip, New York]]. He first visited Bay Ridge in 1856 when the Olympic Club, of which he was a member, relocated there. He began buying up cheap undeveloped property, among these a sizable property on Saxton Avenue where he built a [[Victorian architecture|Victorian style cottage]] for his wife and children, and constructed homes which rented out for $100 to $1,000 a month. These cottages were based along Saxton Avenue, [[Awixa Avenue]] and Main Street and, although considered a highly risky investment, the area eventually became popular vacation spot for many prominent New York and Brooklyn families during the mid- to late 19th century.<ref name="Awixa">{{Cite web |url=http://awixacastle.com/neighborhood/daniel_d_conover.htm |title=Daniel D. Conover |accessdate=12 January 2009 |author= |last= |first= |authorlink= |coauthors= |date= |year= |month= |work=Penataquit Point History |publisher=AwixaCastle.com |location= |pages= |language= |doi= |archiveurl= |archivedate= |quote=}}</ref>

He also expended his projects to further develop the area and attract affluent residents. Among these included the dredging of nearby creeks allowing the navigation of larger boats. Using a [[steam shovel]], he was able to deepen and widen the channels of Awixa Creek, Champlin’s Creek and Orowoc Creek. The east end of Awixa Creek was cut off, dredged from its mouth northward toward South Country Road, and a freshwater lake was built. A road which would have connected Awixa Avenue to Saxton Avenue over the lake was also planned but never completed. Other changes included the channel of Champlin’s Creek being widened 90 feet while Orowoc Creek was dredged and the surrounding area filled for additional land.<ref name="Awixa"/>

Conover's experience in [[public transportation]] aided him in improving roads and highways in [[Long Island]] which were described as "of full width, flat surfaces, and composed of clam shells and in equal parts" that would "stand all seasons". While his designs were superior over the existing dirt roads, little was done to improve general road conditions despite complaints from local residents. One of his roads, the South Country Road, was one of the earliest modern public highways in Long Island and eventually became Moffitt Boulevard, located north of the [[Long Island Rail Road]] line. Islip in particular benefited greatly from Conover's land development and encouraged others to invest in the area as well. One of Conover's properties, Orowoc Pond, held a fishing expedition in 1899 which hosted former president [[Grover Cleveland]].<ref name="Awixa"/>

Conover remained in Bay Ridge until his death on August 15, 1896, and was buried in Oakwood Cemetery.<ref name="Article"/> His historic Saxton Avenue residence was bought from the Conover family by Franklyn and Edna Hutton in 1912, and then by Philip B. Weld in 1921 and finally H. Cecil Sharp in 1929. Sharp purchased additional property surrounding the home during the next few years and, in 1933, the house was removed in order for Sharp to build a new house. The barn and windmill however, remain on the property up to the present{{When|date=April 2017}} day.<ref name="Awixa"/>

==References==
{{Reflist}}

==Further reading==
*Costello, Augustine E. ''Our Police Protectors: History of the New York Police from the Earliest Period to the Present Time''. New York: A.E. Costello, 1885.
*Havemeyer, Harry W. "Along the Great South Bay, From Oakdale to Babylon - The Story of a Summer Spa, 1840-1940". 1996.
*Hickey, John J. ''Our Police Guardians: History of the Police Department of the City of New York, and the Policing of Same for the Past One Hundred Years''. New York: John J. Hickey, 1925.

==External links==
*{{pgbio|id=connors-conquest.html#0ZO0PPOOX|name=Daniel D. Connover}}

{{DEFAULTSORT:Conover, Daniel D.}}
[[Category:1822 births]]
[[Category:1896 deaths]]
[[Category:American industrialists]]
[[Category:American businesspeople]]
[[Category:Politicians from New York City]]
[[Category:People from New Jersey]]
[[Category:People from Bay Shore, New York]]
[[Category:People from Brooklyn]]