{{About||the 1946 prototype airliner|Ilyushin Il-18 (1946)|a proinflammatory [[cytokine]] dubbed "Il-18"|Interleukin 18}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox aircraft begin
 |name = Il-18 / Il-20 / Il-22
 |image = File:Rossiya Ilyushin Il-18.jpg
 |caption = Operational Ilyushin Il-18 of [[Russia State Transport Company]]
}}{{Infobox aircraft type
 |type = Turboprop [[airliner]] and [[reconnaissance aircraft]]
|manufacturer =
 |designer = [[Ilyushin]]
 |builder = Moscow Machinery Plant No. 30
 |first flight = 4 July 1957
 |introduced =
 |produced = 1957–1985
 |number built = at least 678<ref>[http://russianplanes.net/planelist/Ilushin/Il-18/20/22/38  реестр самолётов типа Ильюшин Ил-18/20/22/38], Il-18 russianplanes.net</ref>
 |status = Out of production, in military and limited civilian service
 |unit cost = $24,500,000 (2011 USD equivalent) <!-- Il-18s were sold to Ghana for 1.5 million dollars each in 1961. Adjusted relative to GDP per capita this would give the above figure. -->
 |primary user = [[Aeroflot|Aeroflot Soviet Airlines]] 
 |more users = [[Rossiya (airline)|Rossiya]]
 |developed from =
 |variants with their own articles = [[Ilyushin Il-38]]
}}
|}

The '''Ilyushin Il-18''' ([[NATO reporting name]]: '''Coot''') is a large [[turboprop]] [[airliner]] that first flew in 1957 and became one of the best known and durable Soviet aircraft of its era. The Il-18 was one of the world's principal airliners for several decades and was widely exported. Due to the aircraft's airframe durability, many examples achieved over 45,000 flight hours and the type remains operational in both military and (to a lesser extent) civilian capacities. The Il-18's successor was the long range [[Il-62]] jet airliner.

==Design and development==
Two [[Soviet Union|Soviet]] aircraft shared the designation '''Ilyushin Il-18'''. The first Il-18 was a propeller-driven airliner of 1946 but after a year of test flights that programme was abandoned.

In the early 1950s a need to replace older designs and increase the size of the Soviet civil transport fleet, a Soviet Council of Ministers directive was issued on 30 December 1955 to the chief designers Kuznetsov and Ivchenko to develop new turboprop engines and to Ilyushin and Antonov to design an aircraft to use these engines.<ref name="aerofax1">Gordon and Komissarov 2004, pp. 7-9</ref> The two aircraft designs were developed as the Ilyushin Il-18 and the [[Antonov An-10]] and the engine chosen was the [[Kuznetsov NK-4]] rather than the [[Ivchenko AI-20]].<ref name="aerofax1" />

[[File:Aeroflot Ilyushin Il-18 McKnight.jpg|thumb|[[Aeroflot]] Il-18V at [[Prestwick Airport]], 1960s]]

The Il-18 design had started in 1954 before the directive was issued and experience with the piston-engined Il-18 was used although the aircraft was a new design.<ref name="aerofax1" /> The design was for a four-engined low-wing monoplane with a circular pressurised fuselage and a conventional tail.<ref name="aerofax1" /> The forward retracting tricycle landing gear had four wheels fitted on the main leg bogies, the main legs bogies rotated 90 degrees and retracted into the rear of the inboard engines.<ref name="aerofax1" /> A new feature at the time was the fitting of a weather radar in the nose and it was the first soviet airliner to have an automatic approach system.<ref name="aerofax1" />  The aircraft has two entry doors on the port-side before and after the wing and two overwing emergency exits on each side.<ref name="aerofax1" />

The prototype SSSR-L5811 was rolled out in June 1957 and after ground-testing it began taxi test and high-speed runs on 1 July 1957.<ref name="aerofax1" /> On 4 July 1957 the prototype first flew from [[Khodynka Aerodrome|Khodynka]].<ref name="aerofax1" /> On 10 July 1957 the aircraft was flown to [[Vnukovo International Airport|Moscow-Vnukovo Airport]] to be presented to a Soviet government commission; also present was the prototype [[Antonov An-10]] and the [[Tupolev Tu-114]].<ref name="aerofax1" /> The Il-18 type was formally named ''Moskva'' and this was painted on the fuselage, although the name was not used when the aircraft entered production.<ref name="aerofax1" />

The Moscow Machinery Plant No. 30 located at Khodynka, near where the Ilyushin design office and the prototype had been built, was chosen to manufacture the aircraft.<ref name="aerofax2">Gordon and Komissarov 2004, pp. 13-49</ref> During 1957 the plant began to reduce its production of the [[Ilyushin Il-14]] and prepare to build the production aircraft designated '''IL-18A'''.<ref name="aerofax2" /> The Il-18A was only different from the prototype in minor details, mainly internal configuration to increase the seating from 75 to 89.<ref name="aerofax2" />

[[File:Malev Ilyushin Il-18 extreme old livery Soderstrom.jpg|left|thumb|[[Malev]] Il-18 in Sweden, 1972]]
The first production aircraft were powered by the [[Kuznetsov NK-4]] but the engines were plagued with problems so the Council of Ministers decreed in July 1958 that all production from November 1958 would use the [[Ivchenko AI-20]] and earlier production would be re-engined.<ref name="aerofax2" /> Only 20 IL-18As were built before production changed to the improved '''Il-18B''', this new variant had a higher gross weight and the nose was re-designed with a larger radome which increased the length by 20&nbsp;cm.<ref name="aerofax2" />  The first Il-18B flew on 30 September 1958 powered by the AI-20; a VIP variant was also built as the IL-18S for the Soviet Air Force.<ref name="aerofax2" /> From April 1961 a TG-18 [[Auxiliary Power Unit]] was fitted for ground starting rather than the bank of lead-acid batteries. Some aircraft were modified to allow the APU to be run in flight.<ref name="aerofax2" />

With experience of the earlier aircraft a further improvement was the Il-18V variant.<ref name="aerofax2" /> The Il-18V was structurally the same but the interior was re-designed including moving the galley and some minor system changes.<ref name="aerofax2" /> The first Il-18V appeared in December 1959 and was to continue into production until 1965 after 334 had been built.<ref name="aerofax2" /> Specialised variants of the aircraft also appeared, including aircraft modified for flight calibration and a long-range polar variant.<ref name="aerofax2" /> Military variants also appeared including the anti-submarine [[Ilyushin Il-38]].<ref name="aerofax2" />

==Operational history==
[[File:Ilyushin Il-18D YR-IML Tarom RWY 25.06.88 edited-2.jpg|thumb|Ilyushin Il-18D of [[TAROM]] at [[Manchester Airport]] in 1988]]
The first Il-18, initially equipped with [[Kuznetsov Design Bureau|Kuznetsov NK-4 engines]], flew on 4 July 1957. On 17 September 1958 the aircraft first flew with the new [[Ivchenko AI-20]] engines. [[Vladimir Kokkinaki]] was the test pilot. Between 1958 and 1960 twenty-five world records were set by this aircraft, among them flight range and altitude records with various payloads. In 1958 the aircraft was awarded the [[Expo '58|Brussels World Fair]] Grand Prix. In April 1979 a monument was unveiled at [[Sheremetyevo International Airport|Sheremetyevo airport]] to commemorate this remarkable aircraft.

Seventeen foreign air carriers acquired some 125 Il-18 aircraft, seating 100-120 passengers. Il-18s are still in service in [[Siberia]], [[Air Koryo|North Korea]] and the [[Middle East]], whilst a number of examples manufactured in the mid-1960s were still in civilian use in Africa and south Asia as at 2014. The type operates in various military capacities, including the Il-22PP jamming and reconnaissance aircraft (entered service in October 2016).<ref>http://www.ilyushin.org/press/news/ev6273/</ref>

An Il-18 (registration DDR-STD) belonging to [[Interflug]] and used as a transport by East German leaders, including [[Erich Honecker]], has been converted into a static hotel suite in The Netherlands.<ref>[http://www.thelocal.de/society/20111007-38007.html The Local, 7th October 2011]</ref>

==Variants==
[[File:IL-18-700px.jpg|thumb|Il-18 on display at [[Sheremetyevo International Airport]]]]
[[File:Hans-Grade-Museum 03.jpg|thumb|An example at a museum in [[Borkheide]], Germany]]
[[File:Ilyushin Il-20M (2).jpg|thumb|An Il-20 in 2009]]
[[File:MALEV IL-18.JPG|thumb|[[Malev]] Il-18 in at an open-air aircraft museum at the [[Budapest Ferihegy International Airport]]]]
[[File:Djibintairp.jpg|thumb|right|An Il-18 at the [[Djibouti–Ambouli International Airport]]]]
''Data from:OKB Ilyushin<ref name="GordonILY">{{cite book|last=Gordon|first=Yefim|author2=Dmitry Komissarov|others=Sergey Komissarov|title=OKB Ilyushin|publisher=Midland publishing|location=Hinkley|year=2004|edition=1st|pages=193–251|isbn=1-85780-187-3}}</ref>
;Il-18
: Designation of the sole prototype of the Il-18 family.
;Il-18A
: The original production model, equivalent to pre-production, powered by either [[Kuznetsov NK-4]] or [[Ivchenko AI-20]] turboprop engines. Circa 20 built.
;Il-18B
: First major production model, a medium-haul airliner that could seat 84 passengers.
;Il-18 Combi
:Il-18 aircraft modified to mixed passenger / cargo configuration
;Il-18D
: Similar to Il-18I, but equipped with an extra centre section fuel tank for increased range. The Il-18D is fitted with four 3,169&nbsp;ekW (4,250&nbsp;hp) Ivchenko AI-20M turboprop engines.
;Il-18D communications relay
: Three aircraft modified to provide communications relay between VIP aircraft and Government bodies.
;Il-18D Pomor
: A single Il-18D converted to a fisheries reconnaissance aircraft, (''Pomor'' - person who lives by the sea)
;Il-18D Salon
: VIP version of the Il-18D
;IL-18DORR
:Two IL-18Ds modified as fishery reconnaissance aircraft for the Polar Institute of Oceanic Fishery and Oceanography, the modification mainly involved the fitment of specialised mission equipment. First flown in 1985 they were later modified back as standard Il-18Ds.
;Il-18E
: Similar to the Il-18I, but without the increased fuel capacity.
;Il-18E Salon
: VIP transport version of the Il-18E.
;Il-18Gr
: Aircraft converted to cargo configuration, (Gr - ''Gruzovoy'' - cargo).
;Il-18GrM
: Several Il-18 aircraft modified to Gr standard with the addition of a pressurised side cargo door.
;Il-18I
: Equipped with more powerful Ivchenko AI-20M turboprop engines, producing 3,170&nbsp;kW (4,250&nbsp;shp). Seating increased to 122 passengers in an enlarged cabin gained by moving the aft pressure bulkhead rearwards by {{convert|1.64|m|ft|abbr=on|0}}.
;Il-18LL
:(''Letayuschchaya Laboratoriya'' - flying laboratory), one aircraft modified to be an anti-icing test-bed and an Il-18V used by the Czechoslovak flight test centre as an engine testbed.
;IL-18RT
:Two IL-18Vs were modified as Telemetry Relay Aircraft to rocket and unmanned air vehicle trials.
;Il-18S
: VIP variant of Il-18B
;Il-18T
: This designation was given to civil and military cargo transport aircraft converted from Il-18A/B/V aircraft.
::'''Il-18AT:''' Military transport/casevac version based on the Il-18A
::'''Il-18BT:''' Military transport/casevac version based on the Il-18B
::'''Il-18VT:''' Military transport/casevac version based on the Il-18V
;IL-18TD
:One IL-18T was modified as a military transport variant to take either 69 stretcher cases or 118 paratroopers. Not wanted by the military it was converted to IL-18D standard.
;IL-18USh
: One IL-18V was modified as a navigator trainer including two dorsal astro-sextant windows. Although it was tested and found acceptable the Soviet Air Force used a variant of the twin-jet [[Tupolev Tu-124]] instead.
;Il-18V
: Standard Aeroflot version, which entered service in 1961. The Il-18V was powered by four [[Ivchenko AI-20K]] turboprop engines, seating 90-100 passengers.
;Il-18V Salon
: VIP version of the Il-18V
;Il-18V/polar
: a single Il-18V modified for ''[[Polyarnaya Aviahtsiya]]'' - Polar Aviation use.
;Il-18V-26A
: a single Il-18V modified for ''[[Polyarnaya Aviahtsiya]]'' - Polar Aviation use with an auxiliary fuel tank in the cabin, revised window layout and enlarged oil tanks on the engines, covered by protruding fairings on the engine nacelles (sometimes referred to as the Il-18D, before the real D model emerged).
;Il-18V calibrator
: a single Il-18V operated by [[Interflug]] for navaid calibration.
[[File:Russian Air Force Ilyushin Il-20 Naumenko-1.jpg|thumb|Russian Air Force Ilyushin Il-20]]
;Il-20M Coot-A
: [[ELINT]] electronic, radar reconnaissance version. Also known as the '''Il-18D-36 Bizon'''.
;Il-20RT
: Four Telemetry and Communications Relay aircraft used to support the Soviet space activities, later replaced by a variant of the IL-76.
;Il-22 Coot-B
: Airborne command post version.
;Il-22M
: Same as the Il-22 but had new mission equipment.
;Il-24N
: Two Il-18Ds modified for ice reconnaissance similar to the IL-20M but with civilian reconnaissance equipment, both later modified back to standard configuration and sold.
;[[Ilyushin Il-38|Il-38]]
: Maritime reconnaissance, [[anti-submarine warfare]] version.
;SL-18
:Designation of a number of different test and research aircraft, normally had a letter suffix like SL-18D for avionics trials.
;Il-118
:A proposed upgrade powered by two [[Lotarev D-236-T]] propfan engines.

==Operators==
[[File:IL-18 Pyongyang to Samjiyon.jpg|thumb|230px|IL-18 of [[North Korea]]n national airline [[Air Koryo]].]]
[[File:Rossiya Ilyushin Il-18 Ates-1.jpg|thumb|230px|IL-18 of  [[Rossiya (airline)|Rossiya Airlines]].]]

===Civil operators===

====Current operators====
;{{RUS}}
* [[NPP MIR (ChK Leninets)]] -  1 in use
;{{flag|North Korea}}
* [[Air Koryo]] - 1 in use

====Former operators====
;{{AFG}}
* [[Ariana Afghan Airlines]]
;{{PRC}}
* [[Civil Aviation Administration of China]]
;{{flag|Bulgaria|1950}}
* [[Balkan Bulgarian Airlines|Balkan Airlines]]
;{{CUB}}
* [[Aerocaribbean]]
* [[Cubana]]
;{{CZS}}
* [[Czechoslovak Airlines]]
;{{DJI}}
* [[Daallo Airlines]]
;{{GER}}
* [[Berline (airline)|Berline]]
* [[German European Airlines]]
;{{GDR}}
* [[Deutsche Lufthansa (East Germany)|Deutsche Lufthansa]]
* [[Interflug]] (16 operated)<ref>Michał Petrykowski, ''Samoloty Ił-18 Lufthansy'', Lotnictwo Nr. 12/2009, p.20 {{pl icon}}</ref>
;{{EGY}}
* [[Egyptair]] (formerly United Arab Airlines)
;{{GHA}}
* [[Ghana Airways]] (8 purchased, 4 later returned to the USSR)
;{{GUI}}
* [[Air Guinée]]
;{{HUN}}
* [[Malév Hungarian Airlines]]
;{{KAZ}}
* [[Irbis Aero]]
;{{KGZ}}
* [[Anikay Air]]
;{{MLI}}
* [[Air Mali (1960-1989)|Air Mali]]
;{{POL}}
[[File:LOT Ilyushin Il-18 Osta-1.jpg|thumb|230px|Il-18E of [[LOT Polish Airlines]] in [[Warsaw]]. (1990)]]
* [[LOT Polish Airlines|LOT]] (10 operated from 1961 until the early 1990s<ref>[http://www.polrail.com/Aerolot/fleet/fleet_history.html AeroLOT Fleet history]</ref>)
* [[Polonia Airways]] (1 operated in 90s)
* [[Polnippon]] (3 operated from 1990 until 1996)
;{{flag|Romania|1952}}
* [[Tarom]]
;{{USSR}}
* [[Aeroflot]]
;{{RUS}}
* [[ASTAir]]
* [[Domodedovo Airlines]]
* [[GVG Airline]]
* [[Rossiya (airline)|Rossiya]] 
* [[Tretyakovo Airlines]]
;{{SOM}}
* [[Daallo Airlines]]
* [[Jubba Airways]]
;{{SRI}}
* [[Expo Aviation]]
;{{UKR}}
* [[Lviv Airlines]]
* [[Sevastopol Avia]]
;{{UAE}}
* [[Phoenix Aviation]]
;{{VNM}}
* [[Vietnam Airlines]]
;{{YEM}}
* [[Yemen Airways]]

===Military operators===
'''<big>Current operators:</big>'''
;{{PRK}}
*[[North Korean Air Force]] - 1 leased from [[Air Koryo]] 
;{{RUS}}
* [[Russian Air Force]] - Il-20 and Il-22PP versions<ref>http://vpk-news.ru/news/33210</ref><ref>https://news.rambler.ru/articles/36077812-aviatsiya-flota-poluchila-zavisayuschuyu-bombu/</ref>
'''<big>Former operators:</big>'''
;{{AFG}}
* [[Afghan Air Force]] (Five were delivered in 1968, and have since been retired.)
;{{ALG}}
*[[Algerian Air Force]]
;{{flag|Bulgaria|1950}}
* Government of Bulgaria
;{{CZS}}
* [[Czechoslovakian Air Force]]
;{{GDR}}
* [[Air Forces of the National People's Army|East German Air Force]]
;{{GEO|1990}}
*[[Georgian Air Force]]
;{{INA}}
*Government of Indonesia (Presidential aircraft)
;{{flag|North Yemen}}
* North Yemen Air Force
;{{POL}}
* [[Polish Air Force]] (VIP Transport, replaced by [[Tu-134]])
;{{flag|Romania|1952}}
* Government of Romania
;{{USSR}}
* [[Soviet Air Forces]]
* [[Soviet Naval Aviation]]
;{{flag|Vietnam}}
*[[Vietnam People's Air Force]]
;{{YUG}}
*[[SFR Yugoslav Air Force]]

==Accidents and incidents==
{{main article|List of accidents and incidents involving the Ilyushin Il-18}}

==Specifications (Il-18D)==
[[File:Il-18V.svg|thumb|250px|Layout of Il-18]]
{{Aircraft specs
|ref=OKB Ilyushin<ref name="GordonILY"/><!-- reference -->
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification,
met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|crew=9 
|capacity=65-120 passengers
|length m=35.9
|span m=37.4
|height m=10.165
|wing area sqm=140
|empty weight kg=35,000
|max takeoff weight kg=64,000
|fuel capacity={{convert|30,000|l|impgal|abbr=on|0}}
|more general=<br/>
* '''Fuselage diameter:''' {{convert|3.5|m|ft|abbr=on|0}}
* '''Max. landing weight:''' {{convert|52,600|kg|lb|abbr=on|0}}
* '''Max. zero-fuel weight:''' {{convert|48,800|kg|lb|abbr=on|0}}
* '''Max. taxi weight :''' {{convert|64,500|kg|lb|abbr=on|0}}
<!--
        Powerplant
-->
|eng1 number=4
|eng1 name=[[Ivchenko AI-20M]]
|eng1 type=axial flow turboprop engines
|eng1 hp=4,250<!-- prop engines -->
|more power=<br/>
** '''Auxiliary power unit:''' [[TG-16M]] (28 Volt DC)

|prop blade number=4<!-- propeller aircraft -->
|prop name=[[AW-68 I]] constant speed feathering propellers
|prop dia m=4.5<!-- propeller aircraft -->
<!--
        Performance
-->
|max speed kmh=675
|max speed mach=<!-- supersonic aircraft -->0.65
|cruise speed kmh=625
|cruise speed note= at {{convert|8,000|m|ft|abbr=on|0}}
|range km=6,500
|range note=with {{convert|6,500|kg|lb|abbr=on|0}} payload, maximum fuel and reserves for one hour.
**{{convert|3,700|km|mi|abbr=on|0}} with {{convert|13,500|kg|lb|abbr=on|0}} maximum payload, at 84 - 85% of maximum continuous power.
|ceiling m=11,800
|more performance=<br/>
* '''Approach minima:''' [[International Civil Aviation Organization|ICAO]] CAT 1 Decision Height 60 m (200 ft) / 800 m (Visibility) or 550 m [[Runway visual range|RVR]]
*'''Take-off run:''' {{convert|1,350|m|ft|abbr=on|0}}
*'''Landing run:''' {{convert|850|m|ft|abbr=on|0}}
|avionics=
**'''RPSN-2AMG:''' or '''RPSN-2N''' ''Emblema'' weather radar
**'''NAS-1B:''' autonomous navigation system
***'''DISS-1:''' doppler speed/drift sensor
***'''ANU-1:''' autonomous navigation computer
**'''Put'-4M:''' navigation system
**'''KS-6G:''' compass system
**'''DAK-DB:''' remote celestial compass
**'''RSBN-2S Svod:''' [[SHORAN]] (''Svod'' - Dome)
**'''SP-50 Materik:''' ILS
**'''RV-UM:''' radio altimeter
**'''NI-50BM-1:''' navigation display
**'''ARK-11:'''main and backup [[Automatic direction finder|ADF]] (automatic direction finder)
**'''RSB-5/1230:''' communications radio
**'''RSIU-5 (R802G):''' command link radio, 2 of.
**'''SR-2M Khrom:''' IFF transponder (''Khrom'' - Chromium)
**'''MSRP-12-96:''' flight data recorder
}}

==See also==

===Related development===
* [[Ilyushin Il-38]]

===Comparable aircraft===
* [[Antonov An-10]]
* [[Bristol Britannia]]
* [[Douglas DC-6]]
* [[Lockheed L-188 Electra]]
* [[Vickers Vanguard]]
* [[Vickers Viscount]]

===Related lists===
* [[List of civil aircraft]]

==References==

===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
*Gordon, Yefim and Komissarov, Dmitry. ''Ilyushin Il-18/-20/-22 - A Versatile Turboprop Transport''. Hinckley, Leicestershire, UK: Midland Publishing, 2004, ISBN 1-85780-157-1
*{{cite book|last=Gordon|first=Yefim|author2=Dmitry Komissarov|others=Sergey Komissarov|title=OKB Ilyushin|publisher=Midland publishing|location=Hinkley|year=2004|edition=1st|pages=193–251|isbn=1-85780-187-3}}
{{refend}}

==External links==
{{Commons|Ilyushin_Il-18}}
*[http://www.flugsimulator-il18.de/ Flight simulator Il-18 in the Museum of Technology "Hugo Junkers" in Dessau (Germany)]
*http://www.globalsecurity.org/military/world/russia/il-18.htm

{{Ilyushin aircraft}}

[[Category:Ilyushin aircraft|Il-018]]
[[Category:Soviet airliners 1950–1959]]
[[Category:Four-engined tractor aircraft]]
[[Category:Low-wing aircraft]]
[[Category:Turboprop aircraft]]