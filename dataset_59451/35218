{{good article}}
{{Infobox hurricane season
| Basin=NIO
| Year=2000
| Track=2000 North Indian Ocean cyclone season summary.jpg
| First storm formed=March 27, 2000
| Last storm dissipated=December 28, 2000
| Strongest storm name=BOB 05
| Strongest storm pressure=958
| Strongest storm winds=102
| Average wind speed=3
| Total depressions=6
| Total storms=5
| Total intense=2
| Total super=0
| Fatalities=238
| Damages=185
| five seasons=[[1998 North Indian Ocean cyclone season|1998]], [[1999 North Indian Ocean cyclone season|1999]], '''2000''', [[2001 North Indian Ocean cyclone season|2001]], [[2002 North Indian Ocean cyclone season|2002]]
|Atlantic season=2000 Atlantic hurricane season
|East Pacific season=2000 Pacific hurricane season
|West Pacific season=2000 Pacific typhoon season
}}
The '''2000 North Indian Ocean cyclone season''' was fairly quiet compared to its [[1999 North Indian Ocean cyclone season|predecessor]], with all of the activity [[tropical cyclogenesis|originating]] in the [[Bay of Bengal]]. The basin comprises the [[Indian Ocean]] north of the [[equator]], with warnings issued by the [[India Meteorological Department]] (IMD) in [[New Delhi]]. There were six [[Tropical cyclone scales#North Indian Ocean|depressions]] throughout the year, of which five intensified into cyclonic storms &ndash; [[tropical cyclone]]s with winds of 65&nbsp;mph (40&nbsp;km/h) sustained over 3&nbsp;minutes. Two of the storms strengthened into a very severe cyclonic storm, which has winds of at least 120&nbsp;km/h (75&nbsp;mph), equivalent to a minimal [[Saffir-Simpson Hurricane Wind Scale|hurricane]]. The [[Joint Typhoon Warning Center]] (JTWC) also tracked storms in the basin on an unofficial basis, estimating winds sustained over 1&nbsp;minute.

The first storm of the season originated toward the end of March in the Bay of Bengal, one of only five March storms at the time in that body of water. Strong [[wind shear]], which plagued several storms during the season, caused the storm to rapidly dissipate over open waters. In August, a weak depression struck the Indian state of [[Andhra Pradesh]], producing additional flooding after a deluge affected the area in July. There were 131&nbsp;deaths in Andhra Pradesh, mostly by drownings or collapsed walls, while damage was estimated at ₹7.76&nbsp;billion [[Indian rupee|rupees]] ($170&nbsp;million [[United States dollar|USD]]).{{refn|All damage totals are valued as of 2000 and originally reported in [[Indian rupee]]s. Totals converted to [[United States dollar]]s via the [[Oanda Corporation]] website.<ref name="Historical Exchange Rates">{{cite web|publisher=Oanda Corporation|year=2015|title=Historical Exchange Rates|accessdate=2015-07-30|url=http://www.oanda.com/currency/historical-rates/}}</ref>|group="nb"}} There were two short-lived storms in October &ndash; one dissipated offshore India in the middle of the month, and the other struck [[Bangladesh]] toward the end of the month. The latter storm destroyed many homes and boats, killing 77 in Bangladesh including 52&nbsp;fishermen, and damage in the Indian state of [[Meghalaya]] was estimated at ₹600 million rupees ($13 million USD). The strongest storm of the season struck [[Tamil Nadu]] in November, causing damages of ₹700&nbsp;million rupees ($15&nbsp;million USD) and 12&nbsp;deaths. The [[2000 Sri Lanka cyclone|final storm of the season]] hit eastern [[Sri Lanka]], leaving 500,000&nbsp;homeless and killing nine.

__TOC__
{{clear}}

==Season summary==
<center>
<timeline>
ImageSize = width:781 height:175
PlotArea  = top:10 bottom:80 right:50 left:20
Legend    = columns:3 left:30 top:58 columnwidth:270
AlignBars = early
DateFormat = dd/mm/yyyy
Period     = from:01/03/2000 till:01/01/2001
TimeAxis   = orientation:horizontal
ScaleMinor = grid:black unit:month increment:1 start:01/03/2000
Colors     =
  id:canvas value:gray(0.88)
  id:GP     value:red
  id:TD     value:rgb(0.5,0.8,1)      legend:Depression
  id:DD     value:rgb(0.37,0.73,1)    legend:Deep_Depression
  id:TS     value:rgb(0,0.98,0.96)    legend:Cyclonic_Storm
  id:ST     value:rgb(0.8,1,1)        legend:Severe_Cyclonic_Storm
  id:VS     value:rgb(1,1,0.8)        legend:Very_Severe_Cyclonic_Storm
  id:ES     value:rgb(1,0.76,0.25)    legend:Extremely_Severe_Cyclonic_Storm
  id:SU     value:rgb(1,0.38,0.38)    legend:Super_Cyclonic_Storm
Backgroundcolors = canvas:canvas
BarData =
  barset:Hurricane
  bar:Month
PlotData=
  barset:Hurricane width:10 align:left fontsize:S shift:(4,-4) anchor:till
  from:27/03/2000 till:30/03/2000 color:TS text:"BOB 01"
  from:23/08/2000 till:24/08/2000 color:TD text:"BOB 02"
  from:15/10/2000 till:19/10/2000 color:TS text:"BOB 03"
  barset:break
  from:25/10/2000 till:29/10/2000 color:TS text:"BOB 04"
  from:26/11/2000 till:30/11/2000 color:ES text:"BOB 05"
  from:23/12/2000 till:28/12/2000 color:ES text:"BOB 06"
 bar:Month width:5 align:center fontsize:S shift:(0,-20) anchor:middle color:canvas
  from:01/03/2000 till:31/03/2000 text:March
  from:01/04/2000 till:30/04/2000 text:April
  from:01/05/2000 till:31/05/2000 text:May
  from:01/06/2000 till:30/06/2000 text:June
  from:01/07/2000 till:31/07/2000 text:July
  from:01/08/2000 till:31/08/2000 text:August
  from:01/09/2000 till:30/09/2000 text:September
  from:01/10/2000 till:31/10/2000 text:October
  from:01/11/2000 till:30/11/2000 text:November
  from:01/12/2000 till:31/12/2000 text:December
</timeline>
</center>

The [[India Meteorological Department]] (IMD) in [[New Delhi]] &ndash; the official [[Regional Specialized Meteorological Center]] for the northern [[Indian Ocean]] as recognized by the [[World Meteorological Organization]] &ndash; issued warnings for tropical cyclones developing in the region, using satellite imagery and surface data to assess and predict storms. The basin's activity is sub-divided between the [[Arabian Sea]] and the [[Bay of Bengal]] on opposite coasts of India, and is generally split before and after the [[monsoon]] season.<ref name="wmo">{{cite report|publisher=World Meteorological Organization|title=Annual Summary of the Global Tropical Cyclone Season 2000|accessdate=2015-05-22|url=http://www.wmo.int/pages/prog/www/tcp/documents/pdf/TD1082-TCseason2000.pdf|format=PDF}}</ref> Storms were also tracked on an unofficial basis by the American-based [[Joint Typhoon Warning Center]].<ref name="atcr">{{cite report|author=Joint Typhoon Warning Center|publisher=United States Navy|accessdate=2012-07-25|title=Annual Tropical Cyclone Report|url=http://www.usno.navy.mil/NOOC/nmfc-ph/RSS/jtwc/atcr/2000atcr.pdf|format=PDF}}</ref>

The season was much less active than the devastating [[1999 North Indian Ocean cyclone season|1999 season]]. Despite near normal water temperatures over the [[Arabian Sea]], no storms developed in that portion of the basin. Convection was also lower than normal across the Bay of Bengal. The main factor against [[tropical cyclogenesis]] was persistently unfavorable [[wind shear]]. Overall, there were six depressions, five of which intensified into a [[Tropical cyclone scales#North Indian Ocean|cyclonic storm]], which has [[maximum sustained wind]]s of at least 65&nbsp;km/h (40&nbsp;mph).<ref name="imd"/>
{{clear}}

==Systems==

===Cyclonic Storm BOB 01===
{{storm path needed}}
{{Infobox Hurricane Small
|Basin=NIO
|Image=Tropical Depression 29 mar 2000 0824Z.jpg
|Formed=March 27
|Dissipated=March 30
|3-min winds=45
|1-min winds=50
|Pressure=998
}}
Toward the end of March, an area of [[convection (meteorology)|convection]] increased over the southern Bay of Bengal from an active [[intertropical convergence zone|equatorial trough]]. The system progressed northward,<ref name="imd">{{cite report|title=Report on Cyclonic Disturbances Over North Indian Ocean During 2000|publisher=India Meteorological Department|date=February 2001|accessdate=2015-05-22|url=http://www.rsmcnewdelhi.imd.gov.in/images/pdf/archive/rsmc/2000.pdf|format=PDF}}</ref> with a weak center between [[Sri Lanka]] and [[Sumatra]] by March&nbsp;25.<ref name="mgp">{{cite web|author=Gary Padgett|year=2000|accessdate=2015-05-16|title=Monthly Tropical Weather Summary for March 2000|url=http://australiasevereweather.com/cyclones/2000/summ0003.htm}}</ref> On March&nbsp;27, a [[low pressure area]] developed, which the IMD designated as a depression by 12:00&nbsp;[[Coordinated Universal Time|UTC]]. The storm moved to the north-northwest and failed to strengthen at first.<ref name="imd"/> However, the JTWC issued a [[Tropical Cyclone Formation Alert]] (TCFA) on March&nbsp;29, a signal of further organization.<ref name="mgp"/> That day, the IMD upgraded the system to a cyclonic storm,<ref name="imd"/> and early on March&nbsp;30 the storm attained winds of 85&nbsp;km/h (50&nbsp;mph) while curving to the north-northeast.<ref name="mbt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=2001 Missing (2000088N08090)|publisher=Bulletin of the American Meteorological Society|accessdate=2015-05-22|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-2000088N08090}}</ref> Increased wind shear from the [[westerlies]] imparted rapid weakening, causing the convection to dwindle to the northeast. According to the IMD, the storm rapidly dissipated on March&nbsp;30.<ref name="imd"/><ref name="mgp"/>

A climatological outlier, the storm was one of only five cyclonic storms at the time in the month of March in the Bay of Bengal. It dropped heavy rainfall in the [[Andaman and Nicobar Islands]], reaching {{convert|230|mm|in|abbr=on}} on [[Little Andaman|Hut Bay]].<ref name="imd"/> Although the storm dissipated over the Bay of Bengal according to the IMD, one analysis suggested the storm re-intensified and made [[landfall (meteorology)|landfall]] on southeastern India between [[Chennai]] and [[Pondicherry]] on April&nbsp;1 with winds potentially as high as 110&nbsp;km/h (70&nbsp;mph).<ref name="mgp"/> The storm did not receive advisories from the JTWC.<ref name="atcr"/>
{{clear}}

===Depression BOB 02===
{{Infobox Hurricane Small
|Basin=NIO
|Formed=August 23
|Dissipated=August 24
|3-min winds=25
|Pressure=994
}}
In late August, the [[monsoon trough]] spawned a series of disturbances in the Bay of Bengal, including one that developed on August&nbsp;19 off the [[Odisha]] coast. It persisted and gradually organized,<ref name="agp">{{cite web|author=Gary Padgett|year=2000|accessdate=2015-05-22|title=Monthly Tropical Weather Summary for August 2000|url=http://australiasevereweather.com/cyclones/2001/summ0008.htm}}</ref> becoming a well-marked low pressure area on August&nbsp;22. On the following day, the system became a depression,<ref name="imd"/> located about 150&nbsp;km (90&nbsp;mi) south-southeast of [[Visakhapatnam]], [[Andhra Pradesh]].<ref name="agp"/> Moving westward, the system soon moved ashore near [[Kakinada]] without intensifying beyond winds of 45&nbsp;km/h (30&nbsp;mph), and quickly weakened into a remnant low on August&nbsp;24. The low continued westward, eventually dissipating over [[Gujarat]] on August&nbsp;28.<ref name="imd"/>

While moving ashore, the depression produced torrential rainfall across Andhra Pradesh. The capital city of [[Hyderabad]] recorded {{convert|240|mm|in|abbr=on}} of rainfall on August&nbsp;24.<ref name="imd"/> During the last week of August, the state recorded the highest precipitation in 46&nbsp;years, which overflowed lakes and flooded several towns. The rains followed deadly flooding in July and preceded another flood event in September.<ref name="unicef">{{cite report|work=U.N.'s Children Fund|date=2000-09-28|title=UNICEF Report on the Drought and Floods in India 28 Sep 2000|publisher=ReliefWeb|accessdate=2015-05-22|url=http://reliefweb.int/report/india/unicef-report-drought-and-floods-india-28-sep-2000}}</ref> About 98,000&nbsp;people evacuated their houses to 189&nbsp;shelters,<ref name="imd"/> aided by the military, including about 35,000&nbsp;people in Hyderabad and neighboring [[Secunderabad]]. Thousands were forced to ride out the floods on their roofs, and helicopters airdropped food and relief goods.<ref name="unicef"/> The depression damaged 27,026&nbsp;houses and destroyed another 8,651 in 2,886&nbsp;towns or villages. Widespread irrigation systems were damaged, and {{convert|177987|ha|acre|abbr=on}} of crops were lost, in addition to 5,368&nbsp;killed cattle. Traffic was disrupted after {{convert|7435|km|mi|abbr=on}} of roads were damaged, impacting 2,389&nbsp;roads. The rains also marred the electrical system, with 6,000&nbsp;power lines damaged. There were 131&nbsp;deaths in Andhra Pradesh, mostly by drownings or collapsed walls, while damage was estimated at ₹7.76&nbsp;billion [[Indian rupee|rupees]] ($170&nbsp;million [[United States dollar|USD]]).<ref name="imd"/> 
{{Clear}}

===Cyclonic Storm BOB 03===
{{Infobox Hurricane Small
|Basin=NIO
|Image=Tropical Cyclone 01B 16 oct 2000 0939Z.jpg
|Track=Cyclone 01B 2000 track.png
|Formed=October 15
|Dissipated=October 19
|3-min winds=35
|1-min winds=35
|Pressure=996
}}
The active monsoon spawned a low pressure area in the central Bay of Bengal on October&nbsp;12.<ref name="imd"/> The system had an area of convection about 925&nbsp;km (575&nbsp;mi) southeast of [[Kolkata]], which moved slowly westward. By October&nbsp;14, there was an exposed circulation center east of the convection,<ref name="ogp">{{cite web|author=Gary Padgett|year=2000|accessdate=2015-05-22|title=Monthly Tropical Weather Summary for October 2000|url=http://australiasevereweather.com/cyclones/2001/summ0010.htm}}</ref> although it organized enough for the IMD to classify it as a well-marked low pressure area. On October&nbsp;15, the agency classified it as a depression as the circulation moved closer to the thunderstorms.<ref name="imd"/> Later that day, the JTWC issued a TCFA and the IMD upgraded it to a deep depression, based on improving [[outflow (meteorology)|outflow]] and organization.<ref name="ogp"/> On October&nbsp;16, a nearby ship reported winds of 65&nbsp;km/h (40&nbsp;mph),<ref name="imd"/> and that day the JTWC began tracking the system as Tropical Cyclone 01B.<ref name="ogp"/> Early the next day, the IMD followed suit and upgraded the deep depression to a cyclonic storm, estimating peak winds of 65&nbsp;km/h (40&nbsp;mph).<ref name="imd"/> However, the system persisted in an area of weak to moderate wind shear, preventing further development. The circulation became exposed from the convection, and the wind shear increased. On October&nbsp;18, the IMD downgraded the system to a deep depression, and the storm dissipated on the next day as it approached the eastern coast of India.<ref name="imd"/><ref name="ogp"/>

While the storm was active, officials issued warnings for fishermen not to venture out at sea. In Odisha, residents organized public prayers in hopes of avoiding a repeat of the deadly [[1999 Odisha cyclone]].<ref>{{cite news|date=2000-10-16|title=Cyclone threatens Indian states a year after killer storm|publisher=ReliefWeb|agency=Agence France-Presse|accessdate=2015-05-23|url=http://reliefweb.int/report/india/cyclone-threatens-indian-states-year-after-killer-storm}}</ref> Although it dissipated offshore, the storm brought rainfall to Andhra Pradesh and Odisha, but no damage was reported.<ref name="imd"/> Strong winds associated with the system killed 100&nbsp;pelican chicks in [[Srikakulam]] after blowing them out of their nests.<ref name="ogp"/>
{{clear}}

===Cyclonic Storm BOB 04===
{{Infobox Hurricane Small
|Basin=NIO
|Image=Tropical Cyclone 02B 26 oct 2000 0922Z.jpg
|Track=Cyclone 02B 2000 track.png
|Formed=October 25
|Dissipated=October 29
|3-min winds=35
|1-min winds=35
|Pressure=998
}}
Similar to the previous storm, the active monsoon trough spawned a low pressure area over the [[Andaman Sea]] on October&nbsp;24. There was a weak center that had good outflow. On October&nbsp;25, the IMD classified the system as a depression about 925&nbsp;km (575&nbsp;mi) southeast of Kolkata. The system moved to the northwest and developed more convection close to the center, although the thunderstorms were intermittent. Turning more to the north, the depression intensified into a deep depression and later cyclonic storm on October&nbsp;27, reaching peak winds of 65&nbsp;km/h (40&nbsp;mph); the JTWC also classified it as Tropical Cyclone 02B. That day, the wind shear increased, although the convection was able to increase over the center and organize into a comma-shaped [[rainband]]. Early on October&nbsp;28, the storm made landfall in southern [[Bangladesh]] near [[Mongla Upazila|Mongla]], by which time the wind shear had displaced much of the convection to the northeast. It rapidly weakened over land, degenerating into a remnant low over northern Bangladesh early on October&nbsp;29.<ref name="imd"/><ref name="ogp"/>

The storm dropped heavy rainfall, both in the Andaman and Nicobar Islands as well as northeastern India. In [[Meghalaya]] state in northeastern India, the storm damaged hundreds of houses, leaving thousands homeless. Many livestock were lost, and crops were decimated. Damage in Meghalaya was estimated at ₹600&nbsp;million rupees ($13&nbsp;million USD).<ref name="imd"/> While moving ashore in Bangladesh, the cyclone produced a [[storm tide]] of 1.2&ndash;2.1&nbsp;m (4&ndash;7&nbsp;ft), which wrecked hundreds of boats,<ref name="ocha"/> and left 100&nbsp;fishermen missing despite forewarning;<ref name="dpa"/> by a day after the storm, only eight fishermen were rescued from four boats, with 52&nbsp;fishermen killed.<ref name="bbc">{{cite news|publisher=BBC|title=Death toll rises in Bangladesh storms|accessdate=2015-05-22|date=2000-10-29|url=http://news.bbc.co.uk/2/hi/south_asia/995348.stm}}</ref> Heavy rainfall, totaling {{convert|119|mm|in|abbr=on}} in [[Khulna]], overflowed rivers and flooded houses after previous deadly flooding in September.<ref name="dpa"/> High floods and wind gusts up to 100&nbsp;km/h (60&nbsp;mph) damaged homes in [[Satkhira District|Satkhira]] and [[Jessore District|Jessore]] districts,<ref name="ocha"/> forcing thousands to evacuate to storm shelters.<ref name="dpa">{{cite news|agency=Deutsche Presse Agentur|title=25 dead, hundreds missing in Bangladesh cyclone|date=2000-10-29|work=ReliefWeb|accessdate=2015-05-22|url=http://reliefweb.int/report/bangladesh/25-dead-hundreds-missing-bangladesh-cyclone}}</ref> The storm knocked over trees, wrecked roads, and destroyed rice fields along its path through the low-lying country. On land in Bangladesh, 25&nbsp;people died due to the storm, with over 500&nbsp;injured.<ref name="dpa"/> After the storm, local governments provided relief goods to the worst affected areas.<ref name="ocha">{{cite report|work=United Nations Office for the Coordination of Humanitarian Affairs|date=2000-10-29|title=Bangladesh - Tropical Cyclonic Storm OCHA Situation Report No. 1|publisher=ReliefWeb|accessdate=2015-05-22|url=http://reliefweb.int/report/bangladesh/bangladesh-tropical-cyclonic-storm-ocha-situation-report-no-1}}</ref>
{{clear}}

===Extremely Severe Cyclonic Storm BOB 05===
{{Infobox Hurricane Small
|Basin=NIO
|Image=Tropical Cyclone 03B 28 nov 2000 0934Z.jpg
|Track=Cyclone 03B 2000 track.png
|Formed=November 26
|Dissipated=November 30
|3-min winds=102
|1-min winds=115
|Pressure=958
}}
An [[cold-core low|upper-level low]] persisted over the Andaman Sea on November&nbsp;24.<ref name="imd"/> By the next day, a circulation center was present about 370&nbsp;km (230&nbsp;mi) west of Thailand, although convection was dislocated to the west due to wind shear.<ref name="ngp">{{cite web|author=Gary Padgett|year=2000|accessdate=2015-05-22|title=Monthly Tropical Weather Summary for November 2000|url=http://australiasevereweather.com/cyclones/2001/summ0011.htm}}</ref> After the thunderstorms concentrated over the center early on November&nbsp;26, the IMD classified the system as a depression.<ref name="imd"/> A [[ridge (meteorology)|ridge]] to the north steered the system generally westward. Outflow and convective organization gradually increased, and late on November&nbsp;26 the JTWC classified it as Tropical Cyclone 03B. As the rainbands organized around the center, the winds increased; the IMD upgraded the system to a cyclonic storm on November&nbsp;27, and to a severe and later a very severe cyclonic storm on November&nbsp;28.<ref name="imd"/><ref name="ogp"/>

By November&nbsp;28, a 20&nbsp;km (12&nbsp;mi) wide [[eye (cyclone)|eye]] was developing, prompting the JTWC to upgrade the storm to the equivalent of a minimal hurricane with winds of 120&nbsp;km/h (75&nbsp;mph). By comparison, the IMD estimated peak winds of 190&nbsp;km/h (115&nbsp;mph). Wind shear in the region prevented further strengthening, and the storm weakened slightly before making landfall on November&nbsp;29 in eastern India near [[Cuddalore]]. A station there recorded a pressure of {{convert|983|mbar|inHg|abbr=on}}. The storm rapidly weakened over land, and degenerated into a remnant low on November&nbsp;30. The remnants emerged into the eastern Arabian Sea on December&nbsp;1, by which time most thunderstorms had dissipated over the deteriorating center. Two days later, the JTWC reissued advisories, based on an increase in outflow and convective organization. This was short-lived, as the thunderstorms soon dwindled, and the JTWC ceased issuing advisories on December&nbsp;5. The remnants continued westward without development toward eastern [[Somalia]].<ref name="imd"/><ref name="ogp"/>

Heavy rainfall, peaking at {{convert|450|mm|in|abbr=on}} in [[Tholudur]], spread across [[Tamil Nadu]]. During the passage of the eye, residents reported a period of calm lasting about 45&nbsp;minutes. Across Tamil Nadu, high winds knocked over 30,000&nbsp;trees, and many coconuts, plantains, and rice paddy farms were damaged in nearby [[Puducherry]]. The winds also damaged about 41,000&nbsp;houses, about 1,000 of which lost their roofs. Flooding washed away 14&nbsp;brick buildings, while 300&nbsp;others were inundated by the sea. Over 1,000&nbsp;power lines were damaged. Overall damages were estimated at ₹700&nbsp;million rupees ($15&nbsp;million USD), and there were 12&nbsp;deaths.<ref name="imd"/> 
{{clear}}

===Extremely Severe Cyclonic Storm BOB 06===
{{Infobox Hurricane Small
|Basin=NIO
|Image=2000 Sri Lanka Cyclone.JPG
|Track=Cyclone 04B 2000 track.png
|Formed=December 23
|Dissipated=December 28
|3-min winds=90
|1-min winds=65
|Pressure=970
}}
{{Main|2000 Sri Lanka cyclone}}
A near-equatorial trough spawned a low pressure area on December&nbsp;22 in the central Bay of Bengal. A circulation within the system developed into a depression on December&nbsp;23 about 500&nbsp;km (310&nbsp;mi) east-southeast of Sri Lanka.<ref name="imd"/> A low-latitude storm, the system organized while moving slowly westward.<ref name="dgp">{{cite web|author=Gary Padgett|year=2001|accessdate=2015-05-24|title=Monthly Tropical Weather Summary for December 2000|url=http://australiasevereweather.com/cyclones/2001/summ0012.htm}}</ref> On December&nbsp;24, the depression strengthened into a deep depression, and the following day into a cyclonic storm,<ref name="imd"/> the same day that the JTWC classified it as Tropical Cyclone 04B.<ref name="dgp"/> An eye developed in the center of the blossoming convection, and the system rapidly intensified into a very severe cyclonic storm on December&nbsp;26. According to the IMD, the cyclone attained peak winds of 165&nbsp;km/h (105&nbsp;mph), and made landfall at that intensity along eastern Sri Lanka near [[Trincomalee]] around 12:00&nbsp;UTC on December&nbsp;26.<ref name="imd"/> The JTWC assessed lower winds of 120&nbsp;km/h (75&nbsp;mph).<ref name="dgp"/> Weakening quickly over land, the storm emerged into the [[Gulf of Mannar]] on December&nbsp;27 and failed to restrengthen. Later that day, it made a second landfall in extreme southern India near [[Thoothukudi]] as a cyclonic storm. Continuing westward, the system emerged into the Arabian Sea on December&nbsp;28 as a depression, and degenerated into a remnant low the next day. The low merged with a trough and spread rainfall northward through India.<ref name="imd"/>

The strongest storm to threaten Sri Lanka since 1992,<ref name="imd"/> the cyclone produced estimated wind gusts of 175&nbsp;km/h (110&nbsp;mph) near where it moved ashore.<ref name="dgp"/> About 500,000&nbsp;people were left homeless,<ref name="atcr"/> after the winds destroyed the roofs of many houses. One entire fishing village was destroyed, and about {{convert|20,000|ha|acre|abbr=on}} of rice fields were wrecked.<ref name="dgp"/> While crossing the country, the cyclone dropped between 4 and 8&nbsp;inches (100 to 200&nbsp;mm) of precipitation, compounding the effects of severe [[monsoon]]al flooding from the previous month. There were nine deaths in the country.<ref>{{cite web|author=Vijitha Silva|year=2001|title=Cyclone wreaks havoc across northern Sri Lanka|publisher=World Socialist Web Site|accessdate=January 4, 2007|url=http://www.wsws.org/articles/2001/jan2001/sri-j04.shtml| archiveurl= https://web.archive.org/web/20061215083625/http://www.wsws.org/articles/2001/jan2001/sri-j04.shtml| archivedate= 15 December 2006 <!--DASHBot--> |deadurl=no}}</ref> Later, the storm brought heavy rainfall to southern India, with a peak 24&#8209;hour total of {{convert|180|mm|in|abbr=on}} in [[Nagapattinam]]. The storm damaged 480&nbsp;houses and wrecked 95&nbsp;fishing boats.<ref name="imd"/>
{{clear}}

==See also==
{{Portal|Tropical cyclones}}
*[[2000 Atlantic hurricane season]]
*[[2000 Pacific hurricane season]]
*[[2000 Pacific typhoon season]]
*South-West Indian Ocean cyclone seasons: [[1999–00 South-West Indian Ocean cyclone season|1999–00]], [[2000–01 South-West Indian Ocean cyclone season|2000–01]]
*Australian region cyclone seasons: [[1999–00 Australian region cyclone season|1999–00]], [[2000–01 Australian region cyclone season|2000–01]]
*South Pacific cyclone seasons: [[1999–00 South Pacific cyclone season|1999–00]], [[2000–01 South Pacific cyclone season|2000–01]]

== Notes==
{{reflist|group =nb}}

==References==
{{Reflist}}

==External links==
*[http://australiasevereweather.com/cyclones/tc2005bt.htm Gary Padgett Tropical Cyclone Summary]
*[http://australiasevereweather.com/cyclones/tc2006bt.htm Gary Padgett Tropical Cyclone Summary Part 2]
*[http://www.wmo.ch/web/www/TCP/OperationPlans/TCP21-OP2005.pdf Tropical Cyclone Operational Plan for the Bay of Bengal and the Arabian Sea]
*[http://www.imd.gov.in/services/cyclone/impact.htm Impact of Cyclonic Storms and Suggested Mitigation Actions] (by India Meteorological Department)
*[http://ftp.wmo.int/pages/prog/www/tcp/documents/pdf/TD1082-TCseason2000.pdf Annual Summary of Global TC Season 2000]
*[http://ftp.wmo.int/pages/prog/www/Peng/TCP_vO/Reports/PTC-28-FINAL-REPORT.pdf WMO/ESCAP Panel on Tropical Cyclones Final Report]

{{TC Decades|Year=2000|basin=North Indian Ocean|type=cyclone}}

{{DEFAULTSORT:2000 North Indian Ocean Cyclone Season}}
[[Category:2000 North Indian Ocean cyclone season|*]]