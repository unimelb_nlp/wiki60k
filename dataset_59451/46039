{{EngvarB|date=April 2015}}
{{Use dmy dates|date=April 2015}}

{{Infobox officeholder
| name        = Adedayo Clement Adeyeye
| image       = IMG PRINCE ADEYEYE.jpg
| office      = [[Former Minister of State for Works]]
| birth_date  = {{birth date and age|df=yes|1957|04|04}}
| birth_place = Ikere Ekiti
| death_date  =
| party       = [[People's Democratic Party (Nigeria)|PDP]]
}}
'''Adedayo Clement Adeyeye''' was born on 4 April 1957 at Ikere-Ekiti, to late Oba (King) David Opeyemi Adeyeye, Agunsoye II, Arinjale of Ise Ekiti who reigned between 1932 and 1976, and Olori Mary Ojulege Adeyeye, a princess of Are, Ikere-Ekiti.
His grandfather was Oba Aweloye I, Arinjale of Ise Ekiti (1887-1919).

== Education ==
Between 1964 and 1968, he attended St. John’s Primary School, Are, Ikere-Ekiti for his primary education.
He later proceeded to Annunciation school, Ikere-Ekiti (1969-1973), before attending the famous [[Christ's School Ado Ekiti]] (1973-1975).
Adeyeye holds a bachelor's degree in [[political science]] from the [[University of Ibadan]] (1978) and a master's degree in political science (international relations) from the [[University of Lagos]] (1981). He also obtained a law degree from the [[University of Lagos]] in 1986 and was called to the Nigerian Bar in 1987.

== Professional career ==
His work experience has spanned three major professional fields - teaching, journalism and legal practice.

Prince Adeyeye was a [[teacher]] at Isuikwuato High School, Isuikwuato, Imo State during his National Youth service (1978-1979) and proceeded to teach at Mary Immaculate Grammar School, [[Ado-Ekiti]] (1980).
Adeyeye made the switch to [[journalism]] in 1981 and was Editor II at the Federal Radio Corporation of Nigeria (FRCN), Ikoyi, between 1981 and 1982.
He had a brief stint at Rank Xerox Limited in 1983 where he was a Senior Sales Executive.
Between 1983 and 1987, he held various editorial positions at The Punch newspapers.
During his active years in legal practice, Prince Adeyeye was the Principal Partner at the law firm of Dayo Adeyeye & Co. between 1990 and 2000.

== In Nigerian politics ==
As a pro-democracy activist, he was a member of the [[National Democratic Coalition (Nigeria)|National Democratic Coalition]] (NADECO).
He was Director of Publicity, Falae for President Campaign Organisation (1990-1992), Adviser on policy and Press Matters, M.K.O. Abiola for President Campaign Organisation (1993), spokesperson for the Alliance for Democracy (AD), and a member of the South-West Delegation to the Nigerian leaders of Thought Conference, Abuja. Prince Adeyeye, was the youngest of the 21 eminent leaders who represented the South Western zone of Nigeria at the Conference (2001)<ref>''The Nation Newspaper'' (22 July 2007).[http://www.thenationonlineng.net/archive2/tblnews_Detail.php?id=25818].</ref>
He was also the National Publicity secretary of the Pan Yoruba Socio-political group, [[Afenifere]] between 2001-2004 as well as the spokesperson of the Alliance for Democracy (AD) between 2004-2006.

He was a member of the ODU (Odua Development Union) which was established for the overall Socio-political and economic development of the South-West. 

In 2006, he was an Ekiti State governorship aspirant under Alliance for Democracy (AD), which later metamorphosed into the Action Congress of Nigeria, and was runner up to Governor [[Kayode Fayemi]] in the controversial primary elections that led to the defection of 13 of the total 16 aspirants from the Action Congress of Nigeria to the [[People's Democratic Party (Nigeria)|People's Democratic Party]] (PDP). Prince Adeyeye was among those who defected.

After the 2007 election, he was nominated for a position as a minister of the [[Federal Republic of Nigeria]] under the President [[Umaru Musa Yaradua]] administration, but his past roles as spokesperson for the opposition party and the National Publicity secretary of the Afenifere group worked against his final appointment as minister.<ref>[http://www.thenationonlineng.net/archive2/tblnews_Detail.php?id=31792 ''The Nation Newspaper'' (20 September 2007)]</ref>

He was subsequently appointed Executive Chairman of Ekiti [[State Universal Basic Education Board]] (SUBEB) under the administration of Governor Segun Oni, where he recorded several achievements and won the Universal Basic Education Commission (UBEC) award as the best chairman in the South-West of Nigeria in 2008. The award came along with a cash prize of Seventy Million Naira. He again won the same award as the best Chairman of SUBEB in the South West in 2009. The funds derived from the awards were used to provide more infrastructure for schools across Ekiti State.<ref>[http://www.thenigerianvoice.com/nvnews/123834/1/ekiti-2014-as-adeyeye-becomes-the-issue.html ''The Nigerian Voice Newspaper'' (9 September 2013)]</ref> That same year, Prince Adedayo Adeyeye was adjudged the most innovative SUBEB chairman in Nigeria by the Presidential Committee on Schools’ debate. He was the only SUBEB chairman in the country selected by The World Bank to attend the conference on Strategies for Education Reform, sponsored by the World Bank in Washington, USA in March,2010. His administration successfully organised the training of over 12,000 teachers within Ekiti State on contemporary teaching methods.<ref>[http://newsnigeria.onlinenigeria.com/templates/?a=1111 ''News Nigeria Online'' (28 March 2009)] {{webarchive |url=https://web.archive.org/web/20131219020604/http://newsnigeria.onlinenigeria.com/templates/?a=1111 |date=19 December 2013 }}</ref>
As chairman of Ekiti SUBEB, Adeyeye changed the face of public school buildings in the State with the introduction of storey buildings and tiled floors and provision of school furniture. During his tenure as SUBEB chairman, pupils stopped carrying furniture to school from their homes. The quality of the furnishings were regarded as the best in the country.

Once again, in  2014, Prince Adeyeye was nominated by President Goodluck Jonathan as a Minister of the Federal Republic of Nigeria. This time, his nomination was approved by the Nigerian senate and he was sworn in by President Goodluck Jonathan on 9th July of that year as Minister of State for Works. He served as Minister till the end of President Goodluck Jonathan's administration on 29th May, 2015.

Prince Adeyeye was appointed by the Ekiti State Government as Pro Chancellor of the Ekiti State University in June 2015 and he is the current Chairman of the University's Governing Council.

== National Caretaker Committee Appointment ==
Following the inauguration of the National Caretaker Committee of the People's Democratic Party on 7th June, 2016, Prince Adeyeye was nominated as the representative of the South-Western Region of Nigeria. He was subsequently appointed as the National Publicity Secretary of the party under the leadership of Senator Ahmed Makarfi on 9th June, 2016 and currently serves the party in that capacity.

== Campaign For Ekiti State Governorship ==
Prince Adedayo Adeyeye was an aspirant for the Ekiti State 2014 gubernatorial elections under the platform of the PDP whose support cut across party lines. He has often been described as a unifying factor for the PDP within the state.<ref>[http://theeagleonline.com.ng/news/ex-afenifere-scribeadeyeye-declares-governorship-ambition/ "The Eagle Newspaper" (30 April 2013)]</ref>

In an interview with the Leadership Newspaper, Adeyeye highlighted his first eleven priorities which pointed at revolutions in agriculture, education, employment figures, health and social welfare amongst others.<ref>[http://leadership.ng/news/070513/power-incumbency-will-not-work-during-ekiti-guber-election-adeyeye "The Leadership Newspaper" (7 May 2013)]</ref>

== References ==
{{Reflist}}

== Further reading ==
* {{cite news |url=http://www.ekitireports.com/tag/prince-dayo-adeyeye |title=Prince Dayo Adeyeye |publisher=Ekiti Reports}}
* {{cite news |url=http://www.tribune.com.ng/news2013/index.php/en/component/k2/item/26483-we-will-rescue-ekiti-state-from-pillagers-adeyeye.html|title=We will rescue Ekiti State|publisher=Nigerian Tribune}}
* {{cite news |url=http://www.ngrguardiannews.com/index.php/news/national-news/138026-why-apc-will-never-rule-nigeria-by-adeyeye|title=Why APC will never rule Nigeria|publisher=The Guardian}}
* {{cite news |url=http://www.punchng.com/news/adeyeye-decries-state-of-hospitals-in-ekiti/|title=Adeyeye decries the state of hospitals in Ekiti|publisher=The Punch Newspapers}}

{{DEFAULTSORT:Adeyeye, Prince Adedayo Clement}}
[[Category:1957 births]]
[[Category:Living people]]
[[Category:People from Ekiti State]]
[[Category:Yoruba politicians]]
[[Category:University of Ibadan alumni]]
[[Category:University of Lagos alumni]]
[[Category:People's Democratic Party (Nigeria) politicians]]
[[Category:Yoruba royalty]]
[[Category:Nigerian schoolteachers]]
[[Category:Yoruba educators]]
[[Category:Nigerian editors]]
[[Category:Christ's School, Ado Ekiti alumni]]