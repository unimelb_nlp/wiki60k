{{italic title}}
'''''Graphs and Combinatorics''''' (ISSN 0911-0119, abbreviated ''Graphs Combin.'') is a [[peer review|peer-reviewed]] [[academic journal]] in [[graph theory]] and [[combinatorics]] published by [[Springer Science+Business Media|Springer Japan]]. Its editor-in-chief is Katsuhiro Ota of [[Keio University]].<ref>{{citation|url=http://link.springer.com/journal/373|publisher=[[Springer Science+Business Media]]|title=Graphs and Combinatorics|accessdate=2016-01-30}}.</ref>

The journal was first published in 1985. Its founding editor in chief was Hoon Heng Teh of Singapore, the president of the Southeast Asian Mathematics Society, and its managing editor was [[Jin Akiyama]].<ref>{{citation
 | last1 = Kano | first1 = M.
 | last2 = Ruiz | first2 = Mari-Jo P.
 | last3 = Urrutia | first3 = Jorge
 | doi = 10.1007/s00373-007-0720-5
 | issue = S1
 | journal = Graphs and Combinatorics
 | pages = 1–39
 | title = Jin Akiyama: A Friend and His Mathematics
 | url = http://www.matem.unam.mx/urrutia/online_papers/Akiyama-Article.pdf
 | volume = 23
 | year = 2007}}.</ref> Originally, it was subtitled "An Asian Journal".<ref>{{citation|title=Springer-Verlag: History of a Scientific Publishing House: Part 2: 1945–1992. Rebuilding – Opening Frontiers – Securing the Future|first=Heinz|last=Götze|translator=M. Schäfer|publisher=Springer-Verlag|year=2008|isbn=9783540928881|page=132|url=https://books.google.com/books?id=3f1DAAAAQBAJ&pg=PA132}}.</ref>

In most years since 1999, it has been ranked as a second-quartile journal in [[discrete mathematics]] and [[theoretical computer science]] by [[SCImago Journal Rank]].<ref>{{citation|url=http://www.scimagojr.com/journalsearch.php?q=28476&tip=sid|title=Graphs and Combinatorics|publisher=[[SCImago Journal Rank]]|accessdate=2016-01-30}}.</ref>

==References==
{{reflist}}

[[Category:Publications established in 1985]]
[[Category:Mathematics journals]]