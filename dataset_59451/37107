{{good article}}
{{Infobox single
| Name        = Balans
| Artist      = [[Alexandra Stan]] featuring [[Mohombi]]
| Cover       = Balans (cover).jpg
| Alt         = The cover shows Stan and Mohombi staying close to each other in front of a grey background, with the song's title, "Balans" being displayed in front of them
| Border      = yes
| Album       = [[Alesta (album)|Alesta]]
| Released    = 2 March 2016
| Format      = [[Music download|Digital download]]
| Genre       = {{Flat list|
*[[Dance-pop]]
*[[Tropical music|tropical]]
*[[Latin music (genre)|Latin]]
}}
| Length      = {{duration|m=3|s=06}}
| Label       = Global
| Writer      = {{Flat list|
*[[Play & Win|Sebastian Barac]]
*[[Play & Win|Marcel Botezan]]
*[[Mohombi|Mohombi Nzasi Moupondo]]
*Breyan Isaac
}}
| Producer    = {{Flat list|
*Sebastian Barac
*Marcel Botezan
}}
| Chronology  = [[Alexandra Stan]] singles
| Last single = "[[I Did It, Mama!]]"<br />(2016)
| This single = "'''Balans'''"<br />(2016)
| Next single = "[[Écoute (Alexandra Stan song)|Écoute]]"<br />(2016)
| Misc =
{{Extra chronology
 | Artist      = [[Mohombi]] singles
 | Type        = single
 | Last single = "[[Baddest Girl in Town]]"<br />(2015)
 | This single = "'''Balans'''"<br />(2016)
 | Next single = "Infinity"<br />(2016) 
 }}
{{External music video|{{YouTube|fbzTwo4UQ_M|"Balans"}}}}
}}

"'''Balans'''" (pronounced "balance") is a song recorded by [[Romanians|Romanian]] recording artist [[Alexandra Stan]] for her eponymous and second studio album, ''[[Alesta (album)|Alesta]]'' (2016). Released on 2 March 2016 through Global Records, the track features the vocal collaboration of [[Swedish people|Swedish]]-[[Democratic Republic of the Congo|Congolese]] singer-songwriter [[Mohombi]]. "Balans" was produced by [[Play & Win]] members Sebastian Barac and Marcel Botezan, while the writing process was handled by the latter two in collaboration with Mohombi and Breyan Isaac.

A [[dance-pop]] song incorporating [[tropical music]] and [[latin music (genre)|Latin music]] influences into its sound, the recording was compared by critics to the works of fellow Romanian female singer [[Inna]]. An official music video for "Balans" was shot by Anton San and was uploaded on 9 March 2016 onto [[YouTube]] in order to accompany the single's release. It portrays both Stan and Mohombi being present at a party held in a hall filled halfway by water. Following its release, the song received a mixed review by ''RnB Junk''; they commended the collaboration, but criticized its lack of innovation and formulaic production. Commercially, the song peaked in Japan and Russia.

==Background and reception==
"Balans", the third single released from Stan's debut record, ''Alesta'' (2016), was written by [[Play & Win]] members Sebastian Barac and Marcel Botezan, Mohombi and Breyan Isaac, while production was handled by both Barac and Botezan.<ref name="credits"/> The artwork used to commercialize the track in Japan was a photograph picked up from the photo shooting for her album, while the international cover sees Stan and Mohombi sporting the outfits from the official music video.<ref name="credits"/> The track is an up-tempo [[dance-pop]] song, which includes musical elements of [[tropical music]] and [[latin music (genre)|latin music]].<ref name="rnbjunk">{{cite web|url=http://www.rnbjunk.com/alexandra-stan-feat-mohombi-balans-video-premiere/|title=Alexandra Stan feat. Mohombi - Balans|accessdate=19 May 2016|publisher=''RnB Junk''|date=9 March 2016|author=Olivio, Umberto|language=Italian| archivedate=31 March 2016|archiveurl=https://web.archive.org/web/20160331042937/http://www.rnbjunk.com/alexandra-stan-feat-mohombi-balans-video-premiere/}}</ref><ref name="iTunes.ro">{{cite web|url=https://itunes.apple.com/ro/album/balans-feat.-mohombi-single/id1091249357|title=Balans (feat. Mohombi) - Single by Alexandra Stan on iTunes|accessdate=19 May 2016|work=iTunes Store|date=4 March 2016}}</ref><ref name="europaFM">{{cite web|url=http://www.europafm.com/noticias/musica/alexandra-stan-calienta-motores-nuevo-album-balans_2016031056e165896584a8839ae5e23e.html|title=Alexandra Stan calienta motores para su nuevo álbum con 'Balans'|accessdate=19 May 2016|publisher=Europa FM|date=10 March 2016|author=Vilalta, Alex|language=Spanish|trans_title=Alexandra Stan, hot in "Balans", the new song for her new album}}</ref>

Upon its release, the recording received a mixed review from Italian publication ''Rnb Junk'' writer Umberto Olivio, who commended that the song resembles the works of fellow Romanian female singer [[Inna]].<ref name="rnbjunk"/> He went on into praising Stan's collaboration with [[Mohombi]], but also saying that the "formula" for the recording was "nothing innovative", with him describing the track as "quite obvious".<ref name="rnbjunk"/> Olivio as well named "the level of palatability" of the tune "a lot weaker than the other songs from ''Alesta''", and denied the possible success of the track in mainstream clubs.<ref name="rnbjunk"/>

==Promotion and music video==
[[File:Balans_screenshot.jpg|left|230px|thumb|Stan (''right'') and Mohombi (''left'') flirting during the music video for "Balans".]]Stan included "Balans" on the setlist for her Japanese one-week concert tours that promoted the release of her studio album, ''Alesta'', in that territory.<ref name="credits"/><ref name="cancan"/><ref name="balans">{{cite news|url=http://www.radio21.ro/alexandra-stan-si-mohombi-canta-pe-ritmuri-de-balans/|title=Alexandra Stan si Mohombi canta pe ritmuri de "Balans"|trans_title=Alexandra Stan and Mohombi sing to the rhythm of "Balans"|publisher=Radio 21|date=2 March 2016|accessdate=3 March 2016|language=Romanian|archivedate=3 March 2016|archiveurl=https://web.archive.org/web/20160303124003/http://www.radio21.ro/alexandra-stan-si-mohombi-canta-pe-ritmuri-de-balans/}}</ref> Stan performed an acoustic version of the song on Romanian radio station [[Pro FM]].<ref>{{cite web|url=https://www.youtube.com/watch?v=5uZib52jfDk|title=Alexandra Stan - Balans (Live la Radio ZU)|accessdate=20 May 2016|work=[[YouTube]]|date=18 March 2016}}</ref> After this, she had also performed a live acoustic version of "[[Be the One (Dua Lipa song)|Be the One]]" by English recording artist [[Dua Lipa]].<ref>{{cite web|url=https://www.youtube.com/watch?v=2j8WSXNq0nQ|title=Alexandra Stan - Be the One (Dua Lipa) (Live la Radio ZU)|accessdate=20 May 2016|work=[[YouTube]]|date=18 March 2016}}</ref>

An accompanying music video for "Balans" was directed by Anton San and released onto Stan's YouTube channel on 2 March 2016; by May 2016, the clip amassed over one million views.<ref name="video">{{cite web|url=https://www.youtube.com/watch?v=fbzTwo4UQ_M|title=Alexandra Stan feat. Mohombi - Balans [Official Music Video]|accessdate=19 May 2016|work=[[YouTube]]|date=9 March 2016}}</ref><ref name="urban">{{cite web | title=Alexandra Stan feat. Mohombi - Balans (videoclip nou)| url=http://www.urban.ro/muzica/videoclipuri/alexandra-stan-feat-mohombi---balans-videoclip-nou|language=Romanian | publisher=Urban.ro|accessdate=19 May 2016|date=10 March 2016|trans_title=Alexandra Stan feat. Mohombi - Balans (new video)|author=Scris de Alex}}</ref> The clip was filmed in an abandoned warehouse or a polygon where the inside temperature was of 6&nbsp;°C.<ref name="europaFM"/><ref name="video2">{{cite web|url=https://www.youtube.com/watch?v=584kuIeDa-E|language=Romanian|title=Making of "Alexandra Stan feat. Mohombi - Balans" official video|accessdate=19 May 2016|work=[[YouTube]]|date=28 March 2016}}</ref> About the video, Stan particularly confessed that "[it] is very dinamic and colored, the way that the life of a pop artist looks like."<ref name="cancan">{{cite web|url=http://www.cancan.ro/showbiz/showbiz-intern/alexandra-stan-lanseaza-videoclipul-balans-alaturi-de-mohombi.html|title=Alexandra Stan a lansat videoclipul "Balans" alături de Mohombi|accessdate=19 May 2016|publisher=''Cancan''|date=11 March 2016|language=Romanian|trans_title=Alexandra Stan and Mohombi have released the video for "Balans"}}</ref> The clip commences with Stan standing on a pickup, with fellow background dancers being present inside the car. Following their leaving, she looks from above at a partying crowd in a hall, which appears to be halfway filled with water, while sporting pink and red clothing. Subsequently, Stan is displayed dressing a pink [[fur coat]] and pants,<ref name="europaFM"/> while walking in the surroundings and finally discovering Mohombi. Following this, he provides his singing part when flirting with Stan, with her dancing around him. Following this, the crowd is once again shown dancing together to the song, and the clip closes with Stan being left alone in the room. Scenes interspersed through the main video show her posing in front of a blue wall or her lying atop the pickup car, with her head being placed on a golden purse.<ref name="video"/> Italian publication ''RnB Junk'' writer, Umberto Olivio, criticized the clip for not having a plot and for being "unrealistic".<ref name="rnbjunk"/>

==Track listing==
{{Track listing
| headline  = Digital download<ref name="iTunes.ro"/>
| title1  = Balans
| note1 = featuring Mohombi
| length1  = 3:06
}}

==Credits and personnel==
Credits adapted from the liner notes of ''Alesta''.<ref name="credits">{{cite AV media notes |title=Alesta |others=[[Alexandra Stan]] |year=2016 |type=Liner notes/ CD booklet |publisher=[[Victor Entertainment]] (Barcode: 4988002709182) |location=[[Tokyo]], Japan}}</ref>

{{col-begin}}
{{col-2}}
;Vocal credits
*Alexandra Stan – [[Vocals|lead vocals]]
*Mohombi – featured artist

;Technical and songwriting credits
*Sebastian Barac – [[songwriter]], [[Record producer|producer]]
*Marcel Botezan – songwriter, producer
*Mohombi Nzasi Moupondo – songwriter
*Breyan Isaac – songwriter
{{col-2}}

;Visual credits
*Anton San – [[Music director|director]], [[director of photography]]
*Bogdan Filip – director of photography
*Claudiu Sarghe – [[hair styling]]
*Razvan Firea – styling
*Marius Ferascu – styling
{{col-end}}

==Charts==

===Weekly charts===
{|class="wikitable unsortable plainrowheaders"
|-
! scope="col"| Chart (2016) 
! scope="col"| Peak<br>position
|-
! scope="row"|Japan Radio Songs ([[Billboard Japan|''Billboard'']])<ref>{{cite news|url=http://www.billboard-japan.com/charts/detail?a=radio_songs&year=2016&month=03&day=28|title=Billboard Japan Radio Songs <nowiki>|</nowiki> Charts <nowiki>|</nowiki> Billboard Japan|publisher=''[[Billboard Japan]]''|language=Japanese|accessdate=11 July 2016}}</ref>
|align=center| 32
|-
! scope="row"|Russia Airplay ([[Tophit]])<ref>{{cite news|url=http://www.tophit.ru/ru/tracks/74892/view|title=Balans – Alexandra Stan & Mohombi – Tophit.ru|publisher=[[Tophit]]|language=Russian|accessdate=9 July 2016}}</ref>
|align=center| 290
|}

==Release history==
{|class="wikitable plainrowheaders unsortable"
!Country
!Date
!Format
!Label
|-
!scope="row" rowspan="1"|Japan<ref name="iTunes.jp">{{cite web|url=https://itunes.apple.com/jp/album/balans-feat.-mohombi-single/id1086853402|work=iTunes Store|language=Japanese|date=2 March 2016|accessdate=19 May 2016|title= Balans feat. Mohombi - Single}}</ref>
|rowspan="1"|2 March 2016
|rowspan="11"|Digital single
|rowspan="11"|Global
|-
!scope="row" rowspan="1"|Brazil<ref>{{cite web|url=https://itunes.apple.com/br/album/balans-feat.-mohombi-single/id1091249357|work=iTunes Store|date=4 March 2016|accessdate=20 May 2016|title= Balans (feat. Mohombi) - Single de Alexandra Stan no iTunes}}</ref>
|rowspan="10"|4 March 2016
|-
!scope="row" rowspan="1"|Greece<ref>{{cite web|url=https://itunes.apple.com/gr/album/balans-feat.-mohombi-single/id1091249357|work=iTunes Store|date=4 March 2016|accessdate=20 May 2016|title= Balans (feat. Mohombi) - Single by Alexandra Stan on iTunes}}</ref>
|-
!scope="row" rowspan="1"|Netherlands<ref>{{cite web|url=https://itunes.apple.com/nl/album/balans-feat.-mohombi-single/id1091249357|work=iTunes Store|date=4 March 2016|accessdate=20 May 2016|title= Balans (feat. Mohombi) - Single van Alexandra Stan in iTunes}}</ref>
|-
!scope="row" rowspan="1"|Portugal<ref>{{cite web|url=https://itunes.apple.com/pt/album/balans-feat.-mohombi-single/id1091249357|work=iTunes Store|date=4 March 2016|accessdate=20 May 2016|title= Balans (feat. Mohombi) - Single de Alexandra Stan no iTunes}}</ref>
|-
!scope="row" rowspan="1"|Romania<ref name="iTunes.ro"/>
|-
!scope="row" rowspan="1"|Russia<ref>{{cite web|url=https://itunes.apple.com/ru/album/balans-feat.-mohombi-single/id1091249357|work=iTunes Store|date=4 March 2016|accessdate=20 May 2016|title= Balans (feat. Mohombi) - Single by Alexandra Stan on iTunes}}</ref>
|-
!scope="row" rowspan="1"|Sweden<ref>{{cite web|url=https://itunes.apple.com/se/album/balans-feat.-mohombi-single/id1091249357 |work=iTunes Store |date=4 March 2016 |accessdate=20 May 2016 |title=Balans (feat. Mohombi) - Single av Alexandra Stan på iTunes }}{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>
|-
!scope="row" rowspan="1"|Switzerland<ref>{{cite web|url=https://itunes.apple.com/ch/album/balans-feat.-mohombi-single/id1091249357|work=iTunes Store|date=4 March 2016|accessdate=20 May 2016|title= Balans (feat. Mohombi) - Single von Alexandra Stan in iTunes}}</ref>
|-
!scope="row" rowspan="1"|Taiwan<ref>{{cite web|url=https://itunes.apple.com/tw/album/balans-feat.-mohombi-single/id1091249357|work=iTunes Store|date=4 March 2016|accessdate=20 May 2016|title= Balans (feat. Mohombi) - Single by Alexandra Stan on iTunes}}</ref>
|-
!scope="row" rowspan="1"|United States<ref>{{cite web|url=https://itunes.apple.com/us/album/balans-feat.-mohombi-single/id1091249357|work=iTunes Store|date=4 March 2016|accessdate=20 May 2016|title= Balans (feat. Mohombi) - Single by Alexandra Stan on iTunes}}</ref>
|}

==References==
{{Reflist|30em}}

==External links==
*[http://www.alexandrastantheartist.com/ Alexandra Stan's official website]
*[http://www.mohombi.com/ Mohombi's official website]
* {{MetroLyrics song|alexandra-stan|balans}}<!-- Licensed lyrics provider -->

{{Alexandra Stan}}
{{Mohombi}}

[[Category:2016 songs]]
[[Category:Alexandra Stan songs]]
[[Category:Mohombi songs]]
[[Category:Tropical songs]]