{{About|the Powderfinger song|the 1948 pop standard|My Happiness (country song)}}
{{Use dmy dates|date=October 2015}}
{{Use Australian English|date=October 2015}}
{{Infobox single
| Name        = My Happiness
| Artist      = [[Powderfinger]]
| Cover       = My Happiness - Powderfinger.jpg
| Album       = [[Odyssey Number Five]]
| Released    = 21 August 2000 
| Recorded    = Sing Sing Studios, [[Melbourne]], [[Australia]]
| Genre       = [[Alternative rock]]
| Length      = 4:36
| Label       = [[Universal Music Australia]]
| Writer      = [[Jon Coghill]], [[John Collins (Australian musician)|John Collins]], [[Bernard Fanning]], [[Ian Haug]], [[Darren Middleton]]
| Producer    = [[Nick DiDia]]
| Last single  = "[[My Kind of Scene]]"<br />(2000)
| This single  = "My Happiness"<br />(2000)
| Next single  = "[[Like a Dog (song)|Like a Dog]]"<br />(2000)
}}

"'''My Happiness'''" is the album of [[Australia]]n rock band by [[Powderfinger]]. It was released on 21 August 2000 by [[Universal Music Australia]]. It won the 2001 [[ARIA Music Awards of 2001|ARIA Music Award]] for Single of the Year. ''[[Odyssey Number Five]]''. The single is Powderfinger's most successful; it peaked at number four on the Australian [[ARIA Singles Chart]], and charted in the United States on the [[Hot Modern Rock Tracks]] chart—the first Powderfinger song to do so.

Powderfinger frontman [[Bernard Fanning]] wrote the lyrics for "My Happiness" as a reflection on the time the band spent touring to promote their work, and the loneliness that came as a result. It was inspired by his love of [[Gospel music|gospel]] and [[soul music]]. The rest of the band are co-credited with Fanning for composing the track. Despite its melancholy mood, "My Happiness" is considered by many to be a [[love song]], a suggestion Fanning regards as mystifying.<ref name=fingerprints />

"My Happiness" was released as a single with "[[My Kind of Scene]]" as a [[B-side]]. It was instantly successful, charting highly in Australia and New Zealand. It won an [[ARIA Award]] and an [[APRA Award]] and topped the [[Triple J Hottest 100]] in [[Triple J Hottest 100, 2000|2000]] as well as coming 27th in the 2009 Hottest 100 of all Time. "My Happiness" was highly praised by critics, with even negative reviews of ''Odyssey Number Five'' noting it as a highlight, especially for its catchy chorus. "My Kind of Scene" was equally popular. One of the highlights of Powderfinger's United States tour with [[Coldplay]] was a performance of "My Happiness" on ''[[Late Show with David Letterman]]''; they were only the fourth Australian act to appear on the show.

== Production and content ==
<blockquote class="toccolours" style="text-align:left; width:30%; float:left; padding: 10px 15px 10px 15px; display:table;">If you can't cop a bit of emotional stuff then you should go and get the lamp shade extracted from your arse. If you don't think there is enough rock in your life then let me know and I will personally come around to your house and chuck stones at you.<p style="text-align: right;">—[[Bernard Fanning]]<br /><small>In response to "My Happiness" being described by fans as "like [[Lauryn Hill]], bland and boring Top 40 bullshit".</small><ref name=SWJuice /></blockquote>

The lyrics for "My Happiness" were written by [[Bernard Fanning]], Powderfinger's [[lead singer]] and [[songwriter]]. The rest of the band are co-credited with Fanning for composing the track.<ref name="2001Nom">{{cite web | url = http://www.apra-amcos.com.au/APRAAwards/MusicAwards/Nominations/Nominations2001.aspx#soty | title = Nominations – 2001 | publisher = [[Australasian Performing Right Association]] (APRA) &#124; [[Australasian Mechanical Copyright Owners Society]] (AMCOS) | accessdate = 20 February 2014 }}</ref> The song describes feelings of love and separation; ''[[Sain (magazine)|Sain]]''{{'}}s Pennie Dennison said it described "the pining feeling you experience when you spend time away from the one you love".<ref name=SainPennie>{{cite news|title=Odyssey Number Five Is Born|publisher=''[[Sain (magazine)|Sain]]''|author=Dennison, Pennie|date=September 2000}}<!-- http://www.ozmusic-central.com.au/powderfinger/text/articl30.htm --></ref> Fanning called it "a sad story of touring and the absence loneliness that comes with it".<ref name=fingerprints>{{cite book|authorlink=Powderfinger|title=[[Fingerprints: The Best of Powderfinger, 1994-2000]] companion booklet|pages=1|publisher=[[Universal Music Australia]]}}</ref> The extensive time spent touring took its toll on the band, and it was on the back of this that Fanning wrote "My Happiness".<ref name=PTTP /> Thus, he expressed confusion at its being considered a romantic song.<ref name=fingerprints />

"My Happiness" was attacked by some fans as being "like [[Lauryn Hill]], bland and boring Top 40 bullshit"; guitarist [[Ian Haug]] rebutted by pointing out that the song was an example of the new emotional level on which Powderfinger made music, while Fanning was more aggressive in his defence of the song.<ref name=SWJuice>{{cite news|title=This Sporting Life|publisher=''Juice''|author=Wooldridge, Simon|date=September 2000}}<!-- http://www.ozmusic-central.com.au/powderfinger/text/articl39.htm --></ref> In response to being dubbed "Mr Miserable" by ''[[The Sun-Herald]]''{{'}}s Peter Holmes for the lyrics of "My Happiness" and "[[These Days (Powderfinger song)|These Days]]", Fanning pointed out that the songs could be construed either as melancholy, or as part of "the most hopeful record ... in a long time".<ref name=Lickin>{{cite news|title=Powderfinger Lickin' Good|publisher=''[[The Sun-Herald]]''|author=Holmes, Peter|date=19 August 2001}}<!-- http://www.ozmusic-central.com.au/powderfinger/text/articl36.htm --></ref>

Much of Fanning's writing is inspired by non-rock music, and "My Happiness" is no exception. [[Gospel music|Gospel]] and [[soul music]] that is "unashamedly about love and how good it makes you feel" was common during the ''Odyssey Number Five'' recording sessions.<ref name=SainPennie /> Powderfinger worked hard in those sessions to ensure a more polished work than ''Internationalist''; guitarist [[Darren Middleton]] concluded that "My Happiness", "[[The Metre]]", and "Up & Down & Back Again" were more "complete" because of the band's efforts.<ref>{{cite news|title=Trusty Old Jackets|publisher=''Massive''|author=Yates, Rod|date=September 2000}}<!-- http://www.ozmusic-central.com.au/powderfinger/text/articl31.htm --></ref> The lighter elements of "My Happiness" in comparison to some of the band's earlier work saw Fanning reveal his passion for several other musicians, such as [[James Taylor]]—something that "five years ago ... would have been an embarrassing thing to say".<ref>{{cite news|title=Putting the emphasis on songwriting |publisher=''[[The Press-Enterprise]]''|author=Maestri, Cathy|date=8 June 2001|page=13}}</ref>

== Touring and promotion ==
"My Happiness" was put on heavy rotation by [[Los Angeles]] [[radio station]] [[KROQ-FM]] two months prior to its United States release,<ref>{{cite news|title=Top band shunned - Triple M rejects single.|publisher=''[[The Sunday Telegraph]]''|author=McCabe, Kathy|date=4 February 2001|page=21}}</ref> and Powderfinger signed a contract with United States label [[Republic Records|Republic]] as a result of the song's early success.<ref name=RSA /> ''Beat'' journalist Jayson Argall joked the song had received "a bit" of airplay.<ref name=Beat>{{cite news|title=Bowling Maidens Over|publisher=''Beat''|author=Argall, Jayson|date=August 2001}}<!-- http://www.ozmusic-central.com.au/powderfinger/text/articl40.htm --></ref> Although "My Happiness" was subsequently dropped from KROQ-FM's roster, other radio stations continued to give the song high priority.<ref name=Billboard>{{cite web|url=http://www.billboard.com/articles/news/79441/powderfinger-exports-its-aussie-appeal-stateside |title=Powderfinger Exports Its Aussie Appeal Stateside |publisher=[[Billboard.com]]|author=Pesselnick, Jill |date=13 June 2001}} Accessed 22 August 2008.</ref>

"My Happiness" peaked at number 23 on the [[Hot Modern Rock Tracks]], making it the first Powderfinger song to appear on a ''[[Billboard (magazine)|Billboard]]'' chart.<ref name=BBCharts /> According to Susan Groves of [[WGY-FM|WHRL]], part of the song's success came about because very few people knew of Powderfinger, but were drawn towards "My Happiness" because it was "melodic, [and] pretty"—a change from what she described as "middle of the road rock" popular in the United States.<ref name=Billboard /> Meanwhile, Australians were "starting to get sick of My Happiness"—Cameron Adams argued in ''[[The Hobart Mercury]]'' that this was one of the reasons Powderfinger decided to focus on the offshore market.<ref>{{cite news|title=Odyssey Continues|author=Adams, Cameron|publisher=''[[The Hobart Mercury]]''|date=1 February 2007|page=27}}</ref>

Powderfinger performed "My Happiness" live on [[talk show]] ''[[Late Show with David Letterman]]'' while touring North America with British rock group [[Coldplay]],.<ref name=PTTP>{{cite news|publisher=''RM Rave''|title=Powder to the People|author=Sawford, Gavin|date=12 July 2001}}<!-- http://www.ozmusic-central.com.au/powderfinger/text/articl41.htm --></ref> They were the fourth Australian act (after [[The Living End]], [[Silverchair]], and [[Nick Cave]]) to play on the show.<ref name=Internationalists>{{cite news|title=Internationalists|author=Munro, Kelsey|publisher=''Juice''|date=November 2001}}<!-- http://www.ozmusic-central.com.au/powderfinger/text/articl33.htm --></ref> The band also did free promotional shows leading up to the release of the single.<ref>{{cite news|title=The Diary|publisher=''[[The Sun Herald]]''|author=Sharp, Annette|date=23 July 2000|page=28}}<!-- http://en.wikipedia.org/w/index.php?diff=prev&oldid=211602126 --></ref> In Europe, "My Happiness" received approximately four weeks of airplay on German music video program ''Viva II'', and the band sold out for three nights in a row in [[London]], partly due to the success of the single.<ref>{{cite news|title=Bombing Europe.|publisher=''[[The Courier Mail]]''|author=Jefferys, Campbell|date=1 September 2001}}</ref>

== Music video ==
[[File:My Happiness Slinky Video.png|thumb|A frame from the "My Happiness" music video shows a [[wikt:sentient|sentient]] [[slinky]] dancing as Powderfinger perform the song in the background]]
The [[music video]] for "My Happiness" starts at a train station ([[Roma Street railway station, Brisbane|Roma Street railway station]] in [[Brisbane]]) with a boy and girl stepping off a train. As the pair leave the train, the boy turns and tries to reach for something, but the girl pulls him back. It is shown that he was reaching for a [[wikt:sentient|sentient]] [[slinky]]. The slinky leaves the train, and passes Middleton [[busking]] in the train station. The slinky ventures to find the boy, facing a range of challenges along the way; these include avoiding fruit falling on it and riding a skateboard. In the middle of the music clip, the slinky is shown making its way through a music room in which Powderfinger are performing "My Happiness". It rests on the bar and the band finishes playing, while the background music continues. As Powderfinger leaves, the slinky is picked up by Haug. He gets into a car and places the slinky on the car's dashboard, but it falls out the window as the car turns a tight corner. It lands outside the gate of a house and is picked up and brought inside to the boy.

The video was created by Fifty Fifty Films, who created numerous other Powderfinger music videos.<ref>{{cite web|url=http://www.fiftyfifty.tv/cgi-bin/CMS.pl?T=1&P=0|title=www.fiftyfifty.tv|publisher=Fifty Fifty Films}} Accessed 22 August 2008.</ref> It was directed by Chris Applebaum and produced by Keeley Gould of A Band Apart, with editing by Jeff Selis. Cameron Adams of ''[[The Courier Mail]]'' reported that following the music video's release, [[slinky]] sales increased dramatically.<ref>{{cite news|title=Powder to the people|author=Adams, Cameron|publisher=''[[The Courier Mail]]''|date=26 January 2001}}</ref>

== Release and commercial success ==
"My Happiness" was released as a [[single (music)|single]] in Australia on 21 August 2000.<ref>{{cite news|title=DINO, the truth, the whole truth & nothing but the truth.|author=Scatena, Dino|publisher=''[[The Daily Telegraph (Australia)|The Daily Telegraph]]''|date=27 July 2000}}</ref> When asked how they chose the release date, Fanning jokingly said "the release date is timed to coincide with the [[2000 Summer Olympics|Olympics]], when all the visitors are here&nbsp;... they can go into [[HMV]] and pick it up."<ref>{{cite news|title=DINO, the truth, the whole truth & nothing but the truth.|author=Scatena, Dino|publisher=''[[The Daily Telegraph (Australia)|The Daily Telegraph]]''|date=3 August 2000}}</ref> At the time of the single's release, the band's previous album, ''[[Internationalist (album)|Internationalist]]'', was still in the top 50 on the [[ARIA Albums Chart]], 95 weeks after entering.<ref>{{cite news|title=Powderfinger on road |author=McCabe, Kathy|publisher=''[[The Sunday Telegraph (Australia)|The Sunday Telegraph]]''|date=6 August 2000|page=95}}</ref><ref>[http://australian-charts.com/weekchart.asp?year=2000&date=20000827&cat=a ARIA Albums Chart 27/08/2000]. australian-charts.com. Accessed 22 August 2008.</ref> The single featured B-side "[[My Kind of Scene]]", which had already received strong airplay due to its appearance on the [[Mission: Impossible II soundtrack|''Mission: Impossible II'' soundtrack]].<ref>{{cite news|title=Making a killing for kids' charity|author=McCabe, Kathy|publisher=''[[The Sunday Telegraph (Australia)|The Sunday Telegraph]]''|date=16 July 2000|page=138}}</ref> "My Happiness" appeared on a [[Triple M]] [[compilation album|compilation]] entitled ''Triple M's New Stuff'',<ref>{{cite web|url={{Allmusic|class=song|id=t4617478|pure_url=yes}} |title=My Happiness > Appears on|publisher=[[Allmusic]]}} Accessed 22 August 2008.</ref> and on a ''[[Kerrang!]]'' compilation, ''Kerrang!<sup>2</sup> The Album''.<ref name=AusCharts />

"My Happiness" entered the [[ARIA Charts|ARIA singles chart]] at number four—making it Powderfinger's highest-charting single in Australia—and spent 24 weeks on the chart.<ref name=AusCharts>{{cite web|url=http://www.australian-charts.com/showitem.asp?key=168324&cat=s |title=Powderfinger - My Happiness|publisher=australian-charts.com}} Accessed 22 August 2008.</ref> It reached number two on the [[Queensland]] singles chart,<ref>{{cite news|title=Bold 'Finger|author=Yorke, Ritche|publisher=''[[The Sunday Mail (Brisbane)|The Sunday Mail]]''|date=27 August 2000|page=74}}</ref>  and peaked at number seven on the [[RIANZ|New Zealand singles chart]], on which it spent 23 weeks.<ref name=NZCharts>{{cite web|url=http://charts.org.nz/showitem.asp?interpret=Powderfinger&titel=My+Happiness&cat=s |title=Powderfinger - My Happiness|publisher=charts.org.nz}} Accessed 22 August 2008.</ref> "My Happiness" was Powderfinger's first single to chart in the USA, reaching number 23 on the [[Modern Rock Tracks]] chart.<ref name=BBCharts>{{cite web |url={{BillboardURLbyName|artist=powderfinger|chart=all}} |title=Artist Chart History - Powderfinger|publisher=[[Billboard.com]]}} Accessed 22 August 2008.</ref>

The song won the "Single of the Year" award at the [[ARIA Awards]] of [[ARIA Music Awards of 2001|2001]],<ref name="arias-list">{{cite web|url=http://www.ariaawards.com.au/history-by-artist.php?letter=P&artist=Powderfinger|title=History: Winners by Artist: Powderfinger|publisher=[[Australian Recording Industry Association]]}} Accessed 22 August 2008.</ref> and the 2001 "Song of the Year" [[APRA Award]].<ref name=APRA>{{cite web|url=http://www.apra.com.au/awards/music/winners2001.asp|title=APRA Music Awards 2001|publisher=[[Australasian Performing Right Association]]|archiveurl = https://web.archive.org/web/20080723005556/http://www.apra.com.au/awards/music/winners2001.asp |archivedate = 23 July 2008|deadurl=yes}} Accessed 22 August 2008.</ref> Furthermore, "My Happiness" topped the [[Triple J Hottest 100]] chart in [[Triple J Hottest 100, 2000|2000]],<ref name=JJJ>{{cite web|url=http://www.abc.net.au/triplej/hottest100/history/2000.htm |title=Hottest 100 History 2000|publisher=[[Triple J]]}} Accessed 22 August 2008.</ref> and appeared on that year's [[CD]] release.<ref>{{cite web|url=http://www.abc.net.au/triplej/hottest100/history/vol8.htm|title=Hottest 100 CD 2000|publisher=[[Triple J]]|archiveurl = https://web.archive.org/web/20080621074027/http://www.abc.net.au/triplej/hottest100/history/vol8.htm |archivedate = 21 June 2008|deadurl=yes}} Accessed 22 August 2008.</ref> ''[[Rolling Stone|Rolling Stone Australia]]'' named "My Happiness" "Song of the Year" in a reader poll.<ref name=RSA>{{cite news|title=Powderfinger - Band Of The Year|publisher=''[[Rolling Stone]]''|author=Apter, Jeff|date=April 2001}}<!-- http://www.ozmusic-central.com.au/powderfinger/text/articl34.htm --></ref> "My Happiness" was the eighth most-played song on Australian radio in 2001.<ref>{{cite news|title=Savage crashes but fans' passion burns|publisher=''[[The Sunday Times (Western Australia)|The Sunday Times]]''|date=30 December 2001|page=20}}</ref>

== Critical reception ==
{{Listen|filename=Powderfinger-My Happiness-23s.ogg|title="My Happiness"|description=While Chad Watson of ''[[The Newcastle Herald]]'' described a "restrained yet warmly infectious chorus",<ref name=Newcastle /> Richard Jinman of ''[[The Sydney Morning Herald]]'' argued the hook was not as "hummable" as some of Powderfinger's other work.<ref name=hummable />}}

"My Happiness" was a critical success. Cameron Adams of ''[[The Herald Sun]]'' wrote that "My Happiness" did not disappoint in the trend of excellent first singles from Powderfinger, citing "[[Pick You Up]]" and "[[The Day You Come]]" as examples. He praised the song's structure, stating that "the verses almost crash into the chorus". Adams also expressed surprise that "My Kind of Scene" was only released as a B-side.<ref name=AdamsSingles>{{cite news|title=The singles|author=Adams, Cameron|publisher=''[[The Herald Sun]]''|date=10 August 2000|page=56}}</ref> ''[[The Newcastle Herald]]''{{'}}s Chad Watson described a mixture of [[acoustic guitar|acoustic]] and [[electric guitar]] and "a restrained yet warmly infectious chorus".<ref name=Newcastle>{{cite news|title=My Happiness review|author=Watson, Chad|publisher=''[[The Newcastle Herald]]''|date=10 August 2000|page=46}}</ref> Despite praising it as a "Big Rock Anthem<sup>TM</sup>", Richard Jinman of ''[[The Sydney Morning Herald]]'' complained that "My Happiness" was not as "hummable" as past singles "[[Passenger (Powderfinger song)|Passenger]]" or "[[These Days (Powderfinger song)|These Days]]".<ref name=hummable>{{cite news|title=Other New Singles|publisher=''[[The Sydney Morning Herald]]''|author=Jinman, Richard|date=18 August 2000|page=25}}</ref> Devon Powers of [[PopMatters]] described it, and "[[Waiting for the Sun (Powderfinger song)|Waiting for the Sun]]", as sounding bored.<ref>{{cite web|url=http://www.popmatters.com/music/reviews/p/powderfinger-odyssey.shtml|title=Powderfinger: Odyssey #5 review|publisher=[[PopMatters]]|author=Powers, Devon}} Accessed 22 August 2008.</ref> ''[[North-West Evening Mail|The Evening Mail]]'' agreed; it argued the "rock-lite" song, while sounding [[wiktionary:lush|lush]], failed to "make you really sit up and take notice".<ref>{{cite news|title=Music - Singles|publisher=''[[North-West Evening Mail|The Evening Mail]]''|date=7 September 2001|page=48}}</ref>

In his highly negative review of ''Odyssey Number Five'', [[Allmusic]]'s Dean Carlson labelled it one of the best songs, for the [[riff]] Powderfinger executed "better than most bands of their stature".<ref>{{cite web|url={{Allmusic|class=album|id=r524182|pure_url=yes}} |title=Odyssey Number Five > Review|author=Carlson, Dean|publisher=[[Allmusic]]}} Accessed 22 August 2008.</ref> Adams also enjoyed the song's "wobbly guitar",<ref name=AdamsSingles /> and ''[[Sain (magazine)|Sain]]''{{'}}s Christie Eliszer approved of the "acoustic strumalong",<ref>{{cite news|title=Five Easy Pieces|publisher=''[[Sain (magazine)|Sain]]''|author=Eliszer, Christie|date=September 2000}}<!-- http://www.ozmusic-central.com.au/powderfinger/text/articl29.htm --></ref> but ''[[The Advertiser (Adelaide)|The Advertiser]]''{{'}}s Michael Duffy said the song was "a familiar piece of yearning guitar indie that is polished but pedestrian"; he reserved his praise for "My Kind of Scene", which he described as akin to the best of ''Internationalist''.<ref name=advertiser>{{cite news|title=Music Review|publisher=''[[The Advertiser (Adelaide)|The Advertiser]]''|author=Duffy, Michael|date=24 August 2000|page=52}}</ref> Darren Bunting wrote in the ''[[Hull Daily Mail]]'' that "My Happiness" was the best song on ''Odyssey Number Five'', praising "soaring vocals, heartfelt lyrics and chiming guitar".<ref>{{cite news|title=Summer sound of Powderfinger.|publisher=''[[Hull Daily Mail]]''|author=Bunting, Darren|date=7 September 2001}}</ref> ''[[Entertainment Weekly]]''{{'}}s Marc Weingarten said that on "My Happiness", "Fanning's heavy heart is tattered by scratching and clawing guitars".<ref>{{cite web|url=http://www.ew.com/ew/article/0,,280590,00.html |title=Odyssey Number Five {{!}} Music Review|publisher=''[[Entertainment Weekly]]''|author=Weingarten, Marc|date=30 March 2001}} Accessed 22 August 2008.</ref>

== Charts ==
{| class="wikitable"
! Chart (2000–01)
! Peak<br />position
|-
| align="left"| [[Australian Recording Industry Association|ARIA]] [[ARIA Charts|Singles Chart]]
| align="center"| 4<ref name=AusCharts />
|-
| align="left"| [[RIANZ|RIANZ Singles Chart]]
| align="center"| 7<ref name=NZCharts />
|-
| align="left"| [[Billboard (magazine)|Billboard]] [[Modern Rock Tracks]]
| align="center"| 23<ref name=BBCharts />
|}

== Awards ==
{| class="wikitable"
|-
! Year
! Organisation
! Ceremony
! Award
! Result
|-
|2000
|align=center|[[Triple J]]
|[[Triple J Hottest 100, 2000|Hottest 100]]
|align=center|<small>N/A</small>
| #1<ref name=JJJ />
|-
|rowspan=2|2001
|align=center|[[Australasian Performing Right Association|APRA]]
|[[APRA Awards (Australia)|APRA Awards]]
| Song of the Year
|style="background-color: #dfd"| Won<ref name=APRA />
|-
|align=center|[[Australian Recording Industry Association|ARIA]]
|[[ARIA Music Awards of 2001|ARIA Music Awards]]
| Single of the Year
|style="background-color: #dfd"| Won<ref name="arias-list" />
|}

== Track listing ==
# "My Happiness" – 4:36
# "[[My Kind of Scene]]" – 4:37
# "Nature Boy" – 3:12
# "Odyssey #1" (Demo) – 4:09

== Personnel ==
<big>'''Powderfinger'''</big>
* [[Bernard Fanning]] – [[Vocal]]s, [[tambourine]]<ref name=AMGcreidts>{{cite web|url={{Allmusic|class=album|id=r524182|pure_url=yes}} |title=Odyssey Number Five > Credits|publisher=[[Allmusic]]}} Accessed 22 August 2008.</ref>
* [[Darren Middleton]] – Guitars and backing vocals
* [[Ian Haug]] – Guitars
* [[John Collins (Australian musician)|John Collins]] – [[Bass guitar]]s
* [[Jon Coghill]] – [[Drum]]s and [[percussion]]

<big>'''Production'''</big>
* Nick DiDia – [[Record producer|Producer]], [[Audio engineer|engineer]] and [[Audio mixing (recorded music)|mixer]]<ref name=AMGcreidts />
* Matt Voigt – Assistant engineer
* Anton Hagop – Assistant engineer
* Stewart Whitmore – Digital editing
* Stephen Marcussen – Mastering
* Anton Hagop – Assistant producer
* Kevin Wilkins – [[Art direction]] and [[photography]]

== References ==
{{Reflist|30em}}

==External links==
* {{MetroLyrics song|powderfinger|my-happiness}}<!-- Licensed lyrics provider -->

{{S-start}}
{{succession box
| title= [[Triple J Hottest 100|Triple J Hottest 100 Winner]]
| before= [[These Days (Powderfinger song)|These Days]] by Powderfinger
| after= [[Amazing (Alex Lloyd song)|Amazing]] by [[Alex Lloyd]]
| years= 2000
}}
{{S-end}}

{{Powderfinger}}
{{ARIA Award for Single of the Year 2000s}}
{{APRA Award for Song of the Year}}

{{Featured article}}

[[Category:APRA Award winners]]
[[Category:ARIA Award-winning songs]]
[[Category:Songs written by Bernard Fanning]]
[[Category:Songs written by Jon Coghill]]
[[Category:Songs written by John Collins (Australian musician)]]
[[Category:Songs written by Ian Haug]]
[[Category:Songs written by Darren Middleton]]
[[Category:Powderfinger songs]]
[[Category:2000 singles]]
[[Category:2000 songs]]
[[Category:Universal Music Group singles]]