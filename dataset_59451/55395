{{Infobox journal
| title = Albany Government Law Review
| cover = [[File:Albany_Law_Reviews.jpg|150px]]
| former_name = <!-- or |former_names= -->
| abbreviation = Albany Gov. Law Rev.
| discipline = [[Law]]
| editor = Brenda T. Baddam
| publisher = [[Albany Law School]]
| country = United States
| history = 2007-present
| frequency = Biannual
| openaccess = Yes
| license = 
| impact = 
| impact-year = 
| ISSN = 2328-2975
| eISSN = 2328-3033
| CODEN =
| JSTOR = 
| LCCN = 2008250055
| OCLC = 196429571
| website = http://www.albanygovernmentlawreview.org/Pages/home.aspx
| link2 = http://www.albanygovernmentlawreview.org/archives/Pages/default.aspx
| link2-name = Online archive
}}
The '''''Albany Government Law Review''''' is a biannual student-edited [[law review]] at [[Albany Law School]].<ref>Albany Law School, [http://www.albanylaw.edu/academics/Pages/Journals.aspx "Journals & Publications"]</ref> It covers legal aspects of government and public policy. The journal hosts a symposium each year. In 2011 and 2012, the journal published a third issue focused on New York legislation.  

== History ==
The ''Albany Government Law Review'' replaced the ''Environmental Outlook Journal'' in the 2007-2008 academic year.<ref>Carol DeMare, [http://timesunion.com/AspStories/story.asp?storyID=669265&category=LAWBEATCOL&BCCode=&newsdate=3/5/2008 "School honors American Indian legal pioneer, New law journal,"] Albany Times Union, March 5, 2008{{dead link|date=May 2015}}</ref>

== Editors-in-chief ==
The following persons have been [[editor-in-chief]] of the journal:
{{columns-list|colwidth=30em|
* Monique F. Mazza (2007–2008)
* Molly Adams Breslin (2008–2009)
* Benjamin L. Loefke (2009–2010)
* Robert S. Barrows (2010–2011)
* Andrew M. Stengel (2011–2012; resigned prior to official completion of term)
* Katie Valder (2012–2013)
* Heath Hardman (2013–2014)
* Michael LiPetri (2014–2015)
* Christopher D. Acosta (2015-2016)
* Brenda T. Baddam (2016-2017)
}}

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.albanygovernmentlawreview.org/Pages/home.aspx}}

[[Category:American law journals]]
[[Category:Biannual journals]]
[[Category:Law and public policy journals]]
[[Category:English-language journals]]
[[Category:Publications established in 2007]]