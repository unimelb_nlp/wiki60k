{{Infobox astronaut
| name          =Lodewijk van den Berg
| image         =Lodewijk van den Berg.jpg
| type          =[[EG&G]] Payload Specialist
| birth_date    ={{Birth date and age|1932|03|24}}
| birth_place   =[[Terneuzen|Sluiskil]], Netherlands
| occupation    =[[Chemical engineer]]
| time          =7d 00h 08m
| mission       =[[STS-51-B]]
| insignia      =[[Image:Sts-51-b-patch.png|30px]]
|}}
'''Lodewijk van den Berg''' ({{IPA-nl|ˈloːdəʋɛik vɑn dɛn ˈbɛrx}};{{efn|In isolation, ''van'' and ''den'' are pronounced, respectively, {{IPA-nl|vɑn|}} and {{IPA-nl|dɛn|}}.}} born March 24, 1932) is a [[Dutch American]] [[chemical engineer]], specializing in [[crystal growth]], who flew on a 1985 [[Space Shuttle Challenger|Space Shuttle ''Challenger'']] mission as a [[Payload Specialist]].

He was the first Dutch-born astronaut, a fact that is often ignored in the Netherlands because he was a [[naturalization|naturalized]] American and no longer a Dutch citizen at the time of flight.{{efn|Van den Berg became a naturalized citizen of the US in 1975. According to [http://www.minbuza.nl/en/Services/Consular_Services/Dutch_Nationality Dutch regulations] a person automatically loses the Dutch nationality if he voluntarily accepted another nationality before April 1, 2003. Van den Berg has stated that as of 2004 he never verified his Dutch citizenship status.}} He is married and has two children.<ref name="nasa-bio">{{cite web|url=http://www.jsc.nasa.gov/Bios/htmlbios/vandenberg-l.html |title=Payload Specialist Astronaut Bio: Lodewijk van den Berg |publisher=Lyndon B. Johnson Space Center, [[NASA]] |date=May 1985|accessdate=2008-04-09}}</ref> {{As of|2014}}, he resides in [[Florida]] and works as a chief scientist at the [[Constellation Technology Corporation]].<ref name="delft-integraal">{{Cite news|title=Niet Wubbo maar Lodewijk van den Berg was de eerste|last=van Engelen |first=Gert |periodical=Delft Integraal |issue=2005, 3 |pages=23–26  |date= |language=Dutch |url=http://www.tudelft.nl/live/pagina.jsp?id=2b28a3d0-2131-4a70-a161-e6154cbfb254&lang=nl&binary=/doc/DI05-3-5LodewijkvdBerg.pdf |accessdate=2008-04-09|postscript=<!--None-->}}</ref>

==Education and early career==
Van den Berg was born on March 24, 1932, in [[Sluiskil]], Netherlands. Van den Berg was educated in the Netherlands where he attended the [[Delft University of Technology]] from 1949 to 1961 and earned his [[Engineer's degree]] in [[chemical engineering]].<ref name="delft-integraal" /> He then moved to the United States to continue studying at the [[University of Delaware]], where he obtained a [[Master of Science|MSc]] degree in [[applied science]], followed by a [[Doctor of Philosophy|PhD]] degree in 1974, also in applied science.<ref name="nasa-bio" />

[[Image:Crystal in VCGS furnace.jpg|thumb|A crystal in the VCGS furnace]]
After he had completed his doctoral study, he was offered a job at [[EG&G Corporation]] Energy Measurements in [[Goleta, California]], to work on crystal growth. EG&G was a [[defense contractor]] of the United States government and dealt with sensitive information and science. In 1975, this required Van den Berg to become a naturalized U.S. citizen. Van den Berg gathered years of research and management experience in the preparation of [[crystalline]] materials—in particular, the growth of single crystals of chemical compounds, and the investigation of associated defect chemistry and electronic properties. He became an international authority on vapor growth techniques with an emphasis on [[Mercury(II) iodide|mercuric iodide]] crystals and its application in the nuclear industry as [[gamma ray]] detectors. During his work at [[EG&G]], Van den Berg asked NASA for permission to conduct crystal growth experiments in space, which NASA approved.<ref name="nasa-bio" />

==Spaceflight==
===Selection===
[[Image:Vapor Crystal Growth System Furnace.jpg|thumb|The Vapor Crystal Growth System Furnace experiment of STS-51-B.]]
[[Image:STS51B-06-010.jpg|thumb|Lodewijk van den Berg observes the crystal growth aboard Spacelab.]]
Van den Berg and his colleagues designed the EG&G Vapor Crystal Growth System experiment apparatus for a Space Shuttle flight. The experiment required an in-flight operator and NASA decided that it would be easier to train a crystal growth scientist to become an astronaut, than it would be the other way around. NASA asked EG&G and Van den Berg to compile a list of eight people who would qualify to perform the science experiments in space and to become a [[Payload Specialist]]. Van den Berg and his chief, Dr. Harold A. Lamonds could only come up with seven names. Lamonds subsequently proposed adding Van den Berg to the list, joking with Van den Berg that due to his age, huge glasses and little strength, he would probably be dropped during the first selection round; but at least they would have eight names. Van den Berg agreed to be added to the list, but didn't really consider himself being selected to be a realistic scenario.<ref name="delft-integraal" /><ref name="netwerk">{{cite video|title=De `vergeten astronaut` |url=http://www.netwerk.tv/node/3884 |medium=documentary |publisher=[[Netwerk]], [[NCRV]] and [[Evangelische Omroep|EO]] |accessdate=2008-04-09 }}</ref>

The first selection round, consisted of a selection based on science qualifications in the field in question, which Van den Berg easily passed. The final four candidates were tested on physical and mental qualifications which he also passed, while two of the others failed due to possible heart issues. He was now part of the final two, and NASA always trains two astronauts, a prime and a back-up. In 1983 he started to train as an astronaut and six months before the launch he was told that he would be the prime astronaut, much to his own surprise. When he went into space he was 53 years old, making him one of the oldest [[rookie]] astronauts.<ref name="delft-integraal" /><ref name="netwerk" />

===STS-51B===
Van den Berg was assigned as [[Payload Specialist]] on [[STS-51B]] ''Challenger'' (April 29–May 6, 1985). STS-51B, the Spacelab-3 mission, was launched from the [[Kennedy Space Center]], [[Florida]], and returned to land at [[Edwards Air Force Base]], [[California]]. It was the first operational [[Spacelab]] mission. The seven-man crew aboard ''Challenger'' conducted investigations into crystal growth, drop dynamics leading to containerless material processing, atmospheric trace gas spectroscopy, solar and planetary atmospheric simulation, cosmic rays, and laboratory-animal and human medical monitoring.

As a co-investigator of the Vapor Crystal Growth System (VCGS) experiment, Van den Berg was responsible for the crystal growth aspects of the VCGS experiment. He had intimate knowledge of VCGS and Fluid Experiment System (FES) hardware, and had participated in all major design and science reviews of those systems.

By the end of the mission, Van den Berg had traveled over 2.9 million miles in 110 Earth orbits, and logged over 168 hours in space.<ref name="nasa-bio" />

==Career after NASA==
After returning to Earth, Van den Berg continued to work on crystal growth experiments at EG&G in California and he became the head for the section of [[materials science]]. At a later time he moved to [[Florida]] to become a chief scientist at the [[Constellation Technology Corporation]]. At age 72, he continued to work up to 40 hours a week and grow crystals, a process he compares to gardening.<ref name="netwerk" /> The crystals he grows (Mercuric Iodide crystals)<ref>[http://www.contech.com/Mercuric_Iodide_Detectors.htm Mercuric Iodide Detectors<!-- Bot generated title -->]</ref> are used to make precision detectors for [[nuclear radiation]]. These detectors are used in medical applications, by the defense industry and the [[International Atomic Energy Agency]].<ref name="netwerk" />

He visits the Netherlands every two years,<ref name="delft-integraal" /> and was the subject of a short 2004 documentary by [[Netwerk]] called: ''The `forgotten astronaut`''.<ref name="netwerk" />

==Asteroid==
On September 28, 2007 the [[main belt]] [[asteroid]] 11430 (9560 P-L) was named after him and is now known as [[11430 Lodewijkberg]].<ref>{{cite web|url=http://ssd.jpl.nasa.gov/sbdb.cgi?sstr=11430|title=JPL Small-Body Database Browser: 11430 Lodewijkberg (9560 P-L)|accessdate=2008-06-26|publisher=NASA, [[Jet Propulsion Lab]]}}
</ref> The asteroid was discovered October 17, 1960 by [[Cornelis Johannes van Houten]] and [[Ingrid van Houten-Groeneveld]] at [[Leiden Observatory]], where they were studying [[photographic plate]]s taken by [[Tom Gehrels]] using the [[Palomar Observatory]]'s [[Samuel Oschin telescope]].<ref>{{cite web|url=http://www.astronomie.nl/nieuws/669/elf__nederlanders_geven_naam_aan_planetoïde.html|title=Elf Nederlanders geven naam aan planetoïde|date=2007-10-28|accessdate=2008-06-26}}</ref>

==Academic publications (incomplete)==
:*"Fabrication of mercuric iodide radiation detectors", Lodewijk van den Berg and Ron D. Vigil, ''Nuclear Instruments and Methods in Physics Research Section A: Accelerators,Spectrometers,Detectors and Associated Equipment'', Volume 458, Issues 1-2, 1 February 2001, Pages 148-151

:*"Improved yield of high resolution mercuric iodide gamma-ray spectrometers", Vernon Gerrish and Lodewijk van den Berg, ''Nuclear Instruments and Methods in Physics Research Section A: Accelerators, Spectrometers, Detectors and Associated Equipment'', Volume 299, Issues 1-3, 20 December 1990, Pages 41-44

:"Vapor growth of HgI2 by periodic source or crystal temperature oscillation", by M. Schieber∗, W.F. Schnepple, L. Van den Berg. Journal of Crystal Growth, Volume 33, Issue 1, April 1976, Pages 125–135

==Notes==
{{noteslist}}

==References==
{{reflist}}

==External links==
{{commons category|Lodewijk van den Berg}}
*[http://www.contech.com/ Constellation Technology Corporation]
*[http://www.spacefacts.de/bios/astronauts/english/vandenberg_lodewijk.htm Spacefacts biography of Lodewijk van den Berg]

{{good article}}

{{DEFAULTSORT:Berg, Lodewijk Van Den}}
[[Category:1932 births]]
[[Category:Living people]]
[[Category:American astronauts]]
[[Category:American people of Dutch descent]]
[[Category:Delft University of Technology alumni|Berg, Lodewijk Van Den]]
[[Category:Dutch astronauts|Berg, Lodewijk Van Den]]
[[Category:Dutch emigrants to the United States|Berg, Lodewijk Van Den]]
[[Category:Dutch expatriates in the United States]]
[[Category:People from Terneuzen|Berg, Lodewijk Van Den]]
[[Category:University of Delaware alumni]]
[[Category:People with acquired American citizenship]]