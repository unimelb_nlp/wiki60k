{{pp-move-indef}}
{{Taxobox
| name = Rufous-crowned sparrow
| image = Rufous Crowned Sparrow A.r. eremoeca Texas.jpg
| image_caption = ''Aimophila ruficeps eremoeca'', in Texas
| status = LC
| status_system = IUCN3.1
| status_ref = <ref name=IUCN>{{cite journal | author = BirdLife International | author-link = BirdLife International | title = ''Aimophila ruficeps'' | journal = [[IUCN Red List of Threatened Species]] | volume = 2012 | page = e.T22721288A39944668 | publisher = [[IUCN]] | year = 2012 | url = http://www.iucnredlist.org/details/22721288/0 | doi = 10.2305/IUCN.UK.2012-1.RLTS.T22721288A39944668.en | accessdate = 27 August 2016}}</ref>
| regnum = [[Animal]]ia
| phylum = [[Chordate|Chordata]]
| classis = [[Bird|Aves]]
| ordo = [[Passerine|Passeriformes]]
| familia = [[Emberizidae]]
| genus = ''[[Aimophila]]''
| species = '''''A. ruficeps'''''
| binomial = ''Aimophila ruficeps''
| binomial_authority = ([[John Cassin|Cassin]], 1852)
| range_map = Aimophila_ruficeps_map.svg
| range_map_caption = Range of ''A. ruficeps'' resident populations
| subdivision_ranks = [[Subspecies]]
| subdivision = ''See text''
| synonyms = ''Ammodramus ruficeps''<ref name=Byers/><br/>
''Peucaea ruficeps''<ref name=Storer/>
}}

The '''rufous-crowned sparrow''' (''Aimophila ruficeps'') is a small [[American sparrow]]. This [[passerine]] is primarily found across the [[Southwestern United States]] and much of the interior of Mexico, south to the [[Trans-Mexican Volcanic Belt|transverse mountain range]], and to the Pacific coast to the southwest of the transverse range. Its distribution is patchy, with populations often being isolated from each other. Twelve [[subspecies]] are generally recognized, though up to eighteen have been suggested. This bird has a brown back with darker streaks and gray underparts. The crown is [[rufous]], and the face and [[supercilium]] are gray with a brown or rufous streak extending from each eye and a thick black [[Cheek|malar]] streak.

These sparrows feed primarily on seeds in the winter and insects in the spring and summer. The birds are often territorial, with males guarding [[territory (animal)|their territory]] through song and displays. Flight is awkward for this species, which prefers to hop along the ground for locomotion. They are monogamous and breed during spring. Two to five eggs are laid in the bird's nest, which is cup-shaped and well hidden. Adult sparrows are preyed upon by house cats and small [[bird of prey|raptors]], while young may be taken by a range of mammals and reptiles. They have been known to live for up to three years, two months. Although the species has been classified as [[least concern]], or unthreatened with extinction, some subspecies are threatened by habitat destruction and one may be extinct.

==Taxonomy==
[[File:Ammodromus ruficeps Cassin.jpg|thumb|left|Drawing joined to the original description by John Cassin]]
This bird belongs to the family [[Emberizidae]], which consists of the American sparrows and Eurasian [[Bunting (bird)|buntings]]. The American sparrows are seed-eating New World birds with conical bills, brown or gray plumage, and distinctive head patterns. Birds in the genus ''[[Aimophila]]'' tend to be medium-sized at {{convert|5|to|8|in|cm|.01}} in length, live in arid [[scrubland]], have long bills and tails in proportion to their body size as well as short, rounded wings, and build cup-shaped nests.<ref name=Howell/><ref name=Omari/>

The rufous-crowned sparrow was described in 1852 by American ornithologist [[John Cassin]] as ''[[Ammodramus]] ruficeps''.<ref name=Byers/> It has also been described as belonging to the genus ''Peucaea'', which contains several sparrows in the genus ''Aimophila'' that share characteristics, such as a larger bill and a patch of yellow under the bend of the wing, that other members of the genus do not.<ref name=Storer/><ref name=Wolf1977/> However, splitting the ''Peucaea'' sparrows into a separate genus is not generally recognized.<ref name=Byers/><ref name=ITIS/> A 2008 [[phylogenetic]] analysis of the genus ''Aimophila'' divided it into four genera, with the rufous-crowned sparrow and its two closest relatives, the [[Oaxaca sparrow]] and [[rusty sparrow]], being maintained as the genus ''Aimophila''.<ref name=DaCosta/> In addition, this study suggested that the rufous-crowned sparrow may be more closely related to the brown [[towhee]]s of the genus ''Pipilo'' than the other members of the historical genus ''Aimophila''.<ref name=DaCosta/>

The derivation of the current genus name, ''Aimophila'', is from the [[Greek language|Greek]] ''aimos''/ἀιμος, meaning "thicket", and ''-philos''/-φιλος, meaning "loving".<ref name=Holloway2003/> The [[Specific name (zoology)|specific epithet]] is a literal derivation of the common name, derived from the [[Latin]] ''rufus'', meaning "reddish" or "tawny", and ''-ceps'', from ''caput'', meaning "head".<ref name=Simpson1979/> The bird is also occasionally referred to colloquially as the rock sparrow because of its preference for rocky slopes.<ref name=Thorngate/>

===Subspecies===
Twelve subspecies are generally recognized,<ref name=Byers/> although sometimes up to eighteen are named.<ref name=ITIS/>
* ''A. r. ruficeps'', the [[Subspecies#Nomenclature|nominate subspecies]], was described by Cassin in 1852.<ref name=MBR/> It is found in the coastal ranges of California and on the western slopes of the [[Sierra Nevada (U.S.)|Sierra Nevada]].<ref name=Clements/> This subspecies is darker and noticeably smaller than ''A. r. eremoeca'' and has distinct rufous-brown streaking on its upperparts.<ref name=Byers/>
* ''A. r. canescens'' was described by American ornithologist [[W. E. Clyde Todd]] in 1922,<ref name=MBR/> and it is found in southwestern California and northeast [[Baja California]] as far east as the base of the [[Sierra de San Pedro Mártir|San Pedro Mártir]].<ref name=Clements/> While the species itself is listed as of least concern, this subspecies is listed as a "species of special concern" by the [[California Department of Fish and Wildlife]], signifying that this population is threatened with [[extinction]].<ref name=Thorngate/> It appears to be extremely similar to ''A. r. ruficeps'' but is darker.<ref name=Byers/>
* ''A. r. obscura'', described by Donald R. Dickey and Adriaan van Rossem in 1923,<ref name=MBR/> is found in the [[Channel Islands of California]] on [[Santa Cruz Island|Santa Cruz]], [[Anacapa Island|Anacapa]], and formerly on [[Santa Catalina Island, California|Santa Catalina]].<ref name=Thorngate/><ref name=Clements/> While the Santa Catalina population has not been observed since 1863, the subspecies seems to have colonized Anacapa Island.<ref name=Thorngate/> No records exist of them before 1940.<ref name=Johnson1972/> This subspecies is similar to ''A. r. canescens'' but is darker.<ref name=Byers/>
* ''A. r. sanctorum'' was described by van Rossem in 1947.<ref name=MBR/> It was found on the [[Isla Todos Santos|Todos Santos Islands]] off the coast of northwest Baja California.<ref name=Clements/><ref name=Howell1917/> This subspecies is believed to be extinct.<ref name=Donlan2000/><!-- not been seen since the 1970s. needs source--><ref name=Thorngate/> This is the darkest of the coastal subspecies, especially on its underbelly.<ref name=Byers/>
* ''A. r. sororia'' was described by [[Robert Ridgway]] in 1898,<ref name=MBR/> and is found in the mountains of southern Baja California, specifically the [[Sierra de la Laguna]].<ref name=Clements/> It is the palest of the coastal subspecies.<ref name=Byers/>
* ''A. r. scottii'', described by George Sennett in 1888,<ref name=MBR/> is found from northern [[Arizona]] to [[New Mexico]] south to northeastern [[Sonora]] and northwestern [[Coahuila]].<ref name=Clements/> It appears to be a darker gray than ''A. r. eremoeca'' and has narrower and darker rufous streaks on its breast.<ref name=Byers/>
* ''A. r. rupicola'' was described by van Rossem in 1946.<ref name=MBR/> It is found in the mountains of southwestern Arizona.<ref name=Clements/> It is similar in appearance to ''A. r. scottii'' but is darker and grayer on its back.<ref name=Byers/>
* ''A. r. simulans'' was described by van Rossem in 1934,<ref name=MBR/> and it is found in northwestern Mexico from southeastern Sonora and southwestern [[Chihuahua (state)|Chihuahua]] to [[Nayarit]] and northern [[Jalisco]].<ref name=Clements/> It has more rufous coloration on its back and is paler on its underbelly than ''A. r. scottii''.<ref name=Byers/>
[[File:AimophilaRuficeps.svg|thumb|Rufous-crowned sparrow general characteristics]]
* ''A. r. eremoeca'' was described by N. C. Brown in 1882.<ref name=MBR/> It is found from southeastern Colorado to New Mexico, [[Texas]], northern Chihuahua, and central Coahuila.<ref name=Clements/> It has grayish upperparts and a dark breast.<ref name=Byers/>
* ''A. r. fusca'', described by [[Edward William Nelson]] in 1897,<ref name=MBR/> is found in western Mexico from southern Nayarit to southwestern Jalisco, northern [[Colima]], and [[Michoacán|Michoacan]].<ref name=Clements/> It is darker and more rufous on its upperparts than ''A. r. australis''. It also possesses a darker rufous crown which does not show a gray stripe down the middle.<ref name=Byers/>
* ''A. r. boucardi'' was described by [[Philip Sclater]] in 1867,<ref name=MBR/> and it is found in eastern Mexico from southern Coahuila to [[San Luis Potosí]], northern [[Puebla]], and southern [[Oaxaca]].<ref name=Clements/> This subspecies is darker than ''A. r. eremoeca'' and has dull brown, not rufous, streaking on the chest.<ref name=Byers/>
* ''A. r. australis'', described by Edward William Nelson in 1897,<ref name=MBR/> occurs in southern Mexico from [[Guerrero]] to southern Puebla and Oaxaca.<ref name=Clements/> ''A. r. scottii'' is similar in appearance, but this subspecies is smaller and has a shorter bill.<ref name=Byers/>
The other six subspecies that are occasionally recognized are ''A. r. extima'' and ''A. r. pallidissima'', which were described by A. R. Phillips in 1966, ''A. r. phillipsi'', which was described by J.P. Hubbard and Crossin in 1974, and ''A. r. duponti'', ''A. r. laybournae'', and ''A. r. suttoni'', which were described by J.P. Hubbard in 1975.<ref name=ITIS/>

==Description==
[[File:ZonotrichiaBoucardiSmit.jpg|thumb|''A. r. boucardi'']]
The rufous-crowned sparrow is a smallish sparrow at {{convert|5.25|in|cm|.01}} in length, with males tending to be larger than females.<ref name=Byers/><ref name=Omari/><ref name=MBR/> It ranges from {{convert|15|to|23|g|oz|abbr=on}} in weight and averages about {{convert|19|g|oz|abbr=on}}.<ref name=Omari/> It has a brown back with darker streaks and gray underparts. Its [[wing]]s are short, rounded, and brown and lack wingbars, or a line of feathers of a contrasting color in the middle of the bird's wing. The sparrow's tail is long, brown, and rounded. The face and [[supercilium]] (the area above the eye) are gray with a brown or rufous streak extending from each eye and a thick black streak on each cheek.<ref name=MBR/> The [[Crown (anatomy)|crown]] ranges from rufous to chestnut, a feature which gives it its common name, and some subspecies have a gray streak running through the center of the crown.<ref name=Byers/><ref name=Howell/> The bill is yellow and cone-shaped.<ref name=MBR/> The sparrow's throat is white with a dark stripe. Its legs and feet are pink-gray.<ref name=Byers/> Both sexes are similar in appearance, but the juvenile rufous-crowned sparrow has a brown crown and numerous streaks on its breast and flanks during the spring and autumn.<ref name=MBR/>

The song is a short, fast, bubbling series of ''chip'' notes that can accelerate near the end, and the calls include a nasal ''chur'' and a thin ''tsi''.<ref name=Byers/> When threatened or separated from its mate, the sparrow makes a ''dear-dear-dear'' call.<ref name=Kaufman/>

==Distribution and habitat==
[[File:Chaparral1.jpg|thumb|[[Chaparral]] nesting habitat in California]]
This bird is found in the southwestern United States and Mexico from sea level up to {{convert|9800|ft|m|.01}}, though it tends to be found between {{convert|3000|and|6000|ft|m|.01}}.<ref name=Byers/><ref name=Thorngate/> It lives in [[California]], southern [[Arizona]], southern [[New Mexico]], [[Texas]], and central [[Oklahoma]] south along [[Baja California]] and in western Mexico to southern [[Puebla]] and [[Oaxaca]]. In the [[midwestern United States]], the sparrow is found as far east as a small part of western [[Arkansas]], and also in a small region of northeastern [[Kansas]], its most northeastern habitat. The range of this species is discontinuous and is made up of many small, isolated populations.<ref name=Thorngate/> The rufous-crowned sparrow is a [[bird migration|non-migratory]] species, though the mountain subspecies are known to descend to lower elevations during severe winters.<ref name=Thorngate/> Male sparrows maintain and defend their [[Territory (animal)|territories]] throughout the year.<ref name=Thorngate/>

This sparrow is found in open oak woodlands and dry uplands with grassy vegetation and bushes. It is often found near rocky outcroppings. The species is also known from coastal scrublands and [[chaparral]] areas.<ref name=Byers/> The rufous-crowned sparrow thrives in open areas cleared by burning.<ref name=Thorngate/>

==Ecology and behavior==
The average territory size of rufous-crowned sparrows in the [[chaparral]] of California ranges from {{convert|2|acre|ha|.01}} to {{convert|4|acre|ha|.01}}.<ref name=Thorngate/> The density of territories varies by [[habitat]], including 2.5 to 5.8 territories per {{convert|99|acre|ha|.01}} of three- to five-year-old burned chaparral to 3.9 to 6.9 territories for the same amount of coastal scrubland.<ref name=Thorngate/> One pair tends to be supported by a territory, although birds without a mate have been seen sharing a territory with a mated pair.<ref name=Thorngate/>

This sparrow is awkward in flight and primarily uses running and hopping to move.<ref name=Omari/> The rufous-crowned sparrow will at times forage in pairs during the breeding season, and in family-sized flocks in late summer and early autumn. During the winter they can occasionally be found in loose [[mixed-species foraging flock]]s.<ref name=Thorngate/>

Predators of adult sparrows include house cats and small raptors like [[Cooper's hawk|Cooper's]] and [[sharp-shinned hawk]]s, [[American kestrel]]s, and [[white-tailed kite]]s.<ref name=Morrison2004/> The nests may be raided by a range of species including mammals and reptiles such as snakes, though nest predation has not yet been directly observed, and nesting sparrows have been observed using three kinds of [[distraction display|displays]] to distract potential predators; the ''rodent run'', the ''broken wing'', and the ''tumbling off the bush''.<ref name=Thorngate/> Birds adopt a ''rodent run'' display to distract predators. The head, neck and tail are lowered, wings held out, and feathers fluffed as the bird runs rapidly and voices a continuous alarm call.<ref name=Rowley1962/><ref name=Barrows2001/> In the ''broken wing'' display, the sparrow imitates having a broken wing by dropping one to the ground and hopping away from the nest with one wing dragging, leading the predator away until the bird ceases the act and escapes the predator.<ref name=Hauser1997/> The adult rufous-crowned sparrow distracts a nest predator by falling from the top of a bush to attract the predator to itself in the ''tumbling off the bush'' display.<ref name=Collins1999/>

The longest lifespan recorded for a rufous-crowned sparrow is three years, two months.<ref name=Omari/> Two species of [[tick]], ''[[Amblyomma americanum]]'' and ''[[Ixodes pacificus]]'', are known to parasitize the sparrow.<ref name=Omari/>

===Diet===
[[File:Rufous-crowned Sparrows.jpg|thumb|A pair in California]]
This sparrow feeds primarily on small grass and [[forb]] seeds, fresh grass stems, and tender plant shoots during autumn and winter.<ref name=Thorngate/> During these seasons, insects such as [[ant]]s, [[grasshopper]]s, [[ground beetle]]s, and [[scale insect]]s as well as [[spider]]s make up a small part of its diet. In the spring and summer, the bird's diet includes a greater quantity and variety of insects.<ref name=Kaufman/>

The rufous-crowned sparrow forages slowly on or near the ground by walking or hopping under shrubs or dense grasses.<ref name=Thorngate/> Though it occasionally forages in weedy areas, it is almost never observed foraging in the open. It has occasionally been observed feeding in branches and low shrubs.<ref name=Kaufman/> During the breeding season, it gleans its food from grasses and low shrubs.<ref name=Thorngate/> However, normally the species obtains its food by either pecking or less frequently scratching at leaf litter. This bird tends to forage in a small family group and in a limited area.<ref name=Kaufman/>

It is unknown whether this species obtains all of the water it needs from its food or if it must also drink; however, it has been observed both drinking and bathing in pools of water after rain storms.<ref name=Thorngate/>

===Reproduction===
The rufous-crowned sparrow breeds in sparsely vegetated scrubland. Males attract a mate by singing from regular positions at the edge of their territories throughout the [[breeding season]]. These birds are monogamous, taking only one mate at a time, and pairs often remain together for several years.<ref name=Thorngate/> If singing males come within contact of each other, they may initially raise their crowns and face the ground to display this feature; if that fails to make the other bird leave, they stiffen their body, droop their wings, raise their tails, and stick their head straight out.<ref name=Thorngate/> Males guard their territories year-round.<ref name=Thorngate/>

While it is not known when precisely the breeding season starts, the earliest that a sparrow has been observed carrying nesting material was on March 2 in southern California.<ref name=Thorngate/> The female bird builds a bulky, thick-walled open-cup nest typically on the ground, though occasionally in a low bush up to {{convert|18|in|cm|abbr=on}} above it, from dried grasses and rootlets, sometimes with strips of bark, small twigs, and weed stems.<ref name=Omari/><ref name=Thorngate/> Nests are well hidden, as they are built near bushes or tall grasses or overhanging rock with concealing vegetation.<ref name=Thorngate/> Once a sparrow chooses a nesting site, it tends to return to the site for many years.<ref name=Thorngate/> It lays between two and five eggs at a time and typically only raises one brood a year, though some birds in California have been observed raising two or even three broods a year.<ref name=Thorngate/><ref name=MBR/> In case of a nesting failure, replacement clutches may be laid.<ref name=Thorngate/> The eggs are an unmarked, pale bluish-white.<ref name=Kaufman/> Broods of the rufous-crowned sparrow have very occasionally been observed to be [[Brood parasite|parasitized]] by the [[brown-headed cowbird]].<ref name=Thorngate/><ref name=Miles1986/>

[[Avian incubation|Incubation]] of the eggs lasts 11 to 13 days and is performed solely by the female. The hatchlings are naked and quills do not begin to show until the third day. Only females brood the nestlings, though both parents may bring whole insects to their young. When a young rufous-crowned sparrow leaves the nest after eight or nine days, it is still incapable of flight, though it can run through the underbrush; during this time it is still fed by the parents. Juveniles tend to leave their parent's territory and move into adjacent habitat in autumn or early winter. Reproductive success varies strongly with annual rainfall and is highest in wet [[El Niño-Southern Oscillation|El Niño]] years, since cool rainy weather reduces the activity of snakes, the main predator of the sparrow's nests.<ref name=Morrison2002/>

==Conservation==
The rufous-crowned sparrow is treated as a species of [[least concern]], or not threatened with extinction, by [[BirdLife International]] due to its large geographical range of about {{convert|463323|mi2|km2|abbr=on}}, estimated population of 2.4&nbsp;million individuals, and lack of a 30% population decline over the last ten years.<ref name=BirdLife/> In years without sufficient rains, many birds fail to breed and those that do produce fewer offspring.<ref name=Morrison2004/><ref name=Bolger2005/> Some of the local populations of this bird are threatened and declining in number.<ref name=Thorngate/> The island subspecies and populations have declined in some cases: ''A. r. sanctorum'' of the Todos Santos Islands is believed to be extinct,<ref name=Donlan2000/> and the populations on Santa Catalina Island and Baja California's Islas de San Martin have not been observed since the early 1900s.<ref name=Thorngate/> Populations of the species in southern California are also becoming more restricted in range because of urbanization and agricultural development in the region. Additionally, the sparrow is known to have been poisoned by the rodenticide [[warfarin]], though more research is needed to determine the effects of pesticides on the rufous-crowned sparrow.<ref name=Thorngate/>

==References==
<!-- Condor7:134 -->
{{reflist|35em|refs=
<ref name=Barrows2001>{{cite book |last=Barrows |first=Edward M. |title=Animal Behavior Desk Reference |year=2001 |edition=2nd |publisher=CRC press |isbn=0-8493-2005-4 |page=177}}</ref>

<ref name=BirdLife>{{cite web |title=Species factsheet: ''Aimophila ruficeps'' |publisher=BirdLife International |year=2007 |url=http://www.birdlife.org/datazone/species/index.html?action=SpcHTMDetails.asp&sid=9023&m=0 |accessdate=19 January 2008 }}</ref>

<ref name=Bolger2005>{{cite journal |last1=Bolger |first1=Douglas T. |last2=Patten |first2=Michael A. |last3= Bostock |first3=David C. |year=2005 |title=Avian reproductive failure in response to an extreme climatic event |journal=Oecologia |volume=142 |pages=398–406 |accessdate=13 February 2009 |url=http://www.suttoncenter.org/2005%20Bolger%20et%20al%20Oecologia.pdf |archive-url=https://web.archive.org/web/20060908152338/http://www.suttoncenter.org:80/2005%20Bolger%20et%20al%20Oecologia.pdf |dead-url=yes |archive-date=8 September 2006 |doi=10.1007/s00442-004-1734-9 |pmid=15549403 |issue=3 }}</ref>

<ref name=Byers>{{cite book |last1=Byers |first1=Clive |last2=Curson |first2=Jon |last3=Olsson |first3=Urban |title=Sparrows and Buntings: A Guide to the Sparrows and Buntings of North America and the World |year=1995 |publisher=Pica Press |isbn=1-873403-19-4 |pages=296–297 }}</ref>

<ref name=Clements>{{cite book |last=Clements |first=James F. |authorlink=James Clements |title=[[The Clements Checklist of Birds of the World]] |edition=6th |publisher=Comstock Publishing Associates |year=2007 |location=Ithaca, NY |pages=681–682 |isbn=978-0-8014-4501-9}}</ref>

<ref name=Collins1999>{{cite encyclopedia |last=Collins |first=Paul W. |title=Rufous-crowned Sparrow (Aimophila ruficeps) |encyclopedia=The Birds of North America Online |publisher=Cornell Lab of Ornithology |location=Ithaca, NY |year=1999 |url=http://bna.birds.cornell.edu/bna/species/472/articles/behavior |doi=10.2173/bna.472 |accessdate=11 April 2009 |editor1-last=Poole |editor1-first=A. |editor2-last=Gill |editor2-first=F.}}</ref>

<ref name=DaCosta>{{cite journal |last=DaCosta |first=Jeffrey M. |first2=Garth M. |last2=Spellman |first3=Patricia |last3=Escalante |first4=John |last4=Klicka |title=A molecular systematic review of two historically problematic songbird clades: ''Aimophila'' and ''Pipilo'' |journal=Journal of Avian Biology |volume=40 |issue=2 |pages=206–216 |publisher=Munksgaard International |location=Copenhagen |year=2009 |doi=10.1111/j.1600-048X.2009.04514.x}}</ref>

<ref name=Donlan2000>{{cite book |accessdate=13 February 2009 |url=http://www.advancedconservation.org/library/donlan_etal_2000.pdf |archive-url=https://web.archive.org/web/20101128090419/http://www.advancedconservation.org/library/donlan_etal_2000.pdf |dead-url=yes |archive-date=28 November 2010 |last1=Donlan |first1=C.J. |last2=Tershy |first2=B.R. |last3=Keitt |first3=B.S. |last4=Wood |first4=B. |last5=Sanchez |first5=J.A. |last6=Weinstein |first6=A. |last7=Croll |first7=D.A. |last8=Alguilar |first8=J.L. |year=2000 |title=Island conservation action in northwest Mexico |editor-last1=Browne |editor-first1=D.H. |editor-last2=Chaney |editor-first2=H. |editor-last3=Mitchell |editor-first3=K. |work=Proceedings of the Fifth California Islands Symposium |pages=330–338 |publisher=Santa Barbara Museum of Natural History |location=Santa Barbara, California, USA}}</ref>

<ref name=Hauser1997>{{cite book |last=Hauser |first=Marc D. |title=The Evolution of Communication |publisher=MIT Press |year=1997 |location=Cambridge, MA |page=588 |url=https://books.google.com/?id=QbunyscCJBoC&pg=RA1-PA588 |isbn=0-262-58155-8 }}</ref>

<ref name=Holloway2003>{{cite book |title=Dictionary of Birds of the United States: Scientific and Common Names |last=Holloway |first=J.E. |year=2003 |publisher=Timber Press |location=Portland, Oregon |isbn=0-88192-600-0 |page=17}}</ref>

<ref name=Howell>{{cite book |last=Howell |first=Steve N.G. |first2=Sophie |last2=Webb |title=[[A Guide to the Birds of Mexico and Northern Central America]] |publisher=Oxford University Press |year=1995 |location=New York |isbn =0-19-854012-4 }}</ref>

<ref name=Howell1917>{{cite journal |last=Howell |first= Alfred Brazier |date=June 1917 |title=Birds of the islands off the coast of southern California |journal=Pacific Coast Avifauna |volume=12 |page=80 |url=http://sora.unm.edu/node/107 |accessdate=13 February 2009}}</ref>

<ref name=ITIS>{{cite web |title=ITIS Standard Report Page: ''Aimophila ruficeps'' |work=Integrated Taxonomic Information System |url=http://www.itis.gov/servlet/SingleRpt/SingleRpt?search_topic=TSN&search_value=179377&source |accessdate=12 April 2009}}</ref>

<ref name=Johnson1972>{{cite journal |last=Johnson |first=Ned K. |title=Origin and differentiation of the avifauna of the Channel Islands, California |pages=295–315 |journal=Condor |issue=3 |volume=74 |year=1972 |accessdate=13 February 2009 |url=http://sora.unm.edu/sites/default/files/journals/condor/v074n03/p0295-p0315.pdf |doi=10.2307/1366591 |jstor=1366591}}</ref>

<ref name=Kaufman>{{cite book |last=Kaufman |first=Kenn |authorlink=Kenn Kaufman |title=Lives of North American Birds |publisher=Houghton Mifflin |year=1996 |location=Boston |page=583 |isbn=0-618-15988-6}}</ref>

<ref name=MBR>{{cite web |last=Gough |first=Gregory |title=Rufous-crowned sparrow ''Aimophila ruficeps'' |work=Patuxent Bird Identification InfoCenter |publisher=USGS Patuxent Wildlife Research Center |date=28 December 2000 |url=http://www.mbr-pwrc.usgs.gov/Infocenter/i5800id.html |accessdate=18 January 2007}}</ref>

<ref name=Miles1986>{{cite journal |last=Miles |first=D.B. |year=1986 |title=A record of Brown-headed Cowbird ''Molothrus ater'' nest parasitism of Rufous-crowned Sparrows ''Aimophila ruficeps'' |jstor=3670570 |journal=Southwestern Naturalist |volume=31 |issue=2 |pages=253–254 |doi=10.2307/3670570}}</ref>

<ref name=Morrison2004>{{cite journal |last1=Morrison |first1=Scott A. |last2=Bolger |first2=Douglas T. |last3=Sillett |first3=Scott T. |year=2004 |title=Annual survivorship of the sedentary rufous-crowned sparrow (''Aimophila ruficeps''): no detectable effects of edge or rainfall in southern California |journal=The Auk |volume=121 |issue=3 |pages=904–916 |doi=10.1642/0004-8038(2004)121[0904:ASOTSR]2.0.CO;2 }}</ref>

<ref name=Morrison2002>{{cite journal |last1=Morrison |first1=Scott A. |last2=Bolger |first2=Douglas T. |date=November 2002 |title=Variation in a sparrow's reproductive success with rainfall: food and predator-mediated processes |journal=Oecologia |volume=133 |issue=3 |pages=315–324 |doi=10.1007/s00442-002-1040-3|jstor=4223423}}</ref>

<ref name=Omari>{{cite web |last=Omari |first=Amel |first2=Ann |last2=Fraser |title=Aimophila ruficeps |publisher=Animal Diversity Web |year=2007 |url=http://animaldiversity.ummz.umich.edu/site/accounts/information/Aimophila_ruficeps.html |accessdate=12 April 2009}}</ref>

<ref name=Rowley1962>{{cite journal |last=Rowley |first=Ian |year=1962 |title='Rodent-run' distraction display by a passerine, the Superb Blue Wren ''Malurus cyaneus'' (L.) |journal=Behaviour |volume=19 |pages=170–176 |doi=10.1163/156853961X00240|jstor=4533009}}</ref>

<ref name=Simpson1979>{{cite book |last=Simpson |first=D.P. |title=Cassell's Latin Dictionary |publisher=Cassell |year=1979 |edition=5th |location=London |isbn=0-304-52257-0 |page=883}}</ref>

<ref name=Storer>{{cite journal |last=Storer |first=Robert W. |year=1955 |title=A preliminary survey of the sparrows of the genus ''Aimophila'' |journal=The Condor |volume=57 |issue=4 |pages=193–201 |accessdate=13 February 2009 |url=http://sora.unm.edu/sites/default/files/journals/condor/v057n04/p0193-p0201.pdf |doi=10.2307/1365082 |jstor=1365082}}</ref>

<ref name=Thorngate>{{cite web |last=Thorngate |first=Nellie |first2=Monika |last2=Parsons |title=California Partners in Flight coastal scrub and chaparral bird conservation plan Rufous-crowned Sparrow (''Aimophila ruficeps'') |publisher=California Partners in Flight |year=2005 |url=http://www.prbo.org/calpif/htmldocs/species/scrub/rufous_crowned_sparrow.htm |accessdate=19 January 2008}}</ref>

<ref name=Wolf1977>{{cite book |title=Species relationships in the avian genus ''Aimophila'' |series=Ornithological Monographs |number=23 |publisher=American Ornithologists' Union |year=1977 |last=Wolf |first=Larry L. |url=http://sora.unm.edu/sites/default/files/journals/om/om023.pdf}}</ref>
}}

==Further reading==
{{refbegin}}
* {{cite journal |last=Baptista |first=L.F. |year=1973 |title=Leaf bathing in three species of emberizines |journal= Wilson Bulletin |volume=85 |issue=3 |pages=346–347 |url=http://sora.unm.edu/sites/default/files/journals/wilson/v085n03/p0346-p0347.pdf}}
* {{cite journal |last=Behle |first=W.H. |year=1976 |title=Mojave Desert avifauna in the Virgin River Valley of Utah Nevada and Arizona USA |jstor=1366914 |journal=Condor |volume=78 |issue=1 |pages=40–48 |doi=10.2307/1366914 |url=http://sora.unm.edu/sites/default/files/journals/condor/v078n01/p0040-p0048.pdf}}
* {{cite journal |last=Bolger |first=D.T. |year=2002 |title=Habitat fragmentation effects on birds in southern California: Contrast to the "top-down" paradigm |journal=Studies in Avian Biology |volume=25 |pages=141–157}}
* {{cite journal |last1=Bolger |first1=D.T. |last2=Scott |first2=T.A. |last3=Rotenberry |first3= J.T. |year=1997 |title=Breeding bird abundance in an urbanizing landscape in coastal Southern California |journal=Conservation Biology |volume=11 |issue=2 |pages=406–421 |doi=10.1046/j.1523-1739.1997.96307.x}}
* {{cite journal |last1=Borror |first1=D.J. |year=1971 |title=Songs of ''Aimophila'' sparrows occurring in the United States |journal=Wilson Bulletin |volume=83 |issue=2 |pages=132–151 |url=http://sora.unm.edu/sites/default/files/journals/wilson/v083n02/p0132-p0151.pdf}}
* {{cite journal |last1=Carson |first1=R.J. |last2=Spicer |first2=G.S. |year=2003 |title=A phylogenetic analysis of the emberizid sparrows based on three mitochondrial genes |journal=Molecular Phylogenetics & Evolution |volume=29 |issue=1 |pages=43–57 |doi=10.1016/S1055-7903(03)00110-6}}
* {{cite journal |last1=Deviche |first1=P. |last2=McGraw |first2=K. |last3=Greiner |first3=E.C. |year=2005 |title=Interspecific differences in hematozoan infection in Sonoran desert ''Aimophila'' sparrows |journal=Journal of Wildlife Diseases |volume=41 |issue=3 |pages=532–541 |pmid=16244063 |doi=10.7589/0090-3558-41.3.532}}
* {{cite thesis |last=Groschupf |first=K.D. |degree=Ph.D. |year=1983 |title=Comparative study of the vocalizations and singing behavior of four ''Aimophila'' sparrows |publisher=Virginia Polytechnic Institute and State University}}
* {{cite journal |last=Hardy |first= J.W. |year=1980 |title=The Oaxaca Sparrow ''Aimophila notosticta'' has a chatter vocalization |jstor=1366802 |journal=Condor |volume=82 |issue=1 |doi=10.2307/1366802 |page=111}}
* {{cite journal |last=Hubbard |first= J.P. |year=1975 |title=Geographic variation in non-California populations of the Rufous Crowned Sparrow |journal=[[Nemouria]] |volume=15 |pages=1–28}}
* {{cite thesis |last=Morrison |first=S.A. |degree=Ph.D. |year=2001 |title=Demography of a fragmentation-sensitive songbird: Edge and ENSO effects |publisher=Dartmouth College}}
* {{cite journal |last1=Morrison |first1=S.A. |last2=Bolger |first2=D.T. |year=2002 |title=Lack of an urban edge effect on reproduction in a fragmentation-sensitive sparrow |journal=Ecological Applications |volume=12 |issue=2 |pages=398–411 |doi=10.1890/1051-0761(2002)012[0398:LOAUEE]2.0.CO;2 }}
* {{cite journal |last1=Parker |first1=S.A. |last2=Stotz |first2=D. |year=1977 |title=An observation on the foraging behavior of the Arizona Ridge-Nosed Rattlesnake ''Crotalus willardi willardi'' (Serpentes: Crotalidae) |journal=Bulletin of the Maryland Herpetological Society |volume=13 |issue=2 |page=123}}
* {{cite journal |last1=Patten |first1=M.A. |last2=Bolger |first2=D.T. |year=2003 |title=Variation in top-down control of avian reproductive success across a fragmentation gradient |journal=Oikos |volume=101 |issue=3 |pages=479–488 |doi=10.1034/j.1600-0706.2003.12515.x}}
* {{cite journal |last1=Pulliam |first1=H.R. |last2=Mills |first2=G.S. |year=1977 |title=The use of space by wintering sparrows |jstor=1935091 |journal=Ecology |volume=58 |issue=6 |pages=1393–1399 |doi=10.2307/1935091}}
* {{cite journal |last1=Remsen |first1=J.V.J. |last2=Cardiff |first2=S. |year=1979 |title=First Records of the Race ''scottii'' of the Rufous-crowned Sparrow in California |journal=Western Birds |volume=10 |issue=1 |pages=45–46 |url=http://sora.unm.edu/sites/default/files/journals/wb/v10n01/p0045-p0046.pdf}}
* {{cite journal |last=Spicer |first=G.S. |year=1977 |title=Two new nasal mites of the genus ''Ptilonyssus'' Mesostigmata Rhinonyssidae from Texas USA |journal=Acarologia |volume=18 |issue=4 |pages=594–601}}
* {{cite thesis |last=Wimer |first=M.C. |degree=M.S. |year=1995 |title=Song variation in insular and mainland Rufous-crowned Sparrows |publisher=California State University, Long Beach |location=Long Beach, California}}
{{refend}}

==External links==
{{Commons category|Aimophila ruficeps}}
{{wikispecies|Aimophila ruficeps}}
* {{Xeno-canto species|Aimophila|ruficeps|Rufous-crowned sparrow}}
* {{VIREO|rufous-crowned+sparrow}}
* {{InternetBirdCollection|rufous-crowned-sparrow-aimophila-ruficeps|Rufous-crowned sparrow}}
* [http://digitalgallery.nypl.org/nypldigital/id?112454 Old illustration of the "Brown-headed Finch" from Cassin's Birds of California and Texas]
* {{ITIS |id=179377 |taxon=''Aimophila ruficeps'' |accessdate=24 February 2009 }}

{{taxonbar}}
{{featured article}}

{{DEFAULTSORT:sparrow, rufous-crowned}}
[[Category:Aimophila|rufous-crowned sparrow]]
[[Category:Birds of the Baja California Peninsula]]
[[Category:Birds of Cordillera Neovolcanica Mexico]]<!-- the www.natureserve.org RangeMaps show: Oklahoma, Kansas, Arkansas, and the two Mountain ranges of Mexico: west: Sierra Madre Occidental, and east: Sierra Madre Oriental. the bird prefers only the mountains of Mexico (?) -->
[[Category:Birds of Mexico]]
[[Category:Native birds of the Plains-Midwest (United States)]]
[[Category:Native birds of the Southwestern United States]]
[[Category:Fauna of the California chaparral and woodlands]]
[[Category:Near threatened fauna of North America]]
[[Category:Near threatened fauna of the United States]]
[[Category:Vulnerable biota of Mexico]]
[[Category:Birds described in 1852|rufous-crowned sparrow]]