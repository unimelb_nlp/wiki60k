{{Infobox journal
| title = Adult Education Quarterly 
| cover = [[File:Adult Education Quarterly.tif]]
| editor =  Jeff Zacharakis, Leona M. English, Catherine A. Hansman, Qi Sun  
| discipline = [[Social Sciences]]
| former_names = 
| abbreviation = Adult Educ. Q.
| publisher = [[Sage Publications]] on behalf of the [[American Association of Adult and Continuing Education]]
| country =
| frequency = Quarterly
| history = 1950-present
| openaccess = 
| license = 
| impact = 0.780 
| impact-year = 2014
| website = http://www.sagepub.com/journals/Journal200765/title
| link1 = http://aeq.sagepub.com/content/current
| link1-name = Online access
| link2 = http://aeq.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 9406042
| LCCN = 84642128 
| CODEN = 
| ISSN = 0741-7136 
| eISSN = 1552-3047 
}}
'''''Adult Education Quarterly''''' is a quarterly [[peer-reviewed]] [[academic journal]] covering the field of [[education]] It was established in 1950 and is published by [[Sage Publications]] on behalf of the [[American Association of Adult and Continuing Education]]. {{As of|2014}}, the [[editors-in-chief]] have been Jeff Zacharakis ([[Kansas State University]]), Leona M. English ([[St. Francis Xavier University]]), Catherine A. Hansman ([[Cleveland State University]]), and Qi Sun ([[University of Wyoming]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2014 [[impact factor]] is 0.780, ranking it 105th out of 224 journals in the category "Education & Educational Research".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Education & Educational Research|title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |work=[[Web of Science]]}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.sagepub.com/journals/Journal200765/title}}
* [http://www.aaace.org/ American Association of Adult and Continuing Education]

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1950]]
[[Category:Education journals]]