{{Orphan|date=June 2015}}

{{Use mdy dates|date=August 2015}}
{{Infobox person
| name          = John Grazier
| image         = John Grazier American artist.jpg
| caption       = Photo of John Grazier taken on High Rock, [[Appalachian Trail]], MD on 23 June 1992.
| birth_date    = {{birth date|1946|6|23}}
| birth_place   = [[Long Beach, New York]], US
| occupation    = [[Realism (visual arts)|Realist]] painter
}}

'''John Grazier''' (born 1946) is an American [[Realism (arts)|realist]] painter, working with [[India ink]] [[airbrush]], pencil and oil paint. He is an American artist of the late-20th century known for his meticulous cross-[[hatching]] technique,<ref name=RichardPaul(16September1978)>Richard, Paul (16 September 1978). "Portrait of the City; Crystallizing Show of Late-'70s Washington; Providing Realist Portraits of Life in the City." ''The Washington Post.'' Retrieved 10 March 2015.</ref> skewed perspective,<ref name=Forgey1975>Forgey, Benjamin (19 September 1975). "A Puzzling Perspective That Leaves You Absorbed.". ''The Washington Star''. Retrieved 10 March 2015.</ref> and a "dreamlike" representation of seemingly ordinary subjects,<ref name=FendrickGallery>"John Grazier: September 16 - October 11, 1975". ''Washington, DC: Fendrick Gallery.'' Retrieved 3 April 2015. http://arcade.nyarc.org/record=b594227~S8</ref><ref>{{cite news|last1=Tannous|first1=David|title=Jasper Johns Returns to the Fendrick|accessdate=4 April 2015|work=Washington Star-News|date=11 October 1974}}</ref> such as buses, coffee cups,<ref name=Lewis,JoAnn(10May1980)>Lewis, Jo Ann (10 May 1980). "Artistic Transport." ''The Washington Post.'' Retrieved 4 April 2015.</ref> office buildings,<ref name=HamillPete(1995)>Hamill, Pete (1995). ''Tools as Art: the Hechinger Collection.'' New York: Harry N. Abrahams. ISBN 0-8109-3873-1. http://www.petehamill.com/books/toolsasart.html</ref> Victorian-style porches,<ref name=Lewis,JoAnn(13June1990)>Lewis, Jo Ann (13 June 1990). "Down and Out, Up and Coming. Homeless Artist John Grazier, on the Way to Fame and Riches." ''The Washington Post.'' Retrieved 2 April 2015.</ref>  and phone booths.<ref>{{cite news|last1=Richard|first1=Paul|title=Three Art Shows in One Museum|accessdate=3 April 2015|work=The Washington Post|date=23 October 1974}}</ref>

== Early life ==

John Grazier was born in Long Beach, New York in 1946. His mother, Josephine Stine Grazier, attended [[Wellesley College]] and received an advanced degree at Harvard Graduate School. His father, back from [[World War II]], owned the Bellevue Inn, a hotel in [[Delaware Water Gap]], PA.<ref name=Lewis,JoAnn(13June1990)/> He was only two when his father was diagnosed with cancer, went bankrupt and died.<ref>{{cite web|title=John Grazier Biography|url=http://www.artnet.com/artists/john-grazier/biography|website=www.artnet.com|accessdate=4 April 2015}}</ref> Several of his paintings were based on his lingering childhood memories of his father’s hotel.<ref name=OSullivan(18May1996)>O’Sullivan, Michael (18 May 1996). "Drawing on Memory." ''The Washington Post.'' Retrieved 18 May 1996.</ref> In 1968 he went to study at the [[Corcoran College of Art and Design|Corcoran School of Art]], and in 1971-72 he attended the [[Maryland Institute College of Art]] for a year on a full scholarship. He has been awarded grants from the [[National Endowment for the Arts]] (1974) and the [[Pollock-Krasner Foundation]].<ref name=Lewis,JoAnn(13June1990)/> He also won first place in the 1975 Davidson National Print and Drawing Competition.<ref name=DavidsonCollege>{{cite web|title=Davidson College Library Archives.|url=http://library.davidson.edu/archives/davidsonian/PDFs/19770318.pdf|website=Davidson College |accessdate=6 August 2015}}</ref>

== Methods ==

John Grazier started drawing images of coffee cups, buses, diners, tunnels and bridges at the beginning of his career in 1973.<ref>{{cite news|last1=Brace|first1=Eric|title=Braking for Murals at a Former Depot|accessdate=4 April 2015|work=The Washington Post|date=18 December 1995}}</ref>

As [[the Washington Post]] art critic Jo Ann Lewis wrote of John Grazier’s technique and subject matter (1980):

“…Silent, unpeopled interiors with empty coffee cups, overlooking a parking lot full of buses… He starts with an overall design in his head, draws in the basic lines with a ruler and then fills in the images with free-hand cross-hatching that retains the integrity of each line...”<ref name=Lewis,JoAnn(10May1980)/>

During the 1990s, his subject matter evolved further, focusing on facades of [[Victorian architecture]] buildings,<ref name=Lewis,JoAnn(13June1990)/> railed porches and balconies, windows, elaborate moldings (“Porch of the Bellevue Inn”, “The Silence of the Attic”), phones (“Sunset Strip”) and drawing persons (“The Carousel of Dreams” in 1996).<ref name=OSullivan(18May1996)/>

In 2001, Grazier started working in color using oil paints.<ref name=Johnson(14October2001)>Johnson, Darragh (14 October 2001). "You Can't Eat Art; Critics agree, John Grazier is a wonderful artist. That, and a desperate door-to-door campaign to sell even one of his canvases, may keep a roof over his head. Or it may not." ''The Washington Post.'' Retrieved 10 March 2015 from ''http://www.washingtonpost.com/lifestyle/magazine/you-cant-eat-art/2011/08/01/gIQAj5wwpI_story.html''.</ref>

== Public Collections ==

His works, “House on a Hill in a Dream” (1974) and “Memory of a Porch” (1976), are in the permanent collection of the [[Smithsonian American Art Museum]].<ref name=Smithsonian>{{cite web|title=John Grazier|url=http://americanart.si.edu/collections/search/artwork/results/index.cfm?rows=10&q=&page=1&start=0&fq=name:%22Grazier%2C%20John%22|website=Smithsonian American Art Museum|accessdate=3 April 2015}}</ref> Another pencil drawing “End of the Line” (1980) is in the [[Art Institute of Chicago]].<ref name=ArtInstituteChicago>{{cite web|title=John Grazier|url=http://www.artic.edu/aic/collections/artwork/artist/Grazier,+John|website=The Art Institute of Chicago|accessdate=4 April 2015}}</ref> “Breaking Up”(1976) and “Memory of a Porch (1975) are in the [[National Gallery of Art]].<ref name=NatGalleryArt>{{cite web|title=John Grazier|url=http://www.nga.gov/content/ngaweb/global-site-search-page.html?searchterm=grazier&searchpath=%2Fcontent%2Fngaweb&pageNumber=1|website=The National Gallery of Art|accessdate=4 April 2015}}</ref>
The drawing “Passing Windows in Fall” is included in the Hechinger Collection “Tools as Art.”<ref name=HamillPete(1995)/> His works are also included in the permanent collections of the [[Library of Congress]],<ref name=Johnson(14October2001)/> the [[National Law Enforcement Museum]] in Washington, DC,<ref name=NationalPoliceMuseum>"John Grazier". ''National Law Enforcement Museum Insider, March 2011 Vol. 3(1).'' Retrieved 29 August 2015. http://www.nleomf.org/assets/pdfs/newsletters/museum_insider/Museum_Insider_March_2011.pdf</ref> and the [[Arkansas Arts Center]].<ref>{{cite web|title=John Grazier|url=https://www.yumpu.com/en/document/view/5978249/list-of-artists-in-the-arkansas-arts-center-foundation-collection-501/7|website=Arkansas Arts Center Foundation|accessdate=4 April 2015}}</ref> Two of his works, "Untitled" and "Rattling Windows", are included in the [[Pollock-Krasner Foundation]] image collection.<ref name=Pollock>{{cite web|title=John Grazier|url=http://www.pkf-imagecollection.org/artist/John_Grazier/works/#!3484|website=Pollock-Krasner Foundation|accessdate=23 July 2015}}</ref>

His works are also included in the following university collections: “City Lines” (1978) in [[Brandeis University]] (Waltham, Massachusetts), [[Davidson College]] (Davidson, North Carolina), [[Dartmouth College]] (Hanover, New Hampshire), [[University of Rochester]] (Rochester, New York), and [[Wellesley College]] (Wellesley, Massachusetts).<ref name=ZenithGallery>"John Grazier". ''Zenith Gallery.'' Retrieved 10 March 2015. http://www.zenithgallery.com/inventory/grazier/grazier.html</ref>

His work has been purchased by many law firms and corporations.<ref name=Johnson(14October2001)/> His work are also in the private collections of [[Jim Lehrer]],<ref name=Lewis,JoAnn(13June1990)/> Truland Systems (“Night of the Shooting Stars”, “Dreams of the Wild Child”, “Burning Bush”, "The Prosperous House", "Whispers in the Attic", “Rebecca’s Doll House”),<ref group=note>Robert Truland paid $188,275 on March 28 for several paintings his company previously bought from galleries such as the Corcoran and Zenith, including "The Prosperous House" and "Whispers in the Attic" by John Grazier. Truland Group had acquired the paintings over more than 20 years, according to the court document.</ref><ref name=WashBusJournal>{{cite web|title=Trustee to probe whether Robert Truland paid a fair price for company artwork|url=http://www.bizjournals.com/washington/breaking_ground/2014/10/trustee-to-probe-whether-robert-truland-paid-a.html?page=all|website=Washington Business Journal|accessdate=4 April 2015}}</ref><ref name=Hom(2011)>{{cite news|last1=Hom|first1=Kathleen|title=Whatever Happened To...the artist who shunned dealers to sell his own work?|url=http://www.washingtonpost.com/lifestyle/magazine/whatever-happened-tothe-artist-who-shunned-dealers-to-sell-his-own-work/2011/07/07/gIQA5j3ThI_story.html|accessdate=4 April 2015|work=The Washington Post|date=29 July 2011}}</ref> [[Nixon Peabody]], Hogan & Hartson (“The Visitor”, currently [[Hogan Lovells]]),<ref name=Johnson(14October2001)/> [[Owens Corning]] Corporation (Toledo, Ohio),<ref name=ZenithGallery/> Cyrus and Myrtle Katzen family (founders of the [[Katzen Arts Center]] at the [[American University]] in Washington, DC).<ref>{{cite news|last1=Lewis|first1=Jo Ann|title=Dentist Who Put Teeth In AU’s Artistic Ambition|url=http://www.washingtonpost.com/wp-dyn/content/article/2005/07/01/AR2005070100376.html|accessdate=10 March 2015|work=The Washington Post|date=3 July 2005}}</ref>

== One-Man Shows ==

In 1974, he had his first one-man show at the [[Baltimore Museum of Art]] (Baltimore, Maryland, 1974, 22 pieces, including “Sound of the Wind.”<ref>{{cite journal|title=Three Exhibitions: Boul, Grazier, Turnbul|journal=Baltimore Museum of Art Record (U.S.A.)|date=1974|volume=1|issue=5|pages=5–8|accessdate=10 March 2015}}</ref> David Tannous of Washington Star-News wrote of that show:  
“Grazier deals almost exclusively with sections of architectural exteriors…his perspective twists and changes unpredictably from point to point and in several planes at once… because of this, Grazier’s buildings stretch and pull in different directions…small delicate strokes of the pencil multiply into many-layered cross-hatchings…“<ref>{{cite news|last1=Tannous|first1=David|title=Jasper Johns Returns to the Fendrick|accessdate=10 March 2015|work=Washington Star-News|date=11 October 1974}}</ref>

His other one-man shows include: the Fendrick Gallery (“Memories of a Lady’s Lace”, “Tall Building”),<ref name=Forgey1975/> in Washington, DC (1975)<ref name=FendrickGallery/><ref>{{cite news|last1=Lewis|first1=Jo Ann|title=Summers Retrospective at the Phillips, or How to Lose Friends|accessdate=10 March 2015|work=The Washington Post|date=2 July 1978}}</ref><ref>{{cite news|last1= Secrest |first1= Meryle |title= Material Glory|accessdate=10 March 2015|work=The Washington Post|date=4 October 1975}}</ref>  [[Davidson College]] (Davidson, North Carolina)<ref name=DavidsonCollege/> and the Lunn Gallery in Washington, DC, (1980, “End of the Line”, “Empty Vessels”).<ref name=Lewis,JoAnn(10May1980)/><ref>{{cite news|last1=Lewis|first1=Jo Ann|title=Sticks as Bones|accessdate=10 March 2015|work=The Washington Post|date=26 May 1983}}</ref><ref>{{cite web|title=Harry Lunn Paper|url=http://archives2.getty.edu:8082/xtf/view?docId=ead/2004.M.17/2004.M.17.xml;chunk.id=ref10;brand=default|website=The Getty Research Institute|accessdate=10 March 2015}}</ref>

In September–October 1991, John Grazier had a one-man show named “A Ticket to ...”at the Washington, DC’s Zenith Gallery. Featured pieces included large airbrush India ink paintings on paper: “Echoes: Coaches Idling”, “Junk Yard Dogs”, “You Can’t Go Home Again”, “The Children Who Would Gallop”, “House on a Hill in a Dream."<ref name=Lewis,JoAnn(13June1990)/><ref>{{cite news|last1=Wilson|first1=Janet|title=Navigations on a Sea of Symbols|accessdate=19 March 2015|work=The Washington Post|date=5 October 1991}}</ref>

== Greyhound Bus Terminal Project in Washington, DC ==

In a summer of 1990, Grazier had signed a $125,000 contract with the Canadian developer [[Manulife Financial|Manulife Real Estate]] to produce 18 black-and-white airbrush paintings for the [[Greyhound Lines|Greyhound Bus]] Terminal lobby in [[Washington, D.C.]]. The restoration of the terminal was part of an agreement with Manulife and area preservationists to keep the 1930s [[Art Deco]] building by architect William S. Arrasmith<ref>{{cite book|last1=Wrenick|first1=Frank E.|title=The Streamline Era Greyhound Terminals: The Architecture of W.S. Arrasmith|date=8 December 2006|publisher=McFarland|isbn=0786425504|url=http://www.amazon.com/Streamline-Era-Greyhound-Terminals-Architecture/dp/0786425504/ref=sr_1_1?s=books&ie=UTF8&qid=1426015841&sr=1-1&keywords=9780786425501|accessdate=10 March 2015}}</ref> at 1100 New York Ave., N.W. intact as a lobby-entrance to a 12-story office building going up behind it.<ref>{{cite news|last1=Lewis|first1=Jo Ann|title=Homeless Artist Gets Commission|url=http://www.highbeam.com/doc/1P2-1141771.html|accessdate=10 March 2015|work=The Washington Post|date=10 August 1990}}</ref> Upon its reopening in 1991, the building’s lobby featured enlarged photographs of the original 18 paintings featuring buses, coffee cups, lonely cityscapes and Mount Rushmore reflected in a bus windshield.<ref name=AllenHenry(13September1991)>Allen, Henry (13 September 1991). "Terminal of Endearment; Memories and a New Life for the Greyhound Station."''The Washington Post.'' Retrieved 10 March 2015. http://www.highbeam.com/doc/1P2-1084683.html</ref><ref>{{cite news|last1=Forgey|first1=Benjamin|title=The Dignified Depot; Happy Revival of the Greyhound Building|accessdate=4 April 2015|work=The Washington Post|date=14 September 1991}}</ref><ref>{{cite news|last1=Adler|first1=Jerry|title=What a Swell Ride It Was|accessdate=4 April 2015|work=Newsweek|date=14 October 1991}}</ref>

== Exhibitions ==

He has been included in many gallery exhibitions, including the Davidson National Print and Drawing Competition,<ref name=Lewis,JoAnn(10May1980)/>  Middendorf/Lane Gallery (Washington, DC),<ref name=RichardPaul(16September1978)/> Foundry Gallery (“25 Washington Artists: Realism and Representation, Washington, DC), [[Corcoran Gallery of Art]] (Washington, DC),<ref name=Lewis,JoAnn(10May1980)/> a United States Information Agency Tour of the Middle East, The [[Mint Museum]] (North Carolina),<ref name=ZenithGallery/> [[Washington Project for the Arts]] Exhibition.<ref>{{cite web|title=Washington Project for the Arts|url=http://catalyst.wpadc.org/?page_id=86|website=Catalyst|accessdate=4 April 2015}}</ref> His urban landscape “Memory of a Trombone” has been exhibited at the [[Brooklyn Museum|Brooklyn Museum of Art]] (American Drawing in Black and White: 1970-80, Brooklyn, New York in 1980).<ref name=KramerHilton(28November1980)>Kramer, Hilton (28 November 1980). "American Drawings of the 70’s at Brooklyn". ''The New York Times.'' Retrieved 4 April 2015.</ref>  His works have also been exhibited at the [[Southeastern Center for Contemporary Art]] (North Carolina), the [[Tampa Museum of Art]] (Florida),<ref name=HamillPete(1995)/>   the [[Farragut West Station|Farragut West]] branch of [[Citibank]] in Washington, DC (“Sunset Strip,” “Where the Children Will Play,” “The Silence of the Attic,” “The Sound of the Wind,” “The Toy Chair,” “The Carousel of Dreams”).<ref name=OSullivan(18May1996)/>

In 1990, John Grazier was one of only two living artists represented in a show at DC’s Adams-Davidson Gallery featuring “200 years of American Master Drawings.”<ref>{{cite journal|last1=Witty|first1=Merrill|title=Wheel Life. Buses Or Bust.|journal=Mid-Atlantic Magazine|date=December 1990|accessdate=4 April 2015}}</ref>

He currently lives and works in [[Shamokin, Pennsylvania]].

==Selected Works==

{| class="wikitable"
|-
! Title
! Medium
! Date
! Collection
! Dimensions
! Image
! Reference
|-
| House on a Hill in a Dream
| Pencil on paper
| 1974
| [[Smithsonian American Art Museum]]
| 16 x 28 1/2 in.(40.6 x 72.4&nbsp;cm)
| [[File:House on a Hill in a Dream 1974 Smithonian.jpg|thumb| "House on a Hill in a Dream"]]
| <ref name=Smithsonian/>
|-
| Memory of a Porch
| Lithograph
| 1976
| [[Smithsonian American Art Museum]]
| 22 x 30 in.(55.9 x 76.2&nbsp;cm)
|
| <ref name=Smithsonian/>
|-
| Memory of a Porch
| Lithograph
| 1975
| [[National Gallery of Art]]
| 22 1/8 x 29 15/16 in.(56.2 x 76.1&nbsp;cm)
|
| <ref name=NatGalleryArt/>
|-
| Breaking Up	
| Lithograph	
| 1976	
| [[National Gallery of Art]]	
| 22 1/8 x 30 1/16 in.(56.2 x 76.3&nbsp;cm)
|
| <ref name=NatGalleryArt/>
|-
| End of the Line
| Graphite with touches of erasing on paper	
| 1980	
| [[Art Institute of Chicago]]
| 26 3/4 x 36 2/3 in.(68 x 93.5&nbsp;cm)		
|
| <ref name=ArtInstituteChicago/>
|-
| Untitled	
| India ink on paper	
| 1985	
| The [[Pollock-Krasner Foundation]]	
| 40 x 60 in.(101.6 x 152.4&nbsp;cm)
| 
| <ref name=Pollock/>
|-
| Rattling Windows	
| Pencil on paper	
| 1980	
| The [[Pollock-Krasner Foundation]]	
| 30 x 40 in.(76.2 x 101.6&nbsp;cm)
| 
| <ref name=Pollock/>
|-
| Cop Motors at Rest
| Aquatint Etching
|
| [[National Law Enforcement Museum]]
|
|
| <ref name=NationalPoliceMuseum/>
|-
| Passing Windows in Fall	
| Graphite on paper	
| 1983	
| The Hechinger Collection	
| 28 x 38 in.(71.1 x 96.5&nbsp;cm)
| 
| <ref name=HechingerCollection>John Grazier: "Passing Windows in Fall" (1983). '' The Hechinger Collection.'' Retrieved 3 April 2015. http://www.artsandartists.org/exhibitions-hechinger-collection-f-l.php#G</ref>
|-
| City Lines	
| Pencil on paper	
| 1978	
| Brandeis University, The Herbert W. Plimpton Collection of Realist Art 	
| 77 x 27.7 in.(195.6 x 70.4&nbsp;cm)
|
| <ref name=BrandeisUniversity>John Grazier: "City Lines" (1978). '' Brandeis University, The Herbert W. Plimpton Collection of Realist Art .'' Retrieved 3 April 2015. https://archive.org/stream/herbertwplimpton00rose/herbertwplimpton00rose_djvu.txt</ref>
|-
| Reflections of Mount Rushmore	
| India ink airbrush, photographs	
| 1991	
| Lobby of 1100 New York Avenue NW Washington, DC
|
|
| <ref name=AllenHenry(13September1991)/>
|-
| Rebecca’s Doll House	
| Oil on canvas		
| 
| Truland Systems
|
|
| <ref name=Hom(2011)/> 
|-
| Burning Bush	
| Oil on canvas		
| 
| Truland Systems	
| 30 x 40 in. (76.2 x 101.6&nbsp;cm)
|
| <ref name=WashBusJournal/>
|-
| The Visitor	
| Oil on canvas	
| 2001	
| Hogan Lovells, formerly known as Hogan & Hartson
|
|
| <ref name=Johnson(14October2001)/>
|}

==Notes==
{{reflist|group=note}}

==References==
{{reflist|2}}

{{DEFAULTSORT:Grazier, John}}
[[Category:American artists]]
[[Category:1946 births]]
[[Category:Living people]]
[[Category:People from Shamokin, Pennsylvania]]