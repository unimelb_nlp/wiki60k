{{Infobox journal
| title = Journal asiatique
| formernames = 
| abbreviation = J. asiat.
| cover = [[File:Journal-asiatique-2005.gif|200px]]
| editor = [[Jean-Marie Durand]]
| discipline = [[Asian studies]]
| language = English, French, German, Italian, Spanish
| publisher = [[Peeters Publishers]] on behalf of the [[Société Asiatique]]
| country = 
| frequency = Biannual
| history = 1822-present
| openaccess =
| license =
| impact =
| impact-year =
| website = http://www.peeters-leuven.be/journoverz.asp?nr=10
| link1 = http://poj.peeters-leuven.be/content.php?url=journal.php&code=JA
| link1-name = Online access
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 714198965
| LCCN = sc82008193
| CODEN =
| ISSN = 0021-762X
| eISSN = 1783-1504
}}
The '''''Journal asiatique''''' (full earlier title ''Journal Asiatique ou Recueil de Mémoires, d'Extraits et de Notices relatifs à l'Histoire, à la Philosophie, aux Langues et à la Littérature des Peuples Orientaux'') is a biannual [[Peer review|peer-reviewed]] [[academic journal]] established in 1822 by the [[Société Asiatique]] covering [[Asian studies]]. It publishes articles  in [[French language|French]] and several other European languages. Cited texts are presented in their original languages. Each issue also includes news of the Société Asiatique and its members, obituaries of notable Orientalists, critical reviews, and books received. The journal is published by  [[Peeters Publishers]] on behalf of the [[Société Asiatique]] and the [[editor-in-chief]] is [[Jean-Marie Durand]].
[[File:Journal-asiatique-tome1.jpg|thumb|left|Cover of first issue (1822)]]

It is one of the oldest continuous French publication.

== Abstracting and indexing ==
The journal is abstracted and indexed in: [[Bibliographie linguistique/Linguistic Bibliography]], [[ATLA Religion Database]], [[Index to the Study of Religions Online]], [[Index Islamicus]], and [[Scopus]].

== Previous editors ==
The following people have been [[editor-in-chief]]:
{{columns-list|colwidth=30em|
* [[Antoine-Jean Saint-Martin]] (1822–1832) 
* Grangeret de Lagrange (1832–1858) 
* [[Julius von Mohl]] (1858–1876) 
* [[Barbier de Meynard]] (1876–1892) 
* [[Rubens Duval]] (1892–1908) 
* [[Louis Finot (archeologist)|Louis Finot]] (1908–1920) 
* Gabriel Ferrand (1920–1935) 
* [[René Grousset]] (1935–1946) 
* [[Jean Sauvaget]] (1946–1950) 
* [[Marcelle Lalou]] (1950–1967) 
* [[James Germain Février]] (1967–1972) 
* Daniel Gimaret (1972–1992) 
* Denis Matringe (1993–2001) 
* Cristina Scherrer-Schaub (2001–2008) 
* Gérard Colas (2008–2011)
}}

==External links==
*{{Official|1=http://www.peeters-leuven.be/journoverz.asp?nr=10}}
*[http://gallica.bnf.fr/ark:/12148/cb34348774p/date ''Journal asiatique'' issues from 1822 to 1940] in [[Gallica]] {{fr icon}}
*[[:s:fr:Journal asiatique|''Journal asiatique'' on French Wikisource]]

{{Authority control}}
{{DEFAULTSORT:Journal Asiatique}}
[[Category:Publications established in 1822]]
[[Category:Asian studies journals]]
[[Category:Biannual journals]]
[[Category:Multilingual journals]]
[[Category:Peeters Publishers academic journals]]
[[Category:Academic journals associated with learned and professional societies]]
[[Category:1822 establishments in France]]