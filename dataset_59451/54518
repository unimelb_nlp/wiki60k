{{Infobox book|name = Aziyadé|author = [[Pierre Loti]]|language = French|published = 1879|publisher = [[Éditions Gallimard]]|isbn = 2070381471}}

'''''Aziyadé''''' ([[1879 in literature|1879]]; also known as '''''Constantinople''''') is a novel by French author [[Pierre Loti]]. Originally published anonymously, it was his first book, and along with ''[[Le Mariage de Loti]]'' (1880, also published anonymously), would introduce the author to the French public and quickly propel him to fame; his anonymous persona did not last long.

''Aziyadé'' is semi-autobiographical, based on a diary Loti kept during a 3-month period as a French Naval officer in [[Greece]] and [[İstanbul]] in the fall and winter of 1876. It tells the story of the 27-year-old Loti's illicit love affair with an 18-year-old [[Circassian beauties|"Circassian"]] harem girl named Aziyadé. Although Aziyadé was one of many conquests in the exotic romantic's life, she was his greatest love, he would wear a gold ring with her name on it for the rest of his life. Forming a love triangle, the book also describes Loti's "friendship" with a Spanish man servant named Samuel, for which most critics believe, based on Loti's diary entries, that some sort of homosexual affair occurred (indeed some believe Aziyadé never existed and the entire work is a cover for a homosexual love story, however the evidence for Aziyadé's existence seems overwhelming, (See Blanch)). It also describes Loti's love affair with Turkish culture which became a central part of his "exotica" persona.

The only known English translation is by Marjorie Laurie which can be found in many editions, no longer in copyright, however some of the parts have been sanitized regarding harem life, prostitution and homosexuality. The original French first edition is very rare and has become a highly prized collectors item.

==Resources==
*Lesley Blanch (1983). ''Pierre Loti: the legendary romantic''. Chapters 6 to 8. (Republished as ''Pierre Loti: Travels with the Legendary Romantic''. Tauris, 2004. ISBN 1-85043-429-8.)
*Richard M. Berrong. [http://findarticles.com/p/articles/mi_qa3709/is_199810/ai_n8824441/pg_1 "Portraying male same-sex desire in nineteenth-century French literature: Pierre Loti's Aziyadé"], ''College Literature'', Fall 1998.

{{DEFAULTSORT:Aziyade}}
[[Category:1879 novels]]
[[Category:Autobiographical novels]]
[[Category:Novels by Pierre Loti]]
[[Category:1876 in fiction]]
[[Category:Novels set in Greece]]
[[Category:Novels set in Istanbul]]


{{1870s-novel-stub}}