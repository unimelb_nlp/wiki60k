{{Infobox person
|name          = Fred Ernest Weick
|image         = NASA EL-1999-00640.jpeg
|image_size    = 250px
|caption       = Portrait of Fred E. Weick, 1936
|birth_name    = 
|birth_date    = 1899
|birth_place   = [[Berwyn, Illinois]] 
|death_date    = 8 July 1993
|death_place   = [[Vero Beach, Florida]]
|death_cause   = 
|resting_place = 
|resting_place_coordinates = 
|residence     = 
|nationality   = 
|ethnicity     = 
|citizenship   = 
|other_names   = 
|known_for     = Aircraft Engineer
|awards        = [[Daniel Guggenheim Medal]] <small>(1989)</small>
|education     =
|alma_mater    = 
|employer      = 
|occupation    = 
|home_town     = 
|title         = 
|salary        = 
|networth      = 
|height        = 
|weight        = 
|term          = 
|predecessor   = 
|successor     = 
|party         = 
|boards        = 
|religion      = 
|spouse        = Dorothy Church
|partner       = 
|children      = 
|parents       = 
|relations     = 
|signature     = 
|website       = 
|footnotes     = 
}}

'''Fred Ernest Weick''' (1899 [[Berwyn, Illinois]] – July 8, 1993) was one of the [[United States]]' earliest aviation pioneers, working as an [[airmail|airmail pilot]], [[research]] [[engineer]], and aircraft designer. 
A contemporary of aviation legends [[Charles Lindbergh]] and [[Amelia Earhart]], he did not receive the same attention as his more glamorous colleagues, yet his contribution to the United States' struggling [[aerospace manufacturer|aircraft industry]] was "in the league of the [[Wright Brothers]]".<ref>{{cite news| url=http://articles.dailypress.com/1993-07-10/news/9307100050_1_plane-nasa-langley-research-center-modern-airliners| title=Fred E. Weick, Aviation Pioneer| date=July 10, 1993| author= KIRK SAVILLE| work= Daily Press }}</ref>

==Life==
[[Image:Weick, Lindbergh, and Hamilton - GPN-2000-001302.jpg|right|thumb|1927 – Weick in rear cockpit with Lindbergh in front]]
[[Image:Amelia Earhart NASA GPN-2000-001389.jpg|right|thumb|1928 – Weick in rear with Amelia Earhart]] 
A 1922 graduate of the [[University of Illinois at Urbana-Champaign|University of Illinois]], he was one of the first university graduates to apply his degree to a career in aeronautics. Weick was also one of the first engineers hired by the original [[United States Postal Service|U.S. Air Mail Service]].<ref name="independent1">{{cite web|url=http://www.independent.co.uk/news/people/obituary-fred-weick-1485008.html |title=Obituary: Fred Weick |publisher=The Independent |date=15 July 1993| author=MIKE JERRAM |accessdate=2010-07-30}}</ref> His efforts in the early 1920s to establish emergency fields for night-flying mail pilots addressed a major challenge.

Weick worked for the [[National Advisory Committee for Aeronautics]] (NACA) at its [[Langley Aeronautical Laboratory]], in [[Hampton, Virginia]] beginning in November 1925. 
He helped design the first [[wind tunnel]] devoted to full-scale [[propeller]] research and wrote a [[textbook]] on propeller design that became a classic.<ref name="nytimes1993">{{cite web|last=Lambert |first=Bruce |url=https://www.nytimes.com/1993/07/11/obituaries/fred-weick-93-dies-in-florida-was-pioneer-in-airplane-design.html |title=Fred Weick, 93, Dies in Florida - Was Pioneer in Airplane Design |publisher=The New York Times |date=1993-07-11 |accessdate=2010-07-30}}</ref>

It was also at Langley that Weick headed the development of [[Streamliner|streamlined]], low-drag [[NACA cowling|engine cowling]] technology that was to advance aircraft performance dramatically. The NACA cowling first revolutionized [[airline|civil air transport]] by making aircraft faster and more profitable. It also found application on the [[bombers]] and [[fighter aircraft|fighters]] of [[World War II]]. For this engineering breakthrough, he won the prestigious [[Collier Trophy]] for NACA in 1929.<ref name="independent1"/>

The [[experimental airplane]] he built in the early 1930s demonstrated Weick's passion for safety. 
He left NACA in 1936, and joined ERCO's fledgling aircraft team as chief designer.
His goal was to make flying as easy and safe as driving the family auto. In addition to the integrated controls for ease of flying, he incorporated the [[tricycle landing gear]] that later became standard on most of the world's aircraft.<ref name="nytimes1993"/>

Later in the 1930s, Weick improved on that design with the [[Ercoupe]], the two-seat, all-metal, low-wing aircraft that was so easy and safe to fly that many students mastered it in five hours or less. Half of the 6,000 Ercoupes built were still flying at the time of Weick's death. In February 1946, he received the [[Fawcett Aviation Award]] for the greatest contribution to the scientific advancement of private flying.<ref>{{cite web|url=http://www.bassace.com/ercoupe/ercoupe_history.htm |title=Some Ercoupe history |publisher=Bassace.com |date= |accessdate=2010-07-30}}</ref>

Weick joined [[Texas A&M University]] in 1948. There, he worked on the design and development of the [[Texas A&M College Ag-1|Ag-1]] crop duster, and designed the Ag-3, predecessor to the [[Piper PA-25 Pawnee]] series. The same basic configuration and design concepts pioneered in the Ag-1 can be seen in more modern crop dusters including the [[Air Tractor AT-802]].

In a 1979 interview about general aviation's future past the year 2000, Weick accurately envisioned the continued interest in sport aviation and the practical use of aircraft for medium-range transportation. He mentioned that he had seen gas turbines demonstrated as early as 1922, and that their future use in light aircraft would only be viable with development of cost-efficient materials that could withstand the heat. He felt future aircraft would not be radically different, but could benefit from safety improvements in controllability.<ref>{{cite journal|magazine=Air Progress|title=Designers talk about the future|date=January 1979}}</ref>

He joined [[Piper Aircraft]] in 1957 as director and chief engineer of its development center, remaining there until his retirement at age 70. In addition to the Pawnee, Weick co-designed Piper's [[Piper PA-28|Cherokee]] line of personal and business [[light aircraft]].<ref name="nytimes1993"/> Weick remained active in [[general aviation]], regularly attending the [[Experimental Aircraft Association]] [[EAA AirVenture Oshkosh|Oshkosh airshow]] for entertainment and lectures.<ref>{{cite journal|magazine=Air Progress|date=November 1978|page=29}}</ref>

He married Dorothy Church, (died 1991); they had three children.
Weick died on Thursday, July 8, 1993, in [[Vero Beach, Florida]].

== Gallery ==
<gallery>
Image:Early Test Installations for Cowlings - GPN-2000-001394.jpg|NACA Cowlings
Image:Weick W-1A of 1934 - GPN-2000-001238.jpg|Weick W1A
Image:First JATO assisted Flight - GPN-2000-001538.jpg|Ercoupe
Image:PA-25 01.jpg|Pawnee
Image:Piper PA-28 Cherokee D-EKIN 20090627 008.JPG|Piper Cheroke
</gallery>

==References==
{{reflist}}

==Sources==
* {{cite book
|author1=Weick, Fred E.  |author2=Hansen, James R.
|title=From the Ground Up
|publisher=Smithsonian Institution Press
|location = Washington D.C.
|year=1988
|isbn=0-87474-950-6}}
* [http://www.centennialofflight.gov/essay/Dictionary/Weick/DI125.htm "Fred Weick"] ''US Centennial of Flight Commission'', retrieved January 12, 2006
*[http://www.nasm.si.edu/research/arch/findaids/pdf/Weick_Acc_XXXX-0425.pdf "FRED E. WEICK AUTOBIOGRAPHICAL TRANSCRIPTS"], ''NATIONAL AIR AND SPACE ARCHIVES'', Accession XXXX-0425
*[http://edburkhead.com/saved%20ercoupe2/coupe_landings.htm "interview I had with Fred Weick"] ''Coupe landings and landing gears'', September 1991 ''Coupe Capers'': Ed Burkhead

==External links==
* [https://books.google.com/books?id=xd0DAAAAMBAJ&pg=PA659 "Flivver Plane Without Tail Almost Flies Itself" ''Popular Mechanics'', November 1935]

{{Authority control}}

{{DEFAULTSORT:Weick, Frederick E.}}
[[Category:1899 births]]
[[Category:1993 deaths]]
[[Category:Aircraft designers]]
[[Category:American aerospace engineers]]
[[Category:American aviators]]
[[Category:Aviators from Illinois]]
[[Category:Collier Trophy recipients]]
[[Category:People from Berwyn, Illinois]]
[[Category:Texas A&M University faculty]]
[[Category:University of Illinois alumni]]
[[Category:United States airmail pilots]]
[[Category:Piper Aircraft, Inc.]]