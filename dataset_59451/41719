[[File:Bodl Canon.Misc.378 roll159B frame28.jpg|thumb|Page from a medieval copy of the ''Notitia Dignitatum'' commissioned in 1436 by [[Pietro Donato]], depicting shields of ''Magister Militum Praesentalis II'', a late Roman register of military commands]]
[[File:Notitia Dignitatum - Dux Palestinae.jpg|thumb|Palestine and the River Jordan, from the  ''Notitia Dignitatum'']]
The '''''Notitia Dignitatum''''' ([[Latin]] for "The List of Offices") is a document of the [[Late Antiquity|late]] [[Roman Empire]] that details the administrative organization of the Eastern and Western Empires. It is unique as one of very few surviving documents of Roman government and describes several thousand offices from the imperial court to provincial governments, [[diplomatic mission|diplomatic missions]], and [[Late Roman army|army units]]. It is usually considered to be accurate for the [[Western Roman Empire]] in the AD 420s and for the [[Byzantine Empire|Eastern or Byzantine Empire]] in the AD 390s. However, the text does not date its own authorship or accuracy, and omissions complicate ascertaining its date from its content.

== Copies of the manuscript ==
There are several extant 15th- and 16th-century copies of the document, plus a color-illuminated iteration of 1542. All the known, extant copies are derived, either directly or indirectly, from ''Codex Spirensis'', a [[codex]] known to have existed in the library of the Chapter of [[Speyer Cathedral]] in 1542, but which was lost before 1672 and has not been rediscovered. The ''Codex Spirensis'' was a collection of documents, of which the ''Notitia'' was the final and largest document, occupying 164 pages, that brought together several previous documents of which one was of the 9th century.{{Citation needed|date=February 2011}} The [[heraldry]] in illuminated manuscript copies of the ''Notitia'' is thought to copy or imitate only that illustrated in the lost ''Codex Spirensis''.

The iteration of 1542 made for [[Otto Henry, Elector Palatine]], was revised with "illustrations more faithful to the originals added at a later date", and is preserved by the [[Bavarian State Library]].<ref>{{Cite web| title = Publication of Offices - Notitia Dignitatum (Sammelhandschrift)| work = World Digital Library| accessdate = 2014-06-21| date = 1542| url = http://www.wdl.org/en/item/4103 }}</ref>

The most important copy of the ''Codex'' is that made for [[Pietro Donato]] in 1436 and illuminated by [[Peronet Lamy]].{{Citation needed|date=February 2011}}

==Contents==
For each half of the Empire, the ''Notitia'' enumerates all the major "dignities", i. e. offices, that it could bestow, often with the location and specific ''[[Officium (Ancient Rome)|officium]]'' ("staff") enumerated, except for the most junior members, for each. The dignities are ordered by:

*Court officials, including the most senior dignitaries such as [[praetorian prefect|praetorian prefects]];
*[[Vicarius|Vicars]] and [[praeses|provincial governors]], arranged by [[praetorian prefecture]] and [[Roman diocese|diocese]]; and
*[[Magister militum|Martial commanders]], ''[[comes|Comites Rei Militaris]]'', and ''[[dux|Duces]]'', providing the full titles and stations of their regiments.

== Interpretation ==
The ''Notitia'' presents four primary problems as regards the study of the Empire's military:

# The Notitia depicts the [[Roman army]] at the end of the AD 4th century. Therefore, its development from the structure of the [[Principate]] is largely conjectural because of the lack of other evidence.
# It was compiled at two different times. The section for the Eastern Empire apparently dates from circa AD 395 and that for the Western Empire from circa AD 420. Further, each section is probably not a contemporaneous "snapshot", but relies on data pre-dating it by as many as 20 years. The Eastern section may contain data from as early as AD 379, the beginning of the reign of [[Emperor Theodosius I]]. The Western section contains data from as early as circa AD 400: for example, it shows units deployed in [[Britannia]], which must date from before 410, when the Empire lost government of the island. In consequence, there is substantial duplication, with the same unit often listed under different commands. It is impossible to ascertain whether these were detachments of the same unit in different places simultaneously, or the same whole unit at different times. Also, it is likely that some units were merely nominal or minimally staffed.<ref>A. Goldsworthy, ''Roman Warfare'' (2000), p. 198.</ref> According to [[Roger Collins]], the ''Notitia Dignitatum'' was an archaising text written circa AD 425, whose unreliability is demonstrated by "the supposed existence of traditional (Roman military) units in Britain and Spain at a time when other evidence shows they were not there."<ref>Roger Collins, ''Early Medieval Europe: 300-1000'' (London: The Macmillan Press Ltd., 1991), pp. 89-90.</ref>
# The ''Notitia'' has many sections missing and ''lacunae'' ("gaps") within sections. This is doubtless due to accumulated textual losses and copying errors, because it was repeatedly copied over the centuries: the earliest manuscript possessed today dates from the 15th century. The ''Notitia'' cannot therefore provide a comprehensive list of all units that existed.
# The ''Notitia'' does not record the number of personnel. Given that and the paucity of other evidence of unit sizes at that time, the size of individual units and the various commands cannot be ascertained. In turn, this makes it impossible to assess accurately the total size of the army. Depending on the strength of units, the late AD 4th century army may, at one extreme, have equaled the size of the AD 2nd century force, i. e. over 400,000 men;<ref>P. Heather, ''Fall of the Roman Empire'' (2005), p. 63.</ref> and at the other extreme, it may have been far smaller. For example, the forces deployed in Britain circa AD 400 may have been merely 18,000 against circa 55,000 in the AD 2nd century.<ref>D. Mattingly, ''An Imperial Possession: Britain in the Roman Empire'' (2006), p. 239.</ref>

== Depictions ==
[[File:Notitia Dignitatum - Magister Peditum 4.jpg|thumb|Shield pattern of the ''armigeri defensores seniores'' (4th row, third from left)<ref name="Giovanni Monastra (2000)"/><ref name="Isabelle Robinet (2008), 934"/><ref name="Helmut Nickel (1991), 146, 5"/>]]
The ''Notitia'' contains symbols similar to the diagram which later came to be known as [[yin and yang symbol]].<ref name="Giovanni Monastra (2000)">Giovanni Monastra: ''[http://www.estovest.net/tradizione/yinyang_en.html#t24 The "Yin-Yang" among the Insignia of the Roman Empire?]'', ''Sophia'', Bd. 6, Nr. 2 (2000) {{webarchive |url=https://web.archive.org/web/20080915032448/http://www.estovest.net/tradizione/yinyang_en.html#t24 |date=September 15, 2008 }}</ref><ref name="Isabelle Robinet (2008), 934">Isabelle Robinet: "Taiji tu. Diagram of the Great Ultimate", in: Fabrizio Pregadio (ed.): ''The Encyclopedia of Taoism A−Z'', Routledge, Abingdon (Oxfordshire) 2008, ISBN 978-0-7007-1200-7, pp. 934−936 (934)</ref><ref name="Helmut Nickel (1991), 146, 5">Helmut Nickel: ''The Dragon and the Pearl'', ''Metropolitan Museum Journal'', Bd. 26 (1991), S. 146, Fn. 5</ref> The infantry units ''armigeri defensores seniores'' ("shield-bearers") and ''Mauri Osismiaci'' had a shield design which corresponds to the dynamic, clockwise version of the symbol, albeit with red dots, instead of dots of the opposite colour.<ref name="Giovanni Monastra (2000)"/> The emblem of the ''Thebaei'', another Western Roman infantry regiment, featured a pattern of concentric circles comparable to its static version. The Roman patterns predate the earliest [[Taoism|Taoist]] versions by almost seven hundred years,<ref name="Giovanni Monastra (2000)"/> and there is no evidence for a connection between the two.

==See also==
*[[Laeti]]
*[[Tabula Peutingeriana]]
*[[List of Late Roman provinces]]

== Citations ==
{{reflist}}

==Sources and references==
*''Notitia Dignitatum'', edited by Robert Ireland, in ''British Archaeological Reports'', International Series '''63'''.2.
* ''Westermann Großer Atlas zur Weltgeschichte'' contains many precise maps
* [[Pauly-Wissowa]].
* A.H.M. Jones, ''The Later Roman Empire, 284-602. A Social, Economic and Administrative Survey'', The Johns Hopkins University Press, 1986, ISBN 0-8018-3285-3

== External links ==
{{commons|Notitia dignitatum}}
*[http://members.iinet.net.au/~igmaier/notitia.htm Dr Ingo G. Maier, ''Compilation Notitia Dignitatum''], extensive links and resources
*[http://pelagios.org/recogito/map?doc=27 Placenames from ''Notitia Dignitatum''] [[GIS]] from  Pelagios/Pleiades. 1505 toponyms. 1164 matches.

===Manuscripts===
* [[Bodleian Library]]: [http://bodley30.bodley.ox.ac.uk:8180/luna/servlet/view/all/what/MS.+Canon.+Misc.+378?sort=Shelfmark%2cFolio_Page%2cRoll_%23%2cFrame_%23 manuscript]
*[[Bavarian State Library]]: [http://daten.digitale-sammlungen.de/~db/bsb00005863/images/index.html?id=00005863&fip=86.132.15.248&no=5&seite=1 Notitia Dignitatum Clm 10291] − full online scan of the 1542 manuscript with its modernised illustrations

===Latin, web versions===
*{{Wikisource-inline|links=[[s:la:Notitia dignitatum|Notitia dignitatum]]}}
*[http://www.intratext.com/IXT/LAT0212/_INDEX.HTM Notitia Dignitatum] at [[IntraText]]
*[http://www.fh-augsburg.de/~harsch/Chronologia/Lspost05/Notitia/not_intr.html ''Notitia Dignitatum''], with pictures, from bibliotheca Augustana

===Editions===
*''[Notitia dignitatum; accedunt Notitia urbis Constantinopolitanae et laterculi prouinciarum]'', Latin with notes by [[Otto Seeck]] (1876): [https://archive.org/details/notitiadignitat00seecgoog Internet Archive], [http://catalog.hathitrust.org/api/volumes/oclc/10027454.html HathiTrust], [http://www.europeana.eu/portal/record/9200143/BibliographicResource_2000069296043.html Europeana] 
*[http://www.fordham.edu/halsall/source/notitiadignitatum.asp Medieval Sourcebook] Partial English translation by William Fairley, 1899

{{Italic title}}

[[Category:Military history of ancient Rome]]
[[Category:Government of the Roman Empire]]
[[Category:Latin prose texts]]
[[Category:Coats of arms of the Roman Empire]]
[[Category:Comitatenses]]
[[Category:Late Roman military]]
[[Category:Late antiquity]]
[[Category:Illuminated manuscripts]]