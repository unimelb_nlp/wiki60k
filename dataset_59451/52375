{{Infobox journal
| title = Nature Precedings
| cover = 
| editor = 
| discipline = [[Interdisciplinary]]
| abbreviation = 
| publisher = [[Nature Publishing Group]]
| country = United Kingdom
| frequency = 
| history = 2007–2012
| openaccess = 
| license = 
| impact = 
| impact-year = 
| website = http://precedings.nature.com/
| link1 = 
| link1-name = 
| link2 = 
| link2-name =
| JSTOR = 
| OCLC = 
| LCCN = 
| CODEN = 
| ISSN = 1756-0357
| eISSN = 
}}
'''''Nature Precedings''''' was an [[open access (publishing)|open access]] electronic [[preprint]] [[Disciplinary repository|repository]] of scholarly work in the fields of [[biomedical sciences]], [[chemistry]], and [[earth sciences]]. It ceased accepting new submissions as of April 3, 2012. 

''Nature Precedings'' functioned as a permanent, citable archive for pre-publication research and preliminary findings. It was a place for researchers to share documents, including presentations, posters, white papers, technical papers, supplementary findings, and non-[[peer review|peer-reviewed]] manuscripts. It provided a rapid way to share preliminary findings, disseminate emerging results, solicit community feedback, and claim priority over discoveries. The content was curated and developed by the [[Nature Publishing Group]].

== Description==
''Nature Precedings'' was started in June 2007 by the Nature Publishing Group under the direction of Timo Hannay, its director for web publishing. The [[British Library]], the [[European Bioinformatics Institute]], [[Science Commons]], and the [[Wellcome Trust]] were partners.<ref name="about">[http://precedings.nature.com/about "About ''Nature Precedings''"], ''Nature Precedings''; accessed July 3, 2007</ref> ''Nature Precedings'' supported the [[Open Archives Initiative Protocol for Metadata Harvesting]] (OAI-PMH, version 2).<ref>[http://network.nature.com/groups/precedings/forum/topics/2824 "OAI-PMH interface for ''Nature Precedings''"]</ref> Although content is solely author-[[copyright]]ed, it can be used under the terms of the [[Creative Commons Attribution License]], versions 2.5 or 3.0.

Documents that were manuscripts, preliminary reports, [[white paper]]s, or presentations reporting work in the fields of [[biology]], [[chemistry]], [[earth science]]s, or [[medicine]] (except [[clinical trial]] results) could be submitted for posting. [[Physics]] and [[mathematics]] were excluded as they are already covered by the [[arXiv]] preprint server. Submissions could be revised after posting, although the original submission remained and revisions were made available for access as newer versions. Submitted documents were not reviewed by editors or experts, and "genuine contributions from qualified scientists"<ref name="about"/> were accepted for immediate posting. Non-scientific or pseudo-scientific work was rejected. Submitters were expected to have copyrights and appropriate permissions for material presented in the submitted documents. Opportunity for non-anonymous, informal peer review was available through a commenting system on the ''Nature Precedings'' website.

A ''Nature Precedings'' preprint is cited like a traditional journal article. However, the preprint's [[Digital object identifier|DOI]] is used as the document identifier instead of journal volume, issue, and page numbers. Scientists and journal publishers have expressed concerns regarding preprint submissions to ''Nature Precedings'' and whether these submissions would lead to violations of the [[Ingelfinger rule]], a policy in which journals will not publish a manuscript if its findings have been reported elsewhere.<ref name=TS>[http://www.the-scientist.com/news/display/53294/ "New site pits 'published' vs. 'posted'"], Andrea Gawrylewski, ''[[The Scientist (magazine)|The Scientist]]'', 19 June 2007</ref>

== Growth ==
Fifty-five preprints were posted in the first 15 days of ''Nature Precedings''. Of those, 26 were submitted as manuscripts and 29 as posters/presentations.<ref>[http://precedings.nature.com/search/advanced ''Nature Precedings'' search]; searched July 3, 2007</ref> Corresponding numbers for the first 50 days were 89, 48, and 41, and for the first six months, 303, 227, and 76. About 500 and 650 preprints were published during the first and second years, respectively. The growth of the repository can be tracked graphically on the ''Scientific Commons'' page for ''Nature Precedings''.<ref>{{cite web|url=http://en.scientificcommons.org/repository/nature_precedings |title=Scientific Commons: Nature Precedings |publisher=En.scientificcommons.org |date=2010-08-05 |accessdate=2012-04-11}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]].<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2015-10-28}}</ref>

==References==
<references/>

== External links ==
* {{Official website}}

[[Category:Creative Commons-licensed journals]]
[[Category:Nature Publishing Group academic journals]]
[[Category:Publications established in 2007]]
[[Category:English-language journals]]
[[Category:Publications disestablished in 2012]]
[[Category:Multidisciplinary scientific journals]]