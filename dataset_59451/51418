{{multiple issues|
{{notability|date=December 2012}}
{{primary sources|date=December 2012}}
}}

{{Infobox journal
| italic title = force
| title = Group Facilitation
| cover = [[File:Low resolution cover of the journal Group Facilitation - A Research and Applications Journal.jpg|200px]]
| editor = Stephen Thorpe
| discipline = [[Management]], [[business]]
| former_names = 
| abbreviation = Group Facil.
| publisher = [[International Association of Facilitators]]
| country =
| frequency = Annual
| history = 1999-present
| openaccess = 
| license = 
| impact = 
| impact-year = 
| website = http://iaf-world.org/index/ToolsResources/IAFJournal.aspx
| link1 = 
| link1-name = 
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 476092163
| LCCN = 2001213705
| CODEN = 
| ISSN = 1534-5653
| eISSN = 1545-5947
}}
'''''Group Facilitation''': A Research and Applications Journal'' is a [[Peer review|peer-reviewed]] [[academic journal]] covering all aspects of group facilitation and the repercussions for the individuals, groups, organizations, and communities involved. It is published by the [[International Association of Facilitators]] and the current [[editor-in-chief]] is Stephen Thorpe.

== Overview ==
The journal has the following sections: ''Application and Practice'', containing articles that reflect on the facilitator experience; ''Theory and Research'', containing articles that explore, propose, or test practices, principles, or other aspects of facilitation models; ''Edge Thinking'', containing commentaries on new concepts and issues; and [[Book review|''Book Reviews'']].

== History ==
The journal originally started production during 1996 when the IAF website was first launched.<ref>{{cite web |first=Beret E. |last=Griffiths |first2=Jean |last2=Watts |title=A chronological history of the IAF |url=http://www.iaf-world.org/aboutiaf/HistoryofIAF.aspx |publisher=International Association of Facilitators |accessdate=18 December 2012}}</ref> The first issue of the journal was officially published during the fall of 1999. Since this date, eleven issues have been published.<ref>{{cite web |url=http://iaf-world.org/index/ToolsResources/IAFJournal/IAFJournal.aspx |title=Group Facilitation: A Research and Applications Journal |publisher=International Association of Facilitators |accessdate=18 December 2012}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in [[EBSCO Publishing|Business Source]], [[ABI/INFORM]], and [[ProQuest|ProQuest Central]].<ref>{{cite web |title=Group Facilitation: A Research and Applications Journal |url=https://ulrichsweb.serialssolutions.com/title/1355926195765/488502 |work=UlrichsWeb Global Serials Directory |publisher=ProQuest, LLC|accessdate=19 December 2012}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://iaf-world.org/index/ToolsResources/IAFJournal.aspx}}

[[Category:Publications established in 1999]]
[[Category:Business and management journals]]
[[Category:Annual journals]]
[[Category:English-language journals]]
[[Category:Academic journals published by learned and professional societies]]