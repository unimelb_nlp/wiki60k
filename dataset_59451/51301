{{Infobox journal
| title = Experimental & Molecular Medicine
| cover = [[File:Experimental & Molecular Medicine cover.jpg|200px]]
| former_names = Taehan Saenghwa Hakhoe Chapchi, Korean Journal of Biochemistry
| abbreviation = Exp. Mol. Med.
| discipline = [[Biochemistry]], [[molecular biology]]
| editor = [[Dae-Myung Jue]]
| publisher = [[Nature Publishing Group]]
| country = 
| history = 1996-present
| frequency = Monthly
| openaccess = Yes
| license = 
| impact = 3.446
| impact-year = 2014
| ISSN = 1226-3613
| eISSN = 2092-6413
| CODEN = EMMEF3
| JSTOR = 
| LCCN = 
| OCLC = 38557732
| website = http://www.nature.com/emm/index.html
| link1 = http://www.nature.com/emm/archive/index.html
| link1-name = Online archive
| link2 = http://www.ncbi.nlm.nih.gov/pmc/journals/872/
| link2-name = 2008-present archive at PubMed Central
}}
'''''Experimental & Molecular Medicine''''' is a monthly [[peer-reviewed]] [[open access]] [[medical journal]] covering [[biochemistry]] and [[molecular biology]]. It was established in 1964 as the '''''Korean Journal of Biochemistry''''' or '''''Taehan Saenghwa Hakhoe Chapchi''''' and published bi-annually.<ref name= CASSI/> It was originally in Korean becoming an English-language journal in 1975. In 1994 the journal began publishing quarterly. It obtained its current name in 1996 at which time it also began publishing bi-monthly, switching to monthly in 2009. It is the official journal of the [[Korean Society for Medical Biochemistry and Molecular Biology]]. The [[editor-in-chief]] is [[Dae-Myung Jue]] ([[Catholic University of Korea]]). It is published by the [[Nature Publishing Group]]. The full text of the journal from 2008 to the present is available at [[PubMed Central]].

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[BIOSIS Previews]]<ref name= ISI/>
* [[Chemical Abstracts]]<ref name=CASSI>{{cite web|url=http://cassi.cas.org/search.jsp |archive-url=http://wayback.archive-it.org/all/20100211181038/http://cassi.cas.org/search.jsp |dead-url=yes |archive-date=2010-02-11 |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2014-12-13 }}</ref>
* [[Embase]]<ref name=UWEB>{{cite web |title= ''Experimental & Molecular Medicine'' |url= http://www.ulrichsweb.serialssolutions.com/title/1418911498375/84726 |work= [[Ulrichsweb]] |publisher= [[ProQuest]] |accessdate=2014-12-18 |subscription= yes}}</ref>
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]/[[PubMed Central]]<ref name=MEDLINE>{{cite web |url= https://www.ncbi.nlm.nih.gov/nlmcatalog/9607880 |title= ''Experimental & Molecular Medicine'' |work=NLM Catalog |publisher= [[National Center for Biotechnology Information]] |accessdate= }}</ref>
* [[Science Citation Index Expanded]]<ref name=ISI>{{cite web |url= http://ip-science.thomsonreuters.com/mjl/ |title= Master Journal List |publisher= [[Thomson Reuters]] |work= Intellectual Property & Science |accessdate= }}</ref>
*[[Scopus]]<ref name=UWEB/>
}}
According to the ''[[Journal Citation Reports]]'', the journal had a 2013 [[impact factor]] of 2.462, ranking it 54th out of 122 journals in the category "Medicine, Research & Experimental"<ref name=WoS2>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Medicine, Research & Experimental |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref> and 176th out of 291 journals in the category "Biochemistry & Molecular Biology".<ref name=WoS3>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Biochemistry & Molecular Biology |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{reflist}}

== External links ==
*{{Official website|http://www.nature.com/emm/index.html}}
*[http://ksbmb.or.kr/sub/catalog_english.php?CatNo=127 Korean Society for Medical Biochemistry and Molecular Biology]

{{DEFAULTSORT:Experimental and Molecular Medicine}}
[[Category:Biochemistry journals]]
[[Category:English-language journals]]
[[Category:Nature Publishing Group academic journals]]
[[Category:Publications established in 1964]]
[[Category:Monthly journals]]
[[Category:Academic journals associated with learned and professional societies]]