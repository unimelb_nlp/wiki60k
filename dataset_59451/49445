{{Infobox person
|name          = Jack Mahaney
|image         = Jack Mahaney in Professional Criminals of America.png
|image_size    = 
|caption       = 
|birth_name    = 
|birth_date    = 1844
|birth_place   = [[New York City, New York]], [[United States]]
|death_date    = 
|death_place   = 
|death_cause   = 
|resting_place = 
|resting_place_coordinates = 
|residence     = 
|nationality   = [[Irish-American]]
|other_names   = 
|known_for     = Pickpocket, sneak thief and gang leader in New York City during the late 19th century. 
|education     =
|alma_mater    = 
|employer      = 
|occupation    = Criminal
|home_town     = 
|title         = 
|salary        = 
|networth      = 
|height        = 
|weight        = 
|term          = 
|predecessor   = 
|successor     = 
|party         = 
|boards        = 
|religion      = [[Catholic]]
|spouse        = 
|partner       = 
|children      = 
|parents       = 
|relations     = 
|signature     = 
|website       = 
|footnotes     = 
}}

'''Jack Mahaney''' (1844-?) was an American criminal, sneak thief, confidence man and gang leader in New York City during the late-19th century. He was also one of the most notorious prisoner escapees of his time, popularly referred to as the "[[Jack Sheppard|American Jack Sheppard]]", successfully escaping from virtually every major prison in the [[eastern United States]] including [[The Tombs]] and [[Sing Sing]]. Mahaney also escaped from custody which included well-publicized incidents where he jumped from a train without injury.<ref name="Asbury">[[Herbert Asbury|Asbury, Herbert]]. ''The Gangs of New York: An Informal History of the New York Underworld''. New York: Alfred A. Knopf, 1928. (pg. 222-223) ISBN 1-56025-275-8</ref><ref name="Sifakis">Sifakis, Carl. ''The Dictionary of Historic Nicknames: A Treasury of More Than 7,500 Famous and Infamous Nicknames from World History''. New York: Facts on File Publications, 1984. (pg. 13) ISBN 0-87196-561-5</ref>

==Biography==
{{Expand section|more biographical info if known|date=January 2011}}
Jack Mahaney was born in New York City in 1844. When he was 10, his father died and he was sent to a boarding school by his mother. He was, by all accounts, a mischievous student who was frequently in trouble at the school and eventually ran away after being [[flogged]] repeatedly by teachers. When police found him, Mahaney was in company with a gang of teenage "dock rats" and was placed in the House of Refuge. He escaped from the juvenile institution, along with a dozen other youths, and fled to the [[Five Points, Manhattan|Five Points]].<ref name="Asbury"/><ref name="Sifakis"/>

Mahaney joined the gang of Italian Dave, the famed sneak thief and [[fagin]], whose group of street urchins and pickpockets were among the notorious thieves of the Five Points. Italian Dave had between 30-40 youths, between the ages of 9 and 15, whom he housed in a 
Paradise Square tenement building. It was here that Mahaney was trained to steal along with Italian Dave's other pupils.<ref>Johnson, Claudia Durst. ''Youth Gangs in Literature: Exploring Social Issues Through Literature''. Westport, Connecticut: Greenwood Publishing Group, 2004. (pg. 21) ISBN 0-313-32749-1</ref> Practicing on dummies dressed as men and women, Mahaney and the others learned how to pickpocket and snatch valuables such as purses and muffs. They were also instructed on begging and stealing from store windows and counters. Whenever a student was clumsy or otherwise displeased Italian Dave, their leader, dressed in a policeman's uniform, would hit them with a nightstick.<ref name="Asbury"/><ref name="Sifakis"/>

Mahaney was very successful in this trade and, becoming a noted pickpocket and sneak thief in his own right, was personally taken under the wing of Italian Dave and made his protégé. He was also allowed privileges given to few others including going on marauding expeditions where he and Italian Dave would roll a drunk or snatch a purse. Mahaney eventually left the gang as he entered adulthood and formed his own gang of butcher cart thieves active in the Five Points. He also became a noted confidence man and swindler in his later years.<ref name="Asbury"/><ref name="Sifakis"/>

==References==
{{Reflist}}

{{DEFAULTSORT:Mahaney, Jack}}
[[Category:1844 births]]
[[Category:Year of death missing]]
[[Category:American people of Irish descent]]
[[Category:Criminals from New York City]]
[[Category:People from Manhattan]]