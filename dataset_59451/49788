{{other uses}}
{{infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name         = Rupert of Hentzau
| orig title   =
| translator   =
| image        =Hentzau1898.jpg<!--prefer 1st edition - which is the UK version-->
| caption = Cover of 1898 United States [[Grosset & Dunlap]] edition
| author       = [[Anthony Hope]]
| cover_artist =
| illustrator  = [[Charles Dana Gibson]]
| country      = United Kingdom
| language     = English
| series       = 
| genre        = [[Adventure novel]]
| publisher    = [[J. W. Arrowsmith]], Bristol & London
| release_date = 1898 (written in 1894)
| media_type   = Print ([[Hardcover|Hardback]] & [[Paperback]])
| pages        = 385 pp
| preceded_by  = [[The Prisoner of Zenda]]
| followed_by  = 
}}

'''''Rupert of Hentzau''''' is a [[sequel]] by [[Anthony Hope]] to ''[[The Prisoner of Zenda]]'', [[1895 in literature|written in 1895]], but not [[1898 in literature|published until 1898]].

==Plot summary==
The story is set within a [[frame story|framing narrative]] told by a [[supporting character]] from ''[[The Prisoner of Zenda]]''.  The frame implies that the events related in both books took place in the late 1870s and early 1880s.  This story commences three years after the conclusion of ''Zenda'', and deals with the same [[fictional country]] somewhere in Germanic [[Middle Europe]], the kingdom of [[Ruritania]]. Most of the same characters recur: Rudolf Elphberg, the dissolute [[absolute monarch]] of Ruritania; Rudolf Rassendyll, the English gentleman who had acted as his [[political decoy]], being his distant cousin and [[look alike]]; Flavia, the princess, now queen; Rupert of Hentzau, the dashing well-born villain; Fritz von Tarlenheim, the loyal courtier.

Queen Flavia, dutifully but unhappily [[cousin marriage|married to her cousin]] Rudolf V, writes to her true love Rudolf Rassendyll.  The letter is carried by von Tarlenheim to be delivered by hand, but it is stolen by the exiled Rupert of Hentzau, who sees in it a chance to return to favour by informing the pathologically jealous and paranoid King. Rassendyll returns to Ruritania to aid the Queen, but is once more forced to impersonate the King after Rupert shoots Rudolf V. After an epic duel, Rassendyll kills Rupert, but is assassinated in the hour of triumph by one of Rupert's henchmen—and thus is spared the crisis of conscience over whether or not to continue the royal deception for years. He is buried as the King in a [[state funeral]], and Flavia reigns on alone, the last of the Elphberg dynasty.
[[File:Rupert-of-Hentzau-1923-LC-1.jpg|right|thumb|260px| [[Elaine Hammerstein]] and [[Bert Lytell]] in ''Rupert of Hentzau'' (1923)]]

==Adaptations==
Several adaptations were made, although not as many as for the film career of ''Zenda''. Film versions of ''Rupert of Hentzau'' include:
*1915 film version ''[[Rupert of Hentzau (1915 film)|Rupert of Hentzau]]'' starring [[Gerald Ames]] in the title role and [[Henry Ainley]] as Rassendyl
*1923 [[Lewis J. Selznick|Selznick Pictures]] film with [[Lew Cody]] as Rupert, turning the tragic ending on its head: Flavia abdicates to marry Rassendyll, and Ruritania is declared a [[republic]]).
*A spoof version, ''[[Rupert of Hee Haw]]'', was released in 1924. [[Stan Laurel]] plays an alcoholic king, whose queen, [[Mae Laurel]], deposes and replaces him with an identical salesman named Rudolph Razz. Razz's manners are so uncourtly that a courtier, [[James Finlayson (actor)|James Finlayson]], challenges him to a duel.  (See also [[Lord Haw-haw]].)
*A 1964 British television series ''[[Rupert of Hentzau (TV series)|Rupert of Hentzau]]'' starring [[George Baker (actor)|George Baker]] as Rudolf Rassendyl, [[Barbara Shelley]] as Queen Flavia and [[Peter Wyngarde]] as Rupert of Hentzau.

[[David O. Selznick]] considered making a [[film adaptation|film version of the novel]], as a follow-up to his hugely successful 1937 film of the ''The Prisoner of Zenda'', using again [[Douglas Fairbanks Jr.]]  He decided not to because of the tragic subject matter and his commitment to filming ''[[Gone with the Wind (film)|Gone with the Wind]]''. The 1923 silent film version had been produced by his father, [[Lewis J. Selznick]], and his brother, [[Myron Selznick]].

On screen, Rupert as a character has been played by [[matinee idol]]s such as [[Ramon Novarro]] (1922), [[Douglas Fairbanks, Jr.]] (1937), and [[James Mason]] (1952).

==External links==
{{Commons category|Rupert of Hentzau}}
{{Gutenberg|no=1145|name=Rupert of Hentzau}}
* {{librivox book | title=Rupert of Hentzau | author=Anthony HOPE}}

{{The Prisoner of Zenda}}

[[Category:1898 novels]]
[[Category:British novels adapted into films]]
[[Category:English novels]]
[[Category:English adventure novels]]
[[Category:Novels by Anthony Hope]]
[[Category:J. W. Arrowsmith books]]
[[Category:Novels set in Europe]]
[[Category:Sequel novels]]