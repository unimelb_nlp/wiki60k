{{Infobox journal
| title        = Harvard Journal on Legislation
| cover        =
| editor       =
| discipline   = [[Law review]]
| language     = [[English language|English]]
| abbreviation = HJOL
| publisher    = Harvard Law School
| country      = {{flagicon|USA}} [[United States|USA]]
| frequency    = Semiannually<br /><small>(Twice each year, Winter and Summer)</small>
| history      = 1964 to present
| openaccess   =
| license      =
| impact       =
| impact-year  =
| website      = http://www.law.harvard.edu/students/orgs/jol/
| link1        = http://www.law.harvard.edu/students/orgs/jol/general/index.php
| link1-name   = HJOL Archive: Vols. 36 and later
| link2        = http://heinonline.org/HOL/Index?collection=journals&index=journals/hjl
| link2-name   = HeinOnline Archive: all Vols.
| RSS          =
| atom         =
| JSTOR        =
| OCLC         =
| LCCN         =
| CODEN        =
| ISSN         = 0017-808X
| eISSN        =
}}
The '''''Harvard Journal on Legislation''''' is a journal of legal scholarship published by students at [[Harvard Law School]].

==Overview==
The ''Harvard Journal on Legislation'' publishes articles analyzing legislation and the legislative process.  The ''Journal'' specializes in articles focusing on the organizational and procedural factors affecting the efficiency and effectiveness of legislative decisionmaking.  In particular, the ''Journal'' aims to publish articles that address legislative reform or that examine public policy problems of nationwide significance and propose legislation to resolve such problems.  Articles are written by leading academics and practitioners, as well as by Harvard Law School students.  The ''Journal'''s biannual Congress Issue includes policy essays written by sitting members of the [[U.S. Congress]].<ref>[http://www.law.harvard.edu/students/orgs/jol/ ''Harvard Journal on Legislation'' homepage]</ref>  The ''Journal'' is generally ranked among the top 50 most influential student-edited law journals.<ref>See Washington & Lee University School of Law, [http://lawlib.wlu.edu/LJ/index.aspx Law Journals: Submissions and Ranking] (''Harvard Journal on Legislation'' ranked at number 46 for "combined score" among student-edited law journals during 2001–08 time period).</ref>

The ''Harvard Journal on Legislation'' is published semiannually (Winter and Summer) by Harvard Law School students.  Additionally, the ''Journal'' holds an annual Symposium featuring discussion of a policy issue of national significance.<ref>[http://www.law.harvard.edu/students/orgs/jol/next/events.php ''Harvard Journal on Legislation'', Symposium webpage]</ref>

Student editors need not participate in a competition to join the ''Journal'', though senior ''Journal'' editors are selected largely through an application and interviewing process.  Upper-level masthead positions are filled by election.

The ''Harvard Journal on Legislation'' maintains an office on the Harvard Law School campus as part of the Student Publications Center in Hastings Hall.

==History==
The ''Harvard Journal on Legislation'' published its first issue in 1964.  The ''Journal''—along with the ''[[Harvard Civil Rights-Civil Liberties Law Review]]'' and the ''[[Harvard International Law Journal]]''—was founded by Harvard Law School Dean [[Erwin N. Griswold]] to provide students who were not members of the ''[[Harvard Law Review]]'' with an opportunity to gain similar writing and editing experience.<ref>Erwin N. Griswold, Ould Fields, New Corne: The Personal Memoirs of a Twentieth Century Lawyer 232 (1992).</ref>  The ''Harvard Journal on Legislation'' was originally affiliated with the Harvard Student Legislative Research Bureau at Harvard Law School.  In addition to printing articles written on legislative matters by academics, attorneys in private practice, and government officials, the ''Journal'' published draft statutes and accompanying analyses prepared by student members of the Harvard Student Legislative Research Bureau.<ref>Erwin N. Griswold, ''Preface'', 1 Harv. J. on Legis. 3, 3–4 (1964).</ref>

By 1969, the ''Journal'' had expanded beyond the Legislative Research Bureau to become "a full-scale law review operation."<ref>Jennifer L. Carter, [http://lsr.nellco.org/cgi/viewcontent.cgi?article=1012&context=harvard/students ''The Rise of the Specialty Journals at Harvard Law School''], Harvard Law School Student Scholarship Series (2007), at 11 (quoting Ronald Patterson, "Legislation Journal", ''[[Harvard Law Record]]'', February 6, 1969, at 4, 4).</ref>  It also became the first of the Harvard Law School Journals to accept first-year students as members of its staff.<ref>[http://lsr.nellco.org/cgi/viewcontent.cgi?article=1012&context=harvard/students ''Id.''], at 12.</ref>

==References==
{{Reflist}}

==External links==
*[http://www.harvardjol.com Official website]

{{Harvard}}

{{DEFAULTSORT:Harvard Journal On Legislation}}
[[Category:American law journals]]
[[Category:Harvard University academic journals]]
[[Category:Harvard Law School]]
[[Category:Publications established in 1964]]
[[Category:Law journals edited by students]]