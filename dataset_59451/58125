{{Infobox journal
| title = Journal of Family Theory and Review
| cover = 
| editor = [https://www.udmercy.edu/about/meet_faculty/clae/Libby-Balter-Blume.htm Libby Balter Blume]
| discipline = [[Family studies]]
| former_names = 
| abbreviation =  J. Fam. Theory Rev.
| publisher = [[Wiley-Blackwell]] on behalf of the [[National Council on Family Relations]]
| country = 
| frequency = Quarterly
| history = 2009-present
| openaccess = 
| license = 
| impact = 
| impact-year = 
| website = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1756-2589
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1756-2589/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1756-2589/issues
| link2-name = Online archive
| JSTOR = 
| OCLC = 712801407
| LCCN = 2009214790
| CODEN = 
| ISSN = 1756-2570
| eISSN = 1756-2589
}}
The [http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1756-2589 Journal of Family Theory and Review] is a quarterly [[peer-reviewed]] [[academic journal]] published by [[Wiley-Blackwell]] on behalf of the [https://www.ncfr.org/ National Council on Family Relations]. Established in 2009 by founding editor  Robert M. Milardo, the current editor-in-chief is [https://www.udmercy.edu/about/meet_faculty/clae/Libby-Balter-Blume.htm Libby Balter Blume] ([[University of Detroit Mercy]]).  The Journal of Family Theory & Review has been accepted for its initial [[Web of Science]] ranking and impact factor in the [[Social Sciences Citation Index]]. The first impact factor will be reported in 2016, a two-year impact factor based on the journal's 2013 and 2014 volumes.

JFTR publishes original contributions in all areas of family theory, including new advances in theory development, reviews of existing theory, and analyses of the interface of theory and method, as well as integrative and theory-based reviews of content areas, and book reviews. Consistent with its mission, JFTR does not publish empirical reports with the exception of meta-analyses of content areas.

The journal draws from a broad range of the social sciences, including [[family studies]], [[sociology]], [[developmental psychology]], [[social psychology]], [[communications]], [[gerontology]], [[gender studies]], and [[health]].. Families are viewed broadly and inclusively to include individuals of varying ages and genders, sexual orientations, ethnicities, and nationalities.

The Journal of Family Theory and Review has a [https://www.ncfr.org/jftr/blog blog], a [https://www.facebook.com/jftrpage Facebook page], and a [https://twitter.com/jftr_ncfr Twitter account] designed to facilitate the exchange and sharing of the thoughtful discussions of issues regarding family theory, integrative ideas, and methods. Family scholars, media and the general public are invited to participate in rigorous, thoughtful conversations.  In 2015 the journal began to explore the development of [[digital scholarship]] and formed a [http://onlinelibrary.wiley.com/doi/10.1111/jftr.12079/full board of scholars to develop strategies] for engaging the public and a wider range of professionals.

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1756-2589}}
* [http://www.ncfr.org/ National Council on Family Relations]

[[Category:Wiley-Blackwell academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 2009]]
[[Category:Sociology journals]]
[[Category:Quarterly journals]]