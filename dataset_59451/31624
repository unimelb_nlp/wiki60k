{{featured article}}
{{Infobox coin
| Country             = United States
| Denomination        = Huguenot-Walloon Tercentenary half dollar
| Value               = 50 cents (0.50
| Unit                = [[United States dollar|US dollars]])
| Mass                = 12.5
| Diameter            = 30.61
| Diameter_inh        = 1.20
| Thickness           = 2.15
| Thickness_inch      = 0.08
| Edge                = [[Reeding|Reeded]]
| Silver_troy_oz      = 0.36169
| Composition =
  {{plainlist |
* 90.0% silver
* 10.0% copper
 }}
| Years of Minting    = 1924
| Mintage = 142,080 including 80 pieces for the [[United States Assay Commission|Assay Commission]]
| Mint marks          = None, all pieces struck at the Philadelphia Mint without mint mark.
| Obverse             = {{Css Image Crop|Image = 1924 50C Huguenot-Walloon New Netherland.jpg|bSize = 475|cWidth = 235|cHeight = 235|oTop = 3|oLeft = 3|Location = center}}
| Obverse Design      = [[Jugate]] busts of [[Gaspard II de Coligny|Gaspard de Coligny]] (left) and [[William the Silent]]
| Obverse Designer    = [[George T. Morgan]], based on sketches by John Baer Stoudt
| Obverse Design Date = 1924
| Reverse             = {{Css Image Crop|Image = 1924 50C Huguenot-Walloon New Netherland.jpg|bSize = 475|cWidth = 235|cHeight = 235|oTop = 3|oLeft = 238|Location = center}}
| Reverse Design      = The ship ''Nieuw Nederlandt''
| Reverse Designer    = [[George T. Morgan]], based on sketches by John Baer Stoudt
| Reverse Design Date = 1924
}}

The '''Huguenot-Walloon half dollar''' or '''Huguenot-Walloon Tercentenary half dollar''' is a [[Early United States commemorative coins|commemorative coin]] issued by the [[United States Mint|United States Bureau of the Mint]] in 1924. It marks the 300th anniversary of the voyage of the ''Nieuw Nederland'' which landed in the New York area in 1624. Many of the passengers were [[Huguenots]] who had lived in what is now Belgium, where they were known as [[Walloons]]; they became early settlers of New York State and the surrounding area.

A commission run by the [[Federal Council of Churches]] in America sought issuance of a [[Half dollar (United States)|half dollar]] to mark the anniversary, and the bill passed through Congress without opposition in 1923 and was signed by President [[Warren G. Harding]]. Sketches were prepared by commission chairman Rev. John Baer Stoudt and converted to plaster models by the Mint's aging chief engraver, [[George T. Morgan]]. The models were initially rejected by the [[Commission of Fine Arts]], which required revisions under the supervision of [[Buffalo nickel]] designer [[James Earle Fraser (sculptor)|James Earle Fraser]].

Of the 300,000 coins authorized by Congress, fewer than half were actually struck, and of these, 55,000 were returned to the Mint and released into circulation. The coin excited some controversy because of its sponsorship by a religious group.  The choice of [[William the Silent]] and [[Gaspard II de Coligny|Gaspard de Coligny]] to appear on the [[obverse and reverse|obverse]] was also questioned as the men are considered martyrs by the Huguenots and died decades before the voyage of the ''Nieuw Nederlandt''. The coins are currently valued in the hundreds of dollars, depending on condition.

== Background ==
The [[Huguenot]]s were French Protestants, who were often in conflict with the Catholic majority. Many Huguenots fled France in the 16th and 17th centuries, when there was intense persecution of them, most notably in the [[St. Bartholomew's Day massacre]] of 1572.  Among those who fell in the rioting that day was the Huguenot military and political leader, Admiral [[Gaspard II de Coligny|Gaspard de Coligny]].{{sfn|Slabaugh|p=59}}

Many Huguenots who fled France settled in what is today Belgium and have become known as [[Walloons]]. Others came to live in the Netherlands.  [[William the Silent]], the [[Prince of Orange]] was one of the leaders of the [[Dutch Revolt|Dutch War of Independence]] against Spain.{{sfn|Slabaugh|p=59}} He was assassinated in 1584 by [[Balthasar Gérard]], a pro-Spanish extremist. Some Huguenots went elsewhere: on March 29, 1624, the ship ''Nieuw Nederlandt'' set out for [[New Netherland]], the Dutch possessions centered on what is now the state of New York; more ships followed. These were many of the early settlers of the area.  In 1626, [[Peter Minuit]], the Director General for the [[Dutch West India Company]], famously purchased the island of Manhattan from the Native Americans for goods worth some 60 guilders, often rendered as $24.{{sfn|Flynn|p=103}}

The Huguenot-Walloon New Netherland Commission was established in 1922 under the auspices of the [[Federal Council of Churches|Federal Council of Churches of Christ in America]] in anticipation of the upcoming anniversary. President [[Warren G. Harding]] was the honorary president of the commission, and Belgium's King [[Albert I of Belgium|Albert I]] accepted an honorary chairmanship.{{sfn|House of Representatives hearings|pp=6–7}} The commission, led by its chairman, Rev. Dr. John Baer Stoudt, planned an observance for the 300th anniversary of the ''Nieuw Nederlandt'' voyage, and sought the issuance of commemorative stamps and coins.{{sfn|Swiatek & Breen|pp=105–106}} At the time, commemorative coins were not sold by the government—Congress, in authorizing legislation, designated an organization which had the exclusive right to purchase the coins at face value and vend them to the public at a premium.{{sfn|Slabaugh|pp=3–5}}

== Legislation ==
A bill for a Huguenot-Walloon half dollar was introduced in the House of Representatives on January 15, 1923 by Pennsylvania Congressman [[Fred Gernerd]],<ref name ="timeline">{{cite web|url=http://congressional.proquest.com/congressional/result/congressional/pqpdocumentview?accountid=14541&groupid=96011&pgId=45dcf3b1-2ec3-4d45-90fb-77ef29f7cf97&rsId=14EB66F695F#BillActions|title=67 Bill Profile S. 4468 (1921–1923)|publisher=ProQuest Congressional|accessdate=August 21, 2015}}{{subscription}}</ref> who was of Huguenot descent. It received a hearing before the House Committee on Coinage, Weights, and Measures on February 7, with Indiana Representative [[Albert Vestal]], a Republican, presiding. Congressman Gernerd told the committee that there were plans  to have local celebrations in 1924 to celebrate the 300th anniversary of the Huguenot voyage in cities where people of that heritage lived. Gernerd stated that while the sale of half dollars would raise money towards the observance, it was not intended as a serious fundraiser, but as a symbol of the occasion. He reminded the committee that the 300th anniversary of the voyage of the ''[[Mayflower]]'' had [[Pilgrim Tercentenary half dollar|seen a half dollar issued]].{{sfn|House of Representatives hearings|pp=1–4}}

[[File:John Baer Stoudt.jpg|thumb|upright|left|Rev. John Baer Stoudt]]
Vestal stated that he could not support the bill as introduced because it did not designate who should order the coins, but Gernerd indicated that the Fifth National Bank of New York had agreed to act in that capacity. Stoudt appeared before the committee, explaining that his commission planned a design with the arrival of the ''New Netherlandt'' for one side, and for the other, [[Peter Minuit]] purchasing Manhattan from the Native Americans. New Jersey Congressman [[Ernest R. Ackerman]] briefly addressed the committee in support, noting that the coins would likely be retained as souvenirs, to the profit of the government. West Virginia's [[Wells Goodykoontz]] also spoke in favor. The witnesses, all urging passage of the bill, concluded with a number of local pastors, led by E.O. Watson, secretary of the Federal Council of Churches.{{sfn|House of Representatives hearings|pp=4–11}} On February 10, 1923, Vestal issued a report recommending that the bill pass with an amendment adding the bank as the ordering organization.<ref>{{cite web|title=Coinage of 50-cent pieces in commemoration of the three hundredth anniversary of the settlement of New Netherland, the Middle States, in 1624, by Walloons, French and Belgian Huguenots, under the Dutch West India Company|last=Vestal|first=Albert|authorlink=Albert Vestal|url=http://congressional.proquest.com/congressional/result/pqpresultpage.gispdfhitspanel.pdflink/$2fapp-bin$2fgis-serialset$2f8$2fd$2f3$2f7$2f8158_hrp1577_from_1_to_1.pdf+/new+netherland+walloon$40$2fapp-gis$2fserialset$2f8158_h.rp.1577$40Serial+Set+1$40House+and+Senate+Reports$3b+Reports+on+Public+Bill$40February+10,+1923?pgId=ffcb2463-99fa-48ba-88a0-884568f1c4d1&rsId=14EB719364B|date=February 10, 1923|publisher=United States Government Printing Office}}{{subscription}}</ref>

A parallel bill was introduced into the Senate by Pennsylvania's [[David A. Reed]] on January 29, 1923<ref name ="timeline">{{cite web|url=http://congressional.proquest.com/congressional/result/congressional/pqpdocumentview?accountid=14541&groupid=96011&pgId=45dcf3b1-2ec3-4d45-90fb-77ef29f7cf97&rsId=14EB66F695F#BillActions|title=67 Bill Profile S. 4468 (1921–1923)|publisher=ProQuest Congressional|accessdate=August 21, 2015}}{{subscription}}</ref> and was  referred to the  [[United States Senate Committee on Banking, Housing, and Urban Affairs|Committee on Banking and Currency]].<ref>{{USCongRec|1923|2897|date=February 2, 1923}}</ref> It was reported favorably on February 9 by Pennsylvania's [[George W. Pepper|George Pepper]], and the Senate passed the bill without objection.<ref>{{USCongRec|1923|3295|date=February 9, 1923}}</ref> The Senate-passed bill was received by the House the following day and was referred to Vestal's committee.<ref>{{USCongRec|1923|3416|date=February 10, 1923}}</ref>  On February 19, the House considered its bill on the [[unanimous consent]] calendar.  Gernerd asked that the Senate-passed bill be substituted for the House bill (the two were identical) and when this was agreed, proffered an amendment to add the bank as the depository for the coins. Texas Congressman [[Thomas Lindsay Blanton|Thomas L. Blanton]] asked several questions about the bank's interest, but subsided once he was told that it would receive no compensation and that President Harding was connected with the commission.  The amended bill passed the House without objection.<ref name="Ref-1">{{USCongRec|1923|4034|date=February 19, 1923}}</ref> The following day, the Senate agreed to the House amendment,<ref>{{USCongRec|1923|4061|date=February 20, 1923}}</ref> and the bill was enacted by Harding's signature on February 26, 1923.<ref name = "timeline" /> The act provided for a maximum of 300,000 half dollars.<ref name="Ref-1"/>

== Preparation ==
[[File:1879S Morgan Dollar NGC MS67plus Obverse.png|thumb|right|[[George T. Morgan]] had designed the [[Morgan dollar]] in 1878.]]
Stoudt supplied the concept for the coins, as well as sketches. Rather than seeking a private designer to produce plaster models, the Huguenot-Walloon commission approached the Mint's chief engraver, [[George T. Morgan]], who turned 78 in 1923.  Morgan, best remembered for his 1878 design for the [[Morgan dollar]], had been chief engraver since 1917, following forty years as an assistant, mostly under Chief Engraver [[Charles E. Barber]].  Both Barber and Morgan felt that coins should be designed by the Mint's engravers, and were sometimes hostile when private sculptors were engaged to do the work.{{sfn|Taxay|pp=69–70}}{{sfn|Swiatek & Breen|p=106}}

Morgan's models were transmitted on October 26, 1923, to the [[Commission of Fine Arts]],{{sfn|Taxay|p=70}} charged with rendering advisory opinions on coins by a 1921 [[executive order]] by President Harding.{{sfn|Taxay|pp=v–vi}} Morgan's work was examined by sculptor member [[James Earle Fraser (sculptor)|James Earle Fraser]], designer of the [[Buffalo nickel]]. On November 19, committee chairman Charles Moore wrote to Mint Director [[Robert J. Grant]], "while the ideas intended to be expressed are excellent, the execution is bad.  The lettering is poor, the heads are not well modeled and the ship is ill designed. The workmanship is below the standard of excellence attained in previous coins. The models are therefore not approved."{{sfn|Taxay|p=70}}

After discussion, it was decided to allow Morgan to revise his model under Fraser's supervision. Numismatists Anthony Swiatek and [[Walter Breen]] noted, "[This] must have been doubly and trebly humiliating in that Fraser's initial was then adorning the current 5¢ nickel, while neither Barber's nor Morgan's was on any regular issue coinage then in production".{{sfn|Swiatek & Breen|p=106}} On January 3, 1924, Fraser wrote to Moore that the new models had been considerably improved, and complained that Vestal had advised the Huguenot-Walloon commission to have the models made at the Mint as he had been told by its officials that private artists made models in a [[relief]] too high to be easily coined. "It seems to me perfectly disgusting that this inane and lying criticism should go on constantly".{{sfn|Taxay|p=70}} The Fine Arts Commission approved the revised designs.{{sfn|Taxay|p=70}}

== Design ==
[[File:Hugo-sail1.jpg|thumb|left|The one cent stamp issued by the [[United States Post Office Department]] for the anniversary displays a design similar to the half dollar's reverse.]]
The obverse features [[jugate]] busts of Admiral [[Gaspard II de Coligny|Gaspard de Coligny]] (1519–1572) and [[William the Silent]], the [[Prince of Orange]] (1533–1584). Neither had any direct involvement with the voyage of the ''Nieuw Nederlandt'', having been killed forty years or longer before it.{{sfn|Swiatek & Breen|p=105}} Both were Protestant leaders of [[the Reformation]], and according to Swiatek, "their relationship with the 1624 founding was strictly spiritual in nature,"{{sfn|Swiatek|p=146}} as the two are considered martyrs by Huguenots.{{sfn|Flynn|p=104}} The 1924 ''Report of the Director of the Mint'' explained that both were "leaders in the strife for civil and religious liberty".{{sfn|Flynn|p=103}}  Slabaugh, noting that the coin caused some controversy after it was issued, suggested that if the obverse had shown someone connected with the settlement of New Netherland, "chances are that the coin would have borne no religious significance and its promotion by the Churches of Christ in America would have been given little notice".{{sfn|Slabaugh|p=58}}

The March 29, 1924 edition of the [[Jesuit]] journal ''[[America (Jesuit magazine)|America]]'' contained an article by F.J. Zwierlein, who stated that the new coin "is more Protestant than the descriptions in the newspaper dispatches led us to believe".{{sfn|Zwierlein|p=565}} He asserted that the two men featured on the coin were not killed for their religion and were anti-Catholic: "the United States Government was duped into issuing this Huguenot half-dollar so as to make a Protestant demonstration out of the tercentenary of the colonization of the State of New York".{{sfn|Zwierlein|p=566}} The president of the Huguenot Society of North Carolina responded in a [[letter to the editor]] of ''[[The New York Times]]'', "that Coligny and William the Silent were 'martyrs in the fight for religious liberty' let the truth of history attest".<ref>{{cite news|title=Huguenot half-dollars|newspaper=[[The New York Times]]|date=April 6, 1924|url=https://query.nytimes.com/mem/archive-free/pdf?res=9D03E4D61E3CE733A25755C0A9629C946595D6CF}}{{subscription}}</ref>

The men have their names below the busts on the obverse, and wear hats of their period. They gaze toward the legend <small>[[In God We Trust|IN GOD WE TRUST]]</small>, the only one of the national mottos usually present on U.S. coins to appear. The name of the country arcs above their heads, while <small>HUGUENOT HALF DOLLAR</small> is below them. Morgan's initial "M" is on Coligny's shoulder.{{sfn|Swiatek|pp=145–146}} The reverse depicts the ship ''Nieuw Nederlandt'' and the words, <small>HUGUENOT – WALLOON – TERCENTENARY – FOUNDING OF NEW NETHERLAND</small> with the years 1624 and 1924 to either side of the ship.{{sfn|Flynn|p=103}} Stoudt's sketch for the reverse was also used on the one cent denomination of the stamp set issued in conjunction with the tercentenary.{{sfn|Swiatek & Breen|p=106}}

Art historian [[Cornelius Vermeule]] noted that the half dollar was probably one of Morgan's last works (he died in January 1925) and that the coin "is a worthy conclusion to Morgan's long career of distinguished and rich production, marked by imagination within the conservative framework and by a generally high level of appeal".{{sfn|Vermeule|p=167}} Vermeule stated that the Huguenot-Walloon half dollar showed "that the die-engravers trained in and around the Mint did have the ability to combine clear-cut designs with considerable detail."{{sfn|Vermeule|pp=167–168}}

==Production, distribution and collecting ==
[[File:NNC-1924-50C-Huguenot-Walloon Tercentenary half dollar (reverse, uniface die trial).jpg|thumb|right|Uniface die trial of the reverse, in the [[National Numismatic Collection]]]]

A total of 142,080 Huguenot-Walloon half dollars were struck at the [[Philadelphia Mint]] in February and April 1924, with 80 of those pieces retained for inspection and testing by the 1925 [[Assay Commission]]. The Huguenot-Walloon commission, to boost sales, engaged as distributor the man they considered to be the most prominent numismatist in the country, Moritz Wormser, president of the [[American Numismatic Association]] (ANA).{{sfn|Bowers|p=174}} Wormser's involvement, and the fact that Stoudt was an ANA member, led numismatist John F. Jones to deem this issue "the first instance we believe, where the coin fraternity has been consulted in the issue of a commemorative half dollar".{{sfn|Jones|p=394}}

The coins were sold to the public for $1 each through the Fifth National Bank and other outlets. Bulk sales were made to certain groups.{{sfn|Bowers|p=174}} The coins did not sell as well as expected, and 55,000 were returned, after which they were placed in circulation.  Relatively few are known in worn condition, causing author and coin dealer [[Q. David Bowers]] to conclude the public picked them out of pocket change. Money from the Huguenot-Walloon half dollar was used towards a celebration in New York in May 1924, during which the National Huguenot Memorial Church on [[Staten Island]] was dedicated. There was some debate in the pages of ''[[The Numismatist]]'', the ANA's journal, both as to whether William and Coligny should have appeared on the coin since they had nothing to do with the voyage, and whether the Churches of Christ should be allowed to sponsor a coin in view of the [[First Amendment to the United States Constitution|First Amendment's]] prohibition [[Establishment clause|of an establishment of religion]].{{sfn|Bowers|pp=174–175}} There was controversy in the press, which criticized the inclusion of William and Coligny as irrelevant to the commemoration and as religious propaganda.{{sfn|Jones|pp=394–395}} This in 1925 made politically infeasible the attempts of Minnesota Representative [[Ole Juulson Kvale]], a Lutheran pastor, to obtain a coin for the [[Norse-American Centennial]]; he instead settled for [[Norse-American medal|a congressionally authorized medal]].{{sfn|Shultz|p=1287}}

Arlie R. Slabaugh, in his 1975 volume on commemoratives, noted that a uniface die trial of the reverse in brass was made.{{sfn|Slabaugh|p=59}} Swiatek in 2012 stated that both obverse and reverse die trials are known.{{sfn|Swiatek|p=146}} The edition of [[R.S. Yeoman]]'s ''[[A Guide Book of United States Coins]]'' published in 2015 lists the half dollar at between $130 and $650 depending on condition; a near-pristine specimen sold for $2,115 in 2014.{{sfn|Yeoman|p=1131}}

== References ==
{{Reflist|20em}}

== Sources ==
* {{cite book
  | last = Bowers
  | first = Q. David
  | authorlink = Q. David Bowers
  | year = 1992
  | title = Commemorative Coins of the United States: A Complete Encyclopedia
  | publisher = Bowers and Merena Galleries, Inc.
  | location = Wolfeboro, NH
  | isbn = 978-0-943161-35-8
  | ref = {{sfnRef|Bowers}}
  }}
* {{cite book
  | last = Flynn
  | first = Kevin
  | year = 2008
  | title = The Authoritative Reference on Commemorative Coins 1892–1954
  | publisher = Kyle Vick
  | location = Roswell, GA
  | oclc = 711779330
  | ref = {{sfnRef|Flynn}}
  }}
*{{cite journal
 | last=Jones
 | first = John F.
 | title = The Series of United States Commemorative Coins 
 | url = http://www.exacteditions.com/browsePages.do?issue=45186&size=3&pageLabel=393
 | subscription = yes
 | journal = [[The Numismatist]]
 | publisher = [[American Numismatic Association]]
 | date = May 1937
 | pages = 393–396
 | ref = {{sfnRef|Jones}}
}}
*{{cite journal
|last=Shultz
|first=April
|title='The Pride of the Race Had Been Touched': The 1925 Norse-American Immigration Centennial and Ethnic Identity
|journal=Journal of American History
|date=March 1991
|volume=77
|issue=4
|pages=1265–1295
|url=http://web.b.ebscohost.com/ehost/pdfviewer/pdfviewer?sid=394ff980-69f4-458d-bf1f-c108a51869b1%40sessionmgr111&vid=15&hid=124
|issn=0021-8723
| ref = {{sfnRef|Shultz}}
  }}{{subscription}}
* {{cite book
  | last = Slabaugh
  | first = Arlie R.
  | edition = second
  | year = 1975
  | title = United States Commemorative Coinage
  | publisher = Whitman Publishing
  | location = Racine, WI
  | isbn = 978-0-307-09377-6
  | ref = {{sfnRef|Slabaugh}}
  }}
* {{cite book
  | last = Swiatek
  | first = Anthony
  | year = 2012
  | title = Encyclopedia of the Commemorative Coins of the United States
  | publisher = KWS Publishers
  | location = Chicago
  | isbn = 978-0-9817736-7-4
  | ref = {{sfnRef|Swiatek}}
  }}
* {{cite book
  | last = Swiatek
  | first = Anthony
  | last2 = Breen
  | first2 = Walter
  | authorlink2 = Walter Breen
  | year = 1981
  | title = The Encyclopedia of United States Silver & Gold Commemorative Coins, 1892 to 1954
  | publisher = Arco Publishing
  | location = New York
  | isbn = 978-0-668-04765-4
  | ref = {{sfnRef|Swiatek & Breen}}
  }}
* {{cite book
  | last = Taxay
  | first = Don
  | authorlink = Don Taxay
  | year = 1967
  | title = An Illustrated History of U.S. Commemorative Coinage
  | publisher = Arco Publishing
  | location = New York
  | isbn = 978-0-668-01536-3
  | ref = {{sfnRef|Taxay}}
  }}
* {{cite book
|author=United States House of Representatives Committee on Coinage, Weights and Measures
|title=Coinage of 50-Cent Pieces in Commemoration of the Three Hundredth Anniversary of the Founding of New Netherland, the Middle States, in 1624
|date=February 7, 1923
|url=http://congressional.proquest.com/congressional/result/pqpresultpage.gispdfhitspanel.pdflink/$2fapp-bin$2fgis-hearing$2fa$2f3$2fc$2f5$2fhrg-1923-cwe-0001_from_1_to_13.pdf+/huguenot-walloon$40$2fapp-gis$2fhearing$2fhrg-1923-cwe-0001$40Hearing$40Hearings+Published$40February+07,+1923?pgId=0d592972-db11-4ff5-a8dd-71c29fb559f8&rsId=14EB660169D
|publisher=[[United States Government Printing Office]]
| location = Washington, DC
|ref={{sfnRef|House of Representatives hearings}}
}}{{subscription}}
* {{cite book
  | last = Vermeule
  | first = Cornelius
  | authorlink = Cornelius Clarkson Vermeule III
  | year = 1971
  | title = Numismatic Art in America
  | publisher = The Belknap Press of Harvard University Press
  | location = Cambridge, MA
  | isbn = 978-0-674-62840-3
  | ref = {{sfnRef|Vermeule}}
  }}
* {{cite book
  | last = Yeoman
  | first = R.S.
  | authorlink = Richard S. Yeoman
  | year = 2015
  | title = [[A Guide Book of United States Coins]]
  | edition = 1st Deluxe
  | publisher = Whitman Publishing
  | location = Atlanta, GA
  | isbn = 978-0-7948-4307-6
  | ref = {{sfnRef|Yeoman}}
  }}
* {{cite journal
 | last = Zwierlein
 | first = F.J.
 | title = Huguenot Half-Dollar
 | journal = [[America (magazine)|America]]
 | date = March 29, 1924
 | pages =565–566
 | ref = {{sfnRef|Zwierlein}}
 | url = http://web.a.ebscohost.com/ehost/pdfviewer/pdfviewer?vid=12&sid=c8b8e690-ba1e-4bf6-b712-245a27aae9e3%40sessionmgr4004&hid=4112
}}{{subscription}}

== External links ==
* {{Commons category-inline|Huguenot-Walloon half dollar}}

{{Coinage (United States)}}
{{Portal bar|Arts|Business and economics|Ohio|Numismatics|United States|Visual arts}}



[[Category:1924 introductions]]
[[Category:Commemorative coins of the United States]]
[[Category:Fifty-cent coins]]
[[Category:Huguenot history in the United States]]
[[Category:Huguenots]]
[[Category:United States silver coins]]
[[Category:Walloon diaspora]]