{{Infobox journal
| title = British Journal of Social Psychology
| cover =
| discipline= [[Social psychology]]
| abbreviation= Br. J. Soc. Psychol.
| formernames = British Journal of Social and Clinical Psychology
| editor = [[Jolanda Jetten]], John Dixon
| publisher = [[Wiley-Blackwell]] on behalf of the [[British Psychological Society]]
| country = [[United Kingdom]]
| history = 1962-present
| frequency = Quarterly
| impact = 2.056
| impact-year = 2010
| website = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)2044-8309
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)2044-8309/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)2044-8309/issues
| link2-name = Online archive
| ISSN = 0144-6665
| eISSN = 2044-8309
| OCLC = 475047529
| CODEN = BJSPDA
| LCCN = 81642357
}}
The '''''British Journal of Social Psychology''''' is a [[Peer review|peer-reviewed]] [[academic journal]] published by [[Wiley-Blackwell]] on behalf of the [[British Psychological Society]]. It publishes original papers on subjects like [[social cognition]], attitudes, group processes, social influence, intergroup relations, self and identity, nonverbal communication, and [[Social psychology|social psychological]] aspects of affect and emotion, and of language and discourse. The journal was established in 1962 as the ''British Journal of Social and Clinical Psychology'' and obtained its current title in 1981. According to the ''[[Journal Citation Reports]]'', the journal has a 2010 [[impact factor]] of 2.056.<ref name=WoS>{{cite book |year=2012 |chapter=British Journal of Social Psychology |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2012-03-16 |series=Web of Science |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)2044-8309}}

[[Category:Publications established in 1962]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Social psychology journals]]
[[Category:British Psychological Society academic journals]]