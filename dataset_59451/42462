<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{{Use dmy dates|date=December 2016}}
{{Use British English|date=December 2016}}
{|{{Infobox Aircraft Begin
 | name=M.R.1
 | image=Bristol MR.1.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=Experimental metal reconnaissance
 | national origin=[[United Kingdom]]
 | manufacturer=[[Bristol Aeroplane Company|The British & Colonial Aeoplane Co. Ltd]]
 | designer=[[Frank Barnwell]]
 | first flight=1917
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=2
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Bristol M.R.1''' was an experimental [[biplane]] with an [[aluminium]] [[monocoque]] [[fuselage]] and metal wings, produced by Bristol during the [[World War I|First World War]]. Two were built to government order.

==Development==
Soon into the development of powered flight, some manufacturers were beginning to consider the use of metal in airframes to replace wood. Metal structures, even fabric-covered metal frames, offered greater robustness for handling and transportation as well as better resistance to tropical climates, and some designers could see the possibilities of metal skinning, stressed or not, for aerodynamically-clean [[Cantilever#Aircraft|cantilever]] wings and advanced monocoque fuselages. There was a realisation too, that mild steel, familiar from bicycle manufacture but with a low strength-to-weight ratio, was not going to be the material of choice once the problems of joining aluminium alloy members together and preventing their corrosion had been solved. [[Vickers]] in the UK were one of the first to make steel-framed and [[Spar (aviation)|spar]]red aircraft that flew, with their series of [[Robert EsnaultPelterie|R.E.P]]-type [[Vickers Monoplane|monoplanes no.s 1-8]] produced between 1911 and 1913.<ref>{{Harvnb|Andrews|Morgan|1988|pp=34–42}}</ref> In Germany, Junkers produced the first true all-metal (for years, aircraft with fabric-clad metal frames were described as all-metal, but the Junkers was steel-skinned as well) aircraft,<ref>{{Harvnb|Turner|Nowarra|1971|p=11}}</ref> the [[Junkers J.1]], flown in 1915. Bristol's first draft designs for metal aircraft date from 1914, but it was not until the increase of aircraft production during the First World War began to put pressure on the supply of high-grade timber that there was official interest. During 1916 Bristol's designer, [[Frank Barnwell]] submitted a design<ref name="Barnes">{{Harvnb|Barnes|1964|pp=126–9}}</ref> for a metal two-seat [[reconnaissance]] aircraft, the M.R.1 (M.R. for Metal Reconnaissance) and gained a contract for two evaluation aircraft.

The fuselage construction was quite novel. Barnwell borrowed from marine experience by using [[duralumin]] sheet, varnished to prevent corrosion and used these to make the fuselage in four sections. The two forward sections were semi-monocoque (i.e. open channels) with braced longitudinal upper members which, bolted together, held the engine, a water-cooled inline upright 140&nbsp;hp (100&nbsp;kW) [[Hispano-Suiza]]) and the pilot's cockpit. Aft, two more sections, both true monocoques, held the observer and carried the tail unit. The two cockpits were close together, with the pilot under the wing at mid-[[Chord (aircraft)|chord]] and the observer under a trailing edge cutout; Barnwell proposed that the short observer's fuselage section should be removable to turn the M.R.1 into a single-seater, though this configuration was not realised. The monocoque sections were very early examples of double-skinned construction, with a smooth outer skin riveted to a longitudinally-corrugated inner skin. The detailed design was by [[W.T.Read]]. The complete fuselage was of round-cornered rectangular cross-section and quite slender, mounted between the wings. The M.R.1 was a two-bay biplane without [[Stagger (aviation)|stagger]] or [[Swept wing|sweep]], with [[aileron]]s on both planes. Aluminium wing spars proved difficult to make sufficiently rigid and Bristol outsourced their manufacture to [[The Steel Wing Company]] at Gloucester, who had built experimental steel wings for other aircraft.<ref name="Barnes"/>

With the fuselage of the first M.R.1 completed before the wings, Bristol decided to make a set of conventional wooden wings, with ailerons only on the upper planes, for flight trials in mid-1917.  These went well and the aircraft was handed over to the [[Air Ministry|Air Board]] in October 1917. The second M.R.1 did not fly until late in 1918 when the metal wings were at last ready, powered by a 180&nbsp;hp (130&nbsp;kW) [[Wolseley Viper]] engine. It was damaged beyond repair at the end of its delivery flight to the [[Royal Aircraft Establishment]] in April 1919. The first M.R.1 was fitted with metal wings by 1918 and continued to provide useful information on metal airframe construction. In 1923, Bristol's rationalisation of type numbers labelled the M.R.1 the Type 13.<ref name="Barnes"/>
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications==
<!--Use one OR other of the two specification templates. Delete the template code of the one you do not use. aero-specs is designed to handle the specification of gliders and lighter-than-air craft well. They each have their own documentation. In aircraft-specifications the parameter "xxxx more" allows for the addition of a qualifier to the value eg "at low level", "unladen". -->

{{aerospecs
|ref={{harvnb|Barnes|1964|page=129}}  Unfortunately Barnes did not state which engine the following specifications apply to, nor if the metal or wooden wings were used.
|met or eng?=eng<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->

|crew=two
|capacity=
|length m=12.85
|length ft=27
|length in=0
|span m=8.23
|span ft=42
|span in=2
|height m=3.12
|height ft=10
|height in=3
|wing area sqm=42.6
|wing area sqft=458
|empty weight kg=770
|empty weight lb=1,700
|gross weight kg=1,275
|gross weight lb=2,810
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->

|eng1 number=
|eng1 type=
|eng1 kw=<!-- prop engines -->
|eng1 hp=<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|max speed kmh=177
|max speed mph=110
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=
|range miles=
|endurance h=5<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->

|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{commons category|Bristol aircraft}}
;Notes
{{reflist}}
;Bibliography
{{refbegin}}
*{{cite book |title= Vickers Aircraft since 1908|last=Andrews|first=C.F.|last2=Morgan|first2=E.B. |authorlink= |year=1988 |publisher=Putnam Publishing  |location=London |isbn= 0-370-00015-3|page= |pages= |url=|ref=harv }}
*{{cite book |title= Junkers: an aircraft album no.3|last= Turner|first=P. StJ.|last2=Nowarra|first2=Heinz|authorlink= | year=1971 |publisher=Arco Publishing  |location=London |isbn= 0-668-02506-9|page= |pages= |url=|ref=harv }}
*{{cite book |title= Bristol Aircraft since 1910|last= Barnes|first=C. H. |authorlink= |year=1964 |publisher=Putnam Publishing  |location=New York |isbn= 0-85177-815-1|page= |pages= |url=|ref=harv }}
{{refend}}

<!-- ==External links== -->
{{Bristol aircraft}}

[[Category:British military aircraft 1910–1919]]
[[Category:Bristol Aeroplane Company aircraft|M.R.1]]