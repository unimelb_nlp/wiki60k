<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = Vixen<!--please avoid stating manufacturer in this field; it's stated two lines below -->
  |image = Vickers Vixen.jpg
  |caption = Vickers Vixen
}}{{Infobox Aircraft Type
  |type = Light Bomber / Reconnaissance / Fighter
  |manufacturer = [[Vickers]]
  |designer = <!--only appropriate for individuals, not for project leaders or a company or corporation-->
  |first flight = February 1923 
  |introduced = <!--date the aircraft entered or will enter military or revenue service-->
  |introduction= <!--date the aircraft will enter military or revenue service, if the aircraft is still in development -->
  |retired = <!--date the aircraft left military or revenue service. If vague or multiples, it probably should be skipped-->
  |status = <!--in most cases, this field is redundant; use it sparingly-->
  |primary user = [[Chile]]<!--please list only one, and don't use those tiny flags as they limit horizontal space-->
  |more users =  <!--limited to three "more users" total. please separate with <br/>-->
  |produced = <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
  |number built = 20
  |unit cost = 
  |developed from = <!-- the aircraft which formed the basis for the topic type -->
  |variants with their own articles = [[Vickers Venture]]<br />
[[Vickers Valparaiso]]
}}
|}
The '''Vickers Vixen''' was a [[United Kingdom|British]] general-purpose [[biplane]] of the 1920s. Designed and developed by [[Vickers]] in a number of variants, with 18 Vixen Mark V sold to [[Chile]]. A prototype of a version with metal wings was built as the '''Vickers Vivid'''. The Vixen also formed the basis of the closely related [[Vickers Venture|Venture]] and [[Vickers Valparaiso|Valparaiso]] aircraft, which were also built and sold in small numbers in the 1920s

==Development and design==
In 1922, Vickers designed a two-seat biplane as a private venture as a possible replacement for the [[Airco DH.9A]] and [[Bristol F.2 Fighter]]. Building on the experience of the unsuccessful wartime [[Vickers FB.14|FB.14]] [[fighter aircraft|fighter]]-[[reconnaissance]] aircraft, the Vixen was a single-bay biplane with a steel tube [[fuselage]] and wooden wings, powered by a 450&nbsp;hp (340&nbsp;kW) [[Napier Lion]] engine. The first prototype aircraft, the '''Type 71 Vixen I''', given the civil registration ''G-EBEC'', flew in February 1923.<ref name ="mason bomber">{{Cite book |author=Mason, Francis K |title=The British Bomber since 1914 |publisher=Putnam Aeronautical Books |location= London |year=1994 |isbn= 0-85177-861-5}}</ref> It was tested at [[RAF Martlesham Heath|Martlesham Heath]] and showed good performance, prompting modification to a day [[bomber]] role as the '''Type 87 Vixen II''', which was fitted with a ventral [[radiator]] between the [[Landing gear|undercarriage]] legs replacing the car-type radiator of the Vixen I, first flying in this form on 13 August 1923.<ref name="Andrews Vickers">{{cite book |last= Andrews|first=E.N. |author2=Morgan, E.B.  |title= Vickers Aircraft Since 1908 |edition= Second |year= 1988 |publisher= Putnam|location= London |isbn=0-85177-815-1 }}</ref> The Vixen I and II formed the basis of the Venture army co-operation aircraft for the [[Royal Air Force]] and the Valparaiso for export purposes.

The next version of the Vixen was the '''Vickers Type 91 Vixen III''', first flying in April 1924 which was fitted with larger wings (with wingspan of 44&nbsp;ft/13.4&nbsp;m rather than 34&nbsp;ft 6&nbsp;in/10.5&nbsp;m earlier aircraft) for increased performance at altitude,<ref name="Andrews Vickers"/> and reverted to a nose-mounted radiator. The Vixen III was tested with both wheel and [[floatplane|float]] undercarriages, and was later converted back to a landplane, and used for air racing, competing in the 1925, 1926 and 1927 [[King's Cup Race]]s.<ref name="Jacksonv3">{{cite book |last= Jackson|first= A.J |title= British Civil Aircraft since 1919 Volume 3|year= 1988|publisher= Putnam|location= London|isbn=0-85177-818-6}}</ref> The Vixen III formed the basis of the '''Type 116 Vixen V''', fitted with a high-compression Lion V engine and a modified tail, of which 18 were purchased by [[Chile]].

[[File:Vickers Vivid in flight.jpg|thumb|left|300px|<center>Vickers Vivid</center>]]The Vixen II prototype was modified to use the more powerful [[Rolls-Royce Condor]] engine at the end of 1924 as the '''Type 105 Vixen IV''', which was intended for use as a [[night fighter]]. While it showed improved performance over the Lion-powered versions, it was not successful, and was modified with the enlarged wings of the Vixen III as a general-purpose aircraft (the '''Type 124 Vixen VI''') for evaluation as a private venture entry to meet the requirements of [[Air Ministry]] [[List of Air Ministry Specifications|Specification 26/27]], competing against the [[Bristol Beaver]], [[Fairey Ferret]], [[de Havilland Hound]], [[Gloster Goral]], [[Westland Wapiti]], and Vickers' own [[Vickers 131 Valiant|Valiant]]. The Vixen was rejected on the grounds that the Condor engine was too heavy and powerful for the role.<ref name ="mason bomber"/>

When the problems encountered by the wooden wings of the Vixen V in Chile were realised, it was decided to produce a version with metal wings. This was initially designated Vixen VII, but was soon renamed '''Vickers Vivid'''. The Vixen III prototype was rebuilt with metal wings to become the '''Type 130 Vivid''', first flying in this form on 27 June 1927,<ref name="Andrews Vickers"/> powered by a Lion VA engine, later being re-engined with a 540&nbsp;hp (400&nbsp;kW) Lion XI as the '''Type 146 Vivid'''. The Vivid was evaluated by [[Romania]], but no orders resulted, with the prototype being sold to a private buyer in 1931, being destroyed in a fire at [[Chelmsford]] in 1932.<ref name ="mason bomber"/>

==Operational history==
The [[Chilean Air Force|Military Aviation Service of Chile]] placed an initial order for twelve Vixen Vs in May 1925, this being increased to 18 in July. While prone to engine problems owing to the problems with the special fuel (⅔ petrol to ⅓ [[Benzene|benzol]]) required for the high-compression Lion V engine, and requiring frequent re-rigging owing to the use of wooden wings in the high temperature of Northern Chile, the Vixen Vs, operated by the ''Grupo Mixto de Aviación'' N° 3.<ref>{{cite web |url=http://html.rincondelvago.com/aviacion_1.html |title= Aviación|accessdate=2007-08-04 |language= Spanish}}</ref> were popular in Chilean service, being used for long-distance flights of several hundred miles and continued in service for several years.<ref name="Andrews Vickers"/> Vixens participated in bombing raids against mutinying ships of the [[Chilean Navy]] (including the Battleship {{Ship|Chilean battleship|Almirante Latorre||2}}) during the [[Sailors' mutiny]] of September 1931.

After rejection by the Royal Air Force, the Vixen VI, piloted by the [[Test pilot]] [[Joseph Summers]] and [[Colonel]] Charles Russell of the [[Irish Air Corps]], carried the first [[Ireland|Irish]] [[Air Mail]], between [[Galway]] and [[London]].<ref name="Andrews Vickers"/>

==Variants==
;Type 71 Vixen I
:Prototype fighter-reconnaissance aircraft. Powered by 450&nbsp;hp (340&nbsp;kW) Napier Lion Engine.
;Type 87 Vixen II
:Conversion of Vixen I prototype as [[light bomber]].
;Type 91 Vixen III
:Version with larger wings for improved high-altitude performance. One built.
;Type 105 Vixen IV
:Further modification of Vixen II with 650&nbsp;hp (490&nbsp;kW) [[Rolls-Royce Condor]] III engine for use as [[night fighter]].
;Type 106 Vixen III
:Modification of Type 91 for entry in the 1925 King's Cup Air Race.
[[File:Vickers Vivid on ground.jpg|thumb|right|300px|<center>Vickers Vivid</center>]]
;Type 116 Vixen V
:General purpose aircraft for [[Chile]], powered by high compression Lion V engine. 18 built.
;Type 124 Vixen VI
:Modification of Vixen IV as general purpose aircraft. Powered by Rolls-Royce Condor IIIA engine. No production.
;Type 130 Vivid
:Modification of Vixen III prototype with metal wings and powered by Lion VA engine.
;Type 142 Vivid
:Type 130 prototype re-engined with Lion XI. No production.
;Type 148 Vixen III
:Further single-seat modification of Type 106 Vixen III for air racing. Finished second in 1926 King's Cup race and third in 1927.<ref name="Jacksonv3"/>

==Operators==
;{{CHI}}
*[[Chilean Air Force]]

==Specifications (Vixen V)==
{{Aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop
|ref= ''Vickers Aircraft Since 1908'' <ref name="Andrews Vickers"/> 
|crew=Two
|capacity=
|length main= 29 ft 0 in
|length alt= 8.84 m
|span main= 44 ft 0 in
|span alt= 13.41 m
|height main= 12 ft 0 in
|height alt= 3.66 m
|area main= 590 ft²
|area alt= 54.8 m²
|airfoil=
|empty weight main= 3,320 lb
|empty weight alt= 1,509 kg
|loaded weight main= 5,080 lb
|loaded weight alt= 2,309 kg
|useful load main=  
|useful load alt=  
|max takeoff weight main=  
|max takeoff weight alt=  
|more general=
|engine (prop)=[[Napier Lion]] V
|type of prop= 12-cylinder water-cooled [[W engine|W-block]]  
|number of props=1
|power main= 500 hp
|power alt= 373 kW
|power original=
|max speed main= 116 kn
|max speed alt= 134 mph, 215 km/h
|max speed more= at sea level
|cruise speed main=  
|cruise speed alt=  
|never exceed speed main=  
|never exceed speed alt=  
|stall speed main=  
|stall speed alt=  
|range main= 664 nm
|range alt= 764 mi, 1,230 km
|ceiling main= 20,000 ft
|ceiling alt= 9,100 m
|ceiling more=(absolute)
|climb rate main= 1,250 ft/min
|climb rate alt= 116 m/s
|loading main= 8.61 lb/ft²
|loading alt= 52.1 kg/m²
|thrust/weight=<!-- a unitless ratio -->
|power/mass main= 0.098 hp/lb
|power/mass alt= 0.16 kW/kg
|more performance=*'''Climb to 15,000 ft (4,600 m):''' 25 min
|armament=
|avionics=
}}

==See also==
{{Aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=
*[[Vickers Venture]]
*[[Vickers Valparaiso]]
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
|see also=<!-- other relevant information -->
}}

==References==
{{commons category|Vickers Vixen}}
{{Reflist}}

{{Vickers aircraft}}

[[Category:British fighter aircraft 1920–1929]]
[[Category:British military reconnaissance aircraft 1920–1929]]
[[Category:Vickers aircraft|Vixen]]