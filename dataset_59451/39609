{{About|financial dividends|dividends in arithmetic|Division (mathematics)}}
{{Use mdy dates|date=August 2014}}
{{Accounting}}
A '''dividend''' is a payment made by a [[corporation]] to its [[shareholder]]s, usually as a distribution of [[Profit (accounting)|profit]]s.<ref>{{cite book
  | last = O'Sullivan
  | first = Arthur
  | authorlink = Arthur O'Sullivan (economist)
  | first2 = Steven M. | last2 = Sheffrin
  | title = Economics: Principles in Action
  | publisher = Pearson Prentice Hall
  | year = 2003
  | location = Upper Saddle River, New Jersey 07458
  | page = 273
  | doi =
  | id =
  | isbn = 0-13-063085-3}}</ref> When a corporation earns a profit or surplus, the corporation is able to re-invest the profit in the business (called [[retained earnings]]) and pay a proportion of the profit as a dividend to shareholders. Distribution to shareholders may be in cash (usually a deposit into a bank account) or, if the corporation has a [[dividend reinvestment plan]], the amount can be paid by the issue of further shares or [[share repurchase]].<ref name = "Simkovic Disclose">Michael Simkovic, [http://ssrn.com/abstract=1117303 "The Effect of Enhanced Disclosure on Open Market Stock Repurchases"], 6 Berkeley Bus. L.J. 96 (2009).</ref><ref name = "Simkovic Owner">Amedeo De Cesari, Susanne Espenlaub, Arif Khurshed and Michael Simkovic, [http://ssrn.com/abstract=1884171 "The Effects of Ownership and Stock Liquidity on the Timing of Repurchase Transactions"], 2010</ref>

A dividend is allocated as a fixed amount per share, with shareholders receiving a dividend in proportion to their shareholding.  For the [[joint-stock company]], paying dividends is not an [[expense]]; rather, it is the division of after tax profits among shareholders. Retained earnings (profits that have not been distributed as dividends) are shown in the shareholders' equity section on the company's balance sheet &ndash; the same as its issued share capital. [[Public company|Public companies]] usually pay dividends on a fixed schedule, but may declare a dividend at any time, sometimes called a [[special dividend]] to distinguish it from the fixed schedule dividends. [[Cooperative]]s, on the other hand, allocate dividends according to members' activity, so their dividends are often considered to be a pre-tax expense.

The word "dividend" comes from the [[Latin language|Latin]] word "''dividendum''" ("thing to be divided").<ref>{{cite web | title = dividend | work = Online Etymology Dictionary | publisher = Douglas Harper | year = 2001 | url = http://www.etymonline.com/index.php?search=dividend&searchmode=none | accessdate = November 9, 2006 }}</ref>

==Forms of payment==

'''Cash dividends''' are the most common form of payment and are paid out in currency, usually via [[electronic funds transfer]] or a printed paper [[cheque|check]]. Such dividends are a form of investment income and are usually taxable to the recipient in the year they are paid. This is the most common method of sharing corporate profits with the shareholders of the company. For each share owned, a declared amount of money is distributed. Thus, if a person owns 100 shares and the cash dividend is 50 cents per share, the holder of the stock will be paid $50.  Dividends paid are not classified as an [[expense]], but rather a deduction of [[retained earnings]].  Dividends paid does not show up on an [[income statement]] but does appear on the [[balance sheet]].

'''Stock or scrip dividends''' are those paid out in the form of additional stock shares of the issuing corporation, or another corporation (such as its subsidiary corporation).  They are usually issued in proportion to shares owned (for example, for every 100 shares of stock owned, a 5% stock dividend will yield 5 extra shares).

Nothing tangible will be gained if the stock is [[stock split|split]] because the total number of shares increases, lowering the price of each share, without changing the [[market capitalization]], or total value, of the shares held. (See also [[Stock dilution]].)

'''Stock dividend distributions''' are issues of new shares made to limited partners by a partnership in the form of additional shares.  Nothing is split, these shares increase the market capitalization and total value of the company at the same time reducing the original cost basis per share.

Stock dividends are not includable in the gross income of the shareholder for US income tax purposes. Because the shares are issued for proceeds equal to the pre-existing market price of the shares; there is no negative dilution in the amount recoverable.<ref>[http://www.sec.gov/Archives/edgar/data/54502/000095012901500750/0000950129-01-500750.txt]<!-- When we (Kinder Morgan Management, LLC) receive additional i-units from Kinder Morgan Energy Partners, L.P., we will issue and distribute an equal number of our shares to all of our shareholders. ~ The number of i-units and shares will remain equal.-->Public offering Kinder Morgan Management, LLC</ref><ref>[http://www.kindermorgan.com/investor/kmr_2001_annual_report_financials.pdf] <!--On January 17, 2002, we (Kinder Morgan Management, LLC) announced that our board of directors had declared a share distribution payable on February 14, 2002 to shareholders of record as of January 31, 2002, based on the $0.55 per common unit distribution declared by Kinder Morgan Energy Partners, L.P.  This distribution was paid in the form of additional shares or fractions thereof, as appropriate, based on the average market price of a share determined for a ten-trading day period ending on the trading day immediately prior to the ex-dividend date for our shares.-->U.S. Securities and Exchange Commission</ref><ref>[http://sec.edgar-online.com/kinder-morgan-management-llc/s-1a-securities-registration-statement/2001/04/30/section8.aspx] EDGAR Online, Inc.</ref>

'''Property dividends''' or dividends '''''in specie''''' ([[Latin language|Latin]] for "[[in kind]]") are those paid out in the form of assets from the issuing corporation or another corporation, such as a subsidiary corporation. They are relatively rare and most frequently are securities of other companies owned by the issuer, however they can take other forms, such as products and services.

'''Interim dividends''' are dividend payments made before a company's Annual General Meeting (AGM) and final financial statements. This declared dividend usually accompanies the company's interim financial statements.

'''Other dividends''' can be used in [[structured finance]]. Financial assets with a known market value can be distributed as dividends; warrants are sometimes distributed in this way. For large companies with subsidiaries, dividends can take the form of shares in a subsidiary company. A common technique for "spinning off" a company from its parent is to distribute shares in the new company to the old company's shareholders. The new shares can then be traded independently.

==Reliability of dividends==

Two metrics are commonly used to examine a firm's [[dividend policy]].

''Payout ratio'' is calculated by dividing the company's dividend by the [[earnings per share]].  A payout ratio greater than 1 means the company is paying out more in dividends for the year than it earned.

''[[Dividend cover]]'' is calculated by dividing the company's [[Cash flow statement|cash flow from operations]] by the dividend.  This ratio is apparently popular with analysts of [[income trust]]s in Canada.{{Citation needed|date=November 2008}}
Dividends are payments made by a corporation to its shareholder members. It is the portion of corporate profits paid out to stockholders.

==Dividend dates==
A dividend that is declared must be approved by a company's [[board of directors]] before it is paid. For [[public company|public companies]], four dates are relevant regarding dividends:<ref>[http://www.sec.gov/answers/dividen.htm Securities and Exchange Commission site]</ref>

'''Declaration date''' — the day the board of directors announces its intention to pay a dividend. On that day, a [[Liability (financial accounting)|liability]] is created and the company records that liability on its books; it now owes the money to the stockholders.

'''In-dividend date''' — the last day, which is one trading day before the ''ex-dividend date'', where the stock is said to be ''cum dividend'' ('with ['''in'''cluding] dividend'). In other words, existing holders of the stock and anyone who buys it on this day will receive the dividend, whereas any holders selling the stock lose their right to the dividend.  After this date the stock becomes ''ex dividend''.

'''[[Ex-dividend date]]''' — the day on which shares bought and sold no longer come attached with the right to be paid the most recently declared dividend. In the United States, it is typically 2 trading days before the ''record date''. This is an important date for any company that has many stockholders, including those that trade on exchanges, to enable reconciliation of who is entitled to be paid the dividend. Existing holders of the stock will receive the dividend even if they sell the stock on or after that date, whereas anyone who bought the stock will not receive the dividend. It is relatively common for a stock's price to decrease on the ex-dividend date by an amount roughly equal to the dividend paid. This reflects the decrease in the company's assets resulting from the declaration of the dividend.

'''[[Book closure|Book closure date]]''' —when a company announces a dividend, it will also announce a date on which the company will ideally temporarily close its books for fresh transfers of stock, which is also usually the record date.

'''Record date''' — [[Stockholder of Record|shareholders registered]] in the company's record as of the record date will be paid the dividend.  Shareholders who are not registered as of this date will not receive the dividend. Registration in most countries is essentially automatic for shares purchased before the ex-dividend date.

'''Payment date''' — the day on which the dividend cheque will actually be mailed to shareholders or credited to their bank account.

==Dividend-reinvestment==

Some companies have [[dividend reinvestment plan]]s, or DRIPs, not to be confused with scrips. DRIPs allow shareholders to use dividends to systematically buy small amounts of stock, usually with no commission and sometimes at a slight discount.  In some cases, the shareholder might not need to pay taxes on these re-invested dividends, but in most cases they do.

==Dividend taxation==
{{main|Dividend tax}}
Most countries impose a [[corporate tax]] on the profits made by a company. A dividend paid by a company is not an expense of the company, but is income of the shareholder. The tax treatment of this dividend income varies considerably between countries:

===United States and Canada===
The United States and Canada impose a lower tax rate on dividend income than ordinary income, on the basis that company profits had already been taxed as [[corporate tax]].

===Australia and New Zealand===
Australia and New Zealand have a [[dividend imputation]] system, wherein companies can attach [[franking credit]]s or [[Dividend imputation|imputation credit]]s to dividends. These franking credits represent the tax paid by the company upon its pre-tax profits. One dollar of company tax paid generates one franking credit. Companies can attach any proportion of franking up to a maximum amount that is calculated from the prevailing company tax rate: for each dollar of dividend paid, the maximum level of franking is the company tax rate divided by (1 - company tax rate). At the current 30% rate, this works out at 0.30 of a credit per 70 cents of dividend, or 42.857 cents per dollar of dividend. The shareholders who are able to use them, apply these credits against their income tax bills at a rate of a dollar per credit, thereby effectively eliminating the [[double taxation]] of company profits.

===United Kingdom===
Dividends from UK companies are paid out of profits after corporation tax (Corporation tax is at 20% but decreases to 19% from 1 April 2017 - split periods have pro-rata applied). Dividend income is taxable on UK residents at the rate of 7.5% for basic rate payers, 32.5% for higher rate tax payers and 38.1% for additional rate payers. The income tax on dividend receipts is collected via personal tax returns. 
The first £5,000 of dividend income is not taxed, however dividend income above that amount is subject to the rate that would have applied if the £5,000 exemption had not been given.
UK limited companies do not pay tax on dividends received from their investments or from their subsidiaries. This is classed as "franked investment income".

===India===
In India, companies declaring or distributing dividend, are required to pay a Corporate Dividend Tax in addition to the tax levied on their income. The dividend received by the shareholders is then exempt in their hands.
However, dividend income over and above Rs. 1000,000 shall attract 10 per cent dividend tax in the hands of the shareholder with effect from April 2016.

==Effect on stock price==
After a stock goes ex-dividend (i.e. when a dividend has just been paid, so there is no anticipation of another imminent dividend payment), the stock price should drop.

To calculate the amount of the drop, the traditional method is to view the financial effects of the dividend from the perspective of the company. Since the company has paid say £x in dividends per share out of its cash account on the left hand side of the balance sheet, the equity account on the right side should decrease an equivalent amount. This means that a £x dividend should result in a £x drop in the share price.

A more accurate method of calculating this price is to look at the share price and dividend from the after-tax perspective of a share holder. The after-tax drop in the share price (or capital gain/loss) should be equivalent to the after-tax dividend. For example, if the tax of capital gains Tcg is 35%, and the tax on dividends Td is 15%, then a £1 dividend is equivalent to £0.85 of after tax money. To get the same financial benefit from a capital loss, the after tax capital loss value should equal £0.85. The pre-tax capital loss would be £0.85/(1-Tcg) = £0.85/(1-35%) = £0.85/65% = £1.30. In this case, a dividend of £1 has led to a larger drop in the share price of £1.30, because the tax rate on capital losses is higher than the dividend tax rate.

Finally, security analysis that does not take dividends into account may mute the decline in share price, for example in the case of a [[Price–earnings ratio]] target that does not back out cash; or amplify the decline, for example in the case of [[Trend following]].

===Criticism===
Some believe that company profits are best re-invested in the company: research and development, capital investment, expansion, etc. Proponents of this view (and thus critics of dividends per se) suggest that an eagerness to return profits to shareholders may indicate the management having run out of good ideas for the future of the company. Some studies, however, have demonstrated that companies that pay dividends have higher earnings growth, suggesting that dividend payments may be evidence of confidence in earnings growth and sufficient profitability to fund future expansion.<ref>{{cite news|ssrn=390143|title=Surprise! Higher Dividends equal Higher Earnings Growth|author=Robert D. Arnott and Clifford S. Asness|work=Financial Analysts Journal|date=January–February 2003|accessdate=January 4, 2011}}</ref>

Taxation of dividends is often used as justification for retaining earnings, or for performing a [[stock buyback]], in which the company buys back stock, thereby increasing the value of the stock left outstanding.

When dividends are paid, individual shareholders in many countries suffer from [[dividend tax|double taxation]] of those dividends:
# the company pays income tax to the government when it earns any income, and then
# when the dividend is paid, the individual shareholder pays income tax on the dividend payment.
In many countries, the tax rate on dividend income is lower than for other forms of income to compensate for tax paid at the corporate level.

<!--In contrast, corporate shareholders often do not pay tax on dividends because the tax regime is designed to tax corporate income (as opposed to individual income) only once. (NOTE: This is confusing and/or false information and needs to be cited if it remains. Does "corporate shareholders" in this context mean corporate managers who are also shareholders or majority shareholders/owners or just simple, everyday shareholders?) -->
A capital gain should not be confused with a dividend. Generally, a capital gain occurs where a capital asset is sold for an amount greater than the amount of its cost at the time the investment was purchased. A dividend is a parsing out a share of the profits, and is taxed at the dividend tax rate. If there is an increase of value of stock, and a shareholder chooses to sell the stock, the shareholder will pay a tax on capital gains (often taxed at a lower rate than [[ordinary income]]). If a holder of the stock chooses to not participate in the buyback, the price of the holder's shares could rise (as well as it could fall), but the tax on these gains is delayed until the sale of the shares.

Certain types of specialized investment companies (such as a [[Real estate investment trust|REIT]] in the U.S.) allow the shareholder to partially or fully avoid double taxation of dividends.

Shareholders in companies that pay little or no cash dividends can reap the benefit of the company's profits when they sell their shareholding, or when a company is wound down and all assets [[liquidated]] and distributed amongst shareholders.  This, in effect, delegates the dividend policy from the board to the individual shareholder.  <!-- the winding down distribution applies to some other types of corporations --> Payment of a dividend can increase the borrowing requirement, or [[Leverage (finance)|leverage]], of a company. <!-- this also applies to other types of corporations -->

==Other corporate entities==

===Cooperatives===
[[Cooperative]] businesses may retain their earnings, or distribute part or all of them as dividends to their members.  They distribute their dividends in proportion to their members' activity, instead of the value of members' shareholding.  Therefore, co-op dividends are often treated as pre-tax [[Operating expense|expenses]]. In other words, local tax or accounting rules may treat a dividend as a form of customer rebate or a staff bonus to be deducted from turnover before profit ([[tax profit]] or [[operating profit]]) is calculated.

[[Consumers' cooperative]]s allocate dividends according to their members' trade with the co-op.  For example, a [[credit union]] will pay a dividend to represent [[interest]] on a saver's deposit.  A retail co-op store chain may return a percentage of a member's purchases from the co-op, in the form of cash, store credit, or [[Equity (finance)|equity]].  This type of dividend is sometimes known as a patronage dividend or '''patronage refund''', as well as being informally named ''divi'' or ''divvy''.<ref>{{cite web|url=http://sec.edgar-online.com/2001/03/22/0000002024-01-000003/Section2.asp|title=Annual Report, Section 1, Business, 10-K405 SEC Filing|author=Ace Hardware|date=March 22, 2001|authorlink=Ace Hardware}}</ref><ref>{{Cite news
|url=http://news.bbc.co.uk/1/hi/business/6247926.stm
|title=Co-op pays out £19.6m in 'divi'
|publisher=[[BBC News]] via [[bbc.co.uk]]
|date=June 28, 2007
|accessdate=May 15, 2008
}}</ref><ref>{{Cite web
|url=http://www.historycooperative.org/proceedings/asslh/balnave.html
|title=The History Cooperative – Conference Proceedings - ASSLH - Rochdale consumer co-operatives and Australian labour history
|author=Nikola Balnave and Greg Patmore
}}</ref>

Producer cooperatives, such as [[worker cooperative]]s, allocate dividends according to their members' contribution, such as the hours they worked or their salary.<ref>{{cite news
|url=https://www.theguardian.com/business/2007/mar/30/smes.technology
|accessdate=June 9, 2009
|date=March 3, 2007
|title=Cooperatives pay big dividends
|last=Norris
|first=Sue
|work=The Guardian
}}</ref>

===Trusts{{anchor|Dividends from trusts}}===
In [[real estate investment trust]]s and [[royalty trust]]s, the distributions paid often will be consistently greater than the company earnings. This can be sustainable because the accounting earnings do not recognize any increasing value of real estate holdings and resource reserves. If there is no economic increase in the value of the company's assets then the excess distribution (or dividend) will be a [[return of capital]] and the [[book value]] of the company will have shrunk by an equal amount.  This may result in [[capital gain]]s which may be taxed differently from dividends representing distribution of earnings.

===Mutuals===
The distribution of profits by other forms of [[mutual organization]] also varies from that of [[joint-stock company|joint-stock companies]], though may not take the form of a dividend.

In the case of [[mutual insurance]], for example, in the United States, a distribution of profits to holders of [[Participating policy|participating life policies]] is called a ''dividend''.
These profits are generated by the investment returns of the insurer's general account, in which premiums are invested and from which claims are paid.
<ref>{{cite web|quote="In short, the portion of the premium determined not to have been necessary to provide coverage and benefits, to meet expenses, and to maintain the company's financial position, is returned to policyowners in the form of dividends."|url=http://www.newyorklife.com/cda/0,3254,10542,00.html|title=What Are Dividends?|publisher=[[New York Life]]|accessdate=April 29, 2008}}</ref>
The participating dividend may be used to decrease premiums, or to increase the cash value of the policy.
<ref>{{cite book|pages=591|url=https://books.google.com/books?id=F1hk6UFlsUsC|isbn=0-471-22092-2|oclc=    52323583|editor=Fabozzi, Frank J.|year=2002|title=Handbook of Financial Instruments|publisher=Wiley|chapter=24, Investment-Oriented Life Insurance|author=Hoboken, NJ}}</ref>
Some life policies pay nonparticipating dividends.
As a contrasting example, in the United Kingdom, the surrender value of a [[with-profits policy]] is increased by a ''bonus'', which also serves the purpose of distributing profits.
[[Life insurance]] dividends and bonuses, while typical of mutual insurance, are also paid by some [[joint stock insurer]]s.

Insurance dividend payments are not restricted to life policies.  For example, general insurer [[State Farm Insurance|State Farm]] Mutual Automobile Insurance Company can distribute dividends to its vehicle insurance policyholders.<ref>{{cite news|url=http://www.statefarm.com/about/media/media_releases/auto_dividends.asp|title=State Farm Announces $1.25 Billion Mutual Auto Policyholder Dividend|publisher=[[State Farm]]|date=March 1, 2007}}</ref>

==See also==
{{colbegin|colwidth=30em}}
*[[Dividend units]]
*[[Dividend yield]]
*[[Liquidating dividend]]
*[[Qualified dividend]]
*[[List of companies paying scrip dividends]]
*[[Capital structure substitution theory#Dividend policy|CSS dividend policy]]
{{colend}}

==References==
{{reflist|30em}}

==External links==
{{wiktionary|dividend}}
* [http://www.sec.gov/answers/dividen.htm Ex-Dividend Dates: When Are You Entitled to Stock and Cash Dividends] – U.S. Securities and Exchange Commission
*[http://www.dividendgrowthinvestor.com/2008/11/why-should-companies-pay-out-dividends.html Why Should Companies Pay Dividends?]
* [http://www.studyfinance.com/lessons/dividends/index.mv Dividend Policy] from studyfinance.com at the [[University of Arizona]]
* [https://web.archive.org/web/20071128090709/http://tscpa.com/Journal/articles/dividend_tax_cut_traps.pdf The new U.S. dividend tax cut traps] from Tennessee CPA Journal, Nov. 2004
{{Stock market}}

{{Authority control}}

[[Category:Dividends| ]]
[[Category:Shareholders]]