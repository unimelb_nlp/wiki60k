{{Infobox journal
| title = American Journal of Health-System Pharmacy
| cover = 
| former_name =
| abbreviation = Am. J. Health-Syst. Pharm.
| discipline = [[Pharmacy practice]]
| editor = Daniel J. Cobaugh
| publisher = [[American Society of Health-System Pharmacists]]
| country = [[United States]]
| history = 1965-present
| frequency = Biweekly
| openaccess =
| license =
| impact = 1.882
| impact-year = 2014
| ISSN = 1079-2082
| eISSN = 1535-2900
| CODEN = AHSPEK
| JSTOR =
| LCCN =
| OCLC = 41233599
| website = http://www.ajhp.org/
| link1 = http://www.ajhp.org/content/current
| link1-name = Online access
| link2 = http://www.ajhp.org/content
| link2-name = Online archive
}}
The '''''American Journal of Health-System Pharmacy''''' is a biweekly [[peer-reviewed]] [[medical journal]] covering all aspects of [[drug therapy]] and [[pharmacy practice]] specific to hospitals. It was established in 1965 and is published by the [[American Society of Health-System Pharmacists]]. The [[editor-in-chief]] is Daniel J. Cobaugh.

==Abstracting and indexing==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
*[[Chemical Abstracts Service]]<ref name=CASSI>{{cite web|url=http://cassi.cas.org/search.jsp |archive-url=http://wayback.archive-it.org/all/20100211181038/http://cassi.cas.org/search.jsp |dead-url=yes |archive-date=2010-02-11 |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2016-04-18 }}</ref>
*[[CINAHL]]<ref name=CINAHL>{{cite web |url=https://www.ebscohost.com/nursing/products/cinahl-databases/cinahl-complete |title=CINAHL Complete Database Coverage List |publisher=[[EBSCO Information Services]] |work=[[CINAHL]] |accessdate=2016-04-18}}</ref>
*[[Current Contents]]/Clinical Medicine<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2016-04-18}}</ref>
*Current Contents/Life Sciences<ref name=ISI/>
*[[Index Medicus]]/[[MEDLINE]]/[[PubMed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/9503023 |title=American Journal of Health-System Pharmacy |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=2016-04-18}}</ref>
*[[Science Citation Index]]<ref name=ISI/>
*[[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2016-04-18}}</ref>
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.882.<ref name=WoS>{{cite book |year=2015 |chapter=American Journal of Health-System Pharmacy |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{Reflist|30em}}

==External links==
*{{Official website|http://www.ajhp.org/}}

[[Category:Pharmacy in the United States]]
[[Category:Biweekly journals]]
[[Category:Pharmacology journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1965]]


{{Pharmacology-journal-stub}}