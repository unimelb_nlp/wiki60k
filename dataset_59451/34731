{{EngvarB|date=April 2014}}
{{Use dmy dates|date=April 2014}}
{{Good article}}
{{Infobox building
|name               = 108 St Georges Terrace
|image              = BankWest Tower.jpg
|image_size         = 200px
| address             = 108 [[St Georges Terrace]]<ref>{{cite web | url=http://www.108sgt.com.au/the-building.htm | title=The Building | publisher=Brookfield | work=www.108sgt.com.au | accessdate=7 March 2012}}</ref>
|location           = [[Perth, Western Australia]]
|coordinates        = {{coord|31|57|16|S|115|51|25.5|E|region:AU|display=inline,title}}
|start_date         = 1981
|completion_date    = 1988
|building_type      = Office
|antenna_spire      = {{convert|247|m|ft}}
|roof               = {{convert|214|m|ft}}
|floor_count        = 50 (plus 2 below ground)
|elevator_count     = 15
|cost               = [[Australian dollar|A$]]120 million
|floor_area         = {{convert|39500|m2|ft2}}<ref name="Full"/>
|architect          = Cameron Chisolm Nicol
|main_contractor    = [[Multiplex (company)|Multiplex]]
|developer          = Austmark International and [[R&I Bank]]
|owner              = Brookfield Mutliplex (50%), Stockland (50%)
}}

'''108 St Georges Terrace''' or '''South32 Tower''' (formerly known as '''Bankwest Tower''', the '''Bond Tower''' and the '''R&I Tower''') is a 50-storey [[office tower]] in [[Perth, Western Australia]]. Completed in 1988, the building measures {{convert|214|m|ft}} to its roof and {{convert|247|m|ft}} to the tip of its [[Antenna (radio)|communications antenna]]. It was the tallest building in Perth from its completion in 1988 until 1992 when it was overtaken in height by [[Central Park (skyscraper)|Central Park]]. As of 2012, it remains the third-tallest building in the city. The concrete tower has a distinctive profile, with a triangular plan.

The site occupied by the tower was home to the [[Palace Hotel, Perth|Palace Hotel]], and organised opposition was formed to try to save that building from demolition to make way for an office tower. The site was subsequently acquired by businessman [[Alan Bond]] and the tower was approved and constructed in a plan that would retain much of the Palace Hotel. The tower then remained the headquarters of Bond's companies until their collapse. The tower has also been the headquarters of Western Australia's [[state bank]], [[Bankwest]] (formerly known as the R&I Bank), between its completion and 2012.

==History==

===Pre-1978===
{{Main article|Palace Hotel, Perth}}
The tower's prime location at the corner of [[William Street, Perth|William Street]] and [[St Georges Terrace, Perth|St Georges Terrace]] was the site of the first [[liquor license|licensed premises]] in Perth from the 1830s.<ref>{{cite news |first=Roy |last=Gibson |title=Walks reveal city's history |work=[[The West Australian]] |page=19 |date=21 April 2008 }}</ref>
The then-opulent Palace Hotel opened on this corner in 1897 during the days of the Western Australian [[gold rush]], and developed a "colourful" history, hosting numerous celebrities of the time.<ref name="Palace">{{cite news |first=Frank |last=Dunn |title=Palace Hotel |work=[[The Sunday Times (Western Australia)|Sunday Times]] |page=4 |date=4 July 2004}}</ref>
The plot was purchased by the [[Commonwealth Bank|Commonwealth Banking Corporation]], which announced in 1972 that it planned to redevelop the site as high-rise offices.<ref name="Palace"/>
During the 1950s and 1960s, many of Perth's older buildings had been demolished to make way for modern developments, which led to wide criticism of the [[City of Perth|Perth City Council]] for approving such redevelopments.<ref>{{cite news |author=Staff writer |title=Race for the sky will scale greater heights |work=[[The West Australian]] |page=11 |date=21 September 1987 |quote=The PCC is criticised from many quarters over the loss of older buildings to new developments. }}</ref>
In this atmosphere, wide public protest resulted and a lobby group of concerned citizens calling themselves "The Palace Guards" worked to save the historic building.<ref name="BuildingChallenge">{{cite book |last=Hocking |first=Ian |title=Perth—the building challenge |date=c. 1987 |publisher=The Master Builders' Association of Western Australia |location=Perth |isbn=0-9598935-3-9 |pages=32–33 }}</ref>
This outcry led to condemnation of the plans and heritage listing of the property by the National Trust, and forced the Commonwealth Bank to ask the Federal Government to take the property off its hands.<ref name="Palace"/>
The property was subsequently purchased from the Commonwealth Bank by businessman Alan Bond in 1978 along with the adjacent Terrace Arcade.

===Bond Corporation and construction: 1978–1988===

In 1980, Bond unveiled new plans for the redevelopment of the site. These plans made some effort at preservation of the Palace Hotel, by retaining the facade and main foyer area of the building.<ref name="PrettyFacade">{{cite news |first=Robert |last=Wainwright |title=Just a pretty facade |work=[[The West Australian]] |page=3 |date=4 June 1986 }}</ref>
However, the construction of the modern office tower at the site's north-eastern corner required demolition of Terrace Arcade, the eastern accommodation wing and the hotel's renowned dining room. The rationale given for the works required was that extensive renovations to the Palace Hotel in 1915 and the 1930s had weakened its structure significantly, as well as problems controlling [[white ant]]s in the structure.<ref name="PrettyFacade"/>
The development exceeded the acceptable [[Floor Area Ratio|plot ratio]] in the town planning scheme, however the Perth City Council pre-approved the plans nonetheless, on the condition that Bond Corporation kept the Palace Hotel operating as a hotel.<ref name="PrettyFacade"/>

Demolition of parts of the site commenced in August 1981, and by August 1983 construction had completed on the tower's foundation and three levels of underground parking.<ref name="Reviver">{{cite news |first=Paul |last=McGeough |title=Reviver for Palace Project |work=[[The West Australian]] |page=1 |date=8 September 1984 }}</ref>
However, there were continuing doubts about the viability of the building,<ref name="Success">{{cite news |author=Staff writer |title=At long last, it's a big success story |work=[[Western Mail (Western Australia)|The Western Mail]] |publisher=Western Mail Limited |page=56 |date=5–6 October 1985 }}</ref>
slowing the pace of the development. This changed on 7 September 1984 when [[R&I Bank]] Chairman David Fischer signed a joint venture agreement over the development with Austmark International, a subsidiary of [[Bond Corporation]].<ref name="Reviver"/> The bank, which was at the time wholly owned by the [[Government of Western Australia]], consulted then-[[Premier of Western Australia|Premier]] [[Brian Burke (Australian politician)|Brian Burke]] in deciding whether to invest in the project.<ref name="Reviver"/>

Following the investment in the project by the bank, the developers successfully sought modification of the original planning approval by the Perth City Council to allow the use of the Palace Hotel as a bank, rather than remaining as a public hotel.<ref name="PrettyFacade"/> With this permission, construction of the development by [[Multiplex (company)|Multiplex]]<ref name="Reviver"/> proceeded, and floor space was leased rapidly; by October 1985 only four floors of the building remained for lease and it was expected to be fully let before completion.<ref name="Success"/>
With construction finally proceeding once more, the Palace Hotel closed in June 1986.<ref name="PrettyFacade"/> In April 1987, while the tower was still under construction, five men were arrested after illegally entering the site and [[parachuting]] from the top of the tower in the early hours of the morning.<ref>{{cite news |title=Police get the drop on stunt jumpers |work=[[Sydney Morning Herald]] |page=11 |date=28 April 1987}}</ref>
Construction proceeded at the rate of about one floor every eight to ten days.<ref name="CII"/>

The construction of the tower took three years to complete, with the first occupants moving into the tower in July 1988.<ref name="Open">{{cite news |first=Rob |last=MacDonald |title=214m tower is all set to open for business |work=[[Daily News (Perth, Western Australia)|Daily News]] |page=14 |date=8 July 1988 }}</ref>
The tower was officially opened in October 1989. Construction of the building cost [[Australian dollar|A$]]120 million,<ref name="Open"/> up from the 1984 estimates of $100 million.<ref name="Reviver"/>
Upon its completion, the building was the 55th-tallest building in the world, the third-tallest in Australia and the tallest in Perth (overtaking [[St Martins Tower]]).<ref name="Success"/><ref name="CII">{{cite book |last=John |first=H. |title=Bank Tower reaches new heights |series=Construction Industry International |volume=13 |date=November 1987 |pages=35–37 }}</ref>
It was also the eighth-tallest concrete skyscraper in the world.<ref name="CII"/><ref name="Open"/>
However, at this time there were already plans to build a taller building on the site of the former David Jones department store between [[Hay Street, Perth|Hay Street]] and St Georges Terrace,<ref name="Open"/> which became [[Central Park (skyscraper)|Central Park]].

===After completion: 1988–2012===

The top three floors of the building were occupied by Alan Bond's private investment company Dallhold Investments, which owned a majority stake in Bond Corporation.<ref name="Success"/><ref>{{cite news |first=Colleen |last=Ryan |title=How Bond will probably get his nickel's worth in private |work=[[Sydney Morning Herald]] |page=25 |date=16 January 1990 |quote=Dallhold took pride of place in the new Bond headquarters in the R & I Tower in Perth, it had the top floors with the glass atrium in which was displayed a multi million dollar art collection. }}</ref>
In November 1987, Alan Bond bought the [[Vincent van Gogh]] painting ''[[Irises (painting)|Irises]]'' and unveiled it in a purpose-built secure art gallery on the 49th floor of the tower on 23 December 1988.<ref>{{cite news |title=Business Tycoon Says He Bought World's Most Valuable Painting |agency=Associated Press |date=22 December 1988}}</ref>
Amidst a worsening financial situation for the Bond companies, Bond Corporation sold its half share in the building to R&I Bank for $108 million, making R&I Bank its outright owner.<ref>{{cite news |first=Bruce |last=Jacques |title=Court decision threatens Bond deal |work=[[Financial Times]] |page=22 |date=20 December 1989 |quote=Mr Bond also told yesterday's Bond Corporation meeting the company had sold its half share in the 50-storey R&I Tower building in Perth for ADollars 108m. ... Mr Bond said the group had experienced a difficult period where conflicts had been sensationalised by the media. He said the group's recently announced Australian record loss of more than $800m had to be seen in perspective because it included large provisions and write-offs. }}</ref>
In 1991 amid the collapse of the Bond empire, Bond shifted his offices out of the tower, leaving the top three floors vacant.<ref>{{cite news |first=Tony |last=Kaye |title=Bond Keeps Low Profile As Dallhold's Creditors Close In |work=[[The Age|Sunday Age]] |page=3 |date=5 May 1991 |quote=The Bond family company, Dallhold Investments, has also had its fair share of problems of late. In the past few weeks, Dallhold has moved offices from its palatial 50th floor accommodation in the R&I Bank Tower (formerly Bond Tower) in Perth, to a ground-floor spot further up the road. }}</ref>
These floors then remained vacant for almost a decade.<ref name="Full">{{cite news |first=Virginia |last=Egerton-Warburton |title=Former Bond Tower Hangs Out 'full' Sign |work=[[The West Australian]] |page=64 |date=15 March 2000 }}</ref>

From when it opened, the tower was referred to as both the Bond Tower<ref>See e.g. {{cite news |first=Victoria |last=Laurie |title=City of lights to again reflect its civic pride |work=[[The Australian]] |page=15 |date=21 August 2000 }}</ref> and the R&I Tower, owing to the presence of both logos on the building's exterior.<ref>See e.g. {{cite web |url=http://henrietta.liswa.wa.gov.au/search~S6?/Xperth+demolished&searchscope=6&SORT=DZ/Xperth+demolished&searchscope=6&SORT=DZ&extended=0&SUBKEY=perth%20demolished/1%2C49%2C49%2CB/frameset&FF=Xperth+demolished&searchscope=6&SORT=DZ&1%2C1%2C |title=Demolished multi-storey carpark on Stirling and James Streets Perth (picture) |accessdate=7 February 2009 |last=Woldendorp |first=Richard |year=c. 1990 |publisher=State Library of Western Australia }}</ref> The Bond Corporation logos were later removed from the tower,<ref>See e.g. {{cite web |url=http://henrietta.liswa.wa.gov.au/search~S6?/dAerial+photographs+--+1990-2000/daerial+photographs+1990+2000/-3%2C-1%2C0%2CB/frameset&FF=daerial+photographs+1990+2000&146%2C%2C164 |title=Aerial view of Perth CBD and bus station (picture) |accessdate=7 February 2009 |last=Woldendorp |first=Richard |year=c. 1992 |publisher=State Library of Western Australia }}</ref> and by the end of 1994, the signage on the building was updated to match the new name and logo of its owner and head tenant, [[Bankwest]].<ref>See e.g. {{cite web |url=http://henrietta.liswa.wa.gov.au/search~S6?/dAerial+photographs+--+1990-2000/daerial+photographs+1990+2000/-3%2C-1%2C0%2CB/frameset&FF=daerial+photographs+1990+2000&134%2C%2C164 |title=Aerial view looking west over Perth central business district (picture) |accessdate=7 February 2009 |last=Woldendorp |first=Richard |date=November 1994 |publisher=State Library of Western Australia }}</ref>

Bankwest sold the building in November 1994 for $146 million to Jetcloud Pty Ltd, which was majority owned by the [[AMP Society]].<ref name="Full"/> However, Bankwest retained the head lease over the building, and in 2000 the Bank occupied around 20 floors in the building.<ref name="Full"/> In 2002, Valad purchased a half-share in the tower for $92.5 million, which it then sold to Stockland in early 2007. Meanwhile, [[Multiplex (company)|Multiplex]] acquired its half-stake in the tower from its merger with Ronin Property Group<ref>{{cite news |first=Cathy |last=Saunders |title=Multiplex owner puts tower on the market |work=[[The West Australian]] |page=78 |date=10 November 2007 }}</ref>
which was proposed in late 2004.<ref>{{cite web |url=http://www.brookfieldmultiplex.com/about/news/news/?NewsID=143 |title=Merger of Multiplex Group and Ronin Property Group |accessdate=7 November 2008 |publisher=Brookfield Multiplex |date=27 September 2004 }}</ref> Brookfield Multiplex put its half share in the building up for sale in early 2008,<ref>{{cite news |first=Cathy |last=Saunders |title=Multiplex puts tower stake on the market |work=[[The West Australian]] |page=49 |date=21 February 2008 }}</ref> however an offer for the tower by Luke Saraceni fell through and the half share in the tower was taken off the property market.<ref>{{cite news |first=Cathy |last=Saunders |title=Big towers off market as offers fall short |work=[[The West Australian]] |page=45 |date=8 August 2008 }}</ref>

Bankwest announced in 2006 that it would not renew its occupancy lease, instead moving to a new tower proposed for Raine Square.<ref name="MultiplexDeal">{{cite news |first = Rebecca | last = Keenan | title = Multiplex holds key to $134m BankWest deal | work = [[The West Australian]] | page = 46 | date = 28 December 2006}}</ref>
Due to delays with the Raine Square development, the Bank was forced in November 2009 to seek a 5-year renewal of its lease in the Bankwest Tower, the remainder of which will be taken on by Raine Square property developer Saracen Properties.<ref>{{cite news |first=Cathy |last=Saunders |title=Raine Square 'to keep BankWest' |work=[[The West Australian]] |page=58 |date=15 October 2008 }}</ref> Saracen Properties announced in May 2009 that they would not seek to renew their lease, and would vacate at the termination of their lease in November 2009. It was also revealed that the 50th floor office space once occupied by Alan Bond was still in the same condition as when he left in 1991, with Bond's former chair, desk and boardroom table available as part of the lease.<ref>{{cite news|url=http://www.news.com.au/business/story/0,27753,25456614-462,00.html |title=Bondy's old office up for lease |accessdate=12 May 2009 |publisher=NEWS.com.au |date=10 May 2009 |archiveurl=https://web.archive.org/web/20090514022518/http://www.news.com.au/business/story/0,27753,25456614-462,00.html |archivedate=14 May 2009 |deadurl=no |df=dmy }}</ref>

It was announced in January 2015 that base metal and mining company [[South32]] would sign 10½-year lease for {{convert|8300|m2|ft2}} of office space, starting from May 2015. The company also took up the naming and rooftop signage rights for the tower.<ref>{{cite news|title=South32 to adorn city skyline |first=Peter |last=Klinger |url=https://au.news.yahoo.com/thewest/wa/a/25980667/south32-to-adorn-city-skyline/ |newspaper=[[The West Australian]] |date=13 January 2015 |accessdate=24 June 2015 |archiveurl=http://www.webcitation.org/6ZWZjCNBG?url=https%3A%2F%2Fau.news.yahoo.com%2Fthewest%2Fwa%2Fa%2F25980667%2Fsouth32-to-adorn-city-skyline%2F |archivedate=24 June 2015 |deadurl=no |df=dmy }}</ref><ref>{{cite news|title=South32 signs 10-year Perth lease |url=http://www.theaustralian.com.au/business/latest/south32-signs-10-year-perth-lease/story-e6frg90f-1227183146997 |newspaper=[[The Australian]] |date=13 January 2015 |accessdate=24 June 2015 |archiveurl=http://www.webcitation.org/6ZWZvmmjr?url=http%3A%2F%2Fwww.theaustralian.com.au%2Fbusiness%2Flatest%2Fsouth32-signs-10-year-perth-lease%2Fstory-e6frg90f-1227183146997%3Fnk%3Dea7ecb9cc4d5452fe3bae55b289cff21-1435147674 |archivedate=24 June 2015 |deadurl=no |df=dmy }}</ref>

==Design==
[[File:Bankwest tower.jpg|thumb|right|The south-western face of the tower, viewed from the intersection of St Georges Terrace and William Street]]
The tower was designed by architects Cameron Chisholm Nicol. The major parameter given to them in the planning of the building was that "all office areas should face the excellent views that exist of the [[Swan River (Western Australia)|Swan River]]".<ref name="Architect">{{cite book |author1=Chisholm, Cameron |author2=Architects, Nicol |title=R&I Tower |series=The Architect (Western Australia) |volume=29–4 |year=1989 |pages=16–17 }}</ref>
Working within this brief, they selected a [[right triangle|triangular]] cross-section of the tower because it lent "itself to open plan office layouts" by allowing natural light to reach most parts of the working floors.<ref name="CII"/> Also, the stepped front of the building maximised the number of [[corner office]]s on each floor. The eastern and northern sides of the building are slip-formed concrete shear walls,<ref name="Informit">{{cite web |url=http://search.informit.com.au/documentSummary;dn=648389541056009;res=IELENG |title=The R and I Bank Tower, Perth |accessdate=7 November 2008 | last1=Roberts | first1=A. | last2=Ryan | first2=D.C. |year=1987 |work=First National Structural Engineering Conference 1987: Preprints of Papers |publisher=Institution of Engineers, Australia |location=Canberra |pages=679–684 }}</ref> and have fewer windows. These sides house the services of the building, including the [[elevator|lift]] shafts and stairwells. Some difficulty was encountered in the design stage with how to execute the diagonal slopes forming the top of these walls alongside the top five floors of the building, since a standard concrete pour was not possible. Instead, reinforced concrete panels were prefabricated elsewhere and subsequently attached to the structure.<ref name="CII"/>

The floor plates of the tower are constructed from conventional reinforced concrete beams and slabs.<ref name="Informit"/> The building's 14 passenger lifts are divided into three zones: low-rise (floors G-17), mid-rise (18–29) and high-rise (30–51),<ref name="Open"/> thought to be the first building in Australia to use such a configuration.<ref name="Architect"/>

The foyer of the tower was originally planned to be an open-air forecourt between the building and the remains of the Palace Hotel, however extensive [[Aeroelasticity|aeroelastic]] wind testing of the structure by the Institute of Environmental Sciences at [[Murdoch University]] forced this area to be covered with a glass canopy.<ref name="CII"/> Mocks of the [[Curtain wall (architecture)|curtain wall]]s to be used in the tower were also tested to 1.5 times the [[Factor of safety|proof load]] to ensure they could withstand [[tropical cyclone|cyclone]]-force weather and [[earthquake|seismic]] shocks.<ref name="CII"/> The building is clad with green-tinted double-pane glass on windows, with the service core and structure of the building covered with aluminium sheeting skin coated with a light grey [[fluoropolymer]] paint.<ref name="CII"/><ref name="Architect"/>

Upon completion, the mass of the building above ground was {{convert|66000|t|lb|lk=on}}.<ref name="Open"/> The building rests upon 43 belled concrete and steel [[deep foundation|piles]], of average length {{convert|30|m|ft}}, which go "through 3 layers of swamp" to solid [[siltstone]] [[bedrock]].<ref name="CII"/><ref name="Open"/> These piles range in diameter from {{convert|2.5|m|ftin}} to {{convert|2.9|m|ftin}}.<ref name="CII"/> The [[basement]] of the building has a depth of {{convert|16|m|ft}}, and has a {{convert|0.5|m|ftin|adj=on}} thick [[Vertical damp proof barrier|diaphragm wall]] to prevent water ingress.<ref name="CII"/>

While the building was under construction, on 21 September 1987 the Perth City Council approved the addition of a {{convert|48|m|ft|adj=on}} spire to the top of the tower to house television and radio antennas, [[microwave antenna]]s, navigation lights and surveillance cameras. However, when asked about the surveillance cameras, the R&I Bank's development spokesman Terry Pilbeam denied any knowledge about what any cameras would be used for. The spire was also approved by the Department of Transport and Communications, which said the spire would cause no air traffic problems,<ref name="Spire">{{cite news |first=Robert |last=Wainwright |title=Bid to add spire to R&I Tower |work=[[The West Australian]] |page=3 |date=22 September 1987 }}</ref> and the spire was added to the building upon completion.

==Gallery==
<gallery>
Image:Bankwest Tower.jpg|The northern side of the tower as viewed from William Street
Image:Golden reflection at dusk.jpg|108 St Georges Terrace at sunset, alongside Central Park and the AMP Building
Image:BankWest Tower view.jpg|A typical view over the Swan River from an office in the tower
Image:BankWest Building.JPG|A view showing the north and east sides of the building, which contain the service cores
Image:Palace hotel from westpac.jpg|The Palace Hotel and the forecourt's distinctive spiked glass roof
</gallery>

{{Tallest buildings in Australia|taller=[[Telstra Corporate Centre]]|theight=218m|shorter=[[Aurora Tower]]|sheight=207m}}

==See also==
* [[Central Park (skyscraper)]]
* [[List of tallest buildings in Perth]]
* [[List of tallest buildings in Australia]]

==References==
{{Reflist|2}}

==External links==
{{Commons category|BankWest Tower}}
*[http://108stgeorgesterrace.com 108 St Georges Terrace Website]
*[http://www.emporis.com/en/wm/bu/?id=bankwesttower-perth-australia Emporis listing for the tower]
*[http://skyscraperpage.com/cities/?buildingID=283 SkyscraperPage for the tower]
*[http://henrietta.liswa.wa.gov.au/search~S6?/dR+%26+I+Bank+of+Western+Australia+Ltd./dr+and+i+bank+of+western+australia+ltd/-3%2C-1%2C0%2CB/exact&FF=dr+and+i+bank+tower+perth+w+a+photographs&1%2C2%2C State Library of Western Australia] – photographs of the building as proposed and under construction

{{Perth skyscrapers}}

[[Category:Office buildings completed in 1988]]
[[Category:Landmarks in Perth, Western Australia]]
[[Category:Skyscrapers in Perth, Western Australia]]
[[Category:Skyscrapers between 200 and 249 meters]]
[[Category:Office buildings in Perth, Western Australia]]
[[Category:Bank buildings in Australia]]
[[Category:St Georges Terrace]]
[[Category:William Street, Perth]]
[[Category:Skyscraper office buildings in Australia]]