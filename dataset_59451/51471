{{EngvarB|date=August 2016}}
{{Use dmy dates|date=August 2016}}
{{Infobox journal
| title = Historical Materialism
| cover =
| editor = Alex Anievas, Sebastian Budgen, Steve Edwards, Sara Farris, Juan Grigera, Ashok Kumar, Esther Leslie, Maia Pal, Paul Reynolds, Mary Robertson, Peter Thomas, Alberto Toscano
| discipline = [[Political science]]
| abbreviation =
| publisher = [[Brill Publishers]]
| country =
| frequency = Quarterly
| history = 1997–present
| openaccess =
| license =
| impact = 0.705
| impact-year = 2012
| website = http://www.historicalmaterialism.org/journal
| link1 = http://www.ingentaconnect.com/content/brill/hm
| link1-name = Online access
| link2 = http://www.brill.nl/hima
| link2-name = Journal page at publisher's website
| JSTOR =
| OCLC = 39026496
| LCCN = sn98018404
| CODEN =
| ISSN = 1465-4466
| eISSN = 1569-206X 
}}
'''''Historical Materialism''''' is a quarterly [[Peer review|peer-reviewed]] [[academic journal]] published by [[Brill Publishers]] of [[historical materialism]], the study of society, economics, and history using a [[Marxism|Marxist]] approach. The journal started as a project at the [[London School of Economics]] from 1995 to 1998. Currently it is affiliated at the SOAS the [[University of London]]. Starting from 2008 the journal organises an annual conference.<ref>[http://www.historicalmaterialism.org/conferences "Historical Materialism – Conferences"] Retrieved 15 May 2014.</ref> and a book series.

== Abstracting and indexing ==
The journal is abstracted in ''[[Political Science Abstracts]], [[Sociological Abstracts]], [[Current Contents]]/Social and Behavioral Sciences, [[Social Sciences Citation Index]], [[Arts and Humanities Citation Index]]'', and the ''[[Journal Citation Reports]]/Social Sciences Edition''.<ref name=Brill>{{cite web |url=http://www.brill.nl/hima |title=Historical Materialism – BRILL |format= |work= |accessdate=2 November 2009}}</ref><ref name="ISI">{{cite web | url = http://science.thomsonreuters.com/mjl/ | title = Master Journal List – Science – Thomson Reuters | format = | work = | accessdate = 2 November 2009}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 0.705.<ref name=WoS>{{cite book |year=2013 |chapter=Historical Materialism |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |work=Web of Science |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.historicalmaterialism.org/journal}}
* [http://www.brill.nl/hm ''Historical Materialism Book Series'' homepage]

[[Category:Critical theory]]
[[Category:Political science journals]]
[[Category:Marxist journals]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1997]]
[[Category:Brill Publishers academic journals]]


{{poli-journal-stub}}