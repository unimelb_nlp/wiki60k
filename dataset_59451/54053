{{Use dmy dates|date=August 2011}}
{{Use Australian English|date=August 2011}}
[[File:Australian International Horse Trials Logo.png|right|frame]]
The '''Australian International Three Day Event''' is an annual [[eventing|three-day event]] held in the [[South Australia]]n capital of [[Adelaide]]. It was known as the Adelaide International Horse Trials up until 2007. It comprises [[dressage]], [[cross-country equestrianism|cross-country]] and [[show-jumping]] and is usually staged in early November. The event is unique in being held in a city-centre, taking place in the [[Adelaide Park Lands]].<ref>{{cite web | accessdate =1 August 2006 | url =http://www.eques.com.au/sport/horse_trials_2004/page1.htm | archive-url =https://web.archive.org/web/20041206042236/http://www.eques.com.au:80/sport/horse_trials_2004/page1.htm | dead-url =yes | archive-date =6 December 2004 | title = 	
Mitsubishi Adelaide International Horse Trials | publisher =''Eques Magazine'' }}</ref>

==History==

The Adelaide International Horse Trials was created in 1997 to replace the Gawler Horse Trials that had been staged in [[Gawler, South Australia|Gawler]], north of Adelaide, since 1954.<ref>{{cite web
 |accessdate=1 August 2006 
 |url=http://www.australian3de.com
 |title=2006 Adelaide International Horse Trials: General Information 
 |publisher=Adelaide Horse Trials Management 
}}{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> In its period as the Gawler Trials, it was a successful competition and was selected to host the [[Eventing World Championship]] in 1986. On this occasion the Australian Post Office issued a special set of commemorative postmarked covers featuring a set of four stamps called Horses of Australia.

In 2007 the event was renamed the Australian International Three Day Event and is held at the end of November each year.  Since 2011 the Australian International is part of the [[HSBC FEI Classics]], an international eventing series of all CCI****-events around the world.<ref>[http://www.horse-canada.com/horse-news/australias-first-hsbc-fei-classics%E2%84%A2-competition/ Australia’s First HSBC FEI Classics™ Competition], 16 November 2011</ref>

==Event==

The event, managed by Adelaide Horse Trials Management Inc, is held throughout the East Parklands in separate stages over three days.

The dressage phase is held on Friday on the arena in front of the heritage-listed Victoria Park Grandstand. The cross-country phase is held on Saturday across the parklands and attracts the most spectators. The course starts in the arena in front of the grandstand, crosses [[Wakefield Road, Adelaide|Wakefield Road]] to Park 15 and then crosses Bartels road into [[Rymill Park, Adelaide|Rymill Park]] and then returns to the arena again. The cross-country course has been designed by Michael Etherington-Smith, who has designed courses for the Olympics in 2000 and 2008 and other CCI**** events, and more recently, by Australian Wayne Copping. The water jumps in Rymill Park are regarded as some of the most challenging in international competition. The final show-jumping phase is held on Sunday in the main arena in Victoria Park.

The event also incorporates the Australian stages of the biannual [[Trans Tasman Championship]], which has been staged in Adelaide since 1985.  A youth Trans Tasman is held in the alternate years.

==Winners==
List of winners of the four star event:
*2016 Hazel Shannon (NSW) on Clifford 
*2015 Shane Rose (NSW) on CP Qualified
*2014 Jessica Manson (WA) on Legal Star
*2013 Chris Burton (QLD) on TS Jamaimo
*2012 Craig Barrett (NSW) on Sandhills Brillaire
*2011 Stuart Tinney (NSW) on Panamera
*2010 [[Wendy Schaeffer]] (SA) on Koyuna Sun Dancer
*2009 Stuart Tinney (NSW) on Vettori
*2008 [http://www.youngda.equestrian.org.au/?Page=17256&MenuID=EFA_Services%2F11755 Chris Burton] (QLD) on Newsprint
*2007 Trials were not held due to [[Equine Influenza]] interrupting all equestrian activities in Australia
*2006 Heath Ryan (NSW) Flame
*2005 [[Megan Jones (rider)|Megan Jones]] (SA) on Kirby Park Irish Jester</td></tr>
*2004 Shane Rose (NSW) on Beauford Miss Dior</td></tr>
*2003 Boyd Martin (NSW) on True Blue Toozac
*2002 Wendy Schaeffer(SA) on Koyuna Sun Glo
*2001 Matthew Grayling (NZ) on Revo
*2000 David Middleton (VIC) on Willowbank Jack
*1999 Natalie Blundell (NSW) on Billy Bathgate
*1998 Peter Haynes (AUS) on Alcheringa (CCI***)
*1997 Nick Larkin (NZ) on Red (CCI***)

==See also==
The other four-star events are:

*[[Badminton Horse Trials]]
*[[Burghley Horse Trials]]
*[[Rolex Kentucky Three Day|Kentucky Three Day Event]]
*[[Luhmühlen Horse Trials]]
*[[Stars of Pau]]

==References==
{{reflist}}

==External links==
*[http://www.australian3de.com.au/ Australian International 3 Day Event]

[[Category:Eventing]]
[[Category:Sport in Adelaide|Horse Trials, Adelaide International]]
[[Category:Equestrian sports competitions in Australia]]