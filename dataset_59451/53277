{| class="infobox" style="width: 26em; font-size: 95%;"
|-
| colspan="2" style="text-align: center; font-size: 110%;" | '''North Carolina – NC State History'''
|-
| align="center" colspan="2" | 
|-
| '''Teams''' <!-- In alphabetical order -->
| [[NC State Wolfpack]]<br>[[North Carolina Tar Heels]]
|-
|colspan="2"|<center>'''Football Series'''</center>
|-
| '''Last Winner'''
| NC State  <small>(2016)</small>
|-
| '''Current Win Streak'''
| 1 – NC State <small>(2016)</small>
|-
|colspan="2"|<center>'''Basketball Series'''</center>
|-*North Carolina: 152; NC State: 76
|-
| '''Last Winner'''
| North Carolina <small>(2017)</small>
|-
| '''Current Win Streak'''
| 3 – North Carolina <small>(2016)</small>
|-
|colspan="2"|<center>'''Baseball Series'''</center>
|-
| '''Last Winner'''
| NC State <small>(2016)</small>
|-
| '''Current Win Streak'''
| 1 – NC State <small>(2016)</small>
| colspan="2" align="center"| <hr>
|-
| align="center" style="width:13em" | [[Image:North Carolina State University Athletic logo.svg|62px|NC State logo]]
| align="center" style="width:13em" | [[Image:University of North Carolina Tarheels Interlocking NC logo.svg|80px|North Carolina logo]]
|- 
| align="center" | '''NC State'''
| align="center" | '''North Carolina'''
|}

The '''Carolina–State Game'''<!--Article name-->, also known as the North Carolina–NC State game, NCSU-UNC game, or other similar permutations, is an ongoing series of athletic competitions between the [[University of North Carolina at Chapel Hill]] and [[North Carolina State University]]. The intensity of the game is driven by the universities' similar sizes, the fact the schools are separated by only 25 miles, and the large number of alumni that live within the state's borders. Both are charter members of the [[Atlantic Coast Conference]] (ACC) and are part of the [[Tobacco Road]] schools. The most popular games between the two are in [[American football|football]], [[basketball]], and [[baseball]].

In football, when the current structure of the ACC's divisional system was implemented, North Carolina and NC State were matched up as permanent partners so as to allow both schools to face each other annually despite being in different divisions.{{citation needed|date=March 2017}} NC State is in the conference's Atlantic Division and North Carolina is in the Coastal Division. The annual football game between both schools is the biggest in the state of North Carolina every year. <ref>http://www.fanragsports.com/cfb/north-carolina-nc-state-the-rivalry-america-doesnt-care-about/</ref>

Both schools are also known for their basketball history. They have won eight national championships between them (North Carolina 6, NC State 2), and 27 of the past 57 ACC Tournaments (North Carolina 17, NC State 10).

Oddly enough, the ACC has split the two in baseball so that the two schools did not meet in the 2014 regular season;  the result of that is the ACC will reinstate permanent partners for baseball starting in 2015.  In the interim, the two schools agreed to a nonconference neutral-site game in Durham.

==Men's basketball all-time series records==
''Listed by number of games won.''

=== All-time series record===
*North Carolina: 154; NC State: 77

===By location===
*In [[Chapel Hill, North Carolina|Chapel Hill]]: North Carolina: 41; NC State: 10
**At [[Carmichael Auditorium]]: North Carolina 18; NC State: 4
**At [[Dean Smith Center|Smith Center]]: North Carolina: 23; NC State: 6
*In [[Raleigh, North Carolina|Raleigh]]: North Carolina: 58 ; NC State: 45
**At [[Reynolds Coliseum]]: North Carolina: 29; NC State: 29
**At [[RBC Center|ESA/RBC/PNC]]: North Carolina: 11; NC State: 4
*At neutral site: North Carolina: 18; NC State: 9

===By Titles===
*NCAA Championships: North Carolina: 6 (1957, 1982, 1993, 2005, 2009, 2017); NC State: 2 (1974, 1983)
*[[Southern Conference]] Tournament: NC State: 7; North Carolina: 3
*[[Dixie Classic (basketball tournament)|Dixie Classic]]: NC State: 7; North Carolina: 3
*Big Four Tournament: NC State: 3; North Carolina: 2
*[[Atlantic Coast Conference|ACC]] [[ACC Tournament|Tournament]]: North Carolina: 18; NC State: 10

===By Final Four appearances===
*North Carolina: 20; NC State: 3

===By coach===

==== North Carolina coaches ====
*[[Frank McGuire]]: 13–9
*[[Dean Smith]]: 60–30
*[[Bill Guthridge]]: 6–1
*[[Matt Doherty (basketball)|Matt Doherty]]: 2–4
*[[Roy Williams (coach)|Roy Williams]]: 26–3 (31–3 overall)

==== NC State coaches ====
*[[Everett Case]]: 25–19
*[[Press Maravich]]: 2–2
*[[Norm Sloan]]: 13–26
*[[Jim Valvano]]: 6–18
*[[Les Robinson]]: 5–7
*[[Herb Sendek]]: 5–17
*[[Sidney Lowe]]: 1–10
*[[Mark Gottfried]]: 2–10

===By decade===
*1910s: NC State: 2; North Carolina:0
*1920s: North Carolina:17; NC State:3
*1930s: North Carolina:16; NC State:6
*1940s: North Carolina:13; NC State:10
*1950s: NC State: 18 North Carolina:10
*1960s: North Carolina:18 NC State:4
*1970s: North Carolina:17; NC State:13
*1980s: North Carolina:18; NC State:7
*1990s: North Carolina:15; NC State:7
*2000s: North Carolina:16; NC State:5 
*2010s: North Carolina:12; NC State:2

===Other===
''Listed by score.''
*North Carolina's largest victory margin: 107–56 (January 8, 2017)
*NC State's largest victory margin: 79–39 (February 19, 1949)

===Scores of games (1998–2017)===

Winning team is shown in '''bold'''.  Ranking of the team at the time of the game by the AP poll is shown in parenthesis next to the team name (failure to list AP ranking does not necessarily mean the team was not ranked at the time of the game).  
<div style="background:white" class="NavFrame">

<div style="background:white" class="NavHead">Complete List of Scores</div>


{| border="1" cellpadding="3" cellspacing="0"
!bgcolor="#e5e5e5"| Date !! colspan="2" bgcolor="#e5e5e5"| NC State !! colspan="2" bgcolor="#e5e5e5"| North Carolina    !! bgcolor="#e5e5e5" | Site !! bgcolor="#e5e5e5" | Notes about game

|-style="background-color:#56A0D3; color:white"
|January 21, 1998|| NC State || 60 || '''North Carolina''' || 74 || Reynolds Coliseum || none

|-style="background-color:#E00000; color:white"
|February 21, 1998|| '''NC State''' || 86 || North Carolina || 72 || Dean Smith Center || none

|-style="background-color:#56A0D3; color:white"
|March 6, 1998|| NC State || 46 || '''North Carolina''' || 74 || Greensboro Coliseum|| none

|-style="background-color:#56A0D3; color:white"
|January 16, 1999|| NC State || 56 || '''North Carolina''' || 59 || Reynolds Coliseum || none

|-style="background-color:#56A0D3; color:white"
|February 17, 1999|| NC State || 53 || '''North Carolina''' || 62 || Dean Smith Center || none

|-style="background-color:#56A0D3; color:white"
|January 8, 2000|| NC State || 75 || '''North Carolina''' || 83 || Dean Smith Center || none

|-style="background-color:#56A0D3; color:white"
|February 9, 2000|| NC State || 62 || '''North Carolina''' || 70 || PNC Arena || none

|-style="background-color:#56A0D3; color:white"
|January 28, 2001|| NC State || 52 || '''North Carolina''' || 60 || PNC Arena || none

|-style="background-color:#56A0D3; color:white"
|February 28, 2001|| NC State || 63 || '''North Carolina''' || 76 || Dean Smith Center || none

|-style="background-color:#E00000; color:white"
|January 23, 2002|| '''NC State''' (NR) || 77 || North Carolina (NR) || 59 || Dean Smith Center || [http://espn.go.com/ncb/conversation?gameId=220230153 Article about the game]

|-style="background-color:#E00000; color:white"
|February 24, 2002|| '''NC State''' (NR) || 98 || North Carolina (NR) || 76 || PNC Arena || [http://espn.go.com/ncb/conversation?gameId=220550152 Article about the game]

|-style="background-color:#E00000; color:white"
|January 26, 2003|| '''NC State''' (NR) || 86 || North Carolina (NR) || 77 || PNC Arena || [http://espn.go.com/ncb/recap?gameId=230260152 Article about the game]

|-style="background-color:#E00000; color:white"
|February 25, 2003|| '''NC State''' (NR) || 75 || North Carolina (NR) || 67 || Dean Smith Center || Overtime; [http://espn.go.com/ncb/recap?gameId=230560153 Article]

|-style="background-color:#56A0D3; color:white"
|January 28, 2004|| NC State (NR) || 66 || '''North Carolina''' (12) || 68 || Dean Smith Center || [http://espn.go.com/ncb/recap?gameId=240280153 Article about game]

|-style="background-color:#56A0D3; color:white"
|February 29, 2004|| NC State (14) || 64 || '''North Carolina''' (12) || 71 || PNC Arena || [http://espn.go.com/ncb/recap?gameId=240600152 Article about game]

|-style="background-color:#56A0D3; color:white"
|February 3, 2005|| NC State (NR) || 71 || '''North Carolina''' (2) || 95 || Dean Smith Center || [http://espn.go.com/ncb/recap?gameId=250340153 Article about game]

|-style="background-color:#56A0D3; color:white"
|February 22, 2005|| NC State (NR) || 71 || '''North Carolina''' (2) || 81 || PNC Arena || [http://espn.go.com/ncb/recap?gameId=250530152 Article about game]

|-style="background-color:#56A0D3; color:white"
|January 7, 2006|| NC State (13) || 69 || '''North Carolina''' (25) || 82 || Dean Smith Center || [http://espn.go.com/ncb/recap?gameId=260070153 Article about game]

|-style="background-color:#56A0D3; color:white"
|February 22, 2006|| NC State (15) || 71 || '''North Carolina''' (21) || 95 || PNC Arena || [http://espn.go.com/ncb/recap?gameId=260530152 Article about game]

|-style="background-color:#E00000; color:white"
|February 3, 2007|| '''NC State''' (NR) || 83 || North Carolina (3) || 79 || PNC Arena || [http://espn.go.com/ncb/recap?gameId=270340152 Article about game]

|-style="background-color:#56A0D3; color:white"
|February 21, 2007|| NC State (NR) || 64 || '''North Carolina''' (5) || 83 || Dean Smith Center || [http://espn.go.com/ncb/recap?gameId=270520153 Article about game]

|-style="background-color:#56A0D3; color:white"
|March 11, 2007|| NC State (NR) || 80 || '''North Carolina''' (8) || 89 || Tampa Bay Times Forum || [http://espn.go.com/ncb/recap?gameId=270700153 Article about game]

|-style="background-color:#56A0D3; color:white"
|January 12, 2008|| NC State (NR) || 62 || '''North Carolina''' (1) || 93 || Dean Smith Center || [http://espn.go.com/ncb/recap?gameId=280120153 Article about game]

|-style="background-color:#56A0D3; color:white"
|February 20, 2008|| NC State (NR) || 70 || '''North Carolina''' (3) || 84 || PNC Arena || [http://espn.go.com/ncb/recap?gameId=280510152 Article about game]

|-style="background-color:#56A0D3; color:white"
|January 31, 2009|| NC State (NR) || 76 || '''North Carolina''' (5) || 93 || PNC Arena || [http://espn.go.com/ncb/recap?gameId=290310152 Article about game]

|-style="background-color:#56A0D3; color:white"
|February 18, 2009|| NC State (NR) || 80 || '''North Carolina''' (3) || 89 || Dean Smith Center || [http://espn.go.com/ncb/recap?gameId=290490153 Article about game]

|-style="background-color:#56A0D3; color:white"
|January 26, 2010|| NC State (NR) || 63 || '''North Carolina''' (NR) || 77 || PNC Arena || [http://espn.go.com/ncb/recap?gameId=300260152 Article about game]

|-style="background-color:#56A0D3; color:white"
|February 13, 2010|| NC State (NR) || 61 || '''North Carolina''' (NR) || 74 || Dean Smith Center || [http://espn.go.com/ncb/recap?gameId=300440153 Article about game]

|-style="background-color:#56A0D3; color:white"
|January 29, 2011|| NC State (NR) || 64 || '''North Carolina''' (NR) || 84 || Dean Smith Center || [http://espn.go.com/ncb/recap?gameId=310290153 Article about game]

|-style="background-color:#56A0D3; color:white"
|February 23, 2011|| NC State (NR) || 63 || '''North Carolina''' (19) || 75 || PNC Arena || [http://espn.go.com/ncb/recap?gameId=310540152 Article about game]

|-style="background-color:#56A0D3; color:white"
|January 26, 2012|| NC State (NR) || 55 || '''North Carolina''' (7) || 74 || Dean Smith Center || [http://espn.go.com/ncb/recap?gameId=320260153 Article about game]

|-style="background-color:#56A0D3; color:white"
|February 21, 2012|| NC State (NR) || 74 || '''North Carolina''' (7) || 86 || PNC Arena || [http://espn.go.com/ncb/recap?gameId=320520152 Article about game]

|-style="background-color:#56A0D3; color:white"
|March 10, 2012|| NC State (NR) || 67 || '''North Carolina''' (4) || 69 || Phillips Arena || [http://espn.go.com/ncb/recap?gameId=320700153 Article about game]

|-style="background-color:#E00000; color:white"
|January 26, 2013|| '''NC State''' (18) || 91 || North Carolina (NR) || 83 || PNC Arena || [http://espn.go.com/ncb/recap?gameId=330260152 Article about game]

|-style="background-color:#56A0D3; color:white"
|February 23, 2013|| NC State (NR) || 65 || '''North Carolina''' (NR) || 76 || Dean Smith Center || [http://espn.go.com/ncb/recap?gameId=330540153 Article about game]

|-style="background-color:#56A0D3; color:white"
|February 1, 2014|| NC State (NR) || 70 || '''North Carolina''' (NR) || 84 || Dean Smith Center || [http://espn.go.com/ncb/recap?gameId=400502827 Article about game]

|-style="background-color:#56A0D3; color:white"
|February 26, 2014|| NC State (NR) ||84 || '''North Carolina''' (19) || 85 || PNC Arena || Overtime; [http://scores.espn.go.com/ncb/recap?gameId=400502879 Article]

|-style="background-color:#56A0D3; color:white"
|January 14, 2015||NC State (NR) ||79 || '''North Carolina''' (15) || 81 || PNC Arena || [http://scores.espn.go.com/ncb/recap?gameId=400587919 Article about game]

|-style="background-color:#E00000; color:white"
|February 24, 2015|| '''NC State''' (NR) || 58 || North Carolina (15) || 46 || Dean Smith Center || [http://espn.go.com/ncb/recap?gameId=400587998 Article about game]

|-style="background-color:#56A0D3; color:white"
|January 16, 2016|| NC State (NR) || 55 || '''North Carolina''' (5) || 67 || Dean Smith Center || [http://espn.go.com/ncb/recap?gameId=400839811 Article about game]

|-style="background-color:#56A0D3; color:white"
|February 24, 2016|| NC State (NR) || 68 || '''North Carolina''' (7) || 80 || PNC Arena || [http://espn.go.com/ncb/recap?gameId=400839811 Article about game]

|-style="background-color:#56A0D3; color:white"
|January 8, 2017|| NC State (NR) || 56 || '''North Carolina''' (14) || 107 || Dean Smith Center || [http://www.espn.com/mens-college-basketball/game?gameId=400933455 Article about game]

|-style="background-color:#56A0D3; color:white"
|February 15, 2017|| NC State (NR)|| 73 || '''North Carolina''' (10)|| 97|| PNC Arena|| [http://www.espn.com/mens-college-basketball/recap?gameId=400915444 Article about game]
|}
</div>
<div class="NavEnd">&nbsp;</div>

==Women's Basketball all-time series record==
*NC State: 53; North Carolina: 50
*As of January 2017

==Football all-time series records==
{{main|Carolina – NC State football rivalry}}

''Listed by number of matches won.''
*Current streak: 1 – NC State won in 2016.
* All-time record<ref>{{cite web|url=http://www.cfbdatawarehouse.com/data/div_ia/acc/north_carolina/opponents_records.php?teamid=2287 |title=North Carolina Game by Game against Opponents |deadurl=yes |archiveurl=https://web.archive.org/web/20131202233516/http://www.cfbdatawarehouse.com/data/div_ia/acc/north_carolina/opponents_records.php?teamid=2287 |archivedate=2013-12-02 |df= }}</ref> 
North Carolina: 66; NC State: 34; Ties: 6 
* Record in ACC (1953–2010): North Carolina: 35; NC State: 29<ref name="footscore">{{cite web|url=http://www.jhowell.net/cf/scores/NorthCarolinaState.htm#2006 |title=North Carolina State Historical Scores |deadurl=yes |archiveurl=https://web.archive.org/web/20070426040737/http://www.jhowell.net:80/cf/scores/NorthCarolinaState.htm |archivedate=2007-04-26 |df= }}</ref>
* Record in Kenan Memorial Stadium:  North Carolina: 27; NC State: 18; Ties: 1
* Record in Carter-Finley Stadium<ref>{{cite web|url=http://www.cfbdatawarehouse.com/data/div_ia/acc/north_carolina/opponents_records.php?teamid=2287 |title=North Carolina Game by Game against Opponents |deadurl=yes |archiveurl=https://web.archive.org/web/20131202233516/http://www.cfbdatawarehouse.com/data/div_ia/acc/north_carolina/opponents_records.php?teamid=2287 |archivedate=2013-12-02 |df= }}</ref> North Carolina: 12; NC State: 12
* Record in Chapel Hill, NC:  North Carolina leads 34–18–2
* Record in Charlotte, NC:  North Carolina leads 2–0
* Record in Raleigh, NC:  North Carolina leads 28–17–4

===Largest victory margins===
''Listed by score.''
* NC State's largest victory margin: 45 points, score: 48–3 (October 15, 1988)
* North Carolina's largest victory margin: 51 points, score: 107–56 (January 8, 2017)

===All time records===
*North Carolina 647–488–54(.567)
*NC State 586–569–55(.507)

==Baseball All-Time Series Records==

=== All-Time Series Record===
* North Carolina: 160; NC State: 129; Ties: 1
*As of June, 2015

=== ACC Regular Season Championships===
* North Carolina: 13 (1960, 1964, 1966, 1969, 1980, 1983, 1984, 1989, 1990, 2006, 2007, 2009, 2013)
* NC State: 1 (1968)

=== ACC Tournament Championships===
* North Carolina: 6 (1982, 1983, 1984, 1990, 2007, 2013)
* NC State: 4 (1973, 1974, 1975, 1992)

=== College World Series Appearances ===
* North Carolina: 10 (1960, 1966, 1978, 1989, 2006, 2007, 2008, 2009, 2011, 2013)
* NC State: 2 (1968, 2013)

=== College World Series Victories ===
* North Carolina: 16
* NC State: 3

=== National Title Game/Series Appearances ===
* North Carolina: 2 (2006, 2007)
* NC State: 0

==References==
{{reflist}}

{{University of North Carolina at Chapel Hill}}
{{North Carolina Tar Heels men's basketball navbox}}
{{North Carolina State University}}
{{NC State Wolfpack men's basketball navbox}}
{{Atlantic Coast Conference rivalry navbox}}

{{DEFAULTSORT:Carolina-State game}}
[[Category:College basketball rivalries in the United States]]
[[Category:NC State Wolfpack]]
[[Category:University of North Carolina at Chapel Hill rivalries]]