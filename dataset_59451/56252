{{Infobox journal
| title = Cladistics
| cover = [[File:Cladistics cover.gif|Image:Cladistics cover.gif]]
| editor = James M. Carpenter
| frequency = Bimonthly
| discipline = [[Cladistics]]
| history = 1985-present
| country = 
| publisher = [[Wiley-Blackwell]]
| impact = 6.217
| impact-year = 2014
| ISSN = 0748-3007
| eISSN = 1096-0031
| website = http://www.wiley.com/bw/submit.asp?ref=0748-3007
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1096-0031/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1096-0031/issues
| link2-name = Online archive
}}
'''''Cladistics''''' is a bimonthly [[Peer review|peer-reviewed]] [[scientific journal]] publishing research in [[cladistics]]. It is published by [[Wiley-Blackwell]] on behalf of the [[Willi Hennig Society]]. ''Cladistics'' publishes [[academic publishing|papers]] relevant to [[evolution]], [[systematics]], and [[integrative Biology|integrative biology]]. Papers of both a conceptual or [[philosophy of science|philosophical]] nature, discussions of methodology, empirical studies on taxonomic groups from animals to bacteria, and applications of systematics in disciplines such as [[genomics]] and [[paleontology]] are accepted. Five types of paper appear in the journal: [[review journal|review]]s, regular papers, forum papers, [[letter to the editor|letters to the editor]], and [[book review]]s. According to the ''[[Journal Citation Reports]]'', the journal has a 2014[[impact factor]] of 6.217, ranking it 6th out of 46 journals in the category "Evolutionary Biology". Its [[editor-in-chief]] is Dennis Wm. Stevenson.

== External links ==
* {{Official website|1=http://www.wiley.com/bw/submit.asp?ref=0748-3007}}
* [http://www.cladistics.org/ Willi Hennig Society]

[[Category:Evolutionary biology journals]]
[[Category:Phylogenetics]]
[[Category:Bimonthly journals]]
[[Category:English-language journals]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Publications established in 1985]]
[[Category:Academic journals associated with learned and professional societies]]