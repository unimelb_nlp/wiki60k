{{Infobox character
| name        = Gendry
| series      = [[A Song of Ice and Fire]] ''character'' <br> [[Game of Thrones]]
| portrayer   = [[Joe Dempsie]]<br>(''[[Game of Thrones]]'')
| creator     = [[George R. R. Martin]]
| image       = Gendry-Joe Dempsie.jpg
| caption     = [[Joe Dempsie]] as Gendry
| first       = '''Novel''': <br>''[[A Game of Thrones]]'' (1996) <br>'''Television''': <br>"[[Cripples, Bastards, and Broken Things]]" (2011)
| last        = 
| occupation  =
| title       = 
| alias       = The Bull<br>Ser Gendry of the hollow hill
| gender      = Male
| family      = [[House Baratheon]]
| spouse      = 
| children    = 
| relatives   =  [[Robert Baratheon]] {{small|(father)}}<br>[[Mya Stone ]] {{small|(half-sister)}}<br>Bella {{small|(half-sister)}}<br>[[Edric Storm]] {{small|(half-brother)}}<br>Barra {{small|(half-sister)}}<br>Other half-brothers and sisters
| lbl21       = Kingdom
| data21      = [[Crownlands|The Crownlands]]
}}

'''Gendry''' is a [[fictional character]] in the ''[[A Song of Ice and Fire]]'' series of [[fantasy]] novels by American author [[George R. R. Martin]], and its television adaptation ''[[Game of Thrones]]''.<ref>{{cite web|url=http://viewers-guide.hbo.com/guide/houses/sbaratheon/stannis-baratheon|title=Game of Thrones Viewer's Guide|publisher=[[HBO]]}}</ref>

He first appeared in 1996's ''[[A Game of Thrones]]''. He subsequently appeared in Martin's ''[[A Clash of Kings]]'' (1998) and ''[[A Storm of Swords]]'' (2000) and ''[[A Feast for Crows]]'' (2011). Gendry is an apprentice [[blacksmith]] in King's Landing and an unacknowledged bastard of King [[Robert Baratheon]].

Gendry is portrayed by [[Joe Dempsie]] in the [[HBO]] television adaptation.<ref name=HBOCast>{{cite web | url=http://www.hbo.com/game-of-thrones/index.html#/game-of-thrones/cast-and-crew/gendry/bio/gendry.html |title=''Game of Thrones'' Cast and Crew: Gendry played by Joe Dempsie |publisher=[[HBO]] | accessdate=September 15, 2016}}</ref><ref>{{cite web|url=http://www.hbo.com/game-of-thrones/index.html#/game-of-thrones/cast-and-crew/index.html|title=The Official Website for the HBO Series Game of Thrones - Season 4|publisher=HBO}}</ref><ref>{{cite web|url=http://grrm.livejournal.com/164794.html |title=From HBO |publisher= |deadurl=yes |archiveurl=https://web.archive.org/web/20160307150640/http://grrm.livejournal.com/164794.html |archivedate=2016-03-07 |df= }}</ref> He and the rest of the cast were nominated for Screen Actors Guild Awards for Outstanding Performance by an Ensemble in a Drama Series in 2014.

== Character description ==
Gendry is one of many bastard sons of King [[Robert Baratheon]].{{Sfn|''A Game of Thrones''|loc=Appendix}} He is portrayed as tall and very muscled, being very similar to his own father. He also has blue eyes and thick, black hair.<ref name="part14">{{cite news|url=http://www.tor.com/2011/07/01/a-read-of-ice-and-fire-a-game-of-thrones-part-14/ |title=A Read of Ice and Fire: A Game of Thrones, Part 14 |publisher=Tor |date=July 1, 2011 |accessdate=September 15, 2016 |first=Leigh |last=Butler}}</ref><ref name="Accesshollywood">{{cite news|url=https://www.accesshollywood.com/articles/game-of-thrones-joe-dempsie-talks-gendry-fitness-competitions-on-set-life-after-skins-116951/ |title=‘Game Of Thrones’: Joe Dempsie Talks Gendry, Fitness Competitions On Set & Life After ‘Skins’ |publisher=Accesshollywood |date=April 24, 2012 |accessdate=September 15, 2016 |first=Jolie |last=Lash}}</ref>

== Overview ==
Gendry is not a [[Narration#Third-person|point of view]] character in the novels, so his actions are witnessed and interpreted through the eyes of other people, such as [[Arya Stark]]. Gendry is mostly a background character in the novels.<ref>{{cite web|url=http://viewers-guide.hbo.com/map/special/areas-of-control/4/|title=Game of Thrones Viewer's Guide|publisher=}}</ref>

== Storylines ==
[[File:A Song of Ice and Fire arms of House Baratheon yellow scroll English.png|upright|thumb|alt=A coat of arms showing a black stag on a yellow field.|Coat of arms of House Baratheon]]
===Books===
Gendry is one of [[Robert Baratheon]]'s many bastard children. He lives in King's Landing as an armorer's apprentice and is unaware of his true parentage.<ref name="part14"/> In ''A Clash of Kings'' Gendry joins the Night's Watch recruits, meeting [[Arya Stark]] along the way. When their party is ambushed, he escapes with [[Arya Stark|Arya]] and others. However they are later captured by the Mountain's Men and taken to Harrenhal. After escaping Harrenhal, Gendry and Arya cross paths with The Brotherhood without Banners and Gendry agrees to serve as their blacksmith.

He encounters [[Brienne of Tarth]] and her travelling companions at the Crossroads Inn, where he is watching over a group of war orphans. Brienne at first mistakes him for Renly, but works out he is a bastard of Robert. Gendry is revealed to have converted to R'hllorism. When a group of Brave Companions, a particularly vile sellsword company, come to the Inn, Brienne fights them to protect the war orphans. When she is almost killed by Biter, Gendry saves her by running a spear through Biter's neck.

=== TV adaptation ===
Gendry is played by [[Joe Dempsie]] in the television adaption of the series of books.<ref>{{cite web|author=Take a Bow 10 August 2010 at 10:12 PM |url=http://grrm.livejournal.com/173074.html |title=Not A Blog – Take a Bow |publisher=Grrm.livejournal.com |date=August 10, 2010 |accessdate=September 15, 2016 |deadurl=yes |archiveurl=https://web.archive.org/web/20120318141227/http://grrm.livejournal.com/173074.html |archivedate=18 March 2012 |df= }}</ref>

====First season====
Eddard tells the smith that if Gendry ever shows interest in wielding a sword to send him to Eddard. Gendry shows promise as a smith and makes a helmet in the shape of a bull's head; Eddard compliments the helmet, offering to purchase it. Gendry refuses, to the shame of the master smith. After Eddard Stark's fall and eventual execution, arrangements are made for Yoren of the Night's Watch to take Gendry to the Wall with him. 

====Second season====
He travels North with Yoren and other recruits, including Arry, Lommy Greenhands, Hot Pie and Jaqen H'ghar. During their journey, they are stopped by the Goldcloaks, who demand that Yoren give up Gendry as King Joffrey wants all of his father's bastards killed but are forced to leave by Yoren. Later, Gendry reveals to Arry that he knows she is a girl disguised as a boy all along and is surprised to learn she is Arya, Ned Stark's daughter. After the Goldcloaks get help from Ser Amory Lorch and his men who kill Yoren, Gendry's life is saved by Arya when she lies to the Goldcloaks that Lommy, who was killed during the attack, was Gendry. Gendry and the rest of recruits are sent to Harrenhal where Ser Gregor Clegane arbitrarily has many of the prisoners tortured and killed. Gendry was about to suffer this fate but is saved by the arrival of Lord Tywin Lannister, who chides Clegane's men for their reckless behavior. Thanks to Jaqen, Arya, Gendry and Hot Pie are able to escape Harrenhal.

====Third season====
As they head towards the Riverlands, the group encounters the Brotherhood Without Banners, a group of Outlaws that defend the weak. Inspired, he decides to join the Brotherhood but is betrayed by them when they sell him to Lady Melisandre as ordered by the Lord of Light. Melisandre later reveals to Gendry that King Robert was his father and she is bringing him to meet his uncle, King Stannis. But in truth, Melisandre and Stannis planned to use him for her blood magic where Stannis uses his nephew's blood to make a death curse on the usurpers to his throne, Joffrey, Robb, and Balon Greyjoy. Before they can use him as a sacrifice, Davos Seaworth helps Gendry escape and puts him on a boat to King's Landing. Unable to swim or row, Gendry is nevertheless convinced that the Red Woman has a surer death in spare for him, and Davos asks him to "have a bowl'o brown for me" when returning to Flea Bottom.<ref>{{cite web|url=http://blog.zap2it.com/frominsidethebox/2013/06/game-of-thrones-season-3-finale-recap-mhysa.html |title='Game of Thrones' Season 3 finale recap: 'Mhysa' |first=Terri |last=Schwartz |work=[[Zap2it]] |publisher=[[Tribune Media Services]] |date=June 9, 2013 |accessdate=September 15, 2016}}</ref>

===Family tree of House Baratheon===
{{Family tree of House Baratheon}}

==TV adaptation==
[[File:Joseph Dempsie 2007.jpg|thumb|right|upright|[[Joe Dempsie]] plays the role of Gendry in the television series ''[[Game of Thrones]]''.]]
In January 2007, [[HBO]] secured the rights to adapt Martin's [[Game of Thrones|series for television]].<ref>{{cite web |url=http://collider.com/game-of-thrones-season-3-4-george-r-r-martin-interview/ |title=Producers David Benioff, Dan Weiss & George R.R. Martin Talk ''Game of Thrones'' Season 3 and 4, Martin’s Cameo, the End of the Series, and More |first=Christina |last=Radish |website=Collider.com |year=2013 |accessdate=August 3, 2014}}</ref> Years later [[Joe Dempsie]] portrayed Gendry in the first season.<ref name="Accesshollywood"/>

Remarking on how he got cast for the role:
<blockquote>When I was cast as Gendry, I didn't have any of the physical attributes the part required. I was astounded that I got the role, to be honest. But David and Dan said, "We need to die ''[sic]'' his hair black ... and it'd be great you hit the gym before we start filming." So I was told to get in shape.<ref>{{cite news|title='Game of Thrones' Q&A: Joe Dempsie on Gendry's Long, Strange Trip|url=http://www.rollingstone.com/movies/news/game-of-thrones-q-a-joe-dempsie-on-gendrys-long-strange-trip-20130606 |last=Collins |first=Sean T. |accessdate=September 15, 2016 |work=[[Rolling Stone]] |date=June 6, 2013}}</ref></blockquote>

===Recognition and awards===
The English actor [[Joe Dempsie]] has received positive reviews for his performance as Gendry in the television series. He and the rest of the cast were nominated for [[Screen Actors Guild Award]]s for [[Screen Actors Guild Award for Outstanding Performance by an Ensemble in a Drama Series|Outstanding Performance by an Ensemble in a Drama Series]] in 2014.<ref>{{cite web|url=http://www.deadline.com/2013/12/sag-awards-2013-nominations-full-list/ | title=SAG Awards Nominations: ''12 Years A Slave'' And ''Breaking Bad'' Lead Way |publisher=[[Deadline.com]] | date=December 11, 2013| accessdate=September 15, 2016 }}</ref><ref>{{cite web |url=http://www.deadline.com/2014/01/sag-awards-2014-stunt-winners-lone-survivor-game-of-thrones/ | title=SAG Awards: ''Lone Survivor'', ''Game Of Thrones'' Win Stunt Honors |publisher=Deadline.com | date=January 18, 2014 | accessdate=September 15, 2016}}</ref>

== References ==
{{Reflist|30em}}

==Sources==
* {{cite book|ref={{SfnRef|''A Game of Thrones''}}|last=Martin|first=George R. R.|title=[[A Game of Thrones]]|edition=US hardcover|series=A Song of Ice and Fire|date=September 1996|publisher=[[Bantam Spectra]]|isbn=978-0-553-10354-0}}

{{ASOIAF}}

{{DEFAULTSORT:Gendry}}
[[Category:A Song of Ice and Fire characters]]
[[Category:Fictional characters introduced in 1996]]
[[Category:Characters in American novels of the 20th century]]
[[Category:Characters in American novels of the 21st century]]
[[Category:Fictional adoptees]]
[[Category:Fictional orphans]]
[[Category:Fictional swordsmen]]
[[Category:Fictional smiths]]