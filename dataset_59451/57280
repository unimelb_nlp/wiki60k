{{italic title}}
The '''''History of Philosophy Quarterly''''' (''HPQ'') is a [[peer-review]]ed [[academic journal]] dedicated to the [[history of philosophy]].  The journal is indexed by [[PhilPapers]] and the [[Philosopher's Index]].<ref>[http://philpapers.org/journals PhilPapers Publications List]</ref><ref>[http://philindex.org/downloads/PIC_Alphabetical_Coverage.pdf Philosopher's Index Publications List]</ref>

The ''History of Philosophy Quarterly'' was founded in 1984 by [[Nicholas Rescher]] of the [[University of Pittsburgh]].<ref>"Notice", ''Mind'' New Series, vol. 93, no. 369 (Jan., 1984), p. 110.</ref> In the first issue, the editors of the journal announced that a focus would be on looking to the history of philosophy to help solve contemporary issues, advocating "that approach to philosophical history, increasingly prominent in recent years, which refuses to see the boundary between philosophy and its history as an impassable barrier, but regards historical studies as a way of dealing with problems of continued interest and importance."<ref>"Editorial Statement", ''History of Philosophy Quarterly'', vol. 1, no. 1 (Jan., 1984), p. 2.</ref>  The journal is published by the [[University of Illinois Press]] and the current editor is Richard C. Taylor at [[Marquette University]].<ref>[http://www.press.uillinois.edu/journals/hpq/hpqeditors.html UI Press | Journals | History of Philosophy Quarterly | Editor Contact Information]</ref>

==Notes==
{{reflist}}

==External links==
*{{Official website|http://www.press.uillinois.edu/journals/hpq.html}}
*[http://philpapers.org/pub/385 PhilPapers listing for ''History of Philosophy Quarterly'']
*[http://www.jstor.org/journal/histphilquar JSTOR Listing for ''History of Philosophy Quarterly]

[[Category:History of philosophy journals]]
[[Category:Publications established in 1984]]
[[Category:Quarterly journals]]
[[Category:Academic journals published by university presses of the United States]]


{{philo-journal-stub}}