<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=iXess
 | image=Ixess hang glider arp.jpg
 | caption=iXess wing mounted on an [[Air Creation Tanarg]] trike
}}{{Infobox Aircraft Type
 | type=[[Ultralight trike]] [[wing]]
 | national origin=[[France]]
 | manufacturer=[[Air Creation]]
 | designer=
 | first flight=
 | introduced=circa 2000
 | retired=
 | status= In production (2013)
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= <!-- [[US$]] [[Euro|€]] [[Pound sterling|£]] (date) -->
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Air Creation iXess''' is a [[France|French]] double-surface [[ultralight trike]] [[wing]], designed and produced by [[Air Creation]] of [[Aubenas]]. The wing is widely used on Air Creation trikes, as well as those of other manufacturers.<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 200. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref>

==Design and development==
The wing is a [[Flying wires|cable-braced]], [[king post]]-equipped [[hang glider]]-style wing designed as a high performance and competition wing for two-place trikes, although it is also used for [[flight training]]. It currently comes in one size, the iXess 15, named for its metric wing area of {{convert|15|m2|sqft|abbr=on}}, although a {{convert|13|m2|sqft|abbr=on}} version was once available.<ref name="WDLA11" /><ref name="iXess">{{cite web|url = http://www.aircreation.fr/en/catalog/wings/ixess_15_234|title = iXess|accessdate = 27 February 2013|last = [[Air Creation]]|date = n.d.}}</ref>

The wing is made from bolted-together [[aluminum]] tubing, with its 90% double surface wing covered in [[Dacron]] sailcloth. Its {{convert|10|m|ft|1|abbr=on}} span wing has a nose angle of 120°, an [[Aspect ratio (wing)|aspect ratio]] of 6.66:1 and uses an "A" frame weight-shift control bar.<ref name="WDLA11" /><ref name="Specs">{{cite web|url = http://www.aircreation.fr/produits/ailes/ixess-15/ctp_ixess_15_a.pdf|title = iXess 15 Specifications|accessdate = 27 February 2013|last = [[Air Creation]]|date = n.d.}}</ref>

Originally the top-of-the line wing in Air Creation's catalog, the iXess has lost that position to the [[Air Creation BioniX]], although the iXess remained in production through 2013.<ref name="WDLA11" /><ref name="iXess" />

==Operational history==
The iXess has been widely used in microlight competition and was used to win World Championships in 2001, 2005 and 2007, as well as the European Championships in 2002, 2004 and 2006.<ref name="iXess" />

==Variants==
;iXess 13
:{{convert|13|m2|sqft|abbr=on}} version, no longer in production<ref name="WDLA11" />
;iXess 15
:{{convert|15|m2|sqft|abbr=on}} version, still in production in 2013<ref name="WDLA11" /><ref name="iXess" />

==Applications==
[[File:Air Creation Tanarg.JPG|thumb|right|iXess wing mounted on an [[Air Creation Tanarg]] trike]]

*[[Air Creation iXess Clipper 582]]<ref name="WDLA04">Bertrand, Noel; Rene Coulon; et al: ''World Directory of Leisure Aviation 2003-04'', pages 91-92. Pagefast Ltd, Lancaster OK, 2003. ISSN 1368-485X</ref>
*[[Air Creation Skypper]]<ref name="WDLA11" />
*[[Air Creation Tanarg]]<ref name="WDLA11" />
*[[Air Creation Trek]]<ref name="WDLA11" />
*[[Apollo Jet Star]]
*[[Take Off Merlin]]
*[[Ventura 1200]]

==Specifications (iXess 15) ==
{{Aircraft specs
|ref=Bayerl and Air Creation<ref name="WDLA11" /><ref name="Specs" />
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=
|capacity=
|length m=
|length ft=
|length in=
|length note=
|span m=10
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=15
|wing area sqft=
|wing area note=
|aspect ratio=6.66:1
|airfoil=
|empty weight kg=53
|empty weight lb=
|empty weight note=wing weight
|gross weight kg=450
|gross weight lb=
|gross weight note=maximum aircraft gross weight allowed
|more general=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=60
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=130
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=+6/-3 at 450 kg (992 lb)
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=maximum
|more performance=
}}

==References==
{{reflist}}

==External links==
{{Commons category|Air Creation}}
*{{Official website|http://www.aircreation.fr/en/catalog/wings/ixess_15_234}}
{{Air Creation aircraft}}

[[Category:Ultralight trike wings]]