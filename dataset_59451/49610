{{for|the 1993 New Zealand film|Desperate Remedies (film)}}
{{EngvarB|date=November 2015}}
{{Use dmy dates|date=November 2015}}

{{infobox book | 
| name          = Desperate Remedies
| title_orig    =
| translator    =
| image         = Desperate Remedies 1871.jpg
| image_size = 180px
| caption = ''First edition title page''
| author        = [[Thomas Hardy]]
| illustrator   =
| cover_artist  =
| country       = United Kingdom
| language      = English
| series        =
| genre         = Novel
| publisher     = [[William Tinsley (publisher)|Tinsley Brothers]]
| release_date  = 1871
| english_release_date =
| media_type    = Print (hardcover)
| pages         =
| preceded_by   =
| followed_by   =
}}

'''''Desperate Remedies''''' is a novel by [[Thomas Hardy]], published anonymously by [[William Tinsley (publisher)|Tinsley Brothers]] in 1871.

==Plot summary==
In ''Desperate Remedies'' a young woman, Cytherea Graye, is forced by poverty to accept a post as lady's maid to the eccentric Miss Aldclyffe, the woman whom her father had loved but had been unable to marry. Cytherea loves a young architect, Edward Springrove, but Miss Adclyffe's machinations, the discovery that Edward is already engaged to a woman whom he does not love, and the urgent need to support a sick brother drive Cytherea to accept the hand of Aeneas Manston, Miss Adclyffe's illegitimate son, whose first wife is believed to have perished in a fire; however, their marriage is almost immediately nullified when it emerges that his first wife had left the inn before it caught fire. Manston's wife, apparently, returns to live with him, but Cytherea, her brother, the local rector, and Edward come to suspect that the woman claiming to be Mrs. Manston is an impostor. It emerges that Manston killed his wife in an argument after she left the inn, and had brought in the impostor to prevent his being prosecuted for murder, as the argument had been heard (but not seen) by a poacher, who suspected Manston of murder and had planned to go to the police if his wife did not turn up alive. In the novel's climax, Manston attempts to kidnap Cytherea and flee, but is stopped by Edward; he later commits suicide in his cell, and Cytherea and Edward marry.

== Publishing history==
After Hardy had trouble publishing his [[The Poor Man and the Lady|first novel]], he was told to "attempt a novel with a purely artistic purpose, giving it a more 'complicated' plot than was attempted with his [[The Poor Man and the Lady|first, unpublished novel]]." The publication of ''Desperate Remedies'' was Hardy's breakthrough, and the first of a long string of novels that propelled him to the forefront of Victorian letters.<ref>C.J.P. Beatty's introduction to the 1975 publication of ''Desperate Remedies''</ref>

== Criticism==
Some critics cite "quasi-gothic" elements in ''Desperate Remedies''. It was positively reviewed in the [[Athenaeum (British magazine)|Athenaeum]] and [[Morning Post]]. However, the review in [[the Spectator]] excoriated Hardy and his work, calling the book "a desperate remedy for an emaciated purse" and that the unknown author had "prostituted his powers to the purposes of idle prying into the way of wickedness."<ref>Tomalin, Claire. "Thomas Hardy." New York: Penguin, 2007.</ref> Hardy wrote of the review, "alas...[[the Spectator]] brought down its heaviest leaded pastoral staff on the prematurely happy volumes...the bitterness of that moment was never forgotten, at that moment I wished I was dead."<ref>F.E. Hardy, ''The Life of Thomas Hardy 1840–1928'' (1962) pg.84</ref>

==References==
{{reflist}}

== External links==
{{wikisource|Desperate Remedies (Hardy)}}
*[https://books.google.com/books?id=3IHkVDnGNMcC&printsec=frontcover&dq=desperate+remedies&hl=en&ei=f5RxTPXPFoPGlQeKqcCUDg&sa=X&oi=book_result&ct=result&resnum=1&ved=0CC8Q6AEwAA#v=onepage&q&f=false ''Desperate Remedies''] Full text at [[Google Books]]
{{Gutenberg|no=3044|name=Desperate Remedies}}
* {{librivox book | title=Desperate Remedies | author=Thomas Hardy}}

{{Thomas Hardy}}

[[Category:1871 novels]]
[[Category:19th-century British novels]]
[[Category:Novels by Thomas Hardy]]
[[Category:Works published anonymously]]