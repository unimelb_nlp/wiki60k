{{Infobox journal
| title = Computational Biology and Chemistry
| cover = [[File:Computational_Biology_and_Chemistry_cover.gif]]
| editor =  [[Wentian Li]], [[Jaap Heringa]]
| discipline = [[Computational biology]] and [[computational chemistry|chemistry]]
| formernames = Computer & Chemistry
| abbreviation = Comp. Biol. Chem.
| publisher = [[Elsevier]]
| country =
| frequency = 7/year
| history = 1976–present
| openaccess =
| license =
| impact = 1.014
| impact-year = 2015
| website = http://www.journals.elsevier.com/computational-biology-and-chemistry/
| link1 = http://www.sciencedirect.com/science/journal/14769271
| link1-name = Online access
| link2 =
| link2-name =
| JSTOR =
| OCLC = 643939876
| LCCN = 2003252370
| CODEN = CBCOCH
| ISSN = 1476-9271
| eISSN =
}}
'''''Computational Biology and Chemistry''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] published by [[Elsevier]] covering all areas of computational [[life sciences]]. The current [[editor-in-chief]] are  [[Wentian Li]] ([[The Feinstein Institute for Medical Research]]) and [[Jaap Heringa]] ([[Vrije Universiteit]]). The journal was established in 1976 as ''Computer & Chemistry''. It obtained its current title in 2003 under the editorship of [[Andrzej Konopka]] and [[James Crabble]] ([[University of Bedfordshire]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[BIOSIS Previews|BIOSIS]]
* [[CSA (database company)|Cambridge Scientific Abstracts]]
* [[Chemical Abstracts Service|Chemical Abstracts]]
* [[Current Contents]]
* [[EMBiology]]
* [[Engineering Index]]
* [[Inspec]]
* [[MEDLINE]]
* [[Mathematical Reviews|MathSciNet]]
* [[PASCAL (database)|PASCAL]]
* [[Science Citation Index]]
* [[Scopus]]
}}
According to the ''[[Journal Citation Reports]]'', the journal had a 2011 [[impact factor]] of 1.551, ranking it 42nd out of 85 journals in the category "Biology"<ref name=WoS1>{{cite book |year=2013 |chapter=Journals Ranked by Impact: Biology |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2013-01-10 |series=[[Web of Science]] |postscript=.}}</ref> and 36th out of 99 journals in the category "Computer Science, Interdisciplinary Applications"<ref name=WoS>{{cite book |year=2013 |chapter=Journals Ranked by Impact: Computer Science, Interdisciplinary Applications |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2013-01-10 |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official|http://www.journals.elsevier.com/computational-biology-and-chemistry/}}

[[Category:Biomedical informatics journals]]
[[Category:Publications established in 1976]]
[[Category:English-language journals]]
[[Category:Elsevier academic journals]]