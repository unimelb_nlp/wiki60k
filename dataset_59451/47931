{{orphan|date=January 2012}}
{{Infobox person
| name        = Vagn F. Flyger
| image       = 
| alt         = 
| caption     = 
| birth_name  = 
| birth_date  = {{Birth date|1922|01|14}}
| birth_place = [[Aalborg, Denmark]]
| death_date  = {{Death date and age|2006|01|09|1922|01|14}}
| death_place = [[Silver Spring, Maryland]], [[United States]]
| nationality = Danish
| other_names = 
| known_for   = 
| occupation  = [[Wildlife biologist]], [[Zoologist]]
}}

'''Vagn F. Flyger''' (14 January 1922 – 9 January 2006) was a [[Denmark|Danish]]-[[United States|American]] [[wildlife biologist]] and one of the world's foremost authorities on [[squirrels]].<ref name=Adams>{{cite journal|last=Adams|first=Lowell W.|author2=Franklin, T.M. |author3=Hadidian, John |author4=Sheffield, S.R. |title=Vagn F. Flyger, International Squirrel Authority|journal=Wildlife Society Bulletin|year=2006|issue=34|pages=1248–1249|doi=10.2193/0091-7648(2006)34[1248:VFFISA]2.0.CO;2|url=http://www.bioone.org/doi/abs/10.2193/0091-7648%282006%2934%5B1248%3AVFFISA%5D2.0.CO%3B2?journalCode=wbul|accessdate=1 January 2012 |volume=34}}</ref>  His landmark work was ''The 1968 Squirrel "Migration" In The Eastern United States''.<ref name=GSM>{{cite web|last=Flyger|first=Vagn|title=The 1968 squirrel 'migration' in the eastern United States|url=http://www.myoutbox.net/flyger.htm|publisher=Smithsonian Institution}}</ref> Flyger was also one of the first wildlife biologists to utilize [[Tranquilliser gun|tranquilizer guns]] in the 1950s when they were first introduced.<ref name=Pa-obit>{{cite news|last=Gelfand|first=Alexander|title=In Memoriam: Vagn Flyger|url=http://www.alexandergelfand.com/journalism/flyger.html|newspaper=The Penn Stater|date=May–June 2006}}</ref> In the 1960s, he experimented with dart guns in the [[Arctic]] on [[Pinniped|seals]], [[whales]] and [[polar bears]]. However, seeking a field of study that kept him closer to home, Flyger focused most of his remaining research on squirrels.<ref name=Bernstein>{{cite news|last=Bernstein|first=Adam|title=Vagn Flyger, 83; Biologist was Expert on Squirrels|url=http://www.washingtonpost.com/wp-dyn/content/article/2006/01/11/AR2006011102262.html|newspaper=Washington Post|date=January 11, 2006}}</ref>

==Background==
Vagn Folkmann Flyger was born January 14, 1922 in [[Aalborg, Denmark]] and his family immigrated to [[Jamestown, New York]] in 1923. Their home in Western New York was within walking distance of [[Chautauqua Lake]], in addition to many streams, peat bogs, fields and forests. These environs were teeming with wildlife, and he spent his youth catching an abundance of snakes, insects and other little animals. At a young age, he decided that he wanted to become a [[biologist]], perhaps even an [[entomologist]] or [[herpetologist]].<ref name=Bio >{{cite web|last=Flyger|first=Vagn|title=Biography|url=http://www.pwrc.usgs.gov/resshow/perry/bios/FlygerVagn.htm|work=obit|publisher=Patuxent Wildlife Research Center|accessdate=1 January 2012}}</ref>

Flyger became a U.S. citizen in October 1942[http://www.kinquest.com/dkgenealogy/wwiidanes.php] and enlisted in the Army in 1943. He served from 1943–1946, 18 months of which were in Europe as a medical and surgical technician in the 1263rd combat engineer battalion.<ref name=SCWDS>{{cite journal|last=Prestwood|first=Katherine|title=Vagn Flyger|journal=SCWDS Briefs|date=January 2006|volume=21|issue=4|pages=8–9|url=http://www.uga.edu/scwds/briefs/106brief.pdf}}</ref>

==Education==
Flyger attended [[Cornell University]] and graduated in 1948 with a bachelor's degree in [[zoology]]. In 1952, he received a master's degree in wildlife management from [[Pennsylvania State University]] and in 1956 earned a doctoral degree in vertebrate ecology from [[Johns Hopkins University]]. His studies focused on [[Mammal|mammalian]] ecology and wildlife habitats.<ref name=Adams />

==Work==

===Deer in Maryland===
After graduating from Johns Hopkins, Flyger worked as a research biologist for the state of Maryland at their [[Chesapeake Biological Laboratory]] in [[Solomons, Maryland]]. He collected extensive data on deer, which assisted in regulating the [[white-tailed deer]] population in the state. His methods of analyzing the deer population are still considered foundational in deer management in the state.<ref name=Dedication>{{cite journal|title=Dedication|journal=Maryland Annual Deer Report|year=2005–2006|page=2|url=http://www.dnr.maryland.gov/wildlife/Hunt.../md_annual_deer_report05-06.pdf|publisher=Dept. of Natural Resources|location=Annapolis, Maryland}}</ref>

Flyger also developed the used of a syringe gun (the "[[Tranquilliser gun|Cap-Chur Gun]]") which would sedate the deer for transfer and study.  In the late 1950s, hunters in Maryland were clamoring for more deer, while, at the same time, the [[Aberdeen Proving Ground]] had an overabundance of deer. Using the Cap-Chur Gun and traps, Flyger and his colleagues transferred 1500 white-tailed deer from the Aberdeen Proving Ground into the northern areas of the state where there were little or no deer. Although the hunters were pleased with the restocking, the farmers began experiencing trouble with deer eating their crops.  Flyger then developed a method he had learned in the Army during [[World War II|WWII]] to scare off the deer by setting up strings of [[M80 assault rifle|M80s]], which proved very effective. However, some [[Poaching|poachers]] had their fingers blown off, so the method was abandoned, though Flyger continued to believe that this system had merit.<ref name=video>{{cite web|last=Flyger|first=Vagn|title=Celebrating our Wildlife Conservation Heritage|url=http://vimeo.com/channels/cowch#21313058|publisher=The Wildlife Society}}</ref>

===Polar expeditions===
His work with the syringe tipped arrows in Maryland led Flyger to believe that it might be a useful tool with larger mammals. A Norwegian company gave him the funds to travel to the Arctic to test his equipment on whales. He spent two summers in the Arctic (around [[Kendall Island]] in the Northwest Territory) with the [[Inuit]], adapting the devices so that they could function in the extreme cold and be able to penetrate the skin and blubber layers of the whale.<ref name=PB66>{{cite journal|last=Flyger|first=Vagn|title=Polar Bear Studies During 1966|journal=Arctic Institute of North America|year=1967|volume=20|issue=1|url=http://arctic.synergiesprairies.ca/arctic/index.php/arctic/article/view/3279}}</ref>

In 1966, Flyger and Dr. Martin Schein, traveled to the Arctic Research Laboratory in [[Point Barrow, Alaska]] to use the sedation guns on polar bears so that they could be tagged or marked for research about their numbers and movements. The following year, Flyger traveled with an international team of scientists to [[Svalbard, Norway]].

At the time of these expeditions, the [[National Aeronautics and Space Administration]] (NASA) was interested in utilizing satellites for tracking polar bears, so Flyger and Schein experimented with placing collars (simulating transmitters) on the bears. However, in the end, NASA underwent funding cuts, and the satellite tracking never happened with the polar bears.<ref name=PB66 />

In the summer of 1963 and 1964, Flyger traveled to [[Ross Island]] in the [[Antarctic]] to study the effects of three immobilizing drugs on [[Weddell seal|Weddell seals]], using the automatic projectile syringe.<ref>{{cite journal|last=Flyger|first=Vagn|author2=Murray Smith |author3=Robert Damm |author4=Richard Peterson |title=Effects of three immobilizing drugs on Weddell seals|journal=Journal of Mammalogy|date=May 1965|volume=46|issue=2|pages=345–347|doi=10.2307/1377872|url=http://www.vliz.be/imisdocs/publications/149547.pdf}}</ref>

===Squirrel Studies===
Following his graduation from Cornell, Flyger worked as a game biologist for the state of Maryland, and began his research with squirrels. This led to the discovery that half of the females being killed during the hunting season were either pregnant or nursing. Flyger recommended that the squirrel hunting season be delayed into October, and this has been the schedule since the 1950s.

In 1968, the [[Smithsonian Institution]] formed the [[Center for Short-Lived Phenomena]], which was established to study short-lived natural phenomena such as as earthquakes, volcanoes, and other unusual ecological events. Flyger requested to be notified by the Center of any squirrel migrations and, soon after, was informed about an unusual squirrel migration occurring from North Carolina into Tennessee. This was one of the first events in which the center became involved. Flyger examined several hundreds of squirrel carcasses found in the area and wrote his [[doctoral dissertation]] on the subject. He determined that the "migration" was a result of a rise in squirrel births which happened to coincide with a poor acorn crop, causing them to seek food in unfamiliar territories.<ref name=GSM />

Flyger devised a number of marking systems for tracking the movements of squirrels in both urban and rural environments, including the use of radio collars. He created nesting boxes to facilitate the study of squirrels along with many feeding devices that challenged the squirrels' intelligence, memory, and agility. He was well known for distributing "wanted" posters throughout the countryside requesting that road-kill squirrels be sent to the Natural Resources Institute at the [[University of Maryland, College Park]], where he was Chair of the Department of Forestry, Fish and Wildlife. Unfortunately, some people took it upon themselves to mail dead carcasses through the Post Office. However, these and other squirrels that he collected over a half-century provided valuable information about diseases, parasites, population, environmental factors, among other findings. Flyger's decades of research on squirrels included the endangered [[Delmarva fox squirrel]] and [[Flying squirrel|flying squirrels]]. His work with flying squirrels was the subject of a [[BBC]] documentary.<ref name=Adams />

===Educator===
In 1962, Flyger became a faculty member at the University of Maryland's Natural Resources Institute. In addition to his research, he taught at the University for 25 years. He was a well-respected and popular professor at the University, who inspired many students to enter the field of [[wildlife conservation]] and mentored many young professionals.<ref name=Dedication /> One of Flyger's most significant contributions to the field may have been his ability to educate the public about wildlife in an engaging, informative and interesting manner. Flyger was also known for his quick wit and dry humor, and was often interviewed by newspapers, magazines, and other media. The backyard of his home in [[Silver Spring, Maryland]] opened into parkland, which made it a refuge for squirrels and other wildlife. Even after his retirement, when he became an Emeritus Professor, it was the site of on-going research, interviews and documentaries. He was interviewed by the [[National Wildlife Federation]], [[National Geographic Society|National Geographic]], and the BBC in this wooded setting.<ref name=Adams />

==Publications==
{{example farm|date=January 2012}}
(Partial list. Many available online to academic institutions)

'''Deer'''
* ''The Status of the White-Tailed Deer in Maryland, 1956'', Vagn Flyger (Resource Study Report, No. 13, Maryland Department of Research and Education, Solomons, Md.)
* ''Tooth impressions as an aid in the determination of age in deer'' Vagn Flyger (The Journal of Wildlife Management, Vol. 22, No. 4 (Oct., 1958), pp.&nbsp;442–443)
* ''Capturing deer in Maryland for research and relocation'' Vagn Flyger (Issue 130 of Contribution (Maryland. Board of Natural Resources. Dept. of Research and Education, 32 pages) 1960)
* ''Factors in the Mass Mortality of a Herd of Sika Deer, Cervus nippon'' John J. Christian, Vagn Flyger, David E. Davis (Chesapeake Science, Vol. 1, No. 2 (Jun., 1960), pp.&nbsp;79–95
* ''Sika deer on islands in Maryland and Virginia'' Vagn Flyger, (Journal of Mammalogy, Vol. 41, No. 1 (Feb., 1960), p.&nbsp;140)
* ''Distribution of sika deer (Cervus nippon) in Maryland and Virginia in 1962'' Vagn Flyger, Norman W. Davis, (Chesapeake Science, Vol. 5, No. 4 (Dec., 1964), pp.&nbsp;212–213)
* ''Trauma with Secondary Shock in Four White-Tailed Deer'' Vagn F. Flyger, Theodore R. Ridgeway, Annie K. Prestwood, Frank A. Hayes (Chesapeake Science, Vol. 3, No. 4 (Dec., 1962), pp.&nbsp;236–243
* ''Thyroidal radioiodine concentrations in North American deer following 1961-1963 nuclear weapons tests'' Hanson, W. C.; Dahl, A. H.; Whicker, F. W.; Longhurst, W. M.; Flyger, V.; Davey, S. P.; Greer, K. R., (December 1963 - Volume 9 - Issue 12 Chesapeake Biological Laboratory:  1963)
* ''Handling wild mammals with a new tranquilizer'' (Issue 229 of Contribution (Chesapeake Biological Laboratory : 1961)
* ''Crop damage caused by Maryland deer'' Vagn Flyger, Theodore Theorig. (Natural Resources Institute, Issue 283 of Contribution (Chesapeake Biological Laboratory : 1961)
* ''Relationship of sex and age to strontium-90 accumulation in white-tailed deer mandibles'' Vincent Schultz, Vagn Flyger,  (The Journal of Wildlife Management, Vol. 29, No. 1 (Jan., 1965), pp.&nbsp;39–43)

'''Arctic'''
* ''Hunters of the White Whale'' Vagn Flyger, (published in Beaver magazine, Hudson's Bay Company Canada's National History Society, DATE)
* ''Effects of three immobilizing drugs on Weddell seals''  Flyger, Vagn & Murray S. R. Smith, Robert Damm, Richard S. Peterson. (Journal of Mammalogy, Vol. 46, No. 2 (May, 1965), pp.&nbsp;345–347, 1965) www.vliz.be/imisdocs/publications/149547.pdf
* ''Capturing And Handling Polar Bears— A Progress Report On Polar Bear Ecological Research'' Vagn Flyger with Martin W. Schein. Albert W. Erikson Thor Larsen, (Published by the Wildlife Management Institute, Washington, D.C., 1967)<ref>https://books.google.com/books/about/Capturing_and_handling_polar_bears_a_pro.html?id=fGjiNwAACAAJ</ref>
* ''Succinylcholine chloride for killing or capturing whales'' Vagn Flyger, (Issue 253 of Contribution (Chesapeake Biological Laboratory : 1964)
* ''Polar Bear Studies during 1966'' Vagn Flyger,  (Arctic, Vol. 20, No. 1 (Mar., 1967), p.&nbsp;53)
* ''The Polar Bear: A Matter for International Concern'' Vagn Flyger (Arctic Institute of North America, (1967) Vol 20, No 3, 1967)<ref>{{cite web|url=http://arctic.synergiesprairies.ca/arctic/index.php/arctic/article/view/3291|title=The Polar Bear: A Matter for International Concern - Flyger - ARCTIC|work=synergiesprairies.ca}}</ref>
* ''The Migration of Polar Bears'' by Vagn Flyger and Marjorie R. Townsend, (Scientific American, Inc., New York. 1968. AF2 - Scientific American (February 1968, Vol. 218, No. 2)

'''Squirrels'''
* ''A bibliography of the tree squirrels and other pertinent literature'' Vagn Flyger (Penna. Cooper. Wildl. Research Unit Quart. Report, 13:19-38. 1955)
* ''The Social Behavior of the gray squirrel and population (Sciurus carolinensis Gmelin)'' in Maryland  Vagn Flyger
* ''A comparison of methods for estimating squirrel populations'' Vagn Flyger (The Journal of Wildlife Management, Vol. 23, No. 2 (Apr., 1959), pp.&nbsp;220–223 Published by: Allen Press, 1959)<ref>http://www.jstor.org/stable/3797645</ref>
* ''Movements and Home Range of the Gray Squirrel Sciurus Carolinensis, in Two Maryland Woodlots'' Vagn Flyger ("Ecology" Vol. 41, No. 2, April 1960), pp.&nbsp;365–369 Published by: Ecological Society of America<ref>http://www.jstor.org/stable/1930232</ref>
* ''The utilization of nesting boxes by gray squirrels'' Vagn Flyger and H. Rebecca Cooper, (Issue 331 of Contribution, University of Maryland, College Park. Natural Resources Institute, 1967)
* ''The 1968 squirrel 'migration' in the eastern United States'' Vagn Flyger (Smithsonian Institution. Center for Short-lived Phenomena Contribution No. 379)<ref>http://www.myoutbox.net/flyger.htm</ref>
* ''Uroporphyrinogen III Cosynthetase Activity in the Fox Squirrel'' (Sciurus niger) Ephraim Y. Levin and Vagn Flyger, (Science 1, October 1971: Vol. 174 no. 4004 pp.&nbsp;59–60 {{doi|10.1126/science.174.4004.59}} 1971)
* ''Erythropoietic Porphyria of the Fox Squirrel Sciurus niger''  Ephraim Yale Levin & Vagn Flyger (Published in Volume 52, Issue 1, Journal of Clinical Investigation, 1973; 52(1):96–105 {{doi|10.1172/JCI107178}})
* ''Distribution of the Delmarva Fox Squirrel (Sciurus niger cinereus) in Maryland''. 1974 Gary Taylor, Vagn Flyger, (Chesapeake Science, Vol. 15, No. 1 Mar., 1974, pp.&nbsp;59–60)

== References ==
{{reflist}}

{{DEFAULTSORT:Flyger, Vagn F.}}
[[Category:Articles created via the Article Wizard]]
[[Category:Danish zoologists]]
[[Category:1922 births]]
[[Category:2006 deaths]]
[[Category:Cornell University alumni]]