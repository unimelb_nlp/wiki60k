{{italic title}}
[[Image:Cage-sonatas-preparation-1.jpg|318px|thumb|right|A piano prepared for a performance of ''Sonatas and Interludes'']]
'''''Sonatas and Interludes''''' is a collection of twenty pieces for [[prepared piano]] by American [[avant-garde]] [[composer]] [[John Cage]] (1912–1992). It was composed in 1946–48, shortly after Cage's introduction to [[Indian philosophy]] and the teachings of art historian [[Ananda Coomaraswamy|Ananda K. Coomaraswamy]], both of which became major influences on the composer's later work. Significantly more complex than his other works for prepared piano,<ref name="ishii">Reiko Ishii. ''The Development of Extended Piano Techniques in Twentieth-Century American Music'', pp.&nbsp;38–41. The Florida State University, College of Music, 2005. [http://etd.lib.fsu.edu/theses/available/etd-11142005-064729/ Available online] (accessed December 29, 2007).</ref><ref name="Pritchett, p.&nbsp;32.">Pritchett, p.&nbsp;32.</ref> ''Sonatas and Interludes'' is generally recognized as one of Cage's finest achievements.<ref>Pritchett, Grove: "''Sonatas and Interludes'' is a truly exceptional work and may be said to mark the real start of Cage's mature compositional life."</ref><ref>Nicholls, p.&nbsp;80: "Most critics agree that ''Sonatas and Interludes'' (1946&ndash;48) is the finest composition of Cage's early period."</ref>

The cycle consists of sixteen sonatas (thirteen of which are cast in [[binary form]], the remaining three in ternary form) and four more freely structured interludes. The aim of the pieces is to express the eight permanent emotions of the [[Rasa (aesthetics)|rasa]] Indian tradition. In ''Sonatas and Interludes'', Cage elevated his technique of rhythmic proportions to a new level of complexity.<ref name="Pritchett, p.&nbsp;32."/> In each sonata a short sequence of natural numbers and fractions defines the structure of the work and that of its parts, informing structures as localized as individual melodic lines.<ref name="Cage, p.&nbsp;57.">Cage, p.&nbsp;57.</ref>

== History of composition ==
Cage underwent an artistic crisis in the early 1940s.<ref>Nicholls, p.&nbsp;81. In addition to problems in his artistic life, Cage also had to deal with personal troubles: a divorce from his wife of ten years in 1945, and a change in his sexuality that had happened by 1942. For details on this topic, see Marjorie Perloff, Charles Junkerman: ''John Cage: Composed in America'', University of Chicago Press, 1994. ISBN 0-226-66057-5.</ref> His compositions were rarely accepted by the public,<ref>Pritchett, p.&nbsp;36.</ref> and he grew more and more disillusioned with the idea of art as communication. He later gave an account of the reasons: "Frequently I misunderstood what another composer was saying simply because I had little understanding of his language. And I found other people misunderstanding what I myself was saying when I was saying something pointed and direct".<ref name="cott">John Cage interview by Johnatan Cott, 1963. Available as streaming audio at [https://archive.org/details/CottInterviews the Internet Archive] (accessed December 29, 2007).</ref> At the beginning of 1946, Cage met Gita Sarabhai, an Indian musician who came to the United States concerned about Western influence on the music of her country. Sarabhai wanted to spend several months in the US, studying Western music. She took lessons in [[counterpoint]] and contemporary music with Cage, who offered to teach her for free if she taught him about [[Music of India|Indian music]] in return.<ref>Cage, p.&nbsp;127.</ref> Sarabhai agreed and through her Cage became acquainted with Indian music and philosophy. The purpose of music, according to Sarabhai's teacher in India, was "to sober and quiet the mind, thus rendering it susceptible to divine influences",<ref name="cott"/><ref>Cage, pp.&nbsp;158, 226.</ref> and this definition became one of the cornerstones of Cage's view on music and art in general.

At around the same time, Cage began studying the writings of the Indian art historian [[Ananda Coomaraswamy|Ananda K. Coomaraswamy]]. Among the ideas that influenced Cage was the description of the [[Rasa (aesthetics)|rasa]] aesthetic and of its eight "permanent emotions". These emotions are divided into two groups: four white (humor, wonder, erotic, and heroic—"accepting one's experience", in Cage's words<ref name="cott"/>) and four black (anger, fear, disgust, and sorrow). They are the first eight of the ''[[Navras|navarasas]]'' or ''navrasas'' ("nine emotions"), and they have a common tendency towards the ninth of the ''navarasas'': tranquility.<ref>Cage quoted in Kostelanetz, p.&nbsp;67.</ref> Cage never specified which of the pieces relate to which emotions, or whether there even exists such direct correspondence between them.<ref>Pritchett, p.&nbsp;30.</ref> He mentioned, though, that the "pieces with bell-like sounds suggest Europe and others with a drum-like resonance suggest the East".<ref name="booklet">Cage, booklet text for Ajemian's recording of the cycle: ''John Cage: Sonatas and Interludes'', Composers Recordings Inc. CRI 700 (reissue).</ref> (A short excerpt from Sonata II, which is clearly inspired by Eastern music: {{Audio|Cage-sonata02-tilbury.ogg|listen}}.)<ref>Perry, p.&nbsp;48.</ref> Cage also stated that Sonata XVI, the last of the cycle ({{Audio|Cage-sonata16-hinterhauser.ogg|listen}}), is "clearly European. It was the signature of a composer from the West."<ref>Kostelanetz, p.&nbsp;67.</ref>

[[Image:Sonatas-interludes-cage&ajemian.jpg|250px|thumb|right|John Cage with the pianist Maro Ajemian, to whom he dedicated ''Sonatas and Interludes'']]
Cage started working on the cycle in February 1946, while living in [[New York City]]. The idea of a collection of short pieces was apparently prompted by the poet [[Edwin Denby (poet)|Edwin Denby]], who had remarked that short pieces "can have in them just as much as long pieces can".<ref>Cage quoted in Pritchett, p.&nbsp;29.</ref> The choice of materials and the technique of piano preparation in ''Sonatas and Interludes'' were largely dependent on improvisation: Cage later wrote that the cycle was composed "by playing the piano, listening to differences [and] making a choice".<ref>Cage, pp.&nbsp;19–57, essay ''Composition as Process''. Cage uses the same description of the process several times, but it is unclear whether he refers to piano preparation only or composition as well.</ref> On several accounts he offered a poetic metaphor for this process, comparing it with collecting shells while walking along a beach.<ref name="booklet" /><ref>Cage, p.&nbsp;25.</ref> Work on the project was interrupted in early 1947, when Cage made a break to compose ''[[The Seasons (Cage)|The Seasons]]'', a ballet in one act also inspired by ideas from Indian philosophy. Immediately after ''The Seasons'' Cage returned to ''Sonatas and Interludes'', and by March 1948 it was completed.

Cage dedicated ''Sonatas and Interludes'' to [[Maro Ajemian]], a pianist and friend. Ajemian performed the work many times since 1949, including one of the first performances of the complete cycle on January 12, 1949, in [[Carnegie Hall]]. On many other occasions in the late 1940s and early 1950s, Cage performed it himself. Critical reaction was uneven,<ref>{{cite news
|publisher=[[Time magazine]]
|date=January 24, 1949
|title=Sonata for Bolt & Screw
|url=http://www.time.com/time/magazine/article/0,9171,799692,00.html 
|accessdate=2007-12-15}}</ref> but mostly positive,<ref>''The New York Times'' review of the Carnegie Hall concert quoted in Pritchett, p.&nbsp;35: "The work "left one with the feeling that Mr. Cage is one of this country's finest composers."</ref> and the success of ''Sonatas and Interludes'' led to a grant from the [[John Simon Guggenheim Memorial Foundation|Guggenheim Foundation]], which Cage received in 1949, allowing him to make a six-month trip to Europe. There he met [[Olivier Messiaen]], who helped organize a performance of the work for his students in Paris on June 7, 1949; and he befriended [[Pierre Boulez]], who became an early admirer of the work and wrote a lecture about it for the June 17, 1949 performance at the salon of Suzanne Tézenas in Paris.<ref name="boulezlecture">For details and the full text of Boulez' lecture, see Pierre Boulez, John Cage, Robert Samuels, Jean-Jacques Nattiez, ''The Boulez-Cage Correspondence'', translated by Robert Samuels, Cambridge University Press, 1995. ISBN 0-521-48558-4.</ref> While still living in Paris, Cage began writing ''[[String Quartet in Four Parts]]'', yet another work influenced by Indian philosophy.

== Analysis ==

=== Piano preparation ===
[[Image:Sonatas-interludes-table.gif|300px|thumb|right|Part of the table of preparations of ''Sonatas and Interludes'']]
In the text accompanying the first recording of ''Sonatas and Interludes'', Cage specifically stated that the use of preparations is not a criticism of the instrument, but a simple practical measure.<ref name="booklet" /> Cage started composing for [[prepared piano]] in 1940, when he wrote a piece called ''Bacchanale'' for a dance by [[Syvilla Fort]], and by 1946 had already composed a large number of works for the instrument. However, in ''Sonatas and Interludes'' the preparation is very complex, more so than in any of the earlier pieces.<ref name="ishii" /> Forty-five notes are prepared, mostly using screws and various types of bolts, but also with fifteen pieces of rubber, four pieces of plastic, several [[Nut (hardware)|nuts]] and one eraser.<ref>Table of preparations in Edition Peters, 6755. Copyright 1960 by Henmar Press.</ref> It takes about two or three hours to prepare a piano for performance. Despite the detailed instructions, any preparation is bound to be different from any other, and Cage himself suggested that there is no strict plan to adhere to: "if you enjoy playing the ''Sonatas and Interludes'' then do it so that it seems right to you".<ref name="booklet" />

For the most part Cage avoids using lower registers of the piano, and much of the music's melodic foreground lies in the [[soprano]] range. Of the forty-five prepared notes, only three belong to the three lowest octaves below F#3: D3, D2 and D1. Furthermore, D2 is prepared in such a way that the resulting sound has the frequency of a D4 (resulting in two variants of D4 available, one more prepared than the other).<ref name="Perry, p.&nbsp;39.">Perry, p.&nbsp;39.</ref> The portion of the keyboard above F#3 is divided into roughly three registers: low, middle, and high. The low register has the heaviest preparation, and the high register the lightest.<ref name="ishii" /> Different methods are used: certain notes produce sounds that retain the original [[Audio frequency|frequency]] and a pianistic character; others become drum-like sounds, detuned versions of the original notes, or metallic, rattling sounds that have no sense of the [[fundamental frequency]] at all. The use of the [[soft pedal]], which makes the hammers strike only two of the three strings of each note (or one, for notes with only two strings), complicates the matter further. For example, the note C5 is a metallic sound with no fundamental discernible when the soft pedal is depressed, but it sounds fairly normal if the pedal is released.<ref name="Perry, p.&nbsp;39."/> It appears that Cage was fully aware of the implications of this: certain sonatas feature interplay between two versions of one note, others place special emphasis on particular notes, and still others are very dependent on particular note combinations.<ref>See examples of analysis in Perry, and in Nicholls, pp.&nbsp;83–84. Nicholls also provides examples of sonatas connected by the use of identical note combinations.</ref>

[[Image:Sonatas-interludes-sonata15.gif|700px|thumb|center|'''Example 1'''. The first five bars of Sonata XV in Cage's [[Calligraphy|calligraphic]] notation. (The definitive recording by Maro Ajemian, supervised by the composer: {{Audio|Cage-sonata15-ajemian.ogg|listen}}.) All three major groups of sounds used in ''Sonatas and Interludes'' are present: the heavily prepared notes with no fundamental frequency discernible (for instance, D6), lightly prepared notes (G{{music|flat}}-4), and non-prepared notes (G5). The [[Una corda|soft pedal]] is depressed throughout—otherwise some of the sounds would be different.]]

=== Structure ===
The cycle comprises sixteen sonatas and four interludes, arranged symmetrically. Four groups of four sonatas each are separated by interludes in the following way: 
:''Sonatas&nbsp;I–IV&nbsp;&nbsp;&nbsp;&nbsp;Interlude&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;Sonatas&nbsp;V–VIII''
:''Interludes&nbsp;2–3''
:''Sonatas&nbsp;IX–XII&nbsp;&nbsp;&nbsp;&nbsp;Interlude&nbsp;4&nbsp;&nbsp;&nbsp;&nbsp;Sonatas&nbsp;XIII–XVI''
Cage refers to his pieces as  [[sonata]] in the sense that these works are cast in the form that early classical keyboard sonatas (such as those of [[Domenico Scarlatti|Scarlatti]]) were: AABB. The works are not cast in the later [[sonata form]] which is far more elaborate. The only exceptions are sonatas IX–XI, which feature three sections: prelude, interlude, and postlude.<ref name="booklet" /> Sonatas XIV–XV follow the AABB scheme but are paired and given the joint title ''Gemini—after the work of Richard Lippold'', referring to a sculpture by [[Richard Lippold|Lippold]]. The interludes, on the other hand, do not have a unifying scheme. The first two are free-form movements, whereas interludes 3 and 4 have a four-section structure with repeats for each section.<ref name="Pritchett, p.&nbsp;32."/>

{| class="wikitable" style="margin: 1em auto 1em auto"
|+ '''Forms and structural proportions in ''Sonatas and Interludes'''''<ref>Pritchett, pp.&nbsp;29–33.</ref><ref>Score: Edition Peters, 6755. Copyright 1960 by Henmar Press.</ref>
! Piece
! Form
! Proportions
! Unit size (bars)
|-
| Sonata I
| AABB
| 1¼, ¾, 1¼, ¾, 1½, 1½
| 7
|-
| Sonata II
| AABB
| 1½, 1½, 2⅜, 2⅜
| 7¾
|-
| Sonata III
| AABB
| 1, 1, 3¼, 3¼
| 8½
|-
| Sonata IV
| AABB
| 3, 3, 2, 2
| 10
|-
| Interlude 1
| N/A (no repeats)
| 1½, 1½, 2, 1½, 1½, 2
| 10
|-
| Sonata V
| AABB
| 2, 2, 2½, 2½
| 9
|-
| Sonata VI
| AABB
| 2⅔, 2⅔, ⅓, ⅓
| 6
|-
| Sonata VII
| AABB
| 2, 2, 1, 1
| 6
|-
| Sonata VIII
| AABB
| 2, 2, 1½, 1½
| 7
|-
| Interlude 2
| N/A (no repeats)
| N/A (unclear)
| 8
|-
| Interlude 3
| AABBCCDD
| 1¼, 1¼, 1, 1, ¾, ¾, ½, ½
| 7
|-
| Sonata IX
| ABBCC
| 1, 2, 2, 1½, 1½
| 8
|-
| Sonata X
| AABBC
| 1, 1, 1, 1, 2
| 6
|-
| Sonata XI
| AABCC
| 2, 2, 3, 1½, 1½
| 10
|-
| Sonata XII
| AABB
| 2, 2, 2½, 2½
| 9
|-
| Interlude 4
| AABBCCDD
| 1, 1, 1, 1, 1, 1, 1¼, 1¼
| 8½
|-
| Sonata XIII
| AABB
| 1½, 1½, 3½, 3½ 
| 10
|-
| Sonata XIV
| AABB
| 2, 2, 3, 3
| 10
|-
| Sonata XV
| AABB
| 2, 2, 3, 3
| 10
|-
| Sonata XVI
| AABB
| 3½, 3½, 1½, 1½
| 10
|}

[[Image:Sonatas-interludes-sonata3graph.gif|350px|thumb|right|'''Example 2'''. Rhythmic proportions in Sonata III]]
The main technique Cage used for composition is that of nested proportions: an arbitrary sequence of numbers defines the structure of a piece on both the macroscopic and the microscopic level, so that the larger parts of each piece are in the same relation to the whole as the smaller parts are to a single unit of it.<ref name="Cage, p.&nbsp;57."/> For instance, the proportion for Sonata III is 1, 1, 3¼, 3¼ (in [[whole note]]s), and a unit here is equal to 8½ [[Bar (music)|bars]] (the end of a unit is marked with a double [[bar (music)|barline]] in the score, unless it coincides with the end of a section). The structure of this sonata is AABB. Section A consists of a single unit, composed according to the given proportion: correlation on the microscopic level. A is repeated, and AA forms the first part of the proportion on the macroscopic level: 1, 1. B consists of three units and an appendix of ¼ of a unit. B is also repeated, and BB gives the second half of the proportion: 3¼, 3¼. Therefore, AABB has proportions 1, 1, 3¼, 3¼: correlation on the macroscopic level. The musical phrases within each unit are also governed by the same proportion.<ref>Pritchett, p.&nbsp;33. Analogous explanations are given in Nicholls pp.&nbsp;82–83, Perry pp.&nbsp;41–42, and many other sources.</ref> See ''Example 2'' for a graph of the structure of Sonata III.

The proportions were chosen arbitrarily in all but the last four pieces in the cycle: sonatas XIII and XVI use symmetrical proportions, and sonatas XIV and XV share the 2, 2, 3, 3 proportion. This symmetry, and the adherence of all four sonatas to the ten-bar unit, were explained by Cage as an expression of tranquility.<ref>Cage quoted in Pritchett, p.&nbsp;30.</ref> The complexity of proportions prompted Cage to use asymmetric musical phrases and somewhat frequent changes of [[time signature]] to achieve both microscopic and macroscopic correlation. For example, unit length of 8½ in the first section of Sonata III is achieved by using six bars in 2/2 time and two in 5/4 (rather than eight bars in 2/2 and one in 1/2). In many sonatas the microstructure—how the melodic lines are constructed—deviates slightly from the pre-defined proportion.<ref name="Nicholls, p.&nbsp;83.">Nicholls, p.&nbsp;83.</ref>

Cage had frequently used the nested proportions technique and its variations before, most notably in ''[[Construction (Cage)|First Construction (in Metal)]]'' (1939), which was the first piece to use it,<ref>Pritchett, Grove.</ref> and numerous [[Works for prepared piano by John Cage|dance-related works for prepared piano]]. In ''Sonatas and Interludes'', however, the proportions are more complex, partly because fractions are used.<ref name="Pritchett, p.&nbsp;32."/><ref name="Nicholls, p.&nbsp;83."/> In his 1949 lecture on ''Sonatas and Interludes'' [[Pierre Boulez]] specifically emphasized the connection between tradition and innovation in ''Sonatas and Interludes'': "The structure of these sonatas brings together a pre-Classical structure and a rhythmic structure which belong to two entirely different worlds."<ref name="boulezlecture" />

== Recordings ==
''Sonatas and Interludes'' has been recorded many times, both in its complete form and in parts. This list is organized chronologically and presents only the complete recordings. Years of recording are given, not years of release. Catalogue numbers are indicated for the latest available CD versions. For the complete discography with reissues and partial recordings listed, see the link to the John Cage database below.
* [[Maro Ajemian]] – 1951, [[Dial Records (1946)|Dial Records]] 20–21. Reissued in the 1960s, Composers Recordings Inc. CRI 700. Reissued on CD, él records ACMEM88CD<ref>{{cite web |url=http://www.cherryred.co.uk/el/artists/johncage.htm |publisher=El Records |title=John Cage: Sonatas And Interludes For Prepared Piano |accessdate=2007-12-14 |archiveurl = https://web.archive.org/web/20080111011253/http://www.cherryred.co.uk/el/artists/johncage.htm |archivedate = January 11, 2008|deadurl=yes}}</ref>
* [[Yuji Takahashi]]: 
** 1965, Fylkingen Records FYCD 1010 (mono)<ref>{{cite web |url=http://www.fylkingen.se/node/244 |title=Yuji Takahashi plays John Cage - Sonatas and Interludes Vol.1-2 |publisher= Fylkingen |accessdate=2007-12-14 }}</ref>
** 1975, Denon COCO 70757 (stereo, digital)
* John Damgaard – 1971, Membran Quadromania 222190-444 (4CD, incl. many other works)
* [[John Tilbury]] – 1974, Explore Records EXP0004<ref>{{cite web |url=http://www.explorerecords.com/downloadshop/release.php?catno=0004 |publisher=explore records |title= Cage: Sonatas & Interludes |accessdate=2007-12-14 }}</ref>
* Joshua Pierce: 
** 1975, Wergo WER 60156-50<ref>{{cite web |url=http://www.wergo.de/shop/de_DE/3/show,93468.html|publisher=Wergo |title= CDs - Sonatas & Interludes |accessdate=2007-12-14 }}</ref>
** 1988, Newport Classic NPD 85526
** 1999, Ants Records AG 06 (2CD, live recording)
** 2000, SoLyd Records SLR 0303 (live recording)
* Gérard Frémy – 1980, Pianovox PIA 521-2, Ogam Records 488004-2, Etcetera Records KTC 2001<ref>{{cite news
|first=Bernard 
|last=Holland
|title= What Hath Liszt Wrought? Jean Sibelius, Henri Dutilleux and John Cage.
|work=[[The New York Times]]
|date=December 13, 1987 
 |url=https://query.nytimes.com/gst/fullpage.html?res=9B0DE3DB1F3CF930A25751C1A961948260&sec=&spon=&pagewanted=all
|accessdate=2007-12-15}}</ref>
* Nada Kolundžija – c. 1981, Diskos LPD-930 (2LP)<ref>{{cite web |url=http://www.nadakolundzija.info/recordings.html|publisher=NadaKolundzija.info |title=Nada Kolundžija - Recordings |accessdate=2007-12-14 }}</ref>
* Darryl Rosenberg – c. 1986, VQR Digital VQR 2001<ref>Ev Grimes, John Cage. ''Conversation with American Composers'', Music Educators Journal, Vol. 73, No. 3 (Nov., 1986), pp.&nbsp;47–49,&nbsp;58–59.</ref><ref>{{cite news
|first=Anthony 
|last=Tommasini
|title= The Zest of the Uninteresting
|work=[[The New York Times]]
|date= April 23, 1989 
|url=https://query.nytimes.com/gst/fullpage.html?res=950DEFDF103BF930A15757C0A96F948260&sec=&spon=&pagewanted=print
|accessdate=2007-12-15}}</ref> (LP)
* [[Mario Bertoncini]] – 1991, released 2001 as Edition RZ 20001 (Parallele 20001)<ref>{{cite web |url=http://edition.r2010.de/edition/223-106,1,0.html |title=John Cage: Sonatas and Interludes  |publisher= Edition RZ |accessdate=2007-12-14 | archiveurl = https://web.archive.org/web/20070216230644/http://edition.r2010.de/edition/223-106,1,0.html| archivedate = February 16, 2007}}</ref>
* [[Nigel Butterley]] – 1992, Tall Poppies TP025<ref>{{cite web |url=http://members.iinet.net.au/~tallpoppies/index.cgi?tp=cd&val=25 |publisher=Tall Poppies Records|title=Music by John Cage for Prepared Piano |accessdate=2007-12-14 }}</ref>
* Herbert Henck – 1993, ECM New Series 1842<ref>{{cite web |url=http://www.ecm-records.com/Catalogue/New_Series/1800/1842.php?stickylabel=1 |title=ECM New Series 1842 |publisher= ECM Records |accessdate=2007-12-14 }}</ref> (2CD, incl. Henck's ''Festeburger Fantasien'')
* Louis Goldstein – 1994, Greensye Music 4794<ref name="goldstein">Larry J. Solomon. Review of Sonatas and Interludes for prepared piano by John Cage, performances by Louis Goldstein and Julia Steinberg. American Music, Vol. 16, No. 1 (Spring, 1998), pp.&nbsp;130–133.</ref> (incl. ''Dream'')
* Philipp Vandré – 1994, Mode 50<ref name="mode">{{cite web |url=http://www.moderecords.com/catalog/050cage.html |title=John Cage - Piano Works 2 |publisher= Mode Records |accessdate=2007-12-14 }}</ref> (according to the liner notes,<ref name="mode" /> this is the first recording made on a [[Steinway & Sons|Steinway]] "O"-type baby grand piano, the model Cage originally composed the piece on)
* Julie Steinberg – 1995, Music & Arts 937<ref>{{cite web |url=http://www.musicandarts.com/CDpages/CD4937.html |publisher=Musicandarts.com |title=Cage's Prepared Piano Revisited: Julie Steinberg, piano, on Music & Arts CD-937 |accessdate=2007-12-14 }}</ref>
* [[Markus Hinterhäuser]] – 1996, Col Legno WWE 1CD 20001<ref>{{cite web |url=http://www.col-legno.com |title= col legno|publisher= col legno.com|accessdate=2007-12-14 }}</ref>
* [[Steffen Schleiermacher]] – 1996, MDG 613 0781-2<ref>{{cite web |url=http://www.mdg.de/titel/0781.htm |title=John Cage (1912-1992) - Complete Piano Music Vol. 1|publisher= MDG Recording|accessdate=2007-12-14 }}</ref> (3CD, part of ''John Cage: Complete Piano Works'' 18CD series)
* Aleck Karis – 1997, Bridge 9081 A/B<ref>{{cite web |url=http://www.bridgerecords.com/pages/catalog/9081.htm |title=John Cage: Sonatas and Interludes|publisher=Bridge Records Inc.|accessdate=2007-12-14 | archiveurl = https://web.archive.org/web/20071021075057/http://www.bridgerecords.com/pages/catalog/9081.htm| archivedate = October 21, 2007}}</ref> (2CD, incl. Cage's lecture ''Composition in Retrospect'')
* Jean Pierre Dupuy – 1997, Stradivarius 33422
* [[Boris Berman]] – 1998, Naxos 8.559042<ref>{{cite web |url=http://www.naxos.com/catalogue/item.asp?item_code=8.559042 |title=CAGE: Sonatas and Interludes for Prepared Piano|publisher=naxos.com |accessdate=2007-12-14 }}</ref> or Naxos 8.554345<ref>{{cite web |url=http://www.naxos.com/catalogue/item.asp?item_code=8.554345 |title=CAGE: Sonatas and Interludes for Prepared Piano |publisher=naxos.com|accessdate=2007-12-14 }}</ref>
* [[Joanna MacGregor]] – 1998, SoundCircus SC 003<ref>{{cite web |url=http://www.soundcircus.com/releases/sc003/sc003.htm |title=Perilous Night |publisher= SoundCircus|accessdate=2007-12-14 }}</ref> (2CD, includes miscellaneous other works by Cage and other composers)
* Giancarlo Cardini – 1999, Materiali Sonori<ref>{{cite web |url=http://www.matson.it/recordings.php |title=Materiali Sonori Produzioni |publisher= Materiali Sonori|accessdate=2015-02-22}}</ref><ref>{{cite web|url=https://itunes.apple.com/gb/album/sonatas-interludes-for-prepared/id144915389|title=Sonatas and Interludes for Prepared Piano|publisher=iTunes|accessdate=2015-02-22}}</ref>
* Kumi Wakao – 1999, Mesostics MESCD-0011<ref>{{cite web |url=http://www.d6.dion.ne.jp/~kwakao/label.html |publisher=d6.dion.ne.jp |title=Mesostics releases |accessdate=2007-12-14 |archiveurl = https://web.archive.org/web/20070625174624/http://www.d6.dion.ne.jp/~kwakao/label.html |archivedate = June 25, 2007|deadurl=yes}}</ref>
* Tim Ovens – c. 2002, CordAria CACD 566 (incl. a multimedia CD)
* [[Margaret Leng Tan]] – 2003, Mode 158<ref>{{cite web |url=http://www.moderecords.com/catalog/158cage.html |title=John Cage - Volume 34: The Piano Works 7 |publisher= Mode Records|accessdate=2007-12-14 }}</ref> (CD and DVD, incl. many other works and several [[Documentary film|documentaries]])
* Nora Skuta – 2004, Hevhetia Records HV 0011-2-131<ref>{{cite web |url=http://www.hevhetia.com/detail.php?cid=17 |publisher= Hevhetia s.r.o. hudobné vydavatel'stvo a distribúcia |title= John Cage - Nora Skuta: Sonatas & Interludes |accessdate=2007-12-14 |archiveurl = https://web.archive.org/web/20070808220001/http://www.hevhetia.com/detail.php?cid=17 |archivedate = August 8, 2007|deadurl=yes}}</ref> (SACD)
* Giancarlo Simonacci – 2005, Brilliant Classics 8189 (3CD, part of ''Complete Music for Prepared Piano'')
* Antonis Anissegos – 2014, WERGO (WER 67822)

== See also ==
* [[List of compositions by John Cage]]
* [[Works for prepared piano by John Cage]]
* ''[[String Quartet in Four Parts]]''
* ''[[The Seasons (Cage)|The Seasons]]''
* ''[[Makrokosmos]]'', several collections of works for prepared piano or played using extended technique (or both), by [[George Crumb]]

== Notes ==
{{Reflist|30em}}

== References and further reading ==

=== Books ===
* John Cage. ''Silence: Lectures and Writings'', Wesleyan Paperback, 1973 (first edition 1961). ISBN 0-8195-6028-6
* Richard Kostelanetz. ''Conversing with John Cage'', Routledge, 2003. ISBN 0-415-93792-2
* David Nicholls. ''[[Cambridge Companions to Music|The Cambridge Companion to John Cage]]'', Cambridge University Press, 2002. ISBN 0-521-78968-0
* James Pritchett. ''The Music of John Cage'', Cambridge University Press, 1993. ISBN 0-521-56544-8
* {{GroveOnline|title=John Cage|author=James Pritchett|author2=Laura Kuhn|access-date=15 December 2006}}

=== Dissertations and articles ===
* E.S. Baumgartner. ''Sonatas and Interludes, by John Cage: A Structural Analysis'', Mills College, 1994.
* Gregory Jay Clough. ''Sonatas and Interludes for Prepared Piano (1946–48) by John Cage: An Analytical Basis for Interpretation'', MM University of Arkansas, Fayetteville, 1968.
* Jeffrey Perry. "Cage's Sonatas and Interludes for prepared piano: performance, hearing and analysis", ''Music Theory Spectrum'', Spring 2005, Vol. 27, No. 1, pp.&nbsp;35–66.

== External links ==
* [http://johncage.info/workscage/sonatasinterludes.html ''Sonatas and Interludes'' data sheet] and [http://johncage.info/cdlabels/discographylist2.html#son48 discography] at the John Cage database DEAD LINKS
* [http://www.musicdepartment.org.uk/year13/john_cage_revision.htm John Cage Revision Notes: Sonatas and Interludes for prepared piano], a technical essay on the work DEAD LINK
* [http://www.rosewhitemusic.com/cage/texts/sixviews.html James Pritchett: "Six views of the Sonatas and interludes"]

=== Media ===
* [https://www.youtube.com/watch?v=VYsx5Di3bso Sonata V performed by Bobby Mitchell], YouTube link
* [http://www.soundnet.org/concerts/mov_refs/2002.shtml#cage 4'33" and Sonatas and Interludes for prepared piano performed by James Tenney] at SASSAS sound, concert archive (streaming QuickTime format).

{{John Cage}}
{{Sonatas}}
{{featured article}}

[[Category:Compositions by John Cage]]