<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = F.VII
  |image =Mittelholzer-fokker.jpg
  |caption = [[Swissair]] Fokker F.VIIb-3 m (CH-192) piloted by [[Walter Mittelholzer]] in [[Kassala]] (Sudan), February 1934.
}}{{Infobox Aircraft Type
  |type = Passenger & military transport
  |manufacturer =[[Fokker]]
  |designer =
  |first flight = 24 November 1924 
  |introduced = 1925
  |retired = 
  |status = 
  |primary user = [[SABENA]]
  |more users = [[KLM]]<br/>[[Polish Air Force]]<br/>''[[LOT Polish Airlines|Polskie Linie Lotnicze LOT]]''
  |produced = 1925-1932
  |number built =
  |unit cost =
  |developed from = [[Fokker F.V]]
  |variants with their own articles = [[Fokker F.10]]
}}
|}
[[File:Fokker FVIIa3m wiki.jpg|thumb|The ''[[Josephine Ford (aircraft)|Josephine Ford]]'' at [[The Henry Ford]].]]
[[File:Southern cross.jpg|thumb|The ''[[Southern Cross (aircraft)|Southern Cross]]'' in 1943.]]
[[File:Ad Astra Aero - Fokker F-VII-B 3-m (CH190).jpg|thumb|Fokker V.VIIb 3-m (CH-190) operated by [[Ad Astra Aero]]]]
[[File:COLLECTIE TROPENMUSEUM Vliegveld Tjililitan met benzinestation bij Meester Cornelis Batavia Java TMnr 10010739.jpg|thumb|Fokker F.VII at [[Halim Perdanakusuma Airport|Tjililitan airfield]], Batavia (now [[Jakarta]], [[Indonesia]])]]
[[File:Fokker C-2 090713-F-1234K-067.jpg|thumb|right|A C-2 of the United States Army]]

The '''Fokker F.VII''', also known as the '''Fokker Trimotor''', was an [[airliner]] produced in the 1920s by the Dutch aircraft manufacturer [[Fokker]], Fokker's American subsidiary [[Atlantic Aircraft|Atlantic Aircraft Corporation]], and other companies under licence.

==Design and development==
The F.VII was designed as a single-engined transport aircraft by [[Walter Rethel]]. Five examples of this model were built for the Dutch airline KLM. One of these planes, registered H-NACC, was used in 1924 for the first flight from the Netherlands to the Dutch East Indies. In 1925, while living in the US, Anthony Fokker heard of the inaugural [[Ford National Reliability Air Tour|Ford Reliability Tour]], which was proposed as a competition for transport aircraft. Fokker had the company's head designer, [[Reinhold Platz]], convert a single-engined F.VII A airliner (a 1924 [[Walter Rethel]] design) to a trimotor configuration, powered by 200&nbsp;hp [[Wright Whirlwind]] radial engines. The resulting aircraft was designated the Fokker F.VII A/3M. Following shipment to the US, it won the Ford Reliability Tour in late 1925. The Trimotor's structure consisted of a fabric-covered steel-tube fuselage and a plywood-skinned wooden wing.<ref>{{cite book | last = Thurston | first = David B. | authorlink = | coauthors = | title = The World's Most Significant and Magnificent Aircraft: Evolution of the Modern Airplane | publisher = SAE | series = | volume = | edition = | year = 2000 | location = | pages = 127–128 | language = | url = | doi = | id = | isbn = 978-0-7680-0537-0 | mr = | zbl = | jfm = }}</ref>

The Fokker F.VII B/3M had a slightly increased wing area over the A/3M, with power increased to 220&nbsp;hp per engine, while the [[Fokker F.10|F.10]] was slightly enlarged, carrying 12 passengers in an enclosed cabin. The aircraft became popularly known as the '''Fokker Trimotor'''.<ref>[http://www.allstar.fiu.edu/aero/FokkF_VII.htm "Fokker F-VII."] ''Aeronautics Learning Laboratory.'' Retrieved: 20 December 2010.</ref>

==Operational history==
The eight- to 12-passenger Fokker was the aircraft of choice for many early airlines, both in Europe and the Americas. Along with the similar [[Ford Trimotor]], itself having an all-metal design based on the World War I aircraft designs of German engineer [[Hugo Junkers]], it dominated the American market in the late 1920s. However, the popularity of the Fokker quickly came to an end after the 1931 death of [[University of Notre Dame|Notre Dame]] [[American football|football]] coach [[Knute Rockne]] in the crash of [[TWA Flight 599]], a Fokker F.10. The subsequent investigation, which revealed problems with the Fokker's [[plywood]]-[[laminate]] construction, resulted in the temporary banning of the aircraft from commercial flights, more stringent requirements for its maintenance, and the rise of all-metal aircraft such as the [[Boeing 247]] and [[Douglas DC-2]].<ref>Mola, Roger. [http://www.centennialofflight.net/essay/Government_Role/accident_invest/POL17.htm "CAA investigation of Flight 599."] ''centennialofflight.net,'' 2003. Retrieved: 20 December 2010.</ref>

===Pioneers and explorers===
The F.VII was used by many explorers and aviation pioneers, including:
* [[Richard E. Byrd]] claimed to have flown over the [[North Pole]] in the Fokker F.VIIa/3m ''[[Josephine Ford (aircraft)|Josephine Ford]]'' on 9 May 1926, a few days before [[Roald Amundsen]] accomplished the feat in the [[airship]] ''[[Norge (airship)|Norge]]''.<ref name="famfok">Baaker, Leo. [http://www.leob.nl/chrono.htm "Famous Fokker Flights."] ''tiscali.nl.''Retrieved: 20 December 2010.</ref>
* Two lieutenants of the [[United States Army Air Corps]], [[Lester Maitland]] and [[Albert Hegenberger]], made the first transpacific flight from the continental [[United States]] to [[Hawaii]] (c. 2,400&nbsp;mi/3,862&nbsp;km) in the Atlantic-Fokker C-2 ''[[Bird of Paradise (aircraft)|Bird of Paradise]]'' on 28–29 June 1927.<ref name="famfok" />
* Also on 29 June 1927, Richard E. Byrd, [[Bernt Balchen]] and two others flew the first official [[transatlantic flight|transatlantic]] [[airmail]] in the civilian-owned C-2 ''[[America (airplane)|America]]'' (NX206), crash-landing off the coast of [[France]] on 1 July.<ref>[http://www.check-six.com/Crash_Sites/America-NX206.htm "The Trans-Atlantic Flight of the 'America'."] ''check-six.com,'' 19 October 2010. Retrieved: 20 December 2010.</ref>
* [[Lieutenant Colonel]] [[Frederick F. Minchin|'Dan' Minchin]], [[Captain (land)|Captain]] Leslie Hamilton and [[Princess Anne of Löwenstein-Wertheim-Freudenberg]] attempted on 31 August 1927 to become the first aviators to cross the Atlantic from east to west using a Fokker F.VIIa named the ''[[St. Raphael (aircraft)|St. Raphael]]''. Their fate remains unknown.
* [[James DeWitt Hill]] and [[Lloyd W. Bertaud]] made a failed attempt to fly from New York to Rome in F.VIIa ''[[Old Glory (aircraft)|Old Glory]]'' when they and the aircraft were lost in the North Atlantic 7 September 1927.
* [[Sir Charles Kingsford Smith]]'s F.VIIb/3m ''[[Southern Cross (aircraft)|Southern Cross]]'' was the first aircraft to cross the [[Pacific Ocean|Pacific]] from the United States to [[Australia]] in June 1928, and the first to cross the [[Tasman Sea]], flying from Australia to [[New Zealand]] and back in September of that year.<ref>Naughton, Russell. [http://www.ctie.monash.edu.au/hargrave/k-smith.html "The Pioneers - Charles Kingsford Smith."] ''monash.edu.au.'' Retrieved: 20 December 2010.</ref>
* [[Amelia Earhart]] became the first woman to fly across the Atlantic on 17 June 1928, as a passenger aboard the Fokker F.VIIb/3m ''Friendship''.<ref name="famfok" />
* A group of [[United States Army Air Corps|U. S. Army Air Corps]] flyers, led by then-[[Major]] [[Carl Andrew Spaatz|Carl Spaatz]], set an endurance record of over 150 hours with the [[Question Mark (airplane)|''Question Mark'']], a Fokker C-2A over Los Angeles on 1 to 7 January 1929. The purpose of this mission was to set a flight endurance record using [[aerial refueling]].<ref>[https://www.airforcehistory.hq.af.mil/PopTopics/refueling.htm "Question Mark."] ''USAF Historical Studies Office''. Retrieved: 20 December 2010.</ref>

==Variants==
;F.VII: Single-engined transport aircraft, powered by a 360&nbsp;hp (268.5&nbsp;kW) [[Rolls-Royce Eagle]] piston engine, accommodation for two crew and six passengers; five built.
;F.VIIa (F.VIIa/1m): Single-engined transport aircraft, slightly larger than F.VII with new undercarriage and wing. Flown on 12 March 1925. First aircraft had {{convert|420|hp|abbr=on}} V-12 [[Liberty L-12|Packard Liberty]] engine but remaining 39 F.VIIa had mostly radial [[Bristol Jupiter]] or [[Pratt & Whitney Wasp]] engines.
;F.VIIa/3m: Version with two additional underwing engines, flown on 4 September 1925. The first two aircraft were identical to the F.VIIa. From the third aircraft, the fuselage was 31&nbsp;in (80&nbsp;cm) longer and was powered by 200&nbsp;hp (149&nbsp;kW) [[Wright J-4 Whirlwind]] radial engines. Probably only 18 were built while many F.VIIa were upgraded to the F.VIIa/3m standard.
First two Fokker F.VIIa were converted into three-engined transport aircraft.
;F.VIIb/3m: Main production version with greater span; 154 built including built under licence.
;F.9: American built version of the Fokker F.VIIBb/3m; built by the Atlantic Aircraft Corporation in the United States.
;[[Fokker F.10]]: Enlarged version of the Fokker F.VII airliner, able to carry up to 12 passengers; built by the Atlantic Aircraft Corporation in the United States.
;C-2: Military transport version of the Fokker F.9, powered by three 220&nbsp;hp (164&nbsp;kW) Wright J-5 radial piston engines, accommodation for two pilots and ten passengers; three built in 1926 for the US Army Air Corps.
;C-2A: Military transport version for the US Army Air Corps, with greater wingspan, powered by three 220&nbsp;hp (164&nbsp;kW) Wright J-5 radial piston engines, accommodation for two pilots and ten passengers; eight built in 1928.
;XC-7: One C-2A fitted with three 330&nbsp;hp (246&nbsp;kW) Wright J-6-9 radial piston engines. Redesignated C-7 when four C-2A examples were similarly reconfigured.
;C-7: Military transport conversion of C-2A for the US Army Air Corps by re-engining with 300&nbsp;hp (220&nbsp;kW) Wright R-975 engines. XC-7 prototype and four C-2As redesignated in 1931. 
;C-7A: Six new production C-7 (Wright R-975) aircraft with larger wings, new vertical fin design, and fuselages patterned after the commercial [[Fokker F.10|F.10A]].
;XLB-2: Experimental light bomber version of the C-7, powered by three 410&nbsp;hp (306&nbsp;kW) Pratt & Whitney R-1380 radial piston engines; one built.
;TA-1: Military transport version of the US Navy and Marine Corps; three built.
;TA-2: Military transport version for the US Navy; three built.
;TA-3: Military transport version for the US Navy, powered by three Wright J-6 radial piston engines; one built.
;RA-1: Redesignation of the TA-1.
;RA-2: Redesignation of the TA-2.
;RA-3: Redesignation of the TA-3.

===Licensed copies===
*[[SABCA]], 29 aircraft built.
*[[Avia]], 18 aircraft built.
* Three aircraft built in Italy as the IMAM Ro.10, powered by 3x215hp Alfa Romeo Lynx engines. 3 built for operation by [[Avio Linee Italiane]] and [[Ala Littoria]].
*[[Plage i Laśkiewicz]]. Between 1929 and 1930 11 passenger and 20 domestically developed (by [[Jerzy Rudlicki]]) bomber aircraft.
* Three aircraft built in Spain.
*[[Avro]], 14 aircraft known as [[Avro 618 Ten]].
*[[Atlantic Aircraft Corporation]]

==Operators==

===Civilian operators===
;{{BEL}}
*[[SABENA]] operated 28 aircraft.
;{{DNK}}
*''[[Det Danske Luftfartselskab]]'' operated three F.VIIa aircraft.
;{{FRA}}
*[[CIDNA]] operated seven F.VIIa aircraft.
*[[Société de Transport Aérien Rapide|STAR]] operated one F.VIIa aircraft.
;{{flag|Italy|1861}}
*[[Avio Linee Italiane]]
*[[Ala Littoria]]
;{{flag|Hungary|1920}}
*[[Malert]] operated two F.VIIa aircraft.
;{{NLD}}
*[[KLM]] received all five F.VII aircraft and 15 F.VIIas.
;{{POL}}
*[[Aero (Polish airline)|Aero]] operated six F.VIIa aircraft for a short period in 1928. Since 1 January 1929, all aircraft were handed over to PLL LOT airline.
*[[LOT Polish Airlines|''Polskie Linie Lotnicze'' LOT]] operated six F.VIIas and 13 F.VIIb/3ms between 1929 and 1939.
;{{POR}}
*[[Aero Portuguesa]] operated one F.VIIb-3m aircraft.
;{{ESP}}
*[[CLASSA]]
*[[LAPE]]
;{{SUI}}
*[[Ad Astra Aero]] at least one F.VIIb-3m
*[[Swissair]] operated one F.VIIa and eight F.VIIb-3m aircraft.
;{{flag|United States|1912}}
*American Airways, which later became [[American Airlines]].
*[[TWA]]
*[[Pan Am]] operated F.VIIb/3ms aircraft.

===Military operators===
;{{BEL}}
*[[Belgian Air Force]]
;{{flag|Belgian Congo}}
*[[Force Publique]]
;{{flag|Independent State of Croatia}}
*''[[Air Force of the Independent State of Croatia|Zrakoplovstvo Nezavisne Države Hrvatske]]''
;{{CZS}}
*[[Czechoslovak Air Force]]
;{{FIN}}
*[[Finnish Air Force]] operated one F.VIIa.
;{{FRA}}
*[[French Air Force]] - 5 F.VIIa/3m and 2 F.VII/3m aircraft, impressed into military service in 1939/1940.
;{{flag|Kingdom of Hungary}}
*Royal [[Hungarian Air Force]]
;{{flag|Italy|1861}}
*[[Regia Aeronautica]]
;{{NLD}}
*[[Royal Netherlands Air Force]] received three bomber F.VIIa/3m aircraft.
;{{POL}}
*[[Polish Air Force]] operated 21 F.VIIb/3m (20 of them were licence-built) aircraft as bombers and transports between 1929 and 1939.
**[[1 Pułk Lotniczy|1 ''Pułk Lotniczy'']]
***[[211 Eskadra Bombowa|211 ''Eskadra Bombowa'']]
***[[212 Eskadra Bombowa|212 ''Eskadra Bombowa'']]
***[[213 Eskadra Bombowa|213 ''Eskadra Bombowa'']]
;{{flag|Spanish Republic|1928}}
* [[Spanish Republican Air Force]], operated four aircraft in the squadron of the [[Spanish Sahara|Sahara]] and other two in [[Madrid]].
;{{flag|United States|1912}}
*[[United States Army Air Corps]] designations include Atlantic-Fokker '''C-2''', '''C-5''' and '''C-7'''.<ref>Baugher, Joe. [http://www.joebaugher.com/ustransports/cdesig.html "Cargo Aircraft Designations."] ''US transports,'' 11 August 2007. Retrieved: 20 December 2010.</ref>
*[[United States Navy]] and [[United States Marine Corps]], originally designated '''TA''' then '''RA'''<ref>Painter, K.M.  [https://books.google.com/books?id=0N8DAAAAMBAJ&pg=PA762&dq=Junkers+stratosphere&hl=en&ei=9voOTe-zH8qUnQeRrdHyDQ&sa=X&oi=book_result&ct=result&resnum=5&ved=0CDYQ6AEwBDgK#v=onepage&q=Junkers%20stratosphere&f=true "Help From The Skies."] ''Popular Mechanics,'' November 1929.</ref>
;{{flag|Kingdom of Yugoslavia}}
*[[Yugoslav Royal Air Force]]

==Accidents and incidents==
*On June 21, 1926, a [[KLM]] F.VII (H-NACL) force-landed at [[Seabrook, Kent|Seabrook Beach]], [[Sandgate, Kent|Sandgate]] near Hythe, Kent, due to fuel exhaustion caused by pilot error; all five on board survived, but the aircraft was written off.
*On July 9, 1926, a KLM F.VII (H-NACC) struck ground in fog at [[Wolvertem]], Belgium, killing both pilots.
*At 9:44pm on 31 August 1927 the oil tanker [[SS Josiah Macy]] reported the last known sighting of the F.VIIa ''[[St. Raphael (aircraft)|St. Raphael]]'' on a trans-atlantic attempt from [[Upavon]], England to [[Ottawa|Ottawa, Ontario]], Canada, piloted by [[Leslie Hamilton]] and [[Frederick F. Minchin]], with [[Princess Anne of Löwenstein-Wertheim-Freudenberg]] as passenger.
*On 7 September 1927, ''[[Old Glory (aircraft)|Old Glory]]'' disappeared with [[Lloyd W. Bertaud]] and [[J. D. Hill]] at the controls of an attempted transatlantic flight from [[Old Orchard Beach]], [[Maine]] to [[Rome]], [[Italy]]. The flight's last known location was in the [[North Atlantic]], 960&nbsp;km East of [[Cape Race]], [[Newfoundland]]. 
*On September 17, 1927, a Reynolds Airways F.VII (''C776'') crashed at Dunellen, New Jersey due to loss of control following engine failure, killing seven of 12 on board. The aircraft was formerly operated by KLM and had been imported to the United States. 
*On July 4, 1928, [[Alfred Lowenstein]] disappeared during a flight over the [[English Channel]] in unknown circumstances.
*On August 15, 1928, a [[Pan American World Airways|Pan Am]] F.VIIa/3m (NC53, ''General Machado'') ditched in the Gulf of Mexico off Egmont Key, Florida.
*On September 11, 1930, a [[Sabena]] F.VII (OO-AIN) crashed on climbout from [[Croydon Airport]] due to an in-flight fire, killing both pilots.
*On December 6, 1931, a KLM F.VIIb/3m (PH-AFO) crashed at [[Bangkok]] after failing to take off, killing five of seven on board.
*On April 3, 1940, a [[British Overseas Airways Corporation|BOAC]] Avro 618 Ten (G-AASP, ''Hercules'') crashed on takeoff from Cairo; there were no casualties, but the aircraft was written off.
*On May 10, 1940, a KLM F.VII (PH-ACJ) was destroyed on the ground at [[Schiphol Airport]] by the Luftwaffe during the German invasion of the Netherlands.

==Specifications==

===Fokker F.VIIb/3m; Atlantic-Fokker C-2A===
{{aircraft specifications
|plane or copter?=plane
|jet or prop?=prop
|ref=<ref name="aerofav">[http://aerofavourites.nl/fokf7-04.htm "Fokker."] ''Aero Favourites.'' Retrieved: 20 December 2010.</ref>
|crew= 2
|capacity= 8 passengers
|length main=47 ft 11 in 	
|length alt=14.60 m
|span main=71 ft 2 in	
|span alt=21.70 m
|height main=12 ft 8 in 	
|height alt=3.90 m
|area main=	
|area alt=
|empty weight main=6,725 lb 	
|empty weight alt=3,050 kg
|loaded weight main=11,570 lb 	
|loaded weight alt=5,200 kg
|engine (prop)=	[[Wright J-5 Whirlwind]]
|type of prop=radial engines
|number of props=3
|power main=220 hp 	
|power alt=164 kW
|cruise speed main=92 kn 	
|cruise speed alt=170 km/h
|armament=
}}

==See also==
{{aircontent
|related=
*[[Avro 618 Ten]]
*[[Fokker F.10]]
|similar aircraft=
*[[Ford Trimotor]]
|lists=
* [[List of aircraft of World War II]]
*[[List of aircraft of the Finnish Air Force]]
*[[List of military aircraft of the United States]]
*[[List of military aircraft of the United States (naval)]]
*[[List of civil aircraft]]
|see also=
* [[Alfred Loewenstein]], a Belgian financier who fell mysteriously to his death from his private Fokker F.VII
}}

==References==
{{Commons category}}
;Notes
{{Reflist}}
;Bibliography
{{Refbegin}}
* Bowers, Peter and Ernest McDowell. ''Triplanes: A Pictorial History of the World's Triplanes and Multiplanes''.  St. Paul, Minnesota: Motorbooks International, 1993. ISBN 0-87938-614-2.
* Dierikx, Marc. ''Fokker: A Transatlantic Biography''. Washington, DC: Smithsonian Institution Press, 1997. ISBN 1-56098-735-9.
* Molson, K.M. ''Pioneering in Canadian Air Transport''. Winnipeg: James Richardson & Sons, Ltd., 1974. ISBN 0-919212-39-5.
* Nevin, David. ''The Pathfinders (The Epic of Flight Series)''. Alexandria, Virginia: Time-Life Books, 1980. ISBN 0-8094-3256-0.
* Postma, Thijs. ''Fokker: Aircraft Builders to the World''. London: Jane's, 1979. ISBN 0-7106-0059-3.
* Weyl, A.R. ''Fokker: The Creative Years''. London: Putnam, 1965.
{{Refend}}

{{Fokker aircraft}}
{{IMAM aircraft}}
{{USAF transports}}
{{lone designation|system=[[United States Navy]]/[[United States Marine Corps|USMC]] transport designations pre-1931|designation=TA}}
{{USN transports}}

[[Category:Fokker aircraft|F 07]]
[[Category:Dutch airliners 1920–1929]]
[[Category:United States airliners 1920–1929]]
[[Category:Trimotors]]
[[Category:High-wing aircraft]]
[[Category:Atlantic Aircraft aircraft|F.VII]]