{{Orphan|date=May 2014}}

'''Investment outsourcing''' is the process whereby institutional investors and high-net-worth families engage a third party to manage all or a portion of their investment portfolio.  This arrangement can include functions such as establishing the [[asset allocation]], selecting investment managers, implementing portfolio decisions (both strategic and tactical), providing on-going oversight, performing risk management and other areas of portfolio management.

Outsourced investment management is a large and growing market segment with over half a trillion dollars currently managed by outsourced managers.<ref>“The New Gate Keepers,” Casey Quirk Associates, 2008 Retrieved 2012-07-31</ref><ref>"Strong Growth in Investment Outsourcing Predicted," Pensions & Investments, July 11, 2011 Retrieved 2012-07-31</ref> According to a survey of outsourcers by aiCIO magazine, the volume of outsourced assets increased 200% between 2007 and 2011.<ref name="ai-ciodigital">"2012 Investment Outsourcing Buyers Guide," p 54, available at http://www.ai-ciodigital.com/ai-cio/20120102#pg54</ref>

The outsourcing trend began with smaller institutions that could not or did not want to build an internal investment team.  According to a study by the Family Wealth Alliance, approximately four in ten wealthy families have outsourced discretionary investment authority.  The trend is even clearer among families with less than $500 million in assets where two-thirds have outsourced management.<ref>Inaugural 2009 Single-Family Office Study, Family Wealth Alliance Retrieved 2012-07-31</ref>  Similarly, just 11% of college endowments between $100 and $500 million internally manage their portfolios and instead rely on outsourced managers.<ref>2009 NACUBO/Commonfund Study of Endowment Results</ref> Increasingly, however, it is not just small investors seeking to outsource.  There has been a marked increase in the number of multibillion outsourcing engagements since the [[2008 financial crisis]] as firms grapple with increasing complexity and the need for better risk management.<ref>“CIO in a Box Gaining Popularity: Business Booms as More Institutions Outsource Investment Management,” Pensions & Investments, June 14, 2010 Retrieved 2012-07-31</ref>

Investment outsourcing goes by many names including "fiduciary management", “outsourced chief investment officer”, “outsourced CIO,” “OCIO,”, “CIO in a box,” and “implemented consulting.”

==Outsourcing Models==

=== Background ===

Donald E. Callaghan and [[Jonathan Hirtle]], [[Chief executive officer|Chief Executive Officer]] for Hirtle, Callaghan & Co.,<ref name = leadership>{{cite web|title=Who We Are: Leadership|url=http://www.hirtlecallaghan.com/who-we-are/leadership/jonathan-hirtle.aspx|publisher=Hirtle Callaghan & Co.|accessdate=15 April 2014}}</ref> pioneered the outsourced [[Chief investment officer|Chief Investment Officer]] (OCIO) model, which serves family groups and organizations that do not employ fully staffed investment departments.<ref name = whoweare>{{cite web|title=Who We Are|url=http://www.hirtlecallaghan.com/who-we-are/at-a-glance.aspx|publisher=Hirtle Callaghan & Co.|accessdate=15 April 2014}}</ref><ref name = williamson>{{cite web|last=Williamson|first=Christine|title=Hirtle Callaghan co-founder was an early leader in developing investment outsourcing|url=http://www.pionline.com/article/20110822/PRINT/308229960/hirtle-callaghan-co-founder-was-an-early-leader-in-developing-investment-outsourcing|publisher=Pensions & Investments|accessdate=15 April 2014}}</ref> For his OCIO innovations, Hirtle has been dubbed the “[[Oracle]] of [[Outsourcing|Outsource]].”<ref>{{cite web|last=Rabicoff|first=Richard|title=Hirtle Hedge Fund Hurtles to $37.8M|url=http://philadelphia.citybizlist.com/article/hirtle-hedge-fund-hurtles-378m-cbl-0|publisher=citybizlist|accessdate=15 April 2014}}</ref>

=== Current OCIO models ===

The model by which outsourced CIOs service clients is still evolving in this nascent business.  The most common model is to outsource all decision making including asset allocation, manager selection and monitoring.  The OCIO reports back to the client but the burden is largely lifted from the client and placed on the new provider.  Among OCIOs utilizing this approach, there is a “continuum of outsourcing approaches and providers: manager-of-manager programs; funds-of-funds; former CIOs offering a diversified model portfolio.”<ref>Urie, Sandra. “CIO Outsourcing: Questions Investment Committees Need to Ask” Forbes, June 30, 2010.  Viewed June 20, 2013.</ref>  The common thread amongst these approaches is the use of commingled funds or model portfolios which creates economies of scale for the OCIO.

A different model is pursued by a small subset of the OCIO universe.  The members of this group work alongside the client’s staff – not as a replacement to them.  According to investment industry newsletter FundFire, “An increasing number of CIOs see outsourcing not as a threat to their job, but as a source of complementary expertise and advice, as well as investment opportunities.”  Like much of the nomenclature around the OCIO business, this more customized, bespoke solution does not yet have a widely recognized name.  Fiduciary Research (FRC), an OCIO who oversees about $9 billion on behalf of a small list of pension fund clients, calls itself an iCIO for integrated chief investment office.<ref>Sturock, Tim. “Institutions Split Over Hybrid CIO/OCIO Model” FundFire, June 5, 2013.</ref>

==Fees==

The fees charged by outsourced managers vary widely based upon a number of factors but are generally between 30 and 100 bps with some firms also charging an incentive fee in addition to their base management fee.<ref>See page 27 of "San Jose Police and Fire Board Retreat", presentation by investment committee chairman Vincent Sunzeri, available at http://www.sjretirement.com/uploads/PF/3_aitemPFSpecMtgMar12.pdf</ref><ref>Consulting firm RV Kuhns reports fees range from 30-100 bps based on a number of factors, "Considering the OCIO Option: Not an Everyday Decision for Fiduciaries", February 2013, available online at http://www.iiforums.com/cfr/presentations/cfr13-ocio.pdf, viewed April 24, 2013</ref><ref>Additional fee data was obtained from various sources including filings with the Securities & Exchange Commission.</ref> In a panel discussion organized by ''CIO'' magazine, representatives for three different firms discussing the rise in investment outsourcing gave ranges of between 25 and 65 bps with some of the difference explained by the use of internal or proprietary funds or a purely open-architecture approach.<ref>{{cite web|title=Outsourced CIOs: The Interrogation|website=https://www.seic.com/enUS/institutions/13624.htm|publisher=aiCIO|accessdate=October 6, 2014}}</ref>  In some quarters discontent has been voiced about investment outsourcing firms which allocate client capital to proprietary products charging a second layer of fees.<ref>Jones, Lillian.  "Outsourced CIO Services a Big Fat Scam?"  Emerging Manager Blog, May 25, 2011.  Viewed March 12, 2013.</ref>

==Investment Outsourcers By Assets==

The next two tables use data from Charles Skorina & Company’s “Ultimate Outsourcer’s List” and is divided into two categories: 1) outsourcers who advise on a diverse set of mandates including pensions, non-profits and family trusts and 2) university endowment-style outsource managers.<ref>The Skorina Letter, No 29, August 11, 2011 Retrieved 2012-07-31</ref> The list curated by Skorina & Company is more expansive than the other most commonly referenced list prepared by ''Pensions & Investments'' ''(P&I)''.  The P&I list counts only the top 25 and is available only to paid subscribers.

{| class="wikitable sortable"
|-
! Firm Name !! Assets Under Management
|-
| [[Russell Investments]] || $96.0 billion
|-
| [[SEI Investments Company|SEI]] || $64.7 billion
|-
| [[Mercer (consulting firm)|Mercer-Hammond]] || $51.0 billion
|-
| [[Towers Watson]] || $50.0 billion
|-
| [[State Street Bank and Trust Company|State Street Global Advisors]] || $41.0 billion
|-
| BlackRock || $55.0 billion
|-
| [[Northern Trust]] || $20.0 billion
|-
|- [[Angeles Investment Advisors]] || $1.4 billion

| Clearbrook Financial || $30.0 billion
|-
| Strategic Investment Group || $21.2 billion
|-
| PFM Asset Management || $1.6 billion
|-
| Hall Capital Partners || $26.0 billion
|-
| [[Morgan Stanley|Morgan Stanley (Graystone Consulting)]] || $22.0 billion
|-
| Hirtle Callaghan || $22.0 billion
|-
| [[Partners Capital|Partners Capital Investment Group]] || $16.2 billion 
|-
| Hewitt EnnisKnupp || $13.4 billion
|-
| [[UBS]] || $12.8 billion
|-
| Rogerscasey || $12.5 billion
|-
| [[Wilshire Associates]] || $9.7 billion 
|-
| Pyramis Global || $6.7 billion 
|-
| Spruce Private Investors || $3.2 billion 
|-
| Spider Management Company || $3.1 billion 
|-
| NEPC || $2.2 billion 
|-
| Angeles Investment Advisors || $2.1 billion 
|-
| Fund Evaluation Group || $1.9 billion 
|-
| Wurts & Associates || $1.2 billion 
|-
| Post Rock Advisors || $1.1 billion 
|-
| Balentine || $1.4 billion 
|-
| [[Goldman Sachs]] || $25.0 billion
|-
| [[JPMorgan Chase]] || $23.0 billion
|-
| Athena Capital Advisors || $2.8 billion
|-
| BNY Mellon Asset Management || unknown
|-
| Fortress Partners Funds || $1.8 billion
|-
| New Providence Asset Management || $2.4 billion
|-
| Rocaton Investment Advisors || $0.5 billion
|-
| Rockefeller Financial || $10.0 billion
|-
| Fiduciary Research (FRC) || $9.0 billion
|-
| Salient Partners || $17.0 billion
|-
| Fiduciary Asset Management || $2.0 billion 
|-
| SECOR Asset Management || $7.5 billion
|-
| Pacific Global Advisors || $20.0 billion
|}

==Consultants Push into Outsourcing==

In the general investment outsourcing area, where outsourcers serve a variety of clients including [[pension funds]], [[non-profit]]s and [[High-net-worth individual|families]], the business grew out of investment consulting firms and to a lesser extent [[financial services|large banks]] and [[money manager]]s.  In the early 1970s, as corporate pension plans were implementing [[ERISA]], investment consulting firms came of age.  Large organizations often had both in-house investment staffs and investment committees populated by sophisticated professionals. These groups wanted to make all important decisions themselves, and therefore they looked to the consultants only for recommendations, especially regarding [[asset allocation]] and manager selection.  “In other words, investment consulting began, and remained for many decades, a non-discretionary, advisory-only model for delivering investment advice. The consultants made recommendations, but the clients decided what to do.”<ref>“The Outsourced CIO Model,” Whitepaper No. 49, Greycourt, 2010</ref> Northern Trust was the first firm to offer an outsourced model in 1979 followed a year later by Russell Investments.<ref name="ai-ciodigital" />

Investment consultants have been eager to exit the “largely unsatisfactory, low-margin, and litigious business” and “keen to move into what is perceived as a significantly better business model, where fees are asset-based.” These consulting firms typically have large staffs and experience consulting on a large (multibillion dollar) scale.  On the other hand, “critics assert that consultants suffer from a lack of competence as portfolio managers, and that their experience with the administration of a fund is nonexistent.”<ref>“Consultant Corner: Investment Outsourcing,” aiCIO magazine, June 13, 2011 Retrieved 2012-07-31</ref> Others have raised concerns about the possibility for conflicts of interests when consultants attempt to move into the outsourced CIO business.<ref>“In Search of an Honest Man,” Balentine, November 2010</ref><ref>“Institutional Investors Using Boutique Consultants for Advice on outsourcing: Because large consulting firms offer outsourcing, most are referring clients to smaller peers for searches”, Pensions & Investments, May 14, 2012 Retrieved 2012-07-31</ref> British advisory firm Allenbridge Epic, after surveying 11 OCIOs, concluded that several lacked transparency and suffered from conflicts of interest.<ref>Sullivan, Ruth.  "Fiduciary management minefield for trustees," Financial Times, June 9, 2013.  Viewed August 27, 2013.</ref>

==Pension Investment Outsourcing==

One of the first institutional scale investment outsourcing arrangements occurred in 1986.  Three investment managers who had worked for [[IBM|IBM’s]] [[pension fund]] left the company and formed SASCO Capital.  They managed a portion of the IBM pension plan but IBM also kept a large portion of the assets and investment team in-house.  Another early example of pension fund investment outsourcing involved [[General Dynamics]].  In November 1994 General Dynamics became the first client when its treasurer formed a new outsourced investment firm, Fiduciary Asset Management.  At the time of General Dynamics’ initial allocation, this was a $2.5 billion mandate.<ref>“General Dynamics Spin-Off, Plan Sponsor magazine, November 1994 Retrieved 2012-07-31</ref> In 2001 another of the early large pension outsourcing arrangements occurred when [[Xerox]] hired [[General Motors|General Motors Asset Management]] (GMAM) to manage its $4 billion defined pension plan.  This move was prompted, in part, by the departure of two senior Xerox professionals to run other investment plans.  As part of the arrangement, Xerox’s chief investment officer joined GMAM.  Following GM’s bankruptcy, in 2010 Xerox took back the management of its pension assets.<ref>Second Client: Xerox signs on with GM Asset,” Pensions & Investments, October 29, 2001 Retrieved 2012-07-31</ref>

Other early examples of pension fund investment outsourcing included [[Brown & Williamson]], [[Weyerhauser]], [[Maytag]], ADM and [[Kmart|K-Mart]].<ref>"Upfront: Anatomy of an Outsourcing Deal,” Plan Sponsor magazine, January 2002 Retrieved 2012-07-31</ref> 

In the [[United States]], movement toward outsourcing by state, county or municipal pension plans has been slower than corporate pensions.  This is despite the difficulty that many public pension funds have recruiting and retaining qualified staffs, particularly in [[alternative investments|alternatives]] such as [[hedge fund]]s, [[private equity]] and real assets such as [[real estate]], [[fossil fuel|oil & gas]] and [[Infrastructure Leasing & Financial Services|infrastructure]].<ref>“Public Fund Salaries Remain Skimpy,” Plan Sponsor magazine, November 1998 Retrieved 2012-07-31</ref>

The most high profile outsourcing attempt involves the San Diego County Employees Retirement Association.  The Board of Trustees hired Lee Partridge, who created a firm called Integrity Capital, as their "outsourced CIO" in 2009.  The pension system kept three staff investment officers, who were in charge of the funds private market investments which totaled 30% of the fund.  This arrangement sparked controversy among certain groups in San Diego.<ref>“Bonanza prospects fade Conflict-of-interest warning a bump on road for Integrity Capital founder's push to manage outsourced assets,” Pensions & Investments, April 5, 2010 Retrieved 2012-07-31</ref>  In 2010, Integrity Capital merged with [[Salient Partners]], and Partridge and Salient continued to fulfill the original contract.<ref>{{cite web|title=Salient Partners Acquires Integrity Capital|url=http://www.businesswire.com/news/home/20101119005970/en/Salient-Partners-Acquires-Integrity-Capital-Services-LLC#.U5uuwvldWSo|publisher=Business Wire|accessdate=June 12, 2014}}</ref> In 2012, Salient won a competitive bid to continue to oversee the $9.1 billion plan.  On June 5, 2014, the entirety of the San Diego investment program, including the private market assets that had been managed by investment staff, was outsourced to Salient.<ref>{{cite web|title=SDCERA outsources entire portfolio|url=http://www.sdcera.org/PDF/SDCERA_Board_implements_cost_saving_adjustments.pdf}}</ref>

In June 2012, the San Jose Police & Fire Department Retirement Plan and the San Jose Federated City Employees' Retirement System were known to be exploring outsourcing the management of their plans.  Internal research prepared for San Jose estimated that an outsourcer would charge between 30 bps and 100 bps to manage their $2.646 billion plan but thought it would be recovered through higher returns.<ref>"San Jose Police and Fire Board Retreat", March 7, 2012.  Presentation by investment committee chairman Vincent Sunzeri, posted online at http://www.sjretirement.com/uploads/PF/3_aitemPFSpecMtgMar12.pdf</ref>

The table below presents a subsection of the list of outsourcers narrowed to focus on firms that specialize in pension outsourcing.

{| class="wikitable sortable"
|-
! Firm Name !! Assets Under Management
|-
| Fiduciary Research (FRC) || $9.0 billion
|-
| Salient Partners || $9.1 billion
|-
| Fiduciary Asset Management || $2.0 billion 
|}

Beyond outsourcing, pension fund investment teams have occasionally grown to take in other, unrelated, assets.  Previously the Weyerhauser/Morgan Stanley and General Motors Asset Management examples were noted.  Other examples include [[Owens-Illinois]], [[General Electric]] and [[American Airlines]].  In 1987 Owens-Illinois’ investment management subsidiary, Harbor Capital Advisors, launched five [[mutual fund]]s targeting defined contribution plans.  American Airlines’ AMR Investment Services (now called Beacon Funds) has its own family of mutual fund products.  GE Investments offers a variety of funds to others in addition to managing [[mutual fund]]s. Fiduciary Asset Management, one of the early total plan outsourcers, instead of taking on additional outsourcing clients, created a team that invests in [[master limited partnership]] and now manages nearly $4 billion in external capital.  Fiduciary Asset Management was previously owned by [[Piper Jaffray]].<ref>"Angell to buy FAMCO from Piper Jaffray," St Louis Business Journal, March 8, 2013.  Viewed March 12, 2013.</ref>

==Endowment Model Outsourcing==

The outsourced investment model for universities, foundations and non-profits was pioneered by Commonfund.  In 1971 Commonfund was created as a result of a [[Ford Foundation]] funded study which found that small endowments chronically underperformed.<ref>Commonfund website</ref>  More recently, the success of university endowments such as [[Yale University|Yale]], [[Harvard University|Harvard]] and [[Stanford University|Stanford]] to generate attractive returns prompted a number of former endowment investment teams to open outsourced CIO firms.  These firms include (alphabetically): Agility ([[University of Colorado]] / [[University of Texas]]), Global Endowment Management ([[Duke Endowment]] and [[Duke University]]), Investure ([[University of Virginia]]), Lowe, Brockenbrough & Co's Bespoke Strategies ([[University of Richmond]] / [[University of Texas]]), Makena Capital ([[Stanford University]]), Morgan Creek ([[University of North Carolina at Chapel Hill]]), Spider Management ([[University of Richmond]]), TIAA-CREF ([[Rice University]]) and Verger Capital Management (Wake Forest).

The fees charged by endowment style outsourced managers vary widely based upon a number of factors including size and complexity.  In general, fees charged by endowment outsourcing managers tend to be higher than generalist outsourcers.  The primary difference stems from the experience and expertise that endowment-style firms usually have in areas such as hedge funds, private equity and real assets.  Outsourcers that grew out of consulting firms as a general rule tend to be weak in these areas.   Endowment outsourcers fees range between 35 bps and 150 bps with most groups charging an incentive fee as well.<ref>Fee data obtained from various sources including filings with the Securities & Exchange Commission.</ref>  One endowment outsourcer, Global Endowment Management (GEM), quoted their fees as follows: 0.75% on the first $100 million, 0.675% on the next $150 million, and 0.60% on all amounts above $250 million.  GEM also levies a 5% incentive fee above an 8% return net of all manager fees with a high water mark. CommonFund (and its affiliates CommonFund Capital and CommonFund Asset Management), one of the true pioneers in the outsourced endowment model, quote a complex schedule of fees which they summarize this way: "Commonfund Capital’s fees for separate accounts ranges from 50-100 basis points, with varying ranges with respect to the carried interest."  An analysis of CommondFund Asset Management's Form ADV Part 2 filing with the SEC for a hypothetical $1 billion account with a 50% equity allocation, 30% in hedge funds and 20% in fixed income would result in total management fees of 85 basis points plus an unspecified level of carried interest.<ref>Form ADV 2 filed by CommonFund Capital and CommonFund Assetment Management, both dated September 28, 2012.  Viewed May 1, 2013.</ref>

{| class="wikitable sortable"
|-
! Firm Name !! Assets Under Management
|-
| Commonfund || $20.0 billion
|-
| Makena || $16.0 billion
|-
| Partners Capital Investment Group || $16.2 billion
|-
| Morgan Creek || $8.0 billion
|-
| Investure || $9.5 billion
|-
| The Investment Fund for Foundations || $9.2 billion
|-
| Cambridge Associates || $7.3 billion
|-
| Global Endowment Management || $6.7 billion
|-
| Agility (Perella Weinberg) || $6.1 billion
|-
| Edgehill Endowment Partners || $4.1 Billion
|-
| HighVista || $3.5 billion
|-
| Cornerstone Partners || $3.0 billion
|-
| Spider Management Company || $3.1 billion
|-
| Covariance Capital Management (TIAA) || $2.4 billion
|-
| New Providence Asset Management || $2.4 billion
|-
| Mangham Partners || $2.3 billion
|-
| Angeles Investment Advisors || $2.1 billion
|-
| Okabena Advisors || $1.3 billion
|-
| Verger Capital Management || $1.3 billion
|-
| Lowe, Brockenbrough & Company (Bespoke Strategies) || $1.2 billion
|}

==References==
<references/>

[[Category:Investment management]]
[[Category:Financial services]]
[[Category:Institutional investors]]
[[Category:Financial markets]]