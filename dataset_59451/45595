{{Use British English|date=March 2014}}
{{Use dmy dates|date=August 2013}}
:''See also other people called [[Tony Snell (disambiguation)]].''
{{Infobox military person
|name=Anthony Noel Snell
|honorific_suffix = {{post-nominals|post-noms=DSO}}
|birth_date={{Birth date|1922|3|19|df=yes}}
|death_date={{Death date and age|2013|8|4|1922|3|19|df=yes}}
|birth_place=[[Tunbridge Wells]], [[Kent]], [[England]], [[United Kingdom of Great Britain and Ireland|UK]]
|death_place=[[British Virgin Islands]]
|image=
|caption=
|nickname=Tony
|allegiance={{flagu|United Kingdom}}
|serviceyears=
|rank=[[Flight Lieutenant]]
|branch={{air force|United Kingdom}}
|servicenumber=119146
|commands=
|unit=[[No. 242 Squadron RAF]]<br>[[No. 504 Squadron RAF]]
|battles=World War II:
*[[North African Campaign]]
*[[Allied invasion of Sicily]]
|awards=[[Distinguished Service Order]] 
|laterwork=
}}
[[Flight Lieutenant]] '''Anthony Noel "Tony" Snell''' {{post-nominals|country=GBR-cats|DSO}} (19 March 1922 &ndash; 4 August 2013), was a British [[Royal Air Force|RAF]] pilot during the [[Second World War]]. He flew in the North African campaign in 1942 and was shot down during the Allied invasion of Sicily in 1943. Initially captured by the Germans he escaped from a firing squad but was recaptured. He again escaped German captivity whilst in Italy and became one of the very few men to be awarded the DSO exclusively for escaping from the enemy.<ref>{{cite web|url=http://www.telegraph.co.uk/news/obituaries/10228970/Flight-Lieutenant-Tony-Snell.html|title=Obituary - Flight Lieutenant Tony Snell|publisher=[[The Daily Telegraph]]|date=7 Aug 2013|accessdate=11 August 2013}}</ref>

==Second World War==
Snell was born in [[Tunbridge Wells]], [[Kent]], in 1933 and attended [[Cheltenham College]]. In November 1940 he volunteered for the RAF and was shipped to the United States for pilot training under the "Arnold" Scheme.<ref>{{cite web|url=http://www.arnold-scheme.org/The%20Arnold%20Register.htm|title=Arnold Scheme 1941-1943|publisher=arnold-scheme.org|date=|accessdate=11 August 2013}}</ref>

===North Africa===
Snell returned to Britain during the summer of 1942 and joined [[No. 242 Squadron RAF]] flying [[Supermarine Spitfire|Spitfire]]s. In October 1942 his unit was transferred to North Africa to provide air cover for the [[Operation Torch]] landings. For the rest of 1942 and into early 1943, Snell's squadron provided air interception and ground attack sorties in support of the [[British First Army]] as it drove towards [[Tunis]].<ref>{{cite web|url=http://www.dailymail.co.uk/news/article-2387120/RAF-Spitfire-pilot-Tony-Snell-fled-German-firing-squad-shot-World-War-Two.html?ito=feeds-newsxml|title=RAF Spitfire pilot fled from German firing squad during extraordinary series of escapes after being shot down in World War Two|work=[[The Daily Mail]]|date=9 Aug 2013|accessdate=11 August 2013}}</ref>

===Sicily===
After the capitulation of Axis forces in May 1943, 242 Squadron was reassigned to [[Malta]] to refit for [[Operation Husky]], the invasion of Sicily. On 10 July 1943, Snell was detailed to provide air cover over the Allied beachhead but was 'bounced' by German [[Messerschmitt 109]] fighters. His Spitfire was hit and he crash landed in enemy territory.

He initially ran into an Italian patrol and then a German one that fired at him. In his attempt to escape from the Germans he found he had hid in a minefield. After slowly finding his way out of the minefield he was captured by the Germans near an airfield. Thinking Snell was a spy they intended to execute him. Snell made another run for it and managed to again escape but was wounded in the right shoulder.

===Escape from captivity===
Weakened by his wound Snell was recaptured but this time was able to prove he was an Allied pilot. Treated for his wounds he was later transferred to a military hospital in [[Lucca]]. After [[Armistice between Italy and Allied armed forces|Italy surrendered in September 1943]] the Germany Army took control of the prison camp, and directed that the prisoners be transferred by train to Germany. While other prisoners on the train distracted the guards Snell, along with Major [[Peter Lewis (British Army officer)|Per Lewis]], escaped through a small window. The following morning they found they were near [[Mantua]]. After a six-day walk they encountered members of the [[Italian resistance movement]] near the small village of Fabrico, who helped them hide in a safe house in [[Modena]] for almost two months.<ref name="tel">{{cite web |url=http://www.telegraph.co.uk/news/obituaries/4218443/Major-Peter-Lewis.html |title=Major Peter Lewis - Telegraph |date=12 Jan 2009 |publisher=The Daily Telegraph |accessdate=2009-01-18}}</ref><ref>{{cite web |url=http://www.nationalarchives.gov.uk/documentsonline/details-result.asp?Edoc_Id=7673717 |title=Recommendations for Honours and Awards (Army)—Image details—Lewis, Peter John; Mentions in Despatches |work=DocumentsOnline |publisher=[[The National Archives]] |accessdate=2009-01-20}}</ref><ref>{{cite book |title=Shot down and on the run |author=[[Air Commodore]] [[Graham Pitchfork]] |publisher=[[The National Archives]] |isbn=978-1-905615-06-3 |year=2007 |pages=194–197}}</ref> With help from the resistance the pair gradually made it to the Swiss border, and they returned to Britain in November 1944. Lewis was [[mentioned in despatches]] on 1 January 1945, and Snell was awarded the [[Distinguished Service Order]], a rare example of the award given for escaping from the enemy.<ref name="tel"/><ref>{{LondonGazette |issue=36961 |supp=yes |startpage=1187 |date=27 February 1945 |accessdate=2009-01-20}}</ref><ref>{{LondonGazette |issue=37666 |supp=yes |startpage=384 |date=23 July 1946 |accessdate=2009-01-20}}</ref>

Snell spent time in hospital recuperating and later joined [[No. 504 Squadron RAF]] flying [[Gloster Meteor]] jet fighters. The squadron was assigned to Germany just after hostilities ceased and Snell remained August 1946 until discharged from the RAF a short while later.

==Post war==
Snell travelled through Africa and met his future wife Jackie in New York in 1964. They travelled together in the United States and [[Mexico]]. He worked as an actor in films and theatre, and also as a songwriter and entertainer throughout his life. He recorded the album ''An Englishman Abroad'' whilst in New York. Returning to the UK in 1966, Snell and Jackie moved to [[Ibiza]] where they ran a charter service on a catamaran they had sailed there. In 1970 he moved to the [[British Virgin Islands]] to run an unsuccessful boat charter company.

His wife had opened a restaurant called ''The Last Resort'' which burnt down. Selling everything left in Ibiza the Snells rebuilt the restaurant into a success. Snell continued to provide entertainment to patrons up until his death.

He wrote an account of his life in ''Spitfire Troubadour''.

==Distinguished Service Order citation==
* 23 July 1946<ref>{{cite news |title=Supplement to the London Gazette |url=http://www.london-gazette.co.uk/issues/37666/supplements/3834/page.pdf  |format=PDF |newspaper=[[The London Gazette]] |publisher=[[His Majesty's Stationery Office]] |issue=36542 |date=26 July 1946 |page=3834 |accessdate=11 August 1943}}</ref> – Flight Lieutenant Anthony Noel Snell (119146), Royal Air Force Volunteer Reserve {{Quotation|On 10th July, 1943, this officer's aircraft was shot down during a patrol over the beach head in Sicily, where allied landings were taking place. He was then engaged in attacking a force of Messerschmidts. The crash landing took place in territory controlled by the enemy, but Flight Lieutenant Snell was able to evade capture and, after dark, endeavoured to return to the beach head. He first encountered a number of Italians whom he bluffed into thinking him a Vichy Frenchman. On escaping from the Italians, he eventually found a road which he recognised from his map. Whilst following this road he was challenged by some Germans who ordered him to put his hands up. Without warning they rolled a grenade at him along the ground. Just in time, he jumped aside and ran back, followed by more grenades; he escaped by taking cover in the scrub, shortly after this, he found himself in a minefield through which he picked his way for half an hour before reaching a track. Following this track, Flight Lieutenant Snell blundered on a German airfield, very near the battle area, where he was captured. The Germans decided to execute him as a spy. Flight Lieutenant Snell was marched out to a small open space and ordered to kneel down. Realising that he was to be shot in cold blood, he did not obey the order, but sprang away as the Germans fired. He was wounded in several places, his right shoulder being smashed. Despite this, Flight Lieutenant Snell evaded his captors, and hid for a time amongst boulders, before making a last attempt to reach the British lines. Owing to the extreme weakness and pain caused by his wounds, this attempt was not successful. Flight Lieutenant Snell was re-captured at dawn after he had collapsed from exhaustion. He was again threatened with execution for spying on the airfield, but finally managed to prove his identity to the satisfaction of the Germans. He was taken to a field hospital where his wounds received attention. Later, Flight Lieutenant Snell was transferred to [[Catania]] and thence to [[Lucca]] by sea. Here he was in hospital for about 2 months, until the Germans, who controlled the prisoners, decided to move them by train to Germany. Although not fully recovered from his wounds, Flight Lieutenant Snell determined to escape during the journey and made all possible preparations for this. In company with an American officer, he jumped from the train while it was passing through a junction, afterwards discovered to be [[Mantova]]. For the next week, they travelled south. During this journey, they had several narrow escapes from the Germans and were assisted by a number of Anti-Fascist Italians. With this help, the officers were able to reach [[Modena]] where they were sheltered by various friendly Italians for several months. It was eventually decided that Flight Lieutenant Snell and his companion should attempt to escape over the [[Alps]] to [[Switzerland]]. They made a long and risky train journey, accompanied by several of their Italian friends, to a small village near the frontier. There they were introduced to two guides who took them over the mountains. After a very long and steep climb, the frontier was reached and crossed. In Switzerland, Flight Lieutenant Snell was interned until October, 1944, when, the American advance reached the Swiss border.}}

==Notes==
{{div col}}
{{Reflist}}
{{div col end}}

{{Authority control}}

{{DEFAULTSORT:Snell, Tony}}
[[Category:1922 births]]
[[Category:2013 deaths]]
[[Category:Royal Air Force officers]]
[[Category:Companions of the Distinguished Service Order]]
[[Category:World War II prisoners of war held by Germany]]
[[Category:British World War II prisoners of war]]
[[Category:Escapees from German detention]]
[[Category:British escapees]]
[[Category:Royal Air Force personnel of World War II]]
[[Category:People educated at Cheltenham College]]
[[Category:People from Royal Tunbridge Wells]]
[[Category:British Virgin Islands people]]