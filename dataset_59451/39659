{{About|the mental health movement|the GROW windowing environment|Graphical ROMable Object Windows|the series of puzzle games|GROW (series)|the coaching method|GROW model}}

[[File:Blue book cover.png|thumb|right|200px|The "Blue Book", ''GROW: World Community Mental Health Movement: The Program of Growth to Maturity'']]'''GROW''' is a [[peer support]] and [[Social work with groups#Mutual aid|mutual-aid]] organization for recovery from, and prevention of, serious [[mental disorder|mental illness]]. GROW was founded in [[Sydney, Australia]] in 1957 by [[Cornelius Keogh|Father Cornelius B. "Con" Keogh]], a [[Roman Catholic Church|Roman Catholic]] [[priest]], and psychiatric patients who sought help with their mental illness in [[Alcoholics Anonymous]] (AA). Consequently, GROW adapted many of AA's principles and practices. As the organization matured, GROW members learned of [[Recovery International]], an organization also created to help people with serious [[mental illness]], and integrated pieces of its [[Will (philosophy)|will]]-training methods.<ref name="KURTZ1987">{{cite journal | last = Kurtz | first = Linda F. |author2=Chambon, Adrienne | year = 1987 | title = Comparison of self-help groups for mental health | journal = Health & Social Work | volume = 12 | issue = 4 | pages = 275–283 | issn = 0360-7283 | oclc = 2198019 | pmid = 3679015}}</ref><ref name="KEOGH1979">{{cite book | author = Keogh, C.B. | authorlink = Cornelius Keogh |url = http://publishing.yudu.com/Freedom/Actiz/GROWcomesofageaceleb/resources/ | title = GROW Comes of Age: A Celebration and a Vision! | year = 1979 | publisher = GROW Publications | location = [[Sydney, Australia]] | isbn = 0-909114-01-3 | oclc = 27588634}}</ref> As of 2005 there were more than 800 GROW groups active worldwide.<ref name="CORRIGAN2005">{{cite journal | last1 = Corrigan | first1 = Patrick | last2 = Slopen |first2 = Natalie |last3 = Garcia |first3 = Gabriela | last4 = Keogh | first4 = Cornelius B. | last5 = Keck | first5 = Lorraine | title = Some Recovery Processes in Mutual-Help Groups for Persons with Mental Illness; II: Qualitative Analysis of Participant Interviews | journal = Community Mental Health Journal | volume = 41 | issue = 6 |date=December 2005 | pages = 721–735 | doi = 10.1007/s10597-005-6429-0 | pmid = 16328585 | issn = 0010-3853 | oclc = 38584278}}</ref> GROW groups are open to anyone who would like to join, though they specifically seek out those who have a history of psychiatric hospitalization or are socioeconomically disadvantaged. Despite the capitalization, GROW is not an acronym.<ref name="RAPPAPORT">{{cite journal | last1 = Rappaport | first1 = J. | last2 = Seidman | first2 = E. | last3 = Toro | first3 = P. A. | last4 = McFadden | first4 = L. S. | last5 = Reischl | first5 = T. M. | last6 =  Robers | first6 = L. J. | last7 = Salem | first7 = D. A. | last8 = Stein | first8 = C. H. | last9 = Zimmerman | first9 = M. A.; |date=Winter 1985 | title = Collaborative research with a mutual help organization | journal = Social Policy | volume = 15 | issue = 3 | pages = 12–24 | issn = 0037-7783 | oclc = 1765683 | pmid = 10270879}}</ref> Much of GROW's initial development was made possible with support from [[Orval Hobart Mowrer]], Reuben F. Scarf, [[W. Clement Stone]] and [[Lions Clubs International]].<ref name="KEOGH1979"/>

== Processes ==
: ''For more details on this topic, see [[Self-help groups for mental health#Group processes|Self-help groups for mental health: Group processes]]

GROW's literature includes the Twelve Stages of Decline, which indicate that emotional illness begins with self-centeredness, and the Twelve Steps of Personal Growth, a blend of AA's [[Twelve Steps]] and [[Will (philosophy)|will]]-training methods from [[Recovery International]]. GROW members view recovery as an ongoing life process rather than an outcome and are expected to continue following the Steps after completing them in order to maintain their [[mental health]].<ref name="KURTZ1987"/><ref name="CLAY2005">{{cite book | last = Clay | first = Sally | title = On Our Own, Together: Peer Programs for People with Mental Illness | year = 2005 | chapter = Chapter 7: GROW in Illinois | pages = 141–158 | isbn = 0-8265-1466-9 | oclc = 56050965 | location = [[Nashville, Tennessee]] | publisher = [[Vanderbilt University]] Press | url = http://www.sallyclay.net/z.together/excerptchap7.pdf|format=PDF}}</ref><ref name="BLUEBOOK">{{cite book | author = GROW | title = GROW: World Community Mental Health Movement: The Program of Growth to Maturity | publisher = GROW Publications | location = Sydney, Australia | oclc = 66288113 | year = 1983 | url = https://books.google.com/books?id=3ClHAAAAMAAJ}}</ref> 
{{Col-begin}}
{{Col-2}}
'''The Twelve Stages of Decline'''
# We gave too much importance to ourselves and our feelings.
# We grew inattentive to God's presence and providence and God's natural order in our lives.
# We let competitive motives, in our dealings with others, prevail over our common personal welfare.
# We expressed our suppressed certain feelings against the better judgment of conscience or sound advice.
# We began thinking in isolation from others, following feelings and imagination instead of reason.
# We neglected the care and control of our bodies.
# We avoided recognizing our personal decline and shrank from the task of changing.
# We systematically disguised in our imaginations the real nature of our unhealthy conduct.
# We became a prey to obsessions, delusions and hallucinations.
# We practised irrational habits, under elated feelings of irresponsibility or despairing feelings of inability or compulsion. <!-- NOTE: The literature uses the Australian spelling of "practiced". -->
# We rejected advice and refused to co-operate with help.
# We lost all insight into our condition.
{{Col-2}}
'''The Twelve Steps of Recovery and Personal Growth'''
# We admitted we were inadequate or maladjusted to life.
# We firmly resolved to get well and co-operated with the help that we needed.
# We surrendered to the healing power of a wise and loving God.
# We made a personal inventory and accepted ourselves.
# We made a moral inventory and cleaned out our hearts.
# We endured until cured.
# We took care and control of our bodies.
# We learned to think by reason rather than by feelings and imagination.
# We trained our wills to govern our feelings.
# We took our responsible and caring place in society.
# We grew daily closer to maturity.
# We carried GROW's hopeful, healing, and transforming message to others in need.
{{Col-end}}

GROW suggests [[atheists]] and [[agnostics]] use "We became inattentive to objective natural order in our lives" and "We trusted in a health-giving power in our lives as a whole" for the Second Stage of Decline and Third Step of Personal Growth, respectively.<ref name="BLUEBOOK"/>

=== Results of qualitative analysis ===
Statistical evaluations of interviews with GROW members found they identified [[Individualism|self-reliance]], industriousness, [[peer support]], and gaining a sense of [[Value (personal and cultural)|personal value]] or [[self-esteem]] as the essential ingredients of recovery.<ref name="CORRIGAN2005"/> Similar evaluations of GROW's literature revealed thirteen core principles of GROW's program. They are reproduced in the list below by order of relevance, with a quote from GROW's literature, explaining the principle.<ref name="CORRIGAN2002">{{cite journal | last1 = Corrigan | first1 = Patrick W. | last2 = Calabrese | first2 = Joseph D | last3 = Diwan | first3 = Sarah E. | last4 = Keogh | first4 =Cornelius, B | last5 = Keck | first5 = Lorraine | last6 = Mussey | first6 = Carol | title = Some Recovery Processes in Mutual-Help Groups for Persons with Mental Illness; I: Qualitative Analysis of Program Materials and Testimonies | journal = Community Mental Health Journal | volume = 38 | issue = 4 | year = 2002 | pages = 287–301 | issn = 0010-3853 | oclc = 38584278 | pmid = 12166916 | doi = 10.1023/A:1015997208303}}</ref>

{{Col-begin}}
{{Col-2}}
# ''Be Reasonable'': "We learned to think by reason rather than by feelings and imagination."
# ''Decentralize, participate in community'': "...decentralization from self and participation in a community of persons is the very process of recovery or personal growth."
# ''Surrender to the Healing Power of a wise and loving God'': "God, who made me and everything connected with me, can overcome any and every evil that affects my life."
# ''Grow Closer to Maturity'': "Maturity is a coming to terms with oneself, with others, and with life as a whole."
# ''Activate One's Self to Recover and Grow'' "Take your fingers off your pulse and start living."
# ''Become Hopeful'': "I can, and ultimately will, become completely well; God who made me can restore me and enable me to do my part. The best in life and love and happiness is ahead of me."
# ''Settle for Disorder'': "Settle for disorder in lesser things for the sake of order in greater things; and therefore be content to be discontent in many things."
{{Col-2}}
<ol start="8">
<li>''Be Ordinary'': "I can do whatever ordinary good people do, and avoid whatever ordinary good people avoid. My special abilities will develop in harmony only if my foremost aim is to be a good ordinary human being."
<li>''Help Others'': We carried the GROW message to others in need. 
<li>''Accept One's Personal Value'': "No matter how bad my physical, mental, social or spiritual condition I am always a human person, loved by God and a connecting link between persons; I am still valuable, my life has a purpose, and I have my unique place and my unique part in my Creator's own saving, healing and transforming work."
<li>''Use GROW'': "Use the hopeful and cheerful language of GROW."
<li>''Gain Insight'': "We made moral inventory and cleaned out our hearts."
<li>''Accept Help'': "We firmly resolved to get well and co-operated with the help that we needed."
</ol>
{{Col-end}}

== Effectiveness ==
: ''For more details on this topic, see [[Effectiveness of self-help groups for mental health|Self-help groups for mental health: Effectiveness]]''

Participation in GROW has been shown to decrease the number of hospitalizations per member as well as the duration of hospitalizations when they occur. Members report an increased sense of [[security]] and [[self-esteem]], and decreased [[anxiety]].<ref name="KENNEDY1990">{{cite conference | first = Mellen | last = Kennedy | year = 1990 | title = Psychiatric Hospitalizations of GROWers | conference = Second Biennial Conference on Community Research and Action, [[East Lansing, Michigan]]}} cited in {{cite book | last = Kyrouz | first = Elaina M. |author2=Humphreys, Keith |author3=Loomis, Colleen |date=October 2002 | title = American Self-Help Group Clearinghouse Self-Help Group Sourcebook | edition = 7th  | chapter = Chapter 4: A Review of Research on the Effectiveness of Self-help Mutual Aid Groups | chapterurl = https://www.researchgate.net/publication/238074993_A_Review_of_Research_on_the_Effectiveness_of_Self-Help_Mutual_Aid_Groups | editor = White, Barbara J. |editor2=Madara, Edward J. | isbn = 1-930683-00-6 | pages = 71–86 | url = http://www.mentalhelp.net/selfhelp/ | publisher = American Self-Help Group Clearinghouse | accessdate = 2008-01-06}}</ref> A longitudinal study of GROW membership found time involved in the program correlated with increased [[autonomy]], environmental mastery, personal growth, [[self-acceptance]] and [[social skills]].<ref name="FINN2007">{{cite journal | last = Finn | first = Lisabeth D. |author2=Bishop, Brian |author3=Sparrow, Neville H. | title = Mutual help groups: an important gateway to wellbeing and mental health | journal = Australian Health Review | volume  = 31 | issue = 2 | pages = 246–255 |date=May 2007 | issn = 1449-8944 | pmid = 17470046 | doi=10.1071/ah070246}}</ref> Women in particular experience positive [[Identity (social science)|identity]] [[Identity formation|transformation]], build [[friendship]]s and find a [[sense of community]] in GROW groups.<ref name="KERCHEVAL2005">{{cite thesis|last=Kercheval |first=Briony L |title=Women's experiences at GROW: 'There's an opportunity there to grow way beyond what you thought you could...' |url=http://www.grow.net.au/Information/Research/DrLisabethFinn/ThesisBrionyKercheval.pdf |publisher=Victoria University, Footscray |location=Victoria, Australia |date=March 2005 |degree=Master of Applied Psychology (Community) School of Psychology, Faculty of Arts |archivedate=2010-01-22 |archiveurl=http://www.webcitation.org/5myMx5Z1S?url=http://www.grow.net.au/Information/Research/DrLisabethFinn/ThesisBrionyKercheval.pdf |accessdate=2010-01-22 |deadurl=yes |df= }}</ref>

== Literature ==
''The Program of Growth to Maturity'', generally referred to as the 'Blue Book', is the principal literature used in GROW groups. The book is divided into three sections based on the developmental stages of members: 'Beginning Growers', 'Progressing Growers' and 'Seasoned Growers'. Additionally, there are three related books written by Cornelius B. Keogh, and one by Anne Waters, used in conjunction with the ''Blue Book''.

* {{cite book | author = GROW | title = GROW: World Community Mental Health Movement: The Program of Growth to Maturity (the "Blue Book") | publisher = GROW Publications | location = [[Sydney, Australia]] | oclc = 66288113 | year = 1983 | url = https://books.google.com/books?id=3ClHAAAAMAAJ}}
* {{cite book | last = Keogh | first = Cornelius B. | title = Readings for mental health (the "Brown Book") | year = 1975 | publisher = GROW Publications | location = [[Sydney, Australia]] | isbn = 0-909114-00-5 | oclc = 47699449}}
* {{cite book | last1 = Keogh | first1 = Cornelius B. | author2 = GROW (Australia) | title = Readings for recovery (the "Red Book") | year = 1967 | publisher = GROW | location = [[Sydney Australia]] | oclc = 154602570}}
* {{cite book | last = Keogh | first = Cornelius B. | title = Recovery | year = 1967 | location = [[Sydney, Australia]] | oclc =  57499165}}
* {{cite book | last = Waters | first = Anne | title = GROWing to Maturity: A Potpourri of Readings for Mental Health (the "Lavender Book") |publisher = GROW in Ireland Ltd |year=2005 |isbn=0-9529198-2-6}}
<!-- NOTICE TO EDITORS: I am leaving this out as it is a duplicate entry for the 'Blue Book'. 
* {{cite book | author = GROW | title = Grow : the program of growth to maturity. | publisher = GROW Publications | location = [[Sydney, Australia]] | year = 1975 | oclc = 154677054}} -->

== See also ==
* [[Emotions Anonymous]]
* [[Recovery International]] (formerly Recovery, Inc.)
* [[Self-help groups for mental health]]

== References ==
{{reflist|2}}

== External links ==
* [http://www.growinamerica.org/ GROW in America]
* [http://www.grow.org.au GROW in Australia]
** {{worldcat id|id=nc-grow+australia|name=GROW (Australia)}}
** {{worldcat id|id=nc-grow+organization+australia|name=GROW (Organization : Australia)}}
** {{worldcat id|id=nc-grow+australian|name=GROW (Australian)}}
** {{worldcat id|id=nc-grow$victorian+branch|name=GROW Victorian Branch}}
* [http://www.grow.ie/ GROW in Ireland]
* [http://www.grow.org.nz/ GROW in New Zealand]
** {{worldcat id|id=nc-grow+new+zealand|name=GROW New Zealand}}
* {{worldcat id|id=lccn-n91-69009|name=GROW (Movement)}}
* {{worldcat id|id=nc-grow|name=GROW}}

[[Category:Mental health support groups]]
[[Category:Mental health organisations in Australia]]
[[Category:Spiritual organizations]]
[[Category:Non-profit organisations based in Australia]]
[[Category:Support groups]]
[[Category:Organizations established in 1957]]
[[Category:Twelve-step programs]]