{{Infobox scientist
| name              = Alexander Holevo
| birth_date        = {{birth date|1943|09|02|df=y}}
| birth_place       = [[Russian SFSR]], [[Soviet Union]]
| image             = Holevo, Alexander2.jpg
| nationality       = [[Russia]]n
| field             = [[Mathematician]]
| work_institution  = [[Steklov Mathematical Institute]], [[Moscow State University]],
[[Moscow Institute of Physics and Technology]]
| alma_mater        = [[Moscow Institute of Physics and Technology]]
| doctoral_students =
| known_for         = [[Holevo's theorem]]
| awards            =
}}

'''Alexander Holevo'''<ref>{{cite web|title=Timeline of quantum computing|url=http://wikipedia.org/wiki/timeline_of_quantum_computing}}</ref>({{lang-ru|Алексáндр Семéнович Хóлево}}, also spelled as Kholevo and Cholewo) is a [[Soviet Union|Soviet]] and [[Russia]]n  [[mathematician]], one of the pioneers of [[quantum information science]].

== Biography ==
Alexander Holevo has been a member of [[Steklov Mathematical Institute]], Moscow, since 1969. He graduated from [[Moscow Institute of Physics and Technology]] in 1966, defended a PhD Thesis in 1969 and a Doctor of Science Thesis in 1975. Since 1986 A.S. Holevo is a Professor ([[Moscow State University]] and [[Moscow Institute of Physics and Technology]]).<ref name="mathnet">{{cite web|title=Holevo Alexander Semenovich on Russian informational portal Math-Net|url=http://www.mathnet.ru/php/person.phtml?option_lang=eng&personid=8777|accessdate=13 January 2014}}</ref>

== Research ==
A.S. Holevo made substantial contributions in the mathematical foundations of [[quantum mechanics|quantum theory]], [[quantum statistics]] and [[quantum information]] theory. In 1973 he obtained an upper bound for the amount of classical information that can be extracted from an ensemble of quantum states by quantum measurements (this result is known as ''[[Holevo's theorem]]''). A.S. Holevo developed the mathematical theory of [[quantum_channel|quantum communication channels]], the noncommutative theory of statistical decisions, he proved [[classical_capacity|coding theorems]] in quantum information theory and revealed the structure of quantum Markov semigroups and measurement processes. A.S. Holevo is the author of about one-hundred and seventy published works, including five monographs.<ref>{{cite web|title=Homepage of Alexander S. Holevo|url=http://www.mi.ras.ru/~holevo/eindex.html|accessdate=13 January 2014}}</ref>

== Honours and awards ==
* Andrey Markov Prize of Russian Academy of Sciences (1997)<ref name="mathnet"/><ref>{{cite web|title=Andrey Markov Award|url=http://www.ras.ru/about/awards/awdlist.aspx?awdid=65|accessdate=13 January 2014}}</ref>
* Prizes for the best scientific achievements of Russian Academy of Sciences (1992, 1995, 2008) <ref name="mathnet"/>
* Quantum Communication Award (1996)<ref>{{cite web|title=Quantum Communication Award|url=http://qcmc2012.org/awards/|accessdate=13 January 2014}}</ref>
* Alexander von Humboldt Research Award (1999).<ref>{{cite web|title=Alexander von Humboldt Foundation|url=http://www.humboldt-foundation.de/pls/web/pub_hn_query.humboldtianer_details?p_externe_id=7000154970&p_lang=en&p_pattern=Holevo|accessdate=13 January 2014}}</ref>
*Claude E. Shannon Award (2016)<ref>{{cite web|title=Claude E. Shannon Award|url=http://www.itsoc.org/honors/claude-e-shannon-award}}</ref>

== Bibliography ==
*{{cite journal
|first=A. S.
|last=Holevo
|journal=Problems of Information Transmission
|pages=177–183
|volume=9|issue=3|year=1973
|title=Bounds for the quantity of information transmitted by a quantum communication channel
}}
*{{cite journal
|first=A. S.
|last=Holevo
|journal=Proc. Steklov Math. Inst.
|publisher=[[Steklov Mathematical Institute]]
|title=Studies in general theory of statistical decisions
|volume=124 
|year=1978
}} 
*{{cite book
|first=A. S.
|last=Holevo
|title=Probabilistic and statistical aspects of quantum theory 
|publisher=[[North Holland]] 
|isbn=0-444-86333-8
|year=1982
}}
*{{cite journal
|first=A. S.
|last=Holevo
|journal=Lect. Notes Phys. Monographs
|publisher=[[Springer Science+Business Media|Springer]]
|isbn=3-540-42082-7
|title=Statistical structure of quantum theory 
|volume=67
|year=2001}}
*{{cite book
|first=A. S.
|last=Holevo
|title=Quantum systems, channels, information 
|publisher=[[De Gruyter]]
|isbn=978-3-11-027325-0
|year=2013
}}

==See also==
* [[History of quantum computing]]

== References ==
{{Reflist}}

{{Claude E. Shannon Award recipients}}

{{Authority control}}


{{DEFAULTSORT:Holevo, Alexander}}
[[Category:1943 births]]
[[Category:Living people]]
[[Category:Moscow Institute of Physics and Technology alumni]]
[[Category:Moscow Institute of Physics and Technology faculty]]
[[Category:Moscow State University faculty]]
[[Category:Quantum information science]]
[[Category:Russian academics]]
[[Category:Russian mathematicians]]
[[Category:Soviet mathematicians]]

{{Russia-mathematician-stub}}