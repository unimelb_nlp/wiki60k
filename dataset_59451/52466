{{Infobox journal
| title = Optical Review
| cover  = [[File:Optical Review.jpg]]
| editor = Suezo Nakadate
| discipline = [[Optical science]], [[optical engineering]]
| former_names =
| abbreviation = Opt. Rev.
| publisher = [[Springer Science+Business Media]]
| country =
| frequency = Bimonthly
| history = 1994-present
| openaccess =
| license =
| impact = 0.702
| impact-year = 2012
| website = http://www.springer.com/physics/journal/10043
| link1  = http://link.springer.com/journal/volumesAndIssues/10043
| link1-name = Online access
| link2  =
| link2-name =
| JSTOR  =
| OCLC  = 53863964
| LCCN  =
| CODEN  = OPREFN
| ISSN  = 1340-6000
| eISSN  = 1349-9432
}}
'''''Optical Review''''' is a bimonthly [[Peer review|peer-reviewed]] [[scientific journal]] that was established in 1994 and is published by [[Springer Science+Business Media]] in partnership with the [[Optical Society of Japan]]. The [[editor-in-chief]] is [[Suezo Nakadate]]. The journal publishes research and review papers in all [[branches of science|subdisciplines]] of [[optical science]] and [[optical engineering]].

Subdisciplines include [[optics|general]] and [[physical optics]], [[spectroscopy]], [[quantum optics]], [[optical computing]], [[photonics]], [[optoelectronics]], [[lasers]], [[nonlinear optics]], environmental optics, [[adaptive optics]], and space optics. Optics regarding the [[visible spectrum]], [[infrared]], and short wavelength optics are also included. Coverage encompasses required materials as well as suitable manufacturing tools, technologies, and methodologies.

== Abstracting and indexing ==
The journal is abstracted and/or indexed in:<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2013-12-27}}</ref>
{{columns-list|colwidth=30em|
* [[Science Citation Index Expanded]]
* [[Current Contents]]/ Physical, Chemical & Earth Sciences
* Current Contents/Engineering, Computing & Technology
* [[Academic OneFile]]
* [[Academic Search]]
* [[Astrophysics Data System]]
* [[Chemical Abstracts Service]]
* [[Inspec]]
* [[PASCAL (database)|PASCAL]]
* [[Scopus]]
* TOC Premier
* [[VINITI Database RAS]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 0.702.<ref name=WoS>{{cite book |year=2013 |chapter=Optical Review |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== See also ==
* ''[[Applied Physics Express]]''
* ''[[Japanese Journal of Applied Physics]]''

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.springer.com/physics/journal/10043}}
* [https://www.jstage.jst.go.jp/browse/or ''Optical Review''] at Optical Society of Japan website
* [http://www.ncbi.nlm.nih.gov/nlmcatalog/?term=1340-6000 NLM Catalog]

[[Category:Optics journals]]
[[Category:Science and technology in Japan]]
[[Category:Publications established in 1994]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:English-language journals]]
[[Category:Academic journals associated with learned and professional societies]]