{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{good article}}
{{Infobox album <!-- See Wikipedia:WikiProject_Albums -->
| Name        = Before and After Science
| Type        = studio
| Artist      = [[Brian Eno]]
| Cover       = Beforeandafterscience.jpg
| Alt         = A picture of the album cover depicting a white border with a stark black and white image of the side profile of Brian Eno's face. In the top right corner is Brian Eno's name. In the bottom right corner the album's title is written.
| Released    = {{Start date|df=yes|1977|12}}
| Studio = [[Sarm West Studios|Basing Street Studios]], London; [[Conny Plank|Conny's Studio]], [[Cologne]]<ref name="albumNotes">{{cite AV media notes |title=Before and After Science |others=Brian Eno |year=1977 |type=Vinyl back cover |publisher=[[Island Records|Island]] |id=ILPS-9478 }}</ref>
| Genre       = {{flat list|
*[[Art rock]]{{sfn|Seabrook|2008|p=160}}
*[[experimental pop]]<ref>{{cite web|last=Pickard |first=Joshua |url=http://nooga.com/171254/record-bin-the-experimental-pop-lucidity-of-brian-enos-before-and-after-science/ |title=Record Bin: The experimental pop lucidity of Brian Eno's "Before and After Science" |website=Nooga.com |date=2015-10-10 |accessdate=2016-06-01}}</ref>
*{{nowrap|[[art pop]]}}<ref name="ArtPop" />
* [[avant-pop]]<ref name="obrien2016">{{cite magazine|last1=O'Brien|first1=Glenn|title=New Again: Brian Eno|magazine=[[Interview Magazine]]|date=November 23, 2016}}</ref>
}}
| Length      = 39:30
| Label       = [[Polydor Records|Polydor]]
| Producer    = Brian Eno, [[Rhett Davies]]
| Last album  = ''[[Cluster & Eno]]''<br /> (1975)
| This album  = '''''Before and After Science'''''<br />(1977)
| Next album  = ''[[Ambient 1: Music for Airports]]''<br />(1978)
 |Misc = {{Singles
  | Name           = Before and After Science
  | Type           = album
  | single 1       = [[King's Lead Hat]]
  | single 1 date  = January 1978
}}}}

'''''Before and After Science''''' is the fifth studio album by British musician [[Brian Eno]]. Produced by Eno and [[Rhett Davies]], it was originally released by [[Polydor Records]] in December 1977. Guest musicians from the United Kingdom and Germany helped with the album, including members of [[Roxy Music]], [[Free (band)|Free]], [[Fairport Convention]], [[Can (band)|Can]] and [[Cluster (band)|Cluster]]. Over one hundred tracks were written with only ten making the album's final cut. The musical styles of the album range from energetic and jagged to more languid and pastoral.

The album marks Eno's last foray into rock music for the 1970s as a solo artist, with all his remaining albums of the decade showcasing more of Eno's avant-garde and [[ambient music]], which was hinted at on the second half of ''Before and After Science''. The album was Eno's second to chart in the United States. The song "[[King's Lead Hat]]", the title of which is an anagram for [[Talking Heads (band)|Talking Heads]], was remixed and released as a single, although it didn't chart in the United Kingdom. Critical response to the album has remained positive, with several critics calling it one of Eno's best works.

== Production ==
Unlike Eno's previous albums, which were recorded in a very short time, ''Before and After Science'' was two years in the making.{{sfn|Tamm|1989|p=107}} During this two-year period, Eno was busy working on his solo [[ambient music]] albums ''[[Music for Films]]'' and ''[[Discreet Music]]''.{{sfn|Tamm|1989|p=107}} Due to the very positive critical reception accorded his previous rock music-oriented album ''[[Another Green World]]'', Eno was afraid of repeating himself but still wanted to release a high-quality product.{{sfn|Tamm|1989|p=107}}

As on previous rock-based recordings, for ''Before and After Science'' Eno worked with a plethora of guest musicians. Several artists from German and British groups of the era contributed to the album, collaborating with Eno for the first time. Guitarist [[Fred Frith]] caught the attention of [[Brian Eno]] who was "excited by the timbral possibilities that [Frith had] been discovering" on his album ''[[Guitar Solos]]''.<ref name=moffom>{{cite web |archiveurl=https://web.archive.org/web/20071214042237/http://www.moffom.org/en/iaf-fred-frith.php|archivedate=14 December 2007 |url=http://www.moffom.org/en/articles-and-interviews/interview-fred-frith |date=January 2007|title=Interview with Fred Frith |work=Music on Film / Film on Music|last=Jónsson|first=Darryl |accessdate=16 July 2009}}</ref> Eno asked Frith to record with him, and this resulted in Frith playing guitar on the album.<ref name=moffom /> [[Jaki Liebezeit]] of the [[Music of Germany|German]] [[krautrock]] group [[Can (band)|Can]] played drums for Eno on "Backwater", while German [[ambient music]] group [[Cluster (band)|Cluster]] contributed to the songwriting and instrumentation of the track "By This River".<ref name="albumNotes" /><ref name=Allmusic /> Eno had previously worked with Cluster on their album ''[[Cluster & Eno]]'' released in 1977.<ref name=AllmusicCluster>{{cite web
| last       = Waynick
| first      = Michael
| title      = ''Cluster & Eno'' album review
| publisher  = [[AllMusic]]
| url        = {{Allmusic|class=album|id=r105951|pure_url=yes}}
| accessdate = 31 May 2009
}}</ref> Additional session musicians included [[Dave Mattacks]] of British [[folk music|folk]] band [[Fairport Convention]], who contributed drums to "Kurt's Rejoinder" and "Here He Comes", and [[Andy Fraser]] of British [[blues rock]] band [[Free (band)|Free]], who played drums on "King's Lead Hat".<ref name="albumNotes" /><ref name=AllmusicFraser>{{cite web
| last       = True
| first      = Christopher M.
| title      = Andy Fraser overview at Allmusic
| publisher  = [[AllMusic]]
| url        = {{Allmusic|class=artist|id=p17685|pure_url=yes}}
| accessdate = 31 May 2009
}}</ref><ref name=AllmusicMattacks>{{cite web
| last       = McDonald
| first      = Steven
| title      = Dave Mattacks overview at Allmusic
| publisher  = [[AllMusic]]
| url        = {{Allmusic|class=artist|id=p102848|pure_url=yes}}
| accessdate = 31 May 2009
}}</ref>

Eno also had several musicians who he had worked with on previous solo albums return. [[Percy Jones (musician)|Percy Jones]] of [[Brand X]] and [[Phil Collins]] of Brand X and [[Genesis (band)|Genesis]], who had been on Eno's two previous rock albums, played bass and drums respectively.<ref name=Allmusic /> Other contributors included [[Robert Fripp]] of [[King Crimson]], [[Paul Rudolph (musician)|Paul Rudolph]] of [[Hawkwind]] and [[Bill MacCormick]] and [[Phil Manzanera]] of [[Quiet Sun]].<ref name=AllmusicQuietSun>{{cite web
| last       = Mills
| first      = Ted
| title      = Quiet Sun overview at Allmusic
| publisher  = [[AllMusic]]
| url        = {{Allmusic|class=artist|id=p11480|pure_url=yes}}
| accessdate = 31 May 2009
}}</ref> [[Robert Wyatt]] went under the pseudonym of Shirley Williams and is credited on the album for "time" and "brush timbales" on "Through Hollow Lands" and "Kurt's Rejoinder" respectively.<ref>{{cite AV media
| title        = A Quantity of Stuff – The Brian Eno Story
| credits      = [[Stuart Maconie]]
| network      = BBC Radio 2
| airdate      = 1 February 2003
}}</ref> Working extensively with the musicians and his instructional cards—the [[Oblique Strategies]]—during the two years working on the album, Eno wrote over one hundred songs.<ref name="albumNotes" />{{sfn|Tamm|1989|p=107}}<ref name="BangsVoice">{{cite journal |first=Lester|last=Bangs|authorlink=Lester Bangs|title=Eno Sings With the Fishes|page=49|date=4 March 1978|journal=[[Village Voice]]}}</ref>

== Music and lyrics==
{{listen
| pos = left
| filename = King's Lead Hat - Brian Eno.ogg
| title="King's Lead Hat"
| description = Rock critic [[Lester Bangs]] described the song "King's Lead Hat" as a track that emphasises "Eno's affinities with [[New wave music|new wave]] in its rushed mechanical rhythms".<ref name="BangsVoice" />
}}
Jim DeRogatis, author of ''Turn on Your Mind: Four Decades of Great Psychedelic Rock'', described the overall sound of ''Before and After Science'' as "the coldest and most clinical of Eno's pop efforts",{{sfn|DeRogatis|2003|p=245}} while David Ross Smith of online music database [[AllMusic]] wrote that "Despite the album's pop format, the sound is unique and strays far from the mainstream".<ref name=Allmusic>{{cite web| last= Ross Smith| first = David| title = ''Before and After Science'' album review |publisher = [[AllMusic]]| url= {{Allmusic|class=album|id=r2277493|pure_url=yes}}| accessdate = 29 May 2009}}</ref> According to [[David Bowie]]'s biographer Thomas Jerome Seabrook, the album is "split between up-tempo art-rock on side one and more pastoral material on side two",{{sfn|Seabrook|2008|p=160}} while Piotr Orlov of ''[[LA Weekly]]'' categorized it as an [[art pop]] record.<ref name="ArtPop">{{cite news|last=Orlov|first=Piotr|url=http://www.laweekly.com/2004-12-16/music/lit-up-and-emotional/?showFullText=true|title=Lit Up and Emotional: Enorchestra|newspaper=[[LA Weekly]]|date=16 December 2004|accessdate=7 February 2015}}</ref> The album's opening tracks "No One Receiving" and "Backwater" start the album as upbeat and bouncy songs.<ref name=Allmusic /> "King's Lead Hat" is an [[anagram]] of [[Talking Heads]], a new wave group Eno had met after a concert in England when they were touring with [[Ramones]].{{sfn|DeRogatis|2003|p=246}}{{sfn|Gittins|2004|p=36}} Eno would later produce Talking Heads' second, third and fourth albums, including ''[[Remain in Light]]''.<ref name=MoreSongs>{{cite web| title      = ''More Songs About Buildings and Food'' album credits| publisher  = [[AllMusic]]| url        = {{Allmusic|class=album|id=r19632|pure_url=yes}}| accessdate = 31 May 2009}}</ref> The last five songs of the album have been described as having "an occasional pastoral quality" and being "pensive and atmospheric".<ref name=Allmusic />

Opposed to ''[[Another Green World]]'''s music, which Eno described as "sky music", Eno referred to the music of ''Before and After Science'' as "ocean music".<ref name="BangsVoice" /> References to water in the lyrics appear in songs such as "Backwater", "Julie With..." and "By this River".{{sfn|Reynolds|1996|p=203}} Author Simon Reynolds noted themes of "boredom" and "bliss" through the album, citing "Here He Comes", about "a boy trying to vanish by floating through the sky through a different time" and "Spider and I", about a boy watching the sky and dreaming about being carried away with a ship, as examples.{{sfn|Reynolds|1996|p=203}} Eno's songwriting style was described as "a sound-over-sense approach".<ref name=Allmusic /> Influenced by poet [[Kurt Schwitters]], Eno consciously did not make songwriting or lyrics the main focus in the music.<ref name=Allmusic /> Tom Carson of ''Rolling Stone'' noted this style, stating that the lyrics are "only complementary variables" to the music on the album.<ref name=rollingstone>{{cite web
| last       = Carson
| first      = Tom
| title      = ''Before and After Science'' album review
| publisher  = ''[[Rolling Stone]]''
| date       = 18 May 1978
| url        = http://www.rollingstone.com/reviews/album/117241/review/5940433
| accessdate = 29 May 2009
| archiveurl= https://web.archive.org/web/20090619210507/http://www.rollingstone.com/reviews/album/117241/review/5940433| archivedate= 19 June 2009 <!--DASHBot-->| deadurl= no}}</ref> Lester Bangs commented on Eno's lyrical style on  "Julie with..." stating that the lyrics themes "could be a murderer's ruminations, or simply a lovers' retreat... or Julie could be three years old".<ref name="BangsVoice" /> Schwitters' influence is also shown on the song "Kurt's Rejoinder", on which samples of Schwitters' poem "Ursonate" can be heard.<ref name=Allmusic />{{sfn|DeRogatis|2003|p=246}}

== Release ==
[[File:Four Years.jpg|left|thumb|150px|alt=an illustration of an empty room featuring two floors connected by a carpeted stairway.|[[Peter Schmidt (artist)|Peter Schmidt]]'s "Four Years" was one of four prints included in the original pressings of the album<ref name="Schmidt">{{cite web |url= http://www.peterschmidtweb.com/BeforeAndAfterScience.html|title=Peter Scmidth Web  |accessdate=31 May 2009 |work=Peter Schmidt Web| archiveurl= https://web.archive.org/web/20090619215821/http://www.peterschmidtweb.com/BeforeAndAfterScience.html| archivedate= 19 June 2009 <!--DASHBot-->| deadurl= no}}</ref> ]]
''Before and After Science'' was released in December 1977 on [[Polydor Records|Polydor]] in the United Kingdom and on [[Island Records|Island]] in the United States.{{sfn|Strong|1998|p=245}} The first pressings of the album included four offset prints by [[Peter Schmidt (artist)|Peter Schmidt]].<ref name="Schmidt" /> The back cover of the LP says "Fourteen Pictures" under the album title, referencing Eno's ten songs and Peter Schmidt's 4 prints. These prints included "The Road to the Crater", "Look at September, look at October", "The Other House" and "Four Years".<ref name="Schmidt" /> The album did not chart in the United Kingdom, but was Eno's first album since ''[[Here Come the Warm Jets]]'' to chart in the United States, where it peaked at 171 on the [[Billboard 200|''Billboard'' Top LPs & Tapes]] chart.<ref name="charter">{{cite web |url= {{Allmusic|class=artist|id=p74178|pure_url=yes}}|title=Brian Eno > Charts & Awards > Billboard Albums  |accessdate=29 May 2009 |format= |work=Allmusic}}</ref>{{sfn|Warwick|2004|p=379}} "King's Lead Hat" was remixed and released as a single in January 1978, featuring the B-side "R.A.F.", which is credited to "Eno & Snatch" (in the UK, not the US).{{sfn|Strong|1998|p=245}} This single failed to chart and has never been reissued in any form.<ref name="charter" />{{sfn|Warwick|2004|p=379}}

''Before and After Science'' was re-issued on compact disc through [[E.G. Records]] in January 1987.{{sfn|Strong|1998|p=245}} In 2004, [[Virgin Records]] began reissuing Eno's albums in batches of four to five.<ref name=NME>{{cite web |url= http://www.nme.com/news/brian-eno/16325|title= The Musical Life of Brian! : News : NME.com |accessdate=30 May 2009 |work=NME| archiveurl= https://web.archive.org/web/20090517062358/http://www.nme.com/news/brian-eno/16325| archivedate= 17 May 2009 <!--DASHBot-->| deadurl= no}}</ref> The [[Audio mastering|remastered]] [[digipak]] release of ''Before and After Science'' was released on 31 May 2004 in the United Kingdom and on 1 June 2004 in North America.<ref name=popmattersList>{{cite web|url=http://www.popmatters.com/pm/review/enobrian-before2004/|title=Brian Eno: Before and After Science [reissue] – PopMatters Music Review|date=29 July 2004|author=Davidson, John|accessdate=30 May 2009|publisher=[[Popmatters]]| archiveurl= https://web.archive.org/web/20090620014252/http://www.popmatters.com/pm/review/enobrian-before2004/| archivedate= 20 June 2009 <!--DASHBot-->| deadurl= no}}</ref>

== Critical reception ==
{{Album ratings
| title =
| subtitle =
| state =
<!-- Reviewers -->
| rev1 = [[AllMusic]]
| rev1Score = {{Rating|5|5}}<ref name=Allmusic />
| rev2 = ''[[Blender (magazine)|Blender]]''
| rev2Score = {{Rating|5|5}}<ref>{{cite web
| first       = Douglas
| last        = Wolk
| title       = Blender: Brian Eno (various reissues)
| url         = http://www.blender.com/reviews/review_2169.html
| archiveurl  = http://www.webcitation.org/5iya6ehIr?url=http://web.archive.org/web/20040806081840/http://www.blender.com/reviews/review_2169.html
| work        = [[Blender (magazine)|Blender]]
| year        = 2004
| archivedate = 12 August 2009
}}</ref>
| rev3 = [[Robert Christgau]]
| rev3Score = A−{{sfn|Christgau|1990|p=127}}
| rev4 = ''[[Crawdaddy!]]''
| rev4Score = favourable<ref name="crawdaddy" />
| rev5 = ''[[Down Beat]]''
| rev5Score = favourable<ref name="downbeat" />
| rev6 = [[Pitchfork Media]]
| rev6Score = 7.7/10<ref name="pitchfork" />
| rev7 = [[PopMatters]]
| rev7Score = favourable<ref name=popmattersList />
| rev8 = ''[[Rolling Stone]]''
| rev8Score = favourable<ref name=rollingstone />

| rev10 = ''[[Spin Alternative Record Guide]]''
| rev10Score = 9/10{{sfn|Weisbard|Marks|1995|p=129}}
| rev11 = ''[[Stylus Magazine]]''
| rev11Score = favourable<ref>{{cite web |title=Stranded: ''Before and After Science'' – Article – Stylus Magazine |url=http://www.stylusmagazine.com/articles/weekly_article/stranded-before-and-after-science.htm |publisher=[[Stylus Magazine]] |accessdate=12 June 2009| archiveurl= https://web.archive.org/web/20090620170121/http://www.stylusmagazine.com/articles/weekly_article/stranded-before-and-after-science.htm| archivedate= 20 June 2009 <!--DASHBot-->| deadurl= no}}</ref>
}}
On the album's initial release, the album received very positive reviews from rock critics. Writing for ''[[Creem]]'', Joe Fernbacher called the ''Before and After Science'' "the perfect Eno album",<ref name="creem">{{cite journal |title=Records: Before and After Science|date=April 1978 |journal=[[Creem]] |issue=9|page=67}}</ref> while Mitchell Schneider wrote a positive review in ''[[Crawdaddy!]]'', stating that he couldn't "remember the last time a record took such a hold of [him]—and gave [him] such an extreme case of [[Vertigo (medical)|vertigo]], too".<ref name="crawdaddy">{{cite journal |title=Brave New Eno: Before and After Science|date=May 1978 |journal=[[Crawdaddy!]] |issue=84|page=64 |quote=Brian Eno is an agent from some other time and some other place who seems to know something that we don't but should...I can't remember the last time a record took such a hold of me—and gave such an extreme case of vertigo, too.)}}</ref> In ''[[Down Beat]]'', Russel Shaw wrote that "[''Before and After Science''] is another typically awesome, stunning and numbing Brian Eno album—the record [[Pink Floyd]] ''could'' make if they set their collective mind to it."<ref name="downbeat">{{cite journal |title=Record Reviews: Before and After Science|issue=45|date=13 July 1978|journal=[[Down Beat]] |page=36 |quote=What a wonderland of a zoo, a cross between steaming smoke, atonal mystery and hanging, frothy ditties...This is another typically awesome, stunning, numbing Brian Eno album—the record Pink Floyd could make if they set their collective mind to it.)}}</ref> Tom Carson of ''[[Rolling Stone]]'' noted that the album "is less immediately ingratiating than either ''[[Taking Tiger Mountain (By Strategy)|Taking Tiger Mountain]]'' or ''[[Here Come the Warm Jets]]''. Still, the execution here is close to flawless, and despite Eno's eclecticism, the disparate styles he employs connect brilliantly."<ref name=rollingstone /> Critic [[Robert Christgau]] gave the album an A− rating, stating that he "didn't like the murkiness of the quiet, largely instrumental reflections that take over side two", but didn't find that it "diminishes side one's oblique, charming tour of the popular rhythms of the day".{{sfn|Christgau|1990|p=127}} In 1979, ''Before and After Science'' was voted 12th best album of the year on the ''[[Village Voice]]'''s [[Pazz & Jop]] critics poll for 1978.<ref>{{cite web |url= http://www.robertchristgau.com/xg/pnj/pjres78.php|title= The 1978 Pazz & Jop Critics Poll  |accessdate=31 May 2009 |work=[[Village Voice]] | archiveurl= https://web.archive.org/web/20090619143455/http://www.robertchristgau.com/xg/pnj/pjres78.php| archivedate= 19 June 2009 <!--DASHBot-->| deadurl= no}}</ref>

Modern reviews of ''Before and After Science'' have also been positive. David Ross Smith of [[AllMusic]] awarded the album the highest rating of five stars stating that it ranks alongside ''[[Here Come the Warm Jets]]'' and ''[[Another Green World]]'' "as the most essential Eno material".<ref name=Allmusic /> The music webzine ''[[Tiny Mix Tapes]]'' awarded the album their highest rating, stating that it "is not only one of the best albums in Eno's catalog, but of the 1970s as a whole".<ref name="tinymix">{{cite web |url= http://www.tinymixtapes.com/Brian-Eno,5982|title= Brian Eno – Before and After Science – Delorian Reviews – Tiny Mix Tapes  |accessdate=31 May 2009 |work=[[Tiny Mix Tapes]] |archiveurl=https://web.archive.org/web/20090620170136/http://www.tinymixtapes.com/Brian-Eno,5982 <!--Added by H3llBot--> |archivedate=20 June 2009 }}</ref> The webzine [[Pitchfork Media]] gave ''Before and After Science'' a positive but less enthusiastic review, calling the album a "neutered star in search of fuel, boasting only 'King's Lead Hat' for the pop world, and the luminous pure prog-jazz of 'Energy Fools the Magician' for the out-rock contingent".<ref name="pitchfork">{{cite web|url=http://pitchfork.com/reviews/albums/11730-here-come-the-warm-jets-taking-tiger-mountain-by-strategy-another-green-world-before-and-after-science/ |title=Pitchfork: Brian Eno: Here Come the Warm Jets / Taking Tiger Mountain (By Strategy) / Another Green World / Before and After Science |date=13 June 2004 |accessdate=31 May 2009 |work=[[Pitchfork Media]] |archiveurl=https://web.archive.org/web/20090620203737/http://pitchfork.com/reviews/albums/11730-here-come-the-warm-jets-taking-tiger-mountain-by-strategy-another-green-world-before-and-after-science/ |archivedate=20 June 2009 |deadurl=yes |df=dmy }}</ref> Ten days later Pitchfork placed ''Before and After Science'' at number 100 on their list of "Top 100 Albums of the 1970s", referring to it as a "lovely, charming album" and going on to state that, while "not formally groundbreaking, it's frequently overlooked when discussing great albums from an era that's romanticized as placing premiums on progression and innovation—and particularly in the context of Eno's career, which is so full of both".<ref>{{cite web |url= http://pitchfork.com/features/staff-lists/5932-top-100-albums-of-the-1970s/|title=Pitchfork: Top 100 Albums of the 1970s |date=23 June 2004 |accessdate=31 May 2009 |work=[[Pitchfork Media]] | archiveurl= https://web.archive.org/web/20090531052330/http://pitchfork.com/features/staff-lists/5932-top-100-albums-of-the-1970s/| archivedate= 31 May 2009 <!--DASHBot-->| deadurl= no}}</ref>

The album was included in Robert Dimery's ''[[1001 Albums You Must Hear Before You Die]]''.

== Track listing ==
{{Track listing
| headline        = Side one
| all_writing     = [[Brian Eno]], except where noted<ref name="albumNotes" /><ref>{{cite web|url={{Allmusic|class=album|id=r1708426|pure_url=yes}}|work=Allmusic|title=allmusic: Before and After Science: Ten Pictures|accessdate=July 30, 2010}}</ref><!-- this last cite is for the correct lengths of songs from the remaster -->
| title1          = No One Receiving
| length1         = 3:52
| title2          = Backwater
| length2         = 3:43
| title3          = Kurt's Rejoinder
| length3         = 2:55
| title4          = Energy Fools the Magician
| note4           = arranged by [[Percy Jones (musician)|Percy Jones]], Eno
| length4         = 2:04
| title5          = [[King's Lead Hat]]
| length5         = 3:56
}}
{{Track listing
| headline        = Side two
| title1          = Here He Comes
| length1         = 5:38
| title2          = Julie With ...
| length2         = 6:19
| title3          = By This River
| note3           = Eno, [[Hans-Joachim Roedelius]], [[Dieter Moebius]]
| length3         = 3:03
| title4          = Through Hollow Lands
| note4           = for [[Harold Budd]]) (arranged by [[Fred Frith]], Eno
| length4         = 3:56
| title5          = Spider and I
| length5         = 4:10
}}

== Personnel ==
'''Musicians'''
* [[Brian Eno]] – voices, synthesizers ([[Minimoog]], [[EMS Synthi AKS]], [[Yamaha CS-80]]), guitar, synthesised percussion, piano, [[brass instrument|brass]], vibes, metallics, bell
{{colbegin}}
* [[Paul Rudolph (musician)|Paul Rudolph]] – bass, rhythm guitar, harmonic bass
* [[Phil Collins]] – drums
* [[Percy Jones (musician)|Percy Jones]] – [[fretless bass]], analogue delay bass
* [[Rhett Davies]] – agong-gong, stick
* [[Jaki Liebezeit]] – drums
* [[Dave Mattacks]] – drums
* Shirley Williams ([[Robert Wyatt]]) – brush [[timbales]], time
* [[Kurt Schwitters]] – voice
* [[Fred Frith]] – modified guitar, cascade guitars
* [[Andy Fraser]] – drums
* [[Phil Manzanera]] – rhythm guitar
* [[Robert Fripp]] – guitar
* [[Hans Joachim Roedelius|Achim Roedelius]] – [[grand piano]], [[electric piano]]
* [[Dieter Moebius|Möbi Moebius]] – [[Rhodes piano|bass Fender piano]]
* [[Bill MacCormick]] – bass
* Brian Turrington – bass
{{colend}}
'''Production'''
{{columns-list|colwidth=50em|
* [[Peter Schmidt (artist)|Peter Schmidt]] – art prints
* Rhett Davies – producer, [[audio engineer]]
* Ritva Saarikko – cover photograph
* Brian Eno – cover design, producer
* [[Conny Plank]] – engineer
* Dave Hutchins – engineer
* Cream – cover artwork
}}

== Chart positions==
{|class="wikitable sortable"
|-
!Chart (1978)
!Peak<br />position
|-
|[[RIANZ|New Zealand Albums Chart]]<ref name="rianz">{{cite web|title=charts.org.nz – Brian Eno – Before and After Science |work=[[Recording Industry Association of New Zealand]] |url=http://charts.org.nz/showitem.asp?interpret=Brian+Eno&titel=Before+And+After+Science&cat=a |accessdate=8 July 2010 |deadurl=yes |archiveurl=https://web.archive.org/web/20121024073922/http://charts.org.nz/showitem.asp?interpret=Brian+Eno&titel=Before+And+After+Science&cat=a |archivedate=24 October 2012 |df=dmy }}</ref>
| style="text-align:center;"|18
|-
|[[Sverigetopplistan|Swedish Albums Chart]]<ref name="swedishchart">{{cite web|title=swedishcharts.com – Brian Eno – Before and After Science |work=[[Sverigetopplistan]] |url=http://swedishcharts.com/showitem.asp?interpret=Brian+Eno&titel=Before+And+After+Science&cat=a |accessdate=8 July 2010 |deadurl=yes |archiveurl=https://web.archive.org/web/20121024151834/http://swedishcharts.com/showitem.asp?interpret=Brian+Eno&titel=Before+And+After+Science&cat=a |archivedate=24 October 2012 |df=dmy }}</ref>
| style="text-align:center;"|25
|}

== Notes ==
{{Reflist|30em}}

== References ==
* {{cite book
| last      = Tamm
| first     = Eric
| title     = Brian Eno: His Music and the Vertical Color of Sound
| publisher = [[Faber and Faber]]
| year      = 1989
| isbn      = 0-571-12958-7 |ref=harv
}}
*{{cite book
 | last= Weisbard
 | first= Eric
 | first2= Craig |last2=Marks
 | title= Spin Alternative Record Guide
 |publisher= Vintage Books
 |year= 1995
 |isbn= 0-679-75574-8 |ref=harv
}}
* {{cite book
| last      = DeRogatis
| first     = Jim
| title     = Turn on Your Mind: Four Decades of Great Psychedelic Rock
| year      = 2004
| publisher = [[Hal Leonard Corporation]]
| isbn      = 0-634-05548-8
| url       = https://books.google.com/books?id=U7cQmRsLgN8C |ref=harv
}}
* {{cite book
| last      = Gittins
| first     = Ian
| title     = Talking Heads: once in a lifetime: the stories behind every song
| year      = 2004
| publisher = [[Hal Leonard Corporation]]
| isbn      = 0-634-08033-4
| url       = https://books.google.com/books?id=ZvhoZyTzspYC |ref=harv
}}
*{{cite book
 | last= Strong
 | first= M. C.
 | title= The Great Rock Discography
 |publisher= [[Giunti]]
 |year= 1998
 |isbn= 88-09-21522-2 |ref=harv
}}
*{{cite book
 | last= Warwick
 | first= Neil
 | first2= Jon |last2=Kutner |first3=Tony |last3=Brown
 | title= The Complete Book of the British Charts: Singles and Albums
 |publisher= [[Omnibus Press]]
 |year= 2004
 |isbn= 1-84449-058-0 |ref=harv
}}
*{{cite book
 | last= Christgau
 | first= Robert
 | authorlink=Robert Christgau
 | title= Rock albums of the '70s
 |publisher= [[Da Capo Press]]
 |year= 1990
 |isbn= 0-306-80409-3 |ref=harv
}}
*{{cite book
 | last= Seabrook
 | first= Thomas Jerome
 | title= Bowie in Berlin: A New Career in a New Town
 |publisher= [[Jawbone Press]]
 |year= 2008
 |isbn= 1-906002-08-8 |ref=harv
}}
*{{cite book
 | last= Reynolds
 | first= Simon
 | title= The Sex Revolts: Gender, Rebellion, and Rock 'n' Roll
 |publisher= [[Joy Press]]
 |year= 1996
 |isbn= 0-674-80273-X
 |url=https://books.google.com/books?id=Acq7ZOYd_AcC |ref=harv
}}

== External links ==
{{Portal|Rock music}}
*[http://www.radio3net.ro/dbartists/supersearch/YmVmb3JlIGFuZCBhZnRlciBzY2llbmNl/before%20and%20after%20science Album online at radio3net.ro]
* ''[http://www.google.com/musicl?lid=qKT0Y9oHUM&aid=DP8qs0_UzaM Before and After Science]'' at [[Google]] music (US only)
* ''[http://rateyourmusic.com/release/album/brian_eno/before_and_after_science/ Before and After Science]'' at [[Rate Your Music]]
*[http://www.astralwerks.com/eno/albums.html Brian Eno – "Early Works" reissues] at [[Astralwerks]]

{{Brian Eno}}

[[Category:1977 albums]]
[[Category:Brian Eno albums]]
[[Category:Albums produced by Brian Eno]]
[[Category:Albums produced by Rhett Davies]]
[[Category:Polydor Records albums]]
[[Category:Island Records albums]]
[[Category:Experimental pop albums]]
[[Category:Art pop albums]]
[[Category:Avant-pop albums]]