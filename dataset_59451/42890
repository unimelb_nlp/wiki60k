{|{{Infobox Aircraft Begin
  |name = Ag Cat 
  |image = G164B AgCat.jpg
  |caption = Ag Cat G-164B
}}{{Infobox Aircraft Type
  |type = Agricultural aircraft 
  |manufacturer = [[Grumman]]
  |designer = 
  |first flight = 1957<ref name="Wood">Wood, Derek: ''Jane's World Aircraft recognition Handbook'', page 460. Jane's Publishing, 1983. ISBN 0-7106-0202-2</ref>
  |introduced = 1957<ref name="Wood"/>
  |retired =
  |status = 
  |primary user = 
  |produced = 
  |number built = 
  |unit cost =
  |developed from = 
  |variants with their own articles = 
}}
|}
[[File:AgCat.jpg|right|thumb|Schweizer Ag Cat B]]
[[File:G164A on Floats.jpg|thumb|G164A on Floats]]

The '''Grumman G-164 Ag Cat''' is a single-engine [[biplane]] [[agricultural aircraft]], developed by [[Grumman]] in the 1950s.

==Development==
The Ag Cat was the first aircraft specifically designed by a major aircraft company for agricultural aviation.<ref name="Smithsonian">{{cite web|url = http://www.nasm.si.edu/collections/artifact.cfm?id=A20080395000|title = Grumman C-164 Ag-Cat|accessdate = 12 May 2010|last = Smithsonian National Air and Space Museum|authorlink = |year = n.d.}}</ref>

In 1955, Grumman preliminary design engineers Joe Lippert and Arthur Koch proposed the design for a "purpose built" crop dusting airplane as a means of fulfilling a pressing need in the agricultural community as well as the perceived need for Grumman to diversify its product lines.  The first G-164, which was built by Grumman, accomplished its maiden flight on May 27, 1957 with Grumman test pilot Hank Kurt at the controls.<ref name="Smithsonian"/><ref name="Schweizer">Schweizer, William: ''The Ageless Ag-Cat: The Forty-year History of the Ag-Cat Agricultural Airplane'', Rivilo Books, 1995 ISBN 0-9630731-1-7</ref>

At this time, the Grumman G-164 did not have a name.  Leroy Grumman suggested "The Grasshopper." However, Dick Reade suggested "Ag-Cat," following Grumman's naming tradition using the suffix "-Cat" in aircraft names (e.g., F4F Wildcat and F6F Hellcat).  Mr. Grumman agreed and the Grumman G-164 became the "Ag-Cat." <ref name="Schweizer"/>
 
Large military orders prevented the production of the Ag-Cat at Grumman's Bethpage facility.  Grumman's Board of Directors chose to subcontract the entire program to the Schweizer Aircraft Company of Elmira, New York.  Initial production was through a contract between [[Schweizer Aircraft Corporation]],<ref name="Montgomery">Montgomery, MR & Gerald Foster: ''A Field Guide to Airplanes - Second Edition'', page 14. Houghton Mifflin Publishing, 1992. ISBN 0-7232-3697-6</ref> and Grumman.  The first Schweizer-built Ag-Cat, bearing registration number N10200 flew on October 17, 1958 under the control of Schweizer test pilot Clyde Cook.<ref name="Schweizer"/>  Full production began in January 1959 with Schweizer delivering 12 FAA certified airplanes to Grumman by March 1959.  The FAA granted type certification on January 20, 1959.<ref name="1A16">{{Cite web|url = http://www.airweb.faa.gov/Regulatory_and_Guidance_Library/rgMakeModel.nsf/0/f257e75636f09ba586256a070066912c/$FILE/1a16.pdf|title = Type Certificate Data Sheet No. 1A16 |accessdate = 1 January 2011|last = [[Federal Aviation Administration]]|authorlink = |date=February 2001}}</ref>

The ownership of the Ag-Cat design has changed hands several times.  Grumman transferred ownership to its commercial aircraft subsidiary, Grumman American, in 1973.  The Grumman American subsidiary, which also owned the Grumman Gulfstream design series, was sold to American Jet Industries in 1978.  From initial production through 1981, Schweizer built 2,455 aircraft under contract.<ref name="Taylor">Taylor John WR: ''Jane's Pocket Book Light Aircraft - Second Edition'', page 215. Jane's Publishing Company, 1982. ISBN 0-7106-0195-6</ref> In 1981 Schweizer bought the rights to the design and continued production under the name Schweizer Ag-Cat.<ref name="Montgomery"/>

Schweizer sold the design to Ag-Cat Corp. of Malden, [[Missouri]] in 1995. Five model G-164B aircraft were produced, and registered, before Ag-Cat Corp. entered bankruptcy.  One additional aircraft, a G-164BT500, is listed in the FAA registry as having been produced by Ag-Cat Corp., however no tail number was issued.<ref>{{cite web|url=http://registry.faa.gov/aircraftinquiry/AcftRef_Results.aspx?Mfrtxt=&Modeltxt=G-164&PageNo=1|title=FAA Registry - Aircraft - Make / Model Inquiry|work=faa.gov|accessdate=10 August 2015}}</ref>  This may have been an upgrade to an existing airframe.

In February 2001 the design was sold to Allied Ag-Cat Productions Inc. of [[Walnut Ridge, Arkansas]].<ref name="1A16" /> Allied Ag-Cat are not producing new aircraft although a related company operates a large fleet of Ag-Cats.

The basic airframe incorporates many safety innovations, including a pressurized cockpit to keep pesticides out, air conditioning and a fuselage structure that is designed to progressively collapse in the event of a collision.<ref name="Montgomery"/>  Lippert and Koch were recognized for their innovation in agricultural aircraft, being awarded the Puffer Award by Delta Air Lines in 1974. <ref>{{cite web|url=http://www.agaviation.org/pufferaward|title=National Agricultural Aviation Association|work=agaviation.org|accessdate=10 August 2015}}</ref>

Floats were approved for the aircraft in the early 1990s in Australia.{{Citation needed|date=July 2013}}

==Variants==
[[File:Grumman G-164 Ag-Cat - Góraszka.jpg|right|thumb|An Ag Cat set up for wingwalking]]
[[File:Grumman G-164A Ag-Cat Air Show Góraszka 2007.jpg|thumb|right|Grumman G-164A Ag Cat]]
[[File:Grumman G-164 Ag-Cat AVV Creek.jpg|thumb|G-164 Ag-Cat]]
;Ag Cat
:The basic model Ag Cat was certified with four different engines: the 220-225&nbsp;hp (164-168&nbsp;kW) [[Continental Motors Company|Continental Motors]] [[radial engine]], the 240&nbsp;hp (179&nbsp;kW) Gulf Coast W-670-240 radial engine, the 245&nbsp;hp (183&nbsp;kW) Jacobs L-4M or L-4MB radial engine and the 275-300&nbsp;hp (205-224&nbsp;kW) Jacobs R-755 radial engine. A total of 400 of this model were produced.<ref name="Taylor"/>

;Super Ag Cat A/450
:The G-164A became the main model starting with serial number 401. This model featured a 450&nbsp;hp (335&nbsp;kW) [[Pratt & Whitney R-985]] radial engine along with a higher gross weight, increased fuel capacity, larger diameter wheels and improved brakes.<ref name="Taylor"/>

;Super Ag Cat A/600
:The A/600 incorporated the same improvements embodied in the A/400, but was powered by a [[Pratt & Whitney R-1340]] radial engine of 600&nbsp;hp (450&nbsp;kW).<ref name="Taylor"/>

;Super Ag Cat B/450
:The B/450 improved on the "A" model by increasing the wingspan from 35&nbsp;ft 11&nbsp;in (10.95&nbsp;m) to 42&nbsp;ft 3&nbsp;in (12.88&nbsp;m). The  fin and rudder were enlarged and the fuselage was also lengthened.<ref name="Taylor"/> The upper wing was raised on the "B" model by 8&nbsp;in (20&nbsp;cm) to reduce aerodynamic interference between the wings and improve cockpit visibility.<ref name="Montgomery"/>

;Super Ag Cat B/525
:The B/525 incorporated the design improvements of the B/450, but was powered by a Continental /Page R-975 engine.<ref name="Taylor"/>

;Super Ag Cat C/600
:The C/600 first flew in 1976. It is similar to the model B/450 but has its fuselage further stretched to incorporate a 500&nbsp;US gal (1,892&nbsp;[[litre|l]]) agricultural hopper. The model is powered by a 600&nbsp;hp (450&nbsp;kW) [[Pratt & Whitney R-1340]] radial engine.<ref name="Taylor"/>

;Turbo Ag Cat D/T
:The "D" model is similar to the C/600 but replaced the radial piston engine with a [[Pratt & Whitney PT6A]] turboprop powerplant of 680&nbsp;shp (507&nbsp;kW).<ref name="Taylor"/>

;Turbo Ag Cat D/ST
:The D/ST model is identical to the Turbo Ag Cat D/T, but the engine was a [[Pratt & Whitney PT6A]] turboprop powerplant of 750&nbsp;shp (559&nbsp;kW).<ref name="Taylor"/>

;Turbo Ag Cat D/SST
:The D/SST model is identical to the Turbo Ag Cat D/T, but is powered by a [[Pratt & Whitney PT6A]] turboprop powerplant of 850&nbsp;shp (634&nbsp;kW).<ref name="Taylor"/>

;Ag Cat B-Plus/600
:Introduced in 1982 the B-Plus/600 is powered by a Pratt & Whitney R-1340 of 600&nbsp;hp (450&nbsp;kW). It has the larger hopper of the "C" model.<ref name="Taylor"/>

;Ag Cat B-Plus/450
:Also made available for the first time in 1982 the lowered powered B-Plus/450 is powered by a Pratt & Whitney R-985 of 450&nbsp;hp (335&nbsp;kW). This model was available only as a custom order.<ref name="Taylor"/>

;Marsh G-164 C-T Turbo Cat
:This aftermarket conversion was created by re-engining the Super Ag Cat C/600 with a [[Garrett TPE331|Garrett TPE331-1-101]] turboprop, de-rated to 600&nbsp;hp (450&nbsp;kW).<ref name="Taylor"/>
;Mid-Continent King Cat
:This aftermarket conversion of the Super Ag Cat C/600 replaced the 600&nbsp;hp (450&nbsp;kW) Pratt & Whitney R-1340 engine with a Wright R-1820-202A radial engine that produces 1,200&nbsp;hp (895&nbsp;kW).<ref name="Taylor"/>
;Ethiopian Airlines Eshet
:License-built Ag-Cat Super B Turbine with local content from 1986.<ref>Lambert 1994, p. 70</ref>
<!-- ==Operators== -->

==Aircraft on display==
*[[National Air and Space Museum]], [[Steven F. Udvar-Hazy Center]] - G-164A Super Ag-Cat A/600<ref name="Smithsonian" />

==Specifications (G-164B Super B Turbine)==
{{aircraft specifications|
|plane or copter?=plane
|jet or prop?=prop
|ref=Jane's Civil and Military Aircraft Upgrades 1994–95<ref name="Janes Up94 p398-9">Michell 1994, pp. 398–399.</ref>
|crew=one 
|capacity=400 US gal (1,514 liters) in forward hopper
|length main=27 ft 7¼ in
|length alt=8.41 m
|span main=42 ft 4½ in
|span alt=12.92 m
|height main=12 ft 1 in
|height alt=3.68 m
|area main=392.7 ft²
|area alt=36.48 m²
|empty weight main=3,150 lb
|empty weight alt=1,429 kg
|loaded weight main=
|loaded weight alt=
|max takeoff weight main=7,020 lb
|max takeoff weight alt=3,184 kg
|engine (prop)=[[Pratt & Whitney PT6A]]-34AG
|type of prop=[[turboprop]]
|number of props=1
|power main=750 shp
|power alt=560 kW
|max speed main= 
|max speed alt= 
|cruise speed main=113 knots
|cruise speed alt=130 mph, 209 km/h
|stall speed main=56 knots
|stall speed alt=64 mph, 103 km/h
|never exceed speed main=136 knots
|never exceed speed alt=157 mph, 252 km/h
|range main=172 nmi
|range alt=198 mi, 318 km
|ceiling main= 
|ceiling alt= 
|climb rate main= 
|climb rate alt= 
|loading main= 
|loading alt= 
|thrust/weight=
|power/mass main= 
|power/mass alt= 
* Armament: None.
}}

==See also==
{{aircontent|
|sequence=
|related=
|similar aircraft=
* [[Aero Boero 260AG]]
* [[Air Tractor AT-802]]
* [[Auster Agricola]]
* [[Ayres Thrush]]
* [[Cessna 188]]
* [[Embraer EMB 202 Ipanema]]
* [[PAC Cresco]]
* [[PAC Fletcher]]
* [[Piper PA-25 Pawnee]]
* [[PZL-106 Kruk]]
* [[Zlin Z-37 Cmelak]]
|lists=
* [[List of civil aircraft]]
|see also=
}}

==References==
;Notes
{{reflist}}
; Bibliography 
*{{cite book |editor-last= Lambert|editor-first= Mark|authorlink= |title= Jane's All the World's Aircraft 1994-95|year= 1994|publisher= Jane's Information Group |location=Coulsdon, Surrey, United Kingdom|isbn=0-7106-1160-9}}
* Michell, Simon. ''Jane's Civil and Military Aircraft Upgrades 1994-95''. Coulsdon, UK:Jane's Information Group, 1994. ISBN 0-7106-1208-7.

==External links==
{{Commons category|Grumman G-164 Ag-Cat}}
* [http://www.airliners.net/info/stats.main?id=14 Airliners.net: Grumman G-164 Ag Cat]

{{Grumman aircraft}}
{{Schweizer aircraft}}

[[Category:Biplanes]]
[[Category:Single-engined tractor aircraft]]
[[Category:United States agricultural aircraft 1950–1959]]
[[Category:Grumman aircraft|Ag Cat]]