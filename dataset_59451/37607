{{Use mdy dates|date=February 2013}}
{{Infobox ice hockey player
| image = 130205 Steve Begin Flames.png
| image_size = 230px
| image_alt = Upper body of a man staring intently into the distance.  He is in a full hockey uniform; the jersey is red with black and yellow trim, and a black stylized "C" logo on his chest.
| position = [[Centre (ice hockey)|Centre]]
| shoots = Left
| played_for = [[Calgary Flames]]<br />[[Montreal Canadiens]]<br />[[Dallas Stars]]<br />[[Boston Bruins]]<br />[[Nashville Predators]]
| height_ft = 5
| height_in = 11
| weight_lb = 195
| birth_date = {{birth date and age|mf=yes|1978|6|14}}
| birth_place = [[Trois-Rivières]], [[Quebec|QC]], [[Canada|CAN]]
| draft = 40th overall
| draft_year = 1996
| draft_team = [[Calgary Flames]]
| career_start = 1997
| career_end = 2013
}}
'''Joseph Denis Stéphan Bégin''' ({{IPA-fr|stefɑ̃ beʒɛ̃}}); born June 14, 1978) is a [[Canadians|Canadian]] former professional [[ice hockey]] [[centre (ice hockey)|centre]] who played in 13 [[National Hockey League]] (NHL) seasons.  He was a second-round selection of the [[Calgary Flames]], 40th overall, in the [[1996 NHL Entry Draft]], and played with the Flames, [[Montreal Canadiens]], [[Dallas Stars]], [[Boston Bruins]] and [[Nashville Predators]] in his NHL career.  After missing a full season due to injury, Bégin made a successful comeback by rejoining the Flames in [[2012–13 NHL season|2012–13]] before another injury forced his retirement.

Bégin played [[junior hockey]] in Quebec with the [[Val-d'Or Foreurs]] where he was a member of their [[Quebec Major Junior Hockey League]] (QMJHL) championship winning team in 1998.  He also led the [[Saint John Flames]] to the [[American Hockey League]] (AHL)'s [[Calder Cup]] championship in 2001 and won the [[Jack A. Butterfield Trophy]] as the most valuable player of the playoffs. Bégin adopted a role as a defensive specialist and [[grinder (ice hockey)|grinder]] in an NHL career where he has played over 500 games.

==Personal life==
A native of [[Trois-Rivières]], Quebec,<ref name="LOH">{{cite web |url=http://www.legendsofhockey.net/LegendsOfHockey/jsp/SearchPlayer.jsp?player=11469 |title=Steve Begin biography |publisher=Hockey Hall of Fame |accessdate=2013-04-25}}</ref> Bégin grew up in a single parent household, raised by his father Gilles on a welfare income.<ref name="HeraldMastertonNominee">{{cite news |last=Cruickshank |first=Scott |title=Begin's perseverance pays off in spades |work=Calgary Herald |date=2013-04-24 |page=C4}}</ref> Gilles worked as a landscaper, while Steve often helped his father at work until he was 18 years old.<ref name="1213FlamesYearbook">{{cite book |title=Player profile: Steve Begin |work=2012–13 Calgary Flames Yearbook |publisher=Calgary Flames Hockey Club |year=2013 |page=75}}</ref>

Introduced to hockey by family friends, Bégin began playing at age six and was an offensively minded player in his [[minor hockey]] years.<ref name="2008Profile">{{cite news |last=Hickey |first=Pat |url=http://www.faceoff.com/hockey/teams/story.html?id=1087816&add_feed_url=http%3A%2F%2Fwww.faceoff.com%2Fscripts%2FSP6Atom.aspx%3Fid%3D912139 |title=Bégin: Scoring machine to defensive specialist |work=Montreal Gazette |date=2008-12-17 |accessdate=2013-04-28}}</ref> He wore second-hand equipment as his father struggled to pay the costs of hockey, but from a young age expressed his confidence he would make it to the [[National Hockey League]] (NHL).<ref name="HeraldMastertonNominee" />

Bégin moved to [[Val-d'Or, Quebec]] to play [[junior hockey]], where he met his wife, Amélie.<ref name="1213FlamesYearbook" /> The couple have two daughters and eventually settled in Montreal.<ref name="SunMastertonNominee" />

==Playing career==

===Junior===
Bégin played three years for the [[Val-d'Or Foreurs]] of the [[Quebec Major Junior Hockey League]] (QMJHL) between 1995 and 1998, where he scored 44 [[goal (ice hockey)|goals]] and recorded 117 [[point (ice hockey)|points]] to go along with 520 [[penalty (ice hockey)|penalty minutes]] in 157 games.<ref name="NHLProfile">{{cite web |url=http://www.nhl.com/ice/player.htm?id=8464994 |title=Steve Begin player card |publisher=National Hockey League |accessdate=2013-04-25}}</ref> The [[Calgary Flames]] selected him with their second round pick, 40th overall, in the [[1996 NHL Entry Draft]].<ref>{{cite book |editor-last=Halls |editor-first=Pat |title=2002–03 Calgary Flames Media Guide |publisher=Calgary Flames Hockey Club |year=2002 |page=92}}</ref> Following the [[1996–97 QMJHL season]], the Flames assigned Bégin to their [[American Hockey League]] (AHL) affiliate, the [[Saint John Flames]], with whom he recorded two assists in four playoff games.<ref name="0203FlamesMG">{{cite book |editor-last=Halls |editor-first=Pat |title=2002–03 Calgary Flames Media Guide |publisher=Calgary Flames Hockey Club |year=2002 |page=69}}</ref>

Bégin earned a spot with the Calgary Flames to begin the [[1997–98 NHL season|1997–98 season]].<ref name="LOH" /> He made his NHL debut on October 1, 1998, against the [[Detroit Red Wings]], and appeared in five games before he was returned to Val-d'Or for the remainder of the season on October 28.<ref name="0203FlamesMG" /> The Foreurs went on to win the [[President's Cup (QMJHL)|President's Cup]] as QMJHL champions.<ref name="SignsNashville">{{cite news |url=http://www.clarksvilleonline.com/2010/10/22/predators-sign-forward-steve-begin-to-a-one-year-two-way-contract/ |title=Predators sign Forward Steve Begin to a One-year, Two-Way Contract |publisher=Clarksville Online |date=2010-10-22 |accessdate=2013-04-26}}</ref>

Bégin made his lone appearance with the Canadian national team that season, playing and serving as an [[captain (ice hockey)|alternate captain]] with the [[Canada men's national junior ice hockey team|junior team]] at the [[1998 World Junior Ice Hockey Championships|1998 World Junior Hockey Championship]].<ref name="LOH" /> He had no points and ten penalty minutes in seven games in what was an eighth place finish for Team Canada.<ref>{{cite book |editor-last=Podnieks |editor-first=Andrew |title=IIHF Guide & Record Book 2012 |year=2011 |publisher=International Ice Hockey Federation |isbn=978-0-7710-9598-6 |page=456}}</ref>

===Calgary Flames===
After graduating from junior hockey, Bégin played his first full professional season with Saint John in [[1998–99 AHL season|1998–99]]. He had 20 points and 156 penalty minutes in 73 games, and scored his first professional goal on October 8, 1998, against the [[St. John's Maple Leafs]].<ref name="0203FlamesMG" /> Bégin spent the majority of the next two seasons in Saint John, but made sporadic appearances with Calgary.<ref name="PodnieksPlayers">{{cite book |last=Podnieks |first=Andrew |title=Players: The ultimate A–Z guide of everyone who has ever played in the NHL |publisher=Doubleday Canada |year=2003 |location=Toronto |page=52 |isbn=0-385-25999-9}}</ref> He appeared in 13 games with the Flames in [[1999–2000 NHL season|1999–2000]] and scored his first NHL goal on February 12, 2000, on goaltender [[Sean Burke]] of the [[Phoenix Coyotes]].<ref name="0203FlamesMG" /> He played in four NHL games in [[2000–01 NHL season|2000–01]] while enjoying the best statistical season of his professional career in Saint John, scoring 28 points in 58 games.<ref name="LOH" /> In the playoffs, Bégin led Saint John to the [[Calder Cup]] championship. He scored 17 points in 19 games and was named the recipient of the [[Jack A. Butterfield Trophy]] as the most valuable player of the post-season. His 17 points were fourth best in the AHL, and 10 goals were second.<ref name="0203FlamesMG" />

Bégin played his first full NHL season in [[2001–02 NHL season|2001–02]], appearing in 53 games for the Flames where he scored 7 goals and 12 points to go along with 79 penalty minutes.<ref name="0203FlamesMG" /> He had 4 goals and 4 assists in 50 games for Calgary in [[2002–03 NHL season|2002–03]].<ref name="NHLProfile" /> He left Calgary in the offseason as he was part of a three team trade on July 3, 2003.  Bégin was dealt to the [[Buffalo Sabres]] along with [[Chris Drury]] in exchange for [[Rhett Warrener]] and [[Steve Reinprecht]], whom the Sabres acquired from the [[Colorado Avalanche]] to complete the deal.<ref>{{cite news |url=http://www.cbc.ca/sports/story/2003/07/03/flames030703.html |title=Flames acquire Reinprecht, Warrener |publisher=Canadian Broadcasting Corporation |date=2003-07-05 |accessdate=2013-04-25}}</ref>

===Montreal Canadiens===
[[File:Steve Begin.jpg|thumb|right|Bégin as a member of the Boston Bruins.|alt=A hockey player in a Black uniform with gold trim and a stylized "B" logo on his chest defends his position during a game.]]
Bégin was promoted by Buffalo coach [[Lindy Ruff]] as a hard working, "blue collar" type player that Sabres fans would enjoy watching, but he never played a game with the Sabres. He was exposed to the [[Waivers (NHL)|waiver]] draft prior to the start of the [[2003–04 NHL season]] and claimed by the [[Montreal Canadiens]].<ref>{{cite news |last=Graham |first=Tim |url=http://www.highbeam.com/doc/1P2-22589746.html |title=Sabres waive goodbye to Begin, Bouchard |work=Buffalo News |date=2003-10-04 |accessdate=2013-04-25}}{{subscription required|via=Highbeam}}</ref> He played an energy role for the Canadiens,<ref>{{cite news |last=Todd |first=Jack |url=http://www.boston.com/sports/hockey/bruins/articles/2004/04/19/canadiens_look_to_finish_fight/ |title=Canadiens look to finish fight |work=Boston Globe |date=2004-04-19 |accessdate=2013-04-25}}</ref> and scored 10 goals for Montreal in 52 games.<ref name="NHLProfile" />

After playing through injury in 2003–04, Bégin underwent shoulder surgery that caused him to miss five months of playing time. When he returned to action, an ongoing [[2004–05 NHL lockout|labour dispute]] in the NHL led to his being assigned to the AHL's [[Hamilton Bulldogs (AHL)|Hamilton Bulldogs]] in February 2005, with whom he was immediately counted upon to play a leadership role.<ref name="WithHamilton">{{cite news |last=Mckay |first=Garry |url=http://pqasb.pqarchiver.com/thestar/access/813705741.html?FMT=ABS |title=Why Begin's a hot Dog now; He's a 'leader on and off the ice,' says Hamilton's coach |work=Hamilton Spectator |date=2005-03-29 |accessdate=2013-04-25}}</ref> Bégin returned to the Canadiens in [[2005–06 NHL season|2005–06]] and set career highs in goals (11), points (23) and penalty minutes (113).<ref name="NHLProfile" /> The Montreal media named him the recipient of the Jacques-Beauchamp Molson Trophy, a team award given to a Canadiens' player who played a "dominant role" with the team, without earning any other honours.<ref name="HabsAward">{{cite news |url=http://canadiens.nhl.com/club/news.htm?id=489124 |title=Begin Jacques Beauchamp-Molson Trophy recipient for 2005-06 |publisher=Montreal Canadiens Hockey Club |date=2006-04-14 |accessdate=2013-04-25}}</ref> Plagued by injuries, Bégin missed time due to rib and shoulder injuries.<ref>{{cite news |url=http://www.cbc.ca/sports/hockey/story/2006/12/14/lecavalier-lightning.html |title=Streaking Lecavalier faces Habs |publisher=Canadian Broadcasting Corporation |date=2006-12-14 |accessdate=2013-04-26}}</ref><ref>{{cite news |url=http://www.cbc.ca/sports/hockey/story/2007/12/11/nhl-canadiens-begin-smolinski.html |title=Canadiens forced to shelve Smolinski, Begin |publisher=Canadian Broadcasting Corporation |date=2007-12-11 |accessdate=2013-04-26}}</ref> He appeared in only 52 games in [[2006–07 NHL season|2006–07]] and 44 games in [[2007–08 NHL season|2007–08]] and scored 18 points combined over the two seasons.<ref name="NHLProfile" />

===Dallas, Boston and Nashville===
Bégin was increasingly left out of the Canadiens' playing lineup in [[2008–09 NHL season|2008–09]], and after being sat out of the lineup for five consecutive games, expressed a desire to be traded if the team had no use for his services.  The Canadiens obliged, completing a deal on February 26, 2009, that sent him to the [[Dallas Stars]] in exchange for defenceman [[Doug Janik]].<ref>{{cite news |last=Gordon |first=Sean |url=http://www.theglobeandmail.com/sports/hockey/globe-on-hockey/gordon-so-long-steve/article973321/ |title=So long Steve |work=The Globe and Mail |date=2009-02-26 |accessdate=2013-04-26}}</ref> Stars' general manager [[Les Jackson (ice hockey)|Les Jackson]] promoted Bégin's qualities as a checking-line forward: "Steve is a gritty, honest player with a very strong work ethic. He's good on the penalty kill and he is a competitor in every sense of the word."<ref>{{cite news |url=http://www.nhl.com/ice/news.htm?id=411240 |title=Canadiens trade grinder Steve Begin to Stars for defenceman Doug Janik |publisher=National Hockey League |date=2009-02-26 |accessdate=2013-04-26}}</ref>  Bégin had 12 points in 62 games combined between Montreal and Dallas.<ref name="NHLProfile" />

Leaving Dallas following the season, Bégin signed a one-year contract with the [[Boston Bruins]] for the [[2009–10 NHL season|2009–10 season]].<ref>{{cite news |url=http://usatoday30.usatoday.com/sports/hockey/nhl/2009-07-01-1296361395_x.htm |title=Bruins sign center Steve Begin |work=USA Today |date=2009-07-01 |accessdate=2013-04-26}}</ref> He played in 77 games for Boston, the most of any single season in his career, and recorded 14 points.<ref name="NHLProfile" /> He scored his first career playoff goal, in his 30th post-season game, against the [[Philadelphia Flyers]].<ref>{{cite news |last=Kalman |first=Matt |url=http://sports.espn.go.com/boston/nhl/columns/story?columnist=kalman_matt&id=5154576 |title=Everything fell in line for Bruins |publisher=ESPN |date=2010-05-01 |accessdate=2013-04-26}}</ref> The Bruins opted not to re-sign the 32-year-old Bégin, due both to their salary cap constraints and a desire to build a younger lineup.<ref>{{cite news |last=Murphy |first=James |url=http://espn.go.com/blog/boston/bruins/tag/_/name/steve-begin |title=Begin says B's have shown interest |publisher=ESPN |date=2010-08-30 |accessdate=2013-04-28}}</ref>

Without a contract, Bégin remained a [[free agent]] as the [[2010–11 NHL season|2010–11 season]] began.  He eventually signed a one-year contract with the [[Nashville Predators]] on October 22, 2010, but was assigned to their AHL affiliate, the [[Milwaukee Admirals]].<ref name="SignsNashville" /> Bégin spent the majority of the season in Milwaukee and appeared in only two games with Nashville.<ref name="NHLProfile" />

===Return to Calgary===
Plagued by a hip injury,<ref name="HeraldMastertonNominee" /> Bégin was again left a free agent prior to the [[2011–12 NHL season|2011–12 season]]. He signed a professional tryout offer with the [[Vancouver Canucks]],<ref>{{cite news |url=http://www.cbc.ca/sports/hockey/story/2011/09/07/sp-canucks-tryouts.html |title=Canucks give Begin, Legace tryouts |publisher=Canadian Broadcasting Corporation |date=2011-09-07 |accessdate=2013-04-26}}</ref> but was released by the team during the pre-season.<ref name="HeraldMastertonNominee" /> [[Bob Hartley]], coach of [[ZSC Lions]] in the Swiss [[National League A]] offered him a spot on their team.  Though he had already scheduled surgery to repair his hip, Bégin was willing to put it off to play in Switzerland, but was convinced by Hartley to complete the procedure. His recovery prevented Bégin from playing anywhere during the season.<ref name="SunMastertonNominee">{{cite news |last=Sportak |first=Randy |title=In the Beginning |work=Calgary Herald |date=2013-04-24 |page=S7}}</ref>

{{Quote box| quote ="It's great to put this jersey on. I worked hard this past year. Sometimes, I was twice a day in the gym. I skated five to six times a week. I wanted to be ready. I didn’t want to miss my chance. I knew this would be the last call for me, the last chance I would get. And I made it, so it pays off at the end."|align=right |width=30%|source=—Bégin explains his emotions after earning a spot with the Flames in 2013.<ref>{{cite news |last=Hall |first=Vicki |last2=Johnson |first2=George |url=http://www.calgaryherald.com/sports/hockey/calgary-flames/Flames+notebook+Steve+Begin+hopes+comeback/7842053/story.html |title=Flames notebook: Steve Begin hopes for comeback after long recovery from hip surgery |work=Calgary Herald |date=2013-01-18 |accessdate=2013-04-28}}</ref>}}
When Hartley took over as the Flames' head coach prior to the [[2012–13 NHL season]], he offered Bégin a tryout in Calgary.<ref name="SunMastertonNominee" /> Bégin's work ethic during the team's training camp prior to the [[2012–13 NHL lockout|lockout]]-shortened season earned him a contract with the Flames, who believed he could serve as a penalty killer and energy player.<ref>{{cite news |url=http://www.nhl.com/ice/news.htm?id=651186 |title=Calgary Flames sign veteran centre Steve Begin to one-year contract |publisher=National Hockey League |date=2013-01-18 |accessdate=2013-04-26}}</ref> Over two years since his last NHL game, Bégin returned to the league,<ref name="HeraldMastertonNominee" /> and appeared in his 500th game on February 24, 2013, against the Coyotes.<ref name="500thGame">{{cite news |last=Hall |first=Vicki |url=http://www.calgaryherald.com/sports/hockey/calgary-flames/Calgary+Flames+veteran+Steve+Begin+soldiers/8010120/story.html |title=Begin soldiers into his 500th career NHL game |work=Calgary Herald |date=2013-02-24 |accessdate=2013-04-26}}</ref>  He appeared in 36 games for the Flames, scoring four goals and adding four assists.<ref name="NHLProfile" /> In recognition of his successful comeback, the Calgary chapter of the [[Professional Hockey Writers' Association]] named him the Flames' nominee for the [[Bill Masterton Memorial Trophy]] for dedication and perseverance.<ref name="HeraldMastertonNominee" />

Bégin was signed to a one-year, AHL contract with the [[Abbotsford Heat]] for the [[2013–14 AHL season|2013–14 season]].<ref>{{cite news |url=http://www.nhl.com/ice/news.htm?id=681163 |title=Flames sign Begin to minor-league contract |publisher=National Hockey League |date=2013-08-28 |accessdate=2014-01-16}}</ref> He was unable to play due to another injury and, after doctors informed him that he would miss the entire season, Bégin announced his retirement on January 16, 2014.<ref>{{cite news |url=http://www.rds.ca/hockey/lnh/b%C3%A9gin-annonce-sa-retraite-avec-%C3%A9motion-1.832155 |title=Bégin annonce sa retraite avec émotion |language=French |publisher=Réseau des sports |date=2014-01-16 |accessdate=2014-01-16}}</ref>

==Playing style==
Bégin's father taught his son that his best chance of making a career in hockey was as a defensive specialist and impressed on him the need for a strong work ethic.  He was raised to follow role players like [[Mario Tremblay]], [[Guy Carbonneau]] and [[Bob Gainey]] rather than stars such as [[Guy Lafleur]] or [[Wayne Gretzky]].<ref name="2008Profile" />  Bégin has played the majority of his career as a [[grinder (ice hockey)|grinder]], willing to play a physical game.<ref name="500thGame" /> His style often results in bruises and injuries,<ref name="2008Profile" /> a part of the game he relishes.  Speaking of his comeback with the Flames in 2013, he noted: "What I missed the most was the pain of playing hockey. The pain from blocking shots, getting hit, hitting people... I missed it a lot."<ref name="SunMastertonNominee" />  That attitude impressed his coaches; Bob Hartley argued that players like Bégin are a valuable commodity: "Players with the commitment of Steve Begin, I really believe that there’s not enough (of them) in the NHL.  I always admired the way that he played. I look at the spirit, the leadership. Pretty amazing what he’s done for us."<ref>{{cite news |last=Cruickshank |first=Scott |url=http://www.calgaryherald.com/sports/Heart+soul+Flames+forward+Begin+relishes+turn+goal+scoring+hero/8264002/story.html |title=Heart-and-soul Flames forward Begin relishes turn as a goal-scoring hero |work=Calgary Herald |date=2013-04-18 |accessdate=2013-04-28}}</ref>

==Career statistics==

===Regular season and playoffs===
{| border="0" cellpadding="1" cellspacing="0" style="text-align:center; width:50em"
|- bgcolor="#e0e0e0"
! colspan="3"  bgcolor="#ffffff" |  
! rowspan="99" bgcolor="#ffffff" |  
! colspan="5"                    | [[Regular season]]
! rowspan="99" bgcolor="#ffffff" |  
! colspan="5"                    | [[Playoffs]]
|- bgcolor="#e0e0e0"
! [[Season (sports)|Season]]
! Team
! League
! GP
! [[Goal (ice hockey)|G]]
! [[Assist (ice hockey)|A]]
! [[Point (ice hockey)|Pts]]
! [[Penalty (ice hockey)|PIM]]
! GP
! G
! A
! Pts
! PIM
|-
| [[1995–96 QMJHL season|1995–96]]
| [[Val-d'Or Foreurs]]
| [[Quebec Major Junior Hockey League|QMJHL]]
| 64
| 13
| 23
| 36
| 218
| 13
| 1
| 3
| 4
| 33
|- bgcolor="#f0f0f0"
| [[1996–97 QMJHL season|1996–97]]
| Val-d'Or Foreurs
| QMJHL
| 58
| 13
| 33
| 46
| 207
| 10
| 0
| 3
| 3
| 8
|-
| [[1996–97 AHL season|1996–97]]
| [[Saint John Flames]]
| [[American Hockey League|AHL]]
| —
| —
| —
| —
| —
| 4
| 0
| 2
| 2
| 6
|- bgcolor="#f0f0f0"
| [[1997–98 QMJHL season|1997–98]]
| Val-d'Or Foreurs
| QMJHL
| 35
| 18
| 17
| 35
| 73
| 15
| 2
| 12
| 14
| 34
|-
| [[1997–98 NHL season|1997–98]]
| [[Calgary Flames]]
| [[National Hockey League|NHL]]
| 5
| 0
| 0
| 0
| 23
| —
| —
| —
| —
| —
|- bgcolor="#f0f0f0"
| [[1998–99 AHL season|1998–99]]
| Saint John Flames
| AHL
| 73
| 11
| 9
| 20
| 156
| 7
| 2
| 0
| 2
| 18
|-
| [[1999–2000 AHL season|1999–00]]
| Saint John Flames
| AHL
| 47
| 13
| 12
| 25
| 99
| —
| —
| —
| —
| —
|- bgcolor="#f0f0f0"
| [[1999–2000 NHL season|1999–00]]
| Calgary Flames
| NHL
| 13
| 1
| 1
| 2
| 18
| —
| —
| —
| —
| —
|-
| [[2000–01 AHL season|2000–01]]
| Saint John Flames
| AHL
| 58
| 14
| 14
| 28
| 109
| 19
| 10
| 7
| 17
| 18 
|- bgcolor="#f0f0f0"
| [[2000–01 NHL season|2000–01]]
| Calgary Flames
| NHL
| 4
| 0
| 0
| 0
| 21
| —
| —
| —
| —
| —
|-
| [[2001–02 NHL season|2001–02]]
| Calgary Flames
| NHL
| 51
| 7
| 5
| 12
| 79
| —
| —
| —
| —
| —
|- bgcolor="#f0f0f0"
| [[2002–03 NHL season|2002–03]]
| Calgary Flames
| NHL
| 50
| 3
| 1
| 4
| 51
| —
| —
| —
| —
| —
|-
| [[2003–04 NHL season|2003–04]]
| [[Montreal Canadiens]]
| NHL
| 52
| 10
| 5
| 15
| 41
| 9
| 0
| 1
| 1
| 10
|- bgcolor="#f0f0f0"
| [[2004–05 AHL season|2004–05]]
| [[Hamilton Bulldogs (AHL)|Hamilton Bulldogs]]
| AHL
| 21
| 10
| 3
| 13
| 20
| 4
| 0
| 2
| 2
| 8
|-
| [[2005–06 NHL season|2005–06]]
| Montreal Canadiens
| NHL
| 76
| 11
| 12
| 23
| 113
| 2
| 0
| 0
| 0
| 2
|- bgcolor="#f0f0f0"
| [[2006–07 NHL season|2006–07]]
| Montreal Canadiens
| NHL
| 52
| 5
| 5
| 10
| 46
| —
| —
| —
| —
| —
|-
| [[2007–08 NHL season|2007–08]]
| Montreal Canadiens
| NHL
| 44
| 3
| 5
| 8
| 48
| 12
| 0
| 3
| 3
| 8
|- bgcolor="#f0f0f0"
| [[2008–09 NHL season|2008–09]]
| Montreal Canadiens
| NHL
| 42
| 6
| 4
| 10
| 27
| —
| —
| —
| —
| —
|-
| 2008–09
| [[Dallas Stars]]
| NHL
| 20
| 1
| 1
| 2
| 15
| —
| —
| —
| —
| —
|- bgcolor="#f0f0f0"
| [[2009–10 NHL season|2009–10]]
| [[Boston Bruins]]
| NHL
| 77
| 5
| 9
| 14
| 53
| 13
| 1
| 0
| 1
| 10
|-
| [[2010–11 AHL season|2010–11]]
| [[Milwaukee Admirals]]
| AHL
| 36
| 3
| 3
| 6
| 30
| 13
| 3
| 4
| 7
| 12
|- bgcolor="#f0f0f0"
| [[2010–11 NHL season|2010–11]]
| [[Nashville Predators]]
| NHL
| 2
| 0
| 0
| 0
| 4
| —
| —
| —
| —
| —
|-
| 2011–12
| 
| 
| colspan=10|''Did not play – injured''
|- bgcolor="#f0f0f0"
| [[2012–13 NHL season|2012–13]]
| Calgary Flames
| NHL
| 36
| 4
| 4
| 8
| 22
| —
| —
| —
| —
| —
|- bgcolor="#e0e0e0"
! colspan="3" | NHL totals
! 524
! 56
! 52
! 108
! 561
! 36
! 3
! 7
! 10
! 30
|}

===International===
{| border="0" cellpadding="1" cellspacing="0" style="text-align:center; width:50em"
|- ALIGN="center" bgcolor="#e0e0e0"
! Year
! Team
! Event
! GP
! G
! A
! Pts
! PIM
|-
| [[1998 World Junior Ice Hockey Championships|1998]]
| [[Canada men's national junior ice hockey team|Canada]]
| [[IIHF World U20 Championship|WJC]]
| 7
| 0
| 0
| 0
| 10
|}

==Awards and honours==
{| class="wikitable"
|-
! Award
! Year
!
|-  style="text-align:center; background:#e0e0e0;"
| colspan="3" | [[American Hockey League]]
|-
| [[Calder Cup]] champion
| [[2000–01 AHL season|2000–01]]
| <ref name="0203FlamesMG" />
|-
| [[Jack A. Butterfield Trophy]]<br /><small>Most valuable player of the playoffs</small>
| 2001
| <ref name="0203FlamesMG" />
|-  style="text-align:center; background:#e0e0e0;"
| colspan="3" | NHL team awards
|-
| Jacques-Beauchamp Molson Trophy<br /><small>Montreal – Unheralded "dominant" player</small>
| [[2006–07 Montreal Canadiens season|2006–07]]
| <ref name="HabsAward" />
|}

==References==
{{reflist|30em}}

==External links==
*{{Eliteprospects}}
*{{nhlprofile|8464994}}
*{{hockeydb|26801}}

{{good article}}

{{DEFAULTSORT:Begin, Steve}}
[[Category:1978 births]]
[[Category:Living people]]
[[Category:Boston Bruins players]]
[[Category:Calgary Flames draft picks]]
[[Category:Calgary Flames players]]
[[Category:Canadian ice hockey centres]]
[[Category:Dallas Stars players]]
[[Category:French Quebecers]]
[[Category:Hamilton Bulldogs (AHL) players]]
[[Category:Sportspeople from Trois-Rivières]]
[[Category:Milwaukee Admirals players]]
[[Category:Montreal Canadiens players]]
[[Category:Nashville Predators players]]
[[Category:Saint John Flames players]]
[[Category:Val-d'Or Foreurs players]]
[[Category:Ice hockey people from Quebec]]